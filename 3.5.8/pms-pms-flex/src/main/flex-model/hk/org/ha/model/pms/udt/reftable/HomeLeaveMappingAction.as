/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.udt.reftable {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.udt.reftable.HomeLeaveMappingAction")]
    public class HomeLeaveMappingAction extends Enum {

        public static const Insert:HomeLeaveMappingAction = new HomeLeaveMappingAction("Insert", _, "A", "Insert");
        public static const Update:HomeLeaveMappingAction = new HomeLeaveMappingAction("Update", _, "C", "Update");
        public static const Delete:HomeLeaveMappingAction = new HomeLeaveMappingAction("Delete", _, "D", "Delete");

        function HomeLeaveMappingAction(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || Insert.name), restrictor, (dataValue || Insert.dataValue), (displayValue || Insert.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [Insert, Update, Delete];
        }

        public static function valueOf(name:String):HomeLeaveMappingAction {
            return HomeLeaveMappingAction(Insert.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):HomeLeaveMappingAction {
            return HomeLeaveMappingAction(Insert.dataConstantOf(dataValue));
        }
    }
}