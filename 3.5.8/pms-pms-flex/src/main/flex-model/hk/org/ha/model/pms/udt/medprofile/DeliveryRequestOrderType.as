/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.udt.medprofile {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.udt.medprofile.DeliveryRequestOrderType")]
    public class DeliveryRequestOrderType extends Enum {

        public static const None:DeliveryRequestOrderType = new DeliveryRequestOrderType("None", _, "-", "None");
        public static const NewOrder:DeliveryRequestOrderType = new DeliveryRequestOrderType("NewOrder", _, "N", "New");
        public static const RefillOrder:DeliveryRequestOrderType = new DeliveryRequestOrderType("RefillOrder", _, "R", "Refill");
        public static const Both:DeliveryRequestOrderType = new DeliveryRequestOrderType("Both", _, "B", "Both");

        function DeliveryRequestOrderType(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || None.name), restrictor, (dataValue || None.dataValue), (displayValue || None.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [None, NewOrder, RefillOrder, Both];
        }

        public static function valueOf(name:String):DeliveryRequestOrderType {
            return DeliveryRequestOrderType(None.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):DeliveryRequestOrderType {
            return DeliveryRequestOrderType(None.dataConstantOf(dataValue));
        }
    }
}