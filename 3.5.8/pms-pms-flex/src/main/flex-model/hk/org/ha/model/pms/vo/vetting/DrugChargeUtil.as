package hk.org.ha.model.pms.vo.vetting {
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	
	import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
	import hk.org.ha.model.pms.udt.vetting.ActionStatus;
	import hk.org.ha.model.pms.udt.vetting.RegimenDurationUnit;
	import hk.org.ha.model.pms.vo.charging.PatientSfi;
	import hk.org.ha.model.pms.vo.charging.PatientSfiDtl;
	import hk.org.ha.model.pms.vo.rx.DoseGroup;
	import hk.org.ha.model.pms.vo.rx.Regimen;
	import hk.org.ha.model.pms.vo.security.PropMap;
	
	[Bindable]
	public class DrugChargeUtil {
		
		public function DrugChargeUtil():void
		{			
			super();			
		}	
		
		public static function sfiSubsidizedExceedMaxSupplyDay(pharmOrderItem:PharmOrderItem, inPatHospCode:String, sfiQty:Number, issueQty:Number, patientSfi:PatientSfi, propMap:PropMap):Boolean {
			if ( isActiveSfiSubsidizedItem(pharmOrderItem, inPatHospCode, patientSfi) ) {
				return (calItemSupplyDay(pharmOrderItem, sfiQty, issueQty) > propMap.getValueAsInteger("charging.sfi.subsidizeItem.duration"));
			}
			return false;
		}
		
		public static function isSfiSubsidizedItemStatus(pharmOrderItem:PharmOrderItem):Boolean {
			return ( pharmOrderItem.actionStatus == ActionStatus.PurchaseByPatient || pharmOrderItem.actionStatus == ActionStatus.SafetyNet );
		}
		
		private static function isActiveSfiSubsidizedItem(pharmOrderItem:PharmOrderItem, inPatHospCode:String, patientSfi:PatientSfi):Boolean 
		{	
			if ( !isSfiSubsidizedItemStatus(pharmOrderItem) ) {
				return false;
			}
			
			if ( patientSfi == null ) {
				return false;
			}
			
			var patHospCodeFlag:Boolean = false;
			var itemCodeFlag:Boolean = false;
			
			for each ( var patientSfiDtl:PatientSfiDtl in patientSfi.patientSfiDtlList ) {
				patHospCodeFlag = isSubsidizedPatHospCode(inPatHospCode, patientSfiDtl.hospCodeList);
				itemCodeFlag = isSubsidizedItemCode(pharmOrderItem.itemCode, patientSfiDtl.itemCodeList);
				
				if ( patHospCodeFlag && itemCodeFlag ) {
					break;
				}
			}
			
			return ( patHospCodeFlag && itemCodeFlag );
		}
		
		private static function isSubsidizedPatHospCode(inPatHospCode:String, hospCodeList:ListCollectionView):Boolean {
			for each ( var hospCode:String in hospCodeList ) {
				if ( inPatHospCode == hospCode ) {
					return true;
				}
			}
			return false;
		}
		
		private static function isSubsidizedItemCode(inItemCode:String, itemCodeList:ListCollectionView):Boolean {
			for each ( var itemCode:String in itemCodeList ) {
				if ( itemCode == inItemCode ) {
					return true;
				}
			}
			return false;
		}
		
		private static function calItemSupplyDay(pharmOrderItem:PharmOrderItem, sfiQty:Number, issueQty:Number):Number
		{
			if ( isNaN(sfiQty) || sfiQty == 0 ) {
				return 0;
			}
			
			if ( isNaN(issueQty) || issueQty == 0 ) {
				return 0;
			}
			
			if ( sfiQty > issueQty ) {
				return 0;
			}

			var regimen:Regimen = pharmOrderItem.regimen;
			var durationInDay:Number = 0;
			
			for each ( var doseGroup:DoseGroup in regimen.doseGroupList ) {
				durationInDay += doseGroup.duration * getDurationInDayByUnit(doseGroup.durationUnit);
			}			

			return Math.floor( (sfiQty / issueQty) * durationInDay );
		}
		
		private static function getDurationInDayByUnit(durationUnit:RegimenDurationUnit):Number 
		{
			var result:Number = 1;
			switch(durationUnit){
				case RegimenDurationUnit.Day:
					result = 1;
					break;
				case RegimenDurationUnit.Week:
					result = 7;
					break;
				case RegimenDurationUnit.Month:
					result = 28;
					break;
				case RegimenDurationUnit.Cycle:
					result = 28;
					break;
			}
			return result;
		}
		
	}
}