/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.udt.reftable {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.udt.reftable.OperationProfileType")]
    public class OperationProfileType extends Enum {

        public static const Eds:OperationProfileType = new OperationProfileType("Eds", _, "E", "EDS");
        public static const Pms:OperationProfileType = new OperationProfileType("Pms", _, "P", "PMS");
        public static const NonPeak:OperationProfileType = new OperationProfileType("NonPeak", _, "N", "Non-Peak");

        function OperationProfileType(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || Eds.name), restrictor, (dataValue || Eds.dataValue), (displayValue || Eds.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [Eds, Pms, NonPeak];
        }

        public static function valueOf(name:String):OperationProfileType {
            return OperationProfileType(Eds.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):OperationProfileType {
            return OperationProfileType(Eds.dataConstantOf(dataValue));
        }
    }
}