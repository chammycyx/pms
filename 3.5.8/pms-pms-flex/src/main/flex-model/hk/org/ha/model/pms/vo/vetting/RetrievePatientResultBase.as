/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (RetrievePatientResult.as).
 */

package hk.org.ha.model.pms.vo.vetting {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import hk.org.ha.model.pms.persistence.disp.Patient;

    [Bindable]
    public class RetrievePatientResultBase implements IExternalizable {

        private var _patient:Patient;
        private var _successFlag:Boolean;

        public function set patient(value:Patient):void {
            _patient = value;
        }
        public function get patient():Patient {
            return _patient;
        }

        public function set successFlag(value:Boolean):void {
            _successFlag = value;
        }
        public function get successFlag():Boolean {
            return _successFlag;
        }

        public function readExternal(input:IDataInput):void {
            _patient = input.readObject() as Patient;
            _successFlag = input.readObject() as Boolean;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_patient);
            output.writeObject(_successFlag);
        }
    }
}