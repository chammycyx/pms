/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (EhrAllergyDetail.as).
 */

package hk.org.ha.model.pms.vo.alert.ehr {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import mx.collections.ListCollectionView;

    [Bindable]
    public class EhrAllergyDetailBase implements IExternalizable {

        private var _adrDetailList:ListCollectionView;
        private var _allergenDetailList:ListCollectionView;

        public function set adrDetailList(value:ListCollectionView):void {
            _adrDetailList = value;
        }
        public function get adrDetailList():ListCollectionView {
            return _adrDetailList;
        }

        public function set allergenDetailList(value:ListCollectionView):void {
            _allergenDetailList = value;
        }
        public function get allergenDetailList():ListCollectionView {
            return _allergenDetailList;
        }

        public function readExternal(input:IDataInput):void {
            _adrDetailList = input.readObject() as ListCollectionView;
            _allergenDetailList = input.readObject() as ListCollectionView;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_adrDetailList);
            output.writeObject(_allergenDetailList);
        }
    }
}