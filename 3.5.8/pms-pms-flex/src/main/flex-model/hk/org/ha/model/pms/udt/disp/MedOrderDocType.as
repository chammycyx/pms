/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.udt.disp {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.udt.disp.MedOrderDocType")]
    public class MedOrderDocType extends Enum {

        public static const Normal:MedOrderDocType = new MedOrderDocType("Normal", _, "N", "Normal");
        public static const Manual:MedOrderDocType = new MedOrderDocType("Manual", _, "M", "Manual");

        function MedOrderDocType(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || Normal.name), restrictor, (dataValue || Normal.dataValue), (displayValue || Normal.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [Normal, Manual];
        }

        public static function valueOf(name:String):MedOrderDocType {
            return MedOrderDocType(Normal.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):MedOrderDocType {
            return MedOrderDocType(Normal.dataConstantOf(dataValue));
        }
    }
}