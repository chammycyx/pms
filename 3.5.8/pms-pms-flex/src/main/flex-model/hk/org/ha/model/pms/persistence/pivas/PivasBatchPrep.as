/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * NOTE: this file is only generated if it does not exist. You may safely put
 * your custom code here.
 */

package hk.org.ha.model.pms.persistence.pivas {
	import hk.org.ha.model.pms.dms.persistence.Company;
	import hk.org.ha.model.pms.dms.persistence.DmDiluent;
	import hk.org.ha.model.pms.dms.persistence.DmDrug;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.persistence.pivas.PivasBatchPrep")]
    public class PivasBatchPrep extends PivasBatchPrepBase {
		
		public function get drugItemCode():String {
			return pivasBatchPrepItem.drugItemCode;
		}
		
		public function set drugItemCode(drugItemCode:String):void {
			pivasBatchPrepItem.drugItemCode = drugItemCode;
		}
		
		public function get drugBatchNum():String {
			return pivasBatchPrepItem.drugBatchNum;
		}
		
		public function set drugBatchNum(drugBatchNum:String):void {
			pivasBatchPrepItem.drugBatchNum = drugBatchNum;
		}
		
		public function get drugExpiryDate():Date {
			return pivasBatchPrepItem.drugExpiryDate;
		}
		
		public function set drugExpiryDate(drugExpiryDate:Date):void {
			pivasBatchPrepItem.drugExpiryDate = drugExpiryDate;
		}
		
		public function get drugManufCode():String {
			return pivasBatchPrepItem.drugManufCode;
		}
		
		public function set drugManufCode(drugManufCode:String):void {
			pivasBatchPrepItem.drugManufCode = drugManufCode;
		}
		
		public function get solventCode():String {
			return pivasBatchPrepItem.solventCode;
		}
		
		public function set solventCode(solventCode:String):void {
			pivasBatchPrepItem.solventCode = solventCode;
		}
		
		public function get solventDesc():String {
			return pivasBatchPrepItem.solventDesc;
		}
		
		public function set solventDesc(solventDesc:String):void {
			pivasBatchPrepItem.solventDesc = solventDesc;
		}
		
		public function get solventDmDrug():DmDrug {
			return pivasBatchPrepItem.solventDmDrug;
		}
		
		public function set solventDmDrug(solventDmDrug:DmDrug):void {
			pivasBatchPrepItem.solventDmDrug = solventDmDrug;
		}
		
		public function get solventItemCode():String {
			return pivasBatchPrepItem.solventItemCode;
		}
		
		public function set solventItemCode(solventItemCode:String):void {
			pivasBatchPrepItem.solventItemCode = solventItemCode;
		}
		
		public function get solventBatchNum():String {
			return pivasBatchPrepItem.solventBatchNum;
		}
		
		public function set solventBatchNum(solventBatchNum:String):void {
			pivasBatchPrepItem.solventBatchNum = solventBatchNum;
		}
		
		public function get solventManufCode():String {
			return pivasBatchPrepItem.solventManufCode;
		}
		
		public function set solventManufCode(solventManufCode:String):void {
			pivasBatchPrepItem.solventManufCode = solventManufCode;
		}
		
		public function get solventExpiryDate():Date {
			return pivasBatchPrepItem.solventExpiryDate;
		}
		
		public function set solventExpiryDate(solventExpiryDate:Date):void {
			pivasBatchPrepItem.solventExpiryDate = solventExpiryDate;
		}
		
		public function get solventCompany():Company {
			return pivasBatchPrepItem.solventCompany;
		}
		
		public function set solventCompany(solventCompany:Company):void {
			pivasBatchPrepItem.solventCompany = solventCompany;
		}
		
		public function get solventDmDiluent():DmDiluent {
			return pivasBatchPrepItem.solventDmDiluent;
		}
		
		public function set solventDmDiluent(solventDmDiluent:DmDiluent):void {
			pivasBatchPrepItem.solventDmDiluent = solventDmDiluent;
		}
    }
}