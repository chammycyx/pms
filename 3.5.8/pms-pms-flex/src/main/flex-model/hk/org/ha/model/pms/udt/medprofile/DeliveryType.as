/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.udt.medprofile {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.udt.medprofile.DeliveryType")]
    public class DeliveryType extends Enum {

        public static const Normal:DeliveryType = new DeliveryType("Normal", _, "N", "Normal");

        function DeliveryType(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || Normal.name), restrictor, (dataValue || Normal.dataValue), (displayValue || Normal.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [Normal];
        }

        public static function valueOf(name:String):DeliveryType {
            return DeliveryType(Normal.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):DeliveryType {
            return DeliveryType(Normal.dataConstantOf(dataValue));
        }
    }
}