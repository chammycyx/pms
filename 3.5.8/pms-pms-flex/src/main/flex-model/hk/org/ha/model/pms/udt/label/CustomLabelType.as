/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.udt.label {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.udt.label.CustomLabelType")]
    public class CustomLabelType extends Enum {

        public static const Special:CustomLabelType = new CustomLabelType("Special", _, "S", "Special");
        public static const HkIdBarCode:CustomLabelType = new CustomLabelType("HkIdBarCode", _, "H", "HKID BarCode");

        function CustomLabelType(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || Special.name), restrictor, (dataValue || Special.dataValue), (displayValue || Special.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [Special, HkIdBarCode];
        }

        public static function valueOf(name:String):CustomLabelType {
            return CustomLabelType(Special.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):CustomLabelType {
            return CustomLabelType(Special.dataConstantOf(dataValue));
        }
    }
}