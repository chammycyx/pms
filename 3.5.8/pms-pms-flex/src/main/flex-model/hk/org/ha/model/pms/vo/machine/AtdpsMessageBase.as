/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (AtdpsMessage.as).
 */

package hk.org.ha.model.pms.vo.machine {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;

    [Bindable]
    public class AtdpsMessageBase implements IExternalizable {

        private var _actionStatus:String;
        private var _adjQty:String;
        private var _baseunit:String;
        private var _batchCode:String;
        private var _bedNum:String;
        private var _binNum:String;
        private var _caseNum:String;
        private var _dispDate:String;
        private var _dosage:String;
        private var _durationUnit:String;
        private var _fdnFlag:Boolean;
        private var _fmFlag:Boolean;
        private var _formCode:String;
        private var _fractionDoseFlag:Boolean;
        private var _freqCode:String;
        private var _hkid:String;
        private var _hospCode:String;
        private var _hospName:String;
        private var _hostname:String;
        private var _instruction:String;
        private var _issueDuration:String;
        private var _issueQty:String;
        private var _itemCode:String;
        private var _itemDesc:String;
        private var _itemNum:String;
        private var _labelIndicator:String;
        private var _mealTimeInfo:String;
        private var _mpDispLabelBarcodeInfo:String;
        private var _multipleDoseFlag:Boolean;
        private var _pasPayCode:String;
        private var _patName:String;
        private var _pickStationNum:Number;
        private var _portNum:int;
        private var _printInstructFlag:Boolean;
        private var _printLang:String;
        private var _printOverrideIndicatorFlag:Boolean;
        private var _printWardReturnFlag:Boolean;
        private var _prnFlag:Boolean;
        private var _refillFlag:Boolean;
        private var _reprintFlag:Boolean;
        private var _specCode:String;
        private var _supplFreqCode:String;
        private var _tradeName:String;
        private var _trxId:Number;
        private var _updateDate:String;
        private var _urgentFlag:Boolean;
        private var _userCode:String;
        private var _verifyUser:String;
        private var _wardCode:String;
        private var _warnDesc1:String;
        private var _warnDesc2:String;
        private var _warnDesc3:String;
        private var _warnDesc4:String;
        private var _workstationCode:String;
        private var _workstoreCode:String;

        public function set actionStatus(value:String):void {
            _actionStatus = value;
        }
        public function get actionStatus():String {
            return _actionStatus;
        }

        public function set adjQty(value:String):void {
            _adjQty = value;
        }
        public function get adjQty():String {
            return _adjQty;
        }

        public function set baseunit(value:String):void {
            _baseunit = value;
        }
        public function get baseunit():String {
            return _baseunit;
        }

        public function set batchCode(value:String):void {
            _batchCode = value;
        }
        public function get batchCode():String {
            return _batchCode;
        }

        public function set bedNum(value:String):void {
            _bedNum = value;
        }
        public function get bedNum():String {
            return _bedNum;
        }

        public function set binNum(value:String):void {
            _binNum = value;
        }
        public function get binNum():String {
            return _binNum;
        }

        public function set caseNum(value:String):void {
            _caseNum = value;
        }
        public function get caseNum():String {
            return _caseNum;
        }

        public function set dispDate(value:String):void {
            _dispDate = value;
        }
        public function get dispDate():String {
            return _dispDate;
        }

        public function set dosage(value:String):void {
            _dosage = value;
        }
        public function get dosage():String {
            return _dosage;
        }

        public function set durationUnit(value:String):void {
            _durationUnit = value;
        }
        public function get durationUnit():String {
            return _durationUnit;
        }

        public function set fdnFlag(value:Boolean):void {
            _fdnFlag = value;
        }
        public function get fdnFlag():Boolean {
            return _fdnFlag;
        }

        public function set fmFlag(value:Boolean):void {
            _fmFlag = value;
        }
        public function get fmFlag():Boolean {
            return _fmFlag;
        }

        public function set formCode(value:String):void {
            _formCode = value;
        }
        public function get formCode():String {
            return _formCode;
        }

        public function set fractionDoseFlag(value:Boolean):void {
            _fractionDoseFlag = value;
        }
        public function get fractionDoseFlag():Boolean {
            return _fractionDoseFlag;
        }

        public function set freqCode(value:String):void {
            _freqCode = value;
        }
        public function get freqCode():String {
            return _freqCode;
        }

        public function set hkid(value:String):void {
            _hkid = value;
        }
        public function get hkid():String {
            return _hkid;
        }

        public function set hospCode(value:String):void {
            _hospCode = value;
        }
        public function get hospCode():String {
            return _hospCode;
        }

        public function set hospName(value:String):void {
            _hospName = value;
        }
        public function get hospName():String {
            return _hospName;
        }

        public function set hostname(value:String):void {
            _hostname = value;
        }
        public function get hostname():String {
            return _hostname;
        }

        public function set instruction(value:String):void {
            _instruction = value;
        }
        public function get instruction():String {
            return _instruction;
        }

        public function set issueDuration(value:String):void {
            _issueDuration = value;
        }
        public function get issueDuration():String {
            return _issueDuration;
        }

        public function set issueQty(value:String):void {
            _issueQty = value;
        }
        public function get issueQty():String {
            return _issueQty;
        }

        public function set itemCode(value:String):void {
            _itemCode = value;
        }
        public function get itemCode():String {
            return _itemCode;
        }

        public function set itemDesc(value:String):void {
            _itemDesc = value;
        }
        public function get itemDesc():String {
            return _itemDesc;
        }

        public function set itemNum(value:String):void {
            _itemNum = value;
        }
        public function get itemNum():String {
            return _itemNum;
        }

        public function set labelIndicator(value:String):void {
            _labelIndicator = value;
        }
        public function get labelIndicator():String {
            return _labelIndicator;
        }

        public function set mealTimeInfo(value:String):void {
            _mealTimeInfo = value;
        }
        public function get mealTimeInfo():String {
            return _mealTimeInfo;
        }

        public function set mpDispLabelBarcodeInfo(value:String):void {
            _mpDispLabelBarcodeInfo = value;
        }
        public function get mpDispLabelBarcodeInfo():String {
            return _mpDispLabelBarcodeInfo;
        }

        public function set multipleDoseFlag(value:Boolean):void {
            _multipleDoseFlag = value;
        }
        public function get multipleDoseFlag():Boolean {
            return _multipleDoseFlag;
        }

        public function set pasPayCode(value:String):void {
            _pasPayCode = value;
        }
        public function get pasPayCode():String {
            return _pasPayCode;
        }

        public function set patName(value:String):void {
            _patName = value;
        }
        public function get patName():String {
            return _patName;
        }

        public function set pickStationNum(value:Number):void {
            _pickStationNum = value;
        }
        public function get pickStationNum():Number {
            return _pickStationNum;
        }

        public function set portNum(value:int):void {
            _portNum = value;
        }
        public function get portNum():int {
            return _portNum;
        }

        public function set printInstructFlag(value:Boolean):void {
            _printInstructFlag = value;
        }
        public function get printInstructFlag():Boolean {
            return _printInstructFlag;
        }

        public function set printLang(value:String):void {
            _printLang = value;
        }
        public function get printLang():String {
            return _printLang;
        }

        public function set printOverrideIndicatorFlag(value:Boolean):void {
            _printOverrideIndicatorFlag = value;
        }
        public function get printOverrideIndicatorFlag():Boolean {
            return _printOverrideIndicatorFlag;
        }

        public function set printWardReturnFlag(value:Boolean):void {
            _printWardReturnFlag = value;
        }
        public function get printWardReturnFlag():Boolean {
            return _printWardReturnFlag;
        }

        public function set prnFlag(value:Boolean):void {
            _prnFlag = value;
        }
        public function get prnFlag():Boolean {
            return _prnFlag;
        }

        public function set refillFlag(value:Boolean):void {
            _refillFlag = value;
        }
        public function get refillFlag():Boolean {
            return _refillFlag;
        }

        public function set reprintFlag(value:Boolean):void {
            _reprintFlag = value;
        }
        public function get reprintFlag():Boolean {
            return _reprintFlag;
        }

        public function set specCode(value:String):void {
            _specCode = value;
        }
        public function get specCode():String {
            return _specCode;
        }

        public function set supplFreqCode(value:String):void {
            _supplFreqCode = value;
        }
        public function get supplFreqCode():String {
            return _supplFreqCode;
        }

        public function set tradeName(value:String):void {
            _tradeName = value;
        }
        public function get tradeName():String {
            return _tradeName;
        }

        public function set trxId(value:Number):void {
            _trxId = value;
        }
        public function get trxId():Number {
            return _trxId;
        }

        public function set updateDate(value:String):void {
            _updateDate = value;
        }
        public function get updateDate():String {
            return _updateDate;
        }

        public function set urgentFlag(value:Boolean):void {
            _urgentFlag = value;
        }
        public function get urgentFlag():Boolean {
            return _urgentFlag;
        }

        public function set userCode(value:String):void {
            _userCode = value;
        }
        public function get userCode():String {
            return _userCode;
        }

        public function set verifyUser(value:String):void {
            _verifyUser = value;
        }
        public function get verifyUser():String {
            return _verifyUser;
        }

        public function set wardCode(value:String):void {
            _wardCode = value;
        }
        public function get wardCode():String {
            return _wardCode;
        }

        public function set warnDesc1(value:String):void {
            _warnDesc1 = value;
        }
        public function get warnDesc1():String {
            return _warnDesc1;
        }

        public function set warnDesc2(value:String):void {
            _warnDesc2 = value;
        }
        public function get warnDesc2():String {
            return _warnDesc2;
        }

        public function set warnDesc3(value:String):void {
            _warnDesc3 = value;
        }
        public function get warnDesc3():String {
            return _warnDesc3;
        }

        public function set warnDesc4(value:String):void {
            _warnDesc4 = value;
        }
        public function get warnDesc4():String {
            return _warnDesc4;
        }

        public function set workstationCode(value:String):void {
            _workstationCode = value;
        }
        public function get workstationCode():String {
            return _workstationCode;
        }

        public function set workstoreCode(value:String):void {
            _workstoreCode = value;
        }
        public function get workstoreCode():String {
            return _workstoreCode;
        }

        public function readExternal(input:IDataInput):void {
            _actionStatus = input.readObject() as String;
            _adjQty = input.readObject() as String;
            _baseunit = input.readObject() as String;
            _batchCode = input.readObject() as String;
            _bedNum = input.readObject() as String;
            _binNum = input.readObject() as String;
            _caseNum = input.readObject() as String;
            _dispDate = input.readObject() as String;
            _dosage = input.readObject() as String;
            _durationUnit = input.readObject() as String;
            _fdnFlag = input.readObject() as Boolean;
            _fmFlag = input.readObject() as Boolean;
            _formCode = input.readObject() as String;
            _fractionDoseFlag = input.readObject() as Boolean;
            _freqCode = input.readObject() as String;
            _hkid = input.readObject() as String;
            _hospCode = input.readObject() as String;
            _hospName = input.readObject() as String;
            _hostname = input.readObject() as String;
            _instruction = input.readObject() as String;
            _issueDuration = input.readObject() as String;
            _issueQty = input.readObject() as String;
            _itemCode = input.readObject() as String;
            _itemDesc = input.readObject() as String;
            _itemNum = input.readObject() as String;
            _labelIndicator = input.readObject() as String;
            _mealTimeInfo = input.readObject() as String;
            _mpDispLabelBarcodeInfo = input.readObject() as String;
            _multipleDoseFlag = input.readObject() as Boolean;
            _pasPayCode = input.readObject() as String;
            _patName = input.readObject() as String;
            _pickStationNum = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            _portNum = input.readObject() as int;
            _printInstructFlag = input.readObject() as Boolean;
            _printLang = input.readObject() as String;
            _printOverrideIndicatorFlag = input.readObject() as Boolean;
            _printWardReturnFlag = input.readObject() as Boolean;
            _prnFlag = input.readObject() as Boolean;
            _refillFlag = input.readObject() as Boolean;
            _reprintFlag = input.readObject() as Boolean;
            _specCode = input.readObject() as String;
            _supplFreqCode = input.readObject() as String;
            _tradeName = input.readObject() as String;
            _trxId = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            _updateDate = input.readObject() as String;
            _urgentFlag = input.readObject() as Boolean;
            _userCode = input.readObject() as String;
            _verifyUser = input.readObject() as String;
            _wardCode = input.readObject() as String;
            _warnDesc1 = input.readObject() as String;
            _warnDesc2 = input.readObject() as String;
            _warnDesc3 = input.readObject() as String;
            _warnDesc4 = input.readObject() as String;
            _workstationCode = input.readObject() as String;
            _workstoreCode = input.readObject() as String;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_actionStatus);
            output.writeObject(_adjQty);
            output.writeObject(_baseunit);
            output.writeObject(_batchCode);
            output.writeObject(_bedNum);
            output.writeObject(_binNum);
            output.writeObject(_caseNum);
            output.writeObject(_dispDate);
            output.writeObject(_dosage);
            output.writeObject(_durationUnit);
            output.writeObject(_fdnFlag);
            output.writeObject(_fmFlag);
            output.writeObject(_formCode);
            output.writeObject(_fractionDoseFlag);
            output.writeObject(_freqCode);
            output.writeObject(_hkid);
            output.writeObject(_hospCode);
            output.writeObject(_hospName);
            output.writeObject(_hostname);
            output.writeObject(_instruction);
            output.writeObject(_issueDuration);
            output.writeObject(_issueQty);
            output.writeObject(_itemCode);
            output.writeObject(_itemDesc);
            output.writeObject(_itemNum);
            output.writeObject(_labelIndicator);
            output.writeObject(_mealTimeInfo);
            output.writeObject(_mpDispLabelBarcodeInfo);
            output.writeObject(_multipleDoseFlag);
            output.writeObject(_pasPayCode);
            output.writeObject(_patName);
            output.writeObject(_pickStationNum);
            output.writeObject(_portNum);
            output.writeObject(_printInstructFlag);
            output.writeObject(_printLang);
            output.writeObject(_printOverrideIndicatorFlag);
            output.writeObject(_printWardReturnFlag);
            output.writeObject(_prnFlag);
            output.writeObject(_refillFlag);
            output.writeObject(_reprintFlag);
            output.writeObject(_specCode);
            output.writeObject(_supplFreqCode);
            output.writeObject(_tradeName);
            output.writeObject(_trxId);
            output.writeObject(_updateDate);
            output.writeObject(_urgentFlag);
            output.writeObject(_userCode);
            output.writeObject(_verifyUser);
            output.writeObject(_wardCode);
            output.writeObject(_warnDesc1);
            output.writeObject(_warnDesc2);
            output.writeObject(_warnDesc3);
            output.writeObject(_warnDesc4);
            output.writeObject(_workstationCode);
            output.writeObject(_workstoreCode);
        }
    }
}