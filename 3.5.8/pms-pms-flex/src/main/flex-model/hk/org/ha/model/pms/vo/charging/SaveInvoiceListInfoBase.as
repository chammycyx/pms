/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (SaveInvoiceListInfo.as).
 */

package hk.org.ha.model.pms.vo.charging {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import mx.collections.ListCollectionView;

    [Bindable]
    public class SaveInvoiceListInfoBase implements IExternalizable {

        private var _alertMsg:String;
        private var _invoiceList:ListCollectionView;
        private var _suspendCode:String;
        private var _suspendOrder:Boolean;

        public function set alertMsg(value:String):void {
            _alertMsg = value;
        }
        public function get alertMsg():String {
            return _alertMsg;
        }

        public function set invoiceList(value:ListCollectionView):void {
            _invoiceList = value;
        }
        public function get invoiceList():ListCollectionView {
            return _invoiceList;
        }

        public function set suspendCode(value:String):void {
            _suspendCode = value;
        }
        public function get suspendCode():String {
            return _suspendCode;
        }

        public function set suspendOrder(value:Boolean):void {
            _suspendOrder = value;
        }
        public function get suspendOrder():Boolean {
            return _suspendOrder;
        }

        public function readExternal(input:IDataInput):void {
            _alertMsg = input.readObject() as String;
            _invoiceList = input.readObject() as ListCollectionView;
            _suspendCode = input.readObject() as String;
            _suspendOrder = input.readObject() as Boolean;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_alertMsg);
            output.writeObject(_invoiceList);
            output.writeObject(_suspendCode);
            output.writeObject(_suspendOrder);
        }
    }
}