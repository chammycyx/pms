package hk.org.ha.model.pms.vo.vetting {
	
	[Bindable]
	public class PharmOrderEditUiInfo {
		
		//Icon variables
		[Embed(source="/assets/add.png")]
		public static const addIcon:Class;
		
		[Embed(source="/assets/remove.png")]
		public static const removeIcon:Class;
		
		//Drug Item Line Grid column width : PMSU-2079, PMSU-2037
		public static const itemCodeColWidth:int = 65;		
		public static const drugNameColWidth:int = 350;		
		public static const formDescColWidth:int = 110;		
		public static const strengthColWidth:int = 99;		
		public static const volumeColWidth:int = 134;		
		public static const totalDoseColWidth:int = 235;
		
		//Regimen Grid column width		
		//dose line width (633)
		public static const dosageColWidth:int = 165;		
		public static const dailyFreqColWidth:int = 120;	
		public static const supplFreqColWidth:int = 138;
		public static const prnColWidth:int = 34;		
		public static const prnPercentWidth:int = 62;
		public static const dispColWidth:int = 38;
		public static const routeSiteColWidth:int = 85;
		public static const doseAddRemoveColWidth:int = 42;
		//extra dose col width (360)
		public static const durationColWidth:int = 186;		
		public static const qtyColWidth:int = 115;		
		public static const doseGroupAddRemoveColWidth:int = 59;				
		
		//Input component width												
		public static const dosageTxtInputWidth:int 		= 57;		
		public static const dosageUnitLblWidth:int 			= 118;		
		public static const mealBtnWidth:int 				= 23;		
		public static const prnChkboxWidth:int 				= 15;		
		public static const prnCbxWidth:int 				= 55;					
		public static const siteDropDownListWidth:int 		= 87;		
		public static const durationTxtInputWidth:int 		= 27;		
		public static const durationDropDownListWidth:int 	= 63;		
		public static const durationDateFieldWidth:int 		= 83; 		
		public static const durationToLblWidth:int 			= 15;		
		public static const qtyTxtInputWidth:int 			= 50;		
		public static const qtyDropDownListWidth:int 		= 69;		
		public static const addRemoveBtnWidth:int 			= 18;							
		public static const doseQtyTextInputWidth:int 		= 108;		
		public static const doseUnitLblWidth:int 			= 120;
		public static const maxFreqDropDownHeight:int		= 225;
		public static const maxSiteDropDownHeight:int		= 225;
		
		//Reserved width
		public static const scrollBarWidth:int = 19;		
		
		//Layout variable
		public static const paddingSpace:int=2;	
		public static const minPaddingSpace:int=1;
		public static const pharmLineHeight:int=260;
		public static const uiComponentHeight:int=23;
		
		public static function get doseLineWidth():Number
		{
			return dosageColWidth
			+ dailyFreqColWidth
				+ supplFreqColWidth						
				+ prnColWidth	
				+ dispColWidth
				+ routeSiteColWidth		
				+ doseAddRemoveColWidth;
		}
		
		public function PharmOrderEditUiInfo():void
		{
			super();			
		}		
		
		
	}
}