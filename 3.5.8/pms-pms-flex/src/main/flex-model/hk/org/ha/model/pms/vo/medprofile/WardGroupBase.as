/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (WardGroup.as).
 */

package hk.org.ha.model.pms.vo.medprofile {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import mx.collections.ListCollectionView;

    [Bindable]
    public class WardGroupBase implements IExternalizable {

        private var _selected:Boolean;
        private var _wardCodeList:ListCollectionView;
        private var _wardGroupCode:String;

        public function set selected(value:Boolean):void {
            _selected = value;
        }
        public function get selected():Boolean {
            return _selected;
        }

        public function set wardCodeList(value:ListCollectionView):void {
            _wardCodeList = value;
        }
        public function get wardCodeList():ListCollectionView {
            return _wardCodeList;
        }

        public function set wardGroupCode(value:String):void {
            _wardGroupCode = value;
        }
        public function get wardGroupCode():String {
            return _wardGroupCode;
        }

        public function readExternal(input:IDataInput):void {
            _selected = input.readObject() as Boolean;
            _wardCodeList = input.readObject() as ListCollectionView;
            _wardGroupCode = input.readObject() as String;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_selected);
            output.writeObject(_wardCodeList);
            output.writeObject(_wardGroupCode);
        }
    }
}