/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.udt.vetting {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.udt.vetting.ActionStatusWrapper")]
    public class ActionStatusWrapper extends ActionStatus {

        function ActionStatusWrapper(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || DispByPharm.name), restrictor, (dataValue || DispByPharm.dataValue), (displayValue || DispByPharm.displayValue));
        }

        public static function getConstantsByFmStatus(fmStatus:String, isSpecialDrug:Boolean=true):Array {			
			
			switch( fmStatus ) {
				case FmStatus.GeneralDrug.dataValue:
					return [DispByPharm, ContinueWithOwnStock, DispInClinic];					
				case FmStatus.SpecialDrug.dataValue:
					return [DispByPharm, PurchaseByPatient, ContinueWithOwnStock, DispInClinic];
				case FmStatus.SafetyNetItem.dataValue:
					if ( isSpecialDrug ) {
						return [DispByPharm, PurchaseByPatient, SafetyNet, ContinueWithOwnStock, DispInClinic];						
					} else {
						return [PurchaseByPatient, SafetyNet, ContinueWithOwnStock];
					}												
				case FmStatus.UnRegisteredDrug.dataValue:
				case FmStatus.SampleItem.dataValue:
					return [DispByPharm, PurchaseByPatient, ContinueWithOwnStock, DispInClinic];																
				case FmStatus.SelfFinancedItemD.dataValue:
				case FmStatus.SelfFinancedItemC.dataValue:
				case FmStatus.FreeTextEntryItem.dataValue:				
					return [PurchaseByPatient, ContinueWithOwnStock];									
				default :
					return [DispByPharm, PurchaseByPatient, SafetyNet, ContinueWithOwnStock, DispInClinic];
			}			            
        }     
		
		public static function getCars2NonIpmoeActionStatusList(fmStatus:String, isSpecialDrug:Boolean=true):Array {			
			
			switch( fmStatus ) {
				case FmStatus.GeneralDrug.dataValue:
					return [DispByPharm, ContinueWithOwnStock];					
				case FmStatus.SpecialDrug.dataValue:
					return [DispByPharm, PurchaseByPatient, ContinueWithOwnStock];
				case FmStatus.SafetyNetItem.dataValue:
					if ( isSpecialDrug ) {
						return [DispByPharm, PurchaseByPatient, SafetyNet, ContinueWithOwnStock];						
					} else {
						return [PurchaseByPatient, SafetyNet, ContinueWithOwnStock];
					}												
				case FmStatus.UnRegisteredDrug.dataValue:
				case FmStatus.SampleItem.dataValue:
					return [DispByPharm, PurchaseByPatient, ContinueWithOwnStock];																
				case FmStatus.SelfFinancedItemD.dataValue:
				case FmStatus.SelfFinancedItemC.dataValue:
					return [PurchaseByPatient, ContinueWithOwnStock];									
				case FmStatus.FreeTextEntryItem.dataValue:				
					return [DispByPharm, ContinueWithOwnStock];
				default :
					return [DispByPharm, PurchaseByPatient, SafetyNet, ContinueWithOwnStock];
			}			            
		}    
    }
}