/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (PreparationCheckServiceBean.as).
 */

package hk.org.ha.model.pms.biz.alert.mds {

    import flash.utils.flash_proxy;
    import hk.org.ha.model.pms.dms.vo.PreparationProperty;
    import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
    import org.granite.tide.BaseContext;
    import org.granite.tide.Component;
    import org.granite.tide.ITideResponder;
    
    use namespace flash_proxy;

    public class PreparationCheckServiceBeanBase extends Component {

        public function checkPreparation(arg0:MedOrderItem, arg1:PreparationProperty, arg2:Boolean, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("checkPreparation", arg0, arg1, arg2, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("checkPreparation", arg0, arg1, arg2, resultHandler);
            else if (resultHandler == null)
                callProperty("checkPreparation", arg0, arg1, arg2);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function destroy(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("destroy", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("destroy", resultHandler);
            else if (resultHandler == null)
                callProperty("destroy");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }
    }
}
