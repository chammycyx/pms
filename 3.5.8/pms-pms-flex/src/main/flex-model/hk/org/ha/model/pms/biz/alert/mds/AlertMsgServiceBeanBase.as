/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (AlertMsgServiceBean.as).
 */

package hk.org.ha.model.pms.biz.alert.mds {

    import flash.utils.flash_proxy;
    import hk.org.ha.model.pms.persistence.disp.MedItemInf;
    import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
    import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
    import org.granite.tide.BaseContext;
    import org.granite.tide.Component;
    import org.granite.tide.ITideResponder;
    
    use namespace flash_proxy;

    public class AlertMsgServiceBeanBase extends Component {

        public function retrieveAlertMsgByItemNum(arg0:Number, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrieveAlertMsgByItemNum", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrieveAlertMsgByItemNum", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("retrieveAlertMsgByItemNum", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function retrieveAlertMsgForDrugSearch(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrieveAlertMsgForDrugSearch", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrieveAlertMsgForDrugSearch", resultHandler);
            else if (resultHandler == null)
                callProperty("retrieveAlertMsgForDrugSearch");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function retrieveAlertMsgByMedOrderItem(arg0:MedOrderItem, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrieveAlertMsgByMedOrderItem", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrieveAlertMsgByMedOrderItem", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("retrieveAlertMsgByMedOrderItem", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function retrieveMedProfileAlertMsg(arg0:MedItemInf, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrieveMedProfileAlertMsg", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrieveMedProfileAlertMsg", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("retrieveMedProfileAlertMsg", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function retrieveMpAlertMsg(arg0:MedProfileMoItem, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrieveMpAlertMsg", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrieveMpAlertMsg", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("retrieveMpAlertMsg", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function destroy(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("destroy", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("destroy", resultHandler);
            else if (resultHandler == null)
                callProperty("destroy");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }
    }
}
