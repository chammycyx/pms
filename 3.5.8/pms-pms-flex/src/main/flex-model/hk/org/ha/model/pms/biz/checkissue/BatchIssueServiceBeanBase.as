/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (BatchIssueServiceBean.as).
 */

package hk.org.ha.model.pms.biz.checkissue {

    import flash.utils.flash_proxy;
    import hk.org.ha.model.pms.persistence.corp.Workstore;
    import hk.org.ha.model.pms.vo.checkissue.BatchIssueDetail;
    import org.granite.tide.BaseContext;
    import org.granite.tide.Component;
    import org.granite.tide.ITideResponder;
    
    use namespace flash_proxy;

    public class BatchIssueServiceBeanBase extends Component {

        public function updateBatchIssueLogWorkstation(arg0:Date, arg1:Workstore, arg2:BatchIssueDetail, arg3:String, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("updateBatchIssueLogWorkstation", arg0, arg1, arg2, arg3, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("updateBatchIssueLogWorkstation", arg0, arg1, arg2, arg3, resultHandler);
            else if (resultHandler == null)
                callProperty("updateBatchIssueLogWorkstation", arg0, arg1, arg2, arg3);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function retrieveBatchIssueDetail(arg0:Date, arg1:Workstore, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrieveBatchIssueDetail", arg0, arg1, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrieveBatchIssueDetail", arg0, arg1, resultHandler);
            else if (resultHandler == null)
                callProperty("retrieveBatchIssueDetail", arg0, arg1);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function deleteBatchIssueLog(arg0:Date, arg1:Workstore, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("deleteBatchIssueLog", arg0, arg1, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("deleteBatchIssueLog", arg0, arg1, resultHandler);
            else if (resultHandler == null)
                callProperty("deleteBatchIssueLog", arg0, arg1);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function updateBatchIssueLogDetail(arg0:BatchIssueDetail, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("updateBatchIssueLogDetail", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("updateBatchIssueLogDetail", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("updateBatchIssueLogDetail", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function isResult(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("isResult", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("isResult", resultHandler);
            else if (resultHandler == null)
                callProperty("isResult");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function getErrMsgCode(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("getErrMsgCode", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("getErrMsgCode", resultHandler);
            else if (resultHandler == null)
                callProperty("getErrMsgCode");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function isShowExceptionList(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("isShowExceptionList", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("isShowExceptionList", resultHandler);
            else if (resultHandler == null)
                callProperty("isShowExceptionList");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function getWorkstationCode(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("getWorkstationCode", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("getWorkstationCode", resultHandler);
            else if (resultHandler == null)
                callProperty("getWorkstationCode");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function retrieveBatchIssueSfiRpt(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrieveBatchIssueSfiRpt", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrieveBatchIssueSfiRpt", resultHandler);
            else if (resultHandler == null)
                callProperty("retrieveBatchIssueSfiRpt");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function retrieveBatchIssueExceptionRpt(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrieveBatchIssueExceptionRpt", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrieveBatchIssueExceptionRpt", resultHandler);
            else if (resultHandler == null)
                callProperty("retrieveBatchIssueExceptionRpt");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function retrieveBatchIssueRpt(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrieveBatchIssueRpt", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrieveBatchIssueRpt", resultHandler);
            else if (resultHandler == null)
                callProperty("retrieveBatchIssueRpt");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function printBatchIssueRpt(arg0:String, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("printBatchIssueRpt", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("printBatchIssueRpt", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("printBatchIssueRpt", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function destroy(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("destroy", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("destroy", resultHandler);
            else if (resultHandler == null)
                callProperty("destroy");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }
    }
}
