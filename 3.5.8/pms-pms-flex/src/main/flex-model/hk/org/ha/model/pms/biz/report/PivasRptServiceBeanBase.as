/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (PivasRptServiceBean.as).
 */

package hk.org.ha.model.pms.biz.report {

    import flash.utils.flash_proxy;
    import hk.org.ha.model.pms.udt.YesNoBlankFlag;
    import hk.org.ha.model.pms.udt.report.PivasReportType;
    import hk.org.ha.model.pms.udt.report.PivasRptDispMethod;
    import hk.org.ha.model.pms.udt.report.PivasRptItemCatFilterOption;
    import org.granite.tide.BaseContext;
    import org.granite.tide.Component;
    import org.granite.tide.ITideResponder;
    
    use namespace flash_proxy;

    public class PivasRptServiceBeanBase extends Component {

        public function retrievePivasRpt(arg0:PivasReportType, arg1:Date, arg2:Date, arg3:YesNoBlankFlag, arg4:String, arg5:PivasRptDispMethod, arg6:PivasRptItemCatFilterOption, arg7:String, arg8:Boolean, arg9:String, arg10:String, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrievePivasRpt", arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrievePivasRpt", arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, resultHandler);
            else if (resultHandler == null)
                callProperty("retrievePivasRpt", arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function generatePivasRpt(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("generatePivasRpt", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("generatePivasRpt", resultHandler);
            else if (resultHandler == null)
                callProperty("generatePivasRpt");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function printPivasRpt(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("printPivasRpt", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("printPivasRpt", resultHandler);
            else if (resultHandler == null)
                callProperty("printPivasRpt");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function setPivasRptPassword(arg0:String, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("setPivasRptPassword", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("setPivasRptPassword", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("setPivasRptPassword", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function exportPivasRpt(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("exportPivasRpt", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("exportPivasRpt", resultHandler);
            else if (resultHandler == null)
                callProperty("exportPivasRpt");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function isRetrieveSuccess(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("isRetrieveSuccess", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("isRetrieveSuccess", resultHandler);
            else if (resultHandler == null)
                callProperty("isRetrieveSuccess");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function destroy(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("destroy", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("destroy", resultHandler);
            else if (resultHandler == null)
                callProperty("destroy");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }
    }
}
