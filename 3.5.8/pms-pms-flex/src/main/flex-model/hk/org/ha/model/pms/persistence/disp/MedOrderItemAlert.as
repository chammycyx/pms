/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * NOTE: this file is only generated if it does not exist. You may safely put
 * your custom code here.
 */

package hk.org.ha.model.pms.persistence.disp {
	import hk.org.ha.model.pms.persistence.AlertEntity;
	import hk.org.ha.model.pms.udt.alert.mds.AlertType;
	import hk.org.ha.model.pms.vo.alert.mds.AlertAdr;
	import hk.org.ha.model.pms.vo.alert.mds.AlertAllergen;
	import hk.org.ha.model.pms.vo.alert.mds.AlertDdcm;
	import hk.org.ha.model.pms.vo.alert.mds.AlertDdim;
	import hk.org.ha.model.pms.vo.alert.mds.AlertHlaTest;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.persistence.disp.MedOrderItemAlert")]
    public class MedOrderItemAlert extends MedOrderItemAlertBase {
		
		public function MedOrderItemAlert(alertEntity:AlertEntity=null) {
			super();
			
			if (alertEntity != null) {
				this.alertType          = alertEntity.alertType;
				this.alertXml           = alertEntity.alertXml;
				this.overrideReason     = alertEntity.overrideReason;
				this.ackUser            = alertEntity.ackUser;
				this.ackDate            = alertEntity.ackDate;
				this.sortSeq            = alertEntity.sortSeq;
				this.alert              = alertEntity.alert;
			}
		}
		
		public override function get medItem():MedItemInf {
			return medOrderItem;
		}
		
		public override function set medItem(medItem:MedItemInf):void {
			if (medItem is MedOrderItem) {
				medOrderItem = medItem as MedOrderItem;
			}
			else {
				medOrderItem = null;
			}
		}
    }
}