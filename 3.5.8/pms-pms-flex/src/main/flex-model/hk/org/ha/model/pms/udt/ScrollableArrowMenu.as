package hk.org.ha.model.pms.udt {
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import mx.controls.Button;
	import mx.controls.Menu;
	import mx.controls.listClasses.IListItemRenderer;
	import mx.controls.menuClasses.IMenuItemRenderer;
	import mx.controls.scrollClasses.ScrollBar;
	import mx.core.FlexGlobals;
	import mx.core.ScrollPolicy;
	import mx.core.mx_internal;
	import mx.events.MenuEvent;
	import mx.events.ScrollEvent;
	import mx.managers.PopUpManager;
	
	use namespace mx_internal;
	
	[Bindable]
	public class ScrollableArrowMenu extends ScrollableMenu	{
		private var upButton:Button;
		private var downButton:Button;
		private var timer:Timer;
		public var scrollSpeed:Number = 100;
	   	public var scrollJump:Number = 1;
		public var numChild:Number;
	   	private var _arrowScrollPolicy:String = ScrollPolicy.AUTO;
	   	
	   	public function get arrowScrollPolicy():String {
	   		return _arrowScrollPolicy;
	   	}
	   	public function set arrowScrollPolicy(value:String):void {
	   		this._arrowScrollPolicy = value;
	   		
	   		invalidateDisplayList();
	   	}
		
		[Embed(source = "/assets/menu_assets.swf#up_arrow")]
		public var up_icon:Class;
		
		[Embed(source = "/assets/menu_assets.swf#down_arrow")]
		public var down_icon:Class;
		
		[Embed(source = "/assets/menu_assets.swf#up_arrow_disabled")]
		public var up_icon_disabled:Class;
		
		[Embed(source = "/assets/menu_assets.swf#down_arrow_disabled")]
		public var down_icon_disabled:Class;
		
		public static function createMenu(parent:DisplayObjectContainer, mdp:Object, showRoot:Boolean=true):ScrollableArrowMenu {	
	        var menu:ScrollableArrowMenu = new ScrollableArrowMenu();
	        menu.tabEnabled = false;
	        menu.owner = DisplayObjectContainer(FlexGlobals.topLevelApplication);
	        menu.showRoot = showRoot;
	        popUpMenu(menu, parent, mdp);
			
	        return menu;
	    }
	    
		public function ScrollableArrowMenu() {
			super();
		}
		
		override protected function createChildren():void {
			super.createChildren();
			this.addEventListener(MenuEvent.MENU_SHOW, setScrollPosition);
			this.addEventListener(MenuEvent.ITEM_CLICK, setScrollPosition);
					
			upButton = new Button();
			upButton.setStyle("cornerRadius", 0);
			upButton.setStyle("fillAlphas", [1,1,1,1]);
			
			downButton = new Button();
			downButton.setStyle("cornerRadius", 0);
			downButton.setStyle("fillAlphas", [1,1,1,1]);
			
			upButton.setStyle("icon", up_icon);
			downButton.setStyle("icon", down_icon);
			upButton.setStyle("disabledIcon", up_icon_disabled);
			downButton.setStyle("disabledIcon", down_icon_disabled);
			
			addChild(upButton);
			addChild(downButton);

			upButton.addEventListener(MouseEvent.ROLL_OVER, startScrollingUp);
			upButton.addEventListener(MouseEvent.ROLL_OUT, stopScrolling);	
			upButton.addEventListener(MouseEvent.CLICK, stopScrolling);

			downButton.addEventListener(MouseEvent.ROLL_OVER, startScrollingDown);
			downButton.addEventListener(MouseEvent.ROLL_OUT, stopScrolling);
			downButton.addEventListener(MouseEvent.CLICK, stopScrolling);

			this.addEventListener(ScrollEvent.SCROLL, checkButtons);
		}
		
		private function setScrollPosition(event:MenuEvent):void {
			this.verticalScrollPosition = 0;
		}
		
	    override mx_internal function openSubMenu(row:IListItemRenderer):void {
	        supposedToLoseFocus = true;
	
	        var r:Menu = getRootMenu();
	        var menu:ScrollableArrowMenu;

            menu = new ScrollableArrowMenu();
            menu.maxHeight = this.maxHeight;
            menu.verticalScrollPolicy = this.verticalScrollPolicy;
            menu.arrowScrollPolicy = this.arrowScrollPolicy;	
            menu.parentMenu = this;
            menu.owner = this;
            menu.showRoot = showRoot;
            menu.dataDescriptor = r.dataDescriptor;
            menu.styleName = r;
            menu.labelField = r.labelField;
            menu.labelFunction = r.labelFunction;
            menu.iconField = r.iconField;
            menu.iconFunction = r.iconFunction;
            menu.itemRenderer = r.itemRenderer;
            menu.rowHeight = r.rowHeight;
            menu.scaleY = r.scaleY;
            menu.scaleX = r.scaleX;

            if (row.data && _dataDescriptor.isBranch(row.data) && _dataDescriptor.hasChildren(row.data)) {
                menu.dataProvider = _dataDescriptor.getChildren(row.data);
            }
			
            menu.sourceMenuBar = sourceMenuBar;
            menu.sourceMenuBarItem = sourceMenuBarItem;
			menu.sourceMenuBarItem.visible = true;
            IMenuItemRenderer(row).menu = menu;			
			PopUpManager.addPopUp(menu, r, false);
	        super.openSubMenu(row);
	    }
		
		override protected  function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void {
			measure();
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			var w:Number = unscaledWidth;
			
			if(verticalScrollBar) {
				w = unscaledWidth - ScrollBar.THICKNESS;
			}
			
			upButton.setActualSize(w, 15);
			downButton.setActualSize(w, 15);		
			upButton.move(0, 0);
			downButton.move(0, measuredHeight - downButton.height);	
			checkButtons(null);
		}
		
		private function checkButtons(event:Event):void {
			if(this.arrowScrollPolicy == ScrollPolicy.AUTO) {
				upButton.visible = upButton.enabled = (this.verticalScrollPosition != 0);
				downButton.visible = downButton.enabled = (this.verticalScrollPosition != this.maxVerticalScrollPosition);
			}
			else if(this.arrowScrollPolicy == ScrollPolicy.ON) {
				upButton.visible = downButton.visible = true;
				upButton.enabled = (this.verticalScrollPosition != 0);
				downButton.enabled = (this.verticalScrollPosition != this.maxVerticalScrollPosition);
			}
			else {
				upButton.visible = upButton.enabled = downButton.visible = downButton.enabled = false;
			}
		}

		private function startScrollingUp(event:Event):void {
	    	if(timer && timer.running) {
				timer.stop();
			}		
			timer = new Timer(this.scrollSpeed);
			timer.addEventListener(TimerEvent.TIMER, scrollUp);
			timer.start();
	    }
	    
		private function startScrollingDown(event:Event):void {
	    	if(timer && timer.running) {
				timer.stop();
			}
			timer = new Timer(this.scrollSpeed);
			timer.addEventListener(TimerEvent.TIMER, scrollDown);
			timer.start();
	    }
	    
		private function stopScrolling(event:Event):void {
	    	if(timer && timer.running) {
				timer.stop();
			}
		}
	    
		private function scrollUp(event:TimerEvent):void {
	    	if(this.verticalScrollPosition - scrollJump > 0) {
	    		this.verticalScrollPosition -= scrollJump;
	    	}
	    	else {
	    		this.verticalScrollPosition = 0;
	    	}
	    	checkButtons(null);
	    }
	    
		private function scrollDown(event:TimerEvent):void {
	    	if(this.verticalScrollPosition + scrollJump < this.maxVerticalScrollPosition) {
	    		this.verticalScrollPosition += scrollJump;
	    	}
	    	else {
	    		this.verticalScrollPosition = this.maxVerticalScrollPosition;
	    	}
	    	checkButtons(null);
	    }
	}
}