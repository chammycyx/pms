/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (MedProfileItem.as).
 */

package hk.org.ha.model.pms.persistence.medprofile {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.getQualifiedClassName;
    import hk.org.ha.model.pms.persistence.VersionEntity;
    import hk.org.ha.model.pms.udt.medprofile.EndType;
    import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
    import mx.core.IUID;
    import mx.data.utils.Managed;
    import mx.utils.UIDUtil;
    import org.granite.collections.IPersistentCollection;
    import org.granite.meta;
    import org.granite.tide.IEntityManager;
    import org.granite.tide.IPropertyHolder;
    import org.granite.util.Enum;

    use namespace meta;

    [Managed]
    public class MedProfileItemBase extends VersionEntity {

        [Transient]
        meta var entityManager:IEntityManager = null;

        private var __initialized:Boolean = true;
        private var __detachedState:String = null;

        private var _adminDate:Date;
        private var _amendFlag:Boolean;
        private var _deliveryGenFlag:Boolean;
        private var _discontModified:Boolean;
        private var _endType:EndType;
        private var _groupNum:Number;
        private var _id:Number;
        private var _medProfile:MedProfile;
        private var _medProfileErrorLog:MedProfileErrorLog;
        private var _medProfileId:Number;
        private var _medProfileMoItem:MedProfileMoItem;
        private var _medProfileMoItemId:Number;
        private var _modified:Boolean;
        private var _newOrgItemNum:Number;
        private var _orgItemNum:Number;
        private var _orgStatus:MedProfileItemStatus;
        private var _replenishFlag:Boolean;
        private var _status:MedProfileItemStatus;
        private var _statusUpdateDate:Date;
        private var _verifyDate:Date;
        private var _verifyIndex:Number;
        private var _verifyUser:String;

        meta override function isInitialized(name:String = null):Boolean {
            if (!name)
                return __initialized;

            var property:* = this[name];
            return (
                (!(property is MedProfileItem) || (property as MedProfileItem).meta::isInitialized()) &&
                (!(property is IPersistentCollection) || (property as IPersistentCollection).isInitialized())
            );
        }
        
        [Bindable(event="dirtyChange")]
		public function get meta_dirty():Boolean {
			return Managed.getProperty(this, "meta_dirty", false);
		}

        public function set adminDate(value:Date):void {
            _adminDate = value;
        }
        public function get adminDate():Date {
            return _adminDate;
        }

        public function set amendFlag(value:Boolean):void {
            _amendFlag = value;
        }
        public function get amendFlag():Boolean {
            return _amendFlag;
        }

        public function set deliveryGenFlag(value:Boolean):void {
            _deliveryGenFlag = value;
        }
        public function get deliveryGenFlag():Boolean {
            return _deliveryGenFlag;
        }

        public function set discontModified(value:Boolean):void {
            _discontModified = value;
        }
        public function get discontModified():Boolean {
            return _discontModified;
        }

        public function set endType(value:EndType):void {
            _endType = value;
        }
        public function get endType():EndType {
            return _endType;
        }

        public function set groupNum(value:Number):void {
            _groupNum = value;
        }
        public function get groupNum():Number {
            return _groupNum;
        }

        public function set id(value:Number):void {
            _id = value;
        }
        [Id]
        public function get id():Number {
            return _id;
        }

        public function set medProfile(value:MedProfile):void {
            _medProfile = value;
        }
        public function get medProfile():MedProfile {
            return _medProfile;
        }

        public function set medProfileErrorLog(value:MedProfileErrorLog):void {
            _medProfileErrorLog = value;
        }
        public function get medProfileErrorLog():MedProfileErrorLog {
            return _medProfileErrorLog;
        }

        public function set medProfileId(value:Number):void {
            _medProfileId = value;
        }
        public function get medProfileId():Number {
            return _medProfileId;
        }

        public function set medProfileMoItem(value:MedProfileMoItem):void {
            _medProfileMoItem = value;
        }
        public function get medProfileMoItem():MedProfileMoItem {
            return _medProfileMoItem;
        }

        public function set medProfileMoItemId(value:Number):void {
            _medProfileMoItemId = value;
        }
        public function get medProfileMoItemId():Number {
            return _medProfileMoItemId;
        }

        public function set modified(value:Boolean):void {
            _modified = value;
        }
        public function get modified():Boolean {
            return _modified;
        }

        public function set newOrgItemNum(value:Number):void {
            _newOrgItemNum = value;
        }
        public function get newOrgItemNum():Number {
            return _newOrgItemNum;
        }

        public function set orgItemNum(value:Number):void {
            _orgItemNum = value;
        }
        public function get orgItemNum():Number {
            return _orgItemNum;
        }

        public function set orgStatus(value:MedProfileItemStatus):void {
            _orgStatus = value;
        }
        public function get orgStatus():MedProfileItemStatus {
            return _orgStatus;
        }

        public function set replenishFlag(value:Boolean):void {
            _replenishFlag = value;
        }
        public function get replenishFlag():Boolean {
            return _replenishFlag;
        }

        public function set status(value:MedProfileItemStatus):void {
            _status = value;
        }
        public function get status():MedProfileItemStatus {
            return _status;
        }

        public function set statusUpdateDate(value:Date):void {
            _statusUpdateDate = value;
        }
        public function get statusUpdateDate():Date {
            return _statusUpdateDate;
        }

        public function set verifyDate(value:Date):void {
            _verifyDate = value;
        }
        public function get verifyDate():Date {
            return _verifyDate;
        }

        public function set verifyIndex(value:Number):void {
            _verifyIndex = value;
        }
        public function get verifyIndex():Number {
            return _verifyIndex;
        }

        public function set verifyUser(value:String):void {
            _verifyUser = value;
        }
        public function get verifyUser():String {
            return _verifyUser;
        }

        public function set uid(value:String):void {
            // noop...
        }
        public function get uid():String {
            if (isNaN(_id))
                return UIDUtil.createUID();
            return getQualifiedClassName(this) + "#[" + String(_id) + "]";
        }

        meta override function merge(em:IEntityManager, obj:*):void {
            var src:MedProfileItemBase = MedProfileItemBase(obj);
            __initialized = src.__initialized;
            __detachedState = src.__detachedState;
            if (meta::isInitialized()) {
                super.meta::merge(em, obj);
               em.meta_mergeExternal(src._adminDate, _adminDate, null, this, 'adminDate', function setter(o:*):void{_adminDate = o as Date}, false);
               em.meta_mergeExternal(src._amendFlag, _amendFlag, null, this, 'amendFlag', function setter(o:*):void{_amendFlag = o as Boolean}, false);
               em.meta_mergeExternal(src._deliveryGenFlag, _deliveryGenFlag, null, this, 'deliveryGenFlag', function setter(o:*):void{_deliveryGenFlag = o as Boolean}, false);
               em.meta_mergeExternal(src._discontModified, _discontModified, null, this, 'discontModified', function setter(o:*):void{_discontModified = o as Boolean}, false);
               em.meta_mergeExternal(src._endType, _endType, null, this, 'endType', function setter(o:*):void{_endType = o as EndType}, false);
               em.meta_mergeExternal(src._groupNum, _groupNum, null, this, 'groupNum', function setter(o:*):void{_groupNum = o as Number}, false);
               em.meta_mergeExternal(src._id, _id, null, this, 'id', function setter(o:*):void{_id = o as Number}, false);
               em.meta_mergeExternal(src._medProfile, _medProfile, null, this, 'medProfile', function setter(o:*):void{_medProfile = o as MedProfile}, false);
               em.meta_mergeExternal(src._medProfileErrorLog, _medProfileErrorLog, null, this, 'medProfileErrorLog', function setter(o:*):void{_medProfileErrorLog = o as MedProfileErrorLog}, false);
               em.meta_mergeExternal(src._medProfileId, _medProfileId, null, this, 'medProfileId', function setter(o:*):void{_medProfileId = o as Number}, false);
               em.meta_mergeExternal(src._medProfileMoItem, _medProfileMoItem, null, this, 'medProfileMoItem', function setter(o:*):void{_medProfileMoItem = o as MedProfileMoItem}, false);
               em.meta_mergeExternal(src._medProfileMoItemId, _medProfileMoItemId, null, this, 'medProfileMoItemId', function setter(o:*):void{_medProfileMoItemId = o as Number}, false);
               em.meta_mergeExternal(src._modified, _modified, null, this, 'modified', function setter(o:*):void{_modified = o as Boolean}, false);
               em.meta_mergeExternal(src._newOrgItemNum, _newOrgItemNum, null, this, 'newOrgItemNum', function setter(o:*):void{_newOrgItemNum = o as Number}, false);
               em.meta_mergeExternal(src._orgItemNum, _orgItemNum, null, this, 'orgItemNum', function setter(o:*):void{_orgItemNum = o as Number}, false);
               em.meta_mergeExternal(src._orgStatus, _orgStatus, null, this, 'orgStatus', function setter(o:*):void{_orgStatus = o as MedProfileItemStatus}, false);
               em.meta_mergeExternal(src._replenishFlag, _replenishFlag, null, this, 'replenishFlag', function setter(o:*):void{_replenishFlag = o as Boolean}, false);
               em.meta_mergeExternal(src._status, _status, null, this, 'status', function setter(o:*):void{_status = o as MedProfileItemStatus}, false);
               em.meta_mergeExternal(src._statusUpdateDate, _statusUpdateDate, null, this, 'statusUpdateDate', function setter(o:*):void{_statusUpdateDate = o as Date}, false);
               em.meta_mergeExternal(src._verifyDate, _verifyDate, null, this, 'verifyDate', function setter(o:*):void{_verifyDate = o as Date}, false);
               em.meta_mergeExternal(src._verifyIndex, _verifyIndex, null, this, 'verifyIndex', function setter(o:*):void{_verifyIndex = o as Number}, false);
               em.meta_mergeExternal(src._verifyUser, _verifyUser, null, this, 'verifyUser', function setter(o:*):void{_verifyUser = o as String}, false);
            }
            else {
               em.meta_mergeExternal(src._id, _id, null, this, 'id', function setter(o:*):void{_id = o as Number});
            }
        }

        public override function readExternal(input:IDataInput):void {
            __initialized = input.readObject() as Boolean;
            __detachedState = input.readObject() as String;
            if (meta::isInitialized()) {
                super.readExternal(input);
                _adminDate = input.readObject() as Date;
                _amendFlag = input.readObject() as Boolean;
                _deliveryGenFlag = input.readObject() as Boolean;
                _discontModified = input.readObject() as Boolean;
                _endType = Enum.readEnum(input) as EndType;
                _groupNum = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _id = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _medProfile = input.readObject() as MedProfile;
                _medProfileErrorLog = input.readObject() as MedProfileErrorLog;
                _medProfileId = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _medProfileMoItem = input.readObject() as MedProfileMoItem;
                _medProfileMoItemId = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _modified = input.readObject() as Boolean;
                _newOrgItemNum = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _orgItemNum = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _orgStatus = Enum.readEnum(input) as MedProfileItemStatus;
                _replenishFlag = input.readObject() as Boolean;
                _status = Enum.readEnum(input) as MedProfileItemStatus;
                _statusUpdateDate = input.readObject() as Date;
                _verifyDate = input.readObject() as Date;
                _verifyIndex = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _verifyUser = input.readObject() as String;
            }
            else {
                _id = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            }
        }

        public override function writeExternal(output:IDataOutput):void {
            output.writeObject(__initialized);
            output.writeObject(__detachedState);
            if (meta::isInitialized()) {
                super.writeExternal(output);
                output.writeObject((_adminDate is IPropertyHolder) ? IPropertyHolder(_adminDate).object : _adminDate);
                output.writeObject((_amendFlag is IPropertyHolder) ? IPropertyHolder(_amendFlag).object : _amendFlag);
                output.writeObject((_deliveryGenFlag is IPropertyHolder) ? IPropertyHolder(_deliveryGenFlag).object : _deliveryGenFlag);
                output.writeObject((_discontModified is IPropertyHolder) ? IPropertyHolder(_discontModified).object : _discontModified);
                output.writeObject((_endType is IPropertyHolder) ? IPropertyHolder(_endType).object : _endType);
                output.writeObject((_groupNum is IPropertyHolder) ? IPropertyHolder(_groupNum).object : _groupNum);
                output.writeObject((_id is IPropertyHolder) ? IPropertyHolder(_id).object : _id);
                output.writeObject((_medProfile is IPropertyHolder) ? IPropertyHolder(_medProfile).object : _medProfile);
                output.writeObject((_medProfileErrorLog is IPropertyHolder) ? IPropertyHolder(_medProfileErrorLog).object : _medProfileErrorLog);
                output.writeObject((_medProfileId is IPropertyHolder) ? IPropertyHolder(_medProfileId).object : _medProfileId);
                output.writeObject((_medProfileMoItem is IPropertyHolder) ? IPropertyHolder(_medProfileMoItem).object : _medProfileMoItem);
                output.writeObject((_medProfileMoItemId is IPropertyHolder) ? IPropertyHolder(_medProfileMoItemId).object : _medProfileMoItemId);
                output.writeObject((_modified is IPropertyHolder) ? IPropertyHolder(_modified).object : _modified);
                output.writeObject((_newOrgItemNum is IPropertyHolder) ? IPropertyHolder(_newOrgItemNum).object : _newOrgItemNum);
                output.writeObject((_orgItemNum is IPropertyHolder) ? IPropertyHolder(_orgItemNum).object : _orgItemNum);
                output.writeObject((_orgStatus is IPropertyHolder) ? IPropertyHolder(_orgStatus).object : _orgStatus);
                output.writeObject((_replenishFlag is IPropertyHolder) ? IPropertyHolder(_replenishFlag).object : _replenishFlag);
                output.writeObject((_status is IPropertyHolder) ? IPropertyHolder(_status).object : _status);
                output.writeObject((_statusUpdateDate is IPropertyHolder) ? IPropertyHolder(_statusUpdateDate).object : _statusUpdateDate);
                output.writeObject((_verifyDate is IPropertyHolder) ? IPropertyHolder(_verifyDate).object : _verifyDate);
                output.writeObject((_verifyIndex is IPropertyHolder) ? IPropertyHolder(_verifyIndex).object : _verifyIndex);
                output.writeObject((_verifyUser is IPropertyHolder) ? IPropertyHolder(_verifyUser).object : _verifyUser);
            }
            else {
                output.writeObject(_id);
            }
        }
    }
}
