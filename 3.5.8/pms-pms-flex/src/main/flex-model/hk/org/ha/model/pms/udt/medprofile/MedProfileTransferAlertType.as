/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.udt.medprofile {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.udt.medprofile.MedProfileTransferAlertType")]
    public class MedProfileTransferAlertType extends Enum {

        public static const None:MedProfileTransferAlertType = new MedProfileTransferAlertType("None", _, "-", "None");
        public static const Review:MedProfileTransferAlertType = new MedProfileTransferAlertType("Review", _, "R", "Review");
        public static const Notify:MedProfileTransferAlertType = new MedProfileTransferAlertType("Notify", _, "N", "Notify");

        function MedProfileTransferAlertType(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || None.name), restrictor, (dataValue || None.dataValue), (displayValue || None.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [None, Review, Notify];
        }

        public static function valueOf(name:String):MedProfileTransferAlertType {
            return MedProfileTransferAlertType(None.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):MedProfileTransferAlertType {
            return MedProfileTransferAlertType(None.dataConstantOf(dataValue));
        }
    }
}