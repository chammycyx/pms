/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (ChargeSpecialtyListServiceBean.as).
 */

package hk.org.ha.model.pms.biz.reftable {

    import flash.utils.flash_proxy;
    import mx.collections.ListCollectionView;
    import org.granite.tide.BaseContext;
    import org.granite.tide.Component;
    import org.granite.tide.ITideResponder;
    
    use namespace flash_proxy;

    public class ChargeSpecialtyListServiceBeanBase extends Component {

        public function retrieveChargeSpecialtyList(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrieveChargeSpecialtyList", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrieveChargeSpecialtyList", resultHandler);
            else if (resultHandler == null)
                callProperty("retrieveChargeSpecialtyList");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function saveChargeSpecialtyList(arg0:ListCollectionView, arg1:ListCollectionView, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("saveChargeSpecialtyList", arg0, arg1, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("saveChargeSpecialtyList", arg0, arg1, resultHandler);
            else if (resultHandler == null)
                callProperty("saveChargeSpecialtyList", arg0, arg1);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function printSpecMapSfiRpt(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("printSpecMapSfiRpt", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("printSpecMapSfiRpt", resultHandler);
            else if (resultHandler == null)
                callProperty("printSpecMapSfiRpt");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function printSpecMapSnRpt(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("printSpecMapSnRpt", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("printSpecMapSnRpt", resultHandler);
            else if (resultHandler == null)
                callProperty("printSpecMapSnRpt");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function destroy(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("destroy", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("destroy", resultHandler);
            else if (resultHandler == null)
                callProperty("destroy");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }
    }
}
