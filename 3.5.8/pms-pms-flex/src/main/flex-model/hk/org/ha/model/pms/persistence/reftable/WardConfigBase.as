/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (WardConfig.as).
 */

package hk.org.ha.model.pms.persistence.reftable {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.getQualifiedClassName;
    import hk.org.ha.model.pms.persistence.VersionEntity;
    import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
    import hk.org.ha.model.pms.udt.printing.PrintLang;
    import mx.core.IUID;
    import mx.data.utils.Managed;
    import mx.utils.UIDUtil;
    import org.granite.collections.IPersistentCollection;
    import org.granite.meta;
    import org.granite.tide.IEntityManager;
    import org.granite.tide.IPropertyHolder;
    import org.granite.util.Enum;

    use namespace meta;

    [Managed]
    public class WardConfigBase extends VersionEntity {

        [Transient]
        meta var entityManager:IEntityManager = null;

        private var __initialized:Boolean = true;
        private var __detachedState:String = null;

        private var _atdpsWardFlag:Boolean;
        private var _dosageInstructionPrintLang:PrintLang;
        private var _enableAtdpsWard:Boolean;
        private var _id:Number;
        private var _mpWardFlag:Boolean;
        private var _printDosageInstructionFlag:Boolean;
        private var _wardCode:String;
        private var _workstoreGroup:WorkstoreGroup;

        meta override function isInitialized(name:String = null):Boolean {
            if (!name)
                return __initialized;

            var property:* = this[name];
            return (
                (!(property is WardConfig) || (property as WardConfig).meta::isInitialized()) &&
                (!(property is IPersistentCollection) || (property as IPersistentCollection).isInitialized())
            );
        }
        
        [Bindable(event="dirtyChange")]
		public function get meta_dirty():Boolean {
			return Managed.getProperty(this, "meta_dirty", false);
		}

        public function set atdpsWardFlag(value:Boolean):void {
            _atdpsWardFlag = value;
        }
        public function get atdpsWardFlag():Boolean {
            return _atdpsWardFlag;
        }

        public function set dosageInstructionPrintLang(value:PrintLang):void {
            _dosageInstructionPrintLang = value;
        }
        public function get dosageInstructionPrintLang():PrintLang {
            return _dosageInstructionPrintLang;
        }

        public function set enableAtdpsWard(value:Boolean):void {
            _enableAtdpsWard = value;
        }
        public function get enableAtdpsWard():Boolean {
            return _enableAtdpsWard;
        }

        public function set id(value:Number):void {
            _id = value;
        }
        [Id]
        public function get id():Number {
            return _id;
        }

        public function set mpWardFlag(value:Boolean):void {
            _mpWardFlag = value;
        }
        public function get mpWardFlag():Boolean {
            return _mpWardFlag;
        }

        public function set printDosageInstructionFlag(value:Boolean):void {
            _printDosageInstructionFlag = value;
        }
        public function get printDosageInstructionFlag():Boolean {
            return _printDosageInstructionFlag;
        }

        public function set wardCode(value:String):void {
            _wardCode = value;
        }
        public function get wardCode():String {
            return _wardCode;
        }

        public function set workstoreGroup(value:WorkstoreGroup):void {
            _workstoreGroup = value;
        }
        public function get workstoreGroup():WorkstoreGroup {
            return _workstoreGroup;
        }

        public function set uid(value:String):void {
            // noop...
        }
        public function get uid():String {
            if (isNaN(_id))
                return UIDUtil.createUID();
            return getQualifiedClassName(this) + "#[" + String(_id) + "]";
        }

        meta override function merge(em:IEntityManager, obj:*):void {
            var src:WardConfigBase = WardConfigBase(obj);
            __initialized = src.__initialized;
            __detachedState = src.__detachedState;
            if (meta::isInitialized()) {
                super.meta::merge(em, obj);
               em.meta_mergeExternal(src._atdpsWardFlag, _atdpsWardFlag, null, this, 'atdpsWardFlag', function setter(o:*):void{_atdpsWardFlag = o as Boolean}, false);
               em.meta_mergeExternal(src._dosageInstructionPrintLang, _dosageInstructionPrintLang, null, this, 'dosageInstructionPrintLang', function setter(o:*):void{_dosageInstructionPrintLang = o as PrintLang}, false);
               em.meta_mergeExternal(src._enableAtdpsWard, _enableAtdpsWard, null, this, 'enableAtdpsWard', function setter(o:*):void{_enableAtdpsWard = o as Boolean}, false);
               em.meta_mergeExternal(src._id, _id, null, this, 'id', function setter(o:*):void{_id = o as Number}, false);
               em.meta_mergeExternal(src._mpWardFlag, _mpWardFlag, null, this, 'mpWardFlag', function setter(o:*):void{_mpWardFlag = o as Boolean}, false);
               em.meta_mergeExternal(src._printDosageInstructionFlag, _printDosageInstructionFlag, null, this, 'printDosageInstructionFlag', function setter(o:*):void{_printDosageInstructionFlag = o as Boolean}, false);
               em.meta_mergeExternal(src._wardCode, _wardCode, null, this, 'wardCode', function setter(o:*):void{_wardCode = o as String}, false);
               em.meta_mergeExternal(src._workstoreGroup, _workstoreGroup, null, this, 'workstoreGroup', function setter(o:*):void{_workstoreGroup = o as WorkstoreGroup}, false);
            }
            else {
               em.meta_mergeExternal(src._id, _id, null, this, 'id', function setter(o:*):void{_id = o as Number});
            }
        }

        public override function readExternal(input:IDataInput):void {
            __initialized = input.readObject() as Boolean;
            __detachedState = input.readObject() as String;
            if (meta::isInitialized()) {
                super.readExternal(input);
                _atdpsWardFlag = input.readObject() as Boolean;
                _dosageInstructionPrintLang = Enum.readEnum(input) as PrintLang;
                _enableAtdpsWard = input.readObject() as Boolean;
                _id = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _mpWardFlag = input.readObject() as Boolean;
                _printDosageInstructionFlag = input.readObject() as Boolean;
                _wardCode = input.readObject() as String;
                _workstoreGroup = input.readObject() as WorkstoreGroup;
            }
            else {
                _id = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            }
        }

        public override function writeExternal(output:IDataOutput):void {
            output.writeObject(__initialized);
            output.writeObject(__detachedState);
            if (meta::isInitialized()) {
                super.writeExternal(output);
                output.writeObject((_atdpsWardFlag is IPropertyHolder) ? IPropertyHolder(_atdpsWardFlag).object : _atdpsWardFlag);
                output.writeObject((_dosageInstructionPrintLang is IPropertyHolder) ? IPropertyHolder(_dosageInstructionPrintLang).object : _dosageInstructionPrintLang);
                output.writeObject((_enableAtdpsWard is IPropertyHolder) ? IPropertyHolder(_enableAtdpsWard).object : _enableAtdpsWard);
                output.writeObject((_id is IPropertyHolder) ? IPropertyHolder(_id).object : _id);
                output.writeObject((_mpWardFlag is IPropertyHolder) ? IPropertyHolder(_mpWardFlag).object : _mpWardFlag);
                output.writeObject((_printDosageInstructionFlag is IPropertyHolder) ? IPropertyHolder(_printDosageInstructionFlag).object : _printDosageInstructionFlag);
                output.writeObject((_wardCode is IPropertyHolder) ? IPropertyHolder(_wardCode).object : _wardCode);
                output.writeObject((_workstoreGroup is IPropertyHolder) ? IPropertyHolder(_workstoreGroup).object : _workstoreGroup);
            }
            else {
                output.writeObject(_id);
            }
        }
    }
}
