/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (WorkstoreDrugServiceBean.as).
 */

package hk.org.ha.model.pms.biz.drug {

    import flash.utils.flash_proxy;
    import hk.org.ha.model.pms.vo.rx.DhRxDrug;
    import hk.org.ha.model.pms.vo.rx.RxItem;
    import org.granite.tide.BaseContext;
    import org.granite.tide.Component;
    import org.granite.tide.ITideResponder;
    
    use namespace flash_proxy;

    public class WorkstoreDrugServiceBeanBase extends Component {

        public function retrieveWorkstoreDrug(arg0:String, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrieveWorkstoreDrug", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrieveWorkstoreDrug", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("retrieveWorkstoreDrug", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function retrieveWorkstoreDrugForOrderEdit(arg0:String, arg1:String, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrieveWorkstoreDrugForOrderEdit", arg0, arg1, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrieveWorkstoreDrugForOrderEdit", arg0, arg1, resultHandler);
            else if (resultHandler == null)
                callProperty("retrieveWorkstoreDrugForOrderEdit", arg0, arg1);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function retrieveWorkstoreDrugForMpOrderEdit(arg0:RxItem, arg1:String, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrieveWorkstoreDrugForMpOrderEdit", arg0, arg1, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrieveWorkstoreDrugForMpOrderEdit", arg0, arg1, resultHandler);
            else if (resultHandler == null)
                callProperty("retrieveWorkstoreDrugForMpOrderEdit", arg0, arg1);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function retrieveWorkstoreDrugForDhOrderEdit(arg0:DhRxDrug, arg1:String, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrieveWorkstoreDrugForDhOrderEdit", arg0, arg1, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrieveWorkstoreDrugForDhOrderEdit", arg0, arg1, resultHandler);
            else if (resultHandler == null)
                callProperty("retrieveWorkstoreDrugForDhOrderEdit", arg0, arg1);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function destroy(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("destroy", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("destroy", resultHandler);
            else if (resultHandler == null)
                callProperty("destroy");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }
    }
}
