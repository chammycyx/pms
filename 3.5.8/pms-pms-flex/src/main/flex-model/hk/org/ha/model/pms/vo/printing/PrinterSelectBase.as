/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (PrinterSelect.as).
 */

package hk.org.ha.model.pms.vo.printing {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import hk.org.ha.model.pms.persistence.reftable.Workstation;
    import hk.org.ha.model.pms.udt.reftable.PrintDocType;
    import org.granite.util.Enum;

    [Bindable]
    public class PrinterSelectBase implements IExternalizable {

        private var _copies:int;
        private var _defaultPrinterWorkstation:Workstation;
        private var _printDocType:PrintDocType;
        private var _printerWorkstation:Workstation;
        private var _reverse:Boolean;

        public function set copies(value:int):void {
            _copies = value;
        }
        public function get copies():int {
            return _copies;
        }

        public function set defaultPrinterWorkstation(value:Workstation):void {
            _defaultPrinterWorkstation = value;
        }
        public function get defaultPrinterWorkstation():Workstation {
            return _defaultPrinterWorkstation;
        }

        public function set printDocType(value:PrintDocType):void {
            _printDocType = value;
        }
        public function get printDocType():PrintDocType {
            return _printDocType;
        }

        public function set printerWorkstation(value:Workstation):void {
            _printerWorkstation = value;
        }
        public function get printerWorkstation():Workstation {
            return _printerWorkstation;
        }

        public function set reverse(value:Boolean):void {
            _reverse = value;
        }
        public function get reverse():Boolean {
            return _reverse;
        }

        public function readExternal(input:IDataInput):void {
            _copies = input.readObject() as int;
            _defaultPrinterWorkstation = input.readObject() as Workstation;
            _printDocType = Enum.readEnum(input) as PrintDocType;
            _printerWorkstation = input.readObject() as Workstation;
            _reverse = input.readObject() as Boolean;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_copies);
            output.writeObject(_defaultPrinterWorkstation);
            output.writeObject(_printDocType);
            output.writeObject(_printerWorkstation);
            output.writeObject(_reverse);
        }
    }
}