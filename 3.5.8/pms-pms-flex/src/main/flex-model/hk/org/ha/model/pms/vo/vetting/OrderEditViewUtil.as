package hk.org.ha.model.pms.vo.vetting {
	
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	import flash.utils.Dictionary;
	
	import flexlib.scheduling.util.DateUtil;
	
	import hk.org.ha.event.pms.main.drug.RetrieveIpOthersSiteMapListByDrugRouteCriteriaEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveWorkstoreDrugListByRxItemEvent;
	import hk.org.ha.event.pms.main.vetting.mp.popup.ShowMpOrderEditItemPopupEvent;
	import hk.org.ha.event.pms.main.vetting.popup.ShowDailyFreqPopupEvent;
	import hk.org.ha.event.pms.main.vetting.popup.ShowRouteSitePopupEvent;
	import hk.org.ha.event.pms.main.vetting.popup.ShowSupplFreqPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.core.ExtendedAbbDateField;
	import hk.org.ha.fmk.pms.flex.components.core.ExtendedComboBox;
	import hk.org.ha.fmk.pms.flex.components.core.ExtendedDropDownList;
	import hk.org.ha.fmk.pms.flex.utils.ExtendedDateFieldValidator;
	import hk.org.ha.model.pms.dms.persistence.DmAdminTime;
	import hk.org.ha.model.pms.dms.persistence.DmDailyFrequency;
	import hk.org.ha.model.pms.dms.persistence.DmDrug;
	import hk.org.ha.model.pms.dms.persistence.DmSite;
	import hk.org.ha.model.pms.dms.persistence.DmSupplFrequency;
	import hk.org.ha.model.pms.dms.persistence.DmSupplSite;
	import hk.org.ha.model.pms.dms.vo.DrugRouteCriteria;
	import hk.org.ha.model.pms.persistence.disp.MedOrder;
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
	import hk.org.ha.model.pms.udt.vetting.ActionStatus;
	import hk.org.ha.model.pms.udt.vetting.FmStatus;
	import hk.org.ha.model.pms.udt.vetting.PrnPercentage;
	import hk.org.ha.model.pms.udt.vetting.RegimenDurationUnit;
	import hk.org.ha.model.pms.udt.vetting.RegimenType;
	import hk.org.ha.model.pms.vo.drug.DmDrugLite;
	import hk.org.ha.model.pms.vo.drug.DmAdminTimeLite;
	import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
	import hk.org.ha.model.pms.vo.rx.Dose;
	import hk.org.ha.model.pms.vo.rx.DoseFluid;
	import hk.org.ha.model.pms.vo.rx.DoseGroup;
	import hk.org.ha.model.pms.vo.rx.Freq;
	import hk.org.ha.model.pms.vo.rx.InjectionRxDrug;
	import hk.org.ha.model.pms.vo.rx.IvAdditive;
	import hk.org.ha.model.pms.vo.rx.IvFluid;
	import hk.org.ha.model.pms.vo.rx.IvRxDrug;
	import hk.org.ha.model.pms.vo.rx.Regimen;
	import hk.org.ha.model.pms.vo.rx.RxDrug;
	import hk.org.ha.model.pms.vo.rx.RxItem;
	import hk.org.ha.model.pms.vo.rx.Site;
	import hk.org.ha.model.pms.vo.security.PropMap;
	import hk.org.ha.model.pms.vo.sys.SysMsgMap;
	import hk.org.ha.view.pms.main.medprofile.ItemRenderer;
	import hk.org.ha.view.pms.main.medprofile.MedProfileUtils;
	import hk.org.ha.view.pms.main.vetting.OrderEditView;
	import hk.org.ha.view.pms.main.vetting.mp.popup.MpOrderEditItemPopup;
	import hk.org.ha.view.pms.main.vetting.popup.DailyFreqPopup;
	import hk.org.ha.view.pms.main.vetting.popup.DayOfWeekPopup;
	import hk.org.ha.view.pms.main.vetting.popup.RouteSitePopup;
	import hk.org.ha.view.pms.main.vetting.popup.SupplFreqPopup;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	import mx.controls.DataGrid;
	import mx.core.FlexGlobals;
	import mx.core.IFlexDisplayObject;
	import mx.core.UIComponent;
	import mx.formatters.DateFormatter;
	import mx.managers.PopUpManager;
	import mx.utils.StringUtil;
	
	import org.granite.tide.seam.Context;
	
	import spark.components.Application;
	import spark.components.ComboBox;
	import spark.components.DropDownList;
	import spark.components.supportClasses.DropDownListBase;
	import spark.events.DropDownEvent;
	import spark.events.IndexChangeEvent;
	
	[Bindable]
	public class OrderEditViewUtil {
		
		public static var lastPopupDateTime:Date = null;
		
		public function OrderEditViewUtil():void
		{			
			super();			
		}		
		
		private static function commonComboBoxKeyDownAction(event:KeyboardEvent):Boolean {
			return event.keyCode == Keyboard.ENTER || event.keyCode == Keyboard.TAB || event.keyCode == Keyboard.DELETE || event.keyCode == Keyboard.BACKSPACE;
		}
		
		/*  Daily Frequency Function */
		public static function clearDailyFreq(data:Dose, dailyFreqCbx:ExtendedComboBox):void
		{
			data.dailyFreq				= new Freq;
			data.dmDailyFrequency		= new DmDailyFrequency;
			dailyFreqCbx.selectedItem	= null;
		}
		
		public static function dailyFreqLabelFunc(item:Object, data:Dose, dailyFreqCbx:ExtendedComboBox, itemRenderer:ItemRenderer):String
		{				
			if ( data && data.dailyFreq ) {	
				if ( dailyFreqCbx.isDropDownOpen ) {					
					return DmDailyFrequency(item).dailyFreqDesc;
				} else {
					if ( data.dailyFreq.desc ) {
						return StringUtil.trim( data.dailyFreq.desc ).toUpperCase();
					} else {
						return itemRenderer['dailyFreqDispText'];
					}
				}
			}
			if ( item ) {
				if ( item is DmDailyFrequency ) {
					return DmDailyFrequency(item).dailyFreqDesc;
				} else {
					return "";
				}
			}
			return "";
		}
		
		public static function dailyFreqCbxKeyDownHandler(event:KeyboardEvent, itemRenderer:*, updateFunc:Function):void
		{
			var dose:Dose = itemRenderer.data as Dose;
			var dailyFreqCbx:ExtendedComboBox = itemRenderer.dailyFreqCbx as ExtendedComboBox;
			var dataProvider:ListCollectionView = dailyFreqCbx.dataProvider as ListCollectionView;
			
			if ( event.keyCode == Keyboard.UP || 
				event.keyCode == Keyboard.DOWN ) {						
				dailyFreqCbx.openDropDown();
				dailyFreqCbx.textInput.textDisplay.horizontalScrollPosition = 0;
			} else if ( commonComboBoxKeyDownAction(event) && !dailyFreqCbx.isDropDownOpen ) {
				if ( StringUtil.trim(dailyFreqCbx.textInput.text) == "" ) { 
					itemRenderer.dailyFreqDispText = "";
					if ( dose.dailyFreq && 
						dose.dailyFreq.code && 
						dose.dailyFreq.code != "" ) {
						clearDailyFreq(dose,dailyFreqCbx);
						if ( updateFunc != null ) {
							updateFunc();
						}
					}
				} else {
					if (event.keyCode == Keyboard.ENTER) {
						updateDailyFreqAfterUserChange(dose, dailyFreqCbx);
					}					
				}
				if (event.keyCode == Keyboard.TAB) {
					if ( dose.dailyFreqErrorString ) {
						dose.dailyFreqErrorString = null;
					}
				}
			}
		}
		
		public static function updateDailyFreqAfterUserChange(data:*, dailyFreqCbx:ExtendedComboBox):void {
			var dose:Dose = data as Dose;
			var dmDailyFrequency:DmDailyFrequency = dose.dmDailyFrequency;
			if (dailyFreqCbx.selectedIndex < 0 || dose.dailyFreq.code == null || StringUtil.isWhitespace(dose.dailyFreq.code)) {
				var indexVector:Vector.<int> = OrderEditViewUtil.dailyFreqItemMatchingFunc(dailyFreqCbx, StringUtil.trim(dailyFreqCbx.textInput.text));
				if (indexVector.length > 0 && indexVector[0] >= 0) {
					dmDailyFrequency = dailyFreqCbx.dataProvider.getItemAt(indexVector[0]) as DmDailyFrequency;
					if (dmDailyFrequency.numOfInputValue <= 0 && StringUtil.trim(dailyFreqCbx.textInput.text) == dmDailyFrequency.dailyFreqDesc) {
						dose.dailyFreq.code = dmDailyFrequency.dailyFreqCode;
						dose.dailyFreq.desc = dmDailyFrequency.dailyFreqDesc;
						dose.dailyFreq.param = [];
						dose.dmDailyFrequency = dailyFreqCbx.dataProvider.getItemAt(indexVector[0]) as DmDailyFrequency;
						dailyFreqCbx.dispatchEvent(new KeyboardEvent(KeyboardEvent.KEY_DOWN));
					}
				}
			} else if (dmDailyFrequency && dmDailyFrequency.numOfInputValue == 0) {					
				if (dmDailyFrequency.dailyFreqDesc != dose.dailyFreq.desc) {
					if (dmDailyFrequency.dailyFreqCode == dose.dailyFreq.code) {
						if (dailyFreqCbx.selectedIndex >= 0) {
							dose.dailyFreq.desc = dmDailyFrequency.dailyFreqDesc;
						}
					}
				}
			}
		}
		
		public static function dailyFreqCbxDoubleClickHandler(event:MouseEvent, itemRenderer:*, updateFunc:Function):void
		{
			try {
				var dailyFreqCbx:ExtendedComboBox = itemRenderer.dailyFreqCbx as ExtendedComboBox;
				var data:Dose = itemRenderer.data as Dose;
				
				if ( !dailyFreqCbx.isDropDownOpen ) {
					if ( !isNaN(data.dmDailyFrequency.numOfInputValue) && data.dmDailyFrequency.numOfInputValue > 0 ) {
						itemRenderer.retainDescFlag = true;
						dailyFreqPopupFunc(itemRenderer);
					}
				}
			} catch (e:Error) {
				trace(e.getStackTrace());
			}
		}
		
		public static function dailyFreqCbxFocusOutHandler(itemRenderer:*):void
		{
			try {		
				var dailyFreqCbx:ExtendedComboBox = itemRenderer.dailyFreqCbx as ExtendedComboBox; 
				var data:Dose = itemRenderer.data as Dose;
				var orderEditViewInfo:OrderEditViewInfo = itemRenderer.orderEditViewInfo as OrderEditViewInfo;
				var dmDailyFrequency:DmDailyFrequency = data.dmDailyFrequency;
				
				if (dmDailyFrequency.numOfInputValue <= 0) {
					if (dailyFreqCbx.selectedIndex < 0) {
						var indexVector:Vector.<int> = dailyFreqItemMatchingFunc(dailyFreqCbx, StringUtil.trim(dailyFreqCbx.textInput.text));
						if (indexVector.length > 0 && indexVector[0] >= 0) {
							data.dmDailyFrequency = dailyFreqCbx.dataProvider.getItemAt(indexVector[0]) as DmDailyFrequency;						
						}
						dailyFreqCbx.dispatchEvent(new KeyboardEvent(KeyboardEvent.KEY_DOWN));
					} 
					else if (dmDailyFrequency.dailyFreqDesc != data.dailyFreq.desc) {					
						if (dmDailyFrequency.numOfInputValue == 0) {
							if (dmDailyFrequency.dailyFreqCode == data.dailyFreq.code) {
								if (dailyFreqCbx.selectedIndex >= 0) {
									data.dailyFreq.desc = dmDailyFrequency.dailyFreqDesc;
								}
							}
						}
					}
				}
				
				dailyFreqCbx.textInput.textDisplay.horizontalScrollPosition = 0;
				if ( data.dailyFreqErrorString ) {
					orderEditViewInfo.customizedCallLater(
						function():void {
							data.dailyFreqErrorString = null;	
							orderEditViewInfo.orderEditView.manualErrorString = null;
						}
					);
				}
			} catch (e:Error) {
				trace(e.getStackTrace());					
			}
		}
		
		public static function dailyFreqCbxOpenHandler(itemRenderer:*):void
		{
			try {
				var data:Dose = itemRenderer.data as Dose;
				var dailyFreqCbx:ExtendedComboBox = itemRenderer.dailyFreqCbx as ExtendedComboBox;
				
				if (dailyFreqCbx.dropDown) {
					dailyFreqCbx.dropDown.width=400;
				}
				//PMS-3055
				//Follow Outpatient Behavior;
				if ( data.dmDailyFrequency && data.dailyFreq && data.dailyFreq.param && data.dailyFreq.param.length > 0 ) {
					itemRenderer.retainDescFlag = true
					dailyFreqCbx.selectedIndex = -1;							
				}
			} catch (e:Error) {
				dailyFreqCbx.selectedIndex = -1;
				trace(e.getStackTrace());
			}
		}	
		
		public static function dailyFreqCbxCloseHandler(itemRenderer:*):void
		{		
			try {
				var data:Dose = itemRenderer.data as Dose;
				var dailyFreqCbx:ExtendedComboBox = itemRenderer.dailyFreqCbx as ExtendedComboBox;
				
				if (dailyFreqCbx.selectedItem is DmDailyFrequency) {
					var selectedDmDailyFrequency:DmDailyFrequency = dailyFreqCbx.selectedItem;
					var orderEditViewInfo:OrderEditViewInfo = itemRenderer.orderEditViewInfo as OrderEditViewInfo;
					var updateFunc:Function;
					if ( orderEditViewInfo.isMoiEditFlag ) {
						updateFunc = itemRenderer.commonUpdate;
					}
					
					if ( dailyFreqCbx.selectedIndex > -1 ) {
						if ( checkDailyFreqPopup(data,dailyFreqCbx,updateFunc) ) {
							itemRenderer.retainDescFlag = true;
							dailyFreqCbx.callLater(dailyFreqPopupFunc,[itemRenderer]);
						} else {
							itemRenderer.retainDescFlag = false;
							if ( StringUtil.trim(selectedDmDailyFrequency.dailyFreqDesc) != StringUtil.trim(dailyFreqCbx.textInput.text) ) {
								dailyFreqCbx.textInput.text = selectedDmDailyFrequency.dailyFreqDesc;
							}
						}
					}
				}
			} catch (e:Error) {
				trace(e.getStackTrace());
			}
		}
		
		public static function dailyFreqItemMatchingFunc(comboBox:ExtendedComboBox, inputText:String):Vector.<int> {
			var result:Vector.<int> = new Vector.<int>;
			var dmDailyFreq:DmDailyFrequency;
			var enteredText:String = inputText.toUpperCase();
			var trimmedText:String = StringUtil.trim(enteredText);
			var firstTargetIndex:int = -1;
			var secondTargetIndex:int = -1;
			for ( var index:int = 0; index < comboBox.dataProvider.length; index++ ) {
				dmDailyFreq = comboBox.dataProvider.getItemAt(index) as DmDailyFrequency;
				if ( enteredText != "" && dmDailyFreq.dailyFreqDesc.indexOf(enteredText) == 0 ) {
					result.push(index);					
				} 
				if ( enteredText != "" && dmDailyFreq.dailyFreqDesc == enteredText ) {
					firstTargetIndex = index;					
				}else if ( trimmedText != "" && dmDailyFreq.dailyFreqDesc == trimmedText ) {
					secondTargetIndex = index;					
				}
			}
			
			if ( firstTargetIndex != -1 ) {
				result.unshift(firstTargetIndex);
			} else if ( secondTargetIndex != -1 ) {	
				if ( result.length == 0 ) {
					result.unshift(secondTargetIndex);
				}
			}
			
			return result;
		}
		
		public static function checkDailyFreqPopup(data:Dose, dailyFreqCbx:ExtendedComboBox, updateFunc:Function):Boolean
		{		
			if ( !data || !data.dailyFreq ) {
				updateFunc.call();
				return false;
			}
			
			var dmDailyFrequency:DmDailyFrequency = data.dmDailyFrequency;
			if ( isNaN(dmDailyFrequency.numOfInputValue) || dmDailyFrequency.numOfInputValue <= 0 ) {
				if ( updateFunc != null ) {
					updateFunc.call();
				}
				return false;
			}
			
			if ( !data.checkDailyFreqParam() || dailyFreqCbx.isDropDownOpen ) {
				return false;
			}
			
			return true;
		}
		
		public static function dailyFreqChangeFunc(event:IndexChangeEvent, updateFunc:Function, itemRenderer:*):void
		{
			var data:Dose = itemRenderer.data as Dose;
			var dmDailyFrequency:DmDailyFrequency;
			var orderEditViewInfo:OrderEditViewInfo = itemRenderer.orderEditViewInfo as OrderEditViewInfo;
			var dailyFreqCbx:ExtendedComboBox 	    = itemRenderer.dailyFreqCbx as ExtendedComboBox;
			var pattern:RegExp = /[ ]*$/;
			var inputText:String = dailyFreqCbx.textInput.text.replace(pattern,"");
			
			if ( event.newIndex < 0 ) {
				if ( data.dmDailyFrequency && 
					data.dailyFreq && 
					data.dailyFreq.param && 
					data.dailyFreq.param.length > 0 &&
					itemRenderer.retainDescFlag && 
					event.newIndex == -1 ) {
					
					dailyFreqCbx.selectedItem = data.dmDailyFrequency;
					dailyFreqCbx.textInput.text = data.dailyFreq.desc;	
					itemRenderer.retainDescFlag = false;
				} else {
					if ( !data.dailyFreq || inputText != data.dailyFreq.desc ) {										
						clearDailyFreq(data as Dose,dailyFreqCbx);
						itemRenderer.dailyFreqDispText = inputText;
						if ( StringUtil.trim(dailyFreqCbx.textInput.text) != "" ) {
							orderEditViewInfo.customizedCallLater(
								function():void {
									if ( StringUtil.trim(dailyFreqCbx.textInput.text) != "" ) {
										data.dailyFreqErrorString = orderEditViewInfo.sysMsgMap.getMessage("0055");
										orderEditViewInfo.orderEditView.manualErrorString = data.dailyFreqErrorString;
										dailyFreqCbx.setFocus();
									}
								}
							);
						}
						if ( updateFunc != null ) {
							updateFunc();	
						}
					}
				}
			} 
			else {						
				dmDailyFrequency			   = orderEditViewInfo.orderEditView.parentApplication.dmDailyFrequencyList.getItemAt(event.newIndex) as DmDailyFrequency;							
				data.dmDailyFrequency 		   = dmDailyFrequency;
				data.dailyFreq 		  		   = new Freq;
				data.dailyFreq.desc   		   = dmDailyFrequency.dailyFreqDesc;
				data.dailyFreq.param  		   = [];
				data.dailyFreq.code   		   = dmDailyFrequency.dailyFreqCode;
				data.dailyFreq.multiplierType  = "-";
				itemRenderer.dailyFreqDispText = "";
				if ( dmDailyFrequency.multiplierType == "F" ) {
					if ( !orderEditViewInfo.isMoiEditFlag ) {
						data.dmAdminTimeLite = null;
						data.adminTimeCode = null;
					}
				}//PMS-2544
				
				if ( dmDailyFrequency.numOfInputValue <= 0 || isNaN(dmDailyFrequency.numOfInputValue) ) {
					if ( data.dailyFreqErrorString ) {
						data.dailyFreqErrorString = null;
						orderEditViewInfo.orderEditView.manualErrorString = null;
					}
				}
				if ( data.isStop ) {
					data.clearDoseForStop(true);
				}												
				if ( updateFunc != null ) {
					updateFunc();	
				}				
			}	
			itemRenderer.callLater(itemRenderer.doseList.refresh);
		}
		
		public static function dailyFreqPopupFunc(itemRenderer:*):void
		{
			var data:Dose 							= itemRenderer.data as Dose;
			var dailyFreqCbx:ExtendedComboBox 		= itemRenderer.dailyFreqCbx as ExtendedComboBox;
			var orderEditViewInfo:OrderEditViewInfo = itemRenderer.orderEditViewInfo as OrderEditViewInfo;
			var orderEditView:OrderEditView			= orderEditViewInfo.orderEditView;
			var updateFunc:Function;
			if ( orderEditViewInfo.isMoiEditFlag ) {
				updateFunc = itemRenderer.commonUpdate;
			} else {
				updateFunc = itemRenderer.recalHandler;
			}
			
			var okHandler:Function = function(value:Object):void {		
				try {
					var popup:DailyFreqPopup 		= value as DailyFreqPopup;															
					data.dailyFreq.param 			= [Number(popup.param1.selectedItem).toString()];				
					data.dailyFreq.desc 			= data.getDailyFreqFullDesc();
					dailyFreqCbx.textInput.text 	= data.dailyFreq.desc;
					dailyFreqCbx.toolTip 			= data.dailyFreq.desc;
					itemRenderer.dailyFreqDispText 	= "";
					if ( data.dailyFreqErrorString ) {
						data.dailyFreqErrorString = null;
						orderEditView.manualErrorString = null;
					}					
					orderEditViewInfo.disableProcessFlag();
					orderEditViewInfo.customizedCallLater(dailyFreqCbx.setFocus,100);
					updateFunc.call();
					PopUpManager.removePopUp(popup);
				} catch (e:Error) {
					trace(e.getStackTrace());
				}
			}									
			
			var cancelHandler:Function = function(evt:Event):void {
				try {
					itemRenderer.dailyFreqDispText = "";
					clearDailyFreq(data, dailyFreqCbx);
					if ( data.dailyFreqErrorString ) {
						data.checkDailyFreq(orderEditViewInfo.sysMsgMap, true);
					}					
					PopUpManager.removePopUp( DailyFreqPopup(UIComponent(evt.target).parentDocument) );
					orderEditViewInfo.disableProcessFlag();
					orderEditViewInfo.customizedCallLater(dailyFreqCbx.setFocus,100);
					updateFunc.call();
				} catch (e:Error) {
					trace(e.getStackTrace());
				}
			}
			
			orderEditViewInfo.context.dispatchEvent( new ShowDailyFreqPopupEvent(data.dmDailyFrequency,okHandler, cancelHandler));
			orderEditViewInfo.customizedCallLater(function():void{
				if ( orderEditViewInfo.isMoiEditFlag ) {
					closeAllDropDown(orderEditView.moView, dailyFreqCbx);
				} else {
					closeAllDropDown(orderEditView.poView, dailyFreqCbx);
				}
			},300);
		}
		
		/* Supplementary Frequency Function */
		public static function supplFreqCbxKeyDownHandler(event:KeyboardEvent, itemRenderer:*, updateFunc:Function):void
		{
			try {
				var dose:Dose = itemRenderer.data as Dose;
				var supplFreqCbx:ExtendedComboBox = itemRenderer.supplFreqCbx as ExtendedComboBox;
				
				if ( event.keyCode == Keyboard.UP || 
					event.keyCode == Keyboard.DOWN ) {					
					supplFreqCbx.openDropDown();		
					supplFreqCbx.textInput.textDisplay.horizontalScrollPosition = 0;
					//PMS-2986
				} else if ( commonComboBoxKeyDownAction(event) && !supplFreqCbx.isDropDownOpen ) {
					if ( StringUtil.trim(supplFreqCbx.textInput.text) == "" ) {
						itemRenderer.supplFreqDispText = "";
						if ( dose.supplFreq && 
							dose.supplFreq.code && 
							dose.supplFreq.code != "" ) {
							clearSupplFreq(dose,supplFreqCbx);
							if ( updateFunc != null ) {
								updateFunc();
							}
						}	
					} else {
						if (event.keyCode == Keyboard.ENTER) {
							updateSupplFreqAfterUserChange(dose, supplFreqCbx);
						}
					}
					if (event.keyCode == Keyboard.TAB) {
						if ( dose.supplFreqErrorString ) {
							dose.supplFreqErrorString = null;
						}
					}
				}
				
			} catch (e:Error) {
				trace(e.getStackTrace());
			}
		}
		
		public static function updateSupplFreqAfterUserChange(data:*,supplFreqCbx:ExtendedComboBox):void {
			var dose:Dose = data as Dose;
			var dmSupplFrequency:DmSupplFrequency = dose.dmSupplFrequency;
			if (supplFreqCbx.selectedIndex < 0 || dose.supplFreq.code == null || StringUtil.isWhitespace(dose.supplFreq.code)) {
				var indexVector:Vector.<int> = OrderEditViewUtil.supplFreqItemMatchingFunc(supplFreqCbx, StringUtil.trim(supplFreqCbx.textInput.text));
				if (indexVector.length > 0 && indexVector[0] >= 0) {
					dmSupplFrequency = supplFreqCbx.dataProvider.getItemAt(indexVector[0]) as DmSupplFrequency;
					if (dmSupplFrequency.numOfInputValue <= 0 && StringUtil.trim(supplFreqCbx.textInput.text) == dmSupplFrequency.supplFreqDesc) {
						dose.supplFreq.code = dmSupplFrequency.supplFreqCode;
						dose.supplFreq.desc = dmSupplFrequency.supplFreqDesc;
						dose.supplFreq.param = [];
						dose.dmSupplFrequency = supplFreqCbx.dataProvider.getItemAt(indexVector[0]) as DmSupplFrequency;
						supplFreqCbx.dispatchEvent(new KeyboardEvent(KeyboardEvent.KEY_DOWN));
					}
				}
			} else if (dmSupplFrequency && dmSupplFrequency.numOfInputValue == 0 && dmSupplFrequency.multiplierType != "E") {					
				if (dmSupplFrequency.supplFreqDesc != dose.supplFreq.desc) {
					if (dmSupplFrequency.supplFreqCode == dose.supplFreq.code) {
						if (supplFreqCbx.selectedIndex >= 0) {
							dose.supplFreq.desc = dmSupplFrequency.supplFreqDesc;
						}
					}
				}
			}
		}
		
		public static function supplFreqCbxDoubleClickHandler(itemRenderer:*):void
		{
			try {
				var data:Dose = itemRenderer.data as Dose;
				var supplFreqCbx:ExtendedComboBox = itemRenderer.supplFreqCbx as ExtendedComboBox;
				
				if ( !supplFreqCbx.isDropDownOpen ) {
					itemRenderer.retainDescFlag = true;
					data.prevParam = data.supplFreq.param;
					supplFreqPopupFunc(itemRenderer);
				}
			} catch (e:Error) {
				trace(e.getStackTrace());
			}
		}
		
		public static function supplFreqCbxFocusOutHandler(itemRenderer:*):void
		{
			try {
				var data:Dose = itemRenderer.data as Dose;
				var orderEditViewInfo:OrderEditViewInfo = itemRenderer.orderEditViewInfo as OrderEditViewInfo;
				var supplFreqCbx:ExtendedComboBox = itemRenderer.supplFreqCbx as ExtendedComboBox;
				var dmSupplFrequency:DmSupplFrequency = data.dmSupplFrequency;
				
				if (dmSupplFrequency == null) {
					return;
				}
				
				if (dmSupplFrequency.multiplierType != 'E' && dmSupplFrequency.numOfInputValue <= 0) {
					if (supplFreqCbx.selectedIndex < 0) {
						var indexVector:Vector.<int> = supplFreqItemMatchingFunc(supplFreqCbx, StringUtil.trim(supplFreqCbx.textInput.text));
						if (indexVector.length > 0 && indexVector[0] >= 0) {
							data.dmSupplFrequency = supplFreqCbx.dataProvider.getItemAt(indexVector[0]) as DmSupplFrequency;						
						}
						supplFreqCbx.dispatchEvent(new KeyboardEvent(KeyboardEvent.KEY_DOWN));
					}
					else if (dmSupplFrequency.supplFreqDesc != data.supplFreq.desc) {					
						if (dmSupplFrequency.numOfInputValue == 0) {
							if (dmSupplFrequency.supplFreqCode == data.supplFreq.code) {
								if (supplFreqCbx.selectedIndex >= 0) {
									data.supplFreq.desc = dmSupplFrequency.supplFreqDesc;	
								}
							}
						}
					}
				}
				
				supplFreqCbx.textInput.textDisplay.horizontalScrollPosition = 0;
				
				if ( data.supplFreqErrorString ) {
					orderEditViewInfo.customizedCallLater(
						function():void {
							data.supplFreqErrorString = null;	
							orderEditViewInfo.orderEditView.manualErrorString = null;
						}
					);
				}
			} catch (e:Error) {
				trace(e.getStackTrace());					
			}
		}	
		
		public static function supplFreqCbxOpenHandler(itemRenderer:*):void
		{				
			try {
				var data:Dose = itemRenderer.data as Dose;
				var orderEditViewInfo:OrderEditViewInfo = itemRenderer.orderEditViewInfo as OrderEditViewInfo;
				var supplFreqCbx:ExtendedComboBox = itemRenderer.supplFreqCbx as ExtendedComboBox;
				
				supplFreqCbx.dropDown.width=400;
				//PMS-3055 
				//Follow Outpatient Behavior;
				if ( data.dmSupplFrequency && data.supplFreq && data.supplFreq.param && data.supplFreq.param.length > 0 ) {					
					itemRenderer.retainDescFlag = true;
					orderEditViewInfo.moSupplFreqOldIndex = supplFreqCbx.selectedIndex;
					supplFreqCbx.selectedIndex = -1;
				}
			} catch (e:Error) {
				supplFreqCbx.selectedIndex = -1;
				trace(e.getStackTrace());
			}
		}
		
		public static function supplFreqCbxCloseHandler(itemRenderer:*,updateFunc:Function):void
		{			
			try {
				var data:Dose = itemRenderer.data as Dose;
				var orderEditViewInfo:OrderEditViewInfo = itemRenderer.orderEditViewInfo as OrderEditViewInfo;
				var supplFreqCbx:ExtendedComboBox = itemRenderer.supplFreqCbx as ExtendedComboBox;
				var selectedDmSupplFrequency:DmSupplFrequency = supplFreqCbx.selectedItem as DmSupplFrequency;
				
				if ( supplFreqCbx.selectedIndex > -1 ) {
					if ( OrderEditViewUtil.checkSupplFreqPopup(data, supplFreqCbx, updateFunc) ) {
						itemRenderer.retainDescFlag = true;
						supplFreqPopupFunc(itemRenderer);
					} else {
						itemRenderer.retainDescFlag = false;
						if ( StringUtil.trim(selectedDmSupplFrequency.supplFreqDesc) != StringUtil.trim(supplFreqCbx.textInput.text) ) {
							supplFreqCbx.textInput.text = selectedDmSupplFrequency.supplFreqDesc;
						}
					}
				}
			} catch (e:Error) {
				trace(e.getStackTrace());
			}
		}
		
		public static function supplFreqLabelFunc(item:Object, data:Dose, supplFreqCbx:ExtendedComboBox, itemRenderer:ItemRenderer):String
		{	
			if ( data && data.supplFreq ) {	
				if ( supplFreqCbx.isDropDownOpen ) {					
					return DmSupplFrequency(item).supplFreqDesc;
				} else {
					if ( data.supplFreq.desc ) {
						return StringUtil.trim( data.supplFreq.desc ).toUpperCase();
					} else {
						return itemRenderer['supplFreqDispText'];
					}
				}
			}
			
			if ( item ) {
				if ( item is DmSupplFrequency ) {
					return DmSupplFrequency(item).supplFreqDesc;
				} else {
					return "";
				}
			}
			return "";
		}
		
		public static function supplFreqItemMatchingFunc(comboBox:ExtendedComboBox, inputText:String):Vector.<int> {
			var result:Vector.<int> = new Vector.<int>;
			var dmSupplFreq:DmSupplFrequency;
			var enteredText:String = inputText.toUpperCase();
			var trimmedText:String = StringUtil.trim(enteredText);
			var firstTargetIndex:int = -1;
			var secondTargetIndex:int = -1;
			
			for ( var index:int = 0; index < comboBox.dataProvider.length; index++ ) {
				dmSupplFreq = comboBox.dataProvider.getItemAt(index) as DmSupplFrequency;
				if ( enteredText != "" && dmSupplFreq.supplFreqDesc.indexOf(enteredText) == 0 ) {
					result.push(index);					
				} 
				if ( enteredText != "" && dmSupplFreq.supplFreqDesc == enteredText ) {
					firstTargetIndex = index;					
				}else if ( trimmedText != "" && dmSupplFreq.supplFreqDesc == trimmedText ) {
					secondTargetIndex = index;					
				}
			}
			
			if ( firstTargetIndex != -1 ) {
				result.unshift(firstTargetIndex);
			} else if ( secondTargetIndex != -1 ) {	
				if ( result.length == 0 ) {
					result.unshift(secondTargetIndex);
				}
			}
			return result;
		}
		
		public static function clearSupplFreq(data:Dose, supplFreqCbx:ExtendedComboBox):void
		{
			data.supplFreq 				= new Freq;
			data.dmSupplFrequency 		= new DmSupplFrequency;
			supplFreqCbx.selectedItem 	= null;
		}
		
		public static function checkSupplFreqPopup(data:Dose,supplFreqCbx:ExtendedComboBox,updateFunc:Function):Boolean
		{
			if ( !data || !data.supplFreq ) {
				updateFunc.call();
				return false;
			}	
			
			if ( !(data.dmSupplFrequency.numOfInputValue > 0 || data.dmSupplFrequency.multiplierType == "E") ) {
				return false;
			}
			
			if ( !data.checkSupplFreqParam() || supplFreqCbx.isDropDownOpen ) {
				return false;
			}
			
			return true;
		}
		
		public static function supplFreqPopupFunc(itemRenderer:*):void
		{
			var data:Dose 							= itemRenderer.data as Dose;
			var orderEditViewInfo:OrderEditViewInfo = itemRenderer.orderEditViewInfo as OrderEditViewInfo;
			var orderEditView:OrderEditView			= orderEditViewInfo.orderEditView;
			var supplFreqCbx:ExtendedComboBox 		= itemRenderer.supplFreqCbx as ExtendedComboBox;
			var dmSupplFrequency:DmSupplFrequency 	= data.dmSupplFrequency;
			var regimen:Regimen;
			var okHandler:Function;
			var updateFunc:Function;
			
			if ( orderEditViewInfo.isMoiEditFlag ) {
				updateFunc = itemRenderer.commonUpdate;
				regimen = orderEditViewInfo.medOrderItem.regimen;
			} else {
				updateFunc = itemRenderer.recalHandler;
				regimen = itemRenderer.pharmOrderItem.regimen;
			}
			
			if ( dmSupplFrequency.multiplierType == "E" ) {
				okHandler = function(value:*):void {
					try {
						var popup:DayOfWeekPopup = value as DayOfWeekPopup;
						var resultParam:String = ((popup.sunday.selected)?"1":"0") +
							((popup.monday.selected)?"1":"0") +
							((popup.tuesday.selected)?"1":"0") +
							((popup.wednesday.selected)?"1":"0") +
							((popup.thursday.selected)?"1":"0") +
							((popup.friday.selected)?"1":"0") +
							((popup.saturday.selected)?"1":"0");
						data.supplFreq.param 		  	= [resultParam];
						data.supplFreq.desc  		  	= data.supplFreqFullDesc;
						data.supplFreq.multiplierType 	= "E"
						data.supplFreqErrorString     	= null;
						supplFreqCbx.textInput.text   	= String(data.supplFreq.desc).toUpperCase();						
						supplFreqCbx.toolTip          	= data.supplFreq.desc;
						itemRenderer.supplFreqDispText 	= "";
						orderEditView.manualErrorString = null;
						orderEditViewInfo.disableProcessFlag();
						orderEditViewInfo.customizedCallLater(supplFreqCbx.setFocus,100);
						updateFunc.call();
						PopUpManager.removePopUp(popup);
					} catch (e:Error) {
						trace(e.getStackTrace());
					}
				}									
			} else {
				if ( isNaN(dmSupplFrequency.numOfInputValue) || dmSupplFrequency.numOfInputValue <= 0 ) {
					updateFunc.call();
					return;
				}	
				okHandler = function(value:Object):void {	
					try {
						var popup:SupplFreqPopup = value as SupplFreqPopup;						
						data.supplFreq.param     = [];
						if( dmSupplFrequency.numOfInputValue > 0 ) {
							data.supplFreq.param.push( String(Number(popup.param1.selectedItem)) );
						}
						if( dmSupplFrequency.numOfInputValue > 1 ) {
							data.supplFreq.param.push( String(Number(popup.param2.selectedItem)) );
						}	
						data.supplFreq.desc				= data.supplFreqFullDesc;
						data.supplFreqErrorString   	= null;
						supplFreqCbx.textInput.text 	= data.supplFreq.desc;
						supplFreqCbx.toolTip 			= data.supplFreq.desc;
						itemRenderer.supplFreqDispText 	= "";
						orderEditView.manualErrorString = null;
						orderEditViewInfo.disableProcessFlag();
						orderEditViewInfo.customizedCallLater(supplFreqCbx.setFocus,100);
						updateFunc.call();	
						PopUpManager.removePopUp(popup);
					} catch (e:Error) {
						trace(e.getStackTrace());
					}
				}
			}				
			
			var cancelHandler:Function = function(value:*):void {
				try {
					var popup:SupplFreqPopup = value as SupplFreqPopup;
					PopUpManager.removePopUp( popup );
					itemRenderer.supplFreqDispText = "";
					clearSupplFreq(data,supplFreqCbx);
					if ( data.supplFreqErrorString ) {
						if ( regimen.type != RegimenType.Daily && regimen.type != RegimenType.StepUpDown ) {
							data.checkSupplFreq(orderEditViewInfo.sysMsgMap,true);
						} else {
							data.supplFreqErrorString = null;
						}
						orderEditView.manualErrorString = data.supplFreqErrorString;
					}
					orderEditViewInfo.disableProcessFlag();
					orderEditViewInfo.customizedCallLater(supplFreqCbx.setFocus,100);
					updateFunc.call();
				} catch (e:Error) {
					trace(e.getStackTrace());
				}
			}
			
			orderEditViewInfo.context.dispatchEvent( new ShowSupplFreqPopupEvent(
				data.prevParam,
				dmSupplFrequency, 										
				okHandler,
				cancelHandler)
			);
			orderEditViewInfo.customizedCallLater(function():void{
				if ( orderEditViewInfo.isMoiEditFlag ) {
					closeAllDropDown(orderEditView.moView, supplFreqCbx);
				} else {
					closeAllDropDown(orderEditView.poView, supplFreqCbx);
				}
			},300);
		}
		
		public static function supplFreqChangeFunc(event:IndexChangeEvent, itemRenderer:*, updateFunc:Function):void
		{	
			var data:Dose = itemRenderer.data as Dose;
			var orderEditViewInfo:OrderEditViewInfo = itemRenderer.orderEditViewInfo as OrderEditViewInfo;
			var dmSupplFrequency:DmSupplFrequency;
			var supplFreqCbx:ExtendedComboBox = itemRenderer.supplFreqCbx as ExtendedComboBox;
			var retainDescFlag:Boolean		 = itemRenderer.retainDescFlag as Boolean;
			var pattern:RegExp = /[ ]*$/;
			var inputText:String = supplFreqCbx.textInput.text.replace(pattern,"");
			
			if ( event.newIndex < 0 ) {
				if ( data.dmSupplFrequency && 
					data.supplFreq && 
					data.supplFreq.param && 
					data.supplFreq.param.length > 0 &&
					retainDescFlag &&
					event.newIndex == -1 ) {
					
					supplFreqCbx.selectedItem = data.dmSupplFrequency;
					supplFreqCbx.textInput.text = data.supplFreq.desc;
					retainDescFlag = false;
				} else {
					var supplFreqDesc:String = !data.supplFreq || !data.supplFreq.desc?"":data.supplFreq.desc;
					if ( !data.supplFreq || inputText != supplFreqDesc ) {
						itemRenderer.supplFreqDispText = inputText;
						clearSupplFreq(data as Dose,supplFreqCbx);
						if ( StringUtil.trim(supplFreqCbx.textInput.text) != "" ) {
							orderEditViewInfo.customizedCallLater(
								function():void {
									data.supplFreqErrorString = orderEditViewInfo.sysMsgMap.getMessage("0057");
									supplFreqCbx.setFocus();
									orderEditViewInfo.orderEditView.manualErrorString = data.supplFreqErrorString;
								}
							);
						} else {
							data.supplFreqErrorString = null;
							orderEditViewInfo.orderEditView.manualErrorString = data.supplFreqErrorString;
						}
						
						if ( updateFunc != null ) {
							updateFunc();
						}
					}
				}
			} else {						
				dmSupplFrequency              = orderEditViewInfo.dmSupplFrequencyList.getItemAt(event.newIndex) as DmSupplFrequency;
				data.dmSupplFrequency 		  = dmSupplFrequency;
				if ( data.dmSupplFrequency && data.dmSupplFrequency.multiplierType == "E" && dmSupplFrequency.multiplierType == "E" ) 
				{
					if ( data.supplFreq && orderEditViewInfo.moSupplFreqOldIndex == event.newIndex) 
					{
						data.prevParam = data.supplFreq.param;
					} 
					else 
					{
						data.prevParam = [];
					}
					
				} 
				else 
				{
					data.prevParam = [];
				}
				data.supplFreq 				  	= new Freq;
				data.supplFreq.desc			  	= dmSupplFrequency.supplFreqDesc;
				data.supplFreq.param 		  	= [];
				data.supplFreq.code 		  	= dmSupplFrequency.supplFreqCode;
				data.supplFreq.multiplierType 	= "-";
				itemRenderer.supplFreqDispText 	= data.supplFreq.desc
				
				if ( dmSupplFrequency.numOfInputValue <= 0 || isNaN(dmSupplFrequency.numOfInputValue) ) {
					if ( data.supplFreqErrorString ) {
						data.supplFreqErrorString = null;
						orderEditViewInfo.orderEditView.manualErrorString = null;
					}
					if ( updateFunc != null ) {
						updateFunc();
					}
				}					
			}
			
			itemRenderer.callLater(itemRenderer.doseList.refresh);
		}
		
		/* PRN function */
		public static function prnPercentLabelFunc(item:PrnPercentage, data:Dose, prnCbx:ExtendedDropDownList):String
		{
			try {														
				if ( item ) {
					return StringUtil.trim(item.displayValue);
				}
			} catch(e:Error) {					
				trace(e.getStackTrace());
			}
			return "";			
		}
		
		public static function updatePrnPercentage(orderEditViewInfo:OrderEditViewInfo, value:Object, durationInDay:Number=NaN):void
		{
			var dose:Dose;
			var doseGroup:DoseGroup;
			var regimen:Regimen;
			
			if ( value is Dose ) {
				dose = value as Dose;
				if ( dose.prn ) {
					OrderEditViewUtil.setPrnPercentFunc(dose,
						durationInDay,
						orderEditViewInfo
					);
				}
			} else if ( value is DoseGroup ) {						
				doseGroup = value as DoseGroup;
				for each ( dose in doseGroup.doseList ) {
					if ( dose.prn ) {
						OrderEditViewUtil.setPrnPercentFunc(dose,
							doseGroup.getDurationInDay(orderEditViewInfo.dmRegimenDictionary),					
							orderEditViewInfo
						);
					}
				}
			} else if ( value is Regimen ) {
				regimen = value as Regimen;
				for each ( doseGroup in regimen.doseGroupList ) {
					for each ( dose in doseGroup.doseList ) {
						if ( dose.prn ) {
							OrderEditViewUtil.setPrnPercentFunc(dose,
								doseGroup.getDurationInDay(orderEditViewInfo.dmRegimenDictionary),
								orderEditViewInfo
							);
						}
					}
				}
			}							
		}				
		
		public static function prnDisplayValueOf(inputText:String, list:ListCollectionView):PrnPercentage
		{
			for each (var prnPercent:PrnPercentage in list) {
				if (prnPercent.displayValue == inputText ) {
					return prnPercent;
				}
			}
			return null;
		}
		
		public static function checkInvalidPrnPercentage(inputText:String, list:ListCollectionView):Boolean
		{
			var prnPercent:PrnPercentage = prnDisplayValueOf(inputText, list);
			if ( prnPercent == null ) {
				return true;
			}
			else {
				return false;
			}
		}
		
		public static function prnCbxChangeHandler(itemRenderer:*, event:IndexChangeEvent):void
		{
			try {
				var prnCbx:ExtendedDropDownList = itemRenderer.prnCbx as ExtendedDropDownList;
				var orderEditViewInfo:OrderEditViewInfo = itemRenderer.orderEditViewInfo as OrderEditViewInfo;
				var dose:Dose = itemRenderer.data as Dose;
				
				if ( event.newIndex >= 0 ) {
					dose.prnPercent = prnCbx.dataProvider.getItemAt(event.newIndex) as PrnPercentage;
					if ( dose.prnPercentErrorString ) {
						dose.prnPercentErrorString = null;
					}
					if ( orderEditViewInfo.isMoiEditFlag ) {
						itemRenderer.clearDispQty();
						itemRenderer.refreshToDefaultDoseGroupPrn();
						if ( orderEditViewInfo.medOrderItem.dangerDrugFlag ) {
							itemRenderer.calDispQtyForDangerDrug();
						}
						orderEditViewInfo.updateMoView();
					} else {
						itemRenderer.recalHandler();
					}
					
				}
				orderEditViewInfo.orderEditView.manualErrorString = dose.prnPercentErrorString;
				itemRenderer.callLater(itemRenderer.doseList.refresh);
			} catch (e:Error) {
				trace(e.getStackTrace());
			}
		}
		
		public static function checkReleasePrnPercentByRegimen(regimen:Regimen, propMap:PropMap, orderEditViewInfo:OrderEditViewInfo):void
		{
			var prnDuration:Number = Number(propMap.getValue("vetting.regimen.prn.duration"));
			var durationInDay:Number;
			for each ( var doseGroup:DoseGroup in regimen.doseGroupList ) {
				durationInDay = doseGroup.getDurationInDay(orderEditViewInfo.dmRegimenDictionary);
				for each ( var dose:Dose in doseGroup.doseList ) {
					if ( isNaN(durationInDay) || durationInDay == 0 ) {
						dose.prnCbxEnabled = true;
						continue;
					}  
					
					if ( durationInDay <= prnDuration ) {
						dose.prnCbxEnabled = false;
					} else {
						dose.prnCbxEnabled = true;
					}
				}
			}
		}
		
		public static function checkDefaultPrnPercent(regimen:Regimen, orderEditViewInfo:OrderEditViewInfo):void
		{			
			if ( regimen.firstDose.dispQtyText != "" ) {
				return;
			}
			
			var prnDuration:Number = Number(FlexGlobals.topLevelApplication.propMap.getValue("vetting.regimen.prn.duration"));
			var durationInDay:Number;
			
			for each ( var doseGroup:DoseGroup in regimen.doseGroupList ) {
				durationInDay = doseGroup.getDurationInDay(orderEditViewInfo.dmRegimenDictionary);				
				for each ( var dose:Dose in doseGroup.doseList ) {
					if ( !dose.prn ) {
						continue;
					}
					if ( !dose.prnCbxEnabled ) {
						if ( durationInDay <= prnDuration ) {
							dose.prnPercent = PrnPercentage.Hundred;
						}
					} else {
						if ( dose.dispQtyText == "" && (dose.prnPercent == null || dose.prnPercent == PrnPercentage.Zero) ) {
							OrderEditViewUtil.updatePrnPercentage(orderEditViewInfo, dose, durationInDay);
						} else if ( durationInDay <= 0 || isNaN(durationInDay) ) {
							dose.prnPercent = PrnPercentage.Zero;
						}
					}
				}
			}
		}				
		
		public static function setPrnPercentFunc(data:Dose, durationInDay:Number, orderEditViewInfo:OrderEditViewInfo):void
		{
			if ( !data.prn ) {
				return;
			} else {
				var prnDuration:Number = Number(FlexGlobals.topLevelApplication.propMap.getValue("vetting.regimen.prn.duration"));
				var isMaintainedRoutePrn:Boolean;
				if ( !orderEditViewInfo.medOrderItem.isFreeTextEntryFlag 
					&& orderEditViewInfo.medOrderItem.dmRouteLite ) { 
					isMaintainedRoutePrn = FlexGlobals.topLevelApplication.prnRouteDescMap.getValueAsBoolean(orderEditViewInfo.medOrderItem.dmRouteLite.fullRouteDesc);
				} else {
					isMaintainedRoutePrn = FlexGlobals.topLevelApplication.prnRouteDescMap.getValueAsBoolean(orderEditViewInfo.medOrderItem.routeDesc);
				}
				
				if ( isNaN(durationInDay) || durationInDay == 0 || (!orderEditViewInfo.medOrderItem.dangerDrugFlag && orderEditViewInfo.medOrderItem.regimen.totalQty != 0) ) {
					data.prnPercent = PrnPercentage.Zero;				
				} else {
					
					//With duration and prnPercent - No Change - G22,23, J15,16
					if( orderEditViewInfo.medOrderItem.dangerDrugFlag && orderEditViewInfo.medOrderItem.regimen.totalQty == 0 ){
						if(data.prnPercent != PrnPercentage.Zero && data.prnPercent != null){
							return;
						}
					}
					
					if ( durationInDay <= prnDuration || orderEditViewInfo.medOrderItem.fmStatus == FmStatus.FreeTextEntryItem.dataValue ) {
						if ( data.prnPercent != PrnPercentage.Hundred ) {
							data.prnPercent = PrnPercentage.Hundred;									
						}
					} else {
						
						if ( isMaintainedRoutePrn ) {	
							var prnPercent:String = FlexGlobals.topLevelApplication.propMap.getValue("vetting.regimen.prn.percent");
							data.prnPercent = PrnPercentage.dataValueOf(prnPercent);
						} else {
							data.prnPercent = PrnPercentage.Hundred;
						}
					}
				}
			}
		}
		
		/* Site function */
		public static function siteCbxKeyDownHandler(event:KeyboardEvent, itemRenderer:*, isOp:Boolean, postCallFunc:Function):void
		{
			try {
				var data:Dose = itemRenderer.data as Dose;
				var siteCbx:ExtendedComboBox = itemRenderer.siteCbx as ExtendedComboBox;
				
				if ( event.keyCode == Keyboard.UP || 
					event.keyCode == Keyboard.DOWN ) {				
					siteCbx.openDropDown();		
					siteCbx.isPopUp = true;
					siteCbx.textInput.textDisplay.horizontalScrollPosition = 0;
				} else if ( commonComboBoxKeyDownAction(event) && !siteCbx.isDropDownOpen ) {
					if ( StringUtil.trim(siteCbx.textInput.text) == "" ) {
						itemRenderer.siteDispText = "";
						if ( data.siteCode != null ) {
							clearSite(data as Dose,siteCbx);
							if (postCallFunc != null) {
								postCallFunc()
							}
						}
					} else {
						if (event.keyCode == Keyboard.ENTER) {
							updateSiteAfterUserChange(data, siteCbx, isOp);
						}
					}
					if (event.keyCode == Keyboard.TAB) {
						if (data.siteErrorString) {
							data.siteErrorString = null;
						}
					}
				}
			} catch (e:Error) {
				trace(e.getStackTrace());
			}
		}
		
		public static function siteCbxOpenHandler(itemRenderer:*):void
		{
			try {
				var data:Dose = itemRenderer.data as Dose;
				var siteCbx:ExtendedComboBox = itemRenderer.siteCbx as ExtendedComboBox;
				if (siteCbx) {
					siteCbx.dropDown.width=400;					
					if ( data.inOthersSiteList ) {	
						itemRenderer.retainDescFlag = true;
						siteCbx.selectedIndex = -1;							
					}
				}
			} catch (e:Error) {
				siteCbx.selectedIndex = -1;
				trace(e.getStackTrace());
			}
		}	
		
		public static function siteCbxCloseHandler(itemRenderer:*):void
		{			
			try {
				itemRenderer.retainDescFlag = false;
			} catch (e:Error) {
				trace(e.getStackTrace());
			}
		}
		
		public static function siteItemMatchingFunc(comboBox:ExtendedComboBox, inputText:String):Vector.<int> {
			var result:Vector.<int> = new Vector.<int>;
			var site:Site;
			var enteredText:String = inputText.toUpperCase();
			var trimmedText:String = StringUtil.trim(enteredText);
			var firstTargetIndex:int = -1;
			var secondTargetIndex:int = -1;
			var siteLabelDesc:String;
			
			if (comboBox.dataProvider == null) {
				return result;
			}
			
			for ( var index:int = 0; index < comboBox.dataProvider.length; index++ ) {
				site = comboBox.dataProvider.getItemAt(index) as Site;
				siteLabelDesc = site.siteLabelDesc.toUpperCase();
				if ( enteredText != "" && siteLabelDesc.indexOf(enteredText) == 0 ) {
					result.push(index);					
				} 
				if ( enteredText != "" && siteLabelDesc == enteredText ) {
					firstTargetIndex = index;					
				}else if ( trimmedText != "" && siteLabelDesc == trimmedText ) {
					secondTargetIndex = index;					
				}
			}
			
			if ( firstTargetIndex != -1 ) {
				result.unshift(firstTargetIndex);
			} else if ( secondTargetIndex != -1 ) {	
				if ( result.length == 0 ) {
					result.unshift(secondTargetIndex);
				}
			}
			
			return result;
		}
		
		public static function siteLabelFunc(data:Dose, item:Object, siteCbx:ExtendedComboBox, itemRenderer:ItemRenderer):String
		{
			if ( siteCbx.isPopUp || siteCbx.isDropDownOpen ) {
				if (item) {
					return Site(item).siteLabelDesc;
				} else {
					return siteCbx.textInput.text;
				}
			} else {
				if ( data.siteCode != null ) {
					if ( data.supplSiteDesc != null && data.supplSiteDesc != "" ) {
						return Site(data.site).supplSiteDesc;
					}
					return Site(data.site).siteCode;
				} else {
					return itemRenderer['siteDispText'];
				}
			}
			return Site(item).siteLabelDesc;	
		}
		
		public static function mpSiteLabelFunction(dose:Dose, item:Object, siteCbx:ExtendedComboBox, itemRenderer:ItemRenderer):String 
		{				
			try {
				var site:Site = item as Site;
				var siteDispText:String = itemRenderer['siteDispText'] as String;
				if ( !siteCbx.isPopUp && !siteCbx.isDropDownOpen ) {
					if ( dose.siteCode != null ) {
						if ( dose.supplSiteDesc != null && dose.supplSiteDesc != "" ) {
							return MedProfileUtils.toTitleCase(dose.ipSite.supplSiteDesc);
						}
						if (dose.dmSite) {
							if (dose.dmSite.ipmoeDesc != null) {
								return dose.dmSite.ipmoeDesc;
							}
						} 
						return dose.siteCode;						
					} else {
						return siteDispText;
					}
				} else {
					if ( site ) {
						if (site.isSupplSite) {
							return MedProfileUtils.toTitleCase(site.supplSiteDesc);						
						} else {
							return site.siteLabelDesc;
						}
					} else {
						return siteDispText;
					}
				}
			} catch (e:Error) {
				trace(e.getStackTrace());
			}
			return "";
		}		
		
		public static function siteChangeFunc(event:IndexChangeEvent, itemRenderer:*, updateFunc:Function, orderEditViewInfo:OrderEditViewInfo):void
		{
			var data:Dose = itemRenderer.data as Dose;
			var orderEditViewInfo:OrderEditViewInfo = itemRenderer.orderEditViewInfo as OrderEditViewInfo;
			var orderEditView:OrderEditView 		= orderEditViewInfo.orderEditView;
			var medOrder:MedOrder					= orderEditViewInfo.medOrder;
			var siteCbx:ExtendedComboBox 	    	= itemRenderer.siteCbx as ExtendedComboBox;
			var retainDescFlag:Boolean		 		= itemRenderer.retainDescFlag as Boolean;
			var pattern:RegExp = /[ ]*$/;
			var inputText:String = siteCbx.textInput.text.replace(pattern,"");
			var isMpDischarge:Boolean = orderEditViewInfo.medOrder.isMpDischarge();
			var siteLabelDesc:String;
			var dmSite:DmSite;
			var dmSupplSite:DmSupplSite;
			
			if ( event.newIndex < 0 ) {
				if ( event.newIndex == -1 && 
					retainDescFlag &&
					data.inOthersSiteList ) {
					if (isMpDischarge) {
						siteCbx.textInput.text = data.ipSite.siteLabelDesc;
					} else {
						siteCbx.textInput.text = data.site.siteLabelDesc;
					}
					retainDescFlag = false;
				} else {
					
					siteLabelDesc = medOrder.isMpDischarge()?data.ipSite.siteLabelDesc:data.site.siteLabelDesc;
					siteLabelDesc = siteLabelDesc?siteLabelDesc:"";
					if (siteLabelDesc.toUpperCase() != inputText.toUpperCase()) {
						
						itemRenderer.siteDispText = inputText;
						if (siteLabelDesc) {
							clearSite(data as Dose,siteCbx);
							if ( !orderEditViewInfo.isMoiEditFlag ) {
								updatePharmOrderItemDispQty(orderEditViewInfo,itemRenderer.pharmOrderItem,0,0);
							}
							updateFunc.call();
						}
						if ( StringUtil.trim(siteCbx.textInput.text) != "" ) {
							
							orderEditViewInfo.customizedCallLater(
								function():void {
									data.siteErrorString = orderEditViewInfo.sysMsgMap.getMessage("0376");
									orderEditView.manualErrorString = data.siteErrorString;
									siteCbx.setFocus();		
								}
							);
						} else {
							data.siteErrorString = null;
							orderEditView.manualErrorString = data.siteErrorString;
						}
					}
				}				
				itemRenderer.doseList.refresh();
			} else {						
				var site:Site = siteCbx.selectedItem as Site;			
				if ( site.siteCode != "OTHERS" ) {
					
					data.updateSite(site.siteCode,site.supplSiteDesc,!isMpDischarge);
					itemRenderer.siteDispText 	= "";
					itemRenderer.doseList.refresh();
					updateFunc.call();
					orderEditView.manualErrorString = null;
				} else {
					
					//dispatch event to show other route/site popup
					var okHandler:Function = function(value:Object):void {		
						try {
							var routeSitePopup:RouteSitePopup = value as RouteSitePopup;
							if ( !routeSitePopup ) {
								return;
							} else {
								var selectedSite:Site;
								if (!isMpDischarge) {
									selectedSite = routeSitePopup.treeList.selectedItem.site as Site;
								} else {
									selectedSite = routeSitePopup.ipMoeTreeList.selectedItem.site as Site;
								}
								
								if ( !selectedSite ) {
									return;
								} else {			
									
									routeSitePopup.okBtn.enabled = false;
									
									data.updateSite(selectedSite.siteCode,selectedSite.supplSiteDesc,!medOrder.isMpDischarge());
									siteCbx.textInput.text 	= data.siteLabelDesc;									
									itemRenderer.siteDispText 	= "";
									if ( data.siteErrorString ) {
										data.siteErrorString = null;
										orderEditView.manualErrorString = null;
									}
									if ( !orderEditViewInfo.isMoiEditFlag ) {
										if ( data.dosageUnit != null && data.dosageUnit != "" ) {
											if ( data.dosageText == "" ) {
												data.dosageText = "0";
											}
										}
									}																		
									
									updateFunc.call();						
									itemRenderer.doseList.refresh();
									orderEditViewInfo.disableProcessFlag();									
									orderEditViewInfo.customizedCallLater(siteCbx.setFocus,100);
									PopUpManager.removePopUp(routeSitePopup);
								}
							}
						} catch (e:Error) {
							trace(e.getStackTrace());
						}
						
					}										
					var cancelHandler:Function = function(value:*):void {
						try {
							OrderEditViewUtil.clearSite(data,siteCbx);
							itemRenderer.siteDispText = "";
							siteCbx.textInput.text = "";
							if (data.siteErrorString) {
								data.checkSite(orderEditViewInfo.sysMsgMap, true);
								orderEditView.manualErrorString = data.siteErrorString;
							}
							PopUpManager.removePopUp(value);						
							updateFunc.call();
							itemRenderer.doseList.refresh();
							orderEditViewInfo.disableProcessFlag();
							orderEditViewInfo.customizedCallLater(siteCbx.setFocus,100);
						} catch (e:Error) {
							trace(e.getStackTrace());
						}
					}					
					var routeSitePopupEvent:Event = new ShowRouteSitePopupEvent(okHandler, cancelHandler, isMpDischarge);					
					if (isMpDischarge) {
						var drugRouteCriteria:DrugRouteCriteria = new DrugRouteCriteria;
						if (!orderEditViewInfo.medOrderItem.isFreeTextEntryFlag) {
							drugRouteCriteria.dispHospCode = orderEditView.workstore.hospCode;
							drugRouteCriteria.dispWorkstore = orderEditView.workstore.workstoreCode;
							if (orderEditViewInfo.isMoiEditFlag) {
								drugRouteCriteria.trueDisplayname = orderEditViewInfo.medOrderItem.displayName;
								drugRouteCriteria.formCode = orderEditViewInfo.medOrderItem.formCode;
								drugRouteCriteria.saltProperty = orderEditViewInfo.medOrderItem.saltProperty;
							} else {
								drugRouteCriteria.itemCode = itemRenderer.pharmOrderItem.itemCode;
							}
						}
						//!orderEditViewInfo.isMoiEditFlag = exclude parenteral sites in pharm. line
						orderEditViewInfo.context.dispatchEvent(new RetrieveIpOthersSiteMapListByDrugRouteCriteriaEvent(drugRouteCriteria, routeSitePopupEvent))
					} else {
						orderEditViewInfo.context.dispatchEvent(routeSitePopupEvent);
					}
					orderEditViewInfo.customizedCallLater(
						function():void
						{
							if (orderEditViewInfo.isMoiEditFlag) {
								closeAllDropDown(orderEditView.moView, siteCbx);
							} else {
								closeAllDropDown(orderEditView.poView, siteCbx);
							}
							if (orderEditViewInfo.context.routeSitePopup) {
								orderEditViewInfo.context.routeSitePopup.setFocus();
							}
						},300
					);
				}
			}
		}
		
		public static function updateSiteAfterUserChange(data:*, siteCbx:ExtendedComboBox, isOp:Boolean):void {
			var dose:Dose = data as Dose;
			if (!dose.inOthersSiteList) {
				if (siteCbx.selectedIndex < 0) {
					var indexVector:Vector.<int> = siteItemMatchingFunc(siteCbx, StringUtil.trim(siteCbx.textInput.text));
					if (indexVector.length > 0 && indexVector[0] >= 0) {
						if (isOp) {
							dose.site = siteCbx.dataProvider.getItemAt(indexVector[0]) as Site;
						} else {
							dose.ipSite = siteCbx.dataProvider.getItemAt(indexVector[0]) as Site;
						}
					}
					siteCbx.dispatchEvent(new KeyboardEvent(KeyboardEvent.KEY_DOWN));
				}
			}
		}
		
		public static function siteCbxFocusOutHandler(itemRenderer:*):void
		{
			try {
				var siteCbx:ExtendedComboBox = itemRenderer.siteCbx as ExtendedComboBox;
				var data:Dose = itemRenderer.data as Dose;
				var orderEditViewInfo:OrderEditViewInfo = itemRenderer.orderEditViewInfo as OrderEditViewInfo;
				var isMpDischarge:Boolean = orderEditViewInfo.medOrder.isMpDischarge();
				
				updateSiteAfterUserChange(data, siteCbx, !isMpDischarge);
				
			} catch (e:Error) {
				trace(e.getStackTrace());
			}
		}
		
		public static function clearSite(data:Dose,siteCbx:ExtendedComboBox):void
		{
			if ( !data || !siteCbx ) {
				return;
			}
			data.clearSite();
			siteCbx.selectedItem   = null;
		}
		
		public static function startDateCalendarChangeHandler(itemRenderer:*):void
		{		
			var doseGroup:DoseGroup = itemRenderer.data as DoseGroup;
			var startDateCalendar:ExtendedAbbDateField = itemRenderer.startDateCalendar as ExtendedAbbDateField;
			var endDateCalendar:ExtendedAbbDateField = itemRenderer.endDateCalendar as ExtendedAbbDateField;
			var orderEditViewInfo:OrderEditViewInfo = itemRenderer.orderEditViewInfo as OrderEditViewInfo;
			var tmpCalendar:ExtendedAbbDateField = new ExtendedAbbDateField;
			var dateFormatter:DateFormatter = new DateFormatter;
			dateFormatter.formatString = ""
			
			if ( !ExtendedDateFieldValidator.isValidDate(startDateCalendar) ) {
				doseGroup.startDateErrorString = orderEditViewInfo.sysMsgMap.getMessage("0288");
			} else {
				doseGroup.updateEndDate(orderEditViewInfo.dmRegimenDictionary);
				tmpCalendar.selectedDate = doseGroup.endDate;
				endDateCalendar.text = tmpCalendar.text;				
				doseGroup.startDateErrorString = null;				
			}
			orderEditViewInfo.orderEditView.manualErrorString = doseGroup.startDateErrorString;			
		}
		
		public static function endDateCalendarChangeHandler(itemRenderer:*):void
		{		
			var doseGroup:DoseGroup = itemRenderer.data as DoseGroup;
			var endDateCalendar:ExtendedAbbDateField = itemRenderer.endDateCalendar as ExtendedAbbDateField;
			var orderEditViewInfo:OrderEditViewInfo = itemRenderer.orderEditViewInfo as OrderEditViewInfo;
			if ( !ExtendedDateFieldValidator.isValidDate(endDateCalendar) ) {
				doseGroup.endDateErrorString = orderEditViewInfo.sysMsgMap.getMessage("0289");
			} else {				
				if ( !doseGroup.checkEndDateSmallerThanStartDate(orderEditViewInfo.sysMsgMap,false) ) {
					doseGroup.determineDuration(orderEditViewInfo.dmRegimenDictionary);	
				} else {
					doseGroup.durationText = "";
				}
				doseGroup.endDateErrorString = null;
			}
			
			orderEditViewInfo.orderEditView.manualErrorString = doseGroup.endDateErrorString;
		}
		
		public static function getSysMsg(sysMsgMap:SysMsgMap, msgCode:String, showErrorEnabled:Boolean):String
		{
			if ( showErrorEnabled ) {				
				return sysMsgMap.getMessage(msgCode);				
			} else {
				return null;
			}
		}
		
		public static function updatePharmOrderItemDispQty(orderEditViewInfo:OrderEditViewInfo, poiValue:PharmOrderItem, inCalQty:Number, inDoseQty:Number):void
		{
			orderEditViewInfo.backendProcessFlag = false;				
			if (!poiValue) {				
				return;
			}
			
			poiValue.calQty 	  = inCalQty;
			poiValue.issueQtyText = String(inCalQty);							
			poiValue.doseQty 	  = inDoseQty;				
		}
		
		//PMS-3396
		public static function closeAllDropDown(current:DisplayObjectContainer, srcDropDownListBase:DropDownListBase):void
		{
			for(var idx:int = 0; idx < current.numChildren; idx++)
			{
				var child:Object = current.getChildAt(idx);
				
				if ( srcDropDownListBase == child ) {
					continue;
				} else {
					if ( child is DropDownListBase ) {
						if ( DropDownListBase(child).isDropDownOpen ) {
							DropDownListBase(child).closeDropDown(false);						
						}
					} else {
						if ( child is DisplayObjectContainer ) {
							closeAllDropDown(child as DisplayObjectContainer, srcDropDownListBase);
						}
					}
				}
			}
		}
		
		private static function triggerStartAndEndDateFieldInput(orderEditViewInfo:OrderEditViewInfo, regimen:Regimen, showDateFieldFlag:Boolean):void {
			
			var doseGroup:DoseGroup = regimen.firstDoseGroup;
			var regimenType:RegimenType = regimen.type;
			
			if( doseGroup.startDateEnabled == true && doseGroup.checkStartDate(orderEditViewInfo.sysMsgMap, true) ) {					
				return;														
			} else {		
				
				doseGroup.startDateEnabled = showDateFieldFlag;
				if( regimenType == RegimenType.Daily ) {
					doseGroup.endDateEnabled = showDateFieldFlag;
				} 
				else {
					if ( doseGroup.checkEndDate(orderEditViewInfo.sysMsgMap, true) ) {
						return;
					} else {
						doseGroup.endDateEnabled = false;
					}
				}
				
				if ( showDateFieldFlag ) {
					regimen.determineEndDate(orderEditViewInfo.dmRegimenDictionary);						
				} else {
					regimen.determineDuration(orderEditViewInfo.dmRegimenDictionary);
				}
			}
		}
		
		public static function toggleDurationDateFieldFlag(orderEditViewInfo:OrderEditViewInfo):void
		{
			if ( orderEditViewInfo.isMoiEditFlag ) {
				orderEditViewInfo.moDurationDateFieldFlag = !orderEditViewInfo.moDurationDateFieldFlag;
			} else {
				orderEditViewInfo.poDurationDateFieldFlag = !orderEditViewInfo.poDurationDateFieldFlag;
			}
		}
		
		public static function durationClickHandler(orderEditViewInfo:OrderEditViewInfo):void
		{				
			toggleDurationDateFieldFlag(orderEditViewInfo);
			
			var orderEditView:OrderEditView = orderEditViewInfo.orderEditView;
			var showDateFieldFlag:Boolean = orderEditViewInfo.getDurationDateFieldFlag();
			var medOrderItem:MedOrderItem = orderEditViewInfo.medOrderItem;
			
			orderEditView.manualErrorString = null;			
			if( orderEditViewInfo.isMoiEditFlag ) {
				triggerStartAndEndDateFieldInput(
					orderEditViewInfo, 
					medOrderItem.regimen, 
					showDateFieldFlag
				);
				
			} else {
				for each( var poi:PharmOrderItem in medOrderItem.pharmOrderItemList ) {
					triggerStartAndEndDateFieldInput(
						orderEditViewInfo, 
						poi.regimen, 
						showDateFieldFlag 
					);
				}
			}
		}	
		
		public static function dailyFreqFilterFunction(item:Object):Boolean
		{				
			return DmDailyFrequency(item).dailyFreqCode != "00039";
		}
		
		public static function capdDailyFreqFilterFunction(item:Object):Boolean
		{
			return DmDailyFrequency(item).dailyFreqCode == "00001" || DmDailyFrequency(item).dailyFreqCode == "00035";
		}
		
		public static function checkErrorFocusToUiComponent(orderEditViewInfo:OrderEditViewInfo, uiComponent:UIComponent, errorString:String):void
		{
			if (uiComponent.visible) {
				if (uiComponent.enabled) {
					if (errorString && errorString != "") {
						uiComponent.setFocus();						
						orderEditViewInfo.hasFocusFlag = true;
						orderEditViewInfo.orderEditView.manualErrorString = errorString;
					}
				}
			}
		}
		
		private static function findIvAdditiveByDisplayName(targetDisplayName:String, ivAdditiveList:ListCollectionView):IvAdditive {
			if (ivAdditiveList) {
				for each (var ivAdditive:IvAdditive in ivAdditiveList) {
					if (targetDisplayName == ivAdditive.displayName) {
						return ivAdditive;
					}
				}
			}
			return null;
		}
		
		private static function findIvFluidByDiluentCode(targetDiluentCode:String, ivFluidList:ListCollectionView):IvFluid {
			if (ivFluidList) {
				for each (var ivFluid:IvFluid in ivFluidList) {
					if (targetDiluentCode == ivFluid.diluentCode) {
						return ivFluid;
					}
				}
			}
			return null;
		}
		
		public static function updateFmAndActionStatus(rxItem:RxItem, poi:*):void
		{
			var ivRxDrug:IvRxDrug;
			var injectionRxDrug:InjectionRxDrug;
			var ivAdditive:IvAdditive;
			var ivFluid:IvFluid;
			var dmDrugLite:DmDrugLite = poi.dmDrugLite as DmDrugLite;
			var displayName:String = dmDrugLite.displayname;
			var diluentCode:String = dmDrugLite.diluentCode;
			if (rxItem.isIvRxDrug) {
				ivRxDrug = IvRxDrug(rxItem);
				ivAdditive = findIvAdditiveByDisplayName(displayName, ivRxDrug.ivAdditiveList);
				ivFluid	= findIvFluidByDiluentCode(diluentCode, ivRxDrug.ivFluidList);
				if (ivAdditive) {
					poi.fmStatus = ivAdditive.fmStatus;
					poi.actionStatus = ivAdditive.actionStatus;
				} else if (ivFluid) {
					poi.fmStatus = null;//ivFluid has no fmStatus yet
					poi.actionStatus = ivFluid.actionStatus;
				} else {
					if (ivRxDrug.ivAdditiveList && ivRxDrug.ivAdditiveList.length > 0) {
						poi.fmStatus = IvAdditive(ivRxDrug.ivAdditiveList.getItemAt(0)).fmStatus;
						poi.actionStatus = IvAdditive(ivRxDrug.ivAdditiveList.getItemAt(0)).actionStatus;
					} else {
						poi.fmStatus = null;//ivFluid has no fmStatus yet
						poi.actionStatus = IvFluid(ivRxDrug.ivFluidList.getItemAt(0)).actionStatus;
					}
				}
			} 
			else if (rxItem.isInjectionRxDrug) { 
				injectionRxDrug = InjectionRxDrug(rxItem);					
				if (injectionRxDrug.displayName == displayName) {
					poi.fmStatus	 = injectionRxDrug.fmStatus;
					poi.actionStatus = injectionRxDrug.actionStatus;
				} else {
					var doseFluid:DoseFluid;
					var regimen:Regimen = injectionRxDrug.regimen;
					
					poi.actionStatus = injectionRxDrug.actionStatus;//default value from injectionRxDrug
					poi.fmStatus	 = injectionRxDrug.fmStatus;//default value from injectionRxDrug
					
					for each (var dose:Dose in regimen.getFullDoseList()) {
						doseFluid = dose.doseFluid;
						if (doseFluid && doseFluid.diluentCode != null && doseFluid.diluentCode == diluentCode) {							
							poi.actionStatus = injectionRxDrug.regimen.firstDose.doseFluid.actionStatus;
							poi.fmStatus	 = null;//doseFluid has no fmStatus yet
						}
					}
				}
			}
			else {
				poi.fmStatus	 = RxDrug(rxItem).fmStatus;
				poi.actionStatus = RxDrug(rxItem).actionStatus;
			}
		}
		
		public static function postItemCodeKeyUpHandlerForMp(ctx:Context, rxItem:RxItem, manualProfileFlag:Boolean, callBackFunc:Function):void {
			var okHandler:Function = function(evt:Event):void {
				var popup:MpOrderEditItemPopup = UIComponent(evt.target).parentDocument as MpOrderEditItemPopup;
				var drug:WorkstoreDrug = null;
				if( popup.itemListGrid.selectedIndex != -1 ) {
					drug = popup.itemListGrid.selectedItem as WorkstoreDrug;
				} else if ( popup.drugDiluentListGrid.selectedIndex != -1 ) {
					drug = popup.drugDiluentListGrid.selectedItem as WorkstoreDrug;
				} else if ( popup.solventListGrid.selectedIndex != -1 ) {
					drug = popup.solventListGrid.selectedItem as WorkstoreDrug;
				}
				
				if(drug != null && drug.msWorkstoreDrug.suspend != 'Y') {
					if (manualProfileFlag) {
						callBackFunc(drug.dmDrug.itemCode);
					} else {
						callBackFunc(drug);
					}
					PopUpManager.removePopUp(popup);
				}
			}																	
			var retrieveEvent:Event = new RetrieveWorkstoreDrugListByRxItemEvent(rxItem, false);
			var itemPopupEvent:Event = new ShowMpOrderEditItemPopupEvent(rxItem, okHandler, manualProfileFlag, retrieveEvent);										
			ctx.dispatchEvent(itemPopupEvent);
		}
		
		public static function checkIsParenteralSite(rxItem:RxItem, dmSiteParenteralMap:PropMap):Boolean {
			if (rxItem.isIvRxDrug) {	
				var ivRxDrug:IvRxDrug = rxItem as IvRxDrug;
				if ( ivRxDrug.siteCode ) {
					return dmSiteParenteralMap.getValueAsBoolean(
						ivRxDrug.siteCode
					);
				} else {
					return true;
				}
			} else if (rxItem.isCapdRxDrug) { 
				return false;
			} else {
				return dmSiteParenteralMap.getValueAsBoolean(
					RxDrug(rxItem).regimen.firstDose.siteCode
				);						
			}
		}
		
		private static function createMpSite(dose:Dose):Site {				
			if (dose.dmSupplSite) {
				return createMpSupplSite(dose);
			} else{
				return createMpPrincipalSite(dose);
			}
		}						
		
		private static function createMpPrincipalSite(dose:Dose):Site {
			var site:Site 		= new Site;				
			site.siteCode 		= dose.siteCode;								
			site.supplSiteDesc 	= null;
			site.isSupplSite 	= false;
			
			site.siteCategoryCode = dose.dmSite.siteCategoryCode;
			site.siteDescEng = dose.dmSite.siteDescEng;
			site.ipmoeDesc = dose.dmSite.ipmoeDesc;
			
			if ( site.ipmoeDesc && site.ipmoeDesc != "" ) {
				site.siteLabelDesc  = dose.dmSite.ipmoeDesc;
			} else {
				site.siteLabelDesc  = dose.dmSite.siteDescEng;
			}			
			
			return site;
		}
		
		private static function createMpSupplSite(dose:Dose):Site {
			var site:Site 		= new Site;															
			site.siteCode 		= dose.siteCode;
			site.supplSiteDesc 	= dose.supplSiteDesc;
			site.siteLabelDesc 	= dose.supplSiteDesc;	
			
			site.siteCategoryCode = dose.dmSite.siteCategoryCode;
			site.siteDescEng = dose.dmSite.siteDescEng;
			site.ipmoeDesc = dose.dmSite.ipmoeDesc;
			
			site.isSupplSite 	= true;										
			return site;
		}
		
		public static function assignParenteralSiteToDose(dmSiteDictionary:Dictionary, dmSupplSiteDictionary:Dictionary, rxItem:RxItem, dose:Dose):void
		{
			if ( rxItem.isIvRxDrug ) {					
				dose.siteCode		= IvRxDrug(rxItem).siteCode;
				dose.siteDesc		= IvRxDrug(rxItem).siteDesc;
				dose.supplSiteDesc  = null;
				if ( dose.siteCode != null ) {
					dose.dmSite			= dmSiteDictionary[dose.siteCode];
				}
			} else {
				dose.siteCode	    = RxDrug(rxItem).regimen.firstDose.siteCode;
				dose.siteDesc		= RxDrug(rxItem).regimen.firstDose.siteDesc;
				dose.supplSiteDesc  = RxDrug(rxItem).regimen.firstDose.supplSiteDesc;
				if ( dose.siteCode != null ) {
					dose.dmSite			= dmSiteDictionary[dose.siteCode];
					dose.dmSupplSite	= dmSupplSiteDictionary[dose.siteCode+dose.supplSiteDesc];
				}
			}
			dose.siteList = new ArrayCollection;					
			dose.siteList.addItem( createMpSite(dose) );
		}
		
		public static function invokePopupOnce(funcValue:Function, timeLapse:Number=1500, ...parameters):void {
			var timeDiff:Number=9999999;
			if (lastPopupDateTime != null) {
				timeDiff = (new Date).time - lastPopupDateTime.time;
				lastPopupDateTime = DateUtil.addTime(lastPopupDateTime, timeDiff);
			} else {
				lastPopupDateTime = new Date;				
			}
			if (timeDiff > timeLapse) {
				if (parameters.length == 0) {
					funcValue.call(funcValue);
				}
				else {
					funcValue.call(funcValue, parameters);
				}
			}
		}
		
		private static function getDmAdminTimeIndex(dmAdminTimeList:ListCollectionView, dmAdminTimeLite:DmAdminTimeLite):int {
			var i:int = 0;
			for each (var adminTime:DmAdminTime in dmAdminTimeList) {
				if (adminTime.adminTimeCode == dmAdminTimeLite.adminTimeCode) {
					return i;
				}
				++i;
			}
			return dmAdminTimeList.length;
		}
		
		public static function mealTimeSelection(adminTimeLite:DmAdminTimeLite, context:Context):void
		{			
			var index:int = FlexGlobals.topLevelApplication.dmAdminTimeList.length;
			if ( adminTimeLite ) {
				if ( adminTimeLite.adminTimeCode != "-1") {				
					index = getDmAdminTimeIndex(FlexGlobals.topLevelApplication.dmAdminTimeList, adminTimeLite);
				}					
			} 
			(context.lookupPopup.lookupGrid as DataGrid).selectedIndex = index;
			(context.lookupPopup.lookupGrid as DataGrid).scrollToIndex(index);
		}				
	}
}