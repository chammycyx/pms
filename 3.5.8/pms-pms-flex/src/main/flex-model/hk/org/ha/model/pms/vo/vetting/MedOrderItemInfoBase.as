/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (MedOrderItemInfo.as).
 */

package hk.org.ha.model.pms.vo.vetting {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
    import org.granite.util.Enum;

    [Bindable]
    public class MedOrderItemInfoBase implements IExternalizable {

        private var _itemNum:Number;
        private var _numOfLabel:Number;
        private var _showExemptCodeFlag:Boolean;
        private var _showWarnCodeFlag:Boolean;
        private var _status:MedOrderItemStatus;
        private var _vetFlag:Boolean;

        public function set itemNum(value:Number):void {
            _itemNum = value;
        }
        public function get itemNum():Number {
            return _itemNum;
        }

        public function set numOfLabel(value:Number):void {
            _numOfLabel = value;
        }
        public function get numOfLabel():Number {
            return _numOfLabel;
        }

        public function set showExemptCodeFlag(value:Boolean):void {
            _showExemptCodeFlag = value;
        }
        public function get showExemptCodeFlag():Boolean {
            return _showExemptCodeFlag;
        }

        public function set showWarnCodeFlag(value:Boolean):void {
            _showWarnCodeFlag = value;
        }
        public function get showWarnCodeFlag():Boolean {
            return _showWarnCodeFlag;
        }

        public function set status(value:MedOrderItemStatus):void {
            _status = value;
        }
        public function get status():MedOrderItemStatus {
            return _status;
        }

        public function set vetFlag(value:Boolean):void {
            _vetFlag = value;
        }
        public function get vetFlag():Boolean {
            return _vetFlag;
        }

        public function readExternal(input:IDataInput):void {
            _itemNum = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            _numOfLabel = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            _showExemptCodeFlag = input.readObject() as Boolean;
            _showWarnCodeFlag = input.readObject() as Boolean;
            _status = Enum.readEnum(input) as MedOrderItemStatus;
            _vetFlag = input.readObject() as Boolean;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_itemNum);
            output.writeObject(_numOfLabel);
            output.writeObject(_showExemptCodeFlag);
            output.writeObject(_showWarnCodeFlag);
            output.writeObject(_status);
            output.writeObject(_vetFlag);
        }
    }
}