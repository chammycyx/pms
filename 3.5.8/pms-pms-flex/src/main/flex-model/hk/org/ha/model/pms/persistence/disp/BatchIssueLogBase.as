/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (BatchIssueLog.as).
 */

package hk.org.ha.model.pms.persistence.disp {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.getQualifiedClassName;
    import hk.org.ha.model.pms.persistence.VersionEntity;
    import hk.org.ha.model.pms.persistence.corp.Workstore;
    import hk.org.ha.model.pms.persistence.reftable.Workstation;
    import mx.collections.ListCollectionView;
    import mx.core.IUID;
    import mx.data.utils.Managed;
    import mx.utils.UIDUtil;
    import org.granite.collections.IPersistentCollection;
    import org.granite.meta;
    import org.granite.tide.IEntityManager;
    import org.granite.tide.IPropertyHolder;

    use namespace meta;

    [Managed]
    public class BatchIssueLogBase extends VersionEntity {

        [Transient]
        meta var entityManager:IEntityManager = null;

        private var __initialized:Boolean = true;
        private var __detachedState:String = null;

        private var _batchIssueLogDetailList:ListCollectionView;
        private var _errorCount:Number;
        private var _id:Number;
        private var _issueCount:Number;
        private var _issueDate:Date;
        private var _updateWorkstation:Workstation;
        private var _workstore:Workstore;

        meta override function isInitialized(name:String = null):Boolean {
            if (!name)
                return __initialized;

            var property:* = this[name];
            return (
                (!(property is BatchIssueLog) || (property as BatchIssueLog).meta::isInitialized()) &&
                (!(property is IPersistentCollection) || (property as IPersistentCollection).isInitialized())
            );
        }
        
        [Bindable(event="dirtyChange")]
		public function get meta_dirty():Boolean {
			return Managed.getProperty(this, "meta_dirty", false);
		}

        public function set batchIssueLogDetailList(value:ListCollectionView):void {
            _batchIssueLogDetailList = value;
        }
        public function get batchIssueLogDetailList():ListCollectionView {
            return _batchIssueLogDetailList;
        }

        public function set errorCount(value:Number):void {
            _errorCount = value;
        }
        public function get errorCount():Number {
            return _errorCount;
        }

        public function set id(value:Number):void {
            _id = value;
        }
        [Id]
        public function get id():Number {
            return _id;
        }

        public function set issueCount(value:Number):void {
            _issueCount = value;
        }
        public function get issueCount():Number {
            return _issueCount;
        }

        public function set issueDate(value:Date):void {
            _issueDate = value;
        }
        public function get issueDate():Date {
            return _issueDate;
        }

        public function set updateWorkstation(value:Workstation):void {
            _updateWorkstation = value;
        }
        public function get updateWorkstation():Workstation {
            return _updateWorkstation;
        }

        public function set workstore(value:Workstore):void {
            _workstore = value;
        }
        public function get workstore():Workstore {
            return _workstore;
        }

        public function set uid(value:String):void {
            // noop...
        }
        public function get uid():String {
            if (isNaN(_id))
                return UIDUtil.createUID();
            return getQualifiedClassName(this) + "#[" + String(_id) + "]";
        }

        meta override function merge(em:IEntityManager, obj:*):void {
            var src:BatchIssueLogBase = BatchIssueLogBase(obj);
            __initialized = src.__initialized;
            __detachedState = src.__detachedState;
            if (meta::isInitialized()) {
                super.meta::merge(em, obj);
               em.meta_mergeExternal(src._batchIssueLogDetailList, _batchIssueLogDetailList, null, this, 'batchIssueLogDetailList', function setter(o:*):void{_batchIssueLogDetailList = o as ListCollectionView}, false);
               em.meta_mergeExternal(src._errorCount, _errorCount, null, this, 'errorCount', function setter(o:*):void{_errorCount = o as Number}, false);
               em.meta_mergeExternal(src._id, _id, null, this, 'id', function setter(o:*):void{_id = o as Number}, false);
               em.meta_mergeExternal(src._issueCount, _issueCount, null, this, 'issueCount', function setter(o:*):void{_issueCount = o as Number}, false);
               em.meta_mergeExternal(src._issueDate, _issueDate, null, this, 'issueDate', function setter(o:*):void{_issueDate = o as Date}, false);
               em.meta_mergeExternal(src._updateWorkstation, _updateWorkstation, null, this, 'updateWorkstation', function setter(o:*):void{_updateWorkstation = o as Workstation}, false);
               em.meta_mergeExternal(src._workstore, _workstore, null, this, 'workstore', function setter(o:*):void{_workstore = o as Workstore}, false);
            }
            else {
               em.meta_mergeExternal(src._id, _id, null, this, 'id', function setter(o:*):void{_id = o as Number});
            }
        }

        public override function readExternal(input:IDataInput):void {
            __initialized = input.readObject() as Boolean;
            __detachedState = input.readObject() as String;
            if (meta::isInitialized()) {
                super.readExternal(input);
                _batchIssueLogDetailList = input.readObject() as ListCollectionView;
                _errorCount = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _id = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _issueCount = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _issueDate = input.readObject() as Date;
                _updateWorkstation = input.readObject() as Workstation;
                _workstore = input.readObject() as Workstore;
            }
            else {
                _id = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            }
        }

        public override function writeExternal(output:IDataOutput):void {
            output.writeObject(__initialized);
            output.writeObject(__detachedState);
            if (meta::isInitialized()) {
                super.writeExternal(output);
                output.writeObject((_batchIssueLogDetailList is IPropertyHolder) ? IPropertyHolder(_batchIssueLogDetailList).object : _batchIssueLogDetailList);
                output.writeObject((_errorCount is IPropertyHolder) ? IPropertyHolder(_errorCount).object : _errorCount);
                output.writeObject((_id is IPropertyHolder) ? IPropertyHolder(_id).object : _id);
                output.writeObject((_issueCount is IPropertyHolder) ? IPropertyHolder(_issueCount).object : _issueCount);
                output.writeObject((_issueDate is IPropertyHolder) ? IPropertyHolder(_issueDate).object : _issueDate);
                output.writeObject((_updateWorkstation is IPropertyHolder) ? IPropertyHolder(_updateWorkstation).object : _updateWorkstation);
                output.writeObject((_workstore is IPropertyHolder) ? IPropertyHolder(_workstore).object : _workstore);
            }
            else {
                output.writeObject(_id);
            }
        }
    }
}
