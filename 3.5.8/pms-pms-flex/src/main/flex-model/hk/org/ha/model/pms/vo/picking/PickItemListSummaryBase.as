/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (PickItemListSummary.as).
 */

package hk.org.ha.model.pms.vo.picking {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import mx.collections.ListCollectionView;

    [Bindable]
    public class PickItemListSummaryBase implements IExternalizable {

        private var _pendingItemList:ListCollectionView;
        private var _pickedItemList:ListCollectionView;

        public function set pendingItemList(value:ListCollectionView):void {
            _pendingItemList = value;
        }
        public function get pendingItemList():ListCollectionView {
            return _pendingItemList;
        }

        public function set pickedItemList(value:ListCollectionView):void {
            _pickedItemList = value;
        }
        public function get pickedItemList():ListCollectionView {
            return _pickedItemList;
        }

        public function readExternal(input:IDataInput):void {
            _pendingItemList = input.readObject() as ListCollectionView;
            _pickedItemList = input.readObject() as ListCollectionView;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_pendingItemList);
            output.writeObject(_pickedItemList);
        }
    }
}