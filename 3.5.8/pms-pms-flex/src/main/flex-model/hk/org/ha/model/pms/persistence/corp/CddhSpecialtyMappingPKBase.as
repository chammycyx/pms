/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (CddhSpecialtyMappingPK.as).
 */

package hk.org.ha.model.pms.persistence.corp {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;

    [Bindable]
    public class CddhSpecialtyMappingPKBase implements IExternalizable {

        private var _hospCode:String;
        private var _specCode:String;

        public function set hospCode(value:String):void {
            _hospCode = value;
        }
        public function get hospCode():String {
            return _hospCode;
        }

        public function set specCode(value:String):void {
            _specCode = value;
        }
        public function get specCode():String {
            return _specCode;
        }

        public function readExternal(input:IDataInput):void {
            _hospCode = input.readObject() as String;
            _specCode = input.readObject() as String;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_hospCode);
            output.writeObject(_specCode);
        }
    }
}