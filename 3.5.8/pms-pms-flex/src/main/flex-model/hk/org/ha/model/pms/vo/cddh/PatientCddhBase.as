/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (PatientCddh.as).
 */

package hk.org.ha.model.pms.vo.cddh {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import mx.collections.ListCollectionView;

    [Bindable]
    public class PatientCddhBase implements IExternalizable {

        private var _patientCddhPharmItemList:ListCollectionView;

        public function set patientCddhPharmItemList(value:ListCollectionView):void {
            _patientCddhPharmItemList = value;
        }
        public function get patientCddhPharmItemList():ListCollectionView {
            return _patientCddhPharmItemList;
        }

        public function readExternal(input:IDataInput):void {
            _patientCddhPharmItemList = input.readObject() as ListCollectionView;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_patientCddhPharmItemList);
        }
    }
}