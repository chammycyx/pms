/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (PreVetInfo.as).
 */

package hk.org.ha.model.pms.vo.prevet {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;

    [Bindable]
    public class PreVetInfoBase implements IExternalizable {

        private var _itemNum:Number;
        private var _preVetNoteDate:Date;
        private var _preVetNoteText:String;
        private var _preVetNoteUser:String;

        public function set itemNum(value:Number):void {
            _itemNum = value;
        }
        public function get itemNum():Number {
            return _itemNum;
        }

        public function set preVetNoteDate(value:Date):void {
            _preVetNoteDate = value;
        }
        public function get preVetNoteDate():Date {
            return _preVetNoteDate;
        }

        public function set preVetNoteText(value:String):void {
            _preVetNoteText = value;
        }
        public function get preVetNoteText():String {
            return _preVetNoteText;
        }

        public function set preVetNoteUser(value:String):void {
            _preVetNoteUser = value;
        }
        public function get preVetNoteUser():String {
            return _preVetNoteUser;
        }

        public function readExternal(input:IDataInput):void {
            _itemNum = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            _preVetNoteDate = input.readObject() as Date;
            _preVetNoteText = input.readObject() as String;
            _preVetNoteUser = input.readObject() as String;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_itemNum);
            output.writeObject(_preVetNoteDate);
            output.writeObject(_preVetNoteText);
            output.writeObject(_preVetNoteUser);
        }
    }
}