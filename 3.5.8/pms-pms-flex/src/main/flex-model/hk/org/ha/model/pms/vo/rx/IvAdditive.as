/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * NOTE: this file is only generated if it does not exist. You may safely put
 * your custom code here.
 */

package hk.org.ha.model.pms.vo.rx {
	import hk.org.ha.fmk.pms.flex.utils.WordUtil;
	
	import mx.utils.StringUtil;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.vo.rx.IvAdditive")]
    public class IvAdditive extends IvAdditiveBase {

		override public function get displayNameFormSalt():String {
			
			var desc:String = "";
			
			// MOE display name
			if (displayName != null) {
				desc += WordUtil.capitalize(displayName);
			}
			
			// Salt / special property
			if (saltProperty != null && StringUtil.trim(saltProperty) != "") {
				desc += " " + WordUtil.capitalize(saltProperty);
			}
			
			// Form description
			if (formDesc != null) {
				desc += " " + StringUtil.trim(formDesc).toLowerCase();
			}
			
			return desc;
		}

	}
}