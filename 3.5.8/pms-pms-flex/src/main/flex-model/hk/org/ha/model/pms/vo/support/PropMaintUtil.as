package hk.org.ha.model.pms.vo.support {
	
	import mx.events.ValidationResultEvent;
	import mx.utils.StringUtil;
	import mx.validators.DateValidator;
	
	import hk.org.ha.model.pms.udt.DataType;
	import hk.org.ha.model.pms.udt.YesNoFlag;
	
	[Bindable]
	public class PropMaintUtil {
		
		public function PropMaintUtil():void
		{			
			super();			
		}

		public static function validateRuleValue(ruleValue:String, dataType:DataType, validation:String, errorString:String, isSupportMaint:Boolean):Object 
		{
			var result:Object = new Object;
			result.valid = true;
			result.errorCode = "";
			result.errorMsgParm = null;
			
			if ( PropMaintUtil.isNumericInput(dataType) ) {
				var validInput:Boolean = true;
				
				if ( isNaN((Number(ruleValue))) ) {
					validInput = false;
				} else {
					var validRange:Boolean = false;
					
					for each (var rangeStr:String in validation.split(',')) {
						if (rangeStr.split('-').length == 1) {
							if (StringUtil.trim(ruleValue) == "" && rangeStr == "") {
								validRange = true;
							} else if ( Number(ruleValue) == Number(rangeStr) ) {
								validRange = true;
							}
						} else if ( Number(ruleValue) >= Number(rangeStr.split('-')[0]) && Number(ruleValue) <= Number(rangeStr.split('-')[1]) ) {
							validRange = true;
						}
						if (validRange) {
							break;
						}
					}
					if ( ! validRange) {
						validInput = false;
					}
				}
				
				if ( ! validInput) {
					var errorMsg:String = "";
					for each (var rangeStr:String in validation.split(',')) {
						if (errorMsg.length > 0) {
							errorMsg += " or "
						}
						if (rangeStr.split('-').length == 1) {
							if (rangeStr == "") {
								errorMsg += "[empty]";
							} else {
								errorMsg += "[" + rangeStr + "]";
							}
						} else {
							errorMsg += "from [" + rangeStr.split('-')[0] + "] to [" + rangeStr.split('-')[1] + "]";
						}
						
					}
					
					result.valid = false;
					result.errorCode = "0250"
					result.errorMsgParm = new Array(errorMsg);
				}
				
			} else if ( dataType == DataType.StringType && validation.split('-').length > 1 ) {
				if ( (ruleValue == null) && (validation.split('-')[0] > 0) ) 
				{
					result.valid = false;
					result.errorCode = isSupportMaint?"0488":"0483";
				} 
				else if ( (ruleValue.length < validation.split('-')[0]) || 
					(ruleValue.length > validation.split('-')[1]) ) 
				{
					result.valid = false;
					result.errorCode = isSupportMaint?"0488":"0483";
				} 
			} 
			else if ( dataType == DataType.TextAreaType ) 
			{
				if ( errorString != null && errorString != "" ) {
					result.valid = false;
					result.errorCode = errorString;
				}
			} 
			else if ( dataType == DataType.DateTimeType) 
			{
				if ( errorString == null || errorString == "" )
				{
					if ( !isValidDateTime(ruleValue, validation) ) 
					{
						result.valid = false;
						result.errorCode = "0034";
						result.errorMsgParm = new Array([validation]);
					}
				} 
				else 
				{
					result.valid = false;
				}
			} 
			return result;
		}
		
		private static function isValidDateTime(value:String, format:String):Boolean {
			var yrIndex:int = format.indexOf("yyyy");
			var monthIndex:int = format.indexOf("MM");
			var dayIndex:int = format.indexOf("dd");
			var hourIndex:int = format.indexOf("HH");
			var minIndex:int = format.indexOf("mm");
			var secondIndex:int = format.indexOf("ss");
			var milliSecondIndex:int = format.indexOf("SSS");
			
			var date:String = value.substr(yrIndex, 4)+value.substr(monthIndex, 2)+value.substr(dayIndex, 2);
			if ( !isValidDate(date) ) {
				return false;
			} 
			
			if ( hourIndex >=0 ) {
				if ( !isValidHour(Number(value.substr(hourIndex, 2))) ) {
					return false;
				}
			}
			
			if ( minIndex >= 0 ) {
				if ( !isValidMinSec(Number(value.substr(minIndex, 2))) ) {
					return false;
				}
			}
			
			if ( secondIndex >= 0 ) {
				if ( !isValidMinSec(Number(value.substr(secondIndex, 2))) ) {
					return false;
				}
			}
			
			if ( milliSecondIndex >= 0 ) {
				if ( !isValidMilliSec(Number(value.substr(milliSecondIndex, 3))) ) {
					return false;
				}
			}
			return true;
		}
		
		private static function isValidHour(value:Number):Boolean {
			return ( value >= 0 && value <=23);
		}
		
		private static function isValidMinSec(value:Number):Boolean {
			return ( value >= 0 && value <=59);
		}
		
		private static function isValidMilliSec(value:Number):Boolean {
			return ( value >= 0 && value <=999);
		}
		
		private static function isValidDate(date:String):Boolean {
			var dateValidator:DateValidator = new DateValidator();
			dateValidator.inputFormat = "yyyymmdd";
			
			if ( dateValidator.validate(date).type ==  ValidationResultEvent.INVALID ) {
				return false;
			}
			return true;
		}
		
		public static function calculateTextAreaProp(validation:String):Object 
		{
			var prop:Object = new Object();
			
			var array:Array = validation.split(',');
			prop.maxRow = array[0].toString().substring(1);
			prop.maxChar = array[1].toString().substring(1);
			prop.height = prop.maxRow * 15 + 5;
			
			return prop;
		}
		
		public static function getDisplayValue(value:String, type:DataType):String {
			if ( type == DataType.BooleanType ) { 
				return YesNoFlag.dataValueOf(value).displayValue;
			} 
			return value;
		}
		
		public static function isNumericInput(type:DataType):Boolean {
			return (type == DataType.DayType || 
				type == DataType.HourType ||
				type == DataType.IntegerType || 
				type == DataType.MinuteType || 
				type == DataType.MonthType ||
				type == DataType.DoubleType ||
				type == DataType.WeekType ||
				type == DataType.YearType || 
				type == DataType.SecondType || 
				type == DataType.MillisecondType || 
				type == DataType.Hour1To24Type || 
				type == DataType.DayOfWeekType);
		}
	}
}