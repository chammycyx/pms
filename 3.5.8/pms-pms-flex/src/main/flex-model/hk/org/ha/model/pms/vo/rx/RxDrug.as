/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * NOTE: this file is only generated if it does not exist. You may safely put
 * your custom code here.
 */

package hk.org.ha.model.pms.vo.rx {
	import hk.org.ha.fmk.pms.flex.utils.WordUtil;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItemFm;
	import hk.org.ha.model.pms.udt.vetting.FmStatus;
	import hk.org.ha.model.pms.udt.vetting.NameType;
	
	import mx.utils.StringUtil;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.vo.rx.RxDrug")]
    public class RxDrug extends RxDrugBase {

		private var _medProfileMoItemFm:MedProfileMoItemFm;
		
		private var _gcnSeqNum:String;
		
		private var _rdfgenId:String;
		
		private var _rgenId:String;
		
		public function set medProfileMoItemFm(medProfileMoItemFm:MedProfileMoItemFm):void {
			_medProfileMoItemFm = medProfileMoItemFm;
		}
		
		public function get medProfileMoItemFm():MedProfileMoItemFm {
			return _medProfileMoItemFm;
		}
		
		public function set gcnSeqNum(gcnSeqNum:String):void {
			_gcnSeqNum = gcnSeqNum;
		}
		
		public function get gcnSeqNum():String {
			return _gcnSeqNum;
		}
		
		public function set rdfgenId(rdfgenId:String):void {
			_rdfgenId = rdfgenId;
		}
		
		public function get rdfgenId():String {
			return _rdfgenId;
		}
		
		public function set rgenId(rgenId:String):void {
			_rgenId = rgenId;
		}
		
		public function get rgenId():String {
			return _rgenId;
		}
		
		private function getDisplayNameSalt():String {
			var desc:String = "";
			
			var isIncludeAlias:Boolean = false;
			
			if (aliasName != null && StringUtil.trim(aliasName) != "" && nameType == NameType.TradeName) {
				isIncludeAlias = true;
			}
			
			// alias name
			if (isIncludeAlias) {
				desc += WordUtil.capitalize(aliasName) + " (";
			}
			
			// MOE display name
			if (displayName != null) {
				desc += WordUtil.capitalize(displayName);
			}
			
			// Salt / special property
			if (saltProperty != null && StringUtil.trim(saltProperty) != "") {
				desc += " " + WordUtil.capitalize(saltProperty);
			}
			
			if (isIncludeAlias) {
				desc += ")";
			}
			
			return desc;
		}
		
		private function getDisplayNameFormSalt(isNormalDescFlag:Boolean):String {
			var desc:String = isNormalDescFlag?getDisplayNameSalt():getDisplayNameSalt().toUpperCase();
			
			// Form description
			if (formDesc != null) {
				desc += " " + StringUtil.trim(formDesc).toLowerCase();
			}
			else if ( dmFormLite != null ) {
				desc += " " + StringUtil.trim(dmFormLite.labelDesc).toLowerCase();
			}
			
			return desc;
		}
		
		public function get displayNameFormSalt():String {
			return getDisplayNameFormSalt(true);
		}
		
		public function get strengthVolume():String {
			
			var desc:String = "";
			
			// Strength
			if ( strength != null ) {
				desc += " " + strength.toLowerCase();
			}
			
			// Volume value & Volume unit
			var volumnStr:String = "";
			if ( !isNaN(volume) && volume > 0 ) {
				volumnStr += volume;
				if (volumeUnit != null) {
					volumnStr += volumeUnit;
				}
				desc += " " + volumnStr.toLowerCase();
			}
			
			if ( extraInfo != null && (StringUtil.trim(aliasName) == "" || extraInfo != aliasName) ) {
				desc += " (" + extraInfo.toLowerCase() + ")";
			}
			
			return desc;
		}
		
		private function getMoeDrugDesc(isNormalDescFlag:Boolean):String {
			// Medical Line Display Name
			var desc:String = "";
			desc += getDisplayNameFormSalt(isNormalDescFlag);
			desc += strengthVolume;
			
			if (fmStatus != null) {
				var tempFmStatus:FmStatus = FmStatus.dataValueOf(fmStatus);
				if (tempFmStatus != FmStatus.GeneralDrug && tempFmStatus != FmStatus.FreeTextEntryItem) {
					desc += " <" + tempFmStatus.displayValue + "> ";
				}
			}
			return desc;
		}
		
		public function get moeDrugDesc():String {
			return getMoeDrugDesc(true);
		}
				
		public function get moeDrugDescForDrugSet():String {
			return getMoeDrugDesc(false);
		}
    }
}