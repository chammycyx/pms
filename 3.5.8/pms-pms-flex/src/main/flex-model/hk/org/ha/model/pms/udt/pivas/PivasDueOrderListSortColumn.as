/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.udt.pivas {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.udt.pivas.PivasDueOrderListSortColumn")]
    public class PivasDueOrderListSortColumn extends Enum {

        public static const DueDate:PivasDueOrderListSortColumn = new PivasDueOrderListSortColumn("DueDate", _, "1", "Due Date");
        public static const PreparationDtlDueDate:PivasDueOrderListSortColumn = new PivasDueOrderListSortColumn("PreparationDtlDueDate", _, "2", "Preparation Details & Due Date");
        public static const PreparationDtlPhsWardBedNumPatName:PivasDueOrderListSortColumn = new PivasDueOrderListSortColumn("PreparationDtlPhsWardBedNumPatName", _, "3", "Preparation Details, PHS Ward, Bed & Patient Name");
        public static const PatNamePreparationDtl:PivasDueOrderListSortColumn = new PivasDueOrderListSortColumn("PatNamePreparationDtl", _, "4", "Patient Name & Preparation Details");
        public static const PhsWardBedNumPatNamePreparationDtl:PivasDueOrderListSortColumn = new PivasDueOrderListSortColumn("PhsWardBedNumPatNamePreparationDtl", _, "5", "PHS Ward, Bed, Patient Name & Preparation Details");

        function PivasDueOrderListSortColumn(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || DueDate.name), restrictor, (dataValue || DueDate.dataValue), (displayValue || DueDate.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [DueDate, PreparationDtlDueDate, PreparationDtlPhsWardBedNumPatName, PatNamePreparationDtl, PhsWardBedNumPatNamePreparationDtl];
        }

        public static function valueOf(name:String):PivasDueOrderListSortColumn {
            return PivasDueOrderListSortColumn(DueDate.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):PivasDueOrderListSortColumn {
            return PivasDueOrderListSortColumn(DueDate.dataConstantOf(dataValue));
        }
    }
}