/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.udt.onestop {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.udt.onestop.OneStopOrderType")]
    public class OneStopOrderType extends Enum {

        public static const ManualOrder:OneStopOrderType = new OneStopOrderType("ManualOrder", _, "MO", "Manual Order");
        public static const MedOrder:OneStopOrderType = new OneStopOrderType("MedOrder", _, "CO", "Medication Order");
        public static const DispOrder:OneStopOrderType = new OneStopOrderType("DispOrder", _, "DO", "Dispensing Order");
        public static const RefillOrder:OneStopOrderType = new OneStopOrderType("RefillOrder", _, "RO", "Refill Order");
        public static const RefillOrderReadOnly:OneStopOrderType = new OneStopOrderType("RefillOrderReadOnly", _, "RR", "Refill Order Read-only");
        public static const SfiOrder:OneStopOrderType = new OneStopOrderType("SfiOrder", _, "SO", "SFI Order");
        public static const DhOrder:OneStopOrderType = new OneStopOrderType("DhOrder", _, "DH", "DH Order");

        function OneStopOrderType(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || ManualOrder.name), restrictor, (dataValue || ManualOrder.dataValue), (displayValue || ManualOrder.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [ManualOrder, MedOrder, DispOrder, RefillOrder, RefillOrderReadOnly, SfiOrder, DhOrder];
        }

        public static function valueOf(name:String):OneStopOrderType {
            return OneStopOrderType(ManualOrder.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):OneStopOrderType {
            return OneStopOrderType(ManualOrder.dataConstantOf(dataValue));
        }
    }
}