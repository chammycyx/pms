/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (PivasMergeFormulaWorksheetRawMaterialDtl.as).
 */

package hk.org.ha.model.pms.vo.label {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;

    [Bindable]
    public class PivasMergeFormulaWorksheetRawMaterialDtlBase implements IExternalizable {

        private var _batchNum:String;
        private var _diluentDesc:String;
        private var _displayName:String;
        private var _drugKey:Number;
        private var _expiryDate:Date;
        private var _formCode:String;
        private var _itemCode:String;
        private var _itemDesc:String;
        private var _manufCode:String;
        private var _saltProperty:String;
        private var _solventDesc:String;
        private var _sortSeq:Number;
        private var _strength:String;
        private var _vendSupplySolvFlag:Boolean;
        private var _volumeUnit:String;
        private var _volumeValue:String;

        public function set batchNum(value:String):void {
            _batchNum = value;
        }
        public function get batchNum():String {
            return _batchNum;
        }

        public function set diluentDesc(value:String):void {
            _diluentDesc = value;
        }
        public function get diluentDesc():String {
            return _diluentDesc;
        }

        public function set displayName(value:String):void {
            _displayName = value;
        }
        public function get displayName():String {
            return _displayName;
        }

        public function set drugKey(value:Number):void {
            _drugKey = value;
        }
        public function get drugKey():Number {
            return _drugKey;
        }

        public function set expiryDate(value:Date):void {
            _expiryDate = value;
        }
        public function get expiryDate():Date {
            return _expiryDate;
        }

        public function set formCode(value:String):void {
            _formCode = value;
        }
        public function get formCode():String {
            return _formCode;
        }

        public function set itemCode(value:String):void {
            _itemCode = value;
        }
        public function get itemCode():String {
            return _itemCode;
        }

        public function set itemDesc(value:String):void {
            _itemDesc = value;
        }
        public function get itemDesc():String {
            return _itemDesc;
        }

        public function set manufCode(value:String):void {
            _manufCode = value;
        }
        public function get manufCode():String {
            return _manufCode;
        }

        public function set saltProperty(value:String):void {
            _saltProperty = value;
        }
        public function get saltProperty():String {
            return _saltProperty;
        }

        public function set solventDesc(value:String):void {
            _solventDesc = value;
        }
        public function get solventDesc():String {
            return _solventDesc;
        }

        public function set sortSeq(value:Number):void {
            _sortSeq = value;
        }
        public function get sortSeq():Number {
            return _sortSeq;
        }

        public function set strength(value:String):void {
            _strength = value;
        }
        public function get strength():String {
            return _strength;
        }

        public function set vendSupplySolvFlag(value:Boolean):void {
            _vendSupplySolvFlag = value;
        }
        public function get vendSupplySolvFlag():Boolean {
            return _vendSupplySolvFlag;
        }

        public function set volumeUnit(value:String):void {
            _volumeUnit = value;
        }
        public function get volumeUnit():String {
            return _volumeUnit;
        }

        public function set volumeValue(value:String):void {
            _volumeValue = value;
        }
        public function get volumeValue():String {
            return _volumeValue;
        }

        public function readExternal(input:IDataInput):void {
            _batchNum = input.readObject() as String;
            _diluentDesc = input.readObject() as String;
            _displayName = input.readObject() as String;
            _drugKey = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            _expiryDate = input.readObject() as Date;
            _formCode = input.readObject() as String;
            _itemCode = input.readObject() as String;
            _itemDesc = input.readObject() as String;
            _manufCode = input.readObject() as String;
            _saltProperty = input.readObject() as String;
            _solventDesc = input.readObject() as String;
            _sortSeq = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            _strength = input.readObject() as String;
            _vendSupplySolvFlag = input.readObject() as Boolean;
            _volumeUnit = input.readObject() as String;
            _volumeValue = input.readObject() as String;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_batchNum);
            output.writeObject(_diluentDesc);
            output.writeObject(_displayName);
            output.writeObject(_drugKey);
            output.writeObject(_expiryDate);
            output.writeObject(_formCode);
            output.writeObject(_itemCode);
            output.writeObject(_itemDesc);
            output.writeObject(_manufCode);
            output.writeObject(_saltProperty);
            output.writeObject(_solventDesc);
            output.writeObject(_sortSeq);
            output.writeObject(_strength);
            output.writeObject(_vendSupplySolvFlag);
            output.writeObject(_volumeUnit);
            output.writeObject(_volumeValue);
        }
    }
}