/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (DoseFluid.as).
 */

package hk.org.ha.model.pms.vo.rx {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;

    [Bindable]
    public class DoseFluidBase extends Fluid {


        public override function readExternal(input:IDataInput):void {
            super.readExternal(input);
        }

        public override function writeExternal(output:IDataOutput):void {
            super.writeExternal(output);
        }
    }
}