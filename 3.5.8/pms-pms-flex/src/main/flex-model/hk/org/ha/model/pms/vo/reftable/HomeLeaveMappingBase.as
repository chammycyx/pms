/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (HomeLeaveMapping.as).
 */

package hk.org.ha.model.pms.vo.reftable {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import hk.org.ha.model.pms.udt.YesNoFlag;
    import hk.org.ha.model.pms.udt.reftable.HomeLeaveMappingAction;
    import hk.org.ha.model.pms.udt.reftable.HomeLeaveMappingType;
    import org.granite.util.Enum;

    [Bindable]
    public class HomeLeaveMappingBase implements IExternalizable {

        private var _defaultType:HomeLeaveMappingType;
        private var _homeLeaveFlag:YesNoFlag;
        private var _homeLeaveMappingAction:HomeLeaveMappingAction;
        private var _hospCode:String;
        private var _pasDescription:String;
        private var _pasSpecCode:String;
        private var _workstoreCode:String;

        public function set defaultType(value:HomeLeaveMappingType):void {
            _defaultType = value;
        }
        public function get defaultType():HomeLeaveMappingType {
            return _defaultType;
        }

        public function set homeLeaveFlag(value:YesNoFlag):void {
            _homeLeaveFlag = value;
        }
        public function get homeLeaveFlag():YesNoFlag {
            return _homeLeaveFlag;
        }

        public function set homeLeaveMappingAction(value:HomeLeaveMappingAction):void {
            _homeLeaveMappingAction = value;
        }
        public function get homeLeaveMappingAction():HomeLeaveMappingAction {
            return _homeLeaveMappingAction;
        }

        public function set hospCode(value:String):void {
            _hospCode = value;
        }
        public function get hospCode():String {
            return _hospCode;
        }

        public function set pasDescription(value:String):void {
            _pasDescription = value;
        }
        public function get pasDescription():String {
            return _pasDescription;
        }

        public function set pasSpecCode(value:String):void {
            _pasSpecCode = value;
        }
        public function get pasSpecCode():String {
            return _pasSpecCode;
        }

        public function set workstoreCode(value:String):void {
            _workstoreCode = value;
        }
        public function get workstoreCode():String {
            return _workstoreCode;
        }

        public function readExternal(input:IDataInput):void {
            _defaultType = Enum.readEnum(input) as HomeLeaveMappingType;
            _homeLeaveFlag = Enum.readEnum(input) as YesNoFlag;
            _homeLeaveMappingAction = Enum.readEnum(input) as HomeLeaveMappingAction;
            _hospCode = input.readObject() as String;
            _pasDescription = input.readObject() as String;
            _pasSpecCode = input.readObject() as String;
            _workstoreCode = input.readObject() as String;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_defaultType);
            output.writeObject(_homeLeaveFlag);
            output.writeObject(_homeLeaveMappingAction);
            output.writeObject(_hospCode);
            output.writeObject(_pasDescription);
            output.writeObject(_pasSpecCode);
            output.writeObject(_workstoreCode);
        }
    }
}