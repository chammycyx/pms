/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (MpSfiInvoicePrivatePatInfo.as).
 */

package hk.org.ha.model.pms.vo.charging {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;

    [Bindable]
    public class MpSfiInvoicePrivatePatInfoBase implements IExternalizable {

        private var _bedNum:String;
        private var _caseNum:String;
        private var _dischargeDate:Date;
        private var _lastInvoicePrintDate:Date;
        private var _medProfileId:Number;
        private var _pasSpecCode:String;
        private var _patName:String;
        private var _patNameChi:String;
        private var _ward:String;

        public function set bedNum(value:String):void {
            _bedNum = value;
        }
        public function get bedNum():String {
            return _bedNum;
        }

        public function set caseNum(value:String):void {
            _caseNum = value;
        }
        public function get caseNum():String {
            return _caseNum;
        }

        public function set dischargeDate(value:Date):void {
            _dischargeDate = value;
        }
        public function get dischargeDate():Date {
            return _dischargeDate;
        }

        public function set lastInvoicePrintDate(value:Date):void {
            _lastInvoicePrintDate = value;
        }
        public function get lastInvoicePrintDate():Date {
            return _lastInvoicePrintDate;
        }

        public function set medProfileId(value:Number):void {
            _medProfileId = value;
        }
        public function get medProfileId():Number {
            return _medProfileId;
        }

        public function set pasSpecCode(value:String):void {
            _pasSpecCode = value;
        }
        public function get pasSpecCode():String {
            return _pasSpecCode;
        }

        public function set patName(value:String):void {
            _patName = value;
        }
        public function get patName():String {
            return _patName;
        }

        public function set patNameChi(value:String):void {
            _patNameChi = value;
        }
        public function get patNameChi():String {
            return _patNameChi;
        }

        public function set ward(value:String):void {
            _ward = value;
        }
        public function get ward():String {
            return _ward;
        }

        public function readExternal(input:IDataInput):void {
            _bedNum = input.readObject() as String;
            _caseNum = input.readObject() as String;
            _dischargeDate = input.readObject() as Date;
            _lastInvoicePrintDate = input.readObject() as Date;
            _medProfileId = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            _pasSpecCode = input.readObject() as String;
            _patName = input.readObject() as String;
            _patNameChi = input.readObject() as String;
            _ward = input.readObject() as String;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_bedNum);
            output.writeObject(_caseNum);
            output.writeObject(_dischargeDate);
            output.writeObject(_lastInvoicePrintDate);
            output.writeObject(_medProfileId);
            output.writeObject(_pasSpecCode);
            output.writeObject(_patName);
            output.writeObject(_patNameChi);
            output.writeObject(_ward);
        }
    }
}