/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (Chest.as).
 */

package hk.org.ha.model.pms.persistence.reftable {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.getQualifiedClassName;
    import hk.org.ha.model.pms.persistence.VersionEntity;
    import hk.org.ha.model.pms.persistence.corp.Workstore;
    import mx.collections.ListCollectionView;
    import mx.core.IUID;
    import mx.data.utils.Managed;
    import mx.utils.UIDUtil;
    import org.granite.collections.IPersistentCollection;
    import org.granite.meta;
    import org.granite.tide.IEntityManager;
    import org.granite.tide.IPropertyHolder;

    use namespace meta;

    [Managed]
    public class ChestBase extends VersionEntity {

        [Transient]
        meta var entityManager:IEntityManager = null;

        private var __initialized:Boolean = true;
        private var __detachedState:String = null;

        private var _chestItemList:ListCollectionView;
        private var _defaultAdvStart:Number;
        private var _defaultDuration:Number;
        private var _id:Number;
        private var _unitDoseName:String;
        private var _workstore:Workstore;

        meta override function isInitialized(name:String = null):Boolean {
            if (!name)
                return __initialized;

            var property:* = this[name];
            return (
                (!(property is Chest) || (property as Chest).meta::isInitialized()) &&
                (!(property is IPersistentCollection) || (property as IPersistentCollection).isInitialized())
            );
        }
        
        [Bindable(event="dirtyChange")]
		public function get meta_dirty():Boolean {
			return Managed.getProperty(this, "meta_dirty", false);
		}

        public function set chestItemList(value:ListCollectionView):void {
            _chestItemList = value;
        }
        public function get chestItemList():ListCollectionView {
            return _chestItemList;
        }

        public function set defaultAdvStart(value:Number):void {
            _defaultAdvStart = value;
        }
        public function get defaultAdvStart():Number {
            return _defaultAdvStart;
        }

        public function set defaultDuration(value:Number):void {
            _defaultDuration = value;
        }
        public function get defaultDuration():Number {
            return _defaultDuration;
        }

        public function set id(value:Number):void {
            _id = value;
        }
        [Id]
        public function get id():Number {
            return _id;
        }

        public function set unitDoseName(value:String):void {
            _unitDoseName = value;
        }
        public function get unitDoseName():String {
            return _unitDoseName;
        }

        public function set workstore(value:Workstore):void {
            _workstore = value;
        }
        public function get workstore():Workstore {
            return _workstore;
        }

        public function set uid(value:String):void {
            // noop...
        }
        public function get uid():String {
            if (isNaN(_id))
                return UIDUtil.createUID();
            return getQualifiedClassName(this) + "#[" + String(_id) + "]";
        }

        meta override function merge(em:IEntityManager, obj:*):void {
            var src:ChestBase = ChestBase(obj);
            __initialized = src.__initialized;
            __detachedState = src.__detachedState;
            if (meta::isInitialized()) {
                super.meta::merge(em, obj);
               em.meta_mergeExternal(src._chestItemList, _chestItemList, null, this, 'chestItemList', function setter(o:*):void{_chestItemList = o as ListCollectionView}, false);
               em.meta_mergeExternal(src._defaultAdvStart, _defaultAdvStart, null, this, 'defaultAdvStart', function setter(o:*):void{_defaultAdvStart = o as Number}, false);
               em.meta_mergeExternal(src._defaultDuration, _defaultDuration, null, this, 'defaultDuration', function setter(o:*):void{_defaultDuration = o as Number}, false);
               em.meta_mergeExternal(src._id, _id, null, this, 'id', function setter(o:*):void{_id = o as Number}, false);
               em.meta_mergeExternal(src._unitDoseName, _unitDoseName, null, this, 'unitDoseName', function setter(o:*):void{_unitDoseName = o as String}, false);
               em.meta_mergeExternal(src._workstore, _workstore, null, this, 'workstore', function setter(o:*):void{_workstore = o as Workstore}, false);
            }
            else {
               em.meta_mergeExternal(src._id, _id, null, this, 'id', function setter(o:*):void{_id = o as Number});
            }
        }

        public override function readExternal(input:IDataInput):void {
            __initialized = input.readObject() as Boolean;
            __detachedState = input.readObject() as String;
            if (meta::isInitialized()) {
                super.readExternal(input);
                _chestItemList = input.readObject() as ListCollectionView;
                _defaultAdvStart = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _defaultDuration = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _id = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _unitDoseName = input.readObject() as String;
                _workstore = input.readObject() as Workstore;
            }
            else {
                _id = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            }
        }

        public override function writeExternal(output:IDataOutput):void {
            output.writeObject(__initialized);
            output.writeObject(__detachedState);
            if (meta::isInitialized()) {
                super.writeExternal(output);
                output.writeObject((_chestItemList is IPropertyHolder) ? IPropertyHolder(_chestItemList).object : _chestItemList);
                output.writeObject((_defaultAdvStart is IPropertyHolder) ? IPropertyHolder(_defaultAdvStart).object : _defaultAdvStart);
                output.writeObject((_defaultDuration is IPropertyHolder) ? IPropertyHolder(_defaultDuration).object : _defaultDuration);
                output.writeObject((_id is IPropertyHolder) ? IPropertyHolder(_id).object : _id);
                output.writeObject((_unitDoseName is IPropertyHolder) ? IPropertyHolder(_unitDoseName).object : _unitDoseName);
                output.writeObject((_workstore is IPropertyHolder) ? IPropertyHolder(_workstore).object : _workstore);
            }
            else {
                output.writeObject(_id);
            }
        }
    }
}
