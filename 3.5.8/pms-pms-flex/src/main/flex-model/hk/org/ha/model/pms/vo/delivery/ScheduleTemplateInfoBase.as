/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (ScheduleTemplateInfo.as).
 */

package hk.org.ha.model.pms.vo.delivery {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import hk.org.ha.model.pms.persistence.medprofile.DeliverySchedule;
    import mx.collections.ListCollectionView;

    [Bindable]
    public class ScheduleTemplateInfoBase implements IExternalizable {

        private var _currentDeliverySchedule:DeliverySchedule;
        private var _defaultDeliverySchedule:DeliverySchedule;
        private var _deliveryScheduleItemList:ListCollectionView;
        private var _deliveryScheduleList:ListCollectionView;
        private var _emptyDefaultTemplateNameFlag:Boolean;

        public function set currentDeliverySchedule(value:DeliverySchedule):void {
            _currentDeliverySchedule = value;
        }
        public function get currentDeliverySchedule():DeliverySchedule {
            return _currentDeliverySchedule;
        }

        public function set defaultDeliverySchedule(value:DeliverySchedule):void {
            _defaultDeliverySchedule = value;
        }
        public function get defaultDeliverySchedule():DeliverySchedule {
            return _defaultDeliverySchedule;
        }

        public function set deliveryScheduleItemList(value:ListCollectionView):void {
            _deliveryScheduleItemList = value;
        }
        public function get deliveryScheduleItemList():ListCollectionView {
            return _deliveryScheduleItemList;
        }

        public function set deliveryScheduleList(value:ListCollectionView):void {
            _deliveryScheduleList = value;
        }
        public function get deliveryScheduleList():ListCollectionView {
            return _deliveryScheduleList;
        }

        public function set emptyDefaultTemplateNameFlag(value:Boolean):void {
            _emptyDefaultTemplateNameFlag = value;
        }
        public function get emptyDefaultTemplateNameFlag():Boolean {
            return _emptyDefaultTemplateNameFlag;
        }

        public function readExternal(input:IDataInput):void {
            _currentDeliverySchedule = input.readObject() as DeliverySchedule;
            _defaultDeliverySchedule = input.readObject() as DeliverySchedule;
            _deliveryScheduleItemList = input.readObject() as ListCollectionView;
            _deliveryScheduleList = input.readObject() as ListCollectionView;
            _emptyDefaultTemplateNameFlag = input.readObject() as Boolean;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_currentDeliverySchedule);
            output.writeObject(_defaultDeliverySchedule);
            output.writeObject(_deliveryScheduleItemList);
            output.writeObject(_deliveryScheduleList);
            output.writeObject(_emptyDefaultTemplateNameFlag);
        }
    }
}