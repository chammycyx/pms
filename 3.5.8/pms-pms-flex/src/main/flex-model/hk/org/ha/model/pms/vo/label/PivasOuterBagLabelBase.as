/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (PivasOuterBagLabel.as).
 */

package hk.org.ha.model.pms.vo.label {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;

    [Bindable]
    public class PivasOuterBagLabelBase implements IExternalizable {

        private var _batchNum:String;
        private var _bedNum:String;
        private var _caseNum:String;
        private var _concn:String;
        private var _containerName:String;
        private var _diluentDesc:String;
        private var _finalVolume:String;
        private var _finalVolumeSplit:String;
        private var _firstDoseDateTime:Date;
        private var _formulaPrintName:String;
        private var _freq:String;
        private var _hkid:String;
        private var _manufactureDate:Date;
        private var _numOfLabel:Number;
        private var _orderDosage:String;
        private var _orderDosageSplit:String;
        private var _patName:String;
        private var _patNameChi:String;
        private var _pivasDosageUnit:String;
        private var _prepExpiryDate:Date;
        private var _shelfLifeZeroFlag:Boolean;
        private var _siteDesc:String;
        private var _splitCount:String;
        private var _supplFreq:String;
        private var _trxId:String;
        private var _ward:String;

        public function set batchNum(value:String):void {
            _batchNum = value;
        }
        public function get batchNum():String {
            return _batchNum;
        }

        public function set bedNum(value:String):void {
            _bedNum = value;
        }
        public function get bedNum():String {
            return _bedNum;
        }

        public function set caseNum(value:String):void {
            _caseNum = value;
        }
        public function get caseNum():String {
            return _caseNum;
        }

        public function set concn(value:String):void {
            _concn = value;
        }
        public function get concn():String {
            return _concn;
        }

        public function set containerName(value:String):void {
            _containerName = value;
        }
        public function get containerName():String {
            return _containerName;
        }

        public function set diluentDesc(value:String):void {
            _diluentDesc = value;
        }
        public function get diluentDesc():String {
            return _diluentDesc;
        }

        public function set finalVolume(value:String):void {
            _finalVolume = value;
        }
        public function get finalVolume():String {
            return _finalVolume;
        }

        public function set finalVolumeSplit(value:String):void {
            _finalVolumeSplit = value;
        }
        public function get finalVolumeSplit():String {
            return _finalVolumeSplit;
        }

        public function set firstDoseDateTime(value:Date):void {
            _firstDoseDateTime = value;
        }
        public function get firstDoseDateTime():Date {
            return _firstDoseDateTime;
        }

        public function set formulaPrintName(value:String):void {
            _formulaPrintName = value;
        }
        public function get formulaPrintName():String {
            return _formulaPrintName;
        }

        public function set freq(value:String):void {
            _freq = value;
        }
        public function get freq():String {
            return _freq;
        }

        public function set hkid(value:String):void {
            _hkid = value;
        }
        public function get hkid():String {
            return _hkid;
        }

        public function set manufactureDate(value:Date):void {
            _manufactureDate = value;
        }
        public function get manufactureDate():Date {
            return _manufactureDate;
        }

        public function set numOfLabel(value:Number):void {
            _numOfLabel = value;
        }
        public function get numOfLabel():Number {
            return _numOfLabel;
        }

        public function set orderDosage(value:String):void {
            _orderDosage = value;
        }
        public function get orderDosage():String {
            return _orderDosage;
        }

        public function set orderDosageSplit(value:String):void {
            _orderDosageSplit = value;
        }
        public function get orderDosageSplit():String {
            return _orderDosageSplit;
        }

        public function set patName(value:String):void {
            _patName = value;
        }
        public function get patName():String {
            return _patName;
        }

        public function set patNameChi(value:String):void {
            _patNameChi = value;
        }
        public function get patNameChi():String {
            return _patNameChi;
        }

        public function set pivasDosageUnit(value:String):void {
            _pivasDosageUnit = value;
        }
        public function get pivasDosageUnit():String {
            return _pivasDosageUnit;
        }

        public function set prepExpiryDate(value:Date):void {
            _prepExpiryDate = value;
        }
        public function get prepExpiryDate():Date {
            return _prepExpiryDate;
        }

        public function set shelfLifeZeroFlag(value:Boolean):void {
            _shelfLifeZeroFlag = value;
        }
        public function get shelfLifeZeroFlag():Boolean {
            return _shelfLifeZeroFlag;
        }

        public function set siteDesc(value:String):void {
            _siteDesc = value;
        }
        public function get siteDesc():String {
            return _siteDesc;
        }

        public function set splitCount(value:String):void {
            _splitCount = value;
        }
        public function get splitCount():String {
            return _splitCount;
        }

        public function set supplFreq(value:String):void {
            _supplFreq = value;
        }
        public function get supplFreq():String {
            return _supplFreq;
        }

        public function set trxId(value:String):void {
            _trxId = value;
        }
        public function get trxId():String {
            return _trxId;
        }

        public function set ward(value:String):void {
            _ward = value;
        }
        public function get ward():String {
            return _ward;
        }

        public function readExternal(input:IDataInput):void {
            _batchNum = input.readObject() as String;
            _bedNum = input.readObject() as String;
            _caseNum = input.readObject() as String;
            _concn = input.readObject() as String;
            _containerName = input.readObject() as String;
            _diluentDesc = input.readObject() as String;
            _finalVolume = input.readObject() as String;
            _finalVolumeSplit = input.readObject() as String;
            _firstDoseDateTime = input.readObject() as Date;
            _formulaPrintName = input.readObject() as String;
            _freq = input.readObject() as String;
            _hkid = input.readObject() as String;
            _manufactureDate = input.readObject() as Date;
            _numOfLabel = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            _orderDosage = input.readObject() as String;
            _orderDosageSplit = input.readObject() as String;
            _patName = input.readObject() as String;
            _patNameChi = input.readObject() as String;
            _pivasDosageUnit = input.readObject() as String;
            _prepExpiryDate = input.readObject() as Date;
            _shelfLifeZeroFlag = input.readObject() as Boolean;
            _siteDesc = input.readObject() as String;
            _splitCount = input.readObject() as String;
            _supplFreq = input.readObject() as String;
            _trxId = input.readObject() as String;
            _ward = input.readObject() as String;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_batchNum);
            output.writeObject(_bedNum);
            output.writeObject(_caseNum);
            output.writeObject(_concn);
            output.writeObject(_containerName);
            output.writeObject(_diluentDesc);
            output.writeObject(_finalVolume);
            output.writeObject(_finalVolumeSplit);
            output.writeObject(_firstDoseDateTime);
            output.writeObject(_formulaPrintName);
            output.writeObject(_freq);
            output.writeObject(_hkid);
            output.writeObject(_manufactureDate);
            output.writeObject(_numOfLabel);
            output.writeObject(_orderDosage);
            output.writeObject(_orderDosageSplit);
            output.writeObject(_patName);
            output.writeObject(_patNameChi);
            output.writeObject(_pivasDosageUnit);
            output.writeObject(_prepExpiryDate);
            output.writeObject(_shelfLifeZeroFlag);
            output.writeObject(_siteDesc);
            output.writeObject(_splitCount);
            output.writeObject(_supplFreq);
            output.writeObject(_trxId);
            output.writeObject(_ward);
        }
    }
}