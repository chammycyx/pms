/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * NOTE: this file is only generated if it does not exist. You may safely put
 * your custom code here.
 */

package hk.org.ha.model.pms.vo.vetting {
	import hk.org.ha.model.pms.udt.Gender;
	import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
	import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
	import hk.org.ha.model.pms.vo.security.PropMap;
	
    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.vo.vetting.OrderViewInfo")]
    public class OrderViewInfo extends OrderViewInfoBase {
		
		public function clearInfoMsg():void {
			infoMsgCodeList.removeAll();
			infoMsgParamList.removeAll();
		}
		
		public function clearErrorMsg():void {
			errorMsgCodeList.removeAll();
			errorMsgParamList.removeAll();
		}
		
		public function pregnancyCheckVisible(gender:Gender, docType:MedOrderDocType, propMap:PropMap):Boolean {
			if (gender != Gender.Female || !propMap.getValueAsBoolean("alert.mds.enabled")) {
				return false;
			}
			
			switch (oneStopOrderType) {
				case OneStopOrderType.MedOrder:
					return propMap.getValueAsBoolean("vetting.alert.mds.check");
					
				case OneStopOrderType.ManualOrder:
				case OneStopOrderType.DhOrder:
					return propMap.getValueAsBoolean("vetting.order.manual.alert.mds.check");
					
				case OneStopOrderType.DispOrder:
					if (docType == MedOrderDocType.Normal) {
						return propMap.getValueAsBoolean("vetting.alert.mds.check");
					}
					else {
						return propMap.getValueAsBoolean("vetting.order.manual.alert.mds.check");
					}
					
				case OneStopOrderType.RefillOrder:
					return propMap.getValueAsBoolean("refill.standard.alert.mds.check");
					
				case OneStopOrderType.SfiOrder:
					return propMap.getValueAsBoolean("refill.sfi.alert.mds.check");
					
				default:
					return false;
			}
		}
    }
}
