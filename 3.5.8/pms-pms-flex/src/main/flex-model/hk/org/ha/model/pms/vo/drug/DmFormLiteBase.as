/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (DmFormLite.as).
 */

package hk.org.ha.model.pms.vo.drug {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;

    [Bindable]
    public class DmFormLiteBase implements IExternalizable {

        private var _formCode:String;
        private var _formDescEng:String;
        private var _labelDesc:String;
        private var _moeDesc:String;

        public function set formCode(value:String):void {
            _formCode = value;
        }
        public function get formCode():String {
            return _formCode;
        }

        public function set formDescEng(value:String):void {
            _formDescEng = value;
        }
        public function get formDescEng():String {
            return _formDescEng;
        }

        public function set labelDesc(value:String):void {
            _labelDesc = value;
        }
        public function get labelDesc():String {
            return _labelDesc;
        }

        public function set moeDesc(value:String):void {
            _moeDesc = value;
        }
        public function get moeDesc():String {
            return _moeDesc;
        }

        public function readExternal(input:IDataInput):void {
            _formCode = input.readObject() as String;
            _formDescEng = input.readObject() as String;
            _labelDesc = input.readObject() as String;
            _moeDesc = input.readObject() as String;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_formCode);
            output.writeObject(_formDescEng);
            output.writeObject(_labelDesc);
            output.writeObject(_moeDesc);
        }
    }
}