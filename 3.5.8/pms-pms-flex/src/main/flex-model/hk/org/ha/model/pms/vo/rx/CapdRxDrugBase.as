/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (CapdRxDrug.as).
 */

package hk.org.ha.model.pms.vo.rx {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import hk.org.ha.model.pms.udt.vetting.ActionStatus;
    import mx.collections.ListCollectionView;
    import org.granite.util.Enum;

    [Bindable]
    public class CapdRxDrugBase extends RxItem {

        private var _actionStatus:ActionStatus;
        private var _calciumStrength:String;
        private var _capdItemList:ListCollectionView;
        private var _supplierSystem:String;
        private var _updateCapdItemQty:Boolean;
        private var _updateGroupNumFlag:Boolean;

        public function set actionStatus(value:ActionStatus):void {
            _actionStatus = value;
        }
        public function get actionStatus():ActionStatus {
            return _actionStatus;
        }

        public function set calciumStrength(value:String):void {
            _calciumStrength = value;
        }
        public function get calciumStrength():String {
            return _calciumStrength;
        }

        public function set capdItemList(value:ListCollectionView):void {
            _capdItemList = value;
        }
        public function get capdItemList():ListCollectionView {
            return _capdItemList;
        }

        public function set supplierSystem(value:String):void {
            _supplierSystem = value;
        }
        public function get supplierSystem():String {
            return _supplierSystem;
        }

        public function set updateCapdItemQty(value:Boolean):void {
            _updateCapdItemQty = value;
        }
        public function get updateCapdItemQty():Boolean {
            return _updateCapdItemQty;
        }

        public function set updateGroupNumFlag(value:Boolean):void {
            _updateGroupNumFlag = value;
        }
        public function get updateGroupNumFlag():Boolean {
            return _updateGroupNumFlag;
        }

        public override function readExternal(input:IDataInput):void {
            super.readExternal(input);
            _actionStatus = Enum.readEnum(input) as ActionStatus;
            _calciumStrength = input.readObject() as String;
            _capdItemList = input.readObject() as ListCollectionView;
            _supplierSystem = input.readObject() as String;
            _updateCapdItemQty = input.readObject() as Boolean;
            _updateGroupNumFlag = input.readObject() as Boolean;
        }

        public override function writeExternal(output:IDataOutput):void {
            super.writeExternal(output);
            output.writeObject(_actionStatus);
            output.writeObject(_calciumStrength);
            output.writeObject(_capdItemList);
            output.writeObject(_supplierSystem);
            output.writeObject(_updateCapdItemQty);
            output.writeObject(_updateGroupNumFlag);
        }
    }
}