/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (PendReasonServiceBean.as).
 */

package hk.org.ha.model.pms.biz.reftable.mp {

    import flash.utils.flash_proxy;
    import mx.collections.ListCollectionView;
    import org.granite.tide.BaseContext;
    import org.granite.tide.Component;
    import org.granite.tide.ITideResponder;
    
    use namespace flash_proxy;

    public class PendReasonServiceBeanBase extends Component {

        public function retrievePendingReasonList(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrievePendingReasonList", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrievePendingReasonList", resultHandler);
            else if (resultHandler == null)
                callProperty("retrievePendingReasonList");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function updatePendingReasonList(arg0:ListCollectionView, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("updatePendingReasonList", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("updatePendingReasonList", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("updatePendingReasonList", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function destroy(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("destroy", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("destroy", resultHandler);
            else if (resultHandler == null)
                callProperty("destroy");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }
    }
}
