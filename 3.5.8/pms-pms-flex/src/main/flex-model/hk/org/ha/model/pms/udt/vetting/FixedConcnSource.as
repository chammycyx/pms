/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.udt.vetting {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.udt.vetting.FixedConcnSource")]
    public class FixedConcnSource extends Enum {

        public static const None:FixedConcnSource = new FixedConcnSource("None", _, "-", "None");
        public static const PMS:FixedConcnSource = new FixedConcnSource("PMS", _, "PMS", "PMS");
        public static const PIVAS:FixedConcnSource = new FixedConcnSource("PIVAS", _, "PIVAS", "PIVAS");

        function FixedConcnSource(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || None.name), restrictor, (dataValue || None.dataValue), (displayValue || None.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [None, PMS, PIVAS];
        }

        public static function valueOf(name:String):FixedConcnSource {
            return FixedConcnSource(None.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):FixedConcnSource {
            return FixedConcnSource(None.dataConstantOf(dataValue));
        }
    }
}