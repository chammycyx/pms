/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (RenalChartRpt.as).
 */

package hk.org.ha.model.pms.vo.vetting.mp {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;

    [Bindable]
    public class RenalChartRptBase implements IExternalizable {

        private var _createDate:Date;
        private var _ecrcl:Number;

        public function set createDate(value:Date):void {
            _createDate = value;
        }
        public function get createDate():Date {
            return _createDate;
        }

        public function set ecrcl(value:Number):void {
            _ecrcl = value;
        }
        public function get ecrcl():Number {
            return _ecrcl;
        }

        public function readExternal(input:IDataInput):void {
            _createDate = input.readObject() as Date;
            _ecrcl = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_createDate);
            output.writeObject(_ecrcl);
        }
    }
}