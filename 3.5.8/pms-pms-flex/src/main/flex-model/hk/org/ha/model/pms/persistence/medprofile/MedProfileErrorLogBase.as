/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (MedProfileErrorLog.as).
 */

package hk.org.ha.model.pms.persistence.medprofile {

    import flash.events.EventDispatcher;
    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import flash.utils.getQualifiedClassName;
    import hk.org.ha.model.pms.udt.medprofile.ErrorLogType;
    import hk.org.ha.model.pms.vo.medprofile.ErrorInfo;
    import mx.core.IUID;
    import mx.data.utils.Managed;
    import mx.utils.UIDUtil;
    import org.granite.collections.IPersistentCollection;
    import org.granite.meta;
    import org.granite.tide.IEntity;
    import org.granite.tide.IEntityManager;
    import org.granite.tide.IPropertyHolder;
    import org.granite.util.Enum;

    use namespace meta;

    [Managed]
    public class MedProfileErrorLogBase implements IExternalizable, IUID {

        [Transient]
        meta var entityManager:IEntityManager = null;

        private var __initialized:Boolean = true;
        private var __detachedState:String = null;

        private var _createDate:Date;
        private var _deliveryRequestId:Number;
        private var _errorInfo:ErrorInfo;
        private var _errorXml:String;
        private var _hideErrorStatFlag:Boolean;
        private var _hideFollowUpFlag:Boolean;
        private var _id:Number;
        private var _medProfileItem:MedProfileItem;
        private var _message:String;
        private var _type:ErrorLogType;

        meta function isInitialized(name:String = null):Boolean {
            if (!name)
                return __initialized;

            var property:* = this[name];
            return (
                (!(property is MedProfileErrorLog) || (property as MedProfileErrorLog).meta::isInitialized()) &&
                (!(property is IPersistentCollection) || (property as IPersistentCollection).isInitialized())
            );
        }
        
        [Bindable(event="dirtyChange")]
		public function get meta_dirty():Boolean {
			return Managed.getProperty(this, "meta_dirty", false);
		}

        public function set createDate(value:Date):void {
            _createDate = value;
        }
        public function get createDate():Date {
            return _createDate;
        }

        public function set deliveryRequestId(value:Number):void {
            _deliveryRequestId = value;
        }
        public function get deliveryRequestId():Number {
            return _deliveryRequestId;
        }

        public function set errorInfo(value:ErrorInfo):void {
            _errorInfo = value;
        }
        public function get errorInfo():ErrorInfo {
            return _errorInfo;
        }

        public function set errorXml(value:String):void {
            _errorXml = value;
        }
        public function get errorXml():String {
            return _errorXml;
        }

        public function set hideErrorStatFlag(value:Boolean):void {
            _hideErrorStatFlag = value;
        }
        public function get hideErrorStatFlag():Boolean {
            return _hideErrorStatFlag;
        }

        public function set hideFollowUpFlag(value:Boolean):void {
            _hideFollowUpFlag = value;
        }
        public function get hideFollowUpFlag():Boolean {
            return _hideFollowUpFlag;
        }

        public function set id(value:Number):void {
            _id = value;
        }
        [Id]
        public function get id():Number {
            return _id;
        }

        public function set medProfileItem(value:MedProfileItem):void {
            _medProfileItem = value;
        }
        public function get medProfileItem():MedProfileItem {
            return _medProfileItem;
        }

        public function set message(value:String):void {
            _message = value;
        }
        public function get message():String {
            return _message;
        }

        public function set type(value:ErrorLogType):void {
            _type = value;
        }
        public function get type():ErrorLogType {
            return _type;
        }

        public function set uid(value:String):void {
            // noop...
        }
        public function get uid():String {
            if (isNaN(_id))
                return UIDUtil.createUID();
            return getQualifiedClassName(this) + "#[" + String(_id) + "]";
        }

        meta function merge(em:IEntityManager, obj:*):void {
            var src:MedProfileErrorLogBase = MedProfileErrorLogBase(obj);
            __initialized = src.__initialized;
            __detachedState = src.__detachedState;
            if (meta::isInitialized()) {
               em.meta_mergeExternal(src._createDate, _createDate, null, this, 'createDate', function setter(o:*):void{_createDate = o as Date}, false);
               em.meta_mergeExternal(src._deliveryRequestId, _deliveryRequestId, null, this, 'deliveryRequestId', function setter(o:*):void{_deliveryRequestId = o as Number}, false);
               em.meta_mergeExternal(src._errorInfo, _errorInfo, null, this, 'errorInfo', function setter(o:*):void{_errorInfo = o as ErrorInfo}, false);
               em.meta_mergeExternal(src._errorXml, _errorXml, null, this, 'errorXml', function setter(o:*):void{_errorXml = o as String}, false);
               em.meta_mergeExternal(src._hideErrorStatFlag, _hideErrorStatFlag, null, this, 'hideErrorStatFlag', function setter(o:*):void{_hideErrorStatFlag = o as Boolean}, false);
               em.meta_mergeExternal(src._hideFollowUpFlag, _hideFollowUpFlag, null, this, 'hideFollowUpFlag', function setter(o:*):void{_hideFollowUpFlag = o as Boolean}, false);
               em.meta_mergeExternal(src._id, _id, null, this, 'id', function setter(o:*):void{_id = o as Number}, false);
               em.meta_mergeExternal(src._medProfileItem, _medProfileItem, null, this, 'medProfileItem', function setter(o:*):void{_medProfileItem = o as MedProfileItem}, false);
               em.meta_mergeExternal(src._message, _message, null, this, 'message', function setter(o:*):void{_message = o as String}, false);
               em.meta_mergeExternal(src._type, _type, null, this, 'type', function setter(o:*):void{_type = o as ErrorLogType}, false);
            }
            else {
               em.meta_mergeExternal(src._id, _id, null, this, 'id', function setter(o:*):void{_id = o as Number});
            }
        }

        public function readExternal(input:IDataInput):void {
            __initialized = input.readObject() as Boolean;
            __detachedState = input.readObject() as String;
            if (meta::isInitialized()) {
                _createDate = input.readObject() as Date;
                _deliveryRequestId = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _errorInfo = input.readObject() as ErrorInfo;
                _errorXml = input.readObject() as String;
                _hideErrorStatFlag = input.readObject() as Boolean;
                _hideFollowUpFlag = input.readObject() as Boolean;
                _id = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _medProfileItem = input.readObject() as MedProfileItem;
                _message = input.readObject() as String;
                _type = Enum.readEnum(input) as ErrorLogType;
            }
            else {
                _id = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            }
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(__initialized);
            output.writeObject(__detachedState);
            if (meta::isInitialized()) {
                output.writeObject((_createDate is IPropertyHolder) ? IPropertyHolder(_createDate).object : _createDate);
                output.writeObject((_deliveryRequestId is IPropertyHolder) ? IPropertyHolder(_deliveryRequestId).object : _deliveryRequestId);
                output.writeObject((_errorInfo is IPropertyHolder) ? IPropertyHolder(_errorInfo).object : _errorInfo);
                output.writeObject((_errorXml is IPropertyHolder) ? IPropertyHolder(_errorXml).object : _errorXml);
                output.writeObject((_hideErrorStatFlag is IPropertyHolder) ? IPropertyHolder(_hideErrorStatFlag).object : _hideErrorStatFlag);
                output.writeObject((_hideFollowUpFlag is IPropertyHolder) ? IPropertyHolder(_hideFollowUpFlag).object : _hideFollowUpFlag);
                output.writeObject((_id is IPropertyHolder) ? IPropertyHolder(_id).object : _id);
                output.writeObject((_medProfileItem is IPropertyHolder) ? IPropertyHolder(_medProfileItem).object : _medProfileItem);
                output.writeObject((_message is IPropertyHolder) ? IPropertyHolder(_message).object : _message);
                output.writeObject((_type is IPropertyHolder) ? IPropertyHolder(_type).object : _type);
            }
            else {
                output.writeObject(_id);
            }
        }
    }
}
