/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.udt.disp {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.udt.disp.MpDischargeTrxType")]
    public class MpDischargeTrxType extends Enum {

        public static const MpNewOrder:MpDischargeTrxType = new MpDischargeTrxType("MpNewOrder", _, "DO1", "New Discharge Medication Order");
        public static const MpModifyOrder:MpDischargeTrxType = new MpDischargeTrxType("MpModifyOrder", _, "AR6", "Modify Discharge Medication Order");
        public static const MpDeleteOrder:MpDischargeTrxType = new MpDischargeTrxType("MpDeleteOrder", _, "DR6", "Delete Discharge Medication Order");
        public static const MpChangePrescType:MpDischargeTrxType = new MpDischargeTrxType("MpChangePrescType", _, "RT6", "Change Discharge Presciption Type");
        public static const MpAddPostDispComment:MpDischargeTrxType = new MpDischargeTrxType("MpAddPostDispComment", _, "RT7", "Add Discharge Post Dispensing Comment");
        public static const MpResumeAdvanceOrder:MpDischargeTrxType = new MpDischargeTrxType("MpResumeAdvanceOrder", _, "RO6", "Change Advance Order to Normal Order (Reverse MOE suspend)");
        public static const MpConfirmRemark:MpDischargeTrxType = new MpDischargeTrxType("MpConfirmRemark", _, "CR6", "Confirm remark");
        public static const MpDiscontinue:MpDischargeTrxType = new MpDischargeTrxType("MpDiscontinue", _, "DD6", "Discontinue Item");
        public static const MpCancelDiscontinue:MpDischargeTrxType = new MpDischargeTrxType("MpCancelDiscontinue", _, "DD7", "Cancel Discontinue Item");
        public static const MpVetOrder:MpDischargeTrxType = new MpDischargeTrxType("MpVetOrder", _, "VO6", "Vet order without remark");
        public static const MpVetOrderWithRemark:MpDischargeTrxType = new MpDischargeTrxType("MpVetOrderWithRemark", _, "VO7", "Vet order with remark");

        function MpDischargeTrxType(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || MpNewOrder.name), restrictor, (dataValue || MpNewOrder.dataValue), (displayValue || MpNewOrder.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [MpNewOrder, MpModifyOrder, MpDeleteOrder, MpChangePrescType, MpAddPostDispComment, MpResumeAdvanceOrder, MpConfirmRemark, MpDiscontinue, MpCancelDiscontinue, MpVetOrder, MpVetOrderWithRemark];
        }

        public static function valueOf(name:String):MpDischargeTrxType {
            return MpDischargeTrxType(MpNewOrder.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):MpDischargeTrxType {
            return MpDischargeTrxType(MpNewOrder.dataConstantOf(dataValue));
        }
    }
}