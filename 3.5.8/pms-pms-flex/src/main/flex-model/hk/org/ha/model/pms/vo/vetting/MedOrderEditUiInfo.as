package hk.org.ha.model.pms.vo.vetting {
	
	[Bindable]
	public class MedOrderEditUiInfo {
		
		//Icon variables		
		[Embed(source="/assets/add.png")]
		public static const addIcon:Class;				
		[Embed(source="/assets/remove.png")]
		public static const removeIcon:Class;
		
		//Drug Title Label width
		public static const drugTitleWidth:int 				= 650;
		public static const squareBtnWidth:int				= 20;
		public static const advanceBtnWidth:int				= 118;
		public static const preparationBtnWidth:int			= 118;
		
		//Regimen Grid column width (Total width should be 991)		
		public static const dosageColWidth:int 				= 170;				
		public static const dailyFreqColWidth:int 			= 125;				
		public static const supplFreqColWidth:int 			= 120;				
		public static const prnColWidth:int 				= 50;				
		public static const dispColWidth:int 				= 38;				
		public static const routeSiteColWidth:int 			= 90;			
		public static const durationColWidth:int 			= 193;			
		public static const qtyColWidth:int 				= 105;				
		public static const doseGroupAddRemoveColWidth:int 	= 59;				
		public static const doseAddRemoveColWidth:int 		= 42;
		
		//Component layout variables		
		public static const dosageTxtInputWidth:int 		= 57;				
		public static const dosageUnitLblWidth:int 			= 120;				
		public static const prnChkboxWidth:int 				= 15;				
		public static const prnCbxWidth:int 				= 60;							
		public static const siteCbxWidth:int 				= 87;				
		public static const durationTxtInputWidth:int 		= 27;				
		public static const durationDropDownListWidth:int 	= 63;
		public static const durationDateFieldWidth:int 		= 85; 				
		public static const durationToLblWidth:int 			= 15;				
		public static const qtyTxtInputWidth:int 			= 34;				
		public static const qtyDropDownListWidth:int 		= 60;						
		public static const addRemoveBtnWidth:int 			= 18;		　
		public static const maxInstructionHeight:int 		= 160;
		public static const minInstructionHeight:int 		= 50;
		public static const maxFreqDropDownHeight:int		= 225;
		public static const maxSiteDropDownHeight:int		= 225;
		//Reserved width		
		public static const scrollBarWidth:int 				= 19;		
		
		//Layout variable		
		public static const paddingSpace:int				= 2;
		public static const minPaddingSpace:int				= 1;
		public static const maxBottomLayoutHeight:int		= 225;
		public static const minBottomHeight:int				= 104;
		public static const listVisible:Boolean				= true;				
		public static const maxMoiRegimenGridHeight:int		= 212;		
		public static const minMoiRegimenGridHeight:int		= 73;		
		public static const maxMoiViewGap:int				= 8;
		public static const minMoiViewGap:int				= 5;
		public static const uiComponentHeight:int			= 23;				
		
		public static const specialInstExcludeSpecialNoteWidth:int = 495;
		public static const specialInstWidth:int  				   = 257;
		public static const postDispCommentPanelWidth:int		   = 236;
		
		public static function get minDosageColWidth():int
		{
			return dosageTxtInputWidth+paddingSpace;
		}
		
		public static function get dosageUnitColWidth():int
		{
			return dosageColWidth - minDosageColWidth;
		}
		
		public function MedOrderEditUiInfo():void
		{
			super();			
		}		
		
		
	}
}