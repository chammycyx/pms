package hk.org.ha.model.pms.vo.alert.mds {
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	import mx.formatters.DateFormatter;
	
	import hk.org.ha.model.pms.persistence.AlertEntity;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItemAlert;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
	import hk.org.ha.model.pms.persistence.medprofile.PurchaseRequest;
	import hk.org.ha.model.pms.udt.alert.mds.AlertType;
	import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
	import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
	import hk.org.ha.model.pms.udt.medprofile.MedProfileMoItemStatus;
	import hk.org.ha.model.pms.vo.alert.SteroidTag;
	import hk.org.ha.view.pms.main.medprofile.MedProfileUtils;
	import hk.org.ha.view.pms.main.vetting.mp.VettingUtils;
	
	import org.granite.collections.BasicMap;
	import org.granite.collections.IMap;
	
	public class MdsUtils {
		
		private static const DATE_TIME_FORMATTER:DateFormatter = new DateFormatter();
		private static const MINUTES_PER_HOUR:int = 60;
		private static const SECOND_PER_MINUTES:int = 60;
		private static const MILLISECOND_PER_SECOND:int = 1000;
		public  static const MDS_SUSPEND_REASON:String = "MDS checking pending";
		
		{
			DATE_TIME_FORMATTER.formatString = "DD-MMM-YYYY JJ:NN";
		}
		
		private static function createAlertMap(alertList:ListCollectionView, checkOverrideReason:Boolean=false):IMap {
			
			var map:IMap = new BasicMap();
			
			for each (var alert:AlertEntity in alertList) {
				
				var count:int;
				var keys:String = alert.alert.keys;
				
				if (checkOverrideReason) {
					for each (var reason:String in alert.overrideReason) {
						keys += "\n" + reason;
					}
				}
				
				if (map.containsKey(keys)) {
					count = (map.get(keys) as int) + 1;
				}
				else {
					count = 1;
				}
				map.put(keys, count);
			}
			
			return map;
		}
		
		private static function isMapEqual(map1:IMap, map2:IMap):Boolean {
			if (map1.length != map2.length) {
				return false;
			}
			for each (var key:String in map1.keySet) {
				if (!map2.containsKey(key)) {
					return false;
				}
				if (map1.get(key) != map2.get(key)) {
					return false;
				}
			}
			return true;
		}
		
		public static function isAlertsEqual(list1:ListCollectionView, list2:ListCollectionView, checkOverrideReason:Boolean=false):Boolean {
			return isMapEqual(createAlertMap(list1, checkOverrideReason), createAlertMap(list2, checkOverrideReason));
		}
		
		public static function findPatOverrideAlert(findMoItem:MedProfileMoItem, mpMoItemList:ListCollectionView):MedProfileMoItemAlert {
			
			var latestPatAlert:MedProfileMoItemAlert = null;
			
			var findAlertMap:IMap = createAlertMap(findMoItem.patAlertList);
			
			for each (var mpMoItem:MedProfileMoItem in mpMoItemList) {
				if (mpMoItem.medProfileItem.status == MedProfileItemStatus.Deleted
				 || mpMoItem.status == MedProfileMoItemStatus.SysDeleted
				 || mpMoItem.displayNameSaltForm != findMoItem.displayNameSaltForm
				 || mpMoItem.patAlertList.length == 0) {
					continue;
				}
				
				var firstPatAlert:MedProfileMoItemAlert = mpMoItem.patAlertList.getItemAt(0) as MedProfileMoItemAlert;
				if (firstPatAlert.overrideReason.length == 0) {
					continue;
				}
				
				var itemAlertMap:IMap = createAlertMap(mpMoItem.patAlertList);
				if (!isMapEqual(findAlertMap, itemAlertMap)) {
					continue;
				}
				
				if (latestPatAlert == null || firstPatAlert.ackDate > latestPatAlert.ackDate) {
					latestPatAlert = firstPatAlert;
				}
			}
			
			return latestPatAlert;
		}
		
		public static function findDdiOverrideAlert(findMoItemAlert:MedProfileMoItemAlert, mpMoItemList:ListCollectionView, allMoItemList:ListCollectionView):MedProfileMoItemAlert {
			
			var latestDdiAlert:MedProfileMoItemAlert = null;
			
			var findMoItem:MedProfileMoItem = findMoItemAlert.medProfileMoItem;
			
			for each (var mpMoItem:MedProfileMoItem in mpMoItemList) {
				if (mpMoItem.medProfileItem.status == MedProfileItemStatus.Deleted
					|| mpMoItem.status == MedProfileMoItemStatus.SysDeleted
					|| mpMoItem.ddiAlertList.length == 0) {
					continue;
				}
				
				for each (var itemDdiAlert:MedProfileMoItemAlert in mpMoItem.ddiAlertList) {

					if (itemDdiAlert.overrideReason.length == 0
						|| itemDdiAlert.getDdiKey(allMoItemList) != findMoItemAlert.getDdiKey(allMoItemList)) {
						continue;
					}
					if (latestDdiAlert == null || itemDdiAlert.ackDate > latestDdiAlert.ackDate) {
						latestDdiAlert = itemDdiAlert;
					}
				}
			}
			
			return latestDdiAlert;
		}
		
		public static function replaceAlertByDrugKey(orgItemList:ListCollectionView, newMoItemList:ListCollectionView):void {
			
			var orgMoItemMap:IMap = new BasicMap();
			
			for each (var orgItem:MedProfileItem in orgItemList) {
				
				var orgMoItem:MedProfileMoItem = orgItem.medProfileMoItem;
				orgMoItem.clearAlert();
				
				var key:Number = (orgMoItem.medProfilePoItemList.getItemAt(0) as MedProfilePoItem).dmDrugLite.drugKey;

				if (orgMoItemMap.containsKey(key)) {
					(orgMoItemMap.get(key) as ListCollectionView).addItem(orgMoItem);
				}
				else {
					var list:ListCollectionView = new ArrayCollection();
					list.addItem(orgMoItem);
					orgMoItemMap.put(key, list);
				}
			}
			
			for each (var newMoItem:MedProfileMoItem in newMoItemList) {
				var key2:Number = (newMoItem.medProfilePoItemList.getItemAt(0) as MedProfilePoItem).dmDrugLite.drugKey;
				for each (var orgMoItem2:MedProfileMoItem in orgMoItemMap.get(key2)) {
					replaceAlert(orgMoItem2, newMoItem);
				}
			}
		}
		
		public static function replaceAlertAndStatusByItemNum(inOrgItemList:ListCollectionView, newMoItemList:ListCollectionView, activeList:ListCollectionView, inactiveList:ListCollectionView, prList:ListCollectionView):void {
			
			var orgItemList:ListCollectionView = new ArrayCollection();
			orgItemList.addAll(inOrgItemList);
			
			var newMoItemMap:IMap = new BasicMap();
			var deferIpmoeItemMap:IMap = new BasicMap();

			//LoopManualItem to Clear MDS Suspend Reason
			clearDdiDeferAlertEntity(newMoItemList, deferIpmoeItemMap);

			for each (var moItem:MedProfileMoItem in newMoItemList) {
				newMoItemMap.put(moItem.itemNum, moItem);
			}
			
			for each (var orgItem:MedProfileItem in orgItemList) {
				
				var orgMoItem:MedProfileMoItem = orgItem.medProfileMoItem;
				var newMoItem:MedProfileMoItem = newMoItemMap.get(orgMoItem.itemNum) as MedProfileMoItem;
				
				//Empty MDS result for IPMOE Item in non-IPMOE SaveProfile
				if( newMoItem == null ){
					continue;
				}
				
				if( orgItem.isManualItem ){
					if (orgItem.status != newMoItem.medProfileItem.status) {
						orgItem.modified = true;
						if (newMoItem.medProfileItem.status == MedProfileItemStatus.Vetted || newMoItem.medProfileItem.status == MedProfileItemStatus.Unvet) {
							orgItem.verifyDate = null;
							updatePurchaseRequestStatus(newMoItem.medProfileItem, prList);
						}
					}
					
					if (newMoItem.medProfileItem.status == MedProfileItemStatus.Deleted) {
						if (isNaN(orgItem.id)) {
							activeList.removeItemAt(activeList.getItemIndex(orgItem));
						}
						else {
							orgItem.status = MedProfileItemStatus.Deleted;
						}
						MedProfileUtils.findAndDeletePurchaseRequestList(prList, activeList, false);
					}
					else if (newMoItem.medProfileItem.status == MedProfileItemStatus.Discontinued) {
						orgItem.status = MedProfileItemStatus.Discontinued;
						orgMoItem.endDate =  new Date;
						if (!inactiveList.contains(orgItem.medProfileMoItem)) {
							orgItem.medProfileMoItem.mdsSuspendReason = null;
							VettingUtils.addToInactiveList(inactiveList, orgItem.medProfileMoItem);
							MedProfileUtils.findAndDeletePurchaseRequestList(prList, activeList, false);
						}
					}
					else {
						orgItem.status = newMoItem.medProfileItem.status;
						replaceAlert(orgMoItem, newMoItem);
					}
				}else{//IPMOE Item
					
					if( newMoItem.mdsSuspendReason != null ){
						orgItem.modified = true;
						if( deferIpmoeItemMap.containsKey(newMoItem.itemNum) ){
							orgItem.medProfileMoItem.mdsSuspendReason = newMoItem.mdsSuspendReason;
						}
						if (orgItem.status == MedProfileItemStatus.Verified || orgItem.status == MedProfileItemStatus.Vetted) {
							orgItem.status = newMoItem.medProfileItem.status;
						}else {
							orgItem.status = orgItem.orgStatus; 
						}
						updatePurchaseRequestStatus(orgItem, prList);
					}
				}
			}
		}
				
		public static function clearDdiDeferAlertEntity(newMoItemList:ListCollectionView, deferIpmoeItemMap:IMap):void{
			for each (var manualNewItem:MedProfileMoItem in newMoItemList) {
				if( !manualNewItem.isManualItem ){
					continue;
				}
				
				if( manualNewItem.medProfileItem.status == MedProfileItemStatus.Deleted || manualNewItem.medProfileItem.status == MedProfileItemStatus.Discontinued ){
					continue;
				}
				
				var containsDdiDeferAlert:Boolean = false;
				var containsPatDeferAlert:Boolean = false;
				var newAlertList:ArrayCollection = new ArrayCollection(manualNewItem.medProfileMoItemAlertList.toArray());
				
				for each (var newAlert:MedProfileMoItemAlert in newAlertList){
					if( newAlert.overrideReason == null || newAlert.overrideReason.length == 0){
						if( newAlert.isDdiAlert() ){
							var manualDdim:AlertDdim = newAlert.alert as AlertDdim;
							if( manualDdim.isCrossDdi() ){
								var otherItemNum:Number = manualDdim.itemNum1 < manualDdim.itemNum2 ? manualDdim.itemNum1 : manualDdim.itemNum2;
								deferIpmoeItemMap.put(otherItemNum, otherItemNum);
							}
							containsDdiDeferAlert = true;
							manualNewItem.medProfileMoItemAlertList.removeItemAt(manualNewItem.medProfileMoItemAlertList.getItemIndex(newAlert));
						}else{
							containsPatDeferAlert = true;
						}
					}
				}
				
				if( manualNewItem.mdsSuspendReason != null && !containsPatDeferAlert && !containsDdiDeferAlert ){
					manualNewItem.mdsSuspendReason = null;
				}
			}
		}
		
		public static function updatePurchaseRequestStatus(mpi:MedProfileItem, prList:ListCollectionView):void{
			for each(var existPr:PurchaseRequest in prList){
				if( existPr.invoiceStatus == InvoiceStatus.SysDeleted ){
					continue;
				}
				if( existPr.invoiceDate == null && existPr.mpiOrgItemNum == mpi.orgItemNum){
					if( existPr.mpPharmInfo.medProfileItemStatus != mpi.status ){
						existPr.mpPharmInfo.medProfileItemStatus = mpi.status;
						existPr.itemStatus = existPr.mpPharmInfo.medProfileItemStatus.displayValue;
					}
				}
			}
		}
			
		public static function replaceAlert(destItem:MedProfileMoItem, sourceItem:MedProfileMoItem):void {

			var orgAlertFlag:Boolean = destItem.alertFlag;
			var orgMdsSuspendReason:String = destItem.mdsSuspendReason;
			var orgAlertList:ListCollectionView = new ArrayCollection(destItem.medProfileMoItemAlertList.toArray());
			
			destItem.clearAlert();
			
			if (sourceItem != null) {
				for each (var mpAlert:MedProfileMoItemAlert in sourceItem.medProfileMoItemAlertList) {
					destItem.medProfileMoItemAlertList.addItem(mpAlert);
					mpAlert.medProfileMoItem = destItem;
				}
				destItem.updateAlertFlag();
				destItem.mdsSuspendReason = sourceItem.mdsSuspendReason;
			}

			destItem.medProfileItem.modified = destItem.medProfileItem.modified
				|| (orgAlertFlag != destItem.alertFlag)
				|| (orgMdsSuspendReason != destItem.mdsSuspendReason)
				|| !MdsUtils.isAlertsEqual(orgAlertList, destItem.medProfileMoItemAlertList, true);
		}
		
		public static function replacePatAlert(destItem:MedProfileMoItem, sourceItem:MedProfileMoItem):void {

			var orgAlertFlag:Boolean = destItem.alertFlag;
			var orgMdsSuspendReason:String = destItem.mdsSuspendReason;
			var orgAlertList:ListCollectionView = new ArrayCollection(destItem.medProfileMoItemAlertList.toArray());

			var alertList:ListCollectionView = new ArrayCollection(destItem.ddiAlertList.toArray());
			if (sourceItem != null) {
				alertList.addAll(sourceItem.medProfileMoItemAlertList);
			}
			
			destItem.clearAlert();
			
			for each (var mpAlert:MedProfileMoItemAlert in alertList) {
				destItem.medProfileMoItemAlertList.addItem(mpAlert);
				mpAlert.medProfileMoItem = destItem;
			}
			destItem.updateAlertFlag();
			
			destItem.medProfileItem.modified = destItem.medProfileItem.modified
				|| (orgAlertFlag != destItem.alertFlag)
				|| (orgMdsSuspendReason != destItem.mdsSuspendReason)
				|| !MdsUtils.isAlertsEqual(orgAlertList, destItem.medProfileMoItemAlertList, true);
		}
		
		public static function replaceItemsAckAndReason(destAlertList:ListCollectionView, sourceAlert:AlertEntity):void {
			for each (var destAlert:AlertEntity in destAlertList) {
				replaceItemAckAndReason(destAlert, sourceAlert);
			}
		}
		
		public static function replaceItemAckAndReason(destAlert:AlertEntity, sourceAlert:AlertEntity):void {
			destAlert.ackDate = sourceAlert.ackDate;
			destAlert.ackUser = sourceAlert.ackUser;
			destAlert.overrideReason.removeAll();
			destAlert.overrideReason.addAll(sourceAlert.overrideReason);
		}
		
		public static function isGroupWithin24Hrs(medProfileItemList:ListCollectionView, groupNum:Number):Boolean {
			for each (var medProfileItem:MedProfileItem in medProfileItemList) {
				if (medProfileItem.groupNum == groupNum) {
					if (!isWithin24Hrs(medProfileItem.createDate)) {
						return false;
					}
				}
			}
			return true;
		}
		
		public static function isWithin24Hrs(date:Date):Boolean {
			if (date == null) {
				return true;
			}
			var timeElapse:Number = (new Date()).getTime() - date.getTime();
			return timeElapse < (24 * MINUTES_PER_HOUR * SECOND_PER_MINUTES * MILLISECOND_PER_SECOND);
		}
		
		public static function removeRelatedDdi(mpMoItem:MedProfileMoItem, mpItemList:ListCollectionView):void {
			
			for each (var mpMoItemAlert:MedProfileMoItemAlert in mpMoItem.ddiAlertList) {
				
				var alertDdim:AlertDdim = mpMoItemAlert.alert as AlertDdim;
				var otherItemNum:Number = mpMoItem.itemNum == alertDdim.itemNum1 ? alertDdim.itemNum2 : alertDdim.itemNum1;
				
				for each (var otherMpItem:MedProfileItem in mpItemList) {
					if (otherMpItem.status == MedProfileItemStatus.Deleted || otherMpItem.status == MedProfileItemStatus.Discontinued) {
						continue;
					}
					var otherMpMoItem:MedProfileMoItem = otherMpItem.medProfileMoItem;
					
					if (otherMpMoItem.itemNum == otherItemNum) {
						
						var removeIndex:Number = NaN;
						
						for each (var otherAlert:MedProfileMoItemAlert in otherMpMoItem.ddiAlertList) {
							var otherAlertDdim:AlertDdim = otherAlert.alert as AlertDdim;
							
							if (otherAlertDdim.isEqualTo(alertDdim)) {
								removeIndex = otherMpMoItem.medProfileMoItemAlertList.getItemIndex(otherAlert);
								break;
							}
						}
						
						if (!isNaN(removeIndex)) {
							otherMpMoItem.medProfileMoItemAlertList.removeItemAt(removeIndex);
							otherMpMoItem.updateAlertFlag();
							otherMpMoItem.medProfileItem.modified = true;
						}
						
						break;
					}
				}
			}
		}
		
		public static function updateRelatedDdi(mpMoItemAlert:MedProfileMoItemAlert, mpItemList:ListCollectionView):void {
			
			var alertDdim:AlertDdim = mpMoItemAlert.alert as AlertDdim;
			var otherItemNum:Number = mpMoItemAlert.medProfileMoItem.itemNum == alertDdim.itemNum1 ? alertDdim.itemNum2 : alertDdim.itemNum1;
				
			for each (var otherMpItem:MedProfileItem in mpItemList) {

				var otherMpMoItem:MedProfileMoItem = otherMpItem.medProfileMoItem;
				if (otherMpMoItem.itemNum != otherItemNum) {
					continue;
				}
				
				if( !otherMpItem.medProfileMoItem.isManualItem ){
					continue;
				}
				
				for each (var otherAlert:MedProfileMoItemAlert in otherMpMoItem.ddiAlertList) {

					var otherAlertDdim:AlertDdim = otherAlert.alert as AlertDdim;
					if (!otherAlertDdim.isEqualTo(alertDdim)) {
						continue;
					}

					otherAlert.overrideReason.removeAll();
					otherAlert.overrideReason.addAll(mpMoItemAlert.overrideReason);
					otherAlert.ackDate = mpMoItemAlert.ackDate;
					otherAlert.ackUser = mpMoItemAlert.ackUser;
					otherAlert.medProfileMoItem.medProfileItem.modified = true;
					break;
				}
			}
		}

		public static function constructSteroidTagInfo(st:SteroidTag):String {
			return "Created By: "  + (st.createUser==null ? "" : st.createUser) + " (" + st.patHospCode + ")"
				+ "\nOrder No.: "   + st.orderNum
				+ "\nCreate Date: " + (st.createDate==null ? "" : DATE_TIME_FORMATTER.format(st.createDate))
				+ "\nExpire Date: " + (st.expiryDate==null ? "" : DATE_TIME_FORMATTER.format(st.expiryDate))
				+ "\nIndication: "  + st.getIndication();
		}
	}
}