/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (EhrPatient.as).
 */

package hk.org.ha.model.pms.vo.ehr {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import hk.org.ha.model.pms.udt.Gender;
    import org.granite.util.Enum;

    [Bindable]
    public class EhrPatientBase implements IExternalizable {

        private var _dob:Date;
        private var _documentType:String;
        private var _ehrEndDate:Date;
        private var _ehrNum:String;
        private var _ehrStartDate:Date;
        private var _givenName:String;
        private var _hkid:String;
        private var _nonHaIndicator:Boolean;
        private var _otherDocNo:String;
        private var _patKey:String;
        private var _sex:Gender;
        private var _surname:String;

        public function set dob(value:Date):void {
            _dob = value;
        }
        public function get dob():Date {
            return _dob;
        }

        public function set documentType(value:String):void {
            _documentType = value;
        }
        public function get documentType():String {
            return _documentType;
        }

        public function set ehrEndDate(value:Date):void {
            _ehrEndDate = value;
        }
        public function get ehrEndDate():Date {
            return _ehrEndDate;
        }

        public function set ehrNum(value:String):void {
            _ehrNum = value;
        }
        public function get ehrNum():String {
            return _ehrNum;
        }

        public function set ehrStartDate(value:Date):void {
            _ehrStartDate = value;
        }
        public function get ehrStartDate():Date {
            return _ehrStartDate;
        }

        public function set givenName(value:String):void {
            _givenName = value;
        }
        public function get givenName():String {
            return _givenName;
        }

        public function set hkid(value:String):void {
            _hkid = value;
        }
        public function get hkid():String {
            return _hkid;
        }

        public function set nonHaIndicator(value:Boolean):void {
            _nonHaIndicator = value;
        }
        public function get nonHaIndicator():Boolean {
            return _nonHaIndicator;
        }

        public function set otherDocNo(value:String):void {
            _otherDocNo = value;
        }
        public function get otherDocNo():String {
            return _otherDocNo;
        }

        public function set patKey(value:String):void {
            _patKey = value;
        }
        public function get patKey():String {
            return _patKey;
        }

        public function set sex(value:Gender):void {
            _sex = value;
        }
        public function get sex():Gender {
            return _sex;
        }

        public function set surname(value:String):void {
            _surname = value;
        }
        public function get surname():String {
            return _surname;
        }

        public function readExternal(input:IDataInput):void {
            _dob = input.readObject() as Date;
            _documentType = input.readObject() as String;
            _ehrEndDate = input.readObject() as Date;
            _ehrNum = input.readObject() as String;
            _ehrStartDate = input.readObject() as Date;
            _givenName = input.readObject() as String;
            _hkid = input.readObject() as String;
            _nonHaIndicator = input.readObject() as Boolean;
            _otherDocNo = input.readObject() as String;
            _patKey = input.readObject() as String;
            _sex = Enum.readEnum(input) as Gender;
            _surname = input.readObject() as String;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_dob);
            output.writeObject(_documentType);
            output.writeObject(_ehrEndDate);
            output.writeObject(_ehrNum);
            output.writeObject(_ehrStartDate);
            output.writeObject(_givenName);
            output.writeObject(_hkid);
            output.writeObject(_nonHaIndicator);
            output.writeObject(_otherDocNo);
            output.writeObject(_patKey);
            output.writeObject(_sex);
            output.writeObject(_surname);
        }
    }
}