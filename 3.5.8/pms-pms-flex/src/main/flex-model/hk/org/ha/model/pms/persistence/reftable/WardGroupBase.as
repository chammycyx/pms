/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (WardGroup.as).
 */

package hk.org.ha.model.pms.persistence.reftable {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.getQualifiedClassName;
    import hk.org.ha.model.pms.persistence.VersionEntity;
    import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
    import mx.collections.ListCollectionView;
    import mx.core.IUID;
    import mx.data.utils.Managed;
    import mx.utils.UIDUtil;
    import org.granite.collections.IPersistentCollection;
    import org.granite.meta;
    import org.granite.tide.IEntityManager;
    import org.granite.tide.IPropertyHolder;

    use namespace meta;

    [Managed]
    public class WardGroupBase extends VersionEntity {

        [Transient]
        meta var entityManager:IEntityManager = null;

        private var __initialized:Boolean = true;
        private var __detachedState:String = null;

        private var _id:Number;
        private var _wardGroupCode:String;
        private var _wardGroupItemList:ListCollectionView;
        private var _workstoreGroup:WorkstoreGroup;

        meta override function isInitialized(name:String = null):Boolean {
            if (!name)
                return __initialized;

            var property:* = this[name];
            return (
                (!(property is WardGroup) || (property as WardGroup).meta::isInitialized()) &&
                (!(property is IPersistentCollection) || (property as IPersistentCollection).isInitialized())
            );
        }
        
        [Bindable(event="dirtyChange")]
		public function get meta_dirty():Boolean {
			return Managed.getProperty(this, "meta_dirty", false);
		}

        public function set id(value:Number):void {
            _id = value;
        }
        [Id]
        public function get id():Number {
            return _id;
        }

        public function set wardGroupCode(value:String):void {
            _wardGroupCode = value;
        }
        public function get wardGroupCode():String {
            return _wardGroupCode;
        }

        public function set wardGroupItemList(value:ListCollectionView):void {
            _wardGroupItemList = value;
        }
        public function get wardGroupItemList():ListCollectionView {
            return _wardGroupItemList;
        }

        public function set workstoreGroup(value:WorkstoreGroup):void {
            _workstoreGroup = value;
        }
        public function get workstoreGroup():WorkstoreGroup {
            return _workstoreGroup;
        }

        public function set uid(value:String):void {
            // noop...
        }
        public function get uid():String {
            if (isNaN(_id))
                return UIDUtil.createUID();
            return getQualifiedClassName(this) + "#[" + String(_id) + "]";
        }

        meta override function merge(em:IEntityManager, obj:*):void {
            var src:WardGroupBase = WardGroupBase(obj);
            __initialized = src.__initialized;
            __detachedState = src.__detachedState;
            if (meta::isInitialized()) {
                super.meta::merge(em, obj);
               em.meta_mergeExternal(src._id, _id, null, this, 'id', function setter(o:*):void{_id = o as Number}, false);
               em.meta_mergeExternal(src._wardGroupCode, _wardGroupCode, null, this, 'wardGroupCode', function setter(o:*):void{_wardGroupCode = o as String}, false);
               em.meta_mergeExternal(src._wardGroupItemList, _wardGroupItemList, null, this, 'wardGroupItemList', function setter(o:*):void{_wardGroupItemList = o as ListCollectionView}, false);
               em.meta_mergeExternal(src._workstoreGroup, _workstoreGroup, null, this, 'workstoreGroup', function setter(o:*):void{_workstoreGroup = o as WorkstoreGroup}, false);
            }
            else {
               em.meta_mergeExternal(src._id, _id, null, this, 'id', function setter(o:*):void{_id = o as Number});
            }
        }

        public override function readExternal(input:IDataInput):void {
            __initialized = input.readObject() as Boolean;
            __detachedState = input.readObject() as String;
            if (meta::isInitialized()) {
                super.readExternal(input);
                _id = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _wardGroupCode = input.readObject() as String;
                _wardGroupItemList = input.readObject() as ListCollectionView;
                _workstoreGroup = input.readObject() as WorkstoreGroup;
            }
            else {
                _id = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            }
        }

        public override function writeExternal(output:IDataOutput):void {
            output.writeObject(__initialized);
            output.writeObject(__detachedState);
            if (meta::isInitialized()) {
                super.writeExternal(output);
                output.writeObject((_id is IPropertyHolder) ? IPropertyHolder(_id).object : _id);
                output.writeObject((_wardGroupCode is IPropertyHolder) ? IPropertyHolder(_wardGroupCode).object : _wardGroupCode);
                output.writeObject((_wardGroupItemList is IPropertyHolder) ? IPropertyHolder(_wardGroupItemList).object : _wardGroupItemList);
                output.writeObject((_workstoreGroup is IPropertyHolder) ? IPropertyHolder(_workstoreGroup).object : _workstoreGroup);
            }
            else {
                output.writeObject(_id);
            }
        }
    }
}
