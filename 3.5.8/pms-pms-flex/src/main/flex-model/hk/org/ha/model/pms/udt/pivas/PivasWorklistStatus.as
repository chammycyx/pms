/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.udt.pivas {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.udt.pivas.PivasWorklistStatus")]
    public class PivasWorklistStatus extends Enum {

        public static const New:PivasWorklistStatus = new PivasWorklistStatus("New", _, "N", "New");
        public static const Refill:PivasWorklistStatus = new PivasWorklistStatus("Refill", _, "R", "Refill");
        public static const Prepared:PivasWorklistStatus = new PivasWorklistStatus("Prepared", _, "P", "Prepared");
        public static const Completed:PivasWorklistStatus = new PivasWorklistStatus("Completed", _, "C", "Completed");
        public static const Deleted:PivasWorklistStatus = new PivasWorklistStatus("Deleted", _, "D", "Deleted");
        public static const SysDeleted:PivasWorklistStatus = new PivasWorklistStatus("SysDeleted", _, "X", "SysDeleted");

        function PivasWorklistStatus(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || New.name), restrictor, (dataValue || New.dataValue), (displayValue || New.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [New, Refill, Prepared, Completed, Deleted, SysDeleted];
        }

        public static function valueOf(name:String):PivasWorklistStatus {
            return PivasWorklistStatus(New.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):PivasWorklistStatus {
            return PivasWorklistStatus(New.dataConstantOf(dataValue));
        }
    }
}