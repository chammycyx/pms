/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (WardCodeMap.as).
 */

package hk.org.ha.model.pms.vo.reftable {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import org.granite.collections.IMap;

    [Bindable]
    public class WardCodeMapBase implements IExternalizable {

        private var _wardCodeMap:IMap;

        public function set wardCodeMap(value:IMap):void {
            _wardCodeMap = value;
        }
        public function get wardCodeMap():IMap {
            return _wardCodeMap;
        }

        public function readExternal(input:IDataInput):void {
            _wardCodeMap = input.readObject() as IMap;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_wardCodeMap);
        }
    }
}