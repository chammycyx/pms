/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (PatientListRpt.as).
 */

package hk.org.ha.model.pms.vo.report {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import mx.collections.ListCollectionView;

    [Bindable]
    public class PatientListRptBase implements IExternalizable {

        private var _patientListDtlList:ListCollectionView;
        private var _wardCode:String;

        public function set patientListDtlList(value:ListCollectionView):void {
            _patientListDtlList = value;
        }
        public function get patientListDtlList():ListCollectionView {
            return _patientListDtlList;
        }

        public function set wardCode(value:String):void {
            _wardCode = value;
        }
        public function get wardCode():String {
            return _wardCode;
        }

        public function readExternal(input:IDataInput):void {
            _patientListDtlList = input.readObject() as ListCollectionView;
            _wardCode = input.readObject() as String;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_patientListDtlList);
            output.writeObject(_wardCode);
        }
    }
}