/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.udt.report {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.udt.report.PivasExportType")]
    public class PivasExportType extends Enum {

        public static const PivasConsumptionExport:PivasExportType = new PivasExportType("PivasConsumptionExport", _, "PivasConsumptionXls", "PivasConsumptionXls");
        public static const PivasPatientDispReport:PivasExportType = new PivasExportType("PivasPatientDispReport", _, "PivasPatientDispXls", "PivasPatientDispXls");
        public static const PivasBatchDispReport:PivasExportType = new PivasExportType("PivasBatchDispReport", _, "PivasBatchDispXls", "PivasBatchDispXls");
        public static const PivasWorkloadSummaryPatientRpt:PivasExportType = new PivasExportType("PivasWorkloadSummaryPatientRpt", _, "PivasWorkloadSummaryPatientXls", "PivasWorkloadSummaryPatientXls");
        public static const PivasWorkloadSummaryBatchRpt:PivasExportType = new PivasExportType("PivasWorkloadSummaryBatchRpt", _, "PivasWorkloadSummaryBatchXls", "PivasWorkloadSummaryBatchXls");

        function PivasExportType(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || PivasConsumptionExport.name), restrictor, (dataValue || PivasConsumptionExport.dataValue), (displayValue || PivasConsumptionExport.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [PivasConsumptionExport, PivasPatientDispReport, PivasBatchDispReport, PivasWorkloadSummaryPatientRpt, PivasWorkloadSummaryBatchRpt];
        }

        public static function valueOf(name:String):PivasExportType {
            return PivasExportType(PivasConsumptionExport.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):PivasExportType {
            return PivasExportType(PivasConsumptionExport.dataConstantOf(dataValue));
        }
    }
}