/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (PivasWorklistResponse.as).
 */

package hk.org.ha.model.pms.vo.pivas.worklist {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import mx.collections.ListCollectionView;

    [Bindable]
    public class PivasWorklistResponseBase implements IExternalizable {

        private var _errorCode:String;
        private var _errorParamList:ListCollectionView;
        private var _errorRemark:String;

        public function set errorCode(value:String):void {
            _errorCode = value;
        }
        public function get errorCode():String {
            return _errorCode;
        }

        public function set errorParamList(value:ListCollectionView):void {
            _errorParamList = value;
        }
        public function get errorParamList():ListCollectionView {
            return _errorParamList;
        }

        public function set errorRemark(value:String):void {
            _errorRemark = value;
        }
        public function get errorRemark():String {
            return _errorRemark;
        }

        public function readExternal(input:IDataInput):void {
            _errorCode = input.readObject() as String;
            _errorParamList = input.readObject() as ListCollectionView;
            _errorRemark = input.readObject() as String;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_errorCode);
            output.writeObject(_errorParamList);
            output.writeObject(_errorRemark);
        }
    }
}