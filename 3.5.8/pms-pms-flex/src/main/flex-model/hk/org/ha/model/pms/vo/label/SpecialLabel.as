/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * NOTE: this file is only generated if it does not exist. You may safely put
 * your custom code here.
 */

package hk.org.ha.model.pms.vo.label {

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.vo.label.SpecialLabel")]
    public class SpecialLabel extends SpecialLabelBase {
		private var _newRecord:Boolean;
		private var _bold:Boolean;
		
		public function get newRecord():Boolean
		{
			return _newRecord;
		}
		
		public function set newRecord(newRecord:Boolean):void
		{
			_newRecord = newRecord;
		}
		
		public function get bold():Boolean
		{
			return _bold;
		}
		
		public function set bold( bold:Boolean):void
		{
			_bold = bold;
		}

    }
}