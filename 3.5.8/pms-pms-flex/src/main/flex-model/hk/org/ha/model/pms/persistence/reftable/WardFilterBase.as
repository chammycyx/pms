/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (WardFilter.as).
 */

package hk.org.ha.model.pms.persistence.reftable {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.getQualifiedClassName;
    import hk.org.ha.model.pms.persistence.VersionEntity;
    import hk.org.ha.model.pms.persistence.corp.Workstore;
    import mx.core.IUID;
    import mx.data.utils.Managed;
    import mx.utils.UIDUtil;
    import org.granite.collections.IPersistentCollection;
    import org.granite.meta;
    import org.granite.tide.IEntityManager;
    import org.granite.tide.IPropertyHolder;

    use namespace meta;

    [Managed]
    public class WardFilterBase extends VersionEntity {

        [Transient]
        meta var entityManager:IEntityManager = null;

        private var __initialized:Boolean = true;
        private var __detachedState:String = null;

        private var _endTime:String;
        private var _friFlag:Boolean;
        private var _id:Number;
        private var _markDelete:Boolean;
        private var _monFlag:Boolean;
        private var _satFlag:Boolean;
        private var _startTime:String;
        private var _sunFlag:Boolean;
        private var _thuFlag:Boolean;
        private var _tueFlag:Boolean;
        private var _wardCode:String;
        private var _wedFlag:Boolean;
        private var _workstore:Workstore;

        meta override function isInitialized(name:String = null):Boolean {
            if (!name)
                return __initialized;

            var property:* = this[name];
            return (
                (!(property is WardFilter) || (property as WardFilter).meta::isInitialized()) &&
                (!(property is IPersistentCollection) || (property as IPersistentCollection).isInitialized())
            );
        }
        
        [Bindable(event="dirtyChange")]
		public function get meta_dirty():Boolean {
			return Managed.getProperty(this, "meta_dirty", false);
		}

        public function set endTime(value:String):void {
            _endTime = value;
        }
        public function get endTime():String {
            return _endTime;
        }

        public function set friFlag(value:Boolean):void {
            _friFlag = value;
        }
        public function get friFlag():Boolean {
            return _friFlag;
        }

        public function set id(value:Number):void {
            _id = value;
        }
        [Id]
        public function get id():Number {
            return _id;
        }

        public function set markDelete(value:Boolean):void {
            _markDelete = value;
        }
        public function get markDelete():Boolean {
            return _markDelete;
        }

        public function set monFlag(value:Boolean):void {
            _monFlag = value;
        }
        public function get monFlag():Boolean {
            return _monFlag;
        }

        public function set satFlag(value:Boolean):void {
            _satFlag = value;
        }
        public function get satFlag():Boolean {
            return _satFlag;
        }

        public function set startTime(value:String):void {
            _startTime = value;
        }
        public function get startTime():String {
            return _startTime;
        }

        public function set sunFlag(value:Boolean):void {
            _sunFlag = value;
        }
        public function get sunFlag():Boolean {
            return _sunFlag;
        }

        public function set thuFlag(value:Boolean):void {
            _thuFlag = value;
        }
        public function get thuFlag():Boolean {
            return _thuFlag;
        }

        public function set tueFlag(value:Boolean):void {
            _tueFlag = value;
        }
        public function get tueFlag():Boolean {
            return _tueFlag;
        }

        public function set wardCode(value:String):void {
            _wardCode = value;
        }
        public function get wardCode():String {
            return _wardCode;
        }

        public function set wedFlag(value:Boolean):void {
            _wedFlag = value;
        }
        public function get wedFlag():Boolean {
            return _wedFlag;
        }

        public function set workstore(value:Workstore):void {
            _workstore = value;
        }
        public function get workstore():Workstore {
            return _workstore;
        }

        public function set uid(value:String):void {
            // noop...
        }
        public function get uid():String {
            if (isNaN(_id))
                return UIDUtil.createUID();
            return getQualifiedClassName(this) + "#[" + String(_id) + "]";
        }

        meta override function merge(em:IEntityManager, obj:*):void {
            var src:WardFilterBase = WardFilterBase(obj);
            __initialized = src.__initialized;
            __detachedState = src.__detachedState;
            if (meta::isInitialized()) {
                super.meta::merge(em, obj);
               em.meta_mergeExternal(src._endTime, _endTime, null, this, 'endTime', function setter(o:*):void{_endTime = o as String}, false);
               em.meta_mergeExternal(src._friFlag, _friFlag, null, this, 'friFlag', function setter(o:*):void{_friFlag = o as Boolean}, false);
               em.meta_mergeExternal(src._id, _id, null, this, 'id', function setter(o:*):void{_id = o as Number}, false);
               em.meta_mergeExternal(src._markDelete, _markDelete, null, this, 'markDelete', function setter(o:*):void{_markDelete = o as Boolean}, false);
               em.meta_mergeExternal(src._monFlag, _monFlag, null, this, 'monFlag', function setter(o:*):void{_monFlag = o as Boolean}, false);
               em.meta_mergeExternal(src._satFlag, _satFlag, null, this, 'satFlag', function setter(o:*):void{_satFlag = o as Boolean}, false);
               em.meta_mergeExternal(src._startTime, _startTime, null, this, 'startTime', function setter(o:*):void{_startTime = o as String}, false);
               em.meta_mergeExternal(src._sunFlag, _sunFlag, null, this, 'sunFlag', function setter(o:*):void{_sunFlag = o as Boolean}, false);
               em.meta_mergeExternal(src._thuFlag, _thuFlag, null, this, 'thuFlag', function setter(o:*):void{_thuFlag = o as Boolean}, false);
               em.meta_mergeExternal(src._tueFlag, _tueFlag, null, this, 'tueFlag', function setter(o:*):void{_tueFlag = o as Boolean}, false);
               em.meta_mergeExternal(src._wardCode, _wardCode, null, this, 'wardCode', function setter(o:*):void{_wardCode = o as String}, false);
               em.meta_mergeExternal(src._wedFlag, _wedFlag, null, this, 'wedFlag', function setter(o:*):void{_wedFlag = o as Boolean}, false);
               em.meta_mergeExternal(src._workstore, _workstore, null, this, 'workstore', function setter(o:*):void{_workstore = o as Workstore}, false);
            }
            else {
               em.meta_mergeExternal(src._id, _id, null, this, 'id', function setter(o:*):void{_id = o as Number});
            }
        }

        public override function readExternal(input:IDataInput):void {
            __initialized = input.readObject() as Boolean;
            __detachedState = input.readObject() as String;
            if (meta::isInitialized()) {
                super.readExternal(input);
                _endTime = input.readObject() as String;
                _friFlag = input.readObject() as Boolean;
                _id = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _markDelete = input.readObject() as Boolean;
                _monFlag = input.readObject() as Boolean;
                _satFlag = input.readObject() as Boolean;
                _startTime = input.readObject() as String;
                _sunFlag = input.readObject() as Boolean;
                _thuFlag = input.readObject() as Boolean;
                _tueFlag = input.readObject() as Boolean;
                _wardCode = input.readObject() as String;
                _wedFlag = input.readObject() as Boolean;
                _workstore = input.readObject() as Workstore;
            }
            else {
                _id = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            }
        }

        public override function writeExternal(output:IDataOutput):void {
            output.writeObject(__initialized);
            output.writeObject(__detachedState);
            if (meta::isInitialized()) {
                super.writeExternal(output);
                output.writeObject((_endTime is IPropertyHolder) ? IPropertyHolder(_endTime).object : _endTime);
                output.writeObject((_friFlag is IPropertyHolder) ? IPropertyHolder(_friFlag).object : _friFlag);
                output.writeObject((_id is IPropertyHolder) ? IPropertyHolder(_id).object : _id);
                output.writeObject((_markDelete is IPropertyHolder) ? IPropertyHolder(_markDelete).object : _markDelete);
                output.writeObject((_monFlag is IPropertyHolder) ? IPropertyHolder(_monFlag).object : _monFlag);
                output.writeObject((_satFlag is IPropertyHolder) ? IPropertyHolder(_satFlag).object : _satFlag);
                output.writeObject((_startTime is IPropertyHolder) ? IPropertyHolder(_startTime).object : _startTime);
                output.writeObject((_sunFlag is IPropertyHolder) ? IPropertyHolder(_sunFlag).object : _sunFlag);
                output.writeObject((_thuFlag is IPropertyHolder) ? IPropertyHolder(_thuFlag).object : _thuFlag);
                output.writeObject((_tueFlag is IPropertyHolder) ? IPropertyHolder(_tueFlag).object : _tueFlag);
                output.writeObject((_wardCode is IPropertyHolder) ? IPropertyHolder(_wardCode).object : _wardCode);
                output.writeObject((_wedFlag is IPropertyHolder) ? IPropertyHolder(_wedFlag).object : _wedFlag);
                output.writeObject((_workstore is IPropertyHolder) ? IPropertyHolder(_workstore).object : _workstore);
            }
            else {
                output.writeObject(_id);
            }
        }
    }
}
