/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.udt.machine {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.udt.machine.ScriptProPrintType")]
    public class ScriptProPrintType extends Enum {

        public static const New:ScriptProPrintType = new ScriptProPrintType("New", _, "N", "Original");
        public static const Reprint:ScriptProPrintType = new ScriptProPrintType("Reprint", _, "R", "Reprint");

        function ScriptProPrintType(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || New.name), restrictor, (dataValue || New.dataValue), (displayValue || New.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [New, Reprint];
        }

        public static function valueOf(name:String):ScriptProPrintType {
            return ScriptProPrintType(New.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):ScriptProPrintType {
            return ScriptProPrintType(New.dataConstantOf(dataValue));
        }
    }
}