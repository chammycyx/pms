/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (AdrLog.as).
 */

package hk.org.ha.model.pms.vo.alert.mds {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import mx.collections.ListCollectionView;

    [Bindable]
    public class AdrLogBase implements IExternalizable {

        private var _adr:String;
        private var _hicSeqNumList:ListCollectionView;
        private var _hiclSeqNum:String;

        public function set adr(value:String):void {
            _adr = value;
        }
        public function get adr():String {
            return _adr;
        }

        public function set hicSeqNumList(value:ListCollectionView):void {
            _hicSeqNumList = value;
        }
        public function get hicSeqNumList():ListCollectionView {
            return _hicSeqNumList;
        }

        public function set hiclSeqNum(value:String):void {
            _hiclSeqNum = value;
        }
        public function get hiclSeqNum():String {
            return _hiclSeqNum;
        }

        public function readExternal(input:IDataInput):void {
            _adr = input.readObject() as String;
            _hicSeqNumList = input.readObject() as ListCollectionView;
            _hiclSeqNum = input.readObject() as String;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_adr);
            output.writeObject(_hicSeqNumList);
            output.writeObject(_hiclSeqNum);
        }
    }
}