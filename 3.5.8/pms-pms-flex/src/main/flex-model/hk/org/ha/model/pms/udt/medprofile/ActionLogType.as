/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.udt.medprofile {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.udt.medprofile.ActionLogType")]
    public class ActionLogType extends Enum {

        public static const Suspend:ActionLogType = new ActionLogType("Suspend", _, "S", "Suspend");
        public static const ResumeSuspend:ActionLogType = new ActionLogType("ResumeSuspend", _, "RS", "ResumeSuspend");
        public static const Pending:ActionLogType = new ActionLogType("Pending", _, "P", "Pending");
        public static const Override:ActionLogType = new ActionLogType("Override", _, "O", "Override");
        public static const ResumePending:ActionLogType = new ActionLogType("ResumePending", _, "RP", "ResumePending");

        function ActionLogType(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || Suspend.name), restrictor, (dataValue || Suspend.dataValue), (displayValue || Suspend.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [Suspend, ResumeSuspend, Pending, Override, ResumePending];
        }

        public static function valueOf(name:String):ActionLogType {
            return ActionLogType(Suspend.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):ActionLogType {
            return ActionLogType(Suspend.dataConstantOf(dataValue));
        }
    }
}