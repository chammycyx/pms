/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.udt.vetting {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.udt.vetting.PrnPercentage")]
    public class PrnPercentage extends Enum {

        public static const Zero:PrnPercentage = new PrnPercentage("Zero", _, "0", " ");
        public static const TwentyFive:PrnPercentage = new PrnPercentage("TwentyFive", _, "25", "25%");
        public static const Fifty:PrnPercentage = new PrnPercentage("Fifty", _, "50", "50%");
        public static const SeventyFive:PrnPercentage = new PrnPercentage("SeventyFive", _, "75", "75%");
        public static const Hundred:PrnPercentage = new PrnPercentage("Hundred", _, "100", "100%");

        function PrnPercentage(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || Zero.name), restrictor, (dataValue || Zero.dataValue), (displayValue || Zero.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [Zero, TwentyFive, Fifty, SeventyFive, Hundred];
        }

        public static function valueOf(name:String):PrnPercentage {
            return PrnPercentage(Zero.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):PrnPercentage {
            return PrnPercentage(Zero.dataConstantOf(dataValue));
        }
    }
}