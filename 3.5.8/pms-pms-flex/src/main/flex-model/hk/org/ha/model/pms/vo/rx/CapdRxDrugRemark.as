/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * NOTE: this file is only generated if it does not exist. You may safely put
 * your custom code here.
 */

package hk.org.ha.model.pms.vo.rx {
	import mx.collections.ListCollectionView;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.vo.rx.CapdRxDrugRemark")]
    public class CapdRxDrugRemark extends CapdRxDrugRemarkBase {
		
		public function get capdItemRemarkList():ListCollectionView {
			return capdItemList;
		}		
    }
}