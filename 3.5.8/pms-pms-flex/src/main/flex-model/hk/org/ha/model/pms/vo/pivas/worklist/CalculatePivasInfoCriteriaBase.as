/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (CalculatePivasInfoCriteria.as).
 */

package hk.org.ha.model.pms.vo.pivas.worklist {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import hk.org.ha.model.pms.persistence.pivas.PivasWorklist;
    import mx.collections.ListCollectionView;

    [Bindable]
    public class CalculatePivasInfoCriteriaBase implements IExternalizable {

        private var _changeAdjQtyFlag:Boolean;
        private var _changeIssueQtyFlag:Boolean;
        private var _changeMaterialRequestItemFlag:Boolean;
        private var _changeOtherFieldFlag:Boolean;
        private var _changeSatelliteFlag:Boolean;
        private var _changeSplitCountFlag:Boolean;
        private var _changeSupplyDateFlag:Boolean;
        private var _mrMedProfilePoItemList:ListCollectionView;
        private var _pivasWorklist:PivasWorklist;

        public function set changeAdjQtyFlag(value:Boolean):void {
            _changeAdjQtyFlag = value;
        }
        public function get changeAdjQtyFlag():Boolean {
            return _changeAdjQtyFlag;
        }

        public function set changeIssueQtyFlag(value:Boolean):void {
            _changeIssueQtyFlag = value;
        }
        public function get changeIssueQtyFlag():Boolean {
            return _changeIssueQtyFlag;
        }

        public function set changeMaterialRequestItemFlag(value:Boolean):void {
            _changeMaterialRequestItemFlag = value;
        }
        public function get changeMaterialRequestItemFlag():Boolean {
            return _changeMaterialRequestItemFlag;
        }

        public function set changeOtherFieldFlag(value:Boolean):void {
            _changeOtherFieldFlag = value;
        }
        public function get changeOtherFieldFlag():Boolean {
            return _changeOtherFieldFlag;
        }

        public function set changeSatelliteFlag(value:Boolean):void {
            _changeSatelliteFlag = value;
        }
        public function get changeSatelliteFlag():Boolean {
            return _changeSatelliteFlag;
        }

        public function set changeSplitCountFlag(value:Boolean):void {
            _changeSplitCountFlag = value;
        }
        public function get changeSplitCountFlag():Boolean {
            return _changeSplitCountFlag;
        }

        public function set changeSupplyDateFlag(value:Boolean):void {
            _changeSupplyDateFlag = value;
        }
        public function get changeSupplyDateFlag():Boolean {
            return _changeSupplyDateFlag;
        }

        public function set mrMedProfilePoItemList(value:ListCollectionView):void {
            _mrMedProfilePoItemList = value;
        }
        public function get mrMedProfilePoItemList():ListCollectionView {
            return _mrMedProfilePoItemList;
        }

        public function set pivasWorklist(value:PivasWorklist):void {
            _pivasWorklist = value;
        }
        public function get pivasWorklist():PivasWorklist {
            return _pivasWorklist;
        }

        public function readExternal(input:IDataInput):void {
            _changeAdjQtyFlag = input.readObject() as Boolean;
            _changeIssueQtyFlag = input.readObject() as Boolean;
            _changeMaterialRequestItemFlag = input.readObject() as Boolean;
            _changeOtherFieldFlag = input.readObject() as Boolean;
            _changeSatelliteFlag = input.readObject() as Boolean;
            _changeSplitCountFlag = input.readObject() as Boolean;
            _changeSupplyDateFlag = input.readObject() as Boolean;
            _mrMedProfilePoItemList = input.readObject() as ListCollectionView;
            _pivasWorklist = input.readObject() as PivasWorklist;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_changeAdjQtyFlag);
            output.writeObject(_changeIssueQtyFlag);
            output.writeObject(_changeMaterialRequestItemFlag);
            output.writeObject(_changeOtherFieldFlag);
            output.writeObject(_changeSatelliteFlag);
            output.writeObject(_changeSplitCountFlag);
            output.writeObject(_changeSupplyDateFlag);
            output.writeObject(_mrMedProfilePoItemList);
            output.writeObject(_pivasWorklist);
        }
    }
}