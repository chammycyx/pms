/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * NOTE: this file is only generated if it does not exist. You may safely put
 * your custom code here.
 */

package hk.org.ha.model.pms.vo.alert.mds {

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.vo.alert.mds.AlertAllergen")]
    public class AlertAllergen extends AlertAllergenBase {
		
		public function replaceItemNum(oldItemNum:Number, newItemNum:Number, orderNum:String):void {
			itemNum = newItemNum;
		}
    }
}