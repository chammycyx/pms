/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (RxItem.as).
 */

package hk.org.ha.model.pms.vo.rx {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
    import hk.org.ha.model.pms.udt.disp.RemarkItemStatus;
    import hk.org.ha.model.pms.udt.disp.RemarkStatus;
    import mx.collections.ListCollectionView;
    import org.granite.util.Enum;

    [Bindable]
    public class RxItemBase implements IExternalizable {

        private var _alertFlag:Boolean;
        private var _caseNum:String;
        private var _commentDate:Date;
        private var _commentText:String;
        private var _commentUser:String;
        private var _conditionList:ListCollectionView;
        private var _cycleInfo:CycleInfo;
        private var _discontinueDate:Date;
        private var _discontinueFlag:Boolean;
        private var _discontinueHospCode:String;
        private var _discontinueReason:String;
        private var _docType:MedOrderDocType;
        private var _drugOrderHtml:String;
        private var _drugOrderTlf:String;
        private var _drugOrderXml:String;
        private var _externalUseFlag:Boolean;
        private var _fixPeriodFlag:Boolean;
        private var _fmFlag:Boolean;
        private var _formattedDrugOrderTlf:String;
        private var _hlaFlag:Boolean;
        private var _interfaceVersion:String;
        private var _itemNum:Number;
        private var _manualItemFlag:Boolean;
        private var _moDoseAlertFlag:Boolean;
        private var _mrAnnotation:String;
        private var _orderDate:Date;
        private var _pasSpecCode:String;
        private var _pasSubSpecCode:String;
        private var _patHospCode:String;
        private var _pivasFormulaFlag:Boolean;
        private var _refNum:String;
        private var _remarkItemStatus:RemarkItemStatus;
        private var _remarkStatus:RemarkStatus;
        private var _singleUseFlag:Boolean;
        private var _specialInstruction:String;

        public function set alertFlag(value:Boolean):void {
            _alertFlag = value;
        }
        public function get alertFlag():Boolean {
            return _alertFlag;
        }

        public function set caseNum(value:String):void {
            _caseNum = value;
        }
        public function get caseNum():String {
            return _caseNum;
        }

        public function set commentDate(value:Date):void {
            _commentDate = value;
        }
        public function get commentDate():Date {
            return _commentDate;
        }

        public function set commentText(value:String):void {
            _commentText = value;
        }
        public function get commentText():String {
            return _commentText;
        }

        public function set commentUser(value:String):void {
            _commentUser = value;
        }
        public function get commentUser():String {
            return _commentUser;
        }

        public function set conditionList(value:ListCollectionView):void {
            _conditionList = value;
        }
        public function get conditionList():ListCollectionView {
            return _conditionList;
        }

        public function set cycleInfo(value:CycleInfo):void {
            _cycleInfo = value;
        }
        public function get cycleInfo():CycleInfo {
            return _cycleInfo;
        }

        public function set discontinueDate(value:Date):void {
            _discontinueDate = value;
        }
        public function get discontinueDate():Date {
            return _discontinueDate;
        }

        public function set discontinueFlag(value:Boolean):void {
            _discontinueFlag = value;
        }
        public function get discontinueFlag():Boolean {
            return _discontinueFlag;
        }

        public function set discontinueHospCode(value:String):void {
            _discontinueHospCode = value;
        }
        public function get discontinueHospCode():String {
            return _discontinueHospCode;
        }

        public function set discontinueReason(value:String):void {
            _discontinueReason = value;
        }
        public function get discontinueReason():String {
            return _discontinueReason;
        }

        public function set docType(value:MedOrderDocType):void {
            _docType = value;
        }
        public function get docType():MedOrderDocType {
            return _docType;
        }

        public function set drugOrderHtml(value:String):void {
            _drugOrderHtml = value;
        }
        public function get drugOrderHtml():String {
            return _drugOrderHtml;
        }

        public function set drugOrderTlf(value:String):void {
            _drugOrderTlf = value;
        }
        public function get drugOrderTlf():String {
            return _drugOrderTlf;
        }

        public function set drugOrderXml(value:String):void {
            _drugOrderXml = value;
        }
        public function get drugOrderXml():String {
            return _drugOrderXml;
        }

        public function set externalUseFlag(value:Boolean):void {
            _externalUseFlag = value;
        }
        public function get externalUseFlag():Boolean {
            return _externalUseFlag;
        }

        public function set fixPeriodFlag(value:Boolean):void {
            _fixPeriodFlag = value;
        }
        public function get fixPeriodFlag():Boolean {
            return _fixPeriodFlag;
        }

        public function set fmFlag(value:Boolean):void {
            _fmFlag = value;
        }
        public function get fmFlag():Boolean {
            return _fmFlag;
        }

        public function set formattedDrugOrderTlf(value:String):void {
            _formattedDrugOrderTlf = value;
        }
        public function get formattedDrugOrderTlf():String {
            return _formattedDrugOrderTlf;
        }

        public function set hlaFlag(value:Boolean):void {
            _hlaFlag = value;
        }
        public function get hlaFlag():Boolean {
            return _hlaFlag;
        }

        public function set interfaceVersion(value:String):void {
            _interfaceVersion = value;
        }
        public function get interfaceVersion():String {
            return _interfaceVersion;
        }

        public function set itemNum(value:Number):void {
            _itemNum = value;
        }
        public function get itemNum():Number {
            return _itemNum;
        }

        public function set manualItemFlag(value:Boolean):void {
            _manualItemFlag = value;
        }
        public function get manualItemFlag():Boolean {
            return _manualItemFlag;
        }

        public function set moDoseAlertFlag(value:Boolean):void {
            _moDoseAlertFlag = value;
        }
        public function get moDoseAlertFlag():Boolean {
            return _moDoseAlertFlag;
        }

        public function set mrAnnotation(value:String):void {
            _mrAnnotation = value;
        }
        public function get mrAnnotation():String {
            return _mrAnnotation;
        }

        public function set orderDate(value:Date):void {
            _orderDate = value;
        }
        public function get orderDate():Date {
            return _orderDate;
        }

        public function set pasSpecCode(value:String):void {
            _pasSpecCode = value;
        }
        public function get pasSpecCode():String {
            return _pasSpecCode;
        }

        public function set pasSubSpecCode(value:String):void {
            _pasSubSpecCode = value;
        }
        public function get pasSubSpecCode():String {
            return _pasSubSpecCode;
        }

        public function set patHospCode(value:String):void {
            _patHospCode = value;
        }
        public function get patHospCode():String {
            return _patHospCode;
        }

        public function set pivasFormulaFlag(value:Boolean):void {
            _pivasFormulaFlag = value;
        }
        public function get pivasFormulaFlag():Boolean {
            return _pivasFormulaFlag;
        }

        public function set refNum(value:String):void {
            _refNum = value;
        }
        public function get refNum():String {
            return _refNum;
        }

        public function set remarkItemStatus(value:RemarkItemStatus):void {
            _remarkItemStatus = value;
        }
        public function get remarkItemStatus():RemarkItemStatus {
            return _remarkItemStatus;
        }

        public function set remarkStatus(value:RemarkStatus):void {
            _remarkStatus = value;
        }
        public function get remarkStatus():RemarkStatus {
            return _remarkStatus;
        }

        public function set singleUseFlag(value:Boolean):void {
            _singleUseFlag = value;
        }
        public function get singleUseFlag():Boolean {
            return _singleUseFlag;
        }

        public function set specialInstruction(value:String):void {
            _specialInstruction = value;
        }
        public function get specialInstruction():String {
            return _specialInstruction;
        }

        public function readExternal(input:IDataInput):void {
            _alertFlag = input.readObject() as Boolean;
            _caseNum = input.readObject() as String;
            _commentDate = input.readObject() as Date;
            _commentText = input.readObject() as String;
            _commentUser = input.readObject() as String;
            _conditionList = input.readObject() as ListCollectionView;
            _cycleInfo = input.readObject() as CycleInfo;
            _discontinueDate = input.readObject() as Date;
            _discontinueFlag = input.readObject() as Boolean;
            _discontinueHospCode = input.readObject() as String;
            _discontinueReason = input.readObject() as String;
            _docType = Enum.readEnum(input) as MedOrderDocType;
            _drugOrderHtml = input.readObject() as String;
            _drugOrderTlf = input.readObject() as String;
            _drugOrderXml = input.readObject() as String;
            _externalUseFlag = input.readObject() as Boolean;
            _fixPeriodFlag = input.readObject() as Boolean;
            _fmFlag = input.readObject() as Boolean;
            _formattedDrugOrderTlf = input.readObject() as String;
            _hlaFlag = input.readObject() as Boolean;
            _interfaceVersion = input.readObject() as String;
            _itemNum = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            _manualItemFlag = input.readObject() as Boolean;
            _moDoseAlertFlag = input.readObject() as Boolean;
            _mrAnnotation = input.readObject() as String;
            _orderDate = input.readObject() as Date;
            _pasSpecCode = input.readObject() as String;
            _pasSubSpecCode = input.readObject() as String;
            _patHospCode = input.readObject() as String;
            _pivasFormulaFlag = input.readObject() as Boolean;
            _refNum = input.readObject() as String;
            _remarkItemStatus = Enum.readEnum(input) as RemarkItemStatus;
            _remarkStatus = Enum.readEnum(input) as RemarkStatus;
            _singleUseFlag = input.readObject() as Boolean;
            _specialInstruction = input.readObject() as String;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_alertFlag);
            output.writeObject(_caseNum);
            output.writeObject(_commentDate);
            output.writeObject(_commentText);
            output.writeObject(_commentUser);
            output.writeObject(_conditionList);
            output.writeObject(_cycleInfo);
            output.writeObject(_discontinueDate);
            output.writeObject(_discontinueFlag);
            output.writeObject(_discontinueHospCode);
            output.writeObject(_discontinueReason);
            output.writeObject(_docType);
            output.writeObject(_drugOrderHtml);
            output.writeObject(_drugOrderTlf);
            output.writeObject(_drugOrderXml);
            output.writeObject(_externalUseFlag);
            output.writeObject(_fixPeriodFlag);
            output.writeObject(_fmFlag);
            output.writeObject(_formattedDrugOrderTlf);
            output.writeObject(_hlaFlag);
            output.writeObject(_interfaceVersion);
            output.writeObject(_itemNum);
            output.writeObject(_manualItemFlag);
            output.writeObject(_moDoseAlertFlag);
            output.writeObject(_mrAnnotation);
            output.writeObject(_orderDate);
            output.writeObject(_pasSpecCode);
            output.writeObject(_pasSubSpecCode);
            output.writeObject(_patHospCode);
            output.writeObject(_pivasFormulaFlag);
            output.writeObject(_refNum);
            output.writeObject(_remarkItemStatus);
            output.writeObject(_remarkStatus);
            output.writeObject(_singleUseFlag);
            output.writeObject(_specialInstruction);
        }
    }
}