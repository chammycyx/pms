/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (ManualProfileServiceBean.as).
 */

package hk.org.ha.model.pms.biz.inbox {

    import flash.utils.flash_proxy;
    import org.granite.tide.BaseContext;
    import org.granite.tide.Component;
    import org.granite.tide.ITideResponder;
    
    use namespace flash_proxy;

    public class ManualProfileServiceBeanBase extends Component {

        public function constructManualProfile(arg0:String, arg1:String, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("constructManualProfile", arg0, arg1, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("constructManualProfile", arg0, arg1, resultHandler);
            else if (resultHandler == null)
                callProperty("constructManualProfile", arg0, arg1);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function retrieveSpecCodeListAndWardCodeList(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrieveSpecCodeListAndWardCodeList", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrieveSpecCodeListAndWardCodeList", resultHandler);
            else if (resultHandler == null)
                callProperty("retrieveSpecCodeListAndWardCodeList");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function retrievePasPatient(arg0:String, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrievePasPatient", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrievePasPatient", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("retrievePasPatient", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function destroy(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("destroy", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("destroy", resultHandler);
            else if (resultHandler == null)
                callProperty("destroy");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }
    }
}
