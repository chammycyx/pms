package hk.org.ha.model.pms.vo.vetting {
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.utils.Dictionary;
	import flash.utils.Timer;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	import mx.core.FlexGlobals;
	import mx.core.IFlexDisplayObject;
	import mx.core.UIComponent;
	import mx.managers.PopUpManager;
	import mx.utils.StringUtil;
	import mx.utils.UIDUtil;
	
	import spark.components.List;
	
	import flexlib.scheduling.util.DateUtil;
	
	import hk.org.ha.event.pms.main.drug.RetrieveFormVerbPropertiesEvent;
	import hk.org.ha.event.pms.main.vetting.OrderEditDrugConvertEvent;
	import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	import hk.org.ha.model.pms.dms.persistence.DmAdminTime;
	import hk.org.ha.model.pms.dms.persistence.DmDailyFrequency;
	import hk.org.ha.model.pms.dms.persistence.DmRegimen;
	import hk.org.ha.model.pms.dms.persistence.DmRoute;
	import hk.org.ha.model.pms.dms.persistence.DmSite;
	import hk.org.ha.model.pms.dms.persistence.DmSupplFrequency;
	import hk.org.ha.model.pms.dms.persistence.DmSupplSite;
	import hk.org.ha.model.pms.dms.vo.DsfSearchCriteria;
	import hk.org.ha.model.pms.dms.vo.FormVerb;
	import hk.org.ha.model.pms.dms.vo.PreparationSearchCriteria;
	import hk.org.ha.model.pms.dms.vo.RouteForm;
	import hk.org.ha.model.pms.persistence.disp.MedOrder;
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
	import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
	import hk.org.ha.model.pms.udt.vetting.FmStatus;
	import hk.org.ha.model.pms.udt.vetting.PrnPercentage;
	import hk.org.ha.model.pms.udt.vetting.RegimenDurationUnit;
	import hk.org.ha.model.pms.udt.vetting.RegimenType;
	import hk.org.ha.model.pms.vo.drug.DmAdminTimeLite;
	import hk.org.ha.model.pms.vo.rx.Dose;
	import hk.org.ha.model.pms.vo.rx.DoseGroup;
	import hk.org.ha.model.pms.vo.rx.Freq;
	import hk.org.ha.model.pms.vo.rx.Regimen;
	import hk.org.ha.model.pms.vo.rx.Site;
	import hk.org.ha.model.pms.vo.security.PropMap;
	import hk.org.ha.model.pms.vo.sys.SysMsgMap;
	import hk.org.ha.view.pms.main.vetting.OrderEditView;
	
	import org.granite.tide.seam.Context;
	
	[Bindable]
	public class OrderEditViewInfo {				
		
		public var parentApplication:Object;
		public var orderEditView:OrderEditView;
		public var medOrder:MedOrder;
		public var medOrderItem:MedOrderItem;		
		public var dmSupplFrequencyList:ArrayCollection;	
		public var context:Context;		
		public var formVerbCheckingSysMsg:String;
		public var moDosageUnit:String;
		public var moDefaultFormVerb:FormVerb;
		public var defaultAdminTime:DmAdminTimeLite;
		public var defaultAdminTimeCode:String;
		public var defaultStartDate:Date;			
		public var defaultSiteList:ArrayCollection;						
		public var defaultDmRegimen:DmRegimen;
		public var currentDmRegimen:DmRegimen;
		
		public var autoRoutineFlag:Boolean=false;
		public var focusedObject:Object;
		
		public var showOrderInstructionFlag:Boolean=false;
		
		// Only reflect the status of change of item code and pharm. line re-calculation
		public var ddiAlertFlag:Boolean								   = false;
		public var postDispCommentFlag:Boolean						   = false;
		public var backendProcessFlag:Boolean				   		   = false;
		public var showZoomFlag:Boolean								   = false;
		public var isMoiEditFlag:Boolean							   = true;
		public var fullMoEditScreenFlag:Boolean						   = true;
		public var showAdvancedOptionFlag:Boolean					   = true;
		public var showBackFlag:Boolean								   = false;
		public var dsfSearchCriteria:DsfSearchCriteria 				   = new DsfSearchCriteria;
		public var preparationSearchCriteria:PreparationSearchCriteria = new PreparationSearchCriteria;
		public var preparationPropertyList:ArrayCollection 			   = new ArrayCollection;		
		public var moQtyUnitList:ArrayCollection 					   = new ArrayCollection;			
		public var noAdminTime:DmAdminTime 							   = new DmAdminTime();
		public var mealDescEnabled:Boolean							   = true;
		public var hasFocusFlag:Boolean			   					   = false;
		public var pharmItemFocusIndex:int							   = 0;
		public var warnDesc:String									   = "";		
		public var readOnlyFlag:Boolean								   = false;
		public var routeForm:RouteForm								   = null;
		public var overrideByRestrictedDrugFlag:Boolean				   = false;
		public var isOnHide:Boolean									   = false;			
		
		public var dmRegimenDictionary:Dictionary;
		public var dmSiteDictionary:Dictionary;
		public var dmSupplSiteDictionary:Dictionary;
		public var dmAdminTimeDictionary:Dictionary;
		public var dmRouteDictionary:Dictionary;
		public var sysMsgMap:SysMsgMap;
		public var formVerbMap:PropMap;		
		
		public var backToDrugSearch:Function;
		public var replaceHandler:Function;
		public var validateRegimen:Function;
		public var getSysMsgEvent:Function;
		
		public var checkAddDoseOrStep:Function;		
		public var updateMoView:Function;
		
		/* instances for order Line  */		
		public var focusLockFlag:Boolean							   = false;
		public var convertInProgress:Boolean						   = false;
		public var resetMoViewTagSeqAndInvalidDesc:Function;
		public var saveItemClickHandler:Function;		
		public var convertClickHandler:Function;
		public var preCancelClickhandler:Function;	
		public var checkMoErrorFocus:Function;
		public var setFocusToMoDataRowAt:Function;
		public var checkExpandMoEditView:Function;
		
		public var moAddButtonFocusFlag:Boolean 					   = false;
		public var moCancelButtonFocusFlag:Boolean					   = false;
		
		public var moDosageFocusFlag:Boolean 	 					   = false;
		public var moDosageUnitFocusFlag:Boolean 					   = false;
		public var moDailyFreqFocusFlag:Boolean 					   = false;
		public var moSupplFreqFocusFlag:Boolean						   = false;
		public var moPrnPercentVisibleFlag:Boolean					   = false;
		public var moPrnFocusFlag:Boolean						  	   = false;
		public var moPrnPercentFocusFlag:Boolean					   = false;
		public var moRouteSiteFocusFlag:Boolean						   = false;
		public var moDurationFocusFlag:Boolean 						   = false;
		public var moQtyFocusFlag:Boolean							   = false;
		public var moDurationDateFieldFlag:Boolean					   = false;
		public var moActionStatusFocusFlag:Boolean					   = false;
		public var moRegTypeFocusFlag:Boolean						   = false;
		public var advOptionFocusFlag:Boolean						   = false;
		public var preparationFocusFlag:Boolean						   = false;
		public var saveItemFocusFlag:Boolean						   = false;
		public var convertFocusFlag:Boolean							   = false;
		public var backFocusFlag:Boolean 							   = false;
		public var zoomFocusFlag:Boolean							   = false;
		public var moZoomFocusFlag:Boolean							   = false;
		public var specInstZoomFocusFlag:Boolean					   = false;
		public var regLineFocusFlag:Boolean							   = false;
		public var replaceFocusFlag:Boolean							   = false;
		public var siteLabelStyle:String							   = "default";
		
		public var moSupplFreqOldIndex:Number;
		
		/* instances for pharm. Line */
		public var saveItemInProgressFlag:Boolean					   = false;
		public var retrieveWarnCodeInProgressFlag:Boolean			   = false; /*PMS-3333*/
		public var poDailyFreqList:ArrayCollection;
		public var poSupplFreqList:ArrayCollection;
		
		public var editRegimenLineClickHandler:Function;		
		public var setFocusToPoDataRowAt:Function;
		public var checkPoErrorFocus:Function;
		public var editViewKeyDownHandler:Function;
		public var resetPoViewTabSeqAndInvalidDesc:Function;		
		
		public var itemCodeFocusFlag:Boolean						   = false;
		public var drugNameFocusFlag:Boolean						   = false;
		public var formDescFocusFlag:Boolean						   = false;
		public var strengthFocusFlag:Boolean						   = false;
		public var volumeFocusFlag:Boolean							   = false;		
		
		public var poDosageFocusFlag:Boolean 						   = false;
		public var poDailyFreqFocusFlag:Boolean						   = false;
		public var poSupplFreqFocusFlag:Boolean						   = false;
		public var poPrnFocusFlag:Boolean							   = false;
		public var poPrnPercentFocusFlag:Boolean					   = false;
		public var poPrnPercentVisibleFlag:Boolean					   = false;
		public var poRouteSiteFocusFlag:Boolean 					   = false;
		public var poDurationFocusFlag:Boolean  					   = false;
		public var poQtyFocusFlag:Boolean							   = false;
		public var poDurationDateFieldFlag:Boolean					   = false;
		
		public var drugConversionBtnEnabled:Boolean					   = true;
		public var lastPoRegimenTabIndex:int 						   = -1;
		public var dangerDrugCheckIssueQtyWarningVisible:Boolean	   = false;
		public var clearMoDoseAlertVisible:Boolean					   = false;
		public var latestMappingStatus:String;
		
		/* Timer */
		private var delayProcessTimer:Timer;
		private var delayProcessFunction:Function;
		
		/* Tab Sequence */		
		public var advanceOptionTabIndex:int;
		public var specialInstZoomTabIndex:int;
		public var postDispCommentTabIndex:int;
		public var actionStatusTabIndex:int;
		public var regimenTypeTabIndex:int;
		public var orderLineZoomTabIndex:int;
		public var saveItemTabIndex:int;
		public var convertTabIndex:int;
		public var backTabIndex:int;
		public var addTabIndex:int;
		public var cancelTabIndex:int;
		public var specialInstTabIndex:int;
		
		public var drugConversionTabIndex:int;
		public var warnCode1TabIndex:int;
		public var warnCode2TabIndex:int;
		public var warnCode3TabIndex:int;
		public var warnCode4TabIndex:int;
		
		public function OrderEditViewInfo():void
		{			
			super();			
			noAdminTime.uid			  = UIDUtil.createUID();
			noAdminTime.adminTimeCode = "-1";
			noAdminTime.adminTimeChi  = "";
			noAdminTime.updateDate	  = new Date;
			noAdminTime.updateUser	  = "System";
			noAdminTime.updateVersion = 1;
			noAdminTime.adminTimeEng  = "NO ADMIN TIME";	
			
			delayProcessTimer = new Timer(300,1);
			delayProcessTimer.addEventListener("timer",delayProcessTimerHandler);
			
		}
		
		public function init(orderEditViewIn:OrderEditView, medOrderIn:MedOrder, medOrderItemIn:MedOrderItem):void
		{
			orderEditView				  					= orderEditViewIn;
			parentApplication								= orderEditView.parentApplication;
			medOrder					  					= medOrderIn;
			medOrderItem 			 	  					= medOrderItemIn;
			
			siteLabelStyle									= medOrder.isMpDischarge()?"default":"uppercase";
			
			currentDmRegimen 								= medOrderItemIn.regimen?medOrderItemIn.regimen.dmRegimen:null;
			context      			 	  					= orderEditView.ctx;
			sysMsgMap	   			 	  					= orderEditView.sysMsgMap;
			formVerbMap  			 	  					= orderEditView.formVerbMap;
			checkShowAdvancedOption();
			
			showBackFlag				  					= orderEditView.properties["showBack"];	
			autoRoutineFlag									= orderEditView.properties["autoAddDrug"];
			readOnlyFlag				  					= orderEditView.readOnlyFlag;
			latestMappingStatus								= orderEditView.latestMappingStatus;
			ddiAlertFlag				  					= medOrderItem.ddiAlertList.length > 0?true:false;
			postDispCommentFlag		  						= medOrderItem.commentText && StringUtil.trim(medOrderItemIn.commentText) != ""?true:false;			
			
			//init mo base unit drop down list
			resetMoQtyUnitList(medOrderItem.baseUnit);
			
			//Set display name, form, salt search criteria				
			dsfSearchCriteria.dispHospCode 				= orderEditView.workstore.hospCode;
			dsfSearchCriteria.dispWorkstore 			= orderEditView.workstore.workstoreCode;
			dsfSearchCriteria.displayname 				= medOrderItem.displayName;
			dsfSearchCriteria.formCode 					= medOrderItem.formCode;
			dsfSearchCriteria.saltProperty 				= medOrderItem.saltProperty;				
			
			//Set preparation search criteria	
			preparationSearchCriteria.costIncluded		= true;
			preparationSearchCriteria.dispHospCode 		= orderEditView.workstore.hospCode;
			preparationSearchCriteria.dispWorkstore 	= orderEditView.workstore.workstoreCode;
			preparationSearchCriteria.drugScope 		= "I";
			preparationSearchCriteria.trueDisplayname 	= medOrderItem.displayName;				
			preparationSearchCriteria.formCode 			= medOrderItem.formCode;
			preparationSearchCriteria.saltProperty 		= medOrderItem.saltProperty;
			preparationSearchCriteria.pmsFmStatus 		= medOrderItem.fmStatus;
			preparationSearchCriteria.specialtyType 	= medOrderItem.medOrder.cmsOrderSubType.dataValue;
			preparationSearchCriteria.pasSpecialty 		= orderEditView.moView.pasSpecialty;				 			
			preparationSearchCriteria.pasSubSpecialty 	= orderEditView.moView.pasSubSpecialty;	
			preparationSearchCriteria.hqFlag			= false;
			if (medOrderItem.strengthCompulsory) {
				preparationSearchCriteria.trueAliasname		= medOrderItem.aliasName;
				preparationSearchCriteria.firstDisplayname  = medOrderItem.firstDisplayName;
			}
			
			if (medOrderItem.isIvRxDrug) {
				if (medOrderItem.ivRxDrug.ivAdditiveList != null) {
					if (medOrderItem.ivRxDrug.ivAdditiveList.length == 0) {
						drugConversionBtnEnabled = false;
					}
				}
			}
			
			//Med Order Line Regimen
			if( medOrderItem.regimen ) {

				moDosageUnit = medOrderItem.regimen.firstDose.dosageUnit;												
				moDefaultFormVerb = medOrderItem.regimen.firstDose.defaultFormVerb;				
				defaultAdminTime = medOrderItem.regimen.firstDose.dmAdminTimeLite;
				
				if ( medOrderItem.regimen.firstDose.dmAdminTimeLite != null ) {
					defaultAdminTimeCode = medOrderItem.regimen.firstDose.dmAdminTimeLite.adminTimeCode;
				} else {
					defaultAdminTimeCode = null;
				}
				
				if (!medOrderItem.isInfusion) {
					//Re-set default site list
					defaultSiteList = new ArrayCollection( medOrderItem.regimen.firstDose.siteList.toArray() );					
				}
				updateMoPrnPercentVisible(medOrderItem.regimen);
			}
			
			if ( medOrder.docType == MedOrderDocType.Manual ) {		
				defaultStartDate = DateUtil.copyDate(medOrder.ticket.ticketDate);
			} else {
				defaultStartDate = DateUtil.copyDate(medOrder.orderDate);
			}
			
			initDmInfoDictionary(orderEditView);
		}
		
		public function setToLastFocus():void
		{
			if (focusedObject is UIComponent) {
				UIComponent(focusedObject).setFocus();
			}
		}
		
		public function getCurrentRegimenCode():String
		{
			if (currentDmRegimen) {
				return currentDmRegimen.regimenCode;				
			} else {
				return RegimenDurationUnit.Day.dataValue;
			}
		}
		
		private function initDmInfoDictionary(orderEditViewIn:OrderEditView):void
		{
			var parentApplication:Object = orderEditViewIn.parentApplication;
			if ( orderEditViewIn.dmRegimenDictionary == null ) {
				orderEditViewIn.dmRegimenDictionary = new Dictionary;
				for each ( var dmRegimen:DmRegimen in parentApplication.dmRegimenList ) {
					orderEditViewIn.dmRegimenDictionary[dmRegimen.regimenCode] = dmRegimen;					
				}	
			}
			dmRegimenDictionary = orderEditViewIn.dmRegimenDictionary;
						
			dmSiteDictionary = FlexGlobals.topLevelApplication.getDmSiteDictionary();
			
			dmSupplSiteDictionary = FlexGlobals.topLevelApplication.getDmSuppSiteDictionary(); 
			
			if ( orderEditViewIn.dmAdminTimeDictionary == null ) {
				orderEditViewIn.dmAdminTimeDictionary = new Dictionary;
				for each ( var dmAdminTime:DmAdminTime in parentApplication.dmAdminTimeList ) {
					orderEditViewIn.dmAdminTimeDictionary[dmAdminTime.adminTimeCode] = dmAdminTime;
				}
			}
			dmAdminTimeDictionary = orderEditViewIn.dmAdminTimeDictionary;
			
			if ( orderEditViewIn.dmRouteDictionary == null ) {
				orderEditViewIn.dmRouteDictionary = new Dictionary;
				for each ( var dmRoute:DmRoute in orderEditViewIn.dmRouteList ) {
					orderEditViewIn.dmRouteDictionary[dmRoute.routeCode] = dmRoute.fullRouteDesc;
				}
			}
			dmRouteDictionary = orderEditViewIn.dmRouteDictionary;

			if ( formVerbMap == null || formVerbMap.properties.length == 0 ) {
				orderEditViewIn.dispatchEvent( new RetrieveFormVerbPropertiesEvent(
					function(value:PropMap):void {
						parentApplication.formVerbMap = value;
						formVerbMap = value;
					}
				));
			}
		}
		
		public function resetMoQtyUnitList(baseUnit:String):void
		{
			moQtyUnitList.removeAll();
			moQtyUnitList.addItem("DOSE");
			moQtyUnitList.addItem(baseUnit);
			medOrderItem.refreshDoseList();
		}
		
		public function refreshRegimenSetting():void {								
			
			var defaultDurationUnit:RegimenDurationUnit = null;
			
			if ( RegimenType.dataValueOf(getCurrentRegimenCode()) == RegimenType.StepUpDown 
				|| RegimenType.dataValueOf(getCurrentRegimenCode()) == RegimenType.Daily ) {
				dmSupplFrequencyList = orderEditView.dailySupplFrequencyList;
			}
			
			if ( RegimenType.dataValueOf(getCurrentRegimenCode()) == RegimenType.Weekly ) {					
				defaultDurationUnit = RegimenDurationUnit.Week;
				dmSupplFrequencyList = orderEditView.weeklySupplFrequencyList;
			} else if ( RegimenType.dataValueOf(getCurrentRegimenCode()) == RegimenType.Monthly ) {					
				defaultDurationUnit = RegimenDurationUnit.Month;
				dmSupplFrequencyList = orderEditView.monthlySupplFrequencyList;
			} else if ( RegimenType.dataValueOf(getCurrentRegimenCode()) == RegimenType.Cyclic ) {					
				defaultDurationUnit = RegimenDurationUnit.Cycle;
				dmSupplFrequencyList = orderEditView.cyclicSupplFrequencyList;
			}								
			
			if ( !defaultDurationUnit ) {
				defaultDmRegimen = null;
				return;
			}
			if ( defaultDurationUnit != null ) {
				defaultDmRegimen = dmRegimenDictionary[defaultDurationUnit.dataValue];
			}
		}		
		
		public function replicateMoDailyFreqListToPo():void
		{				
			poDailyFreqList = new ArrayCollection(orderEditView.parentApplication.dmDailyFrequencyList.source);					
			if ( medOrderItem.getRegimenType() == RegimenType.StepUpDown ) {
				poDailyFreqList.filterFunction = null;
			} else {
				poDailyFreqList.filterFunction = OrderEditViewUtil.dailyFreqFilterFunction;
			}
			poDailyFreqList.refresh();
			
			poSupplFreqList = new ArrayCollection(dmSupplFrequencyList.source);
			poSupplFreqList.refresh();
		}
		
		public function addDose(regimen:Regimen, doseGroupIndex:int, doseIndex:int):void {
			hasFocusFlag = false;
			
			var doseGroup:DoseGroup = regimen.doseGroupList.getItemAt(doseGroupIndex) as DoseGroup;
			var doseList:ListCollectionView = doseGroup.doseList;
			var currentDose:Dose = doseList.getItemAt(doseIndex) as Dose;
			
			if( !doseList ) {
				return;
			} else if ( !validateTotalNumOfDosageLine(regimen) ) {
				return;
			} else {
				clearDispQtyFromAllPharmDoseForDangerDrug(orderEditView.poView.list.selectedItem);	//PMSU-4533
					
				var newDose:Dose 		 		= createDose();
				var firstDose:Dose	 			= regimen.firstDose;
				var defaultFormVerb:FormVerb 	= firstDose.defaultFormVerb;
				var prevNonEmptySiteDose:Dose 	= regimen.getNonEmptySiteDose(currentDose);
				if ( isMoiEditFlag ) {
					if ( defaultFormVerb != null ) {//PMSU-2081
						if (!medOrder.isMpDischarge()) {
							if (medOrderItem.fmStatus != FmStatus.FreeTextEntryItem.dataValue) {
								newDose.updateSite(defaultFormVerb.siteCode,defaultFormVerb.supplementarySite,!medOrder.isMpDischarge());
							}
						} else {
							var firstSite:Site = prevNonEmptySiteDose.siteList[0] as Site;
							if (firstSite.siteCode.toUpperCase() != "OTHERS") {
								newDose.updateSite(firstSite.siteCode,firstSite.supplSiteDesc,!medOrder.isMpDischarge());
							}
						}
					}
				} else {
					newDose.updateSite(prevNonEmptySiteDose.siteCode, prevNonEmptySiteDose.supplSiteDesc, !medOrder.isMpDischarge());
				}
				
				newDose.siteList = prevNonEmptySiteDose.siteList;
				
				newDose.defaultFormVerb	= defaultFormVerb;
				newDose.updateDisplaySiteDesc(!medOrder.isMpDischarge());
				
				if ( doseList.length > 0 ) {
					newDose.dosageUnit = firstDose.dosageUnit;
				}
				doseList.addItem( newDose );											
				Dose(doseList.getItemAt( doseList.length - 2 )).removeBtnEnabled = true;
				Dose(doseList.getItemAt( doseList.length - 2 )).addBtnEnabled    = false;				
				updateMoView();
				
				if ( isMoiEditFlag ) {
					resetMoViewTagSeqAndInvalidDesc(10);
					orderEditView.callLater(setFocusToMoDataRowAt,[doseGroupIndex, doseList.length-1]);
				} else {
					newDose.dispQty  = currentDose.dispQty;
					newDose.baseUnit = currentDose.baseUnit;
					resetPoViewTabSeqAndInvalidDesc();
					orderEditView.callLater(setFocusToPoDataRowAt,[orderEditView.poView.list.selectedIndex, doseGroupIndex, doseList.length-1]);
				}
				
				checkExpandMoEditView();
			}
		}
		
		public function removeDose( doseList:ListCollectionView, doseGroupIndex:int, currentIndex:int ):void {
			hasFocusFlag = false;
			
			if( doseList == null ) {
				return;
			} else {
				if ( isMoiEditFlag && !medOrderItem.dangerDrugFlag ) {//PMSU-2394
					if ( currentIndex == 0 && doseGroupIndex == 0 ) {
						Dose(doseList[1]).dispQty  = Dose(doseList[0]).dispQty;
						Dose(doseList[1]).baseUnit = Dose(doseList[0]).baseUnit;
					}
				}
				
				clearDispQtyFromAllPharmDoseForDangerDrug(orderEditView.poView.list.selectedItem);	//PMSU-4533
				
				if ( currentIndex >= 0 ) {
					var dosageUnitList:ListCollectionView = (doseList.getItemAt(currentIndex) as Dose).dosageUnitList;					
					doseList.removeItemAt( currentIndex );
					var lastDose:Dose  = doseList.getItemAt( doseList.length - 1 ) as Dose;					
					// Reset add & remove button of last dose line
					lastDose.addBtnEnabled = true;					
					if ( doseList.length == 1 ) {
						lastDose.removeBtnEnabled = false;
					}					
					if ( currentIndex == 0 ) {
						Dose(doseList.getItemAt( 0 )).dosageUnitList = dosageUnitList;
					}
				}				
				updateMoView();
				doseList.refresh();//PMSU-2188
			}
			
			if ( isMoiEditFlag ) {
				resetMoViewTagSeqAndInvalidDesc(10);
				orderEditView.manualErrorString = medOrderItem.getMoFirstErrorString();
				orderEditView.callLater(setFocusToMoDataRowAt,[doseGroupIndex, doseList.length-1]);
			} else {
				resetPoViewTabSeqAndInvalidDesc();
				orderEditView.manualErrorString = medOrderItem.getPoFirstErrorString();
				orderEditView.callLater(setFocusToPoDataRowAt,[orderEditView.poView.list.selectedIndex, doseGroupIndex, doseList.length-1]);
			}
		}
		
		public function addDoseGroup(regimen:Regimen, doseGroupIndex:int):void {								
			hasFocusFlag = false;
			
			if ( !validateTotalNumOfDosageLine(regimen) ) {
				return;
			} else {
				clearDispQtyFromAllPharmDoseForDangerDrug(orderEditView.poView.list.selectedItem);	//PMSU-4533
			
				var newDose:Dose 						= createDose();
				var newDoseGroup:DoseGroup 				= createDoseGroup();
				var firstDose:Dose	 					= regimen.firstDose;
				var defaultFormVerb:FormVerb 			= firstDose.defaultFormVerb;
				var doseGroupList:ListCollectionView 	= regimen.doseGroupList;
				var currentDoseGroup:DoseGroup  		= doseGroupList.getItemAt(doseGroupIndex) as DoseGroup;
				var doseList:ListCollectionView			= currentDoseGroup.doseList;
				var currentDose:Dose					= doseList.getItemAt(doseList.length-1) as Dose;
				var prevNonEmptySiteDose:Dose			= regimen.getNonEmptySiteDose(currentDose);
				
				//PMSU-2289,PMSU-2081
				if ( isMoiEditFlag ) {
					if ( defaultFormVerb != null ) {
						if (!medOrder.isMpDischarge()) {
							if (medOrderItem.fmStatus != FmStatus.FreeTextEntryItem.dataValue) {
								newDose.siteCode 			= defaultFormVerb.siteCode;
								newDose.supplSiteDesc 		= defaultFormVerb.supplementarySite;
							}
						} else {
							var firstSite:Site = prevNonEmptySiteDose.siteList[0] as Site;
							if (firstSite.siteCode.toUpperCase() != "OTHERS") {
								newDose.updateSite(firstSite.siteCode,firstSite.supplSiteDesc,false);
							}
						}
					}
				} else {
					newDose.siteCode 			= prevNonEmptySiteDose.siteCode;
					newDose.supplSiteDesc 		= prevNonEmptySiteDose.supplSiteDesc;
				}
				
				newDose.siteList = prevNonEmptySiteDose.siteList;
				
				newDose.defaultFormVerb	= defaultFormVerb;
				newDose.updateDisplaySiteDesc(!medOrder.isMpDischarge());
				
				newDoseGroup.doseList.addItem( newDose );			
				doseGroupList.addItem( newDoseGroup );								
				
				// Enable or disable Add/Remove button
				if( RegimenType.dataValueOf(getCurrentRegimenCode()) == RegimenType.StepUpDown ) {
					newDose.addBtnEnabled = true;
				} else {
					newDose.addBtnEnabled = false;
				}
				newDose.removeBtnEnabled = false;				
				DoseGroup(doseGroupList.getItemAt( doseGroupList.length - 2 )).addBtnEnabled = false;				
				
				if ( isMoiEditFlag ) {
					resetMoViewTagSeqAndInvalidDesc(10);
					orderEditView.callLater(setFocusToMoDataRowAt,[doseGroupList.length-1, 0]);
				} else {
					newDose.dispQty  = currentDose.dispQty;
					newDose.baseUnit = currentDose.baseUnit;
					resetPoViewTabSeqAndInvalidDesc();
					orderEditView.callLater(setFocusToPoDataRowAt,[orderEditView.poView.list.selectedIndex,doseGroupList.length-1, 0]);
				}
				
				checkExpandMoEditView();
			}
		}
		
		public function removeDoseGroup( doseGroupList:ListCollectionView, currentIndex:int ):void {
			hasFocusFlag = false;
			
			if( !doseGroupList || currentIndex == -1 ) {
				return;
			}

			clearDispQtyFromAllPharmDoseForDangerDrug(orderEditView.poView.list.selectedItem);	//PMSU-4533
			
			if ( currentIndex > 0 ) {
				doseGroupList.removeItemAt( currentIndex );
				var lastDoseGroup:DoseGroup = doseGroupList.getItemAt( doseGroupList.length - 1 ) as DoseGroup;
				// Enable Add button of last dose group line
				lastDoseGroup.addBtnEnabled = true;
			}
			
			var lastIndex:int = doseGroupList.length-1;
			doseGroupList.refresh();//PMSU-2188
			
			if ( isMoiEditFlag ) {	
				resetMoViewTagSeqAndInvalidDesc(10);
				orderEditView.callLater(setFocusToMoDataRowAt,[doseGroupList.length-1, (doseGroupList.getItemAt(lastIndex) as DoseGroup).doseList.length-1]);
			} else {
				resetPoViewTabSeqAndInvalidDesc();
				orderEditView.callLater(setFocusToPoDataRowAt,[orderEditView.poView.list.selectedIndex,doseGroupList.length-1, (doseGroupList.getItemAt(lastIndex) as DoseGroup).doseList.length-1]);
			}
		}
		
		public function createRegimen() : Regimen {
			
			hasFocusFlag = false;
			var regimenCode:String = getCurrentRegimenCode();
			var regimen:Regimen = new Regimen();
			regimen.type = RegimenType.dataValueOf(regimenCode);
			regimen.dmRegimen = dmRegimenDictionary[regimenCode];
			
			var doseGroupList:ArrayCollection = new ArrayCollection;
			var doseGroupBound:int = 1;
			
			//Step / Up Down default setting has 4 steps
			if( regimen.type == RegimenType.StepUpDown && isMoiEditFlag ) {					
				doseGroupBound = 4;
			}
			//Initialize default DoseGroup & Dose
			for( var i:int=0; i < doseGroupBound; i++ ) {
				var doseGroup:DoseGroup = createDoseGroup();
				var dose:Dose 			= createDose();
				
				doseGroup.doseList.addItem( dose );										
				refreshAddRemoveBtn(doseGroup.doseList, Dose);
				doseGroup.addBtnEnabled = false;
				doseGroup.removeBtnEnabled = false;
				doseGroupList.addItem(doseGroup);		
				//PMSU-3036
				if ( isMoiEditFlag && medOrderItem.formCode == "EYD" && i == 0 ) {//PMSU-3484
					dose.dosage = 1;
				}
			}
			regimen.doseGroupList = doseGroupList;
			
			if ( isMoiEditFlag ) {
				//assign default start date
				regimen.firstDoseGroup.startDate = DateUtil.copyDate( defaultStartDate );
			} else {
				if (medOrderItem.regimen) {
					regimen.firstDoseGroup.startDate = DateUtil.copyDate( medOrderItem.regimen.firstDoseGroup.startDate );
				} else {
					regimen.firstDoseGroup.startDate = DateUtil.copyDate( medOrderItem.ivRxDrug.startDate );
				}
			}
			
			regimen.firstDoseGroup.startDateEnabled = getDurationDateFieldFlag();
			if ( regimen.type == RegimenType.Daily ) {
				regimen.firstDoseGroup.endDateEnabled = getDurationDateFieldFlag();
			}
			
			//Initialize default enable/disable of add & remove button
			if( RegimenType.dataValueOf(getCurrentRegimenCode()) == RegimenType.StepUpDown ) {
				refreshAddRemoveBtn(doseGroupList, DoseGroup);
			}				
			
			if ( isMoiEditFlag ) {
				if ( medOrder.docType == MedOrderDocType.Manual ) {
					updateCarryDuration(regimen);				
				}
			} else {
				if ( medOrderItem.getRegimenType() != RegimenType.StepUpDown ) {
					if (medOrderItem.regimen) {
						regimen.firstDoseGroup.duration = medOrderItem.regimen.firstDoseGroup.duration;
						regimen.firstDoseGroup.durationUnit = medOrderItem.regimen.firstDoseGroup.durationUnit;
					} else {
						regimen.firstDoseGroup.duration = medOrderItem.ivRxDrug.duration;
						regimen.firstDoseGroup.durationUnit = medOrderItem.ivRxDrug.durationUnit;
					}
				} else {
					regimen.firstDoseGroup.duration = 0;
				}
			}
			regimen.determineEndDate(dmRegimenDictionary);
			
			if ( isMoiEditFlag ) {
				if ( !medOrderItem.isFreeTextEntryFlag ) {
					this.autoFillDefaultSite(regimen);
				}
			}								
			
			return regimen;
		}
		
		public function createDose():Dose
		{	
			var dose:Dose = new Dose();				
			var dailyFreq:Freq = new Freq();
			var supplFreq:Freq = new Freq();				
			
			dose.dmDailyFrequency = new DmDailyFrequency;
			dose.dmSupplFrequency = new DmSupplFrequency;
			dose.dailyFreq 		  = dailyFreq;
			dose.supplFreq 		  = supplFreq;				
			dose.siteList 		  = defaultSiteList;				
			dose.dispQtyText	  = "";
			dose.prn			  = false;
			dose.prnPercent		  = PrnPercentage.Hundred;
			
			if ( isMoiEditFlag ) {															
				dose.dosageUnit   	 = moDosageUnit;
				dose.defaultFormVerb = moDefaultFormVerb;
				if ( medOrderItem.dangerDrugFlag ) {
					dose.baseUnit	 = "DOSE";	
				} else {
					dose.baseUnit 	 = medOrderItem.baseUnit;	
				}	
				if ( defaultAdminTime ) {	
					dose.dmAdminTimeLite   = defaultAdminTime;
					dose.adminTimeCode = defaultAdminTime.adminTimeCode;
					dose.adminTimeDesc = defaultAdminTime.adminTimeEng;
				}
			} else {
				if ( orderEditView.poView.list.selectedIndex != -1 && medOrderItem.pharmOrderItemList ) {
					
					var poi:PharmOrderItem = PharmOrderItem(medOrderItem.pharmOrderItemList[orderEditView.poView.list.selectedIndex]);
					dose.baseUnit = poi.baseUnit;
					if ( poi.regimen && poi.regimen.doseGroupList ) {
						dose.dosageUnit = poi.regimen.firstDose.dosageUnit;	
					}
					if ( poi.dmDrugLite && 
						poi.dmDrugLite.adminTimeCode &&
						poi.dmDrugLite.adminTimeCode != "" ) {
						dose.dmAdminTimeLite = DmAdminTimeLite.objValueOf(dmAdminTimeDictionary[poi.dmDrugLite.adminTimeCode]);
						dose.adminTimeCode = dose.dmAdminTimeLite.adminTimeCode;
						dose.adminTimeDesc = dose.dmAdminTimeLite.adminTimeEng;
					}		
					//PMSU-2908
				}					
			}													
			
			//Enable or disable Add/Remove button
			dose.addBtnEnabled 	  = true;
			dose.removeBtnEnabled = true;				
			return dose;
		}
		
		public function createDoseGroup():DoseGroup
		{
			var doseGroup:DoseGroup 	= new DoseGroup();				
			doseGroup.doseList 			= new ArrayCollection();								
			doseGroup.addBtnEnabled 	= true;
			doseGroup.removeBtnEnabled 	= true;												
			doseGroup.startDateEnabled 	= false;
			doseGroup.endDateEnabled 	= false;
			doseGroup.duration			= 0;
			if ( !isMoiEditFlag ) {
				if (getCurrentRegimenCode() != RegimenType.StepUpDown.dataValue) { 
					if (medOrderItem.regimen) {
						doseGroup.duration = DoseGroup(medOrderItem.regimen.doseGroupList[0]).duration;
						doseGroup.durationUnit = DoseGroup(medOrderItem.regimen.doseGroupList[0]).durationUnit;
					} else {
						doseGroup.duration = medOrderItem.ivRxDrug.duration;
						doseGroup.durationUnit = medOrderItem.ivRxDrug.durationUnit;
					}
					if (doseGroup.durationUnit == null) {
						doseGroup.dmRegimen 		= defaultDmRegimen;				
						doseGroup.durationUnit  	= defaultDmRegimen?RegimenDurationUnit.dataValueOf( defaultDmRegimen.regimenCode ):null;
					}
					else {
						doseGroup.dmRegimen = dmRegimenDictionary[doseGroup.durationUnit.dataValue];
					}
				}
			} else {
				doseGroup.dmRegimen 		= defaultDmRegimen;				
				doseGroup.durationUnit  	= defaultDmRegimen?RegimenDurationUnit.dataValueOf( defaultDmRegimen.regimenCode ):null;
			}
			return doseGroup;
		}
		
		public function addPharmLine():void
		{				
			hasFocusFlag 		  = false;
			pharmItemFocusIndex   = medOrderItem.pharmOrderItemList.length;
			
			var poi:PharmOrderItem = new PharmOrderItem();								
			poi.regimen 		   = createRegimen();
			poi.medOrderItem 	   = medOrderItem;
			poi.dirtyFlag 		   = true;
			poi.capdVoucherFlag	   = false;				
			poi.actionStatus	   = medOrderItem.actionStatus;
			poi.fmStatus		   = medOrderItem.fmStatus;								
			
			medOrderItem.pharmOrderItemList.addItem( poi );	
			orderEditView.callLater(orderEditView.setFocusToDataRowAt, [medOrderItem.pharmOrderItemList.length-1]);
			customizedCallLater(resetPoViewTabSeqAndInvalidDesc);
		}
		
		public function removePharmLine():void
		{
			hasFocusFlag 		= false;
			pharmItemFocusIndex = medOrderItem.pharmOrderItemList.length - 2;
			pharmItemFocusIndex = (pharmItemFocusIndex < 0)?0:pharmItemFocusIndex;
						
			var poi:PharmOrderItem;
			var selectedIndex:int = orderEditView.poView.list.selectedIndex;		
			
			if ( selectedIndex >= 0 && medOrderItem.pharmOrderItemList.length > selectedIndex ) {							
				//Disconnect the pharmOrderItem linkdage
				poi 			 = PharmOrderItem(medOrderItem.pharmOrderItemList[selectedIndex]);
				poi.medOrderItem = null;
				poi.pharmOrder 	 = null;
				//Remove it from list(s)
				medOrderItem.pharmOrderItemList.removeItemAt( selectedIndex );
				--selectedIndex;
				selectedIndex = (selectedIndex < 0)?0:selectedIndex;
			}
			
			if ( medOrderItem.pharmOrderItemList && medOrderItem.pharmOrderItemList.length > 0 && selectedIndex >= 0 ) {					
				orderEditView.callLater(orderEditView.setFocusToDataRowAt, [selectedIndex]);
			} else {
				orderEditView.poView.pharmOrderItem	 = new PharmOrderItem;
				orderEditView.poView.list.setFocus();
				warnDesc = "";
			}
			//PMS-2520
			orderEditView.manualErrorString = medOrderItem.getPoFirstErrorString();
			resetPoViewTabSeqAndInvalidDesc();
		}
		
		public function checkShowAdvancedOption():void
		{
			if ( medOrderItem && medOrderItem.getRegimenType() == RegimenType.Daily ) {
				if (medOrderItem.regimen) {
					showAdvancedOptionFlag = !medOrderItem.regimen.isSupplFreqExist;
				} else {
					showAdvancedOptionFlag = false;
				}
			} 
			else {
				showAdvancedOptionFlag = false;
			}
		}
		
		private function validateTotalNumOfDosageLine(regimenValue:Regimen):Boolean
		{
			if ( regimenValue == null ) {
				return false;	
			}
			if ( regimenValue.getTotalNumOfDosageLine() >= 8 ) {
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();					 				
				msgProp.messageCode 			   = "0159";					
				msgProp.messageWinHeight 		   = 200;
				msgProp.setOkDefaultButton 		   = true;		
				orderEditView.dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
				return false;
			}
			
			return true;
		}		
		
		private function autoFillDefaultSite(r:Regimen):void {
			var firstSite:Site = defaultSiteList[0] as Site;
			var dmSite:DmSite;
			var dmSupplSite:DmSupplSite;
			if (medOrder.isMpDischarge()) {
				var defaultFormVerb:FormVerb = r.firstDose.defaultFormVerb;
				if (defaultFormVerb != null) {
					for each ( var doseGroup:DoseGroup in r.doseGroupList ) {
						for each ( var dose:Dose in doseGroup.doseList ) {
							dose.updateSite(defaultFormVerb.siteCode, defaultFormVerb.supplementarySite, false);
						}
					}
				}
			}
			else {
				if (firstSite.siteCode.toUpperCase() != "OTHERS") {
					for each ( var doseGroup:DoseGroup in r.doseGroupList ) {
						for each ( var dose:Dose in doseGroup.doseList ) {
							dose.siteCode 			= firstSite.siteCode;
							if ( firstSite.isSupplSite ) {
								dose.supplSiteDesc 	= firstSite.supplSiteDesc;	
							} else {
								dose.supplSiteDesc	= null;
							}	
							
							dmSite = dmSiteDictionary[firstSite.siteCode];
							dmSupplSite = dmSupplSiteDictionary[firstSite.siteCode + firstSite.supplSiteDesc];
							
							dose.dmSite	= dmSite;			
							dose.dmSupplSite = dmSupplSite;	
							
							dose.siteCategoryCode = dmSite.siteCategoryCode;
							dose.siteDesc = dmSite.siteDescEng;
							dose.ipmoeDesc = dmSite.ipmoeDesc;
						}				
					}
				}
			}
		}
		
		private function updateCarryDuration(r:Regimen):void {
			if (!(r.type == RegimenType.Daily || r.type == RegimenType.Weekly)) {
				return;
			}
			if ( isNaN(orderEditView.carryDuration) || !orderEditView.carryDurationUnit ) {
				return;
			}
			for each( var doseGroup:DoseGroup in r.doseGroupList ) {										
				if ( r.type == RegimenType.Weekly ) {
					if ( doseGroup.durationUnit == orderEditView.carryDurationUnit ) {
						doseGroup.duration = orderEditView.carryDuration;	
					}
				} else {
					doseGroup.duration 	   = orderEditView.carryDuration;
					doseGroup.durationUnit = orderEditView.carryDurationUnit;
				}
			}				
		}
		
		private function refreshAddRemoveBtn( list:ListCollectionView, clazz:Class ) : void {
			var lastIndex:int = list.length - 1;				
			if ( list == null || list.length <= 0 ) {
				return;
			}
			var item:Object;	
			for( var i:int = 0; i < list.length; i++ ) {					
				item = list.getItemAt( i ) as clazz;
				item['removeBtnEnabled'] = true;
				item['addBtnEnabled'] = false;
			}
			
			//reset first line of dose / step
			item = list.getItemAt( 0 ) as clazz;				
			if(list.length == 1) {
				item['addBtnEnabled'] = true;
				item['removeBtnEnabled'] = false;
			} else {
				item['addBtnEnabled'] = false;
				if ( item is Dose ) {
					item['removeBtnEnabled'] = true;
				} else {
					item['removeBtnEnabled'] = false;
				}
			}				
			
			//reset last line of dose / step
			if( lastIndex > 0 ) {					
				item = list.getItemAt( lastIndex ) as clazz;
				item['addBtnEnabled'] = true;
				item['removeBtnEnabled'] = true;
			}								
		}
		
		public function refreshAddRemove(r:Regimen):void {
			if( r.doseGroupList != null && r.doseGroupList.length > 0 ) {
				if( RegimenType.dataValueOf(getCurrentRegimenCode()) == RegimenType.StepUpDown ) {
					refreshAddRemoveBtn( r.doseGroupList, DoseGroup );
				}					
				for each ( var doseGroup:DoseGroup in r.doseGroupList ) {
					refreshAddRemoveBtn( doseGroup.doseList, Dose );	
				}
			}
		}
		
		public function validateRegimenForPharmRecal(regimen:Regimen):Boolean 
		{
			if ( !regimen ) {
				return true;
			}
			
			var doseGroup:DoseGroup;
			var dose:Dose;
			
			for each ( doseGroup in regimen.doseGroupList ) {									
				for each ( dose in doseGroup.doseList ) {
					//check site
					if ( !dose.isStop ) {
						if ( dose.checkSite(sysMsgMap, dose.siteErrorString?true:false) ) {
							return false;
						}
					}
				}
			}
			
			return true;
		}
		
		private function stopDailyFreqOkHandler(regimen:Regimen):Function {
			
			var doseGroupIndex:int = 0;	
			var stopFound:Boolean = false;
			// PMS-3091
			for each ( var doseGroup:DoseGroup in regimen.doseGroupList ) {
				var doseIndex:int = 0;
				for each ( var dose:Dose in doseGroup.doseList ) {
					stopFound = dose.isStop;
					if(stopFound && doseGroup.checkIsStopInMultiDose()) {
						break;
					}
					doseIndex++;
				}
				if(stopFound && doseGroup.checkIsStopInMultiDose()) {
					break;
				}
				doseGroupIndex++;
			}
			
			var okHandler:Function = function(evt:MouseEvent):void {
				PopUpManager.removePopUp((UIComponent(evt.currentTarget).parentDocument) as IFlexDisplayObject);
				orderEditView.moView.getDoseGroupItemRendererAt(doseGroupIndex).getDoseItemRendererAt(doseGroupIndex).dailyFreqCbx.setFocus();
			}
			
			return okHandler;
		}
		
		private function stopDailyFreqFirstStepOkHandler(regimen:Regimen):Function {
			
			var doseIndex:int = 0;
			var doseGroup:DoseGroup = regimen.doseGroupList.getItemAt(0) as DoseGroup;
			for(var i:int = 0;i<doseGroup.doseList.length;i++)
			{
				if((doseGroup.doseList.getItemAt(i) as Dose).isStop)
				{
					break;
				}
				doseIndex++;
			}
			
			var okHandler:Function = function(evt:MouseEvent):void {
				PopUpManager.removePopUp((UIComponent(evt.currentTarget).parentDocument) as IFlexDisplayObject);
				orderEditView.moView.getDoseGroupItemRendererAt(0).getDoseItemRendererAt(doseIndex).dailyFreqCbx.setFocus();
			};
			
			return okHandler;
		}
		
		public function checkStopFreqInStepUpDownRegimen(regimen:Regimen):Boolean 
		{
			var okHandler:Function;
			
			if ( regimen.type != RegimenType.StepUpDown ) {
				return false;
			}				
			if ( regimen.checkIsStopInFirstStep() ) {
				okHandler = stopDailyFreqFirstStepOkHandler(regimen);
				orderEditView.dispatchEvent( getSysMsgEvent("0162",["STOP"], okHandler) );
				return true;
			}				
			if ( regimen.checkIsStopInMultiDose() ) {
				okHandler = stopDailyFreqOkHandler(regimen);
				orderEditView.dispatchEvent( getSysMsgEvent("0161",["STOP"], okHandler) );
				return true;
			}
			if ( !regimen.isBlankForStopFreq() ) { 
				okHandler = function(evt:MouseEvent):void {							
					PopUpManager.removePopUp((UIComponent(evt.currentTarget).parentDocument) as IFlexDisplayObject);
					for each (var doseGroup:DoseGroup in regimen.doseGroupList) {
						for each (var dose:Dose in doseGroup.doseList) {
							if ( dose.isStop ) {
								dose.clearDoseForStop();
							}
						}
					}			
					if ( orderEditView.currentState == "MO" ) {
						orderEditView.dispatchEvent( new OrderEditDrugConvertEvent() );
						orderEditView.callLater(updateMoView);
					} else {
						orderEditView.saveItem();
					}						
				}
				orderEditView.dispatchEvent( getSysMsgEvent("0163",null,okHandler) );
				return true;
			}
			return false;
		}
		
		public function checkExceedMaxDuration(duration:Number, durationUnit:RegimenDurationUnit):Boolean {				
			if ( durationUnit == RegimenDurationUnit.Day && duration > Number(orderEditView.propMap.getValue("vetting.regimen.day.duration.max")) ) {
				return true;	
			}					
			if ( durationUnit == RegimenDurationUnit.Week && duration > Number(orderEditView.propMap.getValue("vetting.regimen.week.duration.max")) ) {
				return true;	
			}									
			if ( durationUnit == RegimenDurationUnit.Month && duration > Number(orderEditView.propMap.getValue("vetting.regimen.month.duration.max")) ) {
				return true;	
			}											
			if ( durationUnit == RegimenDurationUnit.Cycle && duration > Number(orderEditView.propMap.getValue("vetting.regimen.cycle.duration.max")) ) {
				return true;	
			}										
			return false;				
		}	
		
		public function checkExceedMaxDurationByRegimen(regimenIn:Regimen):Boolean {
			var index:int=0;
			try {
				for each ( var doseGroup:DoseGroup in regimenIn.doseGroupList ) {
					if ( checkExceedMaxDuration(doseGroup.duration, doseGroup.durationUnit) ) {
						doseGroup.durationErrorString = sysMsgMap.getMessage("0188");
						return true;
					}
					++index;
				}
			} catch (e:Error) {
				trace(e.getStackTrace());
			}
			return false;
		}
		
		public function validateMedOrderItem(tryCount:int=3):void
		{
			var moiRegimen:Regimen = medOrderItem.regimen;
			var dose:Dose;				
			hasFocusFlag = false;
			
			try {
				if ( !moiRegimen ) {
					disableProcessFlag();
					return;
				}
				
				// validate order line regimen	
				if ( !validateRegimen(medOrderItem, true) ) {
					checkMoErrorFocus();
					autoRoutineFlag = false;						
					return;
				}								
				
				if ( checkMoIssueQtyForNonDangerDrug(true) ) {
					checkMoErrorFocus();
					return;
				}
				
				moiRegimen.clearSiteForStop();				
				if ( checkStopFreqInStepUpDownRegimen(moiRegimen) ) {
					checkMoErrorFocus();
					return;
				}
				
				if ( checkExceedMaxDurationByRegimen(moiRegimen) ) {
					checkMoErrorFocus();
					return;
				}								
				
				if (orderEditView.removeBlankInstruction(moiRegimen)) {						
					validateMedOrderItem(tryCount-1);
					updateMoView();
					return;
				}
				
				// determine duration & end date
				if ( moiRegimen.firstDoseGroup && !moiRegimen.firstDoseGroup.startDateEnabled ) {
					moiRegimen.determineEndDate(dmRegimenDictionary);
					moiRegimen.determineDuration(dmRegimenDictionary);//PMSU-2226
				} else {
					moiRegimen.determineDuration(dmRegimenDictionary);
					moiRegimen.determineEndDate(dmRegimenDictionary);//PMSU-2226
				}
				
				// reset pharm. line edit view
				orderEditView.poViewTabNavigator.selectedIndex = 0;				
				medOrderItem.regimen.updateTotalDurationInDay(dmRegimenDictionary);	
				orderEditView.manualErrorString = null;
				
				// check minimum dosage
				if ( !orderEditView.checkMinimumDosage(moiRegimen) ) {
					orderEditView.checkRestrictedDrug();
				} else {
					backendProcessFlag = false;
				}
			} catch (e:Error) {
				trace(e.getStackTrace());
				if (tryCount <= 0) {
					return;
				}
				validateMedOrderItem(tryCount-1);
			}
		}	
		
		private function checkMoIssueQtyForNonDangerDrug(showErrorEnabled:Boolean):Boolean
		{
			if ( !medOrderItem.dangerDrugFlag && !medOrderItem.isInfusion ) {
				return medOrderItem.regimen.checkFirstDoseMoIssueQtyUnit(sysMsgMap,showErrorEnabled);
			} else {
				return false;
			}
		}
		
		public function disableProcessFlag():void
		{
			try {
				saveItemInProgressFlag = false;
				backendProcessFlag = false;
				convertInProgress = false;
			} catch (e:Error) {
				trace(e.getStackTrace());
			}
		}
		
		//System function
		private function delayProcessTimerHandler(event:TimerEvent):void
		{
			if ( delayProcessFunction != null ) {
				delayProcessFunction();
				delayProcessFunction = null;
			}
		}
		
		public function checkShowStartDateForMoView():void
		{
			moDurationDateFieldFlag = medOrderItem.regimen.checkShowStartDate(medOrder);
			medOrderItem.regimen.enableDurationDateField(moDurationDateFieldFlag);
		}
		
		
		public function getDurationDateFieldFlag():Boolean
		{
			return isMoiEditFlag?moDurationDateFieldFlag:poDurationDateFieldFlag;
		}
		
		public function customizedCallLater(value:Function, delayValue:int=300):void
		{
			delayProcessTimer.reset();
			delayProcessFunction 			= value;
			delayProcessTimer.delay 		= delayValue;
			delayProcessTimer.repeatCount 	= 1;
			delayProcessTimer.start();
		}
		
		public function stopCallLaterTimer():void
		{
			if (delayProcessTimer != null) {
				delayProcessTimer.stop();
			}
		}
		
		public function updateMoPrnPercentVisible(regimen:Regimen):void
		{	
			moPrnPercentVisibleFlag = isPrnPercentVisible(regimen);
		}
		
		public function updatePoPrnPercentVisible(regimen:Regimen):void
		{	
			poPrnPercentVisibleFlag = isPrnPercentVisible(regimen);
		}
		
		public function isPrnPercentVisible(regimen:Regimen):Boolean
		{				
			for each (var doseGroup:DoseGroup in regimen.doseGroupList)
			{
				if (doseGroup.doseList)
				{
					for each(var dose:Dose in doseGroup.doseList)
					{
						if(dose.prn)
						{
							return true;
						}
					}
				}
			}
			return false;
		}
		
		public function resetScrollPosition(pharmOrderIndex:int, doseGroupIndex:Number=NaN, doseIndex:Number=NaN):void
		{
			try {
				var orderList:List = orderEditView.poView.list;
				var previousHeight:int = 0;
				var verticalScrollPosition:int = 0;
				if (orderList.dataProvider != null) {
					if (isNaN(doseGroupIndex) && isNaN(doseIndex))
					{
						previousHeight = calculatePharmOrderItemHeight(orderList, pharmOrderIndex);
					}
					else
					{
						previousHeight = calculateDoseHeight(orderList, pharmOrderIndex, doseGroupIndex, doseIndex);
					}
					if (previousHeight <= 129) {
						verticalScrollPosition = 0;
					} else {
						verticalScrollPosition = previousHeight - 129;
					}
					if (orderList.layout.verticalScrollPosition != verticalScrollPosition) {
						orderList.layout.verticalScrollPosition = verticalScrollPosition;
					}
				}
			} catch(e:Error) {
				trace(e.getStackTrace());
			}
		}
		
		private function calculatePharmOrderItemHeight(orderList:List, pharmOrderIndex:int):int
		{
			var previousHeight:int = 0;
			for (var i:int=0; i < pharmOrderIndex && i < orderList.dataProvider.length; i++)
			{
				previousHeight += orderEditView.poView.getPharmOrderItemItemRendererAt(i).height;
			}
			
			return previousHeight;
		}
		
		private function calculateDoseHeight(orderList:List, pharmOrderIndex:int, doseGroupIndex:Number, doseIndex:Number):int
		{
			var previousHeight:int = 0;
			
			if ( pharmOrderIndex > 0 ) {
				previousHeight += calculatePharmOrderItemHeight(orderList, pharmOrderIndex);				
			}
			var dose:Dose = medOrderItem.pharmOrderItemList[pharmOrderIndex].regimen.doseGroupList[doseGroupIndex].doseList[doseIndex] as Dose;
			var numDoseLine:int = ListCollectionView(medOrderItem.pharmOrderItemList[pharmOrderIndex].regimen.getFullDoseList()).getItemIndex(dose) + 1;
				previousHeight += numDoseLine * 24;
			return previousHeight;
		}
		
		public function updateScollerbarPosition(editedOrderIndex:int):void
		{
			//orderList.dataGroup.contentHeight = total height
			//orderList.height = Ui display maximum height
			var orderList:List = orderEditView.poView.list;			
			if (orderList.scroller.verticalScrollBar.visible)
			{
				if (editedOrderIndex + 1 == orderList.dataProvider.length)
				{
					orderList.layout.verticalScrollPosition = orderList.dataGroup.contentHeight - orderList.height;
				}
				else
				{
					var scrollBarPosition:int = 0;
					for (var i:int = 0; i < orderList.dataProvider.length; i++)
					{						
						//24 is row height and 23 is header height
						scrollBarPosition += (orderList.dataProvider.getItemAt(i).regimen * 24) + 23;
						if (i == editedOrderIndex)
						{	
							break;
						}
					}
					
					orderList.layout.verticalScrollPosition = scrollBarPosition;
				}
			}
		}
		
		public function clearPoViewErrorString():void
		{
			if ( medOrderItem && medOrderItem.pharmOrderItemList ) {
				for each ( var poi:PharmOrderItem in medOrderItem.pharmOrderItemList ) {
					poi.clearErrorString(true);
					poi.regimen.clearErrorString();
				}
			}
		}
		
		public function goToDrugConvert(timeInterval:int=0):void
		{
			if ( timeInterval <= 0 ) {
				//Check auto add item
				if ( this.autoRoutineFlag ) {
					if (medOrderItem.isInfusion || medOrderItem.isDhRxDrug) {
						orderEditView.convertDrug();
					}
					else {
						OrderEditViewUtil.checkDefaultPrnPercent(medOrderItem.regimen, this);
						
						//Skip auto add
						if ( !validateRegimen(medOrderItem,false) ) {
							autoRoutineFlag = false;
							if ( !medOrderItem.regimen.firstDose.checkDailyFreq(sysMsgMap,false) ) {
								medOrderItem.regimen.firstDose.checkSite(sysMsgMap,true);
							}
							orderEditView.moveToMoEditView();
						} else {
							orderEditView.convertDrug();
						}
					}
				} else {
					
					//Check free text entry item
					if ( medOrderItem.isFreeTextEntryFlag ) {
						if(readOnlyFlag)//pms-3408
						{
							orderEditView.callLater(orderEditView.moView.cancel.setFocus);			
							orderEditView.callLater(orderEditView.focusManager.showFocus);
						}
						else
						{
							orderEditView.moveToMoEditView();
							orderEditView.callLater(setFocusToMoDataRowAt);							
						}
					} else {
						
						//Check move to 'MO' or 'PO' edit view
						if ( medOrderItem.pharmOrderItemList == null || medOrderItem.pharmOrderItemList.length == 0 ) {
							if ( showBackFlag ) {
								OrderEditViewUtil.checkDefaultPrnPercent(medOrderItem.regimen, this);	
								medOrderItem.refreshDoseList();
								orderEditView.moveToMoEditView();
							} else {
								orderEditView.moveToPoEditView();
							}
						} else {
							orderEditView.moveToPoEditView();					
						}
					}
				}
			} else {
				orderEditView.callLater(goToDrugConvert,[--timeInterval]);
			}
		}
		
		public function clearDispQtyFromAllPharmDoseForDangerDrug(poi:PharmOrderItem):void {
			// for danger drug, disp qty of order line item is brought forward to pharm line item for total qty and issue qty calculation
			// if key info of pharm line item is modified (e.g. add/remove step/dose, change form code, change dosage/ddu),  
			// clear the disp qty of pharm line item so total qty and issue qty is calculated by pharm line frequency and duration 
			
			if (isMoiEditFlag) {
				// for pharm line item only
				return;
			}
			if (poi == null) {
				return;
			}
			if ( ! poi.dangerDrugFlag) {
				return;
			}
			if (poi.regimen.firstDose.dispQtyText == "" || StringUtil.trim(poi.regimen.firstDose.baseUnit) == "") {
				// no need to clear disp qty of danger drug which is added in pharm line manually
				// (disp qty is carried to pharm line item by item conversion, newly added pharm line item does not have disp qty)
				return;
			}
			
			poi.regimen.clearDispQtyAndBaseUnit();
			
			dangerDrugCheckIssueQtyWarningVisible = true;
		}
	}
}