/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.udt.medprofile {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.udt.medprofile.MedProfileDispTxStatus")]
    public class MedProfileDispTxStatus extends Enum {

        public static const Printed:MedProfileDispTxStatus = new MedProfileDispTxStatus("Printed", _, "P", "Printed");
        public static const Assembled:MedProfileDispTxStatus = new MedProfileDispTxStatus("Assembled", _, "A", "Assembled");
        public static const Checked:MedProfileDispTxStatus = new MedProfileDispTxStatus("Checked", _, "C", "Checked");
        public static const Sent:MedProfileDispTxStatus = new MedProfileDispTxStatus("Sent", _, "S", "Sent");
        public static const KeepRecord:MedProfileDispTxStatus = new MedProfileDispTxStatus("KeepRecord", _, "K", "Keep Record");
        public static const Unlink:MedProfileDispTxStatus = new MedProfileDispTxStatus("Unlink", _, "U", "Unlink");

        function MedProfileDispTxStatus(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || Printed.name), restrictor, (dataValue || Printed.dataValue), (displayValue || Printed.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [Printed, Assembled, Checked, Sent, KeepRecord, Unlink];
        }

        public static function valueOf(name:String):MedProfileDispTxStatus {
            return MedProfileDispTxStatus(Printed.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):MedProfileDispTxStatus {
            return MedProfileDispTxStatus(Printed.dataConstantOf(dataValue));
        }
    }
}