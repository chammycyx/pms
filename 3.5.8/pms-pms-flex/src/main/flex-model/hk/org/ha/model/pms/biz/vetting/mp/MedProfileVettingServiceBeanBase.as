/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (MedProfileVettingServiceBean.as).
 */

package hk.org.ha.model.pms.biz.vetting.mp {

    import flash.utils.flash_proxy;
    import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
    import mx.collections.ListCollectionView;
    import org.granite.tide.BaseContext;
    import org.granite.tide.Component;
    import org.granite.tide.ITideResponder;
    
    use namespace flash_proxy;

    public class MedProfileVettingServiceBeanBase extends Component {

        public function vetMedProfile(arg0:MedProfile, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("vetMedProfile", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("vetMedProfile", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("vetMedProfile", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function checkProfileHasLabelGenerationWithinToday(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("checkProfileHasLabelGenerationWithinToday", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("checkProfileHasLabelGenerationWithinToday", resultHandler);
            else if (resultHandler == null)
                callProperty("checkProfileHasLabelGenerationWithinToday");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function updateMedProfile(arg0:MedProfile, arg1:ListCollectionView, arg2:ListCollectionView, arg3:ListCollectionView, arg4:ListCollectionView, arg5:ListCollectionView, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("updateMedProfile", arg0, arg1, arg2, arg3, arg4, arg5, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("updateMedProfile", arg0, arg1, arg2, arg3, arg4, arg5, resultHandler);
            else if (resultHandler == null)
                callProperty("updateMedProfile", arg0, arg1, arg2, arg3, arg4, arg5);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function releaseMedProfile(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("releaseMedProfile", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("releaseMedProfile", resultHandler);
            else if (resultHandler == null)
                callProperty("releaseMedProfile");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function destroy(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("destroy", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("destroy", resultHandler);
            else if (resultHandler == null)
                callProperty("destroy");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }
    }
}
