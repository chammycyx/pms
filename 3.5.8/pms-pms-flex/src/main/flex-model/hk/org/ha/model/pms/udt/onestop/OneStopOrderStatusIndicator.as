/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.udt.onestop {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.udt.onestop.OneStopOrderStatusIndicator")]
    public class OneStopOrderStatusIndicator extends Enum {

        public static const Suspend:OneStopOrderStatusIndicator = new OneStopOrderStatusIndicator("Suspend", _, "S", "S");
        public static const PharmacyRemark:OneStopOrderStatusIndicator = new OneStopOrderStatusIndicator("PharmacyRemark", _, "RR", "R");
        public static const PharmacyremarkConfirm:OneStopOrderStatusIndicator = new OneStopOrderStatusIndicator("PharmacyremarkConfirm", _, "RG", "R");
        public static const AllowModify:OneStopOrderStatusIndicator = new OneStopOrderStatusIndicator("AllowModify", _, "AM", "A");
        public static const AllowModifyPostComment:OneStopOrderStatusIndicator = new OneStopOrderStatusIndicator("AllowModifyPostComment", _, "AP", "A");
        public static const AllowModifyChangePrescType:OneStopOrderStatusIndicator = new OneStopOrderStatusIndicator("AllowModifyChangePrescType", _, "ACP", "A");
        public static const AllowModifyAll:OneStopOrderStatusIndicator = new OneStopOrderStatusIndicator("AllowModifyAll", _, "ALP", "A");
        public static const Uncollect:OneStopOrderStatusIndicator = new OneStopOrderStatusIndicator("Uncollect", _, "U", "U");

        function OneStopOrderStatusIndicator(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || Suspend.name), restrictor, (dataValue || Suspend.dataValue), (displayValue || Suspend.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [Suspend, PharmacyRemark, PharmacyremarkConfirm, AllowModify, AllowModifyPostComment, AllowModifyChangePrescType, AllowModifyAll, Uncollect];
        }

        public static function valueOf(name:String):OneStopOrderStatusIndicator {
            return OneStopOrderStatusIndicator(Suspend.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):OneStopOrderStatusIndicator {
            return OneStopOrderStatusIndicator(Suspend.dataConstantOf(dataValue));
        }
    }
}