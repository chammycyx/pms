/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.persistence.medprofile {

    import hk.org.ha.model.pms.udt.YesNoBlankFlag;
    import hk.org.ha.model.pms.udt.medprofile.MedProfileMoItemStatus;
    import hk.org.ha.model.pms.udt.rx.RxItemType;
    import hk.org.ha.model.pms.vo.rx.RxItem;

    public interface MedProfileMoItemInf {

        function set alertFlag(value:Boolean):void;
        function get alertFlag():Boolean;

        function set chargeFlag(value:YesNoBlankFlag):void;
        function get chargeFlag():YesNoBlankFlag;

        function set doctorCode(value:String):void;
        function get doctorCode():String;

        function set doctorName(value:String):void;
        function get doctorName():String;

        function set doctorRankCode(value:String):void;
        function get doctorRankCode():String;

        function set endDate(value:Date):void;
        function get endDate():Date;

        function set fmFlag(value:Boolean):void;
        function get fmFlag():Boolean;

        function set itemNum(value:Number):void;
        function get itemNum():Number;

        function set orderDate(value:Date):void;
        function get orderDate():Date;

        function set orgItemNum(value:Number):void;
        function get orgItemNum():Number;

        function set ppmiFlag(value:Boolean):void;
        function get ppmiFlag():Boolean;

        function set rxItem(value:RxItem):void;
        function get rxItem():RxItem;

        function set rxItemType(value:RxItemType):void;
        function get rxItemType():RxItemType;

        function set startDate(value:Date):void;
        function get startDate():Date;

        function set statFlag(value:Boolean):void;
        function get statFlag():Boolean;

        function set status(value:MedProfileMoItemStatus):void;
        function get status():MedProfileMoItemStatus;

        function set unitOfCharge(value:Number):void;
        function get unitOfCharge():Number;
    }
}