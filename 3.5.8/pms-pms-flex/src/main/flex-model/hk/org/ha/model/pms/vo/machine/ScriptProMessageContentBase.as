/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (ScriptProMessageContent.as).
 */

package hk.org.ha.model.pms.vo.machine {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import hk.org.ha.model.pms.udt.machine.ScriptProAction;
    import hk.org.ha.model.pms.udt.machine.ScriptProPrintType;
    import mx.collections.ListCollectionView;
    import org.granite.util.Enum;

    [Bindable]
    public class ScriptProMessageContentBase implements IExternalizable {

        private var _binNum:String;
        private var _caseNum:String;
        private var _ccCode:ListCollectionView;
        private var _deltaChangeType:String;
        private var _dispDate:String;
        private var _dispDosageUnit:String;
        private var _dispQty:String;
        private var _doctorCode:String;
        private var _drugName:String;
        private var _fdnFlag:String;
        private var _formCode:String;
        private var _hkid:String;
        private var _hospCode:String;
        private var _hospName:String;
        private var _hostname:String;
        private var _instruction:String;
        private var _itemCode:String;
        private var _itemNum:String;
        private var _langType:String;
        private var _pasBedNum:String;
        private var _pasSpecCode:String;
        private var _pasWard:String;
        private var _patCatCode:String;
        private var _patName:String;
        private var _portNum:int;
        private var _printBarcodeFlag:String;
        private var _printInstructionFlag:String;
        private var _printType:ScriptProPrintType;
        private var _printWardBarcodeFlag:String;
        private var _regimentType:String;
        private var _requireDeltaChangeIndFlag:Boolean;
        private var _scriptProAction:ScriptProAction;
        private var _ticketNum:String;
        private var _tradeName:String;
        private var _updateDate:Date;
        private var _userCode:String;
        private var _utf8PatientNameFlag:Boolean;
        private var _warnDesc1:String;
        private var _warnDesc2:String;
        private var _warnDesc3:String;
        private var _warnDesc4:String;
        private var _workstoreCode:String;

        public function set binNum(value:String):void {
            _binNum = value;
        }
        public function get binNum():String {
            return _binNum;
        }

        public function set caseNum(value:String):void {
            _caseNum = value;
        }
        public function get caseNum():String {
            return _caseNum;
        }

        public function set ccCode(value:ListCollectionView):void {
            _ccCode = value;
        }
        public function get ccCode():ListCollectionView {
            return _ccCode;
        }

        public function set deltaChangeType(value:String):void {
            _deltaChangeType = value;
        }
        public function get deltaChangeType():String {
            return _deltaChangeType;
        }

        public function set dispDate(value:String):void {
            _dispDate = value;
        }
        public function get dispDate():String {
            return _dispDate;
        }

        public function set dispDosageUnit(value:String):void {
            _dispDosageUnit = value;
        }
        public function get dispDosageUnit():String {
            return _dispDosageUnit;
        }

        public function set dispQty(value:String):void {
            _dispQty = value;
        }
        public function get dispQty():String {
            return _dispQty;
        }

        public function set doctorCode(value:String):void {
            _doctorCode = value;
        }
        public function get doctorCode():String {
            return _doctorCode;
        }

        public function set drugName(value:String):void {
            _drugName = value;
        }
        public function get drugName():String {
            return _drugName;
        }

        public function set fdnFlag(value:String):void {
            _fdnFlag = value;
        }
        public function get fdnFlag():String {
            return _fdnFlag;
        }

        public function set formCode(value:String):void {
            _formCode = value;
        }
        public function get formCode():String {
            return _formCode;
        }

        public function set hkid(value:String):void {
            _hkid = value;
        }
        public function get hkid():String {
            return _hkid;
        }

        public function set hospCode(value:String):void {
            _hospCode = value;
        }
        public function get hospCode():String {
            return _hospCode;
        }

        public function set hospName(value:String):void {
            _hospName = value;
        }
        public function get hospName():String {
            return _hospName;
        }

        public function set hostname(value:String):void {
            _hostname = value;
        }
        public function get hostname():String {
            return _hostname;
        }

        public function set instruction(value:String):void {
            _instruction = value;
        }
        public function get instruction():String {
            return _instruction;
        }

        public function set itemCode(value:String):void {
            _itemCode = value;
        }
        public function get itemCode():String {
            return _itemCode;
        }

        public function set itemNum(value:String):void {
            _itemNum = value;
        }
        public function get itemNum():String {
            return _itemNum;
        }

        public function set langType(value:String):void {
            _langType = value;
        }
        public function get langType():String {
            return _langType;
        }

        public function set pasBedNum(value:String):void {
            _pasBedNum = value;
        }
        public function get pasBedNum():String {
            return _pasBedNum;
        }

        public function set pasSpecCode(value:String):void {
            _pasSpecCode = value;
        }
        public function get pasSpecCode():String {
            return _pasSpecCode;
        }

        public function set pasWard(value:String):void {
            _pasWard = value;
        }
        public function get pasWard():String {
            return _pasWard;
        }

        public function set patCatCode(value:String):void {
            _patCatCode = value;
        }
        public function get patCatCode():String {
            return _patCatCode;
        }

        public function set patName(value:String):void {
            _patName = value;
        }
        public function get patName():String {
            return _patName;
        }

        public function set portNum(value:int):void {
            _portNum = value;
        }
        public function get portNum():int {
            return _portNum;
        }

        public function set printBarcodeFlag(value:String):void {
            _printBarcodeFlag = value;
        }
        public function get printBarcodeFlag():String {
            return _printBarcodeFlag;
        }

        public function set printInstructionFlag(value:String):void {
            _printInstructionFlag = value;
        }
        public function get printInstructionFlag():String {
            return _printInstructionFlag;
        }

        public function set printType(value:ScriptProPrintType):void {
            _printType = value;
        }
        public function get printType():ScriptProPrintType {
            return _printType;
        }

        public function set printWardBarcodeFlag(value:String):void {
            _printWardBarcodeFlag = value;
        }
        public function get printWardBarcodeFlag():String {
            return _printWardBarcodeFlag;
        }

        public function set regimentType(value:String):void {
            _regimentType = value;
        }
        public function get regimentType():String {
            return _regimentType;
        }

        public function set requireDeltaChangeIndFlag(value:Boolean):void {
            _requireDeltaChangeIndFlag = value;
        }
        public function get requireDeltaChangeIndFlag():Boolean {
            return _requireDeltaChangeIndFlag;
        }

        public function set scriptProAction(value:ScriptProAction):void {
            _scriptProAction = value;
        }
        public function get scriptProAction():ScriptProAction {
            return _scriptProAction;
        }

        public function set ticketNum(value:String):void {
            _ticketNum = value;
        }
        public function get ticketNum():String {
            return _ticketNum;
        }

        public function set tradeName(value:String):void {
            _tradeName = value;
        }
        public function get tradeName():String {
            return _tradeName;
        }

        public function set updateDate(value:Date):void {
            _updateDate = value;
        }
        public function get updateDate():Date {
            return _updateDate;
        }

        public function set userCode(value:String):void {
            _userCode = value;
        }
        public function get userCode():String {
            return _userCode;
        }

        public function set utf8PatientNameFlag(value:Boolean):void {
            _utf8PatientNameFlag = value;
        }
        public function get utf8PatientNameFlag():Boolean {
            return _utf8PatientNameFlag;
        }

        public function set warnDesc1(value:String):void {
            _warnDesc1 = value;
        }
        public function get warnDesc1():String {
            return _warnDesc1;
        }

        public function set warnDesc2(value:String):void {
            _warnDesc2 = value;
        }
        public function get warnDesc2():String {
            return _warnDesc2;
        }

        public function set warnDesc3(value:String):void {
            _warnDesc3 = value;
        }
        public function get warnDesc3():String {
            return _warnDesc3;
        }

        public function set warnDesc4(value:String):void {
            _warnDesc4 = value;
        }
        public function get warnDesc4():String {
            return _warnDesc4;
        }

        public function set workstoreCode(value:String):void {
            _workstoreCode = value;
        }
        public function get workstoreCode():String {
            return _workstoreCode;
        }

        public function readExternal(input:IDataInput):void {
            _binNum = input.readObject() as String;
            _caseNum = input.readObject() as String;
            _ccCode = input.readObject() as ListCollectionView;
            _deltaChangeType = input.readObject() as String;
            _dispDate = input.readObject() as String;
            _dispDosageUnit = input.readObject() as String;
            _dispQty = input.readObject() as String;
            _doctorCode = input.readObject() as String;
            _drugName = input.readObject() as String;
            _fdnFlag = input.readObject() as String;
            _formCode = input.readObject() as String;
            _hkid = input.readObject() as String;
            _hospCode = input.readObject() as String;
            _hospName = input.readObject() as String;
            _hostname = input.readObject() as String;
            _instruction = input.readObject() as String;
            _itemCode = input.readObject() as String;
            _itemNum = input.readObject() as String;
            _langType = input.readObject() as String;
            _pasBedNum = input.readObject() as String;
            _pasSpecCode = input.readObject() as String;
            _pasWard = input.readObject() as String;
            _patCatCode = input.readObject() as String;
            _patName = input.readObject() as String;
            _portNum = input.readObject() as int;
            _printBarcodeFlag = input.readObject() as String;
            _printInstructionFlag = input.readObject() as String;
            _printType = Enum.readEnum(input) as ScriptProPrintType;
            _printWardBarcodeFlag = input.readObject() as String;
            _regimentType = input.readObject() as String;
            _requireDeltaChangeIndFlag = input.readObject() as Boolean;
            _scriptProAction = Enum.readEnum(input) as ScriptProAction;
            _ticketNum = input.readObject() as String;
            _tradeName = input.readObject() as String;
            _updateDate = input.readObject() as Date;
            _userCode = input.readObject() as String;
            _utf8PatientNameFlag = input.readObject() as Boolean;
            _warnDesc1 = input.readObject() as String;
            _warnDesc2 = input.readObject() as String;
            _warnDesc3 = input.readObject() as String;
            _warnDesc4 = input.readObject() as String;
            _workstoreCode = input.readObject() as String;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_binNum);
            output.writeObject(_caseNum);
            output.writeObject(_ccCode);
            output.writeObject(_deltaChangeType);
            output.writeObject(_dispDate);
            output.writeObject(_dispDosageUnit);
            output.writeObject(_dispQty);
            output.writeObject(_doctorCode);
            output.writeObject(_drugName);
            output.writeObject(_fdnFlag);
            output.writeObject(_formCode);
            output.writeObject(_hkid);
            output.writeObject(_hospCode);
            output.writeObject(_hospName);
            output.writeObject(_hostname);
            output.writeObject(_instruction);
            output.writeObject(_itemCode);
            output.writeObject(_itemNum);
            output.writeObject(_langType);
            output.writeObject(_pasBedNum);
            output.writeObject(_pasSpecCode);
            output.writeObject(_pasWard);
            output.writeObject(_patCatCode);
            output.writeObject(_patName);
            output.writeObject(_portNum);
            output.writeObject(_printBarcodeFlag);
            output.writeObject(_printInstructionFlag);
            output.writeObject(_printType);
            output.writeObject(_printWardBarcodeFlag);
            output.writeObject(_regimentType);
            output.writeObject(_requireDeltaChangeIndFlag);
            output.writeObject(_scriptProAction);
            output.writeObject(_ticketNum);
            output.writeObject(_tradeName);
            output.writeObject(_updateDate);
            output.writeObject(_userCode);
            output.writeObject(_utf8PatientNameFlag);
            output.writeObject(_warnDesc1);
            output.writeObject(_warnDesc2);
            output.writeObject(_warnDesc3);
            output.writeObject(_warnDesc4);
            output.writeObject(_workstoreCode);
        }
    }
}