/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.udt.medprofile {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.udt.medprofile.MedProfileOrderStatus")]
    public class MedProfileOrderStatus extends Enum {

        public static const Pending:MedProfileOrderStatus = new MedProfileOrderStatus("Pending", _, "P", "Pending");
        public static const Completed:MedProfileOrderStatus = new MedProfileOrderStatus("Completed", _, "C", "Completed");
        public static const Error:MedProfileOrderStatus = new MedProfileOrderStatus("Error", _, "E", "Error");

        function MedProfileOrderStatus(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || Pending.name), restrictor, (dataValue || Pending.dataValue), (displayValue || Pending.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [Pending, Completed, Error];
        }

        public static function valueOf(name:String):MedProfileOrderStatus {
            return MedProfileOrderStatus(Pending.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):MedProfileOrderStatus {
            return MedProfileOrderStatus(Pending.dataConstantOf(dataValue));
        }
    }
}