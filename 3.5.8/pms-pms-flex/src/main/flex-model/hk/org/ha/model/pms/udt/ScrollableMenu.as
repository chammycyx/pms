package hk.org.ha.model.pms.udt{
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	
	import mx.controls.Menu;
	import mx.controls.listClasses.IListItemRenderer;
	import mx.controls.menuClasses.IMenuItemRenderer;
	import mx.controls.scrollClasses.ScrollBar;
	import mx.core.Application;
	import mx.core.mx_internal;
	import mx.events.MenuEvent;
	import mx.managers.PopUpManager;
	
	use namespace mx_internal;

	[Bindable]
	public class ScrollableMenu extends Menu {
		public function ScrollableMenu() {
			super();
		}
		
		public static function createMenu(parent:DisplayObjectContainer, mdp:Object, showRoot:Boolean=true):ScrollableMenu {	
	        var menu:ScrollableMenu = new ScrollableMenu();
	        menu.tabEnabled = false;
	        menu.owner = DisplayObjectContainer(Application.application);
	        menu.showRoot = showRoot;
	        popUpMenu(menu, parent, mdp);
	        return menu;
	    }

		override public function set verticalScrollPolicy(value:String):void {
			var newPolicy:String = value.toLowerCase();

	        itemsSizeChanged = true;

	        if (_verticalScrollPolicy != newPolicy)
	        {
	            _verticalScrollPolicy = newPolicy;
	            dispatchEvent(new Event("verticalScrollPolicyChanged"));
	        }
	        
	        
        	invalidateDisplayList();
		}
		
		override public function get verticalScrollPolicy():String {
			return this._verticalScrollPolicy;
		}
		
		override protected function configureScrollBars():void {
	        var rowCount:int = listItems.length;
	        if (rowCount == 0) return;
	
	        if (rowCount > 1 && rowInfo[rowCount - 1].y + rowInfo[rowCount-1].height > listContent.height)
	            rowCount--;

	        var offset:int = verticalScrollPosition - lockedRowCount - 1;
	        var fillerRows:int = 0;
	        while (rowCount && listItems[rowCount - 1].length == 0)
	        {
	            if (collection && rowCount + offset >= collection.length)
	            {
	                rowCount--;
	                ++fillerRows;
	            }
	            else
	                break;
	        }		
	
	        var colCount:int = listItems[0].length;
	        var oldHorizontalScrollBar:Object = horizontalScrollBar;
	        var oldVerticalScrollBar:Object = verticalScrollBar;
	        var roundedWidth:int = Math.round(unscaledWidth);
	        var length:int = collection ? collection.length - lockedRowCount: 0;
	        var numRows:int = rowCount - lockedRowCount;
	        
	        setScrollBarProperties(
	                            Math.round(listContent.width) ,
	                            roundedWidth, length, numRows);
	        maxVerticalScrollPosition = Math.max(length - numRows, 0);
	    }
	    
	    override mx_internal function openSubMenu(row:IListItemRenderer):void {
	        supposedToLoseFocus = true;
	
	        var r:Menu = getRootMenu();
	        var menu:Menu;

	        if (!IMenuItemRenderer(row).menu)
	        {
	            menu = new ScrollableMenu();
	            menu.maxHeight = this.maxHeight;
	            
	            menu.parentMenu = this;
	            menu.owner = this;
	            menu.showRoot = showRoot;
	            menu.dataDescriptor = r.dataDescriptor;
	            menu.styleName = r;
	            menu.labelField = r.labelField;
	            menu.labelFunction = r.labelFunction;
	            menu.iconField = r.iconField;
	            menu.iconFunction = r.iconFunction;
	            menu.itemRenderer = r.itemRenderer;
	            menu.rowHeight = r.rowHeight;
	            menu.scaleY = r.scaleY;
	            menu.scaleX = r.scaleX;

	            if (row.data && 
	                _dataDescriptor.isBranch(row.data) &&
	                _dataDescriptor.hasChildren(row.data))
	            {
	                menu.dataProvider = _dataDescriptor.getChildren(row.data);
	            }
	            menu.sourceMenuBar = sourceMenuBar;
	            menu.sourceMenuBarItem = sourceMenuBarItem;
	
	            IMenuItemRenderer(row).menu = menu;
	            PopUpManager.addPopUp(menu, r, false);
	        }
	        
	        super.openSubMenu(row);
	    }
	    
		override protected function keyDownHandler(event:KeyboardEvent):void {
			var row:IListItemRenderer = selectedIndex == -1 ? null : listItems[selectedIndex - verticalScrollPosition][0];
			var rowData:Object = row ? row.data : null;
			var menu:Menu = row ? IMenuItemRenderer(row).menu : null;
			var menuEvent:MenuEvent;
			var keyCode:uint = mapKeycodeForLayoutDirection(event);

			if (keyCode == Keyboard.UP){
				if (rowData && _dataDescriptor.isBranch(rowData) && menu && menu.visible){
					supposedToLoseFocus = true;
					menu.setFocus();
				}
				
				event.stopPropagation();
			}else if (keyCode == Keyboard.DOWN){
				if (rowData && _dataDescriptor.isBranch(rowData) && menu && menu.visible){
					supposedToLoseFocus = true;
					menu.setFocus();
				}
				
				event.stopPropagation();
			}else if (keyCode == Keyboard.RIGHT){
				if (rowData && _dataDescriptor.isBranch(rowData)){
					openSubMenu(row);
					menu = IMenuItemRenderer(row).menu;
					supposedToLoseFocus = true;
					menu.setFocus();
				}else{
					if (sourceMenuBar){
						supposedToLoseFocus = true;
						sourceMenuBar.setFocus();
						sourceMenuBar.dispatchEvent(event);
					}
				}
				event.stopPropagation();
			}else if (keyCode == Keyboard.LEFT){
				if (_parentMenu){
					supposedToLoseFocus = true;
					hide();
					_parentMenu.setFocus();
				}else{
					if (sourceMenuBar){
						supposedToLoseFocus = true;
						sourceMenuBar.setFocus();
						sourceMenuBar.dispatchEvent(event);
					}
				}
				event.stopPropagation();
			}else if (keyCode == Keyboard.ENTER || keyCode == Keyboard.SPACE){
				if (rowData && _dataDescriptor.isBranch(rowData)){
					openSubMenu(row);
					menu = IMenuItemRenderer(row).menu;			
					supposedToLoseFocus = true;
					menu.setFocus();
				}else if (rowData){
					setMenuItemToggled(rowData, !_dataDescriptor.isToggled(rowData));
					menuEvent = new MenuEvent(MenuEvent.ITEM_CLICK);
					menuEvent.menu = this;
					menuEvent.index = this.selectedIndex;
					menuEvent.menuBar = sourceMenuBar;
					menuEvent.label = itemToLabel(rowData);
					menuEvent.item = rowData;
					menuEvent.itemRenderer = row;
					getRootMenu().dispatchEvent(menuEvent);
					menuEvent = new MenuEvent(MenuEvent.CHANGE);
					menuEvent.menu = this;
					menuEvent.index = this.selectedIndex;
					menuEvent.menuBar = sourceMenuBar;
					menuEvent.label = itemToLabel(rowData);
					menuEvent.item = rowData;
					menuEvent.itemRenderer = row;
					getRootMenu().dispatchEvent(menuEvent);
					hideAllMenus();
				}
				event.stopPropagation();
			}else if (keyCode == Keyboard.TAB){
				menuEvent = new MenuEvent(MenuEvent.MENU_HIDE);
				menuEvent.menu = getRootMenu();
				menuEvent.menuBar = sourceMenuBar;
				getRootMenu().dispatchEvent(menuEvent);
				hideAllMenus();
				event.stopPropagation();
			}else if (keyCode == Keyboard.ESCAPE){
				if (_parentMenu){
					supposedToLoseFocus = true;
					hide(); // hide this menu
					_parentMenu.setFocus();
				}else{
					menuEvent = new MenuEvent(MenuEvent.MENU_HIDE);
					menuEvent.menu = getRootMenu();
					menuEvent.menuBar = sourceMenuBar;
					getRootMenu().dispatchEvent(menuEvent);
					hideAllMenus();
					event.stopPropagation();
				}
			}
		}

	    override protected function measure():void {
	        super.measure();

			if(measuredHeight > this.maxHeight) {
				measuredHeight = this.maxHeight;
				measuredMinWidth = measuredWidth = measuredWidth + ScrollBar.THICKNESS;
			}    
		}
	}
}