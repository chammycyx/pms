/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.udt.disp {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.udt.disp.MpTrxType")]
    public class MpTrxType extends Enum {

        public static const MpCreateOrderFirstItem:MpTrxType = new MpTrxType("MpCreateOrderFirstItem", _, "ND1", "Create Order for First Item");
        public static const MpCreateOrderOtherItem:MpTrxType = new MpTrxType("MpCreateOrderOtherItem", _, "ND2", "Create Order for Other Item");
        public static const MpModifyOrder:MpTrxType = new MpTrxType("MpModifyOrder", _, "AD1", "Modify Order");
        public static const MpDeleteOrder:MpTrxType = new MpTrxType("MpDeleteOrder", _, "SU1", "Delete Order");
        public static const MpDiscontinueOrder:MpTrxType = new MpTrxType("MpDiscontinueOrder", _, "SU2", "Discontinue Order");
        public static const MpUpdatePregnancy:MpTrxType = new MpTrxType("MpUpdatePregnancy", _, "HU1", "Update Pregnancy Checking Indicator");
        public static const MpAmendSchedule:MpTrxType = new MpTrxType("MpAmendSchedule", _, "RS1", "Amend Schedule");
        public static const MpCustomSchedule:MpTrxType = new MpTrxType("MpCustomSchedule", _, "RS2", "Custom Schedule");
        public static const MpMarkAnnotation:MpTrxType = new MpTrxType("MpMarkAnnotation", _, "MA1", "MR Mark Annotation");
        public static const MpModifyMds:MpTrxType = new MpTrxType("MpModifyMds", _, "AD2", "Modify IPMOE Item for MDS Update");
        public static const MpModifyReviewDate:MpTrxType = new MpTrxType("MpModifyReviewDate", _, "AD3", "Modify IPMOE Item for Review Date");
        public static const MpAddAdminRecord:MpTrxType = new MpTrxType("MpAddAdminRecord", _, "NA1", "Add Admin Record");
        public static const MpModifyAdminRecord:MpTrxType = new MpTrxType("MpModifyAdminRecord", _, "AA1", "Modify Admin Record");
        public static const MpUrgentDispensing:MpTrxType = new MpTrxType("MpUrgentDispensing", _, "UD1", "Create Request for Urgent Dispensing");
        public static const MpDrugReplenishment:MpTrxType = new MpTrxType("MpDrugReplenishment", _, "RR1", "Create Request for Drug Replenishment");
        public static const MpOverrideOnHoldOrder:MpTrxType = new MpTrxType("MpOverrideOnHoldOrder", _, "PO3", "Override On Hold Order Request");
        public static const MpCompleteAdmin:MpTrxType = new MpTrxType("MpCompleteAdmin", _, "CA1", "Complete Admin");
        public static const MpAddWithholdRecord:MpTrxType = new MpTrxType("MpAddWithholdRecord", _, "WH1", "Add Withhold Record");
        public static const MpModifyWithholdRecord:MpTrxType = new MpTrxType("MpModifyWithholdRecord", _, "WH2", "Modify Withhold Record");
        public static const MpEndWithholdRecord:MpTrxType = new MpTrxType("MpEndWithholdRecord", _, "WH3", "End Withhold Record");
        public static const MpVetOrder:MpTrxType = new MpTrxType("MpVetOrder", _, "SU3", "Vet Order");
        public static const MpUpdateDispensing:MpTrxType = new MpTrxType("MpUpdateDispensing", _, "SU5", "Update Dispensing");
        public static const MpOnHoldOrder:MpTrxType = new MpTrxType("MpOnHoldOrder", _, "PO1", "Pharmacy On Hold Order Request");
        public static const MpClearOnHoldStatus:MpTrxType = new MpTrxType("MpClearOnHoldStatus", _, "PO2", "Pharmacy Clear Pending Status");
        public static const MpUrgentDispenseUpdate:MpTrxType = new MpTrxType("MpUrgentDispenseUpdate", _, "UD2", "Urgent Dispense Update");
        public static const MpDrugReplenishmentUpdate:MpTrxType = new MpTrxType("MpDrugReplenishmentUpdate", _, "RR2", "Drug Replenishment Update");
        public static const MpSaveProfile:MpTrxType = new MpTrxType("MpSaveProfile", _, "_SP", "Save Profile");

        function MpTrxType(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || MpCreateOrderFirstItem.name), restrictor, (dataValue || MpCreateOrderFirstItem.dataValue), (displayValue || MpCreateOrderFirstItem.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [MpCreateOrderFirstItem, MpCreateOrderOtherItem, MpModifyOrder, MpDeleteOrder, MpDiscontinueOrder, MpUpdatePregnancy, MpAmendSchedule, MpCustomSchedule, MpMarkAnnotation, MpModifyMds, MpModifyReviewDate, MpAddAdminRecord, MpModifyAdminRecord, MpUrgentDispensing, MpDrugReplenishment, MpOverrideOnHoldOrder, MpCompleteAdmin, MpAddWithholdRecord, MpModifyWithholdRecord, MpEndWithholdRecord, MpVetOrder, MpUpdateDispensing, MpOnHoldOrder, MpClearOnHoldStatus, MpUrgentDispenseUpdate, MpDrugReplenishmentUpdate, MpSaveProfile];
        }

        public static function valueOf(name:String):MpTrxType {
            return MpTrxType(MpCreateOrderFirstItem.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):MpTrxType {
            return MpTrxType(MpCreateOrderFirstItem.dataConstantOf(dataValue));
        }
    }
}