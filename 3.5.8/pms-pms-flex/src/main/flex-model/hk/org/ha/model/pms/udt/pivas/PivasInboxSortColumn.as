/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.udt.pivas {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.udt.pivas.PivasInboxSortColumn")]
    public class PivasInboxSortColumn extends Enum {

        public static const DueDate:PivasInboxSortColumn = new PivasInboxSortColumn("DueDate", _, "1", "Due Date");
        public static const ReceivedDateDueDate:PivasInboxSortColumn = new PivasInboxSortColumn("ReceivedDateDueDate", _, "2", "Received Time & Due Date");
        public static const SupplyDateDueDate:PivasInboxSortColumn = new PivasInboxSortColumn("SupplyDateDueDate", _, "3", "Supply Time & Due Date");
        public static const PhsWardBedNumPatName:PivasInboxSortColumn = new PivasInboxSortColumn("PhsWardBedNumPatName", _, "4", "PHS Ward, Bed & Patient Name");

        function PivasInboxSortColumn(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || DueDate.name), restrictor, (dataValue || DueDate.dataValue), (displayValue || DueDate.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [DueDate, ReceivedDateDueDate, SupplyDateDueDate, PhsWardBedNumPatName];
        }

        public static function valueOf(name:String):PivasInboxSortColumn {
            return PivasInboxSortColumn(DueDate.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):PivasInboxSortColumn {
            return PivasInboxSortColumn(DueDate.dataConstantOf(dataValue));
        }
    }
}