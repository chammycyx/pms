/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (FoodInteraction.as).
 */

package hk.org.ha.model.pms.vo.alert.druginfo {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;

    [Bindable]
    public class FoodInteractionBase implements IExternalizable {

        private var _html:String;
        private var _interactor:String;
        private var _monoId:Number;
        private var _monoType:Number;
        private var _showMonoFlag:Boolean;
        private var _significance:String;
        private var _significanceLevel:String;
        private var _sortSeq:Number;

        public function set html(value:String):void {
            _html = value;
        }
        public function get html():String {
            return _html;
        }

        public function set interactor(value:String):void {
            _interactor = value;
        }
        public function get interactor():String {
            return _interactor;
        }

        public function set monoId(value:Number):void {
            _monoId = value;
        }
        public function get monoId():Number {
            return _monoId;
        }

        public function set monoType(value:Number):void {
            _monoType = value;
        }
        public function get monoType():Number {
            return _monoType;
        }

        public function set showMonoFlag(value:Boolean):void {
            _showMonoFlag = value;
        }
        public function get showMonoFlag():Boolean {
            return _showMonoFlag;
        }

        public function set significance(value:String):void {
            _significance = value;
        }
        public function get significance():String {
            return _significance;
        }

        public function set significanceLevel(value:String):void {
            _significanceLevel = value;
        }
        public function get significanceLevel():String {
            return _significanceLevel;
        }

        public function set sortSeq(value:Number):void {
            _sortSeq = value;
        }
        public function get sortSeq():Number {
            return _sortSeq;
        }

        public function readExternal(input:IDataInput):void {
            _html = input.readObject() as String;
            _interactor = input.readObject() as String;
            _monoId = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            _monoType = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            _showMonoFlag = input.readObject() as Boolean;
            _significance = input.readObject() as String;
            _significanceLevel = input.readObject() as String;
            _sortSeq = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_html);
            output.writeObject(_interactor);
            output.writeObject(_monoId);
            output.writeObject(_monoType);
            output.writeObject(_showMonoFlag);
            output.writeObject(_significance);
            output.writeObject(_significanceLevel);
            output.writeObject(_sortSeq);
        }
    }
}