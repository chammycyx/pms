/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (WorkstationPrint.as).
 */

package hk.org.ha.model.pms.persistence.reftable {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.getQualifiedClassName;
    import hk.org.ha.model.pms.persistence.VersionEntity;
    import hk.org.ha.model.pms.udt.reftable.PrintDocType;
    import hk.org.ha.model.pms.udt.reftable.PrintType;
    import mx.core.IUID;
    import mx.data.utils.Managed;
    import mx.utils.UIDUtil;
    import org.granite.collections.IPersistentCollection;
    import org.granite.meta;
    import org.granite.tide.IEntityManager;
    import org.granite.tide.IPropertyHolder;
    import org.granite.util.Enum;

    use namespace meta;

    [Managed]
    public class WorkstationPrintBase extends VersionEntity {

        [Transient]
        meta var entityManager:IEntityManager = null;

        private var __initialized:Boolean = true;
        private var __detachedState:String = null;

        private var _docType:PrintDocType;
        private var _id:Number;
        private var _type:PrintType;
        private var _value:String;
        private var _workstation:Workstation;

        meta override function isInitialized(name:String = null):Boolean {
            if (!name)
                return __initialized;

            var property:* = this[name];
            return (
                (!(property is WorkstationPrint) || (property as WorkstationPrint).meta::isInitialized()) &&
                (!(property is IPersistentCollection) || (property as IPersistentCollection).isInitialized())
            );
        }
        
        [Bindable(event="dirtyChange")]
		public function get meta_dirty():Boolean {
			return Managed.getProperty(this, "meta_dirty", false);
		}

        public function set docType(value:PrintDocType):void {
            _docType = value;
        }
        public function get docType():PrintDocType {
            return _docType;
        }

        public function set id(value:Number):void {
            _id = value;
        }
        [Id]
        public function get id():Number {
            return _id;
        }

        public function set type(value:PrintType):void {
            _type = value;
        }
        public function get type():PrintType {
            return _type;
        }

        public function set value(value:String):void {
            _value = value;
        }
        public function get value():String {
            return _value;
        }

        public function set workstation(value:Workstation):void {
            _workstation = value;
        }
        public function get workstation():Workstation {
            return _workstation;
        }

        public function set uid(value:String):void {
            // noop...
        }
        public function get uid():String {
            if (isNaN(_id))
                return UIDUtil.createUID();
            return getQualifiedClassName(this) + "#[" + String(_id) + "]";
        }

        meta override function merge(em:IEntityManager, obj:*):void {
            var src:WorkstationPrintBase = WorkstationPrintBase(obj);
            __initialized = src.__initialized;
            __detachedState = src.__detachedState;
            if (meta::isInitialized()) {
                super.meta::merge(em, obj);
               em.meta_mergeExternal(src._docType, _docType, null, this, 'docType', function setter(o:*):void{_docType = o as PrintDocType}, false);
               em.meta_mergeExternal(src._id, _id, null, this, 'id', function setter(o:*):void{_id = o as Number}, false);
               em.meta_mergeExternal(src._type, _type, null, this, 'type', function setter(o:*):void{_type = o as PrintType}, false);
               em.meta_mergeExternal(src._value, _value, null, this, 'value', function setter(o:*):void{_value = o as String}, false);
               em.meta_mergeExternal(src._workstation, _workstation, null, this, 'workstation', function setter(o:*):void{_workstation = o as Workstation}, false);
            }
            else {
               em.meta_mergeExternal(src._id, _id, null, this, 'id', function setter(o:*):void{_id = o as Number});
            }
        }

        public override function readExternal(input:IDataInput):void {
            __initialized = input.readObject() as Boolean;
            __detachedState = input.readObject() as String;
            if (meta::isInitialized()) {
                super.readExternal(input);
                _docType = Enum.readEnum(input) as PrintDocType;
                _id = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _type = Enum.readEnum(input) as PrintType;
                _value = input.readObject() as String;
                _workstation = input.readObject() as Workstation;
            }
            else {
                _id = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            }
        }

        public override function writeExternal(output:IDataOutput):void {
            output.writeObject(__initialized);
            output.writeObject(__detachedState);
            if (meta::isInitialized()) {
                super.writeExternal(output);
                output.writeObject((_docType is IPropertyHolder) ? IPropertyHolder(_docType).object : _docType);
                output.writeObject((_id is IPropertyHolder) ? IPropertyHolder(_id).object : _id);
                output.writeObject((_type is IPropertyHolder) ? IPropertyHolder(_type).object : _type);
                output.writeObject((_value is IPropertyHolder) ? IPropertyHolder(_value).object : _value);
                output.writeObject((_workstation is IPropertyHolder) ? IPropertyHolder(_workstation).object : _workstation);
            }
            else {
                output.writeObject(_id);
            }
        }
    }
}
