package hk.org.ha.model.pms.vo.medprofile
{
	[Bindable]
	public class SelectionItem
	{
		public var selected:Boolean;
		public var data:*;
	}
}