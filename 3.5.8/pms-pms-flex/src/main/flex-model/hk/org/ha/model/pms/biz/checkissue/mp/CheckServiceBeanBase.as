/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (CheckServiceBean.as).
 */

package hk.org.ha.model.pms.biz.checkissue.mp {

    import flash.utils.flash_proxy;
    import hk.org.ha.model.pms.vo.checkissue.mp.CheckDelivery;
    import org.granite.tide.BaseContext;
    import org.granite.tide.Component;
    import org.granite.tide.ITideResponder;
    
    use namespace flash_proxy;

    public class CheckServiceBeanBase extends Component {

        public function retrieveDelivery(arg0:Date, arg1:String, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrieveDelivery", arg0, arg1, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrieveDelivery", arg0, arg1, resultHandler);
            else if (resultHandler == null)
                callProperty("retrieveDelivery", arg0, arg1);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function convertToCheckDelivery(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("convertToCheckDelivery", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("convertToCheckDelivery", resultHandler);
            else if (resultHandler == null)
                callProperty("convertToCheckDelivery");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function updateDeliveryStatusToChecked(arg0:CheckDelivery, arg1:Boolean, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("updateDeliveryStatusToChecked", arg0, arg1, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("updateDeliveryStatusToChecked", arg0, arg1, resultHandler);
            else if (resultHandler == null)
                callProperty("updateDeliveryStatusToChecked", arg0, arg1);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function getResultMsg(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("getResultMsg", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("getResultMsg", resultHandler);
            else if (resultHandler == null)
                callProperty("getResultMsg");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function getBatchCode(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("getBatchCode", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("getBatchCode", resultHandler);
            else if (resultHandler == null)
                callProperty("getBatchCode");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function getBatchDate(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("getBatchDate", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("getBatchDate", resultHandler);
            else if (resultHandler == null)
                callProperty("getBatchDate");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function destroy(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("destroy", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("destroy", resultHandler);
            else if (resultHandler == null)
                callProperty("destroy");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }
    }
}
