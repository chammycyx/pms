/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (MedSummaryRptServiceBean.as).
 */

package hk.org.ha.model.pms.biz.report {

    import flash.utils.flash_proxy;
    import hk.org.ha.model.pms.udt.printing.PrintLang;
    import org.granite.tide.BaseContext;
    import org.granite.tide.Component;
    import org.granite.tide.ITideResponder;
    
    use namespace flash_proxy;

    public class MedSummaryRptServiceBeanBase extends Component {

        public function isRetrieveSuccess(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("isRetrieveSuccess", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("isRetrieveSuccess", resultHandler);
            else if (resultHandler == null)
                callProperty("isRetrieveSuccess");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function printMedSummaryRpt(arg0:Date, arg1:String, arg2:PrintLang, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("printMedSummaryRpt", arg0, arg1, arg2, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("printMedSummaryRpt", arg0, arg1, arg2, resultHandler);
            else if (resultHandler == null)
                callProperty("printMedSummaryRpt", arg0, arg1, arg2);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function retrieveMedSummaryRptList(arg0:Date, arg1:String, arg2:PrintLang, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrieveMedSummaryRptList", arg0, arg1, arg2, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrieveMedSummaryRptList", arg0, arg1, arg2, resultHandler);
            else if (resultHandler == null)
                callProperty("retrieveMedSummaryRptList", arg0, arg1, arg2);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function destroy(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("destroy", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("destroy", resultHandler);
            else if (resultHandler == null)
                callProperty("destroy");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }
    }
}
