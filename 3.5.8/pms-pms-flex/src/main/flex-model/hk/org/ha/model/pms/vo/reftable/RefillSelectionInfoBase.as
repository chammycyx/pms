/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (RefillSelectionInfo.as).
 */

package hk.org.ha.model.pms.vo.reftable {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import mx.collections.ListCollectionView;

    [Bindable]
    public class RefillSelectionInfoBase implements IExternalizable {

        private var _refillModulePasSpecialtyList:ListCollectionView;
        private var _refillSelectionList:ListCollectionView;

        public function set refillModulePasSpecialtyList(value:ListCollectionView):void {
            _refillModulePasSpecialtyList = value;
        }
        public function get refillModulePasSpecialtyList():ListCollectionView {
            return _refillModulePasSpecialtyList;
        }

        public function set refillSelectionList(value:ListCollectionView):void {
            _refillSelectionList = value;
        }
        public function get refillSelectionList():ListCollectionView {
            return _refillSelectionList;
        }

        public function readExternal(input:IDataInput):void {
            _refillModulePasSpecialtyList = input.readObject() as ListCollectionView;
            _refillSelectionList = input.readObject() as ListCollectionView;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_refillModulePasSpecialtyList);
            output.writeObject(_refillSelectionList);
        }
    }
}