/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (Instruction.as).
 */

package hk.org.ha.model.pms.vo.label {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import mx.collections.ListCollectionView;

    [Bindable]
    public class InstructionBase implements IExternalizable {

        private var _lang:String;
        private var _lineList:ListCollectionView;
        private var _mealTimeDesc:String;

        public function set lang(value:String):void {
            _lang = value;
        }
        public function get lang():String {
            return _lang;
        }

        public function set lineList(value:ListCollectionView):void {
            _lineList = value;
        }
        public function get lineList():ListCollectionView {
            return _lineList;
        }

        public function set mealTimeDesc(value:String):void {
            _mealTimeDesc = value;
        }
        public function get mealTimeDesc():String {
            return _mealTimeDesc;
        }

        public function readExternal(input:IDataInput):void {
            _lang = input.readObject() as String;
            _lineList = input.readObject() as ListCollectionView;
            _mealTimeDesc = input.readObject() as String;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_lang);
            output.writeObject(_lineList);
            output.writeObject(_mealTimeDesc);
        }
    }
}