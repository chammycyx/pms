/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (PasParams.as).
 */

package hk.org.ha.model.pms.vo.medprofile {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import hk.org.ha.model.pms.persistence.corp.Workstore;

    [Bindable]
    public class PasParamsBase implements IExternalizable {

        private var _caseNum:String;
        private var _hkid:String;
        private var _pasBedNum:String;
        private var _pasSpecCode:String;
        private var _pasSubSpecCode:String;
        private var _pasWardCode:String;
        private var _patHospCode:String;
        private var _patKey:String;
        private var _privateFlag:Boolean;
        private var _specCode:String;
        private var _updateBodyInfo:Boolean;
        private var _updateItemTransferInfo:Boolean;
        private var _wardCode:String;
        private var _workstore:Workstore;

        public function set caseNum(value:String):void {
            _caseNum = value;
        }
        public function get caseNum():String {
            return _caseNum;
        }

        public function set hkid(value:String):void {
            _hkid = value;
        }
        public function get hkid():String {
            return _hkid;
        }

        public function set pasBedNum(value:String):void {
            _pasBedNum = value;
        }
        public function get pasBedNum():String {
            return _pasBedNum;
        }

        public function set pasSpecCode(value:String):void {
            _pasSpecCode = value;
        }
        public function get pasSpecCode():String {
            return _pasSpecCode;
        }

        public function set pasSubSpecCode(value:String):void {
            _pasSubSpecCode = value;
        }
        public function get pasSubSpecCode():String {
            return _pasSubSpecCode;
        }

        public function set pasWardCode(value:String):void {
            _pasWardCode = value;
        }
        public function get pasWardCode():String {
            return _pasWardCode;
        }

        public function set patHospCode(value:String):void {
            _patHospCode = value;
        }
        public function get patHospCode():String {
            return _patHospCode;
        }

        public function set patKey(value:String):void {
            _patKey = value;
        }
        public function get patKey():String {
            return _patKey;
        }

        public function set privateFlag(value:Boolean):void {
            _privateFlag = value;
        }
        public function get privateFlag():Boolean {
            return _privateFlag;
        }

        public function set specCode(value:String):void {
            _specCode = value;
        }
        public function get specCode():String {
            return _specCode;
        }

        public function set updateBodyInfo(value:Boolean):void {
            _updateBodyInfo = value;
        }
        public function get updateBodyInfo():Boolean {
            return _updateBodyInfo;
        }

        public function set updateItemTransferInfo(value:Boolean):void {
            _updateItemTransferInfo = value;
        }
        public function get updateItemTransferInfo():Boolean {
            return _updateItemTransferInfo;
        }

        public function set wardCode(value:String):void {
            _wardCode = value;
        }
        public function get wardCode():String {
            return _wardCode;
        }

        public function set workstore(value:Workstore):void {
            _workstore = value;
        }
        public function get workstore():Workstore {
            return _workstore;
        }

        public function readExternal(input:IDataInput):void {
            _caseNum = input.readObject() as String;
            _hkid = input.readObject() as String;
            _pasBedNum = input.readObject() as String;
            _pasSpecCode = input.readObject() as String;
            _pasSubSpecCode = input.readObject() as String;
            _pasWardCode = input.readObject() as String;
            _patHospCode = input.readObject() as String;
            _patKey = input.readObject() as String;
            _privateFlag = input.readObject() as Boolean;
            _specCode = input.readObject() as String;
            _updateBodyInfo = input.readObject() as Boolean;
            _updateItemTransferInfo = input.readObject() as Boolean;
            _wardCode = input.readObject() as String;
            _workstore = input.readObject() as Workstore;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_caseNum);
            output.writeObject(_hkid);
            output.writeObject(_pasBedNum);
            output.writeObject(_pasSpecCode);
            output.writeObject(_pasSubSpecCode);
            output.writeObject(_pasWardCode);
            output.writeObject(_patHospCode);
            output.writeObject(_patKey);
            output.writeObject(_privateFlag);
            output.writeObject(_specCode);
            output.writeObject(_updateBodyInfo);
            output.writeObject(_updateItemTransferInfo);
            output.writeObject(_wardCode);
            output.writeObject(_workstore);
        }
    }
}