/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.udt.reftable {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.udt.reftable.OperationWorkstationType")]
    public class OperationWorkstationType extends Enum {

        public static const TicketGeneration:OperationWorkstationType = new OperationWorkstationType("TicketGeneration", _, "T", "Ticket Generation");
        public static const DataEntry:OperationWorkstationType = new OperationWorkstationType("DataEntry", _, "E", "Data Entry");
        public static const Picking:OperationWorkstationType = new OperationWorkstationType("Picking", _, "P", "Picking");
        public static const Assemble:OperationWorkstationType = new OperationWorkstationType("Assemble", _, "A", "Assemble");
        public static const CheckAndIssue:OperationWorkstationType = new OperationWorkstationType("CheckAndIssue", _, "C", "Check & Issue");
        public static const MultiFunction:OperationWorkstationType = new OperationWorkstationType("MultiFunction", _, "#", "Multi Function");
        public static const SendOut:OperationWorkstationType = new OperationWorkstationType("SendOut", _, "S", "Send Out");

        function OperationWorkstationType(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || TicketGeneration.name), restrictor, (dataValue || TicketGeneration.dataValue), (displayValue || TicketGeneration.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [TicketGeneration, DataEntry, Picking, Assemble, CheckAndIssue, MultiFunction, SendOut];
        }

        public static function valueOf(name:String):OperationWorkstationType {
            return OperationWorkstationType(TicketGeneration.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):OperationWorkstationType {
            return OperationWorkstationType(TicketGeneration.dataConstantOf(dataValue));
        }
    }
}