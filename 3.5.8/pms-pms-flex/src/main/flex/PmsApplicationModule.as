package {
	
	import mx.logging.Log;
	import mx.logging.targets.TraceTarget;
	
	import hk.org.ha.control.pms.exception.AccessDeniedExceptionHandler;
	import hk.org.ha.control.pms.exception.ConcurrentUpdateExceptionHandler;
	import hk.org.ha.control.pms.exception.DatabaseExceptionHandler;
	import hk.org.ha.control.pms.exception.DefaultExceptionHandler;
	import hk.org.ha.control.pms.exception.DrugInfoExceptionHandler;
	import hk.org.ha.control.pms.exception.DrugOnHandExceptionHandler;
	import hk.org.ha.control.pms.exception.DrugSetExceptionHandler;
	import hk.org.ha.control.pms.exception.DueOrderPrintExceptionHandler;
	import hk.org.ha.control.pms.exception.LockTimeoutExceptionHandler;
	import hk.org.ha.control.pms.exception.NotLoggedInExceptionHandler;
	import hk.org.ha.control.pms.exception.OptimisticLockExceptionHandler;
	import hk.org.ha.control.pms.exception.SaveNewProfileExceptionHandler;
	import hk.org.ha.control.pms.exception.SaveRefillExceptionHandler;
	import hk.org.ha.control.pms.exception.SecurityExceptionHandler;
	import hk.org.ha.control.pms.exception.SftpExceptionHandler;
	import hk.org.ha.control.pms.exception.VettingExceptionHandler;
	import hk.org.ha.control.pms.main.alert.AlertAuditServiceCtl;
	import hk.org.ha.control.pms.main.alert.AlertProfileServiceCtl;
	import hk.org.ha.control.pms.main.alert.druginfo.DrugInfoServiceCtl;
	import hk.org.ha.control.pms.main.alert.ehr.EhrAlertServiceCtl;
	import hk.org.ha.control.pms.main.alert.mds.AlertMsgServiceCtl;
	import hk.org.ha.control.pms.main.alert.mds.HlaTestServiceCtl;
	import hk.org.ha.control.pms.main.alert.mds.ManualProfileCheckServiceCtl;
	import hk.org.ha.control.pms.main.alert.mds.MedProfileCheckServiceCtl;
	import hk.org.ha.control.pms.main.alert.mds.OverrideReasonServiceCtl;
	import hk.org.ha.control.pms.main.alert.mds.PreparationCheckServiceCtl;
	import hk.org.ha.control.pms.main.alert.mds.PrescCheckServiceCtl;
	import hk.org.ha.control.pms.main.assembling.AssemblingListServiceCtl;
	import hk.org.ha.control.pms.main.assembling.AssemblingServiceCtl;
	import hk.org.ha.control.pms.main.cddh.CddhServiceCtl;
	import hk.org.ha.control.pms.main.charging.ExemptServiceCtl;
	import hk.org.ha.control.pms.main.charging.MpSfiInvoiceServiceCtl;
	import hk.org.ha.control.pms.main.charging.SfiForceProceedServiceCtl;
	import hk.org.ha.control.pms.main.checkissue.BatchIssueListServiceCtl;
	import hk.org.ha.control.pms.main.checkissue.BatchIssueServiceCtl;
	import hk.org.ha.control.pms.main.checkissue.CheckIssueAidServiceCtl;
	import hk.org.ha.control.pms.main.checkissue.CheckIssueServiceCtl;
	import hk.org.ha.control.pms.main.checkissue.CheckIssueUncollectServiceCtl;
	import hk.org.ha.control.pms.main.checkissue.LedOperationListServiceCtl;
	import hk.org.ha.control.pms.main.checkissue.mp.AssignBatchListServiceCtl;
	import hk.org.ha.control.pms.main.checkissue.mp.AssignBatchServiceCtl;
	import hk.org.ha.control.pms.main.checkissue.mp.CheckListServiceCtl;
	import hk.org.ha.control.pms.main.checkissue.mp.CheckServiceCtl;
	import hk.org.ha.control.pms.main.checkissue.mp.LabelDeleteServiceCtl;
	import hk.org.ha.control.pms.main.checkissue.mp.SendListServiceCtl;
	import hk.org.ha.control.pms.main.checkissue.mp.SendServiceCtl;
	import hk.org.ha.control.pms.main.checkissue.mp.UnlinkServiceCtl;
	import hk.org.ha.control.pms.main.dashboard.mp.MedProfileDashboardServiceCtl;
	import hk.org.ha.control.pms.main.delivery.DueOrderPrintServiceCtl;
	import hk.org.ha.control.pms.main.delivery.LabelReprintServiceCtl;
	import hk.org.ha.control.pms.main.dispensing.HkidBarcodeLabelServiceCtl;
	import hk.org.ha.control.pms.main.dispensing.SpecialLabelListServiceCtl;
	import hk.org.ha.control.pms.main.dispensing.SpecialLabelServiceCtl;
	import hk.org.ha.control.pms.main.dispensing.TicketServiceCtl;
	import hk.org.ha.control.pms.main.drug.DmDrugListServiceCtl;
	import hk.org.ha.control.pms.main.drug.DmDrugLiteListServiceCtl;
	import hk.org.ha.control.pms.main.drug.DmDrugServiceCtl;
	import hk.org.ha.control.pms.main.drug.DmFormDosageUnitMappingListServiceCtl;
	import hk.org.ha.control.pms.main.drug.DmFormListServiceCtl;
	import hk.org.ha.control.pms.main.drug.DmFormServiceCtl;
	import hk.org.ha.control.pms.main.drug.DmFormVerbMappingListServiceCtl;
	import hk.org.ha.control.pms.main.drug.DmWarningListServiceCtl;
	import hk.org.ha.control.pms.main.drug.DmWarningServiceCtl;
	import hk.org.ha.control.pms.main.drug.DrugDiluentListServiceCtl;
	import hk.org.ha.control.pms.main.drug.DrugRouteServiceCtl;
	import hk.org.ha.control.pms.main.drug.DrugSearchServiceCtl;
	import hk.org.ha.control.pms.main.drug.ItemConvertServiceCtl;
	import hk.org.ha.control.pms.main.drug.MsFmStatusServiceCtl;
	import hk.org.ha.control.pms.main.drug.PreparationPropertyListServiceCtl;
	import hk.org.ha.control.pms.main.drug.RouteFormListServiceCtl;
	import hk.org.ha.control.pms.main.drug.SiteFormVerbServiceCtl;
	import hk.org.ha.control.pms.main.drug.SiteListServiceCtl;
	import hk.org.ha.control.pms.main.drug.SolventListServiceCtl;
	import hk.org.ha.control.pms.main.drug.WorkstoreDrugListServiceCtl;
	import hk.org.ha.control.pms.main.drug.WorkstoreDrugServiceCtl;
	import hk.org.ha.control.pms.main.ehr.EhrServiceCtl;
	import hk.org.ha.control.pms.main.enquiry.DispItemStatusEnqServiceCtl;
	import hk.org.ha.control.pms.main.enquiry.DrugChargeClearanceEnqServiceCtl;
	import hk.org.ha.control.pms.main.enquiry.DrugSetListServiceCtl;
	import hk.org.ha.control.pms.main.enquiry.MdsExceptionEnqServiceCtl;
	import hk.org.ha.control.pms.main.enquiry.MpSfiInvoiceEnqServiceCtl;
	import hk.org.ha.control.pms.main.enquiry.OrderPendingSuspendLogEnqServiceCtl;
	import hk.org.ha.control.pms.main.enquiry.SfiDrugPriceEnqServiceCtl;
	import hk.org.ha.control.pms.main.enquiry.SfiInvoiceEnqServiceCtl;
	import hk.org.ha.control.pms.main.enquiry.SpecialDrugEnqListServiceCtl;
	import hk.org.ha.control.pms.main.enquiry.SpecialDrugEnqServiceCtl;
	import hk.org.ha.control.pms.main.inbox.PharmInboxServiceCtl;
	import hk.org.ha.control.pms.main.info.ExportKeyGuideServiceCtl;
	import hk.org.ha.control.pms.main.medprofile.PrivilegeAuthenticateServiceCtl;
	import hk.org.ha.control.pms.main.medprofile.TpnRequestServiceCtl;
	import hk.org.ha.control.pms.main.onestop.AllowModifyPrescServiceCtl;
	import hk.org.ha.control.pms.main.onestop.OneStopAllowModifyUncollectValidationServiceCtl;
	import hk.org.ha.control.pms.main.onestop.OneStopServiceCtl;
	import hk.org.ha.control.pms.main.onestop.ReprintServiceCtl;
	import hk.org.ha.control.pms.main.onestop.ReversePharmacyRemarkServiceCtl;
	import hk.org.ha.control.pms.main.onestop.SuspendPrescServiceCtl;
	import hk.org.ha.control.pms.main.onestop.UncollectPrescServiceCtl;
	import hk.org.ha.control.pms.main.onestop.UnlockOrderServiceCtl;
	import hk.org.ha.control.pms.main.picking.DrugPickListServiceCtl;
	import hk.org.ha.control.pms.main.picking.DrugPickServiceCtl;
	import hk.org.ha.control.pms.main.pivas.PivasBatchPrepServiceCtl;
	import hk.org.ha.control.pms.main.pivas.PivasWorklistServiceCtl;
	import hk.org.ha.control.pms.main.prevet.PreVetServiceCtl;
	import hk.org.ha.control.pms.main.refill.RefillScheduleListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.ChargeSpecialtyListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.ChestListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.ChestServiceCtl;
	import hk.org.ha.control.pms.main.reftable.CompanyListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.DosageConversionListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.DrsConfigListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.DrsPatientServiceCtl;
	import hk.org.ha.control.pms.main.reftable.FdnMappingListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.FdnMappingServiceCtl;
	import hk.org.ha.control.pms.main.reftable.FollowUpWarningListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.ItemDispConfigServiceCtl;
	import hk.org.ha.control.pms.main.reftable.ItemLocationListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.ItemSpecialtyServiceCtl;
	import hk.org.ha.control.pms.main.reftable.LocalWarningListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.MsWorkstoreDrugListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.OperationModeListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.PharmRemarkTemplateListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.PmsRestrictItemSpecByItemListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.PmsRestrictItemSpecBySpecListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.PmsSessionInfoListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.PmsSpecMappingListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.PmsSpecMappingServiceCtl;
	import hk.org.ha.control.pms.main.reftable.PmsWorkstoreRedirectListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.PrepackListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.QtyDurationConversionServiceCtl;
	import hk.org.ha.control.pms.main.reftable.RefillConfigServiceCtl;
	import hk.org.ha.control.pms.main.reftable.RefillHolidayListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.RefillHolidayServiceCtl;
	import hk.org.ha.control.pms.main.reftable.RefillQtyAdjExclusionListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.RefillQtyAdjListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.RefillSelectionListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.RouteFormDisplaySeqListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.SuspendReasonListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.WardSelectionServiceCtl;
	import hk.org.ha.control.pms.main.reftable.WardStockServiceCtl;
	import hk.org.ha.control.pms.main.reftable.cmm.CmmDrugGroupServiceCtl;
	import hk.org.ha.control.pms.main.reftable.cmm.CmmSpecialtyServiceCtl;
	import hk.org.ha.control.pms.main.reftable.mp.AomScheduleListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.mp.CommonOrderStatusListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.mp.DueOrderScheduleTemplateMaintServiceCtl;
	import hk.org.ha.control.pms.main.reftable.mp.FixedConcnPrepActivationServiceCtl;
	import hk.org.ha.control.pms.main.reftable.mp.MpDosageConversionListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.mp.MpItemServiceCtl;
	import hk.org.ha.control.pms.main.reftable.mp.MpQtyDurationConversionServiceCtl;
	import hk.org.ha.control.pms.main.reftable.mp.MpWorkingStoreListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.mp.MultiDoseConvListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.mp.PendReasonServiceCtl;
	import hk.org.ha.control.pms.main.reftable.mp.PmsMpSpecMappingListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.mp.WardConfigServiceCtl;
	import hk.org.ha.control.pms.main.reftable.mp.WardFilterServiceCtl;
	import hk.org.ha.control.pms.main.reftable.mp.WardGroupListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.pivas.PivasDrugListServiceCtl;
	import hk.org.ha.control.pms.main.reftable.pivas.PivasFormulaServiceCtl;
	import hk.org.ha.control.pms.main.reftable.pivas.PivasSystemMaintServiceCtl;
	import hk.org.ha.control.pms.main.reftable.pivas.PivasWardMaintServiceCtl;
	import hk.org.ha.control.pms.main.report.CapdVoucherRptServiceCtl;
	import hk.org.ha.control.pms.main.report.DayEndRptServiceCtl;
	import hk.org.ha.control.pms.main.report.DelItemRptServiceCtl;
	import hk.org.ha.control.pms.main.report.DnldTrxFileServiceCtl;
	import hk.org.ha.control.pms.main.report.DrugRefillListRptServiceCtl;
	import hk.org.ha.control.pms.main.report.EdsActivityRptServiceCtl;
	import hk.org.ha.control.pms.main.report.EdsPerfRptServiceCtl;
	import hk.org.ha.control.pms.main.report.EdsStatusDetailRptServiceCtl;
	import hk.org.ha.control.pms.main.report.EdsStatusSummaryRptServiceCtl;
	import hk.org.ha.control.pms.main.report.EdsTicketNumRptServiceCtl;
	import hk.org.ha.control.pms.main.report.EdsWaitTimeAuditRptServiceCtl;
	import hk.org.ha.control.pms.main.report.EdsWaitTimeRptServiceCtl;
	import hk.org.ha.control.pms.main.report.EdsWorkloadRptServiceCtl;
	import hk.org.ha.control.pms.main.report.HaDrugFormularyMgmtRptServiceCtl;
	import hk.org.ha.control.pms.main.report.ItemLocationRptServiceCtl;
	import hk.org.ha.control.pms.main.report.ManualPrescRptServiceCtl;
	import hk.org.ha.control.pms.main.report.MdsExceptionOverrideRptServiceCtl;
	import hk.org.ha.control.pms.main.report.MdsExceptionRptServiceCtl;
	import hk.org.ha.control.pms.main.report.MedSummaryRptServiceCtl;
	import hk.org.ha.control.pms.main.report.MpTrxRptServiceCtl;
	import hk.org.ha.control.pms.main.report.OutstandSfiPaymentRptServiceCtl;
	import hk.org.ha.control.pms.main.report.PatientDrugProfileListRptServiceCtl;
	import hk.org.ha.control.pms.main.report.PatientListRptServiceCtl;
	import hk.org.ha.control.pms.main.report.PivasRptServiceCtl;
	import hk.org.ha.control.pms.main.report.PivasWorklistSummaryRptServiceCtl;
	import hk.org.ha.control.pms.main.report.PrescRptServiceCtl;
	import hk.org.ha.control.pms.main.report.RefillRptServiceCtl;
	import hk.org.ha.control.pms.main.report.SfiHandleChargeExemptionRptServiceCtl;
	import hk.org.ha.control.pms.main.report.SpecMapRptServiceCtl;
	import hk.org.ha.control.pms.main.report.SuspendStatRptServiceCtl;
	import hk.org.ha.control.pms.main.report.UncollectPrescRptServiceCtl;
	import hk.org.ha.control.pms.main.report.WorkloadStatRptServiceCtl;
	import hk.org.ha.control.pms.main.uncollect.UncollectListServiceCtl;
	import hk.org.ha.control.pms.main.uncollect.UncollectServiceCtl;
	import hk.org.ha.control.pms.main.vetting.CapdListServiceCtl;
	import hk.org.ha.control.pms.main.vetting.CapdManualVoucherServiceCtl;
	import hk.org.ha.control.pms.main.vetting.CapdSplitServiceCtl;
	import hk.org.ha.control.pms.main.vetting.ChestConfirmServiceCtl;
	import hk.org.ha.control.pms.main.vetting.DispLabelPreviewServiceCtl;
	import hk.org.ha.control.pms.main.vetting.DrugConversionServiceCtl;
	import hk.org.ha.control.pms.main.vetting.DrugOnHandPopupServiceCtl;
	import hk.org.ha.control.pms.main.vetting.FmIndicationPopupServiceCtl;
	import hk.org.ha.control.pms.main.vetting.OrderEditServiceCtl;
	import hk.org.ha.control.pms.main.vetting.OrderViewServiceCtl;
	import hk.org.ha.control.pms.main.vetting.PharmRemarkServiceCtl;
	import hk.org.ha.control.pms.main.vetting.PrevPrescServiceCtl;
	import hk.org.ha.control.pms.main.vetting.mp.CheckAtdpsServiceCtl;
	import hk.org.ha.control.pms.main.vetting.mp.MedProfileOrderEditServiceCtl;
	import hk.org.ha.control.pms.main.vetting.mp.MedProfileVettingServiceCtl;
	import hk.org.ha.control.pms.main.vetting.mp.RenalAdjServiceCtl;
	import hk.org.ha.control.pms.security.LogonCtl;
	import hk.org.ha.control.pms.security.ScreenLockServiceCtl;
	import hk.org.ha.control.pms.support.CorporatePropListServiceCtl;
	import hk.org.ha.control.pms.support.PropListServiceCtl;
	import hk.org.ha.control.pms.support.WorkstationPropListServiceCtl;
	import hk.org.ha.control.pms.sys.SystemMessageServiceCtl;
	import hk.org.ha.control.pms.sys.TimeServiceCtl;
	import hk.org.ha.fmk.pms.flex.components.window.Window;
	
	import org.granite.tide.ITideModule;
	import org.granite.tide.Tide;
	import org.granite.tide.validators.ValidatorExceptionHandler;
	
	
	[Bindable]
	public class PmsApplicationModule implements ITideModule 
	{
		
		public function init(tide:Tide):void 
		{
			var t:TraceTarget = new TraceTarget();
			t.filters = ["org.granite.*","hk.org.ha.*"];
			Log.addTarget(t);
			
			tide.addExceptionHandler(NotLoggedInExceptionHandler);
			tide.addExceptionHandler(AccessDeniedExceptionHandler);
			tide.addExceptionHandler(SecurityExceptionHandler);
			tide.addExceptionHandler(ValidatorExceptionHandler);
			tide.addExceptionHandler(OptimisticLockExceptionHandler);
			tide.addExceptionHandler(LockTimeoutExceptionHandler);
			tide.addExceptionHandler(VettingExceptionHandler);
			tide.addExceptionHandler(SaveRefillExceptionHandler);
			tide.addExceptionHandler(DrugInfoExceptionHandler);
			tide.addExceptionHandler(SftpExceptionHandler);
			tide.addExceptionHandler(DrugOnHandExceptionHandler);
			tide.addExceptionHandler(DrugSetExceptionHandler);
			tide.addExceptionHandler(ConcurrentUpdateExceptionHandler);
			tide.addExceptionHandler(SaveNewProfileExceptionHandler);
			tide.addExceptionHandler(DueOrderPrintExceptionHandler);
			tide.addExceptionHandler(DatabaseExceptionHandler);	
			tide.addExceptionHandler(DefaultExceptionHandler);						
			
			tide.addComponent("window", Window);

			tide.addComponents(
				[	OneStopServiceCtl,
					ReprintServiceCtl,
					UnlockOrderServiceCtl,
					UncollectPrescServiceCtl,
					AllowModifyPrescServiceCtl,
					DispLabelPreviewServiceCtl,
					SpecialLabelListServiceCtl,
					SpecialLabelServiceCtl,
					OrderViewServiceCtl,
					OrderEditServiceCtl,
					LogonCtl,
					SystemMessageServiceCtl, 
					SuspendReasonListServiceCtl,
					PendReasonServiceCtl,
					WardFilterServiceCtl,
					MpItemServiceCtl,
					WardConfigServiceCtl,
					FdnMappingListServiceCtl,
					ChestServiceCtl,
					ChestListServiceCtl,
					PharmRemarkTemplateListServiceCtl,
					RefillHolidayListServiceCtl,
					RefillHolidayServiceCtl,
					RefillConfigServiceCtl,
					RefillQtyAdjListServiceCtl,
					RefillSelectionListServiceCtl,
					DrsConfigListServiceCtl,
					ChargeSpecialtyListServiceCtl,
					PmsSpecMappingListServiceCtl,
					PmsSpecMappingServiceCtl,
					PmsRestrictItemSpecBySpecListServiceCtl,
					MsWorkstoreDrugListServiceCtl,
					PmsWorkstoreRedirectListServiceCtl,
					PmsWorkstoreRedirectListServiceCtl,
					TicketServiceCtl,
					RefillRptServiceCtl,
					RouteFormDisplaySeqListServiceCtl,
					OperationModeListServiceCtl,
					ItemLocationListServiceCtl,
					PrepackListServiceCtl,
					DrugChargeClearanceEnqServiceCtl,
					DrugConversionServiceCtl,
					ItemConvertServiceCtl,
					CddhServiceCtl, 
					DmDrugListServiceCtl,
					SolventListServiceCtl,
					DmDrugServiceCtl,
					DmFormDosageUnitMappingListServiceCtl,
					DmFormListServiceCtl,
					DmFormServiceCtl,
					DmFormVerbMappingListServiceCtl,
					LocalWarningListServiceCtl,
					DmWarningListServiceCtl,
					DmWarningServiceCtl,
					FollowUpWarningListServiceCtl,
					PmsRestrictItemSpecByItemListServiceCtl,					
					QtyDurationConversionServiceCtl,
					MpQtyDurationConversionServiceCtl,
					SfiDrugPriceEnqServiceCtl,
					DosageConversionListServiceCtl,
					MpDosageConversionListServiceCtl,
					CommonOrderStatusListServiceCtl,
					FixedConcnPrepActivationServiceCtl,
					MultiDoseConvListServiceCtl,
					ItemLocationRptServiceCtl,
					SuspendStatRptServiceCtl,
					WorkloadStatRptServiceCtl,
					EdsPerfRptServiceCtl,
					EdsStatusSummaryRptServiceCtl,
					EdsWorkloadRptServiceCtl,
					DayEndRptServiceCtl,
					DelItemRptServiceCtl,
					EdsActivityRptServiceCtl,
					EdsWaitTimeRptServiceCtl,
					EdsWaitTimeAuditRptServiceCtl,
					SpecMapRptServiceCtl,
					DrugPickListServiceCtl,
					DrugPickServiceCtl,
					SfiHandleChargeExemptionRptServiceCtl,
					EdsTicketNumRptServiceCtl,
					PrescRptServiceCtl,
					CapdVoucherRptServiceCtl,
					MsFmStatusServiceCtl,
					SuspendPrescServiceCtl,
					DrugSearchServiceCtl,
					DrugOnHandPopupServiceCtl,
					AssemblingListServiceCtl,
					AssemblingServiceCtl,
					LedOperationListServiceCtl,
					SpecialDrugEnqListServiceCtl,
					SfiInvoiceEnqServiceCtl,
					FmIndicationPopupServiceCtl,
					SpecialDrugEnqServiceCtl,
					DispItemStatusEnqServiceCtl,
					CheckIssueServiceCtl,
					CheckIssueAidServiceCtl,
					HkidBarcodeLabelServiceCtl,
					BatchIssueListServiceCtl,
					BatchIssueServiceCtl,
					OutstandSfiPaymentRptServiceCtl,
					SfiForceProceedServiceCtl,
					ExemptServiceCtl,
					UncollectListServiceCtl,
					UncollectServiceCtl,
					RefillScheduleListServiceCtl,
					CapdListServiceCtl,
					PreparationPropertyListServiceCtl,
					CapdManualVoucherServiceCtl,
					CapdSplitServiceCtl,
					PrevPrescServiceCtl,
					PharmRemarkServiceCtl,
					ChestConfirmServiceCtl,
					TimeServiceCtl,
					PrivilegeAuthenticateServiceCtl,
					MedProfileDashboardServiceCtl,
					PharmInboxServiceCtl,
					WardSelectionServiceCtl,
					PreVetServiceCtl,
					MedProfileVettingServiceCtl,
					DueOrderPrintServiceCtl,
					ReversePharmacyRemarkServiceCtl,
					DrugSetListServiceCtl, 
					EdsStatusDetailRptServiceCtl, 
					MedSummaryRptServiceCtl,
					AlertMsgServiceCtl,
					OverrideReasonServiceCtl,
					ManualPrescRptServiceCtl,
					PreparationCheckServiceCtl,
					PrescCheckServiceCtl,
					WorkstoreDrugListServiceCtl,
					WorkstoreDrugServiceCtl,
					DnldTrxFileServiceCtl,
					ScreenLockServiceCtl,
					MdsExceptionRptServiceCtl,
					MdsExceptionOverrideRptServiceCtl,
					DrugRouteServiceCtl,
					MedProfileOrderEditServiceCtl,
					DrugDiluentListServiceCtl,
					FdnMappingServiceCtl,
					HlaTestServiceCtl,
					SiteFormVerbServiceCtl,
					AlertProfileServiceCtl, 
					LabelDeleteServiceCtl,
					WardGroupListServiceCtl,
					WardStockServiceCtl,
					HaDrugFormularyMgmtRptServiceCtl,
					MpTrxRptServiceCtl,
					MedProfileCheckServiceCtl, 
					CheckListServiceCtl, 
					LabelReprintServiceCtl,
					CheckServiceCtl,
					UnlinkServiceCtl, 
					DmDrugLiteListServiceCtl,
					MdsExceptionEnqServiceCtl,
					SendListServiceCtl,
					SendServiceCtl, 
					PropListServiceCtl, 
					WorkstationPropListServiceCtl, 
					AssignBatchListServiceCtl, 
					AssignBatchServiceCtl,
					CorporatePropListServiceCtl,
					DrugInfoServiceCtl,
					RouteFormListServiceCtl,
					OneStopAllowModifyUncollectValidationServiceCtl,
					MpWorkingStoreListServiceCtl,
					RefillQtyAdjExclusionListServiceCtl,
					SiteListServiceCtl,
					AlertAuditServiceCtl,
					ExportKeyGuideServiceCtl,
					OrderPendingSuspendLogEnqServiceCtl,
					PmsMpSpecMappingListServiceCtl,
					ItemDispConfigServiceCtl,
					ItemSpecialtyServiceCtl,
					PmsSessionInfoListServiceCtl,
					ManualProfileCheckServiceCtl,
					CheckAtdpsServiceCtl, 
					PatientListRptServiceCtl,
					DrugRefillListRptServiceCtl,
					PatientDrugProfileListRptServiceCtl,
					AomScheduleListServiceCtl,
					MpSfiInvoiceServiceCtl,
					MpSfiInvoiceEnqServiceCtl,
					RenalAdjServiceCtl,
					UncollectPrescRptServiceCtl,
					CheckIssueUncollectServiceCtl,
					PivasDrugListServiceCtl,
					PivasFormulaServiceCtl,
					PivasSystemMaintServiceCtl,
					PivasBatchPrepServiceCtl,
					PivasRptServiceCtl,
					PivasWardMaintServiceCtl,
					PivasWorklistServiceCtl,
					PivasWorklistSummaryRptServiceCtl,
					TpnRequestServiceCtl,
					EhrAlertServiceCtl,
					CompanyListServiceCtl,
					DueOrderScheduleTemplateMaintServiceCtl,
					DrsPatientServiceCtl,
					EhrServiceCtl,
					CmmSpecialtyServiceCtl,
					CmmDrugGroupServiceCtl
				]); 
		}
	}
}
