package hk.org.ha.event.pms.main.inbox
{
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ForceProcessPendingOrdersEvent extends AbstractTideEvent
	{
		private var _medProfile:MedProfile;
		private var _callbackFunction:Function;
		
		public function ForceProcessPendingOrdersEvent(medProfile:MedProfile, callbackFunction:Function)
		{
			super();
			_medProfile = medProfile;
			_callbackFunction = callbackFunction;
		}
		
		public function get medProfile():MedProfile
		{
			return _medProfile;
		}
		
		public function get callbackFunction():Function
		{
			return _callbackFunction;
		}
	}
}