package hk.org.ha.control.pms.main.reftable.mp
{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveCommonOrderStatusInfoEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveCommonOrderStatusInfoHandlerEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveCommonOrderStatusListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveCommonOrderStatusListHandlerEvent;
	import hk.org.ha.event.pms.main.reftable.mp.UpdateCommonOrderStatusListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.UpdateCommonOrderStatusListHandlerEvent;
	import hk.org.ha.model.pms.biz.reftable.mp.CommonOrderStatusListServiceBean;
	import hk.org.ha.model.pms.vo.reftable.mp.CommonOrderStatusInfo;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("pmsCommonOrderStatusListServiceCtl", restrict="true")]
	public class CommonOrderStatusListServiceCtl
	{	
		[In]
		public var commonOrderStatusListService:CommonOrderStatusListServiceBean;

		private var callBackEvent:Event;
		
		[Observer]
		public function retrieveCommonOrderStatusInfo(evt:RetrieveCommonOrderStatusInfoEvent):void
		{
			commonOrderStatusListService.retrieveCommonOrderStatusInfo(evt.dmDrug, retrieveCommonOrderStatusInfoResult);
		}
		
		private function retrieveCommonOrderStatusInfoResult(evt:TideResultEvent):void
		{	
			evt.context.dispatchEvent( new RetrieveCommonOrderStatusInfoHandlerEvent(evt.result as CommonOrderStatusInfo) );
		}
		
		[Observer]
		public function retrieveCommonOrderStatusList(evt:RetrieveCommonOrderStatusListEvent):void
		{
			commonOrderStatusListService.retrievePmsCommonOrderStatusList(evt.patHospCode, evt.itemCode, evt.drugKey, retrieveCommonOrderStatusListResult);
		}
		
		private function retrieveCommonOrderStatusListResult(evt:TideResultEvent):void
		{	
			evt.context.dispatchEvent( new RetrieveCommonOrderStatusListHandlerEvent(evt.result as CommonOrderStatusInfo) );
		}
		
		[Observer]
		public function updateCommonOrderStatusList(evt:UpdateCommonOrderStatusListEvent):void
		{
			commonOrderStatusListService.updateCommonOrderStatusList(evt.commonOrderStatusList, updateCommonOrderStatusListResult);
		}
		
		private function updateCommonOrderStatusListResult(evt:TideResultEvent):void
		{	
			evt.context.dispatchEvent( new UpdateCommonOrderStatusListHandlerEvent() );
		}
	}
}