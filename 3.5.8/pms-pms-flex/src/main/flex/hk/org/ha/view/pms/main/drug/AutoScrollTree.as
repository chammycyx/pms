package hk.org.ha.view.pms.main.drug {
	import flash.events.Event;
	
	import hk.org.ha.fmk.pms.flex.components.core.ExtendedTree;
	
	import mx.collections.ArrayCollection;
	import mx.collections.CursorBookmark;
	import mx.collections.errors.ItemPendingError;
	import mx.controls.Tree;
	import mx.controls.listClasses.IListItemRenderer;
	import mx.core.IFactory;
	import mx.core.ScrollPolicy;
	import mx.core.mx_internal;
	import mx.events.TreeEvent;
	
	public class AutoScrollTree extends ExtendedTree
	{
		private var childNodeNum:int;
		
		public function AutoScrollTree(){
			super();
			horizontalScrollPolicy = ScrollPolicy.AUTO;
		}
		
		// we need to override maxHorizontalScrollPosition because setting
		// Tree's maxHorizontalScrollPosition adds an indent value to it,
		// which we don't need as measureWidthOfItems seems to return exactly
		// what we need.  Not only that, but getIndent() seems to be broken
		// anyways (SDK-12578).
		
		// I hate using mx_internal stuff, but we can't do
		// super.super.maxHorizontalScrollPosition in AS 3, so we have to
		// emulate it.
		override public function get maxHorizontalScrollPosition():Number
		{
			if (isNaN(mx_internal::_maxHorizontalScrollPosition))
				return 0;
			
			return mx_internal::_maxHorizontalScrollPosition;
		}
		
		override public function set maxHorizontalScrollPosition(value:Number):void
		{
			mx_internal::_maxHorizontalScrollPosition = value;
			dispatchEvent(new Event("maxHorizontalScrollPositionChanged"));
			
			scrollAreaChanged = true;
			invalidateDisplayList();
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			// we call measureWidthOfItems to get the max width of the item renderers.
			// then we see how much space we need to scroll, setting maxHorizontalScrollPosition appropriately
			var diffWidth:Number = measureWidthOfItems(0,0) - (unscaledWidth - viewMetrics.left - viewMetrics.right);
			
			if (diffWidth <= 0) {
				maxHorizontalScrollPosition = 0;
			} else {
				maxHorizontalScrollPosition = diffWidth;
			}
			
			super.updateDisplayList(unscaledWidth, unscaledHeight);
		}

		// scroll and place the item at tree bottom 
		public function ensureItemVisibleAtBottom(item:Object):Boolean {
			var itemIndex:int = getItemIndex(item); 
			if (itemIndex < 0) {
				return false;
			}
			return ensureItemIndexVisibleAtBottom(itemIndex);
		}
	
		// scroll and place the item index item at tree bottom 
		public function ensureItemIndexVisibleAtBottom(itemIndex:int, scrollLimit:int=NaN):Boolean {
			var bottomIndex:int = verticalScrollPosition + listItems.length - offscreenExtraRowsBottom;
			bottomIndex--;
			
			if (itemIndex >= bottomIndex || itemIndex < verticalScrollPosition) {
				var scrollPos:int = itemIndex - bottomIndex;
				var newVPos:int;
				if (isNaN(scrollLimit)) {
					newVPos = Math.min(verticalScrollPosition + scrollPos, maxVerticalScrollPosition);
				} else {
					newVPos = Math.min(verticalScrollPosition + scrollPos, maxVerticalScrollPosition, scrollLimit);
				} 
				verticalScrollPosition = newVPos;
				return true;
			}
			return false;
		}
		
		// scroll and place the last child node at tree bottom 
		public function ensureAllChildVisibleAtBottom(item:Object):Boolean {
			var itemIndex:int = getItemIndex(item); 
			if (itemIndex < 0) {
				return false;
			}
			
			// get child node number
			this.childNodeNum = 0;		
			if (isItemOpen(item) && item.children != null && item.children.length > 0) {
				execFuncOnAllTreeNode(item.children as ArrayCollection, countChildNode);
			}
			
			return ensureItemIndexVisibleAtBottom(itemIndex + this.childNodeNum + 1, itemIndex);
		}
		
		private function countChildNode(item:Object):void {
			childNodeNum++;
		}
		
		// execute a function on all tree node
		// rootNodeList: list of all rootNode
		// execFunc: function to execute (must contain 1 parameter in Object type to accept treeNode object)
		public function execFuncOnAllTreeNode(rootNodeList:ArrayCollection, execFunc:Function):void {
			for each (var rootNode:Object in rootNodeList) {
				execFuncOnTreeNode(rootNode, execFunc);
			}			
		}
		
		private function execFuncOnTreeNode(node:Object, execFunc:Function):void {
			execFunc(node);
			if (node.children == null || node.children.length == 0) {
				return;
			}
			for each (var child:Object in node.children) {
				execFuncOnTreeNode(child, execFunc);
			}
		}
	}
}