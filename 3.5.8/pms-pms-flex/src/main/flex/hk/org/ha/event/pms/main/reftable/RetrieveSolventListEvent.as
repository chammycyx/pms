package hk.org.ha.event.pms.main.reftable {
	
	import hk.org.ha.view.pms.main.vetting.popup.OrderEditItemPopup;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSolventListEvent extends AbstractTideEvent 
	{		
		private var _obj:Object
		
		public function RetrieveSolventListEvent(obj:Object=null):void 
		{
			super();
			_obj= obj;
		}
		
		public function get obj():Object
		{
			return _obj;
		}
	}
}