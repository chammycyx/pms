package hk.org.ha.event.pms.main.vetting.mp
{
	import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ReceiveRecalculatedQtyEvent extends AbstractTideEvent
	{
		private var _medProfilePoItem:MedProfilePoItem;
		
		public function ReceiveRecalculatedQtyEvent(medProfilePoItem:MedProfilePoItem)
		{
			super();
			_medProfilePoItem = medProfilePoItem;
		}
		
		public function get medProfilePoItem():MedProfilePoItem
		{
			return _medProfilePoItem;
		}
	}
}