package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDmDrugListByDrugKeyFmStatusEvent extends AbstractTideEvent 
	{
		private var _drugKey:Number;		
		private var _fmStatus:String;		
		private var _callBackEvent:Event;
		
		public function RetrieveDmDrugListByDrugKeyFmStatusEvent(drugKey:Number, fmStatusValue:String, callBackEvent:Event=null):void 
		{
			super();
			_drugKey = drugKey;
			_fmStatus = fmStatusValue;
			_callBackEvent = callBackEvent;
		}
		
		public function get drugKey():Number
		{
			return _drugKey;
		}
		
		public function get fmStatus():String
		{
			return _fmStatus;
		}
		
		public function get callBackEvent():Event
		{
			return _callBackEvent;
		}
	}
}