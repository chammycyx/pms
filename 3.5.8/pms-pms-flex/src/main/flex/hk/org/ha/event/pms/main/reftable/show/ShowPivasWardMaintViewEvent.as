package hk.org.ha.event.pms.main.reftable.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowPivasWardMaintViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		private var _enforce:Boolean;
		
		public function ShowPivasWardMaintViewEvent(clearMessages:Boolean=true, enforce:Boolean=false):void 
		{
			super();
			_clearMessages = clearMessages;
			_enforce = enforce;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
		
		public function get enforce():Boolean {
			return _enforce;
		}
	}
}