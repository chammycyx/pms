package hk.org.ha.event.pms.main.report {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrieveMdsExceptionRptByDeliveryReqIdEvent extends AbstractTideEvent {
		
		private var _deliveryReqId:Number;
		
		private var _callback:Function;
		
		public function RetrieveMdsExceptionRptByDeliveryReqIdEvent(deliveryReqId:Number, callback:Function=null) {
			super();
			_deliveryReqId = deliveryReqId;
			_callback = callback;
		}
		
		public function get deliveryReqId():Number {
			return _deliveryReqId;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}