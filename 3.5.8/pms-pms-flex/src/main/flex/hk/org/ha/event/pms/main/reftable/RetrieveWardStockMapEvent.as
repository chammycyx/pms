package hk.org.ha.event.pms.main.reftable {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveWardStockMapEvent extends AbstractTideEvent 
	{
		private var _callBackFunction:Function;
		
		public function RetrieveWardStockMapEvent(functionValue:Function=null):void 
		{
			super();
			this._callBackFunction = functionValue;
		}
		
		public function get callBackFunction():Function
		{
			return this._callBackFunction;
		}
	}
}