package hk.org.ha.event.pms.main.report {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshWorkloadStatRptEvent extends AbstractTideEvent 
	{
		private var _retrieveSuccess:Boolean;
		
		public function RefreshWorkloadStatRptEvent(retrieveSuccess:Boolean):void 
		{
			super();
			_retrieveSuccess = retrieveSuccess;
		}
		
		public function get retrieveSuccess():Boolean
		{
			return _retrieveSuccess;
		}
		
	}
}