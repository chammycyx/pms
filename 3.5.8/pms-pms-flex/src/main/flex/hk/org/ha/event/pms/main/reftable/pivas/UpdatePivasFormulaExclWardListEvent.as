package hk.org.ha.event.pms.main.reftable.pivas
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdatePivasFormulaExclWardListEvent extends AbstractTideEvent
	{
		private var _pivasFormulaId:Number;
		
		private var _pivasFormulaExclWardList:ArrayCollection;
		
		private var _successCallback:Function;
		
		public function UpdatePivasFormulaExclWardListEvent(pivasFormulaId:Number, pivasFormulaExclWardList:ArrayCollection, successCallback:Function)
		{
			super();
			_pivasFormulaId = pivasFormulaId;
			_pivasFormulaExclWardList = pivasFormulaExclWardList;
			_successCallback = successCallback;
		}
		
		public function get pivasFormulaId():Number
		{
			return _pivasFormulaId;
		}
		
		public function get pivasFormulaExclWardList():ArrayCollection
		{
			return _pivasFormulaExclWardList;
		}
		
		public function get successCallback():Function
		{
			return _successCallback;
		}
		
	}
}