package hk.org.ha.event.pms.main.reftable.popup
{
	import hk.org.ha.model.pms.dms.persistence.PmsCommonOrderStatus;

	public class CommonOrderStatusMaintPopupProp
	{
		public var commonOrderStatus:PmsCommonOrderStatus;
		public var activeHandler:Function;
		public var inactiveHandler:Function;
		public var cancelHandler:Function;
	}
}
