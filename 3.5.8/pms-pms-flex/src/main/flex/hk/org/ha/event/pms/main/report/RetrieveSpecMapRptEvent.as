package hk.org.ha.event.pms.main.report {
	
	import hk.org.ha.model.pms.dms.vo.pms.PmsSpecMapReportCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSpecMapRptEvent extends AbstractTideEvent 
	{
		private var _pmsSpecMapReportCriteria:PmsSpecMapReportCriteria;
		
		public function RetrieveSpecMapRptEvent(pmsSpecMapReportCriteria:PmsSpecMapReportCriteria):void {
			super();
			this._pmsSpecMapReportCriteria = pmsSpecMapReportCriteria;
		}
		
		public function get pmsSpecMapReportCriteria():PmsSpecMapReportCriteria {
			return _pmsSpecMapReportCriteria;
		}
		
	}
}