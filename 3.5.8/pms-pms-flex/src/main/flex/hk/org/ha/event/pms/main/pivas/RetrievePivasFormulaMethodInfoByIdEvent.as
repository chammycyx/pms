package hk.org.ha.event.pms.main.pivas
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePivasFormulaMethodInfoByIdEvent extends AbstractTideEvent 
	{		
		private var _pivasFormulaMethodId:Number;
		
		private var _callbackFunc:Function;
		
		public function RetrievePivasFormulaMethodInfoByIdEvent(pivasFormulaMethodId:Number, callbackFunc:Function):void 
		{
			super();
			_pivasFormulaMethodId = pivasFormulaMethodId;
			_callbackFunc = callbackFunc;
		}

		public function get pivasFormulaMethodId():Number {
			return _pivasFormulaMethodId;
		}

		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}