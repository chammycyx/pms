package hk.org.ha.event.pms.main.reftable.popup {
	
	import hk.org.ha.model.pms.udt.vetting.OrderType;
	
	import mx.controls.LinkButton;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowQtyDurationConversionRptTypePopupEvent extends AbstractTideEvent 
	{
		private var _exportButton:LinkButton;

		private var _callBackFunc:Function;
		
		private var _orderType:OrderType;
		
		private var _rptType:String;
		
		public function get exportButton():LinkButton
		{
			return _exportButton;
		}
		
		public function get callBackFunc():Function
		{
			return _callBackFunc;
		}
		
		public function get orderType():OrderType
		{
			return _orderType;
		}
		
		public function get rptType():String
		{
			return _rptType;
		}

		public function ShowQtyDurationConversionRptTypePopupEvent(exportButton:LinkButton, callBackFunc:Function, orderType:OrderType, rptType:String):void 
		{
			super();
			_exportButton = exportButton;
			_callBackFunc = callBackFunc;
			_orderType = orderType;
			_rptType = rptType;
		}
	}
}