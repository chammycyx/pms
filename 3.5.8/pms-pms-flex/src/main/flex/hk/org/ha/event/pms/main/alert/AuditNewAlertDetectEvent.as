package hk.org.ha.event.pms.main.alert {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	import mx.collections.ListCollectionView;
	
	public class AuditNewAlertDetectEvent extends AbstractTideEvent {
		
		private var _action:String;
		
		private var _medProfileId:Number;
		
		private var _hkid:String;
		
		private var _caseNum:String;
		
		private var _alertMsgList:ListCollectionView;
		
		public function AuditNewAlertDetectEvent(action:String, medProfileId:Number, hkid:String, caseNum:String, alertMsgList:ListCollectionView) {
			super();
			_action = action;
			_medProfileId = medProfileId;
			_hkid = hkid;
			_caseNum = caseNum;
			_alertMsgList = alertMsgList;
		}
		
		public function get action():String {
			return _action;
		}

		public function get medProfileId():Number {
			return _medProfileId;
		}

		public function get hkid():String {
			return _hkid;
		}

		public function get caseNum():String {
			return _caseNum;
		}

		public function get alertMsgList():ListCollectionView {
			return _alertMsgList;
		}
	}
}