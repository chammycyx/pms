package hk.org.ha.event.pms.main.reftable.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowPivasDrugReconstitutionPopupEvent extends AbstractTideEvent 
	{
		private var _pivasDrugReconstitutionPopupProp:PivasDrugReconstitutionPopupProp;
		
		public function ShowPivasDrugReconstitutionPopupEvent(pivasDrugReconstitutionPopupProp:PivasDrugReconstitutionPopupProp):void 
		{
			super();
			_pivasDrugReconstitutionPopupProp = pivasDrugReconstitutionPopupProp;
		}
		
		public function get pivasDrugReconstitutionPopupProp():PivasDrugReconstitutionPopupProp{
			return _pivasDrugReconstitutionPopupProp;
		}
	}
}