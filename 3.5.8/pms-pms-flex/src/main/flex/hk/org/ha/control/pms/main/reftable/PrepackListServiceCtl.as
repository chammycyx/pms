package hk.org.ha.control.pms.main.reftable {
	
	import hk.org.ha.event.pms.main.reftable.RefreshPrepackViewEvent;
	import hk.org.ha.event.pms.main.reftable.RetrievePrepackListEvent;
	import hk.org.ha.event.pms.main.reftable.UpdatePrepackEvent;
	import hk.org.ha.model.pms.biz.reftable.PrepackListServiceBean;
	import hk.org.ha.view.pms.main.reftable.popup.ItemLocationMaintPrepackPopup;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("prepackListServiceCtl", restrict="true")]
	public class PrepackListServiceCtl 
	{
		[In]
		public var prepackListService:PrepackListServiceBean;
		
		private var _popup:ItemLocationMaintPrepackPopup;
		
		[Observer]
		public function retrievePrepackList(evt:RetrievePrepackListEvent):void
		{
			_popup = evt.popup;
			prepackListService.retrievePrepackByItemCode(evt.itemCode, retrievePrepackListResult);
		}
		
		private function retrievePrepackListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshPrepackViewEvent(_popup));
		}
		
		[Observer]
		public function updatePrepackList(evt:UpdatePrepackEvent):void
		{
			prepackListService.updatePrepack(evt.updatePrepack);
		}
	}
}
