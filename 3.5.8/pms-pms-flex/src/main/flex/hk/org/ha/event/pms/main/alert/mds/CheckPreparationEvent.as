package hk.org.ha.event.pms.main.alert.mds {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dms.vo.PreparationProperty;
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	
	public class CheckPreparationEvent extends AbstractTideEvent {
		
		private var _medOrderItem:MedOrderItem;
		
		private var _preparationProperty:PreparationProperty;
		
		private var _successCallback:Function;
		
		private var _returnVettingFunc:Function;
		
		private var _prescribeAlternativeFunc:Function;
		
		public function CheckPreparationEvent(medOrderItem:MedOrderItem, preparationProperty:PreparationProperty, successCallback:Function=null, returnVettingFunc:Function=null, prescribeAlternativeFunc:Function=null) {
			super();
			_medOrderItem = medOrderItem;
			_preparationProperty = preparationProperty;
			_successCallback = successCallback;
			_returnVettingFunc = returnVettingFunc;
			_prescribeAlternativeFunc = prescribeAlternativeFunc;
		}
		
		public function get medOrderItem():MedOrderItem {
			return _medOrderItem;
		}
		
		public function get preparationProperty():PreparationProperty {
			return _preparationProperty;
		}
		
		public function get successCallback():Function {
			return _successCallback;
		}
		
		public function get returnVettingFunc():Function {
			return _returnVettingFunc;
		}
		
		public function get prescribeAlternativeFunc():Function {
			return _prescribeAlternativeFunc;
		}
	}
}
