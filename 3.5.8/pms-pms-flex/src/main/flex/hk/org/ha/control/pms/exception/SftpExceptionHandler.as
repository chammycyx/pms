package hk.org.ha.control.pms.exception {

    import hk.org.ha.event.pms.main.report.popup.ShowSftpExceptionPopupEvent;
    
    import mx.messaging.messages.ErrorMessage;
    
    import org.granite.tide.BaseContext;
    import org.granite.tide.IExceptionHandler;

    public class SftpExceptionHandler implements IExceptionHandler 
	{
		public static const SFTP_COMMAND_EXCEPTION:String = "SftpCommandException";
		private static const EXCEPTION_MESSAGE:String = "Failed to access file server." +
														"  Please contact ITD for support.";
        
        
        public function accepts(emsg:ErrorMessage):Boolean 
		{
          return emsg.faultCode == SFTP_COMMAND_EXCEPTION;
        }

        public function handle(context:BaseContext, emsg:ErrorMessage):void 
		{
			context.dispatchEvent(new ShowSftpExceptionPopupEvent("0351", emsg.extendedData["Exception Class"], EXCEPTION_MESSAGE));
        }
    }
}
