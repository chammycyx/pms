package hk.org.ha.event.pms.main.pivas
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetPivasBatchPrepListEvent extends AbstractTideEvent 
	{		
		private var _pivasBatchPrepList:ArrayCollection;
		
		public function SetPivasBatchPrepListEvent(pivasBatchPrepList:ArrayCollection):void 
		{
			super();
			_pivasBatchPrepList = pivasBatchPrepList;
		}
		
		public function get pivasBatchPrepList():ArrayCollection {
			return _pivasBatchPrepList;
		}
	}
}