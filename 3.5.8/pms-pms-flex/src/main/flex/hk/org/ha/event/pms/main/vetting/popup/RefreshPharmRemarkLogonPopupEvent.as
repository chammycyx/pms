package hk.org.ha.event.pms.main.vetting.popup
{
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class RefreshPharmRemarkLogonPopupEvent extends AbstractTideEvent
	{
		private var _logonFailMsg:Array;
		private var _logonCallbackEvent:Event;
		
		public function RefreshPharmRemarkLogonPopupEvent(logonFailMsg:Array, logonCallbackEvent:Event)
		{
			super();
			_logonFailMsg = logonFailMsg;
			_logonCallbackEvent = logonCallbackEvent;
		}
		
		public function get logonFailMsg():Array {
			return _logonFailMsg;
		}
		
		public function get logonCallbackEvent():Event {
			return _logonCallbackEvent;
		}
	}
}