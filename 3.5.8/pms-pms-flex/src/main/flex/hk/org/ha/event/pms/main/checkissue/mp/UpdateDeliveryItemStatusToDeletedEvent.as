package hk.org.ha.event.pms.main.checkissue.mp
{
	import hk.org.ha.model.pms.vo.checkissue.mp.LabelDelete;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateDeliveryItemStatusToDeletedEvent extends AbstractTideEvent 
	{			
		private var _labelDelete:LabelDelete;
		
		public function UpdateDeliveryItemStatusToDeletedEvent(labelDelete:LabelDelete):void 
		{
			super();
			_labelDelete = labelDelete;
		}
		
		public function get labelDelete():LabelDelete { 
			return _labelDelete;
		}
	}
}