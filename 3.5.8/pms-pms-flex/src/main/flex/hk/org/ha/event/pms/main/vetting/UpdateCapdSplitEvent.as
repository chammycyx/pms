package hk.org.ha.event.pms.main.vetting {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.vetting.CapdSplit;
	
	public class UpdateCapdSplitEvent extends AbstractTideEvent 
	{		
		private var _capdSplit:CapdSplit;
		private var _setGenNewVoucherFlag:Boolean
		private var _callbackFunc:Function;
		
		public function UpdateCapdSplitEvent(capdSplit:CapdSplit, setGenNewVoucherFlag:Boolean, callbackFunc:Function=null) {
			super();
			_capdSplit = capdSplit;
			_setGenNewVoucherFlag = setGenNewVoucherFlag;
			_callbackFunc = callbackFunc;
		}
		
		public function get capdSplit():CapdSplit {
			return _capdSplit;
		}
		
		public function get setGenNewVoucherFlag():Boolean {
			return _setGenNewVoucherFlag;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}
