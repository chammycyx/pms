package hk.org.ha.control.pms.main.refill{
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	import mx.controls.List;
	import mx.core.IFlexDisplayObject;
	import mx.core.UIComponent;
	import mx.managers.PopUpManager;
	import mx.utils.ObjectUtil;
	
	import hk.org.ha.event.pms.main.alert.mds.ShowPrescAlertErrorEvent;
	import hk.org.ha.event.pms.main.alert.mds.ShowPrescAlertMsgEvent;
	import hk.org.ha.event.pms.main.alert.mds.popup.ShowMdsLogPopupEvent;
	import hk.org.ha.event.pms.main.refill.CancelRefillEvent;
	import hk.org.ha.event.pms.main.refill.ModifyRefillEvent;
	import hk.org.ha.event.pms.main.refill.RefreshReprintRefillCouponEvent;
	import hk.org.ha.event.pms.main.refill.RefreshTerminateRefillEvent;
	import hk.org.ha.event.pms.main.refill.RefreshUpdateRefillScheduleListEvent;
	import hk.org.ha.event.pms.main.refill.ReprintRefillCouponEvent;
	import hk.org.ha.event.pms.main.refill.SaveRefillExceptionMsgEvent;
	import hk.org.ha.event.pms.main.refill.StartRefillEvent;
	import hk.org.ha.event.pms.main.refill.UpdateRefillScheduleEvent;
	import hk.org.ha.event.pms.main.refill.UpdateRefillScheduleListEvent;
	import hk.org.ha.event.pms.main.refill.UpdateRefillScheduleListForVettingEvent;
	import hk.org.ha.event.pms.main.refill.show.ShowRefillPrescriptionViewEvent;
	import hk.org.ha.event.pms.main.show.ShowStartupViewEvent;
	import hk.org.ha.event.pms.main.vetting.EnableVettingEvent;
	import hk.org.ha.event.pms.main.vetting.show.ShowOrderViewEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	import hk.org.ha.fmk.pms.flex.components.window.Window;
	import hk.org.ha.fmk.pms.flex.components.window.WindowCloseEvent;
	import hk.org.ha.model.pms.biz.refill.PrintRefillCouponServiceBean;
	import hk.org.ha.model.pms.biz.refill.RefillScheduleListServiceBean;
	import hk.org.ha.model.pms.biz.refill.RefillScheduleServiceBean;
	import hk.org.ha.model.pms.persistence.corp.Workstore;
	import hk.org.ha.model.pms.persistence.disp.PharmOrder;
	import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
	import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
	import hk.org.ha.model.pms.persistence.disp.RefillScheduleItem;
	import hk.org.ha.model.pms.persistence.disp.Ticket;
	import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
	import hk.org.ha.model.pms.udt.disp.DispOrderDayEndStatus;
	import hk.org.ha.model.pms.udt.refill.RefillScheduleActionType;
	import hk.org.ha.model.pms.udt.refill.RefillScheduleRefillStatus;
	import hk.org.ha.model.pms.udt.refill.RefillScheduleStatus;
	import hk.org.ha.model.pms.vo.ehr.EhrPatient;
	import hk.org.ha.model.pms.vo.refill.RefillPrescInfo;
	import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;
	
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("refillScheduleListServiceCtl", restrict="true")]
	public class RefillScheduleListServiceCtl 
	{
			
		[In]
		public var refillScheduleList:ArrayCollection;
		
		[In]
		public var refillPoiItemNumList:ArrayCollection;
	
		[In]
		public var refillPoiItemNumInitList:ArrayCollection;
		
		[In]
		public var pharmOrder:PharmOrder;
		
		[In]
		public var orderViewInfo:OrderViewInfo;
		
		[In]
		public var ctx:Context;
		
		[In]
		public var refillScheduleService:RefillScheduleServiceBean;
		
		[In]
		public var refillScheduleListService:RefillScheduleListServiceBean;
		
		[In]
		public var printRefillCouponService:PrintRefillCouponServiceBean;		
		
		[In]
		public var prescAlertMsgList:ArrayCollection;
		
		[In]
		public var refillPrescInfo:RefillPrescInfo;
		
		[In]
		public var workstore:Workstore;
		
		[In]
		public var window:Window;
		
		private var dummyRefillSchedule:RefillSchedule;
		
		private var dummyRefillScheduleItem:RefillScheduleItem;
		
		private var dummyPharmOrderItem:PharmOrderItem;
		
		private var updateRefillScheduleListCallback:Function;
		
		private var startRefillSourceView:String;
		
		private var ehrPatient:EhrPatient;
		
		[Observer]
		public function startRefillScheduleList(evt:StartRefillEvent):void
		{			
			startRefillSourceView = evt.sourceView;
			ehrPatient = evt.ehrPatient;
			
			var manualRefillSave:Boolean = false;
			if( evt.sourceView == 'vetting' && evt.callbackFunc != null ){
				manualRefillSave = true;
			}
			ctx.meta_updates.removeAll();
			switch ( evt.sourceView ){
				case 'vetting':
					refillScheduleListService.retrieveRefillScheduleFromVetting(manualRefillSave, retrieveRefillScheduleListResult);
					break;
				case 'checkIssue':
					refillScheduleListService.retrieveRefillScheduleFromCheckIssue(evt.refillScheduleId, retrieveRefillScheduleListResult);
					break;
				case 'onestop':
					refillScheduleListService.retrieveRefillScheduleFromOneStop(evt.refillScheduleId, retrieveRefillScheduleListResult);
					break;
				
			}
			
			updateRefillScheduleListCallback = evt.callbackFunc;
		}
		
		[Observer]
		public function reprintStandardRefillCoupon(evt:ReprintRefillCouponEvent):void
		{
			ctx.meta_updates.removeAll();
			printRefillCouponService.printStandardRefillCoupon(evt.refillSchedule, true, evt.printLang, evt.numOfCoupon, reprintStandardRefillCouponResult);
		}

		private function reprintStandardRefillCouponResult(evt:TideResultEvent):void{
			evt.context.dispatchEvent(new RefreshReprintRefillCouponEvent());
		}
		
		[Observer]
		public function updateRefillScheduleForVetting( evt:UpdateRefillScheduleListForVettingEvent ):void{
			var pharmOrderIn:PharmOrder = evt.pharmOrder;
			updateRefillScheduleListCallback = evt.callbackFunc;
			
			var reqRefillPoiItemNumList:ArrayCollection = new ArrayCollection();
			for each( var poiIn:PharmOrderItem in pharmOrderIn.pharmOrderItemList ){
				if( poiIn.requireRefill ){
					reqRefillPoiItemNumList.addItem(poiIn.orgItemNum);
				}
			}
			
			var testRsList:ArrayCollection = new ArrayCollection();
			
			for each ( var rsIn:RefillSchedule in pharmOrderIn.refillScheduleList ){
				
				var copyRs:RefillSchedule = new RefillSchedule();
				copyRs.copy(rsIn);
				
				var copyRsiList:ArrayCollection = new ArrayCollection();
				for each ( var rsiIn:RefillScheduleItem in rsIn.refillScheduleItemList ) {
					if( reqRefillPoiItemNumList.contains(rsiIn.itemNum) ){
						var copyRsi:RefillScheduleItem = new RefillScheduleItem();
						copyRsi.copy(rsiIn);					
						copyRsi.refillSchedule = copyRs;
						copyRsiList.addItem(copyRsi);
					}else{
						rsiIn.itemNum = NaN;
						rsiIn.refillSchedule = null;
						rsiIn.pharmOrderItem = null;
					}
				}
				copyRs.refillScheduleItemList = copyRsiList;
				if( copyRsiList.length > 0 ){
					testRsList.addItem( copyRs );
				}
			}
			ctx.meta_updates.removeAll();
			refillScheduleListService.updateStdRefillScheduleList(testRsList, updateRefillScheduleListResult);
		}
		
		private var cancelRefillCallback:Function;
		
		[Observer]
		public function cancelRefillAction( evt:CancelRefillEvent ):void{
			cancelRefillCallback = evt.callback;
			refillScheduleService.cancelRefill(evt.orderViewInfo, cancelRefillActionResult);
		}

		private function cancelRefillActionResult(evt:TideResultEvent):void{
			if( cancelRefillCallback != null ){
				cancelRefillCallback();
			}
		}

		private var modifyRefillEvent:ModifyRefillEvent;
		
		[Observer]
		public function modifyRefillAction(evt:ModifyRefillEvent):void{
			modifyRefillEvent = evt;
			refillScheduleService.modifyRefillSuccess(evt.orderViewInfo, modifyRefillActionResult);
		}
		
		private function modifyRefillActionResult(evt:TideResultEvent):void{
			modifyRefillEvent.callback(evt.result as String);
		}

		[Observer]
		public function updateRefillSchedule( evt:UpdateRefillScheduleEvent ):void{			
			
			var currRefillSchedule:RefillSchedule = this.disconnectRefillSchedule(evt.refillSchedule);			
			updateRefillScheduleListCallback = null;
			ctx.meta_updates.removeAll();
			switch(evt.updateAction){
				case 'refill':				
					refillScheduleService.refillStdRefillSchedule(currRefillSchedule, evt.orderViewInfo, refreshRefillWithMdsAlertCheckResult );
					break;
				case 'forceComplete':
					refillScheduleService.forceCompleteStdRefillSchedule(currRefillSchedule, evt.orderViewInfo, updateStandardRefillScheduleResult );
					break;
				case 'abort':
					refillScheduleService.abortStdRefillScheduleList(currRefillSchedule, evt.orderViewInfo, refreshUpdateRefillScheduleListResult );
					break;
				case 'cancelDispense':
					refillScheduleService.cancelDispenseStdRefillSchedule(currRefillSchedule, evt.orderViewInfo, updateStandardRefillScheduleResult);
					break;
				case 'suspend':
					refillScheduleService.suspendRefillSchedule(currRefillSchedule, evt.orderViewInfo, refreshUpdateRefillScheduleListResult);
					break;
				case 'reverseSuspend':
					refillScheduleService.reverseSuspendRefillSchedule(currRefillSchedule, evt.orderViewInfo, refreshReverseSuspendWithMdsAlertCheckResult);
					break;
				case 'terminate':
					refillScheduleService.terminateStdRefillScheduleList(currRefillSchedule, evt.orderViewInfo, updateStandardRefillScheduleResult);
					break;
			}
		}
		
		[Observer]
		public function updateRefillScheduleList(evt:UpdateRefillScheduleListEvent):void
		{
			var pharmOrderIn:PharmOrder = evt.pharmOrder;
			updateRefillScheduleListCallback = evt.callbackFunc;
			
			switch( evt.updateAction ){
				case 'edit':
					var testRsList:ListCollectionView = disconnectRefillScheduleList(pharmOrderIn.refillScheduleList);
					ctx.meta_updates.removeAll();
					refillScheduleListService.updateRefillScheduleListOnly(testRsList, evt.currRsId, updateRefillScheduleListOnlyResult);
					break;
				case 'cancel':
					updateRefillScheduleListCallback = null;
					disconnectRefillScheduleListForCancelAction(pharmOrderIn);
					ctx.meta_updates.removeAll();
					refillScheduleListService.updateStdRefillScheduleList(null, cancelUpdateRefillScheduleListResult);
					break;
			}			
		}
		
		private function retrieveRefillScheduleListResult(evt:TideResultEvent):void{
			if( refillScheduleList.length > 0 || refillPoiItemNumList.length > 0 || refillPoiItemNumInitList.length > 0 ){
				for each ( var initPoi:PharmOrderItem in pharmOrder.pharmOrderItemList ) {					
					initPoi.refillScheduleItemList = new ArrayCollection();
				}
				
				for each( var rs:RefillSchedule in refillScheduleList ){
					
					if( rs.refillNum == 1 && rs.ticket == null ){
						rs.ticket = pharmOrder.medOrder.ticket;
					}
					
					for each ( var rsi:RefillScheduleItem in rs.refillScheduleItemList ) {
						rsi.refillSchedule = rs;
						rsi.refillNum = rs.refillNum;
						
						for each ( var poi:PharmOrderItem in pharmOrder.pharmOrderItemList ) {					
							if( rsi.itemNum == poi.orgItemNum ){
								rsi.pharmOrderItem = poi;								
								poi.refillScheduleItemList.addItem(rsi);							
							}	
						}						
					}
					
					rs.pharmOrder = pharmOrder;					
				}
				pharmOrder.refillScheduleList = refillScheduleList;				
				pharmOrder = updateRefillScheduleAllowEditProp(pharmOrder);								
				
				
				if (startRefillSourceView == "vetting") {
					dispatchEvent(new EnableVettingEvent(true));
				}
				
				evt.context.dispatchEvent(new ShowRefillPrescriptionViewEvent(pharmOrder, updateRefillScheduleListCallback, false, true, ehrPatient));
			}else{
				if( updateRefillScheduleListCallback != null ){
					updateRefillScheduleListCallback();
				}
			}
		}
		
		private function refreshUpdateRefillScheduleListResult(evt:TideResultEvent):void{
			if( refillScheduleList.length > 0 ){
				for each ( var initPoi:PharmOrderItem in pharmOrder.pharmOrderItemList ) {					
					initPoi.refillScheduleItemList = new ArrayCollection();
				}
				
				for each( var rs:RefillSchedule in refillScheduleList ){
					
					if( rs.refillNum == 1 && rs.ticket == null ){
						rs.ticket = pharmOrder.medOrder.ticket;
					}
					
					for each ( var rsi:RefillScheduleItem in rs.refillScheduleItemList ) {						
						rsi.refillSchedule = rs;
						rsi.refillNum = rs.refillNum;
						
						for each ( var poi:PharmOrderItem in pharmOrder.pharmOrderItemList ) {					
							if( rsi.itemNum == poi.orgItemNum ){
								rsi.pharmOrderItem = poi;								
								poi.refillScheduleItemList.addItem(rsi);							
							}	
						}						
					}
					
					rs.pharmOrder = pharmOrder;					
				}
				pharmOrder.refillScheduleList = refillScheduleList;
				pharmOrder = updateRefillScheduleAllowEditProp(pharmOrder);				
				evt.context.dispatchEvent(new RefreshUpdateRefillScheduleListEvent(pharmOrder));
			}
		}
		
		private function refreshRefillWithMdsAlertCheckResult(evt:TideResultEvent):void{
			dispatchEvent(new ShowMdsLogPopupEvent(function():void {
				
				// show mds alert msg
				dispatchEvent(new ShowPrescAlertMsgEvent(true, function():void {
					
					if (prescAlertMsgList != null && prescAlertMsgList.length > 0) {
						refillPrescInfo.refillActionType = RefillScheduleActionType.TerminateRefill;
						refreshTerminateRefillResult(evt);
					}else{
						updateStandardRefillScheduleResult(evt);
					}
					
				}));
			}));
		}
		
		private function refreshReverseSuspendWithMdsAlertCheckResult(evt:TideResultEvent):void{
			dispatchEvent(new ShowMdsLogPopupEvent(function():void {
				
				// show mds alert msg
				dispatchEvent(new ShowPrescAlertMsgEvent(true, function():void {
					
					if (prescAlertMsgList != null && prescAlertMsgList.length > 0) {
						refillPrescInfo.refillActionType = RefillScheduleActionType.TerminateRefill;
						refreshTerminateRefillResult(evt);
					}else{
						refreshUpdateRefillScheduleListResult(evt);
					}
				}));
			}));
		}
		//For terminate refill
		private function refreshTerminateRefillResult(evt:TideResultEvent):void{
			evt.context.dispatchEvent(new RefreshTerminateRefillEvent());
		}
		
		private function updateStandardRefillScheduleResult(evt:TideResultEvent):void{
			if( refillPrescInfo.infoMsgCodeList.length > 0 ){
				this.showMsgList(refillPrescInfo.infoMsgCodeList, refillPrescInfo.infoMsgParamList, 0, showOneStopEntry);
			}else{
				showOneStopEntry(evt);
			}
		}
		
		private function showMsgList(msgCodeList:ListCollectionView, msgParamList:ListCollectionView, index:int, callbackFunc:Function=null):void {
			
			if (msgCodeList.length <= index) {
				if (callbackFunc != null) {
					callbackFunc();
				}
				return;
			}
			
			var msgCode:String = msgCodeList.getItemAt(index)  as String;
			var msgParam:Array = msgParamList.getItemAt(index) as Array;
			
			var sysMsgProp:SystemMessagePopupProp = new SystemMessagePopupProp(msgCode);
			sysMsgProp.setOkButtonOnly = true;
			
			if (msgParam != null && msgParam.length > 0) {
				sysMsgProp.messageParams = msgParam;
			}
			
			sysMsgProp.okHandler = function(mouseEvent:MouseEvent):void {
				showMsgList(msgCodeList, msgParamList, index + 1, callbackFunc);
				PopUpManager.removePopUp(((mouseEvent.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
			};
			
			dispatchEvent(new RetrieveSystemMessageEvent(sysMsgProp));
		}
		
		private function showOneStopEntry(evt:TideResultEvent=null):void {
			var cancelRefillCallback:Function = function():void{
				window.closeWindowHandler(new WindowCloseEvent(WindowCloseEvent.CLOSE, ShowStartupViewEvent));
			}
			dispatchEvent(new CancelRefillEvent(orderViewInfo, cancelRefillCallback));
		}
		
		private function updateRefillScheduleListResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			pharmOrder.pharmOrderItemList.filterFunction = null;
			pharmOrder.pharmOrderItemList.refresh();
			if( updateRefillScheduleListCallback != null ){
				updateRefillScheduleListCallback();
			}else{
				for each( var poi:PharmOrderItem in pharmOrder.pharmOrderItemList ){
					poi.stdRefillCheckFlag = true;
					
					if( poi.refillScheduleItemList != null && poi.refillScheduleItemList.length > 0 ){
						for each( var rsi:RefillScheduleItem in poi.refillScheduleItemList ){
							rsi.refillSchedule = null;
							rsi.pharmOrderItem = null;
							rsi.itemNum = NaN;
						}
						poi.refillScheduleItemList = new ArrayCollection();
					}
				}
				window.currentMenuItem.customLabel = "[ Order Entry ]";
				evt.context.dispatchEvent(new ShowOrderViewEvent());
			}
		}
		
		private function updateRefillScheduleListOnlyResult(evt:TideResultEvent):void{
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			refreshUpdateRefillScheduleListResult(evt);
		}
		
		private function cancelUpdateRefillScheduleListResult(evt:TideResultEvent):void{
			if( updateRefillScheduleListCallback != null ){
				updateRefillScheduleListCallback();
			}else{
				pharmOrder.pharmOrderItemList.filterFunction = null;
				pharmOrder.pharmOrderItemList.refresh();
				window.currentMenuItem.customLabel = "[ Order Entry ]";
				evt.context.dispatchEvent(new ShowOrderViewEvent());
			}
		}
		
		private function disconnectRefillScheduleListForCancelAction(pharmOrderIn:PharmOrder):void{			
			for each ( var rs:RefillSchedule in pharmOrderIn.refillScheduleList ){
				for each ( var rsiIn:RefillScheduleItem in rs.refillScheduleItemList ) {
					rsiIn.pharmOrderItem = null;
				}
				rs.pharmOrder = null;
			}
			
			for each(var poi:PharmOrderItem in pharmOrderIn.pharmOrderItemList ){
				poi.refillScheduleItemList = new ArrayCollection();
			}
			pharmOrderIn.refillScheduleList = new ArrayCollection();
		}
		
		private function disconnectRefillScheduleList(rsList:ListCollectionView):ListCollectionView {						
			var testRsList:ArrayCollection = new ArrayCollection();
			
			for each ( var rs:RefillSchedule in rsList ){				
				testRsList.addItem( disconnectRefillSchedule(rs) );
			}
			return testRsList;
		}
		
		private function disconnectRefillSchedule(rsIn:RefillSchedule):RefillSchedule{
			var copyRs:RefillSchedule = new RefillSchedule();
			copyRs.copy(rsIn);
			
			var copyRsiList:ArrayCollection = new ArrayCollection();
			for each ( var rsiIn:RefillScheduleItem in rsIn.refillScheduleItemList ) {
				var copyRsi:RefillScheduleItem = new RefillScheduleItem();
				copyRsi.copy(rsiIn);
				copyRsi.refillSchedule = copyRs;
				copyRsiList.addItem(copyRsi);
			}
			copyRs.refillScheduleItemList = copyRsiList;
			return copyRs;
		}
		
		private function updateRequireRefillProp(pharmOrderIn:PharmOrder):PharmOrder{
			for each ( var poi:PharmOrderItem in pharmOrderIn.pharmOrderItemList ) {
				if( startRefillSourceView == 'vetting' ){
					if ( poi.refillScheduleItemList.length > 1) {
						poi.groupNum = poi.refillScheduleItemList.getItemAt(0).refillSchedule.groupNum;
						poi.requireRefill = true;
						poi.neverRefill = false;
					}else if( poi.refillScheduleItemList.length > 0 ) {
						poi.groupNum = 1;
						poi.requireRefill = false;
						poi.neverRefill = false;
					}else{
						poi.requireRefill = false;
						poi.neverRefill = true;
					}
				}else{
					if( poi.refillScheduleItemList.length > 1 ){
						poi.groupNum = poi.refillScheduleItemList.getItemAt(0).refillSchedule.groupNum;
						poi.requireRefill = true;
						poi.neverRefill = false;
					}else{
						poi.requireRefill = false;
						poi.neverRefill = true;
					}
				}
				if( startRefillSourceView != 'vetting' ){
					poi.refillItemNum = poi.itemNum;
				}
				poi.sumCalQty = poi.issueQty;
				poi.sumRefillDuration = poi.regimen.durationInDay;
			}
			
			return pharmOrderIn;
		}
		
		private function updateRefillScheduleEditableProp(pharmOrderIn:PharmOrder):void{
			for each( var refillSchedule:RefillSchedule in pharmOrderIn.refillScheduleList ){
				if( ObjectUtil.compare(refillSchedule.refillDate,refillSchedule.beforeAdjRefillDate) == 0 ){
					refillSchedule.refillDateAdjusted = false;
				}else{
					refillSchedule.refillDateAdjusted = true;
				}

				if( refillSchedule.nextPharmApptDate != null ){
					refillSchedule.pharmApptDateEnable = true;
				}
				
				refillSchedule.dateEditable = false;
				refillSchedule.editable = false;
				
				if( startRefillSourceView == 'vetting' ){
					refillSchedule.editable = true;
					if( refillSchedule.refillNum > 1 ){
						refillSchedule.dateEditable = true;
					}
				}else if( startRefillSourceView == 'onestop' ){
					if( refillSchedule.ticket != null ){
						if( RefillScheduleRefillStatus.Current == refillSchedule.refillStatus && refillSchedule.refillNum > 1 && refillSchedule.isCurrentTicketWorkstore(workstore) ){
							if( RefillScheduleStatus.BeingProcessed == refillSchedule.status || RefillScheduleStatus.Dispensed == refillSchedule.status ){
								if( !refillSchedule.isDayEndProcessing() ){
									refillSchedule.dateEditable = true;
									refillSchedule.editable = true;
								}
							}	
						}
					}else if( RefillScheduleStatus.Abort != refillSchedule.status ){
						refillSchedule.dateEditable = true;
						refillSchedule.editable = true;
					}
				}
			}
		}
		
		private function updateEnableModifyButtonProp(pharmOrderIn:PharmOrder):PharmOrder{
			var allowEditSubSeq:Boolean = true;
			
			for each( var refillSchedule:RefillSchedule in pharmOrderIn.refillScheduleList ){
				if( RefillScheduleRefillStatus.Current == refillSchedule.refillStatus &&
					refillPrescInfo.currentGroupNum == refillSchedule.groupNum &&
					( RefillScheduleStatus.BeingProcessed == refillSchedule.status || DispOrderAdminStatus.Suspended == refillSchedule.dispOrderAdminStatus) &&
					( ObjectUtil.compare(pharmOrderIn.medOrder.ticket.ticketDate, refillSchedule.ticket.ticketDate) != 0 ||
					  ObjectUtil.compare(pharmOrderIn.medOrder.ticket.ticketNum, refillSchedule.ticket.ticketNum) != 0 ||
					  !refillSchedule.isCurrentTicketWorkstore(workstore))
				){
					allowEditSubSeq = false;
					break;
				}
			}
			
			for each( var refillScheduleModify:RefillSchedule in pharmOrderIn.refillScheduleList ){
				if( refillPrescInfo.refillActionType == RefillScheduleActionType.AbortRefill || refillPrescInfo.refillActionType == RefillScheduleActionType.TerminateRefill ){
					refillScheduleModify.enableModifyButton = false;
				}else if( refillScheduleModify.refillNum == 1 ){
					refillScheduleModify.enableModifyButton = false;
				}else if( refillScheduleModify.groupNum != refillPrescInfo.currentGroupNum ){
					refillScheduleModify.enableModifyButton = false;
				}else if( refillScheduleModify.status == RefillScheduleStatus.Abort || refillScheduleModify.status == RefillScheduleStatus.ForceComplete ){
					refillScheduleModify.enableModifyButton = false;
				}else if( refillScheduleModify.status == RefillScheduleStatus.Dispensed && refillScheduleModify.isDayEndProcessing() ){
					refillScheduleModify.enableModifyButton = false;
				}else if( refillScheduleModify.refillStatus == RefillScheduleRefillStatus.Current && 
					!refillScheduleModify.isCurrentTicketWorkstore(workstore) ){
					refillScheduleModify.enableModifyButton = false;
				}else if( refillScheduleModify.allItemDiscontinueFlag ){
					refillScheduleModify.enableModifyButton = false;
				}else if( refillScheduleModify.ticket == null && !allowEditSubSeq ){
					refillScheduleModify.enableModifyButton = false;
				}
			}
			return pharmOrderIn;
		}
		
		private function updateRefillScheduleAllowEditProp(pharmOrderIn:PharmOrder):PharmOrder{
			updateRequireRefillProp(pharmOrderIn);
			
			updateRefillScheduleEditableProp(pharmOrderIn);
			
			if( startRefillSourceView == 'onestop' ){
				updateEnableModifyButtonProp(pharmOrderIn);
				pharmOrderIn.pharmOrderItemList.filterFunction = pharmOrderItemFilterFunc;
				pharmOrderIn.pharmOrderItemList.refresh();
			}
			return pharmOrderIn;
		}
		
		private function pharmOrderItemFilterFunc(item:PharmOrderItem):Boolean {
			if( item.requireRefill ){
				return true;
			}
			return false;
		}
		
		
		[Observer]
		public function showSaveRefillExceptionMsg(evt:SaveRefillExceptionMsgEvent):void{
			var sysMsgProp:SystemMessagePopupProp = evt.sysMsgProp;
			sysMsgProp.okHandler = function(mouseEvent:MouseEvent):void {
				showOneStopEntry();
				PopUpManager.removePopUp(((mouseEvent.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
			};
			dispatchEvent(new RetrieveSystemMessageEvent(sysMsgProp));
		}
	}
}