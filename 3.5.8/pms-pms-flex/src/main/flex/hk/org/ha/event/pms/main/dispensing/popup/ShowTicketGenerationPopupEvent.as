package hk.org.ha.event.pms.main.dispensing.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowTicketGenerationPopupEvent extends AbstractTideEvent 
	{		
		public function ShowTicketGenerationPopupEvent():void 
		{
			super();
		}        
	}
}