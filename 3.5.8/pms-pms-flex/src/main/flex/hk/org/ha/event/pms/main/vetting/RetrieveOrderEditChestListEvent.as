package hk.org.ha.event.pms.main.vetting {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrieveOrderEditChestListEvent extends AbstractTideEvent 
	{		
		
		private var _callback:Function;
		
		public function RetrieveOrderEditChestListEvent(callback:Function) {
			super();
			_callback = callback;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}
