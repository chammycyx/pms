package hk.org.ha.event.pms.main.reftable.mp {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CheckDueOrderScheduleTemplateMaintDeliveryScheduleNameExistEvent extends AbstractTideEvent 
	{	
		private var _deliveryScheduleName:String;
		private var _callback:Function;
		
		public function CheckDueOrderScheduleTemplateMaintDeliveryScheduleNameExistEvent(deliveryScheduleName:String, callback:Function):void 
		{
			super();
			_deliveryScheduleName = deliveryScheduleName;
			_callback = callback;
		}
		
		public function get deliveryScheduleName():String {
			return _deliveryScheduleName;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}

