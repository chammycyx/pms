package hk.org.ha.event.pms.main.charging.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	import mx.collections.ArrayCollection;
	
	public class ShowMpSfiInvoiceInvoiceListPopupEvent extends AbstractTideEvent 
	{
		private var _mpSfiInvoiceInvoiceListInfoList:ArrayCollection;
		public function ShowMpSfiInvoiceInvoiceListPopupEvent(mpSfiInvoiceInvoiceListInfoList:ArrayCollection):void 
		{
			super();		
			_mpSfiInvoiceInvoiceListInfoList = mpSfiInvoiceInvoiceListInfoList;
		}
		
		public function get mpSfiInvoiceInvoiceListInfoList():ArrayCollection {
			return _mpSfiInvoiceInvoiceListInfoList;
		}
	}
}