package hk.org.ha.event.pms.main.checkissue.popup {
	
	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowTicketMatchMessagePopupEvent extends AbstractTideEvent 
	{
		private var _systemMessagePopupProp:SystemMessagePopupProp
		
		public function ShowTicketMatchMessagePopupEvent(systemMessagePopupProp:SystemMessagePopupProp):void 
		{
			super();
			_systemMessagePopupProp = systemMessagePopupProp;
		}
		
		public function get systemMessagePopupProp():SystemMessagePopupProp {
			return _systemMessagePopupProp;
		}	
	}
}