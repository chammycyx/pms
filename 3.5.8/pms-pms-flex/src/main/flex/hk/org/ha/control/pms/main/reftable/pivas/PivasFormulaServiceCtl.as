package hk.org.ha.control.pms.main.reftable.pivas {
	
	import flash.events.MouseEvent;
	
	import mx.collections.ListCollectionView;
	import mx.core.IFlexDisplayObject;
	import mx.core.UIComponent;
	import mx.managers.PopUpManager;
	
	import hk.org.ha.event.pms.main.drug.RetrieveDmDrugLiteListEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveWorkstoreDrugListEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.CheckAnotherActivePivasMergeFormulaExistsEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.CheckPivasItemSuspendEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.DeletePivasFormulaEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.ExportPivasFormulaEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.RetrieveDmDrugLiteMapEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.RetrieveDrugInfoListByDisplayNameEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.RetrievePivasDrugListInfoByDrugKeyStrengthDiluentCodeEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.RetrievePivasFormulaInfoEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.RetrievePivasFormulaListEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.UpdatePivasFormulaEvent;
	import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	import hk.org.ha.model.pms.biz.reftable.pivas.PivasFormulaServiceBean;
	import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormula;
	import hk.org.ha.model.pms.vo.reftable.pivas.PivasDrugListInfo;
	import hk.org.ha.model.pms.vo.reftable.pivas.PivasFormulaInfo;
	
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("pivasFormulaServiceCtl", restrict="true")]
	public class PivasFormulaServiceCtl {	

		[In]
		public var pivasFormulaService:PivasFormulaServiceBean;

		[Observer]
		public function retrievePivasFormulaList(retrievePivasFormulaListEvent:RetrievePivasFormulaListEvent):void {
			pivasFormulaService.retrievePivasFormulaList(
				retrievePivasFormulaListEvent.drugKey,
				retrievePivasFormulaListEvent.siteCode,
				retrievePivasFormulaListEvent.diluentCode,
				retrievePivasFormulaListEvent.pivasDosageUnit,
				retrievePivasFormulaListEvent.concn,
				retrievePivasFormulaListEvent.strength,
				retrievePivasFormulaListEvent.rate,
				function(evt:TideResultEvent):void {
					retrievePivasFormulaListEvent.callback(evt.result as ListCollectionView);
				}
			);
		}
		
		[Observer]
		public function updatePivasFormula(updatePivasFormulaEvent:UpdatePivasFormulaEvent):void {
			In(Object(pivasFormulaService).errorCode)
			pivasFormulaService.updatePivasFormula(
				updatePivasFormulaEvent.pivasFormula,
				function(evt:TideResultEvent):void {
					var errorCode:String = Object(pivasFormulaService).errorCode;
					if (errorCode == null) {
						updatePivasFormulaEvent.successCallback(evt.result as PivasFormula);
					}
					else {
						var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp(errorCode);
						msgProp.okHandler = function(evt:MouseEvent):void {
							updatePivasFormulaEvent.faultCallback();
							PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
						};
						evt.context.dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
					}
				},
				function(evt:TideFaultEvent):void {
					updatePivasFormulaEvent.faultCallback();
				}
			);
		}
		
		[Observer]
		public function retrieveDmDrugLiteMap(evt:RetrieveDmDrugLiteMapEvent):void {
			pivasFormulaService.retrieveDmDrugLiteMap(evt.itemCodeList,
				function(resultEvt:TideResultEvent):void {
					evt.successCallback(resultEvt.result);
				}
			);
		}
		
		[Observer]
		public function checkAnotherActivePivasMergeFormulaExists(evt:CheckAnotherActivePivasMergeFormulaExistsEvent):void {
			pivasFormulaService.checkAnotherActivePivasMergeFormulaExists(evt.pivasFormula,
				function(resultEvt:TideResultEvent):void {
					evt.successCallback(resultEvt.result);
				}
			);
		}
		
		[Observer]
		public function deletePivasFormula(deletePivasFormula:DeletePivasFormulaEvent):void {
			pivasFormulaService.deletePivasFormula(
				deletePivasFormula.pivasFormula,
				function(evt:TideResultEvent):void {
					deletePivasFormula.successCallback();
				},
				function(evt:TideFaultEvent):void {
					deletePivasFormula.faultCallback();
				}
			);
		}
		
		[Observer]
		public function retrieveDrugInfoListByDisplayName(retrieveDrugInfoListByDisplayNameEvent:RetrieveDrugInfoListByDisplayNameEvent):void {
			pivasFormulaService.retrieveDrugInfoListByDisplayName(
				retrieveDrugInfoListByDisplayNameEvent.displayName,
				function(evt:TideResultEvent):void {
					retrieveDrugInfoListByDisplayNameEvent.callback(evt.result as ListCollectionView);
				}
			);
		}

		[Observer]
		public function retrievePivasFormulaInfo(retrievePivasFormulaInfoEvent:RetrievePivasFormulaInfoEvent):void {
			pivasFormulaService.retrievePivasFormulaInfo(
				retrievePivasFormulaInfoEvent.drugKey, retrievePivasFormulaInfoEvent.includeNonPivasSite,
				function(evt:TideResultEvent):void {
					retrievePivasFormulaInfoEvent.callback(
						evt.result as PivasFormulaInfo,
						retrievePivasFormulaInfoEvent.pivasFormula
					);
				}
			);
		}
		
		[Observer]
		public function retrievePivasDrugListInfoByDrugKeyStrengthDiluentCode(retrievePivasDrugListInfoByDrugKeyStrengthDiluentCodeEvent:RetrievePivasDrugListInfoByDrugKeyStrengthDiluentCodeEvent):void {
			pivasFormulaService.retrievePivasDrugListInfoByDrugKeyStrengthDiluentCode(
				retrievePivasDrugListInfoByDrugKeyStrengthDiluentCodeEvent.drugKey,
				retrievePivasDrugListInfoByDrugKeyStrengthDiluentCodeEvent.strength,
				retrievePivasDrugListInfoByDrugKeyStrengthDiluentCodeEvent.diluentCode,
				retrievePivasDrugListInfoByDrugKeyStrengthDiluentCodeEvent.siteCode,
				function(evt:TideResultEvent):void {
					retrievePivasDrugListInfoByDrugKeyStrengthDiluentCodeEvent.callback(evt.result as PivasDrugListInfo);
				}
			);
		}
		
		[Observer]
		public function checkPivasItemSuspend(checkPivasItemSuspendEvent:CheckPivasItemSuspendEvent):void {
			pivasFormulaService.checkPivasItemSuspend(
				checkPivasItemSuspendEvent.itemCode,
				function(evt:TideResultEvent):void {
					checkPivasItemSuspendEvent.callback(evt.result as Array);
				}
			);
		}
		
		[Observer]
		public function exportPivasFormula(exportPivasFormulaEvent:ExportPivasFormulaEvent):void {
			pivasFormulaService.retrievePivasFormulaForExport(function(evt:TideResultEvent):void {
				exportPivasFormulaEvent.callback();
			});
		}
	}
}
