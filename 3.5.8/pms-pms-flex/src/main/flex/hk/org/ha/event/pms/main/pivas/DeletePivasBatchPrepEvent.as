package hk.org.ha.event.pms.main.pivas
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class DeletePivasBatchPrepEvent extends AbstractTideEvent 
	{		
		private var _lotNo:String;
		private var _manufFromDate:Date;
		private var _manufToDate:Date;
		private var _drugKey:Number;
		
		public function DeletePivasBatchPrepEvent(lotNo:String, manufFromDate:Date, manufToDate:Date, drugKey:Number):void 
		{
			super();
			_lotNo = lotNo;
			_manufFromDate = manufFromDate;
			_manufToDate = manufToDate;
			_drugKey = drugKey;
		}
		
		public function get lotNo():String {
			return _lotNo;
		}
		
		public function get manufFromDate():Date {
			return _manufFromDate;
		}
		
		public function get manufToDate():Date {
			return _manufToDate;
		}
		
		public function get drugKey():Number {
			return _drugKey;
		}
	}
}