package hk.org.ha.event.pms.main.vetting {
	
	import flash.events.Event;
	
	public class ChangeLangEvent extends Event {
		
		public function ChangeLangEvent() {
			super("changeLang", true);
		}
	}
}
