package hk.org.ha.event.pms.main.pivas
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class DeleteFromWorklistEvent extends AbstractTideEvent 
	{		
		private var _pivasWorklistList:ArrayCollection;
		
		private var _callbackFunc:Function;
		
		public function DeleteFromWorklistEvent(pivasWorklistList:ArrayCollection, callbackFunc:Function):void 
		{
			super();
			_pivasWorklistList = pivasWorklistList;
			_callbackFunc = callbackFunc;
		}

		public function get pivasWorklistList():ArrayCollection {
			return _pivasWorklistList;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}