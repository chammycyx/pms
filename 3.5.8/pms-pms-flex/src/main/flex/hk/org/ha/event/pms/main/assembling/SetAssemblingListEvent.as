package hk.org.ha.event.pms.main.assembling
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetAssemblingListEvent extends AbstractTideEvent 
	{		
		private var _assemblingList:ArrayCollection;
		
		public function SetAssemblingListEvent(assemblingList:ArrayCollection):void 
		{
			super();
			_assemblingList = assemblingList;
		}
		
		public function get assemblingList():ArrayCollection {
			return _assemblingList;
		}
	}
}