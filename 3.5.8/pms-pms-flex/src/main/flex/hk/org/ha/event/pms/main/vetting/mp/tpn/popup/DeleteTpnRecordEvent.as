package hk.org.ha.event.pms.main.vetting.mp.tpn.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class DeleteTpnRecordEvent extends AbstractTideEvent
	{
		private var _selectedIndex:Number;
		
		private var _deleteReason:String;
		
		private var _callback:Function;
		
		public function DeleteTpnRecordEvent(selectedIndex:Number, deleteReason:String, callback:Function):void {
			super();
			_selectedIndex = selectedIndex;
			_deleteReason = deleteReason;
			_callback = callback;
		}
		
		public function get selectedIndex():Number{
			return _selectedIndex;
		}
		
		public function get deleteReason():String{
			return _deleteReason;
		}
		
		public function get callback():Function{
			return _callback;
		}
	}
}