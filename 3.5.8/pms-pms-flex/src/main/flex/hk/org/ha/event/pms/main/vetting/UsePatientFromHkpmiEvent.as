package hk.org.ha.event.pms.main.vetting {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.disp.Patient;
	
	public class UsePatientFromHkpmiEvent extends AbstractTideEvent {
		
		private var _patient:Patient;
		
		private var _callback:Function;
		
		public function UsePatientFromHkpmiEvent(patient:Patient, callback:Function) {
			super();
			_patient = patient;
			_callback = callback;
		}
		
		public function get patient():Patient {
			return _patient;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}
