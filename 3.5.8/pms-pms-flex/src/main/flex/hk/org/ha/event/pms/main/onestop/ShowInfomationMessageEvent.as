package hk.org.ha.event.pms.main.onestop {
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowInfomationMessageEvent extends AbstractTideEvent 
	{	
		private var _messages:Array;
		private var _requriedReleaseScreenLock:Boolean;
		
		public function ShowInfomationMessageEvent(messages:Array, requriedReleaseScreenLock:Boolean):void 
		{
			super();
			_messages = messages;
			_requriedReleaseScreenLock = requriedReleaseScreenLock;
		}
		
		public function get messages():Array 
		{
			return _messages;
		}
		
		public function get requriedReleaseScreenLock():Boolean
		{
			return _requriedReleaseScreenLock;
		}
	}
}