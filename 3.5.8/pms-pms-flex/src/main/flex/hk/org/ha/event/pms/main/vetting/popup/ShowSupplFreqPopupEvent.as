package hk.org.ha.event.pms.main.vetting.popup {
	
	import hk.org.ha.model.pms.dms.persistence.DmSupplFrequency;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowSupplFreqPopupEvent extends AbstractTideEvent 
	{		
		private var _prevParam:Array;
		
		private var _dmSupplFrequency:DmSupplFrequency;				
		
		private var _okHandler:Function;
		
		private var _cancelHandler:Function;
		
		public function ShowSupplFreqPopupEvent(param:Array, dmSupplFrequency:DmSupplFrequency, okHandlerVal:Function, cancelHandlerVal:Function):void 
		{
			super();
			_prevParam 		  = param;
			_dmSupplFrequency = dmSupplFrequency;			
			_okHandler 		  = okHandlerVal;
			_cancelHandler 	  = cancelHandlerVal;
		}
		
		public function get prevParam():Array
		{
			return _prevParam;
		}
		
		public function get dmSupplFrequency():DmSupplFrequency 
		{
			return _dmSupplFrequency;
		}		
		
		public function get okHandler():Function 
		{
			return _okHandler;
		}
		
		public function get cancelHandler():Function 
		{
			return _cancelHandler;
		}
	}
}
