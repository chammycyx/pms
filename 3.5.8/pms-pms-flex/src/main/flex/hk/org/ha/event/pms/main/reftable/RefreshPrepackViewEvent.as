package hk.org.ha.event.pms.main.reftable {
	
	import hk.org.ha.view.pms.main.reftable.popup.ItemLocationMaintPrepackPopup;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshPrepackViewEvent extends AbstractTideEvent 
	{
		
		private var _popup:ItemLocationMaintPrepackPopup;
		
		public function get popup():ItemLocationMaintPrepackPopup
		{
			return _popup;
		}

		public function RefreshPrepackViewEvent(popup:ItemLocationMaintPrepackPopup):void 
		{
			_popup = popup;
			super();
		}
	}
}