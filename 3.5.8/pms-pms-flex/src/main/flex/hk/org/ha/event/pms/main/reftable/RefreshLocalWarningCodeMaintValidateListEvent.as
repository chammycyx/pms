package hk.org.ha.event.pms.main.reftable {
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshLocalWarningCodeMaintValidateListEvent extends AbstractTideEvent 
	{
		private var _dmWarningValidateList:ArrayCollection;
		
		public function RefreshLocalWarningCodeMaintValidateListEvent(list:ArrayCollection):void 
		{
			_dmWarningValidateList = list;
			super();
		}

		public function set dmWarningValidateList(list:ArrayCollection):void
		{
			_dmWarningValidateList = list;
		}
		
		public function get dmWarningValidateList():ArrayCollection
		{
			return _dmWarningValidateList;
		}
	}
}