package hk.org.ha.event.pms.main.alert.mds.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowMdsLogPopupEvent extends AbstractTideEvent {
		
		private var _callback:Function;
		
		public function ShowMdsLogPopupEvent(callback:Function=null) {
			super();
			_callback = callback;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}