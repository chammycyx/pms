package hk.org.ha.event.pms.main.reftable.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowPivasSystemMaintContainerPopupEvent extends AbstractTideEvent 
	{
		private var _pivasSystemMaintContainerPopupProp:PivasSystemMaintContainerPopupProp;
		
		public function ShowPivasSystemMaintContainerPopupEvent(pivasSystemMaintContainerPopupProp:PivasSystemMaintContainerPopupProp):void 
		{
			super();
			_pivasSystemMaintContainerPopupProp = pivasSystemMaintContainerPopupProp;
		}
		
		public function get pivasSystemMaintContainerPopupProp():PivasSystemMaintContainerPopupProp{
			return _pivasSystemMaintContainerPopupProp;
		}
	}
}