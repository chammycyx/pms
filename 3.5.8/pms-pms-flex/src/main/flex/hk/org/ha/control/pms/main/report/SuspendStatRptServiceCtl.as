package hk.org.ha.control.pms.main.report {
	import hk.org.ha.event.pms.main.report.PrintSuspendStatRptEvent;
	import hk.org.ha.event.pms.main.report.RefreshSuspendStatRptEvent;
	import hk.org.ha.event.pms.main.report.RetrieveSuspendStatRptEvent;
	import hk.org.ha.model.pms.biz.report.SuspendStatRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	
	[Bindable]
	[Name("suspendStatRptServiceCtl", restrict="true")]
	public class SuspendStatRptServiceCtl 
	{
		[In]
		public var suspendStatRptService:SuspendStatRptServiceBean;
		
		[Observer]
		public function retrieveSuspendStatRpt(evt:RetrieveSuspendStatRptEvent):void
		{
			In(Object(suspendStatRptService).retrieveSuccess)
			suspendStatRptService.retrieveSuspendStatList(evt.pastDay, evt.dateFrom, evt.dateTo, retrieveSuspendStatRptResult);
		}
		
		private function retrieveSuspendStatRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshSuspendStatRptEvent(Object(suspendStatRptService).retrieveSuccess));
		}
		
		[Observer]
		public function printSuspendStatRpt(evt:PrintSuspendStatRptEvent):void
		{
			suspendStatRptService.printSuspendStatRpt();
		}
		
	}
}
