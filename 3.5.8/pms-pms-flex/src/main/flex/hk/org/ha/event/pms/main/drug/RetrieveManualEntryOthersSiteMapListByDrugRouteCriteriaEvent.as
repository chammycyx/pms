package hk.org.ha.event.pms.main.drug {		
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dms.vo.DrugRouteCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveManualEntryOthersSiteMapListByDrugRouteCriteriaEvent extends AbstractTideEvent 
	{
		private var _drugRouteCriteria:DrugRouteCriteria;
		private var _callBackEvent:Event;
		
		public function RetrieveManualEntryOthersSiteMapListByDrugRouteCriteriaEvent(criteriaVal:DrugRouteCriteria, eventVal:Event=null):void 
		{
			super();
			_drugRouteCriteria = criteriaVal;
			_callBackEvent = eventVal;
		}
		
		public function get drugRouteCriteria():DrugRouteCriteria
		{
			return _drugRouteCriteria;
		}
		
		public function get callBackEvent():Event
		{
			return _callBackEvent;
		}
	}
}