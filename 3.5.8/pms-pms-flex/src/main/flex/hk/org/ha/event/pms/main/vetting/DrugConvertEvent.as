package hk.org.ha.event.pms.main.vetting {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;	
	
	public class DrugConvertEvent extends AbstractTideEvent {

		private var _selectedIndex:int;
		
		private var _medOrderItem:MedOrderItem;	
		
		private var _isCapd:Boolean;
		
		private var _cancelHandler:Function;
		public function DrugConvertEvent(selectedIndex:int, medOrderItemValue:MedOrderItem, isCapd:Boolean=false, cancelHandler:Function = null) {
			super();			
			_medOrderItem = medOrderItemValue;
			_selectedIndex = selectedIndex;
			_isCapd = isCapd;
			_cancelHandler = cancelHandler;
		}
		
		public function get selectedIndex():int {
			return _selectedIndex;
		}
		
		public function get medOrderItem():MedOrderItem {
			return _medOrderItem;
		}	
		
		public function get isCapd():Boolean{
			return _isCapd;
		}
		
		public function get cancelHandler():Function{
			return _cancelHandler;
		}
	}
}