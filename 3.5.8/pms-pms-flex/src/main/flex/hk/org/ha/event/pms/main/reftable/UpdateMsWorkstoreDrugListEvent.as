package hk.org.ha.event.pms.main.reftable {
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateMsWorkstoreDrugListEvent extends AbstractTideEvent 
	{
		private var _updateMsWorkstoreDrugList:ListCollectionView;
		private var _itemCode:String;
		private var _editDrugScope:Boolean;
		private var _remark:String;
		
		public function UpdateMsWorkstoreDrugListEvent(updateMsWorkstoreDrugList:ListCollectionView, itemCode:String, remark:String, editDrugScope:Boolean):void 
		{
			super();
			_updateMsWorkstoreDrugList = updateMsWorkstoreDrugList;
			_itemCode = itemCode;
			_editDrugScope = editDrugScope;
			_remark = remark;
		}
		
		public function get updateMsWorkstoreDrugList():ListCollectionView
		{
			return _updateMsWorkstoreDrugList;
		}
		
		public function get itemCode():String
		{
			return _itemCode;
		}
		
		public function get editDrugScope():Boolean
		{
			return _editDrugScope;
		}
		
		public function get remark():String
		{
			return _remark;
		}
	}
}