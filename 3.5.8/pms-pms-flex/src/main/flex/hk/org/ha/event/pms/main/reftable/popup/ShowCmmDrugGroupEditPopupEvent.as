package hk.org.ha.event.pms.main.reftable.popup {
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowCmmDrugGroupEditPopupEvent extends AbstractTideEvent {
		
		private var _cmmDrugGroupList:ListCollectionView;
		private var _groupId:Number;
		private var _callback:Function;
		
		public function ShowCmmDrugGroupEditPopupEvent(cmmDrugGroupList:ListCollectionView, groupId:Number, callback:Function) {
			_cmmDrugGroupList = cmmDrugGroupList;
			_groupId = groupId;
			_callback = callback;
		}
		
		public function get cmmDrugGroupList():ListCollectionView {
			return _cmmDrugGroupList;
		}
		
		public function get groupId():Number {
			return _groupId;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}
