package hk.org.ha.event.pms.main.delivery {
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshLabelReprintWardSelectionListEvent extends AbstractTideEvent 
	{
		private var _wardList:ArrayCollection;
		
		public function RefreshLabelReprintWardSelectionListEvent(list:ArrayCollection):void 
		{
			_wardList = list;
			super();
		}
		
		public function get wardList():ArrayCollection
		{
			return _wardList;
		}
	}
}