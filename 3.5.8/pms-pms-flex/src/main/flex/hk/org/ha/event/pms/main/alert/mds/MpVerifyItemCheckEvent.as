package hk.org.ha.event.pms.main.alert.mds {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
	
	public class MpVerifyItemCheckEvent extends AbstractTideEvent {
		
		private var _medProfileItem:MedProfileItem;
		
		private var _medProfile:MedProfile;
		
		private var _successFunc:Function;
		
		private var _failureFunc:Function;
		
		private var _closeFunc:Function;
		
		public function MpVerifyItemCheckEvent(medProfileItem:MedProfileItem, medProfile:MedProfile, successFunc:Function, failureFunc:Function, closeFunc:Function) {
			super();
			_medProfileItem = medProfileItem;
			_medProfile = medProfile;
			_successFunc = successFunc;
			_failureFunc = failureFunc;
			_closeFunc = closeFunc;
		}
		
		public function get medProfileItem():MedProfileItem {
			return _medProfileItem;
		}
		
		public function get medProfile():MedProfile {
			return _medProfile;
		}
		
		public function get successFunc():Function {
			return _successFunc;
		}
		
		public function get failureFunc():Function {
			return _failureFunc;
		}
		
		public function get closeFunc():Function {
			return _closeFunc;
		}
	}
}
