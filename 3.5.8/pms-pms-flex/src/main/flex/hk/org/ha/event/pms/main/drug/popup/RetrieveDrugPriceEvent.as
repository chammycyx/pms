package hk.org.ha.event.pms.main.drug.popup {
	
	import hk.org.ha.model.pms.dms.vo.DrugName;
	import hk.org.ha.model.pms.dms.vo.PreparationSearchCriteria;
	import hk.org.ha.model.pms.dms.vo.RelatedDrugSearchCriteria;
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDrugPriceEvent extends AbstractTideEvent 
	{
		private var _drugSearchSource:DrugSearchSource;
		private var _drugName:DrugName;										// no need to set if search by related drug
		private var _preparationSearchCriteria:PreparationSearchCriteria;	// no need to set if search by related drug
		private var _relatedDrugSearchCriteria:RelatedDrugSearchCriteria;	// no need to set if search by single drug name
		private var _popupCancelHandler:Function = null;

		public function RetrieveDrugPriceEvent():void 
		{
			super();
		}

		public function get drugSearchSource():DrugSearchSource
		{
			return _drugSearchSource;
		}

		public function set drugSearchSource(drugSearchSource:DrugSearchSource):void
		{
			_drugSearchSource = drugSearchSource;
		}
		
		public function get drugName():DrugName
		{
			return _drugName;
		}
		
		public function set drugName(drugName:DrugName):void
		{
			_drugName = drugName;
		}
		
		public function get preparationSearchCriteria():PreparationSearchCriteria
		{
			return _preparationSearchCriteria;
		}
		
		public function set preparationSearchCriteria(preparationSearchCriteria:PreparationSearchCriteria):void
		{
			_preparationSearchCriteria = preparationSearchCriteria;
		}
		
		public function get relatedDrugSearchCriteria():RelatedDrugSearchCriteria
		{
			return _relatedDrugSearchCriteria;
		}
		
		public function set relatedDrugSearchCriteria(relatedDrugSearchCriteria:RelatedDrugSearchCriteria):void
		{
			_relatedDrugSearchCriteria = relatedDrugSearchCriteria;
		}
		
		public function get popupCancelHandler():Function
		{
			return _popupCancelHandler;
		}
		
		public function set popupCancelHandler(popupCancelHandler:Function):void
		{
			_popupCancelHandler = popupCancelHandler;
		}
	}
}