package hk.org.ha.event.pms.main.checkissue
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetTicketMatchResultEvent extends AbstractTideEvent 
	{		
		private var _callbackFunc:Function;
		
		public function SetTicketMatchResultEvent(callbackFunc:Function):void 
		{
			_callbackFunc = callbackFunc;
		}
		
		public function get callbackFunc():Function
		{
			return _callbackFunc;
		}

	}
}