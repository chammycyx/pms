package hk.org.ha.event.pms.main.onestop
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class DefaultSpecialtyMedOfficerMappingEvent extends AbstractTideEvent
	{
		private var _specCode:String;
		private var _proceed:Boolean;
		
		public function DefaultSpecialtyMedOfficerMappingEvent(specCode:String, proceed:Boolean)
		{
			super();
			_specCode = specCode;
			_proceed = proceed;
		}
		
		public function get specCode():String {
			return _specCode;
		}
		
		public function get proceed():Boolean {
			return _proceed;
		}
	}
}