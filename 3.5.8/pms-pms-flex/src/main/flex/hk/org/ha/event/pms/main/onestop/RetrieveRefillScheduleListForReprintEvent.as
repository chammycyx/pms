package hk.org.ha.event.pms.main.onestop {
		
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveRefillScheduleListForReprintEvent extends AbstractTideEvent 
	{	
		private var _refillScheduleId:Number;
		
		public function RetrieveRefillScheduleListForReprintEvent(refillScheduleId:Number):void 
		{
			super();
			this._refillScheduleId = refillScheduleId;
		}
		
		public function get refillScheduleId():Number {
			return _refillScheduleId;
		}
	}
}