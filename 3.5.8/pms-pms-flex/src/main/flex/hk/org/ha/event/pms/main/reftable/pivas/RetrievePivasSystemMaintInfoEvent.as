package hk.org.ha.event.pms.main.reftable.pivas {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrievePivasSystemMaintInfoEvent extends AbstractTideEvent {
		
		private var _year:String;
		
		public function RetrievePivasSystemMaintInfoEvent(year:String) {
			super();
			_year = year;
		}
		
		public function get year():String {
			return _year;
		}
	}
}