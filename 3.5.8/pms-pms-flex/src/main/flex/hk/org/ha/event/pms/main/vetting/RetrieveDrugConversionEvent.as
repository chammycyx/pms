package hk.org.ha.event.pms.main.vetting {
	
	import flash.events.Event;
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.rx.RxItem;
	
	public class RetrieveDrugConversionEvent extends AbstractTideEvent 
	{		
		private var _rxItem:RxItem;
		private var _callBackEvent:Event;
		
		public function RetrieveDrugConversionEvent(rxItemValue:RxItem, eventValue:Event) 
		{
			super();	
			_rxItem = rxItemValue;
			_callBackEvent = eventValue;
		}		
		
		public function get rxItem():RxItem 
		{
			return _rxItem;
		}
		
		public function get callBackEvent():Event			
		{
			return _callBackEvent;
		}
	}
}
