package hk.org.ha.event.pms.exception.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowExceptionPopupEvent extends AbstractTideEvent 
	{
		private var _faultCode:String;
		private var _faultString:String;
		private var _faultDetail:String;
		
		public function ShowExceptionPopupEvent(faultCode:String, faultString:String, faultDetail:String):void 
		{
			super();
			_faultCode = faultCode;
			_faultString = faultString;
			_faultDetail = faultDetail;	
		}
		
		public function get faultCode():String {
			return _faultCode;
		}
		
		public function get faultString():String {
			return _faultString;
		}
		
		public function get faultDetail():String {
			return _faultDetail;
		}
	}
}