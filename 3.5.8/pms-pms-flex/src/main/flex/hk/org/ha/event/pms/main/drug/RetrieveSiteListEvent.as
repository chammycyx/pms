package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSiteListEvent extends AbstractTideEvent 
	{
		private var _formCode:String;
		private var _callBackFunc:Function;
		
		public function RetrieveSiteListEvent(formCodeValue:String, funcValue:Function):void 
		{
			super();
			_formCode = formCodeValue;
			_callBackFunc = funcValue;
		}
		
		public function get formCode():String
		{
			return _formCode;
		}
		
		public function get callBackFunc():Function
		{
			return _callBackFunc;
		}
	}
}