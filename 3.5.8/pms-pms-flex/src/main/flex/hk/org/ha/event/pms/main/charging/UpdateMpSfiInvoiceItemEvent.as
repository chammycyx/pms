package hk.org.ha.event.pms.main.charging {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.charging.MpSfiInvoiceItem;
	
	public class UpdateMpSfiInvoiceItemEvent extends AbstractTideEvent 
	{	
		private var _mpSfiInvoiceItem:MpSfiInvoiceItem;
		private var _selectedItem:MpSfiInvoiceItem;
		
		public function UpdateMpSfiInvoiceItemEvent(mpSfiInvoiceItem:MpSfiInvoiceItem, selectedItem:MpSfiInvoiceItem):void 
		{
			super();
			_mpSfiInvoiceItem = mpSfiInvoiceItem;
			_selectedItem = selectedItem;
		}
		
		public function get mpSfiInvoiceItem():MpSfiInvoiceItem {
			return _mpSfiInvoiceItem;
		}
		
		public function get selectedItem():MpSfiInvoiceItem {
			return _selectedItem;
		}
	}
}