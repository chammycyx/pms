package hk.org.ha.event.pms.main.prevet
{
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.udt.prevet.PreVetMode;
	
	public class PreVetCallbackScreenSwitchEvent extends AbstractTideEvent
	{
		private var _preVetMode:PreVetMode;
		private var _switchScreen:Boolean;		
		
		public function PreVetCallbackScreenSwitchEvent(preVetMode:PreVetMode, switchScreen:Boolean=true)
		{
			super();
			_preVetMode = preVetMode;
			_switchScreen = switchScreen;
		}
		
		public function get preVetMode():PreVetMode
		{
			return _preVetMode;
		}
		
		public function get switchScreen():Boolean
		{
			return _switchScreen;
		}
	}
}