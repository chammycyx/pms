package hk.org.ha.event.pms.main.checkissue.mp
{
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.checkissue.mp.CheckDelivery;
	
	public class DisplayProblemItemEvent extends AbstractTideEvent 
	{			
		private var _resultMsg:String;
		private var _checkDelivery : CheckDelivery;
		
		public function DisplayProblemItemEvent(resultMsg:String, checkDelivery : CheckDelivery ):void 
		{
			super();
			_resultMsg = resultMsg;
			_checkDelivery= checkDelivery;
		}
		
		public function get resultMsg():String {
			return _resultMsg;
		}
		
		public function get checkDelivery():CheckDelivery {
			return _checkDelivery;
		}
	}
}