package hk.org.ha.control.pms.main.reftable {
	
	import hk.org.ha.event.pms.main.reftable.RetrieveChestListEvent;
	import hk.org.ha.fmk.pms.flex.components.lookup.RefreshLookupPopupDataProviderEvent;
	import hk.org.ha.model.pms.biz.reftable.ChestListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("chestListServiceCtl", restrict="true")]
	public class ChestListServiceCtl 
	{
		[In]
		public var chestListService:ChestListServiceBean;
		
		[In]
		public var chestList:ArrayCollection;
		
		[Observer]
		public function retrieveChestList(evt:RetrieveChestListEvent):void
		{
			chestListService.retrieveChestList(evt.prefixUnitDose, retrieveChestListResult);
		}
		
		private function retrieveChestListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshLookupPopupDataProviderEvent(chestList));
		}
		
	}
}
