package hk.org.ha.event.pms.main.reftable.mp {
	
	import hk.org.ha.model.pms.persistence.reftable.WardConfig;
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateWardConfigEvent extends AbstractTideEvent 
	{
		private var _wardConfig:WardConfig;
		
		public function get wardConfig():WardConfig
		{
			return _wardConfig;
		}

		public function UpdateWardConfigEvent(wardConfig:WardConfig):void 
		{
			super();
			_wardConfig = wardConfig;
		}

	}
}