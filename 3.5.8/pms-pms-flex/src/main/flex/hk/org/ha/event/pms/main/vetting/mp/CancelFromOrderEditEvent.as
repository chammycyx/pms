package hk.org.ha.event.pms.main.vetting.mp
{
	import flash.events.Event;
	
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CancelFromOrderEditEvent extends AbstractTideEvent
	{
		private var _medProfileItem:MedProfileItem;
		private var _callBackEvent:Event;
		
		public function CancelFromOrderEditEvent(medProfileItemValue:MedProfileItem, callBackEventVal:Event=null)
		{
			super();
			_medProfileItem = medProfileItemValue;
			_callBackEvent  = callBackEventVal;
		}
		
		public function get medProfileItem():MedProfileItem
		{
			return _medProfileItem;
		}
		
		public function get callBackEvent():Event
		{
			return _callBackEvent;
		}					
	}
}