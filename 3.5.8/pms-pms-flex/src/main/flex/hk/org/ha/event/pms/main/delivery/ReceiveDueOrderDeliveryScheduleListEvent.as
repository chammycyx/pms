package hk.org.ha.event.pms.main.delivery
{
	import hk.org.ha.model.pms.vo.delivery.ScheduleTemplateInfo;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ReceiveDueOrderDeliveryScheduleListEvent extends AbstractTideEvent
	{
		private var _scheduleTemplateInfo:ScheduleTemplateInfo;
		private var _callbackFunc:Function;
		
		public function ReceiveDueOrderDeliveryScheduleListEvent(scheduleTemplateInfo:ScheduleTemplateInfo, callbackFunc:Function=null)
		{
			super();
			_scheduleTemplateInfo = scheduleTemplateInfo;	
			_callbackFunc = callbackFunc;
		}
		
		public function get scheduleTemplateInfo():ScheduleTemplateInfo{
			return _scheduleTemplateInfo;
		}
		
		public function get callbackFunc():Function{
			return _callbackFunc;
		}
	}
}