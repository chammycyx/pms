package hk.org.ha.control.pms.main.vetting.mp
{	
	import hk.org.ha.event.pms.main.vetting.mp.CheckAtdpsItemLocationByItemCodeEvent;
	import hk.org.ha.model.pms.biz.vetting.mp.CheckAtdpsServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("checkAtdpsServiceCtl", restrict="true")]
	public class CheckAtdpsServiceCtl
	{
		[In]
		public var checkAtdpsService:CheckAtdpsServiceBean;
		
		[In]
		public var atdpsItemLocationList:ArrayCollection;
		
		[Observer]
		public function checkAtdpsItemLocationByItemCode(evt:CheckAtdpsItemLocationByItemCodeEvent):void
		{
			checkAtdpsService.checkAtdpsItemLocationByItemCode(
				evt.itemCode, 
				evt.pickStationNumList, 
				function(event:TideResultEvent):void {
					evt.callBack(atdpsItemLocationList);
				}
			);
		}
	}
}
