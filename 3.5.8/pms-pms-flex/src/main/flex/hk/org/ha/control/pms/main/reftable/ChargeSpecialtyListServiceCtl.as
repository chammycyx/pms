package hk.org.ha.control.pms.main.reftable {
	
	import hk.org.ha.event.pms.main.reftable.PrintSpecMapSfiRptEvent;
	import hk.org.ha.event.pms.main.reftable.RefreshChargeSpecialtyListEvent;
	import hk.org.ha.event.pms.main.reftable.RefreshUpdateChargeSpecialtyListEvent;
	import hk.org.ha.event.pms.main.reftable.RetrieveChargeSpecialtyListEvent;
	import hk.org.ha.event.pms.main.reftable.RetrieveSpecialtyListEvent;
	import hk.org.ha.event.pms.main.reftable.UpdateChargeSpecialtyListEvent;
	import hk.org.ha.model.pms.biz.reftable.ChargeSpecialtyListServiceBean;
	import hk.org.ha.model.pms.biz.reftable.SpecialtyListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("chargeSpecialtyListServiceCtl", restrict="true")]
	public class ChargeSpecialtyListServiceCtl 
	{
		[In]
		public var chargeSpecialtyListService:ChargeSpecialtyListServiceBean;
		
		[In]
		public var specialtyListService:SpecialtyListServiceBean;
				
		[In]
		public var chargeSpecialtyList:ArrayCollection;
		
		[In]
		public var chargeSpecialtySnList:ArrayCollection;
		
		private var callBackFunction:Function;
		
		[Observer]
		public function retrieveSpecialtyList(evt:RetrieveSpecialtyListEvent):void
		{
			specialtyListService.retrieveSpecialtyList();
		}
		
		[Observer]
		public function retrieveChargeSpecialtyList(evt:RetrieveChargeSpecialtyListEvent):void
		{
			callBackFunction = evt.callBackFunction;
			chargeSpecialtyListService.retrieveChargeSpecialtyList(retrieveChargeSpecialtyListResult);
		}
		
		private function retrieveChargeSpecialtyListResult(evt:TideResultEvent):void 
		{
			if ( callBackFunction == null ) {
				evt.context.dispatchEvent(new RefreshChargeSpecialtyListEvent());
			} else {
				callBackFunction(chargeSpecialtyList,chargeSpecialtySnList);
			}
		}
		
		[Observer]
		public function updateChargeSpecialtyList(evt:UpdateChargeSpecialtyListEvent):void
		{
			chargeSpecialtyListService.saveChargeSpecialtyList(evt.sfiChargeSpecialtyList, evt.snChargeSpecialtyList, updateChargeSpecialtyListResult);
		}
		
		private function updateChargeSpecialtyListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshUpdateChargeSpecialtyListEvent());
		}
		
		[Observer]
		public function printSpecMapSfiRpt(evt:PrintSpecMapSfiRptEvent):void
		{
			if(evt.printSfi) {
				chargeSpecialtyListService.printSpecMapSfiRpt();
			}
			else {
				chargeSpecialtyListService.printSpecMapSnRpt();
			}
			
		}	
		
	}
}
