package hk.org.ha.event.pms.main.onestop
{
	import flash.events.Event;
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;

	public class RefreshUnvetOrderEvent extends AbstractTideEvent
	{
		private var _orderNum:String;
		
		public function RefreshUnvetOrderEvent(orderNum:String)
		{
			super();
			_orderNum = orderNum;
		}
		
		public function get orderNum():String {
			return _orderNum;
		}
	}
}