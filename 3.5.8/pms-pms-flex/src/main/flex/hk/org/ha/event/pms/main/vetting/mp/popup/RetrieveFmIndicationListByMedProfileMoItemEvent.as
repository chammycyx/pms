package hk.org.ha.event.pms.main.vetting.mp.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
	
	public class RetrieveFmIndicationListByMedProfileMoItemEvent extends AbstractTideEvent {

		private var _medProfileMoItem:MedProfileMoItem;
		
		public function RetrieveFmIndicationListByMedProfileMoItemEvent(medProfileMoItem:MedProfileMoItem)
		{
			super();
			_medProfileMoItem = medProfileMoItem;
		}
		
		public function get medProfileMoItem():MedProfileMoItem {
			return _medProfileMoItem;
		}
	}
}
