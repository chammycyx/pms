package hk.org.ha.event.pms.main.reftable.pivas {
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdatePivasWardListEvent extends AbstractTideEvent 
	{
		private var _pivasWardList:ListCollectionView;
		
		private var _patHospCode:String;
		
		public function UpdatePivasWardListEvent(pivasWardList:ListCollectionView, patHospCode:String):void 
		{
			super();
			_pivasWardList = pivasWardList;
			_patHospCode = patHospCode;
		}
		
		public function get pivasWardList():ListCollectionView
		{
			return _pivasWardList;
		}
		
		public function get patHospCode():String
		{
			return _patHospCode;
		}
	}
}