package hk.org.ha.view.pms.main.medprofile
{
	import mx.formatters.NumberFormatter;
	
	public class AdvancedNumberFormatter extends NumberFormatter
	{
		public var truncateTrailingZeros:Boolean = false;
		
		public var usePositiveSign:Boolean = false;
		
		public function AdvancedNumberFormatter()
		{
			super();
		}
		
		override public function format(value:Object):String
		{
			var result:String = super.format(value);
			if (truncateTrailingZeros && result.indexOf(decimalSeparatorTo) != -1) {
				result = result.replace(new RegExp("(" + quote(decimalSeparatorTo) + ")?0*$" , "g"), "");
			}
			if (usePositiveSign && value > 0) {
				result = "+" + result;
			}
			
			return result;
		}
		
		private function quote(value:String):String
		{
			var result:Array = new Array();
			for (var i:int; i < value.length; i++) {
				result.push("\\", value.charAt(i));
			}
			return result.join("");
		}
	}
}