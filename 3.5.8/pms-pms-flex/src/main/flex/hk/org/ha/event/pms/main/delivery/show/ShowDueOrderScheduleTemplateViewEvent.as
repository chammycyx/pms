package hk.org.ha.event.pms.main.delivery.show
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDueOrderScheduleTemplateViewEvent extends AbstractTideEvent
	{
		public function ShowDueOrderScheduleTemplateViewEvent()
		{
			super();
		}
	}
}