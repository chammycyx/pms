package hk.org.ha.control.pms.main.reftable.pivas
{
	import hk.org.ha.event.pms.main.reftable.pivas.RetrievePivasSystemMaintInfoEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.SetPivasSystemMaintInfoEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.ShowPivasSystemMaintSaveSuccessMsgEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.UpdatePivasSystemMaintInfoEvent;
	import hk.org.ha.model.pms.biz.reftable.pivas.PivasSystemMaintServiceBean;
	import hk.org.ha.model.pms.vo.reftable.pivas.PivasSystemMaintInfo;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("pivasSystemMaintServiceCtl", restrict="true")]
	public class PivasSystemMaintServiceCtl
	{	
		[In]
		public var pivasSystemMaintService:PivasSystemMaintServiceBean;
		
		[Observer]
		public function retrievePivasSystemMaintInfo(evt:RetrievePivasSystemMaintInfoEvent):void {
			pivasSystemMaintService.retrievePivasSystemMaintInfo(evt.year, refreshPivasSystemMaint);
		}
		
		[Observer]
		public function updatePivasSystemMaintInfo(evt:UpdatePivasSystemMaintInfoEvent):void {
			pivasSystemMaintService.updatePivasSystemMaint(evt.pivasSystemMaintUpdateSummary, updatePivasSystemMaintInfoResult);
		}
		
		private function refreshPivasSystemMaint(evt:TideResultEvent):void {	
			evt.context.dispatchEvent( new SetPivasSystemMaintInfoEvent(evt.result as PivasSystemMaintInfo) );
		}
		
		private function updatePivasSystemMaintInfoResult(evt:TideResultEvent):void {	
			evt.context.dispatchEvent( new ShowPivasSystemMaintSaveSuccessMsgEvent(evt.result as PivasSystemMaintInfo) );
		}
	}
}