package hk.org.ha.event.pms.main.onestop.show {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowOneStopEntryViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		private var _orderNum:String;
		
		public function ShowOneStopEntryViewEvent(clearMessages:Boolean=true, orderNum:String=null):void 
		{
			super();
			_clearMessages = clearMessages;
			_orderNum = orderNum;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
		
		public function get orderNum():String {
			return _orderNum;
		}
	}
}