package hk.org.ha.event.pms.main.report.show
{
	import org.granite.tide.events.AbstractTideEvent;

	public class ShowRefillItemRptViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		public function ShowRefillItemRptViewEvent()
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}