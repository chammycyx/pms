package hk.org.ha.event.pms.main.checkissue
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class PrintBatchIssueRptEvent extends AbstractTideEvent 
	{		
		private var _rptDocType:String;
		
		public function PrintBatchIssueRptEvent(rptDocType:String):void 
		{
			super();
			_rptDocType = rptDocType;
		}
		
		public function get rptDocType():String
		{
			return _rptDocType;
		}
	}
}