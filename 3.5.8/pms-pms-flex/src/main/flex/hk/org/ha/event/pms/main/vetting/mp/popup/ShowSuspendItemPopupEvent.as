package hk.org.ha.event.pms.main.vetting.mp.popup
{
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowSuspendItemPopupEvent extends AbstractTideEvent
	{
		private var _medProfileItem:MedProfileItem;
		
		public function ShowSuspendItemPopupEvent(medProfileItem:MedProfileItem)
		{
			super();
			_medProfileItem = medProfileItem;
		}
		
		public function get medProfileItem():MedProfileItem
		{
			return _medProfileItem;
		}
	}
}