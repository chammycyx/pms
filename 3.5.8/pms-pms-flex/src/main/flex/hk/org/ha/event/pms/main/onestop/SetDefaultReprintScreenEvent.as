package hk.org.ha.event.pms.main.onestop {
		
	import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
	import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
	import hk.org.ha.model.pms.udt.printing.PrintLang;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
		
	public class SetDefaultReprintScreenEvent extends AbstractTideEvent 
	{		
		public function SetDefaultReprintScreenEvent():void 
		{
			super();
		}   
	}
}