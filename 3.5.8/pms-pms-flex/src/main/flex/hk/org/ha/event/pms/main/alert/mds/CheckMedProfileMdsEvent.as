package hk.org.ha.event.pms.main.alert.mds {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	
	import mx.collections.ListCollectionView;
	
	public class CheckMedProfileMdsEvent extends AbstractTideEvent {
		
		private var _callback:Function;
		
		private var _medProfile:MedProfile;
		
		private var _medProfileItemList:ListCollectionView;
		
		public function CheckMedProfileMdsEvent(callback:Function, medProfile:MedProfile, medProfileItemList:ListCollectionView) {
			super();
			_callback = callback;
			_medProfile = medProfile;
			_medProfileItemList = medProfileItemList;
		}
		
		public function get callback():Function {
			return _callback;
		}
		
		public function get medProfile():MedProfile {
			return _medProfile;
		}
		
		public function get medProfileItemList():ListCollectionView {
			return _medProfileItemList;
		}
	}
}
