package hk.org.ha.control.pms.main.reftable {
	
	import hk.org.ha.event.pms.main.reftable.RetrieveRefillHolidayAdvDueDatePropEvent;
	import hk.org.ha.event.pms.main.reftable.UpdateRefillHolidayAdvDueDateEvent;
	import hk.org.ha.model.pms.biz.reftable.RefillHolidayServiceBean;
	
	[Bindable]
	[Name("refillHolidayServiceCtl", restrict="true")]
	public class RefillHolidayServiceCtl 
	{
		[In]
		public var refillHolidayService:RefillHolidayServiceBean;
		
		[Observer]
		public function updateRefillHolidayAdvDueDate(evt:UpdateRefillHolidayAdvDueDateEvent):void
		{
			refillHolidayService.updateRefillHolidayAdvanceDueDate(evt.advDueDate);
		}
		
		[Observer]
		public function retrieveRefillHolidayAdvDueDateProp(evt:RetrieveRefillHolidayAdvDueDatePropEvent):void
		{
			refillHolidayService.retrieveRefillHolidayAdvanceDueDateProp();
		}

	}
}
