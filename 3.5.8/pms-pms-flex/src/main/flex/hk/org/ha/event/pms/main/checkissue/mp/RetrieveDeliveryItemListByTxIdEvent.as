package hk.org.ha.event.pms.main.checkissue.mp
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDeliveryItemListByTxIdEvent extends AbstractTideEvent 
	{
		private var _mpDispLabelQrCodeXml:String;
		
		public function RetrieveDeliveryItemListByTxIdEvent(mpDispLabelQrCodeXml:String):void 
		{
			super();
			_mpDispLabelQrCodeXml = mpDispLabelQrCodeXml;
		}
		
		public function get mpDispLabelQrCodeXml():String {
			return _mpDispLabelQrCodeXml;
		}

	}
}