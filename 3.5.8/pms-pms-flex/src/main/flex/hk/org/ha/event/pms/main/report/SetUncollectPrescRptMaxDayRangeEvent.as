package hk.org.ha.event.pms.main.report {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetUncollectPrescRptMaxDayRangeEvent extends AbstractTideEvent 
	{
		private var _maxDayRange:Number;
		
		public function SetUncollectPrescRptMaxDayRangeEvent(maxDayRange:Number):void 
		{
			super();
			_maxDayRange = maxDayRange;
		}
		
		public function get maxDayRange():Number {
			return _maxDayRange;
		}
	}
}