package hk.org.ha.control.pms.main.reftable {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.reftable.ClearItemLocationViewEvent;
	import hk.org.ha.event.pms.main.reftable.RefreshItemLocationViewEvent;
	import hk.org.ha.event.pms.main.reftable.RetrieveItemLocationDmDrugLiteEvent;
	import hk.org.ha.event.pms.main.reftable.RetrieveItemLocationListByItemCodeEvent;
	import hk.org.ha.event.pms.main.reftable.RetrieveItemLocationListByPickStationNumEvent;
	import hk.org.ha.event.pms.main.reftable.RetrieveItemLocationPickStationListEvent;
	import hk.org.ha.event.pms.main.reftable.SetItemLocationPickStationListEvent;
	import hk.org.ha.event.pms.main.reftable.UpdateItemLocationListEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.ShowLoadingPopupEvent;
	import hk.org.ha.model.pms.biz.reftable.ItemLocationListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("itemLocationListServiceCtl", restrict="true")]
	public class ItemLocationListServiceCtl 
	{
		[In]
		public var itemLocationListService:ItemLocationListServiceBean;
		
		private var callBackEvent:Event;
		
		[Observer]
		public function retrieveItemLocationDmDrugLite(evt:RetrieveItemLocationDmDrugLiteEvent):void
		{
			callBackEvent = evt.callBackEvent;
			itemLocationListService.retrieveDmDrugLite(evt.itemCode, retrieveItemLocationDmDrugLiteResult);
		}
		
		private function retrieveItemLocationDmDrugLiteResult(evt:TideResultEvent):void 
		{
			if ( callBackEvent != null ) {
				evt.context.dispatchEvent( callBackEvent );
			}
		}
		
		[Observer]
		public function retrieveItemLocationPickStationList(evt:RetrieveItemLocationPickStationListEvent):void
		{
			itemLocationListService.retrievePickStationList(retrieveItemLocationPickStationListResult);
		}
		
		private function retrieveItemLocationPickStationListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent( new SetItemLocationPickStationListEvent() );
		}
		
		[Observer]
		public function retrieveItemLocationListByItemCode(evt:RetrieveItemLocationListByItemCodeEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Loading item location information ..."));
			itemLocationListService.retrieveItemLocationListByItemCode(evt.itemCode, retrieveItemLocationListResult);
		}
		
		private function retrieveItemLocationListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			evt.context.dispatchEvent(new RefreshItemLocationViewEvent(evt.result as ArrayCollection));
		}
		
		[Observer]
		public function retrieveItemLocationListByPickStationNumEvent(evt:RetrieveItemLocationListByPickStationNumEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Loading item location information ..."));
			itemLocationListService.retrieveItemLocationListByPickStation(evt.pickStationNum, retrieveItemLocationListResult);
		}
		
		[Observer]
		public function updateItemLocationListEvent(evt:UpdateItemLocationListEvent):void
		{
			In(Object(itemLocationListService).saveSuccess)
			In(Object(itemLocationListService).binNum)
			itemLocationListService.updateItemLocationList(evt.updateItemLocationList, updateItemLocationListEventResult);
		}
		
		private function updateItemLocationListEventResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new ClearItemLocationViewEvent(Object(itemLocationListService).saveSuccess, Object(itemLocationListService).binNum));
		}
	}
}
