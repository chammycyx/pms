package hk.org.ha.event.pms.main.charging.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowMpSfiInvoiceVoidConfrimPopupEvent extends AbstractTideEvent 
	{
		private var _caseNum:String;
		private var _patName:String;
		private var _invoiceNum:String;
		private var _invoiceDateStr:String;
		private var _voidFunc:Function;
		
		public function ShowMpSfiInvoiceVoidConfrimPopupEvent(caseNum:String, patName:String, invoiceNum:String, invoiceDateStr:String, voidFunc:Function):void 
		{
			super();		
			_caseNum = caseNum;
			_patName = patName;
			_invoiceNum = invoiceNum;
			_invoiceDateStr = invoiceDateStr;
			_voidFunc = voidFunc;
		}
		
		public function get caseNum():String {
			return _caseNum;
		}
		
		public function get patName():String {
			return _patName;
		}
		
		public function get invoiceNum():String {
			return _invoiceNum;
		}
		
		public function get invoiceDateStr():String {
			return _invoiceDateStr;
		}
		
		public function get voidFunc():Function {
			return _voidFunc;
		}
	}
}