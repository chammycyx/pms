package hk.org.ha.event.pms.main.vetting.popup {
	
	import flash.events.Event;	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshSfiHandleChargeExemptAuthentPopupEvent extends AbstractTideEvent 
	{		
		private var _errMsg:String;
		private var _validateCallbackFunc:Function;
		
		public function RefreshSfiHandleChargeExemptAuthentPopupEvent(errMsg:String, validateCallbackFunc:Function):void 
		{
			super();
			_errMsg = errMsg;
			_validateCallbackFunc = validateCallbackFunc;
		}
		
		public function get validateCallbackEvent():Function{
			return _validateCallbackFunc;
		}
		
		public function get errMsg():String{
			return _errMsg;
		}
	}
}
