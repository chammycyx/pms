package hk.org.ha.event.pms.main.reftable.mp
{
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshMpDosageConversionRptListEvent extends AbstractTideEvent 
	{
		private var _retrieveSuccess:Boolean;
		
		public function RefreshMpDosageConversionRptListEvent(retrieveSuccess:Boolean):void 
		{
			super();
			_retrieveSuccess = retrieveSuccess;
		}
		
		public function get retrieveSuccess():Boolean
		{
			return _retrieveSuccess;
		}
	}
}