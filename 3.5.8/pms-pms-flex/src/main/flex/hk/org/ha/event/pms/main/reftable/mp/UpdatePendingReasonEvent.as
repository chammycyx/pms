package hk.org.ha.event.pms.main.reftable.mp {
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdatePendingReasonEvent extends AbstractTideEvent 
	{
		private var _pendingReasonList:ListCollectionView;
		
		public function UpdatePendingReasonEvent(pendingReasonList:ListCollectionView):void 
		{
			super();
			_pendingReasonList = pendingReasonList;
		}
		
		public function get pendingReasonList():ListCollectionView
		{
			return _pendingReasonList;
		}
	}
}