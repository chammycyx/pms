package hk.org.ha.event.pms.main.onestop {
		
	import org.granite.tide.events.AbstractTideEvent;
	
	public class WriteHkidUnknownDigitLogEvent extends AbstractTideEvent 
	{			
		private var _hkid:String;
		
		public function WriteHkidUnknownDigitLogEvent(hkid:String):void 
		{
			super();
			_hkid = hkid;
		}        
		
		public function get hkid():String
		{
			return _hkid;
		}
	}
}