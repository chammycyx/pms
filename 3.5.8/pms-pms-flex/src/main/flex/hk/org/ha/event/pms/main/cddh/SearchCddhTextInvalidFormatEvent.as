package hk.org.ha.event.pms.main.cddh {
		
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SearchCddhTextInvalidFormatEvent extends AbstractTideEvent 
	{		
		private var _errMsg:String;
		
		public function SearchCddhTextInvalidFormatEvent(errMsg:String):void 
		{
			super();
			_errMsg = errMsg;
		}
		
		public function get errMsg():String {
			return _errMsg;
		}
	}
}