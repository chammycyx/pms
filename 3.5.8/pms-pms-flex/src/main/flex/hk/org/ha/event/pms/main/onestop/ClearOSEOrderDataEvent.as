package hk.org.ha.event.pms.main.onestop {
		
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ClearOSEOrderDataEvent extends AbstractTideEvent 
	{	
		public function ClearOSEOrderDataEvent():void 
		{
			super();
		}        
	}
}