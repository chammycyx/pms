package hk.org.ha.event.pms.main.reftable {
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdatePmsRestrictItemSpecListEvent extends AbstractTideEvent 
	{
		private var _newPmsRestrictItemSpecList:ListCollectionView;
		private var _itemCode:String;
		private var _fmStatus:String;
		
		public function UpdatePmsRestrictItemSpecListEvent(newPmsRestrictItemSpecList:ListCollectionView, itemCode:String, fmStatus:String):void 
		{
			super();
			_newPmsRestrictItemSpecList = newPmsRestrictItemSpecList;
			_itemCode = itemCode;
			_fmStatus = fmStatus;
		}
		
		public function get newPmsRestrictItemSpecList():ListCollectionView
		{
			return _newPmsRestrictItemSpecList;
		}
		
		public function get itemCode():String
		{
			return _itemCode;
		}
		
		public function get fmStatus():String
		{
			return _fmStatus;
		}
	}
}