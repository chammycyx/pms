package hk.org.ha.event.pms.main.reftable {
	
	import hk.org.ha.model.pms.dms.persistence.PmsSpecMapping;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class DeletePmsSpecMappingEvent extends AbstractTideEvent 
	{
		private var _delPmsSpecMapping:PmsSpecMapping;
		
		public function DeletePmsSpecMappingEvent(delPmsSpecMapping:PmsSpecMapping):void 
		{
			super();
			_delPmsSpecMapping = delPmsSpecMapping;
		}
	
		
		public function get delPmsSpecMapping():PmsSpecMapping
		{
			return _delPmsSpecMapping;
		}
	}
}