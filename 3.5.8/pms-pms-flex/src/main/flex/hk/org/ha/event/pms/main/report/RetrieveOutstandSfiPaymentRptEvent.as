package hk.org.ha.event.pms.main.report
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveOutstandSfiPaymentRptEvent extends AbstractTideEvent 
	{
		private var _dispDate:Date;
		
		public function RetrieveOutstandSfiPaymentRptEvent(dispDate:Date):void {
			super();
			this._dispDate = dispDate;
		}
		
		public function get dispDate():Date {
			return _dispDate;
		}
		
	}
}