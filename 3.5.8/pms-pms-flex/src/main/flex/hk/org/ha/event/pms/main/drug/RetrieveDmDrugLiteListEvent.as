package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDmDrugLiteListEvent extends AbstractTideEvent 
	{
		private var _itemCode:String;
		private var _showNonSuspendItemOnly:Boolean;
		private var _fdnFlag:Boolean;
		private var _callBackEvent:Event; //for ItemCodeLookup call back set focus purpose
		
		public function RetrieveDmDrugLiteListEvent(itemCode:String, showNonSuspendItemOnly:Boolean, fdnFlag:Boolean, callBackEvent:Event=null):void 
		{
			super();
			_itemCode = itemCode;
			_showNonSuspendItemOnly = showNonSuspendItemOnly;
			_fdnFlag = fdnFlag;
			_callBackEvent = callBackEvent;
		}
		
		public function get itemCode():String
		{
			return _itemCode;
		}
		
		public function get showNonSuspendItemOnly():Boolean
		{
			return _showNonSuspendItemOnly;
		}
		
		public function get fdnFlag():Boolean
		{
			return _fdnFlag;
		}
		
		public function get callBackEvent():Event
		{
			return _callBackEvent;
		}
	}
}