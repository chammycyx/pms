package hk.org.ha.event.pms.main.alert.druginfo {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrieveDrugMdsByItemCodeEvent extends AbstractTideEvent {
		
		private var _itemCode:String;
		
		private var _checkHq:Boolean;
		
		private var _checkRenal:Boolean;
		
		private var _callback:Function;
		
		public function RetrieveDrugMdsByItemCodeEvent(itemCode:String, checkHq:Boolean, checkRenal:Boolean, callback:Function) {
			super();
			_itemCode = itemCode;
			_callback = callback;
			_checkHq = checkHq;
			_checkRenal = checkRenal;
		}
		
		public function get itemCode():String {
			return _itemCode;
		}
		
		public function get checkHq():Boolean {
			return _checkHq;
		}
		
		public function get checkRenal():Boolean {
			return _checkRenal;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}
