package hk.org.ha.event.pms.main.checkissue
{
	import hk.org.ha.model.pms.vo.checkissue.CheckIssueTicketMatchAuditInfo;
	import org.granite.tide.events.AbstractTideEvent;
	
	public class AuditTicketNotMatchEvent extends AbstractTideEvent 
	{		
		private var _checkIssueTicketMatchAuditInfo:CheckIssueTicketMatchAuditInfo;
		
		public function AuditTicketNotMatchEvent(checkIssueTicketMatchAuditInfo:CheckIssueTicketMatchAuditInfo):void 
		{
			super();
			_checkIssueTicketMatchAuditInfo = checkIssueTicketMatchAuditInfo;
		}
		
		public function get checkIssueTicketMatchAuditInfo():CheckIssueTicketMatchAuditInfo {
			return _checkIssueTicketMatchAuditInfo;
		}
	}
}