package hk.org.ha.event.pms.main.reftable {
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateFdnMappingListEvent extends AbstractTideEvent 
	{
		private var _fdnMappingList:ListCollectionView;
		private var _itemCode:String;
		
		public function UpdateFdnMappingListEvent(fdnMappingList:ListCollectionView, itemCode:String):void 
		{
			super();
			_fdnMappingList = fdnMappingList;
			_itemCode = itemCode;
		}
		
		public function get fdnMappingList():ListCollectionView
		{
			return _fdnMappingList;
		}
		
		public function get itemCode():String
		{
			return _itemCode;
		}
	}
}