package hk.org.ha.event.pms.main.inbox
{
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class FinishConstructManualProfileEvent extends AbstractTideEvent
	{
		private var _medProfile:MedProfile;
		
		public function FinishConstructManualProfileEvent(medProfile:MedProfile)
		{
			super();
			_medProfile = medProfile
		}
		
		public function get medProfile():MedProfile
		{
			return _medProfile;
		}
	}
}