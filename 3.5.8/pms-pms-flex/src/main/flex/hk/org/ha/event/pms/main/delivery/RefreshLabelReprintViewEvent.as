package hk.org.ha.event.pms.main.delivery {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshLabelReprintViewEvent extends AbstractTideEvent
	{
		public function RefreshLabelReprintViewEvent():void 
		{
			super();
		}
	}
}