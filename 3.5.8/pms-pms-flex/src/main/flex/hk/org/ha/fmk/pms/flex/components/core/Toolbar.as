package hk.org.ha.fmk.pms.flex.components.core
{
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	
	import hk.org.ha.fmk.pms.flex.components.message.ShowSystemMessagePopupEvent;
	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	import hk.org.ha.fmk.pms.flex.components.window.WindowSwitchEvent;
	import hk.org.ha.fmk.pms.flex.utils.PopupUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.LinkBar;
	import mx.controls.LinkButton;
	import mx.core.IFlexDisplayObject;
	import mx.core.UIComponent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	public class Toolbar extends LinkBar
	{			
		public var _addToWorklistFunc:Function = null;
		public var _retrieveFunc:Function = null;
		public var _notMapFunc:Function = null;
		public var _copyFunc:Function = null;
		public var _saveFunc:Function = null;
		public var _saveYesFunc:Function = null;
		public var _saveNoFunc:Function = null;
		public var _savePrintFunc:Function = null;
		public var _deleteYesFunc:Function = null;
		public var _deleteNoFunc:Function = null;
		public var _unlinkFunc:Function = null;		
		public var _clearFunc:Function = null;
		public var _printFunc:Function = null;
		public var _reprintFunc:Function = null;
		public var _voidInvoiceFunc:Function = null;
		public var _exportFunc:Function = null;
		public var _suspendFunc:Function = null;
		public var _unvetFunc:Function = null;
		public var _cancelFunc:Function = null;
		public var _refillFunc:Function = null;
		public var _newFunc:Function = null;
		public var _drugSearchFunc:Function = null;
		public var _displayAllFunc:Function = null;
		public var _assembleListFunc:Function = null;
		public var _sendFunc:Function = null;
		public var _saveItemFunc:Function = null;
		public var _refreshFunc:Function = null;
		public var _checkFunc:Function = null;
		public var _issueFunc:Function = null;
		public var _showLedFunc:Function = null;
		public var _setCheckStationFunc:Function = null;
		public var _medicationSummaryReportFunc:Function = null;
		public var _defaultSequenceFunc:Function = null;
		public var _backToOneStopEntryFunc:Function = null;
		public var _backToDrugAssemblingFunc:Function = null;
		public var _backToDrugCheckingIssuingFunc:Function = null;
		public var _upFunc:Function = null;
		public var _downFunc:Function = null;
		public var _oneStopEntryFunc:Function = null;
		public var _logHistoryFunc:Function = null;
		public var _unlockFunc:Function = null;
		public var _sortFunc:Function = null;
		public var _createManualProfileFunc:Function = null;
		public var _vetFunc:Function = null;
		public var _verifyFunc:Function = null;
		public var _urgentFunc:Function = null;
		public var _pendingFunc:Function = null;
		public var _setLabelInstructionFunc:Function = null;
		public var _previewLabelFunc:Function = null;
		public var _enquireCddhFunc:Function = null;
		public var _saveProfileFunc:Function = null;
		public var _backToInboxFunc:Function = null;
		public var _addDispItemFunc:Function = null;
		public var _deleteDispItemFunc:Function = null;
		public var _resetFunc:Function = null;
		public var _showAllRequestFunc:Function = null;
		public var _resumeFunc:Function = null;
		public var _restoreWarningCodeFunc:Function = null;
		public var _refreshItemCountFunc:Function = null;
		public var _refreshScheduleListFunc:Function = null;
		public var _changeTemplateFunc:Function = null;
		public var _newOrderListFunc:Function = null;
		public var _followUpListFunc:Function = null;
		public var _assignBatchPrintFunc:Function = null;
		public var _reprintDeliveryLabelFunc:Function = null;
		public var _reprintDividerLabelFunc:Function = null;
		public var _assignBatchFunc:Function = null;
		public var _filterFunc:Function = null;
		public var _closeFunc:Function = null;
		public var _batchSendFunc:Function = null;
		public var _batchTicketPrintingFunc:Function = null;
		
		private var operationName:String;
		
		public var addToWorklistButton:LinkButton;
		public var retrieveButton:LinkButton;
		public var notMapButton:LinkButton;
		public var copyButton:LinkButton;
		public var saveButton:LinkButton;
		public var savePrintButton:LinkButton;
		public var deleteButton:LinkButton;
		public var clearButton:LinkButton;
		public var unlinkButton:LinkButton;
		public var printButton:LinkButton;
		public var reprintButton:LinkButton;
		public var voidInvoiceButton:LinkButton;
		public var exportButton:LinkButton;
		public var suspendButton:LinkButton;
		public var unvetButton:LinkButton;
		public var cancelButton:LinkButton;
		public var refillButton:LinkButton;
		public var newButton:LinkButton;
		public var drugSearchButton:LinkButton;
		public var displayAllButton:LinkButton;
		public var assembleListButton:LinkButton;
		public var sendButton:LinkButton;
		public var saveItemButton:LinkButton;
		public var refreshButton:LinkButton;
		public var checkButton:LinkButton;
		public var issueButton:LinkButton;
		public var showLedButton:LinkButton;
		public var setCheckStationButton:LinkButton;
		public var medicationSummaryReportButton:LinkButton;
		public var defaultSequenceButton:LinkButton;
		public var backToOneStopEntryButton:LinkButton;
		public var backToDrugAssemblingButton:LinkButton;
		public var backToDrugCheckingIssuingButton:LinkButton;
		public var upButton:LinkButton;
		public var downButton:LinkButton;
		public var oneStopEntryButton:LinkButton;
		public var logHistoryButton:LinkButton;
		public var unlockButton:LinkButton;
		public var sortButton:LinkButton;
		public var createManualProfileButton:LinkButton;
		public var vetButton:LinkButton;
		public var verifyButton:LinkButton;
		public var urgentButton:LinkButton;
		public var pendingButton:LinkButton;
		public var setLabelInstructionButton:LinkButton;
		public var previewLabelButton:LinkButton;
		public var enquireCddhButton:LinkButton;
		public var saveProfileButton:LinkButton;
		public var backToInboxButton:LinkButton;
		public var addDispItemButton:LinkButton;
		public var deleteDispItemButton:LinkButton;
		public var resetButton:LinkButton;
		public var showAllRequestButton:LinkButton;
		public var resumeButton:LinkButton;
		public var restoreWarningCodeButton:LinkButton;
		public var refreshItemCountButton:LinkButton;
		public var refreshScheduleListButton:LinkButton;
		public var changeTemplateButton:LinkButton;
		public var newOrderListButton:LinkButton;
		public var followUpListButton:LinkButton;
		public var assignBatchPrintButton:LinkButton;
		public var reprintDeliveryLabelButton:LinkButton;
		public var reprintDividerLabelButton:LinkButton;
		public var assignBatchButton:LinkButton;
		public var filterButton:LinkButton;
		public var closeButton:LinkButton;
		public var batchSendButton:LinkButton;
		public var batchTicketPrintingButton:LinkButton;
		
		public var _saveMsg:String = null;
		public var _deleteMsg:String = null;
		public var _saveMsgPopupWidth:int;
		public var _saveMsgPopupHeight:int;
		public var _delMsgPopupWidth:int;
		public var _delMsgPopupHeight:int;
		private var defaultSaveMsg:String = "<font size='16'><b>Are you sure you want to save?</b></font>";
		private var defaultDeleteMsg:String = "<font size='16'><b>Are you sure you want to delete?</b></font>";
		private var defaultNotMapMsg:String = "<font size='16'><b>Are you sure you want to mark \"Not Map\"?</b></font>";
		
		private var btnList:ArrayCollection = new ArrayCollection();
		private var btnSeq:Array = new Array("Add to Worklist", "Retrieve", "Not Map", "Copy", "Save", "Save Item", "Save & Print", "Add Disp. Item", "Delete Disp. Item", "Delete", "Unlink", "New", 
											 "Assign Batch", "Check", "Clear", "Restore Warning Code", "Print", "Reprint", "Void Invoice", "Export", "Cancel", "Sort", "Create Manual Profile", "Vet", 
											 "Verify", "Urgent", "Suspend", "Refill", "Drug Search", "Unlock", "Unvet", "Display All", "Assemble List", 
											 "Send", "Default Sequence", "Refresh", "Issue", "Show LED", "Set Check Station", 
											 "Medication Summary Report", "Back to Drug Checking & Issuing", "Back to One Stop Entry", 
											 "Back to Drug Assembling", "Up", "Down", "Log History", "Pending", "Resume", "Set Label Instruction", 
											 "Preview Label", "Reset", "Show All Request", "Enquire CDDH", "Save Profile", 
											 "Back to Inbox", "Refresh Item Count", "Refresh Schedule List", "Change Template", "New Order List", "Follow Up List",
											 "Assign Batch & Print", "Reprint Delivery Label", "Reprint Divider Label", "Filter", "Batch Send", "Batch Ticket Printing", "Close");
		
		public function Toolbar() {             
			super();
		}
		
		public function hasInited():Boolean {
			if (this.numChildren == 0) {
				return false;
			} else {
				return true;
			}
		}
		
		public function init():void {
			for each(var btnName:String in btnSeq) {
				for each(var btn:LinkButton in btnList) {
					if (btnName == btn.label) {
						this.addChild(btn);
						break;
					}
				}
			}
		}
				
		public function set saveMsg(saveMsg:String):void {
			this._saveMsg = saveMsg;
		}
		
		public function set deleteMsg(deleteMsg:String):void {
			this._deleteMsg = deleteMsg;
		}
		
		public function set saveMsgPopupWidth(saveMsgPopupWidth:int):void {
			this._saveMsgPopupWidth = saveMsgPopupWidth;
		}
		
		public function set saveMsgPopupHeight(saveMsgPopupHeight:int):void {
			this._saveMsgPopupHeight = saveMsgPopupHeight;
		}
		
		public function set delMsgPopupWidth(delMsgPopupWidth:int):void {
			this._delMsgPopupWidth = delMsgPopupWidth;
		}
		
		public function set delMsgPopupHeight(delMsgPopupHeight:int):void {
			this._delMsgPopupHeight = delMsgPopupHeight;
		}

		public function set addToWorklistFunc(addToWorklistFunc:Function):void {
			if (addToWorklistButton == null) {
				this._addToWorklistFunc = addToWorklistFunc;
				addToWorklistButton = new LinkButton();
				addToWorklistButton.label = "Add to Worklist";
				addToWorklistButton.tabFocusEnabled = false;
				addToWorklistButton.focusEnabled = false;
				addToWorklistButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(addToWorklistButton);
			}
		}
		
		public function set retrieveFunc(retrieveFunc:Function):void {
			if (retrieveButton == null) {
				this._retrieveFunc = retrieveFunc;
				retrieveButton = new LinkButton();
				retrieveButton.label = "Retrieve";
				retrieveButton.tabFocusEnabled = false;
				retrieveButton.focusEnabled = false;
				retrieveButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(retrieveButton);
			}
		}
		
		public function set notMapFunc(noteMapFunc:Function):void {
			if (notMapButton == null) {
				this._notMapFunc = noteMapFunc;
				notMapButton = new LinkButton();
				notMapButton.label = "Not Map";
				notMapButton.tabFocusEnabled = false;
				notMapButton.focusEnabled = false;
				notMapButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(notMapButton);
			}
		}
		
		public function set copyFunc(copyFunc:Function):void {
			if (copyButton == null) {
				this._copyFunc = copyFunc;
				copyButton = new LinkButton();
				copyButton.label = "Copy";
				copyButton.tabFocusEnabled = false;
				copyButton.focusEnabled = false;
				copyButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(copyButton);
			}
		}
		
		public function set saveFunc(saveFunc:Function):void {
			if (saveButton == null) {
				this._saveFunc = saveFunc;
				saveButton = new LinkButton();
				saveButton.label = "Save";
				saveButton.tabFocusEnabled = false;
				saveButton.focusEnabled = false;
				saveButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(saveButton);
			}
		}
		
		public function set saveYesFunc(saveYesFunc:Function):void {
			if (saveButton == null) {
				this._saveYesFunc = saveYesFunc;
				saveButton = new LinkButton();
				saveButton.label = "Save";
				saveButton.tabFocusEnabled = false;
				saveButton.focusEnabled = false;
				saveButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(saveButton);
			}
		}
		
		public function set saveNoFunc(saveNoFunc:Function):void {
			if (_saveNoFunc == null) {
				this._saveNoFunc = saveNoFunc;
			}
		}
		
		public function set savePrintFunc(savePrintFunc:Function):void {
			if (savePrintButton == null) {
				this._savePrintFunc = savePrintFunc;
				savePrintButton = new LinkButton();
				savePrintButton.label = "Save & Print";
				savePrintButton.tabFocusEnabled = false;
				savePrintButton.focusEnabled = false;
				savePrintButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(savePrintButton);
			}
		}
		
		public function set deleteYesFunc(deleteYesFunc:Function):void {
			if (deleteButton == null) {
				this._deleteYesFunc = deleteYesFunc;
				deleteButton = new LinkButton();
				deleteButton.label = "Delete";
				deleteButton.tabFocusEnabled = false;
				deleteButton.focusEnabled = false;
				deleteButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(deleteButton);
			}
		}
		
		public function set deleteNoFunc(deleteNoFunc:Function):void {
			if (_deleteNoFunc == null) {
				this._deleteNoFunc = deleteNoFunc;
			}
		}
		
		public function set clearFunc(clearFunc:Function):void {
			if (clearButton == null) {
				this._clearFunc = clearFunc;
				clearButton = new LinkButton();
				clearButton.label = "Clear";
				clearButton.tabFocusEnabled = false;
				clearButton.focusEnabled = false;
				clearButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(clearButton);
			}
		}
		
		public function set unlinkFunc(unlinkFunc:Function):void {
			if (unlinkButton == null) {
				this._unlinkFunc = unlinkFunc;
				unlinkButton = new LinkButton();
				unlinkButton.label = "Unlink";
				unlinkButton.tabFocusEnabled = false;
				unlinkButton.focusEnabled = false;
				unlinkButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(unlinkButton);
			}
		}
		
		public function set printFunc(printFunc:Function):void {
			if (printButton == null) {
				this._printFunc = printFunc;
				printButton = new LinkButton();
				printButton.label = "Print";
				printButton.tabFocusEnabled = false;
				printButton.focusEnabled = false;
				printButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(printButton);
			}
		}
		
		public function set reprintFunc(reprintFunc:Function):void {
			if (reprintButton == null) {
				this._reprintFunc = reprintFunc;
				reprintButton = new LinkButton();
				reprintButton.label = "Reprint";
				reprintButton.tabFocusEnabled = false;
				reprintButton.focusEnabled = false;
				reprintButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(reprintButton);
			}
		}
		
		public function set voidInvoiceFunc(voidInvoiceFunc:Function):void {
			if (voidInvoiceButton == null) {
				this._voidInvoiceFunc = voidInvoiceFunc;
				voidInvoiceButton = new LinkButton();
				voidInvoiceButton.label = "Void Invoice";
				voidInvoiceButton.tabFocusEnabled = false;
				voidInvoiceButton.focusEnabled = false;
				voidInvoiceButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(voidInvoiceButton);
			}
		}
		
		public function set exportFunc(exportFunc:Function):void {
			if (exportButton == null) {
				this._exportFunc = exportFunc;
				exportButton = new LinkButton();
				exportButton.label = "Export";
				exportButton.tabFocusEnabled = false;
				exportButton.focusEnabled = false;
				exportButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(exportButton);
			}
		}
		
		public function set suspendFunc(suspendFunc:Function):void {
			if (suspendButton == null) {
				this._suspendFunc = suspendFunc;
				suspendButton = new LinkButton();
				suspendButton.label = "Suspend";
				suspendButton.tabFocusEnabled = false;
				suspendButton.focusEnabled = false;
				suspendButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(suspendButton);
			}
		}
				
		public function set unvetFunc(unvetFunc:Function):void {
			if (unvetButton == null) {
				this._unvetFunc = unvetFunc;
				unvetButton = new LinkButton();
				unvetButton.label = "Unvet";
				unvetButton.tabFocusEnabled = false;
				unvetButton.focusEnabled = false;
				unvetButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(unvetButton);
			}
		}
		
		public function set cancelFunc(cancelFunc:Function):void {
			if (cancelButton == null) {
				this._cancelFunc = cancelFunc;
				cancelButton = new LinkButton();
				cancelButton.label = "Cancel";
				cancelButton.tabFocusEnabled = false;
				cancelButton.focusEnabled = false;
				cancelButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(cancelButton);
			}
		}
		
		public function set refillFunc(refillFunc:Function):void {
			if (refillButton == null) {
				this._refillFunc = refillFunc;
				refillButton = new LinkButton();
				refillButton.label = "Refill";
				refillButton.tabFocusEnabled = false;
				refillButton.focusEnabled = false;
				refillButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(refillButton);
			}
		}
				
		public function set newFunc(newFunc:Function):void {
			if (newButton == null) {
				this._newFunc = newFunc;
				newButton = new LinkButton();
				newButton.label = "New";
				newButton.tabFocusEnabled = false;
				newButton.focusEnabled = false;
				newButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(newButton);
			}
		}
		
		public function set drugSearchFunc(drugSearchFunc:Function):void {
			if (drugSearchButton == null) {
				this._drugSearchFunc = drugSearchFunc;
				drugSearchButton = new LinkButton();
				drugSearchButton.label = "Drug Search";
				drugSearchButton.tabFocusEnabled = false;
				drugSearchButton.focusEnabled = false;
				drugSearchButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(drugSearchButton);
			}
		}
		
		public function set displayAllFunc(displayAllFunc:Function):void {
			if (displayAllButton == null) {
				this._displayAllFunc = displayAllFunc;
				displayAllButton = new LinkButton();
				displayAllButton.label = "Display All";
				displayAllButton.tabFocusEnabled = false;
				displayAllButton.focusEnabled = false;
				displayAllButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(displayAllButton);
			}
		}
		
		public function set assembleListFunc(assembleListFunc:Function):void {
			if (assembleListButton == null) {
				this._assembleListFunc = assembleListFunc;
				assembleListButton = new LinkButton();
				assembleListButton.label = "Assemble List";
				assembleListButton.tabFocusEnabled = false;
				assembleListButton.focusEnabled = false;
				assembleListButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(assembleListButton);
			}
		}
		
		public function set sendFunc(sendFunc:Function):void {
			if (sendButton == null) {
				this._sendFunc = sendFunc;
				sendButton = new LinkButton();
				sendButton.label = "Send";
				sendButton.tabFocusEnabled = false;
				sendButton.focusEnabled = false;
				sendButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(sendButton);
			}
		}

		public function set saveItemFunc(saveItemFunc:Function):void {
			if (saveItemButton == null) {
				this._saveItemFunc = saveItemFunc;
				saveItemButton = new LinkButton();
				saveItemButton.label = "Save Item";
				saveItemButton.tabFocusEnabled = false;
				saveItemButton.focusEnabled = false;
				saveItemButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(saveItemButton);
			}
		}
				
		public function set refreshFunc(refreshFunc:Function):void {
			if (refreshButton == null) {
				this._refreshFunc = refreshFunc;
				refreshButton = new LinkButton();
				refreshButton.label = "Refresh";
				refreshButton.tabFocusEnabled = false;
				refreshButton.focusEnabled = false;
				refreshButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(refreshButton);
			}
		}
		
		public function set checkFunc(checkFunc:Function):void {
			if (checkButton == null) {
				this._checkFunc = checkFunc;
				checkButton = new LinkButton();
				checkButton.label = "Check";
				checkButton.tabFocusEnabled = false;
				checkButton.focusEnabled = false;
				checkButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(checkButton);
			}
		}
		
		public function set issueFunc(issueFunc:Function):void {
			if (issueButton == null) {
				this._issueFunc = issueFunc;
				issueButton = new LinkButton();
				issueButton.label = "Issue";
				issueButton.tabFocusEnabled = false;
				issueButton.focusEnabled = false;
				issueButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(issueButton);
			}
		}
		
		public function set showLedFunc(showLedFunc:Function):void {
			if (showLedButton == null) {
				this._showLedFunc = showLedFunc;
				showLedButton = new LinkButton();
				showLedButton.label = "Show LED";
				showLedButton.tabFocusEnabled = false;
				showLedButton.focusEnabled = false;
				showLedButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(showLedButton);
			}
		}
		
		public function set setCheckStationFunc(setCheckStationFunc:Function):void {
			if (setCheckStationButton == null) {
				this._setCheckStationFunc = setCheckStationFunc;
				setCheckStationButton = new LinkButton();
				setCheckStationButton.label = "Set Check Station";
				setCheckStationButton.tabFocusEnabled = false;
				setCheckStationButton.focusEnabled = false;
				setCheckStationButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(setCheckStationButton);
			}
		}
		
		public function set medicationSummaryReportFunc(medicationSummaryReportFunc:Function):void {
			if (medicationSummaryReportButton == null) {
				this._medicationSummaryReportFunc = medicationSummaryReportFunc;
				medicationSummaryReportButton = new LinkButton();
				medicationSummaryReportButton.label = "Medication Summary Report";
				medicationSummaryReportButton.tabFocusEnabled = false;
				medicationSummaryReportButton.focusEnabled = false;
				medicationSummaryReportButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(medicationSummaryReportButton);
			}
		}
			
		public function set defaultSequenceFunc(defaultSequenceFunc:Function):void {
			if (defaultSequenceButton == null) {
				this._defaultSequenceFunc = defaultSequenceFunc;
				defaultSequenceButton = new LinkButton();
				defaultSequenceButton.label = "Default Sequence";
				defaultSequenceButton.tabFocusEnabled = false;
				defaultSequenceButton.focusEnabled = false;
				defaultSequenceButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(defaultSequenceButton);
			}
		}
		
		public function set backToOneStopEntryFunc(backToOneStopEntryFunc:Function):void {
			if (backToOneStopEntryButton == null) {
				this._backToOneStopEntryFunc = backToOneStopEntryFunc;
				backToOneStopEntryButton = new LinkButton();
				backToOneStopEntryButton.label = "Back to One Stop Entry";
				backToOneStopEntryButton.tabFocusEnabled = false;
				backToOneStopEntryButton.focusEnabled = false;
				backToOneStopEntryButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(backToOneStopEntryButton);
			}
		}
		
		public function set backToDrugAssemblingFunc(backToDrugAssemblingFunc:Function):void {
			if (backToDrugAssemblingButton == null) {
				this._backToDrugAssemblingFunc = backToDrugAssemblingFunc;
				backToDrugAssemblingButton = new LinkButton();
				backToDrugAssemblingButton.label = "Back to Drug Assembling";
				backToDrugAssemblingButton.tabFocusEnabled = false;
				backToDrugAssemblingButton.focusEnabled = false;
				backToDrugAssemblingButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(backToDrugAssemblingButton);
			}
		}
				
		public function set backToDrugCheckingIssuingFunc(backToDrugCheckingIssuingFunc:Function):void {
			if (backToDrugCheckingIssuingButton == null) {
				this._backToDrugCheckingIssuingFunc = backToDrugCheckingIssuingFunc;
				backToDrugCheckingIssuingButton = new LinkButton();
				backToDrugCheckingIssuingButton.label = "Back to Drug Checking & Issuing";
				backToDrugCheckingIssuingButton.tabFocusEnabled = false;
				backToDrugCheckingIssuingButton.focusEnabled = false;
				backToDrugCheckingIssuingButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(backToDrugCheckingIssuingButton);
			}
		}
		
		public function set upFunc(upFunc:Function):void {
			if (upButton == null) {
				this._upFunc = upFunc;
				upButton = new LinkButton();
				upButton.label = "Up";
				upButton.tabFocusEnabled = false;
				upButton.focusEnabled = false;
				upButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(upButton);
			}
		}
		
		public function set downFunc(downFunc:Function):void {
			if (downButton == null) {
				this._downFunc = downFunc;
				downButton = new LinkButton();
				downButton.label = "Down";
				downButton.tabFocusEnabled = false;
				downButton.focusEnabled = false;
				downButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(downButton);
			}
		}
		
		public function set oneStopEntryFunc(oneStopEntryFunc:Function):void {
			if (oneStopEntryButton == null) {
				this._oneStopEntryFunc = oneStopEntryFunc;
				oneStopEntryButton = new LinkButton();
				oneStopEntryButton.label = "One Stop Entry";
				oneStopEntryButton.tabFocusEnabled = false;
				oneStopEntryButton.focusEnabled = false;
				oneStopEntryButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(oneStopEntryButton);
			}
		}
		
		public function set logHistoryFunc(logHistoryFunc:Function):void {
			if (logHistoryButton == null) {
				this._logHistoryFunc = logHistoryFunc;
				logHistoryButton = new LinkButton();
				logHistoryButton.label = "Log History";
				logHistoryButton.tabFocusEnabled = false;
				logHistoryButton.focusEnabled = false;
				logHistoryButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(logHistoryButton);
			}
		}
		
		public function set batchTicketPrintingFunc(batchTicketPrintingFunc:Function):void {
			if (batchTicketPrintingButton == null) {
				this._batchTicketPrintingFunc = batchTicketPrintingFunc;
				batchTicketPrintingButton = new LinkButton();
				batchTicketPrintingButton.label = "Batch Ticket Printing";
				batchTicketPrintingButton.tabFocusEnabled = false;
				batchTicketPrintingButton.focusEnabled = false;
				batchTicketPrintingButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(batchTicketPrintingButton);
			}
		}
		
		public function set unlockFunc(unlockFunc:Function):void {
			if (unlockButton == null) {
				this._unlockFunc = unlockFunc;
				unlockButton = new LinkButton();
				unlockButton.label = "Unlock";
				unlockButton.tabFocusEnabled = false;
				unlockButton.focusEnabled = false;
				unlockButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(unlockButton);
			}
		}
		
		public function set sortFunc(sortFunc:Function):void {
			if (sortButton == null) {
				this._sortFunc = sortFunc;
				sortButton = new LinkButton();
				sortButton.label = "Sort";
				sortButton.tabFocusEnabled = false;
				sortButton.focusEnabled = false;
				sortButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(sortButton);
			}
		}
		
		public function set createManualProfileFunc(createManualProfileFunc:Function):void {
			if (createManualProfileButton == null) {
				this._createManualProfileFunc = createManualProfileFunc;
				createManualProfileButton = new LinkButton();
				createManualProfileButton.label = "Create Manual Profile";
				createManualProfileButton.tabFocusEnabled = false;
				createManualProfileButton.focusEnabled = false;
				createManualProfileButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(createManualProfileButton);
			}
		}
				
		public function set vetFunc(vetFunc:Function):void {
			if (vetButton == null) {
				this._vetFunc = vetFunc;
				vetButton = new LinkButton();
				vetButton.label = "Vet";
				vetButton.tabFocusEnabled = false;
				vetButton.focusEnabled = false;
				vetButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(vetButton);
			}
		}
		
		public function set verifyFunc(verifyFunc:Function):void {
			if (verifyButton == null) {
				this._verifyFunc = verifyFunc;
				verifyButton = new LinkButton();
				verifyButton.label = "Verify";
				verifyButton.tabFocusEnabled = false;
				verifyButton.focusEnabled = false;
				verifyButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(verifyButton);
			}
		}
		
		public function set urgentFunc(urgentFunc:Function):void {
			if (urgentButton == null) {
				this._urgentFunc = urgentFunc;
				urgentButton = new LinkButton();
				urgentButton.label = "Urgent";
				urgentButton.tabFocusEnabled = false;
				urgentButton.focusEnabled = false;
				urgentButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(urgentButton);
			}
		}
		
		public function set pendingFunc(pendingFunc:Function):void {
			if (pendingButton == null) {
				this._pendingFunc = pendingFunc;
				pendingButton = new LinkButton();
				pendingButton.label = "Pending";
				pendingButton.tabFocusEnabled = false;
				pendingButton.focusEnabled = false;
				pendingButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(pendingButton);
			}
		}
		
		public function set setLabelInstructionFunc(setLabelInstructionFunc:Function):void {
			if (setLabelInstructionButton == null) {
				this._setLabelInstructionFunc = setLabelInstructionFunc;
				setLabelInstructionButton = new LinkButton();
				setLabelInstructionButton.label = "Set Label Instruction";
				setLabelInstructionButton.tabFocusEnabled = false;
				setLabelInstructionButton.focusEnabled = false;
				setLabelInstructionButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(setLabelInstructionButton);
			}
		}
		
		public function set previewLabelFunc(previewLabelFunc:Function):void {
			if (previewLabelButton == null) {
				this._previewLabelFunc = previewLabelFunc;
				previewLabelButton = new LinkButton();
				previewLabelButton.label = "Preview Label";
				previewLabelButton.tabFocusEnabled = false;
				previewLabelButton.focusEnabled = false;
				previewLabelButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(previewLabelButton);
			}
		}
		
		public function set enquireCddhFunc(enquireCddhFunc:Function):void {
			if (enquireCddhButton == null) {
				this._enquireCddhFunc = enquireCddhFunc;
				enquireCddhButton = new LinkButton();
				enquireCddhButton.label = "Enquire CDDH";
				enquireCddhButton.tabFocusEnabled = false;
				enquireCddhButton.focusEnabled = false;
				enquireCddhButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(enquireCddhButton);
			}
		}
		
		public function set saveProfileFunc(saveProfileFunc:Function):void {
			if (saveProfileButton == null) {
				this._saveProfileFunc = saveProfileFunc;
				saveProfileButton = new LinkButton();
				saveProfileButton.label = "Save Profile";
				saveProfileButton.tabFocusEnabled = false;
				saveProfileButton.focusEnabled = false;
				saveProfileButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(saveProfileButton);
			}
		}
		
		public function set backToInboxFunc(backToInboxFunc:Function):void {
			if (backToInboxButton == null) {
				this._backToInboxFunc = backToInboxFunc;
				backToInboxButton = new LinkButton();
				backToInboxButton.label = "Back to Inbox";
				backToInboxButton.tabFocusEnabled = false;
				backToInboxButton.focusEnabled = false;
				backToInboxButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(backToInboxButton);
			}
		}
		
		public function set addDispItemFunc(addDispItemFunc:Function):void {
			if (addDispItemButton == null) {
				this._addDispItemFunc = addDispItemFunc;
				addDispItemButton = new LinkButton();
				addDispItemButton.label = "Add Disp. Item";
				addDispItemButton.tabFocusEnabled = false;
				addDispItemButton.focusEnabled = false;
				addDispItemButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(addDispItemButton);
			}
		}
		
		public function set deleteDispItemFunc(deleteDispItemFunc:Function):void {
			if (deleteDispItemButton == null) {
				this._deleteDispItemFunc = deleteDispItemFunc;
				deleteDispItemButton = new LinkButton();
				deleteDispItemButton.label = "Delete Disp. Item";
				deleteDispItemButton.tabFocusEnabled = false;
				deleteDispItemButton.focusEnabled = false;
				deleteDispItemButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(deleteDispItemButton);
			}
		}
		
		public function set resetFunc(resetFunc:Function):void {
			if (resetButton == null) {
				this._resetFunc = resetFunc;
				resetButton = new LinkButton();
				resetButton.label = "Reset";
				resetButton.tabFocusEnabled = false;
				resetButton.focusEnabled = false;
				resetButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(resetButton);
			}
		}
		
		public function set showAllRequestFunc(showAllRequestFunc:Function):void {
			if (showAllRequestButton == null) {
				this._showAllRequestFunc = showAllRequestFunc;
				showAllRequestButton = new LinkButton();
				showAllRequestButton.label = "Show All Request";
				showAllRequestButton.tabFocusEnabled = false;
				showAllRequestButton.focusEnabled = false;
				showAllRequestButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(showAllRequestButton);
			}
		}
		
		public function set resumeFunc(resumeFunc:Function):void {
			if (resumeButton == null) {
				this._resumeFunc = resumeFunc;
				resumeButton = new LinkButton();
				resumeButton.label = "Resume";
				resumeButton.tabFocusEnabled = false;
				resumeButton.focusEnabled = false;
				resumeButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(resumeButton);
			}
		}
		
		public function set restoreWarningCodeFunc(restoreWarningCodeFunc:Function):void {
			if (restoreWarningCodeButton == null) {
				this._restoreWarningCodeFunc = restoreWarningCodeFunc;
				restoreWarningCodeButton = new LinkButton();
				restoreWarningCodeButton.label = "Restore Warning Code";
				restoreWarningCodeButton.tabFocusEnabled = false;
				restoreWarningCodeButton.focusEnabled = false;
				restoreWarningCodeButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(restoreWarningCodeButton);
			}
		}
		
		public function set refreshItemCountFunc(refreshItemCountFunc:Function):void {
			if (refreshItemCountButton == null) {
				this._refreshItemCountFunc = refreshItemCountFunc;
				refreshItemCountButton = new LinkButton();
				refreshItemCountButton.label = "Refresh Item Count";
				refreshItemCountButton.tabFocusEnabled = false;
				refreshItemCountButton.focusEnabled = false;
				refreshItemCountButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(refreshItemCountButton);
			}
		}
		
		public function set refreshScheduleListFunc(refreshScheduleListFunc:Function):void {
			if (refreshScheduleListButton == null) {
				this._refreshScheduleListFunc = refreshScheduleListFunc;
				refreshScheduleListButton = new LinkButton();
				refreshScheduleListButton.label = "Refresh Schedule List";
				refreshScheduleListButton.tabFocusEnabled = false;
				refreshScheduleListButton.focusEnabled = false;
				refreshScheduleListButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(refreshScheduleListButton);
			}
		}
		
		public function set changeTemplateFunc(changeTemplateFunc:Function):void {
			if (changeTemplateButton == null) {
				this._changeTemplateFunc = changeTemplateFunc;
				changeTemplateButton = new LinkButton();
				changeTemplateButton.label = "Change Template";
				changeTemplateButton.tabFocusEnabled = false;
				changeTemplateButton.focusEnabled = false;
				changeTemplateButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(changeTemplateButton);
			}
		}
		
		public function set newOrderListFunc(newOrderListFunc:Function):void {
			if (newOrderListButton == null) {
				this._newOrderListFunc = newOrderListFunc;
				newOrderListButton = new LinkButton();
				newOrderListButton.label = "New Order List";
				newOrderListButton.tabFocusEnabled = false;
				newOrderListButton.focusEnabled = false;
				newOrderListButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(newOrderListButton);
			}
		}
		
		public function set followUpListFunc(followUpListFunc:Function):void {
			if (followUpListButton == null) {
				this._followUpListFunc = followUpListFunc;
				followUpListButton = new LinkButton();
				followUpListButton.label = "Follow Up List";
				followUpListButton.tabFocusEnabled = false;
				followUpListButton.focusEnabled = false;
				followUpListButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(followUpListButton);
			}
		}
		
		public function set assignBatchPrintFunc(assignBatchPrintFunc:Function):void {
			if (assignBatchPrintButton == null) {
				this._assignBatchPrintFunc = assignBatchPrintFunc;
				assignBatchPrintButton = new LinkButton();
				assignBatchPrintButton.label = "Assign Batch & Print";
				assignBatchPrintButton.tabFocusEnabled = false;
				assignBatchPrintButton.focusEnabled = false;
				assignBatchPrintButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(assignBatchPrintButton);
			}
		}
		
		public function set reprintDeliveryLabelFunc(reprintDeliveryLabelFunc:Function):void {
			if (reprintDeliveryLabelButton == null) {
				this._reprintDeliveryLabelFunc = reprintDeliveryLabelFunc;
				reprintDeliveryLabelButton = new LinkButton();
				reprintDeliveryLabelButton.label = "Reprint Delivery Label";
				reprintDeliveryLabelButton.tabFocusEnabled = false;
				reprintDeliveryLabelButton.focusEnabled = false;
				reprintDeliveryLabelButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(reprintDeliveryLabelButton);
			}
		}
		
		public function set reprintDividerLabelFunc(reprintDividerLabelFunc:Function):void {
			if (reprintDividerLabelButton == null) {
				this._reprintDividerLabelFunc = reprintDividerLabelFunc;
				reprintDividerLabelButton = new LinkButton();
				reprintDividerLabelButton.label = "Reprint Divider Label";
				reprintDividerLabelButton.tabFocusEnabled = false;
				reprintDividerLabelButton.focusEnabled = false;
				reprintDividerLabelButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(reprintDividerLabelButton);
			}
		}
		
		public function set assignBatchFunc(assignBatchFunc:Function):void {
			if (assignBatchButton == null) {
				this._assignBatchFunc = assignBatchFunc;
				assignBatchButton = new LinkButton();
				assignBatchButton.label = "Assign Batch";
				assignBatchButton.tabFocusEnabled = false;
				assignBatchButton.focusEnabled = false;
				assignBatchButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(assignBatchButton);
			}
		}
		
		public function set filterFunc(filterFunc:Function):void {
			if (filterButton == null) {
				this._filterFunc = filterFunc;
				filterButton = new LinkButton();
				filterButton.label = "Filter";
				filterButton.tabFocusEnabled = false;
				filterButton.focusEnabled = false;
				filterButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(filterButton);
			}
		}
		
		public function set closeFunc(closeFunc:Function):void {
			if (closeButton == null) {
				this._closeFunc = closeFunc;
				closeButton = new LinkButton();
				closeButton.label = "Close";
				closeButton.tabFocusEnabled = false;
				closeButton.focusEnabled = false;
				closeButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(closeButton);
			}
		}
		
		public function set batchSendFunc(batchSendFunc:Function):void {
			if (batchSendButton == null) {
				this._batchSendFunc = batchSendFunc;
				batchSendButton = new LinkButton();
				batchSendButton.label = "Batch Send";
				batchSendButton.tabFocusEnabled = false;
				batchSendButton.focusEnabled = false;
				batchSendButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(batchSendButton);
			}
		}
		
		public function itemClickHandler(evt:MouseEvent):void {
			switch (evt.currentTarget.label) {
				case "Add to Worklist":
				case "Back to One Stop Entry":
				case "Back to Drug Assembling":
				case "Back to Drug Checking & Issuing":
				case "Back to Inbox":
				case "Get a List":
				case "Assign Batch & Print":
				case "Save & Print":
					operationName = "_" + (evt.currentTarget.label).substring(0,1).toLowerCase()
					+ (evt.currentTarget.label).substring(1, (evt.currentTarget.label).indexOf(" ") + 1)
					+ (evt.currentTarget.label).substring((evt.currentTarget.label).indexOf(" ")+1, (evt.currentTarget.label).indexOf(" ")+2).toUpperCase()
					+ (evt.currentTarget.label).substring((evt.currentTarget.label).indexOf(" ")+2);
					operationName = operationName.split("&").join("");
					operationName = operationName.split(" ").join("");
					break;
				case "Show LED":
				case "Enquire CDDH":
					operationName = "_" + (evt.currentTarget.label).substring(0,1).toLowerCase()
					+ (evt.currentTarget.label).substring(1, (evt.currentTarget.label).indexOf(" ") + 2)
					+ (evt.currentTarget.label).substring((evt.currentTarget.label).indexOf(" ")+2).toLowerCase(); 
					operationName = operationName = operationName.split(" ").join("");
					break;
				default:
					operationName = "_" + (evt.currentTarget.label).substring(0,1).toLowerCase() + (evt.currentTarget.label).substring(1);
					operationName = operationName.split("&").join("");
					operationName = operationName.split("-").join("");
					operationName = operationName.split(".").join("");
					operationName = operationName.split(" ").join("");
					break;	
			}
			
			if ((operationName == "_save" && _saveYesFunc != null) || 
				operationName == "_savePrint" || operationName == "_delete" || operationName == "_notMap") {
				var navigatorContent:ExtendedNavigatorContent = findParent(parent);
				if (!PopupUtil.isAnyVisiblePopupExists())
				{
					confirmationMessagePopup(operationName);
				}
			} else {				
				this[operationName + "Func"]();
			}
		}
		
		private function confirmationMessagePopup(operationName:String):void {
			var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
			if (operationName == "_save" || operationName == "_savePrint") {
				if (_saveMsg != null) {
					msgProp.displayDesc = _saveMsg;
				} else {
					msgProp.displayDesc = defaultSaveMsg;
				}
				
				if (!isNaN(_saveMsgPopupWidth) && _saveMsgPopupWidth != 0)
				{
					msgProp.messageWinWidth = _saveMsgPopupWidth;
				}
				
				if (!isNaN(_saveMsgPopupHeight) && _saveMsgPopupHeight != 0)
				{
					msgProp.messageWinHeight = _saveMsgPopupHeight;
				}
				
				msgProp.messageTitle = "Save Confirmation";
			} else if (operationName == "_delete") {
				if (_deleteMsg != null) {
					msgProp.displayDesc = _deleteMsg;
				} else {
					msgProp.displayDesc = defaultDeleteMsg;
				}
				
				if (!isNaN(_delMsgPopupWidth) && _delMsgPopupWidth != 0)
				{
					msgProp.messageWinWidth = _delMsgPopupWidth;
				}
				
				if (!isNaN(_delMsgPopupHeight) && _delMsgPopupHeight != 0)
				{
					msgProp.messageWinHeight = _delMsgPopupHeight;
				}
				
				msgProp.messageTitle = "Delete Confirmation";
			}
			else if (operationName == "_notMap") {
				msgProp.displayDesc = defaultNotMapMsg;
				msgProp.messageTitle = "Not Map Confirmation";
			}
			
			msgProp.setYesNoButton = true;
			msgProp.setYesDefaultButton = false;
			msgProp.yesHandler = confirmHandler;
			msgProp.noHandler = cancelHandler;
			dispatchEvent(new ShowSystemMessagePopupEvent(msgProp));
		}
		
		private function confirmHandler(evt:MouseEvent):void {
			closePopupHandler(evt);
			if (operationName == "_save") {
				_saveYesFunc();
			} else if (operationName == "_savePrint") {
				_savePrintFunc();
			} else if (operationName == "_delete") {
				_deleteYesFunc();	
			} else if (operationName == "_notMap") {
				_notMapFunc();	
			}
		}
		
		private function cancelHandler(evt:MouseEvent):void {
			closePopupHandler(evt);
			if (operationName == "_save") {
				if (_saveNoFunc != null) {
					_saveNoFunc();
				} else {
					resetToLastFocus();
				}
			} else {
				if (_deleteNoFunc != null) {
					_deleteNoFunc();
				} else {
					resetToLastFocus();
				}
			}
		}
		
		private function resetToLastFocus():void {
			var navigatorContent:ExtendedNavigatorContent = findParent(parent);
			if (navigatorContent != null) {
				navigatorContent.resetToLastFocus();
			}
		}
		
		private function findParent(component:DisplayObject):ExtendedNavigatorContent {
			
			if (component == null || component is ExtendedNavigatorContent) {
				return component as ExtendedNavigatorContent; 
			} else {
				return findParent(component.parent);
			}
		}
		
		private function closePopupHandler(evt:MouseEvent):void {
			var temp:UIComponent = evt.currentTarget as UIComponent; 
			PopUpManager.removePopUp(temp.parentDocument as IFlexDisplayObject);
			stage.focus = this;
		}
	}
}