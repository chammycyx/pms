package hk.org.ha.control.pms.main.reftable.mp
{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.reftable.mp.RefreshMultiDoseConversionViewEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveMultiDoseConvHandlerEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveMultiDoseConvSetListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.UpdateMultiDoseConvSetListEvent;
	import hk.org.ha.model.pms.biz.reftable.mp.MultiDoseConvListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("multiDoseConvListServiceCtl", restrict="true")]
	public class MultiDoseConvListServiceCtl
	{	
		[In]
		public var multiDoseConvListService:MultiDoseConvListServiceBean;

		private var callBackEvent:Event;
		
		[Observer]
		public function retrieveMultiDoseConvSetList(evt:RetrieveMultiDoseConvSetListEvent):void
		{
			In(Object(multiDoseConvListService).retrieveSuccess)
			multiDoseConvListService.retrievePmsMultiDoseConvSetList(evt.dmDrug, retrieveMultiDoseConvSetListResult);
		}
		
		private function retrieveMultiDoseConvSetListResult(evt:TideResultEvent):void
		{	
			evt.context.dispatchEvent( new RetrieveMultiDoseConvHandlerEvent(evt.result as ArrayCollection) );
		}
		
		[Observer]      
		public function updateMultiDoseConvSetList(evt:UpdateMultiDoseConvSetListEvent):void
		{
			In(Object(multiDoseConvListService).saveSuccess);
			In(Object(multiDoseConvListService).errorCode);
			In(Object(multiDoseConvListService).errorParam);
			multiDoseConvListService.updatePmsMultiDoseConvSetList(evt.pmsMultiDoseConvSetList, evt.dmDrug, updateMultiDoseConvSetListResult);
		}
		
		private function updateMultiDoseConvSetListResult(evt:TideResultEvent):void{
			evt.context.dispatchEvent(new RefreshMultiDoseConversionViewEvent(
				Object(multiDoseConvListService).saveSuccess,
				Object(multiDoseConvListService).errorCode,
				Object(multiDoseConvListService).errorParam ));
			
		}
	}
}