package hk.org.ha.control.pms.main.reftable {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.reftable.RetrieveWardStockMapEvent;
	import hk.org.ha.model.pms.biz.reftable.WardStockServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("wardStockServiceCtl", restrict="true")]
	public class WardStockServiceCtl 
	{
		[In]
		public var wardStockService:WardStockServiceBean
		
		private var callBackFunction:Function;
		
		[Observer]
		public function retrieveWardStockMap(evt:RetrieveWardStockMapEvent):void
		{
			callBackFunction = evt.callBackFunction;
			wardStockService.retrieveWardStockMap(retrieveWardStockMapResult);
		}
		
		private function retrieveWardStockMapResult(evt:TideResultEvent):void 
		{
			if ( callBackFunction != null ) {
				callBackFunction.call(null,evt.result);
			}			
		}
	}
}
