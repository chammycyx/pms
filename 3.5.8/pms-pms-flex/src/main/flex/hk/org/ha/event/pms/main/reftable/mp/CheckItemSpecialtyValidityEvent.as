package hk.org.ha.event.pms.main.reftable.mp {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CheckItemSpecialtyValidityEvent extends AbstractTideEvent 
	{	
		private var _itemCode:String;
		
		private var _chargeSpecCode:String;
		
		private var _callBack:Function;
		
		public function CheckItemSpecialtyValidityEvent(itemCodeValue:String, specCodeValue:String, functionValue:Function):void 
		{
			super();
			_itemCode = itemCodeValue;
			_chargeSpecCode = specCodeValue;
			_callBack = functionValue;
		}
		
		public function get itemCode():String
		{
			return _itemCode;
		}
		
		public function get chargeSpecCode():String
		{
			return _chargeSpecCode;
		}
		
		public function get callBack():Function
		{
			return _callBack;
		}
	}
}