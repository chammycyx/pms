package hk.org.ha.event.pms.main.enquiry
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDrugSetListEvent extends AbstractTideEvent 
	{	
		private var _patHospCode:String;
		private var _getFirstDrugSet:Boolean;
		
		public function RetrieveDrugSetListEvent(patHospCode:String, getFirstDrugSet:Boolean=false):void 
		{
			super();
			_patHospCode = patHospCode;
			_getFirstDrugSet = getFirstDrugSet;
		}
		
		public function get patHospCode():String {
			return _patHospCode;
		}
		
		public function get getFirstDrugSet():Boolean {
			return _getFirstDrugSet;
		}
	}
}