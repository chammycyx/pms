package hk.org.ha.control.pms.main.checkissue.mp{
	
	import hk.org.ha.event.pms.main.checkissue.mp.RetrieveDrugCheckViewListEvent;
	import hk.org.ha.event.pms.main.checkissue.mp.SetDrugCheckViewListSummaryEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.ShowLoadingPopupEvent;
	import hk.org.ha.model.pms.biz.checkissue.mp.CheckListServiceBean;
	import hk.org.ha.model.pms.vo.checkissue.mp.DrugCheckViewListSummary;
	import hk.org.ha.view.pms.main.checkissue.mp.DrugCheckView;
	
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("checkListServiceCtl", restrict="true")]
	public class CheckListServiceCtl 
	{
		[In]
		public var ctx:Context;
		
		[In]
		public var checkListService:CheckListServiceBean;
		
		[Observer]
		public function retrieveDrugCheckViewListSummary(evt:RetrieveDrugCheckViewListEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Refreshing ..."));
			checkListService.retrieveDrugCheckViewListSummary(retrieveDrugCheckViewListSummaryResult, retrieveDrugCheckViewListSummaryFault);
		}
		
		private function retrieveDrugCheckViewListSummaryResult(evt:TideResultEvent):void
		{
			dispatchEvent( new SetDrugCheckViewListSummaryEvent(evt.result as DrugCheckViewListSummary));
			
			var drugCheckView:DrugCheckView = ctx.drugCheckView as DrugCheckView;
			if (drugCheckView != null) {
				drugCheckView.setRefreshingFlag(false);
			}
			dispatchEvent(new CloseLoadingPopupEvent());
		}
		
		private function retrieveDrugCheckViewListSummaryFault(evt:TideFaultEvent):void
		{
			var drugCheckView:DrugCheckView = ctx.drugCheckView as DrugCheckView;
			if (drugCheckView != null) {
				drugCheckView.setRefreshingFlag(false);
			}
			dispatchEvent(new CloseLoadingPopupEvent());
		}
	}
}
