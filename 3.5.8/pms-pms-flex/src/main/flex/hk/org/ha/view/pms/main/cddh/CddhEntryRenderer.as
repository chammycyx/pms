package hk.org.ha.view.pms.main.cddh
{
  import flexlib.scheduling.scheduleClasses.IScheduleEntry;
  import flexlib.scheduling.scheduleClasses.SimpleScheduleEntry;
  import flexlib.scheduling.scheduleClasses.renderers.ColoredSolidScheduleEntryRenderer;

  import flexlib.scheduling.scheduleClasses.renderers.AbstractSolidScheduleEntryRenderer;
  
  import mx.formatters.DateFormatter;
  import hk.org.ha.view.pms.main.cddh.CddhSimpleScheduleEntry;

  public class CddhEntryRenderer extends AbstractSolidScheduleEntryRenderer
  {
	  
	private var formatter:DateFormatter = new DateFormatter();
	
	public function CddhEntryRenderer()
    {
            super();
    }
	
    override public function set data(value:Object):void
    {
      super.data = value;
	  
      entry = value as IScheduleEntry;
      var content:CddhSimpleScheduleEntry = CddhSimpleScheduleEntry(entry);
      setCddhEntryContent(content);
    }
	
	protected function setCddhEntryContent(content:CddhSimpleScheduleEntry):void
	{
		toolTip = content.hospCode + " / " + content.specCode;
		setStyle("backgroundColor", content.backgroundColor);
	}
  }
}