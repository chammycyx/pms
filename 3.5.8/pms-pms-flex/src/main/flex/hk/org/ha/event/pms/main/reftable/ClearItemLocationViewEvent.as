package hk.org.ha.event.pms.main.reftable {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ClearItemLocationViewEvent extends AbstractTideEvent 
	{
		private var _save:Boolean;
		private var _binNum:String;
		
		public function ClearItemLocationViewEvent(save:Boolean, binNum:String=null):void 
		{
			super();
			_save = save;
			_binNum = binNum;
		}
		
		public function get save():Boolean
		{
			return _save;
		}
		
		public function get binNum():String
		{
			return _binNum;
		}
	}
}