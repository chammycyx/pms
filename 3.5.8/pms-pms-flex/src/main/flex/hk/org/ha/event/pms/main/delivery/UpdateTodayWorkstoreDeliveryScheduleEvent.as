package hk.org.ha.event.pms.main.delivery
{
	import hk.org.ha.model.pms.persistence.medprofile.DeliverySchedule;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateTodayWorkstoreDeliveryScheduleEvent extends AbstractTideEvent
	{
		private var _deliverySchedule:DeliverySchedule;
		
		public function UpdateTodayWorkstoreDeliveryScheduleEvent(deliverySchedule:DeliverySchedule)
		{
			_deliverySchedule = deliverySchedule;
		}
		
		public function get deliverySchedule():DeliverySchedule
		{
			return _deliverySchedule;
		}
	}
}