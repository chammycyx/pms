package hk.org.ha.event.pms.main.alert.druginfo.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dms.vo.DrugMds;
	import hk.org.ha.model.pms.dms.vo.PreparationProperty;
	import hk.org.ha.model.pms.vo.alert.mds.DrugLog;
	
	import mx.collections.ListCollectionView;
	
	public class ShowDrugInfoPopupEvent extends AbstractTideEvent {

		private var _gcnSeqNum:String;
		
		private var _rdfgenId:String;
		
		private var _rgenId:String;
		
		private var _displayNameSaltForm:String;
		
		private var _strengthVolume:String;
		
		private var _patHospCode:String;
		
		private var _preparationPropertyList:ListCollectionView;
		
		private var _selectedStrengthIndex:Number;
		
		private var _dob:Date;
		
		public function ShowDrugInfoPopupEvent(gcnSeqNum:String, rdfgenId:String, rgenId:String, displayNameSaltForm:String, strengthVolume:String, patHospCode:String, preparationPropertyList:ListCollectionView, selectedStrengthIndex:Number=NaN, dob:Date=null) {
			super();
			_gcnSeqNum = gcnSeqNum;
			_rdfgenId = rdfgenId;
			_rgenId = rgenId;
			_displayNameSaltForm = displayNameSaltForm;
			_strengthVolume = strengthVolume;
			_patHospCode = patHospCode;
			_preparationPropertyList = preparationPropertyList;
			_selectedStrengthIndex = selectedStrengthIndex;
			_dob = dob;
		}
		
		public function get displayNameSaltForm():String {
			return _displayNameSaltForm;
		}
		
		public function get strengthVolume():String {
			return _strengthVolume;
		}
		
		public function get patHospCode():String {
			return _patHospCode;
		}
		
		public function get preparationPropertyList():ListCollectionView {
			return _preparationPropertyList;
		}
		
		public function get selectedStrengthIndex():Number {
			return _selectedStrengthIndex;
		}
		
		public function get dob():Date {
			return _dob;
		}
		
		public function get drugLog():DrugLog {
			var drugLog:DrugLog = new DrugLog();
			drugLog.gcnSeqNum = _gcnSeqNum
			drugLog.rdfgenId = _rdfgenId;
			drugLog.rgenId = _rgenId;
			return drugLog;
		}
	}
}
