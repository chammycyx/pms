package hk.org.ha.event.pms.main.pivas
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePivasDueOrderListEvent extends AbstractTideEvent 
	{		
		private var _supplyDate:Date;
		
		private var _callbackFunc:Function;
		
		public function RetrievePivasDueOrderListEvent(supplyDate:Date, callbackFunc:Function):void 
		{
			super();
			_supplyDate = supplyDate;
			_callbackFunc = callbackFunc;
		}

		public function get supplyDate():Date {
			return _supplyDate;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}