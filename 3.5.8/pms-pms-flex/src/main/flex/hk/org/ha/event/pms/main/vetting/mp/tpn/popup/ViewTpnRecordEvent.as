package hk.org.ha.event.pms.main.vetting.mp.tpn.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ViewTpnRecordEvent extends AbstractTideEvent
	{
		private var _selectedIndex:Number;
		
		private var _callback:Function;
		
		public function ViewTpnRecordEvent(selectedIndex:Number, callback:Function):void {
			super();
			_selectedIndex = selectedIndex;
			_callback = callback;
		}
		
		public function get selectedIndex():Number {
			return _selectedIndex;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}