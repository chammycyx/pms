package hk.org.ha.event.pms.main.reftable {
	
	import hk.org.ha.model.pms.vo.reftable.RefillSelectionInfo;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetRefillSelectionItemListEvent extends AbstractTideEvent 
	{
		private var _refillSelectionInfo:RefillSelectionInfo;
		
		public function SetRefillSelectionItemListEvent(refillSelectionInfo:RefillSelectionInfo):void 
		{
			super();
			_refillSelectionInfo = refillSelectionInfo;
		}
		
		public function get refillSelectionInfo():RefillSelectionInfo
		{
			return _refillSelectionInfo;
		}
	}
}