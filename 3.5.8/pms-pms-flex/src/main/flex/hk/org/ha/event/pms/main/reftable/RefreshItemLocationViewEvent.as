package hk.org.ha.event.pms.main.reftable {
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshItemLocationViewEvent extends AbstractTideEvent 
	{
		private var _itemLocationList:ArrayCollection;
		public function RefreshItemLocationViewEvent(itemLocationList:ArrayCollection):void 
		{
			super();
			_itemLocationList = itemLocationList;
		}
		
		public function get itemLocationList():ArrayCollection {
			return _itemLocationList;
		}
	}
}