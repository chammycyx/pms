package hk.org.ha.event.pms.main.report {
	
	import hk.org.ha.model.pms.vo.report.DrugRefillListRptInfo;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDrugRefillListRptEvent extends AbstractTideEvent
	{
		private var _drugRefillRptInfo:DrugRefillListRptInfo;
		
		public function RetrieveDrugRefillListRptEvent(drugRefillRptInfo:DrugRefillListRptInfo):void 
		{
			super();
			_drugRefillRptInfo = drugRefillRptInfo;
		}
		
		public function get drugRefillRptInfo():DrugRefillListRptInfo {
			return _drugRefillRptInfo;
		}
	}
}