package hk.org.ha.control.pms.main.enquiry {
	
	import hk.org.ha.event.pms.main.enquiry.RefreshDrugSetEnqViewEvent;
	import hk.org.ha.event.pms.main.enquiry.ResumeDrugSetEnqAccessRightEvent;
	import hk.org.ha.event.pms.main.enquiry.RetrieveDrugSetDtlListEvent;
	import hk.org.ha.event.pms.main.enquiry.RefreshDrugSetHospCodeListEvent;
	import hk.org.ha.event.pms.main.enquiry.RetrieveDrugSetListEvent;
	import hk.org.ha.event.pms.main.enquiry.RetrieveHospitalMappingListEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.model.pms.biz.enquiry.DrugSetListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.events.TideResultEvent;
	

	[Bindable]
	[Name("drugSetListServiceCtl", restrict="true")]
	public class DrugSetListServiceCtl 
	{
		[In]
		public var drugSetHospitalMappingList:ArrayCollection;

		[In]
		public var drugSetListService:DrugSetListServiceBean;
		
		private var subLevelFlag:Boolean;
		
		[Observer]
		public function retrieveHospitalMappingList(evt:RetrieveHospitalMappingListEvent):void
		{			
			drugSetListService.retrieveHospitalMappingList(retrieveHospitalMappingListResult, closeLoadingPopup);
		}
		
		private function retrieveHospitalMappingListResult(evt:TideResultEvent):void {
			if (drugSetHospitalMappingList != null && drugSetHospitalMappingList.length > 0) {
				evt.context.dispatchEvent(new RefreshDrugSetHospCodeListEvent());
			} else {
				refreshDrugSetEnqView(evt);
			}
		}
		
		[Observer]
		public function retrieveDrugSetList(evt:RetrieveDrugSetListEvent):void
		{			
			drugSetListService.retrieveDrugSetList(evt.patHospCode, evt.getFirstDrugSet, refreshDrugSetEnqView, closeLoadingPopup);
		}
		
		[Observer]
		public function retrieveDrugSetDtlList(evt:RetrieveDrugSetDtlListEvent):void
		{		
			subLevelFlag = evt.subLevelFlag;
			drugSetListService.retrieveDrugSetDtlList(evt.patHospCode, evt.drugSetNum, evt.drugSetCode, evt.drugHospCode, evt.subLevelFlag, refreshDrugSetEnqView, closeLoadingPopup);
		}
		
		private function refreshDrugSetEnqView(evt:TideResultEvent):void{
			evt.context.dispatchEvent( new CloseLoadingPopupEvent() );
			evt.context.dispatchEvent( new RefreshDrugSetEnqViewEvent(subLevelFlag));
		}
		
		private function closeLoadingPopup(evt:TideFaultEvent):void{
			evt.context.dispatchEvent( new CloseLoadingPopupEvent() );
			evt.context.dispatchEvent( new ResumeDrugSetEnqAccessRightEvent() );
		}
	}
}
