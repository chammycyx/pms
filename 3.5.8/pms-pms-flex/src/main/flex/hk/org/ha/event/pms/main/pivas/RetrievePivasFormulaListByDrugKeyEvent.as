package hk.org.ha.event.pms.main.pivas
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePivasFormulaListByDrugKeyEvent extends AbstractTideEvent 
	{		
		private var _patHospCode:String;

		private var _pasWardCode:String;
		
		private var _drugKey:Number;
		
		private var _dosageUnit:String;

		private var _siteCode:String;
		
		private var _diluentCode:String;

		private var _strength:String;
		
		private var _callbackFunc:Function;
		
		public function RetrievePivasFormulaListByDrugKeyEvent():void 
		{
			super();
		}
		
		public function get patHospCode():String {
			return _patHospCode;
		}

		public function set patHospCode(patHospCode:String):void {
			_patHospCode = patHospCode;
		}
		
		public function get pasWardCode():String {
			return _pasWardCode;
		}
		
		public function set pasWardCode(pasWardCode:String):void {
			_pasWardCode = pasWardCode;
		}

		public function get drugKey():Number {
			return _drugKey;
		}
		
		public function set drugKey(drugKey:Number):void {
			_drugKey = drugKey;
		}

		public function get dosageUnit():String {
			return _dosageUnit;
		}
		
		public function set dosageUnit(dosageUnit:String):void {
			_dosageUnit = dosageUnit;
		}
		
		public function get siteCode():String {
			return _siteCode;
		}
		
		public function set siteCode(siteCode:String):void {
			_siteCode = siteCode;
		}
		
		public function get diluentCode():String {
			return _diluentCode;
		}
		
		public function set diluentCode(diluentCode:String):void {
			_diluentCode = diluentCode;
		}
		
		public function get strength():String {
			return _strength;
		}
		
		public function set strength(strength:String):void {
			_strength = strength;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
		
		public function set callbackFunc(callbackFunc:Function):void {
			_callbackFunc = callbackFunc;
		}
	}
}