package hk.org.ha.event.pms.main.vetting.popup {
	
	import hk.org.ha.model.pms.dms.persistence.DmDailyFrequency;	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDailyFreqPopupEvent extends AbstractTideEvent 
	{		
		private var _dmDailyFrequency:DmDailyFrequency;								
		
		private var _okHandler:Function;
		
		private var _cancelHandler:Function;
		
		public function ShowDailyFreqPopupEvent(dmDailyFrequency:DmDailyFrequency, okHandlerVal:Function, cancelHandlerVal:Function):void 
		{
			super();
			_dmDailyFrequency = dmDailyFrequency;						
			_okHandler = okHandlerVal;
			_cancelHandler = cancelHandlerVal;
		}
		
		public function get dmDailyFrequency():DmDailyFrequency {
			return _dmDailyFrequency;
		}
		
		public function get okHandler():Function {
			return _okHandler;
		}
		
		public function get cancelHandler():Function {
			return _cancelHandler;
		}
	}
}
