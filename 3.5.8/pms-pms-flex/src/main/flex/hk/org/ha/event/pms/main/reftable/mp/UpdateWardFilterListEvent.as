package hk.org.ha.event.pms.main.reftable.mp {
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateWardFilterListEvent extends AbstractTideEvent 
	{
		private var _wardFilterList:ListCollectionView;
		
		public function UpdateWardFilterListEvent(wardFilterList:ListCollectionView):void 
		{
			super();
			_wardFilterList = wardFilterList;
		}
		
		public function get wardFilterList():ListCollectionView
		{
			return _wardFilterList;
		}
	}
}