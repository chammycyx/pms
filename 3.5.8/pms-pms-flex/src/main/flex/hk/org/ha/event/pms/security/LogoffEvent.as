package hk.org.ha.event.pms.security {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class LogoffEvent extends AbstractTideEvent 
	{
		public function LogoffEvent():void {
			super();
		}	
	}
}