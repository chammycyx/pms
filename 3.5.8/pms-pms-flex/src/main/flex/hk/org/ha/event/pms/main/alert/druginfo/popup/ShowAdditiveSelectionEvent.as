package hk.org.ha.event.pms.main.alert.druginfo.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	import mx.collections.ListCollectionView;
	
	public class ShowAdditiveSelectionEvent extends AbstractTideEvent {
		
		private var _rxDrugList:ListCollectionView;
		
		private var _callback:Function;
		
		public function ShowAdditiveSelectionEvent(rxDrugList:ListCollectionView, callback:Function) {
			super();
			_rxDrugList = rxDrugList;
			_callback = callback;
		}
		
		public function get rxDrugList():ListCollectionView {
			return _rxDrugList;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}