package hk.org.ha.event.pms.main.drug {

	import flash.events.Event;
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.rx.RxItem;
	
	public class RetrieveWorkstoreDrugListByRxItemEvent extends AbstractTideEvent {		
		
		private var _rxItem:RxItem;
		private var _displayNameOnly:Boolean;
		private var _callBackEvent:Event;				
		
		public function RetrieveWorkstoreDrugListByRxItemEvent(rxItemVal:RxItem, displayNameOnlyVal:Boolean, event:Event=null):void
		{
			super();
			_rxItem 		 = rxItemVal;
			_displayNameOnly = displayNameOnlyVal;
			_callBackEvent 	 = event;			
		}		
		
		public function get rxItem():RxItem
		{
			return _rxItem;
		}
		
		public function get displayNameOnly():Boolean
		{
			return _displayNameOnly;
		}
		
		public function get callBackEvent():Event
		{
			return _callBackEvent;
		}
	}
}