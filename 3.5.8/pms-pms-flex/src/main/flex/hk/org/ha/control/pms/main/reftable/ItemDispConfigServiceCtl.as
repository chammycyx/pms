package hk.org.ha.control.pms.main.reftable {		
	
	import hk.org.ha.event.pms.main.reftable.RetrieveItemDispConfigEvent;
	import hk.org.ha.model.pms.biz.reftable.ItemDispConfigServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("itemDispConfigServiceCtl", restrict="true")]
	public class ItemDispConfigServiceCtl 
	{
		[In]
		public var itemDispConfigService:ItemDispConfigServiceBean;
		
		private var callBack:Function;
		
		[Observer]
		public function retrieveItemDispConfig(evt:RetrieveItemDispConfigEvent):void 
		{
			callBack = evt.callBack;
			itemDispConfigService.retrieveItemDispConfig(evt.itemCode,retrieveItemDispConfigResult);
		}
		
		private function retrieveItemDispConfigResult(evt:TideResultEvent):void 
		{
			if ( callBack != null ) {
				callBack(evt.result);
			}
		}
	}
}
