package hk.org.ha.control.pms.main.reftable {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.reftable.RefreshUpdateSuspendReasonListEvent;
	import hk.org.ha.event.pms.main.reftable.RetrieveSuspendReasonListEvent;
	import hk.org.ha.event.pms.main.reftable.SetSuspendReasonListEditableEvent;
	import hk.org.ha.event.pms.main.reftable.UpdateSuspendReasonListEvent;
	import hk.org.ha.event.pms.main.reftable.rdr.ReceiveSuspendReasonRdrListEvent;
	import hk.org.ha.event.pms.main.reftable.rdr.RetrieveSuspendReasonRdrListEvent;
	import hk.org.ha.model.pms.biz.reftable.SuspendReasonListServiceBean;
	import hk.org.ha.model.pms.biz.reftable.rdr.SuspendReasonReaderServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("suspendReasonListServiceCtl", restrict="true")]
	public class SuspendReasonListServiceCtl 
	{
		[In]
		public var suspendReasonListService:SuspendReasonListServiceBean;
		
		[In]
		public var suspendReasonReaderService:SuspendReasonReaderServiceBean;
		
		private var retrieveSuspendReasonRdrListEvent:RetrieveSuspendReasonRdrListEvent;
		
		[Observer]
		public function retrieveSuspendReasonList(evt:RetrieveSuspendReasonListEvent):void
		{
			suspendReasonListService.retrieveSuspendReasonList(retrieveSuspendReasonListResult);
		}
		
		private function retrieveSuspendReasonListResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new SetSuspendReasonListEditableEvent());
		}
		
		[Observer]
		public function updateSuspendReasonList(evt:UpdateSuspendReasonListEvent):void
		{
			suspendReasonListService.updateSuspendReasonList(evt.suspendReasonList, updateSuspendReasonListResult);
		}
		
		private function updateSuspendReasonListResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new RefreshUpdateSuspendReasonListEvent());
		}
		
		[Observer]
		public function retrieveSuspendReasonRdrList(evt:RetrieveSuspendReasonRdrListEvent):void
		{
			suspendReasonReaderService.retrieveSuspendReasonList(retrieveSuspendReasonRdrListResult);
			retrieveSuspendReasonRdrListEvent = evt;
		}
		
		private function retrieveSuspendReasonRdrListResult(evt:TideResultEvent):void
		{
			if (retrieveSuspendReasonRdrListEvent.callbackFunc != null)
			{
				retrieveSuspendReasonRdrListEvent.callbackFunc();
			}
			
			//may remove as no observer can be found
			evt.context.dispatchEvent(new ReceiveSuspendReasonRdrListEvent());
		}
	}
}
