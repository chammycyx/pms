package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDmMedicalOfficerEvent extends AbstractTideEvent 
	{
		private var _medicalOfficerCode:String;
		private var _successEvent:Event;
		private var _failEvent:Event;
		
		public function RetrieveDmMedicalOfficerEvent(medicalOfficerCode:String, successEvent:Event=null, failEvent:Event=null):void 
		{
			super();
			_medicalOfficerCode = medicalOfficerCode;
			_successEvent = successEvent;
			_failEvent = failEvent;
		}
		
		public function get medicalOfficerCode():String
		{
			return _medicalOfficerCode;
		}
		
		public function get successEvent():Event
		{
			return _successEvent;
		}
		
		public function get failEvent():Event
		{
			return _failEvent;
		}
	}
}