package hk.org.ha.event.pms.main.medprofile
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class PrivilegeAuthenticateEvent extends AbstractTideEvent
	{
		private var _userId:String;
		private var _password:String;
		private var _permission:String;
		private var _callbackFunc:Function;
		
		public function PrivilegeAuthenticateEvent(userId:String, password:String, permission:String, callbackFunc:Function)
		{
			_userId = userId;
			_password = password;
			_permission = permission;
			_callbackFunc = callbackFunc;
		}
		
		public function get userId():String
		{
			return _userId;
		}
		
		public function get password():String
		{
			return _password;
		}
		
		public function get right():String
		{
			return _permission;
		}
		
		public function get callbackFunc():Function
		{
			return _callbackFunc;
		}
	}
}