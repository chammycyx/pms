package hk.org.ha.event.pms.main.inbox
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class NotifyMdsEvent extends AbstractTideEvent
	{
		private var _errorStatId:Number;
		private var _notified:Boolean;
		
		public function NotifyMdsEvent(errorStatId:Number, notified:Boolean)
		{
			_errorStatId = errorStatId; 
			_notified = notified;
		}
		
		public function get errorStatId():Number
		{
			return _errorStatId; 
		}
		
		public function get notified():Boolean
		{
			return _notified;
		}
	}
}