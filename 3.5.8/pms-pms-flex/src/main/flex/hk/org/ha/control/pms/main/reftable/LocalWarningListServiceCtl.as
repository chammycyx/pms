package hk.org.ha.control.pms.main.reftable {
	
	import hk.org.ha.event.pms.main.reftable.ClearLocalWarnCodeMaintViewEvent;
	import hk.org.ha.event.pms.main.reftable.ClearRestoreLocalWarnCodeMaintViewEvent;
	import hk.org.ha.event.pms.main.reftable.PrintLocalWarningRptEvent;
	import hk.org.ha.event.pms.main.reftable.RefreshLocalWarningCodeMaintValidateListEvent;
	import hk.org.ha.event.pms.main.reftable.RefreshLocalWarningCodeMaintViewEvent;
	import hk.org.ha.event.pms.main.reftable.RefreshPrintLocalWarningRptEvent;
	import hk.org.ha.event.pms.main.reftable.RetrieveDmWarningValidateListEvent;
	import hk.org.ha.event.pms.main.reftable.RetrieveLocalWarningListEvent;
	import hk.org.ha.event.pms.main.reftable.UpdateLocalWarningListEvent;
	import hk.org.ha.model.pms.biz.reftable.LocalWarningListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("localWarningListServiceCtl", restrict="true")]
	public class LocalWarningListServiceCtl 
	{
		[In]
		public var localWarningListService:LocalWarningListServiceBean;

		[Observer]
		public function retrieveDmWarningValidateList(evt:RetrieveDmWarningValidateListEvent):void
		{
			localWarningListService.retrieveDmWarningValidateList(retrieveDmWarningValidateListResult);
		}
		
		private function retrieveDmWarningValidateListResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshLocalWarningCodeMaintValidateListEvent(evt.result as ArrayCollection));
		}
		
		[Observer]
		public function retrieveLocalWarningList(evt:RetrieveLocalWarningListEvent):void
		{
			In(Object(localWarningListService).followUp)
			In(Object(localWarningListService).validItemCode)
			localWarningListService.retrieveLocalWarningList(evt.itemCode, retrieveLocalWarningListResult);
		}
		
		private function retrieveLocalWarningListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshLocalWarningCodeMaintViewEvent(Object(localWarningListService).validItemCode, Object(localWarningListService).followUp));
		}
		
		[Observer]
		public function updateLocalWarningList(evt:UpdateLocalWarningListEvent):void
		{
			if (!evt.restoreUpdate) {
				localWarningListService.updateLocalWarningList(evt.updateLocalWarningList, updateLocalWarningListResult);
			}else {
				localWarningListService.updateLocalWarningList(evt.updateLocalWarningList, updateRestoreLocalWarningListResult);
			}
			
		}
		
		private function updateLocalWarningListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new ClearLocalWarnCodeMaintViewEvent());
		}
		
		private function updateRestoreLocalWarningListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new ClearRestoreLocalWarnCodeMaintViewEvent());
		}

		[Observer]
		public function printLocalWarningRpt(evt:PrintLocalWarningRptEvent):void
		{
			In(Object(localWarningListService).validPrint)
			localWarningListService.printLocalWarningRpt(printLocalWarningRptResult);
		}
		
		private function printLocalWarningRptResult(evt:TideResultEvent):void 
		{
			if (! Object(localWarningListService).validPrint ) {
				evt.context.dispatchEvent(new RefreshPrintLocalWarningRptEvent());
			}
		}
	}
}
