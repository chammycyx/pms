package hk.org.ha.event.pms.main.reftable {
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateSuspendReasonListEvent extends AbstractTideEvent 
	{
		private var _suspendReasonList:ListCollectionView;
		
		public function UpdateSuspendReasonListEvent(suspendReasonList:ListCollectionView):void 
		{
			super();
			_suspendReasonList = suspendReasonList;
		}
		
		public function get suspendReasonList():ListCollectionView
		{
			return _suspendReasonList;
		}
	}
}