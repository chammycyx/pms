package hk.org.ha.event.pms.main.delivery
{
	import mx.collections.ArrayCollection;	
	import hk.org.ha.model.pms.persistence.medprofile.DeliverySchedule;
	import hk.org.ha.model.pms.persistence.medprofile.DeliveryScheduleItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class AssignBatchAndPrintByDeliveryScheduleEvent extends AbstractTideEvent
	{
		private var _scheduleItem:DeliveryScheduleItem;
		private var _deliveryRequestList:ArrayCollection;
		private var _callbackFunction:Function;
		
		public function AssignBatchAndPrintByDeliveryScheduleEvent(scheduleItem:DeliveryScheduleItem, requestList:ArrayCollection, callbackFunction:Function = null)
		{
			super();
			_scheduleItem = scheduleItem;
			_deliveryRequestList = requestList;
			_callbackFunction = callbackFunction;
		}
		
		public function get scheduleItem():DeliveryScheduleItem{
			return _scheduleItem;
		}
		
		public function get deliveryRequestList():ArrayCollection
		{
			return _deliveryRequestList;
		}		
		
		public function get callbackFunction():Function
		{
			return _callbackFunction;
		}
	}
}