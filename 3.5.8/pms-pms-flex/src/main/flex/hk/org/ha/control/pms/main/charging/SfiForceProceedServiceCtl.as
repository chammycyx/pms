package hk.org.ha.control.pms.main.charging {
	
	import hk.org.ha.event.pms.main.charging.popup.RefreshForceProceedChargeReasonListEvent;
	import hk.org.ha.event.pms.main.charging.popup.RefreshSfiForceProceedPopupEvent;
	import hk.org.ha.event.pms.main.charging.popup.RetrieveForceProceedChargeReasonListEvent;
	import hk.org.ha.event.pms.main.charging.popup.UpdateSfiForceProceedPopupEvent;
	import hk.org.ha.model.pms.biz.charging.SfiForceProceedServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	import org.granite.tide.seam.Context;

	[Bindable]
	[Name("sfiForceProceedServiceCtl", restrict="true")]
	public class SfiForceProceedServiceCtl 
	{
	
		[In]
		public var ctx:Context;
		
		[In]
		public var sfiForceProceedService:SfiForceProceedServiceBean;
		
		private var actionBy:String;
		
		private var sfiForceProceedConfirmCallback:Function;
		
		[Observer]
		public function retrieveForceProceedChargeReasonList(evt:RetrieveForceProceedChargeReasonListEvent):void
		{			
			ctx.meta_updates.removeAll();
			sfiForceProceedService.retrieveForceProceedChargeReasonList(retrieveForceProceedChargeReasonListResult);
		}
		
		public function retrieveForceProceedChargeReasonListResult(evt:TideResultEvent):void{
			evt.context.dispatchEvent( new RefreshForceProceedChargeReasonListEvent() );
		}
		
		[Observer]
		public function updateSfiForceProceed(evt:UpdateSfiForceProceedPopupEvent):void
		{	
			sfiForceProceedConfirmCallback = evt.sfiForceProceedConfirmCallback;
			
			ctx.meta_updates.removeAll();
			
			In(Object(sfiForceProceedService).errMsg)
			In(Object(sfiForceProceedService).errMsgDetail)
			sfiForceProceedService.validateSfiForceProceedEntry(evt.sfiForceProceedEntry,updateSfiForceProceedClearanceResult);
		}
		
		public function updateSfiForceProceedClearanceResult(evt:TideResultEvent):void{
			evt.context.dispatchEvent(new RefreshSfiForceProceedPopupEvent(Object(sfiForceProceedService).errMsg, Object(sfiForceProceedService).errMsgDetail, sfiForceProceedConfirmCallback));
		}
	}
}
