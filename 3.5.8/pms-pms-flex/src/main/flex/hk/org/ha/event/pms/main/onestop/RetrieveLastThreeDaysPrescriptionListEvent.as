package hk.org.ha.event.pms.main.onestop {
		
	import hk.org.ha.model.pms.persistence.disp.Patient;
	import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveLastThreeDaysPrescriptionListEvent extends AbstractTideEvent 
	{	
		private var _oneStopOrderType:OneStopOrderType;
		private var _orderId:Number;
		private var _hkid:String;
		private var _patient:Patient;
		private var _dispOrderId:Number;
		private var _showPreVetView:Boolean;
		private var _orderNum:String;
		private var _refillId:Number;
		private var _medOrderVersion:Number;
		
		public function RetrieveLastThreeDaysPrescriptionListEvent(oneStopOrderType:OneStopOrderType, orderId:Number, patient:Patient, 
																   dispOrderId:Number, showPreVetView:Boolean, orderNum:String=null,
																   refillId:Number=NaN, medOrderVersion:Number=NaN):void 
		{
			super();
			_oneStopOrderType = oneStopOrderType;
			_orderId = orderId;
			_patient = patient;
			_dispOrderId = dispOrderId;
			_showPreVetView = showPreVetView;
			_orderNum = orderNum;
			_patient = patient;
			_refillId = refillId;
			_medOrderVersion = medOrderVersion;
		}        
		
		public function get oneStopOrderType():OneStopOrderType
		{
			return _oneStopOrderType;
		}
		
		public function get orderId():Number
		{
			return _orderId;
		}	
		
		public function get hkid():String {
			if (_patient == null)
			{
				return null;
			}
			
			return _patient.hkid;
		}
		
		public function get patient():Patient {
			return _patient;
		}
		
		public function get dispOrderId():Number
		{
			return _dispOrderId;
		}
		
		public function get showPreVetView():Boolean {
			return _showPreVetView;
		}
		
		public function get orderNum():String {
			return _orderNum;
		}
				
		public function get refillId():Number {
			return _refillId;
		}
		
		public function get medOrderVersion():Number {
			return _medOrderVersion;
		}
	}
}