package hk.org.ha.event.pms.main.report {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshPrescRptEvent extends AbstractTideEvent 
	{	
		private var _retrieveSuccess:Boolean;
		
		private var _validHkid:Boolean;
		
		public function RefreshPrescRptEvent(retrieveSuccess:Boolean, validHkid:Boolean):void 
		{
			super();
			_retrieveSuccess = retrieveSuccess;
			_validHkid = validHkid;
		}
		
		public function get retrieveSuccess():Boolean
		{
			return _retrieveSuccess;
		}
		
		public function get validHkid():Boolean
		{
			return _validHkid;
		}
	}
}