package hk.org.ha.event.pms.main.picking
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetDrugPickSystemMessageEvent extends AbstractTideEvent 
	{		
		private var _errMsg:String;
		
		public function SetDrugPickSystemMessageEvent(errMsg:String):void 
		{
			super();
			_errMsg = errMsg;
		}
		
		public function get errMsg():String {
			return _errMsg;
		}
	}
}