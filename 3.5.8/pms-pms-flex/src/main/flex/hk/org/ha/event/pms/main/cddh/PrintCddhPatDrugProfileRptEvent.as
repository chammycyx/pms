package hk.org.ha.event.pms.main.cddh {

	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.report.CddhPatDrugProfileRpt;
	
	public class PrintCddhPatDrugProfileRptEvent extends AbstractTideEvent 
	{
		private var _cddhPatDrugProfileRpt:CddhPatDrugProfileRpt;
		public function PrintCddhPatDrugProfileRptEvent(cddhPatDrugProfileRpt:CddhPatDrugProfileRpt):void 
		{
			super();
			_cddhPatDrugProfileRpt = cddhPatDrugProfileRpt;
		}
		
		public function get cddhPatDrugProfileRpt():CddhPatDrugProfileRpt
		{
			return _cddhPatDrugProfileRpt;
		}
	}
}
