package hk.org.ha.event.pms.main.vetting.mp {
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CheckProfileHasLabelGenerationWithinToday extends AbstractTideEvent 
	{
		private var _callBack:Function;
		
		public function CheckProfileHasLabelGenerationWithinToday(callBack:Function):void 
		{
			super();
			_callBack = callBack;
		}
		
		public function get callBack():Function
		{
			return _callBack;
		}
	}
}