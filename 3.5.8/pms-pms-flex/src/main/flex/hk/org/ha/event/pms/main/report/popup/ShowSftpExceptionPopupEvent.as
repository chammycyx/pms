package hk.org.ha.event.pms.main.report.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowSftpExceptionPopupEvent extends AbstractTideEvent 
	{
		private var _exceptionClass:String;
		private var _msgCode:String;
		private var _errMsg:String;
		
		public function ShowSftpExceptionPopupEvent(msgCode:String, exceptionClass:String,
													errMsg:String=null):void 
		{
			super();
			_msgCode = msgCode;
			_exceptionClass = exceptionClass;
			_errMsg = errMsg;
		}
		
		public function get msgCode():String{
			return _msgCode;
		}
		
		public function get exceptionClass():String {
			return _exceptionClass;
		}
		
		public function get errMsg():String{
			return _errMsg;
		}
	}
}