package hk.org.ha.control.pms.exception
{
	import mx.messaging.messages.ErrorMessage;
	
	import org.granite.tide.BaseContext;
	import org.granite.tide.IExceptionHandler;
	
	public class DrugInfoExceptionHandler implements IExceptionHandler {
		
		public static const DRUG_INFO_EXCEPTION:String = "DrugInfoException";

		public function accepts(emsg:ErrorMessage):Boolean {
			return emsg.faultCode == DRUG_INFO_EXCEPTION;
		}
		
		public function handle(context:BaseContext, emsg:ErrorMessage):void {
		}
	}
}