package hk.org.ha.event.pms.main.drug.popup {
		
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveFreeTextEntryEvent extends AbstractTideEvent 
	{			
		private var _drugSearchSource:DrugSearchSource;		// null if event is triggered by order edit
		private var _freeTextEntryPopupProp:FreeTextEntryPopupProp;
		
		public function RetrieveFreeTextEntryEvent(freeTextEntryPopupProp:FreeTextEntryPopupProp, drugSearchSource:DrugSearchSource=null):void 
		{
			super();
			_drugSearchSource = drugSearchSource;
			_freeTextEntryPopupProp = freeTextEntryPopupProp;
		}
		
		public function get drugSearchSource():DrugSearchSource {
			return _drugSearchSource;
		}
		
		public function get freeTextEntryPopupProp():FreeTextEntryPopupProp{
			return _freeTextEntryPopupProp;
		}
	}
}