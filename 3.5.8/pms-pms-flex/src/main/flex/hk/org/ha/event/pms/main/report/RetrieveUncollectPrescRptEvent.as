package hk.org.ha.event.pms.main.report {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveUncollectPrescRptEvent extends AbstractTideEvent 
	{
		private var _dateRange:Number;
		
		public function RetrieveUncollectPrescRptEvent( dateRange:Number ):void 
		{
			super();
			_dateRange = dateRange;
		}

		public function get dateRange():Number{
			return _dateRange;
		} 
	}
}