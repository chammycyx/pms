package hk.org.ha.event.pms.main.sys
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveCurrentServerTimeEvent extends AbstractTideEvent
	{
		private var _callbackFunction:Function;
		private var _enableProcessingPopup:Boolean;
		
		public function RetrieveCurrentServerTimeEvent(callbackFunction:Function, enableProcessingPopup:Boolean = false)
		{
			super();
			_callbackFunction = callbackFunction;
			_enableProcessingPopup = enableProcessingPopup;
		}
		
		public function get callbackFunction():Function
		{
			return _callbackFunction;
		}
		
		public function get enableProcessingPopup():Boolean
		{
			return _enableProcessingPopup;
		}
	}
}