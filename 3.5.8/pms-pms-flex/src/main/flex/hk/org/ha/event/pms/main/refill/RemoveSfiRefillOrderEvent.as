package hk.org.ha.event.pms.main.refill {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RemoveSfiRefillOrderEvent extends AbstractTideEvent 
	{		
		private var _refillScheduleId:Number;
		
		public function RemoveSfiRefillOrderEvent(_refillScheduleId:Number):void 
		{
			super();
			_refillScheduleId = refillScheduleId;
		}
		
		public function get refillScheduleId():Number {
			return _refillScheduleId;
		}
	}
}