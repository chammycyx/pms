package hk.org.ha.event.pms.main.vetting.mp.popup {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowConfirmWardTransferPopupEvent extends AbstractTideEvent {

		private var _reviewHandler:Function;
		private var _updateHandler:Function;
		
		public function ShowConfirmWardTransferPopupEvent(reviewHandler:Function, updateHandler:Function) {
			super();
			_reviewHandler = reviewHandler;
			_updateHandler = updateHandler;
		}
		
		public function get reviewHandler():Function {
			return _reviewHandler;
		}
		
		public function get updateHandler():Function {
			return _updateHandler;
		}
	}
}
