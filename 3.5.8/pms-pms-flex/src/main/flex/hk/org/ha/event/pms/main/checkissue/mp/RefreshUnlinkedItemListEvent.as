package hk.org.ha.event.pms.main.checkissue.mp
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshUnlinkedItemListEvent extends AbstractTideEvent 
	{			
		private var _checked: Boolean
		
		public function RefreshUnlinkedItemListEvent(checked:Boolean=false):void 
		{
			super();
			_checked = checked;
		}
		
		public function get checked():Boolean {
			return _checked;
		}
	}
}