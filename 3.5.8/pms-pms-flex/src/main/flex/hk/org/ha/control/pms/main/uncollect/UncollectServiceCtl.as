package hk.org.ha.control.pms.main.uncollect{
	
	import hk.org.ha.event.pms.main.uncollect.RefreshDrugChargingUncollectViewEvent;
	import hk.org.ha.event.pms.main.uncollect.RetrieveUncollectOrderEvent;
	import hk.org.ha.model.pms.biz.uncollect.UncollectServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("uncollectServiceCtl", restrict="true")]
	public class UncollectServiceCtl 
	{
		[In]
		public var uncollectService:UncollectServiceBean;
		
		[Observer]
		public function retrieveUncollectOrder(evt:RetrieveUncollectOrderEvent):void
		{
			uncollectService.retrieveUncollectOrder(evt.ticketDate, evt.ticketNum, retrieveUncollectOrderResult);
		}
		
		private function retrieveUncollectOrderResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshDrugChargingUncollectViewEvent());
		}
	}
}
