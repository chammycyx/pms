package hk.org.ha.control.pms.exception
{
	import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	
	import mx.messaging.messages.ErrorMessage;
	
	import org.granite.tide.BaseContext;
	import org.granite.tide.IExceptionHandler;
	
	public class DueOrderPrintExceptionHandler implements IExceptionHandler
	{
		public static const DUE_ORDER_PRINT_EXCEPTION:String = "DueOrderPrintException";
		
		public function DueOrderPrintExceptionHandler()
		{
		}
		
		public function accepts(emsg:ErrorMessage):Boolean
		{
			return emsg.faultCode == DUE_ORDER_PRINT_EXCEPTION;
		}
		
		public function handle(context:BaseContext, emsg:ErrorMessage):void
		{
			var popupProp:SystemMessagePopupProp = new SystemMessagePopupProp();
			popupProp.messageWinHeight = 300;
			if( emsg.extendedData["alertException"] == "Y" ){
				popupProp.messageCode = "0592";
				popupProp.setOkButtonOnly = true;	
			}else{
				popupProp.messageCode = "0667";
				popupProp.setOkButtonOnly = true;
				popupProp.messageParams = [emsg.extendedData["caseNum"], emsg.extendedData["wardCode"]];
			}
			context.dispatchEvent(new RetrieveSystemMessageEvent(popupProp));			
		}
	}
}