package hk.org.ha.event.pms.main.inbox
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateStatShowAllWardsEvent extends AbstractTideEvent
	{
		private var _showAllWards:Boolean;
		
		public function UpdateStatShowAllWardsEvent(showAllWards:Boolean)
		{
			super();
			_showAllWards = showAllWards;
		}
		
		public function get showAllWards():Boolean
		{
			return _showAllWards;
		}
	}
}