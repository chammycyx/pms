package hk.org.ha.control.pms.main.reftable {
	
	import hk.org.ha.event.pms.main.reftable.InitRefillQtyAdjExclusionTabEvent;
	import hk.org.ha.event.pms.main.reftable.RetrieveRefillQtyAdjExclusionListEvent;
	import hk.org.ha.event.pms.main.reftable.ShowRefillModuleMaintSaveSuccessMsgEvent;
	import hk.org.ha.event.pms.main.reftable.UpdateRefillQtyAdjExclusionListEvent;
	import hk.org.ha.model.pms.biz.reftable.RefillQtyAdjExclusionListServiceBean;
	
	import mx.collections.ArrayCollection;
	import mx.core.UIComponent;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("refillQtyAdjExclusionListServiceCtl", restrict="true")]
	public class RefillQtyAdjExclusionListServiceCtl 
	{
		[In]
		public var refillQtyAdjExclusionListService:RefillQtyAdjExclusionListServiceBean;
		
		private var nextTabIndex:Number = 2;
		private var nextSubTabIndex:Number = 0;
		private var showRefillModuleMaintAlertMsg:Boolean = true;
		private var caller:UIComponent = null;
		
		[Observer]
		public function retrieveRefillQtyAdjExclusionList(evt:RetrieveRefillQtyAdjExclusionListEvent):void
		{
			refillQtyAdjExclusionListService.retrieveRefillQtyAdjExclusionList(initRefillQtyAdjSubTab);
		}
		
		[Observer]
		public function updateRefillQtyAdjExclusionList(evt:UpdateRefillQtyAdjExclusionListEvent):void
		{
			showRefillModuleMaintAlertMsg = evt.showRefillModuleMaintAlertMsg;
			nextTabIndex = evt.nextTabIndex;
			nextSubTabIndex = evt.nextSubTabIndex;
			caller = evt.caller;
			
			refillQtyAdjExclusionListService.updateRefillQtyAdjExclusionList(evt.updateRefillQtyAdjExclusionList, 
																				evt.delRefillQtyAdjExclusionList, 
																				updateRefillQtyAdjListResult);
		}
		
		private function initRefillQtyAdjSubTab(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new InitRefillQtyAdjExclusionTabEvent(evt.result as ArrayCollection));
		}
		
		private function updateRefillQtyAdjListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent( new ShowRefillModuleMaintSaveSuccessMsgEvent(nextTabIndex, nextSubTabIndex, showRefillModuleMaintAlertMsg, caller));
		}
		
	}
}
