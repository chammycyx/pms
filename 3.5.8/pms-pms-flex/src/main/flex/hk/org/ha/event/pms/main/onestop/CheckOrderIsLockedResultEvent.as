package hk.org.ha.event.pms.main.onestop {
		
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CheckOrderIsLockedResultEvent extends AbstractTideEvent 
	{	
		private var _followUpFunc:Function;
		private var _followUpFuncAfterFollowUpFunc:Function;
		
		public function CheckOrderIsLockedResultEvent(followUpFunc:Function, 
													  followUpFuncAfterFollowUpFunc:Function=null):void 
		{
			super();
			_followUpFunc = followUpFunc;
			_followUpFuncAfterFollowUpFunc = followUpFuncAfterFollowUpFunc;
		}        
		
		public function get followUpFunc():Function {
			return _followUpFunc;
		}
		
		public function get followUpFuncAfterFollowUpFunc():Function {
			return _followUpFuncAfterFollowUpFunc;
		}
	}
}