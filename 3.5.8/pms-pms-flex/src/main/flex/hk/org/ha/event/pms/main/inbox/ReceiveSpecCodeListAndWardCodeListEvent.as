package hk.org.ha.event.pms.main.inbox
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ReceiveSpecCodeListAndWardCodeListEvent extends AbstractTideEvent
	{
		public function ReceiveSpecCodeListAndWardCodeListEvent()
		{
			super();
		}
	}
}