package hk.org.ha.event.pms.main.enquiry
{
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrieveDeliveryRequestInfoListEvent extends AbstractTideEvent
	{
		public function RetrieveDeliveryRequestInfoListEvent()
		{
			super();
		}
	}
}