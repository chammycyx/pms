package hk.org.ha.event.pms.main.pivas
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePivasFindPatientListEvent extends AbstractTideEvent 
	{		
		private var _searchKey:String;
		
		private var _callbackFunc:Function;
		
		public function RetrievePivasFindPatientListEvent(searchKey:String, callbackFunc:Function):void 
		{
			super();
			_searchKey = searchKey;
			_callbackFunc = callbackFunc;
		}

		public function get searchKey():String {
			return _searchKey;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}