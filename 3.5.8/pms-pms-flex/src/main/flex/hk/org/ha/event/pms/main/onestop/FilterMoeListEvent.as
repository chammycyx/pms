package hk.org.ha.event.pms.main.onestop {
		
	import hk.org.ha.model.pms.udt.onestop.MoeFilterOption;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class FilterMoeListEvent extends AbstractTideEvent 
	{			
		private var _moeFilterOption:MoeFilterOption;
		private var _prescriptionSortCriteriaList:ArrayCollection;
		private var _callbackFunction:Function;
		
		public function FilterMoeListEvent(moeFilterOption:MoeFilterOption, prescriptionSortCriteriaList:ArrayCollection, 
										   callbackFunction:Function):void 
		{
			super();
			_moeFilterOption = moeFilterOption;
			_prescriptionSortCriteriaList = prescriptionSortCriteriaList;
			_callbackFunction = callbackFunction;
		}
		
		public function get moeFilterOption():MoeFilterOption
		{
			return _moeFilterOption;
		}
		
		public function get prescriptionSortCriteriaList():ArrayCollection
		{
			return _prescriptionSortCriteriaList;
		}
		
		public function get callbackFunction():Function
		{
			return _callbackFunction;
		}
	}
}