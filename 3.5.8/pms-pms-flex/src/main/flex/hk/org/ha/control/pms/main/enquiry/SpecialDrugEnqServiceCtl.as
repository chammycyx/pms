package hk.org.ha.control.pms.main.enquiry {
	
	import hk.org.ha.event.pms.main.enquiry.RefreshDmFmDrugEvent;
	import hk.org.ha.event.pms.main.enquiry.RetrieveDmFmDrugByFullDrugDescEvent;
	import hk.org.ha.event.pms.main.enquiry.RetrieveDmFmDrugEvent;
	import hk.org.ha.model.pms.biz.enquiry.SpecialDrugEnqServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("specialDrugEnqServiceCtl", restrict="true")]
	public class SpecialDrugEnqServiceCtl 
	{
		[In]
		public var specialDrugEnqService:SpecialDrugEnqServiceBean;
		
		[Observer]
		public function retrieveDmFmDrug(evt:RetrieveDmFmDrugEvent):void
		{
			specialDrugEnqService.retrieveDmFmDrug(evt.itemCode, retrieveDmFmDrugResult);
		}
		
		[Observer]
		public function retrieveDmFmDrugByFullDrugDesc(evt:RetrieveDmFmDrugByFullDrugDescEvent):void
		{
			specialDrugEnqService.retrieveDmFmDrugByFullDrugDesc(evt.fullDrugDesc, retrieveDmFmDrugResult);
		}

		private function retrieveDmFmDrugResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshDmFmDrugEvent());
		}
	}
}
