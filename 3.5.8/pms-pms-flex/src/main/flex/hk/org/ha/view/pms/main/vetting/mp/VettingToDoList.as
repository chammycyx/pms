package hk.org.ha.view.pms.main.vetting.mp
{
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	
	import mx.core.mx_internal;
	
	import spark.components.List;
	
	use namespace mx_internal;
	
	public class VettingToDoList extends List
	{
		public function VettingToDoList()
		{
			super();
		}
		
		override protected function keyDownHandler(event:KeyboardEvent):void
		{
			super.keyDownHandler(event);
			
			if (!dataProvider || !layout)
				return;
			
			if (event.keyCode == Keyboard.SPACE)
			{
				setSelectedIndex(caretIndex, true); 
				event.preventDefault();
				return; 
			}
			
			if (findKey(event.charCode))
			{
				event.preventDefault();
				return;
			}
			
			adjustSelectionAndCaretUponNavigation(event); 
		}
	}
}