package hk.org.ha.event.pms.main.vetting.mp
{
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SelectChemoItemFilterEvent extends AbstractTideEvent
	{
		private var _medProfile:MedProfile;
		private var _cycle:Number;
		
		public function SelectChemoItemFilterEvent(medProfile:MedProfile, cycle:Number)
		{
			super();
			_medProfile = medProfile;
			_cycle = cycle;
		}
		
		public function get medProfile():MedProfile
		{
			return _medProfile;
		}
		
		public function get cycle():Number
		{
			return _cycle;
		}
		
	}
}