package hk.org.ha.event.pms.main.inbox
{
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ManualTransferEvent extends AbstractTideEvent
	{
		private var _medProfile:MedProfile;
		private var _transferFlag:Boolean;
		private var _transferDate:Date;
		private var _applyImmediately:Boolean;
		private var _itemCutOffDate:Date;
		private var _callbackFunction:Function;
		
		public function ManualTransferEvent(medProfile:MedProfile, transferFlag:Boolean, transferDate:Date, applyImmediately:Boolean,
											itemCutOffDate:Date, callbackFunction:Function)
		{
			super();
			_medProfile = medProfile;
			_transferFlag = transferFlag;
			_transferDate = transferDate;
			_applyImmediately = applyImmediately;
			_itemCutOffDate = itemCutOffDate;
			_callbackFunction = callbackFunction;
		}
		
		public function get medProfile():MedProfile
		{
			return _medProfile;
		}
		
		public function get transferFlag():Boolean
		{
			return _transferFlag;
		}
		
		public function get transferDate():Date
		{
			return _transferDate;
		}
		
		public function get applyImmediately():Boolean
		{
			return _applyImmediately;
		}
		
		public function get itemCutOffDate():Date
		{
			return _itemCutOffDate;
		}
		
		public function get callbackFunction():Function
		{
			return _callbackFunction;
		}
	}
}