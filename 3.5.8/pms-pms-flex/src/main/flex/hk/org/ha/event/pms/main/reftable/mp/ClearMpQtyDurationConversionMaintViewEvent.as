package hk.org.ha.event.pms.main.reftable.mp {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ClearMpQtyDurationConversionMaintViewEvent extends AbstractTideEvent 
	{	
		private var _saveSuccess:Boolean;
		
		private var _messageCode:String;
		
		public function get saveSuccess():Boolean
		{
			return _saveSuccess;
		}
		
		public function get messageCode():String
		{
			return _messageCode;
		}
		
		public function ClearMpQtyDurationConversionMaintViewEvent(saveSuccess:Boolean, messageCode:String):void 
		{
			super();
			_saveSuccess = saveSuccess;
			_messageCode = messageCode;
		}
	}
}