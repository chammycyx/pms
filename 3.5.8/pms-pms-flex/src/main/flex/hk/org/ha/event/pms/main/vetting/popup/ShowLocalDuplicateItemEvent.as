package hk.org.ha.event.pms.main.vetting.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowLocalDuplicateItemEvent extends AbstractTideEvent {
		
		private var _callback:Function;
		
		public function ShowLocalDuplicateItemEvent(callback:Function) {
			super();
			_callback = callback;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}