package hk.org.ha.control.pms.main.reftable {
	
	import mx.collections.ArrayCollection;
	import mx.core.UIComponent;
	
	import hk.org.ha.event.pms.main.reftable.RetrieveDrsConfigListEvent;
	import hk.org.ha.event.pms.main.reftable.popup.RetrievePasSpecialtyListForDrsConfigEvent;
	import hk.org.ha.event.pms.main.reftable.SetDrsConfigListEvent;
	import hk.org.ha.event.pms.main.reftable.ShowRefillModuleMaintSaveSuccessMsgEvent;
	import hk.org.ha.event.pms.main.reftable.UpdateDrsConfigListEvent;
	import hk.org.ha.model.pms.biz.reftable.DrsConfigListServiceBean;
	import hk.org.ha.model.pms.vo.reftable.DrsConfigInfo;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("drsConfigListServiceCtl", restrict="true")]
	public class DrsConfigListServiceCtl 
	{
		[In]
		public var drsConfigListService:DrsConfigListServiceBean;
		
		private var nextTabIndex:Number = 1;
		private var nextSubTabIndex:Number = 0;
		private var showRefillModuleMaintAlertMsg:Boolean = true;
		private var caller:UIComponent = null;
		private var callbackFunc:Function = null;
		
		[Observer]
		public function retrieveDrsConfigList(evt:RetrieveDrsConfigListEvent):void {
			drsConfigListService.retrieveDrsConfigList(retrieveDrsConfigListResult);
		}
		
		private function retrieveDrsConfigListResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new SetDrsConfigListEvent(evt.result as DrsConfigInfo));
		}
		
		[Observer]
		public function updateDrsConfigList(evt:UpdateDrsConfigListEvent):void {
			showRefillModuleMaintAlertMsg = evt.showRefillModuleMaintAlertMsg;
			nextTabIndex = evt.nextTabIndex;
			nextSubTabIndex = evt.nextSubTabIndex;
			caller = evt.caller;
			drsConfigListService.updateDrsConfigList(evt.updateDrsConfigList, evt.delDrsConfigList, updateDrsConfigListResult);
		}
		
		private function updateDrsConfigListResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent( new ShowRefillModuleMaintSaveSuccessMsgEvent(nextTabIndex, nextSubTabIndex, showRefillModuleMaintAlertMsg, caller));
		}
				
		[Observer]
		public function retrievePasSpecialtyListForDrsConfig(evt:RetrievePasSpecialtyListForDrsConfigEvent):void {
			callbackFunc = evt.callbackFunc;
			drsConfigListService.retrievePasSpecList(evt.prefixSpecCode, evt.specType, retrievePasSpecialtyListForDrsConfigResult);
		}
		
		private function retrievePasSpecialtyListForDrsConfigResult(evt:TideResultEvent):void {
			callbackFunc(evt.result as ArrayCollection);
		}
	}
}
