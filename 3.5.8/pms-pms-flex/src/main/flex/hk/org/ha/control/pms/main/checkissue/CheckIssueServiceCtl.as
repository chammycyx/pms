package hk.org.ha.control.pms.main.checkissue{
	
	import hk.org.ha.event.pms.main.checkissue.AuditTicketNotMatchEvent;
	import hk.org.ha.event.pms.main.checkissue.RefreshDrugCheckIssueViewEvent;
	import hk.org.ha.event.pms.main.checkissue.RefreshPrescDetailEvent;
	import hk.org.ha.event.pms.main.checkissue.RetrieveCheckIssueListEvent;
	import hk.org.ha.event.pms.main.checkissue.RetrieveIssuedListEvent;
	import hk.org.ha.event.pms.main.checkissue.RetrieveOtherCheckListEvent;
	import hk.org.ha.event.pms.main.checkissue.RetrievePrescDetailEvent;
	import hk.org.ha.event.pms.main.checkissue.ReverseUncollectPrescEvent;
	import hk.org.ha.event.pms.main.checkissue.SetCheckIssueInfoStringEvent;
	import hk.org.ha.event.pms.main.checkissue.SetCheckIssuedListEvent;
	import hk.org.ha.event.pms.main.checkissue.SetFocusOnTicketNumEvent;
	import hk.org.ha.event.pms.main.checkissue.UpdateDispOrderStatusToCheckedEvent;
	import hk.org.ha.event.pms.main.checkissue.UpdateDispOrderStatusToIssuedEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.model.pms.biz.checkissue.CheckIssueServiceBean;
	import hk.org.ha.model.pms.vo.checkissue.CheckIssueListSummary;
	import hk.org.ha.model.pms.vo.checkissue.CheckIssueSummary;
	import hk.org.ha.model.pms.vo.checkissue.PrescDetail;
	import hk.org.ha.model.pms.vo.security.PropMap;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("checkIssueServiceCtl", restrict="true")]
	public class CheckIssueServiceCtl 
	{
		[In]
		public var checkIssueService:CheckIssueServiceBean;
		
		[In]
		public var propMap:PropMap;
		
		private var setFocusOnTicketNum:Boolean = false;
		private var action:String; //check / issue / suspend
		
		[Observer]
		public function retrievePrescDetail(evt:RetrievePrescDetailEvent):void
		{
			In(Object(checkIssueService).updateSuccess)
			In(Object(checkIssueService).errMsg)
			In(Object(checkIssueService).refreshPrescDetail)
			In(Object(checkIssueService).processAutoCheckIssue)
			In(Object(checkIssueService).action)
			checkIssueService.retrievePrescDetail(evt.checkIssueViewInfo, retrievePrescDetailResult, closeLoadingPopup);
		}
		
		private function retrievePrescDetailResult(evt:TideResultEvent):void
		{
			if ( Object(checkIssueService).processAutoCheckIssue ) {
				action = Object(checkIssueService).action as String;
				if( getPropMapValueAsBoolean("issue.ticketMatch.enabled")){
					if ( action == "check" ){
						updateDispOrderStatusResult(evt);
					} else {
						refreshPrescDetailResult(evt);
					}
				}else{
					updateDispOrderStatusResult(evt);
				}
			} else {
				refreshPrescDetailResult(evt);
			}
		}
		
		private function refreshPrescDetailResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			evt.context.dispatchEvent( new RefreshPrescDetailEvent(evt.result as CheckIssueSummary) );
		}
		
		[Observer]
		public function updateDispOrderStatusToChecked(evt:UpdateDispOrderStatusToCheckedEvent):void
		{
			action = "check";
			
			In(Object(checkIssueService).updateSuccess)
			In(Object(checkIssueService).errMsg)
			In(Object(checkIssueService).refreshPrescDetail)
			checkIssueService.updateDispOrderStatusToChecked(evt.checkIssueViewInfo, false, updateDispOrderStatusResult, closeLoadingPopup);
		}
		
		[Observer]
		public function updateDispOrderStatusToIssued(evt:UpdateDispOrderStatusToIssuedEvent):void
		{
			action = "issue";
			
			In(Object(checkIssueService).updateSuccess)
			In(Object(checkIssueService).errMsg)
			In(Object(checkIssueService).refreshPrescDetail)
			checkIssueService.updateDispOrderStatusToIssued(evt.checkIssueViewInfo, false,	updateDispOrderStatusResult, closeLoadingPopup);
		}
		
		private function updateDispOrderStatusResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			if ( Object(checkIssueService).updateSuccess) { 
				evt.context.dispatchEvent(new RefreshDrugCheckIssueViewEvent(action, evt.result as CheckIssueSummary));
			} else {
				if ( Object(checkIssueService).refreshPrescDetail ) {
					refreshPrescDetailResult(evt);
				} 
				evt.context.dispatchEvent(new SetCheckIssueInfoStringEvent(Object(checkIssueService).errMsg));
				
			}
		}
		
		private var currentRetrievedList:String; //checkIssueList, otherCheckList, issuedList, all
		
		[Observer]
		public function retrieveCheckIssueList(evt:RetrieveCheckIssueListEvent):void
		{
			setFocusOnTicketNum = evt.setFocusOnTicketNum;
			if ( evt.refreshAll ) {
				currentRetrievedList = "all";
				checkIssueService.refreshCheckIssueList(evt.refreshIssuedList, 
					evt.checkIssueListDate, 
					evt.otherCheckListDate, 
					evt.issuedListDate, 
					retrieveCheckIssueListResult);
			} else {
				currentRetrievedList = "checkIssueList";
				checkIssueService.retrieveCheckIssueList(evt.checkIssueListDate, retrieveCheckIssueListResult);
			}
		}	
		
		[Observer]
		public function retrieveOtherCheckList(evt:RetrieveOtherCheckListEvent):void
		{
			currentRetrievedList = "otherCheckList";
			checkIssueService.retrieveOtherCheckList(evt.ticketDate, retrieveCheckIssueListResult);
		}
		
		[Observer]
		public function retrieveIssuedList(evt:RetrieveIssuedListEvent):void
		{
			currentRetrievedList = "issuedList";
			checkIssueService.retrieveIssuedList(evt.ticketDate, retrieveCheckIssueListResult);
		}
		
		private function retrieveCheckIssueListResult(evt:TideResultEvent):void
		{
			var checkIssueListSummary:CheckIssueListSummary = new CheckIssueListSummary();
			if ( currentRetrievedList == "otherCheckList"){
				checkIssueListSummary.otherCheckOrderList = evt.result as ArrayCollection;
			} else if ( currentRetrievedList == "issuedList"){
				checkIssueListSummary.issuedOrderList = evt.result as ArrayCollection;
			} else {
				checkIssueListSummary = evt.result as CheckIssueListSummary;
			}
			
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			evt.context.dispatchEvent(new SetCheckIssuedListEvent(checkIssueListSummary, currentRetrievedList));
			evt.context.dispatchEvent(new SetFocusOnTicketNumEvent());
		}
		
		[Observer]
		public function reverseUncollectPresc(evt:ReverseUncollectPrescEvent):void
		{
			In(Object(checkIssueService).updateSuccess)
			In(Object(checkIssueService).errMsg)
			In(Object(checkIssueService).refreshPrescDetail)
			In(Object(checkIssueService).processAutoCheckIssue)
			In(Object(checkIssueService).action)
			checkIssueService.reverseUncollectPrescriptionByPropup(evt.checkIssueViewInfo, retrievePrescDetailResult);
		}
		
		private function closeLoadingPopup(evt:TideFaultEvent):void {
			evt.context.dispatchEvent(new SetFocusOnTicketNumEvent());
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
		}
		
		[Observer]
		public function writeTicketMatchAuditLog(evt:AuditTicketNotMatchEvent):void
		{
			checkIssueService.writeCancelTicketMatchAuditMessage(evt.checkIssueTicketMatchAuditInfo);
		}
		
		private function getPropMapValueAsBoolean(name:String):Boolean {
			if ( propMap.getValue(name) == 'Y' || propMap.getValueAsBoolean(name) ) {
				return true;
			} else {
				return false;
			}
		}
	}
}
