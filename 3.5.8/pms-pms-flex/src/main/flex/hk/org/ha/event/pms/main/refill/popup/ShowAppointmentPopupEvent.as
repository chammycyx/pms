package hk.org.ha.event.pms.main.refill.popup {

	import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowAppointmentPopupEvent extends AbstractTideEvent 
	{
		private var _prevRefillSchedule:RefillSchedule;
		private var _currRefillSchedule:RefillSchedule;
		
		public function ShowAppointmentPopupEvent(prevRefillSchedule:RefillSchedule, currRefillSchedule:RefillSchedule):void {
			super();
			_prevRefillSchedule = prevRefillSchedule;
			_currRefillSchedule = currRefillSchedule;
		}
		
		public function get prevRefillSchedule():RefillSchedule {
			return _prevRefillSchedule;
		}
		
		public function get currRefillSchedule():RefillSchedule {
			return _currRefillSchedule;
		}
	}
}