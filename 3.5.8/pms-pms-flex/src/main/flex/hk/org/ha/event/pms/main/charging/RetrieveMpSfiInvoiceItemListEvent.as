package hk.org.ha.event.pms.main.charging {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrieveMpSfiInvoiceItemListEvent extends AbstractTideEvent 
	{	
		private var _hkid:String;
		private var _caseNum:String;
		private var _medProfileId:Number;
		
		public function RetrieveMpSfiInvoiceItemListEvent(hkid:String, caseNum:String, medProfileId:Number=NaN):void 
		{
			super();
			_hkid = hkid;
			_caseNum = caseNum;
			_medProfileId = medProfileId;
		}
		
		public function get hkid():String{
			return _hkid;
		}
		
		public function get caseNum():String{
			return _caseNum;
		}
		
		public function get medProfileId():Number{
			return _medProfileId;
		}
	}
}