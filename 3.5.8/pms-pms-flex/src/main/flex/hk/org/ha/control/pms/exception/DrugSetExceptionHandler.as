package hk.org.ha.control.pms.exception {

	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
    import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
    import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
    
    import mx.messaging.messages.ErrorMessage;
    
    import org.granite.tide.BaseContext;
    import org.granite.tide.IExceptionHandler;

    public class DrugSetExceptionHandler implements IExceptionHandler 
	{
		public static const DRUG_SET_EXCEPTION:String = "DrugSetException";
        
        
        public function accepts(emsg:ErrorMessage):Boolean 
		{
			return emsg.faultCode == DRUG_SET_EXCEPTION;
        }

        public function handle(context:BaseContext, emsg:ErrorMessage):void 
		{
			context.dispatchEvent(new CloseLoadingPopupEvent());
			
			var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp("0491");
			msgProp.setOkButtonOnly = true;
			context.dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
        }
    }
}
