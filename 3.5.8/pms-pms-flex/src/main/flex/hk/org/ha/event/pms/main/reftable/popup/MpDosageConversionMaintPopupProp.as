package hk.org.ha.event.pms.main.reftable.popup
{
	import hk.org.ha.model.pms.dms.persistence.PmsIpDosageConvSet;

	public class MpDosageConversionMaintPopupProp
	{
		public var actionMode:String;
		public var okHandler:Function;
		public var cancelHandler:Function;
		public var pmsIpDosageConvSet:PmsIpDosageConvSet;
		public var isValidDdu:Boolean;
	}
}