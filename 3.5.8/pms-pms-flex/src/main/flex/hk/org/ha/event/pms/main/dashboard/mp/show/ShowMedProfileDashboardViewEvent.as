package hk.org.ha.event.pms.main.dashboard.mp.show
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowMedProfileDashboardViewEvent extends AbstractTideEvent
	{
		public function ShowMedProfileDashboardViewEvent()
		{
			super();
		}
	}
}