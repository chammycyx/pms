package hk.org.ha.event.pms.main.onestop {
		
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePagedPatientEvent extends AbstractTideEvent 
	{			
		private var _oneStopOrderItemSummaryIdList:ArrayCollection;
		private var _caller:String;
		private var _callbackFunction:Function;
		
		public function RetrievePagedPatientEvent(oneStopOrderItemSummaryIdList:ArrayCollection, caller:String, callbackFunction:Function):void 
		{
			super();
			_oneStopOrderItemSummaryIdList = oneStopOrderItemSummaryIdList;
			_caller = caller;
			_callbackFunction = callbackFunction;
		}
		
		public function get oneStopOrderItemSummaryIdList():ArrayCollection
		{
			return _oneStopOrderItemSummaryIdList;
		}
		
		public function get caller():String
		{
			return _caller;
		}
		
		public function get callbackFunction():Function
		{
			return _callbackFunction;
		}
	}
}