package hk.org.ha.event.pms.main.pivas
{
	import hk.org.ha.model.pms.dms.vo.pivas.PivasPrepValidateResponse;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdatePivasBatchPrepResultEvent extends AbstractTideEvent 
	{		
		private var _pivasPrepValidateResponse:PivasPrepValidateResponse;
		
		public function UpdatePivasBatchPrepResultEvent(pivasPrepValidateResponse:PivasPrepValidateResponse):void 
		{
			super();
			_pivasPrepValidateResponse = pivasPrepValidateResponse;
		}
		
		public function get pivasPrepValidateResponse():PivasPrepValidateResponse {
			return _pivasPrepValidateResponse;
		}
	}
}