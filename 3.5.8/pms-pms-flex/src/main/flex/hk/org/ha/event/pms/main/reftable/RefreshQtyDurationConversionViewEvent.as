package hk.org.ha.event.pms.main.reftable {
	
	import hk.org.ha.model.pms.vo.reftable.QtyDurationConversionInfo;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshQtyDurationConversionViewEvent extends AbstractTideEvent 
	{
		private var _retrieveCapdItem:Boolean;
		private var _qtyDurationConversionInfo:QtyDurationConversionInfo;
		
		public function RefreshQtyDurationConversionViewEvent(retrieveCapdItem:Boolean, qtyDurationConversionInfo:QtyDurationConversionInfo):void 
		{
			super();
			_retrieveCapdItem = retrieveCapdItem;
			_qtyDurationConversionInfo = qtyDurationConversionInfo;
		}
		
		public function get retrieveCapdItem():Boolean
		{
			return _retrieveCapdItem;
		}
		
		public function get qtyDurationConversionInfo():QtyDurationConversionInfo 
		{
			return _qtyDurationConversionInfo;
		}

	}
}