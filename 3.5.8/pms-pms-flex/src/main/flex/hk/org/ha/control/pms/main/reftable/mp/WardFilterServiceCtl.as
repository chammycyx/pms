package hk.org.ha.control.pms.main.reftable.mp {
	
	import hk.org.ha.event.pms.main.reftable.mp.RefreshWardFilterMaintViewEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveWardFilterListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.PrintWardFilterRptEvent;
	import hk.org.ha.event.pms.main.reftable.mp.UpdateWardFilterListEvent;
	import hk.org.ha.model.pms.biz.reftable.mp.WardFilterServiceBean;
	import hk.org.ha.model.pms.biz.reftable.mp.WardFilterRptServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("wardFilterServiceCtl", restrict="true")]
	public class WardFilterServiceCtl 
	{
		[In]
		public var wardFilterService:WardFilterServiceBean;
		
		[In]
		public var wardFilterRptService:WardFilterRptServiceBean;
		
		[Observer]
		public function retrieveWardFilterList(evt:RetrieveWardFilterListEvent):void
		{
			wardFilterService.retrieveWardFilterList(retrieveWardFilterListResult);
		}
		
		private function retrieveWardFilterListResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new RefreshWardFilterMaintViewEvent('Retrieve'));
		}
		
		[Observer]
		public function updateWardFilterList(evt:UpdateWardFilterListEvent):void
		{
			wardFilterService.updateWardFilterList(evt.wardFilterList, updateWardFilterListResult);
		}
		
		private function updateWardFilterListResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new RefreshWardFilterMaintViewEvent('Save'));
		}
		
		[Observer]
		public function printWardFilterRpt(evt:PrintWardFilterRptEvent):void {
			wardFilterRptService.printWardFilterRptList();
		}

	}
}
