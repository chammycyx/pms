package hk.org.ha.event.pms.main.checkissue
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetCheckIssueInfoStringEvent extends AbstractTideEvent 
	{		
		private var _errMsgCode:String;
		private var _callbackFunc:Function;
		
		public function SetCheckIssueInfoStringEvent(errMsgCode:String, callbackFunc:Function=null):void 
		{
			super();
			_errMsgCode = errMsgCode;
			_callbackFunc = callbackFunc;
		}
		
		public function get errMsgCode():String
		{
			return _errMsgCode;
		}
		
		public function get callbackFunc():Function
		{
			return _callbackFunc;
		}

	}
}