package hk.org.ha.event.pms.main.pivas.popup
{
	import hk.org.ha.model.pms.vo.pivas.AddPivasBatchPrepDetail;

	public class BatchPrepAddBatchPrepPopupProp
	{
		public var addPivasBatchPrepDetail:AddPivasBatchPrepDetail;
		public var addPivasBatchPrep:Boolean;
		public var quickAddPivasBatchPrep:Boolean;
	}
}
