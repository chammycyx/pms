package hk.org.ha.event.pms.main.inbox
{
	import hk.org.ha.model.pms.udt.medprofile.MedProfileStatAdminType;
	import hk.org.ha.model.pms.udt.medprofile.MedProfileStatOrderType;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateStatTypesAndFiltersEvent extends AbstractTideEvent
	{
		private var _adminType:MedProfileStatAdminType;
		private var _orderType:MedProfileStatOrderType;
		private var _wardFilterList:ArrayCollection;
		private var _wardGroupFilterList:ArrayCollection;
		
		public function UpdateStatTypesAndFiltersEvent(adminType:MedProfileStatAdminType, orderType:MedProfileStatOrderType,
				wardFilterList:ArrayCollection, wardGroupFilterList:ArrayCollection)
		{
			super();
			_adminType = adminType;
			_orderType = orderType;
			_wardFilterList = wardFilterList;
			_wardGroupFilterList = wardGroupFilterList;
		}
		
		public function get adminType():MedProfileStatAdminType
		{
			return _adminType;
		}
		
		public function get orderType():MedProfileStatOrderType
		{
			return _orderType;
		}
		
		public function get wardFilterList():ArrayCollection
		{
			return _wardFilterList;
		}
		
		public function get wardGroupFilterList():ArrayCollection
		{
			return _wardGroupFilterList;
		}
	}
}