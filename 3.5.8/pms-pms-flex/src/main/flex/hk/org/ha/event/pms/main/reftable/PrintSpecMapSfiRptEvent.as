package hk.org.ha.event.pms.main.reftable
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class PrintSpecMapSfiRptEvent extends AbstractTideEvent 
	{
		private var _printSfi:Boolean
		public function PrintSpecMapSfiRptEvent(printSfi:Boolean):void 
		{
			super();
			_printSfi = printSfi;
		}
		
		public function get printSfi():Boolean
		{
			return _printSfi;
		}
	}
}