package hk.org.ha.control.pms.main.report {
	
	import hk.org.ha.event.pms.main.report.ClearHaDrugFormularyMgmtRptListEvent;
	import hk.org.ha.event.pms.main.report.DnldHaDrugFormularyMgmtRptEvent;
	import hk.org.ha.event.pms.main.report.RefreshHaDrugFormularyMgmtViewEvent;
	import hk.org.ha.event.pms.main.report.RetrieveHaDrugFormularyMgmtRptListEvent;
	import hk.org.ha.event.pms.main.report.ZipHaDrugFormularyMgmtRptEvent;
	import hk.org.ha.event.pms.main.report.show.ShowHaDrugFormularyMgmtRptViewEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.model.pms.biz.report.HaDrugFormularyMgmtRptDnldServiceBean;
	import hk.org.ha.model.pms.biz.report.HaDrugFormularyMgmtRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	
	[Bindable]
	[Name("haDrugFormularyMgmtRptServiceCtl ", restrict="true")]
	public class HaDrugFormularyMgmtRptServiceCtl 
	{
		
		[In]
		public var haDrugFormularyMgmtRptService:HaDrugFormularyMgmtRptServiceBean;
		
		[In]
		public var haDrugFormularyMgmtRptDnldService:HaDrugFormularyMgmtRptDnldServiceBean;
		
		[Observer]
		public function retrieveHaDrugFormularyMgmtRptList(evt:RetrieveHaDrugFormularyMgmtRptListEvent):void {
			haDrugFormularyMgmtRptService.retrieveFmRptList(retrieveHaDrugFormularyMgmtRptListResult);
		}
		
		private function retrieveHaDrugFormularyMgmtRptListResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			evt.context.dispatchEvent(new RefreshHaDrugFormularyMgmtViewEvent());
		}
		
		[Observer]
		public function zipHaDrugFormularyMgmtRpt(evt:ZipHaDrugFormularyMgmtRptEvent):void {
			haDrugFormularyMgmtRptDnldService.zipFmRpt(evt.fileName, evt.password, dnldHaDrugFormularyMgmtRptResult);
		}
		
		private function dnldHaDrugFormularyMgmtRptResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new DnldHaDrugFormularyMgmtRptEvent());	
		}
		
		[Observer]
		public function clearHaDrugFormularyMgmtRptList(evt:ClearHaDrugFormularyMgmtRptListEvent):void {
			haDrugFormularyMgmtRptService.clearFmRptList();
		}
	}
}
