package hk.org.ha.control.pms.main.onestop {
	import hk.org.ha.event.pms.main.onestop.OneStopAllowModifyUncollectAuthnicationEvent;
	import hk.org.ha.event.pms.main.onestop.popup.ShowMessagePopupEvent;
	import hk.org.ha.event.pms.main.vetting.popup.RefreshPharmRemarkLogonPopupEvent;
	import hk.org.ha.model.pms.biz.onestop.OneStopAllowModifyUncollectValidationServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
		
	[Bindable]
	[Name("oneStopAllowModifyUncollectValidationServiceCtl", restrict="true")]
	public class OneStopAllowModifyUncollectValidationServiceCtl 
	{
		[In]
		public var oneStopAllowModifyUncollectValidationService:OneStopAllowModifyUncollectValidationServiceBean;
		
		[In]
		public var reAuthUserId:String;
		
		private var oneStopAllowModifyUncollectAuthnicationEvent:OneStopAllowModifyUncollectAuthnicationEvent;
		
		[Observer]
		public function oneStopAllowModifyUncollectAuthnication(evt:OneStopAllowModifyUncollectAuthnicationEvent):void
		{
			In(Object(oneStopAllowModifyUncollectValidationService).allowModifyUncollectFailMsg);
			oneStopAllowModifyUncollectAuthnicationEvent = evt;
			oneStopAllowModifyUncollectValidationService.validateAllowModifyUncollectUser(evt.username, evt.password, evt.targetName, 
				oneStopAllowModifyUncollectAuthnicationResult);
		}
		
		private function oneStopAllowModifyUncollectAuthnicationResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshPharmRemarkLogonPopupEvent(new Array(
				Object(oneStopAllowModifyUncollectValidationService).allowModifyUncollectFailMsg,
				reAuthUserId), oneStopAllowModifyUncollectAuthnicationEvent.followUpEvent));
		}
	}
}