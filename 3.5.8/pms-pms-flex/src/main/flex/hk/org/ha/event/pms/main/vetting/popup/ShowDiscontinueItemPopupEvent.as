package hk.org.ha.event.pms.main.vetting.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	import mx.collections.ListCollectionView;
	
	public class ShowDiscontinueItemPopupEvent extends AbstractTideEvent {
		
		private var _tlfList:ListCollectionView;
		
		private var _callback:Function;
		
		public function ShowDiscontinueItemPopupEvent(tlfList:ListCollectionView, callback:Function=null) {
			super();
			_tlfList = tlfList;
			_callback = callback;
		}
		
		public function get tlfList():ListCollectionView {
			return _tlfList;
		}
		
		public function get callback():Function {
			return _callback;
		}
		
	}
}