package hk.org.ha.event.pms.main.picking.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDrugPickViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		public function ShowDrugPickViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}