package hk.org.ha.event.pms.main.dispensing {
	
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.label.SpecialLabel;
	
	public class SpecialLabelPrintViewEvent extends AbstractTideEvent 
	{
		private var _specialLabel:SpecialLabel;

		public function SpecialLabelPrintViewEvent(specialLabel:SpecialLabel):void {
			super();
			_specialLabel = specialLabel;	

		}
		
		public function get specialLabel():SpecialLabel
		{
			return _specialLabel;
		}

	}
}