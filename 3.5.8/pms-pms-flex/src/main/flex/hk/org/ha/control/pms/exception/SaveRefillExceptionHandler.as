package hk.org.ha.control.pms.exception {
	
	import mx.messaging.messages.ErrorMessage;
	
	import hk.org.ha.event.pms.main.refill.SaveRefillExceptionMsgEvent;
	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;	
	
	import org.granite.tide.BaseContext;
	import org.granite.tide.IExceptionHandler;
	
	public class SaveRefillExceptionHandler implements IExceptionHandler {
		public static const SAVEREFILL_EXCEPTION:String = "SaveRefillException";
		
		public function accepts(emsg:ErrorMessage):Boolean {
			return emsg.faultCode == SAVEREFILL_EXCEPTION;
		}
		
		public function handle(context:BaseContext, emsg:ErrorMessage):void {
			var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp(emsg.extendedData["messageCode"]);
			msgProp.messageParams = emsg.extendedData["messageParam"] as Array;
			msgProp.setOkButtonOnly = true;
			
			var evt:SaveRefillExceptionMsgEvent = new SaveRefillExceptionMsgEvent(msgProp);
			context.dispatchEvent(evt);
		}
	}
}