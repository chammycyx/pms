package hk.org.ha.event.pms.main.charging.popup {
	import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
	import hk.org.ha.model.pms.persistence.medprofile.PurchaseRequest;
		
	public class ConfirmPurchaseRequestProp
	{
		public var addPurchaseRequest:PurchaseRequest;
		public var prevPurchaseRequest:PurchaseRequest;
		public var medProfilePoItem:MedProfilePoItem;
	}
}