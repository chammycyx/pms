package hk.org.ha.event.pms.main.reftable
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ReceiveWardAndWardGroupListEvent extends AbstractTideEvent
	{
		public function ReceiveWardAndWardGroupListEvent()
		{
			super();
		}
	}
}