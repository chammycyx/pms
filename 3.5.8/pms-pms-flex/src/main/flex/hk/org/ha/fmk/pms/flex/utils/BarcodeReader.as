package hk.org.ha.fmk.pms.flex.utils {
	import flash.events.KeyboardEvent;
	import flash.events.TimerEvent;
	import flash.ui.Keyboard;
	import flash.utils.Timer;
	
	import mx.events.FlexEvent;
	import mx.utils.StringUtil;
	
	public class BarcodeReader {
		
		public static function scanBarcode(evt:KeyboardEvent, delayTime:int, callback:Function):void {
			if (evt.keyCode == Keyboard.ENTER) {
				evt.stopPropagation();
				var t:Timer = new Timer(delayTime, 1); // 50 ms         
				t.addEventListener(TimerEvent.TIMER, function():void {
					evt.currentTarget.text = StringUtil.trim(evt.currentTarget.text);
					callback(); 
				} );         
				t.start();     
			}
		}
		
		public static function scanEnterBarcode(evt:FlexEvent, delayTime:int, callback:Function):void {
			evt.stopPropagation();
			var t:Timer = new Timer(delayTime, 1); // 50 ms         
			t.addEventListener(TimerEvent.TIMER, function():void {
				evt.currentTarget.text = StringUtil.trim(evt.currentTarget.text);
				callback(); 
			} );         
			t.start();     
		}
		
		public static function scanEnterBarcodeWithCallbackParam(evt:KeyboardEvent, delayTime:int, callback:Function):void {
			if (evt.keyCode == Keyboard.ENTER) { 
				evt.stopPropagation();
				var t:Timer = new Timer(delayTime, 1); // 50 ms         
				t.addEventListener(TimerEvent.TIMER, function():void {
					evt.currentTarget.text = StringUtil.trim(evt.currentTarget.text);
					callback(evt); 
				} );         
				t.start();     
			}
		}
		
		public static function scanBarcodeFromOneStopEntry(evt:KeyboardEvent, delayTime:int, callback:Function, proceed:Boolean):void {
			if (evt.keyCode == Keyboard.ENTER) {   
				evt.stopPropagation();	
				var t:Timer = new Timer(delayTime, 1); // 50 ms         
				t.addEventListener(TimerEvent.TIMER, function():void { 
					evt.currentTarget.text = StringUtil.trim(evt.currentTarget.text);
					callback(proceed); 
				} );         
				t.start();     
			}
		} 
		
		public static function scanBarcodeFromCheckIssue(evt:KeyboardEvent, delayTime:int, callback:Function):void {
			if (evt.keyCode == Keyboard.INSERT) {  
				evt.stopPropagation();	
				var t:Timer = new Timer(delayTime, 1); // 50 ms         
				t.addEventListener(TimerEvent.TIMER, function():void { 
					evt.currentTarget.text = StringUtil.trim(evt.currentTarget.text);
					callback(); 
				} );         
				t.start();     
			}
		}
		
		public static function scanBarcodeFromTicketGen(evt:KeyboardEvent, delayTime:int, callback:Function):void {
			if (evt.keyCode == Keyboard.INSERT) {  
				evt.stopPropagation();	
				var t:Timer = new Timer(delayTime, 1); // 50 ms         
				t.addEventListener(TimerEvent.TIMER, function():void { 
					evt.currentTarget.text = StringUtil.trim(evt.currentTarget.text);
					callback(); 
				} );         
				t.start();     
			}
		}
		
		public static function scanBarcodeFromLedOperation(evt:KeyboardEvent, delayTime:int, callback:Function):void {
			if (evt.keyCode == Keyboard.INSERT || evt.keyCode == Keyboard.ENTER) {  
				evt.stopPropagation();	
				var t:Timer = new Timer(delayTime, 1); // 50 ms         
				t.addEventListener(TimerEvent.TIMER, function():void { 
					evt.currentTarget.text = StringUtil.trim(evt.currentTarget.text);
					callback(); 
				} );         
				t.start();     
			}
		}
	}
}