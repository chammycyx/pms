package hk.org.ha.event.pms.main.uncollect {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ClearDrugChargingUncollectViewEvent extends AbstractTideEvent 
	{		
		public function ClearDrugChargingUncollectViewEvent():void 
		{
			super();
		}
	}
}