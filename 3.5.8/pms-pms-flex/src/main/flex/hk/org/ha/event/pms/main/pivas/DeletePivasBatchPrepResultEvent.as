package hk.org.ha.event.pms.main.pivas
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class DeletePivasBatchPrepResultEvent extends AbstractTideEvent 
	{		
		private var _errorCode:String;
		
		public function DeletePivasBatchPrepResultEvent(errorCode:String):void 
		{
			super();
			_errorCode = errorCode;
		}
		
		public function get errorCode():String {
			return _errorCode;
		}
	}
}