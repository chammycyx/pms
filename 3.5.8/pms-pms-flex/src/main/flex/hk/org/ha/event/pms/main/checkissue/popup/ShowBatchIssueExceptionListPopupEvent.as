package hk.org.ha.event.pms.main.checkissue.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowBatchIssueExceptionListPopupEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		public function ShowBatchIssueExceptionListPopupEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}