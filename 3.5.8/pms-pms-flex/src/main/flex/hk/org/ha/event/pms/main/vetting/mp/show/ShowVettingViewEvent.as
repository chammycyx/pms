package hk.org.ha.event.pms.main.vetting.mp.show
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowVettingViewEvent extends AbstractTideEvent
	{
		private var _fromPharmacyInbox:Boolean;
		private var _fromManualEntry:Boolean;
		
		public function ShowVettingViewEvent(fromPharmacyInbox:Boolean = false, fromManualEntry:Boolean=false)
		{
			super();
			_fromPharmacyInbox = fromPharmacyInbox;
			_fromManualEntry = fromManualEntry;
		}
		
		public function get fromPharmacyInbox():Boolean
		{
			return _fromPharmacyInbox;
		}
		
		public function get fromManualEntry():Boolean
		{
			return _fromManualEntry;
		}
	}
}