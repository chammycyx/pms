package hk.org.ha.event.pms.main.drug.popup
{
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	
	import mx.collections.ArrayCollection;
	
	public class ShowMpCapdDrugPopupEvent extends AbstractTideEvent
	{
		private var _drugSearchSource:DrugSearchSource;
		
		public function ShowMpCapdDrugPopupEvent(drugSearchSource:DrugSearchSource)
		{
			super();
			_drugSearchSource = drugSearchSource;
		}

		public function get drugSearchSource():DrugSearchSource {
			return _drugSearchSource;
		}
	}
}