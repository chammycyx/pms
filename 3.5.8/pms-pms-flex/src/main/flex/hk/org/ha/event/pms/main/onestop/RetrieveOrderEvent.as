package hk.org.ha.event.pms.main.onestop {
		
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveOrderEvent extends AbstractTideEvent 
	{	
		private var _searchText:String;
		private var _displayAllMoe:Boolean;
		private var _isUnvetTrigger:Boolean;
		private var _callPasAppointment:Boolean;
		
		public function RetrieveOrderEvent(searchText:String, displayAllMoe:Boolean, isUnvetTrigger:Boolean=false, 
										   callPasAppointment:Boolean=true):void 
		{
			super();
			this._searchText = searchText;
			this._displayAllMoe = displayAllMoe;
			this._isUnvetTrigger = isUnvetTrigger;
			this._callPasAppointment = callPasAppointment;
		}        
		
		public function get searchText():String 
		{
			return _searchText;
		}
		
		public function get displayAllMoe():Boolean
		{
			return _displayAllMoe;
		}
		
		public function get isUnvetTrigger():Boolean
		{
			return _isUnvetTrigger;
		}
		
		public function get callPasAppointment():Boolean
		{
			return _callPasAppointment;
		}
	}
}