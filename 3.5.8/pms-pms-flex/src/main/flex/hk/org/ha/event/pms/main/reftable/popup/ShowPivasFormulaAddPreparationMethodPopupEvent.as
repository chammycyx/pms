package hk.org.ha.event.pms.main.reftable.popup {

	import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormula;
	import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaDrug;
	import hk.org.ha.model.pms.vo.reftable.pivas.PivasDrugListInfo;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class ShowPivasFormulaAddPreparationMethodPopupEvent extends AbstractTideEvent {

		private var _pivasDrugListInfo:PivasDrugListInfo
		private var _pivasFormula:PivasFormula;
		private var _pivasFormulaDrug:PivasFormulaDrug;
		private var _okCallback:Function;
		private var _cancelCallback:Function;
		
		public function ShowPivasFormulaAddPreparationMethodPopupEvent(pivasDrugListInfo:PivasDrugListInfo, pivasFormula:PivasFormula, pivasFormulaDrug:PivasFormulaDrug, okCallback:Function, cancelCallback:Function):void {
			super();
			_pivasDrugListInfo = pivasDrugListInfo;
			_pivasFormula = pivasFormula;
			_pivasFormulaDrug = pivasFormulaDrug;
			_okCallback = okCallback;
			_cancelCallback = cancelCallback;
		}
		
		public function get pivasDrugListInfo():PivasDrugListInfo {
			return _pivasDrugListInfo;
		}
		
		public function get pivasFormula():PivasFormula {
			return _pivasFormula;
		}
		
		public function get pivasFormulaDrug():PivasFormulaDrug {
			return _pivasFormulaDrug;
		}
		
		public function get okCallback():Function {
			return _okCallback;
		}
		
		public function get cancelCallback():Function {
			return _cancelCallback;
		}
	}
}
