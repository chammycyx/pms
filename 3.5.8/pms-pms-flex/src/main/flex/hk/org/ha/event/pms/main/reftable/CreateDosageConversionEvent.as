package hk.org.ha.event.pms.main.reftable {
	
	import hk.org.ha.model.pms.dms.persistence.PmsDosageConversionSet;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CreateDosageConversionEvent extends AbstractTideEvent 
	{
		private var _pmsDosageConversionSet:PmsDosageConversionSet;
		
		public function CreateDosageConversionEvent(pmsDosageConversionSet:PmsDosageConversionSet):void 
		{
			super();
			this._pmsDosageConversionSet = pmsDosageConversionSet;
		}

		public function get pmsDosageConversionSet():PmsDosageConversionSet {
			return _pmsDosageConversionSet;
		}
		
	}
}