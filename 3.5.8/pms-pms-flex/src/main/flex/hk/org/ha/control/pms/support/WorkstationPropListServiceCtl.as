package hk.org.ha.control.pms.support{
	
	import hk.org.ha.event.pms.main.support.ClearWorkstationPropInfoListEvent;
	import hk.org.ha.event.pms.main.support.ClearWorkstationPropMaintViewEvent;
	import hk.org.ha.event.pms.main.support.RetrieveHostNameListEvent;
	import hk.org.ha.event.pms.main.support.RetrieveWorkstationPropListEvent;
	import hk.org.ha.event.pms.main.support.RetrieveWorkstationPropListResultEvent;
	import hk.org.ha.event.pms.main.support.RetrieveWorkstationPropViewHospitalListEvent;
	import hk.org.ha.event.pms.main.support.SetWorkstationPropMaintViewHospitalListEvent;
	import hk.org.ha.event.pms.main.support.UpdateWorkstationPropListEvent;
	import hk.org.ha.fmk.pms.flex.components.lookup.RefreshLookupPopupDataProviderEvent;
	import hk.org.ha.model.pms.biz.support.WorkstationPropListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("workstationPropListServiceCtl", restrict="true")]
	public class WorkstationPropListServiceCtl 
	{
		[In]
		public var workstationPropListService:WorkstationPropListServiceBean;
		
		[Observer]
		public function retrieveWorkstationPropViewHospitalList(evt:RetrieveWorkstationPropViewHospitalListEvent):void
		{
			workstationPropListService.retrieveHospitalList(retrieveWorkstationPropViewHospitalListResult);
		}
		
		private function retrieveWorkstationPropViewHospitalListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new SetWorkstationPropMaintViewHospitalListEvent(evt.result as ArrayCollection));
		}
		
		[Observer]
		public function retrieveHostNameList(evt:RetrieveHostNameListEvent):void
		{
			workstationPropListService.retrieveHostNameList(evt.hospCode(), evt.prefixHostName(), retrieveWorkstationListResult);
		}
		
		private function retrieveWorkstationListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshLookupPopupDataProviderEvent(evt.result as ArrayCollection));

		}
		
		[Observer]
		public function retrieveWorkstationPropList(evt:RetrieveWorkstationPropListEvent):void
		{
			In(Object(workstationPropListService).validHostName)
			workstationPropListService.retrieveWorkstationPropList(evt.hospital(), evt.hostName(), retrieveWorkstationPropListResult);
		}
		
		private function retrieveWorkstationPropListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RetrieveWorkstationPropListResultEvent(Object(workstationPropListService).validHostName) );
		}
		
		[Observer]
		public function updateWorkstationPropList(evt:UpdateWorkstationPropListEvent):void
		{
			workstationPropListService.updateWorkstationPropList(evt.workstationPropList, updateWorkstationPropListResult);
		}
		
		private function updateWorkstationPropListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new ClearWorkstationPropMaintViewEvent() );
		}
		
		[Observer]
		public function clearWorkstationPropInfoList(evt:ClearWorkstationPropInfoListEvent):void
		{
			workstationPropListService.clearWorkstationPropInfoList();
		}
	}
}
