package hk.org.ha.event.pms.main.reftable.mp {
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveMultiDoseConvHandlerEvent extends AbstractTideEvent 
	{
		private var _pmsMultiDoseConvSetList:ArrayCollection;

		public function RetrieveMultiDoseConvHandlerEvent(pmsMultiDoseConvSetList:ArrayCollection):void 
		{
			_pmsMultiDoseConvSetList = pmsMultiDoseConvSetList
			super();
		}
		
		public function get pmsMultiDoseConvSetList():ArrayCollection
		{
			return _pmsMultiDoseConvSetList;
		}
	}
}