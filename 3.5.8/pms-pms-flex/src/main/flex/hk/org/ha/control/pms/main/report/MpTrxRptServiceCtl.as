package hk.org.ha.control.pms.main.report {
	import hk.org.ha.event.pms.main.report.PrintMpTrxRptEvent;
	import hk.org.ha.event.pms.main.report.RefreshMpTrxRptSelectedIndexEvent;
	import hk.org.ha.event.pms.main.report.RefreshMpTrxRptViewEvent;
	import hk.org.ha.event.pms.main.report.RefreshMpTrxRptWardSelectionListEvent;
	import hk.org.ha.event.pms.main.report.RetrieveMpTrxRptDeliveryListEvent;
	import hk.org.ha.event.pms.main.report.RetrieveMpTrxRptEvent;
	import hk.org.ha.event.pms.main.report.RetrieveMpTrxRptWardSelectionListEvent;
	import hk.org.ha.event.pms.main.report.ShowMpTrxRptEvent;
	import hk.org.ha.model.pms.biz.report.MpTrxRptPrintServiceBean;
	import hk.org.ha.model.pms.biz.report.MpTrxRptServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	
	[Bindable]
	[Name("mpTrxRptServiceCtl", restrict="true")]
	public class MpTrxRptServiceCtl 
	{
		[In]
		public var mpTrxRptService:MpTrxRptServiceBean;
		
		[In]
		public var mpTrxRptPrintService:MpTrxRptPrintServiceBean;
		
		[Observer]
		public function retrieveMpTrxRptDeliveryList(evt:RetrieveMpTrxRptDeliveryListEvent):void
		{
			mpTrxRptService.retrieveDeliveryList(evt.wardCode, evt.batchCode, evt.caseNum, evt.dispenseDate,
																			retrieveMpTrxRptDeliveryListResult);
		}
		
		private function retrieveMpTrxRptDeliveryListResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshMpTrxRptViewEvent());
		}
		
		[Observer]
		public function retrieveMpTrxRptWardSelectionList(evt:RetrieveMpTrxRptWardSelectionListEvent):void
		{
			mpTrxRptService.retrieveWardSelectionList(retrieveMpTrxRptWardSelectionListResult);
		}
		
		private function retrieveMpTrxRptWardSelectionListResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshMpTrxRptWardSelectionListEvent(evt.result as ArrayCollection));
		}
		
		[Observer]
		public function printMpTrxRpt(evt:PrintMpTrxRptEvent):void
		{
			mpTrxRptPrintService.printMpTrxRptForReport(printMpTrxRptResult);
		}
		
		private function printMpTrxRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshMpTrxRptSelectedIndexEvent());
		}
		
		[Observer]
		public function retrieveMpTrxRpt(evt:RetrieveMpTrxRptEvent):void
		{
			mpTrxRptPrintService.retrieveMpTrxRptListByBatchNum(evt.batchCode, evt.dispenseDate, retrieveMpTrxRptListResult);
		}
		
		private function retrieveMpTrxRptListResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new ShowMpTrxRptEvent());
		}
		
	}
}
