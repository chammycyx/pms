package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetDrugSearchBnfEvent extends AbstractTideEvent 
	{			
		private var _drugSearchSource:DrugSearchSource;
		private var _drugSearchDrugBnfList:ArrayCollection;
		
		public function SetDrugSearchBnfEvent(drugSearchSource:DrugSearchSource, drugSearchDrugBnfList:ArrayCollection):void 
		{
			super();
			_drugSearchSource = drugSearchSource;
			_drugSearchDrugBnfList = drugSearchDrugBnfList;
		}
		
		public function get drugSearchSource():DrugSearchSource {
			return _drugSearchSource;
		}
		
		public function get drugSearchDrugBnfList():ArrayCollection {
			return _drugSearchDrugBnfList;
		}
	}
}