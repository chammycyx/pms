package hk.org.ha.event.pms.main.reftable {
	
	import hk.org.ha.model.pms.persistence.reftable.RefillConfig;
	
	import mx.core.UIComponent;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateRefillConfigEvent extends AbstractTideEvent 
	{		
		private var _refillConfig:RefillConfig;
		private var _nextTabIndex:Number;
		private var _showRefillModuleMaintAlertMsg:Boolean;
		private var _caller:UIComponent;
		
		public function UpdateRefillConfigEvent(refillConfig:RefillConfig, nextTabIndex:Number, showRefillModuleMaintAlertMsg:Boolean, caller:UIComponent):void 
		{
			super();
			_refillConfig = refillConfig;
			_nextTabIndex = nextTabIndex;
			_showRefillModuleMaintAlertMsg = showRefillModuleMaintAlertMsg;
			_caller = caller;
		}
		
		public function get refillConfig():RefillConfig {
			return _refillConfig;
		}
		
		public function get nextTabIndex():Number {
			return _nextTabIndex;
		}
		
		public function get showRefillModuleMaintAlertMsg():Boolean {
			return _showRefillModuleMaintAlertMsg;
		}
		
		public function get caller():UIComponent {
			return _caller;
		}
	}
}