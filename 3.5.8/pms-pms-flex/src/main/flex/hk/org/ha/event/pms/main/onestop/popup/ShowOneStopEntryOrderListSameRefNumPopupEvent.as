package hk.org.ha.event.pms.main.onestop.popup {
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowOneStopEntryOrderListSameRefNumPopupEvent extends AbstractTideEvent 
	{	
		private var _orderList:ArrayCollection;
		
		public function ShowOneStopEntryOrderListSameRefNumPopupEvent(orderList:ArrayCollection):void 
		{
			super();
			_orderList = orderList;
		}
		
		public function get orderList():ArrayCollection 
		{
			return _orderList;
		}
	}
}