package hk.org.ha.event.pms.security.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshSystemIdleProtectionPopupEvent extends AbstractTideEvent
	{
		private var _errMsg:String;
		private var _sessionTimeout:int;
		
		public function RefreshSystemIdleProtectionPopupEvent(errMsg:String, sessionTimeout:int):void {
			super();
			_errMsg = errMsg;
			_sessionTimeout = sessionTimeout;
		}
		
		public function get errMsg():String{
			return _errMsg;
		}	
		
		public function get sessionTimeout():int{
			return _sessionTimeout;
		}	
	}
}