package hk.org.ha.event.pms.main.reftable {
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateFollowUpWarningListEvent extends AbstractTideEvent 
	{
		private var _updateFollowUpWarningList:ListCollectionView;
		
		public function UpdateFollowUpWarningListEvent(updateFollowUpWarningList:ListCollectionView):void 
		{
			super();
			_updateFollowUpWarningList = updateFollowUpWarningList;
		}
		
		public function get updateFollowUpWarningList():ListCollectionView
		{
			return _updateFollowUpWarningList;
		}
	}
}