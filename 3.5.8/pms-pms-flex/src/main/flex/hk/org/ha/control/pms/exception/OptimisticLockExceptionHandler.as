package hk.org.ha.control.pms.exception {

    import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
    import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
    
    import mx.controls.Alert;
    import mx.messaging.messages.ErrorMessage;
    
    import org.granite.tide.BaseContext;
    import org.granite.tide.IExceptionHandler;

    public class OptimisticLockExceptionHandler implements IExceptionHandler 
	{
        
        public static const OPTIMISTIC_LOCK:String = "Persistence.OptimisticLock"; 
        
        
        public function accepts(emsg:ErrorMessage):Boolean 
		{
            return emsg.faultCode == OPTIMISTIC_LOCK;
        }

        public function handle(context:BaseContext, emsg:ErrorMessage):void 
		{
			var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp("0307");
			msgProp.setOkButtonOnly = true;
			context.dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
        }
    }
}
