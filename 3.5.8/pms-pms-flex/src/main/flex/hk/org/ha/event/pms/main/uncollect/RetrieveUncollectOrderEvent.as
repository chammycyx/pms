package hk.org.ha.event.pms.main.uncollect {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveUncollectOrderEvent extends AbstractTideEvent 
	{
		private var _ticketDate:Date;
		private var _ticketNum:String;
		
		public function RetrieveUncollectOrderEvent(ticketDate:Date, ticketNum:String):void 
		{
			super();
			_ticketDate = ticketDate;
			_ticketNum = ticketNum;
		}
		
		public function get ticketDate():Date {
			return _ticketDate;
		}
		
		public function get ticketNum():String {
			return _ticketNum;
		}
	}
}