package hk.org.ha.event.pms.main.reftable {
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateItemLocationListEvent extends AbstractTideEvent 
	{
		private var _updateItemLocationList:ListCollectionView;
		
		public function UpdateItemLocationListEvent(updateItemLocationList:ListCollectionView):void 
		{
			super();
			_updateItemLocationList = updateItemLocationList;
		}
		
		public function get updateItemLocationList():ListCollectionView{
			return _updateItemLocationList;
		}
	}
}