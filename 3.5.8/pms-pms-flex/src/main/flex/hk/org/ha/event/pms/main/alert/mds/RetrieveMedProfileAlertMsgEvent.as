package hk.org.ha.event.pms.main.alert.mds {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.alert.mds.popup.ShowMedProfileAlertMsgPopupEvent;
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.disp.MedItemInf;
	
	public class RetrieveMedProfileAlertMsgEvent extends AbstractTideEvent {
		
		private var _medItem:MedItemInf;
		
		private var _showMedProfileAlertMsgPopupEvent:ShowMedProfileAlertMsgPopupEvent;
		
		public function RetrieveMedProfileAlertMsgEvent(medItem:MedItemInf, showMedProfileAlertMsgPopupEvent:ShowMedProfileAlertMsgPopupEvent=null) {
			super();
			_medItem = medItem;
			_showMedProfileAlertMsgPopupEvent = showMedProfileAlertMsgPopupEvent;
		}
		
		public function get medItem():MedItemInf {
			return _medItem;
		}
		
		public function get showMedProfileAlertMsgPopupEvent():ShowMedProfileAlertMsgPopupEvent {
			return _showMedProfileAlertMsgPopupEvent;
		}
	}
}