package hk.org.ha.event.pms.main.vetting {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class EnableVettingEvent extends AbstractTideEvent {
		
		private var _enabled:Boolean;
		
		public function EnableVettingEvent(enabled:Boolean) {
			super();
			_enabled = enabled;
		}
		
		public function get enabled():Boolean {
			return _enabled;
		}
	}
}
