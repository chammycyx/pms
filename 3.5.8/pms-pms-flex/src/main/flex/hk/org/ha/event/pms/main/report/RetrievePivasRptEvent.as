package hk.org.ha.event.pms.main.report {

	import hk.org.ha.model.pms.udt.report.PivasReportType;
	import hk.org.ha.model.pms.udt.YesNoBlankFlag;
	import hk.org.ha.model.pms.udt.report.PivasRptDispMethod;
	import hk.org.ha.model.pms.udt.report.PivasRptItemCatFilterOption;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePivasRptEvent extends AbstractTideEvent 
	{
		private var _reportType:PivasReportType;
		private var _supplyFromDate:Date;
		private var _supplyToDate:Date;
		private var _satelliteFlag:YesNoBlankFlag;
		private var _hkidCaseNum:String;
		private var _pivasRptDispMethod:PivasRptDispMethod;
		private var _pivasRptItemCatFilterOption:PivasRptItemCatFilterOption;
		private var _itemCode:String;
		private var _drugKeyFlag:Boolean;
		private var _manufCode:String;
		private var _batchNum:String;
		
		public function RetrievePivasRptEvent( reportType:PivasReportType,  supplyFromDate:Date, supplyToDate:Date, 
											   satelliteFlag:YesNoBlankFlag, hkidCaseNum:String, pivasRptDispMethod:PivasRptDispMethod, pivasRptItemCatFilterOption:PivasRptItemCatFilterOption, 
											   itemCode:String, drugKeyFlag:Boolean, manufCode:String, batchNum:String ):void {
			super();
			_reportType = reportType;
			_supplyFromDate = supplyFromDate;
			_supplyToDate = supplyToDate;
			_satelliteFlag = satelliteFlag;
			_hkidCaseNum = hkidCaseNum;
			_pivasRptDispMethod = pivasRptDispMethod;
			_pivasRptItemCatFilterOption = pivasRptItemCatFilterOption;
			_itemCode = itemCode;
			_drugKeyFlag = drugKeyFlag;
			_manufCode = manufCode;
			_batchNum = batchNum;
		}
		
		public function get reportType():PivasReportType{
			return _reportType;
		} 

		public function get supplyFromDate():Date{
			return _supplyFromDate;
		} 
		
		public function get supplyToDate():Date{
			return _supplyToDate;
		}
		
		public function get satelliteFlag():YesNoBlankFlag{
			return _satelliteFlag;
		}
		
		public function get hkidCaseNum():String{
			return _hkidCaseNum;
		}
		
		public function get pivasRptDispMethod():PivasRptDispMethod{
			return _pivasRptDispMethod;
		}
		
		public function get pivasRptItemCatFilterOption():PivasRptItemCatFilterOption{
			return _pivasRptItemCatFilterOption;
		}
		
		public function get itemCode():String{
			return _itemCode;
		} 
		
		public function get drugKeyFlag():Boolean{
			return _drugKeyFlag;
		} 
		
		public function get manufCode():String{
			return _manufCode;
		} 
		
		public function get batchNum():String{
			return _batchNum;
		} 

	}
}