package hk.org.ha.control.pms.main.enquiry {

	import hk.org.ha.event.pms.main.enquiry.RefreshDeliveryRequestInfoListEvent;
	import hk.org.ha.event.pms.main.enquiry.RetrieveDeliveryRequestInfoListEvent;
	import hk.org.ha.model.pms.biz.enquiry.MdsExceptionEnqServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("mdsExceptionEnqServiceCtl", restrict="true")]
	public class MdsExceptionEnqServiceCtl {

		[In]
		public var mdsExceptionEnqService:MdsExceptionEnqServiceBean;

		[Observer]
		public function retrieveDeliveryRequestInfoList(evt:RetrieveDeliveryRequestInfoListEvent):void {
			mdsExceptionEnqService.retrieveDeliveryRequestInfoList(retrieveDeliveryRequestInfoListResult);
		}
		
		private function retrieveDeliveryRequestInfoListResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new RefreshDeliveryRequestInfoListEvent(evt.result as ArrayCollection));
		}
	}
}
