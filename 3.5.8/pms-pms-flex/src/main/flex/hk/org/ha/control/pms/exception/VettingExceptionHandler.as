package hk.org.ha.control.pms.exception {
	
	import hk.org.ha.event.pms.main.vetting.show.ShowOrderViewEvent;
	import hk.org.ha.model.pms.vo.sys.SysMsgMap;
	
	import mx.messaging.messages.ErrorMessage;
	
	import org.granite.tide.BaseContext;
	import org.granite.tide.IExceptionHandler;
	
	public class VettingExceptionHandler implements IExceptionHandler {

		[In]
		public var sysMsgMap:SysMsgMap;
		
		public static const VETTING_EXCEPTION:String = "VettingException";
		
		public function accepts(emsg:ErrorMessage):Boolean {
			return emsg.faultCode == VETTING_EXCEPTION;
		}
		
		public function handle(context:BaseContext, emsg:ErrorMessage):void {
			var evt:ShowOrderViewEvent = new ShowOrderViewEvent();
			evt.errorString = sysMsgMap.getMessage(emsg.extendedData["messageCode"], emsg.extendedData["messageParam"]); 
			context.dispatchEvent(evt);
		}
	}
}