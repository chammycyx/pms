package hk.org.ha.event.pms.main.checkissue
{
	import hk.org.ha.model.pms.vo.checkissue.CheckIssueViewInfo;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ReverseUncollectPrescEvent extends AbstractTideEvent 
	{	
		private var _checkIssueViewInfo:CheckIssueViewInfo;
		
		public function ReverseUncollectPrescEvent(checkIssueViewInfo:CheckIssueViewInfo):void 
		{
			super();
			_checkIssueViewInfo = checkIssueViewInfo;
		}
		
		public function get checkIssueViewInfo():CheckIssueViewInfo {
			return _checkIssueViewInfo;
		}
		
	}
}