package hk.org.ha.event.pms.main.reftable.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowItemLocationMaintPrepackPopupEvent extends AbstractTideEvent 
	{
		private var _itemCode:String;
		
		private var _refreshPrepackQtyHandler:Function;
		
		public function get itemCode():String
		{
			return _itemCode;
		}

		public function get refreshPrepackQtyHandler():Function
		{
			return _refreshPrepackQtyHandler;
		}
		
		public function ShowItemLocationMaintPrepackPopupEvent(itemCode:String, refreshPrepackQtyHandler:Function):void 
		{
			super();
			_itemCode = itemCode;
			_refreshPrepackQtyHandler = refreshPrepackQtyHandler;
		}
	}
}