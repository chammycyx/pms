package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dms.persistence.DmRegimen;
	import hk.org.ha.model.pms.dms.vo.BnfDrugInfo;
	import hk.org.ha.model.pms.dms.vo.DrugCommonDosage;
	import hk.org.ha.model.pms.dms.vo.DrugName;
	import hk.org.ha.model.pms.dms.vo.PreparationProperty;
	import hk.org.ha.model.pms.dms.vo.RouteForm;
	import hk.org.ha.model.pms.udt.drug.DrugScope;
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	import hk.org.ha.model.pms.udt.vetting.RegimenDurationUnit;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CreateSelectedDrugEvent extends AbstractTideEvent 
	{			
		private var _drugSearchSource:DrugSearchSource;
		private var _isQuickAdd:Boolean;
		private var _drugScope:DrugScope;
		private var _drugName:DrugName;
		private var _routeForm:RouteForm;
		private var _preparation:PreparationProperty;
		private var _commonDosage:DrugCommonDosage;
		private var _dmRegimen:DmRegimen;
		private var _carryDuration:Number;
		private var _carryDurationUnit:RegimenDurationUnit;
		private var _specialInstruction:String;
		private var _okCallbackEvent:Event;
		private var _cancelCallbackEvent:Event;

		public function CreateSelectedDrugEvent():void 
		{
			super();
		}

		public function get drugSearchSource():DrugSearchSource
		{
			return _drugSearchSource;
		}

		public function set drugSearchSource(drugSearchSource:DrugSearchSource):void
		{
			_drugSearchSource = drugSearchSource;
		}
		
		public function get isQuickAdd():Boolean
		{
			return _isQuickAdd;
		}

		public function set isQuickAdd(isQuickAdd:Boolean):void
		{
			_isQuickAdd = isQuickAdd;
		}
		
		public function get drugName():DrugName
		{
			return _drugName;
		}

		public function set drugName(drugName:DrugName):void
		{
			_drugName = drugName;
		}
		
		public function get routeForm():RouteForm
		{
			return _routeForm;
		}

		public function set routeForm(routeForm:RouteForm):void
		{
			_routeForm = routeForm;
		}
		
		public function get preparation():PreparationProperty
		{
			return _preparation;
		}

		public function set preparation(preparation:PreparationProperty):void
		{
			_preparation = preparation;
		}
		
		public function get commonDosage():DrugCommonDosage
		{
			return _commonDosage;
		}

		public function set commonDosage(commonDosage:DrugCommonDosage):void
		{
			_commonDosage = commonDosage;
		}
		
		public function get dmRegimen():DmRegimen
		{
			return _dmRegimen;
		}

		public function set dmRegimen(dmRegimen:DmRegimen):void
		{
			_dmRegimen = dmRegimen;
		}
		
		public function get drugScope():DrugScope
		{
			return _drugScope;
		}
		
		public function set drugScope(drugScope:DrugScope):void
		{
			_drugScope = drugScope;
		}
		
		
		public function get carryDuration():Number
		{
			return _carryDuration;
		}
		
		public function set carryDuration(carryDuration:Number):void
		{
			_carryDuration = carryDuration;
		}
		
		public function get carryDurationUnit():RegimenDurationUnit
		{
			return _carryDurationUnit;
		}
		
		public function set carryDurationUnit(carryDurationUnit:RegimenDurationUnit):void
		{
			_carryDurationUnit = carryDurationUnit;
		}
		
		public function get specialInstruction():String
		{
			return _specialInstruction;
		}
		
		public function set specialInstruction(specialInstruction:String):void
		{
			_specialInstruction = specialInstruction;
		}

		public function get okCallbackEvent():Event
		{
			return _okCallbackEvent;
		}
		
		public function set okCallbackEvent(okCallbackEvent:Event):void
		{
			_okCallbackEvent = okCallbackEvent;
		}
		
		public function get cancelCallbackEvent():Event
		{
			return _cancelCallbackEvent;
		}
		
		public function set cancelCallbackEvent(cancelCallbackEvent:Event):void
		{
			_cancelCallbackEvent = cancelCallbackEvent;
		}
	}
}