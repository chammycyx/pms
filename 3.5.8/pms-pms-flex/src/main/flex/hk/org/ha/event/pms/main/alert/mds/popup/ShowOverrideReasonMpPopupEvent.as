package hk.org.ha.event.pms.main.alert.mds.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;
	
	import mx.collections.ListCollectionView;
	
	public class ShowOverrideReasonMpPopupEvent extends AbstractTideEvent {
		
		public var alertMsg:AlertMsg;

		public var ddiAlertIndex:Number = NaN;
		
		public var alertEntityList:ListCollectionView;
		
		public var proceedFunc:Function = null;
		
		public function ShowOverrideReasonMpPopupEvent() {
			super();
		}
	}
}