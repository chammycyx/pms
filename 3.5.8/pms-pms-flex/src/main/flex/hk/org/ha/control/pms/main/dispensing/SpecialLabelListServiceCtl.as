package hk.org.ha.control.pms.main.dispensing {

	import hk.org.ha.event.pms.main.dispensing.RetrieveSpecialLabelListEvent;
	import hk.org.ha.event.pms.main.dispensing.SpecialLabelPrintViewEvent;
	import hk.org.ha.fmk.pms.flex.components.lookup.RefreshLookupPopupDataProviderEvent;
	import hk.org.ha.model.pms.biz.label.SpecialLabelListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;

    [Bindable]
    [Name("specialLabelListServiceCtl", restrict="true")]
    public class SpecialLabelListServiceCtl 
	{
    
		[In]
		public var specialLabelListService:SpecialLabelListServiceBean;
		
		[In]
		public var specialLabelList:ArrayCollection;

		[Observer]
		public function retrieveSpecialLabelList(evt:RetrieveSpecialLabelListEvent):void
		{
			specialLabelListService.retrieveSpecialLabelList(evt.labelNumPrefix, retrieveSpecialLabelListResult);
		}
		
		private function retrieveSpecialLabelListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshLookupPopupDataProviderEvent(specialLabelList));
		}
		
		[Observer]
		public function printSpecialLabel(evt:SpecialLabelPrintViewEvent):void 
		{ 
			specialLabelListService.printSpecialLabel(evt.specialLabel);
		}
    }
}
