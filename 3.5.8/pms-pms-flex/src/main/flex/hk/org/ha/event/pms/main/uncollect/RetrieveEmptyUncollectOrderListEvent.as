package hk.org.ha.event.pms.main.uncollect {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveEmptyUncollectOrderListEvent extends AbstractTideEvent 
	{		
		private var _retrieveEmpty:Boolean;
		
		public function RetrieveEmptyUncollectOrderListEvent(retrieveEmpty:Boolean):void 
		{
			super();
			_retrieveEmpty = retrieveEmpty;
		}
		
		public function get retrieveEmpty():Boolean
		{
			return _retrieveEmpty;
		}
	}
}