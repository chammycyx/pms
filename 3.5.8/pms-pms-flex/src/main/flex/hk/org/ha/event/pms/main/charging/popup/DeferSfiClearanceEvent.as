package hk.org.ha.event.pms.main.charging.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.udt.charging.ClearanceStatus;
	
	public class DeferSfiClearanceEvent extends AbstractTideEvent 
	{	
		private var _actionBy:String;
		private var _dispOrderId:Number;
		
		public function DeferSfiClearanceEvent(actionBy:String, dispOrderId:Number):void 
		{
			super();
			_actionBy = actionBy;
			_dispOrderId = dispOrderId;
		}
		
		public function get actionBy():String{
			return _actionBy;
		}
		
		public function get dispOrderId():Number{
			return _dispOrderId;
		}
	}
}