package hk.org.ha.event.pms.main.vetting.show {
	
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowMedOrderEditViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;				
		
		private var _moSelectedIndex:Number;
		
		public function ShowMedOrderEditViewEvent(moSelectedIndex:Number, clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
			_moSelectedIndex = moSelectedIndex;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
		
		public function get moSelectedIndex():Number {
			return _moSelectedIndex;
		}
	}
}