package hk.org.ha.event.pms.main.vetting.mp.popup
{
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowOrderAmendmentHistoryPopupEvent extends AbstractTideEvent
	{
		private var _medProfileMoItem:MedProfileMoItem;
		
		public function ShowOrderAmendmentHistoryPopupEvent(medProfileMoItem:MedProfileMoItem)
		{
			super();
			_medProfileMoItem = medProfileMoItem;
		}
		
		public function get medProfileMoItem():MedProfileMoItem
		{
			return _medProfileMoItem;
		}
	}
}