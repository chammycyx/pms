package hk.org.ha.event.pms.main.reftable {
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateChargeSpecialtyListEvent extends AbstractTideEvent 
	{
		private var _sfiChargeSpecialtyList:ListCollectionView;
		
		private var _snChargeSpecialtyList:ListCollectionView;
		
		public function UpdateChargeSpecialtyListEvent(sfiChargeSpecialtyList:ListCollectionView, snChargeSpecialtyList:ListCollectionView):void 
		{
			super();
			_sfiChargeSpecialtyList = sfiChargeSpecialtyList;
			_snChargeSpecialtyList = snChargeSpecialtyList;
		}
	
		public function get sfiChargeSpecialtyList():ListCollectionView{
			return _sfiChargeSpecialtyList;
		}
		
		public function get snChargeSpecialtyList():ListCollectionView{
			return _snChargeSpecialtyList;
		}
	}
}