package hk.org.ha.event.pms.main.inbox
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveErrorStatEvent extends AbstractTideEvent
	{
		public function RetrieveErrorStatEvent()
		{
			super();
		}
	}
}