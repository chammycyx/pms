package hk.org.ha.event.pms.main.drug {
	
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CreateBnfTreeEvent extends AbstractTideEvent 
	{			
		private var _drugSearchSource:DrugSearchSource;
		private var _drugSearchBnfMapList:ArrayCollection;
		
		public function CreateBnfTreeEvent(drugSearchSource:DrugSearchSource, drugSearchBnfMapList:ArrayCollection):void 
		{
			super();
			_drugSearchSource = drugSearchSource;
			_drugSearchBnfMapList = drugSearchBnfMapList;
		}
		
		public function get drugSearchSource():DrugSearchSource {
			return _drugSearchSource;
		}
		
		public function get drugSearchBnfMapList():ArrayCollection {
			return _drugSearchBnfMapList;
		}
	}
}