package hk.org.ha.event.pms.main.prevet
{	
	import flash.events.Event;
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.disp.MedOrder;
	
	public class DeleteRxFromPreVetEvent extends AbstractTideEvent
	{		
		private var _userId:String;
		private var _remarkText:String;
		private var _medOrder:MedOrder;
		
		public function DeleteRxFromPreVetEvent(userId:String, remarkText:String, medOrder:MedOrder)
		{
			super();
			_userId = userId;
			_remarkText = remarkText;
			_medOrder = medOrder;
		}
		
		public function get remarkText():String
		{
			return _remarkText;
		}
		
		public function get userId():String
		{
			return _userId;
		}
		
		public function get medOrder():MedOrder
		{
			return _medOrder;
		}
	}
}