package hk.org.ha.event.pms.main.vetting.mp {

	import flash.events.Event;
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	import mx.collections.ListCollectionView;
	
	public class SaveItemFromManualEntryItemEditEvent extends AbstractTideEvent {
		
		private var _medProfileItemList:ListCollectionView; 
		private var	_callBackEvent:Event;
		private var _callBackFunc:Function;
		
		public function SaveItemFromManualEntryItemEditEvent(medProfileItemList:ListCollectionView, eventValue:Event=null, funcValue:Function=null) 
		{
			super();			
			_medProfileItemList = medProfileItemList;
			_callBackEvent = eventValue;
			_callBackFunc = funcValue;
		}		
		
		public function get medProfileItemList():ListCollectionView
		{
			return _medProfileItemList;
		}
		
		public function get callBackEvent():Event
		{
			return _callBackEvent;
		}
		
		public function get callBackFunc():Function
		{
			return _callBackFunc;
		}
	}
}