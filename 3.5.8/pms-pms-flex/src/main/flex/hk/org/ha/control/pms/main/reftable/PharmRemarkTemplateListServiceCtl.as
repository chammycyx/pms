package hk.org.ha.control.pms.main.reftable {
	
	import hk.org.ha.event.pms.main.reftable.RefreshPharmRemarkTemplateListEvent;
	import hk.org.ha.event.pms.main.reftable.RefreshUpdatePharmRemarkTemplateListEvent;
	import hk.org.ha.event.pms.main.reftable.RetrievePharmRemarkTemplateListEvent;
	import hk.org.ha.event.pms.main.reftable.UpdatePharmRemarkTemplateListEvent;
	import hk.org.ha.model.pms.biz.reftable.PharmRemarkTemplateListServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	
	[Bindable]
	[Name("pharmRemarkTemplateListServiceCtl", restrict="true")]
	public class PharmRemarkTemplateListServiceCtl 
	{
		[In]
		public var pharmRemarkTemplateListService:PharmRemarkTemplateListServiceBean;
		
		[Observer]
		public function retrievePharmRemarkTemplateList(evt:RetrievePharmRemarkTemplateListEvent):void
		{	
			pharmRemarkTemplateListService.retrievePharmRemarkTemplateList(retrievePharmRemarkTemplateListResult);
		}
		
		private function retrievePharmRemarkTemplateListResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new RefreshPharmRemarkTemplateListEvent());
		}
		
		[Observer]
		public function updatePharmRemarkTemplateList(evt:UpdatePharmRemarkTemplateListEvent):void
		{
			pharmRemarkTemplateListService.updatePharmRemarkTemplateList(evt.pharmRemarkTemplateList, updatePharmRemarkTemplateListResult);
		}
		
		private function updatePharmRemarkTemplateListResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new RefreshUpdatePharmRemarkTemplateListEvent());
		}
		
		
	}
}
