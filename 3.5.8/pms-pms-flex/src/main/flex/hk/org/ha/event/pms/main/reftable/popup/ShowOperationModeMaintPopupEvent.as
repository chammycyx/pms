package hk.org.ha.event.pms.main.reftable.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowOperationModeMaintPopupEvent extends AbstractTideEvent 
	{
		private var _operationModeMaintPopupProp:OperationModeMaintPopupProp;
		
		public function ShowOperationModeMaintPopupEvent(operationModeMaintPopupProp:OperationModeMaintPopupProp):void 
		{
			super();
			_operationModeMaintPopupProp = operationModeMaintPopupProp;
		}
		
		public function get operationModeMaintPopupProp():OperationModeMaintPopupProp {
			return _operationModeMaintPopupProp;
		}
	}
}