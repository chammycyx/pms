package hk.org.ha.event.pms.main.pivas
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePivasFormulaDrugListByPrefixDisplayNameEvent extends AbstractTideEvent
	{
		private var _prefixDisplayName:String;
		private var _callbackFunc:Function;
		
		public function RetrievePivasFormulaDrugListByPrefixDisplayNameEvent(prefixDisplayName:String, callbackFunc:Function = null)
		{
			super();
			_prefixDisplayName = prefixDisplayName;
			_callbackFunc = callbackFunc;
		}
		
		public function get prefixDisplayName():String {
			return _prefixDisplayName;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}