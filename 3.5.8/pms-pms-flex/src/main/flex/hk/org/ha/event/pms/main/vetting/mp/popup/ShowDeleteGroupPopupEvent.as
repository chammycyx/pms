package hk.org.ha.event.pms.main.vetting.mp.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
	
	import mx.collections.ListCollectionView;
	
	public class ShowDeleteGroupPopupEvent extends AbstractTideEvent {
		
		private var _medProfileItem:MedProfileItem;
		
		private var _medProfileItemList:ListCollectionView;
		
		private var _purchaseRequestList:ListCollectionView;
		
		private var _modifyListFlag:Boolean;
		
		private var _callback:Function;
		
		private var _cancelCallback:Function;
		
		public function ShowDeleteGroupPopupEvent(medProfileItem:MedProfileItem, medProfileItemList:ListCollectionView, purchaseRequestList:ListCollectionView, modifyListFlag:Boolean, callback:Function=null, cancelCallback:Function=null) {
			super();
			_medProfileItem = medProfileItem;
			_medProfileItemList = medProfileItemList;
			_purchaseRequestList = purchaseRequestList;
			_modifyListFlag = modifyListFlag;
			_callback = callback == null ? function():void{} : callback;
			_cancelCallback = cancelCallback == null ? function():void{} : cancelCallback;
		}
		
		public function get medProfileItem():MedProfileItem {
			return _medProfileItem;
		}
		
		public function get medProfileItemList():ListCollectionView {
			return _medProfileItemList;
		}
		
		public function get purchaseRequestList():ListCollectionView {
			return _purchaseRequestList;
		}
		
		public function get modifyListFlag():Boolean {
			return _modifyListFlag;
		}

		public function get callback():Function {
			return _callback;
		}
		
		public function get cancelCallback():Function {
			return _cancelCallback;
		}
	}
}