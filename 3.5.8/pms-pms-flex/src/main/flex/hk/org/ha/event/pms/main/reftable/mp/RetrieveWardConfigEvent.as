package hk.org.ha.event.pms.main.reftable.mp {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveWardConfigEvent extends AbstractTideEvent 
	{
		private var _wardCode:String;
		
		public function get wardCode():String
		{
			return _wardCode;
		}

		public function RetrieveWardConfigEvent(wardCode:String):void 
		{
			super();
			_wardCode = wardCode;
		}

	}
}