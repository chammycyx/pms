package hk.org.ha.event.pms.main.lookup.popup
{
	public class ManufCodeLookupPopupProp
	{
		public var doubleClickHandler:Function;
		public var okHandler:Function;
		public var cancelHandler:Function;
		public var prefixManufCode:String;
	}
}