package hk.org.ha.event.pms.main.reftable {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateRefillHolidayAdvDueDateEvent extends AbstractTideEvent 
	{		
		private var _advDueDate:Number;
		
		public function UpdateRefillHolidayAdvDueDateEvent(advDueDate:Number):void 
		{
			super();
			_advDueDate = advDueDate;
		}
		
		public function get advDueDate():Number{
			return _advDueDate;
		}
	}
}