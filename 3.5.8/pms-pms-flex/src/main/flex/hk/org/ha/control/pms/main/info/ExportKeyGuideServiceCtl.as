package hk.org.ha.control.pms.main.info {
	import hk.org.ha.event.pms.main.info.ExportKeyGuideEvent;
	import hk.org.ha.model.pms.biz.info.ExportKeyGuideServiceBean;
	import hk.org.ha.view.pms.main.info.popup.KeyGuidePopup;
	
	[Bindable]
	[Name("exportKeyGuideServiceCtl", restrict="true")]
	public class ExportKeyGuideServiceCtl {

		import org.granite.tide.events.TideResultEvent;
		import org.granite.tide.seam.In;
		
		[In]
		public var exportKeyGuideService:ExportKeyGuideServiceBean;
		
		private var keyGuidePopup:KeyGuidePopup;
		
		[Observer]
		public function exportKeyGuide(evt:ExportKeyGuideEvent):void {
			keyGuidePopup = evt.keyGuidePopup;
			exportKeyGuideService.exportKeyGuide(evt.exportKeyList, exportKeyGuideResult);
		}
		
		private function exportKeyGuideResult(evt:TideResultEvent):void
		{
			keyGuidePopup.generateReport();
		}
	}
}
