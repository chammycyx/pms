package hk.org.ha.event.pms.main.vetting.mp
{
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SplitChemoMedProfileItemEvent extends AbstractTideEvent
	{
		private var _medProfileItem:MedProfileItem;
		private var _chemoItemList:ArrayCollection;
		private var _callbackFunc:Function;
		
		public function SplitChemoMedProfileItemEvent(medProfileItem:MedProfileItem, chemoItemList:ArrayCollection, callbackFunc:Function)
		{
			super();
			_medProfileItem = medProfileItem;
			_chemoItemList = chemoItemList;
			_callbackFunc = callbackFunc;
		}
		
		public function get medProfileItem():MedProfileItem
		{
			return _medProfileItem;
		}
		
		public function get chemoItemList():ArrayCollection
		{
			return _chemoItemList;
		}
		
		public function get callbackFunc():Function
		{
			return _callbackFunc;
		}
	}
}