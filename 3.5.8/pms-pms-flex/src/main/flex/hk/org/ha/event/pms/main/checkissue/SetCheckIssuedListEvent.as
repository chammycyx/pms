package hk.org.ha.event.pms.main.checkissue
{
	import hk.org.ha.model.pms.vo.checkissue.CheckIssueListSummary;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetCheckIssuedListEvent extends AbstractTideEvent 
	{		
		private var _checkIssueListSummary:CheckIssueListSummary;
		private var _currentList:String;
		
		public function SetCheckIssuedListEvent(checkIssueListSummary:CheckIssueListSummary, currentList:String):void 
		{
			super();
			_checkIssueListSummary = checkIssueListSummary;
			_currentList= currentList;
		}
		
		public function get checkIssueListSummary():CheckIssueListSummary{
			return _checkIssueListSummary;
		}
		
		public function get currentList():String{
			return _currentList;
		}
	}
}