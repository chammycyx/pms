package hk.org.ha.event.pms.main.onestop {
		
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ClearKeyboardBufferEvent extends AbstractTideEvent 
	{			
		public function ClearKeyboardBufferEvent():void 
		{
			super();
		}        
	}
}