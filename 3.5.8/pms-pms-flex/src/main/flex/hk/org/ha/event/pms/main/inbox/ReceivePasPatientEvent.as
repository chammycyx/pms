package hk.org.ha.event.pms.main.inbox
{
	import hk.org.ha.model.pms.persistence.disp.Patient;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ReceivePasPatientEvent extends AbstractTideEvent
	{
		private var _patient:Patient;
		
		public function ReceivePasPatientEvent(patient:Patient)
		{
			super();
			_patient = patient;
		}
		
		public function patient():Patient
		{
			return _patient;
		}
	}
}