package hk.org.ha.event.pms.main.support
{
	import org.granite.tide.events.AbstractTideEvent;

	public class ClearWorkstationPropMaintViewEvent extends AbstractTideEvent 
	{
		public function ClearWorkstationPropMaintViewEvent():void
		{
			super();
		}
	}
}