package hk.org.ha.event.pms.main.vetting.popup {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.vo.charging.HandleExemptAuthInfo;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ValidateHandlingExemptAuthInfoEvent extends AbstractTideEvent 
	{		
		private var _handleExemptAuthInfo:HandleExemptAuthInfo;
		private var _validateCallbackFunc:Function;
		
		public function ValidateHandlingExemptAuthInfoEvent(handleExemptAuthInfo:HandleExemptAuthInfo, validateCallbackFunc:Function):void 
		{
			super();
			_handleExemptAuthInfo = handleExemptAuthInfo;
			_validateCallbackFunc = validateCallbackFunc;
		}
		
		public function get handleExemptAuthInfo():HandleExemptAuthInfo {
			return _handleExemptAuthInfo;
		}
		
		public function get validateCallbackFunc():Function {
			return _validateCallbackFunc;
		}
		
	}
}
