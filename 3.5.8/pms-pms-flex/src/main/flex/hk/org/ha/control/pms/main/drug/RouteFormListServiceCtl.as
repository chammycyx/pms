package hk.org.ha.control.pms.main.drug {		
	
	import hk.org.ha.event.pms.main.drug.RetrieveRouteFormListEvent;
	import hk.org.ha.model.pms.biz.drug.RouteFormListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("routeFormListServiceCtl", restrict="true")]
	public class RouteFormListServiceCtl 
	{
		[In]
		public var routeFormListService:RouteFormListServiceBean;
		
		[In]
		public var ctx:Context;
		
		private var callbackFunc:Function;
		
		[Observer]
		public function RetrieveRouteFormList(evt:RetrieveRouteFormListEvent):void
		{
			callbackFunc = evt.callbackFunc;
			routeFormListService.retrieveRouteFormList(evt.routeFormSearchCriteria,RetrieveRouteFormListResult);
		}
		
		private function RetrieveRouteFormListResult(evt:TideResultEvent):void 
		{			
			if ( callbackFunc != null ) {
				callbackFunc.call(null,evt.result);
			}
		}
	}
}
