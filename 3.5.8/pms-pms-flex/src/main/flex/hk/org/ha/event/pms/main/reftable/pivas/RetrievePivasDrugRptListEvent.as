package hk.org.ha.event.pms.main.reftable.pivas {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePivasDrugRptListEvent extends AbstractTideEvent 
	{
		private var _callbackFunc:Function;
		
		public function RetrievePivasDrugRptListEvent(callbackFunc:Function):void {
			_callbackFunc = callbackFunc;
			super();
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}