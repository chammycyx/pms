package hk.org.ha.event.pms.main.enquiry
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSfiDrugPriceHistoryEvent extends AbstractTideEvent 
	{
		private var _itemCodeList:ArrayCollection;
		private var _drugPriceMonth:Date;
		
		public function RetrieveSfiDrugPriceHistoryEvent(itemCodeList:ArrayCollection, drugPriceMonth:Date):void
		{
			super();
			_itemCodeList = itemCodeList;
			_drugPriceMonth = drugPriceMonth;
		}
		
		public function get itemCodeList():ArrayCollection
		{
			return _itemCodeList;
		}
		
		public function get drugPriceMonth():Date
		{
			return _drugPriceMonth;
		}
	}
}