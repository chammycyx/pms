package hk.org.ha.control.pms.main.drug {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.drug.RetrieveDmDrugListByDisplayNameEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveDmDrugListByDrugKeyFmStatusEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveDmDrugListByDsfSearchCriteriaFmStatusEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveDmDrugListByFullDrugDescEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveDmDrugListEvent;
	import hk.org.ha.model.pms.biz.drug.DmDrugListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("dmDrugListServiceCtl", restrict="true")]
	public class DmDrugListServiceCtl 
	{
		[In]
		public var dmDrugListService:DmDrugListServiceBean;		
		
		[In]
		public var dmDrugList:ArrayCollection;
		
		[In]
		public var ctx:Context;
		
		private var callBackEvent:Event;
		
		[Observer]
		public function retrieveDmDrugList(evt:RetrieveDmDrugListEvent):void
		{
			dmDrugListService.retrieveDmDrugList(evt.itemCode, evt.showNonSuspendItemOnly);
		}
		
		[Observer]
		public function retrieveDmDrugListByFullDrugDesc(evt:RetrieveDmDrugListByFullDrugDescEvent):void
		{
			dmDrugListService.retrieveDmDrugListByFullDrugDesc(evt.fullDrugDesc);
		}
		
		[Observer]
		public function retrieveDmDrugListByDrugKeyFmStatus(evt:RetrieveDmDrugListByDrugKeyFmStatusEvent):void
		{	
			callBackEvent = evt.callBackEvent;
			dmDrugListService.retrieveDmDrugListByDrugKeyFmStatus( evt.drugKey, evt.fmStatus, retrieveDmDrugListResult );
		}
		
		[Observer]
		public function retrieveDmDrugListByDsfAndFmStatus(evt:RetrieveDmDrugListByDsfSearchCriteriaFmStatusEvent):void
		{
			callBackEvent = evt.callBackEvent;
			dmDrugListService.retrieveDmDrugListByDsfSearchCriteriaFmStatus(evt.criteria, evt.fmStatus, retrieveDmDrugListResult);
		}
		
		private function retrieveDmDrugListResult(evt:TideResultEvent):void 
		{			
			if( callBackEvent != null ) {
				evt.context.dispatchEvent( callBackEvent );				
			}
		}		
		
		[Observer]
		public function retrieveDmDrugListByDisplayName(evt:RetrieveDmDrugListByDisplayNameEvent):void 
		{
			dmDrugListService.retrieveDmDrugListByDisplayNameFmStatus( evt.displayName, evt.fmStatus );
		}
	}
}