package hk.org.ha.event.pms.main.reftable.pivas {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RefreshPivasWardListEvent extends AbstractTideEvent {
		private var _patHospCode:String;
		
		public function RefreshPivasWardListEvent(patHospCode:String) {
			super();
			_patHospCode = patHospCode;
		}
		
		public function get patHospCode():String {
			return _patHospCode;
		}
	}
}