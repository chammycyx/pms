package hk.org.ha.control.pms.main.reftable {
			
	import hk.org.ha.event.pms.main.reftable.RetrieveFloatingDrugNameEvent;
	import hk.org.ha.model.pms.biz.reftable.FdnMappingServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("fdnMappingServiceCtl", restrict="true")]
	public class FdnMappingServiceCtl 
	{
		[In]
		public var fdnMappingService:FdnMappingServiceBean;
		
		private var callBackFunc:Function;
		
		[Observer]
		public function retrieveFloatingDrugName(evt:RetrieveFloatingDrugNameEvent):void
		{
			callBackFunc = evt.callBackFunc;
			fdnMappingService.retrieveFloatingDrugName(evt.itemCode, evt.orderType, retrieveFloatingDrugNameResult);
		}
		
		public function retrieveFloatingDrugNameResult(evt:TideResultEvent):void
		{
			if ( callBackFunc != null ) {
				callBackFunc.call(null,evt.result);
			}
		}
		
	}
}
