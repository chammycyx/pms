package hk.org.ha.control.pms.support{
	
	import hk.org.ha.event.pms.main.support.RefreshCorporatePropMaintViewEvent;
	import hk.org.ha.event.pms.main.support.RetrieveCorporatePropListEvent;
	import hk.org.ha.event.pms.main.support.UpdateCorporatePropListEvent;
	import hk.org.ha.model.pms.biz.support.CorporatePropListServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("corporatePropListServiceCtl", restrict="true")]
	public class CorporatePropListServiceCtl 
	{
		[In]
		public var corporatePropListService:CorporatePropListServiceBean;
	
		
		[Observer]
		public function retrieveCorporatePropList(evt:RetrieveCorporatePropListEvent):void
		{
			corporatePropListService.retrieveCorporatePropList();
		}
		
		[Observer]
		public function updateCorporatePropList(evt:UpdateCorporatePropListEvent):void
		{
			corporatePropListService.updateCorporatePropList(evt.corpPropList, updateCorporatePropListResult);
		}
		
		private function updateCorporatePropListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshCorporatePropMaintViewEvent());
		}
	}
}
