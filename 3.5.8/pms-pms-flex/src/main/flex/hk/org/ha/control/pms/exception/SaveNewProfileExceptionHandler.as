package hk.org.ha.control.pms.exception
{
	import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	
	import mx.messaging.messages.ErrorMessage;
	
	import org.granite.tide.BaseContext;
	import org.granite.tide.IExceptionHandler;

	public class SaveNewProfileExceptionHandler implements IExceptionHandler
	{
		public static const SAVE_NEW_PROFILE_EXCEPTION:String = "SaveNewProfileException";
		
		public function SaveNewProfileExceptionHandler()
		{
		}
		
		public function accepts(emsg:ErrorMessage):Boolean
		{
			return emsg.faultCode == SAVE_NEW_PROFILE_EXCEPTION;
		}
		
		public function handle(context:BaseContext, emsg:ErrorMessage):void
		{
			var popupProp:SystemMessagePopupProp = new SystemMessagePopupProp();
			popupProp.messageCode = "0755";
			popupProp.setOkButtonOnly = true;
			context.dispatchEvent(new RetrieveSystemMessageEvent(popupProp));
		}
	}
}