package hk.org.ha.event.pms.main.drug {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.rx.DhRxDrug;
	import hk.org.ha.view.pms.main.vetting.popup.OrderEditItemPopup;
	
	public class RetrieveWorkstoreDrugListByDhRxDrugEvent extends AbstractTideEvent {		
		
		private var _dhRxDrug:DhRxDrug;
		private var _itemCodePrefix:String;
		private var _orderEditItemPopup:OrderEditItemPopup;
		
		public function RetrieveWorkstoreDrugListByDhRxDrugEvent(dhRxDrug:DhRxDrug, itemCodePrefix:String, orderEditItemPopup:OrderEditItemPopup=null) {
			super();			
			_dhRxDrug = dhRxDrug;
			_itemCodePrefix = itemCodePrefix;
			_orderEditItemPopup = orderEditItemPopup;
		}		
		
		public function get dhRxDrug():DhRxDrug {
			return _dhRxDrug;
		}
		
		public function get itemCodePrefix():String {
			return _itemCodePrefix;
		}
		
		public function get orderEditItemPopup():OrderEditItemPopup {
			return _orderEditItemPopup;
		}
	}
}