package hk.org.ha.event.pms.main.report {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshRefillRptEvent extends AbstractTideEvent 
	{
		private var _retrieveSuccess:Boolean;
		private var _validItemCode:Boolean;
		
		public function RefreshRefillRptEvent(retrieveSuccess:Boolean, validItemCode:Boolean):void 
		{
			super();
			_retrieveSuccess = retrieveSuccess;
			_validItemCode = validItemCode;
		}
		
		public function get retrieveSuccess():Boolean
		{
			return _retrieveSuccess;
		}
		
		public function get validItemCode():Boolean
		{
			return _validItemCode;
		}
		
	}
}