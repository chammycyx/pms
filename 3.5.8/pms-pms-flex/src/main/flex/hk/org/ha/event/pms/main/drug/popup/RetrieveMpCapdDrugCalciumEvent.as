package hk.org.ha.event.pms.main.drug.popup {
		
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveMpCapdDrugCalciumEvent extends AbstractTideEvent 
	{			
		private var _drugSearchSource:DrugSearchSource;	
		private var _supplierSystem:String;
		
		public function RetrieveMpCapdDrugCalciumEvent(drugSearchSource:DrugSearchSource, supplierSystem:String):void 
		{
			super();
			_drugSearchSource = drugSearchSource;
			_supplierSystem = supplierSystem;
		}
		
		public function get drugSearchSource():DrugSearchSource {
			return _drugSearchSource;
		}
		
		public function get supplierSystem():String {
			return _supplierSystem;
		}
	}
}