package hk.org.ha.event.pms.main.report {
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class PrintUncollectPrescRptEvent extends AbstractTideEvent 
	{		
		private var _uncollectPrescRptList:ArrayCollection;
		
		public function PrintUncollectPrescRptEvent(uncollectPrescRptList:ArrayCollection):void 
		{
			super();
			_uncollectPrescRptList = uncollectPrescRptList;
		}
		
		public function get uncollectPrescRptList():ArrayCollection {
			return _uncollectPrescRptList;
		}
	}
}