package hk.org.ha.event.pms.main.prevet
{
	import hk.org.ha.model.pms.persistence.disp.Patient;
	import hk.org.ha.model.pms.udt.prevet.PreVetMode;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDischargeMedOrderEvent extends AbstractTideEvent
	{
		private var _orderNum:String;
		private var _preVetMode:PreVetMode;
		private var _patient:Patient;
		private var _orderId:Number;
		private var _medOrderVersion:Number;
		
		public function RetrieveDischargeMedOrderEvent(orderNum:String, preVetMode:PreVetMode, patient:Patient=null, 
													   orderId:Number=NaN, medOrderVersion:Number=NaN)
		{
			super();
			_orderNum = orderNum;
			_preVetMode = preVetMode;
			_patient = patient;
			_orderId = orderId;
			_medOrderVersion = medOrderVersion;
		}
		
		public function get orderNum():String
		{
			return _orderNum;
		}
		
		public function get preVetMode():PreVetMode
		{
			return _preVetMode;
		}
		
		public function get patient():Patient
		{
			return _patient;
		}
		
		public function get orderId():Number
		{
			return _orderId;
		}
		
		public function get medOrderVersion():Number
		{
			return _medOrderVersion;
		}
	}
}