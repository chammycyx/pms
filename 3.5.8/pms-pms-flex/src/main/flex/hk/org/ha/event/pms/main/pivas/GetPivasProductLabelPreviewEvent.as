package hk.org.ha.event.pms.main.pivas
{
	import hk.org.ha.model.pms.persistence.pivas.PivasBatchPrep;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class GetPivasProductLabelPreviewEvent extends AbstractTideEvent 
	{		
		private var _pivasBatchPrep:PivasBatchPrep;
		private var _quickAddPivasBatchPrep:Boolean;
		
		public function GetPivasProductLabelPreviewEvent(pivasBatchPrep:PivasBatchPrep, quickAddPivasBatchPrep:Boolean):void 
		{
			super();
			_pivasBatchPrep = pivasBatchPrep;
			_quickAddPivasBatchPrep = quickAddPivasBatchPrep;
		}

		public function get pivasBatchPrep():PivasBatchPrep {
			return _pivasBatchPrep;
		}
		
		public function get quickAddPivasBatchPrep():Boolean
		{
			return _quickAddPivasBatchPrep;
		}
		
	}
}