package hk.org.ha.event.pms.main.vetting {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class SetCapdManualVoucherGridColFocusEvent extends AbstractTideEvent 
	{
		private var _manualVoucherGridCol:Number;
		private var _manualVoucherNum:String;
		private var _callOkBtn:Boolean;
		
		public function SetCapdManualVoucherGridColFocusEvent(manualVoucherGridCol:Number, manualVoucherNum:String, callOkBtn:Boolean) {
			super();
			_manualVoucherGridCol = manualVoucherGridCol;
			_manualVoucherNum = manualVoucherNum;
			_callOkBtn = callOkBtn;
		}
		
		public function get manualVoucherGridCol():Number {
			return _manualVoucherGridCol;
		}
		
		public function get manualVoucherNum():String {
			return _manualVoucherNum;
		}
		
		public function get callOkBtn():Boolean {
			return _callOkBtn;
		}
	}
}
