package hk.org.ha.event.pms.main.report {
	
	import hk.org.ha.model.pms.udt.printing.PrintLang;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class PrintMedSummaryRptEvent extends AbstractTideEvent 
	{
		private var _dispDate:Date;
		private var _ticketNum:String;
		private var _printLang:PrintLang;
		
		public function PrintMedSummaryRptEvent(dispDate:Date, ticketNum:String, printLang:PrintLang):void 
		{
			super();
			_dispDate = dispDate;
			_ticketNum = ticketNum;
			_printLang = printLang;
		}
		
		public function get dispDate():Date {
			return _dispDate;
		}
		
		public function get ticketNum():String {
			return _ticketNum;
		}
		
		public function get printLang():PrintLang {
			return _printLang;
		}
	}
}