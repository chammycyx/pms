package hk.org.ha.event.pms.main.drug.show {
	
	import flash.events.Event;
	import flash.utils.Dictionary;
	
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDrugSearchViewEvent extends AbstractTideEvent 
	{
		private var _drugSearchSource:DrugSearchSource;
		
		private var _clearMessages:Boolean;
		private var _okCallbackEvent:Event;
		private var _cancelCallbackEvent:Event = null;
		private var _cancelClearCallbackEvent:Event = null;	// for dosage conversion view
		private var _windowCloseFunc:Function = null;	// for dosage conversion view 
		private var _clearScreen:Boolean = true;
		private var _firstItem:Boolean = false;
		private var _enableChest:Boolean = false;
		private var _searchKey:String = null;	// for dosage conversion view
		private var _inPatientDefault:Boolean = false;	//PMS-3517
		private var _properties:Dictionary = null;
		
		public function ShowDrugSearchViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
		
		public function get drugSearchSource():DrugSearchSource {
			return _drugSearchSource;
		}
		
		public function set drugSearchSource(drugSearchSource:DrugSearchSource):void {
			_drugSearchSource = drugSearchSource;
		}
		
		public function get okCallbackEvent():Event {
			return _okCallbackEvent;
		}
		
		public function set okCallbackEvent(okCallbackEvent:Event):void {
			_okCallbackEvent = okCallbackEvent;
		}
		
		public function get cancelCallbackEvent():Event {
			return _cancelCallbackEvent;
		}
		
		public function set cancelCallbackEvent(cancelCallbackEvent:Event):void {
			_cancelCallbackEvent = cancelCallbackEvent;
		}
		
		public function get cancelClearCallbackEvent():Event {
			return _cancelClearCallbackEvent;
		}
		
		public function set cancelClearCallbackEvent(cancelClearCallbackEvent:Event):void {
			_cancelClearCallbackEvent = cancelClearCallbackEvent;
		}
		
		public function get windowCloseFunc():Function {
			return _windowCloseFunc;
		}
		
		public function set windowCloseFunc(windowCloseFunc:Function):void {
			_windowCloseFunc = windowCloseFunc;
		}
		
		public function get clearScreen():Boolean {
			return _clearScreen;
		}
		
		public function set clearScreen(clearScreen:Boolean):void {
			_clearScreen = clearScreen;
		}
		
		public function get firstItem():Boolean {
			return _firstItem;
		}
		
		public function set firstItem(firstItem:Boolean):void {
			_firstItem = firstItem;
		}
		
		public function get enableChest():Boolean {
			return _enableChest;
		}
		
		public function set enableChest(enableChest:Boolean):void {
			_enableChest = enableChest;
		}
		
		public function get searchKey():String {
			return _searchKey;
		}
		
		public function set searchKey(searchKey:String):void {
			_searchKey = searchKey;
		}
		
		public function get inPatientDefault():Boolean {
			return _inPatientDefault;
		}
		
		public function set inPatientDefault(inPatientDefault:Boolean):void {
			_inPatientDefault = inPatientDefault;
		}
		
		public function get properties():Dictionary {
			return _properties;
		}
		
		public function set properties(properties:Dictionary):void {
			_properties = properties;
		}
		
		public override function clone():Event {
			var evt:ShowDrugSearchViewEvent = new ShowDrugSearchViewEvent(clearMessages);
			evt.drugSearchSource			= drugSearchSource;
			evt.okCallbackEvent     		= okCallbackEvent;
			evt.cancelCallbackEvent 		= cancelCallbackEvent;
			evt.cancelClearCallbackEvent	= cancelClearCallbackEvent;
			evt.clearScreen         		= false;
			evt.firstItem           		= firstItem;
			evt.enableChest       			= enableChest;
			evt.searchKey          			= searchKey;
			evt.properties        			= new Dictionary();
			return evt;
		}
	}
}