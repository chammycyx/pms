package hk.org.ha.event.pms.main.report {
	
	import hk.org.ha.model.pms.udt.report.ManualPrescRptType;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveManualPrescListEvent extends AbstractTideEvent 
	{
		private var _dateFrom:Date;
		private var _manualPrescRptType:ManualPrescRptType;
		private var _password:String;
		private var _exportCurrentMonth:Boolean;
		
		public function RetrieveManualPrescListEvent(dateFrom:Date, manualPrescRptType:ManualPrescRptType, password:String, exportCurrentMonth:Boolean):void 
		{
			super();
			_dateFrom = dateFrom;
			_manualPrescRptType = manualPrescRptType;
			_password = password;
			_exportCurrentMonth = exportCurrentMonth;
		}
		
		public function get dateFrom():Date{
			return _dateFrom;	
		}
		
		public function get manualPrescRptType():ManualPrescRptType {
			return _manualPrescRptType;
		}
		
		public function get password():String {
			return _password;
		}
		
		public function get exportCurrentMonth():Boolean {
			return _exportCurrentMonth;
		}
	}
}