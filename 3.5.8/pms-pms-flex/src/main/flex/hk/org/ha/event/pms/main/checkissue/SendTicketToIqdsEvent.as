package hk.org.ha.event.pms.main.checkissue
{
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SendTicketToIqdsEvent extends AbstractTideEvent 
	{		
		private var _iqdsMessageContentList:ListCollectionView;
		private var _singleSend:Boolean;
		private var _callbackFunc:Function;
		
		public function SendTicketToIqdsEvent(iqdsMessageContentList:ListCollectionView, singleSend:Boolean, callbackFunc:Function=null):void 
		{
			super();
			_iqdsMessageContentList = iqdsMessageContentList;
			_singleSend = singleSend;
			_callbackFunc = callbackFunc;
		}
		
		public function get iqdsMessageContentList():ListCollectionView
		{
			return _iqdsMessageContentList;
		}
		
		public function get singleSend():Boolean
		{
			return _singleSend;
		}
		
		public function set callbackFunc(callbackFunc:Function):void {
			_callbackFunc = callbackFunc;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}