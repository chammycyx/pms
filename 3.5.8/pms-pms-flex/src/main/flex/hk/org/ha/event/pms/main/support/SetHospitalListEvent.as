package hk.org.ha.event.pms.main.support
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class SetHospitalListEvent extends AbstractTideEvent 
	{
		private var _list:ArrayCollection;
		
		public function SetHospitalListEvent(list:ArrayCollection):void
		{
			super();
			_list = list;
		}
		
		public function get list():ArrayCollection {
			return _list;
		}
	}
}