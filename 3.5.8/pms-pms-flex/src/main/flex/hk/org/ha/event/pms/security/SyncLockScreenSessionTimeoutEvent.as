package hk.org.ha.event.pms.security {
	
	import hk.org.ha.view.pms.security.popup.SystemIdleProtectionPopup;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SyncLockScreenSessionTimeoutEvent extends AbstractTideEvent 
	{
		private var _systemIdleProtectionPopup:SystemIdleProtectionPopup;
		
		public function SyncLockScreenSessionTimeoutEvent(systemIdleProtectionPopup:SystemIdleProtectionPopup):void 
		{
			super();
			_systemIdleProtectionPopup = systemIdleProtectionPopup;
		}
		
		public function get systemIdleProtectionPopup():SystemIdleProtectionPopup {
			return _systemIdleProtectionPopup;
		}
		
	}
}