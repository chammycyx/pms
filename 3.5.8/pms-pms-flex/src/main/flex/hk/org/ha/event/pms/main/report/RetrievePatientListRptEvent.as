package hk.org.ha.event.pms.main.report {
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePatientListRptEvent extends AbstractTideEvent 
	{
		private var _wardCodeList:ArrayCollection;
		
		public function RetrievePatientListRptEvent(wardCodeList:ArrayCollection):void 
		{
			super();
			_wardCodeList = wardCodeList;
		}
		
		public function get wardCodeList():ArrayCollection {
			return _wardCodeList;
		}
	}
}