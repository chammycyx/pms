package hk.org.ha.event.pms.main.reftable
{
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dms.persistence.DmDrug;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDosageConversionEvent extends AbstractTideEvent
	{
		private var _dmDrug:DmDrug;
		
		private var _callBackEvent:Event;
		
		public function RetrieveDosageConversionEvent(dmDrug:DmDrug, eventValue:Event=null):void
		{
			super();
			_dmDrug = dmDrug;
			_callBackEvent = eventValue;
		}
		
		public function get dmDrug():DmDrug
		{
			return _dmDrug;
		}
		
		public function get callBackEvent():Event
		{
			return _callBackEvent;
		}
	}
}