package hk.org.ha.event.pms.main.checkissue.mp
{
	import hk.org.ha.model.pms.vo.checkissue.mp.SentDelivery;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateDeliveryDateEvent extends AbstractTideEvent 
	{			
		private var _sentDelivery:SentDelivery;
		
		public function UpdateDeliveryDateEvent(sentDelivery:SentDelivery):void 
		{
			super();
			_sentDelivery = sentDelivery;
		}
		
		public function get sentDelivery():SentDelivery {
			return _sentDelivery;
		}
	}
}