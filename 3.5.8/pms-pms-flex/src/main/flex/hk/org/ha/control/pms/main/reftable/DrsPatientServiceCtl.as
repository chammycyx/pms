package hk.org.ha.control.pms.main.reftable {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.reftable.ClearDrsPatientMaintViewEvent;
	import hk.org.ha.event.pms.main.reftable.RefreshDrsPatientEvent;
	import hk.org.ha.event.pms.main.reftable.RetrieveDrsPatientEvent;
	import hk.org.ha.event.pms.main.reftable.UpdateDrsPatientEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.model.pms.biz.reftable.DrsPatientServiceBean;
	import hk.org.ha.model.pms.persistence.reftable.DrsPatient;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("drsPatientServiceCtl", restrict="true")]
	public class DrsPatientServiceCtl 
	{
		[In]
		public var drsPatientService:DrsPatientServiceBean;
				
		[Observer]
		public function retrieveDrsPatient(evt:RetrieveDrsPatientEvent):void
		{
			drsPatientService.retrieveDrsPatient(evt.hkidTxt, retrieveDrsPatientResult);
		}
		
		private function retrieveDrsPatientResult(evt:TideResultEvent):void {
			if (evt.result == null){
				evt.context.dispatchEvent(new RefreshDrsPatientEvent(null));
			}else{
				evt.context.dispatchEvent(new RefreshDrsPatientEvent(evt.result as DrsPatient));
			}
			dispatchEvent(new CloseLoadingPopupEvent());
		}
		
		public function closePopup(evt:Event):void {
			dispatchEvent(new CloseLoadingPopupEvent());			
		}
		
		[Observer]
		public function updateDrsPatient(evt:UpdateDrsPatientEvent):void{
			drsPatientService.updateDrsPatient(evt.drsPatient, updateDrsPatientResult);
		}
		
		private function updateDrsPatientResult(evt:TideResultEvent):void{
			evt.context.dispatchEvent(new ClearDrsPatientMaintViewEvent());
		}
	}
}
