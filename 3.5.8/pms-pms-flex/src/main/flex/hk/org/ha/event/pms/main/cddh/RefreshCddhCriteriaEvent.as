package hk.org.ha.event.pms.main.cddh {
	
	import hk.org.ha.model.pms.vo.cddh.CddhCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshCddhCriteriaEvent extends AbstractTideEvent 
	{
		private var _cddhCriteria:CddhCriteria;
		
		public function RefreshCddhCriteriaEvent(cddhCriteria:CddhCriteria):void 
		{
			super();
			_cddhCriteria = cddhCriteria;
		}
		
		public function get cddhCriteria():CddhCriteria
		{
			return _cddhCriteria;
		}
	}
}