package hk.org.ha.control.pms.main.alert.druginfo {

	import mx.collections.ListCollectionView;
	
	import hk.org.ha.event.pms.main.alert.druginfo.RetrieveCommonOrderEvent;
	import hk.org.ha.event.pms.main.alert.druginfo.RetrieveContraindicationEvent;
	import hk.org.ha.event.pms.main.alert.druginfo.RetrieveDosageRangeEvent;
	import hk.org.ha.event.pms.main.alert.druginfo.RetrieveDrugClassificationListEvent;
	import hk.org.ha.event.pms.main.alert.druginfo.RetrieveDrugInteractionEvent;
	import hk.org.ha.event.pms.main.alert.druginfo.RetrieveDrugMdsByDhRxDrugEvent;
	import hk.org.ha.event.pms.main.alert.druginfo.RetrieveDrugMdsByDisplayNameFormSaltFmStatusEvent;
	import hk.org.ha.event.pms.main.alert.druginfo.RetrieveDrugMdsByItemCodeEvent;
	import hk.org.ha.event.pms.main.alert.druginfo.RetrieveFoodInteractionEvent;
	import hk.org.ha.event.pms.main.alert.druginfo.RetrieveMonographEvent;
	import hk.org.ha.event.pms.main.alert.druginfo.RetrievePatientEducationEvent;
	import hk.org.ha.event.pms.main.alert.druginfo.RetrievePrecautionEvent;
	import hk.org.ha.event.pms.main.alert.druginfo.RetrievePreparationForDhOrderEvent;
	import hk.org.ha.event.pms.main.alert.druginfo.RetrievePreparationForDrugSearchEvent;
	import hk.org.ha.event.pms.main.alert.druginfo.RetrievePreparationForOrderEntryEvent;
	import hk.org.ha.event.pms.main.alert.druginfo.RetrieveSideEffectEvent;
	import hk.org.ha.event.pms.main.alert.druginfo.popup.ShowDrugInfoPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.ShowLoadingPopupEvent;
	import hk.org.ha.model.pms.biz.alert.druginfo.DrugInfoServiceBean;
	import hk.org.ha.model.pms.dms.vo.DrugMds;
	import hk.org.ha.model.pms.dms.vo.DrugName;
	import hk.org.ha.model.pms.dms.vo.PreparationProperty;
	import hk.org.ha.model.pms.dms.vo.RenalAdj;
	import hk.org.ha.model.pms.dms.vo.RouteForm;
	import hk.org.ha.model.pms.vo.alert.mds.RetrievePreparationPropertyListResult;
	import hk.org.ha.model.pms.vo.rx.DhRxDrug;
	
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	import org.granite.tide.seam.In;

	[Bindable]
	[Name("drugInfoServiceCtl", restrict="true")]
	public class DrugInfoServiceCtl {
		
		[In]
		public var ctx:Context;
		
		[In]
		public var drugInfoService:DrugInfoServiceBean;
		
		private var retrieveDrugMdsByItemCodeEvent:RetrieveDrugMdsByItemCodeEvent;
		private var retrieveDrugMdsByDisplayNameFormSaltFmStatusEvent:RetrieveDrugMdsByDisplayNameFormSaltFmStatusEvent;
		
		private var retrievePreparationForOrderEntryEvent:RetrievePreparationForOrderEntryEvent;
		private var retrievePreparationForDrugSearchEvent:RetrievePreparationForDrugSearchEvent;
		private var retrievePreparationForDhOrderEvent:RetrievePreparationForDhOrderEvent;
		
		private var retrieveDrugClassificationListEvent:RetrieveDrugClassificationListEvent;
		private var retrieveSideEffectEvent:RetrieveSideEffectEvent;
		private var retrieveContraindicationEvent:RetrieveContraindicationEvent;
		private var retrievePrecautionEvent:RetrievePrecautionEvent;
		private var retrieveDrugInteractionEvent:RetrieveDrugInteractionEvent;
		private var retrieveFoodInteractionEvent:RetrieveFoodInteractionEvent;
		private var retrievePatientEducationEvent:RetrievePatientEducationEvent;
		private var retrieveCommonOrderEvent:RetrieveCommonOrderEvent;
		private var retrieveDosageRangeEvent:RetrieveDosageRangeEvent;
		private var retrieveMonographEvent:RetrieveMonographEvent;
		private var retrieveDrugMdsByDhRxDrugEvent:RetrieveDrugMdsByDhRxDrugEvent;
		
		[Observer]
		public function retrieveDrugClassificationList(evt:RetrieveDrugClassificationListEvent):void {
			dispatchEvent(new ShowLoadingPopupEvent("Loading Drug Info ..."));
			retrieveDrugClassificationListEvent = evt;
			drugInfoService.retrieveDrugClassificationList(evt.gcnSeqNum, evt.rdfgenId, evt.rgenId, evt.patHospCode, evt.preparationPropertyList, retrieveDrugClassificationListResult, retrieveDrugClassificationListFault);
		}
		
		private function retrieveDrugClassificationListResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			retrieveDrugClassificationListEvent.successCallback(evt.result as ListCollectionView);
		}
		
		private function retrieveDrugClassificationListFault(evt:TideFaultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			retrieveDrugClassificationListEvent.faultCallback();
		}
		
		[Observer]
		public function retrieveSideEffect(evt:RetrieveSideEffectEvent):void {
			dispatchEvent(new ShowLoadingPopupEvent("Loading Drug Info ..."));
			retrieveSideEffectEvent = evt;
			drugInfoService.retrieveSideEffect(evt.gcnSeqNum, evt.rdfgenId, evt.rgenId, evt.patHospCode, evt.preparationPropertyList, retrieveSideEffectResult, retrieveSideEffectFault);
		}
		
		private function retrieveSideEffectResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			retrieveSideEffectEvent.successCallback(evt.result as String);
		}
		
		private function retrieveSideEffectFault(evt:TideFaultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			retrieveSideEffectEvent.faultCallback();
		}
		
		[Observer]
		public function retrieveContraindication(evt:RetrieveContraindicationEvent):void {
			dispatchEvent(new ShowLoadingPopupEvent("Loading Drug Info ..."));
			retrieveContraindicationEvent = evt;
			drugInfoService.retrieveContraindication(evt.gcnSeqNum, evt.rdfgenId, evt.rgenId, evt.patHospCode, evt.preparationPropertyList, retrieveContraindicationResult, retrieveContraindicationFault);
		}
		
		private function retrieveContraindicationResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			retrieveContraindicationEvent.successCallback(evt.result as String);
		}
		
		private function retrieveContraindicationFault(evt:TideFaultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			retrieveContraindicationEvent.faultCallback();
		}
		
		[Observer]
		public function retrievePrecaution(evt:RetrievePrecautionEvent):void {
			dispatchEvent(new ShowLoadingPopupEvent("Loading Drug Info ..."));
			retrievePrecautionEvent = evt;
			drugInfoService.retrievePrecaution(evt.gcnSeqNum, evt.rdfgenId, evt.rgenId, evt.patHospCode, evt.preparationPropertyList, retrievePrecautionResult, retrievePrecautionFault);
		}
		
		private function retrievePrecautionResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			retrievePrecautionEvent.successCallback(evt.result as String);
		}
		
		private function retrievePrecautionFault(evt:TideFaultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			retrievePrecautionEvent.faultCallback();
		}
		
		[Observer]
		public function retrieveDrugInteraction(evt:RetrieveDrugInteractionEvent):void {
			dispatchEvent(new ShowLoadingPopupEvent("Loading Drug Info ..."));
			retrieveDrugInteractionEvent = evt;
			drugInfoService.retrieveDrugInteraction(evt.gcnSeqNum, evt.rdfgenId, evt.rgenId, evt.patHospCode, evt.preparationPropertyList, retrieveDrugInteractionResult, retrieveDrugInteractionFault);
		}
		
		private function retrieveDrugInteractionResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			retrieveDrugInteractionEvent.successCallback(evt.result as ListCollectionView);
		}
		
		private function retrieveDrugInteractionFault(evt:TideFaultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			retrieveDrugInteractionEvent.faultCallback();
		}
		
		[Observer]
		public function retrieveFoodInteraction(evt:RetrieveFoodInteractionEvent):void {
			dispatchEvent(new ShowLoadingPopupEvent("Loading Drug Info ..."));
			retrieveFoodInteractionEvent = evt;
			drugInfoService.retrieveFoodInteraction(evt.gcnSeqNum, evt.rdfgenId, evt.rgenId, evt.patHospCode, evt.preparationPropertyList, retrieveFoodInteractionResult, retrieveFoodInteractionFault);
		}
		
		private function retrieveFoodInteractionResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			retrieveFoodInteractionEvent.successCallback(evt.result as ListCollectionView);
		}
		
		private function retrieveFoodInteractionFault(evt:TideFaultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			retrieveFoodInteractionEvent.faultCallback();
		}
		
		[Observer]
		public function retrievePatientEducation(evt:RetrievePatientEducationEvent):void {
			dispatchEvent(new ShowLoadingPopupEvent("Loading Drug Info ..."));
			retrievePatientEducationEvent = evt;
			drugInfoService.retrievePatientEducation(evt.gcnSeqNum, evt.rdfgenId, evt.rgenId, evt.patHospCode, evt.preparationPropertyList, retrievePatientEducationResult, retrievePatientEducationFault);
		}
		
		private function retrievePatientEducationResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			retrievePatientEducationEvent.successCallback(evt.result as ListCollectionView);
		}
		
		private function retrievePatientEducationFault(evt:TideFaultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			retrievePatientEducationEvent.faultCallback();
		}
		
		[Observer]
		public function retrieveCommonOrder(evt:RetrieveCommonOrderEvent):void {
			dispatchEvent(new ShowLoadingPopupEvent("Loading Drug Info ..."));
			retrieveCommonOrderEvent = evt;
			drugInfoService.retrieveCommonOrder(evt.gcnSeqNum, evt.rdfgenId, evt.rgenId, evt.patHospCode, evt.preparationPropertyList, retrieveCommonOrderResult, retrieveCommonOrderFault);
		}
		
		private function retrieveCommonOrderResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			retrieveCommonOrderEvent.successCallback(evt.result as String);
		}
		
		private function retrieveCommonOrderFault(evt:TideFaultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			retrieveCommonOrderEvent.faultCallback();
		}
		
		[Observer]
		public function retrieveDosageRange(evt:RetrieveDosageRangeEvent):void {
			dispatchEvent(new ShowLoadingPopupEvent("Loading Drug Info ..."));
			retrieveDosageRangeEvent = evt;
			drugInfoService.retrieveDosageRange(evt.gcnSeqNum, evt.rdfgenId, evt.rgenId, evt.patHospCode, evt.age, evt.ageUnit, evt.preparationPropertyList, evt.selectedStrengthIndex, retrieveDosageRangeResult, retrieveDosageRangeFault);
		}
		
		private function retrieveDosageRangeResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			retrieveDosageRangeEvent.successCallback(evt.result as String);
		}
		
		private function retrieveDosageRangeFault(evt:TideFaultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			retrieveDosageRangeEvent.faultCallback();
		}
		
		[Observer]
		public function retrieveMonograph(evt:RetrieveMonographEvent):void {
			dispatchEvent(new ShowLoadingPopupEvent("Loading Drug Info ..."));
			retrieveMonographEvent = evt;
			drugInfoService.retrieveMonograph(evt.monoId, evt.monoType, evt.monoVersion, evt.patHospCode, retrieveMonographResult, retrieveMonographFault);
		}
		
		private function retrieveMonographResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			retrieveMonographEvent.successCallback(evt.result as String);
		}

		private function retrieveMonographFault(evt:TideFaultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			retrieveMonographEvent.faultCallback();
		}
		
		[Observer]
		public function retrieveDrugMdsByItemCode(evt:RetrieveDrugMdsByItemCodeEvent):void {
			retrieveDrugMdsByItemCodeEvent = evt;
			if (evt.checkRenal) {
				In(Object(drugInfoService).renalAdjCode);
				In(Object(drugInfoService).renalReviewDate);
			}
			drugInfoService.retrieveDrugMdsByItemCode(evt.itemCode, evt.checkHq, evt.checkRenal, retrieveDrugMdsByItemCodeResult);
		}
		
		private function retrieveDrugMdsByItemCodeResult(evt:TideResultEvent):void {
			var drugMds:DrugMds;
			if (evt.result != null) {
				drugMds = evt.result as DrugMds;
			}
			if (retrieveDrugMdsByItemCodeEvent.checkRenal) {
				retrieveDrugMdsByItemCodeEvent.callback(drugMds, Object(drugInfoService).renalAdjCode, Object(drugInfoService).renalReviewDate);
			}
			else {
				retrieveDrugMdsByItemCodeEvent.callback(drugMds);
			}
		}
		
		[Observer]
		public function retrieveDrugMdsByDisplayNameFormSaltFmStatus(evt:RetrieveDrugMdsByDisplayNameFormSaltFmStatusEvent):void {
			retrieveDrugMdsByDisplayNameFormSaltFmStatusEvent = evt;
			if (evt.checkRenal) {
				In(Object(drugInfoService).renalAdjCode);
				In(Object(drugInfoService).renalReviewDate);
			}
			drugInfoService.retrieveDrugMdsByDisplayNameFormSaltFmStatus(evt.displayName, evt.formCode, evt.saltProperty, evt.fmStatus, evt.checkHq, evt.checkRenal, retrieveDrugMdsByDisplayNameFormSaltFmStatusResult);
		}
		
		private function retrieveDrugMdsByDisplayNameFormSaltFmStatusResult(evt:TideResultEvent):void {
			var drugMds:DrugMds;
			if (evt.result != null) {
				drugMds = evt.result as DrugMds;
			}
			if (retrieveDrugMdsByDisplayNameFormSaltFmStatusEvent.checkRenal) {
				retrieveDrugMdsByDisplayNameFormSaltFmStatusEvent.callback(drugMds, Object(drugInfoService).renalAdjCode, Object(drugInfoService).renalReviewDate);
			}
			else {
				retrieveDrugMdsByDisplayNameFormSaltFmStatusEvent.callback(drugMds);
			}
		}
		
		[Observer]
		public function retrieveDrugMdsByDhRxDrug(evt:RetrieveDrugMdsByDhRxDrugEvent):void {
			if (evt.checkRenal) {
				In(Object(drugInfoService).renalAdjCode);
				In(Object(drugInfoService).renalReviewDate);
			}
			retrieveDrugMdsByDhRxDrugEvent = evt;
			drugInfoService.retrieveDrugMdsByDhRxDrug(evt.dhRxDrug, evt.checkHq, evt.checkRenal, retrieveDrugMdsByDhRxDrugResult);
		}
		
		private function retrieveDrugMdsByDhRxDrugResult(evt:TideResultEvent):void {
			var drugMds:DrugMds;
			if (evt.result != null) {
				drugMds = evt.result as DrugMds;
			}
			if (retrieveDrugMdsByDhRxDrugEvent.checkRenal) {
				retrieveDrugMdsByDhRxDrugEvent.callback(drugMds, Object(drugInfoService).renalAdjCode, Object(drugInfoService).renalReviewDate);
			}
			else {
				retrieveDrugMdsByDhRxDrugEvent.callback(drugMds);
			}
		}
		
		[Observer]
		public function retrievePreparationForOrderEntry(evt:RetrievePreparationForOrderEntryEvent):void {
			retrievePreparationForOrderEntryEvent = evt;
			drugInfoService.retrievePreparationPropertyListForOrderEntry(evt.displayName, evt.formCode, evt.saltProperty, evt.fmStatus, evt.aliasName, evt.firstDisplayName, evt.checkHq, retrievePreparationForOrderEntryResult);
		}

		private function retrievePreparationForOrderEntryResult(evt:TideResultEvent):void {

			var preparationList:ListCollectionView = evt.result as ListCollectionView;
			var itemCode:String = retrievePreparationForOrderEntryEvent.itemCode;
			var selectedIndex:int = 0;

			if (itemCode != null) {
				for each (var preparation:PreparationProperty in preparationList) {
					if (preparation.itemCode == itemCode) {
						selectedIndex = preparationList.getItemIndex(preparation);
						break;
					}
				}
			}

			dispatchEvent(new ShowDrugInfoPopupEvent(
				retrievePreparationForOrderEntryEvent.gcnSeqNum,
				retrievePreparationForOrderEntryEvent.rdfgenId,
				retrievePreparationForOrderEntryEvent.rgenId,
				retrievePreparationForOrderEntryEvent.displayNameSaltForm,
				retrievePreparationForOrderEntryEvent.strengthVolume,
				retrievePreparationForOrderEntryEvent.patHospCode,
				preparationList,
				selectedIndex,
				retrievePreparationForOrderEntryEvent.dob
			));
		}
		
		[Observer]
		public function retrievePreparationForDrugSearch(evt:RetrievePreparationForDrugSearchEvent):void {
			retrievePreparationForDrugSearchEvent = evt;
			drugInfoService.retrievePreparationPropertyListForDrugSearch(evt.drugName, evt.routeForm, evt.preparationProperty, retrievePreparationForDrugSearchResult);
		}
		
		private function retrievePreparationForDrugSearchResult(evt:TideResultEvent):void {

			var preparationList:ListCollectionView = evt.result as ListCollectionView;
			var selectedIndex:int = 0;
			if (retrievePreparationForDrugSearchEvent.preparationProperty != null) {
				var itemCode:String = retrievePreparationForDrugSearchEvent.preparationProperty.itemCode;
				for each (var preparation:PreparationProperty in preparationList) {
					if (preparation.itemCode == itemCode) {
						selectedIndex = preparationList.getItemIndex(preparation);
						break;
					}
				}
			}
			
			dispatchEvent(new ShowDrugInfoPopupEvent(
				retrievePreparationForDrugSearchEvent.gcnSeqNum,
				retrievePreparationForDrugSearchEvent.rdfgenId,
				retrievePreparationForDrugSearchEvent.rgenId,
				retrievePreparationForDrugSearchEvent.displayNameSaltForm,
				retrievePreparationForDrugSearchEvent.strengthVolume,
				retrievePreparationForDrugSearchEvent.patHospCode,
				preparationList,
				selectedIndex,
				retrievePreparationForDrugSearchEvent.dob
			));
		}
		
		[Observer]
		public function retrievePreparationForDhOrder(evt:RetrievePreparationForDhOrderEvent):void {
			retrievePreparationForDhOrderEvent = evt;
			drugInfoService.retrievePreparationForDhOrder(evt.dhRxDrug, retrievePreparationForDhOrderResult);
		}
		
		private function retrievePreparationForDhOrderResult(evt:TideResultEvent):void {
			
			var result:RetrievePreparationPropertyListResult = evt.result as RetrievePreparationPropertyListResult;
			var preparationList:ListCollectionView = result.preparationPropertyList;
			var dhRxDrug:DhRxDrug = result.rxItem as DhRxDrug;
			var selectedIndex:int = 0;
			
			if (dhRxDrug.itemCode != null) {
				for each (var preparation:PreparationProperty in preparationList) {
					if (preparation.itemCode == dhRxDrug.itemCode) {
						selectedIndex = preparationList.getItemIndex(preparation);
						break;
					}
				}
			}
			
			dispatchEvent(new ShowDrugInfoPopupEvent(
				retrievePreparationForDhOrderEvent.gcnSeqNum,
				retrievePreparationForDhOrderEvent.rdfgenId,
				retrievePreparationForDhOrderEvent.rgenId,
				dhRxDrug.displayNameFormSalt,
				dhRxDrug.itemCode == null ? null : dhRxDrug.strengthVolume,
				retrievePreparationForDhOrderEvent.patHospCode,
				preparationList,
				selectedIndex,
				retrievePreparationForDhOrderEvent.dob
			));
		}
	}
}