package hk.org.ha.control.pms.main.reftable {
	
	import hk.org.ha.event.pms.main.reftable.RefreshPasSpecialtyListEvent;
	import hk.org.ha.event.pms.main.reftable.RefreshPmsRestrictItemSpecListBySpecialtyEvent;
	import hk.org.ha.event.pms.main.reftable.RetrievePasSpecDisplayListEvent;
	import hk.org.ha.event.pms.main.reftable.RetrievePmsRestrictItemSpecListBySpecialtyEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.model.pms.biz.reftable.PmsRestrictItemSpecBySpecListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("pmsRestrictItemSpecBySpecListServiceCtl", restrict="true")]
	public class PmsRestrictItemSpecBySpecListServiceCtl 
	{	
		[In]
		public var pmsRestrictItemSpecBySpecListService:PmsRestrictItemSpecBySpecListServiceBean;
		
		[Observer]
		public function retrievePmsRestrictItemSpecListBySpecialty(evt:RetrievePmsRestrictItemSpecListBySpecialtyEvent):void
		{	
			pmsRestrictItemSpecBySpecListService.retrievePmsRestrictItemListBySpecialty(evt.specType, evt.specCode, evt.subSpecCode, refreshPmsRestrictItemSpecListBySpecialtyResult);
		}
		
		private function refreshPmsRestrictItemSpecListBySpecialtyResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshPmsRestrictItemSpecListBySpecialtyEvent());
		}
		
		[Observer]
		public function retrievePasSpecDisplayList(evt:RetrievePasSpecDisplayListEvent):void
		{
			pmsRestrictItemSpecBySpecListService.retrievePasSpecDisplayList(refreshPasSpecDisplayListResult);
		}
		
		private function refreshPasSpecDisplayListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			evt.context.dispatchEvent(new RefreshPasSpecialtyListEvent(evt.result as ArrayCollection));
		}
	}
}
