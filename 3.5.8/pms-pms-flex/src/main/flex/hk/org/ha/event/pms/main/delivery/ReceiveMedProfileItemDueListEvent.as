package hk.org.ha.event.pms.main.delivery
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ReceiveMedProfileItemDueListEvent extends AbstractTideEvent
	{
		public function ReceiveMedProfileItemDueListEvent()
		{
			super();
		}
	}
}