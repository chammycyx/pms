package hk.org.ha.event.pms.main.reftable {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveItemDispConfigEvent extends AbstractTideEvent 
	{	
		private var _itemCode:String;
		
		private var _callBack:Function;
		
		public function RetrieveItemDispConfigEvent(itemCodeValue:String, functionValue:Function):void 
		{
			super();
			_itemCode = itemCodeValue;
			_callBack = functionValue;
		}
		
		public function get itemCode():String
		{
			return _itemCode;
		}
		
		public function get callBack():Function
		{
			return _callBack;
		}
	}
}