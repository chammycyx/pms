package hk.org.ha.event.pms.main.vetting {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ClearCapdListEvent extends AbstractTideEvent {
		
		public function ClearCapdListEvent() {
			super();
		}
	}
}
