package hk.org.ha.control.pms.main.drug {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.drug.RetrieveDefaultFormVerbByFormCodeEvent;
	import hk.org.ha.model.pms.biz.drug.SiteFormVerbServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("siteFormVerbServiceCtl", restrict="true")]
	public class SiteFormVerbServiceCtl 
	{				
		[In]
		public var ctx:Context;
		
		[In]
		public var siteFormVerbService:SiteFormVerbServiceBean;
		
		private var callBackFunc:Function;		
		
		[Observer]
		public function retrieveDefaultFormVerbByFormCode(evt:RetrieveDefaultFormVerbByFormCodeEvent):void
		{
			callBackFunc = evt.callBackFunc;
			siteFormVerbService.retrieveDefaultFormVerbByFormCode(evt.formCode, evt.itemCode, retrieveDefaultFormVerbByFormCodeResult);			
		}
		
		private function retrieveDefaultFormVerbByFormCodeResult(evt:TideResultEvent):void 
		{
			if ( callBackFunc!=null ) {
				callBackFunc.call(null,evt.result);
			}
		}
	}
}
