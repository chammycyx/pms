package hk.org.ha.event.pms.main.onestop {
		
	import hk.org.ha.model.pms.udt.onestop.MoeSortOption;
	import hk.org.ha.model.pms.vo.onestop.PrescriptionSortCriteria;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SortPagedMoeEvent extends AbstractTideEvent 
	{		
		private var _prescriptionSortCriteria:PrescriptionSortCriteria;
		private var _callbackFunction:Function;
		
		public function SortPagedMoeEvent(prescriptionSortCriteria:PrescriptionSortCriteria, callbackFunction:Function):void 
		{
			super();
			_prescriptionSortCriteria = prescriptionSortCriteria;
			_callbackFunction = callbackFunction;
		}
				
		public function get prescriptionSortCriteria():PrescriptionSortCriteria
		{
			return _prescriptionSortCriteria;
		}
		
		public function get callbackFunction():Function
		{
			return _callbackFunction;
		}
	}
}