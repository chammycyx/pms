package hk.org.ha.event.pms.main.vetting.mp.show
{
	import flash.events.Event;
	
	public class ShowInactiveListEvent extends Event
	{
		public function ShowInactiveListEvent()
		{
			super("showInactiveList");
		}
	}
}