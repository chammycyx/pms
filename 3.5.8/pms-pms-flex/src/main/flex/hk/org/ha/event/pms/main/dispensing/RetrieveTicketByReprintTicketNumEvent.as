package hk.org.ha.event.pms.main.dispensing
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveTicketByReprintTicketNumEvent extends AbstractTideEvent 
	{		
		private var _reprintTicketNum:String;

		public function RetrieveTicketByReprintTicketNumEvent(reprintTicketNum:String):void 
		{
			super();
			_reprintTicketNum = reprintTicketNum;
		}
		
		public function get reprintTicketNum():String
		{
			return _reprintTicketNum;
		}
	}
}