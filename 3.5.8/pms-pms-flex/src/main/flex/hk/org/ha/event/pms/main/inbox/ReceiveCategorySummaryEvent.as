package hk.org.ha.event.pms.main.inbox
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ReceiveCategorySummaryEvent extends AbstractTideEvent
	{
		public function ReceiveCategorySummaryEvent()
		{
			super();
		}
	}
}