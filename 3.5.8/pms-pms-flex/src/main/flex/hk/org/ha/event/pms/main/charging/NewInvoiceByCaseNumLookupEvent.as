package hk.org.ha.event.pms.main.charging {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class NewInvoiceByCaseNumLookupEvent extends AbstractTideEvent 
	{	
		private var _caseNum:String;
		
		public function NewInvoiceByCaseNumLookupEvent(caseNum:String):void 
		{
			super();
			_caseNum = caseNum;
		}
		
		public function get caseNum():String{
			return _caseNum;
		}
	}
}