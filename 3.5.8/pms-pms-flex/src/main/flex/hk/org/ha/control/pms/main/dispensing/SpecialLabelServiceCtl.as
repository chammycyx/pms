package hk.org.ha.control.pms.main.dispensing{
	import hk.org.ha.event.pms.main.dispensing.RefreshSpecialLabelEvent;
	import hk.org.ha.event.pms.main.dispensing.RemoveSpecialLabelEvent;
	import hk.org.ha.event.pms.main.dispensing.RetrieveSpecialLabelEvent;
	import hk.org.ha.event.pms.main.dispensing.UpdateSpecialLabelEvent;
	import hk.org.ha.event.pms.main.reftable.RefreshRemoveSpecialLabelEvent;
	import hk.org.ha.event.pms.main.reftable.RefreshUpdateSpecialLabelListEvent;
	import hk.org.ha.model.pms.biz.label.SpecialLabelServiceBean;
	
	import org.granite.tide.events.TideResultEvent;

	
	
	
	[Bindable]
	[Name("specialLabelServiceCtl", restrict="true")]
	public class SpecialLabelServiceCtl 
	{
		[In]
		public var specialLabelService:SpecialLabelServiceBean;
		
		[Observer]
		public function retrieveSpecialLabel(evt:RetrieveSpecialLabelEvent):void
		{
			specialLabelService.retrieveSpecialLabel(evt.labelNum, retrieveSpecialLabelResult);
		}
		
		private function retrieveSpecialLabelResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshSpecialLabelEvent);
		}

		[Observer]
		public function updateSpecialLabelList(evt:UpdateSpecialLabelEvent):void
		{
			specialLabelService.updateSpecialLabel(evt.specialLabel, updateSpecialLabelListResult);
		}

		private function updateSpecialLabelListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshUpdateSpecialLabelListEvent());
		}
		
		[Observer]
		public function removeSpecialLabel(evt:RemoveSpecialLabelEvent):void
		{
			specialLabelService.removeSpecialLabel(evt.labelNum, removeSpecialLabelResult);
		}
		
		private function removeSpecialLabelResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshRemoveSpecialLabelEvent());
		}
	}
}
