package hk.org.ha.event.pms.main.onestop.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowOneStopEntrySuspendListPopupEvent extends AbstractTideEvent 
	{		
		public function ShowOneStopEntrySuspendListPopupEvent():void 
		{
			super();
		}        
	}
}