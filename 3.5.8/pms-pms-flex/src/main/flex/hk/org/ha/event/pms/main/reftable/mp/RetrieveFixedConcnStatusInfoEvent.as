package hk.org.ha.event.pms.main.reftable.mp {
	
	import hk.org.ha.model.pms.dms.persistence.DmDrug;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveFixedConcnStatusInfoEvent extends AbstractTideEvent 
	{
		private var _dmDrug:DmDrug;

		public function get dmDrug():DmDrug
		{
			return _dmDrug;
		}

		public function RetrieveFixedConcnStatusInfoEvent(dmDrug:DmDrug):void
		{
			_dmDrug = dmDrug;
			super();
		}
	}
}