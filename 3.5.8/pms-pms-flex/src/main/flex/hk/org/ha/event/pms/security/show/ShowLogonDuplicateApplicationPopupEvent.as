package hk.org.ha.event.pms.security.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowLogonDuplicateApplicationPopupEvent extends AbstractTideEvent 
	{
		
		public function ShowLogonDuplicateApplicationPopupEvent():void 
		{
			super();
		}
	}
}