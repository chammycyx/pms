package hk.org.ha.event.pms.main.pivas.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowBatchPrepFormulaSelectionPopupEvent extends AbstractTideEvent 
	{
		private var _displayName:String;
		
		public function ShowBatchPrepFormulaSelectionPopupEvent(displayName:String):void 
		{
			super();
			_displayName = displayName;
		}
		
		public function get displayName():String {
			return _displayName;
		}
	}
}