package hk.org.ha.control.pms.main.enquiry {
	
	import hk.org.ha.event.pms.main.enquiry.RefreshMpSfiInvoiceDetailEvent;
	import hk.org.ha.event.pms.main.enquiry.RefreshMpSfiInvoiceSummaryEvent;
	import hk.org.ha.event.pms.main.enquiry.RefreshMpSfiInvoiceSummaryListEvent;
	import hk.org.ha.event.pms.main.enquiry.RefreshReprintMpSfiInvoiceEvent;
	import hk.org.ha.event.pms.main.enquiry.RefreshReprintSfiInvoiceEvent;
	import hk.org.ha.event.pms.main.enquiry.RefreshSfiInvoiceDetailEvent;
	import hk.org.ha.event.pms.main.enquiry.RefreshSfiInvoiceSummaryEvent;
	import hk.org.ha.event.pms.main.enquiry.RetrieveMpSfiInvoiceDetailEvent;
	import hk.org.ha.event.pms.main.enquiry.RetrieveMpSfiInvoiceSummaryEvent;
	import hk.org.ha.event.pms.main.enquiry.RetrieveMpSfiInvoiceSummaryListEvent;
	import hk.org.ha.model.pms.biz.enquiry.MpSfiInvoiceEnqServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	import org.granite.tide.seam.In;
	

	[Bindable]
	[Name("mpSfiInvoiceEnqServiceCtl", restrict="true")]
	public class MpSfiInvoiceEnqServiceCtl 
	{
	
		[In]
		public var mpSfiInvoiceEnqService:MpSfiInvoiceEnqServiceBean;
				
		[In]
		public var ctx:Context;
					
		[Observer]
		public function retrieveMpSfiInvoiceSummaryList(evt:RetrieveMpSfiInvoiceSummaryListEvent):void{			
			In(Object(mpSfiInvoiceEnqService).retrieveSuccess)
			In(Object(mpSfiInvoiceEnqService).errMsg)
			
			switch (evt.searchType)
			{
				case 'invoiceNum':
					mpSfiInvoiceEnqService.retrieveMpSfiInvoiceByInvoiceNumber(evt.param1, evt.includeVoid, evt.patType, retrieveMpSfiInvoiceSummaryListResult);
					break;
				case 'hkid':
					mpSfiInvoiceEnqService.retrieveMpSfiInvoiceByHkid(evt.param1, evt.includeVoid, evt.patType, retrieveMpSfiInvoiceSummaryListResult);
					break;
				case 'sfiInvDate':
					mpSfiInvoiceEnqService.retrieveMpSfiInvoiceByInvoiceDate(evt.param2, evt.includeVoid, evt.patType, retrieveMpSfiInvoiceSummaryListResult);
					break;
				case 'caseNum':
					mpSfiInvoiceEnqService.retrieveMpSfiInvoiceByCaseNum(evt.param1, evt.includeVoid, evt.patType, retrieveMpSfiInvoiceSummaryListResult);
					break;
				case 'dayRange':
					mpSfiInvoiceEnqService.retrieveMpSfiInvoiceByExtactionPeriod(evt.param3, evt.includeVoid, evt.patType, retrieveMpSfiInvoiceSummaryListResult);
					break;
			}			
		}
		
		public function retrieveMpSfiInvoiceSummaryListResult(evt:TideResultEvent):void{	
			evt.context.dispatchEvent(new RefreshMpSfiInvoiceSummaryListEvent(Object(mpSfiInvoiceEnqService).retrieveSuccess,Object(mpSfiInvoiceEnqService).errMsg));
		}
		
		[Observer]
		public function retrieveMpSfiInvoiceSummary(evt:RetrieveMpSfiInvoiceSummaryEvent):void{		
			mpSfiInvoiceEnqService.retrieveMpSfiInvoicePaymentStatus(evt.sfiInvoiceId, evt.purchaseReqId, retrieveMpSfiInvoiceSummaryResult);
		}
		
		public function retrieveMpSfiInvoiceSummaryResult(evt:TideResultEvent):void{	
			evt.context.dispatchEvent(new RefreshMpSfiInvoiceSummaryEvent() );
		}
		
		[Observer]
		public function retrieveMpSfiInvoiceDetailEvent(evt:RetrieveMpSfiInvoiceDetailEvent):void{		
			In(Object(mpSfiInvoiceEnqService).errMsg)
			if( evt.isPreview ){			
				mpSfiInvoiceEnqService.retrieveMpSfiInvoiceDetail(evt.sfiInvoiceId, evt.purchaseReqId, evt.isPreview, retrieveMpSfiInvoiceDetailResult );
			}else{
				mpSfiInvoiceEnqService.retrieveMpSfiInvoiceDetail(evt.sfiInvoiceId, evt.purchaseReqId, evt.isPreview, reprintMpSfiInvoiceResult);
			}
		}
		
		public function retrieveMpSfiInvoiceDetailResult(evt:TideResultEvent):void{	
			evt.context.dispatchEvent(new RefreshMpSfiInvoiceDetailEvent() );
		}
		
		public function reprintMpSfiInvoiceResult(evt:TideResultEvent):void{	
			evt.context.dispatchEvent(new RefreshReprintMpSfiInvoiceEvent(Object(mpSfiInvoiceEnqService).errMsg) );
		}
		
	}
}

