package hk.org.ha.event.pms.main.checkissue
{
	import hk.org.ha.model.pms.persistence.corp.Workstore;
	import hk.org.ha.model.pms.vo.checkissue.BatchIssueDetail;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateBatchIssueLogWorkstationEvent extends AbstractTideEvent 
	{		
		private var _issueDate:Date;
		private var _workstore:Workstore;
		private var _action:String;
		private var _updateBatchIssueDetail:BatchIssueDetail;
		
		public function UpdateBatchIssueLogWorkstationEvent(issueDate:Date, workstore:Workstore, updateBatchIssueDetail:BatchIssueDetail, action:String):void 
		{
			super();
			_issueDate = issueDate;
			_workstore = workstore;
			_action = action;
			_updateBatchIssueDetail = updateBatchIssueDetail;
		}
		
		public function get issueDate():Date {
			return _issueDate;
		}
		
		public function get workstore():Workstore {
			return _workstore;
		}
		
		public function get action():String {
			return _action;
		}
		
		public function get updateBatchIssueDetail():BatchIssueDetail {
			return _updateBatchIssueDetail;
		}
	}
}