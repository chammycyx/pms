package hk.org.ha.event.pms.main.reftable {
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveRouteFormDisplaySeqListEvent extends AbstractTideEvent 
	{
		private var _specCode:String;
		private var _specType:String;
		
		public function RetrieveRouteFormDisplaySeqListEvent(specCode:String , specType:String):void 
		{
			_specCode = specCode;
			_specType = specType;
			super();
		}
		
		public function get specCode():String 
		{
			return _specCode;
		}
		
		public function get specType():String 
		{
			return _specType;
		}

	}
}