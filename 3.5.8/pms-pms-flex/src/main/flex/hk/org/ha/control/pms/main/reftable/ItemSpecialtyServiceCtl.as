package hk.org.ha.control.pms.main.reftable {		
	
	import hk.org.ha.event.pms.main.reftable.mp.CheckItemSpecialtyValidityEvent;
	import hk.org.ha.model.pms.biz.reftable.ItemSpecialtyServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("itemSpecialtyServiceCtl", restrict="true")]
	public class ItemSpecialtyServiceCtl 
	{
		[In]
		public var itemSpecialtyService:ItemSpecialtyServiceBean;
		
		private var callBack:Function;
		
		[Observer]
		public function checkItemSpecialtyValidity(evt:CheckItemSpecialtyValidityEvent):void 
		{
			callBack = evt.callBack;
			itemSpecialtyService.checkItemSpecialtyValidity(evt.itemCode,evt.chargeSpecCode,checkItemSpecialtyValidityResult);
		}
		
		private function checkItemSpecialtyValidityResult(evt:TideResultEvent):void 
		{
			if ( callBack != null ) {
				callBack(evt.result);
			}
		}
	}
}
