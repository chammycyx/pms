package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDefaultFormVerbByFormCodeEvent extends AbstractTideEvent 
	{
		private var _formCode:String;
		private var _callBackFunc:Function;
		private var _itemCode:String;
		
		public function RetrieveDefaultFormVerbByFormCodeEvent(formCode:String, funcValue:Function=null, itemCode:String=null):void 
		{
			super();
			_formCode = formCode;
			_callBackFunc = funcValue;
			_itemCode = itemCode;
		}
		
		public function get formCode():String
		{
			return _formCode;
		}
		
		public function get callBackFunc():Function
		{
			return _callBackFunc;
		}
		
		public function get itemCode():String {
			return _itemCode;
		}
	}
}