package hk.org.ha.event.pms.main.reftable.mp
{
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dms.persistence.DmDrug;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveMultiDoseConvSetListEvent extends AbstractTideEvent
	{
		private var _dmDrug:DmDrug;
		
		public function RetrieveMultiDoseConvSetListEvent(dmDrug:DmDrug):void
		{
			super();
			_dmDrug = dmDrug;
		}
		
		public function get dmDrug():DmDrug
		{
			return _dmDrug;
		}
	}
}