package hk.org.ha.event.pms.main.vetting.mp.popup
{
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowPendItemPopupEvent extends AbstractTideEvent
	{
		private var _medProfileItem:MedProfileItem;
		
		private var _confirmFunction:Function;
		
		private var _cancelFunction:Function;
		
		public function ShowPendItemPopupEvent(medProfileItem:MedProfileItem, confirmFunction:Function = null, cancelFunction:Function = null)
		{
			super();
			_medProfileItem = medProfileItem;
			_confirmFunction = confirmFunction;
			_cancelFunction = cancelFunction;
		}
		
		public function get medProfileItem():MedProfileItem
		{
			return _medProfileItem;
		}
		
		public function get confirmFunction():Function
		{
			return _confirmFunction;
		}
		
		public function get cancelFunction():Function
		{
			return _cancelFunction;
		}
	}
}