package hk.org.ha.event.pms.main.vetting.mp
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveReplenishmentFromBackendContextEvent extends AbstractTideEvent
	{
		private var _id:Number;
		private var _callBackFunction:Function;
		
		public function RetrieveReplenishmentFromBackendContextEvent(id:Number, callBackFunction:Function=null)
		{
			super();
			_id = id;
			_callBackFunction  = callBackFunction;
		}
		
		public function get id():Number
		{
			return _id;
		}
		
		public function get callBackFunction():Function
		{
			return _callBackFunction;
		}					
	}
}