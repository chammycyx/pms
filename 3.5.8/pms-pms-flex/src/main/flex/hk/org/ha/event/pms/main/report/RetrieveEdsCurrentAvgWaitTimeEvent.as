package hk.org.ha.event.pms.main.report {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveEdsCurrentAvgWaitTimeEvent extends AbstractTideEvent 
	{
		private var _callBack:Function;
		
		public function RetrieveEdsCurrentAvgWaitTimeEvent(callBack:Function):void 
		{
			super();
			_callBack = callBack;
		}
		
		public function get callBack():Function {
			return _callBack;
		}
	}
}