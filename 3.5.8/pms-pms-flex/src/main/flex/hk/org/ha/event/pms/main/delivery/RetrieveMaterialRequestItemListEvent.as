package hk.org.ha.event.pms.main.delivery
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveMaterialRequestItemListEvent extends AbstractTideEvent
	{
		private var _callbackFunction:Function;
		
		public function RetrieveMaterialRequestItemListEvent(callbackFunction:Function)
		{
			super();
			_callbackFunction = callbackFunction;
		}
		
		public function get callbackFunction():Function
		{
			return _callbackFunction;
		}
	}
}