package hk.org.ha.event.pms.main.drug {

	import flash.events.Event;
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.rx.RxItem;
	
	public class RetrieveDrugDiluentListByRxItemEvent extends AbstractTideEvent {		
		
		private var _rxItem:RxItem;							
		
		public function RetrieveDrugDiluentListByRxItemEvent(rxItemVal:RxItem):void
		{
			super();
			_rxItem 		 = rxItemVal;			
		}		
		
		public function get rxItem():RxItem
		{
			return _rxItem;
		}				
	}
}