package hk.org.ha.control.pms.main.ehr {
	
	import hk.org.ha.event.pms.main.ehr.RetrieveEhrssViewerUrlEvent;
	import hk.org.ha.event.pms.main.ehr.ShowEhrssViewerEvent;
	import hk.org.ha.event.pms.main.onestop.ClearKeyboardBufferEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.model.pms.biz.ehr.EhrServiceBean;
	import hk.org.ha.model.pms.vo.ehr.EhrssViewerUrl;
	
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("ehrServiceCtl", restrict="true")]
	public class EhrServiceCtl 
	{
		[In]
		public var ehrService:EhrServiceBean;	
		
		[In]
		public var ctx:Context;
		
		private var _successCallBack:Function;
		private var _errorCallBack:Function;
		
		[Observer]
		public function retrieveEhrssViewerUrl(evt:RetrieveEhrssViewerUrlEvent):void
		{
			ehrService.retrieveEhrssViewerUrl(evt.ehrPatient, retrieveEhrssViewerUrlResult, retrieveEhrssViewerUrlFaultResult);
		}
		
		private function retrieveEhrssViewerUrlResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			evt.context.dispatchEvent( new ShowEhrssViewerEvent(evt.result as EhrssViewerUrl, _successCallBack, _errorCallBack));
		}	
		
		private function retrieveEhrssViewerUrlFaultResult(evt:TideFaultEvent):void 
		{
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());	
		}
	}
}