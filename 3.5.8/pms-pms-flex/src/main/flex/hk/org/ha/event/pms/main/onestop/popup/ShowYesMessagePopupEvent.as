package hk.org.ha.event.pms.main.onestop.popup {
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowYesMessagePopupEvent extends AbstractTideEvent 
	{	
		private var _messages:Array;
		private var _followUpYesFunc:Function;
		
		public function ShowYesMessagePopupEvent(messages:Array, followUpYesFunc:Function):void 
		{
			super();
			_messages = messages;
			_followUpYesFunc = followUpYesFunc;
		}
		
		public function get messages():Array {
			return _messages;
		}
		
		public function get followUpYesFunc():Function
		{
			return _followUpYesFunc;
		}
	}
}