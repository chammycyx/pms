package hk.org.ha.event.pms.main.onestop
{
	import flash.events.Event;
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;

	public class UnlockOrderByRequestEvent extends AbstractTideEvent
	{
		private var _orderId:Number;
		private var _medOrderVersion:Number;
		private var _refillOrderId:Number;
		
		public function UnlockOrderByRequestEvent(medOrderId:Number, medOrderVersion:Number, refillOrderId:Number)
		{
			super();
			_orderId = medOrderId;
			_medOrderVersion = medOrderVersion;
			_refillOrderId = refillOrderId;
		}
		
		public function get orderId():Number {
			return _orderId;
		}
		
		public function get medOrderVersion():Number {
			return _medOrderVersion;
		}
		
		public function get refillOrderId():Number
		{
			return _refillOrderId;
		}
	}
}