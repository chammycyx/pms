package hk.org.ha.control.pms.main.onestop
{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.onestop.ReprintClearEvent;
	import hk.org.ha.event.pms.main.onestop.ReprintLabelAndCouponEvent;
	import hk.org.ha.event.pms.main.onestop.RetrieveDispOrderByTicketNumEvent;
	import hk.org.ha.event.pms.main.onestop.RetrieveRefillScheduleListForReprintEvent;
	import hk.org.ha.event.pms.main.onestop.SelectReprintTabEvent;
	import hk.org.ha.event.pms.main.onestop.SetDefaultReprintScreenEvent;
	import hk.org.ha.event.pms.main.onestop.ShowReprintInfomationMessageEvent;
	import hk.org.ha.event.pms.main.onestop.popup.ShowMessagePopupEvent;
	import hk.org.ha.event.pms.main.onestop.popup.ShowReprintMessagePopupEvent;
	import hk.org.ha.event.pms.main.onestop.show.ShowReprintViewEvent;
	import hk.org.ha.fmk.pms.flex.components.core.MenuItem;
	import hk.org.ha.fmk.pms.flex.components.window.WindowSwitchEvent;
	import hk.org.ha.model.pms.biz.onestop.ReprintCouponLabelServiceBean;
	import hk.org.ha.model.pms.biz.onestop.ReprintServiceBean;
	
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;

	[Bindable]
	[Name("reprintServiceCtl", restrict="true")]
	public class ReprintServiceCtl
	{		
		[In]
		public var reprintService:ReprintServiceBean;
		
		[In]
		public var reprintCouponLabelService:ReprintCouponLabelServiceBean;
		
		private var retrieveDispOrderByTicketNumEvent:RetrieveDispOrderByTicketNumEvent;
				
		[Observer]
		public function retrieveRefillScheduleListForReprint(evt:RetrieveRefillScheduleListForReprintEvent):void
		{
			reprintService.retrieveRefillScheduleListForReprint(evt.refillScheduleId, retrieveRefillScheduleListForReprintResult);
		}
		
		private function retrieveRefillScheduleListForReprintResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new WindowSwitchEvent(new MenuItem("Reprint", ShowReprintViewEvent), null, false));
			evt.context.dispatchEvent(new ShowReprintViewEvent("One Stop Entry", true, true));
		}
		
		[Observer]
		public function retrieveDispOrderByTicketNum(evt:RetrieveDispOrderByTicketNumEvent):void {
			retrieveDispOrderByTicketNumEvent = evt;
			reprintService.retrieveDispOrderByTicketNum(evt.dispDate, evt.ticketNum, evt.triggerByOtherScreen, retrieveDispOrderByTicketNumResult);
		}
		
		private function retrieveDispOrderByTicketNumResult(evt:TideResultEvent):void {
			if (retrieveDispOrderByTicketNumEvent.triggerByOtherScreen)
			{
				evt.context.dispatchEvent(new WindowSwitchEvent(new MenuItem("Reprint", ShowReprintViewEvent), null, false));
				evt.context.dispatchEvent(new ShowReprintViewEvent(retrieveDispOrderByTicketNumEvent.srcEntryView, true, true));
			}
			else
			{
				evt.context.dispatchEvent(new SelectReprintTabEvent());
			}
		}
				
		[Observer]
		public function reprintClear(evt:ReprintClearEvent):void {
			reprintService.clear(reprintClearResult);
		}
		
		private function reprintClearResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new SetDefaultReprintScreenEvent());
		}
		
		[Observer]
		public function reprintLabelAndCoupon(evt:ReprintLabelAndCouponEvent):void {
			reprintCouponLabelService.reprintLabelAndRefillCoupon(evt.dispOrderItemList, evt.stdRefillScheduleItemList, evt.capdVoucherList, 
																  evt.printSfiInvoice, evt.printSfiCoupon, evt.printLanguage, evt.printMedSummaryReport, 
																  evt.ticketDate, evt.ticketNum, reprintLabelAndCouponResult);
		}
		
		private function reprintLabelAndCouponResult(evt:TideResultEvent):void {
			
			if (evt.result != null)
			{
				evt.context.dispatchEvent(new ShowReprintInfomationMessageEvent(new Array(evt.result as String)));
			}
			else
			{
				reprintService.clear(clearResult);
			}
		}
		
		
		private function clearResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new SetDefaultReprintScreenEvent());
			evt.context.dispatchEvent(new ShowReprintMessagePopupEvent(new Array("0196")));
		}
	}
}
