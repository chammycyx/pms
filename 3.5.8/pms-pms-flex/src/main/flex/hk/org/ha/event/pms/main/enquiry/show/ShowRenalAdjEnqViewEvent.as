package hk.org.ha.event.pms.main.enquiry.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowRenalAdjEnqViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		public function ShowRenalAdjEnqViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}