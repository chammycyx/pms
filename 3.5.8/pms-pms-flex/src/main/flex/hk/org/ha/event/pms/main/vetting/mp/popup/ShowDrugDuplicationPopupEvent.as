package hk.org.ha.event.pms.main.vetting.mp.popup
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDrugDuplicationPopupEvent extends AbstractTideEvent
	{
		private var _inactivePasWardFlag:Boolean;
		
		private var _duplicationResultList:ArrayCollection;
		
		private var _confirmFunction:Function;
		
		private var _cancelFunction:Function;
		
		public function ShowDrugDuplicationPopupEvent(inactivePasWardFlag:Boolean, duplicationResultList:ArrayCollection, confirmFunction:Function, cancelFunction:Function)
		{
			super();
			_inactivePasWardFlag = inactivePasWardFlag;
			_duplicationResultList = duplicationResultList;
			_confirmFunction = confirmFunction;
			_cancelFunction = cancelFunction;
		}
		
		public function get inactivePasWardFlag():Boolean
		{
			return _inactivePasWardFlag;
		}
		
		public function get duplicationResultList():ArrayCollection
		{
			return _duplicationResultList;
		}
		
		public function get confirmFunction():Function
		{
			return _confirmFunction;
		}
		
		public function get cancelFunction():Function
		{
			return _cancelFunction;
		}
	}
}