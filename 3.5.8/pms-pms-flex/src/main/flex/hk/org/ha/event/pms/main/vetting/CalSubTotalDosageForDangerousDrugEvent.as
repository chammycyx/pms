package hk.org.ha.event.pms.main.vetting {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
	import hk.org.ha.model.pms.vo.rx.RxItem;
	
	public class CalSubTotalDosageForDangerousDrugEvent extends AbstractTideEvent {		
		
		private var _rxItem:RxItem;		
		private var _doseGroupIndex:int;
		private var _doseIndex:int;
		private var _callBackFunction:Function;
		
		public function CalSubTotalDosageForDangerousDrugEvent(rxItemVal:RxItem, doseGroupIndex:int, doseIndex:int, functionValue:Function)
		{
			super();						
			_rxItem 		  = rxItemVal;
			_doseGroupIndex   = doseGroupIndex;
			_doseIndex		  = doseIndex;
			_callBackFunction = functionValue;
		}		
		
		public function get rxItem():RxItem 
		{
			return _rxItem;
		}
		
		public function get doseGroupIndex():int 
		{
			return _doseGroupIndex;
		}
		
		public function get doseIndex():int 
		{			
			return _doseIndex;
		}
		
		public function get callBackFunction():Function
		{
			return _callBackFunction;
		}
	}
}