package hk.org.ha.view.pms.main.cddh
{
  import flexlib.scheduling.scheduleClasses.SimpleScheduleEntry;
  import hk.org.ha.model.pms.persistence.disp.DispOrderItem;

  public class CddhSimpleScheduleEntry extends SimpleScheduleEntry
  {
	  private var _hospCode:String;
	  private var _specCode:String;
	  private var _itemDesc:String;
	  private var _tradeName:String;
	  
	  public var _backgroundColor : int = 0x339900;
	  
	  public function CddhSimpleScheduleEntry () {
		  super();
	  }
	  
	  public function get hospCode():String {
		  return _hospCode;
	  }
	  
	  public function set hospCode(value:String):void {
		  _hospCode = value;
	  }
	  
	  public function get specCode():String {
		  return _specCode;
	  }
	  
	  public function set specCode(value:String):void {
		  _specCode = value;
	  }
	  
	  public function get itemDesc():String {
		  return _itemDesc;
	  }
	  
	  public function set itemDesc(value:String):void {
		  _itemDesc = value;
	  }
	  
	  public function get tradeName():String {
		  return _tradeName;
	  }
	  
	  public function set tradeName(value:String):void {
		  _tradeName = value;
	  }
	  
	  public function get backgroundColor():int {
		  return _backgroundColor;
	  }
	  
	  public function set backgroundColor(value:int):void {
		  _backgroundColor = value;
	  }
  }
}