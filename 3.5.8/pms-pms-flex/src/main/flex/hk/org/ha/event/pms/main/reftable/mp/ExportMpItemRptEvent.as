package hk.org.ha.event.pms.main.reftable.mp {
	
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.udt.reftable.mp.MpItemReportType;
	
	public class ExportMpItemRptEvent extends AbstractTideEvent 
	{
		private var _mpItemReportType:MpItemReportType;
		
		public function get mpItemReportType():MpItemReportType
		{
			return _mpItemReportType;
		}

		public function set mpItemReportType(value:MpItemReportType):void
		{
			_mpItemReportType = value;
		}

		public function ExportMpItemRptEvent(mpItemReportType:MpItemReportType):void 
		{
			super();
			_mpItemReportType = mpItemReportType;

		}
		
	}
}