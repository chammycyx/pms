package hk.org.ha.event.pms.main.onestop {
		
	import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
	
	import org.granite.tide.events.AbstractTideEvent;
		
	public class CheckManualOrderFcsClearanceEvent extends AbstractTideEvent 
	{
		private var _pasApptSeq:Number;
		private var _patHospCode:String;
		private var _orderId:Number;
		private var _fcsError:String;
		private var _fcsErrorDetails:String;
		private var _fcsForceProceedRequireYes:Function;
		private var _fcsForceProceedRequireNo:Function;
		private var _fcsForceProceedNotRequire:Function;
		
		public function CheckManualOrderFcsClearanceEvent(patHospCode:String, orderId:Number, pasApptSeq:Number, 
															 fcsError:String, fcsErrorDetails:String, 
															 fcsForceProceedRequireYes:Function, fcsForceProceedRequireNo:Function,
															 fcsForceProceedNotRequire:Function):void 
		{
			super();
			_patHospCode = patHospCode;
			_orderId = orderId;
			_pasApptSeq = pasApptSeq;
			_fcsError = fcsError;
			_fcsErrorDetails = fcsErrorDetails;
			_fcsForceProceedRequireYes = fcsForceProceedRequireYes;
			_fcsForceProceedRequireNo = fcsForceProceedRequireNo;
			_fcsForceProceedNotRequire = fcsForceProceedNotRequire;
		}   
		
		public function get orderId():Number
		{
			return _orderId;
		}
		
		public function get pasApptSeq():Number
		{
			return _pasApptSeq;
		}
		
		public function get patHospCode():String
		{
			return _patHospCode;
		}
		
		public function get fcsError():String
		{
			return _fcsError;
		}
		
		public function get fcsErrorDetails():String
		{
			return _fcsErrorDetails;
		}
		
		public function get fcsForceProceedRequireYes():Function
		{
			return _fcsForceProceedRequireYes;
		}
		
		public function get fcsForceProceedRequireNo():Function
		{
			return _fcsForceProceedRequireNo;
		}	
		
		public function get fcsForceProceedNotRequire():Function
		{
			return _fcsForceProceedNotRequire;
		}
	}
}