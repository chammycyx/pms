package hk.org.ha.event.pms.main.assembling
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateDispOrderItemStatusToAssembledEvent extends AbstractTideEvent 
	{		
		private var _ticketDate:Date;
		private var _ticketNum:String;
		private var _itemNum:Number;
		
		public function UpdateDispOrderItemStatusToAssembledEvent(ticketDate:Date, ticketNum:String, itemNum:Number):void 
		{
			super();
			_ticketDate = ticketDate;
			_ticketNum = ticketNum;
			_itemNum = itemNum;
		}
		
		public function get ticketDate():Date {
			return _ticketDate;
		}
		
		public function get ticketNum():String {
			return _ticketNum;
		}
		
		public function get itemNum():Number {
			return _itemNum;
		}
	}
}