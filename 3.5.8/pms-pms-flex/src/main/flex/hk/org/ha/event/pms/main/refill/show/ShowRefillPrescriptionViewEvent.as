package hk.org.ha.event.pms.main.refill.show {
	
	import hk.org.ha.model.pms.persistence.disp.PharmOrder;
	import hk.org.ha.model.pms.vo.ehr.EhrPatient;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowRefillPrescriptionViewEvent extends AbstractTideEvent 
	{
		private var _pharmOrder:PharmOrder;
		private var _callbackFunc:Function;
		private var _skipDataReload:Boolean;
		private var _clearMessages:Boolean;		
		private var _ehrPatient:EhrPatient;
		
		public function ShowRefillPrescriptionViewEvent(pharmOrder:PharmOrder, callbackFunc:Function = null, skipDataReload:Boolean = false, clearMessages:Boolean=true, ehrPatient:EhrPatient=null):void 
		{
			super();
			_pharmOrder = pharmOrder;
			_callbackFunc = callbackFunc;
			_skipDataReload = skipDataReload;			
			_clearMessages = clearMessages;
			_ehrPatient = ehrPatient;
		}
		
		public function get pharmOrder():PharmOrder {
			return _pharmOrder;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
		
		public function get skipDataReload():Boolean {
			return _skipDataReload;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
		
		public function get ehrPatient():EhrPatient {
			return _ehrPatient;
		}
	}
}