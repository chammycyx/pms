package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveWorkstoreDrugForOrderEditEvent extends AbstractTideEvent 
	{
		private var _displayName:String;
		private var _fmStatus:String;
		private var _itemCode:String;
		private var _callBackFunction:Function;	
		
		public function RetrieveWorkstoreDrugForOrderEditEvent(displayNameValue:String, fmStatusValue:String, itemCodeValue:String, functionValue:Function=null):void 
		{
			super();
			_displayName = displayNameValue;
			_fmStatus = fmStatusValue
			_itemCode = itemCodeValue;
			_callBackFunction = functionValue;
		}
		
		public function get displayName():String
		{
			return _displayName;
		}
		
		public function get fmStatus():String
		{
			return _fmStatus;
		}
		
		public function get itemCode():String
		{
			return _itemCode;
		}
		
		public function get callBackFunction():Function 
		{
			return _callBackFunction;
		}
	}
}