package hk.org.ha.event.pms.main.reftable.mp {
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveCommonOrderStatusListEvent extends AbstractTideEvent 
	{
		private var _patHospCode:String;
		private var _itemCode:String;
		private var _drugKey:Number;
		
		public function get patHospCode():String
		{
			return _patHospCode;
		}

		public function get itemCode():String
		{
			return _itemCode;
		}
		
		public function get drugKey():Number
		{
			return _drugKey;
		}

		public function RetrieveCommonOrderStatusListEvent(patHospCode:String, itemCode:String, drugKey:Number):void 
		{
			_patHospCode = patHospCode;
			_itemCode = itemCode;
			_drugKey = drugKey;
			super();
		}
	}
}