package hk.org.ha.event.pms.main.refill {
	
	import hk.org.ha.model.pms.persistence.disp.PharmOrder;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateRefillScheduleListEvent extends AbstractTideEvent 
	{
		private var _sourceView:String;
		private var _pharmOrder:PharmOrder;
		private var _updateAction:String;
		private var _currRsId:Number;
		private var _callbackFunc:Function;
		
		public function UpdateRefillScheduleListEvent(sourceView:String, pharmOrder:PharmOrder, updateAction:String, currRsId:Number = -1, callbackFunc:Function = null):void 
		{
			super();
			_sourceView = sourceView;
			_pharmOrder = pharmOrder;
			_updateAction = updateAction;
			_currRsId = currRsId;
			_callbackFunc = callbackFunc;
		}
		
		public function get sourceView():String {
			return _sourceView;
		}
		
		public function get pharmOrder():PharmOrder {
			return _pharmOrder;
		}
		
		public function get updateAction():String {
			return _updateAction;
		}
		
		public function get currRsId():Number {
			return _currRsId;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}