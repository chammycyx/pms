package hk.org.ha.event.pms.main.inbox
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ConstructManualProfileEvent extends AbstractTideEvent
	{
		private var _hkid:String;
		private var _caseNum:String;
		
		public function ConstructManualProfileEvent(hkid:String, caseNum:String)
		{
			super();
			_hkid = hkid;
			_caseNum = caseNum;
		}
		
		public function get hkid():String
		{
			return _hkid;
		}
		
		public function get caseNum():String
		{
			return _caseNum;
		}
	}
}