package hk.org.ha.control.pms.main.reftable {
	
	import hk.org.ha.event.pms.main.reftable.InitRefillQtyAdjTabEvent;
	import hk.org.ha.event.pms.main.reftable.RetrieveRefillQtyAdjListEvent;
	import hk.org.ha.event.pms.main.reftable.ShowRefillModuleMaintSaveSuccessMsgEvent;
	import hk.org.ha.event.pms.main.reftable.UpdateRefillQtyAdjListEvent;
	import hk.org.ha.model.pms.biz.reftable.RefillQtyAdjListServiceBean;
	
	import mx.collections.ArrayCollection;
	import mx.core.UIComponent;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("refillQtyAdjListServiceCtl", restrict="true")]
	public class RefillQtyAdjListServiceCtl 
	{
		[In]
		public var refillQtyAdjListService:RefillQtyAdjListServiceBean;
		
		private var nextTabIndex:Number = 2;
		private var nextSubTabIndex:Number = 0;
		private var showRefillModuleMaintAlertMsg:Boolean = true;
		private var caller:UIComponent = null;
		
		[Observer]
		public function retrieveRefillQtyAdjList(evt:RetrieveRefillQtyAdjListEvent):void
		{
			refillQtyAdjListService.retrieveRefillQtyAdjList(initRefillQtyAdjSubTab);
		}
		
		[Observer]
		public function updateRefillQtyAdjList(evt:UpdateRefillQtyAdjListEvent):void
		{
			showRefillModuleMaintAlertMsg = evt.showRefillModuleMaintAlertMsg;
			nextTabIndex = evt.nextTabIndex;
			nextSubTabIndex = evt.nextSubTabIndex;
			caller = evt.caller;
			
			refillQtyAdjListService.updateRefillQtyAdjList(evt.updateRefillQtyAdjList, 
															evt.delRefillQtyAdjList, 
															updateRefillQtyAdjListResult);
		}
		
		private function initRefillQtyAdjSubTab(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new InitRefillQtyAdjTabEvent(evt.result as ArrayCollection));
		}
		
		private function updateRefillQtyAdjListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent( new ShowRefillModuleMaintSaveSuccessMsgEvent(nextTabIndex, nextSubTabIndex, showRefillModuleMaintAlertMsg, caller));
		}
		
	}
}
