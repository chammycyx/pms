package hk.org.ha.event.pms.main.reftable.mp {
	
	import hk.org.ha.model.pms.dms.persistence.PmsPcuMapping;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveMpQtyDurationConversionListEvent extends AbstractTideEvent 
	{
		private var _itemCode:String;
		private var _pmsPcuMapping:PmsPcuMapping;
		
		public function RetrieveMpQtyDurationConversionListEvent(itemCode:String, pmsPcuMapping:PmsPcuMapping):void 
		{
			super();
			_itemCode = itemCode;
			_pmsPcuMapping = pmsPcuMapping;
		}
		
		public function get itemCode():String {
			return _itemCode;
		}
		
		public function get pmsPcuMapping():PmsPcuMapping {
			return _pmsPcuMapping;
		}
	}
}