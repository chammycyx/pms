package hk.org.ha.event.pms.main.checkissue
{
	import hk.org.ha.model.pms.persistence.corp.Workstore;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveBatchIssueDetailEvent extends AbstractTideEvent 
	{		
		private var _issueDate:Date;
		private var _workstore:Workstore;
		
		public function RetrieveBatchIssueDetailEvent(issueDate:Date, workstore:Workstore):void 
		{
			super();
			_issueDate = issueDate;
			_workstore = workstore;
		}
		
		public function get issueDate():Date {
			return _issueDate;
		}
		
		public function get workstore():Workstore {
			return _workstore;
		}
	}
}