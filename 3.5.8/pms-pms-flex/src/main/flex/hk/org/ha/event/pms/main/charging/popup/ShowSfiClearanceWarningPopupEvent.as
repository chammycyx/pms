package hk.org.ha.event.pms.main.charging.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.udt.charging.ClearanceStatus;
	
	public class ShowSfiClearanceWarningPopupEvent extends AbstractTideEvent 
	{	
		private var _clearanceStatus:ClearanceStatus;
		private var _actionBy:String;
		private var _dispOrderId:Number;
		
		public function ShowSfiClearanceWarningPopupEvent(clearanceStatus:ClearanceStatus, actionBy:String, dispOrderId:Number):void 
		{
			super();
			_clearanceStatus = clearanceStatus;
			_actionBy = actionBy;
			_dispOrderId = dispOrderId;
		}
		
		public function get clearanceStatus():ClearanceStatus{
			return _clearanceStatus;
		}
		
		public function get actionBy():String{
			return _actionBy;
		}
		
		public function get dispOrderId():Number{
			return _dispOrderId;
		}
	}
}