package hk.org.ha.event.pms.main.pivas.popup
{
	import hk.org.ha.event.pms.main.vetting.mp.popup.ShowEditPivasDetailPopupEvent;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowPivasPrepDetailPivasDetailPopupEvent extends AbstractTideEvent
	{
		private var _showEditPivasDetailPopupEvent:ShowEditPivasDetailPopupEvent;
		
		public function ShowPivasPrepDetailPivasDetailPopupEvent(showEditPivasDetailPopupEvent:ShowEditPivasDetailPopupEvent)
		{
			super();
			_showEditPivasDetailPopupEvent = showEditPivasDetailPopupEvent;
		}
		
		public function get showEditPivasDetailPopupEvent():ShowEditPivasDetailPopupEvent
		{
			return _showEditPivasDetailPopupEvent;
		}
	}
}