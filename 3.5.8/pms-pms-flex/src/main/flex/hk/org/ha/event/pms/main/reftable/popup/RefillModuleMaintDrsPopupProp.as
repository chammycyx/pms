package hk.org.ha.event.pms.main.reftable.popup
{
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.model.pms.persistence.reftable.DrsConfig;

	public class RefillModuleMaintDrsPopupProp 
	{
		public var drsConfig:DrsConfig;
		
		public var pasSpecialtyForDrsConfigList:ArrayCollection;
		
		public var otherDrsConfigList:ArrayCollection;
		
		public var okHandler:Function;
	}
}
