package hk.org.ha.event.pms.main.reftable.pivas {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrieveDrugInfoListByDisplayNameEvent extends AbstractTideEvent {
	
		private var _displayName:String;
		private var _callback:Function;

		public function RetrieveDrugInfoListByDisplayNameEvent(displayName:String, callback:Function) {
			super();
			_displayName = displayName;
			_callback = callback;
		}

		public function get displayName():String {
			return _displayName;
		}

		public function get callback():Function {
			return _callback;
		}
	}
}