package hk.org.ha.event.pms.main.prevet
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class PreVetOperationCompletedFollowUpEvent extends AbstractTideEvent
	{				
		private var _updateGlobalPreVetValue:Boolean;
		
		public function PreVetOperationCompletedFollowUpEvent(updateGlobalPreVetValue:Boolean)
		{
			super();
			_updateGlobalPreVetValue = updateGlobalPreVetValue;
		}
		
		public function get updateGlobalPreVetValue():Boolean
		{
			return _updateGlobalPreVetValue;
		}
	}
}