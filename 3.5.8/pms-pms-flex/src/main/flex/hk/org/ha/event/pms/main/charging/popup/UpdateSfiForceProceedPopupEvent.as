package hk.org.ha.event.pms.main.charging.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.charging.SfiForceProceedInfo;
	import hk.org.ha.view.pms.main.charging.popup.SfiForceProceedPopup;
	
	public class UpdateSfiForceProceedPopupEvent extends AbstractTideEvent 
	{		
		private var _sfiForceProceedEntry:SfiForceProceedInfo;
		private var _sfiForceProceedConfirmCallback:Function;
		
		public function UpdateSfiForceProceedPopupEvent(sfiForceProceedEntry:SfiForceProceedInfo, sfiForceProceedConfirmCallback:Function):void 
		{
			super();
			_sfiForceProceedEntry = sfiForceProceedEntry;
			_sfiForceProceedConfirmCallback = sfiForceProceedConfirmCallback;
		}  
		
		public function get sfiForceProceedEntry():SfiForceProceedInfo{
			return _sfiForceProceedEntry;
		}
		
		public function get sfiForceProceedConfirmCallback():Function{
			return _sfiForceProceedConfirmCallback;
		}
	}
}