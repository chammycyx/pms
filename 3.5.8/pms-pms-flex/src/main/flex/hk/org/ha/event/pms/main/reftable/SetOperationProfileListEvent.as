package hk.org.ha.event.pms.main.reftable {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetOperationProfileListEvent extends AbstractTideEvent 
	{
		private var _invalidateSession:Boolean;
		
		public function SetOperationProfileListEvent(invalidateSession:Boolean):void 
		{
			super();
			_invalidateSession = invalidateSession;
		}

		public function get invalidateSession():Boolean {
			return _invalidateSession;
		}
	}
}