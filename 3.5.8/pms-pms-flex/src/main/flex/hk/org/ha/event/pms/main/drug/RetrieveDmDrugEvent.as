package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDmDrugEvent extends AbstractTideEvent 
	{
		private var _itemCode:String;
		private var _resultEvent:Event;
		
		public function RetrieveDmDrugEvent(itemCode:String, resultEvent:Event=null):void 
		{
			super();
			_itemCode = itemCode;
			_resultEvent = resultEvent;
		}
		
		public function get itemCode():String
		{
			return _itemCode;
		}
		
		public function get resultEvent():Event
		{
			return _resultEvent;
		}
	}
}