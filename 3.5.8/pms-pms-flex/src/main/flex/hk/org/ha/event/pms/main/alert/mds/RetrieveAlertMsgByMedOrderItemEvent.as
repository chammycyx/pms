package hk.org.ha.event.pms.main.alert.mds {
	
	import hk.org.ha.event.pms.main.alert.mds.popup.ShowAlertMsgPopupEvent;
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	
	public class RetrieveAlertMsgByMedOrderItemEvent extends AbstractTideEvent {

		private var _medOrderItem:MedOrderItem;
		
		private var _showAlertMsgPopupEvent:ShowAlertMsgPopupEvent;
		
		private var _orderEditFlag:Boolean;
		
		public function RetrieveAlertMsgByMedOrderItemEvent(medOrderItem:MedOrderItem, showAlertMsgPopupEvent:ShowAlertMsgPopupEvent=null, orderEditFlag:Boolean=false) {
			super();
			_medOrderItem = medOrderItem;
			_showAlertMsgPopupEvent = showAlertMsgPopupEvent;
			_orderEditFlag = orderEditFlag;
		}
		
		public function get medOrderItem():MedOrderItem {
			return _medOrderItem;
		}
		
		public function get showAlertMsgPopupEvent():ShowAlertMsgPopupEvent {
			return _showAlertMsgPopupEvent;
		}
		
		public function get orderEditFlag():Boolean {
			return _orderEditFlag;
		}
	}
}