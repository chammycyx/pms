package hk.org.ha.event.pms.main.report {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	import hk.org.ha.model.pms.vo.alert.AlertProfile;
	import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;
	
	public class PrintMdsMpRptEvent extends AbstractTideEvent {
		
		private var _alertMsg:AlertMsg;
		
		private var _alertProfile:AlertProfile;
		
		public function PrintMdsMpRptEvent(alertMsg:AlertMsg, alertProfile:AlertProfile) {
			super();
			_alertMsg = alertMsg;
			_alertProfile = alertProfile;
		}
		
		public function get alertMsg():AlertMsg {
			return _alertMsg;
		}
		
		public function get alertProfile():AlertProfile {
			return _alertProfile;
		}
	}
}