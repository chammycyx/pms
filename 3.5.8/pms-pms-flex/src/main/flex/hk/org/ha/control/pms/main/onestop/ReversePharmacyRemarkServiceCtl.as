package hk.org.ha.control.pms.main.onestop
{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.onestop.RefreshOrderStatusEvent;
	import hk.org.ha.event.pms.main.onestop.ReversePharmacyRemarkEvent;
	import hk.org.ha.event.pms.main.onestop.ShowInfomationMessageEvent;
	import hk.org.ha.model.pms.biz.onestop.OneStopServiceBean;
	import hk.org.ha.model.pms.biz.onestop.ReversePharmacyRemarkServiceBean;
	import hk.org.ha.model.pms.vo.onestop.OneStopOrderItemSummary;
	import hk.org.ha.model.pms.vo.onestop.OneStopRefillItemSummary;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;

	[Bindable]
	[Name("reversePharmacyRemarkServiceCtl", restrict="true")]
	public class ReversePharmacyRemarkServiceCtl
	{		
		[In]
		public var reversePharmacyRemarkService:ReversePharmacyRemarkServiceBean;
		
		[In]
		public var oneStopService:OneStopServiceBean;
		
		[In]
		public var latestOneStopOrderItemSummary:OneStopOrderItemSummary;
		
		[In]
		public var latestOneStopRefillItemSummary:OneStopRefillItemSummary;
		
		[In]
		public var latestOneStopRefillItemSummaryList:ArrayCollection;
		
		private var medOrderId:Number;
				
		[Observer]
		public function reversePharmacyRemark(evt:ReversePharmacyRemarkEvent):void {
			medOrderId = evt.medOrderId;
			reversePharmacyRemarkService.reversePharmacyRemark(evt.medOrderId, evt.medOrderVersion, reversePharmacyRemarkResult);
		}
		
		private function reversePharmacyRemarkResult(evt:TideResultEvent):void {
			oneStopService.refreshPharmacyOrderStatus(medOrderId, refreshPharmacyOrderStatusResult);
		}
		
		private function refreshPharmacyOrderStatusResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshOrderStatusEvent(latestOneStopOrderItemSummary, 
				latestOneStopRefillItemSummary, latestOneStopRefillItemSummaryList));
			evt.context.dispatchEvent(new ShowInfomationMessageEvent(new Array("0532"), true));
		}
	}
}
