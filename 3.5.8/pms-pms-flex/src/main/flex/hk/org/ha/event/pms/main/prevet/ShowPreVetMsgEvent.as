package hk.org.ha.event.pms.main.prevet
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowPreVetMsgEvent extends AbstractTideEvent
	{		
		public var messageCode:String = null;
		
		public function ShowPreVetMsgEvent(messageCode:String=null)
		{
			super();
			this.messageCode = messageCode;
		}
	}
}