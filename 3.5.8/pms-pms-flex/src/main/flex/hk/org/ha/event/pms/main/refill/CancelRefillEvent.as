package hk.org.ha.event.pms.main.refill {
	import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CancelRefillEvent extends AbstractTideEvent 
	{		
		private var _orderViewInfo:OrderViewInfo;
		private var _callback:Function;
		
		public function CancelRefillEvent(orderViewInfo:OrderViewInfo, callback:Function=null):void 
		{
			super();
			_callback = callback;
			_orderViewInfo = orderViewInfo;
		}
		
		public function get callback():Function {
			return _callback;
		}
		
		public function get orderViewInfo():OrderViewInfo {
			return _orderViewInfo;
		}
	}
}