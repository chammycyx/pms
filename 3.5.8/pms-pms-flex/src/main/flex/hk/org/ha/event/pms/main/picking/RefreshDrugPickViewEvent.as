package hk.org.ha.event.pms.main.picking
{
	import hk.org.ha.model.pms.vo.picking.PickItemListSummary;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshDrugPickViewEvent extends AbstractTideEvent 
	{		
		private var _pickitemListSummary:PickItemListSummary;
		private var _resultMsg:String;
		
		public function RefreshDrugPickViewEvent(pickitemListSummary:PickItemListSummary, resultMsg:String=null):void 
		{
			super();
			_pickitemListSummary = pickitemListSummary;
			_resultMsg = resultMsg;
		}
		
		public function get pickitemListSummary():PickItemListSummary {
			return _pickitemListSummary;
		}
		
		public function get resultMsg():String {
			return _resultMsg;
		}
	}
}