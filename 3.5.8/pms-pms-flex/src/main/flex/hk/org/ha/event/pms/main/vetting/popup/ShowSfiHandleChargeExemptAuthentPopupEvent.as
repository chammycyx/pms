package hk.org.ha.event.pms.main.vetting.popup {
	
	import flash.events.Event;
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowSfiHandleChargeExemptAuthentPopupEvent extends AbstractTideEvent 
	{		
		private var _successHandler:Function;
		
		private var _faultHandler:Function;
		
		private var _medOrderItemList:ListCollectionView;
		
		public function ShowSfiHandleChargeExemptAuthentPopupEvent(successHandler:Function=null, faultHandler:Function=null, medOrderItemList:ListCollectionView=null):void 
		{
			super();
			_successHandler = successHandler;
			_faultHandler = faultHandler;
			_medOrderItemList = medOrderItemList;
		}
		
		public function get successHandler():Function {
			return _successHandler;
		}
		
		public function get faultHandler():Function {
			return _faultHandler;
		}
		
		public function get medOrderItemList():ListCollectionView {
			return _medOrderItemList;
		}
	}
}
