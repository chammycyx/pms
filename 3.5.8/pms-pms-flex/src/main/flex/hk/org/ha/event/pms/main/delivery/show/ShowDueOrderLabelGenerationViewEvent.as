package hk.org.ha.event.pms.main.delivery.show
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDueOrderLabelGenerationViewEvent extends AbstractTideEvent
	{
		public function ShowDueOrderLabelGenerationViewEvent()
		{
			super();
		}
	}
}