package hk.org.ha.event.pms.main.pivas
{
	import hk.org.ha.model.pms.persistence.pivas.PivasWorklist;
	import hk.org.ha.model.pms.udt.pivas.PivasWorklistType;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdatePivasWorklistEvent extends AbstractTideEvent 
	{		
		private var _sourceType:PivasWorklistType;
		
		private var _pivasWorklist:PivasWorklist;

		private var _addToWorklist:Boolean;
		
		private var _callbackFunc:Function;
		
		public function UpdatePivasWorklistEvent(pivasWorklist:PivasWorklist, addToWorklist:Boolean, sourceType:PivasWorklistType, callbackFunc:Function):void 
		{
			super();
			_pivasWorklist = pivasWorklist;
			_addToWorklist = addToWorklist;
			_sourceType = sourceType;
			_callbackFunc = callbackFunc;
		}
		
		public function get pivasWorklist():PivasWorklist {
			return _pivasWorklist;
		}
		
		public function get addToWorklist():Boolean {
			return _addToWorklist;
		}
		
		public function get sourceType():PivasWorklistType {
			return _sourceType;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}