package hk.org.ha.event.pms.main.support
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class SetWorkstationPropMaintViewHospitalListEvent extends AbstractTideEvent 
	{		
		private var _hospitalList:ArrayCollection;
		
		public function SetWorkstationPropMaintViewHospitalListEvent(hospitalList:ArrayCollection):void
		{
			super();
			_hospitalList = hospitalList;
		}
		
		public function get hospitalList():ArrayCollection { 
			return _hospitalList;
		}
	}
}