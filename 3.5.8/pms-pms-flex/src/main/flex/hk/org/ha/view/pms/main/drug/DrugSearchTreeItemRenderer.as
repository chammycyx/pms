package hk.org.ha.view.pms.main.drug {
	
	import mx.controls.treeClasses.TreeItemRenderer;
	import mx.controls.treeClasses.TreeListData;
	
	public class DrugSearchTreeItemRenderer extends TreeItemRenderer
	{
		public function DrugSearchTreeItemRenderer() 
		{
			super();
			mouseEnabled = false;			
		}
		
		override public function set data(value:Object):void
		{
			if(value != null)
			{ 
				super.data = value;
				setStyle("fontWeight", 'bold');
				setStyle("fontSize", '16');
			}
		}	 

		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void {
			super.updateDisplayList(unscaledWidth, unscaledHeight);
		}
	}
}