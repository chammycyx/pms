package hk.org.ha.fmk.pms.flex.components.core {
	
	import mx.core.IButton;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShortcutKeyEvent extends AbstractTideEvent 
	{	
		private var _srcButton:IButton;
		private var _triggerTime:Number
		
		public function ShortcutKeyEvent(srcButton:IButton):void 
		{
			super();
			this._srcButton = srcButton;
			this._triggerTime = new Date().time;
		}
		
		public function get srcButton():IButton
		{
			return _srcButton;
		}
		
		public function get triggerTime():Number
		{
			return _triggerTime;
		}
	}
}