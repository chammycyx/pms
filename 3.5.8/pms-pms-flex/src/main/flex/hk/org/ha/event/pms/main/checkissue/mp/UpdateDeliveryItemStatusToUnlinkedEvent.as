package hk.org.ha.event.pms.main.checkissue.mp
{
	import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
	import hk.org.ha.model.pms.vo.checkissue.mp.CheckDelivery;
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateDeliveryItemStatusToUnlinkedEvent extends AbstractTideEvent 
	{			
		private var _checkDelivery:CheckDelivery;
		
		public function UpdateDeliveryItemStatusToUnlinkedEvent(checkDelivery:CheckDelivery):void 
		{
			super();
			_checkDelivery = checkDelivery;				
		}
		
		public function get checkDelivery():CheckDelivery {
			return _checkDelivery;
		}
	}
}