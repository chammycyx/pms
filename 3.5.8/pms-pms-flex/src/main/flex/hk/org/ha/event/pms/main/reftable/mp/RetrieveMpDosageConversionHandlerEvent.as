package hk.org.ha.event.pms.main.reftable.mp
{
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveMpDosageConversionHandlerEvent extends AbstractTideEvent 
	{
		
		private var _pmsIpDosageConvList:ArrayCollection;
		
		public function get pmsIpDosageConvList():ArrayCollection
		{
			return _pmsIpDosageConvList;
		}

		public function RetrieveMpDosageConversionHandlerEvent(pmsIpDosageConvList:ArrayCollection):void 
		{
			super();
			_pmsIpDosageConvList = pmsIpDosageConvList;
		}

	}
}