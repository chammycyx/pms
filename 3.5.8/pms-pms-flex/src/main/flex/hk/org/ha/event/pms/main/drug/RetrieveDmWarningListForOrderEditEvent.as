package hk.org.ha.event.pms.main.drug {
		
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDmWarningListForOrderEditEvent extends AbstractTideEvent 
	{
		private var _itemCode:String;
		
		private var _callBackFunction:Function;
		
		public function RetrieveDmWarningListForOrderEditEvent(itemCodeVal:String, callBackFunctionVal:Function=null):void 
		{
			super();
			_itemCode = itemCodeVal;
			_callBackFunction = callBackFunctionVal;
		}
		
		public function get itemCode():String
		{
			return _itemCode;
		}
		
		public function get callBackFunction():Function
		{
			return _callBackFunction;
		}
	}
}