package hk.org.ha.event.pms.main.vetting {

	import flash.events.Event;
	
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;

	public class VetOrderLineEvent extends Event {

		private var _medOrderItem:MedOrderItem;
		
		private var _vetFlag:Boolean;

		public function VetOrderLineEvent(medOrderItem:MedOrderItem, vetFlag:Boolean=true) {
			super("vetOrderLine", true);
			_medOrderItem = medOrderItem;
			_vetFlag = vetFlag;
		}
		
		public function get medOrderItem():MedOrderItem {
			return _medOrderItem;
		}
		
		public function get vetFlag():Boolean {
			return _vetFlag;
		}
	}
}