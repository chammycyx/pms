package hk.org.ha.event.pms.main.vetting.show {

	import flash.events.Event;
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.disp.MedOrder;
	
	public class ShowPrevPrescViewEvent extends AbstractTideEvent {

		private var _prevMedOrder:MedOrder;
		
		private var _initFlag:Boolean;
		
		public function ShowPrevPrescViewEvent(prevMedOrder:MedOrder, initFlag:Boolean=true) {
			super();
			_prevMedOrder = prevMedOrder;
			_initFlag = initFlag;
		}
		
		public function get prevMedOrder():MedOrder {
			return _prevMedOrder;
		}
		
		public function get initFlag():Boolean {
			return _initFlag;
		}
		
		public override function clone():Event {
			return new ShowPrevPrescViewEvent(prevMedOrder, false);
		}
	}
}
