package hk.org.ha.event.pms.main.vetting.mp
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CalculatePivasDueDateAndSupplyDateEvent extends AbstractTideEvent
	{
		private var _dailyFreqCode:String;
		
		private var _dueDate:Date;
		
		private var _callbackFunc:Function;
		
		public function CalculatePivasDueDateAndSupplyDateEvent(dailyFreqCode:String, dueDate:Date, callbackFunc:Function)
		{
			super();
			_dailyFreqCode = dailyFreqCode;
			_dueDate = dueDate;
			_callbackFunc = callbackFunc;
		}
		
		public function get dailyFreqCode():String
		{
			return _dailyFreqCode;
		}
		
		public function get dueDate():Date
		{
			return _dueDate;
		}
		
		public function get callbackFunc():Function
		{
			return _callbackFunc;
		}
	}
}