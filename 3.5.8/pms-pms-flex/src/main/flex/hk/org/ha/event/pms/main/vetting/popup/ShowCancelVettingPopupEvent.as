package hk.org.ha.event.pms.main.vetting.popup {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowCancelVettingPopupEvent extends AbstractTideEvent {

		private var _allowUnsuspendFlag:Boolean;
		
		private var _callback:Function;
		
		public function ShowCancelVettingPopupEvent(allowUnsuspendFlag:Boolean, callback:Function) {
			super();
			_allowUnsuspendFlag = allowUnsuspendFlag;
			_callback = callback;
		}
		
		public function get allowUnsuspendFlag():Boolean {
			return _allowUnsuspendFlag;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}
