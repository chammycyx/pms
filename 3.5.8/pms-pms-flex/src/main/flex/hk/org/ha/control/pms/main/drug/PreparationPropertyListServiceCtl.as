package hk.org.ha.control.pms.main.drug {		
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.drug.RetrievePreparationPropertyListEvent;
	import hk.org.ha.model.pms.biz.drug.PreparationPropertyListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("preparationPropertyListServiceCtl", restrict="true")]
	public class PreparationPropertyListServiceCtl 
	{
		[In]
		public var preparationPropertyListService:PreparationPropertyListServiceBean;
		
		[In]
		public var preparationPropertyList:ArrayCollection;
		
		[In]
		public var ctx:Context;
		
		private var callbackEvent:Event;
		
		private var callbackFunc:Function;
		
		[Observer]
		public function RetrievePreparationPropertyList(evt:RetrievePreparationPropertyListEvent):void
		{
			callbackEvent = evt.callbackEvent;
			callbackFunc = evt.callbackFunc;
			ctx.preparationPropertyList = null;
			preparationPropertyListService.retrievePreparationPropertyList(evt.preparationSearchCriteria, RetrievePreparationPropertyListResult);
		}
		
		private function RetrievePreparationPropertyListResult(evt:TideResultEvent):void 
		{
			if ( callbackEvent != null ) {
				evt.context.dispatchEvent( callbackEvent );				
			}
			if ( callbackFunc != null ) {
				callbackFunc.call(null,preparationPropertyList);
			}
		}
	}
}
