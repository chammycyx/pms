package hk.org.ha.event.pms.main.reftable.popup {
	
	import hk.org.ha.event.pms.main.reftable.popup.MpDosageConversionMaintPopupProp;
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowMpDosageConversionMaintPopupEvent extends AbstractTideEvent 
	{
		private var _mpDosageConversionMaintPopupProp:MpDosageConversionMaintPopupProp;
		
		public function ShowMpDosageConversionMaintPopupEvent(mpDosageConversionMaintPopupProp:MpDosageConversionMaintPopupProp):void 
		{
			super();
			_mpDosageConversionMaintPopupProp = mpDosageConversionMaintPopupProp;
		}
		
		public function get mpDosageConversionMaintPopupProp():MpDosageConversionMaintPopupProp{
			return _mpDosageConversionMaintPopupProp;
		}

	}
}