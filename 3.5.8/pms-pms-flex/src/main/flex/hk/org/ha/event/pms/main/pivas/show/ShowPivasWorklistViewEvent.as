package hk.org.ha.event.pms.main.pivas.show
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowPivasWorklistViewEvent extends AbstractTideEvent
	{
		private var _reloadDueOrderTab:Boolean = false;

		private var _clearMessages:Boolean;
		
		public function ShowPivasWorklistViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}

		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}