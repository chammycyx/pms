package hk.org.ha.event.pms.main.reftable.popup
{
	import mx.collections.ArrayCollection;

	public class DueOrderScheduleTemplateMaintWardCodeLookupPopupProp
	{
		public var wardList:ArrayCollection;
		public var okHandler:Function;
		public var cancelHandler:Function;
	}
}