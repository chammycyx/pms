package hk.org.ha.event.pms.main.drug {
		
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDmWarningForOrderEditEvent extends AbstractTideEvent 
	{
		private var _warnCode:String;
		private var _warnIndex:Number
		private var _resultFunc:Function;
		
		public function RetrieveDmWarningForOrderEditEvent(warnCode:String, warnIndex:Number, resultFunc:Function=null):void 
		{
			super();
			_warnCode 	= warnCode;
			_warnIndex 	= warnIndex;
			_resultFunc = resultFunc;
		}
		
		public function get warnCode():String
		{
			return _warnCode;
		}
		
		public function get warnIndex():Number
		{
			return _warnIndex;
		}
		
		public function get resultFunc():Function
		{
			return _resultFunc;
		}
	}
}