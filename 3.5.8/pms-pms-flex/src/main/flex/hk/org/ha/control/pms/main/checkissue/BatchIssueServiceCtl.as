package hk.org.ha.control.pms.main.checkissue{
	
	import hk.org.ha.event.pms.main.checkissue.DeleteBatchIssueLogEvent;
	import hk.org.ha.event.pms.main.checkissue.PrintBatchIssueRptEvent;
	import hk.org.ha.event.pms.main.checkissue.RefreshBatchIssueViewEvent;
	import hk.org.ha.event.pms.main.checkissue.RetrieveBatchIssueDetailEvent;
	import hk.org.ha.event.pms.main.checkissue.SetBatchIssueSystemMessageEvent;
	import hk.org.ha.event.pms.main.checkissue.UpdateBatchIssueLogDetailEvent;
	import hk.org.ha.event.pms.main.checkissue.UpdateBatchIssueLogWorkstationEvent;
	import hk.org.ha.event.pms.main.checkissue.popup.ShowBatchIssueExceptionListPopupEvent;
	import hk.org.ha.model.pms.biz.checkissue.BatchIssueServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("batchIssueServiceCtl", restrict="true")]
	public class BatchIssueServiceCtl 
	{
		[In]
		public var batchIssueService:BatchIssueServiceBean;
		
		private var action:String;
		private var popMsg:Boolean;
		
		[Observer]
		public function retrieveBatchIssueDetail(evt:RetrieveBatchIssueDetailEvent):void
		{
			batchIssueService.retrieveBatchIssueDetail(evt.issueDate, evt.workstore, retrieveBatchIssueDetailResult);
		}
		
		private function retrieveBatchIssueDetailResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshBatchIssueViewEvent());
		}
		
		[Observer]
		public function updateBatchIssueLogDetail(evt:UpdateBatchIssueLogDetailEvent):void
		{
			In(Object(batchIssueService).showExceptionList)
			In(Object(batchIssueService).result)
			In(Object(batchIssueService).errMsgCode)
			In(Object(batchIssueService).workstationCode)
			batchIssueService.updateBatchIssueLogDetail(evt.updateBatchIssueDetail, updateBatchIssueLogDetailResult);
		}
		
		private function updateBatchIssueLogDetailResult(evt:TideResultEvent):void
		{
			if (!Object(batchIssueService).result && Object(batchIssueService).errMsgCode != ""){
				evt.context.dispatchEvent(new SetBatchIssueSystemMessageEvent(Object(batchIssueService).errMsgCode, Object(batchIssueService).workstationCode));
			} else {
				evt.context.dispatchEvent(new RefreshBatchIssueViewEvent(false));
				if(Object(batchIssueService).showExceptionList){
					evt.context.dispatchEvent(new ShowBatchIssueExceptionListPopupEvent());
				}
			} 
		}
		
		[Observer]
		public function deleteBatchIssueLog(evt:DeleteBatchIssueLogEvent):void
		{
			popMsg = evt.popMsg;
			
			In(Object(batchIssueService).result)
			In(Object(batchIssueService).errMsgCode)
			In(Object(batchIssueService).workstationCode)
			batchIssueService.deleteBatchIssueLog(evt.issueDate, evt.workstore, deleteBatchIssueLogResult);
		}
		
		private function deleteBatchIssueLogResult(evt:TideResultEvent):void
		{
			if ( popMsg ) {
				if (!Object(batchIssueService).result && Object(batchIssueService).errMsgCode != ""){
					evt.context.dispatchEvent(new SetBatchIssueSystemMessageEvent(Object(batchIssueService).errMsgCode, Object(batchIssueService).workstationCode));
				}
			}
		}
		
		[Observer]
		public function updateBatchIssueLogWorkstation(evt:UpdateBatchIssueLogWorkstationEvent):void
		{
			if (evt.action == 'update'){
				batchIssueService.updateBatchIssueLogWorkstation(evt.issueDate, evt.workstore, evt.updateBatchIssueDetail, evt.action, updateBatchIssueLogDetailResult);
			} else {
				batchIssueService.updateBatchIssueLogWorkstation(evt.issueDate, evt.workstore, evt.updateBatchIssueDetail, evt.action, retrieveBatchIssueDetailResult);
			}
		}
		
		[Observer]
		public function printBatchIssueRpt(evt:PrintBatchIssueRptEvent):void
		{
			batchIssueService.printBatchIssueRpt(evt.rptDocType);
		}
		
	}
}
