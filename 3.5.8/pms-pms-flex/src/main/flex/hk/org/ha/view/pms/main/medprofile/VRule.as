package hk.org.ha.view.pms.main.medprofile
{
	import mx.controls.VRule;
	
	public class VRule extends mx.controls.VRule
	{
		public function VRule()
		{
			super();
		}
		
		override protected function measure():void
		{
			super.measure();
			measuredHeight = 1;
		}
	}
}