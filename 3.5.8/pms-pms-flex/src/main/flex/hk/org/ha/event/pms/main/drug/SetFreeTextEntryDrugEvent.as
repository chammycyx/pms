package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dms.persistence.DmRegimen;
	import hk.org.ha.model.pms.dms.vo.DrugCommonDosage;
	import hk.org.ha.model.pms.dms.vo.DrugName;
	import hk.org.ha.model.pms.dms.vo.PreparationProperty;
	import hk.org.ha.model.pms.dms.vo.RouteForm;
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	import hk.org.ha.model.pms.udt.vetting.ActionStatus;
	import hk.org.ha.model.pms.udt.vetting.RegimenDurationUnit;
	import hk.org.ha.model.pms.vo.rx.RxDrug;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetFreeTextEntryDrugEvent extends AbstractTideEvent 
	{
		private var _drugSearchSource:DrugSearchSource;
		private var _rxDrug:RxDrug;
		private var _carryDuration:Number = Number.NaN;
		private var _carryDurationUnit:RegimenDurationUnit = null;
		private var _callbackEvent:Event = null;

		public function SetFreeTextEntryDrugEvent(drugSearchSource:DrugSearchSource, rxDrugValue:RxDrug, eventValue:Event):void 
		{
			super();
			_drugSearchSource = drugSearchSource;
			_rxDrug = rxDrugValue;			
			_callbackEvent = eventValue;
		}

		public function get drugSearchSource():DrugSearchSource
		{
			return _drugSearchSource;
		}
		
		public function get rxDrug():RxDrug
		{
			return _rxDrug;
		}
		
		public function set carryDuration(value:Number):void
		{
			_carryDuration = value;
		}
		
		public function get carryDuration():Number
		{
			return _carryDuration;
		}
		
		public function set carryDurationUnit(value:RegimenDurationUnit):void
		{
			_carryDurationUnit = value;
		}
		
		public function get carryDurationUnit():RegimenDurationUnit
		{
			return _carryDurationUnit;
		}				
		
		public function get callbackEvent():Event
		{
			return _callbackEvent;
		}		
	}
}