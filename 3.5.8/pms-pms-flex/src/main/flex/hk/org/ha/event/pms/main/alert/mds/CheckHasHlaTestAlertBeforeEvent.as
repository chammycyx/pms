package hk.org.ha.event.pms.main.alert.mds {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class CheckHasHlaTestAlertBeforeEvent extends AbstractTideEvent {
		
		private var _hkid:String;
		
		private var _patHospCode:String;
		
		private var _caseNum:String;
		
		private var _prescribeFunc:Function;
		
		private var _notPrescribeFunc:Function;
		
		public function CheckHasHlaTestAlertBeforeEvent(hkid:String, patHospCode:String, caseNum:String, prescribeFunc:Function, notPrescribeFunc:Function) {
			super();
			_hkid             = hkid;
			_patHospCode      = patHospCode;
			_caseNum          = caseNum;
			_prescribeFunc    = prescribeFunc;
			_notPrescribeFunc = notPrescribeFunc;
		}
		
		public function get hkid():String {
			return _hkid;
		}
		
		public function get patHospCode():String {
			return _patHospCode;
		}
		
		public function get caseNum():String {
			return _caseNum;
		}
		
		public function get prescribeFunc():Function {
			return _prescribeFunc;
		}
		
		public function get notPrescribeFunc():Function {
			return _notPrescribeFunc;
		}
	}
}
