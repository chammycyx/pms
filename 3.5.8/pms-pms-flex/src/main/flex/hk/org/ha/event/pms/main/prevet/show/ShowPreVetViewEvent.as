package hk.org.ha.event.pms.main.prevet.show
{
	import hk.org.ha.model.pms.udt.prevet.PreVetMode;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowPreVetViewEvent extends AbstractTideEvent
	{
		private var _preVetMode:PreVetMode;
		private var _orderId:Number;
		private var _medOrderVersion:Number;
		
		public function ShowPreVetViewEvent(preVetMode:PreVetMode=null, orderId:Number=NaN, medOrderVersion:Number=NaN)
		{
			super();
			_preVetMode = preVetMode;		
			_orderId = orderId;
			_medOrderVersion = medOrderVersion;
		}
		
		public function get preVetMode():PreVetMode
		{
			return _preVetMode;
		}
		
		public function get orderId():Number
		{
			return _orderId;
		}
		
		public function get medOrderVersion():Number
		{
			return _medOrderVersion;
		}
	}
}