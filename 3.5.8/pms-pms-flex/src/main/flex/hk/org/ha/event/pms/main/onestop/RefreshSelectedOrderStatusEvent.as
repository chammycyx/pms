package hk.org.ha.event.pms.main.onestop
{
	import flash.events.Event;
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;

	public class RefreshSelectedOrderStatusEvent extends AbstractTideEvent
	{
		private var _oneStopOrderType:OneStopOrderType;
		private var _orderId:Number;
		private var _msgCode:String;
		private var _refillId:Number;
		
		public function RefreshSelectedOrderStatusEvent(oneStopOrderType:OneStopOrderType, medOrderId:Number, refillId:Number, msgCode:String=null)
		{
			super();
			_oneStopOrderType = oneStopOrderType;
			_orderId = medOrderId;
			_refillId = refillId;
			_msgCode = msgCode;
		}
		
		public function get oneStopOrderType():OneStopOrderType {
			return _oneStopOrderType;
		}
		
		public function get orderId():Number {
			return _orderId;
		}
		
		public function get refillId():Number {
			return _refillId;
		}
		
		public function get msgCode():String {
			return _msgCode;
		}		
	}
}