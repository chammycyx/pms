package hk.org.ha.event.pms.security.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowLogonReasonPopupEvent extends AbstractTideEvent 
	{
		
		public function ShowLogonReasonPopupEvent():void 
		{
			super();
		}
	}
}