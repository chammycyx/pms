package hk.org.ha.event.pms.main.vetting.mp
{
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class EditItemEvent extends AbstractTideEvent
	{
		private var _medProfileItem:MedProfileItem;
		private var _medProfileMoItem:MedProfileMoItem;
		
		public function EditItemEvent(medProfileItem:MedProfileItem, medProfileMoItem:MedProfileMoItem = null)
		{
			super();
			_medProfileItem = medProfileItem;
			_medProfileMoItem = medProfileMoItem;
		}
		
		public function get medProfileItem():MedProfileItem
		{
			return _medProfileItem;
		}
		
		public function get medProfileMoItem():MedProfileMoItem
		{
			return _medProfileMoItem;
		}
	}
}