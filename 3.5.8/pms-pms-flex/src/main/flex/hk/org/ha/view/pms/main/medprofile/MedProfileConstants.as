package hk.org.ha.view.pms.main.medprofile
{
	import hk.org.ha.model.pms.udt.vetting.ActionStatus;

	public class MedProfileConstants
	{
		public static const TOOLTIP_MDS_ALERT:String = "MDS Alert";
		public static const TOOLTIP_FM_INDICATION:String = "Formulary Management Indication";
		public static const TOOLTIP_DISPENSE_BY_PHARMACY:String = ActionStatus.DispByPharm.displayValue;
		public static const TOOLTIP_PURCHASE_BY_PATIENT:String = ActionStatus.PurchaseByPatient.displayValue;
		public static const TOOLTIP_CONTINUE_WITH_OWN_STOCK:String = "Continue with Own Stock";
		public static const TOOLTIP_DISPENSE_IN_CLINIC:String = ActionStatus.DispInClinic.displayValue;
		public static const TOOLTIP_SAFETY_NET:String = ActionStatus.SafetyNet.displayValue;
		public static const TOOLTIP_CHARGEABLE_ITEM:String = "Chargeable Item";
		public static const TOOLTIP_SINGLE_USE:String = "Single Use";
		public static const TOOLTIP_FIX_PERIOD:String = "Fixed Period";
		public static const TOOLTIP_DANGER_DRUG:String = "Dangerous Drug";
		public static const TOOLTIP_SPECIALTY_RESTRICTED_ITEM:String = "Drug by specialty";
		public static const TOOLTIP_WARD_STOCK:String = "Ward Stock";
		public static const TOOLTIP_HLA:String = "HLA Test";
		public static const TOOLTIP_FIXED_CONCN:String = "Fixed Concentration";
		public static const TOOLTIP_REDISPENSE:String = "Redispense";
		public static const TOOLTIP_VERBAL:String = "Verbal Order";
		public static const TOOLTIP_ITEM_SUSPENDED:String = "Item Suspended";
		public static const TOOLTIP_MDS_EXCEPTION:String = "MDS Exception";
		public static const TOOLTIP_UNIT_DOSE:String = "Unit Dose";
		public static const TOOLTIP_UNIT_DOSE_EXCEPTION:String = "Unit Dose Exception";
		public static const TOOLTIP_DIRECT_LABEL_PRINT:String = "Direct Label Print";
		public static const TOOLTIP_VERIFIED:String = "Verified"; 
		public static const TOOLTIP_SINGLE_DISPENSE:String = "Single Dispense";
		public static const TOOLTIP_MODIFIED_ORDER:String = "Modified Order";
		public static const TOOLTIP_SUSPENDED:String = "Suspended";
		public static const TOOLTIP_PENDING:String = "Pending";
		public static const TOOLTIP_URGENT:String = "Urgent";
		public static const TOOLTIP_PIVAS_NEW_ORDER:String = "New Order";
		public static const TOOLTIP_PIVAS_SATELLITE_ORDER:String = "Satellite Order";
		public static const TOOLTIP_PIVAS_EDITED_ORDER:String = "Edited Order";
		public static const TOOLTIP_PIVAS:String = "PIVAS";
		
		public static const PROP_NAME_MED_PROFILE_ENABLED:String = "medProfile.enabled";
		public static const PROP_NAME_CHEMO_ENABLED:String = "cmm.enabled";
		
		public static const PERMISSION_ITD_SUPPORT:String = "itdSupport";
		
		public static const ITEM_CODE_NOT01:String = "NOT 01";

		public static const DAILY_FREQ_CODE_AT_ONCE:String = "00034";
		public static const DAILY_FREQ_CODE_ONCE:String = "00035";
		public static const DAILY_FREQ_CODE_STAT:String = "00036";
		public static const DAILY_FREQ_AS_DIRECTED:String = "00037";
		
	}
}