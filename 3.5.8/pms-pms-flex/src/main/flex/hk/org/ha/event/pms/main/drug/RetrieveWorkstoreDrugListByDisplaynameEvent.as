package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dms.persistence.DmDrug;
	import hk.org.ha.view.pms.main.vetting.popup.OrderEditItemPopup;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveWorkstoreDrugListByDisplaynameEvent extends AbstractTideEvent
	{
		private var _displayName:String;			
		private var _orderEditItemPopup:OrderEditItemPopup;
		
		public function RetrieveWorkstoreDrugListByDisplaynameEvent(displayName:String, orderEditItemPopup:OrderEditItemPopup):void 
		{
			super();
			_displayName = displayName;			
			_orderEditItemPopup = orderEditItemPopup;
		}
		
		public function get displayName():String
		{
			return _displayName;
		}
		
		public function get orderEditItemPopup():OrderEditItemPopup
		{
			return _orderEditItemPopup;
		}
	}
}