package hk.org.ha.control.pms.main.drug {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.drug.RetrieveDmFormVerbMappingListBySiteCodeEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveFormVerbPropertiesEvent;
	import hk.org.ha.model.pms.biz.drug.DmFormVerbMappingListServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("dmFormVerbMappingListServiceCtl", restrict="true")]
	public class DmFormVerbMappingListServiceCtl 
	{
		[In]
		public var dmFormVerbMappingListService:DmFormVerbMappingListServiceBean;
		
		[In]
		public var ctx:Context;
		
		private var event:Event;
		
		private var callBackFunc:Function;
		
		[Observer]
		public function retrieveDmFormVerbMappingList(evt:RetrieveDmFormVerbMappingListBySiteCodeEvent):void
		{
			event = evt.callBackEvent;
			ctx.dmFormVerbMappingList = null;
			dmFormVerbMappingListService.retrieveDmFormVerbMappingListBySiteCode(evt.siteCode, refreshDmFormListResult);
		}
		
		private function refreshDmFormListResult(evt:TideResultEvent):void 
		{
			if ( event!=null ) {
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function retrieveFormVerbMapProperties(evt:RetrieveFormVerbPropertiesEvent):void
		{
			callBackFunc = evt.callBackFunc;
			dmFormVerbMappingListService.getFormVerbProperties(retrieveFormVerbMapPropertiesResult);
		}
		
		private function retrieveFormVerbMapPropertiesResult(evt:TideResultEvent):void 
		{
			if ( callBackFunc!=null ) {				
				callBackFunc(evt.result);
			}
		}
	}
}
