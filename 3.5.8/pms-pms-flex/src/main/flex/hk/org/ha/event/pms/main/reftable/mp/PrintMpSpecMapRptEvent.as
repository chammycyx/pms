package hk.org.ha.event.pms.main.reftable.mp {
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class PrintMpSpecMapRptEvent extends AbstractTideEvent 
	{	
		private var _mpSpecMapRptList:ArrayCollection;
		public function PrintMpSpecMapRptEvent(mpSpecMapRptList:ArrayCollection):void 
		{
			super();
			_mpSpecMapRptList = mpSpecMapRptList;
		}
		
		public function get mpSpecMapRptList():ArrayCollection {
			return _mpSpecMapRptList;
		}
	}
}