package hk.org.ha.event.pms.main.vetting.mp
{
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SelectManualProfileActiveListEvent extends AbstractTideEvent
	{
		private var _medProfileItem:MedProfileItem;
		private var _selectNextItemFlag:Boolean;
		private var _callBack:Function;		
		
		public function SelectManualProfileActiveListEvent(mpi:MedProfileItem,selectNextItem:Boolean=false,callBackFunc:Function=null)
		{
			super();
			_medProfileItem = mpi;
			_selectNextItemFlag = selectNextItem;
			_callBack = callBackFunc;
		}
		
		public function get medProfileItem():MedProfileItem
		{
			return _medProfileItem;
		}
		
		public function get selectNextItemFlag():Boolean
		{
			return _selectNextItemFlag;
		}
		
		public function get callBack():Function
		{
			return _callBack;
		}
	}
}