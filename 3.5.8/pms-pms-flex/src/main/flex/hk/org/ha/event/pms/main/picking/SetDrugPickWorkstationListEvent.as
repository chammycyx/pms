package hk.org.ha.event.pms.main.picking
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetDrugPickWorkstationListEvent extends AbstractTideEvent 
	{		
		private var _pickWorkstationList:ArrayCollection;
		
		public function SetDrugPickWorkstationListEvent(pickWorkstationList:ArrayCollection):void 
		{
			super();
			_pickWorkstationList = pickWorkstationList;
		}
		
		public function get pickWorkstationList():ArrayCollection {
			return _pickWorkstationList;
		}
		
	}
}