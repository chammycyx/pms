package hk.org.ha.control.pms.main.reftable
{
	import hk.org.ha.event.pms.main.reftable.RetrieveWardAndWardGroupListEvent;
	import hk.org.ha.event.pms.main.reftable.ReceiveWardAndWardGroupListEvent;
	import hk.org.ha.model.pms.biz.reftable.WardSelectionServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;

	[Bindable]
	[Name("wardSelectionServiceCtl", restrict="true")]
	public class WardSelectionServiceCtl
	{
		[In]
		public var ctx:Context;
		
		[In]
		public var wardSelectionService:WardSelectionServiceBean; 
		
		public function WardSelectionServiceCtl()
		{
		}
		
		[Observer]
		public function retrieveWardAndWardGroupList(evt:RetrieveWardAndWardGroupListEvent):void
		{
			wardSelectionService.retrieveWardAndWardGroupList(retrieveWardAndWardGroupListResult);
		}
		
		public function retrieveWardAndWardGroupListResult(evt:TideResultEvent):void
		{
			dispatchEvent(new ReceiveWardAndWardGroupListEvent());
		}
	}
}