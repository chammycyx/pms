package hk.org.ha.event.pms.main.vetting.mp.tpn.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class NewTpnRecordEvent extends AbstractTideEvent
	{	
		private var _duration:Number;
		
		private var _callback:Function;
		
		public function NewTpnRecordEvent(duration:Number, callback:Function):void {
			super();
			_duration = duration;
			_callback = callback;
		}
		
		public function get duration():Number {
			return _duration;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}