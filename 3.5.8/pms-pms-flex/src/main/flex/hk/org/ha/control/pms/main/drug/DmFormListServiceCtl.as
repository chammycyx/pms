package hk.org.ha.control.pms.main.drug {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.drug.RetrieveDmFormListEvent;
	import hk.org.ha.model.pms.biz.drug.DmFormListServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("dmFormListServiceCtl", restrict="true")]
	public class DmFormListServiceCtl 
	{
		[In]
		public var dmFormListService:DmFormListServiceBean;
		
		private var event:Event;
		
		[Observer]
		public function retrieveDmFormList(evt:RetrieveDmFormListEvent):void
		{
			event = evt.event;
			dmFormListService.retrieveDmFormList(evt.formCode, refreshDmFormListResult);
		}
		
		private function refreshDmFormListResult(evt:TideResultEvent):void 
		{
			if ( event!=null ) {
				evt.context.dispatchEvent( event );
			}
		}
	}
}
