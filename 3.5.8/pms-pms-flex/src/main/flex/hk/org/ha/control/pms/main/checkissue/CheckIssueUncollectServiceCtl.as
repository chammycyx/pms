package hk.org.ha.control.pms.main.checkissue{
	
	import hk.org.ha.event.pms.main.checkissue.RefreshUncollectPrescListEvent;
	import hk.org.ha.event.pms.main.checkissue.UncollectPrescEvent;
	import hk.org.ha.model.pms.biz.checkissue.CheckIssueUncollectServiceBean;
	import hk.org.ha.model.pms.vo.checkissue.CheckIssueUncollectSummary;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("checkIssueUncollectServiceCtl", restrict="true")]
	public class CheckIssueUncollectServiceCtl 
	{
		[In]
		public var checkIssueUncollectService:CheckIssueUncollectServiceBean;
		
		[Observer]
		public function uncollectPrescEvent(evt:UncollectPrescEvent):void
		{
			checkIssueUncollectService.uncollectPrescription(evt.ticketDate, evt.ticketNum, evt.issueWindowNum, uncollectPrescEventResult);
		}
		
		private function uncollectPrescEventResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent( new RefreshUncollectPrescListEvent(evt.result as CheckIssueUncollectSummary) );
		}
	}
}
