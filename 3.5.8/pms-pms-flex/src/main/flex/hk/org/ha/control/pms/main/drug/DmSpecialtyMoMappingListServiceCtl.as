package hk.org.ha.control.pms.main.reftable {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.drug.RetrieveDmSpecialtyMoMappingListEvent;
	import hk.org.ha.model.pms.biz.drug.DmSpecialtyMoMappingListServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("dmSpecialtyMoMappingListServiceCtl", restrict="true")]
	public class DmSpecialtyMoMappingListServiceCtl 
	{
		[In]
		public var dmSpecialtyMoMappingListService:DmSpecialtyMoMappingListServiceBean;
		
		private var event:Event;
		
		[Observer]
		public function retrieveDmSpecialtyMoMappingList(evt:RetrieveDmSpecialtyMoMappingListEvent):void
		{
			event = evt.event;
			dmSpecialtyMoMappingListService.retrieveDmSpecialtyMoMappingList(evt.specialtyCode, refreshDmSpecialtyMoMappingListResult);
		}
		
		private function refreshDmSpecialtyMoMappingListResult(evt:TideResultEvent):void 
		{
			if ( event!=null ) {
				evt.context.dispatchEvent( event );
			}
		}
	}
}
