package hk.org.ha.event.pms.main.delivery
{
	import hk.org.ha.model.pms.udt.medprofile.RetrievePurchaseRequestOption;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePurchaseRequestListEvent extends AbstractTideEvent
	{
		private var _option:RetrievePurchaseRequestOption;
		private var _callbackFunction:Function;
		
		public function RetrievePurchaseRequestListEvent(option:RetrievePurchaseRequestOption, callbackFunction:Function)
		{
			super();
			_option = option;
			_callbackFunction = callbackFunction;
		}
		
		public function get option():RetrievePurchaseRequestOption
		{
			return _option;
		}
		
		public function get callbackFunction():Function
		{
			return _callbackFunction;
		}
	}
}