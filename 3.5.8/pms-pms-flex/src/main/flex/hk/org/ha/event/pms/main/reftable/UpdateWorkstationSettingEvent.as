package hk.org.ha.event.pms.main.reftable {
	
	import hk.org.ha.model.pms.persistence.reftable.Workstation;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateWorkstationSettingEvent extends AbstractTideEvent 
	{
		private var _workstation:Workstation;
		private var _addWorkstationFlag:Boolean;
		
		public function UpdateWorkstationSettingEvent(workstation:Workstation, addWorkstationFlag:Boolean):void 
		{
			super();
			_workstation = workstation;
			_addWorkstationFlag = addWorkstationFlag;;
		}
		
		public function get workstation():Workstation {
			return _workstation;
		}
		
		public function get addWorkstationFlag():Boolean {
			return _addWorkstationFlag;
		}

	}
}