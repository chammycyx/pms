package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dms.vo.PreparationSearchCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePreparationPropertyListEvent extends AbstractTideEvent 
	{
		private var _preparationSearchCriteria:PreparationSearchCriteria;
		private var _callbackEvent:Event;
		private var _callbackFunc:Function;
		
		public function RetrievePreparationPropertyListEvent(criteria:PreparationSearchCriteria, eventValue:Event=null, funcValue:Function=null):void 
		{
			super();
			_preparationSearchCriteria = criteria;
			_callbackEvent = eventValue;
			_callbackFunc = funcValue;
		}
		
		public function get preparationSearchCriteria():PreparationSearchCriteria
		{
			return _preparationSearchCriteria;
		}
		
		public function get callbackEvent():Event
		{
			return _callbackEvent;
		}
		
		public function get callbackFunc():Function
		{
			return _callbackFunc;
		}
	}
}