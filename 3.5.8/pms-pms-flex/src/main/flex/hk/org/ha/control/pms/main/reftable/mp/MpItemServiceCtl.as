package hk.org.ha.control.pms.main.reftable.mp {
	
	import hk.org.ha.event.pms.main.reftable.mp.RefreshMpFdnMappingListRptEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RefreshMpItemEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RefreshMpItemPropertyListRptEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RefreshMpItemSpecListRptEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveMpFdnMappingListRptEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveMpItemEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveMpItemPropertyListRptEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveMpItemSpecListRptEvent;
	import hk.org.ha.event.pms.main.reftable.mp.UpdateMpItemEvent;
	import hk.org.ha.model.pms.biz.reftable.mp.MpItemRptServiceBean;
	import hk.org.ha.model.pms.biz.reftable.mp.MpItemServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("mpItemServiceCtl", restrict="true")]
	public class MpItemServiceCtl 
	{
		[In]
		public var mpItemService:MpItemServiceBean;
		
		[In]
		public var mpItemRptService:MpItemRptServiceBean;
		
		private var _itemCode:String;
		
		[Observer]
		public function retrieveMpItemPropertyListRpt(evt:RetrieveMpItemPropertyListRptEvent):void
		{
			mpItemRptService.genMpItemPropertyListRpt(retrieveMpItemPropertyListRptEventResult);
		}
		
		private function retrieveMpItemPropertyListRptEventResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new RefreshMpItemPropertyListRptEvent);
		}
		
		[Observer]
		public function retrieveMpItemSpecListRpt(evt:RetrieveMpItemSpecListRptEvent):void
		{
			mpItemRptService.genMpItemSpecListRpt(retrieveMpItemSpecListRptEventResult);
		}
		
		private function retrieveMpItemSpecListRptEventResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new RefreshMpItemSpecListRptEvent);
		}
		
		[Observer]
		public function retrieveMpFdnMappingListRpt(evt:RetrieveMpFdnMappingListRptEvent):void
		{
			mpItemRptService.genMpFdnMappingListRpt(retrieveMpFdnMappingListRptEventResult);
		}
		
		private function retrieveMpFdnMappingListRptEventResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new RefreshMpFdnMappingListRptEvent);
		}
		
		[Observer]
		public function retrieveMpItem(evt:RetrieveMpItemEvent):void
		{
			_itemCode = evt.itemCode;
			mpItemService.retrieveMpItem(evt.itemCode, retrieveMpItemResult);
		}
		
		private function retrieveMpItemResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new RefreshMpItemEvent('Retrieve'));
		}
		
		[Observer]
		public function updateMpItem(evt:UpdateMpItemEvent):void
		{
			mpItemService.updateMpItem(evt.fdnMappingList, evt.itemSpecialtyList, evt.itemDispConfig,
				evt.itemCode, updateMpItemResult);
		}
		
		private function updateMpItemResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new RefreshMpItemEvent('Save'));
		}
	}
}
