package hk.org.ha.event.pms.main.reftable.mp 
{
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class MpWorkingStorePropListSaveSuccessfulEvent extends AbstractTideEvent 
	{
		public function MpWorkingStorePropListSaveSuccessfulEvent():void 
		{
			super();
		}
	}
}