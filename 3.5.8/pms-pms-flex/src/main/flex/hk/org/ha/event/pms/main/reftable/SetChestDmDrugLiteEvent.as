package hk.org.ha.event.pms.main.reftable {
	
	import hk.org.ha.model.pms.vo.drug.DmDrugLite;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetChestDmDrugLiteEvent extends AbstractTideEvent 
	{
		private var _chestDmDrugLite:DmDrugLite;
		private var _editItemIndex:Number;
		private var _operationResultName:String;
		
		public function SetChestDmDrugLiteEvent(chestDmDrugLite:DmDrugLite):void 
		{
			super();
			_chestDmDrugLite = chestDmDrugLite;
		}
		
		public function get chestDmDrugLite():DmDrugLite {
			return  _chestDmDrugLite;
		}
	}
}