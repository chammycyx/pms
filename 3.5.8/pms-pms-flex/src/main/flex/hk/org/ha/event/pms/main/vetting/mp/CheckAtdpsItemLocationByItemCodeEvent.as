package hk.org.ha.event.pms.main.vetting.mp {
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CheckAtdpsItemLocationByItemCodeEvent extends AbstractTideEvent 
	{
		private var _itemCode:String;
		private var _pickStationNumList:ListCollectionView;
		private var _callBack:Function;
		
		public function CheckAtdpsItemLocationByItemCodeEvent(itemCode:String, pickStationNumList:ListCollectionView, callBack:Function):void 
		{
			super();
			_itemCode = itemCode;
			_pickStationNumList = pickStationNumList;
			_callBack = callBack;
		}
		
		public function get itemCode():String{
			return _itemCode;
		}
		
		public function get pickStationNumList():ListCollectionView{
			return _pickStationNumList;
		}
		
		public function get callBack():Function
		{
			return _callBack;
		}
	}
}