package hk.org.ha.event.pms.main.cddh {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RenderCddhDashBoardMessageEvent extends AbstractTideEvent 
	{
		private var _messageCode:String = "";
		public function RenderCddhDashBoardMessageEvent(messageCode:String):void 
		{
			super();
			_messageCode = messageCode;
		}
		
		public function get messageCode():String
		{
			return _messageCode;
		}
	}
}
