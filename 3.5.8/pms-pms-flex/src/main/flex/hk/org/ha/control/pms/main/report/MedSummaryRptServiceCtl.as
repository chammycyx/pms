package hk.org.ha.control.pms.main.report {
	import hk.org.ha.event.pms.main.report.PrintMedSummaryRptEvent;
	import hk.org.ha.model.pms.biz.report.MedSummaryRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	
	[Bindable]
	[Name("medSummaryRptServiceCtl", restrict="true")]
	public class MedSummaryRptServiceCtl 
	{
		[In]
		public var medSummaryRptService:MedSummaryRptServiceBean;
		
		[Observer]
		public function printMedSummaryRpt(evt:PrintMedSummaryRptEvent):void
		{
			In(Object(medSummaryRptService).retrieveSuccess)
			medSummaryRptService.printMedSummaryRpt(evt.dispDate, evt.ticketNum, evt.printLang);
		}
	}
}
