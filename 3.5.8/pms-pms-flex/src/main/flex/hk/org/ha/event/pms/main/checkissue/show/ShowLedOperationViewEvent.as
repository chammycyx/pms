package hk.org.ha.event.pms.main.checkissue.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowLedOperationViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		private var _backtoCheckIssueButtonEnable:Boolean;
		
		public function ShowLedOperationViewEvent(clearMessages:Boolean=true, backtoCheckIssueButtonEnable:Boolean=false):void 
		{
			super();
			_clearMessages = clearMessages;
			_backtoCheckIssueButtonEnable = backtoCheckIssueButtonEnable;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
		
		public function get backtoCheckIssueButtonEnable():Boolean {
			return _backtoCheckIssueButtonEnable;
		}
	}
}