package hk.org.ha.control.pms.main.reftable {
	
	import hk.org.ha.event.pms.main.reftable.ClearItemAvailabilityMaintDrugScopeViewEvent;
	import hk.org.ha.event.pms.main.reftable.ClearItemAvailabilityMaintViewEvent;
	import hk.org.ha.event.pms.main.reftable.RefreshItemAvailabilityMaintViewEvent;
	import hk.org.ha.event.pms.main.reftable.RetrieveMsWorkstoreDrugListEvent;
	import hk.org.ha.event.pms.main.reftable.UpdateMsWorkstoreDrugListEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.ShowLoadingPopupEvent;
	import hk.org.ha.model.pms.biz.reftable.MsWorkstoreDrugListServiceBean;
	
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("msWorkstoreDrugListServiceCtl", restrict="true")]
	public class MsWorkstoreDrugListServiceCtl 
	{
		[In]
		public var msWorkstoreDrugListService:MsWorkstoreDrugListServiceBean;
		
		private var _itemCode:String;
		
		[Observer]
		public function retrieveMsWorkstoreDrugList(evt:RetrieveMsWorkstoreDrugListEvent):void
		{
			msWorkstoreDrugListService.retrieveMsWorkstoreDrugList(evt.itemCode, retrieveMsWorkstoreDrugListResult);
		}
		
		private function retrieveMsWorkstoreDrugListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshItemAvailabilityMaintViewEvent());
		}
		
		[Observer]
		public function updateMsWorkstoreDrugList(evt:UpdateMsWorkstoreDrugListEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Saving ..."));
			_itemCode = evt.itemCode;
			if (evt.editDrugScope) {
				msWorkstoreDrugListService.updateMsWorkstoreDrugList(evt.updateMsWorkstoreDrugList, evt.remark, updateMsWorkstoreDrugListDrugScopeResult, updateMsWorkstoreDrugListFault);
			}else {
				msWorkstoreDrugListService.updateMsWorkstoreDrugList(evt.updateMsWorkstoreDrugList, evt.remark, updateMsWorkstoreDrugListResult, updateMsWorkstoreDrugListFault);
			}
			
		}
		
		private function updateMsWorkstoreDrugListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			evt.context.dispatchEvent(new ClearItemAvailabilityMaintViewEvent());
		}
		
		private function updateMsWorkstoreDrugListDrugScopeResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			evt.context.dispatchEvent(new ClearItemAvailabilityMaintDrugScopeViewEvent());
		}
		
		private function updateMsWorkstoreDrugListFault(evt:TideFaultEvent):void
		{
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
		}
	}
}
