package hk.org.ha.event.pms.main.alert.druginfo.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowMonographPopupEvent extends AbstractTideEvent{
		
		private var _patHospCode:String;
		
		private var _monoId:Number;
		
		private var _monoType:Number;
		
		public function ShowMonographPopupEvent(patHospCode:String, monoId:Number, monoType:Number=2) {
			super();
			_patHospCode = patHospCode;
			_monoId = monoId;
			_monoType = monoType;
		}

		public function get patHospCode():String {
			return _patHospCode;
		}
		
		public function get monoId():Number {
			return _monoId;
		}
		
		public function get monoType():Number {
			return _monoType;
		}
	}
}