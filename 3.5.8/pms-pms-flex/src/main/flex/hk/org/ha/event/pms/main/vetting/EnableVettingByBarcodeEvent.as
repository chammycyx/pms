package hk.org.ha.event.pms.main.vetting {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class EnableVettingByBarcodeEvent extends AbstractTideEvent {
		
		private var _enabled:Boolean;
		
		public function EnableVettingByBarcodeEvent(enabled:Boolean) {
			super();
			_enabled = enabled;
		}
		
		public function get enabled():Boolean {
			return _enabled;
		}
	}
}
