package hk.org.ha.event.pms.main.lookup.popup
{
	public class SuspendPrescPopupProp
	{
		public var okHandler:Function;
		public var cancelHandler:Function;
		public var ticketDate:Date;
		public var orderNum:String;
		public var ticketNum:String;
		public var caller:String;
	}
}