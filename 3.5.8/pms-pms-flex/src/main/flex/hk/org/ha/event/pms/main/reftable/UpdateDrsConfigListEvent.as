package hk.org.ha.event.pms.main.reftable {
	
	import mx.collections.ListCollectionView;
	import mx.core.UIComponent;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateDrsConfigListEvent extends AbstractTideEvent 
	{		
		private var _updateDrsConfigList:ListCollectionView;
		private var _delDrsConfigList:ListCollectionView;
		private var _nextTabIndex:Number;
		private var _nextSubTabIndex:Number;
		private var _showRefillModuleMaintAlertMsg:Boolean;
		private var _caller:UIComponent;
		
		public function UpdateDrsConfigListEvent(updateDrsConfigList:ListCollectionView, 
													   delDrsConfigList:ListCollectionView, 
													   nextTabIndex:Number, 
													   nextSubTabIndex:Number,
													   showRefillModuleMaintAlertMsg:Boolean, 
													   caller:UIComponent):void {
			super();
			_updateDrsConfigList = updateDrsConfigList;
			_delDrsConfigList = delDrsConfigList;
			_nextTabIndex = nextTabIndex;
			_nextSubTabIndex = nextSubTabIndex;
			_showRefillModuleMaintAlertMsg = showRefillModuleMaintAlertMsg;
			_caller = caller;
		}
		
		public function get updateDrsConfigList():ListCollectionView {
			return _updateDrsConfigList;
		}
		
		public function get delDrsConfigList():ListCollectionView {
			return _delDrsConfigList;
		}
		
		public function get nextTabIndex():Number {
			return _nextTabIndex;
		}
		
		public function get nextSubTabIndex():Number {
			return _nextSubTabIndex;
		}
		
		public function get showRefillModuleMaintAlertMsg():Boolean {
			return _showRefillModuleMaintAlertMsg;
		}
		
		public function get caller():UIComponent {
			return _caller;
		}
	}
}