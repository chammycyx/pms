package hk.org.ha.event.pms.main.reftable.rdr
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ReceiveSuspendReasonRdrListEvent extends AbstractTideEvent
	{
		public function ReceiveSuspendReasonRdrListEvent()
		{
			super();
		}
	}
}