package hk.org.ha.event.pms.main.reftable.mp {
	
	import hk.org.ha.model.pms.persistence.reftable.WardGroup;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RemoveWardGroupEvent extends AbstractTideEvent 
	{		
		private var _wardGroup:WardGroup;
		
		public function RemoveWardGroupEvent(wardGroup:WardGroup):void 
		{
			super();
			_wardGroup = wardGroup;
		}
		
		public function get wardGroup():WardGroup {
			return _wardGroup;
		}
	}
}