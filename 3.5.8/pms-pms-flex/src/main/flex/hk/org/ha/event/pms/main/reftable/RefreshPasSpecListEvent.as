package hk.org.ha.event.pms.main.reftable {
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshPasSpecListEvent extends AbstractTideEvent 
	{
		private var _dataProvider:ArrayCollection;
		
		public function RefreshPasSpecListEvent(dataProvider:ArrayCollection):void 
		{
			super();
			_dataProvider = dataProvider;
		}
		
		public function get dataProvider():ArrayCollection 
		{
			return _dataProvider;
		}

	}
}