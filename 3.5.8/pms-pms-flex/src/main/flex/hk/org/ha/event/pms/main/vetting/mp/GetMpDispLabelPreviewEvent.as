package hk.org.ha.event.pms.main.vetting.mp {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
	
	public class GetMpDispLabelPreviewEvent extends AbstractTideEvent {											
		
		private var _medProfileItem:MedProfileItem;
		private var _medProfileMoItem:MedProfileMoItem;
		
		public function GetMpDispLabelPreviewEvent(medProfileItemVal:MedProfileItem, medProfileMoItemVal:MedProfileMoItem) {
			super();			
			_medProfileItem = medProfileItemVal;
			_medProfileMoItem = medProfileMoItemVal;
		}
		
		public function get medProfileItem():MedProfileItem {
			return _medProfileItem;
		}
		
		public function get medProfileMoItem():MedProfileMoItem {
			return _medProfileMoItem;
		}
	}
}