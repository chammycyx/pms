package hk.org.ha.event.pms.main.cddh {
	
	import org.granite.tide.events.AbstractTideEvent;
	import mx.collections.ArrayCollection;
	
	public class PreviewCddhDispLabelEvent extends AbstractTideEvent 
	{
		private var _dispOrderItemKey:ArrayCollection;

		public function PreviewCddhDispLabelEvent(dispOrderItemKey:ArrayCollection):void {
			super();
			_dispOrderItemKey = dispOrderItemKey;	
		}
		
		public function get dispOrderItemKey():ArrayCollection
		{
			return _dispOrderItemKey;
		}

	}
}