package hk.org.ha.event.pms.main.delivery.show
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowBatchPrintViewEvent extends AbstractTideEvent
	{
		public function ShowBatchPrintViewEvent()
		{
			super();
		}
	}
}