package hk.org.ha.event.pms.main.checkissue.mp.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDrugCheckViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		public function ShowDrugCheckViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}