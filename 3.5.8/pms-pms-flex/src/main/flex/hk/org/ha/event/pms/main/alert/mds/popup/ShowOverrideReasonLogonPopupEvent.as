package hk.org.ha.event.pms.main.alert.mds.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowOverrideReasonLogonPopupEvent extends AbstractTideEvent {
		
		private var _callbackFunc:Function;
		
		public function ShowOverrideReasonLogonPopupEvent(callbackFunc:Function=null) {
			super();
			_callbackFunc = callbackFunc;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}
