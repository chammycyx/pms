package hk.org.ha.event.pms.main.reftable {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveMsWorkstoreDrugListEvent extends AbstractTideEvent 
	{
		private var _itemCode:String;
		
		public function RetrieveMsWorkstoreDrugListEvent(itemCode:String):void 
		{
			super();
			_itemCode = itemCode;
		}
		
		public function get itemCode():String
		{
			return _itemCode;
		}

	}
}