package hk.org.ha.control.pms.main.report {
	import hk.org.ha.event.pms.main.report.ExportManualPrescRptEvent;
	import hk.org.ha.event.pms.main.report.RetrieveManualPrescListEvent;
	import hk.org.ha.model.pms.biz.report.ManualPrescRptServiceBean;
	import hk.org.ha.model.pms.udt.report.ManualPrescRptType;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	
	[Bindable]
	[Name("manualPrescRptServiceCtl", restrict="true")]
	public class ManualPrescRptServiceCtl 
	{
		[In]
		public var manualPrescRptService:ManualPrescRptServiceBean;
		
		private var manualPrescRptType:ManualPrescRptType;
		
		[Observer]
		public function retrieveManualPrescList(evt:RetrieveManualPrescListEvent):void
		{
			manualPrescRptType = evt.manualPrescRptType;
			
			if ( manualPrescRptType == ManualPrescRptType.DetailManualPresc ) {
				In(Object(manualPrescRptService).retrieveSuccess)
				manualPrescRptService.retrieveManualPrescRptList(evt.dateFrom, evt.password, evt.exportCurrentMonth , retrieveManualPrescSummaryListResult);
			} else {
				In(Object(manualPrescRptService).retrieveSuccess)
				manualPrescRptService.retrieveManualPrescSummaryList(evt.dateFrom, evt.password, evt.exportCurrentMonth, retrieveManualPrescSummaryListResult);
			}
		}
		
		private function retrieveManualPrescSummaryListResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new ExportManualPrescRptEvent(Object(manualPrescRptService).retrieveSuccess, manualPrescRptType));
		}
		
	}
}
