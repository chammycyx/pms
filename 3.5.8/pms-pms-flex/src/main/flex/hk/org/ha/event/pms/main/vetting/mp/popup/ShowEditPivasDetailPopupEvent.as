package hk.org.ha.event.pms.main.vetting.mp.popup
{
	import hk.org.ha.model.pms.vo.medprofile.VettingEditItemViewInfo;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowEditPivasDetailPopupEvent extends AbstractTideEvent
	{
		
		private var _vettingEditItemViewInfo:VettingEditItemViewInfo;
		
		private var _hideResumeDispFlag:Boolean;

		private var _fromPivasWorklistFlag:Boolean;
		
		public function ShowEditPivasDetailPopupEvent(vettingEditItemViewInfoIn:VettingEditItemViewInfo, hideResumeDispFlag:Boolean=false, fromPivasWorklistFlag:Boolean=false)
		{
			super();
			_vettingEditItemViewInfo = vettingEditItemViewInfoIn;
			_hideResumeDispFlag = hideResumeDispFlag;
			_fromPivasWorklistFlag = fromPivasWorklistFlag;
		}
		
		public function get vettingEditItemViewInfo():VettingEditItemViewInfo
		{
			return _vettingEditItemViewInfo;
		}
		
		public function get hideResumeDispFlag():Boolean
		{
			return _hideResumeDispFlag;
		}
		
		public function get fromPivasWorklistFlag():Boolean
		{
			return _fromPivasWorklistFlag;
		}
	}
}