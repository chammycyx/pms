package hk.org.ha.event.pms.main.inbox
{
	import hk.org.ha.model.pms.udt.medprofile.DischargeMedOrderListType;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateDischargeTypesAndFiltersEvent extends AbstractTideEvent
	{
		private var _listType:DischargeMedOrderListType;
		private var _wardFilterList:ArrayCollection;
		private var _wardGroupFilterList:ArrayCollection;
		private var _hkidCaseNum:String;
		
		public function UpdateDischargeTypesAndFiltersEvent(listType:DischargeMedOrderListType, wardFilterList:ArrayCollection,
															wardGroupFilterList:ArrayCollection, hkidCaseNum:String)
		{
			super();
			_listType = listType;
			_wardFilterList = wardFilterList;
			_wardGroupFilterList = wardGroupFilterList;
			_hkidCaseNum = hkidCaseNum;
		}
		
		public function get listType():DischargeMedOrderListType
		{
			return _listType;
		}
		
		public function get wardFilterList():ArrayCollection
		{
			return _wardFilterList;
		}
		
		public function get wardGroupFilterList():ArrayCollection
		{
			return _wardGroupFilterList;
		}
		
		public function get hkidCaseNum():String
		{
			return _hkidCaseNum;
		}
	}
}