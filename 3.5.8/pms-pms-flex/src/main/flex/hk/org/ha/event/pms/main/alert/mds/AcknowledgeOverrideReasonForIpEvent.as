package hk.org.ha.event.pms.main.alert.mds {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class AcknowledgeOverrideReasonForIpEvent extends AbstractTideEvent {
		
		private var _callbackFunc:Function;
		
		public function AcknowledgeOverrideReasonForIpEvent(callbackFunc:Function=null) {
			super();
			_callbackFunc = callbackFunc;
		}
		
		public function constructCopy():AcknowledgeOverrideReasonForIpEvent {
			return new AcknowledgeOverrideReasonForIpEvent(callbackFunc);
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}