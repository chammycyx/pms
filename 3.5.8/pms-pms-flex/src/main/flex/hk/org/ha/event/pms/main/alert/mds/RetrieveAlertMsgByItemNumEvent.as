package hk.org.ha.event.pms.main.alert.mds {
	
	import hk.org.ha.event.pms.main.alert.mds.popup.ShowAlertMsgPopupEvent;
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrieveAlertMsgByItemNumEvent extends AbstractTideEvent {

		private var _itemNum:Number;
		
		private var _showAlertMsgPopupEvent:ShowAlertMsgPopupEvent;
		
		private var _allowEditFlag:Boolean;
		
		public function RetrieveAlertMsgByItemNumEvent(itemNum:Number=NaN, showAlertMsgPopupEvent:ShowAlertMsgPopupEvent=null, allowEditFlag:Boolean=true) {
			super();
			_itemNum = itemNum;
			_showAlertMsgPopupEvent = showAlertMsgPopupEvent;
			_allowEditFlag = allowEditFlag;
		}
		
		public function get itemNum():Number {
			return _itemNum;
		}
		
		public function get showAlertMsgPopupEvent():ShowAlertMsgPopupEvent {
			return _showAlertMsgPopupEvent;
		}
		
		public function get allowEditFlag():Boolean {
			return _allowEditFlag; 
		}
	}
}