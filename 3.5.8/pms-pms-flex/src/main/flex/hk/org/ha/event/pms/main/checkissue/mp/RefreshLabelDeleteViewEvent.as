package hk.org.ha.event.pms.main.checkissue.mp
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshLabelDeleteViewEvent extends AbstractTideEvent 
	{			
		private var _msgCode:String;
		
		public function RefreshLabelDeleteViewEvent(msgCode:String):void 
		{
			super();
			_msgCode = msgCode;
		}
		
		public function get msgCode():String {
			return _msgCode;
		}
	}
}