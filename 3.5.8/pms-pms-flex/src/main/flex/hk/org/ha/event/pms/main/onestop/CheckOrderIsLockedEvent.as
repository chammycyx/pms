package hk.org.ha.event.pms.main.onestop {
		
	import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
	
	import org.granite.tide.events.AbstractTideEvent;
		
	public class CheckOrderIsLockedEvent extends AbstractTideEvent 
	{
		private var _orderId:Number;
		private var _medOrderVersion:Number;
		private var _refillOrderId:Number;
		private var _requiredUnlock:Boolean;
		private var _followUpFunc:Function;
		private var _followUpFuncAfterFollowUpFunc:Function;
		
		public function CheckOrderIsLockedEvent(orderId:Number, medOrderVersion:Number, refillOrderId:Number, requiredUnlock:Boolean,
												followUpFunc:Function, followUpFuncAfterFollowUpFunc:Function=null):void 
		{
			super();
			_orderId = orderId;
			_medOrderVersion = medOrderVersion;
			_refillOrderId = refillOrderId;
			_requiredUnlock = requiredUnlock;
			_followUpFunc = followUpFunc;
			_followUpFuncAfterFollowUpFunc = followUpFuncAfterFollowUpFunc;
		}
		
		public function get orderId():Number
		{
			return _orderId;
		}
		
		public function get medOrderVersion():Number
		{
			return _medOrderVersion;
		}
		
		public function get refillOrderId():Number
		{
			return _refillOrderId;
		}
		
		public function get requiredUnlock():Boolean
		{
			return _requiredUnlock;
		}
		
		public function get followUpFunc():Function
		{
			return _followUpFunc;
		}
		
		public function get followUpFuncAfterFollowUpFunc():Function
		{
			return _followUpFuncAfterFollowUpFunc;
		}
	}
}