package hk.org.ha.event.pms.main.reftable {
	
	import hk.org.ha.model.pms.persistence.reftable.OperationWorkstation;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class EditOperationWorkstationProfileItemEvent extends AbstractTideEvent 
	{
		private var _editOperationWorkstation:OperationWorkstation;
		private var _removeRedirectedPrinter:Boolean;
		private var _removeRedirectedFastQueuePrinter:Boolean;
		
		public function EditOperationWorkstationProfileItemEvent(editOperationWorkstation:OperationWorkstation, removeRedirectedPrinter:Boolean, removeRedirectedFastQueuePrinter:Boolean):void 
		{
			super();
			_editOperationWorkstation = editOperationWorkstation;
			_removeRedirectedPrinter = removeRedirectedPrinter;
			_removeRedirectedFastQueuePrinter = removeRedirectedFastQueuePrinter;
		}

		public function get editOperationWorkstation():OperationWorkstation {
			return _editOperationWorkstation;
		}
		
		public function get removeRedirectedPrinter():Boolean {
			return _removeRedirectedPrinter;
		}
		
		public function get removeRedirectedFastQueuePrinter():Boolean {
			return _removeRedirectedFastQueuePrinter;
		}
	}
}