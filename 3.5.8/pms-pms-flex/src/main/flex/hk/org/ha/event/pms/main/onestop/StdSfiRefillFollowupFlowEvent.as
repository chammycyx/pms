package hk.org.ha.event.pms.main.onestop {
		
	import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
	
	import org.granite.tide.events.AbstractTideEvent;
		
	public class StdSfiRefillFollowupFlowEvent extends AbstractTideEvent 
	{		
		private var _oneStopOrderType:OneStopOrderType;
				
		public function StdSfiRefillFollowupFlowEvent(oneStopOrderType:OneStopOrderType):void 
		{
			super();
			_oneStopOrderType = oneStopOrderType;
		}
		
		public function get oneStopOrderType():OneStopOrderType
		{
			return _oneStopOrderType;
		}
	}
}