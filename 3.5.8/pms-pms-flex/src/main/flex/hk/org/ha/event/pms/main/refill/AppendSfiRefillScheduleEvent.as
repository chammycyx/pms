package hk.org.ha.event.pms.main.refill {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class AppendSfiRefillScheduleEvent extends AbstractTideEvent 
	{		
		private var _refillCouponNum:String;
		
		public function AppendSfiRefillScheduleEvent(refillCouponNum:String):void 
		{
			super();
			_refillCouponNum = refillCouponNum;
		}
		
		public function get refillCouponNum():String {
			return _refillCouponNum;
		}
	}
}