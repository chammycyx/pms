package hk.org.ha.event.pms.main.assembling
{
	import hk.org.ha.model.pms.vo.assembling.AssembleItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshDrugAssembleViewEvent extends AbstractTideEvent 
	{	
		private var _updateSuccess:Boolean;
		private var _errMsg:String;
		private var _assembleItem:AssembleItem;
		
		public function RefreshDrugAssembleViewEvent(updateSuccess:Boolean, errMsg:String, assembleItem:AssembleItem):void 
		{
			super();
			_updateSuccess = updateSuccess;
			_errMsg = errMsg;
			_assembleItem = assembleItem;
		}
		
		public function get updateSuccess():Boolean {
			return _updateSuccess;
		}
		
		public function get errMsg():String {
			return _errMsg;
		}
		
		public function get assembleItem():AssembleItem {
			return _assembleItem;
		}
	}
}