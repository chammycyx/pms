package hk.org.ha.control.pms.main.reftable.mp {
	
	import hk.org.ha.event.pms.main.reftable.mp.CloseAddWardGroupPopupEvent;
	import hk.org.ha.event.pms.main.reftable.mp.CreateWardGroupEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RefreshWardGroupMaintViewEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RemoveWardGroupEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveWardGroupListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.ShowWardGroupMaintSaveSuccessMsgEvent;
	import hk.org.ha.event.pms.main.reftable.mp.ShowWardGroupPopupSysMsgEvent;
	import hk.org.ha.event.pms.main.reftable.mp.UpdateWardGroupEvent;
	import hk.org.ha.model.pms.biz.reftable.mp.WardGroupListServiceBean;
	import hk.org.ha.model.pms.vo.reftable.mp.WardGroupInfo;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("wardGroupListServiceCtl", restrict="true")]
	public class WardGroupListServiceCtl 
	{
		[In]
		public var wardGroupListService:WardGroupListServiceBean;
		
		[Observer]
		public function retrieveWardGroupList(evt:RetrieveWardGroupListEvent):void
		{
			wardGroupListService.retrieveWardGroupList(retrieveWardGroupListResult);
		}
		
		private function retrieveWardGroupListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent( new RefreshWardGroupMaintViewEvent(evt.result as WardGroupInfo) );
		}
		
		[Observer]
		public function createWardGroup(evt:CreateWardGroupEvent):void
		{
			In(Object(wardGroupListService).saveSuccess)
			In(Object(wardGroupListService).errCode)
			wardGroupListService.createWardGroup(evt.wardGroupCode, createWardGroupResult);
		}
		
		private function createWardGroupResult(evt:TideResultEvent):void{
			if ( Object(wardGroupListService).saveSuccess ) {
				evt.context.dispatchEvent( new CloseAddWardGroupPopupEvent());
				evt.context.dispatchEvent( new RetrieveWardGroupListEvent());
			} else {
				evt.context.dispatchEvent( new ShowWardGroupPopupSysMsgEvent(Object(wardGroupListService).errCode) );
			}
		}
		
		[Observer]
		public function removeWardGroup(evt:RemoveWardGroupEvent):void
		{
			wardGroupListService.removeWardGroup(evt.wardGroup, retrieveWardGroupListResult);
		}
		
		[Observer]
		public function updateWardGroup(evt:UpdateWardGroupEvent):void
		{
			wardGroupListService.updateWardGroup(evt.wardGroup, evt.selectedWardList, updateWardGroupResult);
		}
		
		private function updateWardGroupResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent( new ShowWardGroupMaintSaveSuccessMsgEvent() );
		}
		
	}
}
