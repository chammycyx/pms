package hk.org.ha.control.pms.main.report {
	import hk.org.ha.event.pms.main.report.ExportPivasRptEvent;
	import hk.org.ha.event.pms.main.report.PrintPivasRptEvent;
	import hk.org.ha.event.pms.main.report.RefreshExportPivasRptEvent;
	import hk.org.ha.event.pms.main.report.RefreshPivasRptEvent;
	import hk.org.ha.event.pms.main.report.RetrievePivasRptEvent;
	import hk.org.ha.model.pms.biz.report.PivasRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("pivasRptServiceCtl", restrict="true")]
	public class PivasRptServiceCtl 
	{
		[In]
		public var pivasRptService:PivasRptServiceBean;
		
		[Observer]
		public function retrievePivasRptEvent(evt:RetrievePivasRptEvent):void
		{
			In(Object(pivasRptService).retrieveSuccess);
			pivasRptService.retrievePivasRpt(evt.reportType, evt.supplyFromDate, evt.supplyToDate, 
											evt.satelliteFlag, evt.hkidCaseNum, evt.pivasRptDispMethod, evt.pivasRptItemCatFilterOption, 
											evt.itemCode, evt.drugKeyFlag, evt.manufCode, evt.batchNum, 
											retrievePivasRptResult);
		}
		
		private function retrievePivasRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshPivasRptEvent(Object(pivasRptService).retrieveSuccess));
		}
		
		[Observer]
		public function printPivasRpt(evt:PrintPivasRptEvent):void
		{
			pivasRptService.printPivasRpt();
		}
		
		[Observer]
		public function exportPivasRpt(evt:ExportPivasRptEvent):void
		{
			pivasRptService.setPivasRptPassword(evt.password, exportPivasRptResult);
		}
		
		private function exportPivasRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshExportPivasRptEvent(true) );
		}
		
	}
}
