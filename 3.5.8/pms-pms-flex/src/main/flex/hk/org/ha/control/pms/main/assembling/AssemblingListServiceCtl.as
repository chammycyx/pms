package hk.org.ha.control.pms.main.assembling{
	
	import hk.org.ha.event.pms.main.assembling.RetrieveAssemblingListEvent;
	import hk.org.ha.event.pms.main.assembling.SetAssemblingListEvent;
	import hk.org.ha.model.pms.biz.assembling.AssemblingListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("assemblingListServiceCtl", restrict="true")]
	public class AssemblingListServiceCtl 
	{
		[In]
		public var assemblingListService:AssemblingListServiceBean;
		
		[Observer]
		public function retrieveAssemblingList(evt:RetrieveAssemblingListEvent):void
		{
			assemblingListService.retrieveAssembleOrderList(retrieveAssemblingListResult);
		}
		
		private function retrieveAssemblingListResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new SetAssemblingListEvent(evt.result as ArrayCollection));
		}
	}
}
