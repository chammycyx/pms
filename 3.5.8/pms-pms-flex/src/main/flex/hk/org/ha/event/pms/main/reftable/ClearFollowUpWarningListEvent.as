package hk.org.ha.event.pms.main.reftable {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ClearFollowUpWarningListEvent extends AbstractTideEvent 
	{		
		public function ClearFollowUpWarningListEvent():void 
		{
			super();
		}
	}
}