package hk.org.ha.event.pms.main.drug {
	
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ResumeAddDrugLoadingEvent extends AbstractTideEvent 
	{			
		private var _drugSearchSource:DrugSearchSource;
		
		public function ResumeAddDrugLoadingEvent(drugSearchSource:DrugSearchSource):void 
		{
			super();
			_drugSearchSource = drugSearchSource;
		}
		
		public function get drugSearchSource():DrugSearchSource {
			return _drugSearchSource;
		}
	}
}