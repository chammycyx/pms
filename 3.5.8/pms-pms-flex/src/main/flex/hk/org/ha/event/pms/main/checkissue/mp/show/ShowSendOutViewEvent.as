package hk.org.ha.event.pms.main.checkissue.mp.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowSendOutViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		public function ShowSendOutViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}