package hk.org.ha.event.pms.main.pivas
{
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.model.pms.udt.pivas.PivasWorklistType;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class AddToWorklistEvent extends AbstractTideEvent 
	{		
		private var _pivasWorklistList:ArrayCollection;

		private var _sourceType:PivasWorklistType;
		
		private var _callbackFunc:Function;
		
		public function AddToWorklistEvent(pivasWorklistList:ArrayCollection, sourceType:PivasWorklistType, callbackFunc:Function):void 
		{
			super();
			_pivasWorklistList = pivasWorklistList;
			_sourceType = sourceType;
			_callbackFunc = callbackFunc;
		}
		
		public function get pivasWorklistList():ArrayCollection {
			return _pivasWorklistList;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
		
		public function get sourceType():PivasWorklistType {
			return _sourceType;
		}
	}
}