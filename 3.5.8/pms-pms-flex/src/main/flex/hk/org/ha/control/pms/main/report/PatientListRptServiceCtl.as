package hk.org.ha.control.pms.main.report {
	import hk.org.ha.event.pms.main.report.PrintPatientListRptEvent;
	import hk.org.ha.event.pms.main.report.RefreshPatientListRptEvent;
	import hk.org.ha.event.pms.main.report.RetrievePatientListRptEvent;
	import hk.org.ha.event.pms.main.report.RetrievePatientListRptInfoEvent;
	import hk.org.ha.event.pms.main.report.SetPatientListRptInfoEvent;
	import hk.org.ha.model.pms.biz.report.PatientListRptServiceBean;
	import hk.org.ha.model.pms.vo.report.PatientListRptInfo;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	
	[Bindable]
	[Name("patientListRptServiceCtl", restrict="true")]
	public class PatientListRptServiceCtl 
	{
		[In]
		public var patientListRptService:PatientListRptServiceBean;
		
		[Observer]
		public function retrievePatientListRptInfo(evt:RetrievePatientListRptInfoEvent):void
		{
			patientListRptService.retrievePatientListRptInfo(retrievePatientListRptInfoResult);
		}
		
		private function retrievePatientListRptInfoResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new SetPatientListRptInfoEvent(evt.result as PatientListRptInfo));
		}
		
		[Observer]
		public function retrievePatientListRpt(evt:RetrievePatientListRptEvent):void
		{
			In(Object(patientListRptService).retrieveSuccess)
			patientListRptService.retrievePatientList(evt.wardCodeList, retrievePatientListRptResult);
		}
		
		private function retrievePatientListRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshPatientListRptEvent(Object(patientListRptService).retrieveSuccess));
		}
		
		[Observer]
		public function printPatientListRpt(evt:PrintPatientListRptEvent):void
		{
			patientListRptService.printPatientListRpt();
		}
		
	}
}
