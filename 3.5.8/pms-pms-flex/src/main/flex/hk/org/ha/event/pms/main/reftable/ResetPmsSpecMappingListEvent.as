package hk.org.ha.event.pms.main.reftable {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ResetPmsSpecMappingListEvent extends AbstractTideEvent 
	{
		
		private var _isDeletePmsSpecMapping:Boolean;
		
		public function ResetPmsSpecMappingListEvent(isDeletePmsSpecMapping:Boolean):void 
		{
			super();
			_isDeletePmsSpecMapping = isDeletePmsSpecMapping;
		}
		
		public function get isDeletePmsSpecMapping():Boolean {
			return _isDeletePmsSpecMapping;
		}

	}
}