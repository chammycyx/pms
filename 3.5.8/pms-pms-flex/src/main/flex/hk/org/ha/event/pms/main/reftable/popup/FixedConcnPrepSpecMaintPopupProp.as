package hk.org.ha.event.pms.main.reftable.popup
{
	import hk.org.ha.model.pms.dms.persistence.PmsFixedConcnStatus;
	
	import mx.collections.ArrayCollection;

	public class FixedConcnPrepSpecMaintPopupProp
	{
		public var pmsFixedConcnSpecList:ArrayCollection;
		public var okHandler:Function;
		public var cancelHandler:Function;
	}
}
