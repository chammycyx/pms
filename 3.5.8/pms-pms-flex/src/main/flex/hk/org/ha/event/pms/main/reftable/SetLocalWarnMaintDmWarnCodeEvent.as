package hk.org.ha.event.pms.main.reftable {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	import spark.components.Label;
	import spark.components.TextInput;

	
	public class SetLocalWarnMaintDmWarnCodeEvent extends AbstractTideEvent 
	{
		private var _warnCodeTxt:TextInput;
		private var _warnDescTxt:TextInput;
		private var _warnUsageType:Label;
		private var _localWarnCatCode:Label;
		
		public function SetLocalWarnMaintDmWarnCodeEvent(warnCodeTxt:TextInput, warnDescTxt:TextInput, warnUsageType:Label, localWarnCatCode:Label):void 
		{
			super();
			_warnCodeTxt = warnCodeTxt;
			_warnDescTxt = warnDescTxt;
			_warnUsageType = warnUsageType;
			_localWarnCatCode = localWarnCatCode;
		}
		
		public function get warnCodeTxt():TextInput{
			return _warnCodeTxt;
		}
		
		public function get warnDescTxt():TextInput{
			return _warnDescTxt;
		}
		
		public function get warnUsageType():Label{
			return _warnUsageType;
		}
		
		public function get localWarnCatCode():Label{
			return _localWarnCatCode;
		}
	}
}