package hk.org.ha.view.pms.main.medprofile
{
	import mx.validators.NumberValidator;
	import mx.validators.ValidationResult;
	
	public class AdvancedNumberValidator extends NumberValidator
	{
		public var customValidationFunction:Function;
		
		public var customValidationError:String;
		
		public function AdvancedNumberValidator()
		{
			super();
		}
		
		override protected function doValidation(value:Object):Array
		{
			var results:Array = super.doValidation(value);
			
			// Return if there are errors
			// or if the required property is set to <code>false</code> and length is 0.
			var val:String = value ? String(value) : "";
			if (results.length > 0 || ((val.length == 0) && !required)) {
				return results;
			}
			
			if (customValidationFunction != null && !customValidationFunction(value)) {
				results.push(new ValidationResult(true, "", "customValidation", customValidationError));
			}
			
			return results;
		}
	}
}