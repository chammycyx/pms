package hk.org.ha.control.pms.main.drug {
	
	import hk.org.ha.event.pms.main.drug.RetrieveDmDrugListByFullDrugDescEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveDmDrugListEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveDmWarningListEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveDmWarningListForOrderEditEvent;
	import hk.org.ha.model.pms.biz.drug.DmDrugListServiceBean;
	import hk.org.ha.model.pms.biz.drug.DmWarningListServiceBean;
		
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("dmWarningListServiceCtl", restrict="true")]
	public class DmWarningListServiceCtl 
	{
		[In]
		public var dmWarningListService:DmWarningListServiceBean;		
		
		[In]
		public var ctx:Context;
		
		private var callBackFunction:Function;
		
		[Observer]
		public function retrieveDmWarningList(evt:RetrieveDmWarningListEvent):void
		{			
			callBackFunction = evt.callBackFunction;
			ctx.dmWarningList = null;
			dmWarningListService.retrieveDmWarningList(evt.warnCode,retrieveDmWarningListResult);
		}
		
		private function retrieveDmWarningListResult(evt:TideResultEvent):void 
		{
			if ( callBackFunction!=null ) {
				callBackFunction.call();
			}
		}
		
		[Observer]
		public function retrieveDmWarningListForOrderEdit(evt:RetrieveDmWarningListForOrderEditEvent):void
		{
			callBackFunction = evt.callBackFunction;
			dmWarningListService.retrieveDmWarningListForOrderEdit(evt.itemCode, retrieveDmWarningListForOrderEditResult);
		}
		
		private function retrieveDmWarningListForOrderEditResult(evt:TideResultEvent):void 
		{
			if ( callBackFunction!=null ) {
				callBackFunction(evt.result as ListCollectionView);
			}
		}
		
	}
}
