package hk.org.ha.event.pms.main.reftable.cmm {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrieveCmmDrugGroupListEvent extends AbstractTideEvent {
		
		private var _prefixItemCode:String;
		private var _callback:Function;
		
		public function RetrieveCmmDrugGroupListEvent(prefixItemCode:String, callback:Function) {
			super();
			_prefixItemCode = prefixItemCode;
			_callback = callback;
		}
		
		public function get prefixItemCode():String {
			return _prefixItemCode;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}
