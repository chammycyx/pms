package hk.org.ha.event.pms.main.cddh {
	
	import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class DeleteDispOrderItemRemarkEvent extends AbstractTideEvent 
	{
		private var _dispOrderItem:DispOrderItem;
		
		public function DeleteDispOrderItemRemarkEvent(dispOrderItem:DispOrderItem):void 
		{
			super();
			_dispOrderItem = dispOrderItem;
		}
		
		public function get dispOrderItem():DispOrderItem
		{
			return _dispOrderItem;
		}
	}
}