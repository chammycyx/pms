package hk.org.ha.event.pms.main.report.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowFilePasswordPopupEvent extends AbstractTideEvent 
	{
		private var _fromView:String;
		private var _fileScope:String;
		private var _fileName:String;
		
		public function ShowFilePasswordPopupEvent(fromView:String, fileScope:String, fileName:String):void 
		{
			super();
			_fromView = fromView;
			_fileScope = fileScope;
			_fileName = fileName;
		}

		public function get fromView():String{
			return _fromView;
		} 
		
		public function get fileScope():String{
			return _fileScope;
		}
		
		public function get fileName():String{
			return _fileName;
		}
	}
}