package hk.org.ha.control.pms.security {
	import hk.org.ha.event.pms.security.AuthenticateEvent;
	import hk.org.ha.event.pms.security.SyncLockScreenSessionTimeoutEvent;
	import hk.org.ha.event.pms.security.popup.RefreshSystemIdleProtectionPopupEvent;
	import hk.org.ha.model.pms.biz.security.ScreenLockServiceBean;
	
	import mx.controls.Alert;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	import org.granite.tide.seam.In;

	[Bindable]
	[Name("screenLockServiceCtl", restrict="true")]
	public class ScreenLockServiceCtl {

		[In]
		public var screenLockService:ScreenLockServiceBean;

		[In]
		public var ctx:Context;
		
		private var syncLockScreenSessionTimeoutEvent:SyncLockScreenSessionTimeoutEvent;
		
		[Observer]
		public function authentication(evt:AuthenticateEvent):void
		{
			In(Object(screenLockService).errMsg);
			In(Object(screenLockService).sessionTimeout);
			ctx.meta_updates.removeAll();
			screenLockService.validateIdleUser(evt.userId, evt.password, authenticationResult);
		}
		
		private function authenticationResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshSystemIdleProtectionPopupEvent(Object(screenLockService).errMsg, 
				Object(screenLockService).sessionTimeout));
		}
		
		[Observer]
		public function syncLockScreenSessionTimeout(evt:SyncLockScreenSessionTimeoutEvent):void
		{
			syncLockScreenSessionTimeoutEvent = evt;
			screenLockService.syncLockScreenSessionTimeout(syncLockScreenSessionTimeoutResult);
		}
		
		private function syncLockScreenSessionTimeoutResult(evt:TideResultEvent):void
		{
			syncLockScreenSessionTimeoutEvent.systemIdleProtectionPopup.registerSessionTimeout(evt.result as int);
		}
		
	}
}
