package hk.org.ha.event.pms.main.reftable {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePmsRestrictItemSpecListByItemEvent extends AbstractTideEvent 
	{	
		private var _itemCode:String;
		private var _pmsFmStatus:String;
		
		public function RetrievePmsRestrictItemSpecListByItemEvent(itemCode:String):void 
		{
			super();
			_itemCode = itemCode;
			_pmsFmStatus = pmsFmStatus;
		}
		
		public function get itemCode():String{
			return _itemCode;
		}
		
		public function get pmsFmStatus():String{
			return _pmsFmStatus;
		}

	}
}