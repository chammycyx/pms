package hk.org.ha.event.pms.main.reftable {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveItemLocationDmDrugLiteEvent extends AbstractTideEvent 
	{
		private var _itemCode:String;
		private var _callBackEvent:Event;
		
		public function RetrieveItemLocationDmDrugLiteEvent(itemCode:String, callBackEvent:Event):void 
		{
			super();
			_itemCode = itemCode;
			_callBackEvent = callBackEvent;
		}
		
		public function get itemCode():String {
			return _itemCode;
		}
		
		public function get callBackEvent():Event {
			return _callBackEvent;
		}

	}
}