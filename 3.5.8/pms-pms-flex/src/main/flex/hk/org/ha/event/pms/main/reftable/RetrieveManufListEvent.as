package hk.org.ha.event.pms.main.reftable {		
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveManufListEvent extends AbstractTideEvent 
	{
		private var _companyCode:String;
		private var _callBackFunction:Function;
		
		public function RetrieveManufListEvent(companyCode:String=null, functionValue:Function=null):void 
		{
			super();
			_companyCode = companyCode;
			_callBackFunction = functionValue;
		}
		
		public function get companyCode():String
		{
			return _companyCode;
		}
		
		public function get callBackFunction():Function
		{
			return _callBackFunction;
		}
	}
}