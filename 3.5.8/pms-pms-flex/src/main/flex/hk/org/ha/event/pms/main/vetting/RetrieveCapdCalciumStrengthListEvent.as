package hk.org.ha.event.pms.main.vetting {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrieveCapdCalciumStrengthListEvent extends AbstractTideEvent 
	{
		private var _supplierSystem:String;
		
		public function RetrieveCapdCalciumStrengthListEvent(supplierSystem:String) {
			super();
			_supplierSystem = supplierSystem;
		}
		
		public function get supplierSystem():String {
			return _supplierSystem;
		}
	}
}
