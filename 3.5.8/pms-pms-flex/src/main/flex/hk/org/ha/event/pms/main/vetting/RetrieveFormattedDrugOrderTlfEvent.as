package hk.org.ha.event.pms.main.vetting {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.rx.RxItem;
	
	public class RetrieveFormattedDrugOrderTlfEvent extends AbstractTideEvent {

		private var _rxItem:RxItem;
		
		private var _callBackFunc:Function;
		
		public function RetrieveFormattedDrugOrderTlfEvent(rxItemIn:RxItem, funcIn:Function) 
		{
			super();			
			_rxItem = rxItemIn;
			_callBackFunc = funcIn;
		}
		
		public function get rxItem():RxItem 
		{
			return _rxItem;
		}
		
		public function get callBackFunc():Function
		{
			return _callBackFunc;
		}
	}
}