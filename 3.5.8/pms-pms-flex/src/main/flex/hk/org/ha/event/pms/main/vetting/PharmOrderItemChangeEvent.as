package hk.org.ha.event.pms.main.vetting {

	import flash.events.Event;
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
	
	public class PharmOrderItemChangeEvent extends AbstractTideEvent {		
		
		private var _pharmOrderItem:PharmOrderItem;
		private var _callBackEvent:Event;		
		
		public function PharmOrderItemChangeEvent(poiValue:PharmOrderItem, eventValue:Event)
		{
			super();			
			_pharmOrderItem = poiValue;
			_callBackEvent = eventValue;
		}		
		
		public function get pharmOrderItem():PharmOrderItem
		{
			return _pharmOrderItem;
		}
		
		public function get callBackEvent():Event
		{
			return _callBackEvent;
		}
	}
}
