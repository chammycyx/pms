package hk.org.ha.control.pms.main.prevet
{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.onestop.ShowInfomationMessageEvent;
	import hk.org.ha.event.pms.main.prevet.CheckPreVetOrderStatusEvent;
	import hk.org.ha.event.pms.main.prevet.DeleteRxFromPreVetEvent;
	import hk.org.ha.event.pms.main.prevet.PreVetAuthnicationEvent;
	import hk.org.ha.event.pms.main.prevet.PreVetAuthnicationValidateFollowUpEvent;
	import hk.org.ha.event.pms.main.prevet.PreVetCallbackScreenSwitchEvent;
	import hk.org.ha.event.pms.main.prevet.PreVetCancelEvent;
	import hk.org.ha.event.pms.main.prevet.PreVetMedOrderEvent;
	import hk.org.ha.event.pms.main.prevet.PreVetOperationCompletedFollowUpEvent;
	import hk.org.ha.event.pms.main.prevet.RetrieveDischargeMedOrderEvent;
	import hk.org.ha.event.pms.main.prevet.ShowPreVetMsgEvent;
	import hk.org.ha.event.pms.main.prevet.show.ShowPreVetViewEvent;
	import hk.org.ha.event.pms.main.vetting.popup.RefreshPharmRemarkLogonPopupEvent;
	import hk.org.ha.event.pms.main.vetting.show.ShowOrderEntryViewEvent;
	import hk.org.ha.fmk.pms.flex.components.core.MenuItem;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.ShowLoadingPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.window.WindowSwitchEvent;
	import hk.org.ha.model.pms.biz.prevet.PreVetOrderStatusCheckServiceBean;
	import hk.org.ha.model.pms.biz.prevet.PreVetServiceBean;
	import hk.org.ha.model.pms.udt.prevet.PreVetMode;
	import hk.org.ha.model.pms.vo.prevet.PreVetAuth;
	
	import org.granite.tide.events.TideResultEvent;

	[Bindable]
	[Name("preVetServiceCtl", restrict="true")]	
	public class PreVetServiceCtl
	{
		[In]
		public var preVetService:PreVetServiceBean;
		
		[In]
		public var preVetOrderStatusCheckService:PreVetOrderStatusCheckServiceBean;
		
		private var retrieveDischargeMedOrderEvent:RetrieveDischargeMedOrderEvent;
		
		private static const PRE_VET:String = "Discharge Rx Reconciliation";
		private static const ORDER_ENTRY:String = "Order Entry";
		private static const CLOSE_ON_LOGOFF:String = "closeOnLogOff";
				
		public function PreVetServiceCtl()
		{
		}
		
		[Observer]
		public function retrieveDischargeMedOrder(evt:RetrieveDischargeMedOrderEvent):void
		{
			retrieveDischargeMedOrderEvent = evt;
			if (evt.patient == null)
			{
				dispatchEvent(new ShowLoadingPopupEvent("Loading PAS Information ..."));
			}
			else
			{
				dispatchEvent(new ShowLoadingPopupEvent("Loading Discharge Order Information ..."));
			}
			
			preVetService.retrieveDischargeMedOrder(evt.orderNum, evt.patient, evt.preVetMode, evt.orderId, retrieveDischargeMedOrderResult, closePopup);
		}
		
		public function retrieveDischargeMedOrderResult(evt:TideResultEvent):void
		{
			dispatchEvent(new CloseLoadingPopupEvent());
			
			if ((evt.result as String) != null)
			{
				evt.context.dispatchEvent(new ShowInfomationMessageEvent(new Array(evt.result as String as String), true));
				return;
			}
			
			var menuItem:MenuItem;
			if (retrieveDischargeMedOrderEvent.preVetMode != PreVetMode.Vetting)
			{
				menuItem = new MenuItem(PRE_VET, ShowPreVetViewEvent);
			}
			else
			{
				menuItem = new MenuItem(ORDER_ENTRY, ShowOrderEntryViewEvent);
			}
			
			menuItem.properties[CLOSE_ON_LOGOFF] = true;
			evt.context.dispatchEvent(new WindowSwitchEvent(menuItem, null, false));
			evt.context.dispatchEvent(new ShowPreVetViewEvent(retrieveDischargeMedOrderEvent.preVetMode, 
												  retrieveDischargeMedOrderEvent.orderId, retrieveDischargeMedOrderEvent.medOrderVersion));
			
		}
		
		public function closePopup(evt:Event):void
		{
			dispatchEvent(new CloseLoadingPopupEvent());
		}
		
		[Observer]
		public function checkPreVetOrderStatus(evt:CheckPreVetOrderStatusEvent):void
		{
			var callbackFunction:Function = evt.callbackFunction;
			preVetOrderStatusCheckService.checkOrderStatus(evt.medOrder, evt.preVetMode, function(evt:TideResultEvent):void {
				if ((evt.result as String) != null)
				{
					evt.context.dispatchEvent(new ShowPreVetMsgEvent(evt.result as String));
				}
				else
				{
					callbackFunction();
				}
			}, preVetCancelFailureHandle);
		}
		
		[Observer]
		public function preVetMedOrder(evt:PreVetMedOrderEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Update Order Information ..."));
			var callbackFunction:Function = evt.callbackFunction;
			
			preVetService.preVetMedOrder(evt.medOrder, evt.preVetMode, evt.user, evt.userDisplayName, function(evt:TideResultEvent):void {
				if ((evt.result as String) != null)
				{
					evt.context.dispatchEvent(new ShowPreVetMsgEvent(evt.result as String));
				}
				
				if (callbackFunction != null && (evt.result as String) == null) {
					callbackFunction();
				}
				dispatchEvent(new CloseLoadingPopupEvent());
			}, preVetMedOrderFailureHandle);
		}
		
		public function preVetMedOrderFailureHandle(evt:Event):void
		{
			dispatchEvent(new CloseLoadingPopupEvent());
			dispatchEvent(new PreVetOperationCompletedFollowUpEvent(false));
		}	
		
		[Observer]
		public function preVetCancel(evt:PreVetCancelEvent):void
		{
			var callbackFunction:Function = evt.callbackFunction;			
			preVetService.releaseMedOrder(evt.medOrder, evt.preVetMode, function(evt:TideResultEvent):void 
			{
				callbackFunction();
			}, preVetCancelFailureHandle);
		}		
		
		public function preVetCancelFailureHandle(evt:Event):void
		{
			dispatchEvent(new PreVetOperationCompletedFollowUpEvent(false));
		}
		
		[Observer]
		public function preVetAuthnication(evt:PreVetAuthnicationEvent):void
		{
			var preVetAuthnicationEvent:PreVetAuthnicationEvent = evt;
			preVetService.preVetAuthnication(evt.userId, evt.password, evt.targetName, function(evt:TideResultEvent):void {
				var preVetAuth:PreVetAuth = evt.result as PreVetAuth;
				(preVetAuthnicationEvent.followUpEvent as PreVetAuthnicationValidateFollowUpEvent).preVetAuth = preVetAuth;
				evt.context.dispatchEvent(new RefreshPharmRemarkLogonPopupEvent(new Array(
					preVetAuth.authFailMsg), preVetAuthnicationEvent.followUpEvent));
			});
		}
		
		[Observer]
		public function deleteRxFromPreVet(evt:DeleteRxFromPreVetEvent):void
		{
			preVetService.removeOrder(evt.userId, evt.remarkText, evt.medOrder, function(evt:TideResultEvent):void {
				dispatchEvent(new PreVetOperationCompletedFollowUpEvent(true));
				dispatchEvent(new PreVetCallbackScreenSwitchEvent(PreVetMode.OneStopEntry, true));
			});
		}
	}
}