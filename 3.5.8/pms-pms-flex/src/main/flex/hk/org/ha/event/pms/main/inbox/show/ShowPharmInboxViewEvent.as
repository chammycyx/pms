package hk.org.ha.event.pms.main.inbox.show
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowPharmInboxViewEvent extends AbstractTideEvent
	{
		private var _fromVetting:Boolean;
		
		public function ShowPharmInboxViewEvent(fromVetting:Boolean = false)
		{
			super();
			_fromVetting = fromVetting;
		}
		
		public function get fromVetting():Boolean {
			return _fromVetting;
		}
	}
}