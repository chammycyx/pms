package hk.org.ha.event.pms.main.vetting.popup {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowHlaTestPopupEvent extends AbstractTideEvent {

		private var _state:String;
		
		private var _overrideReason:String;
		
		private var _prescribeCallback:Function;
		
		private var _doNotPrescribeCallback:Function;
		
		private var _okCallback:Function;
		
		public function ShowHlaTestPopupEvent(state:String="warning") {
			super();
			_state = state;
		}
		
		public function get state():String {
			return _state;
		}
		
		public function get overrideReason():String
		{
			return _overrideReason;
		}
		
		public function set overrideReason(value:String):void
		{
			_overrideReason = value;
		}
		
		public function get prescribeCallback():Function {
			return _prescribeCallback;
		}
		
		public function set prescribeCallback(prescribeCallback:Function):void {
			_prescribeCallback = prescribeCallback;
		}

		public function get doNotPrescribeCallback():Function {
			return _doNotPrescribeCallback;
		}
		
		public function set doNotPrescribeCallback(doNotPrescribeCallback:Function):void {
			_doNotPrescribeCallback = doNotPrescribeCallback;
		}

		public function get okCallback():Function {
			return _okCallback;
		}
		
		public function set okCallback(okCallback:Function):void {
			_okCallback = okCallback;
		}
	}
}