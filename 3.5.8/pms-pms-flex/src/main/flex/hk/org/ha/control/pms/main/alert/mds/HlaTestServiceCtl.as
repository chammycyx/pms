package hk.org.ha.control.pms.main.alert.mds {

	import hk.org.ha.event.pms.main.alert.mds.CheckHasHlaTestAlertBeforeEvent;
	import hk.org.ha.event.pms.main.vetting.popup.ShowHlaTestPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.ShowLoadingPopupEvent;
	import hk.org.ha.model.pms.biz.alert.mds.HlaTestServiceBean;
	import hk.org.ha.model.pms.vo.security.PropMap;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("hlaTestServiceCtl", restrict="true")]
	public class HlaTestServiceCtl {
		
		[In]
		public var ctx:Context;
		
		[In]
		public var hlaTestService:HlaTestServiceBean;
		
		[In]
		public var propMap:PropMap;
		
		private var checkHasHlaTestAlertBeforeEvent:CheckHasHlaTestAlertBeforeEvent;
		
		[Observer]
		public function checkHasHlaTestAlertBefore(evt:CheckHasHlaTestAlertBeforeEvent):void {
			checkHasHlaTestAlertBeforeEvent = evt;
			if (propMap.getValueAsBoolean("patient.pas.patient.enabled") && evt.hkid != null) {
				dispatchEvent(new ShowLoadingPopupEvent("Loading PAS information..."));
			}
			hlaTestService.hasHlaTestAlertBefore(evt.hkid, evt.patHospCode, evt.caseNum, checkHasHlaTestAlertBeforeResult);
		}
		
		private function checkHasHlaTestAlertBeforeResult(evt:TideResultEvent):void {
		
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			
			if (evt.result as Boolean) {
				checkHasHlaTestAlertBeforeEvent.prescribeFunc();
			}
			else {
				var showHlaTestPopupEvent:ShowHlaTestPopupEvent = new ShowHlaTestPopupEvent("confirm");
				showHlaTestPopupEvent.prescribeCallback = checkHasHlaTestAlertBeforeEvent.prescribeFunc;
				showHlaTestPopupEvent.doNotPrescribeCallback = checkHasHlaTestAlertBeforeEvent.notPrescribeFunc;
				evt.context.dispatchEvent(showHlaTestPopupEvent);
			}

		}
	}
}
