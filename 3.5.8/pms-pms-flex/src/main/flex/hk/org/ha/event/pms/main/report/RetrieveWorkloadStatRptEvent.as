package hk.org.ha.event.pms.main.report {
	
	import hk.org.ha.model.pms.vo.report.ItemLocationRpt;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveWorkloadStatRptEvent extends AbstractTideEvent 
	{
		private var _pastDay:Number;
		private var _dateFrom:Date;
		private var _dateTo:Date;
		
		public function RetrieveWorkloadStatRptEvent( pastDay:Number, dateFrom:Date, dateTo:Date):void 
		{
			super();
			_pastDay = pastDay;
			_dateFrom = dateFrom;
			_dateTo = dateTo;
		}
		
		public function get pastDay():Number{
			return _pastDay;
		} 
		
		public function get dateFrom():Date{
			return _dateFrom;
		} 
		
		public function get dateTo():Date{
			return _dateTo;
		}
	}
}