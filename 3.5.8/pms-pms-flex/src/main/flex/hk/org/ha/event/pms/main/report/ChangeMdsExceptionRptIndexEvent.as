package hk.org.ha.event.pms.main.report {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ChangeMdsExceptionRptIndexEvent extends AbstractTideEvent {
		
		private var _index:int;
		
		private var _callback:Function;
		
		public function ChangeMdsExceptionRptIndexEvent(index:int, callback:Function=null) {
			super();
			_index = index;
			_callback = callback;
		}
		
		public function get index():int {
			return _index;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}