package hk.org.ha.event.pms.main.drug.popup {
		
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshMpCapdDrugPopupEvent extends AbstractTideEvent 
	{			
		private var _drugSearchSource:DrugSearchSource;
		
		public function RefreshMpCapdDrugPopupEvent(drugSearchSource:DrugSearchSource):void 
		{
			super();
			_drugSearchSource = drugSearchSource;
		}
		
		public function get drugSearchSource():DrugSearchSource {
			return _drugSearchSource;
		}
	}
}