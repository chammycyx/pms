package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetCapdDrugEvent extends AbstractTideEvent 
	{			
		private var _drugSearchSource:DrugSearchSource;
		private var _callbackEvent:Event;

		public function SetCapdDrugEvent(drugSearchSource:DrugSearchSource, callbackEvent:Event):void 
		{
			super();
			_drugSearchSource = drugSearchSource;
			_callbackEvent = callbackEvent;
		}
		
		public function get drugSearchSource():DrugSearchSource
		{
			return _drugSearchSource;
		}
		
		public function get callbackEvent():Event
		{
			return _callbackEvent;
		}
	}
}