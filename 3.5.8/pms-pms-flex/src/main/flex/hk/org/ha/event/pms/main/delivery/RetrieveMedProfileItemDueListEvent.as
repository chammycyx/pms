package hk.org.ha.event.pms.main.delivery
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveMedProfileItemDueListEvent extends AbstractTideEvent
	{
		private var _medProfileId:Number;
		
		public function RetrieveMedProfileItemDueListEvent(medProfileId:Number)
		{
			_medProfileId = medProfileId;
		}
		
		public function get medProfileId():Number
		{
			return _medProfileId
		}
	}
}