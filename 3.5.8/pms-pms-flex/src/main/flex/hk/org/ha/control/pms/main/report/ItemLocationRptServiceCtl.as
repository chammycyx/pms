package hk.org.ha.control.pms.main.report {
	
	import hk.org.ha.event.pms.main.report.ClearItemLocationRptCbxListEvent;
	import hk.org.ha.event.pms.main.report.ExportItemLocationRptEvent;
	import hk.org.ha.event.pms.main.report.PrintItemLocationRptEvent;
	import hk.org.ha.event.pms.main.report.RefreshExportItemLocationRptEvent;
	import hk.org.ha.event.pms.main.report.RetrieveItemLocationRptEvent;
	import hk.org.ha.event.pms.main.report.ShowItemLocationRptEvent;
	import hk.org.ha.model.pms.biz.report.ItemLocationRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("itemLocationRptServiceCtl", restrict="true")]
	public class ItemLocationRptServiceCtl 
	{
		[In]
		public var itemLocationRptService:ItemLocationRptServiceBean;
		
		[Observer]
		public function retrieveItemLocationRpt(evt:RetrieveItemLocationRptEvent):void
		{
			itemLocationRptService.retrieveItemLocationRpt(evt.itemLocationRpt, retrieveItemLocationRptListResult);
		}
		
		private function retrieveItemLocationRptListResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new ShowItemLocationRptEvent());
		}
		
		[Observer]
		public function clearItemLocationRptCbxList(evt:ClearItemLocationRptCbxListEvent):void
		{
			itemLocationRptService.clearCbxList();
		}
		
		[Observer]
		public function printItemLocationRpt(evt:PrintItemLocationRptEvent):void
		{
			itemLocationRptService.printItemLocationRpt(evt.printItemLocationRptList);
		}
		
		[Observer]
		public function exportItemLocationRpt(evt:ExportItemLocationRptEvent):void
		{
			itemLocationRptService.sendExportItemLocationRpt(evt.exportItemLocationRptList, exportItemLocationRptResult);
		}
		
		private function exportItemLocationRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshExportItemLocationRptEvent());
		}
		
	}
}
