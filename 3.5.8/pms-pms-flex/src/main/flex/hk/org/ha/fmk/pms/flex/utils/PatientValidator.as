package hk.org.ha.fmk.pms.flex.utils {
	public class PatientValidator {
		
		public static function isPatientNameValid(name:String):Boolean {
			if (new RegExp("^[A-Za-z\\-\\s]+,[A-Za-z\\-\\s]*").test(name)) {
				return true;
			}
			
			return false;
		}
	}
}