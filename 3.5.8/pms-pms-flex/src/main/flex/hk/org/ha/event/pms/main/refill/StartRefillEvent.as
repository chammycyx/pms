package hk.org.ha.event.pms.main.refill {
	
	import hk.org.ha.model.pms.vo.ehr.EhrPatient;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class StartRefillEvent extends AbstractTideEvent 
	{
		private var _sourceView:String;
		private var _refillScheduleId:Number;
		private var _callbackFunc:Function;
		private var _ehrPatient:EhrPatient;
		
		public function StartRefillEvent(sourceView:String, refillScheduleId:Number, callbackFunc:Function=null, ehrPatient:EhrPatient=null):void 
		{
			super();
			_sourceView = sourceView;
			_refillScheduleId = refillScheduleId;
			_callbackFunc = callbackFunc;
			_ehrPatient = ehrPatient;
		}
		
		public function get sourceView():String {
			return _sourceView;
		}		
		
		public function get refillScheduleId():Number {
			return _refillScheduleId;
		}
		
		public function set callbackFunc(callbackFunc:Function):void {
			_callbackFunc = callbackFunc;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
		
		public function get ehrPatient():EhrPatient {
			return _ehrPatient;
		}
	}
}