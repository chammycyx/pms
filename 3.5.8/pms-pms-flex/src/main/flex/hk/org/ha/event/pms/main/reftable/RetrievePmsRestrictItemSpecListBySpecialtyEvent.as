package hk.org.ha.event.pms.main.reftable {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePmsRestrictItemSpecListBySpecialtyEvent extends AbstractTideEvent 
	{
		
		private var _specType:String;
		private var _specCode:String;
		private var _subSpecCode:String;
		
		public function RetrievePmsRestrictItemSpecListBySpecialtyEvent(specType:String, specCode:String, subSpecCode:String):void 
		{
			super();
			_specType = specType;
			_specCode = specCode;
			_subSpecCode = subSpecCode;
			
		}
		
		public function get specType():String{
			return _specType;
		}
		
		public function get specCode():String{
			return _specCode;
		}
		
		public function get subSpecCode():String{
			return _subSpecCode;
		}
		
	}
}