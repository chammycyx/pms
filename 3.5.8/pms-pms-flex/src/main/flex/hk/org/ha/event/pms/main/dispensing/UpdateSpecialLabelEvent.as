package hk.org.ha.event.pms.main.dispensing
{
	import hk.org.ha.model.pms.vo.label.SpecialLabel;
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateSpecialLabelEvent extends AbstractTideEvent 
	{		
		private var _specialLabel:SpecialLabel;
		
		public function UpdateSpecialLabelEvent(specialLabel:SpecialLabel):void 
		{
			super();
			_specialLabel = specialLabel;
		}
		
		public function get specialLabel():SpecialLabel {
			return _specialLabel;
		}

	}
}