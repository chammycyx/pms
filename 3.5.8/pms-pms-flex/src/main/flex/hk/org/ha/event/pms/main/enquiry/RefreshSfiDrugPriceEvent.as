package hk.org.ha.event.pms.main.enquiry
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshSfiDrugPriceEvent extends AbstractTideEvent 
	{		
		private var _nextColumnPosition:Number;
		private var _nextRowPosition:Number;
		private var _addNewRecord:Boolean;
		
		public function RefreshSfiDrugPriceEvent(nextColumnPosition:Number, nextRowPosition:Number, addNewRecord:Boolean):void
		{
			super();
			_nextColumnPosition = nextColumnPosition;
			_nextRowPosition = nextRowPosition;
			_addNewRecord = addNewRecord;
		}
		
		public function get nextColumnPosition():Number
		{
			return _nextColumnPosition;
		}
		
		public function get nextRowPosition():Number
		{
			return _nextRowPosition;
		}
		
		public function get addNewRecord():Boolean
		{
			return _addNewRecord;
		}
	}
}