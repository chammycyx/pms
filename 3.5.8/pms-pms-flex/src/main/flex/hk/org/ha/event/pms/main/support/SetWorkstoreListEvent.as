package hk.org.ha.event.pms.main.support
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class SetWorkstoreListEvent extends AbstractTideEvent 
	{
		private var _list:ArrayCollection;
		
		public function SetWorkstoreListEvent(list:ArrayCollection):void
		{
			super();
			_list = list;
		}
		
		public function get list():ArrayCollection {
			return _list;
		}
	}
}