package hk.org.ha.event.pms.main.reftable {
	
	import hk.org.ha.view.pms.main.reftable.popup.ItemLocationMaintPrepackPopup;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePrepackListEvent extends AbstractTideEvent 
	{
		private var _itemCode:String;
		private var _popup:ItemLocationMaintPrepackPopup;
		
		public function get popup():ItemLocationMaintPrepackPopup
		{
			return _popup;
		}

		public function get itemCode():String
		{
			return _itemCode;
		}

		public function RetrievePrepackListEvent(itemCode:String, popup:ItemLocationMaintPrepackPopup):void 
		{
			_itemCode = itemCode;
			_popup = popup;
			super();
		}
	}
}