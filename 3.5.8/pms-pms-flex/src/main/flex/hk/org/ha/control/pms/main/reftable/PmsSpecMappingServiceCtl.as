package hk.org.ha.control.pms.main.reftable {
	
	import hk.org.ha.event.pms.main.reftable.DeletePmsSpecMappingEvent;
	import hk.org.ha.event.pms.main.reftable.ResetPmsSpecMappingListEvent;
	import hk.org.ha.event.pms.main.reftable.UpdatePmsSpecMappingEvent;
	import hk.org.ha.model.pms.biz.reftable.PmsSpecMappingServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("pmsSpecMappingServiceCtl", restrict="true")]
	public class PmsSpecMappingServiceCtl 
	{
		[In]
		public var pmsSpecMappingService:PmsSpecMappingServiceBean;
		
		private var isDeletePmsSpecMapping:Boolean;
		
		[Observer]
		public function updatePmsSpecMapping(evt:UpdatePmsSpecMappingEvent):void
		{
			isDeletePmsSpecMapping = false;
			pmsSpecMappingService.updatePmsSpecMapping(evt.pmsSpecMapping, resetPmsSpecMappingList);
		}
		
		[Observer]
		public function deletePmsSpecMapping(evt:DeletePmsSpecMappingEvent):void
		{
			isDeletePmsSpecMapping = true;
			pmsSpecMappingService.deletePmsSpecMapping(evt.delPmsSpecMapping, resetPmsSpecMappingList);
		}
		
		private function resetPmsSpecMappingList(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new ResetPmsSpecMappingListEvent(isDeletePmsSpecMapping));
		}
	}
}
