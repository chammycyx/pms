package hk.org.ha.event.pms.main.reftable.mp {
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdatePmsMpSpecMappingListEvent extends AbstractTideEvent 
	{			
		private var _pmsMpSpecMappingList:ArrayCollection;
		
		public function UpdatePmsMpSpecMappingListEvent(pmsMpSpecMappingList:ArrayCollection):void 
		{
			super();
			_pmsMpSpecMappingList = pmsMpSpecMappingList;
		}
		
		public function get pmsMpSpecMappingList():ArrayCollection {
			return _pmsMpSpecMappingList;
		}
	}
}