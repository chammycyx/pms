package hk.org.ha.event.pms.main.enquiry
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSfiInvoiceDetailEvent extends AbstractTideEvent 
	{
		private var _sfiInvoiceId:Number;		
		private var _isPreview:Boolean;
		
		public function RetrieveSfiInvoiceDetailEvent(sfiInvoiceId:Number, isPreview:Boolean):void
		{
			super();
			_sfiInvoiceId = sfiInvoiceId;
			_isPreview = isPreview;
		}
		
		public function get sfiInvoiceId():Number
		{
			return _sfiInvoiceId;
		}
		
		public function get isPreview():Boolean
		{
			return _isPreview;
		}
	}
}