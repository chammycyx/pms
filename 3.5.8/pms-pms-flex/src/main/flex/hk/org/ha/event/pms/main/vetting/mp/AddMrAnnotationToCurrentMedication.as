package hk.org.ha.event.pms.main.vetting.mp
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class AddMrAnnotationToCurrentMedication extends AbstractTideEvent
	{
		private var _patHospCode:String;
		private var _orderNum:String;
		private var _itemNum:Number;
		private var _mrAnnotation:String;		
		
		public function AddMrAnnotationToCurrentMedication(patHospCode:String, orderNum:String, itemNum:Number, mrAnnotation:String)
		{
			super();
			_patHospCode = patHospCode;
			_orderNum = orderNum;
			_itemNum = itemNum;
			_mrAnnotation = mrAnnotation;
		}
		
		public function get patHospCode():String
		{
			return _patHospCode;
		}
		
		public function get orderNum():String
		{
			return _orderNum;
		}
		
		public function get itemNum():Number
		{
			return _itemNum;
		}
		
		public function get mrAnnotation():String
		{
			return _mrAnnotation;
		}
	}
}