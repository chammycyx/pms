package hk.org.ha.event.pms.main.drug {
	
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CreateDrugNameTreeEvent extends AbstractTideEvent 
	{			
		private var _drugSearchSource:DrugSearchSource;
		private var _drugSearchNameMapList:ArrayCollection;
		private var _itemCodeByItemCodeSearch:String;
		
		public function CreateDrugNameTreeEvent(drugSearchSource:DrugSearchSource, drugSearchNameMapList:ArrayCollection, itemCodeByItemCodeSearch:String):void 
		{
			super();
			_drugSearchSource = drugSearchSource;
			_drugSearchNameMapList = drugSearchNameMapList;
			_itemCodeByItemCodeSearch = itemCodeByItemCodeSearch;
		}
		
		public function get drugSearchSource():DrugSearchSource {
			return _drugSearchSource;
		}
		
		public function get drugSearchNameMapList():ArrayCollection {
			return _drugSearchNameMapList;
		}
		
		public function get itemCodeByItemCodeSearch():String {
			return _itemCodeByItemCodeSearch;
		}
	}
}