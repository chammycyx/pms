package hk.org.ha.event.pms.main.pivas.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowBatchPrepAddBatchPrepPopupEvent extends AbstractTideEvent 
	{
		private var _batchPrepAddBatchPrepPopupProp:BatchPrepAddBatchPrepPopupProp;
		
		public function ShowBatchPrepAddBatchPrepPopupEvent(batchPrepAddBatchPrepPopupProp:BatchPrepAddBatchPrepPopupProp):void 
		{
			super();
			_batchPrepAddBatchPrepPopupProp = batchPrepAddBatchPrepPopupProp;
		}
		
		public function get batchPrepAddBatchPrepPopupProp():BatchPrepAddBatchPrepPopupProp {
			return _batchPrepAddBatchPrepPopupProp;
		}
	}
}