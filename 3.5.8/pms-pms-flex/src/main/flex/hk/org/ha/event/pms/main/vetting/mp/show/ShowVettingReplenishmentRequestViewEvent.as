package hk.org.ha.event.pms.main.vetting.mp.show
{		
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowVettingReplenishmentRequestViewEvent extends AbstractTideEvent
	{		
		private var _clearMessages:Boolean;
		
		public function ShowVettingReplenishmentRequestViewEvent(clearMessages:Boolean=true)
		{
			super();			
			_clearMessages = clearMessages;
		}				
		
		public function get clearMessages():Boolean
		{
			return _clearMessages;
		}
	}
}