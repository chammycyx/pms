package hk.org.ha.event.pms.main.reftable {		
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveChargeSpecialtyListEvent extends AbstractTideEvent 
	{		
		private var _callBackFunction:Function;
		
		public function RetrieveChargeSpecialtyListEvent(functionValue:Function=null):void 
		{
			super();
			_callBackFunction = functionValue;
		}
		
		public function get callBackFunction():Function
		{
			return _callBackFunction;
		}
	}
}