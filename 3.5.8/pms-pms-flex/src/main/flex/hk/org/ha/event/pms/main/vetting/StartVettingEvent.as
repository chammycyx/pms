package hk.org.ha.event.pms.main.vetting {
		
	import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
	import hk.org.ha.model.pms.vo.ehr.EhrPatient;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class StartVettingEvent extends AbstractTideEvent 
	{
		private var _oneStopOrderType:OneStopOrderType;
		private var _id:Number;
		private var _requiredLockOrder:Boolean;
		private var _ehrPatient:EhrPatient;
		
		public function StartVettingEvent(oneStopOrderType:OneStopOrderType, id:Number, requiredLockOrder:Boolean=true, ehrPatient:EhrPatient=null):void 
		{
			super();
			this._oneStopOrderType = oneStopOrderType;
			this._id = id;
			this._requiredLockOrder = requiredLockOrder;
			this._ehrPatient = ehrPatient;
		}
		
		public function get oneStopOrderType():OneStopOrderType {
			return _oneStopOrderType;
		}
		
		public function get id():Number {
			return _id;
		}
		
		public function get requiredLockOrder():Boolean {
			return _requiredLockOrder;
		}
		
		public function get ehrPatient():EhrPatient {
			return _ehrPatient;
		}
	}
}