package hk.org.ha.event.pms.main.checkissue.mp
{
	import hk.org.ha.model.pms.vo.checkissue.mp.DrugCheckViewListSummary;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateDeliveryStatusToCheckedResultEvent extends AbstractTideEvent 
	{			
		private var _drugCheckViewListSummary:DrugCheckViewListSummary;
		private var _msgCode:String;
		private var _batchDate:String;
		private var _batchCode:String;
		
		public function UpdateDeliveryStatusToCheckedResultEvent(drugCheckViewListSummary:DrugCheckViewListSummary, msgCode:String, batchDate:String=null,batchCode:String=null):void 
		{
			super();
			_drugCheckViewListSummary = drugCheckViewListSummary;
			_msgCode = msgCode;
			_batchDate = batchDate;
			_batchCode = batchCode;
		}
		
		public function get drugCheckViewListSummary():DrugCheckViewListSummary {
			return _drugCheckViewListSummary;
		}
		
		public function get msgCode():String {
			return _msgCode;
		}
		
		public function get batchCode():String {
			return _batchCode;
		}
		public function get batchDate():String {
			return _batchDate;
		}
		
	}
}