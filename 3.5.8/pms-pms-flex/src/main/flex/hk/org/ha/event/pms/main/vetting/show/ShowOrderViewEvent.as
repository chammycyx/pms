package hk.org.ha.event.pms.main.vetting.show {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowOrderViewEvent extends AbstractTideEvent {

		public var saveOrderFlag:Boolean = false;
		
		public var selectedIndex:Number = NaN;
		
		public var vetOrderLine:Number = NaN;
		
		public var skipDuplicateCheckFlag:Boolean = false;
		
		public var skipZeroQtyConfirmCount:int = 0;
		
		public var skipMdsAlertFlag:Boolean = false;
		
		public var skipInfusion:Boolean = false;
		
		public var errorString:String = null;
		
		public var infoString:String = null;
		
		public function ShowOrderViewEvent(errorString:String=null, infoString:String=null) {
			super();
			this.errorString = errorString;
			this.infoString  = infoString;
		}
	}
}
