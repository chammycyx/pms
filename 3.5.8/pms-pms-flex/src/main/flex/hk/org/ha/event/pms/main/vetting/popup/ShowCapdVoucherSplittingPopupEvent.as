package  hk.org.ha.event.pms.main.vetting.popup{
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowCapdVoucherSplittingPopupEvent extends AbstractTideEvent 
	{
		private var _callbackFunc:Function;
		private var _setGenNewVoucherFlag:Boolean
		
		public function ShowCapdVoucherSplittingPopupEvent():void 
		{
			super();
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
		
		public function set callbackFunc(value:Function):void {
			_callbackFunc = value;
		}
		
		public function get setGenNewVoucherFlag():Boolean {
			return _setGenNewVoucherFlag;
		}
		
		public function set setGenNewVoucherFlag(setGenNewVoucherFlag:Boolean):void {
			_setGenNewVoucherFlag = setGenNewVoucherFlag;
		}
	}
}