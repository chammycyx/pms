package hk.org.ha.event.pms.main.onestop
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class DefaultCloseComboBoxSpecialtyMedOfficerMappingEvent extends AbstractTideEvent
	{
		private var _specCode:String;
		
		public function DefaultCloseComboBoxSpecialtyMedOfficerMappingEvent(specCode:String)
		{
			super();
			_specCode = specCode;
		}
		
		public function get specCode():String {
			return _specCode;
		}
	}
}