package hk.org.ha.event.pms.main.reftable {
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateLocalWarningListEvent extends AbstractTideEvent 
	{
		private var _updateLocalWarningList:ListCollectionView;
		private var _restoreUpdate:Boolean;
		
		
		public function UpdateLocalWarningListEvent(updateLocalWarningList:ListCollectionView, restoreUpdate:Boolean):void 
		{
			super();
			_updateLocalWarningList = updateLocalWarningList;
			_restoreUpdate = restoreUpdate;
		}
		
		public function get updateLocalWarningList():ListCollectionView
		{
			return _updateLocalWarningList;
		}
		
		public function get restoreUpdate():Boolean
		{
			return _restoreUpdate;
		}
	}
}