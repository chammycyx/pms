package hk.org.ha.event.pms.main.reftable.pivas
{
	import hk.org.ha.model.pms.dms.persistence.PmsDosageConversion;
	import hk.org.ha.model.pms.dms.persistence.PmsDosageConversionSet;
	import hk.org.ha.model.pms.dms.vo.pivas.PivasDrugValidateResponse;
	
	import mx.collections.ArrayCollection;
	import mx.controls.List;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class UpdatePivasDrugListHandlerEvent extends AbstractTideEvent
	{
		private var _pivasDrugList:ArrayCollection;
		
		private var _pivasDrugValidateResponse:PivasDrugValidateResponse;

		public function UpdatePivasDrugListHandlerEvent(pivasDrugList:ArrayCollection, pivasDrugValidateResponse:PivasDrugValidateResponse):void
		{
			super();
			_pivasDrugList = pivasDrugList;
			_pivasDrugValidateResponse = pivasDrugValidateResponse;
		}
		
		public function get pivasDrugList():ArrayCollection {
			return _pivasDrugList;
		}
		
		public function get pivasDrugValidateResponse():PivasDrugValidateResponse {
			return _pivasDrugValidateResponse;
		}
	}
}