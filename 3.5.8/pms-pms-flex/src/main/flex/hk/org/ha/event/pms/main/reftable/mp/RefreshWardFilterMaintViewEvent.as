package hk.org.ha.event.pms.main.reftable.mp {
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshWardFilterMaintViewEvent extends AbstractTideEvent 
	{
		private var _action:String;
		
		public function get action():String
		{
			return _action;
		}

		public function RefreshWardFilterMaintViewEvent(action:String):void 
		{
			super();
			_action = action;
		}
	}
}