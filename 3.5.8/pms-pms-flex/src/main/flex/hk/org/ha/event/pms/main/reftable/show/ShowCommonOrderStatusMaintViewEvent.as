package hk.org.ha.event.pms.main.reftable.show {
	
	import hk.org.ha.model.pms.dms.persistence.DmDrug;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowCommonOrderStatusMaintViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		private var _drugSearchComplete:Boolean;
		private var _drugSearchExit:Boolean;
		private var _drugSearchDmDrug:DmDrug;
		
		public function ShowCommonOrderStatusMaintViewEvent(drugSearchComplete:Boolean=false, drugSearchExit:Boolean=false, clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
			_drugSearchExit = drugSearchExit;
			_drugSearchComplete = drugSearchComplete;
		}
		
		public function get drugSearchExit():Boolean {
			return _drugSearchExit;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}

		public function get drugSearchComplete():Boolean {
			return _drugSearchComplete;
		}
		
		public function get drugSearchDmDrug():DmDrug {
			return _drugSearchDmDrug;
		}
		
		public function set drugSearchDmDrug(drugSearchDmDrug:DmDrug):void {
			_drugSearchDmDrug = drugSearchDmDrug;
		}
	}
}