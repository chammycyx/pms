package hk.org.ha.event.pms.main.drug.show {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ClearDrugSearchViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		public function ClearDrugSearchViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}