package hk.org.ha.event.pms.main.vetting.mp
{
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class VetMedProfileEvent extends AbstractTideEvent
	{
		private var _medProfile:MedProfile;
		
		public function VetMedProfileEvent(medProfile:MedProfile)
		{
			super();
			_medProfile = medProfile;
		}

		public function get medProfile():MedProfile
		{
			return _medProfile;
		}
		
	}
}