package hk.org.ha.event.pms.main.vetting.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowFormVerbPopupEvent extends AbstractTideEvent 
	{		
		private var _okHandler:Function;					
		private var _cancelHandler:Function;
		private var _allFormVerbRankFlag:Boolean;
		
		public function ShowFormVerbPopupEvent(okHandlerVal:Function, allFormVerbRankFlag:Boolean=false, cancelHandler:Function=null):void 
		{
			super();
			_okHandler = okHandlerVal;	
			_cancelHandler = cancelHandler;
			_allFormVerbRankFlag = allFormVerbRankFlag;
		}
		
		public function get okHandler():Function {
			return _okHandler;
		}
		
		public function get cancelHandler():Function {
			return _cancelHandler;
		}
		
		public function get allFormVerbRankFlag():Boolean {
			return _allFormVerbRankFlag;
		}
	}
}
