package hk.org.ha.control.pms.main.checkissue.mp{
	
	import hk.org.ha.event.pms.main.checkissue.mp.RetrieveSentListEvent;
	import hk.org.ha.model.pms.biz.checkissue.mp.SendListServiceBean;
	
	[Bindable]
	[Name("sendListServiceCtl", restrict="true")]
	public class SendListServiceCtl 
	{
		[In]
		public var sendListService:SendListServiceBean;
		
		[Observer]
		public function retrieveSentList(evt:RetrieveSentListEvent):void
		{
			sendListService.retrieveSentList();
		}

	}
}
