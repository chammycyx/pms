package hk.org.ha.event.pms.main.reftable.popup
{
	import hk.org.ha.model.pms.dms.persistence.DmDrug;
	import hk.org.ha.model.pms.dms.persistence.PmsMultiDoseConvSet;

	public class MultiDoseConversionMaintPopupProp
	{
		public var actionMode:String;
		public var okHandler:Function;
		public var cancelHandler:Function;
		public var pmsMultiDoseConvSet:PmsMultiDoseConvSet;
		public var dmDailyFrequencyArray:Object;
		public var dailySupplFrequencyArray:Object;
		public var drugSearchDmDrug:DmDrug;
	}
}
