package hk.org.ha.event.pms.main.vetting.mp
{		
	import flash.events.Event;
	
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateMedProfileInMemoryEvent extends AbstractTideEvent
	{
		private var _medProfile:MedProfile;
		private var _callBackEvent:Event;
		private var _callBackFunction:Function;
		private var _patientInfoUpdated:Boolean;
		
		public function UpdateMedProfileInMemoryEvent(medProfileValue:MedProfile, eventValue:Event=null, functionValue:Function=null,
													  patientInfoUpdatedValue:Boolean=false)
		{
			super();
			_medProfile         = medProfileValue;
			_callBackEvent      = eventValue;
			_callBackFunction   = functionValue;
			_patientInfoUpdated = patientInfoUpdatedValue;
		}
		
		public function get medProfile():MedProfile
		{
			return _medProfile;
		}
		
		public function get callBackEvent():Event
		{
			return _callBackEvent;
		}
		
		public function get callBackFunction():Function
		{
			return _callBackFunction;
		}
		
		public function get patientInfoUpdated():Boolean
		{
			return _patientInfoUpdated;
		}
	}
}