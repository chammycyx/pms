package hk.org.ha.event.pms.main.reftable {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshQtyConversionRptListEvent extends AbstractTideEvent 
	{
		private var _qtyExportSuccess:Boolean;
		
		public function RefreshQtyConversionRptListEvent(qtyExportSuccess:Boolean):void 
		{
			super();
			_qtyExportSuccess = qtyExportSuccess;
		}
		
		public function get qtyExportSuccess():Boolean
		{
			return _qtyExportSuccess;
		}
		
	}
}