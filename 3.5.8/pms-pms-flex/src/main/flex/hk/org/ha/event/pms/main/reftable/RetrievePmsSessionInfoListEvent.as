package hk.org.ha.event.pms.main.reftable {
	
	import hk.org.ha.view.pms.main.reftable.popup.OperationModeMaintActiveProfileConfirmationPopup;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePmsSessionInfoListEvent extends AbstractTideEvent 
	{		
		private var _popupScreen:OperationModeMaintActiveProfileConfirmationPopup;
		public function RetrievePmsSessionInfoListEvent(popupScreen:OperationModeMaintActiveProfileConfirmationPopup):void 
		{
			super();
			_popupScreen = popupScreen;
		}
		
		public function get popupScreen():OperationModeMaintActiveProfileConfirmationPopup {
			return _popupScreen;
		}
	}
}