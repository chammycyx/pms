package hk.org.ha.event.pms.main.vetting.mp.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrieveRenalPdfByItemCodeEvent extends AbstractTideEvent {
		
		private var _itemCode:String;
		
		private var _callback:Function;
		
		public function RetrieveRenalPdfByItemCodeEvent(itemCode:String, callback:Function=null) {
			super();
			_itemCode = itemCode;
			_callback = callback;
		}
		
		public function get itemCode():String {
			return _itemCode;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}