package hk.org.ha.control.pms.main.drug {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.drug.RetrieveDmRegimenEvent;
	import hk.org.ha.model.pms.biz.drug.DmRegimenServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("dmRegimenServiceCtl", restrict="true")]
	public class DmRegimenServiceCtl 
	{
		[In]
		public var dmRegimenService:DmRegimenServiceBean;
		
		private var successEvent:Event;
		
		private var failEvent:Event;
		
		[Observer]
		public function retrieveDmRegimen(evt:RetrieveDmRegimenEvent):void
		{
			successEvent = evt.successEvent;
			failEvent = evt.failEvent;
			
			In(Object(dmRegimenService).retrieveSuccess)
			dmRegimenService.retrieveDmRegimen(evt.regimenCode, retrieveDmRegimenResult);
		}
		
		private function retrieveDmRegimenResult(evt:TideResultEvent):void 
		{
			if(Object(dmRegimenService).retrieveSuccess){
				if(successEvent!=null){
					evt.context.dispatchEvent( successEvent );
				}
			}else{
				if(failEvent!=null){
					evt.context.dispatchEvent( failEvent );
				}
			}
		}
		
	}
}
