package hk.org.ha.control.pms.main.enquiry {
	
	import hk.org.ha.event.pms.main.enquiry.RefreshReprintSfiInvoiceEvent;
	import hk.org.ha.event.pms.main.enquiry.RefreshSfiInvoiceDetailEvent;
	import hk.org.ha.event.pms.main.enquiry.RefreshSfiInvoiceSummaryEvent;
	import hk.org.ha.event.pms.main.enquiry.RefreshSfiInvoiceSummaryListEvent;
	import hk.org.ha.event.pms.main.enquiry.RetrieveSfiInvoiceDetailEvent;
	import hk.org.ha.event.pms.main.enquiry.RetrieveSfiInvoiceSummaryEvent;
	import hk.org.ha.event.pms.main.enquiry.RetrieveSfiInvoiceSummaryListEvent;
	import hk.org.ha.event.pms.main.enquiry.show.ShowSfiInvoiceEnqViewEvent;
	import hk.org.ha.model.pms.biz.enquiry.SfiInvoiceEnqServiceBean;
	
	import mx.collections.ArrayCollection;
	import mx.utils.StringUtil;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	import org.granite.tide.seam.In;
	

	[Bindable]
	[Name("sfiInvoiceEnqServiceCtl.as", restrict="true")]
	public class SfiInvoiceEnqServiceCtl 
	{
	
		[In]
		public var sfiInvoiceEnqService:SfiInvoiceEnqServiceBean;
				
		[In]
		public var ctx:Context;
		
		private var showView:Boolean;
					
		[Observer]
		public function retrieveSfiInvoiceSummaryList(evt:RetrieveSfiInvoiceSummaryListEvent):void{
		
			showView = evt.showView;
			
			In(Object(sfiInvoiceEnqService).retrieveSuccess)
			In(Object(sfiInvoiceEnqService).errMsg)
			
			switch (evt.searchType)
			{
				case 'invoiceNum':
					sfiInvoiceEnqService.retrieveSfiInvoiceByInvoiceNumber(evt.param1, evt.includeVoid, retrieveSfiInvoiceSummaryListResult);
					break;
				case 'orderNum':
					sfiInvoiceEnqService.retrieveSfiInvoiceByMoeOrderNumber(evt.param1, evt.includeVoid, retrieveSfiInvoiceSummaryListResult);
					break;
				case 'hkid':
					sfiInvoiceEnqService.retrieveSfiInvoiceByHkid(evt.param1, evt.includeVoid, retrieveSfiInvoiceSummaryListResult);
					break;
				case 'sfiInvDate':
					sfiInvoiceEnqService.retrieveSfiInvoiceByInvoiceDate(evt.param2, evt.includeVoid, retrieveSfiInvoiceSummaryListResult);
					break;
				case 'caseNum':
					sfiInvoiceEnqService.retrieveSfiInvoiceByCaseNum(evt.param1, evt.includeVoid, retrieveSfiInvoiceSummaryListResult);
					break;
				case 'ticket':
					sfiInvoiceEnqService.retrieveSfiInvoiceByTicket(evt.param2, evt.param1, evt.includeVoid, retrieveSfiInvoiceSummaryListResult);
					break;
			}			
		}
		
		public function retrieveSfiInvoiceSummaryListResult(evt:TideResultEvent):void{
			if (showView) {
				ctx.sfiEnqRetrieveSuccess = Object(sfiInvoiceEnqService).retrieveSuccess;
				evt.context.dispatchEvent(new ShowSfiInvoiceEnqViewEvent());
				return;
			}
	
			evt.context.dispatchEvent(new RefreshSfiInvoiceSummaryListEvent(Object(sfiInvoiceEnqService).retrieveSuccess,Object(sfiInvoiceEnqService).errMsg));
		}
		
		[Observer]
		public function retrieveSfiInvoiceSummary(evt:RetrieveSfiInvoiceSummaryEvent):void{			
			sfiInvoiceEnqService.retrieveSfiInvoicePaymentStatus(evt.sfiInvoiceId, retrieveSfiInvoiceSummaryResult );			
		}
		
		public function retrieveSfiInvoiceSummaryResult(evt:TideResultEvent):void{	
			evt.context.dispatchEvent(new RefreshSfiInvoiceSummaryEvent() );
		}
		
		[Observer]
		public function retrieveSfiInvoiceDetail(evt:RetrieveSfiInvoiceDetailEvent):void{		
			In(Object(sfiInvoiceEnqService).errMsg)
			if( evt.isPreview ){			
				sfiInvoiceEnqService.retrieveSfiInvoiceDetail(evt.sfiInvoiceId, evt.isPreview, retrieveSfiInvoiceDetailResult );
			}else{
				sfiInvoiceEnqService.retrieveSfiInvoiceDetail(evt.sfiInvoiceId, evt.isPreview, reprintSfiInvoiceResult);
			}
		}
		
		public function retrieveSfiInvoiceDetailResult(evt:TideResultEvent):void{	
			evt.context.dispatchEvent(new RefreshSfiInvoiceDetailEvent() );
		}
		
		public function reprintSfiInvoiceResult(evt:TideResultEvent):void{	
			evt.context.dispatchEvent(new RefreshReprintSfiInvoiceEvent(Object(sfiInvoiceEnqService).errMsg) );
		}
		
	}
}

