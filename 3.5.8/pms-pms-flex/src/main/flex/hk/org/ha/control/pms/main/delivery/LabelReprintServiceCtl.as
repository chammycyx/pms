package hk.org.ha.control.pms.main.delivery {
	
	import hk.org.ha.event.pms.main.delivery.RefreshLabelReprintItemListEvent;
	import hk.org.ha.event.pms.main.delivery.RefreshLabelReprintSelectedIndexEvent;
	import hk.org.ha.event.pms.main.delivery.RefreshLabelReprintViewEvent;
	import hk.org.ha.event.pms.main.delivery.RefreshLabelReprintWardSelectionListEvent;
	import hk.org.ha.event.pms.main.delivery.ReprintDeliveryLabelEvent;
	import hk.org.ha.event.pms.main.delivery.ReprintDividerLabelEvent;
	import hk.org.ha.event.pms.main.delivery.ReprintMpDispLabelEvent;
	import hk.org.ha.event.pms.main.delivery.RetrieveLabelReprintDeliveryListEvent;
	import hk.org.ha.event.pms.main.delivery.RetrieveLabelReprintWardSelectionListEvent;
	import hk.org.ha.event.pms.main.delivery.RetrieveMpDispLabelListEvent;
	import hk.org.ha.model.pms.biz.delivery.LabelReprintServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	
	[Bindable]
	[Name("labelReprintServiceCtl", restrict="true")]
	public class LabelReprintServiceCtl 
	{
		[In]
		public var labelReprintService:LabelReprintServiceBean;
		
		
		[Observer]
		public function retrieveLabelReprintDeliveryList(evt:RetrieveLabelReprintDeliveryListEvent):void
		{
			labelReprintService.retrieveDeliveryList(evt.wardCode, evt.batchCode, evt.caseNum, evt.dispenseDate, evt.trxId,
				retrieveLabelReprintDeliveryListResult);
		}
		
		private function retrieveLabelReprintDeliveryListResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshLabelReprintViewEvent());
		}
		
		[Observer]
		public function retrieveLabelReprintWardSelectionList(evt:RetrieveLabelReprintWardSelectionListEvent):void
		{
			labelReprintService.retrieveWardSelectionList(retrieveLabelReprintWardSelectionListResult);
		}
		
		private function retrieveLabelReprintWardSelectionListResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshLabelReprintWardSelectionListEvent(evt.result as ArrayCollection));
		}
		
		[Observer]
		public function reprintMpDispLabel(evt:ReprintMpDispLabelEvent):void
		{
			if ( evt.resendAtdpsMessage ) {
				labelReprintService.resendAtdpsMessage(evt.mpDispLabels, reprintMpDispLabelResult);
			} else {
				labelReprintService.reprintMpDispLabel(evt.mpDispLabels, reprintMpDispLabelResult);
			}
		}
		
		private function reprintMpDispLabelResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshLabelReprintSelectedIndexEvent());
		}
		
		[Observer]
		public function reprintDividerLabel(evt:ReprintDividerLabelEvent):void
		{
			labelReprintService.reprintDividerLabel(reprintDividerLabelResult);
		}
		
		private function reprintDividerLabelResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshLabelReprintSelectedIndexEvent());
		}
		
		[Observer]
		public function reprintDeliveryLabel(evt:ReprintDeliveryLabelEvent):void
		{
			labelReprintService.reprintDeliveryLabel(reprintDeliveryLabelResult);
		}
		
		private function reprintDeliveryLabelResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshLabelReprintSelectedIndexEvent());
		}
		
		[Observer]
		public function retrieveMpDispLabelList(evt:RetrieveMpDispLabelListEvent):void
		{
			labelReprintService.retrieveMpDispLabelListByBatchNum(evt.batchNum, evt.dispenseDate, evt.caseNum, evt.trxId, retrieveMpDispLabelListResult);
		}
		
		private function retrieveMpDispLabelListResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshLabelReprintItemListEvent());
		}
		
	}
}
