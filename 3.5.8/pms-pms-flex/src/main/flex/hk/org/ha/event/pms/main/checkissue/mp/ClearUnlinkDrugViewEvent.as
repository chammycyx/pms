package hk.org.ha.event.pms.main.checkissue.mp
{
	import hk.org.ha.model.pms.vo.checkissue.mp.CheckDelivery;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ClearUnlinkDrugViewEvent extends AbstractTideEvent 
	{			
		
		private var _checkDelivery:CheckDelivery;
		private var _resultMsg:String; 
		
		public function ClearUnlinkDrugViewEvent(resultMsg:String, checkDelivery:CheckDelivery):void 
		{	_resultMsg = resultMsg;
			_checkDelivery = checkDelivery;	
			super();
					
		}
		
		public function get checkDelivery():CheckDelivery {
			return _checkDelivery;
		}
		
		
		public function get resultMsg():String{
			return _resultMsg;
		}
	}
}