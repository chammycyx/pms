package hk.org.ha.control.pms.main.reftable {
	
	import hk.org.ha.event.pms.main.reftable.ClearFdnMappingMaintViewEvent;
	import hk.org.ha.event.pms.main.reftable.RefreshFdnMappingListEvent;
	import hk.org.ha.event.pms.main.reftable.RetrieveFdnMappingListEvent;
	import hk.org.ha.event.pms.main.reftable.UpdateFdnMappingListEvent;
	import hk.org.ha.model.pms.biz.reftable.FdnMappingListServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("fdnMappingListServiceCtl", restrict="true")]
	public class FdnMappingListServiceCtl 
	{
		[In]
		public var fdnMappingListService:FdnMappingListServiceBean;
		
		[Observer]
		public function retrieveFdnMappingList(evt:RetrieveFdnMappingListEvent):void
		{
			fdnMappingListService.retrieveFdnMappingList(evt.itemCode, retrieveFdnMappingListResult);
		}
		
		private function retrieveFdnMappingListResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new RefreshFdnMappingListEvent());
		}
		
		[Observer]
		public function updateFdnMappingList(evt:UpdateFdnMappingListEvent):void
		{
			fdnMappingListService.updateFdnMappingList(evt.fdnMappingList, evt.itemCode, updateFdnMappingListResult);
		}	
		
		private function updateFdnMappingListResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new ClearFdnMappingMaintViewEvent());
		}
		
	}
}
