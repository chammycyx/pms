package hk.org.ha.event.pms.main.reftable {
	
	import hk.org.ha.model.pms.persistence.reftable.DrsPatient;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshDrsPatientEvent extends AbstractTideEvent 
	{
		private var _drsPatient:DrsPatient;
		
		public function RefreshDrsPatientEvent(drsPatient:DrsPatient):void 
		{
			super();
			_drsPatient = drsPatient;
		}
		
		public function get drsPatient():DrsPatient
		{
			return _drsPatient;
		}

	}
}