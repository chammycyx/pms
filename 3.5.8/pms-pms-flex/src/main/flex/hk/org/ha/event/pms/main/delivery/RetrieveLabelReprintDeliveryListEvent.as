package hk.org.ha.event.pms.main.delivery {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveLabelReprintDeliveryListEvent extends AbstractTideEvent 
	{
		private var _wardCode:String;
		private var _batchCode:String;
		private var _caseNum:String;
		private var _dispenseDate:Date;
		private var _trxId:String;
		
		public function RetrieveLabelReprintDeliveryListEvent( wardCode:String, batchCode:String, caseNum:String, dispenseDate:Date, trxId:String):void 
		{
			super();
			_wardCode = wardCode;
			_batchCode = batchCode;
			_caseNum = caseNum;
			_dispenseDate = dispenseDate;
			_trxId = trxId
		}
		
		public function get wardCode():String{
			return _wardCode;
		}
		
		public function get batchCode():String{
			return _batchCode;
		}
		
		public function get caseNum():String{
			return _caseNum;
		}
		
		public function get dispenseDate():Date{
			return _dispenseDate;
		}
		
		public function get trxId():String{
			return _trxId;
		}
	}
}