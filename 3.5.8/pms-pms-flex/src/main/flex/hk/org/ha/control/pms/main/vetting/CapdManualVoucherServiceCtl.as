package hk.org.ha.control.pms.main.vetting {

	import hk.org.ha.event.pms.main.vetting.RetrieveCapdManualVoucherByManualVoucherNumEvent;
	import hk.org.ha.event.pms.main.vetting.SetCapdManualVoucherGridColFocusEvent;
	import hk.org.ha.model.pms.biz.vetting.CapdManualVoucherServiceBean;
	import hk.org.ha.model.pms.persistence.disp.CapdVoucher;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("capdManualVoucherServiceCtl", restrict="true")]
	public class CapdManualVoucherServiceCtl {

		[In]
		public var ctx:Context;
		
		[In]
		public var capdManualVoucherService:CapdManualVoucherServiceBean;
		
		private var manualVoucherGridCol:Number;
		
		private var manualVoucherNum:String;
		
		private var callOkBtn:Boolean;
		
		[Observer]
		public function retrieveCapdManualVoucherByManualVoucherNum(evt:RetrieveCapdManualVoucherByManualVoucherNumEvent):void {
			manualVoucherGridCol = evt.manualVoucherGridCol;
			manualVoucherNum = evt.capdManualVoucherNum;
			callOkBtn = evt.callOkBtn;
			capdManualVoucherService.retrieveCapdManualVoucherByManualVoucherNum(evt.capdManualVoucherNum, retrieveCapdManualVoucherByManualVoucherNumResult);
		}
		
		private function retrieveCapdManualVoucherByManualVoucherNumResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent( new SetCapdManualVoucherGridColFocusEvent(manualVoucherGridCol, manualVoucherNum, callOkBtn) );
			
		}
	}
}
