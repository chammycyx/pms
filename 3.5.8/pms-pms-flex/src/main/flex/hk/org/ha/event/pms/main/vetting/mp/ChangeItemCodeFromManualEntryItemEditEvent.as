package hk.org.ha.event.pms.main.vetting.mp
{
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ChangeItemCodeFromManualEntryItemEditEvent extends AbstractTideEvent
	{
		private var _displayName:String;
		
		private var _itemCode:String;
		
		private var _callBackFunction:Function;
		
		public function ChangeItemCodeFromManualEntryItemEditEvent(itemCode:String, displayName:String, callBackFunction:Function)
		{
			super();
			_itemCode = itemCode;
			_displayName = displayName;
			_callBackFunction = callBackFunction
		}		

		public function get itemCode():String
		{
			return _itemCode;
		}
		
		public function get displayName():String
		{
			return _displayName;
		}
		
		public function get callBackFunction():Function
		{
			return _callBackFunction;
		}
	}
}