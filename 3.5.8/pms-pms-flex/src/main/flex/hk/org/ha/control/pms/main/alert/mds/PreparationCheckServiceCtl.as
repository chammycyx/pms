package hk.org.ha.control.pms.main.alert.mds {
	import hk.org.ha.event.pms.main.alert.mds.CheckPreparationEvent;
	import hk.org.ha.event.pms.main.alert.mds.popup.ShowAlertMsgPopupEvent;
	import hk.org.ha.event.pms.main.alert.mds.popup.ShowMdsLogPopupEvent;
	import hk.org.ha.model.pms.biz.alert.mds.PreparationCheckServiceBean;
	import hk.org.ha.model.pms.persistence.AlertEntity;
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	import hk.org.ha.model.pms.persistence.disp.MedOrderItemAlert;
	import hk.org.ha.model.pms.persistence.disp.PharmOrder;
	import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;
	import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;

	[Bindable]
	[Name("preparationCheckServiceCtl", restrict="true")]
	public class PreparationCheckServiceCtl {
		
		[In]
		public var ctx:Context;
		
		[In]
		public var orderViewInfo:OrderViewInfo;
		
		[In]
		public var preparationCheckService:PreparationCheckServiceBean;
		
		[In]
		public var pharmOrder:PharmOrder;
		
		[In]
		public var preparationAlertList:ListCollectionView;
		
		private var checkPreparationEvent:CheckPreparationEvent;

		[Observer]
		public function checkPreparation(evt:CheckPreparationEvent):void {
			checkPreparationEvent = evt;
			evt.medOrderItem.dislink();
			preparationCheckService.checkPreparation(evt.medOrderItem, evt.preparationProperty, orderViewInfo.pregnancyCheckFlag, checkPreparationResult);
		}
		
		private function checkPreparationResult(evt:TideResultEvent):void {

			dispatchEvent(new ShowMdsLogPopupEvent(function():void{
			
				var moi:MedOrderItem = checkPreparationEvent.medOrderItem;
				moi.medOrder = pharmOrder.medOrder;
				pharmOrder.relinkAllItem();

				var orgAlertList:ListCollectionView = new ArrayCollection(moi.medOrderItemAlertList.toArray());
				setMedOrderItemAlert(preparationAlertList);

				if (evt.result != null) {
					var showAlertMsgPopupEvent:ShowAlertMsgPopupEvent = new ShowAlertMsgPopupEvent("add");
					showAlertMsgPopupEvent.alertMsg = evt.result as AlertMsg;
					showAlertMsgPopupEvent.overrideAlertFunc = function():void {
						if (checkPreparationEvent.successCallback != null) {
							checkPreparationEvent.successCallback();
						}
					};
					showAlertMsgPopupEvent.returnVettingFunc = function():void {
						setMedOrderItemAlert(orgAlertList);
						if (checkPreparationEvent.returnVettingFunc != null) {
							checkPreparationEvent.returnVettingFunc();
						}
					};
					showAlertMsgPopupEvent.prescribeAlternativeFunc = function():void {
						setMedOrderItemAlert(orgAlertList);
						if (checkPreparationEvent.prescribeAlternativeFunc != null) {
							checkPreparationEvent.prescribeAlternativeFunc();
						}
					}
					dispatchEvent(showAlertMsgPopupEvent);
				}
				else {
					if (checkPreparationEvent.successCallback != null) {
						checkPreparationEvent.successCallback();
					}
				}
				
			}));
		}
		
		private function setMedOrderItemAlert(alertEntityList:ListCollectionView):void {
			var medOrderItem:MedOrderItem = checkPreparationEvent.medOrderItem;
			medOrderItem.medOrderItemAlertList = new ArrayCollection();
			
			for each (var alertEntity:AlertEntity in alertEntityList) {
				var moiAlert:MedOrderItemAlert = new MedOrderItemAlert(alertEntity);
				medOrderItem.medOrderItemAlertList.addItem(moiAlert);
				moiAlert.medOrderItem = medOrderItem;
			}
			
			medOrderItem.markAlertFlag();
		}
	}
}