package hk.org.ha.control.pms.main.pivas{
	
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.event.pms.main.pivas.CreatePivasBatchPrepEvent;
	import hk.org.ha.event.pms.main.pivas.DeletePivasBatchPrepEvent;
	import hk.org.ha.event.pms.main.pivas.DeletePivasBatchPrepResultEvent;
	import hk.org.ha.event.pms.main.pivas.GetPivasProductLabelPreviewEvent;
	import hk.org.ha.event.pms.main.pivas.QuickAddPivasBatchPrepEvent;
	import hk.org.ha.event.pms.main.pivas.RefreshPivasProductLabelPreviewEvent;
	import hk.org.ha.event.pms.main.pivas.ReprintPivasBatchPrepLabelEvent;
	import hk.org.ha.event.pms.main.pivas.RetrieveBatchPrepMethodInfoListEvent;
	import hk.org.ha.event.pms.main.pivas.RetrievePivasBatchPrepEvent;
	import hk.org.ha.event.pms.main.pivas.RetrievePivasBatchPrepListEvent;
	import hk.org.ha.event.pms.main.pivas.RetrievePivasFormulaDrugListByPrefixDisplayNameEvent;
	import hk.org.ha.event.pms.main.pivas.RetrievePivasFormulaListByPrefixDisplayNameEvent;
	import hk.org.ha.event.pms.main.pivas.RetrievePivasFormulaMethodListByFormulaIdEvent;
	import hk.org.ha.event.pms.main.pivas.SetPivasBatchPrepListEvent;
	import hk.org.ha.event.pms.main.pivas.SetPivasFormulaDrugListEvent;
	import hk.org.ha.event.pms.main.pivas.SetPivasFormulaListEvent;
	import hk.org.ha.event.pms.main.pivas.UpdatePivasBatchPrepEvent;
	import hk.org.ha.event.pms.main.pivas.UpdatePivasBatchPrepResultEvent;
	import hk.org.ha.event.pms.main.pivas.popup.BatchPrepAddBatchPrepPopupProp;
	import hk.org.ha.event.pms.main.pivas.popup.ShowBatchPrepAddBatchPrepPopupEvent;
	import hk.org.ha.model.pms.biz.pivas.PivasBatchPrepServiceBean;
	import hk.org.ha.model.pms.dms.vo.pivas.PivasPrepValidateResponse;
	import hk.org.ha.model.pms.vo.pivas.AddPivasBatchPrepDetail;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("pivasBatchPrepServiceCtl", restrict="true")]
	public class PivasBatchPrepServiceCtl 
	{
		[In]
		public var pivasBatchPrepService:PivasBatchPrepServiceBean;

		private var callbackFunc:Function;
		
		private var addBatchPrepFlag:Boolean;
		private var quickAddPivasBatchPrepFlag:Boolean;
		
		[Observer]
		public function retrievePivasFormulaListByPrefixDisplayName(evt:RetrievePivasFormulaListByPrefixDisplayNameEvent):void
		{
			pivasBatchPrepService.retrievePivasFormulaListByPrefixDisplayName(evt.prefixDisplayName, evt.activeBatchPrepFormulaOnly,
				function(resultEvt:TideResultEvent):void {
					if (evt.callbackFunc != null) {
						evt.callbackFunc(resultEvt.result);
					} else {
						resultEvt.context.dispatchEvent(new SetPivasFormulaListEvent(resultEvt.result as ArrayCollection));
					}
				}
			);
		}
		
		[Observer]
		public function retrievePivasFormulaDrugListByPrefixDisplayName(evt:RetrievePivasFormulaDrugListByPrefixDisplayNameEvent):void
		{
			pivasBatchPrepService.retrievePivasFormulaDrugListByPrefixDisplayName(evt.prefixDisplayName,
				function(resultEvt:TideResultEvent):void {
					if (evt.callbackFunc != null) {
						evt.callbackFunc(resultEvt.result);
					} else {
						resultEvt.context.dispatchEvent(new SetPivasFormulaDrugListEvent(resultEvt.result as ArrayCollection));
					}
				}
			);
		}
		
		[Observer]
		public function retrieveBatchPrepMethodInfoList(evt:RetrieveBatchPrepMethodInfoListEvent):void
		{
			callbackFunc = evt.callbackFunc;
			pivasBatchPrepService.retrieveBatchPrepMethodInfoList(evt.pivasFormulaId, retrievePivasFormulaMethodListResult);
		}
		
		private function retrievePivasFormulaMethodListResult(evt:TideResultEvent):void
		{
			callbackFunc(evt.result as ArrayCollection);
		}
		
		[Observer]
		public function retrievePivasFormulaMethodListByFormulaId(evt:RetrievePivasFormulaMethodListByFormulaIdEvent):void
		{
			pivasBatchPrepService.retrievePivasFormulaMethodListByFormulaId(evt.pivasFormulaId,
				function(resultEvt:TideResultEvent):void {
					evt.successCallback(resultEvt.result);
				}
			)
		}
		
		[Observer]
		public function createPivasBatchPrep(evt:CreatePivasBatchPrepEvent):void
		{
			addBatchPrepFlag = true;
			quickAddPivasBatchPrepFlag = false;
			pivasBatchPrepService.createPivasBatchPrep(evt.pivasFormulaMethodId, showBatchPrepAddBatchPrepPopup);
		}
		
		private function showBatchPrepAddBatchPrepPopup(evt:TideResultEvent):void
		{
			var batchPrepAddBatchPrepPopupProp:BatchPrepAddBatchPrepPopupProp = new BatchPrepAddBatchPrepPopupProp();
			batchPrepAddBatchPrepPopupProp.addPivasBatchPrep = addBatchPrepFlag;
			batchPrepAddBatchPrepPopupProp.quickAddPivasBatchPrep = quickAddPivasBatchPrepFlag;
			batchPrepAddBatchPrepPopupProp.addPivasBatchPrepDetail = evt.result as AddPivasBatchPrepDetail;
			evt.context.dispatchEvent(new ShowBatchPrepAddBatchPrepPopupEvent(batchPrepAddBatchPrepPopupProp));
		}
		
		[Observer]
		public function updatePivasBatchPrep(evt:UpdatePivasBatchPrepEvent):void
		{
			pivasBatchPrepService.updatePivasBatchPrep(evt.pivasBatchPrep, updatePivasBatchPrepResult);
		}
		
		private function updatePivasBatchPrepResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new UpdatePivasBatchPrepResultEvent(evt.result as PivasPrepValidateResponse));
		}
		
		[Observer]
		public function retrievePivasBatchPrepList(evt:RetrievePivasBatchPrepListEvent):void
		{
			pivasBatchPrepService.retrievePivasBatchPrepList(evt.manufFromDate, evt.manufToDate, evt.drugKey, retrievePivasBatchPrepListResult);
		}
		
		private function retrievePivasBatchPrepListResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new SetPivasBatchPrepListEvent(evt.result as ArrayCollection));
		}
		
		[Observer]
		public function retrievePivasBatchPrep(evt:RetrievePivasBatchPrepEvent):void
		{
			addBatchPrepFlag = false;
			quickAddPivasBatchPrepFlag = false;
			pivasBatchPrepService.retrievePivasBatchPrep(evt.lotNo, showBatchPrepAddBatchPrepPopup);
		}
		
		[Observer]
		public function deletePivasBatchPrep(evt:DeletePivasBatchPrepEvent):void
		{
			In(Object(pivasBatchPrepService).deleteSuccess)
			pivasBatchPrepService.deletePivasBatchPrep(evt.lotNo, evt.manufFromDate, evt.manufToDate, evt.drugKey, deletePivasBatchPrepResult);
		}
		
		private function deletePivasBatchPrepResult(evt:TideResultEvent):void
		{
			if ( Object(pivasBatchPrepService).deleteSuccess ) {
				retrievePivasBatchPrepListResult(evt);
			} else {
				evt.context.dispatchEvent(new DeletePivasBatchPrepResultEvent(evt.result as String));
			}
		}
		
		[Observer]
		public function quickAddPivasBatchPrep(evt:QuickAddPivasBatchPrepEvent):void
		{
			addBatchPrepFlag = true;
			quickAddPivasBatchPrepFlag = true;
			pivasBatchPrepService.quickAddPivasBatchPrep(evt.lotNo, showBatchPrepAddBatchPrepPopup);
		}
		
		[Observer]
		public function reprintPivasBatchPrepLabel(evt:ReprintPivasBatchPrepLabelEvent):void
		{
			pivasBatchPrepService.printPivasBatchPrepLabel(evt.pivasBatchPrep, true);
		}
		
		[Observer]
		public function getPivasProductLabelPreview(evt:GetPivasProductLabelPreviewEvent):void
		{
			pivasBatchPrepService.getPivasProductLabelPreview(evt.pivasBatchPrep, evt.quickAddPivasBatchPrep, getPivasProductLabelPreviewResult);
		}
		
		private function getPivasProductLabelPreviewResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshPivasProductLabelPreviewEvent());
			
		}
	}
}
