package hk.org.ha.event.pms.main.reftable.pivas
{
	import hk.org.ha.model.pms.dms.persistence.DmDrug;
	import hk.org.ha.model.pms.dms.vo.pivas.PivasDrugValidateCriteria;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class UpdatePivasDrugListEvent extends AbstractTideEvent
	{
		private var _pivasDrugList:ArrayCollection;
		
		private var _pivasDrugValidateCriteria:PivasDrugValidateCriteria;
		
		public function UpdatePivasDrugListEvent(pivasDrugList:ArrayCollection, pivasDrugValidateCriteria:PivasDrugValidateCriteria):void 
		{
			super();
			_pivasDrugList = pivasDrugList;
			_pivasDrugValidateCriteria = pivasDrugValidateCriteria;
		}

		public function get pivasDrugList():ArrayCollection
		{
			return _pivasDrugList;
		}
		
		public function get pivasDrugValidateCriteria():PivasDrugValidateCriteria
		{
			return _pivasDrugValidateCriteria;
		}
	}
}