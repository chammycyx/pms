package hk.org.ha.event.pms.main.reftable.mp {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowWardGroupPopupSysMsgEvent extends AbstractTideEvent 
	{		
		private var _errCode:String;
		
		public function ShowWardGroupPopupSysMsgEvent(errCode:String):void 
		{
			super();
			_errCode = errCode;
		}
		
		public function get errCode():String {
			return _errCode;
		}
	}
}