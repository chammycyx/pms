package hk.org.ha.event.pms.main.report {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveEdsTicketNumRptEvent extends AbstractTideEvent 
	{
		private var _rptDate:Date;
		private var _ticketNum:String;

		public function RetrieveEdsTicketNumRptEvent( rptDate:Date, ticketNum:String):void 
		{
			super();
			_rptDate = rptDate;
			_ticketNum = ticketNum;
		}
		
		public function get rptDate():Date{
			return _rptDate;
		}
		
		public function get ticketNum():String{
			return _ticketNum;
		}
	}
}