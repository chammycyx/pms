package hk.org.ha.event.pms.main.vetting.mp
{
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ReactivateDiscontinuedItemEvent extends AbstractTideEvent
	{
		private var _medProfileItem:MedProfileItem;
		
		public function ReactivateDiscontinuedItemEvent(medProfileItem:MedProfileItem)
		{
			super();
			_medProfileItem = medProfileItem;
		}

		public function get medProfileItem():MedProfileItem
		{
			return _medProfileItem;
		}
	}
}