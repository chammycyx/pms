package hk.org.ha.control.pms.main.reftable.cmm {

	import mx.collections.ListCollectionView;
	
	import hk.org.ha.event.pms.main.reftable.cmm.RetrieveCmmSpecialtyListEvent;
	import hk.org.ha.event.pms.main.reftable.cmm.RetrieveSpecialtyCodeListEvent;
	import hk.org.ha.event.pms.main.reftable.cmm.UpdateCmmSpecialtyListEvent;
	import hk.org.ha.model.pms.biz.reftable.cmm.CmmSpecialtyServiceBean;
	
	import org.granite.tide.events.TideResultEvent;

	[Bindable]
	[Name("cmmSpecialtyServiceCtl", restrict="true")]
	public class CmmSpecialtyServiceCtl {
		
		[In]
		public var cmmSpecialtyService:CmmSpecialtyServiceBean;
		
		private var retrieveSpecialtyCodeListEvent:RetrieveSpecialtyCodeListEvent;
		private var retrieveCmmSpecialtyListEvent:RetrieveCmmSpecialtyListEvent;
		private var updateCmmSpecialtyListEvent:UpdateCmmSpecialtyListEvent;
		
		[Observer]
		public function retrieveSpecialtyCodeList(evt:RetrieveSpecialtyCodeListEvent):void {
			retrieveSpecialtyCodeListEvent = evt;
			cmmSpecialtyService.retrieveSpecialtyCodeList(retrieveSpecialtyCodeListResult);
		}
		
		private function retrieveSpecialtyCodeListResult(evt:TideResultEvent):void {
			retrieveSpecialtyCodeListEvent.callback(evt.result as ListCollectionView);
		}
		
		[Observer]
		public function retrieveCmmSpecialtyList(evt:RetrieveCmmSpecialtyListEvent):void {
			retrieveCmmSpecialtyListEvent = evt;
			cmmSpecialtyService.retrieveCmmSpecialtyList(retrieveCmmSpecialtyListResult);
		}

		private function retrieveCmmSpecialtyListResult(evt:TideResultEvent):void {
			retrieveCmmSpecialtyListEvent.callback(evt.result as ListCollectionView);
		}

		[Observer]
		public function updateCmmSpecialtyList(evt:UpdateCmmSpecialtyListEvent):void {
			updateCmmSpecialtyListEvent = evt;
			cmmSpecialtyService.updateCmmSpecialtyList(evt.cmmSpecialtyList, updateCmmSpecialtyListResult);
		}
		
		private function updateCmmSpecialtyListResult(evt:TideResultEvent):void {
			updateCmmSpecialtyListEvent.callback(evt.result as ListCollectionView);
		}
	}
}
