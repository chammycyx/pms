package hk.org.ha.event.pms.main.reftable {
	
	import hk.org.ha.view.pms.main.reftable.popup.OperationModeMaintActiveProfileConfirmationPopup;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshActiveProfileConfirmationPopupEvent extends AbstractTideEvent 
	{		
		private var _popupScreen:OperationModeMaintActiveProfileConfirmationPopup;
		
		public function RefreshActiveProfileConfirmationPopupEvent(popupScreen:OperationModeMaintActiveProfileConfirmationPopup):void 
		{
			super();
			_popupScreen = popupScreen;
		}
		
		public function get popupScreen():OperationModeMaintActiveProfileConfirmationPopup {
			return _popupScreen;
		}
	}
}