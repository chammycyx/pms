package hk.org.ha.event.pms.main.reftable.popup
{
	import mx.collections.ArrayCollection;

	public class DueOrderScheduleTemplateMaintWardGroupLookupPopupProp
	{
		public var wardGroupList:ArrayCollection;
		public var okHandler:Function;
		public var cancelHandler:Function;
	}
}