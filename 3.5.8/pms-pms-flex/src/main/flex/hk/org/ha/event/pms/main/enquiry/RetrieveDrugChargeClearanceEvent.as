package hk.org.ha.event.pms.main.enquiry
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDrugChargeClearanceEvent extends AbstractTideEvent 
	{
		private var _searchCriteria:String;
		
		public function RetrieveDrugChargeClearanceEvent(searchCriteria:String):void
		{
			super();
			_searchCriteria = searchCriteria;
		}
		
		public function get searchCriteria():String
		{
			return _searchCriteria;
		}		
	}
}