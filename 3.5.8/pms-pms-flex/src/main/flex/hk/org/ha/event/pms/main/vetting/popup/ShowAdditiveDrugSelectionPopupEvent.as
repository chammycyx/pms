package hk.org.ha.event.pms.main.vetting.popup {
		
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowAdditiveDrugSelectionPopupEvent extends AbstractTideEvent 
	{
		private var _rxDrugList:ListCollectionView;		
		
		private var _closeHandler:Function;
		
		public function ShowAdditiveDrugSelectionPopupEvent(listValue:ListCollectionView, functionValue:Function):void 
		{
			super();
			_rxDrugList = listValue;
			_closeHandler = functionValue;
		}
		
		public function get rxDrugList():ListCollectionView
		{
			return _rxDrugList;
		}
		
		public function get closeHandler():Function
		{
			return _closeHandler;
		}
	}
}
