package hk.org.ha.event.pms.main.dispensing
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshHkidBarcodeLabelEvent extends AbstractTideEvent 
	{	
		private var _retrieveSuccess:Boolean;
		
		public function RefreshHkidBarcodeLabelEvent(retrieveSuccess:Boolean):void 
		{
			super();
			_retrieveSuccess = retrieveSuccess;
		}
		
		public function get retrieveSuccess():Boolean {
			return _retrieveSuccess;
		}
	}
}