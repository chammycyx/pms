package hk.org.ha.event.pms.main.reftable.popup {
	
	import mx.controls.LinkButton;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowMpItemRptTypePopupEvent extends AbstractTideEvent 
	{
		private var _exportButton:LinkButton;
		
		private var _callBackFunc:Function;
		
		public function get exportButton():LinkButton
		{
			return _exportButton;
		}

		public function get callBackFunc():Function
		{
			return _callBackFunc;
		}
		
		public function ShowMpItemRptTypePopupEvent(exportButton:LinkButton, callBackFunc:Function):void 
		{
			super();
			_exportButton = exportButton;
			_callBackFunc = callBackFunc;
		}
	}
}