package hk.org.ha.event.pms.main.vetting.mp.tpn.popup
{
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.medprofile.TpnRequest;
	import hk.org.ha.view.pms.main.vetting.mp.tpn.popup.TpnPopup;
	
	public class ShowDurationPopupEvent extends AbstractTideEvent
	{	
		private var _isDuplicateFlag:Boolean;
		private var _selectedItem:TpnRequest;
		private var _selectedIndex:Number;
		private var _tpnPopup:TpnPopup;
		
		public function ShowDurationPopupEvent(isDuplicateFlag:Boolean, selectedItem:TpnRequest, selectedIndex:Number, tpnPopup:TpnPopup):void {
			super();
			_isDuplicateFlag = isDuplicateFlag;
			_tpnPopup = tpnPopup;
			
			if(isDuplicateFlag){
				_selectedItem = selectedItem;
				_selectedIndex = selectedIndex;
			}
		}
		
		public function get isDuplicateFlag():Boolean
		{
			return _isDuplicateFlag;
		}
		
		public function get selectedItem():TpnRequest
		{
			return _selectedItem;
		}
		
		public function get selectedIndex():Number
		{
			return _selectedIndex;
		}
		
		public function get tpnPopup():TpnPopup
		{
			return _tpnPopup;
		}
	}
}