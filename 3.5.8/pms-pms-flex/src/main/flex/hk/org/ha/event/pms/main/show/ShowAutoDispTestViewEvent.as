package hk.org.ha.event.pms.main.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowAutoDispTestViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		public function ShowAutoDispTestViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}