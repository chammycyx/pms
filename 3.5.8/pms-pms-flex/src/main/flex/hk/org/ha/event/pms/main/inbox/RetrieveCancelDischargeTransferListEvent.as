package hk.org.ha.event.pms.main.inbox
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveCancelDischargeTransferListEvent extends AbstractTideEvent
	{
		public function RetrieveCancelDischargeTransferListEvent()
		{
			super();
		}
	}
}