package hk.org.ha.event.pms.main.checkissue.popup {
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDrugCheckIssueDocListPopupEvent extends AbstractTideEvent 
	{
		private var _docList:ListCollectionView;
		private var _clearMessages:Boolean;
		private var _closeFunc:Function;
		
		public function ShowDrugCheckIssueDocListPopupEvent(docList:ListCollectionView, closeFunc:Function=null, clearMessages:Boolean=true):void 
		{
			super();
			_docList = docList;
			_clearMessages = clearMessages;
			_closeFunc = closeFunc;
		}
		
		public function get docList():ListCollectionView {
			return _docList;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
		
		public function get closeFunc():Function {
			return _closeFunc;
		}
	}
}