package hk.org.ha.event.pms.main.reftable.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowPivasSiteLookupPopupEvent extends AbstractTideEvent 
	{
		private var _pivasSiteLookupPopupProp:PivasSiteLookupPopupProp;
		
		public function ShowPivasSiteLookupPopupEvent(pivasSiteLookupPopupProp:PivasSiteLookupPopupProp):void 
		{
			super();
			_pivasSiteLookupPopupProp = pivasSiteLookupPopupProp;
		}
		
		public function get pivasSiteLookupPopupProp():PivasSiteLookupPopupProp{
			return _pivasSiteLookupPopupProp;
		}
	}
}