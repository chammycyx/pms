package hk.org.ha.event.pms.main.reftable
{
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class UpdateDmDrugListEvent extends AbstractTideEvent
	{
		private var _extend:Boolean;
		
		public function UpdateDmDrugListEvent(extend:Boolean):void
		{
			super();
			this._extend = extend;
		}
		
		public function get extend():Boolean {
			return _extend;
		}
	}
}