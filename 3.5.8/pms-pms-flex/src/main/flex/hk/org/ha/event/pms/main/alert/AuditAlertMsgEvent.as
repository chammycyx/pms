package hk.org.ha.event.pms.main.alert {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class AuditAlertMsgEvent extends AbstractTideEvent {

		private var _messageCode:String;
		
		private var _desc:String;
		
		private var _callback:Function;
		
		public function AuditAlertMsgEvent(messageCode:String, desc:String, callback:Function=null) {
			super();
			_messageCode = messageCode;
			_desc = desc;
			_callback = callback;
		}

		public function get messageCode():String {
			return _messageCode;
		}
		
		public function get desc():String {
			return _desc;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}
