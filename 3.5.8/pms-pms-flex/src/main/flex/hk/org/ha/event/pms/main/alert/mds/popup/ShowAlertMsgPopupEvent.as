package hk.org.ha.event.pms.main.alert.mds.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;
	import hk.org.ha.model.pms.vo.alert.mds.MedProfileAlertMsg;
	
	public class ShowAlertMsgPopupEvent extends AbstractTideEvent {
		
		public var state:String;
		
		public var allowEditFlag:Boolean;
		
		public var alertMsg:AlertMsg;
		
		public var acknowledgeFunc:Function;
		
		public var cancelVettingFunc:Function;
		
		public var overrideAlertFunc:Function;
		
		public var prescribeAlternativeFunc:Function;
		
		public var returnVettingFunc:Function;
		
		public var closeFunc:Function;
		
		public function ShowAlertMsgPopupEvent(state:String="enquire", allowEditFlag:Boolean=true, alertMsg:AlertMsg=null) {
			super();
			this.state = state;
			this.allowEditFlag = allowEditFlag;
			this.alertMsg = alertMsg;
		}
	}
}
