package hk.org.ha.control.pms.main.report {
	import hk.org.ha.event.pms.main.report.PrintCapdVoucherRptEvent;
	import hk.org.ha.event.pms.main.report.RefreshCapdVoucherRptEvent;
	import hk.org.ha.event.pms.main.report.RetrieveCapdVoucherRptEvent;
	import hk.org.ha.model.pms.biz.report.CapdVoucherRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	
	[Bindable]
	[Name("capdVoucherRptServiceCtl", restrict="true")]
	public class CapdVoucherRptServiceCtl 
	{
		[In]
		public var capdVoucherRptService:CapdVoucherRptServiceBean;
		
		[Observer]
		public function retrieveSuspendStatRpt(evt:RetrieveCapdVoucherRptEvent):void
		{
			In(Object(capdVoucherRptService).retrieveSuccess)
			In(Object(capdVoucherRptService).validHkid)
			capdVoucherRptService.retrieveCapdVoucherRptList(evt.dateFrom, evt.dateTo, evt.hkid, evt.patName, evt.voucherNum, retrieveCapdVoucherRptResult);
		}
		
		private function retrieveCapdVoucherRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshCapdVoucherRptEvent((Object(capdVoucherRptService).retrieveSuccess), (Object(capdVoucherRptService).validHkid)) );
		}
		
		[Observer]
		public function printCapdVoucherRpt(evt:PrintCapdVoucherRptEvent):void
		{
			capdVoucherRptService.printCapdVoucherRpt();
		}
		
	}
}
