package hk.org.ha.event.pms.main.enquiry {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrieveRenalDmDrugEvent extends AbstractTideEvent {
		
		private var _itemCode:String;
		
		private var _callback:Function;
		
		public function RetrieveRenalDmDrugEvent(itemCode:String, callback:Function) {
			super();
			_itemCode = itemCode;
			_callback = callback;
		}
		
		public function get itemCode():String {
			return _itemCode;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}