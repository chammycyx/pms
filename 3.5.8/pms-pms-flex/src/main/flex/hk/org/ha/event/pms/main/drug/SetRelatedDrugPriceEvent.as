package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetRelatedDrugPriceEvent extends AbstractTideEvent 
	{			
		private var _drugSearchSource:DrugSearchSource;
		
		public function SetRelatedDrugPriceEvent(drugSearchSource:DrugSearchSource):void 
		{
			super();
			_drugSearchSource = drugSearchSource;
		}
		
		public function get drugSearchSource():DrugSearchSource {
			return _drugSearchSource;
		}
	}
}