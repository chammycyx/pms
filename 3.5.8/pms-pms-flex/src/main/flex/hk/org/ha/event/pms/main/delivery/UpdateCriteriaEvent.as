package hk.org.ha.event.pms.main.delivery
{
	import hk.org.ha.model.pms.udt.medprofile.DeliveryRequestOrderType;
	import hk.org.ha.model.pms.vo.delivery.FollowUpItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateCriteriaEvent extends AbstractTideEvent
	{
		private var _dueDate:Date;
		private var _orderType:DeliveryRequestOrderType;
		private var _generateDay:Number;
		private var _caseNum:String;
		
		public function UpdateCriteriaEvent(dueDate:Date, orderType:DeliveryRequestOrderType, generateDay:Number, caseNum:String)
		{
			_dueDate = dueDate;
			_orderType = orderType;
			_generateDay = generateDay;
			_caseNum = caseNum;
		}
		
		public function get dueDate():Date
		{
			return _dueDate;
		}
		
		public function get orderType():DeliveryRequestOrderType
		{
			return _orderType;
		}
		
		public function get generateDay():Number
		{
			return _generateDay;
		}
		
		public function get caseNum():String
		{
			return _caseNum;
		}
	}
}