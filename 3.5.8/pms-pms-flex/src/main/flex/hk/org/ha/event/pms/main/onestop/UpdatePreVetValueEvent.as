package hk.org.ha.event.pms.main.onestop {
		
	import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
	import hk.org.ha.model.pms.vo.onestop.OneStopOrderInfo;
	import hk.org.ha.model.pms.vo.onestop.OneStopSummary;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdatePreVetValueEvent extends AbstractTideEvent 
	{					
		private var _oneStopSummary:OneStopSummary;	
		private var _oneStopOrderInfo:OneStopOrderInfo;
		private var _id:Number;
		private var _callbackFunction:Function;
						
		public function UpdatePreVetValueEvent(oneStopSummary:OneStopSummary, oneStopOrderInfo:OneStopOrderInfo, id:Number, callbackFunction:Function):void 
		{
			super();
			this._oneStopSummary = oneStopSummary;
			this._oneStopOrderInfo = oneStopOrderInfo;
			this._id = id;
			this._callbackFunction = callbackFunction;
		}
		
		public function get oneStopSummary():OneStopSummary {
			return _oneStopSummary;
		}
		
		public function get oneStopOrderInfo():OneStopOrderInfo {
			return _oneStopOrderInfo;
		}

		public function get id():Number {
			return _id;
		}

		public function get callbackFunction():Function
		{
			return _callbackFunction;
		}
	}
}