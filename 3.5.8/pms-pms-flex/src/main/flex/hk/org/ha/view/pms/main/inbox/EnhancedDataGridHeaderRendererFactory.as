package hk.org.ha.view.pms.main.inbox
{
	import mx.binding.utils.BindingUtils;
	import mx.controls.Label;
	import mx.core.IFactory;
	
	public class EnhancedDataGridHeaderRendererFactory implements IFactory
	{
		private var _column:EnhancedDataGridColumn;
		
		public function EnhancedDataGridHeaderRendererFactory(column:EnhancedDataGridColumn)
		{
			_column = column;
		}
		
		public function newInstance():*
		{
			var label:Label = new Label();
			BindingUtils.bindProperty(label, "text", _column, "headerText");
			BindingUtils.bindProperty(label, "toolTip", _column, "headerToolTip");
			label.setStyle("paddingLeft", _column.getStyle("paddingLeft"));
			label.setStyle("paddingRight",  _column.getStyle("paddingRight"));
			return label;
		}
	}
}