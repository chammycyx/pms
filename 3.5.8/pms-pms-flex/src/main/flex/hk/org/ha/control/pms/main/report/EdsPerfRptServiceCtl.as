package hk.org.ha.control.pms.main.report {
	import hk.org.ha.event.pms.main.report.PrintEdsPerfRptEvent;
	import hk.org.ha.event.pms.main.report.RefreshEdsPerfRptEvent;
	import hk.org.ha.event.pms.main.report.RetrieveEdsPerfRptEvent;
	import hk.org.ha.model.pms.biz.report.EdsPerfRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("edsPerfRptServiceCtl", restrict="true")]
	public class EdsPerfRptServiceCtl 
	{
		[In]
		public var edsPerfRptService:EdsPerfRptServiceBean;
		
		[Observer]
		public function retrieveEdsPerfRpt(evt:RetrieveEdsPerfRptEvent):void
		{
			In(Object(edsPerfRptService).retrieveSuccess)
			edsPerfRptService.retrieveEdsPerfRpt(evt.selectedTab, retrieveEdsPerfRptResult);
		}
		
		private function retrieveEdsPerfRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshEdsPerfRptEvent(Object(edsPerfRptService).retrieveSuccess));
		}
		
		[Observer]
		public function printEdsPerfRpt(evt:PrintEdsPerfRptEvent):void
		{
			edsPerfRptService.printEdsPerfRpt();
		}
		
	}
}
