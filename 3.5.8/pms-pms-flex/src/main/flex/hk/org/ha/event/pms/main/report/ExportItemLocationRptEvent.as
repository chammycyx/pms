package hk.org.ha.event.pms.main.report {
	
	import hk.org.ha.model.pms.vo.report.ItemLocationRpt;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ExportItemLocationRptEvent extends AbstractTideEvent 
	{
		private var _exportItemLocationRptList:ArrayCollection;
		
		public function ExportItemLocationRptEvent(exportItemLocationRptList:ArrayCollection):void 
		{
			super();
			_exportItemLocationRptList = exportItemLocationRptList;
		}
		
		public function get exportItemLocationRptList():ArrayCollection {
			return _exportItemLocationRptList;
		}
	}
}