package hk.org.ha.event.pms.main.onestop {
		
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ClearOSEOrderDataFollowUpEvent extends AbstractTideEvent 
	{	
		public function ClearOSEOrderDataFollowUpEvent():void 
		{
			super();
		}        
	}
}