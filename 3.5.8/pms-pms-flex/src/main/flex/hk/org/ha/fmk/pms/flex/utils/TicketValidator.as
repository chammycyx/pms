package hk.org.ha.fmk.pms.flex.utils {
	public class TicketValidator {
		
		public static function isValidTicket(ticketNum:String):Boolean {
			var trunTicketNum:String = ticketNum.substring(2);
			var digit:int = 0;
			var totalSum:int = 0;
			var checkSum:String;
			
			for (var i:int=trunTicketNum.length; i > 0; i--) {
				var index:int = trunTicketNum.length - i;
				if (trunTicketNum.charCodeAt(index) >= 'A'.charCodeAt() && trunTicketNum.charCodeAt(index) <= 'Z'.charCodeAt()) {
					digit = 66 - trunTicketNum.charCodeAt(index);
				} else {
					digit = int (trunTicketNum.substring(index, index + 1));
				}
				
				totalSum = totalSum + digit * i;
			}
			
			checkSum = (trunTicketNum.length - (totalSum % trunTicketNum.length)).toString();
			
			if (ticketNum.substring(1,2) == checkSum.substring(checkSum.length -1, checkSum.length)) {
				return true;
			} else {
				return false;
			}
		}
	}
}