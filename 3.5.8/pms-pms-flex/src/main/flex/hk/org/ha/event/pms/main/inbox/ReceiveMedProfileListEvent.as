package hk.org.ha.event.pms.main.inbox
{
	import hk.org.ha.model.pms.udt.medprofile.FindMedProfileListResult;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ReceiveMedProfileListEvent extends AbstractTideEvent
	{
		private var _result:FindMedProfileListResult;
		
		public function ReceiveMedProfileListEvent(result:FindMedProfileListResult)
		{
			super();
			_result = result;
		}
		
		public function get result():FindMedProfileListResult
		{
			return _result;
		}
	}
}