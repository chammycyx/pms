package hk.org.ha.event.pms.main.reftable.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDueOrderScheduleTemplateMaintCopyPopupEvent extends AbstractTideEvent 
	{
		private var _dueOrderScheduleTemplateMaintCopyPopupProp:DueOrderScheduleTemplateMaintCopyPopupProp;
		
		public function ShowDueOrderScheduleTemplateMaintCopyPopupEvent(dueOrderScheduleTemplateMaintCopyPopupProp:DueOrderScheduleTemplateMaintCopyPopupProp):void 
		{
			super();
			_dueOrderScheduleTemplateMaintCopyPopupProp = dueOrderScheduleTemplateMaintCopyPopupProp;
		}
		
		public function get dueOrderScheduleTemplateMaintCopyPopupProp():DueOrderScheduleTemplateMaintCopyPopupProp {
			return _dueOrderScheduleTemplateMaintCopyPopupProp;
		}

	}
}