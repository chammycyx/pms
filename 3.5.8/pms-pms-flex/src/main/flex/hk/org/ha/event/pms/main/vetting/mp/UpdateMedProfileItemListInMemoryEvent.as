package hk.org.ha.event.pms.main.vetting.mp
{
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateMedProfileItemListInMemoryEvent extends AbstractTideEvent
	{
		private var _medProfileItemList:ListCollectionView;
		private var _callBackFunc:Function;
		
		public function UpdateMedProfileItemListInMemoryEvent(mpiListValue:ListCollectionView, callBackFuncValue:Function=null)
		{
			super();
			_medProfileItemList = mpiListValue;
			_callBackFunc = callBackFuncValue;
		}
		
		public function get medProfileItemList():ListCollectionView
		{
			return _medProfileItemList;
		}
		
		public function get callBackFunc():Function
		{
			return _callBackFunc;
		}
	}
}