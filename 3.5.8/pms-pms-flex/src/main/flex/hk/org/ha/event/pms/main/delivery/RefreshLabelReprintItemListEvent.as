package hk.org.ha.event.pms.main.delivery {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshLabelReprintItemListEvent extends AbstractTideEvent
	{
		public function RefreshLabelReprintItemListEvent():void 
		{
			super();
		}
	}
}