package hk.org.ha.control.pms.main.report {
	import hk.org.ha.event.pms.main.report.ExportRefillRptEvent;
	import hk.org.ha.event.pms.main.report.PrintRefillRptEvent;
	import hk.org.ha.event.pms.main.report.RefreshExportRefillRptEvent;
	import hk.org.ha.event.pms.main.report.RefreshRefillRptEvent;
	import hk.org.ha.event.pms.main.report.RetrieveRefillRptEvent;
	import hk.org.ha.model.pms.biz.report.RefillRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("refillRptServiceCtl", restrict="true")]
	public class RefillRptServiceCtl 
	{
		[In]
		public var refillRptService:RefillRptServiceBean;
		
		[Observer]
		public function retrieveRefillRpt(evt:RetrieveRefillRptEvent):void
		{
			In(Object(refillRptService).retrieveSuccess)
			In(Object(refillRptService).validItemCode)
			refillRptService.retrieveRefillRpt(evt.reportType, evt.workstoreCode, evt.refillDueDateFrom, evt.refillDueDateTo, evt.itemCode, evt.status, evt.drsPatientFlag, retrieveRefillItemRptResult);
		}
		
		private function retrieveRefillItemRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshRefillRptEvent(Object(refillRptService).retrieveSuccess, Object(refillRptService).validItemCode) );
		}
		
		[Observer]
		public function printRefillItemRpt(evt:PrintRefillRptEvent):void
		{
			refillRptService.printRefillRpt();
		}
		
		[Observer]
		public function exportRefillRpt(evt:ExportRefillRptEvent):void
		{
			refillRptService.setRefillRptPassword(evt.password, exportRefillRptResult);
		}
		
		private function exportRefillRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshExportRefillRptEvent(true) );
		}
		
	}
}
