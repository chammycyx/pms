package hk.org.ha.event.pms.main.reftable.mp {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ClearMpSpecMappingMaintViewListEvent extends AbstractTideEvent 
	{			
		public function ClearMpSpecMappingMaintViewListEvent():void 
		{
			super();
		}
	}
}