package hk.org.ha.event.pms.main.drug.popup {
		
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveMpCapdDrugSupplierEvent extends AbstractTideEvent 
	{			
		private var _drugSearchSource:DrugSearchSource;		// null if event is triggered by order edit
		
		public function RetrieveMpCapdDrugSupplierEvent(drugSearchSource:DrugSearchSource):void 
		{
			super();
			_drugSearchSource = drugSearchSource;
		}
		
		public function get drugSearchSource():DrugSearchSource {
			return _drugSearchSource;
		}
	}
}