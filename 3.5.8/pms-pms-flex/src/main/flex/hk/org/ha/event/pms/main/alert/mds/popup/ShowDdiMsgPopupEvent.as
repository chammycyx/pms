package hk.org.ha.event.pms.main.alert.mds.popup {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;
	
	public class ShowDdiMsgPopupEvent extends AbstractTideEvent {

		private var _alertMsg:AlertMsg;
		
		public var doNotPrescribeFunc:Function;
		
		public var prescribeAFunc:Function;
		
		public var prescribeBFunc:Function;
		
		public var overrideAlertFunc:Function;
		
		public var closeFunc:Function;
		
		public var onHandFlag:Boolean = false;
		
		public function ShowDdiMsgPopupEvent(alertMsg:AlertMsg) {
			super();
			_alertMsg = alertMsg;
		}
		
		public function get alertMsg():AlertMsg {
			return _alertMsg;
		}
	}
}
