package hk.org.ha.event.pms.main.drug {

	import hk.org.ha.model.pms.vo.rx.DhRxDrug;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class RetrieveWorkstoreDrugForDhOrderEditEvent extends AbstractTideEvent {

		private var _dhRxDrug:DhRxDrug;
		private var _itemCode:String;
		private var _callBack:Function;	

		public function RetrieveWorkstoreDrugForDhOrderEditEvent(dhRxDrug:DhRxDrug, itemCode:String, callBack:Function):void {
			super();
			_dhRxDrug    = dhRxDrug;			
			_itemCode    = itemCode;
			_callBack    = callBack;
		}

		public function get dhRxDrug():DhRxDrug {
			return _dhRxDrug;
		}

		public function get itemCode():String {
			return _itemCode;
		}
		
		public function get callBack():Function {
			return _callBack;
		}
	}
}
