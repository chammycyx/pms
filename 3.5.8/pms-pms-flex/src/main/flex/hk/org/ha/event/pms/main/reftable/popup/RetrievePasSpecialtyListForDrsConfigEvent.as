package hk.org.ha.event.pms.main.reftable.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.udt.patient.PasSpecialtyType;
	
	public class RetrievePasSpecialtyListForDrsConfigEvent extends AbstractTideEvent {	
		private var _prefixSpecCode:String;
		private var _specType:PasSpecialtyType;
		
		private var _callbackFunc:Function;
		
		public function RetrievePasSpecialtyListForDrsConfigEvent(prefixSpecCode:String, specType:PasSpecialtyType, callbackFunc:Function):void {
			super();
			_prefixSpecCode = prefixSpecCode;
			_specType = specType;
			_callbackFunc  = callbackFunc;
		}
		
		public function get prefixSpecCode():String {
			return _prefixSpecCode;
		}
		
		public function get specType():PasSpecialtyType {
			return _specType;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}