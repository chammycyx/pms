package hk.org.ha.event.pms.main.vetting
{
	import flash.events.Event;
	
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class CheckPharmRemarkItemEvent extends AbstractTideEvent 
	{
		private var _sourceAction:String = null;			// values: remove, convert
		private var _medOrderItem:MedOrderItem = null;		// the current medOrderItem in front-end 
		private var _medOrderSelectedIndex:int;				// selected index of current medOrderItem in front-end 		
		private var _okCallbackFunc:Function = null;		
		private var _cancelCallbackEvent:Event = null;		
		private var _drugConvertEvent:Event = null;			// used in item update case when there is no difference with original item
		private var _logonCancelCallbackFunc:Function = null;	// used when sourceAction  = convert	
		
		public function CheckPharmRemarkItemEvent():void
		{
			super();
		}
		
		public function get sourceAction():String {
			return _sourceAction;
		}
		
		public function set sourceAction(sourceAction:String):void {
			_sourceAction = sourceAction;
		}

		public function get medOrderItem():MedOrderItem {
			return _medOrderItem;
		}
		
		public function set medOrderItem(medOrderItem:MedOrderItem):void {
			_medOrderItem = medOrderItem;
		}
		
		public function get medOrderSelectedIndex():int {
			return _medOrderSelectedIndex;
		}
		
		public function set medOrderSelectedIndex(medOrderSelectedIndex:int):void {
			_medOrderSelectedIndex = medOrderSelectedIndex;
		}
		
		public function get okCallbackFunc():Function {
			return _okCallbackFunc;
		}
		
		public function set okCallbackFunc(okCallbackFunc:Function):void {
			_okCallbackFunc = okCallbackFunc;
		}	

		public function get cancelCallbackEvent():Event {
			return _cancelCallbackEvent;
		}
		
		public function set cancelCallbackEvent(cancelCallbackEvent:Event):void {
			_cancelCallbackEvent = cancelCallbackEvent;
		}
		
		public function get drugConvertEvent():Event {
			return _drugConvertEvent;
		}
		
		public function set drugConvertEvent(drugConvertEvent:Event):void {
			_drugConvertEvent = drugConvertEvent;
		}
		
		public function get logonCancelCallbackFunc():Function {
			return _logonCancelCallbackFunc;
		}
		
		public function set logonCancelCallbackFunc(logonCancelCallbackFunc:Function):void {
			_logonCancelCallbackFunc = logonCancelCallbackFunc;
		}
	}
}