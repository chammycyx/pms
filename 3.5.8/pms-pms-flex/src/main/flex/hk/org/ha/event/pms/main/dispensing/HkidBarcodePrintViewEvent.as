package hk.org.ha.event.pms.main.dispensing {
	
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.label.HkidBarcodeLabel;

	public class HkidBarcodePrintViewEvent extends AbstractTideEvent 
	{
		private var _hkidBarcodeLabel:HkidBarcodeLabel;
				
		public function HkidBarcodePrintViewEvent(hkidBarcodeLabel:HkidBarcodeLabel):void {
			super();
			_hkidBarcodeLabel = hkidBarcodeLabel;	
		}
		
		public function get hkidBarcodeLabel():HkidBarcodeLabel
		{
			return _hkidBarcodeLabel;
		}
		
	}
}