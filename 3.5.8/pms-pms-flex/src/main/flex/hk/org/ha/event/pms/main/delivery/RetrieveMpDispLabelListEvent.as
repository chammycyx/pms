package hk.org.ha.event.pms.main.delivery {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveMpDispLabelListEvent extends AbstractTideEvent 
	{
		private var _batchNum:String;
		private var _dispenseDate:Date;
		private var _caseNum:String;
		private var _trxId:String;

		public function RetrieveMpDispLabelListEvent(batchNum:String, dispenseDate:Date, caseNum:String, trxId:String):void 
		{
			super();
			_batchNum = batchNum;
			_dispenseDate = dispenseDate;
			_caseNum = caseNum;
			_trxId = trxId;
		}
		
		public function get batchNum():String{
			return _batchNum;
		} 
		
		public function get dispenseDate():Date{
			return _dispenseDate;
		}
		
		public function get caseNum():String{
			return _caseNum;
		}
		
		public function get trxId():String{
			return _trxId;
		}
	}
}