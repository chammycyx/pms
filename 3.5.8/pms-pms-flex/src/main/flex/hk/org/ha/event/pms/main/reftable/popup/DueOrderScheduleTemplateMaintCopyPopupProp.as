package hk.org.ha.event.pms.main.reftable.popup
{
	import mx.collections.ArrayCollection;

	public class DueOrderScheduleTemplateMaintCopyPopupProp 
	{
		public var copyFromDeliveryScheduleList:ArrayCollection;
		public var okHandler:Function;
	}
}
