package hk.org.ha.event.pms.main.alert.mds {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	import mx.collections.ListCollectionView;
	
	public class EditPrescReasonEvent extends AbstractTideEvent {
		
		private var _overrideReason:ListCollectionView;
		
		private var _alertIndex:Number;
		
		private var _callbackFunc:Function;
		
		public function EditPrescReasonEvent(overrideReason:ListCollectionView, alertIndex:Number, callbackFunc:Function=null) {
			super();
			_overrideReason = overrideReason;
			_alertIndex = alertIndex;
			_callbackFunc = callbackFunc;
		}
		
		public function get overrideReason():ListCollectionView {
			return _overrideReason;
		}
		
		public function get alertIndex():Number {
			return _alertIndex;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}