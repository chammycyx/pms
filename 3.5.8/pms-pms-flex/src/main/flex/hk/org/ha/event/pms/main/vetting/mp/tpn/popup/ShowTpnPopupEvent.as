package hk.org.ha.event.pms.main.vetting.mp.tpn.popup
{
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowTpnPopupEvent extends AbstractTideEvent
	{
		
		private var _readOnlyFlag:Boolean;
		
		public function ShowTpnPopupEvent(readOnlyFlag:Boolean)
		{
			super();
			_readOnlyFlag = readOnlyFlag;
		}
		
		public function get readOnlyFlag():Boolean {
			return _readOnlyFlag;
		}
	}
}