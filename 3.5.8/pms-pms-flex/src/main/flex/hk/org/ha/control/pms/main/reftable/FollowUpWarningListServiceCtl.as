package hk.org.ha.control.pms.main.reftable {
	
	import hk.org.ha.event.pms.main.reftable.ClearFollowUpWarningListEvent;
	import hk.org.ha.event.pms.main.reftable.PrintFollowUpWarningRptEvent;
	import hk.org.ha.event.pms.main.reftable.RetrieveFollowUpWarningListEvent;
	import hk.org.ha.event.pms.main.reftable.UpdateFollowUpWarningListEvent;
	import hk.org.ha.model.pms.biz.reftable.FollowUpWarningListServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("followUpWarningListServiceCtl", restrict="true")]
	public class FollowUpWarningListServiceCtl 
	{
		[In]
		public var followUpWarningListService:FollowUpWarningListServiceBean;
		
		[Observer]
		public function retrieveFollowUpWarningList(evt:RetrieveFollowUpWarningListEvent):void
		{
			followUpWarningListService.retrieveFollowUpWarningList();
		}
		
		[Observer]
		public function updateFollowUpWarningList(evt:UpdateFollowUpWarningListEvent):void
		{
			followUpWarningListService.updateFollowUpWarningList(evt.updateFollowUpWarningList, updateFollowUpWarningListResult);
		}
		
		private function updateFollowUpWarningListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new ClearFollowUpWarningListEvent());
		}
		
		[Observer]
		public function printFollowUpWarningRpt(evt:PrintFollowUpWarningRptEvent):void
		{
			followUpWarningListService.printFollowUpWarningRpt();
		}
	}
}
