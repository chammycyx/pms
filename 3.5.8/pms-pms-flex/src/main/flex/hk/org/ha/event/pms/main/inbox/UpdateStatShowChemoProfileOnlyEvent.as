package hk.org.ha.event.pms.main.inbox
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateStatShowChemoProfileOnlyEvent extends AbstractTideEvent
	{
		private var _showChemoProfileOnly:Boolean;
		
		public function UpdateStatShowChemoProfileOnlyEvent(showChemoProfileOnly:Boolean)
		{
			super();
			_showChemoProfileOnly = showChemoProfileOnly;
		}
		
		public function get showChemoProfileOnly():Boolean
		{
			return _showChemoProfileOnly;
		}
	}
}