package hk.org.ha.control.pms.main.report {
	import hk.org.ha.event.pms.main.report.RetrieveDnldTrxFileListEvent;
	import hk.org.ha.event.pms.main.report.DnldDailyTrxFileEvent;
	import hk.org.ha.event.pms.main.report.DnldMonthlyArchiveFileEvent;
	import hk.org.ha.event.pms.main.report.RefreshDnldTrxFileEvent;
	import hk.org.ha.model.pms.biz.report.DnldTrxFileServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("dnldTrxFileServiceCtl", restrict="true")]
	public class DnldTrxFileServiceCtl 
	{
		[In]
		public var dnldTrxFileService:DnldTrxFileServiceBean;
		
		[Observer]
		public function retrieveDnldTrxFileList(evt:RetrieveDnldTrxFileListEvent):void
		{
			dnldTrxFileService.retrieveDnldTrxFileList();
		}
		
		[Observer]
		public function dnldDailyTrxFile(evt:DnldDailyTrxFileEvent):void
		{
			dnldTrxFileService.dnldDailyTrxFile(evt.fileName, evt.password, dnldDailyTrxFileResult);
		}
		
		private function dnldDailyTrxFileResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshDnldTrxFileEvent());
		}
		
		[Observer]
		public function dnldMonthlyArchiveFile(evt:DnldMonthlyArchiveFileEvent):void
		{
			dnldTrxFileService.dnldMonthlyArchiveFile(evt.fileName, evt.password, dnldMonthlyArchiveFileResult);
		}
		
		private function dnldMonthlyArchiveFileResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshDnldTrxFileEvent());
		}
		
	}
}
