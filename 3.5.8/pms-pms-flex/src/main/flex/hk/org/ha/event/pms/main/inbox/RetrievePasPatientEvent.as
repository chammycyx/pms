package hk.org.ha.event.pms.main.inbox
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePasPatientEvent extends AbstractTideEvent
	{
		private var _hkid:String;
		
		public function RetrievePasPatientEvent(hkid:String)
		{
			super();
			_hkid = hkid;
		}
		
		public function get hkid():String
		{
			return _hkid;
		}
	}
}