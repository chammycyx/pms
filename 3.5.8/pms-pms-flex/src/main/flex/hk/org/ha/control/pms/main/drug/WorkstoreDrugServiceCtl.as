package hk.org.ha.control.pms.main.drug {
	
	import hk.org.ha.event.pms.main.drug.RetrieveWorkstoreDrugEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveWorkstoreDrugForDhOrderEditEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveWorkstoreDrugForMpOrderEditEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveWorkstoreDrugForOrderEditEvent;
	import hk.org.ha.model.pms.biz.drug.WorkstoreDrugServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("WorkstoreDrugServiceCtl", restrict="true")]
	public class WorkstoreDrugServiceCtl 
	{
		[In]
		public var workstoreDrugService:WorkstoreDrugServiceBean;		
		[In]
		public var ctx:Context;
		
		private var callBackFunction:Function;		
		private var callBackFunctionForOrderEdit:Function;
		
		[Observer]
		public function retrieveWorkstoreDrugForOrderEdit(evt:RetrieveWorkstoreDrugForOrderEditEvent):void
		{
			callBackFunctionForOrderEdit = evt.callBackFunction;
			workstoreDrugService.retrieveWorkstoreDrugForOrderEdit(evt.displayName, evt.itemCode, retrieveWorkstoreDrugForOrderEditResult);
		}
		
		[Observer]
		public function retrieveWorkstoreDrugForMpOrderEdit(evt:RetrieveWorkstoreDrugForMpOrderEditEvent):void
		{
			callBackFunctionForOrderEdit = evt.callBackFunction;
			workstoreDrugService.retrieveWorkstoreDrugForMpOrderEdit(evt.rxItem, evt.itemCode, retrieveWorkstoreDrugForOrderEditResult);
		}
		
		[Observer]
		public function retrieveWorkstoreDrugForDhOrderEdit(evt:RetrieveWorkstoreDrugForDhOrderEditEvent):void {
			callBackFunctionForOrderEdit = evt.callBack;
			workstoreDrugService.retrieveWorkstoreDrugForDhOrderEdit(evt.dhRxDrug, evt.itemCode, retrieveWorkstoreDrugForOrderEditResult);
		}
		
		private function retrieveWorkstoreDrugForOrderEditResult(evt:TideResultEvent):void 
		{
			if ( callBackFunctionForOrderEdit != null ) {
				callBackFunctionForOrderEdit(evt.result);
			}			
		}
		
		[Observer]
		public function retrieveWorkstoreDrug(evt:RetrieveWorkstoreDrugEvent):void
		{
			callBackFunction = evt.callBackFunc;
			workstoreDrugService.retrieveWorkstoreDrug(evt.itemCode, retrieveWorkstoreDrugResult);
		}
		
		private function retrieveWorkstoreDrugResult(evt:TideResultEvent):void 
		{
			if ( callBackFunction!=null ) {
				callBackFunction(evt.result);
			}
			
		}
		
	}
}
