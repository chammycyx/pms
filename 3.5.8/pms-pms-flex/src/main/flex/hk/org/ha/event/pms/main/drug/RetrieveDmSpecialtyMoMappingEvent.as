package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDmSpecialtyMoMappingEvent extends AbstractTideEvent 
	{
		private var _specialtyCode:String;
		private var _successEvent:Event;
		private var _failEvent:Event;
		
		public function RetrieveDmSpecialtyMoMappingEvent(specialtyCode:String, successEvent:Event=null, failEvent:Event=null):void 
		{
			super();
			_specialtyCode = specialtyCode;
			_successEvent = successEvent;
			_failEvent = failEvent;
		}
		
		public function get specialtyCode():String
		{
			return _specialtyCode;
		}
		
		public function get successEvent():Event
		{
			return _successEvent;
		}
		
		public function get failEvent():Event
		{
			return _failEvent;
		}
	}
}