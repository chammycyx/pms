package hk.org.ha.event.pms.main.reftable {
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class InitRefillQtyAdjExclusionTabEvent extends AbstractTideEvent 
	{		
		private var _list:ArrayCollection;
		
		public function InitRefillQtyAdjExclusionTabEvent(list:ArrayCollection):void 
		{
			super();
			_list = list;
		}
		
		public function get list():ArrayCollection {
			return _list;
		}
	}
}