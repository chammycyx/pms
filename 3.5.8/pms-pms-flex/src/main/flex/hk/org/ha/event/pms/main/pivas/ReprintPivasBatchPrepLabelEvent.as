package hk.org.ha.event.pms.main.pivas
{
	import hk.org.ha.model.pms.persistence.pivas.PivasBatchPrep;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ReprintPivasBatchPrepLabelEvent extends AbstractTideEvent 
	{		
		private var _pivasBatchPrep:PivasBatchPrep;
		
		public function ReprintPivasBatchPrepLabelEvent(pivasBatchPrep:PivasBatchPrep):void 
		{
			super();
			_pivasBatchPrep = pivasBatchPrep;
		}
		
		public function get pivasBatchPrep():PivasBatchPrep {
			return _pivasBatchPrep;
		}
	}
}