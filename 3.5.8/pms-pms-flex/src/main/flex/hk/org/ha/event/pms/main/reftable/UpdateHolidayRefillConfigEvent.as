package hk.org.ha.event.pms.main.reftable {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateHolidayRefillConfigEvent extends AbstractTideEvent 
	{		
		private var _enableAdjHolidayFlag:Boolean;
		private var _enableAdjSundayFlag:Boolean;
		private var _nextTabIndex:Number;
		
		public function UpdateHolidayRefillConfigEvent(enableAdjHolidayFlag:Boolean, 
													   enableAdjSundayFlag:Boolean, 
													   nextTabIndex:Number):void 
		{
			super();
			_enableAdjHolidayFlag = enableAdjHolidayFlag;
			_enableAdjSundayFlag = enableAdjSundayFlag;
			_nextTabIndex = nextTabIndex;
		}
		
		public function get enableAdjHolidayFlag():Boolean
		{
			return _enableAdjHolidayFlag;
		}
		
		public function get enableAdjSundayFlag():Boolean
		{
			return _enableAdjSundayFlag;
		}
		
		public function get nextTabIndex():Number
		{
			return _nextTabIndex;
		}
	}
}