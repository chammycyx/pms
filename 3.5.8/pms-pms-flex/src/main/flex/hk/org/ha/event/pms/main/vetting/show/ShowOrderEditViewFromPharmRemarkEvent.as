package hk.org.ha.event.pms.main.vetting.show {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowOrderEditViewFromPharmRemarkEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		private var _callBackEvent:Event;
		
		private var _infoString:String = null;
		
		public function ShowOrderEditViewFromPharmRemarkEvent(eventValue:Event=null, clearMessages:Boolean=true):void 
		{
			super();
			_callBackEvent = eventValue;
			_clearMessages = clearMessages;
		}
		
		public function get callBackEvent():Event
		{
			return _callBackEvent;
		}
		
		public function get clearMessages():Boolean 
		{
			return _clearMessages;
		}
		
		public function get infoString():String {
			return _infoString;
		}
		
		public function set infoString(infoString:String):void {
			_infoString = infoString;
		}
		
		public override function clone():Event {
			return new ShowOrderEditViewFromPharmRemarkEvent(callBackEvent, clearMessages);
		}
	}
}