package hk.org.ha.event.pms.main.reftable {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetRefillConfigFocusEvent extends AbstractTideEvent 
	{
		private var _checkRefillQtyAdjTabEnabled:Boolean;
		private var _setEnableQtyAdjFlag:Boolean;
		private var _setHolidayListConfig:Boolean;
		
		public function SetRefillConfigFocusEvent(checkRefillQtyAdjTabEnabled:Boolean, setEnableQtyAdjFlag:Boolean, setHolidayListConfig:Boolean):void 
		{
			super();
			_checkRefillQtyAdjTabEnabled = checkRefillQtyAdjTabEnabled;
			_setEnableQtyAdjFlag = setEnableQtyAdjFlag;
			_setHolidayListConfig = setHolidayListConfig;
		}
		
		public function get checkRefillQtyAdjTabEnabled():Boolean {
			return _checkRefillQtyAdjTabEnabled;
		}
		
		public function get setEnableQtyAdjFlag():Boolean {
			return _setEnableQtyAdjFlag;
		}
		
		public function get setHolidayListConfig():Boolean {
			return _setHolidayListConfig;
		}

	}
}