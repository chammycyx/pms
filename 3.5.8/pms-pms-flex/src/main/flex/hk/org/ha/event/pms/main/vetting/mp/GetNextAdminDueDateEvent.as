package hk.org.ha.event.pms.main.vetting.mp {

	import mx.collections.ListCollectionView;
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class GetNextAdminDueDateEvent extends AbstractTideEvent {											
		
		private var _dailyFreqCodeList:ListCollectionView;
		private var _pivasFlag:Boolean;
		private var _callBackFunc:Function;
		
		public function GetNextAdminDueDateEvent(dailyFreqCodeListValue:ListCollectionView, pivasFlag:Boolean, callBackFuncValue:Function) {
			super();			
			_dailyFreqCodeList = dailyFreqCodeListValue;
			_pivasFlag = pivasFlag;
			_callBackFunc = callBackFuncValue;
		}
		
		public function get dailyFreqCodeList():ListCollectionView {
			return _dailyFreqCodeList;
		}
		
		public function get pivasFlag():Boolean {
			return _pivasFlag;
		}
		
		public function get callBackFunc():Function {
			return _callBackFunc;
		}
	}
}