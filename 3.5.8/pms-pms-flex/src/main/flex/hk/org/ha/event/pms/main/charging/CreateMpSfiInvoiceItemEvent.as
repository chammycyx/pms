package hk.org.ha.event.pms.main.charging {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.charging.MpSfiInvoiceItem;
	
	public class CreateMpSfiInvoiceItemEvent extends AbstractTideEvent 
	{	
		private var _itemCode:String;
		private var _selectedItem:MpSfiInvoiceItem;
		
		public function CreateMpSfiInvoiceItemEvent(itemCode:String, selectedItem:MpSfiInvoiceItem):void 
		{
			super();
			_itemCode = itemCode;
			_selectedItem = selectedItem;
		}
		
		public function get itemCode():String {
			return _itemCode;
		}
		
		public function get selectedItem():MpSfiInvoiceItem {
			return _selectedItem;
		}
	}
}