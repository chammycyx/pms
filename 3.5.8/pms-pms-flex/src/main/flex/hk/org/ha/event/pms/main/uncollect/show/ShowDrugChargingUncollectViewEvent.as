package hk.org.ha.event.pms.main.uncollect.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDrugChargingUncollectViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		public function ShowDrugChargingUncollectViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}