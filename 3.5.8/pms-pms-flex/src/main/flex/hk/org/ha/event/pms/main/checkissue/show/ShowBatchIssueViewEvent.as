package hk.org.ha.event.pms.main.checkissue.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowBatchIssueViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		public function ShowBatchIssueViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}