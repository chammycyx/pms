package hk.org.ha.event.pms.main.onestop
{
	import flash.events.Event;
	
	import hk.org.ha.view.pms.main.onestop.popup.OneStopEntryPrescriptionSummaryPopup;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePrescriptionDispOrderEvent extends AbstractTideEvent 
	{		
		private var _dispDate:Date;
		private var _ticketNum:String;
		private var _workstoreCode:String;
		private var _oneStopEntryPrescriptionSummaryPopup:OneStopEntryPrescriptionSummaryPopup;

		public function RetrievePrescriptionDispOrderEvent(dispDate:Date, ticketNum:String, workstoreCode:String, 
														   oneStopEntryPrescriptionSummaryPopup:OneStopEntryPrescriptionSummaryPopup):void 
		{
			super();
			_dispDate = dispDate;
			_ticketNum = ticketNum;
			_workstoreCode = workstoreCode;
			_oneStopEntryPrescriptionSummaryPopup = oneStopEntryPrescriptionSummaryPopup;
		}
		
		public function get dispDate():Date {
			return _dispDate;
		}
		
		public function get ticketNum():String {
			return _ticketNum;
		}
		
		public function get workstoreCode():String {
			return _workstoreCode;
		}
		
		public function get oneStopEntryPrescriptionSummaryPopup():OneStopEntryPrescriptionSummaryPopup {
			return _oneStopEntryPrescriptionSummaryPopup;
		}
	}
}