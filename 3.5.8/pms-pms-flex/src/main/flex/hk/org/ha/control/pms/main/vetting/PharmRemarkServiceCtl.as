package hk.org.ha.control.pms.main.vetting
{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.prevet.ShowPreVetMsgEvent;
	import hk.org.ha.event.pms.main.vetting.CheckPharmRemarkItemEvent;
	import hk.org.ha.event.pms.main.vetting.DrugConvertEvent;
	import hk.org.ha.event.pms.main.vetting.RetrievePharmRemarkDateItemNumEvent;
	import hk.org.ha.event.pms.main.vetting.RetrievePharmRemarkItemEvent;
	import hk.org.ha.event.pms.main.vetting.popup.AddPharmRemarkOrderEvent;
	import hk.org.ha.event.pms.main.vetting.popup.RefreshPharmRemarkLogonPopupEvent;
	import hk.org.ha.event.pms.main.vetting.popup.ShowPharmRemarkLogonPopupEvent;
	import hk.org.ha.event.pms.main.vetting.popup.ShowPharmRemarkOrderPopupEvent;
	import hk.org.ha.event.pms.main.vetting.popup.ValidatePharmRemarkLogonEvent;
	import hk.org.ha.event.pms.main.vetting.show.ShowOrderViewEvent;
	import hk.org.ha.event.pms.main.vetting.show.ShowPharmRemarkItemViewEvent;
	import hk.org.ha.fmk.pms.security.UamInfo;
	import hk.org.ha.fmk.pms.sys.SysProfile;
	import hk.org.ha.model.pms.biz.vetting.PharmRemarkServiceBean;
	import hk.org.ha.model.pms.persistence.disp.MedOrder;
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	import hk.org.ha.model.pms.persistence.disp.MedOrderItemAlert;
	import hk.org.ha.model.pms.persistence.disp.PharmOrder;
	import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
	import hk.org.ha.model.pms.udt.disp.RemarkItemStatus;
	import hk.org.ha.model.pms.udt.disp.RemarkStatus;
	import hk.org.ha.model.pms.vo.rx.Dose;
	import hk.org.ha.model.pms.vo.rx.DoseGroup;
	import hk.org.ha.model.pms.vo.rx.Regimen;
	import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;
	
	import mx.collections.ArrayCollection;
	import mx.controls.AdvancedDataGrid;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("pharmRemarkServiceCtl", restrict="true")]
	public class PharmRemarkServiceCtl
	{
		[In]
		public var ctx:Context;
		
		[In]
		public var pharmRemarkService:PharmRemarkServiceBean; 
		
		[In]
		public var uamInfo:UamInfo;
		
		[In]
		public var sysProfile:SysProfile;
		
		[In]
		public var orderViewInfo:OrderViewInfo;		// from vetting screen
		
		[In]
		public var pharmOrder:PharmOrder;	// pharmOrder from order edit screen
		
		[In]
		public var pharmRemarkRight:Boolean;	
		
		[In]
		public var pharmRemarkExist:Boolean;	// only used in item level remark
		
		[In]
		public var pharmRemarkModifyFallback:Boolean;	// only used in item level remark
		
		[In]
		public var pharmRemarkItemNum:int;	// only used in item level remark
		
		[In]
		public var prevMedOrderItemRemark:MedOrderItem;
		
		[In]
		public var pharmRemarkLogonFailMsg:String;
		
		private var sourceAction:String;
		
		private var currentMedOrderItem:MedOrderItem;
		
		private var medOrderSelectedIndex:int;
		
		private var okCallbackFunc:Function;
		
		private var cancelCallbackEvent:Event;
		
		private var drugConvertEvent:Event;		
		
		private var logonCancelCallbackFunc:Function;
		
		private var updateRemarkSource:String;
		
		private var reverseRemarkCallbackFunc:Function;
		
		private var remarkDateItemNumCallbackFunc:Function;
		
		private var logonCallbackEvent:Event;
		
		private var medOrder:MedOrder;
		
		private var logonUser:String;
		
		[Observer]
		public function addPharmRemarkOrder(evt:AddPharmRemarkOrderEvent):void
		{
			cancelCallbackEvent = evt.cancelCallbackEvent;
			medOrder = evt.medOrder;
			pharmRemarkService.addPharmRemarkOrder(addPharmRemarkOrderResult);
		}
		
		private function addPharmRemarkOrderResult(evt:TideResultEvent):void
		{
			var showPharmRemarkOrderPopupEvent:ShowPharmRemarkOrderPopupEvent = new ShowPharmRemarkOrderPopupEvent();
			showPharmRemarkOrderPopupEvent.cancelCallbackEvent = cancelCallbackEvent;
			showPharmRemarkOrderPopupEvent.medOrder = medOrder;
			
			if ( sysProfile.debugEnabled || (cancelCallbackEvent is ShowOrderViewEvent && orderViewInfo.pharmRemarkLogonUser != null) || 
				(cancelCallbackEvent is ShowPreVetMsgEvent && ctx.deleteRxAuthUser != "") || pharmRemarkRight ) {
				
				if (cancelCallbackEvent is ShowOrderViewEvent) {
					// triggered from vetting
					if (orderViewInfo.pharmRemarkLogonUser != null) {
						showPharmRemarkOrderPopupEvent.logonUser = orderViewInfo.pharmRemarkLogonUser;
					} else {
						showPharmRemarkOrderPopupEvent.logonUser = uamInfo.userId;
					}
				} else if (cancelCallbackEvent is ShowPreVetMsgEvent) {
					// triggered from prevet
					if (ctx.deleteRxAuthUser != "") {
						showPharmRemarkOrderPopupEvent.logonUser = ctx.deleteRxAuthUser as String;
					} else {
						showPharmRemarkOrderPopupEvent.logonUser = uamInfo.userId;
					}
				}
				
				dispatchEvent(showPharmRemarkOrderPopupEvent);
				
			} else {
				dispatchEvent(new ShowPharmRemarkLogonPopupEvent(showPharmRemarkOrderPopupEvent, "Pharmacy Remarks", null));
			}
		}
		
		[Observer]
		public function checkPharmRemarkItem(evt:CheckPharmRemarkItemEvent):void
		{
			var medOrderItemCopy:MedOrderItem = null;
			
			sourceAction = evt.sourceAction;
			currentMedOrderItem = evt.medOrderItem;
			
			// copy MedOrderItem to disconnect pharmOrderItem 
			medOrderItemCopy = new MedOrderItem();
			copyPropertiesMedOrderItem(evt.medOrderItem, medOrderItemCopy);
			
			medOrderSelectedIndex = evt.medOrderSelectedIndex;				
			okCallbackFunc = evt.okCallbackFunc;
			cancelCallbackEvent = evt.cancelCallbackEvent;
			drugConvertEvent = evt.drugConvertEvent;
			logonCancelCallbackFunc = evt.logonCancelCallbackFunc;
			
			pharmRemarkService.checkPharmRemarkItem(sourceAction, medOrderSelectedIndex, medOrderItemCopy, checkPharmRemarkItemResult);
		}
		
		private function checkPharmRemarkItemResult(evt:TideResultEvent):void 
		{
			if (pharmRemarkExist) {
				var showPharmRemarkItemViewEvent:ShowPharmRemarkItemViewEvent = new ShowPharmRemarkItemViewEvent();
				showPharmRemarkItemViewEvent.clearScreen = true;
				showPharmRemarkItemViewEvent.isAddRemark = true;
				showPharmRemarkItemViewEvent.addRemarkAction = sourceAction;
				showPharmRemarkItemViewEvent.currentMedOrderItem = currentMedOrderItem;					
				showPharmRemarkItemViewEvent.okCallbackFunc = okCallbackFunc;
				showPharmRemarkItemViewEvent.cancelCallbackEvent = cancelCallbackEvent;
				
				if ( sysProfile.debugEnabled || orderViewInfo.pharmRemarkLogonUser != null || pharmRemarkRight ||
					! orderViewInfo.allowEditFlag) {
					if (orderViewInfo.pharmRemarkLogonUser != null) {
						showPharmRemarkItemViewEvent.logonUser = orderViewInfo.pharmRemarkLogonUser;
					} else {
						showPharmRemarkItemViewEvent.logonUser = uamInfo.userId;
					}
					dispatchEvent(showPharmRemarkItemViewEvent);
				} else {
					try {
						if ( ctx.orderEditView != null ) {
							ctx.orderEditView.disableProcessFlag();
						}
					} catch (e:Error) {
						trace(e.getStackTrace());
					}
					dispatchEvent(new ShowPharmRemarkLogonPopupEvent(showPharmRemarkItemViewEvent, "Pharmacy Remarks", logonCancelCallbackFunc));
				}
				
			} else {
				if (pharmRemarkModifyFallback) {
					// reverse item modify remark
					
					// mark the item being modified
					currentMedOrderItem.changeFlag = true;
					
					// update new item number
					currentMedOrderItem.itemNum = pharmRemarkItemNum;
					
					// fallback remark
					// (remark status of previous item can be unconfirmed if the item is deleted
					//  and save, and then reverse delete and modify and then save, in this case
					//  also mark the new fallback item to no remark)
					currentMedOrderItem.remarkItemStatus = RemarkItemStatus.NoRemark;
					
					// init remark details
					currentMedOrderItem.prevMoItemId = Number.NaN;
					currentMedOrderItem.remarkCreateDate = null;
					currentMedOrderItem.remarkCreateUser = null;
					currentMedOrderItem.remarkText = null;
					currentMedOrderItem.remarkConfirmDate = null;
					currentMedOrderItem.remarkConfirmUser = null;
				}
				
				dispatchEvent(drugConvertEvent);
			}
		}
		
		[Observer]
		public function retrievePharmRemarkItem(evt:RetrievePharmRemarkItemEvent):void
		{			
			updateRemarkSource = evt.updateRemarkSource;
			currentMedOrderItem = evt.medOrderItem;			
			okCallbackFunc = evt.okCallbackFunc;
			cancelCallbackEvent = evt.cancelCallbackEvent;
			reverseRemarkCallbackFunc = evt.reverseRemarkCallbackFunc;
			
			pharmRemarkService.retrievePharmRemarkItem(sourceAction, evt.medOrderItem, retrievePharmRemarkItemResult);
		}
		
		private function retrievePharmRemarkItemResult(evt:TideResultEvent):void 
		{
			var showPharmRemarkItemViewEvent:ShowPharmRemarkItemViewEvent = new ShowPharmRemarkItemViewEvent();
			showPharmRemarkItemViewEvent.clearScreen = true;
			showPharmRemarkItemViewEvent.isAddRemark = false;
			showPharmRemarkItemViewEvent.updateRemarkSource = updateRemarkSource;
			showPharmRemarkItemViewEvent.currentMedOrderItem = currentMedOrderItem;			
			showPharmRemarkItemViewEvent.okCallbackFunc = okCallbackFunc;
			showPharmRemarkItemViewEvent.cancelCallbackEvent = cancelCallbackEvent;
			showPharmRemarkItemViewEvent.reverseRemarkCallbackFunc = reverseRemarkCallbackFunc;
			
			if ( sysProfile.debugEnabled || orderViewInfo.pharmRemarkLogonUser != null || pharmRemarkRight ||
				! orderViewInfo.allowEditFlag || currentMedOrderItem.remarkStatus == RemarkStatus.Confirm) {
				if (orderViewInfo.pharmRemarkLogonUser != null) {
					showPharmRemarkItemViewEvent.logonUser = orderViewInfo.pharmRemarkLogonUser;
				} else {
					showPharmRemarkItemViewEvent.logonUser = uamInfo.userId;
				}
				dispatchEvent(showPharmRemarkItemViewEvent);
			} else {
				dispatchEvent(new ShowPharmRemarkLogonPopupEvent(showPharmRemarkItemViewEvent, "Pharmacy Remarks"));
			}
		}
		
		[Observer]
		public function validatePharmRemarkLogon(evt:ValidatePharmRemarkLogonEvent):void
		{
			logonUser = evt.logonUser;
			logonCallbackEvent = evt.logonCallbackEvent;
			pharmRemarkService.validatePharmRemarkLogon(evt.logonUser, evt.logonPw, validatePharmRemarkLogonResult);
		}
		
		private function validatePharmRemarkLogonResult(evt:TideResultEvent):void 
		{
			if (pharmRemarkLogonFailMsg == null) {
				if (logonCallbackEvent is ShowPharmRemarkOrderPopupEvent) {
					// delete order remark
					var showPharmRemarkOrderPopupEvent:ShowPharmRemarkOrderPopupEvent = logonCallbackEvent as ShowPharmRemarkOrderPopupEvent;
					if (showPharmRemarkOrderPopupEvent.cancelCallbackEvent is ShowOrderViewEvent) {
						orderViewInfo.pharmRemarkLogonUser = logonUser;
					} else if (showPharmRemarkOrderPopupEvent.cancelCallbackEvent is ShowPreVetMsgEvent) {
						ctx.deleteRxAuthUser = logonUser;
					}
				} else {
					// item remark
					orderViewInfo.pharmRemarkLogonUser = logonUser;
				}
			}
			
			dispatchEvent(new RefreshPharmRemarkLogonPopupEvent(new Array(pharmRemarkLogonFailMsg), logonCallbackEvent));			
		}
		
		[Observer]
		public function retrievePharmRemarkDateItemNum(evt:RetrievePharmRemarkDateItemNumEvent):void
		{
			remarkDateItemNumCallbackFunc = evt.callbackFunc;
			pharmRemarkService.retrievePharmRemarkDateItemNum(evt.itemNumRequired, retrievePharmRemarkDateItemNumResult);
		}
		
		private function retrievePharmRemarkDateItemNumResult(evt:TideResultEvent):void 
		{
			remarkDateItemNumCallbackFunc();
		}		
		
		private function copyPropertiesMedOrderItem(srcMoi:MedOrderItem, targetMoi:MedOrderItem):void {
			// copy MedOrderItem to disconnect pharmOrderItem 
			targetMoi.id = srcMoi.id;
			targetMoi.itemNum = srcMoi.itemNum;
			targetMoi.orgItemNum = srcMoi.orgItemNum;
			targetMoi.printSeq = srcMoi.printSeq;
			targetMoi.rxItemType = srcMoi.rxItemType;
			targetMoi.status = srcMoi.status;
			targetMoi.prevMoItemId = srcMoi.prevMoItemId;
			targetMoi.numOfLabel = srcMoi.numOfLabel;
			targetMoi.commentText = srcMoi.commentText;
			targetMoi.commentUser = srcMoi.commentUser;
			targetMoi.commentDate = srcMoi.commentDate;
			targetMoi.unitOfCharge = srcMoi.unitOfCharge;
			targetMoi.ppmiFlag = srcMoi.ppmiFlag;
			targetMoi.chargeFlag = srcMoi.chargeFlag;
			targetMoi.remarkItemStatus = srcMoi.remarkItemStatus;
			targetMoi.action = srcMoi.action;
			targetMoi.showWarnCodeFlag = srcMoi.showWarnCodeFlag;
			targetMoi.showExemptCodeFlag = srcMoi.showExemptCodeFlag;
			targetMoi.vetFlag = srcMoi.vetFlag;
			targetMoi.changeFlag = srcMoi.changeFlag;
			targetMoi.groupEndFlag = srcMoi.groupEndFlag;
			targetMoi.fmIndFlag = srcMoi.fmIndFlag;
			targetMoi.dosageCompulsory = srcMoi.dosageCompulsory;
			targetMoi.strengthCompulsory = srcMoi.strengthCompulsory;
			targetMoi.seqNum = srcMoi.seqNum;
			
			targetMoi.msFmStatus = srcMoi.msFmStatus;
			
			targetMoi.remarkConfirmDate = srcMoi.remarkConfirmDate;
			targetMoi.remarkConfirmUser = srcMoi.remarkConfirmUser;
			targetMoi.remarkCreateDate = srcMoi.remarkCreateDate;
			targetMoi.remarkCreateUser = srcMoi.remarkCreateUser;
			targetMoi.remarkText = srcMoi.remarkText;
			
			targetMoi.updateDate = srcMoi.updateDate;
			targetMoi.createDate = srcMoi.createDate;
			targetMoi.updateUser = srcMoi.updateUser;
			targetMoi.createUser = srcMoi.createUser;
			targetMoi.version = srcMoi.version;
			
			// since there is no linkage from regimen to MedOrderItem, there is no need to create a new regimen object 
			targetMoi.rxDrug = srcMoi.rxDrug;
			targetMoi.capdRxDrug = srcMoi.capdRxDrug;
			
			targetMoi.medOrderItemAlertList = new ArrayCollection();
			for each (var moia:MedOrderItemAlert in srcMoi.medOrderItemAlertList) {
				targetMoi.medOrderItemAlertList.addItem(new MedOrderItemAlert(moia));
			}
		} 
	}
}