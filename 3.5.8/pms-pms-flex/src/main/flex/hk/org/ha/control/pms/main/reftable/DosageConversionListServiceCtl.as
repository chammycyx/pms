package hk.org.ha.control.pms.main.reftable
{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.reftable.PrintDosageConversionRptEvent;
	import hk.org.ha.event.pms.main.reftable.RefreshDosageConversionByMoDosageEvent;
	import hk.org.ha.event.pms.main.reftable.RefreshDosageConversionEvent;
	import hk.org.ha.event.pms.main.reftable.RefreshDosageConversionRptListEvent;
	import hk.org.ha.event.pms.main.reftable.RefreshPrintDosageConversionRptEvent;
	import hk.org.ha.event.pms.main.reftable.RetrieveDosageConversionByMoDosageEvent;
	import hk.org.ha.event.pms.main.reftable.RetrieveDosageConversionEvent;
	import hk.org.ha.event.pms.main.reftable.RetrieveDosageConversionRptListEvent;
	import hk.org.ha.event.pms.main.reftable.UpdateDosageConversionEvent;
	import hk.org.ha.fmk.pms.flex.components.lookup.RefreshLookupPopupDataProviderEvent;
	import hk.org.ha.model.pms.biz.reftable.DosageConversionListServiceBean;
	import hk.org.ha.model.pms.biz.reftable.DosageConversionRptListServiceBean;
	import hk.org.ha.model.pms.dms.persistence.PmsDosageConversionSet;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("dosageConversionListServiceCtl", restrict="true")]
	public class DosageConversionListServiceCtl
	{	
		[In]
		public var dosageConversionListService:DosageConversionListServiceBean;
		
		[In]
		public var dosageConversionRptListService:DosageConversionRptListServiceBean;
		
		[In]
		public var pmsDosageConversionSet:PmsDosageConversionSet;
		
		[In]
		public var pmsDosageConversionSetList:ArrayCollection;
		
		private var callBackEvent:Event;
		
		[Observer]
		public function retrieveDosageConversionSetList(evt:RetrieveDosageConversionEvent):void
		{
			callBackEvent = evt.callBackEvent;
			dosageConversionListService.retrievePmsDosageConversionSetList(evt.dmDrug, retrieveDosageConversionSetListResult);
		}
		
		private function retrieveDosageConversionSetListResult(evt:TideResultEvent):void
		{	
			evt.context.dispatchEvent(new RefreshLookupPopupDataProviderEvent(pmsDosageConversionSetList));
			if ( callBackEvent ) {
				evt.context.dispatchEvent(callBackEvent);
			}	
		}
		
		[Observer]
		public function retrieveDosageConversionByMoDosage(evt:RetrieveDosageConversionByMoDosageEvent):void
		{
			In(Object(dosageConversionListService).retrieveSuccess)
			dosageConversionListService.retrievePmsDosageConversionByMoDosage(
				evt.dmDrug, evt.prescribingDosage, evt.moDosageUnit, retrieveDosageConversionByMoDosageResult);
		}
		
		private function retrieveDosageConversionByMoDosageResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new RefreshDosageConversionByMoDosageEvent(evt.result as PmsDosageConversionSet));
		}
		
		[Observer]
		public function updateDosageConversion(evt:UpdateDosageConversionEvent):void
		{
			In(Object(dosageConversionListService).saveSuccess);
			In(Object(dosageConversionListService).errorCode);
			In(Object(dosageConversionListService).errorParam);
			dosageConversionListService.updateDosageConversion(evt.pmsDosageConversionSet, updateDosageConversionResult);
		}
		
		private function updateDosageConversionResult(evt:TideResultEvent):void{
			evt.context.dispatchEvent(new RefreshDosageConversionEvent(
				Object(dosageConversionListService).saveSuccess,
				Object(dosageConversionListService).errorCode,
				Object(dosageConversionListService).errorParam ));
		}
		
		[Observer]
		public function retrieveDosageConversionRptList(evt:RetrieveDosageConversionRptListEvent):void
		{
			In(Object(dosageConversionRptListService).retrieveSuccess)
			dosageConversionRptListService.genDosageConversionRptList(retrieveDosageConversionRptListResult);
		}
		
		private function retrieveDosageConversionRptListResult(evt:TideResultEvent):void {
			
			evt.context.dispatchEvent(new RefreshDosageConversionRptListEvent( Object(dosageConversionRptListService).retrieveSuccess ));
		}
		
		[Observer]
		public function printDosageConversionRpt(evt:PrintDosageConversionRptEvent):void
		{
			In(Object(dosageConversionRptListService).retrieveSuccess)
			dosageConversionRptListService.printDosageConversionRpt(printDosageConversionRptResult);
		}	
		
		private function printDosageConversionRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshPrintDosageConversionRptEvent(Object(dosageConversionRptListService).retrieveSuccess));
		}
	}
}