package hk.org.ha.event.pms.main.enquiry.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDrugChargeClearanceEnqViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		public function ShowDrugChargeClearanceEnqViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}