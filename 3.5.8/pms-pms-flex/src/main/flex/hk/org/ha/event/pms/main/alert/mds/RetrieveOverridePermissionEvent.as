package hk.org.ha.event.pms.main.alert.mds {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrieveOverridePermissionEvent extends AbstractTideEvent {
		
		private var _successCallbackFunc:Function;
		
		private var _faultCallbackFunc:Function;
		
		public function RetrieveOverridePermissionEvent(successCallbackFunc:Function=null, faultCallbackFunc:Function=null) {
			super();
			_successCallbackFunc = successCallbackFunc;
			_faultCallbackFunc = faultCallbackFunc;
		}
		
		public function get successCallbackFunc():Function {
			return _successCallbackFunc;
		}
		
		public function get faultCallbackFunc():Function {
			return _faultCallbackFunc;
		}
	}
}