package hk.org.ha.event.pms.main.reftable {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshMutiDoseConversionListEvent extends AbstractTideEvent 
	{	
		private var _result:Boolean;
		private var _action:String;
		
		public function RefreshMutiDoseConversionListEvent(action:String, result:Boolean):void 
		{
			super();
			_result = result;
			_action = action;
		}
		
		public function get result():Boolean
		{
			return _result;
		}
		
		public function get action():String
		{
			return _action;
		}
	}
}