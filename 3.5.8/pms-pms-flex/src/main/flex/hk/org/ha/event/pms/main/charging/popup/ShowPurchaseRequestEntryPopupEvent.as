package hk.org.ha.event.pms.main.charging.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
	import hk.org.ha.model.pms.persistence.medprofile.PurchaseRequest;
	
	public class ShowPurchaseRequestEntryPopupEvent extends AbstractTideEvent 
	{
		private var _purchaseRequest:PurchaseRequest;
		private var _medProfilePoItem:MedProfilePoItem;
		private var _isManualProfile:Boolean;
		private var _confirmReqCallbackFunc:Function;
		private var _deferReqCallbackFunc:Function;
		private var _discardReqCallbackFunc:Function;
		
		public function ShowPurchaseRequestEntryPopupEvent(purchaseRequest:PurchaseRequest, 
														   medProfilePoItem:MedProfilePoItem,
														   isManualProfile:Boolean,
														   confirmReqCallbackFunc:Function,
														   deferReqCallbackFunc:Function = null,
														   discardReqCallbackFunc:Function = null):void 
		{
			super();
			_purchaseRequest = purchaseRequest;
			_medProfilePoItem = medProfilePoItem;
			_isManualProfile = isManualProfile;
			_confirmReqCallbackFunc = confirmReqCallbackFunc;
			_deferReqCallbackFunc = deferReqCallbackFunc;
			_discardReqCallbackFunc = discardReqCallbackFunc;
		}
		
		public function get medProfilePoItem():MedProfilePoItem {
			return _medProfilePoItem;
		}
		
		public function get isManualProfile():Boolean {
			return _isManualProfile;
		}
		
		public function get confirmReqCallbackFunc():Function{
			return _confirmReqCallbackFunc;
		}
		
		public function get discardReqCallbackFunc():Function{
			return _discardReqCallbackFunc;
		}
		
		public function get deferReqCallbackFunc():Function{
			return _deferReqCallbackFunc;
		}
		
		public function get purchaseRequest():PurchaseRequest {
			return _purchaseRequest;
		}
	}
}