package hk.org.ha.event.pms.main.lookup.popup
{
	public class WarningCodeLookupPopupProp
	{
		public var doubleClickHandler:Function;
		public var okHandler:Function;
		public var cancelHandler:Function;
		public var prefixWarnCode:String;
		public var ignoreIpOnlyFlag:Boolean=true;
		public var pivasFlag:Boolean=false;
	}
}