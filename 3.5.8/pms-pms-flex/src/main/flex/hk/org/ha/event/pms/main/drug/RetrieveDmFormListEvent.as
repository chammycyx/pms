package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDmFormListEvent extends AbstractTideEvent 
	{
		private var _formCode:String;
		private var _event:Event;
		
		public function RetrieveDmFormListEvent(formCode:String, event:Event=null):void 
		{
			super();
			_formCode = formCode;
			_event = event;
		}
		
		public function get formCode():String
		{
			return _formCode;
		}
		
		public function get event():Event
		{
			return _event;
		}
	}
}