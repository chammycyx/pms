package hk.org.ha.event.pms.main.onestop {
		
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CheckAllowModifyOrderStatusEvent extends AbstractTideEvent 
	{			
		public var _dispOrderId:Number;
		public var _dispOrderVersion:Number;
		public var _followUpFunc:Function;
		
		public function CheckAllowModifyOrderStatusEvent(dispOrderId:Number, dispOrderVersion:Number, followUpFunc:Function):void 
		{
			super();
			this._dispOrderId = dispOrderId;
			this._dispOrderVersion = dispOrderVersion;
			this._followUpFunc = followUpFunc;
		}        
		
		public function get dispOrderId():Number
		{
			return _dispOrderId;
		}
		
		public function get dispOrderVersion():Number
		{
			return _dispOrderVersion;
		}
		
		public function get followUpFunc():Function
		{
			return _followUpFunc;
		}
	}
}