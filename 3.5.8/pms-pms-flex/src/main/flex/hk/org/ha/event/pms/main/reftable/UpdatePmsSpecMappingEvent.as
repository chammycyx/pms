package hk.org.ha.event.pms.main.reftable {
	
	import hk.org.ha.model.pms.dms.persistence.PmsSpecMapping;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdatePmsSpecMappingEvent extends AbstractTideEvent 
	{
		private var _pmsSpecMapping:PmsSpecMapping;
		
		public function UpdatePmsSpecMappingEvent(pmsSpecMapping:PmsSpecMapping):void 
		{
			super();
			_pmsSpecMapping = pmsSpecMapping;
		}
	
		
		public function get pmsSpecMapping():PmsSpecMapping
		{
			return _pmsSpecMapping;
		}
	}
}