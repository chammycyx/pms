package hk.org.ha.event.pms.main.reftable {
	
	import hk.org.ha.model.pms.persistence.reftable.DrsPatient;
	
	import mx.core.UIComponent;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateDrsPatientEvent extends AbstractTideEvent 
	{		
		private var _drsPatient:DrsPatient;
		
		public function UpdateDrsPatientEvent(drsPatient:DrsPatient):void 
		{
			super();
			_drsPatient = drsPatient;
		}
		
		public function get drsPatient():DrsPatient
		{
			return _drsPatient;
		}
		
	}
}