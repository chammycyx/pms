package hk.org.ha.control.pms.main.vetting {

	import hk.org.ha.event.pms.main.vetting.GetDispLabelPreviewEvent;
	import hk.org.ha.event.pms.main.vetting.mp.GetManualEntryDispLabelPreviewEvent;
	import hk.org.ha.event.pms.main.vetting.mp.GetMpDispLabelPreviewEvent;
	import hk.org.ha.event.pms.main.vetting.mp.ShowManualEntryDispLabelPreviewEvent;
	import hk.org.ha.event.pms.main.vetting.mp.ShowMpDispLabelPreviewEvent;
	import hk.org.ha.event.pms.main.vetting.show.ShowDispLabelPreviewEvent;
	import hk.org.ha.model.pms.biz.label.LabelServiceBean;
	import hk.org.ha.model.pms.vo.label.DispLabel;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;

    [Bindable]
    [Name("dispLabelPreviewServiceCtl", restrict="true")]
    public class DispLabelPreviewServiceCtl 
	{
    
        [In]
        public var labelService:LabelServiceBean;

		[In]
		public var ctx:Context;
		
        [Observer]
        public function getDispLabelPreview(evt:GetDispLabelPreviewEvent):void {
			labelService.getDispLabelPreview(evt.pharmOrder, evt.printLang, evt.printType, getDispLabelPreviewResult);			
        }
		
		private function getDispLabelPreviewResult(evt:TideResultEvent):void {
        	evt.context.dispatchEvent(new ShowDispLabelPreviewEvent);
        }
		
		[Observer]
		public function getMpDispLabelPreview(evt:GetMpDispLabelPreviewEvent):void
		{
			labelService.getMpDispLabelPreview(evt.medProfileItem, evt.medProfileMoItem, getMpDispLabelPreviewResult);
		}
		
		private function getMpDispLabelPreviewResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new ShowMpDispLabelPreviewEvent);
		}
		
		[Observer]
		public function getManualEntryDispLabelPreview(evt:GetManualEntryDispLabelPreviewEvent):void
		{
			labelService.getMpDispLabelPreview(
				evt.medProfileItem, 
				evt.medProfileMoItem, 
				function(evt:TideResultEvent):void {
					evt.context.dispatchEvent(new ShowManualEntryDispLabelPreviewEvent);
				}
			);
		}
    }
}
