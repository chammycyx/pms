package hk.org.ha.event.pms.main.vetting {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	
	public class UpdateMedOrderItemFromOrderEditEvent extends AbstractTideEvent {
		
		private var _selectedIndex:int;	
		
		private var _medOrderItem:MedOrderItem;				
		
		private var _callBackFunction:Function;
		
		private var _skipDuplicateCheckFlag:Boolean; 
		
		private var _cancelFunctionCall:Function;
		
		public function UpdateMedOrderItemFromOrderEditEvent(selectedIndex:int, medOrderItem:MedOrderItem, callBackFunction:Function=null, skipDuplicateCheckFlag:Boolean=false, cancelFunctionCall:Function =null) 
		{
			super();			
			_selectedIndex = selectedIndex;
			_medOrderItem = medOrderItem;		
			_callBackFunction = callBackFunction;
			_skipDuplicateCheckFlag = skipDuplicateCheckFlag;
			_cancelFunctionCall = cancelFunctionCall;
		}
		
		public function get selectedIndex():int 
		{
			return _selectedIndex;
		}
		
		public function get medOrderItem():MedOrderItem
		{
			return _medOrderItem;
		}
		
		public function get callBackFunction():Function
		{
			return _callBackFunction;
		}
		
		public function get skipDuplicateCheckFlag():Boolean
		{
			return _skipDuplicateCheckFlag;
		}
		
		public function get cancelFunctionCall():Function
		{
			return _cancelFunctionCall;
		}
	}
}