package hk.org.ha.control.pms.exception
{
	import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	
	import mx.messaging.messages.ErrorMessage;
	
	import org.granite.tide.BaseContext;
	import org.granite.tide.IExceptionHandler;

	public class ConcurrentUpdateExceptionHandler implements IExceptionHandler
	{
		public static const CONCURRENT_UPDATE_EXCEPTION:String = "ConcurrentUpdateException";
		
		public function ConcurrentUpdateExceptionHandler()
		{
		}
		
		public function accepts(emsg:ErrorMessage):Boolean
		{
			return emsg.faultCode == CONCURRENT_UPDATE_EXCEPTION;
		}
		
		public function handle(context:BaseContext, emsg:ErrorMessage):void
		{
			var popupProp:SystemMessagePopupProp = new SystemMessagePopupProp();
			popupProp.messageCode = "0318";
			popupProp.setOkButtonOnly = true;
			context.dispatchEvent(new RetrieveSystemMessageEvent(popupProp));
		}
	}
}