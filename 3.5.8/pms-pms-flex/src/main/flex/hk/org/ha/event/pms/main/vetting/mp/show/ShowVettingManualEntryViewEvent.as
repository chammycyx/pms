package hk.org.ha.event.pms.main.vetting.mp.show
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowVettingManualEntryViewEvent extends AbstractTideEvent
	{
		private var _fromPharmacyInbox:Boolean;
		
		public function ShowVettingManualEntryViewEvent(fromPharmacyInbox:Boolean = false)
		{
			super();
			_fromPharmacyInbox = fromPharmacyInbox;
		}
		
		public function get fromPharmacyInbox():Boolean
		{
			return _fromPharmacyInbox;
		}
	}
}