package hk.org.ha.event.pms.main.vetting.mp
{
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ReleaseMedProfileEvent extends AbstractTideEvent
	{
		public function ReleaseMedProfileEvent()
		{
			super();
		}
	}
}