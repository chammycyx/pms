package hk.org.ha.event.pms.main.lookup.popup {
	
	import hk.org.ha.event.pms.main.lookup.popup.ItemCodeLookupPopupProp;
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowItemCodeLookupPopupEvent extends AbstractTideEvent 
	{
		private var _itemCodeLookupPopupProp:ItemCodeLookupPopupProp;
		
		public function ShowItemCodeLookupPopupEvent(itemCodeLookupPopupProp:ItemCodeLookupPopupProp):void 
		{
			super();
			_itemCodeLookupPopupProp = itemCodeLookupPopupProp;
		}
		
		public function get itemCodeLookupPopupProp():ItemCodeLookupPopupProp{
			return _itemCodeLookupPopupProp;
		}

	}
}