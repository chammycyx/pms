package hk.org.ha.event.pms.main.vetting {
	
	import flash.events.Event;
	
	public class OrderLineFocusOutEvent extends Event {
		
		public function OrderLineFocusOutEvent() {
			super("orderLineFocusOut", true);
		}
	}
}
