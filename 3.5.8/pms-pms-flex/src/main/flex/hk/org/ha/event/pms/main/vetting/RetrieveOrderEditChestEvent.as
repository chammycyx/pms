package hk.org.ha.event.pms.main.vetting {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrieveOrderEditChestEvent extends AbstractTideEvent 
	{		
		private var _unitDoseName:String;
		
		public function RetrieveOrderEditChestEvent(unitDoseName:String) {
			super();
			_unitDoseName = unitDoseName;
		}
		
		public function get unitDoseName():String {
			return _unitDoseName;
		}
	}
}
