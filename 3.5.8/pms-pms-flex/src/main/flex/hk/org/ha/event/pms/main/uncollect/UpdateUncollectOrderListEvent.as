package hk.org.ha.event.pms.main.uncollect {
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateUncollectOrderListEvent extends AbstractTideEvent 
	{
		private var _updateUncollectOrderList:ListCollectionView;
		
		public function UpdateUncollectOrderListEvent(updateUncollectOrderList:ListCollectionView):void 
		{
			super();
			_updateUncollectOrderList = updateUncollectOrderList;
		}
		
		public function get updateUncollectOrderList():ListCollectionView {
			return _updateUncollectOrderList;
		}
	}
}