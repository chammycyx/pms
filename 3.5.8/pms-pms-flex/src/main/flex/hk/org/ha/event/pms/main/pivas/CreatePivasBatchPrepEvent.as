package hk.org.ha.event.pms.main.pivas
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CreatePivasBatchPrepEvent extends AbstractTideEvent 
	{		
		private var _pivasFormulaMethodId:Number;
		
		public function CreatePivasBatchPrepEvent(pivasFormulaMethodId:Number):void 
		{
			super();
			_pivasFormulaMethodId = pivasFormulaMethodId;
		}
		
		public function get pivasFormulaMethodId():Number {
			return _pivasFormulaMethodId;
		}
	}
}