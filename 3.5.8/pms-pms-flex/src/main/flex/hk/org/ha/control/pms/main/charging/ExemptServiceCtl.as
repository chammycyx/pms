package hk.org.ha.control.pms.main.charging {

	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.vetting.popup.RefreshSfiHandleChargeExemptAuthentPopupEvent;
	import hk.org.ha.event.pms.main.vetting.popup.ValidateHandlingExemptAuthInfoEvent;
	import hk.org.ha.model.pms.biz.charging.ExemptServiceBean;
	import hk.org.ha.model.pms.persistence.disp.PharmOrder;
	import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
	import hk.org.ha.model.pms.vo.charging.HandleExemptAuthInfo;
	
	import mx.utils.StringUtil;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("exemptServiceCtl", restrict="true")]
	public class ExemptServiceCtl {

		[In]
		public var ctx:Context;
		
		[In]
		public var exemptService:ExemptServiceBean;

		[In]
		public var pharmOrder:PharmOrder;
		
		private var validateExemptCallbackFunc:Function;
		
		[Observer]
		public function validateHandleExemptAuthInfo(evt:ValidateHandlingExemptAuthInfoEvent):void{
			validateExemptCallbackFunc = evt.validateCallbackFunc;
			exemptService.validateHandleExemptAuthInfo(evt.handleExemptAuthInfo, validateHandleExemptAuthInfoResult);
		}
		
		private function validateHandleExemptAuthInfoResult(evt:TideResultEvent):void{
			var handleExemptAuthInfo:HandleExemptAuthInfo = evt.result as HandleExemptAuthInfo;
			if( StringUtil.trim(handleExemptAuthInfo.errMsg) == "" ){
				for each( var pharmOrderItem:PharmOrderItem in pharmOrder.pharmOrderItemList ){
					if( pharmOrderItem.handleExemptCode != null || pharmOrderItem.nextHandleExemptCode != null ){
						pharmOrderItem.handleExemptUser = handleExemptAuthInfo.exemptAuthBy;
					}
				}
			}
			evt.context.dispatchEvent(new RefreshSfiHandleChargeExemptAuthentPopupEvent(handleExemptAuthInfo.errMsg, validateExemptCallbackFunc));
		}
	}
}
