package hk.org.ha.control.pms.main.checkissue{
	
	import hk.org.ha.event.pms.main.checkissue.RetrieveBatchIssueLogListEvent;
	import hk.org.ha.event.pms.main.checkissue.SetWorkstoreDropDownListSelectIndexEvent;
	import hk.org.ha.event.pms.main.checkissue.popup.ShowBatchIssueLogHistoryPopupEvent;
	import hk.org.ha.model.pms.biz.checkissue.BatchIssueListServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("batchIssueListServiceCtl", restrict="true")]
	public class BatchIssueListServiceCtl 
	{
		[In]
		public var batchIssueListService:BatchIssueListServiceBean;
		
		[Observer]
		public function retrieveBatchIssueLogList(evt:RetrieveBatchIssueLogListEvent):void
		{
			batchIssueListService.retrieveBatchIssueLogList(retrieveBatchIssueLogListResult);
		}
		
		private function retrieveBatchIssueLogListResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new ShowBatchIssueLogHistoryPopupEvent());
			evt.context.dispatchEvent(new SetWorkstoreDropDownListSelectIndexEvent());
		}	
	}
}
