package hk.org.ha.event.pms.main.reftable.show
{
	import org.granite.tide.events.AbstractTideEvent;
	public class ShowPharmRemarkTemplateMaintViewEvent extends AbstractTideEvent 
	{
		
		private var _clearMessages:Boolean;
		
		
		public function ShowPharmRemarkTemplateMaintViewEvent(clearMessages:Boolean=true)
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
		
	}
}