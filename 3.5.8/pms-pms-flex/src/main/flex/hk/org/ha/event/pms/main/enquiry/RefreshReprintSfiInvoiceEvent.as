package hk.org.ha.event.pms.main.enquiry
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshReprintSfiInvoiceEvent extends AbstractTideEvent 
	{				
		private var _errMsg:String;
		
		public function RefreshReprintSfiInvoiceEvent(errMsg:String):void
		{
			super();
			_errMsg = errMsg;
		}
		
		public function get errMsg():String{
			return _errMsg;
		}
	}
}