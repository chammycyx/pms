package hk.org.ha.event.pms.main.reftable.mp {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveWardListEvent extends AbstractTideEvent 
	{	
		private var _prefixWardCode:String;
		
		public function RetrieveWardListEvent(prefixWardCode:String):void 
		{
			super();
			_prefixWardCode = prefixWardCode;
		}
		
		public function get prefixWardCode():String {
			return _prefixWardCode;
		}
	}
}