package hk.org.ha.event.pms.main.vetting.mp.popup
{
	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowProceedCancelMessagePopupEvent extends AbstractTideEvent
	{
		private var _popupProp:SystemMessagePopupProp;
		
		public function ShowProceedCancelMessagePopupEvent(popupProp:SystemMessagePopupProp)
		{
			super();
			_popupProp = popupProp;
		}
		
		public function get popupProp():SystemMessagePopupProp
		{
			return _popupProp;
		}
	}
}