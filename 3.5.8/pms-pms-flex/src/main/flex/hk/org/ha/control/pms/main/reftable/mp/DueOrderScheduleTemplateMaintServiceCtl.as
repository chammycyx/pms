package hk.org.ha.control.pms.main.reftable.mp {
	
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.event.pms.main.reftable.mp.CheckDueOrderScheduleTemplateMaintDeliveryScheduleNameExistEvent;
	import hk.org.ha.event.pms.main.reftable.mp.CopyDueOrderScheduleTemplateMaintDeliveryScheduleEvent;
	import hk.org.ha.event.pms.main.reftable.mp.DeleteDueOrderScheduleTemplateMaintDeliveryScheduleEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RefreshDueOrderScheduleTemplateMaintWardAndWardGroupListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveDueOrderScheduleTemplateMaintDeliveryScheduleItemListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveDueOrderScheduleTemplateMaintDeliveryScheduleListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.UpdateDueOrderScheduleTemplateMaintDeliveryScheduleEvent;
	import hk.org.ha.model.pms.biz.reftable.mp.DueOrderScheduleTemplateMaintServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("dueOrderScheduleTemplateMaintServiceCtl", restrict="true")]
	public class DueOrderScheduleTemplateMaintServiceCtl 
	{
		[In]
		public var ctx:Context;
		
		[In]
		public var dueOrderScheduleTemplateMaintService:DueOrderScheduleTemplateMaintServiceBean;
		
		[Observer]
		public function refreshWardAndWardGroupList(refreshDueOrderScheduleTemplateMaintWardAndWardGroupListEvent:RefreshDueOrderScheduleTemplateMaintWardAndWardGroupListEvent):void
		{
			dueOrderScheduleTemplateMaintService.retrieveWardAndWardGroupList();
		}
		
		[Observer]
		public function retrieveDeliveryScheduleList(retrieveDueOrderScheduleTemplateMaintDeliveryScheduleListEvent:RetrieveDueOrderScheduleTemplateMaintDeliveryScheduleListEvent):void
		{
			dueOrderScheduleTemplateMaintService.retrieveDeliveryScheduleList(
				function(evt:TideResultEvent):void {
					retrieveDueOrderScheduleTemplateMaintDeliveryScheduleListEvent.callback(evt.result as ArrayCollection);
				});
		}
		
		[Observer]
		public function retrieveDeliveryScheduleItemList(retrieveDueOrderScheduleTemplateMaintDeliveryScheduleItemListEvent:RetrieveDueOrderScheduleTemplateMaintDeliveryScheduleItemListEvent):void
		{
			dueOrderScheduleTemplateMaintService.retrieveDeliveryScheduleItemList(retrieveDueOrderScheduleTemplateMaintDeliveryScheduleItemListEvent.deliveryScheduleId, 
				function(evt:TideResultEvent):void {
					retrieveDueOrderScheduleTemplateMaintDeliveryScheduleItemListEvent.callback(evt.result as ArrayCollection);
				});
		}
		
		[Observer]
		public function checkDeliveryScheduleNameExists(checkDueOrderScheduleTemplateMaintDeliveryScheduleNameExistEvent:CheckDueOrderScheduleTemplateMaintDeliveryScheduleNameExistEvent):void
		{
			dueOrderScheduleTemplateMaintService.checkDeliveryScheduleNameExists(checkDueOrderScheduleTemplateMaintDeliveryScheduleNameExistEvent.deliveryScheduleName, 
				function(evt:TideResultEvent):void {
					checkDueOrderScheduleTemplateMaintDeliveryScheduleNameExistEvent.callback(evt.result as Boolean);
				});
		}
		
		[Observer]
		public function copyDeliverySchedule(copyDueOrderScheduleTemplateMaintDeliveryScheduleEvent:CopyDueOrderScheduleTemplateMaintDeliveryScheduleEvent):void
		{
			dueOrderScheduleTemplateMaintService.copyDeliverySchedule(copyDueOrderScheduleTemplateMaintDeliveryScheduleEvent.deliveryScheduleId, 
				function(evt:TideResultEvent):void {
					copyDueOrderScheduleTemplateMaintDeliveryScheduleEvent.callback(evt.result as ArrayCollection);
				});
		}
		
		[Observer]
		public function updateDeliverySchedule(updateDueOrderScheduleTemplateMaintDeliveryScheduleEvent:UpdateDueOrderScheduleTemplateMaintDeliveryScheduleEvent):void
		{
			dueOrderScheduleTemplateMaintService.updateDeliverySchedule(updateDueOrderScheduleTemplateMaintDeliveryScheduleEvent.updateDeliverySchedule, 
																		updateDueOrderScheduleTemplateMaintDeliveryScheduleEvent.updateDeliveryScheduleItemList,  
				function(evt:TideResultEvent):void {
					updateDueOrderScheduleTemplateMaintDeliveryScheduleEvent.callback(evt.result as ArrayCollection);
				});
		}
		
		[Observer]
		public function deleteDeliverySchedule(deleteDueOrderScheduleTemplateMaintDeliveryScheduleEvent:DeleteDueOrderScheduleTemplateMaintDeliveryScheduleEvent):void
		{
			dueOrderScheduleTemplateMaintService.deleteDeliverySchedule(deleteDueOrderScheduleTemplateMaintDeliveryScheduleEvent.deleteDeliveryScheduleId,
				function(evt:TideResultEvent):void {
					deleteDueOrderScheduleTemplateMaintDeliveryScheduleEvent.callback(evt.result as ArrayCollection);
				});
		}
		
	}
}
