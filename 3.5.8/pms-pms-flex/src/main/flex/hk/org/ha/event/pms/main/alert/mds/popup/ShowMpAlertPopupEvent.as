package hk.org.ha.event.pms.main.alert.mds.popup {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;
	
	import mx.collections.ListCollectionView;
	
	public class ShowMpAlertPopupEvent extends AbstractTideEvent {
		
		public var alertMsg:AlertMsg;
		
		public var alertEntityList:ListCollectionView;
		
		public var state:String;
		
		public var allowEditFlag:Boolean = true;
		
		public var overrideCallback:Function = function():void {};
		
		public var acceptCallback:Function = function():void {};
		
		public var deferCallback:Function = function():void {};
		
		public var doNotPrescribeCallback:Function = function():void {};
		
		public var closeCallback:Function = function():void {};
		
		public function ShowMpAlertPopupEvent(alertMsg:AlertMsg, alertEntityList:ListCollectionView, state:String="enquire") {
			super();
			this.alertMsg = alertMsg;
			this.alertEntityList = alertEntityList;
			this.state = state;
		}
	}
}