package hk.org.ha.event.pms.main.reftable.popup {

	import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaMethod;
	import hk.org.ha.model.pms.vo.reftable.pivas.PivasDrugListInfo;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class ShowPivasFormulaEditPreparationMethodPopupEvent extends AbstractTideEvent {

		private var _pivasFormulaMethod:PivasFormulaMethod;
		private var _pivasDrugListInfo:PivasDrugListInfo;
		private var _okCallback:Function;
		private var _cancelCallback:Function;
		private var _isAddMethodFlag:Boolean;
		
		public function ShowPivasFormulaEditPreparationMethodPopupEvent(pivasFormulaMethod:PivasFormulaMethod, pivasDrugListInfo:PivasDrugListInfo, okCallback:Function, cancelCallback:Function, isAddMethodFlag:Boolean):void {
			super();
			_pivasFormulaMethod = pivasFormulaMethod;
			_pivasDrugListInfo = pivasDrugListInfo;
			_okCallback = okCallback;
			_cancelCallback = cancelCallback;
			_isAddMethodFlag = isAddMethodFlag;
		}
		
		public function get pivasFormulaMethod():PivasFormulaMethod {
			return _pivasFormulaMethod;
		}
		
		public function get pivasDrugListInfo():PivasDrugListInfo {
			return _pivasDrugListInfo;
		}

		public function get okCallback():Function {
			return _okCallback;
		}
		
		public function get cancelCallback():Function {
			return _cancelCallback;
		}
		
		public function get isAddMethodFlag():Boolean {
			return _isAddMethodFlag;
		}
	}
}
