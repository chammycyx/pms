package hk.org.ha.event.pms.main.reftable.mp {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDueOrderScheduleTemplateMaintDeliveryScheduleItemListEvent extends AbstractTideEvent 
	{	
		private var _deliveryScheduleId:Number;
		private var _callback:Function;
		
		public function RetrieveDueOrderScheduleTemplateMaintDeliveryScheduleItemListEvent(deliveryScheduleId:Number, callback:Function):void 
		{
			super();
			_deliveryScheduleId = deliveryScheduleId;
			_callback = callback;
		}
		
		public function get deliveryScheduleId():Number {
			return _deliveryScheduleId;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}

