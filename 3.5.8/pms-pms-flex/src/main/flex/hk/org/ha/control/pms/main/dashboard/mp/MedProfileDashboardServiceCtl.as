package hk.org.ha.control.pms.main.dashboard.mp
{
	import hk.org.ha.event.pms.main.dashboard.mp.CountAllDashboardItems;
	import hk.org.ha.event.pms.main.dashboard.mp.RecalculateDashboardRelatedStatEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.ShowLoadingPopupEvent;
	import hk.org.ha.model.pms.biz.dashboard.mp.MedProfileDashboardServiceBean;
	import hk.org.ha.model.pms.biz.inbox.MedProfileSupportServiceBean;
	
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("medProfileDashboardServiceCtl", restrict="true")]
	public class MedProfileDashboardServiceCtl
	{
		[In]
		public var ctx:Context;
		
		[In]
		public var medProfileDashboardService:MedProfileDashboardServiceBean;
		
		[In]
		public var medProfileSupportService:MedProfileSupportServiceBean;
		
		public function MedProfileDashboardServiceCtl()
		{
		}
		
		[Observer]
		public function countAllDashboardItems(evt:CountAllDashboardItems):void
		{
			medProfileDashboardService.countAllDashboardItems();
		}
		
		[Observer]
		public function recalculateDashboardRelatedStat(evt:RecalculateDashboardRelatedStatEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Refreshing ..."));
			medProfileSupportService.recalculateDashboardRelatedStat(closePopup, closePopup);
		}
		
		private function closePopup(evt:Event = null):void
		{
			dispatchEvent(new CloseLoadingPopupEvent());
		}
	}
}