package hk.org.ha.event.pms.main.report {

	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class PrintPivasWorklistSummaryRptEvent extends AbstractTideEvent 
	{		
		private var _printPivasWorklistSummaryRptList:ArrayCollection;
		
		public function PrintPivasWorklistSummaryRptEvent(printPivasWorklistSummaryRptList:ArrayCollection):void
		{
			super();
			_printPivasWorklistSummaryRptList = printPivasWorklistSummaryRptList;
		}
		
		public function get printPivasWorklistSummaryRptList():ArrayCollection {
			return _printPivasWorklistSummaryRptList;
		}
	}
}