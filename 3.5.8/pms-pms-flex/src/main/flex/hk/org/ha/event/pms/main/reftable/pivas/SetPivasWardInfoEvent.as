package hk.org.ha.event.pms.main.reftable.pivas {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.pivas.PivasWardInfo;
	
	public class SetPivasWardInfoEvent extends AbstractTideEvent {
		private var _pivasWardInfo:PivasWardInfo;
		
		public function SetPivasWardInfoEvent(pivasWardInfo:PivasWardInfo) {
			super();
			_pivasWardInfo = pivasWardInfo;
		}
		
		public function get pivasWardInfo():PivasWardInfo
		{
			return _pivasWardInfo;
		}
	}
}