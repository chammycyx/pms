package hk.org.ha.event.pms.main.vetting.mp
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class FinishAddMrAnnotationToOnhandMedicationEvent extends AbstractTideEvent
	{
		private var _success:Boolean;
		
		public function FinishAddMrAnnotationToOnhandMedicationEvent(success:Boolean)
		{
			super();
			_success = success;
		}
		
		public function get success():Boolean
		{
			return _success;
		}
	}
}