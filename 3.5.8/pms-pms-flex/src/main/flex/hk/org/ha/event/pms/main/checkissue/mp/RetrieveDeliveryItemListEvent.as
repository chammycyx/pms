package hk.org.ha.event.pms.main.checkissue.mp
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDeliveryItemListEvent extends AbstractTideEvent 
	{			
		private var _trxDate:Date;
		private var _caseNum:String;
		private var _batchNum:String;
		private var _mpDispLabelQrCodeXml:String;
		
		public function RetrieveDeliveryItemListEvent(trxDate:Date, caseNum:String, batchNum:String, mpDispLabelQrCodeXml:String):void 
		{
			super();
			_trxDate = trxDate ; 
			_caseNum = caseNum;
			_batchNum = batchNum;
			_mpDispLabelQrCodeXml = mpDispLabelQrCodeXml;
		}
		
		public function get trxDate():Date {
			return _trxDate;
		}
		
		public function get caseNum():String {
			return _caseNum;
		}
		
		public function get batchNum():String {
			return _batchNum;
		}
		
		public function get mpDispLabelQrCodeXml():String {
			return _mpDispLabelQrCodeXml;
		}

	}
}