package hk.org.ha.event.pms.main.reftable.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowPivasSolventLookupPopupEvent extends AbstractTideEvent 
	{
		private var _pivasSolventLookupPopupProp:PivasSolventLookupPopupProp;
		
		public function ShowPivasSolventLookupPopupEvent(pivasSolventLookupPopupProp:PivasSolventLookupPopupProp):void 
		{
			super();
			_pivasSolventLookupPopupProp = pivasSolventLookupPopupProp;
		}
		
		public function get pivasSolventLookupPopupProp():PivasSolventLookupPopupProp{
			return _pivasSolventLookupPopupProp;
		}
	}
}