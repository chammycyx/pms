package hk.org.ha.event.pms.main.alert.druginfo {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrievePreparationForOrderEntryEvent extends AbstractTideEvent {
		
		private var _itemCode:String;
		
		private var _displayName:String;
		
		private var _formCode:String;
		
		private var _saltProperty:String;
		
		private var _fmStatus:String;
		
		private var _aliasName:String;
		
		private var _firstDisplayName:String;
		
		private var _checkHq:Boolean;
		
		private var _displayNameSaltForm:String;
		
		private var _strengthVolume:String;
		
		private var _dob:Date;
		
		private var _patHospCode:String;
		
		private var _gcnSeqNum:String;
		
		private var _rdfgenId:String;
		
		private var _rgenId:String;

		public function RetrievePreparationForOrderEntryEvent(itemCode:String, displayName:String, formCode:String, saltProperty:String, fmStatus:String, aliasName:String, firstDisplayName:String,
															  checkHq:Boolean,
															  displayNameSaltForm:String, strengthVolume:String,
															  dob:Date, patHospCode:String, gcnSeqNum:String, rdfgenId:String, rgenId:String) {
			super();
			_itemCode = itemCode;
			_displayName = displayName;
			_formCode = formCode;
			_saltProperty = saltProperty;
			_fmStatus = fmStatus;
			_aliasName = aliasName;
			_firstDisplayName = firstDisplayName;
			_checkHq = checkHq;
			_displayNameSaltForm = displayNameSaltForm;
			_strengthVolume = strengthVolume;
			_dob = dob;
			_patHospCode = patHospCode;
			_gcnSeqNum = gcnSeqNum;
			_rdfgenId = rdfgenId;
			_rgenId = rgenId;
		}
		
		public function get itemCode():String {
			return _itemCode;
		}
		
		public function get displayName():String {
			return _displayName;
		}
		
		public function get formCode():String {
			return _formCode;
		}
		
		public function get saltProperty():String {
			return _saltProperty;
		}
		
		public function get fmStatus():String {
			return _fmStatus;
		}
		
		public function get aliasName():String {
			return _aliasName;
		}
		
		public function get firstDisplayName():String {
			return _firstDisplayName;
		}
		
		public function get checkHq():Boolean {
			return _checkHq;
		}
		
		public function get displayNameSaltForm():String {
			return _displayNameSaltForm;
		}
		
		public function get strengthVolume():String {
			return _strengthVolume;
		}
		
		public function get dob():Date {
			return _dob;
		}
		
		public function get patHospCode():String {
			return _patHospCode;
		}
		
		public function get gcnSeqNum():String {
			return _gcnSeqNum;
		}
		
		public function get rdfgenId():String {
			return _rdfgenId;
		}
		
		public function get rgenId():String {
			return _rgenId;
		}
		
	}
}