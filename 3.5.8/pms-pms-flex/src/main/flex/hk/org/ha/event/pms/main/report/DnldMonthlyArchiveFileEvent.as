package hk.org.ha.event.pms.main.report {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class DnldMonthlyArchiveFileEvent extends AbstractTideEvent 
	{
		private var _fileName:String;
		private var _password:String;
		
		public function DnldMonthlyArchiveFileEvent(fileName:String,password:String):void 
		{
			super();
			_fileName = fileName;
			_password = password;
			
		}
		
		public function get fileName():String{
			return _fileName;
		}
		
		public function get password():String{
			return _password;
		}
	}
}