package hk.org.ha.event.pms.main.vetting.show {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowPharmRemarkItemViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		private var _clearScreen:Boolean = true;
		private var _isAddRemark:Boolean;	// whether add or update remark
		private var _addRemarkAction:String = null;	// only used in add remark, values: remove, convert 
		private var _updateRemarkSource:String;		// source view, "vetting" or "orderEdit", used in remark update only 
		private var _currentMedOrderItem:MedOrderItem = null;		
		private var _okCallbackFunc:Function = null;
		private var _cancelCallbackEvent:Event = null;
		private var _reverseRemarkCallbackFunc:Function = null;	// only used for item delete case which triggered from vetting screen 		
		private var _logonUser:String;
		
		public function ShowPharmRemarkItemViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}

		public function get clearMessages():Boolean {
			return _clearMessages;
		}

		public function get clearScreen():Boolean {
			return _clearScreen;
		}
		
		public function set clearScreen(clearScreen:Boolean):void {
			_clearScreen = clearScreen;
		}
		
		public function get isAddRemark():Boolean {
			return _isAddRemark;
		}
		
		public function set isAddRemark(isAddRemark:Boolean):void {
			_isAddRemark = isAddRemark;
		}
		
		public function get addRemarkAction():String {
			return _addRemarkAction;
		}
		
		public function set addRemarkAction(addRemarkAction:String):void {
			_addRemarkAction = addRemarkAction;
		}
		
		public function get updateRemarkSource():String {
			return _updateRemarkSource;
		}
		
		public function set updateRemarkSource(updateRemarkSource:String):void {
			_updateRemarkSource = updateRemarkSource;
		}
		
		
		public function get currentMedOrderItem():MedOrderItem {
			return _currentMedOrderItem;
		}
		
		public function set currentMedOrderItem(currentMedOrderItem:MedOrderItem):void {
			_currentMedOrderItem = currentMedOrderItem;
		}
		
		public function get okCallbackFunc():Function {
			return _okCallbackFunc;
		}
		
		public function set okCallbackFunc(okCallbackFunc:Function):void {
			_okCallbackFunc = okCallbackFunc;
		}		
		
		public function get cancelCallbackEvent():Event {
			return _cancelCallbackEvent;
		}

		public function set cancelCallbackEvent(cancelCallbackEvent:Event):void {
			_cancelCallbackEvent = cancelCallbackEvent;
		}
		
		public function get reverseRemarkCallbackFunc():Function {
			return _reverseRemarkCallbackFunc;
		}
		
		public function set reverseRemarkCallbackFunc(reverseRemarkCallbackFunc:Function):void {
			_reverseRemarkCallbackFunc = reverseRemarkCallbackFunc;
		}
		
		public function get logonUser():String {
			return _logonUser;
		}
		
		public function set logonUser(logonUser:String):void {
			_logonUser = logonUser;
		}

		public override function clone():Event {
			var evt:ShowPharmRemarkItemViewEvent = new ShowPharmRemarkItemViewEvent(clearMessages);
			evt.isAddRemark                = isAddRemark;
			evt.addRemarkAction			   = addRemarkAction;
			evt.updateRemarkSource         = updateRemarkSource;
			evt.currentMedOrderItem        = currentMedOrderItem;
			evt.okCallbackFunc             = okCallbackFunc;
			evt.cancelCallbackEvent        = cancelCallbackEvent;
			evt.reverseRemarkCallbackFunc = reverseRemarkCallbackFunc;
			evt.logonUser                  = logonUser;
			return evt;
		}
	}
}