package hk.org.ha.event.pms.main.report {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePrescRptEvent extends AbstractTideEvent 
	{
		private var _rptDate:Date;
		private var _ticketNum:String;
		private var _caseNum:String;
		private var _hkid:String;
		private var _itemCode:String;
		private var _nameTxt:String;
		
		public function RetrievePrescRptEvent( rptDate:Date, ticketNum:String, caseNum:String, hkid:String, itemCode:String, nameTxt:String):void 
		{
			super();
			_rptDate = rptDate;
			_ticketNum = ticketNum;
			_caseNum = caseNum;
			_hkid = hkid;
			_itemCode = itemCode;
			_nameTxt = nameTxt;
		}
		
		public function get rptDate():Date{
			return _rptDate;
		}
		
		public function get ticketNum():String{
			return _ticketNum;
		}
		
		public function get caseNum():String{
			return _caseNum;
		}
		
		public function get hkid():String{
			return _hkid;
		}
		
		public function get itemCode():String{
			return _itemCode;
		}
		
		public function get nameTxt():String{
			return _nameTxt;
		}
	}
}