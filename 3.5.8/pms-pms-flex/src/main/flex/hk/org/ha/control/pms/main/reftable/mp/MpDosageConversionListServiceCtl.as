package hk.org.ha.control.pms.main.reftable.mp
{
	import hk.org.ha.event.pms.main.reftable.mp.PrintMpDosageConversionRptEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RefreshMpDosageConversionEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RefreshMpDosageConversionRptListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RefreshPrintMpDosageConversionRptEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveMpDosageConversionEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveMpDosageConversionHandlerEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveMpDosageConversionRptListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.UpdateMpDosageConversionEvent;
	import hk.org.ha.model.pms.biz.reftable.mp.MpDosageConversionListServiceBean;
	import hk.org.ha.model.pms.biz.reftable.mp.MpDosageConversionRptListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("mpDosageConversionListServiceCtl", restrict="true")]
	public class MpDosageConversionListServiceCtl
	{	
		[In]
		public var mpDosageConversionListService:MpDosageConversionListServiceBean;
		
		[In]
		public var mpDosageConversionRptListService:MpDosageConversionRptListServiceBean;

		[Observer]
		public function retrieveMpDosageConversionSetList(evt:RetrieveMpDosageConversionEvent):void
		{
			mpDosageConversionListService.retrievePmsIpDosageConvSetList(evt.dmDrug, retrieveMpDosageConversionSetListResult);
		}
		
		private function retrieveMpDosageConversionSetListResult(evt:TideResultEvent):void
		{	
			evt.context.dispatchEvent(new RetrieveMpDosageConversionHandlerEvent(evt.result as ArrayCollection));
		}
		
		[Observer]
		public function updateDosageConversionSetList(evt:UpdateMpDosageConversionEvent):void
		{
			In(Object(mpDosageConversionListService).saveSuccess);
			In(Object(mpDosageConversionListService).errorCode);
			In(Object(mpDosageConversionListService).errorParam);
			mpDosageConversionListService.updatePmsIpDosageConvSetList(evt.pmsIpDosageConvSetList, evt.markDeletePmsIpDosageConvSetList, updateMpDosageConversionResult);
		}
		
		private function updateMpDosageConversionResult(evt:TideResultEvent):void{
			evt.context.dispatchEvent(new RefreshMpDosageConversionEvent(
				Object(mpDosageConversionListService).saveSuccess,
				Object(mpDosageConversionListService).errorCode,
				Object(mpDosageConversionListService).errorParam ));
			
		}
		
		[Observer]
		public function retrieveDosageConversionRptList(evt:RetrieveMpDosageConversionRptListEvent):void
		{
			In(Object(mpDosageConversionRptListService).retrieveSuccess)
			mpDosageConversionRptListService.genMpDosageConversionRptList(retrieveDosageConversionRptListResult);
		}
		
		private function retrieveDosageConversionRptListResult(evt:TideResultEvent):void {
			
			evt.context.dispatchEvent(new RefreshMpDosageConversionRptListEvent( Object(mpDosageConversionRptListService).retrieveSuccess ));
		}
		
		[Observer]
		public function printDosageConversionRpt(evt:PrintMpDosageConversionRptEvent):void
		{
			In(Object(mpDosageConversionRptListService).retrieveSuccess)
			mpDosageConversionRptListService.printMpDosageConversionRpt(printMpDosageConversionRptResult);
		}	
		
		private function printMpDosageConversionRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshPrintMpDosageConversionRptEvent(Object(mpDosageConversionRptListService).retrieveSuccess));
		}
	}
}