package hk.org.ha.event.pms.main.reftable.popup {
	
	import hk.org.ha.event.pms.main.reftable.popup.MultiDoseConversionMaintPopupProp;
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowMultiDoseConversionMaintPopupEvent extends AbstractTideEvent 
	{
		private var _multiDoseConversionMaintPopupProp:MultiDoseConversionMaintPopupProp;
		
		public function ShowMultiDoseConversionMaintPopupEvent(multiDoseConversionMaintPopupProp:MultiDoseConversionMaintPopupProp):void 
		{
			super();
			_multiDoseConversionMaintPopupProp = multiDoseConversionMaintPopupProp;
		}
		
		public function get multiDoseConversionMaintPopupProp():MultiDoseConversionMaintPopupProp{
			return _multiDoseConversionMaintPopupProp;
		}

	}
}