package hk.org.ha.event.pms.main.reftable {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDrsPatientEvent extends AbstractTideEvent 
	{
		private var _hkidTxt:String;
		
		public function RetrieveDrsPatientEvent(hkidTxt:String):void 
		{
			super();
			_hkidTxt = hkidTxt;
		}
		
		public function get hkidTxt():String
		{
			return _hkidTxt;
		}

	}
}