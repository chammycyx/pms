package hk.org.ha.event.pms.main.vetting {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrieveCapdConcentrationListEvent extends AbstractTideEvent 
	{
		private var _supplierSystem:String;
		private var _calciumStrength:String;
		
		public function RetrieveCapdConcentrationListEvent(supplierSystem:String, calciumStrength:String) {
			super();
			_supplierSystem = supplierSystem;
			_calciumStrength = calciumStrength;
		}
		
		public function get supplierSystem():String {
			return _supplierSystem;
		}
		
		public function get calciumStrength():String {
			return _calciumStrength;
		}
	}
}
