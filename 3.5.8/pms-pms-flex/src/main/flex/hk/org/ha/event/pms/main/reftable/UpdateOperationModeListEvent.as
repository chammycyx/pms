package hk.org.ha.event.pms.main.reftable {
	
	import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateOperationModeListEvent extends AbstractTideEvent 
	{
		private var _activeOperationProfileId:Number;
		private var _operationPropList:ListCollectionView;
		private var _operationWorkstationList:ListCollectionView;
		private var _newWorkstationList:ListCollectionView;
		private var _pickStationList:ListCollectionView;
		private var _workstorePropList:ListCollectionView;
		private var _showApplyProfileMsg:Boolean;
		
		public function UpdateOperationModeListEvent(operationPropList:ListCollectionView, operationWorkstationList:ListCollectionView, 
													 newWorkstationList:ListCollectionView, pickStationList:ListCollectionView, 
													 workstorePropList:ListCollectionView,
													 showApplyProfileMsg:Boolean, activeOperationProfileId:Number=NaN):void 
		{
			super();
			_activeOperationProfileId = activeOperationProfileId;
			_operationPropList = operationPropList;
			_operationWorkstationList = operationWorkstationList;
			_newWorkstationList = newWorkstationList;
			_pickStationList = pickStationList;
			_workstorePropList = workstorePropList;
			_showApplyProfileMsg = showApplyProfileMsg;
		}
		
		public function get activeOperationProfileId():Number {
			return _activeOperationProfileId;
		}
		
		public function get operationPropList():ListCollectionView {
			return _operationPropList;
		}
		
		public function get operationWorkstationList():ListCollectionView {
			return _operationWorkstationList;
		}
		
		public function get newWorkstationList():ListCollectionView {
			return _newWorkstationList;
		}
		
		public function get pickStationList():ListCollectionView {
			return _pickStationList;
		}
		
		public function get showApplyProfileMsg():Boolean {
			return _showApplyProfileMsg;
		}
		
		public function get workstorePropList():ListCollectionView {
			return _workstorePropList;
		}
	}
}