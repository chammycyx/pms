package hk.org.ha.control.pms.main.reftable {
	
	import hk.org.ha.event.pms.main.reftable.RefreshUpdatePmsWorkstoreRedirectListEvent;
	import hk.org.ha.event.pms.main.reftable.RetrievePmsWorkstoreRedirectListEvent;
	import hk.org.ha.event.pms.main.reftable.SetPmsWorkstoreRedirectGridFocusEvent;
	import hk.org.ha.event.pms.main.reftable.UpdatePmsWorkstoreRedirectListEvent;
	import hk.org.ha.model.pms.biz.reftable.PmsWorkstoreRedirectListServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("pmsWorkstoreRedirectListServiceCtl", restrict="true")]
	public class PmsWorkstoreRedirectListServiceCtl 
	{
		[In]
		public var pmsWorkstoreRedirectListService:PmsWorkstoreRedirectListServiceBean;
		
		[Observer]
		public function retrievePmsWorkstoreRedirectList(evt:RetrievePmsWorkstoreRedirectListEvent):void
		{
			pmsWorkstoreRedirectListService.retrievePmsWorkstoreRedirectList(retrievePmsWorkstoreRedirectListResult);
		}
		
		private function retrievePmsWorkstoreRedirectListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new SetPmsWorkstoreRedirectGridFocusEvent());
		}
		
		[Observer]
		public function updatePmsWorkstoreRedirectList(evt:UpdatePmsWorkstoreRedirectListEvent):void
		{
			pmsWorkstoreRedirectListService.updatePmsWorkstoreRedirectList(evt.pmsWorkstoreRedirectList, updatePmsWorkstoreRedirectListResult);
		}
		
		private function updatePmsWorkstoreRedirectListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshUpdatePmsWorkstoreRedirectListEvent());
		}
	}
}
