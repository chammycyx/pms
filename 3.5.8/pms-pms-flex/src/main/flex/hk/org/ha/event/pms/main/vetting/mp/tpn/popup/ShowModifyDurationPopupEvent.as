package hk.org.ha.event.pms.main.vetting.mp.tpn.popup
{
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.medprofile.TpnRequest;
	import hk.org.ha.view.pms.main.vetting.mp.tpn.popup.TpnPopup;
	
	public class ShowModifyDurationPopupEvent extends AbstractTideEvent
	{
		private var _selectedItem:TpnRequest;
		private var _selectedIndex:Number;
		private var _tpnPopup:TpnPopup;
		
		public function ShowModifyDurationPopupEvent(selectedItem:TpnRequest, selectedIndex:Number, tpnPopup:TpnPopup):void {
			super();
			_selectedItem = selectedItem;
			_selectedIndex = selectedIndex;
			_tpnPopup = tpnPopup;
		}
		
		public function get selectedItem():TpnRequest
		{
			return _selectedItem;
		}
		
		public function get selectedIndex():Number
		{
			return _selectedIndex;
		}
		
		public function get tpnPopup():TpnPopup
		{
			return _tpnPopup;
		}
	}
}