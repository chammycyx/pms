package hk.org.ha.event.pms.main.refill {

	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshTerminateRefillEvent extends AbstractTideEvent 
	{		
		public function RefreshTerminateRefillEvent():void 
		{
			super();		
		}		
	}
}