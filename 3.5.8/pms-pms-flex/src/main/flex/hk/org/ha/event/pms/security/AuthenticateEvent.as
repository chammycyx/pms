package hk.org.ha.event.pms.security {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class AuthenticateEvent extends AbstractTideEvent 
	{
		private var _userId:String;
		private var _password:String;
		
		public function AuthenticateEvent(userId:String, password:String):void 
		{
			super();
			_userId = userId;
			_password = password;
		}
		
		public function get userId():String {
			return _userId;
		}
		
		public function get password():String {
			return _password;
		}
		
	}
}