package hk.org.ha.event.pms.main.drug.popup
{
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	import hk.org.ha.model.pms.vo.security.PropMap;
	
	import mx.collections.ArrayCollection;
	
	public class ShowMpPreparationPopupEvent extends AbstractTideEvent
	{
		private var _drugSearchSource:DrugSearchSource;
		private var _drugSearchPreparationList:ArrayCollection;
		private var _drugSearchPreparationDescMap:PropMap;
		private var _popupOkHandler:Function;
		private var _popupCancelHandler:Function;
		
		public function ShowMpPreparationPopupEvent(drugSearchSource:DrugSearchSource, drugSearchPreparationList:ArrayCollection, drugSearchPreparationDescMap:PropMap, popupOkHandler:Function, popupCancelHandler:Function)
		{
			super();
			_drugSearchSource = drugSearchSource;
			_drugSearchPreparationList = drugSearchPreparationList;
			_drugSearchPreparationDescMap = drugSearchPreparationDescMap;
			_popupOkHandler = popupOkHandler;
			_popupCancelHandler = popupCancelHandler;
		}

		public function get drugSearchSource():DrugSearchSource{
			return _drugSearchSource;
		}
		
		public function get drugSearchPreparationList():ArrayCollection{
			return _drugSearchPreparationList;
		}

		public function get drugSearchPreparationDescMap():PropMap{
			return _drugSearchPreparationDescMap;
		}
		
		public function get popupOkHandler():Function{
			return _popupOkHandler;
		}
		
		public function get popupCancelHandler():Function{
			return _popupCancelHandler;
		}
	}
}