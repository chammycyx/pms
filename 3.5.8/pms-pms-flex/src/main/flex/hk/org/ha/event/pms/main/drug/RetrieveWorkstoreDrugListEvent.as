package hk.org.ha.event.pms.main.drug {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveWorkstoreDrugListEvent extends AbstractTideEvent 
	{
		private var _itemCode:String;
		private var _showNonSuspendItemOnly:Boolean;
		private var _fdnFlag:Boolean;
		
		public function RetrieveWorkstoreDrugListEvent(itemCode:String, showNonSuspendItemOnly:Boolean, fdnFlag:Boolean):void 
		{
			super();
			_itemCode = itemCode;
			_showNonSuspendItemOnly = showNonSuspendItemOnly;
			_fdnFlag = fdnFlag;
		}
		
		public function get itemCode():String
		{
			return _itemCode;
		}
		
		public function get showNonSuspendItemOnly():Boolean
		{
			return _showNonSuspendItemOnly;
		}
		
		public function get fdnFlag():Boolean
		{
			return _fdnFlag;
		}
	}
}