package hk.org.ha.event.pms.main.drug.popup
{
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	import hk.org.ha.view.pms.main.drug.popup.FreeTextEntryPopup;
	
	import mx.collections.ArrayCollection;
	
	public class ShowFreeTextEntryPopupEvent extends AbstractTideEvent
	{
		private var _drugSearchSource:DrugSearchSource;
		private var _freeTextEntryPopupProp:FreeTextEntryPopupProp;
		
		public function ShowFreeTextEntryPopupEvent(freeTextEntryPopupProp:FreeTextEntryPopupProp, drugSearchSource:DrugSearchSource = null)
		{
			super();
			_drugSearchSource = drugSearchSource;
			_freeTextEntryPopupProp = freeTextEntryPopupProp;
		}

		public function get drugSearchSource():DrugSearchSource {
			return _drugSearchSource;
		}
		
		public function get freeTextEntryPopupProp():FreeTextEntryPopupProp{
			return _freeTextEntryPopupProp;
		}
	}
}