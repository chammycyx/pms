package hk.org.ha.event.pms.main.vetting.mp.popup {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowDeleteActiveItemPopupEvent extends AbstractTideEvent {

		private var _deleteHandler:Function;
		
		public function ShowDeleteActiveItemPopupEvent(deleteHandler:Function) {
			super();
			_deleteHandler = deleteHandler;
		}
		
		public function get deleteHandler():Function {
			return _deleteHandler;
		}
	}
}
