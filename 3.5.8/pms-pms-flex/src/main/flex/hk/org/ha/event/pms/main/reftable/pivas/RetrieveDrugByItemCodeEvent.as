package hk.org.ha.event.pms.main.reftable.pivas {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDrugByItemCodeEvent extends AbstractTideEvent 
	{
		private var _itemCode:String;
		
		private var _callbackFunc:Function;
		
		public function RetrieveDrugByItemCodeEvent(itemCode:String, callbackFunc:Function):void {
			_itemCode = itemCode;
			_callbackFunc = callbackFunc;
			super();
		}
		
		public function get itemCode():String {
			return _itemCode;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}
