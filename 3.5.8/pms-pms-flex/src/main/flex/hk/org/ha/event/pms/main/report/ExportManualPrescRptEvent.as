package hk.org.ha.event.pms.main.report {
	
	import hk.org.ha.model.pms.udt.report.ManualPrescRptType;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ExportManualPrescRptEvent extends AbstractTideEvent 
	{
		private var _retrieveResult:Boolean;
		private var _manualPrescRptType:ManualPrescRptType;
		
		public function ExportManualPrescRptEvent(retrieveResult:Boolean, manualPrescRptType:ManualPrescRptType):void 
		{
			super();
			_retrieveResult = retrieveResult;
			_manualPrescRptType = manualPrescRptType;
		}
		
		public function get retrieveResult():Boolean {
			return _retrieveResult;
		}
		
		public function get manualPrescRptType():ManualPrescRptType {
			return _manualPrescRptType;
		}
	}
}