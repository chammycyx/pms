package hk.org.ha.event.pms.main.vetting {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;

	public class RetrievePrevPrescEvent extends AbstractTideEvent {

		private var _orderNum:String;

		public function RetrievePrevPrescEvent(orderNum:String):void {
			super();
			_orderNum = orderNum;
		}

		public function get orderNum():String {
			return _orderNum;
		}
	}
}