package hk.org.ha.event.pms.main.reftable {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveItemLocationListByPickStationNumEvent extends AbstractTideEvent 
	{
		private var _pickStationNum:Number;
		
		public function RetrieveItemLocationListByPickStationNumEvent(pickStationNum:Number):void 
		{
			super();
			_pickStationNum = pickStationNum;
		}
		
		public function get pickStationNum():Number
		{
			return _pickStationNum;
		}
	}
}