package hk.org.ha.event.pms.main.reftable {
	
	import hk.org.ha.model.pms.dms.persistence.PmsQtyConversionAdj;
	import hk.org.ha.model.pms.dms.persistence.PmsQtyConversionInsu;
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateMpQtyDurationConversionListEvent extends AbstractTideEvent 
	{
		private var _updatePmsQtyConversionList:ListCollectionView;
		private var _qtyApplyAll:Boolean;
		private var _updatePmsDurationConversionList:ListCollectionView;
		private var _durationApplyAll:Boolean;
		private var _updatePmsQtyConversionInsu:PmsQtyConversionInsu;
		private var _insuApplyAll:Boolean;
		private var _updatePmsQtyConversionAdj:PmsQtyConversionAdj;
		
		public function UpdateMpQtyDurationConversionListEvent(updatePmsQtyConversionList:ListCollectionView, qtyApplyAll:Boolean, 
															 	updatePmsDurationConversionList:ListCollectionView, durationApplyAll:Boolean, 
															 	updatePmsQtyConversionInsu:PmsQtyConversionInsu, insuApplyAll:Boolean, 
																updatePmsQtyConversionAdj:PmsQtyConversionAdj):void 
		{
			super();
			_updatePmsQtyConversionList = updatePmsQtyConversionList;
			_qtyApplyAll = qtyApplyAll;
			_updatePmsDurationConversionList = updatePmsDurationConversionList;
			_durationApplyAll = durationApplyAll;
			_updatePmsQtyConversionInsu = updatePmsQtyConversionInsu;
			_insuApplyAll = insuApplyAll;
			_updatePmsQtyConversionAdj = updatePmsQtyConversionAdj;
		}
		
		public function get updatePmsQtyConversionList():ListCollectionView
		{
			return _updatePmsQtyConversionList;
		}
		
		public function get qtyApplyAll():Boolean 
		{
			return _qtyApplyAll;
		}
		
		public function get updatePmsDurationConversionList():ListCollectionView
		{
			return _updatePmsDurationConversionList;
		}
		
		public function get durationApplyAll():Boolean 
		{
			return _durationApplyAll;
		}
		
		public function get updatePmsQtyConversionInsu():PmsQtyConversionInsu
		{
			return _updatePmsQtyConversionInsu;
		}
		
		public function get insuApplyAll():Boolean 
		{
			return _insuApplyAll;
		}
		
		public function get updatePmsQtyConversionAdj():PmsQtyConversionAdj
		{
			return _updatePmsQtyConversionAdj;
		}
	}
}