package hk.org.ha.event.pms.main.onestop.popup {
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowReprintMessagePopupEvent extends AbstractTideEvent 
	{	
		private var _messages:Array;
		
		public function ShowReprintMessagePopupEvent(messages:Array):void 
		{
			super();
			_messages = messages;
		}
		
		public function get messages():Array 
		{
			return _messages;
		}
	}
}