package hk.org.ha.event.pms.main.vetting {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dms.vo.PmsFmStatusSearchCriteria;
	
	public class CheckFmIndicationForOrderEditEvent extends AbstractTideEvent {
		
		private var _pmsFmStatusSearchCriteria:PmsFmStatusSearchCriteria;
		private var _callbackFunc:Function;
		
		public function CheckFmIndicationForOrderEditEvent(criteria:PmsFmStatusSearchCriteria, funcValue:Function=null) {
			super();
			_pmsFmStatusSearchCriteria = criteria;
			_callbackFunc = funcValue;
		}
		
		public function get pmsFmStatusSearchCriteria():PmsFmStatusSearchCriteria {
			return _pmsFmStatusSearchCriteria;
		}
		
		public function get callbackFunc():Function
		{
			return _callbackFunc;
		}
	}
}
