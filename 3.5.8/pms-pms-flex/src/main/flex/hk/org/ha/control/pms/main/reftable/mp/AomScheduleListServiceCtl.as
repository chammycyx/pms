package hk.org.ha.control.pms.main.reftable.mp {
	
	import hk.org.ha.event.pms.main.reftable.mp.RefreshAomScheduleMaintViewEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveAomScheduleListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.UpdateAomScheduleListEvent;
	import hk.org.ha.model.pms.biz.reftable.mp.AomScheduleListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("aomScheduleListServiceCtl", restrict="true")]
	public class AomScheduleListServiceCtl 
	{
		[In]
		public var aomScheduleListService:AomScheduleListServiceBean
				
		private var action:String; // Retrieve, Save
		
		[Observer]
		public function retrieveAomScheduleList(evt:RetrieveAomScheduleListEvent):void
		{
			action = "Retrieve";
			aomScheduleListService.retrieveAomScheduleList(refreshAomScheduleMaintView);
		}
		
		[Observer]
		public function updateAomScheduleList(evt:UpdateAomScheduleListEvent):void
		{
			action = "Save";
			aomScheduleListService.updateAomScheduleList(evt.updateAomScheduleList, evt.delAomScheduleList, refreshAomScheduleMaintView);
		}
		
		private function refreshAomScheduleMaintView(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new RefreshAomScheduleMaintViewEvent(action, evt.result as ArrayCollection));
		}
		
	}
}
