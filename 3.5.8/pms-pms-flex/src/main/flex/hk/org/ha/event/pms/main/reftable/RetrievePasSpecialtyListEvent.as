package hk.org.ha.event.pms.main.reftable {
	
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.udt.patient.PasSpecialtyType;
	
	public class RetrievePasSpecialtyListEvent extends AbstractTideEvent 
	{	
		private var _prefixSpecCode:String;
		private var _specType:PasSpecialtyType;
		
		public function RetrievePasSpecialtyListEvent(prefixSpecCode:String, specType:PasSpecialtyType):void 
		{
			super();
			_prefixSpecCode = prefixSpecCode;
			_specType = specType;
		}
		
		public function get prefixSpecCode():String {
			return _prefixSpecCode;
		}
		
		public function get specType():PasSpecialtyType {
			return _specType;
		}
	}
}