package hk.org.ha.event.pms.main.checkissue
{
	import hk.org.ha.model.pms.persistence.corp.Workstore;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class DeleteBatchIssueLogEvent extends AbstractTideEvent 
	{		
		private var _issueDate:Date;
		private var _workstore:Workstore;
		private var _popMsg:Boolean;
		
		public function DeleteBatchIssueLogEvent(issueDate:Date, workstore:Workstore, popMsg:Boolean=true):void 
		{
			super();
			_issueDate = issueDate;
			_workstore = workstore;
			_popMsg = popMsg;
		}
		
		public function get issueDate():Date {
			return _issueDate;
		}
		
		public function get workstore():Workstore {
			return _workstore;
		}
		
		public function get popMsg():Boolean {
			return _popMsg;
		}
	}
}