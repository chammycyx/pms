package hk.org.ha.event.pms.main.reftable {
	
	import hk.org.ha.model.pms.dms.persistence.DmDrug;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class RetrieveDosageConversionByMoDosageEvent extends AbstractTideEvent {
		private var _dmDrug:DmDrug;
		private var _prescribingDosage:Number;
		private var _moDosageUnit:String;

		public function RetrieveDosageConversionByMoDosageEvent(dmDrug:DmDrug, prescribingDosage:Number, moDosageUnit:String):void {
			super();
			_prescribingDosage = prescribingDosage;
			_moDosageUnit = moDosageUnit;
			_dmDrug = dmDrug;
		}
		
		public function get dmDrug():DmDrug
		{
			return _dmDrug;
		}
		
		public function get prescribingDosage():Number
		{
			return _prescribingDosage;
		}
		
		public function get moDosageUnit():String
		{
			return _moDosageUnit;
		}
	}
}