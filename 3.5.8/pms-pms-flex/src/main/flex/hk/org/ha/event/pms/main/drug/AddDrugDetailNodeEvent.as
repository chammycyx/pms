package hk.org.ha.event.pms.main.drug {
	
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class AddDrugDetailNodeEvent extends AbstractTideEvent 
	{			
		private var _drugSearchSource:DrugSearchSource;
		private var _drugSearchDetailMapList:ArrayCollection;
		
		public function AddDrugDetailNodeEvent(drugSearchSource:DrugSearchSource, drugSearchDetailMapList:ArrayCollection):void 
		{
			super();
			_drugSearchSource = drugSearchSource;
			_drugSearchDetailMapList = drugSearchDetailMapList;
		}
		
		public function get drugSearchSource():DrugSearchSource {
			return _drugSearchSource;
		}
		
		public function get drugSearchDetailMapList():ArrayCollection {
			return _drugSearchDetailMapList;
		}
	}
}