package hk.org.ha.event.pms.main.reftable.mp
{
	import hk.org.ha.model.pms.vo.reftable.mp.PmsMpSpecMappingInfo;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetPmsMpSpecMappingInfoEvent extends AbstractTideEvent 
	{
		private var _pmsMpSpecMappingInfo:PmsMpSpecMappingInfo;

		public function SetPmsMpSpecMappingInfoEvent(pmsMpSpecMappingInfo:PmsMpSpecMappingInfo):void 
		{
			super();
			_pmsMpSpecMappingInfo = pmsMpSpecMappingInfo;
		}
		
		public function get pmsMpSpecMappingInfo():PmsMpSpecMappingInfo
		{
			return _pmsMpSpecMappingInfo;
		}
	}
}