package hk.org.ha.event.pms.main.report {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveCapdVoucherRptEvent extends AbstractTideEvent 
	{
		private var _dateFrom:Date;
		private var _dateTo:Date;
		private var _hkid:String;
		private var _patName:String;
		private var _voucherNum:String;
		
		public function RetrieveCapdVoucherRptEvent( dateFrom:Date, dateTo:Date, hkid:String, patName:String, voucherNum:String):void 
		{
			super();
			_dateFrom = dateFrom;
			_dateTo = dateTo;
			_hkid = hkid;
			_patName = patName;
			_voucherNum = voucherNum;
		}

		public function get dateFrom():Date{
			return _dateFrom;
		} 
		
		public function get dateTo():Date{
			return _dateTo;
		}
		
		public function get hkid():String{
			return _hkid;
		}
		
		public function get patName():String{
			return _patName;
		}
		
		public function get voucherNum():String{
			return _voucherNum;
		}
	}
}