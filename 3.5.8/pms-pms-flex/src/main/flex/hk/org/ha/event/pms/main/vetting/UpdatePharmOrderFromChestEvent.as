package hk.org.ha.event.pms.main.vetting {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.disp.PharmOrder;
	
	public class UpdatePharmOrderFromChestEvent extends AbstractTideEvent {

		private var _pharmOrder:PharmOrder;
		private var _autoSave:Boolean;
		
		public function UpdatePharmOrderFromChestEvent(pharmOrder:PharmOrder, autoSave:Boolean=false) {
			super();
			_pharmOrder = pharmOrder;
			_autoSave = autoSave;
		}
		
		public function get pharmOrder():PharmOrder {
			return _pharmOrder;
		}
		
		public function get autoSave():Boolean {
			return _autoSave;
		}
	}
}
