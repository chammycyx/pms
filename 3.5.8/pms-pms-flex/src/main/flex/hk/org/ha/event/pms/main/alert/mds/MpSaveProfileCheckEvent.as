package hk.org.ha.event.pms.main.alert.mds {
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	
	public class MpSaveProfileCheckEvent extends AbstractTideEvent {
		
		private var _medProfileItemList:ListCollectionView;
		
		private var _medProfile:MedProfile;
		
		private var _successFunc:Function;
		
		private var _failureFunc:Function;
		
		public function MpSaveProfileCheckEvent(medProfileItemList:ListCollectionView, medProfile:MedProfile, successFunc:Function, failureFunc:Function) {
			super();
			_medProfileItemList = new ArrayCollection();
			_medProfileItemList.addAll(medProfileItemList);
			_medProfile = medProfile;
			_successFunc = successFunc;
			_failureFunc = failureFunc;
		}
		
		public function get medProfileItemList():ListCollectionView {
			return _medProfileItemList;
		}
		
		public function get medProfile():MedProfile {
			return _medProfile;
		}
		
		public function get successFunc():Function {
			return _successFunc;
		}
		
		public function get failureFunc():Function {
			return _failureFunc;
		}
		
		public function getByItemNum(itemNum:Number):MedProfileMoItem {
			for each (var medProfileItem:MedProfileItem in medProfileItemList) {
				if (medProfileItem.medProfileMoItem.itemNum == itemNum) {
					return medProfileItem.medProfileMoItem;
				}
			}
			return null;
		}
	}
}