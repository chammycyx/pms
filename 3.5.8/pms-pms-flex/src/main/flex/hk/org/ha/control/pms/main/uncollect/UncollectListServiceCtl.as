package hk.org.ha.control.pms.main.uncollect{
	
	import hk.org.ha.event.pms.main.uncollect.ClearDrugChargingUncollectViewEvent;
	import hk.org.ha.event.pms.main.uncollect.RetrieveEmptyUncollectOrderListEvent;
	import hk.org.ha.event.pms.main.uncollect.RetrieveUncollectOrderListEvent;
	import hk.org.ha.event.pms.main.uncollect.UpdateUncollectOrderListEvent;
	import hk.org.ha.model.pms.biz.uncollect.UncollectListServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("uncollectListServiceCtl", restrict="true")]
	public class UncollectListServiceCtl 
	{
		[In]
		public var uncollectListService:UncollectListServiceBean
		
		[Observer]
		public function retrieveUncollectOrderList(evt:RetrieveUncollectOrderListEvent):void
		{
			In(Object(uncollectListService).retrieveEmpty)
			uncollectListService.retrieveUncollectOrderList(retrieveUncollectOrderListResult);
		}
		
		private function retrieveUncollectOrderListResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RetrieveEmptyUncollectOrderListEvent((Object(uncollectListService).retrieveEmpty))); 
		}
		
		
		[Observer]
		public function updateUncollectOrderList(evt:UpdateUncollectOrderListEvent):void
		{
			uncollectListService.updateUncollectOrderList(evt.updateUncollectOrderList, updateUncollectOrderListResult);
		}
		
		private function updateUncollectOrderListResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new ClearDrugChargingUncollectViewEvent());
		}
	}
}
