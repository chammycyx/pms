package hk.org.ha.control.pms.main.drug {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.drug.RetrieveDmFormEvent;
	import hk.org.ha.model.pms.biz.drug.DmFormServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("dmFormServiceCtl", restrict="true")]
	public class DmFormServiceCtl 
	{
		[In]
		public var dmFormService:DmFormServiceBean;
		
		private var resultEvent:Event;
		
		[Observer]
		public function retrieveDmForm(evt:RetrieveDmFormEvent):void
		{
			resultEvent = evt.resultEvent;
			
			dmFormService.retrieveDmForm(evt.formCode, retrieveDmFormResult);
		}
		
		private function retrieveDmFormResult(evt:TideResultEvent):void 
		{
			if ( resultEvent!=null ) {
				evt.context.dispatchEvent( resultEvent );
			}
		}
		
	}
}
