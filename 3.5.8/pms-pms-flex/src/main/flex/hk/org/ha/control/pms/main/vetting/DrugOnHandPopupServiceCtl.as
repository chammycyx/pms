package hk.org.ha.control.pms.main.vetting
{
	import hk.org.ha.event.pms.main.vetting.popup.RetrieveDrugOnHandProfileEvent;
	import hk.org.ha.event.pms.main.vetting.popup.ShowDrugOnHandPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.ShowLoadingPopupEvent;
	import hk.org.ha.model.pms.biz.vetting.DrugOnHandListServiceBean;
	import hk.org.ha.model.pms.udt.vetting.CmsOrderSubType;
	import hk.org.ha.model.pms.udt.vetting.CmsOrderType;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("drugOnHandPopupServiceCtl", restrict="true")]
	public class DrugOnHandPopupServiceCtl
	{
		[In]
		public var drugOnHandListService:DrugOnHandListServiceBean; 
		
		[In]
		public var onHandProfileList:ArrayCollection;
		
		[In]
		public var ctx:Context;
		
		[Observer]
		public function retrieveDrugOnHandProfile(evt:RetrieveDrugOnHandProfileEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Loading ..."));
			drugOnHandListService.retrieveDrugOnHandProfileList(evt.onHandProfileCriteria, retrieveDrugOnHandProfileResult);
		}
		
		private function retrieveDrugOnHandProfileResult(evt:TideResultEvent):void 
		{
			dispatchEvent(new CloseLoadingPopupEvent());
			evt.context.dispatchEvent(new ShowDrugOnHandPopupEvent(onHandProfileList));
		}
	}
}