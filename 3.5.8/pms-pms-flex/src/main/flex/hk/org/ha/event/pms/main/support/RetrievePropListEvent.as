package hk.org.ha.event.pms.main.support
{
	import hk.org.ha.model.pms.persistence.corp.Hospital;
	import hk.org.ha.model.pms.udt.support.PropType;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class RetrievePropListEvent extends AbstractTideEvent 
	{
		private var _propType:PropType;
		private var _hospital:Hospital;
		private var _operationProfileIdd:Number;
		
		public function RetrievePropListEvent(propType:PropType, hospital:Hospital, operationProfileIdd:Number=NaN):void
		{
			super();
			_propType = propType;
			_hospital = hospital;
			_operationProfileIdd = operationProfileIdd;
		}
		
		public function get propType():PropType {
			return _propType;
		}
		
		public function get hospital():Hospital {
			return _hospital;
		}
		
		public function get operationProfileIdd():Number {
			return _operationProfileIdd;
		}
	}
}