package hk.org.ha.event.pms.main.checkissue
{
	import hk.org.ha.model.pms.vo.checkissue.BatchIssueDetail;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateBatchIssueLogDetailEvent extends AbstractTideEvent 
	{		
		private var _updateBatchIssueDetail:BatchIssueDetail;
		
		public function UpdateBatchIssueLogDetailEvent(updateBatchIssueDetail:BatchIssueDetail):void 
		{
			super();
			_updateBatchIssueDetail = updateBatchIssueDetail;
		}
		
		public function get updateBatchIssueDetail():BatchIssueDetail {
			return _updateBatchIssueDetail;
		}
	}
}