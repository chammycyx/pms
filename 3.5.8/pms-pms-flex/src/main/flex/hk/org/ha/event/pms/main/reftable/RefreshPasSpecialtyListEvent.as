package hk.org.ha.event.pms.main.reftable {
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshPasSpecialtyListEvent extends AbstractTideEvent 
	{
		private var _pasSpecialtyDisplayList:ArrayCollection;
		
		public function get pasSpecialtyDisplayList():ArrayCollection
		{
			return _pasSpecialtyDisplayList;
		}
		
		public function RefreshPasSpecialtyListEvent(pasSpecialtyDisplayList:ArrayCollection):void 
		{
			super();
			_pasSpecialtyDisplayList = pasSpecialtyDisplayList;
		}
	}
}