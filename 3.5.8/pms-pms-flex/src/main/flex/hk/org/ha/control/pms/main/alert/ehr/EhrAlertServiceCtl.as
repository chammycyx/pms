package hk.org.ha.control.pms.main.alert.ehr {
	
	import hk.org.ha.event.pms.main.alert.ehr.RetrieveEhrAllergyDetailEvent;
	import hk.org.ha.event.pms.main.alert.ehr.popup.ShowEhrAllergyDetailPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.ShowLoadingPopupEvent;
	import hk.org.ha.model.pms.biz.alert.ehr.EhrAlertServiceBean;
	import hk.org.ha.model.pms.vo.alert.ehr.EhrAllergyDetail;
	
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("ehrAlertServiceCtl", restrict="true")]
	public class EhrAlertServiceCtl {
		
		[In]
		public var ehrAlertService:EhrAlertServiceBean;
		
		[Observer]
		public function retrieveEhrAllergyDetail(evt:RetrieveEhrAllergyDetailEvent):void {
			dispatchEvent(new ShowLoadingPopupEvent("Loading eHR allergen and ADR ..."));
			ehrAlertService.retrieveEhrAllergyDetail(evt.patHospCode, evt.ehrNum, retrieveEhrAllergyDetailResult, retrieveEhrAllergyDetailFault);
		}
		
		private function retrieveEhrAllergyDetailResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			evt.context.dispatchEvent(new ShowEhrAllergyDetailPopupEvent(evt.result as EhrAllergyDetail));
		}
		
		private function retrieveEhrAllergyDetailFault(evt:TideFaultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
		}
	}
}
