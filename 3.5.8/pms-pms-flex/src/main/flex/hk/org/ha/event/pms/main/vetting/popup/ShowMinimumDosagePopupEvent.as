package hk.org.ha.event.pms.main.vetting.popup {

	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowMinimumDosagePopupEvent extends AbstractTideEvent {

		private var _sysMsgProp:SystemMessagePopupProp;
		
		public function ShowMinimumDosagePopupEvent(sysMsgProp:SystemMessagePopupProp) {
			super();
			_sysMsgProp = sysMsgProp;
		}
		
		public function get sysMsgProp():SystemMessagePopupProp {
			return _sysMsgProp;
		}
	}
}
