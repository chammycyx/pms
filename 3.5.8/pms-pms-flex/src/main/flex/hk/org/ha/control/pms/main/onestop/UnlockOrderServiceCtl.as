package hk.org.ha.control.pms.main.onestop
{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.onestop.CheckOrderIsLockedEvent;
	import hk.org.ha.event.pms.main.onestop.CheckOrderIsLockedResultEvent;
	import hk.org.ha.event.pms.main.onestop.RefreshUnvetOrderEvent;
	import hk.org.ha.event.pms.main.onestop.RetrieveOrderEvent;
	import hk.org.ha.event.pms.main.onestop.RetrieveOrderStatusEvent;
	import hk.org.ha.event.pms.main.onestop.ShowInfomationMessageEvent;
	import hk.org.ha.event.pms.main.onestop.UnLockOrderByRequestResultHandlerEvent;
	import hk.org.ha.event.pms.main.onestop.UnlockOrderByRequestEvent;
	import hk.org.ha.event.pms.main.onestop.UnvetOrderByRequestEvent;
	import hk.org.ha.model.pms.biz.onestop.OneStopServiceBean;
	import hk.org.ha.model.pms.biz.onestop.UnlockOrderServiceBean;
	import hk.org.ha.model.pms.biz.vetting.VettingServiceBean;
	import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
	
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;

	[Bindable]
	[Name("unlockOrderServiceCtl", restrict="true")]
	public class UnlockOrderServiceCtl
	{
		[In]
		public var unlockOrderService:UnlockOrderServiceBean;
		
		[In]
		public var vettingService:VettingServiceBean;
		
		[In]
		public var oneStopService:OneStopServiceBean;
		
		[In]
		public var errMsg:String;
		
		private var checkOrderIsLockedEvent:CheckOrderIsLockedEvent;
		private var unvetOrderByRequestEvent:UnvetOrderByRequestEvent;
		private var retrieveOrderStatusEvent:RetrieveOrderStatusEvent;
		
		[Observer]
		public function checkOrderIsLocked(evt:CheckOrderIsLockedEvent):void {
			checkOrderIsLockedEvent = evt;
			unlockOrderService.checkOrderIsLocked(evt.orderId, evt.medOrderVersion, evt.refillOrderId, evt.requiredUnlock, checkOrderIsLockedResult);
		}
		
		private function checkOrderIsLockedResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new CheckOrderIsLockedResultEvent(
				checkOrderIsLockedEvent.followUpFunc, checkOrderIsLockedEvent.followUpFuncAfterFollowUpFunc));
		}
		
		[Observer]
		public function unlockOrderByRequest(evt:UnlockOrderByRequestEvent):void {
			unlockOrderService.unlockOrderByRequest(evt.orderId, evt.medOrderVersion, evt.refillOrderId, unlockOrderByRequestResult);
		}
		
		private function unlockOrderByRequestResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new UnLockOrderByRequestResultHandlerEvent());
		}
		
		[Observer]
		public function unvetOrderByRequest(evt:UnvetOrderByRequestEvent):void {
			unvetOrderByRequestEvent = evt;
			vettingService.unvetOrder(evt.orderId, unvetOrderByRequestResult);
		}
		
		private function unvetOrderByRequestResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new RefreshUnvetOrderEvent("MOE" + unvetOrderByRequestEvent.orderNum));
		}
		
		[Observer]
		public function retrieveOrderStatus(evt:RetrieveOrderStatusEvent):void {
			retrieveOrderStatusEvent = evt;
			unlockOrderService.validateMedOrder(evt.orderId, evt.medOrderVersion, evt.refillOrderId, validateMedOrderResult);
		}
		
		private function validateMedOrderResult(evt:TideResultEvent):void {
			if (errMsg == null)
			{
				evt.context.dispatchEvent(retrieveOrderStatusEvent.callbackEvent);
			}
			else
			{
				evt.context.dispatchEvent(new ShowInfomationMessageEvent(new Array(errMsg), true));
			}
		}
			
	}
}
