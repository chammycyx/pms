package hk.org.ha.event.pms.main.vetting {
	
	import flash.events.Event;
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RemoveOrderItemEvent extends AbstractTideEvent {
		
		private var _itemIndex:int;
		private var _callbackEvent:Event;
		private var _remarkCreateDate:Date;
		private var _remarkCreateUser:String;
		private var _remarkText:String;
		private var _callbackFunc:Function;
		
		public function RemoveOrderItemEvent(itemIndex:int, callbackEvent:Event=null) {
			super();
			_itemIndex = itemIndex;
			_callbackEvent = callbackEvent;
		}

		public function get callbackEvent():Event {
			return _callbackEvent;
		}
		
		public function get itemIndex():int {
			return _itemIndex;
		}

		public function set remarkCreateDate(remarkCreateDate:Date):void {
			_remarkCreateDate = remarkCreateDate;
		}
		
		public function get remarkCreateDate():Date {
			return _remarkCreateDate;
		}

		public function set remarkCreateUser(remarkCreateUser:String):void {
			_remarkCreateUser = remarkCreateUser;
		}
		
		public function get remarkCreateUser():String {
			return _remarkCreateUser;
		}

		public function set remarkText(remarkText:String):void {
			_remarkText = remarkText;
		}
		
		public function get remarkText():String {
			return _remarkText;
		}
		
		public function set callbackFunc(callbackFunc:Function):void {
			_callbackFunc = callbackFunc;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}
