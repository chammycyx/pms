package hk.org.ha.control.pms.main.report {
	import hk.org.ha.event.pms.main.report.PrintUncollectPrescRptEvent;
	import hk.org.ha.event.pms.main.report.RefreshUncollectPrescRptEvent;
	import hk.org.ha.event.pms.main.report.RetrieveUncollectPrescRptEvent;
	import hk.org.ha.event.pms.main.report.RetrieveUncollectPrescRptMaxDayRangeEvent;
	import hk.org.ha.event.pms.main.report.SetUncollectPrescRptMaxDayRangeEvent;
	import hk.org.ha.model.pms.biz.report.UncollectPrescRptServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	
	[Bindable]
	[Name("uncollectPrescRptServiceCtl", restrict="true")]
	public class UncollectPrescRptServiceCtl 
	{
		[In]
		public var uncollectPrescRptService:UncollectPrescRptServiceBean;
		
		[Observer]
		public function retrieveUncollectPrescRpt(evt:RetrieveUncollectPrescRptEvent):void
		{
			uncollectPrescRptService.retrieveUncollectPrescRptList(evt.dateRange, retrieveUncollectPrescRptResult);
		}
		
		private function retrieveUncollectPrescRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshUncollectPrescRptEvent(evt.result as ArrayCollection) );
		}
		
		[Observer]
		public function printUncollectPrescRpt(evt:PrintUncollectPrescRptEvent):void
		{
			uncollectPrescRptService.printUncollectPrescRpt(evt.uncollectPrescRptList);
		}
		
		[Observer]
		public function retrieveUncollectPrescRptMaxDayRange(evt:RetrieveUncollectPrescRptMaxDayRangeEvent):void
		{
			uncollectPrescRptService.getMaxDayRange(retrieveUncollectPrescRptMaxDayRangeResult);
		}
		
		private function retrieveUncollectPrescRptMaxDayRangeResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new SetUncollectPrescRptMaxDayRangeEvent(evt.result as Number) );
		}
		
	}
}
