package hk.org.ha.view.pms.main.medprofile
{
	import spark.components.supportClasses.ItemRenderer;
	
	public class ItemRenderer extends spark.components.supportClasses.ItemRenderer
	{
		public function ItemRenderer()
		{
			super();
		}
		
		override protected function set hovered(hovered:Boolean):void
		{
			if (hasState("hovered"))
				super.hovered = hovered;
		}
		
	}
}