package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dms.persistence.DmRegimen;
	import hk.org.ha.model.pms.dms.vo.CommonOrder;
	import hk.org.ha.model.pms.dms.vo.DrugName;
	import hk.org.ha.model.pms.dms.vo.PivasFormula;
	import hk.org.ha.model.pms.dms.vo.PreparationProperty;
	import hk.org.ha.model.pms.dms.vo.RouteForm;
	import hk.org.ha.model.pms.udt.drug.DrugScope;
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CreateSelectedMpDrugEvent extends AbstractTideEvent 
	{			
		private var _drugSearchSource:DrugSearchSource;
		private var _drugScope:DrugScope;
		private var _drugName:DrugName;
		private var _routeForm:RouteForm;
		private var _preparation:PreparationProperty;
		private var _commonOrder:CommonOrder;
		private var _pivasFormula:PivasFormula;
		private var _dmRegimen:DmRegimen;
		private var _pasSpecialty:String;
		private var _pasSubSpecialty:String;
		private var _itemCode:String;
		private var _preparationOkHandler:Function;
		private var _preparationCancelHandler:Function;
		private var _okCallbackEvent:Event;
		private var _cancelCallbackEvent:Event;

		public function CreateSelectedMpDrugEvent():void 
		{
			super();
		}

		public function get drugSearchSource():DrugSearchSource
		{
			return _drugSearchSource;
		}

		public function set drugSearchSource(drugSearchSource:DrugSearchSource):void
		{
			_drugSearchSource = drugSearchSource;
		}
		
		public function get drugName():DrugName
		{
			return _drugName;
		}

		public function set drugName(drugName:DrugName):void
		{
			_drugName = drugName;
		}
		
		public function get routeForm():RouteForm
		{
			return _routeForm;
		}

		public function set routeForm(routeForm:RouteForm):void
		{
			_routeForm = routeForm;
		}
		
		public function get preparation():PreparationProperty
		{
			return _preparation;
		}

		public function set preparation(preparation:PreparationProperty):void
		{
			_preparation = preparation;
		}
		
		public function get commonOrder():CommonOrder
		{
			return _commonOrder;
		}

		public function set commonOrder(commonOrder:CommonOrder):void
		{
			_commonOrder = commonOrder;
		}

		public function get pivasFormula():PivasFormula
		{
			return _pivasFormula;
		}
		
		public function set pivasFormula(pivasFormula:PivasFormula):void
		{
			_pivasFormula = pivasFormula;
		}
		
		public function get dmRegimen():DmRegimen
		{
			return _dmRegimen;
		}

		public function set dmRegimen(dmRegimen:DmRegimen):void
		{
			_dmRegimen = dmRegimen;
		}
		
		public function get drugScope():DrugScope
		{
			return _drugScope;
		}
		
		public function set drugScope(drugScope:DrugScope):void
		{
			_drugScope = drugScope;
		}
		
		public function get pasSpecialty():String
		{
			return _pasSpecialty;
		}
		
		public function set pasSpecialty(pasSpecialty:String):void
		{
			_pasSpecialty = pasSpecialty;
		}

		public function get pasSubSpecialty():String
		{
			return _pasSubSpecialty;
		}
		
		public function set pasSubSpecialty(pasSubSpecialty:String):void
		{
			_pasSubSpecialty = pasSubSpecialty;
		}
		
		public function set itemCode(itemCode:String):void
		{
			_itemCode = itemCode;
		}

		public function get itemCode():String
		{
			return _itemCode;
		}
		
		public function get preparationOkHandler():Function
		{
			return _preparationOkHandler;
		}
		
		public function set preparationOkHandler(preparationOkHandler:Function):void
		{
			_preparationOkHandler = preparationOkHandler;
		}
		
		public function get preparationCancelHandler():Function
		{
			return _preparationCancelHandler;
		}
		
		public function set preparationCancelHandler(preparationCancelHandler:Function):void
		{
			_preparationCancelHandler = preparationCancelHandler;
		}
		
		public function get okCallbackEvent():Event
		{
			return _okCallbackEvent;
		}
		
		public function set okCallbackEvent(okCallbackEvent:Event):void
		{
			_okCallbackEvent = okCallbackEvent;
		}
		
		public function get cancelCallbackEvent():Event
		{
			return _cancelCallbackEvent;
		}
		
		public function set cancelCallbackEvent(cancelCallbackEvent:Event):void
		{
			_cancelCallbackEvent = cancelCallbackEvent;
		}
		
		public override function clone():Event {
			var evt:CreateSelectedMpDrugEvent  = new CreateSelectedMpDrugEvent();
			
			evt.drugSearchSource			= drugSearchSource;
			evt.drugScope					= drugScope;
			evt.drugName					= drugName;
			evt.routeForm					= routeForm;
			evt.preparation					= preparation;
			evt.commonOrder					= commonOrder;
			evt.pivasFormula				= pivasFormula;
			evt.dmRegimen					= dmRegimen;
			evt.pasSpecialty				= pasSpecialty;
			evt.pasSubSpecialty				= pasSubSpecialty;
			evt.itemCode					= itemCode;
			evt.preparationOkHandler		= preparationOkHandler;
			evt.preparationCancelHandler	= preparationCancelHandler;
			evt.okCallbackEvent				= okCallbackEvent;
			evt.cancelCallbackEvent			= cancelCallbackEvent;
			
			return evt;
		}		
	}
}