package hk.org.ha.event.pms.main.onestop {
		
	import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
	
	import org.granite.tide.events.AbstractTideEvent;
		
	public class CheckNormalOrderFcsClearanceEvent extends AbstractTideEvent 
	{
		private var _orderId:Number;
		private var _fcsForceProceedRequireYes:Function;
		private var _fcsForceProceedRequireNo:Function;
		private var _fcsForceProceedNotRequire:Function;
		
		public function CheckNormalOrderFcsClearanceEvent(orderId:Number, fcsForceProceedRequireYes:Function, 
														  fcsForceProceedRequireNo:Function, fcsForceProceedNotRequire:Function):void 
		{
			super();
			_orderId = orderId;
			_fcsForceProceedRequireYes = fcsForceProceedRequireYes;
			_fcsForceProceedRequireNo = fcsForceProceedRequireNo;
			_fcsForceProceedNotRequire = fcsForceProceedNotRequire;
		}   
		
		public function get orderId():Number
		{
			return _orderId;
		}
		
		public function get fcsForceProceedRequireYes():Function
		{
			return _fcsForceProceedRequireYes;
		}
		
		public function get fcsForceProceedRequireNo():Function
		{
			return _fcsForceProceedRequireNo;
		}
		
		public function get fcsForceProceedNotRequire():Function
		{
			return _fcsForceProceedNotRequire;
		}
	}
}