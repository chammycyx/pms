package hk.org.ha.event.pms.main.enquiry
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveOrderPendingSuspendLogEnqListEvent extends AbstractTideEvent 
	{
		private var _fromDate:Date;
		private var _toDate:Date;
		private var _caseNum:String;
		private var _filter:String;
		
		
		public function RetrieveOrderPendingSuspendLogEnqListEvent(fromDate:Date, toDate:Date, caseNum:String, filter:String):void
		{
			super();
			_fromDate = fromDate;
			_toDate = toDate;
			_caseNum = caseNum;
			_filter = filter;
		}
		
		public function get fromDate():Date{
			return _fromDate;
		}
		
		public function get toDate():Date{
			return _toDate;
		}
		
		public function get caseNum():String{
			return _caseNum;
		}
		
		public function get filter():String{
			return _filter;
		}
	}
}