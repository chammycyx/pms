package hk.org.ha.event.pms.main.report {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	import hk.org.ha.model.pms.vo.alert.AlertProfile;
	
	import mx.collections.ListCollectionView;
	
	public class RetrieveMdsExceptionRptByMedProfileAlertMsgEvent extends AbstractTideEvent {
		
		private var _medProfile:MedProfile;
		
		private var _medProfileAlertMsgList:ListCollectionView;
		
		private var _alertProfile:AlertProfile;
		
		public function RetrieveMdsExceptionRptByMedProfileAlertMsgEvent(medProfile:MedProfile, medProfileAlertMsgList:ListCollectionView, alertProfile:AlertProfile) {
			super();
			_medProfile = medProfile;
			_medProfileAlertMsgList = medProfileAlertMsgList
			_alertProfile = alertProfile;
		}
		
		public function get medProfile():MedProfile {
			return _medProfile;
		}
		
		public function get medProfileAlertMsgList():ListCollectionView {
			return _medProfileAlertMsgList;
		}
		
		public function get alertProfile():AlertProfile {
			return _alertProfile;
		}
	}
}