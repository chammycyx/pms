package hk.org.ha.event.pms.main.enquiry {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDmFmDrugEvent extends AbstractTideEvent 
	{
		private var _itemCode:String;
		
		public function RetrieveDmFmDrugEvent(itemCode:String):void 
		{
			super();
			_itemCode = itemCode;
		}
		
		public function get itemCode():String
		{
			return _itemCode;
		}

	}
}