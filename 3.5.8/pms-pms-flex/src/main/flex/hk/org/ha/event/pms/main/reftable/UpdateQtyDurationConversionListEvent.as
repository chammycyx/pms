package hk.org.ha.event.pms.main.reftable {
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateQtyDurationConversionListEvent extends AbstractTideEvent 
	{
		private var _updatePmsQtyConversionList:ListCollectionView;
		private var _qtyApplyAll:Boolean;
		private var _updatePmsDurationConversionList:ListCollectionView;
		private var _durationApplyAll:Boolean;
		
		public function UpdateQtyDurationConversionListEvent(updatePmsQtyConversionList:ListCollectionView, qtyApplyAll:Boolean, 
															 updatePmsDurationConversionList:ListCollectionView, durationApplyAll:Boolean):void 
		{
			super();
			_updatePmsQtyConversionList = updatePmsQtyConversionList;
			_qtyApplyAll = qtyApplyAll;
			_updatePmsDurationConversionList = updatePmsDurationConversionList;
			_durationApplyAll = durationApplyAll;
		}
		
		public function get updatePmsQtyConversionList():ListCollectionView
		{
			return _updatePmsQtyConversionList;
		}
		
		public function get qtyApplyAll():Boolean 
		{
			return _qtyApplyAll;
		}
		
		public function get updatePmsDurationConversionList():ListCollectionView
		{
			return _updatePmsDurationConversionList;
		}
		
		public function get durationApplyAll():Boolean 
		{
			return _durationApplyAll;
		}
	}
}