package hk.org.ha.event.pms.main.vetting.mp
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveCurrentMedicationListEvent extends AbstractTideEvent
	{
		public function RetrieveCurrentMedicationListEvent()
		{
			super();
		}
	}
}