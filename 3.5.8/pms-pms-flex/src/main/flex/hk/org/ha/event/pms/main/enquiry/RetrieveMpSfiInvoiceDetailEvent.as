package hk.org.ha.event.pms.main.enquiry
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveMpSfiInvoiceDetailEvent extends AbstractTideEvent 
	{
		private var _sfiInvoiceId:Number;		
		private var _purchaseReqId:Number;
		private var _isPreview:Boolean;
		
		public function RetrieveMpSfiInvoiceDetailEvent(sfiInvoiceId:Number, purchaseReqId:Number, isPreview:Boolean):void
		{
			super();
			_sfiInvoiceId = sfiInvoiceId;
			_purchaseReqId = purchaseReqId;
			_isPreview = isPreview;
		}
		
		public function get sfiInvoiceId():Number
		{
			return _sfiInvoiceId;
		}
		
		public function get purchaseReqId():Number
		{
			return _purchaseReqId;
		}
		
		public function get isPreview():Boolean
		{
			return _isPreview;
		}
	}
}