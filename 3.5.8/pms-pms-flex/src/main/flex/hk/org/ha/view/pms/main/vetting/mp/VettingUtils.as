package hk.org.ha.view.pms.main.vetting.mp
{
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	import mx.utils.ObjectUtil;
	
	import spark.collections.Sort;
	
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
	import hk.org.ha.model.pms.persistence.medprofile.PurchaseRequest;
	import hk.org.ha.model.pms.persistence.medprofile.Replenishment;
	import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
	import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
	import hk.org.ha.model.pms.vo.chemo.ChemoInfo;
	import hk.org.ha.view.pms.main.medprofile.MedProfileUtils;

	public class VettingUtils
	{
		public static function addToInactiveList(inactiveList:ListCollectionView, medProfileMoItem:MedProfileMoItem):void
		{
			inactiveList.addItemAt(medProfileMoItem, searchInsertPos(inactiveList, medProfileMoItem));
			var firstManualItem:MedProfileMoItem = findFirstManualItem(inactiveList);
			for each (var currentItem:MedProfileMoItem in inactiveList) {
				currentItem.firstManualItem = currentItem === firstManualItem;
			}
		}
		
		public static function inactiveListCompareFunction(medProfileMoItem1:MedProfileMoItem, medProfileMoItem2:MedProfileMoItem, array:Array = null):int 
		{
			if (medProfileMoItem1 == null || medProfileMoItem2 == null) {
				return 0;
			}
			
			var result:int = ObjectUtil.compare(medProfileMoItem1.isManualItem, medProfileMoItem2.isManualItem);
			if (result != 0) {
				return result;
			}
			
			result = ObjectUtil.dateCompare(extractDateForComparison(medProfileMoItem1), extractDateForComparison(medProfileMoItem2));
			if (result != 0) {
				return -1 * result;
			}
			
			return -1 * ObjectUtil.dateCompare(medProfileMoItem1.orderDate, medProfileMoItem2.orderDate);
		}
		
		public static function isActiveMedProfileItem(medProfileItem:MedProfileItem):Boolean {
			return medProfileItem.status == MedProfileItemStatus.Unvet || medProfileItem.status == MedProfileItemStatus.Vetted || medProfileItem.status == MedProfileItemStatus.Verified;
		}
		
		private static function extractDateForComparison(medProfileMoItem:MedProfileMoItem):Date
		{
			var medProfileItem:MedProfileItem = medProfileMoItem.medProfileItem;
			var resultDate:Date;
			
			if (!medProfileMoItem.isManualItem && medProfileMoItem.transferFlag == true && medProfileItem != null && medProfileMoItem === medProfileItem.medProfileMoItem &&
				(medProfileItem.status == MedProfileItemStatus.Unvet || medProfileItem.status == MedProfileItemStatus.Vetted || medProfileItem.status == MedProfileItemStatus.Verified)) {
				resultDate = medProfileMoItem.statusUpdateDate;
			} else {
				resultDate = medProfileMoItem.endDate;
			}
			
			return resultDate == null ? medProfileMoItem.orderDate : resultDate;
		}
		
		private static function searchInsertPos(inactiveList:ListCollectionView, medProfileMoItem:MedProfileMoItem):int
		{
			for (var i:int = 0; i < inactiveList.length; i++) {
				if (inactiveListCompareFunction(inactiveList.getItemAt(i) as MedProfileMoItem, medProfileMoItem) > 0) {
					return i;
				}
			}			
			return inactiveList.length;
		}
		
		private static function findFirstManualItem(inactiveList:ListCollectionView):MedProfileMoItem
		{
			for each (var medProfileMoItem:MedProfileMoItem in inactiveList) {
				if (medProfileMoItem.isManualItem) {
					return medProfileMoItem;
				}
			}
			return null;
		}
		
		public static function resetReplenishment(replenishment:Replenishment, privateFlag:Boolean, toDoListDictionary:Dictionary,
												  activeListDictionary:Dictionary, purchaseRequestList:ArrayCollection):void
		{
			if (replenishment == null) {
				return;
			}
			
			replenishment.dispFlag = false;
			replenishment.updateIssueQtyToEmpty();
			replenishment.clearErrorString();
			
			clearPurchaseRequestForReplenishment(replenishment, toDoListDictionary, activeListDictionary, purchaseRequestList);
		}
		
		public static function clearPurchaseRequestForReplenishment(replenishment:Replenishment, toDoListDictionary:Dictionary,
																	 activeListDictionary:Dictionary, purchaseRequestList:ArrayCollection):void
		{
			var deletePrList:ArrayCollection = new ArrayCollection();
			for each (var purchaseRequest:PurchaseRequest in purchaseRequestList) {
				if (purchaseRequest.printDate != null) {
					continue;
				}
				if (purchaseRequest.invoiceStatus == InvoiceStatus.Void && !purchaseRequest.markDelete) {
					continue;
				}
				
				if (purchaseRequest.replenishmentOrgId == replenishment.orgId) {
					deletePrList.addItem(purchaseRequest);
				}
			}
			
			for each (var purchaseRequest:PurchaseRequest in deletePrList) {
				var mpiId:Number = purchaseRequest.mpPharmInfo.medProfileItemId;
				var mpPoiId:Number = purchaseRequest.mpPharmInfo.medProfilePoItemId;
				var mpPoiUuid:String = purchaseRequest.mpPharmInfo.medProfilePoItemUuid;
				deletePurchaseRequest(purchaseRequest, purchaseRequestList);
				MedProfileUtils.updateEditedPurchaseRequestExistFlag(mpiId, mpPoiId, mpPoiUuid, toDoListDictionary, activeListDictionary, purchaseRequestList);
			}
			
			if (deletePrList.length > 0) {
				purchaseRequestList.refresh();
			}
		}
		
		private static function deletePurchaseRequest(purchaseRequest:PurchaseRequest, purchaseRequestList:ArrayCollection):Boolean
		{
			var index:int = purchaseRequestList.getItemIndex(purchaseRequest);
			if (index == -1) {
				return false;
			}
			
			purchaseRequest.mpPharmInfo.medProfilePoItemId = Number.NaN;
			purchaseRequest.medProfilePoItemId = Number.NaN;
			purchaseRequestList.removeItemAt(index);
			
			return true;
		}
		
		public static function buildProtocolMap(chemoMedProfileList:ArrayCollection):Dictionary
		{
			var protocolMap:Dictionary = new Dictionary();
			for each (var medProfile:MedProfile in chemoMedProfileList) {
				protocolMap[medProfile.id] = medProfile;
			}
			return protocolMap;
		}
		
		public static function buildCycleMap(protocolMap:Dictionary, medProfileItemList:ArrayCollection, activeOnly:Boolean):Dictionary
		{
			var medProfile:MedProfile;
			var cycleMap:Dictionary = new Dictionary();
			
			for each (var medProfileItem:MedProfileItem in medProfileItemList) {
				
				var medProfileMoItem:MedProfileMoItem = medProfileItem.medProfileMoItem;
				
				if (isNaN(medProfileMoItem.cycle) || !VettingUtils.isActiveMedProfileItem(medProfileItem) || protocolMap[medProfileItem.medProfileId] === undefined) {
					continue;
				}
				
				medProfile = protocolMap[medProfileItem.medProfileId];
				
				if (cycleMap[medProfile] === undefined) {
					cycleMap[medProfile] = constructSortedArrayCollection(cycleInfoCompareFunction);
				}
				
				var cycleInfoList:ArrayCollection = cycleMap[medProfile];
				
				if (findCycle(cycleInfoList, medProfileMoItem.cycle) == null) {
					cycleInfoList.addItem(constructCycleInfo(medProfileMoItem));
				}
			}
			return cycleMap;
		}
		
		private static function cycleInfoCompareFunction(cycleInfo1:Object, cycleInfo2:Object, array:Array = null):int
		{
			return ObjectUtil.compare(cycleInfo1.cycle, cycleInfo2.cycle);
		}
		
		private static function protocolInfoCompareFunction(protocolInfo1:Object, protocolInfo2:Object, array:Array = null):int
		{
			return ObjectUtil.compare(protocolInfo1.cycleStartDate, protocolInfo2.cycleStartDate);
		}
		
		private static function findCycle(cycleInfoList:ArrayCollection, cycle:Number):Object
		{
			for each (var cycleInfo:Object in cycleInfoList) {
				if (cycleInfo.cycle == cycle) {
					return cycleInfo;
				}
			}
			return null;
		}
		
		private static function constructCycleInfo(medProfileMoItem:MedProfileMoItem):Object
		{
			var newCycleInfo:Object = new Object();
			newCycleInfo.cycle = medProfileMoItem.cycle;
			newCycleInfo.cycleStartDate = medProfileMoItem.cycleStartDate;				
			return newCycleInfo;				
		}
		
		public static function buildCycleSelectionList(cycleMap:Dictionary):ArrayCollection
		{
			var protocolInfoList:ArrayCollection = constructSortedArrayCollection(protocolInfoCompareFunction);
			
			for (var key:Object in cycleMap) {
				var newProtocolInfo:Object = new Object();
				newProtocolInfo.medProfile = key;
				newProtocolInfo.cycleInfoList = cycleMap[key];
				protocolInfoList.addItem(newProtocolInfo);
			}
			return protocolInfoList;
		}
		
		private static function constructSortedArrayCollection(compareFunction:Function):ArrayCollection
		{
			var list:ArrayCollection = new ArrayCollection();
			list.sort = new Sort();
			list.sort.compareFunction = compareFunction;
			list.refresh();
			return list;
		}
		
		public static function getDefaultChemoItemFilter(chemoMedProfileList:ArrayCollection, medProfileItemList:ArrayCollection):Object
		{
			var cycleSelectionList:ArrayCollection = buildCycleSelectionList(buildCycleMap(buildProtocolMap(chemoMedProfileList), medProfileItemList, true));
			if (cycleSelectionList.length <= 0) {
				return null;
			}
			
			var cycleSelection:Object = cycleSelectionList.getItemAt(0);
			var itemFilter:Object = new Object();
			itemFilter.medProfile = cycleSelection.medProfile;
			itemFilter.cycle = cycleSelection.cycleInfoList.getItemAt(0).cycle;
			return itemFilter;
		}
		
		public static function containsChemoItem(chemoInfo:ChemoInfo):Boolean
		{
			return chemoInfo != null && !MedProfileUtils.isEmpty(chemoInfo.chemoItemList);
		}
		
		public static function isSplittable(chemoInfo:ChemoInfo):Boolean
		{
			return containsChemoItem(chemoInfo) && chemoInfo.chemoItemList.length > 1;
		}
	}
}