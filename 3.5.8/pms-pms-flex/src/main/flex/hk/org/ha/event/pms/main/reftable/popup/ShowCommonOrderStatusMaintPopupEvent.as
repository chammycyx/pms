package hk.org.ha.event.pms.main.reftable.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowCommonOrderStatusMaintPopupEvent extends AbstractTideEvent 
	{
		private var _commonOrderStatusMaintPopupProp:CommonOrderStatusMaintPopupProp;
		
		public function ShowCommonOrderStatusMaintPopupEvent(commonOrderStatusMaintPopupProp:CommonOrderStatusMaintPopupProp):void 
		{
			super();
			_commonOrderStatusMaintPopupProp = commonOrderStatusMaintPopupProp;
		}
		
		public function get commonOrderStatusMaintPopupProp():CommonOrderStatusMaintPopupProp{
			return _commonOrderStatusMaintPopupProp;
		}

	}
}