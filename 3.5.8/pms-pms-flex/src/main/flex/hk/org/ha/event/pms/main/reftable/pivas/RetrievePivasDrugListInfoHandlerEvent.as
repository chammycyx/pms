package hk.org.ha.event.pms.main.reftable.pivas {
	
	import hk.org.ha.model.pms.vo.reftable.mp.FixedConcnStatusInfo;
	import hk.org.ha.model.pms.vo.reftable.pivas.PivasDrugListInfo;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePivasDrugListInfoHandlerEvent extends AbstractTideEvent 
	{
		private var _pivasDrugListInfo:PivasDrugListInfo;
		
		public function RetrievePivasDrugListInfoHandlerEvent(pivasDrugListInfo:PivasDrugListInfo):void 
		{
			_pivasDrugListInfo = pivasDrugListInfo;
			super();
		}
		
		public function get pivasDrugListInfo():PivasDrugListInfo
		{
			return _pivasDrugListInfo;
		}
	}
}