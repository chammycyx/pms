package hk.org.ha.view.pms.main.inbox
{
	[Bindable]
	public interface TabViewSupport
	{
		function showView(tabIndex:Number, fromVetting:Boolean, triggerByTabBar:Boolean):void;
		function hideView(triggerByTabBar:Boolean):void;
		function reload(tabIndex:Number):void;
		function get reloadOnReceiveNotification():Boolean;
		function get supportShowAllWards():Boolean;
		function showAllWards(showAllWards:Boolean):void;
		function get supportShowChemoProfileOnly():Boolean;
		function showChemoProfileOnly(showChemoProfileOnly:Boolean):void;
		function get supportSorting():Boolean;
		function get sortPopupState():String;
		function sort(sortType:String):void;
		function get supportRetrieve():Boolean;
		function retrieve():void;
		function get supportClear():Boolean;
		function clear():void;
	}
}