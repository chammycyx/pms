package hk.org.ha.event.pms.main.vetting.show {
	
	import flash.events.Event;
	import flash.utils.Dictionary;
	
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowOrderEditViewEvent extends AbstractTideEvent 
	{
		private var _moiSelectedIndex:Number;
		
		private var _properties:Dictionary;
		
		private var _clearMessages:Boolean;
		
		public function ShowOrderEditViewEvent(moiSelectedIndex:Number=NaN, propertiesValue:Dictionary=null, /*autoAddDrug:Boolean=false, bypassSaveItemAndPharmRemark:Boolean=false,*/ clearMessages:Boolean=true):void 
		{
			super();
			_moiSelectedIndex = moiSelectedIndex;
			if ( propertiesValue != null ) {
				_properties = propertiesValue;
			} else {
				_properties = new Dictionary;				
			}
			_properties["selectedIndex"] = _moiSelectedIndex;
			_clearMessages = clearMessages;
			
		}
		
		public function get moiSelectedIndex():Number {
			return _moiSelectedIndex;
		}		
		
		public function get properties():Dictionary {
			return _properties;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
		
		public override function clone():Event {
			var newProperties:Dictionary = new Dictionary();
			for (var key:Object in properties) {
				newProperties[key] = properties[key];
			}
			return new ShowOrderEditViewEvent(moiSelectedIndex, newProperties, clearMessages);
		}
	}
}