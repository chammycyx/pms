package hk.org.ha.event.pms.main.onestop.show
{
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowOneStopEntryLastViewEvent extends AbstractTideEvent
	{
		public function ShowOneStopEntryLastViewEvent()
		{
			super();
		}
	}
}