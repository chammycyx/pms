package hk.org.ha.event.pms.main.inbox
{
	import hk.org.ha.model.pms.udt.medprofile.MedProfileStatOrderType;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CategoryButtonClickEvent extends AbstractTideEvent
	{
		private var _orderType:MedProfileStatOrderType;
		
		public function CategoryButtonClickEvent(orderType:MedProfileStatOrderType)
		{
			super();
			_orderType = orderType;
		}
		
		public function get orderType():MedProfileStatOrderType
		{
			return _orderType;
		}
	}
}