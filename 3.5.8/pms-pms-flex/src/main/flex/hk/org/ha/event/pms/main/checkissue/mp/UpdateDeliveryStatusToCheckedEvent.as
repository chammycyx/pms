package hk.org.ha.event.pms.main.checkissue.mp
{
	import hk.org.ha.model.pms.vo.checkissue.mp.CheckDelivery;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateDeliveryStatusToCheckedEvent extends AbstractTideEvent 
	{			
		private var _checkDelivery:CheckDelivery;
		private var _printRptFlag:Boolean;
		
		public function UpdateDeliveryStatusToCheckedEvent(checkDelivery:CheckDelivery, printRptFlag:Boolean):void 
		{
			super();
			_checkDelivery = checkDelivery;
			_printRptFlag = printRptFlag;
		}
		
		public function get checkDelivery():CheckDelivery {
			return _checkDelivery;
		}
		
		public function get printRptFlag():Boolean {
			return _printRptFlag;
		}
	}
}