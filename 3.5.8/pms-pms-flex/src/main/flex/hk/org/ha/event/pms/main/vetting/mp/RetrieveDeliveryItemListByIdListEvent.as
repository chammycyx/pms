package hk.org.ha.event.pms.main.vetting.mp
{
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDeliveryItemListByIdListEvent extends AbstractTideEvent
	{
		private var _medProfileMoItem:MedProfileMoItem;
		private var _deliveryItemIdList:ArrayCollection;
		private var _callback:Function;
		
		public function RetrieveDeliveryItemListByIdListEvent(medProfileMoItem:MedProfileMoItem, deliveryItemIdList:ArrayCollection,
													  callback:Function)
		{
			super();
			_medProfileMoItem = medProfileMoItem;
			_deliveryItemIdList = deliveryItemIdList;
			_callback = callback;
		}
		
		public function get medProfileMoItem():MedProfileMoItem
		{
			return _medProfileMoItem;
		}
		
		public function get deliveryItemIdList():ArrayCollection
		{
			return _deliveryItemIdList;
		}
		
		public function get callback():Function
		{
			return _callback;
		}
	}
}