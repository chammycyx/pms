package hk.org.ha.event.pms.main.alert.mds {
	
	import mx.collections.ListCollectionView;
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
	
	public class UpdateAlertResultListForSaveProfileEvent extends AbstractTideEvent {
		
		private var _medProfileMoItemList:ListCollectionView;
		
		private var _callback:Function;
				
		public function UpdateAlertResultListForSaveProfileEvent(medProfileMoItemList:ListCollectionView, callback:Function) {
			super();
			_medProfileMoItemList = medProfileMoItemList;
			_callback = callback;
		}
		
		public function get medProfileMoItemList():ListCollectionView {
			return _medProfileMoItemList;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}