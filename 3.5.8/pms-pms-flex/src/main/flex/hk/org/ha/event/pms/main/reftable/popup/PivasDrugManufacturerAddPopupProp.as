package hk.org.ha.event.pms.main.reftable.popup
{
	import mx.collections.ArrayCollection;

	public class PivasDrugManufacturerAddPopupProp
	{
		public var companyList:ArrayCollection;
		
		public var otherPivasDrugManufList:ArrayCollection;	// for duplication check
		
		public var okHandler:Function;
	}
}
