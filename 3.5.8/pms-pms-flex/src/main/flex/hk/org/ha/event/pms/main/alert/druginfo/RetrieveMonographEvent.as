package hk.org.ha.event.pms.main.alert.druginfo {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrieveMonographEvent extends AbstractTideEvent {

		private var _monoId:Number;
		
		private var _monoType:Number;
		
		private var _monoVersion:String;
		
		private var _patHospCode:String;
		
		private var _successCallback:Function;
		
		private var _faultCallback:Function;

		public function RetrieveMonographEvent(monoId:Number, monoType:Number, monoVersion:String, patHospCode:String, successCallback:Function, faultCallback:Function) {
			super();
			_monoId = monoId;
			_monoType = monoType;
			_monoVersion = monoVersion;
			_patHospCode = patHospCode;
			_successCallback = successCallback;
			_faultCallback = faultCallback;
		}
		
		public function get monoId():Number {
			return _monoId;
		}
		
		public function get monoType():Number {
			return _monoType;
		}
		
		public function get monoVersion():String {
			return _monoVersion;
		}
		
		public function get patHospCode():String {
			return _patHospCode;
		}
		
		public function get successCallback():Function {
			return _successCallback;
		}
		
		public function get faultCallback():Function {
			return _faultCallback;
		}
	}
}