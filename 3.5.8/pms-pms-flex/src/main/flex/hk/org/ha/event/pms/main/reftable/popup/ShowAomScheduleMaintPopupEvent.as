package hk.org.ha.event.pms.main.reftable.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowAomScheduleMaintPopupEvent extends AbstractTideEvent 
	{
		private var _aomScheduleMaintPopupProp:AomScheduleMaintPopupProp;
		
		public function ShowAomScheduleMaintPopupEvent(aomScheduleMaintPopupProp:AomScheduleMaintPopupProp):void 
		{
			super();
			_aomScheduleMaintPopupProp = aomScheduleMaintPopupProp;
		}
		
		public function get aomScheduleMaintPopupProp():AomScheduleMaintPopupProp{
			return _aomScheduleMaintPopupProp;
		}

	}
}