package hk.org.ha.event.pms.main.pivas
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePivasBatchPrepListEvent extends AbstractTideEvent 
	{		
		private var _manufFromDate:Date;
		private var _manufToDate:Date;
		private var _drugKey:Number;
		
		public function RetrievePivasBatchPrepListEvent(manufFromDate:Date, manufToDate:Date, drugKey:Number):void 
		{
			super();
			_manufFromDate = manufFromDate;
			_manufToDate = manufToDate;
			_drugKey = drugKey;
		}
		
		public function get manufFromDate():Date {
			return _manufFromDate;
		}
		
		public function get manufToDate():Date {
			return _manufToDate;
		}
		
		public function get drugKey():Number {
			return _drugKey;
		}
	}
}