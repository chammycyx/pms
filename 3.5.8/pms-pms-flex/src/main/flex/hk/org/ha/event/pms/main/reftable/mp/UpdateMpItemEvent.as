package hk.org.ha.event.pms.main.reftable.mp {
	
	import hk.org.ha.model.pms.persistence.reftable.ItemDispConfig;
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateMpItemEvent extends AbstractTideEvent 
	{
		private var _fdnMappingList:ListCollectionView;
		private var _itemSpecialtyList:ListCollectionView;
		private var _itemDispConfig:ItemDispConfig;
		private var _itemCode:String;

		public function UpdateMpItemEvent(fdnMappingList:ListCollectionView, itemSpecialtyList:ListCollectionView, itemDispConfig:ItemDispConfig, itemCode:String):void 
		{
			super();
			_fdnMappingList = fdnMappingList;
			_itemSpecialtyList = itemSpecialtyList;
			_itemDispConfig = itemDispConfig;
			_itemCode = itemCode;
		}
		
		public function get fdnMappingList():ListCollectionView
		{
			return _fdnMappingList;
		}
		
		public function get itemSpecialtyList():ListCollectionView
		{
			return _itemSpecialtyList;
		}
		
		public function get itemDispConfig():ItemDispConfig
		{
			return _itemDispConfig;
		}
		
		public function get itemCode():String
		{
			return _itemCode;
		}
	}
}