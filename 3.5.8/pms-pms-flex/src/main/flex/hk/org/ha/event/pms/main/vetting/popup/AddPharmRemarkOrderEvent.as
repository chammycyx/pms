package hk.org.ha.event.pms.main.vetting.popup
{
	import flash.events.Event;
	
	import hk.org.ha.model.pms.persistence.disp.MedOrder;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class AddPharmRemarkOrderEvent extends AbstractTideEvent 
	{
		private var _cancelCallbackEvent:Event;	
		private var _medOrder:MedOrder;	
		
		public function AddPharmRemarkOrderEvent():void
		{
			super();
		}

		public function get cancelCallbackEvent():Event {
			return _cancelCallbackEvent;
		}
		
		public function set cancelCallbackEvent(cancelCallbackEvent:Event):void {
			_cancelCallbackEvent = cancelCallbackEvent;
		}
		
		public function get medOrder():MedOrder {
			return _medOrder;
		}
		
		public function set medOrder(medOrder:MedOrder):void {
			_medOrder = medOrder;
		}
	}
}