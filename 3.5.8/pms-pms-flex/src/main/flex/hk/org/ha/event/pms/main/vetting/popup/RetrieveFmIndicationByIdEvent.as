package hk.org.ha.event.pms.main.vetting.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrieveFmIndicationByIdEvent extends AbstractTideEvent {

		private var _medOrderItemId:Number;
		
		public function RetrieveFmIndicationByIdEvent(medOrderItemId:Number) {
			super();
			_medOrderItemId = medOrderItemId;
		}
		
		public function get medOrderItemId():Number {
			return _medOrderItemId;
		}
	}
}
