package hk.org.ha.event.pms.main.reftable.mp {
	
	import hk.org.ha.model.pms.vo.reftable.mp.FixedConcnStatusInfo;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveFixedConcnStatusInfoHandlerEvent extends AbstractTideEvent 
	{
		private var _fixedConcnStatusInfo:FixedConcnStatusInfo;

		public function get fixedConcnStatusInfo():FixedConcnStatusInfo
		{
			return _fixedConcnStatusInfo;
		}

		public function RetrieveFixedConcnStatusInfoHandlerEvent(fixedConcnStatusInfo:FixedConcnStatusInfo):void 
		{
			_fixedConcnStatusInfo = fixedConcnStatusInfo;
			super();
		}
	}
}