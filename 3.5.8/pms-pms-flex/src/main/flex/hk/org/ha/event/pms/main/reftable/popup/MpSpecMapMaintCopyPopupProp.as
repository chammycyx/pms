package hk.org.ha.event.pms.main.reftable.popup
{
	import mx.collections.ArrayCollection;

	public class MpSpecMapMaintCopyPopupProp
	{
		public var pasWardList:ArrayCollection;
		public var pmsMpSpecMappingHdrList:ArrayCollection;
		public var selectedWardCode:String;
		public var patHospCode:String;
	}
}