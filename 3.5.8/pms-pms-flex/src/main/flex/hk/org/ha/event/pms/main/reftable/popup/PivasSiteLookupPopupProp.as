package hk.org.ha.event.pms.main.reftable.popup
{
	import mx.collections.ArrayCollection;

	public class PivasSiteLookupPopupProp
	{
		public var whiteDmSiteList:ArrayCollection;
		
		public var greyDmSiteList:ArrayCollection;
		
		public var selectedDmSiteList:ArrayCollection;
		
		public var okHandler:Function;
	}
}
