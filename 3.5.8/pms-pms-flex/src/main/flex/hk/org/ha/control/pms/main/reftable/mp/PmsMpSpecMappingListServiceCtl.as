package hk.org.ha.control.pms.main.reftable.mp {
	
	import hk.org.ha.event.pms.main.reftable.mp.ClearMpSpecMappingMaintViewListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.CreatePmsIpSpecMapExclEvent;
	import hk.org.ha.event.pms.main.reftable.mp.ExportMpSpecMapRptEvent;
	import hk.org.ha.event.pms.main.reftable.mp.PrintMpSpecMapRptEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrievePmsMpSpecMappingInfoListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.SendMpSpecMapRptListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.SetPmsMpSpecMappingInfoEvent;
	import hk.org.ha.event.pms.main.reftable.mp.UpdatePmsMpSpecMappingListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.UpdatePmsMpSpecMappingListSuccessEvent;
	import hk.org.ha.model.pms.biz.reftable.mp.PmsMpSpecMappingListServiceBean;
	import hk.org.ha.model.pms.vo.reftable.mp.PmsMpSpecMappingInfo;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("pmsMpSpecMappginListServiceCtl", restrict="true")]
	public class PmsMpSpecMappingListServiceCtl
	{
		
		[In]
		public var pmsMpSpecMappingListService:PmsMpSpecMappingListServiceBean;
		
		[Observer]
		public function retrievePmsMpSpecMappingInfoList(evt:RetrievePmsMpSpecMappingInfoListEvent):void
		{
			pmsMpSpecMappingListService.retrievePmsIpSpecMappingList(retrievePmsMpSpecMappingInfoListResult);
		}
		
		private function retrievePmsMpSpecMappingInfoListResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new SetPmsMpSpecMappingInfoEvent(evt.result as PmsMpSpecMappingInfo));
		}
		
		[Observer]
		public function updatePmsMpSpecMappingList(evt:UpdatePmsMpSpecMappingListEvent):void
		{
			pmsMpSpecMappingListService.updatePmsIpSpecMappingList(evt.pmsMpSpecMappingList, updatePmsMpSpecMappingListResult);
		}
		
		private function updatePmsMpSpecMappingListResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new UpdatePmsMpSpecMappingListSuccessEvent(evt.result as PmsMpSpecMappingInfo));
		}
		
		private var createPmsIpSpecMapExclEvent:CreatePmsIpSpecMapExclEvent;
		
		[Observer]
		public function createPmsIpSpecMapExcl(evt:CreatePmsIpSpecMapExclEvent):void {
			createPmsIpSpecMapExclEvent = evt;
			pmsMpSpecMappingListService.createPmsIpSpecMapExcl(evt.patHospCode, evt.pasSpecialty, evt.ward, createPmsIpSpecMapExclResult);
		}
		
		private function createPmsIpSpecMapExclResult(evt:TideResultEvent):void {
			createPmsIpSpecMapExclEvent.callback();
		}
		
		[Observer]
		public function clearMpSpecMappingMaintViewList(evt:ClearMpSpecMappingMaintViewListEvent):void
		{
			pmsMpSpecMappingListService.clearList();
		}
		
		[Observer]
		public function sendMpSpecMapRptList(evt:SendMpSpecMapRptListEvent):void
		{
			pmsMpSpecMappingListService.sendMpSpecMapRptList(evt.mpSpecMapRptList, sendExportMpSpecMapRptListResult);
		}
		
		private function sendExportMpSpecMapRptListResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new ExportMpSpecMapRptEvent());
		}
		
		[Observer]
		public function printMpSpecMapRpt(evt:PrintMpSpecMapRptEvent):void
		{
			pmsMpSpecMappingListService.printMpSpecMapRpt(evt.mpSpecMapRptList);
		}
		
	}
}
