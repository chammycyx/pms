package hk.org.ha.event.pms.main.vetting  {
	
	import flash.events.Event;
	
	public class ChangeOrderLineEvent extends Event {
		
		private var _changeLine:int;
		
		public function ChangeOrderLineEvent(changeLine:int) {
			super("changeOrderLine", true);
			_changeLine = changeLine;
		}
		
		public function get changeLine():int {
			return _changeLine;
		}
	}
}