package hk.org.ha.event.pms.main.reftable.mp {
	
	import hk.org.ha.model.pms.udt.reftable.QtyDurationConversionRptType;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ExportMpQtyDurationConversionRptEvent extends AbstractTideEvent 
	{
		private var _qtyDurationConversionRptType:QtyDurationConversionRptType;
		
		public function get qtyDurationConversionRptType():QtyDurationConversionRptType
		{
			return _qtyDurationConversionRptType;
		}

		public function set qtyDurationConversionRptType(value:QtyDurationConversionRptType):void
		{
			_qtyDurationConversionRptType = value;
		}
		
		public function ExportMpQtyDurationConversionRptEvent(inQtyDurationConversionRptType:QtyDurationConversionRptType):void 
		{
			super();
			_qtyDurationConversionRptType = inQtyDurationConversionRptType;
		}
	}
}