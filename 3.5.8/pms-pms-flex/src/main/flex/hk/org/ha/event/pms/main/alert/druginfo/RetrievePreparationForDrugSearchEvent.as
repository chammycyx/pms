package hk.org.ha.event.pms.main.alert.druginfo {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dms.vo.DrugName;
	import hk.org.ha.model.pms.dms.vo.PreparationProperty;
	import hk.org.ha.model.pms.dms.vo.RouteForm;
	
	public class RetrievePreparationForDrugSearchEvent extends AbstractTideEvent {
		
		private var _drugName:DrugName;
		
		private var _routeForm:RouteForm;
		
		private var _preparationProperty:PreparationProperty;
		
		private var _displayNameSaltForm:String;
		
		private var _strengthVolume:String;
		
		private var _dob:Date;
		
		private var _patHospCode:String;
		
		private var _gcnSeqNum:String;
		
		private var _rdfgenId:String;
		
		private var _rgenId:String;
		
		public function RetrievePreparationForDrugSearchEvent(drugName:DrugName, routeForm:RouteForm, preparationProperty:PreparationProperty, displayNameSaltForm:String, strengthVolume:String, dob:Date, patHospCode:String, gcnSeqNum:String, rdfgenId:String, rgenId:String) {
			super();
			_drugName = drugName;
			_routeForm = routeForm;
			_preparationProperty = preparationProperty;
			_displayNameSaltForm = displayNameSaltForm;
			_strengthVolume = strengthVolume;
			_dob = dob;
			_patHospCode = patHospCode;
			_gcnSeqNum = gcnSeqNum;
			_rdfgenId = rdfgenId;
			_rgenId = rgenId;
		}
		
		public function get drugName():DrugName {
			return _drugName;
		}
		
		public function get routeForm():RouteForm {
			return _routeForm;
		}
		
		public function get preparationProperty():PreparationProperty {
			return _preparationProperty;
		}
		
		public function get displayNameSaltForm():String {
			return _displayNameSaltForm;
		}
		
		public function get strengthVolume():String {
			return _strengthVolume;
		}

		public function get dob():Date {
			return _dob;
		}
		
		public function get patHospCode():String {
			return _patHospCode;
		}
		
		public function get gcnSeqNum():String {
			return _gcnSeqNum;
		}
		
		public function get rdfgenId():String {
			return _rdfgenId;
		}
		
		public function get rgenId():String {
			return _rgenId;
		}
	}
}
