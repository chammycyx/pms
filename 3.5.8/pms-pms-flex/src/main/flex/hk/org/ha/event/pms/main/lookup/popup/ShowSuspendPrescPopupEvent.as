package hk.org.ha.event.pms.main.lookup.popup {
	
	import hk.org.ha.event.pms.main.lookup.popup.SuspendPrescPopupProp;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowSuspendPrescPopupEvent extends AbstractTideEvent 
	{
		private var _suspendPrescPopupProp:SuspendPrescPopupProp;
		private var _cancelFunc:Function;
		
		public function ShowSuspendPrescPopupEvent(suspendPrescPopupProp:SuspendPrescPopupProp, cancelFunc:Function=null):void 
		{
			super();
			_suspendPrescPopupProp = suspendPrescPopupProp;
			_cancelFunc = cancelFunc;
		}
		
		public function get suspendPrescPopupProp():SuspendPrescPopupProp{
			return _suspendPrescPopupProp;
		}
		
		public function get cancelFunc():Function{
			return _cancelFunc;
		}

	}
}