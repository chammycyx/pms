package hk.org.ha.event.pms.main.vetting {
	
	import flash.events.Event;
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowRetrieveOrderMsgEvent extends AbstractTideEvent {
		
		private var _callback:Function;
		
		public function ShowRetrieveOrderMsgEvent(callback:Function) {
			super();
			_callback = callback;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}
