package hk.org.ha.event.pms.main.checkissue.mp
{
	import hk.org.ha.model.pms.vo.checkissue.mp.DrugCheckViewListSummary;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetDrugCheckViewListSummaryEvent extends AbstractTideEvent 
	{	
		private var _drugCheckViewListSummary:DrugCheckViewListSummary;
		
		public function SetDrugCheckViewListSummaryEvent(drugCheckViewListSummary:DrugCheckViewListSummary):void 
		{
			super();
			_drugCheckViewListSummary = drugCheckViewListSummary;
		}
		
		public function get drugCheckViewListSummary():DrugCheckViewListSummary {
			return _drugCheckViewListSummary;
		}
	}
}