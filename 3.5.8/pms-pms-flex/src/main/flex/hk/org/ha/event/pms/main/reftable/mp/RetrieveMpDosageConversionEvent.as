package hk.org.ha.event.pms.main.reftable.mp
{
	import hk.org.ha.model.pms.dms.persistence.DmDrug;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveMpDosageConversionEvent extends AbstractTideEvent
	{
		private var _dmDrug:DmDrug;
		
		public function RetrieveMpDosageConversionEvent(dmDrug:DmDrug):void
		{
			super();
			_dmDrug = dmDrug;
		}
		
		public function get dmDrug():DmDrug
		{
			return _dmDrug;
		}
	}
}