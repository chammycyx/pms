package hk.org.ha.event.pms.main.drug {
	
	import hk.org.ha.model.pms.udt.vetting.FmStatus;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDmDrugListByDisplayNameEvent extends AbstractTideEvent 
	{
		private var _displayName:String;
		
		private var _fmStatus:FmStatus;
		
		public function RetrieveDmDrugListByDisplayNameEvent(displayName:String, fmStatus:FmStatus):void 
		{
			super();
			_displayName = displayName;
			_fmStatus = fmStatus;
		}
		
		public function get displayName():String
		{
			return _displayName;
		}
		
		public function get fmStatus():FmStatus
		{
			return _fmStatus;
		}
	}
}