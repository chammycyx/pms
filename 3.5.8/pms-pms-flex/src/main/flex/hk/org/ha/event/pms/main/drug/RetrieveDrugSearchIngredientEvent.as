package hk.org.ha.event.pms.main.drug {
	
	import hk.org.ha.model.pms.dms.vo.IngredientSearchCriteria;
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDrugSearchIngredientEvent extends AbstractTideEvent 
	{			
		private var _drugSearchSource:DrugSearchSource;
		private var _ingredientSearchCriteria:IngredientSearchCriteria;

		public function RetrieveDrugSearchIngredientEvent(drugSearchSource:DrugSearchSource, ingredientSearchCriteria:IngredientSearchCriteria):void 
		{
			_drugSearchSource = drugSearchSource;
			_ingredientSearchCriteria = ingredientSearchCriteria;
			super();
		}
		
		public function get drugSearchSource():DrugSearchSource
		{
			return _drugSearchSource;
		}

		public function get ingredientSearchCriteria():IngredientSearchCriteria
		{
			return _ingredientSearchCriteria;
		}
	}
}