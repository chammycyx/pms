package hk.org.ha.event.pms.main.reftable {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshPrintDosageConversionRptEvent extends AbstractTideEvent 
	{
		private var _retrieveSuccess:Boolean;
		
		public function RefreshPrintDosageConversionRptEvent(retrieveSuccess:Boolean):void 
		{
			super();
			_retrieveSuccess = retrieveSuccess;
		}
		
		public function get retrieveSuccess():Boolean
		{
			return _retrieveSuccess;
		}
		
	}
}