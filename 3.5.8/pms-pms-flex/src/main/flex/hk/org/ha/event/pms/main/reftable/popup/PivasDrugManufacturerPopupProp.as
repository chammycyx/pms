package hk.org.ha.event.pms.main.reftable.popup
{
	import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrugManuf;
	import hk.org.ha.model.pms.vo.reftable.pivas.PivasDrugInfo;
	
	import mx.collections.ArrayCollection;

	public class PivasDrugManufacturerPopupProp
	{
		public var selectedPivasDrugInfo:PivasDrugInfo;
		
		public var selectedPivasDrugManuf:PivasDrugManuf;
		
		public var dmWarningList:ArrayCollection;
		
		public var okHandler:Function;
	}
}
