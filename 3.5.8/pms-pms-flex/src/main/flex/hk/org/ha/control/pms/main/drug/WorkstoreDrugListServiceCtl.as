package hk.org.ha.control.pms.main.drug {
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.event.pms.main.drug.RetrieveWorkstoreDrugListByDisplaynameEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveWorkstoreDrugListByDisplaynameFmStatusEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveWorkstoreDrugListByRxItemEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveWorkstoreDrugListDrugKeyEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveWorkstoreDrugListEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveWorkstoreDrugListByDhRxDrugEvent;
	import hk.org.ha.model.pms.biz.drug.WorkstoreDrugListServiceBean;
	import hk.org.ha.view.pms.main.vetting.popup.OrderEditItemPopup;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("workstoreDrugListServiceCtl", restrict="true")]
	public class WorkstoreDrugListServiceCtl 
	{
		[In]
		public var workstoreDrugListService:WorkstoreDrugListServiceBean;		
		
		[In]
		public var workstoreDrugList:ArrayCollection;
		
		[In]
		public var ctx:Context;
		
		private var callBackEvent:Event;
		private var orderEditItemPopup:OrderEditItemPopup;
		private const IGNORE_DRUG_SCOPE:Boolean = false;
		
		[Observer]
		public function retrieveWorkstoreDrugListByDrugKey(evt:RetrieveWorkstoreDrugListDrugKeyEvent):void
		{
			orderEditItemPopup = evt.orderEditItemPopup;
			callBackEvent = evt.callBackEvent;
			ctx.workstoreDrugList = null;
			workstoreDrugListService.retrieveWorkstoreDrugListByDrugKey(evt.drugKey, IGNORE_DRUG_SCOPE, retrieveWorkstoreDrugListResult);
		}		
		
		[Observer]
		public function retrieveWorkstoreDrugListByDisplayName(evt:RetrieveWorkstoreDrugListByDisplaynameEvent):void 
		{	
			orderEditItemPopup 	  = evt.orderEditItemPopup;
			ctx.workstoreDrugList = null;
			workstoreDrugListService.retrieveWorkstoreDrugListByDisplayName( evt.displayName,  retrieveWorkstoreDrugListByDisplayNameResult);
		}
		
		private function retrieveWorkstoreDrugListByDisplayNameResult(evt:TideResultEvent):void
		{
			if (orderEditItemPopup != null) {
				orderEditItemPopup.setDefaultExtendedListItem();
			}
		}
		
		private function retrieveWorkstoreDrugListResult(evt:TideResultEvent):void 
		{			
			if( callBackEvent != null ) {
				evt.context.dispatchEvent( callBackEvent );				
			}
			
			if (orderEditItemPopup != null) {
				orderEditItemPopup.setDefaultExtendedListItem();
			}
		}	
		
		[Observer]
		public function retrieveWorkstoreDrugList(evt:RetrieveWorkstoreDrugListEvent):void 
		{
			workstoreDrugListService.retrieveWorkstoreDrugList(evt.itemCode, evt.showNonSuspendItemOnly, evt.fdnFlag);
		}
		
		[Observer]
		public function retrieveWorkstoreDrugListByRxItem(evt:RetrieveWorkstoreDrugListByRxItemEvent):void
		{
			callBackEvent = evt.callBackEvent;
			ctx.workstoreDrugList = null;
			workstoreDrugListService.retrieveWorkstoreDrugListByRxItem(evt.rxItem,evt.displayNameOnly,retrieveWorkstoreDrugListResult);
		}
		
		[Observer]
		public function retrieveWorkstoreDrugListByDhRxDrug(evt:RetrieveWorkstoreDrugListByDhRxDrugEvent):void {
			orderEditItemPopup = evt.orderEditItemPopup;
			workstoreDrugListService.retrieveWorkstoreDrugListByDhRxDrug(evt.dhRxDrug, evt.itemCodePrefix, retrieveWorkstoreDrugListByDhRxDrugResult);
		}

		private function retrieveWorkstoreDrugListByDhRxDrugResult(evt:TideResultEvent):void {
			if (orderEditItemPopup != null) {
				orderEditItemPopup.setDefaultExtendedListItem();
			}
		}
	}
}