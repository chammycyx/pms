package hk.org.ha.event.pms.main.delivery
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshDueOrderLabelGenerationView extends AbstractTideEvent
	{
		private var _callbackFunction:Function;
		
		public function RefreshDueOrderLabelGenerationView(callbackFunction:Function)
		{
			super();
			_callbackFunction = callbackFunction;
		}
		
		public function get callbackFunction():Function
		{
			return _callbackFunction;
		}
	}
}