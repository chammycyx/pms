package hk.org.ha.event.pms.main.support
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class UpdatePropListEvent extends AbstractTideEvent 
	{
		private var _propList:ArrayCollection;
		
		public function UpdatePropListEvent(propList:ArrayCollection):void
		{
			super();
			_propList = propList;
		}
		
		public function get propList():ArrayCollection {
			return _propList;
		}
	}
}