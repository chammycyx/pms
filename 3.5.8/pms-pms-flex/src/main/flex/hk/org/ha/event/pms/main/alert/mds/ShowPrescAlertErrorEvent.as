package hk.org.ha.event.pms.main.alert.mds {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowPrescAlertErrorEvent extends AbstractTideEvent {
		
		private var _initFlag:Boolean;
		
		private var _callbackFunc:Function;
		
		public function ShowPrescAlertErrorEvent(initFlag:Boolean=false, callbackFunc:Function=null) {
			super();
			_initFlag = initFlag;
			_callbackFunc = callbackFunc;
		}
		
		public function get initFlag():Boolean {
			return _initFlag;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}