package hk.org.ha.event.pms.security.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowSystemIdleProtectionPopupEvent extends AbstractTideEvent
	{
		public function ShowSystemIdleProtectionPopupEvent()
		{
			super();
		}
	}
}