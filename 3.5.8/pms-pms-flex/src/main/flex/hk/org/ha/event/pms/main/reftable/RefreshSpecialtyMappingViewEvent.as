package hk.org.ha.event.pms.main.reftable {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshSpecialtyMappingViewEvent extends AbstractTideEvent 
	{
		
		private var _retrieveSuccess:Boolean;
		
		public function RefreshSpecialtyMappingViewEvent(retrieveSuccess:Boolean):void 
		{
			_retrieveSuccess = retrieveSuccess;
			super();
		}
		
		public function get retrieveSucess():Boolean {
			return _retrieveSuccess;
		}

	}
}