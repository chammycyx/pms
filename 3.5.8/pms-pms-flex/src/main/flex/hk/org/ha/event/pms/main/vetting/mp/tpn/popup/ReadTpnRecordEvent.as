package hk.org.ha.event.pms.main.vetting.mp.tpn.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ReadTpnRecordEvent extends AbstractTideEvent
	{
		private var _pnId:Number;
		
		private var _mode:String;
		
		private var _duration:Number;
		
		private var _selectedIndex:Number;
		
		private var _callback:Function;
		
		public function ReadTpnRecordEvent(pnId:Number, mode:String, duration:Number, selectedIndex:Number, callback:Function):void {
			super();
			_pnId = pnId;
			_mode = mode;
			_duration = duration;
			_selectedIndex = selectedIndex;
			_callback = callback;
		}
		
		public function get pnId():Number{
			return _pnId;
		}
		
		public function get mode():String{
			return _mode;
		}
		
		public function get duration():Number{
			return _duration;
		}
		
		public function get selectedIndex():Number{
			return _selectedIndex;
		}
		
		public function get callback():Function{
			return _callback;
		}
	}
}