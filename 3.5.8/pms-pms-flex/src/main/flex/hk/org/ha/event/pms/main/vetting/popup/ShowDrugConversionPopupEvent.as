package hk.org.ha.event.pms.main.vetting.popup {
		
	import hk.org.ha.model.pms.vo.rx.RxDrug;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDrugConversionPopupEvent extends AbstractTideEvent 
	{
		private var _rxDrug:RxDrug;		
		
		private var _closeHandler:Function;
		
		public function ShowDrugConversionPopupEvent(rxDrugValue:RxDrug, functionValue:Function):void 
		{
			super();
			_rxDrug = rxDrugValue;
			_closeHandler = functionValue;
		}
		
		public function get rxDrug():RxDrug
		{
			return _rxDrug;
		}
		
		public function get closeHandler():Function
		{
			return _closeHandler;
		}
	}
}
