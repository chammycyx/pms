package hk.org.ha.control.pms.main.report
{
	import hk.org.ha.event.pms.main.report.PrintOutstandSfiPaymentRptEvent;
	import hk.org.ha.event.pms.main.report.RetrieveOutstandSfiPaymentRptEvent;
	import hk.org.ha.event.pms.main.report.ShowOutstandSfiPaymentEvent;
	import hk.org.ha.event.pms.main.report.ShowOutstandSfiPaymentRptEvent;
	import hk.org.ha.model.pms.biz.report.OutstandSfiPaymentRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("outstandSfiPaymentRptServiceCtl", restrict="true")]
	public class OutstandSfiPaymentRptServiceCtl 
	{
		[In]
		public var outstandSfiPaymentRptService:OutstandSfiPaymentRptServiceBean;
		
		[Observer]
		public function printOutstandSfiPaymentRpt(evt:PrintOutstandSfiPaymentRptEvent):void
		{
			outstandSfiPaymentRptService.printOutstandSfiPaymentRpt();
		}
		
		[Observer]
		public function retrieveOutstandSfiPaymentRpt(evt:RetrieveOutstandSfiPaymentRptEvent):void
		{
			In(Object(outstandSfiPaymentRptService).retrieveSuccess)
			outstandSfiPaymentRptService.retrieveOutstandSfiPaymentRpt(evt.dispDate, retrieveOutstandSfiPaymentRptResult);
		}
		
		private function retrieveOutstandSfiPaymentRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new ShowOutstandSfiPaymentRptEvent(Object(outstandSfiPaymentRptService).retrieveSuccess));
		}
		
	}
}