package hk.org.ha.event.pms.main.drug {		
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveManualEntrySiteListByItemCodeEvent extends AbstractTideEvent 
	{
		private var _itemCode:String;	
		private var _callBackFunc:Function;
		
		public function RetrieveManualEntrySiteListByItemCodeEvent(itemCodeVal:String, callBackFunc:Function=null):void 
		{
			super();
			_itemCode = itemCodeVal;	
			_callBackFunc = callBackFunc;
		}
		
		public function get itemCode():String
		{
			return _itemCode;
		}
		
		public function get callBackFunc():Function
		{
			return _callBackFunc;
		}
	}
}