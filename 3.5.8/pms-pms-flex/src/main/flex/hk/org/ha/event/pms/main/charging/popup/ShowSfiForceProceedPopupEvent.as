package hk.org.ha.event.pms.main.charging.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.udt.charging.ClearanceStatus;
	
	public class ShowSfiForceProceedPopupEvent extends AbstractTideEvent 
	{		
		private var _dispOrderId:Number;
		private var _clearanceStatus:ClearanceStatus;
		private var _actionBy:String;
		
		//IPSFI used
		private var _ipsfiFlag:Boolean;
		private var _forceProceedReasonOnly:Boolean;
		private var _hkid:String;
		private var _caseNum:String;
		private var _invoiceNum:String;
		private var _patHospCode:String;
		private var _sfiForceProceedConfirmCallback:Function;
		private var _sfiForceProceedCancelCallback:Function;
		
		public function ShowSfiForceProceedPopupEvent(clearanceStatus:ClearanceStatus, actionBy:String, dispOrderId:Number):void 
		{
			super();			
			_dispOrderId = dispOrderId;
			_clearanceStatus = clearanceStatus;
			_actionBy = actionBy;

			//IPSFI used
			_ipsfiFlag = false;
			_forceProceedReasonOnly = false;
			_hkid = null;
			_caseNum = null;
			_invoiceNum = null;
			_patHospCode = null;
			_sfiForceProceedConfirmCallback = null;
			_sfiForceProceedCancelCallback = null;
		}
		
		public function get dispOrderId():Number{
			return _dispOrderId;
		}
		
		public function get clearanceStatus():ClearanceStatus{
			return _clearanceStatus;
		}
		
		public function get actionBy():String{
			return _actionBy;
		}

		public function get ipsfiFlag():Boolean{
			return _ipsfiFlag;
		}
		
		public function set ipsfiFlag(ipsfiFlag:Boolean):void{
			_ipsfiFlag = ipsfiFlag;
		}

		public function get forceProceedReasonOnly():Boolean{
			return _forceProceedReasonOnly;
		}
		
		public function set forceProceedReasonOnly(forceProceedReasonOnly:Boolean):void{
			_forceProceedReasonOnly = forceProceedReasonOnly;
		}
		
		public function get hkid():String{
			return _hkid;
		}
		
		public function set hkid(hkid:String):void{
			_hkid = hkid;
		}
		
		public function get caseNum():String{
			return _caseNum;
		}
		
		public function set caseNum(caseNum:String):void{
			_caseNum = caseNum;
		}
		
		public function get invoiceNum():String{
			return _invoiceNum;
		}
		
		public function set invoiceNum(invoiceNum:String):void{
			_invoiceNum = invoiceNum;
		}
		
		public function get patHospCode():String{
			return _patHospCode;
		}
		
		public function set patHospCode(patHospCode:String):void{
			_patHospCode = patHospCode;
		}
		
		public function get sfiForceProceedConfirmCallback():Function{
			return _sfiForceProceedConfirmCallback;
		}
		
		public function set sfiForceProceedConfirmCallback(sfiForceProceedConfirmCallback:Function):void{
			_sfiForceProceedConfirmCallback = sfiForceProceedConfirmCallback;
		}
		
		public function get sfiForceProceedCancelCallback():Function{
			return _sfiForceProceedCancelCallback;
		}
		
		public function set sfiForceProceedCancelCallback(sfiForceProceedCancelCallback:Function):void{
			_sfiForceProceedCancelCallback = sfiForceProceedCancelCallback;
		}
	}
}