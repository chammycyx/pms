package hk.org.ha.event.pms.main.report {
	
	import hk.org.ha.model.pms.vo.report.PatientListRptInfo;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetPatientListRptInfoEvent extends AbstractTideEvent 
	{
		private var _patientListRptInfo:PatientListRptInfo;
		public function SetPatientListRptInfoEvent(patientListRptInfo:PatientListRptInfo):void 
		{
			super();
			_patientListRptInfo = patientListRptInfo;
		}
		
		public function get patientListRptInfo():PatientListRptInfo {
			return _patientListRptInfo;
		}
	}
}