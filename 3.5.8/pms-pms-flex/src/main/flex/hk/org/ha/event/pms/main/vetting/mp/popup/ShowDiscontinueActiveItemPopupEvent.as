package hk.org.ha.event.pms.main.vetting.mp.popup {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowDiscontinueActiveItemPopupEvent extends AbstractTideEvent {

		private var _discontinueHandler:Function;
		
		public function ShowDiscontinueActiveItemPopupEvent(discontinueHandler:Function) {
			super();
			_discontinueHandler = discontinueHandler;
		}
		
		public function get discontinueHandler():Function {
			return _discontinueHandler;
		}
	}
}
