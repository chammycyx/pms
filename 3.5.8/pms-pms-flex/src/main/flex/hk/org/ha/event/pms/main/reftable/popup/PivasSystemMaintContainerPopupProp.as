package hk.org.ha.event.pms.main.reftable.popup
{
	import hk.org.ha.model.pms.dms.persistence.pivas.PivasContainer;
	import hk.org.ha.model.pms.persistence.reftable.pivas.PivasServiceHour;
	
	import mx.collections.ArrayCollection;

	public class PivasSystemMaintContainerPopupProp
	{
		public var action:String; //add, update
		public var pivasContainer:PivasContainer;
		public var pivasContainerList:ArrayCollection;
		public var okHandler:Function;
	}
}
