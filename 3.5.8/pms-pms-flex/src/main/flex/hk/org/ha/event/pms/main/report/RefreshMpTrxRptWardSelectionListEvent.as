package hk.org.ha.event.pms.main.report {
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshMpTrxRptWardSelectionListEvent extends AbstractTideEvent 
	{
		private var _wardList:ArrayCollection;
		public function RefreshMpTrxRptWardSelectionListEvent(list:ArrayCollection):void 
		{
			super();
			_wardList = list;
		}
		
		public function get wardList():ArrayCollection
		{
			return _wardList;
		}
	}
}