package hk.org.ha.control.pms.main.reftable.cmm {

	import mx.collections.ListCollectionView;
	
	import hk.org.ha.event.pms.main.reftable.cmm.ExportCmmDrugGroupRptEvent;
	import hk.org.ha.event.pms.main.reftable.cmm.RetrieveCmmDrugGroupListEvent;
	import hk.org.ha.event.pms.main.reftable.cmm.UpdateCmmDrugGroupListEvent;
	import hk.org.ha.model.pms.biz.reftable.cmm.CmmDrugGroupServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("cmmDrugGroupServiceCtl", restrict="true")]
	public class CmmDrugGroupServiceCtl {
		
		[In]
		public var cmmDrugGroupService:CmmDrugGroupServiceBean;
		
		private var retrieveCmmDrugGroupListEvent:RetrieveCmmDrugGroupListEvent;
		private var updateCmmDrugGroupListEvent:UpdateCmmDrugGroupListEvent;
		private var exportCmmDrugGroupRptEvent:ExportCmmDrugGroupRptEvent;
		
		[Observer]
		public function retrieveCmmDrugGroupList(evt:RetrieveCmmDrugGroupListEvent):void {
			retrieveCmmDrugGroupListEvent = evt;
			cmmDrugGroupService.retrieveCmmDrugGroupList(evt.prefixItemCode, retrieveCmmDrugGroupListResult);
		}
		
		private function retrieveCmmDrugGroupListResult(evt:TideResultEvent):void {
			retrieveCmmDrugGroupListEvent.callback(evt.result as ListCollectionView);
		}
		
		[Observer]
		public function updateCmmDrugGroupList(evt:UpdateCmmDrugGroupListEvent):void {
			updateCmmDrugGroupListEvent = evt;
			cmmDrugGroupService.updateCmmDrugGroupList(evt.cmmDrugGroupList, updateCmmDrugGroupListResult);
		}
		
		private function updateCmmDrugGroupListResult(evt:TideResultEvent):void {
			updateCmmDrugGroupListEvent.callback();
		}
		
		[Observer]
		public function exportCmmDrugGroupRpt(evt:ExportCmmDrugGroupRptEvent):void {
			exportCmmDrugGroupRptEvent = evt;
			cmmDrugGroupService.retrieveCmmDrugGroupRptList(exportCmmDrugGroupRptResult);
		}
		
		private function exportCmmDrugGroupRptResult(evt:TideResultEvent):void {
			exportCmmDrugGroupRptEvent.callback();
		}
	}
}
