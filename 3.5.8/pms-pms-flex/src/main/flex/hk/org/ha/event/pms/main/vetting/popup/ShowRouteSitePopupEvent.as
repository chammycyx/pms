package hk.org.ha.event.pms.main.vetting.popup {
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowRouteSitePopupEvent extends AbstractTideEvent 
	{		
		private var _okHandler:Function;		
		private var _cancelHandler:Function;		
		private var _isIpmoe:Boolean;
		
		public function ShowRouteSitePopupEvent(okHandlerVal:Function, cancelHandlerVal:Function, isIpmoeVal:Boolean=false):void 
		{
			super();
			_okHandler 		= okHandlerVal;
			_cancelHandler 	= cancelHandlerVal;
			_isIpmoe 		= isIpmoeVal;
		}
		
		public function get okHandler():Function
		{
			return _okHandler;
		}
		
		public function get cancelHandler():Function
		{
			return _cancelHandler;
		}
		
		public function get isIpmoe():Boolean
		{
			return _isIpmoe;
		}
	}
}
