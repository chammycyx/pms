package hk.org.ha.event.pms.main.vetting.popup
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowFmIndicationPopupEvent extends AbstractTideEvent
	{
		private var _fmIndicationList:ArrayCollection;
		
		public function ShowFmIndicationPopupEvent(fmIndicationList:ArrayCollection)
		{
			super();
			_fmIndicationList = fmIndicationList;
		}
		
		public function get fmIndicationList():ArrayCollection {
			return _fmIndicationList;
		}
	}
}