package hk.org.ha.event.pms.main.enquiry
{
	import hk.org.ha.model.pms.vo.enquiry.DispItemStatusEnq;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDispItemStatusEnqListEvent extends AbstractTideEvent 
	{
		private var _dispItemStatusEnq:DispItemStatusEnq;
		
		
		public function RetrieveDispItemStatusEnqListEvent(dispItemStatusEnq:DispItemStatusEnq):void
		{
			super();
			_dispItemStatusEnq = dispItemStatusEnq;
		}
		
		public function get dispItemStatusEnq():DispItemStatusEnq
		{
			return _dispItemStatusEnq;
		}
	}
}