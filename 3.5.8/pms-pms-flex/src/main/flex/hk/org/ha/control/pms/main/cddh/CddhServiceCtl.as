package hk.org.ha.control.pms.main.cddh {
	
	import flash.events.MouseEvent;
	
	import hk.org.ha.event.pms.main.cddh.CheckDeleteCddhRemarkStatusEvent;
	import hk.org.ha.event.pms.main.cddh.CheckUpdateCddhRemarkStatusEvent;
	import hk.org.ha.event.pms.main.cddh.CreateCddhCriteriaEvent;
	import hk.org.ha.event.pms.main.cddh.CreatePatientListViewEvent;
	import hk.org.ha.event.pms.main.cddh.PreviewCddhDispLabelEvent;
	import hk.org.ha.event.pms.main.cddh.PrintCddhPatDrugProfileRptEvent;
	import hk.org.ha.event.pms.main.cddh.RefreshCddhCriteriaEvent;
	import hk.org.ha.event.pms.main.cddh.RefreshCddhEnquiryMedicationEvent;
	import hk.org.ha.event.pms.main.cddh.RefreshDispLabelEvent;
	import hk.org.ha.event.pms.main.cddh.RefreshDispOrderItemListEvent;
	import hk.org.ha.event.pms.main.cddh.ReleaseUiLockEvent;
	import hk.org.ha.event.pms.main.cddh.RenderCddhDashBoardMessageEvent;
	import hk.org.ha.event.pms.main.cddh.RetrieveDispOrderItemListEvent;
	import hk.org.ha.event.pms.main.cddh.RetrieveLegacyDispLabelEvent;
	import hk.org.ha.event.pms.main.cddh.RetrievePatientListEvent;
	import hk.org.ha.event.pms.main.cddh.SearchCddhTextInvalidFormatEvent;
	import hk.org.ha.event.pms.main.cddh.UpdateDispOrderItemRemarkEvent;
	import hk.org.ha.event.pms.main.cddh.popup.ShowDispHistoryEnqPopupEvent;
	import hk.org.ha.event.pms.main.cddh.show.ShowCddhDispLabelPreviewEvent;
	import hk.org.ha.event.pms.main.cddh.show.ShowDispHistoryEnqViewEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.ShowLoadingPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	import hk.org.ha.model.pms.biz.cddh.CddhAidServiceBean;
	import hk.org.ha.model.pms.biz.cddh.CddhServiceBean;
	import hk.org.ha.model.pms.biz.label.LabelServiceBean;
	import hk.org.ha.model.pms.vo.cddh.CddhCriteria;
	import hk.org.ha.model.pms.vo.cddh.CddhCriteriaBase;
	import hk.org.ha.model.pms.vo.cddh.CddhDispOrderItem;
	import hk.org.ha.model.pms.vo.label.DispLabel;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.core.IFlexDisplayObject;
	import mx.core.UIComponent;
	import mx.managers.PopUpManager;
	
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("cddhServiceCtl", restrict="true")]
	
	public class CddhServiceCtl
	{
		[In]
		public var cddhService:CddhServiceBean;
		
		[In]
		public var cddhAidService:CddhAidServiceBean;
		
		[In]
		public var labelService:LabelServiceBean;
		
		[In]
		public var cddhCriteria:CddhCriteria;
		
		[In]
		public var errMsg:String;
		
		private var displayAll:Boolean;
		
		private var errorMsgPopupCallbackFunc:Function;
		
		private var cddhDispOrderItem:CddhDispOrderItem;
		
		private var hkid:String;
		
		[Observer]
		public function retrieveDispOrderItemList(evt:RetrieveDispOrderItemListEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Loading ..."));
			displayAll = evt.displayAll;
			errorMsgPopupCallbackFunc = evt.errorMsgPopupCallbackFunc;
			cddhService.retrieveDispOrderItem(evt.cddhCriteria, retrieveDispOrderItemListResult, retrieveDispOrderItemListError);
		}
		
		private function retrieveDispOrderItemListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			
			if (errMsg != null) {
				if (errMsg == "0033" || errMsg == "0068" || errMsg == "0069" || errMsg == "0246") {
					dispatchEvent(new SearchCddhTextInvalidFormatEvent(errMsg));
				} else {
					var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
					msgProp.messageCode = errMsg;	
					msgProp.messageWinHeight = 200;
					msgProp.setOkButtonOnly = true;
					if (errorMsgPopupCallbackFunc != null) {
						msgProp.okHandler = errorMsgPopupCallbackFunc;
					}
					dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
				}
			} else {
				evt.context.dispatchEvent(new RefreshCddhEnquiryMedicationEvent(null, displayAll));
			}
			
			evt.context.dispatchEvent(new ReleaseUiLockEvent());
		}
		
		private function retrieveDispOrderItemListError(evt:TideFaultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			evt.context.dispatchEvent(new ReleaseUiLockEvent());
		}
		
		[Observer]
		public function retrieveLegacyDispLabel(evt:RetrieveLegacyDispLabelEvent):void
		{
			cddhAidService.retrieveCddhDispLabel(evt.dispOrderItemKey, retrieveLegacyDispLabelResult);
		}
		private function retrieveLegacyDispLabelResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshDispLabelEvent());
		}
		
		[Observer]
		public function updateDispOrderItemRemark(evt:UpdateDispOrderItemRemarkEvent):void
		{
			cddhAidService.updateDispOrderItemRemark(evt.cddhDispOrderItem, updateDispOrderItemRemarkResult);
		}
		
		private function updateDispOrderItemRemarkResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshDispOrderItemListEvent());
		}
		
		private function deleteDispOrderItemRemarkResult(evt:TideResultEvent):void{
			evt.context.dispatchEvent(new RefreshDispOrderItemListEvent());
		}
		
		private function refreshAfterRemarkUpdateError(evt:MouseEvent):void {
			PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
			dispatchEvent(new RefreshCddhEnquiryMedicationEvent(null, false));
		}
		
		[Observer]
		public function checkUpdateCddhRemarkStatus(evt:CheckUpdateCddhRemarkStatusEvent):void{
			this.cddhDispOrderItem = evt.cddhDispOrderItem;
			var displayHospCode:String = cddhDispOrderItem.doHospCode;
			if (cddhDispOrderItem.doCddhWorkstoreGroupCode != null) {
				displayHospCode = cddhDispOrderItem.doCddhWorkstoreGroupCode;
			}
			
			cddhAidService.checkCddhRemarkStatus(cddhDispOrderItem.doId, displayHospCode, checkUpdateCddhRemarkStatusResult);
		}
		
		private function checkUpdateCddhRemarkStatusResult(evt:TideResultEvent):void{			
			if (errMsg != null) {
				if (errMsg == "0082") {
					dispatchEvent(new RenderCddhDashBoardMessageEvent(errMsg));
				} else {
					var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
					msgProp.messageCode = errMsg;
					msgProp.messageWinHeight = 200;
					msgProp.setOkButtonOnly = true;
					msgProp.okHandler = refreshAfterRemarkUpdateError;
					dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
				}
			} else {
				dispatchEvent(new ShowDispHistoryEnqPopupEvent(cddhDispOrderItem));
			}
		}
		
		[Observer]
		public function checkDeleteCddhRemarkStatus(evt:CheckDeleteCddhRemarkStatusEvent):void{
			this.cddhDispOrderItem = evt.cddhDispOrderItem;
			var displayHospCode:String = cddhDispOrderItem.doHospCode;
			if (cddhDispOrderItem.doCddhWorkstoreGroupCode != null){
				displayHospCode = cddhDispOrderItem.doCddhWorkstoreGroupCode;
			}
			cddhAidService.checkCddhRemarkStatus(cddhDispOrderItem.doId, displayHospCode, checkDeleteCddhRemarkStatusResult);
		}
		
		private function checkDeleteCddhRemarkStatusResult(evt:TideResultEvent):void{			
			if (errMsg != null) {
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = errMsg;		
				msgProp.messageWinHeight = 200;
				msgProp.setOkButtonOnly = true;
				msgProp.okHandler = refreshAfterRemarkUpdateError;
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			} else {
				cddhAidService.deleteDispOrderItemRemark(cddhDispOrderItem, deleteDispOrderItemRemarkResult);
			}
		}
		
		private function disableCddhFunction(evt:MouseEvent):void {
			PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
			dispatchEvent(new RefreshCddhCriteriaEvent(cddhCriteria));
		}
		
		[Observer]
		public function createCddhCriteria(evt:CreateCddhCriteriaEvent):void{
			hkid = evt.hkid;
			cddhService.createCddhCriteria(createCddhCriteriaResult);
		}
		
		private function createCddhCriteriaResult(evt:TideResultEvent):void{			
			if (hkid != "" && hkid != null) {
				cddhCriteria.hkid = hkid;
				evt.context.dispatchEvent(new RetrieveDispOrderItemListEvent(cddhCriteria));
				return; 
			}
			
			dispatchEvent(new RefreshCddhCriteriaEvent(cddhCriteria));
			dispatchEvent(new ReleaseUiLockEvent());
		}
		
		[Observer]
		public function retrievePatientList(evt:RetrievePatientListEvent):void{
			dispatchEvent(new ShowLoadingPopupEvent("Loading ..."));
			cddhService.retrieveDispOrderLikePatName(evt.cddhCriteria, retrievePatientListResult, retrievePatientListFault);
		}
		
		private function retrievePatientListResult(evt:TideResultEvent):void{
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			
			if (errMsg != null) {
				if (errMsg == "0069" || errMsg == "0246") {
					dispatchEvent(new SearchCddhTextInvalidFormatEvent(errMsg));
				} else {
					var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
					msgProp.messageCode = errMsg;		
					msgProp.messageWinHeight = 200;
					msgProp.setOkButtonOnly = true;
					dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
				}
				
				dispatchEvent(new ReleaseUiLockEvent());
				
			} else {
				evt.context.dispatchEvent(new CreatePatientListViewEvent);
			}
		}
		
		private function retrievePatientListFault(evt:TideFaultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			dispatchEvent(new ReleaseUiLockEvent());
		}
		
		[Observer]
		public function previewDispLabel(evt:PreviewCddhDispLabelEvent):void 
		{  
			labelService.getCddhDispLabel(evt.dispOrderItemKey, showDispLabelResult);
		}
		
		private function showDispLabelResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new ShowCddhDispLabelPreviewEvent);
		}
		
		[Observer]
		public function printCddhPatDrugProfileRpt(evt:PrintCddhPatDrugProfileRptEvent):void {
			cddhAidService.printCddhPatDrugProfileRpt(evt.cddhPatDrugProfileRpt, printCddhPatDrugProfileRptResult);
		}
		
		private function printCddhPatDrugProfileRptResult(evt:TideResultEvent):void{

		}
	}
}
