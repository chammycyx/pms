package hk.org.ha.control.pms.main.drug {

	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.drug.RetrieveDmDrugLiteListByFullDrugDescEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveDmDrugLiteListEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveDmDrugLiteListForChestItemEvent;
	import hk.org.ha.model.pms.biz.drug.DmDrugLiteListServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("dmDrugLiteListServiceCtl", restrict="true")]
	public class DmDrugLiteListServiceCtl 
	{
		[In]
		public var dmDrugLiteListService:DmDrugLiteListServiceBean;
		
		private var callBackEvent:Event;
		
		[Observer]
		public function retrieveDmDrugLiteList(evt:RetrieveDmDrugLiteListEvent):void
		{
			callBackEvent = evt.callBackEvent;
			dmDrugLiteListService.retrieveDmDrugLiteList(evt.itemCode, evt.showNonSuspendItemOnly, evt.fdnFlag, retrieveDmDrugLiteListResult);
		}
		
		[Observer]
		public function retrieveDmDrugLiteListByFullDrugDesc(evt:RetrieveDmDrugLiteListByFullDrugDescEvent):void
		{
			callBackEvent = evt.callBackEvent;
			dmDrugLiteListService.retrieveDmDrugLiteListByFullDrugDesc(evt.fullDrugDesc, retrieveDmDrugLiteListResult);
		}
		
		private function retrieveDmDrugLiteListResult(evt:TideResultEvent):void 
		{			
			if( callBackEvent != null ) {
				evt.context.dispatchEvent( callBackEvent );				
			}
		}
		
		[Observer]
		public function retrieveDmDrugLiteListForChestItem(evt:RetrieveDmDrugLiteListForChestItemEvent):void
		{
			callBackEvent = evt.callBackEvent;
			dmDrugLiteListService.retrieveDmDrugLiteListForChestItem(evt.itemCode, retrieveDmDrugLiteListResult);
		}
	}
}
