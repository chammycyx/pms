package hk.org.ha.event.pms.main.refill {
	import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ModifyRefillEvent extends AbstractTideEvent 
	{		
		private var _orderViewInfo:OrderViewInfo;
		private var _callback:Function;
		
		public function ModifyRefillEvent(orderViewInfo:OrderViewInfo, callback:Function):void 
		{
			super();
			_callback = callback;
			_orderViewInfo = orderViewInfo;
		}
		
		public function get callback():Function {
			return _callback;
		}
		
		public function get orderViewInfo():OrderViewInfo {
			return _orderViewInfo;
		}
	}
}