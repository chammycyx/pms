package hk.org.ha.event.pms.main.reftable.mp {
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateAomScheduleListEvent extends AbstractTideEvent 
	{	
		private var _updateAomScheduleList:ArrayCollection;
		private var _delAomScheduleList:ArrayCollection;
		
		public function UpdateAomScheduleListEvent(updateAomScheduleList:ArrayCollection, 
													 delAomScheduleList:ArrayCollection):void 
		{
			super();
			_updateAomScheduleList = updateAomScheduleList;
			_delAomScheduleList = delAomScheduleList;
		}
		
		public function get updateAomScheduleList():ArrayCollection {
			return _updateAomScheduleList;
		}
		
		public function get delAomScheduleList():ArrayCollection {
			return _delAomScheduleList;
		}
	}
}