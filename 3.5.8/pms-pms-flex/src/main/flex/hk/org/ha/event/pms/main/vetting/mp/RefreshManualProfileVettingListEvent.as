package hk.org.ha.event.pms.main.vetting.mp
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshManualProfileVettingListEvent extends AbstractTideEvent
	{
		private var _applySorting:Boolean;
		private var _sortByDrugDesc:Boolean;
		public function RefreshManualProfileVettingListEvent(applySorting:Boolean=true, sortByDrugDesc:Boolean=false)
		{			
			super();
			_applySorting = applySorting;
			_sortByDrugDesc = sortByDrugDesc;
		}
		
		public function get applySorting():Boolean
		{
			return _applySorting;
		}
		
		public function get sortByDrugDesc():Boolean
		{
			return _sortByDrugDesc;
		}
	}
}