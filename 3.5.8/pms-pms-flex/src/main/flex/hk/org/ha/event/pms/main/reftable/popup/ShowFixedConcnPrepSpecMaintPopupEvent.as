package hk.org.ha.event.pms.main.reftable.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowFixedConcnPrepSpecMaintPopupEvent extends AbstractTideEvent 
	{
		private var _pmsFixedConcnPrepSpecMaintPopupProp:FixedConcnPrepSpecMaintPopupProp;
		
		public function ShowFixedConcnPrepSpecMaintPopupEvent(pmsFixedConcnPrepSpecMaintPopupProp:FixedConcnPrepSpecMaintPopupProp):void 
		{
			super();
			_pmsFixedConcnPrepSpecMaintPopupProp = pmsFixedConcnPrepSpecMaintPopupProp;
		}
		
		public function get pmsFixedConcnPrepSpecMaintPopupProp():FixedConcnPrepSpecMaintPopupProp{
			return _pmsFixedConcnPrepSpecMaintPopupProp;
		}
		
	}
}