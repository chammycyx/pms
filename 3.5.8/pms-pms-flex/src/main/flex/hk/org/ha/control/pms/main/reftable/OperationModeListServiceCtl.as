package hk.org.ha.control.pms.main.reftable {
	
	import hk.org.ha.event.pms.main.reftable.RetrieveOperationModeListEvent;
	import hk.org.ha.event.pms.main.reftable.RetrieveOperationModePickStationListEvent;
	import hk.org.ha.event.pms.main.reftable.SetOperationProfileListEvent;
	import hk.org.ha.event.pms.main.reftable.ShowApplyProfileConfirmMsgEvent;
	import hk.org.ha.event.pms.main.reftable.UpdateActiveProfileEvent;
	import hk.org.ha.event.pms.main.reftable.UpdateOperationModeListEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.model.pms.biz.reftable.OperationModeListServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("operationModeListServiceCtl", restrict="true")]
	public class OperationModeListServiceCtl 
	{
		[In]
		public var operationModeListService:OperationModeListServiceBean;
		
		[Observer]
		public function retrieveOperationModePickStationList(evt:RetrieveOperationModePickStationListEvent):void
		{
			operationModeListService.retrievePickStationList();
		}
		
		[Observer]
		public function retrieveOperationProfileList(evt:RetrieveOperationModeListEvent):void
		{
			operationModeListService.retrieveOperationModeList(retrieveOperationProfileListResult);
		}
		
		private function retrieveOperationProfileListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new CloseLoadingPopupEvent);
			evt.context.dispatchEvent(new SetOperationProfileListEvent(Object(operationModeListService).invalidateSession));
		}
		
		[Observer]
		public function updateOperationProfileList(evt:UpdateOperationModeListEvent):void
		{
			if ( evt.showApplyProfileMsg ) {
				operationModeListService.updateOperationModeList(evt.operationPropList, evt.operationWorkstationList, 
																	evt.newWorkstationList , evt.pickStationList, evt.activeOperationProfileId,
																	evt.workstorePropList,
																	updateOperationProfileListResult);
			} else {
				In(Object(operationModeListService).invalidateSession)
				operationModeListService.updateOperationModeList(evt.operationPropList, evt.operationWorkstationList, 
																	evt.newWorkstationList , evt.pickStationList, evt.activeOperationProfileId,
																	evt.workstorePropList,
																	retrieveOperationProfileListResult);
			}
		}
		
		private function updateOperationProfileListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new CloseLoadingPopupEvent);
			evt.context.dispatchEvent(new ShowApplyProfileConfirmMsgEvent());
		}
		
		[Observer]
		public function updateActiveProfile(evt:UpdateActiveProfileEvent):void
		{
			operationModeListService.updateActiveProfile(evt.activeOperationProfileId, true, retrieveOperationProfileListResult);
		}
	}
}
