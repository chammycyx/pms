package hk.org.ha.event.pms.main.reftable.popup {

	import hk.org.ha.view.pms.main.reftable.pivas.PivasFormulaCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class ShowPivasFormulaAddFormulaPopupEvent extends AbstractTideEvent {

		private var _criteria:PivasFormulaCriteria;
		private var _postAdd:Function;
		private var _postEdit:Function;
		
		public function ShowPivasFormulaAddFormulaPopupEvent(criteria:PivasFormulaCriteria, postAdd:Function, postEdit:Function):void {
			super();
			_criteria = criteria;
			_postAdd = postAdd;
			_postEdit = postEdit;
		}
		
		public function get criteria():PivasFormulaCriteria {
			return _criteria;
		}
		
		public function get postAdd():Function {
			return _postAdd;
		}
		
		public function get postEdit():Function {
			return _postEdit;
		}
	}
}
