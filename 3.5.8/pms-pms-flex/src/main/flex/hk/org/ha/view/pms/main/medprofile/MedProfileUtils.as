package hk.org.ha.view.pms.main.medprofile
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.text.engine.BreakOpportunity;
	import flash.text.engine.FontWeight;
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.collections.IList;
	import mx.collections.ListCollectionView;
	import mx.collections.SortField;
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.FlexGlobals;
	import mx.core.IChildList;
	import mx.core.IFlexDisplayObject;
	import mx.core.IToolTip;
	import mx.core.UIComponent;
	import mx.events.ValidationResultEvent;
	import mx.formatters.DateFormatter;
	import mx.managers.PopUpManager;
	import mx.managers.ToolTipManager;
	import mx.utils.StringUtil;
	import mx.validators.ValidationResult;
	import mx.validators.Validator;
	
	import spark.collections.Sort;
	import spark.components.TextInput;
	
	import flashx.textLayout.conversion.TextConverter;
	import flashx.textLayout.elements.BreakElement;
	import flashx.textLayout.elements.DivElement;
	import flashx.textLayout.elements.FlowLeafElement;
	import flashx.textLayout.elements.InlineGraphicElement;
	import flashx.textLayout.elements.ParagraphElement;
	import flashx.textLayout.elements.SpanElement;
	import flashx.textLayout.elements.TextFlow;
	
	import hk.org.ha.fmk.pms.flex.utils.TimeUtil;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
	import hk.org.ha.model.pms.persistence.medprofile.PurchaseRequest;
	import hk.org.ha.model.pms.persistence.medprofile.Replenishment;
	import hk.org.ha.model.pms.udt.YesNoBlankFlag;
	import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
	import hk.org.ha.model.pms.udt.medprofile.EndType;
	import hk.org.ha.model.pms.udt.medprofile.InactiveType;
	import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
	import hk.org.ha.model.pms.udt.medprofile.MedProfileStatus;
	import hk.org.ha.model.pms.udt.medprofile.ReplenishmentStatus;
	import hk.org.ha.model.pms.udt.vetting.ActionStatus;
	import hk.org.ha.model.pms.udt.vetting.FmStatus;
	import hk.org.ha.model.pms.vo.alert.AlertProfile;
	import hk.org.ha.model.pms.vo.alert.PatAlert;
	import hk.org.ha.model.pms.vo.chemo.ChemoInfo;
	import hk.org.ha.model.pms.vo.chemo.ChemoItem;
	import hk.org.ha.model.pms.vo.rx.CapdItem;
	import hk.org.ha.model.pms.vo.rx.CapdRxDrug;
	import hk.org.ha.model.pms.vo.rx.Dose;
	import hk.org.ha.model.pms.vo.rx.DoseGroup;
	import hk.org.ha.model.pms.vo.rx.Freq;
	import hk.org.ha.model.pms.vo.rx.IvAdditive;
	import hk.org.ha.model.pms.vo.rx.IvRxDrug;
	import hk.org.ha.model.pms.vo.rx.Regimen;
	import hk.org.ha.model.pms.vo.rx.RxDrug;
	import hk.org.ha.model.pms.vo.rx.RxItem;
	
	import org.granite.collections.BasicMap;
	import org.granite.collections.IMap;

	public class MedProfileUtils extends MedProfileConstants
	{
		private static const DATE_FORMATTER:DateFormatter = new DateFormatter();
		private static const DATE_TIME_FORMATTER:DateFormatter = new DateFormatter();
		private static const DATE_AT_TIME_FORMATTER:DateFormatter = new DateFormatter();
		private static const TODAY_TIME_FORMATTER:DateFormatter = new DateFormatter();
		private static const TIME_DAY_MONTH_FORMATTER:DateFormatter = new DateFormatter();
		private static const TIME_TODAY_FORMATTER:DateFormatter = new DateFormatter();
		private static const TIME_FORMATTER:DateFormatter = new DateFormatter();
		private static const DAY_DATE_TIME_FORMATTER:DateFormatter = new DateFormatter();
		private static const DAY_MONTH_FORMATTER:DateFormatter = new DateFormatter();
		private static const DISCONTINUE_DATE_FORMATTER:DateFormatter = new DateFormatter();		
		private static const CALENDAR_DATE_FORMATTER:DateFormatter = new DateFormatter();
		private static const CALENDAR_TIME_FORMATTER:DateFormatter = new DateFormatter();		
		
		private static const COLOR_DISCONTINUE_TEXT:int = 0xFF0000;
		
		[Embed(source="/assets/mds_alert_12.png")]
		public static var mdsAlertImg:Class;
		
		[Embed(source="/assets/fm.png")]
		public static var fmIndicationImg:Class;
		
		[Embed(source="/assets/disp_by_pharm.png")]
		public static var dispenseByPharmacyImg:Class;
		
		[Embed(source="/assets/purchase_by_pat.png")]
		public static var purchaseByPatientImg:Class;
		
		[Embed(source="/assets/own_stock.png")]
		public static var continueWithOwnStockImg:Class;
		
		[Embed(source="/assets/disp_in_clinic.png")]
		public static var dispenseInClinicImg:Class;
		
		[Embed(source="/assets/purchase_by_pat_safety_net.png")]
		public static var safetyNetImg:Class;
		
		[Embed(source="/assets/chargeable.png")]
		public static var chargeableItemImg:Class;
		
		[Embed(source="/assets/single_use_ind.png")]
		public static var singleUseImg:Class;
		
		[Embed(source="/assets/fixed_concn.png")]
		public static var fixedConcnImg:Class;
		
		[Embed(source="/assets/fix_period.png")]
		public static var fixPeriodImg:Class;
		
		[Embed(source="/assets/danger_drug_12.png")]
		public static var dangerDrugImg:Class;
		
		[Embed(source="/assets/restrict_item.png")]
		public static var specialtyRestrictedItemImg:Class;
		
		[Embed(source="/assets/hla_test.png")]
		public static var hlaImg:Class;
		
		[Embed(source='/assets/ward_stock.png')]
		public static var wardStockImg:Class;
		
		[Embed(source='/assets/redisp.png')]
		public static var redispImg:Class;
		
		[Embed(source='/assets/unit_dose.png')]
		public static var unitDoseImg:Class;
		
		[Embed(source='/assets/unit_dose_exception.png')]
		public static var unitDoseExceptionImg:Class;
		
		[Embed(source='/assets/direct_label_print.png')]
		public static var directLabelPrintImg:Class; 
		
		[Embed(source='/assets/verified.png')]
		public static var verifiedImg:Class; 
		
		[Embed(source="/assets/single_use_ind.png")]
		public static var singleDispImg:Class;

		[Embed(source="/assets/item_suspended.png")]
		public static var itemSuspendedImg:Class;
		
		[Embed(source='/assets/mds_exception.png')]
		public static var mdsExceptionImg:Class;
		
		[Embed(source='/assets/urgent.png')]
		public static var urgentImg:Class;
		
		[Embed(source='/assets/ip_pending.png')]
		public static var pendingImg:Class;
		
		[Embed(source='/assets/ip_suspend.png')]
		public static var suspendedImg:Class;

		[Embed(source="/assets/pivas_new_order.png")]
		public static var pivasNewOrderImg:Class;

		[Embed(source="/assets/pivas_satellite_order.png")]
		public static var pivasSatelliteOrderImg:Class;

		[Embed(source="/assets/pivas_edited_order.png")]
		public static var pivasEditedOrderImg:Class;

		[Embed(source="/assets/pivas_information.png")]
		public static var pivasInformationImg:Class;

		[Embed(source="/assets/pivas_exception.png")]
		public static var pivasExceptionImg:Class;

		[Embed(source="/assets/pivas_max_dose.png")]
		public static var pivasMaxDoseImg:Class;
		
		[Embed(source="/assets/pivas.png")]
		public static var pivasImg:Class;
		
		public static var iconMap:Object = {
			"assets/mds_alert_12.png" : mdsAlertImg,
			"assets/fm.png" : fmIndicationImg,
			"assets/disp_by_pharm.png" : dispenseByPharmacyImg,
			"assets/purchase_by_pat.png" : purchaseByPatientImg,
			"assets/own_stock.png" : continueWithOwnStockImg,
			"assets/disp_in_clinic.png" : dispenseInClinicImg,
			"assets/purchase_by_pat_safety_net.png" : safetyNetImg,
			"assets/chargeable.png" : chargeableItemImg,
			"assets/single_use_ind.png" : singleUseImg,
			"assets/fixed_concn.png" : fixedConcnImg,						
			"assets/fix_period.png" : fixPeriodImg,
			"assets/danger_drug_12.png" : dangerDrugImg,
			"assets/restrict_item.png" : specialtyRestrictedItemImg,
			"assets/hla_test.png" : hlaImg,
			"assets/ward_stock.png" : wardStockImg,
			"assets/redisp.png" : redispImg,
			"assets/unit_dose.png" : unitDoseImg,
			"assets/unit_dose_exception.png" : unitDoseExceptionImg,
			"assets/direct_label_print.png" : directLabelPrintImg,
			"assets/verified.png" : verifiedImg,
			"assets/single_use_ind.png" : singleDispImg,
			"assets/item_suspended.png" : itemSuspendedImg,
			"assets/mds_exception.png" : mdsExceptionImg,
			"assets/urgent.png" : urgentImg,
			"assets/ip_pending.png" : pendingImg,
			"assets/ip_suspend.png" : suspendedImg,
			"assets/pivas_new_order.png" : pivasNewOrderImg,
			"assets/pivas_satellite_order.png" : pivasSatelliteOrderImg,
			"assets/pivas_edited_order.png" : pivasEditedOrderImg,
			"assets/pivas_information.png" : pivasInformationImg,
			"assets/pivas_exception.png" : pivasExceptionImg,
			"assets/pivas_max_dose.png" : pivasMaxDoseImg,
			"assets/pivas.png" : pivasImg
		};
		
		public static var toolTip:IToolTip;
		
		{
			DATE_FORMATTER.formatString = "DD-MMM-YYYY";
			DATE_TIME_FORMATTER.formatString = "DD-MMM-YYYY JJ:NN";
			DATE_AT_TIME_FORMATTER.formatString = "DD-MMM-YYYY at JJ:NN";
			TODAY_TIME_FORMATTER.formatString = "Today JJ:NN";
			TIME_DAY_MONTH_FORMATTER.formatString = "J:NN D-MMM";
			TIME_TODAY_FORMATTER.formatString = "J:NN Today";
			TIME_FORMATTER.formatString = "JJ:NN:SS";
			DAY_DATE_TIME_FORMATTER.formatString = "EEE DD-MMM-YYYY JJ:NN";
			DAY_MONTH_FORMATTER.formatString = 'DD-MMM';
			DISCONTINUE_DATE_FORMATTER.formatString = "DD MMM YYYY";
			CALENDAR_DATE_FORMATTER.formatString = "DD-MMM-YYYY";
			CALENDAR_TIME_FORMATTER.formatString = "JJNN";
		}

		public static function importFromTlfString(str:String):TextFlow 
		{
			var textFlow:TextFlow = TextConverter.importToFlow(
				"<TextFlow xmlns='http://ns.adobe.com/textLayout/2008'>".concat(str == null ? "": str, "</TextFlow>"),
				TextConverter.TEXT_LAYOUT_FORMAT);
			
			var leaf:FlowLeafElement = textFlow.getFirstLeaf();
			
			while (leaf != null) {
				var img:InlineGraphicElement = leaf as InlineGraphicElement;
				if (img != null) {
					var icon:Class = iconMap[img.source is String ? img.source.replace(/^\//, "") : img.source];
					if (icon != null) {
						img.source = icon;
					}
				}
				leaf = leaf.getNextLeaf();
			}
			
			return textFlow;
		}
		
		public static function importForPlainText(str:String):TextFlow
		{
			var tokens:Array = StringUtil.trim(str).split(" ");
			
			var root:TextFlow = new TextFlow();
			root.fontWeight = FontWeight.BOLD;
			
			var document:ParagraphElement = new ParagraphElement();
			root.addChild(document);
			
			for (var i:int = 0; i < tokens.length; i++) {
				appendString(document, tokens[i], i != tokens.length - 1, BreakOpportunity.AUTO);
			}
			return root;
		}
		
		public static function processDrugOrderMouseClick(event:MouseEvent, textFlow:TextFlow, mdsAlertFunc:Function, fmIndFunc:Function, hlaFunc:Function=null):void 
		{
			if (textFlow == null) {
				return;
			}
			
			var mouseX:Number = event.stageX;
			var mouseY:Number = event.stageY;
			
			var leaf:FlowLeafElement = textFlow.getFirstLeaf();
			
			while (leaf != null) {
				var img:InlineGraphicElement = leaf as InlineGraphicElement;
				if (img != null) {
					var graphic:DisplayObject = img.graphic;
					var anchor:Point = graphic.localToGlobal(new Point(0, 0));
					
					if (mouseX >= anchor.x && mouseX <= anchor.x + graphic.width && 
						mouseY >= anchor.y && mouseY <= anchor.y + graphic.height) {
						
						switch (img.source) {
							case mdsAlertImg:
								destroyTooltip();
								mdsAlertFunc();
								return;
							case fmIndicationImg:
								destroyTooltip();
								fmIndFunc();
								return;
							case hlaImg:
								destroyTooltip();
								hlaFunc();
								return;
						}
					}
				}
				leaf = leaf.getNextLeaf();
			}
		}
		
		public static function toggleTooltip(event:MouseEvent, textFlow:TextFlow, rowItem:*=null, tooltipTextFunc:Function=null):void 
		{
			if (textFlow == null) {
				return;
			}
			
			var mouseX:Number = event.stageX;
			var mouseY:Number = event.stageY;
			
			var leaf:FlowLeafElement = textFlow.getFirstLeaf();
			
			
			while (leaf != null) {
				var img:InlineGraphicElement = leaf as InlineGraphicElement;
				if (img != null && img.graphic != null) {
					var graphic:DisplayObject = img.graphic;
					var anchor:Point = graphic.localToGlobal(new Point(0, 0));
					
					if (mouseX >= anchor.x && mouseX <= anchor.x + graphic.width && 
						mouseY >= anchor.y && mouseY <= anchor.y + graphic.height) {
						if (getTooltipText(img) != null) {
							createTooltip(getTooltipText(img), mouseX, mouseY);
						} else if (tooltipTextFunc != null) {
							createTooltip(tooltipTextFunc(img, rowItem), mouseX, mouseY);
						}
						return;
					} else {
						destroyTooltip();
					}
				}
				leaf = leaf.getNextLeaf();
			}			
		}

		private static function createTooltip(tooltipText:String, x:Number, y:Number):void 
		{
			if (toolTip == null && StringUtil.trim(tooltipText).length > 0) {
				toolTip = ToolTipManager.createToolTip(tooltipText, x + 10, y + 10);
				var maxX:Number = FlexGlobals.topLevelApplication.width - toolTip.width;
				var maxY:Number = FlexGlobals.topLevelApplication.height - toolTip.height;
				if (toolTip.x > maxX || toolTip.y > maxY) {
					toolTip.move(Math.min(maxX, toolTip.x), Math.min(maxY, toolTip.y));
				}
			}
		}
		
		public static function destroyTooltip():void
		{
			if (toolTip != null) {
				ToolTipManager.destroyToolTip(toolTip);
				toolTip = null;
			}
		}
		
		private static function getTooltipText(img:InlineGraphicElement):String
		{
			switch (img.source) {
				case mdsAlertImg:
					return TOOLTIP_MDS_ALERT;
				case fmIndicationImg:
					return TOOLTIP_FM_INDICATION;
				case dispenseByPharmacyImg:
					return TOOLTIP_DISPENSE_BY_PHARMACY;
				case purchaseByPatientImg:
					return TOOLTIP_PURCHASE_BY_PATIENT; 
				case continueWithOwnStockImg:
					return TOOLTIP_CONTINUE_WITH_OWN_STOCK;
				case dispenseInClinicImg:
					return TOOLTIP_DISPENSE_IN_CLINIC;
				case safetyNetImg:
					return TOOLTIP_SAFETY_NET;
				case chargeableItemImg:
					return TOOLTIP_CHARGEABLE_ITEM;
				case singleUseImg:
					return TOOLTIP_SINGLE_USE;
				case fixedConcnImg:
					return TOOLTIP_FIXED_CONCN;					
				case fixPeriodImg:
					return TOOLTIP_FIX_PERIOD;
				case dangerDrugImg:
					return TOOLTIP_DANGER_DRUG;
				case specialtyRestrictedItemImg:
					return TOOLTIP_SPECIALTY_RESTRICTED_ITEM;
				case hlaImg:
					return TOOLTIP_HLA;
				case wardStockImg:
					return TOOLTIP_WARD_STOCK;
				case redispImg:
					return TOOLTIP_REDISPENSE;
				case unitDoseImg:
					return TOOLTIP_UNIT_DOSE;
				case unitDoseExceptionImg:
					return TOOLTIP_UNIT_DOSE_EXCEPTION;					
				case directLabelPrintImg:
					return TOOLTIP_DIRECT_LABEL_PRINT;
				case verifiedImg:
					return TOOLTIP_VERIFIED;
				case singleDispImg:
					return TOOLTIP_SINGLE_DISPENSE;
				case itemSuspendedImg:
					return TOOLTIP_ITEM_SUSPENDED;
				case mdsExceptionImg:
					return TOOLTIP_MDS_EXCEPTION;
				case pendingImg:
					return TOOLTIP_PENDING;
				case suspendedImg:
					return TOOLTIP_SUSPENDED;
				case urgentImg:
					return TOOLTIP_URGENT;
				case pivasNewOrderImg:
					return TOOLTIP_PIVAS_NEW_ORDER;
				case pivasSatelliteOrderImg:
					return TOOLTIP_PIVAS_SATELLITE_ORDER;
				case pivasEditedOrderImg:
					return TOOLTIP_PIVAS_EDITED_ORDER;
			}
			return null;
		}
		
		public static function formatRxItem(moItem:*, outpatient:Boolean):TextFlow
		{
			if (outpatient) {
				return formatOutpatientRxItem(moItem);
			} else {
				return importFromTlfString(moItem.rxItem.formattedDrugOrderTlf);
			}
		}
		
		private static function formatOutpatientRxItem(moItem:*):TextFlow
		{
			var rxItem:RxItem = moItem.rxItem;
			
			var root:TextFlow = new TextFlow();
			root.fontWeight = FontWeight.BOLD;
			
			var div:DivElement = new DivElement();
			root.addChild(div);
			
			var rxDrug:RxDrug = rxItem as RxDrug;
			var ivRxDrug:IvRxDrug = rxItem as IvRxDrug;
			var capdRxDrug:CapdRxDrug = rxItem as CapdRxDrug;
			
			if (rxDrug != null) {
				formatRxDrug(createParagraph(div, rxDrug.discontinueFlag), rxDrug);
				formatIndicatorsAndDiscontinueInfo(createParagraph(div), moItem);
			} else if (capdRxDrug != null) {
				formatCapdRxDrug(createParagraph(div, capdRxDrug.discontinueFlag), capdRxDrug);
				formatIndicatorsAndDiscontinueInfo(createParagraph(div), moItem);
			} else if (ivRxDrug != null) {
				root = importFromTlfString(rxItem.formattedDrugOrderTlf);
			}
			
			return root;
		}
		
		private static function formatRxDrug(paragraph:ParagraphElement, rxDrug:RxDrug):void
		{
			appendString(paragraph, rxDrug.moeDrugDescForDrugSet, false, BreakOpportunity.AUTO);
			appendBreak(paragraph);
			appendString(paragraph, toLowerCase(rxDrug.routeDesc));
			appendString(paragraph, " : ");
			
			var doseGroupList:ListCollectionView = rxDrug.regimen.doseGroupList;
			for (var i:int = 0; i < doseGroupList.length; i++) {
				formatDoseGroup(paragraph, doseGroupList.getItemAt(i) as DoseGroup, i == (doseGroupList.length - 1));
			}
			
			formatExtraInfo(paragraph, rxDrug);
		}
		
		private static function formatDoseGroup(paragraph:ParagraphElement, doseGroup:DoseGroup, lastItem:Boolean):void
		{
			var doseList:ListCollectionView = doseGroup.doseList;
			for (var i:int = 0; i < doseList.length; i++) {
				formatDose(paragraph, doseList.getItemAt(i), i == (doseList.length - 1));
			}
			
			if (doseGroup.duration > 0) {
				appendString(paragraph, toLowerCase(doseGroup.durationDesc));
			}
			
			if (!lastItem) {
				appendString(paragraph, ", then");
				appendBreak(paragraph);
			}
		}
		
		private static function formatDose(paragraph:ParagraphElement, dose:*, lastItem:Boolean):void {
			
			appendString(paragraph, dose is CapdItem ? dose.getDoseDesc() : toLowerCase(dose.getDoseDesc()),
					false, BreakOpportunity.AUTO);
			
			if (!lastItem) {
				appendString(paragraph, " and");
				appendBreak(paragraph);
			}
		}
		
		private static function formatCapdRxDrug(paragraph:ParagraphElement, capdRxDrug:CapdRxDrug):void
		{
			appendString(paragraph, "CAPD ");
			appendString(paragraph, capdRxDrug.supplierSystem, true);
			appendString(paragraph, formatBrackets(capdRxDrug.calciumStrength), true);			
			appendBreak(paragraph);
			
			var capdItemList:ListCollectionView = capdRxDrug.capdItemList;
			for (var i:int = 0; i < capdItemList.length; i++) {
				formatDose(paragraph, capdItemList.getItemAt(i), i == (capdItemList.length - 1));
			}
			
			formatExtraInfo(paragraph, capdRxDrug);
		}
		
		private static function formatExtraInfo(paragraph:ParagraphElement, rxItem:RxItem):void 
		{
			if (StringUtil.trim(rxItem.specialInstruction).length > 0) {
				appendBreak(paragraph);
				appendString(paragraph, rxItem.specialInstruction, false, BreakOpportunity.AUTO);
			}
		}
		
		private static function formatIndicatorsAndDiscontinueInfo(paragraph:ParagraphElement, moItem:*):void
		{
			var rxItem:* = moItem.rxItem;
			
			appendIcon(paragraph, chargeableItemImg, moItem.chargeFlag == YesNoBlankFlag.Yes, true);
			appendIcon(paragraph, dispenseByPharmacyImg, rxItem.actionStatus == ActionStatus.DispByPharm, false);
			appendIcon(paragraph, purchaseByPatientImg, rxItem.actionStatus == ActionStatus.PurchaseByPatient, false);
			appendIcon(paragraph, safetyNetImg, rxItem.actionStatus == ActionStatus.SafetyNet, false);
			appendIcon(paragraph, continueWithOwnStockImg, rxItem.actionStatus == ActionStatus.ContinueWithOwnStock, false);
			appendIcon(paragraph, dispenseInClinicImg, rxItem.actionStatus == ActionStatus.DispInClinic, false);
			appendIcon(paragraph, mdsAlertImg, rxItem.alertFlag, false);
			appendIcon(paragraph, dangerDrugImg, rxItem.dangerDrugFlag, false);
			appendIcon(paragraph, singleUseImg, rxItem.singleUseFlag, false);
			appendIcon(paragraph, fmIndicationImg, rxItem.fmFlag, false);
			
			if (rxItem.discontinueFlag) {
				appendBreak(paragraph);
				appendString(paragraph, "Discontinued", false, BreakOpportunity.NONE, COLOR_DISCONTINUE_TEXT);
				appendString(paragraph, formatBrackets(rxItem.discontinueReason, " [", "]"), false, BreakOpportunity.AUTO,
						COLOR_DISCONTINUE_TEXT);
				appendString(paragraph, ", ", false, BreakOpportunity.AUTO, COLOR_DISCONTINUE_TEXT);
				appendString(paragraph, rxItem.discontinueHospCode, false, BreakOpportunity.NONE, COLOR_DISCONTINUE_TEXT);
				appendString(paragraph, ", ", false, BreakOpportunity.AUTO, COLOR_DISCONTINUE_TEXT);
				appendString(paragraph, formatDiscontinueDate(rxItem.discontinueDate), false, BreakOpportunity.NONE,
						COLOR_DISCONTINUE_TEXT);
			}
		}
		
		private static function createParagraph(div:DivElement, lineThrough:Boolean = false):ParagraphElement
		{
			var paragraph:ParagraphElement = new ParagraphElement();
			div.addChild(paragraph);

			if (lineThrough) {
				paragraph.lineThrough = true;
			}
			return paragraph;
		}

		private static function appendString(paragraph:ParagraphElement, str:String, appendSpace:Boolean = false,
											 breakOpportunity:* = BreakOpportunity.NONE, color:* = null):void
		{
			if (StringUtil.trim(str).length <= 0) {
				return;
			}
			
			var span:SpanElement = new SpanElement();
			span.breakOpportunity = breakOpportunity;
			if (color != null) {
				span.color = color;
			}
			span.text = str + (appendSpace ? " " : "");
			paragraph.addChild(span);
		}
		
		private static function appendIcon(paragraph:ParagraphElement, iconClass:Class, visible:Boolean,
										   includeInLayout:Boolean = false):void
		{
			var image:InlineGraphicElement;
			if (visible) {
				image = new InlineGraphicElement();
				image.source = iconClass;
				image.paddingRight = 2;
				paragraph.addChild(image);
			} else if (includeInLayout) {
				image = new InlineGraphicElement();
				image.width = 12;
				image.paddingRight = 2;
				paragraph.addChild(image);
			}
		}
		
		private static function appendSpace(paragraph:ParagraphElement):void
		{
			var span:SpanElement = new SpanElement();
			span.text = " ";
			paragraph.addChild(span);
		}
		
		private static function appendBreak(paragraph:ParagraphElement):void
		{
			paragraph.addChild(new BreakElement());
		}
		
		public static function dateRenderFunction(item:Object, column:DataGridColumn):String
		{
			var date:Date = resolve(item, column.dataField);
			return formatDate(date);
		}
		
		public static function dateTimeRenderFunction(item:Object, column:DataGridColumn):String 
		{
			var date:Date = resolve(item, column.dataField);
			return formatDateTime(date);
		}
		
		public static function timeRenderFunction(item:Object, column:DataGridColumn):String
		{
			var date:Date = resolve(item, column.dataField);
			return formatTime(date);
		}
		
		public static function numberRenderFunction(item:Object, column:DataGridColumn):String 
		{
			var value:Number = resolve(item, column.dataField);
			return toString(value);
		}
		
		public static function booleanRenderFunction(item:Object, column:DataGridColumn, renderFalse:Boolean = true):String
		{
			var value:Boolean = resolve(item, column.dataField);
			return value ? "Y" : (renderFalse ? "N" : "");
		}
		
		public static function yesRenderFunction(item:Object, column:DataGridColumn):String
		{
			return booleanRenderFunction(item, column, false);
		}
		
		public static function resolve(obj:*, path:String):*
		{
			if (path == null)
				return null;
			
			for each (var token:String in path.split("\.")) {
				if (obj == null || !obj.hasOwnProperty(token))
					return null;
				obj = obj[token];
			}
			
			return obj;
		}
				
		public static function formatMedProfileStatus(status:MedProfileStatus, inactiveType:InactiveType):String 
		{
			if (status == MedProfileStatus.Inactive) {
				return (inactiveType == null ? InactiveType.Discharge : inactiveType).displayValue; 
			} else {
				return status.displayValue;
			}			
		}
		
		public static function formatDate(date:Date):String
		{
			return DATE_FORMATTER.format(date);
		}
		
		public static function formatDiscontinueDate(date:Date):String
		{
			return DISCONTINUE_DATE_FORMATTER.format(date);
		}
		
		public static function formatDateTime(date:Date, formatToday:Boolean = true, def:String = ""):String
		{
			if (date == null)
				return def;
			else if (formatToday && isToday(date))
				return TODAY_TIME_FORMATTER.format(date);
			else
				return DATE_TIME_FORMATTER.format(date);
		}
		
		public static function formatDateAtTime(date:Date):String
		{
			return DATE_AT_TIME_FORMATTER.format(date);
		}
		
		public static function formatTimeDayMonth(date:Date):String
		{
			if (date == null)
				return "";
			else if (isToday(date))
				return TIME_TODAY_FORMATTER.format(date);
			else
				return TIME_DAY_MONTH_FORMATTER.format(date);
		}
		
		public static function formatDayDateTime(date:Date):String
		{
			return DAY_DATE_TIME_FORMATTER.format(date);
		}
		
		public static function formatDayMonth(date:Date):String
		{
			return DAY_MONTH_FORMATTER.format(date);
		}
		
		public static function formatTime(date:Date):String
		{
			return TIME_FORMATTER.format(date);
		}
		
		public static function formatCalendarDate(date:Date):String
		{
			return CALENDAR_DATE_FORMATTER.format(date);	
		}
		
		public static function formatCalendarTime(date:Date):String
		{
			return CALENDAR_TIME_FORMATTER.format(date);	
		}
		
		public static function formatTimeDiff(date1:Date, date2:Date):String
		{
			if (date1 == null || date2 == null)
				return "";
			
			var hours:Number = Math.ceil(Math.abs(date2.getTime() - date1.getTime()) / 3600000);
			if (hours < 24) {
				return hours + (hours > 1 ? " hours" : " hour");
			} else {
				var days:Number = Math.ceil(hours / 24);
				return days + (days > 1 ? " days" : " day");
			}
		}
		
		public static function formatAge(dob:Date):String
		{
			if (dob == null) {
				return "";
			}
			
			dob = MedProfileUtils.toDayBegin(dob);
			var now:Date = MedProfileUtils.toDayBegin(new Date());
			
			var monthDiff:int = MedProfileUtils.monthdiff(dob, now);
			
			if (monthDiff < 3) {
				var dayDiff:int = MedProfileUtils.daydiff(dob, now);
				return dayDiff + (dayDiff > 1 ? " days" : " day");
			} else if (monthDiff < 36) {
				return monthDiff + " months";
			} else {
				return int(monthDiff / 12) + " years";
			}			
		}
		
		public static function formatSignedNumber(value:Number):String 
		{
			if (isNaN(value)) {
				return "";
			}
			return (value > 0 ? "+" : (value < 0 ? "–" : "")) + Math.abs(value); 
		}
		
		public static function splitToken(str:String, pos:int, delim:String = " "):String {

			if (str == null) {
				return null;
			}
			
			var tokens:Array = str.split(delim);
			if (pos < tokens.length) {
				return tokens[pos];
			}
			return null;
		}
		
		public static function isToday(date:Date, ifNullReturnValue:Boolean = false):Boolean 
		{
			if (date == null)
				return ifNullReturnValue;
			
			var now:Date = TimeUtil.getCurrentServerTime();
			return date.fullYear == now.fullYear && date.month == now.month && date.date == now.date;
		}
		
		public static function toDayBegin(date:Date):Date
		{
			if (date == null)
				return null;
			
			return new Date(date.fullYear, date.month, date.date, 0, 0, 0, 0);
		}
		
		public static function daydiff(date1:Date, date2:Date):int 
		{
			if (date1 == null || date2 == null)
				return 0;
			
			return Math.ceil((date2.getTime() - date1.getTime()) / 86400000);
		}
		
		public static function monthdiff(date1:Date, date2:Date):int
		{
			if (date1 == null || date2 == null)
				return 0;

			return (date2.fullYear - date1.fullYear) * 12 + date2.month - date1.month - (date2.date - date1.date < 0 ? 1 : 0);
		}
		
		public static function isEmpty(a:ICollectionView):Boolean 
		{
			return a == null || a.length <= 0;
		}
		
		public static function isNotEmpty(a:ICollectionView):Boolean 
		{
			return !isEmpty(a);
		}
		
		public static function escapeHTML(str:String):String 
		{
			str = str.replace(/&/g, "&amp;");
			str = str.replace(/</g, "&lt;");
			str = str.replace(/>/g, "&gt;");
			return str;
		}
		
		public static function isStatOrAtOnceDose(regimen:*):Boolean 
		{
			try {
				var dose:Dose = regimen.doseGroupList.getItemAt(0).doseList.getItemAt(0);
				return dose.isStat || dose.isAtOnce;
			} catch (e:Error) {
			}
			return false;
		}		
		
		public static function isDoseSpecified(medProfileMoItem:MedProfileMoItem):Boolean
		{
			try {
				if (medProfileMoItem.rxItem is IvRxDrug) {
					var ivRxDrug:IvRxDrug = medProfileMoItem.rxItem as IvRxDrug;
					if (ivRxDrug.dispQty > 0) {
						return true;
					}
				} else if (medProfileMoItem.rxItem is CapdRxDrug) {
					var capdRxDrug:CapdRxDrug = medProfileMoItem.rxItem as CapdRxDrug;
					for each (var capdItem:CapdItem in capdRxDrug.capdItemList) {
						if (capdItem.dispQty > 0) {
							return true;
						}
					}
				} else if (medProfileMoItem.rxItem is RxDrug) {
					var rxDrug:RxDrug = medProfileMoItem.rxItem as RxDrug;
					for each (var doseGroup:DoseGroup in rxDrug.regimen.doseGroupList) {
						for each (var dose:Dose in doseGroup.doseList) {
							if (dose.dispQty > 0) {
								return true;
							}
						}
					}
				}
			} catch (e:Error) {
			}
			return false;
		}
		
		public static function isAsDirectDose(regimen:*):Boolean
		{
			try {
				return regimen.doseGroupList.getItemAt(0).doseList.getItemAt(0).dailyFreq.code == DAILY_FREQ_AS_DIRECTED;
			} catch (e:Error) {
			}
			return false;
		}
		
		public static function isOwnStockOrFreeTextEntry(medProfileMoItem:MedProfileMoItem):Boolean
		{
			if (medProfileMoItem == null)
				return false;
			
			if (medProfileMoItem.rxItem is RxDrug) {
				var rxDrug:RxDrug = medProfileMoItem.rxItem as RxDrug;
				return rxDrug.actionStatus == ActionStatus.ContinueWithOwnStock || rxDrug.fmStatus == FmStatus.FreeTextEntryItem.dataValue;
			} else if (medProfileMoItem.rxItem is IvRxDrug) {
				var ivRxDrug:IvRxDrug = medProfileMoItem.rxItem as IvRxDrug;
				for each (var ivAdditive:IvAdditive in ivRxDrug.ivAdditiveList) {
					if (ivAdditive.actionStatus == ActionStatus.ContinueWithOwnStock) {
						return true;
					}
				}
			} else if (medProfileMoItem.rxItem is CapdRxDrug) {
				var capdRxDrug:CapdRxDrug = medProfileMoItem.rxItem as CapdRxDrug;
				return capdRxDrug.actionStatus == ActionStatus.ContinueWithOwnStock;
			}
				
			return false;
		}
		
		public static function isManualItem(itemNum:Number):Boolean
		{
			return itemNum >= 10000;
		}
		
		public static function checkDuplicateDrugs(medProfileItemList:ICollectionView, medProfileItemList2CheckAgainst:ICollectionView):ArrayCollection
		{
			var resultList:ArrayCollection = new ArrayCollection();
			
			for each (var medProfileItem:MedProfileItem in medProfileItemList) {
			
				for each (var medProfilePoItem:MedProfilePoItem in medProfileItem.medProfileMoItem.medProfilePoItemList) {
					
					var duplicatePoItemList:ArrayCollection = new ArrayCollection();
					
					for each (var medProfileItem2CheckAgainst:MedProfileItem in medProfileItemList2CheckAgainst) {
						
						if (medProfileItem !== medProfileItem2CheckAgainst &&
							(medProfileItem2CheckAgainst.status == MedProfileItemStatus.Unvet ||
							medProfileItem2CheckAgainst.status == MedProfileItemStatus.Vetted ||
							medProfileItem2CheckAgainst.status == MedProfileItemStatus.Verified)) {
							duplicatePoItemList.addAll(checkDuplicateDrugsByItem(medProfilePoItem, medProfileItem2CheckAgainst));
						}
					}
					
					if (duplicatePoItemList.length > 0) {
						var item:Object = new Object();
						item.medProfilePoItem = medProfilePoItem;
						item.duplicatePoItemList = duplicatePoItemList;
						resultList.addItem(item);
					}
				}
			}
			
			return resultList;
		}
		
		private static function checkDuplicateDrugsByItem(medProfilePoItem:MedProfilePoItem,
														  medProfileItem2CheckAgainst:MedProfileItem):ArrayCollection
		{
			var resultList:ArrayCollection = new ArrayCollection();
			var medProfileMoItem2CheckAgainst:MedProfileMoItem = medProfileItem2CheckAgainst.medProfileMoItem;
			
			for each (var medProfilePoItem2CheckAgainst:MedProfilePoItem in medProfileMoItem2CheckAgainst.medProfilePoItemList) {
				if (isDuplicateDrugsByPoItem(medProfilePoItem, medProfilePoItem2CheckAgainst)) {
					resultList.addItem(medProfilePoItem2CheckAgainst);
				}
			}
			return resultList;
		}
		
		private static function isDuplicateDrugsByPoItem(medProfilePoItem:MedProfilePoItem,
															medProfilePoItem2CheckAgainst:MedProfilePoItem):Boolean
		{
			if (medProfilePoItem.fmStatus == FmStatus.FreeTextEntryItem.dataValue || medProfilePoItem2CheckAgainst.fmStatus == FmStatus.FreeTextEntryItem.dataValue) {
				return false;
			}
			
			if (medProfilePoItem.itemCode == MedProfileConstants.ITEM_CODE_NOT01 || medProfilePoItem.itemCode == MedProfileConstants.ITEM_CODE_NOT01) {
				return false;
			}
			
			if (!isSubjectedToDuplicationCheckByFreq(medProfilePoItem) || !isSubjectedToDuplicationCheckByFreq(medProfilePoItem2CheckAgainst)) {
				return false;
			}
			
			return medProfilePoItem.dmDrugLite != null && medProfilePoItem2CheckAgainst.dmDrugLite != null &&
				medProfilePoItem.dmDrugLite.displayname == medProfilePoItem2CheckAgainst.dmDrugLite.displayname;
		}
		
		private static function isSubjectedToDuplicationCheckByFreq(medProfilePoItem:MedProfilePoItem):Boolean
		{
			var regimen:Regimen = medProfilePoItem.regimen;
			if (regimen == null) {
				return true;
			}
			
			for each (var doseGroup:DoseGroup in regimen.doseGroupList) {
				for each (var dose:Dose in doseGroup.doseList) {
					var dailyFreq:Freq = dose.dailyFreq;
					var supplFreq:Freq = dose.supplFreq;
					if ((dailyFreq.code != DAILY_FREQ_CODE_AT_ONCE && dailyFreq.code != DAILY_FREQ_CODE_ONCE && dailyFreq.code != DAILY_FREQ_CODE_STAT) ||
							(supplFreq != null && supplFreq.code != null)) {
						return true;
					}
				}
			}
			return false;
		}
		
		public static function formatBrackets(value:*, openBracket:String = "(", closeBracket:String = ")"):String
		{
			if ((value is Number && isNaN(value)) || value == null || (value is String && StringUtil.trim(value as String).length <= 0))
				return "";
			else
				return openBracket.concat(value, closeBracket);
		}
		
		public static function formatStartEndDate(item:*):String
		{
			var startDate:Date = null;
			var endDate:Date = null;
			var rxDrug:RxDrug = item.rxItem as RxDrug;
			var ivRxDrug:IvRxDrug = item.rxItem as IvRxDrug;
			var capdRxDrug:CapdRxDrug = item.rxItem as CapdRxDrug;
			
			if (rxDrug != null && rxDrug.regimen != null && rxDrug.regimen.doseGroupList.length > 0) {
				startDate = rxDrug.regimen.doseGroupList.getItemAt(0).startDate;
				endDate = rxDrug.regimen.doseGroupList.getItemAt(rxDrug.regimen.doseGroupList.length - 1).endDate;
			} else if (ivRxDrug != null) {
				startDate = ivRxDrug.startDate;
				endDate = ivRxDrug.endDate;
			} else if (capdRxDrug != null) {
				for each (var capdItem:CapdItem in capdRxDrug.capdItemList) {
					if (capdItem.startDate != null && (startDate == null || capdItem.startDate.time < startDate.time)) {
						startDate = capdItem.startDate;
					}
					if (capdItem.endDate != null && (endDate == null || capdItem.endDate.time > endDate.time)) {
						endDate = capdItem.endDate;
					}
				}
			}
			
			return formatDate(startDate) + "\n" + (endDate == null ? "-" : formatDate(endDate));
		}
		
		public static function formatDoseFluidDesc(dose:*):String {

			var result:Array = new Array();
			
			var startAdminRate:* = dose.startAdminRate;
			var endAdminRate:* = dose.endAdminRate;
			
			var doseFluid:* = dose.doseFluid;
			
			if (doseFluid != null && doseFluid.diluentCode != null) {
				result.push(formatDilutionMethod(doseFluid));
				result.push(formatFluidVolume(doseFluid));
				result.push(" ");
				result.push(doseFluid.diluentAbbr);
				result.push(" ");
			}
			
			result.push(formatFullAdminRate(startAdminRate, endAdminRate));
			
			return result.join("");
		}
		
		public static function formatIvRxDrugFluidDesc(rxItem:*):String {
			
			var hasAdditive:Boolean = rxItem.ivAdditiveList != null && rxItem.ivAdditiveList.length > 0;
			
			var result:Array = new Array();
			
			var startAdminRate:* = rxItem.startAdminRate;
			var endAdminRate:* = rxItem.endAdminRate;
			
			var baseFluid:* = rxItem.ivFluidList == null || rxItem.ivFluidList.length <= 0 ? null : rxItem.ivFluidList.getItemAt(0);
			
			if (baseFluid != null && baseFluid.diluentCode != null && hasAdditive) {
				
				result.push(formatDilutionMethod(baseFluid));
				result.push(formatFluidVolume(baseFluid));
				result.push(" ");
				
				// If Changed, Notify Carloo the changes. File: IvRxDrug.getDisplayNameForHeader
				for each (var fluid:* in rxItem.ivFluidList) {
					if (rxItem.ivFluidList.length > 1) {
						result.push(fluid.fluidQty);
					}
					result.push(fluid.diluentAbbr);
				}
				
				result.push(" ");
			}
			
			result.push(formatFullAdminRate(startAdminRate, endAdminRate));
			
			return result.join("");
		}
		
		public static function formatDilutionMethod(fluid:*):String {
			
			if (fluid.dilutionMethod == "M") {
				return "make up to ";
			} else if (fluid.dilutionMethod == "A") {
				return "in ";
			}
			return "";
		}
		
		public static function formatFluidVolume(fluid:*):String {
			
			var result:Array = new Array();
			
			result.push(fluid.fluidVolume);
			result.push(" ");
			result.push(toLowerCase(fluid.fluidVolumeUnit));
			
			return result.join("");
		}		
		
		public static function formatFullAdminRate(startAdminRate:*, endAdminRate:*):String {

			if (startAdminRate != null && startAdminRate.rateFormat == "F")
				return "Full Rate";
			
			var result:Array = new Array();
			
			result.push(formatAdminRate(startAdminRate));
			
			var endAdminRateDesc:String = formatAdminRate(endAdminRate);
			if (StringUtil.trim(endAdminRateDesc).length > 0) {
				result.push(" to ");
				result.push(endAdminRateDesc);
			}
			
			return result.join("");
		}
		
		public static function formatAdminRate(adminRate:*):String {
			
			var result:Array = new Array();
			
			if (adminRate != null) {
				
				if (adminRate.rateFormat == "D" && !isNaN(adminRate.rateDuration) && adminRate.rateDuration != 0) {
					
					result.push("over ");
					result.push(adminRate.rateDuration);
					result.push(" ");
					result.push(formatTimeUnit(adminRate.rateDurationUnit, adminRate.rateDuration));
					
				} else if (adminRate.rateFormat == "V" && !isNaN(adminRate.rateVolume) && adminRate.rateVolume != 0) {
					
					result.push(adminRate.rateVolume);
					result.push(toLowerCase(adminRate.rateVolumeUnit));
					result.push(" per ");
					result.push(formatTimeUnit(adminRate.rateVolumePerUnit));
				}
			}
			
			return result.join("");
		}
		
		public static function formatTimeUnit(unit:*, value:* = NaN):String 
		{
			var plural:String = value > 1 ? "s" : "";
			if (unit == "H") {
				return "hour" + plural;
			} else if (unit == "M") {
				return "min" + plural;
			}
			return "";
		}
		
		public static function canSetLabelInstruction(rxItem:RxItem):Boolean
		{
			if (rxItem != null) {
				if (rxItem.conditionList != null && rxItem.conditionList.length > 0) {
					return false;
				}
				
				var ivRxDrug:IvRxDrug = rxItem as IvRxDrug;
				var rxDrug:RxDrug = rxItem as RxDrug;
				
				if (ivRxDrug != null) {
					
					if (ivRxDrug.lineDesc != null || ivRxDrug.startAdminRate != null || 
						(ivRxDrug.ivFluidList != null && ivRxDrug.ivFluidList.length > 0))
						return false;
					
					for each (var ivAdditive:IvAdditive in ivRxDrug.ivAdditiveList) {
						if (ivAdditive.specialInstruction != null)
							return false;
					}
					
				} else if (rxDrug != null) {
					
					if (rxDrug.specialInstruction != null)
						return false;
					
					if (rxDrug.regimen != null) {
						for each (var doseGroup:DoseGroup in rxDrug.regimen.doseGroupList) {
							for each (var dose:Dose in doseGroup.doseList) {
								if (dose.doseFluid != null || dose.startAdminRate != null)
									return false;
							}
						}
					}
				}
			}
			return true;
		}
		
		public static function formatFmStatusShortDesc(fmStatus:String):String
		{
			switch (fmStatus) {
				case FmStatus.SpecialDrug.dataValue:
					return "Special Drug";
				case FmStatus.SafetyNetItem.dataValue:
					return "SFIwSN";
				case FmStatus.SelfFinancedItemC.dataValue:
				case FmStatus.SelfFinancedItemD.dataValue:
					return "SFI";
				case FmStatus.UnRegisteredDrug.dataValue:
					return "Unreg Drug";
				case FmStatus.SampleItem.dataValue:
					return "Sample";
			}
			return "";
		}
		
		public static function formatActionStatusShortDesc(actionStatus:ActionStatus):String
		{
			switch (actionStatus) {
				case ActionStatus.PurchaseByPatient:
					return "SFI";
				case ActionStatus.SafetyNet:
					return "SN";
			}
			return "";
		}
		
		public static function formatTreatmentDayDesc(chemoInfo:ChemoInfo):String
		{
			if (chemoInfo == null) {
				return "";
			}
			
			var treatmentDayList:ArrayCollection = new ArrayCollection();
			treatmentDayList.sort = new Sort();
			treatmentDayList.refresh();
			
			for each (var chemoItem:ChemoItem in chemoInfo.chemoItemList) {
				if (!isNaN(chemoItem.treatmentDay)) {
					treatmentDayList.addItem(chemoItem.treatmentDay);
				}
			}
			
			var buf:Array = new Array();
			
			var prevTreatmentDay:Number = NaN;
			var startTreatmentDay:Number = NaN;
			for each (var treatmentDay:Number in treatmentDayList) {
				if (isNaN(startTreatmentDay)) {
					startTreatmentDay = treatmentDay; 
				}
				
				if (!isNaN(prevTreatmentDay) && prevTreatmentDay != treatmentDay - 1) {
					appendTreatmentDayPart(buf, startTreatmentDay, prevTreatmentDay);
					prevTreatmentDay = treatmentDay;
					startTreatmentDay = treatmentDay;
				} else {
					prevTreatmentDay = treatmentDay;
				}
			}
			appendTreatmentDayPart(buf, startTreatmentDay, prevTreatmentDay);
			
			return buf.join(", ");
		}
		
		private static function appendTreatmentDayPart(buf:Array, startTreatmentDay:Number, endTreatmentDay:Number):void
		{
			if (isNaN(startTreatmentDay)) {
				return;
			}
			
			if (startTreatmentDay == endTreatmentDay) {
				buf.push(startTreatmentDay + "D");
			} else {
				buf.push(startTreatmentDay + "D-" + endTreatmentDay + "D");
			}
		}
				
		public static function isTimeWithin(time:Number, withinMilliseconds:int):Boolean
		{
			return !isNaN(time) && time > new Date().time - withinMilliseconds;
		}
		
		public static function formatUserName(user:String, userName:String):String
		{
			return StringUtil.trim(userName).length <= 0 ? toString(user) : userName + " " + formatBrackets(user); 
		}
		
		public static function toString(value:*):String 
		{
			return (value is Number && isNaN(value)) || value == null ? "" : String(value);
		}
		
		public static function concat(...values):String
		{
			var buf:Array = new Array();
			
			for each (var value:* in values) {
				if (!((value is Number && isNaN(value)) || value == null)) {
					buf.push(value);
				}
			}
			
			return buf.join(" ");
		}
		
		public static function toLowerCase(str:String):String
		{
			return str == null ? null : str.toLowerCase();
		}
		
		public static function toTitleCase(str:String):String
		{
			if (str == null)
				return null;
			
			return str.replace(/(\w{1})(\w*)/g, 
				function (match:String, firstChar:String, otherChars:String, index:int, full:String):String {
					return firstChar.toUpperCase() + otherChars.toLowerCase();
				}
			);			
		}
		
		public static function union(list1:IList, list2:IList):ArrayCollection
		{
			var resultList:ArrayCollection = new ArrayCollection();
			resultList.addAll(list1);
			for each (var item:Object in list2) {
				if (!exactContains(list1, item)) {
					resultList.addItem(item);
				}
			}
			return resultList;
		}
		
		public static function exactContains(list:IList, obj:Object):Boolean
		{
			for each (var item:Object in list) {
				if (item === obj) {
					return true;
				}
			}
			return false;
		}
		
		public static function removeItem(list:IList, obj:Object):void
		{
			var index:int = list.getItemIndex(obj);
			if (index != -1) {
				list.removeItemAt(index);
			}
		}
		
		public static function toArrayCollection(obj:Object):ArrayCollection
		{
			var resultList:ArrayCollection = new ArrayCollection();
			for each (var item:Object in obj) {
				resultList.addItem(item);
			}
			return resultList;
		}
		
		public static function filterList(list:IList, filterFunc:Function = null):ArrayCollection
		{
			var result:ArrayCollection = new ArrayCollection();
			for each (var item:Object in list) {
				if (filterFunc == null || filterFunc(item)) {
					result.addItem(item);
				}
			}
			return result;
		}
		
		public static function filterModifiedItemList(list:IList, filterManualItem:Boolean=false):ArrayCollection
		{
			if ( filterManualItem ) {
				return filterList(list, modifiedListFilterFunctionForVetting);
			}
			return filterList(list, modifiedListFilterFunction);
		}
		
		public static function filterUpdateItemList(itemList:IList, conflictItemIdList:IList):ArrayCollection
		{
			var result:ArrayCollection = new ArrayCollection();
			for each (var item:Object in itemList) {
				if (modifiedListFilterFunction(item) && !itemExists(item, conflictItemIdList)) {
					result.addItem(item);
				}
			}
			return result;
		}
		
		private static function itemExists(item:*, conflictItemList:IList):Boolean
		{
			for each (var conflictItemId:Number in conflictItemList) {
				if (item.id == conflictItemId) {
					return true;
				}
			}
			return false;
		}
		
		public static function modifiedListFilterFunctionForVetting(medProfileItem:*):Boolean {
			if ( medProfileItem.isManualItem ) {
				return false;
			}
			return modifiedListFilterFunction(medProfileItem);
		}
		
		public static function modifiedListFilterFunction(medProfileItem:*):Boolean 
		{
			if (medProfileItem.modified)
				return true;
			
			if (medProfileItem.medProfileMoItem == null)
				return false;
			
			for each (var medProfilePoItem:* in medProfileItem.medProfileMoItem.medProfilePoItemList) {
				if ((medProfileItem.status != MedProfileItemStatus.Unvet || !isNaN(medProfileItem.id)) && medProfilePoItem.modified)
					return true;
			}
			
			return false;
		}
		
		public static function selectedListFilterFunction(item:*):Boolean
		{
			return item.selected;
		}
		
		public static function formatUnit(value:*, unit:*):String 
		{
			unit = String(unit).match(/^[0-9]+/g).length > 0 ? 'x ' + unit : unit;
			return String(unit).replace(/\((.*?)\)/g, Number(value) > 1 ? "$1" : "");
		}
		
		public static function isValid(validator:Validator, supressEvents:Boolean = true):Boolean
		{
			var event:ValidationResultEvent = validator.validate(null, supressEvents);
			if (event == null) {
				return true;
			}
			
			for each (var result:ValidationResult in event.results) {
				if (result.isError) {
					return false;
				}
			}
			return true;
		}
		
		public static function scrollToSelectedItem(dataGrid:DataGrid):void
		{
			if (dataGrid.selectedIndex >= 0) {
				dataGrid.scrollToIndex(dataGrid.selectedIndex);
			}
		}
		
		public static function findParent(component:DisplayObject, classes:Array):Object
		{
			if (component == null || classes == null) {
				return null;
			} else {
				for each (var cls:Class in classes) {
					if (component is cls) {
						return component;
					}
				}
			}
			return findParent(component.parent, classes);
		}
		
		public static function containsParent(component:DisplayObject, cls:Class):Boolean
		{
			return findParent(component, [cls]) != null;
		}
		
		public static function isPopupExists(cls:Class, applicationInstance:Object = null):Boolean 
		{
			if (applicationInstance == null) {
				applicationInstance = FlexGlobals.topLevelApplication;
			}
			
			var rawChildren:IChildList = applicationInstance.systemManager.rawChildren;
			
			for (var i:int = 0; i < rawChildren.numChildren; i++)
			{
				var rawChild:UIComponent = rawChildren.getChildAt(i) as UIComponent;
				if (rawChild != null && rawChild.isPopUp && rawChild.visible && rawChild is cls) {
					return true;
				}
			}
			
			return false;
		}
		
		public static function removePopup(evt:Event):void
		{
			if (evt != null) {
				PopUpManager.removePopUp((evt.currentTarget as UIComponent).parentDocument as IFlexDisplayObject);
			}
		}
		
		public static function extractTextContent(textInput:TextInput):String {
			return textInput == null || StringUtil.trim(textInput.text).length <= 0 ? null : textInput.text;
		}
		
		public static function extractReviewDate(item:*):Date {
			
			var rxDrug:RxDrug = item.rxItem as RxDrug;
			var ivRxDrug:IvRxDrug = item.rxItem as IvRxDrug;
			var capdRxDrug:CapdRxDrug = item.rxItem as CapdRxDrug;
			
			if (rxDrug != null && rxDrug.regimen != null && rxDrug.regimen.doseGroupList.length > 0) {
				return rxDrug.regimen.doseGroupList.getItemAt(0).reviewDate;
			} else if (ivRxDrug != null) {
				return ivRxDrug.reviewDate;
			} else if (capdRxDrug != null && capdRxDrug.capdItemList.length > 0) {
				return capdRxDrug.capdItemList.getItemAt(0).reviewDate;
			}
			return null;
		}
		
		public static function getAlertSummary(alertProfile:AlertProfile):String {
			if ( alertProfile == null ) {
				return "";
			}
			var alertSummary:String = alertProfile.getAllAllergyDesc();
			
			if ( alertProfile.adrDesc != null && alertProfile.adrDesc.length > 0 ) {
				if ( alertSummary.length > 0 ) {
					alertSummary += ", ";
				} 
				alertSummary += alertProfile.adrDesc;
			}
			
			if ( alertProfile.patAlertList != null && alertProfile.patAlertList.length > 0 ) {
				for each ( var patAlert:PatAlert in alertProfile.patAlertList ) {
					if ( alertSummary.length > 0 ) {
						alertSummary += ", ";
					} 
					alertSummary += patAlert.alertDesc;
				}
			}
			
			if (alertProfile.ehrAllergySummary != null) {
				if (alertProfile.ehrAllergySummary.allergy != null) {
					if (alertSummary.length > 0) {
						alertSummary += ", ";
					}
					alertSummary += "[eHRSS Allergen]";
				}
				if (alertProfile.ehrAllergySummary.adr != null) {
					if (alertSummary.length > 0) {
						alertSummary += ", ";
					}
					alertSummary += "[eHRSS ADR Causative Agent]";
				}
			}
			
			return alertSummary;
		}
		
		public static function dissociateRemainingMedProfileItem(medProfileItemList:ListCollectionView, groupNum:Number):void {
			var mpi:MedProfileItem;
			var associatedMpiList:ArrayCollection = new ArrayCollection;
			for each (mpi in medProfileItemList) {
				if (mpi.groupNum == groupNum) {
					associatedMpiList.addItem(mpi);
				}
			}
			if (associatedMpiList.length == 1) {
				for each (mpi in associatedMpiList) {
					mpi.groupNum = Number.NaN;
				}
			}
		}
		
		public static function getGroupedMedProfileItemList(medProfileItemIn:MedProfileItem, medProfileItemListIn:ArrayCollection):ArrayCollection {
			var list:ArrayCollection = new ArrayCollection;
			if (isNaN(medProfileItemIn.groupNum)) {
				list.addItem(medProfileItemIn);
				return list;
			}
			
			for each (var mpi:MedProfileItem in medProfileItemListIn) {
				if (medProfileItemIn.groupNum == mpi.groupNum) {
					list.addItem(mpi);
				}
			}
			
			return list;
		}
		
		public static function activeListFilterFunction(item:*):Boolean 
		{
			var medProfileItem:MedProfileItem = item as MedProfileItem;
			return medProfileItem != null && medProfileItem.isManualItem && 
				(medProfileItem.status == MedProfileItemStatus.Vetted || medProfileItem.status == MedProfileItemStatus.Verified);
		}	
		
		public static function applySortToMedProfileItemList(srcList:ArrayCollection, destList:ArrayCollection, sortByDrugDesc:Boolean=false):void {
			var sortFieldList:ArrayCollection = new ArrayCollection();
			
			if (sortByDrugDesc) {
				sortFieldList.addItem(new SortField("medProfilePoItemDrugDesc", false, false));
			} else {					
				sortFieldList.addItem(new SortField("verifyDate", false, true));
				sortFieldList.addItem(new SortField("verifyIndex", false, true));
				sortFieldList.addItem(new SortField("createDate", false, true));
			}
			
			var sortedMpiList:ArrayCollection = new ArrayCollection(srcList.toArray());
			sortedMpiList.sort = new Sort();
			sortedMpiList.sort.fields = sortFieldList.toArray();
			sortedMpiList.filterFunction = activeListFilterFunction;
			sortedMpiList.refresh();
			
			destList.sort = null;
			destList.refresh();
			destList.removeAll();
			
			if (sortByDrugDesc) {
				destList.source = sortedMpiList.toArray();
			} else {
				for each (var sortedMpi:MedProfileItem in sortedMpiList) {
					if (destList.getItemIndex(sortedMpi) < 0) {
						destList.addAll(getGroupedMedProfileItemList(sortedMpi, sortedMpiList));
					}
				}
			}
			
			var sortSeq:int = 0;
			for each (var item:Object in destList) {
				item.sortSeq = sortSeq;
				sortSeq ++;
			}
			destList.refresh();
		}
		
		public static function getItemIndex(obj:*, compareField:String, itemList:ArrayCollection):int {
			var index:int = 0;
			for each (var item:* in itemList) {
				if (item[compareField] == obj[compareField]) {
					return index; 
				}
				++index;
			}
			return -1;
		}
		
		public static function findAndDeletePurchaseRequestListForWardTransferred(prListIn:ListCollectionView, mpItemList:ListCollectionView):void{
			var wtMpi:MedProfileItem;		
			var mpItemMap:IMap = new BasicMap();
			for each (var mpi:MedProfileItem in mpItemList) {
				mpItemMap.put(mpi.orgItemNum, mpi);
			}
			
			for each ( var delPr:PurchaseRequest in prListIn ){
				if( delPr.markDelete ){
					continue;
				}
				
				wtMpi = mpItemMap.get(delPr.mpiOrgItemNum) as MedProfileItem;
				if (wtMpi && ((MedProfileItemStatus.Ended == wtMpi.status && EndType.WardTransfered == wtMpi.endType) || mpItemList.getItemIndex(wtMpi) < 0) ) {
					if( delPr.isReadOnly() ){
						continue;
					}

					if( delPr.invoiceDate == null ){
						delPr.markSysDeleted();
					}
					delPr.mpPharmInfo.medProfileItemStatus = wtMpi.status;
					delPr.itemStatus = delPr.mpPharmInfo.medProfileItemStatus.displayValue;
				}
			}
		}
		
		public static function findAndDeletePurchaseRequestList(prListIn:ListCollectionView, mpItemList:ListCollectionView, voidInvoice:Boolean):void {
			var mpi:MedProfileItem;		
			var mpItemMap:IMap = new BasicMap();
			for each (var mappedMpi:MedProfileItem in mpItemList) {
				mpItemMap.put(mappedMpi.orgItemNum, mappedMpi);
			}
			
			var prList:ArrayCollection = new ArrayCollection(prListIn.toArray());
			for each ( var delPr:PurchaseRequest in prList ){
				if( delPr.markDelete ){
					continue;
				}
				
				mpi = mpItemMap.get(delPr.mpiOrgItemNum) as MedProfileItem;
				if (mpi && (mpi.status == MedProfileItemStatus.Deleted 
					|| mpi.status == MedProfileItemStatus.Discontinued
					|| mpi.discontModified
					|| mpItemList.getItemIndex(mpi) < 0)) {
					
					if( delPr.isReadOnly() ){
						continue;
					}
					
					if( isNaN(delPr.id) ){
						prListIn.removeItemAt(prListIn.getItemIndex(delPr));
					}else{
						var index:int = prListIn.getItemIndex(delPr);
						if( delPr.invoiceDate == null ){
							delPr.markSysDeleted();
						}else if( voidInvoice && delPr.invoiceDate != null ){
							delPr.markDelete = true;
							delPr.invoiceStatus = InvoiceStatus.Void;
						}
						delPr.itemStatus = mpi.status.displayValue;
						prListIn.setItemAt(delPr, index);
					}
				}
			}
		}
		
		public static function deleteReplenishment(replenishment:Replenishment, replenishmentList:ArrayCollection):void {
			replenishment.medProfileMoItem.medProfileItem.replenishFlag = false;
			replenishmentList.removeItemAt(replenishmentList.getItemIndex(replenishment));
		}
		
		public static function findAndDeleteReplenishment(replenishmentList:ArrayCollection, medProfileItemList:ArrayCollection):void {
			var mpi:MedProfileItem;
			var rList:ArrayCollection = new ArrayCollection(replenishmentList.toArray());
			for each (var replenishment:Replenishment in rList) {
				if (isNaN(replenishment.id)) {
					mpi = replenishment.medProfileMoItem.medProfileItem;
					if (mpi && (mpi.status == MedProfileItemStatus.Deleted
						|| mpi.status == MedProfileItemStatus.Discontinued
						|| mpi.discontModified
						|| medProfileItemList.getItemIndex(mpi) < 0)) {
						deleteReplenishment(replenishment, replenishmentList);
					}
				}
			}
		}
		
		public static function updateEditedPurchaseRequestExistFlag(mpiId:Number, mpPoiId:Number, mpPoiUuid:String, toDoListDictionary:Dictionary, activeListDictionary:Dictionary, purchaseRequestList:ArrayCollection):void{
			var mpPoiCount:int = 0;
			for each(var updatePr:PurchaseRequest in purchaseRequestList){
				if( updatePr.isReadOnly() || updatePr.markDelete ){
					continue;
				}
				
				if( updatePr.mpPharmInfo != null ){
					if( !isNaN(updatePr.mpPharmInfo.medProfilePoItemId) && updatePr.mpPharmInfo.medProfilePoItemId == mpPoiId ){
						mpPoiCount += 1;
					}else if( updatePr.mpPharmInfo.medProfilePoItemUuid == mpPoiUuid ){
						mpPoiCount += 1;
					}
				}
			}
			
			var editPoi:MedProfilePoItem = null;
			var editMpi:MedProfileItem = null;
			
			if( toDoListDictionary[mpiId] != null ){
				editMpi = toDoListDictionary[mpiId];
			}else if( activeListDictionary[mpiId] != null ){
				editMpi = activeListDictionary[mpiId];
			}
			
			if( editMpi != null ){
				for each(var poi:MedProfilePoItem in editMpi.medProfileMoItem.medProfilePoItemList){
					if( !isNaN(poi.id) && poi.id == mpPoiId ){
						editPoi = poi;
						break;
					}else if( poi.uuid == mpPoiUuid ){
						editPoi = poi;
						break;
					}
				}
				
				if( mpPoiCount > 0 ){
					editPoi.purchaseRequestExistFlag = true;
				}else{
					editPoi.purchaseRequestExistFlag = false;
				}
			}
		}
		
		public static function getOutstandingReplenishmentByMedProfileItem(mpi:MedProfileItem, replenishmentList:ListCollectionView):Replenishment
		{
			for each ( var replenishment:Replenishment in replenishmentList ) {
				if ( replenishment.medProfileMoItem.medProfileItem.id == mpi.id 
					&& replenishment.medProfileMoItem.itemNum == mpi.medProfileMoItem.itemNum 
					&& replenishment.status == ReplenishmentStatus.Outstanding
					&& replenishment.dispFlag) {
					return replenishment;
				}
			}
			return null;
		}
		
		public static function resetHomeLeave(medProfile:MedProfile, medProfileItemList:ArrayCollection):ArrayCollection {
			
			var datediff:int;
			
			datediff = daydiff(
				toDayBegin(medProfile.returnDate == null ? medProfile.homeLeaveDate : medProfile.returnDate),
				toDayBegin(TimeUtil.getCurrentServerTime()));
			
			medProfile.returnDate = null;
			medProfile.homeLeaveDate = null;
			
			return updateDueDate(datediff, medProfileItemList);
		}
		
		public static function updateDueDate(datediff:int, medProfileItemList:ArrayCollection):ArrayCollection 
		{
			if (datediff == 0) {
				return new ArrayCollection();
			}
			
			var manualItemList:ArrayCollection = new ArrayCollection();
			for each (var medProfileItem:MedProfileItem in medProfileItemList) {
				
				if ( medProfileItem.isManualItem ) {
					manualItemList.addItem(medProfileItem);
				}
				if (medProfileItem.status == MedProfileItemStatus.Unvet) {
					continue;
				}
				
				for each (var medProfilePoItem:MedProfilePoItem in medProfileItem.medProfileMoItem.medProfilePoItemList) {
					if (medProfilePoItem.dueDate != null) {
						var newDueDate:Date = new Date();
						newDueDate.time = medProfilePoItem.dueDate.time + (datediff * 86400000);
						medProfilePoItem.dueDate = newDueDate;
						medProfilePoItem.modified = true;
					}
				}
			}
			
			return manualItemList;
		}
		
		public static function equalsIgnoreCase(s1:String, s2:String):Boolean
		{
			return (s1 == null ? null : s1.toLowerCase()) == (s2 == null ? null : s2.toLowerCase());
		}
	}
}