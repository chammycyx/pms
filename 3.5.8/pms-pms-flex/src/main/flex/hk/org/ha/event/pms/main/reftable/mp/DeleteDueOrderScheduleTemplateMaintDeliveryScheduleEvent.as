package hk.org.ha.event.pms.main.reftable.mp {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class DeleteDueOrderScheduleTemplateMaintDeliveryScheduleEvent extends AbstractTideEvent 
	{	
		private var _deleteDeliveryScheduleId:Number;
		private var _callback:Function;
		
		public function DeleteDueOrderScheduleTemplateMaintDeliveryScheduleEvent(deleteDeliveryScheduleId:Number, callback:Function):void 
		{
			super();
			_deleteDeliveryScheduleId = deleteDeliveryScheduleId;
			_callback = callback;
		}
		
		public function get deleteDeliveryScheduleId():Number {
			return _deleteDeliveryScheduleId;
		}
		
		public function get callback():Function {
			return _callback;
		}
		
	}
}