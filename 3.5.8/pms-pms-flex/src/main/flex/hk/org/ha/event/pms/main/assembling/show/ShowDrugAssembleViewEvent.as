package hk.org.ha.event.pms.main.assembling.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDrugAssembleViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		public function ShowDrugAssembleViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}