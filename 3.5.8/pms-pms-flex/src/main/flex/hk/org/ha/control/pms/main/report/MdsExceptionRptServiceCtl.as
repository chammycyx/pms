package hk.org.ha.control.pms.main.report {

	import hk.org.ha.event.pms.main.report.ChangeMdsExceptionRptIndexEvent;
	import hk.org.ha.event.pms.main.report.PrintMdsExceptionRptEvent;
	import hk.org.ha.event.pms.main.report.RetrieveMdsExceptionRptByDeliveryReqIdEvent;
	import hk.org.ha.event.pms.main.report.RetrieveMdsExceptionRptByMedProfileAlertMsgEvent;
	import hk.org.ha.model.pms.biz.report.MdsExceptionRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("mdsExceptionRptServiceCtl", restrict="true")]
	public class MdsExceptionRptServiceCtl {

		[In]
		public var mdsExceptionRptService:MdsExceptionRptServiceBean;
		
		private var retrieveMdsExceptionRptByDeliveryReqIdEvent:RetrieveMdsExceptionRptByDeliveryReqIdEvent;
		
		[Observer]
		public function retrieveMdsExceptionRptByDeliveryReqId(evt:RetrieveMdsExceptionRptByDeliveryReqIdEvent):void {
			retrieveMdsExceptionRptByDeliveryReqIdEvent = evt;
			mdsExceptionRptService.retrieveMdsExceptionRptByDeliveryReqId(evt.deliveryReqId, retrieveMdsExceptionRptByDeliveryReqIdResult);
		}
		
		private function retrieveMdsExceptionRptByDeliveryReqIdResult(evt:TideResultEvent):void {
			if (retrieveMdsExceptionRptByDeliveryReqIdEvent.callback != null) {
				retrieveMdsExceptionRptByDeliveryReqIdEvent.callback(evt.result as Number);
			}
		}
		
		[Observer]
		public function printMdsExceptionRpt(evt:PrintMdsExceptionRptEvent):void {
			mdsExceptionRptService.printMdsExceptionRpt();
		}
		
		[Observer]
		public function retrieveMdsExceptionRptByMedProfileAlertMsg(evt:RetrieveMdsExceptionRptByMedProfileAlertMsgEvent):void {
			mdsExceptionRptService.retrieveMdsExceptionRptByMedProfileAlertMsg(evt.medProfile, evt.medProfileAlertMsgList, evt.alertProfile);
		}
		
		private var changeMdsExceptionRptIndexEvent:ChangeMdsExceptionRptIndexEvent;
		
		[Observer]
		public function changeMdsExceptionRptIndex(evt:ChangeMdsExceptionRptIndexEvent):void {
			changeMdsExceptionRptIndexEvent = evt;
			mdsExceptionRptService.changeReportIndex(evt.index, changeMdsExceptionRptIndexResult);
		}
		
		private function changeMdsExceptionRptIndexResult(evt:TideResultEvent):void {
			if (changeMdsExceptionRptIndexEvent.callback != null) {
				changeMdsExceptionRptIndexEvent.callback();
			}
		}
	}
}
