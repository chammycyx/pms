package hk.org.ha.event.pms.main.onestop {
		
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateOneStopPopupDismissTimeEvent extends AbstractTideEvent 
	{			
		public function UpdateOneStopPopupDismissTimeEvent():void 
		{
			super();
		}        
	}
}