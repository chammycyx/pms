package hk.org.ha.event.pms.main.alert.mds.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowMpLogonPopupEvent extends AbstractTideEvent {
		
		private var _permission:String;
		
		private var _successFunc:Function;
		
		public function ShowMpLogonPopupEvent(permission:String, successFunc:Function=null) {
			super();
			_permission = permission;
			_successFunc = successFunc == null ? function():void{} : successFunc;
		}
		
		public function get permission():String {
			return _permission;
		}
		
		public function get successFunc():Function {
			return _successFunc;
		}
	}
}