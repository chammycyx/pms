package hk.org.ha.event.pms.main.reftable.mp {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CreateWardGroupEvent extends AbstractTideEvent 
	{		
		private var _wardGroupCode:String;
		
		public function CreateWardGroupEvent(wardGroupCode:String):void 
		{
			super();
			_wardGroupCode = wardGroupCode;
		}
		
		public function get wardGroupCode():String {
			return _wardGroupCode;
		}
	}
}