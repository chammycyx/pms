package hk.org.ha.event.pms.main.alert.mds {
	
	import hk.org.ha.event.pms.main.alert.mds.popup.ShowAlertMsgPopupEvent;
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrieveAlertMsgForDrugSearchEvent extends AbstractTideEvent {

		private var _showAlertMsgPopupEvent:ShowAlertMsgPopupEvent;
		
		public function RetrieveAlertMsgForDrugSearchEvent(showAlertMsgPopupEvent:ShowAlertMsgPopupEvent=null) {
			super();
			_showAlertMsgPopupEvent = showAlertMsgPopupEvent;
		}
		
		public function get showAlertMsgPopupEvent():ShowAlertMsgPopupEvent {
			return _showAlertMsgPopupEvent;
		}
	}
}