package hk.org.ha.event.pms.main.refill.popup {

	import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class EditRefillScheduleAppointmentDateEvent extends AbstractTideEvent{		
		private var _editRefillSchedule:RefillSchedule;
		
		public function EditRefillScheduleAppointmentDateEvent(editRefillSchedule:RefillSchedule):void {
			super();
			_editRefillSchedule	= editRefillSchedule;	
		}
		
		public function get editRefillSchedule():RefillSchedule {
			return _editRefillSchedule;
		}
	}
}