package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDmDrugLiteListForChestItemEvent extends AbstractTideEvent 
	{
		private var _itemCode:String;
		private var _callBackEvent:Event; //for ItemCodeLookup call back set focus purpose

		public function RetrieveDmDrugLiteListForChestItemEvent(itemCode:String=null, callBackEvent:Event=null):void 
		{
			super();
			_itemCode = itemCode;
			_callBackEvent = callBackEvent;
		}
		
		public function get itemCode():String
		{
			return _itemCode;
		}
			
		public function get callBackEvent():Event
		{
			return _callBackEvent;
		}
	}
}