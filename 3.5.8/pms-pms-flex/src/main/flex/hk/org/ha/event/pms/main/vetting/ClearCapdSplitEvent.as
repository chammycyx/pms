package hk.org.ha.event.pms.main.vetting {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ClearCapdSplitEvent extends AbstractTideEvent {
	
		private var _callbackFunc:Function;
		
		public function ClearCapdSplitEvent(callbackFunc:Function) {
			super();
			_callbackFunc = callbackFunc;
		}
		
		public function get callbackFunc():Function{
			return _callbackFunc;
		}
	}
}
