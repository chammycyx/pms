package hk.org.ha.event.pms.main.vetting.popup
{
	import flash.events.Event;
	
	import hk.org.ha.model.pms.persistence.disp.MedOrder;
	import hk.org.ha.model.pms.udt.vetting.CmsOrderSubType;
	import hk.org.ha.model.pms.udt.vetting.CmsOrderType;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class ShowPharmRemarkOrderPopupEvent extends AbstractTideEvent
	{
		private var _cancelCallbackEvent:Event;	
		private var _logonUser:String;
		private var _medOrder:MedOrder;
		
		public function ShowPharmRemarkOrderPopupEvent()
		{
			super();
		}
		
		public function get cancelCallbackEvent():Event {
			return _cancelCallbackEvent;
		}
		
		public function set cancelCallbackEvent(cancelCallbackEvent:Event):void {
			_cancelCallbackEvent = cancelCallbackEvent;
		}
		
		public function get logonUser():String {
			return _logonUser;
		}
		
		public function set logonUser(logonUser:String):void {
			_logonUser = logonUser;
		}
		
		public function get medOrder():MedOrder {
			return _medOrder;
		}
		
		public function set medOrder(medOrder:MedOrder):void {
			_medOrder = medOrder;
		}
	}
}