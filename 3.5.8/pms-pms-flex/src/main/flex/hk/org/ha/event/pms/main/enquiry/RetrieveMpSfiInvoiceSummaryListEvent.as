package hk.org.ha.event.pms.main.enquiry
{
	import hk.org.ha.model.pms.udt.enquiry.SfiInvoiceEnqPatType;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveMpSfiInvoiceSummaryListEvent extends AbstractTideEvent 
	{
		private var _searchType:String;
		private var _param1:String;
		private var _param2:Date;
		private var _param3:Number;
		private var _includeVoid:Boolean;
		private var _patType:SfiInvoiceEnqPatType;
		
		public function RetrieveMpSfiInvoiceSummaryListEvent(searchType:String, param1:String, param2:Date, param3:Number,
														   includeVoid:Boolean, patType:SfiInvoiceEnqPatType):void
		{
			super();
			_searchType = searchType;
			_param1 = param1;
			_param2 = param2;
			_param3 = param3;
			_includeVoid = includeVoid;
			_patType = patType;
		}
		
		public function get searchType():String
		{
			return _searchType;
		}
		
		public function get param1():String
		{
			return _param1;
		}
		
		public function get param2():Date
		{
			return _param2;
		}
		
		public function get param3():Number
		{
			return _param3;
		}
				
		public function get includeVoid():Boolean
		{
			return _includeVoid;
		}
		
		public function get patType():SfiInvoiceEnqPatType
		{
			return _patType;
		}
	}
}