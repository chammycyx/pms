package hk.org.ha.control.pms.main.drug {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.drug.RetrieveDmMedicalOfficerEvent;
	import hk.org.ha.model.pms.biz.drug.DmMedicalOfficerServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("dmMedicalOfficerServiceCtl", restrict="true")]
	public class DmMedicalOfficerServiceCtl 
	{
		[In]
		public var dmMedicalOfficerService:DmMedicalOfficerServiceBean;
		
		private var successEvent:Event;
		
		private var failEvent:Event;
		
		[Observer]
		public function retrieveDmMedicalOfficer(evt:RetrieveDmMedicalOfficerEvent):void
		{
			successEvent = evt.successEvent;
			failEvent = evt.failEvent;
			
			In(Object(dmMedicalOfficerService).retrieveSuccess)
			dmMedicalOfficerService.retrieveDmMedicalOfficer(evt.medicalOfficerCode, retrieveDmMedicalOfficerResult);
		}
		
		private function retrieveDmMedicalOfficerResult(evt:TideResultEvent):void 
		{
			if(Object(dmMedicalOfficerService).retrieveSuccess){
				if(successEvent!=null){
					evt.context.dispatchEvent( successEvent );
				}
			}else{
				if(failEvent!=null){
					evt.context.dispatchEvent( failEvent );
				}
			}
		}
		
	}
}
