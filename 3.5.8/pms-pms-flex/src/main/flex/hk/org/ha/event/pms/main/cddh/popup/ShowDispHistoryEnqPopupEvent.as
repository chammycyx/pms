package hk.org.ha.event.pms.main.cddh.popup
{
	import hk.org.ha.model.pms.vo.cddh.CddhDispOrderItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDispHistoryEnqPopupEvent extends AbstractTideEvent 
	{
		private var _cddhDispOrderItem:CddhDispOrderItem;
		
		public function ShowDispHistoryEnqPopupEvent(cddhDispOrderItem:CddhDispOrderItem):void 
		{
			super();
			_cddhDispOrderItem = cddhDispOrderItem;
		}
		
		public function get cddhDispOrderItem():CddhDispOrderItem
		{
			return _cddhDispOrderItem;
		}
	}
}