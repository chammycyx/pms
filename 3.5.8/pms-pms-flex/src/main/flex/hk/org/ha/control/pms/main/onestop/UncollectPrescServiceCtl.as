package hk.org.ha.control.pms.main.onestop
{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.charging.popup.ShowSfiClearanceWarningPopupEvent;
	import hk.org.ha.event.pms.main.charging.popup.ShowSfiForceProceedPopupEvent;
	import hk.org.ha.event.pms.main.onestop.CheckUncollectOrderStatusEvent;
	import hk.org.ha.event.pms.main.onestop.ReleaseScreenLockEvent;
	import hk.org.ha.event.pms.main.onestop.ReversePrescUncollectEvent;
	import hk.org.ha.event.pms.main.onestop.SfiPrescriptionDeferReverseUncollectProceedEvent;
	import hk.org.ha.event.pms.main.onestop.SfiPrescriptionReverseUncollectForceProceedEvent;
	import hk.org.ha.event.pms.main.onestop.ShowInfomationMessageEvent;
	import hk.org.ha.event.pms.main.onestop.SuspendAllowModifyUncollectValidateOrderStatusFollowUpEvent;
	import hk.org.ha.event.pms.main.onestop.UncollectPrescEvent;
	import hk.org.ha.model.pms.biz.onestop.UncollectPrescServiceBean;
	import hk.org.ha.model.pms.vo.charging.FcsPaymentStatus;
	import hk.org.ha.model.pms.vo.security.PropMap;
	
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;

	[Bindable]
	[Name("uncollectPrescServiceCtl", restrict="true")]
	public class UncollectPrescServiceCtl
	{		
		[In]
		public var propMap:PropMap;
		
		[In]
		public var uncollectPrescService:UncollectPrescServiceBean;
		
		private var resultEvent:Event;
		private var dispOrderId:Number;
		private var checkUncollectOrderStatusEvent:CheckUncollectOrderStatusEvent;
		
		[Observer]
		public function checkUncollectOrderStatus(evt:CheckUncollectOrderStatusEvent):void {
			In(Object(uncollectPrescService).statusErrorMsg);
			checkUncollectOrderStatusEvent = evt;
			uncollectPrescService.checkUncollectOrderStatus(evt.dispOrderId, evt.dispOrderVersion, checkUncollectOrderStatusResult);
		}
		
		private function checkUncollectOrderStatusResult(evt:TideResultEvent):void {
			if (Object(uncollectPrescService).statusErrorMsg != null) 
			{
				evt.context.dispatchEvent(new ShowInfomationMessageEvent(new Array(Object(uncollectPrescService).statusErrorMsg), true));
			}
			else
			{
				evt.context.dispatchEvent(new SuspendAllowModifyUncollectValidateOrderStatusFollowUpEvent(
					checkUncollectOrderStatusEvent.followUpFunc));
			}
		}
		
		[Observer]
		public function uncollectPresc(evt:UncollectPrescEvent):void {
			resultEvent = evt.resultEvent;
			uncollectPrescService.uncollectPrescription(evt.dispOrderId, evt.pharmOrderVersion, evt.suspendCode, uncollectPrescResult, uncollectPrescFaultResult);
		}
		
		private function uncollectPrescResult(evt:TideResultEvent):void {
			if (resultEvent!=null) {
				evt.context.dispatchEvent(resultEvent);
			}
		}
		
		private function uncollectPrescFaultResult(evt:TideFaultEvent):void {
			evt.context.dispatchEvent(new ReleaseScreenLockEvent());
		}
		
		[Observer]
		public function reversePrescUncollect(evt:ReversePrescUncollectEvent):void {
			In(Object(uncollectPrescService).fcsPaymentStatus);
			In(Object(uncollectPrescService).sfiForceProceed);
			resultEvent = evt.resultEvent;
			dispOrderId = evt.dispOrderId;
			uncollectPrescService.reverseUncollectPrescription(evt.dispOrderId, evt.dispOrderVersion, reversePrescUncollectResult, reversePrescUncollectFaultResult);
		}
		
		private function reversePrescUncollectResult(evt:TideResultEvent):void {
			if (Object(uncollectPrescService).fcsPaymentStatus == null || Object(uncollectPrescService).sfiForceProceed) {
				evt.context.dispatchEvent(resultEvent);
			} else {
				evt.context.dispatchEvent(new ShowSfiClearanceWarningPopupEvent(Object(uncollectPrescService).fcsPaymentStatus.clearanceStatus ,"revUncollect", dispOrderId));
			}
		}
		
		private function reversePrescUncollectFaultResult(evt:TideFaultEvent):void {
			evt.context.dispatchEvent(new ReleaseScreenLockEvent());
		}
		
		[Observer]
		public function sfiPrescriptionDeferReverseUncollectProceed(evt:SfiPrescriptionDeferReverseUncollectProceedEvent):void {
			resultEvent = evt.resultEvent;
			In(Object(uncollectPrescService).statusErrorMsg);
			uncollectPrescService.sfiPrescriptionUncollectDeferProceed(evt.dispOrderId, evt.dispOrderVersion, sfiPrescriptionReverseUncollectResult);
		}
		
		[Observer]
		public function sfiPrescriptionReverseUncollectForceProceed(evt:SfiPrescriptionReverseUncollectForceProceedEvent):void {
			resultEvent = evt.resultEvent;
			In(Object(uncollectPrescService).statusErrorMsg);
			uncollectPrescService.sfiPrescriptionUncollectForceProceed(evt.dispOrderId, evt.dispOrderVersion, sfiPrescriptionReverseUncollectResult);
		}
		
		private function sfiPrescriptionReverseUncollectResult(evt:TideResultEvent):void {
			if (Object(uncollectPrescService).statusErrorMsg != null) 
			{
				evt.context.dispatchEvent(new ShowInfomationMessageEvent(new Array(Object(uncollectPrescService).statusErrorMsg), false));
			}
			
			evt.context.dispatchEvent(resultEvent);
		}
	}
}
