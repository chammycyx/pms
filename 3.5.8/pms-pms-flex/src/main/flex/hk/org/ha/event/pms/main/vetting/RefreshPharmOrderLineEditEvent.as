package hk.org.ha.event.pms.main.vetting {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dms.persistence.DmDrug;
	
	public class RefreshPharmOrderLineEditEvent extends AbstractTideEvent {

		public function RefreshPharmOrderLineEditEvent() {
			super();						
		}			
	}
}