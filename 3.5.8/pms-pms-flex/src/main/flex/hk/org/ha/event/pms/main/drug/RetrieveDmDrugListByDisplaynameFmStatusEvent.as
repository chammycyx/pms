package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dms.persistence.DmDrug;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDmDrugListByDisplaynameFmStatusEvent extends AbstractTideEvent
	{
		private var _dmDrug:DmDrug;
		
		public function RetrieveDmDrugListByDisplaynameFmStatusEvent(dmDrug:DmDrug):void 
		{
			super();
			_dmDrug = dmDrug;
		}
		
		public function get dmDrug():DmDrug
		{
			return _dmDrug;
		}
	}
}