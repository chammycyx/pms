package hk.org.ha.event.pms.main.reftable.mp {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class CreatePmsIpSpecMapExclEvent extends AbstractTideEvent {
		
		private var _patHospCode:String;
		
		private var _pasSpecialty:String;
		
		private var _ward:String;
		
		private var _callback:Function;
		
		public function CreatePmsIpSpecMapExclEvent(patHospCode:String, pasSpecialty:String, ward:String, callback:Function) {
			super();
			_patHospCode = patHospCode;
			_pasSpecialty = pasSpecialty;
			_ward = ward;
			_callback = callback;
		}
		
		public function get patHospCode():String {
			return _patHospCode;
		}
		
		public function get pasSpecialty():String {
			return _pasSpecialty
		}
		
		public function get ward():String {
			return _ward;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}
