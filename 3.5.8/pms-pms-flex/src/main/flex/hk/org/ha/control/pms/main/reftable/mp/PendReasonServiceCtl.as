package hk.org.ha.control.pms.main.reftable.mp {
	
	import hk.org.ha.event.pms.main.reftable.mp.RefreshPendingReasonMaintViewEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrievePendReasonListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.UpdatePendingReasonEvent;
	import hk.org.ha.event.pms.main.reftable.rdr.ReceivePendReasonRdrListEvent;
	import hk.org.ha.event.pms.main.reftable.rdr.RetrievePendReasonRdrListEvent;
	import hk.org.ha.model.pms.biz.reftable.mp.PendReasonServiceBean;
	import hk.org.ha.model.pms.biz.reftable.rdr.PendReasonReaderServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("pendReasonServiceCtl", restrict="true")]
	public class PendReasonServiceCtl 
	{
		[In]
		public var pendReasonService:PendReasonServiceBean;
		
		[In]
		public var pendReasonReaderService:PendReasonReaderServiceBean;
		
		[Observer]
		public function retrievePendingReasonList(evt:RetrievePendReasonListEvent):void
		{
			pendReasonService.retrievePendingReasonList(retrievePendingReasonListResult);
		}
		
		private function retrievePendingReasonListResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new RefreshPendingReasonMaintViewEvent('Retrieve'));
		}
		
		[Observer]
		public function updatePendingReasonList(evt:UpdatePendingReasonEvent):void
		{
			pendReasonService.updatePendingReasonList(evt.pendingReasonList, updatePendingReasonListResult);
		}
		
		private function updatePendingReasonListResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new RefreshPendingReasonMaintViewEvent('Save'));
		}
		
		[Observer]
		public function retrievePendReasonRdrList(evt:RetrievePendReasonRdrListEvent):void
		{
			pendReasonReaderService.retrievePendingReasonList(retrievePendReasonRdrListResult);
		}
		
		private function retrievePendReasonRdrListResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new ReceivePendReasonRdrListEvent());
		}
		
	}
}
