package hk.org.ha.control.pms.main.onestop
{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.charging.popup.ShowSfiClearanceWarningPopupEvent;
	import hk.org.ha.event.pms.main.charging.popup.ShowSfiForceProceedPopupEvent;
	import hk.org.ha.event.pms.main.onestop.CheckSuspendOrderStatusEvent;
	import hk.org.ha.event.pms.main.onestop.ReleaseScreenLockEvent;
	import hk.org.ha.event.pms.main.onestop.ReversePrescSuspendEvent;
	import hk.org.ha.event.pms.main.onestop.SfiPrescriptionDeferReverseSuspendProceedEvent;
	import hk.org.ha.event.pms.main.onestop.SfiPrescriptionReverseSuspendForceProceedEvent;
	import hk.org.ha.event.pms.main.onestop.ShowInfomationMessageEvent;
	import hk.org.ha.event.pms.main.onestop.SuspendAllowModifyUncollectValidateOrderStatusFollowUpEvent;
	import hk.org.ha.event.pms.main.onestop.SuspendPrescEvent;
	import hk.org.ha.model.pms.biz.onestop.OneStopServiceBean;
	import hk.org.ha.model.pms.biz.onestop.SuspendPrescServiceBean;
	import hk.org.ha.model.pms.udt.charging.ClearanceStatus;
	import hk.org.ha.model.pms.udt.reftable.OperationProfileType;
	import hk.org.ha.model.pms.vo.charging.FcsPaymentStatus;
	import hk.org.ha.model.pms.vo.security.PropMap;
	
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;

	[Bindable]
	[Name("suspendPrescServiceCtl", restrict="true")]
	public class SuspendPrescServiceCtl
	{
		[In]
		public var suspendPrescService:SuspendPrescServiceBean;
		
		[In]
		public var oneStopService:OneStopServiceBean;
		
		[In]
		public var propMap:PropMap;
		
		private var fcsPaymentStatus:FcsPaymentStatus;
		private var suspendPrescEvent:SuspendPrescEvent;
		private var checkSuspendOrderStatusEvent:CheckSuspendOrderStatusEvent;
		private var reversePrescSuspendEvent:ReversePrescSuspendEvent;
		private var resultEvent:Event;
		
		[Observer]
		public function checkSuspendOrderStatus(evt:CheckSuspendOrderStatusEvent):void {
			In(Object(suspendPrescService).statusErrorMsg);
			checkSuspendOrderStatusEvent = evt;
			suspendPrescService.checkSuspendOrderStatus(evt.dispOrderId, evt.dispOrderVersion, checkSuspendOrderStatusResult);
		}
		
		private function checkSuspendOrderStatusResult(evt:TideResultEvent):void {
			if (Object(suspendPrescService).statusErrorMsg != null) 
			{
				evt.context.dispatchEvent(new ShowInfomationMessageEvent(new Array(Object(suspendPrescService).statusErrorMsg), true));
			}
			else
			{
				evt.context.dispatchEvent(new SuspendAllowModifyUncollectValidateOrderStatusFollowUpEvent(
					checkSuspendOrderStatusEvent.followUpFunc));
			}
		}
		
		[Observer]
		public function suspendPresc(evt:SuspendPrescEvent):void {
			suspendPrescEvent = evt;
			suspendPrescService.verifySuspendPrescription(evt.dispOrderId, evt.dispOrderVersion, evt.suspendCode, evt.caller,
				 suspendPrescResult, suspendPrescFaultResult);
		}
		
		private function suspendPrescResult(evt:TideResultEvent):void {
			if (suspendPrescEvent.resultEvent != null) {
				evt.context.dispatchEvent(suspendPrescEvent.resultEvent);
			}
		}
		
		private function suspendPrescFaultResult(evt:TideFaultEvent):void {
			if (suspendPrescEvent.caller == "oneStopEntry")
			{
				evt.context.dispatchEvent(new ReleaseScreenLockEvent());
			}
		}
		
		[Observer]
		public function reversePrescSuspend(evt:ReversePrescSuspendEvent):void {
			In(Object(suspendPrescService).fcsPaymentStatus);
			In(Object(suspendPrescService).sfiForceProceed);
			reversePrescSuspendEvent = evt;
			suspendPrescService.reversePrescriptionSuspend(evt.dispOrderId, evt.dispOrderVersion, "oneStopEntry", 
				reversePrescSuspendResult, reversePrescSuspendFaultResult);
		}
				
		private function reversePrescSuspendResult(evt:TideResultEvent):void {
			if (Object(suspendPrescService).fcsPaymentStatus == null || Object(suspendPrescService).sfiForceProceed) {
				evt.context.dispatchEvent(reversePrescSuspendEvent.resultEvent);
			} else {
				//show ForceProceed Popup only in PMS Mode
				if ( OperationProfileType.Pms.displayValue == propMap.getValue("active.operation.mode") ){
					evt.context.dispatchEvent(new ShowSfiForceProceedPopupEvent(Object(suspendPrescService).fcsPaymentStatus.clearanceStatus, 
						"revSuspend", reversePrescSuspendEvent.dispOrderId));
				}else{
					evt.context.dispatchEvent(new ShowSfiClearanceWarningPopupEvent(Object(suspendPrescService).fcsPaymentStatus.clearanceStatus, 
						"revSuspend", reversePrescSuspendEvent.dispOrderId));
				}
			}
		}
		
		private function reversePrescSuspendFaultResult(evt:TideFaultEvent):void {
			evt.context.dispatchEvent(new ReleaseScreenLockEvent());
		}

		[Observer]
		public function sfiPrescriptionDeferReverseSuspendProceed(evt:SfiPrescriptionDeferReverseSuspendProceedEvent):void {
			resultEvent = evt.resultEvent;
			In(Object(suspendPrescService).statusErrorMsg);
			suspendPrescService.sfiPrescriptionSuspendDeferProceed(evt.dispOrderId, evt.dispOrderVersion, "oneStopEntry", sfiPrescriptionReverseSuspendResult);
		}
		
		[Observer]
		public function sfiPrescriptionReverseSuspendForceProceed(evt:SfiPrescriptionReverseSuspendForceProceedEvent):void {
			resultEvent = evt.resultEvent;
			In(Object(suspendPrescService).statusErrorMsg);
			suspendPrescService.sfiPrescriptionSuspendForceProceed(evt.dispOrderId, evt.dispOrderVersion, "oneStopEntry", sfiPrescriptionReverseSuspendResult);
		}
		
		private function sfiPrescriptionReverseSuspendResult(evt:TideResultEvent):void {
			if (Object(suspendPrescService).statusErrorMsg != null) 
			{
				evt.context.dispatchEvent(new ShowInfomationMessageEvent(new Array(Object(suspendPrescService).statusErrorMsg), false));
			}

			evt.context.dispatchEvent(resultEvent);
		}
	}
}
