package hk.org.ha.event.pms.main.charging {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.charging.MpSfiInvoice;
	
	public class SaveAndPrintMpSfiInvoiceEvent extends AbstractTideEvent 
	{	
		private var _mpSfiInvoice:MpSfiInvoice;
		
		public function SaveAndPrintMpSfiInvoiceEvent(mpSfiInvoice:MpSfiInvoice):void 
		{
			super();
			_mpSfiInvoice = mpSfiInvoice;
		}
		
		public function get mpSfiInvoice():MpSfiInvoice {
			return _mpSfiInvoice;
		}
	}
}