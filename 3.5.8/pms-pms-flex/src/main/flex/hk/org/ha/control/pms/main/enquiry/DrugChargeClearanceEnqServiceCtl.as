package hk.org.ha.control.pms.main.enquiry {
	
	import hk.org.ha.event.pms.main.enquiry.RefreshDrugChargeClearanceEvent;
	import hk.org.ha.event.pms.main.enquiry.RetrieveDrugChargeClearanceEvent;
	import hk.org.ha.model.pms.biz.enquiry.DrugChargeClearanceEnqServiceBean;
	import hk.org.ha.model.pms.vo.charging.DrugCheckClearanceResult;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	

	[Bindable]
	[Name("drugChargeClearanceEnqServiceCtl", restrict="true")]
	public class DrugChargeClearanceEnqServiceCtl 
	{
	
		[In]
		public var drugChargeClearanceEnqService:DrugChargeClearanceEnqServiceBean;
		
		[In]
		public var drugClearCheckResult:DrugCheckClearanceResult;
		
		[Observer]
		public function retrieveDrugChargeClearance(evt:RetrieveDrugChargeClearanceEvent):void
		{			
			drugChargeClearanceEnqService.retrieveDrugClearance(evt.searchCriteria, retrieveDrugChargeClearanceResult);
		}
		
		public function retrieveDrugChargeClearanceResult(evt:TideResultEvent):void{
			evt.context.dispatchEvent(new RefreshDrugChargeClearanceEvent(drugClearCheckResult));
		}		
	}
}
