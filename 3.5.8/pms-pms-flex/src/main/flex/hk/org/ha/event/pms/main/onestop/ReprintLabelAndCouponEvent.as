package hk.org.ha.event.pms.main.onestop {
		
	import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
	import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
	import hk.org.ha.model.pms.udt.printing.PrintLang;
	import hk.org.ha.model.pms.vo.onestop.ReprintOrderSummary;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
		
	public class ReprintLabelAndCouponEvent extends AbstractTideEvent 
	{
		private var _dispOrderItemList:ArrayCollection;
		private var _stdRefillScheduleItemList:ArrayCollection;
		private var _capdVoucherList:ArrayCollection;
		private var _reprintOrderSummary:ReprintOrderSummary;
		private var _printSfiInvoice:Boolean;
		private var _printSfiCoupon:Boolean;
		private var _printLanguage:PrintLang;
		private var _printMedSummaryReport:Boolean;
		private var _ticketDate:Date;
		private var _ticketNum:String;
		
		public function ReprintLabelAndCouponEvent(dispOrderItemList:ArrayCollection, stdRefillScheduleItemList:ArrayCollection, 
												   capdVoucherList:ArrayCollection, printSfiInvoice:Boolean, printSfiCoupon:Boolean, printLanguage:PrintLang, 
												   printMedSummaryReport:Boolean, ticketDate:Date, ticketNum:String):void 
		{
			super();
			_dispOrderItemList = dispOrderItemList;
			_stdRefillScheduleItemList = stdRefillScheduleItemList;
			_capdVoucherList = capdVoucherList;
			_printSfiInvoice = printSfiInvoice;
			_printSfiCoupon = printSfiCoupon;
			_printLanguage = printLanguage;
			_printMedSummaryReport = printMedSummaryReport;
			_ticketDate = ticketDate;
			_ticketNum = ticketNum;
		}   

		public function get dispOrderItemList():ArrayCollection
		{
			return _dispOrderItemList;
		}
		
		public function get stdRefillScheduleItemList():ArrayCollection
		{
			return _stdRefillScheduleItemList;
		}
		
		public function get capdVoucherList():ArrayCollection
		{
			return _capdVoucherList;
		}
		
		public function get printSfiInvoice():Boolean
		{
			return _printSfiInvoice;
		}
		
		public function get printSfiCoupon():Boolean
		{
			return _printSfiCoupon;
		}
				
		public function get printLanguage():PrintLang
		{
			return _printLanguage;
		}
		
		public function get printMedSummaryReport():Boolean
		{
			return _printMedSummaryReport;
		}
		
		public function get ticketDate():Date
		{
			return _ticketDate;
		}
		
		public function get ticketNum():String
		{
			return _ticketNum;
		}
	}
}