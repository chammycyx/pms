package hk.org.ha.event.pms.main.reftable.mp {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshMpItemEvent extends AbstractTideEvent 
	{
		private var _action:String;
		
		public function get action():String
		{
			return _action;
		}

		public function RefreshMpItemEvent(action:String):void 
		{
			_action = action;
			super();
		}

	}
}