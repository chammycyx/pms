package hk.org.ha.event.pms.main.onestop {
		
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowChargingPopupByPatientTypeEvent extends AbstractTideEvent 
	{	
		private var _title:String;
		
		public function ShowChargingPopupByPatientTypeEvent(title:String):void 
		{
			super();
			_title = title;
		}        
		
		public function get title():String {
			return _title;
		}
	}
}