package hk.org.ha.event.pms.main.alert.popup {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.alert.AlertProfileHistory;
	
	public class ShowAlertProfileHistoryPopupEvent extends AbstractTideEvent {

		private var _alertProfileHistory:AlertProfileHistory;
		
		public function ShowAlertProfileHistoryPopupEvent(alertProfileHistory:AlertProfileHistory) {
			super();
			_alertProfileHistory = alertProfileHistory;
		}
		
		public function get alertProfileHistory():AlertProfileHistory {
			return _alertProfileHistory;
		}
	}
}
