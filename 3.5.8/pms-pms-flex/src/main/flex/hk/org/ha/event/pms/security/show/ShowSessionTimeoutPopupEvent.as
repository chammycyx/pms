package hk.org.ha.event.pms.security.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowSessionTimeoutPopupEvent extends AbstractTideEvent 
	{
		
		public function ShowSessionTimeoutPopupEvent():void 
		{
			super();
		}
	}
}