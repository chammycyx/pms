package hk.org.ha.event.pms.main.picking
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CheckUserCodeEvent extends AbstractTideEvent 
	{		
		private var _userCode:String;
		private var _callDrugPick:Boolean;
		
		public function CheckUserCodeEvent(userCode:String, callDrugPick:Boolean):void 
		{
			super();
			_userCode = userCode;
			_callDrugPick = callDrugPick;
		}
		
		public function get userCode():String {
			return _userCode;
		}
		
		public function get callDrugPick():Boolean {
			return _callDrugPick;
		}
	}
}