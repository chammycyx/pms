package hk.org.ha.event.pms.main.vetting.mp.tpn.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class DuplicateTpnRecordEvent extends AbstractTideEvent
	{
		private var _duration:Number;
		private var _pnId:Number;
		private var _callback:Function;
		
		public function DuplicateTpnRecordEvent(duration:Number, pnId:Number, callback:Function):void {
			super();
			_duration = duration;
			_pnId = pnId;
			_callback = callback;
		}
		
		public function get duration():Number{
			return _duration;
		}
		
		public function get pnId():Number{
			return _pnId;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}