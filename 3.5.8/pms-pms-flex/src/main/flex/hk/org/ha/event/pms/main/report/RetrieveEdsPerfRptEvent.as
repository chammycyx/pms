package hk.org.ha.event.pms.main.report {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveEdsPerfRptEvent extends AbstractTideEvent 
	{
		private var _selectedTab:Number;
		
		public function RetrieveEdsPerfRptEvent(selectedTab:Number):void 
		{
			super();
			_selectedTab = selectedTab;
		}
		
		public function get selectedTab():Number{
			return _selectedTab;
		}
		
	}
}