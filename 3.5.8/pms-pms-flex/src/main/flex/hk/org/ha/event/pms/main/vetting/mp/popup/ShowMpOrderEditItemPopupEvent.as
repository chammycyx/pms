package hk.org.ha.event.pms.main.vetting.mp.popup {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	import hk.org.ha.model.pms.vo.rx.RxItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowMpOrderEditItemPopupEvent extends AbstractTideEvent 
	{						
		private var _rxItem:RxItem;		
		private var _okHandler:Function;
		private var _callBackEvent:Event;
		private var _manualProfileFlag:Boolean;
		
		public function ShowMpOrderEditItemPopupEvent(rxItemVal:RxItem, okHandlerVal:Function, manualProfileFlag:Boolean, eventVal:Event):void 
		{
			super();
			_rxItem 	= rxItemVal;			
			_okHandler 	= okHandlerVal;
			_manualProfileFlag = manualProfileFlag;
			_callBackEvent = eventVal;
		}
		
		public function get rxItem():RxItem 
		{
			return _rxItem;
		}		
		
		public function get okHandler():Function
		{
			return _okHandler;
		}
		
		public function get manualProfileFlag():Boolean
		{
			return _manualProfileFlag;
		}
		
		public function get callBackEvent():Event
		{
			return _callBackEvent;
		}
	}
}
