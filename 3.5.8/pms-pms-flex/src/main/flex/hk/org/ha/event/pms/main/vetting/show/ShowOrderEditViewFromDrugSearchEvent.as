package hk.org.ha.event.pms.main.vetting.show {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	import hk.org.ha.model.pms.udt.drug.DrugScope;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowOrderEditViewFromDrugSearchEvent extends AbstractTideEvent 
	{
		private var _moiSelectedIndex:Number;
		private var _autoAddDrug:Boolean;
		private var _drugScope:DrugScope;
		private var _clearMessages:Boolean;										
		
		public function ShowOrderEditViewFromDrugSearchEvent(moiSelectedIndex:Number=NaN, autoAddDrug:Boolean=false, drugScope:DrugScope=null, clearMessages:Boolean=true):void 
		{
			super();			
			_moiSelectedIndex = moiSelectedIndex;
			_autoAddDrug = autoAddDrug;	
			_drugScope = drugScope;	
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
		
		public function get moiSelectedIndex():Number {
			return _moiSelectedIndex;
		}		
		
		public function get autoAddDrug():Boolean {
			return _autoAddDrug;
		}

		public function get drugScope():DrugScope {
			return _drugScope;
		}
		
		public override function clone():Event {
			return new ShowOrderEditViewFromDrugSearchEvent(moiSelectedIndex, autoAddDrug, drugScope, clearMessages); 
		}
	}
}