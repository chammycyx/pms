package hk.org.ha.event.pms.main.reftable.mp {
	
	import hk.org.ha.model.pms.vo.reftable.mp.WardGroupInfo;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshWardGroupMaintViewEvent extends AbstractTideEvent 
	{				
		private var _wardGroupInfo:WardGroupInfo;
		
		public function RefreshWardGroupMaintViewEvent(wardGroupInfo:WardGroupInfo):void 
		{
			super();
			_wardGroupInfo = wardGroupInfo;
		}
		
		public function get wardGroupInfo():WardGroupInfo {
			return _wardGroupInfo;
		}
	}
}