package hk.org.ha.event.pms.main.reftable.mp {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshMpDurConversionRptListEvent extends AbstractTideEvent 
	{
		private var _durExportSuccess:Boolean;
		
		public function RefreshMpDurConversionRptListEvent(durExportSuccess:Boolean):void 
		{
			super();
			_durExportSuccess = durExportSuccess;
		}
		
		public function get durExportSuccess():Boolean
		{
			return _durExportSuccess;
		}
		
	}
}