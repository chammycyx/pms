package hk.org.ha.event.pms.main.alert.ehr.popup {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.alert.ehr.EhrAllergyDetail;
	
	public class ShowEhrAllergyDetailPopupEvent extends AbstractTideEvent {

		private var _ehrAllergyDetail:EhrAllergyDetail;
		
		public function ShowEhrAllergyDetailPopupEvent(ehrAllergyDetail:EhrAllergyDetail) {
			super();
			_ehrAllergyDetail = ehrAllergyDetail;
		}
		
		public function get ehrAllergyDetail():EhrAllergyDetail {
			return _ehrAllergyDetail;
		}
	}
}
