package hk.org.ha.event.pms.main.reftable {
	
	import hk.org.ha.model.pms.persistence.reftable.Chest;
	import org.granite.tide.events.AbstractTideEvent;
	
	public class DeleteChestEvent extends AbstractTideEvent 
	{
		private var _chest:Chest; 
		
		public function DeleteChestEvent(chest:Chest):void 
		{
			super();
			_chest = chest;
		}
		
		public function get chest():Chest {
			return _chest;
		}

	}
}