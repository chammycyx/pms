package hk.org.ha.control.pms.main.alert.mds {
	import hk.org.ha.event.pms.main.alert.mds.AcknowledgeOverrideReasonForIpEvent;
	import hk.org.ha.event.pms.main.alert.mds.AcknowledgeOverrideReasonForOpEvent;
	import hk.org.ha.event.pms.main.alert.mds.AuthenticateAcknowledgePermissionForIpEvent;
	import hk.org.ha.event.pms.main.alert.mds.AuthenticateAcknowledgePermissionForOpEvent;
	import hk.org.ha.event.pms.main.alert.mds.AuthenticateMpPermissionEvent;
	import hk.org.ha.event.pms.main.alert.mds.AuthenticateOverridePermissionEvent;
	import hk.org.ha.event.pms.main.alert.mds.CheckPreparationEvent;
	import hk.org.ha.event.pms.main.alert.mds.EditItemReasonEvent;
	import hk.org.ha.event.pms.main.alert.mds.EditPrescReasonEvent;
	import hk.org.ha.event.pms.main.alert.mds.RetrieveAlertMsgByItemNumEvent;
	import hk.org.ha.event.pms.main.alert.mds.RetrieveAlertMsgByMedOrderItemEvent;
	import hk.org.ha.event.pms.main.alert.mds.RetrieveAlertMsgForDrugSearchEvent;
	import hk.org.ha.event.pms.main.alert.mds.RetrieveMedProfileAlertMsgEvent;
	import hk.org.ha.event.pms.main.alert.mds.RetrieveOverridePermissionEvent;
	import hk.org.ha.event.pms.main.alert.mds.popup.ShowAcknowledgeReasonLogonPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	import hk.org.ha.model.pms.biz.alert.mds.OverrideReasonServiceBean;
	import hk.org.ha.model.pms.persistence.AlertEntity;
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	import hk.org.ha.model.pms.persistence.disp.MedOrderItemAlert;
	import hk.org.ha.model.pms.persistence.disp.PharmOrder;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItemAlert;
	import hk.org.ha.model.pms.vo.alert.mds.Alert;
	import hk.org.ha.model.pms.vo.alert.mds.AlertDdim;
	import hk.org.ha.model.pms.vo.alert.mds.MedProfileAlertMsg;
	import hk.org.ha.model.pms.vo.alert.mds.OverrideReasonResult;
	import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;
	import hk.org.ha.model.pms.vo.vetting.mp.MedProfileVettingContext;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;

	[Bindable]
	[Name("overrideReasonServiceCtl", restrict="true")]
	public class OverrideReasonServiceCtl {

		[In]
		public var ctx:Context;
		
		[In]
		public var overrideReasonService:OverrideReasonServiceBean;
		
		[In]
		public var pharmOrder:PharmOrder;
		
		[In]
		public var drugSearchMedOrderItem:MedOrderItem;
		
		[In]
		public var orderViewInfo:OrderViewInfo;
		
		[In]
		public var medProfileVettingContext:MedProfileVettingContext;
		
		private var orderEditFlag:Boolean;
		private var alertMedOrderItem:MedOrderItem;
		private var alertMedProfileMoItem:MedProfileMoItem;
		
		private var acknowledgeOverrideReasonForOpEvent:AcknowledgeOverrideReasonForOpEvent;
		private var acknowledgeOverrideReasonForIpEvent:AcknowledgeOverrideReasonForIpEvent;
		
		private var showAcknowledgeReasonLogonPopupEvent:ShowAcknowledgeReasonLogonPopupEvent;
		
		private var authenticateAcknowledgePermissionForOpEvent:AuthenticateAcknowledgePermissionForOpEvent;
		private var authenticateAcknowledgePermissionForIpEvent:AuthenticateAcknowledgePermissionForIpEvent;
		
		private var authenticateOverridePermissionEvent:AuthenticateOverridePermissionEvent;
		
		private var editItemReasonEvent:EditItemReasonEvent;
		
		private var editPrescReasonEvent:EditPrescReasonEvent;
		
		private var retrieveOverridePermissionEvent:RetrieveOverridePermissionEvent;
		
		[Observer]
		public function acknowledgeOverrideReasonForOp(evt:AcknowledgeOverrideReasonForOpEvent):void {
			acknowledgeOverrideReasonForOpEvent = evt;
			overrideReasonService.acknowledgeOverrideReasonForOp(acknowledgeOverrideReasonResultForOp);
		}
		
		private function acknowledgeOverrideReasonResultForOp(evt:TideResultEvent):void {

			var overrideReasonResult:OverrideReasonResult = evt.result as OverrideReasonResult;
			
			if (overrideReasonResult.successFlag) {
				
				setAckUserInfoForMoi(alertMedOrderItem, overrideReasonResult);
				
				if (acknowledgeOverrideReasonForOpEvent.successFunc != null) {
					acknowledgeOverrideReasonForOpEvent.successFunc();
				}
			}
			else {
				var successFunc:Function = function():void {
					dispatchEvent(acknowledgeOverrideReasonForOpEvent.constructCopy());
				};
				var cancelFunc:Function = function():void {
					if (acknowledgeOverrideReasonForOpEvent.cancelFunc != null) {
						acknowledgeOverrideReasonForOpEvent.cancelFunc();
					}
				}
				dispatchEvent(new ShowAcknowledgeReasonLogonPopupEvent("OP", successFunc, cancelFunc));
			}
		}
		
		[Observer]
		public function acknowledgeOverrideReasonForIp(evt:AcknowledgeOverrideReasonForIpEvent):void {
			acknowledgeOverrideReasonForIpEvent = evt;
			overrideReasonService.acknowledgeOverrideReasonForIp(medProfileVettingContext.oneTimeMdsAckPermissionUser, acknowledgeOverrideReasonResultForIp);
		}
		
		private function acknowledgeOverrideReasonResultForIp(evt:TideResultEvent):void {
			
			var overrideReasonResult:OverrideReasonResult = evt.result as OverrideReasonResult;
			
			if (overrideReasonResult.successFlag) {
				
				setAckUserInfoForMpMoItem(alertMedProfileMoItem, overrideReasonResult);
				
				if (acknowledgeOverrideReasonForIpEvent.callbackFunc != null) {
					acknowledgeOverrideReasonForIpEvent.callbackFunc();
				}
			}
			else {
				dispatchEvent(new ShowAcknowledgeReasonLogonPopupEvent("IP", function():void {
					dispatchEvent(acknowledgeOverrideReasonForIpEvent.constructCopy());
				}));
			}
		}
		
		private function setAckUserInfoForMoi(medOrderItem:MedOrderItem, overrideReasonResult:OverrideReasonResult):void {
			
			for each (var medOrderItemAlert:MedOrderItemAlert in medOrderItem.medOrderItemAlertList) {
				medOrderItemAlert.ackUser = overrideReasonResult.ackUser;
				medOrderItemAlert.ackDate = overrideReasonResult.ackDate;
			}
			
			medOrderItem.changeFlag = true;
		}
		
		private function setAckUserInfoForMpMoItem(medProfileMoItem:MedProfileMoItem, overrideReasonResult:OverrideReasonResult):void {
			
			for each (var medProfileMoItemAlert:MedProfileMoItemAlert in medProfileMoItem.medProfileMoItemAlertList) {
				medProfileMoItemAlert.ackUser = overrideReasonResult.ackUser;
				medProfileMoItemAlert.ackDate = overrideReasonResult.ackDate;
			}
		}
		
		[Observer]
		public function authenticateAcknowledgePermissionForOp(evt:AuthenticateAcknowledgePermissionForOpEvent):void {
			authenticateAcknowledgePermissionForOpEvent = evt;
			overrideReasonService.authenticateAcknowledgePermissionForOp(evt.userName, evt.password, authenticateAcknowledgePermissionForOpResult);
		}
		
		private function authenticateAcknowledgePermissionForOpResult(evt:TideResultEvent):void {

			var overrideReasonResult:OverrideReasonResult = evt.result as OverrideReasonResult;
			
			if (overrideReasonResult.successFlag) {
				orderViewInfo.acknowledgeReasonUser = authenticateAcknowledgePermissionForOpEvent.userName;
				if (authenticateAcknowledgePermissionForOpEvent.successCallback != null) {
					authenticateAcknowledgePermissionForOpEvent.successCallback();
				}
			}
			else {
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp(overrideReasonResult.msgCode);
				msgProp.setOkButtonOnly = true;
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
				if (authenticateAcknowledgePermissionForOpEvent.faultCallback != null) {
					authenticateAcknowledgePermissionForOpEvent.faultCallback();
				}
			}
		}
		
		[Observer]
		public function authenticateAcknowledgePermissionForIp(evt:AuthenticateAcknowledgePermissionForIpEvent):void {
			authenticateAcknowledgePermissionForIpEvent = evt;
			overrideReasonService.authenticateAcknowledgePermissionForIp(evt.userName, evt.password, authenticateAcknowledgePermissionForIpResult);
		}
		
		private function authenticateAcknowledgePermissionForIpResult(evt:TideResultEvent):void {
			
			var overrideReasonResult:OverrideReasonResult = evt.result as OverrideReasonResult;
			
			if (overrideReasonResult.successFlag) {
				medProfileVettingContext.oneTimeMdsAckPermissionUser = overrideReasonResult.ackUser;
				if (authenticateAcknowledgePermissionForIpEvent.successCallback != null) {
					authenticateAcknowledgePermissionForIpEvent.successCallback();
				}
			}
			else {
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp(overrideReasonResult.msgCode);
				msgProp.setOkButtonOnly = true;
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
				if (authenticateAcknowledgePermissionForIpEvent.faultCallback != null) {
					authenticateAcknowledgePermissionForIpEvent.faultCallback();
				}
			}
		}
		
		[Observer]
		public function authenticateOverridePermission(evt:AuthenticateOverridePermissionEvent):void {
			authenticateOverridePermissionEvent = evt;
			overrideReasonService.authenticateOverridePermission(evt.userName, evt.password, authenticateOverridePermissionResult);
		}
		
		private function authenticateOverridePermissionResult(evt:TideResultEvent):void {
			
			var overrideReasonResult:OverrideReasonResult = evt.result as OverrideReasonResult;
			
			if (overrideReasonResult.successFlag) {
				orderViewInfo.overrideAlertUser = authenticateOverridePermissionEvent.userName;
				if (authenticateOverridePermissionEvent.successCallback != null) {
					authenticateOverridePermissionEvent.successCallback();
				}
			}
			else {
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp(overrideReasonResult.msgCode);
				msgProp.setOkButtonOnly = true;
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
				if (authenticateOverridePermissionEvent.faultCallback != null) {
					authenticateOverridePermissionEvent.faultCallback();
				}
			}
		} 
		
		[Observer]
		public function retrieveOverridePermission(evt:RetrieveOverridePermissionEvent):void {
			retrieveOverridePermissionEvent = evt;
			overrideReasonService.retrieveOverridePermission(retrieveOverridePermissionResult);
		}
		
		private function retrieveOverridePermissionResult(evt:TideResultEvent):void {
			
			var overrideReasonResult:OverrideReasonResult = evt.result as OverrideReasonResult;
			
			if (overrideReasonResult.successFlag) {
				if (retrieveOverridePermissionEvent.successCallbackFunc != null) {
					retrieveOverridePermissionEvent.successCallbackFunc();
				}
			}
			else {
				if (retrieveOverridePermissionEvent.faultCallbackFunc != null) {
					retrieveOverridePermissionEvent.faultCallbackFunc();
				}
			}
		}
		
		[Observer]
		public function editItemReason(evt:EditItemReasonEvent):void {
			editItemReasonEvent = evt;
			overrideReasonService.editItemReason(evt.overrideReason, evt.ddiAlertIndex, orderEditFlag, editItemReasonResult);
		}
		
		private function editItemReasonResult(evt:TideResultEvent):void {
			
			setOverrideReasonToMedOrderItem(evt.result as OverrideReasonResult);

			if (editItemReasonEvent.callbackFunc != null) {
				editItemReasonEvent.callbackFunc();
			}
		}
		
		[Observer]
		public function editPrescReason(evt:EditPrescReasonEvent):void {
			editPrescReasonEvent = evt;
 			overrideReasonService.editPrescReason(evt.overrideReason, evt.alertIndex, editPrescReasonResult);
		}
		
		private function editPrescReasonResult(evt:TideResultEvent):void {
			if (editPrescReasonEvent.callbackFunc != null) {
				editPrescReasonEvent.callbackFunc();
			}
		}
		
		private function setOverrideReasonToMedOrderItem(overrideReasonResult:OverrideReasonResult):void {
			alertMedOrderItem.changeFlag = true;
			for each (var alertEntity:AlertEntity in getAlertEntityList(alertMedOrderItem, editItemReasonEvent.ddiAlertIndex)) {
				alertEntity.overrideReason = editItemReasonEvent.overrideReason;
				alertEntity.ackUser = overrideReasonResult.ackUser;
				alertEntity.ackDate = overrideReasonResult.ackDate;
			}
		}
		
		private function getAlertEntityList(moi:MedOrderItem, ddiAlertIndex:Number):ListCollectionView {
			if (isNaN(ddiAlertIndex)) {
				return moi.patAlertList;
			}
			
			var moiAlert:MedOrderItemAlert = moi.ddiAlertList.getItemAt(ddiAlertIndex) as MedOrderItemAlert;
			if (((moiAlert.alert) as AlertDdim).isOnHand || orderEditFlag) {
				return new ArrayCollection(new Array(moiAlert));
			}
			else {
				var list:ListCollectionView = new ArrayCollection();
				list.addItem(moiAlert);
				list.addItem(moi.medOrder.getDdiRelatedAlert(moi.itemNum, moiAlert.alert as AlertDdim));
				return list;
			}
		}
		
		[Observer]
		public function setAlertMedOrderItemByItemNum(evt:RetrieveAlertMsgByItemNumEvent):void {
			if (evt.allowEditFlag) {
				alertMedOrderItem = pharmOrder.medOrder.getMedOrderItemByItemNum(evt.itemNum);
				orderEditFlag = false;
			}
		}
		
		[Observer]
		public function setAlertMedOrderItemByMedOrderItem(evt:RetrieveAlertMsgByMedOrderItemEvent):void {
			alertMedOrderItem = evt.medOrderItem;
			orderEditFlag = evt.orderEditFlag;
		}
		
		[Observer]
		public function setAlertMedOrderItemForDrugSearch(evt:RetrieveAlertMsgForDrugSearchEvent):void {
			alertMedOrderItem = drugSearchMedOrderItem;
			orderEditFlag = false;
		}
		
		[Observer]
		public function setAlertMedOrderItemForCheckPreparation(evt:CheckPreparationEvent):void {
			alertMedOrderItem = evt.medOrderItem;
			orderEditFlag = false;
		}
		
		[Observer]
		public function setMedProfileMoItem(evt:RetrieveMedProfileAlertMsgEvent):void {
			if (evt.medItem is MedProfileMoItem) {
				alertMedProfileMoItem = evt.medItem as MedProfileMoItem;
			}
		}
		
		private var authenticateMpPermissionEvent:AuthenticateMpPermissionEvent;
		
		[Observer]
		public function authenticateMpPermission(evt:AuthenticateMpPermissionEvent):void {
			authenticateMpPermissionEvent = evt;
			overrideReasonService.authenticateMpPermission(evt.userName, evt.password, evt.permission, authenticateMpPermissionResult);
		}
		
		private function authenticateMpPermissionResult(evt:TideResultEvent):void {
			authenticateMpPermissionEvent.callback(evt.result as String);
		}
	}
}