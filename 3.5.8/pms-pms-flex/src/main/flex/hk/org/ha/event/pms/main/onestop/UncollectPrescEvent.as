package hk.org.ha.event.pms.main.onestop
{
	import flash.events.Event;
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;

	public class UncollectPrescEvent extends AbstractTideEvent
	{
		private var _dispOrderId:Number;
		private var _suspendCode:String;
		private var _pharmOrderVersion:Number;
		private var _resultEvent:Event;
		
		public function UncollectPrescEvent(dispOrderId:Number, pharmOrderVersion:Number, suspendCode:String, resultEvent:Event=null)
		{
			super();
			_dispOrderId = dispOrderId;
			_pharmOrderVersion = pharmOrderVersion;
			_suspendCode = suspendCode;
			_resultEvent = resultEvent;
		}
		
		public function get dispOrderId():Number {
			return _dispOrderId;
		}
		
		public function get pharmOrderVersion():Number {
			return _pharmOrderVersion;
		}
		
		public function get suspendCode():String {
			return _suspendCode;
		}
		
		public function get resultEvent():Event {
			return _resultEvent;
		}
	}
}