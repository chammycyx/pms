package hk.org.ha.event.pms.main.alert.mds {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class AuthenticateMpPermissionEvent extends AbstractTideEvent {
		
		private var _userName:String;
		
		private var _password:String;
		
		private var _permission:String;
		
		private var _callback:Function;
		
		public function AuthenticateMpPermissionEvent(userName:String, password:String, permission:String, callback:Function=null) {
			super();
			_userName = userName;
			_password = password;
			_permission = permission;
			_callback = callback == null ? function(o:*):void{} : callback;
		}
		
		public function get userName():String {
			return _userName;
		}
		
		public function get password():String {
			return _password;
		}
		
		public function get permission():String {
			return _permission;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}