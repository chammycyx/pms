package hk.org.ha.event.pms.main.refill {
	
	import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
	import hk.org.ha.model.pms.udt.printing.PrintLang;	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ReprintRefillCouponEvent extends AbstractTideEvent 
	{
		private var _refillSchedule:RefillSchedule;		
		private var _printLang:PrintLang;
		private var _numOfCoupon:Number;
		
		public function ReprintRefillCouponEvent(refillSchedule:RefillSchedule, printLang:PrintLang, numOfCoupon:Number):void 
		{
			super();
			_refillSchedule = refillSchedule;
			_printLang = printLang;
			_numOfCoupon = numOfCoupon;
		}
		
		public function get refillSchedule():RefillSchedule {
			return _refillSchedule;
		}
		
		public function get printLang():PrintLang {
			return _printLang;
		}
		
		public function get numOfCoupon():Number {
			return _numOfCoupon;
		}
		
	}
}