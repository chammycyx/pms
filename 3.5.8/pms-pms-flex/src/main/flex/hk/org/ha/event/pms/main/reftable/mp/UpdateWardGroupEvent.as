package hk.org.ha.event.pms.main.reftable.mp {
	
	import hk.org.ha.model.pms.persistence.reftable.WardGroup;
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateWardGroupEvent extends AbstractTideEvent 
	{		
		private var _wardGroup:WardGroup;
		private var _selectedWardList:ListCollectionView;
		
		public function UpdateWardGroupEvent(wardGroup:WardGroup, selectedWardList:ListCollectionView):void 
		{
			super();
			_wardGroup = wardGroup;
			_selectedWardList = selectedWardList;
		}
		
		public function get wardGroup():WardGroup {
			return _wardGroup;
		}
		
		public function get selectedWardList():ListCollectionView {
			return _selectedWardList;
		}
	}
}