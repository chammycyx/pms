package hk.org.ha.event.pms.main.enquiry
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshDrugSetEnqViewEvent extends AbstractTideEvent 
	{	
		private var _subLevelFlag:Boolean;
		
		public function RefreshDrugSetEnqViewEvent(subLevelFlag:Boolean):void 
		{
			super();
			_subLevelFlag = subLevelFlag;
		}
		
		public function get subLevelFlag():Boolean {
			return _subLevelFlag;
		}
	}
}