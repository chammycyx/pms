package hk.org.ha.event.pms.main.alert.mds {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	import mx.collections.ListCollectionView;
	
	public class EditItemReasonEvent extends AbstractTideEvent {

		private var _overrideReason:ListCollectionView;
		
		private var _ddiAlertIndex:Number;
		
		private var _callbackFunc:Function;
		
		public function EditItemReasonEvent(overrideReason:ListCollectionView, ddiAlertIndex:Number, callbackFunc:Function=null) {
			super();
			_overrideReason = overrideReason;
			_ddiAlertIndex = ddiAlertIndex;
			_callbackFunc = callbackFunc;
		}
		
		public function get overrideReason():ListCollectionView {
			return _overrideReason;
		}
		
		public function get ddiAlertIndex():Number {
			return _ddiAlertIndex;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}
