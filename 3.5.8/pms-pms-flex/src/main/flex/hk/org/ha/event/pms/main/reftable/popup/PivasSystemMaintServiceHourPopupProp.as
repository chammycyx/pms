package hk.org.ha.event.pms.main.reftable.popup
{
	import hk.org.ha.model.pms.persistence.reftable.pivas.PivasServiceHour;
	
	import mx.collections.ArrayCollection;

	public class PivasSystemMaintServiceHourPopupProp
	{
		public var action:String; //add, update
		public var pivasServiceHour:PivasServiceHour;
		public var pivasServiceHourList:ArrayCollection;
		public var okHandler:Function;
	}
}
