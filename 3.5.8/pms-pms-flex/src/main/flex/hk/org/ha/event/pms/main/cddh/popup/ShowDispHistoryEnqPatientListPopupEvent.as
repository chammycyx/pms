package hk.org.ha.event.pms.main.cddh.popup
{
	import hk.org.ha.model.pms.vo.cddh.CddhCriteria;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDispHistoryEnqPatientListPopupEvent extends AbstractTideEvent 
	{
		private var _cddhDispOrderList:ArrayCollection;
		private var _cddhCriteria:CddhCriteria;
		private var _popupOkHandler:Function;
		private var _popupCancelHandler:Function;
		
		public function ShowDispHistoryEnqPatientListPopupEvent(cddhDispOrderList:ArrayCollection, cddhCriteria:CddhCriteria, popupOkHandler:Function, popupCancelHandler:Function):void 
		{
			super();
			_cddhDispOrderList = cddhDispOrderList;
			_cddhCriteria = cddhCriteria;
			_popupOkHandler = popupOkHandler;
			_popupCancelHandler = popupCancelHandler;
		}
		
		public function get cddhDispOrderList():ArrayCollection {
			return _cddhDispOrderList;
		}
		
		public function set cddhDispOrderList(cddhDispOrderList:ArrayCollection):void {
			_cddhDispOrderList = cddhDispOrderList;
		}
		
		public function get cddhCriteria():CddhCriteria {
			return _cddhCriteria;
		}
		
		public function set cddhCriteria(cddhCriteria:CddhCriteria):void {
			_cddhCriteria = cddhCriteria;
		}
		
		public function get popupOkHandler():Function {
			return _popupOkHandler;
		}
		
		public function set popupOkHandler(popupOkHandler:Function):void {
			_popupOkHandler = popupOkHandler;
		}

		public function get popupCancelHandler():Function {
			return _popupCancelHandler;
		}
		
		public function set popupCancelHandler(popupCancelHandler:Function):void {
			_popupCancelHandler = popupCancelHandler;
		}
	}
}