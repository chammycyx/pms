package hk.org.ha.event.pms.main.cddh {
	
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.cddh.CddhCriteria;
	
	public class RetrievePatientListEvent extends AbstractTideEvent 
	{
		private var _cddhCriteria:CddhCriteria;
		
		public function RetrievePatientListEvent(cddhCriteria:CddhCriteria):void 
		{
			super();
			_cddhCriteria = cddhCriteria;
		}
		
		public function get cddhCriteria():CddhCriteria
		{
			return _cddhCriteria;
		}
	}
}