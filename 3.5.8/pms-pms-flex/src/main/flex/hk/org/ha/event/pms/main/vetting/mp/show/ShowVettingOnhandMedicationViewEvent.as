package hk.org.ha.event.pms.main.vetting.mp.show
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowVettingOnhandMedicationViewEvent extends AbstractTideEvent
	{
		public function ShowVettingOnhandMedicationViewEvent()
		{
			super();
		}
	}
}