package hk.org.ha.event.pms.security.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowUnauthorizeWorkstationPopupEvent extends AbstractTideEvent 
	{
		
		public function ShowUnauthorizeWorkstationPopupEvent():void 
		{
			super();
		}
	}
}