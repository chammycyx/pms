package hk.org.ha.event.pms.main.vetting.mp.popup
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowMedProfileFmIndicationPopupEvent extends AbstractTideEvent
	{
		private var _fmIndicationList:ArrayCollection;
		
		public function ShowMedProfileFmIndicationPopupEvent(fmIndicationList:ArrayCollection)
		{
			super();
			_fmIndicationList = fmIndicationList;
		}
		
		public function get fmIndicationList():ArrayCollection {
			return _fmIndicationList;
		}
	}
}