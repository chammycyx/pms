package hk.org.ha.control.pms.main.alert {
	
	import hk.org.ha.event.pms.main.alert.RetrieveAlertProfileHistoryEvent;
	import hk.org.ha.event.pms.main.alert.popup.ShowAlertProfileHistoryPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.ShowLoadingPopupEvent;
	import hk.org.ha.model.pms.biz.alert.AlertProfileServiceBean;
	import hk.org.ha.model.pms.vo.alert.AlertProfileHistory;
	import hk.org.ha.model.pms.vo.alert.PatHistoryAdr;
	import hk.org.ha.model.pms.vo.alert.PatHistoryAlert;
	import hk.org.ha.model.pms.vo.alert.PatHistoryAllergy;
	
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("alertProfileServiceCtl", restrict="true")]
	public class AlertProfileServiceCtl {
		
		[In]
		public var ctx:Context;
		
		[In]
		public var alertProfileService:AlertProfileServiceBean;
		
		private var retrieveAlertProfileHistoryEvent:RetrieveAlertProfileHistoryEvent;
		
		private var patHistoryAllergy:PatHistoryAllergy;
		private var patHistoryAdr:PatHistoryAdr;
		private var patHistoryAlert:PatHistoryAlert;
		
		[Observer]
		public function retrieveAlertProfileHistory(evt:RetrieveAlertProfileHistoryEvent):void {
			if (retrieveAlertProfileHistoryEvent == null) {
				retrieveAlertProfileHistoryEvent = evt;
			}
			else {
				return;
			}
			dispatchEvent(new ShowLoadingPopupEvent("Loading Alert Profile History ..."));
			alertProfileService.retrieveAlertProfileHistory(evt.caseNum, evt.patHospCode, evt.patientEntity, retrieveAlertProfileHistoryResult);
		}
		
		private function retrieveAlertProfileHistoryResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			dispatchEvent(new ShowAlertProfileHistoryPopupEvent(evt.result as AlertProfileHistory));
			retrieveAlertProfileHistoryEvent = null;
		}
		
		private function retrieveAlertProfileHistoryFault(evt:TideFaultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
		}
	}
}
