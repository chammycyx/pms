package hk.org.ha.event.pms.main.vetting {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrieveCapdManualVoucherByManualVoucherNumEvent extends AbstractTideEvent 
	{
		private var _capdManualVoucherNum:String;
		private var _manualVoucherGridCol:Number;
		private var _callOkBtn:Boolean;
		
		public function RetrieveCapdManualVoucherByManualVoucherNumEvent(capdManualVoucherNum:String, manualVoucherGridCol:Number, callOkBtn:Boolean) {
			super();
			_capdManualVoucherNum = capdManualVoucherNum;
			_manualVoucherGridCol = manualVoucherGridCol;
			_callOkBtn = callOkBtn;
		}
		
		public function get capdManualVoucherNum():String {
			return _capdManualVoucherNum;
		}
		
		public function get manualVoucherGridCol():Number {
			return _manualVoucherGridCol;
		}
		
		public function get callOkBtn():Boolean {
			return _callOkBtn;
		}
	}
}
