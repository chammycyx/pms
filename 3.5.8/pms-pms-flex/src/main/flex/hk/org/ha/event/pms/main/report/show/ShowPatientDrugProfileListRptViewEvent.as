package hk.org.ha.event.pms.main.report.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowPatientDrugProfileListRptViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		public function ShowPatientDrugProfileListRptViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}