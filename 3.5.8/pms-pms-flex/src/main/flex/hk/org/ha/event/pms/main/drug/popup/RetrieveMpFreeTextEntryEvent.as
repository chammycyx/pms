package hk.org.ha.event.pms.main.drug.popup {
		
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveMpFreeTextEntryEvent extends AbstractTideEvent 
	{			
		private var _drugSearchSource:DrugSearchSource;		// null if event is triggered by order edit
		private var _mpFreeTextEntryPopupProp:MpFreeTextEntryPopupProp;
		
		public function RetrieveMpFreeTextEntryEvent(mpFreeTextEntryPopupProp:MpFreeTextEntryPopupProp, drugSearchSource:DrugSearchSource=null):void 
		{
			super();
			_drugSearchSource = drugSearchSource;
			_mpFreeTextEntryPopupProp = mpFreeTextEntryPopupProp;
		}
		
		public function get drugSearchSource():DrugSearchSource {
			return _drugSearchSource;
		}
		
		public function get mpFreeTextEntryPopupProp():MpFreeTextEntryPopupProp{
			return _mpFreeTextEntryPopupProp;
		}
	}
}