package hk.org.ha.event.pms.main.vetting.mp.popup {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowReplenishPopupEvent extends AbstractTideEvent 
	{						
		private var _medProfileItem:MedProfileItem;		
		private var _okHandler:Function;
		
		public function ShowReplenishPopupEvent(medProfileItem:MedProfileItem, okHandler:Function):void {
			super();
			_medProfileItem = medProfileItem;
			_okHandler 	= okHandler;
		}
		
		public function get medProfileItem():MedProfileItem {
			return _medProfileItem;
		}
		
		public function get okHandler():Function {
			return _okHandler;
		}
	}
}
