package hk.org.ha.control.pms.main.vetting {

	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	import mx.core.IFlexDisplayObject;
	import mx.core.UIComponent;
	import mx.managers.PopUpManager;
	
	import hk.org.ha.event.pms.main.alert.mds.OverridePrescAlertEvent;
	import hk.org.ha.event.pms.main.alert.mds.ShowPrescAlertErrorEvent;
	import hk.org.ha.event.pms.main.alert.mds.ShowPrescAlertMsgEvent;
	import hk.org.ha.event.pms.main.alert.mds.popup.ShowMdsLogPopupEvent;
	import hk.org.ha.event.pms.main.alert.mds.popup.ShowSteroidAlertPopupEvent;
	import hk.org.ha.event.pms.main.alert.popup.ShowAlertProfilePopupEvent;
	import hk.org.ha.event.pms.main.charging.popup.ShowSfiClearanceWarningPopupEvent;
	import hk.org.ha.event.pms.main.drug.show.ShowDrugSearchViewEvent;
	import hk.org.ha.event.pms.main.onestop.ClearKeyboardBufferEvent;
	import hk.org.ha.event.pms.main.onestop.ClearOSEOrderDataEvent;
	import hk.org.ha.event.pms.main.onestop.ShowInfomationMessageEvent;
	import hk.org.ha.event.pms.main.refill.AppendSfiRefillScheduleEvent;
	import hk.org.ha.event.pms.main.refill.RemoveSfiRefillOrderEvent;
	import hk.org.ha.event.pms.main.refill.StartRefillEvent;
	import hk.org.ha.event.pms.main.refill.UpdateSfiRefillCouponNumTxtEvent;
	import hk.org.ha.event.pms.main.show.ShowStartupViewEvent;
	import hk.org.ha.event.pms.main.vetting.AddItemFromDrugSearchEvent;
	import hk.org.ha.event.pms.main.vetting.CancelEditItemEvent;
	import hk.org.ha.event.pms.main.vetting.CancelOrderEvent;
	import hk.org.ha.event.pms.main.vetting.CancelVettingEvent;
	import hk.org.ha.event.pms.main.vetting.EnableVettingByBarcodeEvent;
	import hk.org.ha.event.pms.main.vetting.EnableVettingEvent;
	import hk.org.ha.event.pms.main.vetting.EndVettingEvent;
	import hk.org.ha.event.pms.main.vetting.OrderEditClearCacheEvent;
	import hk.org.ha.event.pms.main.vetting.RemoveOrderEvent;
	import hk.org.ha.event.pms.main.vetting.RemoveOrderItemEvent;
	import hk.org.ha.event.pms.main.vetting.RetrievePatientByHkidEvent;
	import hk.org.ha.event.pms.main.vetting.SfiPrescReverseSuspendForEndVettingEvent;
	import hk.org.ha.event.pms.main.vetting.ShowMdsMsgEvent;
	import hk.org.ha.event.pms.main.vetting.ShowRetrieveOrderMsgEvent;
	import hk.org.ha.event.pms.main.vetting.StartVettingEvent;
	import hk.org.ha.event.pms.main.vetting.UpdateOrderFromOrderViewEvent;
	import hk.org.ha.event.pms.main.vetting.UpdatePharmRemarkEvent;
	import hk.org.ha.event.pms.main.vetting.UsePatientFromHkpmiEvent;
	import hk.org.ha.event.pms.main.vetting.popup.ShowCancelVettingPopupEvent;
	import hk.org.ha.event.pms.main.vetting.popup.ShowDiscontinueItemPopupEvent;
	import hk.org.ha.event.pms.main.vetting.popup.ShowLocalDuplicateItemEvent;
	import hk.org.ha.event.pms.main.vetting.show.ShowOrderEditCapdItemViewEvent;
	import hk.org.ha.event.pms.main.vetting.show.ShowOrderEditViewFromDrugSearchEvent;
	import hk.org.ha.event.pms.main.vetting.show.ShowOrderEntryViewEvent;
	import hk.org.ha.event.pms.main.vetting.show.ShowOrderViewEvent;
	import hk.org.ha.fmk.pms.flex.components.core.MenuItem;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.ShowLoadingPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	import hk.org.ha.fmk.pms.flex.components.window.Window;
	import hk.org.ha.fmk.pms.flex.components.window.WindowCloseEvent;
	import hk.org.ha.fmk.pms.flex.components.window.WindowSwitchEvent;
	import hk.org.ha.model.pms.biz.refill.SfiRefillScheduleAppendServiceBean;
	import hk.org.ha.model.pms.biz.vetting.OrderViewServiceBean;
	import hk.org.ha.model.pms.persistence.disp.MedOrder;
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	import hk.org.ha.model.pms.persistence.disp.PharmOrder;
	import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
	import hk.org.ha.model.pms.udt.alert.AlertProfileStatus;
	import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
	import hk.org.ha.model.pms.vo.alert.AlertProfile;
	import hk.org.ha.model.pms.vo.charging.MoiCapdChargeInfo;
	import hk.org.ha.model.pms.vo.charging.MoiChargeInfo;
	import hk.org.ha.model.pms.vo.ehr.EhrPatient;
	import hk.org.ha.model.pms.vo.rx.CapdItem;
	import hk.org.ha.model.pms.vo.security.PropMap;
	import hk.org.ha.model.pms.vo.sys.SysMsgMap;
	import hk.org.ha.model.pms.vo.vetting.MedOrderItemInfo;
	import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;
	import hk.org.ha.model.pms.vo.vetting.OrderViewUtil;
	import hk.org.ha.model.pms.vo.vetting.PharmOrderItemInfo;
	import hk.org.ha.model.pms.vo.vetting.RetrievePatientResult;
	import hk.org.ha.view.pms.main.vetting.OrderEditView;
	
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("OrderViewServiceCtl", restrict="true")]
	public class OrderViewServiceCtl {

		private static const CLOSE_ON_LOGOFF:String = "closeOnLogOff";
		
		[In]
		public var ctx:Context;
		
		[In]
		public var orderViewService:OrderViewServiceBean;
		
		[In]
		public var sfiRefillScheduleAppendService:SfiRefillScheduleAppendServiceBean;
		
		[In]
		public var pharmOrder:PharmOrder;
		
		[In]
		public var drugSearchMedOrderItem:MedOrderItem;
		
		[In]
		public var orderViewInfo:OrderViewInfo;
		
		[In]
		public var propMap:PropMap;
		
		[In]
		public var localDuplicateItemList:ArrayCollection;
		
		[In]
		public var prescAlertMsgList:ArrayCollection;
		
		[In]
		public var steroidAlertList:ArrayCollection;
		
		[In]
		public var orderEditView:OrderEditView;
		
		[In]
		public var moiChargeInfoList:ArrayCollection;
		
		private var dummyMoiChargeInfo:MoiChargeInfo;
		
		private var dummyMoiCapdChargeInfo:MoiCapdChargeInfo;
		
		[In]
		public var sysMsgMap:SysMsgMap;
		
		[In]
		public var window:Window;
		
		private var updateOrderFromOrderViewEvent:UpdateOrderFromOrderViewEvent;
		
		private var removeOrderItemCallbackEvent:Event;
		private var removeOrderItemCallbackFunc:Function;
		private var updatePharmRemarkCallbackEvent:Event;
		private var lastEndVettingEvent:EndVettingEvent;
		private var retrievePatientByHkidEvent:RetrievePatientByHkidEvent;
		private var usePatientFromHkpmiEvent:UsePatientFromHkpmiEvent;
		private var cancelOrderEvent:CancelOrderEvent;
		private var ehrPatient:EhrPatient;
		
		[Observer]
		public function startVetting(evt:StartVettingEvent):void {
			ehrPatient = evt.ehrPatient;
			dispatchEvent(new ShowLoadingPopupEvent("Loading Order Information ..."));
			orderViewService.retrieveOrder(evt.id, evt.oneStopOrderType, evt.requiredLockOrder, retrieveOrderResult, retrieveOrderFault);
		}
		
		private function retrieveOrderResult(evt:TideResultEvent):void {
			dispatchEvent(new CloseLoadingPopupEvent());
			dispatchEvent(new OrderEditClearCacheEvent);
			showViewByOneStopOrderType(orderViewInfo.alertProfile, orderViewInfo.oneStopOrderType);
		}
		
		[Observer]
		public function showRetrieveOrderMsg(evt:ShowRetrieveOrderMsgEvent):void {

			showOrderViewMsgForRetrieve(function():void {

				showDiscontinueMessage(function():void {

					showAlertProfile(orderViewInfo.alertProfile, function():void {
						
						showDrsMsgCodeList(function():void {
							if (evt.callback != null) {
								evt.callback();
							}
	
						});
					
					});

				});

			});
		}
		
		[Observer]
		public function showMdsMsg(evt:ShowMdsMsgEvent):void {

			dispatchEvent(new ShowMdsLogPopupEvent(function():void {
				
				// show mds alert error
				dispatchEvent(new ShowPrescAlertErrorEvent(true, function():void {
					
					// show mds alert msg
					dispatchEvent(new ShowPrescAlertMsgEvent(true, function():void{
						
						dispatchEvent(new ShowSteroidAlertPopupEvent(steroidAlertList, function():void {
							
							if (evt.callback != null) {
								evt.callback();
							}
						}, pharmOrder.medOrder.docType == MedOrderDocType.Normal && prescAlertMsgList != null && prescAlertMsgList.length > 0));
					}));
				}));
			}));
		}
		
		private function showViewByOneStopOrderType(alertProfile:AlertProfile, oneStopOrderType:OneStopOrderType):void {

			var nextViewEvent:Event;
			var showMsgFlag:Boolean = false;
			var clearOSEData:Boolean = true;

			switch (oneStopOrderType) {

				case OneStopOrderType.MedOrder: // show vetting list for moe order
				case OneStopOrderType.DispOrder:
				case OneStopOrderType.DhOrder:
					nextViewEvent = new ShowOrderViewEvent(null, OrderViewUtil.retrieveAlertProfileMsg(alertProfile, sysMsgMap, propMap));
					showMsgFlag = true;
					break;

				case OneStopOrderType.SfiOrder:
					nextViewEvent = new ShowOrderViewEvent();
					showMsgFlag = true;
					break;

				case OneStopOrderType.ManualOrder: // show drug search for manual order
					nextViewEvent = constructDrugSearchViewEvent(OrderViewUtil.retrieveAlertProfileMsg(alertProfile, sysMsgMap, propMap));
					showMsgFlag = true;
					break;

				case OneStopOrderType.RefillOrder: // show refill from oneStop
					if (orderViewInfo.errorMsgCodeList.length > 0) {
						showMsgFlag = true;
					}
					else {
						nextViewEvent = new StartRefillEvent("onestop", orderViewInfo.refillScheduleId);
					}
					break;

				case OneStopOrderType.RefillOrderReadOnly: // show refill from check & issue
					nextViewEvent = new StartRefillEvent("checkIssue", orderViewInfo.refillScheduleId, null, ehrPatient);
					clearOSEData = false;
					break;
			}

			if (showMsgFlag) {
				dispatchEvent(new ShowRetrieveOrderMsgEvent(function():void {
					initOrderEntryView(nextViewEvent, clearOSEData);
				}));
			}
			else {
				initOrderEntryView(nextViewEvent, clearOSEData);
			}
		}
		
		private function initOrderEntryView(evt:Event, clearOSEData:Boolean):void {
			dispatchEvent(evt);
			if( clearOSEData ){
				dispatchEvent(new ClearOSEOrderDataEvent());
			}
			var menuItem:MenuItem = new MenuItem("Order Entry", ShowOrderEntryViewEvent);
			menuItem.properties[CLOSE_ON_LOGOFF] = true;
			dispatchEvent(new WindowSwitchEvent(menuItem, null, false));
		}
		
		private function showAlertProfile(alertProfile:AlertProfile, callbackFunc:Function):void {
			
			var postShowAlertProfileError:Function = function():void {
				if (alertProfile != null && alertProfile.status == AlertProfileStatus.RecordExist && propMap.getValueAsBoolean("vetting.alert.profile.popup")) {
					var caseNum:String = null;
					if (pharmOrder.medCase != null) {
						caseNum = pharmOrder.medCase.caseNum;
					}
					dispatchEvent(new ShowAlertProfilePopupEvent(alertProfile, caseNum, pharmOrder.medOrder.patHospCode, pharmOrder.getPatient(), callbackFunc));
				}
				else {
					if (callbackFunc != null) {
						callbackFunc();
					}
				}
			}
			
			if (alertProfile != null && alertProfile.message != null) {
				showAlertProfileError(alertProfile, postShowAlertProfileError);
			}
			else {
				postShowAlertProfileError();
			}
		}
		
		private function showDrsMsgCodeList(callback:Function):void{
			if( orderViewInfo.drsMsgCodeList.length > 0 ){
				showDrsMsg(orderViewInfo.drsMsgCodeList, 0, callback);
			}else{
				if( callback != null ){
					callback();
				}
			}
		}
		
		private function retrieveOrderFault(evt:TideFaultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
		}
		
		[Observer]
		public function endVetting(evt:EndVettingEvent):void {

			lastEndVettingEvent = evt;

			dispatchEvent(new ShowLoadingPopupEvent("Saving Order Information ..."));

			dispatchEvent(new EnableVettingEvent(false));
			dispatchEvent(new EnableVettingByBarcodeEvent(false));
			orderViewInfo.saveSuspendCode = evt.suspendCode;
			orderViewInfo.skipDuplicateCheckFlag = evt.overrideLocalDuplicateCheck;
			orderViewInfo.clearInfoMsg();
			orderViewInfo.clearErrorMsg();

			orderViewService.saveOrder(orderViewInfo, saveOrderResult, saveOrderFault);
		}
		
		private function saveOrderResult(evt:TideResultEvent):void {

			dispatchEvent(new EnableVettingEvent(true));
			dispatchEvent(new CloseLoadingPopupEvent());
			
			// duplicate item exist
			if (localDuplicateItemList != null && localDuplicateItemList.length > 0) {
				dispatchEvent(new ShowLocalDuplicateItemEvent(function():void {
					lastEndVettingEvent = lastEndVettingEvent.clone() as EndVettingEvent;
					lastEndVettingEvent.overrideLocalDuplicateCheck = true;
					dispatchEvent(lastEndVettingEvent);
				}));
				return;
			}
			
			if( !isNaN(orderViewInfo.dispOrderId) ){
				evt.context.dispatchEvent(new ShowSfiClearanceWarningPopupEvent(orderViewInfo.sfiInvoiceClearanceStatus,"saveRx", orderViewInfo.dispOrderId));
				return;
			}

			showOrderViewMsgForSave(function():void {

				dispatchEvent(new ShowMdsMsgEvent(function():void {

					// no mds alert
					if (prescAlertMsgList == null || prescAlertMsgList.length == 0) {
						if (steroidAlertList == null || steroidAlertList.length == 0) {
							showOneStopEntry();
							dispatchEvent(new EnableVettingByBarcodeEvent(true));
						}
						else {
							orderViewInfo.alertOverrideFlag = true;
							dispatchEvent(lastEndVettingEvent.clone());
						}
					}
					// has mds alert
					else if (allowOverride()) {
						dispatchEvent(new OverridePrescAlertEvent(function():void{
							if (pharmOrder.medOrder.medOrderItemList.length == 0) {
								dispatchEvent(constructDrugSearchViewEvent());
							}
							else {
								orderViewInfo.alertOverrideFlag = true;
								dispatchEvent(lastEndVettingEvent.clone());
							}
						}));
					}
					
				}));
			});
		}
		
		private function saveOrderFault(evt:TideFaultEvent):void {
			dispatchEvent(new EnableVettingEvent(true));
			dispatchEvent(new ClearKeyboardBufferEvent());
			dispatchEvent(new EnableVettingByBarcodeEvent(true));
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
		}

		private function allowOverride():Boolean {
			return (orderViewInfo.oneStopOrderType == OneStopOrderType.ManualOrder) ||
				   (orderViewInfo.oneStopOrderType == OneStopOrderType.DhOrder) ||
				   (orderViewInfo.oneStopOrderType == OneStopOrderType.DispOrder && pharmOrder.medOrder.docType == MedOrderDocType.Manual);
		}
		
		[Observer]
		public function cancelVetting(evt:CancelVettingEvent):void {

			var callback:Function = function():void {
				if (evt.callback != null) {
					evt.callback();
				}
				showOneStopEntry();
			};
			
			if ( orderEditView != null ) {
				orderEditView.carryDuration = NaN;
				orderEditView.carryDurationUnit = null;
			}
			
			if (!orderViewInfo.allowEditFlag) {
				callback();
				return;
			}

			var allowUnsuspendFlag:Boolean = orderViewInfo.autoSuspendFlag || !sfiExist();
			
			switch (orderViewInfo.oneStopOrderType) {
				case OneStopOrderType.MedOrder:
				case OneStopOrderType.DhOrder:
					confirmCancelVetting(function():void {
						dispatchEvent(new CancelOrderEvent(false, null, callback));
					});
					break;

				case OneStopOrderType.ManualOrder:
					confirmCancelVetting(callback);
					break;

				case OneStopOrderType.DispOrder:
					dispatchEvent(new ShowCancelVettingPopupEvent(allowUnsuspendFlag, callback));
					break;

				case OneStopOrderType.SfiOrder:
					if (orderViewInfo.dispSfiRefillFlag) {
						dispatchEvent(new ShowCancelVettingPopupEvent(allowUnsuspendFlag, callback));
					}
					else {
						confirmCancelVetting(function():void {
							dispatchEvent(new CancelOrderEvent(false, null, callback));
						});
					}
					break;
			}
		}
		
		private function showOneStopEntry(evt:TideResultEvent=null):void {			
			window.closeWindowHandler(new WindowCloseEvent(WindowCloseEvent.CLOSE, ShowStartupViewEvent));
		}
		
		private function confirmCancelVetting(callback:Function):void {
			var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp("0113");
			msgProp.setYesNoButton = true;
			msgProp.setYesDefaultButton = false;
			msgProp.yesHandler = function(evt:MouseEvent):void {
				callback();
				PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
			};
			dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
		}
		
		private function sfiExist():Boolean {
			for each (var moi:MedOrderItem in pharmOrder.medOrder.medOrderItemList) {
				if (moi.isSfi()) {
					return true;
				}
			}
			return false;
		}
		
		[Observer]
		public function cancelOrder(evt:CancelOrderEvent):void {
			cancelOrderEvent = evt;
			orderViewService.cancelOrder(evt.suspendFlag, evt.suspendCode, cancelOrderResult);
		}
		
		private function cancelOrderResult(evt:TideResultEvent):void {
			if (cancelOrderEvent.callback != null) {
				cancelOrderEvent.callback();
			}
		}
		
		[Observer]
		public function removeOrder(evt:RemoveOrderEvent):void {
			orderViewService.removeOrder(evt.logonUser, evt.remarkText, showOneStopEntry);
		}
		
		[Observer]
		public function removeSfiRefillOrder(evt:RemoveSfiRefillOrderEvent):void {
			orderViewService.removeSfiRefillOrder(orderViewInfo.refillScheduleId, removeSfiRefillOrderResult);
		}
		
		private function removeSfiRefillOrderResult(evt:TideResultEvent):void {
			showOrderViewMsgForSfiOrderDelete(showOneStopEntry);
		}
		
		[Observer]
		public function addItemFromDrugSearch(event:AddItemFromDrugSearchEvent):void {

			var medOrder:MedOrder = pharmOrder.medOrder;
			
			var itemIndex:int;

			if (isNaN(event.index) || event.index >= medOrder.medOrderItemList.length) { // add item
				itemIndex = medOrder.medOrderItemList.length;
				medOrder.medOrderItemList.addItem(drugSearchMedOrderItem);
				drugSearchMedOrderItem.orgItemNum = drugSearchMedOrderItem.itemNum;
			}
			else { // replace item
				itemIndex = event.index;

				drugSearchMedOrderItem.id = (medOrder.medOrderItemList.getItemAt(itemIndex) as MedOrderItem).id;
				drugSearchMedOrderItem.orgItemNum = (medOrder.medOrderItemList.getItemAt(itemIndex) as MedOrderItem).orgItemNum;

				// carry forward prevMoItemId of previous item (for checking whether the remark is item add remark or item update remark)
				drugSearchMedOrderItem.prevMoItemId = (medOrder.medOrderItemList.getItemAt(itemIndex) as MedOrderItem).prevMoItemId;
				
				// carry forward remark status of previous item (for checking of fallback item update remark)
				drugSearchMedOrderItem.remarkCreateDate = (medOrder.medOrderItemList.getItemAt(itemIndex) as MedOrderItem).remarkCreateDate;	
				drugSearchMedOrderItem.remarkCreateUser = (medOrder.medOrderItemList.getItemAt(itemIndex) as MedOrderItem).remarkCreateUser;	
				drugSearchMedOrderItem.remarkText = (medOrder.medOrderItemList.getItemAt(itemIndex) as MedOrderItem).remarkText;	
				drugSearchMedOrderItem.remarkConfirmDate = (medOrder.medOrderItemList.getItemAt(itemIndex) as MedOrderItem).remarkConfirmDate;	
				drugSearchMedOrderItem.remarkConfirmUser = (medOrder.medOrderItemList.getItemAt(itemIndex) as MedOrderItem).remarkConfirmUser;	

				medOrder.medOrderItemList.removeItemAt(itemIndex);
				medOrder.medOrderItemList.addItemAt(drugSearchMedOrderItem, itemIndex);
			}
			
			// link MedOrder
			drugSearchMedOrderItem.medOrder = medOrder;
			
			// link PharmOrder
			for each (var pharmOrderItem:PharmOrderItem in drugSearchMedOrderItem.pharmOrderItemList) {
				pharmOrderItem.pharmOrder = pharmOrder;
				pharmOrder.pharmOrderItemList.addItem(pharmOrderItem);
			}

			if (drugSearchMedOrderItem.capdRxDrug == null) {				
				dispatchEvent(new ShowOrderEditViewFromDrugSearchEvent(itemIndex, event.autoAddDrug, event.drugScope));
			}
			else {
				dispatchEvent(new ShowOrderEditCapdItemViewEvent(itemIndex, false));
			}
		}
		
		[Observer]
		public function removeOrderItem(event:RemoveOrderItemEvent):void {
			removeOrderItemCallbackEvent = event.callbackEvent;
			removeOrderItemCallbackFunc = event.callbackFunc;
			orderViewService.removeMedOrderItem(orderViewInfo, event.itemIndex, event.remarkCreateDate, event.remarkCreateUser, event.remarkText, removeOrderItemResult);
		}

		private function removeOrderItemResult(evt:TideResultEvent):void {
			if( moiChargeInfoList != null && moiChargeInfoList.length > 0 ){
				var medOrder:MedOrder = pharmOrder.medOrder;
				for each ( var moiChargeInfo:MoiChargeInfo in moiChargeInfoList ) {
					var updateMoi:MedOrderItem = MedOrderItem(medOrder.medOrderItemList.getItemAt(moiChargeInfo.medOrderItemIndex));
					updateMoi.cmsChargeFlag = moiChargeInfo.cmsChargeFlag;
					updateMoi.chargeFlag = moiChargeInfo.chargeFlag;
					updateMoi.unitOfCharge = moiChargeInfo.unitOfCharge;
					
					if( moiChargeInfo.moiCapdChargeInfoList != null && moiChargeInfo.moiCapdChargeInfoList.length > 0 ){
						for each( var moiCapdChargeInfo:MoiCapdChargeInfo in moiChargeInfo.moiCapdChargeInfoList ){
							var selectedCapdItem:CapdItem = CapdItem(updateMoi.capdRxDrug.capdItemList.getItemAt( moiCapdChargeInfo.medOrderCapdItemIndex ));
							selectedCapdItem.unitOfCharge = moiCapdChargeInfo.unitOfCharge;
						}
					}
				}
			}
			
			if (removeOrderItemCallbackEvent != null) {
				dispatchEvent(removeOrderItemCallbackEvent);
			}
			
			if (removeOrderItemCallbackFunc != null) {
				removeOrderItemCallbackFunc();
			}
		}
		
		[Observer]
		public function updateOrderViewInfo(evt:UpdateOrderFromOrderViewEvent):void {
			
			updateOrderFromOrderViewEvent = evt;

			orderViewInfo.medOrderItemInfoList = new ArrayCollection();
			orderViewInfo.pharmOrderItemInfoList = new ArrayCollection();
			
			for each (var medOrderItem:MedOrderItem in pharmOrder.medOrder.medOrderItemList) {
				orderViewInfo.medOrderItemInfoList.addItem(new MedOrderItemInfo(medOrderItem));
				for each (var pharmOrderItem:PharmOrderItem in medOrderItem.pharmOrderItemList) {
					orderViewInfo.pharmOrderItemInfoList.addItem(new PharmOrderItemInfo(pharmOrderItem));
				}
			}
			
			orderViewService.updateOrderFromOrderView(orderViewInfo, updateOrderViewInfoResult);
		}
		
		private function updateOrderViewInfoResult(evt:TideResultEvent):void {
			if (updateOrderFromOrderViewEvent != null && updateOrderFromOrderViewEvent.callbackFunc != null) {
				updateOrderFromOrderViewEvent.callbackFunc();
			}
		}
		
		[Observer]
		public function cancelEditItem(evt:CancelEditItemEvent):void {

			var medOrder:MedOrder = pharmOrder.medOrder;

			if (medOrder.medOrderItemList.length == 0 && evt.showOneStopFlag) {
				showOneStopEntry();
			}
			else {
				var showOrderViewEvent:ShowOrderViewEvent = new ShowOrderViewEvent();
				showOrderViewEvent.selectedIndex = evt.selectedIndex;
				dispatchEvent(showOrderViewEvent);
			}
		}
		
		[Observer]
		public function updatePharmRemark(event:UpdatePharmRemarkEvent):void {
			updatePharmRemarkCallbackEvent = event.callbackEvent;
			orderViewService.updatePharmRemark(orderViewInfo, event.itemIndex, event.remarkCreateDate, event.remarkCreateUser, event.remarkText, updatePharmRemarkResult);
		}
		
		private function updatePharmRemarkResult(evt:TideResultEvent):void {
			dispatchEvent(updatePharmRemarkCallbackEvent);
		}
		
		private function constructDrugSearchViewEvent(infoString:String=null):ShowDrugSearchViewEvent {
			var showDrugSearchViewEvent:ShowDrugSearchViewEvent = new ShowDrugSearchViewEvent();
			showDrugSearchViewEvent.drugSearchSource = DrugSearchSource.Vetting;
			showDrugSearchViewEvent.okCallbackEvent = new AddItemFromDrugSearchEvent();
			if (pharmOrder.medOrder.docType == MedOrderDocType.Normal) {
				showDrugSearchViewEvent.cancelCallbackEvent = new CancelVettingEvent();
			} else {
				showDrugSearchViewEvent.cancelCallbackEvent = null;
			}
			showDrugSearchViewEvent.firstItem = true;
			showDrugSearchViewEvent.enableChest = true;
			
			showDrugSearchViewEvent.properties = new Dictionary();
			showDrugSearchViewEvent.properties["infoString"] = infoString;

			return showDrugSearchViewEvent;
		}
		
		[Observer]
		public function appendRefillScheduleItem(evt:AppendSfiRefillScheduleEvent):void{
			In(Object(sfiRefillScheduleAppendService).errMsg)
			In(Object(sfiRefillScheduleAppendService).useMoItemNumFlag)
			sfiRefillScheduleAppendService.appendSfiRefillSchedule(orderViewInfo, appendRefillScheduleItemResult);
		}
		
		private function appendRefillScheduleItemResult(evt:TideResultEvent):void {	
			
			var infoDashboardMsg:String = null;
			var errDashboardMsg:String = null;
			var selectedIndex:Number = NaN;
			
			var errMsg:String = Object(sfiRefillScheduleAppendService).errMsg; 
			if( errMsg != "" && errMsg.length == 4 ){
				if( errMsg == "0187" ){
					errDashboardMsg = sysMsgMap.getMessage(errMsg);
				}else{
					infoDashboardMsg = sysMsgMap.getMessage(errMsg);
				}
			}
			else if( orderViewInfo.sfiRefillCouponNumErrorString != "") {
				dispatchEvent(new UpdateSfiRefillCouponNumTxtEvent());
			}

			if (evt.result != null) {
				var orgItemNum:Number = evt.result as Number;
				var useMoItemNumFlag:Boolean = Object(sfiRefillScheduleAppendService).useMoItemNumFlag;
				
				if (orgItemNum == 0) {
					selectedIndex = 0;
				}
				else {
					if( useMoItemNumFlag != "" && useMoItemNumFlag ){
						selectedIndex = pharmOrder.medOrder.medOrderItemList.getItemIndex(pharmOrder.medOrder.getMedOrderItemByItemNum(orgItemNum));
					}else{
						selectedIndex = pharmOrder.medOrder.medOrderItemList.getItemIndex(pharmOrder.medOrder.getMedOrderItemByOrgItemNum(orgItemNum));
					}
				}
			}
			
			if (errDashboardMsg != null || infoDashboardMsg != null || !isNaN(selectedIndex)) {
				var showOrderViewEvent:ShowOrderViewEvent = new ShowOrderViewEvent();
				showOrderViewEvent.vetOrderLine = selectedIndex;
				if( errDashboardMsg != null ){
					showOrderViewEvent.errorString = errDashboardMsg;
				}else if( infoDashboardMsg != null ){
					showOrderViewEvent.infoString = infoDashboardMsg;
				}
				dispatchEvent(showOrderViewEvent);
			}
			
			showDiscontinueMessage();
		}
		
		[Observer]
		public function sfiPrescriptionDeferReverseSuspendProceed(evt:SfiPrescReverseSuspendForEndVettingEvent):void {						
			orderViewInfo.dispOrderId = NaN;
			orderViewInfo.skipSfiClearanceFlag = true;
			
			dispatchEvent(new UpdateOrderFromOrderViewEvent(function():void {					
				dispatchEvent(lastEndVettingEvent.clone());		
			}));
		}
		
		private function showOrderViewMsgForRetrieve(callbackFunc:Function):void {
			if (orderViewInfo.errorMsgCodeList.length > 0) {
				showMsgInOneStop(orderViewInfo.errorMsgCodeList, orderViewInfo.errorMsgParamList);
			}
			else if (orderViewInfo.infoMsgCodeList.length > 0) {
				showMsgList(orderViewInfo.infoMsgCodeList, orderViewInfo.infoMsgParamList, 0, callbackFunc);
			}
			else {
				callbackFunc();
			}
		}
		
		private function showDiscontinueMessage(callbackFunc:Function=null):void {
			if (orderViewInfo.discontinueMessageTlf.length > 0) {
				var itemNumArray:Array = orderViewInfo.discontinueMessageTlf.keySet.toArray().sort(Array.NUMERIC);
				var tlfList:ListCollectionView = new ArrayCollection();
				for (var i:int=0; i<itemNumArray.length; i++) {
					tlfList.addItem(orderViewInfo.discontinueMessageTlf.get(itemNumArray[i]));
				}
				dispatchEvent(new ShowDiscontinueItemPopupEvent(tlfList, callbackFunc));
			}
			else if (callbackFunc != null) {
				callbackFunc();
			}
		}
		
		private function showOrderViewMsgForSfiOrderDelete(callbackFunc:Function):void {
			if (orderViewInfo.errorMsgCodeList.length > 0) {
				showErrorDashboard(orderViewInfo.errorMsgCodeList, orderViewInfo.errorMsgParamList);
				dispatchEvent(new ClearKeyboardBufferEvent());
				dispatchEvent(new EnableVettingByBarcodeEvent(true));
			}else{
				callbackFunc();
			}
		}
		
		private function showOrderViewMsgForSave(callbackFunc:Function):void {
			if (orderViewInfo.errorMsgCodeList.length > 0) {
				showErrorDashboard(orderViewInfo.errorMsgCodeList, orderViewInfo.errorMsgParamList);
				dispatchEvent(new ClearKeyboardBufferEvent());
				dispatchEvent(new EnableVettingByBarcodeEvent(true));
			}
			else if (orderViewInfo.infoMsgCodeList.length > 0) {
				showMsgList(orderViewInfo.infoMsgCodeList, orderViewInfo.infoMsgParamList, 0, callbackFunc);
			}
			else {
				callbackFunc();
			}
		}
		
		private function showDrsMsg(msgCodeList:ListCollectionView, index:int, callbackFunc:Function=null):void {
			
			if (msgCodeList.length <= index) {
				if (callbackFunc != null) {
					callbackFunc();
				}
				return;
			}
			
			var msgCode:String = msgCodeList.getItemAt(index) as String;
			
			var sysMsgProp:SystemMessagePopupProp = new SystemMessagePopupProp(msgCode);
			if( msgCode == "1056" || msgCode == "1057" ){
				sysMsgProp.setYesNoButton = true;
				sysMsgProp.setYesDefaultButton = true;
				
				sysMsgProp.yesHandler = function(mouseEvent:MouseEvent):void {
					showDrsMsg(msgCodeList, index + 1, callbackFunc);
					dispatchEvent(new ClearKeyboardBufferEvent());
					dispatchEvent(new EnableVettingByBarcodeEvent(true));
					PopUpManager.removePopUp(((mouseEvent.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
				};
				
				sysMsgProp.noHandler = function(mouseEvent:MouseEvent):void {
					orderViewInfo.drsFlag = false;
					showDrsMsg(msgCodeList, index + 1, callbackFunc);
					dispatchEvent(new ClearKeyboardBufferEvent());
					dispatchEvent(new EnableVettingByBarcodeEvent(true));
					PopUpManager.removePopUp(((mouseEvent.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
				};
				
			}else{
				sysMsgProp.setOkButtonOnly = true;
				
				sysMsgProp.okHandler = function(mouseEvent:MouseEvent):void {
					showDrsMsg(msgCodeList, index + 1, callbackFunc);
					dispatchEvent(new ClearKeyboardBufferEvent());
					dispatchEvent(new EnableVettingByBarcodeEvent(true));
					PopUpManager.removePopUp(((mouseEvent.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
				};
			}
			
			dispatchEvent(new RetrieveSystemMessageEvent(sysMsgProp));
		}
		
		private function showMsgList(msgCodeList:ListCollectionView, msgParamList:ListCollectionView, index:int, callbackFunc:Function=null):void {
			
			if (msgCodeList.length <= index) {
				if (callbackFunc != null) {
					callbackFunc();
				}
				return;
			}
			
			var msgCode:String = msgCodeList.getItemAt(index)  as String;
			var msgParam:Array = msgParamList.length > index ? msgParamList.getItemAt(index) as Array : null;
			
			var sysMsgProp:SystemMessagePopupProp = new SystemMessagePopupProp(msgCode);
			sysMsgProp.setOkButtonOnly = true;
			
			if (msgParam != null && msgParam.length > 0) {
				sysMsgProp.messageParams = msgParam;
			}
			
			sysMsgProp.okHandler = function(mouseEvent:MouseEvent):void {
				showMsgList(msgCodeList, msgParamList, index + 1, callbackFunc);
				dispatchEvent(new ClearKeyboardBufferEvent());
				dispatchEvent(new EnableVettingByBarcodeEvent(true));
				PopUpManager.removePopUp(((mouseEvent.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
			};

			dispatchEvent(new RetrieveSystemMessageEvent(sysMsgProp));
		}
		
		private function showErrorDashboard(msgCodeList:ListCollectionView, msgParamList:ListCollectionView):void {
			var evt:ShowOrderViewEvent = new ShowOrderViewEvent();
			evt.errorString = sysMsgMap.getMessage(
				msgCodeList.getItemAt(0) as String,
				msgParamList.getItemAt(0) as Array
			);
			dispatchEvent(evt);
		}
		
		private function showMsgInOneStop(msgCodeList:ListCollectionView, msgParamList:ListCollectionView):void {
			if (msgParamList == null || msgParamList.length == 0) {
				dispatchEvent(new ShowInfomationMessageEvent([msgCodeList.getItemAt(0)], true));
			}
			else {
				dispatchEvent(new ShowInfomationMessageEvent([msgCodeList.getItemAt(0), msgParamList.getItemAt(0)], true));
			}
		}
		
		[Observer]
		public function retrievePatientByHkid(evt:RetrievePatientByHkidEvent):void {
			retrievePatientByHkidEvent = evt;
			dispatchEvent(new ShowLoadingPopupEvent("Loading PAS information..."));
			orderViewService.retrievePatientByHkid(evt.hkid, retrievePatientByHkidResult);
		}
		
		private function retrievePatientByHkidResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			var result:RetrievePatientResult = evt.result as RetrievePatientResult;
			if (result.successFlag) {
				retrievePatientByHkidEvent.callback(result.patient);
			}
			else {
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp("0281");
				msgProp.setOkButtonOnly = true;
				msgProp.okHandler = function(evt:MouseEvent):void {
					PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
					retrievePatientByHkidEvent.callback(null);
				};
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
		}
		
		[Observer]
		public function usePatientFromHkpmi(evt:UsePatientFromHkpmiEvent):void {
			pharmOrder.updateByPatientEntity(evt.patient);
			usePatientFromHkpmiEvent = evt;
			orderViewService.usePatientFromHkpmi(evt.patient, usePatientFromHkpmiResult);
		}
		
		private function usePatientFromHkpmiResult(evt:TideResultEvent):void {
			showMsgList(evt.result as ListCollectionView, new ArrayCollection(), 0, function():void {
				showAlertProfile(orderViewInfo.alertProfile, usePatientFromHkpmiEvent.callback);
			});
		}
		
		private function showAlertProfileError(alertProfile:AlertProfile, callback:Function):void {
			var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp("0247");
			msgProp.functionId = "CMS-0001";
			msgProp.messageParams = new Array(alertProfile.message);
			msgProp.setOkButtonOnly = true;
			msgProp.okHandler = function(evt:MouseEvent):void {
				PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
				if (callback != null) {
					callback();
				}
			};
			dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
		}
	}
}
