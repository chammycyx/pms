package hk.org.ha.event.pms.main.reftable.rdr
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ReceivePendReasonRdrListEvent extends AbstractTideEvent
	{
		public function ReceivePendReasonRdrListEvent()
		{
			super();
		}
	}
}