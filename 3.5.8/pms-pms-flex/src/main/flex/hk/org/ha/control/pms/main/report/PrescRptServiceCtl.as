package hk.org.ha.control.pms.main.report {
	import hk.org.ha.event.pms.main.report.PrintPrescRptEvent;
	import hk.org.ha.event.pms.main.report.RefreshPrescRptEvent;
	import hk.org.ha.event.pms.main.report.RetrievePrescRptEvent;
	import hk.org.ha.model.pms.biz.report.PrescRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	
	[Bindable]
	[Name("prescRptServiceCtl", restrict="true")]
	public class PrescRptServiceCtl 
	{
		[In]
		public var prescRptService:PrescRptServiceBean;
		
		[Observer]
		public function retrievePrescRpt(evt:RetrievePrescRptEvent):void
		{
			In(Object(prescRptService).retrieveSuccess)
			In(Object(prescRptService).validHkid)
			prescRptService.retrievePrescRptList(evt.rptDate, evt.ticketNum, evt.caseNum, evt.hkid, evt.itemCode, evt.nameTxt, retrievePrescRptResult);
		}
		
		private function retrievePrescRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshPrescRptEvent((Object(prescRptService).retrieveSuccess), (Object(prescRptService).validHkid)) );
		}
		
		[Observer]
		public function printPrescNumRpt(evt:PrintPrescRptEvent):void
		{
			prescRptService.printPrescRpt();
		}
		
	}
}
