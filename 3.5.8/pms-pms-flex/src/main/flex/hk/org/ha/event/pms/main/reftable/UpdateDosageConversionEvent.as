package hk.org.ha.event.pms.main.reftable
{
	import hk.org.ha.model.pms.dms.persistence.PmsDosageConversion;
	import hk.org.ha.model.pms.dms.persistence.PmsDosageConversionSet;
	
	import mx.controls.List;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class UpdateDosageConversionEvent extends AbstractTideEvent
	{
		private var _pmsDosageConversionSet:PmsDosageConversionSet;
		
		public function UpdateDosageConversionEvent(pmsDosageConversionSet:PmsDosageConversionSet):void
		{
			super();
			_pmsDosageConversionSet = pmsDosageConversionSet;
		}
		
		public function get pmsDosageConversionSet():PmsDosageConversionSet
		{
			return _pmsDosageConversionSet;
		}
	}
}