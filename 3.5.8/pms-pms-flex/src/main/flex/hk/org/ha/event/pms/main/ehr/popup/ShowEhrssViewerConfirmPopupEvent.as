package hk.org.ha.event.pms.main.ehr.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowEhrssViewerConfirmPopupEvent extends AbstractTideEvent 
	{			
		private var _okHandler:Function;
		
		public function ShowEhrssViewerConfirmPopupEvent(okHandler:Function):void 
		{
			super();
			_okHandler = okHandler;
		}
		
		public function get okHandler():Function {
			return _okHandler;
		}
	}
}