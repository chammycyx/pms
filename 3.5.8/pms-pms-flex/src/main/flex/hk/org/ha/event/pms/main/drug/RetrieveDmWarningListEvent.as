package hk.org.ha.event.pms.main.drug {		
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDmWarningListEvent extends AbstractTideEvent 
	{
		private var _warnCode:String;
		private var _callBackFunction:Function
		
		public function RetrieveDmWarningListEvent(warnCode:String=null, functionValue:Function=null):void 
		{
			super();
			_warnCode = warnCode;
			_callBackFunction = functionValue;
		}
		
		public function get warnCode():String
		{
			return _warnCode;
		}
		
		public function get callBackFunction():Function
		{
			return _callBackFunction;
		}
	}
}