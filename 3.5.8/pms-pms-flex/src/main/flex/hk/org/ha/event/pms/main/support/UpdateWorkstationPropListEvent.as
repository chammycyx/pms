package hk.org.ha.event.pms.main.support
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class UpdateWorkstationPropListEvent extends AbstractTideEvent 
	{
		private var _workstationPropList:ArrayCollection;
		
		public function UpdateWorkstationPropListEvent(workstationPropList:ArrayCollection):void
		{
			super();
			_workstationPropList = workstationPropList;
		}
		
		public function get workstationPropList():ArrayCollection {
			return _workstationPropList;
		}
	}
}