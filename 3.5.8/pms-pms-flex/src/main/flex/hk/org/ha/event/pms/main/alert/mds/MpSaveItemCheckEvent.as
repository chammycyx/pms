package hk.org.ha.event.pms.main.alert.mds {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	
	public class MpSaveItemCheckEvent extends AbstractTideEvent {
		
		private var _medProfileItemList:ListCollectionView;
		
		private var _medProfile:MedProfile;
		
		private var _successFunc:Function;
		
		private var _failureFunc:Function;
		
		public function MpSaveItemCheckEvent(medProfileItemList:ListCollectionView, medProfile:MedProfile, successFunc:Function, failureFunc:Function) {
			super();
			_medProfileItemList = new ArrayCollection();
			for each (var mpItem:MedProfileItem in medProfileItemList) {
				if (mpItem.newItemFlag) {
					_medProfileItemList.addItem(mpItem);
				}
			}
			_medProfile = medProfile;
			_successFunc = successFunc;
			_failureFunc = failureFunc;
		}
		
		public function get medProfileItemList():ListCollectionView {
			return _medProfileItemList;
		}
		
		public function get medProfile():MedProfile {
			return _medProfile;
		}
		
		public function get successFunc():Function {
			return _successFunc;
		}
		
		public function get failureFunc():Function {
			return _failureFunc;
		}
	}
}