package hk.org.ha.control.pms.main.enquiry{
	
	import hk.org.ha.event.pms.main.enquiry.RetrieveDmFmDrugListByFullDrugDescEvent;
	import hk.org.ha.event.pms.main.enquiry.RetrieveDmFmDrugListEvent;
	import hk.org.ha.event.pms.main.enquiry.popup.ShowSpecialDrugEnqPopupEvent;
	import hk.org.ha.model.pms.biz.enquiry.SpecialDrugEnqListServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("specialDrugEnqListServiceCtl", restrict="true")]
	public class SpecialDrugEnqListServiceCtl 
	{
		[In]
		public var specialDrugEnqListService:SpecialDrugEnqListServiceBean;
		
		[Observer]
		public function retrieveDmFmDrugList(evt:RetrieveDmFmDrugListEvent):void
		{
			if( evt.showLookup ){
				specialDrugEnqListService.retrieveDmFmDrugList(evt.itemCodePrefix, retrieveDmFmDrugListResult);
			}else{
				specialDrugEnqListService.retrieveDmFmDrugList(evt.itemCodePrefix);
			}
		}

		public function retrieveDmFmDrugListResult(evt:TideResultEvent):void{
			dispatchEvent(new ShowSpecialDrugEnqPopupEvent(false));
		}

		[Observer]
		public function retrieveDmFmDrugListByFullDrugDesc(evt:RetrieveDmFmDrugListByFullDrugDescEvent):void
		{
			if( evt.showLookup ){
				specialDrugEnqListService.retrieveDmFmDrugListByFullDrugDesc(evt.fullDrugDescPrefix, retrieveDmFmDrugListByFullDrugDescResult);
			}else{
				specialDrugEnqListService.retrieveDmFmDrugListByFullDrugDesc(evt.fullDrugDescPrefix);
			}
		}

		public function retrieveDmFmDrugListByFullDrugDescResult(evt:TideResultEvent):void{
			dispatchEvent(new ShowSpecialDrugEnqPopupEvent(true));
		}
	}
}
