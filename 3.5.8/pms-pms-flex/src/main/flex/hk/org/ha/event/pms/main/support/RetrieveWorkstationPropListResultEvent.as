package hk.org.ha.event.pms.main.support
{
	import org.granite.tide.events.AbstractTideEvent;

	public class RetrieveWorkstationPropListResultEvent extends AbstractTideEvent 
	{
		private var _validHostName:Boolean;
		
		public function RetrieveWorkstationPropListResultEvent(validHostName:Boolean):void
		{
			super();
			_validHostName = validHostName;
		}
		
		public function get validHostName():Boolean {
			return _validHostName;
		}
	}
}