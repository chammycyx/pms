package hk.org.ha.event.pms.main.checkissue.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowTicketMatchForceProceedPopupEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;		
		private var _confirmCallback:Function;
		private var _cancelCallback:Function;
		
		public function ShowTicketMatchForceProceedPopupEvent(confirmCallback:Function=null, cancelCallback:Function=null, clearMessages:Boolean=true):void 
		{
			super();
			_confirmCallback = confirmCallback;
			_cancelCallback = cancelCallback;
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
		
		public function get confirmCallback():Function {
			return _confirmCallback;
		}
		
		public function get cancelCallback():Function {
			return _cancelCallback;
		}		
	}
}