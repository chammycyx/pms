package hk.org.ha.event.pms.main.alert.mds.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.alert.mds.MedProfileAlertMsg;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	
	public class ShowDischargeAlertMsgPopupEvent extends AbstractTideEvent {
		
		public var state:String;
		
		public var allowEditFlag:Boolean;
		
		public var acknowledgeFunc:Function;
		
		public var cancelVettingFunc:Function;
		
		public var closeFunc:Function;
		
		public var medProfileAlertMsgList:ListCollectionView;
		
		public function ShowDischargeAlertMsgPopupEvent(evt:ShowAlertMsgPopupEvent=null) {
			super();
			if (evt != null) {
				state             = evt.state;
				allowEditFlag     = evt.allowEditFlag;
				acknowledgeFunc   = evt.acknowledgeFunc;
				cancelVettingFunc = evt.cancelVettingFunc;
				closeFunc         = evt.closeFunc;
			}
		}
		
		public function set medProfileAlertMsg(medProfileAlertMsg:MedProfileAlertMsg):void {
			medProfileAlertMsgList = new ArrayCollection();
			medProfileAlertMsgList.addItem(medProfileAlertMsg);
		}
	}
}