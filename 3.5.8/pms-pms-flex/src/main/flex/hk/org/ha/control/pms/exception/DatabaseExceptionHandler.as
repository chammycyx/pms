package hk.org.ha.control.pms.exception {

    import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
    import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
    
    import mx.messaging.messages.ErrorMessage;
    
    import org.granite.tide.BaseContext;
    import org.granite.tide.IExceptionHandler;

    public class DatabaseExceptionHandler implements IExceptionHandler 
	{
		public static const NO_PRIVILEGES:String = "Database.NoPrivileges";         
				
		public function accepts(emsg:ErrorMessage):Boolean 
		{
			return emsg.faultCode == NO_PRIVILEGES;
		}
		
		public function handle(context:BaseContext, emsg:ErrorMessage):void 
		{
			var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp("0685");
			msgProp.setOkButtonOnly = true;
			context.dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
		}
    }
}
