package hk.org.ha.event.pms.main.drug {
	
	import hk.org.ha.model.pms.dms.vo.BnfSearchCriteria;
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDrugSearchBnfEvent extends AbstractTideEvent 
	{			
		private var _drugSearchSource:DrugSearchSource;
		private var _bnfSearchCriteria:BnfSearchCriteria;

		public function RetrieveDrugSearchBnfEvent(drugSearchSource:DrugSearchSource, bnfSearchCriteria:BnfSearchCriteria):void 
		{
			super();
			_drugSearchSource = drugSearchSource;
			_bnfSearchCriteria = bnfSearchCriteria;
		}
		
		public function get drugSearchSource():DrugSearchSource
		{
			return _drugSearchSource;
		}

		public function get bnfSearchCriteria():BnfSearchCriteria
		{
			return _bnfSearchCriteria;
		}
	}
}