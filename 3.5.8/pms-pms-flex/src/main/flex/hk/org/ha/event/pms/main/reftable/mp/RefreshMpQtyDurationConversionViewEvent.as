package hk.org.ha.event.pms.main.reftable.mp {
	
	import hk.org.ha.model.pms.vo.reftable.mp.MpQtyDurationConversionInfo;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshMpQtyDurationConversionViewEvent extends AbstractTideEvent 
	{
		private var _retrieveCapdItem:Boolean;
		private var _mpQtyDurationConversionInfo:MpQtyDurationConversionInfo;
		
		public function RefreshMpQtyDurationConversionViewEvent(retrieveCapdItem:Boolean, mpQtyDurationConversionInfo:MpQtyDurationConversionInfo):void 
		{
			super();
			_retrieveCapdItem = retrieveCapdItem;
			_mpQtyDurationConversionInfo = mpQtyDurationConversionInfo;
		}
		
		public function get retrieveCapdItem():Boolean
		{
			return _retrieveCapdItem;
		}
		
		public function get mpQtyDurationConversionInfo():MpQtyDurationConversionInfo 
		{
			return _mpQtyDurationConversionInfo;
		}

	}
}