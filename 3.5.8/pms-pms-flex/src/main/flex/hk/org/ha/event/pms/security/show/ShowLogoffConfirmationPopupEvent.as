package hk.org.ha.event.pms.security.show {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowLogoffConfirmationPopupEvent extends AbstractTideEvent {
		
		private var _messageCode:String;
		
		private var _yesCallback:Function;
		
		private var _noCallback:Function;
		
		public function ShowLogoffConfirmationPopupEvent(messageCode:String, yesCallback:Function=null, noCallback:Function=null) {
			super();
			_messageCode = messageCode;
			_yesCallback = yesCallback;
			_noCallback = noCallback;
		}
		
		public function get messageCode():String {
			return _messageCode;
		}
		
		public function get yesCallback():Function {
			return _yesCallback;
		}
		
		public function get noCallback():Function {
			return _noCallback;
		}
	}
}