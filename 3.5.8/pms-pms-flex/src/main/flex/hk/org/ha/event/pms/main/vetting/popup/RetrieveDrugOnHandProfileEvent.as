package hk.org.ha.event.pms.main.vetting.popup
{
	import hk.org.ha.model.pms.vo.vetting.OnHandProfileCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class RetrieveDrugOnHandProfileEvent extends AbstractTideEvent 
	{
		private var _onHandProfileCriteria:OnHandProfileCriteria;
		
		public function RetrieveDrugOnHandProfileEvent(onHandProfileCriteria:OnHandProfileCriteria):void
		{
			super();
			_onHandProfileCriteria = onHandProfileCriteria;
		}
		
		public function get onHandProfileCriteria():OnHandProfileCriteria {
			return _onHandProfileCriteria;
		}
	}
}