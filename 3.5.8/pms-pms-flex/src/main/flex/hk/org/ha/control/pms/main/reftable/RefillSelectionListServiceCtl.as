package hk.org.ha.control.pms.main.reftable {
	
	import hk.org.ha.event.pms.main.reftable.RetrieveRefillSelectionListEvent;
	import hk.org.ha.event.pms.main.reftable.SetRefillSelectionItemListEvent;
	import hk.org.ha.event.pms.main.reftable.ShowRefillModuleMaintSaveSuccessMsgEvent;
	import hk.org.ha.event.pms.main.reftable.UpdateRefillSelectionListEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.model.pms.biz.reftable.RefillSelectionListServiceBean;
	import hk.org.ha.model.pms.vo.reftable.RefillSelectionInfo;
	
	import mx.core.UIComponent;
	
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("refillSelectionListServiceCtl", restrict="true")]
	public class RefillSelectionListServiceCtl 
	{
		[In]
		public var refillSelectionListService:RefillSelectionListServiceBean;
		
		private var nextTabIndex:Number = 1;
		private var nextSubTabIndex:Number = 0;
		private var showRefillModuleMaintAlertMsg:Boolean = true;
		private var caller:UIComponent = null;
		
		[Observer]
		public function retrieveRefillSelectionList(evt:RetrieveRefillSelectionListEvent):void
		{
			refillSelectionListService.retrieveRefillSelectionList(retrieveRefillSelectionListResult, retrieveRefillSelectionListFailResult);
		}
		
		[Observer]
		public function updateRefillSelectionList(evt:UpdateRefillSelectionListEvent):void
		{
			showRefillModuleMaintAlertMsg = evt.showRefillModuleMaintAlertMsg;
			nextTabIndex = evt.nextTabIndex;
			nextSubTabIndex = evt.nextSubTabIndex;
			caller = evt.caller;
			refillSelectionListService.updateRefillSelectionList(evt.updateRefillSelectionList, evt.delRefillSelectionList, updateRefillSelectionListResult);
		}
		
		private function retrieveRefillSelectionListFailResult(evt:TideFaultEvent):void {
			evt.context.dispatchEvent( new CloseLoadingPopupEvent());
		}
		
		private function updateRefillSelectionListResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent( new ShowRefillModuleMaintSaveSuccessMsgEvent(nextTabIndex, nextSubTabIndex, showRefillModuleMaintAlertMsg, caller));
		}
		
		private function retrieveRefillSelectionListResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent( new CloseLoadingPopupEvent());
			evt.context.dispatchEvent(new SetRefillSelectionItemListEvent(evt.result as RefillSelectionInfo));
		}
		
		
	}
}
