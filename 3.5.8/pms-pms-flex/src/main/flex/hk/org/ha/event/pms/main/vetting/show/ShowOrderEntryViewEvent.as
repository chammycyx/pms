package hk.org.ha.event.pms.main.vetting.show {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowOrderEntryViewEvent extends AbstractTideEvent {
		
		public function ShowOrderEntryViewEvent() {
			super();
		}
	}
}
