package hk.org.ha.event.pms.main.pivas
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class QuickAddPivasBatchPrepEvent extends AbstractTideEvent 
	{		
		private var _lotNo:String;
		
		public function QuickAddPivasBatchPrepEvent(lotNo:String):void 
		{
			super();
			_lotNo = lotNo;
		}
		
		public function get lotNo():String {
			return _lotNo;
		}
	}
}