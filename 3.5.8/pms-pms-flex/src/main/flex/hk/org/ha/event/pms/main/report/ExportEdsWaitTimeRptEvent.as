package hk.org.ha.event.pms.main.report {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ExportEdsWaitTimeRptEvent extends AbstractTideEvent 
	{
		private var _date:Date;
		
		public function ExportEdsWaitTimeRptEvent(date:Date):void 
		{
			super();
			_date = date;
		}
		
		public function get date():Date {
			return _date;
		}
	}
}