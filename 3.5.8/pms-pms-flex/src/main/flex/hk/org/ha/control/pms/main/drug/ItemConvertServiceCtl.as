package hk.org.ha.control.pms.main.drug {
	
	import hk.org.ha.event.pms.main.vetting.CalSubTotalDosageForDangerousDrugEvent;
	import hk.org.ha.event.pms.main.vetting.DrugConvertEvent;
	import hk.org.ha.event.pms.main.vetting.RecalPharmOrderItemEvent;
	import hk.org.ha.event.pms.main.vetting.RefreshPharmOrderLineEditEvent;
	import hk.org.ha.event.pms.main.vetting.UpdateMedOrderItemFromOrderEditEvent;
	import hk.org.ha.model.pms.biz.drug.ItemConvertServiceBean;
	import hk.org.ha.model.pms.persistence.disp.MedOrder;
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	import hk.org.ha.model.pms.persistence.disp.PharmOrder;
	import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
	import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
	import hk.org.ha.model.pms.udt.rx.RxItemType;
	import hk.org.ha.model.pms.vo.rx.RxDrug;
	import hk.org.ha.model.pms.vo.rx.RxItem;
	import hk.org.ha.view.pms.main.vetting.OrderEditView;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("itemConvertServiceCtl", restrict="true")]
	public class ItemConvertServiceCtl 
	{
		
		[In]
		public var itemConvertService:ItemConvertServiceBean;
		
		[In]
		public function set pharmOrderItemList(val:ArrayCollection):void {
			_pharmOrderItemList = val;
		}
		
		public function get pharmOrderItemList():ArrayCollection {
			return _pharmOrderItemList;
		}
		
		[In]
		public var ctx:Context;
		
		[In]
		public var pharmOrder:PharmOrder;				
		
		[In]
		public var pharmOrderItemCalQty:Number;
		
		[In]
		public var pharmOrderItemDoseQty:Number;
		
		[In]
		public var itemConvertMedOrderItemNum:int;
		
		[In]
		public var rxItemForOrderEdit:RxItem;
		
		private var _pharmOrderItemList:ArrayCollection;
		
		private var medOrderItem:MedOrderItem;
		
		private var moiSelectedIndex:int;
		
		private var poiSelectedIndex:int;
		
		private var convertMedOrderItemIsCapd:Boolean;
		
		private var callBackFunction:Function;
		
		private var capdCancelFunc:Function;
		
		[Observer]
		public function convertMedOrderItem(evt:DrugConvertEvent):void {			
			ctx.pharmOrderItemList = null;
			medOrderItem = evt.medOrderItem;
			moiSelectedIndex = evt.selectedIndex;
			disconnectMedOrderItem(medOrderItem);
			convertMedOrderItemIsCapd = evt.isCapd;
			capdCancelFunc = evt.cancelHandler;
			itemConvertService.convertMedOrderItem(medOrderItem, convertMedOrderItemResult, convertMedOrderItemFaultResult);	
		}
		
		private function convertMedOrderItemResult(evt:TideResultEvent):void {	
			reconnectMedOrderItem();
			
			if (pharmOrder.medOrder.docType == MedOrderDocType.Manual && ! isNaN(medOrderItem.id)) {
				medOrderItem.itemNum = itemConvertMedOrderItemNum;
			}
			
			//assign formatted drug order details
			if (pharmOrder.medOrder.isMpDischarge()) {								
				if (medOrderItem.rxItem.isRxDrug && rxItemForOrderEdit.isRxDrug) {
					RxDrug(medOrderItem.rxItem).formattedDrugNameTlf = RxDrug(rxItemForOrderEdit).formattedDrugNameTlf;				
					RxDrug(medOrderItem.rxItem).formattedDrugOrderTlf = RxDrug(rxItemForOrderEdit).formattedDrugOrderTlf;
					RxDrug(medOrderItem.rxItem).drugNameHtml = RxDrug(rxItemForOrderEdit).drugNameHtml;
					RxDrug(medOrderItem.rxItem).drugNameTlf = RxDrug(rxItemForOrderEdit).drugNameTlf;
					RxDrug(medOrderItem.rxItem).drugNameXml = RxDrug(rxItemForOrderEdit).drugNameXml;
					RxDrug(medOrderItem.rxItem).drugOrderHtml = RxDrug(rxItemForOrderEdit).drugOrderHtml;
					RxDrug(medOrderItem.rxItem).drugOrderTlf = RxDrug(rxItemForOrderEdit).drugOrderTlf;
					RxDrug(medOrderItem.rxItem).drugOrderXml = RxDrug(rxItemForOrderEdit).drugOrderXml;
				}
			}
			
			if (medOrderItem.rxItem.isRxDrug && rxItemForOrderEdit.isRxDrug) {
				RxDrug(medOrderItem.rxItem).moDoseAlertFlag = rxItemForOrderEdit.moDoseAlertFlag;
			}		
			if ( convertMedOrderItemIsCapd ) {
				evt.context.dispatchEvent( new UpdateMedOrderItemFromOrderEditEvent(moiSelectedIndex, medOrderItem,null,false,capdCancelFunc) );
			} else {
				evt.context.dispatchEvent( new RefreshPharmOrderLineEditEvent );
			}
		}
		
		private function convertMedOrderItemFaultResult(evt:TideFaultEvent):void {
			var orderEditView:OrderEditView = ctx.orderEditView as OrderEditView;
			if ( orderEditView && orderEditView.disableProcessFlag != null ) {
				orderEditView.disableProcessFlag();
			}
		}
		
		[Observer]
		public function recalPharmOrderItem(evt:RecalPharmOrderItemEvent):void {						
			ctx.pharmOrderItemCalQty = null;
			ctx.pharmOrderItemDoseQty = null;
			ctx.pharmOrderItemList = null;
			callBackFunction = evt.callBackFunction;			
			itemConvertService.recalPharmOrderItem(evt.pharmOrderItem,recalPharmOrderItemResult,recalPharmOrderItemFaultResult);
		}			
		
		private function recalPharmOrderItemResult(evt:TideResultEvent):void {	
			if ( callBackFunction != null ) {
				callBackFunction(pharmOrderItemCalQty, pharmOrderItemDoseQty);
			}
		}				
		
		private function recalPharmOrderItemFaultResult(evt:TideFaultEvent):void {
			var orderEditView:OrderEditView = ctx.orderEditView as OrderEditView;
			if ( orderEditView && orderEditView.disableProcessFlag != null ) {
				orderEditView.disableProcessFlag();
			}
		}
		
		[Observer]
		public function calSubTotalDosageForDangerousDrug(evt:CalSubTotalDosageForDangerousDrugEvent):void
		{			
			itemConvertService.calSubTotalDosage(evt.rxItem, evt.doseGroupIndex, evt.doseIndex, function(tideResultEvent:TideResultEvent):void {
				if ( evt.callBackFunction != null ) {
					evt.callBackFunction(tideResultEvent.result);
				}
			});
		}
		
		private function calSubTotalDosageForDangerousDrugResult(evt:TideResultEvent):void
		{
			if ( callBackFunction != null ) {
				callBackFunction(evt.result);
			}
		}
		
		private function disconnectMedOrderItem(moi:MedOrderItem):void {			
			moi.pharmOrderItemList.removeAll();
			if (moi.duplicateItemList != null) {
				moi.duplicateItemList.removeAll();
			}
			moi.medOrder=null;
		}				
		
		private function reconnectMedOrderItem():void {
			var medOrder:MedOrder = pharmOrder.medOrder;
			var moi:MedOrderItem;
			var poi:PharmOrderItem;
			//Re-connect MedOrderItem	
			medOrderItem.medOrder = medOrder;
			
			medOrderItem.pharmOrderItemList = new ArrayCollection;
			
			for each ( poi in pharmOrderItemList ) {				
				poi.medOrderItem = medOrderItem;
				medOrderItem.pharmOrderItemList.addItem(poi);
			}						
			
			pharmOrder.pharmOrderItemList = new ArrayCollection();
			medOrder.medOrderItemList.setItemAt(medOrderItem,moiSelectedIndex);
			for each ( moi in medOrder.medOrderItemList ) {				
				for each ( poi in moi.pharmOrderItemList ) {					
					poi.pharmOrder = pharmOrder;
					pharmOrder.pharmOrderItemList.addItem(poi);
				}					
			}
		}	
	}
}
