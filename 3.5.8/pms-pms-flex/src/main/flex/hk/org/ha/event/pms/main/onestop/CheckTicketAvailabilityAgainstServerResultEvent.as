package hk.org.ha.event.pms.main.onestop {
		
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CheckTicketAvailabilityAgainstServerResultEvent extends AbstractTideEvent 
	{	
		private var _followUpFunc:Function;
		
		public function CheckTicketAvailabilityAgainstServerResultEvent(followUpFunc:Function):void 
		{
			super();
			_followUpFunc = followUpFunc;
		}        
		
		public function get followUpFunc():Function {
			return _followUpFunc;
		}
	}
}