package hk.org.ha.event.pms.main.checkissue.mp
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveAssignBatchSummaryEvent extends AbstractTideEvent 
	{			
		private var _batchDate:Date;
		private var _batchNum:String;
		
		public function RetrieveAssignBatchSummaryEvent(batchDate:Date, batchNum:String):void 
		{
			super();
			_batchDate = batchDate;
			_batchNum = batchNum;
		}
		
		public function get batchDate():Date {
			return _batchDate;
		}
		
		public function get batchNum():String {
			return _batchNum;
		}
	}
}