package hk.org.ha.event.pms.main.vetting.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowConfirmUpdatePatientPopupEvent extends AbstractTideEvent {
		
		private var _name:String;
		
		private var _callback:Function;
		
		public function ShowConfirmUpdatePatientPopupEvent(name:String, callback:Function) {
			super();
			_name = name;
			_callback = callback;
		}
		
		public function get name():String {
			return _name;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}
