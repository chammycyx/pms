package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDmFormVerbMappingListBySiteCodeEvent extends AbstractTideEvent 
	{
		private var _siteCode:String;
		private var _callBackEvent:Event;
		
		public function RetrieveDmFormVerbMappingListBySiteCodeEvent(siteCode:String, callBackEventVal:Event=null):void 
		{
			super();
			_siteCode = siteCode;
			_callBackEvent = callBackEventVal;
		}
		
		public function get siteCode():String
		{
			return _siteCode;
		}
		
		public function get callBackEvent():Event
		{
			return _callBackEvent;
		}
	}
}