package hk.org.ha.control.pms.main.onestop {
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.event.pms.main.onestop.CheckManualOrderFcsClearanceEvent;
	import hk.org.ha.event.pms.main.onestop.CheckNormalOrderFcsClearanceEvent;
	import hk.org.ha.event.pms.main.onestop.CheckTicketAvailabilityAgainstServerEvent;
	import hk.org.ha.event.pms.main.onestop.CheckTicketAvailabilityAgainstServerResultEvent;
	import hk.org.ha.event.pms.main.onestop.ClearKeyboardBufferEvent;
	import hk.org.ha.event.pms.main.onestop.ClearOSEOrderDataEvent;
	import hk.org.ha.event.pms.main.onestop.ClearOSEOrderDataFollowUpEvent;
	import hk.org.ha.event.pms.main.onestop.ClearOrderEvent;
	import hk.org.ha.event.pms.main.onestop.DefaultCloseComboBoxSpecialtyMedOfficerMappingEvent;
	import hk.org.ha.event.pms.main.onestop.DefaultSpecialtyMedOfficerMappingEvent;
	import hk.org.ha.event.pms.main.onestop.FilterMoeListEvent;
	import hk.org.ha.event.pms.main.onestop.InvalidSearchResultEvent;
	import hk.org.ha.event.pms.main.onestop.PrintOSEPrescRptEvent;
	import hk.org.ha.event.pms.main.onestop.RefreshOrderStatusEvent;
	import hk.org.ha.event.pms.main.onestop.RefreshSelectedOrderStatusEvent;
	import hk.org.ha.event.pms.main.onestop.RetrieveDispOrderByTicketNumEvent;
	import hk.org.ha.event.pms.main.onestop.RetrieveLastThreeDaysPrescriptionListEvent;
	import hk.org.ha.event.pms.main.onestop.RetrieveMoeOrderEvent;
	import hk.org.ha.event.pms.main.onestop.RetrieveOrderEvent;
	import hk.org.ha.event.pms.main.onestop.RetrievePagedPatientEvent;
	import hk.org.ha.event.pms.main.onestop.RetrievePrescriptionDispOrderEvent;
	import hk.org.ha.event.pms.main.onestop.RetrievePrescriptionDispOrderResultEvent;
	import hk.org.ha.event.pms.main.onestop.RetrieveSuspendOrderEvent;
	import hk.org.ha.event.pms.main.onestop.RetrieveUnvetAndSuspendCountEvent;
	import hk.org.ha.event.pms.main.onestop.SetCloseComboBoxDefaultMedOfficerEvent;
	import hk.org.ha.event.pms.main.onestop.SetDefaultMedOfficerEvent;
	import hk.org.ha.event.pms.main.onestop.ShowChargingPopupByPatientTypeEvent;
	import hk.org.ha.event.pms.main.onestop.ShowInfomationMessageEvent;
	import hk.org.ha.event.pms.main.onestop.SortPagedMoeEvent;
	import hk.org.ha.event.pms.main.onestop.UpdatePreVetValueEvent;
	import hk.org.ha.event.pms.main.onestop.ValidSearchResultEvent;
	import hk.org.ha.event.pms.main.onestop.WriteHkidUnknownDigitLogEvent;
	import hk.org.ha.event.pms.main.onestop.popup.ShowMessagePopupEvent;
	import hk.org.ha.event.pms.main.onestop.popup.ShowOneStopEntryHkidPopupEvent;
	import hk.org.ha.event.pms.main.onestop.popup.ShowOneStopEntryOrderListSameRefNumPopupEvent;
	import hk.org.ha.event.pms.main.onestop.popup.ShowOneStopEntryPrescriptionListPopupEvent;
	import hk.org.ha.event.pms.main.onestop.popup.ShowOneStopEntryPrescriptionSummaryPopupEvent;
	import hk.org.ha.event.pms.main.onestop.popup.ShowYesNoMessagePopupEvent;
	import hk.org.ha.event.pms.main.onestop.show.ShowOneStopEntryViewEvent;
	import hk.org.ha.event.pms.main.prevet.PreVetOperationCompletedFollowUpEvent;
	import hk.org.ha.event.pms.main.prevet.RetrieveDischargeMedOrderEvent;
	import hk.org.ha.event.pms.main.prevet.ShowPreVetMsgEvent;
	import hk.org.ha.event.pms.main.vetting.StartVettingEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.ShowLoadingPopupEvent;
	import hk.org.ha.model.pms.biz.enquiry.DrugChargeClearanceEnqServiceBean;
	import hk.org.ha.model.pms.biz.onestop.OneStopPrescRptServiceBean;
	import hk.org.ha.model.pms.biz.onestop.OneStopServiceBean;
	import hk.org.ha.model.pms.biz.onestop.PrescOrderListServiceBean;
	import hk.org.ha.model.pms.biz.onestop.SpecialtyMoMappingServiceBean;
	import hk.org.ha.model.pms.biz.ticketgen.TicketServiceBean;
	import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
	import hk.org.ha.model.pms.udt.prevet.PreVetMode;
	import hk.org.ha.model.pms.udt.validation.SearchTextType;
	import hk.org.ha.model.pms.vo.charging.DrugCheckClearanceResult;
	import hk.org.ha.model.pms.vo.onestop.OneStopOrderItemSummary;
	import hk.org.ha.model.pms.vo.onestop.OneStopRefillItemSummary;
	import hk.org.ha.model.pms.vo.onestop.OneStopSummary;
	import hk.org.ha.model.pms.vo.patient.DhPatient;
	import hk.org.ha.model.pms.vo.security.PropMap;
	
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
		
	[Bindable]
	[Name("oneStopServiceCtl", restrict="true")]
	public class OneStopServiceCtl 
	{
		[In]
		public var oneStopService:OneStopServiceBean;
		
		[In]
		public var ticketService:TicketServiceBean;
		
		[In]
		public var specialtyMoMappingService:SpecialtyMoMappingServiceBean;
		
		[In]
		public var drugChargeClearanceEnqService:DrugChargeClearanceEnqServiceBean;
		
		[In]
		public var prescOrderListService:PrescOrderListServiceBean;
		
		[In]
		public var oneStopPrescRptService:OneStopPrescRptServiceBean;
		
		[In]
		public var errMsg:String;
		
		[In]
		public var oneStopSummary:OneStopSummary;
		
		[In]
		public var drugClearCheckResult:DrugCheckClearanceResult;
			
		[In]
		public var propMap:PropMap;
		
		[In]
		public var latestOneStopOrderItemSummary:OneStopOrderItemSummary;
		
		[In]
		public var latestOneStopRefillItemSummary:OneStopRefillItemSummary;
		
		[In]
		public var latestOneStopRefillItemSummaryList:ArrayCollection;
				
		private var retrieveOrderEvent:RetrieveOrderEvent;
		private var checkTicketAvailabilityAgainstServerEvent:CheckTicketAvailabilityAgainstServerEvent;
		private var checkNormalOrderFcsClearanceEvent:CheckNormalOrderFcsClearanceEvent;
		private var checkManualOrderFcsClearanceEvent:CheckManualOrderFcsClearanceEvent;
		private var retrieveLastThreeDaysPrescriptionListEvent:RetrieveLastThreeDaysPrescriptionListEvent;
		private var retrieveMoeOrderEvent:RetrieveMoeOrderEvent;
		private var retrievePrescriptionDispOrderEvent:RetrievePrescriptionDispOrderEvent;
		private var refreshSelectedOrderStatusEvent:RefreshSelectedOrderStatusEvent;
		private var defaultSpecialtyMedOfficerMappingEvent:DefaultSpecialtyMedOfficerMappingEvent;
		private var clearOrderEvent:ClearOrderEvent;
		private var retrievePagedPatientEvent:RetrievePagedPatientEvent;
		private var filterMoeListEvent:FilterMoeListEvent;
		private var sortPagedMoeEvent:SortPagedMoeEvent;
		
		private static var NO_HKPMI_PATIENT_FOUND:String = "noHkpmiPatientFound";
		private static var PATIENT_PAS_PATIENT_ENABLED:String = "patient.pas.patient.enabled";
		private static var ONE_STOP_ENTRY:String = "One Stop Entry";
		
		private var dhPatient:DhPatient;
		
		[Observer]
		public function retrieveOrder(evt:RetrieveOrderEvent):void 
		{
			In(Object(oneStopService).refNumOrderList);
			In(Object(oneStopService).caseNumUnvetList);
			In(Object(oneStopService).caseNumVettedList);
			retrieveOrderEvent = evt;
			oneStopService.retrieveOrder(evt.searchText, evt.displayAllMoe, retrieveMedOrderResult);
		}
		
		private function retrieveMedOrderResult(evt:TideResultEvent):void 
		{
			if (errMsg != null && errMsg != "0080") 
			{
				evt.context.dispatchEvent(new InvalidSearchResultEvent());
				if (errMsg == "0005" || errMsg == "0715") 
				{
					evt.context.dispatchEvent(new ShowInfomationMessageEvent(new Array(errMsg), false));
				}
				return;
			}
						
			if (Object(oneStopService).refNumOrderList != null) 
			{
				evt.context.dispatchEvent(new ShowOneStopEntryOrderListSameRefNumPopupEvent(Object(oneStopService).refNumOrderList));
				return;
			}
						
			switch (oneStopSummary.searchTextType) 
			{
				case SearchTextType.OrderNum:
				case SearchTextType.OrderRefNum:
					if (oneStopSummary.oneStopOrderItemSummaryList.getItemAt(0).patient == null) 
					{
						showPasProgressingPopup();
						oneStopService.retrieveHkPmiPatientByOrderNumOrRefNumOrTickNum(
							retrievePatientAndAppointmentFromPasResult, callThirdPartyFaultResult);
					} 
					else 
					{
						showPasProgressingPopup();
						oneStopService.retrieveHkPmiPatientForVettedOrder(
							retrievePatientAndAppointmentFromPasResult, callThirdPartyFaultResult);
					}
					break;
				
				case SearchTextType.Ticket:
					if (oneStopSummary.oneStopOrderItemSummaryList.length > 0)
					{
						if (oneStopSummary.oneStopOrderItemSummaryList.getItemAt(0).patient == null) 
						{
							showPasProgressingPopup();
							oneStopService.retrieveHkPmiPatientByOrderNumOrRefNumOrTickNum(
								retrievePatientAndAppointmentFromPasResult, callThirdPartyFaultResult);
						} 
						else 
						{
							showPasProgressingPopup();
							oneStopService.retrieveHkPmiPatientForVettedOrder(
								retrievePatientAndAppointmentFromPasResult, callThirdPartyFaultResult);
						}
					}
					else
					{
						oneStopService.retrieveHkPmiPatientForVettedOrder(
							retrievePatientAndAppointmentFromPasResult, callThirdPartyFaultResult);
					}
						
					break;
				
				case SearchTextType.Refill:
				case SearchTextType.SfiRefill:
					 showPasProgressingPopup();
					oneStopService.retrieveHkPmiPatientForVettedOrder(
						retrievePatientAndAppointmentFromPasResult, callThirdPartyFaultResult);
					 break;
				
				case SearchTextType.Case:
					showPasProgressingPopup();
					oneStopService.retrieveHkPmiPatientAndAppointmentByCaseNum(retrieveOrderEvent.searchText, 
						Object(oneStopService).caseNumUnvetList, Object(oneStopService).caseNumVettedList,
						retrievePatientAndAppointmentFromPasResult, callThirdPartyFaultResult);			
					break;
				
				case SearchTextType.Hkid:
					showPasProgressingPopup();
					oneStopService.retrieveOrderByHkid(retrieveOrderEvent.searchText, retrieveOrderEvent.displayAllMoe, 
						retrieveOrderEvent.callPasAppointment, retrievePatientAndAppointmentFromPasResult, 
						callThirdPartyFaultResult);
					break;
				
				case SearchTextType.Capd:
					dispatchEvent(new ShowLoadingPopupEvent("Loading Order information..."));
					var ticketDate:Date = oneStopSummary.oneStopOrderItemSummaryList.getItemAt(0).ticketDate;
					var ticketNum:String = oneStopSummary.oneStopOrderItemSummaryList.getItemAt(0).ticketNum;
					evt.context.dispatchEvent(new RetrieveDispOrderByTicketNumEvent(ONE_STOP_ENTRY, ticketDate, ticketNum, true));
					break;
				default:
					evt.context.dispatchEvent(new ValidSearchResultEvent());
					break;
			}
		}
		
		private function showPasProgressingPopup():void 
		{
			dispatchEvent(new ShowLoadingPopupEvent("Loading PAS information..."));
		}
		
		private function retrievePatientAndAppointmentFromPasResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());			
			
			if (errMsg != null) 
			{
				if (errMsg == NO_HKPMI_PATIENT_FOUND) 
				{
					dispatchEvent(new ShowOneStopEntryHkidPopupEvent());
					return;
				}
				
				evt.context.dispatchEvent(new ValidSearchResultEvent());
				evt.context.dispatchEvent(new ShowInfomationMessageEvent(new Array(errMsg), false));
			} 
			else 
			{
				if (retrieveOrderEvent.isUnvetTrigger)
				{
					evt.context.dispatchEvent(new ShowInfomationMessageEvent(new Array("0543", retrieveOrderEvent.searchText), false));
				}
				
				evt.context.dispatchEvent(new ValidSearchResultEvent());
			}
		}
						
		[Observer]
		public function clearOrder(evt:ClearOrderEvent):void 
		{
			
			if (evt.requestFromOneStopEntryView) 
			{
				oneStopService.clearOrder();
			} 
			else 
			{
				clearOrderEvent = evt;
				oneStopService.clearOrder(clearResult);
			}
		}
		
		private function clearResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new ShowOneStopEntryViewEvent(true, clearOrderEvent.orderNum));
		}
				
		[Observer]
		public function checkTicketAvailabilityAgainstServer(evt:CheckTicketAvailabilityAgainstServerEvent):void
		{
			checkTicketAvailabilityAgainstServerEvent = evt;
			ticketService.retrieveTicketByTicketNum(evt.ticketDate, evt.ticketNum, evt.orderId, evt.oneStopOrderType, 
				retrieveTicketByTicketNumResult);
		}
		
		private function retrieveTicketByTicketNumResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(
				new CheckTicketAvailabilityAgainstServerResultEvent(checkTicketAvailabilityAgainstServerEvent.followUpFunc));
		}		
		
		[Observer]
		public function checkNormalOrderFcsClearance(evt:CheckNormalOrderFcsClearanceEvent):void {
			checkNormalOrderFcsClearanceEvent = evt;
			drugChargeClearanceEnqService.checkClearanceForOnestop(evt.orderId, 
				checkNormalOrderFcsClearanceResult, callThirdPartyFaultResult);
		}
		
		private function checkNormalOrderFcsClearanceResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			if (drugClearCheckResult.systemMessageCode != "") 
			{
				evt.context.dispatchEvent(new ShowYesNoMessagePopupEvent(
					new Array(drugClearCheckResult.systemMessageCode), 
					checkNormalOrderFcsClearanceEvent.fcsForceProceedRequireYes,
					checkNormalOrderFcsClearanceEvent.fcsForceProceedRequireNo));
			} 
			else 
			{
				checkNormalOrderFcsClearanceEvent.fcsForceProceedNotRequire();
			}
		}
		
		[Observer]
		public function checkManualOrderFcsClearance(evt:CheckManualOrderFcsClearanceEvent):void 
		{
			checkManualOrderFcsClearanceEvent = evt;
			drugChargeClearanceEnqService.checkClearanceForOnestopManual(
				evt.patHospCode, evt.orderId, evt.pasApptSeq, checkManualOrderFcsClearanceResult, callThirdPartyFaultResult);
		}
		
		private function checkManualOrderFcsClearanceResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			switch (drugClearCheckResult.systemMessageCode)
			{
				case "0062":
					evt.context.dispatchEvent(new ShowYesNoMessagePopupEvent(
						new Array(drugClearCheckResult.systemMessageCode), 
						checkManualOrderFcsClearanceEvent.fcsForceProceedRequireYes, 
						checkManualOrderFcsClearanceEvent.fcsForceProceedRequireNo));
					break;
				
				case "0063":
					evt.context.dispatchEvent(new ShowChargingPopupByPatientTypeEvent(
						checkManualOrderFcsClearanceEvent.fcsError));
					break;
				
				case "0064":
					evt.context.dispatchEvent(new ShowChargingPopupByPatientTypeEvent(
						checkManualOrderFcsClearanceEvent.fcsErrorDetails));
					break;
				
				default:
					checkManualOrderFcsClearanceEvent.fcsForceProceedNotRequire();
					break;
			}
		}
		
		[Observer]
		public function retrieveLastThreeDaysPrescriptionList(evt:RetrieveLastThreeDaysPrescriptionListEvent):void 
		{
			In(Object(oneStopService).prescriptionOrderList);
			In(Object(oneStopService).vettingMsg);
			retrieveLastThreeDaysPrescriptionListEvent = evt;
			oneStopService.retrievePrescriptionListFromLastThreeDays(evt.oneStopOrderType, evt.orderId, evt.medOrderVersion, 
				evt.dispOrderId, evt.refillId, evt.hkid, retrieveLastThreeDaysPrescriptionListResult);
		}
		
		private function retrieveLastThreeDaysPrescriptionListResult(evt:TideResultEvent):void 
		{
			if (Object(oneStopService).prescriptionOrderList == null)
			{
				evt.context.dispatchEvent(new ShowInfomationMessageEvent(new Array(Object(oneStopService).vettingMsg), true));
				return;
			}
			
			if (Object(oneStopService).prescriptionOrderList.length > 0) 
			{
				switch(retrieveLastThreeDaysPrescriptionListEvent.oneStopOrderType) 
				{
					case OneStopOrderType.RefillOrder:
					case OneStopOrderType.SfiOrder:
						evt.context.dispatchEvent(new ShowOneStopEntryPrescriptionSummaryPopupEvent(
							retrieveLastThreeDaysPrescriptionListEvent.oneStopOrderType, 
							retrieveLastThreeDaysPrescriptionListEvent.refillId, retrieveLastThreeDaysPrescriptionListEvent.showPreVetView,
							retrieveLastThreeDaysPrescriptionListEvent.orderNum, NaN, Object(oneStopService).prescriptionOrderList));
						break;
					
					case OneStopOrderType.DispOrder:
						evt.context.dispatchEvent(new ShowOneStopEntryPrescriptionSummaryPopupEvent(
							retrieveLastThreeDaysPrescriptionListEvent.oneStopOrderType, 
							retrieveLastThreeDaysPrescriptionListEvent.dispOrderId, retrieveLastThreeDaysPrescriptionListEvent.showPreVetView,
							retrieveLastThreeDaysPrescriptionListEvent.orderNum, NaN, Object(oneStopService).prescriptionOrderList));
						break;
					
					default:
						evt.context.dispatchEvent(new ShowOneStopEntryPrescriptionSummaryPopupEvent(
							retrieveLastThreeDaysPrescriptionListEvent.oneStopOrderType, 
							retrieveLastThreeDaysPrescriptionListEvent.orderId, 
							retrieveLastThreeDaysPrescriptionListEvent.showPreVetView,
							retrieveLastThreeDaysPrescriptionListEvent.orderNum, retrieveLastThreeDaysPrescriptionListEvent.medOrderVersion, 
							Object(oneStopService).prescriptionOrderList, retrieveLastThreeDaysPrescriptionListEvent.patient));
						break;
				}
			} 
			else 
			{
				switch(retrieveLastThreeDaysPrescriptionListEvent.oneStopOrderType) 
				{
					case OneStopOrderType.RefillOrder:
					case OneStopOrderType.SfiOrder:
						evt.context.dispatchEvent(new StartVettingEvent(
							retrieveLastThreeDaysPrescriptionListEvent.oneStopOrderType, 
							retrieveLastThreeDaysPrescriptionListEvent.refillId));
						break;
					
					case OneStopOrderType.DispOrder:
						evt.context.dispatchEvent(new StartVettingEvent(
							retrieveLastThreeDaysPrescriptionListEvent.oneStopOrderType, 
							retrieveLastThreeDaysPrescriptionListEvent.dispOrderId));
						break;
										
					case OneStopOrderType.MedOrder:
					case OneStopOrderType.DhOrder:
						if (retrieveLastThreeDaysPrescriptionListEvent.showPreVetView)
						{
							evt.context.dispatchEvent(new RetrieveDischargeMedOrderEvent(retrieveLastThreeDaysPrescriptionListEvent.orderNum, 
													  PreVetMode.OneStopEntry, retrieveLastThreeDaysPrescriptionListEvent.patient, 
													  retrieveLastThreeDaysPrescriptionListEvent.orderId, 
													  retrieveLastThreeDaysPrescriptionListEvent.medOrderVersion));
						}
						else
						{						
							evt.context.dispatchEvent(new StartVettingEvent(
								retrieveLastThreeDaysPrescriptionListEvent.oneStopOrderType, 
								retrieveLastThreeDaysPrescriptionListEvent.orderId));
						}
						break;
					
					default:
						evt.context.dispatchEvent(new StartVettingEvent(
							retrieveLastThreeDaysPrescriptionListEvent.oneStopOrderType, 
							retrieveLastThreeDaysPrescriptionListEvent.orderId));
						break;
				}
			}
		}
		
		[Observer]
		public function refreshSelectedOrderStatus(evt:RefreshSelectedOrderStatusEvent):void {
			refreshSelectedOrderStatusEvent = evt;
			oneStopService.refreshOrderStatus(evt.oneStopOrderType, evt.orderId, evt.refillId, refreshSelectedOrderStatusResult);
		}
		
		private function refreshSelectedOrderStatusResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new RefreshOrderStatusEvent(latestOneStopOrderItemSummary, 
				latestOneStopRefillItemSummary, latestOneStopRefillItemSummaryList));
				
			if (refreshSelectedOrderStatusEvent.msgCode != null)
			{
				evt.context.dispatchEvent(new ShowMessagePopupEvent(new Array(refreshSelectedOrderStatusEvent.msgCode)));
			}
		}
		
		[Observer]
		public function retrieveUnvetAndSuspendCount(evt:RetrieveUnvetAndSuspendCountEvent):void {
			oneStopService.retrieveUnvetAndSuspendCount();
		} 
		
		[Observer]
		public function defaultSpecialtyMedOfficerMapping(evt:DefaultSpecialtyMedOfficerMappingEvent):void 
		{
			defaultSpecialtyMedOfficerMappingEvent = evt;
			specialtyMoMappingService.retrieveDmSpecialtyMoMappingBySpecialtyCode(evt.specCode, 
				defaultSpecialtyMedOfficerMappingResult);
		}
		
		private function defaultSpecialtyMedOfficerMappingResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new SetDefaultMedOfficerEvent(defaultSpecialtyMedOfficerMappingEvent.proceed));
		}
		
		[Observer]
		public function defaultCloseComboBoxSpecialtyMedOfficerMapping(evt:DefaultCloseComboBoxSpecialtyMedOfficerMappingEvent):void 
		{
			specialtyMoMappingService.retrieveDmSpecialtyMoMappingBySpecialtyCode(evt.specCode, 
				defaultCloseComboBoxSpecialtyMedOfficerMappingResult);
		}
		
		private function defaultCloseComboBoxSpecialtyMedOfficerMappingResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new SetCloseComboBoxDefaultMedOfficerEvent());
		}
		
		[Observer]
		public function retrieveMoeOrder(evt:RetrieveMoeOrderEvent):void {
			retrieveMoeOrderEvent = evt;
			dispatchEvent(new ShowLoadingPopupEvent("Loading orders & PAS information..."));
			oneStopService.retrieveMoeOrder(evt.ordDate, evt.moeFilterOption, evt.prescriptionSortCriteriaList, retrieveMoeOrderResult);
		}
		
		private function retrieveMoeOrderResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			if (retrieveMoeOrderEvent.showPopup) {
				dispatchEvent(new ShowOneStopEntryPrescriptionListPopupEvent());
			}
			
			if (retrieveMoeOrderEvent.callbackFunction != null)
			{
				retrieveMoeOrderEvent.callbackFunction();
			}
		}
		
		[Observer]
		public function retrievePagedPatient(evt:RetrievePagedPatientEvent):void
		{
			retrievePagedPatientEvent = evt;
			dispatchEvent(new ShowLoadingPopupEvent("Loading PAS information..."));
			oneStopService.retrievePagedPatient(evt.oneStopOrderItemSummaryIdList, evt.caller, retrievePagedPatientResult);
		}
		
		private function retrievePagedPatientResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			retrievePagedPatientEvent.callbackFunction();
		}
		
		[Observer]
		public function filterMoeUnvetList(evt:FilterMoeListEvent):void
		{
			filterMoeListEvent = evt;
			dispatchEvent(new ShowLoadingPopupEvent("Loading orders & PAS information..."));	
			oneStopService.filterMoeList(evt.moeFilterOption, evt.prescriptionSortCriteriaList, filterMoeUnvetListResult);
		}
		
		private function filterMoeUnvetListResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			filterMoeListEvent.callbackFunction();
		}
		
		[Observer]
		public function sortPagedMoe(evt:SortPagedMoeEvent):void
		{
			sortPagedMoeEvent = evt;
			dispatchEvent(new ShowLoadingPopupEvent("Loading orders & PAS information..."));	
			oneStopService.sortMoeList(evt.prescriptionSortCriteria, sortPagedMoeResult);
		}
		
		private function sortPagedMoeResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			sortPagedMoeEvent.callbackFunction();
		}
		
		[Observer]
		public function retrieveSuspendOrder(evt:RetrieveSuspendOrderEvent):void {
			oneStopService.retrieveSuspendOrder(evt.startDate,function(tideResultEvent:TideResultEvent):void {
				if (evt.callBack != null) {
					evt.callBack();
				}
			});
		}
		
		[Observer]
		public function retrievePrescriptionDispOrder(evt:RetrievePrescriptionDispOrderEvent):void
		{
			In(Object(prescOrderListService).oneStopMedOrderItemList);
			retrievePrescriptionDispOrderEvent = evt;
			prescOrderListService.retrievePrescItemList(evt.dispDate, evt.ticketNum, evt.workstoreCode, retrievePrescriptionDispOrderResult);
		}
		
		private function retrievePrescriptionDispOrderResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RetrievePrescriptionDispOrderResultEvent(Object(prescOrderListService).oneStopMedOrderItemList,
				retrievePrescriptionDispOrderEvent.oneStopEntryPrescriptionSummaryPopup));
		}
		
		private function callThirdPartyFaultResult(evt:TideFaultEvent):void 
		{
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			evt.context.dispatchEvent(new ClearKeyboardBufferEvent());			
		}
		
		[Observer]
		public function clearOSEOrderData(evt:ClearOSEOrderDataEvent):void
		{
			oneStopService.clearOrder(clearOSEOrderDataResult);
		}
		
		private function clearOSEOrderDataResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new ClearOSEOrderDataFollowUpEvent());
		}
				
		[Observer]
		public function updatePreVetValue(evt:UpdatePreVetValueEvent):void
		{
			var callbackFunction:Function = evt.callbackFunction;
			oneStopService.updatePreVetValue(evt.id, evt.oneStopSummary, evt.oneStopOrderInfo, function(evt:TideResultEvent):void 
			{
				if ((evt.result as String) != null)
				{
					evt.context.dispatchEvent(new ShowPreVetMsgEvent(evt.result as String));
				}
				callbackFunction();
			}, updatePreVetValueFailureHandle);
		}
	
		public function updatePreVetValueFailureHandle(evt:Event):void
		{
			dispatchEvent(new PreVetOperationCompletedFollowUpEvent(false));
		}
		
		[Observer]
		public function writeHkidUnknownDigitLog(evt:WriteHkidUnknownDigitLogEvent):void
		{
			oneStopService.writeHkidUnknownDigitLog(evt.hkid);
		}
		
		[Observer]
		public function printOSEPrescRpt(evt:PrintOSEPrescRptEvent):void
		{
			oneStopPrescRptService.printOSEPrescRptData(evt.orderDate, evt.moeFilterOption);
		}
	}
}