package hk.org.ha.event.pms.main.reftable.mp {
	
	import hk.org.ha.model.pms.vo.reftable.mp.CommonOrderStatusInfo;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveCommonOrderStatusListHandlerEvent extends AbstractTideEvent 
	{
		private var _commonOrderStatusInfo:CommonOrderStatusInfo;
		
		public function get commonOrderStatusInfo():CommonOrderStatusInfo
		{
			return _commonOrderStatusInfo;
		}
		
		public function RetrieveCommonOrderStatusListHandlerEvent(commonOrderStatusInfo:CommonOrderStatusInfo):void 
		{
			_commonOrderStatusInfo = commonOrderStatusInfo;
			super();
		}
	}
}