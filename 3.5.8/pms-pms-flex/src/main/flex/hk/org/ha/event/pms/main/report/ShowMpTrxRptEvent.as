package hk.org.ha.event.pms.main.report {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowMpTrxRptEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		public function ShowMpTrxRptEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}