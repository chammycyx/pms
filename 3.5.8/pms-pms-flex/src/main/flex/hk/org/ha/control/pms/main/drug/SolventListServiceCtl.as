package hk.org.ha.control.pms.main.drug {
		
	import hk.org.ha.event.pms.main.reftable.RetrieveSolventListEvent;
	import hk.org.ha.model.pms.biz.drug.SolventListServiceBean;
	import hk.org.ha.view.pms.main.vetting.mp.popup.MpOrderEditItemPopup;
	import hk.org.ha.view.pms.main.vetting.popup.OrderEditItemPopup;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("solventListServiceCtl", restrict="true")]
	public class SolventListServiceCtl 
	{
		[In]
		public var ctx:Context;
		
		[In]
		public var solventListService:SolventListServiceBean;
		
		private var obj:Object;
		
		[Observer]
		public function retrieveSolventList(evt:RetrieveSolventListEvent):void
		{
			obj = evt.obj;
			solventListService.retrieveSolventList(retrieveSolventListResult);
		}				
		
		public function retrieveSolventListResult(evt:TideResultEvent):void {
			if (obj != null && obj is OrderEditItemPopup)
			{				
				obj.setDefaultSolventListItem();
			}
			
			if (obj != null && obj is MpOrderEditItemPopup)
			{
				obj.setDefaultSolventListItem();
			}
		}
	}
}
