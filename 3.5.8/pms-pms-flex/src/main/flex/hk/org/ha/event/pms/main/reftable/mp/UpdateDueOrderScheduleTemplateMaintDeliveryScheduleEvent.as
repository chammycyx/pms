package hk.org.ha.event.pms.main.reftable.mp {
	
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.model.pms.persistence.medprofile.DeliverySchedule;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateDueOrderScheduleTemplateMaintDeliveryScheduleEvent extends AbstractTideEvent 
	{	
		private var _updateDeliverySchedule:DeliverySchedule;
		private var _updateDeliveryScheduleItemList:ArrayCollection;
		private var _callback:Function;
		
		public function UpdateDueOrderScheduleTemplateMaintDeliveryScheduleEvent(updateDeliverySchedule:DeliverySchedule, updateDeliveryScheduleItemList:ArrayCollection, callback:Function):void 
		{
			super();
			_updateDeliverySchedule = updateDeliverySchedule;
			_updateDeliveryScheduleItemList = updateDeliveryScheduleItemList;
			_callback = callback;
		}
		
		public function get updateDeliverySchedule():DeliverySchedule {
			return _updateDeliverySchedule;
		}
		
		public function get updateDeliveryScheduleItemList():ArrayCollection {
			return _updateDeliveryScheduleItemList;
		}
		
		public function get callback():Function {
			return _callback;
		}
		
	}
}