package hk.org.ha.event.pms.main.reftable.pivas {
	
	import hk.org.ha.model.pms.dms.persistence.DmDrug;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePivasDrugListBySolventCodeEvent extends AbstractTideEvent 
	{
		private var _solventCode:String;
		
		public function RetrievePivasDrugListBySolventCodeEvent(solventCode:String):void {
			_solventCode = solventCode;
			super();
		}
		
		public function get solventCode():String {
			return _solventCode;
		}
	}
}