package hk.org.ha.event.pms.main.report {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveMpTrxRptEvent extends AbstractTideEvent 
	{
		private var _batchCode:String;
		private var _dispenseDate:Date;
		
		public function RetrieveMpTrxRptEvent( batchCode:String, dispenseDate:Date ):void 
		{
			super();
			_batchCode = batchCode;
			_dispenseDate = dispenseDate;
		}
		
		public function get batchCode():String{
			return _batchCode;
		}
		
		public function get dispenseDate():Date{
			return _dispenseDate;
		}
	}
}