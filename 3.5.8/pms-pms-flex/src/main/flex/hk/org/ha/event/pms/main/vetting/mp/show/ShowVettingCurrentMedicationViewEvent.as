package hk.org.ha.event.pms.main.vetting.mp.show
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowVettingCurrentMedicationViewEvent extends AbstractTideEvent
	{
		public function ShowVettingCurrentMedicationViewEvent()
		{
			super();
		}
	}
}