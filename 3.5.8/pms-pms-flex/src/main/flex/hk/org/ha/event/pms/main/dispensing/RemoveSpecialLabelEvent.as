package hk.org.ha.event.pms.main.dispensing
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RemoveSpecialLabelEvent extends AbstractTideEvent 
	{		
		private var _labelNum:String;	
		
		public function RemoveSpecialLabelEvent(labelNum:String):void 
		{
			super();
			_labelNum = labelNum;
		}
		
		public function get labelNum():String {
			return _labelNum;
		}
	}
}