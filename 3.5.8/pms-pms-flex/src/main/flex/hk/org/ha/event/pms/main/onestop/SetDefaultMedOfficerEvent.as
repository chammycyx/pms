package hk.org.ha.event.pms.main.onestop {
		
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetDefaultMedOfficerEvent extends AbstractTideEvent 
	{			
		private var _proceed:Boolean;
		
		public function SetDefaultMedOfficerEvent(proceed:Boolean):void 
		{
			super();
			_proceed = proceed;
		}     
		
		public function get proceed():Boolean {
			return _proceed;
		}
	}
}