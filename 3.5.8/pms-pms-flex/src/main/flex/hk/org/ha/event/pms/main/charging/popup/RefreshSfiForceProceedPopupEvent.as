package hk.org.ha.event.pms.main.charging.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RefreshSfiForceProceedPopupEvent extends AbstractTideEvent 
	{		
		private var _errMsg:String;
		private var _errMsgDetail:String;
		private var _sfiForceProceedConfirmCallback:Function;
		
		public function RefreshSfiForceProceedPopupEvent(errMsg:String, errMsgDetail:String, sfiForceProceedConfirmCallback:Function):void 
		{
			super();
			_errMsg = errMsg;
			_errMsgDetail = errMsgDetail;
			_sfiForceProceedConfirmCallback = sfiForceProceedConfirmCallback;
		} 
		
		public function get errMsg():String{
			return _errMsg;
		}
		
		public function get errMsgDetail():String{
			return _errMsgDetail;
		}
		
		public function get sfiForceProceedConfirmCallback():Function{
			return _sfiForceProceedConfirmCallback;
		}
	}
}