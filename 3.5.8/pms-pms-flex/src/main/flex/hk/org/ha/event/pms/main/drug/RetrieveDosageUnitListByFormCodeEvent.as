package hk.org.ha.event.pms.main.drug {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrieveDosageUnitListByFormCodeEvent extends AbstractTideEvent 
	{
		private var _formCode:String;
		private var _callBackFunction:Function;		
		
		public function RetrieveDosageUnitListByFormCodeEvent(formCodeVal:String, functionValue:Function=null) {
			super();
			_formCode = formCodeVal;
			_callBackFunction = functionValue;
		}
		
		public function get formCode():String {
			return _formCode;
		}
		
		public function get callBackFunction():Function {
			return _callBackFunction;
		}
	}
}
