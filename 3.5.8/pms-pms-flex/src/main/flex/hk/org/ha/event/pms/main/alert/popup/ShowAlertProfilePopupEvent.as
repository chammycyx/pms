package hk.org.ha.event.pms.main.alert.popup {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.PatientEntity;
	import hk.org.ha.model.pms.vo.alert.AlertProfile;
	
	public class ShowAlertProfilePopupEvent extends AbstractTideEvent {

		private var _alertProfile:AlertProfile;
		
		private var _caseNum:String;
		
		private var _patHospCode:String;
		
		private var _patientEntity:PatientEntity;
		
		private var _callbackFunc:Function;
		
		public function ShowAlertProfilePopupEvent(alertProfile:AlertProfile, caseNum:String, patHospCode:String, patientEntity:PatientEntity, callbackFunc:Function=null) {
			super();
			_alertProfile  = alertProfile;
			_caseNum       = caseNum;
			_patHospCode   = patHospCode;
			_patientEntity = patientEntity;
			_callbackFunc  = callbackFunc;
		}
		
		public function get alertProfile():AlertProfile {
			return _alertProfile;
		}
		
		public function get caseNum():String {
			return _caseNum;
		}
		
		public function get patHospCode():String {
			return _patHospCode;
		}
		
		public function get patientEntity():PatientEntity {
			return _patientEntity;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}
