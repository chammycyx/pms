package hk.org.ha.event.pms.main.alert.ehr {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrieveEhrAllergyDetailEvent extends AbstractTideEvent {
		
		private var _patHospCode:String;
		
		private var _ehrNum:String;
		
		public function RetrieveEhrAllergyDetailEvent(patHospCode:String, ehrNum:String) {
			super();
			_patHospCode = patHospCode;
			_ehrNum = ehrNum;
		}
		
		public function get patHospCode():String {
			return _patHospCode;
		}
		
		public function get ehrNum():String {
			return _ehrNum;
		}
	}
}