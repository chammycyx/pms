package hk.org.ha.event.pms.main.vetting.mp.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePivasSupplyDateListInfoByDueDateEvent extends AbstractTideEvent 
	{		
		private var _dueDate:Date;
		private var _callbackFunc:Function;
		
		public function RetrievePivasSupplyDateListInfoByDueDateEvent(dueDate:Date, callbackFunc:Function):void 
		{
			super();
			_dueDate = dueDate;
			_callbackFunc = callbackFunc;
		}
		
		public function get dueDate():Date {
			return _dueDate;
		}			
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}