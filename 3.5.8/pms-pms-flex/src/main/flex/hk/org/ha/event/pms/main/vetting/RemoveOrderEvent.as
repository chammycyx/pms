package hk.org.ha.event.pms.main.vetting {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RemoveOrderEvent extends AbstractTideEvent {
		
		private var _logonUser:String;
		private var _remarkText:String;
		
		public function RemoveOrderEvent(logonUser:String=null, remarkText:String=null) {
			super();
			_logonUser = logonUser;
			_remarkText = remarkText;
		}

		public function get remarkText():String {
			return _remarkText;
		}
		
		public function get logonUser():String {
			return _logonUser;
		}
	}
}
