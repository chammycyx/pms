package hk.org.ha.event.pms.main.info.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowKeyGuidePopupEvent extends AbstractTideEvent 
	{	
		private var _callerScreen:String;
		
		public function ShowKeyGuidePopupEvent(callerScreen:String):void 
		{
			super();
			this._callerScreen = callerScreen;
		}
		
		public function get callerScreen():String
		{
			return _callerScreen;
		}
	}
}