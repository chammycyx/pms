package hk.org.ha.event.pms.main.onestop.popup {
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowYesNoMessagePopupEvent extends AbstractTideEvent 
	{	
		private var _messages:Array;
		private var _followUpYesFunc:Function;
		private var _followUpNoFunc:Function;
		
		public function ShowYesNoMessagePopupEvent(messages:Array, followUpYesFunc:Function, followUpNoFunc:Function):void 
		{
			super();
			_messages = messages;
			_followUpYesFunc = followUpYesFunc;
			_followUpNoFunc = followUpNoFunc;
		}
		
		public function get messages():Array {
			return _messages;
		}
		
		public function get followUpYesFunc():Function
		{
			return _followUpYesFunc;
		}
		
		public function get followUpNoFunc():Function
		{
			return _followUpNoFunc;
		}
	}
}