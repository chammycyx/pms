package hk.org.ha.event.pms.main.charging {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrieveMpSfiInvoiceItemListByPrivatePatListEvent extends AbstractTideEvent 
	{	
		private var _caseNum:String;
		private var _medProfileId:Number;
		
		public function RetrieveMpSfiInvoiceItemListByPrivatePatListEvent(caseNum:String, medProfileId:Number=NaN):void 
		{
			super();
			_caseNum = caseNum;
			_medProfileId = medProfileId;
		}
		
		public function get caseNum():String{
			return _caseNum;
		}
		
		public function get medProfileId():Number{
			return _medProfileId;
		}
	}
}