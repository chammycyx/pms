package hk.org.ha.control.pms.main.medprofile
{	
	import hk.org.ha.event.pms.main.vetting.mp.tpn.popup.DeleteTpnRecordEvent;
	import hk.org.ha.event.pms.main.vetting.mp.tpn.popup.DuplicateTpnRecordEvent;
	import hk.org.ha.event.pms.main.vetting.mp.tpn.popup.ModifyTpnRecordEvent;
	import hk.org.ha.event.pms.main.vetting.mp.tpn.popup.ModifyTpnRegimenEvent;
	import hk.org.ha.event.pms.main.vetting.mp.tpn.popup.NewTpnRecordEvent;
	import hk.org.ha.event.pms.main.vetting.mp.tpn.popup.ReadTpnRecordEvent;
	import hk.org.ha.event.pms.main.vetting.mp.tpn.popup.RetrieveTpnRequestListEvent;
	import hk.org.ha.event.pms.main.vetting.mp.tpn.popup.ViewTpnRecordEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.ShowLoadingPopupEvent;
	import hk.org.ha.model.pms.biz.medprofile.TpnRequestServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("tpnRequestServiceCtl", restrict="true")]	
	public class TpnRequestServiceCtl {
		[In]
		public var tpnRequestService:TpnRequestServiceBean;
		
		private var newTpnRecordEvent:NewTpnRecordEvent;
		
		private var modifyTpnRegimenEvent:ModifyTpnRegimenEvent;
		
		private var duplicateRecordEvent:DuplicateTpnRecordEvent;
		
		private var deleteTpnRecordEvent:DeleteTpnRecordEvent;
		
		private var readTpnRecordEvent:ReadTpnRecordEvent;
		
		private var viewTpnRecordEvent:ViewTpnRecordEvent;
		
		public function TpnRequestServiceCtl() {
		}
		
		[Observer]
		public function retrieveTpnRequestList(evt:RetrieveTpnRequestListEvent):void {
			tpnRequestService.retrieveTpnRequestList();
		}
		
		[Observer]
		public function newTpnRecord(evt:NewTpnRecordEvent):void {
			dispatchEvent(new ShowLoadingPopupEvent("Loading TPN ..."));
			newTpnRecordEvent = evt;
			tpnRequestService.newTpnRecord(evt.duration, newTpnRecordResult);
		}
		
		public function newTpnRecordResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			newTpnRecordEvent.callback(evt.result as String);
		}
		
		[Observer]
		public function modifyTpnRecord(evt:ModifyTpnRecordEvent):void {
			tpnRequestService.modifyTpnRecord(evt.duration, evt.selectedIndex);
		}
		
		[Observer]
		public function modifyTpnRegimen(evt:ModifyTpnRegimenEvent):void {
			dispatchEvent(new ShowLoadingPopupEvent("Loading TPN ..."));
			modifyTpnRegimenEvent = evt;
			tpnRequestService.modifyRegimen(evt.selectedIndex, modifyTpnRegimenResult);
		}
		
		public function modifyTpnRegimenResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			modifyTpnRegimenEvent.callback(evt.result as String);
		}
		
		[Observer]
		public function duplicateTpnRecord(evt:DuplicateTpnRecordEvent):void {
			dispatchEvent(new ShowLoadingPopupEvent("Loading TPN ..."));
			duplicateRecordEvent = evt;
			tpnRequestService.duplicateTpnRecord(evt.duration, evt.pnId, duplicateTpnRecordResult);
		}
		
		public function duplicateTpnRecordResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			duplicateRecordEvent.callback(evt.result as String);
		}
		
		[Observer]
		public function deleteTpnRecord(evt:DeleteTpnRecordEvent):void {
			dispatchEvent(new ShowLoadingPopupEvent("Loading TPN ..."));
			deleteTpnRecordEvent = evt;
			tpnRequestService.deleteTpnRecord(evt.selectedIndex, evt.deleteReason, deleteTpnRecordResult);
		}
		
		public function deleteTpnRecordResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			deleteTpnRecordEvent.callback(evt.result as Boolean);
		}
		
		[Observer]
		public function viewTpnRecord(evt:ViewTpnRecordEvent):void {
			dispatchEvent(new ShowLoadingPopupEvent("Loading TPN ..."));
			viewTpnRecordEvent = evt;
			tpnRequestService.viewTpnRecord(evt.selectedIndex, viewTpnRecordResult);
		}
		
		public function viewTpnRecordResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			viewTpnRecordEvent.callback(evt.result as String);
		}
		
		[Observer]
		public function readTpnRecord(evt:ReadTpnRecordEvent):void {
			dispatchEvent(new ShowLoadingPopupEvent("Loading TPN ..."));
			readTpnRecordEvent = evt;
			tpnRequestService.readTpnRecord(evt.pnId, evt.mode, evt.duration, evt.selectedIndex, readTpnRecordResult);
		}
		
		public function readTpnRecordResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			readTpnRecordEvent.callback(evt.result as Boolean);	
		}
	}
}