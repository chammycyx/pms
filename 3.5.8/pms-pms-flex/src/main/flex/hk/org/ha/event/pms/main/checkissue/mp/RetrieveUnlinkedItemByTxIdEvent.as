package hk.org.ha.event.pms.main.checkissue.mp
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveUnlinkedItemByTxIdEvent extends AbstractTideEvent 
	{
		private var _mpDispLabelQrCodeXml:String;
		
		public function RetrieveUnlinkedItemByTxIdEvent(mpDispLabelQrCodeXml:String):void 
		{
			super();
			_mpDispLabelQrCodeXml = mpDispLabelQrCodeXml;
		}
		
		public function get mpDispLabelQrCodeXml():String {
			return _mpDispLabelQrCodeXml;
		}

	}
}