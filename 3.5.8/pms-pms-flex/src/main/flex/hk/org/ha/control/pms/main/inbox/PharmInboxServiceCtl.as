package hk.org.ha.control.pms.main.inbox
{
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.event.pms.main.inbox.ConstructManualProfileEvent;
	import hk.org.ha.event.pms.main.inbox.CountCategoriesEvent;
	import hk.org.ha.event.pms.main.inbox.FindMedProfileListEvent;
	import hk.org.ha.event.pms.main.inbox.FinishConstructManualProfileEvent;
	import hk.org.ha.event.pms.main.inbox.FinishUpdateUrgentFlagInMedOrderEvent;
	import hk.org.ha.event.pms.main.inbox.ForceProcessPendingOrdersEvent;
	import hk.org.ha.event.pms.main.inbox.ManualTransferEvent;
	import hk.org.ha.event.pms.main.inbox.NotifyMdsEvent;
	import hk.org.ha.event.pms.main.inbox.ReceiveCancelDischargeTransferListEvent;
	import hk.org.ha.event.pms.main.inbox.ReceiveCategorySummaryEvent;
	import hk.org.ha.event.pms.main.inbox.ReceiveDischargeMedOrderListEvent;
	import hk.org.ha.event.pms.main.inbox.ReceiveErrorStatListEvent;
	import hk.org.ha.event.pms.main.inbox.ReceiveMedProfileListEvent;
	import hk.org.ha.event.pms.main.inbox.ReceiveMedProfileStatListEvent;
	import hk.org.ha.event.pms.main.inbox.ReceiveMedProfileTrxLogListEvent;
	import hk.org.ha.event.pms.main.inbox.ReceivePasPatientEvent;
	import hk.org.ha.event.pms.main.inbox.ReceiveSpecCodeListAndWardCodeListEvent;
	import hk.org.ha.event.pms.main.inbox.RetrieveCancelDischargeTransferListEvent;
	import hk.org.ha.event.pms.main.inbox.RetrieveErrorStatEvent;
	import hk.org.ha.event.pms.main.inbox.RetrieveMedProfileTrxLogListEvent;
	import hk.org.ha.event.pms.main.inbox.RetrievePasPatientEvent;
	import hk.org.ha.event.pms.main.inbox.RetrieveSpecCodeListAndWardCodeListEvent;
	import hk.org.ha.event.pms.main.inbox.UpdateDischargeShowAllWardsEvent;
	import hk.org.ha.event.pms.main.inbox.UpdateDischargeTypesAndFiltersEvent;
	import hk.org.ha.event.pms.main.inbox.UpdateErrorStatShowAllWardsEvent;
	import hk.org.ha.event.pms.main.inbox.UpdateMedProfileStatTypesEvent;
	import hk.org.ha.event.pms.main.inbox.UpdateShowChemoProfileOnlyEvent;
	import hk.org.ha.event.pms.main.inbox.UpdateStatShowAllWardsEvent;
	import hk.org.ha.event.pms.main.inbox.UpdateStatShowChemoProfileOnlyEvent;
	import hk.org.ha.event.pms.main.inbox.UpdateStatTypesAndFiltersEvent;
	import hk.org.ha.event.pms.main.inbox.UpdateUrgentFlagInMedOrderEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.ShowLoadingPopupEvent;
	import hk.org.ha.model.pms.biz.inbox.CancelDischargeTransferServiceBean;
	import hk.org.ha.model.pms.biz.inbox.CategorySummaryServiceBean;
	import hk.org.ha.model.pms.biz.inbox.DischargeMedOrderServiceBean;
	import hk.org.ha.model.pms.biz.inbox.ErrorStatServiceBean;
	import hk.org.ha.model.pms.biz.inbox.ManualProfileServiceBean;
	import hk.org.ha.model.pms.biz.inbox.MedProfileServiceBean;
	import hk.org.ha.model.pms.biz.inbox.MedProfileSupportServiceBean;
	import hk.org.ha.model.pms.biz.inbox.PharmInboxHistoryServiceBean;
	import hk.org.ha.model.pms.biz.inbox.PharmInboxServiceBean;
	import hk.org.ha.model.pms.persistence.disp.Patient;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	import hk.org.ha.model.pms.udt.medprofile.FindMedProfileListResult;
	import hk.org.ha.model.pms.udt.medprofile.MedProfileStatAdminType;
	import hk.org.ha.model.pms.udt.medprofile.MedProfileStatOrderType;
	
	import org.granite.tide.events.AbstractTideEvent;
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;

	[Bindable]
	[Name("pharmInboxServiceCtl", restrict="true")]
	public class PharmInboxServiceCtl
	{
		[In]
		public var ctx:Context;
		
		[In]
		public var pharmInboxService:PharmInboxServiceBean;
		
		[In]
		public var medProfileService:MedProfileServiceBean;
		
		[In]
		public var categorySummaryService:CategorySummaryServiceBean;
		
		[In]
		public var medProfileSupportService:MedProfileSupportServiceBean;
		
		[In]
		public var manualProfileService:ManualProfileServiceBean;
		
		[In]
		public var errorStatService:ErrorStatServiceBean;
		
		[In]
		public var pharmInboxHistoryService:PharmInboxHistoryServiceBean;
		
		[In]
		public var cancelDischargeTransferService:CancelDischargeTransferServiceBean;
		
		[In]
		public var dischargeMedOrderService:DischargeMedOrderServiceBean;
				
		public function PharmInboxServiceCtl()
		{
		}

		[Observer]
		public function setMedProfileStatTypes(evt:UpdateMedProfileStatTypesEvent):void
		{
			pharmInboxService.setMedProfileStatTypes(evt.adminType, evt.orderType, notifyMedProfileStatListUpdated);
		}
		
		[Observer]
		public function setShowAllWards(evt:UpdateStatShowAllWardsEvent):void
		{
			pharmInboxService.setShowAllWards(evt.showAllWards, notifyMedProfileStatListUpdated);
		}
		
		[Observer]
		public function setStatShowChemoProfileOnly(evt:UpdateStatShowChemoProfileOnlyEvent):void
		{
			pharmInboxService.setShowChemoProfileOnly(evt.showChemoProfileOnly, notifyMedProfileStatListUpdated);
		}
		
		[Observer]
		public function setWardAndWardGroupFilterList(evt:UpdateStatTypesAndFiltersEvent):void
		{
			pharmInboxService.setStatTypesAndFilters(evt.adminType, evt.orderType,
					evt.wardFilterList, evt.wardGroupFilterList, notifyMedProfileStatListUpdated);
		}
		
		public function notifyMedProfileStatListUpdated(evt:TideResultEvent):void 
		{
			dispatchEvent(new ReceiveCategorySummaryEvent());
			dispatchEvent(new ReceiveMedProfileStatListEvent());
		}
		
		[Observer]
		public function setShowChemoProfileOnly(evt:UpdateShowChemoProfileOnlyEvent):void
		{
			categorySummaryService.setShowChemoProfileOnly(evt.showChemoProfileOnly, notifyCategorySummaryUpdated);
		}
		
		[Observer]
		public function countCategories(evt:CountCategoriesEvent):void
		{
			categorySummaryService.countCategories(notifyCategorySummaryUpdated);
		}
		
		public function notifyCategorySummaryUpdated(evt:TideResultEvent):void
		{
			dispatchEvent(new ReceiveCategorySummaryEvent());
		}
		
		[Observer]
		public function findMedProfileList(evt:FindMedProfileListEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Loading PAS Information ..."));
			medProfileService.findMedProfileList(evt.hkidCaseNum, findMedProfileListResult, closePopup);
		}
		
		public function findMedProfileListResult(evt:TideResultEvent):void
		{
			dispatchEvent(new ReceiveMedProfileListEvent(evt.result as FindMedProfileListResult));
			dispatchEvent(new CloseLoadingPopupEvent());
		}
		
		[Observer]
		public function manualTransfer(evt:ManualTransferEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Updating profile ..."));			
			medProfileSupportService.manualTransfer(evt.medProfile, evt.transferFlag, evt.transferDate, evt.applyImmediately, evt.itemCutOffDate,
				function (resultEvt:TideResultEvent):void {
					dispatchEvent(new CloseLoadingPopupEvent());
					evt.callbackFunction(resultEvt.result);
				}, closePopup
			);
		}
		
		[Observer]
		public function forceProcessPendingOrders(evt:ForceProcessPendingOrdersEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Updating profile ..."));
			medProfileSupportService.forceProcessPendingOrders(evt.medProfile,
				function (resultEvt:TideResultEvent):void {
					dispatchEvent(new CloseLoadingPopupEvent());
					evt.callbackFunction();
				}, closePopup
			); 
		}
		
		[Observer]
		public function constructManualProfile(evt:ConstructManualProfileEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Loading PAS Information ..."));
			manualProfileService.constructManualProfile(evt.hkid, evt.caseNum, constructManualProfileResult, closePopup);
		}
		
		public function constructManualProfileResult(evt:TideResultEvent):void
		{
			dispatchEvent(new FinishConstructManualProfileEvent(evt.result as MedProfile));
			dispatchEvent(new CloseLoadingPopupEvent());
		}
		
		[Observer]
		public function retrievePasPatient(evt:RetrievePasPatientEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Loading PAS Information ..."));
			manualProfileService.retrievePasPatient(evt.hkid, retrievePasPatientResult, closePopup);
		}
		
		public function retrievePasPatientResult(evt:TideResultEvent):void
		{
			dispatchEvent(new ReceivePasPatientEvent(evt.result as Patient));
			dispatchEvent(new CloseLoadingPopupEvent());
		}
		
		[Observer]
		public function retrieveSpecCodeListAndWardCodeList(evt:RetrieveSpecCodeListAndWardCodeListEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Loading ..."));
			manualProfileService.retrieveSpecCodeListAndWardCodeList(retrieveSpecCodeListAndWardCodeListResult);
		}
		
		public function retrieveSpecCodeListAndWardCodeListResult(evt:TideResultEvent):void
		{
			dispatchEvent(new ReceiveSpecCodeListAndWardCodeListEvent());
			dispatchEvent(new CloseLoadingPopupEvent());
		}
		
		[Observer]
		public function retrieveErrorStatList(evt:RetrieveErrorStatEvent):void
		{
			errorStatService.retrieveErrorStatList(notifyErrorStatUpdated);
		}
		
		[Observer]
		public function setErrorStatShowAllWards(evt:UpdateErrorStatShowAllWardsEvent):void
		{
			errorStatService.setShowAllWards(evt.showAllWards, notifyErrorStatUpdated);
		}
		
		public function notifyErrorStatUpdated(evt:TideResultEvent):void
		{
			dispatchEvent(new ReceiveCategorySummaryEvent());
			dispatchEvent(new ReceiveErrorStatListEvent());
		}
		
		[Observer]
		public function notifyMds(evt:NotifyMdsEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Updating ..."));
			errorStatService.notifyMds(evt.errorStatId, evt.notified, closePopup, closePopup);
		}
		
		public function closePopup(evt:Event):void
		{
			dispatchEvent(new CloseLoadingPopupEvent());
		}
		
		[Observer]
		public function retrieveMedProfileTrxLogList(evt:RetrieveMedProfileTrxLogListEvent):void
		{
			pharmInboxHistoryService.retrieveMedProfileTrxLogList(receiveMedProfileTrxLogListResult);
		}
		
		public function receiveMedProfileTrxLogListResult(evt:TideResultEvent):void
		{
			dispatchEvent(new ReceiveMedProfileTrxLogListEvent());
		}
		
		[Observer]
		public function retrieveCancelDischargeTransferList(evt:RetrieveCancelDischargeTransferListEvent):void
		{
			cancelDischargeTransferService.retrieveCancelDischargeTransferList(retrieveCancelDischargeTransferListResult);
		}
		
		public function retrieveCancelDischargeTransferListResult(evt:TideResultEvent):void
		{
			dispatchEvent(new ReceiveCategorySummaryEvent());
			dispatchEvent(new ReceiveCancelDischargeTransferListEvent());
		}
		
		[Observer]
		public function setDischargeTypesAndFilters(evt:UpdateDischargeTypesAndFiltersEvent):void
		{
			dischargeMedOrderService.setDischargeTypesAndFilters(evt.listType, evt.wardFilterList, evt.wardGroupFilterList,
					evt.hkidCaseNum, notifyDischargeMedOrderListUpdated);
		}
		
		[Observer]
		public function setDischargeShowAllWards(evt:UpdateDischargeShowAllWardsEvent):void
		{
			dischargeMedOrderService.setShowAllWards(evt.showAllWards, notifyDischargeMedOrderListUpdated);
		}
		
		[Observer]
		public function setUrgent(evt:UpdateUrgentFlagInMedOrderEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Updating ..."));
			dischargeMedOrderService.setUrgent(evt.medOrderId, evt.urgent, evt.version, setUrgentResult, closePopup);
		}
		
		public function setUrgentResult(evt:TideResultEvent):void
		{
			dispatchEvent(new FinishUpdateUrgentFlagInMedOrderEvent(evt.result as String));
			dispatchEvent(new CloseLoadingPopupEvent());
		}
		
		public function notifyDischargeMedOrderListUpdated(evt:TideResultEvent):void
		{
			dispatchEvent(new ReceiveCategorySummaryEvent());
			dispatchEvent(new ReceiveDischargeMedOrderListEvent());
		}
	}
}