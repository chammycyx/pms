package hk.org.ha.event.pms.main.reftable.cmm {
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateCmmSpecialtyListEvent extends AbstractTideEvent {
		
		private var _cmmSpecialtyList:ListCollectionView;
		private var _callback:Function;
		
		public function UpdateCmmSpecialtyListEvent(cmmSpecialtyList:ListCollectionView, callback:Function) {
			super();
			_cmmSpecialtyList = cmmSpecialtyList;
			_callback = callback;
		}
		
		public function get cmmSpecialtyList():ListCollectionView {
			return _cmmSpecialtyList;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}
