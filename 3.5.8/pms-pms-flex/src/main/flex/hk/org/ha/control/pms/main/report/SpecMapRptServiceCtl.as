package hk.org.ha.control.pms.main.report {
	
	import hk.org.ha.event.pms.main.report.ClearSpecMapRptEvent;
	import hk.org.ha.event.pms.main.report.PrintSpecMapRptEvent;
	import hk.org.ha.event.pms.main.report.RefreshSpecMapRptEvent;
	import hk.org.ha.event.pms.main.report.RetrieveSpecMapRptEvent;
	import hk.org.ha.event.pms.main.report.ShowSpecMapRptEvent;
	import hk.org.ha.model.pms.biz.report.SpecMapRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("specMapRptServiceCtl", restrict="true")]
	public class SpecMapRptServiceCtl 
	{
		[In]
		public var specMapRptService:SpecMapRptServiceBean;
		
		[Observer]
		public function clearSpecMapRpt(evt:ClearSpecMapRptEvent):void
		{
			specMapRptService.clearSpecMapRpt(clearSpecMapRptResult);
		}
		
		private function clearSpecMapRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshSpecMapRptEvent());
		}
		
		[Observer]
		public function printSpecMapRpt(evt:PrintSpecMapRptEvent):void
		{
			specMapRptService.printSpecMapRpt();
		}
		
		[Observer]
		public function retrieveSpecMapRpt(evt:RetrieveSpecMapRptEvent):void
		{
			specMapRptService.retrieveSpecMapRpt(evt.pmsSpecMapReportCriteria, retrieveSpecMapRptListResult);
		}
		
		private function retrieveSpecMapRptListResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new ShowSpecMapRptEvent());
		}
		
	}
}
