package hk.org.ha.control.pms.main.delivery
{
	import flash.events.Event;
	import hk.org.ha.event.pms.main.delivery.AssignBatchAndPrintByDeliveryScheduleEvent;
	import hk.org.ha.event.pms.main.delivery.AssignBatchAndPrintEvent;
	import hk.org.ha.event.pms.main.delivery.FinishAssignBatchAndPrintByDeliveryScheduleEvent;
	import hk.org.ha.event.pms.main.delivery.FinishAssignBatchAndPrintEvent;
	import hk.org.ha.event.pms.main.delivery.FinishAssignBatchAndPrintForPivasEvent;
	import hk.org.ha.event.pms.main.delivery.ReceiveDueOrderDeliveryScheduleListEvent;
	import hk.org.ha.event.pms.main.delivery.ReceiveFollowUpItemListEvent;
	import hk.org.ha.event.pms.main.delivery.ReceiveMedProfileDueListEvent;
	import hk.org.ha.event.pms.main.delivery.ReceiveMedProfileItemDueListEvent;
	import hk.org.ha.event.pms.main.delivery.RefreshDueOrderLabelGenerationView;
	import hk.org.ha.event.pms.main.delivery.RetrieveDueOrderDeliveryScheduleListEvent;
	import hk.org.ha.event.pms.main.delivery.RetrieveFollowUpItemListEvent;
	import hk.org.ha.event.pms.main.delivery.RetrieveMaterialRequestItemListEvent;
	import hk.org.ha.event.pms.main.delivery.RetrieveMedProfileDueListEvent;
	import hk.org.ha.event.pms.main.delivery.RetrieveMedProfileItemDueListEvent;
	import hk.org.ha.event.pms.main.delivery.RetrieveOverdueSummaryEvent;
	import hk.org.ha.event.pms.main.delivery.RetrievePurchaseRequestListEvent;
	import hk.org.ha.event.pms.main.delivery.RetrieveTodayDeliveryListEvent;
	import hk.org.ha.event.pms.main.delivery.UpdateTodayWorkstoreDeliveryScheduleEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.ShowLoadingPopupEvent;
	import hk.org.ha.model.pms.biz.delivery.DeliveryListServiceBean;
	import hk.org.ha.model.pms.biz.delivery.DueOrderPrintServiceBean;
	import hk.org.ha.model.pms.biz.delivery.DueOrderScheduleServiceBean;
	import hk.org.ha.model.pms.biz.delivery.FollowUpItemListServiceBean;
	import hk.org.ha.model.pms.biz.delivery.MaterialRequestItemListServiceBean;
	import hk.org.ha.model.pms.biz.delivery.MedProfileDueListServiceBean;
	import hk.org.ha.model.pms.biz.delivery.OverdueSummaryServiceBean;
	import hk.org.ha.model.pms.biz.delivery.PurchaseRequestListServiceBean;
	import hk.org.ha.model.pms.biz.reftable.WardSelectionServiceBean;
	import hk.org.ha.model.pms.persistence.medprofile.DeliveryRequestGroup;
	import hk.org.ha.model.pms.persistence.medprofile.DeliverySchedule;
	import hk.org.ha.model.pms.udt.medprofile.DeliveryRequestGroupStatus;
	import hk.org.ha.model.pms.udt.medprofile.DeliveryRequestStatus;
	import hk.org.ha.model.pms.vo.delivery.ScheduleTemplateInfo;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;

	[Bindable]
	[Name("dueOrderPrintServiceCtl", restrict="true")]
	public class DueOrderPrintServiceCtl
	{
		[In]
		public var ctx:Context;
		
		[In]
		public var dueOrderPrintService:DueOrderPrintServiceBean;
		
		[In]
		public var dueOrderScheduleService:DueOrderScheduleServiceBean;
		
		[In]
		public var overdueSummaryService:OverdueSummaryServiceBean;
		
		[In]
		public var deliveryListService:DeliveryListServiceBean;
		
		[In]
		public var followUpItemListService:FollowUpItemListServiceBean;
		
		[In]
		public var purchaseRequestListService:PurchaseRequestListServiceBean;

		[In]
		public var materialRequestItemListService:MaterialRequestItemListServiceBean;
		
		[In]
		public var medProfileDueListService:MedProfileDueListServiceBean;
		
		[In]
		public var wardSelectionService:WardSelectionServiceBean; 
		
		private var processingAssignBatchAndPrint:Boolean;
		
		public function DueOrderPrintServiceCtl()
		{
		}
		
		[Observer]
		public function assignBatchAndPrintByDeliverySchedule(evt:AssignBatchAndPrintByDeliveryScheduleEvent):void
		{
			if (processingAssignBatchAndPrint) {
				return;
			}
			processingAssignBatchAndPrint = true;
			dispatchEvent(new ShowLoadingPopupEvent("Assigning Batch and Printing ..."));
			
			var continuousGroupProcessFunction:Function = function(resultEvt:TideResultEvent):void {
				processAssignBatchAndPrintRequestGroupResult(resultEvt, continuousGroupProcessFunction, FinishAssignBatchAndPrintByDeliveryScheduleEvent, evt.callbackFunction);
			};
			dueOrderPrintService.assignBatchAndPrintByDeliveryScheduleItem(evt.scheduleItem, evt.deliveryRequestList, continuousGroupProcessFunction, assignBatchAndPrintCompleted);
		}
		
		private function processAssignBatchAndPrintRequestGroupResult(evt:TideResultEvent, continuousGroupProcessFunction:Function, successEventClass:Class, callbackFunction:Function):void
		{			
			if (evt.result == DeliveryRequestGroupStatus.Processing) {
				dueOrderPrintService.retrieveDeliveryRequestGroupStatus(30000, continuousGroupProcessFunction, assignBatchAndPrintCompleted);
			} else {
				assignBatchAndPrintCompleted();
				if (evt.result == DeliveryRequestGroupStatus.Active) {
					if (callbackFunction != null) {
						callbackFunction();
					}
					dispatchEvent(new successEventClass());
				}
			}
		}
		
		[Observer]
		public function assignBatchAndPrint(evt:AssignBatchAndPrintEvent):void
		{
			if (processingAssignBatchAndPrint) {
				return;
			}
			processingAssignBatchAndPrint = true;
			var pivasFlag:Boolean = evt.request.pivasFlag;
			dispatchEvent(new ShowLoadingPopupEvent(pivasFlag ? "Processing Worklist generation ..." : "Assigning Batch and Printing ..."));
			
			var continuousProcessFunction:Function = function(resultEvt:TideResultEvent):void {
				processAssignBatchAndPrintStatusResult(resultEvt, continuousProcessFunction, pivasFlag ? FinishAssignBatchAndPrintForPivasEvent : FinishAssignBatchAndPrintEvent, evt.callbackFunction);
			};
			
			dueOrderPrintService.assignBatchAndPrint(evt.request, continuousProcessFunction, assignBatchAndPrintCompleted);
		}

		private function processAssignBatchAndPrintStatusResult(evt:TideResultEvent, continuousProcessFunction:Function, successEventClass:Class, callbackFunction:Function):void
		{
			if (evt.result == DeliveryRequestStatus.Processing) {
				dueOrderPrintService.retrieveAssignBatchAndPrintStatus(30000, continuousProcessFunction, assignBatchAndPrintCompleted);
			} else {
				assignBatchAndPrintCompleted();
				if (evt.result == DeliveryRequestStatus.Completed) {
					if (callbackFunction != null) {
						callbackFunction();
					}
					dispatchEvent(new successEventClass());
				}
			}
		}
		
		public function assignBatchAndPrintCompleted(evt:Event = null):void
		{
			processingAssignBatchAndPrint = false;
			dispatchEvent(new CloseLoadingPopupEvent());
		}
		
		[Observer]
		public function refreshDueOrderLabelGenerationView(evt:RefreshDueOrderLabelGenerationView):void
		{
			var processCount:int = 3;
			var resultFunction:Function = function(resultEvt:TideResultEvent):void {
				processCount--;
				if (processCount <= 0) {
					closePopup();
					if (evt.callbackFunction != null) {
						evt.callbackFunction();
					}
				}
			};
			dispatchEvent(new ShowLoadingPopupEvent("Refresh Item Count ..."));
			wardSelectionService.retrieveWardAndWardGroupList(resultFunction, closePopup);
			overdueSummaryService.countOverdueItems(false, false, resultFunction, closePopup);
			deliveryListService.retrieveTodayDeliveryList(resultFunction, closePopup);
		}
		
		[Observer]
		public function retrieveOverdueSummary(evt:RetrieveOverdueSummaryEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Refresh Item Count ..."));
			overdueSummaryService.countOverdueItems(evt.explict, evt.retrieveSfiItemCountOnly, closePopup, closePopup);
		}
		
		[Observer]
		public function retrieveTodayDeliveryList(evt:RetrieveTodayDeliveryListEvent):void
		{
			deliveryListService.retrieveTodayDeliveryList();
		}
		
		[Observer]
		public function retrieveFollowUpItemList(evt:RetrieveFollowUpItemListEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Loading Follow Up List ..."));
			followUpItemListService.retrieveFollowUpItemList(retrieveFollowUpItemListResult, closePopup);
		}
		
		public function retrieveFollowUpItemListResult(evt:TideResultEvent):void
		{
			dispatchEvent(new CloseLoadingPopupEvent());
			dispatchEvent(new ReceiveFollowUpItemListEvent());
		}
		
		[Observer]
		public function retrievePurchaseRequestList(evt:RetrievePurchaseRequestListEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Loading ..."));
			purchaseRequestListService.retrievePurchaseRequestList(evt.option,
				function(resultEvt:TideResultEvent):void
				{
					dispatchEvent(new CloseLoadingPopupEvent());
					if (evt.callbackFunction != null) {
						evt.callbackFunction(evt.option, resultEvt.result);
					}
				}, closePopup
			);
		}
		
		[Observer]
		public function retrieveMaterialRequestItemList(evt:RetrieveMaterialRequestItemListEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Loading ..."));
			materialRequestItemListService.retrieveMaterialRequestItemList(
				function(resultEvt:TideResultEvent):void
				{
					dispatchEvent(new CloseLoadingPopupEvent());
					if (evt.callbackFunction != null) {
						evt.callbackFunction(resultEvt.result);
					}
				}, closePopup
			);
		}
		
		[Observer]
		public function retreiveMedProfileDueList(evt:RetrieveMedProfileDueListEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Retrieve New Order List ..."));
			medProfileDueListService.retrieveMedProfileDueList(evt.caseNum, evt.hkid,
					retrieveMedProfileDueListResult, closePopup);
		}
		
		public function retrieveMedProfileDueListResult(evt:TideResultEvent):void
		{
			dispatchEvent(new CloseLoadingPopupEvent());
			dispatchEvent(new ReceiveMedProfileDueListEvent());
		}
			
		[Observer]
		public function retreiveMedProfileItemDueList(evt:RetrieveMedProfileItemDueListEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Retrieve New Order List ..."));
			medProfileDueListService.retrieveMedProfileItemDueList(evt.medProfileId,
				retrieveMedProfileItemDueListResult, closePopup);
		}
		
		public function retrieveMedProfileItemDueListResult(evt:TideResultEvent):void
		{
			dispatchEvent(new CloseLoadingPopupEvent());
			dispatchEvent(new ReceiveMedProfileItemDueListEvent());
		}
		
		private function closePopup(evt:Event = null):void
		{
			dispatchEvent(new CloseLoadingPopupEvent());
		}
		
		[Observer]
		public function retrieveDueOrderDeliveryScheduleList(evt:RetrieveDueOrderDeliveryScheduleListEvent):void{
			dispatchEvent(new ShowLoadingPopupEvent("Retrieve Schedule Template ..."));
			dueOrderScheduleService.retrieveDeliveryScheduleList(retrieveDueOrderDeliveryScheduleListResult);
		}
		
		public function retrieveDueOrderDeliveryScheduleListResult(evt:TideResultEvent):void{
			dispatchEvent(new CloseLoadingPopupEvent());
			dispatchEvent(new ReceiveDueOrderDeliveryScheduleListEvent(evt.result as ScheduleTemplateInfo));
		}
		
		[Observer]
		public function changeDeliveryScheduleTemplate(evt:UpdateTodayWorkstoreDeliveryScheduleEvent):void{
			dispatchEvent(new ShowLoadingPopupEvent("Setting Default Template ..."));
			dueOrderScheduleService.changeDeliveryScheduleTemplate(evt.deliverySchedule.name, changeDeliveryScheduleTemplateResult);
		}
		
		public function changeDeliveryScheduleTemplateResult(evt:TideResultEvent):void{
			dispatchEvent(new CloseLoadingPopupEvent());
			dispatchEvent(new ReceiveDueOrderDeliveryScheduleListEvent(evt.result as ScheduleTemplateInfo));
		}
	}
}