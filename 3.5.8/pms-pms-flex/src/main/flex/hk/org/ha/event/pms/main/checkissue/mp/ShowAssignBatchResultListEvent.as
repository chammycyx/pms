package hk.org.ha.event.pms.main.checkissue.mp
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowAssignBatchResultListEvent extends AbstractTideEvent 
	{			
		private var _assignBatchResultList:ArrayCollection;
		
		public function ShowAssignBatchResultListEvent(assignBatchResultList:ArrayCollection):void 
		{
			super();
			_assignBatchResultList = assignBatchResultList;
		}
		
		public function get assignBatchResultList():ArrayCollection {
			return _assignBatchResultList;
		}
	}
}