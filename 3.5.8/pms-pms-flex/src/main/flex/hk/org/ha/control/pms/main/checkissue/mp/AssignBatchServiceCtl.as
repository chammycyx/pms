package hk.org.ha.control.pms.main.checkissue.mp{
	
	import hk.org.ha.event.pms.main.checkissue.mp.AssignBatchEvent;
	import hk.org.ha.event.pms.main.checkissue.mp.RetrieveAssignBatchSummaryEvent;
	import hk.org.ha.event.pms.main.checkissue.mp.RetrieveUnlinkedItemByTxIdEvent;
	import hk.org.ha.event.pms.main.checkissue.mp.RetrieveUnlinkedItemListEvent;
	import hk.org.ha.event.pms.main.checkissue.mp.SetAssignBatchSummaryEvent;
	import hk.org.ha.event.pms.main.checkissue.mp.SetAssignBatchSummaryItemEvent;
	import hk.org.ha.event.pms.main.checkissue.mp.ShowAssignBatchResultEvent;
	import hk.org.ha.event.pms.main.checkissue.mp.ShowAssignBatchResultListEvent;
	import hk.org.ha.model.pms.biz.checkissue.mp.AssignBatchServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("assignBatchServiceCtl", restrict="true")]
	public class AssignBatchServiceCtl 
	{
		[In]
		public var assignBatchService:AssignBatchServiceBean;
		
		private var checkFlag:Boolean;
		
		private var wardCode: String;
		
		private var batchDate: String;
		
		[Observer]
		public function retrieveUnlinkedItemByTxId(evt:RetrieveUnlinkedItemByTxIdEvent):void
		{
			In(Object(assignBatchService).msgCode)
			assignBatchService.retrieveUnlinkedItemByTrxId(evt.mpDispLabelQrCodeXml, retrieveUnlinkedItemByTxIdResult);
		}
		
		private function retrieveUnlinkedItemByTxIdResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new SetAssignBatchSummaryItemEvent(Object(assignBatchService).msgCode));
		}
		
		[Observer]
		public function retrieveAssignBatchSummary(evt:RetrieveAssignBatchSummaryEvent):void
		{
			assignBatchService.retrieveAssignBatchSummary(evt.batchDate, evt.batchNum, retrieveAssignBatchSummaryResult);
		}
		
		private function retrieveAssignBatchSummaryResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new SetAssignBatchSummaryEvent());
		}
		
		[Observer]
		public function assignBatch(evt:AssignBatchEvent):void
		{
			checkFlag = evt.checkFlag;
			wardCode = evt.wardCode;
			batchDate = evt.batchDate.toDateString();
			if ( evt.callByUnlinkItemList ) {
				assignBatchService.assignBatchByUnlinkItemList(evt.assignBatchItemList, evt.printTrxRptFlag, evt.checkFlag, evt.assignBatch,evt.batchNum,evt.batchDate, assignBatchByUnlinkItemListResult);
			} else {
				assignBatchService.assignBatch(evt.wardCode, evt.assignBatchItemList, evt.printTrxRptFlag, evt.checkFlag, evt.assignBatch,evt.batchNum,evt.batchDate, assignBatchResult);
			}
		}
		
		private function assignBatchResult(evt:TideResultEvent):void {
			if ( checkFlag ) {
				evt.context.dispatchEvent(new ShowAssignBatchResultEvent("0345",batchDate, evt.result as String, wardCode));
			} else {
				evt.context.dispatchEvent(new ShowAssignBatchResultEvent("0346",batchDate, evt.result as String, wardCode));
			}
		}
		
		private function assignBatchByUnlinkItemListResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new ShowAssignBatchResultListEvent(evt.result as ArrayCollection));
		}
	}
}
