package hk.org.ha.event.pms.main.vetting.mp.show
{
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowVettingEditItemViewEvent extends AbstractTideEvent
	{
		private var _medProfileItem:MedProfileItem;		
		private var _medProfileMoItem:MedProfileMoItem;
		private var _showLabelPreviewFlag:Boolean;
		private var _clearMessages:Boolean;
		
		public function ShowVettingEditItemViewEvent(medProfileItemValue:MedProfileItem, medProfileMoItemValue:MedProfileMoItem=null, showLabelPreviewFlag:Boolean=false, clearMessages:Boolean=true)
		{
			super();
			_medProfileItem 	  = medProfileItemValue;
			_medProfileMoItem	  = medProfileMoItemValue;
			_showLabelPreviewFlag = showLabelPreviewFlag;
			_clearMessages 		  = clearMessages;
		}
		
		public function get medProfileItem():MedProfileItem
		{
			return _medProfileItem;
		}
		
		public function get medProfileMoItem():MedProfileMoItem
		{
			return _medProfileMoItem;
		}
		
		public function get showLabelPreviewFlag():Boolean
		{
			return _showLabelPreviewFlag;
		}
		
		public function get clearMessages():Boolean
		{
			return _clearMessages;
		}
	}
}