package hk.org.ha.event.pms.main.reftable {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.udt.patient.PasSpecialtyType;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveRouteFormSortSpecListEvent extends AbstractTideEvent 
	{
		private var _specCode:String;
		private var _specType:PasSpecialtyType;
		
		public function RetrieveRouteFormSortSpecListEvent(specCode:String, specType:PasSpecialtyType):void 
		{
			super();
			_specCode = specCode;
			_specType = specType;
		}
		
		public function get specCode():String
		{
			return _specCode;
		}
		
		public function get specType():PasSpecialtyType
		{
			return _specType;
		}
		
	}
}