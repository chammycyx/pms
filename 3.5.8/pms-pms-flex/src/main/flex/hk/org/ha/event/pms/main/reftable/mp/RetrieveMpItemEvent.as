package hk.org.ha.event.pms.main.reftable.mp {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveMpItemEvent extends AbstractTideEvent 
	{
		private var _itemCode:String;
		
		public function RetrieveMpItemEvent(itemCode:String):void 
		{
			super();
			_itemCode = itemCode;
		}
		
		public function get itemCode():String
		{
			return _itemCode;
		}
	}
}