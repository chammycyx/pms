package hk.org.ha.event.pms.main.checkissue
{
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SendTicketListToIqdsEvent extends AbstractTideEvent 
	{		
		private var _iqdsMessageContentList:ListCollectionView;
		private var _ledCheckedList:ListCollectionView;
		private var _issueWindowNum:Number;
		
		
		public function SendTicketListToIqdsEvent(iqdsMessageContentList:ListCollectionView, ledCheckedList:ListCollectionView, issueWindowNum:Number):void 
		{
			super();
			_iqdsMessageContentList = iqdsMessageContentList;
			_ledCheckedList = ledCheckedList;
			_issueWindowNum = issueWindowNum;
		}
		
		public function get iqdsMessageContentList():ListCollectionView
		{
			return _iqdsMessageContentList;
		}
		
		public function get ledCheckedList():ListCollectionView
		{
			return _ledCheckedList;
		}
		
		public function get issueWindowNum():Number
		{
			return _issueWindowNum;
		}

	}
}