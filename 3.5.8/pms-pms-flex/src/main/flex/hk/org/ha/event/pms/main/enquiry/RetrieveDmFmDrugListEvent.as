package hk.org.ha.event.pms.main.enquiry
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDmFmDrugListEvent extends AbstractTideEvent 
	{		
		private var _itemCodePrefix:String;
		private var _showLookup:Boolean;
		
		public function RetrieveDmFmDrugListEvent(itemCodePrefix:String, showLookup:Boolean=false):void 
		{
			super();
			_itemCodePrefix = itemCodePrefix;
			_showLookup = showLookup;
		}
		
		public function get itemCodePrefix():String
		{
			return _itemCodePrefix;
		}
		
		public function get showLookup():Boolean
		{
			return _showLookup;
		}

	}
}