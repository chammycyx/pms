package hk.org.ha.event.pms.main.onestop {
	
	import hk.org.ha.view.pms.main.onestop.popup.OneStopEntryPrescriptionSummaryPopup;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePrescriptionDispOrderResultEvent extends AbstractTideEvent 
	{		
		private var _oneStopMedOrderItemList:ArrayCollection;
		private var _oneStopEntryPrescriptionSummaryPopup:OneStopEntryPrescriptionSummaryPopup;
		
		public function RetrievePrescriptionDispOrderResultEvent(oneStopMedOrderItemList:ArrayCollection,
																 oneStopEntryPrescriptionSummaryPopup:OneStopEntryPrescriptionSummaryPopup):void 
		{
			super();
			_oneStopMedOrderItemList = oneStopMedOrderItemList;
			_oneStopEntryPrescriptionSummaryPopup = oneStopEntryPrescriptionSummaryPopup;
		}
		
		public function get oneStopMedOrderItemList():ArrayCollection {
			return _oneStopMedOrderItemList;
		}
		
		public function get oneStopEntryPrescriptionSummaryPopup():OneStopEntryPrescriptionSummaryPopup {
			return _oneStopEntryPrescriptionSummaryPopup;
		}
	}
}