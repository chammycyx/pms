package hk.org.ha.control.pms.main.pivas{
	
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.event.pms.main.pivas.AddToWorklistEvent;
	import hk.org.ha.event.pms.main.pivas.DeleteFromWorklistEvent;
	import hk.org.ha.event.pms.main.pivas.RecalculatePivasInfoEvent;
	import hk.org.ha.event.pms.main.pivas.RetrievePivasDueOrderListEvent;
	import hk.org.ha.event.pms.main.pivas.RetrievePivasFindPatientListEvent;
	import hk.org.ha.event.pms.main.pivas.RetrievePivasFormulaByIdEvent;
	import hk.org.ha.event.pms.main.pivas.RetrievePivasFormulaListByDrugKeyEvent;
	import hk.org.ha.event.pms.main.pivas.RetrievePivasFormulaMethodInfoByIdEvent;
	import hk.org.ha.event.pms.main.pivas.RetrievePivasInboxListEvent;
	import hk.org.ha.event.pms.main.pivas.RetrievePivasPrepDetailInfoEvent;
	import hk.org.ha.event.pms.main.pivas.RetrievePivasSupplyDateListInfoEvent;
	import hk.org.ha.event.pms.main.pivas.RetrievePivasWorklistListEvent;
	import hk.org.ha.event.pms.main.pivas.UpdatePivasWorklistEvent;
	import hk.org.ha.event.pms.main.pivas.show.ShowPivasPrepDetailViewEvent;
	import hk.org.ha.model.pms.biz.pivas.PivasWorklistServiceBean;
	import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormula;
	import hk.org.ha.model.pms.persistence.pivas.PivasWorklist;
	import hk.org.ha.model.pms.udt.pivas.PivasWorklistType;
	import hk.org.ha.model.pms.vo.pivas.worklist.CalculatePivasInfoCriteria;
	import hk.org.ha.model.pms.vo.pivas.worklist.PivasDueOrderListInfo;
	import hk.org.ha.model.pms.vo.pivas.worklist.PivasFormulaMethodInfo;
	import hk.org.ha.model.pms.vo.pivas.worklist.PivasPrepDetailInfo;
	import hk.org.ha.model.pms.vo.pivas.worklist.PivasSupplyDateListInfo;
	import hk.org.ha.model.pms.vo.pivas.worklist.PivasWorklistResponse;
	import hk.org.ha.view.pms.main.pivas.PivasWorklistUiInfo;
	
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("pivasWorklistServiceCtl", restrict="true")]
	public class PivasWorklistServiceCtl 
	{
		[In]
		public var pivasWorklistService:PivasWorklistServiceBean;
		
		private var callbackFunc:Function;

		private var inboxRetrieveCallbackFunc:Function;

		private var dueOrderRetrieveCallbackFunc:Function;
		
		private var worklistRetrieveCallbackFunc:Function;

		private var findPatientRetrieveCallbackFunc:Function;

		private var supplyDateRetrieveCallbackFunc:Function;
		
		private var retrievePivasPrepDetailInfoEvent:RetrievePivasPrepDetailInfoEvent;
		
		private var sourceType:PivasWorklistType;
		
		private var calculatePivasInfoCriteria:CalculatePivasInfoCriteria;
		
		private var tempAddToWorklist:Boolean;
		
		[Observer]
		public function retrievePivasInboxList(evt:RetrievePivasInboxListEvent):void {
			inboxRetrieveCallbackFunc = evt.callbackFunc;
			pivasWorklistService.retrievePivasInboxList(retrievePivasInboxListResult, retrievePivasInboxListError);
		}
		
		private function retrievePivasInboxListResult(evt:TideResultEvent):void {
			inboxRetrieveCallbackFunc(evt.result as ArrayCollection);
		}

		private function retrievePivasInboxListError(evt:TideFaultEvent):void {
			// resume procesing flag
			PivasWorklistUiInfo.processingInboxTabFlag = false;
		}
		
		[Observer]
		public function retrievePivasDueOrderList(evt:RetrievePivasDueOrderListEvent):void {
			dueOrderRetrieveCallbackFunc = evt.callbackFunc;
			pivasWorklistService.retrievePivasDueOrderList(evt.supplyDate, retrievePivasDueOrderListResult, retrievePivasDueOrderListError);
		}
		
		private function retrievePivasDueOrderListResult(evt:TideResultEvent):void {
			dueOrderRetrieveCallbackFunc(evt.result as PivasDueOrderListInfo);
		}

		private function retrievePivasDueOrderListError(evt:TideFaultEvent):void {
			// resume procesing flag
			PivasWorklistUiInfo.processingDueOrderTabFlag = false;
		}
		
		[Observer]
		public function retrievePivasWorklistList(evt:RetrievePivasWorklistListEvent):void {
			worklistRetrieveCallbackFunc = evt.callbackFunc;
			pivasWorklistService.retrievePivasWorklistList(retrievePivasWorklistListResult, retrievePivasWorklistListError);
		}
		
		private function retrievePivasWorklistListResult(evt:TideResultEvent):void {
			worklistRetrieveCallbackFunc(evt.result as ArrayCollection);
		}
		
		private function retrievePivasWorklistListError(evt:TideFaultEvent):void {
			// resume procesing flag
			PivasWorklistUiInfo.processingWorklistTabFlag = false;
		}

		[Observer]
		public function retrievePivasFindPatientList(evt:RetrievePivasFindPatientListEvent):void {
			findPatientRetrieveCallbackFunc = evt.callbackFunc;
			pivasWorklistService.retrievePivasFindPatientList(evt.searchKey, retrievePivasFindPatientListResult, retrievePivasFindPatientListError);
		}
		
		private function retrievePivasFindPatientListResult(evt:TideResultEvent):void {
			findPatientRetrieveCallbackFunc(evt.result as ArrayCollection);
		}

		private function retrievePivasFindPatientListError(evt:TideFaultEvent):void {
			// resume procesing flag
			PivasWorklistUiInfo.processingFindPatientTabFlag = false;
		}
		
		[Observer]
		public function retrievePivasSupplyDateListInfo(evt:RetrievePivasSupplyDateListInfoEvent):void {
			supplyDateRetrieveCallbackFunc = evt.callbackFunc;
			pivasWorklistService.retrievePivasSupplyDateListInfo(retrievePivasSupplyDateListInfoResult);
		}
		
		private function retrievePivasSupplyDateListInfoResult(evt:TideResultEvent):void {
			supplyDateRetrieveCallbackFunc(evt.result as PivasSupplyDateListInfo);
		}
		
		[Observer]
		public function addToWorklist(evt:AddToWorklistEvent):void {
			callbackFunc = evt.callbackFunc;
			sourceType = evt.sourceType;
			pivasWorklistService.addToWorklist(evt.pivasWorklistList, addToWorklistResult, addToWorklistError);
		}
		
		private function addToWorklistResult(evt:TideResultEvent):void {
			callbackFunc(evt.result as PivasWorklistResponse);
		}
		
		private function addToWorklistError(evt:TideFaultEvent):void {
			// resume procesing flag
			if (sourceType == PivasWorklistType.Inbox) {
				PivasWorklistUiInfo.processingInboxTabFlag = false;
			} else if (sourceType == PivasWorklistType.DueOrderList) {
				PivasWorklistUiInfo.processingDueOrderTabFlag = false;
			}
		}
		
		[Observer]
		public function deleteFromWorklist(evt:DeleteFromWorklistEvent):void {
			callbackFunc = evt.callbackFunc;
			pivasWorklistService.deleteFromWorklist(evt.pivasWorklistList, deleteFromWorklistResult, deleteFromWorklistError);
		}

		private function deleteFromWorklistResult(evt:TideResultEvent):void {
			callbackFunc(evt.result as PivasWorklistResponse);
		}
		
		private function deleteFromWorklistError(evt:TideFaultEvent):void {
			// resume procesing flag
			PivasWorklistUiInfo.processingWorklistTabFlag = false;
		}
		
		[Observer]
		public function retrievePivasPrepDetailInfo(evt:RetrievePivasPrepDetailInfoEvent):void {
			retrievePivasPrepDetailInfoEvent = evt;
			pivasWorklistService.retrievePivasPrepDetailInfo(evt.pivasWorklist.medProfile.id, evt.pivasWorklist.medProfile.version, evt.pivasWorklist.medProfileMoItem.id, 
				evt.pivasWorklist.pivasPrepMethod.pivasFormulaMethodId, evt.pivasWorklist.firstDoseMaterialRequestId, retrievePivasPrepDetailInfoResult);
		}
		
		private function retrievePivasPrepDetailInfoResult(evt:TideResultEvent):void {
			var showPivasPrepDetailViewEvent:ShowPivasPrepDetailViewEvent = new ShowPivasPrepDetailViewEvent();
			
			showPivasPrepDetailViewEvent.sourceType = retrievePivasPrepDetailInfoEvent.sourceType;
			showPivasPrepDetailViewEvent.pivasWorklist = retrievePivasPrepDetailInfoEvent.pivasWorklist;
			showPivasPrepDetailViewEvent.pivasPrepDetailInfo = evt.result as PivasPrepDetailInfo;
			
			dispatchEvent(showPivasPrepDetailViewEvent);
		}

		[Observer]
		public function retrievePivasFormulaListByDrugKey(evt:RetrievePivasFormulaListByDrugKeyEvent):void {
			callbackFunc = evt.callbackFunc;
			pivasWorklistService.retrievePivasFormulaListByDrugKey(evt.patHospCode, evt.pasWardCode, evt.drugKey, evt.dosageUnit,
				evt.siteCode, evt.diluentCode, evt.strength, retrievePivasFormulaListByDrugKeyResult);
		}
		
		private function retrievePivasFormulaListByDrugKeyResult(evt:TideResultEvent):void {
			callbackFunc(evt.result as ArrayCollection);
		}

		[Observer]
		public function retrievePivasFormulaById(evt:RetrievePivasFormulaByIdEvent):void {
			callbackFunc = evt.callbackFunc;
			pivasWorklistService.retrievePivasFormulaById(evt.pivasFormulaId, retrievePivasFormulaByIdResult);
		}
		
		private function retrievePivasFormulaByIdResult(evt:TideResultEvent):void {
			callbackFunc(evt.result as PivasFormula);
		}
		
		[Observer]
		public function retrievePivasFormulaMethodInfoById(evt:RetrievePivasFormulaMethodInfoByIdEvent):void {
			callbackFunc = evt.callbackFunc;
			pivasWorklistService.retrievePivasFormulaMethodInfoById(evt.pivasFormulaMethodId, retrievePivasFormulaMethodInfoByIdResult);
		}
		
		private function retrievePivasFormulaMethodInfoByIdResult(evt:TideResultEvent):void {
			callbackFunc(evt.result as PivasFormulaMethodInfo);
		}
		
		[Observer]
		public function recalculatePivasInfo(evt:RecalculatePivasInfoEvent):void {
			calculatePivasInfoCriteria = evt.calculatePivasInfoCriteria;
			callbackFunc = evt.callbackFunc;
			pivasWorklistService.recalculatePivasInfo(evt.calculatePivasInfoCriteria, recalculatePivasInfoResult);
		}
		
		private function recalculatePivasInfoResult(evt:TideResultEvent):void {
			callbackFunc(evt.result as PivasWorklist, calculatePivasInfoCriteria);
		}

		[Observer]
		public function updatePivasWorklist(evt:UpdatePivasWorklistEvent):void {
			tempAddToWorklist = evt.addToWorklist; 
			sourceType = evt.sourceType;
			callbackFunc = evt.callbackFunc;
			pivasWorklistService.updatePivasWorklist(evt.pivasWorklist, evt.addToWorklist, evt.sourceType, updatePivasWorklistResult);
		}
		
		private function updatePivasWorklistResult(evt:TideResultEvent):void {
			callbackFunc(evt.result as PivasWorklistResponse, tempAddToWorklist);
		}
	}
}
