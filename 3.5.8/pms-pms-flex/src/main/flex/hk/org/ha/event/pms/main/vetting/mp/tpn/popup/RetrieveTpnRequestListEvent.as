package hk.org.ha.event.pms.main.vetting.mp.tpn.popup
{
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;

	public class RetrieveTpnRequestListEvent extends AbstractTideEvent {	
		public function RetrieveTpnRequestListEvent() {
			super();
		}
	}
}