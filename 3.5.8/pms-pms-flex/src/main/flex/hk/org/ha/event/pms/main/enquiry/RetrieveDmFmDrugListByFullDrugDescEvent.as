package hk.org.ha.event.pms.main.enquiry
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDmFmDrugListByFullDrugDescEvent extends AbstractTideEvent 
	{		
		private var _fullDrugDescPrefix:String;
		private var _showLookup:Boolean;
		
		public function RetrieveDmFmDrugListByFullDrugDescEvent(fullDrugDescPrefix:String, showLookup:Boolean=false):void 
		{
			super();
			_fullDrugDescPrefix = fullDrugDescPrefix;
			_showLookup = showLookup;
		}

		public function get fullDrugDescPrefix():String
		{
			return _fullDrugDescPrefix;
		}
		
		public function get showLookup():Boolean
		{
			return _showLookup;
		}
	}
}