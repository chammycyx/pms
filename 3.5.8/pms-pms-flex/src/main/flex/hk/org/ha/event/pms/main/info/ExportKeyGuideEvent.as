package hk.org.ha.event.pms.main.info {
	
	import hk.org.ha.view.pms.main.info.popup.KeyGuidePopup;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ExportKeyGuideEvent extends AbstractTideEvent 
	{	
		private var _exportKeyList:ArrayCollection;
		private var _keyGuidePopup:KeyGuidePopup;
		
		public function ExportKeyGuideEvent(exportKeyList:ArrayCollection, keyGuidePopup:KeyGuidePopup):void 
		{
			super();
			this._exportKeyList = exportKeyList;
			this._keyGuidePopup = keyGuidePopup;
		}
		
		public function get exportKeyList():ArrayCollection
		{
			return _exportKeyList;
		}
		
		public function get keyGuidePopup():KeyGuidePopup
		{
			return _keyGuidePopup;
		}
	}
}