package hk.org.ha.control.pms.support{
	
	import hk.org.ha.event.pms.main.support.RefreshPropDescGridEvent;
	import hk.org.ha.event.pms.main.support.RetrieveHospitalListEvent;
	import hk.org.ha.event.pms.main.support.RetrieveOperationProfileListEvent;
	import hk.org.ha.event.pms.main.support.RetrievePropListEvent;
	import hk.org.ha.event.pms.main.support.RetrieveWorkstoreListEvent;
	import hk.org.ha.event.pms.main.support.SetHospitalListEvent;
	import hk.org.ha.event.pms.main.support.SetOperationProfileListEvent;
	import hk.org.ha.event.pms.main.support.SetWorkstoreListEvent;
	import hk.org.ha.event.pms.main.support.UpdatePropListEvent;
	import hk.org.ha.model.pms.biz.support.PropListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("propListServiceCtl", restrict="true")]
	public class PropListServiceCtl 
	{
		[In]
		public var propListService:PropListServiceBean;
		
		[Observer]
		public function retrieveHospitalList(evt:RetrieveHospitalListEvent):void
		{
			propListService.retrieveHospitalList(retrieveHospitalListResult);
		}
		
		private function retrieveHospitalListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent( new SetHospitalListEvent(evt.result as ArrayCollection) );
		}
		
		[Observer]
		public function retrievePropList(evt:RetrievePropListEvent):void
		{
			propListService.retrievePropList(evt.propType, evt.hospital, evt.operationProfileIdd, retrievePropListResult);
		}
		
		private function retrievePropListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshPropDescGridEvent() );
		}
		
		[Observer]
		public function updatePropList(evt:UpdatePropListEvent):void
		{
			propListService.updatePropList(evt.propList, retrievePropListResult);
		}
		
		[Observer]
		public function retrieveWorkstoreList(evt:RetrieveWorkstoreListEvent):void
		{
			propListService.retrieveWorkstoreList(retrieveWorkstoreListResult);
		}
		
		private function retrieveWorkstoreListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent( new SetWorkstoreListEvent(evt.result as ArrayCollection) );
		}
		
		[Observer]
		public function retrieveOperationProfileList(evt:RetrieveOperationProfileListEvent):void
		{
			propListService.retrieveOperationProfileList(evt.hospCode, evt.workstoreCode, retrieveOperationProfileListResult);
		}
		
		private function retrieveOperationProfileListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent( new SetOperationProfileListEvent(evt.result as ArrayCollection) );
		}
	}
}
