package hk.org.ha.event.pms.main.reftable.mp {
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshAomScheduleMaintViewEvent extends AbstractTideEvent 
	{	
		private var _action:String;
		private var _aomScheduleList:ArrayCollection;
		
		public function RefreshAomScheduleMaintViewEvent(action:String, aomScheduleList:ArrayCollection=null):void 
		{
			super();
			_action = action;
			_aomScheduleList = aomScheduleList;
		}
		
		public function get action():String {
			return _action;
		}
		
		public function get aomScheduleList():ArrayCollection {
			return _aomScheduleList;
		}
	}
}