package hk.org.ha.event.pms.main.reftable.pivas {
	
	import hk.org.ha.model.pms.dms.persistence.DmDrug;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePivasDrugListBySolventCodeHandlerEvent extends AbstractTideEvent 
	{
		private var _pivasDrugList:ArrayCollection;
		
		public function RetrievePivasDrugListBySolventCodeHandlerEvent(pivasDrugList:ArrayCollection):void {
			_pivasDrugList = pivasDrugList;
			super();
		}
		
		public function get pivasDrugList():ArrayCollection {
			return _pivasDrugList;
		}
	}
}