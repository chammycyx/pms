package hk.org.ha.event.pms.main.inbox
{
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class SortItemListEvent extends AbstractTideEvent
	{
		private var _sortType:String;
		
		public function SortItemListEvent(sortType:String)
		{
			super();
			_sortType = sortType;
		}
		
		public function get sortType():String
		{
			return _sortType;
		}
	}
}