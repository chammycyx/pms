package hk.org.ha.control.pms.main.reftable {
	
	import hk.org.ha.event.pms.main.reftable.ClearQtyDurationConversionMaintViewEvent;
	import hk.org.ha.event.pms.main.reftable.PrintQtyDurationConversionRptEvent;
	import hk.org.ha.event.pms.main.reftable.RefreshDurConversionRptListEvent;
	import hk.org.ha.event.pms.main.reftable.RefreshPrintQtyDurationConversionRptEvent;
	import hk.org.ha.event.pms.main.reftable.RefreshQtyConversionRptListEvent;
	import hk.org.ha.event.pms.main.reftable.RefreshQtyDurationConversionViewEvent;
	import hk.org.ha.event.pms.main.reftable.RetrieveDurConversionRptListEvent;
	import hk.org.ha.event.pms.main.reftable.RetrieveQtyConversionRptListEvent;
	import hk.org.ha.event.pms.main.reftable.RetrieveQtyDurationConversionListEvent;
	import hk.org.ha.event.pms.main.reftable.UpdateQtyDurationConversionListEvent;
	import hk.org.ha.model.pms.biz.reftable.QtyDurationConversionRptServiceBean;
	import hk.org.ha.model.pms.biz.reftable.QtyDurationConversionServiceBean;
	import hk.org.ha.model.pms.dms.persistence.PmsPcuMapping;
	import hk.org.ha.model.pms.udt.vetting.OrderType;
	import hk.org.ha.model.pms.vo.reftable.QtyDurationConversionInfo;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("qtyDurationConversionServiceCtl", restrict="true")]
	public class QtyDurationConversionServiceCtl 
	{
		[In]
		public var qtyDurationConversionService:QtyDurationConversionServiceBean;
		
		[In]
		public var qtyDurationConversionRptService:QtyDurationConversionRptServiceBean;
		
		private var itemCode:String;
		private var pmsPcuMapping:PmsPcuMapping;
		private var exportOrderType:OrderType;
		
		[Observer]
		public function retrievePmsQtyConversionList(evt:RetrieveQtyDurationConversionListEvent):void
		{
			itemCode = evt.itemCode;
			pmsPcuMapping = evt.pmsPcuMapping;

			In(Object(qtyDurationConversionService).retrieveCapdItem)
			qtyDurationConversionService.retrieveQtyDurationConversionList(evt.itemCode, evt.pmsPcuMapping, retrievePmsQtyConversionListResult);
		}
		
		private function retrievePmsQtyConversionListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshQtyDurationConversionViewEvent((Object(qtyDurationConversionService).retrieveCapdItem), evt.result as QtyDurationConversionInfo));
		}
		
		[Observer]
		public function updateQtyDurationConversionList(evt:UpdateQtyDurationConversionListEvent):void
		{	
			In(Object(qtyDurationConversionService).saveSuccess);
			qtyDurationConversionService.updateQtyDurationConversionList(evt.updatePmsQtyConversionList, evt.qtyApplyAll, 
																			evt.updatePmsDurationConversionList, evt.durationApplyAll, 
																			updateQtyDurationConversionListResult);
		}	
		
		private function updateQtyDurationConversionListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new ClearQtyDurationConversionMaintViewEvent( 
				Object(qtyDurationConversionService).saveSuccess,
				(evt.result as String) ));
		}
		
		[Observer]
		public function printQtyDurationConversionRpt(evt:PrintQtyDurationConversionRptEvent):void {
			
			In(Object(qtyDurationConversionRptService).qtyRetrieveSuccess)
			In(Object(qtyDurationConversionRptService).durationRetrieveSuccess)
			qtyDurationConversionRptService.printQtyDurationConversionRpt(refreshPrintDurationConversionMaintRptResult);
		}
		
		private function refreshPrintDurationConversionMaintRptResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshPrintQtyDurationConversionRptEvent( Object(qtyDurationConversionRptService).qtyRetrieveSuccess, Object(qtyDurationConversionRptService).durationRetrieveSuccess  ));
		}
		
		[Observer]
		public function retrieveQtyConversionRptList(evt:RetrieveQtyConversionRptListEvent):void {
			In(Object(qtyDurationConversionRptService).qtyExportSuccess)
			qtyDurationConversionRptService.retrieveQtyConversionRptList(retrieveQtyConversionRptListResult);
		}
		
		private function retrieveQtyConversionRptListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshQtyConversionRptListEvent( Object(qtyDurationConversionRptService).qtyExportSuccess  ));
		}
		
		[Observer]
		public function retrieveDurConversionRptList(evt:RetrieveDurConversionRptListEvent):void {
			In(Object(qtyDurationConversionRptService).durExportSuccess)
			qtyDurationConversionRptService.retrieveDurConversionRptList(retrieveDurConversionRptListResult);
		}
		
		private function retrieveDurConversionRptListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshDurConversionRptListEvent( Object(qtyDurationConversionRptService).durExportSuccess  ));
		}
		
	}
}
