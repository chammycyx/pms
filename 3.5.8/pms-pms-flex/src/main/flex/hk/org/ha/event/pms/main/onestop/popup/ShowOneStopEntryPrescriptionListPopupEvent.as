package hk.org.ha.event.pms.main.onestop.popup
{
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowOneStopEntryPrescriptionListPopupEvent extends AbstractTideEvent
	{
		public function ShowOneStopEntryPrescriptionListPopupEvent()
		{
			super();
		}
	}
}