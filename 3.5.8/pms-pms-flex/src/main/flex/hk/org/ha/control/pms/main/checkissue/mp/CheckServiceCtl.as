package hk.org.ha.control.pms.main.checkissue.mp{
	
	import mx.utils.StringUtil;
	
	import hk.org.ha.event.pms.main.checkissue.mp.RetrieveCheckDeliveryEvent;
	import hk.org.ha.event.pms.main.checkissue.mp.RetrieveCheckOrderResultEvent;
	import hk.org.ha.event.pms.main.checkissue.mp.SetDrugCheckViewListSummaryEvent;
	import hk.org.ha.event.pms.main.checkissue.mp.UpdateDeliveryStatusToCheckedEvent;
	import hk.org.ha.event.pms.main.checkissue.mp.UpdateDeliveryStatusToCheckedResultEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.ShowLoadingPopupEvent;
	import hk.org.ha.model.pms.biz.checkissue.mp.CheckServiceBean;
	import hk.org.ha.model.pms.vo.checkissue.mp.DrugCheckViewListSummary;
	
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("checkServiceCtl", restrict="true")]
	public class CheckServiceCtl 
	{
		[In]
		public var checkService:CheckServiceBean;
		
		[Observer]
		public function retrieveCheckDelivery(evt:RetrieveCheckDeliveryEvent):void
		{
			In(Object(checkService).resultMsg)
			In(Object(checkService).batchCode)
			In(Object(checkService).batchDate)
			
			dispatchEvent(new ShowLoadingPopupEvent("Loading PAS Information..."));
			checkService.retrieveDelivery( evt.batchDate, evt.batchNum, retrieveCheckDeliveryResult, closeLoadingPopup );
		}
		
		private function retrieveCheckDeliveryResult(evt:TideResultEvent):void
		{	
			In(Object(checkService).resultMsg)
			In(Object(checkService).batchCode)
			In(Object(checkService).batchDate)
			
			if ( StringUtil.trim(Object(checkService).resultMsg) == '' ) {
				checkService.convertToCheckDelivery( convertToCheckedDeliveryResult, closeLoadingPopup ) ;
			} else {
				dispatchEvent( new CloseLoadingPopupEvent() );
				dispatchEvent( new UpdateDeliveryStatusToCheckedResultEvent(evt.result as DrugCheckViewListSummary, Object(checkService).resultMsg,Object(checkService).batchDate, Object(checkService).batchCode));
			}
		}
		
		private function convertToCheckedDeliveryResult(evt:TideResultEvent):void
		{
			dispatchEvent( new CloseLoadingPopupEvent() );
			
			if ( StringUtil.trim(Object(checkService).resultMsg) == '' ) {
				dispatchEvent( new SetDrugCheckViewListSummaryEvent(evt.result as DrugCheckViewListSummary));
				dispatchEvent( new RetrieveCheckOrderResultEvent() );
			} else {
				dispatchEvent( new UpdateDeliveryStatusToCheckedResultEvent(evt.result as DrugCheckViewListSummary, Object(checkService).resultMsg, Object(checkService).batchDate, Object(checkService).batchCode));
			}
		}
		
		[Observer]
		public function updateDeliveryStatusToChecked(evt:UpdateDeliveryStatusToCheckedEvent):void
		{
			In(Object(checkService).resultMsg)
			In(Object(checkService).batchCode)
			In(Object(checkService).batchDate)
			
			dispatchEvent(new ShowLoadingPopupEvent("Refreshing ..."));
			checkService.updateDeliveryStatusToChecked(evt.checkDelivery, evt.printRptFlag, updateDeliveryStatusToCheckedResult, closeLoadingPopup);
		}
		
		private function updateDeliveryStatusToCheckedResult(evt:TideResultEvent):void 
		{
			dispatchEvent( new CloseLoadingPopupEvent() );
			dispatchEvent( new UpdateDeliveryStatusToCheckedResultEvent(evt.result as DrugCheckViewListSummary, Object(checkService).resultMsg,Object(checkService).batchDate, Object(checkService).batchCode));
		}
		
		private function closeLoadingPopup(evt:TideFaultEvent):void
		{
			dispatchEvent( new CloseLoadingPopupEvent() );
		}
	}
}
