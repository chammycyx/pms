package hk.org.ha.control.pms.main.reftable {
	
	import hk.org.ha.event.pms.main.reftable.RefreshPmsRestrictItemSpecListByItemEvent;
	import hk.org.ha.event.pms.main.reftable.RefreshPmsRestrictItemSpecListEvent;
	import hk.org.ha.event.pms.main.reftable.RetrievePmsRestrictItemSpecListByItemEvent;
	import hk.org.ha.event.pms.main.reftable.UpdatePmsRestrictItemSpecListEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.model.pms.biz.reftable.PmsRestrictItemSpecByItemListServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("pmsRestrictItemSpecByItemListServiceCtl", restrict="true")]
	public class PmsRestrictItemSpecByItemListServiceCtl 
	{
		[In]
		public var pmsRestrictItemSpecByItemListService:PmsRestrictItemSpecByItemListServiceBean;

		[Observer]
		public function retrievePmsRestrictItemSpecListByItem(evt:RetrievePmsRestrictItemSpecListByItemEvent):void
		{
			pmsRestrictItemSpecByItemListService.retrievePmsRestrictItemListByItem(evt.itemCode, evt.pmsFmStatus, refreshPmsRestrictItemSpecListByItemResult);
		}
		
		private function refreshPmsRestrictItemSpecListByItemResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			evt.context.dispatchEvent(new RefreshPmsRestrictItemSpecListByItemEvent());
		}
		
		[Observer]
		public function updatePmsRestrictItemSpecList(evt:UpdatePmsRestrictItemSpecListEvent):void
		{
			pmsRestrictItemSpecByItemListService.updatePmsRestrictItemList(evt.newPmsRestrictItemSpecList, evt.itemCode, evt.fmStatus, updatePmsRestrictItemSpecListResult);
		}
		
		private function updatePmsRestrictItemSpecListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshPmsRestrictItemSpecListEvent());
		}
	}
}
