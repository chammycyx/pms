package hk.org.ha.event.pms.main.drug.popup
{
	import mx.collections.ArrayCollection;

	public class FreeTextEntryPopupProp
	{
		public var isDrugSearch:Boolean;
		public var popupOkHandler:Function;
		public var popupCancelHandler:Function = null;
		public var drugName:String;
		public var routeDesc:String;
		public var formDesc:String;
		public var strength:String;
		
		public var drugSearchRouteDescList:ArrayCollection;
		public var drugSearchFormDescList:ArrayCollection;
	}
}