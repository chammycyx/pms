package hk.org.ha.event.pms.main.refill {	
	import hk.org.ha.model.pms.persistence.disp.PharmOrder;	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshUpdateRefillScheduleListEvent extends AbstractTideEvent 
	{		
		private var _pharmOrder:PharmOrder;
		
		public function RefreshUpdateRefillScheduleListEvent(pharmOrder:PharmOrder):void 
		{
			super();
			_pharmOrder = pharmOrder;
		}	
		
		public function get pharmOrder():PharmOrder {
			return _pharmOrder;
		}
	}
}