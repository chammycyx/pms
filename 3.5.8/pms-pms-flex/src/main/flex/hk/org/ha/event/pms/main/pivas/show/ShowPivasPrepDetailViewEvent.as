package hk.org.ha.event.pms.main.pivas.show
{
	import hk.org.ha.model.pms.persistence.pivas.PivasWorklist;
	import hk.org.ha.model.pms.udt.pivas.PivasWorklistType;
	import hk.org.ha.model.pms.vo.pivas.worklist.PivasPrepDetailInfo;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowPivasPrepDetailViewEvent extends AbstractTideEvent
	{
		private var _clearMessages:Boolean;
		
		private var _sourceType:PivasWorklistType;
		
		private var _pivasWorklist:PivasWorklist;
		
		private var _pivasPrepDetailInfo:PivasPrepDetailInfo;
		
		public function ShowPivasPrepDetailViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}

		public function get sourceType():PivasWorklistType {
			return _sourceType;
		}
		
		public function set sourceType(sourceType:PivasWorklistType):void {
			_sourceType = sourceType;
		}
		
		public function get pivasWorklist():PivasWorklist {
			return _pivasWorklist;
		}
		
		public function set pivasWorklist(pivasWorklist:PivasWorklist):void {
			_pivasWorklist = pivasWorklist;
		}
		
		public function get pivasPrepDetailInfo():PivasPrepDetailInfo {
			return _pivasPrepDetailInfo;
		}
		
		public function set pivasPrepDetailInfo(pivasPrepDetailInfo:PivasPrepDetailInfo):void {
			_pivasPrepDetailInfo = pivasPrepDetailInfo;
		}
	}
}