package hk.org.ha.event.pms.main.pivas.popup {
	
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowPivasPrepDetailFormulaPopupEvent extends AbstractTideEvent 
	{
		private var _pivasFormulaId:Number;

		private var _pivasFormulaMethodId:Number;
		
		private var _medProfile:MedProfile;
		
		private var _medProfileMoItem:MedProfileMoItem;
		
		private var _refillFlag:Boolean;
		
		private var _okHandler:Function;

		private var _cancelHandler:Function;
		
		public function ShowPivasPrepDetailFormulaPopupEvent(pivasFormulaId:Number, pivasFormulaMethodId:Number, medProfile:MedProfile, medProfileMoItem:MedProfileMoItem, refillFlag:Boolean, okHandler:Function, cancelHandler:Function):void 
		{
			super();
			_pivasFormulaId = pivasFormulaId;
			_pivasFormulaMethodId = pivasFormulaMethodId;
			_medProfile = medProfile;
			_medProfileMoItem = medProfileMoItem;
			_refillFlag = refillFlag;
			_okHandler = okHandler;
			_cancelHandler = cancelHandler;
		}

		public function get pivasFormulaId():Number {
			return _pivasFormulaId;
		}

		public function get pivasFormulaMethodId():Number {
			return _pivasFormulaMethodId;
		}
		
		public function get medProfile():MedProfile {
			return _medProfile;
		}
		
		public function get medProfileMoItem():MedProfileMoItem {
			return _medProfileMoItem;
		}
		
		public function get refillFlag():Boolean {
			return _refillFlag;
		}
		
		public function get okHandler():Function {
			return _okHandler;
		}
		
		public function get cancelHandler():Function {
			return _cancelHandler;
		}
	}
}