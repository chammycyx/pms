package hk.org.ha.event.pms.main.onestop {
		
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ReprintClearEvent extends AbstractTideEvent 
	{			
		public function ReprintClearEvent():void 
		{
			super();
		}        
	}
}