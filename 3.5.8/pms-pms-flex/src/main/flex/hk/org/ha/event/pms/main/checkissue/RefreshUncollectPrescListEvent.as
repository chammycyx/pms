package hk.org.ha.event.pms.main.checkissue
{
	import hk.org.ha.model.pms.vo.checkissue.CheckIssueUncollectSummary;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshUncollectPrescListEvent extends AbstractTideEvent 
	{		
		private var _checkIssueUncollectSummary:CheckIssueUncollectSummary;
		
		public function RefreshUncollectPrescListEvent(checkIssueUncollectSummary:CheckIssueUncollectSummary):void 
		{
			super();
			_checkIssueUncollectSummary = checkIssueUncollectSummary;
		}
		
		public function get checkIssueUncollectSummary():CheckIssueUncollectSummary {
			return _checkIssueUncollectSummary;
		}	
	}
}