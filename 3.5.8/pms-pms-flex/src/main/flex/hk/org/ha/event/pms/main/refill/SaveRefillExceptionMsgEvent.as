package hk.org.ha.event.pms.main.refill {

	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SaveRefillExceptionMsgEvent extends AbstractTideEvent 
	{		
		private var _sysMsgProp:SystemMessagePopupProp;
		
		public function SaveRefillExceptionMsgEvent(sysMsgProp:SystemMessagePopupProp):void 
		{
			super();
			_sysMsgProp = sysMsgProp;
		}
		
		public function get sysMsgProp():SystemMessagePopupProp {
			return _sysMsgProp;
		}
	}
}