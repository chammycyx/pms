package hk.org.ha.event.pms.main.report {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshDayEndWorkstoreListEvent extends AbstractTideEvent 
	{
		public function RefreshDayEndWorkstoreListEvent():void 
		{
			super();
		}
	}
}