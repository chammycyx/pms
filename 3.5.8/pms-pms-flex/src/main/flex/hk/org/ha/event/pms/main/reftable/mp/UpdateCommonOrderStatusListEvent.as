package hk.org.ha.event.pms.main.reftable.mp
{
	import hk.org.ha.model.pms.dms.persistence.PmsDosageConversion;
	import hk.org.ha.model.pms.dms.persistence.PmsDosageConversionSet;
	
	import mx.collections.ArrayCollection;
	import mx.controls.List;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class UpdateCommonOrderStatusListEvent extends AbstractTideEvent
	{
		private var _commonOrderStatusList:ArrayCollection;
		
		public function get commonOrderStatusList():ArrayCollection
		{
			return _commonOrderStatusList;
		}

		public function UpdateCommonOrderStatusListEvent(commonOrderStatusList:ArrayCollection):void
		{
			super();
			_commonOrderStatusList = commonOrderStatusList;
		}
	}
}