package hk.org.ha.event.pms.main.reftable {
	
	import hk.org.ha.model.pms.persistence.reftable.Prepack;
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdatePrepackEvent extends AbstractTideEvent 
	{
		private var _updatePrepack:Prepack;
		
		public function UpdatePrepackEvent(updatePrepack:Prepack):void 
		{
			super();
			_updatePrepack = updatePrepack;
		}
		
		public function get updatePrepack():Prepack {
			return _updatePrepack;
		}
	}
}