package hk.org.ha.event.pms.main.vetting.mp
{
	import flash.events.Event;
	
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CancelFromManualEntryItemEditEvent extends AbstractTideEvent
	{
		private var _itemNumList:ListCollectionView;
		private var _callBackEvent:Event;
		
		public function CancelFromManualEntryItemEditEvent(itemNumList:ListCollectionView, callBackEventVal:Event=null)
		{
			super();
			_itemNumList = itemNumList;
			_callBackEvent  = callBackEventVal;
		}
		
		public function get itemNumList():ListCollectionView
		{
			return _itemNumList;
		}
		
		public function get callBackEvent():Event
		{
			return _callBackEvent;
		}					
	}
}