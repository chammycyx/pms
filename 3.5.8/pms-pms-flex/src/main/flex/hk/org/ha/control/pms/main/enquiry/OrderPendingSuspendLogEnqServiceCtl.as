package hk.org.ha.control.pms.main.enquiry {
	
	import hk.org.ha.event.pms.main.enquiry.RefreshOrderPendingSuspendLogEnqViewEvent;
	import hk.org.ha.event.pms.main.enquiry.RetrieveOrderPendingSuspendLogEnqListEvent;
	import hk.org.ha.model.pms.biz.enquiry.OrderPendingSuspendLogEnqServiceBean;
	import hk.org.ha.model.pms.vo.enquiry.OrderPendingSuspendLogEnq;
	
	import org.granite.tide.events.TideResultEvent;
	
	
	[Bindable]
	[Name("OrderPendingSuspendLogEnqServiceCtl", restrict="true")]
	public class OrderPendingSuspendLogEnqServiceCtl 
	{
		[In]
		public var orderPendingSuspendLogEnqService:OrderPendingSuspendLogEnqServiceBean;
		
		private var ordPend:OrderPendingSuspendLogEnq;
		
		[Observer]
		public function retrieveOrderPendingSuspendLogEnqList(evt:RetrieveOrderPendingSuspendLogEnqListEvent):void
		{
			orderPendingSuspendLogEnqService.retrieveOrderPendingSuspendLogEnqList(evt.fromDate, evt.toDate, evt.caseNum, evt.filter, retrieveOrderPendingSuspendLogEnqListResult);
		}
		
		private function retrieveOrderPendingSuspendLogEnqListResult(evt:TideResultEvent):void
		{			
			evt.context.dispatchEvent(new RefreshOrderPendingSuspendLogEnqViewEvent());
		}
		
	}
}
