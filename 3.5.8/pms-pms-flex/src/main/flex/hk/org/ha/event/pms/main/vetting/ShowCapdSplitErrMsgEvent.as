package hk.org.ha.event.pms.main.vetting {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowCapdSplitErrMsgEvent extends AbstractTideEvent 
	{		
		private var _errMsg:String;
		private var _errItemCode:String;
		
		public function ShowCapdSplitErrMsgEvent(errMsg:String, errItemCode:String) {
			super();
			_errMsg = errMsg;
			_errItemCode = errItemCode;
		}
		
		public function get errMsg():String {
			return _errMsg;
		}
		
		public function get errItemCode():String {
			return _errItemCode;
		}
	}
}
