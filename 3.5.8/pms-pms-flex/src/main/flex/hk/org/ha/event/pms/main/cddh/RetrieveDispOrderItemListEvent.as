package hk.org.ha.event.pms.main.cddh {
	
	import hk.org.ha.model.pms.vo.cddh.CddhCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDispOrderItemListEvent extends AbstractTideEvent 
	{
		private var _cddhCriteria:CddhCriteria;
		private var _displayAll:Boolean;
		private var _errorMsgPopupCallbackFunc:Function = null;
		
		public function RetrieveDispOrderItemListEvent(cddhCriteria:CddhCriteria, displayAll:Boolean=false):void 
		{
			super();
			_cddhCriteria = cddhCriteria;
			_displayAll = displayAll;
		}
		
		public function get cddhCriteria():CddhCriteria
		{
			return _cddhCriteria;
		}
		
		public function get displayAll():Boolean
		{
			return _displayAll;
		}

		public function set errorMsgPopupCallbackFunc(errorMsgPopupCallbackFunc:Function):void
		{
			_errorMsgPopupCallbackFunc = errorMsgPopupCallbackFunc;
		}
		
		public function get errorMsgPopupCallbackFunc():Function
		{
			return _errorMsgPopupCallbackFunc;
		}
	}
}