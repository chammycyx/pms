package hk.org.ha.control.pms.main.report {
	import hk.org.ha.event.pms.main.report.PrintDrugRefillListRptEvent;
	import hk.org.ha.event.pms.main.report.RefreshDrugRefillListRptEvent;
	import hk.org.ha.event.pms.main.report.RetrieveDrugRefillListRptEvent;
	import hk.org.ha.event.pms.main.report.RetrieveDrugRefillListRptInfoEvent;
	import hk.org.ha.event.pms.main.report.SetDrugRefillListRptInfoEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.model.pms.biz.report.DrugRefillListRptServiceBean;
	import hk.org.ha.model.pms.vo.report.DrugRefillListRptInfo;
	
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("drugRefillListRptServiceCtl", restrict="true")]
	public class DrugRefillListRptServiceCtl 
	{
		[In]
		public var drugRefillListRptService:DrugRefillListRptServiceBean;
		
		private var refillListRptInfo:DrugRefillListRptInfo;
		
		[Observer]
		public function retrieveDrugRefillListRptInfo(evt:RetrieveDrugRefillListRptInfoEvent):void
		{
			drugRefillListRptService.retrieveDrugRefillListRptInfo(retrieveDrugRefillListRptInfoResult);
		}
		
		private function retrieveDrugRefillListRptInfoResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new SetDrugRefillListRptInfoEvent(evt.result as DrugRefillListRptInfo));
		}
		
		[Observer]
		public function retrieveDrugRefillListRpt(evt:RetrieveDrugRefillListRptEvent):void
		{
			In(Object(drugRefillListRptService).retrieveSuccess)
			drugRefillListRptService.retrieveDrugRefillList(evt.drugRefillRptInfo, retrieveDrugRefillListRptResult, retrieveDrugRefillListRptFault);
		}

		private function retrieveDrugRefillListRptFault(evt:TideFaultEvent):void
		{
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
		}
		
		private function retrieveDrugRefillListRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshDrugRefillListRptEvent(Object(drugRefillListRptService).retrieveSuccess));
		}
		
		[Observer]
		public function printDrugRefillListRpt(evt:PrintDrugRefillListRptEvent):void
		{
			drugRefillListRptService.printDrugRefillListRpt();
		}
	}
}
