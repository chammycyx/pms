package hk.org.ha.event.pms.main.pivas.popup {
	
	import hk.org.ha.model.pms.udt.pivas.PivasWorklistType;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowPivasWorklistSortPopupEvent extends AbstractTideEvent 
	{
		private var _sourceType:PivasWorklistType;
		
		private var _okHandler:Function;

		private var _cancelHandler:Function;
		
		public function ShowPivasWorklistSortPopupEvent(sourceType:PivasWorklistType, okHandler:Function, cancelHandler:Function):void 
		{
			super();
			_sourceType = sourceType;
			_okHandler = okHandler;
			_cancelHandler = cancelHandler;
		}

		public function get sourceType():PivasWorklistType {
			return _sourceType;
		}
		
		public function get okHandler():Function {
			return _okHandler;
		}
		
		public function get cancelHandler():Function {
			return _cancelHandler;
		}
	}
}