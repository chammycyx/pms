package hk.org.ha.control.pms.main.report {
	import hk.org.ha.event.pms.main.report.PrintSfiHandleChargeExemptionRptEvent;
	import hk.org.ha.event.pms.main.report.RefreshSfiHandleChargeExemptionRptEvent;
	import hk.org.ha.event.pms.main.report.RetrieveSfiHandleChargeExemptionRptEvent;
	import hk.org.ha.model.pms.biz.report.SfiHandleChargeExemptionRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	
	[Bindable]
	[Name("sfiHandleChargeExemptionRptServiceCtl", restrict="true")]
	public class SfiHandleChargeExemptionRptServiceCtl 
	{
		[In]
		public var sfiHandleChargeExemptionRptService:SfiHandleChargeExemptionRptServiceBean;
		
		[Observer]
		public function retrieveSfiHandleChargeExemptionRpt(evt:RetrieveSfiHandleChargeExemptionRptEvent):void
		{
			In(Object(sfiHandleChargeExemptionRptService).retrieveSuccess)
			sfiHandleChargeExemptionRptService.retrieveSfiHandleChargeExemptionRptList(evt.rptDate, retrieveSfiHandleChargeExemptionRptResult );
		}
		
		private function retrieveSfiHandleChargeExemptionRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshSfiHandleChargeExemptionRptEvent(Object(sfiHandleChargeExemptionRptService).retrieveSuccess));
		}
		
		[Observer]
		public function printSfiHandleChargeExemptionRpt(evt:PrintSfiHandleChargeExemptionRptEvent):void
		{
			sfiHandleChargeExemptionRptService.printSfiHandleChargeExemptionRpt();
		}
		
	}
}
