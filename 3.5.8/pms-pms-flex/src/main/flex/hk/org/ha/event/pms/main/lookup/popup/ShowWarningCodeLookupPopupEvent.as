package  hk.org.ha.event.pms.main.lookup.popup{
	
	import hk.org.ha.event.pms.main.lookup.popup.WarningCodeLookupPopupProp;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowWarningCodeLookupPopupEvent extends AbstractTideEvent 
	{
		private var _warningCodeLookupPopupProp:WarningCodeLookupPopupProp;
		
		public function ShowWarningCodeLookupPopupEvent(warningCodeLookupPopupProp:WarningCodeLookupPopupProp):void 
		{
			super();
			_warningCodeLookupPopupProp = warningCodeLookupPopupProp;
		}
		
		public function get warningCodeLookupPopupProp():WarningCodeLookupPopupProp{
			return _warningCodeLookupPopupProp;
		}
	}
}