package hk.org.ha.event.pms.main.vetting {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
	
	public class RecalPharmOrderItemEvent extends AbstractTideEvent {		
		
		private var _pharmOrderItem:PharmOrderItem;		
		private var _callBackFunction:Function;
		
		public function RecalPharmOrderItemEvent(pharmOrderItemValue:PharmOrderItem, callBackfunctionValue:Function) 
		{
			super();						
			_pharmOrderItem 	  = pharmOrderItemValue;
			_callBackFunction 	  = callBackfunctionValue;
		}		
		
		public function get pharmOrderItem():PharmOrderItem 
		{
			return _pharmOrderItem;
		}	
		
		public function get callBackFunction():Function
		{
			return _callBackFunction;
		}
	}
}