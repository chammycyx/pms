package hk.org.ha.event.pms.main.vetting {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrieveCapdSupplierSystemListEvent extends AbstractTideEvent 
	{
		public function RetrieveCapdSupplierSystemListEvent() {
			super();
		}
	}
}
