package hk.org.ha.event.pms.main.vetting {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.disp.PharmOrder;
	import hk.org.ha.model.pms.udt.printing.PrintLang;	
	
	public class GetDispLabelPreviewEvent extends AbstractTideEvent {

		private var _pharmOrder:PharmOrder;				
		
		private var _printLang:PrintLang;
		
		private var _printType:String;
		
		public function GetDispLabelPreviewEvent(pharmOrder:PharmOrder, printLang:PrintLang, printType:String) {
			super();			
			_pharmOrder = pharmOrder;					
			_printLang = printLang;
			_printType = printType;
		}
		
		public function get pharmOrder():PharmOrder {
			return _pharmOrder;
		}
		
		public function get printLang():PrintLang {
			return _printLang;
		}
		
		public function get printType():String {
			return _printType;
		}
		
	}
}