package hk.org.ha.event.pms.main.reftable {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshDmDrugListEvent extends AbstractTideEvent 
	{
		public var _event:Event;
		
		public function RefreshDmDrugListEvent(event:Event=null):void 
		{
			super();
			_event = event;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}