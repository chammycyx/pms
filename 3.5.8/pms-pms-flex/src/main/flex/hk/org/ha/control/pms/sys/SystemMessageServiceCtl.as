package hk.org.ha.control.pms.sys
{
	import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
	import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageListEvent;
	import hk.org.ha.fmk.pms.flex.components.message.ShowSystemMessagePopupEvent;
	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	import hk.org.ha.fmk.pms.flex.utils.SystemMessageBuilder;
	import hk.org.ha.fmk.pms.sys.entity.SystemMessage;
	import hk.org.ha.model.pms.biz.sys.SystemMessageServiceBean;
	import hk.org.ha.model.pms.vo.sys.SysMsgMap;
	
	import mx.core.UIComponent;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("systemMessageServiceCtl", restrict="false")]
	public class SystemMessageServiceCtl
	{
		[In]
		public var systemMessageService:SystemMessageServiceBean;
		
		[In]
		public var sysMsgMap:SysMsgMap;
			
		[In]
		public var systemMessage:SystemMessage;
		
		private var sysMsgProp:SystemMessagePopupProp;
		
		private var callbackFunc:Function;
		
		[Observer]
		public function retrieveSystemMessage(evt:RetrieveSystemMessageEvent):void
		{
			if (evt.sysMsgProp.messageCode == "0004") {
				sysMsgProp = evt.sysMsgProp;			
				systemMessageService.retrieveSystemMessage(sysMsgProp.messageCode, sysMsgProp.messageParams, retrieveSystemMessageResult);
			} else {
				var sysMsg:SystemMessage = sysMsgMap.getSystemMessage(evt.sysMsgProp.messageCode);
				sysMsgProp = evt.sysMsgProp;
				sysMsgProp.messageCode = sysMsg.messageCode;
				sysMsgProp.displayDesc = SystemMessageBuilder.createPopupSystemMessage(sysMsg, sysMsgProp.messageParams);
				sysMsgProp.messageTitle = "Message Code: "+ sysMsg.applicationId + "-" + sysMsg.severityCode + "-" + sysMsg.messageCode;
				if (sysMsgProp.functionId != null && sysMsgProp.functionId != "") {
					sysMsgProp.messageTitle += "-" + sysMsgProp.functionId;
				}
				dispatchEvent(new ShowSystemMessagePopupEvent(sysMsgProp));
			}
		}

		public function retrieveSystemMessageResult(evt:TideResultEvent):void 
		{				
			sysMsgProp.messageCode = systemMessage.messageCode;
			sysMsgProp.displayDesc = systemMessage.displayDesc;
			sysMsgProp.messageTitle = "Message Code: "+systemMessage.applicationId + "-" + systemMessage.severityCode + "-" + systemMessage.messageCode;
			evt.context.dispatchEvent(new ShowSystemMessagePopupEvent(sysMsgProp));
		}
	}
}