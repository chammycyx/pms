package hk.org.ha.event.pms.main.report {
	
	import hk.org.ha.model.pms.udt.report.RefillItemRptStatus;
	import hk.org.ha.model.pms.udt.report.RefillReportType;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveRefillRptEvent extends AbstractTideEvent 
	{
		private var _reportType:RefillReportType;
		private var _workstoreCode:String
		private var _refillDueDateFrom:Date;
		private var _refillDueDateTo:Date;
		private var _itemCode:String;
		private var _status:RefillItemRptStatus;
		private var _drsPatientFlag:Boolean;
		
		public function RetrieveRefillRptEvent( reportType:RefillReportType, workstoreCode:String, refillDueDateFrom:Date, refillDueDateTo:Date, itemCode:String, status:RefillItemRptStatus, drsPatientFlag:Boolean = false):void 
		{
			super();
			_reportType = reportType;
			_workstoreCode = workstoreCode;
			_refillDueDateFrom = refillDueDateFrom;
			_refillDueDateTo = refillDueDateTo;
			_itemCode = itemCode;
			_status = status;
			_drsPatientFlag = drsPatientFlag;
		}
		
		public function get reportType():RefillReportType{
			return _reportType;
		} 
		
		public function get workstoreCode():String{
			return _workstoreCode;
		} 

		public function get refillDueDateFrom():Date{
			return _refillDueDateFrom;
		} 
		
		public function get refillDueDateTo():Date{
			return _refillDueDateTo;
		}
		
		public function get itemCode():String{
			return _itemCode;
		} 
		
		public function get status():RefillItemRptStatus{
			return _status;
		}
		
		public function get drsPatientFlag():Boolean{
			return _drsPatientFlag;
		}
	}
}