package hk.org.ha.control.pms.main.report {
	import hk.org.ha.event.pms.main.report.PrintEdsTicketNumRptEvent;
	import hk.org.ha.event.pms.main.report.RefreshEdsTicketNumRptEvent;
	import hk.org.ha.event.pms.main.report.RetrieveEdsTicketNumRptEvent;
	import hk.org.ha.model.pms.biz.report.EdsTicketNumRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	
	[Bindable]
	[Name("edsTicketNumRptServiceCtl", restrict="true")]
	public class EdsTicketNumRptServiceCtl 
	{
		[In]
		public var edsTicketNumRptService:EdsTicketNumRptServiceBean;
		
		[Observer]
		public function retrieveEdsTicketNumRpt(evt:RetrieveEdsTicketNumRptEvent):void
		{
			In(Object(edsTicketNumRptService).retrieveSuccess)
			edsTicketNumRptService.retrieveEdsTicketNumRptList(evt.rptDate, evt.ticketNum, retrieveEdsTicketNumRptResult);
		}
		
		private function retrieveEdsTicketNumRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshEdsTicketNumRptEvent(Object(edsTicketNumRptService).retrieveSuccess));
		}
		
		[Observer]
		public function printEdsTicketNumRpt(evt:PrintEdsTicketNumRptEvent):void
		{
			edsTicketNumRptService.printEdsTicketNumRpt();
		}
		
	}
}
