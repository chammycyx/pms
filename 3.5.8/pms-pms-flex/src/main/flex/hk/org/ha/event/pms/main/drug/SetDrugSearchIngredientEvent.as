package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetDrugSearchIngredientEvent extends AbstractTideEvent 
	{			
		private var _drugSearchSource:DrugSearchSource;
		private var _drugSearchDrugIngredientList:ArrayCollection;
		
		public function SetDrugSearchIngredientEvent(drugSearchSource:DrugSearchSource, drugSearchDrugIngredientList:ArrayCollection):void 
		{
			super();
			_drugSearchSource = drugSearchSource;
			_drugSearchDrugIngredientList = drugSearchDrugIngredientList;
		}
		
		public function get drugSearchSource():DrugSearchSource {
			return _drugSearchSource;
		}
		
		public function get drugSearchDrugIngredientList():ArrayCollection {
			return _drugSearchDrugIngredientList;
		}
	}
}