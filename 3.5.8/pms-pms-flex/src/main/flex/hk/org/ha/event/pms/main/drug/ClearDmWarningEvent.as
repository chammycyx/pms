package hk.org.ha.event.pms.main.drug {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ClearDmWarningEvent extends AbstractTideEvent 
	{
		public function ClearDmWarningEvent():void 
		{
			super();
		}
	}
}