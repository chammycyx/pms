package hk.org.ha.event.pms.main.alert.druginfo {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.rx.DhRxDrug;
	
	public class RetrievePreparationForDhOrderEvent extends AbstractTideEvent {

		private var _dhRxDrug:DhRxDrug;
		
		private var _dob:Date;
		
		private var _patHospCode:String;
		
		private var _gcnSeqNum:String;
		
		private var _rdfgenId:String;
		
		private var _rgenId:String;

		public function RetrievePreparationForDhOrderEvent(dhRxDrug:DhRxDrug, dob:Date, patHospCode:String,
														   gcnSeqNum:String, rdfgenId:String, rgenId:String) {
			super();
			_dhRxDrug = dhRxDrug;
			_dob = dob;
			_patHospCode = patHospCode;
			_gcnSeqNum = gcnSeqNum;
			_rdfgenId = rdfgenId;
			_rgenId = rgenId;
		}
		
		public function get dhRxDrug():DhRxDrug {
			return _dhRxDrug;
		}

		public function get dob():Date {
			return _dob;
		}
		
		public function get patHospCode():String {
			return _patHospCode;
		}
		
		public function get gcnSeqNum():String {
			return _gcnSeqNum;
		}
		
		public function get rdfgenId():String {
			return _rdfgenId;
		}
		
		public function get rgenId():String {
			return _rgenId;
		}
	}
}