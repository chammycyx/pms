package hk.org.ha.event.pms.main.checkissue
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveCheckIssueListEvent extends AbstractTideEvent 
	{		
		private var _checkIssueListDate:Date;
		private var _otherCheckListDate:Date;
		private var _issuedListDate:Date;
		private var _setFocusOnTicketNum:Boolean;
		private var _refreshAll:Boolean;
		private var _refreshIssuedList:Boolean;

		public function RetrieveCheckIssueListEvent(setFocusOnTicketNum:Boolean, 
													refreshAll:Boolean, 
													refreshIssuedList:Boolean, 
													checkIssueListDate:Date, 
													otherCheckListDate:Date, 
													issuedListDate:Date):void 
		{
			super();
			_checkIssueListDate = checkIssueListDate;
			_otherCheckListDate = otherCheckListDate;
			_issuedListDate = issuedListDate;
			_setFocusOnTicketNum = setFocusOnTicketNum;
			_refreshAll = refreshAll;
			_refreshIssuedList = refreshIssuedList;
		}
		
		public function get checkIssueListDate():Date {
			return _checkIssueListDate;
		}
		
		public function get otherCheckListDate():Date {
			return _otherCheckListDate;
		}
		
		public function get issuedListDate():Date {
			return _issuedListDate;
		}
		
		public function get setFocusOnTicketNum():Boolean {
			return _setFocusOnTicketNum;
		}
		
		public function get refreshAll():Boolean {
			return _refreshAll;
		}
		
		public function get refreshIssuedList():Boolean {
			return _refreshIssuedList;
		}
	}
}