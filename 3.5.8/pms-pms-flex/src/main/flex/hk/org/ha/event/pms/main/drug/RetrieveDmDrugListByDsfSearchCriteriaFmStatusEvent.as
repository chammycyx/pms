package hk.org.ha.event.pms.main.drug {

	import flash.events.Event;
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dms.vo.DsfSearchCriteria;	
	
	public class RetrieveDmDrugListByDsfSearchCriteriaFmStatusEvent extends AbstractTideEvent {		
		
		private var _criteria:DsfSearchCriteria;
		
		private var _fmStatus:String;
		
		private var _callBackEvent:Event;				
		
		public function RetrieveDmDrugListByDsfSearchCriteriaFmStatusEvent(criteria:DsfSearchCriteria, fmStatus:String, event:Event=null) 
		{
			super();			
			_callBackEvent = event;
			_criteria = criteria;
			_fmStatus = fmStatus;
		}		
		
		public function get criteria():DsfSearchCriteria
		{
			return _criteria;
		}
		
		public function get fmStatus():String
		{
			return _fmStatus;
		}
		
		public function get callBackEvent():Event
		{
			return _callBackEvent;
		}
	}
}