package hk.org.ha.event.pms.main.prevet
{
	import hk.org.ha.model.pms.persistence.disp.MedOrder;
	import hk.org.ha.model.pms.udt.prevet.PreVetMode;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CheckPreVetOrderStatusEvent extends AbstractTideEvent
	{		
		private var _medOrder:MedOrder;
		private var _preVetMode:PreVetMode;
		private var _callbackFunction:Function;
		
		public function CheckPreVetOrderStatusEvent(medOrder:MedOrder, preVetMode:PreVetMode, callbackFunction:Function)
		{
			super();
			_medOrder = medOrder;
			_preVetMode = preVetMode;
			_callbackFunction = callbackFunction;
		}
		
		public function get medOrder():MedOrder
		{
			return _medOrder;
		}
		
		public function get preVetMode():PreVetMode
		{
			return _preVetMode;
		}
		
		public function get callbackFunction():Function
		{
			return _callbackFunction;
		}
	}
}