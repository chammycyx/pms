package hk.org.ha.event.pms.main.picking
{
	import hk.org.ha.model.pms.persistence.reftable.Workstation;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateDispOrderItemStatusToPickedEvent extends AbstractTideEvent 
	{		
		private var _ticketDate:Date;
		private var _ticketNum:String;
		private var _itemNum:Number;
		private var _userCode:String;
		private var _workstationId:Number;
		
		public function UpdateDispOrderItemStatusToPickedEvent(ticketDate:Date, ticketNum:String, itemNum:Number, userCode:String, workstationId:Number):void 
		{
			super();
			_ticketDate = ticketDate;
			_ticketNum = ticketNum;
			_itemNum = itemNum;
			_userCode = userCode;
			_workstationId = workstationId;
		}
		
		public function get ticketDate():Date {
			return _ticketDate;
		}
		
		public function get ticketNum():String {
			return _ticketNum;
		}
		
		public function get itemNum():Number {
			return _itemNum;
		}
		
		public function get userCode():String {
			return _userCode;
		}
		
		public function get workstationId():Number {
			return _workstationId;
		}
	}
}