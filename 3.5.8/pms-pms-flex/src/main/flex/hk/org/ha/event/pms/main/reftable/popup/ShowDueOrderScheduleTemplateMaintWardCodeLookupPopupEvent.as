package  hk.org.ha.event.pms.main.reftable.popup {
	
	import hk.org.ha.event.pms.main.reftable.popup.DueOrderScheduleTemplateMaintWardCodeLookupPopupProp;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDueOrderScheduleTemplateMaintWardCodeLookupPopupEvent extends AbstractTideEvent 
	{
		private var _dueOrderScheduleTemplateMaintWardCodeLookupPopupProp:DueOrderScheduleTemplateMaintWardCodeLookupPopupProp;
		
		public function ShowDueOrderScheduleTemplateMaintWardCodeLookupPopupEvent(dueOrderScheduleTemplateMaintWardCodeLookupPopupProp:DueOrderScheduleTemplateMaintWardCodeLookupPopupProp):void 
		{
			super();
			_dueOrderScheduleTemplateMaintWardCodeLookupPopupProp = dueOrderScheduleTemplateMaintWardCodeLookupPopupProp;
		}
		
		public function get dueOrderScheduleTemplateMaintWardCodeLookupPopupProp():DueOrderScheduleTemplateMaintWardCodeLookupPopupProp {
			return _dueOrderScheduleTemplateMaintWardCodeLookupPopupProp;
		}
	}
}