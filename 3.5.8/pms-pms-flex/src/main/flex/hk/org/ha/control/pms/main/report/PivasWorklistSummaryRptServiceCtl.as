package hk.org.ha.control.pms.main.report {
	
	import hk.org.ha.event.pms.main.report.PrintPivasWorklistSummaryRptEvent;
	import hk.org.ha.model.pms.biz.report.PivasWorklistSummaryRptServiceBean;

	[Bindable]
	[Name("pivasWorklistSummaryRptServiceCtl", restrict="true")]
	public class PivasWorklistSummaryRptServiceCtl 
	{
		[In]
		public var pivasWorklistSummaryRptService:PivasWorklistSummaryRptServiceBean;

		[Observer]
		public function printPivasWorklistSummaryRpt(evt:PrintPivasWorklistSummaryRptEvent):void
		{
			pivasWorklistSummaryRptService.printPivasWorklistSummaryRpt(evt.printPivasWorklistSummaryRptList);
		}
		
	}
}
