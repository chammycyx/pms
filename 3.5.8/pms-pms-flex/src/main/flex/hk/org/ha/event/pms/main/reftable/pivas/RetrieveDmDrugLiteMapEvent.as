package hk.org.ha.event.pms.main.reftable.pivas
{
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDmDrugLiteMapEvent extends AbstractTideEvent
	{
		private var _itemCodeList:ListCollectionView;
		private var _successCallback:Function;
		
		public function RetrieveDmDrugLiteMapEvent(itemCodeList:ListCollectionView, successCallback:Function)
		{
			super();
			_itemCodeList = itemCodeList;
			_successCallback = successCallback;
		}
		
		public function get itemCodeList():ListCollectionView
		{
			return _itemCodeList;
		}
		
		public function get successCallback():Function
		{
			return _successCallback;
		}
	}
}