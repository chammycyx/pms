package hk.org.ha.event.pms.main.report {
	
	import hk.org.ha.model.pms.vo.report.ItemLocationRpt;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveItemLocationRptEvent extends AbstractTideEvent 
	{
		private var _itemLocationRpt:ItemLocationRpt;
		
		public function RetrieveItemLocationRptEvent( itemLocationRpt:ItemLocationRpt):void 
		{
			super();
			_itemLocationRpt = itemLocationRpt;
		}
		
		public function get itemLocationRpt():ItemLocationRpt{
			return _itemLocationRpt;
		} 
	}
}