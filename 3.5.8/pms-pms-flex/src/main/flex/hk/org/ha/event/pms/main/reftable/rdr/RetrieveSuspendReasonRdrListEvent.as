package hk.org.ha.event.pms.main.reftable.rdr {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSuspendReasonRdrListEvent extends AbstractTideEvent 
	{
		private var _callbackFunc:Function;
		
		public function RetrieveSuspendReasonRdrListEvent(callbackFunc:Function=null):void 
		{
			super();
			this._callbackFunc = callbackFunc;
		}
		
		public function get callbackFunc():Function
		{
			return _callbackFunc;
		}

	}
}