package hk.org.ha.event.pms.main.reftable {
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class InitRefillQtyAdjTabEvent extends AbstractTideEvent 
	{		
		private var _list:ArrayCollection;
		
		public function InitRefillQtyAdjTabEvent(list:ArrayCollection):void 
		{
			super();
			_list = list;
		}
		
		public function get list():ArrayCollection {
			return _list;
		}
	}
}