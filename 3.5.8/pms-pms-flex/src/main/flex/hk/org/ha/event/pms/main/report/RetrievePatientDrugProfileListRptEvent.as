package hk.org.ha.event.pms.main.report {
	
	import hk.org.ha.model.pms.vo.report.PatientDrugProfileListRptInfo;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePatientDrugProfileListRptEvent extends AbstractTideEvent
	{
		private var _patientDrugProfileListRptInfo:PatientDrugProfileListRptInfo;
		
		public function RetrievePatientDrugProfileListRptEvent(patientDrugProfileListRptInfo:PatientDrugProfileListRptInfo):void 
		{
			super();
			_patientDrugProfileListRptInfo = patientDrugProfileListRptInfo;
		}
		
		public function get patientDrugProfileListRptInfo():PatientDrugProfileListRptInfo {
			return _patientDrugProfileListRptInfo;
		}
	}
}