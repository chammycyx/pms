package hk.org.ha.event.pms.main.vetting {
	
	import flash.events.Event;
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;

	public class EndVettingEvent extends AbstractTideEvent {
		
		private var _suspendCode:String;
		
		private var _overrideLocalDuplicateCheck:Boolean;
		
		public function EndVettingEvent(suspendCode:String=null, overrideLocalDuplicateCheck:Boolean=false) {
			super();
			_suspendCode = suspendCode;
			_overrideLocalDuplicateCheck = overrideLocalDuplicateCheck;
		}
		
		public function get suspendCode():String {
			return _suspendCode;
		}
		
		public function get overrideLocalDuplicateCheck():Boolean {
			return _overrideLocalDuplicateCheck;
		}
		
		public function set overrideLocalDuplicateCheck(overrideLocalDuplicateCheck:Boolean):void {
			_overrideLocalDuplicateCheck = overrideLocalDuplicateCheck;
		}
		
		public override function clone():Event {
			return new EndVettingEvent(suspendCode, overrideLocalDuplicateCheck);
		}
	}
}