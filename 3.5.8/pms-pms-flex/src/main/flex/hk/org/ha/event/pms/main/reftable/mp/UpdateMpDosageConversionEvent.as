package hk.org.ha.event.pms.main.reftable.mp
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class UpdateMpDosageConversionEvent extends AbstractTideEvent
	{
		private var _pmsIpDosageConvSetList:ArrayCollection;
		private var _markDeletePmsIpDosageConvSetList:ArrayCollection;
		
		public function get pmsIpDosageConvSetList():ArrayCollection
		{
			return _pmsIpDosageConvSetList;
		}
		
		public function get markDeletePmsIpDosageConvSetList():ArrayCollection
		{
			return _markDeletePmsIpDosageConvSetList;
		}
		
		public function UpdateMpDosageConversionEvent(pmsIpDosageConvSetList:ArrayCollection, markDeletePmsIpDosageConvSetList:ArrayCollection):void 
		{
			super();
			_pmsIpDosageConvSetList = pmsIpDosageConvSetList;
			_markDeletePmsIpDosageConvSetList = markDeletePmsIpDosageConvSetList;
		}
		
	}
}