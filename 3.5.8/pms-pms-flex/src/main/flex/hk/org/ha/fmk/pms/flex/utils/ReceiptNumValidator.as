package hk.org.ha.fmk.pms.flex.utils {
	public class ReceiptNumValidator {
		
		public static function isValidReceiptNum(receiptNum:String):Boolean {			
			if (receiptNum.length < 21 && new RegExp("^(T|R|D|Z)[0-9a-zA-Z]*$").test(receiptNum)) {
				return true;
			}
			
			return false;
		}
	}
}