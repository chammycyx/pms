package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.vo.rx.RxItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveWorkstoreDrugForMpOrderEditEvent extends AbstractTideEvent 
	{
		private var _rxItem:RxItem;
		private var _itemCode:String;
		private var _callBackFunction:Function;	
		
		public function RetrieveWorkstoreDrugForMpOrderEditEvent(rxItemVal:RxItem, itemCodeVal:String, functionVal:Function=null):void 
		{
			super();
			_rxItem 			= rxItemVal;			
			_itemCode 			= itemCodeVal;
			_callBackFunction 	= functionVal;
		}
		
		public function get rxItem():RxItem
		{
			return _rxItem;
		}
				
		public function get itemCode():String
		{
			return _itemCode;
		}
		
		public function get callBackFunction():Function 
		{
			return _callBackFunction;
		}
	}
}