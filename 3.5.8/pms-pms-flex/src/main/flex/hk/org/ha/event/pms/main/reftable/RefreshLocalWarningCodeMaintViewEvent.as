package hk.org.ha.event.pms.main.reftable {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshLocalWarningCodeMaintViewEvent extends AbstractTideEvent 
	{
		private var _validItemCode:Boolean;
		private var _followUp:Boolean;
		
		public function RefreshLocalWarningCodeMaintViewEvent(validItemCode:Boolean, followUp:Boolean):void 
		{
			super();
			_validItemCode = validItemCode;
			_followUp = followUp;
		}
		
		public function get followUp():Boolean {
			return _followUp;
		}
		public function get validItemCode():Boolean {
			return _validItemCode;
		}
	}
}