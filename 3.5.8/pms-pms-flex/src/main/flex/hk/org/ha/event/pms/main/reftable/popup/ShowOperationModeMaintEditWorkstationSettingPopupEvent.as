package hk.org.ha.event.pms.main.reftable.popup {
	
	import hk.org.ha.model.pms.persistence.reftable.Workstation;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowOperationModeMaintEditWorkstationSettingPopupEvent extends AbstractTideEvent 
	{
		private var _workstation:Workstation;
		private var _redirectList:ArrayCollection;
		private var _pivasPrinterEnabledFlag:Boolean;
		private var _addWorkstationFlag:Boolean;
		
		public function ShowOperationModeMaintEditWorkstationSettingPopupEvent(workstation:Workstation,
																			   redirectList:ArrayCollection, 
																			   pivasPrinterEnabledFlag:Boolean,
																			   addWorkstationFlag:Boolean=false):void 
		{
			super();
			_workstation = workstation;
			_redirectList = redirectList;
			_pivasPrinterEnabledFlag = pivasPrinterEnabledFlag;
			_addWorkstationFlag = addWorkstationFlag;
		}
		
		public function get workstation():Workstation {
			return _workstation;
		}
		
		public function get redirectList():ArrayCollection {
			return _redirectList;
		}
		
		public function get pivasPrinterEnabledFlag():Boolean {
			return _pivasPrinterEnabledFlag;
		}
		
		public function get addWorkstationFlag():Boolean {
			return _addWorkstationFlag;
		}
	}
}