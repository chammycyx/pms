package hk.org.ha.event.pms.main.onestop.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowReprintViewEvent extends AbstractTideEvent 
	{
		private var _srcEntryView:String;
		private var _clearMessages:Boolean;
		private var _selectReprintTab:Boolean;
		
		public function ShowReprintViewEvent(srcEntryView:String=null, clearMessages:Boolean=true, selectReprintTab:Boolean=false):void 
		{
			super();
			_srcEntryView = srcEntryView;
			_clearMessages = clearMessages;
			_selectReprintTab = selectReprintTab;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
		
		public function get selectReprintTab():Boolean {
			return _selectReprintTab;
		}
		
		public function get srcEntryView():String {
			return _srcEntryView;
		}
	}
}