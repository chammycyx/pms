package hk.org.ha.event.pms.main.reftable {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ClearDrsPatientMaintViewEvent extends AbstractTideEvent 
	{		
		public function ClearDrsPatientMaintViewEvent():void 
		{
			super();
		}
	}
}