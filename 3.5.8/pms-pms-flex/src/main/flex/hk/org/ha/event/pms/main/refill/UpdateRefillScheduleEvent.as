package hk.org.ha.event.pms.main.refill {
	
	import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
	import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateRefillScheduleEvent extends AbstractTideEvent 
	{		
		private var _refillSchedule:RefillSchedule;
		private var _updateAction:String;
		private var _orderViewInfo:OrderViewInfo;
		
		public function UpdateRefillScheduleEvent(updateAction:String, refillSchedule:RefillSchedule, orderViewInfo:OrderViewInfo = null ):void 
		{
			super();
			_updateAction = updateAction;
			_refillSchedule = refillSchedule;			
			_orderViewInfo = orderViewInfo;
		}
		
		public function get updateAction():String {
			return _updateAction;
		}	
		
		public function get refillSchedule():RefillSchedule {
			return _refillSchedule;
		}
		
		public function get orderViewInfo():OrderViewInfo {
			return _orderViewInfo;
		}	
	}
}