package hk.org.ha.event.pms.main.vetting.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowHkidOtherDocPopupEvent extends AbstractTideEvent {
		
		private var _callbackFunc:Function;
		
		public function ShowHkidOtherDocPopupEvent(callbackFunc:Function) {
			super();
			_callbackFunc = callbackFunc;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}
