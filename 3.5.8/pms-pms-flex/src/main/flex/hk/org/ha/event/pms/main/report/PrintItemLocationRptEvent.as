package hk.org.ha.event.pms.main.report {
	
	import hk.org.ha.model.pms.vo.report.ItemLocationRpt;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class PrintItemLocationRptEvent extends AbstractTideEvent 
	{
		private var _printItemLocationRptList:ArrayCollection;
		
		public function PrintItemLocationRptEvent(printItemLocationRptList:ArrayCollection):void 
		{
			super();
			_printItemLocationRptList = printItemLocationRptList;
		}
		
		public function get printItemLocationRptList():ArrayCollection {
			return _printItemLocationRptList;
		}
	}
}