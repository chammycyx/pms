package hk.org.ha.control.pms.main.vetting
{	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	import mx.controls.Alert;
	import mx.core.IFlexDisplayObject;
	import mx.core.UIComponent;
	import mx.managers.PopUpManager;
	import mx.rpc.AsyncToken;
	
	import hk.org.ha.event.pms.main.vetting.CheckDhMappingUpToDateEvent;
	import hk.org.ha.event.pms.main.vetting.RetrieveFormattedDrugOrderTlfEvent;
	import hk.org.ha.event.pms.main.vetting.RetrieveMedOrderItemForOrderEditEvent;
	import hk.org.ha.event.pms.main.vetting.UpdateMedOrderItemFromOrderEditEvent;
	import hk.org.ha.event.pms.main.vetting.popup.ShowCorpDuplicateItemEvent;
	import hk.org.ha.event.pms.main.vetting.show.ShowOrderViewEvent;
	import hk.org.ha.model.pms.biz.vetting.OrderEditServiceBean;
	import hk.org.ha.model.pms.persistence.disp.MedOrder;
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	import hk.org.ha.model.pms.persistence.disp.MedOrderItemAlert;
	import hk.org.ha.model.pms.persistence.disp.PharmOrder;
	import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
	import hk.org.ha.model.pms.udt.YesNoBlankFlag;
	import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
	import hk.org.ha.model.pms.udt.rx.RxItemType;
	import hk.org.ha.model.pms.udt.vetting.ActionStatus;
	import hk.org.ha.model.pms.vo.alert.mds.AlertDdim;
	import hk.org.ha.model.pms.vo.charging.MoiCapdChargeInfo;
	import hk.org.ha.model.pms.vo.charging.MoiChargeInfo;
	import hk.org.ha.model.pms.vo.rx.CapdItem;
	import hk.org.ha.model.pms.vo.rx.Dose;
	import hk.org.ha.model.pms.vo.rx.DoseGroup;
	import hk.org.ha.model.pms.vo.rx.RxDrug;
	import hk.org.ha.model.pms.vo.rx.RxItem;
	import hk.org.ha.model.pms.vo.security.PropMap;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("orderEditServiceCtl", restrict="true")]
	public class OrderEditServiceCtl
	{
		[In]
		public var orderEditService:OrderEditServiceBean
		
		[In]
		public var ctx:Context;
		
		[In]
		public var pharmOrder:PharmOrder;
		
		[In]
		public var medOrderItem:MedOrderItem;
		
		[In]
		public var propMap:PropMap;
		
		[In]
		public var corpDuplicateItemList:ArrayCollection;
		
		[In]
		public var moiChargeInfoList:ArrayCollection;
		
		[In]
		public var rxItemForOrderEdit:RxItem;
		
		private var dummyMoiChargeInfo:MoiChargeInfo;
		
		private var dummyMoiCapdChargeInfo:MoiCapdChargeInfo;
		
		private var tmpMedOrderItem:MedOrderItem;
		
		private var selectedIndex:int;
		
		private var nextEvent:Event;
		
		private var callBackFunction:Function;
		
		private var capdCancelFunc:Function;
		
		[Observer]
		public function retrieveFormattedDrugOrderTlf(evt:RetrieveFormattedDrugOrderTlfEvent):void {
			callBackFunction = evt.callBackFunc;
			orderEditService.retrieveFormattedDrugOrderTlf(evt.rxItem,retrieveFormattedDrugOrderTlfResult);
		}
		
		private function retrieveFormattedDrugOrderTlfResult(evt:TideResultEvent):void {
			if (callBackFunction != null) {
				callBackFunction(evt.result);
			}
		}
		
		[Observer]
		public function retrieveMedOrderItemForOrderEdit(evt:RetrieveMedOrderItemForOrderEditEvent):void {
			selectedIndex = evt.selectedIndex;
			nextEvent = evt.event;
			
			orderEditService.retrieveMedOrderItemForOrderEdit(selectedIndex,retrieveMedOrderItemForOrderEditResult);			
		}
		
		private function retrieveMedOrderItemForOrderEditResult(evt:TideResultEvent):void {
			var medOrder:MedOrder = pharmOrder.medOrder;
			if ( medOrderItem != null ) {
				medOrder.medOrderItemList.setItemAt(medOrderItem, selectedIndex);	
				medOrderItem.medOrder = medOrder;				
			} else if ( selectedIndex < medOrder.medOrderItemList.length ) {			
				medOrder.medOrderItemList.removeItemAt(selectedIndex);				
			}
			
			pharmOrder.relinkAllItem();			
			evt.context.dispatchEvent(nextEvent);
		}
		
		[Observer]
		public function updateMedOrderFromOrderEdit(evt:UpdateMedOrderItemFromOrderEditEvent):void {
			
			selectedIndex = evt.selectedIndex;	
			tmpMedOrderItem = evt.medOrderItem;
			callBackFunction = evt.callBackFunction;
			capdCancelFunc = evt.cancelFunctionCall;
			
			if ( ! validateEmptyPharmLine(tmpMedOrderItem)) {
				return;
			}
			
			tmpMedOrderItem.dislink();
			orderEditService.updateMedOrderFromOrderEdit(selectedIndex, tmpMedOrderItem, evt.skipDuplicateCheckFlag, updateMedOrderFromOrderEditResult);			
		}
		
		private function validateEmptyPharmLine(checkMedOrderItem:MedOrderItem):Boolean {
			if (checkMedOrderItem.rxItemType == RxItemType.Capd) {
				// validation only applies to item edit screen of non-CAPD item
				// (for CAPD item, this function is triggered from item conversion of CAPD edit screen, it is not possible
				//  to have empty pharmacy line) 
				return true;
			}
			
			var emptyPharmLine:Boolean = false;
			for each (var pharmOrderItem:PharmOrderItem in checkMedOrderItem.pharmOrderItemList) {
				for each (var doseGroup:DoseGroup in pharmOrderItem.regimen.doseGroupList) {
					for each (var dose:Dose in doseGroup.doseList) {
						if (dose.dailyFreq == null || dose.dailyFreq.code == null || dose.dailyFreq.code == '') {
							emptyPharmLine = true;
						}
					}					
				}
			}

			if (emptyPharmLine) {
				if ( ctx.orderEditView ) {
					dispatchEvent(ctx.orderEditView.getSysMsgEvent("0164", 
						null, 
						function(evt:MouseEvent):void {
							PopUpManager.removePopUp((UIComponent(evt.currentTarget).parentDocument) as IFlexDisplayObject);
							
							ctx.orderEditView.disableProcessFlag();
							ctx.orderEditView.setFocusToFirstItemCode();
							
							ctx.orderEditView.poView.list.setFocus();
						}));
				}
				
				return false;
			}
			
			return true;
		}
		
		private function updateItemNum(itemNumList:ListCollectionView):void {
			var i:int=0;
			for each ( var poi:PharmOrderItem in tmpMedOrderItem.pharmOrderItemList ) {
				poi.orgItemNum = itemNumList.getItemAt(i) as Number;
				poi.itemNum = itemNumList.getItemAt(i+1) as Number;
				i+=2;
			}
		}
		
		private function updateMedOrderFromOrderEditResult(evt:TideResultEvent):void {						
			
			updateItemNum(evt.result as ListCollectionView);
			
			if (corpDuplicateItemList != null && corpDuplicateItemList.length > 0) {				
				dispatchEvent(new ShowCorpDuplicateItemEvent(corpDuplicateItemList, 
					function():void {
						dispatchEvent(new UpdateMedOrderItemFromOrderEditEvent(selectedIndex, tmpMedOrderItem, callBackFunction, true));
					},
					function():void {
						if ( ctx.orderEditView ) {
							ctx.orderEditView.disableProcessFlag();
							ctx.orderEditView.setFocusToFirstItemCode();
						}
						pharmOrder.relinkAllItem();
						if ( medOrderItem ) {
							medOrderItem.medOrder = pharmOrder.medOrder;			
							pharmOrder.medOrder.medOrderItemList.setItemAt(medOrderItem, selectedIndex);
						}
						
						if(capdCancelFunc != null)
						{
							capdCancelFunc();
						}	
					}
				));
				return;
			}					
			
			if ( callBackFunction != null ) {
				callBackFunction();
			}
			
			pharmOrder.relinkAllItem();
			tmpMedOrderItem.medOrder = pharmOrder.medOrder;			
			pharmOrder.medOrder.medOrderItemList.setItemAt(tmpMedOrderItem, selectedIndex);			
			
			var medOrder:MedOrder = pharmOrder.medOrder;
				
			if( moiChargeInfoList != null && moiChargeInfoList.length > 0 ){
				for each ( var moiChargeInfo:MoiChargeInfo in moiChargeInfoList ) {
					var updateMoi:MedOrderItem = MedOrderItem(medOrder.medOrderItemList.getItemAt(moiChargeInfo.medOrderItemIndex));
					updateMoi.cmsChargeFlag = moiChargeInfo.cmsChargeFlag;
					updateMoi.chargeFlag = moiChargeInfo.chargeFlag;
					updateMoi.unitOfCharge = moiChargeInfo.unitOfCharge;
					
					if( moiChargeInfo.moiCapdChargeInfoList != null && moiChargeInfo.moiCapdChargeInfoList.length > 0 ){
						for each( var moiCapdChargeInfo:MoiCapdChargeInfo in moiChargeInfo.moiCapdChargeInfoList ){
							var selectedCapdItem:CapdItem = CapdItem(updateMoi.capdRxDrug.capdItemList.getItemAt( moiCapdChargeInfo.medOrderCapdItemIndex ));
							selectedCapdItem.unitOfCharge = moiCapdChargeInfo.unitOfCharge;
						}
					}
				}
			}
			
			medOrder.maxItemNum = 0;
			for each (var moi:MedOrderItem in medOrder.medOrderItemList) {
				if (medOrder.maxItemNum < moi.itemNum) {
					medOrder.maxItemNum = moi.itemNum;
				}
			}
			
			tmpMedOrderItem.updateDdiRelatedAlert();
			
			if (medOrder.isMpDischarge() && rxItemForOrderEdit != null) {						
				if (tmpMedOrderItem.rxItem.isRxDrug && rxItemForOrderEdit.isRxDrug) {
					RxDrug(rxItemForOrderEdit).formattedDrugNameTlf = RxDrug(tmpMedOrderItem.rxItem).formattedDrugNameTlf;
					RxDrug(rxItemForOrderEdit).formattedDrugOrderTlf = RxDrug(tmpMedOrderItem.rxItem).formattedDrugOrderTlf; 
					RxDrug(rxItemForOrderEdit).drugNameHtml = RxDrug(tmpMedOrderItem.rxItem).drugNameHtml;
					RxDrug(rxItemForOrderEdit).drugNameTlf = RxDrug(tmpMedOrderItem.rxItem).drugNameTlf;
					RxDrug(rxItemForOrderEdit).drugNameXml = RxDrug(tmpMedOrderItem.rxItem).drugNameXml;
					RxDrug(rxItemForOrderEdit).drugOrderHtml = RxDrug(tmpMedOrderItem.rxItem).drugOrderHtml;
					RxDrug(rxItemForOrderEdit).drugOrderTlf = RxDrug(tmpMedOrderItem.rxItem).drugOrderTlf;
					RxDrug(rxItemForOrderEdit).drugOrderXml = RxDrug(tmpMedOrderItem.rxItem).drugOrderXml;
				}
				
				tmpMedOrderItem.updateRxItem(rxItemForOrderEdit);
				
				if (rxItemForOrderEdit.isInjectionRxDrug) {
					tmpMedOrderItem.rxItemType = RxItemType.Injection;
				} else if (rxItemForOrderEdit.isOralRxDrug) {
					tmpMedOrderItem.rxItemType = RxItemType.Oral;
				}
			}
			
			var showOrderViewEvent:ShowOrderViewEvent = new ShowOrderViewEvent();
			showOrderViewEvent.vetOrderLine = selectedIndex;
			showOrderViewEvent.skipDuplicateCheckFlag = true;
			showOrderViewEvent.skipZeroQtyConfirmCount= tmpMedOrderItem.pharmOrderItemList.length;
			showOrderViewEvent.skipMdsAlertFlag       = true;
			showOrderViewEvent.skipInfusion           = true;
			evt.context.dispatchEvent( showOrderViewEvent );		
		}
		
		private function updateMaxPharmOrderItemForOrderEditResult(evt:TideResultEvent):void
		{
			if ( callBackFunction != null ) {
				var itemNum:Array = evt.result as Array;
				callBackFunction(itemNum[0], itemNum[1]);
			}
		}
		
		[Observer]
		public function checkDhMappingUpToDate(checkDhMappingUpToDateEvent:CheckDhMappingUpToDateEvent):void {
			orderEditService.isDhMappingUpToDate(checkDhMappingUpToDateEvent.index, function(tideResultEvent:TideResultEvent):void {
				checkDhMappingUpToDateEvent.callback(tideResultEvent.result as Boolean);
			});
		}
	}
}
