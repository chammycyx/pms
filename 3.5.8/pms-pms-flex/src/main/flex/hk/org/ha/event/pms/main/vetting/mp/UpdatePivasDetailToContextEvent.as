package hk.org.ha.event.pms.main.vetting.mp {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.pivas.PivasWorklist;
	
	public class UpdatePivasDetailToContextEvent extends AbstractTideEvent {
		
		private var _pivasWorklist:PivasWorklist;
		
		public function UpdatePivasDetailToContextEvent(pivasWorklist:PivasWorklist) 
		{
			super();			
			_pivasWorklist = pivasWorklist;
		}		
		
		public function get pivasWorklist():PivasWorklist
		{
			return _pivasWorklist;
		}
	}
}