package hk.org.ha.control.pms.main.enquiry {
	
	import hk.org.ha.event.pms.main.enquiry.RefreshDispItemStatusEnqViewEvent;
	import hk.org.ha.event.pms.main.enquiry.RetrieveDispItemStatusEnqListEvent;
	import hk.org.ha.model.pms.biz.enquiry.DispItemStatusEnqServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("dispItemStatusEnqServiceCtl", restrict="true")]
	public class DispItemStatusEnqServiceCtl 
	{
	
		[In]
		public var dispItemStatusEnqService:DispItemStatusEnqServiceBean;
					
		[Observer]
		public function retrieveDispItemStatusEnqList(evt:RetrieveDispItemStatusEnqListEvent):void
		{
			
			dispItemStatusEnqService.retrieveDispItemStatusEnqList(evt.dispItemStatusEnq, retrieveDispItemStatusEnqListResult);
		
		}
		
		public function retrieveDispItemStatusEnqListResult(evt:TideResultEvent):void{	
			evt.context.dispatchEvent(new RefreshDispItemStatusEnqViewEvent() );
		}
		
	}
}

