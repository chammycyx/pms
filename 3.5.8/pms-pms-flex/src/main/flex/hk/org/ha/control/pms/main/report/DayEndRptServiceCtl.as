package hk.org.ha.control.pms.main.report {
	import hk.org.ha.event.pms.main.report.PrintDayEndRptEvent;
	import hk.org.ha.event.pms.main.report.RefreshDayEndRptEvent;
	import hk.org.ha.event.pms.main.report.RetrieveDayEndRptEvent;
	import hk.org.ha.model.pms.biz.report.DayEndRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("dayEndRptServiceCtl", restrict="true")]
	public class DayEndRptServiceCtl 
	{
		[In]
		public var dayEndRptService:DayEndRptServiceBean;
		
		[Observer]
		public function retrieveDayEndRpt(evt:RetrieveDayEndRptEvent):void
		{
			In(Object(dayEndRptService).retrieveSuccess)
			dayEndRptService.retrieveDayEndRpt(evt.selectedTab, evt.workstoreCode, evt.date, retrieveDayEndRptResult);
		}
		
		private function retrieveDayEndRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshDayEndRptEvent(Object(dayEndRptService).retrieveSuccess));
		}
		
		[Observer]
		public function printDayEndRpt(evt:PrintDayEndRptEvent):void
		{
			dayEndRptService.printDayEndRpt();
		}
		
	}
}
