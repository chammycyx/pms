package hk.org.ha.event.pms.main.vetting.mp
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveOnhandMedicationListEvent extends AbstractTideEvent
	{
		public function RetrieveOnhandMedicationListEvent()
		{
			super();
		}
	}
}