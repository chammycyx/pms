package hk.org.ha.control.pms.main.checkissue.mp{
	
	import hk.org.ha.event.pms.main.checkissue.mp.RefreshLabelDeleteViewEvent;
	import hk.org.ha.event.pms.main.checkissue.mp.RetrieveDeliveryItemListEvent;
	import hk.org.ha.event.pms.main.checkissue.mp.UpdateDeliveryItemStatusToDeletedEvent;
	import hk.org.ha.model.pms.biz.checkissue.mp.LabelDeleteServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("labelDeleteServiceCtl", restrict="true")]
	public class LabelDeleteServiceCtl 
	{
		[In]
		public var labelDeleteService:LabelDeleteServiceBean;
		
		[Observer]
		public function retrieveDeliveryItemList(evt:RetrieveDeliveryItemListEvent):void
		{
			In(Object(labelDeleteService).msgCode)
			labelDeleteService.retrieveLabelDeleteItemList(evt.trxDate, evt.caseNum, evt.batchNum, evt.mpDispLabelQrCodeXml, retrieveDeliveryItemListResult);
		}
		
		private function retrieveDeliveryItemListResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent( new RefreshLabelDeleteViewEvent(Object(labelDeleteService).msgCode) ) ;
		}
		
		[Observer]
		public function updateDeliveryItemStatusToDeleted(evt:UpdateDeliveryItemStatusToDeletedEvent):void{
			labelDeleteService.updateDeliveryItemStatusToDeleted(evt.labelDelete, retrieveDeliveryItemListResult);
		}

	}
}
