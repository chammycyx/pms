package hk.org.ha.control.pms.main.drug {
	
	import hk.org.ha.event.pms.main.drug.RetrieveSiteListEvent;
	import hk.org.ha.model.pms.biz.drug.SiteListServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("siteListServiceCtl", restrict="true")]
	public class SiteListServiceCtl 
	{
		[In]
		public var siteListService:SiteListServiceBean;
		
		private var callBackFunc:Function;
		
		[Observer]
		public function retrieveSiteList(evt:RetrieveSiteListEvent):void
		{
			callBackFunc = evt.callBackFunc;
			siteListService.retrieveSiteListByFormCode(evt.formCode,retrieveSiteListResult);
		}
		
		private function retrieveSiteListResult(evt:TideResultEvent):void 
		{
			if ( callBackFunc != null ) {
				callBackFunc(evt.result);
			}
		}
		
	}
}
