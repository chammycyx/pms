package hk.org.ha.control.pms.main.vetting {	
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.vetting.RetrieveDrugConversionEvent;
	import hk.org.ha.model.pms.biz.vetting.DrugConversionServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;

    [Bindable]
    [Name("drugConversionServiceCtl", restrict="true")]
    public class DrugConversionServiceCtl 
	{
    
        [In]
        public var drugConversionService:DrugConversionServiceBean;

		[In]
		public var ctx:Context;
		
		private var callBackEvent:Event;
		
        [Observer]
        public function retrieveDrugConversion(evt:RetrieveDrugConversionEvent):void {
			callBackEvent = evt.callBackEvent;
			drugConversionService.retrieveDrugConversion(evt.rxItem, retrieveDrugConversionResult);
        }
		
		private function retrieveDrugConversionResult(evt:TideResultEvent):void {  
			if ( callBackEvent ) {     	
				evt.context.dispatchEvent( callBackEvent );
			}
        }				
    }
}
