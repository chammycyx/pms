package hk.org.ha.event.pms.main.reftable {
	
	import mx.collections.ListCollectionView;
	import mx.core.UIComponent;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateRefillSelectionListEvent extends AbstractTideEvent 
	{		
		private var _updateRefillSelectionList:ListCollectionView;
		private var _delRefillSelectionList:ListCollectionView;
		private var _nextTabIndex:Number;
		private var _nextSubTabIndex:Number;
		private var _showRefillModuleMaintAlertMsg:Boolean;
		private var _caller:UIComponent;
		
		public function UpdateRefillSelectionListEvent(updateRefillSelectionList:ListCollectionView, 
													   delRefillSelectionList:ListCollectionView, 
													   nextTabIndex:Number, 
													   nextSubTabIndex:Number,
													   showRefillModuleMaintAlertMsg:Boolean, 
													   caller:UIComponent):void 
		{
			super();
			_updateRefillSelectionList = updateRefillSelectionList;
			_delRefillSelectionList = delRefillSelectionList;
			_nextTabIndex = nextTabIndex;
			_nextSubTabIndex = nextSubTabIndex;
			_showRefillModuleMaintAlertMsg = showRefillModuleMaintAlertMsg;
			_caller = caller;
		}
		
		public function get updateRefillSelectionList():ListCollectionView
		{
			return _updateRefillSelectionList;
		}
		
		public function get delRefillSelectionList():ListCollectionView
		{
			return _delRefillSelectionList;
		}
		
		public function get nextTabIndex():Number
		{
			return _nextTabIndex;
		}
		
		public function get nextSubTabIndex():Number
		{
			return _nextSubTabIndex;
		}
		
		public function get showRefillModuleMaintAlertMsg():Boolean
		{
			return _showRefillModuleMaintAlertMsg;
		}
		
		public function get caller():UIComponent 
		{
			return _caller;
		}
	}
}