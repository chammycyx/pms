package hk.org.ha.event.pms.main.vetting {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class CancelVettingEvent extends AbstractTideEvent {
		
		private var _callback:Function;
		
		public function CancelVettingEvent(callback:Function=null) {
			super();
			_callback = callback;
		}
		
		public function get callback():Function {
			return _callback;
		}
		
		public function set callback(callback:Function):void {
			_callback = callback;
		}
	}
}
