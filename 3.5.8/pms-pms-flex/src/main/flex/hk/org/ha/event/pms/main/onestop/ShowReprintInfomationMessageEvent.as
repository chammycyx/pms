package hk.org.ha.event.pms.main.onestop {
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowReprintInfomationMessageEvent extends AbstractTideEvent 
	{	
		private var _messages:Array;
		
		public function ShowReprintInfomationMessageEvent(messages:Array):void 
		{
			super();
			_messages = messages;
		}
		
		public function get messages():Array 
		{
			return _messages;
		}
	}
}