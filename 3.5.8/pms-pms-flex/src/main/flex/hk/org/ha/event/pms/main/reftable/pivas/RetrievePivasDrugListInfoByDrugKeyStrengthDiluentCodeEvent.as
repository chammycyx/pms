package hk.org.ha.event.pms.main.reftable.pivas {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrievePivasDrugListInfoByDrugKeyStrengthDiluentCodeEvent extends AbstractTideEvent {
	
		private var _drugKey:Number;
		private var _strength:String;
		private var _diluentCode:String;
		private var _siteCode:String;
		private var _callback:Function;
		
		public function RetrievePivasDrugListInfoByDrugKeyStrengthDiluentCodeEvent(drugKey:Number, strength:String, diluentCode:String, siteCode:String, callback:Function) {
			super();
			_drugKey = drugKey;
			_strength = strength;
			_diluentCode = diluentCode;
			_siteCode = siteCode;
			_callback = callback;
		}
		
		public function get drugKey():Number {
			return _drugKey;
		}
		
		public function get strength():String {
			return _strength;
		}

		public function get diluentCode():String {
			return _diluentCode;
		}
		
		public function get siteCode():String {
			return _siteCode;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}