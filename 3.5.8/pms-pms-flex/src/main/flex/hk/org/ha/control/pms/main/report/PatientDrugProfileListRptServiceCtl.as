package hk.org.ha.control.pms.main.report {
	import hk.org.ha.event.pms.main.report.PrintPatientDrugProfileListRptEvent;
	import hk.org.ha.event.pms.main.report.RefreshPatientDrugProfileListRptEvent;
	import hk.org.ha.event.pms.main.report.RetrievePatientDrugProfileListRptEvent;
	import hk.org.ha.event.pms.main.report.RetrievePatientDrugProfileListRptInfoEvent;
	import hk.org.ha.event.pms.main.report.SetPatientDrugProfileListRptInfoEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.model.pms.biz.report.PatientDrugProfileListRptServiceBean;
	import hk.org.ha.model.pms.vo.report.PatientDrugProfileListRptInfo;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("patientDrugProfileListRptServiceCtl", restrict="true")]
	public class PatientDrugProfileListRptServiceCtl 
	{
		[In]
		public var patientDrugProfileListRptService:PatientDrugProfileListRptServiceBean;
		
		[Observer]
		public function retrievePatientDrugProfileListRptInfo(evt:RetrievePatientDrugProfileListRptInfoEvent):void
		{
			patientDrugProfileListRptService.retrievePatientDrugProfileListRptInfo(retrievePatientDrugProfileListRptInfoResult);
		}
		
		private function retrievePatientDrugProfileListRptInfoResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new SetPatientDrugProfileListRptInfoEvent(evt.result as PatientDrugProfileListRptInfo));
		}
		
		[Observer]
		public function retrievePatientDrugProfileListRpt(evt:RetrievePatientDrugProfileListRptEvent):void
		{
			In(Object(patientDrugProfileListRptService).retrieveSuccess)
			patientDrugProfileListRptService.retrievePatientDrugProfileListRpt(evt.patientDrugProfileListRptInfo, retrievePatientDrugProfileListRptResult, retrievePatientDrugProfileListRptFault);
		}
		
		private function retrievePatientDrugProfileListRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshPatientDrugProfileListRptEvent(Object(patientDrugProfileListRptService).retrieveSuccess));
		}
		
		private function retrievePatientDrugProfileListRptFault(evt:TideFaultEvent):void
		{
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
		}
		
		[Observer]
		public function printPatientDrugProfileListRpt(evt:PrintPatientDrugProfileListRptEvent):void
		{
			patientDrugProfileListRptService.printPatientDrugProfileListRpt();
		}
	}
}
