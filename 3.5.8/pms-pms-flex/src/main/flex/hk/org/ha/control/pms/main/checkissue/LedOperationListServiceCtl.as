package hk.org.ha.control.pms.main.checkissue{
	import hk.org.ha.event.pms.main.checkissue.CheckIssueSendTicketToIqdsEvent;
	import hk.org.ha.event.pms.main.checkissue.RefreshIssueWindowListEvent;
	import hk.org.ha.event.pms.main.checkissue.RefreshTicketLabelEvent;
	import hk.org.ha.event.pms.main.checkissue.RetrieveIssueWindowListEvent;
	import hk.org.ha.event.pms.main.checkissue.RetrieveIssueWindowNumForLedOperationEvent;
	import hk.org.ha.event.pms.main.checkissue.RetrieveLedListEvent;
	import hk.org.ha.event.pms.main.checkissue.RetrieveLedOperationLogListEvent;
	import hk.org.ha.event.pms.main.checkissue.SendTicketListToIqdsEvent;
	import hk.org.ha.event.pms.main.checkissue.SendTicketToIqdsEvent;
	import hk.org.ha.event.pms.main.checkissue.SetIssueWindowNumForLedOperationEvent;
	import hk.org.ha.event.pms.main.checkissue.SetTicketListSendAllTicketCheckBoxEvent;
	import hk.org.ha.model.pms.biz.checkissue.LedOperationListServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("ledOperationListServiceCtl", restrict="true")]
	public class LedOperationListServiceCtl 
	{
		public var _singleSend:Boolean = false;
		private var sendTicketToIqdsCallback:Function;
		
		[In]
		public var ledOperationListService:LedOperationListServiceBean;
		
		[Observer]
		public function retrieveIssueWindowList(evt:RetrieveIssueWindowListEvent):void
		{
			ledOperationListService.retrieveIssueWindowList(retrieveIssueWindowListResult);
		}
		
		private function retrieveIssueWindowListResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshIssueWindowListEvent());
		}
		
		[Observer]
		public function retrieveLedList(evt:RetrieveLedListEvent):void
		{
			ledOperationListService.retrieveLedList();
		}
		
		[Observer]
		public function retrieveLedOperationLogList(evt:RetrieveLedOperationLogListEvent):void
		{
			ledOperationListService.retrieveLedOperationLogList(evt.iqdsAction);
		}
		
		[Observer]
		public function sendTicketToIqds(evt:SendTicketToIqdsEvent):void
		{
			ledOperationListService.sendTicketToIqds(evt.iqdsMessageContentList, sendTicketToIqdsResult);
			_singleSend = evt.singleSend;
			sendTicketToIqdsCallback = evt.callbackFunc;
		}
		
		private function sendTicketToIqdsResult(evt:TideResultEvent):void
		{
			if(_singleSend){
				evt.context.dispatchEvent(new RefreshTicketLabelEvent());
			}
			if (sendTicketToIqdsCallback != null) {
				sendTicketToIqdsCallback();
			}
		}
		
		[Observer]
		public function checkIssueSendTicketToIqds(evt:CheckIssueSendTicketToIqdsEvent):void
		{
			ledOperationListService.sendTicketToIqds(evt.iqdsMessageContentList);
		}
		
		[Observer]
		public function sendTicketListToIqds(evt:SendTicketListToIqdsEvent):void
		{
			ledOperationListService.sendTicketListToIqds(evt.iqdsMessageContentList, evt.ledCheckedList, evt.issueWindowNum, sendTicketListToIqdsResult);
		}
		
		private function sendTicketListToIqdsResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new SetTicketListSendAllTicketCheckBoxEvent());
		}
		
		[Observer]
		public function retrieveIssueWindowNumForLedOperation(evt:RetrieveIssueWindowNumForLedOperationEvent):void
		{
			ledOperationListService.retrieveIssueWindowNum(retrieveIssueWindowNumForLedOperationResult);
		}
		
		private function retrieveIssueWindowNumForLedOperationResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new SetIssueWindowNumForLedOperationEvent(evt.result as Number));
		}
	}
}
