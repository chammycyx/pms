package hk.org.ha.event.pms.main.enquiry
{	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSfiInvoiceSummaryEvent extends AbstractTideEvent 
	{
		private var _sfiInvoiceId:Number;
		
		public function RetrieveSfiInvoiceSummaryEvent(sfiInvoiceId:Number):void
		{
			super();
			_sfiInvoiceId = sfiInvoiceId;
		}
		
		public function get sfiInvoiceId():Number
		{
			return _sfiInvoiceId;
		}
	}
}