package hk.org.ha.event.pms.main.checkissue.mp
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDeliveryItemListForUnlinkDrugEvent extends AbstractTideEvent 
	{
		private var _mpDispLabelQrCodeXml:String;
		private var _batchDate:Date;
		private var _batchNum:String;
		private var _caseNum:String;
 		
		
		public function RetrieveDeliveryItemListForUnlinkDrugEvent(
			batchDate:Date	 ,batchNum:String ,caseNum:String ,mpDispLabelQrCodeXml:String			):void 
		{
			super();
			_mpDispLabelQrCodeXml = mpDispLabelQrCodeXml;
			_batchDate = batchDate;
			_batchNum = batchNum;
			_caseNum = caseNum;
		}
		
		public function get mpDispLabelQrCodeXml():String {
			return _mpDispLabelQrCodeXml;
		}
		public function get batchDate():Date {
			return _batchDate ;
		}
		public function get batchNum():String {
			return _batchNum;
		}
		public function get caseNum():String {
			return _caseNum;
		}

	}
}