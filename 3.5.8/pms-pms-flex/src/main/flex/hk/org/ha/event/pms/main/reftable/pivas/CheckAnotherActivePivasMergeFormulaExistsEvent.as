package hk.org.ha.event.pms.main.reftable.pivas
{
	import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormula;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CheckAnotherActivePivasMergeFormulaExistsEvent extends AbstractTideEvent
	{
		private var _pivasFormula:PivasFormula;
		
		private var _successCallback:Function;
		
		public function CheckAnotherActivePivasMergeFormulaExistsEvent(pivasFormula:PivasFormula, successCallback:Function)
		{
			super();
			_pivasFormula = pivasFormula;
			_successCallback = successCallback;
		}
		
		public function get pivasFormula():PivasFormula
		{
			return _pivasFormula;
		}
		
		public function get successCallback():Function
		{
			return _successCallback;
		}
	}
}