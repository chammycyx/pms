package hk.org.ha.event.pms.main.vetting.mp
{
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CheckConflictExistsEvent extends AbstractTideEvent
	{
		private var _medProfile:MedProfile;
		
		private var _medProfileItemList:ArrayCollection;
		
		private var _replenishmentList:ArrayCollection;
		
		public function CheckConflictExistsEvent(medProfile:MedProfile, medProfileItemList:ArrayCollection, replenishmentList:ArrayCollection)
		{
			super();
			_medProfile = medProfile
			_medProfileItemList = medProfileItemList;
			_replenishmentList = replenishmentList;
		}
		
		public function get medProfile():MedProfile
		{
			return _medProfile;
		}
		
		public function get medProfileItemList():ArrayCollection
		{
			return _medProfileItemList;
		}
		
		public function get replenishmentList():ArrayCollection
		{
			return _replenishmentList;
		}
	}
}