package hk.org.ha.control.pms.main.checkissue.mp{
	
	import hk.org.ha.event.pms.main.checkissue.mp.SendDeliveryEvent;
	import hk.org.ha.event.pms.main.checkissue.mp.SendDeliveryResultEvent;
	import hk.org.ha.event.pms.main.checkissue.mp.UpdateDeliveryDateEvent;
	import hk.org.ha.event.pms.main.checkissue.mp.ReadyToSendDeliveryListEvent;
	import hk.org.ha.event.pms.main.checkissue.mp.BatchSendDeliveryListEvent;
	import hk.org.ha.event.pms.main.checkissue.mp.SentBatchDeliveryListEvent;
	import hk.org.ha.event.pms.main.checkissue.mp.UpdateReadySendOutListCounterEvent;
	import hk.org.ha.model.pms.biz.checkissue.mp.SendServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("sendServiceCtl", restrict="true")]
	public class SendServiceCtl 
	{
		[In]
		public var sendService:SendServiceBean;
		
		[Observer]
		public function sendDelivery(evt:SendDeliveryEvent):void
		{
			sendService.sendDelivery(evt.batchDate, evt.batchNum, sendDeliveryResult);
		}
		
		private function sendDeliveryResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent( new SendDeliveryResultEvent() );
		}
		
		private function sentBatchDeliveryListResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent( new SentBatchDeliveryListEvent() );
		}
		
		private function readyToSendDeliveryResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent( new UpdateReadySendOutListCounterEvent() );
		}
		
		[Observer]
		public function updateDeliveryDate(evt:UpdateDeliveryDateEvent):void
		{
			sendService.updateDeliveryDate(evt.sentDelivery, sendDeliveryResult);
		}
		
		[Observer]
		public function readyToSendDelivery(evt:ReadyToSendDeliveryListEvent):void
		{
			sendService.readyToSendDelivery(readyToSendDeliveryResult);
		}
		
		[Observer]
		public function batchSendDelivery(evt:BatchSendDeliveryListEvent):void
		{
			sendService.batchSendDelivery(evt.readyToSendDeliveryList, sentBatchDeliveryListResult);
		}
		
		
		
	}
}
