package hk.org.ha.event.pms.main.vetting.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowOrderEditChestViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		private var _initFlag:Boolean;
		
		public function ShowOrderEditChestViewEvent(clearMessages:Boolean=true, initFlag:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
			_initFlag      = initFlag;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
		
		public function get initFlag():Boolean {
			return _initFlag;
		}
	}
}