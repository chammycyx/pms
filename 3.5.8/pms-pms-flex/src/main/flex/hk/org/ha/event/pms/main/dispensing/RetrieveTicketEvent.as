package hk.org.ha.event.pms.main.dispensing
{
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.udt.ticketgen.TicketType;
	
	public class RetrieveTicketEvent extends AbstractTideEvent 
	{		
		private var _ticketType:TicketType;

		public function RetrieveTicketEvent(ticketType:TicketType):void 
		{
			super();
			_ticketType = ticketType;
		}
		
		public function get ticketType():TicketType
		{
			return _ticketType;
		}
	}
}