package hk.org.ha.event.pms.main.pivas
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetPivasFormulaListEvent extends AbstractTideEvent 
	{		
		private var _pivasFormulaList:ArrayCollection;
		
		public function SetPivasFormulaListEvent(pivasFormulaList:ArrayCollection):void 
		{
			super();
			_pivasFormulaList = pivasFormulaList;
		}
		
		public function get pivasFormulaList():ArrayCollection {
			return _pivasFormulaList;
		}
	}
}