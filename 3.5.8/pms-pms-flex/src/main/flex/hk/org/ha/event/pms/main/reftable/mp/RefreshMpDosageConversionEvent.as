package hk.org.ha.event.pms.main.reftable.mp
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshMpDosageConversionEvent extends AbstractTideEvent 
	{
		private var _saveSuccess:Boolean;
		private var _errorCode:String;
		private var _errorParam:Array;

		public function RefreshMpDosageConversionEvent(saveSuccess:Boolean, errorCode:String, errorParam:Array):void 
		{
			super();
			_saveSuccess = saveSuccess;
			_errorCode = errorCode;
			_errorParam = errorParam;
		}
		
		public function get saveSuccess():Boolean
		{
			return _saveSuccess;
		}
		
		public function get errorCode():String
		{
			return _errorCode;
		}
		
		public function get errorParam():Array
		{
			return _errorParam;
		}
	}
}