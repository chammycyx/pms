package hk.org.ha.control.pms.main.reftable.mp {
	
	import hk.org.ha.event.pms.main.reftable.UpdateMpQtyDurationConversionListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.ClearMpQtyDurationConversionMaintViewEvent;
	import hk.org.ha.event.pms.main.reftable.mp.PrintMpQtyDurationConversionRptEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RefreshMpDurConversionRptListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RefreshMpQtyConversionRptListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RefreshMpQtyDurationConversionViewEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RefreshPrintMpQtyDurationConversionRptEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RefreshQtyConversionInsuRptListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveMpDurConversionRptListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveMpQtyConversionRptListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveMpQtyDurationConversionListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveQtyConversionInsuRptListEvent;
	import hk.org.ha.model.pms.biz.reftable.mp.MpQtyDurationConversionRptServiceBean;
	import hk.org.ha.model.pms.biz.reftable.mp.MpQtyDurationConversionServiceBean;
	import hk.org.ha.model.pms.dms.persistence.PmsPcuMapping;
	import hk.org.ha.model.pms.vo.reftable.mp.MpQtyDurationConversionInfo;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("mpQtyDurationConversionServiceCtl", restrict="true")]
	public class MpQtyDurationConversionServiceCtl 
	{
		[In]
		public var mpQtyDurationConversionService:MpQtyDurationConversionServiceBean;
		
		[In]
		public var mpQtyDurationConversionRptService:MpQtyDurationConversionRptServiceBean;
		
		private var itemCode:String;
		private var pmsPcuMapping:PmsPcuMapping;
		
		[Observer]
		public function retrieveMpQtyDurationConversionList(evt:RetrieveMpQtyDurationConversionListEvent):void
		{
			itemCode = evt.itemCode;
			pmsPcuMapping = evt.pmsPcuMapping;
			
			In(Object(mpQtyDurationConversionService).retrieveCapdItem)
			mpQtyDurationConversionService.retrieveMpQtyDurationConversionList(evt.itemCode, evt.pmsPcuMapping, retrieveMpPmsQtyConversionListResult);
		}
		
		private function retrieveMpPmsQtyConversionListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshMpQtyDurationConversionViewEvent((Object(mpQtyDurationConversionService).retrieveCapdItem), evt.result as MpQtyDurationConversionInfo));
		}
		
		[Observer]
		public function updateMpQtyDurationConversionList(evt:UpdateMpQtyDurationConversionListEvent):void
		{	
			In(Object(mpQtyDurationConversionService).saveSuccess);
			mpQtyDurationConversionService.updateMpQtyDurationConversionList(evt.updatePmsQtyConversionList, evt.qtyApplyAll, 
																			evt.updatePmsDurationConversionList, evt.durationApplyAll, 
																			evt.updatePmsQtyConversionInsu, evt.insuApplyAll, 
																			evt.updatePmsQtyConversionAdj,
																			updateMpQtyDurationConversionListResult);
		}
		
		private function updateMpQtyDurationConversionListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new ClearMpQtyDurationConversionMaintViewEvent( 
				Object(mpQtyDurationConversionService).saveSuccess, 
				(evt.result as String) ));
		}
		
		[Observer]
		public function printMpDurationConversionMaintRpt(evt:PrintMpQtyDurationConversionRptEvent):void {
			In(Object(mpQtyDurationConversionRptService).qtyRetrieveSuccess)
			In(Object(mpQtyDurationConversionRptService).durationRetrieveSuccess)
			mpQtyDurationConversionRptService.printMpQtyDurationConversionRpt(refreshPrintDurationConversionMaintRptResult);
		}
		
		private function refreshPrintDurationConversionMaintRptResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshPrintMpQtyDurationConversionRptEvent( Object(mpQtyDurationConversionRptService).qtyRetrieveSuccess, Object(mpQtyDurationConversionRptService).durationRetrieveSuccess  ));
		}
		
		[Observer]
		public function retrieveMpQtyConversionRptList(evt:RetrieveMpQtyConversionRptListEvent):void {
			In(Object(mpQtyDurationConversionRptService).qtyExportSuccess)
			mpQtyDurationConversionRptService.retrieveMpQtyConversionRptList(retrieveQtyConversionRptListResult);
		}
		
		private function retrieveQtyConversionRptListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshMpQtyConversionRptListEvent( Object(mpQtyDurationConversionRptService).qtyExportSuccess  ));
		}
		
		[Observer]
		public function retrieveDurConversionRptList(evt:RetrieveMpDurConversionRptListEvent):void {
			In(Object(mpQtyDurationConversionRptService).durExportSuccess)
			mpQtyDurationConversionRptService.retrieveMpDurConversionRptList(retrieveDurConversionRptListResult);
		}
		
		private function retrieveDurConversionRptListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshMpDurConversionRptListEvent( Object(mpQtyDurationConversionRptService).durExportSuccess  ));
		}
		
		[Observer]
		public function retrieveQtyConversionInsuRptList(evt:RetrieveQtyConversionInsuRptListEvent):void {
			In(Object(mpQtyDurationConversionRptService).qtyInsuExportSuccess)
			mpQtyDurationConversionRptService.retrieveQtyConversionInsuRptList(retrieveQtyConversionInsuRptListResult);
		}
		
		private function retrieveQtyConversionInsuRptListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshQtyConversionInsuRptListEvent( Object(mpQtyDurationConversionRptService).qtyInsuExportSuccess  ));
		}
		
	}
}
