package hk.org.ha.event.pms.main.drug {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrieveDmWarningForOrderViewEvent extends AbstractTideEvent {

		private var _warnCode:String;
		
		private var _callback:Function;
		
		public function RetrieveDmWarningForOrderViewEvent(warnCode:String, callback:Function) {
			super();
			_warnCode = warnCode;
			_callback = callback;
		}
		
		public function get warnCode():String {
			return _warnCode;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}
