package hk.org.ha.control.pms.main.drug {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.drug.RetrieveDrugDiluentListByRxItemEvent;	
	import hk.org.ha.model.pms.biz.drug.DrugDiluentListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("drugDiluentListServiceCtl", restrict="true")]
	public class DrugDiluentListServiceCtl 
	{
		[In]
		public var drugDiluentListService:DrugDiluentListServiceBean;		
		
		[In]
		public var drugDiluentList:ArrayCollection;
		
		[In]
		public var ctx:Context;		
				
		[Observer]
		public function retrieveDrugDiluentListByRxItem(evt:RetrieveDrugDiluentListByRxItemEvent):void
		{				
			ctx.meta_updates.removeAll();			
			drugDiluentListService.retrieveDrugDiluentListByRxItem(evt.rxItem);
		}		
	}
}