package hk.org.ha.event.pms.main.reftable.pivas {
	
	import hk.org.ha.model.pms.dms.persistence.DmDrug;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePivasDrugListInfoEvent extends AbstractTideEvent 
	{
		private var _dmDrug:DmDrug;
		
		public function RetrievePivasDrugListInfoEvent(dmDrug:DmDrug):void {
			_dmDrug = dmDrug;
			super();
		}
		
		public function get dmDrug():DmDrug {
			return _dmDrug;
		}
	}
}