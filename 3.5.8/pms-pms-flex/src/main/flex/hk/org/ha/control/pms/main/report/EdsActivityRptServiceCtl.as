package hk.org.ha.control.pms.main.report {
	import hk.org.ha.event.pms.main.report.PrintEdsActivityRptEvent;
	import hk.org.ha.event.pms.main.report.RefreshEdsActivityRptEvent;
	import hk.org.ha.event.pms.main.report.RetrieveEdsActivityRptEvent;
	import hk.org.ha.model.pms.biz.report.EdsActivityRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("edsActivityRptServiceCtl", restrict="true")]
	public class EdsActivityRptServiceCtl 
	{
		[In]
		public var edsActivityRptService:EdsActivityRptServiceBean;
		
		[Observer]
		public function retrieveEdsActivityRpt(evt:RetrieveEdsActivityRptEvent):void
		{
			In(Object(edsActivityRptService).retrieveSuccess)
			edsActivityRptService.retrieveEdsActivityRpt(evt.selectedTab, evt.date, retrieveEdsActivityRptResult);
		}
		
		private function retrieveEdsActivityRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshEdsActivityRptEvent(Object(edsActivityRptService).retrieveSuccess));
		}
		
		[Observer]
		public function printEdsActivityRpt(evt:PrintEdsActivityRptEvent):void
		{
			edsActivityRptService.printEdsActivityRpt();
		}
		
	}
}
