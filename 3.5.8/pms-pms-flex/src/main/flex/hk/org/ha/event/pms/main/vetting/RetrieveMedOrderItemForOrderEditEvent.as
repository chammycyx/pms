package hk.org.ha.event.pms.main.vetting {

	import flash.events.Event;
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrieveMedOrderItemForOrderEditEvent extends AbstractTideEvent {

		private var _selectedIndex:int;	
		
		private var _event:Event;
		
		public function RetrieveMedOrderItemForOrderEditEvent(selectedIndex:int, event:Event) 
		{
			super();			
			_selectedIndex = selectedIndex;
			_event = event;
		}
		
		public function get selectedIndex():int 
		{
			return _selectedIndex;
		}
		
		public function get event():Event
		{
			return _event;
		}
	}
}