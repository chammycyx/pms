package hk.org.ha.event.pms.security.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowLogonSuccessEvent extends AbstractTideEvent 
	{
		
		public function ShowLogonSuccessEvent():void 
		{
			super();
		}
	}
}