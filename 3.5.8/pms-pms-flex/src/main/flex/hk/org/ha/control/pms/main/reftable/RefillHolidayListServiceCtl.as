package hk.org.ha.control.pms.main.reftable {
	
	import hk.org.ha.event.pms.main.reftable.RetrieveRefillHolidayListEvent;
	import hk.org.ha.event.pms.main.reftable.SetHolidayListDateRangeFilterEvent;
	import hk.org.ha.event.pms.main.reftable.ShowRefillModuleMaintSaveSuccessMsgEvent;
	import hk.org.ha.event.pms.main.reftable.UpdateRefillHolidayListEvent;
	import hk.org.ha.model.pms.biz.reftable.RefillHolidayListServiceBean;
	
	import mx.core.UIComponent;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("refillHolidayListServiceCtl", restrict="true")]
	public class RefillHolidayListServiceCtl 
	{
		[In]
		public var refillHolidayListService:RefillHolidayListServiceBean;
		
		private var setUIFocus:Boolean = true;
		private var nextTabIndex:Number = 3;
		private var showRefillModuleMaintAlertMsg:Boolean = true;
		private var caller:UIComponent = null;
		
		[Observer]
		public function retrieveRefillHolidayList(evt:RetrieveRefillHolidayListEvent):void
		{
			setUIFocus = true;			
			refillHolidayListService.retrieveRefillHolidayList(refillHolidayListServiceResult);
		}
		
		private function refillHolidayListServiceResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new SetHolidayListDateRangeFilterEvent(setUIFocus));
		}
		
		[Observer]
		public function updateRefillHolidayList(evt:UpdateRefillHolidayListEvent):void
		{
			showRefillModuleMaintAlertMsg = evt.showRefillModuleMaintAlertMsg;
			nextTabIndex = evt.nextTabIndex;
			setUIFocus = false;
			caller = evt.caller;
			
			refillHolidayListService.updateRefillHolidayList(evt.updateRefillHolidayList, evt.delRefillHolidayList, updateRefillHolidayListResult);
		}
		
		private function updateRefillHolidayListResult(evt:TideResultEvent):void {
			setUIFocus = false;
			evt.context.dispatchEvent( new ShowRefillModuleMaintSaveSuccessMsgEvent(nextTabIndex, 0, showRefillModuleMaintAlertMsg, caller));
		}
	}
}
