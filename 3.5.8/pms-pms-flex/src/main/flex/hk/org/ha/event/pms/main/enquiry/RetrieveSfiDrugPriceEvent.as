package hk.org.ha.event.pms.main.enquiry
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSfiDrugPriceEvent extends AbstractTideEvent 
	{
		private var _itemCode:String;
		private var _drugPriceMonth:Date;
		private var _nextColumnPosition:Number;
		private var _nextRowPosition:Number;
		private var _addNewRecord:Boolean;
		
		public function RetrieveSfiDrugPriceEvent(itemCode:String, drugPriceMonth:Date, nextColumnPosition:Number, nextRowPosition:Number, addNewRecord:Boolean):void
		{
			super();
			_itemCode = itemCode;
			_drugPriceMonth = drugPriceMonth;
			_nextColumnPosition = nextColumnPosition;
			_nextRowPosition = nextRowPosition;
			_addNewRecord = addNewRecord;
		}
		
		public function get itemCode():String
		{
			return _itemCode;
		}
		
		public function get drugPriceMonth():Date
		{
			return _drugPriceMonth;
		}
		
		public function get nextColumnPosition():Number
		{
			return _nextColumnPosition;
		}
		
		public function get nextRowPosition():Number
		{
			return _nextRowPosition;
		}
		
		public function get addNewRecord():Boolean
		{
			return _addNewRecord;
		}
	}
}