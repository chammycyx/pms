package hk.org.ha.event.pms.main.delivery
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDueOrderDeliveryScheduleListEvent extends AbstractTideEvent
	{
		public function RetrieveDueOrderDeliveryScheduleListEvent()
		{
			super();
		}
	}
}