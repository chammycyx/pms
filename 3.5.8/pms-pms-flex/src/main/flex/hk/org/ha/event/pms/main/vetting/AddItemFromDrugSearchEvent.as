package hk.org.ha.event.pms.main.vetting {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.udt.drug.DrugScope;
	
	public class AddItemFromDrugSearchEvent extends AbstractTideEvent {

		private var _index:Number;
		private var _autoAddDrug:Boolean;  
		private var _drugScope:DrugScope;  
		
		public function AddItemFromDrugSearchEvent(index:Number=NaN) {
			super();
			_index = index;
		}
		
		public function get index():Number {
			return _index;
		}
		
		public function get autoAddDrug():Boolean {
			return _autoAddDrug;
		}
		
		public function set autoAddDrug(autoAddDrug:Boolean):void {
			_autoAddDrug = autoAddDrug;
		}
		
		public function get drugScope():DrugScope {
			return _drugScope;
		}
		
		public function set drugScope(drugScope:DrugScope):void {
			_drugScope = drugScope;
		}
	}
}
