package hk.org.ha.control.pms.main.report {

	import hk.org.ha.event.pms.main.report.PrintMdsMpRptEvent;
	import hk.org.ha.event.pms.main.report.PrintMdsOpRptEvent;
	import hk.org.ha.model.pms.biz.report.MdsExceptionOverrideRptServiceBean;
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	import hk.org.ha.model.pms.persistence.disp.MedOrderItemAlert;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItemAlert;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("mdsExceptionOverrideRptServiceCtl", restrict="true")]
	public class MdsExceptionOverrideRptServiceCtl {

		[In]
		public var mdsExceptionOverrideRptService:MdsExceptionOverrideRptServiceBean;
		
		[In]
		public var ctx:Context;
		
		private var moiList:ListCollectionView;
		
		private var mpList:ListCollectionView;
		
		[Observer]
		public function printMdsOpRpt(evt:PrintMdsOpRptEvent):void {
			
			moiList = new ArrayCollection();

			var moiAlertList:ListCollectionView = evt.alertMsg.alertList;

			for each (var moiAlert:MedOrderItemAlert in moiAlertList) {
				if (moiAlert.medOrderItem == null) {
					continue;
				}
				if (!moiList.contains(moiAlert.medOrderItem)) {
					moiList.addItem(moiAlert.medOrderItem);
				}
				moiAlert.medOrderItem = null;
			}

			mdsExceptionOverrideRptService.printMdsExceptionOpOverrideRpt(evt.alertMsg, evt.alertProfile, printMdsOpRptResult);
		}
		
		private function printMdsOpRptResult(evt:TideResultEvent):void {
			for each (var moi:MedOrderItem in moiList) {
				for each (var moiAlert:MedOrderItemAlert in moi.medOrderItemAlertList) {
					moiAlert.medOrderItem = moi;
				}
			}
		}
		
		[Observer]
		public function printMdsMpRpt(evt:PrintMdsMpRptEvent):void {
			
			mpList = new ArrayCollection();
			
			var mpAlertList:ListCollectionView = evt.alertMsg.alertList;
			
			for each (var mpAlert:MedProfileMoItemAlert in mpAlertList) {
				if (mpAlert.medProfileMoItem == null) {
					continue;
				}
				if (!mpList.contains(mpAlert.medProfileMoItem)) {
					mpList.addItem(mpAlert.medProfileMoItem);
				}
				mpAlert.medProfileMoItem = null;
			}
			
			mdsExceptionOverrideRptService.printMdsExceptionMpOverrideRpt(evt.alertMsg, evt.alertProfile, printMdsMpRptResult);
		}
		
		private function printMdsMpRptResult(evt:TideResultEvent):void {
			for each (var mp:MedProfileMoItem in mpList) {
				for each (var mpAlert:MedProfileMoItemAlert in mp.medProfileMoItemAlertList) {
					mpAlert.medProfileMoItem = mp;
				}
			}
		}
	}
}
