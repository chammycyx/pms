package hk.org.ha.event.pms.main.report {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveEdsActivityRptEvent extends AbstractTideEvent 
	{
		private var _selectedTab:Number;
		private var _date:Date;
		
		public function RetrieveEdsActivityRptEvent( selectedTab:Number, date:Date):void 
		{
			super();
			_selectedTab = selectedTab;
			_date = date;
		}
		
		public function get selectedTab():Number{
			return _selectedTab;
		} 
		
		public function get date():Date{
			return _date;
		} 
	}
}