package hk.org.ha.event.pms.main.drug {

	import flash.events.Event;
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dms.vo.Drug;
	import hk.org.ha.model.pms.dms.vo.DsfSearchCriteria;
	import hk.org.ha.view.pms.main.vetting.popup.OrderEditItemPopup;
	
	public class RetrieveWorkstoreDrugListDrugKeyEvent extends AbstractTideEvent {		
		
		private var _drugKey:Number;
		
		private var _callBackEvent:Event;
		
		private var _orderEditItemPopup:OrderEditItemPopup;
		
		public function RetrieveWorkstoreDrugListDrugKeyEvent(drugKeyValue:Number, event:Event=null, orderEditItemPopup:OrderEditItemPopup=null) 
		{
			super();			
			_drugKey = drugKeyValue;
			_callBackEvent = event;
			_orderEditItemPopup = orderEditItemPopup;
		}		
		
		public function get drugKey():Number
		{
			return _drugKey;
		}
		
		public function get callBackEvent():Event
		{
			return _callBackEvent;
		}
				
		public function get orderEditItemPopup():OrderEditItemPopup
		{
			return _orderEditItemPopup;
		}
		
	}
}