package hk.org.ha.event.pms.main.onestop {
		
	import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
	import hk.org.ha.model.pms.udt.onestop.MoeFilterOption;
	import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
	import hk.org.ha.model.pms.udt.printing.PrintLang;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
		
	public class PrintOSEPrescRptEvent extends AbstractTideEvent 
	{		
		private var _orderDate:Date;
		private var _moeFilterOption:MoeFilterOption;
		
		public function PrintOSEPrescRptEvent(orderDate:Date, moeFilterOption:MoeFilterOption):void 
		{
			super();
			_orderDate = orderDate;
			_moeFilterOption = moeFilterOption;
		}
		
		public function get orderDate():Date
		{
			return _orderDate;
		}
		
		public function get moeFilterOption():MoeFilterOption
		{
			return _moeFilterOption;
		}
	}
}