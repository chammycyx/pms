package hk.org.ha.event.pms.main.support
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class UpdateCorporatePropListEvent extends AbstractTideEvent 
	{
		private var _corpPropList:ArrayCollection;
		
		public function UpdateCorporatePropListEvent(corpPropList:ArrayCollection):void
		{
			super();
			_corpPropList = corpPropList;
		}
		
		public function get corpPropList():ArrayCollection {
			return _corpPropList;
		}
	}
}