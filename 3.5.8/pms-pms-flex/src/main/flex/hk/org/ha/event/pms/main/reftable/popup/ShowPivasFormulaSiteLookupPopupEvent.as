package hk.org.ha.event.pms.main.reftable.popup {

	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class ShowPivasFormulaSiteLookupPopupEvent extends AbstractTideEvent {

		private var _whiteList:ListCollectionView;
		private var _greyList:ListCollectionView;
		private var _callback:Function;
		
		public function ShowPivasFormulaSiteLookupPopupEvent(whiteList:ListCollectionView, greyList:ListCollectionView, callback:Function):void {
			super();
			_whiteList = whiteList;
			_greyList = greyList;
			_callback = callback;
		}
		
		public function get whiteList():ListCollectionView {
			return _whiteList;
		}
		
		public function get greyList():ListCollectionView {
			return _greyList;
		}

		public function get callback():Function {
			return _callback;
		}
	}
}
