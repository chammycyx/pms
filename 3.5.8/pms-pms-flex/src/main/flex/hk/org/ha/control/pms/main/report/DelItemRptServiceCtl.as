package hk.org.ha.control.pms.main.report {
	import hk.org.ha.event.pms.main.report.PrintDelItemRptEvent;
	import hk.org.ha.event.pms.main.report.RefreshDelItemRptEvent;
	import hk.org.ha.event.pms.main.report.RetrieveDelItemRptEvent;
	import hk.org.ha.model.pms.biz.report.DelItemRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("delItemRptServiceCtl", restrict="true")]
	public class DelItemRptServiceCtl 
	{
		[In]
		public var delItemRptService:DelItemRptServiceBean;
		
		[Observer]
		public function retrieveDelItemRpt(evt:RetrieveDelItemRptEvent):void
		{
			In(Object(delItemRptService).retrieveSuccess)
			delItemRptService.retrieveDelItemRpt(evt.selectedTab, evt.workstoreCode, evt.date, retrieveDelItemRptResult);
		}
		
		private function retrieveDelItemRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshDelItemRptEvent(Object(delItemRptService).retrieveSuccess));
		}
		
		[Observer]
		public function printDelItemRpt(evt:PrintDelItemRptEvent):void
		{
			delItemRptService.printDelItemRpt();
		}
		
	}
}
