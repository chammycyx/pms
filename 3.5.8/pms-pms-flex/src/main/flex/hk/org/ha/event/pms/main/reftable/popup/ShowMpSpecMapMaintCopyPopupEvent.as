package hk.org.ha.event.pms.main.reftable.popup {
	
	import hk.org.ha.event.pms.main.reftable.popup.MpSpecMapMaintCopyPopupProp;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowMpSpecMapMaintCopyPopupEvent extends AbstractTideEvent 
	{
		private var _mpSpecMapMaintCopyPopupProp:MpSpecMapMaintCopyPopupProp;
		
		public function ShowMpSpecMapMaintCopyPopupEvent(mpSpecMapMaintCopyPopupProp:MpSpecMapMaintCopyPopupProp):void 
		{
			super();
			_mpSpecMapMaintCopyPopupProp = mpSpecMapMaintCopyPopupProp;
		}
		
		public function get mpSpecMapMaintCopyPopupProp():MpSpecMapMaintCopyPopupProp{
			return _mpSpecMapMaintCopyPopupProp;
		}

	}
}