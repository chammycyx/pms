package hk.org.ha.event.pms.main.vetting.mp.show
{
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowManualEntryItemEditViewEvent extends AbstractTideEvent
	{
		private var _reactivateFlag:Boolean;
		private var _medProfileItemList:ListCollectionView;
		private var _medProfileItem:MedProfileItem;		
		private var _medProfileMoItem:MedProfileMoItem;
		private var _clearMessages:Boolean;
		
		public function ShowManualEntryItemEditViewEvent(medProfileItemList:ListCollectionView, medProfileItemValue:MedProfileItem, medProfileMoItemValue:MedProfileMoItem=null, reactivateFlag:Boolean=false, clearMessages:Boolean=true)
		{
			super();			
			_medProfileItemList   = medProfileItemList;
			_medProfileItem 	  = medProfileItemValue;
			_medProfileMoItem	  = medProfileMoItemValue;
			_reactivateFlag		  = reactivateFlag;
			_clearMessages 		  = clearMessages;
		}
		
		public function get medProfileItemList():ListCollectionView
		{
			return _medProfileItemList;
		}
		
		public function get medProfileItem():MedProfileItem
		{
			return _medProfileItem;
		}
		
		public function get medProfileMoItem():MedProfileMoItem
		{
			return _medProfileMoItem;
		}

		public function get reactivateFlag():Boolean
		{
			return _reactivateFlag;
		}
		
		public function get clearMessages():Boolean
		{
			return _clearMessages;
		}
	}
}