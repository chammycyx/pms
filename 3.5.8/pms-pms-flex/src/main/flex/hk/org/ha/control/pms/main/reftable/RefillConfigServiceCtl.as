package hk.org.ha.control.pms.main.reftable {
	
	import hk.org.ha.event.pms.main.reftable.RetrieveRefillConfigEvent;
	import hk.org.ha.event.pms.main.reftable.SetRefillConfigFocusEvent;
	import hk.org.ha.event.pms.main.reftable.ShowRefillModuleMaintSaveSuccessMsgEvent;
	import hk.org.ha.event.pms.main.reftable.UpdateEnableSelectionFlagEvent;
	import hk.org.ha.event.pms.main.reftable.UpdateHolidayRefillConfigEvent;
	import hk.org.ha.event.pms.main.reftable.UpdateRefillConfigEvent;
	import hk.org.ha.model.pms.biz.reftable.RefillConfigServiceBean;
	
	import mx.core.UIComponent;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("refillConfigServiceCtl", restrict="true")]
	public class RefillConfigServiceCtl 
	{
		[In]
		public var refillConfigService:RefillConfigServiceBean;
		
		private var checkRefillQtyAdjTabEnabled:Boolean = false;
		private var setEnableQtyAdjFlag:Boolean = false;
		private var setHolidayListConfig:Boolean = false;
		private var showRefillModuleMaintAlertMsg:Boolean = true;
		private var nextRefillTabIndex:Number = 0;
		private var showSaveSuccessMsg:Boolean = true;
		private var caller:UIComponent = null;
		
		[Observer]
		public function retrieveRefillConfig(evt:RetrieveRefillConfigEvent):void
		{
			checkRefillQtyAdjTabEnabled = evt.checkRefillQtyAdjTabEnabled;
			setEnableQtyAdjFlag = evt.setEnableQtyAdjFlag;
			setHolidayListConfig = evt.setHolidayListConfig;
			
			refillConfigService.retrieveRefillConfig(retrieveRefillConfigResult);
		}
		
		private function retrieveRefillConfigResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent( new SetRefillConfigFocusEvent(checkRefillQtyAdjTabEnabled, setEnableQtyAdjFlag, setHolidayListConfig) );
		}
		
		[Observer]
		public function updateRefillConfig(evt:UpdateRefillConfigEvent):void
		{
			nextRefillTabIndex = evt.nextTabIndex;
			showRefillModuleMaintAlertMsg = evt.showRefillModuleMaintAlertMsg;
			caller = evt.caller;
			
			refillConfigService.updateRefillConfig(evt.refillConfig, updateRefillConfigResult);
		}
		
		private function updateRefillConfigResult(evt:TideResultEvent):void 
		{
			var setEnableQtyAdjFlag:Boolean = true;
			evt.context.dispatchEvent( new ShowRefillModuleMaintSaveSuccessMsgEvent(nextRefillTabIndex, 0, showRefillModuleMaintAlertMsg, caller, setEnableQtyAdjFlag));
		}
		
		[Observer]
		public function updateEnableSelectionFlag(evt:UpdateEnableSelectionFlagEvent):void
		{
			nextRefillTabIndex = evt.nextTabIndex;
			showRefillModuleMaintAlertMsg = evt.showRefillModuleMaintAlertMsg;
			showSaveSuccessMsg = evt.showSaveSuccessMsg;
			caller = evt.caller;
			
			refillConfigService.updateEnableSelectionFlag(evt.enableSelectionFlag, updateEnableSelectionFlagResult);
		}
		
		private function updateEnableSelectionFlagResult(evt:TideResultEvent):void 
		{
			var setEnableQtyAdjFlag:Boolean = false;
			if ( showSaveSuccessMsg ) 
			{
				evt.context.dispatchEvent( new ShowRefillModuleMaintSaveSuccessMsgEvent(nextRefillTabIndex, 0, showRefillModuleMaintAlertMsg, caller, setEnableQtyAdjFlag));
			}
		}
		
		[Observer]
		public function updateHolidayRefillConfig(evt:UpdateHolidayRefillConfigEvent):void
		{
			nextRefillTabIndex = evt.nextTabIndex;
			showRefillModuleMaintAlertMsg = false;
			showSaveSuccessMsg = false;
			
			refillConfigService.updateHolidayRefillConfig(evt.enableAdjHolidayFlag, evt.enableAdjSundayFlag, updateEnableSelectionFlagResult);
		}
	}
}
