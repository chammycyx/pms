package hk.org.ha.event.pms.main.reftable.popup
{
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.model.pms.persistence.reftable.OperationWorkstation;

	public class OperationModeMaintPopupProp
	{
		public var operationWorkstation:OperationWorkstation;
		public var printerConnectedStationList:ArrayCollection;
		public var redirectStationStr:String;
		public var fastQueuePrinterConnectedStationList:ArrayCollection;
		public var redirectFastQueueStationStr:String;
	}
}
