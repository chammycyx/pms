package hk.org.ha.event.pms.main.vetting {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrievePharmRemarkDateItemNumEvent extends AbstractTideEvent {		
		private var _callbackFunc:Function = null;		// used by item level remark only
		private var _itemNumRequired:Boolean;

		public function RetrievePharmRemarkDateItemNumEvent(callbackFunc:Function, itemNumRequired:Boolean) {
			super();
			_callbackFunc = callbackFunc;
			_itemNumRequired = itemNumRequired;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
		
		public function get itemNumRequired():Boolean {
			return _itemNumRequired;
		}
	}
}
