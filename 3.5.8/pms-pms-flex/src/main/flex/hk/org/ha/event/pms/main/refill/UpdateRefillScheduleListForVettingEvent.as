package hk.org.ha.event.pms.main.refill {
	
	import hk.org.ha.model.pms.persistence.disp.PharmOrder;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateRefillScheduleListForVettingEvent extends AbstractTideEvent 
	{
		private var _pharmOrder:PharmOrder;
		private var _callbackFunc:Function;

		public function UpdateRefillScheduleListForVettingEvent(pharmOrder:PharmOrder, callbackFunc:Function = null):void{
			super();
			_pharmOrder = pharmOrder;
			_callbackFunc = callbackFunc;
		}

		public function get pharmOrder():PharmOrder {
			return _pharmOrder;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}