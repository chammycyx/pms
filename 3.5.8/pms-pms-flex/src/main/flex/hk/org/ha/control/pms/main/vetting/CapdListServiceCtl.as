package hk.org.ha.control.pms.main.vetting {
	
	import hk.org.ha.event.pms.main.vetting.ClearCapdListEvent;
	import hk.org.ha.event.pms.main.vetting.RefreshOrderEditCapdViewEvent;
	import hk.org.ha.event.pms.main.vetting.RetrieveCapdCalciumStrengthListEvent;
	import hk.org.ha.event.pms.main.vetting.RetrieveCapdConcentrationListEvent;
	import hk.org.ha.event.pms.main.vetting.RetrieveCapdListEvent;
	import hk.org.ha.event.pms.main.vetting.RetrieveCapdSupplierSystemListEvent;
	import hk.org.ha.event.pms.main.vetting.SetCapdListSelectedIndexEvent;
	import hk.org.ha.model.pms.biz.vetting.CapdListServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("capdListServiceCtl", restrict="true")]
	public class CapdListServiceCtl {
		
		[In]
		public var capdListService:CapdListServiceBean;
		
		[In]
		public var ctx:Context;
		
		[Observer]
		public function retrieveCapdList(evt:RetrieveCapdListEvent):void {
			capdListService.retrieveCapdList(evt.supplierSystem, evt.calciumStrength, retrieveCapdListResult);
		}
		
		private function retrieveCapdListResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent( new SetCapdListSelectedIndexEvent());
			refreshOrderEditCapdView(evt);
		}
		
		private function refreshOrderEditCapdView(evt:TideResultEvent):void {
			evt.context.dispatchEvent( new RefreshOrderEditCapdViewEvent());
		}
		
		[Observer]
		public function retrieveCapdSupplierSystemList(evt:RetrieveCapdSupplierSystemListEvent):void {
			capdListService.retrieveCapdSupplierSystemList(refreshOrderEditCapdView);
		}
		
		[Observer]
		public function retrieveCapdCalciumStrengthList(evt:RetrieveCapdCalciumStrengthListEvent):void {
			capdListService.retrieveCapdCalciumStrengthList(evt.supplierSystem);
		}
		
		[Observer]
		public function retrieveCapdConcentrationList(evt:RetrieveCapdConcentrationListEvent):void {
			capdListService.retrieveCapdConcentrationList(evt.supplierSystem, evt.calciumStrength);
		}
		
		[Observer]
		public function clearCapdList(evt:ClearCapdListEvent):void {
			capdListService.clearCapdList();
		}
		
	}
}
