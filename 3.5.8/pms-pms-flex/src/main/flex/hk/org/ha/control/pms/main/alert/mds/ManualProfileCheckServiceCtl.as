package hk.org.ha.control.pms.main.alert.mds {

	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	import mx.core.IFlexDisplayObject;
	import mx.core.UIComponent;
	import mx.managers.PopUpManager;
	
	import hk.org.ha.event.pms.main.alert.AuditCancelPrescribeEvent;
	import hk.org.ha.event.pms.main.alert.AuditNewAlertDetectEvent;
	import hk.org.ha.event.pms.main.alert.mds.CheckManualItemForSaveProfileEvent;
	import hk.org.ha.event.pms.main.alert.mds.MpSaveItemCheckEvent;
	import hk.org.ha.event.pms.main.alert.mds.MpSaveProfileCheckEvent;
	import hk.org.ha.event.pms.main.alert.mds.MpVerifyItemCheckEvent;
	import hk.org.ha.event.pms.main.alert.mds.UpdateAlertResultListForSaveProfileEvent;
	import hk.org.ha.event.pms.main.alert.mds.popup.ShowMdsLogPopupEvent;
	import hk.org.ha.event.pms.main.alert.mds.popup.ShowMpAlertPopupEvent;
	import hk.org.ha.event.pms.main.alert.mds.popup.ShowMpDdiAlertPopupEvent;
	import hk.org.ha.event.pms.main.drug.show.ShowDrugSearchViewEvent;
	import hk.org.ha.event.pms.main.vetting.mp.RetrieveInactiveListEvent;
	import hk.org.ha.event.pms.main.vetting.mp.UpdateMedProfileItemListInMemoryEvent;
	import hk.org.ha.event.pms.main.vetting.mp.popup.ShowDeleteGroupPopupEvent;
	import hk.org.ha.event.pms.main.vetting.mp.popup.ShowDiscontinueGroupPopupEvent;
	import hk.org.ha.event.pms.main.vetting.mp.show.ShowManualEntryItemEditViewEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	import hk.org.ha.model.pms.biz.alert.mds.ManualProfileCheckServiceBean;
	import hk.org.ha.model.pms.persistence.AlertEntity;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItemAlert;
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
	import hk.org.ha.model.pms.vo.alert.mds.AlertDdim;
	import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;
	import hk.org.ha.model.pms.vo.alert.mds.ErrorInfo;
	import hk.org.ha.model.pms.vo.alert.mds.ManualProfileCheckResult;
	import hk.org.ha.model.pms.vo.alert.mds.MdsUtils;
	import hk.org.ha.model.pms.vo.security.PropMap;
	import hk.org.ha.view.pms.main.medprofile.MedProfileUtils;
	import hk.org.ha.view.pms.main.vetting.mp.VettingUtils;
	
	import org.granite.collections.BasicMap;
	import org.granite.collections.IMap;
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;

	[Bindable]
	[Name("manualProfileCheckServiceCtl", restrict="true")]
	public class ManualProfileCheckServiceCtl {
		
		[In]
		public var manualProfileCheckService:ManualProfileCheckServiceBean;
		
		[In]
		public var medProfileItemList:ListCollectionView;
		
		[In]
		public var inactiveList:ListCollectionView;
		
		[In]
		public var purchaseRequestList:ListCollectionView;
		
		[In]
		public var propMap:PropMap;
		
		[In]
		public var medProfile:MedProfile;
		
		[In]
		public var mdsExceptionInfo:ErrorInfo;

		[In]
		public var ctx:Context;
		
		private var allMoItemList:ListCollectionView = new ArrayCollection();
		
		private var mpVerifyItemCheckEvent:MpVerifyItemCheckEvent;
		
		private var mpSaveItemCheckEvent:MpSaveItemCheckEvent;
		
		private var mpSaveProfileCheckEvent:MpSaveProfileCheckEvent;
		
		[Observer]
		public function mpVerifyItemCheck(evt:MpVerifyItemCheckEvent):void {
			if (propMap.getValueAsBoolean("alert.mds.enabled") && propMap.getValueAsBoolean("vetting.medProfile.alert.mds.check")) {
				mpVerifyItemCheckEvent = evt;
				manualProfileCheckService.retrieveAlertForVerifyItem(evt.medProfileItem, evt.medProfile, mpVerifyItemCheckResult);
			}
			else {
				evt.successFunc();
			}
		}
		
		private function mpVerifyItemCheckResult(evt:TideResultEvent):void {
			
			reloadAllItemList(function():void{

			var result:ManualProfileCheckResult = evt.result as ManualProfileCheckResult;
			var orgItem:MedProfileItem = mpVerifyItemCheckEvent.medProfileItem;
			var newMoItem:MedProfileMoItem = result.isEmpty() ? null : result.medProfileMoItemList.getItemAt(0) as MedProfileMoItem;
			var alertMsg:AlertMsg = newMoItem == null ? null : result.get(newMoItem).getItemAt(0) as AlertMsg;
			
			var successFunc:Function = function():void {
				MdsUtils.replacePatAlert(orgItem.medProfileMoItem, newMoItem);
				mpVerifyItemCheckEvent.successFunc();
			};
			var failureFunc:Function = function():void {
				removeItem(orgItem, medProfileItemList, true, mpVerifyItemCheckEvent.failureFunc, mpVerifyItemCheckEvent.closeFunc);
				orgItem.modified = true;
			};
			
			dispatchEvent(new ShowMdsLogPopupEvent(function():void {
				
				if (newMoItem == null) {
					successFunc();
					return;
				}
				
				var latestOverrideAlert:MedProfileMoItemAlert = MdsUtils.findPatOverrideAlert(orgItem.medProfileMoItem, new ArrayCollection([orgItem.medProfileMoItem]));
				if (latestOverrideAlert == null) {
					latestOverrideAlert = MdsUtils.findPatOverrideAlert(newMoItem, allMoItemList);
				}
				if (latestOverrideAlert != null) {
					for each (var alertEntity:AlertEntity in alertMsg.alertList) {
						alertEntity.overrideReason.removeAll();
						alertEntity.overrideReason.addAll(latestOverrideAlert.overrideReason);
					}
				}

				var evt:ShowMpAlertPopupEvent = new ShowMpAlertPopupEvent(alertMsg, newMoItem.medProfileMoItemAlertList, "verify");
				evt.overrideCallback = successFunc;
				evt.acceptCallback = successFunc;
				evt.doNotPrescribeCallback = function():void {
					writeCancelPrescribeAuditLog(alertMsg.mdsOrderDesc, "Verify Item");
					failureFunc();
				};
				evt.closeCallback = mpVerifyItemCheckEvent.closeFunc;
				
				dispatchEvent(evt);
				
				writeNewAlertAuditLog(result, "Verify Item");
			}));
			});
		}
		
		[Observer]
		public function mpSaveItemCheck(evt:MpSaveItemCheckEvent):void {
			if (propMap.getValueAsBoolean("alert.mds.enabled") && propMap.getValueAsBoolean("vetting.medProfile.alert.mds.check") && evt.medProfileItemList.length > 0) {
				mpSaveItemCheckEvent = evt;
				manualProfileCheckService.retrieveAlertForSaveItem(evt.medProfileItemList, evt.medProfile, mpSaveItemCheckResult);
			}
			else {
				evt.successFunc();
			}
		}
		
		private function mpSaveItemCheckResult(evt:TideResultEvent):void {
			
			reloadAllItemList(function():void{
			
			var result:ManualProfileCheckResult = evt.result as ManualProfileCheckResult;
			
			dispatchEvent(new ShowMdsLogPopupEvent(function():void {
				promptAlertForSaveItem(result);
			}));

			});
		}
		
		private function promptAlertForSaveItem(result:ManualProfileCheckResult, itemIndex:int=0):void {

			if (result.medProfileMoItemList.length <= itemIndex) {
				MdsUtils.replaceAlertByDrugKey(mpSaveItemCheckEvent.medProfileItemList, result.medProfileMoItemList);
				mpSaveItemCheckEvent.successFunc();
				return;
			}
			
			var newMoItem:MedProfileMoItem = result.medProfileMoItemList.getItemAt(itemIndex) as MedProfileMoItem;
			var alertMsg:AlertMsg = result.get(newMoItem).getItemAt(0) as AlertMsg;

			var latestOverrideAlert:MedProfileMoItemAlert = MdsUtils.findPatOverrideAlert(newMoItem, allMoItemList);
			if (latestOverrideAlert != null) {
				for each (var alertEntity:AlertEntity in alertMsg.alertList) {
					alertEntity.overrideReason.removeAll();
					alertEntity.overrideReason.addAll(latestOverrideAlert.overrideReason);
				}
			}
			
			var successFunc:Function = function():void {
				promptAlertForSaveItem(result, itemIndex + 1);
			};
			
			var evt:ShowMpAlertPopupEvent = new ShowMpAlertPopupEvent(alertMsg, newMoItem.medProfileMoItemAlertList, "save");
			evt.overrideCallback = successFunc;
			evt.acceptCallback = successFunc;
			evt.deferCallback = function():void {
				newMoItem.mdsSuspendReason = MdsUtils.MDS_SUSPEND_REASON;
				successFunc();
			};
			evt.doNotPrescribeCallback = function():void {
				writeCancelPrescribeAuditLog(alertMsg.mdsOrderDesc, "Save Item");
				mpSaveItemCheckEvent.failureFunc();
				showDrugSearchView();
			};
			dispatchEvent(evt);
			
			writeNewAlertAuditLog(result, "Save Item");
		}
		
		private function showDrugSearchView():void {
			var showDrugSearchViewEvent:ShowDrugSearchViewEvent = new ShowDrugSearchViewEvent();
			showDrugSearchViewEvent.drugSearchSource = DrugSearchSource.MpVetting;
			showDrugSearchViewEvent.okCallbackEvent = new ShowManualEntryItemEditViewEvent(new ListCollectionView, new MedProfileItem);
			showDrugSearchViewEvent.cancelCallbackEvent = null;
			dispatchEvent(showDrugSearchViewEvent);
		}
		
		[Observer]
		public function mpSaveProfileCheck(evt:MpSaveProfileCheckEvent):void {
			medProfile.mdsCheckFlag = false;
			if (propMap.getValueAsBoolean("alert.mds.enabled") && propMap.getValueAsBoolean("vetting.medProfile.alert.mds.check")) {
				mpSaveProfileCheckEvent = evt;
				var itemNumList:ListCollectionView = new ArrayCollection();
				for each (var medProfileItem:MedProfileItem in evt.medProfileItemList) {
					if (medProfileItem.medProfileMoItem == null || medProfileItem.status == MedProfileItemStatus.Deleted || medProfileItem.status == MedProfileItemStatus.Discontinued) {
						continue;
					}
					itemNumList.addItem(medProfileItem.medProfileMoItem.itemNum);
				}
				manualProfileCheckService.retrieveAlertForSaveProfile(evt.medProfile, itemNumList, mpSaveProfileCheckResult);
			}
			else {
				evt.successFunc();
			}
		}
		
		private function mpSaveProfileCheckResult(evt:TideResultEvent):void {
			
			medProfile.mdsCheckFlag = true;
			
			reloadAllItemList(function():void{

			var result:ManualProfileCheckResult = evt.result as ManualProfileCheckResult;
			result.reminderIndex = 0;
			
			dispatchEvent(new ShowMdsLogPopupEvent(function():void {
				showMdsReminder(result, function():void {
					promptAlertForSaveProfile(result);
				});
			}));
				
			});
		}
		
		private function writeNewAlertAuditLog(result:ManualProfileCheckResult, action:String):void {
			if (!result.writeAuditLog) {
				return;
			}

			var alertMsgList:ListCollectionView = new ArrayCollection();
			for each (var list:ListCollectionView in result.map.values) {
				alertMsgList.addAll(list);
			}
			dispatchEvent(new AuditNewAlertDetectEvent(action, medProfile.id, medProfile.patient.hkid, medProfile.medCase.caseNum, alertMsgList));
			
			result.writeAuditLog = false;
		}
		
		private function writeCancelPrescribeAuditLog(orderDesc:String, action:String):void {
			dispatchEvent(new AuditCancelPrescribeEvent(action, medProfile.id, medProfile.patient.hkid, medProfile.medCase.caseNum, orderDesc));
		}
		
		private function promptAlertForSaveProfile(result:ManualProfileCheckResult, itemIndex:int=0, alertIndex:int=0):void {
			
			if (result.medProfileMoItemList.length <= itemIndex) {
				if (mdsExceptionInfo == null) {
					MdsUtils.replaceAlertAndStatusByItemNum(mpSaveProfileCheckEvent.medProfileItemList, result.medProfileMoItemList, medProfileItemList, inactiveList, purchaseRequestList);
				}
				mpSaveProfileCheckEvent.successFunc();
				return;
			}
			
			var newMoItem:MedProfileMoItem = result.medProfileMoItemList.getItemAt(itemIndex) as MedProfileMoItem;
			var alertMsgList:ListCollectionView = result.get(newMoItem);

			if (alertMsgList.length <= alertIndex) {
				promptAlertForSaveProfile(result, itemIndex+1);
				return;
			}
			
			var alertMsg:AlertMsg = alertMsgList.getItemAt(alertIndex) as AlertMsg;
			var promptNextAlert:Function = function():void {
				promptAlertForSaveProfile(result, itemIndex, alertIndex+1);
			};
			
			if (alertMsg.patAlertList.length > 0) {
				promptPatAlertForSaveProfile(result, newMoItem, alertMsg, promptNextAlert);
			}
			else {
				promptDdiAlertForSaveProfile(result, newMoItem, alertMsg, promptNextAlert);
			}
		}
		
		//Loop1 : PatAlert
		private function promptPatAlertForIpmoeSaveProfile(result:ManualProfileCheckResult, patAlertItemIndex:int=0, patAlertIndex:int=0):void{
			if (result.medProfileMoItemList.length <= patAlertItemIndex) {
				promptDdiAlertForIpmoeSaveProfile(result);
				return;
			}
			
			var patAlertNewMoItem:MedProfileMoItem = result.medProfileMoItemList.getItemAt(patAlertItemIndex) as MedProfileMoItem;
			var alertMsgList:ListCollectionView = result.get(patAlertNewMoItem);
			
			if (alertMsgList.length <= patAlertIndex) {
				promptPatAlertForIpmoeSaveProfile(result, patAlertItemIndex+1);
				return;
			}
			
			var patAlertMsg:AlertMsg = alertMsgList.getItemAt(patAlertIndex) as AlertMsg;
			var promptNextAlert:Function = function():void {
				promptPatAlertForIpmoeSaveProfile(result, patAlertItemIndex, patAlertIndex+1);
			};
			
			if (patAlertMsg.patAlertList.length > 0) {
				promptPatAlertForSaveProfile(result, patAlertNewMoItem, patAlertMsg, promptNextAlert);
			}else{
				promptPatAlertForIpmoeSaveProfile(result, patAlertItemIndex, patAlertIndex+1);
			}
		}
		
		//Loop2 : DdiAlert
		private function promptDdiAlertForIpmoeSaveProfile(result:ManualProfileCheckResult, ddiAlertItemIndex:int=0, ddiAlertIndex:int=0):void{
			if (result.medProfileMoItemList.length <= ddiAlertItemIndex) {
				if( result.includeCrossDdiFlag ){
					promptCrossDdiAlertForIpmoeSaveProfile(result);
				}else{
					dispatchEvent(new UpdateAlertResultListForSaveProfileEvent(result.medProfileMoItemList, mpSaveProfileCheckEvent.successFunc));
				}
				return;
			}
			
			var ddiAlertNewMoItem:MedProfileMoItem = result.medProfileMoItemList.getItemAt(ddiAlertItemIndex) as MedProfileMoItem;
			var alertMsgList:ListCollectionView = result.get(ddiAlertNewMoItem);
			
			if (alertMsgList.length <= ddiAlertIndex) {
				promptDdiAlertForIpmoeSaveProfile(result, ddiAlertItemIndex+1);
				return;
			}
			
			var ddiAlertMsg:AlertMsg = alertMsgList.getItemAt(ddiAlertIndex) as AlertMsg;
			var promptNextAlert:Function = function():void {
				promptDdiAlertForIpmoeSaveProfile(result, ddiAlertItemIndex, ddiAlertIndex+1);
			};
			
			
			if( ddiAlertMsg.crossDdiFlag ){
				promptDdiAlertForIpmoeSaveProfile(result, ddiAlertItemIndex, ddiAlertIndex+1);
			}else if( ddiAlertMsg.ddiAlertList.length > 0 ){
				promptDdiAlertForSaveProfile(result, ddiAlertNewMoItem, ddiAlertMsg, promptNextAlert);
			}else{
				promptDdiAlertForIpmoeSaveProfile(result, ddiAlertItemIndex, ddiAlertIndex+1);
			}
		}
		
		//Loop3 : CrossDdiAlert
		private function promptCrossDdiAlertForIpmoeSaveProfile(result:ManualProfileCheckResult, crossDdiAlertItemIndex:int=0, crossDdiAlertIndex:int=0):void{
			if (result.medProfileMoItemList.length <= crossDdiAlertItemIndex) {
				dispatchEvent(new UpdateAlertResultListForSaveProfileEvent(result.medProfileMoItemList, mpSaveProfileCheckEvent.successFunc));
				return;
			}
			
			var crossDdiAlertNewMoItem:MedProfileMoItem = result.medProfileMoItemList.getItemAt(crossDdiAlertItemIndex) as MedProfileMoItem;
			var alertMsgList:ListCollectionView = result.get(crossDdiAlertNewMoItem);
			
			if (alertMsgList.length <= crossDdiAlertIndex) {
				promptCrossDdiAlertForIpmoeSaveProfile(result, crossDdiAlertItemIndex+1);
				return;
			}
			
			var crossDdiAlertMsg:AlertMsg = alertMsgList.getItemAt(crossDdiAlertIndex) as AlertMsg;
			var promptNextAlert:Function = function():void {
				promptCrossDdiAlertForIpmoeSaveProfile(result, crossDdiAlertItemIndex, crossDdiAlertIndex+1);
			};
			
			if( crossDdiAlertMsg.crossDdiFlag ){
				promptDdiAlertForSaveProfile(result, crossDdiAlertNewMoItem, crossDdiAlertMsg, promptNextAlert);
			}else{
				promptCrossDdiAlertForIpmoeSaveProfile(result, crossDdiAlertItemIndex, crossDdiAlertIndex+1);
			}
		}
		
		[Observer]
		public function promptAlertForIpmoeSaveProfile(evt:CheckManualItemForSaveProfileEvent):void{
			reloadAllItemList(function():void{
			
				mpSaveProfileCheckEvent = new MpSaveProfileCheckEvent(evt.medProfileItemList, evt.medProfile, evt.successFunc, evt.failureFunc);
				
				//clear MDS Exception Reason - apply to IPMOE Item only - For CrossDdi
				for each (var mpi:MedProfileItem in evt.manualProfileCheckResult.medProfileItemList) {
					if( mpi.medProfileMoItem.isManualItem ){
						continue;
					}
					
					if (mpi.medProfileMoItem.mdsSuspendReason != null) {
						mpi.medProfileMoItem.mdsSuspendReason = null;
					}
				}
				promptPatAlertForIpmoeSaveProfile(evt.manualProfileCheckResult);
			});
		}
		
		private function promptPatAlertForSaveProfile(result:ManualProfileCheckResult, newMoItem:MedProfileMoItem, alertMsg:AlertMsg, callback:Function):void {
			
			if (newMoItem.medProfileItem.status == MedProfileItemStatus.Deleted || newMoItem.medProfileItem.status == MedProfileItemStatus.Discontinued) {
				callback();
				return;
			}
			
			var orgMoItem:MedProfileMoItem = mpSaveProfileCheckEvent.getByItemNum(newMoItem.itemNum);
			
			if (MdsUtils.isAlertsEqual(newMoItem.patAlertList, orgMoItem.patAlertList)) {
				MdsUtils.replaceItemsAckAndReason(newMoItem.patAlertList, orgMoItem.patAlertList.getItemAt(0) as MedProfileMoItemAlert);
				for each (var mpAlert:MedProfileMoItemAlert in newMoItem.patAlertList) {
					if (mpAlert.getOverrideReason() == null) {
						newMoItem.mdsSuspendReason = MdsUtils.MDS_SUSPEND_REASON;
						break;
					}
				}
				callback();
				return;
			}
			
			var latestOverrideAlert:MedProfileMoItemAlert = MdsUtils.findPatOverrideAlert(newMoItem, allMoItemList);
			if (latestOverrideAlert != null) {
				for each (var alertEntity:AlertEntity in alertMsg.alertList) {
					alertEntity.overrideReason.removeAll();
					alertEntity.overrideReason.addAll(latestOverrideAlert.overrideReason);
				}
			}
			
			var evt:ShowMpAlertPopupEvent = new ShowMpAlertPopupEvent(alertMsg, newMoItem.patAlertList, "save");
			evt.overrideCallback = callback;
			evt.acceptCallback = callback;
			evt.deferCallback = function():void {
				if (newMoItem.medProfileItem.status == MedProfileItemStatus.Verified) {
					newMoItem.medProfileItem.status = MedProfileItemStatus.Vetted;
				}
				newMoItem.mdsSuspendReason = MdsUtils.MDS_SUSPEND_REASON;
				callback();
			};
			evt.doNotPrescribeCallback = function():void {
				writeCancelPrescribeAuditLog(alertMsg.mdsOrderDesc, "Save Profile");
				removeItem(newMoItem.medProfileItem, result.medProfileItemList, false, callback, mpSaveProfileCheckEvent.failureFunc);
			};
			evt.closeCallback = mpSaveProfileCheckEvent.failureFunc;
			dispatchEvent(evt);
			
			writeNewAlertAuditLog(result, "Save Profile");
		}
				
		private function promptDdiAlertForSaveProfile(result:ManualProfileCheckResult, newMoItem:MedProfileMoItem, alertMsg:AlertMsg, callback:Function):void {
			
			if (newMoItem.medProfileItem.status == MedProfileItemStatus.Deleted || newMoItem.medProfileItem.status == MedProfileItemStatus.Discontinued) {
				callback();
				return;
			}
			
			var orgMoItem:MedProfileMoItem = mpSaveProfileCheckEvent.getByItemNum(newMoItem.itemNum);
			var ddiAlert:MedProfileMoItemAlert = alertMsg.ddiAlertList.getItemAt(0) as MedProfileMoItemAlert;
			var alertDdim:AlertDdim = ddiAlert.alert as AlertDdim;

			if (alertDdim.copyFlag) {
				callback();
				return;
			}
			
			var orgDdiAlert:AlertEntity = orgMoItem.getDdiAlert(alertDdim);
			var newDdiAlert:AlertEntity = newMoItem.getDdiAlert(alertDdim);
			
			if (newDdiAlert == null) {
				callback();
				return;
			}
				
			var moItem1:MedProfileMoItem = result.getByItemNum(alertDdim.itemNum1);
			var moItem2:MedProfileMoItem = result.getByItemNum(alertDdim.itemNum2);
			
			var otherMoItem:MedProfileMoItem = newMoItem == moItem1 ? moItem2 : moItem1;
			var otherMoItemAlert:MedProfileMoItemAlert = otherMoItem.getDdiAlert(alertDdim);
			
			if( alertMsg.crossDdiFlag ){
				var showXDdiAlertFlag:Boolean = false;
				for each(var xDdiAlert:MedProfileMoItemAlert in alertMsg.ddiAlertList){
					var alertXDdim:AlertDdim = xDdiAlert.alert as AlertDdim;
					
					if (alertXDdim.copyFlag) {
						continue;
					}
					
					var orgXDdiAlert:AlertEntity = orgMoItem.getDdiAlert(alertXDdim);
					var newXDdiAlert:AlertEntity = newMoItem.getDdiAlert(alertXDdim);
					
					if (newXDdiAlert == null) {
						continue;
					}
					
					if (orgXDdiAlert != null) {
						MdsUtils.replaceItemAckAndReason(newXDdiAlert, orgXDdiAlert);
						continue;
					}
					showXDdiAlertFlag = true;
				}
				
				if( !showXDdiAlertFlag ){
					callback();
					return;
				}
			}else{
				if (orgDdiAlert != null) {
					var findItemNum1:Boolean = false;
					var findItemNum2:Boolean = false;
					for each (var mpi:MedProfileItem in medProfileItemList) {
						if (mpi.medProfileMoItem.itemNum == alertDdim.itemNum1 && mpi.medProfileMoItem.getDdiAlert(alertDdim) != null) {
							findItemNum1 = true;
						}
						if (mpi.medProfileMoItem.itemNum == alertDdim.itemNum2 && mpi.medProfileMoItem.getDdiAlert(alertDdim) != null) {
							findItemNum2 = true;
						}
					}
					if (findItemNum1 && findItemNum2) {
						MdsUtils.replaceItemAckAndReason(newDdiAlert, orgDdiAlert);
						MdsUtils.replaceItemAckAndReason(otherMoItemAlert, orgDdiAlert);
						callback();
						return;
					}
				}
				
				var latestOverrideAlert:MedProfileMoItemAlert = MdsUtils.findDdiOverrideAlert(ddiAlert, allMoItemList, allMoItemList);
				if (latestOverrideAlert != null) {
					ddiAlert.overrideReason.removeAll();
					ddiAlert.overrideReason.addAll(latestOverrideAlert.overrideReason);
				}
			}
			var evt:ShowMpDdiAlertPopupEvent = new ShowMpDdiAlertPopupEvent(alertMsg, newDdiAlert, alertMsg.crossDdiFlag?"integrate":"manual");
			evt.doNotPrescribeCallback = function():void {
				if( alertMsg.crossDdiFlag ){
					var drugName:String = alertDdim.itemNum1 > 10000 ? alertDdim.drugName1 : alertDdim.drugName2;
					var itemToBeDeleted:MedProfileMoItem = alertDdim.itemNum1 > 10000 ? moItem1 :moItem2;
					
					writeCancelPrescribeAuditLog("Do not prescribe  "+ drugName, "Save Profile");
					removeItem(itemToBeDeleted.medProfileItem, result.medProfileItemList, false, callback, mpSaveProfileCheckEvent.failureFunc);
				}else{
					writeCancelPrescribeAuditLog("Do not prescribe both drugs, A=" + alertDdim.drugName1 + ", B=" + alertDdim.drugName2, "Save Profile");
					removeItem(moItem1.medProfileItem, result.medProfileItemList, false, function():void{
						if (moItem2.medProfileItem.status == MedProfileItemStatus.Deleted || moItem2.medProfileItem.status == MedProfileItemStatus.Discontinued) {
							callback();
						}
						else {
							removeItem(moItem2.medProfileItem, result.medProfileItemList, false, callback, mpSaveProfileCheckEvent.failureFunc);
						}
					}, mpSaveProfileCheckEvent.failureFunc);
				}
			};
			evt.prescribeACallback = function():void {
				if( alertMsg.crossDdiFlag ){
					//nothing	
				}else{
					writeCancelPrescribeAuditLog("Prescribe A only, A=" + alertDdim.drugName1 + ", B=" + alertDdim.drugName2, "Save Profile");
					removeItem(moItem2.medProfileItem, result.medProfileItemList, false, callback, mpSaveProfileCheckEvent.failureFunc);
				}
			};
			evt.prescribeBCallback = function():void {
				if( alertMsg.crossDdiFlag ){
					//nothing	
				}else{
					writeCancelPrescribeAuditLog("Prescribe B only, A=" + alertDdim.drugName1 + ", B=" + alertDdim.drugName2, "Save Profile");
					removeItem(moItem1.medProfileItem, result.medProfileItemList, false, callback, mpSaveProfileCheckEvent.failureFunc);
				}
			};
			evt.overrideCallback = function():void {
				if( alertMsg.crossDdiFlag ){
					//nothing
				}else{
					MdsUtils.replaceItemAckAndReason(otherMoItemAlert, newDdiAlert);
				}
				callback();
			};
			evt.acceptCallback = evt.overrideCallback;
			evt.deferCallback = function():void {
				if ((newMoItem.firstPrintDate == null && otherMoItem.firstPrintDate == null) || (newMoItem.firstPrintDate != null && otherMoItem.firstPrintDate != null)) {
					newMoItem.medProfileItem.status   = MedProfileItemStatus.Vetted;
					otherMoItem.medProfileItem.status = MedProfileItemStatus.Vetted;
					newMoItem.mdsSuspendReason = MdsUtils.MDS_SUSPEND_REASON;
					otherMoItem.mdsSuspendReason = MdsUtils.MDS_SUSPEND_REASON;
				}
				else {
					if (newMoItem.firstPrintDate == null) {
						newMoItem.medProfileItem.status = MedProfileItemStatus.Vetted;
						newMoItem.mdsSuspendReason = MdsUtils.MDS_SUSPEND_REASON;
					}
					else {
						otherMoItem.medProfileItem.status = MedProfileItemStatus.Vetted;
						otherMoItem.mdsSuspendReason = MdsUtils.MDS_SUSPEND_REASON;
					}
				}
				
				if( alertMsg.crossDdiFlag ){
					//do nothing
				}else{
					otherMoItemAlert.overrideReason.removeAll();
				}
				callback();
			};
			evt.closeCallback = mpSaveProfileCheckEvent.failureFunc;
			dispatchEvent(evt);
			
			writeNewAlertAuditLog(result, "Save Profile");
		}
		
		private function removeItem(mpItem:MedProfileItem, mpItemList:ListCollectionView, modifyListFlag:Boolean, callback:Function, cancelCallback:Function):void {
			if (isNaN(mpItem.groupNum)) {
				var deleteItemList:ArrayCollection = new ArrayCollection;
				MdsUtils.removeRelatedDdi(mpItem.medProfileMoItem, mpItemList);
				if (MdsUtils.isWithin24Hrs(mpItem.createDate)) {
					mpItem.status = MedProfileItemStatus.Deleted;
					if( modifyListFlag ){
						mpItem.modified = true;
						MedProfileUtils.findAndDeletePurchaseRequestList(purchaseRequestList, medProfileItemList, false);
						if( isNaN(mpItem.id) ){
							deleteItemList.addItem(mpItem);
							medProfileItemList.removeItemAt(medProfileItemList.getItemIndex(mpItem));
						}
					}
				}
				else {
					mpItem.status = MedProfileItemStatus.Discontinued;
					mpItem.medProfileMoItem.endDate = new Date;
					if (modifyListFlag && !inactiveList.contains(mpItem.medProfileMoItem)) {
						mpItem.modified = true;
						mpItem.medProfileMoItem.mdsSuspendReason = null;
						VettingUtils.addToInactiveList(inactiveList, mpItem.medProfileMoItem);
						MedProfileUtils.findAndDeletePurchaseRequestList(purchaseRequestList, medProfileItemList, false);
					}
				}
				dispatchEvent(new UpdateMedProfileItemListInMemoryEvent(deleteItemList, callback));
			}
			else {
				if (MdsUtils.isGroupWithin24Hrs(medProfileItemList, mpItem.groupNum)) {
					dispatchEvent(new ShowDeleteGroupPopupEvent(mpItem, mpItemList, purchaseRequestList, modifyListFlag, callback, cancelCallback));
				}
				else {
					dispatchEvent(new ShowDiscontinueGroupPopupEvent(mpItem, mpItemList, purchaseRequestList, modifyListFlag, callback, cancelCallback));
				}
			}
		}
		
		private function reloadAllItemList(callback:Function):void {

			allMoItemList.removeAll();
			addActiveList();
			
			if (ctx.inactiveList == null) {
				dispatchEvent(new RetrieveInactiveListEvent(function(evt:Event=null):void{
					addInactiveList();
					callback();
				}));
			}
			else {
				addInactiveList();
				callback();
			}
		}
		
		private function addActiveList():void {
			for each (var mpi:MedProfileItem in medProfileItemList) {
				allMoItemList.addItem(mpi.medProfileMoItem);
			}
		}
		
		private function addInactiveList():void {
			for each (var mpMoi:MedProfileMoItem in inactiveList) {
				if (!allMoItemList.contains(mpMoi)) {
					allMoItemList.addItem(mpMoi);
				}
			}
		}
		
		private function showMdsReminder(result:ManualProfileCheckResult, callback:Function):void {
			
			if (result.errorInfoList == null || result.reminderIndex >= result.errorInfoList.length) {
				callback();
				return;
			}
			
			var errorInfo:ErrorInfo = result.errorInfoList.getItemAt(result.reminderIndex) as ErrorInfo;
			result.reminderIndex++;
			
			var sysMsgProp:SystemMessagePopupProp = new SystemMessagePopupProp("0247");
			sysMsgProp.messageWinHeight = 500;
			sysMsgProp.messageWinWidth  = 970;
			sysMsgProp.setOkButtonOnly = true;
			sysMsgProp.functionId = "CMS-0002";
			if (errorInfo.supplMsg != null) {
				sysMsgProp.messageParams = new Array(errorInfo.mainMsg, errorInfo.supplMsg);
			}
			else {
				sysMsgProp.messageParams = new Array(errorInfo.mainMsg);
			}
			sysMsgProp.okHandler = function(mouseEvent:MouseEvent):void {
				showMdsReminder(result, callback);
				PopUpManager.removePopUp(((mouseEvent.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
			};
			
			dispatchEvent(new RetrieveSystemMessageEvent(sysMsgProp));
		}
	}
}
