package hk.org.ha.event.pms.main.alert.mds.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	import mx.collections.ListCollectionView;
	
	public class ShowSteroidAlertPopupEvent extends AbstractTideEvent {
		
		private var _steroidAlertList:ListCollectionView;
		
		private var _callback:Function;
		
		private var _closeOnly:Boolean;
		
		public function ShowSteroidAlertPopupEvent(steroidAlertList:ListCollectionView, callback:Function=null, closeOnly:Boolean=false) {
			super();
			_steroidAlertList = steroidAlertList;
			_callback = callback;
			_closeOnly = closeOnly;
		}
		
		public function get steroidAlertList():ListCollectionView {
			return _steroidAlertList;
		}
		
		public function get callback():Function {
			return _callback == null ? function():void{} : _callback;
		}
		
		public function get closeOnly():Boolean {
			return _closeOnly;
		}
	}
}
