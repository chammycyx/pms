package hk.org.ha.event.pms.main.drug {
	
	import hk.org.ha.model.pms.dms.vo.DrugName;
	import hk.org.ha.model.pms.dms.vo.RouteFormSearchCriteria;
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDrugDetailEvent extends AbstractTideEvent 
	{			
		private var _drugSearchSource:DrugSearchSource;
		private var _routeFormSearchCriteria:RouteFormSearchCriteria;
		private var _theraGroup:String;

		public function RetrieveDrugDetailEvent(drugSearchSource:DrugSearchSource, routeFormSearchCriteria:RouteFormSearchCriteria, theraGroup:String):void 
		{
			_drugSearchSource = drugSearchSource;
			_routeFormSearchCriteria = routeFormSearchCriteria;
			_theraGroup = theraGroup;
			super();
		}

		public function get drugSearchSource():DrugSearchSource
		{
			return _drugSearchSource;
		}
		
		public function get routeFormSearchCriteria():RouteFormSearchCriteria
		{
			return _routeFormSearchCriteria;
		}
		
		public function get theraGroup():String
		{
			return _theraGroup;
		}
	}
}