package hk.org.ha.event.pms.main.vetting {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class CancelOrderEvent extends AbstractTideEvent {
		
		private var _suspendFlag:Boolean;
		private var _suspendCode:String;
		private var _releaseOrderFlag:Boolean;
		private var _callback:Function;

		public function CancelOrderEvent(suspendFlag:Boolean, suspendCode:String, callback:Function) {
			super();
			_suspendFlag = suspendFlag;
			_suspendCode = suspendCode;
			_callback = callback;
		}

		public function get suspendFlag():Boolean {
			return _suspendFlag;
		}
		
		public function get suspendCode():String {
			return _suspendCode;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}
