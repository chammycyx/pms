package  hk.org.ha.event.pms.main.reftable.popup {
	
	import hk.org.ha.event.pms.main.reftable.popup.DueOrderScheduleTemplateMaintWardGroupLookupPopupProp;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDueOrderScheduleTemplateMaintWardGroupLookupPopupEvent extends AbstractTideEvent 
	{
		private var _dueOrderScheduleTemplateMaintWardGroupLookupPopupProp:DueOrderScheduleTemplateMaintWardGroupLookupPopupProp;
		
		public function ShowDueOrderScheduleTemplateMaintWardGroupLookupPopupEvent(dueOrderScheduleTemplateMaintWardGroupLookupPopupProp:DueOrderScheduleTemplateMaintWardGroupLookupPopupProp):void 
		{
			super();
			_dueOrderScheduleTemplateMaintWardGroupLookupPopupProp = dueOrderScheduleTemplateMaintWardGroupLookupPopupProp;
		}
		
		public function get dueOrderScheduleTemplateMaintWardGroupLookupPopupProp():DueOrderScheduleTemplateMaintWardGroupLookupPopupProp {
			return _dueOrderScheduleTemplateMaintWardGroupLookupPopupProp;
		}
	}
}