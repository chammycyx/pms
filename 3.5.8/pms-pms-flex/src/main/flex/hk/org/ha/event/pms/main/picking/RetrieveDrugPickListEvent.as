package hk.org.ha.event.pms.main.picking
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDrugPickListEvent extends AbstractTideEvent 
	{		
		private var _workstationId:Number
		private var _resultMsg:String;
		
		public function RetrieveDrugPickListEvent(workstationId:Number, resultMsg:String=null):void 
		{
			super();
			_workstationId = workstationId;
			_resultMsg = resultMsg;
		}
		
		public function get workstationId():Number {
			return _workstationId;
		}
		
		public function get resultMsg():String {
			return _resultMsg;
		}
	}
}