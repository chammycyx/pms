package hk.org.ha.event.pms.main.reftable {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetSuspendReasonListEditableEvent extends AbstractTideEvent 
	{
		
		public function SetSuspendReasonListEditableEvent():void 
		{
			super();
		}
	}
}