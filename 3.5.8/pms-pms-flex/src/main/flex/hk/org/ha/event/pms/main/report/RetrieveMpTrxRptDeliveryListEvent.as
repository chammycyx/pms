package hk.org.ha.event.pms.main.report {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveMpTrxRptDeliveryListEvent extends AbstractTideEvent 
	{
		private var _wardCode:String;
		private var _batchCode:String;
		private var _caseNum:String;
		private var _dispenseDate:Date;

		public function RetrieveMpTrxRptDeliveryListEvent( wardCode:String, batchCode:String, caseNum:String, dispenseDate:Date):void 
		{
			super();
			_wardCode = wardCode;
			_batchCode = batchCode;
			_caseNum = caseNum;
			_dispenseDate = dispenseDate;
		}
		
		public function get wardCode():String{
			return _wardCode;
		}
		
		public function get batchCode():String{
			return _batchCode;
		}
		
		public function get caseNum():String{
			return _caseNum;
		}
		
		public function get dispenseDate():Date{
			return _dispenseDate;
		}
	}
}