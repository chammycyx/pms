package hk.org.ha.event.pms.main.reftable {
	
	import mx.collections.ListCollectionView;
	import mx.core.UIComponent;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateRefillHolidayListEvent extends AbstractTideEvent 
	{
		private var _updateRefillHolidayList:ListCollectionView;
		private var _delRefillHolidayList:ListCollectionView;
		private var _nextTabIndex:Number;
		private var _showRefillModuleMaintAlertMsg:Boolean;
		private var _caller:UIComponent;
		
		public function UpdateRefillHolidayListEvent(updateRefillHolidayList:ListCollectionView, 
													 delRefillHolidayList:ListCollectionView, 
													 nextTabIndex:Number, 
													 showRefillModuleMaintAlertMsg:Boolean, 
													 caller:UIComponent):void 
		{
			super();
			_updateRefillHolidayList = updateRefillHolidayList;
			_delRefillHolidayList = delRefillHolidayList;
			_nextTabIndex = nextTabIndex;
			_showRefillModuleMaintAlertMsg = showRefillModuleMaintAlertMsg;
			_caller = caller;
		}
		
		public function get updateRefillHolidayList():ListCollectionView
		{
			return _updateRefillHolidayList;
		}
		
		public function get delRefillHolidayList():ListCollectionView
		{
			return _delRefillHolidayList;
		}
		
		public function get nextTabIndex():Number
		{
			return _nextTabIndex;
		}
		
		public function get showRefillModuleMaintAlertMsg():Boolean {
			return _showRefillModuleMaintAlertMsg;
		}
		
		public function get caller():UIComponent {
			return _caller;
		}
	}
}