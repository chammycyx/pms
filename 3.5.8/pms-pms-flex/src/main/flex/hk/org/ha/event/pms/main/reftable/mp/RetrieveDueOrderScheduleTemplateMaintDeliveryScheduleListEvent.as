package hk.org.ha.event.pms.main.reftable.mp {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDueOrderScheduleTemplateMaintDeliveryScheduleListEvent extends AbstractTideEvent 
	{	
		private var _callback:Function;
		
		public function RetrieveDueOrderScheduleTemplateMaintDeliveryScheduleListEvent(callback:Function):void 
		{
			super();
			_callback = callback;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}

