package hk.org.ha.fmk.pms.flex.utils {
	public class OrderNumValidator {
		
		public static function isValidOrderNum(orderNum:String):Boolean {
			if (isOrderNum(orderNum)) {
				if ((orderNum.length == 15 && new RegExp("^MOE[A-Za-z][A-Za-z][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$").test(orderNum)) || 
					orderNum.length == 16) {
					return checkOrderNumDigit(orderNum);
				}
				
				return true;
			}
			
			return false;
		}
		
		private static function isOrderNum(orderNum:String):Boolean {
			if (orderNum.length == 14 && new RegExp("^MOE[A-Za-z][A-Za-z][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$").test(orderNum)) {
				return true;
			}
			
			if (orderNum.length == 15 && (new RegExp("^MOE[A-Za-z][A-Za-z][A-Za-z ][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$").test(orderNum) || 
				new RegExp("^MOE[A-Za-z][A-Za-z][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$").test(orderNum))) {
				return true;
			}
			
			if (orderNum.length == 16 && new RegExp("^MOE[A-Za-z][A-Za-z][A-Za-z ][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$").test(orderNum)) {
				return true;
			}
			
			return false;
		}
		
		private static function checkOrderNumDigit(orderNum:String):Boolean {
			var trunOrderNum:String;
			var digit:int;
			var totalSum:int;
			var checkSum:int;
			var checkDigit:String;
			
			if 	(orderNum.length == 15) {
				trunOrderNum = orderNum.substring(5, 14);
			} else {
				trunOrderNum = orderNum.substring(6, 15);
			}
			
			for (var i:int = trunOrderNum.length; i > 0; i--) {
				var index:int = trunOrderNum.length - i;
				if (trunOrderNum.charCodeAt(index) >= 'A'.charCodeAt() && trunOrderNum.charCodeAt(index) <= 'Z'.charCodeAt()) {
					digit = 66 - trunOrderNum.charCodeAt(index);
				} else {
					digit = parseInt(trunOrderNum.substring(index, index + 1));
				}
				totalSum = totalSum + digit * i;
			}
			
			checkSum = trunOrderNum.length - (totalSum % trunOrderNum.length);
			
			if (checkSum > 9) {
				checkDigit = String.fromCharCode(55 + checkSum);
			} else {
				checkDigit = checkSum.toString();;
			}
			
			if (orderNum.substring(orderNum.length-1) == checkDigit) {
				return true;
			}
			
			return false;
		}
	}
}