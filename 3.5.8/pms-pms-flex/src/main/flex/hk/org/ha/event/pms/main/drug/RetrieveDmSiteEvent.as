package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDmSiteEvent extends AbstractTideEvent 
	{
		private var _siteCode:String;
		private var _successEvent:Event;
		private var _failEvent:Event;
		
		public function RetrieveDmSiteEvent(siteCode:String, successEvent:Event=null, failEvent:Event=null):void 
		{
			super();
			_siteCode = siteCode;
			_successEvent = successEvent;
			_failEvent = failEvent;
		}
		
		public function get siteCode():String
		{
			return _siteCode;
		}
		
		public function get successEvent():Event
		{
			return _successEvent;
		}
		
		public function get failEvent():Event
		{
			return _failEvent;
		}
	}
}