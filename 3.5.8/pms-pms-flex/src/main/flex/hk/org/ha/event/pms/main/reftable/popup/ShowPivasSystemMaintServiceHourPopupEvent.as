package hk.org.ha.event.pms.main.reftable.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowPivasSystemMaintServiceHourPopupEvent extends AbstractTideEvent 
	{
		private var _pivasSystemMaintServiceHourPopupProp:PivasSystemMaintServiceHourPopupProp;
		
		public function ShowPivasSystemMaintServiceHourPopupEvent(pivasSystemMaintServiceHourPopupProp:PivasSystemMaintServiceHourPopupProp):void 
		{
			super();
			_pivasSystemMaintServiceHourPopupProp = pivasSystemMaintServiceHourPopupProp;
		}
		
		public function get pivasSystemMaintServiceHourPopupProp():PivasSystemMaintServiceHourPopupProp{
			return _pivasSystemMaintServiceHourPopupProp;
		}
	}
}