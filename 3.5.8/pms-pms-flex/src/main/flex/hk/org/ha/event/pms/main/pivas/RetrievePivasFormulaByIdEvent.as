package hk.org.ha.event.pms.main.pivas
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePivasFormulaByIdEvent extends AbstractTideEvent 
	{		
		private var _pivasFormulaId:Number;
		
		private var _callbackFunc:Function;
		
		public function RetrievePivasFormulaByIdEvent(pivasFormulaId:Number, callbackFunc:Function):void 
		{
			super();
			_pivasFormulaId = pivasFormulaId;
			_callbackFunc = callbackFunc;
		}

		public function get pivasFormulaId():Number {
			return _pivasFormulaId;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}