package hk.org.ha.event.pms.main.checkissue.mp
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveUnlinkedItemListEvent extends AbstractTideEvent 
	{			
		private var _batchDate:Date;
		private var _checked: Boolean
		
		public function RetrieveUnlinkedItemListEvent(batchDate:Date, checked:Boolean=false):void 
		{
			super();
			_batchDate = batchDate;
			_checked = checked;
		}
		
		public function get batchDate():Date {
			return _batchDate;
		}
		
		public function get checked():Boolean {
			return _checked;
		}
	}
}