package hk.org.ha.event.pms.main.dispensing {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSpecialLabelListEvent extends AbstractTideEvent 
	{
		private var _labelNumPrefix:String;
		
		public function RetrieveSpecialLabelListEvent( labelNumPrefix:String):void 
		{
			super();
			_labelNumPrefix = labelNumPrefix;
		}
		
		public function get labelNumPrefix():String {
			return _labelNumPrefix;
		}
	}
}