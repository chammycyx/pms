package hk.org.ha.event.pms.main.vetting.mp
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateManualEntryVettingAssociationEvent extends AbstractTideEvent
	{
		private var _map:Object;
		
		public function UpdateManualEntryVettingAssociationEvent(map:Object)
		{
			super();
			_map = map;
		}
		
		public function get map():Object
		{
			return _map;
		}
	}
}