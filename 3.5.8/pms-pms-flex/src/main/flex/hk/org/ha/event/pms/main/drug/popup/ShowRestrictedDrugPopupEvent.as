package hk.org.ha.event.pms.main.drug.popup
{
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	import mx.collections.ArrayCollection;
	
	public class ShowRestrictedDrugPopupEvent extends AbstractTideEvent
	{
		private var _drugName:String;
		private var _specialInstruction:String;
		private var _popupOkHandler:Function;
		private var _popupCancelHandler:Function;
		
		public function ShowRestrictedDrugPopupEvent(drugName:String, specialInstructionValue:String, popupOkHandler:Function, popupCancelHandler:Function=null)
		{
			super();
			_drugName = drugName;
			_specialInstruction = specialInstructionValue;
			_popupOkHandler = popupOkHandler;
			_popupCancelHandler = popupCancelHandler;
		}

		public function get drugName():String{
			return _drugName;
		}
		
		public function get specialInstruction():String
		{
			return _specialInstruction;
		}
		
		public function get popupOkHandler():Function{
			return _popupOkHandler;
		}
		
		public function get popupCancelHandler():Function{
			return _popupCancelHandler;
		}
	}
}