package hk.org.ha.event.pms.main.reftable.mp
{
	import hk.org.ha.model.pms.dms.persistence.PmsDosageConversion;
	import hk.org.ha.model.pms.dms.persistence.PmsDosageConversionSet;
	
	import mx.collections.ArrayCollection;
	import mx.controls.List;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class UpdateFixedConcnStatusListEvent extends AbstractTideEvent
	{
		private var _fixedConcnStatusList:ArrayCollection;
		
		public function get fixedConcnStatusList():ArrayCollection
		{
			return _fixedConcnStatusList;
		}

		public function UpdateFixedConcnStatusListEvent(fixedConcnStatusList:ArrayCollection):void
		{
			super();
			_fixedConcnStatusList = fixedConcnStatusList;
		}
	}
}