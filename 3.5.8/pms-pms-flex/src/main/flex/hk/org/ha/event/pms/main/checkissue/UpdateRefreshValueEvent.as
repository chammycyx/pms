package hk.org.ha.event.pms.main.checkissue
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateRefreshValueEvent extends AbstractTideEvent 
	{		
		private var _refresh:Boolean;

		public function UpdateRefreshValueEvent(refresh:Boolean):void 
		{
			super();
			_refresh = refresh;
		}

		public function get refresh():Boolean {
			return _refresh;
		}
	}
}