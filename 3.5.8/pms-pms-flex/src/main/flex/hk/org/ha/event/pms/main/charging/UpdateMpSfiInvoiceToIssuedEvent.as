package hk.org.ha.event.pms.main.charging {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.charging.MpSfiInvoice;
	
	public class UpdateMpSfiInvoiceToIssuedEvent extends AbstractTideEvent 
	{	
		private var _mpSfiInvoice:MpSfiInvoice;
		
		public function UpdateMpSfiInvoiceToIssuedEvent(mpSfiInvoice:MpSfiInvoice):void 
		{
			super();
			_mpSfiInvoice = mpSfiInvoice;
		}
		
		public function get mpSfiInvoice():MpSfiInvoice {
			return _mpSfiInvoice;
		}
	}
}