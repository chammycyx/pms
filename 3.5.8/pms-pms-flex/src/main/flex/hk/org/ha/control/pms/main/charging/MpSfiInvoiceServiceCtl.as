package hk.org.ha.control.pms.main.charging {

	import hk.org.ha.event.pms.main.charging.ClearMpSfiInvoiceViewEvent;
	import hk.org.ha.event.pms.main.charging.CreateMpSfiInvoiceItemEvent;
	import hk.org.ha.event.pms.main.charging.RefreshMpSfiInvoiceItemListEvent;
	import hk.org.ha.event.pms.main.charging.RefreshMpSfiInvoiceViewEvent;
	import hk.org.ha.event.pms.main.charging.ReprintMpSfiInvoiceEvent;
	import hk.org.ha.event.pms.main.charging.RetrieveInvoiceEvent;
	import hk.org.ha.event.pms.main.charging.RetrieveMpSfiInvoiceInvoiceListEvent;
	import hk.org.ha.event.pms.main.charging.RetrieveMpSfiInvoiceItemListEvent;
	import hk.org.ha.event.pms.main.charging.RetrieveMpSfiInvoicePrivatePatInfoListEvent;
	import hk.org.ha.event.pms.main.charging.SaveAndPrintMpSfiInvoiceEvent;
	import hk.org.ha.event.pms.main.charging.ShowMpSfiInvoiceItemSystemMessageEvent;
	import hk.org.ha.event.pms.main.charging.UpdateMpSfiInvoiceItemEvent;
	import hk.org.ha.event.pms.main.charging.UpdateMpSfiInvoiceToIssuedEvent;
	import hk.org.ha.event.pms.main.charging.VoidMpSfiInvoiceEvent;
	import hk.org.ha.event.pms.main.charging.VoidMpSfiInvoiceSuccessEvent;
	import hk.org.ha.event.pms.main.charging.popup.ShowMpSfiInvoiceInvoiceListPopupEvent;
	import hk.org.ha.event.pms.main.charging.popup.ShowMpSfiInvoicePrivatePatListPopupEvent;
	import hk.org.ha.model.pms.biz.charging.MpSfiInvoiceServiceBean;
	import hk.org.ha.model.pms.vo.charging.MpSfiInvoice;
	import hk.org.ha.model.pms.vo.charging.MpSfiInvoiceInvoiceListInfo;
	import hk.org.ha.model.pms.vo.charging.MpSfiInvoiceItem;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("mpSfiInvoiceServiceCtl", restrict="true")]
	public class MpSfiInvoiceServiceCtl {
		
		[In]
		public var mpSfiInvoiceService:MpSfiInvoiceServiceBean;
		
		private var selectedItem:MpSfiInvoiceItem;
		private var invoiceNum:String;
		private var _mpSfiInvoiceInvoiceListInfo:MpSfiInvoiceInvoiceListInfo;
		
		[Observer]
		public function retrieveMpSfiInvoiceItemList(evt:RetrieveMpSfiInvoiceItemListEvent):void{
			In(Object(mpSfiInvoiceService).messageCode)
			mpSfiInvoiceService.retrieveDeliveryItemList(evt.hkid, evt.caseNum, evt.medProfileId, retrieveMpSfiInvoiceItemListResult);
		}
		
		[Observer]
		public function retrieveInvoice(evt:RetrieveInvoiceEvent):void{
			mpSfiInvoiceService.retrieveMpSfiInvoice(evt.invoiceNum, retrieveMpSfiInvoiceItemListResult);
		}
		
		private function retrieveMpSfiInvoiceItemListResult(evt:TideResultEvent):void{
			evt.context.dispatchEvent(new RefreshMpSfiInvoiceItemListEvent(evt.result as MpSfiInvoice, Object(mpSfiInvoiceService).messageCode));
		}
		
		[Observer]
		public function retrieveMpSfiInvoicePrivatePatInfoList(evt:RetrieveMpSfiInvoicePrivatePatInfoListEvent):void{
			mpSfiInvoiceService.retrievePrivatePatList(retrieveMpSfiInvoicePrivatePatInfoListResult);
		}
		
		private function retrieveMpSfiInvoicePrivatePatInfoListResult(evt:TideResultEvent):void{
			evt.context.dispatchEvent(new ShowMpSfiInvoicePrivatePatListPopupEvent());
		}
		
		[Observer]
		public function saveAndPrintMpSfiInvoice(evt:SaveAndPrintMpSfiInvoiceEvent):void{
			In(Object(mpSfiInvoiceService).messageCode)
			In(Object(mpSfiInvoiceService).success)
			mpSfiInvoiceService.saveAndPrintInvoice(evt.mpSfiInvoice, saveAndPrintMpSfiInvoiceResult);
		}
		
		private function saveAndPrintMpSfiInvoiceResult(evt:TideResultEvent):void{
			evt.context.dispatchEvent(new RefreshMpSfiInvoiceViewEvent(Object(mpSfiInvoiceService).messageCode, Object(mpSfiInvoiceService).success));
		}
		
		[Observer]
		public function updateMpSfiInvoiceToIssued(evt:UpdateMpSfiInvoiceToIssuedEvent):void{
			In(Object(mpSfiInvoiceService).messageCode)
			In(Object(mpSfiInvoiceService).success)
			mpSfiInvoiceService.updateMpSfiInvoiceToIssued(evt.mpSfiInvoice, updateMpSfiInvoiceToIssuedResult);
		}
		
		private function updateMpSfiInvoiceToIssuedResult(evt:TideResultEvent):void{
			evt.context.dispatchEvent(new RefreshMpSfiInvoiceViewEvent(Object(mpSfiInvoiceService).messageCode, Object(mpSfiInvoiceService).success));
		}
		
		[Observer]
		public function createMpSfiInvoiceItem(evt:CreateMpSfiInvoiceItemEvent):void{
			selectedItem = evt.selectedItem;
			mpSfiInvoiceService.createMpSfiInvoiceItem(evt.itemCode, createMpSfiInvoiceItemResult);
		}
		
		private function createMpSfiInvoiceItemResult(evt:TideResultEvent):void{
			evt.context.dispatchEvent(new UpdateMpSfiInvoiceItemEvent(evt.result as MpSfiInvoiceItem, selectedItem));
		}
		
		[Observer]
		public function retrieveMpSfiInvoiceInvoiceList(evt:RetrieveMpSfiInvoiceInvoiceListEvent):void{
			mpSfiInvoiceService.retrieveInvoiceList(evt.hkid, evt.caseNum, retrieveMpSfiInvoiceInvoiceListResult);
		}
		
		private function retrieveMpSfiInvoiceInvoiceListResult(evt:TideResultEvent):void{
			var mpSfiInvoiceInvoiceListInfoList:ArrayCollection = evt.result as ArrayCollection;
			if ( mpSfiInvoiceInvoiceListInfoList.length > 0 ) {
				evt.context.dispatchEvent(new ShowMpSfiInvoiceInvoiceListPopupEvent(evt.result as ArrayCollection)); 
			} else {
				evt.context.dispatchEvent(new ShowMpSfiInvoiceItemSystemMessageEvent("0005"));
			}
		}
		
		[Observer]
		public function reprintMpSfiInvoice(evt:ReprintMpSfiInvoiceEvent):void{
			In(Object(mpSfiInvoiceService).messageCode)
			mpSfiInvoiceService.reprintMpSfiInvoice(reprintMpSfiInvoiceResult);
		}
		
		private function reprintMpSfiInvoiceResult(evt:TideResultEvent):void{
			var messageCode:String = Object(mpSfiInvoiceService).messageCode as String;
			if ( messageCode != null && messageCode.length > 0 ) {
				evt.context.dispatchEvent(new ShowMpSfiInvoiceItemSystemMessageEvent(Object(mpSfiInvoiceService).messageCode));
			}
		}
		
		[Observer]
		public function voidMpSfiInvoice(evt:VoidMpSfiInvoiceEvent):void{
			invoiceNum = evt.invoiceNum;
			mpSfiInvoiceService.voidMpSfiInvoice(evt.invoiceNum, evt.deliveryItemIdList, voidMpSfiInvoiceResult);
		}
		
		private function voidMpSfiInvoiceResult(evt:TideResultEvent):void{
			evt.context.dispatchEvent(new VoidMpSfiInvoiceSuccessEvent(invoiceNum));
		}
		
		[Observer]
		public function clearMpSfiInvoiceView(evt:ClearMpSfiInvoiceViewEvent):void{
			mpSfiInvoiceService.clearScreen();
		}
	}
}
