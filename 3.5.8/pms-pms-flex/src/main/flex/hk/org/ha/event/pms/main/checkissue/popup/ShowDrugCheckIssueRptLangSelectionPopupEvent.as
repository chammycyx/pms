package hk.org.ha.event.pms.main.checkissue.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDrugCheckIssueRptLangSelectionPopupEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		private var _ticketDate:Date;
		private var _ticketNum:String;
		private var _closeFunc:Function;
		
		public function ShowDrugCheckIssueRptLangSelectionPopupEvent(ticketDate:Date, ticketNum:String, closeFunc:Function=null, clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
			_ticketDate = ticketDate;
			_ticketNum = ticketNum;
			_closeFunc = closeFunc;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
		
		public function get ticketDate():Date {
			return _ticketDate;
		}
		
		public function get ticketNum():String {
			return _ticketNum;
		}
		
		public function get closeFunc():Function {
			return _closeFunc;
		}
	}
}