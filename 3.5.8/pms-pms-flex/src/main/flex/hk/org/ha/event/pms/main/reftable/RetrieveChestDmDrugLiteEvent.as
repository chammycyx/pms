package hk.org.ha.event.pms.main.reftable {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveChestDmDrugLiteEvent extends AbstractTideEvent 
	{
		private var _itemCode:String;
		private var _editItemIndex:Number;
		private var _operationResultName:String;
		
		public function RetrieveChestDmDrugLiteEvent(itemCode:String):void 
		{
			super();
			_itemCode = itemCode;
		}
		
		public function get itemCode():String {
			return _itemCode;
		}
	}
}