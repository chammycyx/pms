package hk.org.ha.control.pms.main.report {
	import hk.org.ha.event.pms.main.report.PrintEdsWorkloadRptEvent;
	import hk.org.ha.event.pms.main.report.RefreshEdsWorkloadRptEvent;
	import hk.org.ha.event.pms.main.report.RetrieveEdsWorkloadRptEvent;
	import hk.org.ha.model.pms.biz.report.EdsWorkloadRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("edsWorkloadRptServiceCtl", restrict="true")]
	public class EdsWorkloadRptServiceCtl 
	{
		[In]
		public var edsWorkloadRptService:EdsWorkloadRptServiceBean;
		
		[Observer]
		public function retrieveEdsWorkloadRpt(evt:RetrieveEdsWorkloadRptEvent):void
		{
			In(Object(edsWorkloadRptService).retrieveSuccess)
			edsWorkloadRptService.retrieveEdsWorkloadRpt(evt.selectedTab, evt.date, retrieveEdsWorkloadRptResult);
		}
		
		private function retrieveEdsWorkloadRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshEdsWorkloadRptEvent(Object(edsWorkloadRptService).retrieveSuccess));
		}
		
		[Observer]
		public function printEdsWorkloadRpt(evt:PrintEdsWorkloadRptEvent):void
		{
			edsWorkloadRptService.printEdsWorkloadRpt();
		}
		
	}
}
