package hk.org.ha.event.pms.main.drug {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveFormVerbPropertiesEvent extends AbstractTideEvent 
	{		
		private var _callBackFunc:Function;
		
		public function RetrieveFormVerbPropertiesEvent(callBackFuncValue:Function):void 
		{
			super();
			_callBackFunc = callBackFuncValue;
		}
			
		public function get callBackFunc():Function
		{
			return _callBackFunc;
		}
	}
}