package hk.org.ha.control.pms.main.report {
	import hk.org.ha.event.pms.main.report.PrintWorkloadStatRptEvent;
	import hk.org.ha.event.pms.main.report.RefreshWorkloadStatRptEvent;
	import hk.org.ha.event.pms.main.report.RetrieveWorkloadStatRptEvent;
	import hk.org.ha.model.pms.biz.report.WorkloadStatRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("workloadStatRptServiceCtl", restrict="true")]
	public class WorkloadStatRptServiceCtl 
	{
		[In]
		public var workloadStatRptService:WorkloadStatRptServiceBean;
		
		[Observer]
		public function retrieveWorkloadStatRpt(evt:RetrieveWorkloadStatRptEvent):void
		{
			In(Object(workloadStatRptService).retrieveSuccess)
			workloadStatRptService.retrieveWorkloadStatList(evt.pastDay, evt.dateFrom, evt.dateTo, retrieveWorkloadStatRptResult);
		}
		
		private function retrieveWorkloadStatRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshWorkloadStatRptEvent(Object(workloadStatRptService).retrieveSuccess));
		}
		
		[Observer]
		public function printWorkloadStatRpt(evt:PrintWorkloadStatRptEvent):void
		{
			workloadStatRptService.printWorkloadStatRpt();
		}
		
	}
}
