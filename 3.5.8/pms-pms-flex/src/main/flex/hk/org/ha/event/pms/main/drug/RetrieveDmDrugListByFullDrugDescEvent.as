package hk.org.ha.event.pms.main.drug {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDmDrugListByFullDrugDescEvent extends AbstractTideEvent 
	{
		private var _fullDrugDesc:String;
		
		public function RetrieveDmDrugListByFullDrugDescEvent(fullDrugDesc:String=null):void 
		{
			super();
			_fullDrugDesc = fullDrugDesc;
		}
		
		public function get fullDrugDesc():String
		{
			return _fullDrugDesc;
		}
	}
}