package hk.org.ha.event.pms.main.reftable {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveChestEvent extends AbstractTideEvent 
	{
		private var _unitDoseName:String;
		
		public function RetrieveChestEvent(unitDoseName:String):void 
		{
			super();
			_unitDoseName = unitDoseName;
		}
		
		public function get unitDoseName():String
		{
			return _unitDoseName;
		}
	}
}