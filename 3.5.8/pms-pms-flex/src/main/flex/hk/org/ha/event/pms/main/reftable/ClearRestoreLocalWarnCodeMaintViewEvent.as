package hk.org.ha.event.pms.main.reftable {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ClearRestoreLocalWarnCodeMaintViewEvent extends AbstractTideEvent 
	{		
		public function ClearRestoreLocalWarnCodeMaintViewEvent():void 
		{
			super();
		}
	}
}