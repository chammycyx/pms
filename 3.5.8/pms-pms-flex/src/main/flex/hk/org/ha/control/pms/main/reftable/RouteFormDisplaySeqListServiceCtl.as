package hk.org.ha.control.pms.main.reftable {
	
	import hk.org.ha.event.pms.main.reftable.RefreshPasSpecListEvent;
	import hk.org.ha.event.pms.main.reftable.RefreshPmsRouteFormSortSpecListEvent;
	import hk.org.ha.event.pms.main.reftable.RefreshRouteFormDefaultSeqListEvent;
	import hk.org.ha.event.pms.main.reftable.RefreshRouteFormDisplaySeqListEvent;
	import hk.org.ha.event.pms.main.reftable.RetrievePasSpecialtyListEvent;
	import hk.org.ha.event.pms.main.reftable.RetrieveRouteFormDefaultSeqListEvent;
	import hk.org.ha.event.pms.main.reftable.RetrieveRouteFormSortSpecListEvent;
	import hk.org.ha.event.pms.main.reftable.UpdatePmsRouteSortSpecListEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.lookup.RefreshLookupPopupDataProviderEvent;
	import hk.org.ha.model.pms.biz.reftable.RouteFormDisplaySeqListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("routeFormDisplaySeqListServiceCtl", restrict="true")]
	public class RouteFormDisplaySeqListServiceCtl 
	{	
		[In]
		public var routeFormDisplaySeqListService:RouteFormDisplaySeqListServiceBean;
		
		[In]
		public var routeFormPasSpecialtyList:ArrayCollection;
		
		[Observer]
		public function retrieveRouteFormSortSpecList(evt:RetrieveRouteFormSortSpecListEvent):void
		{
			routeFormDisplaySeqListService.retrieveRouteFormSortSpecList(evt.specCode, evt.specType, retrieveRouteFormSortSpecListResult);
		}
		
		private function retrieveRouteFormSortSpecListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshRouteFormDisplaySeqListEvent);
		}
		
		[Observer]
		public function retrievePasSpecialtyList(evt:RetrievePasSpecialtyListEvent):void
		{
			routeFormDisplaySeqListService.retrievePasSpecList(evt.prefixSpecCode, evt.specType, retrievePasSpecialtyListResult);
		}
		
		private function retrievePasSpecialtyListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshLookupPopupDataProviderEvent(routeFormPasSpecialtyList)); 
		}
		
		[Observer]
		public function updatePmsRouteSortSpecList(evt:UpdatePmsRouteSortSpecListEvent):void
		{
			routeFormDisplaySeqListService.updateRouteSortSpecList(evt.newPmsRouteSortSpecList, updatePmsRouteSortSpecListResult);
		}
		
		private function updatePmsRouteSortSpecListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshPmsRouteFormSortSpecListEvent());
		}
		
		[Observer]
		public function retrieveRouteFormDefaultSeqList(evt:RetrieveRouteFormDefaultSeqListEvent):void
		{
			routeFormDisplaySeqListService.retrieveRouteFormDefaultSeqList(evt.specCode, evt.specType, retrieveRouteFormDefaultSeqListResult);
		}

		private function retrieveRouteFormDefaultSeqListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshRouteFormDefaultSeqListEvent);
		}
	}
}
