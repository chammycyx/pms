package hk.org.ha.event.pms.main.report {
	
	import hk.org.ha.model.pms.vo.report.DrugRefillListRptInfo;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetDrugRefillListRptInfoEvent extends AbstractTideEvent 
	{
		private var _drugRefillListRptInfo:DrugRefillListRptInfo;
		
		public function SetDrugRefillListRptInfoEvent(drugRefillListRptInfo:DrugRefillListRptInfo):void 
		{
			super();
			_drugRefillListRptInfo = drugRefillListRptInfo;
		}
		
		public function get drugRefillListRptInfo():DrugRefillListRptInfo {
			return _drugRefillListRptInfo;
		}
	}
}