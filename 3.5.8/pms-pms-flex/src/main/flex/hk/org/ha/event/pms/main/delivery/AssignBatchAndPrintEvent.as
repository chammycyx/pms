package hk.org.ha.event.pms.main.delivery
{
	import hk.org.ha.model.pms.persistence.medprofile.DeliveryRequest;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class AssignBatchAndPrintEvent extends AbstractTideEvent
	{
		private var _request:DeliveryRequest;
		private var _callbackFunction:Function;
		
		public function AssignBatchAndPrintEvent(request:DeliveryRequest, callbackFunction:Function = null)
		{
			super();
			_request = request;
			_callbackFunction = callbackFunction;
		}
		
		public function get request():DeliveryRequest
		{
			return _request;
		}
		
		public function get callbackFunction():Function
		{
			return _callbackFunction;
		}
	}
}