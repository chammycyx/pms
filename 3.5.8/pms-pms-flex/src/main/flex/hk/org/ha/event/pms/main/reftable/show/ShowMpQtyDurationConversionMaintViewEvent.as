package  hk.org.ha.event.pms.main.reftable.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowMpQtyDurationConversionMaintViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		public function ShowMpQtyDurationConversionMaintViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}