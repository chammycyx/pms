package hk.org.ha.event.pms.main.reftable {
	
	import mx.core.UIComponent;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateEnableSelectionFlagEvent extends AbstractTideEvent 
	{		
		private var _enableSelectionFlag:Boolean;
		private var _nextTabIndex:Number;
		private var _showRefillModuleMaintAlertMsg:Boolean;
		private var _showSaveSuccessMsg:Boolean;
		private var _caller:UIComponent;
		
		public function UpdateEnableSelectionFlagEvent(enableSelectionFlag:Boolean, 
													   nextTabIndex:Number, 
													   showRefillModuleMaintAlertMsg:Boolean, 
													   showSaveSuccessMsg:Boolean, 
													   caller:UIComponent):void 
		{
			super();
			_enableSelectionFlag = enableSelectionFlag;
			_nextTabIndex = nextTabIndex;
			_showRefillModuleMaintAlertMsg = showRefillModuleMaintAlertMsg;
			_showSaveSuccessMsg = showSaveSuccessMsg;
			_caller = caller;
		}
		
		public function get enableSelectionFlag():Boolean
		{
			return _enableSelectionFlag;
		}
		
		public function get nextTabIndex():Number
		{
			return _nextTabIndex;
		}
		
		public function get showRefillModuleMaintAlertMsg():Boolean
		{
			return _showRefillModuleMaintAlertMsg;
		}
		
		public function get showSaveSuccessMsg():Boolean
		{
			return _showSaveSuccessMsg;
		}
		
		public function get caller():UIComponent
		{
			return _caller;
		}
	}
}