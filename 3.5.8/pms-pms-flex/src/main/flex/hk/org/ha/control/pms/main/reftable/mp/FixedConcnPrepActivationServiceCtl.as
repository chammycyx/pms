package hk.org.ha.control.pms.main.reftable.mp
{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveFixedConcnStatusInfoEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveFixedConcnStatusInfoHandlerEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveFixedConcnStatusListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveFixedConcnStatusListHandlerEvent;
	import hk.org.ha.event.pms.main.reftable.mp.UpdateFixedConcnStatusListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.UpdateFixedConcnStatusListHandlerEvent;
	import hk.org.ha.model.pms.biz.reftable.mp.FixedConcnPrepActivationServiceBean;
	import hk.org.ha.model.pms.vo.reftable.mp.FixedConcnStatusInfo;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("fixedConcnPrepActivationServiceCtl", restrict="true")]
	public class FixedConcnPrepActivationServiceCtl
	{	
		[In]
		public var fixedConcnPrepActivationService:FixedConcnPrepActivationServiceBean;

		private var callBackEvent:Event;
		
		[Observer]
		public function retrieveFixedConcnStatusInfo(evt:RetrieveFixedConcnStatusInfoEvent):void
		{
			fixedConcnPrepActivationService.retrieveFixedConcnStatusInfo(evt.dmDrug, retrieveFixedConcnStatusInfoResult);
		}
		
		private function retrieveFixedConcnStatusInfoResult(evt:TideResultEvent):void
		{	
			evt.context.dispatchEvent( new RetrieveFixedConcnStatusInfoHandlerEvent(evt.result as FixedConcnStatusInfo) );
		}
		
		[Observer]
		public function retrieveFixedConcnStatusList(evt:RetrieveFixedConcnStatusListEvent):void
		{
			fixedConcnPrepActivationService.retrievePmsFixedConcnStatusList(evt.patHospCode, evt.dmDrug, retrieveFixedConcnStatusListResult);
		}
		
		private function retrieveFixedConcnStatusListResult(evt:TideResultEvent):void
		{	
			evt.context.dispatchEvent( new RetrieveFixedConcnStatusListHandlerEvent(evt.result as FixedConcnStatusInfo) );
		}
		
		[Observer]
		public function updateFixedConcnStatusList(evt:UpdateFixedConcnStatusListEvent):void
		{
			fixedConcnPrepActivationService.updateFixedConcnStatusList(evt.fixedConcnStatusList, updateFixedConcnStatusListResult);
		}
		
		private function updateFixedConcnStatusListResult(evt:TideResultEvent):void
		{	
			evt.context.dispatchEvent( new UpdateFixedConcnStatusListHandlerEvent() );
		}
	}
}