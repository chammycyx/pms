package hk.org.ha.event.pms.main.vetting.mp.tpn.popup
{	
	import org.granite.tide.events.AbstractTideEvent;

	public class ModifyTpnRecordEvent extends AbstractTideEvent
	{
		private var _duration:Number;
		private var _selectedIndex:Number;
		
		public function ModifyTpnRecordEvent(duration:Number, selectedIndex:Number)
		{
			super();
			_duration = duration;
			_selectedIndex = selectedIndex;
		}
		
		public function get duration():Number{
			return _duration;
		}
		
		public function get selectedIndex():Number{
			return _selectedIndex;
		}
	}
}