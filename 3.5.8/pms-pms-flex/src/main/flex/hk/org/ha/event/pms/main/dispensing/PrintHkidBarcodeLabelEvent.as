package hk.org.ha.event.pms.main.dispensing {
	
	import hk.org.ha.model.pms.vo.label.HkidBarcodeLabel;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class PrintHkidBarcodeLabelEvent extends AbstractTideEvent 
	{
		private var _hkidBarcodeLabel:HkidBarcodeLabel;
		
		public function PrintHkidBarcodeLabelEvent(hkidBarcodeLabel:HkidBarcodeLabel):void 
		{
			super();
			_hkidBarcodeLabel = hkidBarcodeLabel;
		}
		
		public function get hkidBarcodeLabel():HkidBarcodeLabel {
			return _hkidBarcodeLabel;
		}
	}
}