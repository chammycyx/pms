package hk.org.ha.event.pms.main.alert.mds {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class AcknowledgeOverrideReasonForOpEvent extends AbstractTideEvent {
		
		private var _successFunc:Function;
		private var _cancelFunc:Function;
		
		public function AcknowledgeOverrideReasonForOpEvent(successFunc:Function=null, cancelFunc:Function=null) {
			super();
			_successFunc = successFunc;
			_cancelFunc = cancelFunc;
		}
		
		public function constructCopy():AcknowledgeOverrideReasonForOpEvent {
			return new AcknowledgeOverrideReasonForOpEvent(this.successFunc, this.cancelFunc);
		}
		
		public function get successFunc():Function {
			return _successFunc;
		}
		
		public function get cancelFunc():Function {
			return _cancelFunc;
		}
	}
}