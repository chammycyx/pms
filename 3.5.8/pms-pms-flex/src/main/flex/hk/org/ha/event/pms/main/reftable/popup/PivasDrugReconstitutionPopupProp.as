package hk.org.ha.event.pms.main.reftable.popup
{
	import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrugManuf;
	import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrugManufMethod;
	import hk.org.ha.model.pms.vo.reftable.pivas.PivasDrugInfo;
	
	import mx.collections.ArrayCollection;

	public class PivasDrugReconstitutionPopupProp
	{
		public var createReconstitution:Boolean;
		
		public var selectedPivasDrugInfo:PivasDrugInfo;
		
		public var selectedPivasDrugManuf:PivasDrugManuf;

		public var selectedPivasDrugManufMethod:PivasDrugManufMethod;
		
		public var otherPivasDrugManufMethodList:ArrayCollection;
		
		public var okHandler:Function;
	}
}
