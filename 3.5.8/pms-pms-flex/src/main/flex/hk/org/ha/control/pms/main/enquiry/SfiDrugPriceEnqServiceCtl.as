package hk.org.ha.control.pms.main.enquiry {
	
	
	import hk.org.ha.event.pms.main.enquiry.RefreshSfiDrugPriceEvent;
	import hk.org.ha.event.pms.main.enquiry.RefreshSfiDrugPriceGenDateListEvent;
	import hk.org.ha.event.pms.main.enquiry.RefreshSfiDrugPriceHistoryEvent;
	import hk.org.ha.event.pms.main.enquiry.RetrieveSfiDrugPriceEvent;
	import hk.org.ha.event.pms.main.enquiry.RetrieveSfiDrugPriceGenDateListEvent;
	import hk.org.ha.event.pms.main.enquiry.RetrieveSfiDrugPriceHistoryEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.model.pms.biz.enquiry.SfiDrugPriceEnqServiceBean;
	import hk.org.ha.model.pms.biz.enquiry.SfiDrugPriceGenDateListServiceBean
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	

	[Bindable]
	[Name("sfiDrugPriceEnqServiceCtl", restrict="true")]
	public class SfiDrugPriceEnqServiceCtl 
	{
	
		[In]
		public var sfiDrugPriceEnqService:SfiDrugPriceEnqServiceBean;
		
		[In]
		public var sfiDrugPriceGenDateListService:SfiDrugPriceGenDateListServiceBean;
		
		private var nextColumnPosition:Number;
		private var nextRowPosition:Number;
		private var addNewRecord:Boolean;
				
		[Observer]
		public function retrieveSfiDrugPriceGenDateList(evt:RetrieveSfiDrugPriceGenDateListEvent):void{
			sfiDrugPriceGenDateListService.retrieveDrugPriceGenDateList(retrieveSfiDrugPriceGenDateListResult);			
		}
		
		private function retrieveSfiDrugPriceGenDateListResult(evt:TideResultEvent):void{
			evt.context.dispatchEvent( new CloseLoadingPopupEvent() );
			evt.context.dispatchEvent(new RefreshSfiDrugPriceGenDateListEvent());
		}
		
		private function closeLoadingPopup(evt:TideFaultEvent):void{
			evt.context.dispatchEvent( new CloseLoadingPopupEvent() );
			evt.context.dispatchEvent(new RefreshSfiDrugPriceGenDateListEvent());
		}
		
		[Observer]
		public function retrieveSfiDrugPrice(evt:RetrieveSfiDrugPriceEvent):void{
			nextColumnPosition = evt.nextColumnPosition;
			nextRowPosition = evt.nextRowPosition;
			addNewRecord = evt.addNewRecord;
			
			sfiDrugPriceEnqService.retrieveSfiDrugPrice(evt.itemCode, evt.drugPriceMonth, retrieveSfiDrugPriceResult);			
		}
		
		public function retrieveSfiDrugPriceResult(evt:TideResultEvent):void{					
			evt.context.dispatchEvent(new RefreshSfiDrugPriceEvent(nextColumnPosition, nextRowPosition, addNewRecord));
		}
		
		[Observer]
		public function retrieveSfiDrugPriceHistory(evt:RetrieveSfiDrugPriceHistoryEvent):void{
			sfiDrugPriceEnqService.retrieveSfiDrugPriceHistory(evt.itemCodeList, evt.drugPriceMonth, retrieveSfiDrugPriceHistoryResult);
		}
		
		public function retrieveSfiDrugPriceHistoryResult(evt:TideResultEvent):void{
			evt.context.dispatchEvent(new RefreshSfiDrugPriceHistoryEvent());
		}
		
	}
}

