package hk.org.ha.event.pms.main.drug.popup
{
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	import hk.org.ha.view.pms.main.drug.popup.FreeTextEntryPopup;
	
	import mx.collections.ArrayCollection;
	
	public class ShowMpFreeTextEntryPopupEvent extends AbstractTideEvent
	{
		private var _drugSearchSource:DrugSearchSource;
		private var _mpFreeTextEntryPopupProp:MpFreeTextEntryPopupProp;
		
		public function ShowMpFreeTextEntryPopupEvent(mpFreeTextEntryPopupProp:MpFreeTextEntryPopupProp, drugSearchSource:DrugSearchSource = null)
		{
			super();
			_drugSearchSource = drugSearchSource;
			_mpFreeTextEntryPopupProp = mpFreeTextEntryPopupProp;
		}

		public function get drugSearchSource():DrugSearchSource {
			return _drugSearchSource;
		}
		
		public function get mpFreeTextEntryPopupProp():MpFreeTextEntryPopupProp{
			return _mpFreeTextEntryPopupProp;
		}
	}
}