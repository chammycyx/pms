package hk.org.ha.event.pms.main.alert.druginfo {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.rx.DhRxDrug;
	
	public class RetrieveDrugMdsByDhRxDrugEvent extends AbstractTideEvent {
		
		private var _dhRxDrug:DhRxDrug;
		
		private var _checkHq:Boolean;
		
		private var _checkRenal:Boolean;
		
		private var _callback:Function;
		
		public function RetrieveDrugMdsByDhRxDrugEvent(dhRxDrug:DhRxDrug, checkHq:Boolean, checkRenal:Boolean, callback:Function) {
			super();
			_dhRxDrug = dhRxDrug;
			_callback = callback;
			_checkHq = checkHq;
			_checkRenal = checkRenal;
		}
		
		public function get dhRxDrug():DhRxDrug {
			return _dhRxDrug;
		}
		
		public function get checkHq():Boolean {
			return _checkHq;
		}
		
		public function get checkRenal():Boolean {
			return _checkRenal;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}
