package hk.org.ha.event.pms.main.reftable.cmm {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ExportCmmDrugGroupRptEvent extends AbstractTideEvent {

		private var _callback:Function;
		
		public function ExportCmmDrugGroupRptEvent(callback:Function) {
			super();
			_callback = callback;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}
