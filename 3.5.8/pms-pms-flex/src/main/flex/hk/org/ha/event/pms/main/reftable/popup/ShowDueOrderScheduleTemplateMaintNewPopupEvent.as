package hk.org.ha.event.pms.main.reftable.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDueOrderScheduleTemplateMaintNewPopupEvent extends AbstractTideEvent 
	{
		private var _dueOrderScheduleTemplateMaintNewPopupProp:DueOrderScheduleTemplateMaintNewPopupProp;
		
		public function ShowDueOrderScheduleTemplateMaintNewPopupEvent(dueOrderScheduleTemplateMaintNewPopupProp:DueOrderScheduleTemplateMaintNewPopupProp):void 
		{
			super();
			_dueOrderScheduleTemplateMaintNewPopupProp = dueOrderScheduleTemplateMaintNewPopupProp;
		}
		
		public function get dueOrderScheduleTemplateMaintNewPopupProp():DueOrderScheduleTemplateMaintNewPopupProp{
			return _dueOrderScheduleTemplateMaintNewPopupProp;
		}

	}
}