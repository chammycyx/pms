package hk.org.ha.event.pms.main.reftable.popup
{
	import mx.collections.ArrayCollection;

	public class PivasSolventLookupPopupProp
	{
		public var manufacturerCode:String; 
		
		public var selectedDmSiteList:ArrayCollection;
		
		public var solventDescPrefix:String;
		
		public var dmDrugReconstitutionList:ArrayCollection;
		
		public var pivasSolventList:ArrayCollection;
		
		public var okHandler:Function;
	}
}
