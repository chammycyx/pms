package hk.org.ha.control.pms.main.alert.mds {
	import hk.org.ha.event.pms.main.alert.mds.RetrieveAlertMsgByItemNumEvent;
	import hk.org.ha.event.pms.main.alert.mds.RetrieveAlertMsgByMedOrderItemEvent;
	import hk.org.ha.event.pms.main.alert.mds.RetrieveAlertMsgForDrugSearchEvent;
	import hk.org.ha.event.pms.main.alert.mds.RetrieveMedProfileAlertMsgEvent;
	import hk.org.ha.event.pms.main.alert.mds.RetrieveMpAlertMsgEvent;
	import hk.org.ha.event.pms.main.alert.mds.popup.ShowAlertMsgPopupEvent;
	import hk.org.ha.event.pms.main.alert.mds.popup.ShowDischargeAlertMsgPopupEvent;
	import hk.org.ha.event.pms.main.alert.mds.popup.ShowMedProfileAlertMsgPopupEvent;
	import hk.org.ha.event.pms.main.alert.mds.popup.ShowMpAlertPopupEvent;
	import hk.org.ha.model.pms.biz.alert.mds.AlertMsgServiceBean;
	import hk.org.ha.model.pms.persistence.AlertEntity;
	import hk.org.ha.model.pms.persistence.disp.MedOrder;
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	import hk.org.ha.model.pms.persistence.disp.PharmOrder;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItemAlert;
	import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;
	import hk.org.ha.model.pms.vo.alert.mds.MedProfileAlertMsg;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("alertMsgServiceCtl", restrict="true")]
	public class AlertMsgServiceCtl {

		[In]
		public var ctx:Context;

		[In]
		public var alertMsgService:AlertMsgServiceBean;
		
		[In]
		public var pharmOrder:PharmOrder;
		
		private var medOrder:MedOrder;
		private var pharmOrderItemList:ListCollectionView;
		
		private var showAlertMsgPopupEvent:ShowAlertMsgPopupEvent;
		
		private var retrieveAlertMsgByItemNumEvent:RetrieveAlertMsgByItemNumEvent;
		private var retrieveAlertMsgByMedOrderItemEvent:RetrieveAlertMsgByMedOrderItemEvent;
		private var retrieveAlertMsgForDrugSearchEvent:RetrieveAlertMsgForDrugSearchEvent;
		private var retrieveMedProfileAlertMsgEvent:RetrieveMedProfileAlertMsgEvent;
		private var retrieveMpAlertMsgEvent:RetrieveMpAlertMsgEvent;
		
		[Observer]
		public function retrieveAlertMsgByItemNum(evt:RetrieveAlertMsgByItemNumEvent):void {
			retrieveAlertMsgByItemNumEvent = evt;
			alertMsgService.retrieveAlertMsgByItemNum(evt.itemNum, retrieveAlertMsgByItemNumResult);
		}
		
		private function retrieveAlertMsgByItemNumResult(evt:TideResultEvent):void {
			
			var medProfileAlertMsg:MedProfileAlertMsg = evt.result as MedProfileAlertMsg;
			var showAlertMsgPopupEvent:ShowAlertMsgPopupEvent = retrieveAlertMsgByItemNumEvent.showAlertMsgPopupEvent;
			
			if (pharmOrder.medOrder.isMpDischarge() && showAlertMsgPopupEvent.state != "add") {
				var showDischargeAlertMsgPopupEvent:ShowDischargeAlertMsgPopupEvent = new ShowDischargeAlertMsgPopupEvent(showAlertMsgPopupEvent);
				showDischargeAlertMsgPopupEvent.medProfileAlertMsg = medProfileAlertMsg;
				dispatchEvent(showDischargeAlertMsgPopupEvent);
			}
			else {
				showAlertMsgPopupEvent.alertMsg = medProfileAlertMsg.alertMsgList.getItemAt(0) as AlertMsg;
				dispatchEvent(showAlertMsgPopupEvent);
			}
		}
		
		[Observer]
		public function retrieveAlertMsgByMedOrderItem(evt:RetrieveAlertMsgByMedOrderItemEvent):void {

			retrieveAlertMsgByMedOrderItemEvent = evt;
			var moi:MedOrderItem = evt.medOrderItem;

			pharmOrderItemList = moi.pharmOrderItemList;
			medOrder           = moi.medOrder;
			moi.medOrder           = null;
			moi.pharmOrderItemList = null;
			
			alertMsgService.retrieveAlertMsgByMedOrderItem(evt.medOrderItem, retrieveAlertMsgByMedOrderItemResult);
		}
		
		private function retrieveAlertMsgByMedOrderItemResult(evt:TideResultEvent):void {
			
			var moi:MedOrderItem = retrieveAlertMsgByMedOrderItemEvent.medOrderItem;
			moi.medOrder           = medOrder;
			moi.pharmOrderItemList = pharmOrderItemList;

			var medProfileAlertMsg:MedProfileAlertMsg = evt.result as MedProfileAlertMsg;
			var showAlertMsgPopupEvent:ShowAlertMsgPopupEvent = retrieveAlertMsgByMedOrderItemEvent.showAlertMsgPopupEvent;
			
			if (medOrder.isMpDischarge() && showAlertMsgPopupEvent.state != "add") {
				var showDischargeAlertMsgPopupEvent:ShowDischargeAlertMsgPopupEvent = new ShowDischargeAlertMsgPopupEvent(showAlertMsgPopupEvent);
				showDischargeAlertMsgPopupEvent.medProfileAlertMsg = medProfileAlertMsg;
				dispatchEvent(showDischargeAlertMsgPopupEvent);
			}
			else {
				showAlertMsgPopupEvent.alertMsg = medProfileAlertMsg.alertMsgList.getItemAt(0) as AlertMsg;
				dispatchEvent(showAlertMsgPopupEvent);
			}
		}
		
		[Observer]
		public function retrieveAlertMsgForDrugSearch(evt:RetrieveAlertMsgForDrugSearchEvent):void {
			retrieveAlertMsgForDrugSearchEvent = evt;
			alertMsgService.retrieveAlertMsgForDrugSearch(retrieveAlertMsgForDrugSearchResult);
		}
		
		private function retrieveAlertMsgForDrugSearchResult(evt:TideResultEvent):void {
			if (retrieveAlertMsgForDrugSearchEvent.showAlertMsgPopupEvent != null) {
				retrieveAlertMsgForDrugSearchEvent.showAlertMsgPopupEvent.alertMsg = evt.result as AlertMsg;
				evt.context.dispatchEvent(retrieveAlertMsgForDrugSearchEvent.showAlertMsgPopupEvent);
			}
		}
		
		[Observer]
		public function retrieveMedProfileAlertMsg(evt:RetrieveMedProfileAlertMsgEvent):void {
			retrieveMedProfileAlertMsgEvent = evt;
			for each (var alertEntity:AlertEntity in evt.medItem.alertList) {
				alertEntity.medItem = null;
			}
			alertMsgService.retrieveMedProfileAlertMsg(evt.medItem, retrieveMedProfileAlertMsgResult);
		}
		
		private function retrieveMedProfileAlertMsgResult(evt:TideResultEvent):void {

			for each (var alertEntity:AlertEntity in retrieveMedProfileAlertMsgEvent.medItem.alertList) {
				alertEntity.medItem = retrieveMedProfileAlertMsgEvent.medItem;
			}
			
			var medProfileAlertMsg:MedProfileAlertMsg = evt.result as MedProfileAlertMsg;

			var showMedProfileAlertMsgPopupEvent:ShowMedProfileAlertMsgPopupEvent;
			if (retrieveMedProfileAlertMsgEvent.showMedProfileAlertMsgPopupEvent == null) {
				showMedProfileAlertMsgPopupEvent = new ShowMedProfileAlertMsgPopupEvent("enquire", new ArrayCollection(new Array(medProfileAlertMsg)));
			}
			else {
				showMedProfileAlertMsgPopupEvent = retrieveMedProfileAlertMsgEvent.showMedProfileAlertMsgPopupEvent;
				showMedProfileAlertMsgPopupEvent.medProfileAlertMsg = medProfileAlertMsg;
			}

			evt.context.dispatchEvent(showMedProfileAlertMsgPopupEvent);
		}
		
		[Observer]
		public function retrieveMpAlertMsg(evt:RetrieveMpAlertMsgEvent):void {
			retrieveMpAlertMsgEvent = evt;
			alertMsgService.retrieveMpAlertMsg(evt.medProfileMoItem, retrieveMpAlertMsgResult);
		}
		
		private function retrieveMpAlertMsgResult(evt:TideResultEvent):void {
			var alertMsg:AlertMsg = evt.result as AlertMsg;
			var showMpAlertPopupEvent:ShowMpAlertPopupEvent = new ShowMpAlertPopupEvent(alertMsg, retrieveMpAlertMsgEvent.medProfileMoItem.medProfileMoItemAlertList);
			showMpAlertPopupEvent.allowEditFlag = retrieveMpAlertMsgEvent.allowEditFlag;
			dispatchEvent(showMpAlertPopupEvent);
		}
	}
}
