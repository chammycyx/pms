package hk.org.ha.event.pms.main.delivery {
	
	import hk.org.ha.model.pms.udt.printing.PrintLang;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ReprintMpDispLabelEvent extends AbstractTideEvent 
	{
		private var _mpDispLabels:ArrayCollection;
		private var _resendAtdpsMessage:Boolean;
		
		public function ReprintMpDispLabelEvent(mpDispLabels:ArrayCollection, resendAtdpsMessage:Boolean=false):void 
		{
			super();
			_mpDispLabels = mpDispLabels;
			_resendAtdpsMessage = resendAtdpsMessage;
		}
		
		public function get mpDispLabels():ArrayCollection{
			return _mpDispLabels;
		} 
		
		public function get resendAtdpsMessage():Boolean{
			return _resendAtdpsMessage;
		} 
	}
}