package hk.org.ha.event.pms.security {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	import mx.core.UIComponent;
	
	public class CloseAndLogoffEvent extends AbstractTideEvent {
		
		private var _caller:UIComponent;
		
		private var _waitFlag:Boolean;
		
		public function CloseAndLogoffEvent(caller:UIComponent, waitFlag:Boolean=true) {
			super();
			_caller = caller;
			_waitFlag = waitFlag;
		}
		
		public function get caller():UIComponent {
			return _caller;
		}
		
		public function get waitFlag():Boolean {
			return _waitFlag;
		}
	}
}