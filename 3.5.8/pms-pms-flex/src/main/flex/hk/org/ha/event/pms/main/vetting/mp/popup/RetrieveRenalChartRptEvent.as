package hk.org.ha.event.pms.main.vetting.mp.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	import mx.collections.ListCollectionView;
	
	public class RetrieveRenalChartRptEvent extends AbstractTideEvent {
		
		private var _mpRenalEcrclLogList:ListCollectionView;
		
		private var _highlightLastRecord:Boolean;
		
		private var _callback:Function;
		
		public function RetrieveRenalChartRptEvent(mpRenalEcrclLogList:ListCollectionView, highlightLastRecord:Boolean, callback:Function) {
			super();
			_mpRenalEcrclLogList = mpRenalEcrclLogList;
			_highlightLastRecord = highlightLastRecord;
			_callback = callback;
		}
		
		public function get mpRenalEcrclLogList():ListCollectionView {
			return _mpRenalEcrclLogList;
		}
		
		public function get highlightLastRecord():Boolean {
			return _highlightLastRecord;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}
