package hk.org.ha.control.pms.main.checkissue{
	
	import hk.org.ha.event.pms.main.checkissue.RefreshOutstandingMoeOrderCountEvent;
	import hk.org.ha.event.pms.main.checkissue.RetrieveIssueWindowNumEvent;
	import hk.org.ha.event.pms.main.checkissue.UpdateCheckWorkstationEvent;
	import hk.org.ha.event.pms.main.checkissue.UpdateRefreshValueEvent;
	import hk.org.ha.model.pms.biz.checkissue.CheckIssueAidServiceBean;
	
	[Bindable]
	[Name("checkIssueAidServiceCtl", restrict="true")]
	public class CheckIssueAidServiceCtl 
	{
		[In]
		public var checkIssueAidService:CheckIssueAidServiceBean;
		
		[Observer]
		public function retrieveIssueWindowNum(evt:RetrieveIssueWindowNumEvent):void
		{
			checkIssueAidService.retrieveIssueWindowNum();
		}
		
		[Observer]
		public function updateCheckWorkstation(evt:UpdateCheckWorkstationEvent):void
		{
			checkIssueAidService.updateCheckWorkstationRuleCode();
		}
		
		[Observer]
		public function updateRefreshValue(evt:UpdateRefreshValueEvent):void
		{
			checkIssueAidService.updateRefresh(evt.refresh);
		}
		
		[Observer]
		public function refreshOutstandingMoeOrderCount(evt:RefreshOutstandingMoeOrderCountEvent):void
		{
			checkIssueAidService.retrieveOutstandingMoeOrderCount();
		}

	}
}
