package hk.org.ha.event.pms.main.drug {
	
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class AddBnfNodeEvent extends AbstractTideEvent 
	{			
		private var _drugSearchSource:DrugSearchSource;
		private var _drugSearchBnfNodeMapList:ArrayCollection;
		
		public function AddBnfNodeEvent(drugSearchSource:DrugSearchSource, drugSearchBnfNodeMapList:ArrayCollection):void 
		{
			super();
			_drugSearchSource = drugSearchSource;
			_drugSearchBnfNodeMapList = drugSearchBnfNodeMapList;
		}
		
		public function get drugSearchSource():DrugSearchSource {
			return _drugSearchSource;
		}
		
		public function get drugSearchBnfNodeMapList():ArrayCollection {
			return _drugSearchBnfNodeMapList;
		}
	}
}