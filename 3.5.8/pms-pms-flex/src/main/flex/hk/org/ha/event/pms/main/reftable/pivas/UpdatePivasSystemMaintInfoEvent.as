package hk.org.ha.event.pms.main.reftable.pivas {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.reftable.pivas.PivasSystemMaintUpdateSummary;
	
	public class UpdatePivasSystemMaintInfoEvent extends AbstractTideEvent {
		
		private var _pivasSystemMaintUpdateSummary:PivasSystemMaintUpdateSummary;
		
		public function UpdatePivasSystemMaintInfoEvent(pivasSystemMaintUpdateSummary:PivasSystemMaintUpdateSummary) {
			super();
			_pivasSystemMaintUpdateSummary = pivasSystemMaintUpdateSummary;
		}
		
		public function get pivasSystemMaintUpdateSummary():PivasSystemMaintUpdateSummary {
			return _pivasSystemMaintUpdateSummary;
		}
	}
}