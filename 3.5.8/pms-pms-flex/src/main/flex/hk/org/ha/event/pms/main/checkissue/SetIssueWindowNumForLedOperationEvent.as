package hk.org.ha.event.pms.main.checkissue
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetIssueWindowNumForLedOperationEvent extends AbstractTideEvent 
	{		
		private var _issueWindowNum:Number;

		public function SetIssueWindowNumForLedOperationEvent(issueWindowNum:Number):void 
		{
			super();
			_issueWindowNum = issueWindowNum;
		}
		
		public function get issueWindowNum():int {
			return _issueWindowNum;
		}

	}
}