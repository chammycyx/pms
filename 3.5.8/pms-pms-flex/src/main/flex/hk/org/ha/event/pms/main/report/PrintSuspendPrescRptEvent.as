package hk.org.ha.event.pms.main.report {
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class PrintSuspendPrescRptEvent extends AbstractTideEvent 
	{
		private var _edsStatusDetailRptSuspendItemList:ArrayCollection;
		
		public function PrintSuspendPrescRptEvent(edsStatusDetailRptSuspendItemList:ArrayCollection):void 
		{
			super();
			_edsStatusDetailRptSuspendItemList = edsStatusDetailRptSuspendItemList;
		}
		
		public function get edsStatusDetailRptSuspendItemList():ArrayCollection {
			return _edsStatusDetailRptSuspendItemList;
		}
	}
}