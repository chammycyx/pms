package hk.org.ha.view.pms.main.reftable.mp {
	
	import mx.controls.treeClasses.TreeItemRenderer;
	import mx.controls.treeClasses.TreeListData;
	import hk.org.ha.model.pms.dms.persistence.PmsFixedConcnStatus;
	
	public class FixedConcnPrepTreeItemRenderer extends TreeItemRenderer
	{
		public function FixedConcnPrepTreeItemRenderer() 
		{
			super();
			mouseEnabled = false;			
		}
		
		override public function set data(value:Object):void
		{
			if(value != null)
			{ 
				super.data = value;
				setStyle("fontWeight", 'bold');
				setStyle("fontSize", '16');
				setStyle("color", '#000000');
				
				if( value.prep != null ){
					var prep:PmsFixedConcnStatus = value.prep as PmsFixedConcnStatus;
					if( prep.localStatus == 'I' || 
						( ( prep.localStatus == null || prep.localStatus == '' ) && prep.defaultStatus == 'I' ) ){
						setStyle("color", '#999999');
					}
				}
			}
		}	 

		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void {
			super.updateDisplayList(unscaledWidth, unscaledHeight);
		}
	}
}