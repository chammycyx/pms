package hk.org.ha.event.pms.main.enquiry
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveFmIndicatorEvent extends AbstractTideEvent 
	{		
		private var _itemCode:String;
		private var _pmsFmStatus:String;
		
		public function RetrieveFmIndicatorEvent( itemCode:String, pmsFmStatus:String ):void 
		{
			super();
			_itemCode = itemCode;
			_pmsFmStatus = pmsFmStatus;
		}
		
		public function get itemCode():String
		{
			return _itemCode;
		}
		
		public function get pmsFmStatus():String
		{
			return _pmsFmStatus;
		}
	}
}