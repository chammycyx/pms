package hk.org.ha.event.pms.main.reftable.pivas {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.reftable.pivas.PivasSystemMaintInfo;
	
	public class ShowPivasSystemMaintSaveSuccessMsgEvent extends AbstractTideEvent {
		
		private var _pivasSystemMaintInfo:PivasSystemMaintInfo;
		
		public function ShowPivasSystemMaintSaveSuccessMsgEvent(pivasSystemMaintInfo:PivasSystemMaintInfo) {
			super();
			_pivasSystemMaintInfo = pivasSystemMaintInfo;
		}
		
		public function get pivasSystemMaintInfo():PivasSystemMaintInfo {
			return _pivasSystemMaintInfo;
		}
	}
}