package hk.org.ha.event.pms.main.reftable {
	
	import hk.org.ha.model.pms.persistence.reftable.RefillQtyAdj;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetRefillQtyAdjDmFormEvent extends AbstractTideEvent 
	{
		private var _refillQtyAdj:RefillQtyAdj;
		private var _addNewRecord:Boolean;
		private var _callSave:Boolean;
		
		public function SetRefillQtyAdjDmFormEvent(refillQtyAdj:RefillQtyAdj, addNewRecord:Boolean, callSave:Boolean):void 
		{
			super();
			_addNewRecord = addNewRecord;
			_refillQtyAdj = refillQtyAdj;
			_callSave = callSave;
		}
		
		public function get addNewRecord():Boolean {
			return _addNewRecord;
		}
		
		public function get refillQtyAdj():RefillQtyAdj {
			return _refillQtyAdj;
		}
		
		public function get callSave():Boolean {
			return _callSave;
		}
	
	}
}