package hk.org.ha.event.pms.main.report {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveEdsStatusSummaryRptEvent extends AbstractTideEvent 
	{
		private var _selectedTab:Number;
		private var _date:Date;
		
		public function RetrieveEdsStatusSummaryRptEvent( selectedTab:Number, date:Date):void 
		{
			super();
			_selectedTab = selectedTab;
			_date = date;
		}
		
		public function get selectedTab():Number{
			return _selectedTab;
		} 
		
		public function get date():Date{
			return _date;
		} 

	}
}