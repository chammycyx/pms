package hk.org.ha.event.pms.main.enquiry {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	import mx.collections.ArrayCollection;
	
	public class RefreshDeliveryRequestInfoListEvent extends AbstractTideEvent {
		
		private var _infoList:ArrayCollection;
		
		public function RefreshDeliveryRequestInfoListEvent(infoList:ArrayCollection) {
			super();
			_infoList = infoList;
		}
		
		public function get infoList():ArrayCollection {
			return _infoList;
		}
	}
}