package hk.org.ha.event.pms.main.vetting
{
	import flash.events.Event;
	
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class RetrievePharmRemarkItemEvent extends AbstractTideEvent 
	{
		private var _updateRemarkSource:String = null;		// source view, "vetting" or "orderEdit"		
		private var _medOrderItem:MedOrderItem = null;						
		private var _okCallbackFunc:Function = null;
		private var _cancelCallbackEvent:Event = null;		
		private var _reverseRemarkCallbackFunc:Function = null;	// only used for item delete case which triggered from vetting screen 		
		
		public function RetrievePharmRemarkItemEvent():void
		{
			super();
		}
		
		public function get updateRemarkSource():String {
			return _updateRemarkSource;
		}
		
		public function set updateRemarkSource(updateRemarkSource:String):void {
			_updateRemarkSource = updateRemarkSource;
		}
		
		public function get medOrderItem():MedOrderItem {
			return _medOrderItem;
		}
		
		public function set medOrderItem(medOrderItem:MedOrderItem):void {
			_medOrderItem = medOrderItem;
		}

		public function get okCallbackFunc():Function {
			return _okCallbackFunc;
		}
		
		public function set okCallbackFunc(okCallbackFunc:Function):void {
			_okCallbackFunc = okCallbackFunc;
		}	
		
		public function get cancelCallbackEvent():Event {
			return _cancelCallbackEvent;
		}
		
		public function set cancelCallbackEvent(cancelCallbackEvent:Event):void {
			_cancelCallbackEvent = cancelCallbackEvent;
		}

		public function get reverseRemarkCallbackFunc():Function {
			return _reverseRemarkCallbackFunc;
		}
		
		public function set reverseRemarkCallbackFunc(reverseRemarkCallbackFunc:Function):void {
			_reverseRemarkCallbackFunc = reverseRemarkCallbackFunc;
		}
	}
}