package hk.org.ha.event.pms.main.drug {
	
	import hk.org.ha.model.pms.dms.vo.RelatedDrugSearchCriteria;
	import hk.org.ha.model.pms.dms.vo.RouteFormSearchCriteria;
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SearchDrugEvent extends AbstractTideEvent 
	{			
		private var _drugSearchSource:DrugSearchSource;
		private var _relatedDrugSearchCriteria:RelatedDrugSearchCriteria;
		private var _routeFormSearchCriteria:RouteFormSearchCriteria;
		
		public function SearchDrugEvent(drugSearchSource:DrugSearchSource, relatedDrugSearchCriteria:RelatedDrugSearchCriteria, routeFormSearchCriteria:RouteFormSearchCriteria):void 
		{
			_drugSearchSource = drugSearchSource;
			_relatedDrugSearchCriteria = relatedDrugSearchCriteria;
			_routeFormSearchCriteria = routeFormSearchCriteria;
			super();
		}

		public function get drugSearchSource():DrugSearchSource
		{
			return _drugSearchSource;
		}
		
		public function get relatedDrugSearchCriteria():RelatedDrugSearchCriteria
		{
			return _relatedDrugSearchCriteria;
		}
		
		public function get routeFormSearchCriteria():RouteFormSearchCriteria
		{
			return _routeFormSearchCriteria;
		}
	}
}