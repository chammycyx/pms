package hk.org.ha.event.pms.main.checkissue.mp
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class AssignBatchEvent extends AbstractTideEvent 
	{			
		private var _wardCode:String;
		private var _assignBatchItemList:ArrayCollection;
		private var _printTrxRptFlag:Boolean;
		private var _checkFlag:Boolean;
		private var _assignBatch:Boolean;
		private var _batchDate:Date;
		private var _batchNum:String;
		private var _callByUnlinkItemList:Boolean;
		
		public function AssignBatchEvent(wardCode:String, assignBatchItemList:ArrayCollection, printTrxRptFlag:Boolean, 
										 checkFlag:Boolean, assignBatch :Boolean, batchDate: Date, 
										 batchNum:String=null, callByUnlinkItemList:Boolean=false):void 
		{
			super();
			_batchDate = batchDate;
			_wardCode = wardCode;
			_assignBatchItemList = assignBatchItemList;
			_printTrxRptFlag = printTrxRptFlag;
			_checkFlag = checkFlag;
			_assignBatch = assignBatch;
			_batchNum = batchNum;
			_callByUnlinkItemList = callByUnlinkItemList;
		}
		
		public function get wardCode():String {
			return _wardCode;
		}
		
		public function get assignBatchItemList():ArrayCollection {
			return _assignBatchItemList;
		}
		
		public function get printTrxRptFlag():Boolean {
			return _printTrxRptFlag;
		}
		
		public function get checkFlag():Boolean {
			return _checkFlag;
		}
		
		public function get assignBatch():Boolean {
			return _assignBatch;
		}
		public function get batchDate():Date {
			return _batchDate;
		}
		
		public function get batchNum():String {
			return _batchNum;
		}
		
		public function get callByUnlinkItemList():Boolean {
			return _callByUnlinkItemList;
		}
	}
}