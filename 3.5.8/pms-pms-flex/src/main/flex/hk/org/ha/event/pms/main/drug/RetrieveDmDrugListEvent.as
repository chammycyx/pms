package hk.org.ha.event.pms.main.drug {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDmDrugListEvent extends AbstractTideEvent 
	{
		private var _itemCode:String;
		private var _showNonSuspendItemOnly:Boolean;
		
		public function RetrieveDmDrugListEvent(itemCode:String, showNonSuspendItemOnly:Boolean):void 
		{
			super();
			_itemCode = itemCode;
			_showNonSuspendItemOnly = showNonSuspendItemOnly;
		}
		
		public function get itemCode():String
		{
			return _itemCode;
		}
		
		public function get showNonSuspendItemOnly():Boolean
		{
			return _showNonSuspendItemOnly;
		}
	}
}