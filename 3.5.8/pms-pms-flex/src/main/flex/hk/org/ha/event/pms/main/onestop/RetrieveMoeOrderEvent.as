package hk.org.ha.event.pms.main.onestop {
		
	import hk.org.ha.model.pms.udt.onestop.MoeFilterOption;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveMoeOrderEvent extends AbstractTideEvent 
	{			
		private var _ordDate:Date;
		private var _moeFilterOption:MoeFilterOption;
		private var _prescriptionSortCriteriaList:ArrayCollection;
		private var _showPopup:Boolean;
		private var _callbackFunction:Function;
		
		public function RetrieveMoeOrderEvent(ordDate:Date, moeFilterOption:MoeFilterOption, prescriptionSortCriteriaList:ArrayCollection, 
											  showPopup:Boolean, callbackFunction:Function=null):void 
		{
			super();
			_ordDate = ordDate;
			_moeFilterOption = moeFilterOption;
			_prescriptionSortCriteriaList = prescriptionSortCriteriaList;
			_showPopup = showPopup;
			_callbackFunction = callbackFunction;
		}
		
		public function get ordDate():Date
		{
			return _ordDate;
		}
		
		public function get moeFilterOption():MoeFilterOption
		{
			return _moeFilterOption;
		}
		
		public function get prescriptionSortCriteriaList():ArrayCollection
		{
			return _prescriptionSortCriteriaList;
		}
		
		public function get showPopup():Boolean
		{
			return _showPopup;
		}
		
		public function get callbackFunction():Function
		{
			return _callbackFunction;
		}
	}
}