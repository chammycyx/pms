package hk.org.ha.event.pms.main.reftable {
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdatePmsRouteFormSortSpecListEvent extends AbstractTideEvent 
	{
		private var _newPmsRouteFormSortSpecList:ListCollectionView;
		
		public function UpdatePmsRouteFormSortSpecListEvent(newPmsRouteFormSortSpecList:ListCollectionView):void 
		{
			super();
			_newPmsRouteFormSortSpecList = newPmsRouteFormSortSpecList;
		}
		
		public function get newPmsRouteFormSortSpecList():ListCollectionView
		{
			return _newPmsRouteFormSortSpecList;
		}
	}
}