package hk.org.ha.event.pms.main.reftable.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowRefillModuleMaintDrsPopupEvent extends AbstractTideEvent {
		private var _refillModuleMaintDrsPopupProp:RefillModuleMaintDrsPopupProp;
		
		public function ShowRefillModuleMaintDrsPopupEvent(refillModuleMaintDrsPopupProp:RefillModuleMaintDrsPopupProp):void {
			super();
			_refillModuleMaintDrsPopupProp = refillModuleMaintDrsPopupProp;
		}
		
		public function get refillModuleMaintDrsPopupProp():RefillModuleMaintDrsPopupProp {
			return _refillModuleMaintDrsPopupProp;
		}
	}
}