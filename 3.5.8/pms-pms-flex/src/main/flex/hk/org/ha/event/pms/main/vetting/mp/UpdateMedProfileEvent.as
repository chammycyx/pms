package hk.org.ha.event.pms.main.vetting.mp
{
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateMedProfileEvent extends AbstractTideEvent
	{
		private var _medProfile:MedProfile;
		private var _medProfileItemList:ArrayCollection;
		private var _replenishmentList:ArrayCollection;
		private var _purchaseRequestList:ArrayCollection;
		private var _faultCallBack:Function;
		
		public function UpdateMedProfileEvent(medProfile:MedProfile, medProfileItemList:ArrayCollection,
											  replenishmentList:ArrayCollection, 
											  purchaseRequestList:ArrayCollection = null,
											  faultCallBack:Function = null)
		{
			super();
			_medProfile = medProfile;
			_medProfileItemList = medProfileItemList;
			_replenishmentList = replenishmentList;
			_purchaseRequestList = purchaseRequestList; 
			_faultCallBack = faultCallBack;
		}
		
		public function get medProfile():MedProfile
		{
			return _medProfile;
		}
		
		public function get medProfileItemList():ArrayCollection
		{
			return _medProfileItemList;
		}
		
		public function get replenishmentList():ArrayCollection
		{
			return _replenishmentList;
		}
		
		public function get purchaseRequestList():ArrayCollection
		{
			return _purchaseRequestList;
		}
		
		public function get faultCallBack():Function
		{
			return _faultCallBack;
		}
	}
}