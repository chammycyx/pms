package hk.org.ha.control.pms.main.picking{
	
	import hk.org.ha.event.pms.main.picking.RetrieveDrugPickWorkstationListEvent;
	import hk.org.ha.event.pms.main.picking.SetDrugPickWorkstationListEvent;
	import hk.org.ha.model.pms.biz.picking.DrugPickListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("drugPickListServiceCtl", restrict="true")]
	public class DrugPickListServiceCtl 
	{
		[In]
		public var drugPickListService:DrugPickListServiceBean;
		
		private var resultMsg:String;
		
		[Observer]
		public function retrieveDrugPickWorkstationList(evt:RetrieveDrugPickWorkstationListEvent):void
		{
			drugPickListService.retrieveWorkstationList(retrieveDrugPickWorkstationListResult);
		}
		
		private function retrieveDrugPickWorkstationListResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new SetDrugPickWorkstationListEvent(evt.result as ArrayCollection));
		}
	}
}
