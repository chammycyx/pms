package hk.org.ha.event.pms.main.pivas
{
	import hk.org.ha.model.pms.vo.pivas.worklist.CalculatePivasInfoCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RecalculatePivasInfoEvent extends AbstractTideEvent 
	{
		private var _calculatePivasInfoCriteria:CalculatePivasInfoCriteria;
		
		private var _callbackFunc:Function;
		
		public function RecalculatePivasInfoEvent(calculatePivasInfoCriteria:CalculatePivasInfoCriteria, callbackFunc:Function):void 
		{
			super();
			_calculatePivasInfoCriteria = calculatePivasInfoCriteria;
			_callbackFunc = callbackFunc;
		}

		public function get calculatePivasInfoCriteria():CalculatePivasInfoCriteria {
			return _calculatePivasInfoCriteria;
		}

		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}