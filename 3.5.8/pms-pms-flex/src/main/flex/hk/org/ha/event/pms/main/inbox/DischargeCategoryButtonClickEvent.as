package hk.org.ha.event.pms.main.inbox
{
	import hk.org.ha.model.pms.udt.medprofile.DischargeMedOrderListType;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class DischargeCategoryButtonClickEvent extends AbstractTideEvent
	{
		private var _listType:DischargeMedOrderListType;
		
		public function DischargeCategoryButtonClickEvent(listType:DischargeMedOrderListType)
		{
			super();
			_listType = listType;
		}
		
		public function get listType():DischargeMedOrderListType
		{
			return _listType;
		}
	}
}