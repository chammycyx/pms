package hk.org.ha.event.pms.main.vetting.show {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowOrderEditCapdItemViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		private var _readOnlyFlag:Boolean;
		
		private var _moSelectedIndex:Number;
		
		private var _skipPharmOrderItemListCheck:Boolean;

		private var _infoString:String = null;
		
		public function ShowOrderEditCapdItemViewEvent(moSelectedIndex:Number, 
													   skipPharmOrderItemListCheck:Boolean, 
													   readOnlyFlag:Boolean=false, 
													   clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
			_readOnlyFlag = readOnlyFlag;
			_moSelectedIndex = moSelectedIndex;
			_skipPharmOrderItemListCheck = skipPharmOrderItemListCheck;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
		
		public function get readOnlyFlag():Boolean {
			return _readOnlyFlag;
		}
		
		public function get moSelectedIndex():Number {
			return _moSelectedIndex;
		}
		
		public function get skipPharmOrderItemListCheck():Boolean {
			return _skipPharmOrderItemListCheck;
		}
		
		public function set skipPharmOrderItemListCheck(skipPharmOrderItemListCheck:Boolean):void {
			_skipPharmOrderItemListCheck = skipPharmOrderItemListCheck;
		}

		public function get infoString():String {
			return _infoString;
		}
		
		public function set infoString(infoString:String):void {
			_infoString = infoString;
		}
		
		public override function clone():Event {
			return new ShowOrderEditCapdItemViewEvent(moSelectedIndex, true, readOnlyFlag, clearMessages);
		}
	}
}