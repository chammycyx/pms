package hk.org.ha.control.pms.main.report {
	
	import hk.org.ha.event.pms.main.report.PrintEdsWaitTimeAuditRptEvent;
	import hk.org.ha.event.pms.main.report.RetrieveAvailableMonthForMonthlyWaitTimeAuditRptEvent;
	import hk.org.ha.event.pms.main.report.RetrieveEdsCurrentAvgWaitTimeEvent;
	import hk.org.ha.event.pms.main.report.RetrieveEdsWaitTimeAuditRptEvent;
	import hk.org.ha.model.pms.biz.report.EdsWaitTimeAuditRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("edsWaitTimeAuditRptServiceCtl", restrict="true")]
	public class EdsWaitTimeAuditRptServiceCtl 
	{
		[In]
		public var edsWaitTimeAuditRptService:EdsWaitTimeAuditRptServiceBean;
		
		[Observer]
		public function retrieveEdsWaitTimeAuditRpt(evt:RetrieveEdsWaitTimeAuditRptEvent):void
		{
			var resultFunc:Function = function(resultEvent:TideResultEvent):void {
				if (evt.callBack != null) {
					evt.callBack(resultEvent.result);
				}
			}
			
			if (evt.isDailyReport) {
				edsWaitTimeAuditRptService.retrieveDailyWaitTimeAuditRpt(
					evt.retrieveDate, 
					resultFunc
				);
			} else if (evt.isMonthlyReport) {
				edsWaitTimeAuditRptService.retrieveMonthlyWaitTimeAuditRpt(
					evt.retrieveDate, 
					resultFunc
				);
			}
		}		
		
		[Observer]
		public function retrieveAvailableMonthForMonthlyWaitTimeAuditRpt(evt:RetrieveAvailableMonthForMonthlyWaitTimeAuditRptEvent):void {
			var resultFunc:Function = function(resultEvent:TideResultEvent):void {
				if (evt.callBack != null) {
					evt.callBack(resultEvent.result);
				}
			}
			edsWaitTimeAuditRptService.retrieveAvailableMonthForMonthlyWaitTimeAuditRpt(resultFunc);
		}
		
		[Observer]
		public function retrieveEdsCurrentAvgWaitTime(evt:RetrieveEdsCurrentAvgWaitTimeEvent):void {
			var resultFunc:Function = function(resultEvent:TideResultEvent):void {
				if (evt.callBack != null) {
					evt.callBack(resultEvent.result);
				}
			}
			edsWaitTimeAuditRptService.retrieveCurrentAvgWaitTime(resultFunc);
		}
		
		[Observer]
		public function printEdsWaitTimeAuditRpt(evt:PrintEdsWaitTimeAuditRptEvent):void {
			if (evt.isDailyReport) {
				edsWaitTimeAuditRptService.printEdsDailyWaitTimeAuditRpt();
			}
			if (evt.isMonthlyReport) {
				edsWaitTimeAuditRptService.printEdsMonthlyWaitTimeAuditRpt();
			}
		}
		
	}
}
