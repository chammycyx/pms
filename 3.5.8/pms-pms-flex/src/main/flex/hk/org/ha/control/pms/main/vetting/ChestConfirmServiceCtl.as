package hk.org.ha.control.pms.main.vetting {

	import hk.org.ha.event.pms.main.vetting.ClearChestEvent;
	import hk.org.ha.event.pms.main.vetting.RetrieveOrderEditChestEvent;
	import hk.org.ha.event.pms.main.vetting.RetrieveOrderEditChestListEvent;
	import hk.org.ha.event.pms.main.vetting.SetOrderEditChestEvent;
	import hk.org.ha.event.pms.main.vetting.UpdatePharmOrderFromChestEvent;
	import hk.org.ha.event.pms.main.vetting.show.ShowOrderViewEvent;
	import hk.org.ha.model.pms.biz.vetting.ChestConfirmServiceBean;
	import hk.org.ha.model.pms.persistence.disp.MedOrder;
	import hk.org.ha.model.pms.persistence.disp.PharmOrder;
	import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("chestConfirmServiceCtl", restrict="true")]
	public class ChestConfirmServiceCtl {

		[In]
		public var chestConfirmService:ChestConfirmServiceBean
		
		[In]
		public var ctx:Context;
		
		[In]
		public var medOrder:MedOrder;
		
		private var autoSave:Boolean;
		
		[In]
		public var pharmOrder:PharmOrder;
		
		[Observer]
		public function updatePharmOrderFromChest(evt:UpdatePharmOrderFromChestEvent):void {
			var po:PharmOrder = evt.pharmOrder;
			this.disconnectPharmOrder(po);
			this.autoSave = evt.autoSave;
			chestConfirmService.updatePharmOrderFromChest(po, updatePharmOrderFromChestResult);
		}
		
		private function disconnectPharmOrder(pharmOrder:PharmOrder):void {
			pharmOrder.medOrder = null;
			pharmOrder.refillScheduleList = null;
			pharmOrder.dispOrderList = null;
			pharmOrder.medCase = null;
			
			for each ( var pharmOrderItem:PharmOrderItem in pharmOrder.pharmOrderItemList ) {
				pharmOrderItem.medOrderItem = null;
				pharmOrderItem.refillScheduleItemList = null;
				pharmOrderItem.dispOrderItemList = null;
			}
		}
		
		private function updatePharmOrderFromChestResult(evt:TideResultEvent):void {
			var showOrderViewEvent:ShowOrderViewEvent = new ShowOrderViewEvent();
			showOrderViewEvent.saveOrderFlag = autoSave;
			evt.context.dispatchEvent( showOrderViewEvent );
		}
		
		private var retrieveOrderEditChestListEvent:RetrieveOrderEditChestListEvent;
		
		[Observer]
		public function retrieveOrderEditChestList(evt:RetrieveOrderEditChestListEvent):void {
			retrieveOrderEditChestListEvent = evt;
			chestConfirmService.retrieveChestList(retrieveOrderEditChestListResult);
		}
		
		private function retrieveOrderEditChestListResult(evt:TideResultEvent):void {
			pharmOrder.maxItemNum = evt.result as int;
			retrieveOrderEditChestListEvent.callback();
		}
		
		[Observer]
		public function retrieveOrderEditChest(evt:RetrieveOrderEditChestEvent):void {
			chestConfirmService.retrieveChest(evt.unitDoseName, retrieveOrderEditChestResult);
		}
		
		private function retrieveOrderEditChestResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent( new SetOrderEditChestEvent() );
		}
		
		[Observer]
		public function clearChest(evt:ClearChestEvent):void {
			chestConfirmService.clearChest();
		}
	}
}
