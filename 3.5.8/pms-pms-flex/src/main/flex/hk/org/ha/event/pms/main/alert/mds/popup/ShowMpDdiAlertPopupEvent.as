package hk.org.ha.event.pms.main.alert.mds.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.AlertEntity;
	import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;
	
	public class ShowMpDdiAlertPopupEvent extends AbstractTideEvent {
		
		public var alertMsg:AlertMsg;
		
		public var alertEntity:AlertEntity;

		public var state:String;
		
		public var doNotPrescribeCallback:Function = function():void {};
		
		public var prescribeACallback:Function = function():void {};
		
		public var prescribeBCallback:Function = function():void {};
		
		public var overrideCallback:Function = function():void {};
		
		public var acceptCallback:Function = function():void {};
		
		public var deferCallback:Function = function():void {};
		
		public var closeCallback:Function = function():void {};

		public function ShowMpDdiAlertPopupEvent(alertMsg:AlertMsg, alertEntity:AlertEntity, state:String="manual") {
			super();
			this.alertMsg = alertMsg;
			this.alertEntity = alertEntity;
			this.state = state;
		}
	}
}