package hk.org.ha.event.pms.main.alert.mds {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
	
	public class RetrieveMpAlertMsgEvent extends AbstractTideEvent {
		
		private var _medProfileMoItem:MedProfileMoItem;
		
		private var _allowEditFlag:Boolean;
		
		public function RetrieveMpAlertMsgEvent(medProfileMoItem:MedProfileMoItem, allowEditFlag:Boolean=true) {
			super();
			_medProfileMoItem = medProfileMoItem;
			_allowEditFlag = allowEditFlag;
		}
		
		public function get medProfileMoItem():MedProfileMoItem {
			return _medProfileMoItem;
		}
		
		public function get allowEditFlag():Boolean {
			return _allowEditFlag;
		}
	}
}