package hk.org.ha.event.pms.main.drug {
	
	import hk.org.ha.model.pms.dms.vo.BnfCriteria;
	import hk.org.ha.model.pms.dms.vo.RelatedDrugSearchCriteria;
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveBnfTreeDetailEvent extends AbstractTideEvent 
	{			
		private var _drugSearchSource:DrugSearchSource;
		private var _bnfCriteria:BnfCriteria;

		public function RetrieveBnfTreeDetailEvent(drugSearchSource:DrugSearchSource, bnfCriteria:BnfCriteria):void 
		{
			_drugSearchSource = drugSearchSource;
			_bnfCriteria = bnfCriteria;
			super();
		}
		
		public function get drugSearchSource():DrugSearchSource
		{
			return _drugSearchSource;
		}
		
		public function get bnfCriteria():BnfCriteria
		{
			return _bnfCriteria;
		}
	}
}