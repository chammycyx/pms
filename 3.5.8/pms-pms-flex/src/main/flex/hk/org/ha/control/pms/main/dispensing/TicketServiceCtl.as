package hk.org.ha.control.pms.main.dispensing {
	
	import hk.org.ha.event.pms.main.dispensing.RefreshTicketEvent;
	import hk.org.ha.event.pms.main.dispensing.RetrieveBatchTicketEvent;
	import hk.org.ha.event.pms.main.dispensing.RetrieveTicketByOrderNumCaseNumEvent;
	import hk.org.ha.event.pms.main.dispensing.RetrieveTicketByReprintTicketNumEvent;
	import hk.org.ha.event.pms.main.dispensing.RetrieveTicketEvent;
	import hk.org.ha.event.pms.main.dispensing.show.ShowTicketGenerationViewEvent;
	import hk.org.ha.model.pms.biz.ticketgen.TicketServiceBean;
	import hk.org.ha.model.pms.persistence.disp.Ticket;
	import hk.org.ha.model.pms.vo.ticketgen.TicketGenResult;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("ticketServiceCtl", restrict="true")]
	public class TicketServiceCtl 
	{
		[In]
		public var ticketService:TicketServiceBean;
				
		[In]
		public var ticketGenResult:TicketGenResult;
				
		[Observer]
		public function retrieveTicket(evt:RetrieveTicketEvent):void
		{				
			ticketService.retrieveTicketByTicketType(evt.ticketType, retrieveTicketResult);
		}

		[Observer]
		public function retrieveTicketByOrderNumCaseNum(evt:RetrieveTicketByOrderNumCaseNumEvent):void
		{			
			ticketService.retrieveTicketByOrderNumCaseNum(evt.moeOrderNumCaseNum, evt.enableDrugCharge, evt.checkOpas, evt.checkFcs, evt.forcePrintTicket, retrieveTicketResult);			
		}
		
		[Observer]
		public function retrieveTicketByReprintTicketNum(evt:RetrieveTicketByReprintTicketNumEvent):void
		{			
			ticketService.retrieveTicketByReprintTicketNum(evt.reprintTicketNum, retrieveTicketResult);
		}
		
		private function retrieveTicketResult(evt:TideResultEvent):void{
			evt.context.dispatchEvent(new RefreshTicketEvent(ticketGenResult));
		}

		[Observer]
		public function retrieveBatchTicket(evt:RetrieveBatchTicketEvent):void
		{				
			ticketService.retrieveBatchTicket(retrieveTicketResult);
		}

		private function retrieveBatchTicketResult(evt:TideResultEvent):void{
			evt.context.dispatchEvent(new RefreshTicketEvent(ticketGenResult));
		}

		
	}
}
