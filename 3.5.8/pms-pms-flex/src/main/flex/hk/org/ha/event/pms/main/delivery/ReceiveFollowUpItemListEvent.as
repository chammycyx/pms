package hk.org.ha.event.pms.main.delivery
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ReceiveFollowUpItemListEvent extends AbstractTideEvent
	{
		public function ReceiveFollowUpItemListEvent()
		{
			super();
		}
	}
}