package hk.org.ha.event.pms.main.cddh {
	
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
	import mx.collections.ArrayCollection;
	
	public class RetrieveLegacyDispLabelEvent extends AbstractTideEvent 
	{
		private var _dispOrderItemKey:ArrayCollection;
		
		public function RetrieveLegacyDispLabelEvent(dispOrderItemKey:ArrayCollection):void 
		{
			super();
			_dispOrderItemKey = dispOrderItemKey;
		}
		
		public function get dispOrderItemKey():ArrayCollection
		{
			return _dispOrderItemKey;
		}
	}
}