package hk.org.ha.event.pms.main.onestop {
		
	import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
	
	import org.granite.tide.events.AbstractTideEvent;
		
	public class UnLockOrderByRequestResultHandlerEvent extends AbstractTideEvent 
	{		
		public function UnLockOrderByRequestResultHandlerEvent():void 
		{
			super();
		}   
	}
}