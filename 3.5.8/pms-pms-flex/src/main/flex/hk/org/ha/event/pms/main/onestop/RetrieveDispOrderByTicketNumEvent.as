package hk.org.ha.event.pms.main.onestop {
		
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDispOrderByTicketNumEvent extends AbstractTideEvent 
	{	
		private var _srcEntryView:String;
		private var _dispDate:Date;
		private var _ticketNum:String;
		private var _triggerByOtherScreen:Boolean;
		
		public function RetrieveDispOrderByTicketNumEvent(srcEntryView:String, dispDate:Date, ticketNum:String, triggerByOtherScreen:Boolean=false):void 
		{
			super();
			this._srcEntryView = srcEntryView;
			this._dispDate = dispDate;
			this._ticketNum = ticketNum;
			this._triggerByOtherScreen = triggerByOtherScreen;
		}
		
		public function get srcEntryView():String {
			return _srcEntryView;
		}
		
		public function get dispDate():Date {
			return _dispDate;
		} 
		
		public function get ticketNum():String {
			return _ticketNum;
		}
		
		public function get triggerByOtherScreen():Boolean {
			return _triggerByOtherScreen;
		}
	}
}