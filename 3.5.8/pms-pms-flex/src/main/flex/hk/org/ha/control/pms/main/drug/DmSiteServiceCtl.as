package hk.org.ha.control.pms.main.drug {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.drug.RetrieveDmSiteEvent;
	import hk.org.ha.model.pms.biz.drug.DmSiteServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("dmSiteServiceCtl", restrict="true")]
	public class DmSiteServiceCtl 
	{
		[In]
		public var dmSiteService:DmSiteServiceBean;
		
		private var successEvent:Event;
		
		private var failEvent:Event;
		
		[Observer]
		public function retrieveDmSite(evt:RetrieveDmSiteEvent):void
		{
			successEvent = evt.successEvent;
			failEvent = evt.failEvent;
			
			In(Object(dmSiteService).retrieveSuccess)
			dmSiteService.retrieveDmSite(evt.siteCode, retrieveDmSiteResult);
		}
		
		private function retrieveDmSiteResult(evt:TideResultEvent):void 
		{
			if(Object(dmSiteService).retrieveSuccess){
				if(successEvent!=null){
					evt.context.dispatchEvent( successEvent );
				}
			}else{
				if(failEvent!=null){
					evt.context.dispatchEvent( failEvent );
				}
			}
		}
		
	}
}
