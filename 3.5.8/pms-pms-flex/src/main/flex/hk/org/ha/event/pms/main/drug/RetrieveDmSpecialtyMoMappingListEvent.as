package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDmSpecialtyMoMappingListEvent extends AbstractTideEvent 
	{
		private var _specialtyCode:String;
		private var _event:Event;
		
		public function RetrieveDmSpecialtyMoMappingListEvent(specialtyCode:String, event:Event=null):void 
		{
			super();
			_specialtyCode = specialtyCode;
			_event = event;
		}
		
		public function get specialtyCode():String
		{
			return _specialtyCode;
		}
		
		public function get event():Event
		{
			return _event;
		}
	}
}