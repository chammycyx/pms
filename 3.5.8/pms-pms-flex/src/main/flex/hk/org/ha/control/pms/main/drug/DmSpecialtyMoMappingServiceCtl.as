package hk.org.ha.control.pms.main.reftable {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.drug.RetrieveDmSpecialtyMoMappingEvent;
	import hk.org.ha.model.pms.biz.drug.DmSpecialtyMoMappingServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("dmSpecialtyMoMappingServiceCtl", restrict="true")]
	public class DmSpecialtyMoMappingServiceCtl 
	{
		[In]
		public var dmSpecialtyMoMappingService:DmSpecialtyMoMappingServiceBean;
		
		private var successEvent:Event;
		
		private var failEvent:Event;
		
		[Observer]
		public function retrieveDmSpecialtyMoMapping(evt:RetrieveDmSpecialtyMoMappingEvent):void
		{
			successEvent = evt.successEvent;
			failEvent = evt.failEvent;
			
			In(Object(dmSpecialtyMoMappingService).retrieveSuccess)
			dmSpecialtyMoMappingService.retrieveDmSpecialtyMoMapping(evt.specialtyCode, retrieveDmSpecialtyMoMappingResult);
		}
		
		private function retrieveDmSpecialtyMoMappingResult(evt:TideResultEvent):void 
		{
			if(Object(dmSpecialtyMoMappingService).retrieveSuccess){
				if(successEvent!=null){
					evt.context.dispatchEvent( successEvent );
				}
			}else{
				if(failEvent!=null){
					evt.context.dispatchEvent( failEvent );
				}
			}
		}
		
	}
}
