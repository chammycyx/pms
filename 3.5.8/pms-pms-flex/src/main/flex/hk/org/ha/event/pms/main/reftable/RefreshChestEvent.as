package hk.org.ha.event.pms.main.reftable {
	
	import hk.org.ha.model.pms.persistence.reftable.Chest;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshChestEvent extends AbstractTideEvent 
	{
		private var _result:Boolean;
		private var _action:String;
		private var _chest:Chest;
		
		public function RefreshChestEvent(action:String, result:Boolean, chest:Chest=null):void 
		{
			super();
			_result = result;
			_action = action;
			_chest = chest;
		}
		
		public function get result():Boolean
		{
			return _result;
		}
		
		public function get action():String
		{
			return _action;
		}
		
		public function get chest():Chest 
		{
			return _chest;
		}
	}
}