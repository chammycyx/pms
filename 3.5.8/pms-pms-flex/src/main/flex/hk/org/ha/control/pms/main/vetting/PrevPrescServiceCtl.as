package hk.org.ha.control.pms.main.vetting {

	import hk.org.ha.event.pms.main.vetting.RetrievePrevPrescEvent;
	import hk.org.ha.event.pms.main.vetting.show.ShowPrevPrescViewEvent;
	import hk.org.ha.model.pms.biz.vetting.PrevPrescServiceBean;
	import hk.org.ha.model.pms.persistence.disp.MedOrder;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;

	[Bindable]
	[Name("PrevPrescServiceCtl", restrict="true")]
	public class PrevPrescServiceCtl {

		[In]
		public var ctx:Context;
		
		[In]
		public var prevPrescService:PrevPrescServiceBean;
		
		[Observer]
		public function retrievePrevPresc(evt:RetrievePrevPrescEvent):void {
			prevPrescService.retrievePrevPresc(evt.orderNum, retrievePrevPrescResult);
		}

		private function retrievePrevPrescResult(evt:TideResultEvent):void {
			if (evt.result == null) {
				return;
			}
			dispatchEvent(new ShowPrevPrescViewEvent(evt.result as MedOrder));
		}
	}
}
