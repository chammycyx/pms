package hk.org.ha.control.pms.main.drug {
	
	import flash.events.Event;
	import flash.utils.Dictionary;
	
	import hk.org.ha.event.pms.main.alert.mds.RetrieveAlertMsgForDrugSearchEvent;
	import hk.org.ha.event.pms.main.alert.mds.popup.ShowAlertMsgPopupEvent;
	import hk.org.ha.event.pms.main.alert.mds.popup.ShowMdsLogPopupEvent;
	import hk.org.ha.event.pms.main.drug.AddBnfNodeEvent;
	import hk.org.ha.event.pms.main.drug.AddDrugDetailNodeEvent;
	import hk.org.ha.event.pms.main.drug.CreateBnfTreeEvent;
	import hk.org.ha.event.pms.main.drug.CreateDrugNameTreeEvent;
	import hk.org.ha.event.pms.main.drug.CreateSelectedDrugEvent;
	import hk.org.ha.event.pms.main.drug.CreateSelectedMpDrugEvent;
	import hk.org.ha.event.pms.main.drug.ResumeAddDrugLoadingEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveBnfTreeDetailEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveDrugDetailEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveDrugSearchBnfEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveDrugSearchIngredientEvent;
	import hk.org.ha.event.pms.main.drug.SearchDrugByBnfEvent;
	import hk.org.ha.event.pms.main.drug.SearchDrugEvent;
	import hk.org.ha.event.pms.main.drug.SetCapdDrugEvent;
	import hk.org.ha.event.pms.main.drug.SetDrugSearchBnfEvent;
	import hk.org.ha.event.pms.main.drug.SetDrugSearchIngredientEvent;
	import hk.org.ha.event.pms.main.drug.SetFreeTextEntryDrugEvent;
	import hk.org.ha.event.pms.main.drug.SetMpCapdDrugEvent;
	import hk.org.ha.event.pms.main.drug.SetMpFreeTextEntryDrugEvent;
	import hk.org.ha.event.pms.main.drug.SetRelatedDrugPriceEvent;
	import hk.org.ha.event.pms.main.drug.popup.FreeTextEntryPopupProp;
	import hk.org.ha.event.pms.main.drug.popup.MpFreeTextEntryPopupProp;
	import hk.org.ha.event.pms.main.drug.popup.RefreshMpCapdDrugPopupEvent;
	import hk.org.ha.event.pms.main.drug.popup.RetrieveDrugPriceEvent;
	import hk.org.ha.event.pms.main.drug.popup.RetrieveFreeTextEntryEvent;
	import hk.org.ha.event.pms.main.drug.popup.RetrieveMpCapdDrugCalciumEvent;
	import hk.org.ha.event.pms.main.drug.popup.RetrieveMpCapdDrugConcentrationEvent;
	import hk.org.ha.event.pms.main.drug.popup.RetrieveMpCapdDrugSupplierEvent;
	import hk.org.ha.event.pms.main.drug.popup.RetrieveMpFreeTextEntryEvent;
	import hk.org.ha.event.pms.main.drug.popup.ShowDrugPriceEnquiryPopupEvent;
	import hk.org.ha.event.pms.main.drug.popup.ShowFreeTextEntryPopupEvent;
	import hk.org.ha.event.pms.main.drug.popup.ShowMpCapdDrugPopupEvent;
	import hk.org.ha.event.pms.main.drug.popup.ShowMpFreeTextEntryPopupEvent;
	import hk.org.ha.event.pms.main.drug.popup.ShowMpPreparationPopupEvent;
	import hk.org.ha.event.pms.main.drug.show.ClearDrugSearchViewEvent;
	import hk.org.ha.event.pms.main.reftable.show.ShowCommonOrderStatusMaintViewEvent;
	import hk.org.ha.event.pms.main.reftable.show.ShowDosageConversionMaintViewEvent;
	import hk.org.ha.event.pms.main.reftable.show.ShowFixedConcnPrepActivationMaintViewEvent;
	import hk.org.ha.event.pms.main.reftable.show.ShowMpDosageConversionMaintViewEvent;
	import hk.org.ha.event.pms.main.reftable.show.ShowMultiDoseConversionMaintViewEvent;
	import hk.org.ha.event.pms.main.reftable.show.ShowPivasDrugMaintViewEvent;
	import hk.org.ha.event.pms.main.vetting.AddItemFromDrugSearchEvent;
	import hk.org.ha.event.pms.main.vetting.UpdateOrderFromOrderViewEvent;
	import hk.org.ha.event.pms.main.vetting.mp.show.ShowManualEntryItemEditViewEvent;
	import hk.org.ha.event.pms.main.vetting.show.ShowOrderEditCapdItemViewEvent;
	import hk.org.ha.model.pms.biz.drug.DrugSearchDrugBnfServiceBean;
	import hk.org.ha.model.pms.biz.drug.DrugSearchIngredientServiceBean;
	import hk.org.ha.model.pms.biz.drug.DrugSearchServiceBean;
	import hk.org.ha.model.pms.dms.persistence.DmDrug;
	import hk.org.ha.model.pms.dms.vo.DrugCommonDosage;
	import hk.org.ha.model.pms.dms.vo.DrugName;
	import hk.org.ha.model.pms.dms.vo.PreparationSearchCriteria;
	import hk.org.ha.model.pms.dms.vo.RelatedDrugSearchCriteria;
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	import hk.org.ha.model.pms.persistence.disp.PharmOrder;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
	import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
	import hk.org.ha.model.pms.udt.drug.DrugScope;
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	import hk.org.ha.model.pms.vo.drug.DrugSearchReturn;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("drugSearchServiceCtl", restrict="true")]
	public class DrugSearchServiceCtl 
	{	
		[In]
		public var ctx:Context;
		
		[In]
		public var drugSearchService:DrugSearchServiceBean;

		[In]
		public var drugSearchDrugBnfService:DrugSearchDrugBnfServiceBean

		[In]
		public var drugSearchIngredientService:DrugSearchIngredientServiceBean;

		[In]
		public var medProfile:MedProfile;
		
		[In]
		public var medProfileItemList:ListCollectionView;
		
		private var createSelectedDrugProperties:Dictionary = new Dictionary();;
		
		private var retrieveDrugPriceProperties:Dictionary = new Dictionary();;
		
		private var retrieveFreeTextEntryProperties:Dictionary = new Dictionary();;
		
		private var setFreeTextEntryDrugProperties:Dictionary = new Dictionary();;
		
		private var setCapdDrugProperties:Dictionary = new Dictionary();;

		
		[Observer]
		public function drugSearch(evt:SearchDrugEvent):void {
			drugSearchService.drugSearch(evt.drugSearchSource, evt.relatedDrugSearchCriteria, evt.routeFormSearchCriteria, drugSearchResult);
		}

		private function drugSearchResult(evt:TideResultEvent):void {
			var drugSearchReturn:DrugSearchReturn = evt.result as DrugSearchReturn;
			dispatchEvent(new CreateDrugNameTreeEvent(drugSearchReturn.drugSearchSource, drugSearchReturn.drugSearchNameMapList as ArrayCollection, drugSearchReturn.itemCodeByItemCodeSearch));
		}
		
		[Observer]
		public function drugSearchByBnf(evt:SearchDrugByBnfEvent):void {
			drugSearchService.drugSearchByBnf(evt.drugSearchSource, evt.bnfCriteria, drugSearchByBnfResult);
		}

		private function drugSearchByBnfResult(evt:TideResultEvent):void {
			var drugSearchReturn:DrugSearchReturn = evt.result as DrugSearchReturn;
			dispatchEvent(new CreateBnfTreeEvent(drugSearchReturn.drugSearchSource, drugSearchReturn.drugSearchBnfMapList as ArrayCollection));
		}

		[Observer]
		public function retrieveBnfTreeDetail(evt:RetrieveBnfTreeDetailEvent):void {
			drugSearchService.retrieveBnfTreeDetail(evt.drugSearchSource, evt.bnfCriteria, retrieveBnfTreeDetailResult);
		}
		
		private function retrieveBnfTreeDetailResult(evt:TideResultEvent):void {
			var drugSearchReturn:DrugSearchReturn = evt.result as DrugSearchReturn;
			dispatchEvent(new AddBnfNodeEvent(drugSearchReturn.drugSearchSource, drugSearchReturn.drugSearchBnfNodeMapList as ArrayCollection));
		}
		
		[Observer]
		public function retrieveDrugDetail(evt:RetrieveDrugDetailEvent):void {
			drugSearchService.retrieveDrugDetail(evt.drugSearchSource, evt.routeFormSearchCriteria, evt.theraGroup, retrieveDrugDetailResult);
		}

		private function retrieveDrugDetailResult(evt:TideResultEvent):void {
			var drugSearchReturn:DrugSearchReturn = evt.result as DrugSearchReturn;
			dispatchEvent(new AddDrugDetailNodeEvent(drugSearchReturn.drugSearchSource, drugSearchReturn.drugSearchDetailMapList as ArrayCollection));
		}

		[Observer]
		public function retrieveDrugSearchBnf(evt:RetrieveDrugSearchBnfEvent):void {
			drugSearchDrugBnfService.retrieveDrugSearchBnf(evt.drugSearchSource, evt.bnfSearchCriteria, retrieveDrugSearchBnfResult);
		}

		private function retrieveDrugSearchBnfResult(evt:TideResultEvent):void {
			var drugSearchReturn:DrugSearchReturn = evt.result as DrugSearchReturn;
			dispatchEvent(new SetDrugSearchBnfEvent(drugSearchReturn.drugSearchSource, drugSearchReturn.drugSearchDrugBnfList as ArrayCollection));
		}

		[Observer]
		public function retrieveDrugSearchIngredient(evt:RetrieveDrugSearchIngredientEvent):void {
			drugSearchIngredientService.retrieveDrugSearchIngredient(evt.drugSearchSource, evt.ingredientSearchCriteria, retrieveDrugSearchIngredientResult);
		}

		private function retrieveDrugSearchIngredientResult(evt:TideResultEvent):void {
			var drugSearchReturn:DrugSearchReturn = evt.result as DrugSearchReturn;
			dispatchEvent(new SetDrugSearchIngredientEvent(drugSearchReturn.drugSearchSource, drugSearchReturn.drugSearchDrugIngredientList as ArrayCollection));
		}
		
		[Observer]
		public function retrieveDrugPrice(evt:RetrieveDrugPriceEvent):void {
			if (evt.drugName == null) {
				// retrieve price of related drug from Drug Price Enquiry popup 	
				drugSearchService.retrieveDrugPrice(evt.drugSearchSource, null, evt.preparationSearchCriteria, evt.relatedDrugSearchCriteria, retrieveRelatedDrugPriceResult);
			} else {
				var callProperties:Dictionary = new Dictionary();
				retrieveDrugPriceProperties[evt.drugSearchSource.dataValue] = callProperties;
				callProperties['preparationSearchCriteria'] = evt.preparationSearchCriteria;
				callProperties['relatedDrugSearchCriteria'] = evt.relatedDrugSearchCriteria;
				callProperties['popupCancelHandler'] = evt.popupCancelHandler;
				
				// retrieve drug price from selected drug search tree node
				drugSearchService.retrieveDrugPrice(evt.drugSearchSource, evt.drugName, evt.preparationSearchCriteria, null, retrieveDrugPriceResult);
			}
		}

		private function retrieveRelatedDrugPriceResult(evt:TideResultEvent):void {
			var drugSearchReturn:DrugSearchReturn = evt.result as DrugSearchReturn;
			dispatchEvent(new SetRelatedDrugPriceEvent(drugSearchReturn.drugSearchSource));
		}
		
		private function retrieveDrugPriceResult(evt:TideResultEvent):void {
			var drugSearchReturn:DrugSearchReturn = evt.result as DrugSearchReturn;
			
			var callProperties:Dictionary = retrieveDrugPriceProperties[drugSearchReturn.drugSearchSource.dataValue];
			var preparationSearchCriteria:PreparationSearchCriteria = callProperties['preparationSearchCriteria'] as PreparationSearchCriteria;
			var relatedDrugSearchCriteria:RelatedDrugSearchCriteria = callProperties['relatedDrugSearchCriteria'] as RelatedDrugSearchCriteria;
			var popupCancelHandler:Function = callProperties['popupCancelHandler'] as Function;
			
			var showDrugPriceEnquiryPopupEvent:ShowDrugPriceEnquiryPopupEvent = new ShowDrugPriceEnquiryPopupEvent();
			showDrugPriceEnquiryPopupEvent.drugSearchSource = drugSearchReturn.drugSearchSource;
			showDrugPriceEnquiryPopupEvent.preparationSearchCriteria = preparationSearchCriteria;
			showDrugPriceEnquiryPopupEvent.relatedDrugSearchCriteria = relatedDrugSearchCriteria;
			showDrugPriceEnquiryPopupEvent.popupCancelHandler = popupCancelHandler;
			dispatchEvent(showDrugPriceEnquiryPopupEvent);
		}
		
		[Observer]
		public function retrieveFreeTextEntry(evt:RetrieveFreeTextEntryEvent):void {
			var callProperties:Dictionary = new Dictionary();
			if (evt.drugSearchSource == null) {
				// handle the case of free text popup triggered by order edit
				retrieveFreeTextEntryProperties['orderEditScreen'] = callProperties;
			} else {
				retrieveFreeTextEntryProperties[evt.drugSearchSource.dataValue] = callProperties;
			}
			callProperties['freeTextEntryPopupProp'] = evt.freeTextEntryPopupProp;
			
			drugSearchService.retrieveFreeTextEntry(evt.drugSearchSource, retrieveFreeTextEntryResult);
		}
		
		private function retrieveFreeTextEntryResult(evt:TideResultEvent):void {
			var drugSearchReturn:DrugSearchReturn = evt.result as DrugSearchReturn;
			
			var callProperties:Dictionary;
			if (drugSearchReturn.drugSearchSource == null) {
				callProperties = retrieveFreeTextEntryProperties['orderEditScreen'];
			} else {
				callProperties = retrieveFreeTextEntryProperties[drugSearchReturn.drugSearchSource.dataValue];
			}
			var freeTextEntryPopupProp:FreeTextEntryPopupProp = callProperties['freeTextEntryPopupProp'] as FreeTextEntryPopupProp;
			
			freeTextEntryPopupProp.drugSearchRouteDescList = drugSearchReturn.drugSearchRouteDescList as ArrayCollection;
			freeTextEntryPopupProp.drugSearchFormDescList = drugSearchReturn.drugSearchFormDescList as ArrayCollection;
			
			dispatchEvent(new ShowFreeTextEntryPopupEvent(freeTextEntryPopupProp, drugSearchReturn.drugSearchSource));
		}
		
		[Observer]
		public function retrieveMpFreeTextEntry(evt:RetrieveMpFreeTextEntryEvent):void {
			var callProperties:Dictionary = new Dictionary();
			retrieveFreeTextEntryProperties[evt.drugSearchSource.dataValue] = callProperties;
			callProperties['mpFreeTextEntryPopupProp'] = evt.mpFreeTextEntryPopupProp;
			
			drugSearchService.retrieveMpFreeTextEntry(evt.drugSearchSource, retrieveMpFreeTextEntryResult);
		}
		
		private function retrieveMpFreeTextEntryResult(evt:TideResultEvent):void {
			var drugSearchReturn:DrugSearchReturn = evt.result as DrugSearchReturn;
			
			var callProperties:Dictionary = retrieveFreeTextEntryProperties[drugSearchReturn.drugSearchSource.dataValue];
			var mpFreeTextEntryPopupProp:MpFreeTextEntryPopupProp = callProperties['mpFreeTextEntryPopupProp'] as MpFreeTextEntryPopupProp;
			
			mpFreeTextEntryPopupProp.drugSearchSiteList = drugSearchReturn.drugSearchSiteList as ArrayCollection;
			
			dispatchEvent(new ShowMpFreeTextEntryPopupEvent(mpFreeTextEntryPopupProp, drugSearchReturn.drugSearchSource));
		}
		
		[Observer]
		public function retrieveMpCapdDrugSupplier(evt:RetrieveMpCapdDrugSupplierEvent):void {
			drugSearchService.retrieveMpCapdDrugSupplier(evt.drugSearchSource, retrieveMpCapdDrugSupplierResult);
		}		
		
		private function retrieveMpCapdDrugSupplierResult(evt:TideResultEvent):void {
			var drugSearchReturn:DrugSearchReturn = evt.result as DrugSearchReturn;
			dispatchEvent(new ShowMpCapdDrugPopupEvent(drugSearchReturn.drugSearchSource));
		}
		
		[Observer]
		public function retrieveMpCapdDrugCalcium(evt:RetrieveMpCapdDrugCalciumEvent):void {
			drugSearchService.retrieveMpCapdDrugCalcium(evt.drugSearchSource, evt.supplierSystem, retrieveMpCapdDrugCalciumResult);
		}

		private function retrieveMpCapdDrugCalciumResult(evt:TideResultEvent):void {
			var drugSearchReturn:DrugSearchReturn = evt.result as DrugSearchReturn;
			dispatchEvent(new RefreshMpCapdDrugPopupEvent(drugSearchReturn.drugSearchSource));
		}

		[Observer]
		public function retrieveMpCapdDrugConcentration(evt:RetrieveMpCapdDrugConcentrationEvent):void {
			drugSearchService.retrieveMpCapdDrugConcentration(evt.drugSearchSource, evt.supplierSystem, evt.calciumStrength, retrieveMpCapdDrugConcentrationResult);
		}
		
		private function retrieveMpCapdDrugConcentrationResult(evt:TideResultEvent):void {
			var drugSearchReturn:DrugSearchReturn = evt.result as DrugSearchReturn;
			dispatchEvent(new RefreshMpCapdDrugPopupEvent(drugSearchReturn.drugSearchSource));
		}
		
		[Observer]
		public function setFreeTextEntryDrug(evt:SetFreeTextEntryDrugEvent):void {
			var callProperties:Dictionary = new Dictionary();
			if (evt.drugSearchSource == null) {
				// handle the case of free text popup triggered by order edit
				setFreeTextEntryDrugProperties['orderEditScreen'] = callProperties;
			} else {
				setFreeTextEntryDrugProperties[evt.drugSearchSource.dataValue] = callProperties;
			}
			callProperties['okCallbackEvent'] = evt.callbackEvent;
			
			drugSearchService.createFreeTextEntryDrug(evt.drugSearchSource, evt.rxDrug, evt.carryDuration, evt.carryDurationUnit, setFreeTextEntryDrugResult);
		}
		
		private function setFreeTextEntryDrugResult(evt:TideResultEvent):void {
			var drugSearchReturn:DrugSearchReturn = evt.result as DrugSearchReturn;
			
			var callProperties:Dictionary;
			if (drugSearchReturn.drugSearchSource == null) {
				callProperties = setFreeTextEntryDrugProperties['orderEditScreen'];
			} else {
				callProperties = setFreeTextEntryDrugProperties[drugSearchReturn.drugSearchSource.dataValue];
			}
			var okCallbackEvent:Event = callProperties['okCallbackEvent'] as Event;
			
			if (okCallbackEvent is AddItemFromDrugSearchEvent) {
				okCallbackEvent["autoAddDrug"] = false;
			}
			dispatchEvent(okCallbackEvent);
		}
		
		[Observer]
		public function setCapdDrug(evt:SetCapdDrugEvent):void {
			var callProperties:Dictionary = new Dictionary();
			setCapdDrugProperties[evt.drugSearchSource.dataValue] = callProperties;
			callProperties['okCallbackEvent'] = evt.callbackEvent;
			
			drugSearchService.createCapdDrug(evt.drugSearchSource, setCapdDrugResult);
		}
		
		private function setCapdDrugResult(evt:TideResultEvent):void {
			var drugSearchReturn:DrugSearchReturn = evt.result as DrugSearchReturn;
			
			var callProperties:Dictionary = setCapdDrugProperties[drugSearchReturn.drugSearchSource.dataValue];
			var okCallbackEvent:Event = callProperties['okCallbackEvent'] as Event;
			
			dispatchEvent(okCallbackEvent);
		}
		
		[Observer]
		public function createSelectedDrug(evt:CreateSelectedDrugEvent):void {
			var callProperties:Dictionary = new Dictionary();
			createSelectedDrugProperties[evt.drugSearchSource.dataValue] = callProperties;
			callProperties['okCallbackEvent'] = evt.okCallbackEvent;
			
			if (evt.drugSearchSource.maintenance) {
				drugSearchService.createSelectedDrugByDosageConversion(evt.drugSearchSource, evt.drugName, evt.routeForm, evt.preparation, evt.commonDosage, evt.dmRegimen, createSelectedDrugByDosageConversionResult);
			} else {
				callProperties['cancelCallbackEvent'] = evt.cancelCallbackEvent;
				callProperties['isQuickAdd'] = evt.isQuickAdd;
				callProperties['drugScope'] = evt.drugScope;
				
				dispatchEvent(new UpdateOrderFromOrderViewEvent(function():void {
					drugSearchService.createSelectedDrugByVetting(evt.drugSearchSource, evt.drugName, evt.routeForm, evt.preparation, evt.commonDosage, evt.dmRegimen, evt.carryDuration, evt.carryDurationUnit, evt.specialInstruction, createSelectedDrugByVettingResult);
				}));
			}
		}
		
		private function createSelectedDrugByDosageConversionResult(evt:TideResultEvent):void {
			var drugSearchReturn:DrugSearchReturn = evt.result as DrugSearchReturn;
			
			var drugSearchDmDrug:DmDrug = drugSearchReturn.drugSearchDmDrug;
			var drugSearchCommonDosage:DrugCommonDosage = drugSearchReturn.drugSearchCommonDosage;
			
			var callProperties:Dictionary = createSelectedDrugProperties[drugSearchReturn.drugSearchSource.dataValue];
			var okCallbackEvent:Event = callProperties['okCallbackEvent'] as Event;
			
			if (okCallbackEvent is ShowDosageConversionMaintViewEvent) {
				(okCallbackEvent as ShowDosageConversionMaintViewEvent).drugSearchDmDrug = drugSearchDmDrug;
				(okCallbackEvent as ShowDosageConversionMaintViewEvent).drugSearchCommonDosage = drugSearchCommonDosage;
			} else if (okCallbackEvent is ShowMpDosageConversionMaintViewEvent) {
				(okCallbackEvent as ShowMpDosageConversionMaintViewEvent).drugSearchDmDrug = drugSearchDmDrug;
			} else if (okCallbackEvent is ShowMultiDoseConversionMaintViewEvent) {
				(okCallbackEvent as ShowMultiDoseConversionMaintViewEvent).drugSearchDmDrug = drugSearchDmDrug;
			} else if (okCallbackEvent is ShowCommonOrderStatusMaintViewEvent) {
				(okCallbackEvent as ShowCommonOrderStatusMaintViewEvent).drugSearchDmDrug = drugSearchDmDrug;
			} else if (okCallbackEvent is ShowFixedConcnPrepActivationMaintViewEvent) {
				(okCallbackEvent as ShowFixedConcnPrepActivationMaintViewEvent).drugSearchDmDrug = drugSearchDmDrug;
			} else if (okCallbackEvent is ShowPivasDrugMaintViewEvent) {
				(okCallbackEvent as ShowPivasDrugMaintViewEvent).drugSearchDmDrug = drugSearchDmDrug;
			}
			dispatchEvent(okCallbackEvent);
			
			dispatchEvent(new ResumeAddDrugLoadingEvent(drugSearchReturn.drugSearchSource));
		}
		
		private function createSelectedDrugByVettingResult(evt:TideResultEvent):void {
			var drugSearchReturn:DrugSearchReturn = evt.result as DrugSearchReturn;
			
			var drugSearchMedOrderItem:MedOrderItem = drugSearchReturn.drugSearchMedOrderItem;
			
			var callProperties:Dictionary = createSelectedDrugProperties[drugSearchReturn.drugSearchSource.dataValue];
			var okCallbackEvent:Event = callProperties['okCallbackEvent'] as Event;
			var cancelCallbackEvent:Event = callProperties['cancelCallbackEvent'] as Event;
			var isQuickAdd:Boolean = callProperties['isQuickAdd'] as Boolean;
			var drugScope:DrugScope = callProperties['drugScope'] as DrugScope;
			
			dispatchEvent(new ShowMdsLogPopupEvent(function():void {
				
				if (okCallbackEvent is AddItemFromDrugSearchEvent) {
					okCallbackEvent["autoAddDrug"] = isQuickAdd;
					okCallbackEvent["drugScope"] = drugScope;
				}
				
				if (drugSearchMedOrderItem != null && drugSearchMedOrderItem.alertFlag) {
					var showAlertMsgPopupEvent:ShowAlertMsgPopupEvent = new ShowAlertMsgPopupEvent("add");
					
					showAlertMsgPopupEvent.prescribeAlternativeFunc = function():void {
						dispatchEvent(new ClearDrugSearchViewEvent());
					};
					
					showAlertMsgPopupEvent.returnVettingFunc = function():void {
						dispatchEvent(cancelCallbackEvent);
					};
					
					showAlertMsgPopupEvent.overrideAlertFunc = function():void {
						dispatchEvent(okCallbackEvent);
					}
					
					dispatchEvent(new RetrieveAlertMsgForDrugSearchEvent(showAlertMsgPopupEvent));
				}
				else {
					dispatchEvent(okCallbackEvent);
				}
				
			}));
			
			dispatchEvent(new ResumeAddDrugLoadingEvent(drugSearchReturn.drugSearchSource));
		}
		
		[Observer]
		public function setMpFreeTextEntryDrug(evt:SetMpFreeTextEntryDrugEvent):void {
			var callProperties:Dictionary = new Dictionary();
			if (evt.drugSearchSource == null) {
				// handle the case of free text popup triggered by order edit
				setFreeTextEntryDrugProperties['orderEditScreen'] = callProperties;
			} else {
				setFreeTextEntryDrugProperties[evt.drugSearchSource.dataValue] = callProperties;
			}
			callProperties['okCallbackEvent'] = evt.callbackEvent;
			
			drugSearchService.createMpFreeTextEntryDrug(evt.drugSearchSource, evt.rxDrug, setMpFreeTextEntryDrugResult);
		}

		private function setMpFreeTextEntryDrugResult(evt:TideResultEvent):void {
			var drugSearchReturn:DrugSearchReturn = evt.result as DrugSearchReturn;
			
			var callProperties:Dictionary;
			if (drugSearchReturn.drugSearchSource == null) {
				callProperties = setFreeTextEntryDrugProperties['orderEditScreen'];
			} else {
				callProperties = setFreeTextEntryDrugProperties[drugSearchReturn.drugSearchSource.dataValue];
			}
			var okCallbackEvent:Event = callProperties['okCallbackEvent'] as Event;
			
			if (okCallbackEvent is ShowManualEntryItemEditViewEvent) {
				dispatchEvent(new ShowManualEntryItemEditViewEvent(drugSearchReturn.drugSearchMedProfileItemList,MedProfileItem(drugSearchReturn.drugSearchMedProfileItemList.getItemAt(0))));
			} else {
				dispatchEvent(okCallbackEvent);
			}
		}
		
		[Observer]
		public function setMpCapdDrug(evt:SetMpCapdDrugEvent):void {
			var callProperties:Dictionary = new Dictionary();
			setCapdDrugProperties[evt.drugSearchSource.dataValue] = callProperties;
			callProperties['okCallbackEvent'] = evt.callbackEvent;
			
			drugSearchService.createMpCapdDrug(evt.drugSearchSource, evt.supplierSystem, evt.calciumStrength, evt.capd, setMpCapdDrugResult);
		}
		
		private function setMpCapdDrugResult(evt:TideResultEvent):void {
			var drugSearchReturn:DrugSearchReturn = evt.result as DrugSearchReturn;
			
			var callProperties:Dictionary = setCapdDrugProperties[drugSearchReturn.drugSearchSource.dataValue];
			var okCallbackEvent:Event = callProperties['okCallbackEvent'] as Event;
			
			if (okCallbackEvent is ShowManualEntryItemEditViewEvent) {
				dispatchEvent(new ShowManualEntryItemEditViewEvent(drugSearchReturn.drugSearchMedProfileItemList,MedProfileItem(drugSearchReturn.drugSearchMedProfileItemList.getItemAt(0))));
			} else {
				dispatchEvent(okCallbackEvent);
			}
		}
		
		[Observer]
		public function createSelectedMpDrug(evt:CreateSelectedMpDrugEvent):void {
			var callProperties:Dictionary = new Dictionary();
			createSelectedDrugProperties[evt.drugSearchSource.dataValue] = callProperties;
			
			callProperties['preparationOkHandler'] = evt.preparationOkHandler;
			callProperties['preparationCancelHandler'] = evt.preparationCancelHandler;
			callProperties['okCallbackEvent'] = evt.okCallbackEvent;
			
			if (evt.drugSearchSource.maintenance) {
				// implement later for dosgae conversion if required
			} else {
				callProperties['cancelCallbackEvent'] = evt.cancelCallbackEvent;
				callProperties['drugScope'] = evt.drugScope;
				
				drugSearchService.createSelectedMpDrugByVetting(evt.drugSearchSource, evt.drugName, evt.routeForm, evt.preparation, evt.commonOrder, evt.pivasFormula, evt.dmRegimen, evt.pasSpecialty, evt.pasSubSpecialty, evt.itemCode, createSelectedMpDrugByVettingResult);
			}
		}
		
		private function createSelectedMpDrugByVettingResult(evt:TideResultEvent):void {
			var drugSearchReturn:DrugSearchReturn = evt.result as DrugSearchReturn;
			
			var callProperties:Dictionary = createSelectedDrugProperties[drugSearchReturn.drugSearchSource.dataValue];
			
			var preparationOkHandler:Function = callProperties['preparationOkHandler'] as Function;
			var preparationCancelHandler:Function = callProperties['preparationCancelHandler'] as Function;
			var okCallbackEvent:Event = callProperties['okCallbackEvent'] as Event;
			var cancelCallbackEvent:Event = callProperties['cancelCallbackEvent'] as Event;
			var drugScope:DrugScope = callProperties['drugScope'] as DrugScope;
			
			if (drugSearchReturn.drugSearchPreparationList != null) {
				dispatchEvent(new ShowMpPreparationPopupEvent(drugSearchReturn.drugSearchSource, drugSearchReturn.drugSearchPreparationList as ArrayCollection, 
					drugSearchReturn.drugSearchPreparationDescMap, preparationOkHandler, preparationCancelHandler));
				
				dispatchEvent(new ResumeAddDrugLoadingEvent(drugSearchReturn.drugSearchSource));
				
				return;
			}

			dispatchEvent(new ResumeAddDrugLoadingEvent(drugSearchReturn.drugSearchSource));
			
			if (okCallbackEvent is ShowManualEntryItemEditViewEvent) {
			    dispatchEvent(new ShowManualEntryItemEditViewEvent(drugSearchReturn.drugSearchMedProfileItemList,MedProfileItem(drugSearchReturn.drugSearchMedProfileItemList.getItemAt(0))));
			} else {
				dispatchEvent(okCallbackEvent);
			}
		}
	}
}