package hk.org.ha.event.pms.main.reftable {
	
	import mx.core.UIComponent;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowRefillModuleMaintSaveSuccessMsgEvent extends AbstractTideEvent 
	{		
		private var _nextTabIndex:Number;
		private var _nextSubTabIndex:Number;
		private var _setEnableQtyAdjFlag:Boolean;
		private var _caller:UIComponent;
		private var _showRelogonMsg:Boolean;
		
		public function ShowRefillModuleMaintSaveSuccessMsgEvent(nextTabIndex:Number, 
																 nextSubTabIndex:Number, 
																 showRelogonMsg:Boolean, 
																 caller:UIComponent, 
																 setEnableQtyAdjFlag:Boolean=false):void 
		{
			super();
			_nextTabIndex = nextTabIndex;
			_nextSubTabIndex = nextSubTabIndex;
			_showRelogonMsg = showRelogonMsg;
			_caller = caller;
			_setEnableQtyAdjFlag = setEnableQtyAdjFlag;
		}
		
		public function get nextTabIndex():Number {
			return _nextTabIndex;
		}
		
		public function get nextSubTabIndex():Number {
			return _nextSubTabIndex;
		}
		
		public function get showRelogonMsg():Boolean {
			return _showRelogonMsg;
		}
		
		public function get caller():UIComponent{
			return _caller;
		}
		
		public function get setEnableQtyAdjFlag():Boolean {
			return _setEnableQtyAdjFlag;
		}
	}
}