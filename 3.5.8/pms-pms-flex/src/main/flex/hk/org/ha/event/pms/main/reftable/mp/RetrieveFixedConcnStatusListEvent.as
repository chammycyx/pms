package hk.org.ha.event.pms.main.reftable.mp {
	
	import hk.org.ha.model.pms.dms.persistence.DmDrug;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveFixedConcnStatusListEvent extends AbstractTideEvent 
	{
		private var _patHospCode:String;
		private var _dmDrug:DmDrug;
		
		public function get patHospCode():String
		{
			return _patHospCode;
		}

		public function get dmDrug():DmDrug
		{
			return _dmDrug;
		}

		public function RetrieveFixedConcnStatusListEvent(patHospCode:String, dmDrug:DmDrug):void 
		{
			_patHospCode = patHospCode;
			_dmDrug = dmDrug;
			super();
		}
	}
}