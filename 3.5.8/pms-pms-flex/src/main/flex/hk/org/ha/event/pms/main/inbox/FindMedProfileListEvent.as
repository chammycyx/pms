package hk.org.ha.event.pms.main.inbox
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class FindMedProfileListEvent extends AbstractTideEvent
	{
		private var _hkidCaseNum:String;
		
		public function FindMedProfileListEvent(hkidCaseNum:String):void
		{
			super();
			_hkidCaseNum = hkidCaseNum;
		}
		
		public function get hkidCaseNum():String
		{
			return _hkidCaseNum;
		}
	}
}