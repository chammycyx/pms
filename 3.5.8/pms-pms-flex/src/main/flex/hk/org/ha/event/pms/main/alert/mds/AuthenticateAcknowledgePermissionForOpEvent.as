package hk.org.ha.event.pms.main.alert.mds {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;

	public class AuthenticateAcknowledgePermissionForOpEvent extends AbstractTideEvent {
		
		private var _userName:String;
		
		private var _password:String;
		
		private var _successCallback:Function;
		
		private var _faultCallback:Function;
		
		public function AuthenticateAcknowledgePermissionForOpEvent(userName:String, password:String, successCallback:Function=null, faultCallback:Function=null) {
			super();
			_userName = userName;
			_password = password;
			_successCallback = successCallback;
			_faultCallback = faultCallback;
		}
		
		public function get userName():String {
			return _userName;
		}
		
		public function get password():String {
			return _password;
		}

		public function get successCallback():Function {
			return _successCallback;
		}
		
		public function get faultCallback():Function {
			return _faultCallback;
		}
	}
}
