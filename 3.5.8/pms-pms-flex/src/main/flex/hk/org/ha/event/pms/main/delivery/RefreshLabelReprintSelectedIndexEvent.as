package hk.org.ha.event.pms.main.delivery {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshLabelReprintSelectedIndexEvent extends AbstractTideEvent 
	{
		public function RefreshLabelReprintSelectedIndexEvent():void 
		{
			super();
		}
	}
}