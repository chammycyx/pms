package hk.org.ha.event.pms.main.inbox
{
	import hk.org.ha.model.pms.udt.medprofile.MedProfileStatAdminType;
	import hk.org.ha.model.pms.udt.medprofile.MedProfileStatOrderType;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateMedProfileStatTypesEvent extends AbstractTideEvent
	{
		private var _adminType:MedProfileStatAdminType;
		private var _orderType:MedProfileStatOrderType;
		
		public function UpdateMedProfileStatTypesEvent(adminType:MedProfileStatAdminType, orderType:MedProfileStatOrderType)
		{
			super();
			_adminType = adminType;
			_orderType = orderType;
		}
		
		public function get adminType():MedProfileStatAdminType
		{
			return _adminType;
		}
		
		public function get orderType():MedProfileStatOrderType
		{
			return _orderType;
		}
	}
}