package hk.org.ha.event.pms.main.inbox
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ReceiveCancelDischargeTransferListEvent extends AbstractTideEvent
	{
		public function ReceiveCancelDischargeTransferListEvent()
		{
			super();
		}
	}
}