package hk.org.ha.event.pms.main.vetting.mp.tpn.popup
{
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.view.pms.main.vetting.mp.tpn.popup.TpnPopup;
	
	public class ShowDeleteRequestPopupEvent extends AbstractTideEvent
	{
		private var _selectedIndex:Number;
		
		private var _tpnPopup:TpnPopup;
		
		public function ShowDeleteRequestPopupEvent(selectedIndex:Number, tpnPopup:TpnPopup):void {
			super();
			_selectedIndex = selectedIndex;
			_tpnPopup = tpnPopup;
		}
		
		public function get selectedIndex():Number
		{
			return _selectedIndex;
		}
		
		public function get tpnPopup():TpnPopup
		{
			return _tpnPopup;
		}
	}
}