package hk.org.ha.event.pms.main.ehr {
	
	import hk.org.ha.model.pms.vo.ehr.EhrssViewerUrl;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowEhrssViewerEvent extends AbstractTideEvent 
	{			
		private var _ehrssViewerUrl:EhrssViewerUrl;
		private var _successCallBack:Function;
		private var _errorCallBack:Function;
		
		public function ShowEhrssViewerEvent(ehrssViewerUrl:EhrssViewerUrl, successCallBack:Function, errorCallBack:Function):void 
		{
			super();
			_ehrssViewerUrl = ehrssViewerUrl;
			_successCallBack = successCallBack;
			_errorCallBack = errorCallBack;
		}
		
		public function get ehrssViewerUrl():EhrssViewerUrl {
			return _ehrssViewerUrl;
		}
		
		public function get successCallBack():Function {
			return _successCallBack;
		}
		
		public function get errorCallBack():Function {
			return _errorCallBack;
		}
	}
}