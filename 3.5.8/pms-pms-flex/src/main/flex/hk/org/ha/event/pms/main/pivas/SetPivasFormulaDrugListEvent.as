package hk.org.ha.event.pms.main.pivas
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetPivasFormulaDrugListEvent extends AbstractTideEvent
	{
		private var _pivasFormulaDrugList:ArrayCollection
		
		public function SetPivasFormulaDrugListEvent(pivasFormulaDrugList:ArrayCollection)
		{
			super();
			_pivasFormulaDrugList = pivasFormulaDrugList;
		}
		
		public function get pivasFormulaDrugList():ArrayCollection
		{
			return _pivasFormulaDrugList;
		}
	}
}