package hk.org.ha.event.pms.main.reftable.mp {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshPrintMpQtyDurationConversionRptEvent extends AbstractTideEvent 
	{
		private var _qtyRetrieveSuccess:Boolean;
		private var _durationRetrieveSuccess:Boolean;
		
		public function RefreshPrintMpQtyDurationConversionRptEvent(qtyRetrieveSuccess:Boolean, durationRetrieveSuccess:Boolean):void 
		{
			super();
			_qtyRetrieveSuccess = qtyRetrieveSuccess;
			_durationRetrieveSuccess = durationRetrieveSuccess;
		}
		
		public function get qtyRetrieveSuccess():Boolean
		{
			return _qtyRetrieveSuccess;
		}
		
		public function get durationRetrieveSuccess():Boolean
		{
			return _durationRetrieveSuccess;
		}
	}
}