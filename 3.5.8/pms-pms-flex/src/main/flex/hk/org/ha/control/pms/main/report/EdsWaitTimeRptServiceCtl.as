package hk.org.ha.control.pms.main.report {
	import hk.org.ha.event.pms.main.report.PrintEdsWaitTimeRptEvent;
	import hk.org.ha.event.pms.main.report.RefreshEdsWaitTimeRptEvent;
	import hk.org.ha.event.pms.main.report.RetrieveEdsWaitTimeRptEvent;
	import hk.org.ha.model.pms.biz.report.EdsWaitTimeRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	
	[Bindable]
	[Name("edsWaitTimeRptServiceCtl", restrict="true")]
	public class EdsWaitTimeRptServiceCtl 
	{
		[In]
		public var edsWaitTimeRptService:EdsWaitTimeRptServiceBean;
		
		[Observer]
		public function retrieveEdsWaitTimeRpt(evt:RetrieveEdsWaitTimeRptEvent):void
		{
			In(Object(edsWaitTimeRptService).retrieveSuccess)
			edsWaitTimeRptService.retrieveEdsWaitTimeRpt(evt.selectedTab, evt.date, retrieveEdsWaitTimeRptResult);
		}
		
		private function retrieveEdsWaitTimeRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshEdsWaitTimeRptEvent(Object(edsWaitTimeRptService).retrieveSuccess));
		}
		
		[Observer]
		public function printEdsWaitTimeRpt(evt:PrintEdsWaitTimeRptEvent):void
		{
			edsWaitTimeRptService.printEdsWaitTimeRpt();
		}
		
	}
}
