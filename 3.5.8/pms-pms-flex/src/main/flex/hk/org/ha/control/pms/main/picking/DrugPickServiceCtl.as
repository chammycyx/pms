package hk.org.ha.control.pms.main.picking{
	
	import hk.org.ha.event.pms.main.picking.CallDrugPickingEvent;
	import hk.org.ha.event.pms.main.picking.CheckUserCodeEvent;
	import hk.org.ha.event.pms.main.picking.ReprintDispLabelEvent;
	import hk.org.ha.event.pms.main.picking.RetrieveDrugPickListEvent;
	import hk.org.ha.event.pms.main.picking.SetDrugPickSystemMessageEvent;
	import hk.org.ha.event.pms.main.picking.UpdateDispOrderItemStatusToPickedEvent;
	import hk.org.ha.model.pms.biz.picking.DrugPickServiceBean;
	import hk.org.ha.model.pms.persistence.reftable.Workstation;
	import hk.org.ha.model.pms.vo.picking.PickItemListSummary;
	import hk.org.ha.event.pms.main.picking.RefreshDrugPickViewEvent;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("drugPickServiceCtl", restrict="true")]
	public class DrugPickServiceCtl 
	{
		[In]
		public var drugPickService:DrugPickServiceBean;
		
		private var workstationId:Number;
		private var callDrugPick:Boolean;
		
		[Observer]
		public function updateDispOrderItemStatusToPicked(evt:UpdateDispOrderItemStatusToPickedEvent):void
		{
			workstationId = evt.workstationId;
			
			In(Object(drugPickService).updateSuccess)
			In(Object(drugPickService).resultMsg)
			drugPickService.updateDispOrderItemStatusToPicked(evt.ticketDate, evt.ticketNum, evt.itemNum, evt.userCode, evt.workstationId, updateDispOrderItemStatusToPickedResult);
		}
		
		private function updateDispOrderItemStatusToPickedResult(evt:TideResultEvent):void
		{
			if ( Object(drugPickService).updateSuccess) {
				evt.context.dispatchEvent(new RefreshDrugPickViewEvent(evt.result as PickItemListSummary, Object(drugPickService).resultMsg));
			} else {
				evt.context.dispatchEvent(new SetDrugPickSystemMessageEvent(Object(drugPickService).resultMsg));
			}
		}
		
		[Observer]
		public function checkUserCode(evt:CheckUserCodeEvent):void
		{
			callDrugPick = evt.callDrugPick;
			
			In(Object(drugPickService).resultMsg)
			drugPickService.checkUserCode(evt.userCode, checkUserCodeResult);
		}
		
		private function checkUserCodeResult(evt:TideResultEvent):void
		{
			var resultMsg:String = Object(drugPickService).resultMsg;
			if ( resultMsg && resultMsg != "" ) {
				evt.context.dispatchEvent(new SetDrugPickSystemMessageEvent(Object(drugPickService).resultMsg));
			} else if ( callDrugPick ) {
				evt.context.dispatchEvent(new CallDrugPickingEvent());
			}
		}
		
		[Observer]
		public function reprintDispLabel(evt:ReprintDispLabelEvent):void
		{
			drugPickService.reprintDispLabel(evt.dispOrderItemIdList, evt.printLang);
		}
		
		[Observer]
		public function retrieveDrugPickList(evt:RetrieveDrugPickListEvent):void
		{
			drugPickService.retrievePickingList(evt.workstationId, retrieveDrugPickListResult);
		}
		private var resultMsg:String;
		private function retrieveDrugPickListResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshDrugPickViewEvent(evt.result as PickItemListSummary));
		}
	}
}
