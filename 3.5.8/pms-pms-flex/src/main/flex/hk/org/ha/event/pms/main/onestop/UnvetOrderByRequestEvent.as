package hk.org.ha.event.pms.main.onestop
{
	import flash.events.Event;
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;

	public class UnvetOrderByRequestEvent extends AbstractTideEvent
	{
		private var _orderId:Number;
		private var _orderNum:String;
		
		public function UnvetOrderByRequestEvent(medOrderId:Number, orderNum:String)
		{
			super();
			_orderId = medOrderId;
			_orderNum = orderNum;
		}
		
		public function get orderId():Number {
			return _orderId;
		}
		
		public function get orderNum():String {
			return _orderNum;
		}
	}
}