package hk.org.ha.event.pms.main.reftable.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowFixedConcnPrepActivationMaintPopupEvent extends AbstractTideEvent 
	{
		private var _fixedConcnPrepActivationMaintPopupProp:FixedConcnPrepActivationMaintPopupProp;
		
		public function ShowFixedConcnPrepActivationMaintPopupEvent(fixedConcnPrepActivationMaintPopupProp:FixedConcnPrepActivationMaintPopupProp):void 
		{
			super();
			_fixedConcnPrepActivationMaintPopupProp = fixedConcnPrepActivationMaintPopupProp;
		}
		
		public function get fixedConcnPrepActivationMaintPopupProp():FixedConcnPrepActivationMaintPopupProp{
			return _fixedConcnPrepActivationMaintPopupProp;
		}
		
	}
}