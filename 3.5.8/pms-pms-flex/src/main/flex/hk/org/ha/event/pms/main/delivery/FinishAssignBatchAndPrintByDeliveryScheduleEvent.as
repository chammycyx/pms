package hk.org.ha.event.pms.main.delivery
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class FinishAssignBatchAndPrintByDeliveryScheduleEvent extends AbstractTideEvent
	{
		public function FinishAssignBatchAndPrintByDeliveryScheduleEvent()
		{
			super();
		}
	}
}