package hk.org.ha.control.pms.main.reftable {
	
	import hk.org.ha.event.pms.main.reftable.RefreshActiveProfileConfirmationPopupEvent;
	import hk.org.ha.event.pms.main.reftable.RetrievePmsSessionInfoListEvent;
	import hk.org.ha.model.pms.biz.reftable.PmsSessionInfoListServiceBean;
	import hk.org.ha.view.pms.main.reftable.popup.OperationModeMaintActiveProfileConfirmationPopup;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("pmsSessionInfoListServiceCtl", restrict="true")]
	public class PmsSessionInfoListServiceCtl 
	{
		[In]
		public var pmsSessionInfoListService:PmsSessionInfoListServiceBean;
		
		private var popupScreen:OperationModeMaintActiveProfileConfirmationPopup;
		
		[Observer]
		public function retrievePmsSessionInfoList(evt:RetrievePmsSessionInfoListEvent):void
		{
			popupScreen = evt.popupScreen;
			pmsSessionInfoListService.retrievePmsSessionInfoList(retrievePmsSessionInfoListResult);
		}
		
		private function retrievePmsSessionInfoListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshActiveProfileConfirmationPopupEvent(popupScreen));
		}
	}
}
