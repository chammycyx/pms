package hk.org.ha.event.pms.main.reftable.mp
{
	import hk.org.ha.model.pms.dms.persistence.DmDrug;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class UpdateMultiDoseConvSetListEvent extends AbstractTideEvent
	{
		private var _pmsMultiDoseConvSetList:ArrayCollection;
		
		private var _dmDrug:DmDrug;
		
		public function get dmDrug():DmDrug
		{
			return _dmDrug;
		}

		public function get pmsMultiDoseConvSetList():ArrayCollection
		{
			return _pmsMultiDoseConvSetList;
		}
		
		public function UpdateMultiDoseConvSetListEvent(pmsMultiDoseConvSetList:ArrayCollection, dmDrug:DmDrug):void 
		{
			super();
			_pmsMultiDoseConvSetList = pmsMultiDoseConvSetList;
			_dmDrug = dmDrug;
		}
		
	}
}