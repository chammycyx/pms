package hk.org.ha.event.pms.main.delivery
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class FinishAssignBatchAndPrintEvent extends AbstractTideEvent
	{
		public function FinishAssignBatchAndPrintEvent()
		{
			super();
		}
	}
}