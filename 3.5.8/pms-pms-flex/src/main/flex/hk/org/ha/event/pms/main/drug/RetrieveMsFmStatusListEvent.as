package hk.org.ha.event.pms.main.drug {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveMsFmStatusListEvent extends AbstractTideEvent 
	{
		private var _fmStatus:String;
		
		public function RetrieveMsFmStatusListEvent(fmStatus:String=null):void 
		{
			super();
			_fmStatus = fmStatus;
		}
		
		public function get fmStatus():String
		{
			return _fmStatus;
		}
	}
}