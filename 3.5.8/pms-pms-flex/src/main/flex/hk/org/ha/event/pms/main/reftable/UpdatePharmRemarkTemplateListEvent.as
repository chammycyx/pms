package hk.org.ha.event.pms.main.reftable {
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdatePharmRemarkTemplateListEvent extends AbstractTideEvent 
	{
		private var _pharmRemarkTemplateList:ListCollectionView;
		
		public function UpdatePharmRemarkTemplateListEvent(pharmRemarkTemplateList:ListCollectionView):void 
		{
			super();
			_pharmRemarkTemplateList = pharmRemarkTemplateList;
			
		}
		
		public function get pharmRemarkTemplateList():ListCollectionView
		{
			return _pharmRemarkTemplateList;
		}
	}
}