package hk.org.ha.event.pms.main.enquiry
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshSfiInvoiceSummaryListEvent extends AbstractTideEvent 
	{		
		private var _retrieveSuccess:Boolean; 
		private var _errMsg:String;
		
		public function RefreshSfiInvoiceSummaryListEvent(retrieveSuccess:Boolean, errMsg:String = ""):void
		{
			super();
			_retrieveSuccess = retrieveSuccess;
			_errMsg = errMsg;
		}
		
		public function get retrieveSuccess():Boolean
		{
			return _retrieveSuccess;
		}
		
		public function get errMsg():String
		{
			return _errMsg;
		}
	}
}