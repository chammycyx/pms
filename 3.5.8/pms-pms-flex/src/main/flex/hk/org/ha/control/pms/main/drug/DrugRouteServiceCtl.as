package hk.org.ha.control.pms.main.drug {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.drug.RetrieveIpOthersSiteMapListByDrugRouteCriteriaEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveIpSiteListByItemCodeEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveManualEntryOthersSiteMapListByDrugRouteCriteriaEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveManualEntrySiteListByItemCodeEvent;
	import hk.org.ha.model.pms.biz.drug.DrugRouteServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("drugRouteServiceCtl", restrict="true")]
	public class DrugRouteServiceCtl 
	{
		[In]
		public var drugRouteService:DrugRouteServiceBean;
		
		[In]
		public var ipSiteMapList:ArrayCollection;
		
		[In]
		public var ctx:Context;
		
		private var callBackEvent:Event;
		
		[Observer]
		public function retrieveManualEntrySiteListByItemCode(evt:RetrieveManualEntrySiteListByItemCodeEvent):void
		{		
			drugRouteService.retrieveManualEntrySiteListByItemCode(evt.itemCode, function(tideResultEvent:TideResultEvent):void {
				if (evt.callBackFunc != null) {
					evt.callBackFunc(tideResultEvent.result);
				}
			});
		}
		
		[Observer]
		public function retrieveIpSiteListByItemCode(evt:RetrieveIpSiteListByItemCodeEvent):void
		{		
			ctx.meta_updateDisabled = true;	
			drugRouteService.retrieveIpSiteListByItemCode(evt.itemCode, function(tideResultEvent:TideResultEvent):void {
				if (evt.callBackFunc != null ) {
					evt.callBackFunc(tideResultEvent.result);
				}
			});
		}
		
				
		[Observer]
		public function retrieveIpSiteMapListByDrugRouteCriteria(evt:RetrieveIpOthersSiteMapListByDrugRouteCriteriaEvent):void
		{		
			callBackEvent = evt.callBackEvent;			
			ctx.meta_updateDisabled = true;	
			drugRouteService.retrieveIpOthersSiteMapListByDrugRouteCriteria(evt.drugRouteCriteria, retrieveIpOthersSiteMapListByDrugRouteCriteriaResult);
		}
		
		private function retrieveIpOthersSiteMapListByDrugRouteCriteriaResult(evt:TideResultEvent):void 
		{
			if (callBackEvent != null) {
				evt.context.dispatchEvent( callBackEvent );
			}
		}
		
		[Observer]
		public function retrieveManualEntrySiteMapListByDrugRouteCriteria(evt:RetrieveManualEntryOthersSiteMapListByDrugRouteCriteriaEvent):void
		{
			ctx.ipSiteMapList = null;
			ctx.meta_updateDisabled = true;	
			drugRouteService.retrieveManualEntryOthersSiteMapListByDrugRouteCriteria(
				evt.drugRouteCriteria, 
				function(event:TideResultEvent):void {
					if (evt.callBackEvent != null) {
						event.context.dispatchEvent(evt.callBackEvent);
					}
				}
			);
		}
		
	}
}
