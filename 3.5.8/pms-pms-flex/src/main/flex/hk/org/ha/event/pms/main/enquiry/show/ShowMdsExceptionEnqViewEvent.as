package hk.org.ha.event.pms.main.enquiry.show {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowMdsExceptionEnqViewEvent extends AbstractTideEvent {
		
		private var _clearMessages:Boolean;
		
		public function ShowMdsExceptionEnqViewEvent(clearMessages:Boolean=true):void {
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}