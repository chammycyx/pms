package hk.org.ha.event.pms.main.inbox
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateUrgentFlagInMedOrderEvent extends AbstractTideEvent
	{
		private var _medOrderId:Number;
		private var _urgent:Boolean;
		private var _version:Number;
		
		public function UpdateUrgentFlagInMedOrderEvent(medOrderId:Number, urgent:Boolean, version:Number)
		{
			super();
			_medOrderId = medOrderId;
			_urgent = urgent;
			_version = version;
		}
		
		public function get medOrderId():Number
		{
			return _medOrderId;
		}
		
		public function get urgent():Boolean
		{
			return _urgent;
		}
		
		public function get version():Number
		{
			return _version;
		}
	}
}