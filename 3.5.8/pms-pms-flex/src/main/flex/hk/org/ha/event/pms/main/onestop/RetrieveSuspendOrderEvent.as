package hk.org.ha.event.pms.main.onestop {
		
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSuspendOrderEvent extends AbstractTideEvent 
	{			
		private var _startDate:Date;
		private var _callBack:Function;
		public function RetrieveSuspendOrderEvent(startDateValue:Date, callBackFunc:Function):void 
		{
			super();
			_startDate = startDateValue;
			_callBack = callBackFunc;
		}
		
		public function get startDate():Date {
			return _startDate;
		}
		
		public function get callBack():Function {
			return _callBack;
		}
	}
}