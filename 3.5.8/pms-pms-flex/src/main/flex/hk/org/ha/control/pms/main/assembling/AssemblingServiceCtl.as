package hk.org.ha.control.pms.main.assembling{
	
	import hk.org.ha.event.pms.main.assembling.RefreshDrugAssembleViewEvent;
	import hk.org.ha.event.pms.main.assembling.UpdateDispOrderItemStatusToAssembledEvent;
	import hk.org.ha.model.pms.biz.assembling.AssemblingServiceBean;
	import hk.org.ha.model.pms.vo.assembling.AssembleItem;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("assemblingServiceCtl", restrict="true")]
	public class AssemblingServiceCtl 
	{
		[In]
		public var assemblingService:AssemblingServiceBean;
		
		[Observer]
		public function updateDispOrderItemStatusToAssembled(evt:UpdateDispOrderItemStatusToAssembledEvent):void
		{
			In(Object(assemblingService).updateSuccess)
			In(Object(assemblingService).errMsg)
			assemblingService.updateDispOrderItemStatusToAssembled(evt.ticketDate, evt.ticketNum, evt.itemNum, updateDispOrderItemStatusToAssembledResult);
		}
		
		private function updateDispOrderItemStatusToAssembledResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshDrugAssembleViewEvent(Object(assemblingService).updateSuccess, Object(assemblingService).errMsg, evt.result as AssembleItem));
		}

	}
}
