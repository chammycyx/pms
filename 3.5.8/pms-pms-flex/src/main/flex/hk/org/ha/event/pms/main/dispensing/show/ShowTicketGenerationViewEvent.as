package hk.org.ha.event.pms.main.dispensing.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowTicketGenerationViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		public function ShowTicketGenerationViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}