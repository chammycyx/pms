package hk.org.ha.event.pms.main.vetting.popup
{
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class ShowPharmRemarkLogonPopupEvent extends AbstractTideEvent
	{
		private var _logonCallbackEvent:Event;
		private var _caller:String;
		private var _logonCancelCallbackFunc:Function;
		
		public function ShowPharmRemarkLogonPopupEvent(logonCallbackEvent:Event, caller:String, logonCancelCallbackFunc:Function=null)
		{
			super();
			_logonCallbackEvent = logonCallbackEvent;
			_caller = caller;
			_logonCancelCallbackFunc = logonCancelCallbackFunc;	// used when pharmacy remark is triggered by drug convert
		}
		
		public function get logonCallbackEvent():Event {
			return _logonCallbackEvent;
		}
		
		public function get caller():String {
			return _caller;
		}
		
		public function get logonCancelCallbackFunc():Function {
			return _logonCancelCallbackFunc;
		}
	}
}