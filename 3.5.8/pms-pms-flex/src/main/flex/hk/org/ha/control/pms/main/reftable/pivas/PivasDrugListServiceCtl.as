package hk.org.ha.control.pms.main.reftable.pivas
{
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.event.pms.main.reftable.pivas.RetrieveDrugByItemCodeEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.RetrievePivasDrugListBySolventCodeEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.RetrievePivasDrugListBySolventCodeHandlerEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.RetrievePivasDrugListInfoEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.RetrievePivasDrugListInfoHandlerEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.RetrievePivasDrugRptListEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.UpdatePivasDrugListEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.UpdatePivasDrugListHandlerEvent;
	import hk.org.ha.model.pms.biz.reftable.pivas.PivasDrugListServiceBean;
	import hk.org.ha.model.pms.dms.persistence.DmDrug;
	import hk.org.ha.model.pms.dms.vo.pivas.PivasDrugValidateResponse;
	import hk.org.ha.model.pms.vo.reftable.pivas.PivasDrugListInfo;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("pivasDrugListServiceCtl", restrict="true")]
	public class PivasDrugListServiceCtl
	{	
		[In]
		public var pivasDrugListService:PivasDrugListServiceBean;

		private var pivasDrugList:ArrayCollection;
		
		private var callbackFunc:Function;
		
		[Observer]
		public function retrieveDrugByItemCode(evt:RetrieveDrugByItemCodeEvent):void {
			callbackFunc = evt.callbackFunc;
			pivasDrugListService.retrieveDrugByItemCode(evt.itemCode, retrieveDrugByItemCodeResult);
		}
		
		private function retrieveDrugByItemCodeResult(evt:TideResultEvent):void {	
			callbackFunc(evt.result as DmDrug);
		}
		
		[Observer]
		public function retrievePivasDrugListInfo(evt:RetrievePivasDrugListInfoEvent):void {
			pivasDrugListService.retrievePivasDrugListInfo(evt.dmDrug, retrievePivasDrugListInfoResult);
		}
		
		private function retrievePivasDrugListInfoResult(evt:TideResultEvent):void {	
			evt.context.dispatchEvent( new RetrievePivasDrugListInfoHandlerEvent(evt.result as PivasDrugListInfo) );
		}
		
		[Observer]
		public function retrievePivasDrugListBySolventCode(evt:RetrievePivasDrugListBySolventCodeEvent):void {
			pivasDrugListService.retrievePivasDrugListBySolventCode(evt.solventCode, retrievePivasDrugListBySolventCodeResult);
		}
		
		private function retrievePivasDrugListBySolventCodeResult(evt:TideResultEvent):void {	
			evt.context.dispatchEvent( new RetrievePivasDrugListBySolventCodeHandlerEvent(evt.result as ArrayCollection) );
		}
		
		[Observer]
		public function updatePivasDrugList(evt:UpdatePivasDrugListEvent):void {
			pivasDrugList = evt.pivasDrugList;
			pivasDrugListService.updatePivasDrugList(evt.pivasDrugList, evt.pivasDrugValidateCriteria, updatePivasDrugListResult);
		}
		
		private function updatePivasDrugListResult(evt:TideResultEvent):void {	
			evt.context.dispatchEvent( new UpdatePivasDrugListHandlerEvent(pivasDrugList, evt.result as PivasDrugValidateResponse) );
		}
		
		[Observer]
		public function retrievePivasDrugRptList(evt:RetrievePivasDrugRptListEvent):void {
			callbackFunc = evt.callbackFunc;
			pivasDrugListService.retrievePivasDrugRptList(retrievePivasDrugRptListResult);
		}
		
		private function retrievePivasDrugRptListResult(evt:TideResultEvent):void {
			callbackFunc(evt.result as Boolean);
		}
	}
}