package hk.org.ha.control.pms.main.reftable.mp
{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveMpWorkingStoreListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.UpdateMpWorkingStoreListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.SetMpWorkingStorePropGridFocusEvent;
	import hk.org.ha.event.pms.main.reftable.mp.MpWorkingStorePropListSaveSuccessfulEvent;
	import hk.org.ha.model.pms.biz.reftable.mp.MpWorkingStoreListServiceBean;
	
	import org.granite.tide.events.TideEvent;
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("mpWorkingStoreListServiceCtl", restrict="true")]
	public class MpWorkingStoreListServiceCtl {
		
	[In]
	public var mpWorkingStoreListService:MpWorkingStoreListServiceBean;
		
	[Observer]
	public function retrieveMpWorkingStoreList(evt:RetrieveMpWorkingStoreListEvent):void
	{
		mpWorkingStoreListService.retrieveMpWorkStorePropList(retrieveMpWorkingStoreListResult);
	}
	
	public function retrieveMpWorkingStoreListResult(evt:TideEvent):void
	{		
		evt.context.dispatchEvent(new SetMpWorkingStorePropGridFocusEvent());
	}
	
	[Observer]
	public function updateMpWorkingStoreList(evt:UpdateMpWorkingStoreListEvent):void
	{
		mpWorkingStoreListService.updateMpWorkingStorePropList(evt.workingStorePropList,mpWorkingStorePropListSaveSuccessResult);
	}
	
	public function mpWorkingStorePropListSaveSuccessResult(evt:TideEvent):void
	{
		evt.context.dispatchEvent(new MpWorkingStorePropListSaveSuccessfulEvent());
	}
	
}
}