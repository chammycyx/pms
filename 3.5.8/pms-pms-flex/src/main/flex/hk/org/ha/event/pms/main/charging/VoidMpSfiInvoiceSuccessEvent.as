package hk.org.ha.event.pms.main.charging {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class VoidMpSfiInvoiceSuccessEvent extends AbstractTideEvent 
	{
		private var _invoiceNum:String;
		public function VoidMpSfiInvoiceSuccessEvent(invoiceNum:String):void 
		{
			super();		
			_invoiceNum = invoiceNum;
		}
		
		public function get invoiceNum():String {
			return _invoiceNum;
		}
	}
}