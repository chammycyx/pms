package hk.org.ha.control.pms.main.dispensing{
	
	import hk.org.ha.event.pms.main.dispensing.PrintHkidBarcodeLabelEvent;
	import hk.org.ha.event.pms.main.dispensing.RefreshHkidBarcodeLabelEvent;
	import hk.org.ha.event.pms.main.dispensing.RemoveHkidBarcodeLabelEvent;
	import hk.org.ha.event.pms.main.dispensing.RetrieveHkidBarcodeLabelEvent;
	import hk.org.ha.event.pms.main.dispensing.UpdateHkidBarcodeLabelEvent;
	import hk.org.ha.event.pms.main.reftable.RefreshRemoveHkidBarcodeLabelEvent;
	import hk.org.ha.event.pms.main.reftable.RefreshUpdateHkidBarcodeLabelEvent;
	import hk.org.ha.model.pms.biz.label.HkidBarcodeLabelServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("hkidBarcodeLabelServiceCtl", restrict="true")]
	public class HkidBarcodeLabelServiceCtl 
	{
		[In]
		public var hkidBarcodeLabelService:HkidBarcodeLabelServiceBean;
		
		[Observer]
		public function retrieveHkidBarcodeLabel(evt:RetrieveHkidBarcodeLabelEvent):void
		{
			In(Object(hkidBarcodeLabelService).retrieveSuccess)
			hkidBarcodeLabelService.retrieveHkidBarcodeLabel(evt.name, retrieveHkidBarcodeLabelResult);
		}
		
		private function retrieveHkidBarcodeLabelResult(evt:TideResultEvent):void
		{	
			evt.context.dispatchEvent(new RefreshHkidBarcodeLabelEvent(Object(hkidBarcodeLabelService).retrieveSuccess));
		}
		
		[Observer]
		public function printHkidBarcodeLabel(evt:PrintHkidBarcodeLabelEvent):void
		{
			hkidBarcodeLabelService.printHkidBarcodeLabel(evt.hkidBarcodeLabel);
		}
		
		[Observer]
		public function updateHkidBarcodeLabel(evt:UpdateHkidBarcodeLabelEvent):void
		{
			hkidBarcodeLabelService.updateHkidBarcodeLabel(evt.hkidBarcodeLabel, updateHkidBarcodeLabelResult);
		}
		
		private function updateHkidBarcodeLabelResult(evt:TideResultEvent):void
		{	
			evt.context.dispatchEvent(new RefreshUpdateHkidBarcodeLabelEvent());
		}
		
		[Observer]
		public function removeHkidBarcodeLabel(evt:RemoveHkidBarcodeLabelEvent):void
		{
			hkidBarcodeLabelService.removeHkidBarcodeLabel(evt.name, removeHkidBarcodeLabelResult);
		}
		
		private function removeHkidBarcodeLabelResult(evt:TideResultEvent):void
		{	
			evt.context.dispatchEvent(new RefreshRemoveHkidBarcodeLabelEvent());
		}

	}
}
