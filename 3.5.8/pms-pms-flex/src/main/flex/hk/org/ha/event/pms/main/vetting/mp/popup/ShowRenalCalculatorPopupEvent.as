package hk.org.ha.event.pms.main.vetting.mp.popup
{
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowRenalCalculatorPopupEvent extends AbstractTideEvent
	{
		
		private var _readOnlyFlag:Boolean;
		
		public function ShowRenalCalculatorPopupEvent(readOnlyFlag:Boolean)
		{
			super();
			_readOnlyFlag = readOnlyFlag;
		}
		
		public function get readOnlyFlag():Boolean {
			return _readOnlyFlag;
		}
	}
}