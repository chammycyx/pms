package hk.org.ha.view.pms.main.inbox
{
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.ClassFactory;
	import hk.org.ha.view.pms.main.inbox.EnhancedDataGridHeaderRendererFactory;
	
	public class EnhancedDataGridColumn extends mx.controls.dataGridClasses.DataGridColumn
	{
		[Bindable]
		public var headerToolTip:String;
		
		public function EnhancedDataGridColumn(columnName:String=null)
		{
			super(columnName);
			setStyle("paddingLeft", 5);
			setStyle("paddingRight", 5);			
			headerRenderer = new EnhancedDataGridHeaderRendererFactory(this);
		}
	}
}