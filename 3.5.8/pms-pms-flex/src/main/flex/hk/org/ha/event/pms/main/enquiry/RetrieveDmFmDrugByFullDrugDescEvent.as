package hk.org.ha.event.pms.main.enquiry
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDmFmDrugByFullDrugDescEvent extends AbstractTideEvent 
	{		
		private var _fullDrugDesc:String;

		public function RetrieveDmFmDrugByFullDrugDescEvent(fullDrugDesc:String):void 
		{
			super();
			_fullDrugDesc = fullDrugDesc;
		}
		
		public function get fullDrugDesc():String
		{
			return _fullDrugDesc;
		}
	}
}