package hk.org.ha.event.pms.main.onestop.popup {
	
	import hk.org.ha.model.pms.persistence.disp.Patient;
	import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowOneStopEntryPrescriptionSummaryPopupEvent extends AbstractTideEvent 
	{		
		private var _oneStopOrderType:OneStopOrderType;
		private var _orderId:Number;
		private var _showPreVetView:Boolean;
		private var _orderNum:String;
		private var _medOrderVersion:Number;
		private var _prescriptionOrderList:ArrayCollection;
		private var _patient:Patient;
		
		public function ShowOneStopEntryPrescriptionSummaryPopupEvent(oneStopOrderType:OneStopOrderType, orderId:Number, showPreVetView:Boolean,
																	  orderNum:String, medOrderVersion:Number, 
																	  prescriptionOrderList:ArrayCollection, patient:Patient=null):void 
		{
			super();
			this._oneStopOrderType = oneStopOrderType;
			this._orderId = orderId;
			_orderNum = orderNum;
			_showPreVetView = showPreVetView;
			_medOrderVersion = medOrderVersion;
			_prescriptionOrderList = prescriptionOrderList;
			_patient = patient;
		}
		
		public function get oneStopOrderType():OneStopOrderType
		{
			return _oneStopOrderType;
		}
		
		public function get orderId():Number
		{
			return _orderId;
		}
		
		public function get showPreVetView():Boolean
		{
			return _showPreVetView;
		}
		
		public function get medOrderVersion():Number {
			return _medOrderVersion;
		}
		
		public function get prescriptionOrderList():ArrayCollection {
			return _prescriptionOrderList;
		}
		
		public function get orderNum():String
		{
			return _orderNum;
		}
		
		public function get patient():Patient {
			return _patient;
		}
	}
}