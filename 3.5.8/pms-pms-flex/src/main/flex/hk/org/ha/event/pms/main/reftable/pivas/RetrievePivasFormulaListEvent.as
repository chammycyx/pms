package hk.org.ha.event.pms.main.reftable.pivas {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrievePivasFormulaListEvent extends AbstractTideEvent {
		
		private var _drugKey:Number;
		private var _siteCode:String;
		private var _diluentCode:String;
		private var _pivasDosageUnit:String;
		private var _concn:Number;
		private var _strength:String;
		private var _rate:Number;
		private var _callback:Function;

		public function RetrievePivasFormulaListEvent(drugKey:Number, siteCode:String, diluentCode:String, pivasDosageUnit:String, concn:Number, strength:String, rate:Number, callback:Function) {
			super();
			_drugKey = drugKey;
			_siteCode = siteCode;
			_diluentCode = diluentCode;
			_pivasDosageUnit = pivasDosageUnit;
			_concn = concn;
			_strength = strength;
			_rate = rate;
			_callback = callback;
		}

		public function get drugKey():Number {
			return _drugKey;
		}
		
		public function get siteCode():String {
			return _siteCode;
		}
		
		public function get diluentCode():String {
			return _diluentCode;
		}
		
		public function get pivasDosageUnit():String {
			return _pivasDosageUnit;
		}
		
		public function get concn():Number {
			return _concn;
		}
		
		public function get strength():String {
			return _strength;
		}
		
		public function get rate():Number {
			return _rate;
		}

		public function get callback():Function {
			return _callback;
		}
	}
}