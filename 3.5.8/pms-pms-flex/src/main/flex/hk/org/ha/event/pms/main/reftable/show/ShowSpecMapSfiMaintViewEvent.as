package hk.org.ha.event.pms.main.reftable.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowSpecMapSfiMaintViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		public function ShowSpecMapSfiMaintViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}