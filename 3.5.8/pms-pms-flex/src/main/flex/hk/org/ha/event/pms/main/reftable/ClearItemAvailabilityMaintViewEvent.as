package hk.org.ha.event.pms.main.reftable {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ClearItemAvailabilityMaintViewEvent extends AbstractTideEvent 
	{		
		public function ClearItemAvailabilityMaintViewEvent():void 
		{
			super();
		}
	}
}