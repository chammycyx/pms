package hk.org.ha.event.pms.main.charging {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
	
	public class ShowOutstandingPurchaseRequestByIndEvent extends AbstractTideEvent 
	{	
		private var _mpPoi:MedProfilePoItem;
		
		public function ShowOutstandingPurchaseRequestByIndEvent(mpPoi:MedProfilePoItem):void 
		{
			super();
			_mpPoi = mpPoi;
		}
		
		public function get mpPoi():MedProfilePoItem {
			return _mpPoi;
		}
	}
}