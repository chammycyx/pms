package hk.org.ha.event.pms.main.reftable.pivas {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ExportPivasFormulaEvent extends AbstractTideEvent {
		
		private var _callback:Function;
		
		public function ExportPivasFormulaEvent(callback:Function) {
			super();
			_callback = callback;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}