package hk.org.ha.event.pms.main.report {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class PrintMdsExceptionRptEvent extends AbstractTideEvent {
		
		public function PrintMdsExceptionRptEvent() {
			super();
		}
	}
}