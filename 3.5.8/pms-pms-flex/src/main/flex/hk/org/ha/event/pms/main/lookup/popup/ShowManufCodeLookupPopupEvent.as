package  hk.org.ha.event.pms.main.lookup.popup{
	
	import hk.org.ha.event.pms.main.lookup.popup.ManufCodeLookupPopupProp;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowManufCodeLookupPopupEvent extends AbstractTideEvent 
	{
		private var _manufCodeLookupPopupProp:ManufCodeLookupPopupProp;
		
		public function ShowManufCodeLookupPopupEvent(manufCodeLookupPopupProp:ManufCodeLookupPopupProp):void 
		{
			super();
			_manufCodeLookupPopupProp = manufCodeLookupPopupProp;
		}
		
		public function get manufCodeLookupPopupProp():ManufCodeLookupPopupProp{
			return _manufCodeLookupPopupProp;
		}
	}
}