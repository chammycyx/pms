package hk.org.ha.control.pms.main.reftable.mp {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.reftable.mp.ClearWardConfigMaintViewEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RefreshMedProfilesEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RefreshWardConfigEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveWardConfigEvent;
	import hk.org.ha.event.pms.main.reftable.mp.RetrieveWardListEvent;
	import hk.org.ha.event.pms.main.reftable.mp.UpdateWardConfigEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.ShowLoadingPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.lookup.RefreshLookupPopupDataProviderEvent;
	import hk.org.ha.model.pms.biz.reftable.WardListServiceBean;
	import hk.org.ha.model.pms.biz.reftable.mp.WardConfigServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("wardConfigServiceCtl", restrict="true")]
	public class WardConfigServiceCtl 
	{
		[In]
		public var wardConfigService:WardConfigServiceBean;
		
		[In]
		public var wardListService:WardListServiceBean;
		
		[In]
		public var wardListByPrefixWardCode:ArrayCollection;
		
		private var _wardCode:String;
				
		[Observer]
		public function retrieveWardConfig(evt:RetrieveWardConfigEvent):void
		{
			_wardCode = evt.wardCode;
			wardConfigService.retrieveWardConfig(evt.wardCode, retrieveWardConfigResult);
		}
		
		private function retrieveWardConfigResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new RefreshWardConfigEvent());
		}
		
		[Observer]
		public function updateWardConfig(evt:UpdateWardConfigEvent):void
		{
			wardConfigService.updateWardConfig(evt.wardConfig, updateWardConfigResult);
		}
		
		private function updateWardConfigResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new ClearWardConfigMaintViewEvent());
		}
		
		[Observer]
		public function refreshMedProfiles(evt:RefreshMedProfilesEvent):void {
			dispatchEvent(new ShowLoadingPopupEvent("Refreshing ..."));
			wardConfigService.refreshMedProfiles(evt.wardConfig, evt.refreshPasInfo, evt.refreshMrInfo, closePopup, closePopup);
		}
		
		[Observer]
		public function retrieveWardList(evt:RetrieveWardListEvent):void
		{
			wardListService.retrieveWardListByPrefixWardCode(evt.prefixWardCode, retrieveWardListResult);
		}
		
		private function retrieveWardListResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new RefreshLookupPopupDataProviderEvent(wardListByPrefixWardCode));
		}
		
		public function closePopup(evt:Event):void {
			dispatchEvent(new CloseLoadingPopupEvent());			
		}
	}
}
