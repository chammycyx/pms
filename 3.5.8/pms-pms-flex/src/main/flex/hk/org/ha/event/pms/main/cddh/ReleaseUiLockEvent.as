package hk.org.ha.event.pms.main.cddh {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ReleaseUiLockEvent extends AbstractTideEvent 
	{
		public function ReleaseUiLockEvent():void 
		{
			super();
		}
	}
}