package hk.org.ha.event.pms.main.charging {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RefreshMpSfiInvoiceViewEvent extends AbstractTideEvent 
	{	
		private var _messageCode:String;
		private var _success:Boolean;
		
		public function RefreshMpSfiInvoiceViewEvent(messageCode:String, success:Boolean):void 
		{
			super();
			_messageCode = messageCode;
			_success = success;
		}
		
		public function get messageCode():String {
			return _messageCode;
		}
		
		public function get success():Boolean {
			return _success;
		}
	}
}