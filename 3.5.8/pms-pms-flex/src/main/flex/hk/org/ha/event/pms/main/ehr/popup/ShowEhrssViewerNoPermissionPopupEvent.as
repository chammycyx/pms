package hk.org.ha.event.pms.main.ehr.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowEhrssViewerNoPermissionPopupEvent extends AbstractTideEvent 
	{			
		
		public function ShowEhrssViewerNoPermissionPopupEvent():void 
		{
			super();
		}
	}
}