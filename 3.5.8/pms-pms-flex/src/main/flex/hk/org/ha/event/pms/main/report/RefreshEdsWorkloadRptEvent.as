package hk.org.ha.event.pms.main.report {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshEdsWorkloadRptEvent extends AbstractTideEvent 
	{
		private var _retrieveSuccess:Boolean;
		
		public function RefreshEdsWorkloadRptEvent(retrieveSuccess:Boolean):void 
		{
			super();
			_retrieveSuccess = retrieveSuccess;
		}
		
		public function get retrieveSuccess():Boolean
		{
			return _retrieveSuccess;
		}
		
	}
}