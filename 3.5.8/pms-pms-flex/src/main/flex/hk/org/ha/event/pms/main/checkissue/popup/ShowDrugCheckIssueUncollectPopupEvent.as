package hk.org.ha.event.pms.main.checkissue.popup {
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDrugCheckIssueUncollectPopupEvent extends AbstractTideEvent 
	{
		private var _uncollectPrescList:ArrayCollection;
		private var _clearMessages:Boolean;
		private var _closeFunc:Function;
		
		public function ShowDrugCheckIssueUncollectPopupEvent(uncollectPrescList:ArrayCollection, closeFunc:Function=null, clearMessages:Boolean=true):void 
		{
			super();
			_uncollectPrescList = uncollectPrescList;
			_clearMessages = clearMessages;
			_closeFunc = closeFunc;
		}
		
		public function get uncollectPrescList():ArrayCollection {
			return _uncollectPrescList;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
		
		public function get closeFunc():Function {
			return _closeFunc;
		}
	}
}