package hk.org.ha.control.pms.main.vetting.mp
{
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	import mx.core.IFlexDisplayObject;
	import mx.core.UIComponent;
	import mx.managers.PopUpManager;
	
	import hk.org.ha.event.pms.main.drug.show.ShowDrugSearchViewEvent;
	import hk.org.ha.event.pms.main.inbox.show.ShowPharmInboxViewEvent;
	import hk.org.ha.event.pms.main.vetting.mp.AddMrAnnotationToCurrentMedication;
	import hk.org.ha.event.pms.main.vetting.mp.AddMrAnnotationToOnhandMedication;
	import hk.org.ha.event.pms.main.vetting.mp.CheckConflictExistsEvent;
	import hk.org.ha.event.pms.main.vetting.mp.CheckProfileHasLabelGenerationWithinToday;
	import hk.org.ha.event.pms.main.vetting.mp.FinishAddMrAnnotationToCurrentMedicationEvent;
	import hk.org.ha.event.pms.main.vetting.mp.FinishAddMrAnnotationToOnhandMedicationEvent;
	import hk.org.ha.event.pms.main.vetting.mp.FinishCheckSuspendItemReplenishEvent;
	import hk.org.ha.event.pms.main.vetting.mp.RecalculateQtyEvent;
	import hk.org.ha.event.pms.main.vetting.mp.ReceiveMarRptEvent;
	import hk.org.ha.event.pms.main.vetting.mp.ReceiveRecalculatedQtyEvent;
	import hk.org.ha.event.pms.main.vetting.mp.ReleaseMedProfileEvent;
	import hk.org.ha.event.pms.main.vetting.mp.RetrieveCurrentMedicationListEvent;
	import hk.org.ha.event.pms.main.vetting.mp.RetrieveDeliveryItemListByIdListEvent;
	import hk.org.ha.event.pms.main.vetting.mp.RetrieveInactiveListEvent;
	import hk.org.ha.event.pms.main.vetting.mp.RetrieveMarRptEvent;
	import hk.org.ha.event.pms.main.vetting.mp.RetrieveOnhandMedicationListEvent;
	import hk.org.ha.event.pms.main.vetting.mp.UpdateMedProfileEvent;
	import hk.org.ha.event.pms.main.vetting.mp.VetMedProfileEvent;
	import hk.org.ha.event.pms.main.vetting.mp.show.ShowManualEntryItemEditViewEvent;
	import hk.org.ha.event.pms.main.vetting.mp.show.ShowVettingChemoViewEvent;
	import hk.org.ha.event.pms.main.vetting.mp.show.ShowVettingCurrentMedicationViewEvent;
	import hk.org.ha.event.pms.main.vetting.mp.show.ShowVettingOnhandMedicationViewEvent;
	import hk.org.ha.event.pms.main.vetting.mp.show.ShowVettingViewEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.ShowLoadingPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	import hk.org.ha.model.pms.biz.mar.MarServiceBean;
	import hk.org.ha.model.pms.biz.mr.CurrentMedicationServiceBean;
	import hk.org.ha.model.pms.biz.mr.OnhandMedicationServiceBean;
	import hk.org.ha.model.pms.biz.vetting.mp.CheckConflictServiceBean;
	import hk.org.ha.model.pms.biz.vetting.mp.InactiveListServiceBean;
	import hk.org.ha.model.pms.biz.vetting.mp.ItemRecalculateServiceBean;
	import hk.org.ha.model.pms.biz.vetting.mp.MedProfileVettingServiceBean;
	import hk.org.ha.model.pms.biz.vetting.mp.OrderAmendmentHistoryServiceBean;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
	import hk.org.ha.model.pms.persistence.medprofile.MpRenalEcrclLog;
	import hk.org.ha.model.pms.persistence.medprofile.MpRenalMessage;
	import hk.org.ha.model.pms.persistence.medprofile.Replenishment;
	import hk.org.ha.model.pms.persistence.medprofile.ReplenishmentItem;
	import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
	import hk.org.ha.model.pms.udt.medprofile.MedProfileTransferAlertType;
	import hk.org.ha.model.pms.udt.medprofile.MedProfileType;
	import hk.org.ha.model.pms.udt.medprofile.ReplenishmentStatus;
	import hk.org.ha.model.pms.vo.medprofile.CheckConflictItem;
	
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("inpatientVettingServiceCtl", restrict="true")]
	public class MedProfileVettingServiceCtl
	{
		[In]
		public var ctx:Context;
		
		[In]
		public var medProfileVettingService:MedProfileVettingServiceBean;
		
		[In]
		public var inactiveListService:InactiveListServiceBean;
		
		[In]
		public var orderAmendmentHistoryService:OrderAmendmentHistoryServiceBean;
		
		[In]
		public var itemRecalculateService:ItemRecalculateServiceBean;
		
		[In]
		public var checkConflictService:CheckConflictServiceBean;
		
		[In]
		public var onhandMedicationService:OnhandMedicationServiceBean;
		
		[In]
		public var currentMedicationService:CurrentMedicationServiceBean;
		
		[In]
		public var marService:MarServiceBean;
		
		[In]
		public var medProfile:MedProfile;
		
		[In]
		public var mpRenalEcrclLogList:ListCollectionView;
		
		[In]
		public var mpRenalMessageList:ListCollectionView; 
		
		public function MedProfileVettingServiceCtl()
		{
		}
		
		[Observer]
		public function vetMedProfile(evt:VetMedProfileEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Loading PAS Information ..."));
			medProfileVettingService.vetMedProfile(evt.medProfile, vetMedProfileResult, closePopup);
		}
		
		public function vetMedProfileResult(evt:TideResultEvent):void
		{
			if (ctx.medProfileType == MedProfileType.Chemo && 
				(ctx.medProfile != null && (ctx.medProfile.transferAlertType == null || ctx.medProfile.transferAlertType == MedProfileTransferAlertType.None))) {
				dispatchEvent(new ShowVettingChemoViewEvent(true));
			} else if (ctx.medProfile == null || !ctx.medProfile.inactivePasWardFlag) {
				dispatchEvent(new ShowVettingViewEvent(true));
			} else {
				showDrugSearchView();
			}
			dispatchEvent(new CloseLoadingPopupEvent());
		}
		
		private function showUpdateProfileMsgPopup(messageCode:String, callbackFunction:Function = null):void {
			var prop:SystemMessagePopupProp = new SystemMessagePopupProp(messageCode);
			prop.setOkButtonOnly = true;
			if( callbackFunction != null ){
				prop.okHandler = callbackFunction;
			}
			dispatchEvent(new RetrieveSystemMessageEvent(prop));
		}
		
		private function showDrugSearchView():void {
			var showDrugSearchViewEvent:ShowDrugSearchViewEvent = new ShowDrugSearchViewEvent();
			showDrugSearchViewEvent.drugSearchSource = DrugSearchSource.MpVetting;
			showDrugSearchViewEvent.firstItem = true;
			showDrugSearchViewEvent.okCallbackEvent = new ShowManualEntryItemEditViewEvent(new ListCollectionView, new MedProfileItem);
			showDrugSearchViewEvent.cancelCallbackEvent = null;
			dispatchEvent(showDrugSearchViewEvent);
		}
		
		private function clearPharmLineRegimenSiteList(mpiList:ListCollectionView):void {
			for each (var mpi:MedProfileItem in mpiList) {
				for each (var mppi:MedProfilePoItem in mpi.medProfileMoItem.medProfilePoItemList) {
					mppi.regimen.clearSiteList();
				}
			}
		}
		
		[Observer]
		public function releaseMedProfile(evt:ReleaseMedProfileEvent):void
		{
			medProfileVettingService.releaseMedProfile(releaseMedProfileResult);
		}
		
		public function releaseMedProfileResult(evt:TideResultEvent):void
		{
			dispatchEvent(new ShowPharmInboxViewEvent(true));
		}
		
		[Observer]
		public function updateMedProfile(evt:UpdateMedProfileEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Saving Profile ..."));
			clearPharmLineRegimenSiteList(evt.medProfileItemList);
			medProfileVettingService.updateMedProfile(evt.medProfile, evt.medProfileItemList, evt.replenishmentList,
				evt.purchaseRequestList, getNewMpRenalEcrclLog(), getNewMpRenalMessage(), updateMedProfileResult, function(faultEvt:TideFaultEvent):void {
					if (evt.faultCallBack != null) {
						evt.faultCallBack();
					}
					closePopup(faultEvt);
				});
		}
		
		public function updateMedProfileResult(evt:TideResultEvent):void
		{
			dispatchEvent(new CloseLoadingPopupEvent());

			var messageCode:String = evt.result as String;
			if ( messageCode.length > 0 ) {
				showUpdateProfileMsgPopup(messageCode, function(evt:Event):void{
					dispatchEvent(new ShowPharmInboxViewEvent(true));
					PopUpManager.removePopUp((evt.currentTarget as UIComponent).parentDocument as IFlexDisplayObject);
				});
			}else{
				dispatchEvent(new ShowPharmInboxViewEvent(true));
			}
		}
		
		[Observer]
		public function retrieveInactiveList(evt:RetrieveInactiveListEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Loading ..."));
			inactiveListService.retrieveInactiveList(
				function(resultEvt:TideResultEvent):void {
					if (evt.callBack != null) {
						evt.callBack();
					}
					closePopup(resultEvt);
				}, closePopup
			);
		}
		
		[Observer]
		public function retrieveDeliveryItemListByIdList(evt:RetrieveDeliveryItemListByIdListEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Loading ..."));
			orderAmendmentHistoryService.retriveDeliveryItemList(evt.deliveryItemIdList,
				function(resultEvt:TideResultEvent):void {
					if (evt.callback != null) {
						evt.callback(evt.medProfileMoItem, resultEvt.result);
					}
					closePopup(resultEvt);
				}, closePopup
			);
		}
		
		[Observer]
		public function recalculateQty(evt:RecalculateQtyEvent):void
		{
			itemRecalculateService.recalculateQty(evt.medProfilePoItem, 
				function(resultEvt:TideResultEvent):void {
					if (evt.callBackFunc != null) {
						evt.callBackFunc.call(null, resultEvt.result);
					} else {
						dispatchEvent(new ReceiveRecalculatedQtyEvent(resultEvt.result as MedProfilePoItem));
					}
				}
			);
		}
		
		[Observer]
		public function checkConflictExists(evt:CheckConflictExistsEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Checking Profile ..."));
			
			var checkConflictItem:CheckConflictItem;
			var medProfileCheckConflictItemList:ArrayCollection = new ArrayCollection();
			
			for each (var medProfileItem:MedProfileItem in evt.medProfileItemList) {
				checkConflictItem = new CheckConflictItem();
				checkConflictItem.medProfileItemId = medProfileItem.id;
				checkConflictItem.orgItemNum = medProfileItem.orgItemNum;
				checkConflictItem.itemNum = medProfileItem.medProfileMoItem.itemNum;
				checkConflictItem.endDate = medProfileItem.medProfileMoItem.endDate;
				medProfileCheckConflictItemList.addItem(checkConflictItem);
			}
			
			var replenishmentCheckConflictItemList:ArrayCollection = new ArrayCollection();
			for each (var replenishment:Replenishment in evt.replenishmentList) {
				if (replenishment.status != ReplenishmentStatus.Outstanding || !replenishment.dispFlag) {
					continue;
				}

				checkConflictItem = new CheckConflictItem();
				checkConflictItem.medProfileItemId = replenishment.medProfileMoItem.medProfileItem.id;
				checkConflictItem.orgItemNum = replenishment.medProfileMoItem.medProfileItem.orgItemNum;
				checkConflictItem.itemNum = replenishment.medProfileMoItem.itemNum;
				checkConflictItem.endDate = replenishment.medProfileMoItem.endDate;
				checkConflictItem.itemCodeList = new ArrayCollection();
				for each (var replenishmentItem:ReplenishmentItem in replenishment.replenishmentItemList) {
					if (replenishmentItem.issueQty != 0) {
						checkConflictItem.itemCodeList.addItem(replenishmentItem.medProfilePoItem.itemCode);
					}
				}
				replenishmentCheckConflictItemList.addItem(checkConflictItem);
			}
			
			checkConflictService.checkConflictExists(evt.medProfile.orderNum, medProfileCheckConflictItemList, replenishmentCheckConflictItemList,
				checkConflictExistsResult, closePopup);
		}
		
		public function checkConflictExistsResult(evt:TideResultEvent):void
		{
			dispatchEvent(new CloseLoadingPopupEvent());
			dispatchEvent(new FinishCheckSuspendItemReplenishEvent());
		}
		
		[Observer]
		public function retrieveOnhandMedicationList(evt:RetrieveOnhandMedicationListEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Loading Onhand Medication ..."));
			onhandMedicationService.retrieveOnhandMedicationList(retrieveOnhandMedicationListResult, closePopup);
		}
		
		public function retrieveOnhandMedicationListResult(evt:TideResultEvent):void
		{
			dispatchEvent(new ShowVettingOnhandMedicationViewEvent());
			dispatchEvent(new CloseLoadingPopupEvent());
		}
		
		[Observer]
		public function retrieveCurrentMedicationList(evt:RetrieveCurrentMedicationListEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Loading Current Medication ..."));
			currentMedicationService.retrieveCurrentMedicationList(retrieveCurrentMedicationListResult, closePopup);
		}
		
		public function retrieveCurrentMedicationListResult(evt:TideResultEvent):void
		{
			dispatchEvent(new ShowVettingCurrentMedicationViewEvent());
			dispatchEvent(new CloseLoadingPopupEvent());
		}
		
		[Observer]
		public function addMrAnnotationToOnhandMedication(evt:AddMrAnnotationToOnhandMedication):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Creating Annotation ..."));
			onhandMedicationService.addMrAnnotationToOnhandMedication(evt.patHospCode, evt.orderNum, evt.itemNum,
				evt.mrAnnotation, addMrAnnotationToOnhandMedicationResult, closePopup);
		}
		
		public function addMrAnnotationToOnhandMedicationResult(evt:TideResultEvent):void
		{
			dispatchEvent(new FinishAddMrAnnotationToOnhandMedicationEvent(evt.result));
			dispatchEvent(new CloseLoadingPopupEvent());
		}
		
		[Observer]
		public function addMrAnnotationToCurrentMedication(evt:AddMrAnnotationToCurrentMedication):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Creating Annotation ..."));
			currentMedicationService.addMrAnnotationToCurrentMedication(evt.patHospCode, evt.orderNum, evt.itemNum,
				evt.mrAnnotation, addMrAnnotationToCurrentMedicationResult, closePopup);
		}
		
		public function addMrAnnotationToCurrentMedicationResult(evt:TideResultEvent):void
		{
			dispatchEvent(new FinishAddMrAnnotationToCurrentMedicationEvent(evt.result));
			dispatchEvent(new CloseLoadingPopupEvent());
		}
		
		[Observer]
		public function retrieveMarRpt(evt:RetrieveMarRptEvent):void
		{
			dispatchEvent(new ShowLoadingPopupEvent("Download MAR Report ..."));
			marService.retrieveMarRpt(retrieveMarRptResult, closePopup);
		}
		
		public function retrieveMarRptResult(evt:TideResultEvent):void
		{
			dispatchEvent(new ReceiveMarRptEvent());
			dispatchEvent(new CloseLoadingPopupEvent());
		}
		
		public function closePopup(evt:Event):void
		{
			dispatchEvent(new CloseLoadingPopupEvent());
		}
		
		private function getNewMpRenalEcrclLog():ListCollectionView {
			var returnList:ListCollectionView = new ArrayCollection();
			for each (var mpRenalEcrclLog:MpRenalEcrclLog in mpRenalEcrclLogList) {
				if (isNaN(mpRenalEcrclLog.id)) {
					returnList.addItem(mpRenalEcrclLog);
				}
			}
			return returnList;
		}
		
		private function getNewMpRenalMessage():ListCollectionView {
			var returnList:ListCollectionView = new ArrayCollection();
			for each (var mpRenalMessage:MpRenalMessage in mpRenalMessageList) {
				if (isNaN(mpRenalMessage.id)) {
					returnList.addItem(mpRenalMessage);
				}
			}
			return returnList;
		}
		
		[Observer]
		public function checkProfileHasLabelGenerationWithinToday(event:CheckProfileHasLabelGenerationWithinToday):void {
			medProfileVettingService.checkProfileHasLabelGenerationWithinToday(function(evt:TideResultEvent):void{
				event.callBack(evt.result);
			});
		}
	}
}