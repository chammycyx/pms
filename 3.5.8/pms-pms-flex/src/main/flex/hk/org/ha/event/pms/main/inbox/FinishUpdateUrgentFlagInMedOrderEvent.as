package hk.org.ha.event.pms.main.inbox
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class FinishUpdateUrgentFlagInMedOrderEvent extends AbstractTideEvent
	{
		private var _success:Boolean;
		private var _locked:Boolean;
		private var _lockedBy:String;
		
		public function FinishUpdateUrgentFlagInMedOrderEvent(lockedBy:String)
		{
			super();
			_success = lockedBy == null;
			_locked = lockedBy != null && lockedBy.length > 0;
			_lockedBy = lockedBy;
		}
		
		public function get success():Boolean
		{
			return _success;
		}
		
		public function get locked():Boolean
		{
			return _locked;
		}
		
		public function get lockedBy():String
		{
			return _lockedBy;
		}
	}
}