package hk.org.ha.control.pms.main.alert.mds {
	import flash.events.MouseEvent;
	
	import hk.org.ha.event.pms.main.alert.mds.CheckManualItemForSaveProfileEvent;
	import hk.org.ha.event.pms.main.alert.mds.CheckMedProfileMdsEvent;
	import hk.org.ha.event.pms.main.alert.mds.UpdateAlertResultListForSaveProfileEvent;
	import hk.org.ha.event.pms.main.alert.mds.popup.ShowMdsLogPopupEvent;
	import hk.org.ha.event.pms.main.alert.mds.popup.ShowMedProfileAlertMsgPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	import hk.org.ha.model.pms.biz.alert.mds.MedProfileCheckServiceBean;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
	import hk.org.ha.model.pms.persistence.medprofile.PurchaseRequest;
	import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
	import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
	import hk.org.ha.model.pms.vo.alert.mds.ErrorInfo;
	import hk.org.ha.model.pms.vo.alert.mds.ManualProfileCheckResult;
	import hk.org.ha.model.pms.vo.alert.mds.MdsUtils;
	import hk.org.ha.model.pms.vo.alert.mds.MedProfileCheckResult;
	import hk.org.ha.model.pms.vo.security.PropMap;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	import mx.core.IFlexDisplayObject;
	import mx.core.UIComponent;
	import mx.managers.PopUpManager;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;

	[Bindable]
	[Name("medProfileCheckServiceCtl", restrict="true")]
	public class MedProfileCheckServiceCtl {
		
		private const MDS_SUSPEND_REASON:String    = "MDS checking pending";
		private const EMPTY_SUSPEND_CODE:String    = "--";
		private const MDS_ERROR_MSG_CODE:String    = "0247";
		private const MDS_ERROR_FUNCTION_ID:String = "CMS-0002";
		
		[In]
		public var ctx:Context;
		
		[In]
		public var medProfileItemList:ListCollectionView;
		
		[In]
		public var inactiveList:ListCollectionView;
		
		[In]
		public var purchaseRequestList:ListCollectionView;
		
		[In]
		public var medProfileCheckService:MedProfileCheckServiceBean;
		
		[In]
		public var propMap:PropMap;
		
		private var checkMedProfileMdsEvent:CheckMedProfileMdsEvent;
		
		private var errorInfoList:ListCollectionView;
		private var medProfileAlertMsgList:ListCollectionView;
		private var medProfileCheckResult:MedProfileCheckResult;
		
		private var alertResultList:ListCollectionView;
		private var manualCheckResult:ManualProfileCheckResult;
		
		[Observer]
		public function checkMedProfileMds(evt:CheckMedProfileMdsEvent):void {
			
			checkMedProfileMdsEvent = evt;
			if (propMap.getValueAsBoolean("vetting.medProfile.alert.mds.check") && propMap.getValueAsBoolean("alert.mds.enabled") && evt.medProfileItemList.length > 0) {
				medProfileCheckService.checkMedProfileAlertForSaveProfile(evt.medProfile, checkMedProfileMdsResult);
			}
			else {
				checkMedProfileMdsResult();
			}
			
		}
		
		private function checkMedProfileMdsResult(evt:TideResultEvent=null):void {
			
			medProfileCheckResult = evt == null ? null : evt.result as MedProfileCheckResult;
			// no mds error or mds check disabled
			if (medProfileCheckResult == null) {
				clearMdsSuspendReason();
				checkMedProfileMdsEvent.medProfile.mdsCheckFlag = false;
				checkMedProfileMdsEvent.callback();
				return;
			}

			medProfileAlertMsgList = medProfileCheckResult.medProfileAlertMsgList;
			errorInfoList          = new ArrayCollection(medProfileCheckResult.errorInfoList.toArray());
			alertResultList		   = new ArrayCollection();
			manualCheckResult 	   = medProfileCheckResult.manualProfileCheckResult;

			// show mds debug log
			dispatchEvent(new ShowMdsLogPopupEvent(function():void {
			
				// show mds check error from cms
				showMdsError(function():void {
				
					// show new mds alert
					showMdsResult(function():void {
						
						showManualItemCheckResult(function():void {
								// suspend order
								clearMdsSuspendReason();
								if( alertResultList.length > 0 ){
									MdsUtils.replaceAlertAndStatusByItemNum(checkMedProfileMdsEvent.medProfileItemList, alertResultList, medProfileItemList, inactiveList, purchaseRequestList);
								}
								withHoldOrder();
								checkMedProfileMdsEvent.medProfile.mdsCheckFlag = true;
								checkMedProfileMdsEvent.callback();
						})
					})
				});
			}));
		}
		
		private function showManualItemCheckResult(callback:Function):void{
			if( manualCheckResult == null ){
				callback();
				return;
			}
			
			
			dispatchEvent(new CheckManualItemForSaveProfileEvent(
					manualCheckResult,	
					checkMedProfileMdsEvent.medProfileItemList, 
					checkMedProfileMdsEvent.medProfile, 
					function():void {
						callback();
					},
					function():void {})
				);
		}
		
		[Observer]
		public function updateAlertResultListForSaveProfile(evt:UpdateAlertResultListForSaveProfileEvent):void{
			alertResultList = evt.medProfileMoItemList;
			evt.callback();
		}
		
		private function showMdsError(callback:Function):void {
			if (errorInfoList.length == 0) {
				callback();
				return;
			}
			
			var errorInfo:ErrorInfo = errorInfoList.getItemAt(0) as ErrorInfo;
			errorInfoList.removeItemAt(0);

			var sysMsgProp:SystemMessagePopupProp = new SystemMessagePopupProp(MDS_ERROR_MSG_CODE);
			sysMsgProp.messageWinHeight = 500;
			sysMsgProp.messageWinWidth  = 970;
			sysMsgProp.setOkButtonOnly = true;
			sysMsgProp.functionId = MDS_ERROR_FUNCTION_ID;
			if (errorInfo.supplMsg != null) {
				sysMsgProp.messageParams = new Array(errorInfo.mainMsg, errorInfo.supplMsg);
			}
			else {
				sysMsgProp.messageParams = new Array(errorInfo.mainMsg);
			}
			sysMsgProp.okHandler = function(mouseEvent:MouseEvent):void {
				showMdsError(callback);
				PopUpManager.removePopUp(((mouseEvent.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
			};
			
			dispatchEvent(new RetrieveSystemMessageEvent(sysMsgProp));
		}

		private function showMdsResult(callback:Function):void {
			if (medProfileAlertMsgList.length == 0) {
				callback();
				return;
			}
			var showMedProfileAlertMsgPopupEvent:ShowMedProfileAlertMsgPopupEvent = new ShowMedProfileAlertMsgPopupEvent("save", medProfileAlertMsgList);
			showMedProfileAlertMsgPopupEvent.withholdCallback = callback;
			showMedProfileAlertMsgPopupEvent.medProfile = checkMedProfileMdsEvent.medProfile;
			dispatchEvent(showMedProfileAlertMsgPopupEvent);
		}
		
		private function clearMdsSuspendReason():void {
			
			for each (var mpi:MedProfileItem in checkMedProfileMdsEvent.medProfileItemList) {
				if( mpi.medProfileMoItem.isManualItem ){//not clear mds exception for manualItem
					continue;
				}
				
				if (mpi.medProfileMoItem.mdsSuspendReason != null) {
					mpi.medProfileMoItem.mdsSuspendReason = null;
					mpi.modified = true;
				}
			}
		}
		
		private function withHoldOrder():void {
			for each (var id:Number in medProfileCheckResult.withHoldList) {
				for each (var mpi:MedProfileItem in checkMedProfileMdsEvent.medProfileItemList) {

					if( mpi.isManualItem ){
						continue;
					}
					
					if (mpi.id == id) {
						mpi.medProfileMoItem.mdsSuspendReason = MDS_SUSPEND_REASON;
						if (mpi.orgStatus == MedProfileItemStatus.Verified) {
							mpi.status = MedProfileItemStatus.Vetted;
						}
						else {
							mpi.status = mpi.orgStatus; 
						}
						mpi.modified = true;
						MdsUtils.updatePurchaseRequestStatus(mpi, purchaseRequestList);
						break;
					}
				}
			}
		}
	}
}
