package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDmFormEvent extends AbstractTideEvent 
	{
		private var _formCode:String;
		private var _resultEvent:Event;
		
		public function RetrieveDmFormEvent(formCode:String, resultEvent:Event=null):void 
		{
			super();
			_formCode = formCode;
			_resultEvent = resultEvent;
		}
		
		public function get formCode():String
		{
			return _formCode;
		}
		
		public function get resultEvent():Event
		{
			return _resultEvent;
		}
	}
}