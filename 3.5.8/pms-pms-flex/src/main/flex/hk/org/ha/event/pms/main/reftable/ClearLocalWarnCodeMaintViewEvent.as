package hk.org.ha.event.pms.main.reftable {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ClearLocalWarnCodeMaintViewEvent extends AbstractTideEvent 
	{		
		public function ClearLocalWarnCodeMaintViewEvent():void 
		{
			super();
		}
	}
}