package hk.org.ha.event.pms.main.reftable.pivas {

	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormula;
	
	public class RetrievePivasFormulaInfoEvent extends AbstractTideEvent {
	
		private var _drugKey:Number;
		private var _includeNonPivasSite:Boolean;
		private var _pivasFormula:PivasFormula;
		private var _callback:Function;
		
		public function RetrievePivasFormulaInfoEvent(drugKey:Number, includeNonPivasSite:Boolean, pivasFormula:PivasFormula, callback:Function) {
			super();
			_drugKey = drugKey;
			_includeNonPivasSite = includeNonPivasSite;
			_pivasFormula = pivasFormula;
			_callback = callback;
		}
		
		public function get drugKey():Number {
			return _drugKey;
		}
		
		public function get includeNonPivasSite():Boolean {
			return _includeNonPivasSite;
		}
		
		public function get pivasFormula():PivasFormula {
			return _pivasFormula;
		}

		public function get callback():Function {
			return _callback;
		}
	}
}