package hk.org.ha.event.pms.main.reftable.mp {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshMpQtyConversionRptListEvent extends AbstractTideEvent 
	{
		private var _qtyExportSuccess:Boolean;
		
		public function RefreshMpQtyConversionRptListEvent(qtyExportSuccess:Boolean):void 
		{
			super();
			_qtyExportSuccess = qtyExportSuccess;
		}
		
		public function get qtyExportSuccess():Boolean
		{
			return _qtyExportSuccess;
		}
		
	}
}