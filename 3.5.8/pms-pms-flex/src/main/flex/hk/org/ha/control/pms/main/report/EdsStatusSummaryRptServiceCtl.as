package hk.org.ha.control.pms.main.report {
	import hk.org.ha.event.pms.main.report.PrintEdsStatusSummaryRptEvent;
	import hk.org.ha.event.pms.main.report.RefreshEdsStatusSummaryRptEvent;
	import hk.org.ha.event.pms.main.report.RetrieveEdsStatusSummaryRptEvent;
	import hk.org.ha.model.pms.biz.report.EdsStatusSummaryRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("edsStatusSummaryRptServiceCtl", restrict="true")]
	public class EdsStatusSummaryRptServiceCtl 
	{
		[In]
		public var edsStatusSummaryRptService:EdsStatusSummaryRptServiceBean;
		
		[Observer]
		public function retrieveEdsStatusSummaryRpt(evt:RetrieveEdsStatusSummaryRptEvent):void
		{
			In(Object(edsStatusSummaryRptService).retrieveSuccess)
			edsStatusSummaryRptService.retrieveEdsStatusSummaryRpt(evt.selectedTab, evt.date, retrieveEdsStatusSummaryRptResult);
		}
		
		private function retrieveEdsStatusSummaryRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshEdsStatusSummaryRptEvent	(Object(edsStatusSummaryRptService).retrieveSuccess));
		}
		
		[Observer]
		public function printEdsStatusSummaryRpt(evt:PrintEdsStatusSummaryRptEvent):void
		{
			edsStatusSummaryRptService.printEdsStatusSummaryRpt();
		}
		
	}
}
