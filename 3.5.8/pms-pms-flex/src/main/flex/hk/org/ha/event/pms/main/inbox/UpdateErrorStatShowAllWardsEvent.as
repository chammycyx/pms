package hk.org.ha.event.pms.main.inbox
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateErrorStatShowAllWardsEvent extends AbstractTideEvent
	{
		private var _showAllWards:Boolean;
		
		public function UpdateErrorStatShowAllWardsEvent(showAllWards:Boolean)
		{
			super();
			_showAllWards = showAllWards;
		}
		
		public function get showAllWards():Boolean
		{
			return _showAllWards;
		}
	}
}