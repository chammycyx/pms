package hk.org.ha.event.pms.main.drug {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveWorkstoreDrugEvent extends AbstractTideEvent 
	{
		private var _itemCode:String;
		private var _callBackFunc:Function;
		
		public function RetrieveWorkstoreDrugEvent(itemCode:String, callBackFunc:Function):void 
		{
			super();
			_itemCode = itemCode;
			_callBackFunc = callBackFunc;
		}
		
		public function get itemCode():String
		{
			return _itemCode;
		}
		
		public function get callBackFunc():Function
		{
			return _callBackFunc;
		}
	}
}