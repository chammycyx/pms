package hk.org.ha.event.pms.main.reftable {
	
	import hk.org.ha.model.pms.vo.reftable.DrsConfigInfo;
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetDrsConfigListEvent extends AbstractTideEvent 
	{
		private var _drsConfigInfo:DrsConfigInfo;
		
		public function SetDrsConfigListEvent(drsConfigInfo:DrsConfigInfo):void {
			super();
			_drsConfigInfo = drsConfigInfo;
		}
		
		public function get drsConfigInfo():DrsConfigInfo {
			return _drsConfigInfo;
		}
	}
}