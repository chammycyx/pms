package hk.org.ha.event.pms.main.ehr {
	
	import hk.org.ha.model.pms.vo.ehr.EhrPatient;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveEhrssViewerUrlEvent extends AbstractTideEvent 
	{			
		private var _ehrPatient:EhrPatient;
		
		public function RetrieveEhrssViewerUrlEvent(ehrPatient:EhrPatient):void 
		{
			super();
			_ehrPatient = ehrPatient;
		}
		
		public function get ehrPatient():EhrPatient {
			return _ehrPatient;
		}
	}
}