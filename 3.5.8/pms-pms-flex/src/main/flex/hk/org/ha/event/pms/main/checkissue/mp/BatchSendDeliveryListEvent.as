package hk.org.ha.event.pms.main.checkissue.mp
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class BatchSendDeliveryListEvent extends AbstractTideEvent 
	{			
		private var _readyToSendDeliveryList:ArrayCollection;
		
		public function BatchSendDeliveryListEvent(readyToSendDeliveryList:ArrayCollection):void 
		{
			super();
			_readyToSendDeliveryList = readyToSendDeliveryList;
		}
		
		public function get readyToSendDeliveryList():ArrayCollection {
			return _readyToSendDeliveryList;
		}
		
	}
}