package hk.org.ha.event.pms.main.enquiry
{
	import hk.org.ha.model.pms.vo.charging.DrugCheckClearanceResult;	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshDrugChargeClearanceEvent extends AbstractTideEvent 
	{
		private var _drugCheckClearanceResult:DrugCheckClearanceResult;
		
		public function RefreshDrugChargeClearanceEvent(drugCheckClearanceResult:DrugCheckClearanceResult):void{
			super();
			_drugCheckClearanceResult = drugCheckClearanceResult;
		}
		
		public function get drugCheckClearanceResult():DrugCheckClearanceResult
		{
			return _drugCheckClearanceResult;
		}	
	}
}