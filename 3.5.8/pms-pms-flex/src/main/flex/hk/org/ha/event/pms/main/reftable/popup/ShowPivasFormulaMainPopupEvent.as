package hk.org.ha.event.pms.main.reftable.popup {

	import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormula;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class ShowPivasFormulaMainPopupEvent extends AbstractTideEvent {

		private var _pivasFormula:PivasFormula;
		private var _postSave:Function;
		
		public function ShowPivasFormulaMainPopupEvent(pivasFormula:PivasFormula, postSave:Function):void {
			super();
			_pivasFormula = pivasFormula;
			_postSave = postSave;
		}
		
		public function get pivasFormula():PivasFormula {
			return _pivasFormula;
		}

		public function get postSave():Function {
			return _postSave;
		}
	}
}
