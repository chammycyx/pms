package hk.org.ha.event.pms.main.vetting.popup {		
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowPreparationListPopupEvent extends AbstractTideEvent 
	{					
		
		private var _medOrderItem:MedOrderItem;
		
		private var _okHandler:Function;							
		
		private var _cancelHandler:Function;
		
		private var _callBackEvent:Event;
		
		public function ShowPreparationListPopupEvent( medOrderItem:MedOrderItem, okHandlerVal:Function=null, cancelHandlerVal:Function=null, eventVal:Event=null):void 
		{
			super();			
			_medOrderItem = medOrderItem;
			_okHandler = okHandlerVal;			
			_cancelHandler = cancelHandlerVal;
			_callBackEvent	= eventVal;
		}		
		
		public function get medOrderItem():MedOrderItem
		{
			return _medOrderItem;
		}
		
		public function get okHandler():Function
		{
			return _okHandler;
		}	
		
		public function get cancelHandler():Function
		{
			return _cancelHandler;
		}
		
		public function get callBackEvent():Event
		{
			return _callBackEvent;
		}	
	}
}
