package hk.org.ha.event.pms.main.vetting.mp
{
	import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RecalculateQtyEvent extends AbstractTideEvent
	{
		private var _medProfilePoItem:MedProfilePoItem;
		
		private var _callBackFunc:Function;
		
		public function RecalculateQtyEvent(medProfilePoItem:MedProfilePoItem, functionValue:Function=null)
		{
			super();
			_medProfilePoItem = medProfilePoItem;
			_callBackFunc 	  = functionValue;
		}
		
		public function get medProfilePoItem():MedProfilePoItem
		{
			return _medProfilePoItem;
		}
		
		public function get callBackFunc():Function
		{
			return _callBackFunc;
		}
	}
}