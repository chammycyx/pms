package hk.org.ha.event.pms.main.onestop {
		
	import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
	import hk.org.ha.model.pms.vo.onestop.OneStopOrderItemSummary;
	import hk.org.ha.model.pms.vo.onestop.OneStopRefillItemSummary;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
		
	public class RefreshOrderStatusEvent extends AbstractTideEvent 
	{		
		private var _latestOneStopOrderItemSummary:OneStopOrderItemSummary;
		private var _latestOneStopRefillItemSummary:OneStopRefillItemSummary;
		private var _latestOneStopRefillItemSummaryList:ArrayCollection;
				
		public function RefreshOrderStatusEvent(latestOneStopOrderItemSummary:OneStopOrderItemSummary, 
												latestOneStopRefillItemSummary:OneStopRefillItemSummary,
												latestOneStopRefillItemSummaryList:ArrayCollection):void 
		{
			super();
			_latestOneStopOrderItemSummary = latestOneStopOrderItemSummary;
			_latestOneStopRefillItemSummary = latestOneStopRefillItemSummary;
			_latestOneStopRefillItemSummaryList = latestOneStopRefillItemSummaryList;
		}
		
		public function get latestOneStopOrderItemSummary():OneStopOrderItemSummary
		{
			return _latestOneStopOrderItemSummary;
		}
		
		public function get latestOneStopRefillItemSummary():OneStopRefillItemSummary
		{
			return _latestOneStopRefillItemSummary;
		}
		
		public function get latestOneStopRefillItemSummaryList():ArrayCollection
		{
			return _latestOneStopRefillItemSummaryList;
		}
	}
}