package hk.org.ha.event.pms.main.reftable.mp {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshPrintMpDosageConversionRptEvent extends AbstractTideEvent 
	{
		private var _retrieveSuccess:Boolean;
		
		public function RefreshPrintMpDosageConversionRptEvent(retrieveSuccess:Boolean):void 
		{
			super();
			_retrieveSuccess = retrieveSuccess;
		}
		
		public function get retrieveSuccess():Boolean
		{
			return _retrieveSuccess;
		}
		
	}
}