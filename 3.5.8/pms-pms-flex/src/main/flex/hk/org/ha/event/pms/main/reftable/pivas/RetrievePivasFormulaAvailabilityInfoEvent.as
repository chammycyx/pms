package hk.org.ha.event.pms.main.reftable.pivas
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePivasFormulaAvailabilityInfoEvent extends AbstractTideEvent
	{
		private var _pivasFormulaId:Number;
		private var _successCallback:Function;
		
		public function RetrievePivasFormulaAvailabilityInfoEvent(pivasFormulaId:Number, successCallback:Function)
		{
			super();
			_pivasFormulaId = pivasFormulaId;
			_successCallback = successCallback;
		}
		
		public function get pivasFormulaId():Number
		{
			return _pivasFormulaId;
		}
		
		public function get successCallback():Function
		{
			return _successCallback;
		}
	}
}