package hk.org.ha.event.pms.main.reftable.popup
{
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.model.pms.persistence.medprofile.DeliveryScheduleItem;

	public class DueOrderScheduleTemplateMaintItemPopupProp
	{
		public var deliveryScheduleItem:DeliveryScheduleItem;
		public var action:String; /*add, update*/
		public var activeWardSelectionList:ArrayCollection;
		public var wardGroupSelectionList:ArrayCollection;
		public var okHandler:Function;
	}
}
