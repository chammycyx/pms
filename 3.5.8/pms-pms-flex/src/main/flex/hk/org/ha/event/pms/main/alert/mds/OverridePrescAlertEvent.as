package hk.org.ha.event.pms.main.alert.mds {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class OverridePrescAlertEvent extends AbstractTideEvent {
		
		private var _callbackFunc:Function;
		
		public function OverridePrescAlertEvent(callbackFunc:Function=null) {
			super();
			_callbackFunc = callbackFunc;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}