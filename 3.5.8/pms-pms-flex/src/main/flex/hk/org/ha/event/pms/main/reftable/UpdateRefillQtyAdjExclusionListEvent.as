package hk.org.ha.event.pms.main.reftable {
	
	import mx.collections.ListCollectionView;
	import mx.core.UIComponent;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateRefillQtyAdjExclusionListEvent extends AbstractTideEvent 
	{
		private var _updateRefillQtyAdjExclusionList:ListCollectionView;
		private var _delRefillQtyAdjExclusionList:ListCollectionView;
		private var _nextTabIndex:Number;
		private var _nextSubTabIndex:Number;
		private var _showRefillModuleMaintAlertMsg:Boolean;
		private var _caller:UIComponent;
		
		public function UpdateRefillQtyAdjExclusionListEvent(updateRefillQtyAdjExclusionList:ListCollectionView, 
																delRefillQtyAdjExclusionList:ListCollectionView, 
																nextTabIndex:Number, 
																nextSubTabIndex:Number,
																showRefillModuleMaintAlertMsg:Boolean, 
																caller:UIComponent):void 
		{
			super();
			_updateRefillQtyAdjExclusionList = updateRefillQtyAdjExclusionList;
			_delRefillQtyAdjExclusionList = delRefillQtyAdjExclusionList;
			_nextTabIndex = nextTabIndex;
			_nextSubTabIndex = nextSubTabIndex;
			_showRefillModuleMaintAlertMsg = showRefillModuleMaintAlertMsg;
			_caller = caller;
		}
		
		public function get delRefillQtyAdjExclusionList():ListCollectionView
		{
			return _delRefillQtyAdjExclusionList;
		}
		
		public function get updateRefillQtyAdjExclusionList():ListCollectionView
		{
			return _updateRefillQtyAdjExclusionList;
		}
		
		public function get nextTabIndex():Number
		{
			return _nextTabIndex;
		}
		
		public function get nextSubTabIndex():Number
		{
			return _nextSubTabIndex;
		}
		
		public function get showRefillModuleMaintAlertMsg():Boolean
		{
			return _showRefillModuleMaintAlertMsg;
		}
		
		public function get caller():UIComponent
		{
			return _caller;
		}
	}
}