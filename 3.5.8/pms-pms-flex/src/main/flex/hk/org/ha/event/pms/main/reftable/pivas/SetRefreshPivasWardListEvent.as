package hk.org.ha.event.pms.main.reftable.pivas {
	
	import mx.collections.ListCollectionView;
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class SetRefreshPivasWardListEvent extends AbstractTideEvent {
		private var _pivasWardList:ListCollectionView;
		
		public function SetRefreshPivasWardListEvent(pivasWardList:ListCollectionView) {
			super();
			_pivasWardList = pivasWardList;
		}
		
		public function get pivasWardList():ListCollectionView
		{
			return _pivasWardList;
		}
	}
}