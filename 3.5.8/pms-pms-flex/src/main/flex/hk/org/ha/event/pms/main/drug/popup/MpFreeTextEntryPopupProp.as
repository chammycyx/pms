package hk.org.ha.event.pms.main.drug.popup
{
	import mx.collections.ArrayCollection;

	public class MpFreeTextEntryPopupProp
	{
		public var hospCode:String;
		public var workstoreCode:String;
		
		public var popupOkHandler:Function;
		public var popupCancelHandler:Function = null;
		public var drugName:String;
		
		public var drugSearchSiteList:ArrayCollection;
	}
}