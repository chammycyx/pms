package hk.org.ha.event.pms.main.vetting {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class CheckDhMappingUpToDateEvent extends AbstractTideEvent {
		
		private var _index:Number;
		private var _callback:Function;
		
		public function CheckDhMappingUpToDateEvent(index:Number, callback:Function) {
			super();
			_index = index;
			_callback = callback;
		}
		
		public function get index():Number {
			return _index;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}