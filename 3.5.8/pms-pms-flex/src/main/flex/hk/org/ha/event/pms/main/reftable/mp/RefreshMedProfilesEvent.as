package hk.org.ha.event.pms.main.reftable.mp
{
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.reftable.WardConfig;
	
	public class RefreshMedProfilesEvent extends AbstractTideEvent
	{
		private var _wardConfig:WardConfig;
		
		private var _refreshPasInfo:Boolean;
		
		private var _refreshMrInfo:Boolean;
		
		public function RefreshMedProfilesEvent(wardConfig:WardConfig, refreshPasInfo:Boolean, refreshMrInfo:Boolean)
		{
			super();
			_wardConfig = wardConfig;
			_refreshPasInfo = refreshPasInfo;
			_refreshMrInfo = refreshMrInfo;
		}
		
		public function get wardConfig():WardConfig
		{
			return _wardConfig;
		}
		
		public function get refreshPasInfo():Boolean
		{
			return _refreshPasInfo;
		}
		
		public function get refreshMrInfo():Boolean
		{
			return _refreshMrInfo;
		}
	}
}