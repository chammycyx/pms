package hk.org.ha.event.pms.main.pivas
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePivasWorklistListEvent extends AbstractTideEvent 
	{		
		private var _callbackFunc:Function;
		
		public function RetrievePivasWorklistListEvent(callbackFunc:Function):void 
		{
			super();
			_callbackFunc = callbackFunc;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}