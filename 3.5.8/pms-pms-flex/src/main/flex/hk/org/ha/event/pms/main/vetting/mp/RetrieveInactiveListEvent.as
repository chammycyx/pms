package hk.org.ha.event.pms.main.vetting.mp
{
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveInactiveListEvent extends AbstractTideEvent
	{
		private var _callBack:Function;
		
		public function RetrieveInactiveListEvent(callBack:Function=null)
		{
			super();
			_callBack = callBack;
		}
		
		public function get callBack():Function {
			return _callBack;
		}
	}
}