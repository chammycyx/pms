package hk.org.ha.event.pms.main.reftable {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SetHolidayListDateRangeFilterEvent extends AbstractTideEvent 
	{
		private var _setUIFocus:Boolean;
		public function SetHolidayListDateRangeFilterEvent(setUIFocus:Boolean):void 
		{
			super();
			_setUIFocus = setUIFocus;
		}
		
		public function get setUIFocus():Boolean {
			return _setUIFocus;
		}

	}
}