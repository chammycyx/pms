package hk.org.ha.event.pms.security {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class PostLogonEvent extends AbstractTideEvent 
	{
		private var _hospCode:String;        
		private var _workstoreCode:String;
		
		public function PostLogonEvent(hospCode:String=null, workstoreCode:String=null):void 
		{
			super();
			_hospCode = hospCode;
			_workstoreCode = workstoreCode;
		}
		
		public function get hospCode():String 
		{
			return _hospCode;
		}
		
		public function get workstoreCode():String 
		{
			return _workstoreCode;
		}
	}
}