package hk.org.ha.control.pms.main.alert.mds {
	
	import flash.events.MouseEvent;
	
	import hk.org.ha.event.pms.main.alert.mds.OverridePrescAlertEvent;
	import hk.org.ha.event.pms.main.alert.mds.ShowPrescAlertErrorEvent;
	import hk.org.ha.event.pms.main.alert.mds.ShowPrescAlertMsgEvent;
	import hk.org.ha.event.pms.main.alert.mds.popup.ShowAlertMsgPopupEvent;
	import hk.org.ha.event.pms.main.alert.mds.popup.ShowDdiMsgPopupEvent;
	import hk.org.ha.event.pms.main.alert.mds.popup.ShowDischargeAlertMsgPopupEvent;
	import hk.org.ha.event.pms.main.onestop.ClearKeyboardBufferEvent;
	import hk.org.ha.event.pms.main.vetting.EnableVettingByBarcodeEvent;
	import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	import hk.org.ha.model.pms.biz.alert.mds.PrescCheckServiceBean;
	import hk.org.ha.model.pms.persistence.AlertEntity;
	import hk.org.ha.model.pms.persistence.disp.PharmOrder;
	import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
	import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
	import hk.org.ha.model.pms.vo.alert.mds.AlertDdim;
	import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;
	import hk.org.ha.model.pms.vo.alert.mds.ErrorInfo;
	import hk.org.ha.model.pms.vo.alert.mds.MedProfileAlertMsg;
	import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	import mx.core.IFlexDisplayObject;
	import mx.core.UIComponent;
	import mx.managers.PopUpManager;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("prescCheckServiceCtl", restrict="true")]
	public class PrescCheckServiceCtl {
		
		[In]
		public var ctx:Context;
	
		[In]
		public var prescCheckService:PrescCheckServiceBean;
		
		[In]
		public var prescAlertMsgList:ArrayCollection;
		
		[In]
		public var pharmOrder:PharmOrder;
		
		[In]
		public var orderViewInfo:OrderViewInfo;
		
		private var delItemNumList:ArrayCollection;
		
		private var alertMsgIndex:Number;
		
		private var alertErrIndex:Number;
		
		private var alertMsgCallbackFunc:Function;
		
		private var alertErrCallbackFunc:Function;
		
		private var overridePrescAlertEvent:OverridePrescAlertEvent;
		
		private function allowOverride():Boolean {
			return (orderViewInfo.oneStopOrderType == OneStopOrderType.ManualOrder) ||
				   (orderViewInfo.oneStopOrderType == OneStopOrderType.DhOrder) ||
				   (orderViewInfo.oneStopOrderType == OneStopOrderType.DispOrder && pharmOrder.medOrder.docType == MedOrderDocType.Manual);
		}
		
		[Observer]
		public function showPrescAlertMsg(evt:ShowPrescAlertMsgEvent):void {

			if (pharmOrder.medOrder.isMpDischarge()) {
				showMpAlertMsg(evt.callbackFunc);
				return;
			}
			
			if (evt.initFlag) {
				alertMsgIndex = 0;
				alertMsgCallbackFunc = evt.callbackFunc;
				delItemNumList = new ArrayCollection();
			}
			
			if (prescAlertMsgList != null && alertMsgIndex < prescAlertMsgList.length) {
								
				var alertMsg:AlertMsg = (prescAlertMsgList.getItemAt(alertMsgIndex) as MedProfileAlertMsg).alertMsgList.getItemAt(0) as AlertMsg;
				alertMsgIndex++;

				if (alertMsg.supressFlag) {
					dispatchEvent(new ShowPrescAlertMsgEvent());
				}
				else if (allowOverride() && alertMsg.ddiAlertList.length > 0) {
					showDdiAlertMsg(alertMsg);
				}
				else {
					showPatAlertMsg(alertMsg)
				}
			}
			else if (alertMsgCallbackFunc != null) {
				alertMsgCallbackFunc();
			}
		}
		
		private function showMpAlertMsg(callbackFunc:Function):void {
			
			if (prescAlertMsgList == null || prescAlertMsgList.length == 0) {
				callbackFunc();
				return;
			}
			
			var alertMsgState:String;
			if (orderViewInfo.oneStopOrderType == OneStopOrderType.RefillOrder) {
				alertMsgState = "refill";
			}
			else if (orderViewInfo.oneStopOrderType == OneStopOrderType.SfiOrder) {
				alertMsgState = "sfi";
			}
			else {
				alertMsgState = "save";
			}
			
			var evt:ShowDischargeAlertMsgPopupEvent = new ShowDischargeAlertMsgPopupEvent();
			evt.state = alertMsgState;
			evt.allowEditFlag = false;
			evt.closeFunc = callbackFunc;
			evt.medProfileAlertMsgList = prescAlertMsgList;
			dispatchEvent(evt);
		}
		
		private function showPatAlertMsg(alertMsg:AlertMsg):void {

			var alertMsgState:String;
			if (orderViewInfo.oneStopOrderType == OneStopOrderType.RefillOrder) {
				alertMsgState = "refill";
			}
			else if (orderViewInfo.oneStopOrderType == OneStopOrderType.SfiOrder) {
				alertMsgState = "sfi";
			}
			else {
				alertMsgState = "save";
			}
			
			var evt:ShowAlertMsgPopupEvent = new ShowAlertMsgPopupEvent(alertMsgState, false, alertMsg);

			evt.closeFunc = function():void {
				if (allowOverride()) {
					alertMsgCallbackFunc = null;
				}
				else {
					dispatchEvent(new ShowPrescAlertMsgEvent());
				}
			};
			evt.overrideAlertFunc = function():void {
				dispatchEvent(new ShowPrescAlertMsgEvent());
			};
			evt.prescribeAlternativeFunc = function():void {
				markMedOrderItemDelete(alertMsg.itemNum);
				dispatchEvent(new ShowPrescAlertMsgEvent());
			};

			dispatchEvent(evt);
		}
		
		private function showDdiAlertMsg(alertMsg:AlertMsg):void {
			
			var alertDdim:AlertDdim = (alertMsg.ddiAlertList.getItemAt(0) as AlertEntity).alert as AlertDdim;

			var evt:ShowDdiMsgPopupEvent = new ShowDdiMsgPopupEvent(alertMsg);
			
			if (alertDdim.isOnHand) {
				
				var currOrdNo:Number;
				if (pharmOrder.medOrder.orderNum == null) {
					currOrdNo = 0;
				}
				else {
					currOrdNo = Number(pharmOrder.medOrder.orderNum.substr(3));
				}
				
				if (currOrdNo == alertDdim.ordNo1) {
					evt.doNotPrescribeFunc = function():void {
						markMedOrderItemDelete(alertDdim.itemNum1);
						dispatchEvent(new ShowPrescAlertMsgEvent());
					};
				}
				else {
					evt.doNotPrescribeFunc = function():void {
						markMedOrderItemDelete(alertDdim.itemNum2);
						dispatchEvent(new ShowPrescAlertMsgEvent());
					};
				}
			}
			else {
				evt.doNotPrescribeFunc = function():void {
					markMedOrderItemDelete(alertDdim.itemNum1);
					markMedOrderItemDelete(alertDdim.itemNum2);
					dispatchEvent(new ShowPrescAlertMsgEvent());
				};

				evt.prescribeAFunc = function():void {
					markMedOrderItemDelete(alertDdim.itemNum2);
					dispatchEvent(new ShowPrescAlertMsgEvent());
				};
				
				evt.prescribeBFunc = function():void {
					markMedOrderItemDelete(alertDdim.itemNum1);
					dispatchEvent(new ShowPrescAlertMsgEvent());
				};
			}
			
			evt.overrideAlertFunc = function():void {
				dispatchEvent(new ShowPrescAlertMsgEvent());
			};
			
			evt.onHandFlag = alertDdim.isOnHand;
			
			dispatchEvent(evt);
		}
		
		private function markMedOrderItemDelete(itemNum:Number):void {
			delItemNumList.addItem(itemNum);
			for each (var medProfileAlertMsg:MedProfileAlertMsg in prescAlertMsgList) {
				for each (var alertMsg:AlertMsg in medProfileAlertMsg.alertMsgList) {
					alertMsg.removeDdiAlertByItemNum(itemNum, pharmOrder.medOrder.orderNum);
					if (alertMsg.itemNum == itemNum || alertMsg.alertList.length == 0) {
						alertMsg.supressFlag = true;
					}
				}
			}
		}
		
		[Observer]
		public function showPrescAlertError(evt:ShowPrescAlertErrorEvent):void {
			
			if (evt.initFlag) {
				alertErrIndex = 0;
				alertErrCallbackFunc = evt.callbackFunc;
			}
			
			var errorInfoList:ListCollectionView = orderViewInfo.alertErrorInfoList;
			if (alertErrIndex < errorInfoList.length) {

				var errorInfo:ErrorInfo = errorInfoList.getItemAt(alertErrIndex) as ErrorInfo;
				alertErrIndex++;
				
				if (errorInfo.suppressFlag) {
					dispatchEvent(new ShowPrescAlertErrorEvent());
				}
				else {
					showAlertErrorMsg(errorInfo);
				}
			}
			else if (alertErrCallbackFunc != null) {
				alertErrCallbackFunc();
			}
		}
				
		private function showAlertErrorMsg(errorInfo:ErrorInfo):void {
			
			var sysMsgProp:SystemMessagePopupProp = new SystemMessagePopupProp("0247");
			sysMsgProp.messageWinHeight = 500;
			sysMsgProp.messageWinWidth  = 970;
			sysMsgProp.setOkButtonOnly = true;
			sysMsgProp.functionId = "CMS-0002";
			if (errorInfo.supplMsg != null) {
				sysMsgProp.messageParams = new Array(errorInfo.mainMsg, errorInfo.supplMsg);
			}
			else {
				sysMsgProp.messageParams = new Array(errorInfo.mainMsg);
			}
			sysMsgProp.okHandler = function(mouseEvent:MouseEvent):void {
				dispatchEvent(new ShowPrescAlertErrorEvent());
				dispatchEvent(new ClearKeyboardBufferEvent());
				dispatchEvent(new EnableVettingByBarcodeEvent(true));
				PopUpManager.removePopUp(((mouseEvent.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
			};
			
			dispatchEvent(new RetrieveSystemMessageEvent(sysMsgProp));
		}

		[Observer]
		public function overridePrescAlert(evt:OverridePrescAlertEvent):void {
			overridePrescAlertEvent = evt;
			prescCheckService.overridePrescAlert(delItemNumList, overridePrescAlertResult);
		}
		
		private function overridePrescAlertResult(evt:TideResultEvent):void {
			if (overridePrescAlertEvent.callbackFunc != null) {
				overridePrescAlertEvent.callbackFunc();
			}
		}
	}
}
