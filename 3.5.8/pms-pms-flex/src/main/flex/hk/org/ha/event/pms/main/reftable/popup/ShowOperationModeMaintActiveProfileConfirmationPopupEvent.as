package hk.org.ha.event.pms.main.reftable.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowOperationModeMaintActiveProfileConfirmationPopupEvent extends AbstractTideEvent 
	{
		private var _activeProfileName:String;
		private var _yesHandler:Function;
		
		public function ShowOperationModeMaintActiveProfileConfirmationPopupEvent(activeProfileName:String, yesHandler:Function):void 
		{
			super();
			_activeProfileName = activeProfileName; 
			_yesHandler = yesHandler;
		}
		
		public function get activeProfileName():String {
			return _activeProfileName;
		}
		
		public function get yesHandler():Function {
			return _yesHandler;
		}
	}
}