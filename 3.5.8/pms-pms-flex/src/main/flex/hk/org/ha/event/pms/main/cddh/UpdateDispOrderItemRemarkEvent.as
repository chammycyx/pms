package hk.org.ha.event.pms.main.cddh {
	
	import hk.org.ha.model.pms.vo.cddh.CddhDispOrderItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateDispOrderItemRemarkEvent extends AbstractTideEvent 
	{
		private var _cddhDispOrderItem:CddhDispOrderItem;
		
		public function UpdateDispOrderItemRemarkEvent(cddhDispOrderItem:CddhDispOrderItem):void 
		{
			super();
			_cddhDispOrderItem = cddhDispOrderItem;
		}
		
		public function get cddhDispOrderItem():CddhDispOrderItem
		{
			return _cddhDispOrderItem;
		}
	}
}