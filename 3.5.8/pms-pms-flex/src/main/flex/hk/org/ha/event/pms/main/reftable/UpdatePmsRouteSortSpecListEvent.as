package hk.org.ha.event.pms.main.reftable {
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdatePmsRouteSortSpecListEvent extends AbstractTideEvent 
	{
		private var _newPmsRouteSortSpecList:ListCollectionView;
		
		public function UpdatePmsRouteSortSpecListEvent(newPmsRouteSortSpecList:ListCollectionView):void 
		{
			super();
			_newPmsRouteSortSpecList = newPmsRouteSortSpecList;
		}
		
		public function get newPmsRouteSortSpecList():ListCollectionView
		{
			return _newPmsRouteSortSpecList;
		}
	}
}