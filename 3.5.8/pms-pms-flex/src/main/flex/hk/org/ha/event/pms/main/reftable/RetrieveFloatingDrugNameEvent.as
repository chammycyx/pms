package hk.org.ha.event.pms.main.reftable {
	
	import hk.org.ha.model.pms.udt.vetting.OrderType;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveFloatingDrugNameEvent extends AbstractTideEvent 
	{
		private var _itemCode:String;
		private var _orderTypeValue:OrderType;
		private var _callBackFunc:Function;
		
		public function RetrieveFloatingDrugNameEvent(itemCode:String, orderTypeValue:OrderType, callBackFuncValue:Function=null):void 
		{
			super();
			_itemCode 	  = itemCode;
			_orderTypeValue = orderTypeValue;
			_callBackFunc = callBackFuncValue;
		}
		
		public function get itemCode():String
		{
			return _itemCode;
		}
		
		public function get orderType():OrderType
		{
			return _orderTypeValue;
		}
		
		public function get callBackFunc():Function
		{
			return _callBackFunc;
		}
	}
}