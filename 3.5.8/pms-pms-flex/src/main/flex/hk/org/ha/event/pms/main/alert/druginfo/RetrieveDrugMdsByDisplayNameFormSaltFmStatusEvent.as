package hk.org.ha.event.pms.main.alert.druginfo {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrieveDrugMdsByDisplayNameFormSaltFmStatusEvent extends AbstractTideEvent {
		
		private var _displayName:String;
		
		private var _formCode:String;
		
		private var _saltProperty:String;
		
		private var _fmStatus:String;
		
		private var _checkHq:Boolean;
		
		private var _checkRenal:Boolean;
		
		private var _callback:Function;

		public function RetrieveDrugMdsByDisplayNameFormSaltFmStatusEvent(displayName:String, formCode:String, saltProperty:String, fmStatus:String, checkHq:Boolean, checkRenal:Boolean, callback:Function) {
			super();
			_displayName = displayName;
			_formCode = formCode;
			_saltProperty = saltProperty;
			_fmStatus = fmStatus;
			_checkHq = checkHq;
			_checkRenal = checkRenal;
			_callback = callback;
		}
		
		public function get displayName():String {
			return _displayName;
		}
		
		public function get formCode():String {
			return _formCode;
		}
		
		public function get saltProperty():String {
			return _saltProperty;
		}
		
		public function get fmStatus():String {
			return _fmStatus;
		}
		
		public function get checkHq():Boolean {
			return _checkHq;
		}
		
		public function get checkRenal():Boolean {
			return _checkRenal;
		}

		public function get callback():Function {
			return _callback;
		}
	}
}
