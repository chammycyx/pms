package hk.org.ha.event.pms.main.vetting.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	
	public class RetrieveFmIndicationByMedOrderItemEvent extends AbstractTideEvent {
		
		private var _medOrderItem:MedOrderItem;
		
		public function RetrieveFmIndicationByMedOrderItemEvent(medOrderItem:MedOrderItem) {
			super();
			_medOrderItem = medOrderItem;
		}
		
		public function get medOrderItem():MedOrderItem {
			return _medOrderItem;
		}
	}
}
