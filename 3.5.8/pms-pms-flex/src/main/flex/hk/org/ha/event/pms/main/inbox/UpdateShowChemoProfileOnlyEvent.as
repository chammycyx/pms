package hk.org.ha.event.pms.main.inbox
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateShowChemoProfileOnlyEvent extends AbstractTideEvent
	{
		private var _showChemoProfileOnly:Boolean;
		
		public function UpdateShowChemoProfileOnlyEvent(showChemoProfileOnly:Boolean)
		{
			super();
			_showChemoProfileOnly = showChemoProfileOnly;
		}
		
		public function get showChemoProfileOnly():Boolean
		{
			return _showChemoProfileOnly;
		}
	}
}