package hk.org.ha.event.pms.main.dispensing
{
	import hk.org.ha.model.pms.vo.ticketgen.TicketGenResult;
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshTicketEvent extends AbstractTideEvent 
	{		
		private var _ticketGenResult:TicketGenResult;
		
		public function RefreshTicketEvent(ticketGenResult:TicketGenResult):void 
		{
			super();
			_ticketGenResult = ticketGenResult;
		}
		
		public function get ticketGenResult():TicketGenResult
		{
			return _ticketGenResult;
		}
	}
}