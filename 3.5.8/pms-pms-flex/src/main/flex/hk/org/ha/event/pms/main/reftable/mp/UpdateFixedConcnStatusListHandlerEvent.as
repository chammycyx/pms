package hk.org.ha.event.pms.main.reftable.mp
{
	import hk.org.ha.model.pms.dms.persistence.PmsDosageConversion;
	import hk.org.ha.model.pms.dms.persistence.PmsDosageConversionSet;
	
	import mx.collections.ArrayCollection;
	import mx.controls.List;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class UpdateFixedConcnStatusListHandlerEvent extends AbstractTideEvent
	{
		public function UpdateFixedConcnStatusListHandlerEvent():void
		{
			super();
		}
	}
}