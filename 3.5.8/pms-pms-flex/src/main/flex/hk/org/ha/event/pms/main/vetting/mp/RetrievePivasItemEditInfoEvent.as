package hk.org.ha.event.pms.main.vetting.mp
{
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePivasItemEditInfoEvent extends AbstractTideEvent
	{

		private var _medProfileMoItem:MedProfileMoItem;		
		private var _callbackFunc:Function;
		
		public function RetrievePivasItemEditInfoEvent(medProfileMoItem:MedProfileMoItem, callback:Function)
		{
			super();
			_medProfileMoItem = medProfileMoItem;
			_callbackFunc = callback;
		}
		
		public function get medProfileMoItem():MedProfileMoItem {
			return _medProfileMoItem;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}