package hk.org.ha.control.pms.main.checkissue.mp{
	
	import hk.org.ha.event.pms.main.checkissue.mp.ClearUnlinkDrugViewEvent;
	import hk.org.ha.event.pms.main.checkissue.mp.DisplayProblemItemEvent;
	import hk.org.ha.event.pms.main.checkissue.mp.RetrieveDeliveryItemListForUnlinkDrugEvent;
	import hk.org.ha.event.pms.main.checkissue.mp.UpdateDeliveryItemStatusToUnlinkedEvent;
	import hk.org.ha.model.pms.biz.checkissue.mp.UnlinkServiceBean;
	import hk.org.ha.model.pms.vo.checkissue.mp.CheckDelivery;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("unlinkServiceCtl", restrict="true")]
	public class UnlinkServiceCtl 
	{
		[In]
		public var unlinkService:UnlinkServiceBean;
		
		[In]
		public var resultMsg:String;
		
		[Observer]
		public function retrieveDeliveryItemListForUnlinkDrug(evt:RetrieveDeliveryItemListForUnlinkDrugEvent):void
		{
			unlinkService.retrieveDeliveryItemByForUnlinkDrug(
				evt.batchDate,evt.batchNum,evt.caseNum,evt.mpDispLabelQrCodeXml,
				retrieveDeliveryItemListForUnlinkDrugResult);
		}
		
		private function retrieveDeliveryItemListForUnlinkDrugResult(evt:TideResultEvent):void
		{	
			evt.context.dispatchEvent( new DisplayProblemItemEvent(resultMsg ,evt.result as CheckDelivery ) );
		}
		
		[Observer]
		public function updateDeliveryItemStatusToUnlinked(evt:UpdateDeliveryItemStatusToUnlinkedEvent):void
		{
			unlinkService.updateDeliveryItemStatuToUnlink(evt.checkDelivery, updateDeliveryItemStatusToUnlinkedResult);
		}
		
		private function updateDeliveryItemStatusToUnlinkedResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new ClearUnlinkDrugViewEvent(resultMsg, evt.result as CheckDelivery ) );
		}
	}
}
