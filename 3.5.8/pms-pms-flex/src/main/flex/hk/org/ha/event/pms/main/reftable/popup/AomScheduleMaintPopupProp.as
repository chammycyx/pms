package hk.org.ha.event.pms.main.reftable.popup
{
	import hk.org.ha.model.pms.persistence.reftable.AomSchedule;
	
	import mx.collections.ArrayCollection;

	public class AomScheduleMaintPopupProp
	{
		public var aomSchedule:AomSchedule;
		public var okHandler:Function;
		public var action:String; /*add, update*/
		public var aomScheduleList:ArrayCollection;
	}
}
