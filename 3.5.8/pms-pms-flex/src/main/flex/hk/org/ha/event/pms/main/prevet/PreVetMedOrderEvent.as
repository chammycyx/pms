package hk.org.ha.event.pms.main.prevet
{
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.disp.MedOrder;
	import hk.org.ha.model.pms.udt.prevet.PreVetMode;
	
	public class PreVetMedOrderEvent extends AbstractTideEvent
	{
		private var _medOrder:MedOrder;
		private var _preVetMode:PreVetMode;
		private var _callbackFunction:Function;
		private var _user:String;
		private var _userDisplayName:String;
		
		public function PreVetMedOrderEvent(medOrder:MedOrder, preVetMode:PreVetMode, user:String, 
											callbackFunction:Function, userDisplayName:String=null)
		{
			super();
			_medOrder = medOrder;
			_preVetMode = preVetMode;
			_user = user;
			_callbackFunction = callbackFunction;
			_userDisplayName = userDisplayName;
		}
		
		public function get medOrder():MedOrder
		{
			return _medOrder;
		}
		
		public function get preVetMode():PreVetMode
		{
			return _preVetMode;
		}
		
		public function get callbackFunction():Function
		{
			return _callbackFunction;
		}
		
		public function get user():String
		{
			return _user;
		}
		
		public function get userDisplayName():String
		{
			return _userDisplayName;
		}
	}
}