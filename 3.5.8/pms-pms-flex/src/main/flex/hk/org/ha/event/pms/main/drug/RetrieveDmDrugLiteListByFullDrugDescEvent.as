package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDmDrugLiteListByFullDrugDescEvent extends AbstractTideEvent 
	{
		private var _fullDrugDesc:String;
		private var _callBackEvent:Event; //for ItemCodeLookup call back set focus purpose

		public function RetrieveDmDrugLiteListByFullDrugDescEvent(fullDrugDesc:String=null, callBackEvent:Event=null):void 
		{
			super();
			_fullDrugDesc = fullDrugDesc;
			_callBackEvent = callBackEvent;
		}
		
		public function get fullDrugDesc():String
		{
			return _fullDrugDesc;
		}
			
		public function get callBackEvent():Event
		{
			return _callBackEvent;
		}
	}
}