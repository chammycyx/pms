package hk.org.ha.event.pms.main.reftable.mp {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshQtyConversionInsuRptListEvent extends AbstractTideEvent 
	{
		private var _qtyInsuExportSuccess:Boolean;
		
		public function RefreshQtyConversionInsuRptListEvent(qtyInsuExportSuccess:Boolean):void 
		{
			super();
			_qtyInsuExportSuccess = qtyInsuExportSuccess;
		}
		
		public function get qtyInsuExportSuccess():Boolean
		{
			return _qtyInsuExportSuccess;
		}
		
	}
}