package hk.org.ha.event.pms.main.lookup.popup
{
	public class ItemCodeLookupPopupProp
	{
		public var doubleClickHandler:Function;
		public var okHandler:Function;
		public var cancelHandler:Function;
		public var prefixItemCode:String;
		public var prefixFullDrugDesc:String;
		public var showNonSuspendItemOnly:Boolean;
		public var fdnFlag:Boolean=false;
		public var callByChestItemMaint:Boolean=false;
	}
}