package hk.org.ha.event.pms.main.report {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ExportPivasRptEvent extends AbstractTideEvent 
	{
		private var _password:String;
		
		public function ExportPivasRptEvent(password:String):void 
		{
			super();
			_password = password;
			
		}

		public function get password():String{
			return _password;
		}
	}
}