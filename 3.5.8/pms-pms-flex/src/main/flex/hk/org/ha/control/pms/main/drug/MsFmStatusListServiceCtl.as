package hk.org.ha.control.pms.main.drug {
	
	import hk.org.ha.event.pms.main.drug.RetrieveMsFmStatusListEvent;
	import hk.org.ha.event.pms.main.drug.RefreshMsFmStatusListEvent;
	import hk.org.ha.model.pms.biz.drug.MsFmStatusListServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("msFmStatusListServiceCtl", restrict="true")]
	public class MsFmStatusListServiceCtl 
	{
		[In]
		public var msFmStatusListService:MsFmStatusListServiceBean;
		
		[Observer]
		public function retrieveMsFmStatusList(evt:RetrieveMsFmStatusListEvent):void
		{
			msFmStatusListService.retrieveMsFmStatusList(evt.fmStatus, refreshMsFmStatusListResult);
		}
		
		private function refreshMsFmStatusListResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RefreshMsFmStatusListEvent);
		}
		
	}
}
