package hk.org.ha.event.pms.main.cddh {
	
	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshCddhEnquiryMedicationEvent extends AbstractTideEvent 
	{
		private var _msgProp:SystemMessagePopupProp;
		private var _displayAll:Boolean;
		
		public function RefreshCddhEnquiryMedicationEvent(msgProp:SystemMessagePopupProp, displayAll:Boolean):void 
		{
			super();
			_msgProp = msgProp;
			_displayAll = displayAll;
		}
		
		public function get msgProp():SystemMessagePopupProp
		{
			return _msgProp;
		}
		
		public function get displayAll():Boolean
		{
			return _displayAll;
		}
	}
}