package hk.org.ha.event.pms.main.onestop {
		
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ClearOrderEvent extends AbstractTideEvent 
	{	
		private var _requestFromOneStopEntryView:Boolean;
		private var _orderNum:String;
		
		public function ClearOrderEvent(requestFromOneStopEntryView:Boolean, orderNum:String=null):void 
		{
			super();
			_requestFromOneStopEntryView = requestFromOneStopEntryView;
			_orderNum = orderNum;
		}        
		
		public function get requestFromOneStopEntryView():Boolean {
			return _requestFromOneStopEntryView;
		}
		
		public function get orderNum():String {
			return _orderNum;
		}
	}
}