package hk.org.ha.control.pms.main.report {
	import hk.org.ha.event.pms.main.report.ExportSuspendPrescRptEvent;
	import hk.org.ha.event.pms.main.report.PrintSuspendPrescRptEvent;
	import hk.org.ha.event.pms.main.report.RefreshEdsStatusDetailRptListEvent;
	import hk.org.ha.event.pms.main.report.RefreshExportSuspendPrescRptEvent;
	import hk.org.ha.event.pms.main.report.RetrieveEdsStatusDetailRptListEvent;
	import hk.org.ha.model.pms.biz.report.EdsStatusDetailRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	
	[Bindable]
	[Name("edsStatusDetailRptServiceCtl", restrict="true")]
	public class EdsStatusDetailRptServiceCtl 
	{
		[In]
		public var edsStatusDetailRptService:EdsStatusDetailRptServiceBean;
		
		[Observer]
		public function retrieveEdsStatusDetailRptList(evt:RetrieveEdsStatusDetailRptListEvent):void
		{
			edsStatusDetailRptService.retrieveEdsStatusDetailRptList(evt.dayRange, retrieveEdsStatusDetailRptListResult);
		}
		
		private function retrieveEdsStatusDetailRptListResult(evt:TideResultEvent):void
		{	
			evt.context.dispatchEvent(new RefreshEdsStatusDetailRptListEvent);
		}
		
		
		[Observer]
		public function printSuspendPrescRpt(evt:PrintSuspendPrescRptEvent):void
		{
			edsStatusDetailRptService.printSuspendPrescRpt(evt.edsStatusDetailRptSuspendItemList);
		}
		
		[Observer]
		public function exportSuspendPrescRpt(evt:ExportSuspendPrescRptEvent):void
		{
			edsStatusDetailRptService.exportFilteredEdsStatusDetailRpt(evt.edsStatusDetailRptSuspendItemList, exportSuspendPrescRptResult);
		}
		
		private function exportSuspendPrescRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshExportSuspendPrescRptEvent );
		}
		
	}
}
