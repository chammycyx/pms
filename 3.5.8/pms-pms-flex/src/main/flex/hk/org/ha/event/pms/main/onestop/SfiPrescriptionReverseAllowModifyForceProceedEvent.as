package hk.org.ha.event.pms.main.onestop {
		
	import flash.events.Event;
	
	import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
	
	import org.granite.tide.events.AbstractTideEvent;
		
	public class SfiPrescriptionReverseAllowModifyForceProceedEvent extends AbstractTideEvent 
	{
		private var _dispOrderId:Number;
		private var _dispOrderVersion:Number;
		private var _resultEvent:Event;
		
		public function SfiPrescriptionReverseAllowModifyForceProceedEvent(dispOrderId:Number, dispOrderVersion:Number, resultEvent:Event):void 
		{
			super();
			_dispOrderId = dispOrderId;
			_dispOrderVersion = dispOrderVersion;
			_resultEvent = resultEvent;
		}   
		
		public function get dispOrderId():Number
		{
			return _dispOrderId;
		}
		
		public function get dispOrderVersion():Number
		{
			return _dispOrderVersion;
		}
		
		public function get resultEvent():Event {
			return _resultEvent;
		}
	}
}