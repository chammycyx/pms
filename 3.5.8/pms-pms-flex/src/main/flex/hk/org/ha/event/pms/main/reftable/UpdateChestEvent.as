package hk.org.ha.event.pms.main.reftable {
	
	import hk.org.ha.model.pms.persistence.reftable.Chest;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateChestEvent extends AbstractTideEvent 
	{
		private var _chest:Chest; 
		
		public function UpdateChestEvent(chest:Chest):void 
		{
			super();
			_chest = chest;
		}
		
		public function get chest():Chest {
			return _chest;
		}
	}
}