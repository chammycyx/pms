package hk.org.ha.event.pms.main.delivery
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveMedProfileDueListEvent extends AbstractTideEvent
	{
		private var _caseNum:String;
		private var _hkid:String;
		
		public function RetrieveMedProfileDueListEvent(caseNum:String, hkid:String)
		{
			_caseNum = caseNum;
			_hkid = hkid;
		}
		
		public function get caseNum():String
		{
			return _caseNum;
		}
		
		public function get hkid():String
		{
			return _hkid;
		}
	}
}