package hk.org.ha.event.pms.main.vetting {

	import flash.events.Event;
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class OrderEditSaveItemEvent extends AbstractTideEvent {

		private var _callBackEvent:Event;
		
		public function OrderEditSaveItemEvent(callBackEventValue:Event=null) {
			super();			
			_callBackEvent = callBackEventValue;			
		}
		
		public function get callBackEvent():Event {
			return _callBackEvent;
		}		
	}
}