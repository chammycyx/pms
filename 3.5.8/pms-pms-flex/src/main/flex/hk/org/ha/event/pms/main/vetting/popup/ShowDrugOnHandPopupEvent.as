package hk.org.ha.event.pms.main.vetting.popup
{
	import hk.org.ha.model.pms.udt.vetting.CmsOrderSubType;
	import hk.org.ha.model.pms.udt.vetting.CmsOrderType;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class ShowDrugOnHandPopupEvent extends AbstractTideEvent
	{
		private var _onHandProfileList:ArrayCollection;
		
		public function ShowDrugOnHandPopupEvent(onHandProfileList:ArrayCollection)
		{
			super();
			_onHandProfileList = onHandProfileList;
		}
		
		public function get onHandProfileList():ArrayCollection {
			return _onHandProfileList;
		}
	}
}