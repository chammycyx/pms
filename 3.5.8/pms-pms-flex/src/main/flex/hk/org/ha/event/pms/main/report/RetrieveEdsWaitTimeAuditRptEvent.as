package hk.org.ha.event.pms.main.report {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveEdsWaitTimeAuditRptEvent extends AbstractTideEvent 
	{
		public static var CURRENT_WAIT_TIME:int = 0;
		public static var DAILY_REPORT:int = 1;
		public static var MONTHLY_REPORT:int = 2;
		
		private var _retrieveDate:Date;
		
		private var _callBack:Function;
		
		private var _reportType:int;
		
		public function RetrieveEdsWaitTimeAuditRptEvent(retrieveDate:Date, reportType:int, callBack:Function):void 
		{
			super();
			_retrieveDate = retrieveDate;
			_reportType = reportType;
			_callBack = callBack;
		}
		
		public function get retrieveDate():Date {
			return _retrieveDate;
		} 
		
		public function get callBack():Function {
			return _callBack;
		}

		public function get isCurrentWaitTime():Boolean {
			return _reportType == CURRENT_WAIT_TIME;
		}
		
		public function get isDailyReport():Boolean {
			return _reportType == DAILY_REPORT;
		}	
		
		public function get isMonthlyReport():Boolean {
			return _reportType == MONTHLY_REPORT;
		}
	}
}