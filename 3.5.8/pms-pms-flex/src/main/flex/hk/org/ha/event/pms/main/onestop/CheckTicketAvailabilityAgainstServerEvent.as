package hk.org.ha.event.pms.main.onestop {
	
	import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CheckTicketAvailabilityAgainstServerEvent extends AbstractTideEvent 
	{
		private var _ticketDate:Date;
		private var _ticketNum:String;
		private var _orderId:Number;
		private var _oneStopOrderType:OneStopOrderType;
		private var _followUpFunc:Function;
		
		public function CheckTicketAvailabilityAgainstServerEvent(ticketDate:Date, ticketNum:String, orderId:Number, 
																  oneStopOrderType:OneStopOrderType, followUpFunc:Function=null):void {
			super();
			_ticketDate = ticketDate;
			_ticketNum = ticketNum;	
			_orderId = orderId;
			_oneStopOrderType = oneStopOrderType;
			_followUpFunc = followUpFunc;
		}
		
		public function get ticketDate():Date
		{
			return _ticketDate;
		}
		
		public function get ticketNum():String
		{
			return _ticketNum;
		}
		
		public function get orderId():Number
		{
			return _orderId;
		}
		
		public function get oneStopOrderType():OneStopOrderType
		{
			return _oneStopOrderType;
		}
		
		public function get followUpFunc():Function
		{
			return _followUpFunc;
		}
	}
}