package hk.org.ha.event.pms.main.onestop {
		
	import flash.events.Event;
	
	import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
	
	import org.granite.tide.events.AbstractTideEvent;
		
	public class RetrieveOrderStatusEvent extends AbstractTideEvent 
	{		
		private var _orderId:Number;
		private var _medOrderVersion:Number;
		private var _refillOrderId:Number;
		private var _callbackEvent:Event;
		
		public function RetrieveOrderStatusEvent(medOrderId:Number, medOrderVersion:Number, refillOrderId:Number, callbackEvent:Event):void 
		{
			super();
			_orderId = medOrderId;
			_medOrderVersion = medOrderVersion;
			_refillOrderId = refillOrderId;
			_callbackEvent = callbackEvent;
		}
		
		public function get orderId():Number {
			return _orderId;
		}
		
		public function get medOrderVersion():Number {
			return _medOrderVersion;
		}
		
		public function get refillOrderId():Number
		{
			return _refillOrderId;
		}
		
		public function get callbackEvent():Event {
			return _callbackEvent;
		}
	}
}