package hk.org.ha.event.pms.main.vetting.popup {
	
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowOrderEditItemPopupEvent extends AbstractTideEvent 
	{				
		private var _medOrderItem:MedOrderItem;		
		private var _fmStatus:String;
		private var _okHandler:Function;		
		private var _doubleClickHandler:Function;
		private var _cancelHandler:Function;
		
		public function ShowOrderEditItemPopupEvent( medOrderItemValue:MedOrderItem, okHandlerVal:Function=null, doubleClickHandlerVal:Function=null, cancelHandlerVal:Function=null ):void 
		{
			super();
			_medOrderItem 			= medOrderItemValue;
			_okHandler 				= okHandlerVal;
			_doubleClickHandler 	= doubleClickHandlerVal;
			_cancelHandler		= cancelHandlerVal;
		}
		
		public function get medOrderItem():MedOrderItem 
		{
			return _medOrderItem;
		}

		public function get okHandler():Function
		{
			return _okHandler;
		}
		
		public function get doubleClickHandler():Function
		{
			return _doubleClickHandler;
		}
		
		public function get cancelHandler():Function
		{
			return _cancelHandler;
		}
	}
}
