package hk.org.ha.control.pms.main.drug {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.drug.RetrieveMsFmStatusEvent;
	import hk.org.ha.model.pms.biz.drug.MsFmStatusServiceBean;
	import hk.org.ha.model.pms.dms.persistence.MsFmStatus;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("msFmStatusServiceCtl", restrict="true")]
	public class MsFmStatusServiceCtl 
	{
		[In]
		public var msFmStatusService:MsFmStatusServiceBean;
		
		[In]
		public var msFmStatus:MsFmStatus;
		
		private var successEvent:Event;
		
		private var failEvent:Event;
				
		[Observer]
		public function retrieveMsFmStatus(evt:RetrieveMsFmStatusEvent):void
		{
			successEvent = evt.successEvent;
			failEvent = evt.failEvent;
			
			In(Object(msFmStatusService).retrieveSuccess)
			msFmStatusService.retrieveMsFmStatus(evt.fmStatus, retrieveMsFmStatusResult);
		}
		
		private function retrieveMsFmStatusResult(evt:TideResultEvent):void 
		{
			if(Object(msFmStatusService).retrieveSuccess){
				if(successEvent!=null){
					evt.context.dispatchEvent( successEvent );
				}
			}else{
				if(failEvent!=null){
					evt.context.dispatchEvent( failEvent );
				}
			}
		}
		
	}
}
