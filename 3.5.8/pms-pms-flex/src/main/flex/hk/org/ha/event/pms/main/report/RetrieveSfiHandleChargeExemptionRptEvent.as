package hk.org.ha.event.pms.main.report {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSfiHandleChargeExemptionRptEvent extends AbstractTideEvent 
	{
		private var _rptDate:Date;
		
		public function RetrieveSfiHandleChargeExemptionRptEvent( rptDate:Date):void 
		{
			super();
			_rptDate = rptDate;
		}
		
		public function get rptDate():Date{
			return _rptDate;
		} 
	}
}