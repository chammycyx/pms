package hk.org.ha.event.pms.main.vetting.popup {
	
	import flash.events.Event;
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ValidatePharmRemarkLogonEvent extends AbstractTideEvent {		
		private var _logonUser:String;
		private var _logonPw:String;
		private var _logonCallbackEvent:Event;
		
		public function ValidatePharmRemarkLogonEvent(logonUser:String, logonPw:String, logonCallbackEvent:Event) {
			super();
			_logonUser = logonUser;
			_logonPw = logonPw;
			_logonCallbackEvent = logonCallbackEvent;
		}

		public function get logonUser():String {
			return _logonUser;
		}
		
		public function get logonPw():String {
			return _logonPw;
		}

		public function get logonCallbackEvent():Event {
			return _logonCallbackEvent;
		}		
	}
}
