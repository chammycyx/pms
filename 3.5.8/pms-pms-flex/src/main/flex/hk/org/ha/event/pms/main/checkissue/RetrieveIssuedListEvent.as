package hk.org.ha.event.pms.main.checkissue
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveIssuedListEvent extends AbstractTideEvent 
	{		
		private var _ticketDate:Date;
		
		public function RetrieveIssuedListEvent(ticketDate:Date):void 
		{
			super();
			_ticketDate = ticketDate;
		}
		
		public function get ticketDate():Date {
			return _ticketDate;
		}
	}
}