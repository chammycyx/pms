package hk.org.ha.event.pms.main.reftable.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowPivasDrugManufacturerPopupEvent extends AbstractTideEvent 
	{
		private var _pivasDrugManufacturerPopupProp:PivasDrugManufacturerPopupProp;
		
		public function ShowPivasDrugManufacturerPopupEvent(pivasDrugManufacturerPopupProp:PivasDrugManufacturerPopupProp):void 
		{
			super();
			_pivasDrugManufacturerPopupProp = pivasDrugManufacturerPopupProp;
		}
		
		public function get pivasDrugManufacturerPopupProp():PivasDrugManufacturerPopupProp{
			return _pivasDrugManufacturerPopupProp;
		}
	}
}