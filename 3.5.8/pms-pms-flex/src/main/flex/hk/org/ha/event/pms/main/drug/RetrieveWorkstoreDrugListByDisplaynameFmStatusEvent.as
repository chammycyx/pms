package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dms.persistence.DmDrug;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveWorkstoreDrugListByDisplaynameFmStatusEvent extends AbstractTideEvent
	{
		private var _displayName:String;		
		private var _fmStatus:String;
		
		public function RetrieveWorkstoreDrugListByDisplaynameFmStatusEvent(displayName:String, fmStatus:String):void 
		{
			super();
			_displayName = displayName;
			_fmStatus = fmStatus;
		}
		
		public function get displayName():String
		{
			return _displayName;
		}
		
		public function get fmStatus():String
		{
			return _fmStatus;
		}
	}
}