package hk.org.ha.event.pms.main.charging {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.charging.MpSfiInvoice;
	
	public class RefreshMpSfiInvoiceItemListEvent extends AbstractTideEvent 
	{	
		private var _mpSfiInvoice:MpSfiInvoice;
		private var _messageCode:String;
		
		public function RefreshMpSfiInvoiceItemListEvent(mpSfiInvoice:MpSfiInvoice, messageCode:String):void 
		{
			super();
			_mpSfiInvoice = mpSfiInvoice;
			_messageCode = messageCode;
		}
		
		public function get mpSfiInvoice():MpSfiInvoice {
			return _mpSfiInvoice;
		}
		
		public function get messageCode():String {
			return _messageCode;
		}
	}
}