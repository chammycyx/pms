package hk.org.ha.event.pms.main.alert.mds.popup
{
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	import hk.org.ha.model.pms.vo.alert.mds.MedProfileAlertMsg;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	
	public class ShowMedProfileAlertMsgPopupEvent extends AbstractTideEvent {
		
		private var _state:String;
		
		private var _medProfileAlertMsgList:ListCollectionView;
		
		private var _ackCallback:Function;
		
		private var _suspendCallback:Function;
		
		private var _withholdCallback:Function;
		
		private var _medProfile:MedProfile;
		
		public function ShowMedProfileAlertMsgPopupEvent(state:String="enquire", medProfileAlertMsgList:ListCollectionView=null) {
			super();
			_state = state;
			_medProfileAlertMsgList = medProfileAlertMsgList;
		}
		
		public function get state():String {
			return _state;
		}
		
		public function get medProfileAlertMsgList():ListCollectionView {
			if (_medProfileAlertMsgList == null) {
				return new ArrayCollection();
			}
			else {
				return _medProfileAlertMsgList;
			}
		}
		
		public function set medProfileAlertMsgList(medProfileAlertMsgList:ListCollectionView):void {
			_medProfileAlertMsgList = medProfileAlertMsgList;
		}
		
		public function set medProfileAlertMsg(medProfileAlertMsg:MedProfileAlertMsg):void {
			_medProfileAlertMsgList = new ArrayCollection(new Array(medProfileAlertMsg));
		}
		
		public function get ackCallback():Function {
			return _ackCallback;
		}
		
		public function set ackCallback(ackCallback:Function):void {
			_ackCallback = ackCallback;
		}
		
		public function get suspendCallback():Function {
			return _suspendCallback;
		}
		
		public function set suspendCallback(suspendCallback:Function):void {
			_suspendCallback = suspendCallback;
		}
		
		public function get withholdCallback():Function {
			return _withholdCallback;
		}
		
		public function set withholdCallback(withholdCallback:Function):void {
			_withholdCallback = withholdCallback;
		}
		
		public function get medProfile():MedProfile {
			return _medProfile;
		}
		
		public function set medProfile(medProfile:MedProfile):void {
			_medProfile = medProfile;
		}
	}
}
