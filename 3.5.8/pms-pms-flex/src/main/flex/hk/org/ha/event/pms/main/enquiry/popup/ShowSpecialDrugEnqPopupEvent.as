package hk.org.ha.event.pms.main.enquiry.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowSpecialDrugEnqPopupEvent extends AbstractTideEvent 
	{		
		private var _sortByDesc:Boolean;
		
		public function ShowSpecialDrugEnqPopupEvent(sortByDesc:Boolean):void 
		{
			super();
			_sortByDesc = sortByDesc;
		}
		
		public function get sortByDesc():Boolean{
			return _sortByDesc;
		}
	}
}