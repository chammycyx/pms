package hk.org.ha.control.pms.main.vetting {

	import hk.org.ha.event.pms.main.vetting.CheckNumOfCapdSplitEvent;
	import hk.org.ha.event.pms.main.vetting.ClearCapdSplitEvent;
	import hk.org.ha.event.pms.main.vetting.RetrieveCapdSplitEvent;
	import hk.org.ha.event.pms.main.vetting.ShowCapdSplitErrMsgEvent;
	import hk.org.ha.event.pms.main.vetting.UpdateCapdSplitEvent;
	import hk.org.ha.model.pms.biz.vetting.CapdSplitServiceBean;
	import hk.org.ha.model.pms.persistence.disp.MedOrder;
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	import hk.org.ha.model.pms.persistence.disp.PharmOrder;
	import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
	import hk.org.ha.model.pms.vo.vetting.CapdSplit;
	import hk.org.ha.model.pms.vo.vetting.CapdSplitItem;
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("capdSplitServiceCtl", restrict="true")]
	public class CapdSplitServiceCtl {

		[In]
		public var ctx:Context;
		
		[In]
		public var capdSplitService:CapdSplitServiceBean;
		
		[In]
		public var pharmOrder:PharmOrder;
		
		private var updateCapdSplitCallback:Function;
		
		private var currentCapdSplit:CapdSplit;
		
		[Observer]
		public function retrieveCapdSplit(evt:RetrieveCapdSplitEvent):void {
			In(Object(capdSplitService).errMsg)
			In(Object(capdSplitService).errItemCode)
			capdSplitService.retrieveCapdSplit(retrieveCapdSplitResult);
		}
		
		private function retrieveCapdSplitResult(evt:TideResultEvent):void {
			if ( (Object(capdSplitService).errMsg) != "" ) {
				evt.context.dispatchEvent(new ShowCapdSplitErrMsgEvent(Object(capdSplitService).errMsg as String, Object(capdSplitService).errItemCode as String));
			} else {
				evt.context.dispatchEvent(new CheckNumOfCapdSplitEvent());
			}
		}
		
		[Observer]
		public function updateCapdSplit(evt:UpdateCapdSplitEvent):void {
			updateCapdSplitCallback = evt.callbackFunc;
			currentCapdSplit = evt.capdSplit;
			capdSplitService.updateCapdSplit(evt.capdSplit, evt.setGenNewVoucherFlag, updateCapdSplitResult);
		}
		
		private function updateCapdSplitResult(evt:TideResultEvent):void {
			var medOrder:MedOrder = pharmOrder.medOrder;
			for each ( var medOrderItem:MedOrderItem in medOrder.medOrderItemList ) {
				if ( medOrderItem.capdRxDrug != null ) {
					medOrderItem.capdRxDrug.updateCapdItemQty = false;
					medOrderItem.capdRxDrug.updateGroupNumFlag = false;
				}
			}
			
			for each ( var capdSplitItem:CapdSplitItem in currentCapdSplit.capdSplitItemList ) {
				var pharmOrderItem:PharmOrderItem = pharmOrder.getPharmOrderItemByOrgItemNum(capdSplitItem.itemNum);
				pharmOrderItem.issueQty = capdSplitItem.dispQty;
			}
			
			if (updateCapdSplitCallback != null) {
				updateCapdSplitCallback();
			}
		}
		
		[Observer]
		public function clearCapdSplit(evt:ClearCapdSplitEvent):void {
			updateCapdSplitCallback = evt.callbackFunc;
			capdSplitService.clearCapdSplit(clearCapdSplitResult);
		}
		
		private function clearCapdSplitResult(evt:TideResultEvent):void {
			if (updateCapdSplitCallback != null) {
				updateCapdSplitCallback();
			}
		}
	}
}
