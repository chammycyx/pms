package hk.org.ha.event.pms.main.enquiry
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshReprintMpSfiInvoiceEvent extends AbstractTideEvent 
	{				
		private var _errMsg:String;
		
		public function RefreshReprintMpSfiInvoiceEvent(errMsg:String):void
		{
			super();
			_errMsg = errMsg;
		}
		
		public function get errMsg():String{
			return _errMsg;
		}
	}
}