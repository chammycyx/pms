package hk.org.ha.event.pms.main.checkissue
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshBatchIssueViewEvent extends AbstractTideEvent 
	{			
		private var _showSystemMsg:Boolean;
		
		public function RefreshBatchIssueViewEvent(showSystemMsg:Boolean=true):void 
		{
			super();
			_showSystemMsg = showSystemMsg;
		}
		
		public function get showSystemMsg():Boolean{
			return _showSystemMsg;
		}
	}
}