package hk.org.ha.event.pms.main.checkissue
{
	import hk.org.ha.model.pms.vo.checkissue.CheckIssueSummary;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshDrugCheckIssueViewEvent extends AbstractTideEvent 
	{		
		private var _iqdsMessageContentList:ArrayCollection;
		private var _checkIssueSummary:CheckIssueSummary;
		private var _action:String;

		public function RefreshDrugCheckIssueViewEvent(action:String, checkIssueSummary:CheckIssueSummary, iqdsMessageContentList:ArrayCollection=null):void 
		{
			super();
			_action = action;
			_checkIssueSummary = checkIssueSummary;
			_iqdsMessageContentList = iqdsMessageContentList;
		}
		
		public function get action():String{
			return _action;
		}
		
		public function get checkIssueSummary():CheckIssueSummary {
			return _checkIssueSummary;
		}
		
		public function get iqdsMessageContentList():ArrayCollection {
			return _iqdsMessageContentList;
		}
	}
}