package hk.org.ha.event.pms.main.report {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class PrintEdsWaitTimeAuditRptEvent extends AbstractTideEvent 
	{
		
		public static const DAILY_REPORT:int = 1;
		public static const MONTHLY_REPORT:int = 2;
		
		private var _reportType:int = 1;
		
		public function PrintEdsWaitTimeAuditRptEvent(reportType:int):void 
		{
			super();
			_reportType = reportType;
		}
		
		public function get isDailyReport():Boolean {
			return _reportType == DAILY_REPORT;
		}
		
		public function get isMonthlyReport():Boolean {
			return _reportType == MONTHLY_REPORT;
		}
	}
}