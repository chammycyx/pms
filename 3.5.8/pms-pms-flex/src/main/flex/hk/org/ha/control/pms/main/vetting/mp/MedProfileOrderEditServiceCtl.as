package hk.org.ha.control.pms.main.vetting.mp
{	
	import flash.events.Event;
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	
	import hk.org.ha.event.pms.main.vetting.mp.CalculatePivasDueDateAndSupplyDateEvent;
	import hk.org.ha.event.pms.main.vetting.mp.CancelFromManualEntryItemEditEvent;
	import hk.org.ha.event.pms.main.vetting.mp.CancelFromOrderEditEvent;
	import hk.org.ha.event.pms.main.vetting.mp.ChangeItemCodeFromManualEntryItemEditEvent;
	import hk.org.ha.event.pms.main.vetting.mp.GetNextAdminDueDateEvent;
	import hk.org.ha.event.pms.main.vetting.mp.ReactivateDiscontinuedItemEvent;
	import hk.org.ha.event.pms.main.vetting.mp.RetrievePivasItemEditInfoEvent;
	import hk.org.ha.event.pms.main.vetting.mp.RetrieveReplenishmentFromBackendContextEvent;
	import hk.org.ha.event.pms.main.vetting.mp.SaveItemFromManualEntryItemEditEvent;
	import hk.org.ha.event.pms.main.vetting.mp.SaveItemFromOrderEditEvent;
	import hk.org.ha.event.pms.main.vetting.mp.SplitChemoMedProfileItemEvent;
	import hk.org.ha.event.pms.main.vetting.mp.UpdateManualEntryVettingAssociationEvent;
	import hk.org.ha.event.pms.main.vetting.mp.UpdateMedProfileInMemoryEvent;
	import hk.org.ha.event.pms.main.vetting.mp.UpdateMedProfileItemListInMemoryEvent;
	import hk.org.ha.event.pms.main.vetting.mp.UpdatePivasDetailToContextEvent;
	import hk.org.ha.event.pms.main.vetting.mp.popup.RetrievePivasSupplyDateListInfoByDueDateEvent;
	import hk.org.ha.event.pms.main.vetting.mp.show.ShowManualEntryItemEditViewEvent;
	import hk.org.ha.model.pms.biz.vetting.mp.MedProfileOrderEditServiceBean;
	import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
	import hk.org.ha.model.pms.persistence.medprofile.Replenishment;
	import hk.org.ha.model.pms.persistence.medprofile.ReplenishmentItem;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("medProfileOrderEditServiceCtl", restrict="true")]
	public class MedProfileOrderEditServiceCtl
	{
		[In]
		public var medProfileOrderEditService:MedProfileOrderEditServiceBean;
		
		[In]
		public var ctx:Context;
		
		[In]
		public var medProfileItemList:ArrayCollection;		
		
		[In]
		public var medProfileItem:MedProfileItem;				
		
		[In]
		public var medProfile:MedProfile;
		
		[In]
		public var purchaseRequestList:ArrayCollection;
		
		[In]
		public var replenishmentList:ArrayCollection;
		
		private var callbackEvent:Event;
		
		private var callBackFunction:Function;
		
		private var associatedMpiList:ListCollectionView;
		
		private var cancelFromOrderEditEvent:CancelFromOrderEditEvent;
		
		private function getMedProfileItemListItemIndex(medProfileItemIn:MedProfileItem):int {
			var index:int = 0;
			for each (var mpi:MedProfileItem in medProfileItemList) {
				if (mpi.id == medProfileItemIn.id && mpi.orgItemNum == medProfileItemIn.orgItemNum) {
					return index;
				}
				++index;
			}
			return -1;
		}
		
		[Observer]
		public function retrieveReplenishmentFromBackendContext(evt:RetrieveReplenishmentFromBackendContextEvent):void {
			medProfileOrderEditService.retrieveReplenishmentFromBackendContext(
				evt.id,
				function(event:TideResultEvent):void {
					if (evt.callBackFunction != null) {
						evt.callBackFunction(event.result);				
					}
				}
			);
		}
		
		[Observer]
		public function cancelFromOrderEdit(evt:CancelFromOrderEditEvent):void {
			cancelFromOrderEditEvent = evt;
			medProfileOrderEditService.cancelFromOrderEdit(evt.medProfileItem.id, evt.medProfileItem.orgItemNum,cancelFromOrderEditResult);
		}
		
		private function cancelFromOrderEditResult(evt:TideResultEvent):void {
			var selectedIndex:Number = getMedProfileItemListItemIndex(medProfileItem);
			if (selectedIndex >= 0) {
				medProfileItemList.setItemAt(medProfileItem,selectedIndex);
				medProfileItem.relink(medProfile);
				medProfileItem.itemDetailChecked = cancelFromOrderEditEvent.medProfileItem.itemDetailChecked;
			}
			
			//Replenishment logic update
			//PMS-3255
			var deliveryItem:DeliveryItem;
			for each ( var replenishment:Replenishment in replenishmentList ) {
				
				if(replenishment.medProfileMoItem.medProfileItem.id == 	medProfileItem.id 
					&& replenishment.medProfileMoItem.itemNum == medProfileItem.medProfileMoItem.itemNum){
					//PMS-3260
					replenishment.medProfileMoItem = medProfileItem.medProfileMoItem;
					for each ( var replenishmentItem:ReplenishmentItem in replenishment.replenishmentItemList ) {
						for each (var medProfilePoItem:MedProfilePoItem in medProfileItem.medProfileMoItem.medProfilePoItemList){
							if(replenishmentItem.medProfilePoItem.id == medProfilePoItem.id){
								deliveryItem = replenishmentItem.medProfilePoItem.deliveryItem;
								replenishmentItem.medProfilePoItem = medProfilePoItem;	
								replenishmentItem.medProfilePoItem.deliveryItem = deliveryItem;
							}
						}
					}
					break;
				}
				
			}
			evt.context.dispatchEvent(cancelFromOrderEditEvent.callBackEvent);
		}
		
		[Observer]
		public function cancelFromManualEntryItemEditEdit(evt:CancelFromManualEntryItemEditEvent):void {			
			callbackEvent = evt.callBackEvent;	
			medProfileOrderEditService.cancelFromManualEntryItemEdit(evt.itemNumList,cancelFromManualEntryItemEditResult);
		}	
		
		private function cancelFromManualEntryItemEditResult(evt:TideResultEvent):void {
			var returnList:ListCollectionView = evt.result as ListCollectionView;
			if (returnList.length > 0) {
				var mpi:MedProfileItem;
				var mpiDictionary:Dictionary = new Dictionary;
				for each (mpi in returnList) {
					mpiDictionary[mpi.orgItemNum] = mpi;
				}
				for (var i:int=0; i<medProfileItemList.length; i++) {
					mpi = mpiDictionary[MedProfileItem(medProfileItemList.getItemAt(i)).orgItemNum] as MedProfileItem;
					if (mpi != null) {
						medProfileItemList.setItemAt(mpi, i);
						mpi.relink(medProfile);
					}
				}
			}
			if (callbackEvent != null) {
				evt.context.dispatchEvent(callbackEvent);
			}
		}
		
		[Observer]
		public function saveItemFromOrderEdit(evt:SaveItemFromOrderEditEvent):void {
			var frontEndMedProfileItem:MedProfileItem = evt.medProfileItem;	
			frontEndMedProfileItem.dislink();
			
			var saveItemFromOrderEditResult:Function = function(tideResultEvent:TideResultEvent):void {
				evt.medProfileItem.relink(medProfile);
				var selectedIndex:int = getMedProfileItemListItemIndex(frontEndMedProfileItem);
				if (selectedIndex >= 0) {
					medProfileItemList.setItemAt(frontEndMedProfileItem,selectedIndex);
				}
				if ( evt.callbackEvent != null ) {
					tideResultEvent.context.dispatchEvent( evt.callbackEvent );
				}
				if ( evt.callbackFunc != null ) {
					evt.callbackFunc();
				}
			}
			
			medProfileOrderEditService.saveItemFromOrderEdit(frontEndMedProfileItem,saveItemFromOrderEditResult);
		}
		
		[Observer]
		public function saveItemFromManualEntryItemEdit(evt:SaveItemFromManualEntryItemEditEvent):void {
			associatedMpiList = evt.medProfileItemList;
			callbackEvent = evt.callBackEvent;
			callBackFunction = evt.callBackFunc;
			for each (var mpi:MedProfileItem in associatedMpiList) {
				mpi.dislink();
			}
			medProfileOrderEditService.saveItemFromManualEntryItemEdit(
				evt.medProfileItemList,
				saveItemFromManualEntryItemEditResult
			);
		}
		
		private function saveItemFromManualEntryItemEditResult(tideResultEvent:TideResultEvent):void {
			var mpi:MedProfileItem;
			var discontModifyMpi:MedProfileItem;
			var mpiDictionary:Dictionary = new Dictionary;
			var newItemList:ArrayCollection = new ArrayCollection;
			var discontModifyMpiDictionary:Dictionary = new Dictionary;
			
			for each (mpi in associatedMpiList) {
				mpi.relink(medProfile);				
			}
			for each (mpi in medProfileItemList) {				
				mpiDictionary[mpi.orgItemNum] = mpi;
			}
			for each (mpi in tideResultEvent.result) {
				discontModifyMpiDictionary[mpi.orgItemNum] = mpi;
			}
			for (var i:int=0; i<associatedMpiList.length; i++) {
				mpi = mpiDictionary[MedProfileItem(associatedMpiList.getItemAt(i)).orgItemNum] as MedProfileItem;
				if (mpi != null) {
					discontModifyMpi = discontModifyMpiDictionary[MedProfileItem(associatedMpiList.getItemAt(i)).orgItemNum] as MedProfileItem;
					if (discontModifyMpi != null) {
						mpi.orgItemNum = discontModifyMpi.newOrgItemNum;
						mpi.medProfileMoItem.itemNum = discontModifyMpi.newOrgItemNum;
					}
					var currentIndex:int = medProfileItemList.getItemIndex(mpi);
					medProfileItemList.setItemAt(mpi, currentIndex);
					mpi.discontModified = false;
				} else {
					newItemList.addItem(associatedMpiList.getItemAt(i));
				}
			}
			if (newItemList.length > 0) {
				for each (mpi in newItemList) {
					medProfileItemList.addItem(mpi);
				}
			}			
			if (callbackEvent != null) {
				tideResultEvent.context.dispatchEvent(callbackEvent);
			}
			if (callBackFunction != null) {				
				callBackFunction(tideResultEvent.result);
			}
		}
		
		[Observer]
		public function updateMedProfileInMemory(evt:UpdateMedProfileInMemoryEvent):void {
			
			medProfileOrderEditService.updateMedProfile(evt.medProfile, evt.patientInfoUpdated,
				function(resultEvt:TideResultEvent):void {
					if (evt.callBackEvent) {
						resultEvt.context.dispatchEvent(evt.callBackEvent);
					} else if (evt.callBackFunction != null) {
						evt.callBackFunction(resultEvt.result);
					}
				}
			);
		}
		
		[Observer]
		public function reactivateDiscontinuedItem(evt:ReactivateDiscontinuedItemEvent):void {
			medProfileOrderEditService.reactivateDiscontinuedItem(evt.medProfileItem,reactivateDiscontinuedItemResult);
		}
		
		private function reactivateDiscontinuedItemResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new ShowManualEntryItemEditViewEvent(new ArrayCollection([evt.result]), evt.result as MedProfileItem, null, true));
		}
		
		[Observer]
		public function updateManualEntryVettingAssociation(evt:UpdateManualEntryVettingAssociationEvent):void {
			medProfileOrderEditService.updateManualEntryVettingAssociation(evt.map);
		}
		
		[Observer]
		public function changeItemCodeFromManualEntryItemEdit(evt:ChangeItemCodeFromManualEntryItemEditEvent):void {
			medProfileOrderEditService.changeItemCodeFromManualEntryItemEdit(
				evt.itemCode,
				evt.displayName,
				function(event:TideResultEvent):void {
					if (evt.callBackFunction != null) {
						evt.callBackFunction(event.result);
					}
				}
			);
		}
		
		[Observer]
		public function updateBackendMedProfileItemListEvent(evt:UpdateMedProfileItemListInMemoryEvent):void {
			
			if( evt.medProfileItemList.length > 0 ){
				for each (var mpi:MedProfileItem in evt.medProfileItemList) {
					mpi.dislink();
				}
				medProfileOrderEditService.updateBackendMedProfileItemList(
					evt.medProfileItemList,
					function(tideResultEvent:TideResultEvent):void {
						for each (var mpi:MedProfileItem in evt.medProfileItemList) {
							mpi.relink(medProfile);
						}
						if ( evt.callBackFunc != null ) {
							evt.callBackFunc();
						}
					}
				);
			}else{
				if ( evt.callBackFunc != null ) {
					evt.callBackFunc();
				}
			}
		}
		
		private function getCurrentMedProfileItemIndex(mpiValue:MedProfileItem):int
		{
			for ( var i:int=0; i<medProfileItemList.length; i++ ) {
				if ( mpiValue.id == MedProfileItem(medProfileItemList.getItemAt(i)).id ) {
					return i;
				}
			}
			return -1;
		}			
		
		[Observer]
		public function getNextAdminDueDate(evt:GetNextAdminDueDateEvent):void {
			medProfileOrderEditService.retrieveNextAdminDueDate(
				evt.dailyFreqCodeList,
				evt.pivasFlag,
				function(tideResultEvent:TideResultEvent):void {
					if (evt.callBackFunc != null) {
						evt.callBackFunc(tideResultEvent.result);
					}					
				}
			);
		}
		
		[Observer]
		public function retrievePivasSupplyDateList(evt:RetrievePivasSupplyDateListInfoByDueDateEvent):void {
			medProfileOrderEditService.retrievePivasSupplyDateListInfoByDueDate(evt.dueDate, function(tideResultEvent:TideResultEvent):void {
				if (evt.callbackFunc != null) {
					evt.callbackFunc(tideResultEvent.result);
				}
			});
		}
		
		[Observer]
		public function retrievePivasItemEditInfo(evt:RetrievePivasItemEditInfoEvent):void {
			medProfileOrderEditService.retrievePivasItemEditInfo(evt.medProfileMoItem, function(tideResultEvent:TideResultEvent):void {
				if (evt.callbackFunc != null) {
					evt.callbackFunc(tideResultEvent.result);
				}
			})
		}
		
		[Observer]
		public function updatePivasDetailToContext(evt:UpdatePivasDetailToContextEvent):void {
			medProfileOrderEditService.updatePivasDetailToContext(evt.pivasWorklist);
		}
		
		[Observer]
		public function calculatePivasDueDateAndSupplyDate(evt:CalculatePivasDueDateAndSupplyDateEvent):void {
			medProfileOrderEditService.calculatePivasDueDateAndSupplyDate(evt.dailyFreqCode, evt.dueDate,
				function(resultEvt:TideResultEvent):void {
					evt.callbackFunc(resultEvt.result[0], resultEvt.result[1]);
				}
			);
		}
		
		[Observer]
		public function splitChemoMedProfileItem(evt:SplitChemoMedProfileItemEvent):void {
			medProfileOrderEditService.splitChemoMedProfileItem(evt.medProfileItem, evt.chemoItemList,
				function(resultEvt:TideResultEvent):void {
					evt.callbackFunc(resultEvt.result);
				}
			); 
		}
	}
}
