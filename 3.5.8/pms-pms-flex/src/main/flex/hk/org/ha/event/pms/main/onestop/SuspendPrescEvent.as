package hk.org.ha.event.pms.main.onestop
{
	import flash.events.Event;
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;

	public class SuspendPrescEvent extends AbstractTideEvent
	{
		private var _dispOrderId:Number;
		private var _suspendCode:String;
		private var _dispOrderVersion:Number;
		private var _caller:String;
		private var _resultEvent:Event;
//		private var _resultFaultEvent:Event;
		
//		public function SuspendPrescEvent(dispOrderId:Number, dispOrderVersion:Number, suspendCode:String, caller:String,
//										  resultEvent:Event=null, resultFaultEvent:Event=null)
		public function SuspendPrescEvent(dispOrderId:Number, dispOrderVersion:Number, suspendCode:String, caller:String, resultEvent:Event)
		{
			super();
			_dispOrderId = dispOrderId;
			_dispOrderVersion = dispOrderVersion;
			_suspendCode = suspendCode;
			_caller = caller;
			_resultEvent = resultEvent;
//			_resultFaultEvent = resultFaultEvent;
		}
		
		public function get dispOrderId():Number {
			return _dispOrderId;
		}
		
		public function get dispOrderVersion():Number {
			return _dispOrderVersion;
		}
		
		public function get suspendCode():String {
			return _suspendCode;
		}
		
		public function get caller():String {
			return _caller;
		}
		
		public function get resultEvent():Event {
			return _resultEvent;
		}
		
//		public function get resultFaultEvent():Event {
//			return _resultFaultEvent;
//		}
	}
}