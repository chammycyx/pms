package hk.org.ha.control.pms.main.reftable {
	
	import hk.org.ha.event.pms.main.reftable.RefreshSpecialtyMappingViewEvent;
	import hk.org.ha.event.pms.main.reftable.RetrievePmsSpecMappingListEvent;
	import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
	import hk.org.ha.model.pms.biz.reftable.PmsSpecMappingListServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("pmsSpecMappingListServiceCtl", restrict="true")]
	public class PmsSpecMappingListServiceCtl 
	{
		[In]
		public var pmsSpecMappingListService:PmsSpecMappingListServiceBean;
		
		[Observer]
		public function retrievePmsSpecMappingList(evt:RetrievePmsSpecMappingListEvent):void
		{
			In(Object(pmsSpecMappingListService).retrieveSuccess);
			pmsSpecMappingListService.retrievePmsSpecMappingList(retrievePmsSpecMappingListResult);
		}
		
		private function retrievePmsSpecMappingListResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new CloseLoadingPopupEvent());
			evt.context.dispatchEvent(new RefreshSpecialtyMappingViewEvent(Object(pmsSpecMappingListService).retrieveSuccess));
		}
		
	}
}
