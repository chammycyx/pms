package hk.org.ha.event.pms.main.alert.mds.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;
	
	public class ShowOverrideReasonOpPopupEvent extends AbstractTideEvent {
		
		private var _alertMsg:AlertMsg;
		
		public var ddiAlertIndex:Number;
		
		public var proceedFunc:Function;
		
		public var cancelFunc:Function;
		
		public var prescModeFlag:Boolean;
		
		public function ShowOverrideReasonOpPopupEvent(alertMsg:AlertMsg) {
			super();
			_alertMsg = alertMsg;
			ddiAlertIndex = NaN;
			prescModeFlag = false;
		}
		
		public function get alertMsg():AlertMsg {
			return _alertMsg;
		}
	}
}
