package hk.org.ha.fmk.pms.flex.utils {
	public class CaseValidator {
		
		public static function isCaseNumValid(caseNum:String):Boolean {
			if (new RegExp("^[A-Za-z][A-Za-z][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][A-Za-z0-9]{0,2}$").test(caseNum) || 
				new RegExp("^[A-Za-z][A-Za-z][A-Za-z0-9 ]{2}[0-9][0-9][0-9][0-9][0-9][0-9][0-9][A-Za-z0-9]?$").test(caseNum)) {
				
				return true;
			}
			
			return false;
		}
		
		public static function isInPatCase(caseNum:String):Boolean {
			if (new RegExp("^[A-Za-z][A-Za-z][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][A-Za-z0-9]{0,2}$").test(caseNum) || 
				new RegExp("^[A-Za-z][A-Za-z][A-Za-z0-9 ]{2}[0-9][0-9][0-9][0-9][0-9][0-9][0-9][A-Za-z0-9]?$").test(caseNum)) {
				
				if (new RegExp("^AE[0-9]+[A-Za-z0-9]$").test(caseNum) || new RegExp("^HN[0-9]+[A-Za-z0-9]$").test(caseNum)) 
				{
					return true;
				}
			}
			
			return false;
		}
		
		public static function isInPatAeCase(caseNum:String):Boolean {
			if (new RegExp("^[A-Za-z][A-Za-z][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][A-Za-z0-9]{0,2}$").test(caseNum) || 
				new RegExp("^[A-Za-z][A-Za-z][A-Za-z0-9 ]{2}[0-9][0-9][0-9][0-9][0-9][0-9][0-9][A-Za-z0-9]?$").test(caseNum)) {
				
				if (new RegExp("^AE[0-9]+[A-Za-z0-9]$").test(caseNum)) {
					return true;
				}
			}
			
			return false;
		}
		
		public static function isDohCase(caseNum:String):Boolean {
			if (new RegExp("^DHP|CIM[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$").test(caseNum)) {
				return true;
			}
			
			return false;
		}
	}
}