package hk.org.ha.event.pms.main.enquiry.popup
{
	import hk.org.ha.model.pms.dms.persistence.DmFmDrug;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshSpecialDrugEnqEvent extends AbstractTideEvent 
	{		
		private var _dmFmDrug:DmFmDrug;
		
		public function RefreshSpecialDrugEnqEvent( dmFmDrug:DmFmDrug):void 
		{
			super();
			_dmFmDrug = dmFmDrug;
		}

		public function get dmFmDrug():DmFmDrug {
			return _dmFmDrug;
		}
	}
}