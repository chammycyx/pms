package hk.org.ha.event.pms.main.prevet
{	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.vo.prevet.PreVetAuth;
	
	public class PreVetAuthnicationValidateFollowUpEvent extends AbstractTideEvent
	{
		private var _callbackOps:String;
		private var _preVetAuth:PreVetAuth;
		
		public function PreVetAuthnicationValidateFollowUpEvent(callbackOps:String)
		{
			super();
			_callbackOps = callbackOps;
		}
		
		public function get callbackOps():String
		{
			return _callbackOps;
		}
		
		public function get preVetAuth():PreVetAuth
		{
			return _preVetAuth;
		}
		
		public function set preVetAuth(preVetAuth:PreVetAuth):void
		{
			_preVetAuth = preVetAuth;
		}
	}
}