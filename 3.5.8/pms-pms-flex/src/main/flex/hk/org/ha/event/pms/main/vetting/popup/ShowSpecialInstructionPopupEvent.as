package hk.org.ha.event.pms.main.vetting.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowSpecialInstructionPopupEvent extends AbstractTideEvent {
		
		private var _message:String;
		
		private var _callback:Function;
		
		public function ShowSpecialInstructionPopupEvent(message:String, callback:Function=null) {
			super();
			_message = message;
			_callback = callback;
		}

		public function get message():String {
			return _message;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}