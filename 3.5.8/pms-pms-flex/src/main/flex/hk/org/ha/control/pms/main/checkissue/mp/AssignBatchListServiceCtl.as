package hk.org.ha.control.pms.main.checkissue.mp{
	
	import hk.org.ha.event.pms.main.checkissue.mp.RefreshUnlinkedItemListEvent;
	import hk.org.ha.event.pms.main.checkissue.mp.RetrieveUnlinkedItemListEvent;
	import hk.org.ha.event.pms.main.checkissue.mp.RetrieveWardListEvent;
	import hk.org.ha.model.pms.biz.checkissue.mp.AssignBatchListServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("assignBatchListServiceCtl", restrict="true")]
	public class AssignBatchListServiceCtl 
	{
		[In]
		public var assignBatchListService:AssignBatchListServiceBean;
		
		public var checked: Boolean;
		
		[Observer]
		public function retrieveUnlinkedItemList(evt:RetrieveUnlinkedItemListEvent):void
		{	checked = evt.checked;
			assignBatchListService.retrieveUnlinkedDeliveryItemList(evt.batchDate, retrieveUnlinkedItemListResult);
		}
		
		private function retrieveUnlinkedItemListResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new RefreshUnlinkedItemListEvent(checked));
		}
		
		[Observer]
		public function retrieveWardList(evt:RetrieveWardListEvent):void
		{
			assignBatchListService.retrieveWardList();
		}

	}
}
