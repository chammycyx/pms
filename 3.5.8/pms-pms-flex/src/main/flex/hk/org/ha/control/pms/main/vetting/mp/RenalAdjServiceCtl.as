package hk.org.ha.control.pms.main.vetting.mp {
	import flash.external.ExternalInterface;
	
	import hk.org.ha.event.pms.main.enquiry.PrintRenalPdfEvent;
	import hk.org.ha.event.pms.main.enquiry.RetrieveRenalDmDrugEvent;
	import hk.org.ha.event.pms.main.vetting.mp.popup.RetrieveRenalChartRptEvent;
	import hk.org.ha.event.pms.main.vetting.mp.popup.RetrieveRenalPdfByDrugKeyEvent;
	import hk.org.ha.event.pms.main.vetting.mp.popup.RetrieveRenalPdfByItemCodeEvent;
	import hk.org.ha.model.pms.biz.vetting.mp.RenalAdjServiceBean;
	import hk.org.ha.model.pms.dms.persistence.DmDrug;
	import hk.org.ha.model.pms.dms.vo.RenalAdj;
	
	import mx.messaging.config.ServerConfig;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("renalAdjServiceCtl", restrict="true")]
	public class RenalAdjServiceCtl {
		
		[In]
		public var renalAdjService:RenalAdjServiceBean;
		
		private var windowOpenerFeatures:String = "left=0,top=0,titlebar=no,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,resizable=yes";

		[Observer]
		public function retrieveRenalChartRpt(evt:RetrieveRenalChartRptEvent):void {
			renalAdjService.retrieveRenalChartRpt(evt.mpRenalEcrclLogList, evt.highlightLastRecord, function(resultEvent:TideResultEvent):void{
				if (evt.callback() != null) {
					evt.callback();
				}
			});
		}
		
		private function showPopup(evt:TideResultEvent):void {
			
			var url:String = "document.seam?actionMethod=report.xhtml%3ArenalAdjService.generateRenalPdf";
			
			if (ServerConfig.getChannel("my-graniteamf").uri.search("{server.name}") == -1) {
				url = ServerConfig.getChannel("my-graniteamf").uri.replace("graniteamf/amf", "") + url;
			}				
			ExternalInterface.call("window.open", url, "", windowOpenerFeatures);
		}
		
		[Observer]
		public function retrieveRenalPdfByItemCode(evt:RetrieveRenalPdfByItemCodeEvent):void {
			var callback:Object = evt.callback == null ? showPopup as Object : evt.callback as Object;
			renalAdjService.retrieveRenalPdfByItemCode(evt.itemCode, function(tideResultEvent:TideResultEvent):void {
				var renalAdj:RenalAdj = tideResultEvent.result as RenalAdj;
				callback(renalAdj.renalAdjCode, renalAdj.reviewDate);
			});
		}

		[Observer]
		public function retrieveRenalPdfByDrugKey(evt:RetrieveRenalPdfByDrugKeyEvent):void {
			var callback:Object = evt.callback == null ? showPopup as Object : evt.callback as Object;
			renalAdjService.retrieveRenalPdfByDrugKey(evt.displayName, evt.formCode, evt.saltProperty, callback);
		}
		
		[Observer]
		public function retrieveRenalDmDrugEvent(evt:RetrieveRenalDmDrugEvent):void {
			renalAdjService.retrieveDmDrug(evt.itemCode, function(tideResultEvent:TideResultEvent):void {
				evt.callback(tideResultEvent.result == null ? null : tideResultEvent.result as DmDrug);
			});
		}
	
		[Observer]
		public function printRenalPdf(evt:PrintRenalPdfEvent):void {
			renalAdjService.printRenalPdf();
		}
	}
}
