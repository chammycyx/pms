package hk.org.ha.event.pms.main.picking
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ReprintDispLabelEvent extends AbstractTideEvent 
	{		
		private var _dispOrderItemIdList:ArrayCollection;
		private var _printLang:String;
		
		public function ReprintDispLabelEvent(dispOrderItemIdList:ArrayCollection, printLang:String):void 
		{
			super();
			_dispOrderItemIdList = dispOrderItemIdList;
			_printLang = printLang;
		}
		
		public function get dispOrderItemIdList():ArrayCollection {
			return _dispOrderItemIdList;
		}
		
		public function get printLang():String {
			return _printLang;
		}
	}
}