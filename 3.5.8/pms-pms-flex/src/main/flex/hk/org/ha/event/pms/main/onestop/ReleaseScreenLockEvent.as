package hk.org.ha.event.pms.main.onestop {
		
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ReleaseScreenLockEvent extends AbstractTideEvent 
	{			
		public function ReleaseScreenLockEvent():void 
		{
			super();
		}        
	}
}