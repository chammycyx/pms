package hk.org.ha.event.pms.main.cddh {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CreateCddhCriteriaEvent extends AbstractTideEvent 
	{
		private var _hkid:String;
		
		public function CreateCddhCriteriaEvent(hkid:String=""):void 
		{
			super();
			_hkid = hkid;
		}
		
		public function get hkid():String {
			return _hkid;
		}
	}
}
