package hk.org.ha.view.pms.main {
	
	import mx.controls.treeClasses.TreeItemRenderer;
	import mx.controls.treeClasses.TreeListData;
	
	public class ShortcutTreeItemRenderer extends TreeItemRenderer
	{
		public function ShortcutTreeItemRenderer() 
		{
			super();
			mouseEnabled = false;			
		}
		
		override public function set data(value:Object):void
		{
			if(value != null)
			{ 
				super.data = value;

				if (value.url != null) {
					setStyle("textDecoration", "underline");
					setStyle("fontWeight", 'normal');
				} else {
					setStyle("textDecoration", "normal");
					setStyle("fontWeight", 'bold');
				}
			}
		}	 

		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void {
			super.updateDisplayList(unscaledWidth, unscaledHeight);
		}
	}
}