package hk.org.ha.event.pms.main.vetting {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrievePatientByHkidEvent extends AbstractTideEvent {
		
		private var _hkid:String;
		
		private var _callback:Function;
		
		public function RetrievePatientByHkidEvent(hkid:String, callback:Function) {
			super();
			_hkid = hkid;
			_callback = callback;
		}
		
		public function get hkid():String {
			return _hkid;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}
