package hk.org.ha.event.pms.main.checkissue.mp
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowAssignBatchResultEvent extends AbstractTideEvent 
	{			
		private var _msgCode:String;
		private var _batchDate:String;
		private var _batchNum:String;
		private var _wardCode:String;
		
		public function ShowAssignBatchResultEvent(msgCode:String, batchDate:String ,batchNum:String, wardCode:String ):void 
		{
			super();
			_msgCode = msgCode;
			_batchDate = batchDate;
			_batchNum = batchNum;
			_wardCode = wardCode;
		}
		
		public function get msgCode():String {
			return _msgCode;
		}
		
		public function get batchNum():String {
			return _batchNum;
		}
		
		public function get wardCode():String {
			return _wardCode;
		}
		
		public function get batchDate():String {
			return _batchDate;
		}
	}
}