package hk.org.ha.event.pms.main.enquiry
{	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveMpSfiInvoiceSummaryEvent extends AbstractTideEvent 
	{
		private var _sfiInvoiceId:Number;
		private var _purchaseReqId:Number;
		
		public function RetrieveMpSfiInvoiceSummaryEvent(sfiInvoiceId:Number, purchaseReqId:Number):void
		{
			super();
			_sfiInvoiceId = sfiInvoiceId;
			_purchaseReqId = purchaseReqId;
		}
		
		public function get sfiInvoiceId():Number
		{
			return _sfiInvoiceId;
		}
		
		public function get purchaseReqId():Number
		{
			return _purchaseReqId;
		}
	}
}