package hk.org.ha.event.pms.main.pivas
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePivasFormulaListByPrefixDisplayNameEvent extends AbstractTideEvent 
	{		
		private var _prefixDisplayName:String;
		private var _activeBatchPrepFormulaOnly:Boolean;
		private var _callbackFunc:Function;
		
		public function RetrievePivasFormulaListByPrefixDisplayNameEvent(prefixDisplayName:String, activeBatchPrepFormulaOnly:Boolean, callbackFunc:Function = null):void 
		{
			super();
			_prefixDisplayName = prefixDisplayName;
			_activeBatchPrepFormulaOnly = activeBatchPrepFormulaOnly;
			_callbackFunc = callbackFunc;
		}
		
		public function get prefixDisplayName():String {
			return _prefixDisplayName;
		}
		
		public function get activeBatchPrepFormulaOnly():Boolean {
			return _activeBatchPrepFormulaOnly;
		}
		
		public function get callbackFunc():Function {
			return _callbackFunc;
		}
	}
}