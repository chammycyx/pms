package hk.org.ha.control.pms.main.drug {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.drug.ClearDmWarningEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveDmWarningEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveDmWarningForOrderEditEvent;
	import hk.org.ha.event.pms.main.drug.RetrieveDmWarningForOrderViewEvent;
	import hk.org.ha.model.pms.biz.drug.DmWarningServiceBean;
	import hk.org.ha.model.pms.dms.persistence.DmWarning;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("dmWarningServiceCtl", restrict="true")]
	public class DmWarningServiceCtl 
	{
		[In]
		public var dmWarningService:DmWarningServiceBean;		
		[In]
		public var dmWarning:DmWarning;		
		private var warnIndex:Number;		
		private var resultEvent:Event;
		private var resultFunc:Function;
		private var retrieveDmWarningForOrderViewEvent:RetrieveDmWarningForOrderViewEvent;
		
		[Observer]
		public function retrieveDmWarning(evt:RetrieveDmWarningEvent):void
		{
			resultEvent = evt.resultEvent;
			
			dmWarningService.retrieveDmWarning(evt.warnCode, retrieveDmWarningResult);
		}
		
		private function retrieveDmWarningResult(evt:TideResultEvent):void 
		{
			if ( resultEvent!=null ) {
				evt.context.dispatchEvent( resultEvent );
			}
		}
		
		[Observer]
		public function clearDmWarning(evt:ClearDmWarningEvent):void
		{			
			dmWarningService.clearDmWarning();
		}
		
		[Observer]
		public function retrieveDmWarningForOrderEdit(evt:RetrieveDmWarningForOrderEditEvent):void
		{
			warnIndex  = evt.warnIndex;
			resultFunc = evt.resultFunc;
			dmWarningService.retrieveDmWarning(evt.warnCode, retrieveDmWarningForOrderEditResult);
		}
		
		private function retrieveDmWarningForOrderEditResult(evt:TideResultEvent):void 
		{
			if ( resultFunc!=null ) {
				resultFunc.call(null, warnIndex, dmWarning);
			}
		}
		
		[Observer]
		public function retrieveDmWarningForOrderView(evt:RetrieveDmWarningForOrderViewEvent):void {
			retrieveDmWarningForOrderViewEvent = evt;
			dmWarningService.retrieveDmWarning(evt.warnCode, retrieveDmWarningForOrderViewResult);
		}
		
		private function retrieveDmWarningForOrderViewResult(evt:TideResultEvent):void {
			if (retrieveDmWarningForOrderViewEvent.callback != null) {
				retrieveDmWarningForOrderViewEvent.callback(dmWarning);
			}
		}
	}
}
