package hk.org.ha.event.pms.main.charging {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowMpSfiInvoiceItemSystemMessageEvent extends AbstractTideEvent 
	{	
		private var _messageCode:String;
		
		public function ShowMpSfiInvoiceItemSystemMessageEvent(messageCode:String):void 
		{
			super();
			_messageCode = messageCode;
		}
		
		public function get messageCode():String {
			return _messageCode;
		}
	}
}