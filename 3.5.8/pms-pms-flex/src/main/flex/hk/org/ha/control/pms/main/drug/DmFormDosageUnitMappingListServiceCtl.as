package hk.org.ha.control.pms.main.drug {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.drug.RetrieveDosageUnitListByFormCodeEvent;
	import hk.org.ha.model.pms.biz.drug.DmFormDosageUnitMappingListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("dmFormDosageUnitMappingListServiceCtl", restrict="true")]
	public class DmFormDosageUnitMappingListServiceCtl 
	{
		[In]
		public var dmFormDosageUnitMappingListService:DmFormDosageUnitMappingListServiceBean;			
		
		[In]
		public var ctx:Context;
		
		private var callBackFunction:Function;
		
		[Observer]
		public function retrieveDosageUnitList(evt:RetrieveDosageUnitListByFormCodeEvent):void
		{			
			callBackFunction = evt.callBackFunction;
			dmFormDosageUnitMappingListService.retrieveDosageUnitListByFormCode(evt.formCode, retrieveDosageUnitListResult);
		}		
		
		private function retrieveDosageUnitListResult(evt:TideResultEvent):void {
			if ( callBackFunction != null ) {	
				callBackFunction(evt.result);				
			}
		}
	}
}
