package hk.org.ha.control.pms.main.reftable.pivas
{
	import mx.collections.ListCollectionView;
	
	import hk.org.ha.event.pms.main.reftable.pivas.RefreshPivasWardListEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.RetrievePivasFormulaAvailabilityInfoEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.RetrievePivasWardInfoEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.RetrievePivasWardListEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.SetPivasWardInfoEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.SetPivasWardListEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.SetRefreshPivasWardListEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.ShowPivasWardMaintSaveSuccessMsgEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.UpdatePivasFormulaExclWardListEvent;
	import hk.org.ha.event.pms.main.reftable.pivas.UpdatePivasWardListEvent;
	import hk.org.ha.model.pms.biz.reftable.pivas.PivasWardMaintServiceBean;
	import hk.org.ha.model.pms.vo.pivas.PivasWardInfo;
	
	import org.granite.tide.events.TideResultEvent;

	[Bindable]
	[Name("pivasWardMaintServiceCtl", restrict="true")]
	public class PivasWardMaintServiceCtl {
		[In]
		public var pivasWardMaintService:PivasWardMaintServiceBean;
		
		
		public function PivasWardMaintServiceCtl() {	
		}
		
		[Observer]
		public function retrievePivasWardInfo(evt:RetrievePivasWardInfoEvent):void{
			pivasWardMaintService.retrievePivasWardInfo(retrievePivasWardInfoResult);
		}
		
		public function retrievePivasWardInfoResult(evt:TideResultEvent):void{
			evt.context.dispatchEvent(new SetPivasWardInfoEvent (evt.result as PivasWardInfo));
		}
		
		[Observer]
		public function retrievePivasWardList(evt:RetrievePivasWardListEvent):void{
			pivasWardMaintService.retrievePivasWardList(evt.patHospCode, retrievePivasWardListResult);
		}
		
		public function retrievePivasWardListResult(evt:TideResultEvent):void{
			evt.context.dispatchEvent(new SetPivasWardListEvent (evt.result as ListCollectionView));
		}
		
		[Observer]
		public function retrievePivasFormulaAvailabilityInfo(evt:RetrievePivasFormulaAvailabilityInfoEvent):void {
			pivasWardMaintService.retrievePivasFormulaAvailabilityInfo(evt.pivasFormulaId,
				function(resultEvt:TideResultEvent):void {
					evt.successCallback(resultEvt.result);
				}
			);
		}
		
		[Observer]
		public function updatePivasFormulaExclWardList(evt:UpdatePivasFormulaExclWardListEvent):void {
			pivasWardMaintService.updatePivasFormulaExclWardList(evt.pivasFormulaId, evt.pivasFormulaExclWardList,
				function(resultEvt:TideResultEvent):void {
					evt.successCallback();
				}
			);
		}
		
		[Observer]
		public function refreshPivasWardList(evt:RefreshPivasWardListEvent):void{
			pivasWardMaintService.retrievePivasWardList(evt.patHospCode, refreshPivasWardListResult);
		}
		
		public function refreshPivasWardListResult(evt:TideResultEvent):void{
			evt.context.dispatchEvent(new SetRefreshPivasWardListEvent (evt.result as ListCollectionView));
		}
		
		[Observer]
		public function updatePivasWardList(evt:UpdatePivasWardListEvent):void{
			pivasWardMaintService.updatePivasWard(evt.pivasWardList, evt.patHospCode, showPivasWardMaintSaveSuccessMsg);
		}
		
		public function showPivasWardMaintSaveSuccessMsg(evt:TideResultEvent):void{
			evt.context.dispatchEvent(new ShowPivasWardMaintSaveSuccessMsgEvent());
		}
	}
}