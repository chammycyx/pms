package hk.org.ha.event.pms.main.enquiry
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDrugSetDtlListEvent extends AbstractTideEvent 
	{	
		private var _patHospCode:String;
		private var _drugSetNum:Number;
		private var _drugSetCode:String;
		private var _drugHospCode:String;
		private var _subLevelFlag:Boolean;
		
		public function RetrieveDrugSetDtlListEvent(patHospCode:String, drugSetNum:Number, drugSetCode:String, drugHospCode:String, subLevelFlag:Boolean):void 
		{
			super();
			_patHospCode = patHospCode;
			_drugSetNum = drugSetNum;
			_drugSetCode = drugSetCode;
			_drugHospCode = drugHospCode;
			_subLevelFlag = subLevelFlag;
		}
		
		public function get patHospCode():String {
			return _patHospCode;
		}
		
		public function get drugSetNum():Number {
			return _drugSetNum;
		}
		
		public function get drugSetCode():String {
			return _drugSetCode;
		}
		
		public function get drugHospCode():String {
			return _drugHospCode;
		}
		
		public function get subLevelFlag():Boolean {
			return _subLevelFlag;
		}
	}
}