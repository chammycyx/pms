package hk.org.ha.event.pms.main.checkissue
{
	import hk.org.ha.model.pms.udt.machine.IqdsAction;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveLedOperationLogListEvent extends AbstractTideEvent 
	{
		private var _iqdsAction:IqdsAction;
		
		public function RetrieveLedOperationLogListEvent(iqdsAction:IqdsAction):void 
		{
			super();
			_iqdsAction = iqdsAction;
		}
		
		public function get iqdsAction():IqdsAction
		{
			return _iqdsAction;
		}
	}
}