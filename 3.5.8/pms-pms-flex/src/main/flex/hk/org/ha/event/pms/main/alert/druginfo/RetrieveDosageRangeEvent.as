package hk.org.ha.event.pms.main.alert.druginfo {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	import mx.collections.ListCollectionView;
	
	public class RetrieveDosageRangeEvent extends AbstractTideEvent {
		
		private var _gcnSeqNum:Number;
		
		private var _rdfgenId:Number;
		
		private var _rgenId:Number;
		
		private var _patHospCode:String;
		
		private var _age:Number;
		
		private var _ageUnit:String;
		
		private var _preparationPropertyList:ListCollectionView;
		
		private var _selectedStrengthIndex:Number;
		
		private var _successCallback:Function;
		
		private var _faultCallback:Function;

		public function RetrieveDosageRangeEvent(gcnSeqNum:Number, rdfgenId:Number, rgenId:Number, patHospCode:String, age:Number, ageUnit:String, preparationPropertyList:ListCollectionView, selectedStrengthIndex:Number, successCallback:Function, faultCallback:Function) {
			super();
			_gcnSeqNum = gcnSeqNum;
			_rdfgenId = rdfgenId;
			_rgenId = rgenId;
			_patHospCode = patHospCode;
			_age = age;
			_ageUnit = ageUnit;
			_preparationPropertyList = preparationPropertyList;
			_selectedStrengthIndex = selectedStrengthIndex;
			_successCallback = successCallback;
			_faultCallback = faultCallback;
		}
		
		public function get gcnSeqNum():Number {
			return _gcnSeqNum;
		}
		
		public function get rdfgenId():Number {
			return _rdfgenId;
		}
		
		public function get rgenId():Number {
			return _rgenId;
		}
		
		public function get patHospCode():String {
			return _patHospCode;
		}
		
		public function get age():Number {
			return _age;
		}

		public function get ageUnit():String {
			return _ageUnit;
		}

		public function get preparationPropertyList():ListCollectionView {
			return _preparationPropertyList;
		}

		public function get selectedStrengthIndex():Number {
			return _selectedStrengthIndex;
		}
		
		public function get successCallback():Function {
			return _successCallback;
		}
		
		public function get faultCallback():Function {
			return _faultCallback;
		}
	}
}