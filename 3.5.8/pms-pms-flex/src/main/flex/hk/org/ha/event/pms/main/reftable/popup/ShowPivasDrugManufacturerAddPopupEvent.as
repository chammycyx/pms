package hk.org.ha.event.pms.main.reftable.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowPivasDrugManufacturerAddPopupEvent extends AbstractTideEvent 
	{
		private var _pivasDrugManufacturerAddPopupProp:PivasDrugManufacturerAddPopupProp;
		
		public function ShowPivasDrugManufacturerAddPopupEvent(pivasDrugManufacturerAddPopupProp:PivasDrugManufacturerAddPopupProp):void 
		{
			super();
			_pivasDrugManufacturerAddPopupProp = pivasDrugManufacturerAddPopupProp;
		}
		
		public function get pivasDrugManufacturerAddPopupProp():PivasDrugManufacturerAddPopupProp{
			return _pivasDrugManufacturerAddPopupProp;
		}
	}
}