package hk.org.ha.event.pms.main.support
{
	import hk.org.ha.model.pms.persistence.corp.Hospital;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class RetrieveWorkstationPropListEvent extends AbstractTideEvent 
	{
		private var _hospital:Hospital;
		private var _hostName:String;
		
		public function RetrieveWorkstationPropListEvent(hospital:Hospital, hostName:String):void
		{
			super();
			_hospital = hospital;
			_hostName = hostName;
		}
		
		public function hospital():Hospital {
			return _hospital;
		}
		
		public function hostName():String {
			return _hostName;
		}
	}
}