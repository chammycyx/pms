package hk.org.ha.event.pms.main.vetting.mp.popup {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RetrieveRenalPdfByDrugKeyEvent extends AbstractTideEvent {
		
		private var _displayName:String;

		private var _formCode:String;
		
		private var _saltProperty:String;

		private var _callback:Function;

		public function RetrieveRenalPdfByDrugKeyEvent(displayName:String, formCode:String, saltProperty:String, callback:Function=null) {
			super();
			_displayName = displayName;
			_formCode = formCode;
			_saltProperty = saltProperty;
			_callback = callback;
		}
		
		public function get displayName():String {
			return _displayName;
		}

		public function get formCode():String {
			return _formCode
		}
		
		public function get saltProperty():String {
			return _saltProperty;
		}
		
		public function get callback():Function {
			return _callback;
		}
	}
}