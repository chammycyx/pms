package hk.org.ha.event.pms.main.reftable {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dms.persistence.PmsDosageConversionSet;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshDosageConversionByMoDosageEvent extends AbstractTideEvent 
	{
		
		private var _pmsDosageConversionSet:PmsDosageConversionSet;
		
		public function get pmsDosageConversionSet():PmsDosageConversionSet
		{
			return _pmsDosageConversionSet;
		}

		public function RefreshDosageConversionByMoDosageEvent(pmsDosageConversionSet:PmsDosageConversionSet):void 
		{
			_pmsDosageConversionSet = pmsDosageConversionSet;
			super();
		}

	}
}