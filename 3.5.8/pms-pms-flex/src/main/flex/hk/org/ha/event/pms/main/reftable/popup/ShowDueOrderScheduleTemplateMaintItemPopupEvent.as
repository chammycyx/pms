package hk.org.ha.event.pms.main.reftable.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDueOrderScheduleTemplateMaintItemPopupEvent extends AbstractTideEvent 
	{
		private var _dueOrderScheduleTemplateMaintItemPopupProp:DueOrderScheduleTemplateMaintItemPopupProp;
		
		public function ShowDueOrderScheduleTemplateMaintItemPopupEvent(dueOrderScheduleTemplateMaintItemPopupProp:DueOrderScheduleTemplateMaintItemPopupProp):void 
		{
			super();
			_dueOrderScheduleTemplateMaintItemPopupProp = dueOrderScheduleTemplateMaintItemPopupProp;
		}
		
		public function get dueOrderScheduleTemplateMaintItemPopupProp():DueOrderScheduleTemplateMaintItemPopupProp{
			return _dueOrderScheduleTemplateMaintItemPopupProp;
		}

	}
}