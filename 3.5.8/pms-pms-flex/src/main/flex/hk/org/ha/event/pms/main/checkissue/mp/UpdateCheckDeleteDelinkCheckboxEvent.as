package hk.org.ha.event.pms.main.checkissue.mp
{	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateCheckDeleteDelinkCheckboxEvent extends AbstractTideEvent 
	{			
		private var _markUlinkFlag:Boolean;

		private var _markDeleteFlag:Boolean;
		
		public function UpdateCheckDeleteDelinkCheckboxEvent(markUlinkFlag:Boolean, markDeleteFlag:Boolean) {
			super();
			_markUlinkFlag = markUlinkFlag;
			_markDeleteFlag = markDeleteFlag;
		}
		
		public function get markUlinkFlag():Boolean {
			return _markUlinkFlag;
		}

		public function get markDeleteFlag():Boolean {
			return _markDeleteFlag;	}

	}
}