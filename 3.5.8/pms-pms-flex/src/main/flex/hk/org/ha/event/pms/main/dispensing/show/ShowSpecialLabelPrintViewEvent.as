package hk.org.ha.event.pms.main.dispensing.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowSpecialLabelPrintViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		public function ShowSpecialLabelPrintViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}