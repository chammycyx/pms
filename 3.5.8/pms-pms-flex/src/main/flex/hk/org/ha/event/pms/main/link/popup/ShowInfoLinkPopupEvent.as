package hk.org.ha.event.pms.main.link.popup{
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowInfoLinkPopupEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		private var _url:String;
		
		public function ShowInfoLinkPopupEvent(url:String, clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
			_url = url;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
		
		public function get url():String {
			return _url;
		}
	}
}