package hk.org.ha.event.pms.main.charging {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	import mx.collections.ArrayCollection;
	
	public class VoidMpSfiInvoiceEvent extends AbstractTideEvent 
	{	
		private var _invoiceNum:String;
		private var _deliveryItemIdList:ArrayCollection;
		
		public function VoidMpSfiInvoiceEvent(invoiceNum:String, deliveryItemIdList:ArrayCollection):void 
		{
			super();
			_invoiceNum = invoiceNum;
			_deliveryItemIdList = deliveryItemIdList;
		}
		
		public function get invoiceNum():String {
			return _invoiceNum;
		}
		
		public function get deliveryItemIdList():ArrayCollection {
			return _deliveryItemIdList;
		}
	}
}