package hk.org.ha.event.pms.main.support
{
	import org.granite.tide.events.AbstractTideEvent;

	public class RetrieveHostNameListEvent extends AbstractTideEvent 
	{
		private var _hospCode:String;
		private var _prefixHostName:String;
		
		public function RetrieveHostNameListEvent(hospCode:String, prefixHostName:String):void
		{
			super();
			_hospCode = hospCode;
			_prefixHostName = prefixHostName;
		}
		
		public function hospCode():String {
			return _hospCode;
		}
		
		public function prefixHostName():String {
			return _prefixHostName;
		}
	}
}