package hk.org.ha.event.pms.main.enquiry
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSfiInvoiceSummaryListEvent extends AbstractTideEvent 
	{
		private var _searchType:String;
		private var _param1:String;
		private var _param2:Date;
		private var _includeVoid:Boolean;
		private var _showView:Boolean;
		
		
		public function RetrieveSfiInvoiceSummaryListEvent(searchType:String, param1:String, param2:Date, 
														   includeVoid:Boolean, showView:Boolean=false):void
		{
			super();
			_searchType = searchType;
			_param1 = param1;
			_param2 = param2;
			_includeVoid = includeVoid;
			_showView = showView;
		}
		
		public function get searchType():String
		{
			return _searchType;
		}
		
		public function get param1():String
		{
			return _param1;
		}
		
		public function get param2():Date
		{
			return _param2;
		}
				
		public function get includeVoid():Boolean
		{
			return _includeVoid;
		}
		
		public function get showView():Boolean
		{
			return _showView;
		}
	}
}