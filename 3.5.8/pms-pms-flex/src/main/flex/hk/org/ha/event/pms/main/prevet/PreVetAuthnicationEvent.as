package hk.org.ha.event.pms.main.prevet
{	
	import flash.events.Event;
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class PreVetAuthnicationEvent extends AbstractTideEvent
	{		
		private var _userId:String;
		private var _password:String;
		private var _targetName:String;
		private var _followUpEvent:Event;
		
		public function PreVetAuthnicationEvent(userId:String, password:String, targetName:String, followUpEvent:Event)
		{
			super();
			_userId = userId;
			_password = password;
			_targetName = targetName;
			_followUpEvent = followUpEvent;
		}
		
		public function get password():String
		{
			return _password;
		}
		
		public function get userId():String
		{
			return _userId;
		}
		
		public function set userId(userId:String):void {
			_userId = userId;
		}
		
		public function get targetName():String
		{
			return _targetName;
		}
		
		public function get followUpEvent():Event
		{
			return _followUpEvent;
		}
	}
}