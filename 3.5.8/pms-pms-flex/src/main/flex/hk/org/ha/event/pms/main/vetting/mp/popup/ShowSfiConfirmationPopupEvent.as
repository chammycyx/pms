package hk.org.ha.event.pms.main.vetting.mp.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowSfiConfirmationPopupEvent extends AbstractTideEvent
	{
		private var _medProfileItem:*;
		private var _proceedFunction:Function;
		private var _pendingFunction:Function;
		private var _cancelFunction:Function;
		
		public function ShowSfiConfirmationPopupEvent(medProfileItem:*, proceedFunction:Function, pendingFunction:Function,
													  cancelFunction:Function)
		{
			super();
			_medProfileItem = medProfileItem;
			_proceedFunction = proceedFunction;
			_pendingFunction = pendingFunction;
			_cancelFunction = cancelFunction;
		}
		
		public function get medProfileItem():*
		{
			return _medProfileItem;
		}
		
		public function get proceedFunction():Function
		{
			return _proceedFunction;
		}
		
		public function get pendingFunction():Function
		{
			return _pendingFunction;
		}
		
		public function get cancelFunction():Function
		{
			return _cancelFunction;
		}
	}
}