package hk.org.ha.event.pms.main.report {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDelItemRptEvent extends AbstractTideEvent 
	{
		private var _workstoreCode:String;
		private var _date:Date;
		private var _selectedTab:Number;
		
		public function RetrieveDelItemRptEvent(selectedTab:Number, workstoreCode:String, date:Date):void 
		{
			super();
			_workstoreCode = workstoreCode;
			_date = date;
			_selectedTab = selectedTab;
		}
		
		public function get selectedTab():Number{
			return _selectedTab;
		} 
		
		public function get workstoreCode():String{
			return _workstoreCode;
		} 
		
		public function get date():Date{
			return _date;
		} 

	}
}