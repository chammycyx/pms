package hk.org.ha.event.pms.main.drug {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dms.vo.RouteFormSearchCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveRouteFormListEvent extends AbstractTideEvent 
	{
		private var _routeFormSearchCriteria:RouteFormSearchCriteria;
		private var _callbackFunc:Function;
		
		public function RetrieveRouteFormListEvent(criteria:RouteFormSearchCriteria, funcValue:Function=null):void 
		{
			super();
			_routeFormSearchCriteria = criteria;
			_callbackFunc = funcValue;
		}
		
		public function get routeFormSearchCriteria():RouteFormSearchCriteria
		{
			return _routeFormSearchCriteria;
		}
		
		public function get callbackFunc():Function
		{
			return _callbackFunc;
		}
	}
}