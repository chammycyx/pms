package hk.org.ha.event.pms.security {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class DebugLogonEvent extends AbstractTideEvent 
	{
		
		private var _username:String;        
		private var _password:String;
		private var _hospCode:String;        
		private var _workstoreCode:String;
		
		public function DebugLogonEvent(username:String, password:String, hospCode:String, workstoreCode:String):void 
		{
			super();
			_username = username;
			_password = password;
			_hospCode = hospCode;
			_workstoreCode = workstoreCode;
		}        
		
		public function get username():String 
		{
			return _username;
		}
		
		public function get password():String 
		{
			return _password;
		}
		
		public function get hospCode():String 
		{
			return _hospCode;
		}
		
		public function get workstoreCode():String 
		{
			return _workstoreCode;
		}
	}
}