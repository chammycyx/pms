package hk.org.ha.event.pms.main.reftable.popup
{
	import hk.org.ha.model.pms.dms.persistence.PmsFixedConcnStatus;

	public class FixedConcnPrepActivationMaintPopupProp
	{
		public var fixedConcnStatus:PmsFixedConcnStatus;
		public var activeHandler:Function;
		public var inactiveHandler:Function;
		public var cancelHandler:Function;
	}
}
