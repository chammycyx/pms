package hk.org.ha.event.pms.main.vetting {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class CancelEditItemEvent extends AbstractTideEvent {
		
		private var _selectedIndex:Number;
		
		private var _showOneStopFlag:Boolean;
		
		public function CancelEditItemEvent(selectedIndex:Number=NaN, showOneStopFlag:Boolean=true) {
			super();
			_showOneStopFlag = showOneStopFlag;
			
			if (selectedIndex >= 0) {
				_selectedIndex = selectedIndex;
			}
		}
		
		public function get selectedIndex():Number {
			return _selectedIndex;
		}

		public function get showOneStopFlag():Boolean {
			return _showOneStopFlag;
		}
	}
}
