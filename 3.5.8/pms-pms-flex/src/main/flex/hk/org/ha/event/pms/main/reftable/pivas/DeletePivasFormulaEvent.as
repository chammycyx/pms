package hk.org.ha.event.pms.main.reftable.pivas {
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormula;
	
	public class DeletePivasFormulaEvent extends AbstractTideEvent {
		
		private var _pivasFormula:PivasFormula;
		private var _successCallback:Function;
		private var _faultCallback:Function;
		
		public function DeletePivasFormulaEvent(pivasFormula:PivasFormula, successCallback:Function, faultCallback:Function) {
			super();
			_pivasFormula = pivasFormula;
			_successCallback = successCallback;
			_faultCallback = faultCallback;
		}
		
		public function get pivasFormula():PivasFormula {
			return _pivasFormula;
		}
		
		public function get successCallback():Function {
			return _successCallback;
		}

		public function get faultCallback():Function {
			return _faultCallback;
		}
	}
}
