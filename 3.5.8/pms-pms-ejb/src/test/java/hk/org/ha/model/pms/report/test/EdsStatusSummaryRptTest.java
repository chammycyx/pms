package hk.org.ha.model.pms.report.test;

import hk.org.ha.model.pms.biz.report.EdsStatusSummaryRptServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.vo.report.EdsStatusSummaryRpt;

import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.joda.time.DateTime;
import org.testng.annotations.Test;

public class EdsStatusSummaryRptTest extends SeamTest {
	
	@Test
	public void testEdsStatusSummaryRptComponent() throws Exception {
	
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();

		// retrieve list 
		new ComponentTest() {
			protected void testComponents() throws Exception {  
				DateTime inputDate = new DateTime().withDate(2011, 3, 30);
				
				EdsStatusSummaryRptServiceLocal edsStatusSummaryRptService = (EdsStatusSummaryRptServiceLocal) getValue("#{edsStatusSummaryRptService}");
				List<EdsStatusSummaryRpt> edsStatusSummaryRptList =  edsStatusSummaryRptService.retrievEdsStatusSummaryRptList(inputDate.toDate());
				
				assert edsStatusSummaryRptList.size() >0;
			}
		}.run();
		
	}	
}
