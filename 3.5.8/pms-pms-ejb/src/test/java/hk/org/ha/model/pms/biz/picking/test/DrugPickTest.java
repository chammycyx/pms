package hk.org.ha.model.pms.biz.picking.test;

import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.model.pms.biz.order.DispOrderManagerLocal;
import hk.org.ha.model.pms.biz.picking.DrugPickServiceLocal;
import hk.org.ha.model.pms.biz.reftable.WorkstationManagerLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.vo.picking.PickItem;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.joda.time.DateTime;
import org.testng.annotations.Test;

public class DrugPickTest extends SeamTest {

	private Workstation workstation;
	private int totalPendingItem = 0;
	private int totalPickedItem = 0;
	private DateTime dispDate;
	private String ticketNum;
	private Integer itemNum;
	private Long dispOrderId;
	
	@Test
	public void testDrugPickComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
	            
	            UamInfo uamInfo = new UamInfo();
				Contexts.getSessionContext().set("uamInfo", uamInfo);
	            
			}
		}.run();
		
		// retrieve WorkstationList
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				
				invokeMethod("#{drugPickListService.retrieveWorkstationList}");	
				List<Workstation> workstationList = (List<Workstation>)getValue("#{workstationList}");
				
				assert workstationList.size()>0;
			}
		}.run();
		
		// retrievePickingList
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				WorkstationManagerLocal workstationManager = (WorkstationManagerLocal) getInstance("workstationManager");
				UamInfo uamInfo = new UamInfo();
				uamInfo.setWorkstationId("moepms0002");
				uamInfo.setHospital("QMH");
				uamInfo.setWorkstore("WKS1");
				workstation = workstationManager.retrieveWorkstationByUamInfo(uamInfo);
				Contexts.getSessionContext().set("workstationCode", workstation.getWorkstationCode());
				
				invokeMethod("#{drugPickListService.retrievePickingList(workstationCode)}");	
				List<PickItem> pendingItemList = (List<PickItem>)getValue("#{pendingItemList}");
				List<PickItem> pickedItemList = (List<PickItem>)getValue("#{pickedItemList}");
				
				assert pendingItemList.size()>=0;
				assert pickedItemList.size()>0;
				
				totalPendingItem = pendingItemList.size();
				totalPickedItem = pickedItemList.size();
			}
		}.run();
		
		// updateDispOrderItemStatusToPicked//Date dispDate, String ticketNum, Integer itemNum
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				invokeMethod("#{drugPickListService.retrievePickingList(workstationCode)}");	
				List<PickItem> pendingItemList = (List<PickItem>)getValue("#{pendingItemList}");
				
				assert pendingItemList.size() == totalPendingItem;

				dispDate = new DateTime().withDate(2011, 4, 30);
				
				ticketNum = "0506";
				itemNum = 1;
				dispOrderId = Long.valueOf(50);	

				DispOrderManagerLocal dispOrderManager = (DispOrderManagerLocal) getInstance("dispOrderManager");
				DispOrder dispOrder = dispOrderManager.retrieveDispOrderById(dispOrderId);

				assert dispOrder.getStatus() == DispOrderStatus.Vetted;
				
				DrugPickServiceLocal drugPickService = (DrugPickServiceLocal) getInstance("drugPickService");
				drugPickService.updateDispOrderItemStatusToPicked(dispDate.toDate(), ticketNum, itemNum, "Admin", workstation.getId());

				assert drugPickService.isUpdateSuccess();
				
				dispOrder = dispOrderManager.retrieveDispOrderById(dispOrderId);

				assert dispOrder.getStatus() == DispOrderStatus.Picked;
			}
		}.run();
		
		// check ItemList
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				invokeMethod("#{drugPickListService.retrievePickingList(workstationCode)}");	
				List<PickItem> pendingItemList = (List<PickItem>)getValue("#{pendingItemList}");
				List<PickItem> pickedItemList = (List<PickItem>)getValue("#{pickedItemList}");

				assert pendingItemList.size() == (totalPendingItem - 1);
				assert pickedItemList.size() == (totalPickedItem + 1);
				
			}
		}.run();
		
		// item has been picked 
		new ComponentTest() {
			protected void testComponents() throws Exception {
				DrugPickServiceLocal drugPickService = (DrugPickServiceLocal) getInstance("drugPickService");
				drugPickService.updateDispOrderItemStatusToPicked(dispDate.toDate(), ticketNum, itemNum, "Admin", workstation.getId());

				assert !drugPickService.isUpdateSuccess();
				assert StringUtils.equals(drugPickService.getResultMsg(), "0060");
			}
		}.run();
		
		// item has been deleted
		new ComponentTest() {
			protected void testComponents() throws Exception {
				dispDate = new DateTime().withDate(2011, 8, 3);
				
				ticketNum = "0509";
				itemNum = 1;
				dispOrderId = Long.valueOf(80);
				
				DrugPickServiceLocal drugPickService = (DrugPickServiceLocal) getInstance("drugPickService");
				drugPickService.updateDispOrderItemStatusToPicked(dispDate.toDate(), ticketNum, itemNum, "Admin", workstation.getId());

				assert !drugPickService.isUpdateSuccess();
				assert StringUtils.equals(drugPickService.getResultMsg(), "0074");
			}
		}.run();
		
		// updateDispOrderItemStatusToPicked//Date dispDate, String ticketNum, Integer itemNum
		new ComponentTest() {
			protected void testComponents() throws Exception {
				dispDate = new DateTime().withDate(2011, 4, 18);
				
				DrugPickServiceLocal drugPickService = (DrugPickServiceLocal) getInstance("drugPickService");
				drugPickService.updateDispOrderItemStatusToPicked(dispDate.toDate(), "0052", 1, "Admin", workstation.getId());

				assert drugPickService.isUpdateSuccess();
			}
		}.run();
		
		// check ItemList
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				invokeMethod("#{drugPickListService.retrievePickingList(workstationCode)}");	
				List<PickItem> pendingItemList = (List<PickItem>)getValue("#{pendingItemList}");
				List<PickItem> pickedItemList = (List<PickItem>)getValue("#{pickedItemList}");

				assert pendingItemList.size() == (totalPendingItem - 2);
				assert pickedItemList.size() == (totalPickedItem + 2);

				DispOrderManagerLocal dispOrderManager = (DispOrderManagerLocal) getInstance("dispOrderManager");
				DispOrder dispOrder = dispOrderManager.retrieveDispOrderById(Long.parseLong("38"));

				assert dispOrder.getStatus() == DispOrderStatus.Picking;
			}
		}.run();	
		
		// pick invalid DispOrderItem
		new ComponentTest() {
			protected void testComponents() throws Exception {
				dispDate = new DateTime().withDate(2011, 3, 18);
				
				DrugPickServiceLocal drugPickService = (DrugPickServiceLocal) getInstance("drugPickService");
				drugPickService.updateDispOrderItemStatusToPicked(dispDate.toDate(), "0501", 1, "Admin", workstation.getId());
				
				assert !drugPickService.isUpdateSuccess();
				assert StringUtils.equals(drugPickService.getResultMsg(), "0061");
			}
		}.run();
		
		// suspend pharmOrder
		new ComponentTest() {
			protected void testComponents() throws Exception {
				dispDate = new DateTime().withDate(2011, 3, 29);
				
				DrugPickServiceLocal drugPickService = (DrugPickServiceLocal) getInstance("drugPickService");
				drugPickService.updateDispOrderItemStatusToPicked(dispDate.toDate(), "0511", 1, "Admin", workstation.getId());
				
				assert !drugPickService.isUpdateSuccess();
				assert StringUtils.equals(drugPickService.getResultMsg(), "0059");
			}
		}.run();

	}	
}
