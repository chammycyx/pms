package hk.org.ha.model.pms.workflow.test;

import org.jboss.seam.mock.SeamTest;
import org.jbpm.graph.exe.ProcessInstance;
import org.testng.annotations.Test;

public class DispensingPMSTest extends SeamTest
{
   @Test
   public void testNotClearanceNotForceProceedComponent() throws Exception
   {
      new ComponentTest() {
         protected void testComponents() throws Exception
         {			 
			// Login
			assert getValue("#{identity.loggedIn}").equals(false);
			setValue("#{identity.username}", "admin");
			setValue("#{identity.password}", "admin");
			invokeMethod("#{identity.login}");
			assert getValue("#{identity.loggedIn}").equals(true);
         }
      }.run();

      new ComponentTest() {
		 protected void testComponents() throws Exception
         { 	                                    
			// Test for not Clearance and not force proceed
        	invokeMethod("#{dispensingPMSService.checkClearance('5001')}");
        	ProcessInstance processInstance = (ProcessInstance) getValue("#{processInstance}");
			assert processInstance.getRootToken().getNode().getName().equals("end");
         }
      }.run();
   }
   
   @Test
   public void testAllBranchesProceedComponent() throws Exception
   {
      new ComponentTest() {
         protected void testComponents() throws Exception
         {			 
			// Login
			assert getValue("#{identity.loggedIn}").equals(false);
			setValue("#{identity.username}", "admin");
			setValue("#{identity.password}", "admin");
			invokeMethod("#{identity.login}");
			assert getValue("#{identity.loggedIn}").equals(true);
         }
      }.run();

      new ComponentTest() {
		 protected void testComponents() throws Exception
         { 	                                    
			// Test for not Clearance and not force proceed
        	invokeMethod("#{dispensingPMSService.checkClearance('5002')}");
        	ProcessInstance processInstance = (ProcessInstance) getValue("#{processInstance}");
        	assert processInstance.getRootToken().getNode().getName().equals("Enter Override Reason");
        	invokeMethod("#{dispensingPMSService.mdsOverrideReason}");
        	assert processInstance.getRootToken().getNode().getName().equals("Create Refill Schedule");
        	invokeMethod("#{dispensingPMSService.createRefillSchedule}");
        	assert processInstance.getRootToken().getNode().getName().equals("Print SFI Invoice");
        	invokeMethod("#{dispensingPMSService.printSFIInvoice}");
        	assert processInstance.getRootToken().getNode().getName().equals("Suspend Dispensing Order");
        	invokeMethod("#{dispensingPMSService.suspendDispensingOrder}");
        	assert processInstance.getRootToken().getNode().getName().equals("Suspended");
        	invokeMethod("#{dispensingPMSService.resumeSuspendDispensingOrder}");
        	assert processInstance.getRootToken().getNode().getName().equals("Check SFI Clearance");
        	invokeMethod("#{dispensingPMSService.checkSFIClearance}");
        	assert processInstance.getRootToken().getNode().getName().equals("end");
         }
      }.run();
   }
   
   @Test
   public void testSimpleClearanceFlowComponent() throws Exception
   {
      new ComponentTest() {
         protected void testComponents() throws Exception
         {			 
			// Login
			assert getValue("#{identity.loggedIn}").equals(false);
			setValue("#{identity.username}", "admin");
			setValue("#{identity.password}", "admin");
			invokeMethod("#{identity.login}");
			assert getValue("#{identity.loggedIn}").equals(true);
         }
      }.run();

      new ComponentTest() {
		 protected void testComponents() throws Exception
         { 	                                    
			// Test for simple direct workflow
			invokeMethod("#{dispensingPMSService.checkClearance('5003')}");
	        ProcessInstance processInstance = (ProcessInstance) getValue("#{processInstance}");
	        assert processInstance.getRootToken().getNode().getName().equals("end");
         }
      }.run();
   }
   
   @Test
   public void testClearanceWithMDSOverrideReasonComponent() throws Exception
   {
      new ComponentTest() {
         protected void testComponents() throws Exception
         {			 
			// Login
			assert getValue("#{identity.loggedIn}").equals(false);
			setValue("#{identity.username}", "admin");
			setValue("#{identity.password}", "admin");
			invokeMethod("#{identity.login}");
			assert getValue("#{identity.loggedIn}").equals(true);
         }
      }.run();

      new ComponentTest() {
		 protected void testComponents() throws Exception
         { 	                                    
			// Test for Clearance with MDS override reason
			invokeMethod("#{dispensingPMSService.checkClearance('5004')}");
	        ProcessInstance processInstance = (ProcessInstance) getValue("#{processInstance}");
	        assert processInstance.getRootToken().getNode().getName().equals("Enter Override Reason");
	        invokeMethod("#{dispensingPMSService.mdsOverrideReason}");
	        assert processInstance.getRootToken().getNode().getName().equals("end");
         }
      }.run();
   }
   
   @Test
   public void testClearanceWithRefillComponent() throws Exception
   {
      new ComponentTest() {
         protected void testComponents() throws Exception
         {			 
			// Login
			assert getValue("#{identity.loggedIn}").equals(false);
			setValue("#{identity.username}", "admin");
			setValue("#{identity.password}", "admin");
			invokeMethod("#{identity.login}");
			assert getValue("#{identity.loggedIn}").equals(true);
         }
      }.run();

      new ComponentTest() {
		 protected void testComponents() throws Exception
         { 	                                    
			// Test for Clearance with refill
			invokeMethod("#{dispensingPMSService.checkClearance('5005')}");
	        ProcessInstance processInstance = (ProcessInstance) getValue("#{processInstance}");
	        assert processInstance.getRootToken().getNode().getName().equals("Create Refill Schedule");
	        invokeMethod("#{dispensingPMSService.createRefillSchedule}");
	        assert processInstance.getRootToken().getNode().getName().equals("end");
         }
      }.run();
   }
   
   @Test
   public void testClearanceWithSFIComponent() throws Exception
   {
      new ComponentTest() {
         protected void testComponents() throws Exception
         {			 
			// Login
			assert getValue("#{identity.loggedIn}").equals(false);
			setValue("#{identity.username}", "admin");
			setValue("#{identity.password}", "admin");
			invokeMethod("#{identity.login}");
			assert getValue("#{identity.loggedIn}").equals(true);
         }
      }.run();

      new ComponentTest() {
		 protected void testComponents() throws Exception
         { 	                                    
			// Test for Clearance with SFI process
			invokeMethod("#{dispensingPMSService.checkClearance('5006')}");
	        ProcessInstance processInstance = (ProcessInstance) getValue("#{processInstance}");
	        assert processInstance.getRootToken().getNode().getName().equals("Print SFI Invoice");
	        invokeMethod("#{dispensingPMSService.printSFIInvoice}");
	        assert processInstance.getRootToken().getNode().getName().equals("Suspend Dispensing Order");
	        invokeMethod("#{dispensingPMSService.suspendDispensingOrder}");
	        assert processInstance.getRootToken().getNode().getName().equals("Suspended");
	        invokeMethod("#{dispensingPMSService.resumeSuspendDispensingOrder}");
	        assert processInstance.getRootToken().getNode().getName().equals("Check SFI Clearance");
	        invokeMethod("#{dispensingPMSService.checkSFIClearance}");
	        assert processInstance.getRootToken().getNode().getName().equals("end");
         }
      }.run();
   }
}

