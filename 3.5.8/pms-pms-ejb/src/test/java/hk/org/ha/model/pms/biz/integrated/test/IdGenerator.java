package hk.org.ha.model.pms.biz.integrated.test;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;

import org.apache.commons.lang.WordUtils;

public class IdGenerator {

	private static IdGenerator instance;

	private Map<Class<?>, Long> idSeqMap = new HashMap<Class<?>, Long>();
	private long startSeq;
	
	public static IdGenerator getInstance() {
		synchronized (IdGenerator.class) {
			instance = new IdGenerator();
			return instance;
		}
	}

	public IdGenerator() {
		this(10000001L);
	}
	
	public IdGenerator(long startSeq) {
		this.startSeq = startSeq;
	}
	
	public void reflectSetId(Object o) {
		reflectSetId(o, "id");
	}
	
	public void reflectSetId(Object o, String property) {
		
		try {
			
			Class<?> clazz = o.getClass();		
			
			if (!isEntity(clazz)) {
				return;
			}
						
			try {
				Method gm = clazz.getMethod("get" + WordUtils.capitalize(property));
				if (gm.invoke(o) != null) {
					return;
				}
				
				Method sm = clazz.getMethod("set" + WordUtils.capitalize(property), Long.class);
				sm.invoke(o,new Object[]{generateId(clazz)});
			} catch (Exception e) {
				return;
			}			
			
			for (Method method : clazz.getMethods()) 
			{
				if ( method.getName().startsWith("get") &&
						method.getParameterTypes().length == 0) 
				{
					if (Collection.class.isAssignableFrom(method.getReturnType())) 
					{
						Collection<?> col = (Collection<?>) method.invoke(o);
						for (Object _o : col) {
							reflectSetId(_o, property);
						}
					} 
					else if (isEntity(method.getReturnType())) 
					{
						Object _o = method.invoke(o);
						if (_o != null) {
							reflectSetId(_o, property);
						}
					}
				}
			}
			
		} catch (Exception e) {
		}
	}
	
    private boolean isEntity(Class<?> clazz) {
        return clazz.isAnnotationPresent(Entity.class);
    }	
		
	private Long generateId(Class<?> clazz) {
		synchronized (idSeqMap) {
			Long id = idSeqMap.get(clazz);
			if (id == null) {
				id = Long.valueOf(startSeq);
			} else {
				id = Long.valueOf(id.longValue() + 1);
			}
			idSeqMap.put(clazz, id);
			return id;
		}
	}
	
//	public static final void main(String[] args) throws Exception {
//				
//		MedOrder medOrder = new MedOrder();
//		
//		IdGenerator.getInstance().reflectSetId(medOrder);
//	}
}
