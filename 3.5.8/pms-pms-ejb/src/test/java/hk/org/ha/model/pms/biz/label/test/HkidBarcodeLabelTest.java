package hk.org.ha.model.pms.biz.label.test;

import hk.org.ha.model.pms.biz.label.HkidBarcodeLabelServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.reftable.CustomLabel;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.vo.label.HkidBarcodeLabel;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class HkidBarcodeLabelTest extends SeamTest {

	@Test
	public void testHkidBarcodeLabelComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		

		// retrieve hkidBarcodelabel
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				HkidBarcodeLabelServiceLocal hkidBarcodeLabelService = (HkidBarcodeLabelServiceLocal) getValue("#{hkidBarcodeLabelService}");
				hkidBarcodeLabelService.retrieveHkidBarcodeLabel("A1234563");
				HkidBarcodeLabel hkidBarcodeLabel = (HkidBarcodeLabel) getValue("#{hkidBarcodeLabel}");
				assert hkidBarcodeLabel.getPatName().equals("May Lam");
			}
		}.run();
		
		// update hkidBarcodelabel
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				HkidBarcodeLabelServiceLocal hkidBarcodeLabelService = (HkidBarcodeLabelServiceLocal) getValue("#{hkidBarcodeLabelService}");
				HkidBarcodeLabel newHkidBarcodeLabel = new HkidBarcodeLabel();
				newHkidBarcodeLabel.setHkid("A1234563");
				newHkidBarcodeLabel.setPatName("May May Lam");
				newHkidBarcodeLabel.setPatNameChi("May");
				newHkidBarcodeLabel.setPrintLang(PrintLang.Eng);
				hkidBarcodeLabelService.updateHkidBarcodeLabel(newHkidBarcodeLabel);
				
				hkidBarcodeLabelService.retrieveHkidBarcodeLabel("A1234563");
				HkidBarcodeLabel hkidBarcodeLabel = (HkidBarcodeLabel) getValue("#{hkidBarcodeLabel}");
				assert hkidBarcodeLabel.getPatName().equals("May May Lam");
			}
		}.run();

		// add hkidBarcodelabel
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				HkidBarcodeLabelServiceLocal hkidBarcodeLabelService = (HkidBarcodeLabelServiceLocal) getValue("#{hkidBarcodeLabelService}");
				HkidBarcodeLabel newHkidBarcodeLabel = new HkidBarcodeLabel();
				newHkidBarcodeLabel.setHkid("A1111119");
				newHkidBarcodeLabel.setPatName("Janice Man");
				newHkidBarcodeLabel.setPatNameChi("Janice");
				newHkidBarcodeLabel.setPrintLang(PrintLang.Eng);
				hkidBarcodeLabelService.updateHkidBarcodeLabel(newHkidBarcodeLabel);
				
				hkidBarcodeLabelService.retrieveHkidBarcodeLabel("A1111119");
				HkidBarcodeLabel hkidBarcodeLabel = (HkidBarcodeLabel) getValue("#{hkidBarcodeLabel}");
				assert hkidBarcodeLabel.getPatName().equals("Janice Man");
			}
		}.run();
		
		// remove hkidBarcodelabel
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				HkidBarcodeLabelServiceLocal hkidBarcodeLabelService = (HkidBarcodeLabelServiceLocal) getValue("#{hkidBarcodeLabelService}");
				hkidBarcodeLabelService.removeHkidBarcodeLabel("A1111119");

			}
		}.run();

	}	
}
