package hk.org.ha.model.pms.biz.vetting.test;

import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.biz.ticketgen.TicketManagerLocal;
import hk.org.ha.model.pms.biz.vetting.OrderViewServiceLocal;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.InvoiceItem;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.udt.Gender;
import hk.org.ha.model.pms.udt.disp.DispOrderItemRemarkType;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderPatType;
import hk.org.ha.model.pms.udt.disp.PharmOrderStatus;
import hk.org.ha.model.pms.udt.onestop.AllowCommentType;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
import hk.org.ha.model.pms.vo.onestop.OneStopOrderInfo;
import hk.org.ha.model.pms.vo.onestop.OneStopSummary;
import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.joda.time.DateTime;
import org.testng.annotations.Test;

public class OrderViewServiceTest extends SeamTest {

	@Test
	public void testMoeOrder() throws Exception {

		final Long medOrderId = Long.valueOf(48);
		final Patient patient = new Patient();
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
	        }
		}.run();
		
		// session context
		new ComponentTest() {
			protected void testComponents() throws Exception {
				
				// oneStopSummary
				OneStopSummary oneStopSummary = new OneStopSummary();

				Calendar dob = Calendar.getInstance();
				dob.set(1976, 0, 1);
				
				patient.setId(Long.valueOf(1));
				patient.setPatKey("1234567");
				patient.setHkid("A1234563");
				patient.setDob(dob.getTime());
				patient.setSex(Gender.Male);
				patient.setName("KOK, SIU YUK");
				patient.setNameChi(new String(new int[]{33883, 23567, 29577}, 0, 3));
				patient.setOtherDoc("ID");
				
				oneStopSummary.setPatient(patient);
			
				Contexts.getSessionContext().set("oneStopSummary", oneStopSummary);

				// ticket
				TicketManagerLocal ticketManager = (TicketManagerLocal) getValue("#{ticketManager}");
				
				DateTime ticketDateTime = new DateTime(2011,4,30,0,0,0,0);
				ticketManager.retrieveTicket("0507", ticketDateTime.toDate());	
												
				// oneStopOrderData
				OneStopOrderInfo oneStopOrderData = new OneStopOrderInfo();
				
				oneStopOrderData.setPatType(PharmOrderPatType.Public);
				oneStopOrderData.setDoctorCode("441");
				oneStopOrderData.setPhsSpecCode("MED");
				oneStopOrderData.setPhsWardCode("A001");
				oneStopOrderData.setPhsPatCat("S");
				oneStopOrderData.setPrescType(MedOrderPrescType.Out);
				oneStopOrderData.getMedCase().setPasPatGroupCode("GC");
				oneStopOrderData.getMedCase().setPasAttendFlag(Boolean.TRUE);
				
				Contexts.getSessionContext().set("oneStopOrderData", oneStopOrderData);
			}
		}.run();
		
		// retrieve moe order
		new ComponentTest() {
			protected void testComponents() throws Exception {
				
				// create PharmOrder, PharmOrderItem
				OrderViewServiceLocal orderViewService = (OrderViewServiceLocal) getValue("#{orderUtilService}");
				orderViewService.retrieveOrder(medOrderId, OneStopOrderType.MedOrder, true);

				// check MedOrder
				MedOrder medOrder = (MedOrder) getValue("#{medOrder}");
				assert medOrder.getProcessingFlag();
				assert medOrder.getDoctorCode().equals("441");
				assert medOrder.getDmMedicalOfficer() != null;
				assert medOrder.getPrescType() == MedOrderPrescType.Out;
				assert medOrder.getPatient() == patient;

				// check PharmOrder
				PharmOrder pharmOrder = (PharmOrder) getValue("#{pharmOrder}");
				assert pharmOrder.getNumOfRefill() == 0;
				assert pharmOrder.getNumOfGroup() == 0;
				assert pharmOrder.getPatType() == PharmOrderPatType.Public;
				assert pharmOrder.getSpecCode().equals("MED");
				assert pharmOrder.getWorkstore().getHospCode().equals("QMH");
				assert pharmOrder.getWardCode().equals("A001");
				assert pharmOrder.getPatCatCode().equals("S");
				assert pharmOrder.getStatus() == PharmOrderStatus.Outstanding;
				assert pharmOrder.getAdminStatus() == PharmOrderAdminStatus.Normal;
				assert pharmOrder.getAllowCommentType() == AllowCommentType.None;
				assert pharmOrder.getPharmOrderItemList().size() == (medOrder.getMedOrderItemList().size() * 2);
				assert pharmOrder.getMedOrder() == medOrder;
				assert pharmOrder.getWorkstore().getId().equals(medOrder.getWorkstore().getId());
				assert pharmOrder.getMedCase().equals(medOrder.getMedCase());
				assert pharmOrder.getMedCase().getPasPatGroupCode().equals("GC");
				assert pharmOrder.getMedCase().getPasAttendFlag();
			}
		}.run();

		// set issueQty
		new ComponentTest() {
			protected void testComponents() throws Exception {
				
				PharmOrder pharmOrder = (PharmOrder) getValue("#{pharmOrder}");
				for (PharmOrderItem pharmOrderItem : pharmOrder.getPharmOrderItemList()) {
					pharmOrderItem.setIssueQty(pharmOrderItem.getCalQty());
				}
			}
		}.run();
		
		// save moe order
		new ComponentTest() {
			protected void testComponents() throws Exception {

				OrderViewServiceLocal orderViewService = (OrderViewServiceLocal) getValue("#{orderUtilService}");
				orderViewService.saveOrder((OrderViewInfo)getValue("#{orderViewInfo}"));
				MedOrder medOrder = (MedOrder) getValue("#{medOrder}");

				assert !medOrder.getProcessingFlag();
				assert medOrder.getStatus() == MedOrderStatus.Complete;
				for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
					assert medOrderItem.getStatus() == MedOrderItemStatus.Completed;
				}
				
				// check PharmOrder
				PharmOrder pharmOrder = (PharmOrder) getValue("#{pharmOrder}");
				for (PharmOrderItem pharmOrderItem : pharmOrder.getPharmOrderItemList()) {
					assert pharmOrderItem.getCalQty().add(pharmOrderItem.getAdjQty()).compareTo(pharmOrderItem.getIssueQty()) == 0;
				}
				
				// check DispOrder
				DispOrder dispOrder = (DispOrder) getValue("#{dispOrder}");

				assert dispOrder.getDispDate() != null;
				assert dispOrder.getStatus() == DispOrderStatus.Issued;
				assert !dispOrder.getUrgentFlag();
				assert dispOrder.getDispOrderItemList().size() == pharmOrder.getPharmOrderItemList().size();
				assert dispOrder.getPharmOrder() == pharmOrder;
				assert dispOrder.getTicket() != null;
				assert dispOrder.getWorkstore().getId().equals(dispOrder.getPharmOrder().getWorkstore().getId());
				assert dispOrder.getRefillScheduleList().isEmpty();
				assert (dispOrder.getInvoiceList().size() == 1 || dispOrder.getInvoiceList().size() == 2);

				Set<Long> dispOrderItemIdMap = new HashSet<Long>();
				
				// check DispOrderItem
				for (DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList()) {
					dispOrderItemIdMap.add(dispOrderItem.getId());
					assert dispOrderItem.getDispQty() == dispOrderItem.getPharmOrderItem().getIssueQty();
					assert dispOrderItem.getBaseLabel() != null;
					assert dispOrderItem.getRemarkText() == null;
					assert dispOrderItem.getRemarkType() == DispOrderItemRemarkType.Empty;
					assert dispOrderItem.getRemarkUpdateUser() == null;
					assert dispOrderItem.getRemarkUpdateDate() == null;
					assert dispOrderItem.getStatus() == DispOrderItemStatus.Vetted;
					assert dispOrderItem.getPickUser() == null;
					assert dispOrderItem.getPickDate() == null;
					assert dispOrderItem.getAssembleUser() == null;
					assert dispOrderItem.getAssembleDate() == null;
					assert dispOrderItem.getDispOrder() == dispOrder;
					assert pharmOrder.getPharmOrderItemList().contains(dispOrderItem.getPharmOrderItem());
				}
				
				// check Invoice
				assert dispOrder.getInvoiceList().size() == 1;
				assert dispOrder.getInvoiceList() != null;
				Invoice invoice = dispOrder.getInvoiceList().get(0);
				
				assert invoice.getInvoiceNum() != null;
				assert invoice.getForceProceedFlag() == Boolean.FALSE;
				assert invoice.getForceProceedUser() == null;
				assert invoice.getForceProceedCode() == null;
				assert invoice.getReceiptNum() == null;
				assert invoice.getDocType() != null;
				assert invoice.getStatus() == InvoiceStatus.Outstanding;
				assert invoice.getInvoiceItemList().size() == dispOrder.getDispOrderItemList().size();
				assert invoice.getDispOrder().getId().equals(dispOrder.getId());
				
				// check InvoiceItem
				for (InvoiceItem invoiceItem : invoice.getInvoiceItemList()) {
					assert invoiceItem.getCost().compareTo(BigDecimal.ZERO) >= 0;
					assert invoiceItem.getAmount().compareTo(BigDecimal.ZERO) >= 0;
					assert invoiceItem.getInvoice().getId().equals(invoice.getId());
					assert dispOrderItemIdMap.contains(invoiceItem.getDispOrderItem().getId());
				}
			}
			
		}.run();
	}
}