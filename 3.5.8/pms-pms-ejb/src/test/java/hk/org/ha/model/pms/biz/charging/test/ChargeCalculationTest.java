package hk.org.ha.model.pms.biz.charging.test;

import hk.org.ha.model.pms.biz.charging.ChargeCalculationInf;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.disp.InvoiceItem;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.disp.PharmOrderPatType;
import hk.org.ha.model.pms.vo.charging.DrugChargeRuleCriteria;

import java.math.BigDecimal;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class ChargeCalculationTest extends SeamTest {

	@Test
	public void testMaxProfitLimitComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				ChargeCalculationInf chargeCalculation = (ChargeCalculationInf) getValue("#{chargeCalculation}");				
				DrugChargeRuleCriteria chargingRuleCriteria = new DrugChargeRuleCriteria();
				chargingRuleCriteria.setMedOrderPrescType(MedOrderPrescType.Out.getDataValue());
				chargingRuleCriteria.setPharmOrderPatType(PharmOrderPatType.Nep.getDataValue());
				chargingRuleCriteria.setFmStatus("X");
				InvoiceItem invoiceItem = new InvoiceItem();
				invoiceItem.setCost(BigDecimal.valueOf(31004.5));
				invoiceItem.setMarkupAmount(BigDecimal.valueOf(3100));						
				//assert adjustMarkup.compareTo(BigDecimal.valueOf(200)) == 0;
			}
		}.run();
		
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				ChargeCalculationInf chargeCalculation = (ChargeCalculationInf) getValue("#{chargeCalculation}");				
				DrugChargeRuleCriteria chargingRuleCriteria = new DrugChargeRuleCriteria();
				chargingRuleCriteria.setMedOrderPrescType(MedOrderPrescType.Out.getDataValue());
				chargingRuleCriteria.setPharmOrderPatType(PharmOrderPatType.Nep.getDataValue());
				chargingRuleCriteria.setFmStatus("X");
				//BigDecimal adjustMarkup = chargeCalculation.adjustMarkupAmtByMaxProfitLimit(chargingRuleCriteria, BigDecimal.ZERO, BigDecimal.valueOf(400));				
				//assert adjustMarkup.compareTo(BigDecimal.valueOf(400)) == 0;
			}
		}.run();
		
		
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				ChargeCalculationInf chargeCalculation = (ChargeCalculationInf) getValue("#{chargeCalculation}");				
				DrugChargeRuleCriteria chargingRuleCriteria = new DrugChargeRuleCriteria();
				chargingRuleCriteria.setMedOrderPrescType(MedOrderPrescType.Out.getDataValue());
				chargingRuleCriteria.setPharmOrderPatType(PharmOrderPatType.Nep.getDataValue());
				chargingRuleCriteria.setFmStatus("X");
				//BigDecimal adjustMarkup = chargeCalculation.adjustMarkupAmtByMaxProfitLimit(chargingRuleCriteria, BigDecimal.ZERO, BigDecimal.valueOf(3200));				
				//assert adjustMarkup.compareTo(BigDecimal.valueOf(3000)) == 0;
			}
		}.run();
		
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				ChargeCalculationInf chargeCalculation = (ChargeCalculationInf) getValue("#{chargeCalculation}");				
				DrugChargeRuleCriteria chargingRuleCriteria = new DrugChargeRuleCriteria();
				chargingRuleCriteria.setMedOrderPrescType(MedOrderPrescType.Out.getDataValue());
				chargingRuleCriteria.setPharmOrderPatType(PharmOrderPatType.Public.getDataValue());
				chargingRuleCriteria.setFmStatus("X");
				//BigDecimal adjustMarkup = chargeCalculation.adjustMarkupAmtByMaxProfitLimit(chargingRuleCriteria, BigDecimal.ZERO, BigDecimal.valueOf(3200));				
				//assert adjustMarkup.compareTo(BigDecimal.valueOf(3200)) == 0;
			}
		}.run();
			
	}	

}

