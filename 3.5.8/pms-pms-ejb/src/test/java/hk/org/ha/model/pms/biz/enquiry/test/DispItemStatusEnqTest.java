	package hk.org.ha.model.pms.biz.enquiry.test;

import hk.org.ha.model.pms.biz.enquiry.DispItemStatusEnqServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.vo.enquiry.DispItemStatusEnq;

import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class DispItemStatusEnqTest extends SeamTest {

	@Test
	public void testDispItemStatusEnqComponent() throws Exception {
	
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		// retrieve dispensing item status enquiry list By case number
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {   
				DispItemStatusEnqServiceLocal dispItemStatusEnqService = (DispItemStatusEnqServiceLocal) getValue("#{dispItemStatusEnqService}");
				
				DispItemStatusEnq dispItemStatusEnq = new DispItemStatusEnq();
				dispItemStatusEnq.setCaseNum("GORB94115070");
				
				dispItemStatusEnqService.retrieveDispItemStatusEnqList(dispItemStatusEnq);

				List<DispItemStatusEnq> dispItemStatusEnqList = (List<DispItemStatusEnq>) getValue("#{dispItemStatusEnqList}");
				assert dispItemStatusEnqList.size()>0;
			}
		}.run();
		
	}	
}
