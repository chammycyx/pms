package hk.org.ha.model.pms.biz.refill.test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import hk.org.ha.model.pms.biz.refill.RefillScheduleListManagerLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.persistence.disp.RefillScheduleItem;
import hk.org.ha.model.pms.persistence.reftable.RefillConfig;
import hk.org.ha.model.pms.vo.rx.Regimen;

import org.jboss.seam.mock.SeamTest;
import org.joda.time.DateTime;
import org.testng.annotations.Test;

public class RefillScheduleListTest extends SeamTest 
{	
	//@Test 
	public void testCreateRefillScheduleComponent() throws Exception
	{
		  new ComponentTest() {			
				protected void testComponents() throws Exception
				{
					//Login		            
		            assert getValue("#{identity.loggedIn}").equals(false);
		            setValue("#{identity.username}", "admin");
		            setValue("#{identity.password}", "admin");
		            invokeMethod("#{identity.login}");
		            assert getValue("#{identity.loggedIn}").equals(true);
//		            invokeMethod("#{postLogonService.postLogon}");
		            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
		            postLogonService.postLogon("QMH", "WKS1");
				 }
	       }.run();   
	       
			// CreateRefillSchedule - OneItem
			new ComponentTest() {
				protected void testComponents() throws Exception {
					
					RefillScheduleListManagerLocal refillScheduleListManager = (RefillScheduleListManagerLocal) getValue("#{refillScheduleListManager}");
					
					RefillConfig refillConfig = new RefillConfig();
					refillConfig.setThreshold(28);
					refillConfig.setInterval(28);
					refillConfig.setLastIntervalMaxDay(56);					
					
					List<PharmOrderItem> pharmOrderItemList = new ArrayList<PharmOrderItem>();					
					
					MedOrder medOrder = new MedOrder();
					DateTime orderDate = new DateTime(2011, 6, 9, 0, 0, 0, 0);
					medOrder.setOrderDate(orderDate.toDate());
					
					PharmOrder pharmOrder = new PharmOrder();
					pharmOrder.setMedOrder(medOrder);
					
					PharmOrderItem pOI1 = new PharmOrderItem();
					Regimen regimen = new Regimen();
					regimen.setDurationInDay(133);					
					pOI1.setRegimen(regimen);
					pOI1.setIssueQty(BigDecimal.valueOf(133));
					pOI1.setPharmOrder(pharmOrder);					
					pharmOrderItemList.add(pOI1);
					
//					List<RefillSchedule> refillScheduleList = refillScheduleListManager.createRefillScheduleListFromPharmOrder(pharmOrder);
//					assert refillScheduleList != null;
//					assert refillScheduleList.size() == 4;
//					for( RefillSchedule refillSchedule : refillScheduleList ){						
//						if( refillSchedule.getRefillNum() == 1 ){
//							
//							assert refillSchedule.getRefillDate().compareTo((new DateTime(2011, 6, 9, 0, 0, 0, 0)).toDate()) == 0;
//							assert refillSchedule.getRefillNum() == 1;
//							
//							RefillScheduleItem refillScheduleItem = refillSchedule.getRefillScheduleItemList().get(0);
//							assert refillScheduleItem.getRefillQty() == 28;
//							
//						}else if( refillSchedule.getRefillNum() == 2 ){
//							
//							assert refillSchedule.getRefillDate().compareTo((new DateTime(2011, 7, 7, 0, 0, 0, 0)).toDate()) == 0;
//							assert refillSchedule.getRefillNum() == 2;
//							
//							RefillScheduleItem refillScheduleItem = refillSchedule.getRefillScheduleItemList().get(0);
//							assert refillScheduleItem.getRefillQty() == 28;
//							
//						}else if( refillSchedule.getRefillNum() == 3 ){
//							
//							assert refillSchedule.getRefillDate().compareTo((new DateTime(2011, 8, 4, 0, 0, 0, 0)).toDate()) == 0;
//							assert refillSchedule.getRefillNum() == 3;
//							
//							RefillScheduleItem refillScheduleItem = refillSchedule.getRefillScheduleItemList().get(0);
//							assert refillScheduleItem.getRefillQty() == 28;
//						}else if( refillSchedule.getRefillNum() == 4 ){
//							
//							assert refillSchedule.getRefillDate().compareTo((new DateTime(2011, 9, 1, 0, 0, 0, 0)).toDate()) == 0;
//							assert refillSchedule.getRefillNum() == 4;
//							
//							RefillScheduleItem refillScheduleItem = refillSchedule.getRefillScheduleItemList().get(0);
//							assert refillScheduleItem.getRefillQty() == 49;
//						}
//					}
					
				}
				
			}.run();
			
			
			// CreateRefillSchedule - OneItem
			new ComponentTest() {
				protected void testComponents() throws Exception {
					
					RefillScheduleListManagerLocal refillScheduleListManager = (RefillScheduleListManagerLocal) getValue("#{refillScheduleListManager}");
					
					RefillConfig refillConfig = new RefillConfig();
					refillConfig.setThreshold(28);
					refillConfig.setInterval(28);
					refillConfig.setLastIntervalMaxDay(56);					
					refillConfig.setEnableSelectionFlag(false);
					refillConfig.setManuRxNoPasSpecFlag(false);
					refillConfig.setEnableQtyAdjFlag(false);
					refillConfig.setEnablePharmClinicFlag(false);					
					
					List<PharmOrderItem> pharmOrderItemList = new ArrayList<PharmOrderItem>();					
					
					MedOrder medOrder = new MedOrder();
					DateTime orderDate = new DateTime(2011, 6, 9, 0, 0, 0, 0);
					medOrder.setOrderDate(orderDate.toDate());
					
					PharmOrder pharmOrder = new PharmOrder();					
					pharmOrder.setMedOrder(medOrder);
					
					PharmOrderItem pOI1 = new PharmOrderItem();
					Regimen regimen = new Regimen();
					regimen.setDurationInDay(133);					
					pOI1.setItemCode("PARA01");
					pOI1.setRegimen(regimen);
					pOI1.setIssueQty(BigDecimal.valueOf(133));
					pOI1.setPharmOrder(pharmOrder);					
					pharmOrderItemList.add(pOI1);
					
					PharmOrderItem pOI2 = new PharmOrderItem();
					Regimen regimen2 = new Regimen();
					regimen2.setDurationInDay(70);					
					pOI2.setItemCode("ACET10");
					pOI2.setRegimen(regimen2);
					pOI2.setIssueQty(BigDecimal.valueOf(70));
					pOI2.setPharmOrder(pharmOrder);					
					pharmOrderItemList.add(pOI2);					
					
//					List<RefillSchedule> refillScheduleList = refillScheduleListManager.createRefillScheduleListFromPharmOrder(pharmOrder);
//					assert refillScheduleList != null;
//					assert refillScheduleList.size() == 4;
//					for( RefillSchedule refillSchedule : refillScheduleList ){						
//						if( refillSchedule.getRefillNum() == 1 ){
//							
//							assert refillSchedule.getRefillDate().compareTo((new DateTime(2011, 6, 9, 0, 0, 0, 0)).toDate()) == 0;
//							assert refillSchedule.getRefillNum() == 1;
//							assert refillSchedule.getRefillScheduleItemList().size()  == 2;														
//							for( RefillScheduleItem refillScheduleItem : refillSchedule.getRefillScheduleItemList() ){
//								assert refillScheduleItem.getRefillQty() == 28;								
//							}
//							
//						}else if( refillSchedule.getRefillNum() == 2 ){
//							
//							assert refillSchedule.getRefillDate().compareTo((new DateTime(2011, 7, 7, 0, 0, 0, 0)).toDate()) == 0;
//							assert refillSchedule.getRefillNum() == 2;							
//							assert refillSchedule.getRefillScheduleItemList().size()  == 2;	
//							
//							for( RefillScheduleItem refillScheduleItem : refillSchedule.getRefillScheduleItemList() ){
//								if( "PARA01".equals(refillScheduleItem.getPharmOrderItem().getItemCode()) ){
//									assert refillScheduleItem.getRefillQty() == 28;
//								}else if( "ACET10".equals(refillScheduleItem.getPharmOrderItem().getItemCode()) ){
//									assert refillScheduleItem.getRefillQty() == 42;
//								}
//							}
//							
//						}else if( refillSchedule.getRefillNum() == 3 ){
//							
//							assert refillSchedule.getRefillDate().compareTo((new DateTime(2011, 8, 4, 0, 0, 0, 0)).toDate()) == 0;
//							assert refillSchedule.getRefillNum() == 3;
//							assert refillSchedule.getRefillScheduleItemList().size()  == 1;								
//							assert refillSchedule.getRefillScheduleItemList().get(0).getRefillQty() == 28;
//							
//						}else if( refillSchedule.getRefillNum() == 4 ){
//							
//							assert refillSchedule.getRefillDate().compareTo((new DateTime(2011, 9, 1, 0, 0, 0, 0)).toDate()) == 0;
//							assert refillSchedule.getRefillNum() == 4;
//							assert refillSchedule.getRefillScheduleItemList().size()  == 1;								
//							assert refillSchedule.getRefillScheduleItemList().get(0).getRefillQty() == 49;						}
//					}					
				}
				
			}.run();
	}
}
