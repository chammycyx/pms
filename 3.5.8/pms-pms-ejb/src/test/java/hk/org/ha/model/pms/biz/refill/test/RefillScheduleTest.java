package hk.org.ha.model.pms.biz.refill.test;

import java.sql.Date;

import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.persistence.disp.RefillScheduleItem;
import hk.org.ha.model.pms.udt.refill.RefillScheduleStatus;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class RefillScheduleTest extends SeamTest 
{
	private Long refillScheduleId;
	
	@Test 
	public void testRefillScheduleComponent() throws Exception
	{
		  new ComponentTest() {			
				protected void testComponents() throws Exception
				{
					//Login 
					assert getValue("#{identity.loggedIn}").equals(false);
		            setValue("#{identity.username}", "admin");
		            setValue("#{identity.password}", "admin");
		            invokeMethod("#{identity.login}");
		            assert getValue("#{identity.loggedIn}").equals(true);
				 }
	       }.run();       

	       
			// retrieveRefillScheduleByOrderNum
			new ComponentTest() {
				protected void testComponents() throws Exception {
					invokeMethod("#{refillScheduleListManager.retrieveRefillScheduleByOrderNum(100002001)}");
					assert getValue("#{refillScheduleList.size()}").equals(1);

					invokeMethod("#{refillScheduleListManager.retrieveRefillScheduleByOrderNum(100002002)}");
					assert getValue("#{refillScheduleList.size()}").equals(0);
					
					invokeMethod("#{refillScheduleListManager.retrieveRefillScheduleByOrderNum(100002003)}");
					assert getValue("#{refillScheduleList.size()}").equals(0);
					
					invokeMethod("#{refillScheduleListManager.retrieveRefillScheduleByOrderNum(100002004)}");
					assert getValue("#{refillScheduleList.size()}").equals(0);
				}
			}.run();
			
			// Add RefillSchedule
			new ComponentTest() {
				protected void testComponents() throws Exception {
		            // retrievePharmOrder
		            invokeMethod("#{pharmOrderService.retrievePharmOrder(1)}");
		            PharmOrder pharmOrder = (PharmOrder) getValue("#{pharmOrder}");		            
		            assert pharmOrder.getRefillScheduleList().size() == 1;            
		            		            		            		            
		            // Add RefillSchedule		
		            invokeMethod("#{refillScheduleService.createRefillSchedule}");
		            RefillSchedule refillSchedule = (RefillSchedule) getValue("#{refillSchedule}");
		            refillSchedule.setRefillCouponNum("2");
		            refillSchedule.setGroupNum(1);
		            refillSchedule.setRefillNum(2);
		            refillSchedule.setRefillDate(Date.valueOf("2010-12-01"));
		            refillSchedule.setStatus(RefillScheduleStatus.NotYetDispensed);		            
		            refillSchedule.setPharmOrder(pharmOrder);
					
		            invokeMethod("#{refillScheduleService.updateRefillSchedule}");
		            refillScheduleId = (Long) getValue("#{refillSchedule.getId()}");
				}
			}.run();
			
			// Check RefillSchedule
			new ComponentTest() {
				protected void testComponents() throws Exception {
					invokeMethod("#{refillScheduleService.retrieveRefillSchedule("+refillScheduleId+")}");
					assert getValue("#{refillSchedule.refillCouponNum}").equals(2);
					assert getValue("#{refillSchedule.status}").equals("O");
				}
			}.run();
			
			// Update RefillSchedule
			new ComponentTest() {
				protected void testComponents() throws Exception {
					setValue("#{refillSchedule.status}", "I");
					invokeMethod("#{refillScheduleService.updateRefillSchedule}");
				}
			}.run();
			
			// Check RefillSchedule
			new ComponentTest() {
				protected void testComponents() throws Exception {
					invokeMethod("#{refillScheduleService.retrieveRefillSchedule("+refillScheduleId+")}");
					assert getValue("#{refillSchedule.refillCouponNum}").equals(2);
					assert getValue("#{refillSchedule.status}").equals("I");
				}
			}.run();
			
			// Delete RefillSchedule
			new ComponentTest() {
				protected void testComponents() throws Exception {
					invokeMethod("#{refillScheduleService.deleteRefillSchedule(refillSchedule)}");
				}
			}.run();
			
			// Check RefillSchedule
			new ComponentTest() {
				protected void testComponents() throws Exception {
					invokeMethod("#{refillScheduleService.retrieveRefillSchedule("+refillScheduleId+")}");
					assert getValue("#{refillSchedule}") == null;
				}
			}.run(); 
	}
	
	@Test 
	public void testRefillScheduleItemComponent() throws Exception
	{
		  new ComponentTest() {			
				protected void testComponents() throws Exception
				{
					//Login 
					assert getValue("#{identity.loggedIn}").equals(false);
		            setValue("#{identity.username}", "admin");
		            setValue("#{identity.password}", "admin");
		            invokeMethod("#{identity.login}");
		            assert getValue("#{identity.loggedIn}").equals(true);
				 }
	       }.run();       
			
			// retrieve RefillScheduleItemList
			new ComponentTest() {
				protected void testComponents() throws Exception {
		            // retrieveRefillSchedule
		            invokeMethod("#{refillScheduleService.retrieveRefillSchedule(1)}");
		            RefillSchedule refillSchedule = (RefillSchedule) getValue("#{refillSchedule}");		            
		            assert refillSchedule.getRefillScheduleItemList().size() == 1;            
				}
			}.run();
			
			// Add RefillSchedule
			new ComponentTest() {
				protected void testComponents() throws Exception {
		            // retrievePharmOrderItemList
		            invokeMethod("#{pharmOrderService.retrievePharmOrder(1)}");
		            PharmOrder pharmOrder = (PharmOrder) getValue("#{pharmOrder}");		            
		            assert pharmOrder.getPharmOrderItemList().size() == 1;
		            		
		            RefillSchedule refillSchedule = (RefillSchedule) getValue("#{refillSchedule}");
		            
		            // Add RefillScheduleItem				           
		            RefillScheduleItem refillScheduleItem = new RefillScheduleItem();
		            refillScheduleItem.setPharmOrderItem(pharmOrder.getPharmOrderItemList().get(0));
		            refillScheduleItem.setRefillQty(14);
		            refillScheduleItem.setRefillSchedule(refillSchedule);
		            
		            refillSchedule.addRefillScheduleItem(refillScheduleItem);		            
		            invokeMethod("#{refillScheduleService.updateRefillSchedule}");
				}
			}.run();
			
			// Check RefillScheduleItem
			new ComponentTest() {
				protected void testComponents() throws Exception {
					invokeMethod("#{refillScheduleService.retrieveRefillSchedule(1)}");
					RefillSchedule refillSchedule = (RefillSchedule) getValue("#{refillSchedule}");
					assert refillSchedule.getRefillScheduleItemList().size() == 2;
					
					RefillScheduleItem refillScheduleItem = refillSchedule.getRefillScheduleItemList().get(1);
					assert refillScheduleItem.getRefillQty() == 14;
				}
			}.run();
			
			// Update RefillScheduleItem
			new ComponentTest() {
				protected void testComponents() throws Exception {
					RefillSchedule refillSchedule = (RefillSchedule) getValue("#{refillSchedule}");
					RefillScheduleItem refillScheduleItem = refillSchedule.getRefillScheduleItemList().get(1);
					refillScheduleItem.setRefillQty(7);					
					invokeMethod("#{refillScheduleService.updateRefillSchedule}");
				}
			}.run();
			
			// Check RefillScheduleItem
			new ComponentTest() {
				protected void testComponents() throws Exception {
					invokeMethod("#{refillScheduleService.retrieveRefillSchedule(1)}");
					RefillSchedule refillSchedule = (RefillSchedule) getValue("#{refillSchedule}");
					RefillScheduleItem refillScheduleItem = refillSchedule.getRefillScheduleItemList().get(1);
					assert refillScheduleItem.getRefillQty() == 7;
				}
			}.run();
			
			// Delete RefillScheduleItem
			new ComponentTest() {
				protected void testComponents() throws Exception {
					RefillSchedule refillSchedule = (RefillSchedule) getValue("#{refillSchedule}");
					refillSchedule.getRefillScheduleItemList().get(1).setRefillSchedule(null);
					refillSchedule.getRefillScheduleItemList().remove(refillSchedule.getRefillScheduleItemList().get(1));
					invokeMethod("#{refillScheduleService.updateRefillSchedule}");
					
				}
			}.run();
			
			// Check RefillScheduleItemList
			new ComponentTest() {
				protected void testComponents() throws Exception {
					invokeMethod("#{refillScheduleService.retrieveRefillSchedule(1)}");
		            RefillSchedule refillSchedule = (RefillSchedule) getValue("#{refillSchedule}");		            
		            assert refillSchedule.getRefillScheduleItemList().size() == 1; 
				}
			}.run();
	}
}
