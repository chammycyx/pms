	package hk.org.ha.model.pms.report.test;

import hk.org.ha.model.pms.biz.report.ItemLocationRptServiceLocal;
import hk.org.ha.model.pms.biz.report.WorkstoreListManagerLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.ItemLocation;
import hk.org.ha.model.pms.udt.reftable.MachineType;
import hk.org.ha.model.pms.vo.report.ItemLocationRpt;

import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class ItemLocationRptTest extends SeamTest {

	@Test
	public void testItemLocationRptComponent() throws Exception {
	
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		// retrieve item location list sortBy itemCode
		
		//retrieve drop down lists
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {   
				ItemLocationRptServiceLocal itemLocationRptService = (ItemLocationRptServiceLocal) getValue("#{itemLocationRptService}");
				
				ItemLocationRpt itemLocationRpt = new ItemLocationRpt();
				itemLocationRpt.setWorkstoreCode("WKS1");
				itemLocationRpt.setOrderBy("itemCode");
				
				itemLocationRptService.retrieveItemLocationRpt(itemLocationRpt);
				
				List<ItemLocation> itemLocationRptList = itemLocationRptService.retrieveItemLocationList(itemLocationRpt);
				assert itemLocationRptList.size()>0;
				
				List<String> itemCodeList = (List<String>) getValue("#{itemCodeList}");
				assert itemCodeList.size()>0;
				
				List<String> itemLocationRptPickStationList = (List<String>) getValue("#{itemLocationRptPickStationList}");
				assert itemLocationRptPickStationList.size()>0;
				
				List<String> bakerCellList = (List<String>) getValue("#{bakerCellList}");
				assert bakerCellList.size()>0;
				
				List<String> binNumList = (List<String>) getValue("#{binNumList}");
				assert binNumList.size()>0;
			}
		}.run();
				
		// retrieve item location list sortBy pickstation
				new ComponentTest() {
			protected void testComponents() throws Exception {   
				ItemLocationRptServiceLocal itemLocationRptService = (ItemLocationRptServiceLocal) getValue("#{itemLocationRptService}");
				
				ItemLocationRpt itemLocationRpt = new ItemLocationRpt();
				itemLocationRpt.setWorkstoreCode("WKS1");
				itemLocationRpt.setOrderBy("machine.pickStation.pickStationNum");
				
				List<ItemLocation> itemLocationRptList = itemLocationRptService.retrieveItemLocationList(itemLocationRpt);
				assert itemLocationRptList.size()>0;
			}
		}.run();
		
		// retrieve item location list sortBy binNum
		new ComponentTest() {
			protected void testComponents() throws Exception {   
				ItemLocationRptServiceLocal itemLocationRptService = (ItemLocationRptServiceLocal) getValue("#{itemLocationRptService}");
				
				ItemLocationRpt itemLocationRpt = new ItemLocationRpt();
				itemLocationRpt.setWorkstoreCode("WKS1");
				itemLocationRpt.setOrderBy("binNum");
				
				List<ItemLocation> itemLocationRptList = itemLocationRptService.retrieveItemLocationList(itemLocationRpt);
				assert itemLocationRptList.size()>0;
			}
		}.run();
		
		// retrieve item location list sortBy machine.type
		new ComponentTest() {
			protected void testComponents() throws Exception {   
				ItemLocationRptServiceLocal itemLocationRptService = (ItemLocationRptServiceLocal) getValue("#{itemLocationRptService}");
				
				ItemLocationRpt itemLocationRpt = new ItemLocationRpt();
				itemLocationRpt.setWorkstoreCode("WKS1");
				itemLocationRpt.setOrderBy("machine.type");
				
				List<ItemLocation> itemLocationRptList = itemLocationRptService.retrieveItemLocationList(itemLocationRpt);
				assert itemLocationRptList.size()>0;
			}
		}.run();
		
		// retrieve item location list sortBy bakerCellNum
		new ComponentTest() {
			protected void testComponents() throws Exception {   
				ItemLocationRptServiceLocal itemLocationRptService = (ItemLocationRptServiceLocal) getValue("#{itemLocationRptService}");
				
				ItemLocationRpt itemLocationRpt = new ItemLocationRpt();
				itemLocationRpt.setWorkstoreCode("WKS1");
				itemLocationRpt.setOrderBy("bakerCellNum");
				
				List<ItemLocation> itemLocationRptList = itemLocationRptService.retrieveItemLocationList(itemLocationRpt);
				assert itemLocationRptList.size()>0;
			}
		}.run();
		
		// retrieve item location list sortBy suspend
		new ComponentTest() {
			protected void testComponents() throws Exception {   
				ItemLocationRptServiceLocal itemLocationRptService = (ItemLocationRptServiceLocal) getValue("#{itemLocationRptService}");
				
				ItemLocationRpt itemLocationRpt = new ItemLocationRpt();
				itemLocationRpt.setWorkstoreCode("WKS1");
				itemLocationRpt.setOrderBy("suspend");
				itemLocationRpt.setSuspend("N");
				
				itemLocationRptService.retrieveItemLocationRpt(itemLocationRpt);
				List<ItemLocation> itemLocationRptList = itemLocationRptService.retrieveItemLocationList(itemLocationRpt);
				assert itemLocationRptList.size()>0;
			}
		}.run();
		
		// retrieve item location list filter By itemCode
		new ComponentTest() {
			protected void testComponents() throws Exception {   
				ItemLocationRptServiceLocal itemLocationRptService = (ItemLocationRptServiceLocal) getValue("#{itemLocationRptService}");
				
				ItemLocationRpt itemLocationRpt = new ItemLocationRpt();
				itemLocationRpt.setWorkstoreCode("WKS1");
				itemLocationRpt.setOrderBy("itemCode");
				itemLocationRpt.setItemCode("PARA01");
				
				List<ItemLocation> itemLocationRptList = itemLocationRptService.retrieveItemLocationList(itemLocationRpt);
				assert itemLocationRptList.size()>0;
			}
		}.run();
		
		// retrieve item location list filter By pickstation
		new ComponentTest() {
			protected void testComponents() throws Exception {   
				ItemLocationRptServiceLocal itemLocationRptService = (ItemLocationRptServiceLocal) getValue("#{itemLocationRptService}");
				
				ItemLocationRpt itemLocationRpt = new ItemLocationRpt();
				itemLocationRpt.setWorkstoreCode("WKS1");
				itemLocationRpt.setOrderBy("itemCode");
				itemLocationRpt.setPickStationNum(1);
				
				List<ItemLocation> itemLocationRptList = itemLocationRptService.retrieveItemLocationList(itemLocationRpt);
				assert itemLocationRptList.size()>0;
			}
		}.run();
		
		// retrieve item location list filter By binNum
		new ComponentTest() {
			protected void testComponents() throws Exception {   
				ItemLocationRptServiceLocal itemLocationRptService = (ItemLocationRptServiceLocal) getValue("#{itemLocationRptService}");
				
				ItemLocationRpt itemLocationRpt = new ItemLocationRpt();
				itemLocationRpt.setWorkstoreCode("WKS1");
				itemLocationRpt.setOrderBy("itemCode");
				itemLocationRpt.setBinNum("A001");
				
				List<ItemLocation> itemLocationRptList = itemLocationRptService.retrieveItemLocationList(itemLocationRpt);
				assert itemLocationRptList.size()>0;
			}
		}.run();
		
		// retrieve item location list filter By type
		new ComponentTest() {
			protected void testComponents() throws Exception {   
				ItemLocationRptServiceLocal itemLocationRptService = (ItemLocationRptServiceLocal) getValue("#{itemLocationRptService}");
				
				ItemLocationRpt itemLocationRpt = new ItemLocationRpt();
				itemLocationRpt.setWorkstoreCode("WKS1");
				itemLocationRpt.setOrderBy("itemCode");
				itemLocationRpt.setType(MachineType.BakerCell);
				
				List<ItemLocation> itemLocationRptList = itemLocationRptService.retrieveItemLocationList(itemLocationRpt);
				assert itemLocationRptList.size()>0;
			}
		}.run();
		
		// retrieve item location list filter By bakerCellId
		new ComponentTest() {
			protected void testComponents() throws Exception {   
				ItemLocationRptServiceLocal itemLocationRptService = (ItemLocationRptServiceLocal) getValue("#{itemLocationRptService}");
				
				ItemLocationRpt itemLocationRpt = new ItemLocationRpt();
				itemLocationRpt.setWorkstoreCode("WKS1");
				itemLocationRpt.setOrderBy("itemCode");
				itemLocationRpt.setBakerCellNum("2777");
				
				List<ItemLocation> itemLocationRptList = itemLocationRptService.retrieveItemLocationList(itemLocationRpt);
				assert itemLocationRptList.size()>0;
			}
		}.run();
		
		// retrieve item location list filter By suspend
		new ComponentTest() {
			protected void testComponents() throws Exception {   
				ItemLocationRptServiceLocal itemLocationRptService = (ItemLocationRptServiceLocal) getValue("#{itemLocationRptService}");
				
				ItemLocationRpt itemLocationRpt = new ItemLocationRpt();
				itemLocationRpt.setWorkstoreCode("WKS1");
				itemLocationRpt.setOrderBy("itemCode");
				itemLocationRpt.setSuspend("Y");
				
				itemLocationRptService.retrieveItemLocationRpt(itemLocationRpt);
				List<ItemLocation> itemLocationRptList = itemLocationRptService.retrieveItemLocationList(itemLocationRpt);
				assert itemLocationRptList.size()>0;
			}
		}.run(); 

	}	
}
