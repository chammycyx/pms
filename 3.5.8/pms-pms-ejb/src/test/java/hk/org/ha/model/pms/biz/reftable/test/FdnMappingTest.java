package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.reftable.FdnMapping;
import hk.org.ha.model.pms.udt.reftable.FdnType;
import hk.org.ha.model.pms.udt.vetting.OrderType;

import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class FdnMappingTest extends SeamTest {

	@Test
	public void testFdnMappingComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		
		// retrieve FdnMapping list
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				invokeMethod("#{fdnMappingListService.retrieveFdnMappingList('ACTI01')}");
				List<FdnMapping> fdnMappingList = (List<FdnMapping>) getValue("#{fdnMappingList}");
				assert fdnMappingList.size()>0;
			}
		}.run();
		
		// Edit FdnMapping list
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {

				invokeMethod("#{fdnMappingListService.retrieveFdnMappingList('ACTI01')}");
				List<FdnMapping> fdnMappingList = (List<FdnMapping>) getValue("#{fdnMappingList}");
				assert fdnMappingList.size()>0;
				
				for(FdnMapping fdnMapping:fdnMappingList){
					if(fdnMapping.getWorkstore().getWorkstoreCode().equals("WKS1")){
						fdnMapping.setType(FdnType.FloatDrugName);
						fdnMapping.setFloatDrugName("WKS1 ACTIFED Changed");
					}else if(fdnMapping.getWorkstore().getWorkstoreCode().equals("WKS")){
						fdnMapping.setType(FdnType.FloatDrugName);
						fdnMapping.setFloatDrugName("WKS2 ACTIFED Changed");
					}
				}
				invokeMethod("#{fdnMappingListService.updateFdnMappingList(fdnMappingList, 'ACTI01')}");
			}
		
		}.run();
		
		//check FdnMapping list
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				invokeMethod("#{fdnMappingListService.retrieveFdnMappingList('ACTI01')}");
				List<FdnMapping> fdnMappingList = (List<FdnMapping>) getValue("#{fdnMappingList}");
				assert fdnMappingList.size()>0;
				
				int check = 0;
				
				for(FdnMapping fdnMapping:fdnMappingList){
					if(fdnMapping.getWorkstore().getWorkstoreCode().equals("WKS1")){
						assert fdnMapping.getType().equals(FdnType.FloatDrugName);
						assert fdnMapping.getFloatDrugName().equals("WKS1 ACTIFED Changed");
						assert fdnMapping.getOrderType() == OrderType.OutPatient;
						check++;
					}else if(fdnMapping.getWorkstore().getWorkstoreCode().equals("WKS")){
						assert fdnMapping.getType().equals(FdnType.FloatDrugName);
						assert fdnMapping.getFloatDrugName().equals("WKS2 ACTIFED Changed");
						assert fdnMapping.getOrderType() == OrderType.OutPatient;
						check++;
					}
				}
				assert check==2;
			}
		}.run();
		
		//add new FdnMapping
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {

				invokeMethod("#{fdnMappingListService.retrieveFdnMappingList('BARO01')}");
				List<FdnMapping> fdnMappingList = (List<FdnMapping>) getValue("#{fdnMappingList}");
				
				if( !fdnMappingList.isEmpty() ) {
					for(FdnMapping fdnMapping:fdnMappingList){
						if(fdnMapping.getWorkstore().getWorkstoreCode().equals("WKS1")){
							fdnMapping.setType(FdnType.FloatDrugName);
							fdnMapping.setFloatDrugName("BAROS");
						}else if(fdnMapping.getWorkstore().getWorkstoreCode().equals("WKS")){
							fdnMapping.setType(FdnType.FloatDrugName);
							fdnMapping.setFloatDrugName("BAROS");
						}
					}
					invokeMethod("#{fdnMappingListService.updateFdnMappingList(fdnMappingList, 'BARO01')}");
					assert true;
				}else {
					assert false;
				}
			}
		
		}.run();
		
		//check FdnMapping list
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				invokeMethod("#{fdnMappingListService.retrieveFdnMappingList('BARO01')}");
				List<FdnMapping> fdnMappingList = (List<FdnMapping>) getValue("#{fdnMappingList}");
				assert fdnMappingList.size()>0;
				
				int check = 0;
				
				for(FdnMapping fdnMapping:fdnMappingList){
					if(fdnMapping.getWorkstore().getWorkstoreCode().equals("WKS1")){
						assert fdnMapping.getType().equals(FdnType.FloatDrugName);
						assert fdnMapping.getFloatDrugName().equals("BAROS");
						assert fdnMapping.getOrderType() == OrderType.OutPatient;
						check++;
					}else if(fdnMapping.getWorkstore().getWorkstoreCode().equals("WKS")){
						assert fdnMapping.getType().equals(FdnType.FloatDrugName);
						assert fdnMapping.getFloatDrugName().equals("BAROS");
						assert fdnMapping.getOrderType() == OrderType.OutPatient;
						check++;
					}
				}
				assert check==2;
			}
		}.run();
		
		//delete FdnMapping
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {

				invokeMethod("#{fdnMappingListService.retrieveFdnMappingList('BARO01')}");
				List<FdnMapping> fdnMappingList = (List<FdnMapping>) getValue("#{fdnMappingList}");
				
				if( !fdnMappingList.isEmpty() ) {
					for(FdnMapping fdnMapping:fdnMappingList){
						if(fdnMapping.getWorkstore().getWorkstoreCode().equals("WKS1")){
							fdnMapping.setType(FdnType.DefaultName);
							fdnMapping.setFloatDrugName("BAROS");
						}else if(fdnMapping.getWorkstore().getWorkstoreCode().equals("WKS")){
							fdnMapping.setType(FdnType.DefaultName);
							fdnMapping.setFloatDrugName("BAROS");
						}
					}
					invokeMethod("#{fdnMappingListService.updateFdnMappingList(fdnMappingList, 'BARO01')}");
					assert true;
				}else {
					assert false;
				}
			}
		
		}.run();
		
		//check FdnMapping list
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				invokeMethod("#{fdnMappingListService.retrieveFdnMappingList('BARO01')}");
				List<FdnMapping> fdnMappingList = (List<FdnMapping>) getValue("#{fdnMappingList}");
				assert fdnMappingList.size()>0;
				
				int check = 0;
				
				for(FdnMapping fdnMapping:fdnMappingList){
					if(fdnMapping.getWorkstore().getWorkstoreCode().equals("WKS1")){
						assert fdnMapping.getType().equals(FdnType.DefaultName);
						assert fdnMapping.getOrderType() == OrderType.OutPatient;
						check++;
					}else if(fdnMapping.getWorkstore().getWorkstoreCode().equals("WKS")){
						assert fdnMapping.getType().equals(FdnType.DefaultName);
						assert fdnMapping.getOrderType() == OrderType.OutPatient;
						check++;
					}
				}
				assert check==2;
			}
		}.run();
	}	
}
