package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmDrugProperty;
import hk.org.ha.model.pms.dms.persistence.DmMoeProperty;
import hk.org.ha.model.pms.dms.persistence.MsFmStatus;
import hk.org.ha.model.pms.dms.persistence.PmsDosageConversion;
import hk.org.ha.model.pms.dms.persistence.PmsDosageConversionSet;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class DosageConversionListTest  extends SeamTest  {

	@Test
	public void testDosageConversionListComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		//retrieve pmsDosageConversionSet
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				int moDosage = 1000;
				String itemCode = "PARA01";
				DmDrug dmDrug = new DmDrug();
				dmDrug.setDrugKey(2922);
				MsFmStatus m = new MsFmStatus();
				m.setFmStatus("1");
				dmDrug.setPmsFmStatus(m);
				
				DmDrugProperty dmDrugProperty = new DmDrugProperty();
				dmDrugProperty.setDisplayname("PARACETAMOL");
				dmDrug.setDmDrugProperty(dmDrugProperty);
				
				DmMoeProperty dmMoeProperty = new DmMoeProperty(); 
				dmMoeProperty.setMoDosageUnit("MG");
				dmDrug.setDmMoeProperty(dmMoeProperty);
				
				Contexts.getSessionContext().set("dmDrug", dmDrug);
				Contexts.getSessionContext().set("moDosage", moDosage);
				Contexts.getSessionContext().set("itemCode", itemCode);
				
				invokeMethod("#{dosageConversionListService.retrievePmsDosageConversionByMoDosage(dmDrug, moDosage)}");
				PmsDosageConversionSet pmsDosageConversionSet = (PmsDosageConversionSet)getValue("#{pmsDosageConversionSet}");
				
				assert !pmsDosageConversionSet.getPmsDosageConversionList().isEmpty();
				for ( PmsDosageConversion pdc: pmsDosageConversionSet.getPmsDosageConversionList() ) {
					assert pdc.getDmDrug() != null;
				}
			}
		}.run();


		//update dmDrugList - original list
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				String itemCode = "PARA01";
				DmDrug dmDrug = new DmDrug();
				dmDrug.setDrugKey(2922);
				MsFmStatus m = new MsFmStatus();
				m.setFmStatus("1");
				dmDrug.setPmsFmStatus(m);
				
				DmDrugProperty dmDrugProperty = new DmDrugProperty();
				dmDrugProperty.setDisplayname("PARACETAMOL");
				dmDrug.setDmDrugProperty(dmDrugProperty);
				
				DmMoeProperty dmMoeProperty = new DmMoeProperty(); 
				dmMoeProperty.setMoDosageUnit("MG");
				dmDrug.setDmMoeProperty(dmMoeProperty);
				
				Contexts.getSessionContext().set("dmDrug", dmDrug);
				invokeMethod("#{dosageConversionListService.retrieveDmDrugListByDrugKeyFmStatus(dmDrug)}");
				List<WorkstoreDrug> originalDmDrugList = (List<WorkstoreDrug>)getValue("#{originalDmDrugList}");
				
				assert !originalDmDrugList.isEmpty();
				for ( WorkstoreDrug d : originalDmDrugList ) {
					if ( StringUtils.equals(d.getDmDrug().getItemCode(), "PARA01") ) {
						assert StringUtils.equals( d.getDmDrug().getDrugName(), "PARACETAMOL" );
					}
				}
			}
		}.run();
		
		//update dmDrugList - extend list
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				String itemCode = "PARA01";
				DmDrug dmDrug = new DmDrug();
				dmDrug.setDrugKey(2922);
				MsFmStatus m = new MsFmStatus();
				m.setFmStatus("1");
				dmDrug.setPmsFmStatus(m);
				
				DmDrugProperty dmDrugProperty = new DmDrugProperty();
				dmDrugProperty.setDisplayname("PARACETAMOL");
				dmDrug.setDmDrugProperty(dmDrugProperty);
				
				DmMoeProperty dmMoeProperty = new DmMoeProperty(); 
				dmMoeProperty.setMoDosageUnit("MG");
				dmDrug.setDmMoeProperty(dmMoeProperty);
				
				Contexts.getSessionContext().set("dmDrug", dmDrug);
				invokeMethod("#{dosageConversionListService.retrieveDmDrugListByDisplaynameFmStatus(dmDrug)}");
				List<WorkstoreDrug> extendDmDrugList = (List<WorkstoreDrug>)getValue("#{extendDmDrugList}");
				
				assert !extendDmDrugList.isEmpty();
				for ( WorkstoreDrug d : extendDmDrugList ) {
					if ( StringUtils.equals(d.getDmDrug().getItemCode(), "PARA01") ) {
						assert StringUtils.equals( d.getDmDrug().getDrugName(), "PARACETAMOL" );
					}
				}
			}
		}.run();
		
		//create pmsDosageConversionSet
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				int moDosage = 1000;
				String itemCode = "PARA01";
				
				DmDrug dmDrug = new DmDrug();
				dmDrug.setDrugKey(2922);
				MsFmStatus m = new MsFmStatus();
				m.setFmStatus("1");
				dmDrug.setPmsFmStatus(m);
				
				DmDrugProperty dmDrugProperty = new DmDrugProperty();
				dmDrugProperty.setDisplayname("PARACETAMOL");
				dmDrug.setDmDrugProperty(dmDrugProperty);
				
				DmMoeProperty dmMoeProperty = new DmMoeProperty(); 
				dmMoeProperty.setMoDosageUnit("MG");
				dmDrug.setDmMoeProperty(dmMoeProperty);
				
				Contexts.getSessionContext().set("dmDrug", dmDrug);
				
				Contexts.getSessionContext().set("moDosage", moDosage);
				Contexts.getSessionContext().set("itemCode", itemCode);
				
				invokeMethod("#{dosageConversionListService.retrievePmsDosageConversionByMoDosage(dmDrug, moDosage)}");
				PmsDosageConversionSet pmsDosageConversionSet = (PmsDosageConversionSet)getValue("#{pmsDosageConversionSet}");
				Contexts.getSessionContext().set("pmsDosageConversionSet", pmsDosageConversionSet);
			
				invokeMethod("#{dosageConversionListService.updateDosageConversion()}");
			}
		}.run();
		
		//update pmsDosageConversionSet
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				PmsDosageConversionSet pmsDosageConversionSet = new PmsDosageConversionSet();
				pmsDosageConversionSet.setDispHospCode("QMH");
				pmsDosageConversionSet.setDispWorkstore("WKS1");
				pmsDosageConversionSet.setDrugKey(2922);
				pmsDosageConversionSet.setMoDosage(777.0);
				pmsDosageConversionSet.setMoDosageUnit("MG");
				
				List<PmsDosageConversion> pmsDosageConversionList = new ArrayList<PmsDosageConversion>();
				PmsDosageConversion pmsDosageConversion = new PmsDosageConversion();
				pmsDosageConversion.setItemCode("PARA01");
				pmsDosageConversion.setDispenseDosage(77.0);
				pmsDosageConversion.setDispenseDosageUnit("MG");
				pmsDosageConversion.setMarkDelete(false);
				pmsDosageConversionList.add(pmsDosageConversion);
				
				Contexts.getSessionContext().set("pmsDosageConversionSet", pmsDosageConversionSet);
				Contexts.getSessionContext().set("pmsDosageConversionList", pmsDosageConversionList);
				
				invokeMethod("#{dosageConversionListService.updateDosageConversion()}");
			}
		}.run();		
	
		//retrieve pmsDosageConversionSetList
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				String itemCode = "PARA01";
				Contexts.getSessionContext().set("itemCode", itemCode);
				
				invokeMethod("#{dmDrugService.retrieveDmDrug(itemCode)}");
				DmDrug dmDrug = (DmDrug)getValue("#{dmDrug}");
				dmDrug.setDrugKey(0010);
				MsFmStatus m = new MsFmStatus();
				m.setFmStatus("1");
				dmDrug.setPmsFmStatus(m);
				Contexts.getSessionContext().set("dmDrug", dmDrug);
				
				invokeMethod("#{dosageConversionListService.retrievePmsDosageConversionSetList(dmDrug)}");
				List<PmsDosageConversionSet> pmsDosageConversionSetList = (List<PmsDosageConversionSet>)getValue("#{pmsDosageConversionSetList}");
				
				assert !pmsDosageConversionSetList.isEmpty();
			}
		}.run();	
	}
	
}
