package hk.org.ha.model.pms.report.test;

import hk.org.ha.model.pms.biz.report.EdsPerfRptServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.vo.report.EdsPerfRpt;

import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.joda.time.DateTime;
import org.testng.annotations.Test;

public class EdsPerfRptTest extends SeamTest {
	
	@Test
	public void testEdsPerfRptComponent() throws Exception {
	
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();

		// retrieve suspendCount 
		new ComponentTest() {
			protected void testComponents() throws Exception {  
				DateTime today = new DateTime().withDate(2011, 8, 2);
				EdsPerfRptServiceLocal edsPerfRptService = (EdsPerfRptServiceLocal) getValue("#{edsPerfRptService}");
				List<EdsPerfRpt> edsPerfRptList = edsPerfRptService.retrieveEdsPerfRptList(today.toDate());
	
				assert edsPerfRptList.get(0).getEdsPerfRptDtl().getCheckCount() > 0;
				assert edsPerfRptList.get(0).getEdsPerfRptDtl().getSuspendCount() > 0;
			}
		}.run();
		
	}	
}
