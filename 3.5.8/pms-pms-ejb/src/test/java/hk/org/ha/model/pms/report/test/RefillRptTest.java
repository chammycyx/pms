package hk.org.ha.model.pms.report.test;

import hk.org.ha.model.pms.biz.report.RefillRptServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.udt.report.RefillItemRptStatus;
import hk.org.ha.model.pms.vo.report.PharmClinicRefillCouponRpt;
import hk.org.ha.model.pms.vo.report.RefillExpiryRpt;
import hk.org.ha.model.pms.vo.report.RefillItemRpt;
import hk.org.ha.model.pms.vo.report.RefillQtyForecastRpt;
import hk.org.ha.model.pms.vo.report.RefillStatRpt;
import hk.org.ha.model.pms.vo.report.RefillWorkloadForecastRpt;

import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.joda.time.DateTime;
import org.testng.annotations.Test;

public class RefillRptTest extends SeamTest {
	
	@Test
	public void testRefillRptComponent() throws Exception {
	
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();

		// retrieve pharm clinic refill coupon enquiry
		new ComponentTest() {
			protected void testComponents() throws Exception {  
				DateTime dateFrom = new DateTime().withDate(2011, 6, 1);
				DateTime dateTo = new DateTime().withDate(2011, 8, 7);
				RefillRptServiceLocal refillRptService = (RefillRptServiceLocal) getValue("#{refillRptService}");
				List<PharmClinicRefillCouponRpt> pharmClinicRefillCouponRptList = refillRptService.retrievePharmClinicRefillCouponRpt("WKS1", dateFrom.toDate(), dateTo.toDate());
				
				assert pharmClinicRefillCouponRptList.get(0).getPharmClinicRefillCouponRptDtlList().size()>0;

			}
		}.run();
		
		// retrieve refill Qty Forecast Rpt
		new ComponentTest() {
			protected void testComponents() throws Exception {  
				DateTime dateFrom = new DateTime().withDate(2011, 3, 1);
				DateTime dateTo = new DateTime().withDate(2011, 7, 7);
				RefillRptServiceLocal refillRptService = (RefillRptServiceLocal) getValue("#{refillRptService}");
				List<RefillQtyForecastRpt> refillQtyForecastRptList = refillRptService.retrieveRefillQtyForecastRptList("WKS1", dateFrom.toDate(), dateTo.toDate(), "PARA01");
				
				assert refillQtyForecastRptList.size()>0;
			}
		}.run();
		
		// retrieve refill workload Forecast Rpt
		new ComponentTest() {
			protected void testComponents() throws Exception {  
				DateTime dateFrom = new DateTime().withDate(2011, 3, 1);
				DateTime dateTo = new DateTime().withDate(2011, 7, 7);
				RefillRptServiceLocal refillRptService = (RefillRptServiceLocal) getValue("#{refillRptService}");
				List<RefillWorkloadForecastRpt> refillWorkloadForecastRptList = refillRptService.retrieveRefillWorkloadRptList("WKS1", dateFrom.toDate(), dateTo.toDate());
				
				assert refillWorkloadForecastRptList.size()>0;
			}
		}.run();
		
		// retrieve refill item Rpt
		new ComponentTest() {
			protected void testComponents() throws Exception {  
				DateTime dateFrom = new DateTime().withDate(2011, 7, 6);
				DateTime dateTo = new DateTime().withDate(2011, 7, 6);
				RefillRptServiceLocal refillRptService = (RefillRptServiceLocal) getValue("#{refillRptService}");
				List<RefillItemRpt> refillItemRptList = refillRptService.retrieveRefillItemRptList("WKS1", dateFrom.toDate(), dateTo.toDate(), "PARA01", RefillItemRptStatus.All, false);
				
				assert refillItemRptList.size()>0;
			}
		}.run();
		
		// retrieve refill date
		new ComponentTest() {
			protected void testComponents() throws Exception {  
				DateTime dateFrom = new DateTime().withDate(2011, 7, 1);
				DateTime dateTo = new DateTime().withDate(2011, 7, 31);
				RefillRptServiceLocal refillRptService = (RefillRptServiceLocal) getValue("#{refillRptService}");
				List<RefillStatRpt> refillStatRptList = refillRptService.retrieveRefillStatRptList("WKS1", dateFrom.toDate(), dateTo.toDate());
				
				assert refillStatRptList.size()>0;
			}
		}.run();
		
		// retrieve RefillExpiryRpt 
		new ComponentTest() {
			protected void testComponents() throws Exception {  
				DateTime dateFrom = new DateTime().withDate(2011, 8, 1);
				DateTime dateTo = new DateTime().withDate(2011, 8, 31);
				RefillRptServiceLocal refillRptService = (RefillRptServiceLocal) getValue("#{refillRptService}");
				List<RefillExpiryRpt> refillExpiryRptList = refillRptService.retrieveRefillExpiryRpt("WKS1", dateFrom.toDate(), dateTo.toDate());
				
				assert refillExpiryRptList.size()>0;
			}
		}.run();
	}	
}
