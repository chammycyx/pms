package hk.org.ha.model.pms.security.test;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class LogonTest extends SeamTest
{
   @Test
   public void testLogonComponent() throws Exception
   {
      new ComponentTest() {

         @Override
         protected void testComponents() throws Exception
         {
        	// login
            assert getValue("#{identity.loggedIn}").equals(false);
            setValue("#{identity.username}", "admin");
            setValue("#{identity.password}", "admin");
            invokeMethod("#{identity.login}");
            assert getValue("#{user.name}").equals("Admin User");
            assert getValue("#{user.username}").equals("admin");
            assert getValue("#{user.password}").equals("admin");
            assert getValue("#{identity.loggedIn}").equals(true);
            
            // logout
            invokeMethod("#{identity.logout}");
            assert getValue("#{identity.loggedIn}").equals(false);
            
            // wrong password
            setValue("#{identity.username}", "admin");
            setValue("#{identity.password}", "wrong");
            invokeMethod("#{identity.login}");
            assert getValue("#{identity.loggedIn}").equals(false);
         }
         
      }.run();
   }
}
