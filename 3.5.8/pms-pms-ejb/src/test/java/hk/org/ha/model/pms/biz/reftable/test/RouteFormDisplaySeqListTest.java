package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.reftable.RouteFormDisplaySeqListServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.dms.persistence.PmsRouteSortSpec;
import hk.org.ha.model.pms.udt.patient.PasSpecialtyType;
import hk.org.ha.model.pms.vo.patient.PasSpecialty;

import java.util.List;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class RouteFormDisplaySeqListTest extends SeamTest {

	@Test
	public void testRouteFormDisplaySeqComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		//update
		new ComponentTest() 
		{	
			protected void testComponents() throws Exception 
			{		
				RouteFormDisplaySeqListServiceLocal routeFormDisplaySeqListService = (RouteFormDisplaySeqListServiceLocal) getValue("#{routeFormDisplaySeqListService}");
				routeFormDisplaySeqListService.retrieveRouteFormDisplaySeq("AAA", "O");
				List<PmsRouteSortSpec> pmsRouteSortSpecList = (List<PmsRouteSortSpec>) getValue("#{pmsRouteSortSpecList}");
				Contexts.getSessionContext().set("pmsRouteSortSpecList", pmsRouteSortSpecList);
				invokeMethod("#{routeFormDisplaySeqListService.updateRouteSortSpecList(pmsRouteSortSpecList)}");
			}
		}.run();
		
		// retrieve pas specialty
		new ComponentTest() {		 
			protected void testComponents() throws Exception {   
				RouteFormDisplaySeqListServiceLocal routeFormDisplaySeqListService = (RouteFormDisplaySeqListServiceLocal) getValue("#{routeFormDisplaySeqListService}");
				routeFormDisplaySeqListService.retrieveRouteFormSortSpecList("AUD", PasSpecialtyType.OutPatient);
			}
		}.run();
		
		// retrieve route form display seq list
		new ComponentTest() {		 
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {   
				RouteFormDisplaySeqListServiceLocal routeFormDisplaySeqListService = (RouteFormDisplaySeqListServiceLocal) getValue("#{routeFormDisplaySeqListService}");
				routeFormDisplaySeqListService.retrieveRouteFormDisplaySeq("AAA", "O");
				List<PmsRouteSortSpec> pmsRouteSortSpecList = (List<PmsRouteSortSpec>) getValue("#{pmsRouteSortSpecList}");
				assert pmsRouteSortSpecList.size()>0;
			}
		}.run();
		
		// retrieve default seq list
		new ComponentTest() {		 
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {   
				RouteFormDisplaySeqListServiceLocal routeFormDisplaySeqListService = (RouteFormDisplaySeqListServiceLocal) getValue("#{routeFormDisplaySeqListService}");
				routeFormDisplaySeqListService.retrieveRouteFormDefaultSeqList("AAA", PasSpecialtyType.OutPatient);
				List<PmsRouteSortSpec> pmsRouteSortSpecList = (List<PmsRouteSortSpec>) getValue("#{pmsRouteSortSpecList}");
				assert pmsRouteSortSpecList.size()>0;
			}
		}.run();
		
		// retrieve pas Specialty list
		new ComponentTest() {		 
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {   
				RouteFormDisplaySeqListServiceLocal routeFormDisplaySeqListService = (RouteFormDisplaySeqListServiceLocal) getValue("#{routeFormDisplaySeqListService}");
				routeFormDisplaySeqListService.retrievePasSpecList("", PasSpecialtyType.Blank);
				List<PasSpecialty> pasSpecialtyList = (List<PasSpecialty>) getValue("#{pasSpecialtyList}");
				assert pasSpecialtyList.size()>0;
			}
		}.run();
		
		// retrieve pas Specialty list with preffix
		new ComponentTest() {		 
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {   
				RouteFormDisplaySeqListServiceLocal routeFormDisplaySeqListService = (RouteFormDisplaySeqListServiceLocal) getValue("#{routeFormDisplaySeqListService}");
				routeFormDisplaySeqListService.retrievePasSpecList("AU", PasSpecialtyType.Blank);
				List<PasSpecialty> pasSpecialtyList = (List<PasSpecialty>) getValue("#{pasSpecialtyList}");
				assert pasSpecialtyList.size()>0;
			}
		}.run();
	}
}
