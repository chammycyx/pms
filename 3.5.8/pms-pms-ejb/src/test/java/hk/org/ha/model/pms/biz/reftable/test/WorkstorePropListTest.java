package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.phs.PatientCat;
import hk.org.ha.model.pms.persistence.reftable.WorkstoreProp;

import java.util.ArrayList;
import java.util.List;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class WorkstorePropListTest extends SeamTest {

	@Test
	public void testWorkstorePropListComponent() throws Exception {
		
		final List<String> nameList = new ArrayList<String>();
		nameList.add("workstore.name");
		nameList.add("workstore.outstandingDispOrder.elapseTime");
		nameList.add("oneStop.patient.patCatCode.required");
		nameList.add("oneStop.patient.patCatCode");
		assert nameList.size() == 4;
		
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		// retrieve workstoreProp list
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {   		
				Contexts.getSessionContext().set("nameList", nameList);
				invokeMethod("#{workstorePropListService.retrieveWorkstorePropList(nameList)}");
				List<WorkstoreProp> workstorePropList = (List<WorkstoreProp>) getValue("#{workstorePropList}");
				assert workstorePropList.size()>0;
				
				int check = 0;
				for (WorkstoreProp workstoreProp: workstorePropList) {
					if(workstoreProp.getProp().getName().equals("workstore.name")){
						check++;
						assert workstoreProp.getValue().equals("Working Store 1");
//						assert workstoreProp.getProp().getDescription().equals("Working store description");
					}
					else if(workstoreProp.getProp().getName().equals("workstore.outstandingDispOrder.elapseTime")){
						check++;
						assert workstoreProp.getValue().equals("5");
//						assert workstoreProp.getProp().getDescription().equals("Record response time threshold in minutes");
					}
					else if(workstoreProp.getProp().getName().equals("oneStop.patient.patCatCode.required")){
						check++;
						assert workstoreProp.getValue().equals("Y");
//						assert workstoreProp.getProp().getDescription().equals("The indication to control whether the patient category are mandatory in One Stop Entry");
					}
					else if(workstoreProp.getProp().getName().equals("oneStop.patient.patCatCode")){
						check++;
						assert workstoreProp.getValue().equals("OT");
					}
				}
				assert check == 4;
			}
		}.run();
		
		// edit workstoreProp list
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {   		
				Contexts.getSessionContext().set("nameList", nameList);
				invokeMethod("#{workstorePropListService.retrieveWorkstorePropList(nameList)}");
				List<WorkstoreProp> workstorePropList = (List<WorkstoreProp>) getValue("#{workstorePropList}");
				assert workstorePropList.size()==4;
				for (WorkstoreProp workstoreProp: workstorePropList) {
					if(workstoreProp.getProp().getName().equals("workstore.name")){
						workstoreProp.setValue("Working Store 2");
					}
					else if(workstoreProp.getProp().getName().equals("workstore.outstandingDispOrder.elapseTime")){
						workstoreProp.setValue("6");
					}
					else if(workstoreProp.getProp().getName().equals("oneStop.patient.patCatCode.required")){
						workstoreProp.setValue("N");
					}
					else if(workstoreProp.getProp().getName().equals("oneStop.patient.patCatCode")){
						workstoreProp.setValue("");
					}
				}
				
				Contexts.getSessionContext().set("workstorePropList", workstorePropList);
				invokeMethod("#{workstorePropListService.updateWorkstorePropList(workstorePropList)}");
			}
		}.run();
		
		// check workstoreProp list
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {   		
				Contexts.getSessionContext().set("nameList", nameList);
				invokeMethod("#{workstorePropListService.retrieveWorkstorePropList(nameList)}");
				List<WorkstoreProp> workstorePropList = (List<WorkstoreProp>) getValue("#{workstorePropList}");
				assert workstorePropList.size()>0;
				
				int check = 0;
				for (WorkstoreProp workstoreProp: workstorePropList) {
					if(workstoreProp.getProp().getName().equals("workstore.name")){
						check++;
						assert workstoreProp.getValue().equals("Working Store 2");
					}
					else if(workstoreProp.getProp().getName().equals("workstore.outstandingDispOrder.elapseTime")){
						check++;
						assert workstoreProp.getValue().equals("6");
					}
					else if(workstoreProp.getProp().getName().equals("oneStop.patient.patCatCode.required")){
						check++;
						assert workstoreProp.getValue().equals("N");
					}
					else if(workstoreProp.getProp().getName().equals("oneStop.patient.patCatCode")){
						check++;
						assert workstoreProp.getValue()==null;
					}
				}
				assert check == 4;
			}
		}.run();
		
		// retrieve patientCat list
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {   		
				invokeMethod("#{workstorePropListService.retrievePatientCatList()}");
				List<PatientCat> patientCatList = (List<PatientCat>) getValue("#{patientCatList}");
				assert patientCatList.size()>0;
			}
		}.run();
	}
}
