package hk.org.ha.model.pms.report.test;

import java.util.List;

import hk.org.ha.model.pms.biz.report.CapdVoucherRptServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.vo.report.CapdVoucherRpt;

import org.jboss.seam.mock.SeamTest;
import org.joda.time.DateTime;
import org.testng.annotations.Test;

public class CapdVoucherRptTest extends SeamTest {
	
	@Test
	public void testCapdVoucherRptComponent() throws Exception {
	
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();

		// retrieve list by hkid
		new ComponentTest() {
			protected void testComponents() throws Exception {  
				DateTime dateFrom = new DateTime().withDate(2011, 7, 1);
				DateTime dateTo = new DateTime().withDate(2011, 7, 21);
				
				CapdVoucherRptServiceLocal capdVoucherRptService = (CapdVoucherRptServiceLocal) getValue("#{capdVoucherRptService}");
				List<CapdVoucherRpt> capdVoucherRptList = capdVoucherRptService.retrieveCapdVoucherRptList(dateFrom.toDate(), dateTo.toDate(), "A1234563", "", "");
				
				assert capdVoucherRptList.size()>0;
			}
		}.run();
		
		// retrieve list by patName
		new ComponentTest() {
			protected void testComponents() throws Exception {  
				DateTime dateFrom = new DateTime().withDate(2011, 7, 1);
				DateTime dateTo = new DateTime().withDate(2011, 7, 21);
				
				CapdVoucherRptServiceLocal capdVoucherRptService = (CapdVoucherRptServiceLocal) getValue("#{capdVoucherRptService}");
				List<CapdVoucherRpt> capdVoucherRptList = capdVoucherRptService.retrieveCapdVoucherRptList(dateFrom.toDate(), dateTo.toDate(), "", "PATIENT, SAMPLE1", "");
				
				assert capdVoucherRptList.size()>0;
			}
		}.run();
		
		// retrieve list by patName
		new ComponentTest() {
			protected void testComponents() throws Exception {  
				DateTime dateFrom = new DateTime().withDate(2011, 7, 1);
				DateTime dateTo = new DateTime().withDate(2011, 7, 21);
				
				CapdVoucherRptServiceLocal capdVoucherRptService = (CapdVoucherRptServiceLocal) getValue("#{capdVoucherRptService}");
				List<CapdVoucherRpt> capdVoucherRptList = capdVoucherRptService.retrieveCapdVoucherRptList(dateFrom.toDate(), dateTo.toDate(), "", "", "QMH07000092");
				
				assert capdVoucherRptList.size()>0;
			}
		}.run();
		
	}	
}
