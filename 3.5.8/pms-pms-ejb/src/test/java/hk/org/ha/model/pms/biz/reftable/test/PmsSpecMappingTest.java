package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.dms.persistence.PmsSpecMapping;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroupMapping;
import hk.org.ha.model.pms.persistence.phs.Specialty;
import hk.org.ha.model.pms.persistence.phs.Ward;

import java.util.ArrayList;
import java.util.List;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class PmsSpecMappingTest extends SeamTest {

	@Test
	public void testPmsSpecMappingListComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		//retrieve PmsSpecMappingList
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{pmsSpecMappingListService.retrievePmsSpecMappingList()}");
				List<Specialty> phsSpecialtyList = (List<Specialty>)getValue("#{phsSpecialtyList}");
				List<Ward> wardList = (List<Ward>)getValue("#{wardList}");
				List<WorkstoreGroupMapping> workstoreGroupMappingList = (List<WorkstoreGroupMapping>)getValue("#{workstoreGroupMappingList}");
				
				assert phsSpecialtyList.size()>0;
				assert wardList.size()>0;
				assert workstoreGroupMappingList.size()>0;
			}
		}.run();

		//update PmsSpecMappingList
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				List<PmsSpecMapping> pmsSpecMappingList = new ArrayList<PmsSpecMapping>();
				
				Contexts.getSessionContext().set("updatePmsSpecMappingList", pmsSpecMappingList);
				invokeMethod("#{pmsSpecMappingListService.updatePmsSpecMappingList(updatePmsSpecMappingList)}");
			}
		}.run();
	}	
	
	@Test
	public void testPmsSpecMappingComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();

		//delete PmsSpecMappingList
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				PmsSpecMapping delPmsSpecMapping = new PmsSpecMapping();
				delPmsSpecMapping.setRecordId(1);
				Contexts.getSessionContext().set("delPmsSpecMapping", delPmsSpecMapping);
				invokeMethod("#{pmsSpecMappingService.deletePmsSpecMapping(delPmsSpecMapping)}");
			}
		}.run();
	}	
}
