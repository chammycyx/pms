package hk.org.ha.model.pms.report.test;

import hk.org.ha.model.pms.biz.report.EdsStatusDetailRptServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.vo.report.EdsStatusDetailRpt;
import hk.org.ha.model.pms.vo.report.SuspendPrescRpt;

import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class EdsStatusDetailRptTest extends SeamTest {
	private int prescStatusDtlListSize = 0;
	private int suspendItemListSize = 0;
	@Test
	public void testEdsStatusDetailRptComponent() throws Exception {
	
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();

		// retrieve List
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				
				EdsStatusDetailRptServiceLocal edsStatusDetailRptService = (EdsStatusDetailRptServiceLocal) getValue("#{edsStatusDetailRptService}");
				edsStatusDetailRptService.retrieveEdsStatusDetailRptList(-1);
				
				List<EdsStatusDetailRpt> edsStatusDetailRptPrescStatusDtlList = (List<EdsStatusDetailRpt>) getValue("#{edsStatusDetailRptPrescStatusDtlList}");
				List<EdsStatusDetailRpt> edsStatusDetailRptSuspendItemList = (List<EdsStatusDetailRpt>) getValue("#{edsStatusDetailRptSuspendItemList}");
				
				assert edsStatusDetailRptPrescStatusDtlList.size()>0;
				assert edsStatusDetailRptSuspendItemList.size()>0;
				
				prescStatusDtlListSize = edsStatusDetailRptPrescStatusDtlList.size();
				suspendItemListSize = edsStatusDetailRptSuspendItemList.size();
			}
		}.run();
		
		// retrieve List by DayRange
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				
				EdsStatusDetailRptServiceLocal edsStatusDetailRptService = (EdsStatusDetailRptServiceLocal) getValue("#{edsStatusDetailRptService}");
				edsStatusDetailRptService.retrieveEdsStatusDetailRptList(30);
				
				List<EdsStatusDetailRpt> edsStatusDetailRptPrescStatusDtlList = (List<EdsStatusDetailRpt>) getValue("#{edsStatusDetailRptPrescStatusDtlList}");
				List<EdsStatusDetailRpt> edsStatusDetailRptSuspendItemList = (List<EdsStatusDetailRpt>) getValue("#{edsStatusDetailRptSuspendItemList}");
				
				assert edsStatusDetailRptPrescStatusDtlList.size() == prescStatusDtlListSize;
				assert edsStatusDetailRptSuspendItemList.size() < suspendItemListSize;
			}
		}.run();
		
		// retrieve SuspendPrescRptList
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				
				EdsStatusDetailRptServiceLocal edsStatusDetailRptService = (EdsStatusDetailRptServiceLocal) getValue("#{edsStatusDetailRptService}");
				edsStatusDetailRptService.retrieveEdsStatusDetailRptList(-1);

				List<EdsStatusDetailRpt> edsStatusDetailRptSuspendItemList = (List<EdsStatusDetailRpt>) getValue("#{edsStatusDetailRptSuspendItemList}");
				suspendItemListSize = edsStatusDetailRptSuspendItemList.size();
				
				List<SuspendPrescRpt> suspendPrescRptList = edsStatusDetailRptService.retrieveSuspendPrescRptList(edsStatusDetailRptSuspendItemList);

				assert suspendPrescRptList.size() == suspendItemListSize;
			}
		}.run();
	}	
}
