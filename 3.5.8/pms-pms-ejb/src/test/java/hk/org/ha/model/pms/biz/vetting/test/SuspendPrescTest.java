package hk.org.ha.model.pms.biz.vetting.test;

import hk.org.ha.model.pms.biz.order.DispOrderManagerLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class SuspendPrescTest extends SeamTest {
	
	@Test
	public void testSuspendPrescComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		// Suspend Prescription
		new ComponentTest() {
			
			protected void testComponents() throws Exception {
				invokeMethod("#{suspendPrescService.verifySuspendPrescription(1, 0, 'OT')}");
			}
		}.run();
		
		// Check pharm order
		new ComponentTest() {
			
			protected void testComponents() throws Exception {
				
				DispOrderManagerLocal dispOrderManager= (DispOrderManagerLocal) getValue("#{dispOrderManager}");
				DispOrder dispOrder = dispOrderManager.retrieveDispOrderById(1L);

				assert dispOrder != null ;

				assert dispOrder.getAdminStatus().equals(DispOrderAdminStatus.Suspended);
				assert StringUtils.equals(dispOrder.getSuspendCode(), "OT");
			}
		}.run();
	}	
}
