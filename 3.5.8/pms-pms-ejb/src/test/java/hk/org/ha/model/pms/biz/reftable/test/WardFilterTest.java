package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.WardFilter;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class WardFilterTest extends SeamTest {

	
	@Test
	public void testWardFilterComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		//Add wardFilter
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				
				invokeMethod("#{wardFilterService.retrieveWardFilterList}");
				List<WardFilter> wardFilterList = (List<WardFilter>) getValue("#{wardFilterList}");
				assert wardFilterList.size() == 0;

				List<WardFilter> newWardFilterList = new ArrayList<WardFilter>();
				
				WardFilter wardFilter = new WardFilter();
				wardFilter.setWorkstore( (Workstore)getValue("#{workstore}") );
				wardFilter.setMonFlag(true);
				wardFilter.setTueFlag(false);
				wardFilter.setWedFlag(false);
				wardFilter.setThuFlag(false);
				wardFilter.setFriFlag(false);
				wardFilter.setSatFlag(true);
				wardFilter.setSunFlag(false);
				wardFilter.setStartTime("1000");
				wardFilter.setEndTime("2000");
				wardFilter.setCreateUser("admin");
				wardFilter.setCreateDate( new Date() );
				wardFilter.setWardCode("5D");
				newWardFilterList.add(wardFilter);
					
				Contexts.getSessionContext().set("newWardFilterList", newWardFilterList);
				invokeMethod("#{wardFilterService.updateWardFilterList( newWardFilterList )}");
			}
		
		}.run();
		
		//Check wardFilter
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception
			{	
				//retrieveWardFilterList
				invokeMethod("#{wardFilterService.retrieveWardFilterList}");
				List<WardFilter> wardFilterList = (List<WardFilter>) getValue("#{wardFilterList}");
				assert wardFilterList.size()>0;
				
				WardFilter wardFilter = wardFilterList.get(0);
				assert wardFilter.getMonFlag() == true;
				assert wardFilter.getSatFlag() == true;
				assert wardFilter.getCreateUser().equals("admin");
				assert wardFilter.getStartTime().equals("1000");
				assert wardFilter.getEndTime().equals("2000");
				assert wardFilter.getWardCode().equals("5D");
			}
		}.run();	
		
		//Update Ward Filter
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				invokeMethod("#{wardFilterService.retrieveWardFilterList}");
				List<WardFilter> wardFilterList = (List<WardFilter>) getValue("#{wardFilterList}");
				
				WardFilter wardFilter = wardFilterList.get(0);
				wardFilter.setMonFlag(false);
				wardFilter.setTueFlag(false);
				wardFilter.setWedFlag(false);
				wardFilter.setThuFlag(false);
				wardFilter.setFriFlag(false);
				wardFilter.setSatFlag(false);
				wardFilter.setSunFlag(true);
				wardFilter.setStartTime("1200");
				wardFilter.setEndTime("2200");
				wardFilter.setWardCode("A001");
					
				Contexts.getSessionContext().set("wardFilterList", wardFilterList);
				invokeMethod("#{wardFilterService.updateWardFilterList( wardFilterList )}");
			}
		}.run();
	
		//Check wardFilter
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception
			{	
				//retrieveWardFilterList
				invokeMethod("#{wardFilterService.retrieveWardFilterList}");
				List<WardFilter> wardFilterList = (List<WardFilter>) getValue("#{wardFilterList}");
				assert wardFilterList.size()>0;
				
				WardFilter wardFilter = wardFilterList.get(0);
				assert wardFilter.getMonFlag() == false;
				assert wardFilter.getSatFlag() == false;
				assert wardFilter.getSunFlag() == true;
				assert wardFilter.getStartTime().equals("1200");
				assert wardFilter.getEndTime().equals("2200");
				assert wardFilter.getWardCode().equals("A001");
			}
		}.run();
		
		//Delete wardFilter
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				invokeMethod("#{wardFilterService.retrieveWardFilterList}");
				List<WardFilter> wardFilterList = (List<WardFilter>) getValue("#{wardFilterList}");
				
				wardFilterList.remove( wardFilterList.get(0) );
				
				Contexts.getSessionContext().set("wardFilterList", wardFilterList);
				invokeMethod("#{wardFilterService.updateWardFilterList( wardFilterList )}");
			}
		}.run();
		
		//Check wardFilter
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception
			{	
				//retrieveWardFilterList
				invokeMethod("#{wardFilterService.retrieveWardFilterList}");
				List<WardFilter> wardFilterList = (List<WardFilter>) getValue("#{wardFilterList}");
				assert wardFilterList.size() == 0;
			}
		}.run();
		
	}	
}
