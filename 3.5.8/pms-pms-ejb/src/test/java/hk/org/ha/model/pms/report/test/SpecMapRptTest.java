package hk.org.ha.model.pms.report.test;

import hk.org.ha.model.pms.biz.report.SpecMapRptServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.dms.vo.pms.PmsSpecMapReportCriteria;
import hk.org.ha.model.pms.udt.report.ReportFilterOption;

import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class SpecMapRptTest extends SeamTest {
	
	@Test
	public void testSpecMapRptComponent() throws Exception {
	
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();

		// retrieve Specialty Mapping Report drop down lists
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				
				PmsSpecMapReportCriteria pmsSpecMapReportCriteria = new PmsSpecMapReportCriteria();
								
				SpecMapRptServiceLocal specMapRptService = (SpecMapRptServiceLocal) getValue("#{specMapRptService}");
				specMapRptService.retrieveSpecMapRpt(pmsSpecMapReportCriteria);
				
				List<String> patHospCodeStrList = (List<String>) getValue("#{patHospCodeStrList}");
				assert patHospCodeStrList.get(0).equals(ReportFilterOption.All.getDataValue());
				assert patHospCodeStrList.get(1).equals("PMH");
				
				List<String> pasSpecialtyStrList = (List<String>) getValue("#{pasSpecialtyStrList}");
				assert pasSpecialtyStrList.get(0).equals(ReportFilterOption.All.getDataValue());
				assert pasSpecialtyStrList.get(1).equals("SRG");
				
				List<String> pasSubSpecialtyStrList = (List<String>) getValue("#{pasSubSpecialtyStrList}");
				assert pasSubSpecialtyStrList.get(0).equals(ReportFilterOption.All.getDataValue());
				assert pasSubSpecialtyStrList.get(1).equals(ReportFilterOption.Blank.getDataValue());
				assert pasSubSpecialtyStrList.get(2).equals("JE");
				
				List<String> wardStrList = (List<String>) getValue("#{wardStrList}");
				assert wardStrList.get(0).equals(ReportFilterOption.All.getDataValue());
				assert wardStrList.get(1).equals(ReportFilterOption.Blank.getDataValue());
				assert wardStrList.get(2).equals("J9");
				
				List<String> dispHospCodeStrList = (List<String>) getValue("#{dispHospCodeStrList}");
				assert dispHospCodeStrList.get(0).equals(ReportFilterOption.All.getDataValue());
				assert dispHospCodeStrList.get(1).equals("QMH");
				
				List<String> phsSpecialtyStrList = (List<String>) getValue("#{phsSpecialtyStrList}");
				assert phsSpecialtyStrList.get(0).equals(ReportFilterOption.All.getDataValue());
				assert phsSpecialtyStrList.get(1).equals("HN");
				
				List<String> phsWardStrList = (List<String>) getValue("#{phsWardStrList}");
				assert phsWardStrList.get(0).equals(ReportFilterOption.All.getDataValue());
				assert phsWardStrList.get(1).equals(ReportFilterOption.Blank.getDataValue());
				assert phsWardStrList.get(2).equals("A3");
				
				List<String> dispWorkstoreStrList = (List<String>) getValue("#{dispWorkstoreStrList}");
				assert dispWorkstoreStrList.get(0).equals(ReportFilterOption.All.getDataValue());
				assert dispWorkstoreStrList.get(1).equals("WKS1");
			}
		}.run();
		
	}	
}
