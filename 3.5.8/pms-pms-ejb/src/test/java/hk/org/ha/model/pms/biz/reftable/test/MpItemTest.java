package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
import hk.org.ha.model.pms.persistence.reftable.FdnMapping;
import hk.org.ha.model.pms.persistence.reftable.ItemDispConfig;
import hk.org.ha.model.pms.persistence.reftable.ItemSpecialty;
import hk.org.ha.model.pms.persistence.reftable.PendingReason;
import hk.org.ha.model.pms.udt.reftable.FdnType;
import hk.org.ha.model.pms.udt.vetting.OrderType;

import java.util.ArrayList;
import java.util.List;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class MpItemTest extends SeamTest {

	
	@Test
	public void testMpItemComponent() throws Exception {
		
		//Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		
		//retrieve MpItem
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				invokeMethod("#{mpItemService.retrieveMpItem( 'INSU02' )}");
				List<PendingReason> fdnMappingList = (List<PendingReason>) getValue("#{fdnMappingList}");
				assert fdnMappingList.size()>0;
				List<ItemSpecialty> itemSpecialtyList = (List<ItemSpecialty>) getValue("#{itemSpecialtyList}");
				assert itemSpecialtyList.size()>0;
				ItemDispConfig itemDispConfig = (ItemDispConfig) getValue("#{itemDispConfig}");
				assert itemDispConfig != null;
			}
		}.run();
		
		//add MpItem
		new ComponentTest() {
			protected void testComponents() throws Exception {
				
				ItemDispConfig newItemDispConfig = new ItemDispConfig();
				newItemDispConfig.setItemCode("AMIK03");
				newItemDispConfig.setDefaultDispDays(5);
				newItemDispConfig.setDailyRefillFlag(false);
				newItemDispConfig.setSingleDispFlag(true);
				newItemDispConfig.setWarningMessage("Test warning message");
				
				List<FdnMapping> newFdnMappingList = new ArrayList<FdnMapping>();
				
				Workstore workstore = new Workstore("QMH" , "WKS");
				FdnMapping fdnMapping = new FdnMapping();
				fdnMapping.setWorkstore(workstore);
				fdnMapping.setItemCode("AMIK03");
				fdnMapping.setOrderType(OrderType.InPatient);
				fdnMapping.setType(FdnType.FloatDrugName);
				fdnMapping.setFloatDrugName("Test FDN");
				newFdnMappingList.add(fdnMapping);
				
				List<ItemSpecialty> newItemSpecialtyList = new ArrayList<ItemSpecialty>();
				ItemSpecialty itemSpecialty = new ItemSpecialty();
				itemSpecialty.setSpecCode("PAE");
				itemSpecialty.setWorkstoreGroup(new WorkstoreGroup("QMH"));
				itemSpecialty.setItemCode("AMIK03");
				newItemSpecialtyList.add(itemSpecialty);				
				
				Contexts.getSessionContext().set("itemDispConfig", newItemDispConfig);
				Contexts.getSessionContext().set("fdnMappingList", newFdnMappingList);
				Contexts.getSessionContext().set("itemSpecialtyList", newItemSpecialtyList);
				
				invokeMethod("#{mpItemService.updateMpItem( fdnMappingList, itemSpecialtyList, itemDispConfig, 'AMIK03' )}");
			}
		
		}.run();
		
		//check MpItem
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception
			{	
				//retrieveMpItem
				ItemDispConfig itemDispConfig = (ItemDispConfig) getValue("#{itemDispConfig}");
				List<FdnMapping> fdnMappingList = (List<FdnMapping>) getValue("#{fdnMappingList}");
				List<ItemSpecialty> itemSpecialtyList = (List<ItemSpecialty>) getValue("#{itemSpecialtyList}");
				assert itemDispConfig != null;
				assert fdnMappingList.size()>0;
				assert itemSpecialtyList.size()>0;
				
				if (itemDispConfig.getItemCode().equals("AMIK03")){
					assert itemDispConfig.getDefaultDispDays() == 5;
					assert itemDispConfig.getDailyRefillFlag() == false;
					assert itemDispConfig.getSingleDispFlag() == true;
					assert itemDispConfig.getWarningMessage().equals("Test warning message");
					assert itemDispConfig.getWorkstoreGroup().getWorkstoreGroupCode().equals("QMH");
				}
				
				for(FdnMapping fdnMapping:fdnMappingList){
					if(fdnMapping.getItemCode().equals("AMIK03")){
						if(fdnMapping.getWorkstore().getWorkstoreCode().equals("WKS")){
							assert fdnMapping.getOrderType().equals(OrderType.InPatient);
							assert fdnMapping.getType().equals(FdnType.FloatDrugName);
							assert fdnMapping.getWorkstore().getWorkstoreGroup().getWorkstoreGroupCode().equals("QMH");
//							assert fdnMapping.getFloatDrugName().equals("Test FDN");
						}
					}
				}

				for(ItemSpecialty itemSpecialty:itemSpecialtyList){
					if(itemSpecialty.getItemCode().equals("AMIK03")){
						assert itemSpecialty.getSpecCode().equals("PAE");
						assert itemSpecialty.getWorkstoreGroup().getWorkstoreGroupCode().equals("QMH");
					}
				}
			}
		}.run();	
		
		//update MpItem
		new ComponentTest() {
			protected void testComponents() throws Exception {
				
				invokeMethod("#{mpItemService.retrieveMpItem( 'AMIK03' )}");
				ItemDispConfig itemDispConfig = (ItemDispConfig) getValue("#{itemDispConfig}");
				List<FdnMapping> fdnMappingList = (List<FdnMapping>) getValue("#{fdnMappingList}");
				List<ItemSpecialty> itemSpecialtyList = (List<ItemSpecialty>) getValue("#{itemSpecialtyList}");
				
				itemDispConfig.setItemCode("AMIK03");
				itemDispConfig.setDefaultDispDays(1);
				itemDispConfig.setDailyRefillFlag(true);
				itemDispConfig.setSingleDispFlag(false);
				itemDispConfig.setWarningMessage("Test warning message2");
				
				FdnMapping fdnMapping = fdnMappingList.get(0);
				fdnMapping.setFloatDrugName("Test FDN2");
				
				ItemSpecialty itemSpecialty = itemSpecialtyList.get(0);
				itemSpecialty.setSpecCode("MEDC");
				Contexts.getSessionContext().set("itemDispConfig", itemDispConfig);
				Contexts.getSessionContext().set("fdnMappingList", fdnMappingList);
				Contexts.getSessionContext().set("itemSpecialtyList", itemSpecialtyList);
				
				invokeMethod("#{mpItemService.updateMpItem( fdnMappingList, itemSpecialtyList, itemDispConfig, 'AMIK03' )}");
			}
		
		}.run();
	
		//check MpItem
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception
			{	
				//retrieveMpItem
				invokeMethod("#{mpItemService.retrieveMpItem( 'AMIK03' )}");
				ItemDispConfig itemDispConfig = (ItemDispConfig) getValue("#{itemDispConfig}");
				List<FdnMapping> fdnMappingList = (List<FdnMapping>) getValue("#{fdnMappingList}");
				List<ItemSpecialty> itemSpecialtyList = (List<ItemSpecialty>) getValue("#{itemSpecialtyList}");
				assert itemDispConfig != null;
				assert fdnMappingList.size()>0;
				assert itemSpecialtyList.size()>0;
				
				if (itemDispConfig.getItemCode().equals("AMIK03")){
					assert itemDispConfig.getDefaultDispDays() == 1;
					assert itemDispConfig.getDailyRefillFlag() == true;
					assert itemDispConfig.getSingleDispFlag() == false;
					assert itemDispConfig.getWarningMessage().equals("Test warning message2");
					assert itemDispConfig.getWorkstoreGroup().getWorkstoreGroupCode().equals("QMH");
				}
				
				for(ItemSpecialty itemSpecialty:itemSpecialtyList){
					if(itemSpecialty.getItemCode().equals("AMIK03")){
						assert itemSpecialty.getSpecCode().equals("MEDC");
						assert itemSpecialty.getWorkstoreGroup().getWorkstoreGroupCode().equals("QMH");
					}
				}
			}
		}.run();	
		
	}	
}
