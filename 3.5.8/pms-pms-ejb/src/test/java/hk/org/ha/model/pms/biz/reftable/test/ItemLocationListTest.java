package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.reftable.ItemLocationListServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.reftable.ItemLocation;
import hk.org.ha.model.pms.persistence.reftable.Machine;
import hk.org.ha.model.pms.persistence.reftable.PickStation;
import hk.org.ha.model.pms.udt.reftable.MachineType;
import hk.org.ha.model.pms.vo.reftable.ItemLocationInfo;

import java.util.ArrayList;
import java.util.List;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class ItemLocationListTest extends SeamTest {

	private Machine machine = new Machine();
	@Test
	public void testItemLocationListComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		
		// retrieve ItemLocation list by itemCode
		new ComponentTest() {
			protected void testComponents() throws Exception {
				ItemLocationListServiceLocal itemLocationListService = (ItemLocationListServiceLocal)getValue("#{itemLocationListService}");
				List<ItemLocationInfo> itemLocationInfoList = itemLocationListService.retrieveItemLocationListByItemCode("PARA01");
				assert itemLocationInfoList.size()>0;
				
				for (ItemLocationInfo itemLocationInfo:itemLocationInfoList) {
					assert itemLocationInfo.getItemCode().equals("PARA01");
				}
			}
		}.run();
		
		// retrieve ItemLocation list by pickStationNum
		new ComponentTest() {
			protected void testComponents() throws Exception {
				ItemLocationListServiceLocal itemLocationListService = (ItemLocationListServiceLocal)getValue("#{itemLocationListService}");
				List<ItemLocationInfo> itemLocationInfoList = itemLocationListService.retrieveItemLocationListByPickStation(1);
				assert itemLocationInfoList.size()>0;
				
				for (ItemLocationInfo itemLocationInfo:itemLocationInfoList) {
					assert itemLocationInfo.getPickStationNum() == 1;
				}
			}
		}.run();
		
		// add ItemLocation list
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				invokeMethod("#{itemLocationListService.retrievePickStationList()}");
				List<PickStation> itemLocationPickStationList = (List<PickStation>) getValue("#{itemLocationPickStationList}");
				assert itemLocationPickStationList.size()>0;
				
				List<ItemLocationInfo> updateItemLocationInfoList = new ArrayList<ItemLocationInfo>();
				ItemLocationInfo newItemLocationInfo = new ItemLocationInfo();
				newItemLocationInfo.setItemCode("ACET01");
				newItemLocationInfo.setBinNum("Z012");
				newItemLocationInfo.setBakerCellNum("U231");
				for(PickStation pickStation:itemLocationPickStationList) {
					if(pickStation.getPickStationNum()==1){
						for(Machine pickStationMachine:pickStation.getMachineList()) {
							if(pickStationMachine.getType().equals(MachineType.BakerCell)){
								newItemLocationInfo.setMachine(pickStationMachine);
								machine = pickStationMachine;
								break;
							}
						}
						break;
					}
				}
				newItemLocationInfo.setMaxQty(10);
				updateItemLocationInfoList.add(newItemLocationInfo);
				
				Contexts.getSessionContext().set("updateItemLocationInfoList", updateItemLocationInfoList);
				invokeMethod("#{itemLocationListService.updateItemLocationList(updateItemLocationInfoList)}");
				
				ItemLocationListServiceLocal itemLocationListService = (ItemLocationListServiceLocal) getInstance("itemLocationListService");
				assert itemLocationListService.isSaveSuccess();
			}
		}.run();
		
		
		// check ItemLocation list
		new ComponentTest() {
			protected void testComponents() throws Exception {
				ItemLocationListServiceLocal itemLocationListService = (ItemLocationListServiceLocal)getValue("#{itemLocationListService}");
				List<ItemLocationInfo> itemLocationInfoList = itemLocationListService.retrieveItemLocationListByItemCode("ACET01");
				assert itemLocationInfoList.size()>0;
				
				for (ItemLocationInfo itemLocationInfo:itemLocationInfoList) {
					assert itemLocationInfo.getItemCode().equals("ACET01");
					assert itemLocationInfo.getBakerCellNum().equals("U231");
					assert itemLocationInfo.getBinNum().equals("Z012");
					assert itemLocationInfo.getPickStationNum() == 1;
					assert itemLocationInfo.getMachineType() == MachineType.BakerCell;
					assert itemLocationInfo.getMaxQty()==10;
				}
			}
		}.run();
		
		// delete ItemLocation list
		new ComponentTest() {
			protected void testComponents() throws Exception {
				ItemLocationListServiceLocal itemLocationListService = (ItemLocationListServiceLocal)getValue("#{itemLocationListService}");
				List<ItemLocationInfo> itemLocationInfoList = itemLocationListService.retrieveItemLocationListByItemCode("ACET01");
				assert itemLocationInfoList.size()>0;
				
				List<ItemLocationInfo> updateItemLocationInfoList = new ArrayList<ItemLocationInfo>();
				for (ItemLocationInfo itemLocationInfo:itemLocationInfoList) {
					itemLocationInfo.setMarkDelete(true);
					updateItemLocationInfoList.add(itemLocationInfo);
				}

				Contexts.getSessionContext().set("updateItemLocationInfoList", updateItemLocationInfoList);
				invokeMethod("#{itemLocationListService.updateItemLocationList(updateItemLocationInfoList)}");
			}
		}.run();
		
		
		// check ItemLocation list
		new ComponentTest() {
			protected void testComponents() throws Exception {
				ItemLocationListServiceLocal itemLocationListService = (ItemLocationListServiceLocal)getValue("#{itemLocationListService}");
				List<ItemLocationInfo> itemLocationInfoList = itemLocationListService.retrieveItemLocationListByItemCode("ACET01");
				assert itemLocationInfoList.size()==0;
			}
		}.run();
		
		// add Duplicated Bin No. and check save result
		new ComponentTest() {
			protected void testComponents() throws Exception {
				
				List<ItemLocationInfo> updateItemLocationInfoList = new ArrayList<ItemLocationInfo>();
				ItemLocationInfo newItemLocationInfo = new ItemLocationInfo();
				newItemLocationInfo.setItemCode("ACET01");
				newItemLocationInfo.setBinNum("A001");
				newItemLocationInfo.setBakerCellNum("U231");
				newItemLocationInfo.setMachine(machine);
				newItemLocationInfo.setMaxQty(10);
				updateItemLocationInfoList.add(newItemLocationInfo);
				
				Contexts.getSessionContext().set("updateItemLocationInfoList", updateItemLocationInfoList);
				invokeMethod("#{itemLocationListService.updateItemLocationList(updateItemLocationInfoList)}");

				ItemLocationListServiceLocal itemLocationListService = (ItemLocationListServiceLocal) getInstance("itemLocationListService");
				assert !itemLocationListService.isSaveSuccess();
			}
		}.run();
		
		// delete and Add itemLocation
		new ComponentTest() {
			protected void testComponents() throws Exception {
				ItemLocationListServiceLocal itemLocationListService = (ItemLocationListServiceLocal)getValue("#{itemLocationListService}");
				List<ItemLocationInfo> itemLocationInfoList = itemLocationListService.retrieveItemLocationListByPickStation(1);
				assert itemLocationInfoList.size()>0;
				
				List<ItemLocationInfo> updateItemLocationInfoList = new ArrayList<ItemLocationInfo>();
				for (ItemLocationInfo itemLocationInfo:itemLocationInfoList) {
					if(itemLocationInfo.getBinNum().equals("A007")){
						itemLocationInfo.setMarkDelete(true);
						updateItemLocationInfoList.add(itemLocationInfo);
					}
				}
				
				ItemLocationInfo newItemLocationInfo = new ItemLocationInfo();
				newItemLocationInfo.setItemCode("ACET01");
				newItemLocationInfo.setBinNum("A007");
				newItemLocationInfo.setBakerCellNum("U231");
				newItemLocationInfo.setMachine(machine);
				newItemLocationInfo.setMaxQty(10);
				updateItemLocationInfoList.add(newItemLocationInfo);

				Contexts.getSessionContext().set("updateItemLocationInfoList", updateItemLocationInfoList);
				invokeMethod("#{itemLocationListService.updateItemLocationList(updateItemLocationInfoList)}");

				assert itemLocationListService.isSaveSuccess();
			}
		}.run();

		
		// check ItemLocation list
		new ComponentTest() {
			protected void testComponents() throws Exception {
				ItemLocationListServiceLocal itemLocationListService = (ItemLocationListServiceLocal)getValue("#{itemLocationListService}");
				List<ItemLocationInfo> itemLocationInfoList = itemLocationListService.retrieveItemLocationListByPickStation(1);
				assert itemLocationInfoList.size()>0;
				
				boolean check = false;
				for (ItemLocationInfo itemLocationInfo:itemLocationInfoList) {
					if(itemLocationInfo.getBinNum().equals("A007")){
						assert itemLocationInfo.getItemCode().equals("ACET01");
						assert itemLocationInfo.getBakerCellNum().equals("U231");
						assert itemLocationInfo.getMachineType() == machine.getType();
						assert itemLocationInfo.getPickStationNum() == machine.getPickStation().getPickStationNum();
						assert itemLocationInfo.getMaxQty()==10;
						check = true;
					}
				}
				assert check;
			}
		}.run();
	}	
}
