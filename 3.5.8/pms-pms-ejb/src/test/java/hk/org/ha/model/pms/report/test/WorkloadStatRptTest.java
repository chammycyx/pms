package hk.org.ha.model.pms.report.test;

import hk.org.ha.model.pms.biz.report.WorkloadStatRptServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.reftable.WorkloadStat;
import hk.org.ha.model.pms.vo.report.WorkloadStatRpt;

import java.util.Date;
import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.joda.time.DateTime;
import org.testng.annotations.Test;

public class WorkloadStatRptTest extends SeamTest {
	
	@Test
	public void testWorkloadStatRptComponent() throws Exception {
	
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();

		// retrieve workloadStatList by past 2 days
		new ComponentTest() {
			protected void testComponents() throws Exception {  
				DateTime today = new DateTime().withDate(2011, 7, 7);
				WorkloadStatRptServiceLocal workloadStatRptService = (WorkloadStatRptServiceLocal) getValue("#{workloadStatRptService}");
				List<WorkloadStatRpt> workloadStatRptList = workloadStatRptService.constructWorkloadStatList(2, today.toDate(), null, null);
		
				assert workloadStatRptList.size()==2;
			}
		}.run();
		
		// retrieve workloadStatList By date range
		new ComponentTest() {
			protected void testComponents() throws Exception {   
				DateTime dateFrom = new DateTime().withDate(2011, 7, 5);
				DateTime dateTo = new DateTime().withDate(2011, 7, 7);
				
				WorkloadStatRptServiceLocal workloadStatRptService = (WorkloadStatRptServiceLocal) getValue("#{workloadStatRptService}");
				List<WorkloadStatRpt> workloadStatRptList = workloadStatRptService.constructWorkloadStatList(null, new Date(), dateFrom.toDate(), dateTo.toDate());
				
				assert workloadStatRptList.size()>0;
			}
		}.run();
		
	}	
}
