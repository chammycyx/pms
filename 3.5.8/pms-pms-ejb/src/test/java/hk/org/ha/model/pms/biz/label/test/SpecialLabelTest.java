package hk.org.ha.model.pms.biz.label.test;

import hk.org.ha.model.pms.biz.label.SpecialLabelListServiceLocal;
import hk.org.ha.model.pms.biz.label.SpecialLabelServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.vo.label.SpecialLabel;
import hk.org.ha.model.pms.vo.label.SpecialLabelInstruction;

import java.util.ArrayList;
import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class SpecialLabelTest extends SeamTest {

	@Test
	public void testSpecialLabelComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();

		// retrieve specialLabelList
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				SpecialLabelListServiceLocal specialLabelListService = (SpecialLabelListServiceLocal) getValue("#{specialLabelListService}");
				specialLabelListService.retrieveSpecialLabelList("");
				List<SpecialLabel> specialLabelList = (List<SpecialLabel>) getValue("#{specialLabelList}");
				assert specialLabelList.size()>0;
			}
		}.run();
		
		// update specialLabel
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				SpecialLabelListServiceLocal specialLabelListService = (SpecialLabelListServiceLocal) getValue("#{specialLabelListService}");
				specialLabelListService.retrieveSpecialLabelList("");
				List<SpecialLabel> specialLabelList = (List<SpecialLabel>) getValue("#{specialLabelList}");
				
				Integer count = 0;
				count =	specialLabelList.size() + 1;
				
				SpecialLabel specialLabel = new SpecialLabel();
				specialLabel.setLabelNum("999");
				
				List<SpecialLabelInstruction> specialLabelInstructionList = new ArrayList<SpecialLabelInstruction>();
				SpecialLabelInstruction specialLabelInstruction = new SpecialLabelInstruction();
				specialLabelInstruction.setLine("AAA");
				specialLabelInstructionList.add(specialLabelInstruction);
				
				specialLabel.setSpecialLabelInstructionList(specialLabelInstructionList);
				
				SpecialLabelServiceLocal specialLabelService = (SpecialLabelServiceLocal) getValue("#{specialLabelService}");
				specialLabelService.updateSpecialLabel(specialLabel);

				specialLabelListService.retrieveSpecialLabelList("");
				List<SpecialLabel> updateSpecialLabelList = (List<SpecialLabel>) getValue("#{specialLabelList}");
				assert updateSpecialLabelList.size() == count;
				
			}
		}.run();


	}	
}
