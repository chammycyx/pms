package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.reftable.SuspendReason;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.util.ArrayList;
import java.util.List;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class SuspendReasonListTest extends SeamTest {

	
	@Test
	public void testSuspendReasonListComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		
		// retrieve suspend list
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				invokeMethod("#{suspendReasonListService.retrieveSuspendReasonList}");
				List<SuspendReason> suspendReasonList = (List<SuspendReason>) getValue("#{suspendReasonList}");
				assert suspendReasonList.size()>0;
			}
		}.run();
		
		// add suspendReason
		new ComponentTest() {
			protected void testComponents() throws Exception {

				List<SuspendReason> newSuspendReasonList = new ArrayList<SuspendReason>();
				
				SuspendReason suspendReason = new SuspendReason();
				suspendReason.setSuspendCode("MF");
				suspendReason.setDescription("MISSING FREQUENCY");
				suspendReason.setStatus(RecordStatus.Active);
				
				newSuspendReasonList.add(suspendReason);
					
				Contexts.getSessionContext().set("suspendReasonList", newSuspendReasonList);
				invokeMethod("#{suspendReasonListService.updateSuspendReasonList( suspendReasonList )}");
			}
		
		}.run();
		
		//check suspendReason
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception
			{	
				//retrieveSuspendReasonList
				invokeMethod("#{suspendReasonListService.retrieveSuspendReasonList}");
				List<SuspendReason> suspendReasonList = (List<SuspendReason>) getValue("#{suspendReasonList}");
				assert suspendReasonList.size()>0;
				
				for(SuspendReason suspendReason:suspendReasonList){
					if(suspendReason.getSuspendCode().equals("MF")){
						assert suspendReason.getDescription().equals("MISSING FREQUENCY");
						assert suspendReason.getStatus().equals(RecordStatus.Active);
						assert suspendReason.getHospital().getHospCode().equals("QMH");
					}
				}
			}
		}.run();	
		
		//Delete suspendReason
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception{
				invokeMethod("#{suspendReasonListService.retrieveSuspendReasonList}");
				List<SuspendReason> suspendReasonList = (List<SuspendReason>) getValue("#{suspendReasonList}");
				assert suspendReasonList.size() >0;
				
				List<SuspendReason> newSuspendReasonList = new ArrayList<SuspendReason>();
				for(SuspendReason suspendReason: suspendReasonList){
					if(suspendReason.getSuspendCode().equals("MF")){
						suspendReason.setStatus(RecordStatus.Delete);	
						newSuspendReasonList.add(suspendReason); 
					}
				}
				Contexts.getSessionContext().set("suspendReasonList", newSuspendReasonList);
				invokeMethod("#{suspendReasonListService.updateSuspendReasonList( suspendReasonList )}");
			}
		}.run();
	
		//check suspendReason
		new ComponentTest(){
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception{
				invokeMethod("#{suspendReasonListService.retrieveSuspendReasonList}");
				List<SuspendReason> suspendReasonList = (List<SuspendReason>) getValue ("#{suspendReasonList}");
				assert suspendReasonList.size() >0;
				
				for(SuspendReason suspendReason: suspendReasonList){
					assert !suspendReason.getSuspendCode().equals("MF");
				}
			}
		}.run();
		
		//Add suspendReason with status deleted
		new ComponentTest(){
			protected void testComponents() throws Exception{
				
				SuspendReason suspendReason = new SuspendReason();
				suspendReason.setSuspendCode("MF");
				suspendReason.setDescription("AAA");
				
				List<SuspendReason> newSuspendReasonList = new ArrayList<SuspendReason>();
				newSuspendReasonList.add(suspendReason);
				
				Contexts.getSessionContext().set("suspendReasonList", newSuspendReasonList);
				invokeMethod("#{suspendReasonListService.updateSuspendReasonList( suspendReasonList )}");
			}
		}.run();
		
		//check suspendReason
		new ComponentTest(){
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception{
				invokeMethod("#{suspendReasonListService.retrieveSuspendReasonList}");
				List<SuspendReason> suspendReasonList = (List<SuspendReason>) getValue ("#{suspendReasonList}");
				assert suspendReasonList.size() >0;
				
				for(SuspendReason suspendReason: suspendReasonList){
					if(suspendReason.getSuspendCode().equals("MF")){
						assert suspendReason.getStatus().equals(RecordStatus.Active);
						assert suspendReason.getDescription().equals("AAA");
					}
				}
			}
		}.run();
		
	}	
}
