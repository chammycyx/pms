package hk.org.ha.model.pms.report.test;

import java.util.List;

import hk.org.ha.model.pms.biz.report.EdsWaitTimeRptServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.vo.report.EdsWaitTimeRpt;
import hk.org.ha.model.pms.vo.report.EdsElapseTime;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.mock.SeamTest;
import org.joda.time.DateTime;
import org.testng.annotations.Test;

public class EdsWaitTimeRptTest extends SeamTest {
	
	@Test
	public void testEdsWaitTimeRptComponent() throws Exception {
	
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();

		// retrieve list 
		new ComponentTest() {
			protected void testComponents() throws Exception {  
				DateTime inputDate = new DateTime().withDate(2011, 8, 1).withTime(0, 0, 0, 0);
				
				EdsWaitTimeRptServiceLocal edsWaitTimeRptService = (EdsWaitTimeRptServiceLocal) getValue("#{edsWaitTimeRptService}");
				List<EdsWaitTimeRpt> edsWaitTimeRptList =  edsWaitTimeRptService.retrieveEdsWaitTimeRptList(inputDate.toDate());
				
				assert edsWaitTimeRptList.size() >0;

				for (EdsElapseTime edsWaitTimeRptDtl : edsWaitTimeRptList.get(0).getEdsWaitTimeRptDtlList()) {
					if(StringUtils.equals(edsWaitTimeRptDtl.getTimeRange(),"12-13")) {
						assert edsWaitTimeRptDtl.getAvgTime().equals(28);
						break;
					}
				}
			}
		}.run();
		
	}	
}
