package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.reftable.PmsRestrictItemSpecByItemListServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.dms.persistence.PmsRestrictItem;
import hk.org.ha.model.pms.vo.patient.PasSpecialty;

import java.util.ArrayList;
import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class PmsRestrictItemSpecByItemListTest extends SeamTest {

	@Test
	public void testPmsRestrictItemSpecByItemListComponent() throws Exception {
	
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();

		// retrieve restrict list by item code
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {   
				PmsRestrictItemSpecByItemListServiceLocal pmsRestrictItemSpecByItemListService = (PmsRestrictItemSpecByItemListServiceLocal) getValue("#{pmsRestrictItemSpecByItemListService}");
				pmsRestrictItemSpecByItemListService.retrievePasSpecList();
				List<PasSpecialty> pasSpecialtyList = (List<PasSpecialty>) getValue("#{pasSpecialtyList}");
				assert pasSpecialtyList.size()>0;
			}
		}.run();
		
		// retrieve restrict list by item
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {   
				PmsRestrictItemSpecByItemListServiceLocal pmsRestrictItemSpecByItemListService = (PmsRestrictItemSpecByItemListServiceLocal) getValue("#{pmsRestrictItemSpecByItemListService}");
				pmsRestrictItemSpecByItemListService.retrievePmsRestrictItemListByItem("ACET02", "F");
				List<PmsRestrictItem> pmsRestrictItemList = (List<PmsRestrictItem>) getValue("#{pmsRestrictItemList}");
				assert pmsRestrictItemList.size()>0;
			}
		}.run();

		// update restrict List  
		new ComponentTest() {
			protected void testComponents() throws Exception {   
				PmsRestrictItemSpecByItemListServiceLocal pmsRestrictItemSpecByItemListService = (PmsRestrictItemSpecByItemListServiceLocal) getValue("#{pmsRestrictItemSpecByItemListService}");
				List<PmsRestrictItem> newPmsRestrictItemList = new ArrayList<PmsRestrictItem>();
				PmsRestrictItem pmsRestrictItem = new PmsRestrictItem();
				pmsRestrictItem.setPasSpecialty("AUD");
				pmsRestrictItem.setPasSubSpecialty("AUD");
				pmsRestrictItem.setSpecialtyType("O");
				newPmsRestrictItemList.add(pmsRestrictItem);
				
				pmsRestrictItemSpecByItemListService.updatePmsRestrictItemList(newPmsRestrictItemList, "AUD", "F");
			}
		}.run();
	}	
}
