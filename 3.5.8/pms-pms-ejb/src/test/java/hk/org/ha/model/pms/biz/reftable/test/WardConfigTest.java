package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.reftable.WardConfig;
import hk.org.ha.model.pms.udt.printing.PrintLang;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class WardConfigTest extends SeamTest {

	
	@Test
	public void testWardConfigComponent() throws Exception {
		
		//Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		
		//retrieve wardConfig
		new ComponentTest() {
			protected void testComponents() throws Exception {
				invokeMethod("#{wardConfigService.retrieveWardConfig( 'A001' )}");
				WardConfig wardConfig = (WardConfig) getValue("#{wardConfig}");
				assert wardConfig != null;
			}
		}.run();
		
		//add wardConfig
		new ComponentTest() {
			protected void testComponents() throws Exception {
				WardConfig newWardConfig = new WardConfig(); 
				newWardConfig.setWardCode("A003");
				newWardConfig.setPrintDosageInstructionFlag(false);
				newWardConfig.setDosageInstructionPrintLang(PrintLang.Eng);
				newWardConfig.setMpWardFlag(false);
				
				Contexts.getSessionContext().set("wardConfig", newWardConfig);
				invokeMethod("#{wardConfigService.updateWardConfig( wardConfig )}");
			}		
		}.run();
		
		//check wardConfig
		new ComponentTest() {
			protected void testComponents() throws Exception
			{	
				//retrieveWardConfig
				invokeMethod("#{wardConfigService.retrieveWardConfig( 'A003' )}");
				WardConfig wardConfig = (WardConfig) getValue("#{wardConfig}");
				assert wardConfig != null;
				assert wardConfig.getWardCode().equals("A003");
				assert wardConfig.getPrintDosageInstructionFlag() == false;
				assert wardConfig.getDosageInstructionPrintLang() == PrintLang.Eng;
				assert wardConfig.getMpWardFlag() == false;
				assert wardConfig.getWorkstoreGroup().getWorkstoreGroupCode().equals("QMH");
			}
		}.run();	
		
		//update wardConfig
		new ComponentTest() {
			protected void testComponents() throws Exception {
				invokeMethod("#{wardConfigService.retrieveWardConfig( 'A003' )}");
				WardConfig wardConfig = (WardConfig) getValue("#{wardConfig}");
				
				wardConfig.setPrintDosageInstructionFlag(true);
				wardConfig.setDosageInstructionPrintLang(PrintLang.Chi);
				wardConfig.setMpWardFlag(true);

				Contexts.getSessionContext().set("wardConfig", wardConfig);
				invokeMethod("#{wardConfigService.updateWardConfig( wardConfig )}");
			}
		
		}.run();
	
		//check wardConfig
		new ComponentTest() {
			protected void testComponents() throws Exception
			{	
				//retrieveWardConfig
				WardConfig wardConfig = (WardConfig) getValue("#{wardConfig}");
				assert wardConfig != null;
				assert wardConfig.getWardCode().equals("A003");
				assert wardConfig.getPrintDosageInstructionFlag() == true;
				assert wardConfig.getDosageInstructionPrintLang() == PrintLang.Chi;
				assert wardConfig.getMpWardFlag() == true;
				assert wardConfig.getWorkstoreGroup().getWorkstoreGroupCode().equals("QMH");
			}
		}.run();	
		
	}	
}
