package hk.org.ha.model.pms.report.test;

import hk.org.ha.model.pms.biz.report.SfiHandleChargeExemptionRptServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.vo.report.SfiHandleChargeExemptionRpt;

import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.joda.time.DateTime;
import org.testng.annotations.Test;

public class SfiHandleChargeExemptionRptTest extends SeamTest {
	
	@Test
	public void SfiHandleChargeExemptionRpt() throws Exception {
	
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();

		// retrieve suspendStat list by past 2 days
		new ComponentTest() {
			protected void testComponents() throws Exception {  
				DateTime rptDate = new DateTime().withDate(2011, 3, 3);
				SfiHandleChargeExemptionRptServiceLocal sfiHandleChargeExemptionRptService = (SfiHandleChargeExemptionRptServiceLocal) getValue("#{sfiHandleChargeExemptionRptService}");
				List<SfiHandleChargeExemptionRpt> sfiHandleChargeExemptionRptList = sfiHandleChargeExemptionRptService.retrieveSfiHandleChargeExemptionRptList(rptDate.toDate());
				
				assert sfiHandleChargeExemptionRptList.size()>0;
			}
		}.run();
		

		
	}	
}
