package hk.org.ha.model.pms.biz.label.test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import hk.org.ha.model.pms.biz.label.DispLabelBuilderLocal;
import hk.org.ha.model.pms.dms.persistence.DmDailyFrequency;
import hk.org.ha.model.pms.dms.persistence.DmRegimen;
import hk.org.ha.model.pms.dms.persistence.DmSupplFrequency;
import hk.org.ha.model.pms.biz.order.DispOrderManagerLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.corp.cache.DmDailyFrequencyCacherInf;
import hk.org.ha.model.pms.corp.cache.DmSupplFrequencyCacherInf;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.vetting.PrnPercentage;
import hk.org.ha.model.pms.udt.vetting.RegimenDurationUnit;
import hk.org.ha.model.pms.vo.label.DispLabel;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.Freq;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class DispLabelBuilderTest  extends SeamTest  {

	@Test
	public void testDispLabelBuilderBeanComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		// single dose, without step up/down
		new ComponentTest() {
			protected void testComponents() throws Exception {

				DmSupplFrequencyCacherInf dmSupplFrequencyCacher = (DmSupplFrequencyCacherInf) getInstance("dmSupplFrequencyCacher");
				DmDailyFrequencyCacherInf dmDailyFrequencyCacher = (DmDailyFrequencyCacherInf) getInstance("dmDailyFrequencyCacher");
				
				DispOrderManagerLocal dispOrderManager = (DispOrderManagerLocal) getInstance("dispOrderManager");
				DispOrder dispOrder = dispOrderManager.retrieveDispOrderById(Long.parseLong("51"));
				DispOrderItem dispOrderItem = dispOrder.getDispOrderItemList().get(0);
				dispOrderItem.getPharmOrderItem().loadDmInfo();
				DoseGroup doseGroup = new DoseGroup();
				doseGroup.setDuration(8);
				doseGroup.setDurationUnit(RegimenDurationUnit.Week);
				
				Dose dose = new Dose();
				dose.setDosage(BigDecimal.valueOf(5.5));
				dose.setDosageUnit("MG");
				dose.setPrn(false);
				dose.setPrnPercent(PrnPercentage.Zero);
				dose.setSiteCode("ORAL");
				
				DmDailyFrequency dmDailyFrequency = dmDailyFrequencyCacher.getDmDailyFrequencyByDailyFreqCode("00041");
				
				DmSupplFrequency dmSupplFrequency = dmSupplFrequencyCacher.getDmSupplFrequencyBySupplFreqCode("00017");

				Freq dailyFreq = new Freq();
				dailyFreq.setCode("00041");
				String [] dailyParam = {"1"};
				dailyFreq.setParam(dailyParam);
				
				Freq supplFreq = new Freq();
				supplFreq.setCode("00017");
				String [] supplParam = {"1","2"};
				supplFreq.setParam(supplParam);
				
				dose.setDmDailyFrequency(dmDailyFrequency);
				dose.setDmSupplFrequency(dmSupplFrequency);
				dose.setDailyFreq(dailyFreq);
				dose.setSupplFreq(supplFreq);
				dose.setFormVerb("TAKE");
				
				List<Dose> doseList = new ArrayList<Dose>();
				doseList.add(dose);
				doseGroup.setDoseList(doseList);
				
				List<DoseGroup> doseGroupList = new ArrayList<DoseGroup>();
				doseGroupList.add(doseGroup);

				dispOrderItem.getPharmOrderItem().getRegimen().setDoseGroupList(doseGroupList);
				
				dispOrderItem.setLegacy(true);
				
				DispLabelBuilderLocal dispLabelBuilder = (DispLabelBuilderLocal) getValue("#{dispLabelBuilder}");
				DispLabel dispLabel = dispLabelBuilder.convertToDispLabel(dispOrderItem, null);
				
				assert dispLabel.getBedNum().equals("17");
				assert dispLabel.getBinNum().equals("B008");
				assert dispLabel.getCaseNum().equals("AE02665901S");
				assert dispLabel.getBaseUnit().equals("TAB");
				assert dispLabel.getHospName().equals("QMH");
				assert dispLabel.getInstructionList().size() == 2;
				assert dispLabel.getIssueQty().equals(14);
				assert dispLabel.getItemCode().equals("PARA01");
				assert dispLabel.getItemDesc().equals("PARACETAMOL TABLET 500MG");
				assert dispLabel.getPatCatCode().equals("AC");
				assert dispLabel.getPatName().equals("PATIENT, SAMPLE1");
				assert dispLabel.getPatNameChi().equals("\u795D\u5B89\u5EB7");
				assert dispLabel.getSpecCode().equals("MED");
				assert dispLabel.getTicketNum().equals("0507");
				assert dispLabel.getUserCode().equals("admin");
				assert dispLabel.getWard().equals("5A");
				assert dispLabel.getWarningList().size() == 2;
				assert dispLabel.getWarningList().get(0).getLineList().size() == 4;
				assert dispLabel.getWarningList().get(1).getLineList().size() == 4;
				assert dispLabel.getInstructionList().get(0).getLineList().get(0).equals("TAKE^5.5 MG^1 HOUR(S) BEFORE SURGICAL PROCEDURE^(FROM DAY 1 TO DAY 2 PER MONTH)");
		
			}
		}.run();
		
		// single dose, with step up/down
		new ComponentTest() {
			protected void testComponents() throws Exception {

				DmSupplFrequencyCacherInf dmSupplFrequencyCacher = (DmSupplFrequencyCacherInf) getInstance("dmSupplFrequencyCacher");
				DmDailyFrequencyCacherInf dmDailyFrequencyCacher = (DmDailyFrequencyCacherInf) getInstance("dmDailyFrequencyCacher");
				
				DispOrderManagerLocal dispOrderManager = (DispOrderManagerLocal) getInstance("dispOrderManager");
				DispOrder dispOrder = dispOrderManager.retrieveDispOrderById(Long.parseLong("53"));
				DispOrderItem dispOrderItem = dispOrder.getDispOrderItemList().get(0);

				dispOrderItem.getPharmOrderItem().loadDmInfo();
				int doseGroupCounter = 0;
				for (DoseGroup doseGroup:dispOrderItem.getPharmOrderItem().getRegimen().getDoseGroupList()) {
					doseGroupCounter++;
					
					DmRegimen dmRegimen = new DmRegimen();
					dmRegimen.setRegimenMultiplier(7);
					doseGroup.setDmRegimen(dmRegimen);
					doseGroup.setDuration(8);
					doseGroup.setDurationUnit(RegimenDurationUnit.Week);
					
					Dose dose = new Dose();
					dose.setDosage(BigDecimal.valueOf(5.5));
					dose.setDosageUnit("MG");
					dose.setPrn(false);
					dose.setPrnPercent(PrnPercentage.Zero);
					dose.setSiteCode("ORAL");
					dose.setFormVerb("TAKE");
					
					DmDailyFrequency dmDailyFrequency = null;
					DmSupplFrequency dmSupplFrequency = null;
					Freq dailyFreq = new Freq();
					Freq supplFreq = new Freq();
					if (doseGroupCounter == 1) {
						dmDailyFrequency = dmDailyFrequencyCacher.getDmDailyFrequencyByDailyFreqCode("00041");					
						dmSupplFrequency = dmSupplFrequencyCacher.getDmSupplFrequencyBySupplFreqCode("00017");
	
						dailyFreq.setCode("00041");
						String [] dailyParam = {"1"};
						dailyFreq.setParam(dailyParam);
						
						supplFreq.setCode("00017");
						String [] supplParam = {"1","2"};
						supplFreq.setParam(supplParam);
					} else {
						dmDailyFrequency = dmDailyFrequencyCacher.getDmDailyFrequencyByDailyFreqCode("00001");					
						dailyFreq.setCode("00001");
					}
					dose.setDmDailyFrequency(dmDailyFrequency);
					dose.setDmSupplFrequency(dmSupplFrequency);
					dose.setDailyFreq(dailyFreq);
					dose.setSupplFreq(supplFreq);
					
					List<Dose> doseList = new ArrayList<Dose>();
					doseList.add(dose);
					doseGroup.setDoseList(doseList);
				}

				dispOrderItem.setLegacy(true);
				DispLabelBuilderLocal dispLabelBuilder = (DispLabelBuilderLocal) getValue("#{dispLabelBuilder}");
				DispLabel dispLabel = dispLabelBuilder.convertToDispLabel(dispOrderItem, null);
				
				assert dispLabel.getInstructionList().size() == 2;
				assert dispLabel.getInstructionList().get(0).getLineList().size() == 3;
				assert dispLabel.getInstructionList().get(1).getLineList().size() == 3;
				assert dispLabel.getInstructionList().get(0).getLineList().get(0).equals("TAKE^");
				assert dispLabel.getInstructionList().get(0).getLineList().get(1).equals("@^5.5 MG^1 HOUR(S) BEFORE SURGICAL PROCEDURE^(FROM DAY 1 TO DAY 2 PER MONTH)^FOR 56 DAY(S)^THEN");
				assert dispLabel.getInstructionList().get(0).getLineList().get(2).equals("@^5.5 MG^DAILY^FOR 56 DAY(S).");
			}
		}.run();
		
		// multiple dose, without step up/down
		new ComponentTest() {
			protected void testComponents() throws Exception {
				DmSupplFrequencyCacherInf dmSupplFrequencyCacher = (DmSupplFrequencyCacherInf) getInstance("dmSupplFrequencyCacher");
				DmDailyFrequencyCacherInf dmDailyFrequencyCacher = (DmDailyFrequencyCacherInf) getInstance("dmDailyFrequencyCacher");
				DispOrderManagerLocal dispOrderManager = (DispOrderManagerLocal) getInstance("dispOrderManager");
				DispOrder dispOrder = dispOrderManager.retrieveDispOrderById(Long.parseLong("52"));
				DispOrderItem dispOrderItem = dispOrder.getDispOrderItemList().get(0);
				
				dispOrderItem.getPharmOrderItem().loadDmInfo();
				for (DoseGroup doseGroup:dispOrderItem.getPharmOrderItem().getRegimen().getDoseGroupList()) {
					DmRegimen dmRegimen = new DmRegimen();
					dmRegimen.setRegimenMultiplier(7);
					doseGroup.setDmRegimen(dmRegimen);
					doseGroup.setDuration(8);
					doseGroup.setDurationUnit(RegimenDurationUnit.Week);
					int doseCounter = 0;
					for (Dose dose:doseGroup.getDoseList()) {
						doseCounter++;
						dose.setDosage(BigDecimal.valueOf(5.5));
						dose.setDosageUnit("MG");
						dose.setPrn(false);
						dose.setPrnPercent(PrnPercentage.Zero);
						dose.setSiteCode("ORAL");
						dose.setFormVerb("TAKE");
						
						DmDailyFrequency dmDailyFrequency = null;
						DmSupplFrequency dmSupplFrequency = null;
						Freq dailyFreq = new Freq();
						Freq supplFreq = new Freq();
						
						if (doseCounter == 1) {
							dmDailyFrequency = dmDailyFrequencyCacher.getDmDailyFrequencyByDailyFreqCode("00041");					
							dmSupplFrequency = dmSupplFrequencyCacher.getDmSupplFrequencyBySupplFreqCode("00017");
		
							dailyFreq.setCode("00041");
							String [] dailyParam = {"1"};
							dailyFreq.setParam(dailyParam);
							
							supplFreq.setCode("00017");
							String [] supplParam = {"1","2"};
							supplFreq.setParam(supplParam);
						} else {
							dmDailyFrequency = dmDailyFrequencyCacher.getDmDailyFrequencyByDailyFreqCode("00001");					
							dailyFreq.setCode("00001");
						}
					
						dose.setDmDailyFrequency(dmDailyFrequency);
						dose.setDmSupplFrequency(dmSupplFrequency);
						dose.setDailyFreq(dailyFreq);
						dose.setSupplFreq(supplFreq);
					}
				}
				
				dispOrderItem.setLegacy(true);
				DispLabelBuilderLocal dispLabelBuilder = (DispLabelBuilderLocal) getValue("#{dispLabelBuilder}");
				DispLabel dispLabel = dispLabelBuilder.convertToDispLabel(dispOrderItem, null);

				PrintOption printOption = new PrintOption();
				printOption.setPrintLang(PrintLang.Eng);
				dispLabel.setPrintOption(printOption);

				assert dispLabel.getInstructionList().size() == 2;
				assert dispLabel.getInstructionList().get(0).getLineList().size() == 4;
				
				assert dispLabel.getInstructionList().get(0).getLineList().get(0).equals("TAKE^");
				assert dispLabel.getInstructionList().get(0).getLineList().get(1).equals("@^5.5 MG^1 HOUR(S) BEFORE SURGICAL PROCEDURE^(FROM DAY 1 TO DAY 2 PER MONTH)^AND");
				assert dispLabel.getInstructionList().get(0).getLineList().get(2).equals("@^5.5 MG^DAILY^AND");
				assert dispLabel.getInstructionList().get(0).getLineList().get(3).equals("@^5.5 MG^DAILY");

			}
		}.run();
		

		
	}
	
}
