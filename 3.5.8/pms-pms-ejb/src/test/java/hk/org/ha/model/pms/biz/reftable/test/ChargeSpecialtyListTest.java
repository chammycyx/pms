package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.reftable.ChargeSpecialtyListServiceLocal;
import hk.org.ha.model.pms.biz.reftable.SpecialtyListServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.phs.Specialty;
import hk.org.ha.model.pms.persistence.reftable.ChargeSpecialty;
import hk.org.ha.model.pms.udt.reftable.ChargeSpecialtyType;

import java.util.List;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class ChargeSpecialtyListTest extends SeamTest {

	@Test
	public void testChargeSpecialtyListComponent() throws Exception {
		

		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		// retrieve charge specialty list
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {   
				ChargeSpecialtyListServiceLocal chargeSpecialtyListService = (ChargeSpecialtyListServiceLocal) getValue("#{chargeSpecialtyListService}");
				chargeSpecialtyListService.retrieveChargeSpecialtyList();
				List<ChargeSpecialty> chargeSpecialtyList = (List<ChargeSpecialty>) getValue("#{chargeSpecialtyList}");
				assert chargeSpecialtyList.size()==1;
			}
		}.run();
		
		// add charge specialty
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				ChargeSpecialtyListServiceLocal chargeSpecialtyListService = (ChargeSpecialtyListServiceLocal) getValue("#{chargeSpecialtyListService}");
				chargeSpecialtyListService.retrieveChargeSpecialtyList();
				List<ChargeSpecialty> chargeSpecialtyList = (List<ChargeSpecialty>) getValue("#{chargeSpecialtyList}");
				List<ChargeSpecialty> chargeSpecialtySnList = (List<ChargeSpecialty>) getValue("#{chargeSpecialtySnList}");
				
				ChargeSpecialty chargeSpecialty = new ChargeSpecialty();
				chargeSpecialty.setSpecCode("AE");
				chargeSpecialty.setChargeSpecCode("DER");
				chargeSpecialty.setType(ChargeSpecialtyType.Sfi);
				chargeSpecialty.setMarkDelete(false);
				
				chargeSpecialtyList.add(chargeSpecialty);
					
				Contexts.getSessionContext().set("chargeSpecialtyList", chargeSpecialtyList);
				Contexts.getSessionContext().set("chargeSpecialtySnList", chargeSpecialtySnList);
				invokeMethod("#{chargeSpecialtyListService.saveChargeSpecialtyList(chargeSpecialtyList, chargeSpecialtySnList)}");
			}
		
		}.run();
		
		//check charge specialty
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception
			{	
				//retrieve list
				ChargeSpecialtyListServiceLocal chargeSpecialtyListService = (ChargeSpecialtyListServiceLocal) getValue("#{chargeSpecialtyListService}");
				chargeSpecialtyListService.retrieveChargeSpecialtyList();
				List<ChargeSpecialty> chargeSpecialtyList = (List<ChargeSpecialty>) getValue("#{chargeSpecialtyList}");
				assert chargeSpecialtyList.size()==2;
				
				for(ChargeSpecialty chargeSpecialty: chargeSpecialtyList){
					if (chargeSpecialty.getSpecCode().equals("AE")){
						assert chargeSpecialty.getChargeSpecCode().equals("DER");
					}
					if (chargeSpecialty.getSpecCode().equals("DER")){
						assert chargeSpecialty.getChargeSpecCode().equals("AE");
					}
				}
			}
		}.run();

		// edit charge specialty
		new ComponentTest(){
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception{
				//retrieve list
				ChargeSpecialtyListServiceLocal chargeSpecialtyListService = (ChargeSpecialtyListServiceLocal) getValue("#{chargeSpecialtyListService}");
				chargeSpecialtyListService.retrieveChargeSpecialtyList();
				List<ChargeSpecialty> chargeSpecialtyList = (List<ChargeSpecialty>) getValue("#{chargeSpecialtyList}");
				List<ChargeSpecialty> chargeSpecialtySnList = (List<ChargeSpecialty>) getValue("#{chargeSpecialtySnList}");

				for(ChargeSpecialty chargeSpecialty: chargeSpecialtyList){
					if (chargeSpecialty.getSpecCode().equals("AE")){
						chargeSpecialty.setChargeSpecCode("ESO");
						chargeSpecialty.setMarkDelete(false);
					}
					
					if (chargeSpecialty.getSpecCode().equals("DER")){
						chargeSpecialty.setChargeSpecCode("ESO");
						chargeSpecialty.setMarkDelete(false);
					}
				}
				Contexts.getSessionContext().set("chargeSpecialtyList", chargeSpecialtyList);
				Contexts.getSessionContext().set("chargeSpecialtySnList", chargeSpecialtySnList);
				invokeMethod("#{chargeSpecialtyListService.saveChargeSpecialtyList(chargeSpecialtyList, chargeSpecialtySnList)}");
			}
		}.run();

		//check charge specialty
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception
			{	
				//retrieve list
				ChargeSpecialtyListServiceLocal chargeSpecialtyListService = (ChargeSpecialtyListServiceLocal) getValue("#{chargeSpecialtyListService}");
				chargeSpecialtyListService.retrieveChargeSpecialtyList();
				List<ChargeSpecialty> chargeSpecialtyList = (List<ChargeSpecialty>) getValue("#{chargeSpecialtyList}");
				assert chargeSpecialtyList.size()==2;
				
				for(ChargeSpecialty chargeSpecialty: chargeSpecialtyList){
					if (chargeSpecialty.getSpecCode().equals("AE")){
						assert chargeSpecialty.getChargeSpecCode().equals("ESO");
					}
					
					if (chargeSpecialty.getSpecCode().equals("DER")){
						assert chargeSpecialty.getChargeSpecCode().equals("ESO");
					}
				}
			}
		}.run();
		
		// delete charge specialty
		new ComponentTest(){
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception{
				//retrieve list
				ChargeSpecialtyListServiceLocal chargeSpecialtyListService = (ChargeSpecialtyListServiceLocal) getValue("#{chargeSpecialtyListService}");
				chargeSpecialtyListService.retrieveChargeSpecialtyList();
				List<ChargeSpecialty> chargeSpecialtyList = (List<ChargeSpecialty>) getValue("#{chargeSpecialtyList}");
				List<ChargeSpecialty> chargeSpecialtySnList = (List<ChargeSpecialty>) getValue("#{chargeSpecialtySnList}");
				
				for(ChargeSpecialty chargeSpecialty: chargeSpecialtyList){
					if (chargeSpecialty.getSpecCode().equals("DER")){
						chargeSpecialty.setMarkDelete(true);
					}
				}	
				Contexts.getSessionContext().set("chargeSpecialtyList", chargeSpecialtyList);
				Contexts.getSessionContext().set("chargeSpecialtySnList", chargeSpecialtySnList);
				invokeMethod("#{chargeSpecialtyListService.saveChargeSpecialtyList(chargeSpecialtyList, chargeSpecialtySnList)}");
			}
		}.run();
		
		//check charge specialty
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception
			{	
				//retrieve list
				ChargeSpecialtyListServiceLocal chargeSpecialtyListService = (ChargeSpecialtyListServiceLocal) getValue("#{chargeSpecialtyListService}");
				chargeSpecialtyListService.retrieveChargeSpecialtyList();
				List<ChargeSpecialty> chargeSpecialtyList = (List<ChargeSpecialty>) getValue("#{chargeSpecialtyList}");
				assert chargeSpecialtyList.size()==1;
				
				int check = 0;
				for(ChargeSpecialty chargeSpecialty: chargeSpecialtyList){
					if (chargeSpecialty.getSpecCode().equals("DER")){
						check++;
					}

				}
				assert check == 0;
			}
		}.run();
		
		// retrieve specialty list
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {   
				SpecialtyListServiceLocal specialtyListService = (SpecialtyListServiceLocal) getValue("#{specialtyListService}");
				specialtyListService.retrieveSpecialtyList();
				List<Specialty> specialtyList = (List<Specialty>) getValue("#{specialtyList}");
				assert specialtyList.size()>0;
			}
		}.run();
		
		// retrieve saftynet charge specialty list
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {   
				ChargeSpecialtyListServiceLocal chargeSpecialtyListService = (ChargeSpecialtyListServiceLocal) getValue("#{chargeSpecialtyListService}");
				chargeSpecialtyListService.retrieveChargeSpecialtyList();
				List<ChargeSpecialty> chargeSpecialtySnList = (List<ChargeSpecialty>) getValue("#{chargeSpecialtySnList}");
				assert chargeSpecialtySnList.size()==1;
			}
		}.run();
		
		// add saftynet charge specialty
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				ChargeSpecialtyListServiceLocal chargeSpecialtyListService = (ChargeSpecialtyListServiceLocal) getValue("#{chargeSpecialtyListService}");
				chargeSpecialtyListService.retrieveChargeSpecialtyList();
				List<ChargeSpecialty> chargeSpecialtyList = (List<ChargeSpecialty>) getValue("#{chargeSpecialtyList}");
				List<ChargeSpecialty> chargeSpecialtySnList = (List<ChargeSpecialty>) getValue("#{chargeSpecialtySnList}");
				
				ChargeSpecialty chargeSpecialty = new ChargeSpecialty();
				chargeSpecialty.setSpecCode("AE");
				chargeSpecialty.setChargeSpecCode("DER");
				chargeSpecialty.setType(ChargeSpecialtyType.SafetyNet);
				chargeSpecialty.setMarkDelete(false);
				
				chargeSpecialtySnList.add(chargeSpecialty);
					
				Contexts.getSessionContext().set("chargeSpecialtyList", chargeSpecialtyList);
				Contexts.getSessionContext().set("chargeSpecialtyList", chargeSpecialtySnList);
				invokeMethod("#{chargeSpecialtyListService.saveChargeSpecialtyList(chargeSpecialtyList, chargeSpecialtySnList)}");
			}
		
		}.run();
		
		//check saftynet charge specialty
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception
			{	
				//retrieve list
				ChargeSpecialtyListServiceLocal chargeSpecialtyListService = (ChargeSpecialtyListServiceLocal) getValue("#{chargeSpecialtyListService}");
				chargeSpecialtyListService.retrieveChargeSpecialtyList();
				List<ChargeSpecialty> chargeSpecialtySnList = (List<ChargeSpecialty>) getValue("#{chargeSpecialtySnList}");
				assert chargeSpecialtySnList.size()==2;
				
				for(ChargeSpecialty chargeSpecialty: chargeSpecialtySnList){
					if (chargeSpecialty.getSpecCode().equals("DER")){
						assert chargeSpecialty.getChargeSpecCode().equals("AE");
					}
					
					if (chargeSpecialty.getSpecCode().equals("AE")){
						assert chargeSpecialty.getChargeSpecCode().equals("DER");
					}
				}
			}
		}.run();
		
		// edit saftynet charge specialty
		new ComponentTest(){
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception{
				//retrieve list
				ChargeSpecialtyListServiceLocal chargeSpecialtyListService = (ChargeSpecialtyListServiceLocal) getValue("#{chargeSpecialtyListService}");
				chargeSpecialtyListService.retrieveChargeSpecialtyList();
				List<ChargeSpecialty> chargeSpecialtyList = (List<ChargeSpecialty>) getValue("#{chargeSpecialtyList}");
				List<ChargeSpecialty> chargeSpecialtySnList = (List<ChargeSpecialty>) getValue("#{chargeSpecialtySnList}");

				for(ChargeSpecialty chargeSpecialty: chargeSpecialtySnList){
					if (chargeSpecialty.getSpecCode().equals("AE")){
						chargeSpecialty.setChargeSpecCode("ESO");
						chargeSpecialty.setMarkDelete(false);
					}
					
					if (chargeSpecialty.getSpecCode().equals("DER")){
						chargeSpecialty.setChargeSpecCode("ESO");
						chargeSpecialty.setMarkDelete(false);
					}
				}
				
				Contexts.getSessionContext().set("chargeSpecialtyList", chargeSpecialtyList);
				Contexts.getSessionContext().set("chargeSpecialtySnList", chargeSpecialtySnList);
				invokeMethod("#{chargeSpecialtyListService.saveChargeSpecialtyList(chargeSpecialtyList, chargeSpecialtySnList)}");
			}
		}.run();


		//check saftynet charge specialty
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception
			{	
				//retrieve list
				ChargeSpecialtyListServiceLocal chargeSpecialtyListService = (ChargeSpecialtyListServiceLocal) getValue("#{chargeSpecialtyListService}");
				chargeSpecialtyListService.retrieveChargeSpecialtyList();
				List<ChargeSpecialty> chargeSpecialtySnList = (List<ChargeSpecialty>) getValue("#{chargeSpecialtySnList}");
				assert chargeSpecialtySnList.size()==2;
				
				for(ChargeSpecialty chargeSpecialty: chargeSpecialtySnList){
					if (chargeSpecialty.getSpecCode().equals("AE")){
						assert chargeSpecialty.getChargeSpecCode().equals("ESO");
					}
					
					if (chargeSpecialty.getSpecCode().equals("DER")){
						assert chargeSpecialty.getChargeSpecCode().equals("ESO");
					}
				}
			}
		}.run();
		
		// delete saftynet charge specialty
		new ComponentTest(){
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception{
				//retrieve list
				ChargeSpecialtyListServiceLocal chargeSpecialtyListService = (ChargeSpecialtyListServiceLocal) getValue("#{chargeSpecialtyListService}");
				chargeSpecialtyListService.retrieveChargeSpecialtyList();
				List<ChargeSpecialty> chargeSpecialtyList = (List<ChargeSpecialty>) getValue("#{chargeSpecialtyList}");
				List<ChargeSpecialty> chargeSpecialtySnList = (List<ChargeSpecialty>) getValue("#{chargeSpecialtySnList}");
				
				for(ChargeSpecialty chargeSpecialty: chargeSpecialtySnList){
					if (chargeSpecialty.getSpecCode().equals("AE")){
						chargeSpecialty.setMarkDelete(true);
					}
				}	
				Contexts.getSessionContext().set("chargeSpecialtyList", chargeSpecialtyList);
				Contexts.getSessionContext().set("chargeSpecialtySnList", chargeSpecialtySnList);
				invokeMethod("#{chargeSpecialtyListService.saveChargeSpecialtyList(chargeSpecialtyList, chargeSpecialtySnList)}");
			}
		}.run();
		
		//check charge specialty
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception
			{	
				//retrieve list
				ChargeSpecialtyListServiceLocal chargeSpecialtyListService = (ChargeSpecialtyListServiceLocal) getValue("#{chargeSpecialtyListService}");
				chargeSpecialtyListService.retrieveChargeSpecialtyList();
				List<ChargeSpecialty> chargeSpecialtySnList = (List<ChargeSpecialty>) getValue("#{chargeSpecialtySnList}");
				assert chargeSpecialtySnList.size()==1;
				
				Boolean check = true;
				for(ChargeSpecialty chargeSpecialty: chargeSpecialtySnList){
					if (chargeSpecialty.getSpecCode().equals("AE")){
						check = false;
					}	
				}
				assert check == true;
			}
		}.run();

	}	
}
