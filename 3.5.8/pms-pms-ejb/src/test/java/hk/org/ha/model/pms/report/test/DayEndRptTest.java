package hk.org.ha.model.pms.report.test;

import hk.org.ha.model.pms.biz.report.DayEndRptServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.vo.report.DayEndFileExtractRpt;

import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.joda.time.DateTime;
import org.testng.annotations.Test;

public class DayEndRptTest extends SeamTest {
	
	@Test
	public void testDayEndRptComponent() throws Exception {
	
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();

		// retrieve DayEndDefDoctorRpt list
		new ComponentTest() {
			protected void testComponents() throws Exception {  
				DateTime batchDate = new DateTime("2011-09-27");
				
				DayEndRptServiceLocal dayEndRptService = (DayEndRptServiceLocal) getValue("#{dayEndRptService}");
				dayEndRptService.retrieveDayEndRpt(0,"WKS1", batchDate.toDate());
				
				assert dayEndRptService.getReportObj().size()>0;
			}
		}.run();

		// retrieve DayEndFileExtractRpt list
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {  
				DateTime batchDate = new DateTime("2011-09-27");
				
				DayEndRptServiceLocal dayEndRptService = (DayEndRptServiceLocal) getValue("#{dayEndRptService}");
				dayEndRptService.retrieveDayEndRpt(1,"WKS1", batchDate.toDate());
				
				List<DayEndFileExtractRpt> dayEndFileExtractRptlist = (List<DayEndFileExtractRpt>) dayEndRptService.getReportObj();
				
				assert dayEndFileExtractRptlist.get(0).getDayEndFileExtractRptDtl().getOpItemCount()>0;
				assert dayEndFileExtractRptlist.get(0).getDayEndFileExtractRptDtl().getOpPrescCount()>0;
				assert dayEndFileExtractRptlist.get(0).getDayEndFileExtractRptDtl().getOpDispSampleCount()>0;
				assert dayEndFileExtractRptlist.get(0).getDayEndFileExtractRptDtl().getOpDispSfiCount()>0;
				assert dayEndFileExtractRptlist.get(0).getDayEndFileExtractRptDtl().getOpDispSafetyNetItemCount()>0;
				assert dayEndFileExtractRptlist.get(0).getDayEndFileExtractRptDtl().getCapdVoucherCount()>0;
				assert dayEndFileExtractRptlist.get(0).getDayEndFileExtractRptDtl().getCapdItemCount()>0;
			}
		}.run();
		
	}	
}
