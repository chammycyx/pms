package hk.org.ha.model.pms.biz.vetting.test;

import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.udt.vetting.PrnPercentage;
import hk.org.ha.model.pms.udt.vetting.RegimenDurationUnit;
import hk.org.ha.model.pms.udt.vetting.RegimenType;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.Freq;
import hk.org.ha.model.pms.vo.rx.Regimen;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class ChestConfirmServiceTest extends SeamTest {
	
	private String itemCode = "LACT21";
	
	@Test
	public void testChestConfirmServiceComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		new ComponentTest() {
			protected void testComponents() throws Exception {
				
				//create context medOrder and pharmOrder
				MedOrder medOrder = new MedOrder();
				medOrder.clearMedOrderItemList();
				medOrder.clearPharmOrderList();
				
				PharmOrder pharmOrder = new PharmOrder();
				pharmOrder.clearPharmOrderItemList();
				pharmOrder.setMedOrder(medOrder);
				
				medOrder.getPharmOrderList().add(pharmOrder);
				Contexts.getSessionContext().set("pharmOrder", pharmOrder);
				
				//create chest pharmOrder
				PharmOrder inPharmOrder = new PharmOrder();
				inPharmOrder.clearPharmOrderItemList();
				inPharmOrder.setMaxItemNum(1);
				
				//create chest pharmOrderItem
				PharmOrderItem inPharmOrderItem = new PharmOrderItem();
				
				Regimen regimen = new Regimen();
				regimen.setType(RegimenType.Daily);
				regimen.setDuration(1);
				regimen.setDurationUnit(RegimenDurationUnit.Day);
				regimen.setDoseGroupList(new ArrayList<DoseGroup>());
				
				DoseGroup doseGroup = new DoseGroup();
				doseGroup.setDuration(1);
				doseGroup.setDurationUnit(RegimenDurationUnit.Day);
				doseGroup.setStartDate(new Date());
				doseGroup.setEndDate(new Date());
				doseGroup.setDoseList(new ArrayList<Dose>());
				
				Dose dose = new Dose();
				dose.setDosage(BigDecimal.valueOf(300));
				dose.setDosageUnit("SACHET(S)");
				dose.setPrn(Boolean.FALSE);
				dose.setPrnPercent(PrnPercentage.Hundred);
				dose.setSiteCode("ORAL");
				
				Freq dailyDrequency = new Freq();
				dailyDrequency.setCode("00001");
				
				dose.setDailyFreq(dailyDrequency);
				
				doseGroup.getDoseList().add(dose);
				regimen.getDoseGroupList().add(doseGroup);
				
				inPharmOrderItem.setRegimen(regimen);
				inPharmOrderItem.setItemCode(itemCode);
				inPharmOrderItem.setDoseUnit("SACHET(S)");
				inPharmOrderItem.setItemNum(1);
				inPharmOrderItem.setOrgItemNum(1);
				
				inPharmOrder.getPharmOrderItemList().add(inPharmOrderItem);

				Contexts.getSessionContext().set("inPharmOrder", inPharmOrder);
				invokeMethod("#{chestConfirmService.updatePharmOrderFromChest(inPharmOrder)}");
			}
		}.run();
		
		new ComponentTest() {
			protected void testComponents() throws Exception {
				PharmOrder pharmOrder = (PharmOrder)getValue("#{pharmOrder}");
				MedOrder medOrder = pharmOrder.getMedOrder();
				
				assert medOrder != null;
				assert medOrder.getMedOrderItemList().size()==1;
				assert medOrder.getMaxItemNum().equals(2);
				
				for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()){
					Boolean vetFlag = Boolean.TRUE;
					for (PharmOrderItem pharmOrderItem : medOrderItem.getPharmOrderItemList()) {
						if (pharmOrderItem.getIssueQty() == null) {
							vetFlag = Boolean.FALSE;
							break;
						}
					}
					assert medOrderItem.getVetFlag() == vetFlag;
				}
				
				assert pharmOrder.getChestFlag();
				assert !pharmOrder.getPharmOrderItemList().isEmpty();
				
				for (PharmOrderItem pharmOrderItem : pharmOrder.getPharmOrderItemList()){
					if (pharmOrderItem.getItemCode().equals(itemCode)) {
						assert pharmOrderItem.getCalQty()!=null;
						assert pharmOrderItem.getIssueQty()!=null;
					} else {
						assert false;
					}
				}
			}
		}.run();
		
	}	
}
