package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.reftable.ChestServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.reftable.Chest;
import hk.org.ha.model.pms.persistence.reftable.ChestItem;

import java.math.BigDecimal;
import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class ChestTest extends SeamTest {

	@Test
	public void testChestComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		//retrieve chestList
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{chestListService.retrieveChestList('')}");
				List<Chest> chestList = (List<Chest>)getValue("#{chestList}");
				
				assert chestList!=null;
			}
		}.run();

		
		// retrieve Chest
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				ChestServiceLocal chestService = (ChestServiceLocal) getInstance("chestService");
				Chest chest = chestService.retrieveChest("UNIT DOSE 1");
				
				assert chest!=null;
				assert chest.getChestItemList().size()>0;
			}
		}.run();


		// Create new Chest
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				ChestServiceLocal chestService = (ChestServiceLocal) getInstance("chestService");
				Chest chest = chestService.createChest();
				
				assert chest!=null;
			}
		}.run();
		
		//Add Chest
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				ChestServiceLocal chestService = (ChestServiceLocal) getInstance("chestService");
				Chest chest = chestService.createChest();
				assert chest!=null;
				chest.setUnitDoseName("S1");
				chest.setDefaultAdvStart(0);
				chest.setDefaultDuration(2);
				
				List<ChestItem> chestItemList = chest.getChestItemList();
				
				ChestItem chestItemObj = new ChestItem();
				chestItemObj.setChest(chest);
				chestItemObj.setItemCode("OFLO01");
				chestItemObj.setDoseQty(new BigDecimal(5));
				chestItemObj.setDoseUnit("TABLET(S)");
				chestItemList.add(chestItemObj);
				
				chestItemObj = new ChestItem();
				chestItemObj.setChest(chest);
				chestItemObj.setItemCode("ETHA03");
				chestItemObj.setDoseQty(new BigDecimal(1));
				chestItemObj.setDoseUnit("TABLET(S)");
				chestItemList.add(chestItemObj);

				chestService.updateChest(chest);
			}
		}.run();
		
		// check Chest
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				ChestServiceLocal chestService = (ChestServiceLocal) getInstance("chestService");
				Chest chest = chestService.retrieveChest("S1");
				assert chest!=null;
				
				assert chest.getDefaultAdvStart().equals(0);
				assert chest.getDefaultDuration().equals(2);
				
				List<ChestItem> chestItemList = chest.getChestItemList();
				
				if(chestItemList.size()>0)
				{
					for(ChestItem chestItemObj:chestItemList)
					{
						if(chestItemObj.getItemCode().equals("OFLO01"))
						{
							assert chestItemObj.getDoseQty().equals(new BigDecimal(5));
							assert chestItemObj.getDoseUnit().equals("TABLET(S)");
						}
						else if(chestItemObj.getItemCode().equals("ETHA03"))
						{
							assert chestItemObj.getDoseQty().equals(new BigDecimal(1));
							assert chestItemObj.getDoseUnit().equals("TABLET(S)");
						}
						else
						{
							assert false;
						}
					}
				}else
				{
					assert false;
				}
				
			}
		}.run();
		
		//Edit Chest
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				ChestServiceLocal chestService = (ChestServiceLocal) getInstance("chestService");
				Chest chest = chestService.retrieveChest("S1");
				assert chest!=null;

				chest.setDefaultAdvStart(1);
				chest.setDefaultDuration(3);
				
				List<ChestItem> chestItemList = chest.getChestItemList();
				
				if(chestItemList.size()>0)
				{
					for(ChestItem chestItemObj:chestItemList)
					{
						if(chestItemObj.getItemCode().equals("OFLO01"))
						{
							chestItemObj.setDoseQty(new BigDecimal(3));
							chestItemObj.setDoseUnit("ML");
						}
						else if(chestItemObj.getItemCode().equals("ETHA03"))
						{
							chestItemObj.setDoseQty(new BigDecimal(3));
							chestItemObj.setDoseUnit("ML");
						}
						else
						{
							assert false;
						}
					}
				}else
				{
					assert false;
				}

				chestService.updateChest(chest);
			}
		}.run();
		
		// check Chest
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				ChestServiceLocal chestService = (ChestServiceLocal) getInstance("chestService");
				Chest chest = chestService.retrieveChest("S1");
				assert chest!=null;
				
				assert chest.getDefaultAdvStart().equals(1);
				assert chest.getDefaultDuration().equals(3);
				
				List<ChestItem> chestItemList = chest.getChestItemList();
				
				if(chestItemList.size()>0)
				{
					for(ChestItem chestItemObj:chestItemList)
					{
						if(chestItemObj.getItemCode().equals("OFLO01"))
						{
							assert chestItemObj.getDoseQty().equals(new BigDecimal(3));
							assert chestItemObj.getDoseUnit().equals("ML");
						}
						else if(chestItemObj.getItemCode().equals("ETHA03"))
						{
							assert chestItemObj.getDoseQty().equals(new BigDecimal(3));
							assert chestItemObj.getDoseUnit().equals("ML");
						}
						else
						{
							assert false;
						}
					}
				}else
				{
					assert false;
				}
				
			}
		}.run();
		
		//Delete ChestItem
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				ChestServiceLocal chestService = (ChestServiceLocal) getInstance("chestService");
				Chest chest = chestService.retrieveChest("S1");
				assert chest!=null;
				
				List<ChestItem> chestItemList = chest.getChestItemList();
				
				if(chestItemList.size()>0)
				{
					for(ChestItem chestItemObj:chestItemList)
					{
						if(chestItemObj.getItemCode().equals("OFLO01"))
						{
							chestItemObj.setMarkDelete(true);
						}
					}
				}else
				{
					assert false;
				}

				chestService.updateChest(chest);
			}
		}.run();
		
		// check Chest
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				ChestServiceLocal chestService = (ChestServiceLocal) getInstance("chestService");
				Chest chest = chestService.retrieveChest("S1");
				assert chest!=null;
				
				List<ChestItem> chestItemList = chest.getChestItemList();
				
				if(chestItemList.size()>0)
				{
					for(ChestItem chestItemObj:chestItemList)
					{
						assert !chestItemObj.getItemCode().equals("OFLO01");
					}
				}else
				{
					assert false;
				}
				
			}
		}.run();
		
		//Delete Chest
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				ChestServiceLocal chestService = (ChestServiceLocal) getInstance("chestService");
				Chest chest = chestService.retrieveChest("S1");
				assert chest!=null;
				
				List<ChestItem> chestItemList = chest.getChestItemList();
				
				if(chestItemList.size()>0)
				{	
					for(ChestItem chestItemObj:chestItemList){
						if(chestItemObj.getItemCode().equals("ETHA03"))
						{
							chestItemObj.setMarkDelete(true);
						}
					}
				}else
				{
					assert false;
				}

				chestService.updateChest(chest);
			}
		}.run();
		
		// check Chest
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				ChestServiceLocal chestService = (ChestServiceLocal) getInstance("chestService");
				Chest chest = chestService.retrieveChest("S1");
				assert chest!=null;
				
			}
		}.run();
		
		//save exist Chest
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				ChestServiceLocal chestService = (ChestServiceLocal) getInstance("chestService");
				Chest chest = chestService.createChest();
				assert chest!=null;
				chest.setUnitDoseName("S1");
				chest.setDefaultAdvStart(0);
				chest.setDefaultDuration(2);
				
				List<ChestItem> chestItemList = chest.getChestItemList();
				
				ChestItem chestItemObj = new ChestItem();
				chestItemObj.setChest(chest);
				chestItemObj.setItemCode("OFLO01");
				chestItemObj.setDoseQty(new BigDecimal(5));
				chestItemObj.setDoseUnit("TABLET(S)");
				chestItemList.add(chestItemObj);
				
				chestItemObj = new ChestItem();
				chestItemObj.setChest(chest);
				chestItemObj.setItemCode("ETHA03");
				chestItemObj.setDoseQty(new BigDecimal(1));
				chestItemObj.setDoseUnit("TABLET(S)");
				chestItemList.add(chestItemObj);

				chestService.updateChest(chest);
			}
		}.run();
		
		// check Chest
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				ChestServiceLocal chestService = (ChestServiceLocal) getInstance("chestService");
				boolean save = chestService.isSaveSuccess();
				
				assert !save;
				
			}
		}.run();
		
		// delete Chest
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				ChestServiceLocal chestService = (ChestServiceLocal) getInstance("chestService");
				Chest chest = chestService.retrieveChest("S1");
				assert chest!=null;

				chestService.deleteChest(chest);
				

			}
		}.run();
		
		// check Chest
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				ChestServiceLocal chestService = (ChestServiceLocal) getInstance("chestService");
				chestService.retrieveChest("S1");
				boolean retrieve = chestService.isRetrieveSuccess();
				
				assert !retrieve;
			}
		}.run();
	}	
}
