package hk.org.ha.model.pms.report.test;

import hk.org.ha.model.pms.biz.report.EdsTicketNumRptServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.vo.report.TicketNumRpt;

import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.joda.time.DateTime;
import org.testng.annotations.Test;

public class EdsTicketNumRptTest extends SeamTest {
	
	@Test
	public void testEdsTicketNumRptComponent() throws Exception {
	
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();

		// retrieve list 
		new ComponentTest() {
			protected void testComponents() throws Exception {  
				DateTime today = new DateTime().withDate(2011, 3, 30);
				EdsTicketNumRptServiceLocal edsTicketNumRptService = (EdsTicketNumRptServiceLocal) getValue("#{edsTicketNumRptService}");
				List<TicketNumRpt> ticketNumList = edsTicketNumRptService.retrieveEdsTicketNumRptList(today.toDate(), "0702");
				
				assert ticketNumList.size() >0;
			}
		}.run();
		
	}	
}
