package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.reftable.QtyDurationConversionRptServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.dms.persistence.PmsDurationConversion;
import hk.org.ha.model.pms.dms.persistence.PmsPcuMapping;
import hk.org.ha.model.pms.dms.persistence.PmsPcuMappingPK;
import hk.org.ha.model.pms.dms.persistence.PmsQtyConversion;
import hk.org.ha.model.pms.udt.reftable.DurationUnit;
import hk.org.ha.model.pms.vo.reftable.PmsDurationConversionItem;
import hk.org.ha.model.pms.vo.reftable.PmsQtyConversionItem;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class QtyDurationConversionTest extends SeamTest {

	@Test
	public void testQtyDurationConversionComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		//retrievePmsPcuMapping
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{qtyDurationConversionService.retrievePmsPcuMapping()}");
				List<PmsPcuMapping> pmsPcuMappingList  = (List<PmsPcuMapping>) getValue("#{pmsPcuMappingList }");
				
				assert pmsPcuMappingList !=null;
			}
		}.run();
		
		//retrieveQtyDurationConversionList
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				PmsPcuMapping pmsPcuMapping = new PmsPcuMapping();
				pmsPcuMapping.setPseudoHospCode("QMH");
				PmsPcuMappingPK pmsPcuMappingPK = new PmsPcuMappingPK();
				pmsPcuMappingPK.setDispHospCode("QMH");
				pmsPcuMappingPK.setDispWorkstore("WKS1");
				pmsPcuMapping.setCompId(pmsPcuMappingPK);
				
				Contexts.getSessionContext().set("pmsPcuMapping", pmsPcuMapping);
				invokeMethod("#{qtyDurationConversionService.retrieveQtyDurationConversionList('PARA01', pmsPcuMapping)}");
				
				List<PmsQtyConversion> qtyConversionList = (List<PmsQtyConversion>) getValue("#{qtyConversionList}");
				List<PmsQtyConversionItem> qtyConversionOtherWorkstoreList = (List<PmsQtyConversionItem>) getValue("#{qtyConversionOtherWorkstoreList}");
				List<PmsDurationConversion> durationConversionList = (List<PmsDurationConversion>) getValue("#{durationConversionList}");
				List<PmsDurationConversionItem> durationConversionOtherWorkstoreList = (List<PmsDurationConversionItem>) getValue("#{durationConversionOtherWorkstoreList}");
				
				assert qtyConversionList.size() == 1;
				assert qtyConversionOtherWorkstoreList.size()==1;
				assert qtyConversionOtherWorkstoreList.get(0).getPmsQtyConversionList().size()==2;
				assert durationConversionList.size() == 1;
				assert durationConversionOtherWorkstoreList.size()==1;
				assert durationConversionOtherWorkstoreList.get(0).getPmsDurationConversionList().size()==3;
				
				assert qtyConversionList.get(0).getDispHospCode().equals(pmsPcuMapping.getCompId().getDispHospCode());
				assert qtyConversionList.get(0).getDispWorkstore().equals(pmsPcuMapping.getCompId().getDispWorkstore());
				assert durationConversionList.get(0).getDispHospCode().equals(pmsPcuMapping.getCompId().getDispHospCode());
				assert durationConversionList.get(0).getDispWorkstore().equals(pmsPcuMapping.getCompId().getDispWorkstore());
				
				assert qtyConversionOtherWorkstoreList.get(0).getPmsQtyConversionList().get(0).getDispHospCode().equals("QMH");
				assert qtyConversionOtherWorkstoreList.get(0).getPmsQtyConversionList().get(0).getDispWorkstore().equals("WKS");
				assert durationConversionOtherWorkstoreList.get(0).getPmsDurationConversionList().get(0).getDispHospCode().equals("QMH");
				assert durationConversionOtherWorkstoreList.get(0).getPmsDurationConversionList().get(0).getDispWorkstore().equals("WKS");
				
				for (PmsDurationConversion pmsDurationConversion:durationConversionOtherWorkstoreList.get(0).getPmsDurationConversionList()){
					if ( pmsDurationConversion.getDurationFrom() == 1 ) {
						assert StringUtils.equals(pmsDurationConversion.getDurationUnitDesc(), DurationUnit.Day.getDisplayValue());
					} else if ( pmsDurationConversion.getDurationFrom() == 3 ) {
						assert StringUtils.equals(pmsDurationConversion.getDurationUnitDesc(), DurationUnit.Month.getDisplayValue());
					}else if ( pmsDurationConversion.getDurationFrom() == 5 ) {
						assert StringUtils.equals(pmsDurationConversion.getDurationUnitDesc(), DurationUnit.Cycle.getDisplayValue());
					}
					
				}
			}
		}.run();
		
		//updateQtyDurationConversionList
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				List<PmsQtyConversion> udpatePmsQtyConversionList = new ArrayList<PmsQtyConversion>();
				List<PmsDurationConversion> udpatePmsDurationConversionList = new ArrayList<PmsDurationConversion>();
				Contexts.getSessionContext().set("udpatePmsQtyConversionList", udpatePmsQtyConversionList);
				Contexts.getSessionContext().set("udpatePmsDurationConversionList", udpatePmsDurationConversionList);
				invokeMethod("#{qtyDurationConversionService.updateQtyDurationConversionList(udpatePmsQtyConversionList, udpatePmsQtyConversionList)}");
			}
		}.run();
		
		// retrievePmsDurationConversionListForReport
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				QtyDurationConversionRptServiceLocal qtyDurationConversionRptService = (QtyDurationConversionRptServiceLocal) getInstance("qtyDurationConversionService");
				List<PmsDurationConversion> durationConversionList = qtyDurationConversionRptService.retrievePmsDurationConversionListForReport();
				
				for ( PmsDurationConversion durationConversion : durationConversionList ) {
					if ( StringUtils.equals(durationConversion.getItemCode(), "ACET01") ) {
						assert StringUtils.equals( durationConversion.getItemDesc(), "ACETAZOLAMIDE TABLET 250MG" );
						assert StringUtils.equals( durationConversion.getDurationUnitDesc(), "Cycles" );
					} else if ( StringUtils.equals(durationConversion.getItemCode(), "META01") ) {
						assert StringUtils.equals( durationConversion.getItemDesc(), "METABOLIC MINERAL MIXTURE" );
						assert StringUtils.equals( durationConversion.getDurationUnitDesc(), "Days" );
					} else if ( StringUtils.equals(durationConversion.getItemCode(), "PARA01") ) {
						assert StringUtils.equals( durationConversion.getItemDesc(), "PARACETAMOL TABLET 500MG" );
						assert StringUtils.equals( durationConversion.getDurationUnitDesc(), "Months" );
					} else {
						assert 1==2;
					}
				}
			}
		}.run();
	}	
}
