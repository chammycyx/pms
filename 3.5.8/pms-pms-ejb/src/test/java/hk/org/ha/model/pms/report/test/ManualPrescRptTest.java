package hk.org.ha.model.pms.report.test;

import hk.org.ha.model.pms.biz.report.ManualPrescRptServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;

import org.jboss.seam.mock.SeamTest;
import org.joda.time.DateTime;
import org.testng.annotations.Test;

public class ManualPrescRptTest extends SeamTest {
	
	@Test
	public void testManualPrescRptComponent() throws Exception {
	
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();

		new ComponentTest() {
			protected void testComponents() throws Exception {  
				DateTime dateFrom = new DateTime().withDate(2011, 6, 1);
				
				ManualPrescRptServiceLocal manualPrescRptService = (ManualPrescRptServiceLocal) getValue("#{manualPrescRptService}");
				manualPrescRptService.retrieveManualPrescSummaryList(dateFrom.toDate(), "123", false);
				
				assert manualPrescRptService.isRetrieveSuccess();
			}
		}.run();
		
		new ComponentTest() {
			protected void testComponents() throws Exception {  
				DateTime dateFrom = new DateTime().withDate(2011, 6, 1);
				
				ManualPrescRptServiceLocal manualPrescRptService = (ManualPrescRptServiceLocal) getValue("#{manualPrescRptService}");
				manualPrescRptService.retrieveManualPrescRptList(dateFrom.toDate(), "123", false);
				
				assert manualPrescRptService.isRetrieveSuccess();
			}
		}.run();
		
	}	
}
