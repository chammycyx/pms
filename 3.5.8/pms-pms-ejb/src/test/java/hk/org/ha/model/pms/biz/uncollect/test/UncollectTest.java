package hk.org.ha.model.pms.biz.uncollect.test;

import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.biz.uncollect.UncollectServiceLocal;
import hk.org.ha.model.pms.vo.uncollect.UncollectOrder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class UncollectTest extends SeamTest {

	private int uncollectOrderListSize;
	private Date ticketDate;
	
	@Test
	public void testUncollectComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		
		
		//retrieveUncollectList
		new ComponentTest() {
			
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				invokeMethod("#{uncollectListService.retrieveUncollectOrderList}");	
				List<UncollectOrder> uncollectOrderList = (List<UncollectOrder>)getValue("#{uncollectOrderList}");
				
				assert uncollectOrderList!=null;
				uncollectOrderListSize = uncollectOrderList.size();
			}
		}.run();
		
		//retrieveUncollectOrder
		new ComponentTest() {
			
			protected void testComponents() throws Exception {
				Calendar calendar = Calendar.getInstance();
				calendar.set(2011, 4, 22, 0, 0, 0);
				ticketDate = calendar.getTime();
				
				UncollectServiceLocal uncollectService = (UncollectServiceLocal) getInstance("uncollectService");
				uncollectService.retrieveUncollectOrder(ticketDate, "0501");	
				UncollectOrder uncollectOrder = (UncollectOrder)getValue("#{uncollectOrder}");
				
				assert uncollectOrder!=null;
				assert StringUtils.isNotBlank(uncollectOrder.getCaseNum());
				assert uncollectOrder.getDispOrderId() != null;
				assert uncollectOrder.getPharmOrderVersion() != null ;
				assert StringUtils.equals(uncollectOrder.getTicketNum(), "0501");
				assert StringUtils.equals(uncollectOrder.getReceiptNum(), "?");
				assert !uncollectOrder.isSfiFlag();
				assert !uncollectOrder.isWithinUncollectDuration();
			}
		}.run();
		
		//updateUncollectOrderList
		new ComponentTest() {
			
			protected void testComponents() throws Exception {
				UncollectServiceLocal uncollectService = (UncollectServiceLocal) getInstance("uncollectService");
				uncollectService.retrieveUncollectOrder(ticketDate, "0501");	
				UncollectOrder uncollectOrder = (UncollectOrder)getValue("#{uncollectOrder}");
				
				List<UncollectOrder> updateUncollectOrderList = new ArrayList<UncollectOrder>();
				updateUncollectOrderList.add(uncollectOrder);

				Contexts.getSessionContext().set("updateUncollectOrderList", updateUncollectOrderList);
				invokeMethod("#{uncollectListService.updateUncollectOrderList(updateUncollectOrderList)}");	
			}
		}.run();
		
		//check UncollectList
		new ComponentTest() {
			
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				invokeMethod("#{uncollectListService.retrieveUncollectOrderList}");	
				List<UncollectOrder> uncollectOrderList = (List<UncollectOrder>)getValue("#{uncollectOrderList}");
				
				assert uncollectOrderList!=null;
				assert uncollectOrderList.size() == uncollectOrderListSize+1;
			}
		}.run();
		
		//updateUncollectOrderList
		new ComponentTest() {
			
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				invokeMethod("#{uncollectListService.retrieveUncollectOrderList}");	
				List<UncollectOrder> uncollectOrderList = (List<UncollectOrder>)getValue("#{uncollectOrderList}");
				
				assert uncollectOrderList!=null;
				assert uncollectOrderList.size() == uncollectOrderListSize+1;

				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

				List<UncollectOrder> updateUncollectOrderList = new ArrayList<UncollectOrder>();
				
				for ( UncollectOrder uncollectOrder : uncollectOrderList ) {
					if (StringUtils.equals("0501", uncollectOrder.getTicketNum()) && 
							StringUtils.equals(dateFormat.format(uncollectOrder.getTicketDate()), dateFormat.format(ticketDate))) {
						uncollectOrder.setMarkDelete(true);
						updateUncollectOrderList.add(uncollectOrder);
						break;
					}
				}

				Contexts.getSessionContext().set("updateUncollectOrderList", updateUncollectOrderList);
				invokeMethod("#{uncollectListService.updateUncollectOrderList(updateUncollectOrderList)}");	
			}
		}.run();
		
		//check UncollectList
		new ComponentTest() {
			
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				invokeMethod("#{uncollectListService.retrieveUncollectOrderList}");	
				List<UncollectOrder> uncollectOrderList = (List<UncollectOrder>)getValue("#{uncollectOrderList}");
				
				assert uncollectOrderList!=null;
				assert uncollectOrderList.size() == uncollectOrderListSize;
			}
		}.run();
		
	}	
}
