package hk.org.ha.model.pms.biz.checkissue.test;

import hk.org.ha.model.pms.biz.checkissue.LedOperationListServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.disp.DispOrder;

import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class LedOperationTest extends SeamTest {

	@Test
	public void testLedOperationComponent() throws Exception {
		

		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		// retrieve issue window list
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {   
				LedOperationListServiceLocal ledOperationService = (LedOperationListServiceLocal) getValue("#{ledOperationService}");
				ledOperationService.retrieveIssueWindowList();
				List<Integer> issueWindowList = (List<Integer>) getValue("#{issueWindowList}");
				assert issueWindowList.size()>0;
			}
		}.run();
		
		// retrieve Led list
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {   
				LedOperationListServiceLocal ledOperationService = (LedOperationListServiceLocal) getValue("#{ledOperationService}");
				ledOperationService.retrieveLedList();
				List<DispOrder> ledList = (List<DispOrder>) getValue("#{ledList}");
				assert ledList.size()==0;
			}
		}.run();
		
		
	}	
}
