package hk.org.ha.model.pms.report.test;

import hk.org.ha.model.pms.biz.report.DelItemRptServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;

import org.jboss.seam.mock.SeamTest;
import org.joda.time.DateTime;
import org.testng.annotations.Test;

public class DelItemRptTest extends SeamTest {
	
	@Test
	public void testDelItemRptComponent() throws Exception {
	
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();

	/*	// retrieve DelDispItemRpt list
		new ComponentTest() {
			protected void testComponents() throws Exception {  
				DateTime dispDate = new DateTime("2011-09-27");
				
				DelItemRptServiceLocal delItemRptService = (DelItemRptServiceLocal) getValue("#{delItemRptService}");
				delItemRptService.retrieveDelItemRpt(0,"WKS1", dispDate.toDate());
				
				assert delItemRptService.getReportObj().size()>0;
			}
		}.run();

		// retrieve DelCapdVoucherRpt list
		new ComponentTest() {
			protected void testComponents() throws Exception {  
				DateTime dispDate = new DateTime("2011-09-27");
				
				DelItemRptServiceLocal delItemRptService = (DelItemRptServiceLocal) getValue("#{delItemRptService}");
				delItemRptService.retrieveDelItemRpt(1,"WKS1", dispDate.toDate());
				
				assert delItemRptService.getReportObj().size()>0;
			}
		}.run();*/
		
	}	
}
