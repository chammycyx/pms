package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.reftable.LocalWarningListServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.reftable.LocalWarning;
import hk.org.ha.model.pms.vo.report.LocalWarningRpt;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class LocalWarningListTest extends SeamTest {

	@Test
	public void testLocalWarningListComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		//retrieve localWarning
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{localWarningListService.retrieveLocalWarningList('PARA01')}");
				List<LocalWarning> localWarningList = (List<LocalWarning>) getValue("#{localWarningList}");
				
				assert localWarningList.size()>0;

				LocalWarningListServiceLocal localWarningListService = (LocalWarningListServiceLocal) getInstance("localWarningListService");
				assert localWarningListService.isValidItemCode();
			}
		}.run();
		
		//add localWarning List
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				List<LocalWarning> localWarningList = new ArrayList<LocalWarning>();
				
				LocalWarning localWarning = new LocalWarning();
				localWarning.setItemCode("ACET32");
				localWarning.setSortSeq(1);
				localWarning.setWarnCode("7B");
				localWarning.setOrgWarnCode("23");
				
				localWarningList.add(localWarning);

				Contexts.getSessionContext().set("localWarningList", localWarningList);
				invokeMethod("#{localWarningListService.updateLocalWarningList(localWarningList)}");
			}
		}.run();
		
		//check localWarning List
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{localWarningListService.retrieveLocalWarningList('ACET32')}");
				List<LocalWarning> localWarningList = (List<LocalWarning>) getValue("#{localWarningList}");
				
				assert localWarningList.size()>0;
				
				for (LocalWarning localWarning:localWarningList) {
					assert StringUtils.equals("ACET32", localWarning.getItemCode());
					assert localWarning.getSortSeq() == 1;
					assert StringUtils.equals("7B", localWarning.getWarnCode());
					assert StringUtils.equals("23", localWarning.getOrgWarnCode());
				}

			}
		}.run();
		
		//update localWarning List
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{localWarningListService.retrieveLocalWarningList('ACET32')}");
				List<LocalWarning> localWarningList = (List<LocalWarning>) getValue("#{localWarningList}");
				
				assert localWarningList.size()>0;
				

				for (LocalWarning localWarning:localWarningList) {
					localWarning.setSortSeq(1);
					localWarning.setWarnCode("73");
					localWarning.setOrgWarnCode("23");
				}
				

				Contexts.getSessionContext().set("updateLocalWarningList", localWarningList);
				invokeMethod("#{localWarningListService.updateLocalWarningList(updateLocalWarningList)}");
			}
		}.run();
		
		//check localWarning List
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{localWarningListService.retrieveLocalWarningList('ACET32')}");
				List<LocalWarning> localWarningList = (List<LocalWarning>) getValue("#{localWarningList}");
				
				assert localWarningList.size()>0;
				
				for (LocalWarning localWarning:localWarningList) {
					assert StringUtils.equals("ACET32", localWarning.getItemCode());
					assert localWarning.getSortSeq() == 1;
					assert StringUtils.equals("73", localWarning.getWarnCode());
					assert StringUtils.equals("23", localWarning.getOrgWarnCode());
				}

			}
		}.run();
		
		//delete localWarning List
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{localWarningListService.retrieveLocalWarningList('ACET32')}");
				List<LocalWarning> localWarningList = (List<LocalWarning>) getValue("#{localWarningList}");
				
				assert localWarningList.size()>0;
				

				for (LocalWarning localWarning:localWarningList) {
					localWarning.setSortSeq(1);
					localWarning.setWarnCode("");
					localWarning.setOrgWarnCode("23");
				}
				

				Contexts.getSessionContext().set("updateLocalWarningList", localWarningList);
				invokeMethod("#{localWarningListService.updateLocalWarningList(updateLocalWarningList)}");
			}
		}.run();
		
		//check localWarning List
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{localWarningListService.retrieveLocalWarningList('ACET32')}");
				List<LocalWarning> localWarningList = (List<LocalWarning>) getValue("#{localWarningList}");

				assert localWarningList.size()==1;
			}
		}.run();
		
		//retrieve followUp itemCode
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{				
				invokeMethod("#{localWarningListService.retrieveLocalWarningList('ACET01')}");
				
				LocalWarningListServiceLocal localWarningListService = (LocalWarningListServiceLocal) getInstance("localWarningListService");
				assert localWarningListService.isFollowUp();
			}
		}.run();

		//invalidItemCode
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{				
				invokeMethod("#{localWarningListService.retrieveLocalWarningList('')}");
				
				LocalWarningListServiceLocal localWarningListService = (LocalWarningListServiceLocal) getInstance("localWarningListService");
				assert !localWarningListService.isValidItemCode();
			}
		}.run();
		
		//retrieve localWarningRpt List
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				List<LocalWarningRpt> LocalWarningRptList = (List<LocalWarningRpt>) invokeMethod("#{localWarningListService.retrieveLocalWarningRptList()}");
				
				assert LocalWarningRptList.size()>0;
			}
		}.run();
	}	
}
