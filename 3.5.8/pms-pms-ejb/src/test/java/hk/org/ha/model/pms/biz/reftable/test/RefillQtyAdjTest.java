package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.reftable.RefillQtyAdjExclusionListServiceLocal;
import hk.org.ha.model.pms.biz.reftable.RefillQtyAdjListServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.reftable.RefillQtyAdj;
import hk.org.ha.model.pms.persistence.reftable.RefillQtyAdjExclusion;

import java.util.ArrayList;
import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class RefillQtyAdjTest extends SeamTest {

	@Test
	public void testRefillQtyAdjComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		// retrieve RefillQtyAdj and RefillQtyAdjExclusion List
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				RefillQtyAdjListServiceLocal refillQtyAdjListService = (RefillQtyAdjListServiceLocal)getValue("#{refillQtyAdjListService}");
				List<RefillQtyAdj> refillQtyAdjList = refillQtyAdjListService.retrieveRefillQtyAdjList();
				
				assert refillQtyAdjList!=null;
			}
		}.run();
		
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				RefillQtyAdjExclusionListServiceLocal refillQtyAdjExclusionListService = (RefillQtyAdjExclusionListServiceLocal)getValue("#{refillQtyAdjExclusionListService}");
				List<RefillQtyAdjExclusion> refillQtyAdjExclusionList = refillQtyAdjExclusionListService.retrieveRefillQtyAdjExclusionList();
				
				assert refillQtyAdjExclusionList!=null;
			}
		}.run();
		
		//add RefillQtyAdj Item
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				RefillQtyAdjListServiceLocal refillQtyAdjListService = (RefillQtyAdjListServiceLocal)getValue("#{refillQtyAdjListService}");
				List<RefillQtyAdj> refillQtyAdjList = refillQtyAdjListService.retrieveRefillQtyAdjList();
				
				assert refillQtyAdjList!=null;
				
				RefillQtyAdj refillQtyAdj = new RefillQtyAdj();
				refillQtyAdj.setFormCode("ACA");
				refillQtyAdjList.add(refillQtyAdj);				
				
				refillQtyAdjListService.updateRefillQtyAdjList(refillQtyAdjList, new ArrayList<RefillQtyAdj>());
				
			}
		}.run();

		//check RefillQtyAdj List
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				RefillQtyAdjListServiceLocal refillQtyAdjListService = (RefillQtyAdjListServiceLocal)getValue("#{refillQtyAdjListService}");
				List<RefillQtyAdj> refillQtyAdjList = refillQtyAdjListService.retrieveRefillQtyAdjList();
				
				assert refillQtyAdjList!=null;

				boolean checkRefillQtyAdj = false;
				for(RefillQtyAdj refillQtyAdj:refillQtyAdjList)
				{
					if(refillQtyAdj.getFormCode().equals("ACA"))
					{
						checkRefillQtyAdj = true;
					}
				}
				
				assert checkRefillQtyAdj;
			}
		}.run();
		
		//add RefillQtyAdjExclusion Item
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				RefillQtyAdjExclusionListServiceLocal refillQtyAdjExclusionListService = (RefillQtyAdjExclusionListServiceLocal)getValue("#{refillQtyAdjExclusionListService}");
				List<RefillQtyAdjExclusion> refillQtyAdjExclusionList = refillQtyAdjExclusionListService.retrieveRefillQtyAdjExclusionList();
				
				assert refillQtyAdjExclusionList!=null;

				RefillQtyAdjExclusion refillQtyAdjExclusion = new RefillQtyAdjExclusion();
				refillQtyAdjExclusion.setItemCode("SALI31");
				refillQtyAdjExclusionList.add(refillQtyAdjExclusion);
				
				refillQtyAdjExclusionListService.updateRefillQtyAdjExclusionList(refillQtyAdjExclusionList, new ArrayList<RefillQtyAdjExclusion>());
			}
		}.run();
		
		//check RefillQtyAdjExclusion List
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				RefillQtyAdjExclusionListServiceLocal refillQtyAdjExclusionListService = (RefillQtyAdjExclusionListServiceLocal)getValue("#{refillQtyAdjExclusionListService}");
				List<RefillQtyAdjExclusion> refillQtyAdjExclusionList = refillQtyAdjExclusionListService.retrieveRefillQtyAdjExclusionList();
				
				assert refillQtyAdjExclusionList!=null;

				boolean checkRefillQtyAdjExclusion = false;
				for(RefillQtyAdjExclusion refillQtyAdjExclusion:refillQtyAdjExclusionList)
				{
					if(refillQtyAdjExclusion.getItemCode().equals("SALI31"))
					{
						checkRefillQtyAdjExclusion = true;
					}
				}
				
				assert checkRefillQtyAdjExclusion;
			}
		}.run();
		
		//delete RefillQtyAdj List
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				RefillQtyAdjListServiceLocal refillQtyAdjListService = (RefillQtyAdjListServiceLocal)getValue("#{refillQtyAdjListService}");
				List<RefillQtyAdj> refillQtyAdjList = refillQtyAdjListService.retrieveRefillQtyAdjList();
				
				assert refillQtyAdjList!=null;
				
				List<RefillQtyAdj> delRefillQtyAdjList = new ArrayList<RefillQtyAdj>();
				
				for(RefillQtyAdj refillQtyAdj:refillQtyAdjList)
				{
					if(refillQtyAdj.getFormCode().equals("ACA"))
					{
						delRefillQtyAdjList.add(refillQtyAdj);
					}
				}
				
				refillQtyAdjListService.updateRefillQtyAdjList(refillQtyAdjList, delRefillQtyAdjList);
			}
		}.run();
		
		//check RefillQtyAdj List
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				RefillQtyAdjListServiceLocal refillQtyAdjListService = (RefillQtyAdjListServiceLocal)getValue("#{refillQtyAdjListService}");
				List<RefillQtyAdj> refillQtyAdjList = refillQtyAdjListService.retrieveRefillQtyAdjList();
				
				assert refillQtyAdjList!=null;

				for(RefillQtyAdj refillQtyAdj:refillQtyAdjList)
				{
					if(refillQtyAdj.getFormCode().equals("ACA"))
					{
						assert false;
					}
				}
			}
		}.run();
		
		//delete RefillQtyAdjExclusion List
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				RefillQtyAdjExclusionListServiceLocal refillQtyAdjExclusionListService = (RefillQtyAdjExclusionListServiceLocal)getValue("#{refillQtyAdjExclusionListService}");
				List<RefillQtyAdjExclusion> refillQtyAdjExclusionList = refillQtyAdjExclusionListService.retrieveRefillQtyAdjExclusionList();
				
				assert refillQtyAdjExclusionList!=null;
				
				List<RefillQtyAdjExclusion> delRefillQtyAdjExclusionList = new ArrayList<RefillQtyAdjExclusion>();

				for(RefillQtyAdjExclusion refillQtyAdjExclusion:refillQtyAdjExclusionList)
				{
					if(refillQtyAdjExclusion.getItemCode().equals("SALI31"))
					{
						delRefillQtyAdjExclusionList.add(refillQtyAdjExclusion);
					}
				}
				refillQtyAdjExclusionListService.updateRefillQtyAdjExclusionList(refillQtyAdjExclusionList, delRefillQtyAdjExclusionList);
			}
		}.run();
		
		//check RefillQtyAdjExclusion List
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				RefillQtyAdjExclusionListServiceLocal refillQtyAdjExclusionListService = (RefillQtyAdjExclusionListServiceLocal)getValue("#{refillQtyAdjExclusionListService}");
				List<RefillQtyAdjExclusion> refillQtyAdjExclusionList = refillQtyAdjExclusionListService.retrieveRefillQtyAdjExclusionList();
				
				assert refillQtyAdjExclusionList!=null;

				for(RefillQtyAdjExclusion refillQtyAdjExclusion:refillQtyAdjExclusionList)
				{
					if(refillQtyAdjExclusion.getItemCode().equals("SALI31"))
					{
						assert false;
					}
				}
			}
		}.run();
	}	
}
