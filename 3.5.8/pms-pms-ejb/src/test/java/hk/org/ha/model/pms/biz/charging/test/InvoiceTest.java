package hk.org.ha.model.pms.biz.charging.test;

import hk.org.ha.model.pms.biz.charging.InvoiceManagerLocal;
import hk.org.ha.model.pms.biz.order.DispOrderManagerLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.InvoiceItem;
import hk.org.ha.model.pms.udt.disp.InvoiceDocType;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.vo.charging.SaveInvoiceListInfo;
import hk.org.ha.model.pms.vo.onestop.OneStopOrderInfo;
import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class InvoiceTest extends SeamTest {

	private Long invoiceId;
	
	//@Test
	public void testInvoiceComponent() throws Exception {
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
	            
	            OneStopOrderInfo oneStopOrderInfo = new OneStopOrderInfo();
	            oneStopOrderInfo.setStandardForceProceed(true);
	            oneStopOrderInfo.setReceiptNum(null);	            
	            Contexts.getSessionContext().set("oneStopOrderInfo", oneStopOrderInfo);
	            
	            
	            
			}
		}.run();
		
		// Add New Invoice
		new ComponentTest() {
			protected void testComponents() throws Exception {
				invokeMethod("#{dispOrderManager.retrieveDispOrder(1)}");
				DispOrder dispOrder = (DispOrder) getValue("#{dispOrder}");

				invokeMethod("#{invoiceManager.createInvoice}");
				Invoice invoice = (Invoice) getValue("#{invoice}");
				invoice.setStatus(InvoiceStatus.Outstanding);
				invoice.setDispOrder(dispOrder);
				invoice.setForceProceedFlag(false);
				invokeMethod("#{invoiceManager.updateInvoice}");
				invoiceId = (Long) getValue("#{invoice.getId()}");
			}
		}.run();
		
		// Check Inovice
		new ComponentTest() {
			protected void testComponents() throws Exception {
				invokeMethod("#{invoiceManager.retrieveInvoice("+invoiceId+")}");
				Invoice invoice = (Invoice) getValue("#{invoice}");

				assert invoice.getId().equals(invoiceId);
				assert invoice.getStatus() == InvoiceStatus.Outstanding;
			}
		}.run();
		
		// Edit Invoice
		new ComponentTest() {
			protected void testComponents() throws Exception {
				invokeMethod("#{invoiceManager.retrieveInvoice("+invoiceId+")}");
				Invoice invoice = (Invoice) getValue("#{invoice}");

				invoice.setStatus(InvoiceStatus.Settled);
				invokeMethod("#{invoiceManager.updateInvoice}");
			}
		}.run();
		
		// Check Invoice
		new ComponentTest() {
			protected void testComponents() throws Exception {
				invokeMethod("#{invoiceManager.retrieveInvoice("+invoiceId+")}");
				Invoice invoice = (Invoice) getValue("#{invoice}");

				assert invoice.getId().equals(invoiceId);
				assert invoice.getStatus() == InvoiceStatus.Settled;
			}
		}.run();
		
		// Delete Invoice
		new ComponentTest() {
			protected void testComponents() throws Exception {
				invokeMethod("#{invoiceManager.deleteInvoice(invoice)}");
			}
		}.run();
		
		// Check Invoice
		new ComponentTest() {
			protected void testComponents() throws Exception {
				invokeMethod("#{invoiceManager.retrieveInvoice("+invoiceId+")}");
				Invoice invoice = (Invoice) getValue("#{invoice}");
				assert invoice == null;
			}
		}.run();
	}
		
	//CreateInvoiceList - Public PatType
	//@Test
	public void testCreateInvoiceListForPublicComponent() throws Exception {
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
	            
	            OneStopOrderInfo oneStopOrderInfo = new OneStopOrderInfo();
	            oneStopOrderInfo.setStandardForceProceed(true);
	            oneStopOrderInfo.setReceiptNum(null);	            
	            Contexts.getSessionContext().set("oneStopOrderInfo", oneStopOrderInfo);		            
			}
		}.run();
		
		new ComponentTest() {
			protected void testComponents() throws Exception {

				DispOrderManagerLocal dispOrderManager = (DispOrderManagerLocal) getValue("#{dispOrderManager}");
				DispOrder dispOrder = dispOrderManager.retrieveDispOrderById(Long.valueOf(38));				
				
				for (DispOrderItem dispOrderItem:dispOrder.getDispOrderItemList()){
					assert dispOrderItem.getPharmOrderItem() != null;
				}
				
				OrderViewInfo inOrderViewInfo = new OrderViewInfo();
				InvoiceManagerLocal invoiceManager = (InvoiceManagerLocal) getValue("#{invoiceManager}");
				SaveInvoiceListInfo saveInvoiceListInfo = invoiceManager.createInvoiceListFromDispOrder(inOrderViewInfo, dispOrder, new ArrayList<Invoice>()); 	
				assert saveInvoiceListInfo != null;
				List<Invoice> invoiceList = saveInvoiceListInfo.getInvoiceList();
				assert invoiceList.size() == 2;
				
				for( Invoice inv : invoiceList ){
					invokeMethod("#{invoiceManager.retrieveInvoice("+inv.getId()+")}");
					Invoice invoice = (Invoice) getValue("#{invoice}");
					
					if( InvoiceDocType.Sfi.equals(invoice.getDocType()) ){
						assert invoice != null;
						assert invoice.getInvoiceItemList().size() == 3;
						
						for ( InvoiceItem invoiceItem : invoice.getInvoiceItemList() ){					
							
							if( "OLAN01".equals( invoiceItem.getDispOrderItem().getPharmOrderItem().getItemCode() ) ){
								assert invoiceItem.getCost().compareTo(BigDecimal.valueOf(32.479)) == 0;
								assert invoiceItem.getMarkupAmount().compareTo(BigDecimal.valueOf(1.0)) == 0;								
								assert invoiceItem.getHandleAmount().compareTo(BigDecimal.valueOf(50)) == 0;
								assert invoiceItem.getAmount().compareTo(BigDecimal.valueOf(83)) == 0;
							}
							
							if( "ETAN04".equals( invoiceItem.getDispOrderItem().getPharmOrderItem().getItemCode() ) ){
								assert invoiceItem.getCost().compareTo(BigDecimal.valueOf(1061.9407)) == 0;
								assert invoiceItem.getMarkupAmount().compareTo(BigDecimal.ZERO) == 0;
								assert invoiceItem.getHandleAmount().compareTo(BigDecimal.ZERO) == 0;
								assert invoiceItem.getAmount().compareTo(BigDecimal.valueOf(1062)) == 0;
							}
							
							if( "S00682".equals( invoiceItem.getDispOrderItem().getPharmOrderItem().getItemCode() ) ){
								assert invoiceItem.getCost().compareTo(BigDecimal.ZERO) == 0;
								assert invoiceItem.getMarkupAmount().compareTo(BigDecimal.ZERO) == 0;
								assert invoiceItem.getHandleAmount().compareTo(BigDecimal.ZERO) == 0;
								assert invoiceItem.getAmount().compareTo(BigDecimal.ZERO) == 0;
							}
						}
						assert invoice.getTotalAmount().compareTo(BigDecimal.valueOf(1145)) == 0;

					}else if( InvoiceDocType.Standard.equals(invoice.getDocType()) ){

						assert invoice != null;
						assert invoice.getInvoiceItemList().size() == 1;
						assert invoice.getTotalAmount().compareTo(BigDecimal.TEN) == 0;						
					}
				}			
			}
		}.run();
	}
	
	
	//CreateInvoiceList - Private PatType
	@Test
	public void testCreateInvoiceListForPrivateComponent() throws Exception {
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
	            
	            OneStopOrderInfo oneStopOrderInfo = new OneStopOrderInfo();
	            oneStopOrderInfo.setStandardForceProceed(true);
	            oneStopOrderInfo.setReceiptNum(null);	            
	            Contexts.getSessionContext().set("oneStopOrderInfo", oneStopOrderInfo);	
			}
		}.run();
		
		new ComponentTest() {
			protected void testComponents() throws Exception {
				DispOrderManagerLocal dispOrderManager = (DispOrderManagerLocal) getValue("#{dispOrderManager}");
				DispOrder dispOrder = dispOrderManager.retrieveDispOrderById(Long.valueOf(39));
				
				for (DispOrderItem dispOrderItem:dispOrder.getDispOrderItemList()){
					assert dispOrderItem.getPharmOrderItem() != null;
				}
				
				OrderViewInfo inOrderViewInfo = new OrderViewInfo();
				InvoiceManagerLocal invoiceManager = (InvoiceManagerLocal) getValue("#{invoiceManager}");
				SaveInvoiceListInfo saveInvoiceListInfo = invoiceManager.createInvoiceListFromDispOrder(inOrderViewInfo, dispOrder, new ArrayList<Invoice>()); 	
				assert saveInvoiceListInfo != null;
				List<Invoice> invoiceList = saveInvoiceListInfo.getInvoiceList();
				assert invoiceList.size() == 1;	
				
				for( Invoice inv : invoiceList ){
					invokeMethod("#{invoiceManager.retrieveInvoice("+inv.getId()+")}");
					Invoice invoice = (Invoice) getValue("#{invoice}");
					
						assert InvoiceDocType.Sfi.equals(invoice.getDocType());
						assert invoice != null;
						assert invoice.getInvoiceItemList().size() == 4;
						
						for ( InvoiceItem invoiceItem : invoice.getInvoiceItemList() ){					
							
							if( "OLAN01".equals( invoiceItem.getDispOrderItem().getPharmOrderItem().getItemCode() ) ){
								assert invoiceItem.getCost().compareTo(BigDecimal.valueOf(32.479)) == 0;
								assert invoiceItem.getMarkupAmount().compareTo(BigDecimal.valueOf(50)) == 0;
								//assert invoiceItem.getMarkupAmount().compareTo(BigDecimal.valueOf(17)) == 0;								
								assert invoiceItem.getHandleAmount().compareTo(BigDecimal.valueOf(50)) == 0;
								//assert invoiceItem.getAmount().compareTo(BigDecimal.valueOf(99)) == 0;
								assert invoiceItem.getAmount().compareTo(BigDecimal.valueOf(132)) == 0;
							}
							
							if( "ETAN04".equals( invoiceItem.getDispOrderItem().getPharmOrderItem().getItemCode() ) ){
								assert invoiceItem.getCost().compareTo(BigDecimal.valueOf(1061.9407)) == 0;
								assert invoiceItem.getMarkupAmount().compareTo(BigDecimal.ZERO) == 0;
								assert invoiceItem.getHandleAmount().compareTo(BigDecimal.valueOf(50)) == 0;
								assert invoiceItem.getAmount().compareTo(BigDecimal.valueOf(1112)) == 0;
							}
							
							if( "S00682".equals( invoiceItem.getDispOrderItem().getPharmOrderItem().getItemCode() ) ){
								assert invoiceItem.getCost().compareTo(BigDecimal.ZERO) == 0;
								assert invoiceItem.getMarkupAmount().compareTo(BigDecimal.ZERO) == 0;
								assert invoiceItem.getHandleAmount().compareTo(BigDecimal.valueOf(50)) == 0;
								assert invoiceItem.getAmount().compareTo(BigDecimal.valueOf(50)) == 0;
							}
							
							if( "PARA01".equals( invoiceItem.getDispOrderItem().getPharmOrderItem().getItemCode() ) ){
								assert invoiceItem.getCost().compareTo(BigDecimal.valueOf(0.0225)) == 0;
								//assert invoiceItem.getMarkupAmount().compareTo(BigDecimal.valueOf(0)) == 0;
								assert invoiceItem.getMarkupAmount().compareTo(BigDecimal.valueOf(50)) == 0;
								assert invoiceItem.getHandleAmount().compareTo(BigDecimal.valueOf(50)) == 0;
								//assert invoiceItem.getAmount().compareTo(BigDecimal.valueOf(50)) == 0;
								assert invoiceItem.getAmount().compareTo(BigDecimal.valueOf(100)) == 0;
							}
						}
						//assert invoice.getTotalAmount().compareTo(BigDecimal.valueOf(1311)) == 0;
						assert invoice.getTotalAmount().compareTo(BigDecimal.valueOf(1394)) == 0;
				}			
			}
		}.run();
	}
	
	//CreateInvoiceList - NEP PatType with EYE Specialty
	@Test
	public void testCreateInvoiceListForNepSpecComponent() throws Exception {
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
	            
	            OneStopOrderInfo oneStopOrderInfo = new OneStopOrderInfo();
	            oneStopOrderInfo.setStandardForceProceed(true);
	            oneStopOrderInfo.setReceiptNum(null);	            
	            Contexts.getSessionContext().set("oneStopOrderInfo", oneStopOrderInfo);	
			}
		}.run();
		
		new ComponentTest() {
			protected void testComponents() throws Exception {
				DispOrderManagerLocal dispOrderManager = (DispOrderManagerLocal) getValue("#{dispOrderManager}");
				DispOrder dispOrder = dispOrderManager.retrieveDispOrderById(Long.valueOf(40));
				
				for (DispOrderItem dispOrderItem:dispOrder.getDispOrderItemList()){
					assert dispOrderItem.getPharmOrderItem() != null;
				}
				
				OrderViewInfo inOrderViewInfo = new OrderViewInfo();
				InvoiceManagerLocal invoiceManager = (InvoiceManagerLocal) getValue("#{invoiceManager}");
				SaveInvoiceListInfo saveInvoiceListInfo = invoiceManager.createInvoiceListFromDispOrder(inOrderViewInfo, dispOrder, new ArrayList<Invoice>()); 	
				assert saveInvoiceListInfo != null;
				List<Invoice> invoiceList = saveInvoiceListInfo.getInvoiceList();
				assert invoiceList.size() == 1;	
				
				for( Invoice inv : invoiceList ){
					invokeMethod("#{invoiceManager.retrieveInvoice("+inv.getId()+")}");
					Invoice invoice = (Invoice) getValue("#{invoice}");
					
						assert InvoiceDocType.Sfi.equals(invoice.getDocType());
						assert invoice != null;
						assert invoice.getInvoiceItemList().size() == 4;
						
						for ( InvoiceItem invoiceItem : invoice.getInvoiceItemList() ){					
							
							if( "OLAN01".equals( invoiceItem.getDispOrderItem().getPharmOrderItem().getItemCode() ) ){
								assert invoiceItem.getCost().compareTo(BigDecimal.valueOf(32.479)) == 0;
								//Without LowerProfit assert invoiceItem.getMarkupAmount().compareTo(BigDecimal.valueOf(17)) == 0;								
								assert invoiceItem.getMarkupAmount().compareTo(BigDecimal.valueOf(50)) == 0;
								assert invoiceItem.getHandleAmount().compareTo(BigDecimal.valueOf(50)) == 0;
								assert invoiceItem.getAmount().compareTo(BigDecimal.valueOf(132)) == 0;
								//Without LowerProfit assert invoiceItem.getAmount().compareTo(BigDecimal.valueOf(99)) == 0;
							}
							
							if( "ETAN04".equals( invoiceItem.getDispOrderItem().getPharmOrderItem().getItemCode() ) ){
								assert invoiceItem.getCost().compareTo(BigDecimal.valueOf(1061.9407)) == 0;
								assert invoiceItem.getMarkupAmount().compareTo(BigDecimal.ZERO) == 0;
								assert invoiceItem.getHandleAmount().compareTo(BigDecimal.valueOf(50)) == 0;
								assert invoiceItem.getAmount().compareTo(BigDecimal.valueOf(1112)) == 0;
							}
							
							if( "S00682".equals( invoiceItem.getDispOrderItem().getPharmOrderItem().getItemCode() ) ){
								assert invoiceItem.getCost().compareTo(BigDecimal.ZERO) == 0;
								assert invoiceItem.getMarkupAmount().compareTo(BigDecimal.ZERO) == 0;
								assert invoiceItem.getHandleAmount().compareTo(BigDecimal.valueOf(50)) == 0;
								assert invoiceItem.getAmount().compareTo(BigDecimal.valueOf(50)) == 0;
							}
							
							if( "PARA01".equals( invoiceItem.getDispOrderItem().getPharmOrderItem().getItemCode() ) ){
								assert invoiceItem.getCost().compareTo(BigDecimal.valueOf(0.0225)) == 0;
								//assert invoiceItem.getMarkupAmount().compareTo(BigDecimal.valueOf(0)) == 0;								
								assert invoiceItem.getMarkupAmount().compareTo(BigDecimal.valueOf(50)) == 0;
								assert invoiceItem.getHandleAmount().compareTo(BigDecimal.valueOf(50)) == 0;
								//assert invoiceItem.getAmount().compareTo(BigDecimal.valueOf(50)) == 0;
								assert invoiceItem.getAmount().compareTo(BigDecimal.valueOf(100)) == 0;
							}
						}
						//assert invoice.getTotalAmount().compareTo(BigDecimal.valueOf(1311)) == 0;
						assert invoice.getTotalAmount().compareTo(BigDecimal.valueOf(1394)) == 0;
				}
			}
		}.run();
	}
	
	//CreateInvoiceList - NEP PatType
	//@Test
	public void testCreateInvoiceListForNepComponent() throws Exception {
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
	            
	            OneStopOrderInfo oneStopOrderInfo = new OneStopOrderInfo();
	            oneStopOrderInfo.setStandardForceProceed(true);
	            oneStopOrderInfo.setReceiptNum(null);	            
	            Contexts.getSessionContext().set("oneStopOrderInfo", oneStopOrderInfo);	
			}
		}.run();
		
		new ComponentTest() {
			protected void testComponents() throws Exception {
				DispOrderManagerLocal dispOrderManager = (DispOrderManagerLocal) getValue("#{dispOrderManager}");
				DispOrder dispOrder = dispOrderManager.retrieveDispOrderById(Long.valueOf(41));
				
				for (DispOrderItem dispOrderItem:dispOrder.getDispOrderItemList()){
					assert dispOrderItem.getPharmOrderItem() != null;
				}
				
				OrderViewInfo inOrderViewInfo = new OrderViewInfo();
				InvoiceManagerLocal invoiceManager = (InvoiceManagerLocal) getValue("#{invoiceManager}");
				SaveInvoiceListInfo saveInvoiceListInfo = invoiceManager.createInvoiceListFromDispOrder(inOrderViewInfo, dispOrder, new ArrayList<Invoice>()); 	
				assert saveInvoiceListInfo != null;
				List<Invoice> invoiceList = saveInvoiceListInfo.getInvoiceList();
				assert invoiceList.size() == 2;	
				
				for( Invoice inv : invoiceList ){
					invokeMethod("#{invoiceManager.retrieveInvoice("+inv.getId()+")}");
					Invoice invoice = (Invoice) getValue("#{invoice}");
					
					if( InvoiceDocType.Sfi.equals(invoice.getDocType()) ){
						assert invoice != null;
						assert invoice.getInvoiceItemList().size() == 3;
						
						for ( InvoiceItem invoiceItem : invoice.getInvoiceItemList() ){					
							
							if( "OLAN01".equals( invoiceItem.getDispOrderItem().getPharmOrderItem().getItemCode() ) ){
								assert invoiceItem.getCost().compareTo(BigDecimal.valueOf(32.479)) == 0;
								//Without LowerProfit assert invoiceItem.getMarkupAmount().compareTo(BigDecimal.valueOf(17)) == 0;
								assert invoiceItem.getMarkupAmount().compareTo(BigDecimal.valueOf(50)) == 0;
								assert invoiceItem.getHandleAmount().compareTo(BigDecimal.valueOf(50)) == 0;
								//Without LowerProfit assert invoiceItem.getAmount().compareTo(BigDecimal.valueOf(99)) == 0;
								assert invoiceItem.getAmount().compareTo(BigDecimal.valueOf(132)) == 0;
							}
							
							if( "ETAN04".equals( invoiceItem.getDispOrderItem().getPharmOrderItem().getItemCode() ) ){
								assert invoiceItem.getCost().compareTo(BigDecimal.valueOf(1061.9407)) == 0;
								assert invoiceItem.getMarkupAmount().compareTo(BigDecimal.ZERO) == 0;
								assert invoiceItem.getHandleAmount().compareTo(BigDecimal.valueOf(50)) == 0;
								assert invoiceItem.getAmount().compareTo(BigDecimal.valueOf(1112)) == 0;
							}
							
							if( "S00682".equals( invoiceItem.getDispOrderItem().getPharmOrderItem().getItemCode() ) ){
								assert invoiceItem.getCost().compareTo(BigDecimal.ZERO) == 0;
								assert invoiceItem.getMarkupAmount().compareTo(BigDecimal.ZERO) == 0;
								assert invoiceItem.getHandleAmount().compareTo(BigDecimal.ZERO) == 0;
								assert invoiceItem.getAmount().compareTo(BigDecimal.ZERO) == 0;
							}
						}
						//assert invoice.getTotalAmount().compareTo(new BigDecimal(1211)) == 0;
						assert invoice.getTotalAmount().compareTo(new BigDecimal(1244)) == 0;

					}else if( InvoiceDocType.Standard.equals(invoice.getDocType()) ){
						assert invoice != null;
						assert invoice.getInvoiceItemList().size() == 1;
						assert invoice.getTotalAmount().compareTo(BigDecimal.TEN) == 0;						
					}
				}
			}
		}.run();
	}
	
}

