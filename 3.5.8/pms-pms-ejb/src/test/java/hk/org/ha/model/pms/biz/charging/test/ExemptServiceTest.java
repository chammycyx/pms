package hk.org.ha.model.pms.biz.charging.test;

import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.model.pms.biz.charging.ExemptServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.vo.charging.HandleExemptAuthInfo;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class ExemptServiceTest extends SeamTest {

	@Test
	public void testValidateExemptChargeReasonComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");	
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
	            UamInfo uamInfo = new UamInfo();
	            uamInfo.setUserRole("TESTCASE");
	            Contexts.getSessionContext().set("uamInfo", uamInfo);
			}
		}.run();
		
		//Success
		new ComponentTest() 
		{			
			protected void testComponents() throws Exception 
			{
				ExemptServiceLocal exemptService = (ExemptServiceLocal) getValue("#{exemptService}");
				HandleExemptAuthInfo handleExemptAuthInfoIn = new HandleExemptAuthInfo();				
				handleExemptAuthInfoIn.setExemptAuthBy("itdSfiExempt");
				handleExemptAuthInfoIn.setExemptAuthPw("XXXZZZ");				
				HandleExemptAuthInfo handleExemptAuthInfo = exemptService.validateHandleExemptAuthInfo(handleExemptAuthInfoIn);
				assert handleExemptAuthInfo.getExemptAuthBy().equals("itdSfiExempt");
				assert handleExemptAuthInfo.getErrMsg().equals(StringUtils.EMPTY);
			}
		}.run();
		
		//Invalid
		new ComponentTest() 
		{			
			protected void testComponents() throws Exception 
			{
				ExemptServiceLocal exemptService = (ExemptServiceLocal) getValue("#{exemptService}");
				HandleExemptAuthInfo handleExemptAuthInfoIn = new HandleExemptAuthInfo();
				handleExemptAuthInfoIn.setExemptAuthBy("itdSfiExempt");
				handleExemptAuthInfoIn.setExemptAuthPw("XXXTTT");				
				HandleExemptAuthInfo handleExemptAuthInfo = exemptService.validateHandleExemptAuthInfo(handleExemptAuthInfoIn);
				assert handleExemptAuthInfo.getExemptAuthBy().equals("itdSfiExempt");
				assert handleExemptAuthInfo.getErrMsg().equals("0091");
			}
		}.run();
		
		//Not enough rights
		new ComponentTest() 
		{			
			protected void testComponents() throws Exception 
			{
				ExemptServiceLocal exemptService = (ExemptServiceLocal) getValue("#{exemptService}");
				HandleExemptAuthInfo handleExemptAuthInfoIn = new HandleExemptAuthInfo();
				handleExemptAuthInfoIn.setExemptAuthBy("itdSfiFp");
				handleExemptAuthInfoIn.setExemptAuthPw("XXXZZZ");				
				HandleExemptAuthInfo handleExemptAuthInfo = exemptService.validateHandleExemptAuthInfo(handleExemptAuthInfoIn);
				assert handleExemptAuthInfo.getExemptAuthBy().equals("itdSfiFp");
				assert handleExemptAuthInfo.getErrMsg().equals("0236");
			}
		}.run();
	}

}

