package hk.org.ha.model.pms.biz.checkissue.test;

import hk.org.ha.model.pms.biz.checkissue.CheckIssueServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.vo.checkissue.CheckIssueOrder;
import hk.org.ha.model.pms.vo.checkissue.PrescDetail;
import hk.org.ha.model.pms.vo.security.PropMap;

import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.joda.time.DateTime;
import org.testng.annotations.Test;

public class CheckIssueTest extends SeamTest {
	
	@Test
	public void testCheckIssueComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		//retrieveCheckIssueList
//		new ComponentTest() {
//			
//			@SuppressWarnings("unchecked")
//			protected void testComponents() throws Exception {
//				invokeMethod("#{checkIssueListService.retrieveCheckIssueList}");	
//				List<CheckIssueOrder> checkOrderList = (List<CheckIssueOrder>)getValue("#{checkOrderList}");
//				List<CheckIssueOrder> otherCheckOrderList = (List<CheckIssueOrder>)getValue("#{otherCheckOrderList}");
//				List<CheckIssueOrder> issueOrderList = (List<CheckIssueOrder>)getValue("#{issueOrderList}");
//				
//				assert checkOrderList.size()>=0;
//				assert otherCheckOrderList.size()>0;
//				assert issueOrderList.size()>0;
//			}
//		}.run();	
		
		//retrieveIssuedList
//		new ComponentTest() {
			
//			@SuppressWarnings("unchecked")
//			protected void testComponents() throws Exception {
//				invokeMethod("#{issuedListService.retrieveIssuedList}");	
//				List<CheckIssueOrder> issuedOrderList = (List<CheckIssueOrder>)getValue("#{issuedOrderList}");
				
//				assert issuedOrderList.size()>0;
//			}
//		}.run();	

		//updateCheckWorkstationRuleCode
		new ComponentTest() {
			
			protected void testComponents() throws Exception {

				invokeMethod("#{checkIssueAidService.updateCheckWorkstationRuleCode}");	
			}
		}.run();	
		
		//updateRefresh
		new ComponentTest() {
			
			protected void testComponents() throws Exception {

				invokeMethod("#{checkIssueAidService.updateRefresh(false)}");	
				PropMap propMap = (PropMap)getValue("#{propMap}");
				
				assert propMap.getValue("checkIssue.refresh").equals("N");
			}
		}.run();		

		//retrieveIssueWindowNum
		new ComponentTest() {
			
			protected void testComponents() throws Exception {
				invokeMethod("#{checkIssueAidService.retrieveIssueWindowNum}");	
				Integer issueWindowNum = (Integer)getValue("#{issueWindowNum}");
				
				assert issueWindowNum!=null;
				assert issueWindowNum >=0;
			}
		}.run();	

		//retrievePrescDetail
//		new ComponentTest() {
//			
//			protected void testComponents() throws Exception {
//				DateTime dispDate = new DateTime().withDate(2011, 3, 30);
//
//				CheckIssueServiceLocal checkIssueService = (CheckIssueServiceLocal) getInstance("checkIssueService");
//				checkIssueService.retrievePrescDetail(dispDate.toDate(), "0707", false);
//				PrescDetail prescDetail = (PrescDetail)getValue("#{prescDetail}");
//				
//				assert prescDetail!=null;
//			}
//		}.run();	
//		
//		//updateDispOrderStatusToChecked
//		new ComponentTest() {
//			
//			protected void testComponents() throws Exception {
//				DateTime dispDate = new DateTime().withDate(2011, 4, 30);
//
//				CheckIssueServiceLocal checkIssueService = (CheckIssueServiceLocal) getInstance("checkIssueService");
//				checkIssueService.retrievePrescDetail(dispDate.toDate(), "0510", false);
//				PrescDetail prescDetail = (PrescDetail)getValue("#{prescDetail}");
//				
//				assert prescDetail.getDispOrderStatus() == DispOrderStatus.Assembled;
//
////				checkIssueService.updateDispOrderStatusToChecked(dispDate.toDate(), "0510", 1);
//				
//				checkIssueService.retrievePrescDetail(dispDate.toDate(), "0510", false);
//				prescDetail = (PrescDetail)getValue("#{prescDetail}");
//
//				assert checkIssueService.isUpdateSuccess();
//				assert prescDetail.getDispOrderStatus() == DispOrderStatus.Checked;
//				assert prescDetail.getIssueWindowNum() == 1;
//				
//			}
//		}.run();	
//
//		// Order has been suspended
//		new ComponentTest() {
//
//			protected void testComponents() throws Exception {
//				DateTime dispDate = new DateTime().withDate(2011, 4, 30);
//
//				CheckIssueServiceLocal checkIssueService = (CheckIssueServiceLocal) getInstance("checkIssueService");
//				checkIssueService.retrievePrescDetail(dispDate.toDate(), "0505", false);
//				PrescDetail prescDetail = (PrescDetail)getValue("#{prescDetail}");
//				
//				assert prescDetail.getDispOrderAdminStatus() == DispOrderAdminStatus.Suspended;
//
////				checkIssueService.updateDispOrderStatusToChecked(dispDate.toDate(), "0505", 1);
//				
//				assert !checkIssueService.isUpdateSuccess();
//				assert checkIssueService.getErrMsg().equals("0059");
//				
//			}
//		}.run();
//		
//		// Order has been deleted
//		new ComponentTest() {
//
//			protected void testComponents() throws Exception {
//				DateTime dispDate = new DateTime().withDate(2011, 4, 30);
//
//				CheckIssueServiceLocal checkIssueService = (CheckIssueServiceLocal) getInstance("checkIssueService");
//				checkIssueService.retrievePrescDetail(dispDate.toDate(), "0504", false);
//				PrescDetail prescDetail = (PrescDetail)getValue("#{prescDetail}");
//				
//				assert prescDetail.getMedOrderStatus() == MedOrderStatus.SysDeleted;
//
////				checkIssueService.updateDispOrderStatusToChecked(dispDate.toDate(), "0504", 1);
//				
//				assert !checkIssueService.isUpdateSuccess();
//				assert checkIssueService.getErrMsg().equals("0074");
//				
//			}
//		}.run();
//		
//		//Update Order Fail
//		new ComponentTest() {
//			
//			protected void testComponents() throws Exception {
//				DateTime dispDate = new DateTime().withDate(2011, 4, 30);
//
//				CheckIssueServiceLocal checkIssueService = (CheckIssueServiceLocal) getInstance("checkIssueService");
//				checkIssueService.retrievePrescDetail(dispDate.toDate(), "0503", false);
//				PrescDetail prescDetail = (PrescDetail)getValue("#{prescDetail}");
//				
//				assert prescDetail.getDispOrderStatus() == DispOrderStatus.Picked;
//
////				checkIssueService.updateDispOrderStatusToChecked(dispDate.toDate(), "0503", 1);
//				
//				checkIssueService.retrievePrescDetail(dispDate.toDate(), "0503", false);
//				prescDetail = (PrescDetail)getValue("#{prescDetail}");
//
//				assert !checkIssueService.isUpdateSuccess();
//				assert prescDetail.getDispOrderStatus() == DispOrderStatus.Picked;
//				
//			}
//		}.run();
//		
//		//Update Order Fail
//		new ComponentTest() {
//			
//			protected void testComponents() throws Exception {
//				DateTime dispDate = new DateTime().withDate(2011, 4, 30);
//
//				CheckIssueServiceLocal checkIssueService = (CheckIssueServiceLocal) getInstance("checkIssueService");
//				checkIssueService.retrievePrescDetail(dispDate.toDate(), "0503", false);
//				PrescDetail prescDetail = (PrescDetail)getValue("#{prescDetail}");
//				
//				assert prescDetail.getDispOrderStatus() == DispOrderStatus.Picked;
//
////				checkIssueService.updateDispOrderStatusToIssued(dispDate.toDate(), "0503");
//				
//				checkIssueService.retrievePrescDetail(dispDate.toDate(), "0503", false);
//				prescDetail = (PrescDetail)getValue("#{prescDetail}");
//
//				assert !checkIssueService.isUpdateSuccess();
//				assert prescDetail.getDispOrderStatus() == DispOrderStatus.Picked;
//				
//			}
//		}.run();		
//		
//		//retrieve Outstanding MOE Order Count
//		new ComponentTest() {
//			
//			protected void testComponents() throws Exception {
//
//				invokeMethod("#{checkIssueAidService.retrieveOutstandingMoeOrderCount}");	
//				Integer outstandingMoeOrderCount = Integer.parseInt(getValue("#{outstandingMoeOrderCount}").toString());
//				
//				assert outstandingMoeOrderCount>=0;
//			}
//		}.run();	
	}	
}
