package hk.org.ha.model.pms.biz.vetting.test;

import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class DrugOnHandServiceTest extends SeamTest {
	@Test
	public void testMoeServiceComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
	        }
		}.run();

	}
}
