package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.reftable.OperationModeListServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.reftable.Machine;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.persistence.reftable.OperationProp;
import hk.org.ha.model.pms.persistence.reftable.OperationWorkstation;
import hk.org.ha.model.pms.persistence.reftable.PickStation;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.YesNoFlag;
import hk.org.ha.model.pms.udt.reftable.MachineType;
import hk.org.ha.model.pms.udt.reftable.OperationProfileType;
import hk.org.ha.model.pms.udt.reftable.PrinterType;

import java.util.ArrayList;
import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class OperationModeListTest extends SeamTest {

	private int totalWorkstation = 0;
	@Test
	public void testOperationModeListComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		//retrieve operationProfileList
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{operationModeListService.retrieveOperationModeList}");
				List<OperationProfile> operationProfileList = (List<OperationProfile>)getValue("#{operationProfileList}");
				
				assert operationProfileList!=null;
			}
		}.run();
		
		
		//update operationProfileList
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{operationModeListService.retrieveOperationModeList}");
				List<OperationProfile> operationProfileList = (List<OperationProfile>)getValue("#{operationProfileList}");
				assert operationProfileList.size()>0;
				
				List<Workstation> operationModeWorkstationList = (List<Workstation>)getValue("#{operationModeWorkstationList}");
				assert operationModeWorkstationList.size()>0;
				totalWorkstation = operationModeWorkstationList.size();
				
				List<OperationProp> operationPropList = new ArrayList<OperationProp>();
				List<OperationWorkstation> operationWorkstationList = new ArrayList<OperationWorkstation>();
				
				for ( OperationProfile op : operationProfileList ) {
					if ( !op.isActive() ) {
						if ( op.getType() == OperationProfileType.Pms ) {
							for (OperationProp oProp : op.getOperationPropList() ) {
								if ( oProp.getProp().getName().equals("suspend.order.suspendCode.required")) {
									oProp.setValue(YesNoFlag.No.getDataValue());
									operationPropList.add(oProp);
									break;
								}
							}
							for (OperationWorkstation ow : op.getOperationWorkstationList()) {
								if ( ow.getWorkstation().getWorkstationCode().equals("0003")) {
									ow.setPrinterType(PrinterType.DefaultAndUrgent);
									operationWorkstationList.add(ow);
									break;
								}
							}
						}
					}
				}
				
				Workstation workstation = new Workstation();
				workstation.setWorkstationCode("test");
				workstation.setHostName("name");
				List<Workstation> wksList = new ArrayList<Workstation>();
				wksList.add(workstation);
				
				OperationModeListServiceLocal operationModeListService = (OperationModeListServiceLocal) getValue("#{operationModeListService}");
				operationModeListService.updateOperationModeList(operationPropList, operationWorkstationList, wksList, new ArrayList(), null, new ArrayList());
			}
		}.run();
		
		//update operationProfileList
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{operationModeListService.retrieveOperationModeList}");
				List<OperationProfile> operationProfileList = (List<OperationProfile>)getValue("#{operationProfileList}");
				assert operationProfileList!=null;
				
				List<Workstation> operationModeWorkstationList = (List<Workstation>)getValue("#{operationModeWorkstationList}");
				assert operationModeWorkstationList.size() == (totalWorkstation +1) ;
				
				for ( OperationProfile op : operationProfileList ) {
					if ( !op.isActive() ) {
						if ( op.getType() == OperationProfileType.Pms ) {
							for (OperationProp oProp : op.getOperationPropList() ) {
								if ( oProp.getProp().getName().equals("suspend.order.suspendCode.required")) {
									assert oProp.getValue().equals(YesNoFlag.No.getDataValue());
									break;
								}
							}
							for (OperationWorkstation ow : op.getOperationWorkstationList()) {
								if ( ow.getWorkstation().getWorkstationCode().equals("0003")) {
									assert ow.getPrinterType() == PrinterType.DefaultAndUrgent;
								} 
							}
						}
						assert op.getOperationWorkstationList().size() == (totalWorkstation +1) ;
					} else {
						assert op.getOperationWorkstationList().size() == totalWorkstation;
					}
				}
			}
		}.run();
		
		//update operationProfileList
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{operationModeListService.retrieveOperationModeList}");
				List<OperationProfile> operationProfileList = (List<OperationProfile>)getValue("#{operationProfileList}");
				assert operationProfileList.size()>0;
				
				List<Workstation> operationModeWorkstationList = (List<Workstation>)getValue("#{operationModeWorkstationList}");
				assert operationModeWorkstationList.size() == (totalWorkstation+1);
				
				List<OperationProp> operationPropList = new ArrayList<OperationProp>();
				List<OperationWorkstation> operationWorkstationList = new ArrayList<OperationWorkstation>();
				
				for ( OperationProfile op : operationProfileList ) {
					if ( !op.isActive() ) {
						if ( op.getType() == OperationProfileType.Pms ) {
							for (OperationProp oProp : op.getOperationPropList() ) {
								if ( oProp.getProp().getName().equals("suspend.order.suspendCode.required")) {
									oProp.setValue(YesNoFlag.Yes.getDataValue());
									operationPropList.add(oProp);
									break;
								}
							}
							for (OperationWorkstation ow : op.getOperationWorkstationList()) {
								if ( ow.getWorkstation().getWorkstationCode().equals("0003")) {
									ow.setPrinterType(PrinterType.Normal);
									operationWorkstationList.add(ow);
									break;
								}
							}
						}
					}
				}
				
				OperationModeListServiceLocal operationModeListService = (OperationModeListServiceLocal) getValue("#{operationModeListService}");
				operationModeListService.updateOperationModeList(operationPropList, operationWorkstationList, new ArrayList(), new ArrayList(), null, new ArrayList());
			}
		}.run();
		
		//update operationProfileList
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{operationModeListService.retrieveOperationModeList}");
				List<OperationProfile> operationProfileList = (List<OperationProfile>)getValue("#{operationProfileList}");
				assert operationProfileList!=null;
				
				List<Workstation> operationModeWorkstationList = (List<Workstation>)getValue("#{operationModeWorkstationList}");
				assert operationModeWorkstationList.size() == (totalWorkstation +1) ;
				
				for ( OperationProfile op : operationProfileList ) {
					if ( !op.isActive() ) {
						if ( op.getType() == OperationProfileType.Pms ) {
							for (OperationProp oProp : op.getOperationPropList() ) {
								if ( oProp.getProp().getName().equals("suspend.order.suspendCode.required")) {
									assert oProp.getValue().equals(YesNoFlag.Yes.getDataValue());
									break;
								}
							}
							for (OperationWorkstation ow : op.getOperationWorkstationList()) {
								if ( ow.getWorkstation().getWorkstationCode().equals("0003")) {
									assert ow.getPrinterType() == PrinterType.Normal;
								} 
							}
						}
						assert op.getOperationWorkstationList().size() == (totalWorkstation +1) ;
					} else {
						assert op.getOperationWorkstationList().size() == totalWorkstation;
					}
				}
			}
		}.run();	
		
		//active operationProfile
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{operationModeListService.retrieveOperationModeList}");
				List<OperationProfile> operationProfileList = (List<OperationProfile>)getValue("#{operationProfileList}");
				assert operationProfileList!=null;
				
				List<Workstation> operationModeWorkstationList = (List<Workstation>)getValue("#{operationModeWorkstationList}");
				assert operationModeWorkstationList.size() == (totalWorkstation +1) ;
				
				Long activeProfileId = null;
				
				for ( OperationProfile op : operationProfileList ) {
					if ( !op.isActive() ) {
						if ( op.getType() == OperationProfileType.Pms ) {
							activeProfileId = op.getId();
							break;
						}
					} 
				}
				
				OperationModeListServiceLocal operationModeListService = (OperationModeListServiceLocal) getValue("#{operationModeListService}");
				operationModeListService.updateOperationModeList(new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), activeProfileId, new ArrayList());
			}
		}.run();
		
		//check active profile
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{operationModeListService.retrieveOperationModeList}");
				List<OperationProfile> operationProfileList = (List<OperationProfile>)getValue("#{operationProfileList}");
				
				assert operationProfileList!=null;
				
				for ( OperationProfile op : operationProfileList ) {
					if ( op.isActive() ) {
						assert op.getType() == OperationProfileType.Pms ;
						break;
					} 
				}
			}
		}.run();
		
		//active operationProfile
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{operationModeListService.retrieveOperationModeList}");
				List<OperationProfile> operationProfileList = (List<OperationProfile>)getValue("#{operationProfileList}");
				assert operationProfileList!=null;
				
				List<Workstation> operationModeWorkstationList = (List<Workstation>)getValue("#{operationModeWorkstationList}");
				assert operationModeWorkstationList.size() == (totalWorkstation +1) ;
				
				Long activeProfileId = null;
				
				for ( OperationProfile op : operationProfileList ) {
					if ( !op.isActive() ) {
						if ( op.getType() == OperationProfileType.Eds ) {
							activeProfileId = op.getId();
							break;
						}
					} 
				}
				
				OperationModeListServiceLocal operationModeListService = (OperationModeListServiceLocal) getValue("#{operationModeListService}");
				operationModeListService.updateOperationModeList(new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), activeProfileId, new ArrayList());
			}
		}.run();
		
		//check active profile
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{operationModeListService.retrieveOperationModeList}");
				List<OperationProfile> operationProfileList = (List<OperationProfile>)getValue("#{operationProfileList}");
				
				assert operationProfileList!=null;
				
				for ( OperationProfile op : operationProfileList ) {
					if ( op.isActive() ) {
						assert op.getType() == OperationProfileType.Eds ;
						break;
					} 
				}
			}
		}.run();
		
		
		//update workstation
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				List<Workstation> operationModeWorkstationList = (List<Workstation>)getValue("#{operationModeWorkstationList}");
				assert operationModeWorkstationList.size() == (totalWorkstation +1) ;
				
				List<Workstation> updateWksList = new ArrayList<Workstation>();
				
				for ( Workstation wks : operationModeWorkstationList ) {
					if ( wks.getWorkstationCode().equals("test") ) {
						assert wks.getHostName().equals("name");
						wks.setHostName("test");
						updateWksList.add(wks);
						break;
					}
				}
				
				OperationModeListServiceLocal operationModeListService = (OperationModeListServiceLocal) getValue("#{operationModeListService}");
				operationModeListService.updateOperationModeList(new ArrayList(), new ArrayList(), updateWksList, new ArrayList(), null, new ArrayList());
			}
		}.run();
		
		//check workstation
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				List<Workstation> operationModeWorkstationList = (List<Workstation>)getValue("#{operationModeWorkstationList}");
				assert operationModeWorkstationList.size() == (totalWorkstation +1) ;
				
				for ( Workstation wks : operationModeWorkstationList ) {
					if ( wks.getWorkstationCode().equals("test") ) {
						assert wks.getHostName().equals("test");
						break;
					}
				}
			}
		}.run();
		
		//update PickStation
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				
				invokeMethod("#{operationModeListService.retrieveOperationModeList}");
				List<OperationProfile> operationProfileList = (List<OperationProfile>)getValue("#{operationProfileList}");
				assert operationProfileList!=null;
				
				List<PickStation> psList = new ArrayList<PickStation>();
				PickStation ps = new PickStation();
				ps.setPickStationNum(99);
				Machine m = new Machine();
				m.setPickStation(ps);
				m.setHostName("host");
				m.setType(MachineType.Manual);
				ps.setMachineList(new ArrayList<Machine>());
				ps.getMachineList().add(m);
				psList.add(ps);
				
				List<OperationWorkstation> operationWorkstationList = new ArrayList<OperationWorkstation>();
				
				for ( OperationProfile op : operationProfileList ) {
					if ( !op.isActive() ) {
						if ( op.getType() == OperationProfileType.Eds ) {
							for (OperationWorkstation ow : op.getOperationWorkstationList()) {
								if ( ow.getWorkstation().getWorkstationCode().equals("0003")) {
									ow.setMachine(m);
									operationWorkstationList.add(ow);
									break;
								}
							}
						}
					}
				}
				
				OperationModeListServiceLocal operationModeListService = (OperationModeListServiceLocal) getValue("#{operationModeListService}");
				operationModeListService.updateOperationModeList(new ArrayList(), operationWorkstationList, new ArrayList(), psList, null, new ArrayList());
			}
		}.run();
		
		//check PickStation
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{operationModeListService.retrieveOperationModeList}");
				List<OperationProfile> operationProfileList = (List<OperationProfile>)getValue("#{operationProfileList}");
				assert operationProfileList!=null;
				
				for ( OperationProfile op : operationProfileList ) {
					if ( !op.isActive() ) {
						if ( op.getType() == OperationProfileType.Eds ) {
							for (OperationWorkstation ow : op.getOperationWorkstationList()) {
								if ( ow.getWorkstation().getWorkstationCode().equals("0003")) {
									assert ow.getMachine().getType() == MachineType.Manual;
									assert ow.getMachine().getHostName().equals("host");
									assert ow.getMachine().getPickStation().getPickStationNum().equals(99);
									break;
								}
							}
						}
					}
				}
			}
		}.run();
	}	
}
