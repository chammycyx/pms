package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.reftable.PharmRemarkTemplate;

import java.util.ArrayList;
import java.util.List;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class PharmRemarkTemplateListTest extends SeamTest {

	
	@Test
	public void testPharmRemarkTemplateListComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");	
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		
		// retrieve remark list
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				invokeMethod("#{pharmRemarkTemplateListService.retrievePharmRemarkTemplateList()}");
				List<PharmRemarkTemplate> pharmRemarkTemplateList = (List<PharmRemarkTemplate>) getValue("#{pharmRemarkTemplateList}");
				assert pharmRemarkTemplateList.size()==0;
			}
		}.run();
		
		// add remark
		new ComponentTest() {
			protected void testComponents() throws Exception {

				List<PharmRemarkTemplate> newPharmRemarkTemplateList = new ArrayList<PharmRemarkTemplate>();
				
				PharmRemarkTemplate pharmRemarkTemplate = new PharmRemarkTemplate();
				pharmRemarkTemplate.setTemplate("THIS ITEM IS ADDED BY DR.");
				pharmRemarkTemplate.setMarkDelete(false);
				
				newPharmRemarkTemplateList.add(pharmRemarkTemplate);
				
				PharmRemarkTemplate pharmRemarkTemplate2 = new PharmRemarkTemplate();
				pharmRemarkTemplate2.setTemplate("THIS ITEM IS CANCELLED BY DR.");
				pharmRemarkTemplate2.setMarkDelete(false);
				
				newPharmRemarkTemplateList.add(pharmRemarkTemplate2);
				
				PharmRemarkTemplate pharmRemarkTemplate3 = new PharmRemarkTemplate();
				pharmRemarkTemplate3.setTemplate("FREQUENCY C/W DR.");
				pharmRemarkTemplate3.setMarkDelete(false);
				
				newPharmRemarkTemplateList.add(pharmRemarkTemplate3);
					
				Contexts.getSessionContext().set("pharmRemarkTemplateList", newPharmRemarkTemplateList);
				invokeMethod("#{pharmRemarkTemplateListService.updatePharmRemarkTemplateList( pharmRemarkTemplateList)}");
			}
		
		}.run();
		
		//check remark
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception
			{	
				//retrieve list
				invokeMethod("#{pharmRemarkTemplateListService.retrievePharmRemarkTemplateList()}");
				List<PharmRemarkTemplate> pharmRemarkTemplateList = (List<PharmRemarkTemplate>) getValue("#{pharmRemarkTemplateList}");
				assert pharmRemarkTemplateList.size()==3;
				
				for(PharmRemarkTemplate pharmRemarkTemplate: pharmRemarkTemplateList){
					if (pharmRemarkTemplate.getSortSeq().equals(1)){
						assert pharmRemarkTemplate.getTemplate().equals("THIS ITEM IS ADDED BY DR.");
					}
					
					if (pharmRemarkTemplate.getSortSeq().equals(2)){
						assert pharmRemarkTemplate.getTemplate().equals("THIS ITEM IS CANCELLED BY DR.");
					}
					
					if (pharmRemarkTemplate.getSortSeq().equals(3)){
						assert pharmRemarkTemplate.getTemplate().equals("FREQUENCY C/W DR.");
					}
				}
			}
		}.run();
		
		// edit remark
		new ComponentTest(){
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception{
				invokeMethod("#{pharmRemarkTemplateListService.retrievePharmRemarkTemplateList()}");
				List<PharmRemarkTemplate> pharmRemarkTemplateList = (List<PharmRemarkTemplate>) getValue("#{pharmRemarkTemplateList}");
				
				for ( PharmRemarkTemplate pharmRemarkTemplate: pharmRemarkTemplateList){					
					if (pharmRemarkTemplate.getSortSeq().equals(1)){
						pharmRemarkTemplate.setTemplate("DURATION C/W DR.");
						pharmRemarkTemplate.setMarkDelete(false);
					}
					
					if (pharmRemarkTemplate.getSortSeq().equals(2)){
						pharmRemarkTemplate.setTemplate("THIS ITEM IS CANCELLED BY DR.");
						pharmRemarkTemplate.setMarkDelete(false);
					}
					
					if (pharmRemarkTemplate.getSortSeq().equals(3)){
						pharmRemarkTemplate.setTemplate("FREQUENCY C/W DR.");
						pharmRemarkTemplate.setMarkDelete(false);
					}
				}
					
				Contexts.getSessionContext().set("newPharmRemarkTemplateList", pharmRemarkTemplateList);
				invokeMethod("#{pharmRemarkTemplateListService.updatePharmRemarkTemplateList( newPharmRemarkTemplateList)}");
				
			}
		}.run();
		
	//check remark
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception
			{	
				//retrieve list
				invokeMethod("#{pharmRemarkTemplateListService.retrievePharmRemarkTemplateList()}");
				List<PharmRemarkTemplate> pharmRemarkTemplateList = (List<PharmRemarkTemplate>) getValue("#{pharmRemarkTemplateList}");
				assert pharmRemarkTemplateList.size()==3;
				
				for(PharmRemarkTemplate pharmRemarkTemplate: pharmRemarkTemplateList){
					if (pharmRemarkTemplate.getSortSeq().equals(1)){
						assert pharmRemarkTemplate.getTemplate().equals("DURATION C/W DR.");
					}
					
					if (pharmRemarkTemplate.getSortSeq().equals(2)){
						assert pharmRemarkTemplate.getTemplate().equals("THIS ITEM IS CANCELLED BY DR.");
					}
					
					if (pharmRemarkTemplate.getSortSeq().equals(3)){
						assert pharmRemarkTemplate.getTemplate().equals("FREQUENCY C/W DR.");
					}
				}
			}
		}.run();
		
		// edit sequence
		new ComponentTest(){
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception{		
				invokeMethod("#{pharmRemarkTemplateListService.retrievePharmRemarkTemplateList()}");
				List<PharmRemarkTemplate> pharmRemarkTemplateList = (List<PharmRemarkTemplate>) getValue("#{pharmRemarkTemplateList}");
				
				for ( PharmRemarkTemplate pharmRemarkTemplate: pharmRemarkTemplateList){					
					if (pharmRemarkTemplate.equals(2)){
						pharmRemarkTemplate.setTemplate("THIS ITEM IS CANCELLED BY DR.");
						pharmRemarkTemplate.setMarkDelete(false);
					}
					
					if (pharmRemarkTemplate.equals(1)){
						pharmRemarkTemplate.setTemplate("DURATION C/W DR.");
						pharmRemarkTemplate.setMarkDelete(false);
					}
					
					if (pharmRemarkTemplate.equals(3)){
						pharmRemarkTemplate.setTemplate("FREQUENCY C/W DR.");
						pharmRemarkTemplate.setMarkDelete(false);
					}
				}
			
				Contexts.getSessionContext().set("pharmRemarkTemplateList", pharmRemarkTemplateList);
				invokeMethod("#{pharmRemarkTemplateListService.updatePharmRemarkTemplateList( pharmRemarkTemplateList)}");
				
			}
		}.run();
		
	//check remark
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception
			{	
				//retrieve list
				invokeMethod("#{pharmRemarkTemplateListService.retrievePharmRemarkTemplateList()}");
				List<PharmRemarkTemplate> pharmRemarkTemplateList = (List<PharmRemarkTemplate>) getValue("#{pharmRemarkTemplateList}");
				assert pharmRemarkTemplateList.size()>0;
				
				for(PharmRemarkTemplate pharmRemarkTemplate: pharmRemarkTemplateList){
					if (pharmRemarkTemplate.getSortSeq().equals(2)){
						assert pharmRemarkTemplate.getTemplate().equals("THIS ITEM IS CANCELLED BY DR.");
					}
					
					if (pharmRemarkTemplate.getSortSeq().equals(1)){
						assert pharmRemarkTemplate.getTemplate().equals("DURATION C/W DR.");
					}
					
					if (pharmRemarkTemplate.getSortSeq().equals(3)){
						assert pharmRemarkTemplate.getTemplate().equals("FREQUENCY C/W DR.");
					}
				}
			}
		}.run();
			
		// delete remark
		new ComponentTest(){
			protected void testComponents() throws Exception{
				invokeMethod("#{pharmRemarkTemplateListService.retrievePharmRemarkTemplateList()}");
				List<PharmRemarkTemplate> pharmRemarkTemplateList = (List<PharmRemarkTemplate>) getValue("#{pharmRemarkTemplateList}");
				
				for ( PharmRemarkTemplate pharmRemarkTemplate: pharmRemarkTemplateList){	
					if (pharmRemarkTemplate.getSortSeq().equals(2)){
						pharmRemarkTemplate.setTemplate("THIS ITEM IS CANCELLED BY DR.");
						pharmRemarkTemplate.setMarkDelete(true);
					}
					
					if (pharmRemarkTemplate.getSortSeq().equals(1)){
						pharmRemarkTemplate.setTemplate("DURATION C/W DR.");
						pharmRemarkTemplate.setMarkDelete(false);
					}
					
					if (pharmRemarkTemplate.getSortSeq().equals(3)){
						pharmRemarkTemplate.setTemplate("FREQUENCY C/W DR.");
						pharmRemarkTemplate.setMarkDelete(false);
					}
				}
			
					
				Contexts.getSessionContext().set("pharmRemarkTemplateList", pharmRemarkTemplateList);
				invokeMethod("#{pharmRemarkTemplateListService.updatePharmRemarkTemplateList( pharmRemarkTemplateList)}");
				
			}
		}.run();
		
		//check remark
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception
			{	
				//retrieve list
				invokeMethod("#{pharmRemarkTemplateListService.retrievePharmRemarkTemplateList()}");
				List<PharmRemarkTemplate> pharmRemarkTemplateList = (List<PharmRemarkTemplate>) getValue("#{pharmRemarkTemplateList}");
		
				assert pharmRemarkTemplateList.size()==2;
				
				Contexts.getSessionContext().set("pharmRemarkTemplateList", pharmRemarkTemplateList);
				invokeMethod("#{pharmRemarkTemplateListService.updatePharmRemarkTemplateList( pharmRemarkTemplateList)}");
			}
		}.run();
	}	
}


