package hk.org.ha.model.pms.biz.mock;

import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.udt.cddh.CddhRemarkStatus;
import hk.org.ha.model.pms.vo.cddh.CddhCriteria;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpCddhServiceJmsRemote;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;

@Startup
@Name("corpCddhService2Proxy")
@Scope(ScopeType.APPLICATION)
public class MockCorpCddhService2Proxy implements CorpCddhServiceJmsRemote {

	public List<DispOrderItem> retrieveDispOrderItem(CddhCriteria cddhCriteria) {
		List<DispOrderItem> dispOrderItemList = new ArrayList<DispOrderItem>();
		
		if ( StringUtils.equals(cddhCriteria.getHkid(), "A1234563") ){
			PharmOrderItem pharmOrderItem = new PharmOrderItem();
			pharmOrderItem.setId(1L);
			pharmOrderItem.setItemCode("PARA01");
			
			DispOrder dispOrder = new DispOrder();
			dispOrder.setId(1L);
		
			
			DispOrderItem dispOrderItem = new DispOrderItem();
			dispOrderItem.setItemNum(Integer.valueOf(1));
			dispOrderItem.setId(1L);
			dispOrderItem.setDispQty(BigDecimal.valueOf(20));
			dispOrderItem.setPharmOrderItem(pharmOrderItem);
			dispOrderItem.setDispOrder(dispOrder);
			dispOrderItemList.add(dispOrderItem);                                                                                                                                                                               
			
		}
		return dispOrderItemList;
	}

	public List<DispOrder> retrieveDispOrderLikePatName(CddhCriteria cddhCriteria) {
		// TODO Auto-generated method stub
		List<DispOrder> dispOrderList = new ArrayList<DispOrder>();
		
		if(cddhCriteria.getPatientName() == "PAT%"){
			PharmOrder pharmOrder = new PharmOrder();
			pharmOrder.setWardCode("1A");
			pharmOrder.setPatCatCode("CA");
			
			DispOrder dispOrder = new DispOrder();
			dispOrder.setId(1L);
			dispOrder.setDispDate(new Date());
			dispOrder.setPharmOrder(pharmOrder);
			
			dispOrderList.add(dispOrder);
		}
		
		return dispOrderList;
	}

	public void updateDispOrderItemRemark(DispOrderItem dispOrderItem, Boolean legacyPersistenceEnabled) {
		// TODO Auto-generated method stub
		
	}

	public void deleteDispOrderItemRemark(DispOrderItem dispOrderItem, Boolean legacyPersistenceEnabled) {
		// TODO Auto-generated method stub
		
	}

	public CddhRemarkStatus checkDispOrderItemRemarkAccess(Long dispOrderId, String loginHospCode,
			String displayHospCode) {
		// TODO Auto-generated method stub
		return null;
	}
}
