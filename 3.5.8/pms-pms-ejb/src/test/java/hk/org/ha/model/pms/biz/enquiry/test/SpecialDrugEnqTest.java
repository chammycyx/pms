package hk.org.ha.model.pms.biz.enquiry.test;

import hk.org.ha.model.pms.biz.enquiry.SpecialDrugEnqListServiceLocal;
import hk.org.ha.model.pms.biz.enquiry.SpecialDrugEnqServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.dms.persistence.DmFmDrug;
import hk.org.ha.model.pms.dms.vo.FmDrug;
import hk.org.ha.model.pms.dms.vo.FmDrugCorpInd;

import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class SpecialDrugEnqTest extends SeamTest {

	
	@Test
	public void testAssemblingComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		//retrieve sDrugList by itemCode
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				SpecialDrugEnqListServiceLocal specialDrugEnqListService = (SpecialDrugEnqListServiceLocal) getValue("#{specialDrugEnqListService}");
				specialDrugEnqListService.retrieveDmFmDrugList("");
				List<DmFmDrug> dmFmDrugList = (List<DmFmDrug>) getValue("#{dmFmDrugList}");
				assert dmFmDrugList.size()>0;
			}
		}.run();
		
		//retrieve sDrugList by fullDrugDesc
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				SpecialDrugEnqListServiceLocal specialDrugEnqListService = (SpecialDrugEnqListServiceLocal) getValue("#{specialDrugEnqListService}");
				specialDrugEnqListService.retrieveDmFmDrugListByFullDrugDesc("VAR");
				List<DmFmDrug> dmFmDrugList = (List<DmFmDrug>) getValue("#{dmFmDrugList}");
				assert dmFmDrugList.size()>0;
			}
		}.run();
		
		//retrieve sDrug by item Code
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				SpecialDrugEnqServiceLocal specialDrugEnqService = (SpecialDrugEnqServiceLocal) getValue("#{specialDrugEnqService}");
				specialDrugEnqService.retrieveDmFmDrug("ETAN01");
				FmDrug fmDrug = (FmDrug) getValue("#{fmDrug}");
				
				for (FmDrugCorpInd fmDrugCorpInd: fmDrug.getFmDrugCorpIndArray()){
					if(fmDrugCorpInd.getCorpIndicationCode().equals("0001")) {
						assert fmDrugCorpInd.getCorpIndicationOption().equals("XD");
					}
				}
				assert fmDrug.getItemCode().equals("ETAN01");
			}
		}.run();
		
		
	}	
}
