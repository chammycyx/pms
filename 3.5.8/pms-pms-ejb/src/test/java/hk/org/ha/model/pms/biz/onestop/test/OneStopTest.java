package hk.org.ha.model.pms.biz.onestop.test;

import java.util.List;

import hk.org.ha.model.pms.biz.onestop.OneStopPasManagerLocal;
import hk.org.ha.model.pms.biz.onestop.OneStopServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderStatus;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderStatusIndicator;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
import hk.org.ha.model.pms.udt.validation.SearchTextType;
import hk.org.ha.model.pms.vo.onestop.OneStopSummary;
import hk.org.ha.model.pms.vo.security.PropMap;
import static hk.org.ha.model.pms.prop.Prop.*;


import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class OneStopTest extends SeamTest {
		
	@Test
	public void testOneStopComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		//Test for non valid data input
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("123", true);
				String errMsg = (String) getValue("#{errMsg}");
				assert errMsg.equals("0034");
			}
		}.run();

		
		//Test for invalid Hkid
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("A1234561", true);
				String errMsg = (String) getValue("#{errMsg}");
				assert errMsg.equals("0033");
			}
		}.run();
		
		//Test unvet order without check digit with hkpmi and opas are down
		new ComponentTest()
		{
			protected void testComponents() throws Exception
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("MOEQMH110000189", true);
				PropMap propMap = (PropMap) getValue("#{propMap}");
				propMap.add("patient.pas.patient.enabled", "N");
				propMap.add("patient.pas.appt.enabled", "N");
				oneStopService.retrieveHkPmiPatientByOrderNumOrRefNumOrTickNum();
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getOneStopOrderItemSummaryList().size() == 1;
				Patient patient = oneStopSummary.getPatient();
				assert patient.getName() == null;
				assert patient.getPatKey() == null;
				assert patient.getHkid() == null;
				assert "PSY".equals(oneStopSummary.getOneStopOrderItemSummaryList().get(0).getPhsSpecCode());
			}
		}.run();
		
		//Test unvet order with check digit with hkpmi is down and opas is up
//		new ComponentTest()
//		{
//			protected void testComponents() throws Exception
//			{
//				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
//				oneStopService.retrieveOrder("MOEQMH1100001899", true);
//				PropMap propMap = (PropMap) getValue("#{propMap}");
//				propMap.getProperties().put("patient.pas.patient.enabled", "N");
//				oneStopService.retrieveHkPmiPatientByOrderNumOrRefNumOrTickNum();
//				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
//				assert oneStopSummary.getOneStopOrderItemSummaryList().size() == 1;
//				Patient patient = oneStopSummary.getPatient();
//				assert patient.getName() == null;
//				assert patient.getPatKey() == null;
//				assert patient.getHkid() == null;
//				assert "PSY".equals(oneStopSummary.getOneStopOrderItemSummaryList().get(0).getMappedPhsSpecCode());
//			}
//		}.run();

		//Test unvet order with ordernum QMH110000189 / VH 110000189 and with and without check digit
//		new ComponentTest() 
//		{
//			protected void testComponents() throws Exception 
//			{
//				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
//				oneStopService.retrieveOrder("MOEQMH1100001899", true);
//				oneStopPasManagerLocal oneStopPasManager = (oneStopPasManagerLocal) getValue("#{oneStopPasManager}");
//				oneStopPasManager.retrieveHkpmiPatientByCaseNum(patHopsCode, caseNum)
//				oneStopPasManager.retrievePatientAndAppointmentFromPasByCaseNum("MOEQMH1100001899");
//				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
//				assert oneStopSummary.getPatient().getHkid().equals("UM8073753");
//				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getMedCase().getPasAttendFlag() == true;
//				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getMedCase().getPasPatGroupCode().equals("GH");
//				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.MedOrder;
//				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("QMH110000189");
//				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Unvet;
//				
//				oneStopService.retrieveOrder("MOEQMH110000189", true);
//				oneStopPasManager.retrievePatientAndAppointmentFromPasByCaseNum("MOEQMH110000189");
//				oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
//				assert oneStopSummary.getPatient().getHkid().equals("UM8073753");
//				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.MedOrder;
//				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("QMH110000189");
//				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Unvet;
//				
//				oneStopService.retrieveOrder("MOEVH1100001899", true);
//				oneStopPasManager.retrievePatientAndAppointmentFromPasByCaseNum("MOEVH1100001899");
//				oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
//				assert oneStopSummary.getPatient().getHkid().equals("UM8073753");
//				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.MedOrder;
//				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("VH 110000189");
//				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Unvet;
//				
//				oneStopService.retrieveOrder("MOEVH110000189", true);
//				oneStopPasManager.retrievePatientAndAppointmentFromPasByCaseNum("MOEVH110000189");
//				oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
//				assert oneStopSummary.getPatient().getHkid().equals("UM8073753");
//				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.MedOrder;
//				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("VH 110000189");
//				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Unvet;
//				
//				oneStopService.retrieveOrder("MOEVH 110000189", true);
//				oneStopPasManager.retrievePatientAndAppointmentFromPasByCaseNum("MOEVH 110000189");
//				oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
//				assert oneStopSummary.getPatient().getHkid().equals("UM8073753");
//				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.MedOrder;
//				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("VH 110000189");
//				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Unvet;
//			}
//		}.run();
		
		//Test with new order with ordernum QMH110000190 and status is OneStopOrderStatus.ProcessingByPharmacy
		/*new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("MOEQMH110000190", true);
				oneStopPasManagerLocal oneStopPasManager = (oneStopPasManagerLocal) getValue("#{oneStopPasManager}");
				oneStopPasManager.retrievePatientAndAppointmentFromPasByCaseNum("MOEQMH110000190");
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("M8688074");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getMedCase().getPasAttendFlag() == true;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getMedCase().getPasPatGroupCode().equals("GH");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.MedOrder;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("QMH110000190");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.ProcessingByPharmacy;
			}
		}.run();
		
		//Test with new order with ordernum QMH110000192 and ticket (0512) is associate
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("MOEQMH110000192", true);
				oneStopPasManagerLocal oneStopPasManager = (oneStopPasManagerLocal) getValue("#{oneStopPasManager}");
				oneStopPasManager.retrievePatientAndAppointmentFromPasByCaseNum("MOEQMH110000192");
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("UM8073753");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.MedOrder;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("QMH110000192");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Unvet;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getTicketNum().equals("0512");
			}
		}.run();
		
		//Test with vet order with ordernum QMH110000191, ticket 0511 and status is PharmAdminStatus.Suspended
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("MOEQMH110000191", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("M8688074");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.DispOrder;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("QMH110000191");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Suspended;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatusInd() == OneStopOrderStatusIndicator.Suspend;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getTicketNum().equals("0511");
			}
		}.run();
		
		//Test with vet order with ordernum QMH110000193, ticket 0513 and status is PharmAdminStatus.AllowModify
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("MOEQMH110000193", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("M8688074");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.DispOrder;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("QMH110000193");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Suspended;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatusInd() == OneStopOrderStatusIndicator.AllowModify;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getTicketNum().equals("0513");
			}
		}.run();
		
		//Test with vet order with ordernum QMH110000194, ticket 0514 and status is PharmAdminStatus.AllowModify with AllowCommentFlag is true
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("MOEQMH110000194", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("UM8073753");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.DispOrder;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("QMH110000194");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Suspended;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatusInd() == OneStopOrderStatusIndicator.AllowModify;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getTicketNum().equals("0514");
			}
		}.run();
		
		//Test with vet order with ordernum QMH110000195, ticket 0515 and status is PharmAdminStatus.Uncollect
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("MOEQMH110000195", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("M8688074");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.DispOrder;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("QMH110000195");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Suspended;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatusInd() == OneStopOrderStatusIndicator.UnCollect;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getTicketNum().equals("0515");
			}
		}.run();
		
		//Test with vet order with ordernum QMH110000196, ticket 0701 and status is PharmAdminStatus.Suspended and PharmarcyRemark
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("MOEQMH110000196", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("UM8073753");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.DispOrder;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("QMH110000196");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Suspended;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatusInd() == OneStopOrderStatusIndicator.PharmacyRemark;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getTicketNum().equals("0701");
			}
		}.run();
		
		//Test with vet order with ordernum QMH110000197, ticket 0702 and status is PharmAdminStatus.Suspended and PharmarcyRemarkConfirm
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("MOEQMH110000197", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("UM8073753");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.DispOrder;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("QMH110000197");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Suspended;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatusInd() == OneStopOrderStatusIndicator.PharmacyremarkConfirm;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getTicketNum().equals("0702");
			}
		}.run();
		
		//Test with vet order with ordernum QMH110000198, ticket 0703 and status is Vetted
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("MOEQMH110000198", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("M8688074");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.DispOrder;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("QMH110000198");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Vetted;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getTicketNum().equals("0703");
			}
		}.run();
		
		//Test with vet order with (ordernum QMH110000199, ticket 0704, ordernum QMH110000200, ticket 0705) and status is Picking
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("MOEQMH110000199", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("UM8073753");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.DispOrder;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("QMH110000199");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Picking;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getTicketNum().equals("0704");
				oneStopService.retrieveOrder("MOEQMH110000200", true);
				oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("M8688074");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.DispOrder;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("QMH110000200");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Picking;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getTicketNum().equals("0705");
			}
		}.run();
		
		//Test with vet order with (ordernum QMH110000201, ticket 0706, ordernum QMH110000202, ticket 0707) and status is Assembling
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("MOEQMH110000201", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("UM8073753");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.DispOrder;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("QMH110000201");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Assembling;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getTicketNum().equals("0706");
				oneStopService.retrieveOrder("MOEQMH110000202", true);
				oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("M8688074");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.DispOrder;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("QMH110000202");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Assembling;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getTicketNum().equals("0707");
			}
		}.run();
		
		//Test with vet order with ordernum QMH110000203, ticket 0708 and status is Check
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("MOEQMH110000203", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("UM8073753");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.DispOrder;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("QMH110000203");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Checked;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getTicketNum().equals("0708");
			}
		}.run();
		
		//Test with vet order with ordernum QMH110000204, ticket 0709 and status is Issue
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("MOEQMH110000204", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("M8688074");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.DispOrder;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("QMH110000204");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Issued;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getTicketNum().equals("0709");
			}
		}.run();
		
		//Test with refNum with order is unvet
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("1000", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				List<OneStopSummary> refNumOrderList = oneStopService.getRefNumOrderList();
				assert refNumOrderList.size() == 1;
				assert refNumOrderList.get(0).getPatient() == null;
				assert refNumOrderList.get(0).getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Unvet;
				oneStopPasManagerLocal oneStopPasManager = (oneStopPasManagerLocal) getValue("#{oneStopPasManager}");
				oneStopPasManager.retrievePatientFromPasByOrderRefNum(refNumOrderList);
				oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("UM8073753");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("QMH110000189");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Unvet;
			}
		}.run();
		
		//Test with refNum with order is vetted
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("1010", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				List<OneStopSummary> refNumOrderList = oneStopService.getRefNumOrderList();
				assert refNumOrderList == null;
				assert oneStopSummary.getPatient() != null;
				assert oneStopSummary.getPatient().getHkid().equals("M8688074");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("QMH110000198");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getTicketNum().equals("0703");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Vetted;
			}
		}.run();
		
		//Test with refNum 1017 with the same refNum within one year and one with vet and another with unvet order, test for popup
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("1017", true);
				List<OneStopSummary> refNumOrderList = oneStopService.getRefNumOrderList();
				assert refNumOrderList.size() == 4;
				assert refNumOrderList.get(0).getPatient() == null;
				assert refNumOrderList.get(0).getOneStopOrderSummaryItemList().get(0).getMedCase().getCaseNum().equals("SOPD9999001U");
				assert refNumOrderList.get(0).getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Unvet;
				assert refNumOrderList.get(1).getPatient() == null;
				assert refNumOrderList.get(1).getOneStopOrderSummaryItemList().get(0).getMedCase().getCaseNum().equals("AE10013146X");
				assert refNumOrderList.get(1).getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Caneclled;
				assert refNumOrderList.get(2).getPatient() != null;
				assert refNumOrderList.get(2).getOneStopOrderSummaryItemList().get(0).getMedCase().getCaseNum().equals("POD 0610001S");
				assert refNumOrderList.get(2).getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Issued;
				assert refNumOrderList.get(3).getPatient() != null;
				assert refNumOrderList.get(3).getOneStopOrderSummaryItemList().get(0).getMedCase().getCaseNum().equals("SOPD9999001U");
				assert refNumOrderList.get(3).getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Caneclled;
//				oneStopPasManagerLocal oneStopPasManager = (oneStopPasManagerLocal) getValue("#{oneStopPasManager}");
//				oneStopPasManager.retrievePatientFromPasByOrderRefNumWithPopup(refNumOrderList);
//				List<OneStopSummary> refNumOneStopSummaryList = oneStopPasManager.getRefNumOneStopSummaryList();
//				assert refNumOneStopSummaryList.get(0).getPatient() != null;
//				assert refNumOneStopSummaryList.get(0).getPatient().getHkid().equals("M8688074");
//				assert refNumOneStopSummaryList.get(0).getOneStopOrderSummaryItemList().get(0).getCaseNum().equals("SOPD9999001U");
//				assert refNumOneStopSummaryList.get(0).getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Unvet;
//				assert refNumOneStopSummaryList.get(1).getPatient() != null;
//				assert refNumOneStopSummaryList.get(1).getPatient().getHkid().equals("Z1130545");
//				assert refNumOneStopSummaryList.get(1).getOneStopOrderSummaryItemList().get(0).getCaseNum().equals("AE10013146X");
//				assert refNumOneStopSummaryList.get(1).getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Caneclled;
//				assert refNumOneStopSummaryList.get(2).getPatient() != null;
//				assert refNumOneStopSummaryList.get(2).getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Issued;
//				assert refNumOneStopSummaryList.get(2).getOneStopOrderSummaryItemList().get(0).getCaseNum().equals("POD 0610001S");
//				assert refNumOneStopSummaryList.get(3).getPatient() != null;
//				assert refNumOneStopSummaryList.get(3).getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Caneclled;
//				assert refNumOneStopSummaryList.get(3).getOneStopOrderSummaryItemList().get(0).getCaseNum().equals("SOPD9999001U");
			}
		}.run();
		
		//Test with refNum with order is vetted with 5 digit
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("1020A", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				List<OneStopSummary> refNumOrderList = oneStopService.getRefNumOrderList();
				assert refNumOrderList == null;
				assert oneStopSummary.getPatient() != null;
				assert oneStopSummary.getPatient().getHkid().equals("UM8073753");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("VH 110000208");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderStatus() == OneStopOrderStatus.Issued;
			}
		}.run();
		
		//Test with refillNum 13 digit with refill schedule create and no refill start yet
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("VH11000020802", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("UM8073753");
				assert oneStopSummary.getOneStopRefillSummaryItemList().size() == 1;
				assert oneStopSummary.getOneStopRefillSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.ReillOrder;
				assert oneStopSummary.getOneStopRefillSummaryItemList().get(0).getTicketNum() == null;
				assert oneStopSummary.getOneStopRefillSummaryItemList().get(0).getOrderNum().equals("VH 110000208");
			}
		}.run();
		
		//Test with refillNum with 14 digit without refill seq
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("QMH11000020602", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("UM8073753");
				assert oneStopSummary.getOneStopRefillSummaryItemList().size() == 1;
				assert oneStopSummary.getOneStopRefillSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.ReillOrder;
				assert oneStopSummary.getOneStopRefillSummaryItemList().get(0).getTicketNum() == null;
				assert oneStopSummary.getOneStopRefillSummaryItemList().get(0).getOrderNum().equals("QMH110000206");
			}
		}.run();
		
		//Test with refillNum 16 digit with refill schedule create and no refill start yet
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("QMH1100002060201", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("UM8073753");
				assert oneStopSummary.getOneStopRefillSummaryItemList().size() == 1;
				assert oneStopSummary.getOneStopRefillSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.ReillOrder;
				assert oneStopSummary.getOneStopRefillSummaryItemList().get(0).getTicketNum() == null;
				assert oneStopSummary.getOneStopRefillSummaryItemList().get(0).getOrderNum().equals("QMH110000206");
			}
		}.run();

		//Test with refillNum with refill schedule create and refill start for serveral time but not completed yet
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("QMH1100010000301", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("M8688074");
				assert oneStopSummary.getOneStopRefillSummaryItemList().size() == 2;
				assert oneStopSummary.getOneStopRefillSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.ReillOrder;
				assert oneStopSummary.getOneStopRefillSummaryItemList().get(0).getTicketNum() == null;
				assert oneStopSummary.getOneStopRefillSummaryItemList().get(1).getTicketNum().equals("0888");
				assert oneStopSummary.getOneStopRefillSummaryItemList().get(0).getOrderNum().equals("QMH110001000");
			}
		}.run();
		
		//Test with refillNum with refill schedule create and refill completed
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("QMH1100010500201", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("UM8073753");
				assert oneStopSummary.getOneStopRefillSummaryItemList().size() == 1;
				assert oneStopSummary.getOneStopRefillSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.ReillOrder;
				assert oneStopSummary.getOneStopRefillSummaryItemList().get(0).getTicketNum().equals("0891");
				assert oneStopSummary.getOneStopRefillSummaryItemList().get(0).getOrderNum().equals("QMH110001050");
			}
		}.run();
		
		//Test with SFIRefillNum with SFIRefill schedule created and not dispended yet
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("QMHQMH1100002070201", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("M8688074");
				assert oneStopSummary.getOneStopRefillSummaryItemList().isEmpty() == true;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.SfiOrder;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("QMH110000207");
			}
		}.run();
		
		//Test with SFIRefillNum with SFIRefill schedule created and dispended
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("QMHQMH1100002090201", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("M8688074");
				assert oneStopSummary.getOneStopRefillSummaryItemList().size() == 1;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.SfiOrder;
				assert oneStopSummary.getOneStopRefillSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.SfiOrder;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("QMH110000209");
				assert oneStopSummary.getOneStopRefillSummaryItemList().get(0).getOrderNum().equals("QMH110000209");
				assert oneStopSummary.getOneStopRefillSummaryItemList().get(0).getTicketNum().equals("1110");
			}
		}.run();
		
		//Test with ticket num with 12 digit and ticket num is invalid
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("231512001110", true);
				String errMsg = (String) getValue("#{errMsg}");
				assert errMsg.equals("0034");
			}
		}.run();
		
		//Test with ticket num with 12 digit and ticket is used for refill
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("031512001110", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("M8688074");
				assert oneStopSummary.getOneStopRefillSummaryItemList().size() == 1;
				assert oneStopSummary.getOneStopRefillSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.SfiOrder;
				assert oneStopSummary.getOneStopRefillSummaryItemList().get(0).getOrderNum().equals("QMH110000209");
				assert oneStopSummary.getOneStopRefillSummaryItemList().get(0).getTicketNum().equals("1110");
			}
		}.run();
		
		//Test with ticket num with 12 digit and the ticket is vetted without refill
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("033012000709", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("M8688074");
				assert oneStopSummary.getOneStopOrderSummaryItemList().size() == 1;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.DispOrder;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("QMH110000204");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getTicketNum().equals("0709");
			}
		}.run();
		
		//Test with ticket num with 12 digit and the ticket is unvet and ticket is associated
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("032912000512", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient() == null;
				oneStopPasManagerLocal oneStopPasManager = (oneStopPasManagerLocal) getValue("#{oneStopPasManager}");
				oneStopPasManager.retrievePatientAndAppointmentFromPasByCaseNum("032912000512");
				oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("UM8073753");
				assert oneStopSummary.getOneStopOrderSummaryItemList().size() == 1;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.MedOrder;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("QMH110000192");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getTicketNum().equals("0512");
			}
		}.run();
		
		//Test with ticket num with 14 digit with check digit fail
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("91033012000707", true);
				String errMsg = (String) getValue("#{errMsg}");
				assert errMsg.equals("0034");
			}
		}.run();
		
		//Test with ticket num with 14 digit with check digit success
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("97033012000707", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("M8688074");
				assert oneStopSummary.getOneStopOrderSummaryItemList().size() == 1;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.DispOrder;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("QMH110000202");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getTicketNum().equals("0707");
			}
		}.run();
		
		//Test with ticket num with 14 digit without check digit
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("30032011070701", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("M8688074");
				assert oneStopSummary.getOneStopOrderSummaryItemList().size() == 1;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOneStopOrderType() == OneStopOrderType.DispOrder;
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getOrderNum().equals("QMH110000202");
				assert oneStopSummary.getOneStopOrderSummaryItemList().get(0).getTicketNum().equals("0707");
			}
		}.run();
		
		//Test with hkid with invalid digit
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("A1234561", true);
				String errMsg = (String) getValue("#{errMsg}");
				assert errMsg.equals("0033");
			}
		}.run();
		
		//Test with hkid
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("M8688074", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getSearchTextType() == SearchTextType.Hkid;
				oneStopPasManagerLocal oneStopPasManager = (oneStopPasManagerLocal) getValue("#{oneStopPasManager}");
				oneStopPasManager.retrievePatientAndAppointmentFromPasByHkid("M8688074", true);
				oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getName().equals("CHUI, HEALTHY");
				assert oneStopSummary.getOneStopOrderSummaryItemList().isEmpty() == false;
				assert oneStopSummary.getOneStopRefillSummaryItemList().isEmpty() == false;
				List<MedCase> appointmentList = (List<MedCase>) getValue("#{appointmentList}");
				assert appointmentList.isEmpty() == false;
			}
		}.run();
		
		//Test with case num and retrieve appointment
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("SOPD9999001U", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient() == null;
				assert oneStopSummary.getOneStopOrderSummaryItemList().isEmpty() == false;
				assert oneStopSummary.getOneStopRefillSummaryItemList().isEmpty() == false;
				oneStopPasManagerLocal oneStopPasManager = (oneStopPasManagerLocal) getValue("#{oneStopPasManager}");
				oneStopPasManager.retrievePatientAndAppointmentFromPasByCaseNum("SOPD9999001U");
				oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("M8688074");
				List<MedCase> appointmentList = (List<MedCase>) getValue("#{appointmentList}");
				assert appointmentList.isEmpty() == false;
			}
		}.run();
				
		//Test with hkid for manual order and no record found in local db
		/*new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("UM8073753", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getSearchTextType() == SearchTextType.HKID;
				assert oneStopSummary.getOneStopOrderSummaryItemList().isEmpty() == true;
				assert oneStopSummary.getOneStopRefillSummaryItemList().isEmpty() == true;
				oneStopPasManagerLocal oneStopPasManager = (oneStopPasManagerLocal) getValue("#{oneStopPasManager}");
				oneStopPasManager.retrievePatientAndAppointmentFromPasByCaseNumOrHkid("UM8073753");
				oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getName().equals("UNKNOWN");
				assert oneStopSummary.getOneStopOrderSummaryItemList().isEmpty() == true;
				assert oneStopSummary.getOneStopRefillSummaryItemList().isEmpty() == true;
			}
		}.run();
		
		
		//Test with casenum for manual order and no record found in local db
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				OneStopServiceLocal oneStopService = (OneStopServiceLocal) getValue("#{oneStopService}");
				oneStopService.retrieveOrder("SOPD9999001U", true);
				OneStopSummary oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient() == null;
				assert oneStopSummary.getOneStopOrderSummaryItemList().isEmpty() == true;
				assert oneStopSummary.getOneStopRefillSummaryItemList().isEmpty() == true;
				oneStopPasManagerLocal oneStopPasManager = (oneStopPasManagerLocal) getValue("#{oneStopPasManager}");
				oneStopPasManager.retrievePatientAndAppointmentFromPasByCaseNumOrHkid("SOPD9999001U");
				oneStopSummary = (OneStopSummary) getValue("#{oneStopSummary}");
				assert oneStopSummary.getPatient().getHkid().equals("M8688074");
				assert oneStopSummary.getPatient().getName().equals("CHUI, HEALTHY");
				assert oneStopSummary.getOneStopOrderSummaryItemList().isEmpty() == true;
				assert oneStopSummary.getOneStopRefillSummaryItemList().isEmpty() == true;
			}
		}.run();*/
	}
}
