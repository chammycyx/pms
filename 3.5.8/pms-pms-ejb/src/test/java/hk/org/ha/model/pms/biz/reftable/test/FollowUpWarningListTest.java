package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.reftable.FollowUpWarning;
import hk.org.ha.model.pms.persistence.reftable.LocalWarning;
import hk.org.ha.model.pms.vo.reftable.FollowUpWarningItem;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class FollowUpWarningListTest extends SeamTest {

	@Test
	public void testFollowUpWarningListComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		//retrieve FollowUpWarning List
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{				
				invokeMethod("#{followUpWarningListService.retrieveFollowUpWarningList()}");
				List<FollowUpWarning> followUpWarningList = (List<FollowUpWarning>) getValue("#{followUpWarningList}");
				
				assert followUpWarningList.size()==4;
				
				for ( FollowUpWarning followUpWarning : followUpWarningList ) {
					if ( StringUtils.equals(followUpWarning.getItemCode(), "ACET01")){
						assert followUpWarning.getFollowUpWarningItemList().size() == 1;
					} else if ( StringUtils.equals(followUpWarning.getItemCode(), "CLOB02")){
						assert followUpWarning.getFollowUpWarningItemList().size() == 3;
						for ( FollowUpWarningItem followUpWarningItem : followUpWarning.getFollowUpWarningItemList() ){
							
							if ( StringUtils.equals(followUpWarningItem.getCurrentHqWarnCatCode(), "ID") ) {
								assert followUpWarningItem.isFollowCurrentHqWarnCode();
							} else {
								assert !followUpWarningItem.isFollowCurrentHqWarnCode();
							}
						}
						
					} else if ( StringUtils.equals(followUpWarning.getItemCode(), "MULT03")){
						assert followUpWarning.getFollowUpWarningItemList().size() == 1;
					} else if ( StringUtils.equals(followUpWarning.getItemCode(), "PARA03")){
						assert followUpWarning.getFollowUpWarningItemList().size() == 2;
					}
				}
			}
		}.run();
		
		//update FollowUpWarning List
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{followUpWarningListService.retrieveFollowUpWarningList()}");
				List<FollowUpWarning> followUpWarningList = (List<FollowUpWarning>) getValue("#{followUpWarningList}");
				
				assert followUpWarningList.size()==4;
				
				List<FollowUpWarning> updateFollowUpWarningList = new ArrayList<FollowUpWarning>();
				List<LocalWarning> updateFollowUpLocalWarningList = new ArrayList<LocalWarning>();
				
				
				for (FollowUpWarning followUpWarning : followUpWarningList){
					if (StringUtils.equals("CLOB02", followUpWarning.getItemCode())) {
						FollowUpWarning updateFollowUpWarning = new FollowUpWarning();
						updateFollowUpWarning.setId(followUpWarning.getId());
						updateFollowUpWarning.setItemCode(followUpWarning.getItemCode());
						updateFollowUpWarning.setMarkDelete(true);
						updateFollowUpWarningList.add(updateFollowUpWarning);
						for ( FollowUpWarningItem followUpWarningItem : followUpWarning.getFollowUpWarningItemList() ) {
							if ( !StringUtils.equals(followUpWarningItem.getCurrentHqWarnCatCode(), "ID") ){
								LocalWarning localWarning = new LocalWarning();
								localWarning.setId(followUpWarningItem.getLocalWarning().getId());
								localWarning.setItemCode(followUpWarning.getItemCode());
								localWarning.setWarnCode("12");
								localWarning.setOrgWarnCode(followUpWarningItem.getCurrentHqWarnCode());
								localWarning.setSortSeq(followUpWarningItem.getSortSeq());
								updateFollowUpLocalWarningList.add(localWarning);
							} else {
								LocalWarning localWarning = new LocalWarning();
								localWarning.setId(followUpWarningItem.getLocalWarning().getId());
								localWarning.setItemCode(followUpWarning.getItemCode());
								localWarning.setWarnCode("");	
								localWarning.setSortSeq(followUpWarningItem.getSortSeq());
								updateFollowUpLocalWarningList.add(localWarning);
							}
						}
					
						break;
					}
				}

				Contexts.getSessionContext().set("updateFollowUpWarningList", updateFollowUpWarningList);
				Contexts.getSessionContext().set("updateFollowUpLocalWarningList", updateFollowUpLocalWarningList);
				invokeMethod("#{followUpWarningListService.updateFollowUpWarningList(updateFollowUpWarningList, updateFollowUpLocalWarningList)}");
			}
		}.run();
		
		//retrieve FollowUpWarning List and localWarningList
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{				
				invokeMethod("#{followUpWarningListService.retrieveFollowUpWarningList()}");
				List<FollowUpWarning> followUpWarningList = (List<FollowUpWarning>) getValue("#{followUpWarningList}");
				
				assert followUpWarningList.size()==3;
				
				for ( FollowUpWarning followUpWarning : followUpWarningList ) {
					if ( StringUtils.equals(followUpWarning.getItemCode(), "CLOB02")){
						assert false;
					}
				}
				
				DmDrug dmDrug = new DmDrug();
				dmDrug.setItemCode("CLOB02");
				Contexts.getSessionContext().set("dmDrug", dmDrug);
				
				invokeMethod("#{localWarningListService.retrieveLocalWarningList(dmDrug)}");
				List<LocalWarning> localWarningList = (List<LocalWarning>) getValue("#{localWarningList}");
				
				assert localWarningList.size()==0;
			}
		}.run();
		
		//retrieve FollowUpWarningRpt List
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{				
				List<FollowUpWarning> followUpWarningRptList = (List<FollowUpWarning>) invokeMethod("#{followUpWarningListService.retrieveFollowUpWarningRptList()}");
				
				assert followUpWarningRptList.size()>0 ;
			}
		}.run();
	}	
}
