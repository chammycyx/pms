package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.reftable.CustomLabelManagerLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.CustomLabel;
import hk.org.ha.model.pms.udt.label.CustomLabelType;
import hk.org.ha.model.pms.vo.label.HkidBarcodeLabel;
import hk.org.ha.model.pms.vo.label.SpecialLabel;
import hk.org.ha.model.pms.vo.label.SpecialLabelInstruction;

import java.util.ArrayList;
import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class CustomLabelTest extends SeamTest {

	@Test
	public void testCustomLabelComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		// retrieve custom label - hkidLabel
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				CustomLabelManagerLocal customLabelManager = (CustomLabelManagerLocal) getValue("#{customLabelManager}");
				CustomLabel customLabel = customLabelManager.retrieveCustomLabel("A1234563", CustomLabelType.HkIdBarCode);
				assert customLabel.getName().equals("A1234563");
			}
		}.run();
		
		// update custom label - hkidLabel
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				Workstore workstore = (Workstore) getValue("#{workstore}");
				CustomLabelManagerLocal customLabelManager = (CustomLabelManagerLocal) getValue("#{customLabelManager}");
				
				HkidBarcodeLabel hkidBarcodeLabel = new HkidBarcodeLabel();
				hkidBarcodeLabel.setHkid("A224466A");
				hkidBarcodeLabel.setPatName("CHUNG ON HONG");
				
				CustomLabel customLabel = new CustomLabel();
				customLabel.setType(CustomLabelType.HkIdBarCode);
				customLabel.setHkidBarcodeLabel(hkidBarcodeLabel);
				customLabel.setName("A224466A");
				customLabel.setWorkstore(workstore);
				customLabelManager.updateCustomLabel(customLabel);
				
				CustomLabel cl = customLabelManager.retrieveCustomLabel("A224466A", CustomLabelType.HkIdBarCode);
				assert cl.getName().equals("A224466A");
			}
		}.run();
		
		//remove hkid barcode label
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				CustomLabelManagerLocal customLabelManager = (CustomLabelManagerLocal) getValue("#{customLabelManager}");
				customLabelManager.removeCustomLabel("A224466A", CustomLabelType.HkIdBarCode);
				
				CustomLabel customLabel = customLabelManager.retrieveCustomLabel("A224466A", CustomLabelType.HkIdBarCode);
				assert customLabel == null;
			}
		}.run();
		
		//retrieve special label - custom Label List 
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				CustomLabelManagerLocal customLabelManager = (CustomLabelManagerLocal) getValue("#{customLabelManager}");
				List<CustomLabel> customLabelList = customLabelManager.retrieveCustomLabelList(CustomLabelType.Special);
				assert customLabelList.size() > 0;
			}
		}.run();
		
		// update custom label List - special label
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				Workstore workstore = (Workstore) getValue("#{workstore}");
				
				CustomLabelManagerLocal customLabelManager = (CustomLabelManagerLocal) getValue("#{customLabelManager}");
				List<CustomLabel> customLabelList = customLabelManager.retrieveCustomLabelList(CustomLabelType.Special);
				
				SpecialLabel specialLabel = new SpecialLabel();
				specialLabel.setLabelNum("990");
				
				List<SpecialLabelInstruction> specialLabelInstructionList = new ArrayList<SpecialLabelInstruction>();
				specialLabel.setSpecialLabelInstructionList(specialLabelInstructionList);
				
				CustomLabel customLabel = new CustomLabel();
				customLabel.setSpecialLabel(specialLabel);
				customLabel.setType(CustomLabelType.Special);
				customLabel.setWorkstore(workstore);
				customLabel.setName(specialLabel.getLabelNum());
				customLabelList.add(customLabel);

				customLabelManager.updateCustomLabelList(customLabelList);
			}
		}.run();

	}	
}
