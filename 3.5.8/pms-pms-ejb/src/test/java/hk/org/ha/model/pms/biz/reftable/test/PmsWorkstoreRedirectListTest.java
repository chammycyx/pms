package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.dms.persistence.PmsWorkstoreRedirect;

import java.util.ArrayList;
import java.util.List;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class PmsWorkstoreRedirectListTest extends SeamTest {

	@Test
	public void testPmsWorkstoreRedirectListComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		//retrieve PmsWorkstoreRedirectList
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{pmsWorkstoreRedirectListService.retrievePmsWorkstoreRedirectList()}");
				List<PmsWorkstoreRedirect> pmsWorkstoreRedirectList = (List<PmsWorkstoreRedirect>)getValue("#{pmsWorkstoreRedirectList}");
				
				assert pmsWorkstoreRedirectList.size()==1;
			}
		}.run();

		//update PmsWorkstoreRedirectList
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				List<PmsWorkstoreRedirect> updatePmsWorkstoreRedirectList = new ArrayList<PmsWorkstoreRedirect>();
				
				Contexts.getSessionContext().set("updatePmsWorkstoreRedirectList", updatePmsWorkstoreRedirectList);
				invokeMethod("#{pmsWorkstoreRedirectListService.updatePmsWorkstoreRedirectList(updatePmsWorkstoreRedirectList)}");
			}
		}.run();
	}	
}
