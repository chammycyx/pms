package hk.org.ha.model.pms.biz.mock;

import javax.jms.JMSException;
import javax.naming.NamingException;

import hk.org.ha.fmk.pms.jms.MessageCreator;
import hk.org.ha.fmk.pms.jms.MessageProducerInf;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;

@Startup
@Name("pharmInboxGravityMessageProducer")
@Scope(ScopeType.APPLICATION)
public class MockPharmInboxGravityMessageProducer implements MessageProducerInf {

	public void send(MessageCreator arg0) throws JMSException, NamingException {
	}
}
