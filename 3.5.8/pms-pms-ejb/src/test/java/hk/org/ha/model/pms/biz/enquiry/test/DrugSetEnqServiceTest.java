package hk.org.ha.model.pms.biz.enquiry.test;

import hk.org.ha.model.pms.biz.enquiry.DrugSetListServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroupMapping;
import hk.org.ha.model.pms.vo.enquiry.DrugSet;
import hk.org.ha.model.pms.vo.enquiry.DrugSetDtl;

import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class DrugSetEnqServiceTest extends SeamTest {

	@Test
	public void testDrugSetEnqComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		//retrieve workstoreGroupMappingList
		new ComponentTest() 
		{			
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{drugSetListService.retrieveWorkstoreGroupMappingList}");
				List<WorkstoreGroupMapping> workstoreGroupMappingList = (List<WorkstoreGroupMapping>) getValue("#{workstoreGroupMappingList}");
				List<DrugSet> drugSetList = (List<DrugSet>) getValue("#{drugSetList}");
				List<DrugSetDtl> drugSetDtlList = (List<DrugSetDtl>) getValue("#{drugSetDtlList}");
				List<String> subLevelList = (List<String>) getValue("#{subLevelList}");
				
				assert workstoreGroupMappingList.size()>0;
				assert drugSetList.size()==2;
				assert drugSetDtlList.size()==1;
				assert subLevelList.isEmpty();
				
			}
		}.run();
		
		//retrieve retrieveDrugSetDtlList
		new ComponentTest() 
		{			
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{

				List<DrugSet> drugSetList = (List<DrugSet>) getValue("#{drugSetList}");
				assert drugSetList.size()==2;
				
				DrugSet drugSet = drugSetList.get(1);
				
				DrugSetListServiceLocal drugSetListService = (DrugSetListServiceLocal) getValue("#{drugSetListService}");
				drugSetListService.retrieveDrugSetDtlList("QMH", drugSet.getDrugSetNum(), drugSet.getDrugSetCode(), drugSet.getDrugHospCode(), drugSet.getSubLevelFlag());	
				
				List<DrugSetDtl> drugSetDtlList = (List<DrugSetDtl>) getValue("#{drugSetDtlList}");
				List<String> subLevelList = (List<String>) getValue("#{subLevelList}");

				assert drugSetDtlList.size()==2;
				assert subLevelList.size()==2;
				
				for (String subLevel : subLevelList) {
					if ( "subLevel1".equals(subLevel) ) {
						assert true;
					} else if  ( "subLevel2".equals(subLevel) ) {
						assert true;
					} else {
						assert false;
					}
				}
				
			}
		}.run();
	}
}