package hk.org.ha.model.pms.biz.mock;

import java.util.List;

import hk.org.ha.model.pms.uam.udt.AuthorizeStatus;
import hk.org.ha.model.pms.uam.vo.AuthenticateCriteria;
import hk.org.ha.model.pms.uam.vo.AuthenticateResult;
import hk.org.ha.model.pms.uam.vo.ehr.EhrViewerUser;
import hk.org.ha.service.biz.pms.uam.interfaces.UamServiceJmsRemote;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;

@Startup
@Name("uamServiceProxy")
@Scope(ScopeType.APPLICATION)
public class MockUamServiceProxy implements UamServiceJmsRemote {

	@Override
	public AuthorizeStatus authenticate(AuthenticateCriteria authenticateCriteria) {
		if( "sfiExemptReason".equals(authenticateCriteria.getTarget()) ){
			if( "itdSfiFp".equals(authenticateCriteria.getUserName()) ){
				return AuthorizeStatus.NoAccess;
			}else if( "itdSfiExempt".equals(authenticateCriteria.getUserName()) ){
				if( "XXXZZZ".equals(authenticateCriteria.getPassword()) ){
					return AuthorizeStatus.Succeed;
				}else{
					return AuthorizeStatus.Invalid;
				}
			}
		}
		return AuthorizeStatus.Succeed;
	}

	@Override
	public AuthenticateResult authenticateWithUserInfo(AuthenticateCriteria authenticateCriteria) {
		
		AuthenticateResult result = new AuthenticateResult();
		result.setStatus(authenticate(authenticateCriteria));
		if (result.getStatus() != AuthorizeStatus.Invalid) {
			result.setUserName("test");
			result.setDisplayName("Test User");
		}
		return result;
	}

	@Override
	public List<EhrViewerUser> retrieveEhrViewerUserList() {
		// TODO Auto-generated method stub
		return null;
	}
}
