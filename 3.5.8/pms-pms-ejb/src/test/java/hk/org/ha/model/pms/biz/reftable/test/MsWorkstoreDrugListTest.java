package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.dms.persistence.MsWorkstoreDrug;

import java.util.List;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class MsWorkstoreDrugListTest extends SeamTest {

	@Test
	public void testMsWorkstoreDrugListComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		//retrieve msWorkstoreDrugList
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{msWorkstoreDrugListService.retrieveMsWorkstoreDrugList('PARA01')}");
				List<MsWorkstoreDrug> msWorkstoreDrugList = (List<MsWorkstoreDrug>)getValue("#{msWorkstoreDrugList}");
				
				assert msWorkstoreDrugList.size()==1;
			}
		}.run();

		//update msWorkstoreDrugList
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{msWorkstoreDrugListService.retrieveMsWorkstoreDrugList('PARA01')}");
				List<MsWorkstoreDrug> msWorkstoreDrugList = (List<MsWorkstoreDrug>)getValue("#{msWorkstoreDrugList}");
				
				assert msWorkstoreDrugList.size()==1;
				
				Contexts.getSessionContext().set("updateMsWorkstoreDrugList", msWorkstoreDrugList);
				invokeMethod("#{msWorkstoreDrugListService.updateMsWorkstoreDrugList(updateMsWorkstoreDrugList)}");
			}
		}.run();
	}	
}
