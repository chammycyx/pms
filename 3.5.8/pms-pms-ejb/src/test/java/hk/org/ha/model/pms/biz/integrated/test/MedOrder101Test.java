package hk.org.ha.model.pms.biz.integrated.test;

import hk.org.ha.model.pms.biz.order.AutoDispManagerLocal;
import hk.org.ha.model.pms.biz.order.PmsServiceLocal;
import hk.org.ha.model.pms.og.biz.OgInMessageConverter;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.udt.disp.OpTrxType;
import hk.org.ha.model.pms.udt.reftable.OperationProfileType;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class MedOrder101Test extends SeamTest {

	@Test
	public void testMedOrder101() throws Exception {

		final String xmlFileName = "integrated/newOrder101.xml";
		final String orderNo = "100000101";
		final Workstore workstore = new Workstore("QMH","WKS1");
		final String workstationCode = "0002";
		final String userName = "itdadmin";
		final String patientHkid = "A1111119";
		final String patientName = userName + " " + workstationCode;
		
		// Create Order
		new ComponentTest() {
			protected void testComponents() throws Exception
			{
				InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(xmlFileName);				
				String xmlString = IOUtils.toString(inputStream);				
				xmlString = xmlString.replace("${ORDER_NUM}", orderNo);
				
				OgInMessageConverter ogInMessageConverter = (OgInMessageConverter) getValue("#{ogInMessageConverter}");
				MedOrder medOrder = ogInMessageConverter.convertOrder(OpTrxType.OpCreateMoeOrder, xmlString);
													
				Workstore workstore = medOrder.getWorkstore();					
				workstore.setHospital(new Hospital(workstore.getHospCode()));

				IdGenerator.getInstance().reflectSetId(medOrder);
				
				PmsServiceLocal pmsService = (PmsServiceLocal) getValue("#{pmsService}");
				pmsService.receiveNewOrder(medOrder);
			}
		}.run();       
			
		new ComponentTest() {
			
			protected void testComponents() throws Exception
			{
				AutoDispManagerLocal autoDispManager = (AutoDispManagerLocal) getValue("#{autoDispManager}");	

				autoDispManager.login(userName, workstore, workstationCode);
				
				autoDispManager.genTicket();	
				
				autoDispManager.retrieveOrder(workstore.getHospCode() + orderNo);
				
				autoDispManager.retrievePatientAndMedCase(patientHkid, patientName);
				
				autoDispManager.checkTicket();
				
				autoDispManager.startVetting();
				
				//TODO : should enable but need to update the mock of CorpPmsService.endVetting() to support pharm remark first. 
				//autoDispManager.removeItemsIfNotConverted();

				autoDispManager.endVetting();

				OperationProfile activeProfile = autoDispManager.getActiveProfile(workstore);
				if (activeProfile.getType() != OperationProfileType.Pms) {
				
					autoDispManager.drugPick();
					
					autoDispManager.assembling();
					
					autoDispManager.checkOrder();
					
					autoDispManager.issueOrder();
				}					
				
				autoDispManager.logout();
			}
		}.run();
		
	}
	
}
