package hk.org.ha.model.pms.biz.printing.test;

import hk.org.ha.model.pms.biz.printing.PrintLabelManagerLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.persistence.reftable.OperationProp;
import hk.org.ha.model.pms.persistence.reftable.OperationWorkstation;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.reftable.OperationProfileType;
import hk.org.ha.model.pms.udt.reftable.PrinterType;
import hk.org.ha.model.pms.vo.printing.PrintLabelDetail;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class PrintLabelTest extends SeamTest {

	private OperationProfile activeProfile = new OperationProfile();
	@Test
	public void testPrintLabelComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();

		//NonPeak mode
//		new ComponentTest() 
//		{
//			@SuppressWarnings("unchecked")
//			protected void testComponents() throws Exception 
//			{
//				invokeMethod("#{operationModeListService.retrieveOperationModeList}");
//				List<OperationProfile> operationProfileList = (List<OperationProfile>)getValue("#{operationProfileList}");
//				
//				assert operationProfileList!=null;
//
//			//	OperationProfile activeProfile = new OperationProfile();
//				OperationProfile oldActiveProfile = new OperationProfile();
//				
//				List<OperationProfile> updateOperationProfileList = new ArrayList<OperationProfile>();
//				List<OperationProfile> delOperationProfileList = new ArrayList<OperationProfile>();
//				
//				
//				for (OperationProfile operationProfile:operationProfileList) {
//					if (operationProfile.isActive()) {
//						delOperationProfileList.add(operationProfile);
//						oldActiveProfile = operationProfile;
//					}
//					if(operationProfile.getType().equals(OperationProfileType.NonPeak)){
//						activeProfile.setActive(true);
//						activeProfile.setType(operationProfile.getType());
//						activeProfile.setName(operationProfile.getName());
//						
//						List<OperationProp> operationPropList = new ArrayList<OperationProp>();
//						
//						for (OperationProp operationProp:operationProfile.getOperationPropList()) {
//							OperationProp activeOperationProp = new OperationProp();
////							activeOperationProp.setName(operationProp.getName());
////							activeOperationProp.setType(operationProp.getType());
////							activeOperationProp.setDescription(operationProp.getDescription());
//							activeOperationProp.setProp(operationProp.getProp());
//							activeOperationProp.setValue(operationProp.getValue());
//							activeOperationProp.setSortSeq(operationProp.getSortSeq());
//							activeOperationProp.setOperationProfile(activeProfile);
//							operationPropList.add(activeOperationProp);
//						}
//						List<OperationWorkstation> operationWorkstationList = new ArrayList<OperationWorkstation>();
//						for (OperationWorkstation operationWorkstation:operationProfile.getOperationWorkstationList()) {
//							OperationWorkstation activeOperationWorkstation = new OperationWorkstation();
//							activeOperationWorkstation.setPrinterConnectionType(operationWorkstation.getPrinterConnectionType());
//							activeOperationWorkstation.setPrinterType(operationWorkstation.getPrinterType());
//							activeOperationWorkstation.setType(operationWorkstation.getType());
//							activeOperationWorkstation.setEnableFlag(operationWorkstation.getEnableFlag());
//							activeOperationWorkstation.setRedirectPrintStation(operationWorkstation.getRedirectPrintStation());
//							activeOperationWorkstation.setIssueWindowNum(operationWorkstation.getIssueWindowNum());
//							activeOperationWorkstation.setOperationProfile(activeProfile);
//							activeOperationWorkstation.setWorkstation(operationWorkstation.getWorkstation());
//							activeOperationWorkstation.setMachine(operationWorkstation.getMachine());
//							operationWorkstationList.add(activeOperationWorkstation);
//						}
//						
//						activeProfile.setOperationPropList(operationPropList);
//						activeProfile.setOperationWorkstationList(operationWorkstationList);
//						
//					}else{
//						operationProfile.setActive(false);
//					}
//				}
//				
//				operationProfileList.remove(oldActiveProfile);
//
//				operationProfileList.add(activeProfile);
//				updateOperationProfileList.add(activeProfile);
//
//				List<OperationProp> updateOperationPropList = new ArrayList<OperationProp>();
//				List<OperationWorkstation> updateOperationWorkstationList = new ArrayList<OperationWorkstation>();
//				List<Workstation> updateWorkstationList = new ArrayList<Workstation>();
//				
//				Contexts.getSessionContext().set("updateOperationProfileList", updateOperationProfileList);
//				Contexts.getSessionContext().set("delOperationProfileList", delOperationProfileList);
//				Contexts.getSessionContext().set("updateOperationPropList", updateOperationPropList);
//				Contexts.getSessionContext().set("updateOperationWorkstationList", updateOperationWorkstationList);
//				Contexts.getSessionContext().set("updateWorkstationList", updateWorkstationList);
//				invokeMethod("#{operationModeListService.updateOperationModeList(updateOperationProfileList, delOperationProfileList, updateOperationPropList, updateOperationWorkstationList, updateWorkstationList)}");
//			}
//		}.run();
//		
//		new ComponentTest()
//		{
//			protected void testComponents() throws Exception
//			{
//				PrintLabelManagerLocal printLabelManager = (PrintLabelManagerLocal)getValue("#{printLabelManager}");
//				PrintLabelDetail printLabelDetail = printLabelManager.retrievePrintLabelDetailOnNonPeakMode();
//		
//				assert printLabelDetail!=null;
//				assert printLabelDetail.getItemLocation()==null;
//				assert (StringUtils.equals(printLabelDetail.getWorkstation().getWorkstationCode(), "0002") || StringUtils.equals(printLabelDetail.getWorkstation().getWorkstationCode(), "0004"));
//				assert (StringUtils.equals(printLabelDetail.getPrinterWorkstation().getWorkstationCode(), "0002") || StringUtils.equals(printLabelDetail.getPrinterWorkstation().getWorkstationCode(), "0004"));
//				assert (StringUtils.equals(printLabelDetail.getDefaultPrinterWorkstation().getWorkstationCode(), "0002") || StringUtils.equals(printLabelDetail.getDefaultPrinterWorkstation().getWorkstationCode(), "0004"));
//				
//			}
//		}.run();
//		
//		//EDS mode - one Urgent printer and one DefaultAndUrgent printer
//		new ComponentTest() 
//		{
//			@SuppressWarnings("unchecked")
//			protected void testComponents() throws Exception 
//			{
//				invokeMethod("#{operationModeListService.retrieveOperationModeList}");
//				List<OperationProfile> operationProfileList = (List<OperationProfile>)getValue("#{operationProfileList}");
//				
//				assert operationProfileList!=null;
//
//				OperationProfile activeProfile = new OperationProfile();
//				OperationProfile oldActiveProfile = new OperationProfile();
//				
//				List<OperationProfile> updateOperationProfileList = new ArrayList<OperationProfile>();
//				List<OperationProfile> delOperationProfileList = new ArrayList<OperationProfile>();
//				
//				
//				for (OperationProfile operationProfile:operationProfileList) {
//					if (operationProfile.isActive()) {
//						delOperationProfileList.add(operationProfile);
//						oldActiveProfile = operationProfile;
//					}
//					if(operationProfile.getType().equals(OperationProfileType.Eds)){
//						activeProfile.setActive(true);
//						activeProfile.setType(operationProfile.getType());
//						activeProfile.setName(operationProfile.getName());
//						
//						List<OperationProp> operationPropList = new ArrayList<OperationProp>();
//						
//						for (OperationProp operationProp:operationProfile.getOperationPropList()) {
//							OperationProp activeOperationProp = new OperationProp();
////							activeOperationProp.setName(operationProp.getName());
////							activeOperationProp.setType(operationProp.getType());
////							activeOperationProp.setDescription(operationProp.getDescription());
//							activeOperationProp.setProp(operationProp.getProp());
//							activeOperationProp.setValue(operationProp.getValue());
//							activeOperationProp.setSortSeq(operationProp.getSortSeq());
//							activeOperationProp.setOperationProfile(activeProfile);
//							operationPropList.add(activeOperationProp);
//						}
//						List<OperationWorkstation> operationWorkstationList = new ArrayList<OperationWorkstation>();
//						for (OperationWorkstation operationWorkstation:operationProfile.getOperationWorkstationList()) {
//							OperationWorkstation activeOperationWorkstation = new OperationWorkstation();
//							activeOperationWorkstation.setPrinterConnectionType(operationWorkstation.getPrinterConnectionType());
//							activeOperationWorkstation.setPrinterType(operationWorkstation.getPrinterType());
//							activeOperationWorkstation.setType(operationWorkstation.getType());
//							activeOperationWorkstation.setEnableFlag(operationWorkstation.getEnableFlag());
//							activeOperationWorkstation.setRedirectPrintStation(operationWorkstation.getRedirectPrintStation());
//							activeOperationWorkstation.setIssueWindowNum(operationWorkstation.getIssueWindowNum());
//							activeOperationWorkstation.setOperationProfile(activeProfile);
//							activeOperationWorkstation.setWorkstation(operationWorkstation.getWorkstation());
//							activeOperationWorkstation.setMachine(operationWorkstation.getMachine());
//							operationWorkstationList.add(activeOperationWorkstation);
//						}
//						
//						activeProfile.setOperationPropList(operationPropList);
//						activeProfile.setOperationWorkstationList(operationWorkstationList);
//						
//					}else{
//						operationProfile.setActive(false);
//					}
//				}
//				
//				operationProfileList.remove(oldActiveProfile);
//
//				operationProfileList.add(activeProfile);
//				updateOperationProfileList.add(activeProfile);
//
//				List<OperationProp> updateOperationPropList = new ArrayList<OperationProp>();
//				List<OperationWorkstation> updateOperationWorkstationList = new ArrayList<OperationWorkstation>();
//				List<Workstation> updateWorkstationList = new ArrayList<Workstation>();
//				
//				Contexts.getSessionContext().set("updateOperationProfileList", updateOperationProfileList);
//				Contexts.getSessionContext().set("delOperationProfileList", delOperationProfileList);
//				Contexts.getSessionContext().set("updateOperationPropList", updateOperationPropList);
//				Contexts.getSessionContext().set("updateOperationWorkstationList", updateOperationWorkstationList);
//				Contexts.getSessionContext().set("updateWorkstationList", updateWorkstationList);
//				invokeMethod("#{operationModeListService.updateOperationModeList(updateOperationProfileList, delOperationProfileList, updateOperationPropList, updateOperationWorkstationList, updateWorkstationList)}");
//			}
//		}.run();
//		
//		//Urgent print
//		new ComponentTest()
//		{
//			protected void testComponents() throws Exception
//			{
//				PrintLabelManagerLocal printLabelManager = (PrintLabelManagerLocal)getValue("#{printLabelManager}");
//				PrintLabelDetail printLabelDetail = printLabelManager.retrievePrintLabelDetailOnEdsMode("PARA01", 7, Boolean.TRUE);
//		
//				assert printLabelDetail!=null;
//				assert printLabelDetail.getItemLocation()==null;
//				assert (StringUtils.equals(printLabelDetail.getWorkstation().getWorkstationCode(), "0004") || StringUtils.equals(printLabelDetail.getWorkstation().getWorkstationCode(), "0005"));
//				assert (StringUtils.equals(printLabelDetail.getPrinterWorkstation().getWorkstationCode(), "0004") || StringUtils.equals(printLabelDetail.getPrinterWorkstation().getWorkstationCode(), "0005"));
//				assert (StringUtils.equals(printLabelDetail.getDefaultPrinterWorkstation().getWorkstationCode(), "0003") || StringUtils.equals(printLabelDetail.getDefaultPrinterWorkstation().getWorkstationCode(), "0004"));
//			}
//		}.run();
//		
//		//EDS mode - one Urgent printer
//		new ComponentTest() 
//		{
//			@SuppressWarnings("unchecked")
//			protected void testComponents() throws Exception 
//			{
//				invokeMethod("#{operationModeListService.retrieveOperationModeList}");
//				List<OperationProfile> operationProfileList = (List<OperationProfile>)getValue("#{operationProfileList}");
//				
//				assert operationProfileList!=null;
//
//				OperationProfile activeProfile = new OperationProfile();
//				OperationProfile oldActiveProfile = new OperationProfile();
//				
//				List<OperationProfile> updateOperationProfileList = new ArrayList<OperationProfile>();
//				List<OperationProfile> delOperationProfileList = new ArrayList<OperationProfile>();
//				
//				
//				for (OperationProfile operationProfile:operationProfileList) {
//					if (operationProfile.isActive()) {
//						delOperationProfileList.add(operationProfile);
//						oldActiveProfile = operationProfile;
//					}
//					if(operationProfile.getType().equals(OperationProfileType.Eds)){
//						activeProfile.setActive(true);
//						activeProfile.setType(operationProfile.getType());
//						activeProfile.setName(operationProfile.getName());
//						
//						List<OperationProp> operationPropList = new ArrayList<OperationProp>();
//						
//						for (OperationProp operationProp:operationProfile.getOperationPropList()) {
//							OperationProp activeOperationProp = new OperationProp();
////							activeOperationProp.setName(operationProp.getName());
////							activeOperationProp.setType(operationProp.getType());
////							activeOperationProp.setDescription(operationProp.getDescription());
//							activeOperationProp.setProp(operationProp.getProp());
//							activeOperationProp.setValue(operationProp.getValue());
//							activeOperationProp.setSortSeq(operationProp.getSortSeq());
//							activeOperationProp.setOperationProfile(activeProfile);
//							operationPropList.add(activeOperationProp);
//						}
//						List<OperationWorkstation> operationWorkstationList = new ArrayList<OperationWorkstation>();
//						for (OperationWorkstation operationWorkstation:operationProfile.getOperationWorkstationList()) {
//							OperationWorkstation activeOperationWorkstation = new OperationWorkstation();
//							activeOperationWorkstation.setPrinterConnectionType(operationWorkstation.getPrinterConnectionType());
//							if (operationWorkstation.getPrinterType().equals(PrinterType.DefaultAndUrgent)){
//								activeOperationWorkstation.setPrinterType(PrinterType.Normal);
//							} else {
//								activeOperationWorkstation.setPrinterType(operationWorkstation.getPrinterType());
//							}
//							activeOperationWorkstation.setType(operationWorkstation.getType());
//							activeOperationWorkstation.setEnableFlag(operationWorkstation.getEnableFlag());
//							activeOperationWorkstation.setRedirectPrintStation(operationWorkstation.getRedirectPrintStation());
//							activeOperationWorkstation.setIssueWindowNum(operationWorkstation.getIssueWindowNum());
//							activeOperationWorkstation.setOperationProfile(activeProfile);
//							activeOperationWorkstation.setWorkstation(operationWorkstation.getWorkstation());
//							activeOperationWorkstation.setMachine(operationWorkstation.getMachine());
//							operationWorkstationList.add(activeOperationWorkstation);
//						}
//						
//						activeProfile.setOperationPropList(operationPropList);
//						activeProfile.setOperationWorkstationList(operationWorkstationList);
//						
//					}else{
//						operationProfile.setActive(false);
//					}
//				}
//				
//				operationProfileList.remove(oldActiveProfile);
//
//				operationProfileList.add(activeProfile);
//				updateOperationProfileList.add(activeProfile);
//
//				List<OperationProp> updateOperationPropList = new ArrayList<OperationProp>();
//				List<OperationWorkstation> updateOperationWorkstationList = new ArrayList<OperationWorkstation>();
//				List<Workstation> updateWorkstationList = new ArrayList<Workstation>();
//				
//				Contexts.getSessionContext().set("updateOperationProfileList", updateOperationProfileList);
//				Contexts.getSessionContext().set("delOperationProfileList", delOperationProfileList);
//				Contexts.getSessionContext().set("updateOperationPropList", updateOperationPropList);
//				Contexts.getSessionContext().set("updateOperationWorkstationList", updateOperationWorkstationList);
//				Contexts.getSessionContext().set("updateWorkstationList", updateWorkstationList);
//				invokeMethod("#{operationModeListService.updateOperationModeList(updateOperationProfileList, delOperationProfileList, updateOperationPropList, updateOperationWorkstationList, updateWorkstationList)}");
//			}
//		}.run();
//		
//		//Urgent Print
//		new ComponentTest()
//		{
//			protected void testComponents() throws Exception
//			{
//				PrintLabelManagerLocal printLabelManager = (PrintLabelManagerLocal)getValue("#{printLabelManager}");
//				PrintLabelDetail printLabelDetail = printLabelManager.retrievePrintLabelDetailOnEdsMode("PARA01", 7, Boolean.TRUE);
//		
//				assert printLabelDetail!=null;
//				assert printLabelDetail.getItemLocation()==null;
//				assert printLabelDetail.getWorkstation().getWorkstationCode().equals("0005");
//				assert printLabelDetail.getPrinterWorkstation().getWorkstationCode().equals("0005");
//				assert (StringUtils.equals(printLabelDetail.getDefaultPrinterWorkstation().getWorkstationCode(), "0003") || StringUtils.equals(printLabelDetail.getDefaultPrinterWorkstation().getWorkstationCode(), "0004"));
//			}
//		}.run();
//		
//		//EDS mode - one urgent printer and Redirect printing station
//		new ComponentTest() 
//		{
//			@SuppressWarnings("unchecked")
//			protected void testComponents() throws Exception 
//			{
//				invokeMethod("#{operationModeListService.retrieveOperationModeList}");
//				List<OperationProfile> operationProfileList = (List<OperationProfile>)getValue("#{operationProfileList}");
//				
//				assert operationProfileList!=null;
//
//				OperationProfile activeProfile = new OperationProfile();
//				OperationProfile oldActiveProfile = new OperationProfile();
//				
//				List<OperationProfile> updateOperationProfileList = new ArrayList<OperationProfile>();
//				List<OperationProfile> delOperationProfileList = new ArrayList<OperationProfile>();
//				
//				
//				for (OperationProfile operationProfile:operationProfileList) {
//					if (operationProfile.isActive()) {
//						delOperationProfileList.add(operationProfile);
//						oldActiveProfile = operationProfile;
//					}
//					if(operationProfile.getType().equals(OperationProfileType.Eds)){
//						activeProfile.setActive(true);
//						activeProfile.setType(operationProfile.getType());
//						activeProfile.setName(operationProfile.getName());
//						
//						List<OperationProp> operationPropList = new ArrayList<OperationProp>();
//						
//						for (OperationProp operationProp:operationProfile.getOperationPropList()) {
//							OperationProp activeOperationProp = new OperationProp();
////							activeOperationProp.setName(operationProp.getName());
////							activeOperationProp.setType(operationProp.getType());
////							activeOperationProp.setDescription(operationProp.getDescription());
//							activeOperationProp.setProp(operationProp.getProp());
//							activeOperationProp.setValue(operationProp.getValue());
//							activeOperationProp.setSortSeq(operationProp.getSortSeq());
//							activeOperationProp.setOperationProfile(activeProfile);
//							operationPropList.add(activeOperationProp);
//						}
//						List<OperationWorkstation> operationWorkstationList = new ArrayList<OperationWorkstation>();
//						for (OperationWorkstation operationWorkstation:operationProfile.getOperationWorkstationList()) {
//							OperationWorkstation activeOperationWorkstation = new OperationWorkstation();
//							activeOperationWorkstation.setPrinterConnectionType(operationWorkstation.getPrinterConnectionType());
//							if (operationWorkstation.getPrinterType().equals(PrinterType.DefaultAndUrgent)){
//								activeOperationWorkstation.setPrinterType(PrinterType.Normal);
//							} else {
//								activeOperationWorkstation.setPrinterType(operationWorkstation.getPrinterType());
//							}
//							activeOperationWorkstation.setType(operationWorkstation.getType());
//							activeOperationWorkstation.setEnableFlag(operationWorkstation.getEnableFlag());
//							if(operationWorkstation.getWorkstation().getWorkstationCode().equals("0005")){
//								activeOperationWorkstation.setRedirectPrintStation("0003");
//							} else {
//								activeOperationWorkstation.setRedirectPrintStation(operationWorkstation.getRedirectPrintStation());
//							}
//							activeOperationWorkstation.setIssueWindowNum(operationWorkstation.getIssueWindowNum());
//							activeOperationWorkstation.setOperationProfile(activeProfile);
//							activeOperationWorkstation.setWorkstation(operationWorkstation.getWorkstation());
//							activeOperationWorkstation.setMachine(operationWorkstation.getMachine());
//							operationWorkstationList.add(activeOperationWorkstation);
//						}
//						
//						activeProfile.setOperationPropList(operationPropList);
//						activeProfile.setOperationWorkstationList(operationWorkstationList);
//						
//					}else{
//						operationProfile.setActive(false);
//					}
//				}
//				
//				operationProfileList.remove(oldActiveProfile);
//
//				operationProfileList.add(activeProfile);
//				updateOperationProfileList.add(activeProfile);
//
//				List<OperationProp> updateOperationPropList = new ArrayList<OperationProp>();
//				List<OperationWorkstation> updateOperationWorkstationList = new ArrayList<OperationWorkstation>();
//				List<Workstation> updateWorkstationList = new ArrayList<Workstation>();
//				
//				Contexts.getSessionContext().set("updateOperationProfileList", updateOperationProfileList);
//				Contexts.getSessionContext().set("delOperationProfileList", delOperationProfileList);
//				Contexts.getSessionContext().set("updateOperationPropList", updateOperationPropList);
//				Contexts.getSessionContext().set("updateOperationWorkstationList", updateOperationWorkstationList);
//				Contexts.getSessionContext().set("updateWorkstationList", updateWorkstationList);
//				invokeMethod("#{operationModeListService.updateOperationModeList(updateOperationProfileList, delOperationProfileList, updateOperationPropList, updateOperationWorkstationList, updateWorkstationList)}");
//			}
//		}.run();
//		
//		new ComponentTest()
//		{
//			protected void testComponents() throws Exception
//			{
//				PrintLabelManagerLocal printLabelManager = (PrintLabelManagerLocal)getValue("#{printLabelManager}");
//				PrintLabelDetail printLabelDetail = printLabelManager.retrievePrintLabelDetailOnEdsMode("PARA01", 7, Boolean.TRUE);
//		
//				assert printLabelDetail!=null;
//				assert printLabelDetail.getItemLocation()==null;
//				assert printLabelDetail.getWorkstation().getWorkstationCode().equals("0005");
//				assert printLabelDetail.getPrinterWorkstation().getWorkstationCode().equals("0003");
//				assert (StringUtils.equals(printLabelDetail.getDefaultPrinterWorkstation().getWorkstationCode(), "0003") || StringUtils.equals(printLabelDetail.getDefaultPrinterWorkstation().getWorkstationCode(), "0004"));
//			}
//		}.run();
//		
//		//EDS Mode - DefaultAndUrgent Printer
//		new ComponentTest() 
//		{
//			@SuppressWarnings("unchecked")
//			protected void testComponents() throws Exception 
//			{
//				invokeMethod("#{operationModeListService.retrieveOperationModeList}");
//				List<OperationProfile> operationProfileList = (List<OperationProfile>)getValue("#{operationProfileList}");
//				
//				assert operationProfileList!=null;
//
//				OperationProfile activeProfile = new OperationProfile();
//				OperationProfile oldActiveProfile = new OperationProfile();
//				
//				List<OperationProfile> updateOperationProfileList = new ArrayList<OperationProfile>();
//				List<OperationProfile> delOperationProfileList = new ArrayList<OperationProfile>();
//				
//				
//				for (OperationProfile operationProfile:operationProfileList) {
//					if (operationProfile.isActive()) {
//						delOperationProfileList.add(operationProfile);
//						oldActiveProfile = operationProfile;
//					}
//					if(operationProfile.getType().equals(OperationProfileType.Eds)){
//						activeProfile.setActive(true);
//						activeProfile.setType(operationProfile.getType());
//						activeProfile.setName(operationProfile.getName());
//						
//						List<OperationProp> operationPropList = new ArrayList<OperationProp>();
//						
//						for (OperationProp operationProp:operationProfile.getOperationPropList()) {
//							OperationProp activeOperationProp = new OperationProp();
////							activeOperationProp.setName(operationProp.getName());
////							activeOperationProp.setType(operationProp.getType());
////							activeOperationProp.setDescription(operationProp.getDescription());
//							activeOperationProp.setProp(operationProp.getProp());
//							activeOperationProp.setValue(operationProp.getValue());
//							activeOperationProp.setSortSeq(operationProp.getSortSeq());
//							activeOperationProp.setOperationProfile(activeProfile);
//							operationPropList.add(activeOperationProp);
//						}
//						List<OperationWorkstation> operationWorkstationList = new ArrayList<OperationWorkstation>();
//						for (OperationWorkstation operationWorkstation:operationProfile.getOperationWorkstationList()) {
//							OperationWorkstation activeOperationWorkstation = new OperationWorkstation();
//							activeOperationWorkstation.setPrinterConnectionType(operationWorkstation.getPrinterConnectionType());
//							if(operationWorkstation.getWorkstation().getWorkstationCode().equals("0004")){
//								activeOperationWorkstation.setPrinterType(PrinterType.DefaultAndUrgent);
//							} else if (operationWorkstation.getPrinterType().equals(PrinterType.Urgent)){
//								activeOperationWorkstation.setPrinterType(PrinterType.Normal);
//							} else {
//								activeOperationWorkstation.setPrinterType(operationWorkstation.getPrinterType());
//							}
//							activeOperationWorkstation.setType(operationWorkstation.getType());
//							activeOperationWorkstation.setEnableFlag(operationWorkstation.getEnableFlag());
//							activeOperationWorkstation.setRedirectPrintStation(operationWorkstation.getRedirectPrintStation());
//							activeOperationWorkstation.setIssueWindowNum(operationWorkstation.getIssueWindowNum());
//							activeOperationWorkstation.setOperationProfile(activeProfile);
//							activeOperationWorkstation.setWorkstation(operationWorkstation.getWorkstation());
//							activeOperationWorkstation.setMachine(operationWorkstation.getMachine());
//							operationWorkstationList.add(activeOperationWorkstation);
//						}
//						
//						activeProfile.setOperationPropList(operationPropList);
//						activeProfile.setOperationWorkstationList(operationWorkstationList);
//						
//					}else{
//						operationProfile.setActive(false);
//					}
//				}
//				
//				operationProfileList.remove(oldActiveProfile);
//
//				operationProfileList.add(activeProfile);
//				updateOperationProfileList.add(activeProfile);
//
//				List<OperationProp> updateOperationPropList = new ArrayList<OperationProp>();
//				List<OperationWorkstation> updateOperationWorkstationList = new ArrayList<OperationWorkstation>();
//				List<Workstation> updateWorkstationList = new ArrayList<Workstation>();
//				
//				Contexts.getSessionContext().set("updateOperationProfileList", updateOperationProfileList);
//				Contexts.getSessionContext().set("delOperationProfileList", delOperationProfileList);
//				Contexts.getSessionContext().set("updateOperationPropList", updateOperationPropList);
//				Contexts.getSessionContext().set("updateOperationWorkstationList", updateOperationWorkstationList);
//				Contexts.getSessionContext().set("updateWorkstationList", updateWorkstationList);
//				invokeMethod("#{operationModeListService.updateOperationModeList(updateOperationProfileList, delOperationProfileList, updateOperationPropList, updateOperationWorkstationList, updateWorkstationList)}");
//			}
//		}.run();
//		
//		new ComponentTest()
//		{
//			protected void testComponents() throws Exception
//			{
//				PrintLabelManagerLocal printLabelManager = (PrintLabelManagerLocal)getValue("#{printLabelManager}");
//				PrintLabelDetail printLabelDetail = printLabelManager.retrievePrintLabelDetailOnEdsMode("PARA01", 7, Boolean.TRUE);
//		
//				assert printLabelDetail!=null;
//				assert printLabelDetail.getItemLocation()==null;
//				assert printLabelDetail.getWorkstation().getWorkstationCode().equals("0004");
//				assert printLabelDetail.getPrinterWorkstation().getWorkstationCode().equals("0004");
//				assert (StringUtils.equals(printLabelDetail.getDefaultPrinterWorkstation().getWorkstationCode(), "0003") || StringUtils.equals(printLabelDetail.getDefaultPrinterWorkstation().getWorkstationCode(), "0004"));
//			}
//		}.run();
//		
//		//EDS Mode - No Item Location
//		new ComponentTest() 
//		{
//			@SuppressWarnings("unchecked")
//			protected void testComponents() throws Exception 
//			{
//				invokeMethod("#{operationModeListService.retrieveOperationModeList}");
//				List<OperationProfile> operationProfileList = (List<OperationProfile>)getValue("#{operationProfileList}");
//				
//				assert operationProfileList!=null;
//
//				OperationProfile activeProfile = new OperationProfile();
//				OperationProfile oldActiveProfile = new OperationProfile();
//				
//				List<OperationProfile> updateOperationProfileList = new ArrayList<OperationProfile>();
//				List<OperationProfile> delOperationProfileList = new ArrayList<OperationProfile>();
//				
//				
//				for (OperationProfile operationProfile:operationProfileList) {
//					if (operationProfile.isActive()) {
//						delOperationProfileList.add(operationProfile);
//						oldActiveProfile = operationProfile;
//					}
//					if(operationProfile.getType().equals(OperationProfileType.Eds)){
//						activeProfile.setActive(true);
//						activeProfile.setType(operationProfile.getType());
//						activeProfile.setName(operationProfile.getName());
//						
//						List<OperationProp> operationPropList = new ArrayList<OperationProp>();
//						
//						for (OperationProp operationProp:operationProfile.getOperationPropList()) {
//							OperationProp activeOperationProp = new OperationProp();
////							activeOperationProp.setName(operationProp.getName());
////							activeOperationProp.setType(operationProp.getType());
////							activeOperationProp.setDescription(operationProp.getDescription());
//							activeOperationProp.setProp(operationProp.getProp());
//							activeOperationProp.setValue(operationProp.getValue());
//							activeOperationProp.setSortSeq(operationProp.getSortSeq());
//							activeOperationProp.setOperationProfile(activeProfile);
//							operationPropList.add(activeOperationProp);
//						}
//						List<OperationWorkstation> operationWorkstationList = new ArrayList<OperationWorkstation>();
//						for (OperationWorkstation operationWorkstation:operationProfile.getOperationWorkstationList()) {
//							OperationWorkstation activeOperationWorkstation = new OperationWorkstation();
//							activeOperationWorkstation.setPrinterConnectionType(operationWorkstation.getPrinterConnectionType());
//							if (operationWorkstation.getPrinterType().equals(PrinterType.Urgent) || operationWorkstation.getPrinterType().equals(PrinterType.DefaultAndUrgent)){
//								activeOperationWorkstation.setPrinterType(PrinterType.Normal);
//							} else {
//								activeOperationWorkstation.setPrinterType(operationWorkstation.getPrinterType());
//							}
//							activeOperationWorkstation.setType(operationWorkstation.getType());
//							activeOperationWorkstation.setEnableFlag(operationWorkstation.getEnableFlag());
//							activeOperationWorkstation.setRedirectPrintStation(operationWorkstation.getRedirectPrintStation());
//							activeOperationWorkstation.setIssueWindowNum(operationWorkstation.getIssueWindowNum());
//							activeOperationWorkstation.setOperationProfile(activeProfile);
//							activeOperationWorkstation.setWorkstation(operationWorkstation.getWorkstation());
//							activeOperationWorkstation.setMachine(operationWorkstation.getMachine());
//							operationWorkstationList.add(activeOperationWorkstation);
//						}
//						
//						activeProfile.setOperationPropList(operationPropList);
//						activeProfile.setOperationWorkstationList(operationWorkstationList);
//						
//					}else{
//						operationProfile.setActive(false);
//					}
//				}
//				
//				operationProfileList.remove(oldActiveProfile);
//
//				operationProfileList.add(activeProfile);
//				updateOperationProfileList.add(activeProfile);
//
//				List<OperationProp> updateOperationPropList = new ArrayList<OperationProp>();
//				List<OperationWorkstation> updateOperationWorkstationList = new ArrayList<OperationWorkstation>();
//				List<Workstation> updateWorkstationList = new ArrayList<Workstation>();
//				
//				Contexts.getSessionContext().set("updateOperationProfileList", updateOperationProfileList);
//				Contexts.getSessionContext().set("delOperationProfileList", delOperationProfileList);
//				Contexts.getSessionContext().set("updateOperationPropList", updateOperationPropList);
//				Contexts.getSessionContext().set("updateOperationWorkstationList", updateOperationWorkstationList);
//				Contexts.getSessionContext().set("updateWorkstationList", updateWorkstationList);
//				invokeMethod("#{operationModeListService.updateOperationModeList(updateOperationProfileList, delOperationProfileList, updateOperationPropList, updateOperationWorkstationList, updateWorkstationList)}");
//			}
//		}.run();
//		
//		//No ItemLocation, use Default Printer
//		new ComponentTest()
//		{
//			protected void testComponents() throws Exception
//			{
//				PrintLabelManagerLocal printLabelManager = (PrintLabelManagerLocal)getValue("#{printLabelManager}");
//				PrintLabelDetail printLabelDetail = printLabelManager.retrievePrintLabelDetailOnEdsMode("PARA03", 7, Boolean.FALSE);
//		
//				assert printLabelDetail!=null;
//				assert printLabelDetail.getItemLocation()==null;
//				assert printLabelDetail.getWorkstation().getWorkstationCode().equals("0003");
//				assert printLabelDetail.getPrinterWorkstation().getWorkstationCode().equals("0003");
//				assert printLabelDetail.getDefaultPrinterWorkstation().getWorkstationCode().equals("0003");
//			}
//		}.run();
//		
//		//EDS Mode 
//		new ComponentTest() 
//		{
//			@SuppressWarnings("unchecked")
//			protected void testComponents() throws Exception 
//			{
//				invokeMethod("#{operationModeListService.retrieveOperationModeList}");
//				List<OperationProfile> operationProfileList = (List<OperationProfile>)getValue("#{operationProfileList}");
//				
//				assert operationProfileList!=null;
//
//				OperationProfile activeProfile = new OperationProfile();
//				OperationProfile oldActiveProfile = new OperationProfile();
//				
//				List<OperationProfile> updateOperationProfileList = new ArrayList<OperationProfile>();
//				List<OperationProfile> delOperationProfileList = new ArrayList<OperationProfile>();
//				
//				
//				for (OperationProfile operationProfile:operationProfileList) {
//					if (operationProfile.isActive()) {
//						delOperationProfileList.add(operationProfile);
//						oldActiveProfile = operationProfile;
//					}
//					if(operationProfile.getType().equals(OperationProfileType.Eds)){
//						activeProfile.setActive(true);
//						activeProfile.setType(operationProfile.getType());
//						activeProfile.setName(operationProfile.getName());
//						
//						List<OperationProp> operationPropList = new ArrayList<OperationProp>();
//						
//						for (OperationProp operationProp:operationProfile.getOperationPropList()) {
//							OperationProp activeOperationProp = new OperationProp();
////							activeOperationProp.setName(operationProp.getName());
////							activeOperationProp.setType(operationProp.getType());
////							activeOperationProp.setDescription(operationProp.getDescription());
//							activeOperationProp.setProp(operationProp.getProp());
//							activeOperationProp.setValue(operationProp.getValue());
//							activeOperationProp.setSortSeq(operationProp.getSortSeq());
//							activeOperationProp.setOperationProfile(activeProfile);
//							operationPropList.add(activeOperationProp);
//						}
//						List<OperationWorkstation> operationWorkstationList = new ArrayList<OperationWorkstation>();
//						for (OperationWorkstation operationWorkstation:operationProfile.getOperationWorkstationList()) {
//							OperationWorkstation activeOperationWorkstation = new OperationWorkstation();
//							activeOperationWorkstation.setPrinterConnectionType(operationWorkstation.getPrinterConnectionType());
//							if (operationWorkstation.getPrinterType().equals(PrinterType.Urgent) || operationWorkstation.getPrinterType().equals(PrinterType.DefaultAndUrgent)){
//								activeOperationWorkstation.setPrinterType(PrinterType.Normal);
//							} else {
//								activeOperationWorkstation.setPrinterType(operationWorkstation.getPrinterType());
//							}
//							activeOperationWorkstation.setType(operationWorkstation.getType());
//							activeOperationWorkstation.setEnableFlag(operationWorkstation.getEnableFlag());
//							if(operationWorkstation.getWorkstation().getWorkstationCode().equals("0003")){
//								activeOperationWorkstation.setRedirectPrintStation("0004");
//							} else {
//								activeOperationWorkstation.setRedirectPrintStation(operationWorkstation.getRedirectPrintStation());
//							}
//							activeOperationWorkstation.setIssueWindowNum(operationWorkstation.getIssueWindowNum());
//							activeOperationWorkstation.setOperationProfile(activeProfile);
//							activeOperationWorkstation.setWorkstation(operationWorkstation.getWorkstation());
//							activeOperationWorkstation.setMachine(operationWorkstation.getMachine());
//							operationWorkstationList.add(activeOperationWorkstation);
//						}
//						
//						activeProfile.setOperationPropList(operationPropList);
//						activeProfile.setOperationWorkstationList(operationWorkstationList);
//						
//					}else{
//						operationProfile.setActive(false);
//					}
//				}
//				
//				operationProfileList.remove(oldActiveProfile);
//
//				operationProfileList.add(activeProfile);
//				updateOperationProfileList.add(activeProfile);
//
//				List<OperationProp> updateOperationPropList = new ArrayList<OperationProp>();
//				List<OperationWorkstation> updateOperationWorkstationList = new ArrayList<OperationWorkstation>();
//				List<Workstation> updateWorkstationList = new ArrayList<Workstation>();
//				
//				Contexts.getSessionContext().set("updateOperationProfileList", updateOperationProfileList);
//				Contexts.getSessionContext().set("delOperationProfileList", delOperationProfileList);
//				Contexts.getSessionContext().set("updateOperationPropList", updateOperationPropList);
//				Contexts.getSessionContext().set("updateOperationWorkstationList", updateOperationWorkstationList);
//				Contexts.getSessionContext().set("updateWorkstationList", updateWorkstationList);
//				invokeMethod("#{operationModeListService.updateOperationModeList(updateOperationProfileList, delOperationProfileList, updateOperationPropList, updateOperationWorkstationList, updateWorkstationList)}");
//			}
//		}.run();
//		
//		//have one itemLocation(Baker Cell)
//		new ComponentTest()
//		{
//			protected void testComponents() throws Exception
//			{
//				PrintLabelManagerLocal printLabelManager = (PrintLabelManagerLocal)getValue("#{printLabelManager}");
//				PrintLabelDetail printLabelDetail = printLabelManager.retrievePrintLabelDetailOnEdsMode("BACL01", 7, Boolean.FALSE);
//		
//				assert printLabelDetail!=null;
//				assert printLabelDetail.getItemLocation() != null;
//				assert printLabelDetail.getItemLocation().getItemCode().equals("BACL01");
//				assert printLabelDetail.getItemLocation().getMachine().getPickStation().getPickStationNum() == 1;
//				assert StringUtils.equals(printLabelDetail.getItemLocation().getBinNum(), "B007");
//				assert printLabelDetail.getWorkstation().getWorkstationCode().equals("0004");
//				assert printLabelDetail.getPrinterWorkstation().getWorkstationCode().equals("0004");
//				assert printLabelDetail.getDefaultPrinterWorkstation().getWorkstationCode().equals("0004");
//			}
//		}.run();
//		
//		//have itemLocation but qty > Maximun count Qty
//		new ComponentTest()
//		{
//			protected void testComponents() throws Exception
//			{
//				PrintLabelManagerLocal printLabelManager = (PrintLabelManagerLocal)getValue("#{printLabelManager}");
//				PrintLabelDetail printLabelDetail = printLabelManager.retrievePrintLabelDetailOnEdsMode("BACL01", 3000, Boolean.FALSE);
//		
//				assert printLabelDetail!=null;
//				assert printLabelDetail.getItemLocation()==null;
//				assert printLabelDetail.getWorkstation().getWorkstationCode().equals("0003");
//				assert printLabelDetail.getPrinterWorkstation().getWorkstationCode().equals("0004");
//				assert printLabelDetail.getDefaultPrinterWorkstation().getWorkstationCode().equals("0004");
//			}
//		}.run();
//		
//		//have itemLocation but picking station didn't map to operation workstation
//		new ComponentTest()
//		{
//			protected void testComponents() throws Exception
//			{
//				PrintLabelManagerLocal printLabelManager = (PrintLabelManagerLocal)getValue("#{printLabelManager}");
//				PrintLabelDetail printLabelDetail = printLabelManager.retrievePrintLabelDetailOnEdsMode("PACL01", 7, Boolean.FALSE);
//		
//				assert printLabelDetail!=null;
//				assert printLabelDetail.getItemLocation()==null;
//				assert printLabelDetail.getWorkstation().getWorkstationCode().equals("0003");
//				assert printLabelDetail.getPrinterWorkstation().getWorkstationCode().equals("0004");
//				assert printLabelDetail.getDefaultPrinterWorkstation().getWorkstationCode().equals("0004");
//			}
//		}.run();
//		
//		
//		//more than one itemLocation
//		new ComponentTest()
//		{
//			protected void testComponents() throws Exception
//			{
//				PrintLabelManagerLocal printLabelManager = (PrintLabelManagerLocal)getValue("#{printLabelManager}");
//				PrintLabelDetail printLabelDetail = printLabelManager.retrievePrintLabelDetailOnEdsMode("PARA01", 7, Boolean.FALSE);
//		
//				assert printLabelDetail!=null;
//				assert printLabelDetail.getItemLocation()!=null;
//				assert printLabelDetail.getItemLocation().getItemCode().equals("PARA01");
//				assert StringUtils.equals(printLabelDetail.getItemLocation().getBinNum(), "B008");
//				assert printLabelDetail.getWorkstation().getWorkstationCode().equals("0004");
//				assert printLabelDetail.getPrinterWorkstation().getWorkstationCode().equals("0004");
//				assert printLabelDetail.getDefaultPrinterWorkstation().getWorkstationCode().equals("0004");
//			}
//		}.run();
//		
//		//one itemLocation(ScriptPro)
//		new ComponentTest()
//		{
//			protected void testComponents() throws Exception
//			{
//				PrintLabelManagerLocal printLabelManager = (PrintLabelManagerLocal)getValue("#{printLabelManager}");
//				PrintLabelDetail printLabelDetail = printLabelManager.retrievePrintLabelDetailOnEdsMode("ABAC01", 7, Boolean.FALSE);
//		
//				assert printLabelDetail!=null;
//				assert printLabelDetail.getItemLocation()!=null;
//				assert printLabelDetail.getItemLocation().getItemCode().equals("ABAC01");
//				assert StringUtils.equals(printLabelDetail.getItemLocation().getBinNum(), "A012");
//				assert printLabelDetail.getWorkstation().getWorkstationCode().equals("0005");
//				assert printLabelDetail.getPrinterWorkstation()==null;
//				assert printLabelDetail.getDefaultPrinterWorkstation().getWorkstationCode().equals("0004");
//			}
//		}.run();
	}	
}
