package hk.org.ha.model.pms.report.test;

import hk.org.ha.model.pms.biz.report.PrescRptServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.vo.report.PrescRpt;
import hk.org.ha.model.pms.vo.report.PrescRptDtl;

import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.joda.time.DateTime;
import org.testng.annotations.Test;

public class PrescRptTest extends SeamTest {
	
	@Test
	public void testPrescRptComponent() throws Exception {
	
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();

		// retrieve list by ticketNum
		new ComponentTest() {
			protected void testComponents() throws Exception {  
				DateTime rptDate = new DateTime().withDate(2011, 8, 2);
				
				PrescRptServiceLocal prescRptService = (PrescRptServiceLocal) getValue("#{prescRptService}");
				List<PrescRpt> prescRptList = prescRptService.retrievePrescRptList(rptDate.toDate(), "0501", "", "", "", "");
				
				assert prescRptList.size() == 1;
				assert prescRptList.get(0).getPrescRptDtlList().size() == 4;
				assert prescRptList.get(0).getTicketNum().equals("0501");
				
				for (PrescRptDtl prescRptDtl : prescRptList.get(0).getPrescRptDtlList()) {
					if ( prescRptDtl.getItemCode().equals("OLAN01") ) {
						assert prescRptDtl.getStatus().equals(DispOrderItemStatus.Assembled.getDisplayValue());
					}  else if ( prescRptDtl.getItemCode().equals("ACIT01") ) {
						assert prescRptDtl.getStatus().equals(DispOrderItemStatus.Assembled.getDisplayValue());
					} else if ( prescRptDtl.getItemCode().equals("ETAN04") ) {
						assert prescRptDtl.getStatus().equals(DispOrderItemStatus.Assembled.getDisplayValue());
					} else if ( prescRptDtl.getItemCode().equals("PDF 94") ) {
						assert prescRptDtl.getStatus().equals("CAPD");
					} else {
						assert false;
					}
				}
			}
		}.run();

		// retrieve list by ticketNum
		new ComponentTest() {
			protected void testComponents() throws Exception {  
				DateTime rptDate = new DateTime().withDate(2011, 8, 2);
				
				PrescRptServiceLocal prescRptService = (PrescRptServiceLocal) getValue("#{prescRptService}");
				List<PrescRpt> prescRptList = prescRptService.retrievePrescRptList(rptDate.toDate(), "0502", "", "", "", "");
				
				assert prescRptList.size() == 1;
				assert prescRptList.get(0).getPrescRptDtlList().size() == 5;
				assert prescRptList.get(0).getTicketNum().equals("0502");
				
				for (PrescRptDtl prescRptDtl : prescRptList.get(0).getPrescRptDtlList()) {
					if ( prescRptDtl.getItemCode().equals("OLAN01") ) {
						assert prescRptDtl.getStatus().equals(DispOrderAdminStatus.Suspended.getDisplayValue());
					} else if ( prescRptDtl.getItemCode().equals("PARA01") ) {
						assert prescRptDtl.getStatus().equals(DispOrderAdminStatus.Suspended.getDisplayValue());
					} else if ( prescRptDtl.getItemCode().equals("ACIT01") ) {
						assert prescRptDtl.getStatus().equals(DispOrderAdminStatus.Suspended.getDisplayValue());
					} else if ( prescRptDtl.getItemCode().equals("ETAN04") ) {
						assert prescRptDtl.getStatus().equals(DispOrderAdminStatus.Suspended.getDisplayValue());
					} else if ( prescRptDtl.getItemCode().equals("PDF 94") ) {
						assert prescRptDtl.getStatus().equals(DispOrderAdminStatus.Suspended.getDisplayValue());
					} else {
						assert false;
					}
				}
			}
		}.run();	
		
		// retrieve list by caseNum
		new ComponentTest() {
			protected void testComponents() throws Exception {  
				DateTime rptDate = new DateTime().withDate(2011, 8, 2);
				
				PrescRptServiceLocal prescRptService = (PrescRptServiceLocal) getValue("#{prescRptService}");
				List<PrescRpt> prescRptList = prescRptService.retrievePrescRptList(rptDate.toDate(), "", "SOPD1010789C", "", "", "");
				
				assert prescRptList.size() == 2;				
				
				for ( PrescRpt prescRpt : prescRptList ) {
					
					assert prescRpt.getCaseNum().equals("SOPD1010789C");
					
					for (PrescRptDtl prescRptDtl : prescRpt.getPrescRptDtlList()) {
						if ( prescRpt.getTicketNum().equals("0501")) {
							if ( prescRptDtl.getItemCode().equals("OLAN01") ) {
								assert prescRptDtl.getStatus().equals(DispOrderItemStatus.Assembled.getDisplayValue());
							} else if ( prescRptDtl.getItemCode().equals("ACIT01") ) {
								assert prescRptDtl.getStatus().equals(DispOrderItemStatus.Assembled.getDisplayValue());
							} else if ( prescRptDtl.getItemCode().equals("ETAN04") ) {
								assert prescRptDtl.getStatus().equals(DispOrderItemStatus.Assembled.getDisplayValue());
							} else if ( prescRptDtl.getItemCode().equals("PDF 94") ) {
								assert prescRptDtl.getStatus().equals("CAPD");
							} else {
								assert false;
							}
						} else if ( prescRpt.getTicketNum().equals("0502")) {
							if ( prescRptDtl.getItemCode().equals("OLAN01") ) {
								assert prescRptDtl.getStatus().equals(DispOrderAdminStatus.Suspended.getDisplayValue());
							} else if ( prescRptDtl.getItemCode().equals("PARA01") ) {
								assert prescRptDtl.getStatus().equals(DispOrderAdminStatus.Suspended.getDisplayValue());
							} else if ( prescRptDtl.getItemCode().equals("ACIT01") ) {
								assert prescRptDtl.getStatus().equals(DispOrderAdminStatus.Suspended.getDisplayValue());
							} else if ( prescRptDtl.getItemCode().equals("ETAN04") ) {
								assert prescRptDtl.getStatus().equals(DispOrderAdminStatus.Suspended.getDisplayValue());
							} else if ( prescRptDtl.getItemCode().equals("PDF 94") ) {
								assert prescRptDtl.getStatus().equals(DispOrderAdminStatus.Suspended.getDisplayValue());
							} else {
								assert false;
							}
						} else {
							assert false;
						}
					}
				}
			}
		}.run();	
		
		
		// retrieve list by hkid
		new ComponentTest() {
			protected void testComponents() throws Exception {  
				DateTime rptDate = new DateTime().withDate(2011, 8, 2);
				
				PrescRptServiceLocal prescRptService = (PrescRptServiceLocal) getValue("#{prescRptService}");
				List<PrescRpt> prescRptList = prescRptService.retrievePrescRptList(rptDate.toDate(), "", "", "UV9838693", "", "");
				
				assert prescRptList.size() == 2;				
				
				for ( PrescRpt prescRpt : prescRptList ) {
					assert prescRpt.getHkid().equals("UV9838693");
					for (PrescRptDtl prescRptDtl : prescRpt.getPrescRptDtlList()) {
						if ( prescRpt.getTicketNum().equals("0501")) {
							if ( prescRptDtl.getItemCode().equals("OLAN01") ) {
								assert prescRptDtl.getStatus().equals(DispOrderItemStatus.Assembled.getDisplayValue());
							}  else if ( prescRptDtl.getItemCode().equals("ACIT01") ) {
								assert prescRptDtl.getStatus().equals(DispOrderItemStatus.Assembled.getDisplayValue());
							} else if ( prescRptDtl.getItemCode().equals("ETAN04") ) {
								assert prescRptDtl.getStatus().equals(DispOrderItemStatus.Assembled.getDisplayValue());
							} else if ( prescRptDtl.getItemCode().equals("PDF 94") ) {
								assert prescRptDtl.getStatus().equals("CAPD");
							} else {
								assert false;
							}
						} else if ( prescRpt.getTicketNum().equals("0502")) {
							if ( prescRptDtl.getItemCode().equals("OLAN01") ) {
								assert prescRptDtl.getStatus().equals(DispOrderAdminStatus.Suspended.getDisplayValue());
							} else if ( prescRptDtl.getItemCode().equals("PARA01") ) {
								assert prescRptDtl.getStatus().equals(DispOrderAdminStatus.Suspended.getDisplayValue());
							} else if ( prescRptDtl.getItemCode().equals("ACIT01") ) {
								assert prescRptDtl.getStatus().equals(DispOrderAdminStatus.Suspended.getDisplayValue());
							} else if ( prescRptDtl.getItemCode().equals("ETAN04") ) {
								assert prescRptDtl.getStatus().equals(DispOrderAdminStatus.Suspended.getDisplayValue());
							} else if ( prescRptDtl.getItemCode().equals("PDF 94") ) {
								assert prescRptDtl.getStatus().equals(DispOrderAdminStatus.Suspended.getDisplayValue());
							} else {
								assert false;
							}
						} else {
							assert false;
						}
					}
				}
			}
		}.run();	
		
		// retrieve list by itemCode
		new ComponentTest() {
			protected void testComponents() throws Exception {  
				DateTime rptDate = new DateTime().withDate(2011, 8, 2);
				
				PrescRptServiceLocal prescRptService = (PrescRptServiceLocal) getValue("#{prescRptService}");
				List<PrescRpt> prescRptList = prescRptService.retrievePrescRptList(rptDate.toDate(), "", "", "", "OLAN01", "");
				
				assert prescRptList.size() == 2;				
				
				for ( PrescRpt prescRpt : prescRptList ) {
					for (PrescRptDtl prescRptDtl : prescRpt.getPrescRptDtlList()) {
						if ( prescRpt.getTicketNum().equals("0501")) {
							if ( prescRptDtl.getItemCode().equals("OLAN01") ) {
								assert prescRptDtl.getStatus().equals(DispOrderItemStatus.Assembled.getDisplayValue());
							}  else {
								assert false;
							}
						} else if ( prescRpt.getTicketNum().equals("0502")) {
							if ( prescRptDtl.getItemCode().equals("OLAN01") ) {
								assert prescRptDtl.getStatus().equals(DispOrderAdminStatus.Suspended.getDisplayValue());
							} else {
								assert false;
							}
						} else {
							assert false;
						}
					}
				}
			}
		}.run();	
		
		// retrieve list by patName
		new ComponentTest() {
			protected void testComponents() throws Exception {  
				DateTime rptDate = new DateTime().withDate(2011, 8, 2);
				
				PrescRptServiceLocal prescRptService = (PrescRptServiceLocal) getValue("#{prescRptService}");
				List<PrescRpt> prescRptList = prescRptService.retrievePrescRptList(rptDate.toDate(), "", "", "", "", "PATIENT, SAMPLE3");
				
				assert prescRptList.size() == 2;				
				
				for ( PrescRpt prescRpt : prescRptList ) {
					assert prescRpt.getPatName().equals("PATIENT, SAMPLE3");
					for (PrescRptDtl prescRptDtl : prescRpt.getPrescRptDtlList()) {
						if ( prescRpt.getTicketNum().equals("0501")) {
							if ( prescRptDtl.getItemCode().equals("OLAN01") ) {
								assert prescRptDtl.getStatus().equals(DispOrderItemStatus.Assembled.getDisplayValue());
							} else if ( prescRptDtl.getItemCode().equals("ACIT01") ) {
								assert prescRptDtl.getStatus().equals(DispOrderItemStatus.Assembled.getDisplayValue());
							} else if ( prescRptDtl.getItemCode().equals("ETAN04") ) {
								assert prescRptDtl.getStatus().equals(DispOrderItemStatus.Assembled.getDisplayValue());
							} else if ( prescRptDtl.getItemCode().equals("PDF 94") ) {
								assert prescRptDtl.getStatus().equals("CAPD");
							} else {
								assert false;
							}
						} else if ( prescRpt.getTicketNum().equals("0502")) {
							if ( prescRptDtl.getItemCode().equals("OLAN01") ) {
								assert prescRptDtl.getStatus().equals(DispOrderAdminStatus.Suspended.getDisplayValue());
							} else if ( prescRptDtl.getItemCode().equals("PARA01") ) {
								assert prescRptDtl.getStatus().equals(DispOrderAdminStatus.Suspended.getDisplayValue());
							} else if ( prescRptDtl.getItemCode().equals("ACIT01") ) {
								assert prescRptDtl.getStatus().equals(DispOrderAdminStatus.Suspended.getDisplayValue());
							} else if ( prescRptDtl.getItemCode().equals("ETAN04") ) {
								assert prescRptDtl.getStatus().equals(DispOrderAdminStatus.Suspended.getDisplayValue());
							} else if ( prescRptDtl.getItemCode().equals("PDF 94") ) {
								assert prescRptDtl.getStatus().equals(DispOrderAdminStatus.Suspended.getDisplayValue());
							} else {
								assert false;
							}
						} else {
							assert false;
						}
					}
				}
			}
		}.run();
	}	
}
