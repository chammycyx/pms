package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.reftable.RefillHoliday;
import hk.org.ha.model.pms.vo.security.PropMap;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;


public class RefillHolidayTest extends SeamTest {

	private final static String REFILL_ADJHOLIDAY_ADVANCEDAYS = "refill.adjHoliday.advanceDays";
	@Test
	public void testRefillHolidayComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		// retrieve Refill Holiday List
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{refillHolidayListService.retrieveRefillHolidayList}");
				List<RefillHoliday> refillHolidayList = (List<RefillHoliday>) getValue("#{refillHolidayList}");
				
				assert refillHolidayList!=null;
			}
		}.run();
		
		//add Refill Holiday
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{refillHolidayListService.retrieveRefillHolidayList}");
				List<RefillHoliday> refillHolidayList = (List<RefillHoliday>) getValue("#{refillHolidayList}");
				
				assert refillHolidayList!=null;
				
				List<RefillHoliday> updateRefillHolidayList = new ArrayList<RefillHoliday>();
				
				Calendar calendar = Calendar.getInstance();
				calendar.set(2011, 1, 1, 0, 0, 0);
				Date date = calendar.getTime();
				
				RefillHoliday refillHoliday = new RefillHoliday();
				refillHoliday.setHoliday(date);
				updateRefillHolidayList.add(refillHoliday);
				
				calendar.set(2011, 1, 2, 0, 0, 0);
				date = calendar.getTime();
				
				refillHoliday = new RefillHoliday();
				refillHoliday.setHoliday(date);
				updateRefillHolidayList.add(refillHoliday);
				Contexts.getSessionContext().set("updateRefillHolidayList", updateRefillHolidayList);

				List<RefillHoliday> delRefillHolidayList = new ArrayList<RefillHoliday>();
				Contexts.getSessionContext().set("delRefillHolidayList", delRefillHolidayList);
				invokeMethod("#{refillHolidayListService.updateRefillHolidayList(updateRefillHolidayList, delRefillHolidayList)}");
				
			}
		}.run();
		
		// check Refill Holiday List
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{refillHolidayListService.retrieveRefillHolidayList}");
				List<RefillHoliday> refillHolidayList = (List<RefillHoliday>) getValue("#{refillHolidayList}");

				assert refillHolidayList.size()>0;
				
				SimpleDateFormat simpleDateFormat =  new SimpleDateFormat("yyyy-MM-dd"); 
				
				String holiday;
				int checkHoliday = 0;
				for(RefillHoliday refillHoliday:refillHolidayList)
				{
					holiday = simpleDateFormat.format(refillHoliday.getHoliday());
					if(holiday.equals("2011-02-01"))
					{
						checkHoliday++;
					}
					else if (holiday.equals("2011-02-02"))
					{
						checkHoliday++;
					}
				}
				
				assert checkHoliday == 2;
			}
		}.run();
		
		
		//update Refill Holiday
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{refillHolidayListService.retrieveRefillHolidayList}");
				List<RefillHoliday> refillHolidayList = (List<RefillHoliday>) getValue("#{refillHolidayList}");

				assert refillHolidayList.size()>0;

				List<RefillHoliday> updateRefillHolidayList = new ArrayList<RefillHoliday>();
				
				Calendar calendar = Calendar.getInstance();

				
				calendar.set(2011, 3, 1, 0, 0, 0);
				Date date2 = calendar.getTime();
				
				SimpleDateFormat simpleDateFormat =  new SimpleDateFormat("yyyy-MM-dd"); 
				
				String holiday;
				for(RefillHoliday refillHoliday:refillHolidayList)
				{
					holiday = simpleDateFormat.format(refillHoliday.getHoliday());
					if(holiday.equals("2011-02-01"))
					{
						refillHoliday.setHoliday(date2);
						updateRefillHolidayList.add(refillHoliday);
						break;
					}
				}

				Contexts.getSessionContext().set("updateRefillHolidayList", updateRefillHolidayList);
				List<RefillHoliday> delRefillHolidayList = new ArrayList<RefillHoliday>();
				Contexts.getSessionContext().set("delRefillHolidayList", delRefillHolidayList);
				invokeMethod("#{refillHolidayListService.updateRefillHolidayList(updateRefillHolidayList, delRefillHolidayList)}");
			}
		}.run();
		
		// check Refill Holiday List
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{refillHolidayListService.retrieveRefillHolidayList}");
				List<RefillHoliday> refillHolidayList = (List<RefillHoliday>) getValue("#{refillHolidayList}");

				assert refillHolidayList.size()>0;

				SimpleDateFormat simpleDateFormat =  new SimpleDateFormat("yyyy-MM-dd"); 
				
				String holiday;
				int checkHoliday = 0;
				for(RefillHoliday refillHoliday:refillHolidayList)
				{
					holiday = simpleDateFormat.format(refillHoliday.getHoliday());
					if(holiday.equals("2011-02-02"))
					{
						checkHoliday++;
					}
					else if (holiday.equals("2011-04-01"))
					{
						checkHoliday++;
					}
				}
				assert checkHoliday==2;
			}
		}.run();
		
		//delete Refill Holiday
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{refillHolidayListService.retrieveRefillHolidayList}");
				List<RefillHoliday> refillHolidayList = (List<RefillHoliday>) getValue("#{refillHolidayList}");

				assert refillHolidayList.size()>0;
				
				SimpleDateFormat simpleDateFormat =  new SimpleDateFormat("yyyy-MM-dd"); 
				
				List<RefillHoliday> delRefillHolidayList = new ArrayList<RefillHoliday>();
				
				String holiday;
				for(RefillHoliday refillHoliday:refillHolidayList)
				{
					holiday = simpleDateFormat.format(refillHoliday.getHoliday());
					if(holiday.equals("2011-04-01"))
					{
						delRefillHolidayList.add(refillHoliday);
						break;
					}
				}
				List<RefillHoliday> updateRefillHolidayList = new ArrayList<RefillHoliday>();
				Contexts.getSessionContext().set("updateRefillHolidayList", updateRefillHolidayList);

				Contexts.getSessionContext().set("delRefillHolidayList", delRefillHolidayList);
				invokeMethod("#{refillHolidayListService.updateRefillHolidayList(updateRefillHolidayList, delRefillHolidayList)}");
			}
		}.run();
		
		// check Refill Holiday List
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{refillHolidayListService.retrieveRefillHolidayList}");
				List<RefillHoliday> refillHolidayList = (List<RefillHoliday>) getValue("#{refillHolidayList}");

				assert refillHolidayList.size()>0;
				
				SimpleDateFormat simpleDateFormat =  new SimpleDateFormat("yyyy-MM-dd"); 
				
				String holiday;
				for(RefillHoliday refillHoliday:refillHolidayList)
				{
					holiday = simpleDateFormat.format(refillHoliday.getHoliday());
					if(holiday.equals("2011-04-01"))
					{
						assert false;
					}
				}
			}
		}.run();
		
		// update advanceDueDate
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				PropMap propMap = (PropMap) getValue("#{propMap}");
				
				assert propMap.getValue(REFILL_ADJHOLIDAY_ADVANCEDAYS).equals("0");
				
				invokeMethod("#{refillHolidayService.updateRefillHolidayAdvanceDueDate(10)}");
				
				propMap = (PropMap) getValue("#{propMap}");

				assert propMap.getValue(REFILL_ADJHOLIDAY_ADVANCEDAYS).equals("10");

			}
		}.run();
	}	
}
