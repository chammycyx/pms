package hk.org.ha.model.pms.biz.mock;

import hk.org.ha.model.pms.biz.integrated.test.IdGenerator;
import hk.org.ha.model.pms.corp.persistence.FmHospitalMapping;
import hk.org.ha.model.pms.corp.persistence.charging.PatientSfiProfile;
import hk.org.ha.model.pms.corp.persistence.report.MpWorkloadStat;
import hk.org.ha.model.pms.corp.vo.EndVettingResult;
import hk.org.ha.model.pms.corp.vo.StartVettingResult;
import hk.org.ha.model.pms.corp.vo.UnvetOrderResult;
import hk.org.ha.model.pms.persistence.RemarkEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.CapdVoucher;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.InvoiceItem;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.Replenishment;
import hk.org.ha.model.pms.persistence.phs.Ward;
import hk.org.ha.model.pms.persistence.phs.WardStock;
import hk.org.ha.model.pms.persistence.reftable.CorporateProp;
import hk.org.ha.model.pms.persistence.reftable.HospitalProp;
import hk.org.ha.model.pms.persistence.reftable.WorkstoreProp;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.disp.MpTrxType;
import hk.org.ha.model.pms.udt.disp.PharmOrderAdminStatus;
import hk.org.ha.model.pms.udt.enquiry.SfiInvoiceEnqPatType;
import hk.org.ha.model.pms.udt.onestop.AllowCommentType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.charging.FcsPaymentStatus;
import hk.org.ha.model.pms.vo.checkissue.DispInfo;
import hk.org.ha.model.pms.vo.onestop.OneStopOrderItemSummary;
import hk.org.ha.model.pms.vo.reftable.PmsSessionInfo;
import hk.org.ha.model.pms.vo.report.DayEndFileExtractRpt;
import hk.org.ha.model.pms.vo.support.PropInfoItem;
import hk.org.ha.model.pms.vo.uncollect.UncollectOrder;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;

@Startup
@Name("corpPmsServiceProxy")
@Scope(ScopeType.APPLICATION)
public class MockCorpPmsServiceProxy implements CorpPmsServiceJmsRemote {

	@Override
	public StartVettingResult startVetting(String orderNum, Long medOrderId, String hospCode) {
		StartVettingResult result = new StartVettingResult();
		result.setMedOrder(null);
		result.setSuccessFlag(Boolean.TRUE);
		return result;
	}

	@Override
	public EndVettingResult endVetting(DispOrder dispOrder, Long lastPharmOrderId, String hospCode, Boolean remarkFlag, Boolean fcsPersistenceEnabled, Boolean legacyPersistenceEnabled) {
		
		PharmOrder pharmOrder = dispOrder.getPharmOrder();
		MedOrder medOrder = pharmOrder.getMedOrder();
		
		medOrder.setStatus(MedOrderStatus.Complete);
		for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
			medOrderItem.setStatus(MedOrderItemStatus.Completed);
		}

		MedCase medCase = pharmOrder.getMedCase();
		medCase.setId(null);
		IdGenerator.getInstance().reflectSetId(medCase);
		
		EndVettingResult result = new EndVettingResult();
		result.setPharmOrder(dispOrder.getPharmOrder());
		result.setSuccessFlag(Boolean.TRUE);

		return result;
	}

	@Override
	public EndVettingResult endVetting(DispOrder arg0, Long arg1, String arg2,
			Boolean arg3, Boolean arg4, Boolean arg5, Boolean arg6) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public List<MpWorkloadStat> retrieveMpWorkloadStatRptList(Workstore arg0,
			Date arg1, Date arg2) {
		return new ArrayList<MpWorkloadStat>();
	}
	
	@Override
	public void cancelVetting(String arg0, String arg1) {
		// TODO Auto-generated method stub
	}

	@Override
	public void removeOrder(DispOrder arg0, RemarkEntity arg1, Boolean arg2, Boolean arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public UnvetOrderResult unvetOrder(MedOrder arg0, Long arg1, Boolean arg2, Boolean arg3, Boolean arg4, Boolean arg5) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void cancelDispensing(Long arg0, Boolean arg1, Boolean arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateAllowCommentType(DispOrder arg0, AllowCommentType arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public MedCase saveMedCase(MedCase arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Patient savePatient(Patient arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void invalidateSession(Workstore workstore) {
		// TODO Auto-generated method stub		
	}

	@Override
	public void purgeMsWorkstoreDrug(Workstore arg0) {
		// TODO Auto-generated method stub		
	}

	@Override
	public List<WardStock> retrieveWardStockList(Ward arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean saveDispOrderListForIp(List<DispOrder> arg0, Boolean legacyPersistenceEnabled) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<DispOrderItem> retrieveDispOrderItemListByDeliveryItemId(List<Long> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean updateDispOrderItemStatusToDeleted(List<Long> arg0, Boolean legacyPersistenceEnabled) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void sendMedProfileOrder(MpTrxType arg0, List<MedProfileMoItem> arg1) {
		// TODO Auto-generated method stub
	}

	@Override
	public void sendMedProfileReplenishment(MpTrxType arg0, List<Replenishment> arg1) {
		// TODO Auto-generated method stub
	}
	
	@Override
	public List<CorporateProp> retrieveCorporatePropList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateCorporatePropList(List<PropInfoItem> arg0) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void updateUncollectOrderList(List<UncollectOrder> updateUncollectOrderList, Workstore workstore, String uncollectUser) {
		
	}

	@Override
	public void reversePharmacyRemark(MedOrder arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateCorporatePropByPropMaint(List<CorporateProp> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateWorkstoreProp(List<WorkstoreProp> arg0, boolean arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateHospitalProp(List<HospitalProp> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateAdminStatusForceProceed(DispOrder arg0,
			DispOrderAdminStatus arg1, PharmOrderAdminStatus arg2,
			Boolean arg3, Invoice arg4, Boolean arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateCapdVoucherStatusToIssue(List<CapdVoucher> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void endDispensing(Long arg0, DispInfo arg1, Boolean arg2,
			Boolean arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<PmsSessionInfo> retrievePmsSessionInfoList(String arg0,
			Workstore arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DayEndFileExtractRpt> retrieveDayEndFileExtractRptList(
			String arg0, String arg1, Date arg2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateFcsSfiInvoiceForceProceedInfo(Invoice arg0, Boolean arg1) {
		// TODO Auto-generated method stub
	}

	@Override
	public void endDispensingForBatchIssue(List<DispInfo> arg0, Boolean arg1,
			Boolean arg2) {
		// TODO Auto-generated method stub		
	}

	@Override
	public void updateDispOrderAdminStatus(Long arg0,
			DispOrderAdminStatus arg1, Date arg2, Boolean arg3, Boolean arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void voidInvoiceByInvoiceNum(List<String> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String retrieveChargeOrderNum(String arg0, String arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean saveDispOrderListWithVoidInvoiceListForIp(
			List<DispOrder> arg0, Boolean arg1, Boolean arg2, Boolean arg3,
			List<Invoice> arg4, List<Invoice> arg5) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void updateFcsSfiInvoiceStatus(List<String> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Map<Long, String> retrieveInvoiceNumByDeliveryItemId(
			List<Long> deliveryItemIdList) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void cancelDispensing(Long arg0, Boolean arg1, Boolean arg2,
			String arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public EndVettingResult refillDispensing(DispOrder arg0, Boolean arg1,
			Boolean arg2, List<MedOrderItem> arg3, String arg4) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Invoice retrieveInvoiceByInvoiceId(Long invoiceId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateInvoicePaymentStatus(List<Long> arg0,
			Map<String, FcsPaymentStatus> arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Invoice> retrieveInvoiceByInvoiceNumWoPurchaseRequest(
			String arg0, String arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Invoice> retrieveInvoiceListWoPurchaseRequest(String arg0,
			String arg1, String arg2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<InvoiceItem> retrieveSfiInvoiceItemListByCaseNum(
			String caseNum, boolean includeVoidInvoice, OrderType orderType,
			Workstore workstore, SfiInvoiceEnqPatType patType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<InvoiceItem> retrieveSfiInvoiceItemListByExtactionPeriod(
			Integer dayRange, boolean includeVoidInvoice, OrderType orderType,
			Workstore workstore, SfiInvoiceEnqPatType patType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<InvoiceItem> retrieveSfiInvoiceItemListByHkid(String hkid,
			boolean includeVoidInvoice, OrderType orderType,
			Workstore workstore, SfiInvoiceEnqPatType patType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<InvoiceItem> retrieveSfiInvoiceItemListByInvoiceDate(
			Date invoiceDate, boolean includeVoidInvoice, OrderType orderType,
			Workstore workstore, SfiInvoiceEnqPatType patType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<InvoiceItem> retrieveSfiInvoiceItemListByInvoiceNum(
			String invoiceNum, boolean includeVoidInvoice, OrderType orderType,
			Workstore workstore, SfiInvoiceEnqPatType patType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void endDispensing(Long arg0, DispInfo arg1, Boolean arg2,
			Boolean arg3, Invoice arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reverseUncollectAndEndDispensing(DispOrder arg0, DispInfo arg1,
			Boolean arg2, Boolean arg3, Invoice arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<OneStopOrderItemSummary> retrieveOrderByCaseNum(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateDispOrderAdminStatusAndForceProceed(Long dispOrderId,
			DispOrderAdminStatus dispOrderAdminStatus, Date issueDate,
			Boolean legacyPersistenceEnabled,
			Boolean legacyFcsPersistenceEnabled, Invoice invoice) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean updateDispOrderItemStatusToDeletedByTpnRequestIdList(
			List<Long> arg0, Boolean arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<FmHospitalMapping> retrieveFmHospitalMappingList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EndVettingResult refillDispensing(DispOrder arg0, Boolean arg1,
			Boolean arg2, List<MedOrderItem> arg3, String arg4,
			Map<Long, Boolean> arg5) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateAdminStatus(DispOrder arg0, DispOrderAdminStatus arg1,
			PharmOrderAdminStatus arg2, Boolean arg3, Boolean arg4,
			Map<Long, Boolean> arg5) {
		// TODO Auto-generated method stub
	}

	@Override
	public void updateAdminStatusForceProceed(DispOrder arg0,
			DispOrderAdminStatus arg1, PharmOrderAdminStatus arg2,
			Boolean arg3, Invoice arg4, Boolean arg5, Map<Long, Boolean> arg6) {
		// TODO Auto-generated method stub
	}

	@Override
	public void updateDispOrderAdminStatus(Long arg0,
			DispOrderAdminStatus arg1, Date arg2, Boolean arg3, Boolean arg4,
			Map<Long, Boolean> arg5) {
		// TODO Auto-generated method stub
	}

	@Override
	public void updateAdminStatus(DispOrder arg0, DispOrderAdminStatus arg1,
			PharmOrderAdminStatus arg2, Boolean arg3, Boolean arg4) {
		// TODO Auto-generated method stub
	}

	@Override
	public PatientSfiProfile retrievePatientSfiProfile(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StartVettingResult startVetting(String arg0, Long arg1, String arg2,
			String arg3) {
		// TODO Auto-generated method stub
		return null;
	}
}

