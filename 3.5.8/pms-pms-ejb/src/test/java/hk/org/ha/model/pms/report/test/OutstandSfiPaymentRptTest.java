package hk.org.ha.model.pms.report.test;

import static org.testng.AssertJUnit.assertEquals;
import hk.org.ha.model.pms.biz.charging.InvoiceManagerLocal;
import hk.org.ha.model.pms.biz.report.OutstandSfiPaymentRptServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.vo.onestop.OneStopOrderInfo;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.joda.time.DateTime;
import org.testng.annotations.Test;

public class OutstandSfiPaymentRptTest extends SeamTest {

	@Test
	public void testOutstandSfiPaymentRptComponent() throws Exception {
	
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		
		//retrieveOutstandSfiPaymentRpt - record found
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				DateTime dispDateTime = new DateTime("2011-03-31");
				
				OutstandSfiPaymentRptServiceLocal outstandSfiPaymentRptService = (OutstandSfiPaymentRptServiceLocal) getInstance("outstandSfiPaymentRptService");
				List<Invoice> invoiceList = outstandSfiPaymentRptService.retrieveOutstandSfiPaymentRpt(dispDateTime.toDate());
				
				assertEquals("Total number of records retrieved should be ", 6, invoiceList.size() );
				for ( Invoice invoice : invoiceList) {
					 if ( StringUtils.equals( invoice.getInvoiceNum(), "IQMH11000000003" ) ) {
						 assert  StringUtils.equals( invoice.getDispOrder().getTicket().getTicketNum(), "0501" );
						 assert  StringUtils.equals( invoice.getDispOrder().getPharmOrder().getMedOrder().getRefNum(), "0016" );
						 assert  StringUtils.equals( invoice.getDispOrder().getPharmOrder().getMedCase().getCaseNum(), "SOPD0011900S" );
						 assert  StringUtils.equals( invoice.getDispOrder().getPharmOrder().getMedOrder().getPatient().getName(), "PATIENT, SAMPLE3" );
						 assert  StringUtils.equals( invoice.getDispOrder().getPharmOrder().getMedOrder().getPatient().getHkid(), "UV9838693" );
						 assert  invoice.getForceProceedCode()==null;
					 } else if ( StringUtils.equals( invoice.getInvoiceNum(), "IQMH11000000007" ) ) {
						 assert  StringUtils.equals( invoice.getDispOrder().getTicket().getTicketNum(), "0513" );
						 assert  StringUtils.equals( invoice.getDispOrder().getPharmOrder().getMedOrder().getRefNum(), "1005" );
						 assert  StringUtils.equals( invoice.getDispOrder().getPharmOrder().getMedCase().getCaseNum(), "SOPD9999001U" );
						 assert  StringUtils.equals( invoice.getDispOrder().getPharmOrder().getMedOrder().getPatient().getName(), "CHUI, HEALTHY" );
						 assert  StringUtils.equals( invoice.getDispOrder().getPharmOrder().getMedOrder().getPatient().getHkid(), "M8688074" );
						 assert  invoice.getForceProceedCode()==null;
					 } else if ( StringUtils.equals( invoice.getInvoiceNum(), "IQMH11000000027" ) ) {
						 assert  StringUtils.equals( invoice.getDispOrder().getTicket().getTicketNum(), "0510" );
						 assert  StringUtils.equals( invoice.getDispOrder().getPharmOrder().getMedOrder().getRefNum(), "1020a" );
						 assert  StringUtils.equals( invoice.getDispOrder().getPharmOrder().getMedCase().getCaseNum(), "POD 0610001S" );
						 assert  StringUtils.equals( invoice.getDispOrder().getPharmOrder().getMedOrder().getPatient().getName(), "UNKNOWN" );
						 assert  StringUtils.equals( invoice.getDispOrder().getPharmOrder().getMedOrder().getPatient().getHkid(), "UM8073753" );
						 assert  invoice.getForceProceedCode()==null;
					 }
				}
			}
		}.run();
		
		//retrieveOutstandSfiPaymentRpt - record not found
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				DateTime dispDateTime = new DateTime(2010, 11, 28, 0, 0, 0, 0);
				
				OutstandSfiPaymentRptServiceLocal outstandSfiPaymentRptService = (OutstandSfiPaymentRptServiceLocal) getInstance("outstandSfiPaymentRptService");
				List<Invoice> invoiceList = outstandSfiPaymentRptService.retrieveOutstandSfiPaymentRpt(dispDateTime.toDate());
				
				assert invoiceList.isEmpty();
			}
		}.run();
	}
	
	@Test
	public void testUpdateInvoicePaymentsStatusComponent() throws Exception {
	
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
	            
	            OneStopOrderInfo oneStopOrderInfo = new OneStopOrderInfo();
	            oneStopOrderInfo.setStandardForceProceed(false);
	            Contexts.getSessionContext().set("oneStopOrderInfo", oneStopOrderInfo);	
			}
		}.run();
		
		
		//updateInvoicePaymentsStatus - FcsCheckDate is null - Process Update
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				DateTime dispDateTime = new DateTime("2011-04-30");
				
				InvoiceManagerLocal invoiceManager = (InvoiceManagerLocal) getInstance("invoiceManager");
				
				invoiceManager.retrieveInvoice(Long.valueOf(32));
				Invoice invoiceBefore = (Invoice) getInstance("invoice");
				assert invoiceBefore.getFcsCheckDate() == null;
				
				OutstandSfiPaymentRptServiceLocal outstandSfiPaymentRptService = (OutstandSfiPaymentRptServiceLocal) getInstance("outstandSfiPaymentRptService");
				List<Invoice> invoiceList = outstandSfiPaymentRptService.retrieveOutstandSfiPaymentRpt(dispDateTime.toDate());
				assert invoiceList.size() == 1;			
				
				for( Invoice invoice: invoiceList ){
					if( invoice.getId() == 32 ){
						assert invoice.getReceiveAmount().compareTo(BigDecimal.valueOf(-1)) == 0;
						assert invoice.getFcsCheckDate() != null;
					}		
				}
			}
		}.run();
	}
}

