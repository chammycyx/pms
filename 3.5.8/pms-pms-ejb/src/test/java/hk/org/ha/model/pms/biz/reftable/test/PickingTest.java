package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.reftable.Machine;
import hk.org.ha.model.pms.persistence.reftable.PickStation;
import hk.org.ha.model.pms.udt.reftable.MachineType;

import java.util.ArrayList;
import java.util.List;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class PickingTest extends SeamTest {

	@Test
	public void testPickingComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		//retrieve PickStationList
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{operationModeListService.retrievePickStationList}");
				List<PickStation> pickStationList = (List<PickStation>)getValue("#{operationModePickStationList}");
				
				assert pickStationList!=null;
			}
		}.run();		
		
		//add PickStationList
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				List<PickStation> pickStationList = new ArrayList<PickStation>();
				
				PickStation pickStation = new PickStation();
				pickStation.setPickStationNum(5);
				List<Machine> machineList = new ArrayList<Machine>();
				
				Machine machineManual = new Machine();
				machineManual.setType(MachineType.Manual);
				machineManual.setPickStation(pickStation);
				machineList.add(machineManual);
				
				Machine machineBakerCell = new Machine();
				machineBakerCell.setType(MachineType.BakerCell);
				machineBakerCell.setHostName("newMachine");
				machineBakerCell.setPickStation(pickStation);
				machineList.add(machineBakerCell);
				pickStation.setMachineList(machineList);
				pickStationList.add(pickStation);
				
				Contexts.getSessionContext().set("updatePickStationList", pickStationList);
				invokeMethod("#{pickStationListManager.updatePickStationList(updatePickStationList)}");
			}
		}.run();
		
		//check PickStationList
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{operationModeListService.retrievePickStationList}");
				List<PickStation> pickStationList = (List<PickStation>)getValue("#{operationModePickStationList}");
				
				assert pickStationList!=null;
				
				boolean check = false;
				for(PickStation pickStation:pickStationList){
					if(pickStation.getPickStationNum()==5){
						for(Machine machine:pickStation.getMachineList()){
							if(machine.getType().equals(MachineType.BakerCell)){
								assert machine.getHostName().equals("newMachine");
							}else{
								assert machine.getType().equals(MachineType.Manual);
							}
						}
						check = true;
					}
				}
				assert check;
			}
		}.run();	
		
		
		//update PickStationList
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{operationModeListService.retrievePickStationList}");
				List<PickStation> pickStationList = (List<PickStation>)getValue("#{operationModePickStationList}");
				
				assert pickStationList!=null;

				List<PickStation> updatePickStationList = new ArrayList<PickStation>();
				
				for(PickStation pickStation:pickStationList){
					if(pickStation.getPickStationNum()==5){
						for(Machine machine:pickStation.getMachineList()){
							machine.setHostName("host005");
						}
						updatePickStationList.add(pickStation);
						break;
					}
				}
				
				Contexts.getSessionContext().set("updatePickStationList", updatePickStationList);
				invokeMethod("#{pickStationListManager.updatePickStationList(updatePickStationList)}");
			}
		}.run();
		
		//check PickStationList
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{operationModeListService.retrievePickStationList}");
				List<PickStation> pickStationList = (List<PickStation>)getValue("#{operationModePickStationList}");
				
				assert pickStationList!=null;
				
				boolean check = false;
				for(PickStation pickStation:pickStationList){
					if(pickStation.getPickStationNum()==5){
						for(Machine machine:pickStation.getMachineList()){
							if(machine.getType().equals(MachineType.BakerCell)){
								assert machine.getHostName().equals("host005");
							}else{
								assert machine.getType().equals(MachineType.Manual);
							}
						}
						check = true;
					}
				}
				assert check;
			}
		}.run();	
		
	}	
}
