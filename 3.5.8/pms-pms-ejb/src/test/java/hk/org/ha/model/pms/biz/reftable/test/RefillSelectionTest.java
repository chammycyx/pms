package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.reftable.RefillSelection;

import java.util.ArrayList;
import java.util.List;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class RefillSelectionTest extends SeamTest {
	
	@Test
	public void testRefillSelectionComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		// retrieve Refill Selection List
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{refillSelectionListService.retrieveRefillSelectionList}");
				List<RefillSelection> refillSelectionList = (List<RefillSelection>) getValue("#{refillSelectionList}");
				
				assert refillSelectionList!=null;
			}
		}.run();
		
		//add Refill Selection
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{refillSelectionListService.retrieveRefillSelectionList}");
				List<RefillSelection> refillSelectionList = (List<RefillSelection>) getValue("#{refillSelectionList}");
				
				assert refillSelectionList!=null;
				
				RefillSelection refillSelection = new RefillSelection();
				refillSelection.setItemCode("ATRO03");
				refillSelection.setPasSpecCode("A&E");
				refillSelection.setType("I");
				refillSelectionList.add(refillSelection);
				
				refillSelection = new RefillSelection();
				refillSelection.setItemCode("5FLU02");
				refillSelection.setPasSpecCode("FM");
				refillSelection.setType("O");
				refillSelectionList.add(refillSelection);
				
				List<RefillSelection> delRefillSelection = new ArrayList<RefillSelection>();
				Contexts.getSessionContext().set("updateRefillSelection", refillSelectionList);
				Contexts.getSessionContext().set("delRefillSelection", delRefillSelection);
				invokeMethod("#{refillSelectionListService.updateRefillSelectionList(updateRefillSelection, delRefillSelection)}");
				
			}
		}.run();
		
		//check Refill Selection List
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{refillSelectionListService.retrieveRefillSelectionList}");
				List<RefillSelection> refillSelectionList = (List<RefillSelection>) getValue("#{refillSelectionList}");
				
				assert refillSelectionList.size()>0;
				
				int checkRecord =0;
				
				for(RefillSelection refillSelection:refillSelectionList)
				{
					if(refillSelection.getItemCode().equals("ATRO03"))
					{
						assert refillSelection.getPasSpecCode().equals("A&E");
						assert refillSelection.getType().equals("I");
						checkRecord++;
					}
					else if (refillSelection.getPasSpecCode().equals("FM"))
					{
						assert refillSelection.getItemCode().equals("5FLU02");
						assert refillSelection.getType().equals("O");
						checkRecord++;
					}
				}
				
				assert checkRecord == 2;
				
			}
		}.run();
		
		//update Refill Selection List
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{refillSelectionListService.retrieveRefillSelectionList}");
				List<RefillSelection> refillSelectionList = (List<RefillSelection>) getValue("#{refillSelectionList}");
				
				assert refillSelectionList.size()>0;
				
				
				for(RefillSelection refillSelection:refillSelectionList)
				{
					if(refillSelection.getItemCode().equals("ATRO03"))
					{
						refillSelection.setPasSpecCode("ANA");
						refillSelection.setType("O");
					}
					else if (refillSelection.getPasSpecCode().equals("FM"))
					{
						refillSelection.setItemCode("PARA01");
						refillSelection.setType("I");
					}
				}

				Contexts.getSessionContext().set("updateRefillSelection", refillSelectionList);
				List<RefillSelection> delRefillSelection = new ArrayList<RefillSelection>();
				Contexts.getSessionContext().set("delRefillSelection", delRefillSelection);
				invokeMethod("#{refillSelectionListService.updateRefillSelectionList(updateRefillSelection, delRefillSelection)}");
			}
		}.run();
		
		//check Refill Selection List
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{refillSelectionListService.retrieveRefillSelectionList}");
				List<RefillSelection> refillSelectionList = (List<RefillSelection>) getValue("#{refillSelectionList}");
				
				assert refillSelectionList.size()>0;
				
				int checkRecord =0;
				
				for(RefillSelection refillSelection:refillSelectionList)
				{
					if(refillSelection.getItemCode().equals("ATRO03"))
					{
						assert refillSelection.getPasSpecCode().equals("ANA");
						assert refillSelection.getType().equals("O");
						checkRecord++;
					}
					else if (refillSelection.getPasSpecCode().equals("FM"))
					{
						assert refillSelection.getItemCode().equals("PARA01");
						assert refillSelection.getType().equals("I");
						checkRecord++;
					}
				}
				
				assert checkRecord == 2;
				
			}
		}.run();
		
		//delete Refill Selection List
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{refillSelectionListService.retrieveRefillSelectionList}");
				List<RefillSelection> refillSelectionList = (List<RefillSelection>) getValue("#{refillSelectionList}");
				
				assert refillSelectionList.size()>0;

				List<RefillSelection> delRefillSelection = new ArrayList<RefillSelection>();
				
				for(RefillSelection refillSelection:refillSelectionList)
				{
					if(refillSelection.getItemCode().equals("ATRO03"))
					{
						delRefillSelection.add(refillSelection);
					}
					else if (refillSelection.getPasSpecCode().equals("FM"))
					{
						delRefillSelection.add(refillSelection);
					}
				}

				Contexts.getSessionContext().set("updateRefillSelection", refillSelectionList);
				Contexts.getSessionContext().set("delRefillSelection", delRefillSelection);
				invokeMethod("#{refillSelectionListService.updateRefillSelectionList(updateRefillSelection, delRefillSelection)}");
			}
		}.run();
		
		//check Refill Selection List
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{refillSelectionListService.retrieveRefillSelectionList}");
				List<RefillSelection> refillSelectionList = (List<RefillSelection>) getValue("#{refillSelectionList}");
				
				assert refillSelectionList!=null;
				
				for(RefillSelection refillSelection:refillSelectionList)
				{
					if(refillSelection.getItemCode().equals("ATRO03"))
					{
						assert false;
					}
					else if (refillSelection.getPasSpecCode().equals("FM"))
					{
						assert false;
					}
				}
			}
		}.run();
	}	
}
