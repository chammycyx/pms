package hk.org.ha.model.pms.biz.vetting.test;

import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.biz.vetting.FmIndicationServiceLocal;
import hk.org.ha.model.pms.dms.persistence.DmForm;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.udt.vetting.NameType;
import hk.org.ha.model.pms.vo.drug.DmFormLite;
import hk.org.ha.model.pms.vo.rx.OralRxDrug;

import java.math.BigDecimal;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class FmIndicationServiceTest extends SeamTest {

	@Test
	public void testFmIndicationServiceComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
	        }
		}.run();
		
		//retrieve FmIndication without coind desc
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				FmIndicationServiceLocal fmIndicationService = (FmIndicationServiceLocal) getInstance("fmIndicationService");
				MedOrderItem medOrderItem = new MedOrderItem(new OralRxDrug());

				medOrderItem.setNameType(NameType.TradeName);
				medOrderItem.setAliasName("DIPROCEL");
				medOrderItem.setDisplayName("BETAMETHASONE");
				medOrderItem.setSaltProperty("DIPROPIONATE");	
				//DmForm dmForm = new DmForm();
				DmFormLite dmFormLite = DmFormLite.objValueOf(new DmForm());
				dmFormLite.setMoeDesc("OINTMENT");
				medOrderItem.setDmFormLite(dmFormLite);			
				medOrderItem.setStrength("0.05%");
				medOrderItem.setVolume(new BigDecimal(15d));
				medOrderItem.setVolumeUnit("G");
				medOrderItem.setExtraInfo("IN PROPYLENE GLYCOL");						
				
				MedOrder medOrder = new MedOrder();
				medOrder.setPatHospCode("QMH");
				Patient patient = new Patient();
				patient.setPatKey("00391139");
				medOrder.setPatient(patient);
				medOrder.addMedOrderItem(medOrderItem);
				medOrderItem.setItemCode("BETA08");
				medOrderItem.setFormCode("OIN");
				medOrderItem.setFmStatus("F");
				
				PharmOrder pharmOrder = new PharmOrder();
				pharmOrder.setMedOrder(medOrder);
				
				Contexts.getSessionContext().set("medOrder", medOrder);
				Contexts.getSessionContext().set("pharmOrder", pharmOrder);
				fmIndicationService.retrieveFmIndicationListByMedOrderItem(medOrderItem);
			}
		}.run();
		
		//retrieve FmIndication with coind desc
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				FmIndicationServiceLocal fmIndicationService = (FmIndicationServiceLocal) getInstance("fmIndicationService");
				MedOrderItem medOrderItem = new MedOrderItem(new OralRxDrug());
				
				medOrderItem.setNameType(NameType.TradeName);
				medOrderItem.setAliasName("DIPROCEL");
				medOrderItem.setDisplayName("BETAMETHASONE");
				medOrderItem.setSaltProperty("DIPROPIONATE");	
				//DmForm dmForm = new DmForm();
				DmFormLite dmFormLite = DmFormLite.objValueOf(new DmForm());
				dmFormLite.setMoeDesc("OINTMENT");
				medOrderItem.setDmFormLite(dmFormLite);			
				medOrderItem.setStrength("0.05%");
				medOrderItem.setVolume(new BigDecimal(15d));
				medOrderItem.setVolumeUnit("G");
				medOrderItem.setExtraInfo("IN PROPYLENE GLYCOL");						
				
				MedOrder medOrder = new MedOrder();
				medOrder.setPatHospCode("QMH");
				Patient patient = new Patient();
				patient.setPatKey("00391139");
				medOrder.setPatient(patient);
				medOrder.addMedOrderItem(medOrderItem);
				medOrderItem.setItemCode("PARA01");
				medOrderItem.setFormCode("OIN");
				medOrderItem.setFmStatus("F");
				
				Contexts.getSessionContext().set("medOrder", medOrder);
				fmIndicationService.retrieveFmIndicationListByMedOrderItem(medOrderItem);
			}
		}.run();

	}
}