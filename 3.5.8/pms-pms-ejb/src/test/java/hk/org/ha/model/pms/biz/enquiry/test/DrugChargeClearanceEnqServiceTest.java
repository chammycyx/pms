package hk.org.ha.model.pms.biz.enquiry.test;

import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.udt.charging.ClearanceStatus;
import hk.org.ha.model.pms.vo.charging.DrugCheckClearanceResult;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class DrugChargeClearanceEnqServiceTest extends SeamTest {

	@Test
	public void testDrugChargeClearanceEnqStartComponent() throws Exception {
		
		final String invoiceNum = "IQMH11000000003";
		final String invoiceNumNotClear = "IQMH11000000005";
		final String displayInvoiceNum = "I-QMH-11000000003";
		final String displayInvoiceNumNotClear = "I-QMH-11000000005";
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");	         
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		//retrieveByInvoiceNo. - SFI Clear / Standard N/A
		new ComponentTest() 
		{			
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{drugChargeClearanceEnqService.retrieveDrugClearance('"+invoiceNum+"')}");
				DrugCheckClearanceResult drugClearCheckResult = (DrugCheckClearanceResult) getValue("#{drugClearCheckResult}");
				assert drugClearCheckResult.getSfiInvNum().equals(displayInvoiceNum);
				assert drugClearCheckResult.getSfiDrugClearance().equals(ClearanceStatus.PaymentClear);
			}
		}.run();
		
		//retrieveByInvoiceNo. - Not Clear
		new ComponentTest() 
		{			
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{drugChargeClearanceEnqService.retrieveDrugClearance('"+invoiceNumNotClear+"')}");
				DrugCheckClearanceResult drugClearCheckResult = (DrugCheckClearanceResult) getValue("#{drugClearCheckResult}");
				assert drugClearCheckResult.getSfiInvNum().equals(displayInvoiceNumNotClear);
				assert drugClearCheckResult.getSfiDrugClearance().equals(ClearanceStatus.PaymentOutstanding);
			}
		}.run();
		
		//retrieveByInvoiceNo. - Not Found
		new ComponentTest() 
		{			
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{drugChargeClearanceEnqService.retrieveDrugClearance('IQMH11000000000')}");
				DrugCheckClearanceResult drugClearCheckResult = (DrugCheckClearanceResult) getValue("#{drugClearCheckResult}");
				assert drugClearCheckResult.getSystemMessageCode().equals("0005");
				assert drugClearCheckResult.getSfiInvNum() == null;
			}
		}.run();
		
		//retrieveByInvoiceNumber - FCS Error		
		new ComponentTest() 
		{			
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{drugChargeClearanceEnqService.retrieveDrugClearance('IQMH11000000032')}");
				DrugCheckClearanceResult drugClearCheckResult = (DrugCheckClearanceResult) getValue("#{drugClearCheckResult}");
				assert drugClearCheckResult.getSfiDrugClearance().equals(ClearanceStatus.Error);
				assert drugClearCheckResult.getSystemMessageCode().equals("0014");
				assert drugClearCheckResult.getSfiInvNum() == null;
			}
		}.run();
		
		//retrieveByMOEOrderNumber - Not Found
		new ComponentTest() 
		{			
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{drugChargeClearanceEnqService.retrieveDrugClearance('MOEQMH110000000')}");
				DrugCheckClearanceResult drugClearCheckResult = (DrugCheckClearanceResult) getValue("#{drugClearCheckResult}");				
				assert drugClearCheckResult.getSystemMessageCode().equals("0005");
			}
		}.run();
		
		//retrieveByMOEOrderNumber - MO Only FCS Error
		new ComponentTest() 
		{			
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{drugChargeClearanceEnqService.retrieveDrugClearance('MOEQMH110000227')}");
				DrugCheckClearanceResult drugClearCheckResult = (DrugCheckClearanceResult) getValue("#{drugClearCheckResult}");				
				assert drugClearCheckResult.getStdDrugClearance().equals(ClearanceStatus.Error);
				assert drugClearCheckResult.getSystemMessageCode().equals("0014");
			}
		}.run();
		
		//retrieveByMOEOrderNumber - FCS Error
		new ComponentTest() 
		{			
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{drugChargeClearanceEnqService.retrieveDrugClearance('MOEQMH110000228')}");
				DrugCheckClearanceResult drugClearCheckResult = (DrugCheckClearanceResult) getValue("#{drugClearCheckResult}");				
				assert drugClearCheckResult.getStdDrugClearance().equals(ClearanceStatus.Error);
				assert drugClearCheckResult.getSfiDrugClearance().equals(ClearanceStatus.Error);
				assert drugClearCheckResult.getSystemMessageCode().equals("0014");
			}
		}.run();
		
		//retrieveByMOEOrderNumber - Standard Clear (MO only)
		new ComponentTest() 
		{			
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{drugChargeClearanceEnqService.retrieveDrugClearance('MOEQMH110000001')}");
				DrugCheckClearanceResult drugClearCheckResult = (DrugCheckClearanceResult) getValue("#{drugClearCheckResult}");				
				assert drugClearCheckResult.getStdDrugClearance().equals(ClearanceStatus.PaymentClear);
			}
		}.run();
		
		//retrieveByMOEOrderNumber - Standard Not Clear (MO only)
		new ComponentTest() 
		{			
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{drugChargeClearanceEnqService.retrieveDrugClearance('MOEQMH110000002')}");
				DrugCheckClearanceResult drugClearCheckResult = (DrugCheckClearanceResult) getValue("#{drugClearCheckResult}");				
				assert drugClearCheckResult.getStdDrugClearance().equals(ClearanceStatus.PaymentOutstanding);
			}
		}.run();
		
		//retrieveByMOEOrderNumber - Standard and SFI not Clear
		new ComponentTest() 
		{			
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{drugChargeClearanceEnqService.retrieveDrugClearance('MOEQMH110000005')}");
				DrugCheckClearanceResult drugClearCheckResult = (DrugCheckClearanceResult) getValue("#{drugClearCheckResult}");				
				assert drugClearCheckResult.getStdDrugClearance().equals(ClearanceStatus.PaymentOutstanding);
				assert drugClearCheckResult.getSfiDrugClearance().equals(ClearanceStatus.PaymentOutstanding);
				assert drugClearCheckResult.getSfiInvNum().equals(displayInvoiceNumNotClear);
			}
		}.run();
		
		//retrieveByTicketNum - Not Found
		new ComponentTest() 
		{			
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{drugChargeClearanceEnqService.retrieveDrugClearance('T0501')}");
				DrugCheckClearanceResult drugClearCheckResult = (DrugCheckClearanceResult) getValue("#{drugClearCheckResult}");				
				assert drugClearCheckResult.getSfiDrugClearance() == null;
				assert drugClearCheckResult.getSfiInvNum() == null;
				assert drugClearCheckResult.getStdDrugClearance() == null;
				assert drugClearCheckResult.getSystemMessageCode().equals("0005");
			}
		}.run();
		
		//retrieveByBarCodeTicketNum - Not Found
		new ComponentTest() 
		{			
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{drugChargeClearanceEnqService.retrieveDrugClearance('031911220500')}");
				DrugCheckClearanceResult drugClearCheckResult = (DrugCheckClearanceResult) getValue("#{drugClearCheckResult}");				
				assert drugClearCheckResult.getSystemMessageCode().equals("0005");
			}
		}.run();
		
		//retrieveByBarCodeTicketNum - SFI Clear
		new ComponentTest() 
		{			
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{drugChargeClearanceEnqService.retrieveDrugClearance('031911220501')}");
				DrugCheckClearanceResult drugClearCheckResult = (DrugCheckClearanceResult) getValue("#{drugClearCheckResult}");				
				assert drugClearCheckResult.getSfiDrugClearance().equals(ClearanceStatus.PaymentClear);
				assert drugClearCheckResult.getSfiInvNum().equals(displayInvoiceNum);
			}
		}.run();
		
		//retrieveByBarCodeTicketNum - Not Found
		new ComponentTest() 
		{			
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{drugChargeClearanceEnqService.retrieveDrugClearance('031911220502')}");
				DrugCheckClearanceResult drugClearCheckResult = (DrugCheckClearanceResult) getValue("#{drugClearCheckResult}");				
				assert drugClearCheckResult.getSfiDrugClearance() == null;
				assert drugClearCheckResult.getSfiInvNum() == null;
				assert drugClearCheckResult.getStdDrugClearance() == null;
				assert drugClearCheckResult.getSystemMessageCode().equals("0005");				
			}
		}.run();
		
		//invalid Input Format - Not Found
		new ComponentTest() 
		{			
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{drugChargeClearanceEnqService.retrieveDrugClearance('T123456')}");
				DrugCheckClearanceResult drugClearCheckResult = (DrugCheckClearanceResult) getValue("#{drugClearCheckResult}");				
				assert drugClearCheckResult.getSfiDrugClearance() == null;
				assert drugClearCheckResult.getSfiInvNum() == null;
				assert drugClearCheckResult.getStdDrugClearance() == null;
				assert drugClearCheckResult.getSystemMessageCode().equals("0016");				
			}
		}.run();
	}
}