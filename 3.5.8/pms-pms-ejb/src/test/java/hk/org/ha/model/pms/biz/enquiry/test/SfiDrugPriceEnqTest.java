package hk.org.ha.model.pms.biz.enquiry.test;

import hk.org.ha.model.pms.biz.enquiry.SfiDrugPriceEnqServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.vo.charging.DrugPriceInfo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class SfiDrugPriceEnqTest extends SeamTest {

	@Test
	public void testRetrieveSfiDrugPriceComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		//retrieveCurrentPriceItem
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				SfiDrugPriceEnqServiceLocal sfiDrugPriceEnqService = (SfiDrugPriceEnqServiceLocal) getValue("#{sfiDrugPriceEnqService}");
				Calendar retrieveMonth = Calendar.getInstance();
				retrieveMonth.set(Calendar.DATE, 1);
				sfiDrugPriceEnqService.retrieveSfiDrugPrice("OLAN01", retrieveMonth.getTime());
				
				DrugPriceInfo drugPriceEnquiryInfo = (DrugPriceInfo) getValue("#{drugPriceEnquiryInfo}");				
				assert drugPriceEnquiryInfo != null;
				assert drugPriceEnquiryInfo.getCorpDrugPrice() == 32.479;
				assert drugPriceEnquiryInfo.getPublicMarkupFactor() == 1.01;
				assert drugPriceEnquiryInfo.getPrivateMarkupFactor() == 1.5;
				assert drugPriceEnquiryInfo.getNepMarkupFactor() == 1.5;
				assert drugPriceEnquiryInfo.getPrivateHandleCharge() == 50.0;
				assert drugPriceEnquiryInfo.getPublicHandleCharge() == 50.0;
				assert drugPriceEnquiryInfo.getNepHandleCharge() == 50.0;
				assert drugPriceEnquiryInfo.isSuspendItem() == false;
				//assert drugPriceEnquiryInfo.getFullDrugDesc().equals("OLANZAPINE TABLET 500MG");
				assert drugPriceEnquiryInfo.getBaseUnit().equals("TAB");

			}
		}.run();
		
		
		//retrieveHistoryPriceList
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				List<DrugPriceInfo> drugPriceList = new ArrayList<DrugPriceInfo>();
				DrugPriceInfo drugPriceEnquiryInfo = (DrugPriceInfo) getValue("#{drugPriceEnquiryInfo}");
				drugPriceList.add(drugPriceEnquiryInfo);
				
				SfiDrugPriceEnqServiceLocal sfiDrugPriceEnqService = (SfiDrugPriceEnqServiceLocal) getValue("#{sfiDrugPriceEnqService}");
				Calendar retrieveMonth = Calendar.getInstance();
				retrieveMonth.set(Calendar.YEAR, 2010);
				retrieveMonth.set(Calendar.MONTH, 7);
				retrieveMonth.set(Calendar.DATE, 1);
				sfiDrugPriceEnqService.retrieveSfiDrugPriceHistory(drugPriceList, retrieveMonth.getTime());
				List<DrugPriceInfo> drugPriceEnquiryInfoList = (List<DrugPriceInfo>) getValue("#{drugPriceEnquiryInfoList}");
				assert drugPriceEnquiryInfoList.size() == 1;
				DrugPriceInfo drugPriceEnquiryInfoResult = drugPriceEnquiryInfoList.get(0);
				assert drugPriceEnquiryInfoResult.getHistoryFullDrugDesc().equals("OLANZAPINE TABLET 10MG");
				assert drugPriceEnquiryInfoResult.getHistoryCorpDrugPrice() == 32.479;
				assert drugPriceEnquiryInfo.getCorpDrugPrice() == 32.479;
				assert drugPriceEnquiryInfo.getPublicMarkupFactor() == 1.01;
				assert drugPriceEnquiryInfo.getPrivateMarkupFactor() == 1.5;
				assert drugPriceEnquiryInfo.getNepMarkupFactor() == 1.5;
				assert drugPriceEnquiryInfo.getPrivateHandleCharge() == 50.0;
				assert drugPriceEnquiryInfo.getPublicHandleCharge() == 50.0;
				assert drugPriceEnquiryInfo.getNepHandleCharge() == 50.0;
				assert drugPriceEnquiryInfo.isSuspendItem() == false;
				//assert drugPriceEnquiryInfo.getFullDrugDesc().equals("OLANZAPINE TABLET 500MG");
				assert drugPriceEnquiryInfo.getBaseUnit().equals("TAB");
			}
		}.run();
	}
	
	@Test
	public void testRetrieveSfiDrugPriceHistoryComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		//retrieveHistoryPriceItem
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				SfiDrugPriceEnqServiceLocal sfiDrugPriceEnqService = (SfiDrugPriceEnqServiceLocal) getValue("#{sfiDrugPriceEnqService}");
				Calendar retrieveMonth = Calendar.getInstance();
				retrieveMonth.set(Calendar.YEAR, 2010);
				retrieveMonth.set(Calendar.MONTH, 7);
				retrieveMonth.set(Calendar.DATE, 1);
				sfiDrugPriceEnqService.retrieveSfiDrugPrice("ETAN04", retrieveMonth.getTime());
				
				DrugPriceInfo drugPriceEnquiryInfo = (DrugPriceInfo) getValue("#{drugPriceEnquiryInfo}");				
				assert drugPriceEnquiryInfo != null;
				assert drugPriceEnquiryInfo.getHistoryFullDrugDesc().equals("ETANERCEPT PREFILLED SYRINGE 25MG");
				assert drugPriceEnquiryInfo.getHistoryCorpDrugPrice() == 1061.9407;
				assert drugPriceEnquiryInfo.getCorpDrugPrice() == 1061.9407;
				assert drugPriceEnquiryInfo.getPublicMarkupFactor() == 1.0;
				assert drugPriceEnquiryInfo.getPrivateMarkupFactor()== 1.0;
				assert drugPriceEnquiryInfo.getNepMarkupFactor()== 1.0;				
				assert drugPriceEnquiryInfo.getPublicHandleCharge() == 0;				
				assert drugPriceEnquiryInfo.getPrivateHandleCharge() == 0;
				assert drugPriceEnquiryInfo.getNepHandleCharge() == 0;
				//assert drugPriceEnquiryInfo.getFullDrugDesc().equals("ETANERCEPT PREFILLED SYRINGE 25MG");
				assert drugPriceEnquiryInfo.getBaseUnit().equals("NO");
				assert drugPriceEnquiryInfo.isSuspendItem() == false;
			}
		}.run();
	}
}

