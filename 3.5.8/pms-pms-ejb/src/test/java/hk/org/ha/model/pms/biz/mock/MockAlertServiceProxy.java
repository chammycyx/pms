package hk.org.ha.model.pms.biz.mock;

import java.util.List;

import hk.org.ha.model.pms.asa.exception.alert.AlertProfileException;
import hk.org.ha.model.pms.asa.exception.alert.drug.DrugException;
import hk.org.ha.model.pms.asa.exception.alert.druginfo.DrugInfoException;
import hk.org.ha.model.pms.asa.exception.alert.mds.MdsException;
import hk.org.ha.model.pms.asa.vo.alert.drug.DrugAliasName;
import hk.org.ha.model.pms.asa.vo.alert.drug.HicMapping;
import hk.org.ha.model.pms.dms.vo.AllergenIngredient;
import hk.org.ha.model.pms.dms.vo.DrugMds;
import hk.org.ha.model.pms.persistence.PatientEntity;
import hk.org.ha.model.pms.vo.alert.AlertProfile;
import hk.org.ha.model.pms.vo.alert.AlertProfileHistory;
import hk.org.ha.model.pms.vo.alert.druginfo.ClassificationNode;
import hk.org.ha.model.pms.vo.alert.druginfo.DrugInteraction;
import hk.org.ha.model.pms.vo.alert.druginfo.FoodInteraction;
import hk.org.ha.model.pms.vo.alert.druginfo.PatientEducation;
import hk.org.ha.model.pms.vo.alert.mds.MdsCriteria;
import hk.org.ha.model.pms.vo.alert.mds.MdsResult;
import hk.org.ha.service.pms.asa.interfaces.alert.AlertServiceJmsRemote;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;

@Startup
@Name("alertServiceProxy")
@Scope(ScopeType.APPLICATION)
public class MockAlertServiceProxy implements AlertServiceJmsRemote {

	@Override
	public AlertProfile retrieveAlertProfile(PatientEntity arg0, String arg1,
			String arg2, String arg3, String arg4, String arg5)
			throws AlertProfileException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AlertProfileHistory retrieveAlertProfileHistory(PatientEntity arg0,
			String arg1, String arg2, String arg3, String arg4)
			throws AlertProfileException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MdsResult mdsCheck(MdsCriteria arg0) throws MdsException {
		return new MdsResult();
	}

	@Override
	public String retrieveCommonOrder(Integer arg0, Integer arg1, Integer arg2,
			String arg3, List<DrugMds> arg4) throws DrugInfoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String retrieveContraindication(Integer arg0, Integer arg1,
			Integer arg2, String arg3, List<DrugMds> arg4)
			throws DrugInfoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String retrieveDosageRange(Integer arg0, Integer arg1, Integer arg2,
			String arg3, Integer arg4, String arg5, List<DrugMds> arg6,
			Integer arg7) throws DrugInfoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ClassificationNode> retrieveDrugClassificationList(
			Integer arg0, Integer arg1, Integer arg2, String arg3,
			List<DrugMds> arg4) throws DrugInfoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DrugInteraction> retrieveDrugInteraction(Integer arg0,
			Integer arg1, Integer arg2, String arg3, List<DrugMds> arg4)
			throws DrugInfoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<FoodInteraction> retrieveFoodInteraction(Integer arg0,
			Integer arg1, Integer arg2, String arg3, List<DrugMds> arg4)
			throws DrugInfoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String retrieveMonograph(Integer arg0, Integer arg1, String arg2,
			String arg3) throws DrugInfoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<PatientEducation> retrievePatientEducation(Integer arg0,
			Integer arg1, Integer arg2, String arg3, List<DrugMds> arg4)
			throws DrugInfoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String retrievePrecaution(Integer arg0, Integer arg1, Integer arg2,
			String arg3, List<DrugMds> arg4) throws DrugInfoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String retrieveSideEffect(Integer arg0, Integer arg1, Integer arg2,
			String arg3, List<DrugMds> arg4) throws DrugInfoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer updateAllergenIngredient(List<AllergenIngredient> arg0)
			throws DrugException {
		// TODO Auto-generated method stub
		return null;
	}

	public void updateDrugAliasName(List<DrugAliasName> arg0)
			throws DrugException {
		// TODO Auto-generated method stub
		
	}

	public void updateDrugMds(
			List<hk.org.ha.model.pms.asa.vo.alert.drug.DrugMds> arg0)
			throws DrugException {
		// TODO Auto-generated method stub
		
	}

	public void updateHicMapping(List<HicMapping> arg0) throws DrugException {
		// TODO Auto-generated method stub
		
	}
}
