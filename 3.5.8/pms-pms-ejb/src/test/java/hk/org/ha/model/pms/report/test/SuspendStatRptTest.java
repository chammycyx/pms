package hk.org.ha.model.pms.report.test;

import hk.org.ha.model.pms.biz.report.SuspendStatRptServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.reftable.SuspendStat;

import java.util.Date;
import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.joda.time.DateTime;
import org.testng.annotations.Test;

public class SuspendStatRptTest extends SeamTest {
	
	@Test
	public void testSuspendStatRptComponent() throws Exception {
	
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();

		// retrieve suspendStat list by past 2 days
		new ComponentTest() {
			protected void testComponents() throws Exception {  
				DateTime today = new DateTime().withDate(2011, 7, 7);
				SuspendStatRptServiceLocal suspendStatRptService = (SuspendStatRptServiceLocal) getValue("#{suspendStatRptService}");
				List<SuspendStat> suspendStatList = suspendStatRptService.constructSuspendStatList(1, today.toDate(), null, null);
			
				assert suspendStatList.size()==2;
			}
		}.run();
		
		// retrieve suspendStat list By date range
		new ComponentTest() {
			protected void testComponents() throws Exception {   
				DateTime today = new DateTime();
				DateTime dateFrom = new DateTime().withDate(2011, 7, 5);
				DateTime dateTo = new DateTime().withDate(2011, 7, 7);
				
				SuspendStatRptServiceLocal suspendStatRptService = (SuspendStatRptServiceLocal) getValue("#{suspendStatRptService}");
				List<SuspendStat> suspendStatList = suspendStatRptService.constructSuspendStatList(null, today.toDate(), dateFrom.toDate(), dateTo.toDate());
				
				assert suspendStatList.size()>0;
			}
		}.run();
		
	}	
}
