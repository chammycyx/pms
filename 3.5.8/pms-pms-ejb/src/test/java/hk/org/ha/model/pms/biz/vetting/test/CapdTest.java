package hk.org.ha.model.pms.biz.vetting.test;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.model.pms.biz.order.DispOrderManagerLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.biz.vetting.CapdSplitServiceLocal;
import hk.org.ha.model.pms.biz.vetting.CapdVoucherManagerLocal;
import hk.org.ha.model.pms.dms.vo.Capd;
import hk.org.ha.model.pms.persistence.disp.CapdVoucher;
import hk.org.ha.model.pms.persistence.disp.CapdVoucherItem;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.CapdVoucherStatus;
import hk.org.ha.model.pms.vo.drug.DmDrugLite;
import hk.org.ha.model.pms.vo.rx.CapdItem;
import hk.org.ha.model.pms.vo.rx.CapdRxDrug;
import hk.org.ha.model.pms.vo.rx.OralRxDrug;
import hk.org.ha.model.pms.vo.vetting.CapdManualVoucher;
import hk.org.ha.model.pms.vo.vetting.CapdSplit;
import hk.org.ha.model.pms.vo.vetting.CapdSplitItem;
import hk.org.ha.model.pms.vo.vetting.CapdVoucherPrintOut;
import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class CapdTest extends SeamTest {
	
	@Test
	public void testCapdManualVoucherComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
//		 retrieve CapdManualVoucher
		new ComponentTest() {
			protected void testComponents() throws Exception {
				
				invokeMethod("#{capdManualVoucherService.retrieveCapdManualVoucherByManualVoucherNum('QMH11111111')}");	
				CapdVoucher capdManualVoucher = (CapdVoucher)getValue("#{capdManualVoucher}");
				
				assert capdManualVoucher != null;
				assert "QMH11111111".equals(capdManualVoucher.getManualVoucherNum());
			}
		}.run();
		
	}	

	@Test
	public void testCapdListComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
	        }
		}.run();

		// retrieveCapdSupplierSystem
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
	            invokeMethod("#{capdListService.retrieveCapdSupplierSystemList}");
	            List<String> capdSupplierSystemList = (List<String>)getValue("#{capdSupplierSystemList}");
	            
	            assert capdSupplierSystemList.size()==1;
	        }
		}.run();

		// retrieveCapdCalciumStrength
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
	            invokeMethod("#{capdListService.retrieveCapdCalciumStrengthList('BAXTER SPIKE')}");
	            List<String> capdCalciumStrengthList = (List<String>)getValue("#{capdCalciumStrengthList}");
	            
	            assert capdCalciumStrengthList.size()==1;
	        }
		}.run();

		// retrieveCapdConcentration
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
	            invokeMethod("#{capdListService.retrieveCapdConcentrationList('BAXTER SPIKE', 'STANDARD CALCIUM')}");
	            List<Capd> capdConcentrationList = (List<Capd>)getValue("#{capdConcentrationList}");
	            
	            assert capdConcentrationList.size()==1;
	        }
		}.run();
		
		// retrieveCapdList
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
	            invokeMethod("#{capdListService.retrieveCapdList('BAXTER SPIKE', 'STANDARD CALCIUM')}");
	            List<String> capdSupplierSystemList = (List<String>)getValue("#{capdSupplierSystemList}");
	            List<String> capdCalciumStrengthList = (List<String>)getValue("#{capdCalciumStrengthList}");
	            List<Capd> capdConcentrationList = (List<Capd>)getValue("#{capdConcentrationList}");
	            

	            assert capdSupplierSystemList.size()==1;
	            assert capdCalciumStrengthList.size()==1;
	            assert capdConcentrationList.size()==1;
	        }
		}.run();
		
		// Clear CapdList
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
	            invokeMethod("#{capdListService.clearCapdList}");
	            List<String> capdSupplierSystemList = (List<String>)getValue("#{capdSupplierSystemList}");
	            List<String> capdCalciumStrengthList = (List<String>)getValue("#{capdCalciumStrengthList}");
	            List<Capd> capdConcentrationList = (List<Capd>)getValue("#{capdConcentrationList}");
	            
	            assert capdSupplierSystemList == null;
	            assert capdCalciumStrengthList == null;
	            assert capdConcentrationList == null;
	        }
		}.run();
	}
	
	
	String itemCode1 = "PDF 94";
	String supplier1 = "KFS";
	String baseUnit1 = "BAG";
	Integer qty1 = 168;
	Integer newQty1 = 100;
	Integer itemNum1 = 1;
	String connectSystem1 = "ULTRA BAG";
	
	String itemCode2 = "PDF 19";
	String supplier2 = "FMCL";
	String baseUnit2 = "BAG";
	Integer qty2 = 224;
	Integer itemNum2 = 2;
	String connectSystem2 = "SPIKE";

	private static final String JAXB_CONTEXT_VETTING = "hk.org.ha.model.pms.vo.vetting";	
	
	@Test
	public void testCapdSplitComponent() throws Exception {

		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
	        }
		}.run();

		// retrieveCapdSupplierSystem
		new ComponentTest() {
			protected void testComponents() throws Exception {
				
				MedOrder medOrder = new MedOrder();
				MedOrderItem medOrderItem = new MedOrderItem(new CapdRxDrug());
				
				CapdRxDrug capdRxDrug = medOrderItem.getCapdRxDrug();
				capdRxDrug.setUpdateGroupNumFlag(Boolean.TRUE);				
				CapdItem capdItem = new CapdItem();
				capdItem.setItemCode(itemCode1);
				capdItem.setSupplierCode(supplier1);
				capdItem.setBaseUnit(baseUnit1);
				capdRxDrug.getCapdItemList().add(capdItem);
				
				medOrderItem.setActionStatus(ActionStatus.DispByPharm);
				medOrderItem.setStatus(MedOrderItemStatus.Outstanding);
				medOrderItem.setItemNum(itemNum1);
				medOrderItem.setItemCode(itemCode1);
				
				PharmOrder pharmOrder = new PharmOrder();
				PharmOrderItem pharmOrderItem = new PharmOrderItem();
				pharmOrderItem.setItemCode(itemCode1);
				pharmOrderItem.setItemNum(itemNum1);
				pharmOrderItem.setOrgItemNum(itemNum1);
				pharmOrderItem.setIssueQty(BigDecimal.valueOf(qty1));
				pharmOrderItem.setCalQty(BigDecimal.valueOf(qty1));
				pharmOrderItem.setMedOrderItem(medOrderItem);
				pharmOrderItem.setConnectSystem(connectSystem1);
				DmDrugLite dmDrugLite = new DmDrugLite();
				dmDrugLite.setItemCode(itemCode1);
				pharmOrderItem.setDmDrugLite(dmDrugLite);
				
				pharmOrder.setMedOrder(medOrder);
				pharmOrder.getPharmOrderItemList().add(pharmOrderItem);
				medOrderItem.getPharmOrderItemList().add(pharmOrderItem);
				medOrder.getMedOrderItemList().add(medOrderItem);
				medOrder.getPharmOrderList().add(pharmOrder);

				Contexts.getSessionContext().set("pharmOrder", pharmOrder);
				
				invokeMethod("#{capdSplitService.retrieveCapdSplit}");
				
				CapdSplit capdSplit = (CapdSplit)getValue("#{capdSplit}");
				
				assert capdSplit != null;
				assert capdSplit.getCapdSplitItemList().size() == 1;
				assert itemCode1.equals(capdSplit.getCapdSplitItemList().get(0).getItemCode());
				assert supplier1.equals(capdSplit.getCapdSplitItemList().get(0).getSupplierCode());
				assert baseUnit1.equals(capdSplit.getCapdSplitItemList().get(0).getBaseUnit());
				assert qty1.equals(capdSplit.getCapdSplitItemList().get(0).getQty01());
				assert capdSplit.getCapdSplitItemList().get(0).getQty02().equals(0);
				assert capdSplit.getCapdSplitItemList().get(0).getQty03().equals(0);
				assert capdSplit.getCapdSplitItemList().get(0).getQty04().equals(0);
				assert capdSplit.getCapdSplitItemList().get(0).getQty05().equals(0);
				assert capdSplit.getCapdSplitItemList().get(0).getQty06().equals(0);
				assert capdSplit.getCapdSplitItemList().get(0).getQty07().equals(0);
				assert capdSplit.getCapdSplitItemList().get(0).getQty08().equals(0);
				assert capdSplit.getCapdSplitItemList().get(0).getQty09().equals(0);
				assert capdSplit.getCapdSplitItemList().get(0).getQty10().equals(0);
				assert qty1.equals(capdSplit.getCapdSplitItemList().get(0).getDispQty());
				assert capdSplit.getCapdSplitItemList().get(0).getPrintFlag().equals(Boolean.TRUE);
				assert capdSplit.getCapdSplitItemList().get(0).getItemNum().equals(itemNum1);
				assert capdSplit.getCapdSplitItemList().get(0).getMedOrderItemNum().equals(itemNum1);
				
				capdSplit.getCapdSplitItemList().get(0).setQty01(100);
				capdSplit.getCapdSplitItemList().get(0).setQty02(68);
				
				//update CapdSplit
				CapdSplitServiceLocal capdSplitService = (CapdSplitServiceLocal)getValue("#{capdSplitService}");
				capdSplitService.updateCapdSplit(capdSplit, false);
				pharmOrder = (PharmOrder)getValue("#{pharmOrder}");
				
				assert pharmOrder.getCapdSplit() != null;
	        }
		}.run();
		
		// retrieveCapdSupplierSystem
		new ComponentTest() {
			protected void testComponents() throws Exception {
				PharmOrder pharmOrder = (PharmOrder)getValue("#{pharmOrder}");
				
				MedOrder medOrder = pharmOrder.getMedOrder();		
				MedOrderItem medOrderItem = new MedOrderItem(new CapdRxDrug());
				
				CapdRxDrug capdRxDrug = medOrderItem.getCapdRxDrug();
				capdRxDrug.setUpdateGroupNumFlag(Boolean.TRUE);
				CapdItem capdItem = new CapdItem();
				capdItem.setItemCode(itemCode2);
				capdItem.setSupplierCode(null);
				capdItem.setBaseUnit(baseUnit2);
				capdRxDrug.getCapdItemList().add(capdItem);
				
				medOrderItem.setCapdRxDrug(capdRxDrug);
				medOrderItem.setActionStatus(ActionStatus.DispByPharm);
				medOrderItem.setStatus(MedOrderItemStatus.Outstanding);
				medOrderItem.setItemNum(itemNum2);
				medOrderItem.setItemCode(itemCode2);
				
				PharmOrderItem pharmOrderItem = new PharmOrderItem();
				pharmOrderItem.setItemCode(itemCode2);
				pharmOrderItem.setItemNum(itemNum2);
				pharmOrderItem.setOrgItemNum(itemNum2);
				pharmOrderItem.setIssueQty(BigDecimal.valueOf(qty2));
				pharmOrderItem.setCalQty(BigDecimal.valueOf(qty2));
				pharmOrderItem.setMedOrderItem(medOrderItem);
				pharmOrderItem.setConnectSystem(connectSystem2);
				DmDrugLite dmDrugLite = new DmDrugLite();
				dmDrugLite.setItemCode(itemCode2);
				pharmOrderItem.setDmDrugLite(dmDrugLite);

				pharmOrder.getPharmOrderItemList().add(pharmOrderItem);
				medOrderItem.getPharmOrderItemList().add(pharmOrderItem);
				medOrder.getMedOrderItemList().add(medOrderItem);
				
				//non-Capd Item
				MedOrderItem nonCapdMedOrderItem = new MedOrderItem(new OralRxDrug());
				nonCapdMedOrderItem.setItemCode("PARA01");
				nonCapdMedOrderItem.setItemNum(3);
				
				PharmOrderItem nonCapdPharmOrderItem = new PharmOrderItem();
				nonCapdPharmOrderItem.setItemCode("PARA01");
				nonCapdPharmOrderItem.setItemNum(3);
				nonCapdPharmOrderItem.setMedOrderItem(nonCapdMedOrderItem);
				
				nonCapdMedOrderItem.getPharmOrderItemList().add(nonCapdPharmOrderItem);
				pharmOrder.getPharmOrderItemList().add(nonCapdPharmOrderItem);
				medOrder.getMedOrderItemList().add(nonCapdMedOrderItem);

				Contexts.getSessionContext().set("pharmOrder", pharmOrder);
				
				invokeMethod("#{capdSplitService.retrieveCapdSplit}");
				
				CapdSplit capdSplit = (CapdSplit)getValue("#{capdSplit}");
				
				assert capdSplit != null;
				assert capdSplit.getCapdSplitItemList().size() == 2;
				
				int checkCount = 0;
				for (CapdSplitItem capdSplitItem : capdSplit.getCapdSplitItemList()) { 
					if ( itemCode1.equals(capdSplitItem.getItemCode())) {
						checkCount++;
						assert supplier1.equals(capdSplitItem.getSupplierCode());
						assert baseUnit1.equals(capdSplitItem.getBaseUnit());
						assert capdSplitItem.getQty01().equals(100);
						assert capdSplitItem.getQty02().equals(68);
						assert capdSplitItem.getQty03().equals(0);
						assert capdSplitItem.getQty04().equals(0);
						assert capdSplitItem.getQty05().equals(0);
						assert capdSplitItem.getQty06().equals(0);
						assert capdSplitItem.getQty07().equals(0);
						assert capdSplitItem.getQty08().equals(0);
						assert capdSplitItem.getQty09().equals(0);
						assert capdSplitItem.getQty10().equals(0);
						assert qty1.equals(capdSplitItem.getDispQty());
						assert capdSplitItem.getPrintFlag().equals(Boolean.TRUE);
						assert capdSplitItem.getItemNum().equals(itemNum1);
						assert capdSplitItem.getMedOrderItemNum().equals(itemNum1);
						
					} else if ( itemCode2.equals(capdSplitItem.getItemCode())) {
						checkCount++;
						assert supplier2.equals(capdSplitItem.getSupplierCode());
						assert baseUnit2.equals(capdSplitItem.getBaseUnit());
						assert qty2.equals(capdSplitItem.getQty01());
						assert capdSplitItem.getQty02().equals(0);
						assert capdSplitItem.getQty03().equals(0);
						assert capdSplitItem.getQty04().equals(0);
						assert capdSplitItem.getQty05().equals(0);
						assert capdSplitItem.getQty06().equals(0);
						assert capdSplitItem.getQty07().equals(0);
						assert capdSplitItem.getQty08().equals(0);
						assert capdSplitItem.getQty09().equals(0);
						assert capdSplitItem.getQty10().equals(0);
						assert qty2.equals(capdSplitItem.getDispQty());
						assert capdSplitItem.getPrintFlag().equals(Boolean.TRUE);
						assert capdSplitItem.getItemNum().equals(itemNum2);
						assert capdSplitItem.getMedOrderItemNum().equals(itemNum2);
						
						capdSplitItem.setQty01(200);
						capdSplitItem.setQty02(24);
						
					} else {
						assert false;
					}
				}
				assert checkCount == 2;
				
				// update CapdSplit
				CapdSplitServiceLocal capdSplitService = (CapdSplitServiceLocal)getValue("#{capdSplitService}");
				capdSplitService.updateCapdSplit(capdSplit, false);
				pharmOrder = (PharmOrder)getValue("#{pharmOrder}");
				
				assert pharmOrder.getCapdSplit() != null;
	        }
		}.run();
		
		// update PharmOrderItem Qty
		new ComponentTest() {
			protected void testComponents() throws Exception {
				
				PharmOrder pharmOrder = (PharmOrder)getValue("#{pharmOrder}");
				
				for (PharmOrderItem pharmOrderItem : pharmOrder.getPharmOrderItemList()){
					if ( itemCode1.equals(pharmOrderItem.getItemCode()) ) {
						pharmOrderItem.setIssueQty(BigDecimal.valueOf(newQty1));
						break;
					}
				}
				

				Contexts.getSessionContext().set("pharmOrder", pharmOrder);
				
				invokeMethod("#{capdSplitService.retrieveCapdSplit}");
				
				CapdSplit capdSplit = (CapdSplit)getValue("#{capdSplit}");
				
				assert capdSplit != null;
				assert capdSplit.getCapdSplitItemList().size() == 2;
				
				int checkCount = 0;
				for (CapdSplitItem capdSplitItem : capdSplit.getCapdSplitItemList()) { 
					if ( itemCode1.equals(capdSplitItem.getItemCode())) {
						checkCount++;
						assert supplier1.equals(capdSplitItem.getSupplierCode());
						assert baseUnit1.equals(capdSplitItem.getBaseUnit());
						assert capdSplitItem.getQty01().equals(newQty1);
						assert capdSplitItem.getQty02().equals(0);
						assert capdSplitItem.getQty03().equals(0);
						assert capdSplitItem.getQty04().equals(0);
						assert capdSplitItem.getQty05().equals(0);
						assert capdSplitItem.getQty06().equals(0);
						assert capdSplitItem.getQty07().equals(0);
						assert capdSplitItem.getQty08().equals(0);
						assert capdSplitItem.getQty09().equals(0);
						assert capdSplitItem.getQty10().equals(0);
						assert newQty1.equals(capdSplitItem.getDispQty());
						assert capdSplitItem.getPrintFlag().equals(Boolean.TRUE);
						assert capdSplitItem.getItemNum().equals(itemNum1);
						assert capdSplitItem.getMedOrderItemNum().equals(itemNum1);
						
					} else if ( itemCode2.equals(capdSplitItem.getItemCode())) {
						checkCount++;
						assert supplier2.equals(capdSplitItem.getSupplierCode());
						assert baseUnit2.equals(capdSplitItem.getBaseUnit());
						assert capdSplitItem.getQty01().equals(200);
						assert capdSplitItem.getQty02().equals(24);
						assert capdSplitItem.getQty03().equals(0);
						assert capdSplitItem.getQty04().equals(0);
						assert capdSplitItem.getQty05().equals(0);
						assert capdSplitItem.getQty06().equals(0);
						assert capdSplitItem.getQty07().equals(0);
						assert capdSplitItem.getQty08().equals(0);
						assert capdSplitItem.getQty09().equals(0);
						assert capdSplitItem.getQty10().equals(0);
						assert qty2.equals(capdSplitItem.getDispQty());
						assert capdSplitItem.getPrintFlag().equals(Boolean.TRUE);
						assert capdSplitItem.getItemNum().equals(itemNum2);
						assert capdSplitItem.getMedOrderItemNum().equals(itemNum2);
						
					} else {
						assert false;
					}
				}
				assert checkCount == 2;
				
				// update CapdSplit
				CapdSplitServiceLocal capdSplitService = (CapdSplitServiceLocal)getValue("#{capdSplitService}");
				capdSplitService.updateCapdSplit(capdSplit, false);
				pharmOrder = (PharmOrder)getValue("#{pharmOrder}");
				
				assert pharmOrder.getCapdSplit() != null;
	        }
		}.run();
		
		// update MedOrderItem status
		new ComponentTest() {
			protected void testComponents() throws Exception {
				
				PharmOrder pharmOrder = (PharmOrder)getValue("#{pharmOrder}");
				MedOrder medOrder = pharmOrder.getMedOrder();
				
				for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()){
					if ( itemCode1.equals(medOrderItem.getItemCode()) ) {
						medOrderItem.setStatus(MedOrderItemStatus.SysDeleted);
					} else if ( itemCode2.equals(medOrderItem.getItemCode()) ) {
						medOrderItem.setActionStatus(ActionStatus.DispInClinic);
					}
				}
				

				Contexts.getSessionContext().set("pharmOrder", pharmOrder);
				
				invokeMethod("#{capdSplitService.retrieveCapdSplit}");
				
				CapdSplit capdSplit = (CapdSplit)getValue("#{capdSplit}");
				
				assert capdSplit != null;
				assert capdSplit.getCapdSplitItemList().size() == 0;
				
				// update CapdSplit
				CapdSplitServiceLocal capdSplitService = (CapdSplitServiceLocal)getValue("#{capdSplitService}");
				capdSplitService.updateCapdSplit(capdSplit, false);
				pharmOrder = (PharmOrder)getValue("#{pharmOrder}");
				
				assert pharmOrder.getCapdSplit() != null;
	        }
		}.run();
		
		// update MedOrderItem status
		new ComponentTest() {
			protected void testComponents() throws Exception {
				PharmOrder pharmOrder = (PharmOrder)getValue("#{pharmOrder}");
				
				MedOrder medOrder = pharmOrder.getMedOrder();		
				
				CapdSplit capdSplit = new CapdSplit();
				CapdSplitItem newCapdSplitItem = new CapdSplitItem();
				newCapdSplitItem.setPrintFlag(Boolean.TRUE);
				newCapdSplitItem.setGroupNum(1);
				newCapdSplitItem.setItemCode(itemCode1);
				newCapdSplitItem.setQty01(qty1);
				newCapdSplitItem.setQty02(0);
				newCapdSplitItem.setQty03(0);
				newCapdSplitItem.setQty04(0);
				newCapdSplitItem.setQty05(0);
				newCapdSplitItem.setQty06(0);
				newCapdSplitItem.setQty07(0);
				newCapdSplitItem.setQty08(0);
				newCapdSplitItem.setQty09(0);
				newCapdSplitItem.setQty10(0);
				newCapdSplitItem.setDispQty(qty1);
				newCapdSplitItem.setMedOrderItemNum(itemNum1);
				newCapdSplitItem.setItemNum(itemNum1);
				newCapdSplitItem.getVoucherNumList().add("QMH11000001");
				newCapdSplitItem.setSupplierCode(supplier1);
				newCapdSplitItem.setGenVoucherFlag(Boolean.FALSE);
				capdSplit.getCapdSplitItemList().add(newCapdSplitItem);
				capdSplit.setCapdManualVoucherList(new ArrayList<CapdManualVoucher>());
				
				JaxbWrapper<CapdSplit> vettingJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_VETTING);
				String capdSplitXml = vettingJaxbWrapper.marshall(capdSplit);
				pharmOrder.setCapdSplitXml(capdSplitXml);
				
				
				for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()){
					if ( itemCode1.equals(medOrderItem.getItemCode()) ) {
						medOrderItem.setStatus(MedOrderItemStatus.Outstanding);
					} else if ( itemCode2.equals(medOrderItem.getItemCode()) ) {
						medOrderItem.setActionStatus(ActionStatus.DispByPharm);
					}
				}
				

				Contexts.getSessionContext().set("pharmOrder", pharmOrder);
				
				invokeMethod("#{capdSplitService.retrieveCapdSplit}");
				
				capdSplit = (CapdSplit)getValue("#{capdSplit}");
				
				assert capdSplit != null;
				assert capdSplit.getCapdSplitItemList().size() == 2;
				
				int checkCount = 0;
				for (CapdSplitItem capdSplitItem : capdSplit.getCapdSplitItemList()) { 
					if ( itemCode1.equals(capdSplitItem.getItemCode())) {
						checkCount++;
						assert supplier1.equals(capdSplitItem.getSupplierCode());
						assert baseUnit1.equals(capdSplitItem.getBaseUnit());
						assert capdSplitItem.getQty01().equals(newQty1);
						assert capdSplitItem.getQty02().equals(0);
						assert capdSplitItem.getQty03().equals(0);
						assert capdSplitItem.getQty04().equals(0);
						assert capdSplitItem.getQty05().equals(0);
						assert capdSplitItem.getQty06().equals(0);
						assert capdSplitItem.getQty07().equals(0);
						assert capdSplitItem.getQty08().equals(0);
						assert capdSplitItem.getQty09().equals(0);
						assert capdSplitItem.getQty10().equals(0);
						assert newQty1.equals(capdSplitItem.getDispQty());
						assert capdSplitItem.getPrintFlag().equals(Boolean.TRUE);
						assert capdSplitItem.getItemNum().equals(itemNum1);
						assert capdSplitItem.getMedOrderItemNum().equals(itemNum1);
						
					} else if ( itemCode2.equals(capdSplitItem.getItemCode())) {
						checkCount++;
						assert supplier2.equals(capdSplitItem.getSupplierCode());
						assert baseUnit2.equals(capdSplitItem.getBaseUnit());
						assert qty2.equals(capdSplitItem.getQty01());
						assert capdSplitItem.getQty02().equals(0);
						assert capdSplitItem.getQty03().equals(0);
						assert capdSplitItem.getQty04().equals(0);
						assert capdSplitItem.getQty05().equals(0);
						assert capdSplitItem.getQty06().equals(0);
						assert capdSplitItem.getQty07().equals(0);
						assert capdSplitItem.getQty08().equals(0);
						assert capdSplitItem.getQty09().equals(0);
						assert capdSplitItem.getQty10().equals(0);
						assert qty2.equals(capdSplitItem.getDispQty());
						assert capdSplitItem.getPrintFlag().equals(Boolean.TRUE);
						assert capdSplitItem.getItemNum().equals(itemNum2);
						assert capdSplitItem.getMedOrderItemNum().equals(itemNum2);
						
						capdSplitItem.setQty01(200);
						capdSplitItem.setQty02(24);
						
					} else {
						assert false;
					}
				}
				assert checkCount == 2;
				
				// update CapdSplit
				CapdSplitServiceLocal capdSplitService = (CapdSplitServiceLocal)getValue("#{capdSplitService}");
				capdSplitService.updateCapdSplit(capdSplit, true);

				pharmOrder = (PharmOrder)getValue("#{pharmOrder}");
				
				assert pharmOrder.getCapdSplit() != null;
	        }
		}.run();
		

	}
	
	@Test
	public void testCapdVoucherComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
//		update CapdVoucher
		new ComponentTest() {
			protected void testComponents() throws Exception {
				
				OrderViewInfo orderViewInfo = new OrderViewInfo();
				Contexts.getSessionContext().set("orderViewInfo", orderViewInfo);

				CapdVoucherManagerLocal capdVoucherManager = (CapdVoucherManagerLocal)getValue("#{capdVoucherManager}");
				List<CapdVoucher> prevCvList = capdVoucherManager.retrieveCapdVoucherList(null);

				DispOrderManagerLocal dispOrderManager = (DispOrderManagerLocal)getValue("#{dispOrderManager}");
				DispOrder dispOrder = dispOrderManager.retrieveDispOrderById(Long.valueOf("63"));
				dispOrder.getPharmOrder().loadCapdSplit();
				assert dispOrder.getPharmOrder().getCapdSplit() != null;
				
				List<CapdVoucher> capdVoucherList = capdVoucherManager.createCapdVoucherList(dispOrder, prevCvList);
				
				assert capdVoucherList.size() == 2;
				
				for ( CapdVoucher capdVoucher : capdVoucherList ) {
					assert capdVoucher.getStatus().equals(CapdVoucherStatus.Issued);
					assert StringUtils.isNotBlank(capdVoucher.getVoucherNum());
					for (CapdVoucherItem capdVoucherItem : capdVoucher.getCapdVoucherItemList()) {
						if ( itemCode1.equals(capdVoucherItem.getDispOrderItem().getPharmOrderItem().getItemCode())) {
							assert qty1.equals(capdVoucherItem.getQty());
						} else if ( itemCode2.equals(capdVoucherItem.getDispOrderItem().getPharmOrderItem().getItemCode())) {
							assert qty2.equals(capdVoucherItem.getQty());
						} else {
							assert false;
						}
					}
				}
			}
		}.run();
		
//		update and delete CapdVoucher, retrieve CapdVoucherPrintOut
		new ComponentTest() {
			protected void testComponents() throws Exception {
				
				DispOrderManagerLocal dispOrderManager = (DispOrderManagerLocal)getValue("#{dispOrderManager}");
				DispOrder dispOrder = dispOrderManager.retrieveDispOrderById(Long.valueOf("64"));
				dispOrder.loadChild();
				dispOrder.getPharmOrder().loadCapdSplit();
				assert dispOrder.getPharmOrder().getCapdSplit() != null;
								CapdVoucherManagerLocal capdVoucherManager = (CapdVoucherManagerLocal)getValue("#{capdVoucherManager}");
				List<CapdVoucher> prevCvList = capdVoucherManager.retrieveCapdVoucherList(dispOrder);
				assert prevCvList.size() >0;
				
				List<CapdVoucher> capdVoucherList = capdVoucherManager.createCapdVoucherList(dispOrder, prevCvList);
				assert capdVoucherList.size() ==3 ;
				
				for ( CapdVoucher capdVoucher : capdVoucherList ) {
					assert StringUtils.isNotBlank(capdVoucher.getVoucherNum());
					for (CapdVoucherItem capdVoucherItem : capdVoucher.getCapdVoucherItemList()) {
						if ( itemCode2.equals(capdVoucherItem.getDispOrderItem().getPharmOrderItem().getItemCode())) {
							if ( capdVoucher.getStatus() == CapdVoucherStatus.Issued ) {
								assert qty2.equals(capdVoucherItem.getQty());
							}
						} else {
							assert capdVoucher.getStatus() == CapdVoucherStatus.Deleted;
						}
					}
					
					if ( capdVoucher.getStatus() == CapdVoucherStatus.Issued ) {
						CapdVoucherPrintOut capdVoucherPrintOut = capdVoucherManager.retrieveCapdVoucherPrintOut(capdVoucher, false, PrintLang.Eng);
						
						assert capdVoucherPrintOut != null;
						assert capdVoucherPrintOut.getCapdVoucherPrintOutHdr().getVoucherNum().equals(capdVoucher.getVoucherNum());
						assert capdVoucherPrintOut.getCapdVoucherPrintOutDtl().getItemCode1().equals(capdVoucher.getCapdVoucherItemList().get(0).getDispOrderItem().getPharmOrderItem().getItemCode());
						assert capdVoucherPrintOut.getCapdVoucherPrintOutDtl().getDispQty1().equals(capdVoucher.getCapdVoucherItemList().get(0).getQty().toString());
					}
				}				
			}
		}.run();
	}	
}
