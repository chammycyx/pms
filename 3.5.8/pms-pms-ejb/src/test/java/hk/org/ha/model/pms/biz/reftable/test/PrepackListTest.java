package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.persistence.reftable.Prepack;

import java.util.ArrayList;
import java.util.List;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class PrepackListTest extends SeamTest {

	@Test
	public void testPrepackListComponent() throws Exception {
		
		// Login
//		new ComponentTest() {
//			protected void testComponents() throws Exception {
//				assert getValue("#{identity.loggedIn}").equals(false);
//	            setValue("#{identity.username}", "admin");
//	            setValue("#{identity.password}", "admin");
//	            invokeMethod("#{identity.login}");
//	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
//			}
//		}.run();
//		
//		// retrieve prepack list
//		new ComponentTest() {
//			@SuppressWarnings("unchecked")
//			protected void testComponents() throws Exception {
//				invokeMethod("#{prepackListService.retrievePrepackList()}");
//				List<Prepack> prepackList = (List<Prepack>) getValue("#{prepackList}");
//				assert prepackList.size()>0;
//
//			}
//		}.run();
//		
//		// add prepack
//		new ComponentTest() {
//			protected void testComponents() throws Exception {
//				
//				Prepack prepack = new Prepack();
//				prepack.setItemCode("ACET32");
//				List<Integer> prepackQtyList = new ArrayList<Integer>();
//				prepackQtyList.add(1000);
//				prepackQtyList.add(2000);
//				prepack.setPrepackQty(prepackQtyList);
//				
//				List<Prepack> updatePrepackList = new ArrayList<Prepack>();
//				updatePrepackList.add(prepack);
//
//				Contexts.getSessionContext().set("updatePrepackList", updatePrepackList);
//				invokeMethod("#{prepackListService.updatePrepackList(updatePrepackList)}");
//			}
//		}.run();
//		
//		// check prepack
//		new ComponentTest() {
//			@SuppressWarnings("unchecked")
//			protected void testComponents() throws Exception {
//				invokeMethod("#{prepackListService.retrievePrepackList()}");
//				List<Prepack> prepackList = (List<Prepack>) getValue("#{prepackList}");
//				assert prepackList.size()>0;
//				
//				for(Prepack prepack:prepackList) {
//					if (prepack.getItemCode().equals("ACET32")) {
//						List<Integer> prepackQtyList = prepack.getPrepackQty();
//						for (Integer prepackQty:prepackQtyList) {
//							if(prepackQty == 1000){
//								assert true;
//							}else if(prepackQty == 2000){
//								assert true;
//							}else {
//								assert false;
//							}
//						}
//					}
//				}
//			}
//		}.run();
//		
//		// update prepack
//		new ComponentTest() {
//			@SuppressWarnings("unchecked")
//			protected void testComponents() throws Exception {
//				invokeMethod("#{prepackListService.retrievePrepackList()}");
//				List<Prepack> prepackList = (List<Prepack>) getValue("#{prepackList}");
//				assert prepackList.size()>0;
//
//				List<Prepack> updatePrepackList = new ArrayList<Prepack>();
//				List<Integer> updatePrepackQtyList = new ArrayList<Integer>();
//				
//				for(Prepack prepack:prepackList) {
//					if (prepack.getItemCode().equals("ACET32")) {
//						List<Integer> prepackQtyList = prepack.getPrepackQty();
//						for (Integer prepackQty:prepackQtyList) {
//							if(prepackQty == 1000){
//								updatePrepackQtyList.add(10);
//							}else{
//								updatePrepackQtyList.add(prepackQty);
//							}
//						}
//						updatePrepackQtyList.add(3000);
//						prepack.setPrepackQty(updatePrepackQtyList);
//						updatePrepackList.add(prepack);
//					}
//				}
//
//
//				Contexts.getSessionContext().set("updatePrepackList", updatePrepackList);
//				invokeMethod("#{prepackListService.updatePrepackList(updatePrepackList)}");
//			}
//		}.run();
//		
//		// check prepack
//		new ComponentTest() {
//			@SuppressWarnings("unchecked")
//			protected void testComponents() throws Exception {
//				invokeMethod("#{prepackListService.retrievePrepackList()}");
//				List<Prepack> prepackList = (List<Prepack>) getValue("#{prepackList}");
//				assert prepackList.size()>0;
//				
//				for(Prepack prepack:prepackList) {
//					if (prepack.getItemCode().equals("ACET32")) {
//						List<Integer> prepackQtyList = prepack.getPrepackQty();
//						for (Integer prepackQty:prepackQtyList) {
//							if(prepackQty == 10){
//								assert true;
//							}else if(prepackQty == 2000){
//								assert true;
//							}else if(prepackQty == 3000){
//								assert true;
//							}else {
//								assert false;
//							}
//						}
//					}
//				}
//			}
//		}.run();
//		
//		// delete prepack
//		new ComponentTest() {
//			@SuppressWarnings("unchecked")
//			protected void testComponents() throws Exception {
//				invokeMethod("#{prepackListService.retrievePrepackList()}");
//				List<Prepack> prepackList = (List<Prepack>) getValue("#{prepackList}");
//				assert prepackList.size()>0;
//
//				List<Prepack> updatePrepackList = new ArrayList<Prepack>();
//				
//				for(Prepack prepack:prepackList) {
//					if (prepack.getItemCode().equals("ACET32")) {
//						prepack.setMarkDelete(true);
//						updatePrepackList.add(prepack);
//					}
//				}
//
//				Contexts.getSessionContext().set("updatePrepackList", updatePrepackList);
//				invokeMethod("#{prepackListService.updatePrepackList(updatePrepackList)}");
//			}
//		}.run();
//		
//		// check prepack
//		new ComponentTest() {
//			@SuppressWarnings("unchecked")
//			protected void testComponents() throws Exception {
//				invokeMethod("#{prepackListService.retrievePrepackList()}");
//				List<Prepack> prepackList = (List<Prepack>) getValue("#{prepackList}");
//				assert prepackList.size()>0;
//				
//				for(Prepack prepack:prepackList) {
//					if (prepack.getItemCode().equals("ACET32")) {
//						assert false;
//					}
//				}
//			}
//		}.run();
	}	
}
