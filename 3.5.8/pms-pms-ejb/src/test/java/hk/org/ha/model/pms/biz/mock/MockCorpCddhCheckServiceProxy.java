package hk.org.ha.model.pms.biz.mock;

import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpCddhCheckServiceJmsRemote;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;

@Startup
@Name("corpCddhCheckServiceProxy")
@Scope(ScopeType.APPLICATION)
public class MockCorpCddhCheckServiceProxy implements CorpCddhCheckServiceJmsRemote {

	@Override
	public Map<Integer, List<DispOrderItem>> retrieveDuplicateDispensedItem(
			String arg0, String arg1, String arg2, String arg3,
			Map<Integer, List<PharmOrderItem>> arg4, Boolean arg5) {
		return new HashMap<Integer, List<DispOrderItem>>();
	}
}
