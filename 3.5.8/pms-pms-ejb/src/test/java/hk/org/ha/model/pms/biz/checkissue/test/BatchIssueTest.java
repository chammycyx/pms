package hk.org.ha.model.pms.biz.checkissue.test;

import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.model.pms.biz.checkissue.BatchIssueServiceLocal;
import hk.org.ha.model.pms.biz.order.DispOrderManagerLocal;
import hk.org.ha.model.pms.biz.picking.DrugPickServiceLocal;
import hk.org.ha.model.pms.biz.reftable.WorkstationManagerLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.BatchIssueLog;
import hk.org.ha.model.pms.persistence.disp.BatchIssueLogDetail;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.vo.checkissue.BatchIssueDetail;
import hk.org.ha.model.pms.vo.report.BatchIssueRpt;

import java.util.List;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.joda.time.DateTime;
import org.testng.annotations.Test;

public class BatchIssueTest extends SeamTest {
	
	private Integer batchIssueLogSize = 0;
	
	@Test
	public void testBatchIssueComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
	            
	            UamInfo uamInfo = new UamInfo();
				Contexts.getSessionContext().set("uamInfo", uamInfo);
			}
		}.run();
		
		//retrieveBatchIssueLogList
		new ComponentTest() {
			
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				invokeMethod("#{batchIssueListService.retrieveBatchIssueLogList}");	
				List<BatchIssueLog> batchIssueLogList = (List<BatchIssueLog>)getValue("#{batchIssueLogList}");
				
				assert batchIssueLogList != null;
				batchIssueLogSize = batchIssueLogList.size();
			}
		}.run();	
		
		//retrieveWorkstoreList
		new ComponentTest() {
			
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				invokeMethod("#{batchIssueListService.retrieveWorkstoreList}");	
				List<Workstore> workstoreList = (List<Workstore>)getValue("#{workstoreList}");
				
				assert workstoreList.size()>0;
			}
		}.run();
		
		//retrieveBatchIssueLogDetail
		new ComponentTest() {
			
			protected void testComponents() throws Exception {
				DateTime dispDate = new DateTime().withDate(2011, 4, 30);
				
				Workstore workstore = (Workstore)getValue("#{workstore}");
				
				BatchIssueServiceLocal batchIssueService = (BatchIssueServiceLocal) getInstance("batchIssueService");
				batchIssueService.retrieveBatchIssueDetail(dispDate.toDate(), workstore);
				BatchIssueDetail batchIssueDetail = (BatchIssueDetail)getValue("#{batchIssueDetail}");
				
				
				assert batchIssueDetail!=null;
				assert batchIssueDetail.getBatchIssueLogDetailList().size() == 1;
				assert !batchIssueDetail.isEnableIssue();
				assert !batchIssueDetail.isOverrideBatchIssueLogFlag();

			}
		}.run();	
		
		//retrieveBatchIssueDetail
		new ComponentTest() {
			
			protected void testComponents() throws Exception {
				DateTime dispDate = new DateTime().withDate(2011, 5, 2);
				
				Workstore workstore = (Workstore)getValue("#{workstore}");
				
				BatchIssueServiceLocal batchIssueService = (BatchIssueServiceLocal) getInstance("batchIssueService");
				batchIssueService.retrieveBatchIssueDetail(dispDate.toDate(), workstore);
				BatchIssueDetail batchIssueDetail = (BatchIssueDetail)getValue("#{batchIssueDetail}");
				
				
				assert batchIssueDetail!=null;
				assert !batchIssueDetail.isEnableIssue();
				assert batchIssueDetail.isOverrideBatchIssueLogFlag();
				assert batchIssueDetail.getBatchIssueList()!=null;
			}
		}.run();	
		
		
		new ComponentTest() {
			
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				DateTime dispDate = new DateTime().withDate(2011, 5, 20);
				
				Workstore workstore = (Workstore)getValue("#{workstore}");
				
				BatchIssueServiceLocal batchIssueService = (BatchIssueServiceLocal) getInstance("batchIssueService");
				batchIssueService.retrieveBatchIssueDetail(dispDate.toDate(), workstore);
				BatchIssueDetail batchIssueDetail = (BatchIssueDetail)getValue("#{batchIssueDetail}");
				
				
				assert batchIssueDetail!=null;
				assert batchIssueDetail.isEnableIssue();
				assert !batchIssueDetail.isOverrideBatchIssueLogFlag();
				assert batchIssueDetail.getBatchIssueList()!=null;
				assert batchIssueDetail.getBatchIssueList().size() == 1;
				
				invokeMethod("#{batchIssueListService.retrieveBatchIssueLogList}");	
				List<BatchIssueLog> batchIssueLogList = (List<BatchIssueLog>)getValue("#{batchIssueLogList}");
				
				assert batchIssueLogList.size() == batchIssueLogSize+1;
			}
		}.run();
		
		//update BatchIssueLogDetail
		new ComponentTest() {
			
			protected void testComponents() throws Exception {
				DateTime dispDate = new DateTime().withDate(2011, 5, 20);
				
				Workstore workstore = (Workstore)getValue("#{workstore}");
				
				invokeMethod("#{batchIssueService.updateBatchIssueLogDetail(batchIssueDetail)}");
				
				BatchIssueServiceLocal batchIssueService = (BatchIssueServiceLocal) getInstance("batchIssueService");
				batchIssueService.retrieveBatchIssueDetail(dispDate.toDate(), workstore);
				BatchIssueDetail batchIssueDetail = (BatchIssueDetail)getValue("#{batchIssueDetail}");

				assert batchIssueDetail!=null;
				assert !batchIssueDetail.isEnableIssue();
				assert !batchIssueDetail.isOverrideBatchIssueLogFlag();
				assert batchIssueDetail.getBatchIssueLogDetailList().size() == 1;
				for (BatchIssueLogDetail batchIssuelogDetail : batchIssueDetail.getBatchIssueLogDetailList()){
					assert batchIssuelogDetail.getDispOrder().getStatus().equals(DispOrderStatus.Issued);
					
				}
			}
		}.run();
		
		new ComponentTest() {
			
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				DateTime dispDate = new DateTime().withDate(2011, 5, 21);
				
				Workstore workstore = (Workstore)getValue("#{workstore}");
				
				BatchIssueServiceLocal batchIssueService = (BatchIssueServiceLocal) getInstance("batchIssueService");
				batchIssueService.retrieveBatchIssueDetail(dispDate.toDate(), workstore);
				BatchIssueDetail batchIssueDetail = (BatchIssueDetail)getValue("#{batchIssueDetail}");
				
				
				assert batchIssueDetail!=null;
				assert batchIssueDetail.isEnableIssue();
				assert !batchIssueDetail.isOverrideBatchIssueLogFlag();
				assert batchIssueDetail.getBatchIssueList()!=null;
				assert batchIssueDetail.getBatchIssueList().size() == 3;
				
				invokeMethod("#{batchIssueListService.retrieveBatchIssueLogList}");	
				List<BatchIssueLog> batchIssueLogList = (List<BatchIssueLog>)getValue("#{batchIssueLogList}");
				
				assert batchIssueLogList.size() == batchIssueLogSize+2;
			}
		}.run();
		
		//delete BatchIssueLog
		new ComponentTest() {
			
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				DateTime dispDate = new DateTime().withDate(2011, 5, 21);
				
				Workstore workstore = (Workstore)getValue("#{workstore}");
				
				BatchIssueServiceLocal batchIssueService = (BatchIssueServiceLocal) getInstance("batchIssueService");
				batchIssueService.deleteBatchIssueLog(dispDate.toDate(), workstore);
				
				invokeMethod("#{batchIssueListService.retrieveBatchIssueLogList}");	
				List<BatchIssueLog> batchIssueLogList = (List<BatchIssueLog>)getValue("#{batchIssueLogList}");
				
				assert batchIssueLogList.size() == batchIssueLogSize+1;
			}
		}.run();
		
		new ComponentTest() {
			
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				DateTime dispDate = new DateTime().withDate(2011, 5, 21);
				
				Workstore workstore = (Workstore)getValue("#{workstore}");
				
				BatchIssueServiceLocal batchIssueService = (BatchIssueServiceLocal) getInstance("batchIssueService");
				batchIssueService.retrieveBatchIssueDetail(dispDate.toDate(), workstore);
				BatchIssueDetail batchIssueDetail = (BatchIssueDetail)getValue("#{batchIssueDetail}");
				
				
				assert batchIssueDetail!=null;
				assert batchIssueDetail.isEnableIssue();
				assert !batchIssueDetail.isOverrideBatchIssueLogFlag();
				assert batchIssueDetail.getBatchIssueList()!=null;
				assert batchIssueDetail.getBatchIssueList().size() == 3;
				
				invokeMethod("#{batchIssueListService.retrieveBatchIssueLogList}");	
				List<BatchIssueLog> batchIssueLogList = (List<BatchIssueLog>)getValue("#{batchIssueLogList}");
				
				assert batchIssueLogList.size() == batchIssueLogSize+2;
			}
		}.run();
		
		//update BatchIssueLogDetail
		new ComponentTest() {
			
			protected void testComponents() throws Exception {
				DateTime dispDate = new DateTime().withDate(2011, 5, 21);
				
				Workstore workstore = (Workstore)getValue("#{workstore}");
				
				invokeMethod("#{suspendPrescService.verifySuspendPrescription(58, 0, 'OT')}");	
				
				DispOrderManagerLocal dispOrderManager= (DispOrderManagerLocal) getValue("#{dispOrderManager}");
				DispOrder dispOrder = dispOrderManager.retrieveDispOrderById(58L);
				
				assert dispOrder.getAdminStatus() == DispOrderAdminStatus.Suspended;
				
				WorkstationManagerLocal workstationManager = (WorkstationManagerLocal) getInstance("workstationManager");
				UamInfo uamInfo = new UamInfo();
				uamInfo.setWorkstationId("moepms0002");
				uamInfo.setHospital("QMH");
				uamInfo.setWorkstore("WKS1");
				Workstation workstation = workstationManager.retrieveWorkstationByUamInfo(uamInfo);

				DrugPickServiceLocal drugPickService = (DrugPickServiceLocal) getInstance("drugPickService");
				drugPickService.updateDispOrderItemStatusToPicked(dispDate.toDate(), "0504", 1, "Test", workstation.getId());
				
				invokeMethod("#{batchIssueService.updateBatchIssueLogDetail(batchIssueDetail)}");
				
				BatchIssueServiceLocal batchIssueService = (BatchIssueServiceLocal) getInstance("batchIssueService");
				batchIssueService.retrieveBatchIssueDetail(dispDate.toDate(), workstore);
				BatchIssueDetail batchIssueDetail = (BatchIssueDetail)getValue("#{batchIssueDetail}");

				assert batchIssueDetail!=null;
				assert !batchIssueDetail.isEnableIssue();
				assert !batchIssueDetail.isOverrideBatchIssueLogFlag();
				assert batchIssueDetail.getBatchIssueLogDetailList().size() == 3;
				for (BatchIssueLogDetail batchIssuelogDetail : batchIssueDetail.getBatchIssueLogDetailList()){
					if (batchIssuelogDetail.getDispOrder().getTicket().getTicketNum().equals("0503")) {
						assert batchIssuelogDetail.getDispOrder().getStatus() == DispOrderStatus.Vetted;
					} else if (batchIssuelogDetail.getDispOrder().getTicket().getTicketNum().equals("0504")) {
						assert batchIssuelogDetail.getDispOrder().getStatus() == DispOrderStatus.Picked;
						assert batchIssuelogDetail.getStatus() == DispOrderStatus.Vetted;
					}else {
						assert batchIssuelogDetail.getDispOrder().getStatus() == DispOrderStatus.Issued;
					}
				}
			}
		}.run();
		
		//retrieveBatchIssueRpt
		new ComponentTest() {
			
			protected void testComponents() throws Exception {
				BatchIssueServiceLocal batchIssueService = (BatchIssueServiceLocal) getInstance("batchIssueService");
				BatchIssueRpt batchIssueRpt = batchIssueService.retrieveBatchIssueRpt();
				
				assert batchIssueRpt != null;
			}
		}.run();	
		
		
		//retrieveBatchIssueExceptionRpt
		new ComponentTest() {
			
			protected void testComponents() throws Exception {
				BatchIssueServiceLocal batchIssueService = (BatchIssueServiceLocal) getInstance("batchIssueService");
				BatchIssueRpt batchIssueRpt = batchIssueService.retrieveBatchIssueExceptionRpt();
				
				assert batchIssueRpt != null;
			}
		}.run();	
		
		
		//retrieveBatchIssueSfiRpt
		new ComponentTest() {
			
			protected void testComponents() throws Exception {
				BatchIssueServiceLocal batchIssueService = (BatchIssueServiceLocal) getInstance("batchIssueService");
				BatchIssueRpt batchIssueRpt = batchIssueService.retrieveBatchIssueSfiRpt();
				
				assert batchIssueRpt != null;
			}
		}.run();	
	}	
}
