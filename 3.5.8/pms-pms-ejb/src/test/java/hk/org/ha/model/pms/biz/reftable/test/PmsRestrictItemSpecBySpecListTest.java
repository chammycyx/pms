package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.reftable.PmsRestrictItemSpecBySpecListServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.dms.persistence.PmsRestrictItem;
import hk.org.ha.model.pms.vo.patient.PasSpecialty;

import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class PmsRestrictItemSpecBySpecListTest extends SeamTest {

	@Test
	public void testPmsRestrictItemSpecBySpecListComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		// retrieve pasSpecialtyDisplayList 
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {   
				PmsRestrictItemSpecBySpecListServiceLocal pmsRestrictItemSpecBySpecListService = (PmsRestrictItemSpecBySpecListServiceLocal) getValue("#{pmsRestrictItemSpecBySpecListService}");
				pmsRestrictItemSpecBySpecListService.retrievePasSpecDisplayList();
				List<PasSpecialty> pasSpecialtyDisplayList = (List<PasSpecialty>) getValue("#{pasSpecialtyDisplayList}");
				assert pasSpecialtyDisplayList.size()>0;
			}
		}.run();

		// retrieve restrict list by specialty
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {   
				PmsRestrictItemSpecBySpecListServiceLocal pmsRestrictItemSpecBySpecListService = (PmsRestrictItemSpecBySpecListServiceLocal) getValue("#{pmsRestrictItemSpecBySpecListService}");
				pmsRestrictItemSpecBySpecListService.retrievePmsRestrictItemListBySpecialty("I", "AUD", "");
				List<PmsRestrictItem> pmsRestrictItemListBySpecialty = (List<PmsRestrictItem>) getValue("#{pmsRestrictItemListBySpecialty}");
				assert pmsRestrictItemListBySpecialty.size()>0;
			}
		}.run();
		
		
	}	
}
