package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.reftable.PendingReason;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.util.ArrayList;
import java.util.List;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class PendReasonTest extends SeamTest {

	
	@Test
	public void testPendingReasonComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		
		// retrieve pending list
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				invokeMethod("#{pendReasonService.retrievePendingReasonList}");
				List<PendingReason> pendReasonList = (List<PendingReason>) getValue("#{pendReasonList}");
				assert pendReasonList.size()>0;
			}
		}.run();
		
		// add pendingReason
		new ComponentTest() {
			protected void testComponents() throws Exception {

				List<PendingReason> newPendReasonList = new ArrayList<PendingReason>();
				
				PendingReason pendingReason = new PendingReason();
				pendingReason.setPendingCode("AA");
				pendingReason.setDescription("test pending reason");
				pendingReason.setStatus(RecordStatus.Active);
				
				newPendReasonList.add(pendingReason);
					
				Contexts.getSessionContext().set("pendReasonList", newPendReasonList);
				invokeMethod("#{pendReasonService.updatePendingReasonList( pendReasonList )}");
			}
		
		}.run();
		
		//check pendingReason
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception
			{	
				//retrievePendingReasonList
				invokeMethod("#{pendReasonService.retrievePendingReasonList}");
				List<PendingReason> pendReasonList = (List<PendingReason>) getValue("#{pendReasonList}");
				assert pendReasonList.size()>0;
				
				for(PendingReason pendingReason:pendReasonList){
					if(pendingReason.getPendingCode().equals("AA")){
						assert pendingReason.getDescription().equals("test pending reason");
						assert pendingReason.getStatus().equals(RecordStatus.Active);
						assert pendingReason.getHospital().getHospCode().equals("QMH");
					}
				}
			}
		}.run();	
		
		//Delete pendingReason
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception{
				invokeMethod("#{pendReasonService.retrievePendingReasonList}");
				List<PendingReason> pendReasonList = (List<PendingReason>) getValue("#{pendReasonList}");
				assert pendReasonList.size() >0;
				
				List<PendingReason> newPendReasonList = new ArrayList<PendingReason>();
				for(PendingReason pendingReason: pendReasonList){
					if(pendingReason.getPendingCode().equals("AA")){
						pendingReason.setStatus(RecordStatus.Delete);	
						newPendReasonList.add(pendingReason); 
					}
				}
				Contexts.getSessionContext().set("pendReasonList", newPendReasonList);
				invokeMethod("#{pendReasonService.updatePendingReasonList( pendReasonList )}");
			}
		}.run();
	
		//check pendingReason
		new ComponentTest(){
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception{
				invokeMethod("#{pendReasonService.retrievePendingReasonList}");
				List<PendingReason> pendReasonList = (List<PendingReason>) getValue ("#{pendReasonList}");
				assert pendReasonList.size() >0;
				
				for(PendingReason pendingReason: pendReasonList){
					assert !pendingReason.getPendingCode().equals("AA");
				}
			}
		}.run();
		
		//Add pendingReason with status deleted
		new ComponentTest(){
			protected void testComponents() throws Exception{
				
				PendingReason pendingReason = new PendingReason();
				pendingReason.setPendingCode("AA");
				pendingReason.setDescription("bbb");
				
				List<PendingReason> newPendReasonList = new ArrayList<PendingReason>();
				newPendReasonList.add(pendingReason);
				
				Contexts.getSessionContext().set("pendReasonList", newPendReasonList);
				invokeMethod("#{pendReasonService.updatePendingReasonList( pendReasonList )}");
			}
		}.run();
		
		//check pendingReason
		new ComponentTest(){
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception{
				invokeMethod("#{pendReasonService.retrievePendingReasonList}");
				List<PendingReason> pendReasonList = (List<PendingReason>) getValue ("#{pendReasonList}");
				assert pendReasonList.size() >0;
				
				for(PendingReason pendingReason: pendReasonList){
					if(pendingReason.getPendingCode().equals("AA")){
						assert pendingReason.getStatus().equals(RecordStatus.Active);
						assert pendingReason.getDescription().equals("bbb");
					}
				}
			}
		}.run();
		
	}	
}
