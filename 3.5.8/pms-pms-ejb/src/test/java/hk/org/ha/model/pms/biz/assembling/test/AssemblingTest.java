package hk.org.ha.model.pms.biz.assembling.test;

import hk.org.ha.model.pms.biz.assembling.AssemblingServiceLocal;
import hk.org.ha.model.pms.biz.order.DispOrderManagerLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.vo.assembling.AssembleItem;
import hk.org.ha.model.pms.vo.assembling.AssembleOrder;

import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.joda.time.DateTime;
import org.testng.annotations.Test;

public class AssemblingTest extends SeamTest {

	private DateTime ticketDate;
	private String ticketNum;
	private Integer itemNum;
	
	@Test
	public void testAssemblingComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		//retrieveAssemblingList
		new ComponentTest() {
			
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				invokeMethod("#{assemblingListService.retrieveAssembleOrderList}");	
				List<AssembleOrder> assembleOrderList = (List<AssembleOrder>)getValue("#{assembleOrderList}");
				
				assert assembleOrderList.size()>0;
			}
		}.run();		
		
		// Item has been deleted
		new ComponentTest() {

			protected void testComponents() throws Exception {
				ticketDate = new DateTime().withDate(2011, 8, 3);
				
				ticketNum = "0509";
				itemNum = 1;

				AssemblingServiceLocal assemblingService = (AssemblingServiceLocal) getInstance("assemblingService");
				assemblingService.updateDispOrderItemStatusToAssembled(ticketDate.toDate(), ticketNum, itemNum);
				AssembleItem assembleItem = (AssembleItem)getValue("#{assembleItem}");

				assert !assemblingService.isUpdateSuccess();
				assert assembleItem!=null;
				assert ticketDate.toDate().compareTo(assembleItem.getTicketDate()) == 0;
				assert itemNum == assembleItem.getItemNum();
				assert ticketNum.equals(assembleItem.getTicketNum());
				assert assembleItem.getUrgentFlag() == null;
				assert !assembleItem.isCompleteAssembling();
				assert assembleItem.getMessageCode().equals("0074");
			}
		}.run();

		// Item has not been picked
		new ComponentTest() {

			protected void testComponents() throws Exception {
				ticketDate = new DateTime().withDate(2011, 3, 30);
				
				ticketNum = "0701";
				itemNum = 1;

				AssemblingServiceLocal assemblingService = (AssemblingServiceLocal) getInstance("assemblingService");
				assemblingService.updateDispOrderItemStatusToAssembled(ticketDate.toDate(), ticketNum, itemNum);
				AssembleItem assembleItem = (AssembleItem)getValue("#{assembleItem}");

				assert !assemblingService.isUpdateSuccess();
				assert assembleItem!=null;
				assert ticketDate.toDate().compareTo(assembleItem.getTicketDate()) == 0;
				assert itemNum == assembleItem.getItemNum();
				assert ticketNum.equals(assembleItem.getTicketNum());
				assert !assembleItem.getUrgentFlag();
				assert !assembleItem.isCompleteAssembling();
				assert assembleItem.getMessageCode().equals("0072");
			}
		}.run();

		// Item has been assembled
		new ComponentTest() {

			protected void testComponents() throws Exception {
				ticketDate = new DateTime().withDate(2011, 3, 30);
				
				ticketNum = "0706";
				itemNum = 1;

				AssemblingServiceLocal assemblingService = (AssemblingServiceLocal) getInstance("assemblingService");
				assemblingService.updateDispOrderItemStatusToAssembled(ticketDate.toDate(), ticketNum, itemNum);
				AssembleItem assembleItem = (AssembleItem)getValue("#{assembleItem}");

				assert !assemblingService.isUpdateSuccess();
				assert assembleItem!=null;
				assert ticketDate.toDate().compareTo(assembleItem.getTicketDate()) == 0;
				assert itemNum == assembleItem.getItemNum();
				assert ticketNum.equals(assembleItem.getTicketNum());
				assert !assembleItem.getUrgentFlag();
				assert !assembleItem.isCompleteAssembling();
				assert assembleItem.getMessageCode().equals("0071");
			}
		}.run();

		// Update Item Fail
		new ComponentTest() {

			protected void testComponents() throws Exception {
				ticketDate = new DateTime().withDate(2011, 3, 30);
				
				ticketNum = "0111";
				itemNum = 11;

				AssemblingServiceLocal assemblingService = (AssemblingServiceLocal) getInstance("assemblingService");
				assemblingService.updateDispOrderItemStatusToAssembled(ticketDate.toDate(), ticketNum, itemNum);
				AssembleItem assembleItem = (AssembleItem)getValue("#{assembleItem}");

				assert !assemblingService.isUpdateSuccess();
				assert assembleItem!=null;
				assert ticketDate.toDate().compareTo(assembleItem.getTicketDate()) == 0;
				assert itemNum == assembleItem.getItemNum();
				assert ticketNum.equals(assembleItem.getTicketNum());
				assert assembleItem.getUrgentFlag() == null;
				assert !assembleItem.isCompleteAssembling();
				assert assembleItem.getMessageCode().equals("0061");
			}
		}.run();

		// update dispOrderItem status to assembled
		new ComponentTest() {

			protected void testComponents() throws Exception {
				ticketDate = new DateTime().withDate(2011, 4, 30);
				
				ticketNum = "0502";
				itemNum = 1;

				AssemblingServiceLocal assemblingService = (AssemblingServiceLocal) getInstance("assemblingService");
				assemblingService.updateDispOrderItemStatusToAssembled(ticketDate.toDate(), ticketNum, itemNum);
				AssembleItem assembleItem = (AssembleItem)getValue("#{assembleItem}");

				assert assemblingService.isUpdateSuccess();
				assert assembleItem!=null;
				assert ticketDate.toDate().compareTo(assembleItem.getTicketDate()) == 0;
				assert itemNum == assembleItem.getItemNum();
				assert ticketNum.equals(assembleItem.getTicketNum());
				assert assembleItem.getUrgentFlag();
				assert assembleItem.getMessageCode().equals("0073");
				assert !assembleItem.isCompleteAssembling();

				DispOrderManagerLocal dispOrderManager = (DispOrderManagerLocal) getInstance("dispOrderManager");
				DispOrder dispOrder = dispOrderManager.retrieveDispOrderById(Long.parseLong("46"));
				
				assert dispOrder.getStatus() == DispOrderStatus.Assembling;
			}
		}.run();

		// update dispOrderItem status to assembled and change dispOrder status to assembled
		new ComponentTest() {

			protected void testComponents() throws Exception {
				ticketDate = new DateTime().withDate(2011, 4, 30);
				
				ticketNum = "0502";
				itemNum = 2;

				AssemblingServiceLocal assemblingService = (AssemblingServiceLocal) getInstance("assemblingService");
				assemblingService.updateDispOrderItemStatusToAssembled(ticketDate.toDate(), ticketNum, itemNum);
				AssembleItem assembleItem = (AssembleItem)getValue("#{assembleItem}");

				assert assemblingService.isUpdateSuccess();
				assert assembleItem!=null;
				assert ticketDate.toDate().compareTo(assembleItem.getTicketDate()) == 0;
				assert itemNum == assembleItem.getItemNum();
				assert ticketNum.equals(assembleItem.getTicketNum());
				assert assembleItem.getUrgentFlag();
				assert assembleItem.getMessageCode().equals("0073");
				assert assembleItem.isCompleteAssembling();

				DispOrderManagerLocal dispOrderManager = (DispOrderManagerLocal) getInstance("dispOrderManager");
				DispOrder dispOrder = dispOrderManager.retrieveDispOrderById(Long.parseLong("46"));
				
				assert dispOrder.getStatus() == DispOrderStatus.Assembled;
			}
		}.run();

		// update dispOrderItem status to assembled
		new ComponentTest() {

			protected void testComponents() throws Exception {
				ticketDate = new DateTime().withDate(2011, 3, 30);
				
				ticketNum = "0704";
				itemNum = 1;

				AssemblingServiceLocal assemblingService = (AssemblingServiceLocal) getInstance("assemblingService");
				assemblingService.updateDispOrderItemStatusToAssembled(ticketDate.toDate(), ticketNum, itemNum);
				AssembleItem assembleItem = (AssembleItem)getValue("#{assembleItem}");

				assert assemblingService.isUpdateSuccess();
				assert assembleItem!=null;
				assert ticketDate.toDate().compareTo(assembleItem.getTicketDate()) == 0;
				assert itemNum == assembleItem.getItemNum();
				assert ticketNum.equals(assembleItem.getTicketNum());
				assert !assembleItem.getUrgentFlag();
				assert assembleItem.getMessageCode().equals("0073");

				DispOrderManagerLocal dispOrderManager = (DispOrderManagerLocal) getInstance("dispOrderManager");
				DispOrder dispOrder = dispOrderManager.retrieveDispOrderById(Long.parseLong("20"));
				
				assert dispOrder.getStatus() == DispOrderStatus.Picking;
			}
		}.run();
		
			
		// Item has been suspended
		new ComponentTest() {

			protected void testComponents() throws Exception {
				invokeMethod("#{suspendPrescService.verifySuspendPrescription(49, 0, 'OT')}");	
				
				DispOrderManagerLocal dispOrderManager= (DispOrderManagerLocal) getValue("#{dispOrderManager}");
				DispOrder dispOrder = dispOrderManager.retrieveDispOrderById(49L);
				
				assert dispOrder.getAdminStatus() == DispOrderAdminStatus.Suspended;

				ticketDate = new DateTime().withDate(2011, 4, 30);
				
				ticketNum = "0505";
				itemNum = 1;

				AssemblingServiceLocal assemblingService = (AssemblingServiceLocal) getInstance("assemblingService");
				assemblingService.updateDispOrderItemStatusToAssembled(ticketDate.toDate(), ticketNum, itemNum);
				AssembleItem assembleItem = (AssembleItem)getValue("#{assembleItem}");

				assert !assemblingService.isUpdateSuccess();
				assert assembleItem!=null;
				assert ticketDate.toDate().compareTo(assembleItem.getTicketDate()) == 0;
				assert itemNum == assembleItem.getItemNum();
				assert ticketNum.equals(assembleItem.getTicketNum());
				assert assembleItem.getUrgentFlag() == null;
				assert !assembleItem.isCompleteAssembling();
				assert assembleItem.getMessageCode().equals("0059");
			}
		}.run();
		
	}	
}
