package hk.org.ha.model.pms.biz.printing.mock;

import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.vo.printing.PrinterSelect;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;

import java.io.File;
import java.io.IOException;
import java.util.List;

import net.lingala.zip4j.exception.ZipException;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Install;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@AutoCreate
@Name("printAgent")
@Scope(ScopeType.SESSION)
@Install(precedence=Install.MOCK)
public class MockPrintAgent implements PrintAgentInf {

	private static final long serialVersionUID = 1L;

	@Override
	public void keep(String contentId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void print(PrinterSelect printerSelect, String printInfo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void print(String contentId, PrinterSelect printerSelect, String printInfo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void redirect(String contentId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String render(RenderJob job) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void renderAndPrint(RenderAndPrintJob job) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void renderAndPrint(List<RenderAndPrintJob> jobs) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void renderAndPrint(List<RenderAndPrintJob> jobs, boolean includeLotInfo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void renderAndRedirect(RenderJob job) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void redirect(String contentId, String fileName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void renderAndRedirect(RenderJob job, String fileName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public File generateZipFile(List<File> fileArrayList, String password)
			throws ZipException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void zipAndRedirect(RenderJob job, String password)
			throws IOException, ZipException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void zipAndRedirect(RenderJob job, String fileName, String password)
			throws IOException, ZipException {
		// TODO Auto-generated method stub
		
	}
	
}
