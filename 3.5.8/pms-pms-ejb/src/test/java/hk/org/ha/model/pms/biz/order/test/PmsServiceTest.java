package hk.org.ha.model.pms.biz.order.test;

import hk.org.ha.model.pms.biz.order.OrderManagerLocal;
import hk.org.ha.model.pms.biz.order.PmsServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedOrderFm;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedOrderItemAlert;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.udt.Gender;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.disp.OpTrxType;
import hk.org.ha.model.pms.vo.rx.OralRxDrug;

import java.util.ArrayList;
import java.util.Calendar;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class PmsServiceTest extends SeamTest {

	@Test
	public void testPmsServiceCreateOrder() throws Exception {

		final String orderNum = "QMH1000002";
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
	        }
		}.run();       

		new ComponentTest() {
			protected void testComponents() throws Exception {

				PmsServiceLocal pmsService = (PmsServiceLocal) getValue("#{pmsService}");
				
        	 	MedOrder medOrder = new MedOrder();
           	 
        	 	medOrder.setId(Long.valueOf(2001));
	     		medOrder.setDocType(MedOrderDocType.Normal);
	     		medOrder.setOrderNum(orderNum);
     		
	     		Calendar orderDate = Calendar.getInstance();
	     		orderDate.clear();
	     		orderDate.set(2011, 0, 15);	
	     		medOrder.setOrderDate(orderDate.getTime());
             
	     		medOrder.setRefNum("0012");
	     		medOrder.setPrescType(MedOrderPrescType.Out);
	     		medOrder.setStatus(MedOrderStatus.Outstanding);
     		
				MedOrderItem medOrderItem = new MedOrderItem(new OralRxDrug());		
				medOrderItem.setFirstDisplayName("ACET TESTING");
				medOrderItem.setBaseUnit("CAP");
				medOrderItem.setStatus(MedOrderItemStatus.Outstanding);
				medOrderItem.setItemNum(0);
				medOrderItem.setOrgItemNum(0);		
				medOrderItem.setUnitOfCharge(1);
				
				MedOrderItem medOrderItem2 = new MedOrderItem(new OralRxDrug());		
				medOrderItem2.setFirstDisplayName("PARA TESTING");
				medOrderItem2.setBaseUnit("CAP");
				medOrderItem2.setStatus(MedOrderItemStatus.Outstanding);
				medOrderItem2.setItemNum(0);
				medOrderItem2.setOrgItemNum(0);				
				medOrderItem.setUnitOfCharge(2);
				
				MedOrderItemAlert mOIA = new MedOrderItemAlert();
				mOIA.setAckUser("MO001");
				//mOIA.setAdditionInfo("Confirmed with drug-sensitivity test");
				medOrderItem2.addMedOrderItemAlert(mOIA);
				
				Patient patient = new Patient();
				patient.setId(Long.valueOf(1));
				patient.setHkid("A1234563");
				patient.setSex(Gender.Male);
				
				medOrder.setDoctorCode("MO001");				
				medOrder.setSpecCode("MED");     		

	     		Workstore workstore = new Workstore();
	     		workstore.setHospCode("QMH");
	     		workstore.setWorkstoreCode("WKS1");
     			
	     		medOrder.setPatHospCode("QMH");
	     		medOrder.setPatient(patient);
	     		medOrder.setWorkstore(workstore);
	     		medOrder.addMedOrderItem(medOrderItem);
	     		medOrder.addMedOrderItem(medOrderItem2);
	     		
	     		medOrder.setMedOrderFmList(new ArrayList<MedOrderFm>());

				pmsService.receiveNewOrder(medOrder);
			}
		}.run();

		new ComponentTest() {
			protected void testComponents() throws Exception {
				OrderManagerLocal orderManager = (OrderManagerLocal) getValue("#{orderManager}");

				MedOrder medOrder = orderManager.retrieveMedOrder(orderNum);         	 
				assert medOrder.getOrderNum().equals(orderNum);
				assert medOrder.getMedOrderItemList().size() == 2;
				assert medOrder.getSpecCode().equals("MED");
				assert medOrder.getPatient().getHkid().equals("A1234563");
			}
		}.run();
	}

	@Test
	public void testPmsServiceModifyOrder() throws Exception {

		final String orderNum = "QMH1000002";
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
	        }
		}.run();       

		new ComponentTest() {
			protected void testComponents() throws Exception {

				PmsServiceLocal pmsService = (PmsServiceLocal) getValue("#{pmsService}");
				
        	 	MedOrder medOrder = new MedOrder();
           	 
        	 	medOrder.setId(Long.valueOf(2002));
	     		medOrder.setDocType(MedOrderDocType.Normal);
	     		medOrder.setOrderNum(orderNum);
     		
	     		Calendar orderDate = Calendar.getInstance();
	     		orderDate.clear();
	     		orderDate.set(2011, 0, 15);	
	     		medOrder.setOrderDate(orderDate.getTime());
             
	     		medOrder.setRefNum("0012");
	     		medOrder.setPrescType(MedOrderPrescType.Out);
	     		medOrder.setStatus(MedOrderStatus.Deleted);
     		
				MedOrderItem medOrderItem = new MedOrderItem(new OralRxDrug());		
				medOrderItem.setFirstDisplayName("ACET TESTING");
				medOrderItem.setBaseUnit("CAP");
				medOrderItem.setStatus(MedOrderItemStatus.Outstanding);
				medOrderItem.setItemNum(0);
				medOrderItem.setOrgItemNum(0);	
				medOrderItem.setUnitOfCharge(1);
				
				MedOrderItem medOrderItem2 = new MedOrderItem(new OralRxDrug());		
				medOrderItem2.setFirstDisplayName("PARA TESTING");
				medOrderItem2.setBaseUnit("CAP");
				medOrderItem2.setStatus(MedOrderItemStatus.Outstanding);
				medOrderItem2.setItemNum(0);
				medOrderItem2.setOrgItemNum(0);				
				medOrderItem.setUnitOfCharge(2);
				
				MedOrderItemAlert mOIA = new MedOrderItemAlert();
				mOIA.setAckUser("MO002");
				//mOIA.setAdditionInfo("Confirmed with drug-sensitivity test");
				medOrderItem2.addMedOrderItemAlert(mOIA);
				
				Patient patient = new Patient();
				patient.setId(Long.valueOf(1));
				patient.setHkid("A1234563");
				patient.setSex(Gender.Male);
				
				medOrder.setDoctorCode("MO002");				
				medOrder.setSpecCode("OTH");     		

	     		Workstore workstore = new Workstore();
	     		workstore.setHospCode("QMH");
	     		workstore.setWorkstoreCode("WKS1");
     			
	     		medOrder.setPatHospCode("QMH");
	     		medOrder.setPatient(patient);
	     		medOrder.setWorkstore(workstore);
	     		medOrder.addMedOrderItem(medOrderItem);
	     		medOrder.addMedOrderItem(medOrderItem2);

	     		medOrder.setMedOrderFmList(new ArrayList<MedOrderFm>());

				pmsService.receiveUpdateOrder(OpTrxType.OpDeleteMoeOrder, medOrder);
			}
		}.run();

		new ComponentTest() {
			protected void testComponents() throws Exception {
				OrderManagerLocal orderManager = (OrderManagerLocal) getValue("#{orderManager}");

				MedOrder medOrder = orderManager.retrieveMedOrder(orderNum);      
				
				assert medOrder.getOrderNum().equals(orderNum);
				assert medOrder.getMedOrderItemList().size() == 2;
				assert medOrder.getSpecCode().equals("OTH");
				assert medOrder.getPatient().getHkid().equals("A1234563");
				assert medOrder.getStatus().equals(MedOrderStatus.Outstanding);
			}
		}.run();
	}
}
