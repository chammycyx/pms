package hk.org.ha.model.pms.biz.ticketgen.test;

import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.biz.ticketgen.TicketServiceLocal;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.persistence.reftable.WorkstoreProp;
import hk.org.ha.model.pms.udt.charging.ClearanceStatus;
import hk.org.ha.model.pms.udt.ticketgen.TicketType;
import hk.org.ha.model.pms.vo.ticketgen.TicketGenResult;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class TicketServiceTest extends SeamTest {

	@Test
	public void testTicketGenStartComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		//retrieve workstationPropList for Ticket Generation
		new ComponentTest() 
		{
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{ticketService.retrieveWorkstorePropListforTicketGen}");
				List<WorkstoreProp> workStorePropList = (List<WorkstoreProp>)getValue("#{ticketWorkStorePropList}");				
				assert workStorePropList!=null;
				//assert workStorePropList.size() == 6; 
			}
		}.run();
		
	}
	
	@Test
	public void testTicketGenerationComponent() throws Exception {
	
		final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();		
		
		// Simple Ticket Generation
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{				
				//generate ticket for Public Patient
				TicketServiceLocal ticketService = (TicketServiceLocal) getValue("#{ticketService}");				
				ticketService.retrieveTicketByTicketType(TicketType.Queue1);
				Ticket ticket = (Ticket) getValue("#{ticket}");
				assert ticket.getTicketNum().equals("0501");
				assert df.format(ticket.getTicketDate()).equals(df.format(Calendar.getInstance().getTime()));
			}
		}.run();
		
		//Generate Ticket by MOEOrderNumber - DrugCharging Flag not enabled
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{				
				TicketServiceLocal ticketService = (TicketServiceLocal) getValue("#{ticketService}");				
				ticketService.retrieveTicketByOrderNumCaseNum("MOEQMH110000001", false, false, true, true);
				Ticket ticket = (Ticket) getValue("#{ticket}");		
				assert ticket == null;
				TicketGenResult ticketGenResult = (TicketGenResult) getValue("#{ticketGenResult}");
				assert ticketGenResult.getInfoMessageCode().equals("0027");				
			}
		}.run();
		
		//Generate Ticket by MOEOrderNumber - Clear
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{				
				TicketServiceLocal ticketService = (TicketServiceLocal) getValue("#{ticketService}");				
				ticketService.retrieveTicketByOrderNumCaseNum("MOEQMH110000001", false, true, true, true);
				Ticket ticket = (Ticket) getValue("#{ticket}");		
				assert ticket.getTicketNum().equals("0502");
				assert df.format(ticket.getTicketDate()).equals(df.format(Calendar.getInstance().getTime()));
				TicketGenResult ticketGenResult = (TicketGenResult) getValue("#{ticketGenResult}");
				assert ticketGenResult.getOrderNumforDisplay().equals("QMH110000001");
				assert ticketGenResult.getStdDrugClearance().equals(ClearanceStatus.PaymentClear);
				
				//Check Ticket Association
				invokeMethod("#{orderUtilService.retrieveMedOrderByOrderNum('QMH110000001')}");
				MedOrder medOrder = (MedOrder) getValue("#{medOrder}");
				assert medOrder.getTicket().getTicketNum().equals("0502");
				assert df.format(medOrder.getTicket().getTicketDate()).equals(df.format(Calendar.getInstance().getTime()));
			}
		}.run();
		
		
		//Generate Ticket by MOEOrderNumber - Not Clear
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{	
				TicketServiceLocal ticketService = (TicketServiceLocal) getValue("#{ticketService}");				
				ticketService.retrieveTicketByOrderNumCaseNum("MOEQMH110000002", false, true, true, true);
				Ticket ticket = (Ticket) getValue("#{ticket}");		
				assert ticket == null;
				TicketGenResult ticketGenResult = (TicketGenResult) getValue("#{ticketGenResult}");
				assert ticketGenResult.getStdDrugClearance().equals(ClearanceStatus.PaymentOutstanding);
				assert ticketGenResult.getInfoMessageCode().equals("0015");
				assert ticketGenResult.getOrderNumforDisplay().equals("QMH110000002");
				
				//Check Association
				invokeMethod("#{orderUtilService.retrieveMedOrderByOrderNum('QMH110000002')}");
				MedOrder medOrder = (MedOrder) getValue("#{medOrder}");
				assert medOrder.getTicket() == null;
			}
		}.run();
		
		//Generate Ticket by MOEOrderNumber - Not Clear but Force Print Ticket
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{				
				//generate ticket for Public Patient
				TicketServiceLocal ticketService = (TicketServiceLocal) getValue("#{ticketService}");				
				ticketService.retrieveTicketByOrderNumCaseNum("MOEQMH110000002", true, true, true, true);
				Ticket ticket = (Ticket) getValue("#{ticket}");		
				assert ticket.getTicketNum().equals("0503");
				assert df.format(ticket.getTicketDate()).equals(df.format(Calendar.getInstance().getTime()));
				TicketGenResult ticketGenResult = (TicketGenResult) getValue("#{ticketGenResult}");
				assert ticketGenResult.getStdDrugClearance().equals(ClearanceStatus.PaymentOutstanding);
				assert ticketGenResult.getOrderNumforDisplay().equals("QMH110000002");				
				
				//Check Ticket Association
				invokeMethod("#{orderUtilService.retrieveMedOrderByOrderNum('QMH110000002')}");
				MedOrder medOrder = (MedOrder) getValue("#{medOrder}");
				assert medOrder.getTicket().getTicketNum().equals("0503");
				assert df.format(medOrder.getTicket().getTicketDate()).equals(df.format(Calendar.getInstance().getTime()));
			}
		}.run();
		
		
		//Check Ticket Association
		
		//Generate Ticket by MOEOrderNumber - Outstanding MedOrder with Ticket Associated and ticketDate same as system Date - Reprint Ticket
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				TicketServiceLocal ticketService = (TicketServiceLocal) getValue("#{ticketService}");				
				ticketService.retrieveTicketByOrderNumCaseNum("MOEQMH110000002", false, true, true, true);
				Ticket ticket = (Ticket) getValue("#{ticket}");		
				assert ticket.getTicketNum().equals("0503");
				assert df.format(ticket.getTicketDate()).equals(df.format(Calendar.getInstance().getTime()));
				TicketGenResult ticketGenResult = (TicketGenResult) getValue("#{ticketGenResult}");				
				assert ticketGenResult.getStdDrugClearance() == null;
				assert ticketGenResult.getOrderNumforDisplay().equals("QMH110000002");
			}
		}.run();		
		
		//Generate Ticket by MOEOrderNumber - Outstanding MedOrder with Ticket Associated and ticketDate not the same as system Date - Associate New
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{orderUtilService.retrieveMedOrderByOrderNum('QMH110000003')}");
				MedOrder medOrder = (MedOrder) getValue("#{medOrder}");
				assert medOrder.getTicket() != null;
				assert medOrder.getTicket().getTicketNum().equals("0100");
				assert df.format( medOrder.getTicket().getTicketDate()).equals("2011-03-15");				
				
				TicketServiceLocal ticketService = (TicketServiceLocal) getValue("#{ticketService}");				
				ticketService.retrieveTicketByOrderNumCaseNum("MOEQMH110000003", true, true, true, true);
				Ticket ticket = (Ticket) getValue("#{ticket}");
				assert ticket.getTicketNum().equals("0504");
				assert df.format(ticket.getTicketDate()).equals(df.format(Calendar.getInstance().getTime()));
				TicketGenResult ticketGenResult = (TicketGenResult) getValue("#{ticketGenResult}");
				assert ticketGenResult.getStdDrugClearance().equals(ClearanceStatus.PaymentOutstanding);
				assert ticketGenResult.getOrderNumforDisplay().equals("QMH110000003");
				
				//Check Ticket Association
				invokeMethod("#{orderUtilService.retrieveMedOrderByOrderNum('QMH110000003')}");
				medOrder = (MedOrder) getValue("#{medOrder}");
				assert medOrder.getTicket().getTicketNum().equals("0504");
				assert df.format(medOrder.getTicket().getTicketDate()).equals(df.format(Calendar.getInstance().getTime()));				
			}
		}.run();		
				
		// Generate Ticket by CaseNum - No PMI Information
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{				
				TicketServiceLocal ticketService = (TicketServiceLocal) getValue("#{ticketService}");				
				ticketService.retrieveTicketByOrderNumCaseNum("SOPD00112233", false, true, true, true);
				Ticket ticket = (Ticket) getValue("#{ticket}");
				assert ticket == null;
				TicketGenResult ticketGenResult = (TicketGenResult) getValue("#{ticketGenResult}");
				assert ticketGenResult.getOrderNumforDisplay().equals("SOPD00112233");
				assert ticketGenResult.getInfoMessageCode().equals("0020");
			}
		}.run();
				
		// Generate Ticket by CaseNum - No OPAS Information
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{				
				TicketServiceLocal ticketService = (TicketServiceLocal) getValue("#{ticketService}");				
				ticketService.retrieveTicketByOrderNumCaseNum("SOPD0012223A", false, true, true, true);
				Ticket ticket = (Ticket) getValue("#{ticket}");
				assert ticket == null;
				TicketGenResult ticketGenResult = (TicketGenResult) getValue("#{ticketGenResult}");
				assert ticketGenResult.getOrderNumforDisplay().equals("SOPD0012223A");
				assert ticketGenResult.getInfoMessageCode().equals("0021");
			}
		}.run();
		
		
		// Generate Ticket by CaseNum - Multiple Appointments
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{				
				TicketServiceLocal ticketService = (TicketServiceLocal) getValue("#{ticketService}");				
				ticketService.retrieveTicketByOrderNumCaseNum("SOPD0012224B", false, true, true, true);
				Ticket ticket = (Ticket) getValue("#{ticket}");
				assert ticket == null;
				TicketGenResult ticketGenResult = (TicketGenResult) getValue("#{ticketGenResult}");
				assert ticketGenResult.getOrderNumforDisplay().equals("SOPD0012224B");
				assert ticketGenResult.getInfoMessageCode().equals("0022");
			}
		}.run();
				
		// Generate Ticket by CaseNum - Single Appointment without MedOrder
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{				
				TicketServiceLocal ticketService = (TicketServiceLocal) getValue("#{ticketService}");				
				ticketService.retrieveTicketByOrderNumCaseNum("SOPD0012335C", false, true, true, true);
				Ticket ticket = (Ticket) getValue("#{ticket}");
				assert ticket == null;
				TicketGenResult ticketGenResult = (TicketGenResult) getValue("#{ticketGenResult}");
				assert ticketGenResult.getOrderNumforDisplay().equals("SOPD0012335C");
				assert ticketGenResult.getInfoMessageCode().equals("0015");
				assert ticketGenResult.getStdDrugClearance().equals(ClearanceStatus.PaymentOutstanding);
			}
		}.run();
		
		// Generate Ticket by CaseNum - Single Appointment without MedOrder and payFlag = N 
		// Generate Ticket by CaseNum - Single Appointment with MedOrder				
		
		
		// Generate Ticket by CaseNum - HN Case - No PMI Information		
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{				
				TicketServiceLocal ticketService = (TicketServiceLocal) getValue("#{ticketService}");				
				ticketService.retrieveTicketByOrderNumCaseNum("HN11000006T", false, true, true, true);
				Ticket ticket = (Ticket) getValue("#{ticket}");
				assert ticket == null;
				TicketGenResult ticketGenResult = (TicketGenResult) getValue("#{ticketGenResult}");				
				assert ticketGenResult.getInfoMessageCode().equals("0020");
				assert ticketGenResult.getOrderNumforDisplay().equals("HN11000006T");
				assert ticketGenResult.getStdDrugClearance() == null;
			}
		}.run();
		
		// Generate Ticket by CaseNum - HN Case with PMI Information		
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{				
				TicketServiceLocal ticketService = (TicketServiceLocal) getValue("#{ticketService}");				
				ticketService.retrieveTicketByOrderNumCaseNum("HN11000007Z", false, true, true, true);
				Ticket ticket = (Ticket) getValue("#{ticket}");
				assert ticket.getTicketNum().equals("0505");
				assert df.format(ticket.getTicketDate()).equals(df.format(Calendar.getInstance().getTime()));
				TicketGenResult ticketGenResult = (TicketGenResult) getValue("#{ticketGenResult}");
				assert ticketGenResult.getOrderNumforDisplay().equals("HN11000007Z");
				assert ticketGenResult.getStdDrugClearance().equals(ClearanceStatus.PaymentClear);
			}
		}.run();
		
		// Reprint Ticket - Valid TicketNum
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{				
				TicketServiceLocal ticketService = (TicketServiceLocal) getValue("#{ticketService}");				
				ticketService.retrieveTicketByReprintTicketNum("0501");
				Ticket ticket = (Ticket) getValue("#{ticket}");
				assert ticket.getTicketNum().equals("0501");
				assert df.format(ticket.getTicketDate()).equals(df.format(Calendar.getInstance().getTime()));
			}
		}.run();
		
		// Reprint Ticket - Valid TicketNum with MedOrder Association and ticketDate same as system Date
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{	
				TicketServiceLocal ticketService = (TicketServiceLocal) getValue("#{ticketService}");				
				ticketService.retrieveTicketByReprintTicketNum("0502");
				Ticket ticket = (Ticket) getValue("#{ticket}");
				assert ticket.getTicketNum().equals("0502");
				assert df.format(ticket.getTicketDate()).equals(df.format(Calendar.getInstance().getTime()));
				TicketGenResult ticketGenResult = (TicketGenResult) getValue("#{ticketGenResult}");
				assert ticketGenResult.getOrderNumforDisplay().equals("QMH110000001");
			}
		}.run();
				
		// Reprint Ticket - Invalid TicketNum
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{				
				TicketServiceLocal ticketService = (TicketServiceLocal) getValue("#{ticketService}");				
				ticketService.retrieveTicketByReprintTicketNum("0511");
				Ticket ticket = (Ticket) getValue("#{ticket}");
				assert ticket == null;				
				TicketGenResult ticketGenResult = (TicketGenResult) getValue("#{ticketGenResult}");
				assert ticketGenResult.getInfoMessageCode().equals("0005");
			}
		}.run();
	}	

}