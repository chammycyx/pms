package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.reftable.RefillConfigServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.reftable.RefillConfig;
import hk.org.ha.model.pms.udt.YesNoCancelFlag;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class RefillConfigTest extends SeamTest {

	@Test
	public void testRefillConfigComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
//	            invokeMethod("#{postLogonService.postLogon}");
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		// retrieve Refill Config
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{refillConfigService.retrieveRefillConfig}");
				RefillConfig refillConfig = (RefillConfig) getValue("#{refillConfig}");
				
				assert refillConfig!=null;
			}
		}.run();
		
		//add Refill Config
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{refillConfigService.retrieveRefillConfig}");
				RefillConfig refillConfig = (RefillConfig) getValue("#{refillConfig}");
				
				assert refillConfig!=null;
				
				refillConfig.setInterval(20);
				refillConfig.setLastIntervalMaxDay(20);
				refillConfig.setLateRefillAlertDay(20);
				refillConfig.setDefaultLateRefillAlertBtn(YesNoCancelFlag.Yes);
				refillConfig.setEnableQtyAdjFlag(Boolean.TRUE);
				refillConfig.setAbortRefillDay(1);
				refillConfig.setManuRxNoPasSpecFlag(Boolean.TRUE);
				refillConfig.setEnablePharmClinicFlag(Boolean.FALSE);
				refillConfig.setPrintReminderFlag(Boolean.TRUE);
				refillConfig.setEnableSelectionFlag(Boolean.TRUE);
				refillConfig.setThreshold(0);
				
				RefillConfigServiceLocal refillConfigService = (RefillConfigServiceLocal)getValue("#{refillConfigService}");
				refillConfigService.updateRefillConfig(refillConfig);
				
			}
		}.run();
		
		//add Refill Config
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{refillConfigService.retrieveRefillConfig}");
				RefillConfig refillConfig = (RefillConfig) getValue("#{refillConfig}");
				
				assert refillConfig!=null;
				
				assert refillConfig.getInterval().equals(20);
				assert refillConfig.getLastIntervalMaxDay().equals(20);
				assert refillConfig.getLateRefillAlertDay().equals(20);
				assert refillConfig.getDefaultLateRefillAlertBtn().equals(YesNoCancelFlag.Yes);
				assert refillConfig.getEnableQtyAdjFlag();
				assert refillConfig.getAbortRefillDay().equals(1);
				assert refillConfig.getManuRxNoPasSpecFlag();
				assert !refillConfig.getEnablePharmClinicFlag();
				assert refillConfig.getPrintReminderFlag();
				assert refillConfig.getEnableSelectionFlag();
				assert refillConfig.getThreshold().equals(0);
				
			}
		}.run();
		
		//update Refill Config
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{refillConfigService.retrieveRefillConfig}");
				RefillConfig refillConfig = (RefillConfig) getValue("#{refillConfig}");
				
				assert refillConfig!=null;
				
				refillConfig.setInterval(10);
				refillConfig.setLastIntervalMaxDay(10);
				refillConfig.setLateRefillAlertDay(10);
				refillConfig.setDefaultLateRefillAlertBtn(YesNoCancelFlag.Cancel);
				refillConfig.setEnableQtyAdjFlag(Boolean.FALSE);
				refillConfig.setAbortRefillDay(0);
				refillConfig.setManuRxNoPasSpecFlag(Boolean.FALSE);
				refillConfig.setEnablePharmClinicFlag(Boolean.TRUE);
				refillConfig.setPrintReminderFlag(Boolean.FALSE);
				refillConfig.setEnableSelectionFlag(Boolean.FALSE);
				refillConfig.setThreshold(1);
				
				RefillConfigServiceLocal refillConfigService = (RefillConfigServiceLocal)getValue("#{refillConfigService}");
				refillConfigService.updateRefillConfig(refillConfig);
			}
		}.run();
		
		//check Refill Config
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				invokeMethod("#{refillConfigService.retrieveRefillConfig}");
				RefillConfig refillConfig = (RefillConfig) getValue("#{refillConfig}");
				
				assert refillConfig!=null;
				
				assert refillConfig.getInterval().equals(10);
				assert refillConfig.getLastIntervalMaxDay().equals(10);
				assert refillConfig.getLateRefillAlertDay().equals(10);
				assert refillConfig.getDefaultLateRefillAlertBtn().equals(YesNoCancelFlag.Cancel);
				assert !refillConfig.getEnableQtyAdjFlag();
				assert refillConfig.getAbortRefillDay().equals(0);
				assert !refillConfig.getManuRxNoPasSpecFlag();
				assert refillConfig.getEnablePharmClinicFlag();
				assert !refillConfig.getPrintReminderFlag();
				assert !refillConfig.getEnableSelectionFlag();
				assert refillConfig.getThreshold().equals(1);
			}
		}.run();
	}	
}
