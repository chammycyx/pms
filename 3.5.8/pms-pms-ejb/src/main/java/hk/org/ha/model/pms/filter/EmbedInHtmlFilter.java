package hk.org.ha.model.pms.filter;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet Filter implementation class ImageHtmlFilter
 */
public class EmbedInHtmlFilter implements Filter {
	
	private static final String EMBEDDED = "embedded";
	
	private static final String[] REQUIRED_ZOOM100_REPORT = new String[] {
		"RefillItemRpt",
		"RefillWorkloadForecastRpt",
		"RefillQtyForecastRpt",
		"RefillStatRpt",
		"RefillExpiryRpt",
		"PharmClinicRefillCouponRpt",
		"OutstandSfiPaymentRpt",
		"SfiHandleChargeExemptionRpt"};
	
	public void init(FilterConfig fConfig) throws ServletException {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		String emdedded = request.getParameter(EMBEDDED);
		
		if (emdedded != null) {		
			// if already embededd
			chain.doFilter(request, response);
		} else {
			// otherwise wrap as html
			HttpServletRequest req = (HttpServletRequest) request;
			HttpServletResponse resp = (HttpServletResponse) response;
			
			resp.setContentType("text/html");
			PrintWriter out = resp.getWriter();
			String requestURI = req.getRequestURI();
			String url = requestURI + "?" + req.getQueryString() + "&" + EMBEDDED;
			out.print(
				"<!DOCTYPE html>\n" +
				"<html>\n" +
				"<head>\n" +
				"   <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
				"   <title>Preview</title>\n" +
				"	<style type=\"text/css\">\n" +
				"		html, body { height:100%; }\n" +
				"		body { margin:0; padding:0; border:0; }\n" +
				"       object, embed { width:100%; height:100%; border:0; }\n" +
				"	</style>\n" +
				"</head>\n" +
				"<body oncontextmenu=\"return false\">\n");
			if (requestURI.contains(".png")) {
			    out.println(
					" <img src=\"" + url + "\">\n");
			} else if (requestURI.contains(".pdf")) {
				int zoom = -1;								
				for (String reportString : REQUIRED_ZOOM100_REPORT) {
					if (requestURI.contains(reportString)) {
						zoom = 100;
						break;
					}
				}				
				String urlWithPdfParam = url + "#toolbar=0&navpanes=0" + (zoom == -1 ? "" : "&zoom=" + zoom);				
				out.println(
					"	<object id=\"app\" data=\"" + urlWithPdfParam + "\" type=\"application/pdf\">\n" +
					"		<embed src=\"" + urlWithPdfParam + "\" type=\"application/pdf\" />\n" +
					"	</object>\n");
			} else {
				throw new IllegalArgumentException("unsupported document type! url:" + url);
			}
			out.print(
				"</body>\n" +
				"</html>");
		    out.flush();
		}
	}

	public void destroy() {
	}
}
