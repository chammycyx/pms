package hk.org.ha.model.pms.persistence.reftable;

import flexjson.JSON;
import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Customizer;
import org.eclipse.persistence.annotations.PrivateOwned;

@Entity
@Table(name = "WARD_GROUP")
@Customizer(AuditCustomizer.class)
public class WardGroup extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wardGroupSeq")
	@SequenceGenerator(name = "wardGroupSeq", sequenceName = "SQ_WARD_GROUP", initialValue = 100000000)
	private Long id;

	@Column(name = "WARD_GROUP_CODE", nullable = false, length = 4)
	private String wardGroupCode;
	
	@ManyToOne
	@JoinColumn(name = "WORKSTORE_GROUP_CODE", nullable = false)
	private WorkstoreGroup workstoreGroup;
    
    @PrivateOwned
	@OneToMany(mappedBy = "wardGroup")
	@OrderBy("wardCode")
	private List<WardGroupItem> wardGroupItemList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWardGroupCode() {
		return wardGroupCode;
	}

	public void setWardGroupCode(String wardGroupCode) {
		this.wardGroupCode = wardGroupCode;
	}

	public WorkstoreGroup getWorkstoreGroup() {
		return workstoreGroup;
	}

	public void setWorkstoreGroup(WorkstoreGroup workstoreGroup) {
		this.workstoreGroup = workstoreGroup;
	}

	@JSON
	public List<WardGroupItem> getWardGroupItemList() {
		return wardGroupItemList;
	}

	public void setWardGroupItemList(List<WardGroupItem> wardGroupItemList) {
		this.wardGroupItemList = wardGroupItemList;
	}

}
