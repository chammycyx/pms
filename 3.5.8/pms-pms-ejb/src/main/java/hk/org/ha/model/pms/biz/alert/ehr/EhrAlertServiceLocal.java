package hk.org.ha.model.pms.biz.alert.ehr;

import hk.org.ha.model.pms.asa.exception.alert.ehr.EhrAllergyException;
import hk.org.ha.model.pms.vo.alert.ehr.EhrAllergyDetail;

import javax.ejb.Local;

@Local
public interface EhrAlertServiceLocal {
	
	EhrAllergyDetail retrieveEhrAllergyDetail(String patHospCode, String ehrNum) throws EhrAllergyException;
	
	void destroy();

}
