package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class EdsPerfRpt implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private EdsPerfRptHdr edsPerfRptHdr;	
	private EdsPerfRptDtl edsPerfRptDtl;
	public EdsPerfRptHdr getEdsPerfRptHdr() {
		return edsPerfRptHdr;
	}
	public void setEdsPerfRptHdr(EdsPerfRptHdr edsPerfRptHdr) {
		this.edsPerfRptHdr = edsPerfRptHdr;
	}
	public EdsPerfRptDtl getEdsPerfRptDtl() {
		return edsPerfRptDtl;
	}
	public void setEdsPerfRptDtl(EdsPerfRptDtl edsPerfRptDtl) {
		this.edsPerfRptDtl = edsPerfRptDtl;
	}	
}
