package hk.org.ha.model.pms.udt.mr;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum MrType implements StringValuedEnum {
	
	Annotation("A", "Annotation"),
	Discontinue("D", "Discontinue");

	private final String dataValue;
	private final String displayValue;

	MrType(final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}
	
	public static MrType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MrType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<MrType> {

		private static final long serialVersionUID = 1L;

		public Class<MrType> getEnumClass() {
    		return MrType.class;
    	}
    }
}
