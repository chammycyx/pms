package hk.org.ha.model.pms.biz.alert.mds;

import hk.org.ha.model.pms.persistence.AlertEntity;

import java.util.Collection;
import java.util.List;

import javax.ejb.Local;

@Local
public interface AlertComparatorInf {

	List<AlertEntity> constructOutStandingAlertList(Collection<? extends AlertEntity> currentAlertList, Collection<? extends AlertEntity> prevAlertList, boolean allowSubSetPatAlertFlag);
	
	boolean isAlertListEqual(Collection<? extends AlertEntity> alertList1, Collection<? extends AlertEntity> alertList2);
	
	boolean copyAlertOverride(boolean medProfileAlertHlaEnabled, Collection<? extends AlertEntity> mainList, Collection<? extends AlertEntity> subList);
}
