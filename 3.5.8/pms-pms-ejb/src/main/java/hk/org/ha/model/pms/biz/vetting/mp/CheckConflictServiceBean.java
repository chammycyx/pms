package hk.org.ha.model.pms.biz.vetting.mp;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.MsWorkstoreDrug;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileOrder;
import hk.org.ha.model.pms.udt.medprofile.MedProfileOrderStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileOrderType;
import hk.org.ha.model.pms.vo.medprofile.CheckConflictItem;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("checkConflictService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CheckConflictServiceBean implements CheckConflictServiceLocal {

	private static final String MED_PROFILE_ORDER_QUERY_HINT_BATCH = "o.medProfileMoItemList";
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private Workstore workstore;

	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@Out(required = false)
	protected List<Long> conflictMedProfileItemIdList;
	
	@Out(required = false)
	protected List<Long> suspendReplenishmentMedProfileItemIdList;
	
    public CheckConflictServiceBean() {
    }
    
	public void checkConflictExists(String orderNum, List<CheckConflictItem> medProfileCheckConflictItemList,
    		List<CheckConflictItem> replenishmentCheckConflictItemList) {
    	
		List<MedProfileOrder> medProfileOrderList = retrievePendingMedProfileOrders(orderNum);
    	Set<Long> conflictMedProfileItemIdSet = new LinkedHashSet<Long>(); 
    	suspendReplenishmentMedProfileItemIdList = new ArrayList<Long>();
		
		Date now = new Date();
		
		for (CheckConflictItem checkConflictItem: medProfileCheckConflictItemList) {
			if (checkModificationExists(medProfileOrderList, checkConflictItem) || checkTreatmentEnds(now, checkConflictItem)) {
				conflictMedProfileItemIdSet.add(checkConflictItem.getMedProfileItemId());
			}
		}
		
		for (CheckConflictItem checkConflictItem: replenishmentCheckConflictItemList) {

			if (checkModificationExists(medProfileOrderList, checkConflictItem) || checkTreatmentEnds(now, checkConflictItem)) {
				conflictMedProfileItemIdSet.add(checkConflictItem.getMedProfileItemId());
				continue;
			}
			
			for (String itemCode: checkConflictItem.getItemCodeList()) {
				MsWorkstoreDrug msWorkstoreDrug = dmDrugCacher.getMsWorkstoreDrug(workstore, itemCode);
				if (msWorkstoreDrug == null || "Y".equalsIgnoreCase(msWorkstoreDrug.getSuspend())) {
					suspendReplenishmentMedProfileItemIdList.add(checkConflictItem.getMedProfileItemId());
					break;
				}
			}
		}
		conflictMedProfileItemIdList = new ArrayList<Long>(conflictMedProfileItemIdSet);
    }
    
	private boolean checkModificationExists(List<MedProfileOrder> medProfileOrderList, CheckConflictItem checkConflictItem) {
		
		for (MedProfileOrder medProfileOrder: medProfileOrderList) {
			for (MedProfileMoItem pendingMedProfileMoItem: medProfileOrder.getMedProfileMoItemList()) {
				if ((pendingMedProfileMoItem.getOrgItemNum() != null && 
					pendingMedProfileMoItem.getOrgItemNum().equals(checkConflictItem.getOrgItemNum())) ||
					(pendingMedProfileMoItem.getItemNum() != null &&
					pendingMedProfileMoItem.getItemNum().equals(checkConflictItem.getItemNum()))) {
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean checkTreatmentEnds(Date date, CheckConflictItem checkConflictItem) {
	
		return checkConflictItem.getEndDate() != null && 
				checkConflictItem.getEndDate().compareTo(date) < 0;
	}
	
	
	@SuppressWarnings("unchecked")
	private List<MedProfileOrder> retrievePendingMedProfileOrders(String orderNum) {
		
		List<MedProfileOrder> medProfileOrderList = em.createQuery(
				"select o from MedProfileOrder o" + // 20120306 index check : MedProfileOrder.orderNum : FK_MED_PROFILE_ORDER_02
				" where o.orderNum = :orderNum" +
				" and o.status = :pendingStatus" +
				" and o.type in :orderTypeList" +
				" order by o.createDate, o.id")
				.setParameter("orderNum", orderNum)
				.setParameter("pendingStatus", MedProfileOrderStatus.Pending)
				.setParameter("orderTypeList", MedProfileOrderType.Modify_End_Delete_Discontinue_CompleteAdmin)
				.setHint(QueryHints.BATCH, MED_PROFILE_ORDER_QUERY_HINT_BATCH)
				.getResultList();
		
		return medProfileOrderList;
	}
	
    @Remove
	@Override
	public void destroy() {
    	conflictMedProfileItemIdList = null;
    	suspendReplenishmentMedProfileItemIdList = null;
	}
}
