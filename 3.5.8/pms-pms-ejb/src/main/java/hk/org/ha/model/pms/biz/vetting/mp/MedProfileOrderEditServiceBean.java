package hk.org.ha.model.pms.biz.vetting.mp;

import static hk.org.ha.model.pms.prop.Prop.VETTING_MEDPROFILE_PIVAS_NEWITEM_PREPARATIONTIME;
import hk.org.ha.fmk.pms.remote.marshall.EclipseLinkXStreamMarshaller;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.drug.DrugSearchMpManagerLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileItemConverterLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileManagerLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.biz.pivas.PivasWorklistManagerLocal;
import hk.org.ha.model.pms.biz.reftable.ChargeSpecialtyManagerLocal;
import hk.org.ha.model.pms.biz.reftable.MpWardManagerLocal;
import hk.org.ha.model.pms.biz.reftable.mp.AomScheduleManagerLocal;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacher;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasWard;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItemAlert;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItemFm;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.persistence.medprofile.Replenishment;
import hk.org.ha.model.pms.persistence.phs.WardStock;
import hk.org.ha.model.pms.persistence.pivas.PivasWorklist;
import hk.org.ha.model.pms.persistence.reftable.AomSchedule;
import hk.org.ha.model.pms.persistence.reftable.pivas.PivasServiceHour;
import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
import hk.org.ha.model.pms.udt.reftable.ChargeSpecialtyType;
import hk.org.ha.model.pms.udt.reftable.mp.AomScheduleType;
import hk.org.ha.model.pms.vo.chemo.ChemoInfo;
import hk.org.ha.model.pms.vo.chemo.ChemoItem;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.model.pms.vo.pivas.worklist.PivasItemEditInfo;
import hk.org.ha.model.pms.vo.pivas.worklist.PivasSupplyDateListInfo;
import hk.org.ha.model.pms.vo.rx.CapdItem;
import hk.org.ha.model.pms.vo.rx.CapdRxDrug;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.IvRxDrug;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;
import org.springframework.beans.BeanUtils;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("medProfileOrderEditService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MedProfileOrderEditServiceBean implements MedProfileOrderEditServiceLocal {
	
	@Logger
	private Log logger;
	
	@Out(required = false)
	private MedProfileItem medProfileItem;
	
	@In
	private MedProfileManagerLocal medProfileManager;
	
	@In
	private MedProfileItemConverterLocal medProfileItemConverter; 
	
	@In
	private DrugSearchMpManagerLocal drugSearchMpManager;
	
	@In
	private AomScheduleManagerLocal aomScheduleManager;
		
	@In
	private MpWardManagerLocal mpWardManager;
	
	@In
	private ChargeSpecialtyManagerLocal chargeSpecialtyManager;
	
	@In
	private Workstore workstore;
	
	@In
	private PivasWorklistManagerLocal pivasWorklistManager;
		
	@In
	private List<Integer> atdpsPickStationsNumList;
	
	@In
	private List<PivasWorklist> pivasWorklistList;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	private void relinkMedProfileItem(MedProfile medProfileInMemory, MedProfileItem medProfileItemInMemory) {
		medProfileItemInMemory.setMedProfile(medProfileInMemory);		
		medProfileItemInMemory.getMedProfileMoItem().setMedProfileItem(medProfileItemInMemory);		
		
		for ( MedProfileMoItemAlert alert : medProfileItemInMemory.getMedProfileMoItem().getMedProfileMoItemAlertList() ) {
			alert.setMedProfileMoItem(medProfileItemInMemory.getMedProfileMoItem());
		}
		for ( MedProfileMoItemFm fm : medProfileItemInMemory.getMedProfileMoItem().getMedProfileMoItemFmList() ) {
			fm.setMedProfileMoItem(medProfileItemInMemory.getMedProfileMoItem());
		}
		for ( MedProfilePoItem poi : medProfileItemInMemory.getMedProfileMoItem().getMedProfilePoItemList() ) {
			poi.setMedProfileMoItem(medProfileItemInMemory.getMedProfileMoItem());
		}	
	}
	
	private int getItemIndex(List<MedProfileItem> medProfileItemList, Long id, Integer orgItemNum) {
		int index = 0;
		if (id != null) {
			for (MedProfileItem mpi:medProfileItemList) {
				if (id.equals(mpi.getId())) {
					return index;
				}
				++index;
			}
		}
		index = 0;
		for (MedProfileItem mpi:medProfileItemList) {
			if (mpi.getOrgItemNum().equals(orgItemNum)) {
				return index;
			}
			++index;
		}
		return -1;
	}
	
	@SuppressWarnings("unchecked")
	public void cancelFromOrderEdit(Long id, Integer orgItemNum) {
		logger.debug("cancelFromOrderEdit id:#0, orgItemNum:#1", id, orgItemNum);
		List<MedProfileItem> medProfileItemList = (List<MedProfileItem>) Contexts.getSessionContext().get("medProfileItemList");
		
		int index = getItemIndex(medProfileItemList, id, orgItemNum);
		
		// PMS-3378
		if( index >= 0 && index < medProfileItemList.size() ) {
			
			medProfileItem = medProfileItemList.get(index);			
			medProfileItem.setMedProfile(null);
			medProfileItem.getMedProfileMoItem().setMedProfileItem(null);
			
			if (medProfileItem.getMedProfileErrorLog() != null) {
				medProfileItem.getMedProfileErrorLog().setMedProfileItem(null);
			}
			for ( MedProfileMoItemAlert alert : medProfileItem.getMedProfileMoItem().getMedProfileMoItemAlertList() ) {
				alert.setMedProfileMoItem(null);
			}
			for ( MedProfileMoItemFm fm : medProfileItem.getMedProfileMoItem().getMedProfileMoItemFmList() ) {
				fm.setMedProfileMoItem(null);
			}
			for ( MedProfilePoItem poi : medProfileItem.getMedProfileMoItem().getMedProfilePoItemList() ) {
				poi.setMedProfileMoItem(null);
			}			
		} else {
			medProfileItem = null;			
		}
		if (logger.isDebugEnabled()) {
			logger.debug((new EclipseLinkXStreamMarshaller()).toXML(medProfileItem));
		}
	}
	
	@SuppressWarnings("unchecked")
	public void saveItemFromOrderEdit(MedProfileItem medProfileItemIn) {
		logger.info("saveItemFromOrderEdit id:#0, orgItemNum:#1", medProfileItemIn.getId(), medProfileItemIn.getOrgItemNum());
		medProfileItem = null;
		
		List<MedProfileItem> medProfileItemList = (List<MedProfileItem>) Contexts.getSessionContext().get("medProfileItemList");		
		MedProfile medProfileInMemory = (MedProfile)Contexts.getSessionContext().get("medProfile");
			
		int index = getItemIndex(medProfileItemList, medProfileItemIn.getId(), medProfileItemIn.getOrgItemNum());
		if (index == -1) {
			medProfileItemList.add(medProfileItemIn);
		} else {
			medProfileItemList.set(index, medProfileItemIn);
		}
		
		relinkMedProfileItem(medProfileInMemory,medProfileItemIn);		
	}
	
	@SuppressWarnings("unchecked")
	public Replenishment retrieveReplenishmentFromBackendContext(Long id) {
		logger.debug("retrieveReplenishmentFromItemEdit #0", id);
		
		List<Replenishment> replenishmentList = (List<Replenishment>) Contexts.getSessionContext().get("replenishmentList");	
		for (Replenishment r:replenishmentList) {
			if (r.getId().equals(id)) {
				return r;
			}
		}
		
		return null;
	}
		
	@SuppressWarnings("unchecked")
	public List<MedProfileItem> cancelFromManualEntryItemEdit(List<Integer> itemNumList) {
		
		logger.debug("cancelFromManualEntryItemEdit #0", itemNumList.toArray());
		List<MedProfileItem> medProfileItemList = (List<MedProfileItem>) Contexts.getSessionContext().get("medProfileItemList");
		
		Map<Integer, MedProfileItem> mpiHashMap = new HashMap<Integer, MedProfileItem>();
		for (MedProfileItem mpi : medProfileItemList) {
			mpiHashMap.put(mpi.getOrgItemNum(), mpi);
		}
		
		List<MedProfileItem> returnList = new ArrayList<MedProfileItem>();
		
		for (Integer itemNum:itemNumList) {
			if (mpiHashMap.get(itemNum) != null) {
				returnList.add(mpiHashMap.get(itemNum));
			}
		}
		
		if (logger.isDebugEnabled()) {
			logger.debug((new EclipseLinkXStreamMarshaller()).toXML(returnList));
		}
		return returnList;
	}
	
	@SuppressWarnings("unchecked")
	public List<MedProfileItem> saveItemFromManualEntryItemEdit(List<MedProfileItem> mpiList) {
		List<MedProfileItem> medProfileItemList = (List<MedProfileItem>) Contexts.getSessionContext().get("medProfileItemList");		
		MedProfile medProfileInMemory = (MedProfile)Contexts.getSessionContext().get("medProfile");
		
		logger.debug("Size of mpiList: #0, medProfileItemList: #1", mpiList.size(), medProfileItemList.size());
		
		Map<Integer, MedProfileItem> mpiHashMap = new HashMap<Integer, MedProfileItem>();
		for (MedProfileItem mpi : medProfileItemList) {
			mpiHashMap.put(mpi.getOrgItemNum(), mpi);
		}		
		
		List<MedProfileItem> moveToInactiveList = new ArrayList<MedProfileItem>();
		List<MedProfileItem> deleteItemList = new ArrayList<MedProfileItem>();
		
		for (MedProfileItem mpi : mpiList) {
			MedProfileItem mpiInMemory;
			if (BooleanUtils.isTrue(mpi.getDiscontModified())) {
				mpiInMemory = mpiHashMap.get(mpi.getOrgItemNum());
				moveToInactiveList.add(mpiInMemory);
				medProfileItemList.add(mpi);
				
				mpi.setDiscontModified(Boolean.FALSE);
				mpi.setOrgItemNum(medProfileInMemory.getAndIncreaseMaxItemNum());
				mpi.getMedProfileMoItem().setItemNum(mpi.getOrgItemNum());
				
				mpiInMemory.setNewOrgItemNum(mpi.getOrgItemNum());
			} else if( mpi.getId() == null && mpi.getStatus() == MedProfileItemStatus.Deleted ) {
				mpiInMemory = mpiHashMap.get(mpi.getOrgItemNum());
				if( mpiInMemory != null ){
					deleteItemList.add(mpiInMemory);
				}
				continue;
			} else {
				mpiInMemory = mpiHashMap.get(mpi.getOrgItemNum());
				if (mpiInMemory == null) {
					medProfileItemList.add(mpi);
				} else {
					medProfileItemList.set(medProfileItemList.indexOf(mpiInMemory), mpi);
				}
			}
			relinkMedProfileItem(medProfileInMemory, mpi);
		}
		
		logger.debug("Size of deleteItemList is #0", deleteItemList.size(), medProfileItemList.size());
		medProfileItemList.removeAll(deleteItemList);
		
		logger.debug("Size of medProfileItemList is #0", medProfileItemList.size());
		
		return moveToInactiveList;
	}
	
	@SuppressWarnings("unchecked")
	public MedProfile updateMedProfile(MedProfile medProfileIn, boolean patientInfoUpdated) {
		logger.debug("updateMedProfile - id | #0", medProfileIn.getId());
		medProfileItem = null;
		MedProfile result = null;
		
		MedProfile medProfileInMemory = (MedProfile) Contexts.getSessionContext().get("medProfile");
		medProfileIn.setMaxItemNum(medProfileInMemory.getMaxItemNum());
		medProfileInMemory.setPrintLang(medProfileIn.getPrintLang());
		List<MedProfileItem> medProfileItemList = (List<MedProfileItem>) Contexts.getSessionContext().get("medProfileItemList");
		if (patientInfoUpdated && medProfileManager.updateSpecCodeAndWardCodeRelatedSetting(workstore, medProfileInMemory.getSpecCode(),
				medProfileInMemory.getWardCode(), false, medProfileIn, medProfileItemList)) {
			markItemModified(medProfileItemList);
			result = constructUpdateMedProfileResult(medProfileIn, medProfileItemList);
		}
		Contexts.getSessionContext().set("medProfile", medProfileIn);
		return result;
	}
	
	private void markItemModified(List<MedProfileItem> medProfileItemList) {
		
		for (MedProfileItem medProfileItem : medProfileItemList) {
			medProfileItem.setModified(true);
		}
	}
	
	private MedProfile constructUpdateMedProfileResult(MedProfile medProfile, List<MedProfileItem> medProfileItemList) {
		
		MedProfile result = new MedProfile();
		result.setPrintLang(medProfile.getPrintLang());
		
		for (MedProfileItem medProfileItem : medProfileItemList) {
			
			MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
			
			MedProfileItem resultItem = new MedProfileItem();
			result.addMedProfileItem(resultItem);
			
			MedProfileMoItem resultMoItem = new MedProfileMoItem();
			resultItem.setMedProfileMoItem(resultMoItem);
			
			resultMoItem.setItemNum(medProfileMoItem.getItemNum());
			resultMoItem.setPrintInstructionFlag(medProfileMoItem.getPrintInstructionFlag());
			
			for (MedProfilePoItem medProfilePoItem : medProfileItem.getMedProfileMoItem().getMedProfilePoItemList()) {
				MedProfilePoItem resultPoItem = new MedProfilePoItem();
				resultMoItem.addMedProfilePoItem(resultPoItem);
				resultPoItem.setWardStockFlag(medProfilePoItem.getWardStockFlag());
				resultPoItem.setDueDate(medProfilePoItem.getDueDate());
				resultPoItem.setReDispFlag(medProfilePoItem.getReDispFlag());
				resultPoItem.setChargeSpecCode(medProfilePoItem.getChargeSpecCode());
			}
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public void updateManualEntryVettingAssociation(Map<String, Integer> map) {
		List<MedProfileItem> medProfileItemList = (List<MedProfileItem>) Contexts.getSessionContext().get("medProfileItemList");
		Integer groupNum;
		for (MedProfileItem mpi:medProfileItemList) {
			groupNum = (Integer)map.get(mpi.getOrgItemNum().toString());
			if (groupNum != null) {
				if (groupNum.intValue() != -1) {
					mpi.setGroupNum(groupNum);	
				} else {
					mpi.setGroupNum(null);
				}
				mpi.setModified(Boolean.TRUE);
			}
		}
	}
	
	public MedProfileItem reactivateDiscontinuedItem(MedProfileItem mpi) {

		MedProfile medProfileInMemory = (MedProfile)Contexts.getSessionContext().get("medProfile");
		MedProfileMoItem mpmi = mpi.getMedProfileMoItem();
		MedProfilePoItem mppi = mpmi.getMedProfilePoItemList().get(0);	

		Map<String, WardStock> wardStockMap = mpWardManager.retrieveWardStockMap(
				workstore.getWorkstoreGroup().getInstitution().getInstCode(),
				medProfileInMemory.getWardCode());
		
		Map<ChargeSpecialtyType, String> chargeSpecCodeMap = chargeSpecialtyManager.retrieveChargeSpecialtyCodeMap(
				workstore, medProfileInMemory.getSpecCode());
		
		mpi.setId(null);
		mpi.setCreateDate(new Date());//for front end vetting list sorting
		mpi.setCreateUser(null);
		mpi.setUpdateDate(null);
		mpi.setUpdateUser(null);	
		mpi.setVersion(null);
		
		mpi.setOrgItemNum(medProfileInMemory.getAndIncreaseMaxItemNum());
		mpi.setGroupNum(null);
		mpi.setOrgStatus(null);
		mpi.setStatus(MedProfileItemStatus.Unvet);
		mpi.setStatusUpdateDate(null);
		mpi.setVerifyDate(null);
		mpi.setMedProfileErrorLog(null);		
		
		mpmi.setId(null);
		mpmi.setCreateDate(null);
		mpmi.setCreateUser(null);
		mpmi.setUpdateDate(null);
		mpmi.setUpdateUser(null);
		mpmi.setVersion(null);
		
		mpmi.setOrgItemNum(mpi.getOrgItemNum());
		mpmi.setItemNum(mpi.getOrgItemNum());		
		mpmi.setEndDate(null);
		mpmi.setMedProfileMoItemFmList(new ArrayList<MedProfileMoItemFm>());
		mpmi.setMedProfileMoItemAlertList(new ArrayList<MedProfileMoItemAlert>());
		mpmi.setAlertFlag(null);
		mpmi.setMdsAckSuspendReason(null);
		mpmi.setMdsSuspendReason(null);		
		mpmi.setFirstPrintDate(null);
		mpmi.setLastAlertDate(null);
		mpmi.setVerifyBeforeFlag(Boolean.FALSE);
		mpmi.setPivasFlag(Boolean.FALSE);
		mpmi.setUrgentFlag(null);		
		mpmi.setSuspendCode(null);		
		
		mppi.setId(null);
		mppi.setCreateDate(null);
		mppi.setCreateUser(null);
		mppi.setUpdateDate(null);
		mppi.setUpdateUser(null);
		mppi.setVersion(null);
		
		mppi.setLastDueDate(null);
		mppi.setLastPrintDate(null);

		AomScheduleType aomScheduleType = Boolean.TRUE.equals(mpmi.getPivasFlag()) ? AomScheduleType.Pivas : AomScheduleType.General;
		mppi.setDueDate(aomScheduleManager.getNextAdminDueDate(mppi.getRegimen().getDailyFreqCodeList(), aomScheduleType));
		
		mppi.setLastAdjQty(null);
		mppi.setAdjQty(null);
		mppi.setFirstDueDate(null);
		mppi.setWardStockFlag(wardStockMap.containsKey(mppi.getItemCode()));
		mppi.updateChargeSpecCode(chargeSpecCodeMap, medProfileInMemory.getSpecCode());					
		
		mpi.setMedProfile(null);
		mpi.setMedProfileMoItem(mpmi);
		mpmi.setMedProfileItem(mpi);
		mpmi.setMedProfilePoItemList(Arrays.asList(mppi));
		mppi.setMedProfileMoItem(mpmi);
		
		drugSearchMpManager.addMedProfileMoItemFm(medProfileInMemory, mpmi, Arrays.asList(mpmi));
		
		return mpi;
	}
	
	private WorkstoreDrug findWorkstoreDrugFromDrugList(List<WorkstoreDrug> drugList, String itemCode)
	{
		for ( WorkstoreDrug drug : drugList ) {
			if ( drug.getDmDrug().getItemCode().equals(itemCode) ) {
				if ( Integer.valueOf(200).compareTo(drug.getMsWorkstoreDrug().getDrugScope()) > 0 ) {
					return null;
				} else {
					drug.getDmDrug().getFullDrugDesc();
					drug.getDmDrug().getVolumeText();
					return drug;
				}
			}
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public MedProfileMoItem changeItemCodeFromManualEntryItemEdit(String itemCode, String displayName) {
		Workstore workstore = (Workstore)Contexts.getSessionContext().get("workstore");
		List<WorkstoreDrug> drugList = WorkstoreDrugCacher.instance().getWorkstoreDrugListByDisplayName(workstore, displayName);
		WorkstoreDrug drug = findWorkstoreDrugFromDrugList(drugList, itemCode);		
		if (drug != null && !"Y".equals(drug.getMsWorkstoreDrug().getSuspend())) {
			MedProfile medProfileInMemory = (MedProfile)Contexts.getSessionContext().get("medProfile");
			List<Integer> atdpsPickStationsNumList = (List<Integer>)Contexts.getSessionContext().get("atdpsPickStationsNumList");
			return drugSearchMpManager.createMedProfileMoItemByItemCode(itemCode, medProfileInMemory, atdpsPickStationsNumList, true);
		} else {
			return null;
		}
	}
	
	public Date retrieveNextAdminDueDate(List<String> dailyFreqCodeList, Boolean pivasFlag) {
		AomScheduleType aomScheduleType = Boolean.TRUE.equals(pivasFlag) ? AomScheduleType.Pivas : AomScheduleType.General;
		return aomScheduleManager.getNextAdminDueDate(dailyFreqCodeList, aomScheduleType);				
	}
	
	public void updateBackendMedProfileItemList(List<MedProfileItem> mpiList) {
		saveItemFromManualEntryItemEdit(mpiList);
	}
	
	public Object[] calculatePivasDueDateAndSupplyDate(String dailyFreqCode, Date dueDate) {
		
		dueDate = MedProfileUtils.trimSeconds(dueDate);
		Date pivasDueDate = dueDate;
		
		Date preliminarySupplyDate = calculatePrelimiarySupplyDate(workstore.getWorkstoreGroup().getWorkstoreGroupCode(),
				workstore.getHospital(), MedProfileUtils.addMinutes(new Date(), VETTING_MEDPROFILE_PIVAS_NEWITEM_PREPARATIONTIME.get(20)));
		
		if (pivasDueDate == null || preliminarySupplyDate == null) {
			return new Object[] {pivasDueDate, null};
		}
		
		if (ObjectUtils.compare(pivasDueDate, preliminarySupplyDate) <= 0) {
			
			AomSchedule schedule = aomScheduleManager.retrieveAomScheduleByDailyCode(dailyFreqCode, AomScheduleType.Pivas);
			if (schedule == null || schedule.getAdminTime() == null || schedule.getAdminTime().isEmpty()) {
				return new Object[] {preliminarySupplyDate, preliminarySupplyDate};
			}
			pivasDueDate = calculatePivasDueDate(aomScheduleManager.retrieveAomScheduleByDailyCode(dailyFreqCode, AomScheduleType.Pivas), preliminarySupplyDate, pivasDueDate);
		}
		
		return new Object[] {pivasDueDate,
				pivasWorklistManager.calculateSupplyDateByDueDate(workstore.getWorkstoreGroup().getWorkstoreGroupCode(), workstore.getHospital(),
						MedProfileUtils.addMinutes(pivasDueDate, -1), false)};
	}
	
	private Date calculatePrelimiarySupplyDate(String workstoreGroupCode, Hospital hospital, Date dueDate) {
		
		List<PivasServiceHour> serviceHourList = pivasWorklistManager.retrievePivasSupplyDateList(workstoreGroupCode, dueDate, hospital);
		
		for (PivasServiceHour serviceHour: serviceHourList) {
			if (ObjectUtils.compare(dueDate, serviceHour.getSupplyDate()) <= 0) {
				return serviceHour.getSupplyDate();
			}
		}
		
		return null;
	}
	
	private Date calculatePivasDueDate(AomSchedule schedule, Date preliminarySupplyDate, Date dueDate) {
		
		Calendar resultCal = Calendar.getInstance();
		resultCal.setTime(dueDate);
		resultCal.set(Calendar.SECOND, 0);
		resultCal.set(Calendar.MILLISECOND, 0);
		
		for (int i = 0; i < 7; i++) {
			for (String time: schedule.getAdminTime()) {
				resultCal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time.substring(0, 2)));
				resultCal.set(Calendar.MINUTE, Integer.parseInt(time.substring(2)));

				if (ObjectUtils.compare(resultCal.getTime(), dueDate) >= 0 && ObjectUtils.compare(resultCal.getTime(), preliminarySupplyDate) > 0) {
					return resultCal.getTime();
				}
			}
			resultCal.add(Calendar.DATE, 1);
		}
		
		return dueDate;
	}
	
	public PivasSupplyDateListInfo retrievePivasSupplyDateListInfoByDueDate(Date dueDate) {
		Date defaultSupplyDate = pivasWorklistManager.calculateSupplyDateByDueDate(workstore.getWorkstoreGroup().getWorkstoreGroupCode(), workstore.getHospital(), dueDate, false);
		List<PivasServiceHour> pivasServiceHourList = pivasWorklistManager.retrievePivasSupplyDateList(workstore.getWorkstoreGroup().getWorkstoreGroupCode(), new Date(), workstore.getHospital());
		
		PivasSupplyDateListInfo pivasSupplyDateListInfo = new PivasSupplyDateListInfo();
		pivasSupplyDateListInfo.setDefaultSupplyDate(defaultSupplyDate);
		pivasSupplyDateListInfo.setPivasServiceHourList(pivasServiceHourList);
		
		return pivasSupplyDateListInfo;
	}		
	
	public PivasItemEditInfo retrievePivasItemEditInfo(MedProfileMoItem medProfileMoItem) {
		
		PivasItemEditInfo pivasItemEditInfo = new PivasItemEditInfo();
		boolean pivasFormulaFlag = false;
		MedProfile medProfileInMemory = (MedProfile)Contexts.getSessionContext().get("medProfile");
		List<PivasWard> pivasWardList = dmsPmsServiceProxy.retrievePivasWardList(workstore.getDefPatHospCode(), workstore.getWorkstoreGroup().getWorkstoreGroupCode());
		
		for (PivasWard pivasWard : pivasWardList) {			
			if (StringUtils.equals(medProfileInMemory.getMedCase().getPasWardCode(), pivasWard.getCompId().getWard())) {
				pivasItemEditInfo.setPivasWardFlag(Boolean.TRUE);
				break;
			}
		}		
		
		if (Boolean.FALSE.equals(pivasItemEditInfo.getPivasWardFlag())) {
			return pivasItemEditInfo;
		}
		
		pivasFormulaFlag = medProfileMoItem.getPivasFormulaId() != null || medProfileMoItem.getRxItem().getPivasFormulaFlag();
		pivasItemEditInfo.setPivasFormulaFlag(Boolean.valueOf(pivasFormulaFlag));
		
		if (!pivasFormulaFlag) {
			return pivasItemEditInfo;
		}
		
		Integer mpiOrgItemNum = medProfileMoItem.getMedProfileItem().getOrgItemNum();
		Integer mpmiItemNum	= medProfileMoItem.getItemNum();
		for (PivasWorklist worklist : pivasWorklistList) {
			if (mpiOrgItemNum.equals(worklist.getMedProfileMoItem().getMedProfileItem().getOrgItemNum()) && mpmiItemNum.equals(worklist.getMedProfileMoItem().getItemNum())) {
				pivasItemEditInfo.setPivasWorklist(worklist);
				break;
			}
		}
		//get PivasWorklist from DB
		if (pivasItemEditInfo.getPivasWorklist() == null) {
			if (medProfileMoItem.getMedProfileItem().getId() != null) {
				pivasItemEditInfo.setPivasWorklist(pivasWorklistManager.retrievePivasWorklistByMedProfileItemId(medProfileMoItem.getMedProfileItem().getId()));
			}
		}
		//get processed flag
		if (pivasItemEditInfo.getPivasWorklist() != null) {
			pivasItemEditInfo.setProcessFlag(pivasItemEditInfo.getPivasWorklist().getPivasPrepId() != null);
		}
		
		return pivasItemEditInfo;
	}
	
	public void updatePivasDetailToContext(PivasWorklist pivasWorklistIn) {
		
		logger.info("pivasWorklistIn.id:#0, pivasWorklistIn.status:#1", pivasWorklistIn.getId(), pivasWorklistIn.getStatus().getDisplayValue());
		
		boolean inMemoryFlag = false;
		Integer mpiOrgItemNum = pivasWorklistIn.getMedProfileMoItem().getMedProfileItem().getOrgItemNum();
		Integer mpmiItemNum	= pivasWorklistIn.getMedProfileMoItem().getItemNum();
		int i=0;
		for (PivasWorklist worklist : pivasWorklistList) {
			if ( mpiOrgItemNum.equals(worklist.getMedProfileMoItem().getMedProfileItem().getOrgItemNum()) && mpmiItemNum.equals(worklist.getMedProfileMoItem().getItemNum()) ) {
				pivasWorklistList.set(i, pivasWorklistIn);
				inMemoryFlag = true;
				break;
			}
			++i;
		}
		
		if (!inMemoryFlag) {
			pivasWorklistList.add(pivasWorklistIn);
		}		
	}
	
	public List<MedProfileItem> splitChemoMedProfileItem(MedProfileItem medProfileItem, List<ChemoItem> chemoItemList) {

		List<MedProfileItem> resultList = new ArrayList<MedProfileItem>();
		
		MedProfile medProfile = lookupMedProfile(medProfileItem);
		MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
		ChemoInfo originalChemoInfo = medProfileMoItem.getChemoInfo();
		medProfileMoItem.getMedProfilePoItemList().clear();
		if (originalChemoInfo.getChemoItemList().size() == chemoItemList.size()) {
			medProfileItemConverter.convertItemList(workstore, medProfile, Arrays.asList(medProfileMoItem), atdpsPickStationsNumList);
			resultList.add(medProfileItem);
		} else {
			List<ChemoItem> leftOverChemoItemList = minusChemoItemList(originalChemoInfo.getChemoItemList(), chemoItemList); 
			MedProfileItem clonedMedProfileItem = cloneChemoMedProfileItem(medProfileItem);
			medProfileMoItem.setChemoInfo(new ChemoInfo(chemoItemList));
			clonedMedProfileItem.getMedProfileMoItem().setChemoInfo(new ChemoInfo(leftOverChemoItemList));
			updateChemoMedProfileItemFromChemoInfo(medProfileItem, originalChemoInfo);
			updateChemoMedProfileItemFromChemoInfo(clonedMedProfileItem, originalChemoInfo);
			medProfileItemConverter.convertItemList(workstore, medProfile, Arrays.asList(medProfileItem.getMedProfileMoItem(), clonedMedProfileItem.getMedProfileMoItem()),
					atdpsPickStationsNumList);
			resultList.add(medProfileItem);
			resultList.add(clonedMedProfileItem);
		}
		resultList.get(0).getMedProfileMoItem().setTreatmentDaySelected(true);
		MedProfileUtils.loadDmInfoAndInitUuidAndNewlyConvertedPoItem(resultList);
		updateMedProfileItemsInSession(resultList);
		return resultList;
	}
	
	private MedProfile lookupMedProfile(MedProfileItem medProfileItem) {
		
		List<MedProfile> chemoMedProfileListInSession = (List<MedProfile>) Contexts.getSessionContext().get("chemoMedProfileList");
		
		for (MedProfile medProfile: chemoMedProfileListInSession) {
			if (medProfile.getId().equals(medProfileItem.getMedProfileId())) {
				return medProfile;
			}
		}
		throw new RuntimeException("Cannot lookup Chemo MedProfile when try to split Chemo MedProfileItem");
	}
	
	private void updateMedProfileItemsInSession(List<MedProfileItem> medProfileItemList) {
		
		for (MedProfileItem medProfileItem: medProfileItemList) {
			updateMedProfileItemInSession(medProfileItem);
		}
	}
	
	private void updateMedProfileItemInSession(MedProfileItem medProfileItem) {

		List<MedProfileItem> medProfileItemListInSession = (List<MedProfileItem>) Contexts.getSessionContext().get("medProfileItemList");

		for (int i = 0; i < medProfileItemListInSession.size(); i++) {
			MedProfileItem medProfileItemInSession = medProfileItemListInSession.get(i);
			if ((medProfileItem.getId() != null && ObjectUtils.compare(medProfileItem.getId(), medProfileItemInSession.getId()) == 0)) {
				medProfileItemListInSession.set(i, medProfileItem);
				return;
			}
		}
		for (int i = 0; i < medProfileItemListInSession.size(); i++) {
			MedProfileItem medProfileItemInSession = medProfileItemListInSession.get(i);
			if (ObjectUtils.compare(medProfileItem.getOrgItemNum(), medProfileItemInSession.getOrgItemNum()) == 0) {
				medProfileItemListInSession.set(i, medProfileItem);
				return;
			}
		}
		medProfileItemListInSession.add(medProfileItem);
	}
	
	private MedProfileItem cloneChemoMedProfileItem(MedProfileItem medProfileItem) {
		
		MedProfileItem clonedMedProfileItem = new MedProfileItem();
		BeanUtils.copyProperties(medProfileItem, clonedMedProfileItem, new String[]{"id", "medProfile", "medProfileMoItem", "medProfileErrorLog"});
		MedProfileMoItem clonedMedProfileMoItem = cloneChemoMedProfileMoItem(medProfileItem.getMedProfileMoItem());
		clonedMedProfileItem.setMedProfileId(medProfileItem.getMedProfileId());
		clonedMedProfileItem.setMedProfileMoItem(clonedMedProfileMoItem);
		clonedMedProfileMoItem.setMedProfileItem(clonedMedProfileItem);
		return clonedMedProfileItem;
	}
	
	private MedProfileMoItem cloneChemoMedProfileMoItem(MedProfileMoItem medProfileMoItem) {

		MedProfileMoItem clonedMedProfileMoItem = new MedProfileMoItem();
		BeanUtils.copyProperties(medProfileMoItem, clonedMedProfileMoItem, new String[]{"id", "medProfileItem", "medProfilePoItemList"});
		List<MedProfileMoItemAlert> clonedMedProfileMoItemAlertList = cloneChemoMedProfileMoItemAlertList(medProfileMoItem.getMedProfileMoItemAlertList());
		for (MedProfileMoItemAlert clonedMedProfileMoItemAlert: clonedMedProfileMoItemAlertList) {
			clonedMedProfileMoItemAlert.setMedProfileMoItem(clonedMedProfileMoItem);
		}
		List<MedProfileMoItemFm> clonedMedProfileMoItemFmList = cloneChemoMedProfileMoItemFmList(medProfileMoItem.getMedProfileMoItemFmList());
		for (MedProfileMoItemFm clonedMedProfileMoItemFm: clonedMedProfileMoItemFmList) {
			clonedMedProfileMoItemFm.setMedProfileMoItem(clonedMedProfileMoItem);
		}
		
		return clonedMedProfileMoItem;
	}
	
	private List<MedProfileMoItemAlert> cloneChemoMedProfileMoItemAlertList(List<MedProfileMoItemAlert> medProfileMoItemAlertList) {
		
		List<MedProfileMoItemAlert> clonedMedProfileMoItemAlertList = new ArrayList<MedProfileMoItemAlert>();
		for (MedProfileMoItemAlert medProfileMoItemAlert: medProfileMoItemAlertList) {
			MedProfileMoItemAlert clonedMedProfileMoItemAlert = new MedProfileMoItemAlert();
			BeanUtils.copyProperties(medProfileMoItemAlert, clonedMedProfileMoItemAlert, new String[]{"id", "medProfileMoItem"});
			clonedMedProfileMoItemAlertList.add(clonedMedProfileMoItemAlert);
		}
		return clonedMedProfileMoItemAlertList;
	}
	
	private List<MedProfileMoItemFm> cloneChemoMedProfileMoItemFmList(List<MedProfileMoItemFm> medProfileMoItemFmList) {
		
		List<MedProfileMoItemFm> clonedMedProfileMoItemFmList = new ArrayList<MedProfileMoItemFm>();
		for (MedProfileMoItemFm medProfileMoItemFm: medProfileMoItemFmList) {
			MedProfileMoItemFm clonedMedProfileMoItemFm = new MedProfileMoItemFm();
			BeanUtils.copyProperties(medProfileMoItemFm, clonedMedProfileMoItemFm, new String[]{"id", "medProfileMoItem"});
			clonedMedProfileMoItemFmList.add(clonedMedProfileMoItemFm);
		}
		return clonedMedProfileMoItemFmList;
	}
	
	private List<ChemoItem> minusChemoItemList(List<ChemoItem> chemoItemList1, List<ChemoItem> chemoItemList2) {
		
		List<ChemoItem> resultList = new ArrayList<ChemoItem>();
		for (ChemoItem chemoItem1: chemoItemList1) {
			if (!containsChemoItem(chemoItemList2, chemoItem1)) {
				resultList.add(chemoItem1);
			}
		}
		return resultList;
	}
	
	private boolean containsChemoItem(List<ChemoItem> chemoItemList, ChemoItem chemoItem) {
		
		for (ChemoItem chemoItemToCheck: chemoItemList) {
			if (ObjectUtils.compare(chemoItemToCheck.getItemNum(), chemoItem.getItemNum()) == 0) {
				return true;
			}
		}
		return false;
	}
	
	private void updateChemoMedProfileItemFromChemoInfo(MedProfileItem medProfileItem, ChemoInfo originalChemoInfo) {
		
		MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
		List<ChemoItem> chemoItemList = medProfileItem.getMedProfileMoItem().getChemoInfo().getChemoItemList();
		ChemoItem firstChemoItem = findFirstChemoItem(chemoItemList);
		medProfileItem.setOrgItemNum(firstChemoItem.getOrgItemNum());
		medProfileMoItem.setOrgItemNum(firstChemoItem.getOrgItemNum());
		medProfileMoItem.setItemNum(firstChemoItem.getItemNum());
		
		medProfileMoItem.setStartDate(calculateChemoStartDate(chemoItemList));
		medProfileMoItem.setEndDate(calculateChemoEndDate(chemoItemList));
		
		double doseQtyRatio = (double) chemoItemList.size() / originalChemoInfo.getChemoItemList().size();  
		
		medProfileMoItem.preSave();
		medProfileMoItem.postLoad();
		
		RxItem rxItem = medProfileMoItem.getRxItem();
		
		if (rxItem instanceof RxDrug) {
			RxDrug rxDrug = (RxDrug) rxItem;
			rxDrug.setDrugOrderHtml(null);
			rxDrug.setDrugOrderTlf(null);
			rxDrug.setDrugOrderXml(null);
			DoseGroup doseGroup = rxDrug.getRegimen().getDoseGroupList().get(0);
			doseGroup.setStartDate(medProfileMoItem.getStartDate());
			doseGroup.setEndDate(medProfileMoItem.getEndDate());
			for (Dose dose: doseGroup.getDoseList()) {
				if (dose.getDispQty() != null) {
					dose.setDispQty((int) Math.round(dose.getDispQty() * doseQtyRatio));
				}
			}
		} else if (rxItem instanceof IvRxDrug) {
			IvRxDrug ivRxDrug = (IvRxDrug) rxItem;
			ivRxDrug.setDrugOrderHtml(null);
			ivRxDrug.setDrugOrderTlf(null);
			ivRxDrug.setDrugOrderXml(null);
			ivRxDrug.setStartDate(medProfileMoItem.getStartDate());
			ivRxDrug.setEndDate(medProfileMoItem.getEndDate());
			if (ivRxDrug.getDispQty() != null) {
				ivRxDrug.setDispQty((int) Math.round(ivRxDrug.getDispQty() * doseQtyRatio));
			}
		} else if (rxItem instanceof CapdRxDrug) {
			CapdRxDrug capdRxDrug = (CapdRxDrug) rxItem;
			capdRxDrug.setDrugOrderHtml(null);
			capdRxDrug.setDrugOrderTlf(null);
			capdRxDrug.setDrugOrderXml(null);
			for (CapdItem capdItem: capdRxDrug.getCapdItemList()) {
				capdItem.setStartDate(medProfileMoItem.getStartDate());
				capdItem.setEndDate(medProfileMoItem.getEndDate());
				if (capdItem.getDispQty() != null) {
					capdItem.setDispQty((int) Math.round(capdItem.getDispQty() * doseQtyRatio));
				}
			}
		}
	}
	
	private Date calculateChemoStartDate(List<ChemoItem> chemoItemList) {

		Date startDate = null;
		for (ChemoItem chemoItem: chemoItemList) {
			if (ObjectUtils.compare(startDate, chemoItem.getStartDate(), true) > 0) {
				startDate = chemoItem.getStartDate();
			}
		}
		return startDate;
	}
	
	private Date calculateChemoEndDate(List<ChemoItem> chemoItemList) {
		
		Date endDate = null;
		for (ChemoItem chemoItem: chemoItemList) {
			if (chemoItem.getEndDate() == null) {
				return null;
			}
			if (ObjectUtils.compare(endDate, chemoItem.getEndDate()) < 0) {
				endDate = chemoItem.getEndDate();
			}
		}
		return endDate;
	}
	
	private ChemoItem findFirstChemoItem(List<ChemoItem> chemoItemList) {
		
		ChemoItem resultItem = null;
		for (ChemoItem chemoItem: chemoItemList) {
			if (resultItem == null || ObjectUtils.compare(resultItem.getOrgItemNum(), chemoItem.getOrgItemNum()) < 0) {
				resultItem = chemoItem;
			}
		}
		return resultItem;
	}
	
	@Remove
	public void destroy() {		
		if (medProfileItem != null) {
			medProfileItem = null;
		}		
	}
}
