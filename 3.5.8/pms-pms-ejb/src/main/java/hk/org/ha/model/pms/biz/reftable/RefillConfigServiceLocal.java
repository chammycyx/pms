package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.persistence.reftable.RefillConfig;

import javax.ejb.Local;

@Local
public interface RefillConfigServiceLocal {

	void retrieveRefillConfig();
	
	void updateRefillConfig(RefillConfig updateRefillConfig);
	
	void updateEnableSelectionFlag(Boolean enableSelectionFlag);
	
	void updateHolidayRefillConfig(Boolean enableAdjHolidayFlag, Boolean enableAdjSundayFlag);
	
	void destroy();
	
}
