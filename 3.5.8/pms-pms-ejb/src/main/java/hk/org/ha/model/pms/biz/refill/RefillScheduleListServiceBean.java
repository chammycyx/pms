package hk.org.ha.model.pms.biz.refill;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("refillScheduleListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class RefillScheduleListServiceBean implements RefillScheduleListServiceLocal {

	
	@In
	private RefillScheduleListManagerLocal refillScheduleListManager;
		
	public void refreshRefillScheduleList(Long refillScheduleId, String refillActionIn) {
		refillScheduleListManager.refreshRefillScheduleList(refillScheduleId, refillActionIn);
	}
	
	public void retrieveRefillScheduleByOrderNum(String orderNum) {
		refillScheduleListManager.retrieveRefillScheduleByOrderNum(orderNum);
	}
	
	public List<RefillSchedule> retrieveRefillScheduleByPharmOrderId(Long pharmOrderId) {		
		return refillScheduleListManager.retrieveRefillScheduleByPharmOrderId(pharmOrderId);		
	}

	public void retrieveRefillScheduleFromCheckIssue(Long refillScheduleId) {		
		refillScheduleListManager.retrieveRefillScheduleFromCheckIssue(refillScheduleId);
	}
	
	public void retrieveRefillScheduleFromOneStop(Long refillScheduleId) {
		refillScheduleListManager.retrieveRefillScheduleFromOneStop(refillScheduleId);
	}
	
	public void retrieveRefillScheduleFromVetting(boolean manualRefillSave) {
		refillScheduleListManager.retrieveRefillScheduleFromVetting(manualRefillSave);
	}

	public void updateRefillScheduleListOnly(List<RefillSchedule> refillScheduleListIn, Long currRsId) {
		refillScheduleListManager.updateRefillScheduleListOnly(refillScheduleListIn, currRsId);
	}

	
	public void updateStdRefillScheduleList(List<RefillSchedule> refillScheduleListIn) {
		refillScheduleListManager.updateStdRefillScheduleList(refillScheduleListIn);
	}

	@Remove
	public void destroy() {
		
	}
	
}
