package hk.org.ha.model.pms.biz.order;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.biz.order.OrderHelper;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.ejb.TransactionRolledbackLocalException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("orderManager")
@MeasureCalls
public class OrderManagerBean implements OrderManagerLocal {

	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private OrderHelper orderHelper;
	
	@In
	private PatientManagerLocal patientManager;
	
	public MedOrder retrieveMedOrder(String orderNum) {
		return orderHelper.retrieveMedOrder(em, orderNum);
	}
	
	public MedOrder retrieveMedOrder(String orderNum, List<MedOrderStatus> excludeStatusList) {
		return orderHelper.retrieveMedOrder(em, orderNum, excludeStatusList);
	}	
	
	public void lockMedOrder(String orderNum) {
		orderHelper.lockMedOrder(em, orderNum);
	}
	
	public Workstore retrieveWorkstore(Workstore workstore) {

		return orderHelper.retrieveWorkstore(em, workstore);
	}

	public MedCase saveMedCase(MedCase remoteMedCase) {

		return orderHelper.saveMedCase(em, remoteMedCase, false);
	}

	public Patient savePatient(Patient remotePatient) {
		if (remotePatient == null) {
			return null;
		}
		
		Long patientId = null; 

		try {
			patientId = patientManager.savePatient(remotePatient).getId();
		} catch (TransactionRolledbackLocalException e) {
			if (e.getCause() != null && e.getCause().getCause() != null && 
					e.getCause().getCause() instanceof java.sql.SQLIntegrityConstraintViolationException) {
				// if patient with same ID is already inserted, skip saving patient 
				logger.warn("Patient is already inserted by another transaction, patKey = " + remotePatient.getPatKey());
				patientId = remotePatient.getId();
			} else {
				throw e;
			}
		}
		
		// since patient is saved in new transaction, need to get managed patient
		return (Patient) em.createQuery(
				"select o from Patient o" + // 20151012 index check : Patient.id : PK_PATIENT
				" where o.id in :idSet")
				.setParameter("idSet", Arrays.asList(patientId))
				.getResultList()
				.get(0);
	}

	public Ticket saveTicket(Ticket remoteTicket) {

		return orderHelper.saveTicket(em, remoteTicket);
	}	

	public MedOrder insertMedOrder(MedOrder medOrder) {
		return insertMedOrder(medOrder, false, false);
	}

	public MedOrder insertMedOrderByUnvet(MedOrder medOrder) {
		return insertMedOrder(medOrder, false, true);
	}
	
	public MedOrder insertMedOrder(MedOrder remoteMedOrder, boolean allowCreate,  boolean includeTicket) {
		Patient remotePatient = remoteMedOrder.getPatient();
		remoteMedOrder.setPatient(null);
		
		Long patientId = null; 
		
		if (remotePatient != null) {
			try {
				patientId = patientManager.savePatient(remotePatient).getId();
			} catch (TransactionRolledbackLocalException e) {
				if (e.getCause() != null && e.getCause().getCause() != null && 
						e.getCause().getCause() instanceof java.sql.SQLIntegrityConstraintViolationException) {
					// if patient with same ID is already inserted, skip saving patient 
					logger.warn("Patient is already inserted by another transaction, patKey = " + remotePatient.getPatKey());
					patientId = remotePatient.getId();
				} else {
					throw e;
				}
			}
			
			// since patient is saved in new transaction, need to get managed patient by em.find
			remotePatient = em.find(Patient.class, patientId);
		}
			
		MedOrder medOrder = orderHelper.insertMedOrder(em, remoteMedOrder, allowCreate, includeTicket);
		medOrder.setPatient(remotePatient);
		
		return medOrder;
	}
	
	public List<DispOrder> markOrderSysDeleted(
			PharmOrder pharmOrder, 
			Long prevPharmOrderId,
			Boolean remarkFlag) {
		
		orderHelper.markOrderSysDeletedForRefillSchedule(em, pharmOrder, prevPharmOrderId, remarkFlag);
		return orderHelper.markOrderSysDeleted(em, pharmOrder, prevPharmOrderId, remarkFlag);
	}	
	
	public void loadOrderList(MedOrder medOrder) 
	{		
		orderHelper.loadOrderList(em, medOrder);
	}
	
	public void loadPharmOrderList(MedOrder medOrder) 
	{		
		orderHelper.loadPharmOrderList(em, medOrder);
	}

	public void loadDispOrderList(PharmOrder pharmOrder) 
	{
		orderHelper.loadDispOrderList(em, pharmOrder);
	}

	public List<MedOrderItem> retrieveMedOrderItemByItemNum(String orderNum, List<Integer> medOrderItemNumList) {
		return orderHelper.retrieveMedOrderItemByItemNum(em, orderNum, medOrderItemNumList);
	}
	
	public Set<DispOrder> retrieveDispOrderList(List<Invoice> voidInvoiceList) {
		return orderHelper.retrieveDispOrderList(voidInvoiceList);
	}
}
