package hk.org.ha.model.pms.test;

import javax.ejb.Local;

@Local
public interface DBUnitImporterLocal {

    void startImport();
}
