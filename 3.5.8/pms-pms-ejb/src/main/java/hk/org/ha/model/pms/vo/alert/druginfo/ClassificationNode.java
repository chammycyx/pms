package hk.org.ha.model.pms.vo.alert.druginfo;

import java.io.Serializable;
import java.util.List;

public class ClassificationNode implements Serializable {

	private static final long serialVersionUID = 1L;

	private String label;
	
	private Boolean expandFlag;
	
	private List<ClassificationNode> children;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Boolean getExpandFlag() {
		return expandFlag;
	}

	public void setExpandFlag(Boolean expandFlag) {
		this.expandFlag = expandFlag;
	}

	public List<ClassificationNode> getChildren() {
		return children;
	}

	public void setChildren(List<ClassificationNode> children) {
		this.children = children;
	}
}
