package hk.org.ha.model.pms.udt.onestop;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum OneStopOrderStatus implements StringValuedEnum {

	Unvet("1", "Unvet"),
	ProcessingByPharmacy("2", "Processing (by Pharmacy)"),
	Vetted("3", "Vetted"),
	AwaitSfiPayment("4", "Await SFI payment"),
	Suspended("5", "Suspended"),
	Picking("6", "Picking"),
	Assembling("7", "Assembling"),
	Checked("8", "Checked"),
	Issued("9", "Issued"),
	Cancelled("10", "Cancelled"),
	Deleted("11", "Deleted"),
	LockingByDayEnd("12", "Locking (by Day End)"),
	NotYetDispensed("13", "Not Yet Dispensed"),
	BeingProcessed("4", "Being Processed"),
	Dispensed("15", "Dispensed"),
	ForceComplete("16", "Force Complete"),
	Abort("17", "Deleted"),
	PreVet("18", "Reconciled"),
	Withhold("19", "On-hold");
	
    private final String dataValue;
    private final String displayValue;
        
    OneStopOrderStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
       
    public static OneStopOrderStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(OneStopOrderStatus.class, dataValue);
	}
    
	public static class Converter extends StringValuedEnumConverter<OneStopOrderStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<OneStopOrderStatus> getEnumClass() {
    		return OneStopOrderStatus.class;
    	}
    }
}
