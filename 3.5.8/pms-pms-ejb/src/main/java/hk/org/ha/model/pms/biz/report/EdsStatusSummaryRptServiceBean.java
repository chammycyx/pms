package hk.org.ha.model.pms.biz.report;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.EdsStatusSummaryRpt;
import hk.org.ha.model.pms.vo.report.EdsStatusSummaryRptDtl;
import hk.org.ha.model.pms.vo.report.EdsStatusSummaryRptHdr;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("edsStatusSummaryRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class EdsStatusSummaryRptServiceBean implements EdsStatusSummaryRptServiceLocal {
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent; 
	
	@In 
	private Workstore workstore;
	
	@In
	private EdsStatusSummaryRptManagerLocal edsStatusSummaryRptManager;

	private boolean retrieveSuccess;
	
	private List<EdsStatusSummaryRpt> edsStatusSummaryRptList;
	
	private String currentRpt = null;
	
	private List<EdsStatusSummaryRpt> reportObj = null;
	

	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}
	
	public void retrieveEdsStatusSummaryRpt(Integer selectedTab, Date date) {
		reportObj = retrievEdsStatusSummaryRptList(date);
		
		if(selectedTab == 0){
			currentRpt = "EdsStatusSummaryChart";
		}
		else if(selectedTab == 1){
			currentRpt = "EdsStatusSummaryRpt";
		}
	}
	public Map<DispOrderItemStatus, Long> getDispOrderItemStatusCount(Date inputDate) 
	{
		return edsStatusSummaryRptManager.retrieveDispOrderItemStatusCount(inputDate);
	}

	public Map<DispOrderStatus, Long> getDispOrderStatusCount(Date inputDate) 
	{
		return edsStatusSummaryRptManager.retrieveDispOrderStatusCount(inputDate);
	}

	public List<EdsStatusSummaryRpt> retrievEdsStatusSummaryRptList(Date inputDate) {
		Map<DispOrderStatus, Long> dispOrderStatusCountMap = getDispOrderStatusCount(inputDate);
		Map<DispOrderItemStatus, Long> dispOrderItemStatusCountMap = getDispOrderItemStatusCount(inputDate);

		if ( dispOrderStatusCountMap.isEmpty() || dispOrderItemStatusCountMap.isEmpty() ) {
			retrieveSuccess = false;
			return null;
		} else{
			retrieveSuccess = true;
		}

		EdsStatusSummaryRpt edsStatusSummaryRpt = new EdsStatusSummaryRpt();
		OperationProfile op = em.find(OperationProfile.class, workstore.getOperationProfileId()); 
		
		EdsStatusSummaryRptHdr edsStatusSummaryRptHdr = new EdsStatusSummaryRptHdr();
		edsStatusSummaryRptHdr.setActiveProfileName(op.getName());
		edsStatusSummaryRptHdr.setHospCode(workstore.getHospCode());
		edsStatusSummaryRptHdr.setWorkstoreCode(workstore.getWorkstoreCode());

		edsStatusSummaryRpt.setEdsStatusSummaryRptHdr(edsStatusSummaryRptHdr);

		edsStatusSummaryRpt.setEdsStatusSummaryRptDtl(getEdsStatusSummaryRptDtl(inputDate, dispOrderStatusCountMap, dispOrderItemStatusCountMap));
		
		edsStatusSummaryRptList = new ArrayList<EdsStatusSummaryRpt>();
		edsStatusSummaryRptList.add(edsStatusSummaryRpt);
		
		return edsStatusSummaryRptList;
	}
	
	private EdsStatusSummaryRptDtl getEdsStatusSummaryRptDtl(Date inputDate, Map<DispOrderStatus, Long> dispOrderStatusCountMap, Map<DispOrderItemStatus, Long> dispOrderItemStatusCountMap) {
		int totalPickedItemCount = dispOrderItemStatusCountMap.get(DispOrderItemStatus.Picked).intValue() +
								   dispOrderItemStatusCountMap.get(DispOrderItemStatus.Assembled).intValue() +
								   dispOrderItemStatusCountMap.get(DispOrderItemStatus.Checked).intValue() +
								   dispOrderItemStatusCountMap.get(DispOrderItemStatus.Issued).intValue();
		
		int totalAssembledItemCount = dispOrderItemStatusCountMap.get(DispOrderItemStatus.Assembled).intValue() +
									  dispOrderItemStatusCountMap.get(DispOrderItemStatus.Checked).intValue() +
									  dispOrderItemStatusCountMap.get(DispOrderItemStatus.Issued).intValue();
		
		int totalCheckedOrderCount = dispOrderStatusCountMap.get(DispOrderStatus.Checked).intValue() +
									 dispOrderStatusCountMap.get(DispOrderStatus.Issued).intValue();
		
		int totalAssemblingItemCount = dispOrderItemStatusCountMap.get(DispOrderItemStatus.Vetted).intValue() + 
									   dispOrderItemStatusCountMap.get(DispOrderItemStatus.Picked).intValue();
		
		int totalVettingOrderCount = edsStatusSummaryRptManager.retrieveOutstandOrderCount(inputDate);
		
		int totalCheckingOrderCount = totalVettingOrderCount + 
										dispOrderStatusCountMap.get(DispOrderStatus.Vetted).intValue() +
										dispOrderStatusCountMap.get(DispOrderStatus.Picking).intValue() + 
										dispOrderStatusCountMap.get(DispOrderStatus.Picked).intValue() + 
										dispOrderStatusCountMap.get(DispOrderStatus.Assembling).intValue() + 
										dispOrderStatusCountMap.get(DispOrderStatus.Assembled).intValue();
		
		int totalIssuingOrderCount = totalVettingOrderCount + 
										dispOrderStatusCountMap.get(DispOrderStatus.Vetted).intValue() +
										dispOrderStatusCountMap.get(DispOrderStatus.Picking).intValue() + 
										dispOrderStatusCountMap.get(DispOrderStatus.Picked).intValue() + 
										dispOrderStatusCountMap.get(DispOrderStatus.Assembling).intValue() + 
										dispOrderStatusCountMap.get(DispOrderStatus.Assembled).intValue() + 
										dispOrderStatusCountMap.get(DispOrderStatus.Checked).intValue();
										
		
		EdsStatusSummaryRptDtl edsStatusSummaryRptDtl = new EdsStatusSummaryRptDtl();
		edsStatusSummaryRptDtl.setVettingOrderCount(totalVettingOrderCount);
		edsStatusSummaryRptDtl.setVettedOrderCount(edsStatusSummaryRptManager.retrieveVettedOrderCount(inputDate));
		edsStatusSummaryRptDtl.setPickingItemCount(dispOrderItemStatusCountMap.get(DispOrderItemStatus.Vetted).intValue());
		edsStatusSummaryRptDtl.setPickedItemCount(totalPickedItemCount);
		edsStatusSummaryRptDtl.setAssemblingItemCount(totalAssemblingItemCount);
		edsStatusSummaryRptDtl.setAssembledItemCount(totalAssembledItemCount);		
		edsStatusSummaryRptDtl.setCheckingOrderCount(totalCheckingOrderCount);
		edsStatusSummaryRptDtl.setCheckedOrderCount(totalCheckedOrderCount);
		edsStatusSummaryRptDtl.setIssuingOrderCount(totalIssuingOrderCount);
		edsStatusSummaryRptDtl.setIssuedOrderCount( dispOrderStatusCountMap.get(DispOrderStatus.Issued).intValue());

		return edsStatusSummaryRptDtl;
	}
	
	public void generateEdsStatusSummaryRpt() {

		Map<String, Object> parameters = new HashMap<String, Object>();
		if ("EdsStatusSummaryChart".equals(currentRpt)) {
			parameters.put("_PNG_ZOOM_RATIO", Float.valueOf(1f));
			printAgent.renderAndRedirect(new RenderJob(
					currentRpt + "Preview", 
					new PrintOption(), 
					parameters, 
					reportObj));
		} else {
			printAgent.renderAndRedirect(new RenderJob(
					currentRpt, 
					new PrintOption(), 
					parameters, 
					reportObj));
		}
	}
	
	public void printEdsStatusSummaryRpt(){

		printAgent.renderAndPrint(new RenderAndPrintJob(
				currentRpt, 
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				new HashMap<String, Object>(), 
				reportObj));
	}
	
	@Remove
	public void destroy() {
		if(edsStatusSummaryRptList!=null){
			edsStatusSummaryRptList=null;
		}		
	}
}
