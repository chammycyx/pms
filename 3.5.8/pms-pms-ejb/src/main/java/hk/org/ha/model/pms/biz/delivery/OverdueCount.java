package hk.org.ha.model.pms.biz.delivery;

public class OverdueCount {

	private Long newCount;
	private Long refillCount;
	
	public OverdueCount(Long refillCount, Long totalCount) {
		this.refillCount = refillCount;
		if (totalCount != null && refillCount != null) {
			this.newCount = totalCount - refillCount;
		}
	}

	public Long getNewCount() {
		return newCount;
	}

	public void setNewCount(Long newCount) {
		this.newCount = newCount;
	}

	public Long getRefillCount() {
		return refillCount;
	}

	public void setRefillCount(Long refillCount) {
		this.refillCount = refillCount;
	}
}
