package hk.org.ha.model.pms.udt.medprofile;


public enum RetrievePurchaseRequestOption {
	All,
	AwaitPayment,
	Paid;
}
