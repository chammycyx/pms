package hk.org.ha.model.pms.biz.pivas;

import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormula;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaDrug;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaMethod;
import hk.org.ha.model.pms.dms.vo.pivas.PivasPrepValidateResponse;
import hk.org.ha.model.pms.persistence.pivas.PivasBatchPrep;
import hk.org.ha.model.pms.vo.pivas.AddPivasBatchPrepDetail;
import hk.org.ha.model.pms.vo.pivas.BatchPrepMethodInfo;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface PivasBatchPrepServiceLocal {
	
	List<PivasFormula> retrievePivasFormulaListByPrefixDisplayName(String prefixDisplayName, boolean activeBatchPrepFormulaOnly);
	
	List<PivasFormulaDrug> retrievePivasFormulaDrugListByPrefixDisplayName(String prefixDisplayName);
	
	List<BatchPrepMethodInfo> retrieveBatchPrepMethodInfoList(Long pivasFormulaId);
	
	List<PivasFormulaMethod> retrievePivasFormulaMethodListByFormulaId(Long pivasFormulaId);
	
	AddPivasBatchPrepDetail createPivasBatchPrep(Long pivasFormulaMethodId);
	
	PivasPrepValidateResponse updatePivasBatchPrep(PivasBatchPrep pivasBatchPrep);
	
	List<PivasBatchPrep> retrievePivasBatchPrepList(Date manufFromDate, Date manufToDate, Integer drugKey);
	
	AddPivasBatchPrepDetail retrievePivasBatchPrep(String lotNo);
	
	List<PivasBatchPrep> deletePivasBatchPrep(String lotNo, Date manufFromDate, Date manufToDate, Integer drugKey);
	
	AddPivasBatchPrepDetail quickAddPivasBatchPrep(String lotNo);
	
	void printPivasBatchPrepLabel(PivasBatchPrep pivasBatchPrep, boolean isReprint);
	
	void getPivasProductLabelPreview(PivasBatchPrep pivasBatchPrep, boolean isQuickAdd);
	
	void generatePivasProductLabel();
	
	void generatePivasMergeFormulaProductLabel();
	
	boolean isDeleteSuccess();
	
	void destroy();
	
}