package hk.org.ha.model.pms.udt.report;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum WaitTimeAuditRptResult implements StringValuedEnum {

	RecordFound("F","Record found"),
	NoRecordFound("N","No record found"),
	NotApplicable("N/A","Record found but not application");
	
    private final String dataValue;
    private final String displayValue;

    WaitTimeAuditRptResult(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static WaitTimeAuditRptResult dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(WaitTimeAuditRptResult.class, dataValue);
	}
        
	public static class Converter extends StringValuedEnumConverter<WaitTimeAuditRptResult> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<WaitTimeAuditRptResult> getEnumClass() {
    		return WaitTimeAuditRptResult.class;
    	}
    }
}
