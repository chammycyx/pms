package hk.org.ha.model.pms.vo.report;

import hk.org.ha.model.pms.dms.udt.pivas.PivasFormulaType;
import hk.org.ha.model.pms.udt.pivas.PivasBatchPrepStatus;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class PivasBatchDispRptDtl implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<PivasBatchDispRptRawMaterialDtl> pivasBatchDispRptRawMaterialDtlList;
	
	private Date supplyDate;
	
	private Date prepExpiryDate;
	
	private Date printDate;
	
	private String lotNum;
	
	private String prepDesc;
	
	private String drugItemCode;
	
	private String solventItemCode;
	
	private String diluentItemCode;
	
	private String drugManufCode;
	
	private String solventManufCode;
	
	private String diluentManufCode;
	
	private String drugBatchNum;
	
	private String solventBatchNum;
	
	private String diluentBatchNum;
	
	private Date drugExpiryDate;
	
	private Date solventExpiryDate;
	
	private Date diluentExpiryDate;
	
	private String supplyDesc;
	
	private String vendSupplySolvDesc;
	
	private boolean vendSupplySolvFlag;

	// for export
	private PivasFormulaType pivasFormulaType;
	
	private Integer sortSeq;
	
	private String labelDesc;
	
	private String siteDesc;
	
	private String dosageQty;
	
	private String modu;
	
	private String concn;
	
	private String drugVolume;
	
	private String finalVolume;
	
	private String prepQty;
	
	private String containerName;
	
	private String formulaPrintName;
	
	private String numOfLabel;
	
	private String drugItemDesc;
	
	private String solventItemDesc;
	
	private String diluentItemDesc;
	
	private String diluentDesc;
	
	private PivasBatchPrepStatus pivasBatchPrepStatus;
	
	public PivasBatchDispRptDtl() {
		vendSupplySolvFlag = Boolean.FALSE;
	}
	
	public List<PivasBatchDispRptRawMaterialDtl> getPivasBatchDispRptRawMaterialDtlList() {
		return pivasBatchDispRptRawMaterialDtlList;
	}

	public void setPivasBatchDispRptRawMaterialDtlList(
			List<PivasBatchDispRptRawMaterialDtl> pivasBatchDispRptRawMaterialDtlList) {
		this.pivasBatchDispRptRawMaterialDtlList = pivasBatchDispRptRawMaterialDtlList;
	}
	
	public Date getSupplyDate() {
		return supplyDate;
	}

	public void setSupplyDate(Date supplyDate) {
		this.supplyDate = supplyDate;
	}

	public Date getPrepExpiryDate() {
		return prepExpiryDate;
	}

	public void setPrepExpiryDate(Date prepExpiryDate) {
		this.prepExpiryDate = prepExpiryDate;
	}

	public Date getPrintDate() {
		return printDate;
	}

	public void setPrintDate(Date printDate) {
		this.printDate = printDate;
	}

	public String getLotNum() {
		return lotNum;
	}

	public void setLotNum(String lotNum) {
		this.lotNum = lotNum;
	}

	public String getPrepDesc() {
		return prepDesc;
	}

	public void setPrepDesc(String prepDesc) {
		this.prepDesc = prepDesc;
	}

	public String getDrugItemCode() {
		return drugItemCode;
	}

	public void setDrugItemCode(String drugItemCode) {
		this.drugItemCode = drugItemCode;
	}

	public String getSolventItemCode() {
		return solventItemCode;
	}

	public void setSolventItemCode(String solventItemCode) {
		this.solventItemCode = solventItemCode;
	}

	public String getDiluentItemCode() {
		return diluentItemCode;
	}

	public void setDiluentItemCode(String diluentItemCode) {
		this.diluentItemCode = diluentItemCode;
	}

	public String getDrugManufCode() {
		return drugManufCode;
	}

	public void setDrugManufCode(String drugManufCode) {
		this.drugManufCode = drugManufCode;
	}

	public String getSolventManufCode() {
		return solventManufCode;
	}

	public void setSolventManufCode(String solventManufCode) {
		this.solventManufCode = solventManufCode;
	}

	public String getDiluentManufCode() {
		return diluentManufCode;
	}

	public void setDiluentManufCode(String diluentManufCode) {
		this.diluentManufCode = diluentManufCode;
	}

	public String getDrugBatchNum() {
		return drugBatchNum;
	}

	public void setDrugBatchNum(String drugBatchNum) {
		this.drugBatchNum = drugBatchNum;
	}

	public String getSolventBatchNum() {
		return solventBatchNum;
	}

	public void setSolventBatchNum(String solventBatchNum) {
		this.solventBatchNum = solventBatchNum;
	}

	public String getDiluentBatchNum() {
		return diluentBatchNum;
	}

	public void setDiluentBatchNum(String diluentBatchNum) {
		this.diluentBatchNum = diluentBatchNum;
	}

	public Date getDrugExpiryDate() {
		return drugExpiryDate;
	}

	public void setDrugExpiryDate(Date drugExpiryDate) {
		this.drugExpiryDate = drugExpiryDate;
	}

	public Date getSolventExpiryDate() {
		return solventExpiryDate;
	}

	public void setSolventExpiryDate(Date solventExpiryDate) {
		this.solventExpiryDate = solventExpiryDate;
	}

	public Date getDiluentExpiryDate() {
		return diluentExpiryDate;
	}

	public void setDiluentExpiryDate(Date diluentExpiryDate) {
		this.diluentExpiryDate = diluentExpiryDate;
	}

	public String getSupplyDesc() {
		return supplyDesc;
	}

	public void setSupplyDesc(String supplyDesc) {
		this.supplyDesc = supplyDesc;
	}

	public String getVendSupplySolvDesc() {
		return vendSupplySolvDesc;
	}

	public void setVendSupplySolvDesc(String vendSupplySolvDesc) {
		this.vendSupplySolvDesc = vendSupplySolvDesc;
	}

	public boolean isVendSupplySolvFlag() {
		return vendSupplySolvFlag;
	}

	public void setVendSupplySolvFlag(boolean vendSupplySolvFlag) {
		this.vendSupplySolvFlag = vendSupplySolvFlag;
	}

	public PivasFormulaType getPivasFormulaType() {
		return pivasFormulaType;
	}

	public void setPivasFormulaType(PivasFormulaType pivasFormulaType) {
		this.pivasFormulaType = pivasFormulaType;
	}	
	
	
	public Integer getSortSeq() {
		return sortSeq;
	}

	public void setSortSeq(Integer sortSeq) {
		this.sortSeq = sortSeq;
	}	
	
	public String getLabelDesc() {
		return labelDesc;
	}

	public void setLabelDesc(String labelDesc) {
		this.labelDesc = labelDesc;
	}

	public String getSiteDesc() {
		return siteDesc;
	}

	public void setSiteDesc(String siteDesc) {
		this.siteDesc = siteDesc;
	}

	public String getDosageQty() {
		return dosageQty;
	}

	public void setDosageQty(String dosageQty) {
		this.dosageQty = dosageQty;
	}

	public String getModu() {
		return modu;
	}

	public void setModu(String modu) {
		this.modu = modu;
	}

	public String getConcn() {
		return concn;
	}

	public void setConcn(String concn) {
		this.concn = concn;
	}

	public String getDrugVolume() {
		return drugVolume;
	}

	public void setDrugVolume(String drugVolume) {
		this.drugVolume = drugVolume;
	}

	public String getFinalVolume() {
		return finalVolume;
	}

	public void setFinalVolume(String finalVolume) {
		this.finalVolume = finalVolume;
	}

	public String getPrepQty() {
		return prepQty;
	}

	public void setPrepQty(String prepQty) {
		this.prepQty = prepQty;
	}

	public String getContainerName() {
		return containerName;
	}

	public void setContainerName(String containerName) {
		this.containerName = containerName;
	}

	public String getFormulaPrintName() {
		return formulaPrintName;
	}

	public void setFormulaPrintName(String formulaPrintName) {
		this.formulaPrintName = formulaPrintName;
	}

	public String getNumOfLabel() {
		return numOfLabel;
	}

	public void setNumOfLabel(String numOfLabel) {
		this.numOfLabel = numOfLabel;
	}

	public String getDrugItemDesc() {
		return drugItemDesc;
	}

	public void setDrugItemDesc(String drugItemDesc) {
		this.drugItemDesc = drugItemDesc;
	}

	public String getSolventItemDesc() {
		return solventItemDesc;
	}

	public void setSolventItemDesc(String solventItemDesc) {
		this.solventItemDesc = solventItemDesc;
	}

	public String getDiluentItemDesc() {
		return diluentItemDesc;
	}

	public void setDiluentItemDesc(String diluentItemDesc) {
		this.diluentItemDesc = diluentItemDesc;
	}

	public String getDiluentDesc() {
		return diluentDesc;
	}

	public void setDiluentDesc(String diluentDesc) {
		this.diluentDesc = diluentDesc;
	}

	public PivasBatchPrepStatus getPivasBatchPrepStatus() {
		return pivasBatchPrepStatus;
	}

	public void setPivasBatchPrepStatus(PivasBatchPrepStatus pivasBatchPrepStatus) {
		this.pivasBatchPrepStatus = pivasBatchPrepStatus;
	}
}
