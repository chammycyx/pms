package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.pas.PasException;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.cache.PasSpecialtyCacherInf;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.RefillConfig;
import hk.org.ha.model.pms.persistence.reftable.RefillSelection;
import hk.org.ha.model.pms.vo.patient.PasSpecialty;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.reftable.RefillSelectionInfo;
import hk.org.ha.model.pms.vo.report.RefillModuleMaintRpt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("refillSelectionListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class RefillSelectionListServiceBean implements RefillSelectionListServiceLocal {

	@PersistenceContext
	private EntityManager em;

	@In
	private Workstore workstore;
	
	@In
	private PasSpecialtyCacherInf pasSpecialtyCacher;
	
	@In
	private PrintAgentInf printAgent; 

	private List<RefillSelection> refillSelectionList;
	private List<RefillModuleMaintRpt> refillModuleMaintRptList;

	@SuppressWarnings("unchecked")
	public RefillSelectionInfo retrieveRefillSelectionList() throws PasException
	{
		workstore = em.find(Workstore.class, workstore.getId());
		
		refillSelectionList = em.createQuery(
									"select o from RefillSelection o" + // 20120303 index check : RefillSelection.workstore : FK_REFILL_SELECTION_01
									" where o.workstore = :workstore" +
									" order by o.itemCode")
									.setParameter("workstore", workstore)
									.getResultList();
		
		List<PasSpecialty> refillModulePasSpecialtyList = pasSpecialtyCacher.getPasSpecialtyList();
		
		removeInvalidRefillSelection(refillModulePasSpecialtyList);
		
		RefillSelectionInfo refillSelectionInfo = new RefillSelectionInfo();
		refillSelectionInfo.setRefillModulePasSpecialtyList(refillModulePasSpecialtyList);
		refillSelectionInfo.setRefillSelectionList(refillSelectionList);
		
		return refillSelectionInfo;
	}
	
	private void removeInvalidRefillSelection(List<PasSpecialty> pasSpecList) {
		Map<String, PasSpecialty> pasSpecMap = new HashMap<String, PasSpecialty>();
		
		for ( PasSpecialty pasSpec : pasSpecList ) {
			pasSpecMap.put(pasSpec.getType().getDisplayValue()+pasSpec.getPasSpecCode(), pasSpec);
		}
		
		for ( Iterator<RefillSelection> itr = refillSelectionList.iterator(); itr.hasNext(); ) {
			RefillSelection refillSelection = itr.next();
			if ( !"*".equals(refillSelection.getPasSpecCode()) ) {
				if ( pasSpecMap.get(refillSelection.getType()+refillSelection.getPasSpecCode()) == null ) {
					itr.remove();
				}
			}
		}
	}

	public void updateRefillSelectionList(Collection<RefillSelection> updateRefillSelectionList, Collection<RefillSelection> delRefillSelectionList) throws PasException
	{
		workstore = em.find(Workstore.class, workstore.getId());

		if(!delRefillSelectionList.isEmpty())
		{
			for(RefillSelection delRefillSelection:delRefillSelectionList)
			{
				if(delRefillSelection.getId()!=null)
				{
					em.remove(em.merge(delRefillSelection));
				}
			}
		}
		em.flush();
		
		for(RefillSelection refillSelection:updateRefillSelectionList)
		{
			if(StringUtils.isNotBlank(refillSelection.getItemCode())){
				if(refillSelection.getId()==null)
				{
					refillSelection.setWorkstore(workstore);
					em.persist(refillSelection);
				}
				else
				{
					refillSelection.setWorkstore(workstore);
					em.merge(refillSelection);
				}
			}
		}
		
		em.flush();
		em.clear();
	}
	
	@SuppressWarnings("unchecked")
	public List <RefillModuleMaintRpt> retrieveRefillSelectionRptList(){
		refillModuleMaintRptList = new ArrayList<RefillModuleMaintRpt>();
		List<RefillConfig> refillConfigList;
		
		refillConfigList = em.createQuery(
				"select o from RefillConfig o" + // 20120303 index check : RefillConfig.workstore : UI_REFILL_CONFIG_01
				" where o.workstore = :workstore")
				.setParameter("workstore", workstore)
				.getResultList();
		
		if (refillConfigList.get(0).getEnableSelectionFlag()){
			RefillModuleMaintRpt refillModuleMaintRpt = new RefillModuleMaintRpt();
			refillModuleMaintRpt.setPasSpecCode("*");
			refillModuleMaintRpt.setItemCode("*");
			refillModuleMaintRpt.setUpdateDate(refillConfigList.get(0).getUpdateDate());
			refillModuleMaintRpt.setUpdateUser(refillConfigList.get(0).getUpdateUser());
			refillModuleMaintRpt.setMappingLevel(getMappingLevel(refillModuleMaintRpt.getPasSpecCode(), refillModuleMaintRpt.getItemCode()));
			refillModuleMaintRptList.add(refillModuleMaintRpt);
		}
		
		for (RefillSelection refillSelection : refillSelectionList) {
			RefillModuleMaintRpt refillModuleMaintRpt = new RefillModuleMaintRpt();
			refillModuleMaintRpt.setType(refillSelection.getType());
			refillModuleMaintRpt.setPasSpecCode(refillSelection.getPasSpecCode());
			refillModuleMaintRpt.setItemCode(refillSelection.getItemCode());
			refillModuleMaintRpt.setUpdateDate(refillSelection.getUpdateDate());
			refillModuleMaintRpt.setUpdateUser(refillSelection.getUpdateUser());
			refillModuleMaintRpt.setMappingLevel(getMappingLevel(refillModuleMaintRpt.getPasSpecCode(), refillModuleMaintRpt.getItemCode()));
			refillModuleMaintRptList.add(refillModuleMaintRpt);
		}
		
		Collections.sort(refillModuleMaintRptList, new ComparatorForRefillModuleMaintRptList());
		return refillModuleMaintRptList;
	}
	
	public void exportRefillModuleMaintRpt() {	

		retrieveRefillSelectionRptList();

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("hospCode", workstore.getHospCode());
		parameters.put("workstoreCode", workstore.getWorkstoreCode());	
		
		PrintOption po = new PrintOption();
		po.setDocType(ReportProvider.XLS);
		printAgent.renderAndRedirect(new RenderJob(
		    "RefillModuleMaintXls",
		    po,
		    parameters, 
		    refillModuleMaintRptList));		
	}
	
	private String getMappingLevel(String pasSpecCode, String itemCode){
		if ( "*".equals(pasSpecCode) ) {
			return ( "*".equals(itemCode) )?"1":"2";
		} else {
			return ( "*".equals(itemCode) )?"3":"4";
		}
	}
	
	private static class ComparatorForRefillModuleMaintRptList implements Comparator<RefillModuleMaintRpt>, Serializable {
		
		private static final long serialVersionUID = 1L;

		public int compare(RefillModuleMaintRpt r1, RefillModuleMaintRpt r2) {
						
			return new CompareToBuilder().append(r1.getMappingLevel(), r2.getMappingLevel())
										.append(r1.getType(), r2.getType())
										.append(r1.getPasSpecCode(), r2.getPasSpecCode())
										.append(r1.getItemCode(), r2.getItemCode())
										.append(r1.getUpdateDate(), r2.getUpdateDate())
										.append(r1.getUpdateUser(), r2.getUpdateUser())
										.toComparison();
		}
	}
	
	@Remove
	public void destroy() 
	{
		if (refillModuleMaintRptList!=null){
			refillModuleMaintRptList = null;
		}
	}
}
