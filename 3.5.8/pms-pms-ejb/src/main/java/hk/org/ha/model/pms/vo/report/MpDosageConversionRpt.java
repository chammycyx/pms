package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class MpDosageConversionRpt implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String drug;
	
	private String fullDrugDesc;
	
	private Double moDosageFrom;
	
	private Double moDosageTo;
	
	private String moDosageUnit;
	
	private Double dispDosage;
	
	private String dispDosageUnit;
	
	private String fixed;
	
	private String suspend;
	
	private String itemCode;
	
	private String moDosageUnitMultiply;
	
	private String dispDosageUnitMultiply;

	public String getDrug() {
		return drug;
	}

	public void setDrug(String drug) {
		this.drug = drug;
	}

	public String getFullDrugDesc() {
		return fullDrugDesc;
	}

	public void setFullDrugDesc(String fullDrugDesc) {
		this.fullDrugDesc = fullDrugDesc;
	}

	public Double getMoDosageFrom() {
		return moDosageFrom;
	}

	public void setMoDosageFrom(Double moDosageFrom) {
		this.moDosageFrom = moDosageFrom;
	}

	public Double getMoDosageTo() {
		return moDosageTo;
	}

	public void setMoDosageTo(Double moDosageTo) {
		this.moDosageTo = moDosageTo;
	}

	public String getMoDosageUnit() {
		return moDosageUnit;
	}

	public void setMoDosageUnit(String moDosageUnit) {
		this.moDosageUnit = moDosageUnit;
	}

	public Double getDispDosage() {
		return dispDosage;
	}

	public void setDispDosage(Double dispDosage) {
		this.dispDosage = dispDosage;
	}

	public String getDispDosageUnit() {
		return dispDosageUnit;
	}

	public void setDispDosageUnit(String dispDosageUnit) {
		this.dispDosageUnit = dispDosageUnit;
	}

	public String getFixed() {
		return fixed;
	}

	public void setFixed(String fixed) {
		this.fixed = fixed;
	}

	public String getSuspend() {
		return suspend;
	}

	public void setSuspend(String suspend) {
		this.suspend = suspend;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getMoDosageUnitMultiply() {
		return moDosageUnitMultiply;
	}

	public void setMoDosageUnitMultiply(String moDosageUnitMultiply) {
		this.moDosageUnitMultiply = moDosageUnitMultiply;
	}

	public String getDispDosageUnitMultiply() {
		return dispDosageUnitMultiply;
	}

	public void setDispDosageUnitMultiply(String dispDosageUnitMultiply) {
		this.dispDosageUnitMultiply = dispDosageUnitMultiply;
	}
	
	

}