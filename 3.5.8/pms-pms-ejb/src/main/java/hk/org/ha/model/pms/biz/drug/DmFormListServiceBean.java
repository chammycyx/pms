package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmFormCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmForm;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("dmFormListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DmFormListServiceBean implements DmFormListServiceLocal {

	@Logger
	private Log logger;

	@In
	private DmFormCacherInf dmFormCacher;

	@Out(required = false)
	private List<DmForm> dmFormList;	
	
	public void retrieveDmFormList(String prefixFormCode) 
	{
		logger.debug("retrieveDmFormList");
				
		if(StringUtils.isBlank(prefixFormCode)){
			dmFormList = dmFormCacher.getFormList();
		}else{
			dmFormList = dmFormCacher.getFormListByFormCode(prefixFormCode);
		}
	}

	@Remove
	public void destroy() 
	{
		if (dmFormList != null) {
			dmFormList = null;
		}
	}
	
}
