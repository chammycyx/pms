package hk.org.ha.model.pms.biz.delivery;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.Delivery;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("deliveryListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DeliveryListServiceBean implements DeliveryListServiceLocal {

	@In
	private DueOrderPrintManagerLocal dueOrderPrintManager;
	
	@In
	private Workstore workstore;
	
	@Out(required = false)
	protected List<Delivery> deliveryList;
	
    public DeliveryListServiceBean() {
    }
    
    @Override
    public void retrieveTodayDeliveryList() {
    	deliveryList = dueOrderPrintManager.retrieveTodayDeliveryList(workstore);
    }

    @Remove
	@Override
	public void destroy() {
		deliveryList = null;
	}
}
