package hk.org.ha.model.pms.biz.medprofile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;

import java.util.Collection;

import javax.ejb.Local;

@Local
public interface MedProfileStatTrxManagerLocal {
	void recalculateStat(Collection<MedProfile> medProfileList,
			boolean recalculateMedProfileStat, boolean recalculateErrorStat,
			boolean recalculateMedProfilePoItemStat);
}
