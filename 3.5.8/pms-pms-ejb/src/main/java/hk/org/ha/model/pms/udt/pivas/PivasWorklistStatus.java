package hk.org.ha.model.pms.udt.pivas;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

import java.util.Arrays;
import java.util.List;

public enum PivasWorklistStatus implements StringValuedEnum {

	New("N", "New"),
	Refill("R", "Refill"),
	Prepared("P", "Prepared"),
	Completed("C", "Completed"),
	Deleted("D", "Deleted"),
	SysDeleted("X", "SysDeleted");
	
    private final String dataValue;
    private final String displayValue;

    public static final List<PivasWorklistStatus> New_Refill_Prepared = Arrays.asList(New, Refill, Prepared);
    
    PivasWorklistStatus(String dataValue, String displayValue) {
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    } 

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }
    
    public static PivasWorklistStatus dataValueOf(String dataValue) {
    	return StringValuedEnumReflect.getObjectFromValue(PivasWorklistStatus.class, dataValue);
    }
    
	public static class Converter extends StringValuedEnumConverter<PivasWorklistStatus> {
		
		private static final long serialVersionUID = 1L;

		@Override
    	public Class<PivasWorklistStatus> getEnumClass() {
			return PivasWorklistStatus.class;
		}
	}
}
