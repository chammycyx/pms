package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class SfiRefillCouponHdr implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String hospitalNameEng;
	
	private String hospitalNameChi;
	
	private Boolean reprintFlag;
	
	private String patName;
	
	private String patNameChi;
	
	private String caseNum;
	
	private String hkid;
	
	private String sex;
	
	private String age;
	
	private String specCode;
	
	private Date dispDate;
	
	private String ticketNum;
	
	private String workstoreCode;
	
	private Date orgDispDate;
	
	private String orgWorkstoreCode;
	
	private String orgTicketNum;
	
	private String doctorCode;
	
	private Date refillDate;
	
	private String medOrderItemDesc;
	
	private String instruction;
	
	private String specialInstruction;
	
	private boolean infusionFlag;
	
	private String infusionInstruction;

	public String getHospitalNameEng() {
		return hospitalNameEng;
	}
	
	public void setHospitalNameEng(String hospitalNameEng) {
		this.hospitalNameEng = hospitalNameEng;
	}
	
	public String getHospitalNameChi() {
		return hospitalNameChi;
	}
	
	public void setHospitalNameChi(String hospitalNameChi) {
		this.hospitalNameChi = hospitalNameChi;
	}
	
	public Boolean getReprintFlag() {
		return reprintFlag;
	}
	
	public void setReprintFlag(Boolean reprintFlag) {
		this.reprintFlag = reprintFlag;
	}
	
	public String getPatName() {
		return patName;
	}
	
	public void setPatName(String patName) {
		this.patName = patName;
	}
	
	public String getPatNameChi() {
		return patNameChi;
	}
	
	public void setPatNameChi(String patNameChi) {
		this.patNameChi = patNameChi;
	}
	
	public String getCaseNum() {
		return caseNum;
	}
	
	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}
	
	public String getHkid() {
		return hkid;
	}
	
	public void setHkid(String hkid) {
		this.hkid = hkid;
	}
	
	public String getSex() {
		return sex;
	}
	
	public void setSex(String sex) {
		this.sex = sex;
	}
	
	public String getAge() {
		return age;
	}
	
	public void setAge(String age) {
		this.age = age;
	}
	
	public String getSpecCode() {
		return specCode;
	}
	
	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}
	
	public Date getDispDate() {
		return dispDate;
	}
	
	public void setDispDate(Date dispDate) {
		this.dispDate = dispDate;
	}
	
	public String getTicketNum() {
		return ticketNum;
	}
	
	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}
	
	public String getWorkstoreCode() {
		return workstoreCode;
	}
	
	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}
	
	public Date getOrgDispDate() {
		return orgDispDate;
	}
	
	public void setOrgDispDate(Date orgDispDate) {
		this.orgDispDate = orgDispDate;
	}
	
	public String getOrgWorkstoreCode() {
		return orgWorkstoreCode;
	}
	
	public void setOrgWorkstoreCode(String orgWorkstoreCode) {
		this.orgWorkstoreCode = orgWorkstoreCode;
	}
	
	public String getOrgTicketNum() {
		return orgTicketNum;
	}
	
	public void setOrgTicketNum(String orgTicketNum) {
		this.orgTicketNum = orgTicketNum;
	}
	
	public String getDoctorCode() {
		return doctorCode;
	}
	
	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}
	
	public Date getRefillDate() {
		return refillDate;
	}
	
	public void setRefillDate(Date refillDate) {
		this.refillDate = refillDate;
	}
	
	public void setMedOrderItemDesc(String medOrderItemDesc) {
		this.medOrderItemDesc = medOrderItemDesc;
	}
	
	public String getMedOrderItemDesc() {
		return medOrderItemDesc;
	}
	
	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}
	
	public String getInstruction() {
		return instruction;
	}

	public void setSpecialInstruction(String specialInstruction) {
		this.specialInstruction = specialInstruction;
	}

	public String getSpecialInstruction() {
		return specialInstruction;
	}

	public void setInfusionFlag(boolean infusionFlag) {
		this.infusionFlag = infusionFlag;
	}

	public boolean isInfusionFlag() {
		return infusionFlag;
	}

	public void setInfusionInstruction(String infusionInstruction) {
		this.infusionInstruction = infusionInstruction;
	}

	public String getInfusionInstruction() {
		return infusionInstruction;
	}	
}
