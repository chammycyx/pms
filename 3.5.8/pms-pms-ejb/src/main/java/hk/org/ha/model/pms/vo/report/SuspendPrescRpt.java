package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class SuspendPrescRpt implements Serializable {

	private static final long serialVersionUID = 1L;

	private Date dispDate;
	
	private String ticketNum;
	
	private String caseNum;
	
	private String suspendCode;
	
	private String patName;
	
	private String hkid;
	
	private boolean allowModifyFlag;
	
	private boolean uncollectFlag;
	
	private String itemCode;
	
	private String dispQty;
	
	private Boolean ddFlag;
	
	private String suspendUpdateUser;
	


	public Boolean getDdFlag() {
		return ddFlag;
	}

	public void setDdFlag(Boolean ddFlag) {
		this.ddFlag = ddFlag;
	}

	public String getDispQty() {
		return dispQty;
	}

	public void setDispQty(String dispQty) {
		this.dispQty = dispQty;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getSuspendUpdateUser() {
		return suspendUpdateUser;
	}

	public void setSuspendUpdateUser(String suspendUpdateUser) {
		this.suspendUpdateUser = suspendUpdateUser;
	}

	public Date getDispDate() {
		return dispDate;
	}

	public void setDispDate(Date dispDate) {
		this.dispDate = dispDate;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getSuspendCode() {
		return suspendCode;
	}

	public void setSuspendCode(String suspendCode) {
		this.suspendCode = suspendCode;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public void setAllowModifyFlag(boolean allowModifyFlag) {
		this.allowModifyFlag = allowModifyFlag;
	}

	public boolean isAllowModifyFlag() {
		return allowModifyFlag;
	}

	public void setUncollectFlag(boolean uncollectFlag) {
		this.uncollectFlag = uncollectFlag;
	}

	public boolean isUncollectFlag() {
		return uncollectFlag;
	}
	
	


}
