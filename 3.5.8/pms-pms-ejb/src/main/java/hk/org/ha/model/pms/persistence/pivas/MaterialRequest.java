package hk.org.ha.model.pms.persistence.pivas;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.PrivateOwned;

@Entity
@Table(name = "MATERIAL_REQUEST")
public class MaterialRequest extends VersionEntity {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "materialRequestSeq")
	@SequenceGenerator(name = "materialRequestSeq", sequenceName = "SQ_MATERIAL_REQUEST", initialValue = 100000000)
	private Long id;
	
	@Column(name = "FIRST_DOSE_SUPPLY_QTY")
	private Integer firstDoseSupplyQty;
	
	@ManyToOne
	@JoinColumn(name = "HOSP_CODE", nullable = false)
	private Hospital hospital;
	
	@Column(name = "HOSP_CODE", nullable = false, insertable = false, updatable = false)
	private String hospCode;	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MED_PROFILE_ID", nullable = false)
	private MedProfile medProfile;
	
	@Column(name = "MED_PROFILE_ID", nullable = false, insertable = false, updatable = false)
	private Long medProfileId;	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MED_PROFILE_MO_ITEM_ID", nullable = false)
	private MedProfileMoItem medProfileMoItem;
	
	@Column(name = "MED_PROFILE_MO_ITEM_ID", nullable = false, insertable = false, updatable = false)
	private Long medProfileMoItemId;	
	
	@PrivateOwned
	@OneToMany(mappedBy = "materialRequest", cascade = CascadeType.ALL)
	private List<MaterialRequestItem> materialRequestItemList;
	
	public MaterialRequest() {
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getFirstDoseSupplyQty() {
		return firstDoseSupplyQty;
	}

	public void setFirstDoseSupplyQty(Integer firstDoseSupplyQty) {
		this.firstDoseSupplyQty = firstDoseSupplyQty;
	}

	public Hospital getHospital() {
		return hospital;
	}

	public void setHospital(Hospital hospital) {
		this.hospital = hospital;
	}
	
	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public MedProfile getMedProfile() {
		return medProfile;
	}

	public void setMedProfile(MedProfile medProfile) {
		this.medProfile = medProfile;
	}
	
	public Long getMedProfileId() {
		return medProfileId;
	}

	public void setMedProfileId(Long medProfileId) {
		this.medProfileId = medProfileId;
	}

	public MedProfileMoItem getMedProfileMoItem() {
		return medProfileMoItem;
	}

	public void setMedProfileMoItem(MedProfileMoItem medProfileMoItem) {
		this.medProfileMoItem = medProfileMoItem;
	}
	
	public Long getMedProfileMoItemId() {
		return medProfileMoItemId;
	}

	public void setMedProfileMoItemId(Long medProfileMoItemId) {
		this.medProfileMoItemId = medProfileMoItemId;
	}

	public List<MaterialRequestItem> getMaterialRequestItemList() {
		if (materialRequestItemList == null) {
			materialRequestItemList = new ArrayList<MaterialRequestItem>();
		}
		return materialRequestItemList;
	}

	public void setMaterialRequestItemList(List<MaterialRequestItem> materialRequestItemList) {
		this.materialRequestItemList = materialRequestItemList;
	}
	
	public void addMaterialRequestItem(MaterialRequestItem materialRequestItem) {
		materialRequestItem.setMaterialRequest(this);
		getMaterialRequestItemList().add(materialRequestItem);
	}
}
