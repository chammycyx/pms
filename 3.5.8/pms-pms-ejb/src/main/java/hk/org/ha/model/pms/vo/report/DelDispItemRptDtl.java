package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class DelDispItemRptDtl implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String itemCode;
	
	private String labelDesc;
	
	private String labelInstructionText;
	
	private Integer durationInDay;
	
	private Integer dispQty;
	
	private String status;
	
	private String doctorCode;
	
	private Integer itemNum;
	
	public String getLabelDesc() {
		return labelDesc;
	}

	public void setLabelDesc(String labelDesc) {
		this.labelDesc = labelDesc;
	}

	public String getLabelInstructionText() {
		return labelInstructionText;
	}

	public void setLabelInstructionText(String labelInstructionText) {
		this.labelInstructionText = labelInstructionText;
	}

	public Integer getDurationInDay() {
		return durationInDay;
	}

	public void setDurationInDay(Integer durationInDay) {
		this.durationInDay = durationInDay;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public void setDispQty(Integer dispQty) {
		this.dispQty = dispQty;
	}

	
	public int getDispQty() {
		return dispQty;
	}

	public void setDispQty(int dispQty) {
		this.dispQty = dispQty;
	}

	
	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	
}
