package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.PrnRoute;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("prnRouteManager")
@MeasureCalls
public class PrnRouteManagerBean implements PrnRouteManagerLocal {

	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	public List<PrnRoute> getPrnRouteListByWorkstore(Workstore workstore) {		
		return (List<PrnRoute>)em.createQuery(
				"select o from PrnRoute o" + // 20120303 index check : PrnRoute.workstore : FK_PRN_ROUTE_01
				" where o.workstore = :workstore" +
				" and o.status = :recordStatus")
				.setParameter("workstore", workstore)
				.setParameter("recordStatus", RecordStatus.Active)
				.getResultList();			
	}
}
