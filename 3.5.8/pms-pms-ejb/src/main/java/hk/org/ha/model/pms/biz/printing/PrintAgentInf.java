package hk.org.ha.model.pms.biz.printing;

import hk.org.ha.model.pms.vo.printing.PrinterSelect;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import net.lingala.zip4j.exception.ZipException;

public interface PrintAgentInf extends Serializable {
		
	void renderAndPrint(RenderAndPrintJob job);
	
	void renderAndPrint(List<RenderAndPrintJob> jobs);
	
	void renderAndPrint(List<RenderAndPrintJob> jobs, boolean includeLotInfo);
		
	void renderAndRedirect(RenderJob job);

	void renderAndRedirect(RenderJob job, String fileName);

	String render(RenderJob job);
	
	void keep(String contentId);
	
	void print(PrinterSelect printerSelect, String printInfo);

	void print(String contentId, PrinterSelect printerSelect, String printInfo);
	
	void redirect(String contentId);

	void redirect(String contentId, String fileName);

	void zipAndRedirect(RenderJob job, String password) throws IOException, ZipException;
	
	void zipAndRedirect(RenderJob job, String fileName, String password) throws IOException, ZipException;
	
	File generateZipFile(List<File> fileArrayList, String password) throws ZipException;
}
