package hk.org.ha.model.pms.biz.delivery;
import javax.ejb.Local;

@Local
public interface MedProfileDueListServiceLocal {
	void retrieveMedProfileDueList(String caseNum, String hkid);
	void retrieveMedProfileItemDueList(Long medProfileId);
	void destroy();
}
