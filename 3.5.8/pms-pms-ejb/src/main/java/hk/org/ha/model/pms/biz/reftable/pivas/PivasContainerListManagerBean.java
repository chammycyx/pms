package hk.org.ha.model.pms.biz.reftable.pivas;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasContainer;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateless;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("pivasContainerListManager")
@MeasureCalls
public class PivasContainerListManagerBean implements PivasContainerListManagerLocal {

	@In
	private Workstore workstore;

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

	@Logger
	private Log logger;

	public List<PivasContainer> retrievePivasContainerList() {
		List<PivasContainer> list = dmsPmsServiceProxy.retrievePivasContainerList(workstore.getWorkstoreGroup().getWorkstoreGroupCode());
		logger.debug("list size = #0", list.size());
		return list;
	}

	@Remove
	public void destroy() {
	}

}
