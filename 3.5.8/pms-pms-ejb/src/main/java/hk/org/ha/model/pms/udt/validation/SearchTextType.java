package hk.org.ha.model.pms.udt.validation;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum SearchTextType implements StringValuedEnum {
	
	Case("1", "Case"),
	Hkid("2", "Hkid"),
	OrderNum("3", "OrderNum"),
	OrderRefNum("4", "OrderRefNum"),
	Refill("5", "Refill"),
	SfiRefill("6", "SfiRefill"),
	Ticket("7", "Ticket"),
	HkidFail("8", "HkidFail"),
	SfiInvoice("9", "SfiInvoice"),
	ManualPatient("10", "ManualPatient"),
	Unknown("11", "Unknown"),
	Capd("12", "Capd");
	
    private final String dataValue;
    private final String displayValue;

    SearchTextType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    } 

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }  
    
    public static SearchTextType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(SearchTextType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<SearchTextType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<SearchTextType> getEnumClass() {
    		return SearchTextType.class;
    	}
    }
}



