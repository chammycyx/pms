package hk.org.ha.model.pms.biz.reftable;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
import hk.org.ha.model.pms.persistence.reftable.ItemDispConfig;

import java.util.List;

import javax.ejb.Local;

@Local
public interface ItemDispConfigManagerLocal {

	ItemDispConfig retrieveItemDispConfig(String itemCode, WorkstoreGroup workstoreGroup);
	ItemDispConfig retrieveItemDispConfig(String itemCode, WorkstoreGroup workstoreGroup, boolean retrieveDispDays);
	List<ItemDispConfig> retrieveItemDispConfigList(WorkstoreGroup workstoreGroup);
	List<ItemDispConfig> retrieveItemDispConfigList(WorkstoreGroup workstoreGroup, boolean retrieveDispDays);
}
