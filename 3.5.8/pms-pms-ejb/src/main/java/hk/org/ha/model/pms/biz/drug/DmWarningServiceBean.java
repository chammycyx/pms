package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmWarningCacher;
import hk.org.ha.model.pms.dms.persistence.DmWarning;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("dmWarningService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DmWarningServiceBean implements DmWarningServiceLocal {

	@In
	private DmWarningCacher dmWarningCacher;

	@Out(required = false)
	private DmWarning dmWarning;
	
	public void retrieveDmWarning(String warnCode)
	{	
		dmWarning = dmWarningCacher.getDmWarningByWarnCode(warnCode);
	}
	
	public void clearDmWarning()
	{
		dmWarning = null;
	}

	
	@Remove
	public void destroy() 
	{
		if (dmWarning != null) {
			dmWarning = null;
		}
	}

}
