package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class MedSummaryRpt implements Serializable {

	private static final long serialVersionUID = 1L;

	private MedSummaryRptHdr medSummaryRptHdr;
	
	private List<MedSummaryRptDtl> medSummaryRptDtlList;

	public void setMedSummaryRptHdr(MedSummaryRptHdr medSummaryRptHdr) {
		this.medSummaryRptHdr = medSummaryRptHdr;
	}

	public MedSummaryRptHdr getMedSummaryRptHdr() {
		return medSummaryRptHdr;
	}

	public void setMedSummaryRptDtlList(List<MedSummaryRptDtl> medSummaryRptDtlList) {
		this.medSummaryRptDtlList = medSummaryRptDtlList;
	}

	public List<MedSummaryRptDtl> getMedSummaryRptDtlList() {
		if ( medSummaryRptDtlList == null ) {
			medSummaryRptDtlList = new ArrayList<MedSummaryRptDtl>();
		}
		return medSummaryRptDtlList;
	}
}
