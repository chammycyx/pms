package hk.org.ha.model.pms.biz.alert.mds;

import hk.org.ha.model.pms.dms.vo.PreparationProperty;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;

import javax.ejb.Local;

@Local
public interface PreparationCheckServiceLocal {

	AlertMsg checkPreparation(MedOrderItem medOrderItem, PreparationProperty preparationProperty, Boolean pregnancyCheckFlag);
	
	void destroy();

}
