package hk.org.ha.model.pms.udt.vetting;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum RegimenType implements StringValuedEnum {

	Daily("D", "Daily"),
	Weekly("W", "Weekly"),
	Monthly("M", "Monthly"),
	Cyclic("C", "Cyclic"),
	StepUpDown("S", "Step Up / Down");
	
	private final String dataValue;
	private final String displayValue;
	
	RegimenType (final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}

	public static RegimenType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(RegimenType.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<RegimenType> {

		private static final long serialVersionUID = 1L;

		public Class<RegimenType> getEnumClass() {
    		return RegimenType.class;
    	}
    }
}
