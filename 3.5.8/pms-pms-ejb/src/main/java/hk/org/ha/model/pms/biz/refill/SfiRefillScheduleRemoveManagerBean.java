package hk.org.ha.model.pms.biz.refill;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.persistence.disp.RefillScheduleItem;
import hk.org.ha.model.pms.udt.refill.DocType;


import javax.ejb.Stateless;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("sfiRefillScheduleRemoveManager")
@MeasureCalls
public class SfiRefillScheduleRemoveManagerBean implements SfiRefillScheduleRemoveManagerLocal {

	public void removeSfiRefillScheduleItemByPharmOrderItemNum(PharmOrder pharmOrderIn, Integer pharmOrderItemNum){	
		pharmOrderIn.reconnectRefillScheduleList();
		List<RefillSchedule> refillScheduleList = pharmOrderIn.getRefillScheduleList(DocType.Sfi);
		pharmOrderIn.clearRefillScheduleList();
		pharmOrderIn.disconnectRefillScheduleList();
		
		for( RefillSchedule refillSchedule : refillScheduleList ){
			boolean removeRs = false;
			for( RefillScheduleItem refillScheduleItem : refillSchedule.getRefillScheduleItemList() ){
				if( refillScheduleItem.getItemNum().equals(pharmOrderItemNum) ){
					removeRs = true;
					break;
				}
			}
			
			if( !removeRs ){
				refillSchedule.connect(pharmOrderIn);
			}
		}
		pharmOrderIn.disconnectRefillScheduleList();		
	}
}
