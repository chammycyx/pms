package hk.org.ha.model.pms.udt.reftable.pivas;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PivasServiceHourType implements StringValuedEnum{

	NewType("N", "New"),
	Refill("R", "Refill"), 
	Satellite("S", "Satellite");

	private final String dataValue;
	private final String displayValue;
	
	PivasServiceHourType(final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}
	
	public static PivasServiceHourType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PivasServiceHourType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<PivasServiceHourType> {

		private static final long serialVersionUID = 1L;

		public Class<PivasServiceHourType> getEnumClass() {
			return PivasServiceHourType.class;
		}

	}
}
