package hk.org.ha.model.pms.udt.vetting;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum CmsOrderType implements StringValuedEnum {

	None("-", "None"),
	Pharmacy("P", "Pharmacy");
	
	private final String dataValue;
	private final String displayValue;
	
	CmsOrderType (final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}
	
	public static CmsOrderType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(CmsOrderType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<CmsOrderType> {

		private static final long serialVersionUID = 1L;

		public Class<CmsOrderType> getEnumClass() {
    		return CmsOrderType.class;
    	}
    }
}
