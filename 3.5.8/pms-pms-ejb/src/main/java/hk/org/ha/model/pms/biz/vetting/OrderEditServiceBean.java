package hk.org.ha.model.pms.biz.vetting;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.vo.rx.RxItem;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("orderEditService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class OrderEditServiceBean implements OrderEditServiceLocal {


	@In
	private OrderEditManagerLocal orderEditManager;
		
	public String retrieveFormattedDrugOrderTlf(RxItem rxItem) {
		return orderEditManager.getFormattedDrugOrderTlf(rxItem);
	}

	@Override
	public void retrieveMedOrderItemForOrderEdit(int index) {
		orderEditManager.retrieveMedOrderItemForOrderEdit(index);
	}

	@Override
	public List<Integer> updateMedOrderFromOrderEdit(int index,
			MedOrderItem medOrderItemIn, boolean skipDuplicateCheckFlag) {
		return (List<Integer>) orderEditManager.updateMedOrderFromOrderEdit(index, medOrderItemIn, skipDuplicateCheckFlag);
	}
	
	public Boolean isDhMappingUpToDate(int index) {
		return orderEditManager.isDhMappingUpToDate(index);
	}
	
	@Remove
	@Override
	public void destroy() {
	}
}
