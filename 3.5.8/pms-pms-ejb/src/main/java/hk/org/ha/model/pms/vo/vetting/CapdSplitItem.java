package hk.org.ha.model.pms.vo.vetting;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "CapdSplitItem")
@ExternalizedBean(type = DefaultExternalizer.class)
public class CapdSplitItem implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement(name="PrintFlag", required = true)
	private Boolean printFlag;
	
	@XmlElement(name="GroupNum", required = true)
	private Integer groupNum;
	
	@XmlElement(name="ItemCode", required = true)
	private String itemCode;

	@XmlElement(name="Qty01")
	private Integer qty01;

	@XmlElement(name="Qty02")
	private Integer qty02;

	@XmlElement(name="Qty03")
	private Integer qty03;

	@XmlElement(name="Qty04")
	private Integer qty04;

	@XmlElement(name="Qty05")
	private Integer qty05;

	@XmlElement(name="Qty06")
	private Integer qty06;

	@XmlElement(name="Qty07")
	private Integer qty07;

	@XmlElement(name="Qty08")
	private Integer qty08;

	@XmlElement(name="Qty09")
	private Integer qty09;

	@XmlElement(name="Qty10")
	private Integer qty10;

	@XmlElement(name="DispQty", required = true)
	private Integer dispQty;

	@XmlElement(name="MedOrderItemNum")
	private Integer MedOrderItemNum;
	
	@XmlElement(name="ItemNum")
	private Integer itemNum;
	
	@XmlElement(name="VoucherNum")
	private List<String> voucherNumList;

	@XmlElement(name="SupplierCode")
	private String supplierCode;

	@XmlElement(name="GenVoucherFlag")
	private Boolean genVoucherFlag;
	
	@XmlTransient
	private String fullDrugDesc;
	
	@XmlTransient
	private BigDecimal orderQty;
	
	@XmlTransient
	private String baseUnit;
	
	@XmlTransient
	private Integer capdItemItemNum;
	
	@XmlTransient
	private boolean markUpdate = false;
	
	public CapdSplitItem() {
		if ( printFlag==null ){
			printFlag = Boolean.TRUE;
		}
		if ( genVoucherFlag==null ){
			genVoucherFlag = Boolean.FALSE;
		}
	}

	public Boolean getPrintFlag() {
		return printFlag;
	}

	public void setPrintFlag(Boolean printFlag) {
		this.printFlag = printFlag;
	}

	public Integer getGroupNum() {
		return groupNum;
	}

	public void setGroupNum(Integer groupNum) {
		this.groupNum = groupNum;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public Integer getQty01() {
		return qty01;
	}

	public void setQty01(Integer qty01) {
		this.qty01 = qty01;
	}

	public Integer getQty02() {
		return qty02;
	}

	public void setQty02(Integer qty02) {
		this.qty02 = qty02;
	}

	public Integer getQty03() {
		return qty03;
	}

	public void setQty03(Integer qty03) {
		this.qty03 = qty03;
	}

	public Integer getQty04() {
		return qty04;
	}

	public void setQty04(Integer qty04) {
		this.qty04 = qty04;
	}

	public Integer getQty05() {
		return qty05;
	}

	public void setQty05(Integer qty05) {
		this.qty05 = qty05;
	}

	public Integer getQty06() {
		return qty06;
	}

	public void setQty06(Integer qty06) {
		this.qty06 = qty06;
	}

	public Integer getQty07() {
		return qty07;
	}

	public void setQty07(Integer qty07) {
		this.qty07 = qty07;
	}

	public Integer getQty08() {
		return qty08;
	}

	public void setQty08(Integer qty08) {
		this.qty08 = qty08;
	}

	public Integer getQty09() {
		return qty09;
	}

	public void setQty09(Integer qty09) {
		this.qty09 = qty09;
	}

	public Integer getQty10() {
		return qty10;
	}

	public void setQty10(Integer qty10) {
		this.qty10 = qty10;
	}

	public Integer getDispQty() {
		return dispQty;
	}

	public void setDispQty(Integer dispQty) {
		this.dispQty = dispQty;
	}

	public void setFullDrugDesc(String fullDrugDesc) {
		this.fullDrugDesc = fullDrugDesc;
	}

	public String getFullDrugDesc() {
		return fullDrugDesc;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	public String getSupplierCode() {
		return supplierCode;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setMedOrderItemNum(Integer medOrderItemNum) {
		MedOrderItemNum = medOrderItemNum;
	}

	public Integer getMedOrderItemNum() {
		return MedOrderItemNum;
	}

	public void setCapdItemItemNum(Integer capdItemItemNum) {
		this.capdItemItemNum = capdItemItemNum;
	}

	public Integer getCapdItemItemNum() {
		return capdItemItemNum;
	}

	public void setOrderQty(BigDecimal orderQty) {
		this.orderQty = orderQty;
	}

	public BigDecimal getOrderQty() {
		return orderQty;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public Integer getItemNum() {
		return itemNum;
	}

	public void setGenVoucherFlag(Boolean genVoucherFlag) {
		this.genVoucherFlag = genVoucherFlag;
	}

	public Boolean getGenVoucherFlag() {
		return genVoucherFlag;
	}

	public void setMarkUpdate(boolean markUpdate) {
		this.markUpdate = markUpdate;
	}

	public boolean isMarkUpdate() {
		return markUpdate;
	}

	public void setVoucherNumList(List<String> voucherNumList) {
		this.voucherNumList = voucherNumList;
	}

	public List<String> getVoucherNumList() {
		if ( voucherNumList == null ) {
			voucherNumList = new ArrayList<String>();
		}
		return voucherNumList;
	}


}
