package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.List;

public class MdsExceptionRptContainer implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<MdsExceptionRpt> mdsExceptionRptList;
	
	private MdsExceptionOverrideRpt mdsExceptionOverrideRpt;
	
	private boolean isDdiRpt = false;

	public MdsExceptionRptContainer() {
		super();
	}
	
	public MdsExceptionRptContainer(List<MdsExceptionRpt> mdsExceptionRptList) {
		this();
		this.mdsExceptionRptList = mdsExceptionRptList;
	}
	
	public MdsExceptionRptContainer(MdsExceptionOverrideRpt mdsExceptionOverrideRpt, boolean isDdiRpt) {
		this();
		this.mdsExceptionOverrideRpt = mdsExceptionOverrideRpt;
		this.isDdiRpt = isDdiRpt;
	}

	public MdsExceptionOverrideRpt getMdsExceptionOverrideRpt() {
		return mdsExceptionOverrideRpt;
	}

	public void setMdsExceptionOverrideRpt(MdsExceptionOverrideRpt mdsExceptionOverrideRpt) {
		this.mdsExceptionOverrideRpt = mdsExceptionOverrideRpt;
	}

	public boolean isDdiRpt() {
		return isDdiRpt;
	}

	public void setDdiRpt(boolean isDdiRpt) {
		this.isDdiRpt = isDdiRpt;
	}

	public List<MdsExceptionRpt> getMdsExceptionRptList() {
		return mdsExceptionRptList;
	}

	public void setMdsExceptionRptList(List<MdsExceptionRpt> mdsExceptionRptList) {
		this.mdsExceptionRptList = mdsExceptionRptList;
	}
}
