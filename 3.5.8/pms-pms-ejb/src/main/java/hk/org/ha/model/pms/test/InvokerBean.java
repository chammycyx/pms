package hk.org.ha.model.pms.test;

import hk.org.ha.fmk.pms.remote.marshall.EclipseLinkXStreamMarshaller;
import hk.org.ha.fmk.pms.sys.SysProfile;
import hk.org.ha.fmk.pms.web.MeasureCalls;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;

import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.ClassUtils;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@Stateless
@Name("invoker")
@MeasureCalls
public class InvokerBean implements InvokerLocal {

	private static final String ARG = "arg";
	private static final String BEAN_NAME = "beanName";
	private static final String METHOD_NAME = "methodName";
	
	private EclipseLinkXStreamMarshaller xmlMarshaller = new EclipseLinkXStreamMarshaller();
	
	@In
	private FacesContext facesContext;	
	
	@In
	private SysProfile sysProfile;
	
	@Override
	@SuppressWarnings("unchecked")
	public void invoke() throws IOException {		
        HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();
        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
        	
		Writer writer = response.getWriter();
		String result = invoke((Map<String, String[]>) request.getParameterMap());
		writer.write(result);
		
        facesContext.responseComplete();
	}
	
	@SuppressWarnings("unchecked")
    private String invoke(Map<String, String[]> parameterMap) {
		
		if (!sysProfile.isDevelopment()) {
			return "ERROR : Not in development mode, please set 'running-mode=dev' under components.properties";
		}
		
		try {

			if (!parameterMap.containsKey(BEAN_NAME)) {
				return String.format("ERROR : Missing parameter '%s'.", BEAN_NAME);
			}		
			String beanName = parameterMap.get(BEAN_NAME)[0];
			
			if (!parameterMap.containsKey(METHOD_NAME)) {
				return String.format("ERROR : Missing parameter '%s'.", METHOD_NAME);
			}		
			String methodName = parameterMap.get(METHOD_NAME)[0];
			
			Object object = Component.getInstance(beanName);		
			if (object == null) {
				return String.format("ERROR : Bean %s not found.", beanName);
			}
			
			Class clazz = object.getClass();
			
			int maxArgNo = -1;
			for (String paramName : parameterMap.keySet()) {
				if (paramName.startsWith(ARG) && NumberUtils.isDigits(paramName.substring(3))) {
					int currentArgNo = Integer.parseInt(paramName.substring(3));
					if (currentArgNo > maxArgNo) {
						maxArgNo = currentArgNo;
					}
				}
			}

			Method method = null;
			for (Method m : clazz.getMethods()) {
				if (methodName.equals(m.getName()) && m.getParameterTypes().length == maxArgNo + 1) {
					method = m;
					break;
				}
			}
			
			if (method == null) {
				return String.format("ERROR : Method %s arg %d not found.", methodName, maxArgNo);
			}
			
			Class paramClasses[] = method.getParameterTypes();		
			Object paramObjects[] = new Object[paramClasses.length];
			for (int i = 0; i < paramClasses.length; i++) {
				
				String argKey = ARG + i;
				String paramValue = null;
				if (parameterMap.containsKey(argKey)) {
					paramValue = parameterMap.get(argKey)[0];
				} else {
					return String.format("ERROR : Cannot found '%s' for %d %s", argKey, i, paramClasses[i].getName());
				}
				
				if (ClassUtils.isPrimitiveOrWrapper(paramClasses[i])) {
					if (paramClasses[i].isPrimitive() || !StringUtils.isEmpty(paramValue)) {
						paramObjects[i] = toObject(paramClasses[i], paramValue);
					}
				} else if (paramClasses[i] == String.class) {
					paramObjects[i] = paramValue;
				} else {
					Object paramObject = null;
					if (!paramClasses[i].getSimpleName().equals(paramValue)) {
						return String.format("ERROR : Class simple name not match %s for %d %s", paramValue, i, paramClasses[i].getSimpleName());
					}
					try {
						paramObject = paramClasses[i].getConstructor((Class[]) null).newInstance((Object[]) null);
					} catch (Exception e) {
						return String.format("ERROR : Cannot create parameter object %d %s", i, paramClasses[i].getName());
					}
					
					for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
						if (entry.getKey().startsWith(argKey) && entry.getKey().indexOf(".") != -1) {
							String[] property = entry.getKey().split("\\.");
							if (!StringUtils.isEmpty(property[1])) {							
								
								try {
									BeanUtils.setProperty(paramObject, property[1], entry.getValue()[0]);
								} catch (Exception e) {
									return String.format("ERROR : Cannot set %s property for value %s on parameter object %d %s", property[1], entry.getValue()[0], i, paramClasses[i].getName());
								}
							}
						}
					}
					paramObjects[i] = paramObject;				
				}
			}
			
			Object result = null;
			try {
				result = method.invoke(object, paramObjects);
			} catch (Exception e) {
				return String.format("ERROR : Cannot invoke %s with %s\n<pre>\n%s\n</pre>", method, Arrays.asList(paramObjects), 
						printStackTrace(e));
			}
			
			return xmlMarshaller.toXML(result);
			
		} catch (Exception e) {
			return String.format("<pre>\n%s\n</pre>", 
					printStackTrace(e));
		}
	}
	
	private Object toObject(Class<?> clazz, String value) {
		if (Boolean.class == clazz || Boolean.TYPE == clazz)
			return Boolean.parseBoolean(value);
		if (Byte.class == clazz || Byte.TYPE == clazz)
			return Byte.parseByte(value);
		if (Short.class == clazz || Short.TYPE == clazz)
			return Short.parseShort(value);
		if (Integer.class == clazz || Integer.TYPE == clazz)
			return Integer.valueOf(value);
		if (Long.class == clazz || Long.TYPE == clazz)
			return Long.parseLong(value);
		if (Float.class == clazz || Float.TYPE == clazz)
			return Float.parseFloat(value);
		if (Double.class == clazz || Double.TYPE == clazz)
			return Double.parseDouble(value);
		return value;
	}	

	private String printStackTrace(Exception e) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		return sw.toString();
	}	
}
