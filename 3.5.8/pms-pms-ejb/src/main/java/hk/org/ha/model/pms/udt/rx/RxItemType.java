package hk.org.ha.model.pms.udt.rx;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum RxItemType implements StringValuedEnum {
	
	Oral("O", "Oral"),
	Injection("I", "Injection"),
	Iv("V", "IV"),
	Capd("C", "CAPD");
	
    private final String dataValue;
    private final String displayValue;
    
	private RxItemType(String dataValue, String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
    
	@Override
	public String getDataValue() {
		return dataValue;
	}

	@Override
	public String getDisplayValue() {
		return displayValue;
	}
	
    public static RxItemType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(RxItemType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<RxItemType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<RxItemType> getEnumClass() {
    		return RxItemType.class;
    	}
    }
}
