package hk.org.ha.model.pms.biz.inbox;

import hk.org.ha.model.pms.udt.medprofile.MedProfileStatAdminType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatOrderType;

import java.util.Date;





public class CategoryCount {

	private MedProfileStatAdminType adminType;
	
	private MedProfileStatOrderType orderType;
	
	private Long count;
	
	private Long statCount;
	
	private Long pendingCount;
	
	private Long suspendCount;
	
	private Long overridePendingCount;
	
	private Date firstDueDate;

	public CategoryCount(MedProfileStatAdminType adminType, MedProfileStatOrderType orderType,
			Long count, Long statCount, Long pendingCount, Long suspendCount, Long overridePendingCount, Date firstDueDate) {
		super();
		this.adminType = adminType;
		this.orderType = orderType;
		this.count = count;
		this.statCount = statCount;
		this.pendingCount = pendingCount;
		this.suspendCount = suspendCount;
		this.overridePendingCount = overridePendingCount;
		this.firstDueDate = firstDueDate;
	}
	
	public CategoryCount(MedProfileStatAdminType adminType,
			Long count) {
		super();
		this.adminType = adminType;
		this.count = count;
		this.statCount = Long.valueOf(0);
		this.pendingCount = Long.valueOf(0);
		this.suspendCount = Long.valueOf(0);
		this.overridePendingCount = Long.valueOf(0);
	}

	public MedProfileStatAdminType getAdminType() {
		return adminType;
	}

	public void setAdminType(MedProfileStatAdminType adminType) {
		this.adminType = adminType;
	}

	public MedProfileStatOrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(MedProfileStatOrderType orderType) {
		this.orderType = orderType;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Long getStatCount() {
		return statCount;
	}

	public void setStatCount(Long statCount) {
		this.statCount = statCount;
	}
	
	public Long getPendingCount() {
		return pendingCount;
	}

	public void setPendingCount(Long pendingCount) {
		this.pendingCount = pendingCount;
	}

	public Long getSuspendCount() {
		return suspendCount;
	}

	public void setSuspendCount(Long suspendCount) {
		this.suspendCount = suspendCount;
	}
	
	public Long getOverridePendingCount() {
		return overridePendingCount;
	}

	public void setOverridePendingCount(Long overridePendingCount) {
		this.overridePendingCount = overridePendingCount;
	}

	public Date getFirstDueDate() {
		return firstDueDate;
	}

	public void setFirstDueDate(Date firstDueDate) {
		this.firstDueDate = firstDueDate;
	}
}
