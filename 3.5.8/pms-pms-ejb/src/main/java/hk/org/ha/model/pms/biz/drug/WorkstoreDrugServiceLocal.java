package hk.org.ha.model.pms.biz.drug;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.model.pms.vo.rx.DhRxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;

import javax.ejb.Local;

@Local
public interface WorkstoreDrugServiceLocal {
	WorkstoreDrug retrieveWorkstoreDrug(String itemCode);
	WorkstoreDrug retrieveWorkstoreDrugForOrderEdit(String displayName, String itemCode);
	WorkstoreDrug retrieveWorkstoreDrugForMpOrderEdit(RxItem rxItem, String itemCode);
	WorkstoreDrug retrieveWorkstoreDrugForDhOrderEdit(DhRxDrug dhRxDrug, String itemCode);
	void destroy();
}
