package hk.org.ha.model.pms.biz.refill;

import static hk.org.ha.model.pms.prop.Prop.HOSPITAL_NAMECHI;
import static hk.org.ha.model.pms.prop.Prop.HOSPITAL_NAMEENG;
import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_HOSPITAL_NAMEENG;
import static hk.org.ha.model.pms.prop.Prop.REFILL_COUPON_CHARGEABLEITEM_CHKBOX_VISIBLE;
import static hk.org.ha.model.pms.prop.Prop.REFILL_COUPON_REMARKCHI;
import static hk.org.ha.model.pms.prop.Prop.REFILL_COUPON_REMARKENG;
import static hk.org.ha.model.pms.prop.Prop.REFILL_DRS_COUPON_REMARKCHI;
import static hk.org.ha.model.pms.prop.Prop.REFILL_DRS_COUPON_REMARKENG;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.label.InstructionBuilder;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrintLabelManagerLocal;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.biz.report.MpTrxRptBuilderLocal;
import hk.org.ha.model.pms.corp.cache.DmFormCacher;
import hk.org.ha.model.pms.dms.persistence.DmDailyFrequency;
import hk.org.ha.model.pms.dms.persistence.DmForm;
import hk.org.ha.model.pms.dms.persistence.DmSupplFrequency;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.persistence.disp.RefillScheduleItem;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.persistence.reftable.RefillConfig;
import hk.org.ha.model.pms.udt.disp.DiscontinueStatus;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.refill.DocType;
import hk.org.ha.model.pms.udt.refill.RefillScheduleItemStatus;
import hk.org.ha.model.pms.udt.refill.RefillScheduleRefillStatus;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.vetting.NameType;
import hk.org.ha.model.pms.vo.label.CouponReminderLabel;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.PrinterSelect;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.report.RefillCoupon;
import hk.org.ha.model.pms.vo.report.RefillCouponDtl;
import hk.org.ha.model.pms.vo.report.RefillCouponFtr;
import hk.org.ha.model.pms.vo.report.RefillCouponHdr;
import hk.org.ha.model.pms.vo.report.SfiRefillCoupon;
import hk.org.ha.model.pms.vo.report.SfiRefillCouponDtl;
import hk.org.ha.model.pms.vo.report.SfiRefillCouponFtr;
import hk.org.ha.model.pms.vo.report.SfiRefillCouponHdr;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.RxItem;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;


@AutoCreate
@Stateless
@Name("refillCouponRenderer")
@MeasureCalls
public class RefillCouponRendererBean implements RefillCouponRendererLocal{
	
	@In
	private PrintAgentInf printAgent;	
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private InstructionBuilder instructionBuilder;
	
	@In
	private PrintLabelManagerLocal printLabelManager;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private RefillConfig vettingRefillConfig;
	
	@In
	private MpTrxRptBuilderLocal mpTrxRptBuilder;
	
	private static final String SPACE = " ";
	private static final String STANDARD_REFILL_COUPON_RPTNAME = "RefillCoupon";
	private static final String PHARM_REFILL_COUPON_RPTNAME = "PharmClinicRefillCoupon";
	private static final String SFI_REFILL_COUPON_RPTNAME = "SfiRefillCoupon";
	private static final String DAYDESC_ARRAY[] = {"sunday","monday","tuesday","wednesday","thursday","friday","saturday"};
	private static final String LEFT_BRACKET = "(";
	private static final String RIGHT_BRACKET = ")";
	private static final String STANDARD_REMINDER_DOCNAME = "RefillCouponReminder";
	private static final String SFI_REMINDER_DOCNAME = "SfiRefillCouponReminder";
	private static final String DISP_LABEL_DOCNAME = "DispLabel";

	private DateFormat dateTimeFormat = new SimpleDateFormat("ddMMyyyy"); 
	private DecimalFormat decimalFormat = new DecimalFormat("#######.###");
		
	private RefillCouponDtlComparator refillCouponDtlComparator = new RefillCouponDtlComparator();

	private void prepareRefillCouponReminderPrintJob(List<RenderAndPrintJob> printJobList, RefillSchedule currentRs, RefillSchedule nextRs) {
		CouponReminderLabel couponReminderLabel = new CouponReminderLabel();
		
		Ticket refillTicket = currentRs.getTicket();		
		if( refillTicket != null ){
			couponReminderLabel.setTicketNum(refillTicket.getTicketNum());
			couponReminderLabel.setDispDate(refillTicket.getTicketDate());
			couponReminderLabel.setHospName(LABEL_DISPLABEL_HOSPITAL_NAMEENG.get());			
		}
		couponReminderLabel.setUpdateDate(currentRs.getUpdateDate());
		
		PharmOrder pharmOrder = currentRs.getPharmOrder();
		
		if( pharmOrder.getMedCase() != null ){
			couponReminderLabel.setCaseNum(pharmOrder.getMedCase().getCaseNum());
		}
		if( couponReminderLabel.getCaseNum() == null ){
			couponReminderLabel.setCaseNum(StringUtils.EMPTY);
		}

		couponReminderLabel.setOrderNum( nextRs.getRefillCouponNum().substring(0, 12) );
		couponReminderLabel.setPatName(pharmOrder.getPatient().getName());
		couponReminderLabel.setSfiRefillNum(nextRs.getRefillCouponNum());
		
		String refillReminderType = STANDARD_REMINDER_DOCNAME;
		
		if( currentRs.getDocType()==DocType.Sfi ){
			refillReminderType = SFI_REMINDER_DOCNAME;
		}

		PrinterSelect reminderPrinterSelect = null;
		
		List<RenderAndPrintJob> reversePrintJobList = new ArrayList<RenderAndPrintJob>();
		reversePrintJobList.addAll(printJobList);
		Collections.reverse(reversePrintJobList);

		for( RenderAndPrintJob renderPrintJob : reversePrintJobList ){
			if( STANDARD_REMINDER_DOCNAME.equals(renderPrintJob.getDocName()) ||
				SFI_REMINDER_DOCNAME.equals(renderPrintJob.getDocName()) ||
				DISP_LABEL_DOCNAME.equals(renderPrintJob.getDocName()) ){
				reminderPrinterSelect = new PrinterSelect(renderPrintJob.getPrinterSelect());
				break;
			}
		}
		
		if( reminderPrinterSelect == null ){
			reminderPrinterSelect = printLabelManager.retrieveDefaultPrinterSelectForCouponReminder(currentRs.getDispOrder().getUrgentFlag());			
		}
		
		printJobList.add(new RenderAndPrintJob(
				refillReminderType,
				new PrintOption(),
				reminderPrinterSelect,
	    	    Arrays.asList(couponReminderLabel),
	    	    "[" + refillReminderType + "] SfiRefillNum:" + couponReminderLabel.getSfiRefillNum() +
	    	    ", DispDate:" + dateTimeFormat.format(couponReminderLabel.getDispDate()) +
	    	    ", TicketNum:" + couponReminderLabel.getTicketNum()));		
	}
	
	public void printStandardRefillCoupon(RefillSchedule refillScheduleIn, boolean isReprint, PrintLang printLang, Integer numOfCopies){
		
		List<RenderAndPrintJob> printJobList = new ArrayList<RenderAndPrintJob>();
		
		this.prepareStandardRefillCouponPrintJob(printJobList, refillScheduleIn, isReprint, printLang, numOfCopies);
		
		printAgent.renderAndPrint(printJobList);
	}
	
	@Override
	public void prepareStandardRefillCouponPrintJob(List<RenderAndPrintJob> printJobList, RefillSchedule refillScheduleIn, boolean isReprint, PrintLang printLang, Integer numOfCopies) {
		this.prepareStandardRefillCouponPrintJobWithReminderFlag(printJobList, refillScheduleIn, isReprint, printLang, numOfCopies, true);
	}


	private boolean prepareStandardRefillCouponPrintJobWithReminderFlag(List<RenderAndPrintJob> printJobList, RefillSchedule refillScheduleIn, boolean isReprint, PrintLang printLang, Integer numOfCopies, boolean printedReminder) {
		RefillSchedule initRefillSchedule = refillScheduleIn;
		RefillSchedule currentRefillSchedule = refillScheduleIn; 		
		RefillSchedule nextRefillSchedule = null;	
		
		Integer numOfPrintOut = vettingRefillConfig.getCouponPrintCopies();
		PrintLang couponPrintLang = printLang;
		
		List<RefillSchedule> refillScheduleList = retrieveRefillScheduleForRefillCoupon(refillScheduleIn, isReprint);
		
		if( isReprint ){
			for( RefillSchedule refillSchedule : refillScheduleList){
				if( Integer.valueOf(1).equals(refillSchedule.getRefillNum()) ){		
					initRefillSchedule = refillSchedule;
				}
				
				if( refillScheduleIn.getRefillNum().equals(refillSchedule.getRefillNum()) ){			
					currentRefillSchedule = refillSchedule;
				}
				
				if( Integer.valueOf((refillScheduleIn.getRefillNum()+1)).equals((refillSchedule.getRefillNum())) ){
					nextRefillSchedule = refillSchedule;
				}
			}
		} else {
			for( RefillSchedule refillSchedule : refillScheduleList){
				if( Integer.valueOf(1).equals(refillSchedule.getRefillNum()) ){
					initRefillSchedule = refillSchedule;
				}
			
				if(  RefillScheduleRefillStatus.Current == refillSchedule.getRefillStatus() ){
					currentRefillSchedule = refillSchedule;
				}
			
				if( RefillScheduleRefillStatus.Next == refillSchedule.getRefillStatus() ){
					nextRefillSchedule = refillSchedule;
				}
			}
		}
		
		if( nextRefillSchedule == null ){
			return false;
		}
		
		if( !isReprint && nextRefillSchedule.getPrintFlag() ){
			return false;
		}
		
		if( !isReprint ){
			numOfPrintOut = nextRefillSchedule.getNumOfCoupon();
		}
				
		//override NumOfCopies
		if( numOfCopies != null ){
			numOfPrintOut = numOfCopies;
		}
		
		//get printLang from NextRefillSchedule if input is null
		if( couponPrintLang == null ){
			couponPrintLang = nextRefillSchedule.getPrintLang();
		}

		List<RefillCoupon> refillCouponList = convertRefillScheduleToRefillCoupon(initRefillSchedule, currentRefillSchedule, nextRefillSchedule, isReprint);

		if( !refillCouponList.isEmpty() ){

			if( !isReprint && currentRefillSchedule.getPrintReminderFlag() && !printedReminder ){
				this.prepareRefillCouponReminderPrintJob(printJobList, currentRefillSchedule, nextRefillSchedule);
			}
			
			List<RefillCoupon> refillCouponCopyList = new ArrayList<RefillCoupon>();
			if( !isReprint && numOfPrintOut != null && numOfPrintOut > 1 ){
				refillCouponCopyList = convertRefillScheduleToRefillCoupon(initRefillSchedule, currentRefillSchedule, nextRefillSchedule, true);
			}
			this.sentStandardRefillCouponListToPrinter(printJobList, refillCouponList, refillCouponCopyList, isReprint, couponPrintLang, numOfPrintOut);
		}
		
		if(!isReprint){
			RefillSchedule updatePrintRs = em.find(RefillSchedule.class, nextRefillSchedule.getId());
			updatePrintRs.setPrintFlag(true);
		}
		
		if( refillCouponList.isEmpty() ){
			return false;
		}		
		return true;
	}
	
	public void printSfiRefillCoupon(RefillSchedule refillScheduleIn, boolean isReprint, PrintLang printLang){
		
		List<RenderAndPrintJob> printJobList = new ArrayList<RenderAndPrintJob>();

		this.prepareSfiRefillCouponPrintJob(printJobList, refillScheduleIn, isReprint, printLang);
		
		printAgent.renderAndPrint(printJobList);
	}
		
	@SuppressWarnings("unchecked")
	public void prepareSfiRefillCouponPrintJob(List<RenderAndPrintJob> printJobList, RefillSchedule refillScheduleIn, boolean isReprint, PrintLang printLang) {
		
		RefillSchedule initRefillSchedule = refillScheduleIn;
		RefillSchedule currentRefillSchedule = refillScheduleIn; 		
		RefillSchedule nextRefillSchedule = null;
				
		List<RefillSchedule> refillScheduleList = (List<RefillSchedule>) em.createQuery(
				"select o from RefillSchedule o" +  // 20120303 index check : RefillSchedule.pharmOrder : FK_REFILL_SCHEDULE_01
				" where o.pharmOrder.id = :pharmOrderId" +
				" and (o.refillNum = 1 or o.refillStatus in :refillStatusList)" +
				" and o.groupNum = :groupNum and o.docType = :docType")
				.setParameter("pharmOrderId", refillScheduleIn.getPharmOrder().getId() )
				.setParameter("refillStatusList", Arrays.asList(RefillScheduleRefillStatus.Current, RefillScheduleRefillStatus.Next))
				.setParameter("groupNum", refillScheduleIn.getGroupNum())
				.setParameter("docType", DocType.Sfi)
				.getResultList();
		
		for( RefillSchedule refillSchedule : refillScheduleList){
			if( refillSchedule.getRefillNum() == 1 ){		
				initRefillSchedule = refillSchedule;
			}
		
			if(  RefillScheduleRefillStatus.Current.equals(refillSchedule.getRefillStatus()) ){			
				currentRefillSchedule = refillSchedule;
			}
		
			if(  RefillScheduleRefillStatus.Next.equals(refillSchedule.getRefillStatus()) ){
				nextRefillSchedule = refillSchedule;
			}
		}
		
		if( nextRefillSchedule == null ){
			return;
		}
				
		if( !isReprint && nextRefillSchedule.getPrintFlag() ){
			return;
		}
		
		if( !isReprint && skipPrintingSfiRefillCoupon(nextRefillSchedule, currentRefillSchedule.getDispOrder()) ){
			return;
		}
		
		if( !isReprint && currentRefillSchedule.getPrintReminderFlag() ){
			this.prepareRefillCouponReminderPrintJob(printJobList, currentRefillSchedule, nextRefillSchedule);
		}
		
		List<SfiRefillCoupon> refillCouponList = convertRefillScheduleToSfiRefillCoupon(initRefillSchedule, currentRefillSchedule, nextRefillSchedule, isReprint);
		
		this.sentSfiRefillCouponListToPrinter(printJobList, refillCouponList, printLang);
		
		if(!isReprint){
			RefillSchedule updatePrintRs = em.find(RefillSchedule.class, nextRefillSchedule.getId());
			updatePrintRs.setPrintFlag(true);
		}
	}
	
	private Boolean skipPrintingSfiRefillCoupon(RefillSchedule nextRefillSchedule, DispOrder dispOrder){
		if( nextRefillSchedule.getRefillNum() == 2 ){
			for(RefillScheduleItem refillScheduleItem : nextRefillSchedule.getRefillScheduleItemList()){
				if( dispOrder.getPharmOrder().getPrivateFlag() ){
					return Boolean.FALSE;
				}else if( refillScheduleItem.getPharmOrderItem().getIssueQty().compareTo( BigDecimal.valueOf(refillScheduleItem.getCalQty())) != 0 ){
					return Boolean.FALSE;
				}
			}
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	private void sentStandardRefillCouponListToPrinter(List<RenderAndPrintJob> printJobList, List<RefillCoupon> standardRefillCouponList,List<RefillCoupon> standardRefillCouponCopyList, boolean isReprint, PrintLang printLang, Integer numOfCopy){
		if( standardRefillCouponList == null || standardRefillCouponList.isEmpty() ){
			return;
		}
		
		PrintOption printOption = new PrintOption();
		printOption.setPrintLang(printLang);
		
		Map<String, Object> params = new HashMap<String, Object>();	
		params.put("SUBREPORT_DIR", "report/RefillCoupon/");
		
		String couponType = STANDARD_REFILL_COUPON_RPTNAME;
		if( standardRefillCouponList.get(0).getRefillCouponHdr().getNextApptDate() != null ){				
			couponType = PHARM_REFILL_COUPON_RPTNAME;
		}

		PrinterSelect refillCouponPs = printerSelector.retrievePrinterSelect(PrintDocType.RefillCoupon);
		
		if( isReprint ){
			
			if( numOfCopy != null && numOfCopy > 1 ){
				refillCouponPs.setCopies(numOfCopy);
			}
			
			printJobList.add(new RenderAndPrintJob(
						couponType,
						printOption,
						refillCouponPs,
			    	    params,
			    	    standardRefillCouponList,
			    	    "[RefillCoupon] " +
						"isReprint:" + isReprint
						));
		}else{
			
			printJobList.add(new RenderAndPrintJob(
						couponType,
						printOption,
						refillCouponPs,
			    	    params,
			    	    standardRefillCouponList,
			    	    "[RefillCoupon] " +
						"isReprint:" + isReprint
						));
			
				if( standardRefillCouponCopyList == null || standardRefillCouponCopyList.isEmpty() ){
					return;
				}

				try {
					PrinterSelect refillCouponCopyPs = (PrinterSelect) BeanUtils.cloneBean(refillCouponPs);
					refillCouponCopyPs.setCopies(numOfCopy-1);
					printJobList.add(new RenderAndPrintJob(
							couponType,
							printOption,
							refillCouponCopyPs,
				    	    params,
				    	    standardRefillCouponCopyList,
				    	    "[RefillCoupon] " +
							"isReprint:" + isReprint
							));					
				} catch (Exception e) {
					throw new RuntimeException(e);
				}		
		}		
	}
		
	private void sentSfiRefillCouponListToPrinter(List<RenderAndPrintJob> printJobList, List<SfiRefillCoupon> sfiRefillCouponList, PrintLang printLang ){
		if( sfiRefillCouponList == null || sfiRefillCouponList.isEmpty() ){
			return;
		}
		
		PrintOption printOption = new PrintOption();
		printOption.setPrintLang(printLang);
		
		PrinterSelect sfiRefillCouponPs = printerSelector.retrievePrinterSelect(PrintDocType.SfiRefillCoupon);
		
		printJobList.add(new RenderAndPrintJob(
										SFI_REFILL_COUPON_RPTNAME,
										printOption,
										sfiRefillCouponPs,
										sfiRefillCouponList,
							    	    "[SfiRefillCoupon] "
										));
	}
		
	private List<SfiRefillCoupon> convertRefillScheduleToSfiRefillCoupon(RefillSchedule initRsIn, RefillSchedule currRsIn, RefillSchedule nextRsIn, boolean isReprint){
		SfiRefillCouponHdr sfiRcHdr = constructSfiRefillCouponHdr(initRsIn, currRsIn, nextRsIn, isReprint);
		
		List<SfiRefillCouponDtl> sfiRcDtlList = constructSfiRefillCouponDtlList(nextRsIn);
		
		SfiRefillCouponFtr sfiRcFtr = new SfiRefillCouponFtr();
		sfiRcFtr.setLastUpdateTime( nextRsIn.getUpdateDate() );
		
		if( currRsIn.getRefillScheduleItemList().get(0).getNextHandleExemptCode() != null ){
			sfiRcFtr.setNextExemptReason( currRsIn.getRefillScheduleItemList().get(0).getNextHandleExemptCode() );
			sfiRcFtr.setUserId(currRsIn.getRefillScheduleItemList().get(0).getHandleExemptUser());
		}
		sfiRcFtr.setRefillCouponNum(nextRsIn.getRefillCouponNum());
		sfiRcFtr.setRefillCouponRemarkChi(REFILL_COUPON_REMARKCHI.get());
		sfiRcFtr.setRefillCouponRemarkEng(REFILL_COUPON_REMARKENG.get());
		
		SfiRefillCoupon sfiRefillCoupon = new SfiRefillCoupon();
		sfiRefillCoupon.setSfiRefillCouponHdr(sfiRcHdr);
		sfiRefillCoupon.setSfiRefillCouponDtlList(sfiRcDtlList);
		sfiRefillCoupon.setSfiRefillCouponFtr(sfiRcFtr);
		
		return Arrays.asList(sfiRefillCoupon);
	}
	
	private SfiRefillCouponHdr constructSfiRefillCouponHdr(RefillSchedule initRsIn, RefillSchedule currRsIn, RefillSchedule nextRsIn, boolean isReprint) {
		PharmOrder pharmOrder = nextRsIn.getPharmOrder();		
		Patient patient = pharmOrder.getPatient();
		PharmOrderItem pharmOrderItem = nextRsIn.getRefillScheduleItemList().get(0).getPharmOrderItem();
		Ticket currTicket = currRsIn.getTicket();
		Ticket initTicket = initRsIn.getTicket();		
		
		SfiRefillCouponHdr sfiRcHdr = new SfiRefillCouponHdr(); 
		
		sfiRcHdr.setHospitalNameEng(HOSPITAL_NAMEENG.get());
		sfiRcHdr.setHospitalNameChi(HOSPITAL_NAMECHI.get());
		
		sfiRcHdr.setPatName( patient.getName() );
		sfiRcHdr.setPatNameChi( patient.getNameChi() );		
		if( StringUtils.equals(patient.getHkid(), "?") ){
			sfiRcHdr.setHkid( StringUtils.EMPTY );
		}else{
			sfiRcHdr.setHkid( patient.getHkid() );
		}
		sfiRcHdr.setSex( patient.getSex().getDataValue() );

		String age = patient.getAge();
		String dateUnit = patient.getDateUnit(); 
		if( age != null ){
			StringBuilder ageUnit = new StringBuilder();
			ageUnit.append(age);
			if ("day(s)".equals(dateUnit)) {
				ageUnit.append("d");
			}else if ("month(s)".equals(dateUnit)) {
				ageUnit.append("m");
			}else if ("year(s)".equals(dateUnit)) {
				ageUnit.append("y");
			}
			sfiRcHdr.setAge( ageUnit.toString() );
		}
		
		if( pharmOrder.getMedCase() != null ){
			sfiRcHdr.setSpecCode( pharmOrder.getMedCase().getPasSpecCode() );
			sfiRcHdr.setCaseNum( pharmOrder.getMedCase().getCaseNum() );
		}		
		if( sfiRcHdr.getSpecCode() == null ){
			sfiRcHdr.setSpecCode(pharmOrder.getSpecCode());
		}
		
		pharmOrderItem.loadDmInfo();
		pharmOrderItem.getMedOrderItem().loadDmInfo();
		
		if ( pharmOrderItem.getMedOrderItem().getRxItem().isIvRxDrug() || pharmOrderItem.getMedOrderItem().getRxItem().isInjectionRxDrug() ) {
			sfiRcHdr.setInfusionFlag(true);
			RxItem rxItem = pharmOrderItem.getMedOrderItem().getRxItem();			
			if( pharmOrderItem.getMedOrderItem().getRxItem().getDrugOrderXml() != null ){				
				sfiRcHdr.setInfusionInstruction(mpTrxRptBuilder.convertDrugOrderXml(rxItem.getDrugOrderXml(), rxItem.getInterfaceVersion()));
			}else{
				sfiRcHdr.setInfusionInstruction(mpTrxRptBuilder.convertDrugOrderTlf(rxItem.getFormattedDrugOrderTlf()));	
			}
		} else {
			sfiRcHdr.setInfusionFlag(false);
			sfiRcHdr.setMedOrderItemDesc(constructMedOrderItemDesc(pharmOrderItem.getMedOrderItem()));
			sfiRcHdr.setInstruction(contructMedOrderItemInstruction(pharmOrderItem.getMedOrderItem()));
			sfiRcHdr.setSpecialInstruction( pharmOrderItem.getMedOrderItem().getSpecialInstruction() );
		}
		
		sfiRcHdr.setDispDate(currTicket.getTicketDate());
		sfiRcHdr.setTicketNum(currTicket.getTicketNum());
		sfiRcHdr.setWorkstoreCode(currTicket.getWorkstore().getWorkstoreCode());				

		sfiRcHdr.setOrgDispDate(initTicket.getTicketDate());
		sfiRcHdr.setOrgTicketNum(initTicket.getTicketNum());
		sfiRcHdr.setOrgWorkstoreCode(initTicket.getWorkstore().getWorkstoreCode());
		
		sfiRcHdr.setDoctorCode(pharmOrder.getDoctorCode());
		sfiRcHdr.setRefillDate(nextRsIn.getRefillDate());
		sfiRcHdr.setReprintFlag(isReprint);
		
		return sfiRcHdr;
	}
	
	private List<SfiRefillCouponDtl> constructSfiRefillCouponDtlList(RefillSchedule nextRsIn){
		List<SfiRefillCouponDtl> refillCouponDtlList = new ArrayList<SfiRefillCouponDtl>();
		
		for( RefillScheduleItem nextRsi : nextRsIn.getRefillScheduleItemList() ){
			if( nextRsi.getCalQty() == 0 ){
				continue;
			}
			
			PharmOrderItem pharmOrderItem = nextRsi.getPharmOrderItem();
			
			SfiRefillCouponDtl sfiRcDtl = new SfiRefillCouponDtl();
			sfiRcDtl.setItemCode( pharmOrderItem.getItemCode() );
			sfiRcDtl.setBaseUnit( pharmOrderItem.getBaseUnit() );
			sfiRcDtl.setFullDrugDesc( pharmOrderItem.getFullDrugDesc() );
			
			BigDecimal remainDur = BigDecimal.ONE;
			remainDur = remainDur.multiply(BigDecimal.valueOf(pharmOrderItem.getRegimen().getDurationInDay()));
			remainDur = remainDur.multiply(BigDecimal.valueOf(nextRsi.getRefillQty()));
			remainDur = remainDur.divide(pharmOrderItem.getIssueQty(), 0, RoundingMode.CEILING);
			
			StringBuilder remainDurDesc = new StringBuilder();
			remainDurDesc.append( remainDur.setScale(0, RoundingMode.CEILING) );
			
			if( remainDur.setScale(0, RoundingMode.CEILING).compareTo(BigDecimal.ONE) == 0 ){
				remainDurDesc.append(" DAY");
			}else{
				remainDurDesc.append(" DAYS");
			}
			sfiRcDtl.setRemainDuration(remainDurDesc.toString());
			sfiRcDtl.setRemainQty( nextRsi.getRefillQty() );
			sfiRcDtl.setTotalIssueQty(nextRsi.getPharmOrderItem().getIssueQty().intValue());
			
			refillCouponDtlList.add(sfiRcDtl);
		}
		return refillCouponDtlList;
	}
	
	private List<RefillCoupon> convertRefillScheduleToRefillCoupon(RefillSchedule initRefillSchedule, RefillSchedule currentRefillSchedule, RefillSchedule nextRefillSchedule, boolean isReprint){		
		RefillCouponHdr refillCouponHdr = constructRefillCouponHdr(initRefillSchedule, currentRefillSchedule, nextRefillSchedule, isReprint);
		
		List<RefillCouponDtl> refillCouponDtlList = constructRefillCouponDtl(nextRefillSchedule);
		
		if( refillCouponDtlList.isEmpty() ){
			return new ArrayList<RefillCoupon>();
		}
		
		RefillCouponFtr refillCouponFtr = new RefillCouponFtr();
		refillCouponFtr.setRefillCouponNum(nextRefillSchedule.getRefillCouponNum());
		if(Boolean.TRUE.equals(refillCouponHdr.getDrsFlag())){
			refillCouponFtr.setRefillCouponRemarkEng(REFILL_DRS_COUPON_REMARKENG.get());
			refillCouponFtr.setRefillCouponRemarkChi(REFILL_DRS_COUPON_REMARKCHI.get());
		}else{
			refillCouponFtr.setRefillCouponRemarkEng(REFILL_COUPON_REMARKENG.get());
			refillCouponFtr.setRefillCouponRemarkChi(REFILL_COUPON_REMARKCHI.get());
		}
		refillCouponFtr.setLastUpdateTime( nextRefillSchedule.getUpdateDate() );
		
		RefillCoupon refillCoupon = new RefillCoupon();		
		refillCoupon.setRefillCouponHdr(refillCouponHdr);
		refillCoupon.setRefillCouponDtlList(refillCouponDtlList);
		refillCoupon.setRefillCouponFtr(refillCouponFtr);
		
		return Arrays.asList(refillCoupon);
	}
	
	@SuppressWarnings("unchecked")
	private List<RefillSchedule> retrieveRefillScheduleForRefillCoupon(RefillSchedule refillScheduleIn, boolean isReprint) {
		if( isReprint ){
			return (List<RefillSchedule>) em.createQuery(
					"select o from RefillSchedule o" + // 20120303 index check : RefillSchedule.pharmOrder : FK_REFILL_SCHEDULE_01
					" where o.pharmOrder.id = :pharmOrderId" +
					" and (o.refillNum in :refillNumList)" +
					" and o.groupNum = :groupNum" +
					" and o.docType = :docType")
					.setParameter("pharmOrderId", refillScheduleIn.getPharmOrder().getId() )
					.setParameter("refillNumList", Arrays.asList(1, refillScheduleIn.getRefillNum(), refillScheduleIn.getRefillNum()+1))
					.setParameter("groupNum", refillScheduleIn.getGroupNum())
					.setParameter("docType", DocType.Standard)
					.getResultList();
		} else {
			return (List<RefillSchedule>) em.createQuery(
					"select o from RefillSchedule o" + // 20120303 index check : RefillSchedule.pharmOrder : FK_REFILL_SCHEDULE_01
					" where o.pharmOrder.id = :pharmOrderId" +
					" and (o.refillNum = 1 or o.refillStatus in :refillStatusList)" +
					" and o.groupNum = :groupNum" +
					" and o.docType = :docType")
					.setParameter("pharmOrderId", refillScheduleIn.getPharmOrder().getId() )
					.setParameter("refillStatusList", Arrays.asList(RefillScheduleRefillStatus.Current, RefillScheduleRefillStatus.Next))
					.setParameter("groupNum", refillScheduleIn.getGroupNum())
					.setParameter("docType", DocType.Standard)
					.getResultList();
		}
	}
	
	private RefillCouponHdr constructRefillCouponHdr(RefillSchedule initRefillSchedule,RefillSchedule currentRefillSchedule, RefillSchedule nextRefillSchedule, boolean isReprint) {
		PharmOrder pharmOrder = currentRefillSchedule.getPharmOrder();		
				
		Patient patient = pharmOrder.getPatient();
		
		RefillCouponHdr refillCouponHdr = new RefillCouponHdr();
		refillCouponHdr.setPatName( patient.getName() );
		refillCouponHdr.setPatNameChi( patient.getNameChi());
		if(pharmOrder.getDrsFlag() != null){
			refillCouponHdr.setDrsFlag( pharmOrder.getDrsFlag() );
		}else{
			refillCouponHdr.setDrsFlag( Boolean.FALSE );
		}
		
		
		if( pharmOrder.getMedCase() != null ){
			refillCouponHdr.setCaseNum( pharmOrder.getMedCase().getCaseNum() );
		}
		if( StringUtils.equals(patient.getHkid(), "?") ){
			refillCouponHdr.setHkid( StringUtils.EMPTY );
		}else{
			refillCouponHdr.setHkid( patient.getHkid() );
		}
		refillCouponHdr.setSex( patient.getSex().getDataValue() );
		
		String age = patient.getAge();
		String dateUnit = patient.getDateUnit(); 
		if( age != null ){
			StringBuilder ageUnit = new StringBuilder();
			ageUnit.append(age);
			if ("day(s)".equals(dateUnit)) {
				ageUnit.append("d");
			}else if ("month(s)".equals(dateUnit)) {
				ageUnit.append("m");
			}else if ("year(s)".equals(dateUnit)) {
				ageUnit.append("y");
			}
			refillCouponHdr.setAge( ageUnit.toString() );
		}

		refillCouponHdr.setSpecCode(pharmOrder.getSpecCode());
		refillCouponHdr.setHospitalNameEng(HOSPITAL_NAMEENG.get());
		refillCouponHdr.setHospitalNameChi(HOSPITAL_NAMECHI.get());
		refillCouponHdr.setReprintFlag(isReprint);
		refillCouponHdr.setShowChargeInfo(REFILL_COUPON_CHARGEABLEITEM_CHKBOX_VISIBLE.get());
		
		Ticket currTicket = currentRefillSchedule.getTicket();
		
		if( currTicket != null ){
			refillCouponHdr.setTicketNum(currTicket.getTicketNum());
			refillCouponHdr.setDispDate(currTicket.getTicketDate());
			refillCouponHdr.setWorkstoreCode(currTicket.getWorkstore().getWorkstoreCode());
		}
				
		Ticket initTicket = initRefillSchedule.getTicket();
		if( initTicket != null ){
			refillCouponHdr.setOrgDispDate(initTicket.getTicketDate());
			refillCouponHdr.setOrgTicketNum(initTicket.getTicketNum() + (nextRefillSchedule.getReDispFlag()?"*":"") );
			refillCouponHdr.setOrgWorkstoreCode(initTicket.getWorkstore().getWorkstoreCode());
		}
		
		//NextRefill
		refillCouponHdr.setNumOfGroup(pharmOrder.getNumOfGroup());
		refillCouponHdr.setNumOfRefill(nextRefillSchedule.getNumOfRefill());
		
		refillCouponHdr.setGroupNum(nextRefillSchedule.getGroupNum());
		refillCouponHdr.setRefillNum(nextRefillSchedule.getRefillNum());
		refillCouponHdr.setDoctorCode(pharmOrder.getDoctorCode());
		
		//PharmClinicApptDate
		if( nextRefillSchedule.getNextPharmApptDate() != null ){
			refillCouponHdr.setNextApptDate(nextRefillSchedule.getNextPharmApptDate());
		}else{
			refillCouponHdr.setRefillDate(nextRefillSchedule.getRefillDate());
		}
		return refillCouponHdr;
	}
	
	private List<RefillCouponDtl> constructRefillCouponDtl(RefillSchedule nextRefillSchedule) {
		List<RefillCouponDtl> refillCouponDtlList = new ArrayList<RefillCouponDtl>();
		Map<String,String> pharmOrderFullDrugDescMap = new HashMap<String, String>();		
		
		for(RefillScheduleItem refillScheduleItem : nextRefillSchedule.getRefillScheduleItemList()){
		
			RefillCouponDtl refillCouponDtl  = new RefillCouponDtl();
			PharmOrderItem pharmOrderItem = refillScheduleItem.getPharmOrderItem();
			
			pharmOrderItem.loadDmInfo();
			
			refillCouponDtl.setBaseUnit(refillScheduleItem.getPharmOrderItem().getBaseUnit());
			if( pharmOrderFullDrugDescMap.get(pharmOrderItem.getItemCode()) == null ){
				pharmOrderFullDrugDescMap.put(pharmOrderItem.getItemCode(), pharmOrderItem.getFullDrugDesc());			
			}
			refillCouponDtl.setFullDrugDesc(pharmOrderFullDrugDescMap.get( pharmOrderItem.getItemCode()) );
			refillCouponDtl.setDisplayInstruction(instructionBuilder.buildInstructionListForRefillCoupon(refillScheduleItem));
			
			refillCouponDtl.setIssueQty(refillScheduleItem.getRefillQty());
			refillCouponDtl.setItemNum(refillScheduleItem.getPharmOrderItem().getItemNum());

			boolean isDisConItem = false;
			if( refillScheduleItem.getStatus() == RefillScheduleItemStatus.Discontinue ){
				isDisConItem = true;
			}else if( pharmOrderItem.getMedOrderItem().getDiscontinueStatus() != null && pharmOrderItem.getMedOrderItem().getDiscontinueStatus() != DiscontinueStatus.None  ){
				isDisConItem = true;
			}
			
			if( !isDisConItem && refillCouponDtl.getIssueQty() > 0 ){
				refillCouponDtlList.add(refillCouponDtl);
			}
		}
		
		Collections.sort(refillCouponDtlList, refillCouponDtlComparator);
		
		int refillCouponItemNum = 1;
		for( RefillCouponDtl refillCouponDtlOrder :  refillCouponDtlList){
			refillCouponDtlOrder.setItemNum(refillCouponItemNum);
			refillCouponItemNum++;
		}
		return refillCouponDtlList;
	}

	private String contructMedOrderItemInstruction(MedOrderItem medOrderItem){
		StringBuilder moiInstruction = new StringBuilder();
		List<String> moiInstructionLineList = new ArrayList<String>(); 
		
		medOrderItem.postLoadAll();
		
		DoseGroup doseGroup = medOrderItem.getRegimen().getDoseGroupList().get(0);
		
		String fullRouteDesc = StringUtils.EMPTY;
		StringBuilder dispenseQtyDesc = new StringBuilder();
		
		if( !StringUtils.isEmpty(medOrderItem.getRouteDesc()) ){
			fullRouteDesc = medOrderItem.getRouteDesc()+":";
		}
		
		for (Dose dose : doseGroup.getDoseList()){
			if( dose.getDispQty() != null && dose.getDispQty() > 0 ){
				dispenseQtyDesc.append(", dispense ").append(dose.getDispQty());
				if( dose.getBaseUnit() != null ){
					dispenseQtyDesc.append(SPACE);
					dispenseQtyDesc.append(dose.getBaseUnit());
				}
			}
			
			StringBuilder instructionLine = new StringBuilder();
			
			if( dose.getDosage() != null && BigDecimal.ZERO.compareTo(dose.getDosage()) <0 ){
				instructionLine.append( decimalFormat.format(dose.getDosage()) );
				
				if( StringUtils.isNumeric(dose.getDosageUnit().substring(0,1)) ){
					instructionLine.append(" x");
				}
				instructionLine.append(SPACE);
				instructionLine.append(dose.getDosageUnit());				
			}
			
			//dailyFreqDesc
			instructionLine.append(getDailyFreqDesc(dose));
			
			//SupplFreqDesc
			instructionLine.append(getSupplFreqDesc(dose));
			
			//PrnDesc
			instructionLine.append(getPrnDesc(dose));
			
			moiInstructionLineList.add(instructionLine.toString());			
		}		
		moiInstruction.append(fullRouteDesc);
		
		if( medOrderItem.getDangerDrugFlag() ){
			for( int i = 0; i< moiInstructionLineList.size(); i++ ){
				if( i == 0 ){
					moiInstruction.append(SPACE);
				}else{
					if( i == moiInstructionLineList.size()-1 ){
						moiInstruction.append(",");
					}else{
						moiInstruction.append(" and ");
					}
				}
				moiInstruction.append(moiInstructionLineList.get(i));
			}
		}else{		
			for( int i = 0; i< moiInstructionLineList.size(); i++ ){
				if( i == 0 ){
					moiInstruction.append(SPACE);
				}else{
					if( i == moiInstructionLineList.size()-1 ){
						moiInstruction.append(" and ");
					}else{
						moiInstruction.append(",");						
					}
				}
				moiInstruction.append(moiInstructionLineList.get(i));
			}
		}
		
		if( doseGroup.getDuration() != null ) {
			moiInstruction.append(SPACE);
			moiInstruction.append("for");
			moiInstruction.append(SPACE);
			moiInstruction.append(doseGroup.getDurationInDay());
			moiInstruction.append(SPACE);
			moiInstruction.append("days");			
		}
		
		//Dispense Qty
		moiInstruction.append(dispenseQtyDesc.toString());
		
		return moiInstruction.toString().toLowerCase();
	}
	
	private String getDailyFreqDesc(Dose dose){
		StringBuilder dailyFreqDesc = new StringBuilder();
		if( dose.getDmDailyFrequency() != null ){
			DmDailyFrequency dmDailyFreq = dose.getDmDailyFrequency();
			
			if( dmDailyFreq.getDailyFreqBlk1() != null ){
				dailyFreqDesc.append(SPACE);
				dailyFreqDesc.append(dmDailyFreq.getDailyFreqBlk1());					
			}
			
			if( dmDailyFreq.getDailyFreqVal1() != null ){
				dailyFreqDesc.append(SPACE);
				if( dose.getDailyFreq() != null && dose.getDailyFreq().getParam() != null && dose.getDailyFreq().getParam().length > 0 ) {
					dailyFreqDesc.append(dose.getDailyFreq().getParam()[0]); 
				} else {
					dailyFreqDesc.append( dmDailyFreq.getDailyFreqVal1() );
				}
			}
			
			if( dmDailyFreq.getDailyFreqBlk2() != null ) {
				dailyFreqDesc.append(SPACE);
				dailyFreqDesc.append( dmDailyFreq.getDailyFreqBlk2() );
			}
		}
		return dailyFreqDesc.toString();
	}
	
	private String getSupplFreqDesc(Dose dose){
		StringBuilder supplFreqDesc = new StringBuilder();
		
		if( dose.getDmSupplFrequency() != null ){
			DmSupplFrequency dmSupplFreq = dose.getDmSupplFrequency();
			supplFreqDesc.append(SPACE);
			supplFreqDesc.append(LEFT_BRACKET);
			
			if (dmSupplFreq.getSupplFreqCode() != null && dmSupplFreq.getSupplFreqCode().equals("00021")) {
				supplFreqDesc.append("On Every").append(SPACE);
				
				String param = dose.getSupplFreq().getParam()[0];
				
				boolean firstDay = true;
				for (int i = 0; i < param.length(); i++) {
					if (param.charAt(i) == '1') {
						if (!firstDay) {
							supplFreqDesc.append(", ");
						} else {
							firstDay = false;
						}
						supplFreqDesc.append(DAYDESC_ARRAY[i]);
					}
				}	
			}else{
				if( dmSupplFreq.getSupplFreqBlk1() != null ) {				
					supplFreqDesc.append(dmSupplFreq.getSupplFreqBlk1());						
				}
			
				if( dmSupplFreq.getSupplFreqVal1() != null ) {
					
					if( dose.getSupplFreq() != null && dose.getSupplFreq().getParam() != null && dose.getSupplFreq().getParam().length > 0 ) {
						supplFreqDesc.append(SPACE);
						supplFreqDesc.append(dose.getSupplFreq().getParam()[0]);
						supplFreqDesc.append(SPACE);
					} else {
						supplFreqDesc.append(dmSupplFreq.getSupplFreqVal1());							
					}
				}
				
				if( dmSupplFreq.getSupplFreqBlk2() != null ) {					
					supplFreqDesc.append(dmSupplFreq.getSupplFreqBlk2());	
				}
				
				if( dmSupplFreq.getSupplFreqVal2() != null ) {					
					if( dose.getSupplFreq() != null && dose.getSupplFreq().getParam() != null && dose.getSupplFreq().getParam().length > 1 ) {
						supplFreqDesc.append(SPACE);
						supplFreqDesc.append(dose.getSupplFreq().getParam()[1]);
						supplFreqDesc.append(SPACE);
					} else {
						supplFreqDesc.append(dmSupplFreq.getSupplFreqVal2());
					}
				}
				
				if( dmSupplFreq.getSupplFreqBlk3() != null ) {					
					supplFreqDesc.append(dmSupplFreq.getSupplFreqBlk3());	
				}
			}
			supplFreqDesc.append(RIGHT_BRACKET);				
		}
		return supplFreqDesc.toString();
	}
	
	private String getPrnDesc(Dose dose){
		StringBuilder doseDesc = new StringBuilder();
		if( dose.getPrn() ){
			doseDesc.append(SPACE);
			doseDesc.append( "prn" );
			
			if( dose.getPrnPercent() != null ){
				doseDesc.append(SPACE);
				doseDesc.append(LEFT_BRACKET)
				.append(dose.getPrnPercent().getDisplayValue())
				.append(RIGHT_BRACKET);
			}
		}
		return doseDesc.toString();
	}
	
	private String constructMedOrderItemDesc(MedOrderItem medOrderItem){
		StringBuilder medOrderItemDesc = new StringBuilder();
		String volumeValue = StringUtils.EMPTY;
		String tradeName = StringUtils.EMPTY;
		String extraInfo = StringUtils.EMPTY;
		String sampleTag = StringUtils.EMPTY;		
		
		if( medOrderItem.getVolume() != null ){	
			volumeValue =  decimalFormat.format(medOrderItem.getVolume());
		}
		
		if( NameType.TradeName.equals(medOrderItem.getNameType()) && StringUtils.isNotBlank(medOrderItem.getAliasName()) ){
			tradeName = "(" + medOrderItem.getAliasName() +")";
			extraInfo = medOrderItem.getExtraInfo();
			if( "S".equals(medOrderItem.getFmStatus()) ){
				sampleTag = "<Sample>";
			}
		}else{
			if( StringUtils.isNotBlank(medOrderItem.getExtraInfo()) && !medOrderItem.getExtraInfo().equalsIgnoreCase(medOrderItem.getAliasName()) ){
				extraInfo = medOrderItem.getExtraInfo().toLowerCase();
			}
		}		
		
		medOrderItemDesc.append( medOrderItem.getDisplayName().toUpperCase() );medOrderItemDesc.append( SPACE );
		if( StringUtils.isNotBlank(medOrderItem.getSaltProperty()) ){
			medOrderItemDesc.append( medOrderItem.getSaltProperty().toUpperCase() );medOrderItemDesc.append( SPACE );
		}
		if( StringUtils.isNotBlank(tradeName) ){
			medOrderItemDesc.append( tradeName.toUpperCase() );medOrderItemDesc.append( SPACE );
		}
		String formDescEng = DmFormCacher.instance().getFormByFormCode(medOrderItem.getFormCode()).getFormDescEng();
		if( StringUtils.isNotBlank(formDescEng) ){
			medOrderItemDesc.append(formDescEng);medOrderItemDesc.append( SPACE );				
		}
		if( StringUtils.isNotBlank(medOrderItem.getStrength()) ){
			medOrderItemDesc.append( medOrderItem.getStrength() );medOrderItemDesc.append( SPACE );
		}
		if( medOrderItem.getVolume() != null && BigDecimal.ZERO.compareTo(medOrderItem.getVolume()) != 0 ){
	        medOrderItemDesc.append( volumeValue );medOrderItemDesc.append( SPACE );
		}
		
		if( StringUtils.isNotBlank(medOrderItem.getVolumeUnit()) ){
			medOrderItemDesc.append( medOrderItem.getVolumeUnit() );medOrderItemDesc.append( SPACE );
		}
		
		if( StringUtils.isNotBlank(extraInfo) ){			
			medOrderItemDesc.append( extraInfo );medOrderItemDesc.append( SPACE );
		}
		if( StringUtils.isNotBlank(sampleTag) ){
			medOrderItemDesc.append( sampleTag );medOrderItemDesc.append( SPACE );
		}
		
		return medOrderItemDesc.toString();
	}
	
	public void printRefillCouponFromDispOrder(DispOrder dispOrder, boolean isReprint){
		
		List<RenderAndPrintJob> printJobList = new ArrayList<RenderAndPrintJob>();
		
		this.prepareRefillCouponPrintJob(printJobList, dispOrder, isReprint);
		
		printAgent.renderAndPrint(printJobList);
	}
	
	@SuppressWarnings("unchecked")
	public void prepareRefillCouponPrintJob(List<RenderAndPrintJob> printJobList, DispOrder dispOrder, boolean isReprint) {		

		List<RefillSchedule> rsList = em.createQuery(
				"select o from RefillSchedule o" + // 20120303 index check : RefillSchedule.dispOrder : FK_REFILL_SCHEDULE_02
				" where o.dispOrder = :dispOrder" +
				" and o.refillStatus = :refillStatus")
				.setParameter("dispOrder", dispOrder)
				.setParameter("refillStatus", RefillScheduleRefillStatus.Current)				
				.getResultList();
		
		boolean printedReminder = false;
		
		for( RefillSchedule rs : rsList) {
			if( rs.getDocType() == DocType.Standard ){
				if( rs.getPrintReminderFlag() && !printedReminder ){
					printedReminder = prepareStandardRefillCouponPrintJobWithReminderFlag(printJobList, rs, isReprint, null, null, printedReminder);
				}else{
					prepareStandardRefillCouponPrintJobWithReminderFlag(printJobList, rs, isReprint, null, null, printedReminder);
				}
			}else if( rs.getDocType() == DocType.Sfi ){
				this.prepareSfiRefillCouponPrintJob(printJobList, rs, isReprint, rs.getPrintLang());				
			}
		}
	}
	
	private static class RefillCouponDtlComparator implements Comparator<RefillCouponDtl>, Serializable
    {
		private static final long serialVersionUID = 1L;

		@Override
          public int compare(RefillCouponDtl o1, RefillCouponDtl o2) {
                return new CompareToBuilder()
                	  .append( o1.getItemNum(), o2.getItemNum() )
                      .toComparison();
          }
    }
}
