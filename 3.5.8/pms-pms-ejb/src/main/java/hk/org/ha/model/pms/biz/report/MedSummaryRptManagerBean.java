package hk.org.ha.model.pms.biz.report;

import static hk.org.ha.model.pms.prop.Prop.ALERT_EHR_PROFILE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.ALERT_PROFILE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.HOSPITAL_NAMECHI;
import static hk.org.ha.model.pms.prop.Prop.HOSPITAL_NAMEENG;
import static hk.org.ha.model.pms.prop.Prop.REPORT_MEDSUMMARY_2DBARCODE_VISIBLE;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.alert.AlertProfileException;
import hk.org.ha.model.pms.asa.exception.alert.ehr.EhrAllergyException;
import hk.org.ha.model.pms.biz.alert.AlertAuditManagerLocal;
import hk.org.ha.model.pms.biz.alert.AlertProfileManagerLocal;
import hk.org.ha.model.pms.biz.alert.ehr.EhrAlertManagerLocal;
import hk.org.ha.model.pms.biz.label.InstructionBuilder;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.udt.Gender;
import hk.org.ha.model.pms.udt.alert.AlertProfileStatus;
import hk.org.ha.model.pms.udt.alert.ehr.EhrAlertStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.alert.AlertProfile;
import hk.org.ha.model.pms.vo.alert.PatAdr;
import hk.org.ha.model.pms.vo.alert.PatAllergy;
import hk.org.ha.model.pms.vo.label.DispLabel;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.report.MedSummaryRpt;
import hk.org.ha.model.pms.vo.report.MedSummaryRptDtl;
import hk.org.ha.model.pms.vo.report.MedSummaryRptHdr;

import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

import com.jamesmurty.utils.XMLBuilder;
@AutoCreate
@Stateless
@Name("medSummaryRptManager")
@MeasureCalls
public class MedSummaryRptManagerBean implements MedSummaryRptManagerLocal {
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In 
	private AlertProfileManagerLocal alertProfileManager;
	
	@In
	private EhrAlertManagerLocal ehrAlertManager;
	
	@In 
	private InstructionBuilder instructionBuilder;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent;
	
	@In 
	private Workstore workstore;

	@In
	private AlertAuditManagerLocal alertAuditManager; 
	
	private static final int PAT_INFO_XML_LIMIT = 500;
	private static final int ITEM_XML_LIMIT = 1000;
	
	private DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
	
	@SuppressWarnings("unchecked")
	public List<MedSummaryRpt> retrieveMedSummaryRptList(Date ticketDate, String ticketNum, PrintLang printLang) {
		List<MedSummaryRpt> medSummaryRptList = new ArrayList<MedSummaryRpt>();
		
		List<DispOrder> dispOrderList = em.createQuery(
				"select o from DispOrder o" + // 20120306 index check : Ticket.workstore,orderType,ticketDate,ticketNum : UI_TICKET_01
				" where o.ticket.workstore = :workstore" +
				" and o.ticket.orderType = :orderType" + 
				" and o.ticket.ticketDate = :ticketDate" + 
				" and o.ticket.ticketNum = :ticketNum" +
				" and o.adminStatus = :dispOrderAdminStatus" +
				" and o.status not in :status")
				.setParameter("workstore", workstore)
				.setParameter("orderType", OrderType.OutPatient)
				.setParameter("ticketDate", ticketDate)
				.setParameter("ticketNum", ticketNum)
				.setParameter("dispOrderAdminStatus", DispOrderAdminStatus.Normal)
				.setParameter("status", DispOrderStatus.Deleted_SysDeleted)
				.getResultList();

		if ( !dispOrderList.isEmpty() ) {
			if ( !dispOrderList.get(0).getDispOrderItemList().isEmpty() ) {
				DispOrder dispOrder = dispOrderList.get(0);
				MedSummaryRpt medSummaryRpt = new MedSummaryRpt();
				medSummaryRpt.setMedSummaryRptHdr(new MedSummaryRptHdr());
				medSummaryRpt.setMedSummaryRptDtlList(new ArrayList<MedSummaryRptDtl>());
				
				PrintOption printOption = new PrintOption();
				printOption.setPrintLang(printLang);
				
				Date defaultStartDate = null;
				if ( dispOrder.getPharmOrder().getMedOrder().getDocType().equals(MedOrderDocType.Normal) ) {
					defaultStartDate = dispOrder.getPharmOrder().getMedOrder().getOrderDate();
				} else {
					defaultStartDate = dispOrder.getTicket().getTicketDate();
				}
				
				for (DispOrderItem dispOrderItem : dispOrderList.get(0).getDispOrderItemList() ) {
					
					if ( dispOrderItem.getPharmOrderItem().getActionStatus().equals(ActionStatus.DispInClinic) || 
							dispOrderItem.getPharmOrderItem().getActionStatus().equals(ActionStatus.ContinueWithOwnStock) ||
							dispOrderItem.getPharmOrderItem().getActionStatus().equals(ActionStatus.PurchaseByPatient) ) {
						
						if ( dispOrderItem.getDispQty().compareTo(BigDecimal.ZERO) == 0 ) {
							continue;
						}
					}
					
					if ( dispOrderItem.getPharmOrderItem().getChestFlag() || FmStatus.FreeTextEntryItem.getDataValue().equals( StringUtils.trimToNull(dispOrderItem.getPharmOrderItem().getMedOrderItem().getFmStatus()) )) {
						continue;
					}
					
					MedSummaryRptDtl medSummaryRptDtl = new MedSummaryRptDtl();
					convertMedSummaryRptDtl(medSummaryRptDtl, dispOrderItem, printOption, defaultStartDate);
					medSummaryRpt.getMedSummaryRptDtlList().add(medSummaryRptDtl);
				}

				convertMedSummaryRptHdr(medSummaryRpt.getMedSummaryRptHdr(), dispOrderList.get(0), printLang);
				medSummaryRptList.add(medSummaryRpt);
			}
		}
		
		return medSummaryRptList;
	}
	
	private void convertMedSummaryRptDtl(MedSummaryRptDtl medSummaryRptDtl, DispOrderItem dispOrderItem, PrintOption printOption, Date defaultStartDate){
		medSummaryRptDtl.setItemCode(dispOrderItem.getPharmOrderItem().getItemCode());
		
		if ( dispOrderItem.getBaseLabel() != null ) {
			DispLabel dispLabel = (DispLabel)dispOrderItem.getBaseLabel();
			dispLabel.setPrintOption(printOption);
			
			medSummaryRptDtl.setLabelDesc(dispLabel.getItemDesc());
			medSummaryRptDtl.setLabelInstructionText(dispLabel.getInstructionText());
			medSummaryRptDtl.setWarningText(dispLabel.getWarningText());
			
			if ( defaultStartDate != dispLabel.getStartDate() )  {
				medSummaryRptDtl.setStartDate(dispLabel.getStartDate());
			}
		}
		medSummaryRptDtl.setIssueQty(dispOrderItem.getDispQty().intValue());
		medSummaryRptDtl.setBaseUnit(dispOrderItem.getPharmOrderItem().getBaseUnit());
		
		medSummaryRptDtl.setBarCodeEnable(REPORT_MEDSUMMARY_2DBARCODE_VISIBLE.get());
		
		if ( REPORT_MEDSUMMARY_2DBARCODE_VISIBLE.get() ) {
			medSummaryRptDtl.setItemXmlBarCode(constructItemXml(medSummaryRptDtl, dispOrderItem, printOption.getPrintLang()));
		}
	}
	
	private void convertMedSummaryRptHdr(MedSummaryRptHdr medSummaryRptHdr, DispOrder dispOrder, PrintLang printLang){
		medSummaryRptHdr.setPatName(dispOrder.getPharmOrder().getPatient().getName());
		medSummaryRptHdr.setPatNameChi(dispOrder.getPharmOrder().getPatient().getNameChi());
		medSummaryRptHdr.setGender(dispOrder.getPharmOrder().getPatient().getSex().getDisplayValue());

		String age = dispOrder.getPharmOrder().getPatient().getAge();
		String dateUnit = dispOrder.getPharmOrder().getPatient().getDateUnit(); 
		if( age != null ){
			StringBuilder ageUnit = new StringBuilder();
			ageUnit.append(age);
			if ("day(s)".equals(dateUnit)) {
				ageUnit.append("d");
			}else if ("month(s)".equals(dateUnit)) {
				ageUnit.append("m");
			}else if ("year(s)".equals(dateUnit)) {
				ageUnit.append("y");
			}
			medSummaryRptHdr.setAge( ageUnit.toString() );
		}
		
		medSummaryRptHdr.setHkid(dispOrder.getPharmOrder().getPatient().getHkid());
				
		if ( dispOrder.getPharmOrder().getMedCase() != null ) {
			medSummaryRptHdr.setPasSpecCode(dispOrder.getPharmOrder().getMedCase().getPasSpecCode());
			medSummaryRptHdr.setPasSubSpecCode(dispOrder.getPharmOrder().getMedCase().getPasSubSpecCode());
			medSummaryRptHdr.setCaseNum( StringUtils.leftPad(dispOrder.getPharmOrder().getMedCase().getCaseNum(),12));
		}
		
		if (medSummaryRptHdr.getPasSpecCode()==null) {
			medSummaryRptHdr.setPasSpecCode(dispOrder.getSpecCode());
		}
		
		medSummaryRptHdr.setOrderDate(dispOrder.getPharmOrder().getMedOrder().getOrderDate());
		medSummaryRptHdr.setDispDate(dispOrder.getDispDate());
		medSummaryRptHdr.setTicketNum(dispOrder.getTicket().getTicketNum());
		
		if ( printLang == PrintLang.Chi ) {
			medSummaryRptHdr.setHospName(HOSPITAL_NAMECHI.get());
		} else {
			medSummaryRptHdr.setHospName(HOSPITAL_NAMEENG.get());
		}
		
		medSummaryRptHdr.setBarCodeEnable(REPORT_MEDSUMMARY_2DBARCODE_VISIBLE.get());
		
		if ( REPORT_MEDSUMMARY_2DBARCODE_VISIBLE.get() ) {
			medSummaryRptHdr.setPatInforXmlBarCode(constructPatInfoXml(medSummaryRptHdr, printLang));
		}
		convertAlertProfile(medSummaryRptHdr, dispOrder);
		
	}
	
	private void convertAlertProfile(MedSummaryRptHdr medSummaryRptHdr, DispOrder dispOrder){

		AlertProfile alertProfile = null;

		medSummaryRptHdr.setAllergyServerAvailableFlag(false);
		if (ALERT_PROFILE_ENABLED.get(Boolean.FALSE)) {
			try {
				alertProfile = alertProfileManager.retrieveAlertProfileByPharmOrder(dispOrder.getPharmOrder());
			} catch (AlertProfileException e) {
				alertAuditManager.alertServiceUnavailable(dispOrder.getPharmOrder(), dispOrder.getTicket());
			}
		}
		
		if ( alertProfile != null ) {
			
			medSummaryRptHdr.setAlertProfileStatus(alertProfile.getStatus());
			medSummaryRptHdr.setAllergyServerAvailableFlag(true);

			if (!alertProfile.getStatus().equals(AlertProfileStatus.Error) ) {
				
				//set Drug Allergy
				StringBuilder allergenSb = new StringBuilder();
				for ( PatAllergy patAllergy : alertProfile.getPatAllergyList()) {
					if ( StringUtils.isNotBlank(allergenSb.toString()) ) {
						allergenSb.append(" , ");
					}
					allergenSb.append(patAllergy.getAllergenName());
				}
				medSummaryRptHdr.setDrugAllergenDesc(StringUtils.trimToNull(allergenSb.toString()));

				//set Adverse Drug Reaction
				StringBuilder adrSb = new StringBuilder();
				for ( PatAdr patAdr : alertProfile.getPatAdrList()) {
					if ( StringUtils.isNotBlank(adrSb.toString()) ) {
						adrSb.append(" , ");
					}
					adrSb.append(patAdr.getDrugName());
				}
				medSummaryRptHdr.setAdrAlertDesc(StringUtils.trimToNull(adrSb.toString()));
				
				//set other Alert
				if ( alertProfile.getG6pdAlert() != null ) {
					medSummaryRptHdr.setOtherAllergyDesc(StringUtils.trimToNull(alertProfile.getG6pdAlert().getAlertDesc()));
				}
			}
		}
		
		medSummaryRptHdr.setEhrAllergyException(true);
		if (ALERT_EHR_PROFILE_ENABLED.get()) {
			try {
				alertProfile = ehrAlertManager.retrieveEhrAllergySummaryByPharmOrder(alertProfile, dispOrder.getPharmOrder());
				if (alertProfile != null && alertProfile.getEhrAllergySummary() != null && alertProfile.getEhrAllergySummary().getStatus() != EhrAlertStatus.Error) {
					medSummaryRptHdr.setEhrAllergyException(false);
				}
			}
			catch (EhrAllergyException e) {}
		}

		if (alertProfile != null && alertProfile.getEhrAllergySummary() != null) {
			medSummaryRptHdr.setEhrAllergy(alertProfile.getEhrAllergySummary().getAllergy());
			medSummaryRptHdr.setEhrAdr(alertProfile.getEhrAllergySummary().getAdr());
		}
	}
	
	
	public boolean printMedSummaryRpt(Date dispDate, String ticketNum, PrintLang printLang) {
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("hospCode", workstore.getHospCode());
		parameters.put("workstoreCode", workstore.getWorkstoreCode());	
		
		List<MedSummaryRpt> medSummaryRptList = this.retrieveMedSummaryRptList(dispDate, ticketNum, printLang);
		
		if ( !medSummaryRptList.isEmpty() ) 
		{
			String rptType = "MedSummaryRpt";
			
			if ( printLang == PrintLang.Chi ) 
			{
				rptType = "MedSummaryRptChi";
			} 
			else 
			{
				rptType = "MedSummaryRpt";
			}
			
		    printAgent.renderAndPrint(new RenderAndPrintJob(
								    		rptType,
										    new PrintOption(),
										    printerSelector.retrievePrinterSelect(PrintDocType.Report),
										    parameters,
										    medSummaryRptList,
										    "[" + rptType + "]"
										    ));
		    return true;
		} 
		else 
		{
			return false;
		}
	}
	
	private String constructPatInfoXml(MedSummaryRptHdr medSummaryRptHdr, PrintLang printLang) {
		String patInfoXml = StringUtils.EMPTY;

		Properties outputProperties = new Properties();
		outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

		XMLBuilder xmlBuilder;
		StringWriter stringWriter = new StringWriter();
		
		try {
			xmlBuilder = XMLBuilder.create("pi");

			xmlBuilder.element("hoc").text(workstore.getHospCode()).up(); 
			
			if ( printLang==PrintLang.Chi ) {
				String hospNameChi = this.encodeStr(medSummaryRptHdr.getHospName());				
				xmlBuilder.element("ch").text(hospNameChi).up(); 
			} else {
				xmlBuilder.element("hn").text(medSummaryRptHdr.getHospName()).up();
			}
			
			xmlBuilder.element("id").text(medSummaryRptHdr.getHkid().substring(0, 4)+"****").up();
			
			xmlBuilder.element("pn").text(medSummaryRptHdr.getPatName()).up(); 
			
			String patNameChi = "";
			if ( medSummaryRptHdr.getPatNameChi() != null ) {
				patNameChi = encodeStr(medSummaryRptHdr.getPatNameChi());
			}
			xmlBuilder.element("cn").text(StringUtils.trimToEmpty(patNameChi)).up(); 
			
			if ( printLang==PrintLang.Chi ) {
				String sexChi = medSummaryRptHdr.getGender();
				
				if ( medSummaryRptHdr.getGender().equals(Gender.Female.getDisplayValue()) ) {
					sexChi = instructionBuilder.getProperties().getProperty("female");
					sexChi = this.encodeStr(sexChi);
					
				} else if ( medSummaryRptHdr.getGender().equals(Gender.Male.getDisplayValue()) ) {
					sexChi = instructionBuilder.getProperties().getProperty("male");
					sexChi = this.encodeStr(sexChi);
				}
				
				xmlBuilder.element("sc").text(sexChi).up(); 
			} else {
				xmlBuilder.element("s").text(medSummaryRptHdr.getGender()).up();
			}
			
			xmlBuilder.element("a").text(medSummaryRptHdr.getAge()).up();
			
			String su = medSummaryRptHdr.getPasSpecCode();
			if ( StringUtils.isNotBlank(medSummaryRptHdr.getPasSubSpecCode()) ) {
				su +=" / "+medSummaryRptHdr.getPasSubSpecCode();
			}

			xmlBuilder.element("su").text(StringUtils.trimToEmpty(su)).up();
			
			xmlBuilder.element("od").text(sdf.format(medSummaryRptHdr.getOrderDate())).up(); 
			xmlBuilder.element("dd").text(sdf.format(medSummaryRptHdr.getDispDate())).up(); 
			
			if ( StringUtils.isNotBlank(medSummaryRptHdr.getCaseNum() )) {
				String caseNum = medSummaryRptHdr.getCaseNum();
				xmlBuilder.element("ca").text(caseNum.substring(0, 4)+"-"+caseNum.substring(4, 6)+"-"+caseNum.substring(6, 11)+"("+caseNum.substring(11)+")").up();
			} else {
				xmlBuilder.element("ca").text("N/A").up();
			}
			
			if ( printLang==PrintLang.Chi ) {
				xmlBuilder.element("ve").text("CHN").up(); 
			} else {
				xmlBuilder.element("ve").text("ENG").up();
			}

			xmlBuilder.toWriter(stringWriter, outputProperties);
			patInfoXml = stringWriter.toString();
		} catch (ParserConfigurationException e) {
			logger.debug("ParserConfigurationException", e);
		} catch (FactoryConfigurationError e) {
			logger.debug("FactoryConfigurationError", e);
		} catch (TransformerException e) {
			logger.debug("TransformerException", e);
		}

		logger.debug("PatientInfo, Size=#0, Xml=#1", patInfoXml.getBytes().length, patInfoXml);
		
		if ( patInfoXml.getBytes().length > PAT_INFO_XML_LIMIT || StringUtils.isEmpty(patInfoXml)) {
			patInfoXml = null;
		}
		
		return patInfoXml;
	}
	
	private String constructItemXml(MedSummaryRptDtl medSummaryRptDtl, DispOrderItem dispOrderItem, PrintLang printLang){
		String itemXml = StringUtils.EMPTY;
		
		Properties outputProperties = new Properties();
		outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

		XMLBuilder xmlBuilder;
		StringWriter stringWriter = new StringWriter();
		
		try{
			xmlBuilder = XMLBuilder.create("rx");
			xmlBuilder.element("c").text(medSummaryRptDtl.getItemCode()).up(); 
			xmlBuilder.element("dn").text(dispOrderItem.getPharmOrderItem().getDrugName()).up(); 
			xmlBuilder.element("d").text(medSummaryRptDtl.getIssueQty().toString()).up(); 
			xmlBuilder.element("st").text(dispOrderItem.getPharmOrderItem().getStrength()).up(); 
			xmlBuilder.element("bu").text(medSummaryRptDtl.getBaseUnit()).up(); 
				
			String instElem = "i";
			String warningElem = "w";
			
			if ( printLang == PrintLang.Chi ) {
				instElem = "ci";
				warningElem = "cw";
			}
			
			if ( StringUtils.isNotBlank(medSummaryRptDtl.getLabelInstructionText()) ) {
				
				String[] insturation = medSummaryRptDtl.getLabelInstructionText().split("\n");
				int row = 1;
				for (String str : insturation) {
					String instStr = str;
					
					if ( printLang == PrintLang.Chi ) {
						instStr = this.encodeStr(str);
					}
					
					xmlBuilder.element(instElem+row).text(instStr).up(); 
					row++;
				}
			}

			if ( StringUtils.isNotBlank(medSummaryRptDtl.getWarningText()) ) {
				
				String[] warning = medSummaryRptDtl.getWarningText().split("\n");
				int row = 1;
				for ( String str : warning ) {
					String warnStr = str;
					
					if ( printLang == PrintLang.Chi ) {
						warnStr = encodeStr(str);
					}
					
					xmlBuilder.element(warningElem+row).text(warnStr).up();
					row++;
				}
			}
			
			xmlBuilder.toWriter(stringWriter, outputProperties);
			itemXml = stringWriter.toString();
			
		} catch (ParserConfigurationException e) {
			logger.debug("ParserConfigurationException: #0", e);
		} catch (FactoryConfigurationError e) {
			logger.debug("FactoryConfigurationError: #0", e);
		} catch (TransformerException e) {
			logger.debug("TransformerException: #0", e);
		}

		logger.debug("ItemInfo, Size=#0, Xml=#1", itemXml.getBytes().length, itemXml);
		
		if ( itemXml.getBytes().length > ITEM_XML_LIMIT || StringUtils.isEmpty(itemXml)) {
			itemXml = null;
		}
		
		return itemXml;
	}
	
	private String encodeStr(String str){
		try {
			byte[] chiByte = str.getBytes("UTF-16LE");
			byte[] decodeChi = Base64.encodeBase64(chiByte);
			return new String(decodeChi, "8859_1");
			
		} catch (UnsupportedEncodingException e) {
			logger.debug("UnsupportedEncodingException: #0", e);
		}
		
		return "";
	}
}
