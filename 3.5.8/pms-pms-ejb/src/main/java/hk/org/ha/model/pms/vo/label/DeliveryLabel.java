package hk.org.ha.model.pms.vo.label;

import hk.org.ha.model.pms.vo.printing.PrintOption;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class DeliveryLabel implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String hospName;
	
	private String batchNum;
	
	private Date batchDate;
 
	private String workstoreCode;
 
	private Date printDate;

	private String deliveryId;
	
	private String wardCode;
	
	private boolean urgentFlag;
	
	private boolean refrigerateFlag;
	
	private boolean overdueFlag;
	
	private Date targetedDeliveryTime;
	
	private boolean newOrdersFlag;
	
	private boolean refillOrdersFlag;

	private transient PrintOption printOption;
	
	public PrintOption getPrintOption() {
		return printOption;
	}

	public void setPrintOption(PrintOption printOption) {
		this.printOption = printOption;
	}
	
	public String getHospName() {
		return hospName;
	}

	public void setHospName(String hospName) {
		this.hospName = hospName;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public Date getPrintDate() {
		return printDate;
	}

	public void setPrintDate(Date printDate) {
		this.printDate = printDate;
	}

	public String getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(String deliveryId) {
		this.deliveryId = deliveryId;
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public boolean getUrgentFlag() {
		return urgentFlag;
	}

	public void setUrgentFlag(boolean urgentFlag) {
		this.urgentFlag = urgentFlag;
	}

	public boolean getRefrigerateFlag() {
		return refrigerateFlag;
	}

	public void setRefrigerateFlag(boolean refrigerateFlag) {
		this.refrigerateFlag = refrigerateFlag;
	}

	public Date getTargetedDeliveryTime() {
		return targetedDeliveryTime;
	}

	public void setTargetedDeliveryTime(Date targetedDeliveryTime) {
		this.targetedDeliveryTime = targetedDeliveryTime;
	}

	public void setOverdueFlag(boolean overdueFlag) {
		this.overdueFlag = overdueFlag;
	}

	public boolean isOverdueFlag() {
		return overdueFlag;
	}

	public void setNewOrdersFlag(boolean newOrdersFlag) {
		this.newOrdersFlag = newOrdersFlag;
	}

	public boolean isNewOrdersFlag() {
		return newOrdersFlag;
	}

	public void setRefillOrdersFlag(boolean refillOrdersFlag) {
		this.refillOrdersFlag = refillOrdersFlag;
	}

	public boolean isRefillOrdersFlag() {
		return refillOrdersFlag;
	}

	public void setBatchDate(Date batchDate) {
		this.batchDate = batchDate;
	}

	public Date getBatchDate() {
		return batchDate;
	}

}