package hk.org.ha.model.pms.biz.pivas;

import java.io.Serializable;
import java.util.Map;

public interface PivasWorklistClientNotifierInf {

    void notifyClient(Serializable object, Map<String, Object> headerParams);
    
    void dispatchUpdateEvent(String workstoreGroupCode);
}
