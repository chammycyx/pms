package hk.org.ha.model.pms.vo.rx;

import hk.org.ha.model.pms.corp.cache.DmRegimenCacher;
import hk.org.ha.model.pms.dms.persistence.DmRegimen;
import hk.org.ha.model.pms.udt.vetting.RegimenDurationUnit;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class DoseGroup implements Serializable {		
	
	private static final long serialVersionUID = 1L;

	@XmlElement(required = true)
	private Integer duration;

	@XmlElement(required = true)
	private RegimenDurationUnit durationUnit;
	
	@XmlElement
	private Date orgStartDate;
	
	@XmlElement(required = true)
	private Date startDate;
	
	@XmlElement
	private Date endDate;

	@XmlElement
	private Integer validPeriod;

	@XmlElement
	private String validPeriodUnit;

	@XmlElement
	private Date reviewDate;
	
	@XmlElement(name="Dose", required = true)
	private List<Dose> doseList;
	
	@XmlElement
	private Integer doseDay;
	
	@XmlElement
	private String dhDurationUnit;
	
	@XmlTransient
	private DmRegimen dmRegimen;
	
	public void clearDmInfo() {
		if (dmRegimen != null) {
			dmRegimen = null;
		}
		for (Dose dose : this.getDoseList()) {
			dose.clearDmInfo();			
		}
	}
	
	public void loadDmInfo() 
	{
		if (dmRegimen == null && this.getDurationUnit() != null) {
			dmRegimen = DmRegimenCacher.instance().getRegimenByRegimenCode(this.getDurationUnit().getDataValue());
		}
		
		for (Dose dose : this.getDoseList()) {
			dose.loadDmInfo();			
		}
	}
	
	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDurationUnit(RegimenDurationUnit durationUnit) {
		this.durationUnit = durationUnit;
	}

	public RegimenDurationUnit getDurationUnit() {
		return durationUnit;
	}

	public Date getOrgStartDate() {
		return orgStartDate == null ? null : new Date(orgStartDate.getTime());
	}

	public void setOrgStartDate(Date orgStartDate) {
		this.orgStartDate = orgStartDate == null ? null : new Date(orgStartDate.getTime());
	}

	public Date getStartDate() {
		return (startDate == null)? null : new Date(startDate.getTime());
	}

	public void setStartDate(Date startDate) {
		this.startDate = (startDate == null) ? null : new Date(startDate.getTime());
	}

	public Date getEndDate() {
		return (endDate == null)? null : new Date(endDate.getTime());
	}

	public void setEndDate(Date endDate) {
		this.endDate = (endDate == null) ? null : new Date(endDate.getTime());
	}

	public Integer getValidPeriod() {
		return validPeriod;
	}

	public void setValidPeriod(Integer validPeriod) {
		this.validPeriod = validPeriod;
	}

	public String getValidPeriodUnit() {
		return validPeriodUnit;
	}

	public void setValidPeriodUnit(String validPeriodUnit) {
		this.validPeriodUnit = validPeriodUnit;
	}

	public Date getReviewDate() {
		return (reviewDate == null)? null : new Date(reviewDate.getTime());
	}

	public void setReviewDate(Date reviewDate) {
		this.reviewDate = (reviewDate == null) ? null : new Date(reviewDate.getTime());
	}	
	
	public List<Dose> getDoseList() {
		if (doseList == null) {
			doseList = new ArrayList<Dose>();
		}
		return doseList;
	}

	public void setDoseList(List<Dose> doseList) {
		this.doseList = doseList;
	}

	public Integer getDoseDay() {
		return doseDay;
	}

	public void setDoseDay(Integer doseDay) {
		this.doseDay = doseDay;
	}

	public String getDhDurationUnit() {
		return dhDurationUnit;
	}

	public void setDhDurationUnit(String dhDurationUnit) {
		this.dhDurationUnit = dhDurationUnit;
	}

	public DmRegimen getDmRegimen() {
		return dmRegimen;
	}

	public void setDmRegimen(DmRegimen dmRegimen) {
		this.dmRegimen = dmRegimen;
	}
	
	public int getDurationInDay() {
		if ( durationUnit != null ) {
			this.dmRegimen = DmRegimenCacher.instance().getRegimenByRegimenCode(durationUnit.getDataValue());
		}
		if (getDuration() != null && dmRegimen != null) {			
			return getDuration().intValue() * dmRegimen.getRegimenMultiplier().intValue();
		}
		return 0;
	}	
}
