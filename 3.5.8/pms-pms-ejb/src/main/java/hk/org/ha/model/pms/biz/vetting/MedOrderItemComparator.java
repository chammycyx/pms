package hk.org.ha.model.pms.biz.vetting;

import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.vo.rx.IvAdditive;
import hk.org.ha.model.pms.vo.rx.IvFluid;

import java.io.Serializable;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.builder.CompareToBuilder;

public class MedOrderItemComparator implements Comparator<MedOrderItem>, Serializable {

	private static final long serialVersionUID = 1L;

	private Boolean isDhOrder = false;
	
	public MedOrderItemComparator() {
	}
	
	public MedOrderItemComparator(Boolean isDhOrder) {
		this.isDhOrder = isDhOrder;
	}
	
	public int compare(MedOrderItem item1, MedOrderItem item2) {

		if (isDhOrder) {
			return item1.getItemNum().compareTo(item2.getItemNum());
		}
		
		// compare ActionStatus
		Integer actionStatusSeq1 = Integer.valueOf(getActionStatus(item1).getRank());
		Integer actionStatusSeq2 = Integer.valueOf(getActionStatus(item2).getRank());
		if (!actionStatusSeq1.equals(actionStatusSeq2)) {
			return actionStatusSeq1.compareTo(actionStatusSeq2);
		}

		// compare orgItemNum
		Integer orgItemNum1 = item1.getOrgItemNum();
		Integer orgItemNum2 = item2.getOrgItemNum();
		if (!orgItemNum1.equals(orgItemNum2)) {
			return orgItemNum1.compareTo(orgItemNum2);
		}

		return CompareToBuilder.reflectionCompare(item1, item2);
	}
	
	private ActionStatus getActionStatus(MedOrderItem moi) {
		
		if (moi.getRxDrug() != null) {
			return moi.getRxDrug().getActionStatus();
		}
		
		if (moi.getCapdRxDrug() != null) {
			return moi.getCapdRxDrug().getActionStatus();
		}
		
		if (moi.getIvRxDrug() != null) {
			
			moi.loadDmInfo();
			
			Set<ActionStatus> actionStatusSet = new HashSet<ActionStatus>();
			Set<String> sfiCatSet = new HashSet<String>();
			for (IvAdditive ivAdditive : moi.getIvRxDrug().getIvAdditiveList()) {
				actionStatusSet.add(ivAdditive.getActionStatus());
				sfiCatSet.add(ivAdditive.getSfiCat());
			}
			for (IvFluid ivFluid : moi.getIvRxDrug().getIvFluidList()) {
				if (ivFluid.getDiluentCode() == null) {
					continue;
				}
				actionStatusSet.add(ivFluid.getActionStatus());
				sfiCatSet.add(null);
			}
			if (actionStatusSet.size() == 1 && actionStatusSet.size() <= 1) {
				return (ActionStatus) actionStatusSet.toArray()[0];
			}
		}
		
		return ActionStatus.DispByPharm;
	}
}