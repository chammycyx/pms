package hk.org.ha.model.pms.biz.drug;

import java.util.ArrayList;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.FormVerbCacherInf;
import hk.org.ha.model.pms.dms.vo.FormVerb;
import hk.org.ha.model.pms.vo.rx.Site;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("siteListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SiteListServiceBean implements SiteListServiceLocal {

	@In
	private FormVerbCacherInf formVerbCacher;
	
	public List<Site> retrieveSiteListByFormCode(String formCode) {
		Site site;
		ArrayList<Site> list = new ArrayList<Site>();		
		List<FormVerb> formVerbList;
		if ( StringUtils.isEmpty(formCode) ) {
			formVerbList = formVerbCacher.getFormVerbList();
			for ( FormVerb fv : formVerbList ) {
				site = new Site(fv.getSiteCode(), null, true);						
				list.add( site );
			}
		} else {
			formVerbList = formVerbCacher.getFormVerbListByFormCode(formCode);
			for ( FormVerb fv : formVerbList ) {				
				site = new Site(fv.getSiteCode(),fv.getSupplementarySite(),true);
				list.add(site);
			}
		}
		//append Others to siteList
		site = new Site();
		site.setSiteCode( "OTHERS" );
		site.setSupplSiteDesc( "OTHERS" );
		site.setSiteLabelDesc( "OTHERS" );
		list.add( site );
		return list;
	}
	
	@Remove
	public void destroy() 
	{
	}
}
