package hk.org.ha.model.pms.vo.alert;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

import hk.org.ha.model.pms.udt.alert.DrugType;
import hk.org.ha.model.pms.udt.alert.Severity;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class PatAdr implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement
	private String drugName;
	
	@XmlElement
	private DrugType drugType;
	
	@XmlElement
	private String reaction;
	
	@XmlElement
	private String additionalInfo;
	
	@XmlElement
	private Severity severity;
	
	@XmlElement
	private Date updateDate;
	
	@XmlElement
	private String updateUser;
	
	@XmlElement
	private String updateUserRank;
	
	@XmlElement
	private String hospCode;
	
	@XmlElement
	private String textColor;

	public String getDrugName() {
		return drugName;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public DrugType getDrugType() {
		return drugType;
	}

	public void setDrugType(DrugType drugType) {
		this.drugType = drugType;
	}

	public String getReaction() {
		return reaction;
	}

	public void setReaction(String reaction) {
		this.reaction = reaction;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public Severity getSeverity() {
		return severity;
	}

	public void setSeverity(Severity severity) {
		this.severity = severity;
	}

	public Date getUpdateDate() {
		if (updateDate == null) {
			return null;
		}
		else {
			return new Date(updateDate.getTime());
		}
	}

	public void setUpdateDate(Date updateDate) {
		if (updateDate == null) {
			this.updateDate = null;
		}
		else {
			this.updateDate = new Date(updateDate.getTime());
		}
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getUpdateUserRank() {
		return updateUserRank;
	}

	public void setUpdateUserRank(String updateUserRank) {
		this.updateUserRank = updateUserRank;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getTextColor() {
		return textColor;
	}

	public void setTextColor(String textColor) {
		this.textColor = textColor;
	}
}
