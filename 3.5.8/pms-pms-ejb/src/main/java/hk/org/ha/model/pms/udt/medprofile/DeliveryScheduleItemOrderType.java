package hk.org.ha.model.pms.udt.medprofile;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DeliveryScheduleItemOrderType implements StringValuedEnum {
		
	NewOrder("N", "New"),
	NewRefillOrder("NR","New -> Refill"),
	RefillOrder("R", "Refill"),
	Both("B", "Both");
	
    private final String dataValue;
    private final String displayValue;
	
	private DeliveryScheduleItemOrderType(String dataValue, String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}
	
	public static DeliveryScheduleItemOrderType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DeliveryScheduleItemOrderType.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<DeliveryScheduleItemOrderType> {
		
		private static final long serialVersionUID = 1L;

		public Class<DeliveryScheduleItemOrderType> getEnumClass() {
			return DeliveryScheduleItemOrderType.class;
		}
	}
}
