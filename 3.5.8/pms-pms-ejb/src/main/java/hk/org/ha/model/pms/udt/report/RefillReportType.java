package hk.org.ha.model.pms.udt.report;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum RefillReportType implements StringValuedEnum {

	RefillItemReport("RefillItemRpt", "Refill Item Report"),
	WorkloadForecastReport("RefillWorkloadForecastRpt", "Workload Forecast Report"),
	RefillQtyForecastReport("RefillQtyForecastRpt", "Refill Quantity Forecast Report"),
	RefillStatReport("RefillStatRpt", "Month End Report (Refill Statistics)"),
	ExpireRefillReport("RefillExpiryRpt", "Month End Report (Expire Refill)"),
	PharmClinicRefillCouponReport("PharmClinicRefillCouponRpt", "Pharmacist Clinic Refill Coupon Report");

    private final String dataValue;
    private final String displayValue;

    RefillReportType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static RefillReportType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(RefillReportType.class, dataValue);
	}
        
	public static class Converter extends StringValuedEnumConverter<RefillReportType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<RefillReportType> getEnumClass() {
    		return RefillReportType.class;
    	}
    }
}
