package hk.org.ha.model.pms.biz.onestop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.pas.PasException;
import hk.org.ha.model.pms.asa.exception.UnreachableException;
import hk.org.ha.model.pms.pas.PasServiceWrapper;
import hk.org.ha.model.pms.persistence.corp.HospitalMapping;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.vo.security.PropMap;
import hk.org.ha.service.pms.asa.interfaces.pas.PasServiceJmsRemote;

import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.log.Log;
import static hk.org.ha.model.pms.prop.Prop.*;

@AutoCreate
@Stateless
@Name("oneStopPasManager")
@MeasureCalls
public class OneStopPasManagerBean implements OneStopPasManagerLocal {
	
	@Logger
	private Log logger;
	
	@In
	private PasServiceWrapper pasServiceWrapper;
	
	@In
	private PasServiceJmsRemote pasServiceProxy;
	
	@In
	private OneStopUtilsLocal oneStopUtils;
			
	@In
	private PropMap propMap;
	
	@In
	private Workstore workstore;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private String errMsg;
	
	private final static String PATIENT = "patient";
	private final static String HKPMI_MEDCASE_LIST = "hkpmiMedCaseList";
	private final static String PHS_SPECIALTY = "phsSpecailty";
	private final static String PHS_WARD = "phsWard";
			
	public Patient retrieveHkpmiPatientByCaseNum(String patHopsCode, String caseNum) 
	{
		try 
		{
			Patient pasPatient = pasServiceWrapper.retrievePasPatientByCaseNum(patHopsCode, caseNum, true);
			logger.debug("patient is found in hkpmi: #0 - #1", pasPatient.getName(), pasPatient.getHkid());			
			return pasPatient;
		} 
		catch (PasException e) 
		{
			logger.debug("No patient infomation is found and call Hkid flow (hkid popup display)");
			return null;
		} catch (UnreachableException e) {
			errMsg = "0281";
			logger.debug("Hkpmi service is down, error message code is #0", errMsg);
			return null;
		}
	}
	
	public MedCase retriveAppointmentAttendanceAndStatusByAppointmentSequence(String patHospCode, String caseNum, String pasApptSeq)
	{
		try 
		{
			List<MedCase> medCaseList = pasServiceProxy.retrievePasAppointment(patHospCode, caseNum, pasApptSeq, "", "", "", "", "", "");
			
			if (medCaseList.isEmpty()) {
				return null;
			}
						
			logger.debug("appointment information retrieve from order's apptSeq#0: Attendance=#1, pasPayCode=#2, pasPatGroupCode=#3", 
						 pasApptSeq, medCaseList.get(0).getPasAttendFlag(), medCaseList.get(0).getPasPayCode(), medCaseList.get(0).getPasPatGroupCode());
			return medCaseList.get(0);
		} 
		catch (PasException e) 
		{
			logger.debug("no appointment information retrieved from order's apptSeq");
			return null;
		} catch (UnreachableException e) {
			logger.debug("Opas service is down");
			return null;
		}
	}
	
	public List<MedCase> retriveAppointmentAttendanceAndStatusWithoutAppointmentSequence(String patHospCode, String caseNum, String pasSpecCode,
																				   		 String pasSubSpecCode, String orderDate)
	{
		try 
		{
			List<MedCase> medCaseList = pasServiceProxy.retrievePasAppointment(patHospCode, caseNum, "", "", "", pasSpecCode, pasSubSpecCode, orderDate, "");
			
			logger.debug("retrieve appointment without appointment sequence from order");
			return medCaseList;
		} 
		catch (PasException e) 
		{
			logger.debug("no appointment information retrieved from order without apptSeq");
			return new ArrayList<MedCase>();
		} catch (UnreachableException e) {
			logger.debug("Opas service is down");
			return new ArrayList<MedCase>();
		}
	}
	
	public Patient retrieveHkPmiPatientByHkid(String hkid)
	{
		Patient pasPatient = null;
		try 
		{
			pasPatient = pasServiceWrapper.retrievePasPatientByHkid(hkid, "", "", true);
		}
		catch (PasException e)
		{
			logger.debug("No patient is found in HKPMI");
		} catch (UnreachableException e) {
			errMsg = "0281";
			logger.debug("Hkpmi service is down, error message code is #0", errMsg);
		}
		
		return pasPatient;
	}
	
	public Map<String, Object> retrieveHkPmiPatientWithCaseListByHkid(String hkid, Boolean displayAllMoe) 
	{
		Map<String, Object> hkpmiResult = new HashMap<String, Object>();
		String patHospCode = StringUtils.isBlank(workstore.getDefPatHospCode())?workstore.getHospCode():workstore.getDefPatHospCode();
		if (propMap.getValueAsBoolean(PATIENT_PAS_PATIENT_ENABLED.getName())) {
			Patient pasPatient;
			try 
			{							
				List<String> hkpmiMedCaseList = new ArrayList<String>();
				HashMap<String, MedCase> hashMap = new HashMap<String, MedCase>();
				pasPatient = pasServiceWrapper.retrievePasPatientFullCaseListByHkid(patHospCode, hkid, true);
				if (pasPatient != null) {
					hkpmiResult.put(PATIENT, pasPatient);
				
					for (MedCase medCase : pasPatient.getMedCaseList()) {
						if (!hashMap.containsKey(medCase.getCaseNum())) {
							hashMap.put(medCase.getCaseNum(), medCase);
							hkpmiMedCaseList.add(medCase.getCaseNum());
						}
					}
				}
				
				for (HospitalMapping hospitalMapping : workstore.getHospital().getHospitalMappingList())
				{
					pasPatient = pasServiceWrapper.retrievePasPatientFullCaseListByHkid(hospitalMapping.getPatHospCode(), hkid, true);
					
					if (pasPatient != null && hashMap.get(PATIENT) == null) {
						hkpmiResult.put(PATIENT, pasPatient);
					}	
						
					if (pasPatient != null) {
						for (MedCase medCase : pasPatient.getMedCaseList()) {
							if (!hashMap.containsKey(medCase.getCaseNum())) {
								hashMap.put(medCase.getCaseNum(), medCase);
								hkpmiMedCaseList.add(medCase.getCaseNum());
							}
						}
					}
				}
				
				if (!hkpmiMedCaseList.isEmpty()) 
				{
					hkpmiResult.put(HKPMI_MEDCASE_LIST, hkpmiMedCaseList);
				}
				
				logger.debug("Patient is found from hkid. Patient name: #0", pasPatient.getName());
			}
			catch (PasException e)
			{
				logger.debug("No patient is found in HKPMI");
			} catch (UnreachableException e) {
				errMsg = "0281";
				logger.debug("Hkpmi service is down, error message code is #0", errMsg);
			}
		}
		
		return hkpmiResult;
	}
	
	public List<MedCase> retrieveOpasAppointmentList(String dispHospCode, String patientKey) 
	{
		List <MedCase> appointmentList = null;
		
		try
		{
			appointmentList = pasServiceProxy.retrievePasAppointment(dispHospCode, "", "", patientKey, "", "", "", 
																	 oneStopUtils.startDate(false), oneStopUtils.endDate(false));
			logger.debug("retrieve appointment: #0  hospCode: #1  patient key #2 startDate #3  #4", 
					appointmentList.size(), 
					dispHospCode, patientKey, 
					oneStopUtils.startDate(false), 
					oneStopUtils.endDate(false));
			
			//map appointment specialty and ward
			for (MedCase medCase : appointmentList) 
			{
				Map<String, String> specWardMapping = oneStopUtils.phsSpecialtyWardMapping(workstore.getHospCode(), workstore.getWorkstoreCode(),
						 																		  dispHospCode, "O", medCase.getPasSpecCode(),
						 																		  medCase.getPasSubSpecCode(), "", "N", true);

				medCase.setPhsSpecCode(specWardMapping.get(PHS_SPECIALTY));
				medCase.setPhsWardCode(specWardMapping.get(PHS_WARD));
			}
			
			return appointmentList;
		}
		catch (PasException e) 
		{
			logger.debug("*********** No appoinrment is retrieved **************");
		} catch (UnreachableException e) {
			logger.debug("Opas service is down");
		}
		
		return appointmentList;
	}
	
	public Map<String, String> retrieveHkpmiPatientNameListByCaseNumList(List<String> patHospCodeCaseNumList){		
		try 
		{
			String patHospCodeCaseNumString = StringUtils.join(patHospCodeCaseNumList.toArray(),",");
			
			logger.debug("retrieveHkpmiPatientNameListByCaseNumList - #0", patHospCodeCaseNumString);
			
			return pasServiceWrapper.retrievePasPatientNameListByCaseNumList(workstore.getHospCode(), patHospCodeCaseNumString);			
		} 
		catch (PasException e) 
		{
			logger.debug("retrieveHkpmiPatientNameListByCaseNumList - No Patient Name is retrieved");
			return null;
		} catch (UnreachableException e) {
			logger.debug("retrieveHkpmiPatientNameListByCaseNumList - Hkpmi service is down");
			return null;
		}
	}
}