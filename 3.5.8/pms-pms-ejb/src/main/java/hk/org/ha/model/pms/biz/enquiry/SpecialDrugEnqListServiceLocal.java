package hk.org.ha.model.pms.biz.enquiry;

import javax.ejb.Local;

@Local
public interface SpecialDrugEnqListServiceLocal {
	
	void retrieveDmFmDrugList(String itemCodePrefix);
	
	void retrieveDmFmDrugListByFullDrugDesc(String fullDrugDescPrefix);
	
	void destroy();
	
}
