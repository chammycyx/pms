package hk.org.ha.model.pms.udt.medprofile;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;



public enum MedProfileStatAdminType implements StringValuedEnum {
	
	NewArrival("N", "New"),
	Verify("V", "Verify");

    private final String dataValue;
    private final String displayValue;
    
	private MedProfileStatAdminType(String dataValue, String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}

	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}
	
    public static MedProfileStatAdminType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MedProfileStatAdminType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<MedProfileStatAdminType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<MedProfileStatAdminType> getEnumClass() {
    		return MedProfileStatAdminType.class;
    	}
    }
}
