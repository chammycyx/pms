package hk.org.ha.model.pms.vo.report;

import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.disp.PharmOrderPatType;
import hk.org.ha.model.pms.udt.vetting.OrderType;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class SfiInvoiceHdr implements Serializable {

	private static final long serialVersionUID = 1L;

	private String hospCodeDesc;
	
	private Boolean reprintFlag;

	private String name;

	private String nameChi;

	private String hkid;

	private String caseNum;

	private String pasPayCode;

	private String pasPatGroupCode;
	
	private PharmOrderPatType patType;
	
	private MedOrderPrescType prescType;

	private String ticketNum;

	private Date sfiInvDate;

	private String sfiInvNum;

	private Double lessExemptedAmount;

	private String printWorkStation;
	
	private boolean lifeStyleDrugExist;
	
	private String orderType;

	public String getHospCodeDesc() {
		return hospCodeDesc;
	}

	public void setHospCodeDesc(String hospCodeDesc) {
		this.hospCodeDesc = hospCodeDesc;
	}

	public Boolean getReprintFlag() {
		return reprintFlag;
	}

	public void setReprintFlag(Boolean reprintFlag) {
		this.reprintFlag = reprintFlag;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameChi() {
		return nameChi;
	}

	public void setNameChi(String nameChi) {
		this.nameChi = nameChi;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getPasPayCode() {
		return pasPayCode;
	}

	public void setPasPayCode(String pasPayCode) {
		this.pasPayCode = pasPayCode;
	}

	public String getPasPatGroupCode() {
		return pasPatGroupCode;
	}

	public void setPasPatGroupCode(String pasPatGroupCode) {
		this.pasPatGroupCode = pasPatGroupCode;
	}

	public PharmOrderPatType getPatType() {
		return patType;
	}

	public void setPatType(PharmOrderPatType patType) {
		this.patType = patType;
	}

	public MedOrderPrescType getPrescType() {
		return prescType;
	}

	public void setPrescType(MedOrderPrescType prescType) {
		this.prescType = prescType;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public Date getSfiInvDate() {
		return sfiInvDate;
	}

	public void setSfiInvDate(Date sfiInvDate) {
		this.sfiInvDate = sfiInvDate;
	}

	public String getSfiInvNum() {
		return sfiInvNum;
	}

	public void setSfiInvNum(String sfiInvNum) {
		this.sfiInvNum = sfiInvNum;
	}

	public Double getLessExemptedAmount() {
		return lessExemptedAmount;
	}

	public void setLessExemptedAmount(Double lessExemptedAmount) {
		this.lessExemptedAmount = lessExemptedAmount;
	}

	public String getPrintWorkStation() {
		return printWorkStation;
	}

	public void setPrintWorkStation(String printWorkStation) {
		this.printWorkStation = printWorkStation;
	}

	public boolean isLifeStyleDrugExist() {
		return lifeStyleDrugExist;
	}

	public void setLifeStyleDrugExist(boolean lifeStyleDrugExist) {
		this.lifeStyleDrugExist = lifeStyleDrugExist;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getOrderType() {
		return orderType;
	}
}
