package hk.org.ha.model.pms.udt.alert.mds;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

import java.util.Arrays;
import java.util.List;

public enum MpDdiOverrideReason implements StringValuedEnum {

	NA("NA", "No other alternative available"),
	DI("DI", "Patient is taking this drug combination without problem"),
	NP("NP", "Confirmed with doctor (Enter doctor's name in Remarks)"),
	OJ("OJ", "Overridden by pharmacist's judgment");
	
	public static final List<MpDdiOverrideReason> mpDdiOverrideReasonList = Arrays.asList(NA, DI, NP, OJ);
	
	private final String dataValue;
    private final String displayValue;
    
    MpDdiOverrideReason(final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}

	public static MpDdiOverrideReason dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MpDdiOverrideReason.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<MpDdiOverrideReason> {
		
		private static final long serialVersionUID = 1L;

		public Class<MpDdiOverrideReason> getEnumClass() {
    		return MpDdiOverrideReason.class;
    	}
    }
}
