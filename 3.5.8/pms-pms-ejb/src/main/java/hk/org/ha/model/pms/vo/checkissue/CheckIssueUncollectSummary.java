package hk.org.ha.model.pms.vo.checkissue;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class CheckIssueUncollectSummary implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String ticketNum;
	
	private Date ticketDate;
	
	private Integer issueWindowNum;

	private boolean refrigItemFlag;
	
	private String message;

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public Date getTicketDate() {
		return ticketDate;
	}

	public void setTicketDate(Date ticketDate) {
		this.ticketDate = ticketDate;
	}

	public Integer getIssueWindowNum() {
		return issueWindowNum;
	}

	public void setIssueWindowNum(Integer issueWindowNum) {
		this.issueWindowNum = issueWindowNum;
	}

	public boolean getRefrigItemFlag() {
		return refrigItemFlag;
	}

	public void setRefrigItemFlag(boolean refrigItemFlag) {
		this.refrigItemFlag = refrigItemFlag;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}