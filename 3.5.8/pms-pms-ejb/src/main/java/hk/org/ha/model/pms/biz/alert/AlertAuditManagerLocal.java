package hk.org.ha.model.pms.biz.alert;

import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;
import hk.org.ha.model.pms.vo.alert.mds.ErrorInfo;
import hk.org.ha.model.pms.vo.alert.mds.MedProfileAlertMsg;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface AlertAuditManagerLocal {

	void alertServiceUnavailable(PharmOrder pharmOrder, Ticket ticket);
	
	void alertServiceUnavailable(PharmOrder pharmOrder, Date ticketDate, String ticketNum);

	void mdsServiceUnavailable(String hkid, Ticket ticket, String orderNum, String caseNum);
	
	void drugOnHandServiceUnavailable(String orderNum, String hkid, Ticket ticket, String caseNum);

	void newAlertDetect(PharmOrder pharmOrder, Ticket ticket, List<MedProfileAlertMsg> prescAlertMsgList);

	void alertMsg(String messageCode, String desc);
	
	void newAlertDetectForManualProfile(String action, Long medProfileId, String hkid, String caseNum, List<AlertMsg> alertMsgList);
	
	void cancelPrescribeForManualProfile(String action, Long medProfileId, String hkid, String caseNum, String orderDesc);
	
	void reminderMsgForManualProfile(String action, Long medProfileId, String hkid, String caseNum, List<ErrorInfo> errorInfoList);
}
