package hk.org.ha.model.pms.biz.pivas;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrug;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.pivas.PivasWorklist;
import hk.org.ha.model.pms.persistence.reftable.pivas.PivasServiceHour;
import hk.org.ha.model.pms.vo.pivas.PivasDiluentInfo;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.Freq;
import hk.org.ha.model.pms.vo.rx.IvAdditive;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface PivasWorklistManagerLocal {
	PivasWorklist retrievePivasWorklistByMedProfileItemId(Long medProfileItemId);
	
	List<PivasWorklist> retrieveRedundantWorklist(List<Long> medProfileItemIdList);
	
	List<PivasServiceHour> retrievePivasSupplyDateList(String workstoreGroupCode, Hospital hospital);
	
	List<PivasServiceHour> retrievePivasSupplyDateList(String workstoreGroupCode, Date dueDate, Hospital hospital);
	
	Date calculateSupplyDateByDueDate(String workstoreGroupCode, Hospital hospital, Date dueDate, boolean throwException);
	
	void resumeDispInPmsIp(Long medProfileItemId);
	
	List<PivasDiluentInfo> convertPivasDiluentList(List<PivasDrug> pivasDrugDiluentList, Workstore workstore);
	
	List<PivasDiluentInfo> convertPivasSolventList(List<PivasDrug> pivasDrugSolventList, Workstore workstore);
	
	DmDrug retrieveValidDmDrug(String itemCode, Workstore workstore);
	
	Dose extractOrderFirstDose(MedProfileMoItem medProfileMoItem);

	IvAdditive extractOrderFirstIvAdditive(MedProfileMoItem medProfileMoItem);
	
	BigDecimal extractOrderDosage(MedProfileMoItem medProfileMoItem);
	
	String extractOrderDosageUnit(MedProfileMoItem medProfileMoItem);
	
	Freq extractOrderDailyFreq(MedProfileMoItem medProfileMoItem);

	Freq extractOrderSupplFreq(MedProfileMoItem medProfileMoItem);
	
	String extractOrderSiteCode(MedProfileMoItem medProfileMoItem);

	Integer extractOrderDispQty(MedProfileMoItem medProfileMoItem);
}
