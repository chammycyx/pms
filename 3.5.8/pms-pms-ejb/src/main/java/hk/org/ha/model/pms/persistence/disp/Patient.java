package hk.org.ha.model.pms.persistence.disp;

import hk.org.ha.model.pms.persistence.PatientEntity;
import hk.org.ha.model.pms.udt.disp.PatientStatus;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;


@Entity
@Table(name = "PATIENT")
public class Patient extends PatientEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "patientSeq")
	@SequenceGenerator(name = "patientSeq", sequenceName = "SQ_PATIENT", initialValue = 100000000)
	private Long id;
	
	@Converter(name = "Patient.status", converterClass = PatientStatus.Converter.class)
    @Convert("Patient.status")
	@Column(name="STATUS", nullable = false, length = 1)
	private PatientStatus status;
	
	@Transient
	private List<MedCase> medCaseList;
	
	public Patient() {
		super();
		status = PatientStatus.Active;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PatientStatus getStatus() {
		return status;
	}

	public void setStatus(PatientStatus status) {
		this.status = status;
	}

	public List<MedCase> getMedCaseList() {
		if (medCaseList == null) {
			medCaseList = new ArrayList<MedCase>();
    	}
		return medCaseList;
	}

	public void setMedCaseList(List<MedCase> medCaseList) {
		this.medCaseList = medCaseList;
	}
	
	public void updateByPatient(Patient patient) {
		status = patient.getStatus();
		updateByPatientEntity(patient);
	}
}
