package hk.org.ha.model.pms.prop;


import hk.org.ha.model.pms.persistence.PropEntity;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@Name("propSessionCache")
@Scope(ScopeType.SESSION)
public class PropSessionCache implements Serializable {

	private static final long serialVersionUID = 1L;
	
	Map<String, PropEntity> map = Collections.synchronizedMap(new HashMap<String, PropEntity>());
	
	public PropEntity get(String name) {
		return map.get(name);
	}
	
	public PropEntity put(String name, PropEntity value) {
		return map.put(name, value);
	}
	
	public static PropSessionCache instance() {
		if (!Contexts.isSessionContextActive()) {
			throw new IllegalStateException("No active session scope");
		}
		return (PropSessionCache) Component.getInstance("propSessionCache",
				ScopeType.SESSION);
	}

}
