package hk.org.ha.model.pms.udt.alert;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum Certainty implements StringValuedEnum {
	
	Certain("C", "Certain"),
	Suspect("S", "Suspected"),
	SystemConversion("U", "System Conversion");
	
    private final String dataValue;
    private final String displayValue;
    
	Certainty(final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}

	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}

	public static Certainty dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(Certainty.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<Certainty> {
		
		private static final long serialVersionUID = 1L;

		public Class<Certainty> getEnumClass() {
    		return Certainty.class;
    	}
    }
	
}
