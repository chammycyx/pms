package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.model.pms.dms.persistence.DmMedicalOfficer;

import java.util.List;

import javax.ejb.Local;

@Local
public interface DmMedicalOfficerListManagerLocal {

	List<DmMedicalOfficer> retrieveDmMedicalOfficerList();
	
	void retrieveDmMedicalOfficerList(String prefixMedicalOfficerCode);
	
	void destroy();
	
}
