package hk.org.ha.model.pms.biz;

import hk.org.ha.service.biz.pms.interfaces.PmsSubscriberJmsRemote;

import javax.ejb.Local;

@Local
public interface PmsSubscriberLocal extends PmsSubscriberJmsRemote {
}
