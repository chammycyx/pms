package hk.org.ha.model.pms.workflow;

import hk.org.ha.fmk.pms.web.MeasureCalls;

import javax.ejb.Stateless;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.bpm.CreateProcess;
import org.jboss.seam.annotations.bpm.ResumeProcess;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;
import org.jbpm.graph.exe.ProcessInstance;

/**
 * Session Bean implementation class ProductService
 */
@Stateless
@Name("dispensingPMSService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DispensingPMSServiceBean implements DispensingPMSServiceLocal {

	@Logger
	private Log logger;

    @In(required=false) 
    private ProcessInstance processInstance;
    
    @In(scope=ScopeType.BUSINESS_PROCESS, required=false)
    @Out(scope=ScopeType.BUSINESS_PROCESS, required=false)
    private String orderNo;

	@CreateProcess(definition="dispensing-pms", processKey="#{orderNo}")
    public void checkClearance(String orderNo) {
		logger.info("checkClearance: #0", orderNo);
		this.orderNo = orderNo;
	}
	
	@ResumeProcess(definition="dispensing-pms", processKey="#{orderNo}")
	public void mdsOverrideReason() {
		logger.info("mdsOverrideReason: #0", orderNo);
		processInstance.signal();
	}
	
	@ResumeProcess(definition="dispensing-pms", processKey="#{orderNo}")
	public void createRefillSchedule() {
		logger.info("createRefillSchedule: #0", orderNo);
		processInstance.signal();
	}
	
	@ResumeProcess(definition="dispensing-pms", processKey="#{orderNo}")
	public void printSFIInvoice() {
		logger.info("printSFIInvoice: #0", orderNo);
		processInstance.signal();
	}
	
	@ResumeProcess(definition="dispensing-pms", processKey="#{orderNo}")
	public void suspendDispensingOrder() {
		logger.info("suspendDispensingOrder: #0", orderNo);
		processInstance.signal();
	}
	
	@ResumeProcess(definition="dispensing-pms", processKey="#{orderNo}")
	public void resumeSuspendDispensingOrder() {
		logger.info("resumeSuspendDispensingOrder: #0", orderNo);
		processInstance.signal();
	}
	
	@ResumeProcess(definition="dispensing-pms", processKey="#{orderNo}")
	public void checkSFIClearance() {
		logger.info("checkSFIClearance: #0", orderNo);
		processInstance.signal();
	}
}
