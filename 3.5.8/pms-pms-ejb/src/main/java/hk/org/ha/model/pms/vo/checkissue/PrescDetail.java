package hk.org.ha.model.pms.vo.checkissue;

import hk.org.ha.model.pms.persistence.PatientEntity;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.udt.Gender;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.disp.TicketMatchDocType;
import hk.org.ha.model.pms.vo.alert.AlertProfile;
import hk.org.ha.model.pms.vo.charging.DrugCheckClearanceResult;
import hk.org.ha.model.pms.vo.ehr.EhrPatient;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PrescDetail implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Date ticketDate;
	
	private String ticketNum;
	
	private DispOrderStatus dispOrderStatus;
	
	private Long dispOrderId;
	
	private Long pharmOrderId;
	
	private boolean refillFlag;
	
	private String orderNum;
	
	private Integer issueWindowNum;
	
	private DispOrderAdminStatus dispOrderAdminStatus;
	
	private MedOrderStatus medOrderStatus;
	
	private List<CheckIssueDocType> docTypeList;
	
	private String checkWorkstationCode;
	
	private DrugCheckClearanceResult drugCheckClearanceResult;
	
	private Long dispOrderVersion;
	
	private Boolean refillButtonEnable;
	
	private Boolean medSummaryRptButtonEnable;
	
	private String hkid;
	
	private AlertProfile alertProfile;
	
	private boolean alertProfileException = false;
	
	private boolean ehrAlertProfileException = false;
	
	private String alertErr;
	
	private PatientEntity patientEntity;
	
	private String patHospCode;
	
	private String caseNum;
	
	private boolean autoCheckIssue;
	
	private Integer totalDoc;
	
	private List<MedOrderItem> medOrderItemList;

	private List<String> discontinueMessageTlfList;
	
	private Boolean pregnancyCheckFlag;
	
	private Gender sex;
	
	private boolean fastQueueAllowCheckFlag;
	
	private boolean revokeFlag;
	
	private boolean dhOrderFlag;
	
	private boolean dhLatestFlag;
	
	private boolean dhAlertFlag = false;
	
	private boolean fastQueueTicket;
	
	private boolean firstRetrieveFlag;
	
	private TicketMatchDocType firstTicketMatchDocType;
	
	private TicketMatchDocType secondTicketMatchDocType;
	
	private String ticketMatchResult;
	
	private String firstTicketBarcode;
	
	private EhrPatient ehrPatient;
	
	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public Long getDispOrderId() {
		return dispOrderId;
	}

	public void setDispOrderId(Long dispOrderId) {
		this.dispOrderId = dispOrderId;
	}

	public Long getPharmOrderId() {
		return pharmOrderId;
	}

	public void setPharmOrderId(Long pharmOrderId) {
		this.pharmOrderId = pharmOrderId;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public void setIssueWindowNum(Integer issueWindowNum) {
		this.issueWindowNum = issueWindowNum;
	}

	public Integer getIssueWindowNum() {
		return issueWindowNum;
	}

	public DispOrderStatus getDispOrderStatus() {
		return dispOrderStatus;
	}

	public void setDispOrderStatus(DispOrderStatus dispOrderStatus) {
		this.dispOrderStatus = dispOrderStatus;
	}

	public DispOrderAdminStatus getDispOrderAdminStatus() {
		return dispOrderAdminStatus;
	}

	public void setDispOrderAdminStatus(DispOrderAdminStatus dispOrderAdminStatus) {
		this.dispOrderAdminStatus = dispOrderAdminStatus;
	}

	public MedOrderStatus getMedOrderStatus() {
		return medOrderStatus;
	}

	public void setMedOrderStatus(MedOrderStatus medOrderStatus) {
		this.medOrderStatus = medOrderStatus;
	}

	public void setDocTypeList(List<CheckIssueDocType> docTypeList) {
		this.docTypeList = docTypeList;
	}

	public List<CheckIssueDocType> getDocTypeList() {
		if (docTypeList == null) {
			docTypeList = new ArrayList<CheckIssueDocType>();
		}
		return docTypeList;
	}

	public void setCheckWorkstationCode(String checkWorkstationCode) {
		this.checkWorkstationCode = checkWorkstationCode;
	}

	public String getCheckWorkstationCode() {
		return checkWorkstationCode;
	}

	public void setDrugCheckClearanceResult(DrugCheckClearanceResult drugClearanceCheckingResult) {
		this.drugCheckClearanceResult = drugClearanceCheckingResult;
	}

	public DrugCheckClearanceResult getDrugCheckClearanceResult() {
		return drugCheckClearanceResult;
	}

	public void setDisprderVersion(Long dispOrderVersion) {
		this.dispOrderVersion = dispOrderVersion;
	}

	public Long getDispOrderVersion() {
		return dispOrderVersion;
	}

	public void setRefillButtonEnable(Boolean refillButtonEnable) {
		this.refillButtonEnable = refillButtonEnable;
	}

	public Boolean getRefillButtonEnable() {
		if ( refillButtonEnable == null ) {
			refillButtonEnable = false;
		}
		return refillButtonEnable;
	}

	public void setMedSummaryRptButtonEnable(Boolean medSummaryRptButtonEnable) {
		this.medSummaryRptButtonEnable = medSummaryRptButtonEnable;
	}

	public Boolean getMedSummaryRptButtonEnable() {
		if ( medSummaryRptButtonEnable == null ) {
			medSummaryRptButtonEnable = false;
		}
		return medSummaryRptButtonEnable;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getHkid() {
		return hkid;
	}

	public void setAlertProfile(AlertProfile alertProfile) {
		this.alertProfile = alertProfile;
	}

	public AlertProfile getAlertProfile() {
		return alertProfile;
	}

	public void setAlertProfileException(boolean alertProfileException) {
		this.alertProfileException = alertProfileException;
	}

	public boolean isAlertProfileException() {
		return alertProfileException;
	}

	public void setEhrAlertProfileException(boolean ehrAlertProfileException) {
		this.ehrAlertProfileException = ehrAlertProfileException;
	}

	public boolean isEhrAlertProfileException() {
		return ehrAlertProfileException;
	}

	public void setPatientEntity(PatientEntity patientEntity) {
		this.patientEntity = patientEntity;
	}

	public PatientEntity getPatientEntity() {
		return patientEntity;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setTicketDate(Date ticketDate) {
		this.ticketDate = ticketDate;
	}

	public Date getTicketDate() {
		return ticketDate;
	}

	public void setAutoCheckIssue(boolean autoCheckIssue) {
		this.autoCheckIssue = autoCheckIssue;
	}

	public boolean isAutoCheckIssue() {
		return autoCheckIssue;
	}

	public void setTotalDoc(Integer totalDoc) {
		this.totalDoc = totalDoc;
	}

	public Integer getTotalDoc() {
		return totalDoc;
	}

	public void setRefillFlag(boolean refillFlag) {
		this.refillFlag = refillFlag;
	}

	public boolean isRefillFlag() {
		return refillFlag;
	}

	public void setMedOrderItemList(List<MedOrderItem> medOrderItemList) {
		this.medOrderItemList = medOrderItemList;
	}

	public List<MedOrderItem> getMedOrderItemList() {
		return medOrderItemList;
	}

	public void setAlertErr(String alertErr) {
		this.alertErr = alertErr;
	}

	public String getAlertErr() {
		return alertErr;
	}

	public void setDiscontinueMessageTlfList(List<String> discontinueMessageTlfList) {
		this.discontinueMessageTlfList = discontinueMessageTlfList;
	}
	
	public List<String> getDiscontinueMessageTlfList() {
		return discontinueMessageTlfList;
	}

	public void setPregnancyCheckFlag(Boolean pregnancyCheckFlag) {
		this.pregnancyCheckFlag = pregnancyCheckFlag;
	}

	public Boolean getPregnancyCheckFlag() {
		return pregnancyCheckFlag;
	}

	public void setSex(Gender sex) {
		this.sex = sex;
	}

	public Gender getSex() {
		return sex;
	}
	
	public void setFastQueueAllowCheckFlag(boolean fastQueueAllowCheckFlag) {
		this.fastQueueAllowCheckFlag = fastQueueAllowCheckFlag;
	}

	public boolean getFastQueueAllowCheckFlag() {
		return fastQueueAllowCheckFlag;
	}

	public void setRevokeFlag(boolean revokeFlag) {
		this.revokeFlag = revokeFlag;
	}

	public boolean isRevokeFlag() {
		return revokeFlag;
	}

	public void setDhOrderFlag(boolean dhOrderFlag) {
		this.dhOrderFlag = dhOrderFlag;
	}

	public boolean isDhOrderFlag() {
		return dhOrderFlag;
	}

	public void setDhLatestFlag(boolean dhLatestFlag) {
		this.dhLatestFlag = dhLatestFlag;
	}

	public boolean isDhLatestFlag() {
		return dhLatestFlag;
	}

	public void setDhAlertFlag(boolean dhAlertFlag) {
		this.dhAlertFlag = dhAlertFlag;
	}

	public boolean isDhAlertFlag() {
		return dhAlertFlag;
	}

	public String getTicketMatchResult() {
		return ticketMatchResult;
	}

	public void setTicketMatchResult(String ticketMatchResult) {
		this.ticketMatchResult = ticketMatchResult;
	}

	public TicketMatchDocType getFirstTicketMatchDocType() {
		return firstTicketMatchDocType;
	}

	public void setFirstTicketMatchDocType(TicketMatchDocType firstTicketMatchDocType) {
		this.firstTicketMatchDocType = firstTicketMatchDocType;
	}

	public TicketMatchDocType getSecondTicketMatchDocType() {
		return secondTicketMatchDocType;
	}

	public void setSecondTicketMatchDocType(TicketMatchDocType secondTicketMatchDocType) {
		this.secondTicketMatchDocType = secondTicketMatchDocType;
	}

	public boolean isFirstRetrieveFlag() {
		return firstRetrieveFlag;
	}

	public void setFirstRetrieveFlag(boolean firstRetrieveFlag) {
		this.firstRetrieveFlag = firstRetrieveFlag;
	}

	public boolean isFastQueueTicket() {
		return fastQueueTicket;
	}

	public void setFastQueueTicket(boolean fastQueueTicket) {
		this.fastQueueTicket = fastQueueTicket;
	}

	public String getFirstTicketBarcode() {
		return firstTicketBarcode;
	}

	public void setFirstTicketBarcode(String firstTicketBarcode) {
		this.firstTicketBarcode = firstTicketBarcode;
	}

	public EhrPatient getEhrPatient() {
		return ehrPatient;
	}

	public void setEhrPatient(EhrPatient ehrPatient) {
		this.ehrPatient = ehrPatient;
	}
}