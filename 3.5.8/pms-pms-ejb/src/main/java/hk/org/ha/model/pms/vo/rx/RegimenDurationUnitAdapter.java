package hk.org.ha.model.pms.vo.rx;

import java.io.Serializable;

import hk.org.ha.model.pms.udt.vetting.RegimenDurationUnit;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class RegimenDurationUnitAdapter extends XmlAdapter<String, RegimenDurationUnit> implements Serializable {

	private static final long serialVersionUID = 1L;

	public String marshal( RegimenDurationUnit v ) {		
		return (v == null) ? null : v.getDataValue();
	}
	
	public RegimenDurationUnit unmarshal( String v ) {		
		return RegimenDurationUnit.dataValueOf(v);
	}

}