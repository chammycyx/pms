package hk.org.ha.model.pms.vo.charging;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="PatientSfiDtl")
@ExternalizedBean(type=DefaultExternalizer.class)
public class PatientSfiDtl implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@XmlElement(name="hospCode")
	private List<String> hospCodeList;

	@XmlElement(name="itemCode")
	private List<String> itemCodeList;
	
	@XmlElement
	private String fund;
	
	@XmlElement
	private String indicationCode;
	
	@XmlElement
	private String indication;
	
	@XmlElement
	private String cappingProgramFlag;
	
	@XmlTransient
	private String hkid;

	public PatientSfiDtl() {
	}

	public List<String> getHospCodeList() {
		return hospCodeList;
	}

	public void setHospCodeList(List<String> hospCodeList) {
		this.hospCodeList = hospCodeList;
	}

	public List<String> getItemCodeList() {
		return itemCodeList;
	}

	public void setItemCodeList(List<String> itemCodeList) {
		this.itemCodeList = itemCodeList;
	}

	public String getFund() {
		return fund;
	}

	public void setFund(String fund) {
		this.fund = fund;
	}

	public String getIndicationCode() {
		return indicationCode;
	}

	public void setIndicationCode(String indicationCode) {
		this.indicationCode = indicationCode;
	}

	public String getIndication() {
		return indication;
	}

	public void setIndication(String indication) {
		this.indication = indication;
	}

	public String getCappingProgramFlag() {
		return cappingProgramFlag;
	}

	public void setCappingProgramFlag(String cappingProgramFlag) {
		this.cappingProgramFlag = cappingProgramFlag;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}
}
