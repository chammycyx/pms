package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum OpTrxType implements StringValuedEnum {

	// From MOE to PMS
	OpCreateMoeOrder("CO1", "Create MOE Order", OpMessageType.NewOrder, Boolean.TRUE),
	OpModifyMoeOrder("AR1", "Modify MOE Order", OpMessageType.NewOrder, Boolean.TRUE),
	OpDeleteMoeOrder("DR1", "Delete MOE Order", OpMessageType.UpdateOrder, Boolean.TRUE),
	OpConfirmRemark("CR1", "Confirm Remark", OpMessageType.ConfirmRemark, Boolean.TRUE),
	OpChangePrescType("RT1", "Change Prescription Type", OpMessageType.UpdateOrder, Boolean.TRUE),
	OpAddPostDispComment("RT2", "Add Post Dispensing Comment", OpMessageType.UpdateOrder, Boolean.TRUE),
	OpResumeAdvanceOrder("RO1", "Change Advance Order to Normal Order (Reverse MOE suspend)", OpMessageType.UpdateOrder, Boolean.TRUE),
	OpDiscontinue("DD1", "Discontinue Item", OpMessageType.Discontinue, Boolean.TRUE),		
	OpCancelDiscontinue("DD2", "Cancel Discontinue Item", OpMessageType.Discontinue, Boolean.TRUE),

	// From PMS to MOE
	OpCreateManualOrder("CO2", "Create Manual Order", OpMessageType.NewOrder, Boolean.FALSE),
	OpModifyManualOrder("AR2", "Modify Manual Order", OpMessageType.NewOrder, Boolean.FALSE),
	OpDeleteManualOrder("DR2", "Delete Manual Order", OpMessageType.UpdateOrder, Boolean.FALSE),
	OpVetOrderWithoutRemark("VO1", "Vet Order without Remark", OpMessageType.UpdateOrder, Boolean.FALSE),
	OpVetOrderWithRemark("VO2", "Vet Order with Remark", OpMessageType.UpdateOrder, Boolean.FALSE),
	OpAllowModifyLegacyOrder("VO3", "Change Prescription Type for Legacy Order", OpMessageType.AllowModify, Boolean.FALSE),
	OpUpdatePrnProperty("PP1", "Update PRN property", OpMessageType.PrnProperty, Boolean.FALSE);
	
	private final String dataValue;
	private final String displayValue;
	private final OpMessageType messageType;
	private final Boolean toPmsFlag;
	
	OpTrxType (final String dataValue, final String displayValue, final OpMessageType messageType, Boolean toPmsFlag) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
		this.messageType = messageType;
		this.toPmsFlag = toPmsFlag;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}

	public OpMessageType getMessageType() {
		return this.messageType;
	}

	public Boolean getToPmsFlag() {
		return this.toPmsFlag;
	}
	
	public static OpTrxType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(OpTrxType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<OpTrxType> {

		private static final long serialVersionUID = 1L;

		public Class<OpTrxType> getEnumClass() {
    		return OpTrxType.class;
    	}
    }
}
