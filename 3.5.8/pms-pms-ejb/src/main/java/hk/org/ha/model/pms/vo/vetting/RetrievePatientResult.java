package hk.org.ha.model.pms.vo.vetting;

import hk.org.ha.model.pms.persistence.disp.Patient;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class RetrievePatientResult implements Serializable {

	private static final long serialVersionUID = 1L;

	private boolean successFlag;
	
	private Patient patient;

	public RetrievePatientResult() {
		this(false, null);
	}
	
	public RetrievePatientResult(boolean successFlag) {
		this(successFlag, null);
	}
	
	public RetrievePatientResult(boolean successFlag, Patient patient) {
		super();
		this.successFlag = successFlag;
		this.patient = patient;
	}

	public boolean isSuccessFlag() {
		return successFlag;
	}

	public void setSuccessFlag(boolean successFlag) {
		this.successFlag = successFlag;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}
}
