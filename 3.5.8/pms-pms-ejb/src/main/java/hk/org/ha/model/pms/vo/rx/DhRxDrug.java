package hk.org.ha.model.pms.vo.rx;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.StringUtils;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="DhRxDrug")
@ExternalizedBean(type=DefaultExternalizer.class)
public class DhRxDrug extends RxDrug implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@XmlElement
	private String vtm;

	@XmlElement
	private String localDrugKey;
	
	@XmlElement
	private String doseInstructionWithStyle;
	
	@XmlElement
	private String drugNameType;
	
	@XmlElement
	private String strengthExtraInfo;
	
	@XmlElement
	private String dhActionStatus;
	
	@XmlElement
	private String orderLineType;
	
	@XmlElement
	private String mapStatus;
	
	@XmlElement
	private Boolean mapFlag;
	
	@XmlElement
	private String drugRoute;
	
	public String getVtm() {
		return vtm;
	}

	public void setVtm(String vtm) {
		this.vtm = vtm;
	}
	
	public String getLocalDrugKey() {
		return localDrugKey;
	}

	public void setLocalDrugKey(String localDrugKey) {
		this.localDrugKey = localDrugKey;
	}

	public String getDoseInstructionWithStyle() {
		return doseInstructionWithStyle;
	}
	
	public void setDoseInstructionWithStyle(String doseInstructionWithStyle) {
		this.doseInstructionWithStyle = doseInstructionWithStyle;
	}

	public String getDrugNameType() {
		return drugNameType;
	}

	public void setDrugNameType(String drugNameType) {
		this.drugNameType = drugNameType;
	}

	public String getStrengthExtraInfo() {
		return strengthExtraInfo;
	}

	public void setStrengthExtraInfo(String strengthExtraInfo) {
		this.strengthExtraInfo = strengthExtraInfo;
	}

	public String getDhActionStatus() {
		return dhActionStatus;
	}

	public void setDhActionStatus(String dhActionStatus) {
		this.dhActionStatus = dhActionStatus;
	}

	public String getOrderLineType() {
		return orderLineType;
	}

	public void setOrderLineType(String orderLineType) {
		this.orderLineType = orderLineType;
	}

	public String getMapStatus() {
		return mapStatus;
	}

	public void setMapStatus(String mapStatus) {
		this.mapStatus = mapStatus;
	}

	public Boolean getMapFlag() {
		return mapFlag;
	}

	public void setMapFlag(Boolean mapFlag) {
		this.mapFlag = mapFlag;
	}
	
	public String getDrugRoute() {
		return drugRoute;
	}

	public void setDrugRoute(String drugRoute) {
		this.drugRoute = drugRoute;
	}

	public String getDhDrugDesc() {
		return ("F".equals(drugNameType) ? getDisplayName() : getVtm())
				+ (StringUtils.isBlank(getStrength()) ? "" : " " + getStrength())
				+ (StringUtils.isBlank(getRouteCode()) ? "" : " " + getRouteCode())
				+ (StringUtils.isBlank(getFormCode()) ? "" : " " + getFormCode());
	}
}
