package hk.org.ha.model.pms.udt.medprofile;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum InactiveType implements StringValuedEnum {
	
	Discharge("D", "Discharge"),
	CancelAdmission("C", "Cancel Admission"),
	Deleted("X", "Profile is Deleted");
	
    private final String dataValue;
    private final String displayValue;
	
	private InactiveType(String dataValue, String displayValue) {
    	this.dataValue = dataValue;
    	this.displayValue = displayValue;
	}
	
	@Override
	public String getDataValue() {
		return dataValue;
	}

	@Override
	public String getDisplayValue() {
		return displayValue;
	}
	
	public static InactiveType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(InactiveType.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<InactiveType> {
		
		private static final long serialVersionUID = 1L;
		
		@Override
		public Class<InactiveType> getEnumClass() {
			return InactiveType.class;
		}
	}
}
