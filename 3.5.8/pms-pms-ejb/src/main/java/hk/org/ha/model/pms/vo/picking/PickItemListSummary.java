package hk.org.ha.model.pms.vo.picking;

import java.io.Serializable;
import java.util.List;

public class PickItemListSummary implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<PickItem> pendingItemList;

	private List<PickItem> pickedItemList;
	
	public List<PickItem> getPendingItemList() {
		return pendingItemList;
	}

	public void setPendingItemList(List<PickItem> pendingItemList) {
		this.pendingItemList = pendingItemList;
	}

	public List<PickItem> getPickedItemList() {
		return pickedItemList;
	}

	public void setPickedItemList(List<PickItem> pickedItemList) {
		this.pickedItemList = pickedItemList;
	}
}