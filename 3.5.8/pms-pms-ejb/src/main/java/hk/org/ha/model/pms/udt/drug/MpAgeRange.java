package hk.org.ha.model.pms.udt.drug;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum MpAgeRange implements StringValuedEnum {

	ExactAge("E", "Exact Age"),
	Adult("U", "Adult"),
	Pediatric("O", "Paed"),
	Neonate("N", "Neonate");
	
	
    private final String dataValue;
    private final String displayValue;
        
    MpAgeRange(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static MpAgeRange dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MpAgeRange.class, dataValue);
	}
        
	public static class Converter extends StringValuedEnumConverter<MpAgeRange> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<MpAgeRange> getEnumClass() {
    		return MpAgeRange.class;
    	}
    }
}
