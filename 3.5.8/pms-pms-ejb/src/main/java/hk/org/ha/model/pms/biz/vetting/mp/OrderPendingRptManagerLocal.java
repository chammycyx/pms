package hk.org.ha.model.pms.biz.vetting.mp;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;

import java.util.List;

import javax.ejb.Local;

@Local
public interface OrderPendingRptManagerLocal {

	List<RenderAndPrintJob> printPendingOrderRpt(MedProfile medProfile);
	
}
