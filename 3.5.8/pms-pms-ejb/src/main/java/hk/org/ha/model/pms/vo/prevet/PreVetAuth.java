package hk.org.ha.model.pms.vo.prevet;

import java.io.Serializable;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PreVetAuth implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String authFailMsg;
	
	private String userId;
	
	private String userDisplayName;

	public PreVetAuth(String authFailMsg, String userId, String userDisplayName)
	{
		this.authFailMsg = authFailMsg;
		this.userId = userId;
		this.userDisplayName = userDisplayName;
	}
	
	public String getAuthFailMsg() {
		return authFailMsg;
	}

	public void setAuthFailMsg(String authFailMsg) {
		this.authFailMsg = authFailMsg;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setUserDisplayName(String userDisplayName) {
		this.userDisplayName = userDisplayName;
	}

	public String getUserDisplayName() {
		return userDisplayName;
	}
		
}