package hk.org.ha.model.pms.biz;

import hk.org.ha.fmk.pms.util.PropertiesHelper;

import java.io.IOException;
import java.util.Properties;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@AutoCreate
@Name("applicationProp")
@Scope(ScopeType.APPLICATION)
public class ApplicationProp {

	private static final String APPLICATION_PROPERTIES = "application.properties";
	private static final long RELOAD_INTERVAL = 120000L;
		
	public Properties getProperties() {
		try {
			return PropertiesHelper.getProperties(APPLICATION_PROPERTIES, Thread.currentThread().getContextClassLoader(), RELOAD_INTERVAL);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static final String SFTP_PMS_HOST = "sftp.pms.host";
	public static final String SFTP_PMS_USERNAME = "sftp.pms.username";
	public static final String SFTP_PMS_PASSWORD = "sftp.pms.password";
	public static final String SFTP_PMS_ROOT_DIR = "sftp.pms.root-dir";
	public static final String FM_RPT_DATA_SUB_DIR = "fm-rpt.data-sub-dir";
	public static final String DNLD_TRX_FILE = "dnld-trx.file";
	public static final String DNLD_TRX_DATA_SUB_DIR = "dnld-trx.data-sub-dir";
	public static final String FCS_UNCOLLECT_TRX_FILE = "fcs.uncollect.trx-file";
	public static final String EHR_EHRSS_EHR_ACCESS_FAQ_URL = "ehr.ehrss.ehr-access-faq-url";

	public String getSftpPmsHost() {
		return getProperties().getProperty(SFTP_PMS_HOST, "");
	}

	public String getSftpPmsUsername() {
		return getProperties().getProperty(SFTP_PMS_USERNAME, "");
	}
	
	public String getSftpPmsPassword() {
		return getProperties().getProperty(SFTP_PMS_PASSWORD, "");
	}
	
	public String getSftpPmsRootDir() {
		return getProperties().getProperty(SFTP_PMS_ROOT_DIR, "");
	}
	
	public String getFmRptDataSubDir() {
		return getProperties().getProperty(FM_RPT_DATA_SUB_DIR, "");
	}
	
	public String getDnldTrxFile() {
		return getProperties().getProperty(DNLD_TRX_FILE, "");
	}
	
	public String getDnldTrxDataSubDir() {
		return getProperties().getProperty(DNLD_TRX_DATA_SUB_DIR, "");
	}
	
	public String getFcsUncollectTrxFile() {
		return getProperties().getProperty(FCS_UNCOLLECT_TRX_FILE, "");
	}

	public String getEhrEhrssEhrAccessFaqUrl() {
		return getProperties().getProperty(EHR_EHRSS_EHR_ACCESS_FAQ_URL, "");
	}
}
