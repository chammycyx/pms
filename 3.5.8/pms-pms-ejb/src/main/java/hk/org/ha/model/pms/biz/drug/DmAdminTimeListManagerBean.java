package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmAdminTimeCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmAdminTime;

import java.util.List;

import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("dmAdminTimeListManager")
@MeasureCalls
public class DmAdminTimeListManagerBean implements DmAdminTimeListManagerLocal {

	@Logger
	private Log logger;
	
	@In
	private DmAdminTimeCacherInf dmAdminTimeCacher;

	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private List<DmAdminTime> dmAdminTimeList;
	
	public void retrieveDmAdminTimeList(String prefixAdminTimeCode)
	{
		logger.debug("retrieveDmAdminTimeList #0", prefixAdminTimeCode);
		
		if(StringUtils.isEmpty(prefixAdminTimeCode)){
			dmAdminTimeList = dmAdminTimeCacher.getDmAdminTimeList();
		}else{
			dmAdminTimeList = dmAdminTimeCacher.getDmAdminTimeListByAdminTimeCode(prefixAdminTimeCode);
		}

		if ( dmAdminTimeList != null ) {			
			logger.debug("dmAdminTimeList size #0", dmAdminTimeList.size());			
		}		
	}
}
