package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.persistence.reftable.DrsPatient;

import javax.ejb.Local;

@Local
public interface DrsPatientServiceLocal {

	DrsPatient retrieveDrsPatient(String hkidTxt);
	void updateDrsPatient(DrsPatient drsPatient);
	void destroy();
}
