package hk.org.ha.model.pms.biz.uncollect;

import hk.org.ha.model.pms.vo.uncollect.UncollectOrder;

import java.util.List;

import javax.ejb.Local;

@Local
public interface UncollectListServiceLocal {
	
	void retrieveUncollectOrderList();
	
	 void updateUncollectOrderList(List<UncollectOrder> updateUncollectOrderList);
	
	void destroy();
	
}
