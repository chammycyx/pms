package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.persistence.reftable.WardFilter;
import hk.org.ha.model.pms.vo.report.WardFilterRpt;

import java.util.List;

import javax.ejb.Local;

@Local
public interface WardFilterManagerLocal {

	List<WardFilter> retrieveWardFilterList();
	
	void updateWardFilterList(List<WardFilter> newWardFilterList);
	
	List<WardFilterRpt> genWardFilterRptList();
	
}
