package hk.org.ha.model.pms.persistence.corp;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.RecordStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "WORKSTORE_GROUP_MAPPING")
@IdClass(WorkstoreGroupMappingPK.class)
public class WorkstoreGroupMapping extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "WORKSTORE_GROUP_CODE", nullable = false, length = 3)
	private String workstoreGroupCode;
	
	@Id
	@Column(name = "PAT_HOSP_CODE", nullable = false, length = 3)
	private String patHospCode;
		    
	@ManyToOne
    @JoinColumn(name = "WORKSTORE_GROUP_CODE", nullable = false, insertable = false, updatable = false)
    private WorkstoreGroup workstoreGroup;

    @Converter(name = "WorkstoreGroupMapping.status", converterClass = RecordStatus.Converter.class)
    @Convert("WorkstoreGroupMapping.status")
	@Column(name = "STATUS", nullable = false, length = 1)
	private RecordStatus status;

    @Column(name = "SORT_SEQ")
	private Integer sortSeq;

	public WorkstoreGroupMapping() {
		status = RecordStatus.Active;
	}

	public String getWorkstoreGroupCode() {
		return workstoreGroupCode;
	}
	
	public void setWorkstoreGroupCode(String workstoreGroupCode) {
		this.workstoreGroupCode = workstoreGroupCode;
	}

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public RecordStatus getStatus() {
		return status;
	}

	public void setStatus(RecordStatus status) {
		this.status = status;
	}

	public WorkstoreGroup getWorkstoreGroup() {
		return workstoreGroup;
	}

	public void setSortSeq(Integer sortSeq) {
		this.sortSeq = sortSeq;
	}

	public Integer getSortSeq() {
		return sortSeq;
	}
}
