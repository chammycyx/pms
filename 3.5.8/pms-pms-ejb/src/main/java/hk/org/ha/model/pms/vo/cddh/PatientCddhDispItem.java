package hk.org.ha.model.pms.vo.cddh;

import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class PatientCddhDispItem implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@XmlElement
	private Long dispOrderId;	
	
	@XmlElement
	private Long dispOrderItemId;	
	
	@XmlElement
	private Date startDate;

	@XmlElement
	private Date endDate;

	@XmlElement
	private DispOrderAdminStatus adminStatus;
	
	public PatientCddhDispItem() {
	}

	public Long getDispOrderId() {
		return dispOrderId;
	}

	public void setDispOrderId(Long dispOrderId) {
		this.dispOrderId = dispOrderId;
	}

	public Long getDispOrderItemId() {
		return dispOrderItemId;
	}

	public void setDispOrderItemId(Long dispOrderItemId) {
		this.dispOrderItemId = dispOrderItemId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public DispOrderAdminStatus getAdminStatus() {
		return adminStatus;
	}

	public void setAdminStatus(DispOrderAdminStatus adminStatus) {
		this.adminStatus = adminStatus;
	}
}
