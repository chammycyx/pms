package hk.org.ha.model.pms.vo.alert;

import hk.org.ha.model.pms.udt.alert.AllergenType;
import hk.org.ha.model.pms.udt.alert.Certainty;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class PatAllergy implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement
	private String allergenName;
	
	@XmlElement
	private AllergenType allergenType;

	@XmlElement
	private String clinicalManifestation;
	
	@XmlElement
	private String additionalInfo;
	
	@XmlElement
	private Certainty certainty;
	
	@XmlElement
	private Date updateDate;
	
	@XmlElement
	private String updateUser;
	
	@XmlElement
	private String updateUserRank;
	
	@XmlElement
	private String hospCode;
	
	@XmlElement
	private String textColor;

	public String getAllergenName() {
		return allergenName;
	}

	public void setAllergenName(String allergenName) {
		this.allergenName = allergenName;
	}

	public AllergenType getAllergenType() {
		return allergenType;
	}

	public void setAllergenType(AllergenType allergenType) {
		this.allergenType = allergenType;
	}

	public String getClinicalManifestation() {
		return clinicalManifestation;
	}

	public void setClinicalManifestation(String clinicalManifestation) {
		this.clinicalManifestation = clinicalManifestation;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public Certainty getCertainty() {
		return certainty;
	}

	public void setCertainty(Certainty certainty) {
		this.certainty = certainty;
	}

	public Date getUpdateDate() {
		if (updateDate == null) {
			return null;
		}
		else {
			return new Date(updateDate.getTime());
		}
	}

	public void setUpdateDate(Date updateDate) {
		if (updateDate == null) {
			this.updateDate = null;
		}
		else {
			this.updateDate = new Date(updateDate.getTime());
		}
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getUpdateUserRank() {
		return updateUserRank;
	}

	public void setUpdateUserRank(String updateUserRank) {
		this.updateUserRank = updateUserRank;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getTextColor() {
		return textColor;
	}

	public void setTextColor(String textColor) {
		this.textColor = textColor;
	}
	
}
