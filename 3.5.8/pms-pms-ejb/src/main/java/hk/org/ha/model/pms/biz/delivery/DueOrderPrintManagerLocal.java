package hk.org.ha.model.pms.biz.delivery;

import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.Delivery;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryRequest;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryRequestGroup;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryScheduleItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.PurchaseRequest;
import hk.org.ha.model.pms.persistence.pivas.MaterialRequestItem;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.medprofile.PrintMode;
import hk.org.ha.model.pms.udt.medprofile.RetrievePurchaseRequestOption;
import hk.org.ha.model.pms.vo.charging.FcsPaymentStatus;
import hk.org.ha.model.pms.vo.delivery.FollowUpItem;
import hk.org.ha.model.pms.vo.medprofile.Ward;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

@Local
public interface DueOrderPrintManagerLocal {
	OverdueCount retrieveOverdueCountByStat(Workstore workstore, Date startDate,
			Date endDate, Date currentDate, Boolean urgentFlag, Boolean statFlag);
	List<PurchaseRequest> retrievePurchaseRequestList(Workstore workstore, Date currentDate, RetrievePurchaseRequestOption option);
	List<MaterialRequestItem> retrieveMaterialRequestItemList(
			Workstore workstore, Date startDate, Date currentDate);	
	Long retrieveFollowUpCount(Workstore workstore);	
	List<Delivery> retrieveTodayDeliveryList(Workstore workstore);
	List<FollowUpItem> retrieveFollowUpItemList(Workstore workstore);
	Map<String, FcsPaymentStatus> retrieveDrugChargeClearance(List<PurchaseRequest> purchaseRequestList);
	List<MedProfile> retrieveMedProfileList(Ward ward,
			DeliveryRequest request);
	List<MedProfile> retrieveMedProfileListForPivas(DeliveryRequest request);
	List<MedProfile> retrieveMedProfileListForMaterialRequest(DeliveryRequest request);	
	Delivery processMedProfile(DeliveryRequest request,
			boolean anonymousWard, MedProfile medProfile, boolean persistDelivery);
	Delivery processMedProfileForPivas(DeliveryRequest request, MedProfile medProfile);
	DeliveryRequest persistDeliveryRequest(Workstation workstation,
			DeliveryRequest request);
	DeliveryRequest beginProcessDeliveryRequest(Workstation workstation,
			DeliveryRequest request);	
	List<Delivery> createAndProcessDeliveryList(DeliveryRequest request, String wardCode,
			PrintMode printMode, List<DeliveryItem> deliveryItemList);
	void postProcessDeliveryList(List<Delivery> deliveryList);
	void endProcessDeliveryRequest(DeliveryRequest request);
	void revertDeliveryGenFlags(List<MedProfile> medProfileList);
	String directPrint(Workstation workstation, MedProfile medProfile, List<Integer> atdpsPickStationsNumList, List<PurchaseRequest> newPurchaseReqList, List<PurchaseRequest> voidPurchaseReqList);
	void printRemainingLabelAndReport(DeliveryRequest request);	
	Delivery processDeliveryForDrugRefillList(DeliveryRequest request, String wardCode, PrintMode printMode,
    		List<DeliveryItem> deliveryItemList, boolean includeDDnWardStockFlag);
	
	DeliveryRequestGroup processDeliveryRequestGroup(Workstation workstation, DeliveryScheduleItem deliveryScheduleItem);
	void endProcessDeliveryRequestGroup(DeliveryRequestGroup requestGroup);
	
}
