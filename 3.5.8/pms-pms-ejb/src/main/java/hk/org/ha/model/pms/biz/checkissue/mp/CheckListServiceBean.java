package hk.org.ha.model.pms.biz.checkissue.mp;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.vo.checkissue.mp.DrugCheckViewListSummary;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("checkListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CheckListServiceBean implements CheckListServiceLocal {
	
	@In
	private CheckListManagerLocal checkListManager;
	
	public DrugCheckViewListSummary retrieveDrugCheckViewListSummary() {
		return checkListManager.retrieveDrugCheckViewListSummary();
	}
	
	@Remove
	public void destroy() {
	}
}
