package hk.org.ha.model.pms.udt.report;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PivasRptItemCatFilterOption implements StringValuedEnum {

	All("A", "Drug / Solvent / Diluent"),
	Drug("B", "Drug"),
	SolventAndDiluent("C", "Solvent / Diluent");
	
    private final String dataValue;
    private final String displayValue;

    PivasRptItemCatFilterOption(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static PivasRptItemCatFilterOption dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PivasRptItemCatFilterOption.class, dataValue);
	}
        
	public static class Converter extends StringValuedEnumConverter<PivasRptItemCatFilterOption> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<PivasRptItemCatFilterOption> getEnumClass() {
    		return PivasRptItemCatFilterOption.class;
    	}
    }
}
