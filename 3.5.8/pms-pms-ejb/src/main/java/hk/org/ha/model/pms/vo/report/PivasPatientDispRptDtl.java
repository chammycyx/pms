package hk.org.ha.model.pms.vo.report;

import hk.org.ha.model.pms.udt.pivas.PivasWorklistStatus;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class PivasPatientDispRptDtl implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Date supplyDate;
	
	private String itemType;
	
	private String patName;
	
	private String patNameChi;
	
	private String caseNum;
	
	private String specCode;
	
	private String ward;
	
	private String bedNum;
	
	private String sex;
	
	private String age;
	
	private String bodyWeight;
	
	private Date pivasDueDate;
	
	private String prepDesc;
	
	private String drugItemCode;
	
	private String solventItemCode;
	
	private String diluentItemCode;
	
	private String drugManufCode;
	
	private String solventManufCode;
	
	private String diluentManufCode;
	
	private String drugBatchNum;
	
	private String solventBatchNum;
	
	private String diluentBatchNum;
	
	private Date drugExpiryDate;
	
	private Date solventExpiryDate;
	
	private Date diluentExpiryDate;
	
	private String supplyDesc;

	private String vendSupplySolvDesc;
	
	private boolean vendSupplySolvFlag;
	
	// for export
	private Date dob;
	
	private String pivasDesc;
	
	private String siteDesc;
	
	private String freq;
	
	private String supplFreq;
	
	private String orderDosage;
	
	private String modu;
	
	private String concn;
	
	private String drugVolume;
	
	private String finalVolume;
	
	private String issueQty;
	
	private String adjQty;
	
	private String containerName;
	
	private String splitCount;
	
	private String formulaPrintName;
	
	private Date prepExpiryDate;
	
	private String drugItemDesc;
	
	private String solventItemDesc;
	
	private String diluentItemDesc;
	
	private String diluentDesc;
	
	private Date printDate;
	
	private PivasWorklistStatus pivasWorklistStatus;
	
	private String drugCalQty;
	
	private String drugDispQty;
	
	private String drugBaseUnit;

	public PivasPatientDispRptDtl() {
		vendSupplySolvFlag = Boolean.FALSE;
	}

	public Date getSupplyDate() {
		return supplyDate;
	}

	public void setSupplyDate(Date supplyDate) {
		this.supplyDate = supplyDate;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getPatNameChi() {
		return patNameChi;
	}

	public void setPatNameChi(String patNameChi) {
		this.patNameChi = patNameChi;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public String getWard() {
		return ward;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}

	public String getBedNum() {
		return bedNum;
	}

	public void setBedNum(String bedNum) {
		this.bedNum = bedNum;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getBodyWeight() {
		return bodyWeight;
	}

	public void setBodyWeight(String bodyWeight) {
		this.bodyWeight = bodyWeight;
	}

	public Date getPivasDueDate() {
		return pivasDueDate;
	}

	public void setPivasDueDate(Date pivasDueDate) {
		this.pivasDueDate = pivasDueDate;
	}

	public String getPrepDesc() {
		return prepDesc;
	}

	public void setPrepDesc(String prepDesc) {
		this.prepDesc = prepDesc;
	}

	public String getDrugItemCode() {
		return drugItemCode;
	}

	public void setDrugItemCode(String drugItemCode) {
		this.drugItemCode = drugItemCode;
	}

	public String getSolventItemCode() {
		return solventItemCode;
	}

	public void setSolventItemCode(String solventItemCode) {
		this.solventItemCode = solventItemCode;
	}

	public String getDiluentItemCode() {
		return diluentItemCode;
	}

	public void setDiluentItemCode(String diluentItemCode) {
		this.diluentItemCode = diluentItemCode;
	}

	public String getDrugManufCode() {
		return drugManufCode;
	}

	public void setDrugManufCode(String drugManufCode) {
		this.drugManufCode = drugManufCode;
	}

	public String getSolventManufCode() {
		return solventManufCode;
	}

	public void setSolventManufCode(String solventManufCode) {
		this.solventManufCode = solventManufCode;
	}

	public String getDiluentManufCode() {
		return diluentManufCode;
	}

	public void setDiluentManufCode(String diluentManufCode) {
		this.diluentManufCode = diluentManufCode;
	}

	public String getDrugBatchNum() {
		return drugBatchNum;
	}

	public void setDrugBatchNum(String drugBatchNum) {
		this.drugBatchNum = drugBatchNum;
	}

	public String getSolventBatchNum() {
		return solventBatchNum;
	}

	public void setSolventBatchNum(String solventBatchNum) {
		this.solventBatchNum = solventBatchNum;
	}

	public String getDiluentBatchNum() {
		return diluentBatchNum;
	}

	public void setDiluentBatchNum(String diluentBatchNum) {
		this.diluentBatchNum = diluentBatchNum;
	}

	public Date getDrugExpiryDate() {
		return drugExpiryDate;
	}

	public void setDrugExpiryDate(Date drugExpiryDate) {
		this.drugExpiryDate = drugExpiryDate;
	}

	public Date getSolventExpiryDate() {
		return solventExpiryDate;
	}

	public void setSolventExpiryDate(Date solventExpiryDate) {
		this.solventExpiryDate = solventExpiryDate;
	}

	public Date getDiluentExpiryDate() {
		return diluentExpiryDate;
	}

	public void setDiluentExpiryDate(Date diluentExpiryDate) {
		this.diluentExpiryDate = diluentExpiryDate;
	}

	public String getSupplyDesc() {
		return supplyDesc;
	}

	public void setSupplyDesc(String supplyDesc) {
		this.supplyDesc = supplyDesc;
	}

	public String getVendSupplySolvDesc() {
		return vendSupplySolvDesc;
	}

	public void setVendSupplySolvDesc(String vendSupplySolvDesc) {
		this.vendSupplySolvDesc = vendSupplySolvDesc;
	}

	public boolean isVendSupplySolvFlag() {
		return vendSupplySolvFlag;
	}

	public void setVendSupplySolvFlag(boolean vendSupplySolvFlag) {
		this.vendSupplySolvFlag = vendSupplySolvFlag;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getPivasDesc() {
		return pivasDesc;
	}

	public void setPivasDesc(String pivasDesc) {
		this.pivasDesc = pivasDesc;
	}

	public String getSiteDesc() {
		return siteDesc;
	}

	public void setSiteDesc(String siteDesc) {
		this.siteDesc = siteDesc;
	}

	public String getFreq() {
		return freq;
	}

	public void setFreq(String freq) {
		this.freq = freq;
	}

	public String getSupplFreq() {
		return supplFreq;
	}

	public void setSupplFreq(String supplFreq) {
		this.supplFreq = supplFreq;
	}

	public String getOrderDosage() {
		return orderDosage;
	}

	public void setOrderDosage(String orderDosage) {
		this.orderDosage = orderDosage;
	}

	public String getModu() {
		return modu;
	}

	public void setModu(String modu) {
		this.modu = modu;
	}

	public String getConcn() {
		return concn;
	}

	public void setConcn(String concn) {
		this.concn = concn;
	}

	public String getDrugVolume() {
		return drugVolume;
	}

	public void setDrugVolume(String drugVolume) {
		this.drugVolume = drugVolume;
	}

	public String getFinalVolume() {
		return finalVolume;
	}

	public void setFinalVolume(String finalVolume) {
		this.finalVolume = finalVolume;
	}

	public String getIssueQty() {
		return issueQty;
	}

	public void setIssueQty(String issueQty) {
		this.issueQty = issueQty;
	}

	public String getAdjQty() {
		return adjQty;
	}

	public void setAdjQty(String adjQty) {
		this.adjQty = adjQty;
	}

	public String getContainerName() {
		return containerName;
	}

	public void setContainerName(String containerName) {
		this.containerName = containerName;
	}

	public String getSplitCount() {
		return splitCount;
	}

	public void setSplitCount(String splitCount) {
		this.splitCount = splitCount;
	}

	public String getFormulaPrintName() {
		return formulaPrintName;
	}

	public void setFormulaPrintName(String formulaPrintName) {
		this.formulaPrintName = formulaPrintName;
	}

	public Date getPrepExpiryDate() {
		return prepExpiryDate;
	}

	public void setPrepExpiryDate(Date prepExpiryDate) {
		this.prepExpiryDate = prepExpiryDate;
	}

	public String getDrugItemDesc() {
		return drugItemDesc;
	}

	public void setDrugItemDesc(String drugItemDesc) {
		this.drugItemDesc = drugItemDesc;
	}

	public String getSolventItemDesc() {
		return solventItemDesc;
	}

	public void setSolventItemDesc(String solventItemDesc) {
		this.solventItemDesc = solventItemDesc;
	}

	public String getDiluentItemDesc() {
		return diluentItemDesc;
	}

	public void setDiluentItemDesc(String diluentItemDesc) {
		this.diluentItemDesc = diluentItemDesc;
	}

	public String getDiluentDesc() {
		return diluentDesc;
	}

	public void setDiluentDesc(String diluentDesc) {
		this.diluentDesc = diluentDesc;
	}

	public Date getPrintDate() {
		return printDate;
	}

	public void setPrintDate(Date printDate) {
		this.printDate = printDate;
	}

	public PivasWorklistStatus getPivasWorklistStatus() {
		return pivasWorklistStatus;
	}

	public void setPivasWorklistStatus(PivasWorklistStatus pivasWorklistStatus) {
		this.pivasWorklistStatus = pivasWorklistStatus;
	}

	public String getDrugCalQty() {
		return drugCalQty;
	}

	public void setDrugCalQty(String drugCalQty) {
		this.drugCalQty = drugCalQty;
	}

	public String getDrugDispQty() {
		return drugDispQty;
	}

	public void setDrugDispQty(String drugDispQty) {
		this.drugDispQty = drugDispQty;
	}

	public String getDrugBaseUnit() {
		return drugBaseUnit;
	}

	public void setDrugBaseUnit(String drugBaseUnit) {
		this.drugBaseUnit = drugBaseUnit;
	}

}
