package hk.org.ha.model.pms.biz.alert;

import hk.org.ha.model.pms.asa.exception.alert.AlertProfileException;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.vo.alert.AlertProfile;

import javax.ejb.Local;

@Local
public interface AlertProfileManagerLocal {

	AlertProfile retrieveAlertProfileByPharmOrder(PharmOrder pharmOrder) throws AlertProfileException;

	AlertProfile retrieveAlertProfileByMedProfile(MedProfile medProfile) throws AlertProfileException;

	AlertProfile retrieveAlertProfileByMedOrder(MedOrder medOrder, Patient patient) throws AlertProfileException;
	
	AlertProfile retrieveAlertProfile(String patHospCode, String caseNum, Patient patient) throws AlertProfileException;
}
