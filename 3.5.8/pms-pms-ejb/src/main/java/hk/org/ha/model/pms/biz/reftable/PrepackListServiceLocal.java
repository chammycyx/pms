package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.persistence.reftable.Prepack;

import java.util.Collection;

import javax.ejb.Local;

@Local
public interface PrepackListServiceLocal {

	void retrievePrepackByItemCode(String itemCode);
	
	public void updatePrepack(Prepack updatePrepack);
	
	void destroy();
	
}
