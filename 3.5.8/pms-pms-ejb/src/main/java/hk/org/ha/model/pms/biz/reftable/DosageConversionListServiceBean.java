package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.PmsDosageConversion;
import hk.org.ha.model.pms.dms.persistence.PmsDosageConversionSet;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.NoResultException;
import javax.persistence.OptimisticLockException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("dosageConversionListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DosageConversionListServiceBean implements DosageConversionListServiceLocal {

	@Out(required = false)
	private List<PmsDosageConversionSet> pmsDosageConversionSetList;
	
	private PmsDosageConversionSet pmsDosageConversionSet;
	
	@In
	private Workstore workstore;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;
	
	@Out(required = false)
	private List<WorkstoreDrug> extendDmDrugList;
	
	@Out(required = false)
	private List<WorkstoreDrug> originalDmDrugList;
	
	private boolean saveSuccess = true;
	
	private String errorCode = "";
	
	private String[] errorParam = {""};
		
	private boolean retrieveSuccess;

	public void retrievePmsDosageConversionSetList(DmDrug dmDrug) {
		retrieveSuccess = true;
		
		try{
			pmsDosageConversionSet = null;
			pmsDosageConversionSetList = dmsPmsServiceProxy.retrievePmsDosageConversionSetList(
					workstore.getHospCode(), 
					workstore.getWorkstoreCode(),
					dmDrug.getDrugKey(), 
					dmDrug.getPmsFmStatus().getFmStatus());
		} catch (NoResultException e){
			retrieveSuccess = false;
		}
	}
	
	public PmsDosageConversionSet retrievePmsDosageConversionByMoDosage(DmDrug dmDrug, double moDosage, String moDosageUnit) {
		retrieveSuccess = true;
		
		try{
			retrieveDmDrugListByDrugKey(dmDrug);
			retrieveDmDrugListByDisplayname(dmDrug);
			
			pmsDosageConversionSet = dmsPmsServiceProxy.retrievePmsDosageConversionSetByMoDosage(
					workstore.getHospCode(), 
					workstore.getWorkstoreCode(), 
					moDosage, moDosageUnit, dmDrug.getDrugKey(), 
					dmDrug.getPmsFmStatus().getFmStatus());

			setPmsDosageConversion( pmsDosageConversionSet );
			
			if (pmsDosageConversionSet.getId() == null){
				pmsDosageConversionSet.setMoDosageUnit(dmDrug.getDmMoeProperty().getMoDosageUnit());
				pmsDosageConversionSet.setPmsFmStatus(dmDrug.getPmsFmStatus().getFmStatus());
			}
			return pmsDosageConversionSet;
			
		} catch (NoResultException e){
			retrieveSuccess = false;
			return new PmsDosageConversionSet();
		}
	}		
	
	private void setPmsDosageConversion(PmsDosageConversionSet pmsDosageConversionSet){
		for(PmsDosageConversion pdc :pmsDosageConversionSet.getPmsDosageConversionList()){
			WorkstoreDrug workstoreDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, pdc.getItemCode());
			if( workstoreDrug != null && workstoreDrug.getMsWorkstoreDrug() != null ){
				pdc.setDmDrug(workstoreDrug.getDmDrug());
				pdc.setMsWorkstoreDrug( workstoreDrug.getMsWorkstoreDrug() );
				pdc.setSuspend( !workstoreDrug.getMsWorkstoreDrug().isAvailable() );
			}else{
				pdc.setDmDrug( null );
			}
		}
	}

	private void retrieveDmDrugListByDrugKey(DmDrug dmDrug) {
		originalDmDrugList = workstoreDrugCacher.getWorkstoreDrugListByDrugKey(
				workstore,
				dmDrug.getDrugKey());
		
		Collections.sort(originalDmDrugList, new WorkstoreDrugComparator());
	}
	
	private void retrieveDmDrugListByDisplayname(DmDrug dmDrug) {
		extendDmDrugList = workstoreDrugCacher.getWorkstoreDrugListByDisplayName(
				workstore,
				dmDrug.getDmDrugProperty().getDisplayname());

		Collections.sort(extendDmDrugList, new WorkstoreDrugComparator());
	}
	
	public void updateDosageConversion(PmsDosageConversionSet pmsDosageConversionSet) {
		pmsDosageConversionSet.setDispHospCode(workstore.getHospCode());
		pmsDosageConversionSet.setDispWorkstore(workstore.getWorkstoreCode());
		
		try{
			saveSuccess = true;
			dmsPmsServiceProxy.updatePmsDosageConversionSet(pmsDosageConversionSet);
		}catch(Exception e){
			if( e.getCause() instanceof OptimisticLockException ){
				errorCode = "0307";
			}else{
				errorCode = "0729";
			}
			saveSuccess = false;
		}
	}
		
	public boolean isRetrieveSuccess(){
		return retrieveSuccess;
	}

	public boolean isSaveSuccess() {
		return saveSuccess;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String[] getErrorParam() {
		return errorParam;
	}

	public static class WorkstoreDrugComparator implements Comparator<WorkstoreDrug>, Serializable {

		private static final long serialVersionUID = 1L;
		
		@Override
		public int compare(WorkstoreDrug wsDrug1, WorkstoreDrug wsDrug2) {
			CompareToBuilder wsDrugCompareTobuilder = new CompareToBuilder();
			
			wsDrugCompareTobuilder.append( wsDrug1.getDmDrug().getDrugName(), wsDrug2.getDmDrug().getDrugName());
			
			//Form Label Desc
			String wsDrug1LabelDesc = null;
			if( wsDrug1.getDmDrug().getDmForm() != null ){
				wsDrug1LabelDesc = wsDrug1.getDmDrug().getDmForm().getLabelDesc();
			}
			String wsDrug2LabelDesc = null;
			if( wsDrug2.getDmDrug().getDmForm() != null ){
				wsDrug2LabelDesc = wsDrug2.getDmDrug().getDmForm().getLabelDesc();
			}
			wsDrugCompareTobuilder.append(wsDrug1LabelDesc, wsDrug2LabelDesc);
			
			//Strength
			String wsDrug1StrengthUnit = null;
			Double wsDrug1StrengthValue = null;
			if( wsDrug1.getDmDrug().getDmMoeProperty() != null ){
				wsDrug1StrengthUnit = wsDrug1.getDmDrug().getDmMoeProperty().getStrengthUnit();
				wsDrug1StrengthValue = wsDrug1.getDmDrug().getDmMoeProperty().getStrengthValue();
			}
			String wsDrug2StrengthUnit = null;
			Double wsDrug2StrengthValue = null;
			if( wsDrug2.getDmDrug().getDmMoeProperty() != null ){
				wsDrug2StrengthUnit = wsDrug2.getDmDrug().getDmMoeProperty().getStrengthUnit();
				wsDrug2StrengthValue = wsDrug2.getDmDrug().getDmMoeProperty().getStrengthValue();
			}
			wsDrugCompareTobuilder.append( wsDrug1StrengthUnit, wsDrug2StrengthUnit);
			wsDrugCompareTobuilder.append( wsDrug1StrengthValue, wsDrug2StrengthValue);
			
			//Volume
			wsDrugCompareTobuilder.append( wsDrug1.getDmDrug().getVolumeUnit(), wsDrug2.getDmDrug().getVolumeUnit());
			wsDrugCompareTobuilder.append( wsDrug1.getDmDrug().getVolumeValue(), wsDrug2.getDmDrug().getVolumeValue());
			
			//ItemCode
			wsDrugCompareTobuilder.append( wsDrug1.getDmDrug().getItemCode(), wsDrug2.getDmDrug().getItemCode());
			
			return wsDrugCompareTobuilder.toComparison();
		}
	}
	
	@Remove
	public void destroy() {
		if (pmsDosageConversionSet != null) {
			pmsDosageConversionSet = null;
		}
		if (originalDmDrugList != null) {
			originalDmDrugList = null;
		}
		if (extendDmDrugList != null) {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
			extendDmDrugList = null;
		}
		if (pmsDosageConversionSetList != null) {
			pmsDosageConversionSetList = null;
		}
		if ( StringUtils.isNotEmpty(errorCode) ){
			errorCode = "";
		}
		if (errorParam.length > 0){
			errorParam = new String[]{};
		}
	}	
}
