package hk.org.ha.model.pms.vo.reftable;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

import hk.org.ha.model.pms.udt.YesNoFlag;
import hk.org.ha.model.pms.udt.reftable.HomeLeaveMappingAction;
import hk.org.ha.model.pms.udt.reftable.HomeLeaveMappingType;

@ExternalizedBean(type = DefaultExternalizer.class)
public class HomeLeaveMapping implements Serializable {

	private static final long serialVersionUID = 1L;

	private String hospCode;
	
	private String workstoreCode;
	
	private HomeLeaveMappingType defaultType;
	
	private YesNoFlag homeLeaveFlag;
	
	private String pasSpecCode;
	
	private String pasDescription;
	
	private HomeLeaveMappingAction homeLeaveMappingAction;

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public YesNoFlag getHomeLeaveFlag() {
		return homeLeaveFlag;
	}

	public void setHomeLeaveFlag(YesNoFlag homeLeaveFlag) {
		this.homeLeaveFlag = homeLeaveFlag;
	}

	public String getPasSpecCode() {
		return pasSpecCode;
	}

	public void setPasSpecCode(String pasSpecCode) {
		this.pasSpecCode = pasSpecCode;
	}

	public String getPasDescription() {
		return pasDescription;
	}

	public void setPasDescription(String pasDescription) {
		this.pasDescription = pasDescription;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public HomeLeaveMappingType getDefaultType() {
		return defaultType;
	}

	public void setDefaultType(HomeLeaveMappingType defaultType) {
		this.defaultType = defaultType;
	}

	public HomeLeaveMappingAction getHomeLeaveMappingAction() {
		return homeLeaveMappingAction;
	}

	public void setHomeLeaveMappingAction(
			HomeLeaveMappingAction homeLeaveMappingAction) {
		this.homeLeaveMappingAction = homeLeaveMappingAction;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((hospCode == null) ? 0 : hospCode.hashCode());
		result = prime * result
				+ ((pasSpecCode == null) ? 0 : pasSpecCode.hashCode());
		result = prime * result
				+ ((workstoreCode == null) ? 0 : workstoreCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HomeLeaveMapping other = (HomeLeaveMapping) obj;
		if (hospCode == null) {
			if (other.hospCode != null)
				return false;
		} else if (!hospCode.equals(other.hospCode))
			return false;
		if (pasSpecCode == null) {
			if (other.pasSpecCode != null)
				return false;
		} else if (!pasSpecCode.equals(other.pasSpecCode))
			return false;
		if (workstoreCode == null) {
			if (other.workstoreCode != null)
				return false;
		} else if (!workstoreCode.equals(other.workstoreCode))
			return false;
		return true;
	}
}
