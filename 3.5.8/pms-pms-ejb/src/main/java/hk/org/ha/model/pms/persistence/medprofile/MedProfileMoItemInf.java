package hk.org.ha.model.pms.persistence.medprofile;

import hk.org.ha.model.pms.udt.YesNoBlankFlag;
import hk.org.ha.model.pms.udt.medprofile.MedProfileMoItemStatus;
import hk.org.ha.model.pms.udt.rx.RxItemType;
import hk.org.ha.model.pms.vo.rx.RxItem;

import java.util.Date;

public interface MedProfileMoItemInf {

	public Date getOrderDate();

	public void setOrderDate(Date orderDate);
	
	public Integer getOrgItemNum();

	public void setOrgItemNum(Integer orgItemNum);

	public Integer getItemNum();

	public void setItemNum(Integer itemNum);

	public RxItemType getRxItemType();

	public void setRxItemType(RxItemType rxItemType);

	public Date getStartDate();

	public void setStartDate(Date startDate);
	
	public Date getEndDate();

	public void setEndDate(Date endDate);

	public Integer getUnitOfCharge();

	public void setUnitOfCharge(Integer unitOfCharge);

	public YesNoBlankFlag getChargeFlag();

	public void setChargeFlag(YesNoBlankFlag chargeFlag);
	
	public Boolean getStatFlag();

	public void setStatFlag(Boolean statFlag);

	public Boolean getPpmiFlag();

	public void setPpmiFlag(Boolean ppmiFlag);
	
	public Boolean getAlertFlag();

	public void setAlertFlag(Boolean alertFlag);
	
	public Boolean getFmFlag();

	public void setFmFlag(Boolean fmFlag);

	public String getDoctorCode();

	public void setDoctorCode(String doctorCode);

	public String getDoctorRankCode();

	public void setDoctorRankCode(String doctorRankCode);

	public String getDoctorName();

	public void setDoctorName(String doctorName);
	
	public MedProfileMoItemStatus getStatus();

	public void setStatus(MedProfileMoItemStatus status);

	public RxItem getRxItem();

	public void setRxItem(RxItem rxItem);
}
