package hk.org.ha.model.pms.biz.checkissue;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.order.DispOrderManagerLocal;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.biz.sys.SystemMessageManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.BatchIssueLog;
import hk.org.ha.model.pms.persistence.disp.BatchIssueLogDetail;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.checkissue.BatchIssueDetail;
import hk.org.ha.model.pms.vo.checkissue.BatchIssueOrder;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.report.BatchIssueRpt;
import hk.org.ha.model.pms.vo.report.BatchIssueRptDtl;
import hk.org.ha.model.pms.vo.report.BatchIssueRptHdr;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.security.Identity;

@Stateful
@Scope(ScopeType.SESSION)
@Name("batchIssueService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class BatchIssueServiceBean implements BatchIssueServiceLocal {

	@PersistenceContext
	private EntityManager em;

	@In
	private Workstation workstation; 
	
	@In
	private Workstore workstore;
	
	@In
	private Identity identity;
	
	@In
	private SystemMessageManagerLocal systemMessageManager;

	@In
	private DispOrderManagerLocal dispOrderManager;
	
	@In
	private PrintAgentInf printAgent;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private OperationProfile activeProfile;
	
	@Out(required = false)
	private BatchIssueDetail batchIssueDetail;
	
	private boolean result;
	
	private String errMsgCode;
	
	private String workstationCode;
	
	private boolean showExceptionList;

	private static final String BATCHISSUERPT = "BatchIssueRpt";
	private static final String BATCHISSUESFIRPT = "BatchIssueSfiRpt";
	private static final String BATCHISSUEEXCEPTIONRPT = "BatchIssueExceptionRpt";

	@SuppressWarnings("unchecked")
	private BatchIssueLog retrieveBatchIssueLog(Date issueDate, Workstore workstore, boolean lockBatchIssueLog){
		BatchIssueLog batchIssueLog = null;

		Query query = em.createQuery(
						"select o from BatchIssueLog o" + // 20120214 index check : BatchIssueLog.workstore,issueDate : UI_BATCH_ISSUE_LOG_01
						" where o.issueDate = :issueDate" + 
						" and o.workstore = :workstore")
						.setParameter("issueDate", issueDate, TemporalType.DATE)
						.setParameter("workstore", workstore);
		
		if ( lockBatchIssueLog ) {
			query.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock);
		}
		
		List<BatchIssueLog> batchIssueLogList = query.getResultList();
		
		if ( !batchIssueLogList.isEmpty() ) {
			batchIssueLog = batchIssueLogList.get(0);
			batchIssueLog.getBatchIssueLogDetailList().size();
			batchIssueLog.getWorkstore();
			batchIssueLog.getUpdateWorkstation();
			
			for ( BatchIssueLogDetail bacthIssueLogDetail : batchIssueLog.getBatchIssueLogDetailList() ) {
				bacthIssueLogDetail.getBatchIssueLog();
				bacthIssueLogDetail.getDispOrder();
				bacthIssueLogDetail.getDispOrder().getTicket();
				bacthIssueLogDetail.getDispOrder().getPharmOrder();
				bacthIssueLogDetail.getDispOrder().getPharmOrder().getMedCase();
				bacthIssueLogDetail.getDispOrder().getPharmOrder().getMedOrder();
				bacthIssueLogDetail.getDispOrder().getPharmOrder().getPatient();
			}
		}
		return batchIssueLog;
	}
	
	private void updateBatchIssueLog(BatchIssueLog batchIssueLog, int issueCount, int errorCount){

		batchIssueLog.setIssueCount(Integer.valueOf(issueCount));
		batchIssueLog.setErrorCount(Integer.valueOf(errorCount));
		batchIssueLog.setUpdateWorkstation(workstation);
		
		if ( batchIssueLog.getId() == null ) {
			em.persist(batchIssueLog);
		} else {
			em.merge(batchIssueLog);
		}

		em.flush();
	}
	
	public void updateBatchIssueLogWorkstation(Date issueDate, Workstore workstore, BatchIssueDetail updateBatchIssueDetail, String action){
		BatchIssueLog batchIssueLog = retrieveBatchIssueLog(issueDate, workstore, false);
		if (batchIssueLog!=null){
			if (batchIssueLog.getIssueCount()==0 && batchIssueLog.getErrorCount() == 0){
				updateBatchIssueLog(batchIssueLog, 0, 0);
			}
		}
		if (StringUtils.equals("retrieve", action)){
			retrieveBatchIssueDetail(issueDate, workstore);
		} else if (StringUtils.equals("update", action)){
			updateBatchIssueLogDetail(updateBatchIssueDetail);
		}
	}
	
	@SuppressWarnings("unchecked") 
	public void retrieveBatchIssueDetail(Date issueDate, Workstore workstore) {
		batchIssueDetail = new BatchIssueDetail();
		batchIssueDetail.setIssueDate(issueDate);
		Workstore batchIssueWorkstore = em.find(Workstore.class, workstore.getId());
		batchIssueDetail.setWorkstore(batchIssueWorkstore);		
		
		BatchIssueLog batchIssueLog = retrieveBatchIssueLog(issueDate, workstore, false);

		if (batchIssueLog != null ) {
			batchIssueLog.getBatchIssueLogDetailList().size();
			batchIssueDetail = checkBatchIssueLog(batchIssueLog, batchIssueDetail);
		} else {
			batchIssueDetail.setEnableIssue(true);
		}

		if ( batchIssueDetail.isEnableIssue() || batchIssueDetail.isOverrideBatchIssueLogFlag()) {
			
			List<DispOrderStatus> statusList = new ArrayList<DispOrderStatus>();
			statusList.add(DispOrderStatus.Vetted);
			statusList.add(DispOrderStatus.Picking);
			statusList.add(DispOrderStatus.Picked);
			statusList.add(DispOrderStatus.Assembling);
			statusList.add(DispOrderStatus.Assembled);
			statusList.add(DispOrderStatus.Checked);

			List<DispOrder> dispOrderList = em.createQuery(
				"select o from DispOrder o " + // 20120225 index check : Ticket.workstore,orderType,ticketDate : UI_TICKET_01
				" where o.ticket.workstore = :workstore" +
				" and o.ticket.orderType = :orderType" +
				" and o.ticket.ticketDate = :ticketDate" + 
				" and o.status in :dispOrderStatusList" +
				" and o.adminStatus <> :dispOrderAdminStatus" +
				" and o.status <> :dispOrderStatus")
				.setParameter("workstore", workstore)
				.setParameter("orderType", OrderType.OutPatient)
				.setParameter("ticketDate", issueDate)
				.setParameter("dispOrderStatusList", statusList)
				.setParameter("dispOrderAdminStatus", DispOrderAdminStatus.Suspended)
				.setParameter("dispOrderStatus", DispOrderStatus.SysDeleted)
				.getResultList();

			if ( !dispOrderList.isEmpty() ) {
				
				if (batchIssueDetail.isEnableIssue() && batchIssueLog == null){
					batchIssueLog = new BatchIssueLog();
					batchIssueLog.setIssueDate(issueDate);
					batchIssueLog.setWorkstore(batchIssueWorkstore);
					updateBatchIssueLog(batchIssueLog, 0, 0);
				}
				
				for (DispOrder dispOrder : dispOrderList) { 
					BatchIssueOrder batchIssueOrder = setBatchIssueListItem(dispOrder, dispOrder.getStatus());
					if (dispOrder.getSfiFlag()){
						batchIssueDetail.getBatchIssueSfiPrescList().add(batchIssueOrder);
					}
					batchIssueDetail.getBatchIssueList().add(batchIssueOrder);
				}
			} else {
				batchIssueDetail.setEnableIssue(false);
				if (batchIssueLog != null ){
					this.deleteBatchIssueLog(issueDate, workstore);
				}
			}
		}
	}
	
	public void deleteBatchIssueLog(Date issueDate, Workstore workstore){
		BatchIssueLog batchIssueLog = retrieveBatchIssueLog(issueDate, workstore, false);
		errMsgCode = "";
		workstationCode = "";
		result = true;
		
		if ( batchIssueLog != null ) {
			if ( batchIssueLog.getUpdateWorkstation().getId().equals(workstation.getId()) && batchIssueLog.getIssueCount() == 0 && batchIssueLog.getErrorCount() == 0) {
				em.remove(batchIssueLog);
				em.flush();
				result = true;
			} else {
				result = false;
				errMsgCode = "0104";
				workstationCode = batchIssueLog.getUpdateWorkstation().getWorkstationCode();
			}
		}
	}
	
	private BatchIssueDetail checkBatchIssueLog(BatchIssueLog batchIssueLog, BatchIssueDetail batchIssueDetail) {
		if (batchIssueLog.getIssueCount()>0 || batchIssueLog.getErrorCount() > 0){
			batchIssueDetail.setEnableIssue(false);
			batchIssueDetail.setOverrideBatchIssueLogFlag(false);
			batchIssueDetail.setBatchIssueLogDetailList(batchIssueLog.getBatchIssueLogDetailList());

			for (BatchIssueLogDetail batchIssueLogDetail : batchIssueLog.getBatchIssueLogDetailList()) { 

				if (batchIssueLogDetail.getIssueFlag()){
					batchIssueDetail.getBatchIssueList().add(setBatchIssueListItem(batchIssueLogDetail.getDispOrder(), DispOrderStatus.Issued));
				} else {
					batchIssueDetail.getBatchIssueList().add(setBatchIssueListItem(batchIssueLogDetail.getDispOrder(), batchIssueLogDetail.getStatus()));
				}
			}
			
		} else {
			if ( batchIssueLog.getUpdateWorkstation().getId().equals(workstation.getId()) ) {
				batchIssueDetail.setEnableIssue(true);
			} else {
				batchIssueDetail.setEnableIssue(false);
				batchIssueDetail.setOverrideBatchIssueLogFlag(true);
			}
		}
		
		return batchIssueDetail;
	}
	
	private BatchIssueOrder setBatchIssueListItem(DispOrder dispOrder, DispOrderStatus status){
			BatchIssueOrder batchIssueOrder = new BatchIssueOrder();
			batchIssueOrder.setTicketNum(dispOrder.getTicket().getTicketNum());
			if ( dispOrder.getPharmOrder().getMedOrder().getDocType() == MedOrderDocType.Normal ) {
				batchIssueOrder.setRefNum(dispOrder.getPharmOrder().getMedOrder().getRefNum());
			}
			batchIssueOrder.setPatName(dispOrder.getPharmOrder().getPatient().getName());
			batchIssueOrder.setPatNameChi(dispOrder.getPharmOrder().getPatient().getNameChi());
			batchIssueOrder.setHkid(dispOrder.getPharmOrder().getPatient().getHkid());
			if (dispOrder.getPharmOrder().getMedCase()!=null) {
				batchIssueOrder.setCaseNum(dispOrder.getPharmOrder().getMedCase().getCaseNum());
				batchIssueOrder.setPasSpecCode(dispOrder.getPharmOrder().getMedCase().getPasSpecCode());
				batchIssueOrder.setPasSubSpecCode(dispOrder.getPharmOrder().getMedCase().getPasSubSpecCode());
			}
			batchIssueOrder.setTicketDate(dispOrder.getTicket().getTicketDate());
			batchIssueOrder.setStatus(status);
			batchIssueOrder.setSfiFlag(dispOrder.getSfiFlag());
			batchIssueOrder.setSpecCode(dispOrder.getPharmOrder().getSpecCode());
			batchIssueOrder.setWard(dispOrder.getPharmOrder().getWardCode());
			batchIssueOrder.setPatCatCode(dispOrder.getPharmOrder().getPatCatCode());
			batchIssueOrder.setDispOrder(dispOrder);
			return batchIssueOrder;
	} 
	
	@SuppressWarnings("unchecked")
	public void updateBatchIssueLogDetail(BatchIssueDetail updateBatchIssueDetail){
		
		BatchIssueLog batchIssueLog = retrieveBatchIssueLog(updateBatchIssueDetail.getIssueDate(), updateBatchIssueDetail.getWorkstore(), true);
		
		BatchIssueDetail batchIssueDetail = new BatchIssueDetail();
		
		if (batchIssueLog != null ) {
			batchIssueDetail = checkBatchIssueLog(batchIssueLog, batchIssueDetail);
		} else {
			batchIssueDetail.setEnableIssue(true);
			batchIssueLog = new BatchIssueLog();
			batchIssueLog.setIssueDate(updateBatchIssueDetail.getIssueDate());
			batchIssueLog.setWorkstore(updateBatchIssueDetail.getWorkstore());
		}
		
		List<DispOrder> batchIssueOrderList = new ArrayList<DispOrder>();
		List<Long> dispOrderIdList = new ArrayList<Long>();

		if (batchIssueDetail.isEnableIssue()) {
			int issueCount = 0;
			int errCount = 0;
			
			for ( BatchIssueOrder batchIssueOrder : updateBatchIssueDetail.getBatchIssueList() ) {
				dispOrderIdList.add(batchIssueOrder.getDispOrder().getId());
			}
			
			List<DispOrder> dispOrderList = em.createQuery(
					"select o from DispOrder o" + // 20150204 index check : DispOrder.id : PK_DISP_ORDER
					" where o.id in :dispOrderIdList")
					.setParameter("dispOrderIdList", dispOrderIdList)
					.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
					.getResultList();
			
			Map<Long, DispOrder> dispOrderMap = new HashMap<Long, DispOrder>();
			for ( DispOrder dispOrder : dispOrderList ) {
				dispOrderMap.put(dispOrder.getId(), dispOrder);
			}
			
			for ( BatchIssueOrder batchIssueOrder : updateBatchIssueDetail.getBatchIssueList() ) {
				DispOrder updateDispOrder = batchIssueOrder.getDispOrder();
				BatchIssueLogDetail batchIssueLogDetail = new BatchIssueLogDetail();
				
				batchIssueLogDetail.setDispOrder(updateDispOrder);
				
				if (updateDispOrder.getSfiFlag()) {
					//Rx contains Self-financed item
					batchIssueLogDetail.setStatus(updateDispOrder.getStatus());
					batchIssueLogDetail.setIssueFlag(Boolean.FALSE);
					batchIssueLogDetail.setResultMsg(systemMessageManager.retrieveMessageDesc("0101"));
					errCount++;
				} else {
					DispOrder dispOrder = dispOrderMap.get(updateDispOrder.getId());
					
					//check status change
					if ( !updateDispOrder.getStatus().getDisplayValue().equals(dispOrder.getStatus().getDisplayValue()) ) {
						
						batchIssueLogDetail.setStatus(updateDispOrder.getStatus());
						batchIssueLogDetail.setIssueFlag(Boolean.FALSE);
						batchIssueLogDetail.setResultMsg(systemMessageManager.resolve(("0103"),dispOrder.getStatus().getDisplayValue()));
						errCount++;
					} else if ( dispOrder.getAdminStatus().equals(DispOrderAdminStatus.Suspended) ) {
						
						batchIssueLogDetail.setStatus(updateDispOrder.getStatus());
						batchIssueLogDetail.setIssueFlag(Boolean.FALSE);
						batchIssueLogDetail.setResultMsg(systemMessageManager.resolve("0103", dispOrder.getAdminStatus().getDisplayValue()));
						errCount++;
					} else if ( dispOrder.getStatus() == DispOrderStatus.SysDeleted ) {
						
						batchIssueLogDetail.setStatus(updateDispOrder.getStatus());
						batchIssueLogDetail.setIssueFlag(Boolean.FALSE);
						batchIssueLogDetail.setResultMsg(systemMessageManager.resolve("0103", dispOrder.getStatus().getDisplayValue()));
						errCount++;
					} else {
						batchIssueOrderList.add(dispOrder);
						batchIssueLogDetail.setStatus(updateDispOrder.getStatus());
						batchIssueLogDetail.setIssueFlag(Boolean.TRUE);
						batchIssueLogDetail.setResultMsg(systemMessageManager.retrieveMessageDesc("0102"));
						issueCount++;
					}
				}
				
				batchIssueLog.addBatchIssueLogDetail(batchIssueLogDetail);
			}
			
			updateBatchIssueLog(batchIssueLog, issueCount, errCount);
			
			if ( !batchIssueOrderList.isEmpty() ) {
				dispOrderManager.endDispensingForBatchIsuse(batchIssueOrderList);
			}
			
			if ( errCount > 0 ){
				showExceptionList = true;
			} else {
				showExceptionList = false;
			}
			result = true;
			errMsgCode = "";
			
			em.clear();
			retrieveBatchIssueDetail(updateBatchIssueDetail.getIssueDate(), updateBatchIssueDetail.getWorkstore());
		} else if (batchIssueDetail.isOverrideBatchIssueLogFlag()){
			result = false;
			errMsgCode = "0107";
		} else {
			result = false;
			errMsgCode = "0109";
		}

	}

	public boolean isResult() {
		return result;
	}

	public String getErrMsgCode() {
		return errMsgCode;
	}

	public boolean isShowExceptionList() {
		return showExceptionList;
	}

	public String getWorkstationCode() {
		return workstationCode;
	} 
	
	public BatchIssueRpt retrieveBatchIssueSfiRpt(){
		BatchIssueRpt batchIssueRpt = new BatchIssueRpt();
		BatchIssueRptHdr batchIssueRptHdr = retrieveBatchIssueRptHdr();
		List<BatchIssueRptDtl> batchIssueRptDtlList = new ArrayList<BatchIssueRptDtl>();
		
		for (BatchIssueOrder batchIssueOrder : batchIssueDetail.getBatchIssueSfiPrescList()){
			BatchIssueRptDtl batchIssueRptDtl = new BatchIssueRptDtl();
			batchIssueRptDtl.setTicketNum(batchIssueOrder.getTicketNum());
			batchIssueRptDtl.setRefNum(batchIssueOrder.getRefNum());
			batchIssueRptDtl.setPatName(batchIssueOrder.getPatName());
			batchIssueRptDtl.setPatNameChi(batchIssueOrder.getPatNameChi());
			batchIssueRptDtl.setHkid(batchIssueOrder.getHkid());
			batchIssueRptDtl.setCaseNum(batchIssueOrder.getCaseNum());
			batchIssueRptDtl.setDispOrderStatus(batchIssueOrder.getStatus());
			batchIssueRptDtl.setSpecCode(batchIssueOrder.getSpecCode());
			batchIssueRptDtl.setPasSpecCode(batchIssueOrder.getPasSpecCode());
			batchIssueRptDtl.setPasSubSpecCode(batchIssueOrder.getPasSubSpecCode());
			batchIssueRptDtl.setPatCatCode(batchIssueOrder.getPatCatCode());
			batchIssueRptDtl.setWard(batchIssueOrder.getWard());
			batchIssueRptDtl.setIssueFlag("N");
			batchIssueRptDtlList.add(batchIssueRptDtl);
			
		}
		
		batchIssueRpt.setBatchIssueHdr(batchIssueRptHdr);
		batchIssueRpt.setBatchIssueRptDtlList(batchIssueRptDtlList);
		return batchIssueRpt;
	}
	
	public BatchIssueRpt retrieveBatchIssueExceptionRpt(){
		BatchIssueRpt batchIssueRpt = new BatchIssueRpt();
		BatchIssueRptHdr batchIssueRptHdr = retrieveBatchIssueRptHdr();
		List<BatchIssueRptDtl> batchIssueRptDtlList = new ArrayList<BatchIssueRptDtl>();
		
		BatchIssueLog batchIssueLog = retrieveBatchIssueLog(batchIssueDetail.getIssueDate(), batchIssueDetail.getWorkstore(), false);
		batchIssueRptHdr.setProcessDate(batchIssueLog.getUpdateDate());
		batchIssueRptHdr.setProcessUser(batchIssueLog.getUpdateUser());
		
		for ( BatchIssueLogDetail batchIssueLogDetail : batchIssueLog.getBatchIssueLogDetailList() ) {
			if ( !batchIssueLogDetail.getIssueFlag() ) {
				BatchIssueRptDtl batchIssueRptDtl = new BatchIssueRptDtl();
				batchIssueRptDtl.setTicketNum(batchIssueLogDetail.getDispOrder().getTicket().getTicketNum());
				batchIssueRptDtl.setRefNum(batchIssueLogDetail.getDispOrder().getPharmOrder().getMedOrder().getRefNum());
				batchIssueRptDtl.setPatName(batchIssueLogDetail.getDispOrder().getPharmOrder().getPatient().getName());
				batchIssueRptDtl.setPatNameChi(batchIssueLogDetail.getDispOrder().getPharmOrder().getPatient().getNameChi());
				batchIssueRptDtl.setHkid(batchIssueLogDetail.getDispOrder().getPharmOrder().getPatient().getHkid());
				
				if (batchIssueLogDetail.getDispOrder().getPharmOrder().getMedCase()!=null) {
					batchIssueRptDtl.setCaseNum(batchIssueLogDetail.getDispOrder().getPharmOrder().getMedCase().getCaseNum());
					batchIssueRptDtl.setPasSpecCode(batchIssueLogDetail.getDispOrder().getPharmOrder().getMedCase().getPasSpecCode());
					batchIssueRptDtl.setPasSubSpecCode(batchIssueLogDetail.getDispOrder().getPharmOrder().getMedCase().getPasSubSpecCode());
				}
				batchIssueRptDtl.setSpecCode(batchIssueLogDetail.getDispOrder().getPharmOrder().getSpecCode());
				batchIssueRptDtl.setWard(batchIssueLogDetail.getDispOrder().getPharmOrder().getWardCode());
				batchIssueRptDtl.setPatCatCode(batchIssueLogDetail.getDispOrder().getPharmOrder().getPatCatCode());
				batchIssueRptDtl.setDispOrderStatus(batchIssueLogDetail.getStatus());
				batchIssueRptDtl.setIssueFlag("N");
				batchIssueRptDtl.setResultMsg(batchIssueLogDetail.getResultMsg());
				
				batchIssueRptDtlList.add(batchIssueRptDtl);
			}

		}
		
		batchIssueRpt.setBatchIssueHdr(batchIssueRptHdr);
		batchIssueRpt.setBatchIssueRptDtlList(batchIssueRptDtlList);
		return batchIssueRpt;
	}
	
	private BatchIssueRptHdr retrieveBatchIssueRptHdr(){
		
		BatchIssueRptHdr batchIssueRptHdr = new BatchIssueRptHdr();
		batchIssueRptHdr.setHospCode(batchIssueDetail.getWorkstore().getHospCode());
		batchIssueRptHdr.setWorkstoreCode(batchIssueDetail.getWorkstore().getWorkstoreCode());
		batchIssueRptHdr.setActiveProfileName(activeProfile.getName());
		batchIssueRptHdr.setPrintUser(identity.getCredentials().getUsername());
		batchIssueRptHdr.setTicketDate(batchIssueDetail.getIssueDate());
		
		return batchIssueRptHdr;
	}
	
	public BatchIssueRpt retrieveBatchIssueRpt(){

		BatchIssueRpt batchIssueRpt = new BatchIssueRpt();
		BatchIssueRptHdr batchIssueRptHdr = retrieveBatchIssueRptHdr();
		
		BatchIssueLog batchIssueLog = retrieveBatchIssueLog(batchIssueDetail.getIssueDate(), batchIssueDetail.getWorkstore(), false);
		
		List<BatchIssueRptDtl> batchIssueRptDtlList = new ArrayList<BatchIssueRptDtl>();

		if(batchIssueLog != null && (batchIssueLog.getIssueCount() > 0 || batchIssueLog.getErrorCount() > 0) ) {
			batchIssueRptHdr.setProcessDate(batchIssueLog.getUpdateDate());
			batchIssueRptHdr.setProcessUser(batchIssueLog.getUpdateUser());
			
			for ( BatchIssueLogDetail batchIssueLogDetail : batchIssueLog.getBatchIssueLogDetailList() ) {
				BatchIssueRptDtl batchIssueRptDtl = new BatchIssueRptDtl();
				batchIssueRptDtl.setTicketNum(batchIssueLogDetail.getDispOrder().getTicket().getTicketNum());
				batchIssueRptDtl.setRefNum(batchIssueLogDetail.getDispOrder().getPharmOrder().getMedOrder().getRefNum());
				batchIssueRptDtl.setPatName(batchIssueLogDetail.getDispOrder().getPharmOrder().getPatient().getName());
				batchIssueRptDtl.setPatNameChi(batchIssueLogDetail.getDispOrder().getPharmOrder().getPatient().getNameChi());
				batchIssueRptDtl.setHkid(batchIssueLogDetail.getDispOrder().getPharmOrder().getPatient().getHkid());
				
				if (batchIssueLogDetail.getDispOrder().getPharmOrder().getMedCase()!=null) {
					batchIssueRptDtl.setCaseNum(batchIssueLogDetail.getDispOrder().getPharmOrder().getMedCase().getCaseNum());
					batchIssueRptDtl.setPasSpecCode(batchIssueLogDetail.getDispOrder().getPharmOrder().getMedCase().getPasSpecCode());
					batchIssueRptDtl.setPasSubSpecCode(batchIssueLogDetail.getDispOrder().getPharmOrder().getMedCase().getPasSubSpecCode());
				}
				batchIssueRptDtl.setSpecCode(batchIssueLogDetail.getDispOrder().getPharmOrder().getSpecCode());
				batchIssueRptDtl.setWard(batchIssueLogDetail.getDispOrder().getPharmOrder().getWardCode());
				batchIssueRptDtl.setPatCatCode(batchIssueLogDetail.getDispOrder().getPharmOrder().getPatCatCode());

				if (batchIssueLogDetail.getIssueFlag()){
					batchIssueRptDtl.setDispOrderStatus(DispOrderStatus.Issued);
					batchIssueRptDtl.setIssueFlag("Y");
				} else {
					batchIssueRptDtl.setDispOrderStatus(batchIssueLogDetail.getStatus());
					batchIssueRptDtl.setIssueFlag("N");
					batchIssueRptDtl.setResultMsg(batchIssueLogDetail.getResultMsg());
				}
				
				batchIssueRptDtlList.add(batchIssueRptDtl);
			}

		} else {
			for ( BatchIssueOrder batchIssueOrder : batchIssueDetail.getBatchIssueList() ) {
				BatchIssueRptDtl batchIssueRptDtl = new BatchIssueRptDtl();
				batchIssueRptDtl.setTicketNum(batchIssueOrder.getTicketNum());
				batchIssueRptDtl.setRefNum(batchIssueOrder.getRefNum());
				batchIssueRptDtl.setPatName(batchIssueOrder.getPatName());
				batchIssueRptDtl.setPatNameChi(batchIssueOrder.getPatNameChi());
				batchIssueRptDtl.setHkid(batchIssueOrder.getHkid());
				batchIssueRptDtl.setCaseNum(batchIssueOrder.getCaseNum());
				batchIssueRptDtl.setDispOrderStatus(batchIssueOrder.getStatus());
				batchIssueRptDtl.setSpecCode(batchIssueOrder.getSpecCode());
				batchIssueRptDtl.setPasSpecCode(batchIssueOrder.getPasSpecCode());
				batchIssueRptDtl.setPasSubSpecCode(batchIssueOrder.getPasSubSpecCode());
				batchIssueRptDtl.setPatCatCode(batchIssueOrder.getPatCatCode());
				batchIssueRptDtl.setWard(batchIssueOrder.getWard());
				batchIssueRptDtl.setIssueFlag("N");
				batchIssueRptDtlList.add(batchIssueRptDtl);
			}
		}

		batchIssueRpt.setBatchIssueHdr(batchIssueRptHdr);
		batchIssueRpt.setBatchIssueRptDtlList(batchIssueRptDtlList);

		return batchIssueRpt;
	}
	
	public void printBatchIssueRpt(String rptDocType)
	{
		List<BatchIssueRpt> batchIssueRptList = new ArrayList<BatchIssueRpt>();

		if (BATCHISSUERPT.equals(rptDocType)){
			batchIssueRptList.add(retrieveBatchIssueRpt());
		} if (BATCHISSUESFIRPT.equals(rptDocType)){
			batchIssueRptList.add(retrieveBatchIssueSfiRpt());
		} else if (BATCHISSUEEXCEPTIONRPT.equals(rptDocType)){
			batchIssueRptList.add(retrieveBatchIssueExceptionRpt());
		}

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("hospCode", workstore.getHospCode());
		parameters.put("workstoreCode", workstore.getWorkstoreCode());

		if (!batchIssueRptList.isEmpty()){
		    printAgent.renderAndPrint(new RenderAndPrintJob(
		    		rptDocType,
				    new PrintOption(),
				    printerSelector.retrievePrinterSelect(PrintDocType.Report),
				    parameters,
				    batchIssueRptList));
		}
	}

	@Remove
	public void destroy() 
	{
		if (batchIssueDetail != null) {
			batchIssueDetail = null;
		}
	}
}
