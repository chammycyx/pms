package hk.org.ha.model.pms.vo.alert;

import java.io.Serializable;

import hk.org.ha.model.pms.udt.alert.Severity;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class SeverityAdapter extends XmlAdapter<String, Severity> implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public String marshal(Severity v) { 
		return v.getDataValue();
	}

	public Severity unmarshal(String v) {
		return Severity.dataValueOf(v);
	}

}
