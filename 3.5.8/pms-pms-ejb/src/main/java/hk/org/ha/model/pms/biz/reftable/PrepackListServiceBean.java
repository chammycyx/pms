package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.Prepack;

import java.util.Collection;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("prepackListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PrepackListServiceBean implements PrepackListServiceLocal {

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<Prepack> prepackList;

	@In
	private Workstore workstore;
	
	@SuppressWarnings("unchecked")
	public void retrievePrepackByItemCode(String itemCode)
	{
		prepackList = em.createQuery(
				"select o from Prepack o" + // 20120613 index check : Prepack.workstore,itemCode : UI_PREPACK_01
				" where o.workstore = :workstore" +
				" and o.itemCode = :itemCode")
				.setParameter("workstore", workstore)
				.setParameter("itemCode", itemCode)
				.getResultList();
	}
	
	public void updatePrepack(Prepack updatePrepack)
	{
		if(updatePrepack.isMarkDelete()){
			em.remove(em.merge(updatePrepack));
		}else if(updatePrepack.getWorkstore() == null) {
			updatePrepack.setWorkstore(workstore);
			em.persist(updatePrepack);
		}else {
			em.merge(updatePrepack);
		}
		em.flush();
		em.clear();
	}

	@Remove
	public void destroy() 
	{
		if (prepackList != null) {
			prepackList = null;
		}
	}
}
