package hk.org.ha.model.pms.vo.alert.mds;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.model.pms.persistence.AlertEntity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class AlertMsg implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final String JAXB_CONTEXT_MDS_ALERT = "hk.org.ha.model.pms.vo.alert.mds";

	@XmlTransient
	private List<AlertEntity> patAlertList;
	
	@XmlTransient
	private List<AlertEntity> ddiAlertList;
	
	@XmlTransient
	private List<AlertEntity> drcAlertList;
	
	@XmlElement
	private String mdsOrderDesc;

	@XmlElement
	private String displayNameSaltForm;
	
	@XmlElement
	private Integer itemNum;
	
	@XmlElement
	private Boolean crossDdiFlag;
		
	@XmlElement(name="AlertXml")
	private List<String> alertXmlList;
	
	public AlertMsg() {
		super();
		patAlertList = new ArrayList<AlertEntity>();
		ddiAlertList = new ArrayList<AlertEntity>();
		drcAlertList = new ArrayList<AlertEntity>();
		crossDdiFlag = false;
	}
	
	public AlertMsg(
			Integer itemNum,
			String mdsOrderDesc,
			String displayNameSaltForm,
			List<AlertEntity> patAlertList,
			List<AlertEntity> ddiAlertList,
			List<AlertEntity> drcAlertList
		) {
		
		this();
		this.itemNum = itemNum;
		this.mdsOrderDesc = mdsOrderDesc;
		this.displayNameSaltForm = displayNameSaltForm;
		
		setPatAlertList(patAlertList);
		setDdiAlertList(ddiAlertList);
		setDrcAlertList(drcAlertList);
		
		loadDrugDesc();
	}
	
	public AlertMsg(
			Integer itemNum,
			String mdsOrderDesc,
			String displayNameSaltForm,
			List<AlertEntity> patAlertList,
			List<AlertEntity> ddiAlertList
		) {
		this(itemNum, mdsOrderDesc, displayNameSaltForm, patAlertList, ddiAlertList, null);
	}
	
	public AlertMsg(
			Integer itemNum,
			String mdsOrderDesc,
			String displayNameSaltForm,
			List<AlertEntity> patAlertList
		) {
		this(itemNum, mdsOrderDesc, displayNameSaltForm, patAlertList, null);
	}
	
	public AlertMsg(
			Integer itemNum,
			String mdsOrderDesc,
			String displayNameSaltForm
		) {
		this(itemNum, mdsOrderDesc, displayNameSaltForm, null);
	}
	
	public List<AlertEntity> getPatAlertList() {
		if (patAlertList == null) {
			return new ArrayList<AlertEntity>();
		}
		else {
			return new ArrayList<AlertEntity>(patAlertList);
		}
	}

	public void setPatAlertList(List<AlertEntity> patAlertList) {
		if (patAlertList == null) {
			this.patAlertList = new ArrayList<AlertEntity>();
		}
		else {
			this.patAlertList = new ArrayList<AlertEntity>(patAlertList);
		}
	}

	public List<AlertEntity> getDdiAlertList() {
		if (ddiAlertList == null) {
			return new ArrayList<AlertEntity>();
		}
		else {
			return new ArrayList<AlertEntity>(ddiAlertList);
		}
	}

	public void setDdiAlertList(List<AlertEntity> ddiAlertList) {
		if (ddiAlertList == null) {
			this.ddiAlertList = new ArrayList<AlertEntity>();
		}
		else {
			this.ddiAlertList = new ArrayList<AlertEntity>(ddiAlertList);
		}
	}
	
	public List<AlertEntity> getDrcAlertList() {
		if (drcAlertList == null) {
			return new ArrayList<AlertEntity>();
		}
		else {
			return new ArrayList<AlertEntity>(drcAlertList);
		}
	}

	public void setDrcAlertList(List<AlertEntity> drcAlertList) {
		if (drcAlertList == null) {
			this.drcAlertList = new ArrayList<AlertEntity>();
		}
		else {
			this.drcAlertList = new ArrayList<AlertEntity>(drcAlertList);
		}
	}

	public String getMdsOrderDesc() {
		return mdsOrderDesc;
	}

	public void setMdsOrderDesc(String mdsOrderDesc) {
		this.mdsOrderDesc = mdsOrderDesc;
	}

	public String getDisplayNameSaltForm() {
		return displayNameSaltForm;
	}

	public void setDisplayNameSaltForm(String displayNameSaltForm) {
		this.displayNameSaltForm = displayNameSaltForm;
	}

	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public Boolean getCrossDdiFlag() {
		return crossDdiFlag;
	}

	public void setCrossDdiFlag(Boolean crossDdiFlag) {
		this.crossDdiFlag = crossDdiFlag;
	}

	public List<String> getAlertXmlList() {
		return alertXmlList;
	}

	public void setAlertXmlList(List<String> alertXmlList) {
		this.alertXmlList = alertXmlList;
	}

	public void saveAlertList() {
		List<AlertEntity> alertEntityList = new ArrayList<AlertEntity>();
		alertEntityList.addAll(patAlertList);
		alertEntityList.addAll(ddiAlertList);
		alertEntityList.addAll(drcAlertList);
		
		alertXmlList = new ArrayList<String>();
		JaxbWrapper<Alert> mdsAlertJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_MDS_ALERT);
		for (AlertEntity alertEntity : alertEntityList) {
			alertXmlList.add(mdsAlertJaxbWrapper.marshall(alertEntity.getAlert()));
		}
	}
	
	public List<Alert> getAlertList() {
		List<Alert> alertList = new ArrayList<Alert>();
		JaxbWrapper<Alert> mdsAlertJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_MDS_ALERT);
		if (alertXmlList != null) {
			for (String alertXml : alertXmlList) {
				Alert alert = mdsAlertJaxbWrapper.unmarshall(alertXml);
				alert.setSortSeq(Long.valueOf(alertXmlList.indexOf(alertXml)));
				alert.loadDrugDesc(displayNameSaltForm);
				alertList.add(alert);
			}
		}
		return alertList;
	}
	
	public void loadDrugDesc() {
		List<AlertEntity> alertEntityList = new ArrayList<AlertEntity>();
		alertEntityList.addAll(patAlertList);
		alertEntityList.addAll(ddiAlertList);
		alertEntityList.addAll(drcAlertList);
		
		for (AlertEntity alertEntity : alertEntityList) {
			alertEntity.getAlert().loadDrugDesc(displayNameSaltForm);
		}
	}
	
	public void loadAlertEntityList() {
		
		patAlertList = new ArrayList<AlertEntity>();
		ddiAlertList = new ArrayList<AlertEntity>();
		drcAlertList = new ArrayList<AlertEntity>();
		
		for (Alert alert : getAlertList()) {
			AlertEntity alertEntity = new AlertEntity(alert);
			if (alertEntity.isPatAlert()) {
				patAlertList.add(alertEntity);
			}
			else if (alertEntity.isDdiAlert()) {
				ddiAlertList.add(alertEntity);
			}
			else if (alertEntity.isDrcAlert()) {
				drcAlertList.add(alertEntity);
			}
		}
	}
}
