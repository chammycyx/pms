package hk.org.ha.model.pms.biz.reftable.pivas;

import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormula;
import hk.org.ha.model.pms.vo.drug.DmDrugLite;
import hk.org.ha.model.pms.vo.reftable.pivas.PivasDrugListInfo;
import hk.org.ha.model.pms.vo.reftable.pivas.PivasFormulaInfo;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

@Local
public interface PivasFormulaServiceLocal {

	public List<PivasFormula> retrievePivasFormulaList(Integer drugKey, String siteCode, String diluentCode, String pivasDosageUnit, BigDecimal concn, String strength, BigDecimal rate);

	public PivasFormula updatePivasFormula(PivasFormula pivasFormula);
	
	public Map<String, DmDrugLite> retrieveDmDrugLiteMap(List<String> itemCodeList);
	
	public boolean checkAnotherActivePivasMergeFormulaExists(PivasFormula pivasFormula);
	
	public void deletePivasFormula(PivasFormula pivasFormula);
	
	public List<PivasFormula> retrieveDrugInfoListByDisplayName(String displayName);

	public PivasFormulaInfo retrievePivasFormulaInfo(Integer drugKey, boolean includeNonPivasSite);
	
	public PivasDrugListInfo retrievePivasDrugListInfoByDrugKeyStrengthDiluentCode(Integer drugKey, String strength, String diluentCode, String siteCode);
	
	public Boolean[] checkPivasItemSuspend(String itemCode);
	
	public String getErrorCode();
	
	public void retrievePivasFormulaForExport();
	
	public void exportPivasFormula();
	
	void destroy();
	
}
 