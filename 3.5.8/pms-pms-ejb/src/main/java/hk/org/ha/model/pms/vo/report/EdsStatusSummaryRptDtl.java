package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class EdsStatusSummaryRptDtl implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private int vettingOrderCount;
	
	private int pickingItemCount;
	
	private int assemblingItemCount;
	
	private int checkingOrderCount;
	
	private int issuingOrderCount;
	
	private int vettedOrderCount;
	
	private int pickedItemCount;
	
	private int assembledItemCount;
	
	private int checkedOrderCount;
	
	private int issuedOrderCount;

	public void setVettingOrderCount(int vettingOrderCount) {
		this.vettingOrderCount = vettingOrderCount;
	}

	public int getVettingOrderCount() {
		return vettingOrderCount;
	}

	public int getPickingItemCount() {
		return pickingItemCount;
	}

	public void setPickingItemCount(int pickingItemCount) {
		this.pickingItemCount = pickingItemCount;
	}

	public int getAssemblingItemCount() {
		return assemblingItemCount;
	}

	public void setAssemblingItemCount(int assemblingItemCount) {
		this.assemblingItemCount = assemblingItemCount;
	}

	public int getCheckingOrderCount() {
		return checkingOrderCount;
	}

	public void setCheckingOrderCount(int checkingOrderCount) {
		this.checkingOrderCount = checkingOrderCount;
	}

	public int getIssuingOrderCount() {
		return issuingOrderCount;
	}

	public void setIssuingOrderCount(int issuingOrderCount) {
		this.issuingOrderCount = issuingOrderCount;
	}

	public int getVettedOrderCount() {
		return vettedOrderCount;
	}

	public void setVettedOrderCount(int vettedOrderCount) {
		this.vettedOrderCount = vettedOrderCount;
	}

	public int getPickedItemCount() {
		return pickedItemCount;
	}

	public void setPickedItemCount(int pickedItemCount) {
		this.pickedItemCount = pickedItemCount;
	}

	public int getAssembledItemCount() {
		return assembledItemCount;
	}

	public void setAssembledItemCount(int assembledItemCount) {
		this.assembledItemCount = assembledItemCount;
	}

	public int getCheckedOrderCount() {
		return checkedOrderCount;
	}

	public void setCheckedOrderCount(int checkedOrderCount) {
		this.checkedOrderCount = checkedOrderCount;
	}

	public int getIssuedOrderCount() {
		return issuedOrderCount;
	}

	public void setIssuedOrderCount(int issuedOrderCount) {
		this.issuedOrderCount = issuedOrderCount;
	}
}
