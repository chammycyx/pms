package hk.org.ha.model.pms.udt.medprofile;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PrintMode implements StringValuedEnum {

	None("-", "None"),
	Batch("B", "Batch"),
	Direct("D", "Direct");

    private final String dataValue;
    private final String displayValue;
    
	private PrintMode(String dataValue, String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}

	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}
	
    public static PrintMode dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PrintMode.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<PrintMode> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<PrintMode> getEnumClass() {
    		return PrintMode.class;
    	}
    }
}
