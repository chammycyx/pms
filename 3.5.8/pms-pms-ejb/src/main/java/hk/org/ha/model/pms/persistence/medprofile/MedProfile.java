package hk.org.ha.model.pms.persistence.medprofile;

import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.udt.disp.PharmOrderPatType;
import hk.org.ha.model.pms.udt.medprofile.InactiveType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileTransferAlertType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileType;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.vo.alert.AlertProfile;
import hk.org.ha.model.pms.vo.ehr.EhrPatient;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;


@Entity
@Table(name = "MED_PROFILE")
public class MedProfile extends MedProfileOrderEntity {

	private static final long serialVersionUID = 1L;

	public static final int INITIAL_ITEM_NUM = 10001;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "medProfileSeq")
	@SequenceGenerator(name = "medProfileSeq", sequenceName = "SQ_MED_PROFILE", initialValue = 100000000)
	private Long id;

	@Column(name = "ORDER_NUM", nullable = false, length = 36)
	private String orderNum;
	
	@Column(name = "PAT_HOSP_CODE", nullable = false, length = 3)
	private String patHospCode;

	@Converter(name = "MedProfile.type", converterClass = MedProfileType.Converter.class)
	@Convert("MedProfile.type")
	@Column(name = "TYPE", nullable = true, length = 1)
	private MedProfileType type;
	
    @Converter(name = "MedProfile.status", converterClass = MedProfileStatus.Converter.class)
    @Convert("MedProfile.status")
	@Column(name = "STATUS", nullable = false, length = 1)
	private MedProfileStatus status;
    
    @Converter(name = "MedProfile.inactiveType", converterClass = InactiveType.Converter.class)
    @Convert("MedProfile.inactiveType")
    @Column(name = "INACTIVE_TYPE", length = 1)
    private InactiveType inactiveType;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DISCHARGE_DATE")
    private Date dischargeDate;
    
    @Temporal(TemporalType.DATE)
    @Column(name = "KEEP_ACTIVE_UNTIL_DATE")
    private Date keepActiveUntilDate;
    
    @Temporal(TemporalType.DATE)
    @Column(name = "CANCEL_ADMISSION_DATE")
    private Date cancelAdmissionDate;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CANCEL_DISCHARGE_DATE")
    private Date cancelDischargeDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CONFIRM_CANCEL_DISCHARGE_DATE")
    private Date confirmCancelDischargeDate;

    @Column(name = "CONFIRM_CANCEL_DISCHARGE_USER", length = 12)
    private String confirmCancelDischargeUser;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "RESUME_ACTIVE_DATE")
    private Date resumeActiveDate;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "RETURN_DATE")
    private Date returnDate;
    
    @Column(name = "CURR_SEQ_NUM")
    private Integer currSeqNum;
    
    @Column(name = "OUT_CURR_SEQ_NUM")
    private Integer outCurrSeqNum;
    
    @Column(name = "PROCESSING_FLAG", nullable = false)
    private Boolean processingFlag;
    
    @Column(name = "PRIVATE_FLAG", nullable = false)
    private Boolean privateFlag;
    
    @Column(name = "PREGNANCY_CHECK_FLAG", nullable = false)
    private Boolean pregnancyCheckFlag;
    
    @Column(name = "OTHER_DOC", length = 36)
    private String otherDoc;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "HOME_LEAVE_DATE")
    private Date homeLeaveDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LAST_ITEM_DATE")
    private Date lastItemDate;

    @Converter(name = "MedProfile.printLang", converterClass = PrintLang.Converter.class)
    @Convert("MedProfile.printLang")
	@Column(name = "PRINT_LANG", nullable = false, length = 10)
    private PrintLang printLang;
    
    @Column(name = "TRANSFER_FLAG")
    private Boolean transferFlag;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TRANSFER_DATE")
    private Date transferDate;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ITEM_CUT_OFF_DATE")
    private Date itemCutOffDate;
    
    @Column(name = "DELIVERY_GEN_FLAG", nullable = false)
    private Boolean deliveryGenFlag;
    
    @Column(name = "TPN_FLAG")
    private Boolean tpnFlag;
    
    @Column(name = "WORKSTATION_ID")
    private Long workstationId;
    
    @Column(name = "MAX_ITEM_NUM")
    private Integer maxItemNum;
    
    @Column(name = "DIAGNOSIS", nullable = false, length = 200)
    private String diagnosis;
    
    @Column(name = "DRUG_SENSITIVITY", nullable = false, length = 200)
    private String drugSensitivity;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PATIENT_ID", nullable = false)
    private Patient patient;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MED_CASE_ID", nullable = false)
    private MedCase medCase;
    
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "medProfile", fetch = FetchType.LAZY)
    private MedProfileErrorStat medProfileErrorStat;
    
    @OneToMany(mappedBy = "medProfile")
    private List<MedProfileItem> medProfileItemList;
    
    @OneToMany(mappedBy = "medProfile")
    private List<MedProfileStat> medProfileStatList;
    
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE",      referencedColumnName = "HOSP_CODE",      nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;
	
	@Column(name = "HOSP_CODE", nullable = false, insertable = false, updatable = false)
	private String hospCode;	
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ADMISSION_DATE")
    private Date admissionDate;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LAST_INVOICE_PRINT_DATE")
    private Date lastInvoicePrintDate;
    
    @Column(name = "CHARGE_ORDER_NUM", length = 12)
	private String chargeOrderNum;
        
    @Converter(name = "MedProfile.transferAlertType", converterClass = MedProfileTransferAlertType.Converter.class)
    @Convert("MedProfile.transferAlertType")
    @Column(name = "TRANSFER_ALERT_TYPE", length = 1)
    private MedProfileTransferAlertType transferAlertType;
    
    @Column(name = "PROTOCOL_CODE", length = 20)
    private String protocolCode;
    
    @Column(name = "PROTOCOL_VERSION")
    private Integer protocolVersion;
    
	private transient AlertProfile alertProfile;
	private transient Date currentItemCutOffDate;
	
	@Transient
	private String workstationHostName;
	
	@Transient
	private Boolean mdsCheckFlag;
	
	@Transient
	private String pendingUserName;
	
	@Transient
	private String pendingDisplayName;
	
	@Transient
	private String resumePendingUserName;
	
	@Transient
	private String resumePendingDisplayName;
	
	@Transient
	private Boolean inactivePasWardFlag;
	
	@Transient
	private boolean inactivePasWardFlagInited;
	
	@Transient
	private Boolean atdpsWardFlag;
	
	@Transient
	private PharmOrderPatType patType;
	
	@Transient
	private Integer newItemCount;
	
	@Transient
	private Boolean showTransferInfoAlertFlag;
	
	@Transient
	private EhrPatient ehrPatient;
	
	public MedProfile() {
		super();
		type = MedProfileType.Normal;
		status = MedProfileStatus.Active;
		processingFlag = Boolean.FALSE;
		privateFlag = Boolean.FALSE;
		pregnancyCheckFlag = Boolean.FALSE;
		printLang = PrintLang.Eng;
		transferFlag = Boolean.FALSE;
		deliveryGenFlag = Boolean.FALSE;
		tpnFlag = Boolean.FALSE;
		inactivePasWardFlag = Boolean.FALSE;
		newItemCount = 0;
		transferAlertType = MedProfileTransferAlertType.None;
		showTransferInfoAlertFlag = Boolean.FALSE;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}
		
	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}
	
	public MedProfileType getType() {
		return type;
	}

	public void setType(MedProfileType type) {
		this.type = type;
	}

	public MedProfileStatus getStatus() {
		return status;
	}

	public void setStatus(MedProfileStatus status) {
		this.status = status;
	}
	
	public InactiveType getInactiveType() {
		return inactiveType;
	}

	public void setInactiveType(InactiveType inactiveType) {
		this.inactiveType = inactiveType;
	}
	
	public InactiveType resolveInactiveType() {
		return status == MedProfileStatus.Inactive ? (inactiveType == null ? InactiveType.Discharge : inactiveType) : null;
	}
	
	public String resolveStatusString() {
		InactiveType inactiveType = resolveInactiveType();
		return inactiveType != null ? inactiveType.getDisplayValue() : status.getDisplayValue(); 
	}
	
	public boolean isDischarge() {
		return resolveInactiveType() == InactiveType.Discharge;
	}
	
	public boolean isCancelAdmission() {
		return resolveInactiveType() == InactiveType.CancelAdmission;
	}
	
	public boolean isDeleted() {
		return resolveInactiveType() == InactiveType.Deleted;
	}

	public Date getDischargeDate() {
		return dischargeDate == null ? null : new Date(dischargeDate.getTime());
	}

	public void setDischargeDate(Date dischargeDate) {
		this.dischargeDate = dischargeDate == null ? null : new Date(dischargeDate.getTime());
	}
	
	public Date getKeepActiveUntilDate() {
		return keepActiveUntilDate == null ? null : new Date(keepActiveUntilDate.getTime());
	}

	public void setKeepActiveUntilDate(Date keepActiveUntilDate) {
		this.keepActiveUntilDate = keepActiveUntilDate == null ? null : new Date(keepActiveUntilDate.getTime());
	}
	
	public Date getCancelAdmissionDate() {
		return cancelAdmissionDate == null ? null : new Date(cancelAdmissionDate.getTime());
	}
	
	public void setCancelAdmissionDate(Date cancelAdmissionDate) {
		this.cancelAdmissionDate = cancelAdmissionDate == null ? null : new Date(cancelAdmissionDate.getTime());
	}

	public Date getCancelDischargeDate() {
		return cancelDischargeDate == null ? null : new Date(cancelDischargeDate.getTime());
	}

	public void setCancelDischargeDate(Date cancelDischargeDate) {
		this.cancelDischargeDate = cancelDischargeDate == null ? null : new Date(cancelDischargeDate.getTime());
	}

	public Date getConfirmCancelDischargeDate() {
		return confirmCancelDischargeDate == null ? null : new Date(confirmCancelDischargeDate.getTime());
	}

	public void setConfirmCancelDischargeDate(Date confirmCancelDischargeDate) {
		this.confirmCancelDischargeDate = confirmCancelDischargeDate == null ? null : new Date(confirmCancelDischargeDate.getTime());
	}

	public String getConfirmCancelDischargeUser() {
		return confirmCancelDischargeUser;
	}

	public void setConfirmCancelDischargeUser(String confirmCancelDischargeUser) {
		this.confirmCancelDischargeUser = confirmCancelDischargeUser;
	}

	public Date getResumeActiveDate() {
		return resumeActiveDate == null ? null : new Date(resumeActiveDate.getTime());
	}

	public void setResumeActiveDate(Date resumeActiveDate) {
		this.resumeActiveDate = resumeActiveDate == null ? null : new Date(resumeActiveDate.getTime());
	}

	public Date getReturnDate() {
		return returnDate == null ? null : new Date(returnDate.getTime());
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate == null ? null : new Date(returnDate.getTime());
	}
	
	public Integer getCurrSeqNum() {
		return currSeqNum;
	}

	public void setCurrSeqNum(Integer currSeqNum) {
		this.currSeqNum = currSeqNum;
	}
	
	public Integer getOutCurrSeqNum() {
		return outCurrSeqNum;
	}

	public void setOutCurrSeqNum(Integer outCurrSeqNum) {
		this.outCurrSeqNum = outCurrSeqNum;
	}
	
	public Integer incrementOutCurrSeqNum() {
		return outCurrSeqNum = outCurrSeqNum == null ? Integer.valueOf(1) : outCurrSeqNum + 1;
	}
	
	public Boolean getProcessingFlag() {
		return processingFlag;
	}

	public void setProcessingFlag(Boolean processingFlag) {
		this.processingFlag = processingFlag;
	}
	
	public Boolean getPrivateFlag() {
		return privateFlag;
	}

	public void setPrivateFlag(Boolean privateFlag) {
		this.privateFlag = privateFlag;
	}

	public Boolean getPregnancyCheckFlag() {
		return pregnancyCheckFlag;
	}

	public void setPregnancyCheckFlag(Boolean pregnancyCheckFlag) {
		this.pregnancyCheckFlag = pregnancyCheckFlag;
	}
	
	public String getOtherDoc() {
		return otherDoc;
	}

	public void setOtherDoc(String otherDoc) {
		this.otherDoc = otherDoc;
	}
	
	public Date getHomeLeaveDate() {
		return homeLeaveDate;
	}

	public void setHomeLeaveDate(Date homeLeaveDate) {
		this.homeLeaveDate = homeLeaveDate;
	}
	
	public Date getLastItemDate() {
		return lastItemDate;
	}

	public void setLastItemDate(Date lastItemDate) {
		this.lastItemDate = lastItemDate;
	}
	
	public PrintLang getPrintLang() {
		return printLang;
	}

	public void setPrintLang(PrintLang printLang) {
		this.printLang = printLang;
	}
	
	public Boolean getTransferFlag() {
		return transferFlag;
	}

	public void setTransferFlag(Boolean transferFlag) {
		this.transferFlag = transferFlag;
	}
	
	public Date getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(Date transferDate) {
		this.transferDate = transferDate;
	}
	
	public Date getItemCutOffDate() {
		return itemCutOffDate;
	}

	public void setItemCutOffDate(Date itemCutOffDate) {
		this.itemCutOffDate = itemCutOffDate;
	}

	public Boolean getDeliveryGenFlag() {
		return deliveryGenFlag;
	}

	public void setDeliveryGenFlag(Boolean deliveryGenFlag) {
		this.deliveryGenFlag = deliveryGenFlag;
	}
	
	public Boolean getTpnFlag() {
		return tpnFlag;
	}

	public void setTpnFlag(Boolean tpnFlag) {
		this.tpnFlag = tpnFlag;
	}
	
	public Long getWorkstationId() {
		return workstationId;
	}

	public void setWorkstationId(Long workstationId) {
		this.workstationId = workstationId;
	}

	public Integer getMaxItemNum() {
		return maxItemNum;
	}

	public void setMaxItemNum(Integer maxItemNum) {
		this.maxItemNum = maxItemNum;
	}

	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}

	public String getDiagnosis() {
		return diagnosis;
	}

	public void setDrugSensitivity(String drugSensitivity) {
		this.drugSensitivity = drugSensitivity;
	}

	public String getDrugSensitivity() {
		return drugSensitivity;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public MedCase getMedCase() {
		return medCase;
	}

	public void setMedCase(MedCase medCase) {
		this.medCase = medCase;
	}
	
	public MedProfileErrorStat getMedProfileErrorStat() {
		return medProfileErrorStat;
	}

	public void setMedProfileErrorStat(MedProfileErrorStat medProfileErrorStat) {
		this.medProfileErrorStat = medProfileErrorStat;
	}

	public List<MedProfileItem> getMedProfileItemList() {
		if (medProfileItemList == null) {
			medProfileItemList = new ArrayList<MedProfileItem>();
		}
		return medProfileItemList;
	}
	
	public void setMedProfileItemList(List<MedProfileItem> medProfileItemList) {
		this.medProfileItemList = medProfileItemList;
	}
	
	public void addMedProfileItem(MedProfileItem medProfileItem) {
		medProfileItem.setMedProfile(this);
		getMedProfileItemList().add(medProfileItem);
	}
	
	public void clearMedProfileItemList() {
		medProfileItemList = new ArrayList<MedProfileItem>();
	}	
		
	public List<MedProfileStat> getMedProfileStatList() {
		if (medProfileStatList == null) {
			medProfileStatList = new ArrayList<MedProfileStat>();
		}
		return medProfileStatList;
	}
	
	public void setMedProfileStatList(List<MedProfileStat> medProfileStatList) {
		this.medProfileStatList = medProfileStatList;
	}
	
	public void addMedProfileStat(MedProfileStat medProfileStat) {
		medProfileStat.setMedProfile(this);
		getMedProfileStatList().add(medProfileStat);
	}
	
	public void clearMedProfileStatList() {
		medProfileStatList = new ArrayList<MedProfileStat>();
	}	

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}
	
	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public AlertProfile getAlertProfile() {
		return alertProfile;
	}

	public void setAlertProfile(AlertProfile alertProfile) {
		this.alertProfile = alertProfile;
	}
	
	public Date getCurrentItemCutOffDate() {
		return currentItemCutOffDate;
	}

	public void setCurrentItemCutOffDate(Date currentItemCutOffDate) {
		this.currentItemCutOffDate = currentItemCutOffDate;
	}

	public String getWorkstationHostName() {
		return workstationHostName;
	}

	public void setWorkstationHostName(String workstationHostName) {
		this.workstationHostName = workstationHostName;
	}

	public Boolean getMdsCheckFlag() {
		return mdsCheckFlag;
	}

	public void setMdsCheckFlag(Boolean mdsCheckFlag) {
		this.mdsCheckFlag = mdsCheckFlag;
	}
	
	public String getPendingUserName() {
		return pendingUserName;
	}

	public void setPendingUserName(String pendingUserName) {
		this.pendingUserName = pendingUserName;
	}

	public String getPendingDisplayName() {
		return pendingDisplayName;
	}

	public void setPendingDisplayName(String pendingDisplayName) {
		this.pendingDisplayName = pendingDisplayName;
	}

	public String getResumePendingUserName() {
		return resumePendingUserName;
	}

	public void setResumePendingUserName(String resumePendingUserName) {
		this.resumePendingUserName = resumePendingUserName;
	}

	public String getResumePendingDisplayName() {
		return resumePendingDisplayName;
	}

	public void setResumePendingDisplayName(String resumePendingDisplayName) {
		this.resumePendingDisplayName = resumePendingDisplayName;
	}
	
	public void initInactivePasWardFlag(boolean inactivePasWardFlag) {
		this.inactivePasWardFlag = inactivePasWardFlag;
		this.inactivePasWardFlagInited = true;
	}
	
	public Boolean getInactivePasWardFlag() {
		return inactivePasWardFlag;
	}
	
	public boolean isInactivePasWardFlagInited() {
		return inactivePasWardFlagInited;
	}

	public Integer getAndIncreaseMaxItemNum() {
		if (maxItemNum == null) {
			maxItemNum = INITIAL_ITEM_NUM;
		} else {
			maxItemNum = Integer.valueOf(maxItemNum.intValue() + 1);
		}
		return maxItemNum;
	}
	
	public boolean isManualProfile() {
		return MedProfileUtils.isManualProfile(orderNum);
	}

	public void setAtdpsWardFlag(Boolean atdpsWardFlag) {
		this.atdpsWardFlag = atdpsWardFlag;
	}

	public Boolean getAtdpsWardFlag() {
		return atdpsWardFlag;
	}

	public void setAdmissionDate(Date admissionDate) {
		this.admissionDate = admissionDate;
	}

	public Date getAdmissionDate() {
		return admissionDate;
	}
	
	public void refreshPrivateFlag() {
		MedCase medCase = getMedCase();
		if (isManualProfile() && medCase != null && medCase.getPasWardCode() != null) {
			setPrivateFlag(MedCase.PAS_PAY_CODE_PIP.equals(medCase.getPasPayCode()));
		}
	}

	public void setLastInvoicePrintDate(Date lastInvoicePrintDate) {
		this.lastInvoicePrintDate = lastInvoicePrintDate;
	}

	public Date getLastInvoicePrintDate() {
		return lastInvoicePrintDate;
	}

	public void setChargeOrderNum(String chargeOrderNum) {
		this.chargeOrderNum = chargeOrderNum;
	}

	public String getChargeOrderNum() {
		return chargeOrderNum;
	}

	public void setTransferAlertType(MedProfileTransferAlertType transferAlertType) {
		this.transferAlertType = transferAlertType;
	}
	
	public MedProfileTransferAlertType getTransferAlertType() {
		return transferAlertType;
	}
	
	public String getProtocolCode() {
		return protocolCode;
	}

	public void setProtocolCode(String protocolCode) {
		this.protocolCode = protocolCode;
	}
	
	public Integer getProtocolVersion() {
		return protocolVersion;
	}

	public void setProtocolVersion(Integer protocolVersion) {
		this.protocolVersion = protocolVersion;
	}

	public void setPatType(PharmOrderPatType patType) {
		this.patType = patType;
	}

	public PharmOrderPatType getPatType() {
		return patType;
	}

	public Integer getNewItemCount() {
		return newItemCount;
	}

	public void setNewItemCount(Integer newItemCount) {
		this.newItemCount = newItemCount;
	}
	
	public Integer incrementNewItemCount() {
		return newItemCount = newItemCount == null ? Integer.valueOf(1) : newItemCount + 1;
	}
	
	public Boolean getShowTransferInfoAlertFlag() {
		return showTransferInfoAlertFlag;
	}

	public void setShowTransferInfoAlertFlag(Boolean showTransferInfoAlertFlag) {
		this.showTransferInfoAlertFlag = showTransferInfoAlertFlag;
	}

	public EhrPatient getEhrPatient() {
		return ehrPatient;
	}

	public void setEhrPatient(EhrPatient ehrPatient) {
		this.ehrPatient = ehrPatient;
	}	
}
