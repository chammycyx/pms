package hk.org.ha.model.pms.biz.reftable;

import javax.ejb.Local;

@Local
public interface SpecialtyListServiceLocal {
	
	void retrieveSpecialtyList();
	
	void destroy();
	
}
