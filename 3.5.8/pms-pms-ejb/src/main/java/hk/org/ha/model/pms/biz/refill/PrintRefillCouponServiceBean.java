package hk.org.ha.model.pms.biz.refill;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.udt.printing.PrintLang;

import javax.ejb.Remove;
import javax.ejb.Stateless;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;


@AutoCreate
@Stateless
@Name("printRefillCouponService")
@RemoteDestination
@MeasureCalls
public class PrintRefillCouponServiceBean implements PrintRefillCouponServiceLocal{
	
	@In
	private RefillCouponRendererLocal refillCouponRenderer;

	public void printStandardRefillCoupon(RefillSchedule refillScheduleIn,
			boolean isReprint, PrintLang printLang, Integer numOfCopies) {
		refillCouponRenderer.printStandardRefillCoupon(refillScheduleIn, isReprint, printLang, numOfCopies);
	}
	
	@Remove
	public void destroy(){		

	}

}
