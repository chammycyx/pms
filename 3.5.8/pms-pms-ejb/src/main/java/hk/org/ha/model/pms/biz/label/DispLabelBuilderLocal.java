package hk.org.ha.model.pms.biz.label;

import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.vo.label.ChestLabel;
import hk.org.ha.model.pms.vo.label.DispLabel;
import hk.org.ha.model.pms.vo.label.MpDispLabel;

import java.util.Map;

import javax.ejb.Local;

@Local
public interface DispLabelBuilderLocal {

	Map<String, Object> getDispLabelParam(DispLabel dispLabel);
	
	DispLabel convertToDispLabel(DispOrderItem dispOrderItem, Map<String,String> fdnMappingMap);
	
	Map<String, Object> getMpDispLabelParam(MpDispLabel mpDispLabel);
	
	MpDispLabel convertToMpDispLabel(DispOrderItem dispOrderItem);
	
	Map<String, Object> getChestLabelParam(ChestLabel chestLabel);
	
	String getHospitalNameEng(String fullHospitalNameEng);
	
	String getMaskedUserCode(String userCode);
	
	boolean checkRefrigerateWarning(DispOrderItem dispOrderItem);
	
}