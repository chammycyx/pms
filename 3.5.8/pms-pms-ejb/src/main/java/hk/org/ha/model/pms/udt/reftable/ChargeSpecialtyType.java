package hk.org.ha.model.pms.udt.reftable;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum ChargeSpecialtyType implements StringValuedEnum {

	Sfi("P", "SFI"),
	SafetyNet("S", "Safety Net");

    private final String dataValue;
    private final String displayValue;

    ChargeSpecialtyType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }
    
    public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}
	
	public static ChargeSpecialtyType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(ChargeSpecialtyType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<ChargeSpecialtyType> {

		private static final long serialVersionUID = 1L;

		public Class<ChargeSpecialtyType> getEnumClass() {
			return ChargeSpecialtyType.class;
		}
		
	}
}
