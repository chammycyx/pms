package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DupChkDrugType implements StringValuedEnum {

	Oral("O", "Oral"),
	Other("T", "Other");
	
    private final String dataValue;
    private final String displayValue;

    DupChkDrugType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    } 
    
    public static DupChkDrugType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DupChkDrugType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<DupChkDrugType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<DupChkDrugType> getEnumClass() {
    		return DupChkDrugType.class;
    	}
    }
}
