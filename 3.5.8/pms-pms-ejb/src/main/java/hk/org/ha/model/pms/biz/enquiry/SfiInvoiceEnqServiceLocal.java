package hk.org.ha.model.pms.biz.enquiry;

import java.util.Date;
import javax.ejb.Local;

@Local
public interface SfiInvoiceEnqServiceLocal {
	void retrieveSfiInvoiceByInvoiceNumber(String invoiceNum, boolean includeVoidInvoice);	
	void retrieveSfiInvoiceByInvoiceDate(Date invoiceDate, boolean includeVoidInvoice);
	void retrieveSfiInvoiceByMoeOrderNumber(String moeOrderNum, boolean includeVoidInvoice);
	void retrieveSfiInvoiceByCaseNum(String caseNum, boolean includeVoidInvoice);
	void retrieveSfiInvoiceByHkid(String hkid, boolean includeVoidInvoice);
	void retrieveSfiInvoiceByTicket(Date ticketDate, String ticketNum, boolean includeVoidInvoice);	
	void retrieveSfiInvoicePaymentStatus(Long invoiceId);
	void retrieveSfiInvoiceDetail(Long invoiceId, boolean isPreview);
	void destroy();	
}