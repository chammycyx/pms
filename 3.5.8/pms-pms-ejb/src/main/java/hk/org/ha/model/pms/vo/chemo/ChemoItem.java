package hk.org.ha.model.pms.vo.chemo;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class ChemoItem implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@XmlElement(required = true)
	private Integer itemNum;

	@XmlElement(required = true)
	private Integer orgItemNum;
	
	@XmlElement(required = true)
	private Date startDate;

	@XmlElement
	private Date endDate;
	
	@XmlElement
	private Integer treatmentDay;
	
	@XmlElement
	private Date scheduleDate;

	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public Integer getOrgItemNum() {
		return orgItemNum;
	}

	public void setOrgItemNum(Integer orgItemNum) {
		this.orgItemNum = orgItemNum;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getTreatmentDay() {
		return treatmentDay;
	}

	public void setTreatmentDay(Integer treatmentDay) {
		this.treatmentDay = treatmentDay;
	}

	public Date getScheduleDate() {
		return scheduleDate;
	}

	public void setScheduleDate(Date scheduleDate) {
		this.scheduleDate = scheduleDate;
	}	
}
