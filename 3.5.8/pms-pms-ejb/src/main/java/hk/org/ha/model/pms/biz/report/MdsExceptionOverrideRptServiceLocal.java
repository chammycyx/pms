package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.vo.alert.AlertProfile;
import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;

import javax.ejb.Local;

@Local
public interface MdsExceptionOverrideRptServiceLocal {

	void printMdsExceptionOpOverrideRpt(AlertMsg alertMsg, AlertProfile alertProfile);
	
	void printMdsExceptionMpOverrideRpt(AlertMsg alertMsg, AlertProfile alertProfile);
	
	void destroy();
}
