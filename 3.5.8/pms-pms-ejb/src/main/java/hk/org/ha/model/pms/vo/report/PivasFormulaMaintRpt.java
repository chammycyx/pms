package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class PivasFormulaMaintRpt implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String workstoreGroupCode;
	
	private String pivasFormulaType;
	
	private String pivasFormulaStatus;
	
	private String pivasFormulaMethodStatus;
	
	private String targetProductItemCode;
	
	private Integer drugKey;
	
	private String printName;
	
	private String siteCode;
	
	private String diluentDesc;
	
	private String pivasDosageUnit;
	
	private BigDecimal concn;
	
	private BigDecimal rate;
	
	private BigDecimal formulaFinalVolume;
	
	private String strength;
	
	private Boolean batchPrepFlag;
	
	private String targetType;
	
	private String containerName;
	
	private BigDecimal upperDoseLimit;
	
	private String dispHospCode;
	
	private Integer shelfLife;
	
	private Integer satelliteShelfLife;
	
	private Integer labelOption;
	
	private String labelOptionType;
	
	private Boolean coreFlag;
	
	private BigDecimal volume;
	
	private BigDecimal diluentVolume;
	
	private BigDecimal afterConcn;
	
	private BigDecimal afterVolume;
	
	private String warnCode1;
	
	private String warnCode2;
	
	private String warnCode3;
	
	private String warnCode4;
	
	private BigDecimal formulaMethodDrugDosage;
	
	private String formulaDrugPivasDosageUnit;
	
	private String itemCode;
	
	private String manufacturerCode;

	private Boolean requireSolventFlag;
	
	private String solventDesc;
	
	private BigDecimal dosageQty;
	
	private String drugPivasDosageUnit;
	
	private BigDecimal solventVolume;
	
	private BigDecimal reconVolume;
	
	private BigDecimal reconConcn;
	
	private Long formulaId;
	
	private String formulaUpdateUser;
	
	private Date formulaUpdateDate;
	
	private Long formulaMethodId;
	
	private String formulaMethodUpdateUser;
	
	private Date formulaMethodUpdateDate;
	
	private Long formulaConfigId;
	
	private String drugStatus;
	
	private String drugManufStatus;
	
	private Long drugManufMethodId;
	
	private String drugManufMethodStatus;
	
	private Long formulaDrugId;
	
	private String displayname;
	
	private String saltProperty;
	
	private String formCode;
	
	private String diluentCode;
	
	private String expiryDateTitle;

	public String getWorkstoreGroupCode() {
		return workstoreGroupCode;
	}

	public void setWorkstoreGroupCode(String workstoreGroupCode) {
		this.workstoreGroupCode = workstoreGroupCode;
	}

	public String getPivasFormulaType() {
		return pivasFormulaType;
	}

	public void setPivasFormulaType(String pivasFormulaType) {
		this.pivasFormulaType = pivasFormulaType;
	}

	public String getPivasFormulaStatus() {
		return pivasFormulaStatus;
	}

	public void setPivasFormulaStatus(String pivasFormulaStatus) {
		this.pivasFormulaStatus = pivasFormulaStatus;
	}

	public String getPivasFormulaMethodStatus() {
		return pivasFormulaMethodStatus;
	}

	public void setPivasFormulaMethodStatus(String pivasFormulaMethodStatus) {
		this.pivasFormulaMethodStatus = pivasFormulaMethodStatus;
	}

	public String getTargetProductItemCode() {
		return targetProductItemCode;
	}

	public void setTargetProductItemCode(String targetProductItemCode) {
		this.targetProductItemCode = targetProductItemCode;
	}

	public Integer getDrugKey() {
		return drugKey;
	}

	public void setDrugKey(Integer drugKey) {
		this.drugKey = drugKey;
	}

	public String getPrintName() {
		return printName;
	}

	public void setPrintName(String printName) {
		this.printName = printName;
	}

	public String getSiteCode() {
		return siteCode;
	}

	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

	public String getDiluentDesc() {
		return diluentDesc;
	}

	public void setDiluentDesc(String diluentDesc) {
		this.diluentDesc = diluentDesc;
	}

	public String getPivasDosageUnit() {
		return pivasDosageUnit;
	}

	public void setPivasDosageUnit(String pivasDosageUnit) {
		this.pivasDosageUnit = pivasDosageUnit;
	}

	public BigDecimal getConcn() {
		return concn;
	}

	public void setConcn(BigDecimal concn) {
		this.concn = concn;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public BigDecimal getFormulaFinalVolume() {
		return formulaFinalVolume;
	}

	public void setFormulaFinalVolume(BigDecimal formulaFinalVolume) {
		this.formulaFinalVolume = formulaFinalVolume;
	}

	public String getStrength() {
		return strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public Boolean getBatchPrepFlag() {
		return batchPrepFlag;
	}

	public void setBatchPrepFlag(Boolean batchPrepFlag) {
		this.batchPrepFlag = batchPrepFlag;
	}

	public String getTargetType() {
		return targetType;
	}

	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}

	public String getContainerName() {
		return containerName;
	}

	public void setContainerName(String containerName) {
		this.containerName = containerName;
	}

	public BigDecimal getUpperDoseLimit() {
		return upperDoseLimit;
	}

	public void setUpperDoseLimit(BigDecimal upperDoseLimit) {
		this.upperDoseLimit = upperDoseLimit;
	}

	public String getDispHospCode() {
		return dispHospCode;
	}

	public void setDispHospCode(String dispHospCode) {
		this.dispHospCode = dispHospCode;
	}

	public Integer getShelfLife() {
		return shelfLife;
	}

	public void setShelfLife(Integer shelfLife) {
		this.shelfLife = shelfLife;
	}

	public Integer getSatelliteShelfLife() {
		return satelliteShelfLife;
	}

	public void setSatelliteShelfLife(Integer satelliteShelfLife) {
		this.satelliteShelfLife = satelliteShelfLife;
	}

	public Integer getLabelOption() {
		return labelOption;
	}

	public void setLabelOption(Integer labelOption) {
		this.labelOption = labelOption;
	}

	public String getLabelOptionType() {
		return labelOptionType;
	}

	public void setLabelOptionType(String labelOptionType) {
		this.labelOptionType = labelOptionType;
	}

	public Boolean getCoreFlag() {
		return coreFlag;
	}

	public void setCoreFlag(Boolean coreFlag) {
		this.coreFlag = coreFlag;
	}

	public BigDecimal getVolume() {
		return volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	public BigDecimal getDiluentVolume() {
		return diluentVolume;
	}

	public void setDiluentVolume(BigDecimal diluentVolume) {
		this.diluentVolume = diluentVolume;
	}

	public BigDecimal getAfterConcn() {
		return afterConcn;
	}

	public void setAfterConcn(BigDecimal afterConcn) {
		this.afterConcn = afterConcn;
	}

	public BigDecimal getAfterVolume() {
		return afterVolume;
	}

	public void setAfterVolume(BigDecimal afterVolume) {
		this.afterVolume = afterVolume;
	}

	public String getWarnCode1() {
		return warnCode1;
	}

	public void setWarnCode1(String warnCode1) {
		this.warnCode1 = warnCode1;
	}

	public String getWarnCode2() {
		return warnCode2;
	}

	public void setWarnCode2(String warnCode2) {
		this.warnCode2 = warnCode2;
	}

	public String getWarnCode3() {
		return warnCode3;
	}

	public void setWarnCode3(String warnCode3) {
		this.warnCode3 = warnCode3;
	}

	public String getWarnCode4() {
		return warnCode4;
	}

	public void setWarnCode4(String warnCode4) {
		this.warnCode4 = warnCode4;
	}

	public BigDecimal getFormulaMethodDrugDosage() {
		return formulaMethodDrugDosage;
	}

	public void setFormulaMethodDrugDosage(BigDecimal formulaMethodDrugDosage) {
		this.formulaMethodDrugDosage = formulaMethodDrugDosage;
	}

	public String getFormulaDrugPivasDosageUnit() {
		return formulaDrugPivasDosageUnit;
	}

	public void setFormulaDrugPivasDosageUnit(String formulaDrugPivasDosageUnit) {
		this.formulaDrugPivasDosageUnit = formulaDrugPivasDosageUnit;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getManufacturerCode() {
		return manufacturerCode;
	}

	public void setManufacturerCode(String manufacturerCode) {
		this.manufacturerCode = manufacturerCode;
	}

	public Boolean getRequireSolventFlag() {
		return requireSolventFlag;
	}

	public void setRequireSolventFlag(Boolean requireSolventFlag) {
		this.requireSolventFlag = requireSolventFlag;
	}

	public String getSolventDesc() {
		return solventDesc;
	}

	public void setSolventDesc(String solventDesc) {
		this.solventDesc = solventDesc;
	}

	public BigDecimal getDosageQty() {
		return dosageQty;
	}

	public void setDosageQty(BigDecimal dosageQty) {
		this.dosageQty = dosageQty;
	}

	public String getDrugPivasDosageUnit() {
		return drugPivasDosageUnit;
	}

	public void setDrugPivasDosageUnit(String drugPivasDosageUnit) {
		this.drugPivasDosageUnit = drugPivasDosageUnit;
	}

	public BigDecimal getSolventVolume() {
		return solventVolume;
	}

	public void setSolventVolume(BigDecimal solventVolume) {
		this.solventVolume = solventVolume;
	}

	public BigDecimal getReconVolume() {
		return reconVolume;
	}

	public void setReconVolume(BigDecimal reconVolume) {
		this.reconVolume = reconVolume;
	}

	public BigDecimal getReconConcn() {
		return reconConcn;
	}

	public void setReconConcn(BigDecimal reconConcn) {
		this.reconConcn = reconConcn;
	}

	public Long getFormulaId() {
		return formulaId;
	}

	public void setFormulaId(Long formulaId) {
		this.formulaId = formulaId;
	}

	public String getFormulaUpdateUser() {
		return formulaUpdateUser;
	}

	public void setFormulaUpdateUser(String formulaUpdateUser) {
		this.formulaUpdateUser = formulaUpdateUser;
	}

	public Date getFormulaUpdateDate() {
		return formulaUpdateDate;
	}

	public void setFormulaUpdateDate(Date formulaUpdateDate) {
		this.formulaUpdateDate = formulaUpdateDate;
	}

	public Long getFormulaMethodId() {
		return formulaMethodId;
	}

	public void setFormulaMethodId(Long formulaMethodId) {
		this.formulaMethodId = formulaMethodId;
	}

	public String getFormulaMethodUpdateUser() {
		return formulaMethodUpdateUser;
	}

	public void setFormulaMethodUpdateUser(String formulaMethodUpdateUser) {
		this.formulaMethodUpdateUser = formulaMethodUpdateUser;
	}

	public Date getFormulaMethodUpdateDate() {
		return formulaMethodUpdateDate;
	}

	public void setFormulaMethodUpdateDate(Date formulaMethodUpdateDate) {
		this.formulaMethodUpdateDate = formulaMethodUpdateDate;
	}

	public Long getFormulaConfigId() {
		return formulaConfigId;
	}

	public void setFormulaConfigId(Long formulaConfigId) {
		this.formulaConfigId = formulaConfigId;
	}

	public String getDrugStatus() {
		return drugStatus;
	}

	public void setDrugStatus(String drugStatus) {
		this.drugStatus = drugStatus;
	}

	public String getDrugManufStatus() {
		return drugManufStatus;
	}

	public void setDrugManufStatus(String drugManufStatus) {
		this.drugManufStatus = drugManufStatus;
	}

	public Long getDrugManufMethodId() {
		return drugManufMethodId;
	}

	public void setDrugManufMethodId(Long drugManufMethodId) {
		this.drugManufMethodId = drugManufMethodId;
	}

	public String getDrugManufMethodStatus() {
		return drugManufMethodStatus;
	}

	public void setDrugManufMethodStatus(String drugManufMethodStatus) {
		this.drugManufMethodStatus = drugManufMethodStatus;
	}

	public Long getFormulaDrugId() {
		return formulaDrugId;
	}

	public void setFormulaDrugId(Long formulaDrugId) {
		this.formulaDrugId = formulaDrugId;
	}

	public String getDisplayname() {
		return displayname;
	}

	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}

	public String getSaltProperty() {
		return saltProperty;
	}

	public void setSaltProperty(String saltProperty) {
		this.saltProperty = saltProperty;
	}

	public String getFormCode() {
		return formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	public String getDiluentCode() {
		return diluentCode;
	}

	public void setDiluentCode(String diluentCode) {
		this.diluentCode = diluentCode;
	}

	public String getExpiryDateTitle() {
		return expiryDateTitle;
	}

	public void setExpiryDateTitle(String expiryDateTitle) {
		this.expiryDateTitle = expiryDateTitle;
	}
}
