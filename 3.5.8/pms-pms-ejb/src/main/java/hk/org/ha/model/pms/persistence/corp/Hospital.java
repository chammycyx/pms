package hk.org.ha.model.pms.persistence.corp;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;


@Entity
@Table(name = "HOSPITAL")
public class Hospital extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "HOSP_CODE", length = 3)
	private String hospCode;

    @ManyToOne
    @JoinColumn(name = "CLUSTER_CODE",  nullable = false)
    private HospitalCluster hospitalCluster;

    @Converter(name = "Hospital.status", converterClass = RecordStatus.Converter.class)
    @Convert("Hospital.status")
	@Column(name="STATUS", nullable = false, length = 1)
	private RecordStatus status;
    
	@OneToMany(mappedBy = "hospital")
	private List<Workstore> workstoreList; 
    
    @OneToMany(mappedBy = "hospital")
    private List<HospitalMapping> hospitalMappingList;

    @ManyToMany(targetEntity = CddhWorkstoreGroup.class, mappedBy = "hospitalList")
    private List<CddhWorkstoreGroup> cddhWorkstoreGroupList;
    
	public Hospital() {
		status = RecordStatus.Active;
	}

	public Hospital(String hospCode) {
		this();
		this.hospCode = hospCode;
	}

	public Hospital(String hospCode, String clusterCode) {
		this(hospCode);
		this.hospitalCluster = new HospitalCluster(clusterCode);
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public RecordStatus getStatus() {
		return status;
	}

	public void setStatus(RecordStatus status) {
		this.status = status;
	}
	
	public List<Workstore> getWorkstoreList() {
		return workstoreList;
	}

	public void setWorkstoreList(List<Workstore> workstoreList) {
		this.workstoreList = workstoreList;
	}
	
	public List<HospitalMapping> getHospitalMappingList() {
		return hospitalMappingList;
	}
	
	public void setHospitalMappingList(List<HospitalMapping> hospitalMappingList) {
		this.hospitalMappingList = hospitalMappingList;
	}

	public HospitalCluster getHospitalCluster() {
		return hospitalCluster;
	}

	public void setHospitalCluster(HospitalCluster hospitalCluster) {
		this.hospitalCluster = hospitalCluster;
	}

	public List<CddhWorkstoreGroup> getCddhWorkstoreGroupList() {
		return cddhWorkstoreGroupList;
	}

	public void setCddhWorkstoreGroupList(List<CddhWorkstoreGroup> cddhWorkstoreGroupList) {
		this.cddhWorkstoreGroupList = cddhWorkstoreGroupList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((hospCode == null) ? 0 : hospCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Hospital other = (Hospital) obj;
		if (hospCode == null) {
			if (other.hospCode != null)
				return false;
		} else if (!hospCode.equals(other.hospCode))
			return false;
		return true;
	}
}
