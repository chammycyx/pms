package hk.org.ha.model.pms.persistence.reftable;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.eclipse.persistence.annotations.Customizer;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@Entity
@Table(name = "CHARGE_REASON")
@ExternalizedBean(type = DefaultExternalizer.class)
@Customizer(AuditCustomizer.class)
public class ChargeReason extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "chargeReasonSeq")
	@SequenceGenerator(name = "chargeReasonSeq", sequenceName = "SQ_CHARGE_REASON", initialValue = 100000000)
	private Long id;
	
	@Column(name = "REASON_CODE", nullable = false, length = 2)
	private String reasonCode;
	
	@Column(name = "TYPE", nullable = false, length = 1)
	private String type;
	
	@Column(name = "DESCRIPTION", nullable = false, length = 1000)
	private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
