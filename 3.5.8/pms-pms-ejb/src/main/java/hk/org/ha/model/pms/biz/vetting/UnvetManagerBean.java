package hk.org.ha.model.pms.biz.vetting;

import static hk.org.ha.model.pms.prop.Prop.CDDH_LEGACY_PERSISTENCE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.order.OrderManagerLocal;
import hk.org.ha.model.pms.biz.refill.RefillScheduleUtilsLocal;
import hk.org.ha.model.pms.corp.vo.UnvetOrderResult;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.disp.RemarkStatus;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.joda.time.DateTime;

@AutoCreate
@Stateless
@Name("unvetManager")
@MeasureCalls
public class UnvetManagerBean implements UnvetManagerLocal {

	@PersistenceContext
	private EntityManager em;
	
	@In
	private OrderManagerLocal orderManager;
	
	@In
	private RefillScheduleUtilsLocal refillScheduleUtils;
	
	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
	
	
	public List<Long> unvetMedOrder(Long medOrderId, boolean removeOrderByCms, boolean removeOrderByPms) {
		//  no identity.loggedIn restriction for triggered by PmsServiceBean
		
		MedOrder medOrder = em.find(MedOrder.class, medOrderId);
        medOrder.loadChild();
        
        // mark delete
        orderManager.loadOrderList(medOrder);
        List<DispOrder> dispOrderList = medOrder.markSysDeleted();
		refillScheduleUtils.markSysDeletedByMedOrder(medOrder);
        for (DispOrder dispOrder : dispOrderList) {
        	dispOrder.markItemDeleted();
        }
        medOrder.setUnvetFlag(Boolean.TRUE);

        // detach all
        em.flush();
        em.clear();           

        // keep ticket for cancel moe order 
        if (removeOrderByCms) {
            Ticket prevTicket = null;
            for (DispOrder dispOrder : dispOrderList) {
            	if ( ! dispOrder.getRefillFlag() ) {
            		prevTicket = dispOrder.getTicket();
            		break;
            	}
            }
            if (prevTicket != null) {
            	medOrder.setTicket(prevTicket);
            }
        }
        
        // clean up b4 send to corp
        medOrder.setUnvetFlag(Boolean.FALSE);
        medOrder.disconnectPharmOrderList();
        medOrder.clearId();
		
        if ( ! removeOrderByCms) {
        	medOrder.setRemarkStatus(RemarkStatus.None);
        	medOrder.setItemRemarkConfirmedFlag(Boolean.FALSE);	// reset to status that no item has been confirmed
		}
        if ( ! removeOrderByCms) {
        	if (medOrder.getPreVetDate() != null) {
        		// if discharge order is already prevet, fallback to prevet status
        		medOrder.markPrevet();
        	} else {
        		medOrder.markOutstanding();
        	}
        } else {
        	// if unvet is triggered by CMS cancel order, order status is set to DELETE in pms-corp, 
        	// so no need to mark medOrder status to OUTSTANDING
        	
        	// if unvet is triggered by CMS cancel order, do not fallback item DELETE status which is by unconfirmed remark
        	// delete, and do not fallback item SYSDELETE status which is by confirmed remark delete
        	medOrder.setStatus(MedOrderStatus.Outstanding);
    		for (MedOrderItem item : medOrder.getMedOrderItemList()) {			
    			if (item.getStatus() != MedOrderItemStatus.Deleted && 
    					item.getStatus() != MedOrderItemStatus.SysDeleted) {
    				item.setStatus(MedOrderItemStatus.Outstanding);
    			}
    		}		
        }
		medOrder.setVetDate(null);
		medOrder.setCmsUpdateDate(new DateTime().toDate());
		
		medOrder.clearDmInfo();
		UnvetOrderResult result = corpPmsServiceProxy.unvetOrder(
											medOrder,
											medOrderId,
											CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED.get(medOrder.getWorkstore().getHospital(), false), 
											CDDH_LEGACY_PERSISTENCE_ENABLED.get(false),
											Boolean.valueOf(removeOrderByCms),
											Boolean.valueOf(removeOrderByPms));
		medOrder.loadDmInfo();

		MedOrder newMedOrder = orderManager.insertMedOrderByUnvet(result.getMedOrder());
		
		List<Long> returnList = new ArrayList<Long>();
		returnList.add(newMedOrder.getId());
		returnList.add(dispOrderList.get(0).getId());
		
		return returnList;
	}
	
	@Remove
	public void destroy() {
	}
}
