package hk.org.ha.model.pms.vo.alert.mds;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class MdsLogInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<AllergyLog> allergyLogList;

	private List<AdrLog> adrLogList;
	
	private List<DrugLog> prescDrugLogList;

	private List<DrugLog> onHandDrugLogList;

	private Boolean g6pdFlag;

	private Boolean pregnancyFlag;

	private List<AlertAllergen> alertAllergenList;
	
	private List<AlertAdr> alertAdrList;
	
	private List<AlertDdcm> alertDdcmList;
	
	private List<AlertDdim> alertDdimList;
	
	private String inputXml;
	
	private String outputXml;
	
	public MdsLogInfo() {
		allergyLogList    = new ArrayList<AllergyLog>();
		adrLogList        = new ArrayList<AdrLog>();
		prescDrugLogList  = new ArrayList<DrugLog>();
		onHandDrugLogList = new ArrayList<DrugLog>();
		alertAllergenList = new ArrayList<AlertAllergen>();
		alertAdrList      = new ArrayList<AlertAdr>();
		alertDdcmList     = new ArrayList<AlertDdcm>();
		alertDdimList     = new ArrayList<AlertDdim>();
	}
	
	public List<AllergyLog> getAllergyLogList() {
		return allergyLogList;
	}

	public void setAllergyLogList(List<AllergyLog> allergyLogList) {
		this.allergyLogList = allergyLogList;
	}
	
	public void addAllergyLog(AllergyLog allergyLog) {
		getAllergyLogList().add(allergyLog);
	}

	public List<AdrLog> getAdrLogList() {
		return adrLogList;
	}

	public void setAdrLogList(List<AdrLog> adrLogList) {
		this.adrLogList = adrLogList;
	}
	
	public void addAdrLog(AdrLog adrLog) {
		getAdrLogList().add(adrLog);
	}

	public List<DrugLog> getPrescDrugLogList() {
		return prescDrugLogList;
	}

	public void setPrescDrugLogList(List<DrugLog> prescDrugLogList) {
		this.prescDrugLogList = prescDrugLogList;
	}
	
	public void addPrescDrugLog(DrugLog prescDrugLog) {
		getPrescDrugLogList().add(prescDrugLog);
	}

	public List<DrugLog> getOnHandDrugLogList() {
		return onHandDrugLogList;
	}

	public void setOnHandDrugLogList(List<DrugLog> onHandDrugLogList) {
		this.onHandDrugLogList = onHandDrugLogList;
	}
	
	public void addOnHandDrugLog(DrugLog onHandDrugLog) {
		getOnHandDrugLogList().add(onHandDrugLog);
	}

	public Boolean getG6pdFlag() {
		return g6pdFlag;
	}

	public void setG6pdFlag(Boolean g6pdFlag) {
		this.g6pdFlag = g6pdFlag;
	}

	public Boolean getPregnancyFlag() {
		return pregnancyFlag;
	}

	public void setPregnancyFlag(Boolean pregnancyFlag) {
		this.pregnancyFlag = pregnancyFlag;
	}

	public List<AlertAllergen> getAlertAllergenList() {
		return alertAllergenList;
	}

	public void setAlertAllergenList(List<AlertAllergen> alertAllergenList) {
		this.alertAllergenList = alertAllergenList;
	}
	
	public void addAlertAllergen(AlertAllergen alertAllergen) {
		getAlertAllergenList().add(alertAllergen);
	}

	public List<AlertAdr> getAlertAdrList() {
		return alertAdrList;
	}

	public void setAlertAdrList(List<AlertAdr> alertAdrList) {
		this.alertAdrList = alertAdrList;
	}

	public void addAlertAdr(AlertAdr alertAdr) {
		getAlertAdrList().add(alertAdr);
	}

	public List<AlertDdcm> getAlertDdcmList() {
		return alertDdcmList;
	}

	public void setAlertDdcmList(List<AlertDdcm> alertDdcmList) {
		this.alertDdcmList = alertDdcmList;
	}

	public void addAlertDdcm(AlertDdcm alertDdcm) {
		getAlertDdcmList().add(alertDdcm);
	}

	public List<AlertDdim> getAlertDdimList() {
		return alertDdimList;
	}

	public void setAlertDdimList(List<AlertDdim> alertDdimList) {
		this.alertDdimList = alertDdimList;
	}

	public void addAlertDdim(AlertDdim alertDdim) {
		getAlertDdimList().add(alertDdim);
	}

	public String getInputXml() {
		return inputXml;
	}

	public void setInputXml(String inputXml) {
		this.inputXml = inputXml;
	}

	public String getOutputXml() {
		return outputXml;
	}

	public void setOutputXml(String outputXml) {
		this.outputXml = outputXml;
	}
}
