package hk.org.ha.model.pms.biz.medprofile;
import javax.ejb.Local;

import org.jboss.seam.annotations.async.Asynchronous;
import org.jboss.seam.annotations.async.Duration;
import org.jboss.seam.annotations.async.IntervalDuration;

@Local
public interface MedProfileOrderProcessorLocal {

	@Asynchronous
	void processMedProfileOrderByScheduler(@Duration Long duration, String orderNum);
	
	@Asynchronous
	void processMedProfileOrder(String orderNum, boolean disableSeqCheck);

	@Asynchronous
	void triggerMedProfileTimer1(@Duration Long duration, @IntervalDuration Long intervalDuration);

	@Asynchronous
	void triggerMedProfileTimer2(@Duration Long duration, @IntervalDuration Long intervalDuration);
	
	void releaseProcessingMedProfiles(Long workstationId, boolean noWait);
	void releaseProcessingMedProfile(Long medProfileId, Long version, Long workstationId, boolean noWait);
}
