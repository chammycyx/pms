package hk.org.ha.model.pms.vo.report;

import hk.org.ha.model.pms.udt.disp.DispOrderStatus;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class UncollectPrescRpt implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Date ticketDate;
	
	private String ticketNum;
	
	private boolean moeOrderFlag;
	
	private String caseNum;

	private boolean refrigItemFlag;
	
	private DispOrderStatus dispOrderStatus;
	
	private String updateUser;

	public Date getTicketDate() {
		return ticketDate;
	}

	public void setTicketDate(Date ticketDate) {
		this.ticketDate = ticketDate;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public boolean isMoeOrderFlag() {
		return moeOrderFlag;
	}

	public void setMoeOrderFlag(boolean moeOrderFlag) {
		this.moeOrderFlag = moeOrderFlag;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public DispOrderStatus getDispOrderStatus() {
		return dispOrderStatus;
	}

	public void setDispOrderStatus(DispOrderStatus dispOrderStatus) {
		this.dispOrderStatus = dispOrderStatus;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public void setRefrigItemFlag(boolean refrigItemFlag) {
		this.refrigItemFlag = refrigItemFlag;
	}

	public boolean isRefrigItemFlag() {
		return refrigItemFlag;
	}
}
