package hk.org.ha.model.pms.udt.medprofile;


public enum ManualTransferResult {
	
	NoProfileFound,
	OptimisticLock,
	ProcessingByOthers,
	Success;
}
