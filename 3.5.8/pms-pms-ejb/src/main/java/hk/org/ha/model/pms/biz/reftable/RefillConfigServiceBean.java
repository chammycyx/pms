package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.RefillConfig;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("refillConfigService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class RefillConfigServiceBean implements RefillConfigServiceLocal {

	@PersistenceContext
	private EntityManager em;
	
	@In
	private RefillConfigManagerLocal refillConfigManager;
	
	@Out(required = false)
	private RefillConfig refillConfig;
	
	public void retrieveRefillConfig() 
	{
		refillConfig = refillConfigManager.retrieveRefillConfig();
	}
	
	public void updateRefillConfig(RefillConfig updateRefillConfig) 
	{
		updateRefillConfig.setWorkstore(em.find(Workstore.class, updateRefillConfig.getWorkstore().getId()));
		
		if(updateRefillConfig.getId()!=null)
		{
			em.merge(updateRefillConfig);
		}
		else
		{
			em.persist(updateRefillConfig);
		}
		
		em.flush();
		retrieveRefillConfig();
	}
	
	public void updateEnableSelectionFlag(Boolean enableSelectionFlag)
	{
		RefillConfig config = refillConfigManager.retrieveRefillConfig();
		config.setEnableSelectionFlag(enableSelectionFlag);
		updateRefillConfig(config);
	}
	
	public void updateHolidayRefillConfig(Boolean enableAdjHolidayFlag, Boolean enableAdjSundayFlag) 
	{
		RefillConfig config = refillConfigManager.retrieveRefillConfig();
		config.setEnableAdjHolidayFlag(enableAdjHolidayFlag);
		config.setEnableAdjSundayFlag(enableAdjSundayFlag);
		updateRefillConfig(config);
	}

	
	@Remove
	public void destroy() 
	{
		if (refillConfig != null) {
			refillConfig = null;
		}
	}
}
