package hk.org.ha.model.pms.biz.alert.mds;

import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.vo.alert.mds.MedProfileCheckResult;

import javax.ejb.Local;

@Local
public interface MedProfileCheckServiceLocal {

	MedProfileCheckResult checkMedProfileAlertForSaveProfile(MedProfile medProfile);
	
	void destory();
}
