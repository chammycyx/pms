package hk.org.ha.model.pms.vo.drug;

import java.io.Serializable;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.MsWorkstoreDrug;
import hk.org.ha.model.pms.udt.vetting.FmStatus;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class WorkstoreDrug implements Serializable {

	private static final long serialVersionUID = 1L;

	private DmDrug dmDrug;
	
	private MsWorkstoreDrug msWorkstoreDrug;
	
	private DmDrugLite dmDrugLite;
	//For DH order item usage
	private String siteCode;
	//For DH order item usage
	private String supplSiteDesc;
	
	public WorkstoreDrug() {
		super();
	}

	public WorkstoreDrug(DmDrug dmDrug, MsWorkstoreDrug msWorkstoreDrug) {
		super();
		this.dmDrug = dmDrug;
		this.msWorkstoreDrug = msWorkstoreDrug;
		this.dmDrugLite = createDmDrugLite(dmDrug, msWorkstoreDrug);
	}
	
	private DmDrugLite createDmDrugLite(DmDrug dmDrug, MsWorkstoreDrug msWorkstoreDrug){
		DmDrugLite dmDrugLite = new DmDrugLite();
		
		if (dmDrug!=null) {
			dmDrugLite.setItemCode(dmDrug.getItemCode());
			dmDrugLite.setFullDrugDesc(dmDrug.getFullDrugDesc());
			dmDrugLite.setFmStatusDesc(FmStatus.dataValueOf(dmDrug.getPmsFmStatus().getFmStatus()).getDisplayValue());
			dmDrugLite.setDispenseDosageUnit(dmDrug.getDmMoeProperty().getDispenseDosageUnit());
			dmDrugLite.setDrugName(dmDrug.getDrugName());
			dmDrugLite.setBaseUnit(dmDrug.getBaseUnit());
		}

		if (msWorkstoreDrug!=null) {
			dmDrugLite.setSuspend(msWorkstoreDrug.getSuspend());
			dmDrugLite.setHqSuspend(msWorkstoreDrug.getHqSuspend());
			dmDrugLite.setDrugScopeDesc(msWorkstoreDrug.getDrugScopeDesc());
			dmDrugLite.setItemStatus(msWorkstoreDrug.getItemStatus());
		}

		return dmDrugLite;	
	}

	public DmDrug getDmDrug() {
		return dmDrug;
	}

	public void setDmDrug(DmDrug dmDrug) {
		this.dmDrug = dmDrug;
	}

	public MsWorkstoreDrug getMsWorkstoreDrug() {
		return msWorkstoreDrug;
	}

	public void setMsWorkstoreDrug(MsWorkstoreDrug msWorkstoreDrug) {
		this.msWorkstoreDrug = msWorkstoreDrug;
	}

	public void setDmDrugLite(DmDrugLite dmDrugLite) {
		this.dmDrugLite = dmDrugLite;
	}

	public DmDrugLite getDmDrugLite() {
		return dmDrugLite;
	}

	public String getSiteCode() {
		return siteCode;
	}

	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

	public String getSupplSiteDesc() {
		return supplSiteDesc;
	}

	public void setSupplSiteDesc(String supplSiteDesc) {
		this.supplSiteDesc = supplSiteDesc;
	}	
}
