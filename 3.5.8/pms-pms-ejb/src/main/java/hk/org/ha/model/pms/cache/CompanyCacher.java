package hk.org.ha.model.pms.cache;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.Company;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("companyCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class CompanyCacher extends BaseCacher implements CompanyCacherInf {

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

    private InnerCacher cacher;
	    
	@Override
	public void clear() {
		synchronized (this) {
			cacher = null;
		}
	}	
    
	public Company getCompanyByCompanyCode(String companyCode) {
		return (Company) this.getCacher().get(companyCode);
	}
	
	public List<Company> getCompanyList() {
		return (List<Company>) this.getCacher().getAll();
	}
	
	public List<Company> getManufacturerList() {
		List<Company> ret = new ArrayList<Company>();
		for (Company company : getCompanyList()) {
			if (company.getManufacturerFlag().equals("Y")) {
				ret.add(company);
			}
		}
		return ret;
	}
	
	public List<Company> getCompanyListByCompanyCode(String prefixCompanyCode) {
		List<Company> ret = new ArrayList<Company>();
		for (Company company : getCompanyList()) {
			if (company.getCompanyCode().startsWith(prefixCompanyCode)) {
				ret.add(company);
			}
		}
		return ret;
	}
	
	public List<Company> getManufacturerListByCompanyCode(String prefixCompanyCode) {
		List<Company> ret = new ArrayList<Company>();
		for (Company company : getManufacturerList()) {
			if (company.getCompanyCode().startsWith(prefixCompanyCode)) {
				ret.add(company);
			}
		}
		return ret;
	}
	
    private InnerCacher getCacher() {
		synchronized (this) {
			if (cacher == null) {
				cacher = new InnerCacher(this.getExpireTime());
			}
			return cacher;
		}
	}

	private class InnerCacher extends AbstractCacher<String, Company> {		
        public InnerCacher(int expireTime) {
              super(expireTime);
        }
		
		@Override
		public Company create(String companyCode) {
			this.getAll();
			return this.internalGet(companyCode);
		}

		@Override
		public Collection<Company> createAll() {
			return dmsPmsServiceProxy.retrieveCompanyList();
		}

		@Override
		public String retrieveKey(Company company) {
			return company.getCompanyCode();
		}		
	}
	
	public static CompanyCacherInf instance(){
		  if (!Contexts.isApplicationContextActive()) {
	         throw new IllegalStateException("No active application scope");
	      }
	      return (CompanyCacherInf) Component.getInstance("companyCacher", ScopeType.APPLICATION);
    }	
}
