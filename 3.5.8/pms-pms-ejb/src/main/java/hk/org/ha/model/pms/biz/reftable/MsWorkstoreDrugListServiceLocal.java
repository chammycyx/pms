package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.dms.persistence.MsWorkstoreDrug;

import java.util.List;

import javax.ejb.Local;

@Local
public interface MsWorkstoreDrugListServiceLocal {

	void retrieveMsWorkstoreDrugList(String itemCode);
	
	void updateMsWorkstoreDrugList(List<MsWorkstoreDrug> updateMsWorkstoreDrugList, String remark);
	
	void destroy();
	
}
