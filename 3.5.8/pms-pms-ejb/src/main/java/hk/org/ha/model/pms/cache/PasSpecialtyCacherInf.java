package hk.org.ha.model.pms.cache;

import java.util.List;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.udt.patient.PasSpecialtyType;
import hk.org.ha.model.pms.vo.patient.PasSpecialty;

public interface PasSpecialtyCacherInf extends BaseCacherInf {
	
	PasSpecialty getPasSpecialty(String specCode, PasSpecialtyType specType);
	
	List<PasSpecialty> getPasSpecialtyList();
	
	List<PasSpecialty> getPasSpecialtyListByPatHospCode(String patHospCode);
	
	List<PasSpecialty> getPasSpecialtyWithSubSpecialtyList();
	
	List<PasSpecialty> getPasSpecialtyWithSubSpecialtyListByPatHospCode(String patHospCode);

	List<PasSpecialty> getPasSpecialtyListBySpecCode(String prefixSpecCode, PasSpecialtyType specType);
}
