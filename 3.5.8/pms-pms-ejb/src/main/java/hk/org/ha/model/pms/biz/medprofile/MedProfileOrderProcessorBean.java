package hk.org.ha.model.pms.biz.medprofile;

import static hk.org.ha.model.pms.prop.Prop.MEDPROFILE_CHECKMSGSEQNUM_ROLLOUT;
import static hk.org.ha.model.pms.prop.Prop.MEDPROFILE_PENDINGORDER_DURATION_LIMIT;
import static hk.org.ha.model.pms.prop.Prop.MEDPROFILE_TIMER_FCS_ENABLED;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.security.Uam;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.util.QueryUtils;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.TimerProp;
import hk.org.ha.model.pms.biz.alert.mds.AlertComparator;
import hk.org.ha.model.pms.biz.charging.DrugChargeClearanceManagerLocal;
import hk.org.ha.model.pms.biz.inbox.PharmInboxClientNotifierInf;
import hk.org.ha.model.pms.biz.reftable.ActiveWardManagerLocal;
import hk.org.ha.model.pms.biz.reftable.HospitalClusterTaskManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileActionLog;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileErrorLog;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItemAlert;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileOrder;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.persistence.medprofile.PurchaseRequest;
import hk.org.ha.model.pms.persistence.medprofile.Replenishment;
import hk.org.ha.model.pms.persistence.medprofile.ReplenishmentItem;
import hk.org.ha.model.pms.prop.Prop;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.medprofile.ActionLogType;
import hk.org.ha.model.pms.udt.medprofile.EndType;
import hk.org.ha.model.pms.udt.medprofile.ErrorLogType;
import hk.org.ha.model.pms.udt.medprofile.InactiveType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileMoItemStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileOrderStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileOrderType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileTransferAlertType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileType;
import hk.org.ha.model.pms.udt.medprofile.WardTransferType;
import hk.org.ha.model.pms.vo.rx.CapdItem;
import hk.org.ha.model.pms.vo.rx.CapdRxDrug;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.IvRxDrug;
import hk.org.ha.model.pms.vo.rx.Regimen;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.exceptions.DatabaseException;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.springframework.beans.BeanUtils;

@AutoCreate
@Stateless
@Name("medProfileOrderProcessor")
@TransactionManagement(TransactionManagementType.BEAN)
@MeasureCalls
public class MedProfileOrderProcessorBean implements MedProfileOrderProcessorLocal {

	private static final String UPDATE_MED_PROFILE_TIMER1 = "updateMedProfileTimer1";

	private static final String UPDATE_MED_PROFILE_TIMER2 = "updateMedProfileTimer2";
	
	private static final String MED_PROFILE_QUERY_HINT_BATCH = "p.medProfileItemList.medProfileMoItem.medProfilePoItemList";
	
	private static final String PREVIOUS_MED_PROFILE_MO_ITEM_QUERY_HINT_FETCH = "m.medProfileItem";
	
	private static MedProfileTypeComparator MED_PROFILE_TYPE_COMPARATOR = new MedProfileTypeComparator();
	
	@Logger
	private Log logger;	
	
	@In
	private AuditLogger auditLogger;

	@PersistenceContext
	private EntityManager em;
	
	@In
	private MedProfileItemConverterLocal medProfileItemConverter;
	
	@In
	private MedProfileManagerLocal medProfileManager;
	
	@In
	private MedProfileStatManagerLocal medProfileStatManager;
	
	@In
	private ActiveWardManagerLocal activeWardManager;
	
	@In
	private DrugChargeClearanceManagerLocal drugChargeClearanceManager;
	
	@In
	private PharmInboxClientNotifierInf pharmInboxClientNotifier;
	
	@In
	private MedProfileOrderSchedulerInf medProfileOrderScheduler;
	
	@In
	private HospitalClusterTaskManagerLocal hospitalClusterTaskManager;
	
	@In
	private DmDrugCacher dmDrugCacher;
	
	@In
	private AlertComparator alertComparator;
	
	@In
	private TimerProp timerProp;

	@Resource
	private UserTransaction userTransaction;
	
	@Override
	public void processMedProfileOrderByScheduler(Long duration, String orderNum) {
		
		try {
			String nextOrderNum = orderNum;
			do {
				try {
					Thread.sleep(1000L);
				} catch (Exception ex) {
				}
				logger.info("Processing MedProfileOrder(orderNum = #0)", nextOrderNum);
				if (nextOrderNum != null) {
					processMedProfileOrder(nextOrderNum, true, false);
				}
				nextOrderNum = medProfileOrderScheduler.getNextOrderNumToRun();
			} while (nextOrderNum != null);
			
		} finally {
			logger.info("Processing MedProfileOrder completed");
			medProfileOrderScheduler.stopRunning();
		}
	}
	
	@Override
	public void processMedProfileOrder(String orderNum, boolean disableSeqCheck) {
		processMedProfileOrder(orderNum, true, disableSeqCheck);
	}
	
	private boolean processMedProfileOrder(String orderNum, boolean dispatchUpdateEvent, boolean disableSeqCheck) {
		return processMedProfileOrder(orderNum, dispatchUpdateEvent, disableSeqCheck, false, null);
	}
	
	private boolean processMedProfileOrder(String orderNum, boolean dispatchUpdateEvent, boolean disableSeqCheck, boolean logSeqCheckError, Set<String> updatedHospCodeSet) {

		boolean success = false;
		dmDrugCacher.getDmDrugList();
		
		try {
			beginTransaction(600);

			List<MedProfileOrder> medProfileOrderList = fetchPendingMedProfileOrders(orderNum);
			MedProfile medProfile = fetchMedProfile(orderNum, medProfileOrderList);
			
			if (medProfile != null) {
				processPendingMedProfileOrders(orderNum, medProfile, medProfileOrderList, disableSeqCheck, logSeqCheckError);
				medProfileStatManager.recalculateAllStat(medProfile, dispatchUpdateEvent);
				if (updatedHospCodeSet != null && medProfile.getWorkstore() != null && medProfile.getWorkstore().getHospCode() != null) { 
					updatedHospCodeSet.add(medProfile.getWorkstore().getHospCode());
				}
			}
			success = true;
			
		} catch (OrderProcessingException ex) {
			logger.error(ex);
		} catch (Exception ex) {
			logger.error(ex, ex);
		} finally {
			endTransaction(success);
		}
		return success;
	}
	
	private void processMedProfileTimer1Task() {

		Set<String> updatedHospCodeSet = new HashSet<String>();
		
		List<String> orderNumList = fetchPendingOrderNums();
		
		for (String orderNum: orderNumList) {
			processMedProfileOrder(orderNum, false, false, true, updatedHospCodeSet);
		}
		
		pharmInboxClientNotifier.dispatchUpdateEvent(updatedHospCodeSet);
	}
	
	private void processMedProfileTimer2Task() {

		deleteInactiveMedProfileStat();
		deleteInactiveMedProfileErrorStat();
		deleteInactiveMedProfilePoItemStat();
		
		Set<String> updatedHospCodeSet = new HashSet<String>();
		
		updatePaymentStatus(updatedHospCodeSet);

		List<MedProfile> medProfileList = fetchMedProfilesWithItemsToBeEnd();
		for (MedProfile medProfile: medProfileList) {
			updateMedProfile(medProfile, updatedHospCodeSet);
		}
		
		pharmInboxClientNotifier.dispatchUpdateEvent(updatedHospCodeSet);
	}
	
	@SuppressWarnings("unchecked")
	private void deleteInactiveMedProfileStat() {

		boolean success = false;
		
		try {
			beginTransaction();
			
			List<Long> idList = em.createQuery(
					"select o.id from MedProfile o" + // 20150206 index check : MedProfileStatus.medProfile : FK_MED_PROFILE_STAT_01
					" where o.id in" +
					"  (select distinct s.medProfileId from MedProfileStat s" +
					"   where s.medProfile.status = :inactiveStatus)")
					.setParameter("inactiveStatus", MedProfileStatus.Inactive)
					.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
					.getResultList();
			
			QueryUtils.splitUpdate(em.createQuery(
					"delete from MedProfileStat o" + // 20150206 index check : MedProfileStatus.medProfile : FK_MED_PROFILE_STAT_01
					" where o.medProfileId in :idList"), "idList", idList);
			
			success = true;
			
		} catch (Exception ex) {
			success = false;
			logger.error(ex, ex);
		} finally {
			endTransaction(success);			
		}
	}
	
	@SuppressWarnings("unchecked")
	private void deleteInactiveMedProfileErrorStat() {

		boolean success = false;		
		
		try {
			beginTransaction();
			
			List<Long> idList = em.createQuery(
					"select o.id from MedProfile o" + // 20150206 index check : MedProfileErrorStatus.medProfile : FK_MED_PROFILE_ERROR_STAT_01
					" where o.id in" +
					"  (select distinct s.medProfileId from MedProfileErrorStat s" +
					"   where s.medProfile.status = :inactiveStatus)")
					.setParameter("inactiveStatus", MedProfileStatus.Inactive)
					.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
					.getResultList();
			
			QueryUtils.splitUpdate(em.createQuery(
					"delete from MedProfileErrorStat o" + // 20150206 index check : MedProfileErrorStatus.medProfile : FK_MED_PROFILE_ERROR_STAT_01
					" where o.medProfileId in :idList"), "idList", idList);
			
			success = true;
			
		} catch (Exception ex) {
			success = false;
			logger.error(ex, ex);
		} finally {
			endTransaction(success);			
		}
	}
	
	@SuppressWarnings("unchecked")
	private void deleteInactiveMedProfilePoItemStat() {
		
		boolean success = false;
		
		try {
			beginTransaction();
			
			Date now = new Date();
			Date profileUpdateDate = MedProfileUtils.addDays(now, -7);
			Date overdueStartDate = MedProfileUtils.getOverdueStartDate(now);
			
			List<Long> idList = em.createQuery(
					"select o.id from MedProfile o" + // 20151019 index check : MedProfile.status,updateDate : I_MED_PROFILE_02
					" where o.id in" +
					"  (select distinct s.medProfileId from MedProfilePoItemStat s" +
					"   where (s.medProfile.status = :inactiveStatus and s.medProfile.updateDate > :profileUpdateDate) or s.dueDate < :overdueStartDate)")
					.setParameter("inactiveStatus", MedProfileStatus.Inactive)
					.setParameter("profileUpdateDate", profileUpdateDate)
					.setParameter("overdueStartDate", overdueStartDate)
					.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
					.getResultList();
			
			QueryUtils.splitUpdate(em.createQuery(
					"delete from MedProfilePoItemStat o" +
					" where o.id in" +
					"  (select s.id from MedProfilePoItemStat s" +
					"   where ((s.medProfile.status = :inactiveStatus and s.medProfile.updateDate > :profileUpdateDate) or s.dueDate < :overdueStartDate)" +
					"   and s.medProfileId in :idList)")
					.setParameter("inactiveStatus", MedProfileStatus.Inactive)
					.setParameter("profileUpdateDate", profileUpdateDate)
					.setParameter("overdueStartDate", overdueStartDate), "idList", idList);
			
			success = true;
			
		} catch (Exception ex) {
			success = false;
			logger.error(ex, ex);
		} finally {
			endTransaction(success);			
		}
	}
	
	private void updatePaymentStatus(Set<String> updateHospCodeSet) {

		if (!MEDPROFILE_TIMER_FCS_ENABLED.get(Boolean.FALSE)) {
			return;
		}
		
		boolean success = false;
		
		try {
			beginTransaction();
			
			List<PurchaseRequest> purchaseRequestList = fetchPendingPurchaseRequestList();
			drugChargeClearanceManager.updateDrugChargeClearance(purchaseRequestList);
			for (PurchaseRequest purchaseRequest: purchaseRequestList) {
				if (purchaseRequest.getInvoiceStatus() == InvoiceStatus.Settled) {
					updateHospCodeSet.add(purchaseRequest.getHospCode());
				}
			}
			success = true;
		} catch (Exception ex) {
			success = false;
			logger.error(ex, ex);
		} finally {
			endTransaction(success);			
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<PurchaseRequest> fetchPendingPurchaseRequestList() {

		Date currentDate = new Date();
		Date startDate = MedProfileUtils.addDays(currentDate, -3);
		
		return em.createQuery(
				"select o from PurchaseRequest o" + // 20160615 index check : PurchaseRequest.hospCode,invoiceDate,invoiceStatus : I_PURCHASE_REQUEST_03
				" where o.medProfile.processingFlag = false and o.medProfile.deliveryGenFlag = false" +
		    	" and o.medProfile.status <> :inactiveStatus" +
		    	" and (o.medProfile.status = :activeStatus" +
		    	" or (o.medProfile.status = :homeLeaveStatus" +
		    	" and o.medProfile.returnDate < :currentDate))" +
				" and o.invoiceStatus = :outstandingInvoiceStatus and o.forceProceedFlag = false" +
		    	" and o.invoiceDate >= :startDate" +
		    	" and o.printDate is null" +
		    	" and o.directPrintFlag = false" +
		    	" order by o.invoiceDate, o.createDate")
		    	.setParameter("inactiveStatus", MedProfileStatus.Inactive)
				.setParameter("activeStatus", MedProfileStatus.Active)
				.setParameter("homeLeaveStatus", MedProfileStatus.HomeLeave)
				.setParameter("startDate", startDate)
				.setParameter("currentDate", currentDate)
				.setParameter("outstandingInvoiceStatus", InvoiceStatus.Outstanding)
				.getResultList();
	}
		
	private boolean updateMedProfile(MedProfile medProfile, Set<String> updatedHospCodeSet) {

		boolean success = false;
		
		try {
			beginTransaction();
			medProfile = lockMedProfile(medProfile.getId());
			
			if (medProfile != null) {
				
				for (MedProfileItem medProfileItem: medProfile.getMedProfileItemList()) {
					success |= updateMedProfileItem(new Date(), medProfileItem);
				}
				if (success) {
					medProfileStatManager.recalculateAllStat(medProfile, false);
					updatedHospCodeSet.add(medProfile.getWorkstore().getHospCode());
				}
			}
		} catch (Exception ex) {
			success = false;
			logger.error(ex, ex);
		} finally {
			endTransaction(success);			
		}
		return success;
	}
	
	@Override
	public void releaseProcessingMedProfiles(Long workstationId, boolean noWait) {

		Set<String> updatedHospCodeSet = new HashSet<String>();
		List<MedProfile> medProfileList = fetchProcessingMedProfileList(workstationId);
		for (MedProfile medProfile: medProfileList) {
			releaseProcessingMedProfile(medProfile.getId(), null, workstationId, noWait);
			processMedProfileOrder(medProfile.getOrderNum(), false, false, false, updatedHospCodeSet);
		}
		
		pharmInboxClientNotifier.dispatchUpdateEvent(updatedHospCodeSet);
	}
	
	@Override
	public void releaseProcessingMedProfile(Long medProfileId, Long version, Long workstationId, boolean noWait) {

		boolean success = false;
		
		try {
			beginTransaction();
			
			MedProfile medProfile = fetchLockedProcessingMedProfile(medProfileId, version, workstationId, noWait);
			
			if (medProfile != null) {
				medProfile.setProcessingFlag(Boolean.FALSE);
				medProfile.setWorkstationId(null);
				success = true;
				logger.info("MedProfile(id=#0, orderNum=#1) has been released by Workstation(id=#2)", medProfileId, medProfile.getOrderNum(), workstationId);
			}
		} catch (DatabaseException ex) {
			String message = ex.getMessage();
			if (message != null && message.toLowerCase().indexOf("nowait") != -1) {
				logger.info("Processing state cannot be release for MedProfile(#0)," +
						" which is currently locked by another transaction.", medProfileId);
			} else {
				logger.error(ex, ex);
			}
		} catch (Exception ex) {
			logger.error(ex, ex);
		} finally {
			endTransaction(success);
		}
	}
	
	private boolean updateMedProfileItem(Date now, MedProfileItem medProfileItem) {
		
		if (!MedProfileItemStatus.Unvet_Vetted_Verified.contains(medProfileItem.getStatus())) {
			return false;
		}
		
		if (medProfileItem.getMedProfileMoItem() == null) {
			logger.warn("Missing medProfileMoItem for medProfileItem(ID=#0, OrderNum=#1)",
					medProfileItem.getId(), medProfileItem.getMedProfile().getOrderNum());
			return false;
		}		
		
		return updateMedProfileMoItem(now, medProfileItem.getMedProfileMoItem());
	}
	
	private boolean updateMedProfileMoItem(Date now, MedProfileMoItem medProfileMoItem) {

		Date endDate = medProfileMoItem.getEndDate();
		if (endDate == null || endDate.compareTo(now) >= 0) {
			return false;
		}
		
		MedProfileItem medProfileItem = medProfileMoItem.getMedProfileItem();
		medProfileItem.hideErrorStat();
		if (medProfileItem.isUnvetAndNotAdminYet()) {
			medProfileItem.setStatus(MedProfileItemStatus.Deleted);
			medProfileMoItem.setStatus(MedProfileMoItemStatus.SysDeleted);
		} else {
			medProfileItem.setStatus(MedProfileItemStatus.Ended);
			medProfileItem.setEndType(EndType.EndTimePassed);
		}
		medProfileItem.setStatusUpdateDate(endDate);
		em.flush();
		
		clearUpCompletedMedProfileMoItem(medProfileItem.getMedProfile(), medProfileMoItem);
		return true;
	}
	
	@SuppressWarnings("unchecked")
	private MedProfile lockMedProfile(Long id) {
		
		return (MedProfile) MedProfileUtils.getFirstItem(em.createQuery(
				"select p from MedProfile p" + // 20120215 index check : MedProfile.id : PK_MED_PROFILE
				" where p.processingFlag = false" +
				" and p.deliveryGenFlag = false" +
				" and p.id = :id")
				.setParameter("id", id)
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
				.setHint(QueryHints.BATCH, MED_PROFILE_QUERY_HINT_BATCH)
				.getResultList());
	}
	
	private void processPendingMedProfileOrders(String orderNum, MedProfile medProfile, List<MedProfileOrder> medProfileOrderList,
			boolean disableSeqCheck, boolean logSeqCheckError) {
		
		boolean enableSeqCheck = false;
		if (!disableSeqCheck) {
			Hospital hospital = resolveHospital(medProfile, medProfileOrderList);
			if (hospital == null) {
				logger.info("Hospital not found while processing MedProfileOrder(orderNum=#0)", orderNum);
				return;
			}
			enableSeqCheck = MEDPROFILE_CHECKMSGSEQNUM_ROLLOUT.get(hospital, false);
		}
		boolean pasInfoUpdated = false;
		boolean specialtyUpdated = false;
		MedProfileOrder pendingOrder;
		
		Integer nextSeqNum = initializeNextSeqNum(medProfile);
		
		while ((pendingOrder = extractMedProfileOrder(medProfileOrderList, nextSeqNum, enableSeqCheck)) != null) {
			
			if (isAbnormalOrder(medProfile, pendingOrder)) {
				continue;
			}
			pendingOrder.setStatus(MedProfileOrderStatus.Completed);
			copyValues(pendingOrder, medProfile);
			
			if (medProfile.getId() == null) {
				medProfileManager.initializePasInfo(medProfile, pendingOrder);
				pasInfoUpdated = true;
			} else if (pendingOrder.getType() == MedProfileOrderType.Discharge ||
					(!pasInfoUpdated && medProfile.isCancelAdmission() && pendingOrder.getType().getMpTrxType() != null)) {
				medProfileManager.updatePasInfo(medProfile);
				pasInfoUpdated = true;
			} else if (!pasInfoUpdated && !specialtyUpdated && !MedProfileOrderType.WardTransfer_CancelTransfer.contains(pendingOrder.getType())) {
				medProfileManager.mapSpecialty(medProfile);
				specialtyUpdated = true;
			}
			
			if (pendingOrder.getProtocolCode() == null || !chemoPendingOrderPartRemained(medProfileOrderList, nextSeqNum, enableSeqCheck)) {
				nextSeqNum = incrementSeqNum(medProfile, pendingOrder, nextSeqNum, enableSeqCheck);
			}
			
			if (medProfile.getId() == null) {
				em.persist(medProfile);
			}
			processPendingMedProfileOrder(medProfile, pendingOrder);
		}
		
		logPendingError(medProfileOrderList, enableSeqCheck && logSeqCheckError);
	}
	
	private boolean chemoPendingOrderPartRemained(List<MedProfileOrder> pendingOrderList, Integer currSeqNum, boolean enableSeqCheck) {
		
		if (!enableSeqCheck || currSeqNum == null) {
			return false;
		}
		
		for (MedProfileOrder pendingOrder: pendingOrderList) {
			if (currSeqNum.equals(pendingOrder.getSeqNum())) {
				return true;
			}
		}
		return false;
	}
	
	private void logPendingError(List<MedProfileOrder> medProfileOrderList, boolean logError) {
		
		MedProfileOrder medProfileOrder = MedProfileUtils.getFirstItem(medProfileOrderList);
		
		if (logError && medProfileOrder != null) {
			logger.fatal("Cannot process pending MedProfileOrder(OrderNum=#0, Count=#1) caused by message not in sequence",
					medProfileOrder.getOrderNum(), medProfileOrderList.size());
		}
	}
	
	private Hospital resolveHospital(MedProfile medProfile, List<MedProfileOrder> medProfileOrderList) {
		
		if (medProfile != null && medProfile.getWorkstore() != null && medProfile.getWorkstore().getHospital() != null) {
			return medProfile.getWorkstore().getHospital();
		}
		
		Hospital hospital = null;
		
		for (MedProfileOrder medProfileOrder: medProfileOrderList) {
			if (medProfileOrder.getWorkstore() != null && medProfileOrder.getWorkstore().getHospital() != null) {
				if (hospital == null) {
					hospital = medProfileOrder.getWorkstore().getHospital();
				} else if (MedProfileUtils.compare(hospital.getHospCode(),
						medProfileOrder.getWorkstore().getHospital().getHospCode()) != 0) {
					throw new OrderProcessingException("Unexpected different hospitals found while processing MedProfileOrder(OrderNum=" +
							medProfileOrder.getOrderNum() + ")");
				}
			}
		}
		return hospital;
	}
	
	private Integer initializeNextSeqNum(MedProfile medProfile) {
		
		if (medProfile.getCurrSeqNum() != null) {
			return medProfile.getCurrSeqNum() + 1;
		} else if (medProfile.getId() == null) {
			return 1;
		}
		return null;
	}
	
	private Integer incrementSeqNum(MedProfile medProfile, MedProfileOrder medProfileOrder, Integer currSeqNum, boolean enableSeqCheck) {
		
		if (enableSeqCheck) {
			if (medProfileOrder.getSeqNum() != null) {
				medProfile.setCurrSeqNum(medProfileOrder.getSeqNum());
				return medProfile.getCurrSeqNum() + 1;
			}
			return currSeqNum;
		}
		medProfile.setCurrSeqNum(null);
		return null;
	}
	
	private void copyValues(MedProfileOrder medProfileOrder, MedProfile medProfile) {

		if (medProfile.getOrderNum() == null) {
			medProfile.setOrderNum(medProfileOrder.getOrderNum());
		}
		
		if (medProfileOrder.getProtocolCode() != null) {
			medProfile.setType(MedProfileType.Chemo);
			medProfile.setProtocolCode(medProfileOrder.getProtocolCode());
		}
		if (medProfile.getProtocolVersion() != null) {
			medProfile.setProtocolVersion(medProfileOrder.getProtocolVersion());
		}
		if (medProfileOrder.getPatHospCode() != null) {
			medProfile.setPatHospCode(medProfileOrder.getPatHospCode());
		}
		if (medProfileOrder.getWardCode() != null && medProfile.getId() == null) {
			medProfile.setWardCode(medProfileOrder.getWardCode());
		}
		if (medProfileOrder.getPatCatCode() != null) {
			medProfile.setPatCatCode(medProfileOrder.getPatCatCode());
		}
		if (medProfileOrder.getSpecCode() != null && medProfile.getId() == null) {
			medProfile.setSpecCode(medProfileOrder.getSpecCode());
		}
		if (medProfileOrder.getBodyWeight() != null) {
			medProfile.setBodyWeight(medProfileOrder.getBodyWeight());
		}
		if (medProfileOrder.getBodyHeight() != null) {
			medProfile.setBodyHeight(medProfileOrder.getBodyHeight());
		}
		if (medProfileOrder.getBodyInfoUpdateDate() != null) {
			medProfile.setBodyInfoUpdateDate(medProfileOrder.getBodyInfoUpdateDate());
		}
		if (medProfileOrder.getPrivateFlag() != null) {
			medProfile.setPrivateFlag(medProfileOrder.getPrivateFlag());
		}
		if (medProfileOrder.getPregnancyCheckFlag() != null) {
			medProfile.setPregnancyCheckFlag(medProfileOrder.getPregnancyCheckFlag());
		}
		
		if (medProfile.getWorkstore() == null) {
			medProfile.setWorkstore(medProfileOrder.getWorkstore());
		}
		
		if (medProfile.getWorkstore() == null) {
			throw new OrderProcessingException("Missing workstore while processing MedProfileOrder(OrderNum=" +
					medProfileOrder.getOrderNum() + ", ID=" + medProfileOrder.getId() +")");
		}
		
		if (MedProfileOrderType.Create_Add_Modify_Replenishment.contains(medProfileOrder.getType())) {
			medProfile.setLastItemDate(medProfileOrder.getCreateDate());
		}
	}
	
	private void processPendingMedProfileOrder(MedProfile medProfile, MedProfileOrder medProfileOrder) {
		
		logger.info("Processing pending MedProfileOrder(#0) of Type #1", medProfileOrder.getId(), medProfileOrder.getType());
		
		switch (medProfileOrder.getType()) {
			case PatientRegistrationMp:
				processPatientRegistration(medProfile, medProfileOrder);
				break;
			case Discharge:
				break;
			case CancelAdmission:
				processCancelAdmission(medProfile, medProfileOrder);
				break;
			case CancelDischarge:
				processCancelDischarge(medProfile, medProfileOrder);
				break;
			case WardTransfer:
				processWardTransfer(medProfile, medProfileOrder, false);
				break;
			case CancelTransfer:
				processWardTransfer(medProfile, medProfileOrder, true);
				break;
			case CheckPregnancy:
				break;
			case AddAdmin:
				processAddAdminOrder(medProfile, medProfileOrder);
				break;
			case ModifyAdmin:
				processModifyAdminOrder(medProfile, medProfileOrder);
				break;
			case Modify:
				processModifyOrder(medProfile, medProfileOrder);
				break;
			case End:
				processEndOrder(medProfile, medProfileOrder);
				break;
			default:
				processPendingMedProfileMoItems(medProfile, medProfileOrder);
		}
	}
	
	private void processPendingMedProfileMoItems(MedProfile medProfile, MedProfileOrder medProfileOrder) {
		
		List<MedProfileMoItem> medProfileMoItemList = medProfileOrder.getMedProfileMoItemList();
		
		for (MedProfileMoItem medProfileMoItem: medProfileMoItemList) {
			processPendingMedProfileMoItem(medProfile, medProfileOrder, medProfileMoItem);
		}
	}
	
	private void processPendingMedProfileMoItem(MedProfile medProfile, MedProfileOrder medProfileOrder, MedProfileMoItem medProfileMoItem) {

		MedProfileItem medProfileItem = fetchMedProfileItem(medProfile, medProfileOrder, medProfileMoItem);
		
		if (MedProfileOrderType.Urgent_ModifyMds_ModifyReviewDate.contains(medProfileOrder.getType()) && medProfileItem.getMedProfileMoItem() != null &&
				MedProfileUtils.compare(medProfileItem.getMedProfileMoItem().getItemNum(), medProfileMoItem.getItemNum()) != 0) {
			logger.info("Current MedProfileMoItem(itemNum=#0) is newer than the one described in " +
					"MedProfileOrder(id=#1, orderNum=#2, caseNum=#3, itemNum=#4, type=#5). Skip ...", 
					medProfileItem.getMedProfileMoItem().getItemNum(), medProfileOrder.getId(), medProfileOrder.getOrderNum(),
					medProfileOrder.getCaseNum(), medProfileMoItem.getItemNum(), medProfileOrder.getType());
			return;
		}
		
		switch (medProfileOrder.getType()) {
			case Create:
				processCreateOrder(medProfileItem, medProfileMoItem);
				break;
			case Add:
				processAddOrder(medProfileItem, medProfileMoItem);
				break;
			case Delete:
				processDeleteOrder(medProfileOrder, medProfileItem);
				break;
			case Discontinue:
				processDiscountinueOrder(medProfileOrder, medProfileItem);
				break;
			case Urgent:
				processUrgentOrder(medProfileItem, medProfileMoItem);
				break;
			case Replenishment:
				processReplenishmentOrder(medProfileOrder, medProfileItem, medProfileMoItem);
				break;
			case CompleteAdmin:
				processCompleteAdminOrder(medProfileOrder, medProfileItem);
				break;
			case OverridePending:
				processOverridePendingOrder(medProfileOrder, medProfileItem, medProfileMoItem);
				break;
			case AmendSchedule:
			case CustomSchedule:
				processAmendScheduleOrder(medProfileOrder, medProfileItem, medProfileMoItem);
				break;
			case MarkAnnotation:
				processMarkAnnotationOrder(medProfileOrder, medProfileItem);
				break;
			case ModifyMds:
				processModifyMdsOrder(medProfileOrder, medProfileItem, medProfileMoItem);
				break;
			case ModifyReviewDate:
				processModifyReviewDateOrder(medProfileOrder, medProfileItem);
				break;
		}
		
		if (medProfileItem.getMedProfileMoItem() == null) {
			throw new OrderProcessingException("Missing MedProfileMoItem after processing MedProfileOrder(Type=" +
					medProfileOrder.getType() + " ,ID=" + medProfileOrder.getId() +
					", OrderNum=" + medProfileOrder.getOrderNum() + ")");
		}
	}
	
	private void processPatientRegistration(MedProfile medProfile, MedProfileOrder medProfileOrder) {
		if (medProfile.getType() == MedProfileType.Chemo) {
			medProfile.getMedCase().setCaseNum(medProfileOrder.getCaseNum());
		}
	}
	
	private void processCancelAdmission(MedProfile medProfile, MedProfileOrder medProfileOrder) {
		
		medProfileManager.markProfileAsInactive(medProfile, InactiveType.CancelAdmission, null, medProfileOrder.getOrderDate());
	}
	
	private void processCancelDischarge(MedProfile medProfile, MedProfileOrder medProfileOrder) {

		if (medProfile.isDeleted()) {
			return;
		}
		
		if (medProfileOrder.getCancelDischargeDate() == null) {
			throw new OrderProcessingException("Missing cancelDischargeDate while processing MedProfileOrder(Type=" +
					medProfileOrder.getType() + " ,ID=" + medProfileOrder.getId() +
					", OrderNum=" + medProfileOrder.getOrderNum() + ")");
		}
		
		medProfileManager.markProfileAsInactive(medProfile, InactiveType.Discharge,
				medProfileOrder.getDischargeDate() == null && medProfile.isDischarge() ?
				medProfile.getDischargeDate(): medProfileOrder.getDischargeDate(), null);
		medProfile.setCancelDischargeDate(medProfileOrder.getCancelDischargeDate());
		
		if (MedProfileUtils.compare(
				MedProfileUtils.addHours(medProfileOrder.getCancelDischargeDate(),
						Prop.MEDPROFILE_CONFIRMCANCELDISCHARGE_AUTO_DURATION.get(medProfile.getWorkstore().getHospital(), 24) * -1),
				medProfileOrder.getDischargeDate()) < 0) {
			medProfile.setStatus(MedProfileStatus.Active);
			medProfile.setInactiveType(null);
			medProfile.setConfirmCancelDischargeDate(new Date());
			medProfile.setConfirmCancelDischargeUser("system");
			auditCancelDischarge(medProfile);
		}		
	}
	
	private void auditCancelDischarge(MedProfile medProfile) {

		Uam uam = (Uam) Component.getInstance(Uam.class, true);
		UamInfo originalUamInfo = uam.getUamInfo();
		UamInfo uamInfo = new UamInfo();
		uamInfo.setHospital(medProfile.getWorkstore().getHospCode());
		uam.setUamInfo(uamInfo);
		auditLogger.log("#0692", medProfile.getOrderNum(), medProfile.getMedCase().getCaseNum(), true);
		uam.setUamInfo(originalUamInfo);
	}
	
	private void processWardTransfer(MedProfile medProfile, MedProfileOrder medProfileOrder, boolean cancelTransfer) {

		updatePasDetails(medProfile, medProfileOrder, cancelTransfer);
		medProfileManager.mapSpecialty(medProfile);
		
		if (!activeWardManager.isActiveWard(medProfile.getPatHospCode(), medProfile.getMedCase().getPasWardCode())) {
			if (!cancelTransfer && (!Boolean.TRUE.equals(medProfile.getTransferFlag()) || medProfile.getTransferDate() == null)) {
				medProfile.setTransferDate(medProfileOrder.getTransferDate());
			}
			medProfile.setTransferFlag(true);
		} else if (Boolean.TRUE.equals(medProfile.getTransferFlag())) {
			updateCurrentItemCutOffDate(medProfile);
			medProfileManager.markItemTransferInfo(medProfile, medProfile.getMedProfileItemList(),
					medProfile.getCurrentItemCutOffDate(), WardTransferType.Normal);
		}
		
		if (cancelTransfer) {
			clearAllUnitDoseExceptions(medProfile);
		}
	}
	
	private void updateCurrentItemCutOffDate(MedProfile medProfile) {
	
		if (medProfile.getCurrentItemCutOffDate() == null) {
			medProfile.setCurrentItemCutOffDate(medProfileManager.retrieveItemCutOffDate(medProfile));
		}
	}
	
	private void updatePasDetails(MedProfile medProfile, MedProfileOrder medProfileOrder, boolean cancelTransfer) {

		MedCase medCase = medProfile.getMedCase();
		String orgPasWardCode = medCase.getPasWardCode();
		
		if (medProfileOrder.getPasSpecCode() != null) {
			medCase.setPasSpecCode(medProfileOrder.getPasSpecCode());
		}
		medCase.setPasWardCode(medProfileOrder.getPasWardCode());
		medCase.setPasBedNum(medProfileOrder.getPasBedNum());
		
		medProfileManager.updatePasWardCodeRelatedSetting(medProfile.getWorkstore(), orgPasWardCode, medProfile, cancelTransfer);
	}
	
	private void processCreateOrder(MedProfileItem medProfileItem, MedProfileMoItem medProfileMoItem) {
		
		MedProfile medProfile = medProfileItem.getMedProfile();
		
		clearAllMdsExceptions(medProfile);
		markInactiveToAllFrozenItems(medProfile);
		medProfileMoItem.setMedProfileItem(medProfileItem);
		
		medProfileItemConverter.formatMedProfileMoItemList(Arrays.asList(medProfileMoItem));
		medProfileItem.setMedProfileMoItem(medProfileMoItem);
		
		medProfileManager.datebackTransferInfo(medProfile, medProfileItem, medProfileMoItem, new Date());
	}
	
	private void processAddOrder(MedProfileItem medProfileItem, MedProfileMoItem medProfileMoItem) {
		processCreateOrder(medProfileItem, medProfileMoItem);
	}
	
	private void processModifyOrder(MedProfile medProfile, MedProfileOrder medProfileOrder) {
		
		List<MedProfileMoItem> medProfileMoItemList = medProfileOrder.getMedProfileMoItemList();
		if (medProfileMoItemList.size() != 2) {
			throw new OrderProcessingException("Unexpected Number of MedProfileMoItem in MedProfileOrder." +
					" Expected 2, current " + medProfileMoItemList.size());
		}
		
		MedProfileMoItem prevMedProfileMoItem = medProfileMoItemList.get(0);
		MedProfileMoItem medProfileMoItem = medProfileMoItemList.get(1);
		
		MedProfileItem medProfileItem = endPreviousMedProfileMoItem(medProfile, medProfileOrder, prevMedProfileMoItem);
		
		updateCurrentMedProfileMoItem(medProfileItem, medProfileMoItem);
	}
	
	private void processEndOrder(MedProfile medProfile, MedProfileOrder medProfileOrder) {

		List<MedProfileMoItem> medProfileMoItemList = medProfileOrder.getMedProfileMoItemList();
		if (medProfileMoItemList.size() != 2) {
			throw new OrderProcessingException("Unexpected Number of MedProfileMoItem in MedProfileOrder." +
					" Expected 2, current " + medProfileMoItemList.size());
		}
		
		clearAllMdsExceptions(medProfile);
		
		MedProfileMoItem prevMedProfileMoItem = medProfileMoItemList.get(0);
		
		MedProfileItem medProfileItem = endPreviousMedProfileMoItem(medProfile, medProfileOrder, prevMedProfileMoItem);
		medProfileItem.hideErrorStat();
		medProfileItem.setStatus(MedProfileItemStatus.Ended);
		medProfileItem.setEndType(EndType.EndNow);
		medProfileItem.setStatusUpdateDate(medProfileOrder.getOrderDate());
	}
	
	private MedProfileItem endPreviousMedProfileMoItem(MedProfile medProfile, MedProfileOrder medProfileOrder, MedProfileMoItem medProfileMoItem) {
		
		MedProfileMoItem medProfileMoItemInDB = fetchPreviousMedProfileMoItem(medProfile, medProfileMoItem.getItemNum());
		if (medProfileMoItemInDB == null) {
			throw new OrderProcessingException("Previous MedProfileMoItem(itemNum=" + 
					medProfileMoItem.getItemNum() + ") not found for Order Type Modify/End");
		}
			
		medProfileMoItemInDB.setStartDate(medProfileMoItem.getStartDate());
		medProfileMoItemInDB.setEndDate(medProfileMoItem.getEndDate());
		MedProfileItem medProfileItem = medProfileMoItemInDB.getMedProfileItem();
		if (medProfileItem.getAdminDate() == null && medProfileOrder.getCmsAdminStatus() != null) {
			medProfileItem.setAdminDate(medProfileOrder.getAdminDate());
		}
		if (medProfileItem.isUnvetAndNotAdminYet()) {
			medProfileMoItemInDB.setStatus(MedProfileMoItemStatus.SysDeleted);
		}
		clearUpCompletedMedProfileMoItem(medProfile, medProfileMoItemInDB);
		return medProfileItem;
	}
	
	private void updateCurrentMedProfileMoItem(MedProfileItem medProfileItem, MedProfileMoItem medProfileMoItem) {
		
		MedProfile medProfile = medProfileItem.getMedProfile();
		
		if (!medProfileItem.isUnvetAndNotAdminYet()) {
			medProfileItem.setAmendFlag(true);
		}
		clearAllMedProfileItemStates(medProfileItem);
		clearAllMdsExceptions(medProfile);
		
		medProfileMoItem.setMedProfileItem(medProfileItem);
		
		if (MedProfileItemStatus.Vetted_Verified.contains(medProfileItem.getStatus())) {
			medProfileItem.hideErrorStat();
			medProfileItem.setStatus(MedProfileItemStatus.Unvet);
			medProfileItem.setStatusUpdateDate(medProfileMoItem.getOrderDate());
		}

		medProfileItemConverter.formatMedProfileMoItemList(Arrays.asList(medProfileMoItem));
		medProfileItem.setMedProfileMoItem(medProfileMoItem);

		medProfileManager.datebackTransferInfo(medProfile, medProfileItem, medProfileMoItem, new Date());
		
		em.flush();
	}
	
	private void clearAllMedProfileItemStates(MedProfileItem medProfileItem) {
		medProfileItem.setReplenishFlag(Boolean.FALSE);
		medProfileItem.setAdminDate(null);
	}
	
	private void clearAllMdsExceptions(MedProfile medProfile) {
		List<MedProfileItem> medProfileItemList = medProfile.getMedProfileItemList();
		
		for (MedProfileItem medProfileItem: medProfileItemList) {
			
			if (!MedProfileItemStatus.Unvet_Vetted_Verified.contains(medProfileItem.getStatus())) {
				continue;
			}
			
			MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
			if (medProfileMoItem != null && medProfileMoItem.isManualItem()){
				continue;
			}
			
			MedProfileErrorLog errorLog = medProfileItem.getMedProfileErrorLog();
			if (errorLog != null && errorLog.getType() == ErrorLogType.MdsAlert) {
				errorLog.setHideErrorStatFlag(Boolean.TRUE);
			}
			
			if (medProfileMoItem != null && medProfileMoItem.getMdsSuspendReason() != null) {
				medProfileMoItem.setMdsSuspendReason(null);
			}
		}
	}
	
	private void clearAllUnitDoseExceptions(MedProfile medProfile) {
		
		for (MedProfileItem medProfileItem: medProfile.getMedProfileItemList()) {
			MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
			if (medProfileMoItem != null && Boolean.TRUE.equals(medProfileMoItem.getUnitDoseExceptFlag())) {
				medProfileMoItem.setUnitDoseExceptFlag(Boolean.FALSE);	
			}
		}
	}
	
	private void markInactiveToAllFrozenItems(MedProfile medProfile){
		boolean inactiveWard = !activeWardManager.isActiveWard(medProfile.getPatHospCode(), medProfile.getMedCase().getPasWardCode());
		
		if( inactiveWard ){
			return;
		}
		
		boolean containsFrozenItem = false;
		Date now = new Date();
		for (MedProfileItem medProfileItem: medProfile.getMedProfileItemList()) {
			MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
			if (medProfileMoItem != null && Boolean.TRUE.equals(medProfileMoItem.getFrozenFlag())) {
				containsFrozenItem = true;
				medProfileItem.setStatus(MedProfileItemStatus.Ended);
				medProfileItem.setEndType(EndType.WardTransfered);
				medProfileItem.setStatusUpdateDate(now);
				medProfileMoItem.setFrozenFlag(Boolean.FALSE);
				medProfileMoItem.setEndDate(now);
				clearUpCompletedMedProfileMoItem(medProfileItem.getMedProfile(), medProfileMoItem);
			}
		}
			
		if( containsFrozenItem ){
			medProfile.setTransferAlertType(MedProfileTransferAlertType.Notify);
		}
	}
	
	private void processDeleteOrder(MedProfileOrder medProfileOrder, MedProfileItem medProfileItem) {
		
		clearAllMdsExceptions(medProfileItem.getMedProfile());
		
		medProfileItem.hideErrorStat();
		if (medProfileItem.isUnvetAndNotAdminYet()) {
			medProfileItem.setStatus(MedProfileItemStatus.Deleted);
			medProfileItem.getMedProfileMoItem().setStatus(MedProfileMoItemStatus.SysDeleted);
		} else {
			medProfileItem.setStatus(MedProfileItemStatus.Discontinued);
		}
		medProfileItem.setStatusUpdateDate(medProfileOrder.getOrderDate());
		
		clearUpCompletedMedProfileMoItem(medProfileItem.getMedProfile(), medProfileItem.getMedProfileMoItem());
	}
	
	private void processDiscountinueOrder(MedProfileOrder medProfileOrder, MedProfileItem medProfileItem) {

		clearAllMdsExceptions(medProfileItem.getMedProfile());
		
		MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem(); 
		medProfileItem.hideErrorStat();
		if (medProfileItem.getAdminDate() == null && medProfileOrder.getCmsAdminStatus() != null) {
			medProfileItem.setAdminDate(medProfileOrder.getAdminDate());
		}
		
		if (medProfileItem.isUnvetAndNotAdminYet()) {
			medProfileItem.setStatus(MedProfileItemStatus.Deleted);
			medProfileMoItem.setStatus(MedProfileMoItemStatus.SysDeleted);
		} else {
			medProfileItem.setStatus(MedProfileItemStatus.Discontinued);
			
			medProfileMoItem.setEndDate(medProfileOrder.getDiscontinueDate());
			
			RxItem rxItem = medProfileMoItem.getRxItem();
			if (rxItem != null) {
				rxItem.setDiscontinueFlag(true);
				rxItem.setDiscontinueDate(medProfileOrder.getDiscontinueDate());
				rxItem.setDiscontinueHospCode(medProfileOrder.getDiscontinueHospCode());
				rxItem.setDiscontinueReason(medProfileOrder.getDiscontinueReason());
			}
			medProfileItemConverter.formatMedProfileMoItemList(Arrays.asList(medProfileMoItem));
		}
		medProfileItem.setStatusUpdateDate(medProfileOrder.getOrderDate());
		
		clearUpCompletedMedProfileMoItem(medProfileItem.getMedProfile(), medProfileMoItem);
	}

	private void processUrgentOrder(MedProfileItem medProfileItem, MedProfileMoItem medProfileMoItem) {
		
		MedProfileMoItem latestMedProfileMoItem = medProfileItem.getMedProfileMoItem();
		
		if (latestMedProfileMoItem.getFirstPrintDate() == null ||
				!latestMedProfileMoItem.getFirstPrintDate().before(MedProfileUtils.getTodayBegin())) {
			
			latestMedProfileMoItem.setUrgentFlag(true);
			latestMedProfileMoItem.setUrgentNum(medProfileMoItem.getUrgentNum());
		}
	}
	
	private void processReplenishmentOrder(MedProfileOrder medProfileOrder, MedProfileItem medProfileItem,
			MedProfileMoItem medProfileMoItem) {
		
		medProfileItem.setReplenishFlag(true);
		
		Replenishment replenishment = findReplenishment(
				medProfileOrder.getReplenishmentList(), medProfileMoItem);
		
		if (replenishment != null) {
			
			MedProfileMoItem latestMedProfileMoItem = medProfileItem.getMedProfileMoItem();
			
			replenishment.setMedProfile(medProfileItem.getMedProfile());
			replenishment.setMedProfileMoItem(latestMedProfileMoItem);
			
			for (MedProfilePoItem medProfilePoItem: latestMedProfileMoItem.getMedProfilePoItemList()) {
				
				ReplenishmentItem replenishmentItem = new ReplenishmentItem();
				replenishmentItem.setReplenishment(replenishment);
				replenishmentItem.setMedProfilePoItem(medProfilePoItem);
				em.persist(replenishmentItem);
			}
		}
	}
	
	private Replenishment findReplenishment(List<Replenishment> replenishmentList, MedProfileMoItem medProfileMoItem) {
		
		if (replenishmentList == null || medProfileMoItem == null) {
			return null;
		}
		
		for (Replenishment replenishment: replenishmentList) {
			if (medProfileMoItem.getId() != null &&
					medProfileMoItem.getId().equals(replenishment.getMedProfileMoItem().getId())) {
				return replenishment;
			}
		}
		
		return null;
	}
	
	private void processAddAdminOrder(MedProfile medProfile, MedProfileOrder medProfileOrder) {

		if (!countAsGivenAdminStatus(medProfileOrder.getCmsAdminStatus())) {
			return;
		}
		
		Integer itemNum = resolveItemNum(medProfileOrder);
		MedProfileMoItem medProfileMoItem = fetchPreviousMedProfileMoItem(medProfile, itemNum);
		if (medProfileMoItem == null) {
			throw new OrderProcessingException("Previous MedProfileMoItem(itemNum=" + 
					itemNum + ") not found for Order Type " + medProfileOrder.getType());
 		}
		
		medProfileMoItem.getMedProfileItem().setAdminDate(medProfileOrder.getAdminDate());
	}
	
	private void processModifyAdminOrder(MedProfile medProfile, MedProfileOrder medProfileOrder) {
		processAddAdminOrder(medProfile, medProfileOrder);
	}
	
	private void processCompleteAdminOrder(MedProfileOrder medProfileOrder, MedProfileItem medProfileItem) {
		
		clearAllMdsExceptions(medProfileItem.getMedProfile());
		
		medProfileItem.hideErrorStat();
		medProfileItem.setAdminDate(medProfileOrder.getAdminDate());
		medProfileItem.setStatus(MedProfileItemStatus.Ended);
		medProfileItem.setEndType(EndType.CompleteAdmin);
		medProfileItem.setStatusUpdateDate(medProfileOrder.getOrderDate());
		medProfileItem.getMedProfileMoItem().setEndDate(medProfileOrder.getAdminDate());
		
		clearUpCompletedMedProfileMoItem(medProfileItem.getMedProfile(), medProfileItem.getMedProfileMoItem());
	}
	
	private boolean countAsGivenAdminStatus(Integer adminStatus) {
		return adminStatus == null || MedProfileOrder.ADMIN_STATUS_GIVEN_LIST.contains(adminStatus);
	}
	
	private void processOverridePendingOrder(MedProfileOrder medProfileOrder, MedProfileItem medProfileItem, MedProfileMoItem medProfileMoItem) {
		
		MedProfileMoItem latestMedProfileMoItem = medProfileItem.getMedProfileMoItem();
		
		latestMedProfileMoItem.setOverrideDesc(medProfileMoItem.getOverrideDesc());
		latestMedProfileMoItem.setOverrideDoctorCode(medProfileMoItem.getOverrideDoctorCode());
		latestMedProfileMoItem.setOverrideDoctorName(medProfileMoItem.getOverrideDoctorName());
		latestMedProfileMoItem.setOverrideDoctorRankCode(medProfileMoItem.getOverrideDoctorRankCode());
		latestMedProfileMoItem.setOverrideDate(medProfileMoItem.getOverrideDate());
		
		MedProfileActionLog medProfileActionLog = new MedProfileActionLog();
		medProfileActionLog.setMedProfileMoItem(latestMedProfileMoItem);
		medProfileActionLog.setWardCode(medProfileItem.getMedProfile().getWardCode());
		medProfileActionLog.setType(ActionLogType.Override);
		medProfileActionLog.setMessage(medProfileMoItem.getOverrideDesc());
		medProfileActionLog.setCreateDate(medProfileMoItem.getOverrideDate());
		medProfileActionLog.setActionUser(medProfileMoItem.getOverrideDoctorCode());
		medProfileActionLog.setActionUserName(medProfileMoItem.getOverrideDoctorName());
		
		em.persist(medProfileActionLog);
	}
	
	private void processAmendScheduleOrder(MedProfileOrder medProfileOrder, MedProfileItem medProfileItem, MedProfileMoItem medProfileMoItem) {

		MedProfileMoItem latestMedProfileMoItem = medProfileItem.getMedProfileMoItem();
		RxItem rxItem = latestMedProfileMoItem.getRxItem();
		
		Date newStartDate = ObjectUtils.firstNonNull(medProfileMoItem.getStartDate(), MedProfileUtils.extractStartDate(rxItem, true));
		
		if (newStartDate == null || latestMedProfileMoItem.getFirstPrintDate() != null ||
			(medProfileItem.getStatus() != MedProfileItemStatus.Unvet && latestMedProfileMoItem.getMedProfilePoItemList().size() <= 0)) {
			return;
		}
		
		if (rxItem instanceof RxDrug) {
			RxDrug rxDrug = (RxDrug) rxItem;
			MedProfileUtils.updateStartDate(rxDrug.getRegimen(), newStartDate);
		} else if (rxItem instanceof IvRxDrug) {
			IvRxDrug ivRxDrug = (IvRxDrug) rxItem;
			ivRxDrug.setStartDate(newStartDate);
		} else if (rxItem instanceof CapdRxDrug) {
			CapdRxDrug capdRxDrug = (CapdRxDrug) rxItem;
			for (CapdItem capdItem: capdRxDrug.getCapdItemList()) {
				capdItem.setStartDate(newStartDate);
			}
		}
		
		latestMedProfileMoItem.setStartDate(newStartDate);
		
		for (MedProfilePoItem medProfilePoItem: latestMedProfileMoItem.getMedProfilePoItemList()) {
			medProfilePoItem.setDueDate(newStartDate);
			MedProfileUtils.updateStartDate(medProfilePoItem.getRegimen(), newStartDate);
		}
	}
	
	private void processMarkAnnotationOrder(MedProfileOrder medProfileOrder, MedProfileItem medProfileItem) {
		
		MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
		RxItem rxItem = medProfileMoItem.getRxItem();
		if (rxItem != null) {
			rxItem.setMrAnnotation(medProfileOrder.getMrAnnotation());
		}
		medProfileItemConverter.formatMedProfileMoItemList(Arrays.asList(medProfileMoItem));
	}
	
	private void processModifyMdsOrder(MedProfileOrder medProfileOrder, MedProfileItem medProfileItem, MedProfileMoItem medProfileMoItem) {
		
		MedProfileMoItem latestMedProfileMoItem = medProfileItem.getMedProfileMoItem();
		if (MedProfileUtils.compare(latestMedProfileMoItem.getLastAlertDate(), medProfileMoItem.getOrderDate()) > 0) {
			logger.warn("Modify MDS [MedProfileMoItem(#0)] Order Date earlier than the last one [MedProfileMoItem(#1)] received before. Skip ... ",
					medProfileMoItem.getId(), latestMedProfileMoItem.getId());
			return;
		}
		
		List<MedProfileMoItemAlert> medProfileMoItemAlertListCopy = createCopy(medProfileMoItem.getMedProfileMoItemAlertList()); 
		
		boolean retainStatus = alertComparator.copyAlertOverride(
				Prop.MEDPROFILE_ALERT_HLA_ENABLED.get(medProfileItem.getMedProfile().getWorkstore().getHospital()),
				latestMedProfileMoItem.getMedProfileMoItemAlertList(), medProfileMoItemAlertListCopy);
		
		if (medProfileItem.getStatus() == MedProfileItemStatus.Verified && !retainStatus) {
			medProfileItem.setStatus(MedProfileItemStatus.Vetted);
			medProfileItem.setStatusUpdateDate(medProfileOrder.getOrderDate());
		}
		
		clearAllMdsExceptions(medProfileItem.getMedProfile());
		latestMedProfileMoItem.setAlertFlag(medProfileMoItem.getAlertFlag());
		latestMedProfileMoItem.setMdsSuspendReason(null);
		latestMedProfileMoItem.setLastAlertDate(medProfileMoItem.getOrderDate());
		
		RxItem rxItem = latestMedProfileMoItem.getRxItem();
		
		if (rxItem != null && MedProfileUtils.compare(rxItem.getAlertFlag(), medProfileMoItem.getAlertFlag()) != 0) {
			rxItem.setAlertFlag(medProfileMoItem.getAlertFlag());
			medProfileItemConverter.formatMedProfileMoItemList(Arrays.asList(latestMedProfileMoItem));
		}
		
		updateMedProfileMoItemAlertList(latestMedProfileMoItem, medProfileMoItemAlertListCopy);
	}
	
	private void processModifyReviewDateOrder(MedProfileOrder medProfileOrder, MedProfileItem medProfileItem) {
		
		RxItem rxItem = medProfileItem.getMedProfileMoItem().getRxItem();
		
		if (rxItem instanceof RxDrug) {
			Regimen regimen = ((RxDrug) rxItem).getRegimen();
			if (regimen != null) {
				for (DoseGroup doseGroup: regimen.getDoseGroupList()) {
					doseGroup.setReviewDate(medProfileOrder.getReviewDate());
				}
			}
		} else if (rxItem instanceof IvRxDrug) {
			((IvRxDrug) rxItem).setReviewDate(medProfileOrder.getReviewDate());
		} else if (rxItem instanceof CapdRxDrug) {
			for (CapdItem capdItem: ((CapdRxDrug) rxItem).getCapdItemList()) {
				capdItem.setReviewDate(medProfileOrder.getReviewDate());
			}
		}
		
		medProfileItem.getMedProfileMoItem().preSave();
	}
	
	private void clearUpCompletedMedProfileMoItem(MedProfile medProfile, MedProfileMoItem medProfileMoItem) {
		
		medProfileManager.removeUnfinishedPivasRelatedRecords(medProfileMoItem);
		medProfileManager.removeNonProcessedPurchaseRequestListByMedProfileMoItem(medProfile, medProfileMoItem);
	}
	
	private List<MedProfileMoItemAlert> createCopy(List<MedProfileMoItemAlert> medProfileMoItemAlertList) {
		
		List<MedProfileMoItemAlert> medProfileMoItemAlertListCopy = new ArrayList<MedProfileMoItemAlert>();
		
		for (MedProfileMoItemAlert medProfileMoItemAlert: medProfileMoItemAlertList) {
			
			MedProfileMoItemAlert medProfileMoItemAlertCopy = new MedProfileMoItemAlert();
			BeanUtils.copyProperties(medProfileMoItemAlert, medProfileMoItemAlertCopy,
					new String[]{"id", "medProfileMoItem", "version"});
			medProfileMoItemAlertListCopy.add(medProfileMoItemAlertCopy);
		}
		return medProfileMoItemAlertListCopy;
	}
	
	private void updateMedProfileMoItemAlertList(MedProfileMoItem medProfileMoItem, List<MedProfileMoItemAlert> medProfileMoItemAlertList) {
		
		medProfileMoItem.getMedProfileMoItemAlertList().clear();
		for (MedProfileMoItemAlert medProfileMoItemAert: medProfileMoItemAlertList) {
			medProfileMoItemAert.setMedProfileMoItem(medProfileMoItem);
		}
		medProfileMoItem.getMedProfileMoItemAlertList().addAll(medProfileMoItemAlertList);
	}
	
	private MedProfileOrder extractMedProfileOrder(List<MedProfileOrder> medProfileOrderList, Integer seqNum, boolean enableSeqCheck)
	{
		if (!enableSeqCheck) {
			if (!medProfileOrderList.isEmpty()) {
				return medProfileOrderList.remove(0);
			}
		} else if (seqNum != null) {
			for (int i = 0; i < medProfileOrderList.size(); i++) {
				MedProfileOrder medProfileOrder = medProfileOrderList.get(i);
				if (medProfileOrder.getSeqNum() == null || medProfileOrder.getSeqNum().equals(seqNum)) {
					return medProfileOrderList.remove(i);
				}
			}
		} else {
			int currIndex = -1;
			Integer currSeqNum = null;
			
			for (int i = 0; i < medProfileOrderList.size(); i++) {
				MedProfileOrder medProfileOrder = medProfileOrderList.get(i);
				if (currIndex == -1 || MedProfileUtils.compare(medProfileOrder.getSeqNum(), currSeqNum) < 0) {
					currIndex = i;
					currSeqNum = medProfileOrder.getSeqNum();
				}
			}
			if (currIndex != -1) {
				return medProfileOrderList.remove(currIndex);
			}
		}
		return null;
	}
	
	private Integer resolveItemNum(MedProfileOrder medProfileOrder) {
		
		Integer itemNum = medProfileOrder.getItemNum();
		if (itemNum == null && medProfileOrder.getMedProfileMoItemList().size() > 0) {
			itemNum = MedProfileUtils.getFirstItem(medProfileOrder.getMedProfileMoItemList()).getItemNum();
		}
		return itemNum;
	}	
	
	@SuppressWarnings("unchecked")
	private List<String> fetchPendingOrderNums() {
		
		Calendar startCal = Calendar.getInstance();
		startCal.add(Calendar.DAY_OF_MONTH,
				MEDPROFILE_PENDINGORDER_DURATION_LIMIT.get(7).intValue() * -1);
		
		List<String> orderNumList = em.createQuery(
				"select distinct o.orderNum from MedProfileOrder o" + // 20120225 index check : MedProfileOrder.status : FK_MED_PROFILE_ORDER_03
				" where o.status = :status" +
				" and o.createDate > :startDate" +
				" order by o.id")
				.setParameter("status", MedProfileOrderStatus.Pending)
				.setParameter("startDate", startCal.getTime())
				.getResultList();
		
		Collections.sort(orderNumList, MED_PROFILE_TYPE_COMPARATOR);
		
		return orderNumList;
	}
	
	@SuppressWarnings("unchecked")
	private List<MedProfileOrder> fetchPendingMedProfileOrders(String orderNum) {

		Calendar startCal = Calendar.getInstance();
		startCal.add(Calendar.DAY_OF_MONTH,
				MEDPROFILE_PENDINGORDER_DURATION_LIMIT.get(7).intValue() * -1);
		
		Triple<String, String, String> keyInfo = fetchKeyInfoOnManualProfile(orderNum);
		
		Query query = em.createQuery(
				"select o from MedProfileOrder o" + // 20120225 index check : MedProfileOrder.orderNum : FK_MED_PROFILE_ORDER_02
				" where (o.orderNum = :orderNum" +
				(keyInfo != null ? " or ((o.hospCode = :hospCode or o.hospCode is null) and o.patHospCode = :patHospCode and o.caseNum = :caseNum))" : ")") +
				" and o.status = :status" +
				" and o.createDate > :startDate" +
				" order by o.orderDate, o.createDate")
				.setParameter("orderNum", orderNum)
				.setParameter("status", MedProfileOrderStatus.Pending)
				.setParameter("startDate", startCal.getTime())
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock);
		
		if (keyInfo != null) {
			query.setParameter("hospCode", keyInfo.getLeft());
			query.setParameter("patHospCode", keyInfo.getMiddle());
			query.setParameter("caseNum", keyInfo.getRight());
		}
		
		return filterPendingMedProfileOrders(query.getResultList(), orderNum);
	}
	
	@SuppressWarnings("unchecked")
	private Triple<String, String, String> fetchKeyInfoOnManualProfile(String orderNum) {
		
		if (!MedProfileUtils.isManualProfile(orderNum)) {
			return null;
		}
		
		Object[] result = (Object[]) MedProfileUtils.getFirstItem(em.createQuery(
				"select o.hospCode, o.patHospCode, o.medCase.caseNum from MedProfile o" + // 20161128 index check : MedProfile.orderNum : UI_MED_PROFILE_01
				" where o.orderNum = :orderNum")
				.setParameter("orderNum", orderNum)
				.getResultList());
		return result == null || result[2] == null ? null : Triple.of((String) result[0], (String) result[1], (String) result[2]);
	}
	
	private List<MedProfileOrder> filterPendingMedProfileOrders(List<MedProfileOrder> medProfileOrderList, String orderNum) {

		if (!MedProfileUtils.isManualProfile(orderNum)) {
			return medProfileOrderList;
		}
		
		MedProfileOrder ipmoeMedProfileOrder = resolveIpmoeMedProfileOrder(medProfileOrderList);
		String ipmoeOrderNum = ipmoeMedProfileOrder == null ? null : ipmoeMedProfileOrder.getOrderNum();
		
		List<MedProfileOrder> resultList = new ArrayList<MedProfileOrder>();
		
		for (MedProfileOrder medProfileOrder: medProfileOrderList) {
			if (medProfileOrder.getOrderNum().equals(orderNum) || medProfileOrder.getOrderNum().equals(ipmoeOrderNum)) {
				resultList.add(medProfileOrder);
			}
		}
		
		return resultList;
	}
	
	@SuppressWarnings("unchecked")
	private List<MedProfile> fetchMedProfilesWithItemsToBeEnd() {
		
		return em.createQuery(
				"select distinct p from MedProfile p join p.medProfileItemList i" + // 20160809 index check : MedProfile.status : I_MED_PROFILE_02
				" where p.status in :notInactiveStatus" +
				" and i.status in :activeItemStatusList" +
				" and i.medProfileMoItem.endDate < :currentDate order by p.id")
				.setParameter("notInactiveStatus", MedProfileStatus.Not_Inactive)
				.setParameter("activeItemStatusList", MedProfileItemStatus.Unvet_Vetted_Verified)
				.setParameter("currentDate", new Date())
				.getResultList();
	}
	
	private MedProfileOrder resolveIpmoeMedProfileOrder(List<MedProfileOrder> medProfileOrderList) {
		
		MedProfileOrder result = null;
		
		for (MedProfileOrder medProfileOrder: medProfileOrderList) {
			if (MedProfileUtils.isManualProfile(medProfileOrder.getOrderNum())) {
				continue;
			}
			if (medProfileOrder.getType() == MedProfileOrderType.Create) {
				return medProfileOrder;
			}
			if (result == null && medProfileOrder.getWorkstore() != null && medProfileOrder.getCaseNum() != null) {
				result = medProfileOrder;
			}
		}
		return result;
	}
	
	private MedProfile fetchMedProfile(String orderNum, List<MedProfileOrder> medProfileOrderList) {
		
		MedProfile medProfile = fetchMedProfileByOrderNum(orderNum);
		MedProfileOrder ipmoeMedProfileOrder = resolveIpmoeMedProfileOrder(medProfileOrderList);
		
		if (medProfile == null) {
			medProfile = fetchManualProfileByMedProfileOrder(ipmoeMedProfileOrder);
			if (medProfile == null) {
				return new MedProfile();
			}
		}
		
		if (Boolean.TRUE.equals(medProfile.getDeliveryGenFlag()) ||
			Boolean.TRUE.equals(medProfile.getProcessingFlag())) {
			return null;
		} else if (ipmoeMedProfileOrder != null) {
			updateOrderNum(medProfile, ipmoeMedProfileOrder.getOrderNum());
		}
		return medProfile;
	}
	
	@SuppressWarnings("unchecked")
	private void updateOrderNum(MedProfile medProfile, String newOrderNum) {
		
		if (!medProfile.isManualProfile() || medProfile.getOrderNum().equals(newOrderNum) || MedProfileUtils.isManualProfile(newOrderNum)) {
			return;
		}
		
		List<MedProfileMoItemAlert> medProfileMoItemAlertList = em.createQuery(
				"select o from MedProfileMoItemAlert o" + // 20160615 index check : MedProfileItem.medProfileId : FK_MED_PROFILE_ITEM_01
				" where o.medProfileMoItem.medProfileItem.medProfileId = :medProfileId")
				.setParameter("medProfileId", medProfile.getId())
				.getResultList();
		
		for (MedProfileMoItemAlert medProfileMoItemAlert: medProfileMoItemAlertList) {
			medProfileMoItemAlert.getAlert().replaceOrderNum(null, newOrderNum);
			medProfileMoItemAlert.preSave();
		}
		
		auditLogger.log("#0986", medProfile.getId(), medProfile.getMedCase().getCaseNum(), medProfile.getOrderNum(), newOrderNum);
		medProfile.setOrderNum(newOrderNum);
		em.flush();
	}
	
	@SuppressWarnings("unchecked")
	private MedProfile fetchMedProfileByOrderNum(String orderNum) {

		return (MedProfile) MedProfileUtils.getFirstItem(em.createQuery(
				"select p from MedProfile p" + // 20120225 index check : MedProfile.orderNum : UI_MED_PROFILE_01
				" where p.orderNum = :orderNum order by p.id")
				.setParameter("orderNum", orderNum)
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
				.getResultList());
	}
	
	@SuppressWarnings("unchecked")
	private MedProfile fetchManualProfileByMedProfileOrder(MedProfileOrder medProfileOrder) {
		
		if (medProfileOrder == null) {
			return null;
		}
		
		return (MedProfile) MedProfileUtils.getFirstItem(em.createQuery(
				"select p from MedProfile p" + // 20160615 index check : MedCase.caseNum : I_MED_CASE_01
				" where length(p.orderNum) > 12" +
				" and p.hospCode = :hospCode" +
				" and p.patHospCode = :patHospCode" +
				" and p.medCase.caseNum = :caseNum" +
				" and (p.status <> :status or p.inactiveType = :inactiveType)" +
				" order by p.createDate desc")
				.setParameter("hospCode", medProfileOrder.getHospCode())
				.setParameter("patHospCode", medProfileOrder.getPatHospCode())
				.setParameter("caseNum", medProfileOrder.getCaseNum())
				.setParameter("status", MedProfileStatus.Inactive)
				.setParameter("inactiveType", InactiveType.Deleted)
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
				.getResultList());
	}
	
	@SuppressWarnings("unchecked")
	private MedProfileMoItem fetchPreviousMedProfileMoItem(MedProfile medProfile, Integer itemNum) {
		
		if (medProfile.getId() == null || itemNum == null) {
			return null;
		}
		
		List<MedProfileMoItem> medProfileMoItemList = em.createQuery(
				"select m from MedProfileMoItem m" + // 20120225 index check : MedProfileItem.medProfile : FK_MED_PROFILE_ITEM_01
				" where m.medProfileItem.medProfile = :medProfile" +
				" and m.itemNum = :itemNum" +
				" order by m.id desc")
				.setParameter("medProfile", medProfile)
				.setParameter("itemNum", itemNum)
				.setHint(QueryHints.FETCH, PREVIOUS_MED_PROFILE_MO_ITEM_QUERY_HINT_FETCH)
				.getResultList();
		
		for (MedProfileMoItem medProfileMoItem: medProfileMoItemList) {
			if (medProfileMoItem.getStatus() != MedProfileMoItemStatus.SysDeleted) {
				return medProfileMoItem;
			}
		}
		
		return MedProfileUtils.getFirstItem(medProfileMoItemList);
	}
	
	private MedProfileItem fetchMedProfileItem(MedProfile medProfile, MedProfileOrder medProfileOrder, MedProfileMoItem medProfileMoItem) {
		
		Integer orgItemNum = medProfileMoItem.getOrgItemNum();
		Integer itemNum = medProfileMoItem.getItemNum();
		
		if (orgItemNum != null) {
			
			MedProfileItem medProfileItem = fetchMedProfileItemByOrgItemNum(medProfile, orgItemNum);
			if (medProfileItem != null) {
				return medProfileItem;
			}			
		} else {

			MedProfileItem medProfileItem = fetchMedProfileItemByItemNum(medProfile, itemNum);
			if (medProfileItem == null) {
				throw new OrderProcessingException("MedProfileItem cannot be found for ItemNum : " + itemNum);
			} else {
				return medProfileItem;
			}
		}
		
		if (!MedProfileOrderType.Create_Add.contains(medProfileOrder.getType())) {
			throw new OrderProcessingException("Unexpected Order Type " + medProfileOrder.getType() +
					" while the original item(ItemNum=" + orgItemNum +
					") cannot be found for MedProfileOrder(ID=" + medProfileOrder.getId() +
					", OrderNum=" + medProfileOrder.getOrderNum() +")");
		}
		
		MedProfileItem medProfileItem = new MedProfileItem();
		medProfileItem.setMedProfile(medProfile);
		medProfileItem.setOrgItemNum(orgItemNum);
		em.persist(medProfileItem);
		
		return medProfileItem;
	}
	
	@SuppressWarnings("unchecked")
	private MedProfileItem fetchMedProfileItemByOrgItemNum(MedProfile medProfile, Integer orgItemNum) {
		
		return (MedProfileItem) MedProfileUtils.getFirstItem(em.createQuery(
				"select i from MedProfileItem i" + // 20120225 index check : MedProfileItem.medProfile : FK_MED_PROFILE_ITEM_01
				" where i.medProfile = :medProfile and i.orgItemNum = :orgItemNum" +
				" order by i.id")
			.setParameter("medProfile", medProfile)
			.setParameter("orgItemNum", orgItemNum)
			.getResultList());
	}
	
	@SuppressWarnings("unchecked")
	private MedProfileItem fetchMedProfileItemByItemNum(MedProfile medProfile, Integer itemNum) {
		
		return (MedProfileItem) MedProfileUtils.getFirstItem(em.createQuery(
				"select i from MedProfileMoItem m join m.medProfileItem i" + // 20120225 index check : MedProfileItem.medProfile : FK_MED_PROFILE_ITEM_01
				" where m.itemNum = :itemNum and m.medProfileItem.medProfile = :medProfile" +
				" order by i.id")
			.setParameter("medProfile", medProfile)
			.setParameter("itemNum", itemNum)
			.getResultList());
	}
	
	@SuppressWarnings("unchecked")
	private List<MedProfile> fetchProcessingMedProfileList(Long workstationId) {
		
		return em.createQuery(
				"select m from MedProfile m" + // 20130320 index check : MedProfile.workstationId : I_MED_PROFILE_03
				" where m.workstationId = :workstationId" +
				" and m.processingFlag = true" +
				" and m.deliveryGenFlag = false")
				.setParameter("workstationId", workstationId)
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	private MedProfile fetchLockedProcessingMedProfile(Long medProfileId, Long version, Long workstationId, boolean noWait) {
		
		if (workstationId == null) {
			return null;
		}
		
		Query query = em.createQuery(
				"select m from MedProfile m" + // 20120225 index check : MedProfile.id : PK_MED_PROFILE
				" where m.id = :medProfileId" +
				(version != null ? " and m.version = :version" : "") +
				" and m.workstationId = :workstationId" +
				" and m.processingFlag = true" +
				" and m.deliveryGenFlag = false")
				.setParameter("medProfileId", medProfileId)
				.setParameter("workstationId", workstationId)
				.setHint(QueryHints.PESSIMISTIC_LOCK, noWait ? PessimisticLock.LockNoWait : PessimisticLock.Lock);
		
		if (version != null) {
			query.setParameter("version", version);
		}
		return (MedProfile) MedProfileUtils.getFirstItem(query.getResultList());
	}
	
	private boolean isAbnormalOrder(MedProfile medProfile, MedProfileOrder medProfileOrder) {
		
		Integer itemNum = resolveItemNum(medProfileOrder);
		
		if (itemNum == null) {
			return false;
		}
		
		MedProfileMoItem medProfileMoItemInDB = fetchPreviousMedProfileMoItem(medProfile, itemNum);
		
		if (MedProfileOrderType.Create_Add_Modify.contains(medProfileOrder.getType())) {
			return isAbnormalCreateAddModifyOrder(medProfile, medProfileOrder, itemNum, medProfileMoItemInDB);
		} else {
			return isAbnormalOtherOrder(medProfile, medProfileOrder, itemNum, medProfileMoItemInDB);
		}
	}
	
	private boolean isAbnormalCreateAddModifyOrder(MedProfile medProfile, MedProfileOrder medProfileOrder, Integer itemNum, MedProfileMoItem medProfileMoItemInDB) {
		
		List<MedProfileMoItem> medProfileMoItemList = medProfileOrder.getMedProfileMoItemList();
		
		if (medProfileOrder.getType() == MedProfileOrderType.Modify) {
			if (medProfileMoItemList.size() != 2) {
				logger.fatal("Unexpected Number of MedProfileMoItem in MedProfileOrder(id=#1, orderNum=#2, caseNum=#3, type=#4)." +
						" Expected 2, current #5", medProfileOrder.getId(), medProfileOrder.getOrderNum(),
						medProfileOrder.getCaseNum(), medProfileOrder.getType(), medProfileMoItemList.size());
				return true;
			}
			if (isAbnormalOtherOrder(medProfile, medProfileOrder, itemNum, medProfileMoItemInDB)) {
				return true;
			}
			itemNum = medProfileMoItemList.get(1).getItemNum();
			medProfileMoItemInDB = fetchPreviousMedProfileMoItem(medProfile, itemNum);
		}
		if (medProfileMoItemInDB != null) {
			logger.fatal("Duplicate MedProfileMoItem(itemNum=#0) found while processing " +
					"MedProfileOrder(id=#1, orderNum=#2, caseNum=#3, type=#4)", itemNum,
					medProfileOrder.getId(), medProfileOrder.getOrderNum(), medProfileOrder.getCaseNum(), medProfileOrder.getType());
			return true;
		} else {
			return false;
		}
	}
	
	private boolean isAbnormalOtherOrder(MedProfile medProfile, MedProfileOrder medProfileOrder, Integer itemNum, MedProfileMoItem medProfileMoItemInDB) {
		
		if (medProfileMoItemInDB == null) {
			logger.info("Previous MedProfileMoItem(itemNum=#0) not found while processing " +
					"MedProfileOrder(id=#1, orderNum=#2, caseNum=#3, type=#4)", itemNum,
					medProfileOrder.getId(), medProfileOrder.getOrderNum(), medProfileOrder.getCaseNum(), medProfileOrder.getType());
			return true;
		}
		return false;
	}
	
	private void beginTransaction() throws NotSupportedException, SystemException {
		beginTransaction(0);
	}
	
	private void beginTransaction(int timeout) throws NotSupportedException, SystemException {
		if (timeout > 0) {
			userTransaction.setTransactionTimeout(timeout);
		}
		userTransaction.begin();
	}
	
	private void endTransaction(boolean success) {
		if (success) {
			commit();
		} else {
			rollback();
		}
	}

	private void commit() {
		try {
			userTransaction.commit();
		} catch (Exception ex) {
		}
	}	
	
	private void rollback() {
		try {
			userTransaction.rollback();
		} catch (Exception ex) {
		}
	}
		
	public void triggerMedProfileTimer1(Long duration, Long intervalDuration) {
		
		try {
			if (timerProp.isActivate()) 
			{						
				if (hospitalClusterTaskManager.lockHospitalClusterTask(
						UPDATE_MED_PROFILE_TIMER1, 
						new Date(), 
						intervalDuration.longValue())) {			
					processMedProfileTimer1Task();
				} else {
					logger.info("MedProfileTimer1 has been run previously within #0ms, bypass.", intervalDuration);
				}
			} else {
				logger.warn("MedProfileTimer1 temporary disabled by " + TimerProp.TIMER_PROPERTIES);
			}
		} catch (Exception e) {
			logger.error(e,e);
		}
	}
	
	public void triggerMedProfileTimer2(Long duration, Long intervalDuration) {
		
		try {
			if (timerProp.isActivate()) 
			{						
				if (hospitalClusterTaskManager.lockHospitalClusterTask(
						UPDATE_MED_PROFILE_TIMER2, 
						new Date(), 
						intervalDuration.longValue())) {			
					processMedProfileTimer2Task();
				} else {
					logger.info("MedProfileTimer2 has been run previously within #0ms, bypass.", intervalDuration);
				}
			} else {
				logger.warn("MedProfileTimer2 temporary disabled by " + TimerProp.TIMER_PROPERTIES);
			}
		} catch (Exception e) {
			logger.error(e,e);
		}
	}
	
	private static class MedProfileTypeComparator implements Comparator<String> {

		@Override
		public int compare(String orderNum1, String orderNum2) {
			return MedProfileUtils.compare(!MedProfileUtils.isManualProfile(orderNum1), !MedProfileUtils.isManualProfile(orderNum2));
		}
	}
}
