package hk.org.ha.model.pms.biz.mr;
import hk.org.ha.model.pms.asa.exception.mr.MrException;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.vo.mr.MedProfileMrItem;

import java.util.List;

import javax.ejb.Local;

import org.jboss.seam.security.Identity;

@Local
public interface MrManagerLocal {

	List<MedProfileMrItem> retrieveOnhandMedicationList(MedProfile medProfile) throws MrException;
	List<MedProfileMrItem> retrieveCurrentMedicationList(MedProfile medProfile) throws MrException;
	boolean addMrAnnotation(Identity identity, String patHospCode, String orderNum,
			Integer itemNum, String mrAnnotation) throws MrException;
}
