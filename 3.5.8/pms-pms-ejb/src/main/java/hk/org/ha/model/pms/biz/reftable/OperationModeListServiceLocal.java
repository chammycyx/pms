package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.persistence.reftable.OperationProp;
import hk.org.ha.model.pms.persistence.reftable.OperationWorkstation;
import hk.org.ha.model.pms.persistence.reftable.PickStation;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.persistence.reftable.WorkstoreProp;

import java.util.Collection;

import javax.ejb.Local;

@Local
public interface OperationModeListServiceLocal {

	void retrievePickStationList();
	
	void retrieveOperationModeList();
	
	void updateOperationModeList(Collection<OperationProp> operationPropList, Collection<OperationWorkstation> operationWorkstationList, 
									Collection<Workstation> newWorkstationList, Collection<PickStation> pickStationList, 
									Long activeOperationProfileId, Collection<WorkstoreProp> workstorePropList);

	void updateActiveProfile(Long activeOperationProfileId, boolean setInvalidateSession);
	
	boolean isInvalidateSession();
	
	void destroy();
	
}
