package hk.org.ha.model.pms.vo.alert.ehr;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class AdrDetail implements Serializable {

	private static final long serialVersionUID = 1L;

	private String vtmTermId;
	
	private String vtmEhrDesc;
	
	private String adrDesc;

	private String severity;

	private String remark;
	
	private String note;
	
	private String deleteReason;

	private Date lastUpdate;
	
	private String instCode;
	
	private String instDesc;

	private List<AdrDetail> childList;
	
	public String getVtmTermId() {
		return vtmTermId;
	}

	public void setVtmTermId(String vtmTermId) {
		this.vtmTermId = vtmTermId;
	}

	public String getVtmEhrDesc() {
		return vtmEhrDesc;
	}

	public void setVtmEhrDesc(String vtmEhrDesc) {
		this.vtmEhrDesc = vtmEhrDesc;
	}

	public String getAdrDesc() {
		return adrDesc;
	}

	public void setAdrDesc(String adrDesc) {
		this.adrDesc = adrDesc;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getDeleteReason() {
		return deleteReason;
	}

	public void setDeleteReason(String deleteReason) {
		this.deleteReason = deleteReason;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getInstCode() {
		return instCode;
	}

	public void setInstCode(String instCode) {
		this.instCode = instCode;
	}

	public String getInstDesc() {
		return instDesc;
	}

	public void setInstDesc(String instDesc) {
		this.instDesc = instDesc;
	}

	public List<AdrDetail> getChildList() {
		return childList;
	}

	public void setChildList(List<AdrDetail> childList) {
		this.childList = childList;
	}
}
