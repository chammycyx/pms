package hk.org.ha.model.pms.biz.report;

import static hk.org.ha.model.pms.prop.Prop.PIVAS_SATELLITE_ENABLED;
import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.fmk.pms.util.QueryUtils;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmMoeProperty;
import hk.org.ha.model.pms.dms.udt.pivas.PivasFormulaType;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.persistence.pivas.PivasBatchPrep;
import hk.org.ha.model.pms.persistence.pivas.PivasWorklist;
import hk.org.ha.model.pms.udt.YesNoBlankFlag;
import hk.org.ha.model.pms.udt.pivas.PivasBatchPrepStatus;
import hk.org.ha.model.pms.udt.pivas.PivasWorklistStatus;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.report.PivasExportType;
import hk.org.ha.model.pms.udt.report.PivasItemCat;
import hk.org.ha.model.pms.udt.report.PivasReportType;
import hk.org.ha.model.pms.udt.report.PivasRptDispMethod;
import hk.org.ha.model.pms.udt.report.PivasRptItemCatFilterOption;
import hk.org.ha.model.pms.vo.label.LabelContainer;
import hk.org.ha.model.pms.vo.label.PivasMergeFormulaProductLabel;
import hk.org.ha.model.pms.vo.label.PivasMergeFormulaWorksheet;
import hk.org.ha.model.pms.vo.label.PivasMergeFormulaWorksheetDrugInstructionDtl;
import hk.org.ha.model.pms.vo.label.PivasMergeFormulaWorksheetRawMaterialDtl;
import hk.org.ha.model.pms.vo.label.PivasProductLabel;
import hk.org.ha.model.pms.vo.label.PivasWorksheet;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.PivasBatchDispRpt;
import hk.org.ha.model.pms.vo.report.PivasBatchDispRptDtl;
import hk.org.ha.model.pms.vo.report.PivasBatchDispRptRawMaterialDtl;
import hk.org.ha.model.pms.vo.report.PivasConsumptionRpt;
import hk.org.ha.model.pms.vo.report.PivasConsumptionRptDtl;
import hk.org.ha.model.pms.vo.report.PivasPatientDispRpt;
import hk.org.ha.model.pms.vo.report.PivasPatientDispRptDtl;
import hk.org.ha.model.pms.vo.report.PivasRptHdr;
import hk.org.ha.model.pms.vo.report.PivasWorkloadSummaryBatchRpt;
import hk.org.ha.model.pms.vo.report.PivasWorkloadSummaryBatchRptDtl;
import hk.org.ha.model.pms.vo.report.PivasWorkloadSummaryPatientRpt;
import hk.org.ha.model.pms.vo.report.PivasWorkloadSummaryPatientRptDtl;
import hk.org.ha.model.pms.vo.report.PivasWorkloadSummaryPatientRptWardSummaryDtl;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import net.lingala.zip4j.exception.ZipException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.joda.time.DateMidnight;

@Stateful
@Scope(ScopeType.SESSION)
@Name("pivasRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PivasRptServiceBean implements PivasRptServiceLocal {
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent; 
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In 
	private Workstore workstore;

	
	private List<PivasConsumptionRpt> pivasConsumptionRptList;
	private List<PivasConsumptionRptDtl> pivasConsumptionRptDtlList;
	
	private List<PivasPatientDispRpt> pivasPatientDispRptList;
	private List<PivasPatientDispRptDtl> pivasPatientDispRptDtlList;
	
	private List<PivasBatchDispRpt> pivasBatchDispRptList;
	private List<PivasBatchDispRptDtl> pivasBatchDispRptDtlList;
	private List<PivasBatchDispRptDtl> pivasBatchDispRptXlsDtlList;
	
	private List<PivasWorkloadSummaryPatientRpt> pivasWorkloadSummaryPatientRptList;
	private List<PivasWorkloadSummaryPatientRptDtl> pivasWorkloadSummaryPatientRptDtlList;
	private List<PivasWorkloadSummaryPatientRptDtl> pivasWorkloadSummaryPatientXlsList;
	private List<PivasWorkloadSummaryPatientRptWardSummaryDtl> pivasWorkloadSummaryPatientRptWardSummaryDtlList;

	private List<PivasWorkloadSummaryBatchRpt> pivasWorkloadSummaryBatchRptList;
	private List<PivasWorkloadSummaryBatchRptDtl> pivasWorkloadSummaryBatchRptNormalDtlList;
	private List<PivasWorkloadSummaryBatchRptDtl> pivasWorkloadSummaryBatchRptMergeDtlList;
	private List<PivasWorkloadSummaryBatchRptDtl> pivasWorkloadSummaryBatchXlsList;
	
	private Map<String, PivasWorkloadSummaryPatientRptDtl> pivasWorkloadSummaryPatientRptDtlMap;
	private Map<String, PivasWorkloadSummaryPatientRptDtl> pivasWorkloadSummaryPatientXlsMap;
	private Map<String, Integer> pivasWorkloadSummaryPatientRptWardSummaryDtlMap;
	
	private Map<String, PivasWorkloadSummaryBatchRptDtl> pivasWorkloadSummaryBatchRptNormalDtlMap;
	private Map<String, PivasWorkloadSummaryBatchRptDtl> pivasWorkloadSummaryBatchRptMergeDtlMap;
	private Map<String, PivasWorkloadSummaryBatchRptDtl> pivasWorkloadSummaryBatchXlsMap;
	
	private boolean retrieveSuccess;
	
	private Map<String, Object> parameters = new HashMap<String, Object>();
	private PivasReportType currPivasReportType = null;
	private List<?> reportObj = null;
	private List<?> exportObj = null;
	private String xlsName = null;
	private String exportPassword;
	private static final String NEW_LINE = "\n";
	private static final String NEW = "New";
	private static final String REFILL = "Refill";
	private static final String SPACE = " ";
	private static final String UNIT_ML = "ML";
	
	
	private static final String PIVAS_BATCH_PREP_TARGET_ITEM_CODE_QUERY = "o.pivasBatchPrep.itemCode = :itemCode";
	private static final String DRUG_ITEM_CODE_QUERY = "o.drugItemCode = :itemCode";
	private static final String DRUG_MANUF_CODE_QUERY = "o.drugManufCode = :manufCode";
	private static final String DRUG_BATCH_NUM_QUERY = "o.drugBatchNum = :batchNum";
	private static final String SOLVENT_ITEM_CODE_QUERY = "o.solventItemCode = :itemCode";
	private static final String SOLVENT_MANUF_CODE_QUERY = "o.solventManufCode = :manufCode";
	private static final String SOLVENT_BATCH_NUM_QUERY = "o.solventBatchNum = :batchNum";
	private static final String PIVAS_PREP_METHOD_DILUENT_ITEM_CODE_QUERY = "o.pivasPrepMethod.diluentItemCode = :itemCode";
	private static final String PIVAS_PREP_METHOD_DILUENT_MANUF_CODE_QUERY = "o.pivasPrepMethod.diluentManufCode = :manufCode";
	private static final String PIVAS_PREP_METHOD_DILUENT_BATCH_NUM_QUERY = "o.pivasPrepMethod.diluentBatchNum = :batchNum";
	private static final String PIVAS_BATCH_PREP_DILUENT_ITEM_CODE_QUERY = "o.pivasBatchPrep.diluentItemCode = :itemCode";
	private static final String PIVAS_BATCH_PREP_DILUENT_MANUF_CODE_QUERY = "o.pivasBatchPrep.diluentManufCode = :manufCode";
	private static final String PIVAS_BATCH_PREP_DILUENT_BATCH_NUM_QUERY = "o.pivasBatchPrep.diluentBatchNum = :batchNum";
	
	private final JaxbWrapper<Object> labelJaxbWrapper = JaxbWrapper.instance("hk.org.ha.model.pms.vo.label");
	private static PivasConsumptionRptDtlComparator pivasConsumptionRptDtlComparator = new PivasConsumptionRptDtlComparator();
	private static PivasPatientDispRptDtlComparator pivasPatientDispRptDtlComparator = new PivasPatientDispRptDtlComparator();
	private static PivasBatchDispRptDtlComparator pivasBatchDispRptDtlComparator = new PivasBatchDispRptDtlComparator();
	private static PivasBatchDispRptRawMaterialDtlComparator pivasBatchDispRptRawMaterialDtlComparator = new PivasBatchDispRptRawMaterialDtlComparator();
	
	private static PivasWorkloadSummaryPatientRptDtlComparator pivasWorkloadSummaryPatientRptDtlComparator = new PivasWorkloadSummaryPatientRptDtlComparator();
	private static PivasWorkloadSummaryPatientXlsComparator pivasWorkloadSummaryPatientXlsComparator = new PivasWorkloadSummaryPatientXlsComparator();
	private static PivasWorkloadSummaryPatientRptWardSummaryDtlComparator pivasWorkloadSummaryPatientRptWardSummaryDtlComparator = new PivasWorkloadSummaryPatientRptWardSummaryDtlComparator();
	
	private static PivasWorkloadSummaryBatchRptNormalDtlComparator pivasWorkloadSummaryBatchRptNormalDtlComparator = new PivasWorkloadSummaryBatchRptNormalDtlComparator();
	private static PivasWorkloadSummaryBatchRptMergeDtlComparator pivasWorkloadSummaryBatchRptMergeDtlComparator = new PivasWorkloadSummaryBatchRptMergeDtlComparator();
	private static PivasWorkloadSummaryBatchXlsComparator pivasWorkloadSummaryBatchXlsComparator = new PivasWorkloadSummaryBatchXlsComparator();
	
	
	public void retrievePivasRpt(PivasReportType reportType, Date supplyDateFrom, Date supplyDateTo, YesNoBlankFlag satelliteFlag, 
									String hkidCaseNum, PivasRptDispMethod pivasRptDispMethod, PivasRptItemCatFilterOption pivasRptItemCatFilterOption, 
									String itemCode, Boolean drugKeyFlag, String manufCode, String batchNum) {
		
		if (reportType == PivasReportType.PivasConsumptionReport) {
			currPivasReportType = reportType;
			reportObj = retrievePivasConsumptionRptList(supplyDateFrom, supplyDateTo, satelliteFlag, 
														pivasRptDispMethod, pivasRptItemCatFilterOption,
														itemCode, drugKeyFlag, manufCode, batchNum);
			xlsName = PivasExportType.PivasConsumptionExport.getDataValue();
			exportObj = pivasConsumptionRptDtlList;
		
		} else if (reportType == PivasReportType.PivasPatientDispReport) {
			currPivasReportType = reportType;
			reportObj = retrievePivasPatientDispRptList(supplyDateFrom, supplyDateTo, satelliteFlag, 
														hkidCaseNum, pivasRptItemCatFilterOption, 
														itemCode, drugKeyFlag, manufCode, batchNum);
			xlsName = PivasExportType.PivasPatientDispReport.getDataValue();
			exportObj = pivasPatientDispRptDtlList;
		
		} else if (reportType == PivasReportType.PivasBatchDispReport) {
			currPivasReportType = reportType;
			reportObj = retrievePivasBatchDispRptList(supplyDateFrom, supplyDateTo, 
													  pivasRptItemCatFilterOption, 
													  itemCode, drugKeyFlag, manufCode, batchNum);
			xlsName = PivasExportType.PivasBatchDispReport.getDataValue();
			exportObj = pivasBatchDispRptXlsDtlList;
		} else if (reportType == PivasReportType.PivasWorkloadSummaryPatientRpt) {
			currPivasReportType = reportType;
			reportObj = retrievePivasWorkloadSummaryPatientRptList(supplyDateFrom, supplyDateTo, satelliteFlag,  
																	hkidCaseNum, pivasRptItemCatFilterOption, 
																	itemCode, drugKeyFlag, manufCode, batchNum);
			xlsName = PivasExportType.PivasWorkloadSummaryPatientRpt.getDataValue();
			// exportObj uses different list with reportObj
			exportObj = pivasWorkloadSummaryPatientXlsList;
		} else if (reportType == PivasReportType.PivasWorkloadSummaryBatchRpt) {
			currPivasReportType = reportType;
			reportObj = retrievePivasWorkloadSummaryBatchRptList(supplyDateFrom, supplyDateTo,  
																  pivasRptItemCatFilterOption, 
																  itemCode, drugKeyFlag, manufCode, batchNum);
			xlsName = PivasExportType.PivasWorkloadSummaryBatchRpt.getDataValue();
			// exportObj uses different list with reportObj
			exportObj = pivasWorkloadSummaryBatchXlsList;
		}
	}

	
	private List<PivasConsumptionRpt> retrievePivasConsumptionRptList(Date supplyDateFrom, Date supplyDateTo, YesNoBlankFlag satelliteFlag, 
																		PivasRptDispMethod pivasRptDispMethod, PivasRptItemCatFilterOption pivasRptItemCatFilterOption,
																		String itemCode, Boolean drugKeyFlag, String manufCode, String batchNum) {
		
		PivasConsumptionRpt pivasConsumptionRpt = new PivasConsumptionRpt();
		Map<String, PivasConsumptionRptDtl> pivasConsumptionRptDtlMap = new HashMap<String, PivasConsumptionRptDtl>();
		
		//  satelliteFlag for PivasRptDispMethod Batch Dispensing should be BLANK only
		if (pivasRptDispMethod != PivasRptDispMethod.PatientDisp && satelliteFlag == YesNoBlankFlag.Blank) {
			// PivasConsumptionReport processes pivasRptItemCatFilterOption after retrieving PivasWorklist and PivasBatchPrep record
			List<PivasBatchPrep> pivasBatchPrepList = retrievePivasBatchPrepList(supplyDateFrom, supplyDateTo, 
																					null, 
																					null, false, null, null);
			
			if (pivasBatchPrepList != null && !pivasBatchPrepList.isEmpty()) {
				
				Pair<List<PivasBatchPrep>,List<PivasBatchPrep>> pivasBatchPrepListWithPivasFormulaTypePair = buildPivasBatchPrepListWithPivasFormulaTypePair(pivasBatchPrepList);
				
				if (pivasBatchPrepListWithPivasFormulaTypePair.getFirst() != null && !pivasBatchPrepListWithPivasFormulaTypePair.getFirst().isEmpty()) {
					pivasConsumptionRptDtlMap = buildPivasConsumptionRptNormalDtlMapByPivasBatchPrepList(pivasBatchPrepListWithPivasFormulaTypePair.getFirst(), pivasConsumptionRptDtlMap, pivasRptItemCatFilterOption);
				}
				
				if (pivasBatchPrepListWithPivasFormulaTypePair.getSecond() != null && !pivasBatchPrepListWithPivasFormulaTypePair.getSecond().isEmpty()){
					pivasConsumptionRptDtlMap = buildPivasConsumptionRptMergeDtlMapByPivasBatchPrepList(pivasBatchPrepListWithPivasFormulaTypePair.getSecond(), pivasConsumptionRptDtlMap, pivasRptItemCatFilterOption);
				}
			}
		}

		//  retrieve pivasWorklist if selected pivasRptDispMethod is not batch dispensing (All / Patient Dispensing)
		if (pivasRptDispMethod != PivasRptDispMethod.BatchDisp) {
			// PivasConsumptionReport processes pivasRptItemCatFilterOption after retrieving PivasWorklist and PivasBatchPrep record
			List<PivasWorklist> pivasWorklistList = retrievePivasWorklistList(supplyDateFrom, supplyDateTo, satelliteFlag, 
																				null, null, 
																				null, null);
			
			if (pivasWorklistList != null && !pivasWorklistList.isEmpty()) {
				List<DeliveryItem> deliveryItemList = retrieveDeliveryItemListByPivasWorklist(pivasWorklistList);
				if (deliveryItemList != null && !deliveryItemList.isEmpty()) {
					pivasConsumptionRptDtlMap = buildPivasConsumptionRptDtlMapByDeliveryItemList(deliveryItemList, pivasConsumptionRptDtlMap, pivasRptItemCatFilterOption);
				}
			}	
		}		
		if (pivasConsumptionRptDtlMap != null && !pivasConsumptionRptDtlMap.isEmpty()) {

			pivasConsumptionRptDtlList = new ArrayList<PivasConsumptionRptDtl>();
			for (PivasConsumptionRptDtl pivasConsumptionRptDtl : pivasConsumptionRptDtlMap.values()) {
				pivasConsumptionRptDtlList.add(pivasConsumptionRptDtl);
			}

			pivasConsumptionRptDtlList = calPivasConsumptionRptDtlQty(pivasConsumptionRptDtlList);
			Collections.sort(pivasConsumptionRptDtlList, pivasConsumptionRptDtlComparator);
			
			pivasConsumptionRpt.setPivasRptHdr(constructPivasRptHdr(supplyDateFrom, supplyDateTo, satelliteFlag, 
																	null, pivasRptDispMethod, pivasRptItemCatFilterOption, 
																	null, null, null, null));
			
			constructPivasRptParameters(supplyDateFrom, supplyDateTo, satelliteFlag, 
										null, pivasRptDispMethod, pivasRptItemCatFilterOption, 
										null, null, null, null);
			
			pivasConsumptionRpt.setPivasConsumptionRptDtlList(pivasConsumptionRptDtlList);
			pivasConsumptionRptList = Arrays.asList(pivasConsumptionRpt);
			
			retrieveSuccess = true;
		} else {
			retrieveSuccess = false;
		}

		return pivasConsumptionRptList;
	}
	
	
	private List<PivasPatientDispRpt> retrievePivasPatientDispRptList(Date supplyDateFrom, Date supplyDateTo, YesNoBlankFlag satelliteFlag,  
																		String hkidCaseNum, PivasRptItemCatFilterOption pivasRptItemCatFilterOption, 
																		String itemCode, Boolean drugKeyFlag, String manufCode, String batchNum) {
			
		List<PivasWorklist> rawPivasWorklistList = retrievePivasWorklistList(supplyDateFrom, supplyDateTo, satelliteFlag, 
																				hkidCaseNum, pivasRptItemCatFilterOption, 
																				itemCode, drugKeyFlag);
			
		List<PivasWorklist> pivasWorklistList = null;
		if (rawPivasWorklistList != null && !rawPivasWorklistList.isEmpty()) {
			// hkidCaseNum is filtered in retrievePivasWorklistList()
			pivasWorklistList = retrievePivasWorklistListByPivasPrepMethodInfo(rawPivasWorklistList, 
																				pivasRptItemCatFilterOption, 
																				itemCode, drugKeyFlag, manufCode, batchNum);
		}
		
		if (pivasWorklistList != null && !pivasWorklistList.isEmpty()) {
			
			List<DeliveryItem> deliveryItemList = retrieveDeliveryItemListByPivasWorklist(pivasWorklistList);
			
			pivasPatientDispRptDtlList = new ArrayList<PivasPatientDispRptDtl>();
			pivasPatientDispRptDtlList = buildPivasPatientDispRptDtlList(deliveryItemList);
			Collections.sort(pivasPatientDispRptDtlList, pivasPatientDispRptDtlComparator);
			PivasPatientDispRpt pivasPatientDispRpt = new PivasPatientDispRpt();
			
			pivasPatientDispRpt.setPivasRptHdr(constructPivasRptHdr(supplyDateFrom, supplyDateTo, satelliteFlag,  
																	hkidCaseNum, null, pivasRptItemCatFilterOption, 
																	itemCode, drugKeyFlag, manufCode, batchNum));
			
			constructPivasRptParameters(supplyDateFrom, supplyDateTo, satelliteFlag,  
										hkidCaseNum, null, pivasRptItemCatFilterOption, 
										itemCode, drugKeyFlag, manufCode, batchNum);
			
			pivasPatientDispRpt.setPivasPatientDispRptDtlList(pivasPatientDispRptDtlList);
			pivasPatientDispRptList = Arrays.asList(pivasPatientDispRpt);
			
			retrieveSuccess = true;
		} else {
			retrieveSuccess = false;
		}
		
		return pivasPatientDispRptList;
	}
	
	
	private List<PivasBatchDispRpt> retrievePivasBatchDispRptList(Date supplyDateFrom, Date supplyDateTo,   
																  PivasRptItemCatFilterOption pivasRptItemCatFilterOption, 
																  String itemCode, Boolean drugKeyFlag, String manufCode, String batchNum) {
		
		List<PivasBatchPrep> pivasBatchPrepList = retrievePivasBatchPrepList(supplyDateFrom, supplyDateTo, 
																				pivasRptItemCatFilterOption, 
																				itemCode, drugKeyFlag, manufCode, batchNum);
		
		if (pivasBatchPrepList != null && !pivasBatchPrepList.isEmpty()) {
			pivasBatchDispRptDtlList = new ArrayList<PivasBatchDispRptDtl>();
			pivasBatchDispRptXlsDtlList = new ArrayList<PivasBatchDispRptDtl>();

			Pair<List<PivasBatchPrep>,List<PivasBatchPrep>> pivasBatchPrepListWithPivasFormulaTypePair = buildPivasBatchPrepListWithPivasFormulaTypePair(pivasBatchPrepList);
				
			if (pivasBatchPrepListWithPivasFormulaTypePair.getFirst() != null && !pivasBatchPrepListWithPivasFormulaTypePair.getFirst().isEmpty()) {
				this.buildPivasBatchDispRptNormalDtlList(pivasBatchPrepListWithPivasFormulaTypePair.getFirst());
			}
			
			if (pivasBatchPrepListWithPivasFormulaTypePair.getSecond() != null && !pivasBatchPrepListWithPivasFormulaTypePair.getSecond().isEmpty()){
				this.buildPivasBatchDispRptMergeDtlList(pivasBatchPrepListWithPivasFormulaTypePair.getSecond());
			}
			

			Collections.sort(pivasBatchDispRptDtlList, pivasBatchDispRptDtlComparator);
			for (PivasBatchDispRptDtl pivasBatchDispRptDtl  : pivasBatchDispRptDtlList) {
				Collections.sort(pivasBatchDispRptDtl.getPivasBatchDispRptRawMaterialDtlList(), pivasBatchDispRptRawMaterialDtlComparator);
			}
			Collections.sort(pivasBatchDispRptXlsDtlList, pivasBatchDispRptDtlComparator);
			
			PivasBatchDispRpt pivasBatchDispRpt = new PivasBatchDispRpt();
			
			pivasBatchDispRpt.setPivasRptHdr(constructPivasRptHdr(supplyDateFrom, supplyDateTo, null,  
																	null, null, pivasRptItemCatFilterOption, 
																	itemCode, drugKeyFlag, manufCode, batchNum));
			
			constructPivasRptParameters(supplyDateFrom, supplyDateTo, null,  
										null, null, pivasRptItemCatFilterOption, 
										itemCode, drugKeyFlag, manufCode, batchNum);
			
			pivasBatchDispRpt.setPivasBatchDispRptDtlList(pivasBatchDispRptDtlList);
			pivasBatchDispRptList = Arrays.asList(pivasBatchDispRpt);
			
			retrieveSuccess = true;
		} else {
			retrieveSuccess = false;
		}
		
		return pivasBatchDispRptList;
	}
	
	
	private List<PivasWorkloadSummaryPatientRpt> retrievePivasWorkloadSummaryPatientRptList(Date supplyDateFrom, Date supplyDateTo, YesNoBlankFlag satelliteFlag,  
																							String hkidCaseNum, PivasRptItemCatFilterOption pivasRptItemCatFilterOption, 
																							String itemCode, Boolean drugKeyFlag, String manufCode, String batchNum) {

		List<PivasWorklist> rawPivasWorklistList = retrievePivasWorklistList(supplyDateFrom, supplyDateTo, satelliteFlag, 
																			hkidCaseNum, pivasRptItemCatFilterOption, 
																			itemCode, drugKeyFlag);
		
		List<PivasWorklist> pivasWorklistList = null;
		if (rawPivasWorklistList != null && !rawPivasWorklistList.isEmpty()) {
			// hkidCaseNum is filtered in retrievePivasWorklistList()
			pivasWorklistList = retrievePivasWorklistListByPivasPrepMethodInfo(rawPivasWorklistList, 
																				pivasRptItemCatFilterOption, 
																				itemCode, drugKeyFlag, manufCode, batchNum);
		}
			
		if (pivasWorklistList != null && !pivasWorklistList.isEmpty()) {
			List<DeliveryItem> deliveryItemList = retrieveDeliveryItemListByPivasWorklist(pivasWorklistList);
			
			// Initialize map before call in build function
			pivasWorkloadSummaryPatientRptDtlMap = new HashMap<String, PivasWorkloadSummaryPatientRptDtl>();
			pivasWorkloadSummaryPatientXlsMap = new HashMap<String, PivasWorkloadSummaryPatientRptDtl>();
			pivasWorkloadSummaryPatientRptWardSummaryDtlMap = new HashMap<String, Integer>();
			
			// build DtlMap, XlsMap, WardSummaryDtlMap
			this.buildPivasWorkloadSummaryPatientRptMap(deliveryItemList);
			
			pivasWorkloadSummaryPatientRptDtlList = buildPivasWorkloadSummaryPatientRptDtlList(pivasWorkloadSummaryPatientRptDtlMap);
			Collections.sort(pivasWorkloadSummaryPatientRptDtlList, pivasWorkloadSummaryPatientRptDtlComparator);
			// list is sorted till ward and fixed in ordering
			pivasWorkloadSummaryPatientRptDtlList = modifyIndPivasWorkloadSummaryPatientRptPatientDtlList(pivasWorkloadSummaryPatientRptDtlList);
			
			
			// build export list by using common function
			pivasWorkloadSummaryPatientXlsList = buildPivasWorkloadSummaryPatientRptDtlList(pivasWorkloadSummaryPatientXlsMap);
			Collections.sort(pivasWorkloadSummaryPatientXlsList, pivasWorkloadSummaryPatientXlsComparator);
			
			
			pivasWorkloadSummaryPatientRptWardSummaryDtlList = buildPivasWorkloadSummaryPatientRptWardSummaryDtlList(pivasWorkloadSummaryPatientRptWardSummaryDtlMap);
			Collections.sort(pivasWorkloadSummaryPatientRptWardSummaryDtlList, pivasWorkloadSummaryPatientRptWardSummaryDtlComparator);

			
			PivasWorkloadSummaryPatientRpt pivasWorkloadSummaryPatientRpt = new PivasWorkloadSummaryPatientRpt();
				
			pivasWorkloadSummaryPatientRpt.setPivasRptHdr(constructPivasRptHdr(supplyDateFrom, supplyDateTo, satelliteFlag,  
																				null, null, null, 
																				itemCode, drugKeyFlag, null, null));
				
			constructPivasRptParameters(supplyDateFrom, supplyDateTo, satelliteFlag,  
										null, null, null, 
										itemCode, drugKeyFlag, null, null);
				
			pivasWorkloadSummaryPatientRpt.setPivasWorkloadSummaryPatientRptDtlList(pivasWorkloadSummaryPatientRptDtlList);
			pivasWorkloadSummaryPatientRpt.setPivasWorkloadSummaryPatientRptXlsList(pivasWorkloadSummaryPatientXlsList);
			pivasWorkloadSummaryPatientRpt.setPivasWorkloadSummaryPatientRptWardSummaryDtlList(pivasWorkloadSummaryPatientRptWardSummaryDtlList);
			pivasWorkloadSummaryPatientRptList = Arrays.asList(pivasWorkloadSummaryPatientRpt);
				
			retrieveSuccess = true;
		} else {
			retrieveSuccess = false;
		}
		
		return pivasWorkloadSummaryPatientRptList;
	}
	
	private List<PivasWorkloadSummaryBatchRpt> retrievePivasWorkloadSummaryBatchRptList(Date supplyDateFrom, Date supplyDateTo,   
																					  PivasRptItemCatFilterOption pivasRptItemCatFilterOption, 
																					  String itemCode, Boolean drugKeyFlag, String manufCode, String batchNum) {
																				
		List<PivasBatchPrep> pivasBatchPrepList = retrievePivasBatchPrepList(supplyDateFrom, supplyDateTo, 
																			pivasRptItemCatFilterOption, 
																			itemCode, drugKeyFlag, manufCode, batchNum);
	
		if (pivasBatchPrepList != null && !pivasBatchPrepList.isEmpty()) {
			Pair<List<PivasBatchPrep>,List<PivasBatchPrep>> pivasBatchPrepListWithPivasFormulaTypePair = buildPivasBatchPrepListWithPivasFormulaTypePair(pivasBatchPrepList);
			
			// Initialize map before call in build function
			pivasWorkloadSummaryBatchRptNormalDtlMap = new HashMap<String, PivasWorkloadSummaryBatchRptDtl>();
			pivasWorkloadSummaryBatchRptMergeDtlMap = new HashMap<String, PivasWorkloadSummaryBatchRptDtl>();
			pivasWorkloadSummaryBatchXlsMap = new HashMap<String, PivasWorkloadSummaryBatchRptDtl>();
					
			// build DtlMap, XlsMap
			if (pivasBatchPrepListWithPivasFormulaTypePair.getFirst() != null && !pivasBatchPrepListWithPivasFormulaTypePair.getFirst().isEmpty()) {
				this.buildPivasWorkloadSummaryNormalFormulaBatchRptDtlMap(pivasBatchPrepListWithPivasFormulaTypePair.getFirst());
			}

			if (pivasBatchPrepListWithPivasFormulaTypePair.getSecond() != null && !pivasBatchPrepListWithPivasFormulaTypePair.getSecond().isEmpty()){
				this.buildPivasWorkloadSummaryMergeFormulaBatchRptDtlMap(pivasBatchPrepListWithPivasFormulaTypePair.getSecond());
			}
			
			pivasWorkloadSummaryBatchRptNormalDtlList = buildPivasWorkloadSummaryBatchRptDtlList(pivasWorkloadSummaryBatchRptNormalDtlMap);
			pivasWorkloadSummaryBatchRptMergeDtlList = buildPivasWorkloadSummaryBatchRptDtlList(pivasWorkloadSummaryBatchRptMergeDtlMap);
			Collections.sort(pivasWorkloadSummaryBatchRptNormalDtlList, pivasWorkloadSummaryBatchRptNormalDtlComparator);
			Collections.sort(pivasWorkloadSummaryBatchRptMergeDtlList, pivasWorkloadSummaryBatchRptMergeDtlComparator);
			
			// build export list by using common function
			pivasWorkloadSummaryBatchXlsList = buildPivasWorkloadSummaryBatchRptDtlList(pivasWorkloadSummaryBatchXlsMap);
			Collections.sort(pivasWorkloadSummaryBatchXlsList, pivasWorkloadSummaryBatchXlsComparator);
			
			PivasWorkloadSummaryBatchRpt pivasWorkloadSummaryBatchRpt = new PivasWorkloadSummaryBatchRpt();
			
			pivasWorkloadSummaryBatchRpt.setPivasRptHdr(constructPivasRptHdr(supplyDateFrom, supplyDateTo, null,  
																				null, null, null, 
																				itemCode, drugKeyFlag, null, null));
			
			constructPivasRptParameters(supplyDateFrom, supplyDateTo, null,  
										null, null, null, 
										itemCode, drugKeyFlag, null, null);
			
			pivasWorkloadSummaryBatchRpt.setPivasWorkloadSummaryBatchRptNormalDtlList(pivasWorkloadSummaryBatchRptNormalDtlList);
			pivasWorkloadSummaryBatchRpt.setPivasWorkloadSummaryBatchRptMergeDtlList(pivasWorkloadSummaryBatchRptMergeDtlList);
			pivasWorkloadSummaryBatchRptList = Arrays.asList(pivasWorkloadSummaryBatchRpt);
			
			retrieveSuccess = true;
		} else {
			retrieveSuccess = false;
		}
		
		return pivasWorkloadSummaryBatchRptList;
	}

	private Map<String, PivasConsumptionRptDtl> buildPivasConsumptionRptNormalDtlMapByPivasBatchPrepList(List<PivasBatchPrep> pivasBatchPrepList, 
																								Map<String, PivasConsumptionRptDtl> pivasConsumptionRptDtlMap, 
																								PivasRptItemCatFilterOption pivasRptItemCatFilterOption) {
		
		for (PivasBatchPrep pivasBatchPrep : pivasBatchPrepList ) {
			Object label = labelJaxbWrapper.unmarshall(pivasBatchPrep.getLabelXml());
			PivasWorksheet pivasWorksheet = ((LabelContainer) label).getPivasWorksheet();
			
			if (pivasRptItemCatFilterOption == PivasRptItemCatFilterOption.All || pivasRptItemCatFilterOption == PivasRptItemCatFilterOption.Drug) {
				String drugItemCode = pivasWorksheet.getDrugItemCode();
				String drugItemDesc = pivasWorksheet.getDrugItemDesc();
				String drugManufCode = pivasWorksheet.getDrugManufCode();
				String drugBatchNum = pivasWorksheet.getDrugBatchNum();
				Date drugExpiryDate = pivasWorksheet.getDrugExpiryDate();
				String drugModu = pivasWorksheet.getPivasDosageUnit();
				// dosage is not be saved alone in labelXml
				// dosage is saved in printName in labelXml in addPivasBatchPrep screen if printName is not altered by user manually
				BigDecimal dosageQty = pivasBatchPrep.getPivasBatchPrepItemList().get(0).getDosageQty();
				BigDecimal prepQty = new BigDecimal(pivasWorksheet.getPrepQty());
				BigDecimal newDosage = dosageQty.multiply(prepQty);
				
				pivasConsumptionRptDtlMap = constructPivasConsumptionRptDtllMap(pivasConsumptionRptDtlMap, 
						PivasItemCat.Drug, drugItemCode, drugItemDesc, drugManufCode, drugBatchNum, drugExpiryDate, drugModu, newDosage);
			}
			
			if (pivasRptItemCatFilterOption == PivasRptItemCatFilterOption.All || pivasRptItemCatFilterOption == PivasRptItemCatFilterOption.SolventAndDiluent) {
				// 	PivasConsumptionRpt does not show vendor solvent
				if (StringUtils.isNoneBlank(pivasWorksheet.getSolventItemCode())) {
					String solventItemCode = pivasWorksheet.getSolventItemCode();
					String solventItemDesc = pivasWorksheet.getSolventItemDesc();
					String solventManufCode = pivasWorksheet.getSolventManufCode();
					String solventBatchNum = pivasWorksheet.getSolventBatchNum();
					Date solventExpiryDate = pivasWorksheet.getSolventExpiryDate();
					String solventModu = "ML";
					
					pivasConsumptionRptDtlMap = constructPivasConsumptionRptDtllMap(pivasConsumptionRptDtlMap, 
							PivasItemCat.Solvent, solventItemCode, solventItemDesc, solventManufCode, solventBatchNum, solventExpiryDate, solventModu, null);
				}
				
				if (StringUtils.isNoneBlank(pivasWorksheet.getDiluentItemCode())) {
					String diluentItemCode = pivasWorksheet.getDiluentItemCode();
					String diluentItemDesc = pivasWorksheet.getDiluentItemDesc();
					String diluentManufCode = pivasWorksheet.getDiluentManufCode();
					String diluentBatchNum = pivasWorksheet.getDiluentBatchNum();
					Date diluentExpiryDate = pivasWorksheet.getDiluentExpiryDate();
					String diluentModu = "ML";
					
					pivasConsumptionRptDtlMap = constructPivasConsumptionRptDtllMap(pivasConsumptionRptDtlMap, 
							PivasItemCat.Diluent, diluentItemCode, diluentItemDesc, diluentManufCode, diluentBatchNum, diluentExpiryDate, diluentModu, null);
				}
			}
		}

		return pivasConsumptionRptDtlMap;
	}
	
	
	private Map<String, PivasConsumptionRptDtl> buildPivasConsumptionRptMergeDtlMapByPivasBatchPrepList(List<PivasBatchPrep> pivasBatchPrepList, 
																					Map<String, PivasConsumptionRptDtl> pivasConsumptionRptDtlMap, 
																					PivasRptItemCatFilterOption pivasRptItemCatFilterOption) {

		for (PivasBatchPrep pivasBatchPrep : pivasBatchPrepList ) {
			Object label = labelJaxbWrapper.unmarshall(pivasBatchPrep.getLabelXml());
			PivasMergeFormulaWorksheet pivasMergeFormulaWorksheet = ((LabelContainer) label).getPivasMergeFormulaWorksheet();

			
			if (pivasRptItemCatFilterOption == PivasRptItemCatFilterOption.All || pivasRptItemCatFilterOption == PivasRptItemCatFilterOption.Drug) {
				for (PivasMergeFormulaWorksheetRawMaterialDtl pivasMergeFormulaWorksheetRawMaterialDtl : pivasMergeFormulaWorksheet.getPivasMergeFormulaWorksheetRawMaterialDtlList()){	
					
					if ( StringUtils.isBlank(pivasMergeFormulaWorksheetRawMaterialDtl.getSolventDesc()) && StringUtils.isBlank(pivasMergeFormulaWorksheetRawMaterialDtl.getDiluentDesc()) ) {
						String drugItemCode = pivasMergeFormulaWorksheetRawMaterialDtl.getItemCode();
						String drugItemDesc = pivasMergeFormulaWorksheetRawMaterialDtl.getItemDesc();
						String drugManufCode = pivasMergeFormulaWorksheetRawMaterialDtl.getManufCode();
						String drugBatchNum = pivasMergeFormulaWorksheetRawMaterialDtl.getBatchNum();
						Date drugExpiryDate = pivasMergeFormulaWorksheetRawMaterialDtl.getExpiryDate();
						
						for (PivasMergeFormulaWorksheetDrugInstructionDtl pivasMergeFormulaWorksheetDrugInstructionDtl : pivasMergeFormulaWorksheet.getPivasMergeFormulaWorksheetDrugInstructionDtlList()){
							if (pivasMergeFormulaWorksheetRawMaterialDtl.getSortSeq().equals(pivasMergeFormulaWorksheetDrugInstructionDtl.getSortSeq())){
								String drugModu = pivasMergeFormulaWorksheetDrugInstructionDtl.getPivasDosageUnit();
				
								BigDecimal dosageQty = new BigDecimal(pivasMergeFormulaWorksheetDrugInstructionDtl.getDosage());
								BigDecimal prepQty = new BigDecimal(pivasMergeFormulaWorksheet.getPrepQty());				
								BigDecimal newDosage = dosageQty.multiply(prepQty);
								pivasConsumptionRptDtlMap = constructPivasConsumptionRptDtllMap(pivasConsumptionRptDtlMap, 
									PivasItemCat.Drug, drugItemCode, drugItemDesc, drugManufCode, drugBatchNum, drugExpiryDate, drugModu, newDosage);
							
							}
						}
					}
				}
			}

			if (pivasRptItemCatFilterOption == PivasRptItemCatFilterOption.All || pivasRptItemCatFilterOption == PivasRptItemCatFilterOption.SolventAndDiluent) {
				
				for (PivasMergeFormulaWorksheetRawMaterialDtl pivasMergeFormulaWorksheetRawMaterialDtl : pivasMergeFormulaWorksheet.getPivasMergeFormulaWorksheetRawMaterialDtlList()){	
				 	//PivasConsumptionRpt does not show vendor solvent
					if (StringUtils.isNoneBlank(pivasMergeFormulaWorksheetRawMaterialDtl.getItemCode()) && StringUtils.isNoneBlank(pivasMergeFormulaWorksheetRawMaterialDtl.getSolventDesc())
						&& Boolean.FALSE.equals(pivasMergeFormulaWorksheetRawMaterialDtl.getVendSupplySolvFlag())) {
							String solventItemCode = pivasMergeFormulaWorksheetRawMaterialDtl.getItemCode();
							String solventItemDesc = pivasMergeFormulaWorksheetRawMaterialDtl.getItemDesc();
							String solventManufCode = pivasMergeFormulaWorksheetRawMaterialDtl.getManufCode();
							String solventBatchNum = pivasMergeFormulaWorksheetRawMaterialDtl.getBatchNum();
							Date solventExpiryDate = pivasMergeFormulaWorksheetRawMaterialDtl.getExpiryDate();
							String solventModu = "ML";

							pivasConsumptionRptDtlMap = constructPivasConsumptionRptDtllMap(pivasConsumptionRptDtlMap, 
									PivasItemCat.Solvent, solventItemCode, solventItemDesc, solventManufCode, solventBatchNum, solventExpiryDate, solventModu, null);
						

					}
					else if (StringUtils.isNoneBlank(pivasMergeFormulaWorksheetRawMaterialDtl.getDiluentDesc()) && StringUtils.isNoneBlank(pivasMergeFormulaWorksheet.getDiluentItemCode())) {
							String diluentItemCode = pivasMergeFormulaWorksheetRawMaterialDtl.getItemCode();
							String diluentItemDesc = pivasMergeFormulaWorksheetRawMaterialDtl.getItemDesc();
							String diluentManufCode = pivasMergeFormulaWorksheetRawMaterialDtl.getManufCode();
							String diluentBatchNum = pivasMergeFormulaWorksheetRawMaterialDtl.getBatchNum();
							Date diluentExpiryDate = pivasMergeFormulaWorksheetRawMaterialDtl.getExpiryDate();
							String diluentModu = "ML";

							pivasConsumptionRptDtlMap = constructPivasConsumptionRptDtllMap(pivasConsumptionRptDtlMap, 
									PivasItemCat.Diluent, diluentItemCode, diluentItemDesc, diluentManufCode, diluentBatchNum, diluentExpiryDate, diluentModu, null);
						
					}
				}
			}
		}

		return pivasConsumptionRptDtlMap;
	}

	
	private Map<String, PivasConsumptionRptDtl> buildPivasConsumptionRptDtlMapByDeliveryItemList(List<DeliveryItem> deliveryItemList, Map<String, 
																								PivasConsumptionRptDtl> pivasConsumptionRptDtlMap, 
																								PivasRptItemCatFilterOption pivasRptItemCatFilterOption) {
		
		for (DeliveryItem deliveryItem : deliveryItemList ) {
			Object label = labelJaxbWrapper.unmarshall(deliveryItem.getLabelXml());
			PivasWorksheet pivasWorksheet = ((LabelContainer) label).getPivasWorksheet();
			
			if (pivasRptItemCatFilterOption == PivasRptItemCatFilterOption.All || pivasRptItemCatFilterOption == PivasRptItemCatFilterOption.Drug) {
				String drugItemCode = pivasWorksheet.getDrugItemCode();
				String drugItemDesc = pivasWorksheet.getDrugItemDesc();
				String drugManufCode = pivasWorksheet.getDrugManufCode();
				String drugBatchNum = pivasWorksheet.getDrugBatchNum();
				Date drugExpiryDate = pivasWorksheet.getDrugExpiryDate();
				String drugModu = pivasWorksheet.getPivasDosageUnit();
				BigDecimal orderDosage = new BigDecimal(pivasWorksheet.getOrderDosage());
				BigDecimal issueQty = new BigDecimal(pivasWorksheet.getIssueQty());
				BigDecimal adjQty = new BigDecimal(pivasWorksheet.getAdjQty());
				BigDecimal newDosage = orderDosage.multiply(issueQty.add(adjQty));
				
				pivasConsumptionRptDtlMap = constructPivasConsumptionRptDtllMap(pivasConsumptionRptDtlMap, 
						PivasItemCat.Drug, drugItemCode, drugItemDesc, drugManufCode, drugBatchNum, drugExpiryDate, drugModu, newDosage);
			}

			if (pivasRptItemCatFilterOption == PivasRptItemCatFilterOption.All || pivasRptItemCatFilterOption == PivasRptItemCatFilterOption.SolventAndDiluent) {
				// PivasConsumptionRpt does not show vendor solvent
				if (StringUtils.isNotBlank(pivasWorksheet.getSolventItemCode())) {
					String solventItemCode = pivasWorksheet.getSolventItemCode();
					String solventItemDesc = pivasWorksheet.getSolventItemDesc();
					String solventManufCode = pivasWorksheet.getSolventManufCode();
					String solventBatchNum = pivasWorksheet.getSolventBatchNum();
					Date solventExpiryDate = pivasWorksheet.getSolventExpiryDate();
					String solventModu = "ML";
					
					pivasConsumptionRptDtlMap = constructPivasConsumptionRptDtllMap(pivasConsumptionRptDtlMap, 
							PivasItemCat.Solvent, solventItemCode, solventItemDesc, solventManufCode, solventBatchNum, solventExpiryDate, solventModu, null);
				}
			
				if (StringUtils.isNotBlank(pivasWorksheet.getDiluentItemCode())) {
					String diluentItemCode = pivasWorksheet.getDiluentItemCode();
					String diluentItemDesc = pivasWorksheet.getDiluentItemDesc();
					String diluentManufCode = pivasWorksheet.getDiluentManufCode();
					String diluentBatchNum = pivasWorksheet.getDiluentBatchNum();
					Date diluentExpiryDate = pivasWorksheet.getDiluentExpiryDate();
					String diluentModu = "ML";
					
					pivasConsumptionRptDtlMap = constructPivasConsumptionRptDtllMap(pivasConsumptionRptDtlMap, 
							PivasItemCat.Diluent, diluentItemCode, diluentItemDesc, diluentManufCode, diluentBatchNum, diluentExpiryDate, diluentModu, null);
				}
			}
		}
		
		return pivasConsumptionRptDtlMap;
	}
	
	private Map<String, PivasConsumptionRptDtl> constructPivasConsumptionRptDtllMap(Map<String, PivasConsumptionRptDtl> pivasConsumptionRptDtlMap, 
																					PivasItemCat pivasItemCat, String itemCode, String itemDesc, String manufCode, 
																					String batchNum, Date expiryDate, String modu, BigDecimal newDosage) {
		
		List<String> keyList = new ArrayList<String>();
		keyList.add(String.valueOf(pivasItemCat.getDataValue()));
		keyList.add(String.valueOf(itemCode));
		keyList.add(String.valueOf(manufCode));
		keyList.add(String.valueOf(batchNum));
		keyList.add(String.valueOf(expiryDate));
		keyList.add(String.valueOf(modu));
		String key = StringUtils.join(keyList.toArray(), "|");
		
		// 	check duplicated map key
		if (!pivasConsumptionRptDtlMap.containsKey(key)) {
			PivasConsumptionRptDtl pivasConsumptionRptDtl = new PivasConsumptionRptDtl();
			pivasConsumptionRptDtl.setPivasItemCat(pivasItemCat);
			pivasConsumptionRptDtl.setItemCode(itemCode);
			pivasConsumptionRptDtl.setManufCode(manufCode);
			pivasConsumptionRptDtl.setBatchNum(batchNum);
			pivasConsumptionRptDtl.setExpiryDate(expiryDate);
			pivasConsumptionRptDtl.setModu(modu);
			// itemDesc is saved in labelXml and independent of dmDrug
			// first itemDesc for an itemCode is shown in report
			pivasConsumptionRptDtl.setItemDesc(itemDesc);
			
			// 	totalDosage, estimatedQty and roundQty calculation are for drug only
			if (newDosage != null && pivasItemCat == PivasItemCat.Drug)  {
				pivasConsumptionRptDtl.setDosage(newDosage);
			} else {
				pivasConsumptionRptDtl.setDosage(null);
			}
			
			pivasConsumptionRptDtlMap.put(key, pivasConsumptionRptDtl);
		} else {
			// 	This key item is processed before, sum up dosage if this is drug only
			PivasConsumptionRptDtl pivasConsumptionRptDtl = pivasConsumptionRptDtlMap.get(key);
			
			if (pivasConsumptionRptDtl.getDosage() != null && pivasConsumptionRptDtl.getPivasItemCat() == PivasItemCat.Drug) {
				BigDecimal newTotalDosage = pivasConsumptionRptDtl.getDosage().add(newDosage);
				pivasConsumptionRptDtl.setDosage(newTotalDosage);
				pivasConsumptionRptDtlMap.put(key, pivasConsumptionRptDtl);
			}
		}
		
		return pivasConsumptionRptDtlMap;
	}

	
	private List<PivasConsumptionRptDtl> calPivasConsumptionRptDtlQty(List<PivasConsumptionRptDtl> pivasConsumptionRptDtlList) {

		for (PivasConsumptionRptDtl pivasConsumptionRptDtl : pivasConsumptionRptDtlList ) {

			//	totalDosage, estimatedQty and roundQty calculation are for drug only
			if (pivasConsumptionRptDtl.getPivasItemCat() == PivasItemCat.Drug) {
				DmDrug dmDrug = dmDrugCacher.getDmDrug(pivasConsumptionRptDtl.getItemCode());
				
				//	dmDrug may not be found by itemCode if the item is deleted in DMS
				if (dmDrug != null) {
					DmMoeProperty dmMoeProperty = dmDrug.getDmMoeProperty();
					
					if (pivasConsumptionRptDtl.getDosage() != null && dmMoeProperty != null && dmMoeProperty.getDduToMduRatio() != null && dmMoeProperty.getBaseunitToDduRatio() != null) {
						BigDecimal dduToMduRatio = BigDecimal.valueOf(dmMoeProperty.getDduToMduRatio());
						BigDecimal baseunitToDduRatio = BigDecimal.valueOf(dmMoeProperty.getBaseunitToDduRatio());
						BigDecimal totalDosage = pivasConsumptionRptDtl.getDosage().setScale(4, RoundingMode.HALF_UP);
						// rawEstimatedQty is for correct rounding in roundQty
						BigDecimal rawEstimatedQty = pivasConsumptionRptDtl.getDosage().divide( dduToMduRatio.multiply(baseunitToDduRatio), 10, RoundingMode.HALF_UP );
						BigDecimal displayEstimatedQty = rawEstimatedQty.setScale(2, RoundingMode.HALF_UP);
						Integer roundQty = null;
						
						if (rawEstimatedQty.compareTo(BigDecimal.ZERO) == 1 && rawEstimatedQty.compareTo(BigDecimal.ONE) == -1) {
							roundQty = new Integer(1);
						} else {
							roundQty = rawEstimatedQty.setScale(0, RoundingMode.UP).intValue();
						}
						
						if (dmDrug.getBaseUnit() != null) {
							pivasConsumptionRptDtl.setBaseUnit(dmDrug.getBaseUnit());
							pivasConsumptionRptDtl.setTotalDosage(convertToString(totalDosage));
							pivasConsumptionRptDtl.setEstimatedQty(convertToString(displayEstimatedQty));
							pivasConsumptionRptDtl.setRoundQty(convertToString(roundQty));
						}
					}	
				}
			}
		}
		
		return pivasConsumptionRptDtlList;
	}
	
	private List<PivasPatientDispRptDtl> buildPivasPatientDispRptDtlList(List<DeliveryItem> deliveryItemList) {

		List<PivasPatientDispRptDtl> pivasPatientDispRptDtlList = new ArrayList<PivasPatientDispRptDtl>();
		
		for (DeliveryItem deliveryItem : deliveryItemList) {
			
			Object label = labelJaxbWrapper.unmarshall(deliveryItem.getLabelXml());
			PivasProductLabel pivasProductLabel = ((LabelContainer) label).getPivasProductLabel();
			PivasWorksheet pivasWorksheet = ((LabelContainer) label).getPivasWorksheet();
			
			PivasPatientDispRptDtl pivasPatientDispRptDtl = new PivasPatientDispRptDtl();
			
			// pivasWorklist supply date is saved as manufactureDate in labelXml
			pivasPatientDispRptDtl.setSupplyDate(pivasProductLabel.getManufactureDate());
			
			if (pivasWorksheet.getAdhocFlag()) {
				pivasPatientDispRptDtl.setItemType("");
			} else if (pivasWorksheet.getRefillFlag()) {
				pivasPatientDispRptDtl.setItemType(REFILL);
			} else {
				pivasPatientDispRptDtl.setItemType(NEW);
			}

			pivasPatientDispRptDtl.setPatName(pivasProductLabel.getPatName());
			pivasPatientDispRptDtl.setPatNameChi(pivasProductLabel.getPatNameChi());
			pivasPatientDispRptDtl.setCaseNum(pivasProductLabel.getCaseNum());
			pivasPatientDispRptDtl.setSpecCode(pivasProductLabel.getSpecCode());
			pivasPatientDispRptDtl.setWard(pivasProductLabel.getWard());
			pivasPatientDispRptDtl.setBedNum(pivasProductLabel.getBedNum());
			
			pivasPatientDispRptDtl.setSex(pivasProductLabel.getSex());
			pivasPatientDispRptDtl.setAge(pivasProductLabel.getAge());
    		pivasPatientDispRptDtl.setBodyWeight(pivasProductLabel.getBodyWeight());
			
			pivasPatientDispRptDtl.setPivasDueDate(pivasProductLabel.getFirstDoseDateTime());
			pivasPatientDispRptDtl.setPrepDesc(buildPivasPatientDispRptDtlPrepDesc(pivasProductLabel));

			pivasPatientDispRptDtl.setDrugItemCode(pivasWorksheet.getDrugItemCode());
			pivasPatientDispRptDtl.setDrugManufCode(pivasWorksheet.getDrugManufCode());
			pivasPatientDispRptDtl.setDrugBatchNum(pivasWorksheet.getDrugBatchNum());
			pivasPatientDispRptDtl.setDrugExpiryDate(pivasWorksheet.getDrugExpiryDate());
			// for export only
			pivasPatientDispRptDtl.setDrugItemDesc(pivasWorksheet.getDrugItemDesc());
			
			// solventCode is not saved in pivasWorksheet, therefore checking vendSupplySolvFlag to identify vendor solvent case
			if (StringUtils.isNoneBlank(pivasWorksheet.getSolventItemCode())) {
				// not vendor supply solvent
				pivasPatientDispRptDtl.setVendSupplySolvFlag(false);
				pivasPatientDispRptDtl.setVendSupplySolvDesc(null);
				
				pivasPatientDispRptDtl.setSolventItemCode(pivasWorksheet.getSolventItemCode()); 
				pivasPatientDispRptDtl.setSolventManufCode(pivasWorksheet.getSolventManufCode());
				pivasPatientDispRptDtl.setSolventBatchNum(pivasWorksheet.getSolventBatchNum());
				pivasPatientDispRptDtl.setSolventExpiryDate(pivasWorksheet.getSolventExpiryDate());
				// for export only
				pivasPatientDispRptDtl.setSolventItemDesc(pivasWorksheet.getSolventItemDesc());
			} else if (pivasWorksheet.isVendSupplySolvFlag()) {
				// vendor supply solvent, no solventItemCode and solventManufCode, solventBatchNum and solventExpiryDate are optional, but are in pairs
				pivasPatientDispRptDtl.setVendSupplySolvFlag(true);
				pivasPatientDispRptDtl.setVendSupplySolvDesc(pivasWorksheet.getSolventDesc());
				
				pivasPatientDispRptDtl.setSolventItemCode(null);
				pivasPatientDispRptDtl.setSolventManufCode(null);
				
				// guard possible empty solventBatchNum from labelXml
				if (StringUtils.isNotBlank(pivasWorksheet.getSolventBatchNum())) {
					pivasPatientDispRptDtl.setSolventBatchNum(pivasWorksheet.getSolventBatchNum());
					pivasPatientDispRptDtl.setSolventExpiryDate(pivasWorksheet.getSolventExpiryDate());
				}
				// no itemDesc for vendor supply solvent
				pivasPatientDispRptDtl.setSolventItemDesc(null);
			}
						
			if ( StringUtils.isNoneBlank(pivasWorksheet.getDiluentItemCode()) ) {
				pivasPatientDispRptDtl.setDiluentItemCode(pivasWorksheet.getDiluentItemCode());
				pivasPatientDispRptDtl.setDiluentManufCode(pivasWorksheet.getDiluentManufCode());
				pivasPatientDispRptDtl.setDiluentBatchNum(pivasWorksheet.getDiluentBatchNum());
				pivasPatientDispRptDtl.setDiluentExpiryDate(pivasWorksheet.getDiluentExpiryDate());
				// for export only
				pivasPatientDispRptDtl.setDiluentItemDesc(pivasWorksheet.getDiluentItemDesc());
			}
			
			pivasPatientDispRptDtl.setSupplyDesc(buildPivasPatientDispRptDtlSupplyDesc(pivasProductLabel, pivasWorksheet));		
			
			// for export only
			pivasPatientDispRptDtl.setDob(pivasProductLabel.getDob());
			pivasPatientDispRptDtl.setPivasDesc(pivasProductLabel.getPivasDesc());
			pivasPatientDispRptDtl.setSiteDesc(pivasProductLabel.getSiteDesc());
			
			pivasPatientDispRptDtl.setFreq(StringUtils.trim(pivasProductLabel.getFreq()));
			if (StringUtils.isNotBlank(pivasProductLabel.getSupplFreq())) {
				pivasPatientDispRptDtl.setSupplFreq(StringUtils.trim(pivasProductLabel.getSupplFreq().toLowerCase()));
			}
			
			pivasPatientDispRptDtl.setOrderDosage(pivasProductLabel.getOrderDosage());
			pivasPatientDispRptDtl.setModu(pivasProductLabel.getPivasDosageUnit());
			pivasPatientDispRptDtl.setConcn(pivasProductLabel.getConcn());
			
			pivasPatientDispRptDtl.setDrugVolume(pivasWorksheet.getDrugVolume());
			pivasPatientDispRptDtl.setFinalVolume(pivasWorksheet.getFinalVolume());
			
			pivasPatientDispRptDtl.setAdjQty(pivasWorksheet.getAdjQty());
			pivasPatientDispRptDtl.setIssueQty(pivasWorksheet.getIssueQty());
			
			pivasPatientDispRptDtl.setContainerName(pivasWorksheet.getContainerName());
			
			pivasPatientDispRptDtl.setSplitCount(pivasProductLabel.getSplitCount());
			pivasPatientDispRptDtl.setFormulaPrintName(pivasProductLabel.getFormulaPrintName());
			pivasPatientDispRptDtl.setPrepExpiryDate(pivasProductLabel.getPrepExpiryDate());
			pivasPatientDispRptDtl.setDiluentDesc(pivasProductLabel.getDiluentDesc());
			
			// PivasWorklist record obtained must be in completed status
			pivasPatientDispRptDtl.setPivasWorklistStatus(PivasWorklistStatus.Completed);
			
			pivasPatientDispRptDtl.setDrugCalQty(pivasWorksheet.getDrugCalQty());
			pivasPatientDispRptDtl.setDrugDispQty(pivasWorksheet.getDrugDispQty());
			pivasPatientDispRptDtl.setDrugBaseUnit(pivasWorksheet.getDrugBaseUnit());
			
			pivasPatientDispRptDtl.setPrintDate(deliveryItem.getCreateDate());
			
			pivasPatientDispRptDtlList.add(pivasPatientDispRptDtl);
		}

		return pivasPatientDispRptDtlList;
	}
	
	private void buildPivasBatchDispRptNormalDtlList(List<PivasBatchPrep> pivasBatchPrepList) {
		
		for (PivasBatchPrep pivasBatchPrep : pivasBatchPrepList) {
			
			PivasBatchDispRptDtl pivasBatchDispRptDtl = new PivasBatchDispRptDtl();
			List<PivasBatchDispRptRawMaterialDtl> pivasBatchDispRptRawMaterialDtlList = new ArrayList<PivasBatchDispRptRawMaterialDtl>();
			PivasBatchDispRptDtl pivasBatchDispRptXlsDtl = new PivasBatchDispRptDtl();
			
			Object label = labelJaxbWrapper.unmarshall(pivasBatchPrep.getLabelXml());
			PivasProductLabel pivasProductLabel = ((LabelContainer) label).getPivasProductLabel();
			PivasWorksheet pivasWorksheet = ((LabelContainer) label).getPivasWorksheet();
					
			PivasBatchDispRptRawMaterialDtl pivasBatchDispRptRawMaterialDrugDtl = new PivasBatchDispRptRawMaterialDtl();
			
			pivasBatchDispRptRawMaterialDrugDtl.setItemCode(pivasWorksheet.getDrugItemCode());
			pivasBatchDispRptRawMaterialDrugDtl.setManufCode(pivasWorksheet.getDrugManufCode());
			pivasBatchDispRptRawMaterialDrugDtl.setBatchNum(pivasWorksheet.getDrugBatchNum());
			pivasBatchDispRptRawMaterialDrugDtl.setExpiryDate(pivasWorksheet.getDrugExpiryDate());
			pivasBatchDispRptRawMaterialDrugDtl.setSolventDesc(null);
			pivasBatchDispRptRawMaterialDrugDtl.setDiluentDesc(null);
			pivasBatchDispRptRawMaterialDrugDtl.setSortSeq(Integer.valueOf(0));
				
			pivasBatchDispRptRawMaterialDtlList.add(pivasBatchDispRptRawMaterialDrugDtl);
				
			pivasBatchDispRptXlsDtl.setDrugItemCode(pivasWorksheet.getDrugItemCode());
			pivasBatchDispRptXlsDtl.setDrugItemDesc(pivasWorksheet.getDrugItemDesc());
			pivasBatchDispRptXlsDtl.setDrugManufCode(pivasWorksheet.getDrugManufCode());
			pivasBatchDispRptXlsDtl.setDrugBatchNum(pivasWorksheet.getDrugBatchNum());
			pivasBatchDispRptXlsDtl.setDrugExpiryDate(pivasWorksheet.getDrugExpiryDate());
			
			if (StringUtils.isNoneBlank(pivasWorksheet.getSolventItemCode())) {

				PivasBatchDispRptRawMaterialDtl pivasBatchDispRptRawMaterialNonVendorSolventDtl = new PivasBatchDispRptRawMaterialDtl();
				
				pivasBatchDispRptRawMaterialNonVendorSolventDtl.setVendSupplySolvFlag(false);
								
				pivasBatchDispRptRawMaterialNonVendorSolventDtl.setItemCode(pivasWorksheet.getSolventItemCode()); 
				pivasBatchDispRptRawMaterialNonVendorSolventDtl.setManufCode(pivasWorksheet.getSolventManufCode());
				pivasBatchDispRptRawMaterialNonVendorSolventDtl.setBatchNum(pivasWorksheet.getSolventBatchNum());
				pivasBatchDispRptRawMaterialNonVendorSolventDtl.setExpiryDate(pivasWorksheet.getSolventExpiryDate());

				pivasBatchDispRptRawMaterialNonVendorSolventDtl.setSolventDesc(pivasWorksheet.getSolventDesc());
				pivasBatchDispRptRawMaterialNonVendorSolventDtl.setDiluentDesc(null);
				pivasBatchDispRptRawMaterialNonVendorSolventDtl.setSortSeq(Integer.valueOf(1));
				
				pivasBatchDispRptRawMaterialDtlList.add(pivasBatchDispRptRawMaterialNonVendorSolventDtl);
				
				pivasBatchDispRptXlsDtl.setVendSupplySolvFlag(false);
				pivasBatchDispRptXlsDtl.setSolventItemCode(pivasWorksheet.getSolventItemCode()); 		
				pivasBatchDispRptXlsDtl.setSolventItemDesc(pivasWorksheet.getSolventItemDesc());				
				pivasBatchDispRptXlsDtl.setVendSupplySolvDesc(null);
				pivasBatchDispRptXlsDtl.setSolventManufCode(pivasWorksheet.getSolventManufCode());
				pivasBatchDispRptXlsDtl.setSolventBatchNum(pivasWorksheet.getSolventBatchNum());
				pivasBatchDispRptXlsDtl.setSolventExpiryDate(pivasWorksheet.getSolventExpiryDate());				
			
			} else if (pivasWorksheet.isVendSupplySolvFlag()) {

				PivasBatchDispRptRawMaterialDtl pivasBatchDispRptRawMaterialVendorSolventDtl = new PivasBatchDispRptRawMaterialDtl();
				
				pivasBatchDispRptRawMaterialVendorSolventDtl.setVendSupplySolvFlag(true);
				pivasBatchDispRptRawMaterialVendorSolventDtl.setSolventDesc(pivasWorksheet.getSolventDesc());
				pivasBatchDispRptRawMaterialVendorSolventDtl.setDiluentDesc(null);
				pivasBatchDispRptRawMaterialVendorSolventDtl.setItemCode(null); 
				pivasBatchDispRptRawMaterialVendorSolventDtl.setManufCode(null);
				pivasBatchDispRptRawMaterialVendorSolventDtl.setSortSeq(Integer.valueOf(1));
				
				pivasBatchDispRptXlsDtl.setVendSupplySolvFlag(true);
				pivasBatchDispRptXlsDtl.setSolventItemCode(null); 

				pivasBatchDispRptXlsDtl.setSolventItemDesc(null);
				pivasBatchDispRptXlsDtl.setVendSupplySolvDesc(pivasWorksheet.getSolventDesc());
				pivasBatchDispRptXlsDtl.setSolventManufCode(null);
				
				if (StringUtils.isNotBlank(pivasWorksheet.getSolventBatchNum())) {
					pivasBatchDispRptRawMaterialVendorSolventDtl.setBatchNum(pivasWorksheet.getSolventBatchNum());
					pivasBatchDispRptRawMaterialVendorSolventDtl.setExpiryDate(pivasWorksheet.getSolventExpiryDate());
										
					pivasBatchDispRptXlsDtl.setSolventBatchNum(pivasWorksheet.getSolventBatchNum());
					pivasBatchDispRptXlsDtl.setSolventExpiryDate(pivasWorksheet.getSolventExpiryDate());
					
				}
				
				pivasBatchDispRptRawMaterialDtlList.add(pivasBatchDispRptRawMaterialVendorSolventDtl);

			}
						
			if ( StringUtils.isNoneBlank(pivasWorksheet.getDiluentItemCode()) ) {
				PivasBatchDispRptRawMaterialDtl pivasBatchDispRptRawMaterialDiluentDtl = new PivasBatchDispRptRawMaterialDtl();
			
				pivasBatchDispRptRawMaterialDiluentDtl.setItemCode(pivasWorksheet.getDiluentItemCode());
				pivasBatchDispRptRawMaterialDiluentDtl.setManufCode(pivasWorksheet.getDiluentManufCode());
				pivasBatchDispRptRawMaterialDiluentDtl.setBatchNum(pivasWorksheet.getDiluentBatchNum());
				pivasBatchDispRptRawMaterialDiluentDtl.setExpiryDate(pivasWorksheet.getDiluentExpiryDate());
				
				pivasBatchDispRptRawMaterialDiluentDtl.setSolventDesc(null);
				pivasBatchDispRptRawMaterialDiluentDtl.setDiluentDesc(pivasWorksheet.getDiluentDesc());
				pivasBatchDispRptRawMaterialDiluentDtl.setSortSeq(Integer.valueOf(2));
				
				pivasBatchDispRptRawMaterialDtlList.add(pivasBatchDispRptRawMaterialDiluentDtl);
				
				pivasBatchDispRptXlsDtl.setDiluentItemCode(pivasWorksheet.getDiluentItemCode());
				pivasBatchDispRptXlsDtl.setDiluentItemDesc(pivasWorksheet.getDiluentItemDesc());
				pivasBatchDispRptXlsDtl.setDiluentManufCode(pivasWorksheet.getDiluentManufCode());
				pivasBatchDispRptXlsDtl.setDiluentBatchNum(pivasWorksheet.getDiluentBatchNum());
				pivasBatchDispRptXlsDtl.setDiluentExpiryDate(pivasWorksheet.getDiluentExpiryDate());
				pivasBatchDispRptXlsDtl.setDiluentDesc(pivasBatchPrep.getDiluentDesc());

			}
			
			pivasBatchDispRptDtl.setPivasFormulaType(PivasFormulaType.Normal);
			pivasBatchDispRptDtl.setSupplyDate(pivasBatchPrep.getManufactureDate());
			pivasBatchDispRptDtl.setPrepExpiryDate(pivasProductLabel.getPrepExpiryDate());
			pivasBatchDispRptDtl.setPrintDate(pivasBatchPrep.getCreateDate());
			pivasBatchDispRptDtl.setLotNum(pivasProductLabel.getLotNum());
			pivasBatchDispRptDtl.setPrepDesc(buildPivasBatchDispRptNormalDtlPrepDesc(pivasProductLabel, pivasWorksheet));
			pivasBatchDispRptDtl.setLabelDesc(null);
			pivasBatchDispRptDtl.setFormulaPrintName(pivasWorksheet.getFormulaPrintName());			
			
			pivasBatchDispRptDtl.setSupplyDesc(pivasWorksheet.getFinalVolume() + UNIT_ML + NEW_LINE + pivasWorksheet.getPrepQty() + SPACE + pivasWorksheet.getContainerName());
					
			pivasBatchDispRptDtl.setPivasBatchDispRptRawMaterialDtlList(pivasBatchDispRptRawMaterialDtlList);
			pivasBatchDispRptDtlList.add(pivasBatchDispRptDtl);
			
			pivasBatchDispRptXlsDtl.setSupplyDate(pivasBatchPrep.getManufactureDate());
			pivasBatchDispRptXlsDtl.setLotNum(pivasProductLabel.getLotNum());
			pivasBatchDispRptXlsDtl.setPivasFormulaType(PivasFormulaType.Normal);
			
			pivasBatchDispRptXlsDtl.setPrepDesc(null);
			pivasBatchDispRptXlsDtl.setLabelDesc(pivasProductLabel.getPrintName() + SPACE + pivasProductLabel.getSiteDesc());
			pivasBatchDispRptXlsDtl.setSiteDesc(pivasProductLabel.getSiteDesc());
			
			pivasBatchDispRptXlsDtl.setDosageQty(convertToString(pivasBatchPrep.getPivasBatchPrepItemList().get(0).getDosageQty()));	
			pivasBatchDispRptXlsDtl.setModu(pivasProductLabel.getPivasDosageUnit());	
			pivasBatchDispRptXlsDtl.setConcn(pivasWorksheet.getConcn());	
			pivasBatchDispRptXlsDtl.setDrugVolume(pivasWorksheet.getDrugVolume());
					
			pivasBatchDispRptXlsDtl.setFinalVolume(pivasWorksheet.getFinalVolume());
			pivasBatchDispRptXlsDtl.setPrepQty(pivasWorksheet.getPrepQty());
			pivasBatchDispRptXlsDtl.setContainerName(pivasWorksheet.getContainerName());
			pivasBatchDispRptXlsDtl.setPrepExpiryDate(pivasProductLabel.getPrepExpiryDate());
			pivasBatchDispRptXlsDtl.setFormulaPrintName(pivasWorksheet.getFormulaPrintName());			
			pivasBatchDispRptXlsDtl.setNumOfLabel(convertToString(pivasProductLabel.getNumOfLabelSet()));		

			pivasBatchDispRptXlsDtl.setPrintDate(pivasBatchPrep.getCreateDate());
			pivasBatchDispRptXlsDtl.setPivasBatchPrepStatus(pivasBatchPrep.getStatus());
		
			pivasBatchDispRptXlsDtlList.add(pivasBatchDispRptXlsDtl);			
					
		}		
	}

	private void buildPivasBatchDispRptMergeDtlList(List<PivasBatchPrep> pivasBatchPrepList) {
		
		for (PivasBatchPrep pivasBatchPrep : pivasBatchPrepList) {
			
			PivasBatchDispRptDtl pivasBatchDispRptDtl = new PivasBatchDispRptDtl();
			List<PivasBatchDispRptRawMaterialDtl> pivasBatchDispRptRawMaterialDtlList = new ArrayList<PivasBatchDispRptRawMaterialDtl>();

			Map<Integer,PivasBatchDispRptDtl> sortSeqPivasBatchDispRptMergeXlsDtlMap = new HashMap<Integer,PivasBatchDispRptDtl>();
			
			Object label = labelJaxbWrapper.unmarshall(pivasBatchPrep.getLabelXml());
			PivasMergeFormulaProductLabel pivasMergeFormulaProductLabel = ((LabelContainer) label).getPivasMergeFormulaProductLabel();
			PivasMergeFormulaWorksheet pivasMergeFormulaWorksheet = ((LabelContainer) label).getPivasMergeFormulaWorksheet();
			
			pivasBatchDispRptDtl.setPivasFormulaType(PivasFormulaType.Merge);
			pivasBatchDispRptDtl.setSupplyDate(pivasBatchPrep.getManufactureDate());
			pivasBatchDispRptDtl.setPrepExpiryDate(pivasMergeFormulaProductLabel.getPrepExpiryDate());
			pivasBatchDispRptDtl.setPrintDate(pivasBatchPrep.getCreateDate());
			pivasBatchDispRptDtl.setLotNum(pivasMergeFormulaProductLabel.getLotNum());
			pivasBatchDispRptDtl.setFormulaPrintName(pivasMergeFormulaWorksheet.getFormulaPrintName());		
			
			pivasBatchDispRptDtl.setPrepDesc(buildPivasBatchDispRptMergeDtlPrepDesc(pivasMergeFormulaProductLabel));
			pivasBatchDispRptDtl.setLabelDesc(null);
			
			pivasBatchDispRptDtl.setSupplyDesc(pivasMergeFormulaWorksheet.getFinalVolume() + UNIT_ML + NEW_LINE + pivasMergeFormulaWorksheet.getPrepQty() + SPACE + pivasMergeFormulaWorksheet.getContainerName());

			for (PivasMergeFormulaWorksheetRawMaterialDtl pivasMergeFormulaWorksheetRawMaterialDtl : pivasMergeFormulaWorksheet.getPivasMergeFormulaWorksheetRawMaterialDtlList()){	
				if ( StringUtils.isBlank(pivasMergeFormulaWorksheetRawMaterialDtl.getSolventDesc()) && StringUtils.isBlank(pivasMergeFormulaWorksheetRawMaterialDtl.getDiluentDesc()) ) {
					PivasBatchDispRptDtl pivasBatchDispRptMergeXlsDtl = new PivasBatchDispRptDtl();
					
					pivasBatchDispRptMergeXlsDtl.setSupplyDate(pivasBatchPrep.getManufactureDate());
					pivasBatchDispRptMergeXlsDtl.setLotNum(pivasMergeFormulaProductLabel.getLotNum());
					pivasBatchDispRptMergeXlsDtl.setPivasFormulaType(PivasFormulaType.Merge);
					pivasBatchDispRptMergeXlsDtl.setPrepDesc(null);
					pivasBatchDispRptMergeXlsDtl.setLabelDesc(pivasMergeFormulaProductLabel.getPrintName() + SPACE + pivasMergeFormulaProductLabel.getSiteDesc());
					pivasBatchDispRptMergeXlsDtl.setSiteDesc(pivasMergeFormulaProductLabel.getSiteDesc());
		
					pivasBatchDispRptMergeXlsDtl.setFinalVolume(pivasMergeFormulaWorksheet.getFinalVolume());
					pivasBatchDispRptMergeXlsDtl.setPrepQty(pivasMergeFormulaWorksheet.getPrepQty());
					pivasBatchDispRptMergeXlsDtl.setContainerName(pivasMergeFormulaWorksheet.getContainerName());
					pivasBatchDispRptMergeXlsDtl.setPrepExpiryDate(pivasMergeFormulaProductLabel.getPrepExpiryDate());
					pivasBatchDispRptMergeXlsDtl.setFormulaPrintName(pivasMergeFormulaWorksheet.getFormulaPrintName());		
					pivasBatchDispRptMergeXlsDtl.setNumOfLabel(convertToString(pivasMergeFormulaProductLabel.getNumOfLabelSet()));
							
					pivasBatchDispRptMergeXlsDtl.setPrintDate(pivasBatchPrep.getCreateDate());
					pivasBatchDispRptMergeXlsDtl.setPivasBatchPrepStatus(pivasBatchPrep.getStatus());
					
					sortSeqPivasBatchDispRptMergeXlsDtlMap.put(pivasMergeFormulaWorksheetRawMaterialDtl.getSortSeq(), pivasBatchDispRptMergeXlsDtl);
				}
			}
			
			for (PivasMergeFormulaWorksheetRawMaterialDtl pivasMergeFormulaWorksheetRawMaterialDtl : pivasMergeFormulaWorksheet.getPivasMergeFormulaWorksheetRawMaterialDtlList()){	
				
				Integer pivasMergeFormulaWorksheetRawMaterialDtlSortSeq = pivasMergeFormulaWorksheetRawMaterialDtl.getSortSeq();
				PivasBatchDispRptDtl pivasBatchDispRptMergeXlsDtl = sortSeqPivasBatchDispRptMergeXlsDtlMap.get(pivasMergeFormulaWorksheetRawMaterialDtlSortSeq);
				
				if ( StringUtils.isBlank(pivasMergeFormulaWorksheetRawMaterialDtl.getSolventDesc()) && StringUtils.isBlank(pivasMergeFormulaWorksheetRawMaterialDtl.getDiluentDesc()) ) {
					PivasBatchDispRptRawMaterialDtl pivasBatchDispRptRawMaterialDrugDtl = new PivasBatchDispRptRawMaterialDtl();
					
					pivasBatchDispRptRawMaterialDrugDtl.setSolventDesc(null);
					pivasBatchDispRptRawMaterialDrugDtl.setDiluentDesc(null);
					pivasBatchDispRptRawMaterialDrugDtl.setItemCode(pivasMergeFormulaWorksheetRawMaterialDtl.getItemCode());
					pivasBatchDispRptRawMaterialDrugDtl.setManufCode(pivasMergeFormulaWorksheetRawMaterialDtl.getManufCode());
					pivasBatchDispRptRawMaterialDrugDtl.setBatchNum(pivasMergeFormulaWorksheetRawMaterialDtl.getBatchNum());
					pivasBatchDispRptRawMaterialDrugDtl.setExpiryDate(pivasMergeFormulaWorksheetRawMaterialDtl.getExpiryDate());
					
					pivasBatchDispRptRawMaterialDrugDtl.setSortSeq(pivasMergeFormulaWorksheetRawMaterialDtlSortSeq);
					
					pivasBatchDispRptRawMaterialDtlList.add(pivasBatchDispRptRawMaterialDrugDtl);
					
					for (PivasMergeFormulaWorksheetDrugInstructionDtl pivasMergeFormulaWorksheetDrugInstructionDtl : pivasMergeFormulaWorksheet.getPivasMergeFormulaWorksheetDrugInstructionDtlList()){
						if (pivasMergeFormulaWorksheetRawMaterialDtlSortSeq.equals(pivasMergeFormulaWorksheetDrugInstructionDtl.getSortSeq())){
							pivasBatchDispRptMergeXlsDtl.setDosageQty(pivasMergeFormulaWorksheetDrugInstructionDtl.getDosage());
							pivasBatchDispRptMergeXlsDtl.setModu(pivasMergeFormulaWorksheetDrugInstructionDtl.getPivasDosageUnit());	
							pivasBatchDispRptMergeXlsDtl.setConcn(pivasMergeFormulaWorksheetDrugInstructionDtl.getDrugConcn());	
							pivasBatchDispRptMergeXlsDtl.setDrugVolume(pivasMergeFormulaWorksheetDrugInstructionDtl.getDrugVolume());
							break;
						}
					}	
					pivasBatchDispRptMergeXlsDtl.setDrugItemCode(pivasMergeFormulaWorksheetRawMaterialDtl.getItemCode());
					pivasBatchDispRptMergeXlsDtl.setDrugItemDesc(pivasMergeFormulaWorksheetRawMaterialDtl.getItemDesc());
					pivasBatchDispRptMergeXlsDtl.setDrugManufCode(pivasMergeFormulaWorksheetRawMaterialDtl.getManufCode());
					pivasBatchDispRptMergeXlsDtl.setDrugBatchNum(pivasMergeFormulaWorksheetRawMaterialDtl.getBatchNum());
					pivasBatchDispRptMergeXlsDtl.setDrugExpiryDate(pivasMergeFormulaWorksheetRawMaterialDtl.getExpiryDate());
							
				}		 

				else if (StringUtils.isNoneBlank(pivasMergeFormulaWorksheetRawMaterialDtl.getItemCode()) && StringUtils.isNoneBlank(pivasMergeFormulaWorksheetRawMaterialDtl.getSolventDesc())) {

					PivasBatchDispRptRawMaterialDtl pivasBatchDispRptRawMaterialNonVendorSolventDtl = new PivasBatchDispRptRawMaterialDtl();
					
					pivasBatchDispRptRawMaterialNonVendorSolventDtl.setVendSupplySolvFlag(false);
								
					pivasBatchDispRptRawMaterialNonVendorSolventDtl.setItemCode(pivasMergeFormulaWorksheetRawMaterialDtl.getItemCode()); 
					pivasBatchDispRptRawMaterialNonVendorSolventDtl.setManufCode(pivasMergeFormulaWorksheetRawMaterialDtl.getManufCode());
					pivasBatchDispRptRawMaterialNonVendorSolventDtl.setBatchNum(pivasMergeFormulaWorksheetRawMaterialDtl.getBatchNum());
					pivasBatchDispRptRawMaterialNonVendorSolventDtl.setExpiryDate(pivasMergeFormulaWorksheetRawMaterialDtl.getExpiryDate());
					
					pivasBatchDispRptRawMaterialNonVendorSolventDtl.setSolventDesc(pivasMergeFormulaWorksheetRawMaterialDtl.getSolventDesc());
					pivasBatchDispRptRawMaterialNonVendorSolventDtl.setDiluentDesc(null);
					pivasBatchDispRptRawMaterialNonVendorSolventDtl.setSortSeq(pivasMergeFormulaWorksheetRawMaterialDtlSortSeq);
					
					pivasBatchDispRptRawMaterialDtlList.add(pivasBatchDispRptRawMaterialNonVendorSolventDtl);
					
					pivasBatchDispRptMergeXlsDtl.setVendSupplySolvFlag(false);				
					pivasBatchDispRptMergeXlsDtl.setSolventItemCode(pivasMergeFormulaWorksheetRawMaterialDtl.getItemCode()); 
					pivasBatchDispRptMergeXlsDtl.setSolventItemDesc(pivasMergeFormulaWorksheetRawMaterialDtl.getItemDesc());
					pivasBatchDispRptMergeXlsDtl.setVendSupplySolvDesc(null);	
					pivasBatchDispRptMergeXlsDtl.setSolventManufCode(pivasMergeFormulaWorksheetRawMaterialDtl.getManufCode());
					pivasBatchDispRptMergeXlsDtl.setSolventBatchNum(pivasMergeFormulaWorksheetRawMaterialDtl.getBatchNum());
					pivasBatchDispRptMergeXlsDtl.setSolventExpiryDate(pivasMergeFormulaWorksheetRawMaterialDtl.getExpiryDate());
								
				} 
				
				else if (Boolean.TRUE.equals(pivasMergeFormulaWorksheetRawMaterialDtl.getVendSupplySolvFlag())) {

					PivasBatchDispRptRawMaterialDtl pivasBatchDispRptRawMaterialVendorSolventDtl = new PivasBatchDispRptRawMaterialDtl();
					
					pivasBatchDispRptRawMaterialVendorSolventDtl.setVendSupplySolvFlag(true);

					pivasBatchDispRptRawMaterialVendorSolventDtl.setItemCode(null); 
					pivasBatchDispRptRawMaterialVendorSolventDtl.setManufCode(null);
					
					pivasBatchDispRptRawMaterialVendorSolventDtl.setSolventDesc(pivasMergeFormulaWorksheetRawMaterialDtl.getSolventDesc());
					pivasBatchDispRptRawMaterialVendorSolventDtl.setDiluentDesc(null);
					pivasBatchDispRptRawMaterialVendorSolventDtl.setSortSeq(pivasMergeFormulaWorksheetRawMaterialDtlSortSeq);
					
					pivasBatchDispRptMergeXlsDtl.setVendSupplySolvFlag(true);
					pivasBatchDispRptMergeXlsDtl.setSolventItemCode(null); 

					pivasBatchDispRptMergeXlsDtl.setSolventItemDesc(null);			
					pivasBatchDispRptMergeXlsDtl.setVendSupplySolvDesc(pivasMergeFormulaWorksheetRawMaterialDtl.getSolventDesc());
					pivasBatchDispRptMergeXlsDtl.setSolventManufCode(null);
	
					if (StringUtils.isNotBlank(pivasMergeFormulaWorksheetRawMaterialDtl.getBatchNum())) {
						pivasBatchDispRptRawMaterialVendorSolventDtl.setBatchNum(pivasMergeFormulaWorksheetRawMaterialDtl.getBatchNum());
						pivasBatchDispRptRawMaterialVendorSolventDtl.setExpiryDate(pivasMergeFormulaWorksheetRawMaterialDtl.getExpiryDate());
						
						pivasBatchDispRptMergeXlsDtl.setSolventBatchNum(pivasMergeFormulaWorksheetRawMaterialDtl.getBatchNum());
						pivasBatchDispRptMergeXlsDtl.setSolventExpiryDate(pivasMergeFormulaWorksheetRawMaterialDtl.getExpiryDate());							
						
					}
								
					pivasBatchDispRptRawMaterialDtlList.add(pivasBatchDispRptRawMaterialVendorSolventDtl);
					
				}
							
				else if ( StringUtils.isNoneBlank(pivasMergeFormulaWorksheetRawMaterialDtl.getDiluentDesc()) ) {
					PivasBatchDispRptRawMaterialDtl pivasBatchDispRptRawMaterialDiluentDtl = new PivasBatchDispRptRawMaterialDtl();
					
					pivasBatchDispRptRawMaterialDiluentDtl.setItemCode(pivasMergeFormulaWorksheetRawMaterialDtl.getItemCode());
					pivasBatchDispRptRawMaterialDiluentDtl.setManufCode(pivasMergeFormulaWorksheetRawMaterialDtl.getManufCode());
					pivasBatchDispRptRawMaterialDiluentDtl.setBatchNum(pivasMergeFormulaWorksheetRawMaterialDtl.getBatchNum());
					pivasBatchDispRptRawMaterialDiluentDtl.setExpiryDate(pivasMergeFormulaWorksheetRawMaterialDtl.getExpiryDate());

					pivasBatchDispRptRawMaterialDiluentDtl.setSolventDesc(null);
					pivasBatchDispRptRawMaterialDiluentDtl.setDiluentDesc(pivasMergeFormulaWorksheetRawMaterialDtl.getDiluentDesc());
					pivasBatchDispRptRawMaterialDiluentDtl.setSortSeq(pivasMergeFormulaWorksheetRawMaterialDtlSortSeq);
					
					pivasBatchDispRptRawMaterialDtlList.add(pivasBatchDispRptRawMaterialDiluentDtl);
					
					for (PivasBatchDispRptDtl diluentXlsDtl :sortSeqPivasBatchDispRptMergeXlsDtlMap.values()) {
			
						diluentXlsDtl.setDiluentItemCode(pivasMergeFormulaWorksheetRawMaterialDtl.getItemCode());
						diluentXlsDtl.setDiluentItemDesc(pivasMergeFormulaWorksheetRawMaterialDtl.getItemDesc());
						diluentXlsDtl.setDiluentManufCode(pivasMergeFormulaWorksheetRawMaterialDtl.getManufCode());
						diluentXlsDtl.setDiluentBatchNum(pivasMergeFormulaWorksheetRawMaterialDtl.getBatchNum());
						diluentXlsDtl.setDiluentExpiryDate(pivasMergeFormulaWorksheetRawMaterialDtl.getExpiryDate());

						diluentXlsDtl.setDiluentDesc(pivasMergeFormulaWorksheetRawMaterialDtl.getDiluentDesc());
					}
				}		
				
			}
						
			pivasBatchDispRptDtl.setPivasBatchDispRptRawMaterialDtlList(pivasBatchDispRptRawMaterialDtlList);
			pivasBatchDispRptDtlList.add(pivasBatchDispRptDtl);
			
			List<PivasBatchDispRptDtl> pivasBatchDispRptXlsMergeDtlList = new ArrayList<PivasBatchDispRptDtl>(sortSeqPivasBatchDispRptMergeXlsDtlMap.values());
			pivasBatchDispRptXlsDtlList.addAll(pivasBatchDispRptXlsMergeDtlList);
			
		}		
	}

	private void buildPivasWorkloadSummaryPatientRptMap(List<DeliveryItem> deliveryItemList) {
		
		for (DeliveryItem deliveryItem : deliveryItemList) {
			
			Object label = labelJaxbWrapper.unmarshall(deliveryItem.getLabelXml());
			PivasProductLabel pivasProductLabel = ((LabelContainer) label).getPivasProductLabel();
			PivasWorksheet pivasWorksheet = ((LabelContainer) label).getPivasWorksheet();
			
			// supplyDate is used in export only
			// reset hours, minutes, seconds for mapping and display in export
			Date supplyDate = resetDateHours(pivasProductLabel.getManufactureDate());
			
			String itemCode = pivasWorksheet.getDrugItemCode();
			String pivasFormulaDescWithoutRatioDesc = buildPivasWorkloadSummaryRptDtlPivasNormalFormulaWithoutRatioDesc(pivasWorksheet);
			// 10dp ratio is used for mapping precision
			// ratio can be null
			BigDecimal fullRatio = calPivasWorkloadSummaryRptDtlRatio(pivasWorksheet);
			String ward = pivasProductLabel.getWard();
			Integer numOfDose = Integer.valueOf(pivasWorksheet.getIssueQty()) + Integer.valueOf(pivasWorksheet.getAdjQty());
			
			
			List<String> rptDtlKeyList = new ArrayList<String>();
			rptDtlKeyList.add(itemCode);
			rptDtlKeyList.add(pivasFormulaDescWithoutRatioDesc);
			rptDtlKeyList.add(String.valueOf(fullRatio));
			rptDtlKeyList.add(ward);
			String rptDtlKey = StringUtils.join(rptDtlKeyList.toArray(), "|");
			
			pivasWorkloadSummaryPatientRptDtlMap = addPivasWorkloadSummaryPatientRptMap(pivasWorkloadSummaryPatientRptDtlMap, rptDtlKey, 
													itemCode, pivasFormulaDescWithoutRatioDesc, fullRatio, ward, numOfDose, null);
			
			
			List<String> rptXlsKeyList = new ArrayList<String>();
			
			// supplyDate is null in non-export
			rptXlsKeyList.add(String.valueOf(supplyDate));
			
			rptXlsKeyList.add(itemCode);
			rptXlsKeyList.add(pivasFormulaDescWithoutRatioDesc);
			rptXlsKeyList.add(String.valueOf(fullRatio));
			rptXlsKeyList.add(ward);
			String rptXlsKey = StringUtils.join(rptXlsKeyList.toArray(), "|");
			
			pivasWorkloadSummaryPatientXlsMap = addPivasWorkloadSummaryPatientRptMap(pivasWorkloadSummaryPatientXlsMap, rptXlsKey, 
													itemCode, pivasFormulaDescWithoutRatioDesc, fullRatio, ward, numOfDose, supplyDate);
			
			
			pivasWorkloadSummaryPatientRptWardSummaryDtlMap = addPivasWorkloadSummaryPatientRptWardSummaryDtlMap(pivasWorkloadSummaryPatientRptWardSummaryDtlMap, 
																													ward, numOfDose);
			
		}
	}
	
	private Map<String, PivasWorkloadSummaryPatientRptDtl> addPivasWorkloadSummaryPatientRptMap(Map<String, PivasWorkloadSummaryPatientRptDtl> map, String key, 
			String itemCode, String pivasFormulaDescWithoutRatioDesc, BigDecimal fullRatio, String ward, Integer numOfDose, Date supplyDate) {
		
		// check duplicated map key
		if (!map.containsKey(key)) {
			PivasWorkloadSummaryPatientRptDtl pivasWorkloadSummaryPatientRptDtl = new PivasWorkloadSummaryPatientRptDtl();
			
			// supplyDate is used in export only
			pivasWorkloadSummaryPatientRptDtl.setSupplyDate(supplyDate);
			
			pivasWorkloadSummaryPatientRptDtl.setItemCode(itemCode);
			pivasWorkloadSummaryPatientRptDtl.setPivasFormulaDescWithoutRatio(pivasFormulaDescWithoutRatioDesc);
			pivasWorkloadSummaryPatientRptDtl.setRatio(fullRatio);
			
			pivasWorkloadSummaryPatientRptDtl.setWard(ward);
			pivasWorkloadSummaryPatientRptDtl.setTotalNumOfDose(numOfDose);
			
			map.put(key, pivasWorkloadSummaryPatientRptDtl);
		} else { // This key item is processed before, sum up numOfDose
			PivasWorkloadSummaryPatientRptDtl pivasWorkloadSummaryPatientRptDtl = map.get(key);
			pivasWorkloadSummaryPatientRptDtl.setTotalNumOfDose(pivasWorkloadSummaryPatientRptDtl.getTotalNumOfDose() + numOfDose);
			map.put(key, pivasWorkloadSummaryPatientRptDtl);
		}
		
		return map;
	}
	
	private Map<String, Integer> addPivasWorkloadSummaryPatientRptWardSummaryDtlMap(Map<String, Integer> pivasWorkloadSummaryPatientRptWardSummaryDtlMap, 
																					String ward, Integer addWardNumOfDose) {
		
		if (!pivasWorkloadSummaryPatientRptWardSummaryDtlMap.containsKey(ward)) {
			pivasWorkloadSummaryPatientRptWardSummaryDtlMap.put(ward, addWardNumOfDose);
		} else {
			Integer newWardNumOfDose = pivasWorkloadSummaryPatientRptWardSummaryDtlMap.get(ward) + addWardNumOfDose;
			pivasWorkloadSummaryPatientRptWardSummaryDtlMap.put(ward, newWardNumOfDose);
		}
		
		return pivasWorkloadSummaryPatientRptWardSummaryDtlMap;
	}
	
	private List<PivasWorkloadSummaryPatientRptDtl> buildPivasWorkloadSummaryPatientRptDtlList(Map<String, PivasWorkloadSummaryPatientRptDtl> map) {
		
		List<PivasWorkloadSummaryPatientRptDtl> pivasWorkloadSummaryPatientRptDtlList = new ArrayList<PivasWorkloadSummaryPatientRptDtl>();
		for (PivasWorkloadSummaryPatientRptDtl pivasWorkloadSummaryPatientRptDtl : map.values()) {
			
			String pivasFormulaRatioDesc = pivasWorkloadSummaryPatientRptDtl.getPivasFormulaDescWithoutRatio();
			
			if (pivasWorkloadSummaryPatientRptDtl.getRatio() != null) {
				BigDecimal displayRatio = pivasWorkloadSummaryPatientRptDtl.getRatio().setScale(4, RoundingMode.HALF_UP);
				pivasFormulaRatioDesc = pivasFormulaRatioDesc + " (" + convertToString(displayRatio) + ")";
			}
			
			pivasWorkloadSummaryPatientRptDtl.setPivasFormulaDesc(pivasFormulaRatioDesc);
			pivasWorkloadSummaryPatientRptDtlList.add(pivasWorkloadSummaryPatientRptDtl);
		}
		
		return pivasWorkloadSummaryPatientRptDtlList;
	}
	
	private List<PivasWorkloadSummaryPatientRptWardSummaryDtl> buildPivasWorkloadSummaryPatientRptWardSummaryDtlList(Map<String, Integer> pivasWorkloadSummaryPatientRptWardSummaryDtlMap) {
		
		List<PivasWorkloadSummaryPatientRptWardSummaryDtl> pivasWorkloadSummaryPatientRptWardSummaryDtlList = new ArrayList<PivasWorkloadSummaryPatientRptWardSummaryDtl>();
		for (String ward: pivasWorkloadSummaryPatientRptWardSummaryDtlMap.keySet()) {
			PivasWorkloadSummaryPatientRptWardSummaryDtl pivasWorkloadSummaryPatientRptWardSummaryDtl = new PivasWorkloadSummaryPatientRptWardSummaryDtl();
			pivasWorkloadSummaryPatientRptWardSummaryDtl.setWard(ward);
			pivasWorkloadSummaryPatientRptWardSummaryDtl.setTotalNumOfDose(pivasWorkloadSummaryPatientRptWardSummaryDtlMap.get(ward));
			pivasWorkloadSummaryPatientRptWardSummaryDtlList.add(pivasWorkloadSummaryPatientRptWardSummaryDtl);
		}
		
		return pivasWorkloadSummaryPatientRptWardSummaryDtlList;
	}
	
	private List<PivasWorkloadSummaryPatientRptDtl> modifyIndPivasWorkloadSummaryPatientRptPatientDtlList(List<PivasWorkloadSummaryPatientRptDtl> pivasWorkloadSummaryPatientRptDtlList) {
		
		//list is sorted and in fixed sequence
		Map<String, List<Integer> > indexMap = new HashMap<String, List<Integer>>();
		
		for (int dtlIndex = 0 ; dtlIndex < pivasWorkloadSummaryPatientRptDtlList.size() ; dtlIndex++ ) {
			List<String> keyList = new ArrayList<String>();
			keyList.add(pivasWorkloadSummaryPatientRptDtlList.get(dtlIndex).getItemCode());
			keyList.add(pivasWorkloadSummaryPatientRptDtlList.get(dtlIndex).getPivasFormulaDesc());
			String groupKey = StringUtils.join(keyList.toArray(), "|");
			
			List<Integer> dtlIndexList = null;
			
			if (!indexMap.containsKey(groupKey)) {
				dtlIndexList = new ArrayList<Integer>();
			} else {
				dtlIndexList = indexMap.get(groupKey);
			}	
			
			dtlIndexList.add(dtlIndex);
			indexMap.put(groupKey, dtlIndexList);
		}
		
		for (List<Integer> indexList : indexMap.values()) {
			if (indexList.size() == 1) {
				// only one element in group
				PivasWorkloadSummaryPatientRptDtl dtl = pivasWorkloadSummaryPatientRptDtlList.get(indexList.get(0));
				dtl.setGroupFirstDtlInd(true);
				dtl.setGroupLastDtlInd(true);
			} else { // more than one element
				// first element in group
				PivasWorkloadSummaryPatientRptDtl groupFirstDtl = pivasWorkloadSummaryPatientRptDtlList.get(indexList.get(0));
				groupFirstDtl.setGroupFirstDtlInd(true);
				groupFirstDtl.setGroupLastDtlInd(false);
				
				// last element in group
				PivasWorkloadSummaryPatientRptDtl groupLastDtl = pivasWorkloadSummaryPatientRptDtlList.get(indexList.get(indexList.size()-1));
				groupLastDtl.setGroupFirstDtlInd(false);
				groupLastDtl.setGroupLastDtlInd(true);
			}
		}
		
		return pivasWorkloadSummaryPatientRptDtlList;
	}
	
	private void buildPivasWorkloadSummaryNormalFormulaBatchRptDtlMap(List<PivasBatchPrep> pivasBatchPrepList) {
		
		for (PivasBatchPrep pivasBatchPrep : pivasBatchPrepList) {
			Object label = labelJaxbWrapper.unmarshall(pivasBatchPrep.getLabelXml());
			PivasProductLabel pivasProductLabel = ((LabelContainer) label).getPivasProductLabel();
			PivasWorksheet pivasWorksheet = ((LabelContainer) label).getPivasWorksheet();
			
			// supplyDate is used in export only
			// reset hours, minutes, seconds for mapping and display in export
			Date supplyDate = resetDateHours(pivasProductLabel.getManufactureDate());
			
			String itemCode = pivasWorksheet.getDrugItemCode();
			String pivasFormulaDescWithoutRatioDesc = buildPivasWorkloadSummaryRptDtlPivasNormalFormulaWithoutRatioDesc(pivasWorksheet);
			// 10dp ratio is used for mapping precision
			// ratio can be null
			BigDecimal fullRatio = calPivasWorkloadSummaryRptDtlRatio(pivasWorksheet);
			Integer numOfDose = Integer.valueOf(pivasWorksheet.getPrepQty());
			
			
			List<String> rptDtlKeyList = new ArrayList<String>();
			rptDtlKeyList.add(itemCode);
			rptDtlKeyList.add(pivasFormulaDescWithoutRatioDesc);
			rptDtlKeyList.add(String.valueOf(fullRatio));
			String rptDtlKey = StringUtils.join(rptDtlKeyList.toArray(), "|");
			
			pivasWorkloadSummaryBatchRptNormalDtlMap = addPivasWorkloadSummaryBatchRptMap(pivasWorkloadSummaryBatchRptNormalDtlMap, rptDtlKey, 
													itemCode, pivasFormulaDescWithoutRatioDesc, fullRatio, numOfDose, null, PivasFormulaType.Normal);
			
			
			List<String> rptXlsKeyList = new ArrayList<String>();
			
			// supplyDate is null in non-export
			rptXlsKeyList.add(String.valueOf(supplyDate));
			
			rptXlsKeyList.add(itemCode);
			rptXlsKeyList.add(pivasFormulaDescWithoutRatioDesc);
			rptXlsKeyList.add(String.valueOf(fullRatio));
			String rptXlsKey = StringUtils.join(rptXlsKeyList.toArray(), "|");
			
			pivasWorkloadSummaryBatchXlsMap = addPivasWorkloadSummaryBatchRptMap(pivasWorkloadSummaryBatchXlsMap, rptXlsKey, 
													itemCode, pivasFormulaDescWithoutRatioDesc, fullRatio, numOfDose, supplyDate, PivasFormulaType.Normal);
			
		}
	}

	private void buildPivasWorkloadSummaryMergeFormulaBatchRptDtlMap(List<PivasBatchPrep> pivasBatchPrepList) {
		
		for (PivasBatchPrep pivasBatchPrep : pivasBatchPrepList) {
			Object label = labelJaxbWrapper.unmarshall(pivasBatchPrep.getLabelXml());
			PivasMergeFormulaProductLabel pivasMergeFormulaProductLabel = ((LabelContainer) label).getPivasMergeFormulaProductLabel();
			PivasMergeFormulaWorksheet pivasMergeFormulaWorksheet = ((LabelContainer) label).getPivasMergeFormulaWorksheet();
			
			// supplyDate is used in export only
			// reset hours, minutes, seconds for mapping and display in export
			Date supplyDate = resetDateHours(pivasMergeFormulaProductLabel.getManufactureDate());
			
			String itemCode = pivasMergeFormulaWorksheet.getTargetProductItemCode();
			String pivasFormulaDescWithoutRatioDesc = buildPivasWorkloadSummaryRptDtlPivasMergeFormulaWithoutRatioDesc(pivasMergeFormulaWorksheet);
			// ratio does not exist in pivasFormulaDesc for PIVAS Merge Formula                                     
			
			Integer numOfDose = Integer.valueOf(pivasMergeFormulaWorksheet.getPrepQty());
			
			
			List<String> rptDtlKeyList = new ArrayList<String>();
			rptDtlKeyList.add(itemCode);
			rptDtlKeyList.add(pivasFormulaDescWithoutRatioDesc);

			String rptDtlKey = StringUtils.join(rptDtlKeyList.toArray(), "|");
			
			pivasWorkloadSummaryBatchRptMergeDtlMap = addPivasWorkloadSummaryBatchRptMap(pivasWorkloadSummaryBatchRptMergeDtlMap, rptDtlKey, 
													itemCode, pivasFormulaDescWithoutRatioDesc, null, numOfDose, null, PivasFormulaType.Merge);
			
			
			List<String> rptXlsKeyList = new ArrayList<String>();
			
			// supplyDate is null in non-export
			rptXlsKeyList.add(String.valueOf(supplyDate));
			
			rptXlsKeyList.add(itemCode);
			rptXlsKeyList.add(pivasFormulaDescWithoutRatioDesc);
			// ratio does not exist in pivasFormulaDesc for PIVAS Merge Formula                                     
			
			String rptXlsKey = StringUtils.join(rptXlsKeyList.toArray(), "|");
			
			pivasWorkloadSummaryBatchXlsMap = addPivasWorkloadSummaryBatchRptMap(pivasWorkloadSummaryBatchXlsMap, rptXlsKey, 
													itemCode, pivasFormulaDescWithoutRatioDesc, null, numOfDose, supplyDate, PivasFormulaType.Merge); 	
		}
	}			
 
	private Map<String, PivasWorkloadSummaryBatchRptDtl> addPivasWorkloadSummaryBatchRptMap(Map<String, PivasWorkloadSummaryBatchRptDtl> map, String key, 
			String itemCode, String pivasFormulaDescWithoutRatioDesc, BigDecimal fullRatio, Integer numOfDose, Date supplyDate, PivasFormulaType pivasFormulaType ) {
		
		// check duplicated map key
		if (!map.containsKey(key)) {
			PivasWorkloadSummaryBatchRptDtl pivasWorkloadSummaryBatchRptDtl = new PivasWorkloadSummaryBatchRptDtl();
			
			// supplyDate is used in export only
			pivasWorkloadSummaryBatchRptDtl.setSupplyDate(supplyDate);
			
			pivasWorkloadSummaryBatchRptDtl.setItemCode(itemCode);
			pivasWorkloadSummaryBatchRptDtl.setPivasFormulaDescWithoutRatio(pivasFormulaDescWithoutRatioDesc);
			pivasWorkloadSummaryBatchRptDtl.setRatio(fullRatio);
			
			pivasWorkloadSummaryBatchRptDtl.setTotalNumOfDose(numOfDose);
			pivasWorkloadSummaryBatchRptDtl.setPivasFormulaType(pivasFormulaType);
			
			map.put(key, pivasWorkloadSummaryBatchRptDtl);
		} else { // This key item is processed before, sum up numOfDose
			PivasWorkloadSummaryBatchRptDtl pivasWorkloadSummaryBatchRptDtl = map.get(key);
			pivasWorkloadSummaryBatchRptDtl.setTotalNumOfDose(pivasWorkloadSummaryBatchRptDtl.getTotalNumOfDose() + numOfDose);
			map.put(key, pivasWorkloadSummaryBatchRptDtl);
		}
		
		return map;
	}
	
	private List<PivasWorkloadSummaryBatchRptDtl> buildPivasWorkloadSummaryBatchRptDtlList(Map<String, PivasWorkloadSummaryBatchRptDtl> map) {
	
		List<PivasWorkloadSummaryBatchRptDtl> pivasWorkloadSummaryBatchRptDtlList = new ArrayList<PivasWorkloadSummaryBatchRptDtl>();
		for (PivasWorkloadSummaryBatchRptDtl pivasWorkloadSummaryBatchRptDtl : map.values()) {
			
			String pivasFormulaRatioDesc = pivasWorkloadSummaryBatchRptDtl.getPivasFormulaDescWithoutRatio();
			
			if (pivasWorkloadSummaryBatchRptDtl.getPivasFormulaType() == PivasFormulaType.Normal && pivasWorkloadSummaryBatchRptDtl.getRatio() != null) {
				BigDecimal displayRatio = pivasWorkloadSummaryBatchRptDtl.getRatio().setScale(4, RoundingMode.HALF_UP);
				pivasFormulaRatioDesc = pivasFormulaRatioDesc + " (" + convertToString(displayRatio) + ")";
			}
			
			pivasWorkloadSummaryBatchRptDtl.setPivasFormulaDesc(pivasFormulaRatioDesc);
			pivasWorkloadSummaryBatchRptDtlList.add(pivasWorkloadSummaryBatchRptDtl);
		}
		
		return pivasWorkloadSummaryBatchRptDtlList;
	}
	
	private String buildPivasPatientDispRptDtlPrepDesc(PivasProductLabel pivasProductLabel) {
		
		// first line
		String pivasDesc = pivasProductLabel.getFormulaPrintName();
		
		if (!pivasProductLabel.isSplitFlag()) {
			pivasDesc += " " + pivasProductLabel.getOrderDosage(); 
		} else {
			pivasDesc += " " + pivasProductLabel.getOrderDosageSplit();
		}
		pivasDesc += pivasProductLabel.getPivasDosageUnit();
		
		if ((!("ML").equalsIgnoreCase(pivasProductLabel.getPivasDosageUnit()) || StringUtils.isNotBlank(pivasProductLabel.getDiluentDesc())) && StringUtils.isNotBlank(pivasProductLabel.getFinalVolume())) {
			if (!pivasProductLabel.isSplitFlag()) {
				pivasDesc += " in " + pivasProductLabel.getFinalVolume() + "ML";
			} else {
				pivasDesc += " in " + pivasProductLabel.getFinalVolumeSplit() + "ML";
			}
		}
		
		if (StringUtils.isNotBlank(pivasProductLabel.getDiluentDesc())) {
			pivasDesc += " " + pivasProductLabel.getDiluentDesc();
		}
		
		pivasDesc += NEW_LINE;
		
		// second line
		if (!("ML").equalsIgnoreCase(pivasProductLabel.getPivasDosageUnit())) {
			pivasDesc += pivasProductLabel.getConcn() + pivasProductLabel.getPivasDosageUnit() + "/ML" + " - ";
		}
		pivasDesc += pivasProductLabel.getSiteDesc();
		
		pivasDesc += " - " + pivasProductLabel.getFreq();
		
		String supplFreqDesc = pivasProductLabel.getSupplFreq();
		if (StringUtils.isNotBlank(supplFreqDesc)) {
			pivasDesc += " (" +  StringUtils.trim(supplFreqDesc.toLowerCase()) + ")";
		}

		// third line
		if (pivasProductLabel.isSplitFlag()) {
			pivasDesc += NEW_LINE;
			pivasDesc += "x " + pivasProductLabel.getSplitCount() + " split";
		}
		
		return pivasDesc;
	}
	
	private String buildPivasBatchDispRptNormalDtlPrepDesc(PivasProductLabel pivasProductLabel, PivasWorksheet pivasWorksheet) {
		
		String labelDesc = pivasProductLabel.getPrintName();
		labelDesc += NEW_LINE;
		
		if (!("ML").equalsIgnoreCase(pivasProductLabel.getPivasDosageUnit()) && StringUtils.isNoneBlank(pivasWorksheet.getConcn())) {
			labelDesc += pivasWorksheet.getConcn() + pivasProductLabel.getPivasDosageUnit() + "/ML" + " - ";
		}
		
		labelDesc += pivasProductLabel.getSiteDesc();

		return labelDesc;
	}

	private String buildPivasBatchDispRptMergeDtlPrepDesc(PivasMergeFormulaProductLabel pivasMergeFormulaProductLabel) {
		
		String labelDesc = pivasMergeFormulaProductLabel.getPrintName();
		labelDesc += NEW_LINE;

		labelDesc += pivasMergeFormulaProductLabel.getSiteDesc();

		return labelDesc;
	}
	
	private String buildPivasPatientDispRptDtlSupplyDesc(PivasProductLabel pivasProductLabel, PivasWorksheet pivasWorksheet) {
		
		String supplyDesc = "";
		
		if (pivasProductLabel.isSplitFlag()) {
			supplyDesc += pivasProductLabel.getFinalVolumeSplit() + "ML";
		} else {
			supplyDesc += pivasProductLabel.getFinalVolume() + "ML";
		}
		
		if (StringUtils.isNoneBlank(pivasWorksheet.getPrepQty())) {
			supplyDesc += NEW_LINE;
			supplyDesc += pivasWorksheet.getPrepQty();

			if (StringUtils.isNoneBlank(pivasProductLabel.getContainerName())) {
				supplyDesc += " " + pivasProductLabel.getContainerName();
			}

			if (pivasProductLabel.isSplitFlag()) {
				supplyDesc += " x " + pivasProductLabel.getSplitCount();
			}
		}
		
		if(StringUtils.isNoneBlank(pivasWorksheet.getDrugDispQty()) && Long.parseLong(pivasWorksheet.getDrugDispQty()) > 0L)
		{
			supplyDesc += NEW_LINE;
			supplyDesc += pivasWorksheet.getDrugDispQty();
			supplyDesc += " ";
			supplyDesc += pivasWorksheet.getDrugBaseUnit();
		}
		
		return supplyDesc;
	}

	
	private String buildPivasWorkloadSummaryRptDtlPivasNormalFormulaWithoutRatioDesc(PivasWorksheet pivasWorksheet) {
		
		// strength is included in formulaPrintName if PDU = ML
		// formulaPrintName from pivasProductLabel can be null
		String desc = pivasWorksheet.getFormulaPrintName();
		
		// only concn in batch and patient PivasWorksheet are both from PIVAS formula
		if (!("ML").equalsIgnoreCase(pivasWorksheet.getPivasDosageUnit())) {
			desc += " " + pivasWorksheet.getConcn() + pivasWorksheet.getPivasDosageUnit() + "/ML";
		}
		
		// diluentDesc from pivasProductLabel can be null
		if (StringUtils.isNotBlank(pivasWorksheet.getDiluentDesc())) {
			desc += " in " + pivasWorksheet.getDiluentDesc();
		}
		
		desc += " " + pivasWorksheet.getSiteDesc();
		
		return desc;
	}
	
	private String buildPivasWorkloadSummaryRptDtlPivasMergeFormulaWithoutRatioDesc(PivasMergeFormulaWorksheet pivasMergeFormulaWorksheet) {
		
		String desc = pivasMergeFormulaWorksheet.getFormulaPrintName();
		
		desc += " " + pivasMergeFormulaWorksheet.getSiteDesc();
		
		return desc;
	}	

	private BigDecimal calPivasWorkloadSummaryRptDtlRatio(PivasWorksheet pivasWorksheet) {
		
		BigDecimal pivasWorkloadSummaryRptDtlRatio = null;
		
		
		if (("ML").equalsIgnoreCase(pivasWorksheet.getPivasDosageUnit()) && StringUtils.isNotBlank(pivasWorksheet.getDiluentDesc()) 
				&& StringUtils.isNotBlank(pivasWorksheet.getDrugVolume()) && StringUtils.isNotBlank(pivasWorksheet.getRatio())) {
			
			BigDecimal drugVolume = new BigDecimal(pivasWorksheet.getDrugVolume());
			// 10dp is used for mapping precision
			pivasWorkloadSummaryRptDtlRatio = drugVolume.divide(new BigDecimal(pivasWorksheet.getRatio()), 10, RoundingMode.HALF_UP);
			
			// ratio not shown if 1
			if (BigDecimal.ONE.compareTo(pivasWorkloadSummaryRptDtlRatio) == 0) {
				pivasWorkloadSummaryRptDtlRatio = null;
			}
		}
		
		return pivasWorkloadSummaryRptDtlRatio;
	}
	
	// hkidCaseNum searching is for PivasPatientDispReport filtering
	// pivasRptItemCatFilterOption, drugKey and itemCode are for filtering by drug key in PivasPatientDispReport and PivasBatchDispReport
	@SuppressWarnings("unchecked")
	private List<PivasWorklist> retrievePivasWorklistList(Date supplyDateFrom, Date supplyDateTo, YesNoBlankFlag satelliteFlag, 
															String hkidCaseNum, PivasRptItemCatFilterOption pivasRptItemCatFilterOption, 
															String itemCode, Boolean drugKeyFlag) {
		
		boolean isItemCodeSearch = StringUtils.isNotBlank(itemCode);
		boolean isDrugKeySearch = false;
		if (drugKeyFlag != null &&  Boolean.TRUE.equals(drugKeyFlag)) {
			isDrugKeySearch = true;
		}
		boolean isHkidCaseNumSearch = StringUtils.isNotBlank(hkidCaseNum);
		
		
		StringBuilder sb = new StringBuilder(
				"select o.pivasWorklist from DeliveryItem o" + // 20161128 index check : PivasWorklist.workstore,status,supplyDate : I_PIVAS_WORKLIST_02
				" where o.delivery.deliveryRequest.workstore.hospCode = :hospCode" +
				" and o.delivery.deliveryRequest.workstore.workstoreCode = :workstoreCode" +
				" and o.pivasWorklist.status = :status" 
				);
		
		if (supplyDateTo != null) {
			// search by supply date range
			sb.append(" and o.pivasWorklist.supplyDate >= :supplyDateFrom and o.pivasWorklist.supplyDate < :supplyDateTo");
		} else {
			// search by specific supply date with supply time
			sb.append(" and o.pivasWorklist.supplyDate = :supplyDateFrom");
		}
		
		if (satelliteFlag == YesNoBlankFlag.Yes || satelliteFlag == YesNoBlankFlag.No) {
			sb.append(" and o.pivasWorklist.satelliteFlag = :satelliteFlag");
		}
		
		if (isHkidCaseNumSearch) {
			sb.append(" and (o.pivasWorklist.medProfile.medCase.caseNum = :hkidCaseNum" +
						" or o.pivasWorklist.medProfile.patient.hkid = :hkidCaseNum)");
		}
		
		if ( isItemCodeSearch && isDrugKeySearch && pivasRptItemCatFilterOption != null && pivasRptItemCatFilterOption == PivasRptItemCatFilterOption.Drug ) {
			// filter by drugKey if drugKey checkBox is ticked 
			// drugKey checkBox is only allowed to be checked when there is itemCode and Drug pivasRptItemCatFilterOption
			sb.append(" and o.pivasWorklist.pivasPrep.drugKey in :drugKeyList");
		}
		
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("hospCode", workstore.getHospCode());
		query.setParameter("workstoreCode", workstore.getWorkstoreCode());
		query.setParameter("status", PivasWorklistStatus.Completed);
		
		if (supplyDateTo != null) {
			// search by supply date range
			query.setParameter("supplyDateFrom", supplyDateFrom, TemporalType.DATE);
			DateMidnight toDate = new DateMidnight(supplyDateTo).plusDays(1);
			query.setParameter("supplyDateTo", toDate.toDate(), TemporalType.DATE);
		} else {
			// search by specific supply date with supply time
			query.setParameter("supplyDateFrom", supplyDateFrom, TemporalType.TIMESTAMP);
		}
		
		if (satelliteFlag == YesNoBlankFlag.Yes) {
			query.setParameter("satelliteFlag", Boolean.TRUE);
		} else if (satelliteFlag == YesNoBlankFlag.No) {
			query.setParameter("satelliteFlag", Boolean.FALSE);
		}
		//ignore when satelliteFlag is BLANK
		
		if (isHkidCaseNumSearch) {
			query.setParameter("hkidCaseNum", hkidCaseNum);
		}
		
		
		List <PivasWorklist> rawPivasWorklistList = null;
		if ( isItemCodeSearch && isDrugKeySearch && pivasRptItemCatFilterOption != null && pivasRptItemCatFilterOption == PivasRptItemCatFilterOption.Drug) {
			// drugKey search
			List<String> itemCodeList = new ArrayList<String>();
			// isItemCodeSearch ensures itemCode is not null
			itemCodeList.add(itemCode);
			// returned drugKeyList is distinct
			// PivasDrug will never be physically removed from the table
			// retrieved drugKeyList is all drugKey for this itemCode while pivasPrep.drugKey is restricted within the date range
			List<Integer> drugKeyList = dmsPmsServiceProxy.retrievePivasDrugKeyListByItemCode(workstore.getWorkstoreGroup().getWorkstoreGroupCode(), itemCodeList);
			rawPivasWorklistList = QueryUtils.splitExecute(query, "drugKeyList", drugKeyList);
		} else { // normal flow
			rawPivasWorklistList = query.getResultList();
		}
		
		return rawPivasWorklistList;
	}
	
	@SuppressWarnings("unchecked")
	private List<PivasWorklist> retrievePivasWorklistListByPivasPrepMethodInfo(List<PivasWorklist> rawPivasWorklistList,
																				PivasRptItemCatFilterOption pivasRptItemCatFilterOption, 
																				String itemCode, Boolean drugKeyFlag, String manufCode, String batchNum) {
		
		boolean isItemCodeSearch = StringUtils.isNotBlank(itemCode);
		boolean isDrugKeySearch = false;
		if (drugKeyFlag != null && Boolean.TRUE.equals(drugKeyFlag)) {
			isDrugKeySearch = true;
		}
		boolean isManufCodeSearch = StringUtils.isNotBlank(manufCode);
		boolean isBatchNumSearch = StringUtils.isNotBlank(batchNum);
		
		
		// drug key search alone is not required to search in PivasPrepMethod and PivasPrepMethodItem
		if ( (isItemCodeSearch && !isDrugKeySearch) || isManufCodeSearch || isBatchNumSearch) {
			Set<Long> rawPivasPrepMethodIdSet = new HashSet<Long>();
			for (PivasWorklist rawPivasWorklist :rawPivasWorklistList) {
				rawPivasPrepMethodIdSet.add(rawPivasWorklist.getPivasPrepMethodId());
			}
			
			// only need id for filtering, data will be retrieved in labelXml
			StringBuilder sb = new StringBuilder(
					"select o.pivasPrepMethodId from PivasPrepMethodItem o" + // 20161128 index check : PivasPrepMethodItem.pivasPrepMethodId : FK_PIVAS_PREP_METHOD_ITEM_01
					" where o.pivasPrepMethodId in :rawPivasPrepMethodIdSet");
			
			if (pivasRptItemCatFilterOption != null) {
				sb = buildPivasRptItemCatFilterOptionQuery(sb, PivasRptDispMethod.PatientDisp, pivasRptItemCatFilterOption, 
															isItemCodeSearch, isDrugKeySearch, isManufCodeSearch, isBatchNumSearch);
			}	
			
			
			Query query = em.createQuery(sb.toString());
			
			if (pivasRptItemCatFilterOption != null) {
				if (isItemCodeSearch && !isDrugKeySearch) {
					query.setParameter("itemCode", itemCode);
				}

				if (isManufCodeSearch) {
					query.setParameter("manufCode", manufCode);
				}
					
				if (isBatchNumSearch) {
					query.setParameter("batchNum", batchNum);
				}
			}
			
			
			List<Long> pivasPrepMethodIdList = QueryUtils.splitExecute(query, "rawPivasPrepMethodIdSet", rawPivasPrepMethodIdSet);
			
			if (pivasPrepMethodIdList != null && !pivasPrepMethodIdList.isEmpty()) {
				Set<Long> pivasPrepMethodIdSet = new HashSet<Long>(pivasPrepMethodIdList);
				// memory filter required pivasWorklist from rawPivasWorklist by pivasPrepMethodId
				List<PivasWorklist> pivasWorklistList = new ArrayList<PivasWorklist>();
				// rawPivasWorklist is distinct
				for (PivasWorklist rawPivasWorklist : rawPivasWorklistList) {
					if (pivasPrepMethodIdSet.contains(rawPivasWorklist.getPivasPrepMethodId())) {
						pivasWorklistList.add(rawPivasWorklist);
					}
				}
				
				return pivasWorklistList;
			} else {
				// itemCode / manufCode / batchNum search but no result
				return null;
			}
		} else {
			// not itemCode / manufCode / batchNum search
			// hkidCaseNum search alone or drugKey search alone
			return rawPivasWorklistList;
		}
	}
	
	// for PivasConsumptionRpt, This function is called only when satellite flag is BLANK in front end
	@SuppressWarnings("unchecked")
	private List<PivasBatchPrep> retrievePivasBatchPrepList(Date manufFromDate, Date manufToDate, 
															PivasRptItemCatFilterOption pivasRptItemCatFilterOption, 
															String itemCode, Boolean drugKeyFlag, String manufCode, String batchNum){
		
		boolean isItemCodeSearch = StringUtils.isNotBlank(itemCode);
		boolean isDrugKeySearch = false;
		if (drugKeyFlag != null && Boolean.TRUE.equals(drugKeyFlag)) {
			isDrugKeySearch = true;
		}
		boolean isManufCodeSearch = StringUtils.isNotBlank(manufCode);
		boolean isBatchNumSearch = StringUtils.isNotBlank(batchNum);
		
		// use sub-query instead of distinct query because pivasBatchPrep.labelXml is in CLOB type
		// distinct does not support the table with CLOB type
		StringBuilder sb = new StringBuilder(
				"select s from PivasBatchPrep s" + // 20171023 index check : PivasBatchPrep.lotNum : PK_PIVAS_BATCH_PREP
				" where s.lotNum in (" +
				"select o.pivasBatchPrep.lotNum from PivasBatchPrepItem o" + // 20171023 index check : PivasBatchPrepItem.pivasBatchPrep : FK_PIVAS_BATCH_PREP_ITEM_01
				" where o.pivasBatchPrep.hospCode = :hospCode" +
				" and o.pivasBatchPrep.status = :status"
				);
		
		if (manufToDate != null) {
			// search by supply date range
			sb.append(" and o.pivasBatchPrep.manufactureDate >= :manufFromDate and o.pivasBatchPrep.manufactureDate < :manufToDate");
		} else {
			// search by specific supply date with supply time
			sb.append(" and o.pivasBatchPrep.manufactureDate = :manufFromDate");
		}
		
		if ( isItemCodeSearch && isDrugKeySearch && pivasRptItemCatFilterOption != null && pivasRptItemCatFilterOption == PivasRptItemCatFilterOption.Drug ) {

			if (currPivasReportType == PivasReportType.PivasWorkloadSummaryBatchRpt) {
				sb.append(" and (o.drugKey in :drugKeyList or o.pivasBatchPrep.drugKey in :drugKeyList)");
			} else {
				sb.append(" and o.drugKey in :drugKeyList");
			}
		}
		
		// drug key search query for PivasBatchPrep is in buildPivasRptItemCatFilterOptionQuery
		if (pivasRptItemCatFilterOption != null && ((isItemCodeSearch && !isDrugKeySearch) || isManufCodeSearch || isBatchNumSearch)) {
			sb = buildPivasRptItemCatFilterOptionQuery(sb, PivasRptDispMethod.BatchDisp, pivasRptItemCatFilterOption, 
														isItemCodeSearch, isDrugKeySearch, isManufCodeSearch, isBatchNumSearch);
		}
		
		// bracket for sub-query
		sb.append(")");
		
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("hospCode", workstore.getHospCode());
		query.setParameter("status", PivasBatchPrepStatus.Printed);
		
		
		if (manufToDate != null) {
			// search by supply date range
			query.setParameter("manufFromDate", manufFromDate, TemporalType.DATE);
			DateMidnight toDate = new DateMidnight(manufToDate).plusDays(1);
			query.setParameter("manufToDate", toDate.toDate(), TemporalType.DATE);
		} else { 
			// search by specific supply date with supply time
			query.setParameter("manufFromDate", manufFromDate, TemporalType.TIMESTAMP);
		}
		
		if (pivasRptItemCatFilterOption != null && ((isItemCodeSearch && !isDrugKeySearch) || isManufCodeSearch || isBatchNumSearch)) {
			if (isItemCodeSearch && !isDrugKeySearch) {
				query.setParameter("itemCode", itemCode);
			}
				 
			if (isManufCodeSearch) {
				query.setParameter("manufCode", manufCode);
			}
					
			if (isBatchNumSearch) {
				query.setParameter("batchNum", batchNum);
			}
		}
		
		List <PivasBatchPrep> pivasBatchPrepList = null;
		if (isItemCodeSearch && isDrugKeySearch && pivasRptItemCatFilterOption != null && pivasRptItemCatFilterOption == PivasRptItemCatFilterOption.Drug) {
			// drugKey search
			
			List<String> itemCodeList = new ArrayList<String>();
			// isItemCodeSearch ensures itemCode is not null
			itemCodeList.add(itemCode);
			// returned drugKeyList is distinct
			// PivasDrug will never be physically removed from the table
			// drugKeyList retrieved is all drugKey for this itemCode while pivasPrep.drugKey is restricted within the date range
			List<Integer> drugKeyList = dmsPmsServiceProxy.retrievePivasDrugKeyListByItemCode(workstore.getWorkstoreGroup().getWorkstoreGroupCode(), itemCodeList);
			pivasBatchPrepList = QueryUtils.splitExecute(query, "drugKeyList", drugKeyList);
		} else { // normal flow
			pivasBatchPrepList = query.getResultList();
		}
		
		return pivasBatchPrepList;
	}
	
	// pivasRptItemCatFilterOption is checked null before calling this function
	private StringBuilder buildPivasRptItemCatFilterOptionQuery(StringBuilder sb, 
										PivasRptDispMethod pivasRptDispMethod, PivasRptItemCatFilterOption pivasRptItemCatFilterOption, 
										boolean isItemCodeSearch, boolean isDrugKeySearch, boolean isManufCodeSearch, boolean isBatchNumSearch) {
		
		String diluentItemCodeQuery = null;
		String diluentManufCodeQuery = null;
		String diluentBatchNumQuery = null;
		// pivasRptDispMethod will not be null or All
		if (pivasRptDispMethod == PivasRptDispMethod.PatientDisp) {
			diluentItemCodeQuery = PIVAS_PREP_METHOD_DILUENT_ITEM_CODE_QUERY;
			diluentManufCodeQuery = PIVAS_PREP_METHOD_DILUENT_MANUF_CODE_QUERY;
			diluentBatchNumQuery = PIVAS_PREP_METHOD_DILUENT_BATCH_NUM_QUERY;
		} else { // PivasRptDispMethod.BatchDisp
			diluentItemCodeQuery = PIVAS_BATCH_PREP_DILUENT_ITEM_CODE_QUERY;
			diluentManufCodeQuery = PIVAS_BATCH_PREP_DILUENT_MANUF_CODE_QUERY;
			diluentBatchNumQuery = PIVAS_BATCH_PREP_DILUENT_BATCH_NUM_QUERY;
		}
		
		StringBuilder pivasItemCatSb = new StringBuilder();
		List<String> drugQueryList = new ArrayList<String>();
		List<String> solventQueryList = new ArrayList<String>();
		List<String> diluentQueryList = new ArrayList<String>();
		
		// drug query
		if (pivasRptItemCatFilterOption == PivasRptItemCatFilterOption.Drug || pivasRptItemCatFilterOption == PivasRptItemCatFilterOption.All) {
			// drugKey is filtered in retrievePivasWorklistList/retrievePivasBatchPrepList by PivasPrep/PivasBatchPrep
			// in drugKey search, itemCode will not be used to search in here
			if (isItemCodeSearch && !isDrugKeySearch) {

				if (currPivasReportType == PivasReportType.PivasWorkloadSummaryBatchRpt) {
					drugQueryList.add("(" + PIVAS_BATCH_PREP_TARGET_ITEM_CODE_QUERY + " or " + DRUG_ITEM_CODE_QUERY + ")");
				} else {
					drugQueryList.add(DRUG_ITEM_CODE_QUERY);
				}
			}
			if (isManufCodeSearch) {
				drugQueryList.add(DRUG_MANUF_CODE_QUERY);
			}
			if (isBatchNumSearch) {
				drugQueryList.add(DRUG_BATCH_NUM_QUERY);
			}
			pivasItemCatSb = buildPivasItemCatSbByQueryList(pivasItemCatSb, drugQueryList);
		}
		
		if (pivasRptItemCatFilterOption == PivasRptItemCatFilterOption.SolventAndDiluent || pivasRptItemCatFilterOption == PivasRptItemCatFilterOption.All) {
			// solvent query
			if (isItemCodeSearch) {
				solventQueryList.add(SOLVENT_ITEM_CODE_QUERY);
			}
			if (isManufCodeSearch) {
				solventQueryList.add(SOLVENT_MANUF_CODE_QUERY);
			}
			if (isBatchNumSearch) {
				solventQueryList.add(SOLVENT_BATCH_NUM_QUERY);
			}
			pivasItemCatSb = buildPivasItemCatSbByQueryList(pivasItemCatSb, solventQueryList);
			
			// diluent query
			if (isItemCodeSearch) {
				diluentQueryList.add(diluentItemCodeQuery);
			}
			if (isManufCodeSearch) {
				diluentQueryList.add(diluentManufCodeQuery);
			}
			if (isBatchNumSearch) {
				diluentQueryList.add(diluentBatchNumQuery);
			}
			pivasItemCatSb = buildPivasItemCatSbByQueryList(pivasItemCatSb, diluentQueryList);
		}
		
		
		if (pivasItemCatSb.length() > 0) {
			sb.append(" and (" + pivasItemCatSb + ")");
		}
		
		return sb;
	}
	
	// join own itemCat query to pivasItemCatSb for later appending to main query
	private StringBuilder buildPivasItemCatSbByQueryList(StringBuilder pivasItemCatSb, List<String> queryList) {
		if (!queryList.isEmpty()) {
			// no need "or" for first element in itemCatSb
			if (pivasItemCatSb.length() > 0) {
				pivasItemCatSb.append(" or ");
			}
			// delimiter will not be added when there is one element only
			// extra bracket pairs will not affect query because AND relation is only for same itemCat
			pivasItemCatSb.append("(" + StringUtils.join(queryList.iterator(), " and ") + ")");
		}
		return pivasItemCatSb;
	}
	
	@SuppressWarnings("unchecked")
	private List<DeliveryItem> retrieveDeliveryItemListByPivasWorklist(List<PivasWorklist> pivasWorklistList) {
		
		Set<Long> pivasWorklistIdSet = new HashSet<Long>();
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			pivasWorklistIdSet.add(pivasWorklist.getId());
		}
		
		return QueryUtils.splitExecute(em.createQuery(
				"select o from DeliveryItem o" + // 20161129 index check : DeliveryItem.pivasWorklistId : I_DELIVERY_ITEM_02
				" where o.pivasWorklistId in :pivasWorklistIdSet")
				,"pivasWorklistIdSet", pivasWorklistIdSet);
	}
	

	private Pair<List<PivasBatchPrep>,List<PivasBatchPrep>> buildPivasBatchPrepListWithPivasFormulaTypePair(List<PivasBatchPrep> pivasBatchPrepList) {
		List<PivasBatchPrep> pivasNormalFormulaBatchPrepList = new ArrayList<PivasBatchPrep>();
		List<PivasBatchPrep> pivasMergeFormulaBatchPrepList = new ArrayList<PivasBatchPrep>();
			
		for (PivasBatchPrep pivasBatchPrep: pivasBatchPrepList) {
			if (pivasBatchPrep.getPivasFormulaType() != PivasFormulaType.Merge) {
				pivasNormalFormulaBatchPrepList.add(pivasBatchPrep);
			} else {
				pivasMergeFormulaBatchPrepList.add(pivasBatchPrep);
			}
		}
		return new Pair<List<PivasBatchPrep>,List<PivasBatchPrep>>(pivasNormalFormulaBatchPrepList, pivasMergeFormulaBatchPrepList);
	}
	
	
	private PivasRptHdr constructPivasRptHdr(Date supplyDateFrom, Date supplyDateTo, YesNoBlankFlag satelliteFlag, 
											String hkidCaseNum, PivasRptDispMethod pivasRptDispMethod, PivasRptItemCatFilterOption pivasRptItemCatFilterOption,
											String itemCode, Boolean drugKeyFlag, String manufCode, String batchNum) {

		PivasRptHdr pivasRptHdr = new PivasRptHdr();
		
		pivasRptHdr.setSupplyDateFrom(supplyDateFrom);
		if (supplyDateTo != null) {
			// Search in Supply Date Range
			pivasRptHdr.setSupplyDateTo(supplyDateTo);
		} else {
			// Search in Supply Date From and Supply Time
			pivasRptHdr.setSupplyDateTo(null);
		}
		
		// satellite criteria only shows in printed report when PIVAS_SATELLITE_ENABLED is "Y"
		if (satelliteFlag != null && PIVAS_SATELLITE_ENABLED.get(false)) {
			pivasRptHdr.setSatelliteFlag(satelliteFlag);
		} else {
			pivasRptHdr.setSatelliteFlag(null);
		}
		
		pivasRptHdr.setPivasRptItemCatFilterOption(pivasRptItemCatFilterOption);
		pivasRptHdr.setHkidCaseNum(hkidCaseNum);
		pivasRptHdr.setPivasRptDispMethod(pivasRptDispMethod);
		pivasRptHdr.setItemCode(itemCode);
		pivasRptHdr.setDrugKeyFlag(drugKeyFlag);
		pivasRptHdr.setManufCode(manufCode);
		pivasRptHdr.setBatchNum(batchNum);
		
		return pivasRptHdr;
	}
	
	// set report and excel export header criteria
	private void constructPivasRptParameters(Date supplyDateFrom, Date supplyDateTo, YesNoBlankFlag satelliteFlag, 
											String hkidCaseNum, PivasRptDispMethod pivasRptDispMethod, PivasRptItemCatFilterOption pivasRptItemCatFilterOption,
											String itemCode, Boolean drugKeyFlag, String manufCode, String batchNum) {
		
		parameters.clear();
		parameters.put("hospCode", workstore.getHospCode());
		parameters.put("workstoreCode", workstore.getWorkstoreCode());
		
		// report export header parameter
		parameters.put("supplyDateFrom", supplyDateFrom);
		parameters.put("supplyDateTo", supplyDateTo);
		
		if (pivasRptItemCatFilterOption != null && currPivasReportType != PivasReportType.PivasWorkloadSummaryPatientRpt && currPivasReportType != PivasReportType.PivasWorkloadSummaryBatchRpt) {
			parameters.put("pivasRptItemCatFilterOption.displayValue", pivasRptItemCatFilterOption.getDisplayValue());
		}
		
		// satellite criteria only shows in printed report when PIVAS_SATELLITE_ENABLED is "Y"
		// satellite criteria is not shown in exported report if satellite is BLANK in front-end
		if (satelliteFlag != null && satelliteFlag != YesNoBlankFlag.Blank && PIVAS_SATELLITE_ENABLED.get(false)) {
			parameters.put("satelliteFlag.dataValue", satelliteFlag.getDataValue());
		} else {
			parameters.put("satelliteFlag.dataValue", null);
		}
		
		if (pivasRptDispMethod != null && currPivasReportType == PivasReportType.PivasConsumptionReport) {
			parameters.put("pivasRptDispMethod.displayValue", pivasRptDispMethod.getDisplayValue());
		}

		if (StringUtils.isNoneBlank(hkidCaseNum) && currPivasReportType == PivasReportType.PivasPatientDispReport) {
			parameters.put("hkidCaseNum", hkidCaseNum);
		}
		
		// only PivasConsumptionReport does not support itemCode and drugKey search
		if (currPivasReportType != PivasReportType.PivasConsumptionReport) {
			if (StringUtils.isNoneBlank(itemCode)) {
				parameters.put("itemCode", itemCode);
				parameters.put("drugKeyFlag", drugKeyFlag);
			}
		}
		
		if (currPivasReportType == PivasReportType.PivasPatientDispReport || currPivasReportType == PivasReportType.PivasBatchDispReport) {
			if (StringUtils.isNoneBlank(manufCode)) {
				parameters.put("manufCode", manufCode);
			}
			
			if (StringUtils.isNoneBlank(batchNum)) {
				parameters.put("batchNum", batchNum);
			}
		}
	}

	public void generatePivasRpt() {
		printAgent.renderAndRedirect(new RenderJob(
				currPivasReportType.getDataValue(), 
				new PrintOption(), 
				parameters, 
				reportObj));
	}
	
	public void printPivasRpt(){
		printAgent.renderAndPrint(new RenderAndPrintJob(
				currPivasReportType.getDataValue(), 
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				parameters, 
				reportObj));
	}
	
	public void setPivasRptPassword(String password){
		exportPassword = password;
	}
	
	public void exportPivasRpt() throws IOException, ZipException {	
		
		PrintOption po = new PrintOption();
		po.setDocType(ReportProvider.XLS);
		
		if (currPivasReportType == PivasReportType.PivasPatientDispReport) {
			printAgent.zipAndRedirect(new RenderJob(
											xlsName, 
											po, 
											parameters, 
											exportObj), exportPassword);
		} else {
			printAgent.renderAndRedirect(new RenderJob (
					xlsName, 
					po, 
					parameters, 
					exportObj));
		}
	}

	private Date resetDateHours(Date date) {
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(date);
    	cal.set(Calendar.HOUR_OF_DAY, 0);
    	cal.set(Calendar.MINUTE, 0);
    	cal.set(Calendar.SECOND, 0);
    	cal.set(Calendar.MILLISECOND, 0);
    	return cal.getTime();
	}
	
	private <N extends Number> String convertToString(N value) {
		if ( value == null ) {
			return StringUtils.EMPTY;
		}
		String str = null;
		
		if (value instanceof Integer) {
			str = value.toString();
		} else if (value instanceof Long) {
			str = value.toString();
		} else if (value instanceof Double) {
			Double decimalValue =  (Double) value - value.intValue();
			if ( decimalValue == 0 ) {
				str =  Integer.toString(value.intValue());
			} else {
				str = value.toString();
			}
		} else if (value instanceof BigDecimal) {
			Double decimalValue =  value.doubleValue() - value.intValue();
			if ( decimalValue == 0 ) {
				str = Integer.toString(value.intValue());
			} else {
				str = ((BigDecimal) value).stripTrailingZeros().toPlainString();
			}
		}
		
		return str;
	}
	
	private static class PivasConsumptionRptDtlComparator implements Comparator<PivasConsumptionRptDtl>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(PivasConsumptionRptDtl pivasConsumptionRptDtl1, PivasConsumptionRptDtl pivasConsumptionRptDtl2) {
			int compareResult = 0;

			compareResult = new CompareToBuilder()
											.append( pivasConsumptionRptDtl1.getPivasItemCat(), pivasConsumptionRptDtl2.getPivasItemCat() )
											.append( pivasConsumptionRptDtl1.getItemCode(), pivasConsumptionRptDtl2.getItemCode() )
											.append( pivasConsumptionRptDtl1.getModu(), pivasConsumptionRptDtl2.getModu() )
											.append( pivasConsumptionRptDtl1.getManufCode(), pivasConsumptionRptDtl2.getManufCode() )
											.append( pivasConsumptionRptDtl1.getBatchNum(), pivasConsumptionRptDtl2.getBatchNum() )
											.append( pivasConsumptionRptDtl1.getExpiryDate(), pivasConsumptionRptDtl2.getExpiryDate() )
											.toComparison();
			
			return compareResult;
		}
	}
	
	private static class PivasPatientDispRptDtlComparator implements Comparator<PivasPatientDispRptDtl>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(PivasPatientDispRptDtl pivasPatientDispRptDtl1, PivasPatientDispRptDtl pivasPatientDispRptDtl2) {
			int compareResult = 0;

			compareResult = new CompareToBuilder()
											.append( pivasPatientDispRptDtl1.getSupplyDate(), pivasPatientDispRptDtl2.getSupplyDate() )
											.append( pivasPatientDispRptDtl1.getWard(), pivasPatientDispRptDtl2.getWard() )
											.append( pivasPatientDispRptDtl1.getCaseNum(), pivasPatientDispRptDtl2.getCaseNum() )
											.append( pivasPatientDispRptDtl1.getPrepDesc(), pivasPatientDispRptDtl2.getPrepDesc() )
											.toComparison();
			
			return compareResult;
		}
	}
	
	private static class PivasBatchDispRptDtlComparator implements Comparator<PivasBatchDispRptDtl>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(PivasBatchDispRptDtl pivasBatchDispRptDtl1, PivasBatchDispRptDtl pivasBatchDispRptDtl2) {
			int compareResult = 0;

			compareResult = new CompareToBuilder()
											// pivasFormulaType is in descending
											.append( pivasBatchDispRptDtl2.getPivasFormulaType().getDataValue(), pivasBatchDispRptDtl1.getPivasFormulaType().getDataValue())
											.append( pivasBatchDispRptDtl1.getSupplyDate(), pivasBatchDispRptDtl2.getSupplyDate() )
											.append( pivasBatchDispRptDtl1.getFormulaPrintName(), pivasBatchDispRptDtl2.getFormulaPrintName() )
											.append( pivasBatchDispRptDtl1.getLotNum(), pivasBatchDispRptDtl2.getLotNum() )
											.toComparison();
			
			return compareResult;
		}
	}


	
	private static class PivasBatchDispRptRawMaterialDtlComparator implements Comparator<PivasBatchDispRptRawMaterialDtl>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(PivasBatchDispRptRawMaterialDtl pivasBatchDispRptRawMaterialDtl1, PivasBatchDispRptRawMaterialDtl pivasBatchDispRptRawMaterialDtl2) {
			int compareResult = 0;

			compareResult = new CompareToBuilder()
											.append( pivasBatchDispRptRawMaterialDtl1.getSortSeq(), pivasBatchDispRptRawMaterialDtl2.getSortSeq() )
											.append( pivasBatchDispRptRawMaterialDtl1.getSolventDesc(), pivasBatchDispRptRawMaterialDtl2.getSolventDesc() )
											.toComparison();
			
			return compareResult;
		}
	}	
	private static class PivasWorkloadSummaryPatientRptDtlComparator implements Comparator<PivasWorkloadSummaryPatientRptDtl>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(PivasWorkloadSummaryPatientRptDtl pivasWorkloadSummaryPatientRptDtl1, PivasWorkloadSummaryPatientRptDtl pivasWorkloadSummaryPatientRptDtl2) {
			int compareResult = 0;

			compareResult = new CompareToBuilder()
											// supplyDate is used in export only
											.append( pivasWorkloadSummaryPatientRptDtl1.getItemCode(), pivasWorkloadSummaryPatientRptDtl2.getItemCode() )
											.append( pivasWorkloadSummaryPatientRptDtl1.getPivasFormulaDesc(), pivasWorkloadSummaryPatientRptDtl2.getPivasFormulaDesc() )
											.append( pivasWorkloadSummaryPatientRptDtl1.getWard(), pivasWorkloadSummaryPatientRptDtl2.getWard() )
											.toComparison();
			
			return compareResult;
		}
	}
	
	private static class PivasWorkloadSummaryPatientXlsComparator implements Comparator<PivasWorkloadSummaryPatientRptDtl>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(PivasWorkloadSummaryPatientRptDtl pivasWorkloadSummaryPatientRptXls1, PivasWorkloadSummaryPatientRptDtl pivasWorkloadSummaryPatientRptXls2) {
			int compareResult = 0;

			compareResult = new CompareToBuilder()
											// supplyDate is used in export only
											.append( pivasWorkloadSummaryPatientRptXls1.getSupplyDate(), pivasWorkloadSummaryPatientRptXls2.getSupplyDate() )
											.append( pivasWorkloadSummaryPatientRptXls1.getItemCode(), pivasWorkloadSummaryPatientRptXls2.getItemCode() )
											.append( pivasWorkloadSummaryPatientRptXls1.getPivasFormulaDesc(), pivasWorkloadSummaryPatientRptXls2.getPivasFormulaDesc() )
											.append( pivasWorkloadSummaryPatientRptXls1.getWard(), pivasWorkloadSummaryPatientRptXls2.getWard() )
											.toComparison();
			
			return compareResult;
		}
	}
	
	private static class PivasWorkloadSummaryPatientRptWardSummaryDtlComparator implements Comparator<PivasWorkloadSummaryPatientRptWardSummaryDtl>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(PivasWorkloadSummaryPatientRptWardSummaryDtl pivasWorkloadSummaryPatientRptWardSummaryDtl1, PivasWorkloadSummaryPatientRptWardSummaryDtl pivasWorkloadSummaryPatientRptWardSummaryDtl2) {
			int compareResult = 0;

			compareResult = new CompareToBuilder()
											.append( pivasWorkloadSummaryPatientRptWardSummaryDtl1.getWard(), pivasWorkloadSummaryPatientRptWardSummaryDtl2.getWard() )
											.toComparison();
			
			return compareResult;
		}
	}
	
	private static class PivasWorkloadSummaryBatchRptNormalDtlComparator implements Comparator<PivasWorkloadSummaryBatchRptDtl>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(PivasWorkloadSummaryBatchRptDtl pivasWorkloadSummaryBatchRptDtl1, PivasWorkloadSummaryBatchRptDtl pivasWorkloadSummaryBatchRptDtl2) {
			int compareResult = 0;

			compareResult = new CompareToBuilder()
											// supplyDate is used in export only
											.append( pivasWorkloadSummaryBatchRptDtl1.getItemCode(), pivasWorkloadSummaryBatchRptDtl2.getItemCode() )
											.append( pivasWorkloadSummaryBatchRptDtl1.getPivasFormulaDesc(), pivasWorkloadSummaryBatchRptDtl2.getPivasFormulaDesc() )
											.toComparison();
			
			return compareResult;
		}
	}

	private static class PivasWorkloadSummaryBatchRptMergeDtlComparator implements Comparator<PivasWorkloadSummaryBatchRptDtl>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(PivasWorkloadSummaryBatchRptDtl pivasWorkloadSummaryBatchRptDtl1, PivasWorkloadSummaryBatchRptDtl pivasWorkloadSummaryBatchRptDtl2) {
			int compareResult = 0;

			compareResult = new CompareToBuilder()
											// supplyDate is used in export only
											.append( pivasWorkloadSummaryBatchRptDtl1.getPivasFormulaDesc(), pivasWorkloadSummaryBatchRptDtl2.getPivasFormulaDesc() )
											.toComparison();
			
			return compareResult;
		}
	}	
	
	private static class PivasWorkloadSummaryBatchXlsComparator implements Comparator<PivasWorkloadSummaryBatchRptDtl>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(PivasWorkloadSummaryBatchRptDtl pivasWorkloadSummaryBatchRptXls1, PivasWorkloadSummaryBatchRptDtl pivasWorkloadSummaryBatchRptXls2) {
			int compareResult = 0;

			compareResult = new CompareToBuilder()
											// supplyDate is used in export only
											// pivasFormulaType is in descending
											.append( pivasWorkloadSummaryBatchRptXls1.getSupplyDate(), pivasWorkloadSummaryBatchRptXls2.getSupplyDate() )
											.append( pivasWorkloadSummaryBatchRptXls2.getPivasFormulaType().getDataValue(), pivasWorkloadSummaryBatchRptXls1.getPivasFormulaType().getDataValue() )
											.append( pivasWorkloadSummaryBatchRptXls1.getItemCode(), pivasWorkloadSummaryBatchRptXls2.getItemCode() )
											.append( pivasWorkloadSummaryBatchRptXls1.getPivasFormulaDesc(), pivasWorkloadSummaryBatchRptXls2.getPivasFormulaDesc() )
											.toComparison();
			
			return compareResult;
		}
	}
	
	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}
	
	@Remove
	public void destroy() {
		if (parameters != null) {
			parameters = null;
		}
		
		if (pivasConsumptionRptDtlList != null) {
			pivasConsumptionRptDtlList = null;
		}
		
		if (pivasConsumptionRptList != null) {
			pivasConsumptionRptList = null;
		}
		
		if (pivasPatientDispRptDtlList != null) {
			pivasPatientDispRptDtlList = null;
		}
		
		if (pivasPatientDispRptList != null) {
			pivasPatientDispRptList = null;
		}
		
		if (pivasBatchDispRptDtlList != null) {
			pivasBatchDispRptDtlList = null;
		}

		if (pivasBatchDispRptXlsDtlList != null) {
			pivasBatchDispRptXlsDtlList = null;
		}
		
		if (pivasBatchDispRptList != null) {
			pivasBatchDispRptList = null;
		}
		
		if (pivasWorkloadSummaryPatientRptWardSummaryDtlList != null) {
			pivasWorkloadSummaryPatientRptWardSummaryDtlList = null;
		}
		
		if (pivasWorkloadSummaryPatientRptDtlList != null) {
			pivasWorkloadSummaryPatientRptDtlList = null;
		}
		
		if (pivasWorkloadSummaryPatientXlsList != null) {
			pivasWorkloadSummaryPatientXlsList = null;
		}
		
		if (pivasWorkloadSummaryPatientRptList != null) {
			pivasWorkloadSummaryPatientRptList = null;
		}
		
		if (pivasWorkloadSummaryBatchRptNormalDtlList != null) {
			pivasWorkloadSummaryBatchRptNormalDtlList = null;
		}
		
		if (pivasWorkloadSummaryBatchRptMergeDtlList != null) {
			pivasWorkloadSummaryBatchRptMergeDtlList = null;
		}	
		
		if (pivasWorkloadSummaryBatchXlsList != null) {
			pivasWorkloadSummaryBatchXlsList = null;
		}
		
		if (pivasWorkloadSummaryBatchRptList != null) {
			pivasWorkloadSummaryBatchRptList = null;
		}
	}

}
