package hk.org.ha.model.pms.vo.charging;

import hk.org.ha.model.pms.persistence.disp.Invoice;

import java.io.Serializable;
import java.util.Date;

public class FcsSfiInvoiceForceProceedInfo extends FcsSfiInvoiceInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	// manual invoice
	private String manualInvoiceNum;
	private Date forceProceedDate;
	
	// manual receipt
	private String manualReceiptNum;
	
	// force proceed reason
	private Boolean forceProceedFlag;
	private String forceProceedUser;
	private String forceProceedCode;
	
	public FcsSfiInvoiceForceProceedInfo() {
		// default constructor
	}
	
	public FcsSfiInvoiceForceProceedInfo(Invoice invoice) {
		super(invoice);
		
		manualInvoiceNum = invoice.getManualInvoiceNum();
		forceProceedDate = invoice.getForceProceedDate();

		manualReceiptNum = invoice.getManualReceiptNum();
		
		forceProceedFlag = invoice.getForceProceedFlag();
		forceProceedUser = invoice.getForceProceedUser();
		forceProceedCode = invoice.getForceProceedCode();
	}
	
	public String getManualInvoiceNum() {
		return manualInvoiceNum;
	}
	public void setManualInvoiceNum(String manualInvoiceNum) {
		this.manualInvoiceNum = manualInvoiceNum;
	}
	public Date getForceProceedDate() {
		return forceProceedDate;
	}
	public void setForceProceedDate(Date forceProceedDate) {
		this.forceProceedDate = forceProceedDate;
	}
	public String getManualReceiptNum() {
		return manualReceiptNum;
	}
	public void setManualReceiptNum(String manualReceiptNum) {
		this.manualReceiptNum = manualReceiptNum;
	}
	public Boolean getForceProceedFlag() {
		return forceProceedFlag;
	}
	public void setForceProceedFlag(Boolean forceProceedFlag) {
		this.forceProceedFlag = forceProceedFlag;
	}
	public String getForceProceedUser() {
		return forceProceedUser;
	}
	public void setForceProceedUser(String forceProceedUser) {
		this.forceProceedUser = forceProceedUser;
	}
	public String getForceProceedCode() {
		return forceProceedCode;
	}
	public void setForceProceedCode(String forceProceedCode) {
		this.forceProceedCode = forceProceedCode;
	}
}
