package hk.org.ha.model.pms.vo.vetting;

import hk.org.ha.model.pms.persistence.FmEntity;
import hk.org.ha.model.pms.vo.rx.RxDrug;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class FmIndication implements Serializable {

	private static final long serialVersionUID = 1L;

	private RxDrug rxDrug;
	
	private FmEntity fmEntity;

	private Date authDate;

	private String prescibingDoctorName;
	
	public RxDrug getRxDrug() {
		return rxDrug;
	}

	public void setRxDrug(RxDrug rxDrug) {
		this.rxDrug = rxDrug;
	}
	
	public FmEntity getFmEntity() {
		return fmEntity;
	}

	public void setFmEntity(FmEntity fmEntity) {
		this.fmEntity = fmEntity;
	}

	public Date getAuthDate() {
		return authDate;
	}

	public void setAuthDate(Date authDate) {
		this.authDate = authDate;
	}

	public String getPrescibingDoctorName() {
		return prescibingDoctorName;
	}
	
	public void setPrescibingDoctorName(String prescibingDoctorName) {
		this.prescibingDoctorName = prescibingDoctorName;
	}
}
