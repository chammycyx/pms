package hk.org.ha.model.pms.biz.reftable;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
import hk.org.ha.model.pms.persistence.reftable.WardGroup;

import java.util.List;

import javax.ejb.Local;

@Local
public interface WardGroupManagerLocal {
	List<WardGroup> retrieveWardGroupList(WorkstoreGroup workstoreGroup);
}
