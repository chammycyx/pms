package hk.org.ha.model.pms.biz.report;

import java.util.List;

import hk.org.ha.model.pms.vo.report.PatientListRpt;
import hk.org.ha.model.pms.vo.report.PatientListRptInfo;

import javax.ejb.Local;

@Local
public interface PatientListRptServiceLocal {
	
	PatientListRptInfo retrievePatientListRptInfo();
	
	List<PatientListRpt> retrievePatientList(List<String> wardCodeList);
	
	void generatePatientListRpt();

	void printPatientListRpt();
	
	boolean isRetrieveSuccess();
	
	void destroy();
	
}
