package hk.org.ha.model.pms.persistence.reftable;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.reftable.MachineType;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "MACHINE")
@Customizer(AuditCustomizer.class)
public class Machine extends VersionEntity {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "machineSeq")
	/*lower the inital value to avoid the auto delete by using clean_db() */
	@SequenceGenerator(name = "machineSeq", sequenceName = "SQ_MACHINE", initialValue = 1000000)
	private Long id;
	
	@Converter(name = "Machine.type", converterClass = MachineType.Converter.class)
    @Convert("Machine.type")
	@Column(name = "TYPE", nullable = false, length = 1)
	private MachineType type;
	
	@Column(name = "HOST_NAME", length = 15)
	private String hostName;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "PICK_STATION_ID", nullable = false)
	private PickStation pickStation;

	@OneToMany(mappedBy = "machine")
	private List<OperationWorkstation> operationWorkstationList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MachineType getType() {
		return type;
	}

	public void setType(MachineType type) {
		this.type = type;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public PickStation getPickStation() {
		return pickStation;
	}

	public void setPickStation(PickStation pickStation) {
		this.pickStation = pickStation;
	}

	public void setOperationWorkstationList(List<OperationWorkstation> operationWorkstationList) {
		this.operationWorkstationList = operationWorkstationList;
	}

	public List<OperationWorkstation> getOperationWorkstationList() {
		return operationWorkstationList;
	}

}
