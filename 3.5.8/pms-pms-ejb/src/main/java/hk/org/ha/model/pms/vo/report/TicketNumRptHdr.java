package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class TicketNumRptHdr implements Serializable {

	private static final long serialVersionUID = 1L;

	private String hospCode;
	
	private String workstoreCode;
	
	private String activeProfileName;
	
	private Date TicketDate;

	private String ticketNum;
	
	private String dispOrderStatus;
	
	private String patName;
	
	private String patNameChi;
	
	private String hkid;
	
	private String caseNum;
	
	private Integer issueWindowNum;

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public String getActiveProfileName() {
		return activeProfileName;
	}

	public void setActiveProfileName(String activeProfileName) {
		this.activeProfileName = activeProfileName;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getPatNameChi() {
		return patNameChi;
	}

	public void setPatNameChi(String patNameChi) {
		this.patNameChi = patNameChi;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public void setIssueWindowNum(Integer issueWindowNum) {
		this.issueWindowNum = issueWindowNum;
	}

	public Integer getIssueWindowNum() {
		return issueWindowNum;
	}

	public void setDispOrderStatus(String dispOrderStatus) {
		this.dispOrderStatus = dispOrderStatus;
	}

	public String getDispOrderStatus() {
		return dispOrderStatus;
	}

	public void setTicketDate(Date ticketDate) {
		TicketDate = ticketDate;
	}

	public Date getTicketDate() {
		return TicketDate;
	}
}
