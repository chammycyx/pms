package hk.org.ha.model.pms.biz.drug;
import hk.org.ha.model.pms.dms.vo.PreparationSearchCriteria;

import javax.ejb.Local;

@Local
public interface PreparationPropertyListServiceLocal {

	void retrievePreparationPropertyList(PreparationSearchCriteria criteria);
	
	void destroy();	
}
