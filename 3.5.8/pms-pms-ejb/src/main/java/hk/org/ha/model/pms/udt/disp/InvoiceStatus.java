package hk.org.ha.model.pms.udt.disp;

import java.util.Arrays;
import java.util.List;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum InvoiceStatus implements StringValuedEnum {

	None("-", "None"),
	Outstanding("O", "Outstanding"),
	Settled("S", "Settled"),
	Void("V", "Void"),
	SysDeleted("X", "SysDeleted");
	
    private final String dataValue;
    private final String displayValue;
    
    public static final List<InvoiceStatus> None_Outstanding = Arrays.asList(None, Outstanding);
    public static final List<InvoiceStatus> Outstanding_Settle = Arrays.asList(Outstanding, Settled);
    public static final List<InvoiceStatus> None_Outstanding_Settle = Arrays.asList(None, Outstanding, Settled);
    public static final List<InvoiceStatus> ActiveInvoiceStatus = Arrays.asList(None, Outstanding, Settled, Void);
    public static final List<InvoiceStatus> Outstanding_Settle_Void = Arrays.asList(Outstanding, Settled, Void);

    InvoiceStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    } 
    
    public static InvoiceStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(InvoiceStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<InvoiceStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<InvoiceStatus> getEnumClass() {
    		return InvoiceStatus.class;
    	}
    }
}
