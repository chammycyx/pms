package hk.org.ha.model.pms.biz.reftable;

import flexjson.JSONSerializer;
import flexjson.transformer.DateTransformer;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.pas.PasException;
import hk.org.ha.model.pms.cache.PasSpecialtyCacherInf;
import hk.org.ha.model.pms.dms.persistence.PmsFormNum;
import hk.org.ha.model.pms.dms.persistence.PmsRouteFormSortSpec;
import hk.org.ha.model.pms.dms.persistence.PmsRouteFormSortSpecPK;
import hk.org.ha.model.pms.dms.persistence.PmsRouteNum;
import hk.org.ha.model.pms.dms.persistence.PmsRouteSortSpec;
import hk.org.ha.model.pms.dms.persistence.PmsRouteSortSpecPK;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.patient.PasSpecialtyType;
import hk.org.ha.model.pms.vo.patient.PasSpecialty;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("routeFormDisplaySeqListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class RouteFormDisplaySeqListServiceBean implements RouteFormDisplaySeqListServiceLocal {
	
	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
	
	private static final JSONSerializer serializer = new JSONSerializer().transform(new DateTransformer(DATE_FORMAT), Date.class);
	
	@In
	private AuditLogger auditLogger;
	
	@In
	private Workstore workstore;
	
	@Out(required = false)
	private PasSpecialty pasSpecialty;
	
	@Out(required = false)
	private List<PasSpecialty> routeFormPasSpecialtyList;
		
	@Out(required = false)
	private List<PmsRouteSortSpec> pmsRouteSortSpecList;
	
	private List<PmsRouteSortSpec> orgRouteSortSpecList;
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private PasSpecialtyCacherInf pasSpecialtyCacher;
	
	private Map<String, List<PmsRouteFormSortSpec>> orgPmsRouteFormSortSpecMap;
	private Map<String, List<PmsRouteFormSortSpec>> newPmsRouteFormSortSpecMap;
	
	
	public void retrieveRouteFormSortSpecList(String specCode, PasSpecialtyType specType) throws PasException {
		pmsRouteSortSpecList = null;
		
		pasSpecialty = pasSpecialtyCacher.getPasSpecialty(specCode, specType);
		
		if (pasSpecialty != null) {
			retrieveRouteFormDisplaySeq(specCode, specType.getDataValue());	
		}
	}
	
	public void retrievePasSpecList(String prefixSpecCode, PasSpecialtyType specType) throws PasException {
		if(StringUtils.isBlank(prefixSpecCode) &&  specType==PasSpecialtyType.Blank) {
			routeFormPasSpecialtyList = pasSpecialtyCacher.getPasSpecialtyList();
		} else {
			routeFormPasSpecialtyList = pasSpecialtyCacher.getPasSpecialtyListBySpecCode(prefixSpecCode, specType);
		}
	}
	
	public void retrieveRouteFormDisplaySeq(String specCode, String specType) {			
		pmsRouteSortSpecList = dmsPmsServiceProxy.retrievePmsRouteSortSpecList(
				workstore.getHospCode(), 
				workstore.getWorkstoreCode(), 
				specCode, 
				specType);
		
		List<PmsRouteFormSortSpec> pmsRouteFormSortSpecList = dmsPmsServiceProxy.retrievePmsRouteFormSortSpecList(
				workstore.getHospCode(), 
				workstore.getWorkstoreCode(), 
				specCode, 
				specType);

		this.genList(
				dmsPmsServiceProxy.retrievePmsRouteNumList(), 
				dmsPmsServiceProxy.retrievePmsFormNumList(), 
				pmsRouteSortSpecList, 
				pmsRouteFormSortSpecList, 
				specCode, 
				specType);
		
		orgRouteSortSpecList = pmsRouteSortSpecList;
	}

	public void retrieveRouteFormDefaultSeqList(String specCode, PasSpecialtyType specType) {	
		pmsRouteSortSpecList = new ArrayList<PmsRouteSortSpec>();
		
		List<PmsRouteFormSortSpec> pmsRouteFormSortSpecList = new ArrayList<PmsRouteFormSortSpec>();
	
		this.genList(
				dmsPmsServiceProxy.retrievePmsRouteNumList(), 
				dmsPmsServiceProxy.retrievePmsFormNumList(), 
				pmsRouteSortSpecList, 
				pmsRouteFormSortSpecList, 
				specCode, 
				specType.getDataValue());
	}

	private void genList(
			List<PmsRouteNum> pmsRouteNumList, 
			List<PmsFormNum> pmsFormNumList, 
			List<PmsRouteSortSpec> pmsRouteSortSpecList,
			List<PmsRouteFormSortSpec> pmsRouteFormSortSpecList, 
			String specCode, 
			String specType) {
		
		if (pmsRouteSortSpecList.isEmpty()) {
			this.buildPmsRouteSortSpecList(pmsRouteSortSpecList, pmsRouteNumList, specCode, specType);
		}
		
		for (PmsRouteFormSortSpec pmsRouteFormSortSpec : pmsRouteFormSortSpecList) {
			for (PmsRouteSortSpec pmsRouteSortSpec : pmsRouteSortSpecList) {
				if (pmsRouteSortSpec.getPmsRouteNum().getRouteNum().equals(pmsRouteFormSortSpec.getPmsRouteNum().getRouteNum())) {
					pmsRouteSortSpec.getPmsRouteFormSortSpecList().add(pmsRouteFormSortSpec);
				}
			}
		}
				
		for (PmsRouteSortSpec pmsRouteSortSpec : pmsRouteSortSpecList) {
			if (pmsRouteSortSpec.getPmsRouteFormSortSpecList().isEmpty()) {
				this.buildPmsRouteFormSortSpecList(pmsRouteSortSpec.getPmsRouteFormSortSpecList(), pmsFormNumList,pmsRouteSortSpec.getPmsRouteNum(), specCode, specType);
			}
		}
	}
	
	
	private void buildPmsRouteSortSpecList(List<PmsRouteSortSpec> pmsRouteSortSpecList, List<PmsRouteNum> pmsRouteNumList, String specCode, String specType) {	
		for (PmsRouteNum pmsRouteNum : pmsRouteNumList ){
			PmsRouteSortSpec pmsRouteSortSpec = new PmsRouteSortSpec();
			PmsRouteSortSpecPK pmsRouteSortSpecPK = new PmsRouteSortSpecPK();
			pmsRouteSortSpecPK.setDispHospCode(workstore.getHospCode());
			pmsRouteSortSpecPK.setDispWorkstore(workstore.getWorkstoreCode());
			pmsRouteSortSpecPK.setPasSpecialty(specCode);
			pmsRouteSortSpecPK.setSpecialtyType(specType);
			pmsRouteSortSpecPK.setRouteNum(pmsRouteNum.getRouteNum());
			pmsRouteSortSpec.setCompId(pmsRouteSortSpecPK);
			pmsRouteSortSpec.setPmsRouteNum(pmsRouteNum);
			pmsRouteSortSpecList.add(pmsRouteSortSpec);
		}
	}
	
	private void buildPmsRouteFormSortSpecList(List<PmsRouteFormSortSpec> pmsRouteFormSortSpecList, List<PmsFormNum> pmsFormNumList, PmsRouteNum pmsRouteNum, String specCode, String specType) {	
		for (PmsFormNum pmsFormNum : pmsFormNumList ){
			PmsRouteFormSortSpec pmsRouteFormSortSpec = new PmsRouteFormSortSpec();
			
			PmsRouteFormSortSpecPK pmsRouteFormSortSpecPK = new PmsRouteFormSortSpecPK();
			pmsRouteFormSortSpecPK.setDispHospCode(workstore.getHospCode());
			pmsRouteFormSortSpecPK.setDispWorkstore(workstore.getWorkstoreCode());
			pmsRouteFormSortSpecPK.setPasSpecialty(specCode);
			pmsRouteFormSortSpecPK.setSpecialtyType(specType);
			pmsRouteFormSortSpecPK.setRouteNum(pmsRouteNum.getRouteNum());
			pmsRouteFormSortSpecPK.setFormNum(pmsFormNum.getFormNum());
			pmsRouteFormSortSpec.setCompId(pmsRouteFormSortSpecPK);
			pmsRouteFormSortSpec.setPmsRouteNum(pmsRouteNum);
			pmsRouteFormSortSpec.setPmsFormNum(pmsFormNum);
			pmsRouteFormSortSpecList.add(pmsRouteFormSortSpec);
		}
	}

	public void updateRouteSortSpecList(List<PmsRouteSortSpec> newPmsRouteSortSpecList)
	{	
		String pasSpec = newPmsRouteSortSpecList.get(0).getCompId().getPasSpecialty();
		String specType = newPmsRouteSortSpecList.get(0).getCompId().getSpecialtyType();
		
		dmsPmsServiceProxy.updatePmsRouteSortSpecList(newPmsRouteSortSpecList, workstore.getHospCode(), workstore.getWorkstoreCode(), 
			pasSpec , specType );
		
		initSortSpeqMap(orgRouteSortSpecList, newPmsRouteSortSpecList);
		formulateUpdateRouteListAuditLog(newPmsRouteSortSpecList);
		formulateUpdateFormListAuditLog(newPmsRouteSortSpecList);
		auditLogger.log( "#1003", serializer.exclude("pmsRouteFormSortSpecList").serialize(newPmsRouteSortSpecList) );
	}
	
	private void initSortSpeqMap(List<PmsRouteSortSpec> orgRouteSortSpecList, List<PmsRouteSortSpec> newRouteSortSpecList)
	{
		orgPmsRouteFormSortSpecMap = new HashMap<String, List<PmsRouteFormSortSpec>>();
		for ( PmsRouteSortSpec item : orgRouteSortSpecList ) {
			orgPmsRouteFormSortSpecMap.put(item.getPmsRouteNum().getRouteNum(), item.getPmsRouteFormSortSpecList());
		}
		
		newPmsRouteFormSortSpecMap = new HashMap<String, List<PmsRouteFormSortSpec>>();
		for ( PmsRouteSortSpec item : newRouteSortSpecList ) {
			newPmsRouteFormSortSpecMap.put(item.getPmsRouteNum().getRouteNum(), item.getPmsRouteFormSortSpecList());
		}
	}
	
	private void formulateUpdateRouteListAuditLog(List<PmsRouteSortSpec> newPmsRouteSortSpecList)
	{
		for(int i=0;i<newPmsRouteSortSpecList.size();i++)
		{
			if( !newPmsRouteSortSpecList.get(i).getPmsRouteNum().getRouteNum().equals(orgRouteSortSpecList.get(i).getPmsRouteNum().getRouteNum()))
			{
				String pasSpec = newPmsRouteSortSpecList.get(0).getCompId().getPasSpecialty();
				String specType = newPmsRouteSortSpecList.get(0).getCompId().getSpecialtyType();
				
				auditLogger.log("#0620", pasSpec, specType, getRouteSortSeq(orgRouteSortSpecList), getRouteSortSeq(newPmsRouteSortSpecList) );
				break;
			}
		}
	}

	private void formulateUpdateFormListAuditLog(List<PmsRouteSortSpec> newPmsRouteSortSpecList)
	{
	
		Map<String, Boolean> formChangedMap = new HashMap<String, Boolean>();
		
		for ( Map.Entry<String, List<PmsRouteFormSortSpec>> entry : orgPmsRouteFormSortSpecMap.entrySet() ) {
			formChangedMap.put(entry.getKey(), checkFormSeqChanges(newPmsRouteFormSortSpecMap.get(entry.getKey()), entry.getValue()));
		}

		String pasSpec = newPmsRouteSortSpecList.get(0).getCompId().getPasSpecialty();
		String specType = newPmsRouteSortSpecList.get(0).getCompId().getSpecialtyType();
		for ( Map.Entry<String, Boolean> formChanged : formChangedMap.entrySet() ) 
		{
			if ( formChanged.getValue() ) 
			{
				auditLogger.log("#0621",
									pasSpec, 
									specType, 
									formChanged.getKey(), 
									getFormSortSeq(orgPmsRouteFormSortSpecMap.get(formChanged.getKey())), 
									getFormSortSeq(newPmsRouteFormSortSpecMap.get(formChanged.getKey())));
			}
		}
	}
	
	private Boolean checkFormSeqChanges(List<PmsRouteFormSortSpec> updateSpecList, List<PmsRouteFormSortSpec> currentSpecList)
	{
		for(int i=0;i<updateSpecList.size();i++)
		{
			if(!updateSpecList.get(i).getPmsFormNum().getFormNum().equals(currentSpecList.get(i).getPmsFormNum().getFormNum()))
			{
				return true;
			}
		}
		return false;
	}
	
	private String getRouteSortSeq(List<PmsRouteSortSpec> routeList) 
	{
		StringBuilder result = new StringBuilder();
		
		for ( PmsRouteSortSpec route : routeList ) {
			if ( result.length()>0 ) {
				result.append(", ");
			}
			result.append(route.getPmsRouteNum().getRouteNum());
		}
		
		return result.toString();
	}
	
	private String getFormSortSeq(List<PmsRouteFormSortSpec> formList) 
	{
		StringBuilder result = new StringBuilder();
		
		for ( PmsRouteFormSortSpec form : formList ) {
			if ( result.length()>0 ) {
				result.append(", ");
			}
			result.append(form.getPmsFormNum().getFormNum());
		}
		
		return result.toString();
	}
	
	@Remove
	public void destroy() {
		if (routeFormPasSpecialtyList != null) {
			routeFormPasSpecialtyList = null;
		}
		
		if (pmsRouteSortSpecList != null) {
			pmsRouteSortSpecList = null;
		}
	}
}
