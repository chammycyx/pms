package hk.org.ha.model.pms.vo.alert;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class AlertProfileHistory implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<PatHistoryAllergy> patHistoryAllergyList;
	
	private List<PatHistoryAdr> patHistoryAdrList;
	
	private List<PatHistoryAlert> patHistoryAlertList;

	public List<PatHistoryAllergy> getPatHistoryAllergyList() {
		return patHistoryAllergyList;
	}

	public void setPatHistoryAllergyList(List<PatHistoryAllergy> patHistoryAllergyList) {
		this.patHistoryAllergyList = patHistoryAllergyList;
	}

	public List<PatHistoryAdr> getPatHistoryAdrList() {
		return patHistoryAdrList;
	}

	public void setPatHistoryAdrList(List<PatHistoryAdr> patHistoryAdrList) {
		this.patHistoryAdrList = patHistoryAdrList;
	}

	public List<PatHistoryAlert> getPatHistoryAlertList() {
		return patHistoryAlertList;
	}

	public void setPatHistoryAlertList(List<PatHistoryAlert> patHistoryAlertList) {
		this.patHistoryAlertList = patHistoryAlertList;
	}
}
