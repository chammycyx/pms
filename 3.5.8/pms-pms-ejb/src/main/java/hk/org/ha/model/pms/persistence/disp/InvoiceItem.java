package hk.org.ha.model.pms.persistence.disp;

import hk.org.ha.model.pms.persistence.VersionEntity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


@Entity
@Table(name = "INVOICE_ITEM")
@XmlAccessorType(XmlAccessType.FIELD)
public class InvoiceItem extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "invoiceItemSeq")
	@SequenceGenerator(name = "invoiceItemSeq", sequenceName = "SQ_INVOICE_ITEM", initialValue = 100000000)
	@XmlElement
	private Long id;
	
	@Column(name = "COST", nullable = false, precision = 19, scale = 4)
	@XmlElement
	private BigDecimal cost;

	@Column(name = "MARKUP_AMOUNT", nullable = false, precision = 19, scale = 0)
	@XmlElement
	private BigDecimal markupAmount;

	@Column(name = "HANDLE_AMOUNT", nullable = false, precision = 19, scale = 0)
	@XmlElement
	private BigDecimal handleAmount;

	@Column(name = "HANDLE_EXEMPT_AMOUNT", nullable = false, precision = 19, scale = 0)
	@XmlElement
	private BigDecimal handleExemptAmount;
	
	@Column(name = "HANDLE_EXEMPT_CODE", length=2)
	@XmlElement
	private String handleExemptCode;

	@Column(name = "NEXT_HANDLE_EXEMPT_CODE", length=2)
	@XmlElement
	private String nextHandleExemptCode;

	@Column(name = "HANDLE_EXEMPT_USER", length=12)
	@XmlElement
	private String handleExemptUser;
	
	@Column(name = "HANDLE_EXEMPT_DATE")
	@Temporal(TemporalType.DATE)
	@XmlElement
	private Date handleExemptDate;
	
	@Column(name = "AMOUNT", nullable = false, precision=19, scale=0)
	@XmlElement
	private BigDecimal amount;
	
	@Column(name = "ACTUAL_MARKUP_AMOUNT", precision=19, scale=4, nullable=false)
	@XmlElement
	private BigDecimal actualMarkupAmount;
	
	@Column(name = "CAP_MARKUP_AMOUNT", precision=19, scale=4, nullable=false)
	@XmlElement
	private BigDecimal capMarkupAmount;
	
	@ManyToOne
	@JoinColumn(name = "INVOICE_ID", nullable = false)
	@XmlTransient
	private Invoice invoice;
	
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "DISP_ORDER_ITEM_ID", nullable = false)
    @XmlTransient
    private DispOrderItem dispOrderItem;
	
	@Column(name = "CHARGE_QTY", precision = 19, scale = 6)
	@XmlElement
	private BigDecimal chargeQty;
    
	public InvoiceItem() {
		cost = BigDecimal.ZERO;
		markupAmount = BigDecimal.ZERO;
		handleAmount = BigDecimal.ZERO;
    	amount = BigDecimal.ZERO;
    	handleExemptAmount = BigDecimal.ZERO;
    	actualMarkupAmount = BigDecimal.ZERO;
    	capMarkupAmount = BigDecimal.ZERO;
    }
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getCost() {
		return cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

	public BigDecimal getMarkupAmount() {
		return markupAmount;
	}

	public void setMarkupAmount(BigDecimal markupAmount) {
		this.markupAmount = markupAmount;
	}

	public BigDecimal getHandleAmount() {
		return handleAmount;
	}

	public void setHandleAmount(BigDecimal handleAmount) {
		this.handleAmount = handleAmount;
	}

	public BigDecimal getHandleExemptAmount() {
		return handleExemptAmount;
	}

	public void setHandleExemptAmount(BigDecimal handleExemptAmount) {
		this.handleExemptAmount = handleExemptAmount;
	}

	public String getHandleExemptCode() {
		return handleExemptCode;
	}

	public void setHandleExemptCode(String handleExemptCode) {
		this.handleExemptCode = handleExemptCode;
	}

	public String getNextHandleExemptCode() {
		return nextHandleExemptCode;
	}

    public void setNextHandleExemptCode(String nextHandleExemptCode) {
		this.nextHandleExemptCode = nextHandleExemptCode;
	}

	public String getHandleExemptUser() {
		return handleExemptUser;
	}

	public void setHandleExemptUser(String handleExemptUser) {
		this.handleExemptUser = handleExemptUser;
	}

	public Date getHandleExemptDate() {
		return handleExemptDate;
	}

	public void setHandleExemptDate(Date handleExemptDate) {
		this.handleExemptDate = handleExemptDate;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Invoice getInvoice() {
		return invoice;
	}
	
	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public DispOrderItem getDispOrderItem() {
		return dispOrderItem;
	}
	
	public void setDispOrderItem(DispOrderItem dispOrderItem) {
		this.dispOrderItem = dispOrderItem;
	}

	public BigDecimal getActualMarkupAmount() {
		return actualMarkupAmount;
	}

	public void setActualMarkupAmount(BigDecimal actualMarkupAmount) {
		this.actualMarkupAmount = actualMarkupAmount;
	}

	public BigDecimal getCapMarkupAmount() {
		return capMarkupAmount;
	}

	public void setCapMarkupAmount(BigDecimal capMarkupAmount) {
		this.capMarkupAmount = capMarkupAmount;
	}

	public void setChargeQty(BigDecimal chargeQty) {
		this.chargeQty = chargeQty;
	}

	public BigDecimal getChargeQty() {
		return chargeQty;
	}
}
