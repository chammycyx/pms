package hk.org.ha.model.pms.biz.onestop;

import hk.org.ha.model.pms.persistence.disp.MedOrder;

import javax.ejb.Local;

@Local
public interface OneStopOrderStatusManagerLocal {
	
	String retrieveLatestOrderStatusFromLockingCheck(Long orderId, Long refillId);
	
	String verifySuspendAllowModifyUncollectStatus(Long dispOrderId, Long dispOrderVersion);
	
	String verifyMedOrderStatusFromPreVetOperation(MedOrder currentMedOrder);
	
	String verifyMedOrderStatusBeforeSwitchToVetting(Long medOrderId);
	
	String verifyDispOrderStatusBeforeSwitchToVetting(Long dispOrderId);
	
	String verifyRefillOrderStatusBeforeSwitchToVetting(Long dispOrderId);
}
