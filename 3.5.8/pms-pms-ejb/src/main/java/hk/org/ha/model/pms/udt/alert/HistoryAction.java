package hk.org.ha.model.pms.udt.alert;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum HistoryAction implements StringValuedEnum {
	
	Add("A", "Add"),
	Update("E", "Update"),
	Delete("D", "Delete"),
	Convert("CA", "Convert (system)");

	private final String dataValue;
    private final String displayValue;
    
	HistoryAction(final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}

	public static HistoryAction dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(HistoryAction.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<HistoryAction> {

		private static final long serialVersionUID = 1L;

		public Class<HistoryAction> getEnumClass() {
			return HistoryAction.class;
		}
	}
}
