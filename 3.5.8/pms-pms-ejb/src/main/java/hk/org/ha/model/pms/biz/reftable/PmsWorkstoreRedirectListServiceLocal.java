package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.dms.persistence.PmsWorkstoreRedirect;

import java.util.List;

import javax.ejb.Local;

@Local
public interface PmsWorkstoreRedirectListServiceLocal {

	void retrievePmsWorkstoreRedirectList();
	
	void updatePmsWorkstoreRedirectList(List<PmsWorkstoreRedirect> updatePmsWorkstoreRedirectList);
	
	void destroy();
	
}
