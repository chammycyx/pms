package hk.org.ha.model.pms.biz.delivery;

import static hk.org.ha.model.pms.prop.Prop.ALERT_EHR_PROFILE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.ALERT_MDS_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.ALERT_PROFILE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.CDDH_LEGACY_PERSISTENCE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.DELIVERY_ALERT_MDS_CHECK;
import static hk.org.ha.model.pms.prop.Prop.DELIVERY_ALERT_MDS_DDI_CHECK;
import static hk.org.ha.model.pms.prop.Prop.DELIVERY_FOLLOWUPITEM_DURATION_LIMIT;
import static hk.org.ha.model.pms.prop.Prop.DELIVERY_PURCHASEREQUEST_DURATION_LIMIT;
import static hk.org.ha.model.pms.prop.Prop.LABEL_DIVIDERLABEL_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.LABEL_MPDISPLABEL_SORT;
import static hk.org.ha.model.pms.prop.Prop.MACHINE_ATDPS_PORT;
import static hk.org.ha.model.pms.prop.Prop.VETTING_MEDPROFILE_ALERT_PROFILE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.VETTING_MEDPROFILE_INACTIVEWARD_REPORT_TRX_PRINT;
import flexjson.JSONSerializer;
import flexjson.transformer.DateTransformer;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.util.QueryUtils;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.alert.AlertProfileException;
import hk.org.ha.model.pms.asa.exception.alert.ehr.EhrAllergyException;
import hk.org.ha.model.pms.biz.alert.AlertProfileManagerLocal;
import hk.org.ha.model.pms.biz.alert.ehr.EhrAlertManagerLocal;
import hk.org.ha.model.pms.biz.alert.mds.MedProfileCheckManagerLocal;
import hk.org.ha.model.pms.biz.charging.DrugChargeClearanceManagerLocal;
import hk.org.ha.model.pms.biz.charging.InvoiceManagerLocal;
import hk.org.ha.model.pms.biz.delivery.BatchNumGeneratorLocal.BatchType;
import hk.org.ha.model.pms.biz.inbox.PharmInboxClientNotifierInf;
import hk.org.ha.model.pms.biz.label.LabelManagerLocal;
import hk.org.ha.model.pms.biz.label.MpDispLabelComparator;
import hk.org.ha.model.pms.biz.medprofile.MedProfileConstant;
import hk.org.ha.model.pms.biz.medprofile.MedProfileManagerLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileStatManagerLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.biz.order.DispOrderConverterLocal;
import hk.org.ha.model.pms.biz.printing.InvoiceRendererLocal;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.reftable.ActiveWardManagerLocal;
import hk.org.ha.model.pms.biz.reftable.ItemDispConfigManagerLocal;
import hk.org.ha.model.pms.biz.report.MdsExceptionRptManagerLocal;
import hk.org.ha.model.pms.biz.report.MpTrxRptPrintManagerLocal;
import hk.org.ha.model.pms.biz.vetting.mp.OrderPendingRptManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.MsWorkstoreDrug;
import hk.org.ha.model.pms.dms.vo.pivas.PivasPrepCriteria;
import hk.org.ha.model.pms.dms.vo.pivas.PivasPrepCriteriaDetail;
import hk.org.ha.model.pms.dms.vo.pivas.PivasPrepValidateResponse;
import hk.org.ha.model.pms.exception.delivery.DueOrderPrintException;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.InvoiceItem;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.medprofile.Delivery;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryRequest;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryRequestAlert;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryRequestGroup;
import hk.org.ha.model.pms.persistence.medprofile.DeliverySchedule;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryScheduleItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileErrorLog;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.persistence.medprofile.PurchaseRequest;
import hk.org.ha.model.pms.persistence.medprofile.ReplenishmentItem;
import hk.org.ha.model.pms.persistence.pivas.MaterialRequest;
import hk.org.ha.model.pms.persistence.pivas.MaterialRequestItem;
import hk.org.ha.model.pms.persistence.pivas.PivasBatchPrepItem;
import hk.org.ha.model.pms.persistence.pivas.PivasPrep;
import hk.org.ha.model.pms.persistence.pivas.PivasPrepMethod;
import hk.org.ha.model.pms.persistence.pivas.PivasPrepMethodItem;
import hk.org.ha.model.pms.persistence.pivas.PivasWorklist;
import hk.org.ha.model.pms.persistence.reftable.ItemDispConfig;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.persistence.reftable.WorkstationProp;
import hk.org.ha.model.pms.udt.RecordStatus;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.disp.MpTrxType;
import hk.org.ha.model.pms.udt.medprofile.DeliveryItemStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryRequestGroupStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryRequestStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryStatus;
import hk.org.ha.model.pms.udt.medprofile.ErrorLogType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileMoItemStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatus;
import hk.org.ha.model.pms.udt.medprofile.PrintMode;
import hk.org.ha.model.pms.udt.medprofile.ReplenishmentStatus;
import hk.org.ha.model.pms.udt.medprofile.RetrievePurchaseRequestOption;
import hk.org.ha.model.pms.udt.pivas.MaterialRequestItemStatus;
import hk.org.ha.model.pms.udt.pivas.PivasWorklistStatus;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.vo.alert.AlertProfile;
import hk.org.ha.model.pms.vo.alert.mds.MedProfileCheckResult;
import hk.org.ha.model.pms.vo.charging.FcsPaymentStatus;
import hk.org.ha.model.pms.vo.delivery.FollowUpItem;
import hk.org.ha.model.pms.vo.label.MdsOverrideReasonLabel;
import hk.org.ha.model.pms.vo.label.MpDispLabel;
import hk.org.ha.model.pms.vo.label.PivasOuterBagLabel;
import hk.org.ha.model.pms.vo.label.PivasProductLabel;
import hk.org.ha.model.pms.vo.label.PivasWorksheet;
import hk.org.ha.model.pms.vo.machine.AtdpsMessage;
import hk.org.ha.model.pms.vo.machine.AtdpsMessageContent;
import hk.org.ha.model.pms.vo.medprofile.ErrorInfo;
import hk.org.ha.model.pms.vo.medprofile.ErrorInfoItem;
import hk.org.ha.model.pms.vo.medprofile.PasContext;
import hk.org.ha.model.pms.vo.medprofile.Ward;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.report.MdsExceptionRptContainer;
import hk.org.ha.model.pms.vo.rx.RxItem;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;
import hk.org.ha.service.pms.asa.interfaces.machine.MachineServiceJmsRemote;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.UUID;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.tuple.MutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.joda.time.DateMidnight;
import org.springframework.beans.BeanUtils;

@AutoCreate
@Stateless
@Name("dueOrderPrintManager")
@MeasureCalls
public class DueOrderPrintManagerBean implements DueOrderPrintManagerLocal {
	
	private static final String OVERDUE_COUNT_CLASS_NAME = OverdueCount.class.getName();
	private static final String FOLLOW_UP_ITEM_CLASS_NAME = FollowUpItem.class.getName();
	
	private static final String MED_PROFILE_QUERY_HINT_FETCH_1 = "m.patient";
	private static final String MED_PROFILE_QUERY_HINT_FETCH_2 = "m.medCase";

	private static final String MED_PROFILE_QUERY_HINT_BATCH_1 = "p.medProfileItemList.medProfileMoItem.medProfilePoItemList";
	private static final String MED_PROFILE_QUERY_HINT_BATCH_2 = "p.medProfileItemList.medProfileErrorLog";
	
	private static final String MED_PROFILE_ITEM_QUERY_HINT_FETCH_1 = "o.medProfileErrorLog";
	
	private static final String DELIVERY_QUERY_HINT_FETCH_1 = "d.deliveryRequest.workstore";
	
	private static final String PURCHASE_REQUEST_QUERY_HINT_FETCH_1 = "r.medProfilePoItem";
	private static final String PURCHASE_REQUEST_QUERY_HINT_FETCH_2 = "r.medProfilePoItem.medProfileMoItem.medProfileItem.medProfile.patient";
	private static final String PURCHASE_REQUEST_QUERY_HINT_FETCH_3 = "r.medProfilePoItem.medProfileMoItem.medProfileItem.medProfile.medCase";
	private static final String PURCHASE_REQUEST_QUERY_HINT_FETCH_4 = "r.medProfile.patient";
	private static final String PURCHASE_REQUEST_QUERY_HINT_FETCH_5 = "r.medProfile.medCase";
		
	private static final String PIVAS_WORKLIST_QUERY_HINT_FETCH_1 = "o.medProfileMoItem.medProfileItem.medProfile.patient"; 
	private static final String PIVAS_WORKLIST_QUERY_HINT_FETCH_2 = "o.medProfileMoItem.medProfileItem.medProfile.medCase"; 
	
	private static final String MATERIAL_REQUEST_ITEM_QUERY_HINT_FETCH_1 = "i.materialRequest.medProfile.patient";
	private static final String MATERIAL_REQUEST_ITEM_QUERY_HINT_FETCH_2 = "i.materialRequest.medProfile.medCase";
	private static final String MATERIAL_REQUEST_ITEM_QUERY_HINT_FETCH_3 = "i.materialRequest.medProfileMoItem";
	private static final String MATERIAL_REQUEST_ITEM_QUERY_HINT_FETCH_4 = "i.medProfilePoItem.medProfileMoItem.medProfileItem.medProfile.patient";
	private static final String MATERIAL_REQUEST_ITEM_QUERY_HINT_FETCH_5 = "i.medProfilePoItem.medProfileMoItem.medProfileItem.medProfile.medCase";
	
	private static final String MDS_ERROR_MESSAGE = "MDS checking pending";
	
	private static final String JAXB_CONTEXT_LABEL = "hk.org.ha.model.pms.vo.label";
	
	private static final JSONSerializer serializer = new JSONSerializer().transform(new DateTransformer("yyyy-MM-dd HH:mm:ss.SSS"), Date.class);
	
	private static final Comparator<RenderAndPrintJob> COMPARATOR_NORMAL_RENDER_AND_PRINT_JOB = new NormalRenderAndPrintJobComparator();
	private static final Comparator<List<RenderAndPrintJob>> COMPARATOR_PIVAS_RENDER_AND_PRINT_JOB_GROUP = new PivasRenderAndPrintJobGroupComparator();

	@Logger
	private Log logger;
	
	@In
	private AuditLogger auditLogger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private MedProfileManagerLocal medProfileManager;
	
	@In
	private MedProfileStatManagerLocal medProfileStatManager;
	
	@In
	private AlertProfileManagerLocal alertProfileManager;
	
	@In
	private EhrAlertManagerLocal ehrAlertManager;
	
	@In
	private DrugChargeClearanceManagerLocal drugChargeClearanceManager;
	
	@In
	private LabelManagerLocal labelManager;
	
	@In
	private MpTrxRptPrintManagerLocal mpTrxRptPrintManager;
	
	@In
	private MdsExceptionRptManagerLocal mdsExceptionRptManager;
	
	@In
	private MedProfileCheckManagerLocal medProfileCheckManager;
	
	@In
	private BatchNumGeneratorLocal batchNumGenerator;
	
	@In
	private ItemDispConfigManagerLocal itemDispConfigManager;
	
	@In
	private ActiveWardManagerLocal activeWardManager;
	
	@In
	private DispOrderConverterLocal dispOrderConverter;
	
	@In
	private InvoiceRendererLocal invoiceRenderer;
	
	@In
	private InvoiceManagerLocal invoiceManager;
	
	@In
	private PharmInboxClientNotifierInf pharmInboxClientNotifier;
	
	@In
	private PrintAgentInf printAgent; 
	
	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private DmDrugCacherInf dmDrugCacher;	
	
	@In
	private OrderPendingRptManagerLocal orderPendingRptManager;
			
	@In
	private MachineServiceJmsRemote machineServiceProxy;
	
    public DueOrderPrintManagerBean() {
    }
    
    @Override
    public OverdueCount retrieveOverdueCountByStat(Workstore workstore, Date startDate, Date endDate, Date currentDate, Boolean urgentFlag, Boolean statFlag) {
    	
    	Query query = em.createQuery(
    			"select new " + OVERDUE_COUNT_CLASS_NAME + // 20151019 index check : MedProfilePoItemStat.hospCode,dueDate : I_MED_PROFILE_PO_ITEM_STAT_04
    			"  (count(s.lastDueDate) , count(s.id))" +
    			" from MedProfilePoItemStat s" +
    			" where s.hospCode = :hospCode" +
    			" and (s.returnDate < :currentDate or s.returnDate is null)" +
		    	(urgentFlag == null ? "" : " and s.urgentFlag = :urgentFlag") +
		    	(statFlag == null ? "" : " and s.statFlag = :statFlag") +
		    	(endDate == null ? "" : " and s.dueDate < :endDate") +
		    	" and s.dueDate >= :startDate" +
		    	" and s.wardCode in" +
		    	"  (select w.wardCode from Ward w, WardConfig wc" +
		    	"   where w.institution = :institution and w.status = :activeRecordStatus and wc.workstoreGroup = :workstoreGroup" +
		    	"   and w.wardCode = wc.wardCode and wc.mpWardFlag = true)")
		    	.setParameter("hospCode", workstore.getHospCode())
				.setParameter("startDate", startDate)
				.setParameter("currentDate", currentDate)
		    	.setParameter("institution", workstore.getWorkstoreGroup().getInstitution())
		    	.setParameter("workstoreGroup", workstore.getWorkstoreGroup())
		    	.setParameter("activeRecordStatus", RecordStatus.Active);
    	
    	if (endDate != null) {
    		query.setParameter("endDate", endDate);
    	}
    	if (urgentFlag != null) {
    		query.setParameter("urgentFlag", urgentFlag);
    	}
    	if (statFlag != null) {
    		query.setParameter("statFlag", statFlag);
    	}
    	
    	return (OverdueCount) query.getSingleResult();
    }
    
    @SuppressWarnings("unchecked")
	@Override
    public List<PurchaseRequest> retrievePurchaseRequestList(Workstore workstore, Date currentDate, RetrievePurchaseRequestOption option) {
    	
    	if (option == null) {
    		option = RetrievePurchaseRequestOption.All;
    	}
    	Date startDate = MedProfileUtils.addDays(currentDate, DELIVERY_PURCHASEREQUEST_DURATION_LIMIT.get(14).intValue() * -1);
    	
    	Query query = em.createQuery(
    			"select r from PurchaseRequest r" + // 20150420 index check : PurchaseRequest.hospCode,invoiceDate,printDate
    			" where r.hospCode = :hospCode" +
		    	" and r.medProfile.hospCode = :hospCode" +
		    	" and (r.medProfile.status = :activeStatus" +
		    	" or (r.medProfile.status = :homeLeaveStatus" +
		    	" and r.medProfile.returnDate < :currentDate))" +
		    	" and r.invoiceDate >= :startDate" +
		    	" and r.printDate is null" +
		    	" and r.directPrintFlag = false" +
		    	" and r.invoiceStatus in :invoiceStatusList" +
		    	(option == RetrievePurchaseRequestOption.AwaitPayment ? " and r.forceProceedFlag = false" : "") +
		    	" and r.medProfile.wardCode in" +
		    	"  (select w.wardCode from Ward w" +
		    	"   where w.institution = :institution and w.status = :activeRecordStatus)" +
		    	" order by r.invoiceDate, r.createDate")
		    	.setHint(QueryHints.FETCH, PURCHASE_REQUEST_QUERY_HINT_FETCH_4)
		    	.setHint(QueryHints.FETCH, PURCHASE_REQUEST_QUERY_HINT_FETCH_5)
		    	.setParameter("hospCode", workstore.getHospCode())
				.setParameter("activeStatus", MedProfileStatus.Active)
				.setParameter("homeLeaveStatus", MedProfileStatus.HomeLeave)
				.setParameter("startDate", startDate)
				.setParameter("currentDate", currentDate)
				.setParameter("institution", workstore.getWorkstoreGroup().getInstitution())
				.setParameter("activeRecordStatus", RecordStatus.Active);

    	if (option == RetrievePurchaseRequestOption.All) {
			query.setParameter("invoiceStatusList", InvoiceStatus.Outstanding_Settle);
    	} else if (option == RetrievePurchaseRequestOption.AwaitPayment) {
	    	query.setParameter("invoiceStatusList", Arrays.asList(InvoiceStatus.Outstanding));
    	} else if (option == RetrievePurchaseRequestOption.Paid) {
    		query.setParameter("invoiceStatusList", Arrays.asList(InvoiceStatus.Settled));
    	}
    	
    	return query.getResultList();
    }
    
    @SuppressWarnings("unchecked")
	@Override    
    public List<MaterialRequestItem> retrieveMaterialRequestItemList(Workstore workstore, Date startDate, Date currentDate) {
    	
    	List<MaterialRequestItem> materialRequestItemList = em.createQuery(
    			"select i from MaterialRequestItem i" + // 20160615 index check : MaterialRequestItem.hospCode,status,dueDate : I_MATERIAL_REQUEST_ITEM_01
    			" where i.materialRequest.hospCode = :hospCode" +
    			" and i.materialRequest.medProfile.hospCode = :hospCode" +
    			" and (i.materialRequest.medProfile.status = :activeStatus" +
    			" or (i.materialRequest.medProfile.status = :homeLeaveStatus" +
    			" and i.materialRequest.medProfile.returnDate < :currentDate))" +
    			" and i.materialRequest.medProfileMoItem.id = i.materialRequest.medProfileMoItem.medProfileItem.medProfileMoItemId" +
    			" and i.materialRequest.medProfileMoItem.medProfileItem.status = :verifiedStatus" +
    			" and (i.materialRequest.medProfileMoItem.endDate > :currentDate or i.materialRequest.medProfileMoItem.endDate is null)" +
    			" and (i.materialRequest.medProfileMoItem.transferFlag = false or i.materialRequest.medProfileMoItem.transferFlag is null)" +
    			" and (i.materialRequest.medProfileMoItem.frozenFlag = false or i.materialRequest.medProfileMoItem.frozenFlag is null)" +
    			" and i.materialRequest.medProfileMoItem.suspendCode is null and i.materialRequest.medProfileMoItem.pendingCode is null" +
    			" and i.hospCode = :hospCode" +
    			" and i.dueDate >= :startDate" +
    			" and i.status = :outstandingStatus" +
    			" and i.materialRequest.medProfile.wardCode in" +
		    	"  (select w.wardCode from Ward w" +
		    	"   where w.institution = :institution and w.status = :activeRecordStatus)" +
		    	" order by i.dueDate, i.materialRequest.medProfile.wardCode, i.materialRequest.medProfile.medCase.pasBedNum")
		    	.setHint(QueryHints.FETCH, MATERIAL_REQUEST_ITEM_QUERY_HINT_FETCH_1)
		    	.setHint(QueryHints.FETCH, MATERIAL_REQUEST_ITEM_QUERY_HINT_FETCH_2)
		    	.setHint(QueryHints.FETCH, MATERIAL_REQUEST_ITEM_QUERY_HINT_FETCH_3)
		    	.setParameter("hospCode", workstore.getHospCode())
				.setParameter("activeStatus", MedProfileStatus.Active)
				.setParameter("homeLeaveStatus", MedProfileStatus.HomeLeave)
				.setParameter("verifiedStatus", MedProfileItemStatus.Verified)
				.setParameter("startDate", startDate)
				.setParameter("currentDate", currentDate)
				.setParameter("outstandingStatus", MaterialRequestItemStatus.Outstanding)
				.setParameter("institution", workstore.getWorkstoreGroup().getInstitution())
				.setParameter("activeRecordStatus", RecordStatus.Active)		    	
    			.getResultList();
    	
    	List<MaterialRequestItem> resultList = new ArrayList<MaterialRequestItem>();
    	
    	for (MaterialRequestItem materialRequestItem: materialRequestItemList) {
    		activeWardManager.initInvalidPasWardFlag(materialRequestItem.getMaterialRequest().getMedProfile(), currentDate);
    		if (materialRequestItem.getMaterialRequest().getMedProfileMoItem().isManualItem() ||
        			!Boolean.TRUE.equals(materialRequestItem.getMaterialRequest().getMedProfile().getInactivePasWardFlag())) {
    			resultList.add(materialRequestItem);
    		}
    	}
    	return resultList;
    }
    
	@Override
    public Long retrieveFollowUpCount(Workstore workstore) {
    	return Long.valueOf(retrieveFollowUpItemList(workstore).size());
    }
    
    @SuppressWarnings("unchecked")
	@Override
    public List<FollowUpItem> retrieveFollowUpItemList(Workstore workstore) {
    	
		Calendar startCal = Calendar.getInstance();
		startCal.add(Calendar.DAY_OF_MONTH,
				DELIVERY_FOLLOWUPITEM_DURATION_LIMIT.get(7).intValue() * -1);
    	
    	return em.createQuery(
    			"select distinct new " + FOLLOW_UP_ITEM_CLASS_NAME + // 20140128 index check : DeliveryRequest.hospital,createDate : I_DELIVERY_REQUEST_01
    			"  (r.dueDate, r.orderType, r.generateDay, l.medProfileItem.medProfile.medCase.caseNum, l.type)" +
    			" from MedProfileErrorLog l, DeliveryRequest r" +
    			" where l.medProfileItem.medProfile.hospCode = :hospCode" +
    			" and r.hospCode = :hospCode" +
    			" and l.medProfileItem.medProfile.status <> :inactiveStatus" +
    			" and l.medProfileItem.medProfileErrorLog = l" +
    			" and l.deliveryRequestId = r.id" +
    			" and r.createDate > :startDate" +
    			" and r.status = :completedStatus" +
    			" and l.medProfileItem.medProfileMoItem.suspendCode is null" +
    			" and l.medProfileItem.medProfileMoItem.pendingCode is null" +
    			" and l.hideFollowUpFlag = false" +
    			" order by r.dueDate")
    			.setParameter("hospCode", workstore.getHospCode())
    			.setParameter("inactiveStatus", MedProfileStatus.Inactive)
    			.setParameter("startDate", startCal.getTime())
    			.setParameter("completedStatus", DeliveryRequestStatus.Completed)
    			.getResultList();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Delivery> retrieveTodayDeliveryList(Workstore workstore) {
    	
    	return em.createQuery(
    			"select d from Delivery d" + // 20120831 index check : Delivery.hospital,batchDate : I_DELIVERY_01
    			" where d.deliveryRequest.workstore = :workstore" +
    			" and d.deliveryRequest.status = :completedStatus" +
    			" and d.hospital = :hospital" +
    			" and d.batchDate = :batchDate" +
    			" and d.printMode = :printMode" +
    			" order by d.batchNum desc")
    			.setHint(QueryHints.FETCH, DELIVERY_QUERY_HINT_FETCH_1)
    			.setParameter("workstore", workstore)
    			.setParameter("completedStatus", DeliveryRequestStatus.Completed)
    			.setParameter("hospital", workstore.getHospital())
    			.setParameter("batchDate", MedProfileUtils.getTodayBegin())
    			.setParameter("printMode", PrintMode.Batch)
    			.getResultList();
    }
    
    public Map<String, FcsPaymentStatus> retrieveDrugChargeClearance(List<PurchaseRequest> purchaseRequestList){
    	boolean checkProfileCurrentProcessing = true;
    	
    	List<PurchaseRequest> clearanceCheckList = new ArrayList<PurchaseRequest>();
    	for (PurchaseRequest purchaseRequest: purchaseRequestList) {
    		MedProfile medProfile = purchaseRequest.getMedProfilePoItem().getMedProfileMoItem().getMedProfileItem().getMedProfile();
    		if ((!checkProfileCurrentProcessing || 
    				(!Boolean.TRUE.equals(medProfile.getProcessingFlag()) && !Boolean.TRUE.equals(medProfile.getDeliveryGenFlag()))) &&
    				InvoiceStatus.Outstanding == purchaseRequest.getInvoiceStatus()) {
    			clearanceCheckList.add(purchaseRequest);
    		}
    	}
    	Map<String, FcsPaymentStatus> paymentStatusMap = drugChargeClearanceManager.checkSfiClearance(null, clearanceCheckList);
    	return paymentStatusMap;
    }    
    
    
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public List<MedProfile> retrieveMedProfileList(Ward ward, DeliveryRequest request) {

    	List<MedProfile> medProfileList = fetchMedProfileList(ward, request);
    	
    	if (request.isCaseNumOrHkidSelected()) {
    		if (medProfileList.size() == 0) {
    	    	request.setProfileNotFoundFlag(!hasActiveMedProfile(request.getWorkstore(), request.getCaseNum(), request.getHkid()));
    	    }
			removeFollowUp(request);
    	}
    	
    	return medProfileList;
    }

	@Override
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<MedProfile> retrieveMedProfileListForPivas(DeliveryRequest request) {

		return em.createQuery(
				"select distinct o.medProfile from PivasWorklist o" + // 20160615 index check : PivasWorklist.workstore,status,supplyDate : I_PIVAS_WORKLIST_02
				" where o.medProfile.hospCode = :hospCode" +
				" and o.medProfile.status <> :inactiveStatus" +
				" and (o.medProfile.status = :activeStatus" +
				" or (o.medProfile.status = :homeLeaveStatus" +
				" and o.medProfile.returnDate < :requestDate))" +
				" and o.medProfileMoItem.id = o.medProfileMoItem.medProfileItem.medProfileMoItemId" +
				" and o.medProfileMoItem.medProfileItem.status = :verifiedStatus" +
				" and (o.medProfileMoItem.endDate > :requestDate or o.medProfileMoItem.endDate is null)" +
				" and (o.medProfileMoItem.transferFlag = false or o.medProfileMoItem.transferFlag is null)" +
				" and o.medProfileMoItem.suspendCode is null and o.medProfileMoItem.pendingCode is null" +
				" and o.medProfile.wardCode is not null" +
				" and o.workstoreGroupCode = :workstoreGroupCode" +
				" and o.supplyDate = :supplyDate" +
				" and o.status = :preparedStatus" +
				" and o.pivasPrepId is not null")
				.setParameter("hospCode", request.getWorkstore().getHospCode())
				.setParameter("inactiveStatus", MedProfileStatus.Inactive)
				.setParameter("activeStatus", MedProfileStatus.Active)
				.setParameter("homeLeaveStatus", MedProfileStatus.HomeLeave)
				.setParameter("requestDate", request.getCreateDate())
				.setParameter("verifiedStatus", MedProfileItemStatus.Verified)
				.setParameter("workstoreGroupCode", request.getWorkstore().getWorkstoreGroup().getWorkstoreGroupCode())
				.setParameter("supplyDate", request.getSupplyDate())
				.setParameter("preparedStatus", PivasWorklistStatus.Prepared)
				.getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<MedProfile> retrieveMedProfileListForMaterialRequest(DeliveryRequest request) {
		
		return QueryUtils.splitExecute(em.createQuery(
				"select distinct i.materialRequest.medProfile from MaterialRequestItem i" + // 20160615 index check : MaterialRequest.medProfileId MaterialRequestItem,hospCode,status : FK_MATERIAL_REQUEST_02 I_MATERIAL_REQUEST_ITEM_01
				" where i.materialRequest.hospCode = :hospCode" +
				" and i.materialRequest.medProfile.hospCode = :hospCode" +
				" and i.materialRequest.medProfile.status <> :inactiveStatus" +
				" and (i.materialRequest.medProfile.status = :activeStatus" +
				" or (i.materialRequest.medProfile.status = :homeLeaveStatus" +
				" and i.materialRequest.medProfile.returnDate < :requestDate))" +
				" and i.materialRequest.medProfileMoItem.id = i.materialRequest.medProfileMoItem.medProfileItem.medProfileMoItemId" +
				" and i.materialRequest.medProfileMoItem.medProfileItem.status = :verifiedStatus" +
				" and (i.materialRequest.medProfileMoItem.endDate > :requestDate or i.materialRequest.medProfileMoItem.endDate is null)" +
				" and (i.materialRequest.medProfileMoItem.transferFlag = false or i.materialRequest.medProfileMoItem.transferFlag is null)" +
				" and i.materialRequest.medProfileMoItem.suspendCode is null and i.materialRequest.medProfileMoItem.pendingCode is null" +
				" and i.materialRequest.medProfile.wardCode is not null" +
				" and i.hospCode = :hospCode" +
				" and i.status = :outstandingStatus" +
				" and i.materialRequest.medProfileId in :medProfileIdSet")
				.setParameter("hospCode", request.getWorkstore().getHospCode())
		    	.setParameter("inactiveStatus", MedProfileStatus.Inactive)
				.setParameter("activeStatus", MedProfileStatus.Active)
				.setParameter("homeLeaveStatus", MedProfileStatus.HomeLeave)
				.setParameter("requestDate", request.getCreateDate())
				.setParameter("verifiedStatus", MedProfileItemStatus.Verified)
				.setParameter("outstandingStatus", MaterialRequestItemStatus.Outstanding)
				, "medProfileIdSet", request.getMaterialRequestItemMapByMedProfileId().keySet());
	}
    
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public Delivery processMedProfile(DeliveryRequest request, boolean anonymousWard, MedProfile medProfile, boolean persistDelivery) {
    	
    	logger.info("Processing MedProfile(#0) ...", medProfile.getId());
    	
    	try {
	    	Delivery delivery = new Delivery();
	    	if ((medProfile = updateMedProfileFromExternalService(request, medProfile, persistDelivery)) == null) {
	    		return delivery;
	    	}
	    	
	    	List<MedProfileItem> medProfileItemList = medProfile.getMedProfileItemList();
	    	List<PurchaseRequest> purchaseRequestList = retrievePurchaseRequestList(request, medProfile);
	    	List<MaterialRequestItem> materialRequestItemList = retrieveMaterialRequestItemList(request, medProfile);
	    	if (checkAndMarkInvalidWard(request, anonymousWard, medProfile)) {
	    		if (purchaseRequestList.isEmpty()) {
	    			return delivery;
	    		}
	    		medProfileItemList = new ArrayList<MedProfileItem>();
	    	}
	    	
	    	delivery.setWardCode(medProfile.getWardCode());
	    	
	    	DeliveryRequestAlert alert = new DeliveryRequestAlert(medProfile);
	    	List<DeliveryItem> deliveryItemList = processMedProfileItemList(request, anonymousWard, medProfileItemList, purchaseRequestList, materialRequestItemList);
	   		
	    	if (persistDelivery) {
	   			mdsCheck(request, deliveryItemList, medProfile, alert);
		   		medProfileStatManager.recalculateMedProfileErrorStat(medProfile);
	   			markDeliveryGenFlag(medProfile, deliveryItemList);
	   		}
	    	
	   		updateDeliveryRequest(request, alert, deliveryItemList, persistDelivery);
	   		delivery.setDeliveryItemList(deliveryItemList);
	    	return delivery;
	    	
    	} catch (Exception ex) {
    		if( !persistDelivery ){
    			if( ex instanceof AlertProfileException || ex instanceof EhrAllergyException ){
        			throw new DueOrderPrintException(ex, medProfile.getWardCode(),    		
    						(medProfile.getMedCase() == null) ? null : medProfile.getMedCase().getCaseNum(), "Y");
    			}
    			return null;
    		}else{
    			throw new DueOrderPrintException(ex, medProfile.getWardCode(),    		
						(medProfile.getMedCase() == null) ? null : medProfile.getMedCase().getCaseNum());
    		}
    	}
    }
    
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public Delivery processMedProfileForPivas(DeliveryRequest request, MedProfile medProfile) {
    	
    	logger.info("Processing MedProfile(#0) for PIVAS ...", medProfile.getId());
    	
    	try {
	    	Delivery delivery = new Delivery();
	    	String currWardCode = medProfile.getWardCode();
	    	if ((medProfile = updateMedProfileFromExternalService(request, medProfile, true)) == null ||
	    			ObjectUtils.compare(currWardCode, medProfile.getWardCode()) != 0) {
	    		return delivery;
	    	}

	    	delivery.setWardCode(medProfile.getWardCode());
	    	
	    	List<PivasWorklist> pivasWorklists = retrievePivasWorklists(request, medProfile);
	    	
	    	DeliveryRequestAlert alert = new DeliveryRequestAlert(medProfile);
	    	List<DeliveryItem> deliveryItemList = processPivasWorklists(request, pivasWorklists);

	   		markDeliveryGenFlag(medProfile, deliveryItemList);
	    	
	   		updateDeliveryRequest(request, alert, deliveryItemList, true);
	   		delivery.setDeliveryItemList(deliveryItemList);
	    	return delivery;
	    	
    	} catch (Exception ex) {
			throw new DueOrderPrintException(ex, medProfile.getWardCode(),    		
					(medProfile.getMedCase() == null) ? null : medProfile.getMedCase().getCaseNum());
    	}
    }

    private MedProfile updateMedProfileFromExternalService(DeliveryRequest request, MedProfile medProfile, boolean retrievePasInfo) throws AlertProfileException, EhrAllergyException {
    	PasContext context = null;
    	if (retrievePasInfo) {
    		context = medProfileManager.retrieveContext(medProfile, true, false);
    	}
    	AlertProfile alertProfile = retrieveAlertProfile(context, medProfile);

    	MedProfile lockedMedProfile; 
    	if ((lockedMedProfile = findLockedMedProfile(medProfile.getId())) == null) {
    		return null;
    	}

    	if (retrievePasInfo) {
    		auditChanges(context, medProfile);
    		medProfileManager.applyChanges(context, lockedMedProfile);
    	}
    	lockedMedProfile.setAlertProfile(alertProfile);
    	
    	if (lockedMedProfile.getStatus() == MedProfileStatus.Inactive) {
    		return null;
    	}
    	activeWardManager.initInvalidPasWardFlag(lockedMedProfile);
    	
    	return lockedMedProfile;
    }
    
    private boolean checkAndMarkInvalidWard(DeliveryRequest request, boolean anonymousWard, MedProfile medProfile) {
    	
    	if (anonymousWard && isNonMpWard(request.getWorkstore(), medProfile.getWardCode())) {
    		request.setInvalidWardCode(medProfile.getWardCode());
			return true;
    	}
    	
    	return false;
    }
    
    private void markDeliveryGenFlag(MedProfile medProfile, List<DeliveryItem> deliveryItemList) {
    	
    	if (!deliveryItemList.isEmpty()) {
    		medProfile.setDeliveryGenFlag(Boolean.TRUE);
    	}
    }
    
    private void updateDeliveryRequest(DeliveryRequest request, DeliveryRequestAlert alert,
    		List<DeliveryItem> deliveryItemList, boolean persistDelivery) {
    	
    	if (!deliveryItemList.isEmpty() || alert.getMdsCheckResult() != null) {
    		if( persistDelivery ){
	    		if (!em.contains(request)) {
	    			alert.setDeliveryRequest(request);
	    			em.persist(alert);
	    		}
	    		request.addDeliveryRequestAlert(alert);
	    	}else{
	    		alert.setDeliveryRequest(request);
	    		request.addDeliveryRequestAlert(alert);
	    	}
    	}
    }
    
    private void mdsCheck(DeliveryRequest deliveryRequest, List<DeliveryItem> deliveryItemList,
    		MedProfile medProfile, DeliveryRequestAlert alert) {
    	
    	List<MedProfileItem> mpItemList = constructMedProfileItemListForMdsChecking(alert, deliveryItemList);
    	if (mpItemList.size() <= 0) {
    		return;
    	}

    	MedProfileCheckResult result = medProfileCheckManager.checkMedProfileAlertForDueOrderPrint(medProfile, mpItemList, alert.getDdiCheckFlag());
    	if (result == null) {
    		alert.setMdsCheckFlag(false);
    		return;
    	} else if (result.getWithHoldList().isEmpty()) {
    		return;
    	} else {
    		auditLogger.log("#0722",
    				"Due Order Label Generation",
    				medProfile.getId(),
    				medProfile.getPatient().getHkid(),
    				medProfile.getMedCase().getCaseNum(),
    				serializer.exclude("class").exclude("*.class").exclude("alertMsgList.alertList").exclude("alertMsgList.alertXmlList").exclude("*.medItem").exclude("*.medProfileMoItem").deepSerialize(result.getMedProfileAlertMsgList()));
    	}
    	alert.setMdsCheckResult(result);
    	
    	removeDeliveryItemWithNewAlert(deliveryRequest, medProfile, deliveryItemList, result);
    }
    
    private void updateForceProceedInfo(DeliveryRequest deliveryRequest, List<Delivery> deliveryList) {
    	for (Delivery delivery: deliveryList){
    		for (DeliveryItem deliveryItem: delivery.getDeliveryItemList()) {
    			if( deliveryItem.getPurchaseRequest() != null){
    				Long purchaseRequestId = deliveryItem.getPurchaseRequest() == null ? null : deliveryItem.getPurchaseRequest().getId();
    				PurchaseRequest purchaseRequest = this.findPurchaseRequest(purchaseRequestId);
    				if( purchaseRequest != null ){
    	    			purchaseRequest.setForceProceedFlag(deliveryRequest.getForceProceedFlag());
    	    			purchaseRequest.setForceProceedCode(deliveryRequest.getForceProceedCode());
    	    			purchaseRequest.setForceProceedUser(deliveryRequest.getForceProceedUser());
    	    			purchaseRequest.setForceProceedDate(deliveryRequest.getForceProceedDate());
    	    			purchaseRequest.setDueDate(deliveryRequest.getForceProceedDate());
    	    			purchaseRequest.setManualInvoiceNum(deliveryRequest.getManualInvoiceNum());
    	    			purchaseRequest.setManualReceiptNum(deliveryRequest.getManualReceiptNum());
    	    			deliveryItem.setPurchaseRequest(purchaseRequest);
    				}
    			}
    		}
    	}
    }
    
    private List<MedProfileItem> constructMedProfileItemListForMdsChecking(DeliveryRequestAlert alert, List<DeliveryItem> deliveryItemList) {

    	List<MedProfileItem> mpItemList = new ArrayList<MedProfileItem>();
    	
    	if (!ALERT_MDS_ENABLED.get(Boolean.FALSE) || !DELIVERY_ALERT_MDS_CHECK.get(Boolean.FALSE)) {
    		alert.setMdsCheckFlag(false);
    		return mpItemList;
    	}
    	alert.setMdsCheckFlag(DELIVERY_ALERT_MDS_CHECK.get(Boolean.FALSE));
    	alert.setDdiCheckFlag(DELIVERY_ALERT_MDS_DDI_CHECK.get(Boolean.FALSE));
    	
    	// extract MedProfileItem from DeliveryItem
    	for (DeliveryItem deliveryItem : deliveryItemList) {
    		
    		MedProfileMoItem mpMoItem = deliveryItem.getMedProfileMoItem();
    		
    		 // reset mds error
    		if (mpMoItem.getMedProfileItem().getMedProfileErrorLog() != null && mpMoItem.getMedProfileItem().getMedProfileErrorLog().getType() == ErrorLogType.MdsAlert) {
    			mpMoItem.getMedProfileItem().setMedProfileErrorLog(null);
    			mpMoItem.setMdsSuspendReason(null);
    		}
    		if (!mpItemList.contains(mpMoItem.getMedProfileItem())) {
    			mpItemList.add(mpMoItem.getMedProfileItem());
    		}
    	}
    	return mpItemList;
    }
    
    private void removeDeliveryItemWithNewAlert(DeliveryRequest deliveryRequest, MedProfile medProfile,
    		List<DeliveryItem> deliveryItemList, MedProfileCheckResult medProfileCheckResult) {
    	
		// remove DeliveryItem with new alert
		Set<DeliveryItem> removeDeliveryItemSet = new HashSet<DeliveryItem>();
		Set<MedProfileItem> generateMpItemSet = new HashSet<MedProfileItem>();

		for (DeliveryItem deliveryItem : deliveryItemList) {
			MedProfileItem mpItem = deliveryItem.getMedProfileMoItem().getMedProfileItem();
			if (medProfileCheckResult.getWithHoldList().contains(mpItem.getId())) {
				removeDeliveryItemSet.add(deliveryItem);
				generateMpItemSet.add(mpItem);
			}
		}
		
		for (MedProfileItem mpItem : generateMpItemSet) {
			generateErrorLog(deliveryRequest, mpItem, ErrorLogType.MdsAlert, MDS_ERROR_MESSAGE, null);
		}

		deliveryItemList.removeAll(removeDeliveryItemSet);
		deliveryRequest.incrementErrorCount(removeDeliveryItemSet);
    }

	@Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void revertDeliveryGenFlags(List<MedProfile> medProfileList) {
    	
    	Set<Long> idSet = new TreeSet<Long>();
    	
    	for (MedProfile medProfile: medProfileList) {
    		idSet.add(medProfile.getId());
    	}
    	
    	revertDeliveryGenFlagByIdSet(idSet);
    }
	
	@Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public DeliveryRequest beginProcessDeliveryRequest(Workstation workstation, DeliveryRequest request) {
		return persistDeliveryRequest(workstation, request, DeliveryRequestStatus.Processing);
	}
    
    @Override
    public DeliveryRequest persistDeliveryRequest(Workstation workstation, DeliveryRequest request) {

    	return persistDeliveryRequest(workstation, request, DeliveryRequestStatus.Completed);
    }
    
    private DeliveryRequest persistDeliveryRequest(Workstation workstation, DeliveryRequest request, DeliveryRequestStatus initialStatus) {

    	if (request == null) {
    		request = new DeliveryRequest();
    	}

    	Workstore workstore = workstation.getWorkstore();
    	
    	request.setStatus(initialStatus);
    	request.setWorkstore(workstore);
    	request.setWorkstationCode(workstation.getWorkstationCode());
    	
    	if (!Boolean.TRUE.equals(request.getPivasFlag())) {
    		request.setSortName(LABEL_MPDISPLABEL_SORT.get()); 
    		request.setDividerLabelFlag(LABEL_DIVIDERLABEL_ENABLED.get(false));
    		
	    	Set<String> dailyRefillItemCodeSet = new HashSet<String>();
	    	List<ItemDispConfig> itemDispConfigList = itemDispConfigManager.retrieveItemDispConfigList(workstore.getWorkstoreGroup(), false);
	    	for (ItemDispConfig itemDispConfig: itemDispConfigList) {
	    		if (Boolean.TRUE.equals(itemDispConfig.getDailyRefillFlag())) {
	    			dailyRefillItemCodeSet.add(itemDispConfig.getItemCode());
	    		}
	    	}
	    	request.setDailyRefillItemCodeSet(dailyRefillItemCodeSet);
    	}
    	
    	em.persist(request);
    	em.flush();
    	
    	return request;
    }
        
    public Delivery processDeliveryForDrugRefillList(DeliveryRequest request, String wardCode, PrintMode printMode,
    		List<DeliveryItem> deliveryItemList, boolean includeDDnWardStockFlag){
		
		Delivery delivery = createDeliveryForDrugRefillList(request, wardCode, printMode, deliveryItemList);
		
		labelManager.getMpDispLabel(delivery);
		
		if( !includeDDnWardStockFlag ){
			for(Iterator<DeliveryItem> deliveryItemIterator = delivery.getDeliveryItemList().iterator(); deliveryItemIterator.hasNext();){			
				DeliveryItem deliveryItem = (DeliveryItem) deliveryItemIterator.next();
				MedProfilePoItem medProfilePoItem = deliveryItem.resolveMedProfilePoItem(); 
				
				if( medProfilePoItem.getDangerDrugFlag() && !medProfilePoItem.getReDispFlag() ){
					deliveryItem.setDelivery(null);
					deliveryItemIterator.remove();
				}else{
					JaxbWrapper<MpDispLabel> dispLabelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
					MpDispLabel mpDispLabel = dispLabelJaxbWrapper.unmarshall(deliveryItem.getLabelXml());
					
					if( mpDispLabel.getWardStockFlag() && !medProfilePoItem.getReDispFlag() ){
						deliveryItem.setDelivery(null);
						deliveryItemIterator.remove();
					}
				}
			}
		}

		if( delivery.getDeliveryItemList().isEmpty() ){
			return null;
		}
		
		request.getDeliveryList().add(delivery);
		
		return delivery;
    }
    
	@Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public DeliveryRequestGroup processDeliveryRequestGroup(Workstation workstation, DeliveryScheduleItem deliveryScheduleItem){
		DeliveryRequestGroup requestGroup = new DeliveryRequestGroup();
		
		Workstore workstore = workstation.getWorkstore();
		
		requestGroup.setWorkstore(workstore);
		requestGroup.setDeliveryScheduleItem(deliveryScheduleItem);
		requestGroup.setStatus(DeliveryRequestGroupStatus.Processing);
		
		em.persist(requestGroup);
		return requestGroup;
	}
    
	@Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public List<Delivery> createAndProcessDeliveryList(DeliveryRequest request, String wardCode, PrintMode printMode,
    		List<DeliveryItem> deliveryItemList) {
		List<Delivery> deliveryList = createDeliveryList(request, wardCode, printMode, deliveryItemList, false);
		
		if( Boolean.TRUE.equals(request.getForceProceedFlag()) ){
			updateForceProceedInfo(request, deliveryList);
		}
		
		List<RenderAndPrintJob> renderAndPrintJobList = new ArrayList<RenderAndPrintJob>();
		
		List<RenderAndPrintJob> labelRenderAndPrintJobList = null;
		List<DispOrder> dispOrderList = null;
		List<Object> atdpsMessageObjList = null;
		
		if (Boolean.TRUE.equals(request.getPivasFlag())) {
			MutableTriple<List<RenderAndPrintJob>, List<DispOrder>, List<Object>> labelPair = labelManager.printPivasLabelList(deliveryList);
			labelRenderAndPrintJobList = labelPair.getLeft();
			dispOrderList = labelPair.getMiddle();
		} else {
			MutableTriple<List<RenderAndPrintJob>, List<DispOrder>, List<Object>> labelPair = labelManager.printMpDispLabelList(deliveryList, request.getAtdpsPickStationsNumList());
			labelRenderAndPrintJobList = labelPair.getLeft();
			dispOrderList = labelPair.getMiddle();
			atdpsMessageObjList = labelPair.getRight();
		}
		
		updatePrintedCount(deliveryList);
		sendVetOrderMessages(deliveryList, true);
		
		if (labelRenderAndPrintJobList != null && !request.shouldSortByWhole()) {
			renderAndPrintJobList.addAll(labelRenderAndPrintJobList);
		}

		if (Boolean.TRUE.equals(request.getPivasFlag())) {
			renderAndPrintJobList.addAll(mpTrxRptPrintManager.printPivasTrxRpt(deliveryList));
		} else {
			renderAndPrintJobList.addAll(mpTrxRptPrintManager.printMpTrxRpt(deliveryList));
		}
		
		saveDispOrderListToCorp(dispOrderList);
		printAgent.renderAndPrint(renderAndPrintJobList);
		sendAtdpsSocketMessage(atdpsMessageObjList);
		
		postProcessDeliveryList(deliveryList);
		if (labelRenderAndPrintJobList != null && request.shouldSortByWhole()) {
			request.getPendingRenderAndPrintJobList().addAll(labelRenderAndPrintJobList);
		}
		request.getDeliveryList().addAll(deliveryList);
		
		return deliveryList;
    }
	
    private List<Delivery> createDeliveryList(DeliveryRequest request, String wardCode, PrintMode printMode,
    		List<DeliveryItem> deliveryItemList) {
    	
    	return createDeliveryList(request, wardCode, printMode, deliveryItemList, true);
    }
    
    private List<Delivery> createDeliveryList(DeliveryRequest request, String wardCode, PrintMode printMode,
    		List<DeliveryItem> deliveryItemList, boolean addToDeliveryRequest) {

    	Date batchDate = new Date(); 
    	
    	List<Delivery> deliveryList = new ArrayList<Delivery>();
    	
    	for (Pair<Boolean, List<DeliveryItem>> batch: splitBatchList(request, deliveryItemList)) {
	    	Delivery delivery = new Delivery();
	    	delivery.setWardCode(wardCode);
	    	delivery.setPrintMode(printMode);
	    	delivery.setBatchDate(batchDate);
	    	delivery.setStatus(DeliveryStatus.Printed);
	    	delivery.setHospital(request.getWorkstore().getHospital());
	    	delivery.setBatchNum(batchNumGenerator.getNextFormattedBatchNum(
	    			request.getWorkstore().getHospital(), batchDate, determineBatchType(batch.getRight())));
	    	delivery.setErrorFlag(batch.getLeft());
	    	delivery.setDeliveryRequest(request);
	    	
	    	em.persist(delivery);
	    	
	    	for (DeliveryItem deliveryItem: batch.getRight()) {
	    		deliveryItem.setDelivery(delivery);
	    		em.persist(deliveryItem);
	    	}
	    	delivery.setDeliveryItemList(batch.getRight());
	    	em.flush();
	    	
	    	if (addToDeliveryRequest) {
	    		request.getDeliveryList().add(delivery);
	    	}
	    	deliveryList.add(delivery);
    	}
    	
    	return deliveryList;
    }
    
    private List<Pair<Boolean, List<DeliveryItem>>> splitBatchList(DeliveryRequest request, List<DeliveryItem> deliveryItemList) {
    	
    	List<Pair<Boolean, List<DeliveryItem>>> batchList = new ArrayList<Pair<Boolean, List<DeliveryItem>>>();
    	appendBatch(batchList, deliveryItemList, false, request.hasNewNormalItemError());
    	appendBatch(batchList, deliveryItemList, true, request.hasNewSfiItemError());
    	return batchList; 
    }
    
    private void appendBatch(List<Pair<Boolean, List<DeliveryItem>>> batchList, List<DeliveryItem> deliveryItemList,
    		boolean sfiItem, boolean errorFlag) {

    	List<DeliveryItem> partialDeliveryItemList = new ArrayList<DeliveryItem>();
    	for (DeliveryItem deliveryItem: deliveryItemList) {
    		if (sfiItem == (deliveryItem.getPurchaseRequest() != null)) {
    			partialDeliveryItemList.add(deliveryItem);
    		}
    	}
    	
    	if (partialDeliveryItemList.size() > 0) {
    		batchList.add(Pair.of(errorFlag, partialDeliveryItemList));
    	}
    }
    
    private BatchType determineBatchType(List<DeliveryItem> deliveryItemList) {
    	
    	DeliveryItem deliveryItem = MedProfileUtils.getFirstItem(deliveryItemList);

    	if (deliveryItem != null) {
	    	if (deliveryItem.getPurchaseRequest() != null) {
	    		return BatchType.Sfi;
	    	} else if (deliveryItem.getPivasWorklist() != null) {
	    		return BatchType.Pivas;
	    	}
    	}
    	return BatchType.Normal;
    }
    
    private Delivery createDeliveryForDrugRefillList(DeliveryRequest request, String wardCode, PrintMode printMode, List<DeliveryItem> deliveryItemList) {
    	Date batchDate = new Date(); 
    	
    	Delivery delivery = new Delivery();
    	delivery.setWardCode(wardCode);
    	delivery.setPrintMode(printMode);
    	delivery.setBatchDate(batchDate);
    	delivery.setStatus(DeliveryStatus.Printed);
    	delivery.setHospital(request.getWorkstore().getHospital());
    	delivery.setDeliveryRequest(request);
    	
    	for (DeliveryItem deliveryItem: deliveryItemList) {
    		deliveryItem.setDelivery(delivery);
    	}
    	delivery.setDeliveryItemList(deliveryItemList);
    	
    	return delivery;
    }
    
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void endProcessDeliveryRequest(DeliveryRequest request) {
    	
    	request = (DeliveryRequest) em.createQuery(
    			"select o from DeliveryRequest o" + // 20150204 index check : DeliveryRequest.id : PK_DELIVERY_REQUEST   			
    			" where o.id = :id and o.id is not null")
    			.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
    			.setParameter("id", request.getId())
    			.getSingleResult();
    	
    	request.setStatus(DeliveryRequestStatus.Completed);
    	em.flush();
    }
    
    @SuppressWarnings("unchecked")
	@Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void endProcessDeliveryRequestGroup(DeliveryRequestGroup requestGroup) {
    	
    	if( requestGroup.getDeliveryScheduleItem() == null ){
    		return;
    	}
    	
    	DateMidnight requestGroupStartDateMidnight = new DateMidnight(requestGroup.getCreateDate());
    	
    	List<DeliveryRequestGroup> historyRequestGroupList = em.createQuery(
			"select g from DeliveryRequestGroup g" + // 20171023 index check : DeliveryRequestGroup.workstore,createDate : I_DELIVERY_REQUEST_GROUP_01
			" where g.deliveryScheduleItem = :deliveryScheduleItem" +
			" and g.createDate between :fromDate and :toDate" +
			" and g.status in :statusList")
			.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
			.setParameter("deliveryScheduleItem", requestGroup.getDeliveryScheduleItem())
			.setParameter("fromDate", requestGroupStartDateMidnight.toDate())
			.setParameter("toDate", requestGroupStartDateMidnight.plusDays(1).toDate())
			.setParameter("statusList", DeliveryRequestGroupStatus.Active_Processing)
			.getResultList();
    	
    	for(DeliveryRequestGroup historyRequestGroup:historyRequestGroupList){
    		if( historyRequestGroup.getStatus() == DeliveryRequestGroupStatus.Processing ){
    			requestGroup = historyRequestGroup;
    		}else{
    			historyRequestGroup.setStatus(DeliveryRequestGroupStatus.Inactive);
    		}
    	}
    	requestGroup.setStatus(DeliveryRequestGroupStatus.Active);
    	em.flush();
    }
    
    public void postProcessDelivery(Delivery delivery) {
    	postProcessDeliveryList(Arrays.asList(delivery));
    }
    
    @Override
    public void postProcessDeliveryList(List<Delivery> deliveryList) {
    	postProcessDeliveryList(deliveryList, true);
    }
    
    public void postProcessDeliveryList(List<Delivery> deliveryList, boolean lockMedProfiles) {

    	if (lockMedProfiles) {
    		lockMedProfiles(deliveryList);
    	}
    	for (Delivery delivery: deliveryList) {
    		postProcessDeliveryItemList(delivery.getDeliveryItemList());
    	}
    	em.flush();
    }
    
    private void lockMedProfiles(List<Delivery> deliveryList) {
    	
    	Set<Long> idSet = generateOrderedMedProfileIdSet(deliveryList);
    	revertDeliveryGenFlagByIdSet(idSet);
    }
    
    @SuppressWarnings("unchecked")
	private void revertDeliveryGenFlagByIdSet(Set<Long> idSet) {
    	
    	if (idSet == null || idSet.size() <= 0) {
    		return;
    	}
    	
    	List<MedProfile> medProfileList = QueryUtils.splitExecute(em.createQuery(
    			"select p from MedProfile p" + // 20120215 index check : MedProfile.id : PK_MED_PROFILE
    			" where p.id in :idSet")
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock), "idSet", idSet);
    	
    	for (MedProfile medProfile: medProfileList) {
    		medProfile.setDeliveryGenFlag(false);
    		medProfile.setProcessingFlag(false);
    		medProfile.setWorkstationId(null);
    	}
    }
    
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void printRemainingLabelAndReport(DeliveryRequest request) {

    	List<RenderAndPrintJob> renderAndPrintJobList = new ArrayList<RenderAndPrintJob>();
    	
    	if (Boolean.TRUE.equals(request.getPivasFlag())) {
    		renderAndPrintJobList.addAll(sortPivasLabel(request.getPendingRenderAndPrintJobList()));
    	} else if (request.isSortByLocation()) {
			renderAndPrintJobList.addAll(sortNormalLabel(request.getPendingRenderAndPrintJobList()));
		}
    	
		List<MdsExceptionRptContainer> mdsExceptionRptList = mdsExceptionRptManager.retrieveMdsExceptionRptListByDeliveryReqId(request.getId());
		if (!mdsExceptionRptList.isEmpty()) {
			renderAndPrintJobList.addAll(mdsExceptionRptManager.constructRenderAndPrintJob(mdsExceptionRptList));
		}
		
		printAgent.renderAndPrint(renderAndPrintJobList);
    }
    
    private List<RenderAndPrintJob> sortNormalLabel(List<RenderAndPrintJob> renderAndPrintJobList) {
    	
    	List<RenderAndPrintJob> resultList = new ArrayList<RenderAndPrintJob>();
    	resultList.addAll(renderAndPrintJobList);
    	Collections.sort(resultList, COMPARATOR_NORMAL_RENDER_AND_PRINT_JOB);
    	return resultList;
    }
    
    private List<RenderAndPrintJob> sortPivasLabel(List<RenderAndPrintJob> renderAndPrintJobList) {

    	Map<String, List<RenderAndPrintJob>> renderAndPrintJobGroupMap = new HashMap<String, List<RenderAndPrintJob>>();
    	for (RenderAndPrintJob renderJob: renderAndPrintJobList) {
    		String trxId = resolveTrxId(renderJob);
    		if (trxId == null) {
    			continue;
    		}
			List<RenderAndPrintJob> renderAndPrintJobGroup = renderAndPrintJobGroupMap.get(trxId);
			if (renderAndPrintJobGroup == null) {
				renderAndPrintJobGroup = new ArrayList<RenderAndPrintJob>();
				renderAndPrintJobGroupMap.put(trxId, renderAndPrintJobGroup);
			}
			renderAndPrintJobGroup.add(renderJob);
    	}
    	
    	List<List<RenderAndPrintJob>> sortedRenderAndPrintJobGroupList = new ArrayList<List<RenderAndPrintJob>>(renderAndPrintJobGroupMap.values());
    	Collections.sort(sortedRenderAndPrintJobGroupList, COMPARATOR_PIVAS_RENDER_AND_PRINT_JOB_GROUP);

    	List<RenderAndPrintJob> resultList = new ArrayList<RenderAndPrintJob>();
    	for (List<RenderAndPrintJob> renderAndPrintJobGroup :sortedRenderAndPrintJobGroupList) {
    		resultList.addAll(renderAndPrintJobGroup);
    	}
    	
    	return resultList;
    }
    
    private String resolveTrxId(RenderAndPrintJob job) {
    	
    	Object obj = MedProfileUtils.getFirstItem(job.getObjectList());
    	if (obj instanceof PivasWorksheet) {
    		return ((PivasWorksheet) obj).getTrxId();
    	} else if (obj instanceof PivasProductLabel) {
    		return ((PivasProductLabel) obj).getTrxId();
    	} else if (obj instanceof PivasOuterBagLabel) {
    		return ((PivasOuterBagLabel) obj).getTrxId();
    	} else if (obj instanceof MdsOverrideReasonLabel) {
    		return ((MdsOverrideReasonLabel) obj).getTrxId();
    	}
    	return null;
    }
    
    private Set<Long> generateOrderedMedProfileIdSet(List<Delivery> deliveryList) {
    	
    	Set<Long> idSet = new TreeSet<Long>();
    	
    	for (Delivery delivery: deliveryList) {
    		
	    	for (DeliveryItem deliveryItem: delivery.getDeliveryItemList()) {
	    		
	    		MedProfileMoItem medProfileMoItem = deliveryItem.getMedProfileMoItem();
	    		
	    		if (medProfileMoItem != null) {
	    			MedProfile medProfile = medProfileMoItem.getMedProfileItem().getMedProfile();
	    			idSet.add(medProfile.getId());
	    		}
	    	}
    	}
    	
    	return idSet;
    }
    
    private void postProcessDeliveryItemList(List<DeliveryItem> deliveryItemList) {
    	
    	if (deliveryItemList == null) {
    		return;
    	}
    	
    	for (DeliveryItem deliveryItem: deliveryItemList) {
    		postProcessDeliveryItem(deliveryItem);
    	}
    }
    
    private void postProcessDeliveryItem(DeliveryItem deliveryItem) {
    	
    	if (deliveryItem.getPivasWorklist() != null) {
    		postProcessDeliveryItem(deliveryItem, deliveryItem.getPivasWorklist());
    	} else if (deliveryItem.getMaterialRequestItem() != null) {
    		postProcessDeliveryItem(deliveryItem, deliveryItem.getMaterialRequestItem());
    	} else if (deliveryItem.getReplenishmentItem() != null) {
    		postProcessDeliveryItem(deliveryItem, deliveryItem.getReplenishmentItem());
    	} else if (deliveryItem.getPurchaseRequest() != null) {
    		postProcessDeliveryItem(deliveryItem, deliveryItem.getPurchaseRequest());
    	} else if (deliveryItem.getMedProfilePoItem() != null) {
    		postProcessDeliveryItem(deliveryItem, deliveryItem.getMedProfilePoItem());
    	} 
    }
    
    private void postProcessDeliveryItem(DeliveryItem deliveryItem, MedProfilePoItem medProfilePoItem) {

    	Date now = new Date();

    	medProfilePoItem = findMedProfilePoItem(medProfilePoItem.getId());
    	medProfilePoItem.setLastItemCode(medProfilePoItem.getItemCode());
    	if (medProfilePoItem.getTotalIssueQty() != null && medProfilePoItem.getRemainIssueQty() == null) {
    		medProfilePoItem.setRemainIssueQty(medProfilePoItem.getTotalIssueQty());
    	}
    	if (medProfilePoItem.getRemainIssueQty() != null) {
    		medProfilePoItem.setRemainIssueQty(medProfilePoItem.getRemainIssueQty()
    				.subtract(ObjectUtils.firstNonNull(deliveryItem.getPreIssueQty(), deliveryItem.getIssueQty()))
    				.subtract(ObjectUtils.max(deliveryItem.getAdjQty(), BigDecimal.ZERO)).setScale(0, RoundingMode.DOWN).max(BigDecimal.ZERO));
    	}
    	medProfilePoItem.setLastAdjQty(ObjectUtils.firstNonNull(deliveryItem.getAdjQty(), BigDecimal.ZERO));
    	medProfilePoItem.setAdjQty(BigDecimal.ZERO);
    	medProfilePoItem.setLastIssueQty(deliveryItem.getIssueQty());
    	medProfilePoItem.setLastDueDate(deliveryItem.getDueDate());
    	medProfilePoItem.setLastPrintDate(now);
    	medProfilePoItem.setLastDeliveryItemId(deliveryItem.getId());
    	if (medProfilePoItem.getFirstDueDate() == null) {
    		medProfilePoItem.setFirstDueDate(medProfilePoItem.getDueDate());
    	}
    	if (deliveryItem.getNextDueDate() != null) {
    		medProfilePoItem.setDueDate(deliveryItem.getNextDueDate());
    	}

    	updateFirstPrintDate(medProfilePoItem.getMedProfileMoItem(), now);
    }
    
    private void postProcessDeliveryItem(DeliveryItem deliveryItem, ReplenishmentItem replenishmentItem) {
    	
    	Date now = new Date();
    	
    	replenishmentItem = findReplenishmentItem(replenishmentItem.getId());
    	replenishmentItem.setPrintDate(now);

    	updateFirstPrintDate(replenishmentItem.getMedProfilePoItem().getMedProfileMoItem(), now);
    }
    
    private void postProcessDeliveryItem(DeliveryItem deliveryItem, PurchaseRequest purchaseRequest) {
    	
    	Date now = new Date();
    	
    	purchaseRequest = this.findPurchaseRequest(purchaseRequest.getId());
    	purchaseRequest.setPrintDate(now);
    	
    	MedProfilePoItem medProfilePoItem = findMedProfilePoItem(deliveryItem.getMedProfilePoItem().getId());
    	medProfilePoItem.setLastPrintDate(now);
    	
    	updateFirstPrintDate(medProfilePoItem.getMedProfileMoItem(), now);
    }
    
    private void postProcessDeliveryItem(DeliveryItem deliveryItem, PivasWorklist pivasWorklist) {

    	Date now = new Date();
    	
    	pivasWorklist = this.findPivasWorklist(pivasWorklist.getId());
    	pivasWorklist.setStatus(PivasWorklistStatus.Completed);
    	
		List<MaterialRequestItem> materialRequestItemList = this.retrieveUnassignedMaterialRequestItemList(pivasWorklist.getMaterialRequestId());
		for (MaterialRequestItem materialRequestItem: materialRequestItemList) {
			materialRequestItem.setStatus(MaterialRequestItemStatus.Outstanding);
		}
		
    	updateFirstPrintDate(pivasWorklist.getMedProfileMoItem(), now);
    	
		if (Boolean.TRUE.equals(pivasWorklist.getAdHocFlag())) {
			return;
		}
		
    	PivasPrep pivasPrep = this.findPivasPrep(pivasWorklist.getPivasPrep().getId());
		pivasPrep.setDueDate(deliveryItem.getNextDueDate());
    	pivasPrep.setLastPrintDate(now);
    	
    	List<MedProfilePoItem> medProfilePoItemList = retrieveMedProfilePoItemList(pivasWorklist.getMedProfileMoItem());
    	
    	for (MedProfilePoItem medProfilePoItem: medProfilePoItemList) {

    		medProfilePoItem.setLastPrintDate(now);
        	if (medProfilePoItem.getFirstDueDate() == null) {
        		medProfilePoItem.setFirstDueDate(medProfilePoItem.getDueDate());
        	}
        	if (ObjectUtils.compare(deliveryItem.getNextDueDate(), medProfilePoItem.getDueDate()) > 0) {
        		medProfilePoItem.setDueDate(deliveryItem.getNextDueDate());
        	}
    	}
    }
    
    private void postProcessDeliveryItem(DeliveryItem deliveryItem, MaterialRequestItem materialRequestItem) {
    	
    	Date now = new Date();
    	
    	MaterialRequest materialRequest = this.findMaterialRequest(materialRequestItem.getMaterialRequestId());
    	
    	materialRequestItem = this.findMaterialRequestItem(materialRequestItem.getId());
    	materialRequestItem.setStatus(MaterialRequestItemStatus.Processed);
    	materialRequestItem.setIssueQty(deliveryItem.getIssueQty());
    	
    	updateFirstPrintDate(materialRequest.getMedProfileMoItem(), now);
    }
    
    private void updateFirstPrintDate(MedProfileMoItem medProfileMoItem, Date date) {
    	
    	medProfileMoItem = findMedProfileMoItem(medProfileMoItem.getId());
    	if (medProfileMoItem.getFirstPrintDate() == null) {
    		medProfileMoItem.setFirstPrintDate(date);
    	}
    }
        
    private List<DeliveryItem> processMedProfileItemList(DeliveryRequest request, boolean anonymousWard,
    		List<MedProfileItem> medProfileItemList, List<PurchaseRequest> purchaseRequestList, List<MaterialRequestItem> materialRequestItemList) {
    	
    	List<DeliveryItem> deliveryItemList = new ArrayList<DeliveryItem>();
    	
    	for (MedProfileItem medProfileItem: medProfileItemList) {
    		deliveryItemList.addAll(processMedProfileMoItem(request, anonymousWard, medProfileItem.getMedProfileMoItem()));
    	}
    	
    	for (PurchaseRequest purchaseRequest: purchaseRequestList) {
    		deliveryItemList.addAll(processPurchaseRequest(request, anonymousWard, purchaseRequest));
    	}
    	
    	for (MaterialRequestItem materialRequestItem: materialRequestItemList) {
    		deliveryItemList.addAll(processMaterialRequestItem(request, anonymousWard, materialRequestItem));
    	}
    	
    	return deliveryItemList;
    }
    
    private List<DeliveryItem> processPivasWorklists(DeliveryRequest request, List<PivasWorklist> pivasWorklists) {
    	
    	List<DeliveryItem> deliveryItemList = new ArrayList<DeliveryItem>();
    	
    	for (PivasWorklist pivasWorklist: pivasWorklists) {
    		deliveryItemList.addAll(generatePivasDeliveryItem(request, pivasWorklist));
    	}
    	
    	return deliveryItemList;
    }
    
    private List<PivasWorklist> filterPivasWorklistWithInvalidPivasPrepMethod(Workstore workstore, List<PivasWorklist> pivasWorklists) {
    	
    	List<PivasWorklist> resultList = new ArrayList<PivasWorklist>();
    	
    	List<PivasPrepCriteria> criteriaList = new ArrayList<PivasPrepCriteria>();
    	Map<String, PivasWorklist> pivasWorklistMap = new HashMap<String, PivasWorklist>();
    	
    	for (PivasWorklist pivasWorklist: pivasWorklists) {
    		
        	PivasPrepMethod pivasPrepMethod = pivasWorklist.getPivasPrepMethod();
    		
    		PivasPrepCriteria criteria = new PivasPrepCriteria();
    		criteria.setPivasWorklistUuid(UUID.randomUUID().toString());
    		criteria.setPivasFormulaMethodId(pivasPrepMethod.getPivasFormulaMethodId());
    		criteria.setHospCode(workstore.getHospCode());
    		criteria.setWorkstoreCode(workstore.getWorkstoreCode());
    		criteria.setWorkstoreGroupCode(workstore.getWorkstoreGroup().getWorkstoreGroupCode());
    		criteria.setDiluentItemCode(pivasPrepMethod.getDiluentItemCode());
    		criteria.setDiluentManufCode(pivasPrepMethod.getDiluentManufCode());
    		criteria.setWarnCode1(pivasPrepMethod.getWarnCode1());
    		criteria.setWarnCode2(pivasPrepMethod.getWarnCode2());
    		criteria.setPivasPrepCriteriaDetailList(new ArrayList<PivasPrepCriteriaDetail>());
			
			for ( PivasPrepMethodItem pivasPrepMethodItem : pivasPrepMethod.getPivasPrepMethodItemList() ) {
				PivasPrepCriteriaDetail criteriaDetail = new PivasPrepCriteriaDetail();
				criteriaDetail.setPivasFormulaMethodDrugId(pivasPrepMethodItem.getPivasFormulaMethodDrugId());
				criteriaDetail.setSolventItemCode(pivasPrepMethodItem.getSolventItemCode());
				criteriaDetail.setSolventManufCode(pivasPrepMethodItem.getSolventManufCode());
				criteria.getPivasPrepCriteriaDetailList().add(criteriaDetail);
			}
			
			
			pivasWorklistMap.put(criteria.getPivasWorklistUuid(), pivasWorklist);
			criteriaList.add(criteria);
    	}
    	
    	List<PivasPrepValidateResponse> responseList = dmsPmsServiceProxy.validatePivasPrepMethod(criteriaList);
    	for (PivasPrepValidateResponse response: responseList) {
    		if (Boolean.TRUE.equals(response.getValidationResult())) {
    			resultList.add(pivasWorklistMap.get(response.getPivasWorklistUuid()));
    		}
    	}
    	
    	return resultList;
    }
    
    private List<DeliveryItem> processMedProfileMoItem(DeliveryRequest request, boolean anonymousWard, MedProfileMoItem medProfileMoItem) {
    	
    	List<MedProfilePoItem> medProfilePoItemList = medProfileMoItem.getMedProfilePoItemList();
    	List<DeliveryItem> deliveryItemList = new ArrayList<DeliveryItem>();
    	
    	for (MedProfilePoItem medProfilePoItem: medProfilePoItemList) {

    		MedProfileItem medProfileItem = medProfilePoItem.getMedProfileMoItem().getMedProfileItem();
    		List<DeliveryItem> newDeliveryItemList = request.isBatchPrint() ? generateDeliveryItemListForBatchPrint(request, medProfilePoItem):
    				generateDeliveryItemList(request, medProfilePoItem);
    		
    		if (!newDeliveryItemList.isEmpty()) {
	    		if (logError(request, medProfileItem, newDeliveryItemList, !anonymousWard, medProfileItem.isErrorLogged())) {
	    			medProfileItem.setErrorLogged(true);
	    			request.incrementErrorCount(newDeliveryItemList);
	    		} else {
	    			deliveryItemList.addAll(newDeliveryItemList);
	    		}
    		}
    	}
    	
    	return deliveryItemList;
    }
    
    private List<DeliveryItem> processPurchaseRequest(DeliveryRequest request, boolean anonymousWard, PurchaseRequest purchaseRequest) {
    	
    	List<DeliveryItem> deliveryItemList = new ArrayList<DeliveryItem>();
    	
    	MedProfileItem medProfileItem = purchaseRequest.getMedProfilePoItem().getMedProfileMoItem().getMedProfileItem();
    	List<DeliveryItem> newDeliveryItemList = generateDeliveryItemList(request, purchaseRequest);
    	
    	if (!newDeliveryItemList.isEmpty()) {
    		if (logError(request, medProfileItem, newDeliveryItemList, !anonymousWard, medProfileItem.isErrorLogged())) {
    			medProfileItem.setErrorLogged(true);
    			request.incrementErrorCount(newDeliveryItemList);
    		} else {
    			deliveryItemList.addAll(newDeliveryItemList);
    		}
    	}
    	
    	return deliveryItemList;
    }
    
    private List<DeliveryItem> processMaterialRequestItem(DeliveryRequest request, boolean anonymousWard, MaterialRequestItem materialRequestItem) {
    	
    	List<DeliveryItem> deliveryItemList = new ArrayList<DeliveryItem>();
    	
    	MedProfileItem medProfileItem = materialRequestItem.getMedProfilePoItem().getMedProfileMoItem().getMedProfileItem();
    	List<DeliveryItem> newDeliveryItemList = generateDeliveryItemList(request, materialRequestItem);
    	
    	if (!newDeliveryItemList.isEmpty()) {
    		if (logError(request, medProfileItem, newDeliveryItemList, !anonymousWard, medProfileItem.isErrorLogged())) {
    			medProfileItem.setErrorLogged(true);
    			request.incrementErrorCount(newDeliveryItemList);
    		} else {
    			deliveryItemList.addAll(newDeliveryItemList);
    		}
    	}
    	
    	return deliveryItemList;
    }
    
    private boolean logError(DeliveryRequest request, MedProfileItem medProfileItem,
    		List<DeliveryItem> deliveryItemList, boolean checkWardTransfer, boolean checkErrorOnly) {
    	
    	boolean anyError =
    			logMissingSpecialty(request, medProfileItem, deliveryItemList, checkErrorOnly) ||
    			(checkWardTransfer && logWardTransfer(request, medProfileItem, deliveryItemList, checkErrorOnly)) ||
    			logSuspend(request, medProfileItem, deliveryItemList, checkErrorOnly) ||
    			logFrozen(request, medProfileItem, deliveryItemList, checkErrorOnly);
    	
    	if (!anyError && !checkErrorOnly) {
    		medProfileItem.setMedProfileErrorLog(null);
    		medProfileItem.getMedProfileMoItem().setMdsSuspendReason(null);
    	}
    	
    	return anyError;
    }
    
    private boolean logWardTransfer(DeliveryRequest request, MedProfileItem medProfileItem,
    		List<DeliveryItem> deliveryItemList, boolean checkErrorOnly) {
    	
    	if (medProfileItem.getMedProfile().isWardCodeChanged()) {
			if (!checkErrorOnly) {
				generateErrorLog(request, medProfileItem, ErrorLogType.WardTransfer, null, null);
			}
			return true;
    	}
    	return false;
    }
    
    private boolean logMissingSpecialty(DeliveryRequest request, MedProfileItem medProfileItem,
    		List<DeliveryItem> deliveryItemList, boolean checkErrorOnly) {
    	
		if (medProfileItem.getMedProfile().getWardCode() == null) {
			if (!checkErrorOnly) {
				generateErrorLog(request, medProfileItem, ErrorLogType.MissingSpecialty, null, null);
			}
			return true;
		}
		return false;
    }
    
    private boolean logSuspend(DeliveryRequest request, MedProfileItem medProfileItem,
    		List<DeliveryItem> deliveryItemList, boolean checkErrorOnly) {
    	
    	if (deliveryItemList == null) {
    		return false;
    	}
    	
    	for (DeliveryItem deliveryItem: deliveryItemList) {
    		
    		MedProfilePoItem medProfilePoItem = deliveryItem.resolveMedProfilePoItem();
    		if (medProfilePoItem == null) {
    			continue;
    		}
    		
    		if (isItemSuspended(request.getWorkstore(), medProfilePoItem.getItemCode())) {
    			if (!checkErrorOnly) {
    				generateErrorLog(request, medProfileItem, ErrorLogType.Suspend, null,
    						createErrorInfo(medProfilePoItem, ErrorLogType.Suspend));
    			}
    			return true;
    		}
    	}
    	return false;
    }
    
    private boolean isItemSuspended(Workstore workstore, String itemCode) {
    	
    	if (itemCode == null || MedProfileConstant.ITEM_CODE_NOT01.equals(itemCode)) {
    		return false;
    	}
    	
		MsWorkstoreDrug msWorkstoreDrug = dmDrugCacher.getMsWorkstoreDrug(
				workstore, itemCode);
		
		return msWorkstoreDrug == null || "Y".equalsIgnoreCase(msWorkstoreDrug.getSuspend());
    }
    
    private ErrorInfo createErrorInfo(MedProfilePoItem medProfilePoItem, ErrorLogType errorLogType) {
    	
		ErrorInfo errorInfo = new ErrorInfo();
		
		ErrorInfoItem errorInfoItem = new ErrorInfoItem();
		BeanUtils.copyProperties(medProfilePoItem, errorInfoItem);
		
		MedProfileMoItem medProfileMoItem = medProfilePoItem.getMedProfileMoItem();
		if (medProfileMoItem.isManualItem()) {
			errorInfoItem.setDrugSynonym(null);
		} else {
			RxItem rxItem = medProfileMoItem.getRxItem();
			errorInfoItem.setDrugOrderTlf(rxItem == null ? null : rxItem.getFormattedDrugOrderTlf());
		}
		
		switch (errorLogType) {
			case Suspend:
				errorInfo.getSuspendItemList().add(errorInfoItem);
				break;
			case Frozen:
				errorInfo.getFrozenItemList().add(errorInfoItem);
				break;
			default:
		}
		
		return errorInfo;
    }
    
    private boolean logFrozen(DeliveryRequest request, MedProfileItem medProfileItem,
    		List<DeliveryItem> deliveryItemList, boolean checkErrorOnly) {
    	
    	if (deliveryItemList == null) {
    		return false;
    	}
    	
    	for (DeliveryItem deliveryItem: deliveryItemList) {
    		
    		MedProfileMoItem medProfileMoItem = deliveryItem.getMedProfileMoItem();
    		MedProfilePoItem medProfilePoItem = deliveryItem.resolveMedProfilePoItem();
    		if (medProfileMoItem == null || medProfilePoItem == null) {
    			continue;
    		}
    		
    		if (medProfileMoItem.isManualItem() && Boolean.TRUE.equals(medProfileMoItem.getFrozenFlag()) &&
    				!Boolean.TRUE.equals(medProfileMoItem.getMedProfileItem().getMedProfile().getInactivePasWardFlag())) {
    			if (!checkErrorOnly) {
    				generateErrorLog(request, medProfileItem, ErrorLogType.Frozen, null,
    						createErrorInfo(medProfilePoItem, ErrorLogType.Frozen));
    			}
    			return true;
    		}
    	}
    	return false;
    }
    
    private void generateErrorLog(DeliveryRequest request, MedProfileItem medProfileItem, ErrorLogType errorLogType,
    		String message, ErrorInfo errorInfo) {
    	
		MedProfileErrorLog errorLog = new MedProfileErrorLog();
		errorLog.setType(errorLogType);
		errorLog.setMessage(message);
		errorLog.setMedProfileItem(medProfileItem);
		errorLog.setDeliveryRequestId(request == null ? null : request.getId());
		errorLog.setCreateDate(new Date());
		errorLog.setErrorInfo(errorInfo);
		em.persist(errorLog);

		medProfileItem.setMedProfileErrorLog(errorLog);
		em.flush();
		
		switch (errorLogType) {
			case Suspend:
				auditLogger.log("#0536", medProfileItem.getMedProfile().getMedCase().getCaseNum(),
						medProfileItem.getMedProfile().getPatient().getHkid(), errorLog.getId());
			break;
			case MdsAlert:
				MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
				medProfileMoItem.setMdsSuspendReason(message);
				
				auditLogger.log("#0537", medProfileItem.getMedProfile().getMedCase().getCaseNum(),
						medProfileItem.getMedProfile().getPatient().getHkid(), errorLog.getId());
			break;
			default:
		}
    }
    
    private List<DeliveryItem> generateDeliveryItemList(DeliveryRequest request, MedProfilePoItem medProfilePoItem) {
    	
    	List<DeliveryItem> deliveryItemList = new ArrayList<DeliveryItem>();
    	
    	if (!meetCriteria(request, medProfilePoItem)) {
    		return deliveryItemList;
    	}
    	
    	deliveryItemList.add(generateNormalDeliveryItem(request, medProfilePoItem));
    	
    	return deliveryItemList;
    }
    
    private List<DeliveryItem> generateDeliveryItemList(DeliveryRequest request, PurchaseRequest purchaseRequest) {
    	
    	List<DeliveryItem> deliveryItemList = new ArrayList<DeliveryItem>();
    	
    	if (!meetCriteria(request, purchaseRequest)) {
    		return deliveryItemList;
    	}
    	
    	appendDeliveryItemForSfi(purchaseRequest, deliveryItemList);
    	
    	return deliveryItemList;
    }
    
    private List<DeliveryItem> generateDeliveryItemList(DeliveryRequest request, MaterialRequestItem materialRequestItem) {

    	List<DeliveryItem> deliveryItemList = new ArrayList<DeliveryItem>();
    	
    	BigDecimal issueQty = request.getMaterialRequestItemMap().get(materialRequestItem.getId()).getIssueQty();
    	
    	if (ObjectUtils.compare(BigDecimal.ZERO, issueQty) == 0) {
    		materialRequestItem.setIssueQty(issueQty);
    		materialRequestItem.setStatus(MaterialRequestItemStatus.Processed);
    	} else {
    		appendDeliveryItem(materialRequestItem, issueQty, deliveryItemList);
    	}
    	
    	return deliveryItemList;
    }
    
    private List<DeliveryItem> generateDeliveryItemListForBatchPrint(DeliveryRequest request,
    		MedProfilePoItem medProfilePoItem) {
    	
    	List<DeliveryItem> deliveryItemList = new ArrayList<DeliveryItem>();
    	
    	if (!meetCriteria(request.getMedProfilePoItemIdSet(), medProfilePoItem)) {
    		return deliveryItemList;
    	}
    	
    	appendDeliveryItemForNew(deliveryItemList, medProfilePoItem); 
    	
    	return deliveryItemList;
    }
    
    private DeliveryItem generateNormalDeliveryItem(DeliveryRequest request, MedProfilePoItem medProfilePoItem) {
    	
    	DeliveryItem deliveryItem = new DeliveryItem();
    	deliveryItem.setStatus(DeliveryItemStatus.Printed);
    	deliveryItem.setMedProfilePoItem(medProfilePoItem);
    	deliveryItem.setRefillFlag(medProfilePoItem.isRefill());
    	deliveryItem.setMedProfile(medProfilePoItem.getMedProfileMoItem().getMedProfileItem().getMedProfile());
    	
    	boolean dailyRefill = Integer.valueOf(1).equals(medProfilePoItem.getRefillDurationInDays()) &&
    			request.getDailyRefillItemCodeSet().contains(medProfilePoItem.getItemCode());
    	
    	updateIssueQtyAndDuration(request.getCreateDate(), request.getRefillDueDate(), dailyRefill, deliveryItem, medProfilePoItem);
    	
    	return deliveryItem;
    }
    
    private List<DeliveryItem> generatePivasDeliveryItem(DeliveryRequest request, PivasWorklist pivasWorklist) {
    	
		PivasPrepMethod pivasPrepMethod = pivasWorklist.getPivasPrepMethod();

    	if (isItemSuspended(request.getWorkstore(), pivasPrepMethod.getDiluentItemCode())) {
			return new ArrayList<DeliveryItem>();
    	}
    	
    	List<DeliveryItem> deliveryItemList = new ArrayList<DeliveryItem>();
    	
    	for (PivasPrepMethodItem pivasPrepMethodItem: pivasPrepMethod.getPivasPrepMethodItemList()) {
    		
    		if (isItemSuspended(request.getWorkstore(), pivasPrepMethodItem.getDrugItemCode()) ||
    				isItemSuspended(request.getWorkstore(), pivasPrepMethodItem.getSolventItemCode())) {
    			return new ArrayList<DeliveryItem>();
    		}
    	
	    	DeliveryItem deliveryItem = new DeliveryItem();
	    	deliveryItem.setStatus(DeliveryItemStatus.Printed);
	    	
	    	deliveryItem.setRefillFlag(pivasWorklist.getRefillFlag());
	    	deliveryItem.setMedProfile(pivasWorklist.getMedProfile());
	    	deliveryItem.setPivasWorklist(pivasWorklist);

	    	deliveryItem.setAdjQty(ObjectUtils.defaultIfNull(pivasWorklist.getAdjQty(), BigDecimal.ZERO));
	    	deliveryItem.setIssueDuration(BigDecimal.ZERO);
	    	deliveryItem.setPreIssueQty(ObjectUtils.defaultIfNull(pivasWorklist.getIssueQty(), BigDecimal.ZERO));
	    	deliveryItem.setIssueQty(ObjectUtils.max(deliveryItem.getPreIssueQty().add(deliveryItem.getAdjQty()).setScale(0, RoundingMode.UP), BigDecimal.ZERO));
	    	
	    	deliveryItem.setDueDate(pivasWorklist.getDueDate());
	    	deliveryItem.setNextDueDate(pivasWorklist.getNextDueDate());
	    	deliveryItem.setItemCode(pivasPrepMethodItem.getDrugItemCode());
	    	
	    	deliveryItemList.add(deliveryItem);
    	}
    	
    	return deliveryItemList;
    }
    
    private boolean meetCriteria(DeliveryRequest request, MedProfilePoItem medProfilePoItem) {

    	MedProfileMoItem medProfileMoItem = medProfilePoItem.getMedProfileMoItem();
    	MedProfile medProfile = medProfileMoItem.getMedProfileItem().getMedProfile();
    	
    	if (!request.isNormalGenerationRequest()) {
    		return false;
    	}
    	
    	if (Boolean.TRUE.equals(medProfile.getInactivePasWardFlag()) && !medProfileMoItem.isManualItem()) {
    		return false;
    	}
    	
    	if (!Boolean.TRUE.equals(medProfile.getPrivateFlag()) && medProfilePoItem.isSfiItem()) {
    		return false;
    	}
    	
    	if (medProfileMoItem.getMedProfileItem().getStatus() != MedProfileItemStatus.Verified) {
    		return false;
    	}
    	
    	if (Boolean.TRUE.equals(medProfileMoItem.getTransferFlag())) {
    		return false;
    	}
    	
    	if (medProfileMoItem.isManualItem() && Boolean.TRUE.equals(medProfileMoItem.getFrozenFlag()) && Boolean.TRUE.equals(medProfile.getInactivePasWardFlag())) {
    		return false;
    	}
    	
    	if (medProfileMoItem.getSuspendCode() != null || medProfileMoItem.getPendingCode() != null) {
    		return false;
    	}
    	
    	if (Boolean.TRUE.equals(medProfileMoItem.getPivasFlag())) {
    		return false;
    	}
    	
    	if (medProfilePoItem.getDueDate() == null ||
    			medProfilePoItem.getDueDate().compareTo(MedProfileUtils.getOverdueStartDate(request.getCreateDate())) < 0) {
    		return false;
    	}
    	
    	if (medProfilePoItem.getEndDate() != null && medProfilePoItem.getEndDate().compareTo(medProfilePoItem.getDueDate()) <= 0) {
    		return false;
    	}
    	
    	if (medProfilePoItem.getRemainIssueQty() != null && medProfilePoItem.getRemainIssueQty().compareTo(BigDecimal.ZERO) <= 0) {
    		return false;
    	}
    	
    	if (medProfilePoItem.isRefill()) {
    		
    		if (!Boolean.TRUE.equals(request.getRefillFlag())) {
    			return false;
    		}
    		
    		if (request.getRefillDueDate() != null &&
    				request.getRefillDueDate().compareTo(medProfilePoItem.getDueDate()) < 0) {
    			return false;
    		}
    		
    	} else {
    		
    		if (!Boolean.TRUE.equals(request.getNewOrderFlag()) &&
    			(!Boolean.TRUE.equals(request.getUrgentFlag()) || !Boolean.TRUE.equals(medProfileMoItem.getUrgentFlag())) &&
    			(!Boolean.TRUE.equals(request.getStatFlag()) || !Boolean.TRUE.equals(medProfilePoItem.getStatFlag()))) {
    			return false;
    		}
    		
    		if (request.getDueDate() != null &&
    				request.getDueDate().compareTo(medProfilePoItem.getDueDate()) < 0) {
    			return false;
    		}
    	}
    	
    	if (medProfilePoItem.getLastDueDate() != null &&
    			(Boolean.TRUE.equals(medProfilePoItem.getSingleDispFlag()) || MedProfileUtils.compare(medProfilePoItem.getRefillDuration(), Integer.valueOf(0)) <= 0)) {
    		return false;
    	}
    	
    	if (Integer.valueOf(1).equals(medProfilePoItem.getRefillDurationInDays()) &&
    			request.getDailyRefillItemCodeSet().contains(medProfilePoItem.getItemCode()) &&
    			MedProfileUtils.getNextDayBegin(request.getCreateDate()).compareTo(medProfilePoItem.getDueDate()) <= 0) {
    		return false;
    	}
    	    	
    	if (Boolean.TRUE.equals(medProfileMoItem.getUnitDoseExceptFlag())) {
    		return false;
    	}
    	
    	return true;
    }
    
    private boolean meetCriteria(DeliveryRequest request, PurchaseRequest purchaseRequest) {
    	
    	if (!request.isSfiGenerationRequest()) {
    		return false;
    	}
    	
    	if (!request.getPurchaseRequestIdSet().contains(purchaseRequest.getId())) {
    		return false;
    	}
    	
    	if (purchaseRequest.getPrintDate() != null || Boolean.TRUE.equals(purchaseRequest.getDirectPrintFlag())) {
    		return false;
    	}

    	if (purchaseRequest.getInvoiceDate() == null) {
    		return false;
    	}
    	
    	if (purchaseRequest.getInvoiceStatus() != InvoiceStatus.Settled && 
    			(purchaseRequest.getInvoiceStatus() != InvoiceStatus.Outstanding || 
    			(!Boolean.TRUE.equals(purchaseRequest.getForceProceedFlag()) && !Boolean.TRUE.equals(request.getForceProceedFlag())))) {
    		return false;
    	}
    	
    	if (request.getDueDate() != null &&
    			request.getDueDate().compareTo(purchaseRequest.getDueDate()) < 0) {
    		return false;
    	}
    	
    	return true;
    }
    
    private boolean meetCriteria(Set<Long> medProfilePoItemIdSet, MedProfilePoItem medProfilePoItem) {
    	
    	MedProfileMoItem medProfileMoItem = medProfilePoItem.getMedProfileMoItem();
    	MedProfile medProfile = medProfileMoItem.getMedProfileItem().getMedProfile();
    	
    	if (!medProfilePoItemIdSet.contains(medProfilePoItem.getId())) {
    		return false;
    	}
    	
    	if (Boolean.TRUE.equals(medProfile.getInactivePasWardFlag()) && !medProfileMoItem.isManualItem()) {
    		return false;
    	}
    	
    	if (!Boolean.TRUE.equals(medProfile.getPrivateFlag()) && medProfilePoItem.isSfiItem()) {
    		return false;
    	}
    	
    	if (medProfilePoItem.getLastDueDate() != null) {
    		return false;
    	}

    	if (medProfileMoItem.getMedProfileItem().getStatus() != MedProfileItemStatus.Verified) {
    		return false;
    	}
    	
    	if (Boolean.TRUE.equals(medProfileMoItem.getTransferFlag())) {
    		return false;
    	}
    	
    	if (medProfileMoItem.isManualItem() && Boolean.TRUE.equals(medProfileMoItem.getFrozenFlag()) && Boolean.TRUE.equals(medProfile.getInactivePasWardFlag())) {
    		return false;
    	}
    	
    	if (medProfileMoItem.getSuspendCode() != null || medProfileMoItem.getPendingCode() != null) {
    		return false;
    	}
    	
    	if (Boolean.TRUE.equals(medProfileMoItem.getPivasFlag())) {
    		return false;
    	}
    	
    	return true;
    }
    
    private void updateIssueQtyAndDuration(Date requestDate, Date refillDueDate, boolean dailyRefill,
    		DeliveryItem deliveryItem, MedProfilePoItem medProfilePoItem) {
    	
    	Date dueDate = medProfilePoItem.getDueDate();
    	int refillDays = medProfilePoItem.getRefillDurationInDays() == null ? 0 : medProfilePoItem.getRefillDurationInDays().intValue();
    	BigDecimal adjQty = ObjectUtils.firstNonNull(medProfilePoItem.getAdjQty(), BigDecimal.ZERO);
    	BigDecimal issueQty = ObjectUtils.max(medProfilePoItem.getIssueQty(), BigDecimal.ZERO);

    	checkUpdateIssueQtyAndDurationInputs(dueDate);
		long dispenseDays = 0;
		
    	if (refillDays != 0) {
	    	Date refillUntil = calculateRefillUntil(requestDate, refillDueDate, dailyRefill, medProfilePoItem);
	    	deliveryItem.setNextDueDate(refillUntil);
	    	
	    	dispenseDays = dailyRefill ? 1 : BigDecimal.valueOf(Math.max(refillUntil.getTime() - dueDate.getTime(), 0))
	    			.divide(BigDecimal.valueOf(86400000), 0, RoundingMode.UP).longValue();
	    	
	    	issueQty = BigDecimal.valueOf(dispenseDays).multiply(issueQty).divide(BigDecimal.valueOf(refillDays), 4, RoundingMode.FLOOR);
    	}
		deliveryItem.setDueDate(dueDate);
		deliveryItem.setAdjQty(adjQty);
    	deliveryItem.setIssueDuration(BigDecimal.valueOf(dispenseDays));
		deliveryItem.setPreIssueQty(issueQty);
    	deliveryItem.setIssueQty(ObjectUtils.max(ObjectUtils.min(issueQty.add(adjQty).setScale(0, RoundingMode.UP),
    			medProfilePoItem.getRemainIssueQty(), medProfilePoItem.getTotalIssueQty()), BigDecimal.ZERO));
    }
    
    private void checkUpdateIssueQtyAndDurationInputs(Date dueDate) {

    	if (dueDate == null) {
    		throw new RuntimeException("Null value not allowed for MedProfilePoItem.dueDate");
    	}
    }
    
    private Date calculateRefillUntil(Date requestDate, Date refillDueDate, boolean dailyRefill, MedProfilePoItem medProfilePoItem) {
    	
    	requestDate = requestDate == null ? new Date() : requestDate;
    	
    	int refillDays = medProfilePoItem.getRefillDurationInDays() == null ? 0 : medProfilePoItem.getRefillDurationInDays().intValue();    	
    	
    	Calendar refillUntil = Calendar.getInstance();
    	refillUntil.setTime(medProfilePoItem.getDueDate());
    	
    	if (dailyRefill) {
    		if (MedProfileUtils.compare(medProfilePoItem.getDueDate(), requestDate) < 0) {
    			refillUntil.setTime(MedProfileUtils.combineDateTime(requestDate, medProfilePoItem.getDueDate()));
    		}
    		refillUntil.add(Calendar.DATE, 1);
    	} else if (refillDueDate == null || Boolean.TRUE.equals(medProfilePoItem.getSingleDispFlag())) {
    		refillUntil.add(Calendar.DATE, refillDays);
    	} else {
	    	while (refillUntil.getTime().before(refillDueDate)) {
	    		refillUntil.add(Calendar.DATE, refillDays);
	    	}
    	}
    	
    	Date endDate = medProfilePoItem.getEndDate();
    	if (endDate != null && refillUntil.getTime().after(endDate)) {
    		refillUntil.setTime(endDate);
    	}
    	
    	return refillUntil.getTime();
    }
    
	@SuppressWarnings("unchecked")
	private List<MedProfile> fetchMedProfileList(Ward ward, DeliveryRequest request) {
    	
		if (request.isBatchPrint()) {
			return QueryUtils.splitExecute(em.createQuery(
					"select o from MedProfile o" + // 20170418 index check : MedProfile.id : PK_MED_PROFILE
					" where o.id in :idList")
					.setHint(QueryHints.FETCH, MED_PROFILE_QUERY_HINT_FETCH_1)
					.setHint(QueryHints.FETCH, MED_PROFILE_QUERY_HINT_FETCH_2)
					, "idList", request.getMedProfileIdList());
		} else {
	    	Query query = generateFindQuery(request);
	    	updateQueryParameters(query, ward, request);
	    	
	    	return query.getResultList();
		}
    }
	
    private Query generateFindQuery(DeliveryRequest request) {
    	
    	StringBuilder querySb = new StringBuilder(    	
				"select distinct m from MedProfile m" + // 20140709 index check : MedCase.caseNum Patient.hkid MedProfilePoItem.hospital,dueDate : I_MED_CASE_01 I_PATIENT_02 I_MED_PROFILE_PO_ITEM_01
				" join m.medProfileItemList i join i.medProfileMoItem.medProfilePoItemList p" +
				" where p.hospCode = :hospCode" +
				" and m.hospCode = :hospCode");
    	
    	if (request.getCaseNum() != null) {
    		querySb.append(" and m.medCase.caseNum = :caseNum");
    	} else if (request.getHkid() != null) {
    		querySb.append(" and m.patient.hkid = :hkid");
    	} else {
    		querySb.append(" and m.wardCode = :wardCode");
        	querySb.append(" and p.dueDate >= :startDate");    		
    	}
    	querySb.append(" and (p.remainIssueQty > 0 or p.remainIssueQty is null)");
    	querySb.append(" and m.status <> :inactiveStatus");
    	querySb.append(" and (m.status = :activeStatus or (m.status = :homeLeaveStatus and m.returnDate < :requestDate))");
    	querySb.append(" and i.status = :verifiedStatus");
    	querySb.append(" and (i.medProfileMoItem.transferFlag = false or i.medProfileMoItem.transferFlag is null)");
    	querySb.append(" and i.medProfileMoItem.suspendCode is null and i.medProfileMoItem.pendingCode is null");  
    	querySb.append(" and (i.medProfileMoItem.pivasFlag = false or i.medProfileMoItem.pivasFlag is null)");
    	querySb.append(" and (p.lastDueDate is null or (p.singleDispFlag = false and p.refillDuration > 0))");
    	querySb.append(" and (p.dueDate < p.medProfileMoItem.endDate or p.medProfileMoItem.endDate is null)");    	
    	querySb.append(" and (m.privateFlag = true or p.actionStatus not in :sfiActionStatusList)");
    	querySb.append(" and (i.medProfileMoItem.unitDoseExceptFlag = false or i.medProfileMoItem.unitDoseExceptFlag is null)");
    	
    	
    	StringBuilder orderTypeQuerySb = new StringBuilder();
    	if (request.containsAnyNew()) {
    		orderTypeQuerySb.append(" (p.lastDueDate is null");
    		if (request.getDueDate() != null) {
    			orderTypeQuerySb.append(" and p.dueDate < :t1");
    		}
    		
    		if (Boolean.TRUE.equals(request.getUrgentFlag()) && !Boolean.TRUE.equals(request.getNewOrderFlag()) &&
    				!Boolean.TRUE.equals(request.getStatFlag())) {
    			orderTypeQuerySb.append(" and i.medProfileMoItem.urgentFlag = true");
    		}
    		if (Boolean.TRUE.equals(request.getStatFlag()) && !Boolean.TRUE.equals(request.getNewOrderFlag()) &&
    				!Boolean.TRUE.equals(request.getUrgentFlag())) {
    			orderTypeQuerySb.append(" and p.statFlag = true");
    		}
    		
    		orderTypeQuerySb.append(")");
    	}
    	
    	if (Boolean.TRUE.equals(request.getRefillFlag())) {
    		if (orderTypeQuerySb.length() > 0) {
    			orderTypeQuerySb.append(" or");
    		}
    		orderTypeQuerySb.append(" (p.lastDueDate is not null");
    		
    		if (request.getRefillDueDate() != null) {
    			orderTypeQuerySb.append(" and p.dueDate < :t2");
    		}
    		orderTypeQuerySb.append(")");
    	}
    	
    	return em.createQuery(querySb.append(" and (").append(orderTypeQuerySb).append(" ) order by m.id").toString());
    }
    
    private void updateQueryParameters(Query query, Ward ward, DeliveryRequest request) {
    	
    	Date t1 = request.getDueDate();
    	Date t2 = request.getRefillDueDate();
    	
    	query.setParameter("hospCode", request.getWorkstore().getHospCode());
    	if (request.getCaseNum() != null) {
        	query.setParameter("caseNum", request.getCaseNum());
    	} else if (request.getHkid() != null) {
        	query.setParameter("hkid", request.getHkid());
    	} else {
        	query.setParameter("wardCode", ward.getWardCode());
        	query.setParameter("startDate", MedProfileUtils.getOverdueStartDate(request.getCreateDate()));
    	}
    	query.setParameter("inactiveStatus", MedProfileStatus.Inactive);
    	query.setParameter("activeStatus", MedProfileStatus.Active);
    	query.setParameter("homeLeaveStatus", MedProfileStatus.HomeLeave);
    	query.setParameter("requestDate", request.getCreateDate());
    	query.setParameter("verifiedStatus", MedProfileItemStatus.Verified);
    	query.setParameter("sfiActionStatusList", ActionStatus.PurchaseByPatient_SafetyNet);
    	
    	if (request.containsAnyNew() && t1 != null) {
    		query.setParameter("t1", t1);
    	}
    	if (Boolean.TRUE.equals(request.getRefillFlag() && t2 != null)) {
    		query.setParameter("t2", t2);
    	}
    	query.setHint(QueryHints.FETCH, MED_PROFILE_QUERY_HINT_FETCH_1);
    	query.setHint(QueryHints.FETCH, MED_PROFILE_QUERY_HINT_FETCH_2);
    }
    
    private AlertProfile retrieveAlertProfile(PasContext context, MedProfile medProfile) throws AlertProfileException, EhrAllergyException {
    	
    	AlertProfile alertProfile = null;
    	Patient patient = ObjectUtils.defaultIfNull(context == null ? null : context.getPatient(), medProfile.getPatient());
    	
		if (ALERT_PROFILE_ENABLED.get(Boolean.FALSE) &&
				VETTING_MEDPROFILE_ALERT_PROFILE_ENABLED.get(Boolean.FALSE)) {
			alertProfile = alertProfileManager.retrieveAlertProfile(medProfile.getPatHospCode(), medProfile.getMedCase().getCaseNum(), patient);
		}
		
		if (ALERT_EHR_PROFILE_ENABLED.get()) {
			alertProfile = ehrAlertManager.retrieveEhrAllergySummary(alertProfile, medProfile.getPatHospCode(), patient.getPatKey(), patient.getHkid());
		}
		return alertProfile;
    }
    
    private void printSfiInvoice(List<RenderAndPrintJob> renderAndPrintJobList, List<Invoice> invoiceList){
    	if( invoiceList == null ){
    		return;
    	}
    	
    	for( Invoice invoice : invoiceList ){
    		invoiceRenderer.prepareSfiInvoicePrintJob(renderAndPrintJobList, invoice, false);
    	}
    }
    
    private List<Invoice> createInvoiceListFromPurchaseRequest(List<PurchaseRequest> newPurchaseReqList, Workstore workstore){
    	if( newPurchaseReqList == null ){
    		return null;
    	}
    	
    	List<Invoice> invoiceList = new ArrayList<Invoice>();
    	
    	for(PurchaseRequest purchaseReq : newPurchaseReqList){
    		Invoice newInvoice = dispOrderConverter.convertMedProfileItemToInvoice(purchaseReq, false, workstore);
    		
    		InvoiceItem invoiceItem = newInvoice.getInvoiceItemList().get(0);
    		MedCase medCase = newInvoice.getDispOrder().getPharmOrder().getMedCase();
    		
    		purchaseReq.setInvoiceDate(newInvoice.getDispOrder().getDispDate());
    		purchaseReq.setInvoiceNum(newInvoice.getInvoiceNum());
    		purchaseReq.setTotalAmount(newInvoice.getTotalAmount());
    		purchaseReq.setInvoiceStatus(newInvoice.getStatus());
    		purchaseReq.setLifestyleFlag(invoiceItem.getDispOrderItem().getLifestyleFlag());
    		purchaseReq.setOncologyFlag(invoiceItem.getDispOrderItem().getOncologyFlag());
    		if( medCase != null ){
    			purchaseReq.setPasPatGroupCode(medCase.getPasPatGroupCode());
    			purchaseReq.setPasPayCode(medCase.getPasPayCode());
    		}
    		
    		if( InvoiceStatus.Settled == newInvoice.getStatus() ){
    			purchaseReq.setFcsCheckDate(newInvoice.getFcsCheckDate());
    			if( !purchaseReq.getDirectPrintFlag() && purchaseReq.getDueDate() == null ){
    				purchaseReq.setDueDate(newInvoice.getFcsCheckDate());
    			}
    		}

    		if(!Boolean.TRUE.equals(purchaseReq.getDirectPrintFlag()))
    		{
    			purchaseReq.setInvoice(newInvoice);
    		}

    		em.merge(purchaseReq);
    		em.flush();
    		
    		invoiceList.add(newInvoice);
    	}

    	return invoiceList;
    }
    
    private List<Invoice> updatePurchaseRequestInvoiceInfo(List<Delivery> deliveryList, List<DispOrder> dispOrderList){
    	Map<Long, InvoiceItem> invoiceItemMap = new HashMap<Long, InvoiceItem>();
    	List<Invoice> directPrintInvoiceList = new ArrayList<Invoice>();
    	
		for(DispOrder directDispOrder:dispOrderList){
			if( directDispOrder.getInvoiceList() != null){
				for( Invoice invoice : directDispOrder.getInvoiceList() ){
					InvoiceItem invoiceItem = invoice.getInvoiceItemList().get(0);
					Long deliveryItemId = invoiceItem.getDispOrderItem().getDeliveryItemId();
					invoiceItemMap.put(deliveryItemId, invoiceItem);
				}
				directPrintInvoiceList.addAll(directDispOrder.getInvoiceList());
			}
		}
		
		if( directPrintInvoiceList.isEmpty() ){
			return null;
		}
		
		for (Delivery delivery: deliveryList) {
			for( DeliveryItem deliveryItem : delivery.getDeliveryItemList() ){
				if( deliveryItem.getPurchaseRequest() == null ){
					continue;
				}
				
				PurchaseRequest purchaseRequest = findPurchaseRequest(deliveryItem.getPurchaseRequest().getId());
		    	
		    	if( invoiceItemMap != null && invoiceItemMap.get(deliveryItem.getId()) != null ){
		    		InvoiceItem invoiceItem = invoiceItemMap.get(deliveryItem.getId());
		    		MedCase medCase = invoiceItem.getInvoice().getDispOrder().getPharmOrder().getMedCase();
		    		
		    		purchaseRequest.setInvoiceDate(deliveryItem.getDelivery().getBatchDate());
		    		purchaseRequest.setInvoiceNum(invoiceItem.getInvoice().getInvoiceNum());
		    		purchaseRequest.setInvoiceStatus(invoiceItem.getInvoice().getStatus());
		    		purchaseRequest.setTotalAmount(invoiceItem.getInvoice().getTotalAmount());
		    		purchaseRequest.setLifestyleFlag(invoiceItem.getDispOrderItem().getLifestyleFlag());
		    		purchaseRequest.setOncologyFlag(invoiceItem.getDispOrderItem().getOncologyFlag());
		    		if( medCase != null ){
		    			purchaseRequest.setPasPatGroupCode(medCase.getPasPatGroupCode());
		    			purchaseRequest.setPasPayCode(medCase.getPasPayCode());
		    		}
		    	}
			}
		}
		em.flush();
		return directPrintInvoiceList;
    }
    
    public String directPrint(Workstation workstation, MedProfile medProfile, List<Integer> atdpsPickStationsNumList, List<PurchaseRequest> newPurchaseReqList, List<PurchaseRequest> voidPurchaseReqList){
    	List<DeliveryItem> deliveryItemList = createDirectDeliveryItemList(medProfile);
    	List<Invoice> dueOrderInvoiceList = createInvoiceListFromPurchaseRequest(newPurchaseReqList, workstation.getWorkstore());
    	List<Invoice> voidInvoiceList = dispOrderConverter.convertPurchaseRequestToVoidInvoiceList(voidPurchaseReqList, medProfile.getWorkstore());
    	List<Invoice> directPrintInvoiceList = null;
    	
    	List<RenderAndPrintJob> renderAndPrintJobList = new ArrayList<RenderAndPrintJob>();
    	
		renderAndPrintJobList.addAll(orderPendingRptManager.printPendingOrderRpt(medProfile));
    	
		if (!deliveryItemList.isEmpty()) {
			
			DeliveryRequest deliveryRequest = persistDeliveryRequest(workstation, null);
			List<Delivery> deliveryList = createDeliveryList(deliveryRequest, medProfile.getWardCode(), PrintMode.Direct,
					deliveryItemList);
			
			DeliveryRequestAlert deliveryRequestAlert = new DeliveryRequestAlert(medProfile, medProfile.getMdsCheckFlag());
			deliveryRequest.addDeliveryRequestAlert(deliveryRequestAlert);
			
			MutableTriple<List<RenderAndPrintJob>, List<DispOrder>, List<Object>> labelPair = labelManager.printMpDispLabelList(deliveryList, atdpsPickStationsNumList);
			updatePrintedCount(deliveryList);
			sendVetOrderMessages(deliveryList, false);
			
			if( labelPair.getLeft() != null ){
				renderAndPrintJobList.addAll( labelPair.getLeft() );
			}
			
			boolean isActiveWard = activeWardManager.isActiveWard(medProfile.getPatHospCode(), medProfile.getMedCase().getPasWardCode());
			if (isActiveWard || VETTING_MEDPROFILE_INACTIVEWARD_REPORT_TRX_PRINT.get(Boolean.TRUE)) {
				renderAndPrintJobList.addAll(mpTrxRptPrintManager.printMpTrxRpt(deliveryList));
			}
			
			directPrintInvoiceList = updatePurchaseRequestInvoiceInfo(deliveryList, labelPair.getMiddle());
			saveDispOrderListToCorp(labelPair.getMiddle(), dueOrderInvoiceList, voidInvoiceList, true);
			
			sendAtdpsSocketMessage(labelPair.getRight());
			
			postProcessDeliveryList(deliveryList, false);
		}else if( (dueOrderInvoiceList != null  || voidInvoiceList != null) ){
			saveDispOrderListToCorp(new ArrayList<DispOrder>(), dueOrderInvoiceList, voidInvoiceList, true);
		}
		
		//PrintInvoice
		String messageCode = printInvoiceForHAStaff(renderAndPrintJobList, directPrintInvoiceList, dueOrderInvoiceList, medProfile);

		if( !renderAndPrintJobList.isEmpty() ){
			printAgent.renderAndPrint(renderAndPrintJobList);
		}
		
		return messageCode; 
    }
    
    private String printInvoiceForHAStaff(List<RenderAndPrintJob> renderAndPrintJobList, List<Invoice> directPrintInvoiceList, List<Invoice> dueOrderInvoiceList, MedProfile medProfile){
    	List<Invoice> invoicePrintList = new ArrayList<Invoice>();
		if( directPrintInvoiceList != null ){
			invoicePrintList.addAll(directPrintInvoiceList);
		}
		if( dueOrderInvoiceList != null ){
			invoicePrintList.addAll(dueOrderInvoiceList);
		}
    	
    	if( invoicePrintList.isEmpty() ){
    		return StringUtils.EMPTY;
    	}
    	
    	Boolean printInvoiceForHaStaff = false;
    	List<Invoice> haInvoicePrintList = new ArrayList<Invoice>();
    	for( Invoice invoice : invoicePrintList ){
    		Boolean printInvoice = invoiceManager.isExceptionDrugExist(invoice); 
    		if( printInvoice ){
    			printInvoiceForHaStaff = printInvoice;
    			haInvoicePrintList.add(invoice);
    		}
    	}
    	
    	if( !printInvoiceForHaStaff ){
    		return "0097";
    	}
    	
    	printSfiInvoice(renderAndPrintJobList, haInvoicePrintList);
    	
    	MedProfile managedMedProfile = em.find(MedProfile.class, medProfile.getId());
    	managedMedProfile.setLastInvoicePrintDate(new Date());
    	
    	return StringUtils.EMPTY;
    }
    
    private List<DeliveryItem> createDirectDeliveryItemList(MedProfile medProfile) {

		List<DeliveryItem> deliveryItemList = new ArrayList<DeliveryItem>();

		List<MedProfileItem> medProfileItemList = medProfile.getMedProfileItemList();
		
		for (MedProfileItem medProfileItem: medProfileItemList) {
			deliveryItemList.addAll(directPrint(medProfileItem, medProfile));
		}
		
		List<PurchaseRequest> purchaseRequestList = retrievePurchaseRequestMapForDirectPrint(medProfile);
		for( PurchaseRequest purchaseRequest: purchaseRequestList){
			deliveryItemList.addAll(directPrint(purchaseRequest));
		}
		
		List<ReplenishmentItem> replenishmentItemList = retrieveReplenishmentItemListForDirectPrint(medProfile);
		for (ReplenishmentItem replenishmentItem: replenishmentItemList) {
			deliveryItemList.addAll(directPrint(replenishmentItem));
		}
		
		clearErrorLogs(deliveryItemList);
		
    	return deliveryItemList;
    }
    
    @SuppressWarnings("unchecked")
	private void clearErrorLogs(List<DeliveryItem> deliveryItemList) {

    	Set<Long> idSet = new TreeSet<Long>();
    	
		for (DeliveryItem deliveryItem: deliveryItemList) {
			MedProfileMoItem medProfileMoItem = deliveryItem.getMedProfileMoItem();
			if (medProfileMoItem != null && medProfileMoItem.getMedProfileItem() != null) {
				idSet.add(medProfileMoItem.getMedProfileItem().getId());
			}
		}
		
		List<MedProfileItem> medProfileItemList = QueryUtils.splitExecute(em.createQuery(
				"select o from MedProfileItem o" + // 20170418 index check : MedProfileItem.id : PK_MED_PROFILE_ITEM
				" where o.medProfileErrorLog is not null" +
				" and o.id in :idSet")
				.setHint(QueryHints.FETCH, MED_PROFILE_ITEM_QUERY_HINT_FETCH_1)
				, "idSet", idSet);
		
		for (MedProfileItem medProfileItem: medProfileItemList) {
			medProfileItem.setMedProfileErrorLog(null);
		}
    }
    
	private List<DeliveryItem> directPrint(MedProfileItem medProfileItem, MedProfile medProfile) {
		
		if (medProfileItem.getStatus() != MedProfileItemStatus.Verified) {
			return new ArrayList<DeliveryItem>();
		} else {
			return directPrint(medProfileItem.getMedProfileMoItem(), medProfile);
		}
	}
	
	private List<DeliveryItem> directPrint(MedProfileMoItem medProfileMoItem, MedProfile medProfile) {
		
		List<DeliveryItem> deliveryItemList = new ArrayList<DeliveryItem>();
		
		if (medProfileMoItem.getStatus() == MedProfileMoItemStatus.SysDeleted) {
			return deliveryItemList;
		}
		
		if (medProfileMoItem.getSuspendCode() != null || medProfileMoItem.getPendingCode() != null) {
			return deliveryItemList;
		}
		
		if (Boolean.TRUE.equals(medProfileMoItem.getPivasFlag())) {
			return deliveryItemList;
		}
		
		for (MedProfilePoItem medProfilePoItem: medProfileMoItem.getMedProfilePoItemList()) {
			if (Boolean.TRUE.equals(medProfilePoItem.getDirectPrintFlag())) {
				if( medProfile.getPrivateFlag() || !medProfilePoItem.isSfiItem() ){
					appendDeliveryItemForNew(deliveryItemList, medProfilePoItem);
				}
			}
		}
		
		return deliveryItemList;
	}
	
	private void appendDeliveryItemForNew(List<DeliveryItem> deliveryItemList, MedProfilePoItem medProfilePoItem) {

		if (medProfilePoItem.getLastDueDate() != null || medProfilePoItem.getDueDate() == null) {
			return;
		}
		
		DeliveryItem deliveryItem = new DeliveryItem();
		
		deliveryItem.setStatus(DeliveryItemStatus.Printed);
		deliveryItem.setMedProfilePoItem(medProfilePoItem);
		deliveryItem.setMedProfile(medProfilePoItem.getMedProfileMoItem().getMedProfileItem().getMedProfile());
		updateIssueQtyAndDuration(null, null, false, deliveryItem, medProfilePoItem);
		
		deliveryItemList.add(deliveryItem);
	}
	
	private void appendDeliveryItemForSfi(PurchaseRequest purchaseRequest, List<DeliveryItem> deliveryItemList) {
		
		DeliveryItem deliveryItem = new DeliveryItem();
		deliveryItem.setStatus(DeliveryItemStatus.Printed);
		deliveryItem.setDueDate(purchaseRequest.getDueDate() == null ? new Date() : purchaseRequest.getDueDate());
		deliveryItem.setAdjQty(BigDecimal.ZERO);
		deliveryItem.setIssueDuration(purchaseRequest.getDurationInDay() == null ?
				BigDecimal.ZERO : BigDecimal.valueOf(purchaseRequest.getDurationInDay()));
		deliveryItem.setPreIssueQty(purchaseRequest.getIssueQty());
		deliveryItem.setIssueQty(purchaseRequest.getIssueQty());
		
		deliveryItem.setMedProfilePoItem(purchaseRequest.getMedProfilePoItem());
		deliveryItem.setPurchaseRequest(purchaseRequest);
		deliveryItem.setMedProfile(purchaseRequest.getMedProfilePoItem().getMedProfileMoItem().getMedProfileItem().getMedProfile());
		
		deliveryItemList.add(deliveryItem);
	}
	
	private void appendDeliveryItem(MaterialRequestItem materialRequestItem, BigDecimal issueQty, List<DeliveryItem> deliveryItemList) {
		
		DeliveryItem deliveryItem = new DeliveryItem();
		deliveryItem.setStatus(DeliveryItemStatus.Printed);
		deliveryItem.setDueDate(materialRequestItem.getDueDate());
		deliveryItem.setAdjQty(BigDecimal.ZERO);
		deliveryItem.setIssueDuration(BigDecimal.ZERO);
		deliveryItem.setPreIssueQty(issueQty != null ? issueQty : materialRequestItem.getIssueQty());
		deliveryItem.setIssueQty(deliveryItem.getPreIssueQty());
		
		deliveryItem.setMedProfilePoItem(materialRequestItem.getMedProfilePoItem());
		deliveryItem.setMaterialRequestItem(materialRequestItem);
		deliveryItem.setMedProfile(materialRequestItem.getMaterialRequest().getMedProfile());
		
		deliveryItemList.add(deliveryItem);
	}
	
    private List<DeliveryItem> directPrint(PurchaseRequest purchaseRequest) {
    	
    	List<DeliveryItem> deliveryItemList = new ArrayList<DeliveryItem>();
    	
    	MedProfilePoItem medProfilePoItem = purchaseRequest.getMedProfilePoItem();
		MedProfileMoItem medProfileMoItem = medProfilePoItem.getMedProfileMoItem();
		
		if (medProfileMoItem.getStatus() == MedProfileMoItemStatus.SysDeleted) {
			return deliveryItemList;
		}
		
		if (medProfileMoItem.getSuspendCode() != null || medProfileMoItem.getPendingCode() != null) {
			return deliveryItemList;
		}
    	
    	appendDeliveryItemForSfi(purchaseRequest, deliveryItemList);
    	
    	return deliveryItemList;
    }
	
	private List<DeliveryItem> directPrint(ReplenishmentItem replenishmentItem) {
		
		List<DeliveryItem> deliveryItemList = new ArrayList<DeliveryItem>();
		
		MedProfilePoItem medProfilePoItem = replenishmentItem.getMedProfilePoItem();
		MedProfileMoItem medProfileMoItem = medProfilePoItem.getMedProfileMoItem();
		
		if (medProfileMoItem.getSuspendCode() != null || medProfileMoItem.getPendingCode() != null) {
			return deliveryItemList;
		}
		
		if( !medProfileMoItem.getMedProfileItem().getMedProfile().getPrivateFlag() && medProfilePoItem.isSfiItem() ){
			return deliveryItemList;		
		}
		
		DeliveryItem deliveryItem = new DeliveryItem();
		deliveryItem.setStatus(DeliveryItemStatus.Printed);
		deliveryItem.setDueDate(replenishmentItem.getDueDate());
		deliveryItem.setAdjQty(BigDecimal.ZERO);
		deliveryItem.setIssueDuration(BigDecimal.ZERO);
		deliveryItem.setPreIssueQty(replenishmentItem.getIssueQty());
		deliveryItem.setIssueQty(replenishmentItem.getIssueQty());
		deliveryItem.setMedProfilePoItem(medProfilePoItem);
		deliveryItem.setMedProfile(medProfilePoItem.getMedProfileMoItem().getMedProfileItem().getMedProfile());
		deliveryItem.setReplenishmentItem(replenishmentItem);
		deliveryItemList.add(deliveryItem);
		
		return deliveryItemList;
	}
	
	private void removeFollowUp(DeliveryRequest request) {

		List<MedProfile> medProfileList = findMedProfileListForRemoveFollowUp(request);
		
		for (MedProfile medProfile: medProfileList) {
			for (MedProfileItem medProfileItem: medProfile.getMedProfileItemList()) {
				MedProfileErrorLog errorLog = medProfileItem.getMedProfileErrorLog();
				if (errorLog != null && !Boolean.TRUE.equals(errorLog.getHideFollowUpFlag())) {
					errorLog.setHideErrorStatFlag(true);
					errorLog.setHideFollowUpFlag(true);
				}
			}
			medProfileStatManager.recalculateMedProfileErrorStat(medProfile);
		}
		
		pharmInboxClientNotifier.dispatchUpdateEvent(request.getWorkstore().getHospCode());
	}
    
    @SuppressWarnings("unchecked")
    private List<ReplenishmentItem> retrieveReplenishmentItemListForDirectPrint(MedProfile medProfile) {
    	
    	return em.createQuery(
    			"select distinct r from ReplenishmentItem r" + // 20120215 index check : Replenishment.medProfile : FK_REPLENISHMENT_01
    			" where r.printDate is null" +
    			" and r.issueQty > 0" +
    			" and r.directPrintFlag = true" +
    			" and r.replenishment.medProfile = :medProfile" +
    			" and r.replenishment.status = :rStatus" +
    			" and r.medProfilePoItem.medProfileMoItem.medProfileItem.status = :iStatus")
    			.setParameter("medProfile", medProfile)
    			.setParameter("rStatus", ReplenishmentStatus.Processed)
    			.setParameter("iStatus", MedProfileItemStatus.Verified)
    			.getResultList();
    }
    
    @SuppressWarnings("unchecked")
    private List<PurchaseRequest> retrievePurchaseRequestMapForDirectPrint(MedProfile medProfile) {
    	return em.createQuery(
    			"select r from PurchaseRequest r" + // 20150420 index check : PurchaseRequest.medProfile : FK_PURCHASE_REQUEST_01
    			" where r.printDate is null" +
    			" and r.directPrintFlag = true" +
    			" and r.medProfileId = :medProfileId" +
    			" and r.medProfilePoItem.medProfileMoItem.medProfileItem.medProfile.id = :medProfileId" +
    			" and (r.invoiceStatus = :invoiceStatus or r.forceProceedFlag = true)" +
    			" and r.medProfilePoItem.medProfileMoItem.medProfileItem.status = :itemStatus")
    			.setHint(QueryHints.FETCH, PURCHASE_REQUEST_QUERY_HINT_FETCH_1)
    			.setParameter("medProfileId", medProfile.getId())
    			.setParameter("invoiceStatus", InvoiceStatus.Settled)
    			.setParameter("itemStatus", MedProfileItemStatus.Verified)
    			.getResultList();
    }
    
    @SuppressWarnings("unchecked")
	private List<PurchaseRequest> retrievePurchaseRequestList(DeliveryRequest request, MedProfile medProfile) {
    	
    	if (!request.isSfiGenerationRequest()) {
    		return new ArrayList<PurchaseRequest>();
    	}
    	
    	List<PurchaseRequest> purchaseRequestList = em.createQuery(
    			"select r from PurchaseRequest r" + // 20150420 index check : PurchaseRequest.medProfile : FK_PURCHASE_REQUEST_01
    			" where r.printDate is null" +
    			" and r.directPrintFlag = false" +
    			" and r.medProfileId = :medProfileId" +
    			" and r.medProfilePoItem.medProfileMoItem.medProfileItem.medProfile.id = :medProfileId" +
    			" and r.invoiceStatus in :invoiceStatusList" +
    			" and r.invoiceDate is not null")
    			.setHint(QueryHints.FETCH, PURCHASE_REQUEST_QUERY_HINT_FETCH_2)
    			.setHint(QueryHints.FETCH, PURCHASE_REQUEST_QUERY_HINT_FETCH_3)
    			.setParameter("medProfileId", medProfile.getId())
    			.setParameter("invoiceStatusList", InvoiceStatus.Outstanding_Settle)
    			.getResultList();
    	
    	drugChargeClearanceManager.updatePaymentStatus(request.getPaymentStatusMap(), purchaseRequestList, false);
    	
    	return purchaseRequestList;
    }
    
    @SuppressWarnings("unchecked")
	private List<MaterialRequestItem> retrieveMaterialRequestItemList(DeliveryRequest request, MedProfile medProfile) {

    	Map<Long, MaterialRequestItem> materialRequestItemMap = request.getMaterialRequestItemMapByMedProfileId().get(medProfile.getId());
    	
    	if (!Boolean.TRUE.equals(request.getMaterialRequestFlag()) || materialRequestItemMap == null) {
    		return new ArrayList<MaterialRequestItem>();
    	}
    	
    	return QueryUtils.splitExecute(em.createQuery(
    			"select i from MaterialRequestItem i" + // 20160615 index check : MaterialRequestItem.id : PK_MATERIAL_REQUEST_ITEM
    			" where i.materialRequest.medProfileId = :medProfileId" +
    			" and i.materialRequest.medProfileMoItem.id = i.materialRequest.medProfileMoItem.medProfileItem.medProfileMoItemId" +
    			" and i.materialRequest.medProfileMoItem.medProfileItem.status = :verifiedStatus" +
    			" and (i.materialRequest.medProfileMoItem.endDate > :requestDate or i.materialRequest.medProfileMoItem.endDate is null)" +
    			" and (i.materialRequest.medProfileMoItem.transferFlag = false or i.materialRequest.medProfileMoItem.transferFlag is null)" +
    			(Boolean.TRUE.equals(medProfile.getInactivePasWardFlag()) ? " and i.materialRequest.medProfileMoItem.itemNum >= 10000" : "") +
    			" and i.materialRequest.medProfileMoItem.suspendCode is null and i.materialRequest.medProfileMoItem.pendingCode is null" +
    			" and i.status = :outstandingStatus" +
    			" and i.id in :materialRequestItemIdSet")
    			.setHint(QueryHints.FETCH, MATERIAL_REQUEST_ITEM_QUERY_HINT_FETCH_4)
    			.setHint(QueryHints.FETCH, MATERIAL_REQUEST_ITEM_QUERY_HINT_FETCH_5)
    			.setParameter("medProfileId", medProfile.getId())
    			.setParameter("verifiedStatus", MedProfileItemStatus.Verified)
    			.setParameter("requestDate", request.getCreateDate())
    			.setParameter("outstandingStatus", MaterialRequestItemStatus.Outstanding),
    			"materialRequestItemIdSet", materialRequestItemMap.keySet());
    }
    
    @SuppressWarnings("unchecked")
    private List<PivasWorklist> retrievePivasWorklists(DeliveryRequest request, MedProfile medProfile) {
    	
    	return filterPivasWorklistWithInvalidPivasPrepMethod(request.getWorkstore(), em.createQuery(
    			"select o from PivasWorklist o" + // 20160615 index check : PivasWorklist.medProfileId : FK_PIVAS_WORKLIST_04
    			" where o.medProfile.hospCode = :hospCode" +
				" and o.medProfile.status <> :inactiveStatus" +
				" and (o.medProfile.status = :activeStatus" +
				" or (o.medProfile.status = :homeLeaveStatus" +
				" and o.medProfile.returnDate < :requestDate))" +
				" and o.medProfileMoItem.id = o.medProfileMoItem.medProfileItem.medProfileMoItemId" +
				" and o.medProfileMoItem.medProfileItem.status = :verifiedStatus" +
				" and (o.medProfileMoItem.endDate > :requestDate or o.medProfileMoItem.endDate is null)" +
				" and (o.medProfileMoItem.transferFlag = false or o.medProfileMoItem.transferFlag is null)" +
				" and (o.medProfileMoItem.frozenFlag = false or o.medProfileMoItem.frozenFlag is null)" +
				(Boolean.TRUE.equals(medProfile.getInactivePasWardFlag()) ? " and o.medProfileMoItem.itemNum >= 10000" : "") +
				" and o.medProfileMoItem.suspendCode is null and o.medProfileMoItem.pendingCode is null" +
				" and o.workstoreGroupCode = :workstoreGroupCode" +
    			" and o.supplyDate = :supplyDate" +
    			" and o.status = :status" +
    			" and o.medProfileId = :medProfileId" +
    			" and o.pivasPrepId is not null")
				.setParameter("hospCode", request.getWorkstore().getHospCode())
				.setParameter("inactiveStatus", MedProfileStatus.Inactive)
				.setParameter("activeStatus", MedProfileStatus.Active)
				.setParameter("homeLeaveStatus", MedProfileStatus.HomeLeave)
    			.setParameter("requestDate", request.getCreateDate())
    			.setParameter("verifiedStatus", MedProfileItemStatus.Verified)
    			.setParameter("workstoreGroupCode", request.getWorkstore().getWorkstoreGroup().getWorkstoreGroupCode())
    			.setParameter("supplyDate", request.getSupplyDate())
    			.setParameter("status", PivasWorklistStatus.Prepared)
    			.setParameter("medProfileId", medProfile.getId())
    			.setHint(QueryHints.FETCH, PIVAS_WORKLIST_QUERY_HINT_FETCH_1)
    			.setHint(QueryHints.FETCH, PIVAS_WORKLIST_QUERY_HINT_FETCH_2)
    			.getResultList());
    }
    
    @SuppressWarnings("unchecked")
	private MedProfile findLockedMedProfile(Long id) {
    	
    	List<MedProfile> medProfileList = generateFindMedProfileQuery(id)
	    		.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
	    		.setHint(QueryHints.BATCH, MED_PROFILE_QUERY_HINT_BATCH_1)
	    		.setHint(QueryHints.BATCH, MED_PROFILE_QUERY_HINT_BATCH_2)
	    		.getResultList();
    	
    	MedProfileUtils.lookup(medProfileList, MED_PROFILE_QUERY_HINT_BATCH_1);
    	MedProfileUtils.lookup(medProfileList, MED_PROFILE_QUERY_HINT_BATCH_2);
    	
    	return MedProfileUtils.getFirstItem(medProfileList);
    }
    
	@SuppressWarnings("unchecked")
	private List<MedProfile> findMedProfileListForRemoveFollowUp(DeliveryRequest request) {
		
    	List<MedProfile> medProfileList = em.createQuery(
    			"select p from MedProfile p" +  // 20130829 index check : MedProfile.id MedProfile.medCase MedProfile.patient : PK_MED_PROFILE FK_MED_PROFILE_02 FK_MED_PROFILE_01
				" where (p.medCase.caseNum = :caseNum or p.patient.hkid = :hkid)" +
				" and p.hospCode = :hospCode")
	    		.setHint(QueryHints.BATCH, MED_PROFILE_QUERY_HINT_BATCH_1)
	    		.setHint(QueryHints.BATCH, MED_PROFILE_QUERY_HINT_BATCH_2)
	    		.setParameter("caseNum", request.getCaseNum())
	    		.setParameter("hkid", request.getHkid())
	    		.setParameter("hospCode", request.getWorkstore().getHospCode())
	    		.getResultList();
    	
    	MedProfileUtils.lookup(medProfileList, MED_PROFILE_QUERY_HINT_BATCH_1);
    	MedProfileUtils.lookup(medProfileList, MED_PROFILE_QUERY_HINT_BATCH_2);
    	
    	return medProfileList;
    }
    
    private MedProfilePoItem findMedProfilePoItem(Long id) {
    	
    	return (MedProfilePoItem) em.createQuery(
    			"select o from MedProfilePoItem o" + // 20150204 index check : MedProfilePoItem.id : PK_MED_PROFILE_PO_ITEM
    			" where o.id = :id and o.id is not null") // don't delete : magic code fix bug for https://bugs.eclipse.org/bugs/show_bug.cgi?id=347592
    			.setParameter("id", id)
    			.getSingleResult();
    }
    
    private MedProfileMoItem findMedProfileMoItem(Long id) {

    	return (MedProfileMoItem) em.createQuery(
    			"select o from MedProfileMoItem o" + // 20150204 index check : MedProfileMoItem.id : PK_MED_PROFILE_MO_ITEM
    			" where o.id = :id and o.id is not null") // don't delete : magic code fix bug for https://bugs.eclipse.org/bugs/show_bug.cgi?id=347592
    			.setParameter("id", id)
    			.getSingleResult();
    }
    
    private ReplenishmentItem findReplenishmentItem(Long id) {

    	return (ReplenishmentItem) em.createQuery(
    			"select o from ReplenishmentItem o" + // 20150204 index check : ReplenishmentItem.id : PK_REPLENISHMENT_ITEM
    			" where o.id = :id and o.id is not null") // don't delete : magic code fix bug for https://bugs.eclipse.org/bugs/show_bug.cgi?id=347592
    			.setParameter("id", id)
    			.getSingleResult();
    }
    
    private PurchaseRequest findPurchaseRequest(Long id) {

    	return (PurchaseRequest) em.createQuery(
    			"select o from PurchaseRequest o" + // 20150420 index check : PurchaseRequest.id : PK_PURCHASE_REQUEST
    			" where o.id = :id and o.id is not null") // don't delete : magic code fix bug for https://bugs.eclipse.org/bugs/show_bug.cgi?id=347592
    			.setParameter("id", id)
    			.getSingleResult();
    }
    
    private PivasWorklist findPivasWorklist(Long id) {
    	
    	return (PivasWorklist) em.createQuery(
    			"select o from PivasWorklist o" + // 20160615 index check : PivasWorklist.id : PK_PIVAS_WORKLIST
    			" where o.id = :id and o.id is not null") // don't delete : magic code fix bug for https://bugs.eclipse.org/bugs/show_bug.cgi?id=347592
    			.setParameter("id", id)
    			.getSingleResult();
    }
    
    private PivasPrep findPivasPrep(Long id) {
    	
    	return (PivasPrep) em.createQuery(
    			"select o from PivasPrep o" + // 20160615 index check : PivasPrep.id : PK_PIVAS_PREP
    			" where o.id = :id and o.id is not null")
    			.setParameter("id", id)
    			.getSingleResult();
    }
    
    private MaterialRequest findMaterialRequest(Long id) {
    	
    	return (MaterialRequest) em.createQuery(
    			"select o from MaterialRequest o" + // 20160615 index check : MaterialRequest.id : PK_MATERIAL_REQUEST
    			" where o.id = :id and o.id is not null")
    			.setParameter("id", id)
    			.getSingleResult();
    }
    
    private MaterialRequestItem findMaterialRequestItem(Long id) {
    	
    	return (MaterialRequestItem) em.createQuery(
    			"select o from MaterialRequestItem o" + // 20160615 index check : MaterialRequestItem.id : PK_MATERIAL_REQUEST_ITEM
    			" where o.id = :id and o.id is not null")
    			.setParameter("id", id)
    			.getSingleResult();
    }
    
    @SuppressWarnings("unchecked")
	private List<MaterialRequestItem> retrieveUnassignedMaterialRequestItemList(Long materialRequestId) {
    	
    	if (materialRequestId == null) {
    		return Collections.EMPTY_LIST;
    	}
    	
    	return em.createQuery(
    			"select o from MaterialRequestItem o" + // 20160615 index check : MaterialRequestItem.materialRequestId : FK_MATERIAL_REQUEST_ITEM_01
    			" where o.materialRequestId = :materialRequestId" +
    			" and o.status = :status")
    			.setParameter("materialRequestId", materialRequestId)
    			.setParameter("status", MaterialRequestItemStatus.None)
    			.getResultList();
    }
    
	@SuppressWarnings("unchecked")
	private List<MedProfilePoItem> retrieveMedProfilePoItemList(MedProfileMoItem medProfileMoItem) {
    	
    	return em.createQuery(
    			"select o from MedProfilePoItem o" + // 20160615 index check : MedProfilePoItem.medProfileMoItemId : FK_MED_PROFILE_PO_ITEM_01
    			" where o.medProfileMoItemId = :medProfileMoItemId")
    			.setParameter("medProfileMoItemId", medProfileMoItem.getId())
    			.getResultList();
    }
    
    private Query generateFindMedProfileQuery(Long id) {
    	
    	return em.createQuery(
    			"select p from MedProfile p" + // 20120215 index check : MedProfile.id : PK_MED_PROFILE
				" where p.id = :id and p.deliveryGenFlag = false")
				.setParameter("id", id);
    }
    
    private boolean hasActiveMedProfile(Workstore workstore, String caseNum, String hkid) {
    	
    	return (Long) em.createQuery(
    			"select count(p) from MedProfile p" + // 20130223 index check : MedProfile.medCase MedProfile.patient : FK_MED_PROFILE_01 FK_MED_PROFILE_02
    			" where (p.medCase.caseNum = :caseNum or p.patient.hkid = :hkid)" +
    			" and p.status = :activeStatus")
    			.setParameter("caseNum", caseNum)
    			.setParameter("hkid", hkid)
    			.setParameter("activeStatus", MedProfileStatus.Active)
    			.getSingleResult() > 0;
    }
    
    private boolean isNonMpWard(Workstore workstore, String wardCode) {
    	
    	if (wardCode == null) {
    		return false;
    	}
    	
    	return (Long) em.createQuery(
    			"select count(w) from Ward w, WardConfig wc" + // 20130829 index check : Ward.institution,wardCode WardConfig.workstoreGroup,wardCode : PK_WARD UI_WARD_CONFIG_01
    			" where w.institution = :institution and w.status = :activeRecordStatus and w.wardCode = :wardCode" +
    			" and wc.workstoreGroup = :workstoreGroup" +
    			" and wc.mpWardFlag = true and wc.wardCode = :wardCode")
    			.setParameter("institution", workstore.getWorkstoreGroup().getInstitution())
    			.setParameter("workstoreGroup", workstore.getWorkstoreGroup())
    			.setParameter("wardCode", wardCode)
    			.setParameter("activeRecordStatus", RecordStatus.Active)
    			.getSingleResult() <= 0;
    }
    
    private void auditChanges(PasContext context, MedProfile medProfile) {
    	
    	context.markChanges(medProfile);
    	
		Patient patient = medProfile.getPatient();
		MedCase medCase = medProfile.getMedCase();
		
    	if (context.isPatientInfoChanged()) {
    		auditLogger.log("#0355", medCase.getCaseNum(), patient.getHkid(),
    				medProfile.isDischarge(), context.getDischargeDate() != null);
    	}
    	if (context.isWardInfoChanged()) {
    		auditLogger.log("#0356", medCase.getCaseNum(), patient.getHkid(),
    				medCase.getPasSpecCode(), medCase.getPasWardCode(), medCase.getPasBedNum(),
    				context.getPasSpecCode(), context.getPasWardCode(), context.getPasBedNum());
    	}
    }
    
    private void saveDispOrderListToCorp(List<DispOrder> dispOrderList){
    	this.saveDispOrderListToCorp(dispOrderList, null, null, false);
    }
    
    private void saveDispOrderListToCorp(List<DispOrder> dispOrderList, List<Invoice> newInvoiceList, List<Invoice> voidInvoiceList, Boolean directLabelPrintFlag) {
    	
    	if (dispOrderList == null && newInvoiceList == null && voidInvoiceList == null ) {
    		return;
    	}
    	
    	boolean containsSfiItem = false;
    	
    	if( dispOrderList != null ){
			for (DispOrder dispOrder: dispOrderList) {
				if( dispOrder.getSfiFlag() ){
					containsSfiItem = true;
				}
				dispOrder.getPharmOrder().clearDmInfo();
			}
    	}

    	if( newInvoiceList == null && voidInvoiceList == null && !containsSfiItem ){    		
    		corpPmsServiceProxy.saveDispOrderListForIp(dispOrderList, CDDH_LEGACY_PERSISTENCE_ENABLED.get(false));
    	}else{
    		corpPmsServiceProxy.saveDispOrderListWithVoidInvoiceListForIp(dispOrderList, 
												CDDH_LEGACY_PERSISTENCE_ENABLED.get(false),
												CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED.get(false),
												directLabelPrintFlag,
												newInvoiceList,
												voidInvoiceList);
    	}
    }
    
    private void sendVetOrderMessages(List<Delivery> deliveryList, boolean requiresNewTransaction) {
    	
    	if (deliveryList == null) {
    		return;
    	}
    	for (Delivery delivery: deliveryList) {
    		sendVetOrderMessages(delivery, requiresNewTransaction);
    	}
    }
    
    private void sendVetOrderMessages(Delivery delivery, boolean requiresNewTransaction) {
    	
    	if (delivery == null) {
    		return;
    	}
    	
    	try {
    		Map<MedProfile, Map<Long, MedProfileMoItem>> vetOrderMessageMap = constructVetOrderMessageMap(delivery);
    		sendVetOrderMessages(vetOrderMessageMap, requiresNewTransaction);
    	} catch (Exception ex) {
			logger.error("Fail to send VetOrder message to CMS for Delivery(Id=#0)", ex, delivery.getId());
		}
    }
    
    private void sendVetOrderMessages(Map<MedProfile, Map<Long, MedProfileMoItem>> vetOrderMessageMap, boolean requiresNewTransaction) {
    	
		for (Entry<MedProfile, Map<Long, MedProfileMoItem>> entry: vetOrderMessageMap.entrySet()) {
			
			MedProfile medProfile = entry.getKey();
			List<MedProfileMoItem> medProfileMoItemList = new ArrayList<MedProfileMoItem>();
			medProfileMoItemList.addAll(entry.getValue().values());
			try {
				if (requiresNewTransaction) {
					medProfileManager.sendMessagesWithNewTransaction(MpTrxType.MpVetOrder, medProfile, medProfileMoItemList);
				} else {
					medProfileManager.sendMessages(MpTrxType.MpVetOrder, medProfile, medProfileMoItemList);
				}
			} catch (Exception ex) {
				logger.error("Fail to send VetOrder message to CMS for MedProfile(OrderNum=#0)", ex, medProfile.getOrderNum());
			}
		}
    }

    private MedProfile resolveMedProfile(MedProfileMoItem medProfileMoItem) {
    	
    	if (medProfileMoItem == null) {
    		return null;
    	}
    	MedProfileItem medProfileItem = medProfileMoItem.getMedProfileItem();
    	return medProfileItem == null ? null : medProfileItem.getMedProfile();
    }
    
    private Map<MedProfile, Map<Long, MedProfileMoItem>> constructVetOrderMessageMap(Delivery delivery) {
    	
    	Map<MedProfile, Map<Long, MedProfileMoItem>> vetOrderMessageMap = new HashMap<MedProfile, Map<Long, MedProfileMoItem>>();
    	
		for (DeliveryItem deliveryItem: delivery.getDeliveryItemList()) {
			appendVetOrderMessage(vetOrderMessageMap, deliveryItem);
		}
		return vetOrderMessageMap;
    }
    
    private void appendVetOrderMessage(Map<MedProfile, Map<Long, MedProfileMoItem>> vetOrderMessageMap, DeliveryItem deliveryItem) {

		if (deliveryItem.getStatus() != DeliveryItemStatus.KeepRecord) {
			return;
		}
		
		MedProfileMoItem medProfileMoItem = deliveryItem.getMedProfileMoItem();
		MedProfile medProfile = resolveMedProfile(medProfileMoItem);
		
		if (medProfile == null) {
			return;
		}
		
		Map<Long, MedProfileMoItem> medProfileMoItemMap = vetOrderMessageMap.get(medProfile);
		if (medProfileMoItemMap == null) {
			medProfileMoItemMap = new TreeMap<Long, MedProfileMoItem>();
			vetOrderMessageMap.put(medProfile, medProfileMoItemMap);
		}
		medProfileMoItemMap.put(medProfileMoItem.getId(), medProfileMoItem);
    }
    
    private void updatePrintedCount(List<Delivery> deliveryList) {
    	for (Delivery delivery: deliveryList) {
    		delivery.updatePrintedCount();
    	}
    }
    
    private void sendAtdpsSocketMessage(List<Object> atdpsMessageObjList){
    	if ( atdpsMessageObjList == null || atdpsMessageObjList.isEmpty() ) {
    		return;
    	}
		for ( Object atdpsMessageObj : atdpsMessageObjList ) {
			if ( atdpsMessageObj instanceof AtdpsMessageContent ) { 
				AtdpsMessageContent atdpsMessageContent = (AtdpsMessageContent)atdpsMessageObj;
    			atdpsMessageContent.setInstruction(StringUtils.EMPTY);
        		try{
        			logger.info("sendAtdpsSocketMessage itemCode = #0, trxId = #1, pickStationId = #2, hostName = #3, portNum = #4, wardCode = #5, batchCode = #6, reprintFlag = #7",
    								atdpsMessageContent.getItemCode(), 
    								atdpsMessageContent.getTrxId(),
    								atdpsMessageContent.getPickStationNum(),
    								atdpsMessageContent.getHostname(), 
    								MACHINE_ATDPS_PORT.get(), 
    								atdpsMessageContent.getWard(), 
    								atdpsMessageContent.getBatchCode(), 
    								atdpsMessageContent.getReprintFlag());
        			machineServiceProxy.sendAtdpsSocketMessage(atdpsMessageContent);
    	    	} catch (Exception e) {
    				logger.error("Fail to send AtdpsMessageContent to Atdps #0", e);
    			}
			} else if ( atdpsMessageObj instanceof AtdpsMessage ) {
				AtdpsMessage atdpsMessage = (AtdpsMessage)atdpsMessageObj;
				try{
        			logger.info("sendAtdpsSocketMessage itemCode = #0, trxId = #1, pickStationId = #2, hostName = #3, portNum = #4, wardCode = #5, batchCode = #6, reprintFlag = #7",
	            					atdpsMessage.getItemCode(), 
	            					atdpsMessage.getTrxId(),
	            					atdpsMessage.getPickStationNum(),
	            					atdpsMessage.getHostname(), 
    								MACHINE_ATDPS_PORT.get(), 
    								atdpsMessage.getWardCode(), 
    								atdpsMessage.getBatchCode(), 
    								atdpsMessage.isReprintFlag());
        			atdpsMessage.setPickStationNum(null);
        			machineServiceProxy.sendAtdpsSocketMessage(atdpsMessage);
    	    	} catch (Exception e) {
    				logger.error("Fail to send AtdpsMessage to Atdps  #0", e);
    			}
			}
			
    	}
    	
    }
    
    private static class NormalRenderAndPrintJobComparator implements Comparator<RenderAndPrintJob> {

    	private MpDispLabelComparator comparator = new MpDispLabelComparator(MpDispLabelComparator.SORT_BY_L_ARRAY); 
    	
		public int compare(RenderAndPrintJob o1, RenderAndPrintJob o2) {
			
			MpDispLabel label1 = resolveMpDispLabel(o1);
			MpDispLabel label2 = resolveMpDispLabel(o2);
			
			if (label1 == label2) {
				return 0;
			} else if (label1 == null) {
				return -1;
			} else if (label2 == null) {
				return 1;
			} else {
				return comparator.compare(label1, label2);
			}
		}
		
		private MpDispLabel resolveMpDispLabel(RenderAndPrintJob renderAndPrintJob) {
			
			Object obj = MedProfileUtils.getFirstItem(renderAndPrintJob.getObjectList());
			return obj instanceof MpDispLabel ? (MpDispLabel) obj : null; 
		}
    }
    
    private static class PivasRenderAndPrintJobGroupComparator implements Comparator<List<RenderAndPrintJob>> {
    	
		public int compare(List<RenderAndPrintJob> o1, List<RenderAndPrintJob> o2) {
			
			PivasProductLabel label1 = resolvePivasProductLabel(o1);
			PivasProductLabel label2 = resolvePivasProductLabel(o2);

			if (label1 == label2) {
				return 0;
			} else if (label1 == null) {
				return -1;
			} else if (label2 == null) {
				return 1;
			} else {
				int ret;
				if ((ret = ObjectUtils.compare(label1.getPivasDesc(), label2.getPivasDesc())) != 0) {
					return ret;
				}
				if ((ret = ObjectUtils.compare(label1.getWard(), label2.getWard())) != 0) {
					return ret;
				}
				return ObjectUtils.compare(label1.getBedNum(), label2.getBedNum());
			}
		}
		
		private PivasProductLabel resolvePivasProductLabel(List<RenderAndPrintJob> renderAndPrintJobList) {

			for (RenderAndPrintJob renderAndPrintJob: renderAndPrintJobList) {
				Object obj = MedProfileUtils.getFirstItem(renderAndPrintJob.getObjectList());
				if (obj instanceof PivasProductLabel) {
					return (PivasProductLabel) obj;
				}
			}
			return null;
		}
    }
}
