package hk.org.ha.model.pms.biz.vetting;

import java.util.Map;

import hk.org.ha.model.pms.corp.vo.StartVettingResult;
import hk.org.ha.model.pms.exception.vetting.VettingException;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.udt.label.DeltaChangeType;
import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;
import hk.org.ha.model.pms.vo.vetting.PrintJobManagerInfo;

import javax.ejb.Local;

@Local
public interface VettingManagerLocal {

	StartVettingResult startVetting(MedOrder medOrder, String hkid);
	
	StartVettingResult startVetting(Long medOrderId, String hkid);

	PrintJobManagerInfo endVetting(OrderViewInfo orderViewInfo, Object order) throws VettingException;		
	
	void releaseOrder(OrderViewInfo orderViewInfo, MedOrder medOrder);
	
	void releaseUnvetDhOrder(MedOrder medOrder);
	
	void unvetOrder(Long medOrderId);

	Long unvetMedOrder(Long medOrderId, boolean removePmsOrder);
	
	void removeOrder(MedOrder medOrder, DispOrder manualDispOrder, String logonUser, String remarkText);
	
	void removeSfiRefillOrder(Long dispOrderId);
	
	void updatePharmDispOrderItemDeltaChangeType(Map<Long, DeltaChangeType> deltaChangeTypeMap, Long dispOrderId);
	
	void updatePharmDispOrderItemDeltaChangeType(Map<Long, DeltaChangeType> deltaChangeTypeMap, DispOrder dispOrder);
	
	PrintJobManagerInfo endVettingForSfiRefill(OrderViewInfo orderViewInfo, RefillSchedule refillSchedule, Ticket ticket) throws VettingException;
	
	Ticket retrievePrevTicket(String prevTicketNum);
}
