package hk.org.ha.model.pms.biz.checkissue.mp;

import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.checkissue.mp.SentDelivery;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface SendServiceLocal {
	
	void sendDelivery(Date batchDate, String batchNum);
	
	void updateDeliveryDate(SentDelivery inSentDelivery);
	
	void readyToSendDelivery();
	
	List<SentDelivery> batchSendDelivery(List<SentDelivery> readyToSendDeliveryList);
	
	void destroy();
	
	void dispatchUpdateEvent(Workstore workstore) ;
	
}