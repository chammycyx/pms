package hk.org.ha.model.pms.biz.vetting;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.vo.Capd;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.phs.CapdSupplierItem;
import hk.org.ha.model.pms.udt.RecordStatus;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("capdListManager")
@MeasureCalls
public class CapdListManagerBean implements CapdListManagerLocal {
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private Workstore workstore;

	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

	
	public List<String> retrieveCapdSupplierSystemList() {
		String[] capdSupplierSystemArray = dmsPmsServiceProxy.retrieveCapdSupplierSystem(workstore.getHospCode(), workstore.getWorkstoreCode());
		return Arrays.asList(capdSupplierSystemArray);
	}
	
	public List<String> retrieveCapdCalciumStrengthList(String supplierSystem) {
		List<String> capdCalciumStrengthList = null;
		if ( StringUtils.isNotBlank(supplierSystem) ) {
			String[] capdCalciumStrengthArray = dmsPmsServiceProxy.retrieveCapdCalciumStrength(workstore.getHospCode(), workstore.getWorkstoreCode(), supplierSystem);
			capdCalciumStrengthList = Arrays.asList(capdCalciumStrengthArray);
		}
		return capdCalciumStrengthList;
	}
	
	public List<Capd> retrieveCapdConcentrationList(String supplierSystem, String calciumStrength) {
		if ( StringUtils.isBlank(supplierSystem) || StringUtils.isBlank(calciumStrength) ) {
			return null;
		}
		
		Capd[] capdConcentrationArray = dmsPmsServiceProxy.retrieveCapdConcentration(workstore.getHospCode(), workstore.getWorkstoreCode(), supplierSystem, calciumStrength);
		List<Capd> capdConcentrationList = Arrays.asList(capdConcentrationArray);
		
		convertCapdConcentrationList(capdConcentrationList);
		
		return capdConcentrationList;
	}

	public List<Capd> retrieveCapdConcentrationListByItemCode(String itemCode) {
		if ( StringUtils.isBlank(itemCode) ) {
			return new ArrayList<Capd>();
		}
		
		Capd[] capdConcentrationArray = dmsPmsServiceProxy.retrieveCapdConcentrationByItemCode(workstore.getHospCode(), workstore.getWorkstoreCode(), itemCode);
		List<Capd> capdConcentrationList = Arrays.asList(capdConcentrationArray);
		
		convertCapdConcentrationList(capdConcentrationList);
		
		return capdConcentrationList;
	}
	
	private void convertCapdConcentrationList(List<Capd> capdConcentrationList) {
		List<CapdSupplierItem> capdSupplierItemList = retrieveCapdSupplierItem(capdConcentrationList);
		
		for (Capd capd:capdConcentrationList) {
			CapdSupplierItem capdSupplierItem = retrieveCapdSupplierItemByItemCode(capdSupplierItemList, capd.getItemCode());
			if ( capdSupplierItem != null && capdSupplierItem.getStatus() == RecordStatus.Active && capdSupplierItem.getSupplier().getStatus() == RecordStatus.Active) {
				capd.setSupplierCode(capdSupplierItem.getSupplier().getSupplierCode());
			}
			DmDrug dmDrug = dmDrugCacher.getDmDrug(capd.getItemCode());
			if ( dmDrug != null ) {
				capd.setSampleItem(dmDrug.getSampleItem());
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<CapdSupplierItem> retrieveCapdSupplierItem(List<Capd> capdConcentrationList){
		
		if ( capdConcentrationList.isEmpty() ) {
			return new ArrayList<CapdSupplierItem>();
		}
		
		List<String> itemCodeList = new ArrayList<String>();
		
		for (Capd capd:capdConcentrationList) {
			itemCodeList.add(capd.getItemCode());
		}
		
		return em.createQuery(
				"select o from CapdSupplierItem o" + // 20120306 index check : CapdSupplierItem.itemCode : PK_CAPD_SUPPLIER_ITEM 
				" where o.itemCode in :itemCodeList" +
				" order by o.itemCode")
				.setParameter("itemCodeList", itemCodeList)
				.getResultList();
	}
	
	private CapdSupplierItem retrieveCapdSupplierItemByItemCode(List<CapdSupplierItem> capdSupplierItemList, String itemCode){
		CapdSupplierItem result = null;
		for (CapdSupplierItem capdSupplierItem : capdSupplierItemList) {
			if (itemCode.equals(capdSupplierItem.getItemCode())) {
				result = capdSupplierItem;
				break;
			}
		}
		return result;
	}
	
	@Remove
	public void destroy() {
	}
}
