package hk.org.ha.model.pms.biz.vetting;

import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;

import java.util.List;

import javax.ejb.Local;

@Local
public interface DuplicateItemCheckManagerLocal {

	boolean checkLocalDuplicateItem(PharmOrder pharmOrder);

	void clearLocalDuplicateItem();
	
	void checkCorpDuplicateItemByPharmOrder(PharmOrder pharmOrder);
	
	boolean checkCorpDuplicateItemByMedOrderItem(PharmOrder pharmOrder, MedOrderItem medOrderItem);
	
	void setCorpDuplicateItemList(List<DispOrderItem> dispOrderItemList);
	
	void destroy();

}
