package hk.org.ha.model.pms.vo.label;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "PivasProductLabel")
@ExternalizedBean(type=DefaultExternalizer.class)
public class PivasProductLabel implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@XmlElement(required = false)
	private String printName;
	
	@XmlElement(required = true)
	private String siteDesc;
	
	@XmlElement(required = false)
	private String lotNum;
	
	@XmlElement(required = true)
	private Date prepExpiryDate;
	
	@XmlElement(required = true)
	private String warnCode1;
	
	@XmlElement(required = true)
	private String warnCode2;
	
	@XmlElement(required = false)
	private String afterConcn;
	
	@XmlElement(required = true)
	private String pivasDosageUnit;
	
	@XmlElement(required = true)
	private Date manufactureDate;

	@XmlElement(required = false)
	private String trxId;
	
	@XmlElement(required = false)
	private String batchNum;
	
	@XmlElement(required = false)
	private String caseNum;
	
	@XmlElement(required = false)
	private String ward;
	
	@XmlElement(required = false)
	private String patNameChi;
	
	@XmlElement(required = false)
	private String patName;
	
	@XmlElement(required = false)
	private String finalVolume;
	
	@XmlElement(required = false)
	private String diluentDesc;
	
	@XmlElement(required = false)
	private String formulaPrintName;
	
	@XmlElement(required = false)
	private String containerName;
	
	@XmlElement(required = false)
	private Date firstDoseDateTime;
	
	@XmlElement(required = false)
	private String freq;
	
	@XmlElement(required = false)
	private String supplFreq;
	
	@XmlElement(required = false)
	private String bedNum;
	
	@XmlElement(required = false)
	private String orderDosage;
	
	@XmlElement(required = false)
	private String orderDosageSplit;
	
	@XmlElement(required = false)
	private String finalVolumeSplit;
	
	@XmlElement(required = false)
	private String splitCount;

	@XmlElement(required = false)
	private String concn;
	
	@XmlElement(required = false)
	private Integer numOfLabelSet;
	
	@XmlElement(required = false)
	private boolean shelfLifeZeroFlag;
	
	@XmlElement(required = false)
	private boolean splitFlag;
	
	// save for PIVAS report
	@XmlElement(required = false)
	private String specCode;
	
	@XmlElement(required = false)
	private String sex;
	
	@XmlElement(required = false)
	private String age;
	
	@XmlElement(required = false)
	private String bodyWeight;
	
	@XmlElement(required = false)
	private Date dob;
	
	@XmlElement(required = false)
	private String labelType;
	
	@XmlTransient
	private String currentPageNum;

	@XmlTransient
	private boolean reprintFlag;
	
	public PivasProductLabel() {
		shelfLifeZeroFlag = Boolean.FALSE;
		splitFlag = Boolean.FALSE;
		reprintFlag = Boolean.FALSE;
	}
	
	public String getPrintName() {
		return printName;
	}

	public void setPrintName(String printName) {
		this.printName = printName;
	}
	
	public String getPivasDesc() {

		List<String> tokens = new ArrayList<String>();
		tokens.add(formulaPrintName);
		tokens.add(StringUtils.join(new String[]{orderDosage, pivasDosageUnit}));
		
		if (!"ML".equalsIgnoreCase(pivasDosageUnit) || !StringUtils.isBlank(diluentDesc)) {
			tokens.add("in");
			tokens.add(StringUtils.join(new String[]{finalVolume, "ML"}));
			tokens.add(diluentDesc);
		}
		return StringUtils.join(tokens.iterator(), ' ');
	}
	
	public String getDeliveryItemDescTlf() {
		
		StringBuilder sb = new StringBuilder();
		sb.append("<span breakOpportunity=\"auto\" fontWeight=\"bold\">");
		sb.append(StringEscapeUtils.escapeXml(getPivasDesc()));
		sb.append("</span><br/>");
		if (!StringUtils.isEmpty(siteDesc)) {
			sb.append("<span breakOpportunity=\"auto\" fontWeight=\"bold\">");
			sb.append(StringEscapeUtils.escapeXml(siteDesc));
			sb.append(" - </span>");
		}
		if (!StringUtils.isEmpty(freq)) {
			sb.append("<span breakOpportunity=\"auto\" fontWeight=\"bold\">");
			sb.append(StringEscapeUtils.escapeXml(freq.trim()));
			sb.append(" </span>");
		}
		if (!StringUtils.isEmpty(supplFreq)) {
			sb.append("<span breakOpportunity=\"auto\" fontWeight=\"bold\">(");
			sb.append(StringEscapeUtils.escapeXml(supplFreq.trim().toLowerCase()));
			sb.append(") </span>");
		}
		
		sb.append("<img source=\"/assets/pivas.png\"/>");
		
		return sb.toString();
	}

	public String getLotNum() {
		return lotNum;
	}

	public void setLotNum(String lotNum) {
		this.lotNum = lotNum;
	}

	public Date getPrepExpiryDate() {
		return prepExpiryDate;
	}

	public void setPrepExpiryDate(Date prepExpiryDate) {
		this.prepExpiryDate = prepExpiryDate;
	}

	public String getWarnCode1() {
		return warnCode1;
	}

	public void setWarnCode1(String warnCode1) {
		this.warnCode1 = warnCode1;
	}

	public String getWarnCode2() {
		return warnCode2;
	}

	public void setWarnCode2(String warnCode2) {
		this.warnCode2 = warnCode2;
	}

	public String getAfterConcn() {
		return afterConcn;
	}

	public void setAfterConcn(String afterConcn) {
		this.afterConcn = afterConcn;
	}
	
	public String getPivasDosageUnit() {
		return pivasDosageUnit;
	}

	public void setPivasDosageUnit(String pivasDosageUnit) {
		this.pivasDosageUnit = pivasDosageUnit;
	}
	
	public Date getManufactureDate() {
		return manufactureDate;
	}

	public void setManufactureDate(Date manufactureDate) {
		this.manufactureDate = manufactureDate;
	}
	
	public void setReprintFlag(boolean reprintFlag) {
		this.reprintFlag = reprintFlag;
	}

	public boolean isReprintFlag() {
		return reprintFlag;
	}

	public String getSiteDesc() {
		return siteDesc;
	}

	public void setSiteDesc(String siteDesc) {
		this.siteDesc = siteDesc;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getWard() {
		return ward;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}

	public String getDiluentDesc() {
		return diluentDesc;
	}

	public void setDiluentDesc(String diluentDesc) {
		this.diluentDesc = diluentDesc;
	}

	public String getFormulaPrintName() {
		return formulaPrintName;
	}

	public void setFormulaPrintName(String formulaPrintName) {
		this.formulaPrintName = formulaPrintName;
	}

	public String getContainerName() {
		return containerName;
	}

	public void setContainerName(String containerName) {
		this.containerName = containerName;
	}

	public String getCurrentPageNum() {
		return currentPageNum;
	}

	public void setCurrentPageNum(String currentPageNum) {
		this.currentPageNum = currentPageNum;
	}

	public Date getFirstDoseDateTime() {
		return firstDoseDateTime;
	}

	public void setFirstDoseDateTime(Date firstDoseDateTime) {
		this.firstDoseDateTime = firstDoseDateTime;
	}

	public String getFreq() {
		return freq;
	}

	public void setFreq(String freq) {
		this.freq = freq;
	}

	public String getSupplFreq() {
		return supplFreq;
	}

	public void setSupplFreq(String supplFreq) {
		this.supplFreq = supplFreq;
	}

	public boolean isSplitFlag() {
		return splitFlag;
	}

	public void setSplitFlag(boolean splitFlag) {
		this.splitFlag = splitFlag;
	}

	public String getSplitCount() {
		return splitCount;
	}

	public void setSplitCount(String splitCount) {
		this.splitCount = splitCount;
	}

	public String getFinalVolume() {
		return finalVolume;
	}

	public void setFinalVolume(String finalVolume) {
		this.finalVolume = finalVolume;
	}

	public String getFinalVolumeSplit() {
		return finalVolumeSplit;
	}

	public void setFinalVolumeSplit(String finalVolumeSplit) {
		this.finalVolumeSplit = finalVolumeSplit;
	}

	public String getConcn() {
		return concn;
	}

	public void setConcn(String concn) {
		this.concn = concn;
	}

	public String getPatNameChi() {
		return patNameChi;
	}

	public void setPatNameChi(String patNameChi) {
		this.patNameChi = patNameChi;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getBedNum() {
		return bedNum;
	}

	public void setBedNum(String bedNum) {
		this.bedNum = bedNum;
	}

	public String getTrxId() {
		return trxId;
	}

	public void setTrxId(String trxId) {
		this.trxId = trxId;
	}

	public boolean isShelfLifeZeroFlag() {
		return shelfLifeZeroFlag;
	}

	public void setShelfLifeZeroFlag(boolean shelfLifeZeroFlag) {
		this.shelfLifeZeroFlag = shelfLifeZeroFlag;
	}

	public Integer getNumOfLabelSet() {
		return numOfLabelSet;
	}

	public void setNumOfLabelSet(Integer numOfLabelSet) {
		this.numOfLabelSet = numOfLabelSet;
	}

	public String getOrderDosageSplit() {
		return orderDosageSplit;
	}

	public void setOrderDosageSplit(String orderDosageSplit) {
		this.orderDosageSplit = orderDosageSplit;
	}

	public String getOrderDosage() {
		return orderDosage;
	}

	public void setOrderDosage(String orderDosage) {
		this.orderDosage = orderDosage;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getBodyWeight() {
		return bodyWeight;
	}

	public void setBodyWeight(String bodyWeight) {
		this.bodyWeight = bodyWeight;
	}

	public String getLabelType() {
		return labelType;
	}

	public void setLabelType(String labelType) {
		this.labelType = labelType;
	}

}
