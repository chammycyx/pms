package hk.org.ha.model.pms.udt.charging;

public enum FcsSfiInvoiceActionType {
	ISSUE,
	SUSPEND,
	VOID;
}
