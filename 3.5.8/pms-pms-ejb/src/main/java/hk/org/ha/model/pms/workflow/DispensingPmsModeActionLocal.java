package hk.org.ha.model.pms.workflow;

import javax.ejb.Local;

@Local
public interface DispensingPmsModeActionLocal {

	String checkStatus();
}
