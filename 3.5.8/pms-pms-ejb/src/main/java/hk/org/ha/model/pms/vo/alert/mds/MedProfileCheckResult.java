package hk.org.ha.model.pms.vo.alert.mds;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "MedProfileCheckResult")
@ExternalizedBean(type=DefaultExternalizer.class)
public class MedProfileCheckResult implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement(name="MedProfileAlertMsg")
	private List<MedProfileAlertMsg> medProfileAlertMsgList;
	
	@XmlElement
	boolean includeManualProfileFlag;
	
	@XmlElement
	List<AlertMsg> manualProfileAlertMsgList;
	
	@XmlTransient
	List<ErrorInfo> errorInfoList;
	
	@XmlTransient
	List<Long> withHoldList;

	@XmlTransient
	ManualProfileCheckResult manualProfileCheckResult;
	
	public MedProfileCheckResult() {
		medProfileAlertMsgList = new ArrayList<MedProfileAlertMsg>();
		includeManualProfileFlag = false;
		manualProfileAlertMsgList = new ArrayList<AlertMsg>();
		errorInfoList = new ArrayList<ErrorInfo>();
		withHoldList = new ArrayList<Long>();
		manualProfileCheckResult = null;
	}
	
	public List<MedProfileAlertMsg> getMedProfileAlertMsgList() {
		if (medProfileAlertMsgList == null) {
			return new ArrayList<MedProfileAlertMsg>();
		}
		else {
			return new ArrayList<MedProfileAlertMsg>(medProfileAlertMsgList);
		}
	}

	public void setMedProfileAlertMsgList(List<MedProfileAlertMsg> medProfileAlertMsgList) {
		if (medProfileAlertMsgList == null) {
			this.medProfileAlertMsgList = new ArrayList<MedProfileAlertMsg>();
		}
		else {
			this.medProfileAlertMsgList = new ArrayList<MedProfileAlertMsg>(medProfileAlertMsgList);
		}
	}

	public boolean isIncludeManualProfileFlag() {
		return includeManualProfileFlag;
	}

	public void setIncludeManualProfileFlag(boolean includeManualProfileFlag) {
		this.includeManualProfileFlag = includeManualProfileFlag;
	}
	
	
	public List<AlertMsg> getManualProfileAlertMsgList() {
		if (manualProfileAlertMsgList == null) {
			return new ArrayList<AlertMsg>();
		}
		else {
			return new ArrayList<AlertMsg>(manualProfileAlertMsgList);
		}
	}

	public void setManualProfileAlertMsgList(List<AlertMsg> manualProfileAlertMsgList) {
		if (manualProfileAlertMsgList == null) {
			this.manualProfileAlertMsgList = new ArrayList<AlertMsg>();
		}
		else {
			this.manualProfileAlertMsgList = new ArrayList<AlertMsg>(manualProfileAlertMsgList);
		}
	}

	public List<ErrorInfo> getErrorInfoList() {
		if (errorInfoList == null) {
			return new ArrayList<ErrorInfo>();
		}
		else {
			return new ArrayList<ErrorInfo>(errorInfoList);
		}
	}

	public void setErrorInfoList(List<ErrorInfo> errorInfoList) {
		if (errorInfoList == null) {
			this.errorInfoList = new ArrayList<ErrorInfo>();
		}
		else {
			this.errorInfoList = new ArrayList<ErrorInfo>(errorInfoList);
		}
	}
	
	public List<Long> getWithHoldList() {
		if (withHoldList == null) {
			return new ArrayList<Long>();
		}
		else {
			return new ArrayList<Long>(withHoldList);
		}
	}
	
	public void setWithHoldList(List<Long> withHoldList) {
		if (withHoldList == null) {
			this.withHoldList = new ArrayList<Long>();
		}
		else {
			this.withHoldList = new ArrayList<Long>(withHoldList);
		}
	}

	public ManualProfileCheckResult getManualProfileCheckResult() {
		return manualProfileCheckResult;
	}

	public void setManualProfileCheckResult(
			ManualProfileCheckResult manualProfileCheckResult) {
		this.manualProfileCheckResult = manualProfileCheckResult;
	}
}
