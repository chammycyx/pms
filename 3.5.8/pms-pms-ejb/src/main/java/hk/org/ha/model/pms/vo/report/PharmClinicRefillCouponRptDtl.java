package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class PharmClinicRefillCouponRptDtl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String refillCouponNum;
	private Date refillDate;
	private String patName;
	private String patNameChi;
	private String hkid;
	private String caseNum;
	private String orderNum;
	private String pasSpecCode;
	private String pasSubSpecCode;
	private String doWorkstoreCode;
	private String status;
	private String refillNum;
	private String numOfRefill;
	private Date moOrderDate;
	private Date dispDate;
	
	public String getRefillCouponNum() {
		return refillCouponNum;
	}
	public void setRefillCouponNum(String refillCouponNum) {
		this.refillCouponNum = refillCouponNum;
	}
	public Date getRefillDate() {
		return refillDate;
	}
	public Date getMoOrderDate() {
		return moOrderDate;
	}
	public void setMoOrderDate(Date moOrderDate) {
		this.moOrderDate = moOrderDate;
	}
	public Date getDispDate() {
		return dispDate;
	}
	public void setDispDate(Date dispDate) {
		this.dispDate = dispDate;
	}
	public void setRefillDate(Date refillDate) {
		this.refillDate = refillDate;
	}
	public String getPatName() {
		return patName;
	}
	public void setPatName(String patName) {
		this.patName = patName;
	}
	public String getHkid() {
		return hkid;
	}
	public void setHkid(String hkid) {
		this.hkid = hkid;
	}
	public String getCaseNum() {
		return caseNum;
	}
	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}
	public String getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}
	public String getPasSpecCode() {
		return pasSpecCode;
	}
	public void setPasSpecCode(String pasSpecCode) {
		this.pasSpecCode = pasSpecCode;
	}
	public String getPasSubSpecCode() {
		return pasSubSpecCode;
	}
	public void setPasSubSpecCode(String pasSubSpecCode) {
		this.pasSubSpecCode = pasSubSpecCode;
	}
	public String getDoWorkstoreCode() {
		return doWorkstoreCode;
	}
	public void setDoWorkstoreCode(String doWorkstoreCode) {
		this.doWorkstoreCode = doWorkstoreCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRefillNum() {
		return refillNum;
	}
	public void setRefillNum(String refillNum) {
		this.refillNum = refillNum;
	}
	public String getNumOfRefill() {
		return numOfRefill;
	}
	public void setNumOfRefill(String numOfRefill) {
		this.numOfRefill = numOfRefill;
	}
	public void setPatNameChi(String patNameChi) {
		this.patNameChi = patNameChi;
	}
	public String getPatNameChi() {
		return patNameChi;
	}		
}
