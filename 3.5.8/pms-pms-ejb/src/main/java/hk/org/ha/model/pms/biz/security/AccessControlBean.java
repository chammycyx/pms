package hk.org.ha.model.pms.biz.security;

import hk.org.ha.fmk.pms.security.Uam;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.sys.SysProfile;
import hk.org.ha.fmk.pms.util.ConfigHelper;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.link.InfoLinkManagerLocal;
import hk.org.ha.model.pms.biz.reftable.RefTableManagerLocal;
import hk.org.ha.model.pms.biz.reftable.WorkstationManagerLocal;
import hk.org.ha.model.pms.persistence.InfoLink;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.persistence.reftable.OperationWorkstation;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.reftable.OperationProfileType;
import hk.org.ha.model.pms.udt.reftable.OperationWorkstationType;
import hk.org.ha.model.pms.vo.security.SysMenu;
import hk.org.ha.model.pms.vo.security.SysMenuDtl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.internal.jpa.EntityManagerImpl;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.security.Identity;

@AutoCreate
@Stateless
@Name("accessControl")
@MeasureCalls
public class AccessControlBean implements AccessControlLocal {

	private static final String JAXB_CONTEXT_SECURITY = "hk.org.ha.model.pms.vo.security";
	
	private static final String MENU = "menu";
	private static final String DOT_XML = ".xml";
	private static final String MENU_DISPLAY_NAME = "displayName";
	private static final String MENU_CHILDREN = "children";
	private static final String MENU_URL = "url";
	private static final String MENU_ENABLED = "enabled";
	private static final String MENU_TYPE = "type";
	private static final String SEPARATOR = "separator";
	private static final String IS_DEFAULT_SCREEN = "isDefaultScreen";
	private static final String SHORTCUT_ENABLED = "shortcutEnabled";
	private static final String IDLE_ENABLED = "idleEnabled";
	private static final String SPEC_MAP_SFI_MAINT = "specMapSfiMaint";
	private static final String ITEM_AVAILABILITY_MAINT = "itemAvailabilityMaint";
	private static final String HA_DRUG_FORMULARY_MGMT_RPT = "haDrugFormularyMgmtRpt";
	private static final String ACCESS_MGMT = "accessMgmt";
	private static final String[] SPEC_MAP_SFI_MAINT_ARRAY = {"specMapSfiMaintSfi","specMapSfiMaintSfiSn"};
	private static final String[] ITEM_AVAILABILITY_MAINT_ARRAY = {"itemAvailabilityMaintLocalSuspend","itemAvailabilityMaintSfi","itemAvailabilityMaintSampleItem","itemAvailabilityMaintDrugScope"};
	private static final String[] HA_DRUG_FORMULARY_MGMT_RPT_ARRAY = {"fmRptCorp","fmRptClusterHospital"};
	private static final String[] ACCESS_MANAGEMENT_ARRAY = {"roleMaint","userMaint","userRoleMaint" ,"userEnquiry"};
	private static final String LOGON_REASON = "logonReason";
	
	@PersistenceContext
	private EntityManager em;
		
	@In
	private WorkstationManagerLocal workstationManager;
	
	@In
	private RefTableManagerLocal refTableManager;
	
	@In
	private InfoLinkManagerLocal infoLinkManager;
	
	@In
	private Identity identity;
	
	@In(create = true)
	private Uam uam;

	@In(create = true)
	private SysProfile sysProfile;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private UamInfo uamInfo;
		
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private Workstore workstore;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private Workstation workstation;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private OperationProfile activeProfile;

	@SuppressWarnings("unused")
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION,  required=false)
	private List<Map<String, Object>> menuMapList;
	
	@SuppressWarnings("unused")
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private Integer adminLevel;
	
	@SuppressWarnings("unused")
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private OperationWorkstationType operationWorkstationType;

	public boolean retrieveUserAccessControl(String hospCode, String workstoreCode) throws IOException {	
		
		uamInfo = uam.getUamInfo();		
		if (uamInfo == null) {
			// Debug mode
			uamInfo = new UamInfo();
			uamInfo.setUserId("itdadmin");
			uamInfo.setWorkstationId("localhost");
			uamInfo.setHospital(hospCode);
			uamInfo.setWorkstore(workstoreCode);
			uamInfo.setAppCode("PMS");
			uamInfo.setUserRole("__DEBUG__");
			uamInfo.setProfileCode("PMS_" + hospCode);
			uamInfo.setPermissionMap(new HashMap<String, String>());
			uam.setUamInfo(uamInfo);
			
			sysProfile.setDebugEnabled(true);
		}

    	//needs to change check against workstore plus hospital
    	workstation = workstationManager.retrieveWorkstationByUamInfo(uamInfo);	    
	    if (workstation == null) {
	    	return false;
	    }

	    // retrieve all necessary data	    
	    workstation.getWorkstationPrintList().size();
	    workstore = workstation.getWorkstore();
    	workstore.getWorkstoreGroup();    	    	
    	workstore.getWorkstoreGroup().getInstitution();
    	workstore.getHospital().getHospitalMappingList().size();
    	workstore.getHospital().getWorkstoreList().size();
    	workstore.getHospital().getHospitalCluster();    	
	    activeProfile = refTableManager.retrieveActiveProfile(workstore);
	    activeProfile.getOperationWorkstationList().size();

	    // detach it to assure no more further change
    	this.getEmi(em).detach(workstation);    
    	this.getEmi(em).detach(workstore);    
    	this.getEmi(em).detach(workstore.getWorkstoreGroup());    
    	this.getEmi(em).detach(workstore.getWorkstoreGroup().getInstitution());    
    	this.getEmi(em).detach(workstore.getHospital());    
    	this.getEmi(em).detach(workstore.getHospital().getHospitalCluster());    	
    	this.getEmi(em).detach(activeProfile);    
    	
    	// remove all the lazy association
    	workstore.setCddhWorkstoreGroup(null);
		workstore.getWorkstoreGroup().clearWorkstoreList();
		workstore.getWorkstoreGroup().clearWorkstoreGroupMappingList();		
		workstore.getWorkstoreGroup().getInstitution().clearPatientCatList();
		workstore.getWorkstoreGroup().getInstitution().clearWardList();
		workstore.getWorkstoreGroup().getInstitution().clearSpecialtyList();		
		activeProfile.clearOperationPropList();
		
		// read sysMenu
    	SysMenu sysMenu = getSysMenu(workstore);    	
	    this.createAdditionalMenu(sysMenu, sysProfile);	    

    	// retrieve operationWorkstation
	    OperationWorkstation operationWorkstation = refTableManager.retrieveOperationWorkstation(workstation, activeProfile);
	    if (operationWorkstation == null) {
	    	workstation = null;
	    	return false;
	    }  
	    operationWorkstationType = operationWorkstation.getType();
	    
	    // retrieve defaultScreen
		String defaultScreen = retrieveDefaultScreen(operationWorkstation);	    
	    
	    // convert menuMapList
	    menuMapList = convertMenu(sysMenu.getMenu(), activeProfile, defaultScreen, uamInfo);
	    
	    if (identity.hasPermission(LOGON_REASON, "Y")) {
	    	adminLevel = 1;
	    } else {
	    	adminLevel = 0;
	    }
	    
	    return true;
	}
	
	private SysMenu getSysMenu(Workstore workstore) throws IOException {
		
    	JaxbWrapper<SysMenu> securityJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_SECURITY);
    	
		InputStream inputStream = null;

		try {			
			inputStream = ConfigHelper.getResourceAsStream(MENU + "_" + workstore.getHospCode() + "_" + workstore.getWorkstoreCode() + DOT_XML);
		} catch (IOException e) {
		}

		if (inputStream == null) {
			try {
				inputStream = ConfigHelper.getResourceAsStream(MENU + "_" + workstore.getHospCode() + DOT_XML);
			} catch (IOException e) {
			}
		}

		if ( inputStream == null ) {
			inputStream = ConfigHelper.getResourceAsStream(MENU + DOT_XML);
		}
		
    	return securityJaxbWrapper.unmarshall(inputStream);
	}		
	
	private List<Map<String, Object>> convertMenu(List<SysMenuDtl> menu, OperationProfile activeProfile, String defaultScreen, UamInfo uamInfo) {
		
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		for (SysMenuDtl sysMenuDtl:menu) 
		{
			Map<String, Object> menuItem = new HashMap<String, Object>();
			if (sysMenuDtl.getDisplayName().equals(SEPARATOR)) 
			{
				menuItem.put(MENU_TYPE, SEPARATOR);
				resultList.add(menuItem);
			} 
			else if ( sysMenuDtl.isVisible(activeProfile.getType()) ) 
			{				
				boolean accessRight = (sysProfile.isDebugEnabled() ||
						sysMenuDtl.getTarget() == null || 
						checkPermission(sysMenuDtl.getTarget()));	
				
				if (sysMenuDtl.getTarget() != null && sysMenuDtl.getTarget().equals(HA_DRUG_FORMULARY_MGMT_RPT) && !accessRight)
				{
					continue;
				}
				
				menuItem.put(MENU_DISPLAY_NAME, sysMenuDtl.getDisplayName());
				menuItem.put(MENU_URL, sysMenuDtl.getUrl());
				menuItem.put(MENU_ENABLED, accessRight);
				menuItem.put(IS_DEFAULT_SCREEN, defaultScreen != null && StringUtils.equals(sysMenuDtl.getTarget(), defaultScreen));
				menuItem.put(IDLE_ENABLED, sysMenuDtl.getIdleEnabled());
				menuItem.put(SHORTCUT_ENABLED, sysMenuDtl.enabledShortcut(activeProfile.getType()) && accessRight);

				if (sysMenuDtl.getMenu() != null && sysMenuDtl.getMenu().size() > 0) {
					List<Map<String, Object>> children = convertMenu(sysMenuDtl.getMenu(), activeProfile, defaultScreen, uamInfo);
					menuItem.put(MENU_CHILDREN, children);		
				}
				
				
				resultList.add(menuItem);
			}
		}
		return resultList;
	}		
	
	private boolean checkPermission(String target) {
		if (target.equals(SPEC_MAP_SFI_MAINT)) {
			return checkSubPermission(SPEC_MAP_SFI_MAINT_ARRAY);
		} else if (target.equals(ITEM_AVAILABILITY_MAINT)) {
			return checkSubPermission(ITEM_AVAILABILITY_MAINT_ARRAY);
		} else if (target.equals(HA_DRUG_FORMULARY_MGMT_RPT)) {
			return checkClusterHospitalPermission(HA_DRUG_FORMULARY_MGMT_RPT_ARRAY);
		} else if (target.equals(ACCESS_MGMT)) {
			return checkSubPermission(ACCESS_MANAGEMENT_ARRAY);
		} else {
			return identity.hasPermission(target, "Y");
		}
	}
	
	private boolean checkSubPermission(String[] subPermissionArray) {
		for (String target:subPermissionArray) {
			if (identity.hasPermission(target, "Y")) {
				return true;
			}
		}
		return false;
	}
	
	private Boolean checkClusterHospitalPermission(String[] subPermissionArray) {
		for (String target:subPermissionArray) {
			if (uamInfo.getPermissionMap().get(target) == null)
			{
				continue;
			}
			else if (uamInfo.getPermissionMap().get(target).equals("Y") ||
				uamInfo.getPermissionMap().get(target).equals("Both") ||
				uamInfo.getPermissionMap().get(target).equals("Hospital"))
			{
				return true;
			}
		}
		
		return false;		
	}
	
	private void createAdditionalMenu(SysMenu sysMenu, SysProfile sysProfile) 
	{		
		String visibility = OperationProfileType.Eds.getDataValue()+","+OperationProfileType.Pms.getDataValue()+","+OperationProfileType.NonPeak.getDataValue();
		SysMenu rootMenu = sysMenu; 

		SysMenuDtl subMenuItem = null;
		
		for (SysMenuDtl menuItem:rootMenu.getMenu()) {
			if (menuItem.getDisplayName().equals("Useful Links")) {
				List<InfoLink> infoLinkList = infoLinkManager.retrieveInfoLink();
				for (InfoLink infoLink:infoLinkList) {
					subMenuItem = new SysMenuDtl();
					subMenuItem.setDisplayName(infoLink.getName());
					subMenuItem.setUrl(infoLink.getUrl());	
					subMenuItem.setVisibility(visibility);	
					menuItem.getMenu().add(subMenuItem);
				}
			}
		}

		if (sysProfile.isDevelopment()) 
		{
			SysMenuDtl testMenuItem = new SysMenuDtl();
			testMenuItem.setDisplayName("Test");
			testMenuItem.setVisibility(visibility);
			rootMenu.getMenu().add(testMenuItem);

			subMenuItem = new SysMenuDtl();
			subMenuItem.setDisplayName("Auto Dispensing");
			subMenuItem.setUrl("hk.org.ha.event.pms.main.show.ShowAutoDispTestViewEvent()");		
			subMenuItem.setVisibility(visibility);	
			testMenuItem.getMenu().add(subMenuItem);
		}
		
		SysMenuDtl helpMenuItem = new SysMenuDtl();
		helpMenuItem.setDisplayName("Help");
		helpMenuItem.setVisibility(visibility);
		rootMenu.getMenu().add(helpMenuItem);
				
		subMenuItem = new SysMenuDtl();
		subMenuItem.setDisplayName("Welcome Page");
		subMenuItem.setUrl("hk.org.ha.event.pms.main.show.ShowStartupViewEvent()");		
		subMenuItem.setVisibility(visibility);
		helpMenuItem.getMenu().add(subMenuItem);
		
		subMenuItem = new SysMenuDtl();
		subMenuItem.setDisplayName("Key Guide");
		subMenuItem.setVisibility(visibility);		
		helpMenuItem.getMenu().add(subMenuItem);
				
		subMenuItem = new SysMenuDtl();
		subMenuItem.setDisplayName("About");
		subMenuItem.setUrl("hk.org.ha.event.pms.main.info.popup.ShowAboutPopupEvent()");	
		subMenuItem.setVisibility(visibility);	
		helpMenuItem.getMenu().add(subMenuItem);
				
		SysMenuDtl logoffMenuItem = new SysMenuDtl();
		logoffMenuItem.setDisplayName("Logoff");
		logoffMenuItem.setVisibility(visibility);
		rootMenu.getMenu().add(logoffMenuItem);
	}
	
	private EntityManagerImpl getEmi(EntityManager em) {
		return ((EntityManagerImpl) em.getDelegate());
	}
	
	private String retrieveDefaultScreen(OperationWorkstation operationWorkstation) {
		switch (operationWorkstation.getType()) {
			case TicketGeneration: return "ticketGeneration";
			case DataEntry:	return "oneStopEntry";
			case Picking: return "drugPick";
			case Assemble: return "drugAssemble";
			case CheckAndIssue: return "drugCheckIssue";
			case SendOut: return "sendOut";
		}
		return null;
	}	
}