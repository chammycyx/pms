package hk.org.ha.model.pms.vo.onestop;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class ReprintDispOrderItemInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Long dispOrderItemId;
	
	private Integer numOfLabel;

	public void setDispOrderItemId(Long dispOrderItemId) {
		this.dispOrderItemId = dispOrderItemId;
	}

	public Long getDispOrderItemId() {
		return dispOrderItemId;
	}

	public void setNumOfLabel(Integer numOfLabel) {
		this.numOfLabel = numOfLabel;
	}

	public Integer getNumOfLabel() {
		return numOfLabel;
	}
	
}