package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class EdsPerfRptHdr implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String hospCode;
	private String workstoreCode;
	private String activeProfileName;
	public String getHospCode() {
		return hospCode;
	}
	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}
	public String getWorkstoreCode() {
		return workstoreCode;
	}
	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}
	public String getActiveProfileName() {
		return activeProfileName;
	}
	public void setActiveProfileName(String activeProfileName) {
		this.activeProfileName = activeProfileName;
	}
}
