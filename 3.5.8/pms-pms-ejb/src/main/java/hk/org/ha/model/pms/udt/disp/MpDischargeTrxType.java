package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum MpDischargeTrxType implements StringValuedEnum {

	MpNewOrder("DO1", "New Discharge Medication Order", MpDischargeMessageType.NewOrder, Boolean.TRUE),
	MpModifyOrder("AR6", "Modify Discharge Medication Order", MpDischargeMessageType.NewOrder, Boolean.TRUE),
	MpDeleteOrder("DR6", "Delete Discharge Medication Order", MpDischargeMessageType.NewOrder, Boolean.TRUE),
	MpChangePrescType("RT6", "Change Discharge Presciption Type", MpDischargeMessageType.NewOrder, Boolean.TRUE),
	MpAddPostDispComment("RT7", "Add Discharge Post Dispensing Comment", MpDischargeMessageType.NewOrder, Boolean.TRUE),
	MpResumeAdvanceOrder("RO6", "Change Advance Order to Normal Order (Reverse MOE suspend)", MpDischargeMessageType.NewOrder, Boolean.TRUE),
	MpConfirmRemark("CR6", "Confirm remark", MpDischargeMessageType.NewOrder, Boolean.TRUE),
	MpDiscontinue("DD6", "Discontinue Item", MpDischargeMessageType.Discontinue, Boolean.TRUE),		
	MpCancelDiscontinue("DD7", "Cancel Discontinue Item", MpDischargeMessageType.Discontinue, Boolean.TRUE),
	
	MpVetOrder("VO6", "Vet order without remark", MpDischargeMessageType.VetOrder, Boolean.FALSE),
	MpVetOrderWithRemark("VO7", "Vet order with remark", MpDischargeMessageType.VetOrderRemark, Boolean.FALSE);

	
	private final String dataValue;
	private final String displayValue;
	private final MpDischargeMessageType messageType;
	private final Boolean toPmsFlag;
	
	MpDischargeTrxType (final String dataValue, final String displayValue, final MpDischargeMessageType messageType, Boolean toPmsFlag) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
		this.messageType = messageType;
		this.toPmsFlag = toPmsFlag;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}

	public MpDischargeMessageType getMessageType() {
		return this.messageType;
	}
	
	public Boolean getToPmsFlag() {
		return this.toPmsFlag;
	}
	
	public static MpDischargeTrxType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MpDischargeTrxType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<MpDischargeTrxType> {

		private static final long serialVersionUID = 908893648178158318L;

		public Class<MpDischargeTrxType> getEnumClass() {
    		return MpDischargeTrxType.class;
    	}
    }
}
