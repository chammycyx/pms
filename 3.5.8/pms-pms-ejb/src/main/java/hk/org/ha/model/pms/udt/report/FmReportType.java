package hk.org.ha.model.pms.udt.report;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum FmReportType implements StringValuedEnum {
	MonthlyData("MonthlyData", "FM Monthly Detail Report by Hospital"),
	MonthlyHospSpecStat("MonthlyHospSpecStat","FM Monthly Statistic Report by Hospital and Specialty"),
	MonthlyHospStat("MonthlyHospStat","FM Monthly Statistic Report by Hospital"),
	
	MonthlyClusterSpecStat("MonthlyClusterSpecStat", "FM Monthly Statistic Report by Cluster and Specialty"),
	MonthlyClusterStat("MonthlyClusterStat", "FM Monthly Statistic Report by Cluster"),	
	
	MonthlyCorpSpecStat("MonthlyCorpSpecStat", "FM Monthly Statistic Report by Specialty"),
	MonthlyCorpStat("MonthlyCorpStat", "FM Monthly Statistic Report"),	
	
	YearlyAccumulatedHospStat("YearlyAccumulatedHospStat", "FM Yearly Statisitc Report by Hospital"),
	YearlyAccumulatedClusterStat("YearlyAccumulatedClusterStat", "FM Yearly Statistic Report by Cluster"),
	YearlyAccumulatedCorpStat("YearlyAccumulatedCorpStat", "FM Yearly Statistic Report");
	
    private final String dataValue;
    private final String displayValue;

    FmReportType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static FmReportType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(FmReportType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<FmReportType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<FmReportType> getEnumClass() {
    		return FmReportType.class;
    	}
    }
}



