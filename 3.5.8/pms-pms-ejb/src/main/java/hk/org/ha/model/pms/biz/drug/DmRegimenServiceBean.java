package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmRegimenCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmRegimen;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("dmRegimenService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DmRegimenServiceBean implements DmRegimenServiceLocal {

	@Logger
	private Log logger;

	@In
	private DmRegimenCacherInf dmRegimenCacher;

	@Out(required = false)
	private DmRegimen dmRegimen;
	
	private boolean retrieveSuccess;
	
	public void retrieveDmRegimen(String regimenCode)
	{
		logger.debug("retrieveDmRegimen #0", regimenCode);

		retrieveSuccess = false;		
		dmRegimen = dmRegimenCacher.getRegimenByRegimenCode(regimenCode);
		if (dmRegimen != null) {
			retrieveSuccess = true;
		}
		
		logger.debug("retrieveDmRegimen result #0", retrieveSuccess);
	}

	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}
	
	@Remove
	public void destroy() 
	{
		if (dmRegimen != null) {
			dmRegimen = null;
		}
	}

}
