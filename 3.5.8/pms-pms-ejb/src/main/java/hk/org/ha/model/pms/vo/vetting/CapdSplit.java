package hk.org.ha.model.pms.vo.vetting;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="CapdSplit")
@ExternalizedBean(type=DefaultExternalizer.class)
public class CapdSplit implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement(name="CapdSplitItem", required = true)
	private List<CapdSplitItem> capdSplitItemList;
	
	@XmlElement(name="CapdManualVoucher", required = true)
	private List<CapdManualVoucher> capdManualVoucherList;

	public List<CapdSplitItem> getCapdSplitItemList() {
		if (capdSplitItemList == null) {
			capdSplitItemList = new ArrayList<CapdSplitItem>();
		}
		return capdSplitItemList;
	}

	public void setCapdSplitItemList(List<CapdSplitItem> capdSplitItemList) {
		this.capdSplitItemList = capdSplitItemList;
	}

	public void setCapdManualVoucherList(List<CapdManualVoucher> capdManualVoucherList) {
		this.capdManualVoucherList = capdManualVoucherList;
	}

	public List<CapdManualVoucher> getCapdManualVoucherList() {
		if (capdManualVoucherList == null) {
			capdManualVoucherList = new ArrayList<CapdManualVoucher>();
			capdManualVoucherList.add(new CapdManualVoucher());
		}
		return capdManualVoucherList;
	}

}
