package hk.org.ha.model.pms.vo.checkissue.mp;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class AssignBatchSummary implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long deliveryId;

	private Date batchDate;
	
	private String batchNum;
	
	private String wardCode;
	
	private List<AssignBatchSummaryItem> assignBatchItemList;

	public Long getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(Long deliveryId) {
		this.deliveryId = deliveryId;
	}

	public Date getBatchDate() {
		return batchDate;
	}

	public void setBatchDate(Date batchDate) {
		this.batchDate = batchDate;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public List<AssignBatchSummaryItem> getAssignBatchItemList() {
		return assignBatchItemList;
	}

	public void setAssignBatchItemList(List<AssignBatchSummaryItem> assignBatchItemList) {
		this.assignBatchItemList = assignBatchItemList;
	}
}