package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class DrsRefillDispRpt implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private DrsRefillDispRptHdr drsRefillDispRptHdr;
	
	private List<DrsRefillDispRptDtl> drsRefillDispRptDtlList;
	
	private List<DrsRefillDispRptOtherItem> drsRefillDispRptOtherItemList;

	private DrsRefillDispRptFtr drsRefillDispRptFtr;

	public DrsRefillDispRptHdr getDrsRefillDispRptHdr() {
		return drsRefillDispRptHdr;
	}

	public void setDrsRefillDispRptHdr(DrsRefillDispRptHdr drsRefillDispRptHdr) {
		this.drsRefillDispRptHdr = drsRefillDispRptHdr;
	}

	public List<DrsRefillDispRptDtl> getDrsRefillDispRptDtlList() {
		return drsRefillDispRptDtlList;
	}

	public void setDrsRefillDispRptDtlList(List<DrsRefillDispRptDtl> drsRefillDispDtlList) {
		this.drsRefillDispRptDtlList = drsRefillDispDtlList;
	}

	public DrsRefillDispRptFtr getDrsRefillDispRptFtr() {
		return drsRefillDispRptFtr;
	}

	public void setDrsRefillDispRptFtr(DrsRefillDispRptFtr drsRefillDispRptFtr) {
		this.drsRefillDispRptFtr = drsRefillDispRptFtr;
	}

	public List<DrsRefillDispRptOtherItem> getDrsRefillDispRptOtherItemList() {
		return drsRefillDispRptOtherItemList;
	}

	public void setDrsRefillDispRptOtherItemList(List<DrsRefillDispRptOtherItem> drsRefillDispRptOtherItemList) {
		this.drsRefillDispRptOtherItemList = drsRefillDispRptOtherItemList;
	}
}
