package hk.org.ha.model.pms.biz.inbox;

import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatAdminType;
import java.util.List;
import javax.ejb.Local;

@Local
public interface CategorySummaryLocal {
	void countCategories(MedProfileStatAdminType adminType, Workstore workstore, Boolean showChemoProfileOnly, List<String> wardCodeList);
}
