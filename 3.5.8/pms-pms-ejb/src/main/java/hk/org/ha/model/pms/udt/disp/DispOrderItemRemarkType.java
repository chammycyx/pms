package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DispOrderItemRemarkType implements StringValuedEnum {

	Empty("-", "(Blank)"),
	NotDispense("N", "Not Dispensed"),
	OrderCancel("O", "Order Cancelled"),
	ItemCancel("I", "Item Cancelled"),
	WrongItem("W", "Wrong Item"),
	WrongDosage("D", "Wrong Dosage"),
	WrongFreq("F", "Wrong Frequency"),
	WrongSite("S", "Wrong Site"),
	WrongDuration("U", "Wrong Duration"),
	Others("R", "Other...");
	
    private final String dataValue;
    private final String displayValue;

    DispOrderItemRemarkType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static DispOrderItemRemarkType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DispOrderItemRemarkType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<DispOrderItemRemarkType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<DispOrderItemRemarkType> getEnumClass() {
    		return DispOrderItemRemarkType.class;
    	}
    }
}
