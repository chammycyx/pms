package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.persistence.medprofile.Delivery;
import hk.org.ha.model.pms.vo.report.MpTrxRpt;

import java.util.List;

import javax.ejb.Local;

@Local
public interface MpTrxRptBuilderLocal {
	
	List<MpTrxRpt> convertToMpTrxRptList(Delivery delivery);
	
	List<MpTrxRpt> convertToDrugRefillRptList(Delivery delivery);
	
	List<MpTrxRpt> convertToPivasTrxRptList(Delivery delivery);
	
	void retrieveDeliveryAlertProfile(Delivery delivery);
	
	String convertDrugOrderTlf(String drugOrderTlf);
	
	String convertDrugOrderXml(String drugOrderXml, String interfaceVersion);
}
