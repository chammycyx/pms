package hk.org.ha.model.pms.udt.alert.mds;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum AlertType implements StringValuedEnum {

	Adr     ("Adr",      "Drug reaction checking"),
	Allergen("Allergen", "Drug allergy checking"),
	Ddcm    ("Ddcm",     "Drug disease contraindication checking"),
	Ddim    ("Ddim",     "Drug drug interaction checking"),
	OpHla   ("OpHla",    "HLA-B*1502 test alert checking"),
	IpHla   ("IpHla",    "HLA-B*1502 test alert checking"),
	Drcm    ("Drcm",     "Dosage range checking");

	private final String dataValue;
    private final String displayValue;
    
    AlertType(final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}

	public static AlertType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(AlertType.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<AlertType> {
		
		private static final long serialVersionUID = 1L;

		public Class<AlertType> getEnumClass() {
    		return AlertType.class;
    	}
    }
}
