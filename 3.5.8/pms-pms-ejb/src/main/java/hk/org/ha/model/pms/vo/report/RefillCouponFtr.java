package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;

public class RefillCouponFtr implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String refillCouponRemarkEng;
	
	private String refillCouponRemarkChi;
	
	private Date lastUpdateTime;
	
	private String refillCouponNum;

	public String getRefillCouponRemarkEng() {
		return refillCouponRemarkEng;
	}

	public void setRefillCouponRemarkEng(String refillCouponRemarkEng) {
		this.refillCouponRemarkEng = refillCouponRemarkEng;
	}

	public String getRefillCouponRemarkChi() {
		return refillCouponRemarkChi;
	}

	public void setRefillCouponRemarkChi(String refillCouponRemarkChi) {
		this.refillCouponRemarkChi = refillCouponRemarkChi;
	}

	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	public String getRefillCouponNum() {
		return refillCouponNum;
	}

	public void setRefillCouponNum(String refillCouponNum) {
		this.refillCouponNum = refillCouponNum;
	}
	
	
}
