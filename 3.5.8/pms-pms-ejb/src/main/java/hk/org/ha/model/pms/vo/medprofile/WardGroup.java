package hk.org.ha.model.pms.vo.medprofile;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Transient;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class WardGroup implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String wardGroupCode;
	
	private Boolean selected;
	
	@Transient
	private List<String> wardCodeList;
	
	public WardGroup()
	{
		super();
	}
	
	public WardGroup(String wardGroupCode, List<String> wardCodeList) {
		super();
		this.wardGroupCode = wardGroupCode;
		this.wardCodeList = wardCodeList;
		this.selected = Boolean.FALSE;
	}

	public String getWardGroupCode() {
		return wardGroupCode;
	}

	public void setWardGroupCode(String wardGroupCode) {
		this.wardGroupCode = wardGroupCode;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public List<String> getWardCodeList() {
		if (wardCodeList == null) {
			wardCodeList = new ArrayList<String>();  
		}
		return wardCodeList;
	}

	public void setWardCodeList(List<String> wardCodeList) {
		this.wardCodeList = wardCodeList;
	}
}
