package hk.org.ha.model.pms.biz.report;

import static hk.org.ha.model.pms.prop.Prop.REPORT_FM_CRITERIA_YEAR_DURATION;
import hk.org.ha.fmk.pms.ssh.SshSession;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.ApplicationProp;
import hk.org.ha.model.pms.corp.persistence.FmHospitalMapping;
import hk.org.ha.model.pms.exception.sftp.SftpCommandException;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroupMapping;
import hk.org.ha.model.pms.udt.report.FmReportType;
import hk.org.ha.model.pms.vo.report.DnldTrxFile;
import hk.org.ha.model.pms.vo.report.HaDrugFmHospMapping;
import hk.org.ha.model.pms.vo.report.HaDrugFormularyMgmtRpt;
import hk.org.ha.model.pms.vo.security.PropMap;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.contexts.Context;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;
import org.joda.time.DateTime;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.ChannelSftp.LsEntry;

@Stateful
@Scope(ScopeType.SESSION)
@Name("haDrugFormularyMgmtRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class HaDrugFormularyMgmtRptServiceBean implements HaDrugFormularyMgmtRptServiceLocal {

	@PersistenceContext
	private EntityManager em;
	
	@Logger
	private Log logger;

	@In
	private Workstore workstore;
	
	@In
	private ApplicationProp applicationProp;
	
	@In
	private PropMap propMap;
	
	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
	
	@Out(required = false)
	private HaDrugFormularyMgmtRpt haDrugFormularyMgmtRpt;
	
	private List<String> directoryList;
	
	private ChannelSftp fmRptChannelSftp;
	
	private static final String SLASH = "/";
	private static final String UNCHECKED = "unchecked";
	private static final String FM_RPT_CORP = "permission.fmRptCorp";
	private static final String FM_RPT_CLUSTER = "permission.fmRptCluster";
	private static final String FM_RPT_HOSP = "permission.fmRptHosp";
	private static final String EXCEPTION_CLASS_NAME = "HaDrugFormularyMgmtRpt";
	private static final String PERMISSION_DENIED = "Permission denied";
	
	private static final String FM_YEARLY_REPORT_FOR_CORPORATE = "FM Yearly Report For Corporate";
	private static final String FM_YEARLY_REPORT_FOR_CORPORATE_BY_SPECIALTY = "FM Monthly Report For Corporate (Break by Specialty)";
	private static final String FM_MONTHLY_REPORT_FOR_CORPORATE = "FM Monthly Report For Corporate";
	private static final String FM_YEARLY_REPORT_FOR_CLUSTER = "FM Yearly Report For Cluster";
	private static final String FM_YEARLY_REPORT_FOR_CLUSTER_BY_SPECIALTY = "FM Monthly Report For Cluster (Break by Specialty)";
	private static final String FM_MONTHLY_REPORT_FOR_CLUSTER = "FM Monthly Report For Cluster";
	private static final String FM_YEARLY_REPORT_FOR_HOSPITAL = "FM Yearly Report For Hospital";
	private static final String FM_YEARLY_REPORT_FOR_HOSPITAL_BY_SPECIALTY = "FM Monthly Report For Hospital (Break by Specialty)";
	private static final String FM_REPORT_MONTHLY_DATA = "FM Report Monthly Data File";
	private static final String FM_MONTHLY_REPORT_FOR_HOSPITAL = "FM Monthly Report For Hospital";
	private static final String LEFT_BRACKET = "<";
	private static final String RIGHT_BRACKET = ">";
	
	private static final Pattern fmCorporateYearlyPattern = Pattern.compile("^FmReport_yearly_ALL_ALL_.*");
	private static final Pattern fmCorporateMonthlySpecialtyPattern = Pattern.compile("FmReport_monthly_specialty_ALL_ALL_.*");
	private static final Pattern fmCorporateMonthlyAllPattern = Pattern.compile("^FmReport_monthly_ALL_ALL_.*");
	private static final Pattern fmClusterYearlyPattern = Pattern.compile("^FmReport_yearly_.*._ALL.*");
	private static final Pattern fmClusterMonthlySpecialtyPattern = Pattern.compile("^FmReport_monthly_specialty_.*._ALL.*");
	private static final Pattern fmClusterMonthlyAllPattern = Pattern.compile("^FmReport_monthly_.*._ALL.*");
	private static final Pattern fmHospitalYearlyPattern = Pattern.compile("^FmReport_yearly_.*");
	private static final Pattern fmHospitalMonthlySpecialtyPattern = Pattern.compile("^FmReport_monthly_specialty_.*");
	private static final Pattern fmHospitalMonthlyDataPattern = Pattern.compile("^FmReport_monthly_data_.*");
	private static final Pattern fmHospitalMonthlyPattern = Pattern.compile("^FmReport_monthly_.*");
		
	public void clearFmRptList() {
		haDrugFormularyMgmtRpt = null;
	}
	
	@SuppressWarnings("unchecked")
	public void retrieveFmRptList() throws SftpCommandException {
		haDrugFormularyMgmtRpt = new HaDrugFormularyMgmtRpt();
		List<String> availableFolderList = new ArrayList<String> ();
		directoryList = new ArrayList<String>();
		try {
			Context context = Contexts.getEventContext();
			context.set("sftp_pms_host", applicationProp.getSftpPmsHost());
			context.set("sftp_pms_username", applicationProp.getSftpPmsUsername());
			context.set("sftp_pms_password", applicationProp.getSftpPmsPassword());
			context.set("sftp_pms_rootDir", applicationProp.getSftpPmsRootDir());
			fmRptChannelSftp = (ChannelSftp) Component.getInstance("fmRptChannelSftp");
		} 
		catch (Exception e) 
		{
			logger.error("Login failed to file server: #0", applicationProp.getSftpPmsHost());
			throw new SftpCommandException(e, EXCEPTION_CLASS_NAME);
		}
				
		try {
			fmRptChannelSftp.cd(applicationProp.getFmRptDataSubDir());
			List fileVector = fmRptChannelSftp.ls(".");
			if (fileVector.size() != 0) 
			{
				for (int i = 0; i < fileVector.size(); i++) 
				{
					LsEntry lsEntry = (LsEntry) fileVector.get(i);
					if (!lsEntry.getFilename().equals(".") && !lsEntry.getFilename().equals("..")) 
					{
						availableFolderList.add(lsEntry.getFilename());
					}
				}
			}
		} 
		catch (SftpException e) 
		{
			logger.error("Sftp command error");
			throw new SftpCommandException(e, EXCEPTION_CLASS_NAME);
		}
				
		getPatientHospitalList(availableFolderList);
		retrieveFilesFromServer();
		Collections.sort(haDrugFormularyMgmtRpt.getFmRptWorkstoreGroupMappingList(), new ComparatorForPatHospList());
		Collections.sort(haDrugFormularyMgmtRpt.getFmRptTrxFileList(), new ComparatorForFileList());
		logger.info("End retrieve fm report list: #0", new DateTime());
	}

	private void retrieveReportDir(List<String> directoryList, FmReportType fmReportType, int currentYear, 
								   List<WorkstoreGroupMapping> fmRptWorkstoreGroupMappingList, Workstore workstore, 
								   List<String> availableFolderList) {
		
		for (int i = currentYear-(REPORT_FM_CRITERIA_YEAR_DURATION.get(2)-1); i <= currentYear; i++) {
			for (String availableFolder : availableFolderList) {
				if (Integer.toString(i).equals(availableFolder)) {
					switch (fmReportType) {
						case MonthlyData: //get patient hosiptal directoy						
							for (WorkstoreGroupMapping workstoreGroupMapping : fmRptWorkstoreGroupMappingList) {
								directoryList.add(getTrxFileDir(FmReportType.MonthlyData, String.valueOf(i), workstoreGroupMapping.getPatHospCode()));
							}
							break;
						case MonthlyClusterSpecStat: //get cluster directoy
							directoryList.add(getTrxFileDir(FmReportType.MonthlyData, String.valueOf(i), workstore.getHospital().getHospitalCluster().getClusterCode()));
							break;
						case MonthlyCorpSpecStat: //get corporate directory
							directoryList.add(getTrxFileDir(FmReportType.MonthlyCorpSpecStat, String.valueOf(i), ""));
							break;
					}
				}
			}
		}
	}
	
	private void convertClusterPatHospList(List<Workstore> workstoreList, Map<String, WorkstoreGroupMapping> workstoreGroupMap, 
									List<WorkstoreGroupMapping> fmRptWorkstoreGroupMappingList, String clusterCode, List<FmHospitalMapping> fmHospitalMappingList)
	{
		for (Workstore tmpWorkstore : workstoreList)
		{
			for (WorkstoreGroupMapping workstoreGroupMapping : tmpWorkstore.getWorkstoreGroup().getWorkstoreGroupMappingList())
			{
				if (!workstoreGroupMap.containsKey(workstoreGroupMapping.getPatHospCode()))
				{
					workstoreGroupMap.put(workstoreGroupMapping.getPatHospCode(), workstoreGroupMapping);
				}
			}
		}

		for(FmHospitalMapping fmHospitalMapping:fmHospitalMappingList)
		{
			String patHospCode = fmHospitalMapping.getPatHospCode();

			if(clusterCode.equals(fmHospitalMapping.getToClusterCode()) && !workstoreGroupMap.containsKey(patHospCode))
			{
				WorkstoreGroupMapping newWsGpMapping = new WorkstoreGroupMapping();
				newWsGpMapping.setPatHospCode(patHospCode);
				workstoreGroupMap.put(patHospCode, newWsGpMapping);
			}
			else if(clusterCode.equals(fmHospitalMapping.getFromClusterCode()) && workstoreGroupMap.containsKey(patHospCode))
			{
				workstoreGroupMap.remove(patHospCode);
			}
		}
		
		for (Map.Entry<String, WorkstoreGroupMapping> entry : workstoreGroupMap.entrySet()) {
			fmRptWorkstoreGroupMappingList.add(entry.getValue());
		}
	}
	
	private List<HaDrugFmHospMapping> constructHaDrugFmHospMappingList(List<FmHospitalMapping> fmHospitalMappingList)
	{
		List<HaDrugFmHospMapping> resultList = new ArrayList<HaDrugFmHospMapping>();
		
		for(FmHospitalMapping fmHospitalMapping:fmHospitalMappingList)
		{
			HaDrugFmHospMapping haDrugFmHospMapping = new HaDrugFmHospMapping();
			haDrugFmHospMapping.setPatHospCode(fmHospitalMapping.getPatHospCode());
			haDrugFmHospMapping.setFromClusterCode(fmHospitalMapping.getFromClusterCode());
			haDrugFmHospMapping.setToClusterCode(fmHospitalMapping.getToClusterCode());
			resultList.add(haDrugFmHospMapping);
		}
		
		return resultList;
	}
	
	@SuppressWarnings(UNCHECKED)
	private void getPatientHospitalList(List<String> availableFolderList) {
		Workstore fmWorkstore = em.find(Workstore.class, workstore.getId());
		int currentYear = new DateTime().getYear();
		List<WorkstoreGroupMapping> fmRptWorkstoreGroupMappingList = new ArrayList<WorkstoreGroupMapping>();		
		Map<String, WorkstoreGroupMapping> workStoreGroupMap = new HashMap<String, WorkstoreGroupMapping>();

		List<FmHospitalMapping> fmHospitalMappingList = corpPmsServiceProxy.retrieveFmHospitalMappingList();

		String oriClusterCode = fmWorkstore.getHospital().getHospitalCluster().getClusterCode();
		
		if (propMap.getValueAsBoolean(FM_RPT_CLUSTER)) {
			
			List<Workstore> workstoreList = em.createQuery(
					"select o from Workstore o" + // 20120305 index check : Hospital.hospitalCluster : FK_HOSPITAL_01
					" where o.hospital.hospitalCluster.clusterCode = :clusterCode")
					.setParameter("clusterCode", oriClusterCode)
					.getResultList();

			convertClusterPatHospList(workstoreList, workStoreGroupMap, fmRptWorkstoreGroupMappingList, oriClusterCode, fmHospitalMappingList);

			retrieveReportDir(directoryList, FmReportType.MonthlyClusterSpecStat, currentYear, 
							  fmRptWorkstoreGroupMappingList, fmWorkstore, availableFolderList);
		}
		
		if (propMap.getValueAsBoolean(FM_RPT_HOSP) || propMap.getValueAsBoolean(FM_RPT_CLUSTER)) {
			if (fmRptWorkstoreGroupMappingList.isEmpty()) {
				fmRptWorkstoreGroupMappingList = fmWorkstore.getWorkstoreGroup().getWorkstoreGroupMappingList();
			}
			
			retrieveReportDir(directoryList, FmReportType.MonthlyData, currentYear, fmRptWorkstoreGroupMappingList, 
							  fmWorkstore, availableFolderList);
		}
		
		if (propMap.getValueAsBoolean(FM_RPT_CORP)) {
			retrieveReportDir(directoryList, FmReportType.MonthlyCorpSpecStat, currentYear, 
					          fmRptWorkstoreGroupMappingList, fmWorkstore, availableFolderList);
		}
		
		haDrugFormularyMgmtRpt.setFmWorkstore(fmWorkstore);
		haDrugFormularyMgmtRpt.setFmRptWorkstoreGroupMappingList(fmRptWorkstoreGroupMappingList);
		haDrugFormularyMgmtRpt.setHaDrugFmHospMappingList(constructHaDrugFmHospMappingList(fmHospitalMappingList));
	}
	
	@SuppressWarnings(UNCHECKED)
	private void retrieveFilesFromServer() throws SftpCommandException {
		logger.info("Start retrieve fm report list: #0", new DateTime());
		List<DnldTrxFile> fmRptTrxFileList = new ArrayList<DnldTrxFile>();
		for (String directory : directoryList) {
			try {
				fmRptChannelSftp.lstat(applicationProp.getSftpPmsRootDir() + SLASH + directory);
			}
			catch (SftpException e)
			{
				if (e.getMessage().equals(PERMISSION_DENIED))
				{
					logger.error("Sftp command error, permission denied");
					throw new SftpCommandException(e, EXCEPTION_CLASS_NAME);
				}
				
				continue;
			}
			
			try {
				logger.debug("root directory #1, file directory list #2", 
							 fmRptChannelSftp.pwd(),
							 directory);
				fmRptChannelSftp.cd(applicationProp.getSftpPmsRootDir() + SLASH + directory);
				List fileVector = fmRptChannelSftp.ls(".");
			
				if (fileVector.size() != 0) {
					for (int i = 0; i < fileVector.size(); i++) {
						LsEntry lsEntry = (LsEntry) fileVector.get(i);
						if (!lsEntry.getFilename().equals(".") && !lsEntry.getFilename().equals("..") && 
							!lsEntry.getAttrs().isDir()) {
							String fileName = removeFileExt(lsEntry.getFilename());
							fmRptTrxFileList.add(new DnldTrxFile(fileName, new Date((long)lsEntry.getAttrs().getMTime()*1000L),
																 convertDisplayName(fileName)));							
						}
					}
				}
			} catch (SftpException e) {
				logger.error("Sftp command error");
				throw new SftpCommandException(e, EXCEPTION_CLASS_NAME);
			}
		}

		haDrugFormularyMgmtRpt.setFmRptTrxFileList(fmRptTrxFileList);
	}
	
	private String convertDisplayName(String fileName)
	{
		if (fmCorporateYearlyPattern.matcher(fileName).find())
		{
			return FM_YEARLY_REPORT_FOR_CORPORATE + " " + LEFT_BRACKET + convertDisplayNameDetails(fileName, false) + RIGHT_BRACKET;
		}
		else if (fmCorporateMonthlySpecialtyPattern.matcher(fileName).find())
		{
			return FM_YEARLY_REPORT_FOR_CORPORATE_BY_SPECIALTY + " " + LEFT_BRACKET + convertDisplayNameDetails(fileName, false) + RIGHT_BRACKET;
		}
		else if (fmCorporateMonthlyAllPattern.matcher(fileName).find())
		{
			return FM_MONTHLY_REPORT_FOR_CORPORATE + " " + LEFT_BRACKET + convertDisplayNameDetails(fileName, false) + RIGHT_BRACKET;
		}
		else if (fmClusterYearlyPattern.matcher(fileName).find())
		{
			return FM_YEARLY_REPORT_FOR_CLUSTER + " " + LEFT_BRACKET + convertDisplayNameDetails(fileName, false) + RIGHT_BRACKET;
		}
		else if (fmClusterMonthlySpecialtyPattern.matcher(fileName).find())
		{
			return FM_YEARLY_REPORT_FOR_CLUSTER_BY_SPECIALTY + " " + LEFT_BRACKET + convertDisplayNameDetails(fileName, false) + RIGHT_BRACKET;
		}
		else if (fmClusterMonthlyAllPattern.matcher(fileName).find())
		{
			return FM_MONTHLY_REPORT_FOR_CLUSTER + " " + LEFT_BRACKET + convertDisplayNameDetails(fileName, false) + RIGHT_BRACKET;
		}
		else if (fmHospitalYearlyPattern.matcher(fileName).find())
		{
			return FM_YEARLY_REPORT_FOR_HOSPITAL + " " + LEFT_BRACKET + convertDisplayNameDetails(fileName, true) + RIGHT_BRACKET;
		}
		else if (fmHospitalMonthlySpecialtyPattern.matcher(fileName).find())
		{
			return FM_YEARLY_REPORT_FOR_HOSPITAL_BY_SPECIALTY + " " + LEFT_BRACKET + convertDisplayNameDetails(fileName, true) + RIGHT_BRACKET;
		}
		else if (fmHospitalMonthlyDataPattern.matcher(fileName).find())
		{
			return FM_REPORT_MONTHLY_DATA + " " + LEFT_BRACKET + convertDisplayNameDetails(fileName, true) + RIGHT_BRACKET;
		}
		else if (fmHospitalMonthlyPattern.matcher(fileName).find())
		{
			return FM_MONTHLY_REPORT_FOR_HOSPITAL + " " + LEFT_BRACKET + convertDisplayNameDetails(fileName, true) + RIGHT_BRACKET;
		}
				
		return fileName;
	}
	
	private String convertDisplayNameDetails(String fileName, Boolean isHospitalLevel) {
        String[] fileNameArray = fileName.split("_");
        
        if (isHospitalLevel) {
              return fileNameArray[fileNameArray.length-2] +  // Hospital code 
                       "-" + fileNameArray[fileNameArray.length-1].substring(0, 4) +  // Year 
                       "-" + fileNameArray[fileNameArray.length-1].substring(4);      // Month
        } else {
              return fileNameArray[fileNameArray.length-1].substring(0, 4) +    // Year 
                       "-" + fileNameArray[fileNameArray.length-1].substring(4);      // Month
        }
	}
	
	private String getTrxFileDir(FmReportType fmReportType, String year, String folder) {
		switch(fmReportType) {
			case MonthlyData:
			case MonthlyClusterSpecStat:
				return applicationProp.getFmRptDataSubDir() + SLASH + year + SLASH + folder;
				
			case MonthlyCorpSpecStat:
				return applicationProp.getFmRptDataSubDir() + SLASH + year;
				
			default:
				return "";
		}
	}
		
	private String removeFileExt(String fileName) {
		return fileName.substring(0, fileName.lastIndexOf('.'));
	}
		
	public static class ComparatorForPatHospList implements Comparator<WorkstoreGroupMapping>, Serializable {

		private static final long serialVersionUID = 1L;

		public int compare(WorkstoreGroupMapping workstoreGroupMapping1, WorkstoreGroupMapping workstoreGroupMapping2) {
			return workstoreGroupMapping1.getPatHospCode().compareTo(workstoreGroupMapping2.getPatHospCode());
		}
	}
	
	public static class ComparatorForFileList implements Comparator<DnldTrxFile>, Serializable {

		private static final long serialVersionUID = 1L;

		public int compare(DnldTrxFile dnldTrxFile1, DnldTrxFile dnldTrxFile2) {
			return dnldTrxFile1.getFileName().compareTo(dnldTrxFile2.getFileName());
		}
	}
	
	@Remove
	public void destroy() {
		if (haDrugFormularyMgmtRpt != null) {
			haDrugFormularyMgmtRpt = null;
		}
	}
}
