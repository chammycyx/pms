package hk.org.ha.model.pms.biz.reftable.pivas;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
import hk.org.ha.model.pms.persistence.reftable.pivas.PivasHoliday;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("pivasHolidayListManager")
@MeasureCalls
public class PivasHolidayListManagerBean implements PivasHolidayListManagerLocal {		
	
	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	public List<PivasHoliday> retrievePivasHolidayList(WorkstoreGroup workstoreGroup, String year) {
		Calendar startDateCalendar = Calendar.getInstance();
		startDateCalendar.set(Integer.parseInt(year)-1, Calendar.OCTOBER, 1);
		Calendar endDateCalendar = Calendar.getInstance();
		endDateCalendar.set(Integer.parseInt(year)+1, Calendar.MARCH, 31);
		
		Date startDate = startDateCalendar.getTime();
		Date endDate = endDateCalendar.getTime();
		
		return em.createQuery(
					"select o from PivasHoliday o" + // 20160616 index check : PivasHoliday.workstoreGroup,holiday : UI_PIVAS_HOLIDAY
					" where o.workstoreGroup = :workstoreGroup" +
					" and o.holiday between :startDate and :endDate" +
					" order by o.holiday")
					.setParameter("workstoreGroup", workstoreGroup)
					.setParameter("startDate", startDate)
					.setParameter("endDate", endDate)
					.getResultList();
	}

	public void updatePivasHolidayList(List<PivasHoliday> updatePivasHolidayList, List<PivasHoliday> deletePivasHolidayList, WorkstoreGroup workstoreGroup) {
		if ( !deletePivasHolidayList.isEmpty() ) {
			for ( PivasHoliday pivasHoliday : deletePivasHolidayList ) {
				if ( !pivasHoliday.isNew() ) {
					em.remove(em.merge(pivasHoliday));
				}
			}
			em.flush();
		}
		
		if ( !updatePivasHolidayList.isEmpty() ) {
			try {
				for ( PivasHoliday pivasHoliday : updatePivasHolidayList ) {
					if ( pivasHoliday.isNew() ) {
						pivasHoliday.setWorkstoreGroup(workstoreGroup);
						em.persist(pivasHoliday);
					} else {
						em.merge(pivasHoliday);
					}
				}
				em.flush();
			} catch (PersistenceException e) {
				if (e.getMessage().contains("ORA-00001")) {
					throw new OptimisticLockException();
				} else {
					throw e;
				}
			}
		}
	}
	
}
