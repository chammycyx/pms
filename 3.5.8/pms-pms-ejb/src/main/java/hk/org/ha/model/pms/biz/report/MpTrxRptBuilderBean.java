package hk.org.ha.model.pms.biz.report;

import static hk.org.ha.model.pms.prop.Prop.MEDPROFILE_ALERT_HLA_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.REPORT_TRX_HKID_VISIBLE;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.model.pms.biz.alert.mds.AlertMsgManagerLocal;
import hk.org.ha.model.pms.biz.delivery.DeliveryManagerLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.biz.reftable.RefTableManagerLocal;
import hk.org.ha.model.pms.dms.biz.formatengine.FormatHints;
import hk.org.ha.model.pms.dms.biz.formatengine.ParsedOrderLineFormatter;
import hk.org.ha.model.pms.dms.biz.formatengine.ParsedOrderLineFormatterBase;
import hk.org.ha.model.pms.dms.biz.formatengine.ParsedOrderLineFormatterFactory;
import hk.org.ha.model.pms.dms.udt.formatengine.FormatType;
import hk.org.ha.model.pms.exception.delivery.DueOrderPrintException;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.medprofile.Delivery;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryRequestAlert;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItemAlert;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.persistence.medprofile.ReplenishmentItem;
import hk.org.ha.model.pms.persistence.pivas.PivasWorklist;
import hk.org.ha.model.pms.prop.Prop;
import hk.org.ha.model.pms.udt.alert.AlertProfileStatus;
import hk.org.ha.model.pms.udt.alert.AllergenType;
import hk.org.ha.model.pms.udt.medprofile.DeliveryItemStatus;
import hk.org.ha.model.pms.udt.medprofile.PrintMode;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.alert.AlertProfile;
import hk.org.ha.model.pms.vo.alert.PatAdr;
import hk.org.ha.model.pms.vo.alert.PatAllergy;
import hk.org.ha.model.pms.vo.alert.mds.AlertDdim;
import hk.org.ha.model.pms.vo.label.LabelContainer;
import hk.org.ha.model.pms.vo.label.MpDispLabel;
import hk.org.ha.model.pms.vo.label.PivasProductLabel;
import hk.org.ha.model.pms.vo.label.PivasWorksheet;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.report.MpTrxRpt;
import hk.org.ha.model.pms.vo.report.MpTrxRptDtl;
import hk.org.ha.model.pms.vo.report.MpTrxRptHdr;
import hk.org.ha.model.pms.vo.report.MpTrxRptItemDtl;
import hk.org.ha.model.pms.vo.report.MpTrxRptPivasItemDtl;
import hk.org.ha.model.pms.vo.report.MpTrxRptPoItemDtl;
import hk.org.ha.model.pms.vo.report.MpTrxRptPoItemHdr;
import hk.org.ha.model.pms.vo.rx.CapdRxDrug;
import hk.org.ha.model.pms.vo.rx.IvRxDrug;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.joda.time.DateTime;

@AutoCreate
@Stateless
@Scope(ScopeType.STATELESS)
@Name("mpTrxRptBuilder")
public class MpTrxRptBuilderBean implements MpTrxRptBuilderLocal {

	private static final String JAXB_CONTEXT_RX = "hk.org.ha.model.pms.vo.rx";
	private static final String JAXB_CONTEXT_ALERT = "hk.org.ha.model.pms.vo.alert";	
	
	private static final String NEW = "New";
	private static final String REFILL = "Refill";
	private static final String REPLENISH = "Replenish";
	private static final String NOTDUE = "Not Due";
	private static final String NOTDISPENSE = "Not Disp.";
	private static final String COMPLETED = "Completed";
	
	private static final String DAYS = "day(s)";
	

	private static final char[] DELIMITER = new char[] {' ' , '~' , '&' , '-' , '[' , '(' , '<'}; 

	private static final List<DeliveryItemStatus> NON_ACTIVE_DELIVERY_ITEM_STATUS_LIST = Arrays.asList(DeliveryItemStatus.KeepRecord,
																										DeliveryItemStatus.Unlink,
																										DeliveryItemStatus.Deleted);

	public static final int MAX_DRUG_ORDER_TLF_LENGTH = 55;
	public static final String TLF_BREAK_CHAR = "<span breakOpportunity=\"any\"> </span>";
	public static final String NEW_LINE = "\n";
	public static final String TAB = "   ";
	private static final char ASTERISK = '*';
	private static final String ATDPS_BINNUM = "-";
	
	private DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");	
	private DateFormat dateFormatWithBracketTime = new SimpleDateFormat("dd-MMM-yyyy (HH:mm)");
	private DateFormat dateFormatWithTime = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
	private static MpTrxRptDtlComparator mpTrxRptDtlComparator = new MpTrxRptDtlComparator();
	private static MpTrxRptDtlComparatorForPivas mpTrxRptDtlComparatorForPivas = new MpTrxRptDtlComparatorForPivas();
	private static MpTrxRptItemDtlComparator mpTrxRptItemDtlComparator = new MpTrxRptItemDtlComparator();
	private static MpTrxRptPoItemDtlComparator mpTrxRptPoItemDtlComparator = new MpTrxRptPoItemDtlComparator();
	private static MpTrxRptPivasItemDtlComparator mpTrxRptPivasItemDtlComparator = new MpTrxRptPivasItemDtlComparator();
	private static MpTrxRptPoItemDtlComparatorForMpTrxRptItemDtlSorting mpTrxRptPoItemDtlComparatorForMpTrxRptItemDtlSorting = new MpTrxRptPoItemDtlComparatorForMpTrxRptItemDtlSorting();
	
	private static final String JAXB_CONTEXT_LABEL = "hk.org.ha.model.pms.vo.label";
	private static JaxbWrapper<MpDispLabel> labelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
	
	
	
	@In
	private AlertMsgManagerLocal alertMsgManager;
	
	@In
	private DeliveryManagerLocal deliveryManager;
	
	@In
	private RefTableManagerLocal refTableManager;
	
	@In 
	private Workstore workstore;
	
	@PersistenceContext
	private EntityManager em;

	public void retrieveDeliveryAlertProfile(Delivery delivery) {
		

		if (delivery != null){
				
			if (delivery.getDeliveryRequest() != null){
				
				List<DeliveryRequestAlert> deliveryRequestAlertList = delivery.getDeliveryRequest().getDeliveryRequestAlertList();
				
				if (deliveryRequestAlertList.size()>0) {

					HashMap<String, AlertProfile> alertProfileMap = new HashMap<String, AlertProfile>();
					JaxbWrapper<AlertProfile> alertJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_ALERT);
					
					for (DeliveryRequestAlert deliveryRequestAlert : deliveryRequestAlertList) {
						
						if (StringUtils.isNotBlank(deliveryRequestAlert.getAlertProfileXml())) {
							
							alertProfileMap.put(deliveryRequestAlert.getMedProfileId() == null ? deliveryRequestAlert.getCaseNum() : deliveryRequestAlert.getMedProfileId().toString(), 
							alertJaxbWrapper.unmarshall(deliveryRequestAlert.getAlertProfileXml()));
							
						}
						
					}
				
					for (DeliveryItem deliveryItem : delivery.getDeliveryItemList()) {
						
						MedProfileMoItem medProfileMoItem = deliveryItem.getMedProfileMoItem();
						
						MedProfile medProfile = medProfileMoItem.getMedProfileItem().getMedProfile();
						
						String caseNum = medProfile.getMedCase().getCaseNum();
						
						if ( medProfile.getId() != null && alertProfileMap.containsKey(medProfile.getId().toString()) ) {
							medProfile.setAlertProfile(alertProfileMap.get(medProfile.getId().toString()));
						}else if( alertProfileMap.containsKey(caseNum) ){
							medProfile.setAlertProfile(alertProfileMap.get(caseNum));
						}
					}
					
				}
				
			}
			
		}
		
	}
	
	public List<MpTrxRpt> convertToMpTrxRptList(Delivery delivery){
		return convertToMpTrxRptList(delivery, false);
	}
	
	public List<MpTrxRpt> convertToDrugRefillRptList(Delivery delivery){
		return convertToMpTrxRptList(delivery, true);
	}
	
	private List<MpTrxRpt> convertToMpTrxRptList(Delivery delivery, boolean drugRefillListRptFlag) {
    	
		List<MpTrxRpt> mpTrxRptList = new ArrayList<MpTrxRpt>();
		MpTrxRpt mpTrxRpt = new MpTrxRpt();
    	
    	Map<Long, MpTrxRptDtl> medProfilePoItemMap = constructMedProfilePoItem(delivery, mpTrxRpt, drugRefillListRptFlag);

    	Map<String, MpTrxRptDtl> groupedMedProfilePoItemMap = groupMpTrxRptDtlList(medProfilePoItemMap);
    		
        List<MpTrxRptDtl> mpTrxRptDtlList = new ArrayList<MpTrxRptDtl>();
        mpTrxRptDtlList.addAll(groupedMedProfilePoItemMap.values());
    		
    	Collections.sort(mpTrxRptDtlList, mpTrxRptDtlComparator);

        for (MpTrxRptDtl mpTrxRptDtl : mpTrxRptDtlList) {
        		

        	for (MpTrxRptItemDtl mpTrxRptItemDtl : mpTrxRptDtl.getMpTrxRptItemDtlList()) {
        		if(mpTrxRptItemDtl.getMpTrxRptPoItemDtlList() != null) {
        			Collections.sort(mpTrxRptItemDtl.getMpTrxRptPoItemDtlList(), mpTrxRptPoItemDtlComparatorForMpTrxRptItemDtlSorting); 
        		}
        	    	
        	}

        	Collections.sort(mpTrxRptDtl.getMpTrxRptItemDtlList(),  mpTrxRptItemDtlComparator);
        		
        	for (MpTrxRptItemDtl mpTrxRptItemDtl : mpTrxRptDtl.getMpTrxRptItemDtlList()) {
        		if(mpTrxRptItemDtl.getMpTrxRptPoItemDtlList() != null) {
        			Collections.sort(mpTrxRptItemDtl.getMpTrxRptPoItemDtlList(), mpTrxRptPoItemDtlComparator);
        		}
        	}
        		
        	if( !mpTrxRptDtl.getMpTrxRptItemDtlList().isEmpty() ){
        		mpTrxRptDtl.getMpTrxRptItemDtlList().get( mpTrxRptDtl.getMpTrxRptItemDtlList().size()-1 ).setSeparateLineIndicator(false);
        	}
       	}


		mpTrxRpt.setMpTrxRptDtlList(mpTrxRptDtlList);
		mpTrxRptList.add(mpTrxRpt);
		
		return mpTrxRptList;
	}

	public List<MpTrxRpt> convertToPivasTrxRptList(Delivery delivery) {
    	
		List<MpTrxRpt> mpTrxRptList = new ArrayList<MpTrxRpt>();
		MpTrxRpt mpTrxRpt = new MpTrxRpt();

    	Map<Long, MpTrxRptDtl> medProfilePivasItemMap = constructMedProfilePivasItem(delivery, mpTrxRpt);
    	Map<String, MpTrxRptDtl> groupedMedProfilePivasItemMap = groupMpTrxRptDtlList(medProfilePivasItemMap);
    	
        List<MpTrxRptDtl> mpTrxRptDtlList = new ArrayList<MpTrxRptDtl>();
        mpTrxRptDtlList.addAll(groupedMedProfilePivasItemMap.values());

    	Collections.sort(mpTrxRptDtlList, mpTrxRptDtlComparatorForPivas);
    		
    	for (MpTrxRptDtl mpTrxRptDtl : mpTrxRptDtlList) {
    		for (MpTrxRptItemDtl mpTrxRptItemDtl : mpTrxRptDtl.getMpTrxRptItemDtlList()) {
    			if(mpTrxRptItemDtl.getMpTrxRptPivasItemDtlList() != null) {
    				Collections.sort(mpTrxRptItemDtl.getMpTrxRptPivasItemDtlList(), mpTrxRptPivasItemDtlComparator);
    			}  	    	
    		}
    			
        	if( !mpTrxRptDtl.getMpTrxRptItemDtlList().isEmpty() ){
        		mpTrxRptDtl.getMpTrxRptItemDtlList().get( mpTrxRptDtl.getMpTrxRptItemDtlList().size()-1 ).setSeparateLineIndicator(false);
        	}
    	} 

    	mpTrxRpt.setMpTrxRptDtlList(mpTrxRptDtlList);
		mpTrxRptList.add(mpTrxRpt);
		
		return mpTrxRptList;
	}
	
	public Map<String, MpTrxRptDtl> groupMpTrxRptDtlList(Map<Long, MpTrxRptDtl> medProfileMoIdMap) {
		
		Map<String, MpTrxRptDtl> caseNumMap = new HashMap<String, MpTrxRptDtl>();
		
		for (MpTrxRptDtl mpTrxRptDtl : medProfileMoIdMap.values()) {
			if(!mpTrxRptDtl.getMpTrxRptItemDtlList().isEmpty()){
				
				StringBuilder groupMapKey = new StringBuilder();
				groupMapKey.append(mpTrxRptDtl.getCaseNum());
				groupMapKey.append("|");
				groupMapKey.append(mpTrxRptDtl.getHkid());
				
				if (caseNumMap.containsKey(groupMapKey.toString())) {
					caseNumMap.get(groupMapKey.toString()).getMpTrxRptItemDtlList().addAll(mpTrxRptDtl.getMpTrxRptItemDtlList());
				} else {
					caseNumMap.put(groupMapKey.toString(), mpTrxRptDtl);
				}
			}
		}
		return caseNumMap;
	}
	
	private Map<Long, MpTrxRptDtl> constructMedProfilePoItem(Delivery delivery, MpTrxRpt mpTrxRpt, boolean drugRefillListRptFlag){
	
		Map<Long, MpTrxRptDtl> medProfileMoIdMap = new HashMap<Long, MpTrxRptDtl>();
		
		List<DeliveryItem> deliveryItemList = delivery.getDeliveryItemList();
		HashSet<Long> prevMoItemIdSet = new HashSet<Long>();
		Map<Long, List<DeliveryItem>> deliveryItemMap = new HashMap<Long, List<DeliveryItem>>();
		Map<Long, ReplenishmentItem> deliveryReplenishItemForZeroQtyMap = new HashMap<Long, ReplenishmentItem>();
		
    	for (DeliveryItem deliveryItem : deliveryItemList){
    		MedProfilePoItem medProfilePoItem = deliveryItem.resolveMedProfilePoItem();
    		if (medProfilePoItem != null) {
    			if ( !deliveryItemMap.containsKey(medProfilePoItem.getId()) ) {
            		deliveryItemMap.put(medProfilePoItem.getId(), new ArrayList<DeliveryItem>());
    			}
        		deliveryItemMap.get(medProfilePoItem.getId()).add(deliveryItem);
    		}
    		
    		if( deliveryItem.getReplenishmentItem() != null && deliveryItem.getReplenishmentItem().getReplenishment() != null){
        	
    			List<ReplenishmentItem> rrList = deliveryItem.getReplenishmentItem().getReplenishment().getReplenishmentItemList(); 
        		
    			for(ReplenishmentItem rr : rrList){
            		
    				if(rr.getIssueQty().setScale(0).equals(BigDecimal.ZERO) ){
                		
    					deliveryReplenishItemForZeroQtyMap.put(rr.getMedProfilePoItem().getId(), rr);
    					
    				}
    					
    			}
    		}
    		
    	}
    	
		if (deliveryItemList.size()>0){
			
			MpTrxRptHdr mpTrxRptHdr = constructMpTrxRptHdr(deliveryItemList.get(0).getDelivery());
			mpTrxRpt.setMpTrxRptHdr(mpTrxRptHdr);

			List<MpTrxRptItemDtl> mpTrxRptItemDtlList = null;
			MpTrxRptItemDtl mpTrxRptItemDtl = null ;
			JaxbWrapper<RxItem> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
			
			Map<String, Boolean> privateFlagMap = getPrivateFlagMap(delivery);
			
	    	for (DeliveryItem deliveryItem : deliveryItemList) {
	    		try{
	    			
		    		if ((deliveryItem.resolveMedProfilePoItem() != null)) {
		    			
		    			MedProfileMoItem medProfileMoItem = deliveryItem.getMedProfileMoItem();
		    			
		    			MedProfile medProfile = medProfileMoItem.getMedProfileItem().getMedProfile();
		    			MedCase medCase = medProfile.getMedCase();
		    			
			    		if (!prevMoItemIdSet.contains(medProfileMoItem.getId())){
			    			prevMoItemIdSet.add(medProfileMoItem.getId());
			        		
			    			if (medProfileMoIdMap.containsKey(medProfile.getId())){
				    			mpTrxRptItemDtlList = medProfileMoIdMap.get(medProfile.getId()).getMpTrxRptItemDtlList();
				    			
			    			} else {
				    			MpTrxRptDtl mpTrxRptDtl = new MpTrxRptDtl();
				    			
					    		convertAlertProfile(mpTrxRptHdr, mpTrxRptDtl, medProfile);
					    		
					    		mpTrxRptDtl.setDrugSensitivityDesc(medProfile.getDrugSensitivity());					    		
				    			mpTrxRptDtl.setBedNum(medCase.getPasBedNum());
				    			mpTrxRptDtl.setCaseNum(medCase.getCaseNum());
				    			mpTrxRptDtl.setWard(medProfile.getWardCode());
				    			mpTrxRptDtl.setSpecCode(medProfile.getSpecCode());
				    			mpTrxRptDtl.setCheckPregnancyFlag(medProfile.getPregnancyCheckFlag());
				    			
				    			
				    			boolean privateFlag = false;
				    			if ( privateFlagMap.get(medCase.getCaseNum()) != null ) {
				    				privateFlag = privateFlagMap.get(medCase.getCaseNum());
				    			}
				    			mpTrxRptDtl.setPrivateFlag(privateFlag);
				    			
				    			Patient patient = medProfile.getPatient();
				    			mpTrxRptDtl.setPatName(patient.getName());
				    			mpTrxRptDtl.setPatNameChi(patient.getNameChi());
				    			mpTrxRptDtl.setHkid(patient.getHkid());
				    			
				    			if( medCase.getCaseNum() != null ){
				    				mpTrxRptDtl.setPrintPatHkidFlag(REPORT_TRX_HKID_VISIBLE.get(false));
				    			}else{
				    				mpTrxRptDtl.setPrintPatHkidFlag(true);
				    			}
				    			
				    			if( patient.getSex() != null ){
				    				mpTrxRptDtl.setSex(patient.getSex().getDataValue());
				    			}
				    			if( patient.getAge() != null ){
				    				mpTrxRptDtl.setAge(patient.getAge()+patient.getDateUnit().substring(0, 1));
				    			}
				    			
				    			if(medProfile.getBodyWeight() != null){
				    				DecimalFormat df = new DecimalFormat("0.##");
					    			String bodyWeight = df.format(medProfile.getBodyWeight().stripTrailingZeros());
					    			mpTrxRptDtl.setBodyWeight(bodyWeight);
				    			}
				    			
				    			mpTrxRptItemDtlList = new ArrayList<MpTrxRptItemDtl>();
				    			mpTrxRptDtl.setMpTrxRptItemDtlList(mpTrxRptItemDtlList);
				    			
				    			medProfileMoIdMap.put(medProfile.getId(),mpTrxRptDtl);
			    			}
			    		
			    			mpTrxRptItemDtl = new MpTrxRptItemDtl();
			    			
			    			RxItem rxItem = rxJaxbWrapper.unmarshall(medProfileMoItem.getRxItemXml());
			    			rxItem.buildTlf();
			    			
			    			List<MpTrxRptPoItemHdr> mpTrxRptPoItemHdrList = new ArrayList<MpTrxRptPoItemHdr>();
			    			constructOverrideReason(mpTrxRptPoItemHdrList,medProfileMoItem);
			    			
			    			boolean showDosageInstruction = Prop.REPORT_TRX_DOSAGEINSTRUCTION_VISIBLE.get(false);
			    			
			    			if( drugRefillListRptFlag ){
			    				showDosageInstruction = Prop.REPORT_DRUGREFILLLIST_DOSAGEINSTRUCTION_VISIBLE.get(false);
			    			}
			    			
			    			if( medProfileMoItem.getItemNum() > 10000 ){
			    				if( showDosageInstruction ){
				    				MpDispLabel mpDispLabel = labelJaxbWrapper.unmarshall(deliveryItem.getLabelXml());
				    				if( mpDispLabel.getPrintOption() == null ){
				    					PrintOption printOption = new PrintOption();
				    					printOption.setPrintLang(PrintLang.Eng);
				    					mpDispLabel.setPrintOption(printOption);
				    				}
				    				mpTrxRptItemDtl.setDrugOrder(mpDispLabel.getInstructionText());
			    				}
			    			}else{
				    			//Format for IP only
				    			if( rxItem.getDrugOrderXml() != null ){
				    				mpTrxRptItemDtl.setDrugOrder(convertDrugOrderXml(rxItem.getDrugOrderXml(), rxItem.getInterfaceVersion()));
				    			}else{
					    			mpTrxRptItemDtl.setDrugOrder(convertDrugOrderTlf(rxItem.getFormattedDrugOrderTlf()));
				    			}
			    			}
			    			
			    			mpTrxRptItemDtl.setStartDate(medProfileMoItem.getStartDate());
			    			mpTrxRptItemDtl.setEndDate(medProfileMoItem.getEndDate());
			    			
			    			if ( rxItem.isCapdRxDrug() ) {
			    				CapdRxDrug capdRxDrug = (CapdRxDrug) rxItem;
			    				mpTrxRptItemDtl.setReviewDate(capdRxDrug.getCapdItemList().get(0).getReviewDate());
			    			} else if (rxItem.isRxDrug()) {
			    				RxDrug rxDrug = (RxDrug) rxItem;
			    				mpTrxRptItemDtl.setReviewDate(rxDrug.getRegimen().getDoseGroupList().get(0).getReviewDate());
			    			} else if (rxItem.isIvRxDrug()) {
			    				IvRxDrug ivRxDrug = (IvRxDrug)rxItem;
			    				mpTrxRptItemDtl.setReviewDate(ivRxDrug.getReviewDate());
			    			}
			    			
							mpTrxRptItemDtl.setSeparateLineIndicator(true);
							mpTrxRptItemDtl.setMedProfileMoItemId(medProfileMoItem.getId());
							mpTrxRptItemDtl.setMedProfileMoItemOrgItemNum(medProfileMoItem.getOrgItemNum());
			    			List<MpTrxRptPoItemDtl> mpTrxRptPoItemDtlList = new ArrayList<MpTrxRptPoItemDtl>();
				    		
			    			for (MedProfilePoItem medProfilePoItem : medProfileMoItem.getMedProfilePoItemList()){
			    				List<DeliveryItem> deliveryItemMapDeliveryItemList = deliveryItemMap.get(medProfilePoItem.getId());
			    				if ( deliveryItemMapDeliveryItemList != null ) {
				    				for ( DeliveryItem deliveryItemMapDeliveryItem : deliveryItemMapDeliveryItemList ) {
				    					if ( deliveryItemMapDeliveryItem != null ){
				    						Boolean isReplenishment = (deliveryItemMapDeliveryItem.getReplenishmentItem() != null ?  true : false);
				    						if(	!NON_ACTIVE_DELIVERY_ITEM_STATUS_LIST.contains(deliveryItemMapDeliveryItem.getStatus() )) {
					    						if( isReplenishment ){			
					    							MpTrxRptPoItemDtl mpTrxRptPoItemDtl = constructMpTrxRptPoItemDtl(medProfilePoItem, deliveryItemMapDeliveryItem, true, mpTrxRptItemDtl);
						    						mpTrxRptPoItemDtlList.add(mpTrxRptPoItemDtl);
					    						}else{							    				
					    							MpTrxRptPoItemDtl mpTrxRptPoItemDtl = constructMpTrxRptPoItemDtl(medProfilePoItem, deliveryItemMapDeliveryItem, false, mpTrxRptItemDtl);
					    							mpTrxRptPoItemDtlList.add(mpTrxRptPoItemDtl);
					    						}
				    						}else {
				    								//Unlink / Delete / Keep record
				    								MpTrxRptPoItemDtl mpTrxRptPoItemDtl = constructMpTrxRptPoItemDtl(medProfilePoItem, deliveryItemMapDeliveryItem, false, mpTrxRptItemDtl);
				    								mpTrxRptPoItemDtlList.add(mpTrxRptPoItemDtl);
				    						}
				    					}
				    				}
			    				} else {
			    					if(!deliveryReplenishItemForZeroQtyMap.containsKey(medProfilePoItem.getId())){
		    							//no delivery item in MO (not due /complete)
			    						MpTrxRptPoItemDtl mpTrxRptPoItemDtl = constructMpTrxRptPoItemDtl(medProfilePoItem, null, false, mpTrxRptItemDtl);
		    							mpTrxRptPoItemDtlList.add(mpTrxRptPoItemDtl);
		    						}
		    						//else not print item
			    				}
			    			}
			    			mpTrxRptItemDtl.setMpTrxRptPoItemHdrList(mpTrxRptPoItemHdrList);
			    			mpTrxRptItemDtl.setMpTrxRptPoItemDtlList(mpTrxRptPoItemDtlList);
			    			mpTrxRptItemDtl.setMpTrxRptPivasItemDtlList(null);
		    			
			    			boolean allmpTrxRptPoItemDtlItemStatusDisplay = false;
			    			
			    			for (MpTrxRptPoItemDtl mpTrxRptPoItemDtl :mpTrxRptPoItemDtlList){
			    				if(!( mpTrxRptPoItemDtl.getDeleted() || mpTrxRptPoItemDtl.getItemType().equals(NOTDISPENSE) ||  
			    						mpTrxRptPoItemDtl.getItemType().equals(NOTDUE) ||  mpTrxRptPoItemDtl.getItemType().equals(COMPLETED)) ){
				    				
			    					allmpTrxRptPoItemDtlItemStatusDisplay = true;
			    					break;
			    				}
			    				
			    			}
			    			
			    			if(allmpTrxRptPoItemDtlItemStatusDisplay){
			    				mpTrxRptItemDtlList.add(mpTrxRptItemDtl);
				    			
			    			}
		    			}	
		    		}
	    		}catch(Exception e){
					if( deliveryItem != null ){
						MedProfilePoItem medProfilePoItem = deliveryItem.resolveMedProfilePoItem();
						if( medProfilePoItem != null && 
								medProfilePoItem.getMedProfileMoItem() != null && 
								medProfilePoItem.getMedProfileMoItem().getMedProfileItem() != null &&
								medProfilePoItem.getMedProfileMoItem().getMedProfileItem().getMedProfile() != null &&
								medProfilePoItem.getMedProfileMoItem().getMedProfileItem().getMedProfile().getMedCase() != null ){
							throw new DueOrderPrintException(e, delivery.getWardCode(), medProfilePoItem.getMedProfileMoItem().getMedProfileItem().getMedProfile().getMedCase().getCaseNum());
						}else{
							throw new DueOrderPrintException(e, delivery.getWardCode(), "");
						}
					}else{
						throw new DueOrderPrintException(e, "", "");
					}
				}
	    	}
		}
		
		return medProfileMoIdMap;
		
	}
	
	
	private Map<Long, MpTrxRptDtl> constructMedProfilePivasItem(Delivery delivery, MpTrxRpt mpTrxRpt){
		
		Map<Long, MpTrxRptDtl> medProfileMoIdMap = new HashMap<Long, MpTrxRptDtl>();
		
		List<DeliveryItem> deliveryItemList = delivery.getDeliveryItemList();
		HashSet<Long> prevMoItemIdSet = new HashSet<Long>();

		Map<Long, List<PivasWorklist>> pivasWorklistMap = new HashMap<Long, List<PivasWorklist>>();
		Map<Long, List<DeliveryItem>> deliveryItemMap = new HashMap<Long, List<DeliveryItem>>();
		
    	for (DeliveryItem deliveryItem : deliveryItemList){
    		PivasWorklist pivasWorklist = deliveryItem.getPivasWorklist();
    		MedProfileMoItem medProfileMoItem = pivasWorklist.getMedProfileMoItem();
    		if (pivasWorklist != null) {
    			if ( !pivasWorklistMap.containsKey(medProfileMoItem.getId())) {
    				pivasWorklistMap.put(medProfileMoItem.getId(), new ArrayList<PivasWorklist>());
    			}
    			pivasWorklistMap.get(medProfileMoItem.getId()).add(pivasWorklist);
    			
    			if ( !deliveryItemMap.containsKey(pivasWorklist.getId()) ) {
    				deliveryItemMap.put(pivasWorklist.getId(), new ArrayList<DeliveryItem>());
    			}
        		deliveryItemMap.get(pivasWorklist.getId()).add(deliveryItem);
    		}
    	}
    	
		if (deliveryItemList.size()>0){	
			MpTrxRptHdr mpTrxRptHdr = constructMpTrxRptHdr(deliveryItemList.get(0).getDelivery());
			mpTrxRpt.setMpTrxRptHdr(mpTrxRptHdr);

			List<MpTrxRptItemDtl> mpTrxRptItemDtlList = null;
			MpTrxRptItemDtl mpTrxRptItemDtl = null ;
			JaxbWrapper<RxItem> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
			
			Map<String, Boolean> privateFlagMap = getPrivateFlagMap(delivery);
			
	    	for (DeliveryItem deliveryItem : deliveryItemList) {
	    		try{
		    			MedProfileMoItem medProfileMoItem = deliveryItem.getMedProfileMoItem();
		    			MedProfile medProfile = medProfileMoItem.getMedProfileItem().getMedProfile();
		    			MedCase medCase = medProfile.getMedCase();
		    			
			    		if (!prevMoItemIdSet.contains(medProfileMoItem.getId())){
			    			prevMoItemIdSet.add(medProfileMoItem.getId());
			        		
			    			if (medProfileMoIdMap.containsKey(medProfile.getId())){
				    			mpTrxRptItemDtlList = medProfileMoIdMap.get(medProfile.getId()).getMpTrxRptItemDtlList();
				    			
			    			} else {
				    			MpTrxRptDtl mpTrxRptDtl = new MpTrxRptDtl();
				    			
					    		convertAlertProfile(mpTrxRptHdr, mpTrxRptDtl, medProfile);
					    		
					    		mpTrxRptDtl.setDrugSensitivityDesc(medProfile.getDrugSensitivity());					    		
				    			mpTrxRptDtl.setBedNum(medCase.getPasBedNum());
				    			mpTrxRptDtl.setCaseNum(medCase.getCaseNum());
				    			mpTrxRptDtl.setWard(medProfile.getWardCode());
				    			mpTrxRptDtl.setSpecCode(medProfile.getSpecCode());
				    			mpTrxRptDtl.setCheckPregnancyFlag(medProfile.getPregnancyCheckFlag());
				    			
				    			
				    			boolean privateFlag = false;
				    			if ( privateFlagMap.get(medCase.getCaseNum()) != null ) {
				    				privateFlag = privateFlagMap.get(medCase.getCaseNum());
				    			}
				    			mpTrxRptDtl.setPrivateFlag(privateFlag);
				    			
				    			Patient patient = medProfile.getPatient();
				    			mpTrxRptDtl.setPatName(patient.getName());
				    			mpTrxRptDtl.setPatNameChi(patient.getNameChi());
				    			mpTrxRptDtl.setHkid(patient.getHkid());
				    			
				    			if( medCase.getCaseNum() != null ){
				    				mpTrxRptDtl.setPrintPatHkidFlag(REPORT_TRX_HKID_VISIBLE.get(false));
				    			}else{
				    				mpTrxRptDtl.setPrintPatHkidFlag(true);
				    			}
				    			
				    			if( patient.getSex() != null ){
				    				mpTrxRptDtl.setSex(patient.getSex().getDataValue());
				    			}
				    			if( patient.getAge() != null ){
				    				mpTrxRptDtl.setAge(patient.getAge()+patient.getDateUnit().substring(0, 1));
				    			}
				    			
				    			if(medProfile.getBodyWeight() != null){
				    				DecimalFormat df = new DecimalFormat("0.##");
					    			String bodyWeight = df.format(medProfile.getBodyWeight().stripTrailingZeros());
					    			mpTrxRptDtl.setBodyWeight(bodyWeight);
				    			}
				    			
				    			mpTrxRptItemDtlList = new ArrayList<MpTrxRptItemDtl>();
				    			mpTrxRptDtl.setMpTrxRptItemDtlList(mpTrxRptItemDtlList);
				    			
				    			medProfileMoIdMap.put(medProfile.getId(),mpTrxRptDtl);
			    			}
			    		
			    			mpTrxRptItemDtl = new MpTrxRptItemDtl();
			    			
			    			RxItem rxItem = rxJaxbWrapper.unmarshall(medProfileMoItem.getRxItemXml());
			    			rxItem.buildTlf();
			    			
			    			List<MpTrxRptPoItemHdr> mpTrxRptPoItemHdrList = new ArrayList<MpTrxRptPoItemHdr>();
			    			constructOverrideReason(mpTrxRptPoItemHdrList,medProfileMoItem);
			    			
				    		if( rxItem.getDrugOrderXml() != null ){
				    			mpTrxRptItemDtl.setDrugOrder(convertDrugOrderXml(rxItem.getDrugOrderXml(), rxItem.getInterfaceVersion()));
				    		}else{
					    		mpTrxRptItemDtl.setDrugOrder(convertDrugOrderTlf(rxItem.getFormattedDrugOrderTlf()));
				    		}
			    			
			    			mpTrxRptItemDtl.setStartDate(medProfileMoItem.getStartDate());
			    			mpTrxRptItemDtl.setEndDate(medProfileMoItem.getEndDate());
			    			
			    			if ( rxItem.isCapdRxDrug() ) {
			    				CapdRxDrug capdRxDrug = (CapdRxDrug) rxItem;
			    				mpTrxRptItemDtl.setReviewDate(capdRxDrug.getCapdItemList().get(0).getReviewDate());
			    			} else if (rxItem.isRxDrug()) {
			    				RxDrug rxDrug = (RxDrug) rxItem;
			    				mpTrxRptItemDtl.setReviewDate(rxDrug.getRegimen().getDoseGroupList().get(0).getReviewDate());
			    			} else if (rxItem.isIvRxDrug()) {
			    				IvRxDrug ivRxDrug = (IvRxDrug)rxItem;
			    				mpTrxRptItemDtl.setReviewDate(ivRxDrug.getReviewDate());
			    			}
			    			
							mpTrxRptItemDtl.setSeparateLineIndicator(true);
							mpTrxRptItemDtl.setMedProfileMoItemId(medProfileMoItem.getId());
							mpTrxRptItemDtl.setMedProfileMoItemOrgItemNum(medProfileMoItem.getOrgItemNum());
			    			List<MpTrxRptPivasItemDtl> mpTrxRptPivasItemDtlList = new ArrayList<MpTrxRptPivasItemDtl>();
			    			
			    			for ( PivasWorklist pivasWorklist : pivasWorklistMap.get(medProfileMoItem.getId()) ) {
			    				List<DeliveryItem> deliveryItemMapDeliveryItemList = deliveryItemMap.get(pivasWorklist.getId());
			    				if ( deliveryItemMapDeliveryItemList != null ) {
				    				for ( DeliveryItem deliveryItemMapDeliveryItem : deliveryItemMapDeliveryItemList ) {
			    						MpTrxRptPivasItemDtl mpTrxRptPivasItemDtl = constructMpTrxRptPivasItemDtl(deliveryItemMapDeliveryItem.getPivasWorklist(), deliveryItemMapDeliveryItem, mpTrxRptItemDtl);
			    						mpTrxRptPivasItemDtlList.add(mpTrxRptPivasItemDtl);	
				    				}
			    				} else {
			    					//no delivery item in MO (not due /complete)
			    					MpTrxRptPivasItemDtl mpTrxRptPivasItemDtl = constructMpTrxRptPivasItemDtl(deliveryItem.getPivasWorklist(), null, mpTrxRptItemDtl);
			    					mpTrxRptPivasItemDtlList.add(mpTrxRptPivasItemDtl);
			    				}
			    			}
		    				
			    			mpTrxRptItemDtl.setMpTrxRptPoItemHdrList(mpTrxRptPoItemHdrList);
			    			mpTrxRptItemDtl.setMpTrxRptPoItemDtlList(null);
			    			mpTrxRptItemDtl.setMpTrxRptPivasItemDtlList(mpTrxRptPivasItemDtlList);

			    			boolean allmpTrxRptPivasItemDtlItemStatusDisplay = false;
			    			
			    			for (MpTrxRptPivasItemDtl mpTrxRptPivasItemDtl :mpTrxRptPivasItemDtlList) {
			    				if(!( mpTrxRptPivasItemDtl.getDeletedFlag() || mpTrxRptPivasItemDtl.getItemType().equals(NOTDISPENSE) ||  
			    						mpTrxRptPivasItemDtl.getItemType().equals(NOTDUE) ||  mpTrxRptPivasItemDtl.getItemType().equals(COMPLETED)) ){
				    				
			    					allmpTrxRptPivasItemDtlItemStatusDisplay = true;
			    					break;
			    				}
			    			}
			    			
			    			if(allmpTrxRptPivasItemDtlItemStatusDisplay) {
			    				mpTrxRptItemDtlList.add(mpTrxRptItemDtl);
				    			
			    			}	
		    			}
	    		}catch(Exception e){
					if( deliveryItem != null ){
						MedProfilePoItem medProfilePoItem = deliveryItem.resolveMedProfilePoItem();
						if( medProfilePoItem != null && 
								medProfilePoItem.getMedProfileMoItem() != null && 
								medProfilePoItem.getMedProfileMoItem().getMedProfileItem() != null &&
								medProfilePoItem.getMedProfileMoItem().getMedProfileItem().getMedProfile() != null &&
								medProfilePoItem.getMedProfileMoItem().getMedProfileItem().getMedProfile().getMedCase() != null ){
							throw new DueOrderPrintException(e, delivery.getWardCode(), medProfilePoItem.getMedProfileMoItem().getMedProfileItem().getMedProfile().getMedCase().getCaseNum());
						}else{
							throw new DueOrderPrintException(e, delivery.getWardCode(), "");
						}
					}else{
						throw new DueOrderPrintException(e, "", "");
					}
				}
	    	}
		}
		
		return medProfileMoIdMap;
		
	}
	
	private boolean checkMedProfilePoiOrPivasWorklistDisplayable(DeliveryItem deliveryItem){
		if( DeliveryItemStatus.KeepRecord.equals(deliveryItem.getStatus())){
			return true;
		}else{
			return false;
		}
	}

	private MpTrxRptHdr constructMpTrxRptHdr(Delivery delivery) {
		
		MpTrxRptHdr mpTrxRptHdr = new MpTrxRptHdr();

		if (delivery.getDeliveryItemList() != null && delivery.getDeliveryItemList().get(0).getPivasWorklist() != null) {
			mpTrxRptHdr.setPivasTrxRptFlag(true);
		}
		else {
			mpTrxRptHdr.setPivasTrxRptFlag(false);
		}
		
		mpTrxRptHdr.setWardCode(delivery.getWardCode());
		mpTrxRptHdr.setNumOfItem(retrieveNumOfItem(delivery));
		int batchDay = new DateTime(delivery.getBatchDate()).getDayOfMonth();
		mpTrxRptHdr.setBatchCode( (( batchDay < 10 )?"0":"") + batchDay+delivery.getBatchNum() );
		
		String hospCode = workstore.getHospCode();
		
		if( delivery.getBatchNum() != null ){
			mpTrxRptHdr.setDeliveryId(mpTrxRptHdr.getBatchCode() +
												hospCode +
												dateFormat.format(delivery.getBatchDate()) +
												delivery.getWardCode());
		}
		
		if (!mpTrxRptHdr.getPivasTrxRptFlag()) {
			if (delivery.getPrintMode() == PrintMode.Batch) {
					if( delivery.getDeliveryRequest() != null && delivery.getDeliveryRequest().getDueDate() != null ){
						mpTrxRptHdr.setDueDate(delivery.getDeliveryRequest().getRefillDueDate());	
					}else{
						mpTrxRptHdr.setDueDate(delivery.getCreateDate());
					}
				}
				
			

			String asapString ="";
			Date deliveryDate = deliveryManager.calculateTargetDeliveryDate(delivery.getDeliveryItemList()); 
			
			if(deliveryDate != null){
				if( (new Date()).after(deliveryDate)){
					asapString = " ASAP";
				}
				mpTrxRptHdr.setDeliveryDate(dateFormatWithBracketTime.format(deliveryDate) + asapString);
				mpTrxRptHdr.setPivasSupplyDate(null);
			}		
		}
		else {
			mpTrxRptHdr.setDueDate(null);
			mpTrxRptHdr.setDeliveryDate(null);
			if (delivery.getDeliveryItemList() != null && delivery.getDeliveryItemList().get(0).getPivasWorklist() != null) {
				mpTrxRptHdr.setPivasSupplyDate(dateFormatWithBracketTime.format(delivery.getDeliveryItemList().get(0).getPivasWorklist().getSupplyDate()));
			}	
		}
		
		if(!delivery.getDeliveryRequest().getDeliveryRequestAlertList().isEmpty()){
			
			boolean alertProfileFlag = true;
			boolean ehrAlertProfileFlag = true;
			boolean mdsCheckFlag = true;
			boolean ddiCheckFlag = true;
			
			for(DeliveryRequestAlert dra : delivery.getDeliveryRequest().getDeliveryRequestAlertList()){
				if(!dra.getAlertProfileFlag()){
					alertProfileFlag = false;
				}
				if (Boolean.FALSE.equals(dra.getEhrAlertProfileFlag())) {
					ehrAlertProfileFlag = false;
				}
				if(!dra.getMdsCheckFlag()){
					mdsCheckFlag = false;
				}
				
				if(Boolean.FALSE.equals(dra.getDdiCheckFlag())){
					ddiCheckFlag = false;
				}
				
				if(!alertProfileFlag && !mdsCheckFlag && !ddiCheckFlag && !ehrAlertProfileFlag){
					break;
				}
			}

			mpTrxRptHdr.setAlertProfileFlag(alertProfileFlag);
			mpTrxRptHdr.setEhrAlertProfileFlag(ehrAlertProfileFlag);
			mpTrxRptHdr.setMdsCheckFlag(mdsCheckFlag);
			mpTrxRptHdr.setDdiCheckFlag(ddiCheckFlag);
			
		}else{
			
			mpTrxRptHdr.setAlertProfileFlag(false);
			mpTrxRptHdr.setMdsCheckFlag(false);
			mpTrxRptHdr.setDdiCheckFlag(false);
		}
		
		return mpTrxRptHdr;
	}
	
	private Integer retrieveNumOfItem(Delivery delivery) {
		
		int count = 0;
		
		for ( DeliveryItem deliveryItem : delivery.getDeliveryItemList()) {
			if (!NON_ACTIVE_DELIVERY_ITEM_STATUS_LIST.contains(deliveryItem.getStatus())) {
				count++;
			}
		}
		
		return count;
		
	}
	
	private MpTrxRptPoItemDtl constructMpTrxRptPoItemDtl(MedProfilePoItem medProfilePoItem, DeliveryItem deliveryItem, 
																		Boolean isReplenish,
																		MpTrxRptItemDtl mpTrxRptItemDtl) {	

		
		MpTrxRptPoItemDtl mpTrxRptPoItemDtl = new MpTrxRptPoItemDtl();
		mpTrxRptPoItemDtl.setMedProfilePoItemId(medProfilePoItem.getId());
		mpTrxRptPoItemDtl.setDeleted((deliveryItem != null && (DeliveryItemStatus.Deleted ==deliveryItem.getStatus() || DeliveryItemStatus.Unlink ==deliveryItem.getStatus()))?true:false);
		
		if( medProfilePoItem.getLastDueDate() != null ){
			mpTrxRptPoItemDtl.setLastDueDate(medProfilePoItem.getLastDueDate());
		}
		
		if( StringUtils.isNotBlank(medProfilePoItem.getRemarkText()) ){
			mpTrxRptPoItemDtl.setRemark(medProfilePoItem.getRemarkText());
		}
		
		if (isReplenish) {
			mpTrxRptPoItemDtl.setItemType(REPLENISH);
			if( deliveryItem != null ){
				if (deliveryItem.getIssueQty()!=null && StringUtils.isNotBlank(medProfilePoItem.getBaseUnit()) ) {
					mpTrxRptPoItemDtl.setDispQty(deliveryItem.getIssueQty().intValue()+ " " +medProfilePoItem.getBaseUnit());
				}
				if ( deliveryItem.getAdjQty() != null ) {
					mpTrxRptPoItemDtl.setAdjQty(setAdjQty(deliveryItem.getAdjQty()));
					if ( deliveryItem.getPreIssueQty() != null ) {
						mpTrxRptPoItemDtl.setIssueQty(deliveryItem.getPreIssueQty().setScale(0, RoundingMode.UP).toString());
					} else {
						mpTrxRptPoItemDtl.setIssueQty(setIssueQty(deliveryItem.getIssueQty(), deliveryItem.getAdjQty()));
					}
				}
			}
		} else {
			if( deliveryItem != null ){
				if (deliveryItem.getRefillFlag()){
					mpTrxRptPoItemDtl.setItemType(REFILL);
				}else {
					mpTrxRptPoItemDtl.setItemType(NEW);
				}
				
				if(DeliveryItemStatus.Deleted.equals(deliveryItem.getStatus())){
					mpTrxRptPoItemDtl.setItemType(DeliveryItemStatus.Deleted.getDisplayValue());
				}else if(DeliveryItemStatus.Unlink.equals(deliveryItem.getStatus())){
					mpTrxRptPoItemDtl.setItemType(DeliveryItemStatus.Unlink.getDisplayValue());
				}
				
				if (deliveryItem.getIssueQty()!=null && medProfilePoItem.getBaseUnit()!=null) {
					mpTrxRptPoItemDtl.setDispQty(deliveryItem.getIssueQty().intValue()+ " " +medProfilePoItem.getBaseUnit());
				}

				if ( deliveryItem.getAdjQty() != null ) {
					mpTrxRptPoItemDtl.setAdjQty(setAdjQty(deliveryItem.getAdjQty()));
					if ( deliveryItem.getPreIssueQty() != null ) {
						mpTrxRptPoItemDtl.setIssueQty(deliveryItem.getPreIssueQty().setScale(0, RoundingMode.UP).toString());
					} else {
						mpTrxRptPoItemDtl.setIssueQty(setIssueQty(deliveryItem.getIssueQty(), deliveryItem.getAdjQty()));
					}
				}

				if(!medProfilePoItem.getMedProfileMoItem().getDoseSpecifiedFlag()){
					if (deliveryItem.getIssueDuration() != null && deliveryItem.getIssueDuration().doubleValue() > 0){
						mpTrxRptPoItemDtl.setDuration( deliveryItem.getIssueDuration().setScale(0, RoundingMode.CEILING).intValue()
								+(deliveryItem.getIssueDuration().doubleValue()%1 != 0 ? "*" : "")
								+" "+DAYS);
					}
				}
				
				if(checkMedProfilePoiOrPivasWorklistDisplayable(deliveryItem) ) {
					//non dispense - not displayable to display as not dispense
					mpTrxRptPoItemDtl.setItemType(NOTDISPENSE);
					mpTrxRptPoItemDtl.setDuration(null);
					mpTrxRptPoItemDtl.setDispQty(null);
					mpTrxRptPoItemDtl.setAdjQty(null);
					mpTrxRptPoItemDtl.setIssueQty(null);
				}
				
			}else{
				// no delivery item in MO
				if(medProfilePoItem.getDueDate().equals(medProfilePoItem.getEndDate()) || 
						(medProfilePoItem.getSingleDispFlag() && medProfilePoItem.getLastDueDate() != null) || 
						(medProfilePoItem.getRemainIssueQty() != null && medProfilePoItem.getRemainIssueQty().compareTo(BigDecimal.ZERO) <= 0)){
					mpTrxRptPoItemDtl.setItemType(COMPLETED);
				}else{
					mpTrxRptPoItemDtl.setItemType(NOTDUE);
				}
			}
		}
		
		MpDispLabel mpDispLabel = new MpDispLabel();
		if(deliveryItem != null){
			mpDispLabel = labelJaxbWrapper.unmarshall(deliveryItem.getLabelXml());
		}
		
		String newRefillInfo = "<";
		//Replenishment is single dispense
		
		if(Boolean.TRUE.equals(medProfilePoItem.getSingleDispFlag()) || isReplenish){
			newRefillInfo += "S ";
		}
		
		if(Boolean.TRUE.equals(medProfilePoItem.getDangerDrugFlag()) ){
			newRefillInfo += "DD";
		}
		
		if(Boolean.TRUE.equals(mpDispLabel.getWardStockFlag())){
			newRefillInfo += "W";
		}else if(mpDispLabel.getWardStockFlag() == null && Boolean.TRUE.equals(medProfilePoItem.getWardStockFlag()) ){
			newRefillInfo += "W";
		}
		
		//Replenishment is redispense if and only if wardstock
		if(Boolean.TRUE.equals(mpDispLabel.getReDispFlag()) || (isReplenish && Boolean.TRUE.equals(mpDispLabel.getWardStockFlag()))){
			newRefillInfo += "R";
		}else if(mpDispLabel.getReDispFlag() == null){
			if(Boolean.TRUE.equals(medProfilePoItem.getReDispFlag()) || (isReplenish && Boolean.TRUE.equals(medProfilePoItem.getWardStockFlag()))){
				newRefillInfo += "R";
			}
		}
		
		newRefillInfo +=">";
		
		if("<S >".equals(newRefillInfo)){
			newRefillInfo = "<S>";
		}
		
		if("<>".equals(newRefillInfo)){
			mpTrxRptPoItemDtl.setNewRefillInfo("");
		}else{
			mpTrxRptPoItemDtl.setNewRefillInfo(newRefillInfo);
		}

		mpTrxRptPoItemDtl.setActionStatus(medProfilePoItem.getActionStatus());
		
		mpTrxRptPoItemDtl.setItemCode(medProfilePoItem.getItemCode());

		mpTrxRptPoItemDtl.setLabelDesc(buildDrugDesc(
				medProfilePoItem.getDrugName(),
				medProfilePoItem.getFormLabelDesc(),
				medProfilePoItem.getStrength(),
				medProfilePoItem.getVolumeText(),
				deliveryItem == null || deliveryItem.getId() == null ? refTableManager.getFdnByItemCodeOrderTypeWorkstore(medProfilePoItem.getItemCode(), OrderType.InPatient, workstore) : labelJaxbWrapper.unmarshall(deliveryItem.getLabelXml()).getFloatingDrugName()));

		if ( deliveryItem != null && ATDPS_BINNUM.equals(deliveryItem.getBinNum()) ) {
			mpTrxRptPoItemDtl.setUnitDoseFlag(true);
		} else {
			mpTrxRptPoItemDtl.setUnitDoseFlag(false);
		}
		
		return mpTrxRptPoItemDtl;
		
	}
	

	private MpTrxRptPivasItemDtl constructMpTrxRptPivasItemDtl(PivasWorklist pivasWorklist, DeliveryItem deliveryItem, MpTrxRptItemDtl mpTrxRptItemDtl) {	
		
		MpTrxRptPivasItemDtl mpTrxRptPivasItemDtl = new MpTrxRptPivasItemDtl();

		mpTrxRptPivasItemDtl.setDeletedFlag((deliveryItem != null && (DeliveryItemStatus.Deleted ==deliveryItem.getStatus() || DeliveryItemStatus.Unlink ==deliveryItem.getStatus()))?true:false);
		
		if( deliveryItem != null ){
			mpTrxRptPivasItemDtl.setDeliveryItemId(deliveryItem.getId().toString());
			if (pivasWorklist.getAdHocFlag()) {
				mpTrxRptPivasItemDtl.setItemType("");
			} else if (deliveryItem.getRefillFlag()) {
				mpTrxRptPivasItemDtl.setItemType(REFILL);
			} else {
				mpTrxRptPivasItemDtl.setItemType(NEW);
			}
				
			if(DeliveryItemStatus.Deleted.equals(deliveryItem.getStatus())){
				mpTrxRptPivasItemDtl.setItemType(DeliveryItemStatus.Deleted.getDisplayValue());	
			}

			if(checkMedProfilePoiOrPivasWorklistDisplayable(deliveryItem)) {	
				//non dispense - not displayable to display as not dispense
				mpTrxRptPivasItemDtl.setItemType(NOTDISPENSE);
			}	
		} else {
			// no delivery item in MO
			if ((pivasWorklist.getNextDueDate() == null) && (pivasWorklist.getNextSupplyDate() == null)) {
				mpTrxRptPivasItemDtl.setItemType(COMPLETED);
			} else {
				mpTrxRptPivasItemDtl.setItemType(NOTDUE);
			}
		}

		if (Boolean.TRUE.equals(pivasWorklist.getPivasPrep().getSingleDispFlag())){
			mpTrxRptPivasItemDtl.setNewRefillInfo("<S>");
		} else {
			mpTrxRptPivasItemDtl.setNewRefillInfo("");
		}
		
		
		MedProfileMoItem medProfileMoItem = pivasWorklist.getMedProfileMoItem();
		JaxbWrapper<Object> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
		RxItem rxItem = (RxItem) rxJaxbWrapper.unmarshall(rxJaxbWrapper.marshall(medProfileMoItem.getRxItem()));
		RxDrug rxDrug = null;
		ActionStatus actionStatus = null;
		
		if (medProfileMoItem.isManualItem()) {
			MedProfilePoItem medProfilePoItem = MedProfileUtils.getFirstItem(medProfileMoItem.getMedProfilePoItemList());
			if (medProfilePoItem != null) {
				actionStatus = medProfilePoItem.getActionStatus();
			}
		} else if (rxItem instanceof IvRxDrug) {
			IvRxDrug ivRxDrug = (IvRxDrug) rxItem;
			rxDrug = MedProfileUtils.getFirstItem(ivRxDrug.getIvAdditiveList());
		} else if (rxItem instanceof RxDrug) {
			rxDrug = (RxDrug) rxItem;
		}
		mpTrxRptPivasItemDtl.setActionStatus(rxDrug == null ? actionStatus : rxDrug.getActionStatus());
		
		
		JaxbWrapper<Object> labelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
		Object label = labelJaxbWrapper.unmarshall(deliveryItem.getLabelXml());
		PivasProductLabel pivasProductLabel = ((LabelContainer) label).getPivasProductLabel();
		PivasWorksheet pivasWorksheet = ((LabelContainer) label).getPivasWorksheet();

		mpTrxRptPivasItemDtl.setPivasDesc(buildPivasTrxRptPivasDesc(pivasProductLabel));
				
		if (pivasProductLabel.isShelfLifeZeroFlag()) {
			mpTrxRptPivasItemDtl.setPrepExpiryDate("Use Immediately");
		} else {
			mpTrxRptPivasItemDtl.setPrepExpiryDate(dateFormatWithTime.format(pivasProductLabel.getPrepExpiryDate()));
		}

		if (pivasWorksheet.isSplitFlag()) {
			mpTrxRptPivasItemDtl.setFinalVolume(pivasWorksheet.getFinalVolumeSplit());
		} else {
			mpTrxRptPivasItemDtl.setFinalVolume(pivasWorksheet.getFinalVolume());
		}
		
		mpTrxRptPivasItemDtl.setPrepQty(pivasWorksheet.getPrepQty());
		mpTrxRptPivasItemDtl.setContainerName(pivasWorksheet.getContainerName());
		mpTrxRptPivasItemDtl.setFirstDoseDateTime(dateFormatWithTime.format(pivasProductLabel.getFirstDoseDateTime()));

		mpTrxRptPivasItemDtl.setIssueQty(pivasWorksheet.getIssueQty());
		
		String adjQty = pivasWorksheet.getAdjQty();
		if (StringUtils.isNotBlank(adjQty)) {
			Integer adjQtyCompareResult = new BigDecimal(adjQty).compareTo(BigDecimal.ZERO);
			if (adjQtyCompareResult > 0) {
				mpTrxRptPivasItemDtl.setAdjQty("+" + adjQty);
			} else if (adjQtyCompareResult < 0) {
				mpTrxRptPivasItemDtl.setAdjQty(adjQty);
			} else {
				mpTrxRptPivasItemDtl.setAdjQty(null);
			}
		}
		
		return mpTrxRptPivasItemDtl;
	}
	
	private String buildDrugDesc(String drugName, String formLabelDesc, String strength, String volumeText, String synonym){
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(WordUtils.capitalizeFully(drugName, DELIMITER));
		if (StringUtils.isNotBlank(formLabelDesc)){
			sb.append(" ").append(WordUtils.capitalizeFully(formLabelDesc, DELIMITER));
		}
		if (StringUtils.isNotBlank(strength)){
			sb.append(" ").append(WordUtils.capitalizeFully(strength, DELIMITER));
		}
		if (StringUtils.isNotBlank(volumeText)){
			sb.append(" ").append(volumeText.toLowerCase());
		}
		if (StringUtils.isNotBlank(synonym)){
			sb.append(" (").append(synonym.toUpperCase()).append(")");
		}
		
		return sb.toString();
		
	}
	
	private String buildPivasTrxRptPivasDesc(PivasProductLabel pivasProductLabel) {
		
		String pivasDesc = pivasProductLabel.getFormulaPrintName() + " ";
		
		if (!pivasProductLabel.isSplitFlag()) {
			pivasDesc += pivasProductLabel.getOrderDosage();
		} else {
			pivasDesc += pivasProductLabel.getOrderDosageSplit();
		}
		pivasDesc += pivasProductLabel.getPivasDosageUnit() +  " ";
		
		if (!StringUtils.isBlank(pivasProductLabel.getDiluentDesc())) {
			//PDU = ML, have diluent || PDU != ML, have diluent
			pivasDesc += "in " + ((!pivasProductLabel.isSplitFlag())?pivasProductLabel.getFinalVolume():pivasProductLabel.getFinalVolumeSplit()) + "ML " + pivasProductLabel.getDiluentDesc();
		} else if (!"ML".equalsIgnoreCase(pivasProductLabel.getPivasDosageUnit())) {
			//PDU != ML, no diluent
			pivasDesc += "in " + ((!pivasProductLabel.isSplitFlag())?pivasProductLabel.getFinalVolume():pivasProductLabel.getFinalVolumeSplit()) + "ML";
		}
		//else PDU = ML, no diluent, add nothing
		
		pivasDesc += NEW_LINE;
		
		if (!"ML".equalsIgnoreCase(pivasProductLabel.getPivasDosageUnit())) {
			pivasDesc += pivasProductLabel.getConcn() + pivasProductLabel.getPivasDosageUnit() + "/ML - ";
		}
		
		pivasDesc += pivasProductLabel.getSiteDesc();
		
		if (StringUtils.isNotBlank(pivasProductLabel.getFreq())) {
			pivasDesc +=  " - " + StringUtils.trim(pivasProductLabel.getFreq());
		}
		
		if (StringUtils.isNotBlank(pivasProductLabel.getSupplFreq())) {
			pivasDesc +=  " (" + StringUtils.trim(pivasProductLabel.getSupplFreq().toLowerCase()) + ")";
		}
		
		if (pivasProductLabel.isSplitFlag()) {
			pivasDesc += NEW_LINE + "x " + pivasProductLabel.getSplitCount() + " split";
		}
		
		return pivasDesc;
	}

	private void constructOverrideReason(List<MpTrxRptPoItemHdr> mpTrxRptPoItemHdrList ,MedProfileMoItem medProfileMoItem){
		StringBuilder ddimOverrideReason = new StringBuilder();
		StringBuilder drcmOverrideReason = new StringBuilder();
		StringBuilder otherOverrideReason = new StringBuilder();
		
		List<MedProfileMoItemAlert> medProfileMoItemAlertList = new ArrayList<MedProfileMoItemAlert>();
		medProfileMoItemAlertList.addAll(medProfileMoItem.getMedProfileMoItemAlertList());
		alertMsgManager.sortAlertEntityList(medProfileMoItemAlertList);
		Map<String,Boolean> overrideReasonMap = new HashMap<String,Boolean>();		
		Map<String,Boolean> otherReasonMap = new HashMap<String,Boolean>();		
		Set<String> ivDrugDdimPairKeySet = new HashSet<String>();
		boolean isDdiAlertExist = false; //Same ddi alert message in one MO
		boolean isDrcAlertExist = false; //Same drc alert message in one MO
		boolean isOtherAlertExist = false; //Same patient specified alert message in one MO 
		
		for (MedProfileMoItemAlert medProfileMoItemAlert : medProfileMoItemAlertList){
			MpTrxRptPoItemHdr mpTrxRptPoItemHdr = new MpTrxRptPoItemHdr();
			
			if (medProfileMoItemAlert.isDdiAlert() ){
				isDdiAlertExist = false;
				AlertDdim alertDdim = (AlertDdim) medProfileMoItemAlert.getAlert();
				if ( alertDdim.getItemNum1().equals(alertDdim.getItemNum2()) ) {
					// since orderNum1 and orderNum2 of IP DDIM must be the same, only check whether item num is different 
					List<String> keyList = new ArrayList<String>();
					keyList.add(String.valueOf(alertDdim.getOrderNum1()));
					keyList.add(String.valueOf(alertDdim.getItemNum1()));
					keyList.add(String.valueOf(alertDdim.hashCode()));
					keyList.add(String.valueOf(medProfileMoItemAlert.getSortSeq()));
					String key = StringUtils.join(keyList.toArray(), "|");
					
					if (ivDrugDdimPairKeySet.contains(key)) {
						isDdiAlertExist = true;
					} else {
						ivDrugDdimPairKeySet.add(key);
					}
				}
				
				
				if(!isDdiAlertExist){
					mpTrxRptPoItemHdr.setOverrideReasonHeader("!Drug-drug interaction MDS overriding reason:");
					
					for (String medProfileOverrideReason:medProfileMoItemAlert.getOverrideReason()){
						
						if (medProfileOverrideReason.charAt(medProfileOverrideReason.length()-1) == ASTERISK) {
							medProfileOverrideReason = (medProfileOverrideReason.substring(0, medProfileOverrideReason.length()-1));
						}
						ddimOverrideReason.append(medProfileOverrideReason).append(NEW_LINE);
					}
					mpTrxRptPoItemHdr.setOverrideReasonDetail(ddimOverrideReason.toString());
					ddimOverrideReason = new StringBuilder(); 
					
				}
				
				
			} else {
				
				if (medProfileMoItemAlert.isDrcAlert() && !isDrcAlertExist){
					isDrcAlertExist = true;
					mpTrxRptPoItemHdr.setOverrideReasonHeader("!Dosage range MDS overriding reason:");
						
					for (String medProfileOverrideReason:medProfileMoItemAlert.getOverrideReason()){
						if (medProfileOverrideReason.charAt(medProfileOverrideReason.length()-1) == ASTERISK) {
							medProfileOverrideReason = (medProfileOverrideReason.substring(0, medProfileOverrideReason.length()-1));
						}
						if ( overrideReasonMap.get(medProfileOverrideReason) == null ) {
							overrideReasonMap.put(medProfileOverrideReason, Boolean.TRUE);
							drcmOverrideReason.append(medProfileOverrideReason).append(NEW_LINE);
						}
					}
					mpTrxRptPoItemHdr.setOverrideReasonDetail(drcmOverrideReason.toString());
					drcmOverrideReason = new StringBuilder(); 
				} else if (medProfileMoItemAlert.isPatAlert(MEDPROFILE_ALERT_HLA_ENABLED.get()) && !isOtherAlertExist) {
					isOtherAlertExist = true;
					
					mpTrxRptPoItemHdr.setOverrideReasonHeader("!Patient specific MDS overriding reason:");
						
					for (String medProfileOverrideReason:medProfileMoItemAlert.getOverrideReason()){
					
						if (medProfileOverrideReason.charAt(medProfileOverrideReason.length()-1) == ASTERISK) {
							medProfileOverrideReason = (medProfileOverrideReason.substring(0, medProfileOverrideReason.length()-1));
						}
						if ( otherReasonMap.get(medProfileOverrideReason) == null ) {
							otherReasonMap.put(medProfileOverrideReason, Boolean.TRUE);
							otherOverrideReason.append(medProfileOverrideReason).append(NEW_LINE);
						}
					}
				
					mpTrxRptPoItemHdr.setOverrideReasonDetail(otherOverrideReason.toString());
					otherOverrideReason = new StringBuilder(); 
				}
				
			}
			
			if(!StringUtils.isEmpty(mpTrxRptPoItemHdr.getOverrideReasonHeader()) && !StringUtils.isEmpty(mpTrxRptPoItemHdr.getOverrideReasonDetail())){
				mpTrxRptPoItemHdrList.add(mpTrxRptPoItemHdr);
			}
		}
		
		
	}
	
	
	private void convertAlertProfile(MpTrxRptHdr mpTrxRptHdr, MpTrxRptDtl mpTrxRptDtl, MedProfile medProfile) {

		AlertProfile alertProfile = medProfile.getAlertProfile();
		
		if ( alertProfile != null && !alertProfile.isEhrAllergyOnly()) {
			
			mpTrxRptDtl.setAlertProfileStatus(alertProfile.getStatus().toString());
			
			if ( !alertProfile.getStatus().equals(AlertProfileStatus.Error) ) {
				
				//set Drug Allergy
				StringBuilder allergenSb = new StringBuilder();
				for ( PatAllergy patAllergy : alertProfile.getPatAllergyList()) {
					if (patAllergy.getAllergenType() == AllergenType.NonDrug
							 || patAllergy.getAllergenType() == AllergenType.NoKnownDrugAllergy
							 || patAllergy.getAllergenType() == AllergenType.XP) {
								continue;
					}
					
					if ( StringUtils.isNotBlank(allergenSb.toString()) ) {
						allergenSb.append(" , ");
					}
					allergenSb.append(patAllergy.getAllergenName());
				}
				

				if(StringUtils.trimToNull(allergenSb.toString()) == null){
					mpTrxRptDtl.setNoKnownDrugFlag(alertProfile.getLastVerifyDate() != null? true: false);
				}
				
			
				mpTrxRptDtl.setDrugAllergenDesc(StringUtils.trimToNull(allergenSb.toString()));

				//set Adverse Drug Reaction
				StringBuilder adrSb = new StringBuilder();
				for ( PatAdr patAdr : alertProfile.getPatAdrList()) {
					if ( StringUtils.isNotBlank(adrSb.toString()) ) {
						adrSb.append(" , ");
					}
					adrSb.append(patAdr.getDrugName());
					
				}
				mpTrxRptDtl.setAdrAlertDesc(StringUtils.trimToNull(adrSb.toString()));
				
				//set other Alert
				if ( alertProfile.getG6pdAlert() != null ) {
					mpTrxRptDtl.setOtherAllergyDesc(StringUtils.trimToNull(alertProfile.getG6pdAlert().getAlertDesc()));
				}
			}
		}
		
		if (alertProfile != null && alertProfile.getEhrAllergySummary() != null) {
			mpTrxRptDtl.setEhrAllergy(alertProfile.getEhrAllergySummary().getAllergy());
			mpTrxRptDtl.setEhrAdr(alertProfile.getEhrAllergySummary().getAdr());
		}
	}
	
	public String convertDrugOrderXml(String drugOrderXml, String interfaceVersion) {
		if( StringUtils.isBlank( drugOrderXml ) ){
			return "";
		}

		ParsedOrderLineFormatterBase formatter =  ParsedOrderLineFormatterFactory.getParsedOrderLineFormatter(interfaceVersion);
		formatter.setHint(FormatHints.FORMAT_TYPE, FormatType.TXT);
		formatter.setHint(FormatHints.LINE_WIDTH, MAX_DRUG_ORDER_TLF_LENGTH);
		formatter.setHint(FormatHints.HIDE_STRIKE_THROUGH, false);
		formatter.setHint(FormatHints.EXCLUDE_TXT_ONLY, true);
		return formatter.format(drugOrderXml);
	}
	
	public String convertDrugOrderTlf(String drugOrderTlf) {
		if( StringUtils.isBlank( drugOrderTlf ) ){
			return "";
		}
		
		String[] drugOrderTlfArrayList = StringUtils.splitByWholeSeparator(drugOrderTlf, TLF_BREAK_CHAR);
		
		List<StringBuilder> outputStringBuilderList = new ArrayList<StringBuilder>();
		
		StringBuilder sb = new StringBuilder();
		outputStringBuilderList.add(sb);
		
		for ( int i = 0; i < drugOrderTlfArrayList.length; i++ ) {
			
			drugOrderTlfArrayList[i] = removeDrugOrderTlfTag(drugOrderTlfArrayList[i]);
			
			if (StringUtils.isNotBlank(drugOrderTlfArrayList[i])){
			
				if (sb.length() > 0) {
	
					int currentLineLength;
					String currentLineString = sb.toString();
					if (currentLineString.contains(NEW_LINE)){
						currentLineLength = currentLineString.substring(currentLineString.lastIndexOf(NEW_LINE)+1).length();
					} else {
						currentLineLength = sb.length();
					}
					
					int nextLineLength;
					if (drugOrderTlfArrayList[i].contains(NEW_LINE)){
						nextLineLength = drugOrderTlfArrayList[i].substring(0, drugOrderTlfArrayList[i].indexOf(NEW_LINE)).length();
					} else {
						nextLineLength = drugOrderTlfArrayList[i].length();
					}

					if (currentLineLength + nextLineLength + 1 > MAX_DRUG_ORDER_TLF_LENGTH && 
							!drugOrderTlfArrayList[i].startsWith(NEW_LINE)) {
						sb = new StringBuilder();
						
						outputStringBuilderList.add(sb);
					} else {
						sb.append(" ");
					}
				}
				
				sb.append(drugOrderTlfArrayList[i]);
				
			}
			
		}

		StringBuilder outputStringBuilder = new StringBuilder();
		for ( StringBuilder stringBuilder : outputStringBuilderList ) {
			if (outputStringBuilder.length() > 0) {
				outputStringBuilder.append(NEW_LINE);
			}
			outputStringBuilder.append(stringBuilder.toString());
		}
		
		String result = outputStringBuilder.toString();
		
		if( result.endsWith(NEW_LINE) ){
			result = result.substring(0, result.length()-NEW_LINE.length());
		}
		return result;
		
	}
	
	private String setAdjQty(BigDecimal adjQty){
		String str = null;
		
		if ( adjQty != null && adjQty.compareTo(BigDecimal.ZERO) != 0 ) {
			if ( adjQty.compareTo(BigDecimal.ZERO) > 0 ) {
				return "+"+adjQty.intValue();
			} else {
				return Integer.toString(adjQty.intValue());
			}
		}
		
		return str;
	}
	
	private String setIssueQty(BigDecimal dispQty, BigDecimal adjQty) {
		String issueQty = null;
		if ( adjQty != null ) {
			return Integer.toString(dispQty.subtract(adjQty).intValue());
		}
		return issueQty;
	}
	
	private static String removeDrugOrderTlfTag(String drugOrderTlf) {
		
		return drugOrderTlf.replace("<br/>",NEW_LINE)
							.replace("</div>",NEW_LINE)
							.replace("&nbsp;"," ")
							.replace("&ndash;","-")
							.replaceAll("<[^>]*>","")
							.replace("&lt;","<")
							.replace("&gt;",">")
							.replace("&apos;","'")
							.replace("&quot","\"")
							.replace("&amp;","&");
		
	}
	
	private Map<String, Boolean> getPrivateFlagMap(Delivery delivery){
		Map<String, Boolean> privateFlagMap = new HashMap<String, Boolean>();
		if (delivery.getDeliveryRequest() != null){
			
			List<DeliveryRequestAlert> deliveryRequestAlertList = delivery.getDeliveryRequest().getDeliveryRequestAlertList();
			
			if ( !deliveryRequestAlertList.isEmpty() ) {
				for (DeliveryRequestAlert deliveryRequestAlert : deliveryRequestAlertList) {
					if ( !privateFlagMap.containsKey(deliveryRequestAlert.getCaseNum()) ) {
						privateFlagMap.put(deliveryRequestAlert.getCaseNum(), deliveryRequestAlert.getPrivateFlag());
					}
				}
			}
		}
		
		return privateFlagMap;
	}

    private static class MpTrxRptDtlComparator implements Comparator<MpTrxRptDtl>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(MpTrxRptDtl mpTrxRptDtl1, MpTrxRptDtl mpTrxRptDtl2) {
			return new CompareToBuilder()
							.append( mpTrxRptDtl1.getBedNum(), mpTrxRptDtl2.getBedNum() )
							.append( mpTrxRptDtl1.getCaseNum(), mpTrxRptDtl2.getCaseNum() )
							.append( mpTrxRptDtl1.getHkid(), mpTrxRptDtl2.getHkid() )
							.toComparison();
		}
    }
    
    private static class MpTrxRptDtlComparatorForPivas implements Comparator<MpTrxRptDtl>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(MpTrxRptDtl mpTrxRptDtl1, MpTrxRptDtl mpTrxRptDtl2) {
			return new CompareToBuilder()
							.append( mpTrxRptDtl1.getBedNum(), mpTrxRptDtl2.getBedNum() )
							.append( mpTrxRptDtl1.getCaseNum(), mpTrxRptDtl2.getCaseNum() )
							.toComparison();
		}
    }

    private static int compareMpTrxItemDtlByItemNum(Integer item1, Integer item2){
    	if( item1 >= 10000 && item2 >= 10000 ){
    		return 0;
    	}else if(item1 < 10000 && item2 < 10000 ){
    		return 0;
    	}
    	return item1.compareTo(item2);
    }
    
    private static class MpTrxRptItemDtlComparator implements Comparator<MpTrxRptItemDtl>, Serializable {

		private static final long serialVersionUID = 1L;
		
		@Override
		public int compare(MpTrxRptItemDtl mpTrxRptItemDtl1, MpTrxRptItemDtl mpTrxRptItemDtl2) {
			int compareResult = 0;
			
			compareResult = compareMpTrxItemDtlByItemNum(mpTrxRptItemDtl1.getMedProfileMoItemOrgItemNum(), mpTrxRptItemDtl2.getMedProfileMoItemOrgItemNum());
			
			if (compareResult == 0) {
				compareResult = getItemCode(mpTrxRptItemDtl1.getMpTrxRptPoItemDtlList()).compareTo(
						getItemCode(mpTrxRptItemDtl2.getMpTrxRptPoItemDtlList()));
			}

			if (compareResult == 0) {
				Long medProfileMoItemId1 = mpTrxRptItemDtl1.getMedProfileMoItemId();
				Long medProfileMoItemId2 = mpTrxRptItemDtl2.getMedProfileMoItemId();
				compareResult = medProfileMoItemId1.compareTo(medProfileMoItemId2) ;
			}
			
			return compareResult;
		}
		
		private String getItemCode(List<MpTrxRptPoItemDtl> mpTrxRptPoItemDtlList){
			StringBuilder sb = new StringBuilder();
			for(MpTrxRptPoItemDtl mpTrxRptPoItemDtl : mpTrxRptPoItemDtlList){
				sb.append(mpTrxRptPoItemDtl.getItemCode());
			}
			return sb.toString();
		}
		
    }

    private static class MpTrxRptPoItemDtlComparatorForMpTrxRptItemDtlSorting implements Comparator<MpTrxRptPoItemDtl>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(MpTrxRptPoItemDtl mpTrxRptPoItemDtl1, MpTrxRptPoItemDtl mpTrxRptPoItemDtl2) {
			String itemCode1 = mpTrxRptPoItemDtl1.getItemCode();
			String itemCode2 = mpTrxRptPoItemDtl2.getItemCode();
			return itemCode1.compareTo(itemCode2);
		}
   }

     private static class MpTrxRptPoItemDtlComparator implements Comparator<MpTrxRptPoItemDtl>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(MpTrxRptPoItemDtl mpTrxRptPoItemDtl1, MpTrxRptPoItemDtl mpTrxRptPoItemDtl2) {
			int compareResult = 0;
			
			
			if (compareResult == 0) {
				Long medProfilePoItemId1 = mpTrxRptPoItemDtl1.getMedProfilePoItemId();
				Long medProfilePoItemId2 = mpTrxRptPoItemDtl2.getMedProfilePoItemId();
				compareResult = medProfilePoItemId1.compareTo(medProfilePoItemId2) ;
			}
			
			return compareResult;
		}
		
		
    }

     private static class MpTrxRptPivasItemDtlComparator implements Comparator<MpTrxRptPivasItemDtl>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(MpTrxRptPivasItemDtl mpTrxRptPivasItemDtl1, MpTrxRptPivasItemDtl mpTrxRptPivasItemDtl2) {
			String pivasDesc1 = mpTrxRptPivasItemDtl1.getPivasDesc();
			String pivasDesc2 = mpTrxRptPivasItemDtl2.getPivasDesc();
			return pivasDesc1.compareTo(pivasDesc2);
		}	
    }
     
	
}
