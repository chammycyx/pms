package hk.org.ha.model.pms.biz.drug;

import java.util.List;
import java.util.Map;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.SiteFormVerbCacherInf;
import hk.org.ha.model.pms.dms.vo.DrugRouteCriteria;
import hk.org.ha.model.pms.dms.vo.FormVerb;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.rx.Site;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("siteFormVerbService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SiteFormVerbServiceBean implements SiteFormVerbServiceLocal {

	@In
	private Workstore workstore;

	@In
	private SiteFormVerbCacherInf siteFormVerbCacher;
	
	@In
	private DrugRouteManagerLocal drugRouteManager;
	
	public FormVerb retrieveDefaultFormVerbByFormCode(String formCode, String itemCode) {

		FormVerb formVerb = siteFormVerbCacher.getDefaultFormVerbByFormCode(formCode);
		
		if (itemCode == null || formVerb == null) {
			return formVerb;
		}
		String siteCode = formVerb.getSiteCode();

		for (Site site : drugRouteManager.retrieveIpSiteListByItemCode(itemCode)) {
			if (StringUtils.equalsIgnoreCase(site.getSiteCode(), siteCode)) {
				return formVerb;
			}
		}
		
		DrugRouteCriteria criteria = new DrugRouteCriteria();
		criteria.setDispHospCode(workstore.getHospCode());
		criteria.setDispWorkstore(workstore.getWorkstoreCode());
		criteria.setItemCode(itemCode);

		for (Map<String, Object> siteMap : drugRouteManager.retrieveIpOthersSiteMapListByDrugRouteCriteria(criteria)) {
			if (siteMap.get("site") == null || !(siteMap.get("site") instanceof Site)) {
				continue;
			}
			Site site = (Site) siteMap.get("site");
			if (StringUtils.equalsIgnoreCase(site.getSiteCode(), siteCode)) {
				return formVerb;
			}
			if (siteMap.get("children") == null || !(siteMap.get("children") instanceof List)) {
				continue;
			}
			for (Map<String,Object> supplSiteMap : (List<Map<String,Object>>) siteMap.get("children")) {
				Site supplSite = (Site) supplSiteMap.get("site");
				if (StringUtils.equalsIgnoreCase(supplSite.getSupplSiteDesc(), siteCode)){
					return formVerb;
				}
			}
		}
		
		return null;
	} 
	
	@Remove
	public void destroy() 
	{		
	}

}
