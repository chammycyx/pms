package hk.org.ha.model.pms.biz.reftable;

import java.util.Date;

import flexjson.JSONSerializer;
import flexjson.transformer.DateTransformer;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.medprofile.MedProfileStatManagerLocal;
import hk.org.ha.model.pms.dms.persistence.PmsSpecMapping;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("pmsSpecMappingService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PmsSpecMappingServiceBean implements PmsSpecMappingServiceLocal {
	
	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";	
	private static final JSONSerializer serializer = new JSONSerializer().transform(new DateTransformer(DATE_FORMAT), Date.class);	
	
	@In
	private AuditLogger auditLogger;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	

	public void updatePmsSpecMapping(PmsSpecMapping updatePmsSpecMapping)
	{
		dmsPmsServiceProxy.updatePmsSpecMapping(updatePmsSpecMapping);
		auditLogger.log( "#0998", serializer.serialize(updatePmsSpecMapping) );
	}
	
	public void deletePmsSpecMapping(PmsSpecMapping delPmsSpecMapping)
	{
		if(delPmsSpecMapping.getRecordId() >0 ){
			dmsPmsServiceProxy.deletePmsSpecMapping(delPmsSpecMapping);
			auditLogger.log( "#0999", serializer.serialize(delPmsSpecMapping) );
		}
	}
	
	@Remove
	public void destroy() 
	{
	}
}
