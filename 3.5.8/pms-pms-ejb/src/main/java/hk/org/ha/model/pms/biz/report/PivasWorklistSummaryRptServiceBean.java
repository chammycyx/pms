package hk.org.ha.model.pms.biz.report;

import static hk.org.ha.model.pms.prop.Prop.PIVAS_WORKLIST_WORKLIST_SORT;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.label.PivasLabelBuilderLocal;
import hk.org.ha.model.pms.biz.pivas.PivasWorklistManagerLocal;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmMoeProperty;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormula;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.pivas.MaterialRequestItem;
import hk.org.ha.model.pms.persistence.pivas.PivasPrep;
import hk.org.ha.model.pms.persistence.pivas.PivasPrepMethod;
import hk.org.ha.model.pms.persistence.pivas.PivasPrepMethodItem;
import hk.org.ha.model.pms.persistence.pivas.PivasWorklist;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.report.PivasItemCat;
import hk.org.ha.model.pms.udt.pivas.PivasWorklistSortColumn;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.PrinterSelect;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.report.PivasWorklistSummaryRpt;
import hk.org.ha.model.pms.vo.report.PivasWorklistSummaryRptRawMaterialDtl;
import hk.org.ha.model.pms.vo.report.PivasWorklistSummaryRptWorklistDtl;
import hk.org.ha.model.pms.vo.rx.Freq;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang3.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("pivasWorklistSummaryRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PivasWorklistSummaryRptServiceBean implements PivasWorklistSummaryRptServiceLocal {
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private PrintAgentInf printAgent;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private Workstore workstore;
	
	@In
	private PivasLabelBuilderLocal pivasLabelBuilder;
	
	@In
	private PivasWorklistManagerLocal pivasWorklistManager;

	
	private static final String NEW_LINE = "\n";
	private static final String VEND_SUPPLY_SOLV = "VEND_SUPPLY_SOLV";
	private Map<String, DmDrug> pivasItemCodeDmDrugMap = new HashMap<String, DmDrug>();
	
	
	public void printPivasWorklistSummaryRpt(List<PivasWorklist> pivasWorklistList) {
		
		if (pivasWorklistList.size() == 0) {
			return;
		}
		
		PivasWorklistSummaryRpt pivasWorklistSummaryRpt = constructPivasWorklistSummaryRpt(pivasWorklistList);
		printPivasWorklistSummaryRpt(pivasWorklistSummaryRpt);
	}
	
	private PivasWorklistSummaryRpt constructPivasWorklistSummaryRpt(List<PivasWorklist> pivasWorklistList) {

		PivasWorklistSummaryRpt pivasWorklistSummaryRpt = new PivasWorklistSummaryRpt();
		pivasWorklistSummaryRpt.setPivasSupplyDate(pivasWorklistList.get(0).getSupplyDate());
		
		List<PivasWorklistSummaryRptWorklistDtl> pivasWorklistSummaryRptWorklistDtlList = constructPivasWorklistSummaryRptWorklistDtl(pivasWorklistList);
		Collections.sort(pivasWorklistSummaryRptWorklistDtlList, new PivasWorklistSummaryRptWorklistDtlComparator(PIVAS_WORKLIST_WORKLIST_SORT.get()));
		pivasWorklistSummaryRpt.setPivasWorklistSummaryRptWorklistDtlList(pivasWorklistSummaryRptWorklistDtlList);
		
		List<PivasWorklistSummaryRptRawMaterialDtl> pivasWorklistSummaryRptRawMaterialDtlList = constructPivasWorklistSummaryRptRawMaterialDtl(pivasWorklistList);
		pivasWorklistSummaryRptRawMaterialDtlList = calPivasPivasWorklistSummaryRptRawMaterialDtlQty(pivasWorklistSummaryRptRawMaterialDtlList);
		Collections.sort(pivasWorklistSummaryRptRawMaterialDtlList, new PivasWorklistSummaryRptRawMaterialDtlComparator());
		pivasWorklistSummaryRpt.setPivasWorklistSummaryRptRawMaterialDtlList(pivasWorklistSummaryRptRawMaterialDtlList);
		
		return pivasWorklistSummaryRpt;
	}
	
	private List<PivasWorklistSummaryRptWorklistDtl> constructPivasWorklistSummaryRptWorklistDtl(List<PivasWorklist> pivasWorklistList){

		List<PivasWorklistSummaryRptWorklistDtl> pivasWorklistSummaryRptWorklistDtlList = new ArrayList<PivasWorklistSummaryRptWorklistDtl>();

		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			
			PivasWorklistSummaryRptWorklistDtl pivasWorklistSummaryRptWorklistDtl = new PivasWorklistSummaryRptWorklistDtl();
			
			MedProfile medProfile = pivasWorklist.getMedProfile();
			Patient patient = medProfile.getPatient();
			MedCase medCase = medProfile.getMedCase();
			PivasPrepMethod pivasPrepMethod = pivasWorklist.getPivasPrepMethod();
			PivasFormula pivasFormula = pivasWorklist.getPivasPrep().getPivasFormula();
			PivasPrepMethodItem pivasPrepMethodItem = pivasPrepMethod.getPivasPrepMethodItemList().get(0);
			PivasPrep pivasPrep = pivasWorklist.getPivasPrep();
			
			pivasWorklistSummaryRptWorklistDtl.setPatName(patient.getName());
			pivasWorklistSummaryRptWorklistDtl.setPatNameChi(patient.getNameChi());
			pivasWorklistSummaryRptWorklistDtl.setCaseNum(medCase.getCaseNum());
			pivasWorklistSummaryRptWorklistDtl.setSpecCode(medProfile.getSpecCode());
			pivasWorklistSummaryRptWorklistDtl.setWard(medProfile.getWardCode());
			pivasWorklistSummaryRptWorklistDtl.setBedNum(medCase.getPasBedNum());
			
			if ( patient.getSex() != null ) {
				pivasWorklistSummaryRptWorklistDtl.setSex(patient.getSex().getDataValue());
			}

			if ( patient.getAge() != null ) {
				pivasWorklistSummaryRptWorklistDtl.setAge(patient.getAge()+patient.getDateUnit().substring(0, 1));
			}

			if (medProfile.getBodyWeight() != null) {
				DecimalFormat df = new DecimalFormat("0.##");
    			String bodyWeight = df.format(medProfile.getBodyWeight().stripTrailingZeros());
    			pivasWorklistSummaryRptWorklistDtl.setBodyWeight(bodyWeight);
			}
			
			pivasWorklistSummaryRptWorklistDtl.setPivasDueDate(pivasWorklist.getDueDate());
			
			pivasWorklistSummaryRptWorklistDtl.setPrepDesc(buildPrepDesc(pivasWorklist, pivasPrep, pivasPrepMethod, pivasFormula));

			pivasWorklistSummaryRptWorklistDtl.setDrugItemCode(pivasPrepMethodItem.getDrugItemCode());
			pivasWorklistSummaryRptWorklistDtl.setDrugManufCode(pivasPrepMethodItem.getDrugManufCode());
			pivasWorklistSummaryRptWorklistDtl.setDrugBatchNum(pivasPrepMethodItem.getDrugBatchNum());
			pivasWorklistSummaryRptWorklistDtl.setDrugExpiryDate(pivasPrepMethodItem.getDrugExpiryDate());
			
			if ( StringUtils.isNoneBlank(pivasPrepMethodItem.getSolventItemCode()) ) {
				// not vendor supply solvent
				pivasWorklistSummaryRptWorklistDtl.setVendSupplySolvFlag(false);
				pivasWorklistSummaryRptWorklistDtl.setVendSupplySolvDesc(null);
				
				pivasWorklistSummaryRptWorklistDtl.setSolventItemCode(pivasPrepMethodItem.getSolventItemCode()); 
				pivasWorklistSummaryRptWorklistDtl.setSolventManufCode(pivasPrepMethodItem.getSolventManufCode());
				pivasWorklistSummaryRptWorklistDtl.setSolventBatchNum(pivasPrepMethodItem.getSolventBatchNum());
				pivasWorklistSummaryRptWorklistDtl.setSolventExpiryDate(pivasPrepMethodItem.getSolventExpiryDate());
			} else if (VEND_SUPPLY_SOLV.equals(pivasPrepMethodItem.getSolventCode())) {
				// vendor supply solvent, no solventItemCode and solventManufCode, solventBatchNum and solventExpiryDate are optional, but are in pairs
				pivasWorklistSummaryRptWorklistDtl.setVendSupplySolvFlag(true);
				pivasWorklistSummaryRptWorklistDtl.setVendSupplySolvDesc(pivasPrepMethodItem.getSolventDesc());
				
				if (pivasPrepMethodItem.getSolventBatchNum() != null) {
					pivasWorklistSummaryRptWorklistDtl.setSolventBatchNum(pivasPrepMethodItem.getSolventBatchNum());
					pivasWorklistSummaryRptWorklistDtl.setSolventExpiryDate(pivasPrepMethodItem.getSolventExpiryDate());
				}
			}
						
			if ( StringUtils.isNoneBlank(pivasPrepMethod.getDiluentItemCode()) ) {
				pivasWorklistSummaryRptWorklistDtl.setDiluentItemCode(pivasPrepMethod.getDiluentItemCode());
				pivasWorklistSummaryRptWorklistDtl.setDiluentManufCode(pivasPrepMethod.getDiluentManufCode());
				pivasWorklistSummaryRptWorklistDtl.setDiluentBatchNum(pivasPrepMethod.getDiluentBatchNum());
				pivasWorklistSummaryRptWorklistDtl.setDiluentExpiryDate(pivasPrepMethod.getDiluentExpiryDate());
			}

			pivasWorklistSummaryRptWorklistDtl.setSupplyDesc(buildSupplyDesc(pivasWorklist, pivasPrep));
			
			// print only when there is PIVAS Request
			if (pivasWorklist.getMaterialRequest() != null) {
				pivasWorklistSummaryRptWorklistDtl.setPivasRequestDesc(buildPivasRequestDesc(pivasWorklist));
			}
			
			// for sorting
			pivasWorklistSummaryRptWorklistDtl.setPivasWorklistId(pivasWorklist.getId());
			
			pivasWorklistSummaryRptWorklistDtlList.add(pivasWorklistSummaryRptWorklistDtl);
		}

		return pivasWorklistSummaryRptWorklistDtlList;
	}
	
	private List<PivasWorklistSummaryRptRawMaterialDtl> constructPivasWorklistSummaryRptRawMaterialDtl(List<PivasWorklist> pivasWorklistList){
		
		Map<String, PivasWorklistSummaryRptRawMaterialDtl> pivasWorklistSummaryRptRawMaterialMap = new HashMap<String, PivasWorklistSummaryRptRawMaterialDtl>();

		for (PivasWorklist pivasWorklist : pivasWorklistList) {

			PivasPrepMethod pivasPrepMethod = pivasWorklist.getPivasPrepMethod();
			PivasPrepMethodItem pivasPrepMethodItem = pivasPrepMethod.getPivasPrepMethodItemList().get(0);
			
			String drugItemCode = pivasPrepMethodItem.getDrugItemCode();
			String drugManufCode = pivasPrepMethodItem.getDrugManufCode();
			String drugBatchNum = pivasPrepMethodItem.getDrugBatchNum();
			Date drugExpiryDate = pivasPrepMethodItem.getDrugExpiryDate();
			String drugModu = pivasWorklistManager.extractOrderDosageUnit(pivasWorklist.getMedProfileMoItem());
			BigDecimal orderDosage = pivasWorklistManager.extractOrderDosage(pivasWorklist.getMedProfileMoItem());
			BigDecimal newDosage = orderDosage.multiply(pivasWorklist.getIssueQty().add(pivasWorklist.getAdjQty()));

			constructPivasWorklistSummaryRptRawMaterialMap(pivasWorklistSummaryRptRawMaterialMap, 
															PivasItemCat.Drug, drugItemCode, drugManufCode, drugBatchNum, drugExpiryDate, drugModu, newDosage);
			
			// All vendor supply solvents are not included in PivasWorklistSummaryRptRawMaterialDtl
			if ( pivasPrepMethodItem.getSolventItemCode() != null && !VEND_SUPPLY_SOLV.equals(pivasPrepMethodItem.getSolventCode()) ) {
				String solventItemCode = pivasPrepMethodItem.getSolventItemCode();
				String solventManufCode = pivasPrepMethodItem.getSolventManufCode();
				String solventBatchNum = pivasPrepMethodItem.getSolventBatchNum();
				Date solventExpiryDate = pivasPrepMethodItem.getSolventExpiryDate();
				String solventModu = "ML";
				
				constructPivasWorklistSummaryRptRawMaterialMap(pivasWorklistSummaryRptRawMaterialMap, 
																PivasItemCat.Solvent, solventItemCode, solventManufCode, solventBatchNum, solventExpiryDate, solventModu, null);
			}
			
			if (pivasPrepMethod.getDiluentItemCode() != null) {
				String diluentItemCode = pivasPrepMethod.getDiluentItemCode();
				String diluentManufCode = pivasPrepMethod.getDiluentManufCode();
				String diluentBatchNum = pivasPrepMethod.getDiluentBatchNum();
				Date diluentExpiryDate = pivasPrepMethod.getDiluentExpiryDate();
				String diluentModu = "ML";
				
				constructPivasWorklistSummaryRptRawMaterialMap(pivasWorklistSummaryRptRawMaterialMap, 
																PivasItemCat.Diluent, diluentItemCode, diluentManufCode, diluentBatchNum, diluentExpiryDate, diluentModu, null);
			}
		}

		List<PivasWorklistSummaryRptRawMaterialDtl> pivasWorklistSummaryRptRawMaterialDtlList = new ArrayList<PivasWorklistSummaryRptRawMaterialDtl>();
		
		for (PivasWorklistSummaryRptRawMaterialDtl pivasWorklistSummaryRptRawMaterialDtl : pivasWorklistSummaryRptRawMaterialMap.values()) {
			pivasWorklistSummaryRptRawMaterialDtlList.add(pivasWorklistSummaryRptRawMaterialDtl);
		}
		
		return pivasWorklistSummaryRptRawMaterialDtlList;
	}

	private void printPivasWorklistSummaryRpt(PivasWorklistSummaryRpt pivasWorklistSummaryRpt){
		
		List<RenderAndPrintJob> renderAndPrintJobList = new ArrayList<RenderAndPrintJob>();
		PrinterSelect printerSelect = printerSelector.retrievePrinterSelect(PrintDocType.Report);
		PrintOption printOption = new PrintOption();
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("_PNG_ZOOM_RATIO", Float.valueOf(1.3f));
		parameters.put("hospCode", workstore.getHospCode());
		parameters.put("workstoreCode", workstore.getWorkstoreCode());
		
		renderAndPrintJobList.add(new RenderAndPrintJob("PivasWorklistSummaryRpt",
															printOption,
															printerSelect,
															parameters,
															Arrays.asList(pivasWorklistSummaryRpt),
															"[PivasWorklistSummaryRpt] pivasSupplyDate:" + pivasWorklistSummaryRpt.getPivasSupplyDate()
															));

		printAgent.renderAndPrint(renderAndPrintJobList);
	}
	
	private Map<String, PivasWorklistSummaryRptRawMaterialDtl> constructPivasWorklistSummaryRptRawMaterialMap(Map<String, PivasWorklistSummaryRptRawMaterialDtl> pivasWorklistSummaryRptRawMaterialMap, 
																												PivasItemCat pivasItemCat, String itemCode, String manufCode, String batchNum, Date expiryDate,
																												String modu, BigDecimal newDosage) {
		
		List<String> keyList = new ArrayList<String>();
		keyList.add(String.valueOf(pivasItemCat.getDataValue()));
		keyList.add(String.valueOf(itemCode));
		keyList.add(String.valueOf(manufCode));
		keyList.add(String.valueOf(batchNum));
		keyList.add(String.valueOf(expiryDate));
		keyList.add(String.valueOf(modu));
		String key = StringUtils.join(keyList.toArray(), "|");
		
		// check duplicated map key
		if (!pivasWorklistSummaryRptRawMaterialMap.containsKey(key)) {
			PivasWorklistSummaryRptRawMaterialDtl pivasWorklistSummaryRptRawMaterialDtl = new PivasWorklistSummaryRptRawMaterialDtl();
			pivasWorklistSummaryRptRawMaterialDtl.setPivasItemCat(pivasItemCat);
			pivasWorklistSummaryRptRawMaterialDtl.setItemCode(itemCode);
			pivasWorklistSummaryRptRawMaterialDtl.setManufCode(manufCode);
			pivasWorklistSummaryRptRawMaterialDtl.setBatchNum(batchNum);
			pivasWorklistSummaryRptRawMaterialDtl.setExpiryDate(expiryDate);
			pivasWorklistSummaryRptRawMaterialDtl.setModu(modu);
			
			if (!pivasItemCodeDmDrugMap.containsKey(itemCode)) {
					DmDrug dmDrug = dmDrugCacher.getDmDrug(itemCode);
					
					//	dmDrug may not be found by itemCode if the item is deleted in DMS
					if (dmDrug != null) {
						pivasItemCodeDmDrugMap.put(itemCode, dmDrug);
						pivasWorklistSummaryRptRawMaterialDtl.setItemDesc(dmDrug.getFullDrugDesc());
					}	
				} else { // dmDrug of this itemCode is retrieved before
					pivasWorklistSummaryRptRawMaterialDtl.setItemDesc(pivasItemCodeDmDrugMap.get(itemCode).getFullDrugDesc());
				}
		
				// 	totalDosage, estimatedQty and roundQty calculation is for drug only
				if (pivasItemCat == PivasItemCat.Drug && newDosage != null) {
					pivasWorklistSummaryRptRawMaterialDtl.setDosage(newDosage);
				} else {
					pivasWorklistSummaryRptRawMaterialDtl.setDosage(null);
				}
			
				pivasWorklistSummaryRptRawMaterialMap.put(key, pivasWorklistSummaryRptRawMaterialDtl);
		} else {
			// 	This key item is processed before, sum up dosage if this is drug only
			PivasWorklistSummaryRptRawMaterialDtl pivasWorklistSummaryRptRawMaterialDtl = pivasWorklistSummaryRptRawMaterialMap.get(key);
			
			if (pivasWorklistSummaryRptRawMaterialDtl.getPivasItemCat() == PivasItemCat.Drug && pivasWorklistSummaryRptRawMaterialDtl.getDosage() != null) {
				BigDecimal newTotalDosage = pivasWorklistSummaryRptRawMaterialDtl.getDosage().add(newDosage);
				pivasWorklistSummaryRptRawMaterialDtl.setDosage(newTotalDosage);
				pivasWorklistSummaryRptRawMaterialMap.put(key, pivasWorklistSummaryRptRawMaterialDtl);
			}
		}
		
		return pivasWorklistSummaryRptRawMaterialMap;
	}
	
	private List<PivasWorklistSummaryRptRawMaterialDtl> calPivasPivasWorklistSummaryRptRawMaterialDtlQty (List<PivasWorklistSummaryRptRawMaterialDtl> PivasWorklistSummaryRptRawMaterialDtlList) {

		for (PivasWorklistSummaryRptRawMaterialDtl pivasWorklistSummaryRptRawMaterialDtl : PivasWorklistSummaryRptRawMaterialDtlList ) {

			//	totalDosage, estimatedQty and roundQty calculation is for drug only
			if (pivasWorklistSummaryRptRawMaterialDtl.getPivasItemCat() == PivasItemCat.Drug) {
				
				DmDrug dmDrug = pivasItemCodeDmDrugMap.get(pivasWorklistSummaryRptRawMaterialDtl.getItemCode());
				
				//	dmDrug may not be found by itemCode if the item is deleted in DMS
				if (dmDrug != null) {
					DmMoeProperty dmMoeProperty = dmDrug.getDmMoeProperty();
					
					if (pivasWorklistSummaryRptRawMaterialDtl.getDosage() != null && dmMoeProperty != null && dmMoeProperty.getDduToMduRatio() != null && dmMoeProperty.getBaseunitToDduRatio() != null) {
						BigDecimal dduToMduRatio = BigDecimal.valueOf(dmMoeProperty.getDduToMduRatio());
						BigDecimal baseunitToDduRatio = BigDecimal.valueOf(dmMoeProperty.getBaseunitToDduRatio());
						BigDecimal totalDosage = pivasWorklistSummaryRptRawMaterialDtl.getDosage().setScale(4, RoundingMode.HALF_UP);
						// rawEstimatedQty is for correct rounding in roundQty
						BigDecimal rawEstimatedQty = pivasWorklistSummaryRptRawMaterialDtl.getDosage().divide( dduToMduRatio.multiply(baseunitToDduRatio), 10, RoundingMode.HALF_UP );
						BigDecimal displayEstimatedQty = rawEstimatedQty.setScale(2, RoundingMode.HALF_UP);
						Integer roundQty = null;
						
						if (rawEstimatedQty.compareTo(BigDecimal.ZERO) == 1 && rawEstimatedQty.compareTo(BigDecimal.ONE) == -1) {
							roundQty = new Integer(1);
						} else {
							roundQty = rawEstimatedQty.setScale(0, RoundingMode.UP).intValue();
						}
						
						if (dmDrug.getBaseUnit() != null) {
							pivasWorklistSummaryRptRawMaterialDtl.setBaseUnit(dmDrug.getBaseUnit());
							pivasWorklistSummaryRptRawMaterialDtl.setTotalDosage(convertToString(totalDosage));
							pivasWorklistSummaryRptRawMaterialDtl.setEstimatedQty(convertToString(displayEstimatedQty));
							pivasWorklistSummaryRptRawMaterialDtl.setRoundQty(convertToString(roundQty));
						}
					}	
				}
			}
		}
		
		return PivasWorklistSummaryRptRawMaterialDtlList;
	}

	private String buildPrepDesc(PivasWorklist pivasWorklist, PivasPrep pivasPrep, PivasPrepMethod pivasPrepMethod, PivasFormula pivasFormula) {

		// first line
		String prepDesc = pivasPrep.getPrintName();
		
		if (pivasPrep.getSplitDosage() != null) {
			prepDesc += " " + convertToString(pivasPrep.getSplitDosage()) + pivasPrep.getPivasDosageUnit();
		}
		
		if ( (!("ML").equalsIgnoreCase(pivasPrep.getPivasDosageUnit()) || StringUtils.isNotBlank(pivasPrepMethod.getDiluentCode())) && (pivasPrep.getSplitVolume() != null) ) {
			prepDesc += " in " + convertToString(pivasPrep.getSplitVolume()) + "ML";
		}
		
		if (StringUtils.isNotBlank(pivasPrepMethod.getDiluentDesc())) {
			prepDesc += " " + pivasPrepMethod.getDiluentDesc();
		}
		
		prepDesc += NEW_LINE;
		
		// second line
		if (!("ML").equalsIgnoreCase(pivasPrep.getPivasDosageUnit())) {
			prepDesc += convertToString(pivasFormula.getConcn()) + pivasPrep.getPivasDosageUnit() + "/ML" + " - ";
		}
		prepDesc += pivasFormula.getDmSite().getIpmoeDesc();
		
		MedProfileMoItem medProfileMoItem = pivasWorklist.getMedProfileMoItem();
		
		Freq freq = pivasWorklistManager.extractOrderDailyFreq(medProfileMoItem);
		if (freq != null && StringUtils.isNotBlank(freq.getDesc())) {
			prepDesc += " - " + StringUtils.trim(freq.getDesc());
		}

		String supplFreqDesc = pivasLabelBuilder.extractOrderSupplFreqDesc(medProfileMoItem);
		if (StringUtils.isNotBlank(supplFreqDesc)) {
			prepDesc += " (" +  StringUtils.trim(supplFreqDesc.toLowerCase()) + ")";
		}
		
		// third line
		if (pivasPrep.getSplitCount() > 1) {
			prepDesc += NEW_LINE;
			prepDesc += "x " + pivasPrep.getSplitCount() + " split";
		}
		
		return prepDesc;
	}
	
	private String buildSupplyDesc(PivasWorklist pivasWorklist, PivasPrep pivasPrep) {
		
		String supplyDesc = "";
		
		// finalVolume
		if (pivasPrep.getSplitVolume() != null) {
			supplyDesc += convertToString(pivasPrep.getSplitVolume()) + "ML";
		}
		
		if (pivasWorklist.getIssueQty() != null) {
			supplyDesc += NEW_LINE;
			// prepQty
			if (pivasWorklist.getAdjQty() != null) {
				supplyDesc += convertToString(pivasWorklist.getIssueQty().add(pivasWorklist.getAdjQty()));
			}
			else {
				supplyDesc += convertToString(pivasWorklist.getIssueQty());
			}

			if (pivasPrep.getPivasContainer() != null) {
				supplyDesc += " " + pivasPrep.getPivasContainer().getContainerName();
			}

			if (pivasPrep.getSplitCount() > 1) {
				supplyDesc += " x " + convertToString(pivasPrep.getSplitCount());
			}
		}
		
		return supplyDesc;
	}
	
	private String buildPivasRequestDesc(PivasWorklist pivasWorklist) {
		
		String pivasRequestDesc = null;
		
		if (pivasWorklist.getMaterialRequest().getMaterialRequestItemList() != null && pivasWorklist.getMaterialRequest().getMaterialRequestItemList().size() > 0) {
			MaterialRequestItem materialRequestItem = pivasWorklist.getMaterialRequest().getMaterialRequestItemList().get(0);

			if (materialRequestItem.getItemCode() != null) {
				pivasRequestDesc = materialRequestItem.getItemCode();
				pivasRequestDesc += NEW_LINE;
				pivasRequestDesc += convertToString(materialRequestItem.getIssueQty());
				
				if (materialRequestItem.getBaseUnit() != null) {
					pivasRequestDesc += " " + materialRequestItem.getBaseUnit();
				}
			}
		}
		
		return pivasRequestDesc;
	}
	
	private <N extends Number> String convertToString(N value) {
		if ( value == null ) {
			return StringUtils.EMPTY;
		}
		String str = null;
		
		if (value instanceof Integer) {
			str = value.toString();
		} else if (value instanceof Long) {
			str = value.toString();
		} else if (value instanceof Double) {
			Double decimalValue =  (Double) value - value.intValue();
			if ( decimalValue == 0 ) {
				str =  Integer.toString(value.intValue());
			} else {
				str = value.toString();
			}
		} else if (value instanceof BigDecimal) {
			Double decimalValue =  value.doubleValue() - value.intValue();
			if ( decimalValue == 0 ) {
				str = Integer.toString(value.intValue());
			} else {
				str = ((BigDecimal) value).stripTrailingZeros().toPlainString();
			}
		}
		
		return str;
	}

	private static class PivasWorklistSummaryRptWorklistDtlComparator implements Comparator<PivasWorklistSummaryRptWorklistDtl>, Serializable {

		private static final long serialVersionUID = 1L;
		
		private Integer defaultPivasWorklistWorklistSortingNum;
		
		public PivasWorklistSummaryRptWorklistDtlComparator(Integer defaultPivasWorklistWorklistSortingNum) {
			this.defaultPivasWorklistWorklistSortingNum = defaultPivasWorklistWorklistSortingNum;
		}
		
		@Override
		public int compare(PivasWorklistSummaryRptWorklistDtl pivasWorklistSummaryRptWorklistDtl1, PivasWorklistSummaryRptWorklistDtl pivasWorklistSummaryRptWorklistDtl2) {
			int compareResult = 0;
			
			if (defaultPivasWorklistWorklistSortingNum != null) {
				switch (PivasWorklistSortColumn.dataValueOf(String.valueOf(defaultPivasWorklistWorklistSortingNum))) {
					case DueDate:
						compareResult = new CompareToBuilder()
						.append( pivasWorklistSummaryRptWorklistDtl1.getPivasDueDate(), pivasWorklistSummaryRptWorklistDtl2.getPivasDueDate() )
						// follow front-end sorting - use pivasWorklistId to guarantee all records are distinct 
						.append( pivasWorklistSummaryRptWorklistDtl1.getPivasWorklistId(), pivasWorklistSummaryRptWorklistDtl2.getPivasWorklistId() )
						.toComparison();
						break;
					case PreparationDtlDueDate:
						compareResult = new CompareToBuilder()
						.append( pivasWorklistSummaryRptWorklistDtl1.getPrepDesc(), pivasWorklistSummaryRptWorklistDtl2.getPrepDesc() )
						.append( pivasWorklistSummaryRptWorklistDtl1.getPivasDueDate(), pivasWorklistSummaryRptWorklistDtl2.getPivasDueDate() )
						// follow front-end sorting - use pivasWorklistId to guarantee all records are distinct 
						.append( pivasWorklistSummaryRptWorklistDtl1.getPivasWorklistId(), pivasWorklistSummaryRptWorklistDtl2.getPivasWorklistId() )
						.toComparison();
						break;
					case PreparationDtlPhsWardBedNumPatName:
						compareResult = new CompareToBuilder()
						.append( pivasWorklistSummaryRptWorklistDtl1.getPrepDesc(), pivasWorklistSummaryRptWorklistDtl2.getPrepDesc() )
						.append( pivasWorklistSummaryRptWorklistDtl1.getWard(), pivasWorklistSummaryRptWorklistDtl2.getWard() )
						.append( pivasWorklistSummaryRptWorklistDtl1.getBedNum(), pivasWorklistSummaryRptWorklistDtl2.getBedNum() )
						.append( pivasWorklistSummaryRptWorklistDtl1.getPatName(), pivasWorklistSummaryRptWorklistDtl2.getPatName() )
						// follow front-end sorting - use pivasWorklistId to guarantee all records are distinct 
						.append( pivasWorklistSummaryRptWorklistDtl1.getPivasWorklistId(), pivasWorklistSummaryRptWorklistDtl2.getPivasWorklistId() )
						.toComparison();
						break;
					case PatNamePreparationDtl:
						compareResult = new CompareToBuilder()
						.append( pivasWorklistSummaryRptWorklistDtl1.getPatName(), pivasWorklistSummaryRptWorklistDtl2.getPatName() )
						.append( pivasWorklistSummaryRptWorklistDtl1.getPrepDesc(), pivasWorklistSummaryRptWorklistDtl2.getPrepDesc() )
						// follow front-end sorting - use pivasWorklistId to guarantee all records are distinct 
						.append( pivasWorklistSummaryRptWorklistDtl1.getPivasWorklistId(), pivasWorklistSummaryRptWorklistDtl2.getPivasWorklistId() )
						.toComparison();
						break;
					case PhsWardBedNumPatNamePreparationDtl:
						compareResult = new CompareToBuilder()
						.append( pivasWorklistSummaryRptWorklistDtl1.getWard(), pivasWorklistSummaryRptWorklistDtl2.getWard() )
						.append( pivasWorklistSummaryRptWorklistDtl1.getBedNum(), pivasWorklistSummaryRptWorklistDtl2.getBedNum() )
						.append( pivasWorklistSummaryRptWorklistDtl1.getPatName(), pivasWorklistSummaryRptWorklistDtl2.getPatName() )
						.append( pivasWorklistSummaryRptWorklistDtl1.getPrepDesc(), pivasWorklistSummaryRptWorklistDtl2.getPrepDesc() )
						// follow front-end sorting - use pivasWorklistId to guarantee all records are distinct 
						.append( pivasWorklistSummaryRptWorklistDtl1.getPivasWorklistId(), pivasWorklistSummaryRptWorklistDtl2.getPivasWorklistId() )
						.toComparison();
						break;
				}
			}
			return compareResult;
		}
   }
	
	private static class PivasWorklistSummaryRptRawMaterialDtlComparator implements Comparator<PivasWorklistSummaryRptRawMaterialDtl>, Serializable {

		private static final long serialVersionUID = 1L;
		
		@Override
		public int compare(PivasWorklistSummaryRptRawMaterialDtl pivasWorklistSummaryRptRawMaterialDtl1, PivasWorklistSummaryRptRawMaterialDtl pivasWorklistSummaryRptRawMaterialDtl2) {
			int compareResult = 0;

			compareResult = new CompareToBuilder()
										.append( pivasWorklistSummaryRptRawMaterialDtl1.getPivasItemCat(), pivasWorklistSummaryRptRawMaterialDtl2.getPivasItemCat() )
										.append( pivasWorklistSummaryRptRawMaterialDtl1.getItemCode(), pivasWorklistSummaryRptRawMaterialDtl2.getItemCode() )
										.append( pivasWorklistSummaryRptRawMaterialDtl1.getModu(), pivasWorklistSummaryRptRawMaterialDtl2.getModu() )
										.append( pivasWorklistSummaryRptRawMaterialDtl1.getManufCode(), pivasWorklistSummaryRptRawMaterialDtl2.getManufCode() )
										.append( pivasWorklistSummaryRptRawMaterialDtl1.getBatchNum(), pivasWorklistSummaryRptRawMaterialDtl2.getBatchNum() )
										.append( pivasWorklistSummaryRptRawMaterialDtl1.getExpiryDate(), pivasWorklistSummaryRptRawMaterialDtl2.getExpiryDate() )
										.toComparison();

			return compareResult;
		}
   }
	
	@Remove
	public void destroy() {
		if (pivasItemCodeDmDrugMap != null) {
			pivasItemCodeDmDrugMap = null;
		}
	}

}
