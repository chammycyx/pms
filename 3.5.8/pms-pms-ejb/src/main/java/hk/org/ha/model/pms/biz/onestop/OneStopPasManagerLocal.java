package hk.org.ha.model.pms.biz.onestop;

import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.Patient;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

@Local
public interface OneStopPasManagerLocal {
	
	Map<String, Object> retrieveHkPmiPatientWithCaseListByHkid(String hkid, Boolean displayAllMoe);
	
	Patient retrieveHkPmiPatientByHkid(String hkid);
	
	Patient retrieveHkpmiPatientByCaseNum(String patHopsCode, String caseNum);
	
	MedCase retriveAppointmentAttendanceAndStatusByAppointmentSequence(String patHospCode, String caseNum, String pasApptSeq);
	
	List<MedCase> retriveAppointmentAttendanceAndStatusWithoutAppointmentSequence(String patHospCode, String caseNum, String pasSpecCode,
	   		 																	  String pasSubSpecCode, String orderDate);
	
	List<MedCase> retrieveOpasAppointmentList(String dispHospCode, String patientKey);
	
	Map<String, String> retrieveHkpmiPatientNameListByCaseNumList(List<String> patHospCodeCaseNumList);
}
