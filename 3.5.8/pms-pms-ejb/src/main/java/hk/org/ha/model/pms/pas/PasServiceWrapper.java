package hk.org.ha.model.pms.pas;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.UnreachableException;
import hk.org.ha.model.pms.asa.exception.pas.PasException;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.vo.ehr.EhrPatient;
import hk.org.ha.service.pms.asa.interfaces.pas.PasServiceJmsRemote;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

@AutoCreate
@Name("pasServiceWrapper")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class PasServiceWrapper {
	
	@Logger
	private Log logger;
	
	@In
	public PasServiceJmsRemote pasServiceProxy;
		
	public Patient retrievePasPatientByHkid(String hkid, String startDatetime, String endDatetime, boolean getEhrPatient) throws PasException, UnreachableException {
		Patient patient = pasServiceProxy.retrievePasPatientByHkid(hkid, startDatetime, endDatetime);
		checkPatient(patient);
		if ( getEhrPatient && patient != null) {
			patient.setEhrPatient(retrieveEhrPatient(StringUtils.EMPTY, patient.getPatKey(), patient.getHkid()));
		}
		return patient;
	}

	public Patient retrievePasPatientByPatKey(String patKey, String startDatetime, String endDatetime) throws PasException, UnreachableException {
		Patient patient =  pasServiceProxy.retrievePasPatientByPatKey(patKey, startDatetime, endDatetime);
		checkPatient(patient);
		return patient;
	}

	public Patient retrievePasPatientByCaseNum(String patHospCode, String caseNum, boolean getEhrPatient) throws PasException, UnreachableException {
		Patient patient =  pasServiceProxy.retrievePasPatientByCaseNum(patHospCode, caseNum);
		checkPatient(patient);
		if ( getEhrPatient && patient != null) {
			patient.setEhrPatient(retrieveEhrPatient(StringUtils.EMPTY, patient.getPatKey(), patient.getHkid()));
		}
		return patient;
	}
	
	public Patient retrievePasPatientFullCaseListByHkid(String hospCode, String hkid, boolean getEhrPatient) throws PasException, UnreachableException {
		Patient patient =  pasServiceProxy.retrievePasPatientFullCaseListByHkid(hospCode, hkid);
		checkPatient(patient);
		if ( getEhrPatient && patient != null) {
			EhrPatient ehrPatient = retrieveEhrPatient(StringUtils.EMPTY, patient.getPatKey(), patient.getHkid());
			patient.setEhrPatient(ehrPatient);
		}
		return patient;
	}
	
	public Map<String, String> retrievePasPatientNameListByCaseNumList(String hospCode, String caseNumListString) throws PasException, UnreachableException {		
		return pasServiceProxy.retrievePasPatientNameListByCaseNumList(hospCode, caseNumListString);
	}
	
	private void checkPatient(Patient patient) throws PasException { 
		if (patient == null) {
			throw new PasException(new Exception("No patient found from PAS service."));
		}
	}
	
	public boolean isPasPatientByHkid(String hkid, String startDatetime, String endDatetime) throws PasException, UnreachableException {
		return pasServiceProxy.isPasPatientByHkid(hkid, startDatetime, endDatetime);
	}
	
	public EhrPatient retrieveEhrPatient(String patHospCode, String patKey, String hkid){
		if ( StringUtils.isBlank(patKey) || StringUtils.isBlank(hkid) ) {
			return null;
		}
		try {
			return pasServiceProxy.retrieveEhrPatientDetail(patHospCode, patKey, hkid);
		} catch (PasException e) {
			logger.debug("No ehrPatient is found");
		} catch (UnreachableException e) {
			logger.debug("Retrieve ehrPatient service is down");
		}
		return null;
	}
}
