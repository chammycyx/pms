package hk.org.ha.model.pms.biz.reftable;

import javax.ejb.Local;

@Local
public interface DosageConversionRptListServiceLocal {

	void genDosageConversionRptList();
	
	void exportDosageConversionRpt();
	
	void printDosageConversionRpt();
	
	void destroy();
}
