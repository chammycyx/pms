package hk.org.ha.model.pms.biz.refill;

import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import java.util.List;

import javax.ejb.Local;

@Local
public interface RefillScheduleListManagerLocal {

	void retrieveRefillScheduleByOrderNum(String orderNum);
			
	void updateStdRefillScheduleList(List<RefillSchedule> refillScheduleListIn);
	
	void retrieveRefillScheduleFromVetting(boolean manualRefillSave);
	
	void retrieveRefillScheduleFromCheckIssue(Long refillScheduleId);
	
	void retrieveRefillScheduleFromOneStop(Long refillScheduleId);	
		
	void updateRefillScheduleListOnly(List<RefillSchedule> refillScheduleListIn, Long currRsId);
	
	void refreshRefillScheduleList(Long refillScheduleId, String refillActionIn);
	
	List<RefillSchedule> retrieveRefillScheduleByPharmOrderId(Long pharmOrderId);
	
	void refreshOrderAndRefillScheduleList(Long refillScheduleId, String refillActionIn);
	
}
