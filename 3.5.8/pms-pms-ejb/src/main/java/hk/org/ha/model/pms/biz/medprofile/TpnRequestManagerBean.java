package hk.org.ha.model.pms.biz.medprofile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.UnreachableException;
import hk.org.ha.model.pms.asa.exception.tpn.TpnException;
import hk.org.ha.model.pms.biz.order.DispOrderConverterLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.TpnRequest;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;
import hk.org.ha.service.pms.asa.interfaces.tpn.TpnServiceJmsRemote;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("tpnRequestManager")
@MeasureCalls
public class TpnRequestManagerBean implements TpnRequestManagerLocal {
	@Logger
	private Log logger;
	
	@In
	public TpnServiceJmsRemote tpnServiceProxy;
	
	@In
	public Workstore workstore;
	
	@In
	public Workstation workstation;
	
	@In
	public CorpPmsServiceJmsRemote corpPmsServiceProxy;
	
	@In
	public DispOrderConverterLocal dispOrderConverter;
	
	@PersistenceContext
	private EntityManager em;
	
	private Long logoutTpn(Long tpnSessionId) {
		try{
			if(tpnSessionId != null){
				if(tpnServiceProxy.logout(tpnSessionId)){
					return tpnSessionId = null;
				}
			}
			return tpnSessionId;
		} catch(UnreachableException e){
			logger.error(e);
			return null;
		} catch(TpnException e){
			logger.error(e);
			return null;
		}
	}
	
	public Long updateTpnRequest(MedProfile medProfile, List<TpnRequest> tpnRequestList, Long tpnSessionId) {

		if (!tpnRequestList.isEmpty()) {

			medProfile.setTpnFlag(true);
			
			List<TpnRequest> newTpnRequestList = new ArrayList<TpnRequest>();
			List<Long> deleteTpnRequestIdList = new ArrayList<Long>();
			
			for (TpnRequest tpnRequest : tpnRequestList) {
	    		if (!tpnRequest.getModifyFlag()){
	    			continue;
	    		}
	    		if (TpnRequest.CDDH_STATUS_NEW_MODIFY.contains(tpnRequest.getCddhStatus())) {
	    			newTpnRequestList.add(tpnRequest);
	    		}
	    		if (TpnRequest.CDDH_STATUS_DELETE_MODIFY.contains(tpnRequest.getCddhStatus())) {
	    			deleteTpnRequestIdList.add(tpnRequest.getId());
	    		}
    			tpnRequest.setMedProfile(medProfile);
    			em.merge(tpnRequest);
	    	}

			if (!deleteTpnRequestIdList.isEmpty()) {
				corpPmsServiceProxy.updateDispOrderItemStatusToDeletedByTpnRequestIdList(deleteTpnRequestIdList, false);
			}
			if (!newTpnRequestList.isEmpty()) {
				corpPmsServiceProxy.saveDispOrderListWithVoidInvoiceListForIp(
						Arrays.asList(dispOrderConverter.convertTpnRequestListToDispOrder(newTpnRequestList, workstore, workstation)),
						false, false, false, null, null);
			}
		}		
		return logoutTpn(tpnSessionId);
	}
}