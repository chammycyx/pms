package hk.org.ha.model.pms.workflow;

import hk.org.ha.model.pms.persistence.disp.DispOrder;

import javax.ejb.Local;

@Local
public interface DispensingPmsModeLocal {

	void startDispensingFlow(Long dispOrderId);
	
	void updateToPickedStatus(DispOrder dispOrder);
	
	void updateToAssembleStatus(DispOrder dispOrder);
	
	void updateToCheckedStatus(DispOrder dispOrder);
	
	void updateToIssuedStatus(DispOrder dispOrder);
	
	void unvetOrder();
}
