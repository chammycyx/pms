package hk.org.ha.model.pms.cache;
import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.dms.vo.DrugRouteCriteria;
import hk.org.ha.model.pms.dms.vo.DrugSiteMapping;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.Site;

import java.util.List;

import javax.ejb.Local;

@Local
public interface DrugSiteMappingCacherInf extends BaseCacherInf {
	List<Site> getDischargeOrderSiteList(DrugRouteCriteria criteria);
	List<Site> getIpSiteList(DrugRouteCriteria criteria);
	List<DrugSiteMapping> getDrugSiteMappingListByDrugRouteCriteria(DrugRouteCriteria criteria);
	List<Site> getManualEntrySiteList(DrugRouteCriteria criteria);
}
