package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.model.pms.dms.persistence.DmRegimen;
import hk.org.ha.model.pms.dms.vo.Capd;
import hk.org.ha.model.pms.dms.vo.CommonOrder;
import hk.org.ha.model.pms.dms.vo.DrugName;
import hk.org.ha.model.pms.dms.vo.PivasFormula;
import hk.org.ha.model.pms.dms.vo.PreparationProperty;
import hk.org.ha.model.pms.dms.vo.RouteForm;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
import hk.org.ha.model.pms.vo.drug.DrugSearchReturn;
import hk.org.ha.model.pms.vo.rx.RxDrug;

import java.util.List;

import javax.ejb.Local;

@Local
public interface DrugSearchMpManagerLocal {
	
	DrugSearchReturn createMpFreeTextEntryDrug(DrugSearchSource drugSearchSource, RxDrug rxDrug, MedProfile medProfile, List<Integer> atdpsPickStationsNumList);
	
	DrugSearchReturn createMpCapdDrug(DrugSearchSource drugSearchSource, String supplierSystem, String calciumStrength, Capd capd, MedProfile medProfile, List<Integer> atdpsPickStationsNumList);
	
	DrugSearchReturn createSelectedMpDrugByVetting(DrugSearchSource drugSearchSource, DrugName drugName, RouteForm routeForm, PreparationProperty preparation, CommonOrder commonOrder, PivasFormula pivasFormula, DmRegimen dmRegimen, String pasSpecialty, String pasSubSpecialty, String itemCode, MedProfile medProfile, List<Integer> atdpsPickStationsNumList);
	
	void addMedProfileMoItemFm(MedProfile medProfile, MedProfileMoItem medProfileMoItem, List<MedProfileMoItem> shareMedProfileMoItemList);
	
	MedProfileItem createMedProfileItemByItemCode(String itemCode, MedProfile medProfile, List<Integer> atdpsPickStationsNumList, boolean includeFm);
	
	MedProfileMoItem createMedProfileMoItemByItemCode(String itemCode, MedProfile medProfile, List<Integer> atdpsPickStationsNumList, boolean includeFm);
	
	void destroy();	
}
