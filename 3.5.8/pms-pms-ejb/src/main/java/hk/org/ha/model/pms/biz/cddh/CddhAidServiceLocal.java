package hk.org.ha.model.pms.biz.cddh;

import hk.org.ha.model.pms.vo.cddh.CddhDispOrderItem;
import hk.org.ha.model.pms.vo.report.CddhPatDrugProfileRpt;

import java.util.List;

import javax.ejb.Local;

@Local
public interface CddhAidServiceLocal {
	void retrieveCddhDispLabel(List<String> dispOrderItemKey);
	void updateDispOrderItemRemark(CddhDispOrderItem cddhDispOrderItem);
	void deleteDispOrderItemRemark(CddhDispOrderItem cddhDispOrderItem);
	void checkCddhRemarkStatus(Long dispOrderId, String displayHospCode);
	void printCddhPatDrugProfileRpt(CddhPatDrugProfileRpt cddhPatDrugProfileRpt);
	String getErrMsg();
	void destroy();
}
