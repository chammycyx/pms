package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum LedOperationLogStatus implements StringValuedEnum {

	Send("S", "Send"),
	Withdraw("W", "Withdraw"); 
	
    private final String dataValue;
    private final String displayValue;

    LedOperationLogStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static LedOperationLogStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(LedOperationLogStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<LedOperationLogStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<LedOperationLogStatus> getEnumClass() {
    		return LedOperationLogStatus.class;
    	}
    }
}
