package hk.org.ha.model.pms.biz.vetting;

import java.util.List;

import javax.ejb.Local;

@Local
public interface UnvetManagerLocal {

	List<Long> unvetMedOrder(Long medOrderId, boolean removeOrderByCms, boolean removeOrderByPms);
	
	void destroy();
}
