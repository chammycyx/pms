package hk.org.ha.model.pms.vo.alert;

import java.io.Serializable;

import hk.org.ha.model.pms.udt.alert.Certainty;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class CertaintyAdapter extends XmlAdapter<String, Certainty> implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public String marshal(Certainty v) { 
		return v.getDataValue();
	}

	public Certainty unmarshal(String v) {
		return Certainty.dataValueOf(v);
	}

}
