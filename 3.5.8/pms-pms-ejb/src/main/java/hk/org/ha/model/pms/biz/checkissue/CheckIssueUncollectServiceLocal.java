package hk.org.ha.model.pms.biz.checkissue;

import hk.org.ha.model.pms.vo.checkissue.CheckIssueUncollectSummary;

import java.util.Date;

import javax.ejb.Local;

@Local
public interface CheckIssueUncollectServiceLocal {
	
	CheckIssueUncollectSummary uncollectPrescription(Date ticketDate, String ticketNum, Integer issueWindowNum);
	
	void destroy();
	
}