package hk.org.ha.model.pms.biz.reftable.mp;

import flexjson.JSONSerializer;
import flexjson.transformer.DateTransformer;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.PmsIpDurationConv;
import hk.org.ha.model.pms.dms.persistence.PmsIpQtyConv;
import hk.org.ha.model.pms.dms.persistence.PmsPcuMapping;
import hk.org.ha.model.pms.dms.persistence.PmsQtyConversionAdj;
import hk.org.ha.model.pms.dms.persistence.PmsQtyConversionInsu;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.reftable.DurationUnit;
import hk.org.ha.model.pms.vo.drug.DmDrugLite;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.model.pms.vo.reftable.mp.MpPmsDurationConversionItem;
import hk.org.ha.model.pms.vo.reftable.mp.MpPmsQtyConversionInsuItem;
import hk.org.ha.model.pms.vo.reftable.mp.MpPmsQtyConversionItem;
import hk.org.ha.model.pms.vo.reftable.mp.MpQtyDurationConversionInfo;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.OptimisticLockException;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("mpQtyDurationConversionService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MpQtyDurationConversionServiceBean implements MpQtyDurationConversionServiceLocal {
	
	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";	
	private static final JSONSerializer serializer = new JSONSerializer().transform(new DateTransformer(DATE_FORMAT), Date.class);	
	
	@In
	private AuditLogger auditLogger;
	
	@In
	private Workstore workstore;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;
	
	@In
	private List<PmsPcuMapping> pmsPcuMappingList;
	
	private List<PmsIpQtyConv> qtyConversionList;

	private List<MpPmsQtyConversionItem> qtyConversionOtherWorkstoreList;
	
	private List<PmsIpDurationConv> durationConversionList;
	
	private List<MpPmsDurationConversionItem> durationConversionOtherWorkstoreList;

	private List<PmsPcuMapping> pmsPcuMappingAllWorkstoreList;
	
	private List<MpPmsQtyConversionInsuItem> pmsQtyConversionInsuOtherWorkstoreList;
	
	private PmsQtyConversionInsu pmsQtyConversionInsu;
	
	private PmsQtyConversionAdj pmsQtyConversionAdj;
	
	private DmDrugLite qtyDurationConversionDmDrugLite;
	
	private Map<String, PmsPcuMapping> workstoreGroupMap;
	
	private boolean retrieveCapdItem;
	
	private PmsPcuMapping selectedPmsPcuMapping;
	
	private boolean saveSuccess = true;
	
	public MpQtyDurationConversionInfo retrieveMpQtyDurationConversionList(String itemCode, PmsPcuMapping pmsPcuMapping){
		clearList();
		WorkstoreDrug workstoreDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, itemCode);
		retrieveCapdItem = false;
		
		MpQtyDurationConversionInfo mpQtyDurationConversionInfo = new MpQtyDurationConversionInfo();
		if ( workstoreDrug != null && workstoreDrug.getMsWorkstoreDrug() !=null && workstoreDrug.getDmDrug() != null ) {
			qtyDurationConversionDmDrugLite = workstoreDrug.getDmDrugLite();
			if (!StringUtils.equals(workstoreDrug.getDmDrug().getTheraGroup(), "PDF")) {
				selectedPmsPcuMapping = pmsPcuMapping;
				retrievePmsPcuMappingAllWorkstoreList(pmsPcuMapping.getCompId().getDispHospCode());
				retrievePmsQtyConversionList(itemCode, pmsPcuMapping);
				retrievePmsDurationConversionList(itemCode, pmsPcuMapping);
				retrievePmsQtyConversionInsuList(itemCode, pmsPcuMapping);
				retrievePmsQtyConversionAdj(itemCode, pmsPcuMapping);
				mpQtyDurationConversionInfo.setDurationConversionList(durationConversionList);
				mpQtyDurationConversionInfo.setDurationConversionOtherWorkstoreList(durationConversionOtherWorkstoreList);
				mpQtyDurationConversionInfo.setPmsQtyConversionAdj(pmsQtyConversionAdj);
				mpQtyDurationConversionInfo.setPmsQtyConversionInsu(pmsQtyConversionInsu);
				mpQtyDurationConversionInfo.setPmsQtyConversionInsuOtherWorkstoreList(pmsQtyConversionInsuOtherWorkstoreList);
				mpQtyDurationConversionInfo.setQtyConversionList(qtyConversionList);
				mpQtyDurationConversionInfo.setQtyConversionOtherWorkstoreList(qtyConversionOtherWorkstoreList);
				mpQtyDurationConversionInfo.setQtyDurationConversionDmDrugLite(qtyDurationConversionDmDrugLite);
				return mpQtyDurationConversionInfo;
			} else {
				retrieveCapdItem = true;
				return new MpQtyDurationConversionInfo();
			}
		}
		return new MpQtyDurationConversionInfo();
	}
	
	private void retrievePmsPcuMappingAllWorkstoreList(String hospCode){
		pmsPcuMappingAllWorkstoreList = dmsPmsServiceProxy.retrievePmsPcuMappingListByDispHospCode(hospCode);
		
		Map<String, PmsPcuMapping> pmsPcuMappingMap = new HashMap<String, PmsPcuMapping>();

		for ( PmsPcuMapping pmsPcuMapping : pmsPcuMappingAllWorkstoreList ) {
			pmsPcuMappingMap.put(pmsPcuMapping.getCompId().getDispHospCode()+pmsPcuMapping.getCompId().getDispWorkstore(), pmsPcuMapping);
		}

		workstoreGroupMap = new HashMap<String, PmsPcuMapping>();

		for (PmsPcuMapping pmsPcuMapping : pmsPcuMappingList) {
			if ( pmsPcuMappingMap.get(pmsPcuMapping.getCompId().getDispHospCode()+pmsPcuMapping.getCompId().getDispWorkstore()) == null ) {
				pmsPcuMappingAllWorkstoreList.add(pmsPcuMapping);
			}
			workstoreGroupMap.put(pmsPcuMapping.getCompId().getDispHospCode()+pmsPcuMapping.getCompId().getDispWorkstore(), pmsPcuMapping);
		}
	}
	
	private void retrievePmsQtyConversionList(String itemCode, PmsPcuMapping inPmsPcuMapping){		
		List<PmsIpQtyConv> pmsQtyConversionList = dmsPmsServiceProxy.retrievePmsIpQtyConvList(
				inPmsPcuMapping.getCompId().getDispHospCode(), inPmsPcuMapping.getCompId().getDispWorkstore(), itemCode);
		
		qtyConversionList = new ArrayList<PmsIpQtyConv>();
		qtyConversionOtherWorkstoreList = new ArrayList<MpPmsQtyConversionItem>();
		
		boolean findPmsQtyConversion = false;
		for (PmsPcuMapping pmsPcuMapping : pmsPcuMappingAllWorkstoreList ) {

			findPmsQtyConversion = false;
			for (PmsIpQtyConv pmsIpQtyConv : pmsQtyConversionList) {
				if ( !pmsPcuMapping.getCompId().getDispHospCode().equals(pmsIpQtyConv.getDispHospCode()) ||
						!pmsPcuMapping.getCompId().getDispWorkstore().equals(pmsIpQtyConv.getDispWorkstore())) {
					continue;
				}
				findPmsQtyConversion = true;
				if ( pmsIpQtyConv.getDispHospCode().equals(inPmsPcuMapping.getCompId().getDispHospCode())  
						&& pmsIpQtyConv.getDispWorkstore().equals(inPmsPcuMapping.getCompId().getDispWorkstore()) ) {
					
					qtyConversionList.add(pmsIpQtyConv);
				} else {
					
					boolean find = false;
					for ( MpPmsQtyConversionItem mpPmsQtyConversionItem : qtyConversionOtherWorkstoreList ) {
						if (StringUtils.equals(mpPmsQtyConversionItem.getHospCode(), pmsIpQtyConv.getDispHospCode()) && 
								StringUtils.equals(mpPmsQtyConversionItem.getWorkstoreCode(), pmsIpQtyConv.getDispWorkstore())) { 
							mpPmsQtyConversionItem.getPmsIpQtyConvList().add(pmsIpQtyConv);
							find = true;
						}
					}
					if (!find) {
						MpPmsQtyConversionItem mpPmsQtyConversionItem = createMpPmsQtyConversionItem(pmsIpQtyConv.getDispHospCode(), 
																								pmsIpQtyConv.getDispWorkstore());
						mpPmsQtyConversionItem.getPmsIpQtyConvList().add(pmsIpQtyConv);
						qtyConversionOtherWorkstoreList.add(mpPmsQtyConversionItem);
					}
				}
			}
			if ( !findPmsQtyConversion && 
					(!pmsPcuMapping.getCompId().getDispHospCode().equals(inPmsPcuMapping.getCompId().getDispHospCode()) || 
							!pmsPcuMapping.getCompId().getDispWorkstore().equals(inPmsPcuMapping.getCompId().getDispWorkstore()))) {
				MpPmsQtyConversionItem mpPmsQtyConversionItem = createMpPmsQtyConversionItem(pmsPcuMapping.getCompId().getDispHospCode(), 
																						pmsPcuMapping.getCompId().getDispWorkstore());
				
				qtyConversionOtherWorkstoreList.add(mpPmsQtyConversionItem);
			}
		}
	}
	
	private MpPmsQtyConversionItem createMpPmsQtyConversionItem(String hospCode, String workstoreCode){
		MpPmsQtyConversionItem mpPmsQtyConversionItem = new MpPmsQtyConversionItem();
		mpPmsQtyConversionItem.setHospCode(hospCode);
		mpPmsQtyConversionItem.setWorkstoreCode(workstoreCode);
		mpPmsQtyConversionItem.setSameWorkstoreGroup(sameWorkstoreGroup(hospCode, workstoreCode));

		return mpPmsQtyConversionItem;
	}
	
	private MpPmsDurationConversionItem createMpPmsDurationConversionItem(String hospCode, String workstoreCode){
		MpPmsDurationConversionItem mpPmsDurationConversionItem = new MpPmsDurationConversionItem();
		mpPmsDurationConversionItem.setHospCode(hospCode);
		mpPmsDurationConversionItem.setWorkstoreCode(workstoreCode);
		mpPmsDurationConversionItem.setSameWorkstoreGroup(sameWorkstoreGroup(hospCode, workstoreCode));

		return mpPmsDurationConversionItem;
	}
	
	private MpPmsQtyConversionInsuItem createPmsQtyConversionInsuItem(String hospCode, String workstoreCode){
		MpPmsQtyConversionInsuItem pmsQtyConversionInsuItem = new MpPmsQtyConversionInsuItem();
		pmsQtyConversionInsuItem.setHospCode(hospCode);
		pmsQtyConversionInsuItem.setWorkstoreCode(workstoreCode);
		pmsQtyConversionInsuItem.setSameWorkstoreGroup(sameWorkstoreGroup(hospCode, workstoreCode));

		return pmsQtyConversionInsuItem;
	}
	
	private void retrievePmsDurationConversionList(String itemCode, PmsPcuMapping inPmsPcuMapping){
		List<PmsIpDurationConv> pmsDurationConversionList = dmsPmsServiceProxy.retrievePmsIpDurationConvList(
				inPmsPcuMapping.getCompId().getDispHospCode(), inPmsPcuMapping.getCompId().getDispWorkstore(), itemCode);
		
		durationConversionList = new ArrayList<PmsIpDurationConv>();
		durationConversionOtherWorkstoreList = new ArrayList<MpPmsDurationConversionItem>();
		
		boolean findPmsDurationConversion = false;
		for (PmsPcuMapping pmsPcuMapping : pmsPcuMappingAllWorkstoreList ) {

			findPmsDurationConversion = false;
			
			for (PmsIpDurationConv pmsIpDurationConv : pmsDurationConversionList) {
				if ( !pmsPcuMapping.getCompId().getDispHospCode().equals(pmsIpDurationConv.getDispHospCode()) ||
						!pmsPcuMapping.getCompId().getDispWorkstore().equals(pmsIpDurationConv.getDispWorkstore())) {
					continue;
				}
				findPmsDurationConversion = true;
				pmsIpDurationConv.setDurationUnitDesc(DurationUnit.dataValueOf(pmsIpDurationConv.getDurationUnit()).getDisplayValue());
				if ( pmsIpDurationConv.getDispHospCode().equals(inPmsPcuMapping.getCompId().getDispHospCode())  
						&& pmsIpDurationConv.getDispWorkstore().equals(inPmsPcuMapping.getCompId().getDispWorkstore()) ) {
					durationConversionList.add(pmsIpDurationConv);
				} else {
					boolean find = false;
					for ( MpPmsDurationConversionItem mpPmsDurationConversionItem : durationConversionOtherWorkstoreList ) {
						if (StringUtils.equals(mpPmsDurationConversionItem.getHospCode(), pmsIpDurationConv.getDispHospCode()) && 
								StringUtils.equals(mpPmsDurationConversionItem.getWorkstoreCode(), pmsIpDurationConv.getDispWorkstore())) {
							mpPmsDurationConversionItem.getPmsIpDurationConvList().add(pmsIpDurationConv);
							find = true;
						}
					}
					if (!find) {
						MpPmsDurationConversionItem mpPmsDurationConversionItem = createMpPmsDurationConversionItem(pmsIpDurationConv.getDispHospCode(), 
																												pmsIpDurationConv.getDispWorkstore());
						mpPmsDurationConversionItem.getPmsIpDurationConvList().add(pmsIpDurationConv);
						durationConversionOtherWorkstoreList.add(mpPmsDurationConversionItem);
					}
				}
			} 
			if ( !findPmsDurationConversion && 
					(!pmsPcuMapping.getCompId().getDispHospCode().equals(inPmsPcuMapping.getCompId().getDispHospCode()) || 
							!pmsPcuMapping.getCompId().getDispWorkstore().equals(inPmsPcuMapping.getCompId().getDispWorkstore()))) {
				MpPmsDurationConversionItem mpPmsDurationConversionItem = createMpPmsDurationConversionItem(pmsPcuMapping.getCompId().getDispHospCode(), 
																						pmsPcuMapping.getCompId().getDispWorkstore());
				
				durationConversionOtherWorkstoreList.add(mpPmsDurationConversionItem);
			}
		}	
	}
	
	private void retrievePmsQtyConversionInsuList(String itemCode, PmsPcuMapping inPmsPcuMapping){
		List<PmsQtyConversionInsu> pmsQtyConversionInsuList = dmsPmsServiceProxy.retrievePmsQtyConversionInsuList(
				inPmsPcuMapping.getCompId().getDispHospCode(), inPmsPcuMapping.getCompId().getDispWorkstore(), itemCode);
		
		pmsQtyConversionInsu = new PmsQtyConversionInsu();
		pmsQtyConversionInsu.setDispHospCode(inPmsPcuMapping.getCompId().getDispHospCode());
		pmsQtyConversionInsu.setDispWorkstore(inPmsPcuMapping.getCompId().getDispWorkstore());
		pmsQtyConversionInsu.setItemCode(itemCode);
		
		pmsQtyConversionInsuOtherWorkstoreList = new ArrayList<MpPmsQtyConversionInsuItem>();
		
		boolean findItem = false;
		for (PmsPcuMapping pmsPcuMapping : pmsPcuMappingAllWorkstoreList ) {
			findItem = false;
			for (PmsQtyConversionInsu qtyConversionInsu : pmsQtyConversionInsuList) {
				if ( !pmsPcuMapping.getCompId().getDispHospCode().equals(qtyConversionInsu.getDispHospCode()) ||
						!pmsPcuMapping.getCompId().getDispWorkstore().equals(qtyConversionInsu.getDispWorkstore())) {
					continue;
				}
				findItem = true;
				if ( qtyConversionInsu.getDispHospCode().equals(inPmsPcuMapping.getCompId().getDispHospCode())  
						&& qtyConversionInsu.getDispWorkstore().equals(inPmsPcuMapping.getCompId().getDispWorkstore()) ) {
					pmsQtyConversionInsu = qtyConversionInsu;
				} else {
					MpPmsQtyConversionInsuItem pmsQtyConversionInsuItem = createPmsQtyConversionInsuItem(pmsPcuMapping.getCompId().getDispHospCode(), 
																										pmsPcuMapping.getCompId().getDispWorkstore());
					pmsQtyConversionInsuItem.getPmsQtyConversionInsuList().add(qtyConversionInsu);
					pmsQtyConversionInsuOtherWorkstoreList.add(pmsQtyConversionInsuItem);
				}
			}
			
			if ( !findItem && 
					(!pmsPcuMapping.getCompId().getDispHospCode().equals(inPmsPcuMapping.getCompId().getDispHospCode()) || 
							!pmsPcuMapping.getCompId().getDispWorkstore().equals(inPmsPcuMapping.getCompId().getDispWorkstore()))) {
				MpPmsQtyConversionInsuItem pmsQtyConversionInsuItem = createPmsQtyConversionInsuItem(pmsPcuMapping.getCompId().getDispHospCode(), 
																									pmsPcuMapping.getCompId().getDispWorkstore());
				pmsQtyConversionInsuOtherWorkstoreList.add(pmsQtyConversionInsuItem);
			}
		}
	}
	
	private void retrievePmsQtyConversionAdj(String itemCode, PmsPcuMapping inPmsPcuMapping){
		pmsQtyConversionAdj = dmsPmsServiceProxy.retrievePmsQtyConversionAdj(inPmsPcuMapping.getCompId().getDispHospCode(), 
																				inPmsPcuMapping.getCompId().getDispWorkstore(), 
																				itemCode);
		if ( pmsQtyConversionAdj == null ) {
			pmsQtyConversionAdj = new PmsQtyConversionAdj();
			pmsQtyConversionAdj.setDispHospCode(inPmsPcuMapping.getCompId().getDispHospCode());
			pmsQtyConversionAdj.setDispWorkstore(inPmsPcuMapping.getCompId().getDispWorkstore());
			pmsQtyConversionAdj.setItemCode(itemCode);
		}
	}
	
	private boolean sameWorkstoreGroup(String dispHospCode, String dispWorkstore){
		return ( workstoreGroupMap.get(dispHospCode+dispWorkstore) != null );
	}
	
	public String updateMpQtyDurationConversionList(List<PmsIpQtyConv> inPmsQtyConversionList, boolean qtyApplyAll, 
													List<PmsIpDurationConv> inPmsDurationConversionList, boolean durationApplyAll,
													PmsQtyConversionInsu inPmsQtyConversionInsu, boolean insuApplyAll,
													PmsQtyConversionAdj updatePmsQtyConversionAdj){
		
		List<PmsIpQtyConv> updatePmsQtyConversionList = updatePmsQtyConversion(inPmsQtyConversionList, qtyApplyAll);
		List<PmsIpDurationConv> updatePmsDurationConversionList = updatePmsDurationConversion(inPmsDurationConversionList, durationApplyAll);
		List<PmsQtyConversionInsu> updatePmsQtyConversionInsuList = updatePmsQtyConversionInsu(inPmsQtyConversionInsu, insuApplyAll);
		
		String errorCode = StringUtils.EMPTY;
		try{
			dmsPmsServiceProxy.updatePmsIpQtyDurationConv(updatePmsQtyConversionList, updatePmsDurationConversionList, updatePmsQtyConversionInsuList, updatePmsQtyConversionAdj);
			saveSuccess = true;
			errorCode = "0446";
		}catch(Exception e){
			saveSuccess = false;
			errorCode = "0729";
		}
		auditLogger.log( "#0993", serializer.serialize(updatePmsQtyConversionAdj) );
		return errorCode;
	}
	
	private List<PmsIpQtyConv> updatePmsQtyConversion(List<PmsIpQtyConv> inPmsQtyConversionList, boolean applyAllWorkstore){
		List<PmsIpQtyConv> updatePmsQtyConversionList = new ArrayList<PmsIpQtyConv>(inPmsQtyConversionList);
		Map<String, PmsPcuMapping> pmsPcuMappingMap = new HashMap<String, PmsPcuMapping>();
		
		//same workstore group
		for ( PmsPcuMapping pmsPcuMapping : pmsPcuMappingList ) {
			pmsPcuMappingMap.put(pmsPcuMapping.getCompId().getDispHospCode() + pmsPcuMapping.getCompId().getDispWorkstore(), pmsPcuMapping);
			if ( pmsPcuMapping.getCompId().getDispHospCode().equals(selectedPmsPcuMapping.getCompId().getDispHospCode()) && 
					pmsPcuMapping.getCompId().getDispWorkstore().equals(selectedPmsPcuMapping.getCompId().getDispWorkstore()) ) {
				continue;
			}
			for ( PmsIpQtyConv pmsIpQtyConv : inPmsQtyConversionList ) {
				if ( !pmsIpQtyConv.isMarkDelete() ) {
					updatePmsQtyConversionList.add(createPmsQtyConversion(pmsIpQtyConv, pmsPcuMapping));
				}
			}
		}
		
		for ( MpPmsQtyConversionItem mpPmsQtyConversionItem : qtyConversionOtherWorkstoreList ) {
			if ( mpPmsQtyConversionItem.isSameWorkstoreGroup() ) {
				for ( PmsIpQtyConv pmsIpQtyConv : mpPmsQtyConversionItem.getPmsIpQtyConvList() ) {
					pmsIpQtyConv.setMarkDelete(true);
					updatePmsQtyConversionList.add(pmsIpQtyConv);
				}
			}
		}
		
		if ( applyAllWorkstore ) { 
			//other workstore group
			for ( PmsPcuMapping pmsPcuMapping : pmsPcuMappingAllWorkstoreList ) {
				if ( pmsPcuMappingMap.containsKey(pmsPcuMapping.getCompId().getDispHospCode() + pmsPcuMapping.getCompId().getDispWorkstore() ) ) {
					continue;
				}
				
				List<PmsPcuMapping> otherGroupPcuMappingList = dmsPmsServiceProxy.retrievePmsPcuMappingList(pmsPcuMapping.getCompId().getDispHospCode(),  
																										pmsPcuMapping.getCompId().getDispWorkstore());
				
				List<PmsIpQtyConv> pmsQtyConversionList = dmsPmsServiceProxy.retrievePmsIpQtyConvList(pmsPcuMapping.getCompId().getDispHospCode(), 
																												pmsPcuMapping.getCompId().getDispWorkstore(), 
																												qtyDurationConversionDmDrugLite.getItemCode());
				
				Map<String, PmsPcuMapping> otherGroupPcuMappingMap = new HashMap<String, PmsPcuMapping>();
				
				for ( PmsPcuMapping otherGroupPcuMapping : otherGroupPcuMappingList ) {
					otherGroupPcuMappingMap.put(otherGroupPcuMapping.getCompId().getDispHospCode() + otherGroupPcuMapping.getCompId().getDispWorkstore(), otherGroupPcuMapping);
					if ( pmsPcuMappingMap.containsKey(otherGroupPcuMapping.getCompId().getDispHospCode() + otherGroupPcuMapping.getCompId().getDispWorkstore() ) ) {
						continue;
					}
					pmsPcuMappingMap.put(otherGroupPcuMapping.getCompId().getDispHospCode() + otherGroupPcuMapping.getCompId().getDispWorkstore(), otherGroupPcuMapping);
					for ( PmsIpQtyConv pmsIpQtyConv : inPmsQtyConversionList ) {
						if ( !pmsIpQtyConv.isMarkDelete() ) {
							updatePmsQtyConversionList.add(createPmsQtyConversion(pmsIpQtyConv, otherGroupPcuMapping));
						}
					}
				}
				
				if ( !pmsQtyConversionList.isEmpty() ) {
					for ( PmsIpQtyConv pmsIpQtyConv : pmsQtyConversionList ) {
						if ( !otherGroupPcuMappingMap.containsKey(pmsIpQtyConv.getDispHospCode() + pmsIpQtyConv.getDispWorkstore() ) ) {
							continue;
						}
						pmsIpQtyConv.setMarkDelete(true);
						updatePmsQtyConversionList.add(pmsIpQtyConv);
					}
				}
			}
		}

		auditLogger.log( "#0994", serializer.serialize(updatePmsQtyConversionList) );
		return updatePmsQtyConversionList;
	}
	
	private List<PmsIpDurationConv> updatePmsDurationConversion(List<PmsIpDurationConv> inPmsDurationConversionList, boolean applyAllWorkstore){
		List<PmsIpDurationConv> updatePmsDurationConversionList = new ArrayList<PmsIpDurationConv>(inPmsDurationConversionList);
		Map<String, PmsPcuMapping> pmsPcuMappingMap = new HashMap<String, PmsPcuMapping>();
		
		//same workstore group
		for ( PmsPcuMapping pmsPcuMapping : pmsPcuMappingList ) {
			pmsPcuMappingMap.put(pmsPcuMapping.getCompId().getDispHospCode() + pmsPcuMapping.getCompId().getDispWorkstore(), pmsPcuMapping);
			if ( pmsPcuMapping.getCompId().getDispHospCode().equals(selectedPmsPcuMapping.getCompId().getDispHospCode()) && 
					pmsPcuMapping.getCompId().getDispWorkstore().equals(selectedPmsPcuMapping.getCompId().getDispWorkstore()) ) {
				continue;
			}
			for ( PmsIpDurationConv pmsDurationConversion : inPmsDurationConversionList ) {
				if ( !pmsDurationConversion.isMarkDelete() ) {
					updatePmsDurationConversionList.add(createPmsDurationConversion(pmsDurationConversion, pmsPcuMapping));
				}
			}
		}
		
		for ( MpPmsDurationConversionItem mpPmsDurationConversionItem : durationConversionOtherWorkstoreList ) {
			if ( mpPmsDurationConversionItem.isSameWorkstoreGroup() ) {
				for ( PmsIpDurationConv pmsIpDurationConv : mpPmsDurationConversionItem.getPmsIpDurationConvList() ) {
					pmsIpDurationConv.setMarkDelete(true);
					updatePmsDurationConversionList.add(pmsIpDurationConv);
				}
			}
		}
		
		if ( applyAllWorkstore ) { 
			//other workstore group
			for ( PmsPcuMapping pmsPcuMapping : pmsPcuMappingAllWorkstoreList ) {
				if ( pmsPcuMappingMap.containsKey(pmsPcuMapping.getCompId().getDispHospCode() + pmsPcuMapping.getCompId().getDispWorkstore() ) ) {
					continue;
				}
				
				List<PmsPcuMapping> otherGroupPcuMappingList = dmsPmsServiceProxy.retrievePmsPcuMappingList(pmsPcuMapping.getCompId().getDispHospCode(),  
																										pmsPcuMapping.getCompId().getDispWorkstore());
				
				List<PmsIpDurationConv> pmsDurationConversionList = dmsPmsServiceProxy.retrievePmsIpDurationConvList(pmsPcuMapping.getCompId().getDispHospCode(), 
																												pmsPcuMapping.getCompId().getDispWorkstore(), 
																												qtyDurationConversionDmDrugLite.getItemCode());
				
				Map<String, PmsPcuMapping> otherGroupPcuMappingMap = new HashMap<String, PmsPcuMapping>();
				
				for ( PmsPcuMapping otherGroupPcuMapping : otherGroupPcuMappingList ) {
					otherGroupPcuMappingMap.put(otherGroupPcuMapping.getCompId().getDispHospCode() + otherGroupPcuMapping.getCompId().getDispWorkstore(), otherGroupPcuMapping);
					if ( pmsPcuMappingMap.containsKey(otherGroupPcuMapping.getCompId().getDispHospCode() + otherGroupPcuMapping.getCompId().getDispWorkstore() ) ) {
						continue;
					}
					pmsPcuMappingMap.put(otherGroupPcuMapping.getCompId().getDispHospCode() + otherGroupPcuMapping.getCompId().getDispWorkstore(), otherGroupPcuMapping);
					for ( PmsIpDurationConv pmsDurationConversion : inPmsDurationConversionList ) {
						if ( !pmsDurationConversion.isMarkDelete() ) {
							updatePmsDurationConversionList.add(createPmsDurationConversion(pmsDurationConversion, otherGroupPcuMapping));
						}
					}
				}
				
				if ( !pmsDurationConversionList.isEmpty() ) {
					for ( PmsIpDurationConv pmsDurationConversion : pmsDurationConversionList ) {
						if ( !otherGroupPcuMappingMap.containsKey(pmsDurationConversion.getDispHospCode() + pmsDurationConversion.getDispWorkstore() ) ) {
							continue;
						}
						pmsDurationConversion.setMarkDelete(true);
						updatePmsDurationConversionList.add(pmsDurationConversion);
					}
				}
			}
		}

		auditLogger.log( "#0995", serializer.serialize(updatePmsDurationConversionList) );
		return updatePmsDurationConversionList;
	}
	
	private PmsQtyConversionInsu getPmsQtyConversionInsu(String hospCode, String workstoreCode){
		PmsQtyConversionInsu pmsQtyConversionInsu = null;
		for ( MpPmsQtyConversionInsuItem pmsQtyConversionInsuItem :  pmsQtyConversionInsuOtherWorkstoreList) {
			if ( pmsQtyConversionInsuItem.getHospCode().equals(hospCode) && 
					pmsQtyConversionInsuItem.getWorkstoreCode().equals(workstoreCode) ) {
				if( !pmsQtyConversionInsuItem.getPmsQtyConversionInsuList().isEmpty() ) {
					pmsQtyConversionInsu =  pmsQtyConversionInsuItem.getPmsQtyConversionInsuList().get(0);
				}
				break;
			}
		}
		return pmsQtyConversionInsu;
	}
	
	private PmsQtyConversionInsu setPmsQtyConversionInsu(PmsQtyConversionInsu newPmsQtyConversionInsu, PmsQtyConversionInsu pmsQtyConversionInsu){
		newPmsQtyConversionInsu.setItemCode(pmsQtyConversionInsu.getItemCode());
		newPmsQtyConversionInsu.setEnableFlag(pmsQtyConversionInsu.getEnableFlag());
		newPmsQtyConversionInsu.setDoseAdjustment(pmsQtyConversionInsu.getDoseAdjustment());
		newPmsQtyConversionInsu.setBaseUnitAdjustment(pmsQtyConversionInsu.getBaseUnitAdjustment());
		newPmsQtyConversionInsu.setDispenseDosageUnit(pmsQtyConversionInsu.getDispenseDosageUnit());
		newPmsQtyConversionInsu.setMaxStoragePeriod(pmsQtyConversionInsu.getMaxStoragePeriod());
		newPmsQtyConversionInsu.setCalType(pmsQtyConversionInsu.getCalType());
		return newPmsQtyConversionInsu;
	}
	
	private List<PmsQtyConversionInsu> updatePmsQtyConversionInsu(PmsQtyConversionInsu inPmsQtyConversionInsu, boolean applyAllWorkstore){
		List<PmsQtyConversionInsu> updatePmsQtyConversionInsuList = new ArrayList<PmsQtyConversionInsu>();
		Map<String, PmsPcuMapping> pmsPcuMappingMap = new HashMap<String, PmsPcuMapping>();
		
		//same workstore group
		updatePmsQtyConversionInsuList.add(inPmsQtyConversionInsu);
		for ( PmsPcuMapping pmsPcuMapping : pmsPcuMappingList ) {
			pmsPcuMappingMap.put(pmsPcuMapping.getCompId().getDispHospCode() + pmsPcuMapping.getCompId().getDispWorkstore(), pmsPcuMapping);
			if ( pmsPcuMapping.getCompId().getDispHospCode().equals(selectedPmsPcuMapping.getCompId().getDispHospCode()) && 
					pmsPcuMapping.getCompId().getDispWorkstore().equals(selectedPmsPcuMapping.getCompId().getDispWorkstore()) ) {
				continue;
			}
			
			PmsQtyConversionInsu pmsQtyConversionInsu = getPmsQtyConversionInsu(pmsPcuMapping.getCompId().getDispHospCode(), pmsPcuMapping.getCompId().getDispWorkstore());
			
			if ( pmsQtyConversionInsu == null ) {
				PmsQtyConversionInsu newPmsQtyConversionInsu = new PmsQtyConversionInsu();
				newPmsQtyConversionInsu.setDispHospCode(pmsPcuMapping.getCompId().getDispHospCode());
				newPmsQtyConversionInsu.setDispWorkstore(pmsPcuMapping.getCompId().getDispWorkstore());
				updatePmsQtyConversionInsuList.add(setPmsQtyConversionInsu(newPmsQtyConversionInsu, inPmsQtyConversionInsu));
			} else {
				updatePmsQtyConversionInsuList.add(setPmsQtyConversionInsu(pmsQtyConversionInsu, inPmsQtyConversionInsu));
			}
		}

		if ( applyAllWorkstore ) { 
			//other workstore group
			for ( PmsPcuMapping pmsPcuMapping : pmsPcuMappingAllWorkstoreList ) {
				if ( pmsPcuMappingMap.containsKey(pmsPcuMapping.getCompId().getDispHospCode() + pmsPcuMapping.getCompId().getDispWorkstore() ) ) {
					continue;
				}

				List<PmsPcuMapping> otherGroupPcuMappingList = dmsPmsServiceProxy.retrievePmsPcuMappingList(pmsPcuMapping.getCompId().getDispHospCode(),  
																										pmsPcuMapping.getCompId().getDispWorkstore());
				
				List<PmsQtyConversionInsu> pmsQtyConversionInsuList = dmsPmsServiceProxy.retrievePmsQtyConversionInsuList(pmsPcuMapping.getCompId().getDispHospCode(), 
																															pmsPcuMapping.getCompId().getDispWorkstore(), 
																															qtyDurationConversionDmDrugLite.getItemCode());

				Map<String, PmsPcuMapping> otherGroupPcuMappingMap = new HashMap<String, PmsPcuMapping>();
				
				for ( PmsPcuMapping otherGroupPcuMapping : otherGroupPcuMappingList ) {

					otherGroupPcuMappingMap.put(otherGroupPcuMapping.getCompId().getDispHospCode() + otherGroupPcuMapping.getCompId().getDispWorkstore(), otherGroupPcuMapping);
					if ( pmsPcuMappingMap.containsKey(otherGroupPcuMapping.getCompId().getDispHospCode() + otherGroupPcuMapping.getCompId().getDispWorkstore() ) ) {
						continue;
					}
					pmsPcuMappingMap.put(otherGroupPcuMapping.getCompId().getDispHospCode() + otherGroupPcuMapping.getCompId().getDispWorkstore(), otherGroupPcuMapping);

					PmsQtyConversionInsu target = null;
					for ( PmsQtyConversionInsu pmsQtyConversionInsu : pmsQtyConversionInsuList ) {
						if ( pmsQtyConversionInsu.getDispHospCode().equals(otherGroupPcuMapping.getCompId().getDispHospCode()) && 
								pmsQtyConversionInsu.getDispWorkstore().equals(otherGroupPcuMapping.getCompId().getDispWorkstore() )) {

							target = pmsQtyConversionInsu;
							break;
						}
					}
					
					if ( target == null ) {
						PmsQtyConversionInsu newPmsQtyConversionInsu = new PmsQtyConversionInsu();
						newPmsQtyConversionInsu.setDispHospCode(otherGroupPcuMapping.getCompId().getDispHospCode());
						newPmsQtyConversionInsu.setDispWorkstore(otherGroupPcuMapping.getCompId().getDispWorkstore());
						updatePmsQtyConversionInsuList.add(setPmsQtyConversionInsu(newPmsQtyConversionInsu, inPmsQtyConversionInsu));
					} else {
						updatePmsQtyConversionInsuList.add(setPmsQtyConversionInsu(target, inPmsQtyConversionInsu));
					}
				}
			}
		}
		
		auditLogger.log( "#0996", serializer.serialize(updatePmsQtyConversionInsuList) );
		return updatePmsQtyConversionInsuList;
	}
	
	private PmsIpQtyConv createPmsQtyConversion(PmsIpQtyConv inPmsQtyConversion, PmsPcuMapping pmsPcuMapping){
		PmsIpQtyConv pmsIpQtyConv = new PmsIpQtyConv();
		pmsIpQtyConv.setDispHospCode(pmsPcuMapping.getCompId().getDispHospCode());
		pmsIpQtyConv.setDispWorkstore(pmsPcuMapping.getCompId().getDispWorkstore());
		pmsIpQtyConv.setItemCode(inPmsQtyConversion.getItemCode());
		pmsIpQtyConv.setQtyFrom(inPmsQtyConversion.getQtyFrom());
		pmsIpQtyConv.setQtyTo(inPmsQtyConversion.getQtyTo());
		pmsIpQtyConv.setDispenseDosageUnit(inPmsQtyConversion.getDispenseDosageUnit());
		pmsIpQtyConv.setConvertQty(inPmsQtyConversion.getConvertQty());
		pmsIpQtyConv.setConvertBaseUnit(inPmsQtyConversion.getConvertBaseUnit());
		pmsIpQtyConv.setExtrapolateFlag(inPmsQtyConversion.getExtrapolateFlag());
		return pmsIpQtyConv;
	}
	
	private PmsIpDurationConv createPmsDurationConversion(PmsIpDurationConv inPmsDurationConversion, PmsPcuMapping pmsPcuMapping){
		PmsIpDurationConv pmsIpDurationConv = new PmsIpDurationConv();
		pmsIpDurationConv.setDispHospCode(pmsPcuMapping.getCompId().getDispHospCode());
		pmsIpDurationConv.setDispWorkstore(pmsPcuMapping.getCompId().getDispWorkstore());
		pmsIpDurationConv.setItemCode(inPmsDurationConversion.getItemCode());
		pmsIpDurationConv.setDurationFrom(inPmsDurationConversion.getDurationFrom());
		pmsIpDurationConv.setDurationTo(inPmsDurationConversion.getDurationTo());
		pmsIpDurationConv.setDurationUnit(inPmsDurationConversion.getDurationUnit());
		pmsIpDurationConv.setConvertQty(inPmsDurationConversion.getConvertQty());
		pmsIpDurationConv.setConvertBaseUnit(inPmsDurationConversion.getConvertBaseUnit());
		pmsIpDurationConv.setExtrapolateFlag(inPmsDurationConversion.getExtrapolateFlag());
		return pmsIpDurationConv;
	}
	
	public boolean isRetrieveCapdItem() {
		return retrieveCapdItem;
	}
	
	public boolean isSaveSuccess() {
		return saveSuccess;
	}

	private void clearList()
	{
		qtyDurationConversionDmDrugLite = null;
		qtyConversionList = null;
		qtyConversionOtherWorkstoreList = null;
		durationConversionList = null;
		durationConversionOtherWorkstoreList = null;
		pmsQtyConversionInsuOtherWorkstoreList = null;
		pmsQtyConversionInsu = null;
		pmsQtyConversionAdj = null;
		selectedPmsPcuMapping = null;
	}
	
	@Remove
	public void destroy() 
	{
		if (qtyConversionList != null) {
			qtyConversionList = null;
		}
		if (pmsPcuMappingList != null) {
			pmsPcuMappingList = null;
		}
		if (qtyConversionOtherWorkstoreList != null) {
			qtyConversionOtherWorkstoreList = null;
		}
		if ( pmsPcuMappingAllWorkstoreList != null ) {
			pmsPcuMappingAllWorkstoreList = null;
		}
		if ( pmsQtyConversionInsuOtherWorkstoreList != null ) {
			pmsQtyConversionInsuOtherWorkstoreList = null;
		}
		if ( pmsQtyConversionInsu != null ) {
			pmsQtyConversionInsu = null;
		}
		if ( pmsQtyConversionAdj != null ) {
			pmsQtyConversionAdj = null;
		}
		if ( qtyDurationConversionDmDrugLite != null ) {
			qtyDurationConversionDmDrugLite = null;
		}
	}

}
