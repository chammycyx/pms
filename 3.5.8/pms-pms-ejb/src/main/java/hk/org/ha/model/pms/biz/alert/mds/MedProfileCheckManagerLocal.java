package hk.org.ha.model.pms.biz.alert.mds;

import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.vo.alert.mds.MedProfileCheckResult;

import java.util.List;

import javax.ejb.Local;

@Local
public interface MedProfileCheckManagerLocal {

	MedProfileCheckResult checkMedProfileAlertForSaveProfile(MedProfile medProfile, List<MedProfileItem> medProfileItemList);
	
	MedProfileCheckResult checkMedProfileAlertForDueOrderPrint(MedProfile medProfile, List<MedProfileItem> medProfileItemList, boolean checkDdiFlag);
}
