@XmlJavaTypeAdapters({   
    @XmlJavaTypeAdapter(value=DateAdapter.class, type=Date.class),
    @XmlJavaTypeAdapter(value=RegimenTypeAdapter.class, type=RegimenType.class),
    @XmlJavaTypeAdapter(value=DispOrderAdminStatusAdapter.class, type=DispOrderAdminStatus.class)
})  
package hk.org.ha.model.pms.vo.cddh;

import hk.org.ha.fmk.pms.eai.mail.DateAdapter;
import hk.org.ha.model.pms.vo.rx.RegimenTypeAdapter;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatusAdapter;
import hk.org.ha.model.pms.udt.vetting.RegimenType;

import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;
   