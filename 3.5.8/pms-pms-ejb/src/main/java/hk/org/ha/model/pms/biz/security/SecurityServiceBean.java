package hk.org.ha.model.pms.biz.security;

import hk.org.ha.fmk.pms.web.MeasureCalls;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;
import org.jboss.seam.security.Identity;

/**
 * Session Bean implementation class SecurityServiceBean
 */
@Stateful
@Name("securityService")
@Scope(ScopeType.SESSION)
@RemoteDestination
@MeasureCalls
public class SecurityServiceBean implements SecurityServiceLocal {

	@Logger
	private Log logger;
	
	@In
	private Identity identity;
	
	public void logoff() {
		try {
			identity.logout();
		} catch (IllegalStateException e) {
			logger.warn(e.getMessage());
		}
	}
	
	@Remove
	public void destroy() {
	}
	
}
