package hk.org.ha.model.pms.biz.refill;

import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

@Local
public interface SfiRefillScheduleListManagerLocal {

	List<RefillSchedule> getSfiRefillScheduleList(DispOrder dispOrder); 
	List<RefillSchedule> createSfiRefillScheduleList(OrderViewInfo orderViewInfo, DispOrder dispOrder, DispOrder prevDispOrder);
	void cancelDispenseSfiRefillSchedule(List<RefillSchedule> newSfiRsList);
	void updateSfiRefillScheduleListStatus(OrderViewInfo orderViewInfoIn, RefillSchedule refillSchedule);
	Map<Integer, Boolean> getPrintRefillAuxLabelFlagMap( List<Long> currRefillPoiList, List<Long> nextRefillPoiList, RefillSchedule refillSchedule);
}
