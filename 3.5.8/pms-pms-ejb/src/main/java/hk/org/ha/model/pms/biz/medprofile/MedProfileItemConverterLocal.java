package hk.org.ha.model.pms.biz.medprofile;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItemInf;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;

import java.util.List;

import javax.ejb.Local;

@Local
public interface MedProfileItemConverterLocal {
	void convertItemList(Workstore workstore,
			List<MedProfileMoItem> medProfileMoItemList, List<Integer> atdpsPickStationsNumList);
	void convertItemList(Workstore workstore, MedProfile medProfile,
			List<MedProfileMoItem> medProfileMoItemList, List<Integer> atdpsPickStationsNumList);
	MedProfilePoItem recalculateQty(Workstore workstore, MedProfile medProfile, MedProfilePoItem medProfilePoItem);
	List<MedProfilePoItem> recalculateQty(Workstore workstore,
			MedProfile medProfile, List<MedProfilePoItem> medProfilePoItemList);
	void formatMedProfileMoItemList(List<? extends MedProfileMoItemInf> medProfileMoItemList);
	void formatDischargeMoItemList(List<?> itemList);
	boolean isSingleDispenseItem(MedProfile medProfile, MedProfileMoItem medProfileMoItem, MedProfilePoItem medProfilePoItem);
	boolean isUnitDose(Workstore workstore, MedProfile medProfile, MedProfileMoItem medProfileMoItem, MedProfilePoItem medProfilePoItem, List<Integer> atdpsPickStationsNumList);
}
