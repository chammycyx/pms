package hk.org.ha.model.pms.biz.drug;

import javax.ejb.Local;

@Local
public interface DmSpecialtyMoMappingListServiceLocal {

	void retrieveDmSpecialtyMoMappingList(String prefixSpecialtyCode);
	
	void destroy();
	
}
