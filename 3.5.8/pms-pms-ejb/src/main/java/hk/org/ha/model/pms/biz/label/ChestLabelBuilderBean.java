package hk.org.ha.model.pms.biz.label;

import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_HOSPITAL_NAMEENG;
import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_HOSPITAL_NAMECHI;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.vo.label.ChestLabel;
import hk.org.ha.model.pms.vo.label.ChestLabelItem;
import hk.org.ha.model.pms.vo.label.ChestLabelList;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.Freq;

import java.lang.reflect.InvocationTargetException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@AutoCreate
@Stateless
@Name("chestLabelBuilder")
@Scope(ScopeType.STATELESS)
public class ChestLabelBuilderBean implements ChestLabelBuilderLocal {
	
	private static final String ALTERNATE_SUPPL_FREQ_CODE = "00001";
	
	@In
	private UamInfo uamInfo;
	
	private DecimalFormat df = new DecimalFormat("#######.###");
	
	public ChestLabelList convertToChestLabelList(DispOrder dispOrder) {
		ChestLabelList chestLabelList = new ChestLabelList();
		List<ChestLabel> chestLabels = new ArrayList<ChestLabel>();
		ChestLabel chestLabel = convertToChestLabel(dispOrder);
		
		DispOrderItem chestDoi = null;
		for (DispOrderItem doi : dispOrder.getDispOrderItemList()) {
			if (doi.getPharmOrderItem().getChestFlag()) {
				chestDoi = doi;
				break;
			}
		}
		
		List<Date> doseDateList = getDoseDateList(chestDoi);
		for (Date doseDate:doseDateList) {
			try {
				ChestLabel cloneChestLabel;
				cloneChestLabel = (ChestLabel) BeanUtils.cloneBean(chestLabel);
				cloneChestLabel.setDoseDate(doseDate);
				chestLabels.add(cloneChestLabel);
			} catch (IllegalAccessException e) {
			} catch (InstantiationException e) {
			} catch (InvocationTargetException e) {
			} catch (NoSuchMethodException e) {
			}
		}
		chestLabelList.setChestLabels(chestLabels);
		return chestLabelList;
	}
	
	private List<Date> getDoseDateList(DispOrderItem dispOrderItem) {
		List<Date> doseDateList = new ArrayList<Date>();
		DoseGroup doseGroup = dispOrderItem.getPharmOrderItem().getRegimen().getDoseGroupList().get(0);
		Freq supplFreq = doseGroup.getDoseList().get(0).getSupplFreq();
		Calendar curDate = Calendar.getInstance();
		Calendar endDate = Calendar.getInstance();
		curDate.setTime(doseGroup.getStartDate());
		endDate.setTime(doseGroup.getEndDate());
		
		if (supplFreq == null || ALTERNATE_SUPPL_FREQ_CODE.equals(supplFreq.getCode())) {
			boolean isEveryday = (supplFreq == null);
			while(curDate.compareTo(endDate) <= 0) {
				doseDateList.add(curDate.getTime());
				curDate.add(Calendar.DATE, isEveryday?1:2);
			}
		}
		else {
			int doseDay = doseGroup.getDoseDay()==null || doseGroup.getDoseDay().intValue()==0 ? -1 : doseGroup.getDoseDay().intValue();
			while(curDate.compareTo(endDate) <= 0 && doseDay != 0) {
				if (supplFreq.getParam()[0].charAt(curDate.get(Calendar.DAY_OF_WEEK)-1) == '1') {
					doseDateList.add(curDate.getTime());
					doseDay--;
				}
				curDate.add(Calendar.DATE, 1);
			}
		}
		
		return doseDateList;
	}
		
	private ChestLabel convertToChestLabel(DispOrder dispOrder) {
		PharmOrder pharmOrder = dispOrder.getPharmOrder();

		ChestLabel chestLabel = new ChestLabel();
		if (pharmOrder.getMedCase() != null) {
			chestLabel.setBedNum(pharmOrder.getMedCase().getPasBedNum());
			chestLabel.setCaseNum(pharmOrder.getMedCase().getCaseNum());
		}
		chestLabel.setDispDate(dispOrder.getDispDate());
		chestLabel.setSpecCode(pharmOrder.getSpecCode());
		chestLabel.setDoctorCode(dispOrder.getPharmOrder().getDoctorCode());
		chestLabel.setHospCode(dispOrder.getWorkstore().getHospCode());
		chestLabel.setHospName(LABEL_DISPLABEL_HOSPITAL_NAMEENG.get());
		chestLabel.setHospNameChi(LABEL_DISPLABEL_HOSPITAL_NAMECHI.get());
		chestLabel.setUserCode(uamInfo.getUserId());
		chestLabel.setPatCatCode(pharmOrder.getPatCatCode());
		chestLabel.setWard(pharmOrder.getWardCode());
		if (pharmOrder.getMedOrder() != null) {
			String patNameEng = pharmOrder.getMedOrder().getPatient().getName();
			String patNameChi = pharmOrder.getMedOrder().getPatient().getNameChi();
			chestLabel.setPatName(StringUtils.isBlank(patNameEng)?"":patNameEng);
			chestLabel.setPatNameChi(StringUtils.isBlank(patNameChi)?"":StringUtils.trim(patNameChi));
		}
		if (dispOrder.getTicket() != null) {
			chestLabel.setTicketNum(dispOrder.getTicket().getTicketNum());
		}
		
		
		chestLabel.setChestLabelItemList(contructChestLabelItemList(pharmOrder.getPharmOrderItemList()));

		return chestLabel;
	}
	
	private List<ChestLabelItem> contructChestLabelItemList(List<PharmOrderItem> pharmOrderItemList) {
		List<ChestLabelItem> chestLabelItemList = new ArrayList<ChestLabelItem>();
		for (PharmOrderItem pharmOrderItem:pharmOrderItemList) {
			if (!pharmOrderItem.getChestFlag()) {
				continue;
			}
			ChestLabelItem chestLabelItem = new ChestLabelItem();
			chestLabelItem.setItemDesc(pharmOrderItem.getFullDrugDesc());
			chestLabelItem.setDoseQty(df.format(pharmOrderItem.getRegimen().getDoseGroupList().get(0).getDoseList().get(0).getDosage()).toString());
			chestLabelItem.setDoseUnit(pharmOrderItem.getDoseUnit());
			chestLabelItem.setItemCode(pharmOrderItem.getItemCode());
			chestLabelItemList.add(chestLabelItem);
		}
		
		return chestLabelItemList;
	}

}