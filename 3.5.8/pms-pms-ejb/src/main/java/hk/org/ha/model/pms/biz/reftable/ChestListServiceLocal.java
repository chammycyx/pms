package hk.org.ha.model.pms.biz.reftable;

import javax.ejb.Local;

@Local
public interface ChestListServiceLocal {

	void retrieveChestList(String prefixUnitDose);
	
	void destroy();
	
}
