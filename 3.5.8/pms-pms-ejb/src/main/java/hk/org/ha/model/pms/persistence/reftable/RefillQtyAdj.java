package hk.org.ha.model.pms.persistence.reftable;

import flexjson.JSON;
import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.corp.cache.DmFormCacher;
import hk.org.ha.model.pms.dms.persistence.DmForm;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.Customizer;
import org.jboss.seam.security.Identity;

@Entity
@Table(name = "REFILL_QTY_ADJ")
@Customizer(AuditCustomizer.class)
public class RefillQtyAdj extends VersionEntity {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "refillQtyAdjSeq")
	@SequenceGenerator(name = "refillQtyAdjSeq", sequenceName = "SQ_REFILL_QTY_ADJ", initialValue = 100000000)
	private Long id;
	
	@Column(name = "FORM_CODE", nullable = false, length = 3)
	private String formCode;

	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE", referencedColumnName = "HOSP_CODE", nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;

	@Transient
	private DmForm dmForm;	
	
	@PostLoad
	public void postLoad() {
		if (formCode != null && Identity.instance().isLoggedIn()) {
			dmForm = DmFormCacher.instance().getFormByFormCode(formCode);
		}
	}		

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFormCode() {
		return formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}

	public void setDmForm(DmForm dmForm) {
		this.dmForm = dmForm;
	}

	@JSON(include=false)
	public DmForm getDmForm() {
		return dmForm;
	}
}
