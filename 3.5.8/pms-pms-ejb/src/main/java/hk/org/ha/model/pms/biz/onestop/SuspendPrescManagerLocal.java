package hk.org.ha.model.pms.biz.onestop;

import hk.org.ha.model.pms.biz.onestop.SuspendPrescManagerBean.ReversePrescriptionSuspendResult;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.vo.charging.SfiForceProceedInfo;

import javax.ejb.Local;

@Local
public interface SuspendPrescManagerLocal {
	
	void verifySuspendPrescription(Long dispOrderId, Long dispOrderVersion, String suspendCode, String caller);
	
	ReversePrescriptionSuspendResult reversePrescriptionSuspend(Long dispOrderId, Long dispOrderVersion, String caller);
	
	void suspendPrescription(DispOrder dispOrder, String suspendCode, boolean removeIqdsTicketNum);
		
	void sfiPrescriptionSuspendForceProceed(Long dispOrderId, String caller, SfiForceProceedInfo sfiForceProceedInfo);
	
	void sfiPrescriptionSuspendDeferProceed(Long dispOrderId, String caller);
	
	void reverseSuspendByDispOrder(DispOrder dispOrder, String caller, boolean updateCorp, boolean updateLargeLayoutFlag);
	
	DispOrder markRevokeFlagForUncollectOrder(DispOrder dispOrder);
}
