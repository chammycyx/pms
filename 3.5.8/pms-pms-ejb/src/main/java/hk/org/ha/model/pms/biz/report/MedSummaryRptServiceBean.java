package hk.org.ha.model.pms.biz.report;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.vo.report.MedSummaryRpt;

import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("medSummaryRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MedSummaryRptServiceBean implements MedSummaryRptServiceLocal {
	
	@In
	private MedSummaryRptManagerLocal medSummaryRptManager;
	
	private boolean retrieveSuccess;
	
	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}

	public void printMedSummaryRpt(Date dispDate, String ticketNum,
			PrintLang printLang) {
		retrieveSuccess=medSummaryRptManager.printMedSummaryRpt(dispDate, ticketNum, printLang);
	}

	public List<MedSummaryRpt> retrieveMedSummaryRptList(Date dispDate,
			String ticketNum, PrintLang printLang) {
		return medSummaryRptManager.retrieveMedSummaryRptList(dispDate, ticketNum, printLang);
	}

	@Remove
	public void destroy() {
		
	}
}
