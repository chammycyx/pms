package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.SuspendReason;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.util.Collection;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateless
@Name("suspendReasonManager")
@Restrict("#{identity.loggedIn}")
@MeasureCalls
public class SuspendReasonManagerBean implements SuspendReasonManagerLocal {

	@PersistenceContext
	private EntityManager em;
	
	@In
	private Workstore workstore;
	
	@SuppressWarnings("unchecked")
	public String retrieveSuspendReasonDesc(Workstore workstore, String suspendCode) {
		return (String) MedProfileUtils.getFirstItem(em.createQuery(
				"select o.description from SuspendReason o" + // 20120303 index check : SuspendReason.hospital : FK_SUSPEND_REASON_01
				" where o.suspendCode = :suspendCode" +
				" and o.hospital = :hospital" +
				" and o.status = :status")
				.setParameter("suspendCode", suspendCode)
				.setParameter("hospital", workstore.getHospital())
				.setParameter("status", RecordStatus.Active)
				.getResultList());
	}
	
	@SuppressWarnings("unchecked")
	public List<SuspendReason> retrieveSuspendReasonList() {
		return (List<SuspendReason>) em.createQuery(
				"select o from SuspendReason o" + // 20120303 index check : SuspendReason.hospital : FK_SUSPEND_REASON_01
				" where o.hospital = :hospital" +
				" and o.status = :status" +
				" order by o.suspendCode")
				.setParameter("hospital", workstore.getHospital())
				.setParameter("status", RecordStatus.Active)
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public void updateSuspendReasonList(Collection<SuspendReason> newSuspendList) {
		Hospital hospital = workstore.getHospital();

		for (SuspendReason suspendReason : newSuspendList) {
			if (suspendReason.getStatus()== RecordStatus.Delete) {
				em.merge(suspendReason);
			} else {
				List suspendReasonList = em.createQuery(
						"select o from SuspendReason o" + // 20120303 index check : SuspendReason.hospital,suspendCode : UI_SUSPEND_REASON_01
						" where o.hospital = :hospital" +
						" and o.suspendCode = :suspendCode")
						.setParameter("hospital", hospital)
						.setParameter("suspendCode", suspendReason.getSuspendCode()).getResultList();
				
				if (!suspendReasonList.isEmpty()) {
					SuspendReason suspendReasonObj = ((SuspendReason) suspendReasonList.get(0));					
					suspendReasonObj.setStatus(RecordStatus.Active);
					suspendReasonObj.setDescription(suspendReason.getDescription());
				} else {
					suspendReason.setHospital(hospital);
					em.persist(suspendReason);
				}
			}			
		}
		em.flush();		
	}
}
