package hk.org.ha.model.pms.biz.reftable;

import flexjson.JSONSerializer;
import flexjson.transformer.DateTransformer;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.PmsDurationConversion;
import hk.org.ha.model.pms.dms.persistence.PmsPcuMapping;
import hk.org.ha.model.pms.dms.persistence.PmsQtyConversion;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.reftable.DurationUnit;
import hk.org.ha.model.pms.vo.drug.DmDrugLite;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.model.pms.vo.reftable.PmsDurationConversionItem;
import hk.org.ha.model.pms.vo.reftable.PmsQtyConversionItem;
import hk.org.ha.model.pms.vo.reftable.QtyDurationConversionInfo;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.OptimisticLockException;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("qtyDurationConversionService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class QtyDurationConversionServiceBean implements QtyDurationConversionServiceLocal {
	
	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";	
	private static final JSONSerializer serializer = new JSONSerializer().transform(new DateTransformer(DATE_FORMAT), Date.class);	
	
	@In
	private AuditLogger auditLogger;
	
	@In
	private Workstore workstore;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;
	
	@In
	private List<PmsPcuMapping> pmsPcuMappingList;
	
	private List<PmsPcuMapping> pmsPcuMappingAllWorkstoreList;

	private List<PmsQtyConversion> qtyConversionList;
	
	private List<PmsQtyConversionItem> qtyConversionOtherWorkstoreList;

	private List<PmsDurationConversion> durationConversionList;

	private List<PmsDurationConversionItem> durationConversionOtherWorkstoreList;

	private DmDrugLite qtyDurationConversionDmDrugLite;

	private Map<String, PmsPcuMapping> workstoreGroupMap;
	
	private boolean retrieveCapdItem;
	
	private PmsPcuMapping selectedPmsPcuMapping;
	
	private boolean saveSuccess = true;
	
	public QtyDurationConversionInfo retrieveQtyDurationConversionList(String itemCode, PmsPcuMapping pmsPcuMapping){
		clearList();
		WorkstoreDrug workstoreDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, itemCode);
		retrieveCapdItem = false;
		
		QtyDurationConversionInfo qtyDurationConversionInfo = new QtyDurationConversionInfo();
		if ( workstoreDrug != null && workstoreDrug.getMsWorkstoreDrug() !=null && workstoreDrug.getDmDrug() != null ) {
			qtyDurationConversionDmDrugLite = workstoreDrug.getDmDrugLite();
			if (!StringUtils.equals(workstoreDrug.getDmDrug().getTheraGroup(), "PDF")) {
				selectedPmsPcuMapping = pmsPcuMapping;
				retrievePmsPcuMappingAllWorkstoreList(pmsPcuMapping.getCompId().getDispHospCode());
				retrievePmsQtyConversionList(itemCode, pmsPcuMapping);
				retrievePmsDurationConversionList(itemCode, pmsPcuMapping);
				qtyDurationConversionInfo.setQtyDurationConversionDmDrugLite(qtyDurationConversionDmDrugLite);
				qtyDurationConversionInfo.setQtyConversionList(qtyConversionList);
				qtyDurationConversionInfo.setQtyConversionOtherWorkstoreList(qtyConversionOtherWorkstoreList);
				qtyDurationConversionInfo.setDurationConversionList(durationConversionList);
				qtyDurationConversionInfo.setDurationConversionOtherWorkstoreList(durationConversionOtherWorkstoreList);
				return qtyDurationConversionInfo;
			} else {
				retrieveCapdItem = true;
				return qtyDurationConversionInfo;
			}
		}
		return qtyDurationConversionInfo;
	}
	
	private void retrievePmsPcuMappingAllWorkstoreList(String hospCode){
		pmsPcuMappingAllWorkstoreList = dmsPmsServiceProxy.retrievePmsPcuMappingListByDispHospCode(hospCode);
		
		Map<String, PmsPcuMapping> pmsPcuMappingMap = new HashMap<String, PmsPcuMapping>();

		for ( PmsPcuMapping pmsPcuMapping : pmsPcuMappingAllWorkstoreList ) {
			pmsPcuMappingMap.put(pmsPcuMapping.getCompId().getDispHospCode()+pmsPcuMapping.getCompId().getDispWorkstore(), pmsPcuMapping);
		}

		workstoreGroupMap = new HashMap<String, PmsPcuMapping>();

		for (PmsPcuMapping pmsPcuMapping : pmsPcuMappingList) {
			if ( pmsPcuMappingMap.get(pmsPcuMapping.getCompId().getDispHospCode()+pmsPcuMapping.getCompId().getDispWorkstore()) == null ) {
				pmsPcuMappingAllWorkstoreList.add(pmsPcuMapping);
			}
			workstoreGroupMap.put(pmsPcuMapping.getCompId().getDispHospCode()+pmsPcuMapping.getCompId().getDispWorkstore(), pmsPcuMapping);
		}
	}
	
	private void retrievePmsQtyConversionList(String itemCode, PmsPcuMapping inPmsPcuMapping){
		List<PmsQtyConversion> pmsQtyConversionList = dmsPmsServiceProxy.retrievePmsQtyConversionList(
				inPmsPcuMapping.getCompId().getDispHospCode(), inPmsPcuMapping.getCompId().getDispWorkstore(), itemCode);
		
		qtyConversionList = new ArrayList<PmsQtyConversion>();
		qtyConversionOtherWorkstoreList = new ArrayList<PmsQtyConversionItem>();
		
		boolean findPmsQtyConversion = false;
		for (PmsPcuMapping pmsPcuMapping : pmsPcuMappingAllWorkstoreList ) {

			findPmsQtyConversion = false;
			for (PmsQtyConversion pmsQtyConversion : pmsQtyConversionList) {
				if ( !pmsPcuMapping.getCompId().getDispHospCode().equals(pmsQtyConversion.getDispHospCode()) ||
						!pmsPcuMapping.getCompId().getDispWorkstore().equals(pmsQtyConversion.getDispWorkstore())) {
					continue;
				}
				findPmsQtyConversion = true;
				if ( pmsQtyConversion.getDispHospCode().equals(inPmsPcuMapping.getCompId().getDispHospCode())  
						&& pmsQtyConversion.getDispWorkstore().equals(inPmsPcuMapping.getCompId().getDispWorkstore()) ) {
					
					qtyConversionList.add(pmsQtyConversion);
				} else {
					
					boolean find = false;
					for ( PmsQtyConversionItem pmsQtyConversionItem : qtyConversionOtherWorkstoreList ) {
						if (StringUtils.equals(pmsQtyConversionItem.getWorkstoreCode(), pmsQtyConversion.getDispWorkstore()) && 
								StringUtils.equals(pmsQtyConversionItem.getHospCode(), pmsQtyConversion.getDispHospCode())) { 
							pmsQtyConversionItem.getPmsQtyConversionList().add(pmsQtyConversion);
							find = true;
						}
					}
					if (!find) {
						PmsQtyConversionItem pmsQtyConversionItem = createPmsQtyConversionItem(pmsQtyConversion.getDispHospCode(), 
																								pmsQtyConversion.getDispWorkstore());
						pmsQtyConversionItem.getPmsQtyConversionList().add(pmsQtyConversion);
						qtyConversionOtherWorkstoreList.add(pmsQtyConversionItem);
					}
				}
			}
			if ( !findPmsQtyConversion && 
					(!pmsPcuMapping.getCompId().getDispHospCode().equals(inPmsPcuMapping.getCompId().getDispHospCode()) || 
							!pmsPcuMapping.getCompId().getDispWorkstore().equals(inPmsPcuMapping.getCompId().getDispWorkstore()))) {
				PmsQtyConversionItem pmsQtyConversionItem = createPmsQtyConversionItem(pmsPcuMapping.getCompId().getDispHospCode(), 
																						pmsPcuMapping.getCompId().getDispWorkstore());
				
				qtyConversionOtherWorkstoreList.add(pmsQtyConversionItem);
			}
		}
	}
	
	private PmsQtyConversionItem createPmsQtyConversionItem(String hospCode, String workstoreCode){
		PmsQtyConversionItem pmsQtyConversionItem = new PmsQtyConversionItem();
		pmsQtyConversionItem.setHospCode(hospCode);
		pmsQtyConversionItem.setWorkstoreCode(workstoreCode);
		pmsQtyConversionItem.setSameWorkstoreGroup(sameWorkstoreGroup(hospCode, workstoreCode));

		return pmsQtyConversionItem;
	}
	
	private PmsDurationConversionItem createPmsDurationConversionItem(String hospCode, String workstoreCode){
		PmsDurationConversionItem pmsDurationConversionItem = new PmsDurationConversionItem();
		pmsDurationConversionItem.setHospCode(hospCode);
		pmsDurationConversionItem.setWorkstoreCode(workstoreCode);
		pmsDurationConversionItem.setSameWorkstoreGroup(sameWorkstoreGroup(hospCode, workstoreCode));

		return pmsDurationConversionItem;
	}
	
	private void retrievePmsDurationConversionList(String itemCode, PmsPcuMapping inPmsPcuMapping){
		List<PmsDurationConversion> pmsDurationConversionList = dmsPmsServiceProxy.retrievePmsDurationConversionList(
				inPmsPcuMapping.getCompId().getDispHospCode(), inPmsPcuMapping.getCompId().getDispWorkstore(), itemCode);
		
		durationConversionList = new ArrayList<PmsDurationConversion>();
		durationConversionOtherWorkstoreList = new ArrayList<PmsDurationConversionItem>();
		
		boolean findPmsDurationConversion = false;
		for (PmsPcuMapping pmsPcuMapping : pmsPcuMappingAllWorkstoreList ) {

			findPmsDurationConversion = false;
			
			for (PmsDurationConversion pmsDurationConversion : pmsDurationConversionList) {
				if ( !pmsPcuMapping.getCompId().getDispHospCode().equals(pmsDurationConversion.getDispHospCode()) ||
						!pmsPcuMapping.getCompId().getDispWorkstore().equals(pmsDurationConversion.getDispWorkstore())) {
					continue;
				}
				findPmsDurationConversion = true;
				pmsDurationConversion.setDurationUnitDesc(DurationUnit.dataValueOf(pmsDurationConversion.getDurationUnit()).getDisplayValue());
				if ( pmsDurationConversion.getDispHospCode().equals(inPmsPcuMapping.getCompId().getDispHospCode())  
						&& pmsDurationConversion.getDispWorkstore().equals(inPmsPcuMapping.getCompId().getDispWorkstore()) ) {
					durationConversionList.add(pmsDurationConversion);
				} else {
					boolean find = false;
					for ( PmsDurationConversionItem pmsDurationConversionItem : durationConversionOtherWorkstoreList ) {
						if (StringUtils.equals(pmsDurationConversionItem.getWorkstoreCode(), pmsDurationConversion.getDispWorkstore()) && 
								StringUtils.equals(pmsDurationConversionItem.getHospCode(), pmsDurationConversion.getDispHospCode())) {
							pmsDurationConversionItem.getPmsDurationConversionList().add(pmsDurationConversion);
							find = true;
						}
					}
					if (!find) {
						PmsDurationConversionItem pmsDurationConversionItem = createPmsDurationConversionItem(pmsDurationConversion.getDispHospCode(), 
																												pmsDurationConversion.getDispWorkstore());
						pmsDurationConversionItem.getPmsDurationConversionList().add(pmsDurationConversion);
						durationConversionOtherWorkstoreList.add(pmsDurationConversionItem);
					}
				}
			} 
			if ( !findPmsDurationConversion && 
					(!pmsPcuMapping.getCompId().getDispHospCode().equals(inPmsPcuMapping.getCompId().getDispHospCode()) || 
							!pmsPcuMapping.getCompId().getDispWorkstore().equals(inPmsPcuMapping.getCompId().getDispWorkstore()))) {
				PmsDurationConversionItem pmsDurationConversionItem = createPmsDurationConversionItem(pmsPcuMapping.getCompId().getDispHospCode(), 
																						pmsPcuMapping.getCompId().getDispWorkstore());
				
				durationConversionOtherWorkstoreList.add(pmsDurationConversionItem);
			}
		}	
	}
	
	private boolean sameWorkstoreGroup(String dispHospCode, String dispWorkstore){
		return ( workstoreGroupMap.get(dispHospCode+dispWorkstore) != null );
	}
	
	public String updateQtyDurationConversionList(List<PmsQtyConversion> inPmsQtyConversionList, boolean qtyApplyAll,
			List<PmsDurationConversion> inPmsDurationConversionList, boolean durationApplyAll) {		
		List<PmsQtyConversion> updatePmsQtyConversionList = updatePmsQtyConversion(inPmsQtyConversionList, qtyApplyAll);
		List<PmsDurationConversion> updatePmsDurationConversionList = updatePmsDurationConversion(inPmsDurationConversionList, durationApplyAll);
		try{
			dmsPmsServiceProxy.updatePmsQtyDurationConversion(updatePmsQtyConversionList, updatePmsDurationConversionList);
			saveSuccess = true;
			return "0446";
		}catch(Exception e){
			saveSuccess = false;
			return "0729";
		}
	}
	
	private List<PmsQtyConversion> updatePmsQtyConversion(List<PmsQtyConversion> inPmsQtyConversionList, boolean applyAllWorkstore){
		List<PmsQtyConversion> updatePmsQtyConversionList = new ArrayList<PmsQtyConversion>(inPmsQtyConversionList);
		Map<String, PmsPcuMapping> pmsPcuMappingMap = new HashMap<String, PmsPcuMapping>();
		
		List<PmsPcuMapping> targetPmsPcuMappingList = null;
		
		if ( applyAllWorkstore ) {
			//same hospital
			targetPmsPcuMappingList = pmsPcuMappingAllWorkstoreList;
		}else{
			//same workstore group
			targetPmsPcuMappingList = pmsPcuMappingList;
		}
		
		for ( PmsPcuMapping pmsPcuMapping : targetPmsPcuMappingList ) {
			pmsPcuMappingMap.put(pmsPcuMapping.getCompId().getDispHospCode() + pmsPcuMapping.getCompId().getDispWorkstore(), pmsPcuMapping);
			if ( pmsPcuMapping.getCompId().getDispHospCode().equals(selectedPmsPcuMapping.getCompId().getDispHospCode()) && 
					pmsPcuMapping.getCompId().getDispWorkstore().equals(selectedPmsPcuMapping.getCompId().getDispWorkstore()) ) {
				continue;
			}
			for ( PmsQtyConversion pmsQtyConversion : inPmsQtyConversionList ) {
				if ( !pmsQtyConversion.isMarkDelete() ) {
					updatePmsQtyConversionList.add(createPmsQtyConversion(pmsQtyConversion, pmsPcuMapping));
				}
			}
		}
		
		for ( PmsQtyConversionItem pmsQtyConversionItem : qtyConversionOtherWorkstoreList ) {
			if ( pmsQtyConversionItem.isSameWorkstoreGroup() || applyAllWorkstore ) {
				for ( PmsQtyConversion pmsQtyConversion : pmsQtyConversionItem.getPmsQtyConversionList() ) {
					pmsQtyConversion.setMarkDelete(true);
					updatePmsQtyConversionList.add(pmsQtyConversion);
				}
			}
		}
		
		auditLogger.log( "#1001", serializer.serialize(updatePmsQtyConversionList) );
		return updatePmsQtyConversionList;
	}
	
	private List<PmsDurationConversion> updatePmsDurationConversion(List<PmsDurationConversion> inPmsDurationConversionList, boolean applyAllWorkstore){
		List<PmsDurationConversion> updatePmsDurationConversionList = new ArrayList<PmsDurationConversion>(inPmsDurationConversionList);
		Map<String, PmsPcuMapping> pmsPcuMappingMap = new HashMap<String, PmsPcuMapping>();
		
		
		//same workstore group
		for ( PmsPcuMapping pmsPcuMapping : pmsPcuMappingList ) {
			pmsPcuMappingMap.put(pmsPcuMapping.getCompId().getDispHospCode() + pmsPcuMapping.getCompId().getDispWorkstore(), pmsPcuMapping);
			if ( pmsPcuMapping.getCompId().getDispHospCode().equals(selectedPmsPcuMapping.getCompId().getDispHospCode()) && 
					pmsPcuMapping.getCompId().getDispWorkstore().equals(selectedPmsPcuMapping.getCompId().getDispWorkstore()) ) {
				continue;
			}
			for ( PmsDurationConversion pmsDurationConversion : inPmsDurationConversionList ) {
				if ( !pmsDurationConversion.isMarkDelete() ) {
					updatePmsDurationConversionList.add(createPmsDurationConversion(pmsDurationConversion, pmsPcuMapping));
				}
			}
		}
		
		for ( PmsDurationConversionItem pmsDurationConversionItem : durationConversionOtherWorkstoreList ) {
			if ( pmsDurationConversionItem.isSameWorkstoreGroup() ) {
				for ( PmsDurationConversion pmsDurationConversion : pmsDurationConversionItem.getPmsDurationConversionList() ) {
					pmsDurationConversion.setMarkDelete(true);
					updatePmsDurationConversionList.add(pmsDurationConversion);
				}
			}
		}
		
		if ( applyAllWorkstore ) { 
			//other workstore group
			for ( PmsPcuMapping pmsPcuMapping : pmsPcuMappingAllWorkstoreList ) {
				if ( pmsPcuMappingMap.containsKey(pmsPcuMapping.getCompId().getDispHospCode() + pmsPcuMapping.getCompId().getDispWorkstore() ) ) {
					continue;
				}
				
				List<PmsPcuMapping> otherGroupPcuMappingList = dmsPmsServiceProxy.retrievePmsPcuMappingList(pmsPcuMapping.getCompId().getDispHospCode(),  
																										pmsPcuMapping.getCompId().getDispWorkstore());
				
				List<PmsDurationConversion> pmsDurationConversionList = dmsPmsServiceProxy.retrievePmsDurationConversionList(pmsPcuMapping.getCompId().getDispHospCode(), 
																												pmsPcuMapping.getCompId().getDispWorkstore(), 
																												qtyDurationConversionDmDrugLite.getItemCode());
				
				Map<String, PmsPcuMapping> otherGroupPcuMappingMap = new HashMap<String, PmsPcuMapping>();
				
				for ( PmsPcuMapping otherGroupPcuMapping : otherGroupPcuMappingList ) {
					otherGroupPcuMappingMap.put(otherGroupPcuMapping.getCompId().getDispHospCode() + otherGroupPcuMapping.getCompId().getDispWorkstore(), otherGroupPcuMapping);
					if ( pmsPcuMappingMap.containsKey(otherGroupPcuMapping.getCompId().getDispHospCode() + otherGroupPcuMapping.getCompId().getDispWorkstore() ) ) {
						continue;
					}
					pmsPcuMappingMap.put(otherGroupPcuMapping.getCompId().getDispHospCode() + otherGroupPcuMapping.getCompId().getDispWorkstore(), otherGroupPcuMapping);
					for ( PmsDurationConversion pmsDurationConversion : inPmsDurationConversionList ) {
						if ( !pmsDurationConversion.isMarkDelete() ) {
							updatePmsDurationConversionList.add(createPmsDurationConversion(pmsDurationConversion, otherGroupPcuMapping));
						}
					}
				}
				
				if ( !pmsDurationConversionList.isEmpty() ) {
					for ( PmsDurationConversion pmsDurationConversion : pmsDurationConversionList ) {
						if ( !otherGroupPcuMappingMap.containsKey(pmsDurationConversion.getDispHospCode() + pmsDurationConversion.getDispWorkstore() ) ) {
							continue;
						}
						pmsDurationConversion.setMarkDelete(true);
						updatePmsDurationConversionList.add(pmsDurationConversion);
					}
				}
			}
		}

		auditLogger.log( "#1002", serializer.serialize(updatePmsDurationConversionList) );
		return updatePmsDurationConversionList;
	}
	
	private PmsQtyConversion createPmsQtyConversion(PmsQtyConversion inPmsQtyConversion, PmsPcuMapping pmsPcuMapping){
		PmsQtyConversion pmsQtyConversion = new PmsQtyConversion();
		pmsQtyConversion.setDispHospCode(pmsPcuMapping.getCompId().getDispHospCode());
		pmsQtyConversion.setDispWorkstore(pmsPcuMapping.getCompId().getDispWorkstore());
		pmsQtyConversion.setItemCode(inPmsQtyConversion.getItemCode());
		pmsQtyConversion.setQtyFrom(inPmsQtyConversion.getQtyFrom());
		pmsQtyConversion.setQtyTo(inPmsQtyConversion.getQtyTo());
		pmsQtyConversion.setDispenseDosageUnit(inPmsQtyConversion.getDispenseDosageUnit());
		pmsQtyConversion.setConvertQty(inPmsQtyConversion.getConvertQty());
		pmsQtyConversion.setConvertBaseUnit(inPmsQtyConversion.getConvertBaseUnit());
		pmsQtyConversion.setExtrapolateFlag(inPmsQtyConversion.getExtrapolateFlag());
		return pmsQtyConversion;
	}
	
	private PmsDurationConversion createPmsDurationConversion(PmsDurationConversion inPmsDurationConversion, PmsPcuMapping pmsPcuMapping){
		PmsDurationConversion pmsDurationConversion = new PmsDurationConversion();
		pmsDurationConversion.setDispHospCode(pmsPcuMapping.getCompId().getDispHospCode());
		pmsDurationConversion.setDispWorkstore(pmsPcuMapping.getCompId().getDispWorkstore());
		pmsDurationConversion.setItemCode(inPmsDurationConversion.getItemCode());
		pmsDurationConversion.setDurationFrom(inPmsDurationConversion.getDurationFrom());
		pmsDurationConversion.setDurationTo(inPmsDurationConversion.getDurationTo());
		pmsDurationConversion.setDurationUnit(inPmsDurationConversion.getDurationUnit());
		pmsDurationConversion.setConvertQty(inPmsDurationConversion.getConvertQty());
		pmsDurationConversion.setConvertBaseUnit(inPmsDurationConversion.getConvertBaseUnit());
		pmsDurationConversion.setExtrapolateFlag(inPmsDurationConversion.getExtrapolateFlag());
		return pmsDurationConversion;
	}
	
	public boolean isRetrieveCapdItem() {
		return retrieveCapdItem;
	}

	public boolean isSaveSuccess() {
		return saveSuccess;
	}

	private void clearList()
	{
		qtyDurationConversionDmDrugLite = null;
		qtyConversionList = null;
		qtyConversionOtherWorkstoreList = null;
		durationConversionList = null;
		durationConversionOtherWorkstoreList = null;
		selectedPmsPcuMapping = null;
	}
	
	@Remove
	public void destroy() 
	{
		if (qtyConversionList != null) {
			qtyConversionList = null;
		}
		if (pmsPcuMappingList != null) {
			pmsPcuMappingList = null;
		}
		if (qtyConversionOtherWorkstoreList != null) {
			qtyConversionOtherWorkstoreList = null;
		}
		if ( pmsPcuMappingAllWorkstoreList != null ) {
			pmsPcuMappingAllWorkstoreList = null;
		}

		if ( qtyDurationConversionDmDrugLite != null ) {
			qtyDurationConversionDmDrugLite = null;
		}
	}

}
