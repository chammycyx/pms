package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmFormCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmForm;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("dmFormService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DmFormServiceBean implements DmFormServiceLocal {

	@Logger
	private Log logger;

	@In
	private DmFormCacherInf dmFormCacher;

	@Out(required = false)
	private DmForm dmForm;
	
	public void retrieveDmForm(String formCode)
	{
		logger.debug("retrieveDmForm #0", formCode);
	
		dmForm = dmFormCacher.getFormByFormCode(formCode);
	}
	
	@Remove
	public void destroy() 
	{
		if (dmForm != null) {
			dmForm = null;
		}
	}

}
