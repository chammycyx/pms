package hk.org.ha.model.pms.vo.prevet;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PreVetInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Integer itemNum;
	
	private String preVetNoteText;
	
	private String preVetNoteUser;
	
	private Date preVetNoteDate;

	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public String getPreVetNoteText() {
		return preVetNoteText;
	}

	public void setPreVetNoteText(String preVetNoteText) {
		this.preVetNoteText = preVetNoteText;
	}
	
	public String getPreVetNoteUser() {
		return preVetNoteUser;
	}

	public void setPreVetNoteUser(String preVetNoteUser) {
		this.preVetNoteUser = preVetNoteUser;
	}

	public Date getPreVetNoteDate() {
		return preVetNoteDate;
	}

	public void setPreVetNoteDate(Date preVetNoteDate) {
		this.preVetNoteDate = preVetNoteDate;
	}
	
}