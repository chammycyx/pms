package hk.org.ha.model.pms.vo.refill;

import hk.org.ha.model.pms.udt.refill.RefillScheduleActionType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class RefillPrescInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String medOrderNum;
	
	private Integer currentGroupNum;
	
	private RefillScheduleActionType refillActionType;
	
	//for reCalulate and Display
	private Integer refillInterval;
	
	private Integer lastIntervalMaxDay;
	
	private Integer threshold;
	
	private Integer drsRefillInterval;
	
	private Integer drsLastIntervalMaxDay;
	
	private Integer drsThreshold;
	
	//for orgWorkstore
	private String orgHospCode;
	
	private String orgWorkstoreCode;
	
	private String sourceView;
		
	private Long refillScheduleId;
	
	private Integer lateRefillDays;
	
	private Boolean urgentFlag;
	
	private Long currDispOrderId;
	
	private Long currDispOrderVersion;
	
	private Map<Integer, String> refillDiscontinueMessageTlf;
	
	private List<String> infoMsgCodeList;
	
	private List<String[]> infoMsgParamList;
	
	private Boolean drsFlag;

	public RefillPrescInfo(){
		medOrderNum = "<New Order>";
		currentGroupNum = 1;
		lateRefillDays = 0;
		urgentFlag = false;
		drsFlag = Boolean.FALSE;
		infoMsgCodeList = new ArrayList<String>();
		infoMsgParamList = new ArrayList<String[]>();
	}

	public String getMedOrderNum() {
		return medOrderNum;
	}

	public void setMedOrderNum(String medOrderNum) {
		this.medOrderNum = medOrderNum;
	}

	public Integer getCurrentGroupNum() {
		return currentGroupNum;
	}

	public void setCurrentGroupNum(Integer currentGroupNum) {
		this.currentGroupNum = currentGroupNum;
	}

	public Integer getRefillInterval() {
		return refillInterval;
	}

	public void setRefillInterval(Integer refillInterval) {
		this.refillInterval = refillInterval;
	}

	public Integer getLastIntervalMaxDay() {
		return lastIntervalMaxDay;
	}

	public void setLastIntervalMaxDay(Integer lastIntervalMaxDay) {
		this.lastIntervalMaxDay = lastIntervalMaxDay;
	}

	public Integer getThreshold() {
		return threshold;
	}

	public void setThreshold(Integer threshold) {
		this.threshold = threshold;
	}

	public Integer getDrsRefillInterval() {
		return drsRefillInterval;
	}

	public void setDrsRefillInterval(Integer drsRefillInterval) {
		this.drsRefillInterval = drsRefillInterval;
	}

	public Integer getDrsLastIntervalMaxDay() {
		return drsLastIntervalMaxDay;
	}

	public void setDrsLastIntervalMaxDay(Integer drsLastIntervalMaxDay) {
		this.drsLastIntervalMaxDay = drsLastIntervalMaxDay;
	}

	public Integer getDrsThreshold() {
		return drsThreshold;
	}

	public void setDrsThreshold(Integer drsThreshold) {
		this.drsThreshold = drsThreshold;
	}

	public String getOrgHospCode() {
		return orgHospCode;
	}

	public void setOrgHospCode(String orgHospCode) {
		this.orgHospCode = orgHospCode;
	}

	public String getOrgWorkstoreCode() {
		return orgWorkstoreCode;
	}

	public void setOrgWorkstoreCode(String orgWorkstoreCode) {
		this.orgWorkstoreCode = orgWorkstoreCode;
	}

	public String getSourceView() {
		return sourceView;
	}
	
	public void setSourceView(String sourceView) {
		this.sourceView = sourceView;
	}

	public Long getRefillScheduleId() {
		return refillScheduleId;
	}
	
	public void setRefillScheduleId(Long refillScheduleId) {
		this.refillScheduleId = refillScheduleId;
	}

	public Integer getLateRefillDays() {
		return lateRefillDays;
	}
	
	public void setLateRefillDays(Integer lateRefillDays) {
		this.lateRefillDays = lateRefillDays;
	}

	public void setUrgentFlag(Boolean urgentFlag) {
		this.urgentFlag = urgentFlag;
	}

	public Boolean getUrgentFlag() {
		return urgentFlag;
	}

	public void setCurrDispOrderId(Long currDispOrderId) {
		this.currDispOrderId = currDispOrderId;
	}

	public Long getCurrDispOrderId() {
		return currDispOrderId;
	}

	public void setCurrDispOrderVersion(Long currDispOrderVersion) {
		this.currDispOrderVersion = currDispOrderVersion;
	}

	public Long getCurrDispOrderVersion() {
		return currDispOrderVersion;
	}

	public void setRefillDiscontinueMessageTlf(
			Map<Integer, String> refillDiscontinueMessageTlf) {
		this.refillDiscontinueMessageTlf = refillDiscontinueMessageTlf;
	}

	public Map<Integer, String> getRefillDiscontinueMessageTlf() {
		return refillDiscontinueMessageTlf;
	}
	
	public void addInfoMsg(String code) {
		addInfoMsg(code, null);
	}
	
	public void addInfoMsg(String code, String[] param) {
		infoMsgCodeList.add(code);
		infoMsgParamList.add(param);
	}
	
	public List<String> getInfoMsgCodeList() {
		return infoMsgCodeList;
	}
	
	public List<String[]> getInfoMsgParamList() {
		return infoMsgParamList;
	}

	public RefillScheduleActionType getRefillActionType() {
		return refillActionType;
	}

	public void setRefillActionType(RefillScheduleActionType refillActionType) {
		this.refillActionType = refillActionType;
	}

	public Boolean getDrsFlag() {
		return drsFlag;
	}

	public void setDrsFlag(Boolean drsFlag) {
		this.drsFlag = drsFlag;
	}
}