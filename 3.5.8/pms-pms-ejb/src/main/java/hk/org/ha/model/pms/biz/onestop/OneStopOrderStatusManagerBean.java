package hk.org.ha.model.pms.biz.onestop;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.udt.disp.DispOrderDayEndStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
import hk.org.ha.model.pms.vo.onestop.OneStopOrderItemSummary;
import hk.org.ha.model.pms.vo.onestop.OneStopRefillItemSummary;
import hk.org.ha.model.pms.vo.onestop.OneStopSummary;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;

@AutoCreate
@Stateless
@Name("oneStopOrderStatusManager")
@MeasureCalls
public class OneStopOrderStatusManagerBean implements OneStopOrderStatusManagerLocal {
	
	@PersistenceContext
	private EntityManager em;
		
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private OneStopSummary oneStopSummary;
	
	public String retrieveLatestOrderStatusFromLockingCheck(Long medOrderId, Long refillId)
	{
		if (refillId != null)
		{
			return verifyRefillOrderStatusBeforeSwitchToVetting(refillId);
		}
		else
		{
			OneStopOrderItemSummary oneStopOrderItemSummary = retrieveOnestopOrderItemSummaryByMedOrderId(medOrderId);
										
			if (oneStopOrderItemSummary.getDispOrderId() == null) //medOrder checking flow
			{
				MedOrder orignalMedOrder = em.find(MedOrder.class, medOrderId);
				return retriveLatestMedOrderStatus(orignalMedOrder);
			}
			else //dispOrder checking flow
			{
				DispOrder orignalDispOrder = em.find(DispOrder.class, oneStopOrderItemSummary.getDispOrderId());
				MedOrder orignalMedOrder = orignalDispOrder.getPharmOrder().getMedOrder();
				
				//special flow for day end, dispOrder will not mark deleted when cancelled
				if (orignalMedOrder.getDocType() == MedOrderDocType.Normal && orignalDispOrder.getDayEndStatus() != DispOrderDayEndStatus.None)
				{
					if (orignalMedOrder.getStatus() == MedOrderStatus.Deleted)
					{
						return "0604";
					}
					
					return "0590";
				}
				else
				{
					if (orignalDispOrder.getStatus() == DispOrderStatus.Deleted)
					{
						return "0604";
					}
					else if (orignalDispOrder.getStatus() == DispOrderStatus.SysDeleted)
					{
						return retrieveLatestOrderStatus(orignalDispOrder, orignalDispOrder.getId(), OneStopOrderType.DispOrder);
					}
					else
					{
						return "0590";
					}
				}
			}
		}
	}
	
	public String verifyMedOrderStatusFromPreVetOperation(MedOrder currentMedOrder)
	{
		MedOrder latestMedOrder = em.find(MedOrder.class, currentMedOrder.getId());
		
		if (!latestMedOrder.getVersion().equals(currentMedOrder.getVersion())) 
		{
			return retriveLatestMedOrderStatus(latestMedOrder);
		}
		
		return null;
	}
	
	public String verifyMedOrderStatusBeforeSwitchToVetting(Long medOrderId)
	{
		OneStopOrderItemSummary oneStopOrderItemSummary = retrieveOnestopOrderItemSummaryByMedOrderId(medOrderId);
		MedOrder updatedMedOrder = em.find(MedOrder.class, medOrderId);
		if (!updatedMedOrder.getVersion().equals(oneStopOrderItemSummary.getMedOrderVersion())) 
		{
			return retriveLatestMedOrderStatus(updatedMedOrder);
		}
		
		return null;
	}
	
	private String retriveLatestMedOrderStatus(MedOrder latestMedOrder) {
		if (latestMedOrder.getStatus() == MedOrderStatus.Complete)
		{
			return "0605";
		}
		else if (latestMedOrder.getStatus() == MedOrderStatus.Deleted)
		{
			return "0604";
		}
		else
		{
			return "0590";
		}
	}
	
	public String verifyDispOrderStatusBeforeSwitchToVetting(Long dispOrderId)
	{		
		OneStopOrderItemSummary oneStopOrderItemSummary = retrieveOnestopOrderItemSummaryByDispOrderId(dispOrderId);
		DispOrder updatedDispOrder = em.find(DispOrder.class, dispOrderId);
		MedOrder updatedMedOrder = updatedDispOrder.getPharmOrder().getMedOrder();
		
		String dayEndStatus = retrieveDayEndOrderStatus(updatedMedOrder, oneStopOrderItemSummary.getMedOrderVersion(), 
				updatedDispOrder, oneStopOrderItemSummary.getDispOrderVersion());

		if (dayEndStatus != null)
		{
			return dayEndStatus;
		}
		
		if (!updatedDispOrder.getVersion().equals(oneStopOrderItemSummary.getDispOrderVersion()))
		{
			if (updatedDispOrder.getStatus() == DispOrderStatus.Deleted)
			{
				return "0604";
			}
			else if (updatedDispOrder.getStatus() == DispOrderStatus.SysDeleted)
			{
				return retrieveLatestOrderStatus(updatedDispOrder, dispOrderId, OneStopOrderType.DispOrder);
			}
			else
			{
				return "0590";
			}
		}
				
		return null;	
	}
	
	public String verifyRefillOrderStatusBeforeSwitchToVetting(Long refillId)
	{	
		OneStopRefillItemSummary oneStopRefillItemSummary = retrieveOnestopRefillItemSummary(refillId);
		RefillSchedule latestRefillSchedule = em.find(RefillSchedule.class, refillId);
		
		//sfi refill not found but before existed. i.e. last record is modified and drugs are fully dispended. 
		if (latestRefillSchedule == null)
		{
			return "0590";
		}
		
		MedOrder latestMedOrder = latestRefillSchedule.getPharmOrder().getMedOrder();
		DispOrder latestDispOrder = latestRefillSchedule.getDispOrder();
		
		if (!latestMedOrder.getVersion().equals(oneStopRefillItemSummary.getMedOrderVersion()))
		{
			if (latestMedOrder.getStatus() == MedOrderStatus.Deleted)
			{
				return "0604";
			}
			else if (oneStopRefillItemSummary.getDispOrderId() == null && latestDispOrder != null)
			{
				return "0605";
			}
			else
			{
				return "0590";
			}
		}
		else if (oneStopRefillItemSummary.getDispOrderId() == null) 
		{			
			DispOrder prevDispOrder = em.find(DispOrder.class, oneStopRefillItemSummary.getPrevDispOrderId());
			
			if (!prevDispOrder.getVersion().equals(oneStopRefillItemSummary.getPrevDispOrderVersion()))
			{
				return "0590";
			}
			
			return null;
		}
		else if (oneStopRefillItemSummary.getDispOrderId() != null && 
				!latestDispOrder.getVersion().equals(oneStopRefillItemSummary.getDispOrderVersion()))
		{
			return "0590";
		}
				
		return null;
	}
	
	public String verifySuspendAllowModifyUncollectStatus(Long dispOrderId, Long dispOrderVersion)
	{
		OneStopOrderItemSummary oneStopOrderItemSummary = retrieveOnestopOrderItemSummaryByDispOrderId(dispOrderId);
		DispOrder latestDispOrder = em.find(DispOrder.class, dispOrderId);
		MedOrder latestMedOrder = latestDispOrder.getPharmOrder().getMedOrder();
		Long previousMedOrderVersion = null;
		Long previousDispOrderVersion = null;
		
		if (oneStopOrderItemSummary == null)
		{
			OneStopRefillItemSummary oneStopRefillItemSummary = retrieveOnestopRefillItemSummaryByDispOrderId(dispOrderId);
			if (oneStopRefillItemSummary != null)
			{
				previousMedOrderVersion = oneStopRefillItemSummary.getMedOrderVersion();
				previousDispOrderVersion = oneStopRefillItemSummary.getDispOrderVersion();
			}
		}
		else
		{
			previousMedOrderVersion = oneStopOrderItemSummary.getMedOrderVersion();
			previousDispOrderVersion = oneStopOrderItemSummary.getDispOrderVersion();
		}
				
		String dayEndStatus = retrieveDayEndOrderStatus(latestMedOrder, previousMedOrderVersion, 
				latestDispOrder, previousDispOrderVersion);

		if (dayEndStatus != null)
		{
			return dayEndStatus;
		}
		
		if (!latestDispOrder.getVersion().equals(dispOrderVersion))
		{				
			if (latestDispOrder.getStatus() == DispOrderStatus.Deleted)
			{
				return "0604";
			}
			else if (latestDispOrder.getStatus() == DispOrderStatus.SysDeleted)
			{
				return retrieveLatestOrderStatus(latestDispOrder, dispOrderId, 
						(oneStopOrderItemSummary != null?oneStopOrderItemSummary.getOneStopOrderType():OneStopOrderType.SfiOrder));
			}
			else
			{
				return "0590";
			}
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	private String retrieveLatestOrderStatus(DispOrder latestDispOrder, Long prevDispOrderId, OneStopOrderType oneStopOrderType)
	{
		Boolean isLatestOrder = false;
		while (!isLatestOrder) 
		{
			List<DispOrder> dispOrderList = em.createQuery(
					"select o from DispOrder o" + // 20121112 index check : DispOrder.prevDispOrderId : I_DISP_ORDER_09
					" where o.prevDispOrderId = :prevDispOrderId")
					.setParameter("prevDispOrderId", prevDispOrderId)
					.getResultList();
			
			if (!dispOrderList.isEmpty())
			{
				latestDispOrder = dispOrderList.get(0);
				prevDispOrderId = dispOrderList.get(0).getId();
			}
			else
			{
				isLatestOrder = true;
			}
		}
		
		if (latestDispOrder.getPharmOrder().getMedOrder().getStatus() == MedOrderStatus.Deleted)
		{
			return "0604";
		}
		else if (latestDispOrder.getPharmOrder().getMedOrder().getStatus() == MedOrderStatus.SysDeleted)
		{
			List<MedOrder> medOrderList = em.createQuery(
					"select o from MedOrder o" + // 20121112 index check : MedOrder.orderNum : I_MED_ORDER_01
					" where o.orderNum = :prevOrderNum" +
					" order by o.createDate desc, o.id desc")
					.setParameter("prevOrderNum", latestDispOrder.getPharmOrder().getMedOrder().getOrderNum())
					.getResultList();
							
			MedOrder latestMedOrder = medOrderList.get(0);
			
			if (latestMedOrder.getDocType() == MedOrderDocType.Normal && latestMedOrder.getStatus() == MedOrderStatus.SysDeleted)
			{
				List<MedOrder> latestMoeMedOrderList = em.createQuery(
						"select o from MedOrder o" + // 20121112 index check : MedOrder.orderNum : I_MED_ORDER_01
						" where o.orderNum = :orderNum" +
						" and o.status <> :status")
						.setParameter("orderNum", latestMedOrder.getOrderNum())
						.setParameter("status", MedOrderStatus.SysDeleted)
						.getResultList();
				
				latestMedOrder =  latestMoeMedOrderList.get(0);
			}
				
			switch (latestMedOrder.getStatus())
			{
				case Outstanding:
					return "0590";
					
				case SysDeleted:
				case Deleted:
					return "0604";
				
				case Complete:
				default:
					return "0605";
			}
		}
		else
		{
			return "0590";
		}
	}
	
	private String retrieveDayEndOrderStatus(MedOrder medOrder, Long orignalMedOrderVersion, DispOrder dispOrder, Long orignalDispOrderVersion)
	{		
		if (medOrder.getDocType() == MedOrderDocType.Normal && dispOrder.getDayEndStatus() != DispOrderDayEndStatus.None)
		{
			if (medOrder.getStatus() == MedOrderStatus.Deleted)
			{
				return "0604";
			}
			else if (!dispOrder.getVersion().equals(orignalDispOrderVersion) || 
					 (orignalMedOrderVersion != null && !medOrder.getVersion().equals(orignalMedOrderVersion)))
			{
				return "0590";
			}
		}
		
		return null;
	}
	
	private OneStopOrderItemSummary retrieveOnestopOrderItemSummaryByMedOrderId(Long medOrderId)
	{
		for (OneStopOrderItemSummary onestopOrderItemSummary : oneStopSummary.getOneStopOrderItemSummaryList())
		{
			if (onestopOrderItemSummary.getId() != null && onestopOrderItemSummary.getId().equals(medOrderId))
			{
				return onestopOrderItemSummary;
			}
		}
		
		return null;
	}
		
	private OneStopOrderItemSummary retrieveOnestopOrderItemSummaryByDispOrderId(Long dispOrderId)
	{
		for (OneStopOrderItemSummary onestopOrderItemSummary : oneStopSummary.getOneStopOrderItemSummaryList())
		{
			if (onestopOrderItemSummary.getDispOrderId() != null && onestopOrderItemSummary.getDispOrderId().equals(dispOrderId))
			{
				return onestopOrderItemSummary;
			}
		}
		
		return null;
	}
	
	private OneStopRefillItemSummary retrieveOnestopRefillItemSummaryByDispOrderId(Long dispOrderId)
	{
		for (OneStopRefillItemSummary oneStopRefillItemSummary : oneStopSummary.getOneStopRefillItemSummaryList())
		{
			if (oneStopRefillItemSummary.getDispOrderId() != null && oneStopRefillItemSummary.getRefillScheduleId().equals(dispOrderId))
			{
				return oneStopRefillItemSummary;
			}
		}
		
		return null;
	}
	
	private OneStopRefillItemSummary retrieveOnestopRefillItemSummary(Long refillId)
	{
		for (OneStopRefillItemSummary oneStopRefillItemSummary : oneStopSummary.getOneStopRefillItemSummaryList())
		{
			if (oneStopRefillItemSummary.getRefillScheduleId() != null && oneStopRefillItemSummary.getRefillScheduleId().equals(refillId))
			{
				return oneStopRefillItemSummary;
			}
		}
		
		return null;
	}
}