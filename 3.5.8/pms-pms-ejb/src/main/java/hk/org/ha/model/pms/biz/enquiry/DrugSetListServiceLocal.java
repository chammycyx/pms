package hk.org.ha.model.pms.biz.enquiry;

import hk.org.ha.model.pms.asa.exception.moe.DrugSetException;

import javax.ejb.Local;

@Local
public interface DrugSetListServiceLocal {
	
	void retrieveHospitalMappingList() throws DrugSetException;
	
	void retrieveDrugSetList(String patHospCode, Boolean getFirstDrugSet) throws DrugSetException;
	
	void retrieveDrugSetDtlList(String patHospCode, Integer drugSetNum, String drugSetCode, String drugHospCode, Boolean subLevelFlag) throws DrugSetException;
	
	void destroy();	
}
