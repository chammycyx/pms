package hk.org.ha.model.pms.biz.report;

import java.util.Date;

import javax.ejb.Local;

@Local
public interface WaitTimeRptStatManagerLocal {
	void createWaitTimeRptStat(String clusterCode, Date batchDate);	
}
