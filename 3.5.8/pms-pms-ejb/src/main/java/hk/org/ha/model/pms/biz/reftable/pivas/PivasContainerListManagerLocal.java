package hk.org.ha.model.pms.biz.reftable.pivas;

import hk.org.ha.model.pms.dms.persistence.pivas.PivasContainer;

import java.util.List;

import javax.ejb.Local;

@Local
public interface PivasContainerListManagerLocal {

	List<PivasContainer> retrievePivasContainerList();
	
	void destroy();

}
