package hk.org.ha.model.pms.vo.enquiry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
@ExternalizedBean(type=DefaultExternalizer.class)
public class DrugSet implements Serializable {

	private static final long serialVersionUID = 1L;

	private String drugHospCode;
	
	private String drugSetCode;
	
	private Integer drugSetNum;
	
	private String medicalOfficerGroup;
	
	private Boolean subLevelFlag;
	
	private List<DrugSetDtl> drugSetDtlList;

	public String getDrugHospCode() {
		return drugHospCode;
	}

	public void setDrugHospCode(String drugHospCode) {
		this.drugHospCode = drugHospCode;
	}

	public String getDrugSetCode() {
		return drugSetCode;
	}

	public void setDrugSetCode(String drugSetCode) {
		this.drugSetCode = drugSetCode;
	}

	public String getMedicalOfficerGroup() {
		return medicalOfficerGroup;
	}

	public void setMedicalOfficerGroup(String medicalOfficerGroup) {
		this.medicalOfficerGroup = medicalOfficerGroup;
	}

	public void setDrugSetDtlList(List<DrugSetDtl> drugSetDtlList) {
		this.drugSetDtlList = drugSetDtlList;
	}

	public List<DrugSetDtl> getDrugSetDtlList() {
		if ( drugSetDtlList == null ) {
			drugSetDtlList = new ArrayList<DrugSetDtl>();
		}
		return drugSetDtlList;
	}

	public void setDrugSetNum(Integer drugSetNum) {
		this.drugSetNum = drugSetNum;
	}

	public Integer getDrugSetNum() {
		return drugSetNum;
	}

	public void setSubLevelFlag(Boolean subLevelFlag) {
		this.subLevelFlag = subLevelFlag;
	}

	public Boolean getSubLevelFlag() {
		return subLevelFlag;
	}
	
}
