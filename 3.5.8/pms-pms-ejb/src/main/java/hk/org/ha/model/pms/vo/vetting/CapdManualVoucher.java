package hk.org.ha.model.pms.vo.vetting;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "CapdManualVoucher")
@ExternalizedBean(type = DefaultExternalizer.class)
public class CapdManualVoucher implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement(name="Num01")
	private String num01;

	@XmlElement(name="Num02")
	private String num02;

	@XmlElement(name="Num03")
	private String num03;

	@XmlElement(name="Num04")
	private String num04;

	@XmlElement(name="Num05")
	private String num05;

	@XmlElement(name="Num06")
	private String num06;

	@XmlElement(name="Num07")
	private String num07;

	@XmlElement(name="Num08")
	private String num08;

	@XmlElement(name="Num09")
	private String num09;

	@XmlElement(name="Num10")
	private String num10;

	public String getNum01() {
		return num01;
	}

	public void setNum01(String num01) {
		this.num01 = num01;
	}

	public String getNum02() {
		return num02;
	}

	public void setNum02(String num02) {
		this.num02 = num02;
	}

	public String getNum03() {
		return num03;
	}

	public void setNum03(String num03) {
		this.num03 = num03;
	}

	public String getNum04() {
		return num04;
	}

	public void setNum04(String num04) {
		this.num04 = num04;
	}

	public String getNum05() {
		return num05;
	}

	public void setNum05(String num05) {
		this.num05 = num05;
	}

	public String getNum06() {
		return num06;
	}

	public void setNum06(String num06) {
		this.num06 = num06;
	}

	public String getNum07() {
		return num07;
	}

	public void setNum07(String num07) {
		this.num07 = num07;
	}

	public String getNum08() {
		return num08;
	}

	public void setNum08(String num08) {
		this.num08 = num08;
	}

	public String getNum09() {
		return num09;
	}

	public void setNum09(String num09) {
		this.num09 = num09;
	}

	public String getNum10() {
		return num10;
	}

	public void setNum10(String num10) {
		this.num10 = num10;
	}
}
