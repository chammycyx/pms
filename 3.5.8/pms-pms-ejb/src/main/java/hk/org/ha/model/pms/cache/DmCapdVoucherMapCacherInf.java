package hk.org.ha.model.pms.cache;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmCapdVoucherMap;
import hk.org.ha.model.pms.udt.vetting.DmCapdVoucherMapType;

import java.util.List;

public interface DmCapdVoucherMapCacherInf extends BaseCacherInf {
	
	List<DmCapdVoucherMap> getDmCapdVoucherMapList();
	
	DmCapdVoucherMap getDmCapdVoucher(String txtEng, DmCapdVoucherMapType mapType);
	
}
