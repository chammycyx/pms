package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.persistence.reftable.FollowUpWarning;

import java.util.List;

import javax.ejb.Local;

@Local
public interface FollowUpWarningListServiceLocal {
	
	void retrieveFollowUpWarningList();
	
	void updateFollowUpWarningList(List<FollowUpWarning> followUpWarningList);
	
	List<FollowUpWarning> retrieveFollowUpWarningRptList();
	
	void printFollowUpWarningRpt();
	
	void destroy();
	
} 
