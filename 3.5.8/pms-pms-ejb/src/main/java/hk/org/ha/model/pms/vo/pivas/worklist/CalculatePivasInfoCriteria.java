package hk.org.ha.model.pms.vo.pivas.worklist;

import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.persistence.pivas.PivasWorklist;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class CalculatePivasInfoCriteria implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private PivasWorklist pivasWorklist;
	
	private List<MedProfilePoItem> mrMedProfilePoItemList;
	
	private Boolean changeSatelliteFlag;
	
	private Boolean changeSplitCountFlag;
	
	private Boolean changeAdjQtyFlag;

	private Boolean changeIssueQtyFlag;
	
	private Boolean changeSupplyDateFlag;

	private Boolean changeMaterialRequestItemFlag;
	
	private Boolean changeOtherFieldFlag;
	
	public PivasWorklist getPivasWorklist() {
		return pivasWorklist;
	}

	public void setPivasWorklist(PivasWorklist pivasWorklist) {
		this.pivasWorklist = pivasWorklist;
	}

	public List<MedProfilePoItem> getMrMedProfilePoItemList() {
		return mrMedProfilePoItemList;
	}

	public void setMrMedProfilePoItemList(List<MedProfilePoItem> mrMedProfilePoItemList) {
		this.mrMedProfilePoItemList = mrMedProfilePoItemList;
	}

	public Boolean getChangeSatelliteFlag() {
		return changeSatelliteFlag;
	}

	public void setChangeSatelliteFlag(Boolean changeSatelliteFlag) {
		this.changeSatelliteFlag = changeSatelliteFlag;
	}

	public Boolean getChangeSplitCountFlag() {
		return changeSplitCountFlag;
	}

	public void setChangeSplitCountFlag(Boolean changeSplitCountFlag) {
		this.changeSplitCountFlag = changeSplitCountFlag;
	}

	public Boolean getChangeAdjQtyFlag() {
		return changeAdjQtyFlag;
	}

	public void setChangeAdjQtyFlag(Boolean changeAdjQtyFlag) {
		this.changeAdjQtyFlag = changeAdjQtyFlag;
	}

	public Boolean getChangeIssueQtyFlag() {
		return changeIssueQtyFlag;
	}

	public void setChangeIssueQtyFlag(Boolean changeIssueQtyFlag) {
		this.changeIssueQtyFlag = changeIssueQtyFlag;
	}

	public Boolean getChangeSupplyDateFlag() {
		return changeSupplyDateFlag;
	}

	public void setChangeSupplyDateFlag(Boolean changeSupplyDateFlag) {
		this.changeSupplyDateFlag = changeSupplyDateFlag;
	}

	public Boolean getChangeMaterialRequestItemFlag() {
		return changeMaterialRequestItemFlag;
	}

	public void setChangeMaterialRequestItemFlag(Boolean changeMaterialRequestItemFlag) {
		this.changeMaterialRequestItemFlag = changeMaterialRequestItemFlag;
	}

	public Boolean getChangeOtherFieldFlag() {
		return changeOtherFieldFlag;
	}

	public void setChangeOtherFieldFlag(Boolean changeOtherFieldFlag) {
		this.changeOtherFieldFlag = changeOtherFieldFlag;
	}	
}
