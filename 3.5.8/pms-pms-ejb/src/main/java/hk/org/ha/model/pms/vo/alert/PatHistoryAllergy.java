package hk.org.ha.model.pms.vo.alert;

import java.io.Serializable;
import java.util.Date;

import hk.org.ha.model.pms.udt.alert.HistoryAction;

public class PatHistoryAllergy extends PatAllergy implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private HistoryAction action;
	
	private Date createDate;
	
	private String delReasonConvertInfo;

	public HistoryAction getAction() {
		return action;
	}

	public void setAction(HistoryAction action) {
		this.action = action;
	}

	public Date getCreateDate() {
		if (createDate == null) {
			return null;
		}
		else {
			return new Date(createDate.getTime());
		}
	}

	public void setCreateDate(Date createDate) {
		if (createDate == null) {
			this.createDate = null;
		}
		else {
			this.createDate = new Date(createDate.getTime());
		}
	}

	public String getDelReasonConvertInfo() {
		return delReasonConvertInfo;
	}

	public void setDelReasonConvertInfo(String delReasonConvertInfo) {
		this.delReasonConvertInfo = delReasonConvertInfo;
	}
}
