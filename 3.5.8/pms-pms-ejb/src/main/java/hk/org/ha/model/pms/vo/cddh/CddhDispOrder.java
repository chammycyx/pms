package hk.org.ha.model.pms.vo.cddh;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class CddhDispOrder implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String moPatientName;

	private String moPatientHkid;

	private String moPatientPatKey;
	
	private Date moPatientDob;

	private String moPatientPhone;

	private String poName;

	private Long doId;
	
	private String doHospCode;
	
	private Date doIssueDate;
	
	private Boolean doCddhExceptionFlag;

	public String getMoPatientName() {
		return moPatientName;
	}

	public void setMoPatientName(String moPatientName) {
		this.moPatientName = moPatientName;
	}

	public String getMoPatientHkid() {
		return moPatientHkid;
	}

	public void setMoPatientHkid(String moPatientHkid) {
		this.moPatientHkid = moPatientHkid;
	}

	public String getMoPatientPatKey() {
		return moPatientPatKey;
	}

	public void setMoPatientPatKey(String moPatientPatKey) {
		this.moPatientPatKey = moPatientPatKey;
	}

	public Date getMoPatientDob() {
		if (moPatientDob == null) {
			return null;
		} else {
			return new Date(moPatientDob.getTime());
		}
	}

	public void setMoPatientDob(Date moPatientDob) {
		if (moPatientDob == null) {
			this.moPatientDob = null;
		} else {
			this.moPatientDob = new Date(moPatientDob.getTime());
		}
	}

	public String getMoPatientPhone() {
		return moPatientPhone;
	}

	public void setMoPatientPhone(String moPatientPhone) {
		this.moPatientPhone = moPatientPhone;
	}

	public String getPoName() {
		return poName;
	}

	public void setPoName(String poName) {
		this.poName = poName;
	}

	public Long getDoId() {
		return doId;
	}

	public void setDoId(Long doId) {
		this.doId = doId;
	}

	public String getDoHospCode() {
		return doHospCode;
	}

	public void setDoHospCode(String doHospCode) {
		this.doHospCode = doHospCode;
	}

	public Date getDoIssueDate() {
		if (doIssueDate == null) {
			return null;
		} else {
			return new Date(doIssueDate.getTime());
		}
	}

	public void setDoIssueDate(Date doIssueDate) {
		if (doIssueDate == null) {
			this.doIssueDate = null;
		} else {
			this.doIssueDate = new Date(doIssueDate.getTime());
		}
	}

	public Boolean getDoCddhExceptionFlag() {
		return doCddhExceptionFlag;
	}

	public void setDoCddhExceptionFlag(Boolean doCddhExceptionFlag) {
		this.doCddhExceptionFlag = doCddhExceptionFlag;
	}	
}
