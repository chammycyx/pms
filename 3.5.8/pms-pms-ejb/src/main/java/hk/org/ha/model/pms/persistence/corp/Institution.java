package hk.org.ha.model.pms.persistence.corp;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.phs.PatientCat;
import hk.org.ha.model.pms.persistence.phs.Specialty;
import hk.org.ha.model.pms.persistence.phs.Ward;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;


@Entity
@Table(name = "INSTITUTION")
public class Institution extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "INST_CODE", length = 3)
	private String instCode;
	
    @ManyToOne
    @JoinColumn(name = "CLUSTER_CODE",  nullable = false)
    private HospitalCluster hospitalCluster;
    
	@Column(name = "GOPC_FLAG", nullable = false)
	private Boolean gopcFlag;
    
	@Column(name = "PCU_INST_CODE", length = 3)
	private String pucInstCode;

	@Converter(name = "Institution.status", converterClass = RecordStatus.Converter.class)
    @Convert("Institution.status")
	@Column(name="STATUS", nullable = false, length = 1)
	private RecordStatus status;
        
	@OneToMany(mappedBy = "institution")
	private List<PatientCat> patientCatList;

	@OrderBy("wardCode")
	@OneToMany(mappedBy = "institution")
	private List<Ward> wardList;

	@OrderBy("specCode")
	@OneToMany(mappedBy = "institution")
	private List<Specialty> specialtyList; 
        
	public Institution() {
		status = RecordStatus.Active;
		gopcFlag = Boolean.FALSE;
	}

	public Institution(String instCode) {
		this();
		this.instCode = instCode;
	}

	public String getInstCode() {
		return instCode;
	}

	public void setInstCode(String instCode) {
		this.instCode = instCode;
	}	
	
	public HospitalCluster getHospitalCluster() {
		return hospitalCluster;
	}

	public void setHospitalCluster(HospitalCluster hospitalCluster) {
		this.hospitalCluster = hospitalCluster;
	}
	
	public Boolean getGopcFlag() {
		return gopcFlag;
	}

	public void setGopcFlag(Boolean gopcFlag) {
		this.gopcFlag = gopcFlag;
	}
	
	public String getPucInstCode() {
		return pucInstCode;
	}

	public void setPucInstCode(String pucInstCode) {
		this.pucInstCode = pucInstCode;
	}

	public RecordStatus getStatus() {
		return status;
	}

	public void setStatus(RecordStatus status) {
		this.status = status;
	}
	
	public List<PatientCat> getPatientCatList() {
		return patientCatList;
	}

	public void setPatientCatList(List<PatientCat> patientCatList) {
		this.patientCatList = patientCatList;
	}

	public void clearPatientCatList() {
		patientCatList = new ArrayList<PatientCat>();
	}

	public List<Ward> getWardList() {
		return wardList;
	}

	public void setWardList(List<Ward> wardList) {
		this.wardList = wardList;
	}

	public void clearWardList() {
		wardList = new ArrayList<Ward>();
	}

	public List<Specialty> getSpecialtyList() {
		return specialtyList;
	}

	public void setSpecialtyList(List<Specialty> specialtyList) {
		this.specialtyList = specialtyList;
	}

	public void clearSpecialtyList() {
		specialtyList = new ArrayList<Specialty>();
	}
	
}
