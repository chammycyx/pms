package hk.org.ha.model.pms.exception.vetting;

import java.util.Map;

import org.granite.messaging.service.ExceptionConverter;
import org.granite.messaging.service.ServiceException;

public class VettingExceptionConverter implements ExceptionConverter {

	public static final String VETTING_EXCEPTION = "VettingException";
	
	@Override
	public boolean accepts(Throwable t, Throwable finalException) {
		return t.getClass().equals(VettingException.class);
	}

	@Override
	public ServiceException convert(Throwable t, String detail, Map<String, Object> extendedData) {
		ServiceException se = new ServiceException(VETTING_EXCEPTION, t.getMessage(), detail, t);
		VettingException vettingException = (VettingException) t;
		extendedData.put("messageCode", vettingException.getMessageCode());
		extendedData.put("messageParam", vettingException.getMessageParam());
		se.getExtendedData().putAll(extendedData);
		return se;
	}
}
