package hk.org.ha.model.pms.udt.pivas;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PivasInboxSortColumn implements StringValuedEnum {

	DueDate("1", "Due Date"),
	ReceivedDateDueDate("2", "Received Time & Due Date"),
	SupplyDateDueDate("3", "Supply Time & Due Date"),
	PhsWardBedNumPatName("4", "PHS Ward, Bed & Patient Name");
	
    private final String dataValue;
    private final String displayValue;

    PivasInboxSortColumn(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    } 

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static PivasInboxSortColumn dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PivasInboxSortColumn.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<PivasInboxSortColumn> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<PivasInboxSortColumn> getEnumClass() {
    		return PivasInboxSortColumn.class;
    	}
    }
}



