package hk.org.ha.model.pms.biz.report;

import java.util.Date;

import javax.ejb.Local;

@Local
public interface MpTrxRptPrintServiceLocal {
	
	void retrieveMpTrxRptListByBatchNum(String batchNum, Date dispenseDate);
	
	void generateMpTrxRpt();
	
	void printMpTrxRptForReport();
	
    void destroy();    
}