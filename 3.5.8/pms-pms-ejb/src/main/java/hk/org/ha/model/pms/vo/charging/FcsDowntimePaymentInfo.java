package hk.org.ha.model.pms.vo.charging;


import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
import hk.org.ha.model.pms.udt.charging.DowntimePaymentStatus;

@ExternalizedBean(type=DefaultExternalizer.class)
public class FcsDowntimePaymentInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private DowntimePaymentStatus fcsDowntimePaymentStatus;
	
	private String fcsReturnMessage;

	public FcsDowntimePaymentInfo(){
		setFcsDowntimePaymentStatus(DowntimePaymentStatus.Error);
		setFcsReturnMessage(null);
	}

	public void setFcsDowntimePaymentStatus(DowntimePaymentStatus fcsDowntimePaymentStatus) {
		this.fcsDowntimePaymentStatus = fcsDowntimePaymentStatus;
	}

	public DowntimePaymentStatus getFcsDowntimePaymentStatus() {
		return fcsDowntimePaymentStatus;
	}

	public void setFcsReturnMessage(String fcsReturnMessage) {
		this.fcsReturnMessage = fcsReturnMessage;
	}

	public String getFcsReturnMessage() {
		return fcsReturnMessage;
	}
}
