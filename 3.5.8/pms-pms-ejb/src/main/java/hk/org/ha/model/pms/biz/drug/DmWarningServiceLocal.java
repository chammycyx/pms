package hk.org.ha.model.pms.biz.drug;

import javax.ejb.Local;

@Local
public interface DmWarningServiceLocal {
	
	void retrieveDmWarning(String warnCode);
	
	void clearDmWarning();
	
	void destroy();
	
}
 