package hk.org.ha.model.pms.biz.alert.mds;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.AlertEntity;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedOrderItemAlert;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.udt.alert.DiseaseType;
import hk.org.ha.model.pms.vo.alert.mds.Alert;
import hk.org.ha.model.pms.vo.alert.mds.AlertAdr;
import hk.org.ha.model.pms.vo.alert.mds.AlertAllergen;
import hk.org.ha.model.pms.vo.alert.mds.AlertDdcm;
import hk.org.ha.model.pms.vo.alert.mds.AlertDdim;
import hk.org.ha.model.pms.vo.alert.mds.AlertDrcm;
import hk.org.ha.model.pms.vo.alert.mds.AlertHlaTest;
import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;
import hk.org.ha.model.pms.vo.alert.mds.MedProfileAlertMsg;
import hk.org.ha.model.pms.vo.rx.IvAdditive;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.ejb.Stateless;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;

@AutoCreate
@Stateless
@Name("alertMsgManager")
@MeasureCalls
public class AlertMsgManagerBean implements AlertMsgManagerLocal {

	private final AlertEntityComparator alertEntityComparator = new AlertEntityComparator();
	
	private final AlertComparator alertComparator = new AlertComparator();
		
	@SuppressWarnings("unused")
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private MedOrderItem mdsMedOrderItem;
		
	public AlertMsg retrieveAlertMsg(MedOrderItem medOrderItem) {

		mdsMedOrderItem = medOrderItem;
		
		for (MedOrderItemAlert moiAlert : medOrderItem.getMedOrderItemAlertList()) {
			moiAlert.getMedOrderItem();
		}
		
		List<AlertEntity> patAlertList = new ArrayList<AlertEntity>(medOrderItem.getPatAlertList());
		sortAlertEntityList(patAlertList);
		
		return new AlertMsg(
				medOrderItem.getItemNum(),
				medOrderItem.getMdsOrderDesc(),
				medOrderItem.getDisplayNameSaltForm(),
				patAlertList,
				new ArrayList<AlertEntity>(medOrderItem.getDdiAlertList())
		);
	}
	
	public MedProfileAlertMsg retrieveMedProfileAlertMsg(MedOrderItem medOrderItem) {
		
		mdsMedOrderItem = medOrderItem;
		
		for (MedOrderItemAlert moiAlert : medOrderItem.getMedOrderItemAlertList()) {
			moiAlert.getMedOrderItem();
		}
		
		List<AlertMsg> alertMsgList;
		if (medOrderItem.getRxItem().isIvRxDrug()) {
			alertMsgList = constructAlertMsgForIvRxDrug(medOrderItem);
		}
		else {
			alertMsgList = constructAlertMsgForRxDrug(medOrderItem);
		}
		
		MedProfileAlertMsg medProfileAlertMsg = new MedProfileAlertMsg();
		medProfileAlertMsg.setOrderDesc(medOrderItem.getMdsOrderDesc());
		medProfileAlertMsg.setIvRxDrug(medOrderItem.getRxItem().isIvRxDrug());
		medProfileAlertMsg.setAlertMsgList(alertMsgList);
		
		return medProfileAlertMsg;
	}
	
	private List<AlertMsg> constructAlertMsgForRxDrug(MedOrderItem medOrderItem) {
		
		List<AlertEntity> patAlertList = new ArrayList<AlertEntity>(medOrderItem.getPatAlertList());
		sortAlertEntityList(patAlertList);
		
		AlertMsg alertMsg = new AlertMsg(
				medOrderItem.getItemNum(),
				medOrderItem.getMdsOrderDesc(),
				medOrderItem.getDisplayNameSaltForm(),
				patAlertList,
				new ArrayList<AlertEntity>(medOrderItem.getDdiAlertList())
		);
		
		return Arrays.asList(alertMsg);
	}
	
	private List<AlertMsg> constructAlertMsgForIvRxDrug(MedOrderItem medOrderItem) {

		List<AlertMsg> alertMsgList = new ArrayList<AlertMsg>();

		for (IvAdditive ivAdditive : medOrderItem.getIvRxDrug().getIvAdditiveList()) {

			List<AlertEntity> patAlertList = new ArrayList<AlertEntity>();
			List<AlertEntity> ddiAlertList = new ArrayList<AlertEntity>();

			for (MedOrderItemAlert moiAlert : medOrderItem.getMedOrderItemAlertList()) {
				if (!ivAdditive.getAdditiveNum().equals(moiAlert.getAdditiveNum())) {
					continue;
				}
				if (moiAlert.isPatAlert()) {
					patAlertList.add(moiAlert);
				}
				else if (moiAlert.isDdiAlert()) {
					ddiAlertList.add(moiAlert);
				}
			}
			
			if (patAlertList.isEmpty() && ddiAlertList.isEmpty()) {
				continue;
			}
			
			sortAlertEntityList(patAlertList);
			
			alertMsgList.add(new AlertMsg(
					medOrderItem.getItemNum(),
					ivAdditive.getMdsOrderDesc(),
					ivAdditive.getDisplayNameSaltForm(),
					new ArrayList<AlertEntity>(patAlertList),
					new ArrayList<AlertEntity>(ddiAlertList)
			));
		}
		
		return alertMsgList;
	}
	
	public void sortAlertList(List<Alert> alertList) {
		Collections.sort(alertList, alertComparator);
	}
	
	public void sortAlertEntityList(List<? extends AlertEntity> alertEntityList) {
		Collections.sort(alertEntityList, alertEntityComparator);
	}
	
	private static class AlertComparator implements Comparator<Alert>, Serializable {

		private static final long serialVersionUID = 1L;

		private int getTypeSeq(Alert alert) {

			if (alert instanceof AlertAllergen) {
				return 0;
			}
			else if (alert instanceof AlertDdcm) {
				if (((AlertDdcm) alert).getDiseaseType() == DiseaseType.G6pd) {
					return 1;
				}
				else {
					return 2;
				}
			}
			else if (alert instanceof AlertAdr) {
				return 3;
			}
			else if (alert instanceof AlertHlaTest) {
				return 4;
			}
			else if (alert instanceof AlertDdim) {
				if (((AlertDdim) alert).isOnHand()) {
					return 5;
				}
				else {
					return 6;
				}
			}
			else if (alert instanceof AlertDrcm) {
				return 7;
			}
			else {
				return 8;
			}
		}
		
		public int compare(Alert alert1, Alert alert2) {

			Integer typeSeq1 = Integer.valueOf(getTypeSeq(alert1));
			Integer typeSeq2 = Integer.valueOf(getTypeSeq(alert2));

			if (!typeSeq1.equals(typeSeq2)) {
				return typeSeq1.compareTo(typeSeq2);
			}
			
			if (alert1.getSortSeq() != null && alert2.getSortSeq() != null) {
				return alert1.getSortSeq().compareTo(alert2.getSortSeq());
			}

			return Integer.valueOf(alert1.hashCode()).compareTo(Integer.valueOf(alert2.hashCode()));
		}
	}
	
	private static class AlertEntityComparator implements Comparator<AlertEntity>, Serializable {

		private static final long serialVersionUID = 1L;

		private static final AlertComparator alertComparator = new AlertComparator();

		public int compare(AlertEntity alertEntity1, AlertEntity alertEntity2) {
			return alertComparator.compare(alertEntity1.getAlert(), alertEntity2.getAlert());
		}
	}
	
	public AlertMsg retrieveMpAlertMsg(MedProfileMoItem medProfileMoItem) {
		
		List<AlertEntity> patAlertList = new ArrayList<AlertEntity>();
		for (AlertEntity alertEntity : medProfileMoItem.getPatAlertList(false)) {
			if (!alertEntity.getOverrideReason().isEmpty()) {
				patAlertList.add(alertEntity);
			}
		}
		
		sortAlertEntityList(patAlertList);
		
		return new AlertMsg(
				medProfileMoItem.getItemNum(),
				medProfileMoItem.getMdsOrderDesc(),
				medProfileMoItem.getDisplayNameSaltForm(),
				patAlertList,
				new ArrayList<AlertEntity>(medProfileMoItem.getDdiAlertList())
		);
	}

	public AlertMsg retrieveMpPatAlertMsg(MedProfileMoItem medProfileMoItem) {
		List<AlertEntity> patAlertList = new ArrayList<AlertEntity>(medProfileMoItem.getPatAlertList(false));
		sortAlertEntityList(patAlertList);
		
		return new AlertMsg(
				medProfileMoItem.getItemNum(),
				medProfileMoItem.getMdsOrderDesc(),
				medProfileMoItem.getDisplayNameSaltForm(),
				patAlertList
		);
	}

	public List<AlertMsg> retrieveMpDdiAlertMsgList(MedProfileMoItem medProfileMoItem) {

		List<AlertMsg> alertMsgList = new ArrayList<AlertMsg>();
		for (AlertEntity alertEntity : medProfileMoItem.getDdiAlertList()) {
			alertMsgList.add(new AlertMsg(
				medProfileMoItem.getItemNum(),
				medProfileMoItem.getMdsOrderDesc(),
				medProfileMoItem.getDisplayNameSaltForm(),
				null,
				Arrays.asList(alertEntity)
			));
		}
		return alertMsgList;
	}

}
