package hk.org.ha.model.pms.biz.security;
import hk.org.ha.model.pms.vo.sys.PostLogonResult;

import java.io.IOException;

import javax.ejb.Local;

@Local
public interface PostLogonServiceLocal {

	PostLogonResult postLogon(String hospCode, String workstoreCode) throws IOException;
		
	void destroy();
}
