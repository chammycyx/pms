package hk.org.ha.model.pms.persistence.medprofile;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.persistence.AlertEntity;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.disp.MedItemInf;
import hk.org.ha.model.pms.persistence.reftable.WardConfig;
import hk.org.ha.model.pms.udt.YesNoBlankFlag;
import hk.org.ha.model.pms.udt.medprofile.MedProfileMoItemStatus;
import hk.org.ha.model.pms.udt.rx.RxItemType;
import hk.org.ha.model.pms.udt.vetting.FixedConcnSource;
import hk.org.ha.model.pms.vo.chemo.ChemoInfo;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.IvAdditive;
import hk.org.ha.model.pms.vo.rx.IvRxDrug;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.PrivateOwned;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedProperty;

@Entity
@Table(name = "MED_PROFILE_MO_ITEM")
@XmlAccessorType(XmlAccessType.FIELD)
public class MedProfileMoItem extends VersionEntity implements MedProfileMoItemInf, MedItemInf {

	private static final long serialVersionUID = 1L;

	private static final String JAXB_CONTEXT_RX = "hk.org.ha.model.pms.vo.rx";

	private static final String JAXB_CONTEXT_CHEMO = "hk.org.ha.model.pms.vo.chemo";
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "medProfileMoItemSeq")
	@SequenceGenerator(name = "medProfileMoItemSeq", sequenceName = "SQ_MED_PROFILE_MO_ITEM", initialValue = 100000000)
	@XmlElement
	private Long id;
	
	@Column(name = "ORDER_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
    @XmlTransient
	private Date orderDate;
	
	@Column(name = "ORG_ITEM_NUM")
	@XmlElement
	private Integer orgItemNum;
	
	@Column(name = "ITEM_NUM", nullable = false)
	@XmlElement
	private Integer itemNum;

	@Lob
	@Column(name = "RX_ITEM_XML")
	@XmlElement
	private String rxItemXml;

    @Converter(name = "MedProfileMoItem.rxItemType", converterClass = RxItemType.Converter.class)
    @Convert("MedProfileMoItem.rxItemType")
	@Column(name = "RX_ITEM_TYPE", length = 1)
    @XmlTransient
	private RxItemType rxItemType;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "START_DATE")
    @XmlTransient
	private Date startDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "END_DATE")
    @XmlTransient
	private Date endDate;
	
	@Column(name = "SUSPEND_CODE", length = 2)
    @XmlTransient
	private String suspendCode;
	
	@Column(name = "MDS_ACK_SUSPEND_REASON", length = 44)
    @XmlTransient
	private String mdsAckSuspendReason;
	
	@Column(name = "MDS_SUSPEND_REASON", length = 44)
    @XmlTransient
	private String mdsSuspendReason;
	
	@Column(name = "SUSPEND_SUPPL_DESC", length = 255)
    @XmlTransient
	private String suspendSupplDesc;
	
	@Column(name = "PENDING_CODE", length = 2)
    @XmlTransient
	private String pendingCode;
	
	@Column(name = "PENDING_DESC", length = 44)
    @XmlTransient
	private String pendingDesc;
	
	@Column(name = "PENDING_SUPPL_DESC", length = 255)
    @XmlTransient
	private String pendingSupplDesc;

	@Column(name = "PENDING_USER", length = 12)
    @XmlTransient
	private String pendingUser;
	
	@Column(name = "PENDING_USER_NAME", length = 150)
    @XmlTransient
	private String pendingUserName;	

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "PENDING_DATE")
    @XmlTransient
	private Date pendingDate;

	@Column(name = "UNIT_OF_CHARGE")
    @XmlTransient
	private Integer unitOfCharge;
	
	@Converter(name = "MedProfileMoItem.chargeFlag", converterClass = YesNoBlankFlag.Converter.class)
	@Convert("MedProfileMoItem.chargeFlag")
	@Column(name = "CHARGE_FLAG", nullable = false, length = 1)
    @XmlTransient
	private YesNoBlankFlag chargeFlag;
	
	@Column(name = "STAT_FLAG", nullable = false)
    @XmlTransient
	private Boolean statFlag; 
	
	@Column(name = "PPMI_FLAG", nullable = false)
    @XmlTransient
	private Boolean ppmiFlag;
	
	@Column(name = "ALERT_FLAG", nullable = false)
    @XmlTransient
	private Boolean alertFlag;
	
	@Column(name = "FM_FLAG", nullable = false)
    @XmlTransient
	private Boolean fmFlag;
	
    @Column(name = "DOCTOR_CODE", length = 12)
    @XmlTransient
    private String doctorCode;

	@Column(name = "DOCTOR_RANK_CODE", length = 10)
    @XmlTransient
	private String doctorRankCode;
    
    @Column(name = "DOCTOR_NAME", length = 48)
    @XmlTransient
    private String doctorName;
    
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FIRST_PRINT_DATE")
    @XmlTransient
	private Date firstPrintDate;
	
	@Column(name = "URGENT_FLAG", nullable = false)
	@XmlElement
	private Boolean urgentFlag;

	@Column(name = "URGENT_NUM")
    @XmlTransient
	private Integer urgentNum;
	
    @Column(name = "PRINT_INSTRUCTION_FLAG", nullable = false)
    @XmlTransient
    private Boolean printInstructionFlag;
    
    @Converter(name = "MedProfileMoItem.status", converterClass = MedProfileMoItemStatus.Converter.class)
    @Convert("MedProfileMoItem.status")
    @Column(name = "STATUS", nullable = false, length = 1)
	@XmlElement
    private MedProfileMoItemStatus status;
    
	@Column(name = "OVERRIDE_DESC", length = 255)
    @XmlTransient
	private String overrideDesc;
    
    @Column(name = "OVERRIDE_DOCTOR_CODE", length = 12)
    @XmlTransient
    private String overrideDoctorCode;

	@Column(name = "OVERRIDE_DOCTOR_RANK_CODE", length = 10)
    @XmlTransient
	private String overrideDoctorRankCode;
    
    @Column(name = "OVERRIDE_DOCTOR_NAME", length = 48)
    @XmlTransient
    private String overrideDoctorName;
    
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "OVERRIDE_DATE")
    @XmlTransient
	private Date overrideDate;
	
    @Column(name = "RESUME_PENDING_REMARK", length = 255)
    @XmlTransient
    private String resumePendingRemark;
    
    @Column(name = "TRANSFER_FLAG")
    @XmlTransient
    private Boolean transferFlag;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TRANSFER_DATE")
    @XmlTransient
    private Date transferDate;
    
	@Column(name = "UNIT_DOSE_EXCEPT_FLAG")
    @XmlTransient
	private Boolean unitDoseExceptFlag;
    
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_ALERT_DATE")
    @XmlTransient
	private Date lastAlertDate;

	@Column(name = "VERBAL_FLAG")
    @XmlTransient
	private Boolean verbalFlag;    
	
	@Column(name = "VERIFY_BEFORE_FLAG")
    @XmlTransient
	private Boolean verifyBeforeFlag;

	@Column(name = "PIVAS_FORMULA_ID")
    @XmlTransient
	private Long pivasFormulaId;

	@Column(name = "PIVAS_FLAG")
    @XmlTransient
	private Boolean pivasFlag;
	
	@Column(name = "FROZEN_FLAG")
    @XmlTransient
	private Boolean frozenFlag;
	
	@Column(name = "MODIFY_FLAG")
    @XmlTransient
	private Boolean modifyFlag;
	
	@Column(name = "CYCLE")
	@XmlElement
	private Integer cycle;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CYCLE_START_DATE")
	@XmlElement
	private Date cycleStartDate;

    @Column(name = "CHEMO_CATEGORY", length = 1)
	@XmlElement
    private String chemoCategory;
	
	@Lob
	@Column(name = "CHEMO_INFO_XML")
	@XmlElement
	private String chemoInfoXml;

    @Column(name = "CMM_FORMULA_CODE", length = 20)
	@XmlElement
    private String cmmFormulaCode;
	
    @PrivateOwned
    @OneToMany(mappedBy="medProfileMoItem", cascade=CascadeType.ALL)
    @XmlTransient
	private List<MedProfileMoItemFm> medProfileMoItemFmList;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MED_PROFILE_ITEM_ID")
    @XmlTransient
	private MedProfileItem medProfileItem;
	
	@Column(name = "MED_PROFILE_ITEM_ID", insertable = false, updatable = false)
    @XmlTransient
	private Long medProfileItemId;	
		
	@PrivateOwned
	@OrderBy("id")
	@OneToMany(mappedBy = "medProfileMoItem", cascade = CascadeType.ALL)
    @XmlTransient
	private List<MedProfilePoItem> medProfilePoItemList;

	@PrivateOwned
	@OrderBy("sortSeq")
	@OneToMany(mappedBy = "medProfileMoItem", cascade = CascadeType.ALL)
    @XmlTransient
	private List<MedProfileMoItemAlert> medProfileMoItemAlertList;
	
	@Transient
    @XmlTransient
	private RxItem rxItem;
	
	@Transient
    @XmlTransient
	private Boolean mealFlag;
	
	@Transient
    @XmlTransient
	private Boolean printOrderPendingForm;
	
	@Transient
    @XmlTransient
	private Boolean uniqueDrugFlag;
	
	@Transient
    @XmlTransient
	private Integer seqNum;
	
	@Transient
    @XmlTransient
	private List<Long> relatedMedOrderItemIdList;
	
	@Transient
    @XmlTransient
	private Boolean firstItemFlag;
	
	@Transient
    @XmlTransient
	private boolean firstManualItem;

	@Transient
    @XmlTransient
	private ChemoInfo chemoInfo;
	
	@Transient
	@XmlTransient
	private boolean treatmentDaySelected;
	
	public MedProfileMoItem() {
		chargeFlag = YesNoBlankFlag.Blank;
		statFlag = Boolean.FALSE;
		ppmiFlag = Boolean.FALSE;
		mealFlag = Boolean.FALSE;
		printOrderPendingForm = Boolean.FALSE;
		alertFlag = Boolean.FALSE;
		fmFlag = Boolean.FALSE;
		status = MedProfileMoItemStatus.Normal;
    	urgentFlag = Boolean.FALSE;
    	printInstructionFlag = Boolean.FALSE;
    	uniqueDrugFlag = Boolean.TRUE;
    	transferFlag = Boolean.FALSE;
    	unitDoseExceptFlag = Boolean.FALSE;
    	verbalFlag = Boolean.FALSE;
    	firstItemFlag = Boolean.FALSE;
    	verifyBeforeFlag = Boolean.FALSE;
    	pivasFlag = Boolean.FALSE;
    	frozenFlag = Boolean.FALSE;
    	modifyFlag = false;
    	treatmentDaySelected = false;
	}
	
	@PostLoad
	public void postLoad() {
		
    	if (rxItemXml != null) {
    		JaxbWrapper<RxItem> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
    		rxItem = (RxItem) rxJaxbWrapper.unmarshall(rxItemXml);
    		rxItem.buildTlf();
    	}
    	
    	if (chemoInfoXml != null) {
    		JaxbWrapper<ChemoInfo> chemoJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_CHEMO);
    		chemoInfo = (ChemoInfo) chemoJaxbWrapper.unmarshall(chemoInfoXml);
    	}    	
	}
	 
	public void clearDmInfo() {
		if (rxItem != null) {
			if (rxItem.isRxDrug()) {
				((RxDrug) rxItem).clearDmInfo();
			} else if (rxItem.isIvRxDrug()) {
				IvRxDrug ivRxDrug = (IvRxDrug)rxItem;
				for (IvAdditive ivAdditive : ivRxDrug.getIvAdditiveList()) {
					ivAdditive.clearDmInfo();
				}
			}
		}
    }
	
	public void loadDmInfo() {
		if (rxItem != null) {
			this.getMedProfileItem();
			if (rxItem.isRxDrug()) {
				((RxDrug) rxItem).loadDmInfo(null);
			} else if (rxItem.isIvRxDrug()) {
				IvRxDrug ivRxDrug = (IvRxDrug)rxItem;
				for (IvAdditive ivAdditive : ivRxDrug.getIvAdditiveList()) {
					ivAdditive.loadDmInfo(null);
				}
			}
		}
	}
	
	@PrePersist
    @PreUpdate
    public void preSave() {    	
    	 if (rxItem != null) {
     		 JaxbWrapper<RxItem> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
    		 rxItemXml = rxJaxbWrapper.marshall(rxItem);
    	 }
    	 
    	 if (chemoInfo != null) {
     		JaxbWrapper<ChemoInfo> chemoJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_CHEMO);
     		chemoInfoXml = chemoJaxbWrapper.marshall(chemoInfo);
    	 }
    }
	
	public void clearXml() {
		rxItemXml = null;
		chemoInfoXml = null;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	
	public Integer getOrgItemNum() {
		return orgItemNum;
	}

	public void setOrgItemNum(Integer orgItemNum) {
		this.orgItemNum = orgItemNum;
	}

	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public String getRxItemXml() {
		return rxItemXml;
	}

	public void setRxItemXml(String rxItemXml) {
		this.rxItemXml = rxItemXml;
	}
	
	public RxItemType getRxItemType() {
		return rxItemType;
	}

	public void setRxItemType(RxItemType rxItemType) {
		this.rxItemType = rxItemType;
	}

	public Date getStartDate() {
		return startDate == null ? null : new Date(startDate.getTime());
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate == null ? null : new Date(startDate.getTime());
	}
	
	public Date getEndDate() {
		return endDate == null ? null : new Date(endDate.getTime());
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate == null ? null : new Date(endDate.getTime());
	}

	public String getSuspendCode() {
		return suspendCode;
	}

	public void setSuspendCode(String suspendCode) {
		this.suspendCode = suspendCode;
	}
	
	public String getMdsAckSuspendReason() {
		return mdsAckSuspendReason;
	}

	public void setMdsAckSuspendReason(String mdsAckSuspendReason) {
		this.mdsAckSuspendReason = mdsAckSuspendReason;
	}

	public String getMdsSuspendReason() {
		return mdsSuspendReason;
	}

	public void setMdsSuspendReason(String mdsSuspendReason) {
		this.mdsSuspendReason = mdsSuspendReason;
	}

	public String getSuspendSupplDesc() {
		return suspendSupplDesc;
	}

	public void setSuspendSupplDesc(String suspendSupplDesc) {
		this.suspendSupplDesc = suspendSupplDesc;
	}
	
	public String getPendingCode() {
		return pendingCode;
	}

	public void setPendingCode(String pendingCode) {
		this.pendingCode = pendingCode;
	}

	public String getPendingDesc() {
		return pendingDesc;
	}

	public void setPendingDesc(String pendingDesc) {
		this.pendingDesc = pendingDesc;
	}

	public String getPendingSupplDesc() {
		return pendingSupplDesc;
	}

	public void setPendingSupplDesc(String pendingSupplDesc) {
		this.pendingSupplDesc = pendingSupplDesc;
	}

	public String getPendingUser() {
		return pendingUser;
	}

	public void setPendingUser(String pendingUser) {
		this.pendingUser = pendingUser;
	}
	
	public String getPendingUserName() {
		return pendingUserName;
	}

	public void setPendingUserName(String pendingUserName) {
		this.pendingUserName = pendingUserName;
	}

	public Date getPendingDate() {
		return pendingDate == null ? null : new Date(pendingDate.getTime());
	}

	public void setPendingDate(Date pendingDate) {
		this.pendingDate = pendingDate == null ? null : new Date(pendingDate.getTime());
	}
	
	public Integer getUnitOfCharge() {
		return unitOfCharge;
	}

	public void setUnitOfCharge(Integer unitOfCharge) {
		this.unitOfCharge = unitOfCharge;
	}

	public YesNoBlankFlag getChargeFlag() {
		return chargeFlag;
	}

	public void setChargeFlag(YesNoBlankFlag chargeFlag) {
		this.chargeFlag = chargeFlag;
	}
	
	public Boolean getStatFlag() {
		return statFlag;
	}

	public void setStatFlag(Boolean statFlag) {
		this.statFlag = statFlag;
	}

	public Boolean getPpmiFlag() {
		return ppmiFlag;
	}

	public void setPpmiFlag(Boolean ppmiFlag) {
		this.ppmiFlag = ppmiFlag;
	}
	
	public Boolean getAlertFlag() {
		return alertFlag;
	}

	public void setAlertFlag(Boolean alertFlag) {
		this.alertFlag = alertFlag;
	}
	
	public Boolean getFmFlag() {
		return fmFlag;
	}

	public void setFmFlag(Boolean fmFlag) {
		this.fmFlag = fmFlag;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public String getDoctorRankCode() {
		return doctorRankCode;
	}

	public void setDoctorRankCode(String doctorRankCode) {
		this.doctorRankCode = doctorRankCode;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}
	
	public Date getFirstPrintDate() {
		return firstPrintDate == null ? null : new Date(firstPrintDate.getTime());
	}

	public void setFirstPrintDate(Date firstPrintDate) {
		this.firstPrintDate = firstPrintDate == null ? null : new Date(firstPrintDate.getTime());
	}

	public Boolean getUrgentFlag() {
		return urgentFlag;
	}

	public void setUrgentFlag(Boolean urgentFlag) {
		this.urgentFlag = urgentFlag;
	}
	
	public Integer getUrgentNum() {
		return urgentNum;
	}

	public void setUrgentNum(Integer urgentNum) {
		this.urgentNum = urgentNum;
	}

	public Boolean getPrintInstructionFlag() {
		return printInstructionFlag;
	}

	public void setPrintInstructionFlag(Boolean printInstructionFlag) {
		this.printInstructionFlag = printInstructionFlag;
	}
	
	public void markDefaultPrintInstructionFlag(WardConfig wardConfig) {
		setPrintInstructionFlag(
				wardConfig == null ? Boolean.FALSE : getCanSetLabelInstruction() && Boolean.TRUE.equals(wardConfig.getPrintDosageInstructionFlag()));
	}
	
	public MedProfileMoItemStatus getStatus() {
		return status;
	}

	public void setStatus(MedProfileMoItemStatus status) {
		this.status = status;
	}

	public List<MedProfileMoItemFm> getMedProfileMoItemFmList() {
		if (medProfileMoItemFmList == null) {
			medProfileMoItemFmList = new ArrayList<MedProfileMoItemFm>();
		}
		return medProfileMoItemFmList;
	}

	public void setMedProfileMoItemFmList(List<MedProfileMoItemFm> medProfileMoItemFmList) {
		this.medProfileMoItemFmList = medProfileMoItemFmList;
	}

	public void addMedProfileMoItemFm(MedProfileMoItemFm medProfileMoItemFm) {
		medProfileMoItemFm.setMedProfileMoItem(this);
		getMedProfileMoItemFmList().add(medProfileMoItemFm);
	}
	
	public void clearMedProfileMoItemFmList() {
		medProfileMoItemFmList = new ArrayList<MedProfileMoItemFm>();
	}
	
	public List<MedProfilePoItem> getMedProfilePoItemList() {
		if (medProfilePoItemList == null) {
			medProfilePoItemList = new ArrayList<MedProfilePoItem>();
		}
		return medProfilePoItemList;
	}

	public void setMedProfilePoItemList(List<MedProfilePoItem> medProfilePoItemList) {
		this.medProfilePoItemList = medProfilePoItemList;
	}
	
	public void addMedProfilePoItem(MedProfilePoItem medProfilePoItem) {
		medProfilePoItem.setMedProfileMoItem(this);
		getMedProfilePoItemList().add(medProfilePoItem);
	}
	
	public void clearMedProfilePoItemList() {
		medProfilePoItemList = new ArrayList<MedProfilePoItem>();
	}

	public List<MedProfileMoItemAlert> getMedProfileMoItemAlertList() {
		if (medProfileMoItemAlertList == null) {
			medProfileMoItemAlertList = new ArrayList<MedProfileMoItemAlert>();
		}
		return medProfileMoItemAlertList;
	}
	
	public void setMedProfileMoItemAlertList(List<MedProfileMoItemAlert> medProfileMoItemAlertList) {
		this.medProfileMoItemAlertList = medProfileMoItemAlertList;
	}
	
	public void addMedProfileMoItemAlert(MedProfileMoItemAlert medProfileMoItemAlert) {
		medProfileMoItemAlert.setMedProfileMoItem(this);
		getMedProfileMoItemAlertList().add(medProfileMoItemAlert);
	}
	
	public void clearMedProfileMoItemAlertList() {
		medProfileMoItemAlertList = new ArrayList<MedProfileMoItemAlert>();
	}
	
	public MedProfileItem getMedProfileItem() {
		return medProfileItem;
	}
	
	public void setMedProfileItem(MedProfileItem medProfileItem) {
		this.medProfileItem = medProfileItem;
	}
	
	public Long getMedProfileItemId() {
		return medProfileItemId;
	}

	public void setMedProfileItemId(Long medProfileItemId) {
		this.medProfileItemId = medProfileItemId;
	}

	public RxItem getRxItem() {
		return rxItem;
	}

	public void setRxItem(RxItem rxItem) {
		this.rxItem = rxItem;
	}

	public Boolean getMealFlag() {
		return mealFlag;
	}

	public void setMealFlag(Boolean mealFlag) {
		this.mealFlag = mealFlag;
	}
	
	public String getOverrideDesc() {
		return overrideDesc;
	}

	public void setOverrideDesc(String overrideDesc) {
		this.overrideDesc = overrideDesc;
	}

	public String getOverrideDoctorCode() {
		return overrideDoctorCode;
	}

	public void setOverrideDoctorCode(String overrideDoctorCode) {
		this.overrideDoctorCode = overrideDoctorCode;
	}

	public String getOverrideDoctorRankCode() {
		return overrideDoctorRankCode;
	}

	public void setOverrideDoctorRankCode(String overrideDoctorRankCode) {
		this.overrideDoctorRankCode = overrideDoctorRankCode;
	}

	public String getOverrideDoctorName() {
		return overrideDoctorName;
	}

	public void setOverrideDoctorName(String overrideDoctorName) {
		this.overrideDoctorName = overrideDoctorName;
	}
	
	public Date getOverrideDate() {
		return overrideDate == null ? null : new Date(overrideDate.getTime());
	}

	public void setOverrideDate(Date overrideDate) {
		this.overrideDate = overrideDate == null ? null : new Date(overrideDate.getTime());
	}
	
	public String getResumePendingRemark() {
		return resumePendingRemark;
	}

	public void setResumePendingRemark(String resumePendingRemark) {
		this.resumePendingRemark = resumePendingRemark;
	}
	
	public Boolean getTransferFlag() {
		return transferFlag;
	}

	public void setTransferFlag(Boolean transferFlag) {
		this.transferFlag = transferFlag;
	}
	
	public Date getTransferDate() {
		return transferDate == null ? null : new Date(transferDate.getTime());
	}

	public void setTransferDate(Date transferDate) {
		this.transferDate = transferDate == null ? null : new Date(transferDate.getTime());
	}
	
	public Boolean getUnitDoseExceptFlag() {
		return unitDoseExceptFlag;
	}

	public void setUnitDoseExceptFlag(Boolean unitDoseExceptFlag) {
		this.unitDoseExceptFlag = unitDoseExceptFlag;
	}

	public Date getLastAlertDate() {
		return lastAlertDate == null ? null : new Date(lastAlertDate.getTime());
	}

	public void setLastAlertDate(Date lastAlertDate) {
		this.lastAlertDate = lastAlertDate == null ? null : new Date(lastAlertDate.getTime());
	}

	public Boolean getVerbalFlag() {
		return verbalFlag;
	}

	public void setVerbalFlag(Boolean verbalFlag) {
		this.verbalFlag = verbalFlag;
	}

	public void setVerifyBeforeFlag(Boolean verifyBeforeFlag) {
		this.verifyBeforeFlag = verifyBeforeFlag;
	}

	public Boolean getVerifyBeforeFlag() {
		return verifyBeforeFlag;
	}

	public Long getPivasFormulaId() {
		return pivasFormulaId;
	}

	public void setPivasFormulaId(Long pivasFormulaId) {
		this.pivasFormulaId = pivasFormulaId;
	}

	public Boolean getPivasFlag() {
		return pivasFlag;
	}

	public void setPivasFlag(Boolean pivasFlag) {
		this.pivasFlag = pivasFlag;
	}

	public Boolean getFrozenFlag() {
		return frozenFlag;
	}

	public void setFrozenFlag(Boolean frozenFlag) {
		this.frozenFlag = frozenFlag;
	}

	public Boolean getModifyFlag() {
		return modifyFlag;
	}

	public void setModifyFlag(Boolean modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	
	public Integer getCycle() {
		return cycle;
	}

	public void setCycle(Integer cycle) {
		this.cycle = cycle;
	}

	public Date getCycleStartDate() {
		return cycleStartDate;
	}

	public void setCycleStartDate(Date cycleStartDate) {
		this.cycleStartDate = cycleStartDate;
	}

	public String getChemoCategory() {
		return chemoCategory;
	}

	public void setChemoCategory(String chemoCategory) {
		this.chemoCategory = chemoCategory;
	}

	public String getChemoInfoXml() {
		return chemoInfoXml;
	}

	public void setChemoInfoXml(String chemoInfoXml) {
		this.chemoInfoXml = chemoInfoXml;
	}

	public String getCmmFormulaCode() {
		return cmmFormulaCode;
	}

	public void setCmmFormulaCode(String cmmFormulaCode) {
		this.cmmFormulaCode = cmmFormulaCode;
	}

	public Boolean getPrintOrderPendingForm() {
		return printOrderPendingForm;
	}

	public void setPrintOrderPendingForm(Boolean printOrderPendingForm) {
		this.printOrderPendingForm = printOrderPendingForm;
	}
	
	public Boolean getUniqueDrugFlag() {
		return uniqueDrugFlag;
	}

	public void setUniqueDrugFlag(Boolean uniqueDrugFlag) {
		this.uniqueDrugFlag = uniqueDrugFlag;
	}
	
	public Integer getSeqNum() {
		return seqNum;
	}

	public void setSeqNum(Integer seqNum) {
		this.seqNum = seqNum;
	}

	public List<Long> getRelatedMedOrderItemIdList() {
		if (relatedMedOrderItemIdList == null) {
			relatedMedOrderItemIdList = new ArrayList<Long>();
		}
		return relatedMedOrderItemIdList;
	}
	
	public void addRelatedMedOrderItemId(Long relatedMedOrderItemId) {
		getRelatedMedOrderItemIdList().add(relatedMedOrderItemId);
	}

	public void setRelatedMedOrderItemIdList(List<Long> relatedMedOrderItemIdList) {
		this.relatedMedOrderItemIdList = relatedMedOrderItemIdList;
	}
	
	public Boolean getFirstItemFlag() {
		return firstItemFlag;
	}

	public void setFirstItemFlag(Boolean firstItemFlag) {
		this.firstItemFlag = firstItemFlag;
	}

	public List<MedProfileMoItemAlert> getMdsAlertList(Boolean includeHlaFlag) {
		List<MedProfileMoItemAlert> list = new ArrayList<MedProfileMoItemAlert>();
		for (MedProfileMoItemAlert mpMoiAlert : getMedProfileMoItemAlertList()) {
			if (mpMoiAlert.isMdsAlert(includeHlaFlag)) {
				list.add(mpMoiAlert);
			}
		}
		return list;
	}
	
	public List<MedProfileMoItemAlert> getPatAlertList(Boolean includeHlaFlag) {
		List<MedProfileMoItemAlert> list = new ArrayList<MedProfileMoItemAlert>();
		for (MedProfileMoItemAlert mpMoiAlert : getMedProfileMoItemAlertList()) {
			if (mpMoiAlert.isPatAlert(includeHlaFlag)) {
				list.add(mpMoiAlert);
			}
		}
		return list;
	}
	
	public List<MedProfileMoItemAlert> getDdiAlertList() {
		List<MedProfileMoItemAlert> list = new ArrayList<MedProfileMoItemAlert>();
		for (MedProfileMoItemAlert mpMoiAlert : getMedProfileMoItemAlertList()) {
			if (mpMoiAlert.isDdiAlert()) {
				list.add(mpMoiAlert);
			}
		}
		return list;
	}
	
	public List<MedProfileMoItemAlert> getDrcAlertList() {
		List<MedProfileMoItemAlert> list = new ArrayList<MedProfileMoItemAlert>();
		for (MedProfileMoItemAlert mpMoiAlert : getMedProfileMoItemAlertList()) {
			if (mpMoiAlert.isDrcAlert()) {
				list.add(mpMoiAlert);
			}
		}
		return list;
	}
	
	public String getDisplayNameSaltForm() {
		if (rxItem != null) {
			return rxItem.getDisplayNameSaltForm();
		}
		else {
			return null;
		}
	}
	
	public String getMdsOrderDesc() {
		if (rxItem != null) {
			return rxItem.getMdsOrderDesc();
		}
		else {
			return null;
		}
	}
	
	@ExternalizedProperty
	public boolean getDoseSpecifiedFlag()
	{
		if (rxItem != null) {
			
			if (rxItem instanceof IvRxDrug) {
				
				IvRxDrug ivRxDrug = (IvRxDrug) rxItem;
				
				if (MedProfileUtils.isValidNonZeroValue(ivRxDrug.getDispQty())) {
					return true;
				}
				
			} else if (rxItem instanceof RxDrug) {
				
				RxDrug rxDrug = (RxDrug) rxItem;
				
				if (rxDrug.getRegimen() != null) {
					for (DoseGroup doseGroup: rxDrug.getRegimen().getDoseGroupList()) {
						for (Dose dose: doseGroup.getDoseList()) {
							if (MedProfileUtils.isValidNonZeroValue(dose.getDispQty())) {
								return true;
							}
						}
					}
				}
			}
		}
		
		return false;
	}
	
	@ExternalizedProperty
	public boolean getCanSetLabelInstruction()
	{
		if (rxItem != null) {
			return rxItem.getCanPrintLabelInstruction();
		}
		return true;
	}
	
	public FixedConcnSource getFixedConcnSource() {
		if (rxItem != null) {
			return rxItem.getFixedConcnSource();
		}
		
		return FixedConcnSource.None;
	}
	
	@Transient
	public boolean isNoLabelOrder() {
		
		List<MedProfilePoItem> medProfilePoItemList = getMedProfilePoItemList();
		for (MedProfilePoItem medProfilePoItem: medProfilePoItemList) {
			if (!medProfilePoItem.isNoLabelItem()) {
				return false;
			}
		}
		return true;
	}
	
	@Transient
	public boolean isNormalItem(Date now) {
		return !isPendingSuspendItem();
	}
	
	@Transient
	public boolean isUrgentItem(Date now) {
		return !isPendingSuspendItem() && Boolean.TRUE.equals(getUrgentFlag()) &&
				(getFirstPrintDate() == null || MedProfileUtils.isSameDay(getFirstPrintDate(), now));
	}
	
	@Transient
	public boolean isPendingSuspendItem() {
		return getPendingCode() != null || getSuspendCode() != null || 
				getMdsSuspendReason() != null || Boolean.TRUE.equals(getUnitDoseExceptFlag());		
	}

	public List<? extends AlertEntity> getAlertList() {
		return getMedProfileMoItemAlertList();
	}
	
	public void markAlertFlag() {
		setAlertFlag(!getMdsAlertList(false).isEmpty());
	}
	
	public boolean isManualItem() {
		return orgItemNum != null && orgItemNum.intValue() >= 10000;
	}
	
	public boolean isFirstManualItem() {
		return firstManualItem;
	}

	public void setFirstManualItem(boolean firstManualItem) {
		this.firstManualItem = firstManualItem;
	}

	public boolean discardPoItemChanges() {
		return !isManualItem() && getMdsSuspendReason() != null;
	}

	public ChemoInfo getChemoInfo() {
		return chemoInfo;
	}

	public void setChemoInfo(ChemoInfo chemoInfo) {
		this.chemoInfo = chemoInfo;
	}

	public boolean isTreatmentDaySelected() {
		return treatmentDaySelected;
	}

	public void setTreatmentDaySelected(boolean treatmentDaySelected) {
		this.treatmentDaySelected = treatmentDaySelected;
	}
}
