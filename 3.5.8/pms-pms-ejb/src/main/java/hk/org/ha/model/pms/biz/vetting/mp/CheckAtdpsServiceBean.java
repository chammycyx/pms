package hk.org.ha.model.pms.biz.vetting.mp;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.reftable.ItemLocationListManagerLocal;
import hk.org.ha.model.pms.persistence.reftable.ItemLocation;
import hk.org.ha.model.pms.udt.reftable.MachineType;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("checkAtdpsService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CheckAtdpsServiceBean implements CheckAtdpsServiceLocal {

	@In
	private ItemLocationListManagerLocal itemLocationListManager;
	
	@Out(required = false)
	private List<ItemLocation> atdpsItemLocationList;

	public void checkAtdpsItemLocationByItemCode(String itemCode, List<Integer> pickStationNumList) {
		List<MachineType> machineTypeList = new ArrayList<MachineType>();
		machineTypeList.add(MachineType.ATDPS);
		atdpsItemLocationList = itemLocationListManager.retrieveItemLocationList(itemCode, pickStationNumList, machineTypeList);
	}
	
    @Remove
	@Override
	public void destroy() {
    	if ( atdpsItemLocationList != null ) {
    		atdpsItemLocationList = null;
    	}
	}
}
