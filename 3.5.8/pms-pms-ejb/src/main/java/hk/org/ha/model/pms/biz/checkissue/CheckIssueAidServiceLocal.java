package hk.org.ha.model.pms.biz.checkissue;

import javax.ejb.Local;

@Local
public interface CheckIssueAidServiceLocal {
	
	void retrieveIssueWindowNum();
	
	void updateCheckWorkstationRuleCode();
	
	void updateRefresh(boolean value);
	
	void retrieveOutstandingMoeOrderCount();
	
	void destroy();
	
}