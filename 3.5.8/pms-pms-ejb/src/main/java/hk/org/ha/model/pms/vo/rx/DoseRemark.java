package hk.org.ha.model.pms.vo.rx;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class DoseRemark extends Dose implements Serializable {

	private static final long serialVersionUID = 1L;

	private Boolean dosageRemark;
	
	private Boolean dosageUnitRemark;
	
	private Boolean prnRemark;
	
	private Boolean prnPercentRemark;
	
	private Boolean siteRemark;
	
	private Boolean dailyFreqRemark;
	
	private Boolean supplFrequencyRemark;
	
	private Boolean adminTimeCodeRemark;
	
	private Boolean dispQtyRemark;

	private Boolean baseUnitRemark;
	
	public DoseRemark() {
		super();
		dosageRemark = Boolean.FALSE;
		dosageUnitRemark = Boolean.FALSE;
		prnRemark = Boolean.FALSE;
		prnPercentRemark = Boolean.FALSE;
		siteRemark = Boolean.FALSE;
		dailyFreqRemark = Boolean.FALSE;
		supplFrequencyRemark = Boolean.FALSE;
		adminTimeCodeRemark = Boolean.FALSE;
		dispQtyRemark = Boolean.FALSE;
		baseUnitRemark = Boolean.FALSE;
	}
	
	public Boolean getDosageRemark() {
		return dosageRemark;
	}

	public void setDosageRemark(Boolean dosageRemark) {
		this.dosageRemark = dosageRemark;
	}

	public Boolean getDosageUnitRemark() {
		return dosageUnitRemark;
	}

	public void setDosageUnitRemark(Boolean dosageUnitRemark) {
		this.dosageUnitRemark = dosageUnitRemark;
	}

	public Boolean getPrnRemark() {
		return prnRemark;
	}

	public void setPrnRemark(Boolean prnRemark) {
		this.prnRemark = prnRemark;
	}

	public Boolean getPrnPercentRemark() {
		return prnPercentRemark;
	}

	public void setPrnPercentRemark(Boolean prnPercentRemark) {
		this.prnPercentRemark = prnPercentRemark;
	}

	public Boolean getSiteRemark() {
		return siteRemark;
	}

	public void setSiteRemark(Boolean siteRemark) {
		this.siteRemark = siteRemark;
	}

	public Boolean getDailyFreqRemark() {
		return dailyFreqRemark;
	}

	public void setDailyFreqRemark(Boolean dailyFreqRemark) {
		this.dailyFreqRemark = dailyFreqRemark;
	}

	public Boolean getSupplFrequencyRemark() {
		return supplFrequencyRemark;
	}

	public void setSupplFrequencyRemark(Boolean supplFrequencyRemark) {
		this.supplFrequencyRemark = supplFrequencyRemark;
	}

	public Boolean getAdminTimeCodeRemark() {
		return adminTimeCodeRemark;
	}

	public void setAdminTimeCodeRemark(Boolean adminTimeCodeRemark) {
		this.adminTimeCodeRemark = adminTimeCodeRemark;
	}

	public Boolean getDispQtyRemark() {
		return dispQtyRemark;
	}

	public void setDispQtyRemark(Boolean dispQtyRemark) {
		this.dispQtyRemark = dispQtyRemark;
	}

	public Boolean getBaseUnitRemark() {
		return baseUnitRemark;
	}

	public void setBaseUnitRemark(Boolean baseUnitRemark) {
		this.baseUnitRemark = baseUnitRemark;
	}
}
