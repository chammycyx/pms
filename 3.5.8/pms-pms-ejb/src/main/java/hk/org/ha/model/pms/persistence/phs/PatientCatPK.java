package hk.org.ha.model.pms.persistence.phs;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class PatientCatPK implements Serializable {

	private static final long serialVersionUID = 1L;

	private String instCode;
	private String patCatCode;
	
	public PatientCatPK() {
	}
	
	public PatientCatPK(String instCode, String patCatCode) {
		this.instCode = instCode;
		this.patCatCode = patCatCode;
	}

	public String getInstCode() {
		return instCode;
	}

	public void setInstCode(String instCode) {
		this.instCode = instCode;
	}

	public String getPatCatCode() {
		return patCatCode;
	}

	public void setPatCatCode(String patCatCode) {
		this.patCatCode = patCatCode;
	}

	public boolean equals(Object other) {
		return EqualsBuilder.reflectionEquals(this, other);
	}
	
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
}
