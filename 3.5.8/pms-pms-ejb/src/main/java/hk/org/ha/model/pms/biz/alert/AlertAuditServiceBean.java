package hk.org.ha.model.pms.biz.alert;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("alertAuditService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class AlertAuditServiceBean implements AlertAuditServiceLocal {

	@In
	private AlertAuditManagerLocal alertAuditManager;
	
	public void alertMsg(String messageCode, String desc) {
		alertAuditManager.alertMsg(messageCode, desc);
	}
	
	public void newAlertDetectForManualProfile(String action, Long medProfileId, String hkid, String caseNum, List<AlertMsg> alertMsgList) {
		alertAuditManager.newAlertDetectForManualProfile(action, medProfileId, hkid, caseNum, alertMsgList);
	}

	public void cancelPrescribeForManualProfile(String action, Long medProfileId, String hkid, String caseNum, String orderDesc) {
		alertAuditManager.cancelPrescribeForManualProfile(action, medProfileId, hkid, caseNum, orderDesc);
	}
	
	@Remove
	public void destroy() {
	}
}
