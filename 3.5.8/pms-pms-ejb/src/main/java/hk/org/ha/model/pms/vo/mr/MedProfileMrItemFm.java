package hk.org.ha.model.pms.vo.mr;

import hk.org.ha.model.pms.corp.cache.DmCorpIndicationCacher;
import hk.org.ha.model.pms.persistence.FmEntity;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
import org.jboss.seam.security.Identity;

@ExternalizedBean(type=DefaultExternalizer.class)
public class MedProfileMrItemFm extends FmEntity {

	private static final long serialVersionUID = 1L;

	private MedProfileMrItem medProfileMrItem;

    public void postLoad() {
    	if (Identity.instance().isLoggedIn()) {
    		if (this.getCorpIndCode() != null) {
    			setDmCorpIndication(DmCorpIndicationCacher.instance().getDmCorpIndication(this.getCorpIndCode()));
    		}
    	}
    }
    
	public MedProfileMrItem getMedProfileMrItem() {
		return medProfileMrItem;
	}

	public void setMedProfileMrItem(MedProfileMrItem medProfileMrItem) {
		this.medProfileMrItem = medProfileMrItem;
	}
}
