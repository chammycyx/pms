package hk.org.ha.model.pms.biz.drug;
import hk.org.ha.model.pms.dms.vo.FormVerb;

import javax.ejb.Local;

@Local
public interface SiteFormVerbServiceLocal {
	FormVerb retrieveDefaultFormVerbByFormCode(String formCode, String itemCode);
	void destroy();
}
