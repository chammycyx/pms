package hk.org.ha.model.pms.vo.machine;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "AtdpsMessageContent")
@ExternalizedBean(type=DefaultExternalizer.class)
public class AtdpsMessageContent implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@XmlElement(required = false)
	private String hostname;
	
	@XmlElement(required = false)
	private int portNum;
	
	@XmlElement(required = false)
	private String hospCode;
	
	@XmlElement(required = false)
	private String hospName;
	
	@XmlElement(required = false)
	private String workstoreCode;
	
	@XmlElement(required = false)
	private String workstationCode;
	
	@XmlElement(required = false)
	private String langType;
	
	@XmlElement(required = false)
	private String itemNum;
	
	@XmlElement(required = false)
	private String batchNum;
	
	@XmlElement(required = false)
	private String printType;
	
	@XmlElement(required = false)
	private String itemCode;
	
	@XmlElement(required = false)
	private String drugName;
	
	@XmlElement(required = false)
	private String tradeName;
	
	@XmlElement(required = false)
	private String instruction;
	
	@XmlElement(required = false)
	private String formCode;
	
	@XmlElement(required = false)
	private String dispDate;
	
	@XmlElement(required = false)
	private String patName;
	
	@XmlElement(required = false)
	private String hkid;
	
	@XmlElement(required = false)
	private String caseNum;
	
	@XmlElement(required = false)
	private String ward;
	
	@XmlElement(required = false)
	private String refillFlag;
	
	@XmlElement(required = false)
	private String specCode;
	
	@XmlElement(required = false)
	private String pasBedNum;
	
	@XmlElement(required = false)
	private String patCatCode;
	
	@XmlElement(required = false)
	private String doctorCode;
	
	@XmlElement(required = false)
	private String userCode;
	
	@XmlElement(required = false)
	private String prescNature;
	
	@XmlElement(required = false)
	private String dosage;

	@XmlElement(required = false)
	private String duration;
	
	@XmlElement(required = false)
	private String durationUnit;
	
	@XmlElement(required = false)
	private String dispQty;
	
	@XmlElement(required = false)
	private String modBaseunit;
	
	@XmlElement(required = false)
	private String warnDesc1;
	
	@XmlElement(required = false)
	private String warnDesc2;
	
	@XmlElement(required = false)
	private String warnDesc3;
	
	@XmlElement(required = false)
	private String warnDesc4;
	
	@XmlElement(required = false)
	private String fdnFlag;
	
	@XmlElement(required = false)
	private String itemLocationBinNum;
	
	@XmlElement(required = false)
	private String printInstruct;
	
	@XmlElement(required = false)
	private String printBarcode;
	
	@XmlElement(required = false)
	private String printWardBarcode;
	
	@XmlElement(required = false)
	private String updateDate;
	
	@XmlElement(required = false)
	private String updateTime;
	
	@XmlElement(required = false)
	private String msgCode;
	
	@XmlElement(required = false)
	private String overrideRemark;
	
	@XmlElement(required = false)
	private String overrideReason;
	
	@XmlElement(required = false)
	private String mp2dBarCodeData;
	
	@XmlElement(required = false)
	private String freqCode;
	
	@XmlElement(required = false)
	private String supplFreqCode;
	
	@XmlElement(required = false)
	private String mealTimeInfo;
	
	@XmlElement(required = false)
	private String urgentFlag;
	
	@XmlElement(required = false)
	private String batchCode;
	
	@XmlElement(required = false)
	private String fmInficator;
	
	@XmlElement(required = false)
	private String labelIndicator;
	
	@XmlElement(required = false)
	private String reprintFlag;
	
	@XmlElement(required = false)
	private String fractionalDosageFlag;
	
	@XmlElement(required = false)
	private String prnFlag;
	
	@XmlElement(required = false)
	private String verifierCode;
	
	@XmlElement(required = false)
	private Long trxId;
	
	@XmlElement(required = false)
	private List<String> ccCode;
	
	@XmlElement(required = false)
	private Integer pickStationNum;
	
	@XmlElement(required = false)
	private String adjQty;
	
	private transient boolean showInstructionFlag = false;
	
	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public int getPortNum() {
		return portNum;
	}

	public void setPortNum(int portNum) {
		this.portNum = portNum;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getHospName() {
		return hospName;
	}

	public void setHospName(String hospName) {
		this.hospName = hospName;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public String getWorkstationCode() {
		return workstationCode;
	}

	public void setWorkstationCode(String workstationCode) {
		this.workstationCode = workstationCode;
	}

	public String getLangType() {
		return langType;
	}

	public void setLangType(String langType) {
		this.langType = langType;
	}

	public String getItemNum() {
		return itemNum;
	}

	public void setItemNum(String itemNum) {
		this.itemNum = itemNum;
	}

	public String getPrintType() {
		return printType;
	}

	public void setPrintType(String printType) {
		this.printType = printType;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getDrugName() {
		return drugName;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public String getTradeName() {
		return tradeName;
	}

	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}

	public String getInstruction() {
		return instruction;
	}

	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}

	public String getFormCode() {
		return formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	public String getDispDate() {
		return dispDate;
	}

	public void setDispDate(String dispDate) {
		this.dispDate = dispDate;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getPasBedNum() {
		return pasBedNum;
	}

	public void setPasBedNum(String pasBedNum) {
		this.pasBedNum = pasBedNum;
	}

	public String getPatCatCode() {
		return patCatCode;
	}

	public void setPatCatCode(String patCatCode) {
		this.patCatCode = patCatCode;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getPrescNature() {
		return prescNature;
	}

	public void setPrescNature(String prescNature) {
		this.prescNature = prescNature;
	}

	public String getDosage() {
		return dosage;
	}

	public void setDosage(String dosage) {
		this.dosage = dosage;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getDispQty() {
		return dispQty;
	}

	public void setDispQty(String dispQty) {
		this.dispQty = dispQty;
	}

	public String getModBaseunit() {
		return modBaseunit;
	}

	public void setModBaseunit(String modBaseunit) {
		this.modBaseunit = modBaseunit;
	}

	public String getWarnDesc1() {
		return warnDesc1;
	}

	public void setWarnDesc1(String warnDesc1) {
		this.warnDesc1 = warnDesc1;
	}

	public String getWarnDesc2() {
		return warnDesc2;
	}

	public void setWarnDesc2(String warnDesc2) {
		this.warnDesc2 = warnDesc2;
	}

	public String getWarnDesc3() {
		return warnDesc3;
	}

	public void setWarnDesc3(String warnDesc3) {
		this.warnDesc3 = warnDesc3;
	}

	public String getWarnDesc4() {
		return warnDesc4;
	}

	public void setWarnDesc4(String warnDesc4) {
		this.warnDesc4 = warnDesc4;
	}

	public String getItemLocationBinNum() {
		return itemLocationBinNum;
	}

	public void setItemLocationBinNum(String itemLocationBinNum) {
		this.itemLocationBinNum = itemLocationBinNum;
	}

	public String getPrintInstruct() {
		return printInstruct;
	}

	public void setPrintInstruct(String printInstruct) {
		this.printInstruct = printInstruct;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public String getRefillFlag() {
		return refillFlag;
	}

	public void setRefillFlag(String refillFlag) {
		this.refillFlag = refillFlag;
	}

	public String getFdnFlag() {
		return fdnFlag;
	}

	public void setFdnFlag(String fdnFlag) {
		this.fdnFlag = fdnFlag;
	}

	public String getMp2dBarCodeData() {
		return mp2dBarCodeData;
	}

	public void setMp2dBarCodeData(String mp2dBarCodeData) {
		this.mp2dBarCodeData = mp2dBarCodeData;
	}

	public String getMealTimeInfo() {
		return mealTimeInfo;
	}

	public void setMealTimeInfo(String mealTimeInfo) {
		this.mealTimeInfo = mealTimeInfo;
	}

	public String getBatchCode() {
		return batchCode;
	}

	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}

	public String getFmInficator() {
		return fmInficator;
	}

	public void setFmInficator(String fmInficator) {
		this.fmInficator = fmInficator;
	}

	public String getLabelIndicator() {
		return labelIndicator;
	}

	public void setLabelIndicator(String labelIndicator) {
		this.labelIndicator = labelIndicator;
	}

	public String getVerifierCode() {
		return verifierCode;
	}

	public void setVerifierCode(String verifierCode) {
		this.verifierCode = verifierCode;
	}

	public String getPrintBarcode() {
		return printBarcode;
	}

	public void setPrintBarcode(String printBarcode) {
		this.printBarcode = printBarcode;
	}

	public String getPrintWardBarcode() {
		return printWardBarcode;
	}

	public void setPrintWardBarcode(String printWardBarcode) {
		this.printWardBarcode = printWardBarcode;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public String getOverrideRemark() {
		return overrideRemark;
	}

	public void setOverrideRemark(String overrideRemark) {
		this.overrideRemark = overrideRemark;
	}

	public String getOverrideReason() {
		return overrideReason;
	}

	public void setOverrideReason(String overrideReason) {
		this.overrideReason = overrideReason;
	}

	public String getUrgentFlag() {
		return urgentFlag;
	}

	public void setUrgentFlag(String urgentFlag) {
		this.urgentFlag = urgentFlag;
	}

	public String getReprintFlag() {
		return reprintFlag;
	}

	public void setReprintFlag(String reprintFlag) {
		this.reprintFlag = reprintFlag;
	}

	public String getFractionalDosageFlag() {
		return fractionalDosageFlag;
	}

	public void setFractionalDosageFlag(String fractionalDosageFlag) {
		this.fractionalDosageFlag = fractionalDosageFlag;
	}

	public String getPrnFlag() {
		return prnFlag;
	}

	public void setPrnFlag(String prnFlag) {
		this.prnFlag = prnFlag;
	}

	public String getFreqCode() {
		return freqCode;
	}

	public void setFreqCode(String freqCode) {
		this.freqCode = freqCode;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getHkid() {
		return hkid;
	}

	public void setDurationUnit(String durationUnit) {
		this.durationUnit = durationUnit;
	}

	public String getDurationUnit() {
		return durationUnit;
	}

	public void setSupplFreqCode(String supplFreqCode) {
		this.supplFreqCode = supplFreqCode;
	}

	public String getSupplFreqCode() {
		return supplFreqCode;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}

	public String getWard() {
		return ward;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setTrxId(Long trxId) {
		this.trxId = trxId;
	}

	public Long getTrxId() {
		return trxId;
	}

	public void setCcCode(List<String> ccCode) {
		this.ccCode = ccCode;
	}

	public List<String> getCcCode() {
		return ccCode;
	}

	public void setPickStationNum(Integer pickStationNum) {
		this.pickStationNum = pickStationNum;
	}

	public Integer getPickStationNum() {
		return pickStationNum;
	}

	public void setShowInstructionFlag(boolean showInstructionFlag) {
		this.showInstructionFlag = showInstructionFlag;
	}

	public boolean isShowInstructionFlag() {
		return showInstructionFlag;
	}

	public void setAdjQty(String adjQty) {
		this.adjQty = adjQty;
	}

	public String getAdjQty() {
		return adjQty;
	}

}
