package hk.org.ha.model.pms.biz.uncollect;

import static hk.org.ha.model.pms.prop.Prop.UNCOLLECT_DAYENDORDER_DURATION;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.udt.disp.DispOrderDayEndStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.InvoiceDocType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.uncollect.UncollectOrder;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.security.Identity;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;

@Stateful
@Scope(ScopeType.SESSION)
@Name("uncollectListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class UncollectListServiceBean implements UncollectListServiceLocal {

	@PersistenceContext
	private EntityManager em;
	
	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;

	@In
	private Workstore workstore; 
	
	@In
	private Identity identity;
	 
	@Out(required = false)
	private List<UncollectOrder> uncollectOrderList;
	
	private boolean retrieveEmpty;

	@SuppressWarnings("unchecked")
	public void retrieveUncollectOrderList() {

		uncollectOrderList = new ArrayList<UncollectOrder>();
		
		Date createDateStart = new DateMidnight().minusDays(UNCOLLECT_DAYENDORDER_DURATION.get()).toDate();
		Date createDateEnd = new DateMidnight().toDateTime().plusDays(1).minus(1).toDate();
		
		List<DispOrder> dispOrderList = em.createQuery(
				"select o from DispOrder o" + // 20140709 index check : Ticket.workstore,orderType,ticketDate : UI_TICKET_01
				" where o.workstore = :workstore" +
				" and o.orderType = :orderType" +
				" and o.ticketDate between :startDate and :endDate" +
				" and o.status not in :status" +
				" and o.dayEndStatus = :dispOrderDayEndStatus" +
				" and o.uncollectFlag = :uncollectFlag" +
				" and o.createDate between :createDateStart and :createDateEnd" +
				" order by o.ticketDate desc, o.ticketNum desc")
				.setParameter("workstore", workstore)
				.setParameter("orderType", OrderType.OutPatient)
				.setParameter("startDate", new DateTime().minusDays(UNCOLLECT_DAYENDORDER_DURATION.get()).toDate())
				.setParameter("endDate", new Date())
				.setParameter("status", DispOrderStatus.Deleted_SysDeleted)
				.setParameter("dispOrderDayEndStatus", DispOrderDayEndStatus.Completed)
				.setParameter("uncollectFlag", true)
				.setParameter("createDateStart", createDateStart)
				.setParameter("createDateEnd", createDateEnd)
				.setHint(QueryHints.BATCH, "o.invoiceList")
				.getResultList();
		
		
		if ( !dispOrderList.isEmpty() ) {
			
			for ( DispOrder dispOrder : dispOrderList ) {
				UncollectOrder uncollectOrder = new UncollectOrder();
				uncollectOrder.setTicketDate(dispOrder.getTicket().getTicketDate());
				uncollectOrder.setTicketNum(dispOrder.getTicket().getTicketNum());
				if ( dispOrder.getPharmOrder().getMedCase() != null ) {
					uncollectOrder.setCaseNum(dispOrder.getPharmOrder().getMedCase().getCaseNum());
				}
				dispOrder.getPharmOrder().getPharmOrderItemList().size();
				uncollectOrder.setDispOrderId(dispOrder.getId());
				uncollectOrder.setPharmOrderVersion(dispOrder.getPharmOrder().getVersion());
				
				boolean sfiFlag = false;
				
				for (Invoice invoice : dispOrder.getInvoiceList()){
					if (InvoiceDocType.Standard.equals(invoice.getDocType())) {
						uncollectOrder.setReceiptNum(invoice.getReceiptNum());
					} else if (InvoiceDocType.Sfi.equals(invoice.getDocType())) {
						sfiFlag = true;
					}
				}
				
				if ( sfiFlag ) {
					uncollectOrder.setSfiFlag(true);
				}
				
				uncollectOrderList.add(uncollectOrder);
			}
		}
		
		if (uncollectOrderList.isEmpty()) {
			retrieveEmpty = true;
		}else {
			retrieveEmpty = false;
		}
	}
	
	@SuppressWarnings("unchecked")
	public void updateUncollectOrderList(List<UncollectOrder> updateUncollectOrderList){
		
		if ( updateUncollectOrderList.isEmpty() ) {
			retrieveUncollectOrderList();
			return;
		}
		
		Map<Long, UncollectOrder> uncollectOrderMap = new HashMap<Long, UncollectOrder>();
		List<Long> dispOrderIdList = new ArrayList<Long>();
		for ( UncollectOrder uncollectOrder : updateUncollectOrderList ) {
			uncollectOrderMap.put(uncollectOrder.getDispOrderId(), uncollectOrder);
			dispOrderIdList.add(uncollectOrder.getDispOrderId());
		}
		
		List<DispOrder> dispOrderList = em.createQuery(
											"select o from DispOrder o" + // 20120831 index check : DispOrder.id : PK_DISP_ORDER
											" where o.id in :dispOrderIdList")
											.setParameter("dispOrderIdList", dispOrderIdList)
											.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
											.getResultList();
		
		//check version
		for ( DispOrder dispOrder : dispOrderList ) {
			UncollectOrder uncollectOrder = uncollectOrderMap.get(dispOrder.getId());
			PharmOrder pharmOrder = dispOrder.getPharmOrder();
			
			if (!pharmOrder.getVersion().equals(uncollectOrder.getPharmOrderVersion())){
				throw new OptimisticLockException();
			}
		}

		//update to uncollect
		corpPmsServiceProxy.updateUncollectOrderList(updateUncollectOrderList, workstore, identity.getCredentials().getUsername());
		
		for ( DispOrder dispOrder : dispOrderList ) {
			UncollectOrder uncollectOrder = uncollectOrderMap.get(dispOrder.getId());
			
			if ( uncollectOrder.isMarkDelete() ){
				dispOrder.setUncollectFlag(false);
				dispOrder.setUncollectDate(null);
				dispOrder.setUncollectUser(null);
			} else {
				dispOrder.setUncollectFlag(true);
				dispOrder.setUncollectDate(new Date());
				dispOrder.setUncollectUser(identity.getCredentials().getUsername());
			}
		}
		
		em.flush();
		retrieveUncollectOrderList();
	}
	
	public boolean isRetrieveEmpty() {
		return retrieveEmpty;
	}

	@Remove
	public void destroy() 
	{
		if (uncollectOrderList != null) {
			uncollectOrderList = null;
		}
	}
}
