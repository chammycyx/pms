package hk.org.ha.model.pms.udt.alert.mds;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum OpPatOverrideReason implements StringValuedEnum {

	NoAlternative("NA", "No other alternative available"),
	Desensitizing("DE", "Desensitizing the patient with this drug"),
	Doubts       ("DO", "Doubts about the reported history of drug allergy"),
	NoAllergic   ("AL", "Patient is taking this drug without allergic response"),
	NoAdr        ("AD", "Patient can tolerate the adverse drug reaction without any problem"),
	Tolerate     ("TO", "Patient can tolerate the drug at lower dose");
	
	private final String dataValue;
    private final String displayValue;
    
    OpPatOverrideReason(final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}

	public static OpPatOverrideReason dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(OpPatOverrideReason.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<OpPatOverrideReason> {
		
		private static final long serialVersionUID = 1L;

		public Class<OpPatOverrideReason> getEnumClass() {
    		return OpPatOverrideReason.class;
    	}
    }
}
