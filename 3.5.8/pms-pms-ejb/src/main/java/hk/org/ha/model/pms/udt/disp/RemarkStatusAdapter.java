package hk.org.ha.model.pms.udt.disp;

import java.io.Serializable;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class RemarkStatusAdapter extends XmlAdapter<String, RemarkStatus> implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public String marshal(RemarkStatus v) {
		return (v == null) ? null : v.getDataValue();
	}

	public RemarkStatus unmarshal(String v) {
		return RemarkStatus.dataValueOf(v);
	}

}
