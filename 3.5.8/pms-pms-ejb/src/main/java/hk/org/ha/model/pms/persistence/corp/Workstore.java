package hk.org.ha.model.pms.persistence.corp;

import flexjson.JSON;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.RecordStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "WORKSTORE")
@IdClass(WorkstorePK.class)
@XmlAccessorType(XmlAccessType.FIELD)
public class Workstore extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "HOSP_CODE", length=3)
	@XmlElement
	private String hospCode;
	
	@Id
	@Column(name = "WORKSTORE_CODE", length=4)
	@XmlElement
	private String workstoreCode;
	
	@Column(name = "DEF_PAT_HOSP_CODE", length=3)
    @XmlTransient
	private String defPatHospCode;

    @Converter(name = "Workstore.status", converterClass = RecordStatus.Converter.class)
    @Convert("Workstore.status")
	@Column(name="STATUS", nullable = false, length = 1)
    @XmlTransient
	private RecordStatus status;
    
    @ManyToOne
    @JoinColumn(name = "HOSP_CODE", nullable = false, insertable = false, updatable = false)
    @XmlTransient
    private Hospital hospital;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "WORKSTORE_GROUP_CODE", nullable = false)
    @XmlTransient
    private WorkstoreGroup workstoreGroup;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "CDDH_WORKSTORE_GROUP_CODE")
    @XmlTransient
    private CddhWorkstoreGroup cddhWorkstoreGroup;
	
	@Column(name = "OPERATION_PROFILE_ID")
    @XmlTransient
	private Long operationProfileId;

	public Workstore() {
		status = RecordStatus.Active;
	}
	
	public Workstore(String hospCode, String workstoreCode) {
		this();
		this.hospCode = hospCode;
		this.workstoreCode = workstoreCode;
	}
	
	public Workstore(String hospCode, String workstoreCode, String workstoreGroupCode) {
		this(hospCode, workstoreCode);
		this.workstoreGroup = new WorkstoreGroup(workstoreGroupCode);
	}
	
	public String getHospCode() {
		return hospCode;
	}
	
	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}
	
	public String getDefPatHospCode() {
		return defPatHospCode;
	}
	
	public void setDefPatHospCode(String defPatHospCode) {
		this.defPatHospCode = defPatHospCode;
	}

	public RecordStatus getStatus() {
		return status;
	}

	public void setStatus(RecordStatus status) {
		this.status = status;
	}

	@JSON(include=false)
	public Hospital getHospital() {
		return hospital;
	}
	
	public void setHospital(Hospital hospital) {
		this.hospital = hospital;
	}
	
	@JSON(include=false)
	public WorkstoreGroup getWorkstoreGroup() {
		return workstoreGroup;
	}
	
	public void setWorkstoreGroup(WorkstoreGroup workstoreGroup) {
		this.workstoreGroup = workstoreGroup;
	}

	@JSON(include=false)
	public CddhWorkstoreGroup getCddhWorkstoreGroup() {
		return cddhWorkstoreGroup;
	}

	public void setCddhWorkstoreGroup(CddhWorkstoreGroup cddhWorkstoreGroup) {
		this.cddhWorkstoreGroup = cddhWorkstoreGroup;
	}

	public Long getOperationProfileId() {
		return operationProfileId;
	}

	public void setOperationProfileId(Long operationProfileId) {
		this.operationProfileId = operationProfileId;
	}

	// helper methods
	public WorkstorePK getId() {
		return new WorkstorePK(this.getHospCode(), this.getWorkstoreCode());
	}

	@Override
	public int hashCode() {		
		return new HashCodeBuilder()
			.append(hospCode)
			.append(workstoreCode)
			.toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		Workstore other = (Workstore) obj;
		return new EqualsBuilder()
			.append(hospCode, other.getHospCode())
			.append(workstoreCode, other.getWorkstoreCode())
			.isEquals();
	}
}
