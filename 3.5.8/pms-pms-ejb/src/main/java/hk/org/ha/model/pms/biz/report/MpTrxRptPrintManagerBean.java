package hk.org.ha.model.pms.biz.report;

import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.delivery.DeliveryManagerLocal;
import hk.org.ha.model.pms.biz.label.LabelManagerBean.DeliveryComparator;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.persistence.medprofile.Delivery;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.report.MpTrxRpt;
import hk.org.ha.model.pms.vo.report.MpTrxRptHdr;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("mpTrxRptPrintManager")
@MeasureCalls
public class MpTrxRptPrintManagerBean implements MpTrxRptPrintManagerLocal {
	
	@In
	private AuditLogger auditLogger; 
	
	@In
	private DeliveryManagerLocal deliveryManager;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent; 

	@In
	private MpTrxRptBuilderLocal mpTrxRptBuilder;
	
	public List<MpTrxRpt> retrieveMpTrxRptListByBatchNum(String batchNum, Date dispenseDate) {		// Reprint
		
		List<DeliveryItem> deliveryItemList = deliveryManager.retrieveDeliveryItemListByBatchNumForReprint(batchNum, dispenseDate);
		
		Delivery delivery = new Delivery();
		
		if (deliveryItemList.size() > 0) {
			delivery = deliveryItemList.get(0).getDelivery();
			delivery.setDeliveryItemList(deliveryItemList);
			mpTrxRptBuilder.retrieveDeliveryAlertProfile(delivery);
		}
		
		return delivery.isPivasBatch() ? mpTrxRptBuilder.convertToPivasTrxRptList(delivery) : mpTrxRptBuilder.convertToMpTrxRptList(delivery);
	}
	
	public List<MpTrxRpt> retrieveMpTrxRptListByDeliveryList(List<Delivery> deliveryList){
		List<MpTrxRpt> mpTrxRptList = new ArrayList<MpTrxRpt>();
		
		for(Delivery delivery: deliveryList){
			mpTrxRptBuilder.retrieveDeliveryAlertProfile(delivery);
			
			mpTrxRptList.addAll(mpTrxRptBuilder.convertToDrugRefillRptList(delivery));
		}
		return mpTrxRptList;
	}

	public List<RenderAndPrintJob> printMpTrxRpt(List<Delivery> deliveryList) {		// Due order label gen
		List<RenderAndPrintJob> printJobList = new ArrayList<RenderAndPrintJob>();
		
		Collections.sort(deliveryList, new DeliveryComparator());
		
		for( Delivery delivery : deliveryList ){
			printJobList.addAll(printMpTrxRpt(delivery, false, true, false));
		}
		
		return printJobList;
	}
	
	public List<RenderAndPrintJob> printPivasTrxRpt(List<Delivery> deliveryList) {		// Due order label gen
		List<RenderAndPrintJob> printJobList = new ArrayList<RenderAndPrintJob>();
		
		Collections.sort(deliveryList, new DeliveryComparator());
		
		for( Delivery delivery : deliveryList ){
			printJobList.addAll(printPivasTrxRpt(delivery, false, true, false));
		}
		
		return printJobList;
	}
	
	public List<RenderAndPrintJob> printMpTrxRpt(Delivery delivery) {		// Direct Label print
		
		return printMpTrxRpt(delivery, false, true, false);
		
	}

	public void reprintMpTrxRpt(Delivery delivery) {	// Check
		
		printMpTrxRpt(delivery, true, true, true);
		
	}
	
	public void printMpTrxRptForAssignBatch(Delivery delivery) {	// Assign Batch
		
		printMpTrxRpt(delivery, false, true, true);
		
	}

	private List<RenderAndPrintJob> printMpTrxRpt(Delivery delivery, Boolean reprintFlag, Boolean retrieveAlertProfile, Boolean directRender) {

		List<RenderAndPrintJob> renderAndPrintJobList = new ArrayList<RenderAndPrintJob>();
		
		if (retrieveAlertProfile) {
			mpTrxRptBuilder.retrieveDeliveryAlertProfile(delivery);
		}
		
		List<MpTrxRpt> mpTrxRptList = delivery.isPivasBatch() ? mpTrxRptBuilder.convertToPivasTrxRptList(delivery) : mpTrxRptBuilder.convertToMpTrxRptList(delivery);
		
		if (mpTrxRptList != null && mpTrxRptList.get(0).getMpTrxRptHdr() != null && 
				mpTrxRptList.get(0).getMpTrxRptHdr().getNumOfItem() > 0){
		
			Map<String, Object> parameters = new HashMap<String, Object>();
			
			if(delivery.getModifyDate() != null){
				parameters.put("reprintUpdateFlag", reprintFlag);
			}else{
				parameters.put("reprintFlag", reprintFlag);
				
			}
			
			RenderAndPrintJob printJob = new RenderAndPrintJob(
					"MpTrxRpt", 
					new PrintOption(), 
					printerSelector.retrievePrinterSelect(PrintDocType.Report), 
					parameters, 
					mpTrxRptList);
			
			if( directRender ){
				printAgent.renderAndPrint(printJob);
			}else{
				renderAndPrintJobList.add(printJob);
			}
			
			MpTrxRptHdr mpTrxRptHdr = mpTrxRptList.get(0).getMpTrxRptHdr();
			
			auditLogger.log("#0253", mpTrxRptHdr.getBatchCode(), 
										mpTrxRptHdr.getWardCode(), 
										mpTrxRptHdr.getNumOfItem(), 
										delivery.getPrintMode().getDataValue());
			
		}
		
		return renderAndPrintJobList;
		
	}
	
	private List<RenderAndPrintJob> printPivasTrxRpt(Delivery delivery, Boolean reprintFlag, Boolean retrieveAlertProfile, Boolean directRender) {

		List<RenderAndPrintJob> renderAndPrintJobList = new ArrayList<RenderAndPrintJob>();
		
		if (retrieveAlertProfile) {
			mpTrxRptBuilder.retrieveDeliveryAlertProfile(delivery);
		}
		
		List<MpTrxRpt> mpTrxRptList = mpTrxRptBuilder.convertToPivasTrxRptList(delivery);
		
		if (mpTrxRptList != null && mpTrxRptList.get(0).getMpTrxRptHdr() != null && 
				mpTrxRptList.get(0).getMpTrxRptHdr().getNumOfItem() > 0){
		
			Map<String, Object> parameters = new HashMap<String, Object>();
			
			if(delivery.getModifyDate() != null){
				parameters.put("reprintUpdateFlag", reprintFlag);
			}else{
				parameters.put("reprintFlag", reprintFlag);
				
			}
			
			RenderAndPrintJob printJob = new RenderAndPrintJob(
					"MpTrxRpt", 
					new PrintOption(), 
					printerSelector.retrievePrinterSelect(PrintDocType.Report), 
					parameters, 
					mpTrxRptList, 
					"[PivasTrxRpt]");
			
			if( directRender ){
				printAgent.renderAndPrint(printJob);
			}else{
				renderAndPrintJobList.add(printJob);
			}
			
			MpTrxRptHdr mpTrxRptHdr = mpTrxRptList.get(0).getMpTrxRptHdr();
			
			auditLogger.log("#0253", mpTrxRptHdr.getBatchCode(), 
										mpTrxRptHdr.getWardCode(), 
										mpTrxRptHdr.getNumOfItem(), 
										delivery.getPrintMode().getDataValue());
			
		}
		
		return renderAndPrintJobList;
		
	}

}