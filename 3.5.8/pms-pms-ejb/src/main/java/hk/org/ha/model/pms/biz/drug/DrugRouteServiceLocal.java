package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.model.pms.dms.vo.DrugRouteCriteria;
import hk.org.ha.model.pms.vo.rx.Site;

import java.util.List;

import javax.ejb.Local;

@Local
public interface DrugRouteServiceLocal {
	List<Site> retrieveIpSiteListByItemCode(String itemCode);
	List<Site> retrieveManualEntrySiteListByItemCode(String itemCode);
	void retrieveIpOthersSiteMapListByDrugRouteCriteria(DrugRouteCriteria criteria);	
	void retrieveManualEntryOthersSiteMapListByDrugRouteCriteria(DrugRouteCriteria criteria);
	void destroy();		
}
