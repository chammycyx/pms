package hk.org.ha.model.pms.persistence.reftable;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.eclipse.persistence.annotations.Customizer;
import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;

@Entity
@Table(name = "WORKLOAD_STAT")
@Customizer(AuditCustomizer.class)
public class WorkloadStat extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "workloadStatSeq")
	@SequenceGenerator(name = "workloadStatSeq", sequenceName = "SQ_WORKLOAD_STAT", initialValue = 100000000)
	private Long id;
	
	@Column(name = "DAY_END_BATCH_DATE", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date dayEndBatchDate;
	
	@Column(name = "ORDER_COUNT", nullable = false)
	private Integer orderCount;
	
	@Column(name = "ORDER_ITEM_COUNT", nullable = false)
	private Integer orderItemCount;
	
	@Column(name = "REFILL_COUNT", nullable = false)
	private Integer refillCount;
	
	@Column(name = "REFILL_ITEM_COUNT", nullable = false)
	private Integer refillItemCount;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE", referencedColumnName = "HOSP_CODE", nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDayEndBatchDate() {
		if (dayEndBatchDate == null) {
			return null;
		}
		else {
			return new Date(dayEndBatchDate.getTime());
		}
	}

	public void setDayEndBatchDate(Date dayEndBatchDate) {
		if (dayEndBatchDate == null) {
			this.dayEndBatchDate = null;
		}
		else {
			this.dayEndBatchDate = new Date(dayEndBatchDate.getTime());
		}
	}

	public Integer getOrderCount() {
		return orderCount;
	}

	public void setOrderCount(Integer orderCount) {
		this.orderCount = orderCount;
	}

	public Integer getOrderItemCount() {
		return orderItemCount;
	}

	public void setOrderItemCount(Integer orderItemCount) {
		this.orderItemCount = orderItemCount;
	}

	public Integer getRefillCount() {
		return refillCount;
	}

	public void setRefillCount(Integer refillCount) {
		this.refillCount = refillCount;
	}

	public Integer getRefillItemCount() {
		return refillItemCount;
	}

	public void setRefillItemCount(Integer refillItemCount) {
		this.refillItemCount = refillItemCount;
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}

}
