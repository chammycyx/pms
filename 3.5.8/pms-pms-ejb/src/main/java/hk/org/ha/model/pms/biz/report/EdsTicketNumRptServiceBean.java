package hk.org.ha.model.pms.biz.report;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderStatus;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.TicketNumRpt;
import hk.org.ha.model.pms.vo.report.TicketNumRptDtl;
import hk.org.ha.model.pms.vo.report.TicketNumRptHdr;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("edsTicketNumRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class EdsTicketNumRptServiceBean implements EdsTicketNumRptServiceLocal {
	@PersistenceContext
	private EntityManager em;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent; 
	
	@In 
	private Workstore workstore;

	private List<TicketNumRpt> ticketNumRptList;
	
	private boolean retrieveSuccess;
	
	private Map<String, Object> parameters = new HashMap<String, Object>();
	
	public List<TicketNumRpt> retrieveEdsTicketNumRptList(Date rptDate, String ticketNum) {
		ticketNumRptList = new ArrayList<TicketNumRpt>();

		List<DispOrderItem> dispOrderItemList = retrieveDispOrderItemList(rptDate, ticketNum);
				
		TicketNumRptHdr ticketNumRptHdr = setTicketNumRptHdr(dispOrderItemList);
		
		List<TicketNumRptDtl> ticketNumRptDtlList = retrieveTicketNumRptDtlList(dispOrderItemList);
		
		if (dispOrderItemList.size()>0){
			TicketNumRpt ticketNumRpt = new TicketNumRpt();
			ticketNumRpt.setTickeNumRptHdr(ticketNumRptHdr);
			ticketNumRpt.setTicketNumRptDtlList(ticketNumRptDtlList);
			ticketNumRptList.add(ticketNumRpt);
		}
		
		if ( ticketNumRptList.isEmpty() ) {
			retrieveSuccess = false;
		}
		else{
			retrieveSuccess = true;
		}
		
		return ticketNumRptList;
	}
	
	@SuppressWarnings("unchecked")
	private List<DispOrderItem> retrieveDispOrderItemList(Date rptDate, String ticketNum) {
		return (List<DispOrderItem>) em.createQuery(
				"select o from DispOrderItem o" + // 20120305 index check : Ticket.workstore,orderType,ticketDate,ticketNum : UI_TICKET_01
				" where o.dispOrder.ticket.workstore = :workstore" +
				" and o.dispOrder.ticket.orderType = :orderType" +
				" and o.dispOrder.ticket.ticketDate = :ticketDate" +
				" and o.dispOrder.ticket.ticketNum = :ticketNum" +
				" and (o.dispOrder.status not in :status" +
				" or (o.dispOrder.status =:statusDelete" +
				" or (o.dispOrder.status =:statusSysDelete and o.status =:doiStatus) ) )" +
				" order by o.pharmOrderItem.itemNum")
				.setParameter("workstore", workstore)
				.setParameter("orderType", OrderType.OutPatient)
				.setParameter("ticketDate", rptDate, TemporalType.DATE)
				.setParameter("ticketNum", ticketNum)
				.setParameter("status", DispOrderStatus.Deleted_SysDeleted)
				.setParameter("statusDelete", DispOrderStatus.Deleted)
				.setParameter("statusSysDelete", DispOrderStatus.SysDeleted)
				.setParameter("doiStatus", DispOrderItemStatus.Deleted)
				.getResultList();

		
	}
	
	private TicketNumRptHdr setTicketNumRptHdr(List<DispOrderItem> dispOrderItemList) {
		TicketNumRptHdr ticketNumRptHdr = new TicketNumRptHdr();
		
		if(dispOrderItemList.size() >0) {
			DispOrder dispOrder = dispOrderItemList.get(dispOrderItemList.size()-1).getDispOrder();
			ticketNumRptHdr.setTicketDate(dispOrder.getTicket().getTicketDate());
			ticketNumRptHdr.setTicketNum(dispOrder.getTicket().getTicketNum());

			if (dispOrder.getStatus() == DispOrderStatus.Deleted) {
				ticketNumRptHdr.setDispOrderStatus(OneStopOrderStatus.Deleted.getDisplayValue());
				
			} else if (dispOrder.getAdminStatus() == DispOrderAdminStatus.Suspended && dispOrder.getStatus() != DispOrderStatus.SysDeleted) {
				ticketNumRptHdr.setDispOrderStatus(OneStopOrderStatus.Suspended.getDisplayValue());
				
			}else {
				switch (dispOrder.getStatus())  {
				case Vetted:
				case Picking:
					ticketNumRptHdr.setDispOrderStatus(OneStopOrderStatus.Vetted.getDisplayValue());
					break;
				case Picked:
				case Assembling: 	
					ticketNumRptHdr.setDispOrderStatus(OneStopOrderStatus.Picking.getDisplayValue());
					break;
				case Assembled:
					ticketNumRptHdr.setDispOrderStatus(OneStopOrderStatus.Assembling.getDisplayValue());
					break;
				case Checked:
					ticketNumRptHdr.setDispOrderStatus(OneStopOrderStatus.Checked.getDisplayValue());
					break;
				case Issued:
					ticketNumRptHdr.setDispOrderStatus(OneStopOrderStatus.Issued.getDisplayValue());
					break;
				default:
					break;
				}
			}
			if ( dispOrder.getPharmOrder().getMedCase() != null ) {
				ticketNumRptHdr.setCaseNum(dispOrder.getPharmOrder().getMedCase().getCaseNum());
			}
			ticketNumRptHdr.setIssueWindowNum(dispOrder.getIssueWindowNum());
			ticketNumRptHdr.setHkid(dispOrder.getPharmOrder().getPatient().getHkid());
			ticketNumRptHdr.setPatName(dispOrder.getPharmOrder().getPatient().getName());
			ticketNumRptHdr.setPatNameChi(dispOrder.getPharmOrder().getPatient().getNameChi());
			
		}
		OperationProfile activeProfile = em.find(OperationProfile.class, workstore.getOperationProfileId());
		ticketNumRptHdr.setActiveProfileName(activeProfile.getName());
		ticketNumRptHdr.setHospCode(workstore.getHospCode());
		ticketNumRptHdr.setWorkstoreCode(workstore.getWorkstoreCode());
		

		return  ticketNumRptHdr;
	}
	
	private List<TicketNumRptDtl> retrieveTicketNumRptDtlList(List<DispOrderItem> dispOrderItemList){
		List<TicketNumRptDtl> ticketNumRptDtlList = new ArrayList<TicketNumRptDtl>();
		
		if(dispOrderItemList.size()>0){

			for(DispOrderItem dispOrderItem: dispOrderItemList){
				TicketNumRptDtl ticketNumRptDtl = new TicketNumRptDtl();
				ticketNumRptDtl.setItemNum(dispOrderItem.getPharmOrderItem().getItemNum().toString());
				ticketNumRptDtl.setItemCode(dispOrderItem.getPharmOrderItem().getItemCode());
				ticketNumRptDtl.setVetUser(dispOrderItem.getDispOrder().getPharmOrder().getUpdateUser());
				ticketNumRptDtl.setPickUser(dispOrderItem.getPickUser());
				ticketNumRptDtl.setAssembleUser(dispOrderItem.getAssembleUser());
				ticketNumRptDtl.setCheckUser(dispOrderItem.getDispOrder().getCheckUser());
				ticketNumRptDtl.setIssueUser(dispOrderItem.getDispOrder().getIssueUser());
				
				if (dispOrderItem.getDispOrder().getStatus() == DispOrderStatus.SysDeleted || dispOrderItem.getDispOrder().getStatus() == DispOrderStatus.Deleted) {
					ticketNumRptDtl.setDispOrderItemStatus(OneStopOrderStatus.Deleted.getDisplayValue());
					
				} else if (dispOrderItem.getStatus()==DispOrderItemStatus.KeepRecord) {
					if ( dispOrderItem.getPharmOrderItem().getMedOrderItem().getCapdRxDrug() != null && dispOrderItem.getPharmOrderItem().getCapdVoucherFlag() ){
						ticketNumRptDtl.setDispOrderItemStatus("CAPD");
					} else{
						ticketNumRptDtl.setDispOrderItemStatus("No Label");
					}
					
				}else if (dispOrderItem.getDispOrder().getAdminStatus() == DispOrderAdminStatus.Suspended) {
					ticketNumRptDtl.setDispOrderItemStatus("Supended");
					ticketNumRptDtl.setSuspendCode(dispOrderItem.getDispOrder().getSuspendCode());					
				}else {
					ticketNumRptDtl.setDispOrderItemStatus(dispOrderItem.getStatus().getDisplayValue());
				}
				
				ticketNumRptDtlList.add(ticketNumRptDtl);
			}
			
		}
		return ticketNumRptDtlList;
	}
	
	
	public void generateEdsTicketNumRpt() {
		OperationProfile activeProfile = em.find(OperationProfile.class, workstore.getOperationProfileId());

		parameters.put("hospCode", workstore.getHospCode());
		parameters.put("workstoreCode", workstore.getWorkstoreCode());	
		parameters.put("operationMode", activeProfile.getName());	
		
		printAgent.renderAndRedirect(new RenderJob(
				"TicketNumRpt", 
				new PrintOption(), 
				parameters, 
				ticketNumRptList));
	}
	
	public void printEdsTicketNumRpt(){

		printAgent.renderAndPrint(new RenderAndPrintJob(
				"TicketNumRpt", 
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				parameters, 
				ticketNumRptList));
		
	}
	
	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}

	@Remove
	public void destroy() {
		if(ticketNumRptList!=null){
			ticketNumRptList=null;
		}
		
	}

}
