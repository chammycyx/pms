package hk.org.ha.model.pms.workflow;

import javax.ejb.Stateless;

import hk.org.ha.fmk.pms.web.MeasureCalls;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;


@Stateless
@Name("dispensingPMSAction")
@Restrict("#{identity.loggedIn}")
@MeasureCalls
public class DispensingPMSAction implements DispensingPMSActionLocal {
    
	@Logger
	private Log logger;
	
	@In(scope=ScopeType.BUSINESS_PROCESS) 
    private String orderNo;
	
	public String checkClearance() {
		logger.info("CheckClearance: #0", orderNo);
		
		if (orderNo.equals("5001") || orderNo.equals("5002")) {
			return "no";
		} else {
			return "yes";
		}
	}
	
	public String forceProceed() {
		logger.info("forceProceed: #0", orderNo);
		
		if (orderNo.equals("5001")) {
			return "no";
		} else {
			return "yes";
		}
	}
	
	public void retrieveMDSInfo() {
		logger.info("retrieveMDSInfo: #0", orderNo);
	}
	
	public String mdsModified() {
		logger.info("mdsModified: #0", orderNo);
		
		if (orderNo.equals("5002") || orderNo.equals("5004")) {
			return "yes";
		} else {
			return "no";
		}
	}
	
	public void createPharmarcyOrder() {
		logger.info("createPharmarcyOrder: #0", orderNo);
	}
	
	public String hasRefill() {
		logger.info("hasRefill: #0", orderNo);
		
		if (orderNo.equals("5002") || orderNo.equals("5005")) {
			return "yes";
		} else {
			return "no";
		}
	}
	
	public void createDispensingOrder() {
		logger.info("createDispensingOrder: #0", orderNo);
	}
	
	public void createInvoices() {
		logger.info("createInvoice: #0", orderNo);
	}
	
	public String hasSFI() {
		logger.info("hasSFI: #0", orderNo);
		if (orderNo.equals("5002") || orderNo.equals("5006")) {
			return "yes";
		} else {
			return "no";
		}
	}
	
	public void printLabels() {
		logger.info("printLabels: #0", orderNo);
	}
}