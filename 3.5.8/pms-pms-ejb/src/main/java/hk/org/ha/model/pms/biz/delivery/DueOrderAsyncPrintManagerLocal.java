package hk.org.ha.model.pms.biz.delivery;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.model.pms.persistence.medprofile.Delivery;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryRequest;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryRequestGroup;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.vo.security.PropMap;

import java.util.List;

import javax.ejb.Local;

import org.jboss.seam.annotations.async.Asynchronous;

@Local
public interface DueOrderAsyncPrintManagerLocal {

	@Asynchronous
	void assignBatchAndPrint(Workstation workstation, UamInfo uamInfo, PropMap propMap, DeliveryRequest request);
	@Asynchronous
	void assignBatchAndPrintByDeliveryScheduleItem(Workstation workstation, UamInfo uamInfo, PropMap propMap, DeliveryRequestGroup requestGroup, List<DeliveryRequest> requestList);
	List<Delivery> retrieveDeliveryListForDrugRefillList(DeliveryRequest request, boolean includeDDnWardStockFlag);
}
