package hk.org.ha.model.pms.biz.reftable;
import javax.ejb.Local;

@Local
public interface ItemSpecialtyServiceLocal {
	boolean checkItemSpecialtyValidity(String itemCode, String chargeSpecCode);
	void destroy();
}
