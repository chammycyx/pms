package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class PivasWorkloadSummaryPatientRptDtl implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String itemCode;
	
	private String pivasFormulaDescWithoutRatio;
	
	private String pivasFormulaDesc;
	
	private BigDecimal ratio;
	
	private Boolean groupFirstDtlInd;
	
	private Boolean groupLastDtlInd;
	
	private String ward;
	
	private Integer totalNumOfDose;

	// for export only
	private Date supplyDate;
	
	public PivasWorkloadSummaryPatientRptDtl() {
		groupFirstDtlInd = false;
		groupLastDtlInd = false;
	}
	
	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getPivasFormulaDescWithoutRatio() {
		return pivasFormulaDescWithoutRatio;
	}

	public void setPivasFormulaDescWithoutRatio(String pivasFormulaDescWithoutRatio) {
		this.pivasFormulaDescWithoutRatio = pivasFormulaDescWithoutRatio;
	}

	public String getPivasFormulaDesc() {
		return pivasFormulaDesc;
	}

	public void setPivasFormulaDesc(String pivasFormulaDesc) {
		this.pivasFormulaDesc = pivasFormulaDesc;
	}

	public BigDecimal getRatio() {
		return ratio;
	}

	public void setRatio(BigDecimal ratio) {
		this.ratio = ratio;
	}

	public Boolean getGroupFirstDtlInd() {
		return groupFirstDtlInd;
	}

	public void setGroupFirstDtlInd(Boolean groupFirstDtlInd) {
		this.groupFirstDtlInd = groupFirstDtlInd;
	}

	public Boolean getGroupLastDtlInd() {
		return groupLastDtlInd;
	}

	public void setGroupLastDtlInd(Boolean groupLastDtlInd) {
		this.groupLastDtlInd = groupLastDtlInd;
	}

	public String getWard() {
		return ward;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}

	public Integer getTotalNumOfDose() {
		return totalNumOfDose;
	}

	public void setTotalNumOfDose(Integer totalNumOfDose) {
		this.totalNumOfDose = totalNumOfDose;
	}

	public Date getSupplyDate() {
		return supplyDate;
	}

	public void setSupplyDate(Date supplyDate) {
		this.supplyDate = supplyDate;
	}
}
