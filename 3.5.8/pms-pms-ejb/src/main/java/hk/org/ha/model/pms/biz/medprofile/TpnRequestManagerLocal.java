package hk.org.ha.model.pms.biz.medprofile;

import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.TpnRequest;

import java.util.List;

import javax.ejb.Local;

@Local
public interface TpnRequestManagerLocal {
	Long updateTpnRequest(MedProfile medProfile, List<TpnRequest> tpnRequestList, Long tpnSessionId);
}