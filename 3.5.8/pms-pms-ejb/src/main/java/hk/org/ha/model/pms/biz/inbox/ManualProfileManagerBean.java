package hk.org.ha.model.pms.biz.inbox;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.medprofile.MedProfileManagerLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;

import java.util.UUID;

import javax.ejb.Stateless;

import org.apache.commons.lang3.tuple.Pair;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("manualProfileManager")
@MeasureCalls
public class ManualProfileManagerBean implements ManualProfileManagerLocal {
	
	@In
	private MedProfileManagerLocal medProfileManager;

	@Override
	public MedProfile constructManualProfile(String hkid, String caseNum, Workstore workstore) {
		MedProfile medProfile = createNewMedProfile(workstore);
    	Patient patient;
    	
    	if (hkid != null) {
    		patient = medProfileManager.retrievePasPatient(hkid);
    		medProfile.setPatient(patient);
    	} else if (caseNum != null) {
    		Pair<String, Patient> retrievePasPatientResult = medProfileManager.retrievePasPatient(medProfile.getWorkstore(), caseNum);
    		if (retrievePasPatientResult != null) {
	    		medProfile.setPatHospCode(retrievePasPatientResult.getLeft());
	    		medProfile.setPatient(retrievePasPatientResult.getRight());
	        	medProfile.setMedCase(MedProfileUtils.lookupPasMedCase(medProfile.getPatient(), caseNum));
	        }
        	if (medProfile.getMedCase() != null) {
        		medProfileManager.initializeProfileState(medProfile);
        	}    		
    	} else {
    		throw new IllegalArgumentException("Invalid HKID/CaseNum to construct IP Manual Profile");
    	}
    	
    	assignNewPatientAndMedCase(medProfile, hkid, caseNum);
    	
    	return medProfile;
	}
    
    private MedProfile createNewMedProfile(Workstore workstore) {

    	MedProfile medProfile = new MedProfile();
    	medProfile.setWorkstore(workstore);
    	medProfile.setPatHospCode(workstore.getDefPatHospCode());
    	medProfile.setOrderNum(UUID.randomUUID().toString());
    	return medProfile;
    }
	
	private void assignNewPatientAndMedCase(MedProfile medProfile, String hkid, String caseNum) {
	    
    	if (medProfile.getPatient() == null) {
    		Patient patient = new Patient();
    		patient.setHkid(hkid);
    		medProfile.setPatient(patient);
    	}
    	
    	if (medProfile.getMedCase() == null) {
    		MedCase medCase = new MedCase();
    		medCase.setCaseNum(caseNum);
    		medProfile.setMedCase(medCase);
    	}
    }
}
