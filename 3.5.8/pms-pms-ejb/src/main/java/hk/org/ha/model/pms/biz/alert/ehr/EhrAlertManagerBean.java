package hk.org.ha.model.pms.biz.alert.ehr;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.alert.ehr.EhrAllergyException;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.udt.alert.AlertProfileStatus;
import hk.org.ha.model.pms.udt.alert.ehr.EhrAlertStatus;
import hk.org.ha.model.pms.vo.alert.AlertProfile;
import hk.org.ha.model.pms.vo.alert.ehr.EhrAllergySummary;
import hk.org.ha.service.pms.asa.interfaces.alert.EhrAlertServiceJmsRemote;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("ehrAlertManager")
@MeasureCalls
public class EhrAlertManagerBean implements EhrAlertManagerLocal {

	@In
	private EhrAlertServiceJmsRemote ehrAlertServiceProxy;

	public AlertProfile retrieveEhrAllergySummaryByPharmOrder(AlertProfile alertProfile, PharmOrder pharmOrder) throws EhrAllergyException {
		return retrieveEhrAllergySummary(alertProfile, pharmOrder.getMedOrder().getPatHospCode(), pharmOrder.getPatKey(), pharmOrder.getHkid());
	}
	
	public AlertProfile retrieveEhrAllergySummaryByMedProfile(AlertProfile alertProfile, MedProfile medProfile) throws EhrAllergyException {
		return retrieveEhrAllergySummary(alertProfile, medProfile.getPatHospCode(), medProfile.getPatient().getPatKey(), medProfile.getPatient().getHkid());
	}

	public AlertProfile retrieveEhrAllergySummaryByMedOrder(AlertProfile alertProfile, MedOrder medOrder, Patient patient) throws EhrAllergyException {
		return retrieveEhrAllergySummary(alertProfile, medOrder.getPatHospCode(), patient.getPatKey(), patient.getHkid());
	}

	public AlertProfile retrieveEhrAllergySummary(AlertProfile alertProfile, String patHospCode, String patKey, String hkid) throws EhrAllergyException {

		EhrAllergySummary allergySummary = ehrAlertServiceProxy.retrieveEhrAllergySummary(patHospCode, patKey, hkid);

		if (alertProfile == null) {
			alertProfile = new AlertProfile();
		}
		if (alertProfile.getStatus() == AlertProfileStatus.Error || alertProfile.getStatus() == null) {
			alertProfile.setEhrAllergyOnly(true);
		}

		if (allergySummary.getAllergy() != null || allergySummary.getAdr() != null) {
			alertProfile.setStatus(AlertProfileStatus.RecordExist);
		}
		else if (alertProfile.isEhrAllergyOnly()) {
			if (allergySummary.getStatus() == EhrAlertStatus.Exist) {
				alertProfile.setStatus(AlertProfileStatus.NoRecord);
			}
			else {
				alertProfile.setStatus(AlertProfileStatus.Error);
			}
		}
		alertProfile.setAllergySummary(allergySummary);
		return alertProfile;
	}
}
