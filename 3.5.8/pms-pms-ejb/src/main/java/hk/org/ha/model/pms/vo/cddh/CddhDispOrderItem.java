package hk.org.ha.model.pms.vo.cddh;

import hk.org.ha.model.pms.udt.cddh.CddhRemarkStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderItemRemarkType;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.udt.vetting.RegimenDurationUnit;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class CddhDispOrderItem implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String moRefNum;
	
	private OrderType moOrderType;

	private MedOrderDocType moDocType;

	private String moiFmStatus;

	private String moiDrugOrderText;
	
	private String moiAlertDesc;
	
	private String moiAlertRemark;

	private String poWardCode;
	
	private String poDoctorCode;
	
	private String poDoctorName;

	private String poCaseNum;

	private String poPasPatGroupCode;
	
	private String poPasPayCode;
	
	private String poiItemCode;

	private Integer poiItemNum;
	
	private String poiFmStatus;
	
	private ActionStatus poiActionStatus;
	
	private String poiTradeName;
	
	private Boolean poiCapdVoucherFlag;
	
	private String poiBaseUnit;

	private String poiRemarkText;
	
	private Boolean poiMultiDoseFlag;

	private Integer poiRegimenDuration;
	
	private RegimenDurationUnit poiRegimenDurationUnit;
	
	private String poiDmDrugCommodityGroup;

	private String poiDmDrugCommodityType;
	
	private Long doId;
	
	private Date doIssueDate;

	private Boolean doRefillFlag;
	
	private String doSpecCode;

	private String doPatCatCode;
	
	private CddhRemarkStatus doCddhRemarkStatus;
	
	private String doHospCode;

	private String doWorkstoreCode;
	
	private String doCddhWorkstoreGroupCode;
	
	private String doCddhSpecCode;

	private Long doiId;
	
	private BigDecimal doiDispQty;

	private Integer doiDurationInDay;
	
	private Date doiStartDate;

	private Date doiEndDate;
	
	private String doiWarnCode1;
	
	private String doiWarnCode2;
	
	private String doiWarnCode3;
	
	private String doiWarnCode4;

	private String doiWarnDesc1;

	private String doiWarnDesc2;

	private String doiWarnDesc3;

	private String doiWarnDesc4;

	private String doiRemarkText;

	private DispOrderItemRemarkType doiRemarkType;
	
	private String doiRemarkUpdateUser;

	private Date doiRemarkUpdateDate;

	private BigDecimal doiUnitPrice;
	
	private String doiChargeSpecCode;
	
	private Boolean doiLegacy;

	private Boolean doiLegacyMultiDose;
	
	private String doiSingleDosageValue;
	
	private String doiEpisodeType;
	
	private String doiOverrideSeqNum;
	
	private String doiItemCatCode;
	
	private String doiCddhDrugDesc;
	
	private Boolean doiPivasFlag;	
	
	private Boolean doiBaseLabelExist;
	
	private Boolean doiBaseLabelInstructionListExist;
	
	private String doiBaseLabelItemDesc;
	
	private String doiBaseLabelInstructionText;
	
	private String ticketNum;

	private Date ticketDate;

	public String getMoRefNum() {
		return moRefNum;
	}

	public void setMoRefNum(String moRefNum) {
		this.moRefNum = moRefNum;
	}

	public OrderType getMoOrderType() {
		return moOrderType;
	}

	public void setMoOrderType(OrderType moOrderType) {
		this.moOrderType = moOrderType;
	}

	public MedOrderDocType getMoDocType() {
		return moDocType;
	}

	public void setMoDocType(MedOrderDocType moDocType) {
		this.moDocType = moDocType;
	}

	public String getMoiFmStatus() {
		return moiFmStatus;
	}

	public void setMoiFmStatus(String moiFmStatus) {
		this.moiFmStatus = moiFmStatus;
	}

	public String getMoiDrugOrderText() {
		return moiDrugOrderText;
	}

	public void setMoiDrugOrderText(String moiDrugOrderText) {
		this.moiDrugOrderText = moiDrugOrderText;
	}

	public String getPoWardCode() {
		return poWardCode;
	}

	public void setPoWardCode(String poWardCode) {
		this.poWardCode = poWardCode;
	}

	public String getPoDoctorCode() {
		return poDoctorCode;
	}

	public void setPoDoctorCode(String poDoctorCode) {
		this.poDoctorCode = poDoctorCode;
	}

	public String getPoDoctorName() {
		return poDoctorName;
	}

	public void setPoDoctorName(String poDoctorName) {
		this.poDoctorName = poDoctorName;
	}

	public String getPoCaseNum() {
		return poCaseNum;
	}

	public void setPoCaseNum(String poCaseNum) {
		this.poCaseNum = poCaseNum;
	}

	public String getPoPasPatGroupCode() {
		return poPasPatGroupCode;
	}

	public void setPoPasPatGroupCode(String poPasPatGroupCode) {
		this.poPasPatGroupCode = poPasPatGroupCode;
	}

	public String getPoPasPayCode() {
		return poPasPayCode;
	}

	public void setPoPasPayCode(String poPasPayCode) {
		this.poPasPayCode = poPasPayCode;
	}

	public String getPoiItemCode() {
		return poiItemCode;
	}

	public void setPoiItemCode(String poiItemCode) {
		this.poiItemCode = poiItemCode;
	}

	public Integer getPoiItemNum() {
		return poiItemNum;
	}

	public void setPoiItemNum(Integer poiItemNum) {
		this.poiItemNum = poiItemNum;
	}

	public String getPoiFmStatus() {
		return poiFmStatus;
	}

	public void setPoiFmStatus(String poiFmStatus) {
		this.poiFmStatus = poiFmStatus;
	}

	public ActionStatus getPoiActionStatus() {
		return poiActionStatus;
	}

	public void setPoiActionStatus(ActionStatus poiActionStatus) {
		this.poiActionStatus = poiActionStatus;
	}

	public String getPoiTradeName() {
		return poiTradeName;
	}

	public void setPoiTradeName(String poiTradeName) {
		this.poiTradeName = poiTradeName;
	}

	public Boolean getPoiCapdVoucherFlag() {
		return poiCapdVoucherFlag;
	}

	public void setPoiCapdVoucherFlag(Boolean poiCapdVoucherFlag) {
		this.poiCapdVoucherFlag = poiCapdVoucherFlag;
	}

	public String getPoiBaseUnit() {
		return poiBaseUnit;
	}

	public void setPoiBaseUnit(String poiBaseUnit) {
		this.poiBaseUnit = poiBaseUnit;
	}

	public String getPoiRemarkText() {
		return poiRemarkText;
	}

	public void setPoiRemarkText(String poiRemarkText) {
		this.poiRemarkText = poiRemarkText;
	}

	public Boolean getPoiMultiDoseFlag() {
		return poiMultiDoseFlag;
	}

	public void setPoiMultiDoseFlag(Boolean poiMultiDoseFlag) {
		this.poiMultiDoseFlag = poiMultiDoseFlag;
	}

	public Integer getPoiRegimenDuration() {
		return poiRegimenDuration;
	}

	public void setPoiRegimenDuration(Integer poiRegimenDuration) {
		this.poiRegimenDuration = poiRegimenDuration;
	}

	public RegimenDurationUnit getPoiRegimenDurationUnit() {
		return poiRegimenDurationUnit;
	}

	public void setPoiRegimenDurationUnit(RegimenDurationUnit poiRegimenDurationUnit) {
		this.poiRegimenDurationUnit = poiRegimenDurationUnit;
	}

	public String getPoiDmDrugCommodityGroup() {
		return poiDmDrugCommodityGroup;
	}

	public void setPoiDmDrugCommodityGroup(String poiDmDrugCommodityGroup) {
		this.poiDmDrugCommodityGroup = poiDmDrugCommodityGroup;
	}

	public String getPoiDmDrugCommodityType() {
		return poiDmDrugCommodityType;
	}

	public void setPoiDmDrugCommodityType(String poiDmDrugCommodityType) {
		this.poiDmDrugCommodityType = poiDmDrugCommodityType;
	}

	public Long getDoId() {
		return doId;
	}

	public void setDoId(Long doId) {
		this.doId = doId;
	}

	public Date getDoIssueDate() {
		if (doIssueDate == null) {
			return null;
		} else {
			return new Date(doIssueDate.getTime());
		}
	}

	public void setDoIssueDate(Date doIssueDate) {
		if (doIssueDate == null) {
			this.doIssueDate = null;
		} else {
			this.doIssueDate = new Date(doIssueDate.getTime());
		}
	}

	public Boolean getDoRefillFlag() {
		return doRefillFlag;
	}

	public void setDoRefillFlag(Boolean doRefillFlag) {
		this.doRefillFlag = doRefillFlag;
	}

	public String getDoSpecCode() {
		return doSpecCode;
	}

	public void setDoSpecCode(String doSpecCode) {
		this.doSpecCode = doSpecCode;
	}

	public String getDoPatCatCode() {
		return doPatCatCode;
	}

	public void setDoPatCatCode(String doPatCatCode) {
		this.doPatCatCode = doPatCatCode;
	}

	public CddhRemarkStatus getDoCddhRemarkStatus() {
		return doCddhRemarkStatus;
	}

	public void setDoCddhRemarkStatus(CddhRemarkStatus doCddhRemarkStatus) {
		this.doCddhRemarkStatus = doCddhRemarkStatus;
	}

	public String getDoHospCode() {
		return doHospCode;
	}

	public void setDoHospCode(String doHospCode) {
		this.doHospCode = doHospCode;
	}

	public String getDoWorkstoreCode() {
		return doWorkstoreCode;
	}

	public void setDoWorkstoreCode(String doWorkstoreCode) {
		this.doWorkstoreCode = doWorkstoreCode;
	}

	public String getDoCddhWorkstoreGroupCode() {
		return doCddhWorkstoreGroupCode;
	}

	public void setDoCddhWorkstoreGroupCode(String doCddhWorkstoreGroupCode) {
		this.doCddhWorkstoreGroupCode = doCddhWorkstoreGroupCode;
	}

	public String getDoCddhSpecCode() {
		return doCddhSpecCode;
	}

	public void setDoCddhSpecCode(String doCddhSpecCode) {
		this.doCddhSpecCode = doCddhSpecCode;
	}

	public Long getDoiId() {
		return doiId;
	}

	public void setDoiId(Long doiId) {
		this.doiId = doiId;
	}

	public BigDecimal getDoiDispQty() {
		return doiDispQty;
	}

	public void setDoiDispQty(BigDecimal doiDispQty) {
		this.doiDispQty = doiDispQty;
	}

	public Integer getDoiDurationInDay() {
		return doiDurationInDay;
	}

	public void setDoiDurationInDay(Integer doiDurationInDay) {
		this.doiDurationInDay = doiDurationInDay;
	}

	public Date getDoiStartDate() {
		if (doiStartDate == null) {
			return null;
		} else {
			return new Date(doiStartDate.getTime());
		}
	}

	public void setDoiStartDate(Date doiStartDate) {
		if (doiStartDate == null) {
			this.doiStartDate = null;
		} else {
			this.doiStartDate = new Date(doiStartDate.getTime());
		}
	}

	public Date getDoiEndDate() {
		if (doiEndDate == null) {
			return null;
		} else {
			return new Date(doiEndDate.getTime());
		}
	}

	public void setDoiEndDate(Date doiEndDate) {
		if (doiEndDate == null) {
			this.doiEndDate = null;
		} else {
			this.doiEndDate = new Date(doiEndDate.getTime());
		}
	}

	public String getDoiWarnCode1() {
		return doiWarnCode1;
	}

	public void setDoiWarnCode1(String doiWarnCode1) {
		this.doiWarnCode1 = doiWarnCode1;
	}

	public String getDoiWarnCode2() {
		return doiWarnCode2;
	}

	public void setDoiWarnCode2(String doiWarnCode2) {
		this.doiWarnCode2 = doiWarnCode2;
	}

	public String getDoiWarnCode3() {
		return doiWarnCode3;
	}

	public void setDoiWarnCode3(String doiWarnCode3) {
		this.doiWarnCode3 = doiWarnCode3;
	}

	public String getDoiWarnCode4() {
		return doiWarnCode4;
	}

	public void setDoiWarnCode4(String doiWarnCode4) {
		this.doiWarnCode4 = doiWarnCode4;
	}

	public String getDoiWarnDesc1() {
		return doiWarnDesc1;
	}

	public void setDoiWarnDesc1(String doiWarnDesc1) {
		this.doiWarnDesc1 = doiWarnDesc1;
	}

	public String getDoiWarnDesc2() {
		return doiWarnDesc2;
	}

	public void setDoiWarnDesc2(String doiWarnDesc2) {
		this.doiWarnDesc2 = doiWarnDesc2;
	}

	public String getDoiWarnDesc3() {
		return doiWarnDesc3;
	}

	public void setDoiWarnDesc3(String doiWarnDesc3) {
		this.doiWarnDesc3 = doiWarnDesc3;
	}

	public String getDoiWarnDesc4() {
		return doiWarnDesc4;
	}

	public void setDoiWarnDesc4(String doiWarnDesc4) {
		this.doiWarnDesc4 = doiWarnDesc4;
	}

	public String getDoiRemarkText() {
		return doiRemarkText;
	}

	public void setDoiRemarkText(String doiRemarkText) {
		this.doiRemarkText = doiRemarkText;
	}

	public DispOrderItemRemarkType getDoiRemarkType() {
		return doiRemarkType;
	}

	public void setDoiRemarkType(DispOrderItemRemarkType doiRemarkType) {
		this.doiRemarkType = doiRemarkType;
	}

	public String getDoiRemarkUpdateUser() {
		return doiRemarkUpdateUser;
	}

	public void setDoiRemarkUpdateUser(String doiRemarkUpdateUser) {
		this.doiRemarkUpdateUser = doiRemarkUpdateUser;
	}

	public Date getDoiRemarkUpdateDate() {
		if (doiRemarkUpdateDate == null) {
			return null;
		} else {
			return new Date(doiRemarkUpdateDate.getTime());
		}
	}

	public void setDoiRemarkUpdateDate(Date doiRemarkUpdateDate) {
		if (doiRemarkUpdateDate == null) {
			this.doiRemarkUpdateDate = null;
		} else {
			this.doiRemarkUpdateDate = new Date(doiRemarkUpdateDate.getTime());
		}
	}

	public BigDecimal getDoiUnitPrice() {
		return doiUnitPrice;
	}

	public void setDoiUnitPrice(BigDecimal doiUnitPrice) {
		this.doiUnitPrice = doiUnitPrice;
	}

	public String getDoiChargeSpecCode() {
		return doiChargeSpecCode;
	}

	public void setDoiChargeSpecCode(String doiChargeSpecCode) {
		this.doiChargeSpecCode = doiChargeSpecCode;
	}

	public Boolean getDoiLegacy() {
		return doiLegacy;
	}

	public void setDoiLegacy(Boolean doiLegacy) {
		this.doiLegacy = doiLegacy;
	}

	public Boolean getDoiLegacyMultiDose() {
		return doiLegacyMultiDose;
	}

	public void setDoiLegacyMultiDose(Boolean doiLegacyMultiDose) {
		this.doiLegacyMultiDose = doiLegacyMultiDose;
	}

	public String getDoiSingleDosageValue() {
		return doiSingleDosageValue;
	}

	public void setDoiSingleDosageValue(String doiSingleDosageValue) {
		this.doiSingleDosageValue = doiSingleDosageValue;
	}

	public String getDoiEpisodeType() {
		return doiEpisodeType;
	}

	public void setDoiEpisodeType(String doiEpisodeType) {
		this.doiEpisodeType = doiEpisodeType;
	}

	public String getDoiOverrideSeqNum() {
		return doiOverrideSeqNum;
	}

	public void setDoiOverrideSeqNum(String doiOverrideSeqNum) {
		this.doiOverrideSeqNum = doiOverrideSeqNum;
	}

	public String getDoiItemCatCode() {
		return doiItemCatCode;
	}

	public void setDoiItemCatCode(String doiItemCatCode) {
		this.doiItemCatCode = doiItemCatCode;
	}

	public String getDoiCddhDrugDesc() {
		return doiCddhDrugDesc;
	}

	public void setDoiCddhDrugDesc(String doiCddhDrugDesc) {
		this.doiCddhDrugDesc = doiCddhDrugDesc;
	}
	
	public Boolean getDoiPivasFlag() {
		return doiPivasFlag;
	}

	public void setDoiPivasFlag(Boolean doiPivasFlag) {
		this.doiPivasFlag = doiPivasFlag;
	}

	public Boolean getDoiBaseLabelExist() {
		return doiBaseLabelExist;
	}

	public void setDoiBaseLabelExist(Boolean doiBaseLabelExist) {
		this.doiBaseLabelExist = doiBaseLabelExist;
	}

	public Boolean getDoiBaseLabelInstructionListExist() {
		return doiBaseLabelInstructionListExist;
	}

	public void setDoiBaseLabelInstructionListExist(Boolean doiBaseLabelInstructionListExist) {
		this.doiBaseLabelInstructionListExist = doiBaseLabelInstructionListExist;
	}

	public String getDoiBaseLabelItemDesc() {
		return doiBaseLabelItemDesc;
	}

	public void setDoiBaseLabelItemDesc(String doiBaseLabelItemDesc) {
		this.doiBaseLabelItemDesc = doiBaseLabelItemDesc;
	}

	public String getDoiBaseLabelInstructionText() {
		return doiBaseLabelInstructionText;
	}

	public void setDoiBaseLabelInstructionText(String doiBaseLabelInstructionText) {
		this.doiBaseLabelInstructionText = doiBaseLabelInstructionText;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public Date getTicketDate() {
		if (ticketDate == null) {
			return null;
		} else {
			return new Date(ticketDate.getTime());
		}
	}

	public void setTicketDate(Date ticketDate) {
		if (ticketDate == null) {
			this.ticketDate = null;
		} else {
			this.ticketDate = new Date(ticketDate.getTime());
		}
	}

	public void setMoiAlertDesc(String moiAlertDesc) {
		this.moiAlertDesc = moiAlertDesc;
	}

	public String getMoiAlertDesc() {
		return moiAlertDesc;
	}

	public void setMoiAlertRemark(String moiAlertRemark) {
		this.moiAlertRemark = moiAlertRemark;
	}

	public String getMoiAlertRemark() {
		return moiAlertRemark;
	}	
}
