package hk.org.ha.model.pms.biz.medprofile;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.inbox.PharmInboxClientNotifierInf;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("medProfileStatTrxManager")
@TransactionManagement(TransactionManagementType.BEAN)
@MeasureCalls
public class MedProfileStatTrxManagerBean implements MedProfileStatTrxManagerLocal {

	@Logger
	private Log logger;	

	@PersistenceContext
	private EntityManager em;

	@In
	private MedProfileStatManagerLocal medProfileStatManager;
	
	@Resource
	private UserTransaction userTransaction;
	
	@In
	private PharmInboxClientNotifierInf pharmInboxClientNotifier;
	
    public MedProfileStatTrxManagerBean() {
    }
    
    @Override
    public void recalculateStat(Collection<MedProfile> medProfileList, boolean recalculateMedProfileStat, boolean recalculateErrorStat,
    		boolean recalculateMedProfilePoItemStat) {
    	
    	Set<String> updatedHospCodeSet = new HashSet<String>(); 
    	
    	for (MedProfile medProfile: medProfileList) {
    		
    		boolean success = false;
    		
    		try {
    			beginTransaction();
    			
    			medProfile = fetchLockedMedProfile(medProfile);
    			if (medProfile == null) {
    				continue;
    			}
    			
    			if (recalculateErrorStat) {
    				medProfileStatManager.recalculateMedProfileErrorStat(medProfile);
    			}
    			if (recalculateMedProfilePoItemStat) {
    				medProfileStatManager.recalculateMedProfilePoItemStat(medProfile);
    			}
    			if (recalculateMedProfileStat) {
    				medProfileStatManager.recalculateMedProfileStat(medProfile, false);
    			}
    			updatedHospCodeSet.add(medProfile.getHospCode());
    			success = true;
    		
    		} catch (Exception ex) {
    			logger.error(ex, ex);
    		} finally {
    			endTransaction(success);
    		}
    	}
    	pharmInboxClientNotifier.dispatchUpdateEvent(updatedHospCodeSet);
    }
    
    @SuppressWarnings("unchecked")
	private MedProfile fetchLockedMedProfile(MedProfile medProfile) {
    	
    	return (MedProfile) MedProfileUtils.getFirstItem(em.createQuery(
    			"select p from MedProfile p" + // 20151012 index check : MedProfile.id : PK_MED_PROFILE
    			" where p.id in :idList")
    			.setParameter("idList", Arrays.asList(medProfile.getId()))
    			.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
    			.getResultList());
    }
    
	private void beginTransaction() throws NotSupportedException, SystemException {
		beginTransaction(0);
	}
	
	private void beginTransaction(int timeout) throws NotSupportedException, SystemException {
		if (timeout > 0) {
			userTransaction.setTransactionTimeout(timeout);
		}
		userTransaction.begin();
	}
	
	private void endTransaction(boolean success) {
		if (success) {
			commit();
		} else {
			rollback();
		}
	}

	private void commit() {
		try {
			userTransaction.commit();
		} catch (Exception ex) {
		}
	}	
	
	private void rollback() {
		try {
			userTransaction.rollback();
		} catch (Exception ex) {
		}
	}
}
