package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.List;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class EdsWorkloadRpt implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private EdsWorkloadRptHdr edsWorkloadRptHdr;
	
	private List<EdsWorkloadRptDtl> edsWorkloadRptDtlList;

	public EdsWorkloadRptHdr getEdsWorkloadRptHdr() {
		return edsWorkloadRptHdr;
	}

	public void setEdsWorkloadRptHdr(EdsWorkloadRptHdr edsWorkloadRptHdr) {
		this.edsWorkloadRptHdr = edsWorkloadRptHdr;
	}

	public List<EdsWorkloadRptDtl> getEdsWorkloadRptDtlList() {
		return edsWorkloadRptDtlList;
	}

	public void setEdsWorkloadRptDtlList(
			List<EdsWorkloadRptDtl> edsWorkloadRptDtlList) {
		this.edsWorkloadRptDtlList = edsWorkloadRptDtlList;
	}



		
}