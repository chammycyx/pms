package hk.org.ha.model.pms.biz.order;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;

import javax.ejb.Local;

@Local
public interface AutoDispManagerLocal {

	void login(String username, Workstore workstore, String workstationCode) throws Exception;
	
	OperationProfile getActiveProfile(Workstore workstore);
	
	void genTicket();
	
	void retrieveOrder(String orderNum);
	
	void retrievePatientAndMedCase(String patientHkid, String patientName);
		
	void checkTicket();
	
	void startVetting() throws Exception;

	void removeItemsIfNotConverted() throws Exception;
	
	void endVetting() throws Exception;
	
	void drugPick();
	
	void assembling();

	void checkOrder() throws Exception;
	
	void issueOrder() throws Exception;
	
	void cddhSearch(String patientHkid, String patientName);

	void drugSearch(String drugName);

	void logout();	
	
}
