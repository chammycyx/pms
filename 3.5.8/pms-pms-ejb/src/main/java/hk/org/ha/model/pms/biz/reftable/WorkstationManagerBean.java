package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.Workstation;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("workstationManager")
@MeasureCalls
public class WorkstationManagerBean implements WorkstationManagerLocal {

	@PersistenceContext
	private EntityManager em;

	public Workstation retrieveWorkstationByUamInfo(UamInfo uamInfo) {
		return retrieveWorkstation(
				uamInfo.getWorkstationId(),
				uamInfo.getHospital(),
				uamInfo.getWorkstore()
			);
	}
	
	public Workstation retrieveWorkstationByHostNameWorkstore(String hostName, Workstore workstore) {
		return retrieveWorkstation(
				hostName,
				workstore.getHospCode(),
				workstore.getWorkstoreCode()
			);
	}

	@SuppressWarnings("unchecked")
	private Workstation retrieveWorkstation(String hostName, String hospCode, String workstoreCode) {
		List<Workstation> workstationList = em.createQuery(
				"select o from Workstation o" + // 20120303 index check : Workstation.workstore : FK_WORKSTATION_01
				" where o.hostName = :hostName" +
				" and o.workstore.hospCode = :hospCode" +
				" and o.workstore.workstoreCode = :workstoreCode")
				.setParameter("hostName",      hostName)
				.setParameter("hospCode",      hospCode)
				.setParameter("workstoreCode", workstoreCode)
				.getResultList();
		if (workstationList.isEmpty()) {
			return null;
		}
		else {
			return workstationList.get(0);
		}
	}

	public Workstation retrieveWorkstation(Workstore workstore, String workstationCode) {
		return (Workstation) em.createQuery(
				"select o from Workstation o" + // 20120612 index check : Workstation.workstore : FK_WORKSTATION_01
				" where o.workstore = :workstore" +
				" and o.workstationCode = :workstationCode")
				.setParameter("workstore", workstore)
				.setParameter("workstationCode", workstationCode)
				.getSingleResult();
	}
}
