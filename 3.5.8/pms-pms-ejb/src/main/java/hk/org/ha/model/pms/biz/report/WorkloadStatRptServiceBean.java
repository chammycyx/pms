package hk.org.ha.model.pms.biz.report;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.corp.persistence.report.MpWorkloadStat;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.persistence.reftable.WorkloadStat;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.WorkloadStatRpt;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.apache.commons.lang.time.DateUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.joda.time.DateTime;

@Stateful
@Scope(ScopeType.SESSION)
@Name("workloadStatRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class WorkloadStatRptServiceBean implements WorkloadStatRptServiceLocal {
	@PersistenceContext
	private EntityManager em;
	
	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent; 
	
	@In 
	private Workstore workstore;

	private boolean retrieveSuccess;
	
	private List<WorkloadStatRpt> workloadStatRptList;

	private Map<String, Object> parameters = new HashMap<String, Object>();
	
	private DateFormat batchDateFormat = new SimpleDateFormat("yyyyMMdd");
	
	public void retrieveWorkloadStatList(Integer pastDay, Date dateFrom, Date dateTo) {
		constructWorkloadStatList(pastDay, new Date(), dateFrom, dateTo);	
	}
	
	@SuppressWarnings("unchecked")
	public List<WorkloadStatRpt> constructWorkloadStatList(Integer pastDay, Date todayWithTime, Date dateFromWithTime, Date dateToWithTime) {
		Date startDate = null;
		Date endDate = null;
		
		Date today = DateUtils.truncate(todayWithTime, Calendar.DAY_OF_MONTH);
		DateTime todayDt = new DateTime(today);
		
		if (dateFromWithTime != null && dateToWithTime != null){
			startDate = DateUtils.truncate(dateFromWithTime, Calendar.DAY_OF_MONTH);
			
			DateTime endDateDt = new DateTime(DateUtils.truncate(dateToWithTime, Calendar.DAY_OF_MONTH));
			if (endDateDt.isBefore(todayDt)) {
				endDate = endDateDt.toDate();
			} else {
				endDate = todayDt.minusDays(1).toDate();
			}
		} else {
			startDate = todayDt.minusDays(pastDay.intValue()).toDate();
			endDate = todayDt.minusDays(1).toDate();
		}
		
		
		List<WorkloadStat> opWorkloadStatList = em.createQuery(
				"select o from WorkloadStat o" + // 20120306 index check : WorkloadStat.workstore,dayEndBatchDate : UI_WORKLOAD_STAT_01
				" where o.workstore = :workstore"+
				" and o.dayEndBatchDate between :startDate and :endDate "+
				" order by o.dayEndBatchDate desc")
				.setParameter("workstore", workstore)
				.setParameter("startDate", startDate, TemporalType.DATE)
				.setParameter("endDate", endDate, TemporalType.DATE)
				.getResultList();
		
		List<MpWorkloadStat> mpWorkloadStatList = corpPmsServiceProxy.retrieveMpWorkloadStatRptList(workstore, startDate, endDate);
		
		
		Map<String, WorkloadStat> opWorkloadStatMap = new HashMap<String, WorkloadStat>();
		for (WorkloadStat workloadStat : opWorkloadStatList) {
			opWorkloadStatMap.put(batchDateFormat.format(workloadStat.getDayEndBatchDate()), workloadStat);
		}

		Map<String, MpWorkloadStat> mpWorkloadStatMap = new HashMap<String, MpWorkloadStat>();
		for (MpWorkloadStat mpWorkloadStat : mpWorkloadStatList) {
			mpWorkloadStatMap.put(batchDateFormat.format(mpWorkloadStat.getDayEndBatchDate()), mpWorkloadStat);
		}

		
		workloadStatRptList = new ArrayList<WorkloadStatRpt>();
		
		if ( ! opWorkloadStatList.isEmpty() || ! mpWorkloadStatList.isEmpty() ) {
			for (DateTime batchDateDt = new DateTime(startDate); 
					batchDateDt.isBefore(new DateTime(endDate)) || batchDateDt.equals(new DateTime(endDate)); 
					batchDateDt = batchDateDt.plusDays(1)) {
				
				String batchDateKey = batchDateFormat.format(batchDateDt.toDate());
				
				WorkloadStatRpt workloadStatRpt = new WorkloadStatRpt();
				
				workloadStatRpt.setDayEndBatchDate(batchDateDt.toDate());
				
				if (opWorkloadStatMap.containsKey(batchDateKey)) {
					WorkloadStat workloadStat = opWorkloadStatMap.get(batchDateKey);
					workloadStatRpt.setOpOrderCount(workloadStat.getOrderCount());
					workloadStatRpt.setOpOrderItemCount(workloadStat.getOrderItemCount());
					workloadStatRpt.setOpRefillCount(workloadStat.getRefillCount());
					workloadStatRpt.setOpRefillItemCount(workloadStat.getRefillItemCount());
					
				} else {
					workloadStatRpt.setOpOrderCount(0);
					workloadStatRpt.setOpOrderItemCount(0);
					workloadStatRpt.setOpRefillCount(0);
					workloadStatRpt.setOpRefillItemCount(0);
				}
				
				if (mpWorkloadStatMap.containsKey(batchDateKey)) {
					MpWorkloadStat mpWorkloadStat = mpWorkloadStatMap.get(batchDateKey);
					workloadStatRpt.setMpItemCount(mpWorkloadStat.getItemCount());
					workloadStatRpt.setMpSfiItemCount(mpWorkloadStat.getSfiItemCount());
					workloadStatRpt.setMpSafetyNetItemCount(mpWorkloadStat.getSafetyNetItemCount());
					workloadStatRpt.setMpWardStockItemCount(mpWorkloadStat.getWardStockItemCount());
					workloadStatRpt.setMpDangerDrugItemCount(mpWorkloadStat.getDangerDrugItemCount());
					
				} else {
					workloadStatRpt.setMpItemCount(0);
					workloadStatRpt.setMpSfiItemCount(0);
					workloadStatRpt.setMpSafetyNetItemCount(0);
					workloadStatRpt.setMpWardStockItemCount(0);
					workloadStatRpt.setMpDangerDrugItemCount(0);
				}
				
				workloadStatRptList.add(workloadStatRpt);
			}
		}
		
		
		if ( workloadStatRptList.isEmpty() ) {
			retrieveSuccess = false;
		} else{
			retrieveSuccess = true;
		}
		
		return workloadStatRptList;
	}
	
	public void generateWorkloadStatRpt() {
		OperationProfile activeProfile = em.find(OperationProfile.class, workstore.getOperationProfileId());

		parameters.put("hospCode", workstore.getHospCode());
		parameters.put("workstoreCode", workstore.getWorkstoreCode());	
		parameters.put("activeProfileName", activeProfile.getName());	
		
		printAgent.renderAndRedirect(new RenderJob(
				"WorkloadStatRpt", 
				new PrintOption(), 
				parameters, 
				workloadStatRptList));

	}
	
	public void printWorkloadStatRpt(){

		printAgent.renderAndPrint(new RenderAndPrintJob(
				"WorkloadStatRpt", 
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				parameters, 
				workloadStatRptList));
		
	}
	
	public void exportWorkloadStatRpt() {	

		PrintOption po = new PrintOption();
		po.setDocType(ReportProvider.XLS);
		printAgent.renderAndRedirect(new RenderJob(
				"WorkloadStatXls", 
				po, 
				parameters, 
				workloadStatRptList));

	}

	@Remove
	public void destroy() {
		if(workloadStatRptList!=null){
			workloadStatRptList=null;
		}
		
	}

	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}

	
}
