package hk.org.ha.model.pms.biz.checkissue.mp;

import java.util.List;

import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.udt.disp.MpTrxType;

import javax.ejb.Local;

@Local
public interface CheckManagerLocal {
	
	String getProblemItemException(DeliveryItem deliveryItem);
	
	void sendMsgToCmsFromCheckedDeliveryItemList(List<DeliveryItem> deliveryItemList);
	
	void insertTrxLogAndSendMessageToCorp(MpTrxType mpTrxType, List<MedProfileMoItem> moiList);
		
}