package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class SfiInvoiceFtr implements Serializable {

	private static final long serialVersionUID = 1L;

	private String attnChi;

	private String attnEng;

	private Date lastUpdDate;

	private String orderNum;

	private String userCode;
	
	public String getAttnChi() {
		return attnChi;
	}

	public void setAttnChi(String attnChi) {
		this.attnChi = attnChi;
	}

	public String getAttnEng() {
		return attnEng;
	}

	public void setAttnEng(String attnEng) {
		this.attnEng = attnEng;
	}

	public Date getLastUpdDate() {
		return lastUpdDate;
	}

	public void setLastUpdDate(Date lastUpdDate) {
		this.lastUpdDate = lastUpdDate;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

}
