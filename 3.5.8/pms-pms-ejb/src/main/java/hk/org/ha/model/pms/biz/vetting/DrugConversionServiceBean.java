package hk.org.ha.model.pms.biz.vetting;

import java.util.ArrayList;
import java.util.List;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.drug.WorkstoreDrugListManagerLocal;
import hk.org.ha.model.pms.biz.reftable.mp.MultiDoseConvListServiceLocal;
import hk.org.ha.model.pms.biz.sys.SystemMessageManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.PmsDosageConversion;
import hk.org.ha.model.pms.dms.persistence.PmsDosageConversionSet;
import hk.org.ha.model.pms.dms.persistence.PmsDurationConversion;
import hk.org.ha.model.pms.dms.persistence.PmsIpDosageConv;
import hk.org.ha.model.pms.dms.persistence.PmsIpDosageConvSet;
import hk.org.ha.model.pms.dms.persistence.PmsMultiDoseConvSet;
import hk.org.ha.model.pms.dms.persistence.PmsQtyConversion;
import hk.org.ha.model.pms.dms.persistence.dh.DmDhDrugVerMapping;
import hk.org.ha.model.pms.dms.udt.conversion.DhOrderMapperLookupType;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.model.pms.vo.rx.DhRxDrug;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("drugConversionService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DrugConversionServiceBean implements DrugConversionServiceLocal {

	@In
	private SystemMessageManagerLocal systemMessageManager;

	@In
	private WorkstoreDrugListManagerLocal workstoreDrugListManager;

	@In
	private OrderEditManagerLocal orderEditManager;

	@Out(required = false)
	private List<PmsDosageConversion> pmsDosageConversionListForOrderEdit;

	@Out(required = false)
	private List<PmsIpDosageConv> pmsIpDosageConvListForOrderEdit;

	@Out(required = false)
	private List<PmsQtyConversion> pmsQtyConversionListForOrderEdit;

	@Out(required = false)
	private List<PmsDurationConversion> pmsDurationConversionListForOrderEdit;

	@Out(required = false)
	private List<PmsMultiDoseConvSet> pmsMultiDoseConvSetListForOrderEdit;
	
	@Out(required = false)
	private Boolean showMpDosageConvFlag;

	@Out(required = false)
	private DhRxDrug remapDhRxDrug;

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

	@In
	private MultiDoseConvListServiceLocal multiDoseConvListService;
	
	@In
	private DmDrugCacherInf dmDrugCacher;

	@In
	private Workstore workstore;	

	@In
	private PharmOrder pharmOrder; 

	private static final String JAXB_CONTEXT_RX = "hk.org.ha.model.pms.vo.rx";

	@SuppressWarnings("unchecked")
	public void retrieveDrugConversion(RxItem rxItem) {

		Integer drugKey = null;			
		remapDhRxDrug = null;
		String rxItemFmStatus = "";

		if (rxItem.isDhRxDrug()) {
			remapDhRxDrug = rxItem.asDhRxDrug();
			
			Pair<DhOrderMapperLookupType, DmDhDrugVerMapping> pair = dmsPmsServiceProxy.retrieveDhHaDrugMappingInfo(JaxbWrapper.instance(JAXB_CONTEXT_RX).marshall(rxItem.asDhRxDrug()));
			if (pair.getFirst() != DhOrderMapperLookupType.Nil) {
				DmDrug dmDrug = dmDrugCacher.getDmDrug(pair.getSecond().getItemCode());
				
				drugKey = dmDrug.getDrugKey();					
				remapDhRxDrug.setDisplayName(dmDrug.getDmDrugProperty().getDisplayname());
				remapDhRxDrug.setFormCode(dmDrug.getFormCode());
				remapDhRxDrug.setSaltProperty(dmDrug.getDmDrugProperty().getSaltProperty());
				remapDhRxDrug.setFmStatus(dmDrug.getPmsFmStatus().getFmStatus());
				rxItemFmStatus = remapDhRxDrug.getFmStatus();
			} else {
				remapDhRxDrug.setMapStatus("N");
			}
		} else {	
			RxDrug rxDrug = (RxDrug)rxItem;
			rxItemFmStatus = rxDrug.getFmStatus();
			List<DmDrug> dmDrugList = dmDrugCacher.getDmDrugListByDisplayNameFormSaltFmStatus(workstore, rxDrug.getDisplayName(), rxDrug.getFormCode(), rxDrug.getSaltProperty(), rxDrug.getFmStatus());
			if( dmDrugList.size() > 0 ){
				drugKey = dmDrugList.get(0).getDrugKey();
			}
		}

		if (drugKey == null) {
			return;
		}
		
		List<WorkstoreDrug> workstoreDrugList = workstoreDrugListManager.getWorkstoreDrugListByDrugKey(drugKey, true);
		if ( workstoreDrugList != null && workstoreDrugList.size() > 0 ) 
		{
			DmDrug dmDrug = workstoreDrugList.get(0).getDmDrug();
			
			for(WorkstoreDrug workstoreDrug: workstoreDrugList)
			{
				if( workstoreDrug.getDmDrug() != null &&
					workstoreDrug.getDmDrug().getPmsFmStatus() != null &&
					StringUtils.equals(workstoreDrug.getDmDrug().getPmsFmStatus().getFmStatus(), rxItemFmStatus))
				{
					dmDrug = workstoreDrug.getDmDrug();
					break;
				}
			}
			
			pmsDosageConversionListForOrderEdit = new ArrayList<PmsDosageConversion>();
			pmsIpDosageConvListForOrderEdit = new ArrayList<PmsIpDosageConv>();

			if (pharmOrder.getMedOrder().isMpDischarge() && "9".equals(dmDrug.getDmForm().getRank().substring(0, 1))) {

				showMpDosageConvFlag = Boolean.TRUE;
				List<PmsIpDosageConvSet> pmsIpDosageConvSetList = dmsPmsServiceProxy.retrievePmsIpDosageConvSetList(
						workstore.getHospCode(), 
						workstore.getWorkstoreCode(),
						dmDrug.getDrugKey(), 
						dmDrug.getPmsFmStatus().getFmStatus());

				for (PmsIpDosageConvSet pmsIpDosageConvSet:pmsIpDosageConvSetList ) {
					for ( PmsIpDosageConv pmsIpDosageConv:pmsIpDosageConvSet.getPmsIpDosageConvList() ) {
						pmsIpDosageConv.setPmsIpDosageConvSet(pmsIpDosageConvSet);
						pmsIpDosageConv.setDmDrug(dmDrugCacher.getDmDrug(pmsIpDosageConv.getItemCode()));
					}
					pmsIpDosageConvListForOrderEdit.addAll(pmsIpDosageConvSet.getPmsIpDosageConvList());	
				}
			} else {

				showMpDosageConvFlag = Boolean.FALSE;
				List<PmsDosageConversionSet> pmsDosageConversionSetList = dmsPmsServiceProxy.retrievePmsDosageConversionSetList(
						workstore.getHospCode(), 
						workstore.getWorkstoreCode(),
						dmDrug.getDrugKey(), 
						dmDrug.getPmsFmStatus().getFmStatus());	

				for (PmsDosageConversionSet pmsDosageConversionSet:pmsDosageConversionSetList ) {
					for ( PmsDosageConversion pmsDosageConversion:pmsDosageConversionSet.getPmsDosageConversionList() ) {
						pmsDosageConversion.setPmsDosageConversionSet(pmsDosageConversionSet);
						pmsDosageConversion.setDmDrug(dmDrugCacher.getDmDrug(pmsDosageConversion.getItemCode()));
					}
					pmsDosageConversionListForOrderEdit.addAll(pmsDosageConversionSet.getPmsDosageConversionList());	
				}
			}

			pmsQtyConversionListForOrderEdit = dmsPmsServiceProxy.retrievePmsQtyConversionByDrugKey( workstore.getHospCode(), workstore.getWorkstoreCode(), dmDrug.getDrugKey() );				
			pmsDurationConversionListForOrderEdit = dmsPmsServiceProxy.retrievePmsDurationConversionByDrugKey( workstore.getHospCode(), workstore.getWorkstoreCode(), dmDrug.getDrugKey() );
			pmsMultiDoseConvSetListForOrderEdit = multiDoseConvListService.retrievePmsMultiDoseConvSetList(dmDrug);
			
		} else {
			pmsIpDosageConvListForOrderEdit = new ArrayList<PmsIpDosageConv>();
			pmsDosageConversionListForOrderEdit = new ArrayList<PmsDosageConversion>();
			pmsQtyConversionListForOrderEdit = new ArrayList<PmsQtyConversion>();
			pmsDurationConversionListForOrderEdit = new ArrayList<PmsDurationConversion>();	
			pmsMultiDoseConvSetListForOrderEdit = new ArrayList<PmsMultiDoseConvSet>();
		}			
	}

	@Remove
	public void destroy() {	
		if (pmsDosageConversionListForOrderEdit != null) {			
			pmsDosageConversionListForOrderEdit = null;
		}

		if (pmsQtyConversionListForOrderEdit != null) {
			pmsQtyConversionListForOrderEdit = null;			
		}

		if (pmsDurationConversionListForOrderEdit != null) {			
			pmsDurationConversionListForOrderEdit = null;
		}

		if(pmsMultiDoseConvSetListForOrderEdit != null) {
			pmsMultiDoseConvSetListForOrderEdit = null;
		}
		
		if (showMpDosageConvFlag != null) {
			showMpDosageConvFlag = null;
		}
	}
}
