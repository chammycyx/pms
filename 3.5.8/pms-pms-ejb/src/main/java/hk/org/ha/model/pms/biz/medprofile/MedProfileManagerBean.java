package hk.org.ha.model.pms.biz.medprofile;

import static hk.org.ha.model.pms.prop.Prop.CDDH_LEGACY_PERSISTENCE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.MEDPROFILE_MR_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.MEDPROFILE_PATIENT_PAS_PATIENT_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.MEDPROFILE_SENDMSGSEQNUM_ROLLOUT;
import static hk.org.ha.model.pms.prop.Prop.PATIENT_IPMOE_ENABLED;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.remote.marshall.EclipseLinkXStreamMarshaller;
import hk.org.ha.fmk.pms.security.Uam;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.util.QueryUtils;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.mr.MrException;
import hk.org.ha.model.pms.asa.exception.pas.PasException;
import hk.org.ha.model.pms.biz.order.DispOrderConverterLocal;
import hk.org.ha.model.pms.biz.order.OrderManagerLocal;
import hk.org.ha.model.pms.biz.reftable.ActiveWardManagerLocal;
import hk.org.ha.model.pms.biz.reftable.ChargeSpecialtyManagerLocal;
import hk.org.ha.model.pms.biz.reftable.MpWardManagerLocal;
import hk.org.ha.model.pms.biz.reftable.WardConfigManagerLocal;
import hk.org.ha.model.pms.biz.vetting.mp.CheckAtdpsManagerLocal;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasWard;
import hk.org.ha.model.pms.dms.vo.PmsSpecMappingCriteria;
import hk.org.ha.model.pms.dms.vo.PmsSpecialtyMap;
import hk.org.ha.model.pms.pas.PasServiceWrapper;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.corp.HospitalMapping;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileOrder;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileTrxLog;
import hk.org.ha.model.pms.persistence.medprofile.PurchaseRequest;
import hk.org.ha.model.pms.persistence.medprofile.Replenishment;
import hk.org.ha.model.pms.persistence.phs.WardStock;
import hk.org.ha.model.pms.persistence.pivas.MaterialRequestItem;
import hk.org.ha.model.pms.persistence.pivas.PivasPrep;
import hk.org.ha.model.pms.persistence.pivas.PivasWorklist;
import hk.org.ha.model.pms.persistence.reftable.WardConfig;
import hk.org.ha.model.pms.udt.RecordStatus;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.disp.MedCasePasPatInd;
import hk.org.ha.model.pms.udt.disp.MpTrxType;
import hk.org.ha.model.pms.udt.medprofile.InactiveType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileTransferAlertType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileType;
import hk.org.ha.model.pms.udt.medprofile.WardTransferType;
import hk.org.ha.model.pms.udt.pivas.MaterialRequestItemStatus;
import hk.org.ha.model.pms.udt.pivas.PivasWorklistStatus;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.reftable.ChargeSpecialtyType;
import hk.org.ha.model.pms.udt.reftable.MachineType;
import hk.org.ha.model.pms.udt.vetting.CmsOrderType;
import hk.org.ha.model.pms.vo.medprofile.BodyInfo;
import hk.org.ha.model.pms.vo.medprofile.PasContext;
import hk.org.ha.model.pms.vo.medprofile.PasParams;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;
import hk.org.ha.service.pms.asa.interfaces.ipmoe.IpmoeServiceJmsRemote;
import hk.org.ha.service.pms.asa.interfaces.moe.MoeServiceJmsRemote;
import hk.org.ha.service.pms.asa.interfaces.mr.MrServiceJmsRemote;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;
import org.springframework.beans.BeanUtils;

@AutoCreate
@Stateless
@Name("medProfileManager")
@MeasureCalls
public class MedProfileManagerBean implements MedProfileManagerLocal {
	
	private static final String MED_PROFILE_QUERY_HINT_BATCH = "o.medProfileItemList.medProfileMoItem.medProfilePoItemList";
	
	private static final String PIVAS_WORKLIST_QUERY_HINT_FETCH = "o.medProfileMoItem";
	
	@Logger
	private Log logger;
	
	@In
	private AuditLogger auditLogger;
	
	@In
	private PasServiceWrapper pasServiceWrapper;
	
	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
	
	@In
	private IpmoeServiceJmsRemote ipmoeServiceProxy;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy; 
	
	@In
	private MrServiceJmsRemote mrServiceProxy;
	
	@In
	private WardConfigManagerLocal wardConfigManager;
	
	@In
	private MpWardManagerLocal mpWardManager;
	
	@In
	private ActiveWardManagerLocal activeWardManager;
	
	@In
	private ChargeSpecialtyManagerLocal chargeSpecialtyManager;
	
	@In
	private OrderManagerLocal orderManager;
	
	@In
	private MedProfileStatManagerLocal medProfileStatManager;
	
	@In
	private DispOrderConverterLocal dispOrderConverter;
	
	@In
	private CheckAtdpsManagerLocal checkAtdpsManager;
		
	@PersistenceContext
	private EntityManager em;

	private EclipseLinkXStreamMarshaller marshaller = new EclipseLinkXStreamMarshaller();
	
	@Override
	public PasContext retrieveContext(MedProfile medProfile) {
		return retrieveContext(medProfile, false, false);
	}
	
	@Override
	public PasContext retrieveContext(MedProfile medProfile, boolean updateBodyInfo, boolean updateItemTransferInfo) {
		
		if (medProfile.getPatient() == null || medProfile.getMedCase() == null) {
			throw new IllegalArgumentException("Illegal argument - MedProfile without any valid Patient and/or MedCase attached");
		}
		
		Workstore workstore = (Workstore) Contexts.getSessionContext().get("workstore");
		
		PasParams params = new PasParams();
		params.setUpdateBodyInfo(updateBodyInfo);
		params.setUpdateItemTransferInfo(Boolean.TRUE.equals(medProfile.getTransferFlag()) && updateItemTransferInfo);
		params.setWorkstore(workstore == null ? medProfile.getWorkstore() : workstore);
		params.setPatHospCode(medProfile.getPatHospCode());
		params.setPatKey(medProfile.getPatient().getPatKey());
		BeanUtils.copyProperties(medProfile.getMedCase(), params);
		params.setPrivateFlag(medProfile.getMedCase().getPasPatInd() == MedCasePasPatInd.Private);
		
		return retrieveContext(params);
	}
	
	private PasContext retrieveContext(Workstore workstore, MedProfileOrder medProfileOrder) {
		
		PasParams params = new PasParams();
		BeanUtils.copyProperties(medProfileOrder, params);
		params.setWorkstore(workstore);
		
		return retrieveContext(params);
	}
	
	private PasContext retrieveContext(PasParams params) {
		
		PasContext context = new PasContext();
		BeanUtils.copyProperties(params, context);

		if (context.getWorkstore() != null) {
			
			if (MEDPROFILE_PATIENT_PAS_PATIENT_ENABLED.get(context.getWorkstore(), Boolean.FALSE)) {
				context.setPatient(retrievePasPatient(context.getPatHospCode(), context.getCaseNum()));
				context.setMedCase(MedProfileUtils.lookupPasMedCase(context.getPatient(), context.getCaseNum()));
			}
			
			context.setPmsSpecialtyMap(retrieveSpecialtyMapping(context.getPatHospCode(), context.getPasSpecCode(),
					context.getPasWardCode(), context.getWorkstore().getHospCode(), context.getWorkstore().getWorkstoreCode()));

			if (Boolean.TRUE.equals(context.getUpdateBodyInfo()) &&
					PATIENT_IPMOE_ENABLED.get(context.getWorkstore(), Boolean.FALSE)) {
				context.setBodyInfo(retrieveBodyInfo(context.getPatHospCode(), context.getPatKey()));
			}
			
			if (Boolean.TRUE.equals(context.getUpdateItemTransferInfo()) &&
					isWardTransferUpdateEnabled(context.getWorkstore()) &&
					activeWardManager.isActiveWard(context.getPatHospCode(), context.getPasWardCode())) {
				Date itemCutOffDate = retrieveItemCutOffDate(context.getPatHospCode(), context.getHkid(), context.getCaseNum());
				context.setErrorOnRetrieveItemCutOffDate(itemCutOffDate == null);
				context.setItemCutOffDate(itemCutOffDate);
			}
		}
			
		return context;
	}
	
	@Override
	public void applyChanges(PasContext context, MedProfile medProfile) {
		
		if (medProfile.getPatient() == null) {
			savePatient(medProfile, context);
		} else {
			updatePasPatient(medProfile, context.getPatient());
		}
		
		String orgPasWardCode = null;
		
		if (medProfile.getMedCase() == null) {
			saveMedCase(medProfile, context);
		} else {
			orgPasWardCode = medProfile.getMedCase().getPasWardCode();
			updatePasMedCase(medProfile, context.getMedCase());
		}
		
		updateMedProfile(medProfile, context);
		updateSpecCodeAndWardCode(context.getWorkstore(), medProfile, context.getPmsSpecialtyMap(), true);
		updatePasWardCodeRelatedSetting(context.getWorkstore(), orgPasWardCode, medProfile, false);
		
		if (Boolean.TRUE.equals(context.getUpdateBodyInfo())) {
			updateBodyInfo(medProfile, context.getBodyInfo());
		}		
	}
	
	@Override
	public void updatePasInfo(MedProfile medProfile) {
		PasContext context = retrieveContext(medProfile);
		applyChanges(context, medProfile);
	}
	
	@Override
	public void initializePasInfo(MedProfile medProfile, MedProfileOrder medProfileOrder) {
		PasContext context = retrieveContext(medProfile.getWorkstore(), medProfileOrder);
		applyChanges(context, medProfile);
	}
	
	@Override
	public void mapSpecialty(MedProfile medProfile) {
		
		Workstore workstore = medProfile.getWorkstore();
		MedCase medCase = medProfile.getMedCase();
		
		PmsSpecialtyMap specialtyMap = retrieveSpecialtyMapping(medProfile.getPatHospCode(), medCase.getPasSpecCode(),
				medCase.getPasWardCode(), workstore.getHospCode(), workstore.getWorkstoreCode());
		
		mapSpecialty(medProfile, specialtyMap);
	}
	
	@Override
	public void mapSpecialty(MedProfile medProfile, PmsSpecialtyMap specialtyMap) {

		Workstore workstore = medProfile.getWorkstore();
		updateSpecCodeAndWardCode(workstore, medProfile, specialtyMap, false);
	}
	
	@Override
	public void sendMessages(MpTrxType type, MedProfile medProfile, List<MedProfileMoItem> medProfileMoItemList) {
		sendMessages(type, medProfile, medProfileMoItemList, false);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void sendMessagesWithNewTransaction(MpTrxType type, MedProfile medProfile, List<MedProfileMoItem> medProfileMoItemList) {
		sendMessages(type, medProfile, medProfileMoItemList, true);
	}
	
	private void sendMessages(MpTrxType type, MedProfile medProfile, List<MedProfileMoItem> medProfileMoItemList, boolean newTransaction) {

		medProfileMoItemList = filterManualItemForMedProfileMoItemList(medProfileMoItemList);
		medProfileMoItemList = filterNoNeedToSentMessage(type, medProfile, medProfileMoItemList);
		medProfileMoItemList = attachManagedMedProfileForMedProfileMoItemList(medProfile, medProfileMoItemList, newTransaction);
		if (medProfileMoItemList.size() > 0) {
			directSendMessages(medProfile.getWorkstore().getHospital(), type, medProfileMoItemList);
			persistTrxLogs(type, medProfile, medProfileMoItemList);
		}
	}
	
	@Override
	public void directSendReplenishmentUpdateMessages(Hospital hospital, List<Replenishment> replenishmentList) {
		
		replenishmentList = filterManualItemForReplenishmentList(replenishmentList);
		if (replenishmentList.size() > 0) {
			attachSeqNumToReplenishmentList(hospital, replenishmentList);
			corpPmsServiceProxy.sendMedProfileReplenishment(MpTrxType.MpDrugReplenishmentUpdate, replenishmentList);
		}
	}
	
	@Override
	public void directSendMessages(Hospital hospital, MpTrxType type, List<MedProfileMoItem> medProfileMoItemList) {
	
		medProfileMoItemList = filterManualItemForMedProfileMoItemList(medProfileMoItemList);
		if (medProfileMoItemList.size() > 0) {
			cascadeMedProfilePoItemList(type, medProfileMoItemList);
			attachSeqNumToMedProfileMoItemList(hospital, medProfileMoItemList);
			corpPmsServiceProxy.sendMedProfileOrder(type, medProfileMoItemList);
		}
	}

	@Override
	public boolean isTrxLogExists(MpTrxType type, MedProfile medProfile, Integer itemNum) {
		return isTrxLogExists(type, medProfile, MedProfileUtils.join(itemNum));
	}
	
	@Override
	public boolean isTrxLogExists(MpTrxType type, MedProfile medProfile, String refNum) {
		return retrieveSentRefNumSet(type, medProfile, Arrays.asList(refNum)).contains(refNum);
	}
	
	private void cascadeMedProfilePoItemList(MpTrxType type, List<MedProfileMoItem> medProfileMoItemList) {
		
		if (!MpTrxType.MpVetOrder_MpUpdateDispensing.contains(type)) {
			return;
		}
		
		for (MedProfileMoItem medProfileMoItem: medProfileMoItemList) {
			medProfileMoItem.getMedProfilePoItemList().size();
		}
	}
	
	private void attachSeqNumToMedProfileMoItemList(Hospital hospital, List<MedProfileMoItem> medProfileMoItemList) {
		
		boolean sendMsgSeqNum = MEDPROFILE_SENDMSGSEQNUM_ROLLOUT.get(hospital, false);
		
		for (MedProfileMoItem medProfileMoItem: medProfileMoItemList) {
			MedProfile medProfile = medProfileMoItem.getMedProfileItem().getMedProfile();
			if (sendMsgSeqNum) {
				checkMedProfileManaged(medProfile);
				medProfileMoItem.setSeqNum(medProfile.incrementOutCurrSeqNum());
			}
		}
	}
	
	private void attachSeqNumToReplenishmentList(Hospital hospital, List<Replenishment> replenishmentList) {
		
		boolean sendMsgSeqNum = MEDPROFILE_SENDMSGSEQNUM_ROLLOUT.get(hospital, false);
		
		for (Replenishment replenishment: replenishmentList) {
			MedProfile medProfile = replenishment.getMedProfileMoItem().getMedProfileItem().getMedProfile();
			if (sendMsgSeqNum) {
				checkMedProfileManaged(medProfile);
				replenishment.setSeqNum(medProfile.incrementOutCurrSeqNum());
			}
		}
	}
	
	private void checkMedProfileManaged(MedProfile medProfile) {
		
		if (!em.contains(medProfile)) {
			throw new IllegalArgumentException("Cannot update outCurrSeqNum in MedProfile(id=" +
					(medProfile == null ? null : medProfile.getId()) + ") as the entity is not managed by EntityManager");
		}
	}
	
	private void persistTrxLogs(MpTrxType type, MedProfile medProfile, List<MedProfileMoItem> medProfileMoItemList) {
		
		if (!MpTrxType.MpVetOrder_MpUrgentDispenseUpdate.contains(type)) {
			return;
		}
		
		for (MedProfileMoItem medProfileMoItem: medProfileMoItemList) {
			MedProfileTrxLog log = new MedProfileTrxLog();
			log.setType(type);
			log.setMedProfile(medProfile);
			log.setRefNum(MedProfileUtils.join(medProfileMoItem.getItemNum()));
			em.persist(log);
		}
	}
	
	private List<MedProfileMoItem> filterNoNeedToSentMessage(MpTrxType type, MedProfile medProfile, List<MedProfileMoItem> medProfileMoItemList) {
		
		MpTrxType checkType = type == MpTrxType.MpUpdateDispensing ? MpTrxType.MpVetOrder : type;
		boolean sendIfExists = type != checkType;
		
		if (!MpTrxType.MpVetOrder_MpUrgentDispenseUpdate.contains(checkType)) {
			return medProfileMoItemList;
		}

    	List<MedProfileMoItem> resultList = new ArrayList<MedProfileMoItem>();
    	Set<String> sentRefNumSet = retrieveSentRefNumSet(checkType, medProfile, extractRefNumList(medProfileMoItemList));
    	
    	for (MedProfileMoItem medProfileMoItem : medProfileMoItemList) {
    		if (sendIfExists == sentRefNumSet.contains(MedProfileUtils.join(medProfileMoItem.getItemNum()))) {
    			resultList.add(medProfileMoItem);
    		}
    	}
    	return resultList;
    }
	
	private List<MedProfileMoItem> filterManualItemForMedProfileMoItemList(List<MedProfileMoItem> medProfileMoItemList) {
		
    	List<MedProfileMoItem> resultList = new ArrayList<MedProfileMoItem>();

    	for (MedProfileMoItem medProfileMoItem : medProfileMoItemList) {
    		if (!medProfileMoItem.isManualItem()) {
    			resultList.add(medProfileMoItem);
    		}
    	}
    	return resultList;
	}
	
	private List<Replenishment> filterManualItemForReplenishmentList(List<Replenishment> replenishmentList) {

		List<Replenishment> resultList = new ArrayList<Replenishment>();
		
		for (Replenishment replenishment : replenishmentList) {
			if (!replenishment.getMedProfileMoItem().isManualItem()) {
				resultList.add(replenishment);
			}
		}
		return resultList;
	}
	
	private List<MedProfileMoItem> attachManagedMedProfileForMedProfileMoItemList(MedProfile medProfile,
			List<MedProfileMoItem> medProfileMoItemList, boolean newTransaction) {
		
		if (!newTransaction) {
			return medProfileMoItemList;
		}
		
		if ((medProfileMoItemList = filterUnrelatedMedProfileMoItemList(medProfile, medProfileMoItemList)).size() <= 0) {
			return medProfileMoItemList;
		}
		
		if ((medProfile = retreiveManagedMedProfile(medProfile)) == null) {
			return new ArrayList<MedProfileMoItem>();
		}
		
		for (MedProfileMoItem medProfileMoItem : medProfileMoItemList) {
			if (!em.contains(medProfileMoItem.getMedProfileItem().getMedProfile())) {
				medProfileMoItem.getMedProfileItem().setMedProfile(medProfile);
			}
		}
		return medProfileMoItemList;
	}
	
	private List<MedProfileMoItem> filterUnrelatedMedProfileMoItemList(MedProfile medProfile, List<MedProfileMoItem> medProfileMoItemList) {
		
		List<MedProfileMoItem> resultList = new ArrayList<MedProfileMoItem>();
		for (MedProfileMoItem medProfileMoItem : medProfileMoItemList) {
			if (medProfile.getId() != null && medProfile.getId().equals(medProfileMoItem.getMedProfileItem().getMedProfile().getId())) {
				resultList.add(medProfileMoItem);
			} else {
				logger.warn("Try to send message for MedProfileMoItem(id=#0) which is not owned by MedProfile(id=#1, orderNum=#2)",
						medProfileMoItem.getId(), medProfile.getId(), medProfile.getOrderNum());
			}
		}
		return resultList;
	}
	
	@SuppressWarnings("unchecked")
	private MedProfile retreiveManagedMedProfile(MedProfile medProfile) {
		
		medProfile = (MedProfile) MedProfileUtils.getFirstItem(em.createQuery(
				"select o from MedProfile o" + // 20150420 index check : MedProfile.id : PK_MED_PROFILE
				" where o.id = :medProfileId and o.id is not null")
				.setParameter("medProfileId", medProfile.getId())
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
				.getResultList());				
		
		if (medProfile != null) {
			medProfile.getPatient();
			medProfile.getMedCase();
		}
		return medProfile;
	}
    
    @SuppressWarnings("unchecked")
	public Set<String> retrieveSentRefNumSet(MpTrxType type, MedProfile medProfile, List<String> refNumList) {
    	
    	return QueryUtils.toSet(QueryUtils.splitExecute(em.createQuery(
    			"select distinct l.refNum from MedProfileTrxLog l" + // 20130829 index check : MedProfileTrxLog.medProfile : FK_MED_PROFILE_TRX_LOG_01
    			" where l.medProfile = :medProfile" +
    			" and l.type = :type" +
    			" and l.refNum in :refNumList")
    			.setParameter("medProfile", medProfile)
    			.setParameter("type", type)
    			.setParameter("refNumList", refNumList)
    			, "refNumList", refNumList));
    }
    
	@SuppressWarnings("unchecked")
	public Map<Long, Set<String>> retrieveSentRefNumMap(MpTrxType type, Set<Long> medProfileIdSet, List<String> refNumList) {
		
		List<MedProfileTrxLog> medProfileTrxLogList = QueryUtils.splitExecute(em.createQuery(
    			"select l from MedProfileTrxLog l" + // 20160615 index check : MedProfileTrxLog.medProfileId : FK_MED_PROFILE_TRX_LOG_01
    			" where l.medProfileId in :medProfileId" +
    			" and l.type = :type" +
    			" and l.refNum in :refNumList")
    			.setParameter("medProfileId", medProfileIdSet)
    			.setParameter("type", type)
    			.setParameter("refNumList", refNumList)
    			, "refNumList", refNumList);
		
		Map<Long, Set<String>> refNumMap = new HashMap<Long, Set<String>>();
    	for (MedProfileTrxLog medProfileTrxLog : medProfileTrxLogList) {
    		
    		Long medProfileId = medProfileTrxLog.getMedProfileId();

    		Set<String> refNumSet;
    		if (refNumMap.containsKey(medProfileId)) {
    			refNumSet = refNumMap.get(medProfileId);
    		}
    		else {
    			refNumSet = new HashSet<String>();
    			refNumMap.put(medProfileId, refNumSet);
    		}
    		
    		refNumSet.add(medProfileTrxLog.getRefNum());
    	}
    	return refNumMap;
	}

    
    private List<String> extractRefNumList(List<MedProfileMoItem> medProfileMoItemList) {
    	
    	List<String> refNumList = new ArrayList<String>();
    	for (MedProfileMoItem medProfileMoItem : medProfileMoItemList) {
    		refNumList.add(MedProfileUtils.join(medProfileMoItem.getItemNum()));
    	}
    	return refNumList; 
    }
	
    public void saveChargeOrderNum(MedProfile medProfile) {
    	if( StringUtils.isBlank(medProfile.getChargeOrderNum()) ){    	
    		String chargeOrderNum = corpPmsServiceProxy.retrieveChargeOrderNum(medProfile.getWorkstore().getHospCode(), medProfile.getPatHospCode());
    		medProfile.setChargeOrderNum(chargeOrderNum);
    	}
    }
    
    public void savePatientAndMedCase(MedProfile medProfile) {
    	//CARS(Non-IPMOE)
    	PasContext context = new PasContext();
    	context.setPatient(medProfile.getPatient());
    	context.setMedCase(medProfile.getMedCase());
    	savePatient(medProfile, context);
		saveMedCase(medProfile, context);
    }
    
	@SuppressWarnings("unchecked")
	private void savePatient(MedProfile medProfile, PasContext context) {
		
		Patient patient = context.getPatient();
		
		if (patient != null) {
			
			List<Long> idList = em.createQuery(
					"select p.id from Patient p" + // 20120225 index check : Patient.patKey : I_PATIENT_01
					" where p.patKey = :patKey" +
					" order by p.id")
					.setParameter("patKey", patient.getPatKey())
					.getResultList();
			
			if (!idList.isEmpty()) {
				patient.setId(idList.get(0));
			}
		} else  {
			patient = new Patient();
			BeanUtils.copyProperties(context, patient);
		}
		
		patient = corpPmsServiceProxy.savePatient(patient);
		medProfile.setPatient(orderManager.savePatient(patient));		
	}

	private void saveMedCase(MedProfile medProfile, PasContext context) {
		
		MedCase medCase = context.getMedCase();
		
		if (medCase == null) {
			medCase = new MedCase();
			BeanUtils.copyProperties(context, medCase);
			medCase.setPasPatInd(Boolean.TRUE.equals(context.getPrivateFlag()) ? MedCasePasPatInd.Private : MedCasePasPatInd.Normal);
		}
		
		medCase = corpPmsServiceProxy.saveMedCase(medCase);
		medCase = orderManager.saveMedCase(medCase);
		medProfile.setAdmissionDate(medCase.getAdmissionDate());
		medProfile.setMedCase(medCase);
	}
	
	private void updatePasPatient(MedProfile medProfile, Patient pasPatient) {

		if (pasPatient == null) {
			return;
		}

		medProfile.getPatient().updateByPatient(pasPatient);
	}

	private void updatePasMedCase(MedProfile medProfile, MedCase pasMedCase) {
		
		if (pasMedCase == null) {
			return;
		}

		medProfile.getMedCase().updateByMedCase(pasMedCase);
		medProfile.setAdmissionDate(pasMedCase.getAdmissionDate());
		
		if (pasMedCase.getDischargeDate() != null && (medProfile.getKeepActiveUntilDate() == null ||
				medProfile.getKeepActiveUntilDate().compareTo(MedProfileUtils.getTodayBegin()) < 0)) {
			markProfileAsInactive(medProfile, InactiveType.Discharge, pasMedCase.getDischargeDate(), null);
		} else if (pasMedCase.getDischargeDate() == null && medProfile.isCancelAdmission()) {
			medProfile.setStatus(MedProfileStatus.Active);
			medProfile.setInactiveType(null);
			medProfile.setCancelAdmissionDate(null);		
		}
	}
	
	private void updateMedProfile(MedProfile medProfile, PasContext context) {

		Date now = new Date();
		
		if (medProfile.getStatus() == MedProfileStatus.HomeLeave &&
				medProfile.getReturnDate() != null && medProfile.getReturnDate().compareTo(now) <= 0) {
			medProfile.setStatus(MedProfileStatus.Active);
			medProfile.setInactiveType(null);
			medProfile.setReturnDate(null);
		} else if (medProfile.getStatus() == MedProfileStatus.PendingDischarge &&
				medProfile.getResumeActiveDate() != null && medProfile.getResumeActiveDate().compareTo(now) <= 0) {
			medProfile.setStatus(MedProfileStatus.Active);
			medProfile.setInactiveType(null);
			medProfile.setResumeActiveDate(null);
		}

		medProfile.refreshPrivateFlag();
		medProfile.setCurrentItemCutOffDate(context.getItemCutOffDate());
	}
	
	private void updateSpecCodeAndWardCode(Workstore workstore, MedProfile medProfile, PmsSpecialtyMap specialtyMap,
			boolean clearSpecCodeAndWardCodeIfSpecialtyMappingMissing) {
		
		MedCase medCase = medProfile.getMedCase();
		
		if (medProfile.isManualProfile() && medCase.getPasWardCode() == null) {
			return;
		}
		
		String orgSpecCode = medProfile.getSpecCode();
		String orgWardCode = medProfile.getWardCode();

		updateSpecialtyMapping(medCase, specialtyMap);
		
		if (medCase.getPhsSpecCode() != null && medCase.getPhsWardCode() != null) {
			medProfile.setSpecCode(medCase.getPhsSpecCode());
			medProfile.setWardCode(medCase.getPhsWardCode());
		} else if (isValidPhsSpecialtyAndPhsWard(workstore, medCase.getPasSpecCode(), medCase.getPasWardCode())) {
			medProfile.setSpecCode(medCase.getPasSpecCode());
			medProfile.setWardCode(medCase.getPasWardCode());
		} else if (clearSpecCodeAndWardCodeIfSpecialtyMappingMissing) {
			medProfile.setSpecCode(null);
			medProfile.setWardCode(null);
		}
		
		updateSpecCodeAndWardCodeRelatedSetting(workstore, orgSpecCode, orgWardCode, true, medProfile, medProfile.getMedProfileItemList());
	}
		
	@Override
	public void markProfileAsInactive(MedProfile medProfile, InactiveType inactiveType, Date dischargeDate, Date cancelAdmissionDate) {

		if (!medProfile.isDeleted()) {
			medProfile.setStatus(MedProfileStatus.Inactive);
			medProfile.setInactiveType(inactiveType);
			medProfile.setDischargeDate(dischargeDate);
			medProfile.setKeepActiveUntilDate(null);
			medProfile.setCancelAdmissionDate(cancelAdmissionDate);
			medProfile.setCancelDischargeDate(null);
			medProfile.setConfirmCancelDischargeUser(null);
			medProfile.setConfirmCancelDischargeDate(null);
			if (medProfile.getType() == MedProfileType.Chemo) {
				medProfile.getMedCase().setCaseNum(null);
			}
		}
		
		removeUnfinishedMaterialRequestItemsAndPivasOrderFromWorklist(medProfile);
		removeNonProcessedPurchaseRequestListByMedProfile(medProfile);
	}
	
	@Override
	public void initializeProfileState(MedProfile medProfile) {

		MedCase medCase = medProfile.getMedCase();
		medProfile.setAdmissionDate(medCase.getAdmissionDate());
		if (medCase.getDischargeDate() != null) {
			markProfileAsInactive(medProfile, InactiveType.Discharge, medCase.getDischargeDate(), null);
		}
		medProfile.refreshPrivateFlag();
		mapSpecialty(medProfile);
	}
	
	private void removeNonProcessedPurchaseRequestList(MedProfile medProfile, List<PurchaseRequest> nonProcessPurchaseRequestList, boolean voidInvoice){
		List<PurchaseRequest> voidPurchaseRequestList = new ArrayList<PurchaseRequest>();
		
		if( nonProcessPurchaseRequestList != null ){
			for( PurchaseRequest pr: nonProcessPurchaseRequestList ){
				if( pr.getInvoiceDate() == null ){
					pr.setInvoiceStatus(InvoiceStatus.SysDeleted);
					em.merge(pr);
				}else if( voidInvoice && pr.getInvoiceDate() != null ){
					pr.setInvoiceStatus(InvoiceStatus.Void);
					em.merge(pr);
					voidPurchaseRequestList.add(pr);
				}
			}
		}
		
		List<Invoice> voidInvoiceList = dispOrderConverter.convertPurchaseRequestToVoidInvoiceList(voidPurchaseRequestList, medProfile.getWorkstore());
		
		if( voidInvoiceList != null && !voidInvoiceList.isEmpty() ){
			corpPmsServiceProxy.saveDispOrderListWithVoidInvoiceListForIp(new ArrayList<DispOrder>(), 
																					CDDH_LEGACY_PERSISTENCE_ENABLED.get(false),
																					CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED.get(medProfile.getWorkstore().getHospital(), false), true,
																					null, voidInvoiceList);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void removeNonProcessedPurchaseRequestListByMedProfileMoItem(MedProfile medProfile, MedProfileMoItem medProfileMoItem){
		if (medProfile.getId() == null || medProfileMoItem == null) {
			return;
		}
		
		List<PurchaseRequest> nonProcessPurchaseRequestList = em.createQuery(
				"select o from PurchaseRequest o" + // 20151012 index check : MedProfile.id : PK_MED_PROFILE
    			" where o.medProfilePoItem.medProfileMoItem.medProfileItem.medProfile = :medProfile" +
    			" and o.medProfilePoItem.medProfileMoItem.itemNum = :itemNum" +
		    	" and o.printDate is null" +
				" and (o.invoiceStatus is null or o.invoiceStatus in :invoiceStatusList)")
		    	.setParameter("medProfile", medProfile)
		    	.setParameter("itemNum", medProfileMoItem.getItemNum())
		    	.setParameter("invoiceStatusList", InvoiceStatus.None_Outstanding_Settle)
		    	.getResultList();

		removeNonProcessedPurchaseRequestList(medProfile, nonProcessPurchaseRequestList, false);
	}
	
	@SuppressWarnings("unchecked")
	private void removeNonProcessedPurchaseRequestListByMedProfile(MedProfile medProfile){
		
		if (medProfile.getId() == null) {
			return;
		}
		
		List<PurchaseRequest> nonProcessPurchaseRequestList = em.createQuery(
				"select o from PurchaseRequest o" + // 20150420 index check : MedProfile.id : PK_MED_PROFILE
    			" where o.medProfilePoItem.medProfileMoItem.medProfileItem.medProfile = :medProfile" +
		    	" and o.printDate is null" +
		    	" and (o.invoiceStatus is null or o.invoiceStatus in :invoiceStatusList)")
		    	.setParameter("medProfile", medProfile)
		    	.setParameter("invoiceStatusList", InvoiceStatus.None_Outstanding_Settle)
		    	.getResultList();
		removeNonProcessedPurchaseRequestList(medProfile, nonProcessPurchaseRequestList, true);
	}
	
	@Override
	public void updatePasWardCodeRelatedSetting(Workstore workstore, String orgPasWardCode, MedProfile medProfile, boolean cancelWardTransfer) {
		
		if (medProfile.getId() == null) {
			return;
		}
		
		String pasWardCode = medProfile.getMedCase().getPasWardCode();
		
		if (orgPasWardCode == null || pasWardCode == null || ObjectUtils.compare(orgPasWardCode, pasWardCode) == 0) {
			return;
		}
		
		List<PivasWard> pivasWardList = dmsPmsServiceProxy.retrievePivasWardList(medProfile.getPatHospCode(), workstore.getWorkstoreGroup().getWorkstoreGroupCode());
		Set<String> pivasPasWardCodeSet = buildPivasPasWardCodeSet(pivasWardList);
		
		boolean fromPivasWard = pivasPasWardCodeSet.contains(orgPasWardCode);
		boolean toPivasWard = pivasPasWardCodeSet.contains(pasWardCode);
		
		if (fromPivasWard == toPivasWard) {
			//do nothing
		} else if (fromPivasWard) {
			transferFromPivasWard(medProfile);
		} else {
			transferToPivasWard(medProfile);
		}
		
		boolean fromActiveWard = activeWardManager.isActiveWard(medProfile.getPatHospCode(), orgPasWardCode);
		boolean toActiveWard = activeWardManager.isActiveWard(medProfile.getPatHospCode(), pasWardCode);
		
		if( fromActiveWard == toActiveWard ){
			//do nothing
		}else{
			logger.info("transferBetweenActiveWardAndNonActiveWard===========From: #0[#1] TO: #2[#3] cancelWardTransfer:[#4]", orgPasWardCode, fromActiveWard, pasWardCode, toActiveWard, cancelWardTransfer);
			transferBetweenActiveWardAndNonActiveWard(medProfile, cancelWardTransfer);
		}
	}
	
	private Set<String> buildPivasPasWardCodeSet(List<PivasWard> pivasWardList) {
		
		Set<String> pivasPasWardCodeSet = new HashSet<String>();
		for (PivasWard pivasWard: pivasWardList) {
			pivasPasWardCodeSet.add(pivasWard.getCompId().getWard());
		}
		return pivasPasWardCodeSet;
	}
	
	@SuppressWarnings("unchecked")
	private void transferFromPivasWard(MedProfile medProfile) {
		
		List<PivasWorklist> pivasWorklists = em.createQuery(
				"select o from PivasWorklist o" + // 20160615 index check : PivasWorklist.medProfileId : FK_PIVAS_WORKLIST_04
				" where o.medProfileId = :medProfileId" +
				" and o.status in :activeStatusList")
				.setParameter("medProfileId", medProfile.getId())
				.setParameter("activeStatusList", PivasWorklistStatus.New_Refill_Prepared)
				.setHint(QueryHints.FETCH, PIVAS_WORKLIST_QUERY_HINT_FETCH)
				.getResultList();
		
		markDeleteForPivasWorklists(pivasWorklists, true);
		
		List<MedProfileMoItem> medProfileMoItemList = em.createQuery(
				"select o.medProfileMoItem from MedProfileItem o" + // 20161128 index check : MedProfileItem.medProfileId : FK_MED_PROFILE_ITEM_01
				" where o.medProfileId = :medProfileId" +
				" and o.status in :activeStatusList" +
				" and (o.medProfileMoItem.endDate is null or o.medProfileMoItem.endDate > :currentDate)" +
				" and o.medProfileMoItem.pivasFlag = true")
				.setParameter("medProfileId", medProfile.getId())
				.setParameter("activeStatusList", MedProfileItemStatus.Unvet_Vetted_Verified)
				.setParameter("currentDate", new Date())
				.getResultList();

		removePivasFlags(medProfileMoItemList);
		
		List<PivasPrep> pivasPrepList = em.createQuery(
				"select o from PivasPrep o" + // 20161128 index check : MedProfileItem.medProfileId : FK_MED_PROFILE_ITEM_01
				" where o.medProfileMoItem.medProfileItem.medProfileId = :medProfileId" +
				" and o.status <> :deleteStatus")
				.setParameter("medProfileId", medProfile.getId())
				.setParameter("deleteStatus", RecordStatus.Delete)
				.getResultList();
		
		markDeleteForPivasPrepList(pivasPrepList);
		
		List<MaterialRequestItem> materialRequestItemList = em.createQuery(
				"select o from MaterialRequestItem o" + // 20161128 index check : MaterialRequest.medProfileId : FK_MATERIAL_REQUEST_02
				" where o.materialRequest.medProfileId = :medProfileId" +
				" and o.status in :activeStatusList")
				.setParameter("medProfileId", medProfile.getId())
				.setParameter("activeStatusList", MaterialRequestItemStatus.None_Outstanding)
				.getResultList();
		
		markDeleteForMaterialRequestItemList(materialRequestItemList);
	}
	
	@SuppressWarnings("unchecked")
	private void transferToPivasWard(MedProfile medProfile) {
		
		Set<Long> idSet = new TreeSet<Long>();
		
		for (MedProfileItem medProfileItem: medProfile.getMedProfileItemList()) {
			if (medProfileItem.getStatus() == MedProfileItemStatus.Verified) {
				idSet.add(medProfileItem.getMedProfileMoItemId());
			}
		}
		
		idSet = QueryUtils.toSet(QueryUtils.splitExecute(em.createQuery(
				"select distinct o.medProfileMoItemId from PivasWorklist o" + // 20161128 index check : PivasWorklist.medProfileMoItemId : FK_PIVAS_WORKLIST_05
				" where o.medProfileMoItemId in :medProfileMoItemIdSet" +
				" and o.status = :status")
				.setParameter("status", PivasWorklistStatus.Completed)
				, "medProfileMoItemIdSet", idSet));
		
		Date now = new Date();
		
		for (MedProfileItem medProfileItem: medProfile.getMedProfileItemList()) {
			
			if (!idSet.contains(medProfileItem.getMedProfileMoItemId())) {
				continue;
			}
			medProfileItem.setStatus(MedProfileItemStatus.Vetted);
			medProfileItem.setStatusUpdateDate(now);
			medProfileItem.getMedProfileMoItem().setPivasFlag(true);
		}
		
		em.flush();
	}	
	
	private void transferBetweenActiveWardAndNonActiveWard(MedProfile medProfile, boolean cancelWardTransfer) {
		if( cancelWardTransfer ){
			boolean releasefrozenState = MedProfileTransferAlertType.Review.equals(medProfile.getTransferAlertType()); 
			if( releasefrozenState ){
				releaseFrozenForManualItem(medProfile);
				medProfile.setTransferAlertType(MedProfileTransferAlertType.None);
			}else{
				markFrozenForManualItem(medProfile);
				medProfile.setTransferAlertType(MedProfileTransferAlertType.Review);
			}
		}else{
			//mark manualItem to frozen
			markFrozenForManualItem(medProfile);
			medProfile.setTransferAlertType(MedProfileTransferAlertType.Review);
		}
		medProfileStatManager.recalculateAllStat(medProfile, true);
	}
	
	private void releaseFrozenForManualItem(MedProfile medProfile){
		for(MedProfileItem mpi : medProfile.getMedProfileItemList()){
			MedProfileMoItem mpMoi = mpi.getMedProfileMoItem();
			
			if( Boolean.TRUE.equals(mpMoi.getFrozenFlag()) ){
				mpMoi.setFrozenFlag(Boolean.FALSE);
			}
		}
	}
	
	private void markFrozenForManualItem(MedProfile medProfile){
		Date now = new Date();
		for(MedProfileItem mpi : medProfile.getMedProfileItemList()){
			MedProfileMoItem mpMoi = mpi.getMedProfileMoItem();
			
			if( mpMoi.isManualItem() && MedProfileItemStatus.Vetted_Verified.contains(mpi.getStatus())) {
				Date endDate = mpMoi.getEndDate();
				if( endDate == null || endDate.compareTo(now) >= 0 ){
					mpMoi.setFrozenFlag(Boolean.TRUE);
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void removeUnfinishedPivasRelatedRecords(MedProfileMoItem medProfileMoItem) {
		
		List<PivasWorklist> pivasWorklists = em.createQuery(
				"select o from PivasWorklist o" + // 20160615 index check : MedProfileMoItem.medProfileItemId : FK_MED_PROFILE_MO_ITEM_01
				" where o.medProfileMoItem.medProfileItemId = :medProfileItemId" +
				" and o.status in :activeStatusList")
				.setParameter("medProfileItemId", medProfileMoItem.getMedProfileItemId())
				.setParameter("activeStatusList", PivasWorklistStatus.New_Refill_Prepared)
				.setHint(QueryHints.FETCH, PIVAS_WORKLIST_QUERY_HINT_FETCH)
				.getResultList();
		
		markDeleteForPivasWorklists(pivasWorklists, false);
		
		List<PivasPrep> pivasPrepList = em.createQuery(
				"select o from PivasPrep o" + // 20161128 index check : MedProfileMoItem.medProfileItemId : FK_MED_PROFILE_MO_ITEM_01
				" where o.medProfileMoItem.medProfileItemId = :medProfileItemId" + 
				" and o.status <> :deleteStatus")
				.setParameter("medProfileItemId", medProfileMoItem.getMedProfileItemId())
				.setParameter("deleteStatus", RecordStatus.Delete)
				.getResultList();
		
		markDeleteForPivasPrepList(pivasPrepList);
		
		List<MaterialRequestItem> materialRequestItemList = em.createQuery(
				"select o from MaterialRequestItem o" + // 20160615 index check : MedProfileMoItem.medProfileItemId : FK_MED_PROFILE_MO_ITEM_01
				" where o.materialRequest.medProfileMoItem.medProfileItemId = :medProfileItemId" +
				" and o.status in :activeStatusList")
				.setParameter("medProfileItemId", medProfileMoItem.getMedProfileItemId())
				.setParameter("activeStatusList", MaterialRequestItemStatus.None_Outstanding)
				.getResultList();
		
		markDeleteForMaterialRequestItemList(materialRequestItemList);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void removeUnfinishedMaterialRequestItemsAndPivasOrderFromWorklist(MedProfile medProfile) {
		
		List<MaterialRequestItem> materialRequestItemList = em.createQuery(
				"select o from MaterialRequestItem o" + // 20161128 index check : MaterialRequest.medProfileId : FK_MATERIAL_REQUEST_02
				" where o.materialRequest.medProfileId = :medProfileId" +
				" and o.status in :activeStatusList")
				.setParameter("medProfileId", medProfile.getId())
				.setParameter("activeStatusList", MaterialRequestItemStatus.None_Outstanding)
				.getResultList();
		
		markDeleteForMaterialRequestItemList(materialRequestItemList);
		unlinkMaterialRequests(buildMaterialRequestIdSet(materialRequestItemList));
		removePivasOrderFromWorklist(medProfile);
	}
	
	private void markDeleteForPivasWorklists(List<PivasWorklist> pivasWorklists, boolean removePivasFlag) {
		
		for (PivasWorklist pivasWorklist: pivasWorklists) {
			pivasWorklist.setStatus(PivasWorklistStatus.SysDeleted);
			if (removePivasFlag) {
				pivasWorklist.getMedProfileMoItem().setPivasFlag(Boolean.FALSE);
			}
			if (pivasWorklist.getPivasPrep() != null) {
				pivasWorklist.getPivasPrep().setStatus(RecordStatus.Delete);
			}
		}
	}
	
	private void removePivasFlags(List<MedProfileMoItem> medProfileMoItemList) {
		
		for (MedProfileMoItem medProfileMoItem: medProfileMoItemList) {
			medProfileMoItem.setPivasFlag(Boolean.FALSE);
		}
	}
	
	private void markDeleteForPivasPrepList(List<PivasPrep> pivasPrepList) {
		
		for (PivasPrep pivasPrep: pivasPrepList) {
			pivasPrep.setStatus(RecordStatus.Delete);
		}
	}
	
	private void markDeleteForMaterialRequestItemList(List<MaterialRequestItem> materialRequestItemList) {
		
		for (MaterialRequestItem materialRequestItem: materialRequestItemList) {
			materialRequestItem.setStatus(MaterialRequestItemStatus.SysDeleted);
		}
	}
	
	private Set<Long> buildMaterialRequestIdSet(List<MaterialRequestItem> materialRequestItemList) {
		
		Set<Long> idSet = new TreeSet<Long>();
		for (MaterialRequestItem materialRequestItem: materialRequestItemList) {
			idSet.add(materialRequestItem.getMaterialRequestId());
		}
		return idSet;
	}
	
	@SuppressWarnings("unchecked")
	private void unlinkMaterialRequests(Set<Long> materialRequestIdSet) {
		
		List<PivasWorklist> pivasWorklists = (List<PivasWorklist>) CollectionUtils.union(
				QueryUtils.splitExecute(em.createQuery(
						"select o from PivasWorklist o" + // 20161128 index check : PivasWorklist.materialRequestId : FK_PIVAS_WORKLIST_03
						" where o.materialRequestId in :materialRequestIdSet"), "materialRequestIdSet", materialRequestIdSet),
				QueryUtils.splitExecute(em.createQuery(
						"select o from PivasWorklist o" + // 20161128 index check : PivasWorklist.firstDoseMaterialRequestId : I_PIVAS_WORKLIST_03
						" where o.firstDoseMaterialRequestId in :materialRequestIdSet"), "materialRequestIdSet", materialRequestIdSet));
		
		for (PivasWorklist pivasWorklist: pivasWorklists) {
			pivasWorklist.setMaterialRequest(null);
			pivasWorklist.setFirstDoseMaterialRequest(null);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void removePivasOrderFromWorklist(MedProfile medProfile) {
		
		List<PivasWorklist> pivasWorklists = em.createQuery(
				"select o from PivasWorklist o" + // 20161128 index check : PivasWorklist.medProfileId : FK_PIVAS_WORKLIST_04
				" where o.medProfileId = :medProfileId" +
				" and o.status = :status")
				.setParameter("medProfileId", medProfile.getId())
				.setParameter("status", PivasWorklistStatus.Prepared)
				.getResultList();
		
		for (PivasWorklist pivasWorklist: pivasWorklists) {
			pivasWorklist.setStatus(Boolean.TRUE.equals(pivasWorklist.getAdHocFlag()) ? PivasWorklistStatus.SysDeleted :
				(Boolean.TRUE.equals(pivasWorklist.getRefillFlag()) ? PivasWorklistStatus.Refill : PivasWorklistStatus.New));
		}
	}
	
	@Override
	public boolean updateSpecCodeAndWardCodeRelatedSetting(Workstore workstore, String orgSpecCode, String orgWardCode, boolean forceUpdateWardStock,
			MedProfile medProfile, List<MedProfileItem> medProfileItemList) {
		
		boolean wardCodeChanged = medProfile.getWardCode() != null && !StringUtils.equals(orgWardCode, medProfile.getWardCode());
		boolean specCodeChanged = medProfile.getSpecCode() != null && !StringUtils.equals(orgSpecCode, medProfile.getSpecCode());
		
		if (forceUpdateWardStock || wardCodeChanged) {
			updateWardStockRelatedSetting(workstore, medProfile, medProfileItemList);
		}
		if (wardCodeChanged) {
			updateWardTransferRelatedSetting(workstore, orgWardCode, medProfile, medProfileItemList);
		}
		if (specCodeChanged) {
			updateChargeSpecCode(workstore, medProfile.getSpecCode(), medProfileItemList);
		}
		return forceUpdateWardStock || wardCodeChanged || specCodeChanged;
	}
	
	private void updateWardTransferRelatedSetting(Workstore workstore, String orgWardCode, MedProfile medProfile, List<MedProfileItem> medProfileItemList) {

		WardConfig orgWardConfig = wardConfigManager.retrieveWardConfig(workstore.getWorkstoreGroup(), orgWardCode);
		WardConfig wardConfig = wardConfigManager.retrieveWardConfig(workstore.getWorkstoreGroup(), medProfile.getWardCode());
		
		medProfile.setPrintLang(wardConfig == null ? PrintLang.Eng : wardConfig.getDosageInstructionPrintLang());
		
		for (MedProfileItem medProfileItem: medProfileItemList) {
			updatePrintingRelatedSetting(wardConfig, medProfileItem);
		}
		
		boolean orgAtdpsWard = Boolean.TRUE.equals(orgWardConfig == null ? Boolean.FALSE : orgWardConfig.getAtdpsWardFlag());
		boolean currAtdpsWard = Boolean.TRUE.equals(wardConfig == null ? Boolean.FALSE : wardConfig.getAtdpsWardFlag());
		
		if (orgAtdpsWard != currAtdpsWard) {
			Set<String> atdpsItemCodeSet = retrieveAtdpsItemCodeSet(workstore); 
			for (MedProfileItem medProfileItem: medProfileItemList) {
				updateUnitDoseRelatedSetting(workstore, atdpsItemCodeSet, orgAtdpsWard, medProfileItem);
			}
		}
	}
	
	private void updatePrintingRelatedSetting(WardConfig wardConfig, MedProfileItem medProfileItem) {
		
		if (MedProfileItemStatus.Unvet_Vetted_Verified.contains(medProfileItem.getStatus())) {
			medProfileItem.getMedProfileMoItem().markDefaultPrintInstructionFlag(wardConfig);
		}
	}
	
	private void updateUnitDoseRelatedSetting(Workstore workstore, Set<String> atdpsItemCodeSet, boolean fromAtdpsWardToNonAtdpsWard, MedProfileItem medProfileItem) {
		
		if (!MedProfileItemStatus.Vetted_Verified.contains(medProfileItem.getStatus())) {
			return;
		}
		
		MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
		for (MedProfilePoItem medProfilePoItem: medProfileMoItem.getMedProfilePoItemList()) {
			if (!medProfilePoItem.isNoLabelItem() && atdpsItemCodeSet.contains(medProfilePoItem.getItemCode()) &&
				checkAtdpsManager.isValidAtdpsItem(workstore, medProfileMoItem, medProfilePoItem, true, false) &&
				Boolean.TRUE.equals(medProfilePoItem.getUnitDoseFlag()) == fromAtdpsWardToNonAtdpsWard &&
				MedProfileUtils.compare(medProfilePoItem.getDueDate(), medProfileMoItem.getEndDate()) != 0 &&
				(!medProfilePoItem.getSingleDispFlag() || medProfilePoItem.getLastDueDate() == null) &&
				!BigDecimal.ZERO.equals(medProfilePoItem.getRemainIssueQty())) {
				medProfileMoItem.setUnitDoseExceptFlag(Boolean.TRUE);
				break;
			}
		}
	}
	
	private void updateWardStockRelatedSetting(Workstore workstore, MedProfile medProfile, List<MedProfileItem> medProfileItemList) {
		
		Map<String, WardStock> wardStockMap = mpWardManager.retrieveWardStockMap(
				workstore.getWorkstoreGroup().getInstitution().getInstCode(), medProfile.getWardCode());

		Date now = new Date();
		Map<Integer, MedProfileMoItem> reDispFlagModifiedMedProfileMoItemMap = new TreeMap<Integer, MedProfileMoItem>();
		
		for (MedProfileItem medProfileItem: medProfileItemList) {
			updateWardStockRelatedSetting(wardStockMap, now, medProfileItem, reDispFlagModifiedMedProfileMoItemMap);
		}
		
		sendMessages(MpTrxType.MpUpdateDispensing, medProfile, new ArrayList<MedProfileMoItem>(reDispFlagModifiedMedProfileMoItemMap.values()));
	}
	
	private void updateWardStockRelatedSetting(Map<String, WardStock> wardStockMap, Date now, MedProfileItem medProfileItem,
			Map<Integer, MedProfileMoItem> reDispFlagModifiedMedProfileMoItemMap) {

		if (!MedProfileItemStatus.Vetted_Verified.contains(medProfileItem.getStatus())) {
			return;
		}
		
		MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
		
		for (MedProfilePoItem medProfilePoItem: medProfileMoItem.getMedProfilePoItemList()) {
			
			boolean wardStock = wardStockMap.containsKey(medProfilePoItem.getItemCode());
			
			if (Boolean.TRUE.equals(medProfilePoItem.getWardStockFlag()) && !wardStock &&
					!Boolean.TRUE.equals(medProfilePoItem.getDangerDrugFlag())) {
				if (Boolean.TRUE.equals(medProfilePoItem.getReDispFlag())) {
					reDispFlagModifiedMedProfileMoItemMap.put(medProfileMoItem.getItemNum(), medProfileMoItem);
				} else {
					medProfilePoItem.setDueDate(MedProfileUtils.max(now, medProfileMoItem.getStartDate()));
					logger.info("Due date reset due to ward stock/ward transfer, medProfileId=#0, dueDate=#1, phsWard=#2, itemCode=#3",
							medProfileItem.getMedProfileId(),
							medProfilePoItem.getDueDate(),
							medProfileItem.getMedProfile().getWardCode(),
							medProfilePoItem.getItemCode());
				}
				medProfilePoItem.setReDispFlag(Boolean.FALSE);
			}
			medProfilePoItem.setWardStockFlag(wardStock);
		}
	}
	
	private void updateChargeSpecCode(Workstore workstore, String specCode, List<MedProfileItem> medProfileItemList) {
		
		Map<ChargeSpecialtyType, String> chargeSpecCodeMap = chargeSpecialtyManager.retrieveChargeSpecialtyCodeMap(workstore, specCode);
		
		for (MedProfileItem medProfileItem: medProfileItemList) {
			updateChargeSpecCode(chargeSpecCodeMap, specCode, medProfileItem);
		}
	}
	
	private void updateChargeSpecCode(Map<ChargeSpecialtyType, String> chargeSpecCodeMap, String specCode, MedProfileItem medProfileItem) {
		
		if (!MedProfileItemStatus.Unvet_Vetted_Verified.contains(medProfileItem.getStatus())) {
			return;
		}
			
		for (MedProfilePoItem medProfilePoItem: medProfileItem.getMedProfileMoItem().getMedProfilePoItemList()) {
			medProfilePoItem.updateChargeSpecCode(chargeSpecCodeMap, specCode);
		}
	}
	
	private void updateSpecialtyMapping(MedCase medCase, PmsSpecialtyMap specialtyMap) {
		
		if (specialtyMap != null) {
			medCase.setPhsSpecCode(specialtyMap.getPhsSpecialty());
			medCase.setPhsWardCode(specialtyMap.getPhsWard());
		}
	}
	
	private void updateBodyInfo(MedProfile medProfile, BodyInfo bodyInfo) {

		BigDecimal bodyWeight = null;
		BigDecimal bodyHeight = null;
		Date bodyInfoUpdateDate = null;
		
		if (bodyInfo != null) {
			bodyWeight = MedProfileUtils.compare(BigDecimal.ZERO, bodyInfo.getBodyWeight()) == 0 ? null : bodyInfo.getBodyWeight();
			bodyHeight = MedProfileUtils.compare(BigDecimal.ZERO, bodyInfo.getBodyHeight()) == 0 ? null : bodyInfo.getBodyHeight();
			if (bodyWeight != null || bodyHeight != null) {
				bodyInfoUpdateDate = bodyInfo.getBodyInfoUpdateDate();
			}
		}
		
		medProfile.setBodyWeight(bodyWeight);
		medProfile.setBodyHeight(bodyHeight);
		medProfile.setBodyInfoUpdateDate(bodyInfoUpdateDate);
	}
	
	@Override
	public Patient retrievePasPatient(String patHospCode, String caseNum) {
		
		logger.debug("Retrieve PasPatient - PatHospCode : #0, CaseNum : #1",
				patHospCode, caseNum);
		
		if (patHospCode == null || caseNum == null) {
			return null;
		}

		try {
			Patient patient = pasServiceWrapper.retrievePasPatientByCaseNum(patHospCode, caseNum, false);
			logger.debug("Retrieve PasPatient Result : #0", marshaller.toXML(patient));
			return patient;
			
		} catch (PasException e) {
			logger.warn(e);
		} catch (Exception e) {
			logger.error(e, e);
		}
		
		logger.debug("Cannot retrieve PasPatient - PatHospCode : #0, CaseNum : #1",
				patHospCode, caseNum);
		
		return null;
	}
		
	@Override
	public Patient retrievePasPatient(String hkid) {

		if (hkid == null) {
			throw new IllegalArgumentException("Null value not allowed for argument hkid");
		}
		
		try {
			Patient patient = pasServiceWrapper.retrievePasPatientByHkid(hkid, null, null, false);
			return patient;
		} catch (PasException e) {
			logger.warn(e);
		} catch (Exception e) {
			logger.error(e, e);
		}

		return null;
	}
	
	@Override
	public Pair<String, Patient> retrievePasPatient(Workstore workstore, String caseNum) {
		
		if (workstore == null || caseNum == null) {
			return null;
		}
		
		List<String> patHospCodeList = buildPatHospCodeList(workstore);
		for (String patHospCode: patHospCodeList) {
			Patient patient = retrievePasPatient(patHospCode, caseNum);
			if (patient != null) {
				return Pair.of(patHospCode, patient);
			}
		}
		return null;
	}
	
	private List<String> buildPatHospCodeList(Workstore workstore) {
		
		List<String> patHospCodeList = new ArrayList<String>();
		
		for (HospitalMapping mapping: workstore.getHospital().getHospitalMappingList()) {
			if (mapping.getStatus() == RecordStatus.Active) {
				patHospCodeList.add(mapping.getPatHospCode());
			}
		}
		patHospCodeList.remove(workstore.getDefPatHospCode());
		patHospCodeList.add(0, workstore.getDefPatHospCode());
		
		return patHospCodeList;
	}
	
	private BodyInfo retrieveBodyInfo(String patHospCode, String patKey) {
		
		logger.debug("Retrieve BodyInfo - PatHospCode : #0, PatKey : #1",
				patHospCode, patKey);
		
		if (patHospCode == null || patKey == null) {
			return null;
		}
		
		try {
			BodyInfo bodyInfo = ipmoeServiceProxy.retrieveBodyInfo(patHospCode, patHospCode, "", patKey);
			if (bodyInfo != null) {
				logger.debug("Retrieve BodyInfo Result : #0", marshaller.toXML(bodyInfo));
				return bodyInfo;
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		
		logger.debug("Cannot retreive BodyInfo - PatHospCode : #0, PatKey : #1",
				patHospCode, patKey);
		
		return null;
	}
	
	private PmsSpecialtyMap retrieveSpecialtyMapping(String patHospCode, String pasSpecCode, String pasWardCode, String dispHospCode,
			String dispWorkstoreCode) {
		
		logger.debug("Retrieve SpecialtyMapping - PatHospCode : #0, PasSpecCode : #1, PasWardCode : #2, DispHospCode : #3," +
				" DispWorkstoreCode : #4", patHospCode, pasSpecCode, pasWardCode, dispHospCode, dispWorkstoreCode);
		
		PmsSpecMappingCriteria criteria = new PmsSpecMappingCriteria();
		criteria.setPatHospCode(patHospCode);
		criteria.setPasSpecialty(pasSpecCode);
		criteria.setWard(pasWardCode);
		criteria.setDispHospCode(dispHospCode);
		criteria.setDispWorkstore(dispWorkstoreCode);
		
		try {
			PmsSpecialtyMap specialtyMap = dmsPmsServiceProxy.retrieveIpSpecialtyMapping(criteria);
			if (specialtyMap != null) {
				logger.debug("Retrieve SpecialtyMapping Result : #0", marshaller.toXML(specialtyMap));
				return specialtyMap;
			}
		} catch (Exception e) {
			logger.error(e, e);
		}

		logger.debug("Cannot retrieve SpecialtyMapping - PatHospCode : #0, PasSpecCode : #1, PasWardCode : #2, DispHospCode : #3," +
				" DispWorkstoreCode : #4", patHospCode, pasSpecCode, pasWardCode, dispHospCode, dispWorkstoreCode);
		
		return null;
	}
	
	private boolean isValidPhsSpecialtyAndPhsWard(Workstore workstore, String specCode, String wardCode) {
		return isValidPhsSpecialty(workstore, specCode) && isValidPhsWard(workstore, wardCode);
	}
	
	private boolean isValidPhsSpecialty(Workstore workstore, String specCode) {

		if (workstore == null) {
			return false;
		}
		
		Long result = (Long) em.createQuery(
				"select count(s) from Specialty s" + // 20130829 index check : Specialty.institution,specCode : PK_SPECIALTY
				" where s.institution = :institution" + 
				" and s.specCode = :specCode" +
				" and s.status = :status")
				.setParameter("institution", workstore.getWorkstoreGroup().getInstitution())
				.setParameter("specCode", specCode)
				.setParameter("status", RecordStatus.Active)
				.getSingleResult();
		
		return result.longValue() > 0;
	}
	
	private boolean isValidPhsWard(Workstore workstore, String wardCode) {
		
		if (workstore == null) {
			return false;
		}
		
		Long result = (Long) em.createQuery(
				"select count(w) from Ward w" + // 20120225 index check : Ward.institution,wardCode : PK_WARD
				" where w.institution = :institution" + 
				" and w.wardCode = :wardCode" +
				" and w.status = :status")
				.setParameter("institution", workstore.getWorkstoreGroup().getInstitution())
				.setParameter("wardCode", wardCode)
				.setParameter("status", RecordStatus.Active)
				.getSingleResult();
		
		return result.longValue() > 0;
	}
	
    @Override
    public Date retrieveItemCutOffDate(MedProfile medProfile) {
    	
    	if (!medProfile.isManualProfile() && isWardTransferUpdateEnabled(medProfile.getWorkstore())) {
    		return retrieveItemCutOffDate(medProfile.getPatHospCode(), medProfile.getPatient().getHkid(), medProfile.getMedCase().getCaseNum());
    	}
    	return null;
    }
    
    public Date retrieveItemCutOffDate(String patHospCode, String hkid, String caseNum) {

    	try {
    		Date now = new Date();
    		
    		Date itemCutOffDate = mrServiceProxy.retrieveEarliestOrderDateFromMrCurrentDrugList(
    				patHospCode, now, CmsOrderType.Pharmacy, null,
    				hkid, caseNum, Boolean.TRUE,
        			"WKS", "SUPERMED");
    		
    		return itemCutOffDate == null ? now : itemCutOffDate;
    	} catch (MrException e) {
    		logger.warn(e, e);
    	} catch (Exception e) {
    		logger.error(e, e);
    	}
    	return null;
    }
    
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void refreshMedProfile(MedProfile medProfile, boolean refreshPasInfo, boolean refreshMrInfo) {
    	refreshMedProfileList(Arrays.asList(medProfile), refreshPasInfo, refreshMrInfo);
    }
    
	@Override
    public void refreshMedProfileList(List<MedProfile> medProfileList, boolean refreshPasInfo, boolean refreshMrInfo) {
    	
		Map<Long, Pair<PasContext, Date>> refreshInfoMap = buildRefreshInfoMap(medProfileList, refreshPasInfo, refreshMrInfo);
    	List<MedProfile> lockedMedProfileList = fetchLockedMedProfileList(refreshInfoMap.keySet());
    	applyRefreshInfo(refreshInfoMap, lockedMedProfileList);
    }
	
	private Map<Long, Pair<PasContext, Date>> buildRefreshInfoMap(List<MedProfile> medProfileList, boolean refreshPasInfo, boolean refreshMrInfo) {
		
		Map<Long, Pair<PasContext, Date>> refreshInfoMap = new TreeMap<Long, Pair<PasContext, Date>>();
		
		for (MedProfile medProfile: medProfileList) {
			if ((medProfile.getStatus() == MedProfileStatus.Inactive && !medProfile.isCancelAdmission()) ||
					Boolean.TRUE.equals(medProfile.getProcessingFlag()) || Boolean.TRUE.equals(medProfile.getDeliveryGenFlag())) {
				continue;
			}
			PasContext pasContext = (refreshPasInfo) ? retrieveContext(medProfile) : null;
			Date itemCutOffDate = (refreshMrInfo) ? retrieveItemCutOffDate(medProfile) : null;
			if (pasContext != null || itemCutOffDate != null) {
				refreshInfoMap.put(medProfile.getId(), Pair.of(pasContext, itemCutOffDate));
			}
		}
		return refreshInfoMap;
	}

    @SuppressWarnings("unchecked")
    private List<MedProfile> fetchLockedMedProfileList(Set<Long> idSet) {
    	
    	return QueryUtils.splitExecute(em.createQuery(
    			"select o from MedProfile o" + // 20130829 index check : MedProfile.id : PK_MED_PROFILE
    			" where o.processingFlag = false" +
    			" and o.deliveryGenFlag = false" +
    			" and (o.status <> :inactiveStatus" +
    			"  or o.inactiveType = :inactiveType)" +
    			" and o.id in :medProfileIdList")
    			.setParameter("inactiveStatus", MedProfileStatus.Inactive)
    			.setParameter("inactiveType", InactiveType.CancelAdmission)
    			.setHint(QueryHints.BATCH, MED_PROFILE_QUERY_HINT_BATCH)
    			.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock), "medProfileIdList", idSet);
    }
    
    @SuppressWarnings("unchecked")
    @Override
	public Set<String> retrieveAtdpsItemCodeSet(Workstore workstore) {
    	
    	Set<String> resultSet = new TreeSet<String>();
    	resultSet.addAll(em.createQuery(
    			"select distinct i.itemCode from ItemLocation i, OperationWorkstation o" + // 20151012 index check : OperationProfile.id Machine.id : PK_OPERATION_PROFILE PK_MACHINE
    			" where i.machine.type = :atdpsType" +
    			" and i.machine = o.machine" +
    			" and o.operationProfile.id = :operationProfileId" +
    			" and o.enableFlag = true")
    			.setParameter("atdpsType", MachineType.ATDPS)
    			.setParameter("operationProfileId", workstore.getOperationProfileId())
    			.getResultList());
    	
    	return resultSet;
    }
    
    private void applyRefreshInfo(Map<Long, Pair<PasContext, Date>> refreshInfoMap, List<MedProfile> medProfileList) {
    	
    	for (MedProfile medProfile: medProfileList) {
    		Pair<PasContext, Date> serviceResult = refreshInfoMap.get(medProfile.getId());
    		if (serviceResult.getLeft() != null) {
    			applyChanges(serviceResult.getLeft(), medProfile);
    		}
    		if (serviceResult.getRight() != null) {
    			markItemTransferInfo(medProfile, medProfile.getMedProfileItemList(), serviceResult.getRight(), WardTransferType.Manual);
    		}
    		medProfileStatManager.recalculateAllStat(medProfile, false);
    	}
    }
    
    @Override
    public List<MedProfileItem> markItemTransferInfo(MedProfile medProfile, List<MedProfileItem> medProfileItemList,
    		Date itemCutOffDate, WardTransferType type) {
    	
    	boolean inactiveWard = !activeWardManager.isActiveWard(medProfile.getPatHospCode(), medProfile.getMedCase().getPasWardCode());

    	Date now = new Date();
    	
    	if (itemCutOffDate == null || ((!Boolean.TRUE.equals(medProfile.getTransferFlag()) || inactiveWard) && type == WardTransferType.Normal)) {
    		return filterActiveMedProfileItemList(medProfileItemList, now); 
    	}
    	
    	if (type != WardTransferType.Manual) { 
    		medProfile.setTransferFlag(inactiveWard);
    	}
    	medProfile.setItemCutOffDate(itemCutOffDate);
    	
    	auditMarkItemTransfer(medProfile);
    	
    	for (MedProfileItem medProfileItem: medProfileItemList) {
    		markItemTransferInfo(medProfile, medProfileItem, itemCutOffDate, now);
    	}
    	return filterActiveMedProfileItemList(medProfileItemList, now);
    }
    
    private void auditMarkItemTransfer(MedProfile medProfile) {
    	
		Uam uam = (Uam) Component.getInstance(Uam.class, true);
		UamInfo originalUamInfo = uam.getUamInfo();
		UamInfo uamInfo = new UamInfo();
		uamInfo.setHospital(medProfile.getWorkstore().getHospCode());
		uam.setUamInfo(uamInfo);
    	auditLogger.log("#0706", medProfile.getOrderNum(), medProfile.getMedCase().getCaseNum(), medProfile.getItemCutOffDate());
		uam.setUamInfo(originalUamInfo);
    }
    
    private void markItemTransferInfo(MedProfile medProfile, MedProfileItem medProfileItem,
    		Date itemCutOffDate, Date now) {
    	
		MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
		if (medProfileMoItem == null || medProfileMoItem.isManualItem() || medProfileItem.isComplete(now)) {
			return;
		}
		
		if (MedProfileUtils.compare(medProfileItem.getMedProfileMoItem().getOrderDate(), itemCutOffDate) < 0) {
			medProfileMoItem.setTransferFlag(true);
			if (medProfileMoItem.getTransferDate() == null && medProfile.getTransferDate() != null) {
				medProfileMoItem.setTransferDate(medProfile.getTransferDate());
			}
		} else {
			medProfileMoItem.setTransferFlag(false);
			medProfileMoItem.setTransferDate(null);
		}
		medProfileItem.setStatusUpdateDate(now);
    }
    
    @Override
    public void datebackTransferInfo(MedProfile medProfile, MedProfileItem medProfileItem, MedProfileMoItem medProfileMoItem, Date now) {

    	Date itemCutOffDate = medProfile.getItemCutOffDate();
    	
    	if (itemCutOffDate == null || medProfileMoItem.isManualItem()) {
    		return;
    	}
    	
		if (MedProfileUtils.compare(medProfileMoItem.getOrderDate(), itemCutOffDate) < 0) {
			medProfileMoItem.setTransferFlag(true);
			medProfileMoItem.setTransferDate(medProfile.getTransferDate());
			medProfileItem.setStatusUpdateDate(now);
		}
    }
    
    @Override
    public List<MedProfileItem> filterActiveMedProfileItemList(List<MedProfileItem> medProfileItemList, Date now) {

    	List<MedProfileItem> resultList = new ArrayList<MedProfileItem>();
    	
    	for (MedProfileItem medProfileItem: medProfileItemList) {
    		MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
    		if (medProfileMoItem != null && !medProfileItem.isInactive(now)) {
    			resultList.add(medProfileItem);
    		}
    	}
    	return resultList;
    }
    
    private boolean isWardTransferUpdateEnabled(Workstore workstore) {
    	return MEDPROFILE_MR_ENABLED.get(workstore.getHospital(), Boolean.FALSE);
    }
    
    @SuppressWarnings("unchecked")
	public List<MedProfile> retrieveMedProfileList(String caseNum, String hkid, Workstore workstore) {
    	return em.createQuery(
    			"select p from MedProfile p" + // 20150206 index check : MedCase.caseNum, Patient.hkid : I_MED_CASE_01 I_PATIENT_02
    			" where p.hospCode = :hospCode" +
    			" and ((:caseNum is not null and p.medCase.caseNum = :caseNum)" + 
    			"  or (:hkid is not null and :caseNum is null and p.patient.hkid = :hkid and p.medCase.caseNum is null))" +
    			" and (p.status <> :status" +
    			"  or p.inactiveType is null" +
    			"  or p.inactiveType <> :type)")
    			.setParameter("hospCode", workstore.getHospCode())
    			.setParameter("caseNum", caseNum)
    			.setParameter("hkid", hkid)
    			.setParameter("status", MedProfileStatus.Inactive)
    			.setParameter("type", InactiveType.Deleted)
				.setHint(QueryHints.FETCH, "p.medCase")
				.setHint(QueryHints.FETCH, "p.patient")
    			.getResultList();
    }
}
