package hk.org.ha.model.pms.biz.delivery;

import static hk.org.ha.model.pms.prop.Prop.DELIVERY_ITEMCOUNT_REFRESH;
import static hk.org.ha.model.pms.prop.Prop.PIVAS_ENABLED;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.charging.DrugChargeClearanceManagerLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.PurchaseRequest;
import hk.org.ha.model.pms.persistence.pivas.MaterialRequestItem;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.medprofile.RetrievePurchaseRequestOption;

import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang3.ObjectUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;


@Stateful
@Scope(ScopeType.SESSION)
@Name("overdueSummaryService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class OverdueSummaryServiceBean implements OverdueSummaryServiceLocal {
	
	@In
	private DueOrderPrintManagerLocal dueOrderPrintManager;
	
	@In
	private DrugChargeClearanceManagerLocal drugChargeClearanceManager;
	
	@In
	private Workstore workstore;
	
	@Out(required = false)
	protected Date overdueSummaryUpdateDate;
	
	@Out(required = false)
	protected Long overdueNewCount;
	
	@Out(required = false)
	protected Long overdueRefillCount;
	
	@Out(required = false)
	protected Long forecastNewCount;
	
	@Out(required = false)
	protected Long forecastRefillCount;
	
	@Out(required = false)
	protected Long dueOrderPrintUrgentCount;
	
	@Out(required = false)
	protected Long statCount;
	
	@Out(required = false)
	protected Long followUpCount;
	
	@Out(required = false)
	protected Date sfiOverdueSummaryUpdateDate;
	
	@Out(required = false)
	protected Long paidCount;
	
	@Out(required = false)
	protected Long overduePaidCount;
	
	@Out(required = false)
	protected Long awaitPaymentCount;
	
	@Out(required = false)
	protected Long overdueAwaitPaymentCount;
	
	@Out(required = false)
	protected Long materialRequestItemCount;
	
	@Out(required = false)
	protected Long overdueMaterialRequestItemCount;
	
    public OverdueSummaryServiceBean() {
    }
    
    @Override
    public void countOverdueItems(boolean explict, boolean retrieveSfiItemCountOnly) {

    	Date now = new Date();
    	Date startDate = MedProfileUtils.getOverdueStartDate(now);
    	
    	countNormalOverdueItems(explict, retrieveSfiItemCountOnly, startDate, now);
    	countSfiOverdueItems(explict, retrieveSfiItemCountOnly, now);
    }
    
    private void countNormalOverdueItems(boolean explict, boolean retrieveSfiItemCountOnly, Date startDate, Date now) {
    	
    	if (retrieveSfiItemCountOnly) {
    		return;
    	}
    	
    	followUpCount = dueOrderPrintManager.retrieveFollowUpCount(workstore);
    	
    	if (!explict && !DELIVERY_ITEMCOUNT_REFRESH.get(Boolean.TRUE)) {
    		return;
    	}
    	
    	Date forecastDate = MedProfileUtils.addHours(now, 2);
    	
    	OverdueCount overdueCount = dueOrderPrintManager.retrieveOverdueCountByStat(workstore,
    			startDate, now, now, null, false);
    	OverdueCount forecastCount = dueOrderPrintManager.retrieveOverdueCountByStat(workstore,
    			now, forecastDate, now, null, false);
    	
    	overdueSummaryUpdateDate = now;
    	overdueNewCount = overdueCount.getNewCount();
    	overdueRefillCount = overdueCount.getRefillCount();
    	forecastNewCount = forecastCount.getNewCount();
    	forecastRefillCount = forecastCount.getRefillCount();
    	dueOrderPrintUrgentCount = dueOrderPrintManager.retrieveOverdueCountByStat(workstore, startDate, null, now,
    			true, null).getNewCount();
    	statCount = dueOrderPrintManager.retrieveOverdueCountByStat(workstore, startDate, null, now,
    			null, true).getNewCount();
    	
    	if (PIVAS_ENABLED.get(Boolean.FALSE)) {
    		countMaterialRequestOverdueItems(dueOrderPrintManager.retrieveMaterialRequestItemList(workstore, startDate, now), now);
    	}
    }
    
    private void countSfiOverdueItems(boolean explict, boolean checkFcsPaymentStatus, Date now) {
    	
    	if (!explict && !DELIVERY_ITEMCOUNT_REFRESH.get(Boolean.TRUE)) {
    		return;
    	}
    	
    	sfiOverdueSummaryUpdateDate = now;
    	List<PurchaseRequest> purchaseRequestList = dueOrderPrintManager.retrievePurchaseRequestList(workstore,
    			now, RetrievePurchaseRequestOption.All);
    	if (checkFcsPaymentStatus) {
    		drugChargeClearanceManager.updateDrugChargeClearance(purchaseRequestList);
    	}
    	countSfiOverdueItems(purchaseRequestList, now);
    }
    
    private void countSfiOverdueItems(List<PurchaseRequest> purchaseRequestList, Date now) {
    	
    	paidCount = 0L;
    	overduePaidCount = 0L;
    	awaitPaymentCount = 0L;
    	overdueAwaitPaymentCount = 0L;
    	
    	for (PurchaseRequest purchaseRequest: purchaseRequestList) {
    		
    		if (!InvoiceStatus.Outstanding_Settle.contains(purchaseRequest.getInvoiceStatus())) {
    			continue;
    		}
    		
    		boolean overdueFlag = ObjectUtils.compare(now, purchaseRequest.getDueDate(), true) > 0;
    		
			if (purchaseRequest.clearanceCheckRequired()) {
				awaitPaymentCount++;
				overdueAwaitPaymentCount += overdueFlag ? 1 : 0;
			} else {
		    	paidCount++;
		    	overduePaidCount += overdueFlag ? 1 : 0;
			}
		}
    }
    
    private void countMaterialRequestOverdueItems(List<MaterialRequestItem> materialRequestItemList, Date now) {
    	
    	materialRequestItemCount = Long.valueOf(materialRequestItemList.size());
    	overdueMaterialRequestItemCount = 0L;
    	
    	for (MaterialRequestItem materialRequestItem: materialRequestItemList) {
    		if (ObjectUtils.compare(now, materialRequestItem.getDueDate(), true) > 0) {
    			overdueMaterialRequestItemCount++;
    		}
    	}
    }
    
    @Remove
	@Override
	public void destroy() {
	}
}
