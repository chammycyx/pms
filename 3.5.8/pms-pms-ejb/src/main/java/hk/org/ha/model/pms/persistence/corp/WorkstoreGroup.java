package hk.org.ha.model.pms.persistence.corp;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "WORKSTORE_GROUP")
public class WorkstoreGroup extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "WORKSTORE_GROUP_CODE", length = 3)
	private String workstoreGroupCode;
	
    @ManyToOne
    @JoinColumn(name = "INST_CODE",  nullable = false)
    private Institution institution;
	
    @Converter(name = "WorkstoreGroup.status", converterClass = RecordStatus.Converter.class)
    @Convert("WorkstoreGroup.status")
	@Column(name="STATUS", nullable = false, length = 1)
	private RecordStatus status;
    
	@OneToMany(mappedBy = "workstoreGroup")
	private List<Workstore> workstoreList; 
	
	@OrderBy("sortSeq")
	@OneToMany(mappedBy = "workstoreGroup")
	private List<WorkstoreGroupMapping> workstoreGroupMappingList;
	
	public WorkstoreGroup() {
		status = RecordStatus.Active;
	}

	public WorkstoreGroup(String workstoreGroupCode) {
		this();
		this.workstoreGroupCode = workstoreGroupCode;
	}

	public String getWorkstoreGroupCode() {
		return workstoreGroupCode;
	}

	public void setWorkstoreGroupCode(String workstoreGroupCode) {
		this.workstoreGroupCode = workstoreGroupCode;
	}

	public Institution getInstitution() {
		return institution;
	}

	public void setInstitution(Institution institution) {
		this.institution = institution;
	}

	public RecordStatus getStatus() {
		return status;
	}

	public void setStatus(RecordStatus status) {
		this.status = status;
	}

	public List<Workstore> getWorkstoreList() {
		return workstoreList;
	}
	
	public void setWorkstoreList(List<Workstore> workstoreList) {
		this.workstoreList = workstoreList;
	}

	public void clearWorkstoreList() {
		workstoreList = new ArrayList<Workstore>();
	}

	public List<WorkstoreGroupMapping> getWorkstoreGroupMappingList() {
		return workstoreGroupMappingList;
	}

	public void setWorkstoreGroupMappingList(List<WorkstoreGroupMapping> workstoreGroupMappingList) {
		this.workstoreGroupMappingList = workstoreGroupMappingList;
	}
	
	public void clearWorkstoreGroupMappingList() {
		workstoreGroupMappingList = new ArrayList<WorkstoreGroupMapping>();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((workstoreGroupCode == null) ? 0 : workstoreGroupCode
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WorkstoreGroup other = (WorkstoreGroup) obj;
		if (workstoreGroupCode == null) {
			if (other.workstoreGroupCode != null)
				return false;
		} else if (!workstoreGroupCode.equals(other.workstoreGroupCode))
			return false;
		return true;
	}
}
