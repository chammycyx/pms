package hk.org.ha.model.pms.biz.alert.mds;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.alert.AlertAuditManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.AlertEntity;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItemAlert;
import hk.org.ha.model.pms.udt.medprofile.EndType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;
import hk.org.ha.model.pms.vo.alert.mds.ManualProfileCheckResult;
import hk.org.ha.model.pms.vo.alert.mds.MdsResult;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.beanutils.PropertyUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("manualProfileCheckService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ManualProfileCheckServiceBean implements ManualProfileCheckServiceLocal {

	private static final String JAXB_CONTEXT_RX = "hk.org.ha.model.pms.vo.rx";

	@In
	private AlertRetrieveManagerLocal alertRetrieveManager;
	
	@In
	private AlertMsgManagerLocal alertMsgManager;
	
	@In
	private AlertAuditManagerLocal alertAuditManager;
	
	@In
	private List<MedProfileItem> medProfileItemList;

	private List<MedProfileMoItem> retrieveAlerts(Collection<MedProfileItem> medProfileItems, MedProfile medProfile) {
		return retrieveAlerts(medProfileItems, medProfile, null);
	}
	
	private List<MedProfileMoItem> retrieveAlerts(Collection<MedProfileItem> medProfileItems, MedProfile medProfile, ManualProfileCheckResult manualProfileCheckResult) {
		MdsResult mdsResult = alertRetrieveManager.retrieveMpAlert(medProfileItems, medProfile, manualProfileCheckResult != null);
		Map<Integer, List<AlertEntity>> alertEntityMap = mdsResult.getAlertEntityMap();
		
		List<MedProfileMoItem> medProfileMoItemList = new ArrayList<MedProfileMoItem>();
		
		Date currentDate = Calendar.getInstance().getTime();

		for (MedProfileItem medProfileItem : medProfileItems) {

			MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
			medProfileMoItem.clearMedProfileMoItemAlertList();
			medProfileMoItem.setMdsSuspendReason(null);
			
			if (!alertEntityMap.containsKey(medProfileMoItem.getItemNum())) {
				continue;
			}
			for (AlertEntity alertEntity : alertEntityMap.get(medProfileMoItem.getItemNum())) {
				MedProfileMoItemAlert mpAlert = new MedProfileMoItemAlert(alertEntity);
				mpAlert.setCreateDate(currentDate);
				medProfileMoItem.addMedProfileMoItemAlert(mpAlert);
			}
			
			medProfileMoItemList.add(medProfileMoItem);
		}
		
		if (manualProfileCheckResult != null && !mdsResult.getErrorInfoList().isEmpty()) {
			manualProfileCheckResult.setErrorInfoList(mdsResult.getErrorInfoList());
			alertAuditManager.reminderMsgForManualProfile(
					"Save Profile",
					medProfile.getId(),
					medProfile.getPatient() == null ? null : medProfile.getPatient().getHkid(),
					medProfile.getMedCase() == null ? null : medProfile.getMedCase().getCaseNum(),
					mdsResult.getErrorInfoList()
				);
		}
		
		return medProfileMoItemList;
	}
	
	public ManualProfileCheckResult retrieveAlertForSaveItem(List<MedProfileItem> medProfileItemList, MedProfile medProfile) {

		Map<Integer, MedProfileItem> drugKeyMap = new HashMap<Integer, MedProfileItem>();
		DmDrug dmDrug;
		for (MedProfileItem medProfileItem : medProfileItemList) {
			dmDrug = DmDrugCacher.instance().getDmDrug(medProfileItem.getMedProfileMoItem().getMedProfilePoItemList().get(0).getItemCode());
			Integer key = dmDrug.getDrugKey();
			if (drugKeyMap.containsKey(key)) {
				removeItemCode(drugKeyMap.get(key));
			}
			else {
				drugKeyMap.put(key, medProfileItem);
				setItemCodeFromPharmLine(medProfileItem);
			}
		}

		ManualProfileCheckResult result = new ManualProfileCheckResult();
		for (MedProfileMoItem medProfileMoItem : retrieveAlerts(drugKeyMap.values(), medProfile)) {
			result.put(medProfileMoItem, alertMsgManager.retrieveMpPatAlertMsg(medProfileMoItem));
		}
		return result;
	}

	public ManualProfileCheckResult retrieveAlertForSaveProfile(MedProfile medProfile, Set<Integer> itemNumSet) {
		
		JaxbWrapper<RxItem> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
		
		List<MedProfileItem> newList = new ArrayList<MedProfileItem>();
		
		for (MedProfileItem medProfileItem : medProfileItemList) {
			if( medProfileItem.getMedProfileMoItem() == null || !medProfileItem.getMedProfileMoItem().isManualItem()){
				continue;
			}
			
			if( MedProfileItemStatus.Ended == medProfileItem.getStatus() && EndType.WardTransfered == medProfileItem.getEndType() ){
				continue;
			}
			if (!itemNumSet.contains(medProfileItem.getMedProfileMoItem().getItemNum())) {
				continue;
			}
			
			MedProfileItem mpi = new MedProfileItem();
			MedProfileMoItem mpMoi = new MedProfileMoItem();
			try {
				PropertyUtils.copyProperties(mpi, medProfileItem);
				PropertyUtils.copyProperties(mpMoi, medProfileItem.getMedProfileMoItem());
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
			mpi.setMedProfileMoItem(mpMoi);
			mpMoi.setMedProfileItem(mpi);
			if (mpMoi.getRxItem() != null) {
				mpMoi.setRxItem(rxJaxbWrapper.unmarshall(rxJaxbWrapper.marshall(mpMoi.getRxItem())));
			}
			newList.add(mpi);
		}
		
		ManualProfileCheckResult result = new ManualProfileCheckResult();
		for (MedProfileItem medProfileItem : newList) {
			setItemCodeFromPharmLine(medProfileItem);
			result.put(medProfileItem.getMedProfileMoItem(), new ArrayList<AlertMsg>());
		}
		
		for (MedProfileMoItem medProfileMoItem : retrieveAlerts(newList, medProfile, result)) {
			List<AlertMsg> alertMsgList = result.getMap().get(medProfileMoItem);
			if (!medProfileMoItem.getPatAlertList(false).isEmpty()) {
				alertMsgList.add(alertMsgManager.retrieveMpPatAlertMsg(medProfileMoItem));
			}
			alertMsgList.addAll(alertMsgManager.retrieveMpDdiAlertMsgList(medProfileMoItem));
		}
		return result;
	}

	public ManualProfileCheckResult retrieveAlertForVerifyItem(MedProfileItem medProfileItem, MedProfile medProfile) {
		setItemCodeFromPharmLine(medProfileItem);
		ManualProfileCheckResult result = new ManualProfileCheckResult();
		for (MedProfileMoItem medProfileMoItem : retrieveAlerts(Arrays.asList(medProfileItem), medProfile)) {
			result.put(medProfileMoItem, alertMsgManager.retrieveMpPatAlertMsg(medProfileMoItem));
		}
		return result;
	}
	
	private void setItemCodeFromPharmLine(MedProfileItem mpItem) {
		MedProfileMoItem mpMoItem = mpItem.getMedProfileMoItem();
		if (!mpMoItem.getRxItem().isRxDrug()) {
			return;
		}
		((RxDrug) mpMoItem.getRxItem()).setItemCode(mpMoItem.getMedProfilePoItemList().get(0).getItemCode());
	}
	
	private void removeItemCode(MedProfileItem mpItem) {
		MedProfileMoItem mpMoItem = mpItem.getMedProfileMoItem();
		if (!mpMoItem.getRxItem().isRxDrug()) {
			return;
		}
		((RxDrug) mpMoItem.getRxItem()).setItemCode(null);
	}

	@Remove
	public void destory() {
	}

}
