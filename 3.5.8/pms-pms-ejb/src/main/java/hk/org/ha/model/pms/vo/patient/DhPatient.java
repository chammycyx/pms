package hk.org.ha.model.pms.vo.patient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

import hk.org.ha.model.pms.persistence.PatientEntity;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "DhPatient")
@ExternalizedBean(type=DefaultExternalizer.class)
public class DhPatient extends PatientEntity {

	private static final long serialVersionUID = 1L;

	@XmlElement(required = true)	
	private Long pmiNum;

	@XmlElement
	private Boolean allergyFlag;
	
	@XmlElement
	private Boolean adrFlag;
	
	@XmlElement
	private Boolean alertFlag;
	
	public Long getPmiNum() {
		return pmiNum;
	}

	public void setPmiNum(Long pmiNum) {
		this.pmiNum = pmiNum;
	}

	public Boolean getAllergyFlag() {
		return allergyFlag;
	}

	public void setAllergyFlag(Boolean allergyFlag) {
		this.allergyFlag = allergyFlag;
	}

	public Boolean getAdrFlag() {
		return adrFlag;
	}

	public void setAdrFlag(Boolean adrFlag) {
		this.adrFlag = adrFlag;
	}

	public Boolean getAlertFlag() {
		return alertFlag;
	}

	public void setAlertFlag(Boolean alertFlag) {
		this.alertFlag = alertFlag;
	}
	
	public boolean isAlertExist() {
		return (alertFlag != null && alertFlag) || (adrFlag != null && adrFlag) || (allergyFlag != null && allergyFlag);
	}
}
