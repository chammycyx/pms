package hk.org.ha.model.pms.biz.support;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.reftable.CorporateProp;
import hk.org.ha.model.pms.udt.DataType;
import hk.org.ha.model.pms.udt.YesNoFlag;
import hk.org.ha.model.pms.udt.reftable.MaintScreen;
import hk.org.ha.model.pms.vo.support.PropInfoItem;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("corporatePropListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CorporatePropListServiceBean implements CorporatePropListServiceLocal {

	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
	
	@Out(required = false)
	private List<PropInfoItem> corporatePropInfoList;
	
	public void retrieveCorporatePropList() {
		corporatePropInfoList = new ArrayList<PropInfoItem>();
		
		List<CorporateProp> corporatePropList = corpPmsServiceProxy.retrieveCorporatePropList();
		if ( !corporatePropList.isEmpty() ) {
			convertCorporatePropListToCorporatePropInfoList(corporatePropList);
		}
	}
	
	private void convertCorporatePropListToCorporatePropInfoList(List<CorporateProp> corporatePropList) {
		
		for ( CorporateProp corpProp : corporatePropList ) {
			PropInfoItem corpPropInfo = new PropInfoItem();
			corporatePropInfoList.add(corpPropInfo);
			corpPropInfo.setProp(corpProp.getProp());
			corpPropInfo.setSortSeq(corpProp.getSortSeq());
			corpPropInfo.setSupportMaintFlag(getSupportMaintFlag(corpProp.getProp().getMaintScreen()));
			
			corpPropInfo.setPropItemId(corpProp.getId());
			corpPropInfo.setValue(corpProp.getValue());
			if ( corpPropInfo.getProp().getType() == DataType.BooleanType ) {
				corpPropInfo.setDisplayValue(getBooleanDisplayValue(corpProp.getValue()));
			} else {
				corpPropInfo.setDisplayValue(corpProp.getValue());
			}
		}
	}
	
	private String getBooleanDisplayValue(String dataValue) {
		return YesNoFlag.dataValueOf(dataValue).getDisplayValue();
	}
	
	private boolean getSupportMaintFlag(MaintScreen maintScreen) {
		return maintScreen == MaintScreen.Support;
	}
	
	public void updateCorporatePropList(List<PropInfoItem> inCorpPropInfoList) {
		corpPmsServiceProxy.updateCorporatePropList(inCorpPropInfoList);
		retrieveCorporatePropList();
	}
	
	@Remove
	public void destroy() 
	{
		if ( corporatePropInfoList != null ) {
			corporatePropInfoList = null;
		}
	}
}
