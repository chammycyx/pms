package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum MedCasePasPatInd implements StringValuedEnum {

	Normal("N", "Normal"),
	Private("P", "Private");

    private final String dataValue;
    private final String displayValue;

    MedCasePasPatInd(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }
	
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }  
    
    public static MedCasePasPatInd dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MedCasePasPatInd.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<MedCasePasPatInd> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<MedCasePasPatInd> getEnumClass() {
    		return MedCasePasPatInd.class;
    	}
    }
}
