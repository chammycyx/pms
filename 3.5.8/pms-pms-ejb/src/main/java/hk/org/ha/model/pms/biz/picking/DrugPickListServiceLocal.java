package hk.org.ha.model.pms.biz.picking;

import hk.org.ha.model.pms.vo.picking.DrugPickWorkstation;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface DrugPickListServiceLocal {

	List<DrugPickWorkstation> retrieveWorkstationList();
	
	List<Integer> retrieveAvaliableItemNumList(Date ticketDate, String ticketNum);
	
	void destroy();
	
}