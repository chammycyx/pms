package hk.org.ha.model.pms.exception.delivery;

import java.util.Map;

import org.granite.messaging.service.ExceptionConverter;
import org.granite.messaging.service.ServiceException;


public class DueOrderPrintExceptionConverter implements ExceptionConverter {

	public static final String DUE_ORDER_PRINT_EXCEPTION = "DueOrderPrintException";	
	
	@Override
	public boolean accepts(Throwable t, Throwable finalException) {
		return t.getClass().equals(DueOrderPrintException.class);
	}

	@Override
	public ServiceException convert(Throwable t, String detail, Map<String, Object> extendedData) {
		
		ServiceException se = new ServiceException(DUE_ORDER_PRINT_EXCEPTION, t.getMessage(), detail, t);
		se.getExtendedData().putAll(extendedData);
		se.getExtendedData().put("wardCode", ((DueOrderPrintException) t).getWardCode());
		se.getExtendedData().put("caseNum", ((DueOrderPrintException) t).getCaseNum());
		se.getExtendedData().put("alertException", ((DueOrderPrintException) t).getAlertException());
		return se;
	}	
	
}
