package hk.org.ha.model.pms.vo.medprofile;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="ErrorInfo")
@ExternalizedBean(type=DefaultExternalizer.class)
public class ErrorInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@XmlElement(name="SuspendItem")
	private List<ErrorInfoItem> suspendItemList;
	
	@XmlElement(name="FrozenItem")
	private List<ErrorInfoItem> frozenItemList;

	public List<ErrorInfoItem> getSuspendItemList() {
		if (suspendItemList == null) {
			suspendItemList = new ArrayList<ErrorInfoItem>();
		}
		return suspendItemList;
	}

	public void setSuspendItemList(List<ErrorInfoItem> suspendItemList) {
		this.suspendItemList = suspendItemList;
	}

	public List<ErrorInfoItem> getFrozenItemList() {
		if (frozenItemList == null) {
			frozenItemList = new ArrayList<ErrorInfoItem>(); 
		}
		return frozenItemList;
	}

	public void setFrozenItemList(List<ErrorInfoItem> frozenItemList) {
		this.frozenItemList = frozenItemList;
	}
}
