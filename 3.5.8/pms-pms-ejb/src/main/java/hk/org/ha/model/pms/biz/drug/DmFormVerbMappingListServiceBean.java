package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmFormVerbMapping;
import hk.org.ha.model.pms.vo.security.PropMap;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("dmFormVerbMappingListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DmFormVerbMappingListServiceBean implements DmFormVerbMappingListServiceLocal {

	@Out(required = false)
	private List<DmFormVerbMapping> dmFormVerbMappingList;

	@In
	private DmFormVerbMappingManagerLocal dmFormVerbMappingManager;
	
	public void retrieveDmFormVerbMappingListBySiteCode(String siteCode) {
		dmFormVerbMappingList = dmFormVerbMappingManager.getDmFormVerbMappingListBySiteCode(siteCode);
	}
	
	public PropMap getFormVerbProperties() {
		return dmFormVerbMappingManager.getFormVerbProperties();
	}
	
	@Remove
	public void destroy() {
		if ( dmFormVerbMappingList != null ) {
			dmFormVerbMappingList = null;
		}
	}
}
