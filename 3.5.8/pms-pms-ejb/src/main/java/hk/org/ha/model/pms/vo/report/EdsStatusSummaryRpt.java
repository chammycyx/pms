package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class EdsStatusSummaryRpt implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private EdsStatusSummaryRptHdr edsStatusSummaryRptHdr;
	
	private EdsStatusSummaryRptDtl edsStatusSummaryRptDtl;

	public EdsStatusSummaryRptDtl getEdsStatusSummaryRptDtl() {
		return edsStatusSummaryRptDtl;
	}

	public void setEdsStatusSummaryRptDtl(
			EdsStatusSummaryRptDtl edsStatusSummaryRptDtl) {
		this.edsStatusSummaryRptDtl = edsStatusSummaryRptDtl;
	}

	public EdsStatusSummaryRptHdr getEdsStatusSummaryRptHdr() {
		return edsStatusSummaryRptHdr;
	}

	public void setEdsStatusSummaryRptHdr(
			EdsStatusSummaryRptHdr edsStatusSummaryRptHdr) {
		this.edsStatusSummaryRptHdr = edsStatusSummaryRptHdr;
	}

		
}
