package hk.org.ha.model.pms.biz.charging;

import hk.org.ha.model.pms.dms.vo.pms.DrugChargeInfo;
import hk.org.ha.model.pms.udt.disp.PharmOrderPatType;
import hk.org.ha.model.pms.vo.charging.DrugChargeRuleCriteria;
import hk.org.ha.model.pms.vo.charging.OverrideChargeAmountRuleCriteria;

public interface ChargeCalculationInf {
	DrugChargeRuleCriteria checkPrivateHandleRule(DrugChargeRuleCriteria chargingRuleCriteria);
	DrugChargeRuleCriteria checkSfiItemChargeRule(DrugChargeRuleCriteria chargingRuleCriteria);
	Double retrieveMarkupFactorByChargeRule(OverrideChargeAmountRuleCriteria overrideChargeAmountRuleCriteria, DrugChargeInfo drugChargeInfo);
	Double retrieveHandlingAmtByChargeRule(DrugChargeRuleCriteria chargingRuleCriteria, DrugChargeInfo drugChargeInfo);
	boolean isMarkupProfitLimitEnable(DrugChargeRuleCriteria chargingRuleCriteria);
	Integer retrieveMaxProfitLimit(DrugChargeRuleCriteria chargingRuleCriteria);
	DrugChargeRuleCriteria retrieveChargeRuleByPatTypeFmStatus(PharmOrderPatType pharmOrderPatType, String fmStatus);
	void destroy();
}
