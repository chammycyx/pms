package hk.org.ha.model.pms.vo.alert.mds;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class MedProfileAlertMsg implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement(name="AlertMsg")
	private List<AlertMsg> alertMsgList;
	
	@XmlElement
	private String orderDesc;
	
	@XmlElement
	private boolean isIvRxDrug = false;

	public List<AlertMsg> getAlertMsgList() {
		if (alertMsgList == null) {
			return new ArrayList<AlertMsg>();
		}
		else {
			return new ArrayList<AlertMsg>(alertMsgList);
		}
	}

	public void setAlertMsgList(List<AlertMsg> alertMsgList) {
		if (alertMsgList == null) {
			this.alertMsgList = new ArrayList<AlertMsg>();
		}
		else {
			this.alertMsgList = new ArrayList<AlertMsg>(alertMsgList);
		}
	}

	public String getOrderDesc() {
		return orderDesc;
	}

	public void setOrderDesc(String orderDesc) {
		this.orderDesc = orderDesc;
	}
	
	public boolean isIvRxDrug() {
		return isIvRxDrug;
	}

	public void setIvRxDrug(boolean isIvRxDrug) {
		this.isIvRxDrug = isIvRxDrug;
	}

	public void loadDrugDesc() {
		for (AlertMsg alertMsg : getAlertMsgList()) {
			alertMsg.loadDrugDesc();
		}
	}
}
