package hk.org.ha.model.pms.biz.alert.mds;

import javax.ejb.Local;

@Local
public interface HlaTestServiceLocal {

	boolean hasHlaTestAlertBefore(String hkid, String patHospCode, String caseNum);

	void destroy();
}
