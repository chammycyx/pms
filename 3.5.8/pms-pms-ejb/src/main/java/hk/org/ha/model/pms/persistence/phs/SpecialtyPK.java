package hk.org.ha.model.pms.persistence.phs;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class SpecialtyPK implements Serializable {

	private static final long serialVersionUID = 1L;

	private String instCode;
	private String specCode;

	public SpecialtyPK() {
	}
	
	public SpecialtyPK(String instCode, String specCode) {
		this.instCode = instCode;
		this.specCode = specCode;
	}

	public String getInstCode() {
		return instCode;
	}

	public void setInstCode(String instCode) {
		this.instCode = instCode;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}
	
	public boolean equals(Object other) {
		return EqualsBuilder.reflectionEquals(this, other);
	}
	
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
}
