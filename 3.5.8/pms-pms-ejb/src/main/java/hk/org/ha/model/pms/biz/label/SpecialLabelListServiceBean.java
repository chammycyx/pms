package hk.org.ha.model.pms.biz.label;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.biz.reftable.CustomLabelManagerLocal;
import hk.org.ha.model.pms.persistence.reftable.CustomLabel;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.printing.PrintType;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.label.SpecialLabel;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.PrinterSelect;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
/**
 * Session Bean implementation class LabelServiceBean
 */
@Stateful
@Scope(ScopeType.SESSION)
@Name("specialLabelListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SpecialLabelListServiceBean implements SpecialLabelListServiceLocal {

	@Out(required = false)
	private List<SpecialLabel> specialLabelList;
	
	@In
	private CustomLabelManagerLocal customLabelManager;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent; 
	
	public void retrieveSpecialLabelList(String labelNumPrefix){
		specialLabelList = new ArrayList<SpecialLabel>();
		
		List<CustomLabel> customLabelList = customLabelManager.retrieveCustomLabelByLabelNum(labelNumPrefix);
		
		for (CustomLabel customLabel : customLabelList){
			SpecialLabel specialLabel = customLabel.getSpecialLabel();
			specialLabel.setDescription(customLabel.getDescription());
			specialLabelList.add(specialLabel);
		}
		
	}
	
	public void printSpecialLabel(SpecialLabel specialLabel) {
		specialLabel.setLine1(specialLabel.getSpecialLabelInstructionList().get(0).getLine());
		specialLabel.setLine2(specialLabel.getSpecialLabelInstructionList().get(1).getLine());
		specialLabel.setLine3(specialLabel.getSpecialLabelInstructionList().get(2).getLine());
		specialLabel.setLine4(specialLabel.getSpecialLabelInstructionList().get(3).getLine());
		specialLabel.setLine5(specialLabel.getSpecialLabelInstructionList().get(4).getLine());
		specialLabel.setLine6(specialLabel.getSpecialLabelInstructionList().get(5).getLine());
		specialLabel.setLine7(specialLabel.getSpecialLabelInstructionList().get(6).getLine());

		PrinterSelect ps = printerSelector.retrievePrinterSelect(PrintDocType.OtherLabel);
		ps.setCopies(specialLabel.getCopies());
		printAgent.renderAndPrint(new RenderAndPrintJob( 
				"SpecialLabel", 
				new PrintOption(PrintType.Normal, PrintLang.Eng),
				ps,
				Arrays.asList(specialLabel),
				"[SpecialLabel] LabelNum:" + specialLabel.getLabelNum()
				));
	}
	
	@Remove
	public void destroy() {
		if ( specialLabelList != null ) {
			specialLabelList = null;
		}
	}

}