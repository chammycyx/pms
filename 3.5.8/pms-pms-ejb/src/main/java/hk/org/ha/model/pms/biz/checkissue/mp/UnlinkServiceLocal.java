package hk.org.ha.model.pms.biz.checkissue.mp;

import hk.org.ha.model.pms.vo.checkissue.mp.CheckDelivery;

import java.util.Date;

import javax.ejb.Local;

@Local
public interface UnlinkServiceLocal {
	
	CheckDelivery updateDeliveryItemStatuToUnlink(CheckDelivery inCheckOrder);
	
	
	CheckDelivery retrieveDeliveryItemByForUnlinkDrug(
			Date batchDate,
		String batchNum,
		String caseNum,
		String mpDispLabelQrCodeXml
		);
	
	String getResultMsg();
	
	String getBatchNum();
	
	void destroy();
	
}