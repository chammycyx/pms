package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.corp.Workstore;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("dmDrugService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DmDrugServiceBean implements DmDrugServiceLocal {

	@In
	private Workstore workstore;

	@In
	private DmDrugCacherInf dmDrugCacher;
		
	@Out(required = false)
	private DmDrug dmDrug;
	
	public void retrieveDmDrug(String itemCode)
	{	
		dmDrug = dmDrugCacher.getDmDrug(itemCode);
	}
	
	public void retrieveDmDrugForOrderEdit(String displayName, String fmStatus, String itemCode)
	{
		List<DmDrug> dmDrugList = dmDrugCacher.getDmDrugListByDisplayNameFmStatus(workstore, displayName, fmStatus);	
		List<DmDrug> solventList = dmDrugCacher.getDmDrugListBySolvent(workstore);
		dmDrug = null;
		List<DmDrug> drugList = new ArrayList<DmDrug>();
		drugList.addAll(dmDrugList);
		drugList.addAll(solventList);
		
		if ( drugList == null || drugList.size() == 0 ) {
			return;			
		}
		
		for ( DmDrug drug : drugList ) {			
			if ( drug.getItemCode().equals(itemCode) ) {
				dmDrug = drug;
				break;
			}
		}		
				
	}
	
	@Remove
	public void destroy() 
	{
		if (dmDrug != null) {
			dmDrug = null;
		}
	}

}
