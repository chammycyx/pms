package hk.org.ha.model.pms.udt.prevet;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PreVetMode implements StringValuedEnum {
	
	OneStopEntry("O", "OneStopEntry"),
	Inbox("I", "Inbox"),
	Vetting("V", "Vetting");
	
    private final String dataValue;
    private final String displayValue;
        
    PreVetMode(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static PreVetMode dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PreVetMode.class, dataValue);
	}
        
	public static class Converter extends StringValuedEnumConverter<PreVetMode> {

		private static final long serialVersionUID = 1L;

		public Class<PreVetMode> getEnumClass() {
    		return PreVetMode.class;
    	}
    }
}
