package hk.org.ha.model.pms.biz.report;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.corp.WorkstorePK;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.persistence.disp.RefillScheduleItem;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.refill.DocType;
import hk.org.ha.model.pms.udt.refill.RefillScheduleStatus;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.report.RefillExportType;
import hk.org.ha.model.pms.udt.report.RefillItemRptStatus;
import hk.org.ha.model.pms.udt.report.RefillReportType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.PharmClinicRefillCouponRpt;
import hk.org.ha.model.pms.vo.report.PharmClinicRefillCouponRptDtl;
import hk.org.ha.model.pms.vo.report.PharmClinicRefillCouponRptHdr;
import hk.org.ha.model.pms.vo.report.RefillExpiryRpt;
import hk.org.ha.model.pms.vo.report.RefillItemRpt;
import hk.org.ha.model.pms.vo.report.RefillItemRptDtl;
import hk.org.ha.model.pms.vo.report.RefillItemRptSummary;
import hk.org.ha.model.pms.vo.report.RefillQtyForecastRpt;
import hk.org.ha.model.pms.vo.report.RefillStatRpt;
import hk.org.ha.model.pms.vo.report.RefillWorkloadForecastRpt;
import hk.org.ha.model.pms.vo.rx.DoseGroup;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import net.lingala.zip4j.exception.ZipException;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.joda.time.DateMidnight;
import org.joda.time.Days;

@Stateful
@Scope(ScopeType.SESSION)
@Name("refillRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class RefillRptServiceBean implements RefillRptServiceLocal {

	private static final String REFILL_WORKLOAD_FORECAST_RPT_CLASS_NAME = RefillWorkloadForecastRpt.class.getName();
	
	private static final String REFILL_QTY_FORECAST_RPT_CLASS_NAME = RefillQtyForecastRpt.class.getName();
	
	private static final String REFILL_EXPIRY_RPT_CLASS_NAME = RefillExpiryRpt.class.getName();
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent; 
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In 
	private Workstore workstore;

	private List<RefillItemRptDtl> refillItemRptDtlList;
	
	private List<RefillWorkloadForecastRpt> refillWorkloadForecastRptList;
	
	private List<RefillQtyForecastRpt> refillQtyForecastRptList;
	
	private List<RefillStatRpt> refillStatRptList;
	
	private List<RefillExpiryRpt> refillExpiryRptList;
	
	private List<PharmClinicRefillCouponRpt> pharmClinicRefillCouponRptList;
	
	private boolean retrieveSuccess;
	
	private boolean validItemCode;
	
	private Map<String, Object> parameters = new HashMap<String, Object>();
	private RefillReportType currRefillReportType = null;
	private List<?> reportObj = null;
	private List<?> exportObj = null;
	private String xlsName = null;
	private List<PharmClinicRefillCouponRptDtl> pharmClinicRefillCouponRptDtlList;
	
	private String exportPassword;

	public void retrieveRefillRpt(RefillReportType reportType, String workstoreCode, Date refillDueDateFrom, Date refillDueDateTo, String itemCode, RefillItemRptStatus status, Boolean drsPatientFlag) {
		
		if(reportType==RefillReportType.RefillItemReport) {
			xlsName = RefillExportType.RefillItemExport.getDataValue();
			currRefillReportType = reportType;
			reportObj = retrieveRefillItemRptList(workstoreCode, refillDueDateFrom, refillDueDateTo, itemCode, status, drsPatientFlag);
			exportObj = refillItemRptDtlList;
		}
		
		if(reportType==RefillReportType.WorkloadForecastReport) {
			
			currRefillReportType = reportType;
			reportObj = retrieveRefillWorkloadRptList(workstoreCode, refillDueDateFrom, refillDueDateTo);
			xlsName = RefillExportType.WorkloadForecastExport.getDataValue();
			exportObj = refillWorkloadForecastRptList;
		}
		
		if(reportType==RefillReportType.RefillQtyForecastReport) {
			
			currRefillReportType = reportType;
			reportObj = retrieveRefillQtyForecastRptList(workstoreCode, refillDueDateFrom, refillDueDateTo, itemCode);
			xlsName = RefillExportType.RefillQtyForecastExport.getDataValue();
			exportObj = refillQtyForecastRptList;
		}
		
		if(reportType==RefillReportType.RefillStatReport) {
			
			currRefillReportType = reportType;
			reportObj = retrieveRefillStatRptList(workstoreCode, refillDueDateFrom, refillDueDateTo);
			xlsName = RefillExportType.RefillStatExport.getDataValue();
			exportObj = refillStatRptList;
		}
		
		if(reportType==RefillReportType.ExpireRefillReport) {
			
			currRefillReportType = reportType;
			reportObj = retrieveRefillExpiryRpt(workstoreCode, refillDueDateFrom, refillDueDateTo);
			xlsName = RefillExportType.ExpireRefillExport.getDataValue();
			exportObj = refillExpiryRptList;
		}
		
		if(reportType==RefillReportType.PharmClinicRefillCouponReport) {
			currRefillReportType = reportType;
			xlsName = RefillExportType.PharmClinicRefillCouponExport.getDataValue();
			reportObj = retrievePharmClinicRefillCouponRpt(workstoreCode, refillDueDateFrom, refillDueDateTo);
			exportObj = pharmClinicRefillCouponRptDtlList;
		}
	}
	
	public List<RefillItemRpt> retrieveRefillItemRptList(String workstoreCode, Date dateFrom, Date dateTo, String itemCode, RefillItemRptStatus status, Boolean drsPatientFlag) {
		validItemCode = false;
	
		checkValidItemCode(itemCode);
		
		List<RefillItemRpt> refillItemRptList = new ArrayList<RefillItemRpt>();
		if (validItemCode) {
			List<RefillScheduleItem> refillScheduleItemList = genRefillScheduleItemList( workstoreCode,  dateFrom,  dateTo,  itemCode,  status, drsPatientFlag);
			
			refillItemRptDtlList = genRefillItemRptList(refillScheduleItemList, drsPatientFlag);

			RefillItemRptSummary refillItemRptSummary = calculateRefillItemRptSummary(refillItemRptDtlList);
			if (itemCode.isEmpty()) {
				refillItemRptSummary.setTotalRefillQty(null);
			}

			RefillItemRpt refillItemRpt = new RefillItemRpt();
			refillItemRpt.setRefillItemRptDtlList(refillItemRptDtlList);
			refillItemRpt.setRefillItemRptSummary(refillItemRptSummary);
			refillItemRpt.setDateFrom(dateFrom);
			refillItemRpt.setDateTo(dateTo);
			refillItemRpt.setStatusFilter(status.getDisplayValue());
			
			refillItemRptList.add(refillItemRpt);

			if ( !refillScheduleItemList.isEmpty() ) {

				parameters.clear();
				parameters.put("workstoreCode", workstoreCode);
				parameters.put("drsPatientFlag", drsPatientFlag);
				retrieveSuccess = true;
			}
			else{
				retrieveSuccess = false;
			}
		}
		return refillItemRptList;
	}
	
	private void checkValidItemCode(String itemCode){
		if(StringUtils.isBlank(itemCode)) {
			validItemCode = true;
		} else {
			DmDrug dmDrug = dmDrugCacher.getDmDrug(itemCode);
			if ( dmDrug == null){
				validItemCode = false;
			} else {
				validItemCode = true;
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<RefillScheduleItem> genRefillScheduleItemList(String workstoreCode, Date dateFrom, Date dateTo, String itemCode, RefillItemRptStatus status, Boolean drsPatientFlag) {
		
		Workstore ws = em.find(Workstore.class, new WorkstorePK(workstore.getHospCode(), workstoreCode));
		RefillScheduleStatus rsStatus = null;
		DispOrderAdminStatus dispOrderAdminStatus = null;

		if (status.equals(RefillItemRptStatus.Suspended)){
			dispOrderAdminStatus = DispOrderAdminStatus.Suspended;
			
		} else if (status==RefillItemRptStatus.BeingProcessed) {	
			rsStatus = RefillScheduleStatus.BeingProcessed;
			dispOrderAdminStatus = DispOrderAdminStatus.Normal;
				
		} else if (status==RefillItemRptStatus.Delete) {
			rsStatus = RefillScheduleStatus.Abort;
			
		} else if (status==RefillItemRptStatus.Dispensed) {
			rsStatus = RefillScheduleStatus.Dispensed;
			dispOrderAdminStatus = DispOrderAdminStatus.Normal;
			
		} else if (status==RefillItemRptStatus.ForceComplete) {
			rsStatus = RefillScheduleStatus.ForceComplete;
			
		} else if (status==RefillItemRptStatus.NotYetDispensed) {
			rsStatus = RefillScheduleStatus.NotYetDispensed;
			
		}
		
		StringBuilder sb = new StringBuilder(
				"select o from RefillScheduleItem o" + // 20120306 index check : RefillSchedule.workstore,refillDate : I_REFILL_SCHEDULE_03
				" where o.refillSchedule.refillDate between :dateFrom and :dateTo" + 
				" and o.refillSchedule.docType = :docType" +
				" and o.refillSchedule.refillNum > 1");
		
		if ( drsPatientFlag ) {
			sb.append(" and o.refillSchedule.workstore.hospCode = :hospCode");
			sb.append(" and o.refillSchedule.pharmOrder.drsFlag = true");
		} else {
			sb.append(" and o.refillSchedule.workstore = :workstore");
		}
		
		if (rsStatus !=null) {
			sb.append(" and o.refillSchedule.status = :status");
		} else {
			sb.append(" and o.refillSchedule.status <> :status");
		}
		
		if (dispOrderAdminStatus != null) {
			sb.append(" and o.refillSchedule.dispOrder.adminStatus = :adminStatus" );
		}
		
		if (StringUtils.isNotBlank(itemCode)) {
			sb.append(" and o.pharmOrderItem.itemCode = :itemCode" );
		}
		
		sb.append(" order by o.refillSchedule.refillDate, o.refillSchedule.refillCouponNum");
		
		Query query = em.createQuery(sb.toString());
		
		if ( drsPatientFlag ) {
			query.setParameter("hospCode", workstore.getHospCode());
		} else {
			query.setParameter("workstore", ws);
		}
		
		query.setParameter("dateFrom", dateFrom, TemporalType.DATE);
		query.setParameter("dateTo", dateTo, TemporalType.DATE);
		query.setParameter("docType", DocType.Standard);
		
		if (rsStatus !=null){
			query.setParameter("status", rsStatus);	
		} else {
			query.setParameter("status", RefillScheduleStatus.SysDeleted);	
		}
		
		if (dispOrderAdminStatus != null) {
			query.setParameter("adminStatus", dispOrderAdminStatus);
		}
		
		if (!itemCode.isEmpty()) {
			query.setParameter("itemCode", itemCode);
		}

		return query.getResultList();

	}
	
	private List<RefillItemRptDtl> genRefillItemRptList( List<RefillScheduleItem> refillScheduleItemList, Boolean drsPatientFlag) {
		List<RefillItemRptDtl> tmpRefillItemRptDtlList = new ArrayList<RefillItemRptDtl>();
		
		for (RefillScheduleItem refillScheduleItem : refillScheduleItemList) {
			RefillSchedule rs = refillScheduleItem.getRefillSchedule();
			PharmOrder po = rs.getPharmOrder();
			PharmOrderItem poi = refillScheduleItem.getPharmOrderItem();
			MedOrder mo = po.getMedOrder();
			
			RefillItemRptDtl refillItemRptDtl = new RefillItemRptDtl();
			refillItemRptDtl.setRefillCouponNum(rs.getRefillCouponNum());
			
			if (po.getMedCase() != null) {
				refillItemRptDtl.setCaseNum(po.getMedCase().getCaseNum());
			}
			
			refillItemRptDtl.setPatName(po.getPatient().getName());
			
			if (po.getPatient().getHkid()!=null ) {
				refillItemRptDtl.setHkid(po.getPatient().getHkid());
			} else {
				refillItemRptDtl.setHkid(po.getPatient().getOtherDoc());
			}
			
			if (mo.getDocType() != MedOrderDocType.Manual) {
				refillItemRptDtl.setOrderNum(mo.getOrderNum());
				refillItemRptDtl.setRefNum(mo.getRefNum());
			}
			
			refillItemRptDtl.setFullDrugDesc(poi.getFullDrugDesc());			
			refillItemRptDtl.setRefillDate(rs.getRefillDate()); 

			Days d = Days.daysBetween( new DateMidnight(rs.getRefillDate()), new DateMidnight());
			int overDuePeriod = d.getDays();
			
			refillItemRptDtl.setOverDuePeriod( String.valueOf(overDuePeriod) );
			refillItemRptDtl.setRefillNum(rs.getRefillNum().toString());
			refillItemRptDtl.setNumOfRefill(refillScheduleItem.getNumOfRefill().toString());
			
			if (rs.getDispOrder() !=null) {
				refillItemRptDtl.setDoWorkstoreCode(rs.getDispOrder().getWorkstore().getWorkstoreCode());
				refillItemRptDtl.setSpecCode(rs.getDispOrder().getSpecCode());
				refillItemRptDtl.setDispDate(rs.getDispOrder().getDispDate());
			}else {
				refillItemRptDtl.setSpecCode(po.getSpecCode());
			}
			
			DoseGroup doseGrp = poi.getRegimen().getDoseGroupList().get(0);
			refillItemRptDtl.setItemTotalDuration(doseGrp.getDuration().toString());
			refillItemRptDtl.setDurationUnit(doseGrp.getDurationUnit().getDataValue());
			
			refillItemRptDtl.setTotalQty(poi.getIssueQty().toString());
			refillItemRptDtl.setBaseUnit(poi.getBaseUnit());
			
			refillItemRptDtl.setRefillDuration(refillScheduleItem.getRefillDuration().toString());
			
			refillItemRptDtl.setRefillQty(refillScheduleItem.getRefillQty().toString());
			refillItemRptDtl.setAdjQty(refillScheduleItem.getAdjQty().toString());
			refillItemRptDtl.setStatus(rs.getStatus().getDisplayValue());
			
			if ( "Dispensed".equals(rs.getStatus().getDisplayValue()) ) {
				refillItemRptDtl.setStatus(RefillItemRptStatus.Dispensed.getDisplayValue());
			}
			
			if ( "Force Complete".equals(rs.getStatus().getDisplayValue()) ) {
				refillItemRptDtl.setStatus(RefillItemRptStatus.ForceComplete.getDisplayValue());
			}
			
			if (rs.getDispOrder() != null && rs.getDispOrder().getAdminStatus() == DispOrderAdminStatus.Suspended) {
				refillItemRptDtl.setStatus(DispOrderAdminStatus.Suspended.getDisplayValue());
			}
			
			if (rs.getDispOrder() == null) {
				DmDrug dmDrug =  dmDrugCacher.getDmDrug(poi.getItemCode());	
				refillItemRptDtl.setAvgUnitPrice(dmDrug.getDrugChargeInfo().getCorpDrugPrice());
				
			}else {
				refillItemRptDtl.setAvgUnitPrice((Double)refillScheduleItem.getUnitPrice().doubleValue());
			}
			
			if ( drsPatientFlag ) {
				refillItemRptDtl.setDrsWorkstoreCode(rs.getWorkstore().getWorkstoreCode());
			}
			
			tmpRefillItemRptDtlList.add(refillItemRptDtl);

		}
		
		return tmpRefillItemRptDtlList;
	}
	
	@SuppressWarnings("unchecked")
	private RefillItemRptSummary calculateRefillItemRptSummary(List<RefillItemRptDtl> refillItemRptDtlList) {
		Map<String, Integer> itemMap  = new HashMap<String, Integer>();
		Map<String, Double> statusMap  = new HashMap<String, Double>();
		Map<String, HashSet> orderMap  = new HashMap<String, HashSet>();
		Integer totalRefillQty = 0;
		
		for  (RefillItemRptStatus s : RefillItemRptStatus.values()) {
			itemMap.put(s.getDisplayValue(), 0);
			statusMap.put(s.getDisplayValue(), 0.0000);
		}
		
		for (RefillItemRptDtl dtl : refillItemRptDtlList) {	
			Integer i = itemMap.get(dtl.getStatus());
			i = i+1;
			itemMap.put(dtl.getStatus(), i );
			
			Double cost = Integer.valueOf(dtl.getRefillQty()) * dtl.getAvgUnitPrice();
			Double total = statusMap.get(dtl.getStatus());
			statusMap.put(dtl.getStatus(), total + cost);
			
			if (!orderMap.containsKey(dtl.getStatus())) {
				HashSet orderSet = new HashSet();
				orderSet.add(dtl.getRefillCouponNum());
				orderMap.put(dtl.getStatus(), orderSet);
			}else {
				HashSet orderSet = orderMap.get(dtl.getStatus());
				orderSet.add(dtl.getRefillCouponNum());
				orderMap.put(dtl.getStatus(), orderSet);
			}
			
			totalRefillQty = totalRefillQty + Integer.parseInt(dtl.getRefillQty());
			
		}
		
		
		RefillItemRptSummary refillItemRptSummary = new RefillItemRptSummary();
		refillItemRptSummary.setTotalRefillQty(totalRefillQty.toString());
		for (Entry entry: statusMap.entrySet()) {			
			if (entry.getKey() == RefillItemRptStatus.BeingProcessed.getDisplayValue()){
				refillItemRptSummary.setBeingProcessedItemCount(itemMap.get( entry.getKey() ));
				refillItemRptSummary.setBeingProcessedDrugCost(statusMap.get(entry.getKey()));
				
				if (!orderMap.containsKey(entry.getKey())) {
					refillItemRptSummary.setBeingProcessedRefillCount(0);
				}else{
					refillItemRptSummary.setBeingProcessedRefillCount(orderMap.get(entry.getKey()).size());
				}
			}
			else if (entry.getKey() == RefillItemRptStatus.Dispensed.getDisplayValue()) {
				refillItemRptSummary.setCompletedItemCount(itemMap.get( entry.getKey() ));
				refillItemRptSummary.setCompletedDrugCost(statusMap.get(entry.getKey()));
				
				
				if (!orderMap.containsKey(entry.getKey())) {
					refillItemRptSummary.setCompletedRefillCount(0);
				}else{
					refillItemRptSummary.setCompletedRefillCount(orderMap.get(entry.getKey()).size());
				}
			}
			else if (entry.getKey() == RefillItemRptStatus.Delete.getDisplayValue()) {
				refillItemRptSummary.setDeletedItemCount(itemMap.get( entry.getKey() ));
				refillItemRptSummary.setDeletedDrugCost(statusMap.get(entry.getKey()));
				
				
				if (!orderMap.containsKey(entry.getKey())) {
					refillItemRptSummary.setDeletedRefillCount(0);
				}else{
					refillItemRptSummary.setDeletedRefillCount(orderMap.get(entry.getKey()).size());
				}
			}
			else if (entry.getKey() == RefillItemRptStatus.ForceComplete.getDisplayValue()) {
				refillItemRptSummary.setForceCompletedItemCount(itemMap.get( entry.getKey() ));
				refillItemRptSummary.setForceCompletedDrugCost(statusMap.get(entry.getKey()));
				
				
				if (!orderMap.containsKey(entry.getKey())) {
					refillItemRptSummary.setForceCompletedRefillCount(0);
				}else{
					refillItemRptSummary.setForceCompletedRefillCount(orderMap.get(entry.getKey()).size());
				}
			}
			else if (entry.getKey() == RefillItemRptStatus.NotYetDispensed.getDisplayValue()) {
				refillItemRptSummary.setNotYetDispItemCount(itemMap.get( entry.getKey() ));
				refillItemRptSummary.setNotYetDispDrugCost(statusMap.get(entry.getKey()));
				
				if (!orderMap.containsKey(entry.getKey())) {
					refillItemRptSummary.setNotYetDispRefillCount(0);
				}else{
					refillItemRptSummary.setNotYetDispRefillCount(orderMap.get(entry.getKey()).size());
				}
			}
			else if (entry.getKey() == RefillItemRptStatus.Suspended.getDisplayValue()) {
				refillItemRptSummary.setSuspendItemCount(itemMap.get( entry.getKey() ));
				refillItemRptSummary.setSuspendDrugCost(statusMap.get(entry.getKey()));
				
				if (!orderMap.containsKey(entry.getKey())) {
					refillItemRptSummary.setSuspendRefillCount(0);
				}else{
					refillItemRptSummary.setSuspendRefillCount(orderMap.get(entry.getKey()).size());
				}
			}
		}
		return refillItemRptSummary;
	}
	
	@SuppressWarnings("unchecked")
	public List<RefillWorkloadForecastRpt> retrieveRefillWorkloadRptList(String workstoreCode, Date dateFrom, Date dateTo) {
		refillWorkloadForecastRptList = new ArrayList<RefillWorkloadForecastRpt>();
		Workstore ws = em.find(Workstore.class, new WorkstorePK(workstore.getHospCode(), workstoreCode));
		
		List<RefillWorkloadForecastRpt> refillSecheduleReultList = em.createQuery(
				"select new " + REFILL_WORKLOAD_FORECAST_RPT_CLASS_NAME + // 20120306 index check : RefillSchedule.workstore,refillDate : I_REFILL_SCHEDULE_03
				"  (o.refillDate, count(o), 0L)" +
				" from RefillSchedule o" +
				" where o.workstore = :workstore" +
				" and o.refillDate between :dateFrom and :dateTo" +
				" and o.docType = :docType" +
				" and o.status = :status" +
				" group by o.refillDate" +
				" order by o.refillDate")
				.setParameter("workstore", ws)
				.setParameter("dateFrom", dateFrom, TemporalType.DATE)
				.setParameter("dateTo", dateTo, TemporalType.DATE)
				.setParameter("docType", DocType.Standard)
				.setParameter("status", RefillScheduleStatus.NotYetDispensed)
				.getResultList();
		
		List<RefillWorkloadForecastRpt> refillSecheduleItemResultList = em.createQuery(
				"select new " + REFILL_WORKLOAD_FORECAST_RPT_CLASS_NAME + // 20120306 index check : RefillSchedule.workstore,refillDate : I_REFILL_SCHEDULE_03
				"  (o.refillSchedule.refillDate, 0L, count(o))" +
				" from RefillScheduleItem o" +
				" where o.refillSchedule.workstore = :workstore" +
				" and o.refillSchedule.refillDate between :dateFrom and :dateTo" +
				" and o.refillSchedule.docType = :docType" +
				" and o.refillSchedule.status = :status" +
				" group by o.refillSchedule.refillDate" +
				" order by o.refillSchedule.refillDate")
				.setParameter("workstore", ws)
				.setParameter("dateFrom", dateFrom, TemporalType.DATE)
				.setParameter("dateTo", dateTo, TemporalType.DATE)
				.setParameter("docType", DocType.Standard)
				.setParameter("status", RefillScheduleStatus.NotYetDispensed)
				.getResultList();
		

		for (RefillWorkloadForecastRpt rs :refillSecheduleReultList) {	
			for (RefillWorkloadForecastRpt rsi :refillSecheduleItemResultList) {
				if (rs.getRefillDate().equals(rsi.getRefillDate())) {
					rs.setRefillItemCount(rsi.getRefillItemCount());
					refillWorkloadForecastRptList.add(rs);
					break;
				}
			}
		}
		
		if ( !refillWorkloadForecastRptList.isEmpty() ) {
			parameters.clear();
			parameters.put("workstoreCode", workstoreCode);
			parameters.put("startDate", dateFrom);
			parameters.put("endDate", dateTo);
			retrieveSuccess = true;
		}
		else{
			retrieveSuccess = false;
		}
		return refillWorkloadForecastRptList;
	}
	
	@SuppressWarnings("unchecked")
	public List<RefillQtyForecastRpt> retrieveRefillQtyForecastRptList(String workstoreCode, Date dateFrom, Date dateTo, String itemCode) {
		checkValidItemCode(itemCode);
		
		refillQtyForecastRptList = new ArrayList<RefillQtyForecastRpt>();
		
		if (validItemCode) {
			Workstore ws = em.find(Workstore.class, new WorkstorePK(workstore.getHospCode(), workstoreCode));
			
			List<RefillQtyForecastRpt> resultList = em.createQuery(
					"select new " + REFILL_QTY_FORECAST_RPT_CLASS_NAME + // 20120306 index check : RefillSchedule.workstore,refillDate : I_REFILL_SCHEDULE_03
					"  (o.pharmOrderItem.itemCode, '', sum(o.calQty), o.pharmOrderItem.baseUnit)" +
					" from RefillScheduleItem o" +
					" where o.refillSchedule.workstore = :workstore" +
					" and o.refillSchedule.refillDate between :dateFrom and :dateTo" +
					" and o.refillSchedule.docType = :docType" +
					" and o.refillSchedule.status = :status" +
					" and o.pharmOrderItem.itemCode = :itemCode" +
					" group by o.pharmOrderItem.itemCode, o.pharmOrderItem.baseUnit")
					.setParameter("workstore", ws)
					.setParameter("dateFrom", dateFrom, TemporalType.DATE)
					.setParameter("dateTo", dateTo, TemporalType.DATE)
					.setParameter("docType", DocType.Standard)
					.setParameter("status", RefillScheduleStatus.NotYetDispensed)
					.setParameter("itemCode", itemCode)
					.getResultList();
			
			if (!resultList.isEmpty()) {
				RefillQtyForecastRpt refillQtyForecastRpt = resultList.get(0);
				DmDrug dmdrug = dmDrugCacher.getDmDrug(refillQtyForecastRpt.getItemCode());
				refillQtyForecastRpt.setFullDrugDesc(dmdrug.getFullDrugDesc());
				refillQtyForecastRptList.add(refillQtyForecastRpt);
			}
			
			if ( !refillQtyForecastRptList.isEmpty() ) {
				parameters.clear();
				parameters.put("workstoreCode", workstoreCode);
				parameters.put("startDate", dateFrom);
				parameters.put("endDate", dateTo);
				retrieveSuccess = true;
			}
			else{
				retrieveSuccess = false;
			}
		}
		
		return refillQtyForecastRptList;
	}
	
	@SuppressWarnings("unchecked")
	public List<RefillStatRpt> retrieveRefillStatRptList(String workstoreCode, Date dateFrom, Date dateTo) {
		Workstore ws = em.find(Workstore.class, new WorkstorePK(workstore.getHospCode(), workstoreCode));
		
		refillStatRptList = new ArrayList<RefillStatRpt>();

		List<RefillScheduleItem> refillScheduleItemList = em.createQuery(
				"select o from RefillScheduleItem o " + // 20120306 index check : RefillSchedule.workstore,refillDate : I_REFILL_SCHEDULE_03
				" where o.refillSchedule.workstore = :workstore " +
				" and o.refillSchedule.refillDate between :dateFrom and :dateTo "+
				" and o.refillSchedule.docType = :docType " +
				" and o.refillSchedule.refillNum > 1 "+
				" and o.refillSchedule.status <> :status" +
				" order by o.refillSchedule.pharmOrder.specCode, o.refillSchedule.refillCouponNum")
		.setParameter("workstore", ws)
		.setParameter("dateFrom", dateFrom, TemporalType.DATE)
		.setParameter("dateTo", dateTo, TemporalType.DATE)
		.setParameter("docType", DocType.Standard)
		.setParameter("status", RefillScheduleStatus.SysDeleted)
		.getResultList();
		
		Map<String, RefillStatRpt> refillStatMap = new TreeMap<String, RefillStatRpt>();
		Map<String, RefillStatRpt> refillNumMap = new HashMap<String, RefillStatRpt>();
		
		for (RefillScheduleItem rsi : refillScheduleItemList) {
			RefillSchedule rs = rsi.getRefillSchedule();
			PharmOrder po = rs.getPharmOrder();

			if (!refillStatMap.containsKey(po.getSpecCode())) {
				RefillStatRpt refillStatRpt = new RefillStatRpt();
				refillStatRpt.setWorkstoreCode(workstoreCode);
				refillStatRpt.setSpecCode(po.getSpecCode());
				refillStatRpt.setOutstandRefillCount(0);
				refillStatRpt.setOutstandRefillDrugCost(0.0);
				refillStatRpt.setProcessedRefillCount(0);
				refillStatRpt.setProcessedRefillDrugCost(0.0);
				
				
				refillStatMap.put(po.getSpecCode(), refillStatRpt);
			}

			RefillStatRpt refillStatRpt = refillStatMap.get(po.getSpecCode());
			if (rs.getStatus() == RefillScheduleStatus.NotYetDispensed){
				if (!refillNumMap.containsKey(rs.getRefillCouponNum())){
					int count = refillStatRpt.getOutstandRefillCount();
					count++;
					refillStatRpt.setOutstandRefillCount(count);
					refillNumMap.put(rs.getRefillCouponNum().toString(), refillStatRpt);
				}
				
				double cost = refillStatRpt.getOutstandRefillDrugCost();
				if (rsi.getUnitPrice() == null) {
					rsi.setUnitPrice(BigDecimal.valueOf(dmDrugCacher.getDmDrug(rsi.getPharmOrderItem().getItemCode()).getDrugChargeInfo().getCorpDrugPrice()));
				}
				refillStatRpt.setOutstandRefillDrugCost( cost + (rsi.getRefillQty().doubleValue()) * (rsi.getUnitPrice().doubleValue()) );
			}else{
				if (!refillNumMap.containsKey(rs.getRefillCouponNum())){
					int count = refillStatRpt.getProcessedRefillCount();
					count++;
					refillStatRpt.setProcessedRefillCount(count);
					refillNumMap.put(rs.getRefillCouponNum().toString(), refillStatRpt);
				}
				
				double cost = refillStatRpt.getProcessedRefillDrugCost();
				if (rsi.getUnitPrice() == null) {
					rsi.setUnitPrice(BigDecimal.valueOf(dmDrugCacher.getDmDrug(rsi.getPharmOrderItem().getItemCode()).getDrugChargeInfo().getCorpDrugPrice()));
				}
				refillStatRpt.setProcessedRefillDrugCost( cost + (rsi.getRefillQty().doubleValue()) * (rsi.getUnitPrice().doubleValue()) );
			}
			refillStatMap.put(po.getSpecCode(), refillStatRpt);
			
		}
		
		refillStatRptList = new ArrayList<RefillStatRpt>(refillStatMap.values());
		
		if ( !refillScheduleItemList.isEmpty() ) {
			parameters.clear();
			parameters.put("workstoreCode", workstoreCode);
			parameters.put("monthEndReportDate", dateFrom);
			retrieveSuccess = true;
		}
		else{
			retrieveSuccess = false;
		}
		return refillStatRptList;
	}

	@SuppressWarnings("unchecked")
	private void updateRsiUnitPrice(Workstore ws, Date dateFrom, Date dateTo) {
		List<RefillScheduleItem> refillScheduleItemList = em.createQuery(
				"select o from RefillScheduleItem o" + // 20120306 index check : RefillScheduleItem.workstore,refillEndDate : I_REFILL_SCHEDULE_ITEM_01
				" where o.refillSchedule.workstore = :workstore" + 
				" and o.refillEndDate between :dateFrom and :dateTo" + 
				" and o.refillSchedule.status = :refillScheduleStatus")
				.setParameter("workstore", ws)
				.setParameter("dateFrom", dateFrom, TemporalType.DATE)
				.setParameter("dateTo", dateTo, TemporalType.DATE)
				.setParameter("refillScheduleStatus", RefillScheduleStatus.NotYetDispensed)
				.getResultList();

		for (RefillScheduleItem rsi : refillScheduleItemList) {
			if (dmDrugCacher.getDmDrug(rsi.getPharmOrderItem().getItemCode()).getDrugChargeInfo().getCorpDrugPrice()!=null) {
				rsi.setUnitPrice(BigDecimal.valueOf(dmDrugCacher.getDmDrug(rsi.getPharmOrderItem().getItemCode()).getDrugChargeInfo().getCorpDrugPrice()));
			}else {
				rsi.setUnitPrice(BigDecimal.ZERO);
			}
			
		}
	}	
	
	@SuppressWarnings("unchecked")
	public List<RefillExpiryRpt> retrieveRefillExpiryRpt(String workstoreCode, Date dateFrom, Date dateTo) {
		Workstore ws = em.find(Workstore.class, new WorkstorePK(workstore.getHospCode(), workstoreCode));

		updateRsiUnitPrice(ws, dateFrom, dateTo);
		
		refillExpiryRptList = new ArrayList<RefillExpiryRpt>();
		
		List<RefillExpiryRpt> resultList = em.createQuery( 
				"select new " + REFILL_EXPIRY_RPT_CLASS_NAME + // 20120306 index check : RefillScheduleItem.workstore,refillEndDate : I_REFILL_SCHEDULE_ITEM_01
				" (o.refillSchedule.workstore.workstoreCode, o.refillSchedule.pharmOrder.specCode, count(o), sum(o.unitPrice * o.refillQty))" +
				" from RefillScheduleItem o" + 
				" where o.refillSchedule.workstore = :workstore" + 
				" and o.refillEndDate between :dateFrom and :dateTo" + 
				" and o.refillSchedule.status = :refillScheduleStatus"+
				" group by o.refillSchedule.workstore.workstoreCode, o.refillSchedule.pharmOrder.specCode, o.refillSchedule.refillCouponNum"+ 
				" order by o.refillSchedule.pharmOrder.specCode")
				.setParameter("workstore", ws)
				.setParameter("dateFrom", dateFrom, TemporalType.DATE)
				.setParameter("dateTo", dateTo, TemporalType.DATE)
				.setParameter("refillScheduleStatus", RefillScheduleStatus.NotYetDispensed)
				.getResultList();
	
		if ( !resultList.isEmpty() ) {
			parameters.clear();
			parameters.put("workstoreCode", ws.getWorkstoreCode());
			parameters.put("monthEndReportDate", dateFrom);
			
			String preSpecCode = "";
			RefillExpiryRpt newRefillExpiryRpt = null;
			Long totalRefillCoupon = 0L;
			for ( RefillExpiryRpt refillExpiryRpt : resultList ) {
				if ( !preSpecCode.equals(refillExpiryRpt.getSpecCode()) ) {
					totalRefillCoupon = 1L;
					newRefillExpiryRpt = new RefillExpiryRpt(refillExpiryRpt.getWorkstoreCode(), 
																refillExpiryRpt.getSpecCode(), 
																totalRefillCoupon, 
																refillExpiryRpt.getTotalDrugCost());
					refillExpiryRptList.add(newRefillExpiryRpt);
					preSpecCode = refillExpiryRpt.getSpecCode();
				} else {
					totalRefillCoupon++;
					newRefillExpiryRpt.setTotalRefillCount(totalRefillCoupon);
					newRefillExpiryRpt.setTotalDrugCost(newRefillExpiryRpt.getTotalDrugCost().add(refillExpiryRpt.getTotalDrugCost()));
				}
			}
			retrieveSuccess = true;
		}
		else{
			retrieveSuccess = false;
		}
		return refillExpiryRptList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PharmClinicRefillCouponRpt> retrievePharmClinicRefillCouponRpt(String workstoreCode, Date dateFrom, Date dateTo) {
		pharmClinicRefillCouponRptList = new ArrayList<PharmClinicRefillCouponRpt>();
		Workstore ws = em.find(Workstore.class, new WorkstorePK(workstore.getHospCode(), workstoreCode));
		
		List<RefillSchedule> refillScheduleList = em.createQuery(
				"select o from RefillSchedule o" + // 20120306 index check : RefillSchedule.workstore,beforeAdjRefillDate : I_REFILL_SCHEDULE_04
				" where o.workstore = :workstore" +
				" and o.beforeAdjRefillDate between :dateFrom and :dateTo"+
				" and o.docType = :docType" +
				" and o.nextPharmApptDate is not null" +
				" and o.status <> :status" +
				" order by o.refillCouponNum, o.beforeAdjRefillDate")
				.setParameter("workstore", ws)
				.setParameter("dateFrom", dateFrom, TemporalType.DATE)
				.setParameter("dateTo", dateTo, TemporalType.DATE)
				.setParameter("docType", DocType.Standard)
				.setParameter("status", RefillScheduleStatus.SysDeleted)
				.getResultList();

		PharmClinicRefillCouponRpt pharmClinicRefillCouponRpt = new PharmClinicRefillCouponRpt();
		pharmClinicRefillCouponRptDtlList = new ArrayList<PharmClinicRefillCouponRptDtl>();
		
		for (RefillSchedule refillSchedule :refillScheduleList) {
			
			PharmClinicRefillCouponRptDtl pharmClinicRefillCouponRptDtl = new PharmClinicRefillCouponRptDtl();
			pharmClinicRefillCouponRptDtl.setRefillCouponNum(refillSchedule.getRefillCouponNum());
			pharmClinicRefillCouponRptDtl.setRefillDate(refillSchedule.getBeforeAdjRefillDate());
			pharmClinicRefillCouponRptDtl.setPatName(refillSchedule.getPharmOrder().getPatient().getName());
			
			if(refillSchedule.getPharmOrder().getPatient().getNameChi()!=null) {
				pharmClinicRefillCouponRptDtl.setPatNameChi(refillSchedule.getPharmOrder().getPatient().getNameChi());
			} 
			
			pharmClinicRefillCouponRptDtl.setHkid(refillSchedule.getPharmOrder().getPatient().getHkid());
			
			if( refillSchedule.getPharmOrder().getMedOrder().getDocType() == MedOrderDocType.Normal ){
				pharmClinicRefillCouponRptDtl.setOrderNum(refillSchedule.getPharmOrder().getMedOrder().getOrderNum());
			}
			
			if (refillSchedule.getPharmOrder().getMedCase()!=null) {
				pharmClinicRefillCouponRptDtl.setCaseNum(refillSchedule.getPharmOrder().getMedCase().getCaseNum());
				pharmClinicRefillCouponRptDtl.setPasSpecCode(refillSchedule.getPharmOrder().getMedCase().getPasSpecCode());
				pharmClinicRefillCouponRptDtl.setPasSpecCode(refillSchedule.getPharmOrder().getMedCase().getPasSubSpecCode());
			}

			if( refillSchedule.getDispOrder() != null ){
				pharmClinicRefillCouponRptDtl.setDoWorkstoreCode(refillSchedule.getDispOrder().getWorkstore().getWorkstoreCode());
			}
			
			if( refillSchedule.getDispOrder()!=null && refillSchedule.getDispOrder().getAdminStatus() == DispOrderAdminStatus.Suspended ){
				pharmClinicRefillCouponRptDtl.setStatus(DispOrderAdminStatus.Suspended.getDisplayValue());
			}else{
				pharmClinicRefillCouponRptDtl.setStatus(refillSchedule.getStatus().getDisplayValue());
			}
		
			pharmClinicRefillCouponRptDtl.setRefillNum(refillSchedule.getRefillNum().toString());
			pharmClinicRefillCouponRptDtl.setNumOfRefill(refillSchedule.getNumOfRefill().toString());
			pharmClinicRefillCouponRptDtl.setDispDate(refillSchedule.getRefillDate());
			pharmClinicRefillCouponRptDtl.setMoOrderDate(refillSchedule.getPharmOrder().getMedOrder().getOrderDate());
			pharmClinicRefillCouponRptDtlList.add(pharmClinicRefillCouponRptDtl);
		}
		pharmClinicRefillCouponRpt.setPharmClinicRefillCouponRptDtlList(pharmClinicRefillCouponRptDtlList);
		OperationProfile op = em.find(OperationProfile.class, workstore.getOperationProfileId());
		
		PharmClinicRefillCouponRptHdr pharmClinicRefillCouponRptHdr = new PharmClinicRefillCouponRptHdr();
		pharmClinicRefillCouponRptHdr.setHospCode(workstore.getHospCode());
		pharmClinicRefillCouponRptHdr.setWorkstoreCode(workstoreCode);
		pharmClinicRefillCouponRptHdr.setStartDate(dateFrom);
		pharmClinicRefillCouponRptHdr.setEndDate(dateTo);
		pharmClinicRefillCouponRptHdr.setActiveProfileName(op.getName());
		
		pharmClinicRefillCouponRpt.setPharmClinicRefillCouponRptHdr(pharmClinicRefillCouponRptHdr);
		pharmClinicRefillCouponRptList.add(pharmClinicRefillCouponRpt);

		if ( !refillScheduleList.isEmpty() ) {
			parameters.clear();
			retrieveSuccess = true;
		}
		else{
			retrieveSuccess = false;
		}
		
		return pharmClinicRefillCouponRptList;
	}
	
	public void generateRefillRpt() {
		OperationProfile activeProfile = em.find(OperationProfile.class, workstore.getOperationProfileId());
		
		parameters.put("hospCode", workstore.getHospCode());
		parameters.put("operationMode", activeProfile.getName());	
		
		printAgent.renderAndRedirect(new RenderJob(
				currRefillReportType.getDataValue(), 
				new PrintOption(), 
				parameters, 
				reportObj));

	}
	
	public void printRefillRpt(){

		printAgent.renderAndPrint(new RenderAndPrintJob(
				currRefillReportType.getDataValue(), 
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				parameters, 
				reportObj));
		
	}
	
	public void setRefillRptPassword(String password){

		exportPassword = password;
		
	}
	
	public void exportRefillRpt() throws IOException, ZipException {	

		PrintOption po = new PrintOption();
		po.setDocType(ReportProvider.XLS);
		
		printAgent.zipAndRedirect(new RenderJob(
				xlsName, 
				po, 
				parameters, 
				exportObj), exportPassword);

	}

	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}
	
	public boolean isValidItemCode() {
		return validItemCode;
	}
	
	@Remove
	public void destroy() {

		if(refillItemRptDtlList !=null) {
			refillItemRptDtlList = null;
		}
		
		if(refillWorkloadForecastRptList!=null){
			refillWorkloadForecastRptList=null;
		}
		
		if(refillQtyForecastRptList!=null){
			refillQtyForecastRptList=null;
		}
		
		if(refillStatRptList!=null){
			refillStatRptList=null;
		}
		
		if(refillExpiryRptList!=null){
			refillExpiryRptList=null;
		}
		
		if(pharmClinicRefillCouponRptList!=null){
			pharmClinicRefillCouponRptList=null;
		}
		
	}

}
