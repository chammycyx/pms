package hk.org.ha.model.pms.biz.onestop;

import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.charging.InvoiceManagerLocal;
import hk.org.ha.model.pms.biz.reftable.RefillConfigServiceLocal;
import hk.org.ha.model.pms.biz.vetting.CapdVoucherManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.CapdVoucher;
import hk.org.ha.model.pms.persistence.disp.CapdVoucherItem;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.persistence.disp.RefillScheduleItem;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.InvoiceDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderPatType;
import hk.org.ha.model.pms.udt.disp.PharmOrderStatus;
import hk.org.ha.model.pms.udt.refill.DocType;
import hk.org.ha.model.pms.udt.refill.RefillScheduleRefillStatus;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.udt.vetting.RegimenDurationUnit;
import hk.org.ha.model.pms.vo.onestop.ReprintOrderSummary;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;
import org.joda.time.DateTime;

@Stateful
@Scope(ScopeType.SESSION)
@Name("reprintService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ReprintServiceBean implements ReprintServiceLocal {
	
	@PersistenceContext
	private EntityManager em;
	
	@Logger
	private Log logger;
	
	@In
	private Workstore workstore;
	
	@In
	private UamInfo uamInfo;
	
	@In
	private Workstation workstation;
	
	@In
	private CapdVoucherManagerLocal capdVoucherManager;
	
	@In
	private InvoiceManagerLocal invoiceManager;
	
	@In(required = false)
	@Out(required = false)
	private ReprintOrderSummary reprintOrderSummary;
			
	private final static String UNCHECKED = "unchecked";
	private final static String WORKSTORE = "workstore";
	private final static String START_DATE  = "startDate";
	private final static String END_DATE = "endDate";
	private final static String TICKET_NUM = "ticketNum";
	private final static String DISP_ORDER_STATUS = "dispOrderStatus";
	private final static String PHARM_ORDER_STATUS = "pharmOrderStatus";
	private final static String STATUS = "status";
	private final static String PHARM_ORDER = "pharmOrder";
	private final static String ORDER_TYPE = "orderType";

	@In
	private RefillConfigServiceLocal refillConfigService;
	
	public void clear()
	{
		reprintOrderSummary = null;
	}
	
	public void retrieveRefillScheduleListForReprint(Long id)
	{
		reprintOrderSummary = new ReprintOrderSummary();
		reprintOrderSummary.setTriggerByOtherScreen(false);		
		refillConfigService.retrieveRefillConfig();
		List<RefillSchedule> reprintRefillScheduleList = new ArrayList<RefillSchedule>();
		
		RefillSchedule currRefillSchedule = em.find(RefillSchedule.class, id);
		
		List<RefillSchedule> refillScheduleList = retrieveReillScheduleList(currRefillSchedule.getPharmOrder());
		
		for (RefillSchedule refillSchedule : refillScheduleList)
		{
			refillSchedule.loadChild();
			reprintRefillScheduleList.add(refillSchedule);
			
			if (reprintOrderSummary.getReprintPatient() == null)
			{
				reprintOrderSummary.setReprintMedOrder(refillSchedule.getPharmOrder().getMedOrder());
				reprintOrderSummary.setReprintMedCase(refillSchedule.getPharmOrder().getMedCase());
				reprintOrderSummary.setReprintPatient(refillSchedule.getPharmOrder().getPatient());
				reprintOrderSummary.setDoctorCode(refillSchedule.getPharmOrder().getDoctorCode());
				reprintOrderSummary.setDoctorName(refillSchedule.getPharmOrder().getDoctorName());
			}		
			
			reprintOrderSummary.setReprintRefillScheduleList(reprintRefillScheduleList);
		}
	}
	
	@SuppressWarnings(UNCHECKED)
	public void retrieveDispOrderByTicketNum(Date ticketDate, String ticketNum, Boolean triggerByOtherScreen)
	{
		if (triggerByOtherScreen)
		{
			logger.info("Call to show reprint screen (with data) by user #0(#1) with ticket date #2 and ticket num #3", 
					uamInfo.getUserId(), workstation.getHostName(), ticketDate, ticketNum);
		}
		
		reprintOrderSummary = new ReprintOrderSummary();
		reprintOrderSummary.setTriggerByOtherScreen(triggerByOtherScreen);		
		refillConfigService.retrieveRefillConfig();
		
		List<DispOrder> dispOrderList = em.createQuery(
				"select o from DispOrder o " + // 20120227 index check : Ticket.workstore,orderType,ticketDate,ticketNum : UI_TICKET_01
				" where o.ticket.workstore = :workstore" +
				" and o.ticket.orderType = :orderType" +
				" and o.ticket.ticketDate >= :startDate" +
				" and o.ticket.ticketDate < :endDate" +
				" and o.ticket.ticketNum = :ticketNum" +
				" and o.status not in :dispOrderStatus" +
				" and o.pharmOrder.status not in :pharmOrderStatus" +
				" and o.pharmOrder.medOrder.status <> :status" +
				" order by o.createDate, o.id")
				.setParameter(WORKSTORE, workstore)
				.setParameter(ORDER_TYPE, OrderType.OutPatient)
				.setParameter(START_DATE, new DateTime(ticketDate).toDate(), TemporalType.DATE)
				.setParameter(END_DATE, new DateTime(ticketDate).plusDays(1).toDate(), TemporalType.DATE)
				.setParameter(TICKET_NUM, ticketNum)
				.setParameter(DISP_ORDER_STATUS, DispOrderStatus.Deleted_SysDeleted)
				.setParameter(PHARM_ORDER_STATUS, PharmOrderStatus.Deleted_SysDeleted)
				.setParameter(STATUS, MedOrderStatus.Deleted)
				.getResultList();
		
		if (!dispOrderList.isEmpty()) {
			MedOrder medOrder = dispOrderList.get(0).getPharmOrder().getMedOrder();
			if (medOrder.getPrescType() != MedOrderPrescType.Dh || (medOrder.getPrescType() == MedOrderPrescType.Dh && medOrder.getDhLatestFlag()))
			{
				convertReprintInfo(dispOrderList.get(0));
			}
			else
			{
				reprintOrderSummary.setIsDhOutdatedOrder(true);
				if (triggerByOtherScreen)
				{
					addTicketInfoTriggerByOtherScreen(ticketDate, ticketNum);
				}				
			}
		}
		else if (triggerByOtherScreen)
		{
			addTicketInfoTriggerByOtherScreen(ticketDate, ticketNum);
		}
	}
	
	private void addTicketInfoTriggerByOtherScreen(Date ticketDate, String ticketNum)
	{
		Ticket ticket = new Ticket();
		ticket.setTicketDate(ticketDate);
		ticket.setTicketNum(ticketNum);
		reprintOrderSummary.setReprintTicket(ticket);
	}
	
	@SuppressWarnings(UNCHECKED)
	private void convertReprintInfo(DispOrder dispOrder) {
		
		Map<String, Long> removePharmOrderItemId = new HashMap<String, Long>();	
		dispOrder.loadChild();
		dispOrder.loadInvoice();
		
		reprintOrderSummary.setReprintMedOrder(dispOrder.getPharmOrder().getMedOrder());
		reprintOrderSummary.setReprintMedCase(dispOrder.getPharmOrder().getMedCase());
		reprintOrderSummary.setReprintPatient(dispOrder.getPharmOrder().getPatient());
		reprintOrderSummary.setReprintTicket(dispOrder.getTicket());
		reprintOrderSummary.setDoctorCode(dispOrder.getPharmOrder().getDoctorCode());
		reprintOrderSummary.setDoctorName(dispOrder.getPharmOrder().getDoctorName());
				
		List<CapdVoucher> reprintCapdVoucherList = capdVoucherManager.retrieveCapdVoucherList(dispOrder);
		
		if (!reprintCapdVoucherList.isEmpty())
		{
			
			for (CapdVoucher capdVoucher : reprintCapdVoucherList) 
			{
				capdVoucher.loadChild();
				
				capdVoucher.setAdminStatus(dispOrder.getAdminStatus());
				
				for (CapdVoucherItem capdVoucherItem : capdVoucher.getCapdVoucherItemList()) 
				{
					capdVoucherItem.getDrugDesc();
					removePharmOrderItemId.put(capdVoucherItem.getDispOrderItem().getPharmOrderItem().getId().toString(), 
											   capdVoucherItem.getDispOrderItem().getPharmOrderItem().getId());
				}
				
				Collections.sort(capdVoucher.getCapdVoucherItemList(), new ComparatorForCapdVoucherItemList());
			}
		}
		
		reprintOrderSummary.setReprintCapdVoucherList(reprintCapdVoucherList);
				
		if (dispOrder.getSfiFlag())
		{
			for (Invoice invoice : dispOrder.getInvoiceList())
			{
				if (invoice.getDocType() == InvoiceDocType.Sfi && 
					!PharmOrderPatType.Doh.equals(invoice.getDispOrder().getPharmOrder().getPatType()) &&
					invoiceManager.isExceptionDrugExist(invoice))
				{
					reprintOrderSummary.setSfiInvoice(invoice);
					break;
				}
			}
		}
		
		if (!dispOrder.getRefillFlag())
		{
			List<RefillSchedule> reprintRefillScheduleList = new ArrayList<RefillSchedule>();
			List<RefillSchedule> reprintSfiRefillScheduleItemList = new ArrayList<RefillSchedule>();
			
			List<RefillSchedule> refillScheduleList = retrieveReillScheduleList(dispOrder.getPharmOrder());
			
			for (RefillSchedule refillSchedule : refillScheduleList)
			{
				if (refillSchedule.getDocType() == DocType.Standard)
				{
					refillSchedule.loadChild();
					reprintRefillScheduleList.add(refillSchedule);
					reprintOrderSummary.setReprintRefillScheduleList(reprintRefillScheduleList);
				}
				else if (refillSchedule.getDocType() == DocType.Sfi && refillSchedule.getRefillStatus() == RefillScheduleRefillStatus.Current &&
						refillSchedule.getDispOrder().getStatus() != DispOrderStatus.SysDeleted)
				{
					for (RefillScheduleItem refillScheduleItem : refillSchedule.getRefillScheduleItemList())
					{
						if (refillScheduleItem.getCalQty() - refillScheduleItem.getRefillQty() > 0)
						{
							refillSchedule.loadChild();
							reprintSfiRefillScheduleItemList.add(refillSchedule);
							reprintOrderSummary.setReprintSfiRefillScheduleItemList(reprintSfiRefillScheduleItemList);
							break;
						}
					}	
				}
			}
		}
		else
		{			
			List <RefillSchedule> refillScheduleList = em.createQuery(
					"select o from RefillSchedule o" + // 20120227 index check : RefillSchedule.dispOrder : FK_REFILL_SCHEDULE_02
					" where o.dispOrder = :dispOrder")
					.setParameter("dispOrder", dispOrder)
					.getResultList();
			
			for (RefillSchedule refillSchedule : refillScheduleList) {
				convertReprintRefillInfo(refillSchedule);
			}
		}
		
		em.clear();
		DispOrder dispOrderRef = em.find(DispOrder.class, dispOrder.getId());
		dispOrderRef.loadChild();
		dispOrderRef.getTicket();
		List <RefillSchedule> refillScheduleList = em.createQuery(
				"select o from RefillSchedule o" + // 20120831 index check : RefillSchedule.dispOrder : FK_REFILL_SCHEDULE_02
				" where o.dispOrder = :dispOrder" +
				" and o.docType = :docType")
				.setParameter("dispOrder", dispOrderRef)
				.setParameter("docType", DocType.Standard)
				.getResultList();
		
		for (RefillSchedule refillSchedule : refillScheduleList) {
			refillSchedule.loadChild();
		}
				
		List<DispOrderItem> dispOrderItemList = dispOrderRef.getDispOrderItemList();		
		List<DispOrderItem> reprintDispOrderItemList = new ArrayList<DispOrderItem>();
		em.clear();

		for (DispOrderItem dispOrderItem : dispOrderItemList) 
		{
			dispOrderItem.getPharmOrderItem().getMedOrderItem().loadDmInfo();
			dispOrderItem.getPharmOrderItem().loadDmInfo();
			if (!dispOrderItem.getPharmOrderItem().getCapdVoucherFlag() && dispOrderItem.getStatus() != DispOrderItemStatus.KeepRecord && 
				(dispOrderItem.getPharmOrderItem().getActionStatus() != ActionStatus.PurchaseByPatient || 
				dispOrderItem.getDispQty().compareTo(BigDecimal.ZERO) > 0))
			{
				if (!dispOrderItem.getPharmOrderItem().getRefillScheduleItemList().isEmpty()) {
					dispOrderItem.getPharmOrderItem().getRegimen().getDoseGroupList().get(0).setDuration(dispOrderItem.getDurationInDay());
					dispOrderItem.getPharmOrderItem().getRegimen().getDoseGroupList().get(0).setDurationUnit(RegimenDurationUnit.Day);
				}
						
				dispOrderItem.getPharmOrderItem().setIssueQty(dispOrderItem.getDispQty());
				reprintDispOrderItemList.add(dispOrderItem);
			}
		}

		reprintOrderSummary.setReprintDispOrderItemList(reprintDispOrderItemList);
		
		reprintOrderSummary.setIsNoLabelItem((reprintOrderSummary.getReprintCapdVoucherList() == null || reprintOrderSummary.getReprintCapdVoucherList().isEmpty()) && 
				(reprintOrderSummary.getReprintDispOrderItemList() == null || reprintOrderSummary.getReprintDispOrderItemList().isEmpty()) && 
				(reprintOrderSummary.getReprintRefillScheduleList() == null || reprintOrderSummary.getReprintRefillScheduleList().isEmpty()) && 
				(reprintOrderSummary.getReprintSfiRefillScheduleItemList() == null || reprintOrderSummary.getReprintSfiRefillScheduleItemList().isEmpty()));
	}
	
	@SuppressWarnings(UNCHECKED)
	private void convertReprintRefillInfo(RefillSchedule refillScheduleItem) 
	{
		RefillSchedule orignalRefillSchedule = refillScheduleItem;		
		reprintOrderSummary.setOrignalRefillSchedule(refillScheduleItem);
		
		List<RefillSchedule> refillScheduleList = em.createQuery(
				"select o from RefillSchedule o, RefillSchedule i" + // 20120227 index check : RefillSchedule.id : PK_REFILL_SCHEDULE
				" where  i.id = :id" +
				" and o.pharmOrder = i.pharmOrder" + 
				" and o.pharmOrder.status not in :pharmOrderStatus" +
				" and o.pharmOrder.medOrder.status <> :status" +
				" order by o.groupNum, o.refillNum")
				.setParameter("id", orignalRefillSchedule.getId())
				.setParameter("pharmOrderStatus", PharmOrderStatus.Deleted_SysDeleted)
				.setParameter("status", MedOrderStatus.Deleted)
				.getResultList();

		if (reprintOrderSummary.getReprintMedOrder() == null)
		{
			reprintOrderSummary.setReprintMedOrder(refillScheduleList.get(0).getPharmOrder().getMedOrder());
			reprintOrderSummary.setReprintMedCase(refillScheduleList.get(0).getPharmOrder().getMedCase());
			reprintOrderSummary.setReprintPatient(refillScheduleList.get(0).getPharmOrder().getPatient());
			reprintOrderSummary.setDoctorCode(refillScheduleList.get(0).getPharmOrder().getDoctorCode());
			reprintOrderSummary.setDoctorName(refillScheduleList.get(0).getPharmOrder().getDoctorName());
		}
		
		if (orignalRefillSchedule.getDocType() == DocType.Standard) 
		{
			convertStdRefillReprint(refillScheduleList);
		}
		else
		{
			if (reprintOrderSummary.getSfiInvoice() == null)
			{
				for (Invoice invoice : orignalRefillSchedule.getDispOrder().getInvoiceList())
				{
					if (invoice.getDocType() == InvoiceDocType.Sfi && 
						!PharmOrderPatType.Doh.equals(invoice.getDispOrder().getPharmOrder().getPatType()) &&
						invoice.getTotalAmount().compareTo(BigDecimal.ZERO) > 0 &&
						invoiceManager.isExceptionDrugExistExcludeZeroAmount(invoice))
					{
						reprintOrderSummary.setSfiInvoice(invoice);
						break;
					}
				}
			}
			
			convertSfiRefillReprint(refillScheduleList, orignalRefillSchedule);
		}
	}
	
	private void convertStdRefillReprint(List<RefillSchedule> refillScheduleList) {
		List<RefillSchedule> reprintRefillScheduleList = new ArrayList<RefillSchedule>();
		for (RefillSchedule refillSchedule : refillScheduleList)
		{
			if (refillSchedule.getDocType() == DocType.Standard)
			{
				refillSchedule.loadChild();
				reprintRefillScheduleList.add(refillSchedule);
				reprintOrderSummary.setReprintRefillScheduleList(reprintRefillScheduleList);
				
				if (reprintOrderSummary.getReprintTicket() == null)
				{
					reprintOrderSummary.setReprintTicket(new Ticket());
				}
			}
		}
	}
	
	private void convertSfiRefillReprint(List<RefillSchedule> refillScheduleList, RefillSchedule orignalRefillSchedule) {
		List<RefillSchedule> reprintSfiRefillScheduleItemList = new ArrayList<RefillSchedule>();
		for (RefillSchedule refillSchedule : refillScheduleList)
		{			
			if (refillSchedule.getDocType() == DocType.Sfi && refillSchedule.getRefillStatus() == RefillScheduleRefillStatus.Current &&
				refillSchedule.getDispOrder().getStatus() != DispOrderStatus.SysDeleted)
			{	
				for (RefillScheduleItem refillScheduleItem : refillSchedule.getRefillScheduleItemList())
				{
					if (refillScheduleItem.getCalQty() - refillScheduleItem.getRefillQty() > 0)
					{
						if (refillSchedule.getRefillCouponNum().substring(0, 17).equals(orignalRefillSchedule.getRefillCouponNum().substring(0, 17))) {
							refillSchedule.loadChild();
							reprintSfiRefillScheduleItemList.add(refillSchedule);
							reprintOrderSummary.addReprintSfiRefillScheduleList(reprintSfiRefillScheduleItemList);
							
							if (reprintOrderSummary.getReprintTicket() == null)
							{
								reprintOrderSummary.setReprintTicket(new Ticket());
							}
						}
						
						break;
					}		
				}
			}
		}
	}
	
	private static class ComparatorForCapdVoucherItemList implements Comparator<CapdVoucherItem>, Serializable {
		
		private static final long serialVersionUID = 1L;

		public int compare(CapdVoucherItem c1, CapdVoucherItem c2) {
						
			return c1.getDispOrderItem().getPharmOrderItem().getItemNum().compareTo(c2.getDispOrderItem().getPharmOrderItem().getItemNum());
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<RefillSchedule> retrieveReillScheduleList(PharmOrder pharmOrder)
	{
		return em.createQuery(
				"select o from RefillSchedule o" + // 20120227 index check : RefillSchedule.pharmOrder : FK_REFILL_SCHEDULE_01
				" where o.pharmOrder = :pharmOrder" +
				" and o.pharmOrder.status not in :pharmOrderStatus" +
				" and o.pharmOrder.medOrder.status <> :status" +
				" order by o.groupNum, o.refillNum")
				.setParameter(PHARM_ORDER, pharmOrder)
				.setParameter(PHARM_ORDER_STATUS , PharmOrderStatus.Deleted_SysDeleted)
				.setParameter(STATUS, MedOrderStatus.Deleted)
				.getResultList();
	}
	
	@Remove
	public void destroy()
	{		
		if (reprintOrderSummary != null) {
			reprintOrderSummary = null;
		}
	}
}
