package hk.org.ha.model.pms.biz.onestop;

import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.vo.onestop.ReprintDispOrderItemInfo;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface ReprintCouponLabelServiceBeanLocal {
		
	String reprintLabelAndRefillCoupon(List<ReprintDispOrderItemInfo> reprintDispOrderItemList, List<Long> stdRefillScheduleItemList, 
			List<Long> reprintCapdVoucherList, Boolean printSfiInvoice, Boolean printSfiCoupon, PrintLang printLang, 
			Boolean printMedSummaryReport, Date ticketDate, String ticketNum);
	
	void destroy();
		
}
