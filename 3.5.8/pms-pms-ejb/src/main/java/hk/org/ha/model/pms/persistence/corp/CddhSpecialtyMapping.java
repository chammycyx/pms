package hk.org.ha.model.pms.persistence.corp;


import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.RecordStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "CDDH_SPECIALTY_MAPPING")
@IdClass(CddhSpecialtyMappingPK.class)
public class CddhSpecialtyMapping extends VersionEntity {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "SPEC_CODE", nullable = false, length = 4)
	private String specCode;
	
	@Id
	@Column(name = "HOSP_CODE", nullable = false, length = 3)
	private String hospCode;

	@Column(name = "CDDH_SPEC_CODE", nullable = false, length = 4)
	private String cddhSpecCode;

	@Converter(name = "CddhSpecialtyMapping.status", converterClass = RecordStatus.Converter.class)
    @Convert("CddhSpecialtyMapping.status")
	@Column(name = "STATUS", nullable = false, length = 1)
	private RecordStatus status;
    
	@ManyToOne
    @JoinColumn(name = "HOSP_CODE", nullable = false, insertable = false, updatable = false)
	private Hospital hospital;
	
	public Hospital getHospital() {
		return hospital;
	}

	public void setHospital(Hospital hospital) {
		this.hospital = hospital;
	}

    public String getCddhSpecCode() {
		return cddhSpecCode;
	}

	public void setCddhSpecCode(String cddhSpecCode) {
		this.cddhSpecCode = cddhSpecCode;
	}
	
	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public CddhSpecialtyMapping() {
		status = RecordStatus.Active;
	}
	
	public RecordStatus getStatus() {
		return status;
	}

	public void setStatus(RecordStatus status) {
		this.status = status;
	}
}
