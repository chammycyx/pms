package hk.org.ha.model.pms.biz.report;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface DelItemRptServiceLocal {

	void retrieveDelItemRpt(Integer selectedTab, String workstoreCode, Date date);
	
	void generateDelItemRpt();
	
	void printDelItemRpt();
	
	List<?> getReportObj();
	
	void destroy();
	
}
