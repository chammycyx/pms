package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.cache.WardStockCacher;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
import hk.org.ha.model.pms.persistence.phs.WardStock;
import hk.org.ha.model.pms.persistence.reftable.WardGroupItem;
import hk.org.ha.model.pms.udt.RecordStatus;
import hk.org.ha.model.pms.vo.medprofile.Ward;
import hk.org.ha.model.pms.vo.medprofile.WardGroup;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.springframework.util.CollectionUtils;

@AutoCreate
@Stateless
@Name("mpWardManager")
public class MpWardManagerBean implements MpWardManagerLocal {

	private static final String WARD_CLASS_NAME = Ward.class.getName();
	
	private static final String WARD_GROUP_QUERY_HINT_FETCH = "o.wardGroupItemList";
	
	private DateFormat dayFormat = new SimpleDateFormat("EEE", Locale.ENGLISH); 
	private DateFormat timeFormat = new SimpleDateFormat("HHmm"); 	
	
	@PersistenceContext
	private EntityManager em;
	
    public MpWardManagerBean() {
    }

	@Override
	@SuppressWarnings("unchecked")
	public List<WardGroup> retrieveWardGroupList(WorkstoreGroup workstoreGroup)
	{
		List<hk.org.ha.model.pms.persistence.reftable.WardGroup> wardGroupList = em.createQuery(
				"select o from WardGroup o" + // 20120307 index check : WardGroup.workstoreGroup : FK_WARD_GROUP_01
				" where o.workstoreGroup = :workstoreGroup" +
				" order by o.wardGroupCode")
				.setParameter("workstoreGroup", workstoreGroup)
				.setHint(QueryHints.FETCH, WARD_GROUP_QUERY_HINT_FETCH)
				.getResultList();

		List<WardGroup> resultList = new ArrayList<WardGroup>();
		
		for (hk.org.ha.model.pms.persistence.reftable.WardGroup wardGroup: wardGroupList) {
			List<String> wardCodeList = new ArrayList<String>();
			for (WardGroupItem wardGroupItem: wardGroup.getWardGroupItemList()) {
				wardCodeList.add(wardGroupItem.getWardCode());
			}
			resultList.add(new WardGroup(wardGroup.getWardGroupCode(), wardCodeList));
		}
		
		return resultList;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Ward> retrieveWardList(WorkstoreGroup workstoreGroup)
	{
		List<Ward> wardList = em.createQuery(
				"select new " + WARD_CLASS_NAME + " (o.wardCode, o.status) from Ward o" + // 20120307 index check : Ward.institution : FK_WARD_01
				" where o.institution = :institution" +
				" order by o.wardCode")
				.setParameter("institution", workstoreGroup.getInstitution())
				.getResultList();
		
		Set<String> mpWardCodeSet = new HashSet<String>();
		mpWardCodeSet.addAll(em.createQuery(
				"select wc.wardCode from WardConfig wc" + // 20130829 index check : WardConfig.workstoreGroup : FK_WARD_CONFIG_01
				" where wc.workstoreGroup = :workstoreGroup" +
				" and wc.mpWardFlag = true")
				.setParameter("workstoreGroup", workstoreGroup)
				.getResultList());

		for (Ward ward: wardList) {
			ward.setMpWardFlag(mpWardCodeSet.contains(ward.getWardCode()));
		}
		return wardList;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Ward> retrieveActiveWardList(WorkstoreGroup workstoreGroup)
	{
		List<Ward> wardList = em.createQuery(
				"select new " + WARD_CLASS_NAME + " (o.wardCode, o.status) from Ward o" + // 20120307 index check : Ward.institution : FK_WARD_01
				" where o.institution = :institution" +
				" and o.status = :status" +
				" order by o.wardCode")
				.setParameter("institution", workstoreGroup.getInstitution())
				.setParameter("status", RecordStatus.Active)
				.getResultList();
		
		Set<String> mpWardCodeSet = new HashSet<String>();
		mpWardCodeSet.addAll(em.createQuery(
				"select wc.wardCode from WardConfig wc" + // 20130829 index check : WardConfig.workstoreGroup : FK_WARD_CONFIG_01
				" where wc.workstoreGroup = :workstoreGroup" +
				" and wc.mpWardFlag = true")
				.setParameter("workstoreGroup", workstoreGroup)
				.getResultList());

		for (Ward ward: wardList) {
			ward.setMpWardFlag(mpWardCodeSet.contains(ward.getWardCode()));
		}
		return wardList;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<String> retrieveWardCodeList(WorkstoreGroup workstoreGroup)
	{
		return em.createQuery(
				"select o.wardCode from Ward o" + // 20120307 index check : Ward.institution : FK_WARD_01
				" where o.institution = :institution" +
				" order by o.wardCode")
				.setParameter("institution", workstoreGroup.getInstitution())
				.getResultList();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<String> retrieveWardCodeList(List<WardGroup> wardGroupList, WorkstoreGroup workstoreGroup)
	{
		if (wardGroupList == null || wardGroupList.size() <= 0) {
			return new ArrayList<String>();
		}
		
		List<String> wardGroupCodeList = new ArrayList<String>();
		for (WardGroup wardGroup: wardGroupList)
			wardGroupCodeList.add(wardGroup.getWardGroupCode());
		
		return em.createQuery(
				"select distinct o.wardCode from WardGroupItem o" + // 20120307 index check : WardGroup.workstoreGroup,wardGroupCode : UI_WARD_GROUP_01
				" where o.wardGroup.workstoreGroup = :workstoreGroup" +
				" and o.wardGroup.wardGroupCode in :wardGroupCodeList" +
				" and o.wardCode in " +
				"  (select w.wardCode from Ward w, WardConfig wc" +
				"   where w.institution = :institution and wc.workstoreGroup = :workstoreGroup" +
		    	"   and w.wardCode = wc.wardCode and wc.mpWardFlag = true)" +
				" order by o.wardCode")
				.setParameter("workstoreGroup", workstoreGroup)
				.setParameter("institution", workstoreGroup.getInstitution())
				.setParameter("wardGroupCodeList", wardGroupCodeList)
				.getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<String> retrieveActiveWardCodeList(Workstore workstore)
	{
		Date now = new Date();
		String day = dayFormat.format(now).toLowerCase();
		String time = timeFormat.format(now);
		
		return em.createQuery(
				"select distinct o.wardCode from Ward o" + // 20140128 index check : WardFilter.workstore : FK_WARD_FILTER_01
				" where o.institution = :institution" +
				" and (o.wardCode in " +
				"   (select distinct f1.wardCode from WardFilter f1" +
				"     where f1.workstore = :workstore" +
				"     and f1." + day + "Flag = true" +
				"     and f1.startTime <= :time" +
				"     and f1.endTime >= :time)" +
				" or (not exists" +
				"   (select distinct f2.wardCode from WardFilter f2" +
				"     where f2.workstore = :workstore" +
				"     and f2." + day + "Flag = true" +
				"     and f2.startTime <= :time" +
				"     and f2.endTime >= :time)" +
				"   and o.wardCode not in " +
				"   (select distinct f3.wardCode from WardFilter f3" +
				"     where f3.workstore.hospital = :hospital" +
				"     and f3.workstore <> :workstore" +
				"     and f3." + day + "Flag = true" +
				"     and f3.startTime <= :time" +
				"     and f3.endTime >= :time)" +
				" )) order by o.wardCode")
				.setParameter("institution", workstore.getWorkstoreGroup().getInstitution())
				.setParameter("hospital", workstore.getHospital())
				.setParameter("workstore", workstore)
				.setParameter("time", time)
				.getResultList();
	}
	
	@Override
	public boolean isActiveWard(Workstore workstore, String wardCode) {
		return wardCode != null && retrieveActiveWardCodeList(workstore).contains(wardCode);
	}
	
	@Override
	public Map<String, WardStock> retrieveWardStockMap(String instCode, String wardCode) {

		hk.org.ha.model.pms.persistence.phs.Ward ward = new hk.org.ha.model.pms.persistence.phs.Ward();
		ward.setInstCode(instCode);
		ward.setWardCode(wardCode);
		
		List<WardStock> wardStockList = WardStockCacher.instance().getWardStockList(ward);
		
		Map<String, WardStock> wardStockMap = new HashMap<String, WardStock>();
		for (WardStock wardStock : wardStockList) {
			wardStockMap.put(wardStock.getItemCode(), wardStock);
		}
		
		return wardStockMap;
	}
	
	@Override
	public List<String> retrieveWardFilterList(Workstore workstore, List<Ward> wardFilterList, List<WardGroup> wardGroupFilterList)
	{
		if (!CollectionUtils.isEmpty(wardFilterList)) {
			
			List<String> result = new ArrayList<String>();
			for (Ward ward: wardFilterList)
				result.add(ward.getWardCode());
			return result;
			
		} else if (wardGroupFilterList != null && wardGroupFilterList.size() > 0) {
			return retrieveWardCodeList(wardGroupFilterList, workstore.getWorkstoreGroup());
		} else {
			return new ArrayList<String>();
		}
	}

	@Override
	public List<String> retrieveWardFilterList(Workstore workstore, Boolean showAllWards)
	{
		if (Boolean.TRUE.equals(showAllWards)) {
			return retrieveWardCodeList(workstore.getWorkstoreGroup());
		} else {
			return retrieveActiveWardCodeList(workstore);
		}
	}
}
