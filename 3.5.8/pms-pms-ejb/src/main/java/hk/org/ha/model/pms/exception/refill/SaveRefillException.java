package hk.org.ha.model.pms.exception.refill;

import java.util.Arrays;

import javax.ejb.ApplicationException;

@ApplicationException(rollback=true)
public class SaveRefillException extends Exception {

	private static final long serialVersionUID = 1L;

	private String messageCode;
	
	private String[] messageParam;
	
	public SaveRefillException(String messageCode, String[] messageParam) {
		this.messageCode = messageCode;		
		this.messageParam = (messageParam == null) ? null : Arrays.copyOf(messageParam, messageParam.length);
	}

	public String getMessageCode() {
		return messageCode;
	}

	public String[] getMessageParam() {
		return messageParam;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder(this.getClass().getName());
		sb.append(": messageCode=");
		sb.append(messageCode);
		sb.append(", messageParam=");
		if (messageParam == null) {
			sb.append("null");
		}
		else {
			for (String param : messageParam) {
				sb.append(param);
				sb.append(' ');
			}
		}
		return sb.toString();
	}
}
