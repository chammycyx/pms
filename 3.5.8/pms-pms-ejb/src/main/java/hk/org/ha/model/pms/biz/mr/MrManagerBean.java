package hk.org.ha.model.pms.biz.mr;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.mr.MrException;
import hk.org.ha.model.pms.biz.medprofile.MedProfileItemConverterLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.udt.disp.MpTrxType;
import hk.org.ha.model.pms.udt.mr.MrType;
import hk.org.ha.model.pms.udt.vetting.CmsOrderType;
import hk.org.ha.model.pms.vo.mr.MedProfileMrItem;
import hk.org.ha.model.pms.vo.mr.MedProfileMrItemDetail;
import hk.org.ha.model.pms.vo.rx.IvAdditive;
import hk.org.ha.model.pms.vo.rx.IvRxDrug;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;
import hk.org.ha.service.pms.asa.interfaces.mr.MrServiceJmsRemote;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.security.Identity;

@AutoCreate
@Stateless
@Name("mrManager")
@MeasureCalls
public class MrManagerBean implements MrManagerLocal {

	private static final Set<String> FM_STATUS_WITH_IND_SET = new HashSet<String>(Arrays.asList("X","C","M","D"));
	
	@In
	private MedProfileItemConverterLocal medProfileItemConverter;
	
	@In
	private MrServiceJmsRemote mrServiceProxy;
	
    @Override
    public List<MedProfileMrItem> retrieveOnhandMedicationList(MedProfile medProfile) throws MrException {

    	List<MedProfileMrItem> medProfileMrItemList = mrServiceProxy.retrieveMrOnHandDrugList(
    			medProfile.getPatHospCode(), new Date(), CmsOrderType.Pharmacy, null,
    			medProfile.getPatient().getHkid(), medProfile.getMedCase().getCaseNum(), Boolean.TRUE,
    			"WKS", "SUPERMED");
    	processMedProfileMrItemList(medProfileMrItemList);
    	removeReviewInformation(medProfileMrItemList);
    	
    	return medProfileMrItemList;
    }

    @Override
    public List<MedProfileMrItem> retrieveCurrentMedicationList(MedProfile medProfile) throws MrException {

    	List<MedProfileMrItem> medProfileMrItemList = mrServiceProxy.retrieveMrCurrentDrugList(
    			medProfile.getPatHospCode(), new Date(), CmsOrderType.Pharmacy, null,
    			medProfile.getPatient().getHkid(), medProfile.getMedCase().getCaseNum(), Boolean.TRUE,
    			"WKS", "SUPERMED");
    	processMedProfileMrItemList(medProfileMrItemList);
    	
    	return medProfileMrItemList;
    }
    
    @Override
    public boolean addMrAnnotation(Identity identity, String patHospCode, String orderNum, Integer itemNum,
    		String mrAnnotation) throws MrException {
    	
    	Date now = new Date();
    	MedProfileMrItem medProfileMrItem = new MedProfileMrItem();
		medProfileMrItem.setPatHospCode(patHospCode);
		medProfileMrItem.setItemNum(itemNum);
		medProfileMrItem.setOrderNum(orderNum);
		
		MedProfileMrItemDetail medProfileMrItemDetail = new MedProfileMrItemDetail();
		medProfileMrItem.addMedProfileMrItemDetail(medProfileMrItemDetail);
		medProfileMrItemDetail.setAnnotation(mrAnnotation);
		medProfileMrItemDetail.setMrDate(now);
		medProfileMrItemDetail.setMrHospCode(patHospCode);
		medProfileMrItemDetail.setMrSeqNum(0);		
		medProfileMrItemDetail.setMrType(MrType.Annotation);
		medProfileMrItemDetail.setUserCode(identity.getCredentials().getUsername());
		
		return Boolean.TRUE.equals(mrServiceProxy.updateMoeMrDetailsAndSendMsg(
				patHospCode, medProfileMrItem, MpTrxType.MpMarkAnnotation));
    }
    
    private void processMedProfileMrItemList(List<MedProfileMrItem> medProfileMrItemList) {
    	
    	updateRxItemForFormatting(medProfileMrItemList);
    	medProfileItemConverter.formatMedProfileMoItemList(medProfileMrItemList);
    	assignGroupIndex(medProfileMrItemList);
    	removeUnnecessaryInformation(medProfileMrItemList);
    }
    
    private void removeUnnecessaryInformation(List<MedProfileMrItem> medProfileMrItemList) {
    	
    	for (MedProfileMrItem medProfileMrItem: medProfileMrItemList) {
    		medProfileMrItem.setMedProfileMrItemAlertList(null);
    		medProfileMrItem.setMedProfileMrItemFmList(null);
    	}
    }
    
    private void removeReviewInformation(List<MedProfileMrItem> medProfileMrItemList) {
    	
    	for (MedProfileMrItem medProfileMrItem: medProfileMrItemList) {
    		medProfileMrItem.setMedProfileMrItemReview(null);
    	}
    }
    
    private void updateRxItemForFormatting(List<MedProfileMrItem> medProfileMrItemList) {
    	
    	for (MedProfileMrItem medProfileMrItem: medProfileMrItemList) {
    		
    		RxItem rxItem = medProfileMrItem.getRxItem();
    		if (rxItem == null) {
    			continue;
    		}
    		updateFmFlag(rxItem);
    		updateAnnotation(medProfileMrItem, rxItem);
    	}
    }
    
    private void updateFmFlag(RxItem rxItem) {
    	
    	if (rxItem instanceof RxDrug) {
    		updateFmFlag((RxDrug) rxItem, rxItem);
    	} else if (rxItem instanceof IvRxDrug) {
    		IvRxDrug ivRxDrug = (IvRxDrug) rxItem; 
    		for (IvAdditive ivAdditive: ivRxDrug.getIvAdditiveList()) {
        		updateFmFlag(ivAdditive, rxItem);
    		}
    	}
    }
    
    private void updateFmFlag(RxDrug rxDrug, RxItem rxItem) {
    	
    	if (FM_STATUS_WITH_IND_SET.contains(rxDrug.getFmStatus())) {
    		rxItem.setFmFlag(true);
    	}
    }
    
    private void updateAnnotation(MedProfileMrItem medProfileMrItem, RxItem rxItem) {
    	
		for (MedProfileMrItemDetail detail: medProfileMrItem.getMedProfileMrItemDetailList()) {
    		
			if (detail.getMrType() == MrType.Annotation) {
				rxItem.setMrAnnotation(detail.getAnnotation());
			} else if (detail.getMrType() == MrType.Discontinue) {
				rxItem.setDiscontinueFlag(Boolean.TRUE);
				rxItem.setDiscontinueReason(detail.getDcReason());
				rxItem.setDiscontinueHospCode(detail.getMrHospCode());
				rxItem.setDiscontinueDate(detail.getMrDate());
			}
		}
    }
    
    private void assignGroupIndex(List<MedProfileMrItem> medProfileMrItemList) {
    	
    	int i = -1;
    	MedProfileMrItem previousMedProfileMrItem = null;
    	
    	for (MedProfileMrItem medProfileMrItem: medProfileMrItemList) {
    		if (!equalDrugs(previousMedProfileMrItem, medProfileMrItem)) {
    			i++;
    		}
			medProfileMrItem.setGroupIndex(i);
    		previousMedProfileMrItem = medProfileMrItem;
    	}
    }
    
    private boolean equalDrugs(MedProfileMrItem medProfileMrItem1, MedProfileMrItem medProfileMrItem2) {
    	
    	if (medProfileMrItem1 == null || medProfileMrItem2 == null || medProfileMrItem1.getRxItem() == null ||
    		medProfileMrItem2.getRxItem() == null) {
    		return false;
    	}
    	
    	RxItem rxItem1 = medProfileMrItem1.getRxItem();
    	RxItem rxItem2 = medProfileMrItem2.getRxItem();
    	
    	return MedProfileUtils.equalDrugs(rxItem1, rxItem2, false);
    }
}
