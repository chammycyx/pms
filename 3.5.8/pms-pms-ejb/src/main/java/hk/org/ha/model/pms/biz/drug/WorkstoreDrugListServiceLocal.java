package hk.org.ha.model.pms.biz.drug;
import hk.org.ha.model.pms.vo.rx.DhRxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;

import javax.ejb.Local;

@Local
public interface WorkstoreDrugListServiceLocal {

	void retrieveWorkstoreDrugListByDisplayName(String displayName);	
	void retrieveWorkstoreDrugListByDrugKey(Integer drugKey, boolean ignoreDrugScope);
	void retrieveWorkstoreDrugList(String prefixItemCode, boolean showNonSuspendItemOnly, boolean fdn);	
	void retrieveWorkstoreDrugListByRxItem(RxItem rxItem, Boolean displayNameOnly);
	void retrieveWorkstoreDrugListByDhRxDrug(DhRxDrug dhRxDrug, String itemCodePrefix);
	void destroy();
}
