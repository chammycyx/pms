package hk.org.ha.model.pms.persistence.medprofile;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.ItemDispConfig;
import hk.org.ha.model.pms.udt.medprofile.MedProfilePoItemRefillDurationUnit;
import hk.org.ha.model.pms.udt.reftable.ChargeSpecialtyType;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.vo.drug.DmDrugLite;
import hk.org.ha.model.pms.vo.drug.DmWarningLite;
import hk.org.ha.model.pms.vo.drug.DmFormLite;
import hk.org.ha.model.pms.vo.rx.CapdRxDrug;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.Regimen;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.granite.messaging.amf.io.util.externalizer.annotation.IgnoredProperty;

@Entity
@Table(name = "MED_PROFILE_PO_ITEM")
public class MedProfilePoItem extends VersionEntity {
	
	private static final long serialVersionUID = 1L;

	private static final String JAXB_CONTEXT_RX = "hk.org.ha.model.pms.vo.rx";
		
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "medProfilePoItemSeq")
	@SequenceGenerator(name = "medProfilePoItemSeq", sequenceName = "SQ_MED_PROFILE_PO_ITEM", initialValue = 100000000)
	private Long id;
	
	@Column(name = "ITEM_CODE", length = 6, nullable = false)
	private String itemCode;
	
	@Column(name = "LAST_ITEM_CODE", length = 6)
	private String lastItemCode;
	
	@Lob
	@Column(name = "REGIMEN_XML")
	private String regimenXml;
	
	@Column(name = "DOSE_QTY", precision = 19, scale = 4)
	private BigDecimal doseQty;
	
	@Column(name = "DOSE_UNIT", length = 15)
	private String doseUnit;
	
	@Column(name = "CAL_QTY", nullable = false, precision = 19, scale = 4)
	private BigDecimal calQty;
	
	@Column(name = "ADJ_QTY", nullable = false, precision = 19, scale = 4)
	private BigDecimal adjQty;
	
	@Column(name = "LAST_ADJ_QTY", precision = 19, scale = 4)
	private BigDecimal lastAdjQty;
	
	@Column(name = "ISSUE_QTY", nullable = false, precision = 19, scale = 4)
	private BigDecimal issueQty;
	
	@Column(name = "LAST_ISSUE_QTY", precision = 19, scale = 4)
	private BigDecimal lastIssueQty;
	
	@Column(name = "TOTAL_ISSUE_QTY", precision = 19, scale = 4)
	private BigDecimal totalIssueQty;
	
	@Column(name = "REMAIN_ISSUE_QTY", precision = 19, scale = 4)
	private BigDecimal remainIssueQty;
	
	@Column(name = "BASE_UNIT", length = 4)
	private String baseUnit;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DUE_DATE")
	private Date dueDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FIRST_DUE_DATE")
	private Date firstDueDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_DUE_DATE")
	private Date lastDueDate;
	
	@Column(name = "REFILL_DURATION")
	private Integer refillDuration;
	
    @Converter(name = "MedProfilePoItem.refillDurationUnit", converterClass = MedProfilePoItemRefillDurationUnit.Converter.class)
    @Convert("MedProfilePoItem.refillDurationUnit")
	@Column(name = "REFILL_DURATION_UNIT", length = 1)
	private MedProfilePoItemRefillDurationUnit refillDurationUnit;
    
	@Column(name = "DRUG_NAME", length = 59)
	private String drugName;

	@Column(name = "TRADE_NAME", length = 20)
	private String tradeName;
	
	@Column(name = "FORM_CODE", length = 3)
	private String formCode;
	
	@Column(name = "FORM_LABEL_DESC", length = 30)
	private String formLabelDesc;
	
	@Column(name = "ROUTE_CODE", length = 7)
	private String routeCode;
	
	@Column(name = "ROUTE_DESC", length = 20)
	private String routeDesc;
	
	@Column(name = "STRENGTH", length = 59)
	private String strength;
    
    @Column(name = "VOLUME_TEXT", length = 23)
    private String volumeText;
    
    @Column(name = "DANGER_DRUG_FLAG", nullable = false)
    private Boolean dangerDrugFlag;
    
    @Column(name = "WARD_STOCK_FLAG", nullable = false)
    private Boolean wardStockFlag;
    
	@Column(name = "WARN_CODE_1", length = 2)
	private String warnCode1;
	
	@Column(name = "WARN_CODE_2", length = 2)
	private String warnCode2;
	
	@Column(name = "WARN_CODE_3", length = 2)
	private String warnCode3;
	
	@Column(name = "WARN_CODE_4", length = 2)
	private String warnCode4;
	
	@Column(name = "ADDITIVE_NUM")
	private Integer additiveNum;
	
	@Column(name = "FLUID_NUM")
	private Integer fluidNum;
	
	@Column(name = "REMARK_TEXT", length = 125)
	private String remarkText;

	@Column(name = "DRUG_SYNONYM", length = 46)
	private String drugSynonym;
	
	@Column(name = "RE_DISP_FLAG", nullable = false)
	private Boolean reDispFlag;
    
	@Column(name = "CHARGE_SPEC_CODE", length = 4)
	private String chargeSpecCode;
	
	@Column(name = "SINGLE_DISP_FLAG", nullable = false)
	private Boolean singleDispFlag;
	
	@Column(name = "DIRECT_PRINT_FLAG", nullable = false)
	private Boolean directPrintFlag;
	
	@Column(name = "UNIT_DOSE_FLAG", nullable = false)
	private Boolean unitDoseFlag;
	
	@Converter(name = "MedProfilePoItem.actionStatus", converterClass = ActionStatus.Converter.class)
	@Convert("MedProfilePoItem.actionStatus")
	@Column(name = "ACTION_STATUS", nullable = false, length = 1)
	private ActionStatus actionStatus;

	@Column(name = "FM_STATUS", length = 1)
	private String fmStatus;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_PRINT_DATE")
	private Date lastPrintDate;
	
	@Column(name = "MO_ISSUE_QTY")
	private Integer moIssueQty;

	@Column(name = "MO_BASE_UNIT", length = 4)
	private String moBaseUnit;
	
	@Column(name = "STAT_FLAG", nullable = false)
	private Boolean statFlag; 
	
	@Column(name = "NUM_OF_LABEL", nullable = false)
	private Integer numOfLabel;
	
	@Column(name = "LAST_DELIVERY_ITEM_ID")
	private Long lastDeliveryItemId;	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MED_PROFILE_MO_ITEM_ID", nullable = false)
	private MedProfileMoItem medProfileMoItem;

	@Column(name = "MED_PROFILE_MO_ITEM_ID", nullable = false, insertable = false, updatable = false)
	private Long medProfileMoItemId;	
	
	@Column(name = "ORG_MED_PROFILE_PO_ITEM_ID")
	private Long orgMedProfilePoItemId;	

	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE",      referencedColumnName = "HOSP_CODE",      nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;
	
	@Column(name = "HOSP_CODE", nullable = false, insertable = false, updatable = false)
	private String hospCode;	
	
	@Transient
	private Regimen regimen;
	
	@Transient
	private CapdRxDrug capdRxDrug;
	
	@Deprecated
	@IgnoredProperty
	@Transient
	private DmDrug dmDrug = null;// dummy for version 3.2.4 object reference compatibility in xml unmarshalling
	
	@Transient
	private DmDrugLite dmDrugLite;
	
	@Transient
	private DmFormLite dmFormLite;
	
	@Transient
	private DmWarningLite dmWarningLite1;
	
	@Transient
	private DmWarningLite dmWarningLite2;
	
	@Transient
	private DmWarningLite dmWarningLite3;
	
	@Transient
	private DmWarningLite dmWarningLite4;
	
	@Transient
	private Boolean modified;
	
	@Transient
	private ItemDispConfig itemDispConfig;
	
	@Transient
	private Boolean specialControlFlag;
	
	@Transient
	private Boolean dispDurationModified;
	
	@Transient
	private Boolean dispDurationCalcFlag;
	
	@Transient
	private String uuid;
	
	@Transient
	private Boolean calBothIssueQtyAndTotalQtyForDoseSpecified;
	
	@Transient
	private Boolean purchaseRequestExistFlag;
	
	@Transient
	private Boolean unitDoseItem;
	
	public MedProfilePoItem() {
		adjQty = new BigDecimal(0);
		dangerDrugFlag = Boolean.FALSE;
		wardStockFlag = Boolean.FALSE;
		reDispFlag = Boolean.FALSE;
		singleDispFlag = Boolean.FALSE;
		directPrintFlag = Boolean.FALSE;
		unitDoseFlag = Boolean.FALSE;
		actionStatus = ActionStatus.DispByPharm;
		statFlag = Boolean.FALSE;
		modified = Boolean.FALSE;
		numOfLabel = new Integer(1);
		specialControlFlag = Boolean.FALSE;
		dispDurationModified = Boolean.FALSE;
		dispDurationCalcFlag = Boolean.FALSE;
		purchaseRequestExistFlag = Boolean.FALSE;
		unitDoseItem = Boolean.FALSE;
	}
	
	@PostLoad
	public void postLoad() {

    	if (regimenXml != null) {
    		JaxbWrapper<Regimen> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
    		regimen = rxJaxbWrapper.unmarshall(regimenXml);
    	}
	}
	
	public void loadDmInfo() {

		if (dmDrugLite == null && itemCode != null) {
    		dmDrugLite = DmDrugLite.objValueOf(itemCode);
    	}  
    	
    	if (dmFormLite == null && formCode != null) {
    		dmFormLite = DmFormLite.objValueOf(formCode);
    	}
		
    	if (dmWarningLite1 == null && warnCode1 != null) {
    		dmWarningLite1 = DmWarningLite.objValueOf(warnCode1);
    	}
    	
    	if (dmWarningLite2 == null && warnCode2 != null) {
    		dmWarningLite2 = DmWarningLite.objValueOf(warnCode2);
    	}
    	
    	if (dmWarningLite3 == null && warnCode3 != null) {
    		dmWarningLite3 = DmWarningLite.objValueOf(warnCode3);
    	}
    	
    	if (dmWarningLite4 == null && warnCode4 != null) {
    		dmWarningLite4 = DmWarningLite.objValueOf(warnCode4);
    	}
    	
		if (regimen != null) {    			    			
			regimen.loadDmInfo();
			
			if (this.medProfileMoItem != null && this.medProfileMoItem.isManualItem()) {
				regimen.loadManualEntryPharmLineList(itemCode, dmDrugLite.getFormCode(), formCode);
			} else {
				regimen.loadListForMp(itemCode, formCode);
			}
		}    		
	}
	
	@PrePersist
	public void prePersist() {
		if (orgMedProfilePoItemId == null) {
			orgMedProfilePoItemId = id;
		}
		preSave();
	}
	
	
    @PreUpdate
	public void preSave() {
		if (regimen != null) {
			JaxbWrapper<Regimen> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
			regimenXml = rxJaxbWrapper.marshall(regimen);
			
			statFlag = MedProfileUtils.isStatDose(regimen); 
		}
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	
	public String getLastItemCode() {
		return lastItemCode;
	}

	public void setLastItemCode(String lastItemCode) {
		this.lastItemCode = lastItemCode;
	}

	public String getRegimenXml() {
		return regimenXml;
	}

	public void setRegimenXml(String regimenXml) {
		this.regimenXml = regimenXml;
	}
	
	public BigDecimal getDoseQty() {
		return doseQty;
	}

	public void setDoseQty(BigDecimal doseQty) {
		this.doseQty = doseQty;
	}

	public String getDoseUnit() {
		return doseUnit;
	}

	public void setDoseUnit(String doseUnit) {
		this.doseUnit = doseUnit;
	}

	public BigDecimal getCalQty() {
		return calQty;
	}

	public void setCalQty(BigDecimal calQty) {
		this.calQty = calQty;
	}

	public BigDecimal getAdjQty() {
		return adjQty;
	}

	public void setAdjQty(BigDecimal adjQty) {
		this.adjQty = adjQty;
	}

	public BigDecimal getIssueQty() {
		return issueQty;
	}

	public void setIssueQty(BigDecimal issueQty) {
		this.issueQty = issueQty;
	}
	
	public BigDecimal getLastIssueQty() {
		return lastIssueQty;
	}

	public void setLastIssueQty(BigDecimal lastIssueQty) {
		this.lastIssueQty = lastIssueQty;
	}
	
	public BigDecimal getTotalIssueQty() {
		return totalIssueQty;
	}

	public void setTotalIssueQty(BigDecimal totalIssueQty) {
		this.totalIssueQty = totalIssueQty;
	}
	
	public BigDecimal getRemainIssueQty() {
		return remainIssueQty;
	}

	public void setRemainIssueQty(BigDecimal remainIssueQty) {
		this.remainIssueQty = remainIssueQty;
	}

	public BigDecimal getLastAdjQty() {
		return lastAdjQty;
	}

	public void setLastAdjQty(BigDecimal lastAdjQty) {
		this.lastAdjQty = lastAdjQty;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}
	
	public Date getDueDate() {
		return dueDate == null ? null : new Date(dueDate.getTime());
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate == null ? null : new Date(dueDate.getTime());
	}
	
	public void setFirstDueDate(Date firstDueDate) {
		this.firstDueDate = firstDueDate;
	}

	public Date getFirstDueDate() {
		return firstDueDate;
	}

	public Date getLastDueDate() {
		return lastDueDate == null ? null : new Date(lastDueDate.getTime());
	}

	public void setLastDueDate(Date lastDueDate) {
		this.lastDueDate = lastDueDate == null ? null : new Date(lastDueDate.getTime());
	}

	public Integer getRefillDuration() {
		return refillDuration;
	}

	public void setRefillDuration(Integer refillDuration) {
		this.refillDuration = refillDuration;
	}

	public MedProfilePoItemRefillDurationUnit getRefillDurationUnit() {
		return refillDurationUnit;
	}

	public void setRefillDurationUnit(
			MedProfilePoItemRefillDurationUnit refillDurationUnit) {
		this.refillDurationUnit = refillDurationUnit;
	}
	
	public String getDrugName() {
		return drugName;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public String getTradeName() {
		return tradeName;
	}

	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}

	public String getFormCode() {
		return formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	public String getFormLabelDesc() {
		return formLabelDesc;
	}

	public void setFormLabelDesc(String formLabelDesc) {
		this.formLabelDesc = formLabelDesc;
	}
	
	public String getRouteCode() {
		return routeCode;
	}

	public void setRouteCode(String routeCode) {
		this.routeCode = routeCode;
	}

	public String getRouteDesc() {
		return routeDesc;
	}

	public void setRouteDesc(String routeDesc) {
		this.routeDesc = routeDesc;
	}

	public String getStrength() {
		return strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public String getVolumeText() {
		return volumeText;
	}

	public void setVolumeText(String volumeText) {
		this.volumeText = volumeText;
	}
	
	public Boolean getDangerDrugFlag() {
		return dangerDrugFlag;
	}

	public void setDangerDrugFlag(Boolean dangerDrugFlag) {
		this.dangerDrugFlag = dangerDrugFlag;
	}
	
	public Boolean getWardStockFlag() {
		return wardStockFlag;
	}

	public void setWardStockFlag(Boolean wardStockFlag) {
		this.wardStockFlag = wardStockFlag;
	}

	public String getWarnCode1() {
		return warnCode1;
	}

	public void setWarnCode1(String warnCode1) {
		this.warnCode1 = warnCode1;
	}

	public String getWarnCode2() {
		return warnCode2;
	}

	public void setWarnCode2(String warnCode2) {
		this.warnCode2 = warnCode2;
	}

	public String getWarnCode3() {
		return warnCode3;
	}

	public void setWarnCode3(String warnCode3) {
		this.warnCode3 = warnCode3;
	}

	public String getWarnCode4() {
		return warnCode4;
	}

	public void setWarnCode4(String warnCode4) {
		this.warnCode4 = warnCode4;
	}

	public String getRemarkText() {
		return remarkText;
	}

	public void setRemarkText(String remarkText) {
		this.remarkText = remarkText;
	}

	public String getDrugSynonym() {
		return drugSynonym;
	}

	public void setDrugSynonym(String drugSynonym) {
		this.drugSynonym = drugSynonym;
	}
	
	public Boolean getReDispFlag() {
		return reDispFlag;
	}

	public void setReDispFlag(Boolean reDispFlag) {
		this.reDispFlag = reDispFlag;
	}
	
	public String getChargeSpecCode() {
		return chargeSpecCode;
	}

	public void setChargeSpecCode(String chargeSpecCode) {
		this.chargeSpecCode = chargeSpecCode;
	}

	public Boolean getSingleDispFlag() {
		return singleDispFlag;
	}
	
	public void setSingleDispFlag(Boolean singleDispFlag) {
		this.singleDispFlag = singleDispFlag;
	}
	
	public Boolean getDirectPrintFlag() {
		return directPrintFlag;
	}

	public void setDirectPrintFlag(Boolean directPrintFlag) {
		this.directPrintFlag = directPrintFlag;
	}
	
	public Boolean getUnitDoseFlag() {
		return unitDoseFlag;
	}

	public void setUnitDoseFlag(Boolean unitDoseFlag) {
		this.unitDoseFlag = unitDoseFlag;
	}

	public MedProfileMoItem getMedProfileMoItem() {
		return medProfileMoItem;
	}

	public void setMedProfileMoItem(MedProfileMoItem medProfileMoItem) {
		this.medProfileMoItem = medProfileMoItem;
	}
	
	public Long getMedProfileMoItemId() {
		return medProfileMoItemId;
	}

	public void setMedProfileMoItemId(Long medProfileMoItemId) {
		this.medProfileMoItemId = medProfileMoItemId;
	}

	public Long getOrgMedProfilePoItemId() {
		return orgMedProfilePoItemId;
	}

	public void setOrgMedProfilePoItemId(Long orgMedProfilePoItemId) {
		this.orgMedProfilePoItemId = orgMedProfilePoItemId;
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}
	
	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public Regimen getRegimen() {
		return regimen;
	}

	public void setRegimen(Regimen regimen) {
		this.regimen = regimen;
	}

	public CapdRxDrug getCapdRxDrug() {
		return capdRxDrug;
	}

	public void setCapdRxDrug(CapdRxDrug capdRxDrug) {
		this.capdRxDrug = capdRxDrug;
	}
	
	@Deprecated
	public DmDrug getDmDrug() {
		return dmDrug;
	}

	@Deprecated
	public void setDmDrug(DmDrug dmDrug) {
		this.dmDrug = dmDrug;
	}

	public Boolean getModified() {
		return modified;
	}

	public void setModified(Boolean modified) {
		this.modified = modified;
	}
	
	public ItemDispConfig getItemDispConfig() {
		return itemDispConfig;
	}

	public void setItemDispConfig(ItemDispConfig itemDispConfig) {
		this.itemDispConfig = itemDispConfig;
	}
	
	public Boolean getSpecialControlFlag() {
		return specialControlFlag;
	}

	public void setSpecialControlFlag(Boolean specialControlFlag) {
		this.specialControlFlag = specialControlFlag;
	}

	public void setDispDurationModified(Boolean dispDurationModified) {
		this.dispDurationModified = dispDurationModified;
	}

	public Boolean getDispDurationModified() {
		return dispDurationModified;
	}

	public void setDispDurationCalcFlag(Boolean dispDurationCalcFlag) {
		this.dispDurationCalcFlag = dispDurationCalcFlag;
	}

	public Boolean getDispDurationCalcFlag() {
		return dispDurationCalcFlag;
	}

	public void setDmFormLite(DmFormLite dmFormLite) {
		this.dmFormLite = dmFormLite;
	}

	public DmFormLite getDmFormLite() {
		return dmFormLite;
	}

	public void setDmWarningLite1(DmWarningLite dmWarningLite1) {
		this.dmWarningLite1 = dmWarningLite1;
	}

	public DmWarningLite getDmWarningLite1() {
		return dmWarningLite1;
	}

	public void setDmWarningLite2(DmWarningLite dmWarningLite2) {
		this.dmWarningLite2 = dmWarningLite2;
	}

	public DmWarningLite getDmWarningLite2() {
		return dmWarningLite2;
	}

	public void setDmWarningLite3(DmWarningLite dmWarningLite3) {
		this.dmWarningLite3 = dmWarningLite3;
	}

	public DmWarningLite getDmWarningLite3() {
		return dmWarningLite3;
	}

	public void setDmWarningLite4(DmWarningLite dmWarningLite4) {
		this.dmWarningLite4 = dmWarningLite4;
	}

	public DmWarningLite getDmWarningLite4() {
		return dmWarningLite4;
	}
	
	public Integer getAdditiveNum() {
		return additiveNum;
	}

	public void setAdditiveNum(Integer additiveNum) {
		this.additiveNum = additiveNum;
	}

	public Integer getFluidNum() {
		return fluidNum;
	}

	public void setFluidNum(Integer fluidNum) {
		this.fluidNum = fluidNum;
	}

	public ActionStatus getActionStatus() {
		return actionStatus;
	}

	public void setActionStatus(ActionStatus actionStatus) {
		this.actionStatus = actionStatus;
	}

	public String getFmStatus() {
		return fmStatus;
	}

	public void setFmStatus(String fmStatus) {
		this.fmStatus = fmStatus;
	}

	public Date getLastPrintDate() {
		return lastPrintDate == null ? null : new Date(lastPrintDate.getTime());
	}

	public void setLastPrintDate(Date lastPrintDate) {
		this.lastPrintDate = lastPrintDate == null ? null : new Date(lastPrintDate.getTime());
	}

	public void setMoIssueQty(Integer moIssueQty) {
		this.moIssueQty = moIssueQty;
	}

	public Integer getMoIssueQty() {
		return moIssueQty;
	}

	public void setMoBaseUnit(String moBaseUnit) {
		this.moBaseUnit = moBaseUnit;
	}

	public String getMoBaseUnit() {
		return moBaseUnit;
	}
	
	public void setStatFlag(Boolean statFlag) {
		this.statFlag = statFlag;
	}
	
	public Boolean getStatFlag() {
		return statFlag;
	}	
	
	public void setNumOfLabel(Integer numOfLabel) {
		this.numOfLabel = numOfLabel;
	}
	
	public Integer getNumOfLabel() {
		return numOfLabel;
	}
	
	public Long getLastDeliveryItemId() {
		return lastDeliveryItemId;
	}

	public void setLastDeliveryItemId(Long lastDeliveryItemId) {
		this.lastDeliveryItemId = lastDeliveryItemId;
	}

	public void updateChargeSpecCode(Map<ChargeSpecialtyType, String> chargeSpecCodeMap, String specCode) {
		
		String chargeSpecCode = null;
		
		if (actionStatus == ActionStatus.PurchaseByPatient) {
			chargeSpecCode = chargeSpecCodeMap.get(ChargeSpecialtyType.Sfi);
		} else if (actionStatus == ActionStatus.SafetyNet) {
			chargeSpecCode = chargeSpecCodeMap.get(ChargeSpecialtyType.SafetyNet);
		}
		
		if (chargeSpecCode == null) {
			chargeSpecCode = specCode;
		}
		
		if (chargeSpecCode != null && !chargeSpecCode.equals(getChargeSpecCode())) {
			setChargeSpecCode(chargeSpecCode);
		}
	}
	
	@Transient
	public Integer getRefillDurationInDays() {
		if (refillDuration == null || refillDurationUnit == null)
			return null;
		
		switch (refillDurationUnit) {
		case Day:
			return refillDuration;
		case Week:
			return refillDuration * 7;
		case Month:
		case Cycle:
			return refillDuration * 28;
		}
		return null;
	}
	
	@Transient
	public boolean isRefill() {
		return lastDueDate != null;
	}
	
	@Transient
	public boolean isNoLabelItem() {
		return BigDecimal.ZERO.compareTo(MedProfileUtils.add(adjQty, issueQty)) == 0 ||
				actionStatus == ActionStatus.ContinueWithOwnStock ||
				getFmStatusEnum() == FmStatus.FreeTextEntryItem ||
				((Boolean.TRUE.equals(wardStockFlag) || Boolean.TRUE.equals(dangerDrugFlag)) && !Boolean.TRUE.equals(reDispFlag));
	}
	
	@Transient
	public boolean isSfiItem() {
		return ActionStatus.PurchaseByPatient_SafetyNet.contains(actionStatus);
	}
	
	@Transient
	public FmStatus getFmStatusEnum() {
		return fmStatus == null ? null : FmStatus.dataValueOf(fmStatus);
	}
	
	@Transient
	public Date getEndDate() {
		
		MedProfileMoItem medProfileMoItem = getMedProfileMoItem();
		if (medProfileMoItem == null) {
			throw new IllegalStateException("Cannot determine end date without medProfileMoItem");
		}
		
		if (medProfileMoItem.getRxItem() instanceof CapdRxDrug) {
			
			Date endDate = getEndDateFromFirstDoseGroup();
			
			if (endDate != null) {
				return endDate;
			}
		}
		return medProfileMoItem.getEndDate();
	}
	
	private Date getEndDateFromFirstDoseGroup() {
		
		Regimen regimen = getRegimen();
		
		if (regimen == null) {
			return null;
		}
		
		DoseGroup doseGroup = MedProfileUtils.getFirstItem(regimen.getDoseGroupList());
		
		return doseGroup == null ? null : doseGroup.getEndDate();
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getUuid() {
		return uuid;
	}

	public void setCalBothIssueQtyAndTotalQtyForDoseSpecified(
			Boolean calBothIssueQtyAndTotalQtyForDoseSpecified) {
		this.calBothIssueQtyAndTotalQtyForDoseSpecified = calBothIssueQtyAndTotalQtyForDoseSpecified;
	}

	public Boolean getCalBothIssueQtyAndTotalQtyForDoseSpecified() {
		return calBothIssueQtyAndTotalQtyForDoseSpecified;
	}

	public void setPurchaseRequestExistFlag(Boolean purchaseRequestExistFlag) {
		this.purchaseRequestExistFlag = purchaseRequestExistFlag;
	}

	public Boolean getPurchaseRequestExistFlag() {
		return purchaseRequestExistFlag;
	}

	public void setUnitDoseItem(Boolean unitDoseItem) {
		this.unitDoseItem = unitDoseItem;
	}

	public Boolean getUnitDoseItem() {
		return unitDoseItem;
	}
	
	public DmDrugLite getDmDrugLite() {
		return dmDrugLite;
	}

	public void setDmDrugLite(DmDrugLite dmDrugLite) {
		this.dmDrugLite = dmDrugLite;
	}
}
