package hk.org.ha.model.pms.biz.alert.mds;

import static hk.org.ha.model.pms.prop.Prop.REFILL_STANDARD_ALERT_MDS_DDI_CHECK;
import hk.org.ha.fmk.pms.remote.marshall.EclipseLinkXStreamMarshaller;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.alert.AlertAuditManagerLocal;
import hk.org.ha.model.pms.biz.order.MedOrderManagerLocal;
import hk.org.ha.model.pms.biz.refill.RefillScheduleManagerLocal;
import hk.org.ha.model.pms.persistence.AlertEntity;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedOrderItemAlert;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.udt.alert.mds.AlertType;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.OnHandSourceType;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
import hk.org.ha.model.pms.vo.alert.mds.AlertDdim;
import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;
import hk.org.ha.model.pms.vo.alert.mds.MdsResult;
import hk.org.ha.model.pms.vo.alert.mds.MedProfileAlertMsg;
import hk.org.ha.model.pms.vo.alert.mds.SteroidAlert;
import hk.org.ha.model.pms.vo.rx.IvAdditive;
import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ejb.Stateless;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("prescCheckManager")
@MeasureCalls
public class PrescCheckManagerBean implements PrescCheckManagerLocal {

	@Logger
	private Log logger;

	@In
	private AlertRetrieveManagerLocal alertRetrieveManager;
	
	@In
	private AlertMsgManagerLocal alertMsgManager;
	
	@In
	private MedOrderManagerLocal medOrderManager;
	
	@In
	private RefillScheduleManagerLocal refillScheduleManager;
	
	@In
	private AlertComparatorInf alertComparator;
	
	@In
	private AlertAuditManagerLocal alertAuditManager;

	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private List<MedProfileAlertMsg> prescAlertMsgList;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private Map<Integer, List<AlertEntity>> currentAlertMap;

	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private List<SteroidAlert> steroidAlertList;

	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private OnHandSourceType ddimOnHandSourceType;
	
	@In(required = false)
	private PharmOrder pharmOrder;
	
	public void overridePrescAlert(List<Integer> itemNumList) {

		logger.debug("currentAlertMap:\n#0", (new EclipseLinkXStreamMarshaller()).toXML(currentAlertMap));
		
		MedOrder medOrder = getPharmOrder().getMedOrder();

		replacePrescAlert(medOrder);
		
		Long currOrdNo;
		if (medOrder.getOrderNum() == null) {
			currOrdNo = Long.valueOf(0);
		}
		else {
			currOrdNo = Long.valueOf(medOrder.getOrderNum().substring(3));
		}
		
		if (itemNumList != null) {
			for (Integer itemNum : new HashSet<Integer>(itemNumList)) {
				
				MedOrderItem medOrderItem = medOrder.getMedOrderItemByItemNum(itemNum);
				medOrder.removeDeletedMedOrderItem(medOrderItem);
				for (PharmOrderItem poi : medOrderItem.getPharmOrderItemList()) {
					refillScheduleManager.removeRefillScheduleListByPharmOrderItemNum(getPharmOrder(), poi.getOrgItemNum());
				}
				
				currentAlertMap.remove(itemNum);

				for (Entry<Integer, List<AlertEntity>> v : currentAlertMap.entrySet()) {

					Set<AlertEntity> deleteSet = new HashSet<AlertEntity>();
					
					for (AlertEntity alertEntity : v.getValue()) {
						if (alertEntity.getAlertType() == AlertType.Ddim) {
							AlertDdim alertDdim = (AlertDdim) alertEntity.getAlert();
							if ((currOrdNo.equals(alertDdim.getOrdNo1()) && Long.valueOf(itemNum.longValue()).equals(alertDdim.getItemNum1()))
							 || (currOrdNo.equals(alertDdim.getOrdNo2()) && Long.valueOf(itemNum.longValue()).equals(alertDdim.getItemNum2()))) {
								deleteSet.add(alertEntity);
							}
						}
					}

					v.getValue().removeAll(deleteSet);
				}
			}
		}

		medOrderManager.setPharmOrder(getPharmOrder());
	}
	
	private boolean isDdiEnable() {
		if (getOrderViewInfo().getOneStopOrderType() == OneStopOrderType.RefillOrder) {
			return REFILL_STANDARD_ALERT_MDS_DDI_CHECK.get(Boolean.FALSE);
		}
		else {
			return true;
		}
	}
	
	public void replacePrescAlert(MedOrder medOrder) {

		Map<Integer, AlertEntity> ddiAlertMap = new HashMap<Integer, AlertEntity>();
		
		for (List<AlertEntity> alertEntityList : currentAlertMap.values()) {
			for (AlertEntity alertEntity : alertEntityList) {
				if (alertEntity.isDdiAlert()) {
					if (!((AlertDdim) alertEntity.getAlert()).getCopyFlag()) {
						ddiAlertMap.put(alertEntity.getAlert().hashCode(), alertEntity);
					}
				}
			}
		}
		
		for (MedOrderItem moi : medOrder.getMedOrderItemList()) {

			List<AlertEntity> currentAlertList;
			if (currentAlertMap.containsKey(moi.getItemNum())) {
				currentAlertList = currentAlertMap.get(moi.getItemNum());
			}
			else {
				currentAlertList = new ArrayList<AlertEntity>();
			}
			
			if (alertComparator.isAlertListEqual(
					moi.getMdsAlertList(),
					currentAlertList)) {
				continue;
			}

			moi.clearAlert();

			for (AlertEntity alertEntity : currentAlertList) {
				if (alertEntity.isDdiAlert()) {
					AlertDdim alertDdim = (AlertDdim) alertEntity.getAlert();
					if (alertDdim.getCopyFlag()) {
						copyOverrideReason(ddiAlertMap.get(alertDdim.hashCode()), alertEntity);
					}
				}
				moi.addMedOrderItemAlert(new MedOrderItemAlert(alertEntity));
			}
			moi.markAlertFlag();
			moi.setChangeFlag(Boolean.TRUE);
		}
	}

	public boolean checkPrescAlert(OrderViewInfo orderViewInfo) {
		boolean writeAuditLog = pharmOrder.getMedOrder().getDocType() == MedOrderDocType.Normal;
		return checkPrescAlertByMedOrderItemList(orderViewInfo, getPharmOrder().getMedOrder().getMedOrderItemList(), writeAuditLog, pharmOrder.getMedOrder().isMpDischarge(), true);
	}
	
	public boolean checkPrescAlertByMedOrderItemList(OrderViewInfo orderViewInfo, List<MedOrderItem> medOrderItemList, boolean isMpDischarge) {
		return checkPrescAlertByMedOrderItemList(orderViewInfo, medOrderItemList, true, isMpDischarge, false);
	}
	
	private boolean checkPrescAlertByMedOrderItemList(OrderViewInfo orderViewInfo, List<MedOrderItem> medOrderItemList, boolean writeAuditLog, boolean isMpDischarge, boolean checkSteroid) {
		
		prescAlertMsgList = new ArrayList<MedProfileAlertMsg>();
		steroidAlertList = new ArrayList<SteroidAlert>();
		
		orderViewInfo.setAlertCheckFlag(true);
		
		boolean allowSubSetPatAlertFlag; // allow patient-specific alert in current check less than previous check
		switch (getOrderViewInfo().getOneStopOrderType()) {
			case SfiOrder:
			case RefillOrder:
				allowSubSetPatAlertFlag = true;
				break;
			default:
				allowSubSetPatAlertFlag = false;
				break;
		}
		
		MdsResult mdsResult;
		if (orderViewInfo.getAlertOverrideFlag()) {
			mdsResult = new MdsResult();
		}
		else {
			mdsResult = alertRetrieveManager.retrieveAlertByMedOrderItemList(medOrderItemList, isDdiEnable(), checkSteroid);
		}

		// convert AlertEntity to MedOrderItemAlert for outject
		currentAlertMap = new HashMap<Integer, List<AlertEntity>>();
		for (Entry<Integer, List<AlertEntity>> entry : mdsResult.getAlertEntityMap().entrySet()) {
			currentAlertMap.put(entry.getKey(), constructAlertEntityList(entry.getValue()));
		}

		List<MedProfileAlertMsg> patAlertMsgList = new ArrayList<MedProfileAlertMsg>();
		List<MedProfileAlertMsg> ddiAlertMsgList = new ArrayList<MedProfileAlertMsg>();
		
		for (MedOrderItem moi : medOrderItemList) {
			constructAlertMsgListForMedOrderItem(moi, allowSubSetPatAlertFlag, patAlertMsgList, ddiAlertMsgList, isMpDischarge);
		}
		
		prescAlertMsgList.addAll(patAlertMsgList);
		prescAlertMsgList.addAll(ddiAlertMsgList);
		
		steroidAlertList.addAll(mdsResult.getSteroidAlertList());
		
		ddimOnHandSourceType = mdsResult.getDdimOnHandSourceType();
		
		orderViewInfo.setAlertErrorInfoList(mdsResult.getErrorInfoList());
		
		if (writeAuditLog && !prescAlertMsgList.isEmpty()) {
			Ticket ticket = pharmOrder.getMedOrder().getTicket();
			alertAuditManager.newAlertDetect(pharmOrder, ticket, prescAlertMsgList);
		}
		
		Contexts.getSessionContext().set("prescAlertMsgList", prescAlertMsgList);
		Contexts.getSessionContext().set("steroidAlertList",  steroidAlertList);
		Contexts.getSessionContext().set("ddimOnHandSourceType",  ddimOnHandSourceType);
		
		return prescAlertMsgList.isEmpty() && steroidAlertList.isEmpty();
	}
	
	private void constructAlertMsgListForMedOrderItem(MedOrderItem moi, boolean allowSubSetPatAlertFlag, List<MedProfileAlertMsg> patAlertMsgList, List<MedProfileAlertMsg> ddiAlertMsgList, boolean isMpDischarge) {

		List<MedOrderItemAlert> prevAlertList = moi.getMdsAlertList();
		List<AlertEntity> currentAlertList;

		if (currentAlertMap.containsKey(moi.getItemNum())) {
			currentAlertList = currentAlertMap.get(moi.getItemNum());
		}
		else {
			currentAlertList = new ArrayList<AlertEntity>();
		}

		List<AlertEntity> patAlertList = new ArrayList<AlertEntity>();
		List<AlertEntity> ddiAlertList = new ArrayList<AlertEntity>();

		for (AlertEntity alertEntity : alertComparator.constructOutStandingAlertList(currentAlertList, prevAlertList, allowSubSetPatAlertFlag)) {
			if (alertEntity.isPatAlert()) {
				patAlertList.add(alertEntity);
			}
			else if (alertEntity.isDdiAlert() && (!((AlertDdim) alertEntity.getAlert()).getCopyFlag() || isMpDischarge)) {
				ddiAlertList.add(alertEntity);
			}
		}

		if (isMpDischarge) {
			if (!patAlertList.isEmpty() || !ddiAlertList.isEmpty()) {
				patAlertMsgList.add(constructMpDischargeAlertMsgList(moi, patAlertList, ddiAlertList));
			}
		}
		else {
			if (!patAlertList.isEmpty()) {
				patAlertMsgList.add(constructPatAlertMsgList(moi, patAlertList));
			}
			if (!ddiAlertList.isEmpty()) {
				ddiAlertMsgList.addAll(constructDdiAlertMsgList(moi, ddiAlertList));
			}
		}
	}

	private List<AlertEntity> constructAlertEntityList(List<AlertEntity> alertEntityList) {

		List<AlertEntity> moiAlertList = new ArrayList<AlertEntity>();

		for (AlertEntity alertEntity : alertEntityList) {
			MedOrderItemAlert moiAlert = new MedOrderItemAlert(alertEntity);
			moiAlert.setMedOrderItem(null);
			moiAlertList.add(moiAlert);
		}
		
		return moiAlertList;
	}

	private MedProfileAlertMsg constructMpDischargeAlertMsgList(MedOrderItem moi, List<AlertEntity> patAlertList, List<AlertEntity> ddiAlertList) {
		
		alertMsgManager.sortAlertEntityList(patAlertList);
		
		MedProfileAlertMsg medProfileAlertMsg = new MedProfileAlertMsg();
		
		List<AlertMsg> alertMsgList = new ArrayList<AlertMsg>();
		if (moi.getRxItem().isIvRxDrug()) {
			alertMsgList = constructAlertMsgForIvRxDrug(moi, patAlertList, ddiAlertList);
		}
		else {
			alertMsgList = Arrays.asList(new AlertMsg(
					moi.getItemNum(),
					moi.getMdsOrderDesc(),
					moi.getDisplayNameSaltForm(),
					patAlertList,
					ddiAlertList
			));
		}
		
		medProfileAlertMsg.setAlertMsgList(alertMsgList);
		medProfileAlertMsg.setOrderDesc(moi.getMdsOrderDesc());
		medProfileAlertMsg.setIvRxDrug(moi.getRxItem().isIvRxDrug());
		
		return medProfileAlertMsg;
	}
	
	private List<AlertMsg> constructAlertMsgForIvRxDrug(MedOrderItem moi, List<AlertEntity> patAlertList, List<AlertEntity> ddiAlertList) {
		
		List<AlertMsg> alertMsgList = new ArrayList<AlertMsg>();

		for (IvAdditive ivAdditive : moi.getIvRxDrug().getIvAdditiveList()) {
			
			List<AlertEntity> patSubAlertList = new ArrayList<AlertEntity>();
			List<AlertEntity> ddiSubAlertList = new ArrayList<AlertEntity>();

			for (AlertEntity patAlert : patAlertList) {
				if (ivAdditive.getAdditiveNum().equals(patAlert.getAdditiveNum())) {
					patSubAlertList.add(patAlert);
				}
			}
			for (AlertEntity ddiAlert : ddiAlertList) {
				if (ivAdditive.getAdditiveNum().equals(ddiAlert.getAdditiveNum())) {
					ddiSubAlertList.add(ddiAlert);
				}
			}
			
			if (patSubAlertList.isEmpty() && ddiSubAlertList.isEmpty()) {
				continue;
			}
			
			alertMsgList.add(new AlertMsg(
					moi.getItemNum(),
					ivAdditive.getMdsOrderDesc(),
					ivAdditive.getDisplayNameSaltForm(),
					new ArrayList<AlertEntity>(patSubAlertList),
					new ArrayList<AlertEntity>(ddiSubAlertList)
			));
		}
		
		return alertMsgList;
	}

	private MedProfileAlertMsg constructPatAlertMsgList(MedOrderItem moi, List<AlertEntity> patAlertList) {
		
		alertMsgManager.sortAlertEntityList(patAlertList);

		MedProfileAlertMsg medProfileAlertMsg = new MedProfileAlertMsg();
		medProfileAlertMsg.setAlertMsgList(Arrays.asList(new AlertMsg(
				moi.getItemNum(),
				moi.getMdsOrderDesc(),
				moi.getDisplayNameSaltForm(),
				patAlertList
		)));
		medProfileAlertMsg.setOrderDesc(moi.getMdsOrderDesc());
		
		return medProfileAlertMsg;
	}
	
	private List<MedProfileAlertMsg> constructDdiAlertMsgList(MedOrderItem moi, List<AlertEntity> ddiAlertList) {
		
		List<MedProfileAlertMsg> medProfileAlertMsgList = new ArrayList<MedProfileAlertMsg>();

		for (AlertEntity ddiAlert : ddiAlertList) {
			
			MedProfileAlertMsg medProfileAlertMsg = new MedProfileAlertMsg();
			medProfileAlertMsg.setAlertMsgList(Arrays.asList(new AlertMsg(
					moi.getItemNum(),
					moi.getMdsOrderDesc(),
					moi.getDisplayNameSaltForm(),
					null,
					new ArrayList<AlertEntity>(Arrays.asList(ddiAlert))
			)));
			medProfileAlertMsg.setOrderDesc(moi.getMdsOrderDesc());
			
			medProfileAlertMsgList.add(medProfileAlertMsg);
		}
		return medProfileAlertMsgList;
	}

	public void clearPrescAlert(OrderViewInfo orderViewInfo) {
		prescAlertMsgList = new ArrayList<MedProfileAlertMsg>();
		Contexts.getSessionContext().set("prescAlertMsgList", prescAlertMsgList);
		steroidAlertList = new ArrayList<SteroidAlert>();
		Contexts.getSessionContext().set("steroidAlertList", steroidAlertList);
	}
	
	private void copyOverrideReason(AlertEntity fromAlertEntity, AlertEntity toAlertEntity) {
		toAlertEntity.setOverrideReason(fromAlertEntity.getOverrideReason());
		toAlertEntity.setAckDate(fromAlertEntity.getAckDate());
		toAlertEntity.setAckUser(fromAlertEntity.getAckUser());
	}
	
	private PharmOrder getPharmOrder() {
		return (PharmOrder) Contexts.getSessionContext().get("pharmOrder");
	}
	
	private OrderViewInfo getOrderViewInfo() {
		return (OrderViewInfo) Contexts.getSessionContext().get("orderViewInfo");
	}	
}
