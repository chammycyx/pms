package hk.org.ha.model.pms.udt.vetting;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum RegimenDurationUnit implements StringValuedEnum {

	Day("D", "Days"),
	Week("W", "Weeks"),
	Month("M", "Months"),
	Cycle("C", "Cycles");
	
	private final String dataValue;
	private final String displayValue;
	
	RegimenDurationUnit (final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}
	
	public static RegimenDurationUnit dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(RegimenDurationUnit.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<RegimenDurationUnit> {

		private static final long serialVersionUID = 1L;

		public Class<RegimenDurationUnit> getEnumClass() {
    		return RegimenDurationUnit.class;
    	}
    }
}
