package hk.org.ha.model.pms.biz.report;

import static hk.org.ha.model.pms.prop.Prop.REPORT_UNCOLLECTPRESC_CRITERIA_ORDER_DURATION;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.persistence.PropEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.report.UncollectPrescRpt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.joda.time.DateTime;
@Stateful
@Scope(ScopeType.SESSION)
@Name("uncollectPrescRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class UncollectPrescRptServiceBean implements UncollectPrescRptServiceLocal {

	@PersistenceContext
	private EntityManager em;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent;
	
	@In
	private Workstore workstore;
	
	public Integer getMaxDayRange(){
		Integer maxDayRange = 0;
		PropEntity propEntity = REPORT_UNCOLLECTPRESC_CRITERIA_ORDER_DURATION.getPropEntity(workstore);
		
		if ( propEntity != null ) {
			String validation = propEntity.getProp().getValidation();
			int maxDaySubStringStart = validation.length();
			
			for ( int i = validation.length()-1; i > 0 ; i-- ) {
				if ( !isNumeric(validation.substring(i, i+1)) ) {
					maxDayRange = Integer.valueOf(validation.substring(maxDaySubStringStart));
					break;
				} else { 
					maxDaySubStringStart = i;
				}
			}
		}
		
		return maxDayRange;
	}
	
	@SuppressWarnings("unchecked")
	public List<UncollectPrescRpt> retrieveUncollectPrescRptList(Integer dayRange){
		List<UncollectPrescRpt> uncollectPrescRptList = new ArrayList<UncollectPrescRpt>();
		DateTime today = new DateTime();
		
		List<DispOrder> dispOrderList = em.createQuery(
											"select o from DispOrder o" + // 20151012 index check : DispOrder.workstore,orderType,ticketDate : I_DISP_ORDER_15
											" where o.orderType = :orderType" +
											" and o.workstore = :workstore" +
											" and o.ticketDate < :nextday and o.ticketDate > :beforeDate " +
											" and o.revokeFlag = :revokeFlag" +
											" and o.status not in :dispOrderStatus" +
											" order by o.ticketDate desc, o.ticketNum")
											.setParameter("orderType", OrderType.OutPatient)
											.setParameter("workstore", workstore)
											.setParameter("nextday", today.plusDays(1).toDate() ,TemporalType.DATE)
											.setParameter("beforeDate", today.minusDays(dayRange+1).toDate() ,TemporalType.DATE)
											.setParameter("revokeFlag", Boolean.TRUE)
											.setParameter("dispOrderStatus", DispOrderStatus.Deleted_SysDeleted)
											.getResultList();
		
		if ( !dispOrderList.isEmpty() ) {
			for ( DispOrder dispOrder : dispOrderList ) {
				UncollectPrescRpt uncollectPrescRpt = new UncollectPrescRpt();
				convertUncollectPrescRpt(uncollectPrescRpt, dispOrder);
				uncollectPrescRptList.add(uncollectPrescRpt);
			}
		}
		
		return uncollectPrescRptList;
	}
	
	public void printUncollectPrescRpt(List<UncollectPrescRpt> uncollectPrescRptList)
	{
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("hospCode", workstore.getHospCode());
		parameters.put("workstoreCode", workstore.getWorkstoreCode());
		parameters.put("totalPresc", uncollectPrescRptList.size());

	    printAgent.renderAndPrint(new RenderAndPrintJob(
							    		"UncollectPrescRpt",
									    new PrintOption(),
									    printerSelector.retrievePrinterSelect(PrintDocType.Report),
									    parameters,
									    uncollectPrescRptList));
	}
	

	private void convertUncollectPrescRpt(UncollectPrescRpt uncollectPrescRpt, DispOrder dispOrder){
		MedCase medCase = dispOrder.getPharmOrder().getMedCase();
		if ( medCase != null ) {
			uncollectPrescRpt.setCaseNum(medCase.getCaseNum());
		}
		if ( dispOrder.getRefrigItemFlag() == null ) {
			uncollectPrescRpt.setRefrigItemFlag(false);
		} else {
			uncollectPrescRpt.setRefrigItemFlag(dispOrder.getRefrigItemFlag());
		}
		uncollectPrescRpt.setDispOrderStatus(dispOrder.getStatus());
		if ( dispOrder.getPharmOrder().getMedOrder().getDocType() == MedOrderDocType.Normal ) {
			uncollectPrescRpt.setMoeOrderFlag(true);
		} else {
			uncollectPrescRpt.setMoeOrderFlag(false);
		}
		uncollectPrescRpt.setTicketDate(dispOrder.getTicketDate());
		uncollectPrescRpt.setTicketNum(dispOrder.getTicketNum());
		uncollectPrescRpt.setUpdateUser(dispOrder.getRevokeUser());
	}
	
	public static boolean isNumeric(String str)  
	{  
	  try  
	  {  
	    Double.parseDouble(str);  
	  }  
	  catch(NumberFormatException nfe)  
	  {  
	    return false;  
	  }  
	  return true;  
	}
	
	@Remove
	public void destroy() {
	}
}
