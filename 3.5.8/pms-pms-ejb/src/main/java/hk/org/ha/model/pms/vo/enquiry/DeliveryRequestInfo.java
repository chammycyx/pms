package hk.org.ha.model.pms.vo.enquiry;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class DeliveryRequestInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long deliveryRequestId;

	private String wardCodeCsv;

	private String caseNum;
	
	private Date createDate;

	public DeliveryRequestInfo() {
	}
	
	public DeliveryRequestInfo(Long deliveryRequestId, Date createDate, String wardCodeCsv, String caseNum) {
		super();
		this.deliveryRequestId = deliveryRequestId;
		this.createDate = createDate;
		this.wardCodeCsv = wardCodeCsv;
		this.caseNum = caseNum;
	}

	public Long getDeliveryRequestId() {
		return deliveryRequestId;
	}

	public void setDeliveryRequestId(Long deliveryRequestId) {
		this.deliveryRequestId = deliveryRequestId;
	}

	public String getWardCodeCsv() {
		return wardCodeCsv;
	}

	public void setWardCodeCsv(String wardCodeCsv) {
		this.wardCodeCsv = wardCodeCsv;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
}
