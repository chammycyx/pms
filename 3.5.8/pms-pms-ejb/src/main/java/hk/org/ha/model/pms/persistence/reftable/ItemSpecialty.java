package hk.org.ha.model.pms.persistence.reftable;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
import hk.org.ha.model.pms.persistence.phs.Specialty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "ITEM_SPECIALTY")
@Customizer(AuditCustomizer.class)
public class ItemSpecialty extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "itemSpecialtySeq")
	@SequenceGenerator(name = "itemSpecialtySeq", sequenceName = "SQ_ITEM_SPECIALTY", initialValue = 100000000)
	private Long id;
	
	@Column(name = "ITEM_CODE", nullable = false,  length = 6)
	private String itemCode;
	
	@Column(name = "SPEC_CODE", nullable = false,  length = 4)
	private String specCode;
	
    @ManyToOne
    @JoinColumn(name = "WORKSTORE_GROUP_CODE", nullable = false)
    private WorkstoreGroup workstoreGroup;
	
    @Transient
	private Boolean markDelete;
    
	@Transient
	private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public Boolean getMarkDelete() {
		return markDelete;
	}

	public void setMarkDelete(Boolean markDelete) {
		this.markDelete = markDelete;
	}

	public WorkstoreGroup getWorkstoreGroup() {
		return workstoreGroup;
	}

	public void setWorkstoreGroup(WorkstoreGroup workstoreGroup) {
		this.workstoreGroup = workstoreGroup;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	// helper methods
    public boolean isNew() {
    	return this.getId() == null;
    }
	
}
