package hk.org.ha.model.pms.persistence.corp;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.RecordStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "HOSPITAL_MAPPING")
@IdClass(HospitalMappingPK.class)
public class HospitalMapping extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "HOSP_CODE", nullable = false)
	private String hospCode;
	
	@Id
	@Column(name = "PAT_HOSP_CODE", nullable = false)
	private String patHospCode;
	
	@Column(name = "PRESCRIBE_FLAG", nullable = false)
	private Boolean prescribeFlag;
	
    @Converter(name = "HospitalMapping.status", converterClass = RecordStatus.Converter.class)
    @Convert("HospitalMapping.status")
	@Column(name="STATUS", nullable = false, length = 1)
	private RecordStatus status;
    
    @ManyToOne
    @JoinColumn(name = "HOSP_CODE", nullable = false, insertable = false, updatable = false)
    private Hospital hospital;

	public HospitalMapping() {
		status = RecordStatus.Active;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}
	
	public Boolean getPrescribeFlag() {
		return prescribeFlag;
	}

	public void setPrescribeFlag(Boolean prescribeFlag) {
		this.prescribeFlag = prescribeFlag;
	}

	public RecordStatus getStatus() {
		return status;
	}

	public void setStatus(RecordStatus status) {
		this.status = status;
	}

	public Hospital getHospital() {
		return hospital;
	}
}
