package hk.org.ha.model.pms.biz.checkissue.mp;

import static hk.org.ha.model.pms.prop.Prop.*;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.label.LabelManagerLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileManagerLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileStatManagerLocal;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.report.MpTrxRptPrintManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.medprofile.Delivery;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.persistence.medprofile.PurchaseRequest;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryItemStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryRequestStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryStatus;
import hk.org.ha.model.pms.vo.checkissue.mp.CheckDelivery;
import hk.org.ha.model.pms.vo.checkissue.mp.DeliveryProblemItem;
import hk.org.ha.model.pms.vo.checkissue.mp.DrugCheckViewListSummary;
import hk.org.ha.model.pms.vo.label.LabelContainer;
import hk.org.ha.model.pms.vo.label.MpDispLabel;
import hk.org.ha.model.pms.vo.medprofile.PasContext;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("checkService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CheckServiceBean implements CheckServiceLocal {
	
	private static final String JAXB_CONTEXT_LABEL = "hk.org.ha.model.pms.vo.label";

	@PersistenceContext
	private EntityManager em;
	
	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
	
	@In
	private MpTrxRptPrintManagerLocal mpTrxRptPrintManager;
	
	@In
	private MedProfileManagerLocal medProfileManager;
	
	@In
	private MedProfileStatManagerLocal medProfileStatManager;
		
	@In
	private LabelManagerLocal labelManager;
	
	@In
	private CheckListManagerLocal checkListManager;
	
	@In
	private CheckManagerLocal checkManager;
	
	@In
	private SendServiceLocal sendService;
	
	@In
	private PrintAgentInf printAgent;
	
	@In
	private Workstore workstore;
	
	@In
	private AuditLogger auditLogger; 

	private boolean autoUpdate = true;
	
	@Out(required = false)
	private CheckDelivery checkDelivery;
	
	private static final int QUERY_CHUNK_SIZE = 500; 
	
	private String resultMsg;
	
	private String batchCode;
	private String batchDate;
	
	private Delivery delivery;
	private Map<Long,DeliveryItem> deliveryItemAllMap;
	
	private DateFormat df = new SimpleDateFormat("yyyyMMdd"); 
	private DateFormat dfForBatchPrefix = new SimpleDateFormat("dd");
	
	private static final String IP_DRUG_CHECK = "check";
	
	@SuppressWarnings("unchecked")
	public DrugCheckViewListSummary retrieveDelivery(Date batchDate, String batchNumIn) {
		
		String batchNum = null;
		checkDelivery = null;
		resultMsg = StringUtils.EMPTY;
		delivery = null;
		deliveryItemAllMap = null;
		this.batchDate  = batchDate.toString();
		batchCode = batchNumIn;
		
		//FRS Chapter 05 Workflow 145 , prevent Delivery Label barcode value too long 
		if(batchNumIn != null && batchNumIn.length() > 4){
			batchNum = batchNumIn.substring(0, 4);
		}else{
			batchNum = batchNumIn;
		}

		deliveryItemAllMap = retrieveDeliveryItemList(batchDate, batchNum);
		
		boolean validateDelivery = false;
		Map<Long, MedProfile> medProfileMap = new TreeMap<Long, MedProfile>();
		
		if ( !deliveryItemAllMap.isEmpty() ) {
			for ( Long key : deliveryItemAllMap.keySet() ) {
				
				DeliveryItem deliveryItem = deliveryItemAllMap.get(key);
				delivery = deliveryItem.getDelivery();
				
				if(!validateDelivery){
					validDeliveryStatusForCheck(delivery);
					if ( StringUtils.isNotEmpty(resultMsg) ) {
						return retrieveDrugCheckViewListSummary();
					}
					validateDelivery = true;
				}
				
				deliveryItem.getDelivery().getDeliveryRequest();
				if ( deliveryItem.getDelivery().getDeliveryRequest() != null ) {
					deliveryItem.getDelivery().getDeliveryRequest().getDeliveryRequestAlertList();
					deliveryItem.getDelivery().getDeliveryRequest().getWorkstore();
				}
				MedProfile medProfile = deliveryItem.getMedProfile();
				
				medProfile.getMedCase();
				medProfile.getPatient().getName();
				medProfileMap.put(medProfile.getId(), medProfile);
			}
			
		} else {
			
			List<Delivery> deliveryList = em.createQuery(
					"select o from Delivery o" + // 20120317 index check : Delivery.hospital,batchDate,batchNum : I_DELIVERY_01
					" where o.hospital = :hospital" +
					" and o.batchDate = :batchDate" + 
					" and o.batchNum = :batchNum" +
					" and o.deliveryRequest.status = :deliveryRequestStatus")
					.setParameter("hospital", workstore.getHospital())
					.setParameter("batchDate", batchDate)
					.setParameter("batchNum", batchNum)
					.setParameter("deliveryRequestStatus", DeliveryRequestStatus.Completed)
					.getResultList();
			
			if ( deliveryList.isEmpty() ) {
				resultMsg = "0005" ;//Record not founnd.
				return retrieveDrugCheckViewListSummary();
			}else{
				resultMsg = "0637" ; //no item in batch
				return retrieveDrugCheckViewListSummary();
			}
			
		}
		
		if (CHECK_MP_PATIENT_PAS_PATIENT_ENABLED.get()) {
			for ( MedProfile medProfile : medProfileMap.values() ) {
				updatePasInfo(medProfile);
			}
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	private void updatePasInfo(MedProfile medProfile){
		PasContext pasContext = medProfileManager.retrieveContext(medProfile);
		
		List<MedProfile> medProfileList = em.createQuery(
				"select o from MedProfile o" + // 20120214 index check : MedProfile.id : PK_MED_PROFILE
				" where o.id = :medProfileId" +
				" and o.id is not null") // don't delete : magic code fix bug for https://bugs.eclipse.org/bugs/show_bug.cgi?id=347592
				.setParameter("medProfileId", medProfile.getId())
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
				.getResultList();
		
		medProfileManager.applyChanges(pasContext, medProfileList.get(0));
		medProfileStatManager.recalculateMedProfilePoItemStat(medProfileList.get(0));
	}
	
	public DrugCheckViewListSummary convertToCheckDelivery() {

		
		checkDelivery = new CheckDelivery();
		checkDelivery.setBatchDate(delivery.getBatchDate());
		checkDelivery.setBatchNum(delivery.getBatchNum());
		checkDelivery.setDeliveryId(delivery.getId());
		checkDelivery.setDeliveryProblemItemList(new ArrayList<DeliveryProblemItem>());
		checkDelivery.setDeliveryVersion(delivery.getVersion());
		
		if ( !deliveryItemAllMap.isEmpty() ) {
			for ( Long key : deliveryItemAllMap.keySet() ) {
				
				DeliveryItem deliveryItem = deliveryItemAllMap.get(key);
				if( 
						deliveryItem.getStatus() == DeliveryItemStatus.Unlink || 
						deliveryItem.getStatus() == DeliveryItemStatus.Deleted ) 
				{
					continue;
				}
				
				String exception = checkManager.getProblemItemException(deliveryItem);
				
				if ( StringUtils.isNotEmpty(exception) ) {
					DeliveryProblemItem deliveryProblemItem = new DeliveryProblemItem();
					convertDeliveryProblemItem(deliveryProblemItem, deliveryItem, exception);
					if(deliveryItem.getStatus() != DeliveryItemStatus.KeepRecord){
						checkDelivery.getDeliveryProblemItemList().add(deliveryProblemItem);
					}
					
				}
			}
			
			checkDelivery.setDeliveryProblemItemList(markDayEndDeliveryProblemItem(checkDelivery.getDeliveryProblemItemList()));
		}
		
		if ( !checkDelivery.getDeliveryProblemItemList().isEmpty() ) {
			Collections.sort(checkDelivery.getDeliveryProblemItemList(), new DeliveryProblemItemComparator());
			return retrieveDrugCheckViewListSummary();
		} else if ( checkDelivery.getDeliveryId() != null ){
			return updateDeliveryStatusToChecked(checkDelivery, false);
		} else {
			resultMsg = "0606"; //Record not founnd.
			return retrieveDrugCheckViewListSummary();
		}
	}
	
	private void validDeliveryStatusForCheck(Delivery delivery){
		
		if(delivery.getStatus() == DeliveryStatus.KeepRecord || delivery.getStatus() == DeliveryStatus.Deleted){
			resultMsg = "0637";
			return;
		}
		
		if ( delivery.getStatus() != DeliveryStatus.Printed &&  delivery.getStatus() != DeliveryStatus.Assigning) {
			resultMsg = "0215";//Batch has been checked.
			return;
		}
	}
		
	private void convertDeliveryProblemItem(DeliveryProblemItem deliveryProblemItem, DeliveryItem deliveryItem, String exception) {
		
		JaxbWrapper<Object> labelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);

		deliveryProblemItem.setDeliveryItemId(deliveryItem.getId());
		deliveryProblemItem.setCaseNum(deliveryItem.getMedProfile().getMedCase().getCaseNum());
		deliveryProblemItem.setException(exception);
		deliveryProblemItem.setItemCode(deliveryItem.getItemCode());
		deliveryProblemItem.setIssueQty(deliveryItem.getIssueQty());

		MedProfilePoItem medProfilePoItem = deliveryItem.resolveMedProfilePoItem();
		
		if (deliveryItem.getPivasWorklist() != null) {
			deliveryProblemItem.setBaseUnit("DOSE");
		} else if (medProfilePoItem != null) {
			deliveryProblemItem.setDrugName(medProfilePoItem.getDrugName());
			deliveryProblemItem.setFormLabelDesc(medProfilePoItem.getFormLabelDesc());
			deliveryProblemItem.setStrength(medProfilePoItem.getStrength());
			deliveryProblemItem.setVolumeText(medProfilePoItem.getVolumeText());
			
			deliveryProblemItem.setBaseUnit(medProfilePoItem.getBaseUnit());
			deliveryProblemItem.setDoseUnit(medProfilePoItem.getDoseUnit());
		}
		
		Object label = labelJaxbWrapper.unmarshall(deliveryItem.getLabelXml());
		if (label instanceof MpDispLabel) {
			deliveryProblemItem.setWardCode(((MpDispLabel) label).getWard());
			deliveryProblemItem.setBedNum(((MpDispLabel) label).getBedNum());
		} else if (label instanceof LabelContainer){
			deliveryProblemItem.setItemDescTlf(((LabelContainer) label).getPivasProductLabel().getDeliveryItemDescTlf());
			deliveryProblemItem.setWardCode(((LabelContainer) label).getPivasProductLabel().getWard());
			deliveryProblemItem.setBedNum(((LabelContainer) label).getPivasProductLabel().getBedNum());
		}
		
		Patient patient = deliveryItem.getMedProfile().getPatient();
		if(patient.getNameChi() == null || patient.getNameChi().isEmpty()){
			deliveryProblemItem.setPatientName(patient.getName());
			deliveryProblemItem.setHavePatientChiName(false);
		}else{
			deliveryProblemItem.setPatientName(patient.getNameChi());
			deliveryProblemItem.setHavePatientChiName(true);
		}
	

		if(deliveryItem.getStatus() == DeliveryItemStatus.KeepRecord){
			deliveryProblemItem.setKeepRecorded(true);
		}
		if(deliveryItem.getStatus() == DeliveryItemStatus.Unlink){
			deliveryProblemItem.setMarkUnLink(true);
		}
		if(deliveryItem.getStatus() == DeliveryItemStatus.Deleted){
			deliveryProblemItem.setMarkDelete(true);
			deliveryProblemItem.setMarkUnLink(true);
			
		}
		if(!(deliveryItem.getStatus() == DeliveryItemStatus.Unlink || deliveryItem.getStatus() == DeliveryItemStatus.Deleted)){
			deliveryProblemItem.setBatchCode(dfForBatchPrefix.format(deliveryItem.getDelivery().getBatchDate())+deliveryItem.getDelivery().getBatchNum());
		}else{
			deliveryProblemItem.setBatchCode("");
		}
		
		
		if(deliveryItem.getStatus() == DeliveryItemStatus.Checked || deliveryItem.getStatus() == DeliveryItemStatus.Delivered){
			deliveryProblemItem.setKeepRecorded(true);
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private HashMap<Long,DeliveryItem> retrieveDeliveryItemList(Date batchDate, String batchNum) {
		
		
		HashMap<Long,DeliveryItem>  resultMap = new HashMap<Long,DeliveryItem> ();
		List<DeliveryItem> resultList = em.createQuery(
				"select o from DeliveryItem o" + // 20120317 index check : Delivery.hospital,batchDate,batchNum : I_DELIVERY_01
				" where o.delivery.hospital = :hospital" +
				" and o.delivery.batchDate = :batchDate" +
				" and o.delivery.batchNum = :batchNum" +
				" and o.delivery.deliveryRequest.status = :deliveryRequestStatus" +
				" order by o.medProfile.medCase.caseNum," +
				" o.delivery.wardCode," +
				" o.medProfile.medCase.pasBedNum," +
				" o.itemCode")
				.setParameter("hospital", workstore.getHospital())
				.setParameter("batchDate", batchDate, TemporalType.DATE)
				.setParameter("batchNum", batchNum)
				.setParameter("deliveryRequestStatus", DeliveryRequestStatus.Completed)
				.getResultList();
		
		for (DeliveryItem di : resultList){
			resultMap.put(di.getId(), di);
			
		}
		
		return resultMap;
	}
	
	public DrugCheckViewListSummary updateDeliveryStatusToChecked(CheckDelivery inCheckOrder, boolean printRptFlag) {
		
		List<DeliveryItem> deliveryItemWithCheckedStatusList = new ArrayList<DeliveryItem>();
		Delivery deliveryToUpdate = em.find(Delivery.class, inCheckOrder.getDeliveryId());
		resultMsg = StringUtils.EMPTY;
		HashMap<Long,String> aduitDeliveryItemMap = new HashMap<Long,String> ();
		boolean printLabelFlag = false;
		
		if (inCheckOrder.getDeliveryVersion() != null && inCheckOrder.getDeliveryVersion().compareTo(deliveryToUpdate.getVersion())!=0) {
			throw new OptimisticLockException();
		}
		
		if ( inCheckOrder.getDeliveryProblemItemList().isEmpty() ) {
			int unlinkDeleteCount = 0;
			int keepRecordCount = 0;
			deliveryToUpdate.setCheckDate(new Date());
			for ( DeliveryItem deliveryItem : deliveryToUpdate.getDeliveryItemList() ) {
				
				String previousDisplayValue = deliveryItem.getStatus().getDataValue();
				
				if( deliveryItem.getStatus() != DeliveryItemStatus.KeepRecord && 
						deliveryItem.getStatus() != DeliveryItemStatus.Unlink && 
						deliveryItem.getStatus() != DeliveryItemStatus.Deleted ) 
				{
					
					deliveryItem.setStatus(DeliveryItemStatus.Checked);
					printLabelFlag = true;
					deliveryItemWithCheckedStatusList.add(deliveryItem);
				}else if (deliveryItem.getStatus() == DeliveryItemStatus.Unlink || 
						deliveryItem.getStatus() == DeliveryItemStatus.Deleted ){
					unlinkDeleteCount++;
				}else if(deliveryItem.getStatus() == DeliveryItemStatus.KeepRecord){
					keepRecordCount++;
				}
				
				aduitDeliveryItemMap.put(deliveryItem.getId(), previousDisplayValue+"=>"+deliveryItem.getStatus().getDataValue());
				
			}
		
			
			
			batchCode = deliveryToUpdate.getBatchNum();
			batchDate = deliveryToUpdate.getBatchDate().toString();
			
			
			String previousDeliveryStatus = deliveryToUpdate.getStatus().getDataValue();
			
			if(unlinkDeleteCount == deliveryToUpdate.getDeliveryItemList().size()){
				deliveryToUpdate.setStatus(DeliveryStatus.Deleted);
				resultMsg = "0212";
			}else if(keepRecordCount == deliveryToUpdate.getDeliveryItemList().size()){
				deliveryToUpdate.setStatus(DeliveryStatus.KeepRecord);
				resultMsg = "0637";
			}else{
				deliveryToUpdate.setStatus(DeliveryStatus.Checked);
				resultMsg = "0210";
			}
			
			
			if ( printLabelFlag ) {
				printAgent.renderAndPrint(labelManager.printDeliveryLabel(deliveryToUpdate, false));
			}
			em.flush();
			
			if(autoUpdate){
				sendService.dispatchUpdateEvent(deliveryToUpdate.getDeliveryRequest().getWorkstore());
			}
			//No problem
			auditLogger.log("#0223",  
					df.format(deliveryToUpdate.getBatchDate())+ deliveryToUpdate.getBatchNum()+"="+previousDeliveryStatus+"=>"+deliveryToUpdate.getStatus().getDataValue(),aduitDeliveryItemMap);
			
			
			
			//Send message to corp
			if(deliveryItemWithCheckedStatusList != null && deliveryItemWithCheckedStatusList.size() > 0){
				checkManager.sendMsgToCmsFromCheckedDeliveryItemList(deliveryItemWithCheckedStatusList);
				updateFcsSfiInvoiceStatus(deliveryItemWithCheckedStatusList);
			}
			return retrieveDrugCheckViewListSummary();
		} 
		
		Map<Long, DeliveryProblemItem> deliveryProblemItemMarkUnlinkMap = new HashMap<Long, DeliveryProblemItem>();
		Map<Long, DeliveryProblemItem> deliveryProblemItemMarkDeleteMap = new HashMap<Long, DeliveryProblemItem>();
		for ( DeliveryProblemItem deliveryProblemItem : inCheckOrder.getDeliveryProblemItemList() ) {
			if ( deliveryProblemItem.isMarkUnLink() ) {
				deliveryProblemItemMarkUnlinkMap.put(deliveryProblemItem.getDeliveryItemId(), deliveryProblemItem);
			} 

			if(deliveryProblemItem.isMarkDelete()){
				deliveryProblemItemMarkDeleteMap.put(deliveryProblemItem.getDeliveryItemId(), deliveryProblemItem);
			}

		}
		
		boolean unlinkDeletedItem = false;
		boolean deleteItemExist = false;
		int keepRecordCount = 0;
		int unlinkDeleteCount = 0;
		List<Long> deleteDeliveryIdList = new ArrayList<Long>();
		
		Map<Long, DeliveryItem> deleteDeliveryItemMap = new HashMap<Long, DeliveryItem>();
		
		for ( DeliveryItem deliveryItem : deliveryToUpdate.getDeliveryItemList() ) {
			
			String previousDisplayValue = deliveryItem.getStatus().getDataValue();
			
			if(	deliveryItem.getStatus() != DeliveryItemStatus.Deleted ) 
			{
			
				if ( deliveryProblemItemMarkDeleteMap.containsKey(deliveryItem.getId()) ) {
					deleteDeliveryItemMap.put(deliveryItem.getId(), deliveryItem);
					deleteDeliveryIdList.add(deliveryItem.getId());
					deliveryItem.setStatus(DeliveryItemStatus.Deleted);
					
					
					deleteItemExist = true;
					unlinkDeleteCount++;
					unlinkDeletedItem = true;
				}
				
				if ( deliveryProblemItemMarkUnlinkMap.containsKey(deliveryItem.getId()) && !deliveryProblemItemMarkDeleteMap.containsKey(deliveryItem.getId())) {
					deliveryItem.setStatus(DeliveryItemStatus.Unlink);
					unlinkDeletedItem = true;
					unlinkDeleteCount++;
				} 
				
				if(deliveryItem.getStatus() == DeliveryItemStatus.KeepRecord  ){
					keepRecordCount++;
				}
				
				if( !(deliveryItem.getStatus() == DeliveryItemStatus.KeepRecord || deliveryItem.getStatus() == DeliveryItemStatus.Deleted || deliveryItem.getStatus() == DeliveryItemStatus.Unlink ) ){
					if(  !(deliveryProblemItemMarkUnlinkMap.containsKey(deliveryItem.getId()) ||  deliveryProblemItemMarkDeleteMap.containsKey(deliveryItem.getId()))) {
						deliveryItem.setStatus(DeliveryItemStatus.Checked);
						deliveryItemWithCheckedStatusList.add(deliveryItem);
						printLabelFlag = true;
					}
				}
				
				
			}
			
			aduitDeliveryItemMap.put(deliveryItem.getId(),previousDisplayValue+"=>"+ deliveryItem.getStatus().getDataValue());
			
			
		}
		//Send message to corp
		if(deliveryItemWithCheckedStatusList != null && deliveryItemWithCheckedStatusList.size() > 0){
			checkManager.sendMsgToCmsFromCheckedDeliveryItemList(deliveryItemWithCheckedStatusList);
			updateFcsSfiInvoiceStatus(deliveryItemWithCheckedStatusList);
		}
		
		if(deleteItemExist){
			deleteDeliveryItemToCorp(deleteDeliveryIdList, deleteDeliveryItemMap);
		}
		
		
		String previousDeliveryStatus = deliveryToUpdate.getStatus().getDataValue();
		
		if( keepRecordCount > 0 && (keepRecordCount + unlinkDeleteCount ) ==  deliveryToUpdate.getDeliveryItemList().size() ){
			//all record is keep record 
			deliveryToUpdate.setStatus(DeliveryStatus.KeepRecord);
			deliveryToUpdate.setUpdateDate(new Date());
			resultMsg = "0637";
			
		}else if( unlinkDeleteCount ==  deliveryToUpdate.getDeliveryItemList().size()){
			deliveryToUpdate.setStatus(DeliveryStatus.Deleted);
			deliveryToUpdate.setUpdateDate(new Date());
			resultMsg = "0212";
			
		}else{
			
			deliveryToUpdate.setStatus(DeliveryStatus.Checked);
			deliveryToUpdate.setCheckDate(new Date());
			
			batchCode = deliveryToUpdate.getBatchNum();
			batchDate = deliveryToUpdate.getBatchDate().toString();
			
			if ( unlinkDeletedItem ) {
				//for transaction report to check update
				deliveryToUpdate.setModifyDate(new Date());
				resultMsg = "0212";
			} else {
				resultMsg = "0210";
			}
			
			if ( printLabelFlag && printRptFlag ) {
				
				Delivery deliveryToReport = new Delivery();
				deliveryToReport.setBatchDate(deliveryToUpdate.getBatchDate());
				deliveryToReport.setBatchNum(deliveryToUpdate.getBatchNum());
				deliveryToReport.setCheckDate(deliveryToUpdate.getCheckDate());
				deliveryToReport.setCreateDate(deliveryToUpdate.getCreateDate());
				deliveryToReport.setCreateUser(deliveryToUpdate.getCreateUser());
				deliveryToReport.setDeliveryDate(deliveryToUpdate.getDeliveryDate());
				deliveryToReport.setDeliveryRequest(deliveryToUpdate.getDeliveryRequest());
				deliveryToReport.setErrorFlag(deliveryToUpdate.getErrorFlag());
				deliveryToReport.setHospital(deliveryToUpdate.getHospital());
				deliveryToReport.setId(deliveryToUpdate.getId());
				deliveryToReport.setModifyDate(deliveryToUpdate.getModifyDate());
				deliveryToReport.setNumOfLabel(deliveryToUpdate.getNumOfLabel());
				deliveryToReport.setPrintMode(deliveryToUpdate.getPrintMode());
				deliveryToReport.setStatus(deliveryToUpdate.getStatus());
				deliveryToReport.setType(deliveryToUpdate.getType());
				deliveryToReport.setUpdateDate(deliveryToUpdate.getUpdateDate());
				deliveryToReport.setUpdateUser(deliveryToUpdate.getUpdateUser());
				deliveryToReport.setWardCode(deliveryToUpdate.getWardCode());
				for(DeliveryItem di : deliveryToUpdate.getDeliveryItemList()){
						deliveryToReport.getDeliveryItemList().add(di);
				}
				mpTrxRptPrintManager.reprintMpTrxRpt(deliveryToReport);
			}
			
			if ( printLabelFlag ) {
				printAgent.renderAndPrint(labelManager.printDeliveryLabel(deliveryToUpdate, false));
			}
			
		}
		
		if(autoUpdate){
			sendService.dispatchUpdateEvent(deliveryToUpdate.getDeliveryRequest().getWorkstore());
		}
		
		auditLogger.log("#0676",   df.format(deliveryToUpdate.getBatchDate())+ 
				deliveryToUpdate.getBatchNum()+"="+previousDeliveryStatus+"=>"+deliveryToUpdate.getStatus().getDataValue(), aduitDeliveryItemMap.toString(), printRptFlag);
		
		return retrieveDrugCheckViewListSummary();
	}
	
	public static class DeliveryProblemItemComparator implements Comparator<DeliveryProblemItem> , Serializable  {
		private static final long serialVersionUID = 1L;
		@Override
		public int compare(DeliveryProblemItem item1, DeliveryProblemItem item2) {
			int compareResult = 0;
			
			if(item1.isMarkUnLink() && !item2.isMarkUnLink()){return -1;}
			else if(item2.isMarkUnLink() && !item1.isMarkUnLink()){return 1;}
			else{
				compareResult = new CompareToBuilder().append( item1.getCaseNum(), item2.getCaseNum() )
				 .append( item1.getWardCode(), item2.getWardCode() )
				 .append( item1.getBedNum(), item2.getBedNum() )
				 .append( item1.getBatchCode(), item2.getBatchCode() )
				 .append( item1.getItemCode(), item2.getItemCode() )
				 .append( item1.getIssueQty(), item2.getIssueQty() )
				 .toComparison();
			}
			return compareResult;
		}		
	}

	private List<DeliveryProblemItem> markDayEndDeliveryProblemItem(List<DeliveryProblemItem> inDeliveryProblmmItemList){		
		if ( inDeliveryProblmmItemList.isEmpty() ) {
			return null;
		}
		
		List<Long> deliveryItemIdList = new ArrayList<Long>();
		boolean lastSplitEntry = false;
		int counterSplit = 0;
		
		List<DispOrderItem> dispOrderItemList = new ArrayList<DispOrderItem>();
			
		for ( DeliveryProblemItem deliveryProblemItem : inDeliveryProblmmItemList) {
			if( counterSplit < QUERY_CHUNK_SIZE){
				deliveryItemIdList.add(deliveryProblemItem.getDeliveryItemId());
				counterSplit++;
				lastSplitEntry = true;
			}else{
				deliveryItemIdList.add(deliveryProblemItem.getDeliveryItemId());
				List<DispOrderItem> dispOrderItemListSplited = corpPmsServiceProxy.retrieveDispOrderItemListByDeliveryItemId(deliveryItemIdList);
				dispOrderItemList.addAll(dispOrderItemListSplited);
				deliveryItemIdList.clear();
				counterSplit = 0;
				lastSplitEntry = false;
			}
		}
		
		if(lastSplitEntry){
			List<DispOrderItem> dispOrderItemListSplited = corpPmsServiceProxy.retrieveDispOrderItemListByDeliveryItemId(deliveryItemIdList);
			dispOrderItemList.addAll(dispOrderItemListSplited);
		}
		
		if ( !dispOrderItemList.isEmpty() ) {
			for (Iterator<DeliveryProblemItem> itr = inDeliveryProblmmItemList.iterator();itr.hasNext();) {
				DeliveryProblemItem deliveryProblemItem = itr.next();
				
				boolean dayEndFlag = true;
				for (DispOrderItem dispOrderItem : dispOrderItemList) {
					if ( deliveryProblemItem.getDeliveryItemId().equals(dispOrderItem.getDeliveryItemId()) ) {
						deliveryProblemItem.setDayendProcess(false);
						dayEndFlag = false;
					}
				}
				if ( dayEndFlag ) {
					deliveryProblemItem.setDayendProcess(true);
				}
				
			}
		} else {
			for (Iterator<DeliveryProblemItem> itr = inDeliveryProblmmItemList.iterator();itr.hasNext();) {
				DeliveryProblemItem deliveryProblemItem = itr.next();
				deliveryProblemItem.setDayendProcess(true);
			}
		}
		
		return inDeliveryProblmmItemList;
	}
	
	@SuppressWarnings("unchecked")
	private void deleteDeliveryItemToCorp(List<Long> deleteDeliveryItemIdList, Map<Long,DeliveryItem> deliveryItemMap){
		
		if ( !deleteDeliveryItemIdList.isEmpty() ) {
			
			List<DeliveryItem> deleteDeliveryItemList = em.createQuery(
					"select o from DeliveryItem o" + // 20120214 index check : DeliveryItem.id : PK_DELIVERY_ITEM
					" where o.id in :deliveryItemIdList")
					.setParameter("deliveryItemIdList", deleteDeliveryItemIdList)
					.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
					.getResultList();
			
			for ( DeliveryItem deleteDeliveryItem : deleteDeliveryItemList ) {					
				if (!deliveryItemMap.get(deleteDeliveryItem.getId()).getVersion().equals( deleteDeliveryItem.getVersion())) {
					throw new OptimisticLockException();
				}										
			}
			 
			boolean updateDispOrderItem = corpPmsServiceProxy.updateDispOrderItemStatusToDeleted(deleteDeliveryItemIdList, CDDH_LEGACY_PERSISTENCE_ENABLED.get(false));
			
			if ( updateDispOrderItem ) {
				for ( DeliveryItem deleteDeliveryItem : deleteDeliveryItemList ) {		
					deleteDeliveryItem.setStatus(DeliveryItemStatus.Deleted);
					
				}
			} else {
				throw new OptimisticLockException();
			}
		}
		
	}
	
	private void updateFcsSfiInvoiceStatus(List<DeliveryItem> deliveryItemList) {
		if( !StringUtils.equals(IP_DRUG_CHECK, CHARGING_SFI_CHECKISSUE_MP_ISSUEINVOICE.get()) ){
			return;
		}
		
		List<String> invoiceNumList = new ArrayList<String>();
		for ( DeliveryItem deliveryItem : deliveryItemList ) {
			PurchaseRequest purchaseRequest = deliveryItem.getPurchaseRequest();
			if ( isActiveInvoice(purchaseRequest) ) {
				invoiceNumList.add(purchaseRequest.getInvoiceNum());
			}
		}
		if ( !invoiceNumList.isEmpty() ) {
			corpPmsServiceProxy.updateFcsSfiInvoiceStatus(invoiceNumList);
		}
	}
	
	private boolean isActiveInvoice(PurchaseRequest purchaseRequest){
		if ( purchaseRequest == null ) {
			return false;
		}
		return (purchaseRequest.getInvoiceStatus() != InvoiceStatus.Void && purchaseRequest.getInvoiceStatus() != InvoiceStatus.SysDeleted);
	}
	
	// PMS-6050 
	// refresh front-end lists before checkServiceBean call end
	private DrugCheckViewListSummary retrieveDrugCheckViewListSummary() {
		return checkListManager.retrieveDrugCheckViewListSummary();
	}
	
	public String getResultMsg() {
		return resultMsg;
	}

	public String getBatchCode() {
		return batchCode;
	}
	
	public String getBatchDate(){
		
		return batchDate;
	} 
	
	@Remove
	public void destroy() {
		if ( checkDelivery != null ) {
			checkDelivery = null;
		}
	}
}
