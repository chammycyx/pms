package hk.org.ha.model.pms.persistence.disp;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.disp.InvoiceDocType;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.PrivateOwned;


@Entity
@Table(name = "INVOICE")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Invoice")
public class Invoice extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "invoiceSeq")
	@SequenceGenerator(name = "invoiceSeq", sequenceName = "SQ_INVOICE", initialValue = 100000000)
	@XmlElement
	private Long id;
	
	@Column(name = "INVOICE_DATE")
	@Temporal(TemporalType.DATE)
	@XmlElement
	private Date invoiceDate;
	
	@Column(name = "INVOICE_NUM", nullable = false, length = 15)
	@XmlElement
	private String invoiceNum;

	@Column(name = "TOTAL_AMOUNT", nullable = false, precision = 19, scale = 0)
	@XmlElement
	private BigDecimal totalAmount;
	
	@Column(name = "RECEIVE_AMOUNT", precision = 19, scale = 0)
	@XmlElement
	private BigDecimal receiveAmount;
	
	@Column(name = "FORCE_PROCEED_FLAG", nullable = false)
	@XmlTransient
	private Boolean forceProceedFlag;
	
	@Column(name = "FORCE_PROCEED_CODE", length = 12)
	@XmlTransient
	private String forceProceedCode;
	
	@Column(name = "FORCE_PROCEED_USER", length = 12)
	@XmlTransient
	private String forceProceedUser;
	
	@Column(name = "FORCE_PROCEED_DATE")
	@Temporal(TemporalType.DATE)
	@XmlTransient
	private Date forceProceedDate;

	@Column(name = "RECEIPT_NUM", length = 20)
	@XmlTransient
	private String receiptNum;
	
	@Converter(name = "Invoice.docType", converterClass = InvoiceDocType.Converter.class)
	@Convert("Invoice.docType")
	@XmlElement
	@Column(name="DOC_TYPE", nullable = false, length = 1)
	private InvoiceDocType docType;

	@Converter(name = "Invoice.status", converterClass = InvoiceStatus.Converter.class)
    @Convert("Invoice.status")
	@Column(name="STATUS", nullable = false, length = 1)
	@XmlElement
	private InvoiceStatus status;
	
	@Column(name = "FCS_CHECK_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	@XmlElement
	private Date fcsCheckDate;

	@Column(name = "PREV_INVOICE_ID")
	@XmlElement
	private Long prevInvoiceId;
	
	@Column(name = "ORG_INVOICE_ID")
	@XmlElement
	private Long orgInvoiceId;
	
	@Column(name = "MANUAL_INVOICE_NUM", length = 15)
	@XmlTransient
	private String manualInvoiceNum;
	
	@Column(name = "MANUAL_RECEIPT_NUM", length = 20)
	@XmlTransient
	private String manualReceiptNum;
	
    @PrivateOwned
    @OneToMany(mappedBy="invoice", cascade=CascadeType.ALL)
    @XmlElement(name="InvoiceItem")
	private List<InvoiceItem> invoiceItemList;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "DISP_ORDER_ID", nullable = false)
    @XmlTransient
    private DispOrder dispOrder;

	@Column(name= "PRINT_FLAG", nullable = false)
	@XmlElement
	private Boolean printFlag;
	
	@Column(name = "HKID", length = 12)
	@XmlElement
	private String hkid;
	
    @Column(name = "CHARGE_ORDER_NUM", length = 12)
    @XmlElement
	private String chargeOrderNum;
	
    @ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE", referencedColumnName = "HOSP_CODE", nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
    @XmlTransient
	private Workstore workstore;
    
	@Column(name = "HOSP_CODE", nullable = false, insertable = false, updatable = false)
	@XmlElement
	private String hospCode;
	
	@Converter(name = "Invoice.orderType", converterClass = OrderType.Converter.class)
    @Convert("Invoice.orderType")
	@Column(name="ORDER_TYPE", nullable = false, length = 1)
	@XmlElement
	private OrderType orderType;
    
    @Transient
    @XmlTransient
    private String patHospCode;
    
	public Invoice() {
		docType = InvoiceDocType.Standard;
		printFlag = Boolean.FALSE;
		forceProceedFlag = Boolean.FALSE;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getInvoiceNum() {
		return invoiceNum;
	}

	public void setInvoiceNum(String invoiceNum) {
		this.invoiceNum = invoiceNum;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getReceiveAmount() {
		return receiveAmount;
	}

	public void setReceiveAmount(BigDecimal receiveAmount) {
		this.receiveAmount = receiveAmount;
	}

	public Boolean getForceProceedFlag() {
		return this.forceProceedFlag;
	}

	public void setForceProceedFlag(Boolean forceProceedFlag) {
		this.forceProceedFlag = forceProceedFlag;
	}

	public String getForceProceedCode() {
		return forceProceedCode;
	}

	public void setForceProceedCode(String forceProceedCode) {
		this.forceProceedCode = forceProceedCode;
	}

	public String getForceProceedUser() {
		return forceProceedUser;
	}

	public void setForceProceedUser(String forceProceedUser) {
		this.forceProceedUser = forceProceedUser;
	}

	public Date getForceProceedDate() {
		if (forceProceedDate == null) {
			return null;
		}
		else {
			return new Date(forceProceedDate.getTime());
		}
	}

	public void setForceProceedDate(Date forceProceedDate) {
		if (forceProceedDate == null) {
			this.forceProceedDate = null;
		}
		else {
			this.forceProceedDate = new Date(forceProceedDate.getTime());
		}
	}

	public String getReceiptNum() {
		return receiptNum;
	}

	public void setReceiptNum(String receiptNum) {
		this.receiptNum = receiptNum;
	}

	public InvoiceDocType getDocType() {
		return docType;
	}

	public void setDocType(InvoiceDocType docType) {
		this.docType = docType;
	}

	public InvoiceStatus getStatus() {
		return status;
	}

	public void setStatus(InvoiceStatus status) {
		this.status = status;
	}

	public Date getFcsCheckDate() {
		if (fcsCheckDate == null) {
			return null;
		}
		else {
			return new Date(fcsCheckDate.getTime());
		}
	}

	public void setFcsCheckDate(Date fcsCheckDate) {
		if (fcsCheckDate == null) {
			this.fcsCheckDate = null;
		}
		else {
			this.fcsCheckDate = new Date(fcsCheckDate.getTime());
		}
	}

	public Long getPrevInvoiceId() {
		return prevInvoiceId;
	}

	public void setPrevInvoiceId(Long prevInvoiceId) {
		this.prevInvoiceId = prevInvoiceId;
	}

	public Long getOrgInvoiceId() {
		return orgInvoiceId;
	}

	public void setOrgInvoiceId(Long orgInvoiceId) {
		this.orgInvoiceId = orgInvoiceId;
	}

	public String getManualInvoiceNum() {
		return manualInvoiceNum;
	}

	public void setManualInvoiceNum(String manualInvoiceNum) {
		this.manualInvoiceNum = manualInvoiceNum;
	}

	public String getManualReceiptNum() {
		return manualReceiptNum;
	}

	public void setManualReceiptNum(String manualReceiptNum) {
		this.manualReceiptNum = manualReceiptNum;
	}

	public List<InvoiceItem> getInvoiceItemList() {
		if (invoiceItemList == null) {
			invoiceItemList = new ArrayList<InvoiceItem>();
		}
		return invoiceItemList;
	}

	public void setInvoiceItemList(List<InvoiceItem> invoiceItemList) {
		this.invoiceItemList = invoiceItemList;
	}

	public void addInvoiceItem(InvoiceItem invoiceItem) {
		invoiceItem.setInvoice(this);
		getInvoiceItemList().add(invoiceItem);
	}
	
	public void clearInvoiceItemList() {
		invoiceItemList = new ArrayList<InvoiceItem>();
	}

	public DispOrder getDispOrder() {
		return dispOrder;
	}

	public void setDispOrder(DispOrder dispOrder) {
		this.dispOrder = dispOrder;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public String getPatHospCode() {
		return patHospCode;
	}
	
	public void setPrintFlag(Boolean printFlag) {
		this.printFlag = printFlag;
	}

	public Boolean getPrintFlag() {
		return printFlag;
	}

	@Transient
	private transient boolean childLoaded = false;
	
	public void loadChild() {
		loadChild(false);
	}
	
	public void loadChild(boolean includeDispOrder) {
		
		if (getId() == null) return;

		if (!childLoaded) {			
			childLoaded = true;

			DispOrder dispOrder = this.getDispOrder();
			if (dispOrder != null) {
				if ( includeDispOrder ){
					dispOrder.loadChild();
				}
				dispOrder.getDispOrderItemList().size();
			}						
	
			for (InvoiceItem invoiceItem : this.getInvoiceItemList()) {
				invoiceItem.getInvoice();
				if (invoiceItem.getDispOrderItem() != null) {
					invoiceItem.getDispOrderItem().addInvoiceItem(invoiceItem);
				}				
			}
		}
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getHkid() {
		return hkid;
	}

	public void setChargeOrderNum(String chargeOrderNum) {
		this.chargeOrderNum = chargeOrderNum;
	}

	public String getChargeOrderNum() {
		return chargeOrderNum;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}

	public OrderType getOrderType() {
		return orderType;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getHospCode() {
		return hospCode;
	}	
}
