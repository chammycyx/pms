package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class EdsWaitTimeRpt implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private EdsWaitTimeRptHdr edsWaitTimeRptHdr;	
	private List<EdsElapseTime> edsWaitTimeRptDtlList;
	public EdsWaitTimeRptHdr getEdsWaitTimeRptHdr() {
		return edsWaitTimeRptHdr;
	}
	public void setEdsWaitTimeRptHdr(EdsWaitTimeRptHdr edsWaitTimeRptHdr) {
		this.edsWaitTimeRptHdr = edsWaitTimeRptHdr;
	}
	public List<EdsElapseTime> getEdsWaitTimeRptDtlList() {
		return edsWaitTimeRptDtlList;
	}
	public void setEdsWaitTimeRptDtlList(
			List<EdsElapseTime> edsWaitTimeRptDtlList) {
		this.edsWaitTimeRptDtlList = edsWaitTimeRptDtlList;
	}
}
