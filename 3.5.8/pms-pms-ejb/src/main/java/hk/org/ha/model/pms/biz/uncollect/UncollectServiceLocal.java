package hk.org.ha.model.pms.biz.uncollect;

import java.util.Date;

import javax.ejb.Local;

@Local
public interface UncollectServiceLocal {
	
	void retrieveUncollectOrder(Date dispDate, String ticketNum);
	
	void destroy();
	
}