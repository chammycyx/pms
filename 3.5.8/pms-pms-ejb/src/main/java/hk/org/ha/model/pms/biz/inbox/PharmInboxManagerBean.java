package hk.org.ha.model.pms.biz.inbox;

import static hk.org.ha.model.pms.prop.Prop.CMM_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.INBOX_SHOWALLWARDS;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.persistence.reftable.WorkstationProp;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.joda.time.DateMidnight;

@AutoCreate
@Stateless
@Name("pharmInboxManager")
@MeasureCalls
public class PharmInboxManagerBean implements PharmInboxManagerLocal {

	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
    public PharmInboxManagerBean() {
    }
    
    @Override
    public void setShowAllWards(Workstation workstation, Boolean showAllWards) {
    	
    	if (workstation == null) {
    		return;
    	}

    	WorkstationProp prop = (WorkstationProp) generateFindShowAllWardsPropQuery(workstation)
    			.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
    			.getSingleResult();
    	
    	prop.setValue(Boolean.TRUE.equals(showAllWards) ? "Y" : "N");
    	prop.setUpdateDate(new Date());
    	em.merge(prop);
    }
    
    @SuppressWarnings("unchecked")
	@Override
    public Boolean getShowAllWards(Workstation workstation) {
    	
    	if (workstation == null) {
    		return Boolean.FALSE;
    	}
    	
    	List<WorkstationProp> propList = generateFindShowAllWardsPropQuery(workstation).getResultList();
    	
    	if (propList.isEmpty()) {
    		return Boolean.FALSE;
    	}
    	
    	WorkstationProp prop = propList.get(0);
    	
    	return new DateMidnight().compareTo(new DateMidnight(prop.getUpdateDate())) != 0 ?
    			Boolean.FALSE : (Boolean) prop.getConvertedValue(); 
    }

    @Override
    public Boolean getShowChemoProfileOnly(Workstation workstation, Boolean currentValue) {
    	
    	if (currentValue != null) {
    		return currentValue;
    	}
    	
    	if (workstation == null) {
    		return Boolean.FALSE;
    	}
    	
    	return CMM_ENABLED.get(workstation.getWorkstore().getHospital(), Boolean.FALSE) && Boolean.TRUE.equals(workstation.getChemoFlag());
    }
    
    private Query generateFindShowAllWardsPropQuery(Workstation workstation) {
    	
    	return em.createQuery(
    			"select o from WorkstationProp o" + // 20120604 index check : Prop.name : UI_PROP_01
    			" where o.workstation = :workstation" +
    			" and o.prop.name = :name")
    			.setParameter("workstation", workstation)
    			.setParameter("name", INBOX_SHOWALLWARDS.getName());    	
    }
}
