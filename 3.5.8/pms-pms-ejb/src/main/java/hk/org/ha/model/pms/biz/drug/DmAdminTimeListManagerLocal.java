package hk.org.ha.model.pms.biz.drug;

import javax.ejb.Local;

@Local
public interface DmAdminTimeListManagerLocal {
	
	void retrieveDmAdminTimeList(String prefixAdminTimeCode);
	
}
 