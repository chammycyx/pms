package hk.org.ha.model.pms.biz.charging;

import static hk.org.ha.model.pms.prop.Prop.CHARGING_SFI_DISCHARGEPRIVATEPATIENT_DURATION;
import static hk.org.ha.model.pms.prop.Prop.MEDPROFILE_PATIENT_PAS_PATIENT_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.PATIENT_PAS_PATIENT_ENABLED;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.inbox.ManualProfileManagerLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileManagerLocal;
import hk.org.ha.model.pms.biz.order.DispOrderConverterLocal;
import hk.org.ha.model.pms.biz.printing.InvoiceRendererLocal;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.vo.pms.DrugChargeInfo;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.InvoiceItem;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.medprofile.Delivery;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryRequest;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderPatType;
import hk.org.ha.model.pms.udt.medprofile.DeliveryItemStatus;
import hk.org.ha.model.pms.udt.medprofile.InactiveType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatus;
import hk.org.ha.model.pms.udt.rx.RxItemType;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.RegimenType;
import hk.org.ha.model.pms.vo.charging.DrugChargeRuleCriteria;
import hk.org.ha.model.pms.vo.charging.MpSfiInvoice;
import hk.org.ha.model.pms.vo.charging.MpSfiInvoiceInvoiceListInfo;
import hk.org.ha.model.pms.vo.charging.MpSfiInvoiceItem;
import hk.org.ha.model.pms.vo.charging.MpSfiInvoicePrivatePatInfo;
import hk.org.ha.model.pms.vo.charging.OverrideChargeAmountRuleCriteria;
import hk.org.ha.model.pms.vo.drug.DmDrugLite;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.model.pms.vo.label.MpDispLabel;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.rx.CapdItem;
import hk.org.ha.model.pms.vo.rx.CapdRxDrug;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.Freq;
import hk.org.ha.model.pms.vo.rx.OralRxDrug;
import hk.org.ha.model.pms.vo.rx.Regimen;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.security.PropMap;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("mpSfiInvoiceService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MpSfiInvoiceServiceBean implements MpSfiInvoiceServiceLocal {
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private Workstore workstore;

	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private ChargeCalculationInf chargeCalculation;
	
	@In
	private DispOrderConverterLocal dispOrderConverter;
	
	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;

	@In
	private InvoiceManagerLocal invoiceManager;
	
	@In
	private PrintAgentInf printAgent;
		
	@In
	private InvoiceRendererLocal invoiceRenderer;
		
	@In
	private MedProfileManagerLocal medProfileManager;
	
	@In
	private ManualProfileManagerLocal manualProfileManager;	
	
	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;
	
	@In
	private PropMap propMap;
	
	@Out(required=false)
	private List<MpSfiInvoicePrivatePatInfo> mpSfiInvoicePrivatePatInfoList;
	
	private Invoice mpSfiInvoiceCurrentInvoice;
	
	private String messageCode;
	
	private boolean success;
	
	private static MpSfiInvoiceItemComparator mpSfiInvoiceItemComparator = new MpSfiInvoiceItemComparator();
	private static final String JAXB_CONTEXT_LABEL = "hk.org.ha.model.pms.vo.label";
	private static JaxbWrapper<MpDispLabel> labelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
	
	public MpSfiInvoiceItem createMpSfiInvoiceItem(String itemCode){
		MpSfiInvoiceItem mpSfiInvoiceItem = null;
		WorkstoreDrug workstoreDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, itemCode);
		if ( workstoreDrug != null && workstoreDrug.getMsWorkstoreDrug() != null && workstoreDrug.getDmDrug() != null) {
			DmDrug dmDrug = workstoreDrug.getDmDrug();
			mpSfiInvoiceItem = new MpSfiInvoiceItem();
			mpSfiInvoiceItem.setItemCode(dmDrug.getItemCode());
			mpSfiInvoiceItem.setFullDrugDesc(dmDrug.getFullDrugDesc());
			mpSfiInvoiceItem.setDrugName(dmDrug.getDrugName());
			mpSfiInvoiceItem.setBaseUnit(dmDrug.getBaseUnit());
			mpSfiInvoiceItem.setItemSuspend(workstoreDrug.getMsWorkstoreDrug().isLocalSuspend());
			mpSfiInvoiceItem.setDrugAmount(0);
			
			DrugChargeInfo drugChargeInfo = dmDrug.getDrugChargeInfo();
			if( drugChargeInfo != null ){
				convertDrugChargeInfo(drugChargeInfo, dmDrug.getPmsFmStatus().getFmStatus(), mpSfiInvoiceItem);
			}
		}
		return mpSfiInvoiceItem;
	}
	
	public MpSfiInvoice retrieveDeliveryItemList(String hkid, String caseNum, Long medProfileId){
		clearMessageCode();
		
		//check patient exist in hkpmi
		Patient patient = this.getPasPatient(hkid, caseNum);
		if ( patient == null ) { 
			return null;
		}
		
		MpSfiInvoice mpSfiInvoice = new MpSfiInvoice();		
		
		//get the last medProfile id
		MedProfile medProfile = null;
		if ( medProfileId == null ) {
			List<MedProfile> medProfileList = retrieveMedProfile(hkid, caseNum);
			if ( !medProfileList.isEmpty() ) { 
				medProfile = medProfileList.get(0);
				medProfileId = medProfile.getId();
			}
		}
		
		if ( medProfileId != null ) {
			//retrieve delivery item
			List<DeliveryItem> deliveryItemList = retrieveDeliveryItemListForNewInvoice(hkid, caseNum, medProfileId);
			logger.debug("deliveryItemList size : #0", deliveryItemList.size());
			
			if ( !deliveryItemList.isEmpty() ) {
				medProfile = deliveryItemList.get(0).getMedProfilePoItem().getMedProfileMoItem().getMedProfileItem().getMedProfile();
				convertMpSfiInvoice(medProfile, mpSfiInvoice, patient);
				
				for ( DeliveryItem deliveryItem : deliveryItemList ) {
					WorkstoreDrug workstoreDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, deliveryItem.getMedProfilePoItem().getItemCode());
					if ( workstoreDrug == null || workstoreDrug.getMsWorkstoreDrug() == null || workstoreDrug.getDmDrug() == null) {
						logger.debug("workstoreDrug not found itemCode : #0", deliveryItem.getMedProfilePoItem().getItemCode());
						continue;
					}
					
					mpSfiInvoice.getMpSfiInvoiceItemList().add(convertMpSfiInvoiceItem(deliveryItem, workstoreDrug, new MpSfiInvoiceItem()));
				}
			} else {
				//convert mpsfiInvoice with empty item list
				medProfile = em.find(MedProfile.class, medProfileId);
				if ( medProfile == null ) {
					logger.debug("medProfile not found : #0", medProfileId);
					return null;
				}
				convertMpSfiInvoice(medProfile, mpSfiInvoice, patient);
			} 
			
		} else {
			//create medProfile
			medProfile = manualProfileManager.constructManualProfile(hkid, caseNum, workstore);
			convertMpSfiInvoice(medProfile, mpSfiInvoice, patient);
		}
		
		//allow to edit patient type if search item by HKID
		if ( StringUtils.isNotBlank(hkid) ) {
			mpSfiInvoice.setAllowPatTypeEdit(true);
		}
		
		Collections.sort(mpSfiInvoice.getMpSfiInvoiceItemList(), mpSfiInvoiceItemComparator);
		logger.debug("mpSfiInvoiceItemList size : #0", mpSfiInvoice.getMpSfiInvoiceItemList().size());
		return mpSfiInvoice;
	}
	
	public MpSfiInvoice retrieveMpSfiInvoice(String invoiceNum){
		this.clearMessageCode();
		MpSfiInvoice mpSfiInvoice = null;
		List<Invoice> invoiceList = corpPmsServiceProxy.retrieveInvoiceByInvoiceNumWoPurchaseRequest(invoiceNum, workstore.getHospCode());
		
		if ( invoiceList.isEmpty() ) {
			return mpSfiInvoice;
		}
		
		Invoice invoice = invoiceList.get(0);
		Map<Long, DeliveryItem> deliveryItemMap = this.prepareDeliveryItemMap(invoice.getInvoiceItemList());
		PharmOrder pharmOrder = invoice.getDispOrder().getPharmOrder();
		
		//set MpSfiInvoice
		mpSfiInvoice = new MpSfiInvoice();
		mpSfiInvoice.setInvoiceNum(invoice.getInvoiceNum());
		mpSfiInvoice.setInvoiceDate(invoice.getInvoiceDate());
		mpSfiInvoice.setStatus(invoice.getStatus());
		mpSfiInvoice.setMedCase(pharmOrder.getMedCase());
		mpSfiInvoice.setPatient(pharmOrder.getPatient());
		mpSfiInvoice.setMedProfile(new MedProfile());
		mpSfiInvoice.getMedProfile().setPatType(pharmOrder.getPatType());
		mpSfiInvoice.setMpSfiInvoiceItemList(new ArrayList<MpSfiInvoiceItem>());
		mpSfiInvoice.setDispOrderAdminStatus(invoice.getDispOrder().getAdminStatus());
		
		//set MpSfiInvoiceItem
		for ( InvoiceItem invoiceItem : invoice.getInvoiceItemList() ) {
			DispOrderItem dispOrderItem = invoiceItem.getDispOrderItem();
			PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
			DeliveryItem deliveryItem = deliveryItemMap.get(dispOrderItem.getDeliveryItemId());
			MpSfiInvoiceItem mpSfiInvoiceItem = new MpSfiInvoiceItem();
			
			mpSfiInvoiceItem.setIncludeItem(true);
			mpSfiInvoiceItem.setItemCode(pharmOrderItem.getItemCode());
			mpSfiInvoiceItem.setFullDrugDesc(pharmOrderItem.getFullDrugDesc());
			mpSfiInvoiceItem.setDrugName(pharmOrderItem.getDrugName());
			mpSfiInvoiceItem.setBaseUnit(pharmOrderItem.getBaseUnit());
			mpSfiInvoiceItem.setChargeQty(invoiceItem.getChargeQty());
			mpSfiInvoiceItem.setDrugAmount(invoiceItem.getAmount().intValue());
			if ( dispOrderItem.getDurationInDay() != null ) {
				mpSfiInvoiceItem.setIssueDuration(dispOrderItem.getDurationInDay().toString());
			}
			
			if ( deliveryItem != null ) {
				MpDispLabel mpDispLabel = labelJaxbWrapper.unmarshall(deliveryItem.getLabelXml());
				mpSfiInvoiceItem.setWardStockFlag(mpDispLabel.getWardStockFlag());
				mpSfiInvoiceItem.setDispQty(dispOrderItem.getDispQty());
				mpSfiInvoiceItem.setDeliveryItemId(deliveryItem.getId());
				mpSfiInvoiceItem.setDispDate(deliveryItem.getDelivery().getBatchDate());
				mpSfiInvoiceItem.setDeliveryItemStatus(deliveryItem.getStatus());
				mpSfiInvoiceItem.setEndDate(deliveryItem.getMedProfilePoItem().getEndDate());
			}
			
			WorkstoreDrug workstoreDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, pharmOrderItem.getItemCode());
			if ( workstoreDrug == null || workstoreDrug.getMsWorkstoreDrug() == null || workstoreDrug.getDmDrug() == null) {
				logger.debug("workstoreDrug not found itemCode : #0", pharmOrderItem.getItemCode());
				continue;
			}
			
			DmDrug dmDrug = workstoreDrug.getDmDrug();
			DrugChargeInfo drugChargeInfo = dmDrug.getDrugChargeInfo();
			if( drugChargeInfo != null ){
				convertDrugChargeInfo(drugChargeInfo, dmDrug.getPmsFmStatus().getFmStatus(), mpSfiInvoiceItem);
			}
			
			mpSfiInvoiceItem.setItemSuspend(workstoreDrug.getMsWorkstoreDrug().isLocalSuspend());
			mpSfiInvoice.getMpSfiInvoiceItemList().add(mpSfiInvoiceItem);
		} 
		
		Collections.sort(mpSfiInvoice.getMpSfiInvoiceItemList(), mpSfiInvoiceItemComparator);
		mpSfiInvoiceCurrentInvoice = invoice;
		return mpSfiInvoice;
	}
	
	public List<MpSfiInvoiceInvoiceListInfo> retrieveInvoiceList(String hkid, String caseNum) {
		List<MpSfiInvoiceInvoiceListInfo> mpSfiInvoiceListInfoList = new ArrayList<MpSfiInvoiceInvoiceListInfo>();
		List<Invoice> invoiceList = corpPmsServiceProxy.retrieveInvoiceListWoPurchaseRequest(hkid, caseNum, workstore.getHospCode());
		
		for ( Invoice invoice : invoiceList ) {
			MedCase medCase = invoice.getDispOrder().getPharmOrder().getMedCase();
			Patient patient = invoice.getDispOrder().getPharmOrder().getPatient();
			
			MpSfiInvoiceInvoiceListInfo mpSfiInvoiceListInfo = new MpSfiInvoiceInvoiceListInfo();
			mpSfiInvoiceListInfo.setCaseNum(medCase.getCaseNum());
			mpSfiInvoiceListInfo.setHkid(patient.getHkid());
			mpSfiInvoiceListInfo.setInvoiceDate(invoice.getInvoiceDate());
			mpSfiInvoiceListInfo.setInvoiceNum(invoice.getInvoiceNum());
			mpSfiInvoiceListInfo.setPatName(patient.getName());
			mpSfiInvoiceListInfo.setPayCode(medCase.getPasPayCode());
			mpSfiInvoiceListInfo.setStatus(invoice.getStatus());
			mpSfiInvoiceListInfo.setTotalAmount(invoice.getTotalAmount());
			mpSfiInvoiceListInfoList.add(mpSfiInvoiceListInfo);
		}
		
		return mpSfiInvoiceListInfoList;
	}
	
	@SuppressWarnings("unchecked")
	public void retrievePrivatePatList(){
		mpSfiInvoicePrivatePatInfoList = new ArrayList<MpSfiInvoicePrivatePatInfo>();
		List<MedProfile> medProfileList = em.createQuery(
				"select o from MedProfile o" + // 20150420 index check : MedProfile.hospCode,status,privateFlag : I_MED_PROFILE_08
				" where o.hospCode = :hospCode" +
				" and o.privateFlag = :privateFlag" +
				" and (o.status = :activeStatus or (o.status = :inactiveStatus" +
													" and (o.inactiveType is null or o.inactiveType = :inactiveType)" +
													" and o.dischargeDate between :startDate and :endDate))" +
				" order by o.lastInvoicePrintDate desc")
				.setParameter("hospCode", workstore.getHospCode())
				.setParameter("privateFlag", Boolean.TRUE)
				.setParameter("activeStatus", MedProfileStatus.Active)
				.setParameter("inactiveStatus", MedProfileStatus.Inactive)
				.setParameter("inactiveType", InactiveType.Discharge)
				.setParameter("startDate", new DateTime().minusDays(CHARGING_SFI_DISCHARGEPRIVATEPATIENT_DURATION.get(14)).toDate())
				.setParameter("endDate", new Date())
				.getResultList();
		for ( MedProfile medProfile : medProfileList ) {
			mpSfiInvoicePrivatePatInfoList.add(convertMpSfiInvoicePrivatePatInfo(medProfile, new MpSfiInvoicePrivatePatInfo()));
		}

		Collections.sort(mpSfiInvoicePrivatePatInfoList, new MpSfiInvoicePrivatePatInfoComparator());
	}  
	
	public void updateMpSfiInvoiceToIssued(MpSfiInvoice mpSfiInvoice){
		clearMessageCode();
		List<String> invoiceNumList = new ArrayList<String>();
		invoiceNumList.add(mpSfiInvoice.getInvoiceNum());
		corpPmsServiceProxy.updateFcsSfiInvoiceStatus(invoiceNumList);
		success = true;
	}
	
	@SuppressWarnings("unchecked")
	public void saveAndPrintInvoice(MpSfiInvoice mpSfiInvoice) {
		clearMessageCode();
		List<MpSfiInvoiceItem> inMpSfiInvoiceItemList = new ArrayList<MpSfiInvoiceItem>();
		for ( MpSfiInvoiceItem mpSfiInvoiceItem : mpSfiInvoice.getMpSfiInvoiceItemList() ) {
			if ( mpSfiInvoiceItem.isIncludeItem() ) {
				inMpSfiInvoiceItemList.add(mpSfiInvoiceItem);
			}
		}

		MedProfile medProfile = mpSfiInvoice.getMedProfile();
		//use paitent which retrieve from hkpmi
		medProfile.setPatient(mpSfiInvoice.getPatient());
		
		List<Long> deliveryItemIdList = new ArrayList<Long>();
		Map<Long, MpSfiInvoiceItem> mpSfiInvoiceItemMap = new HashMap<Long, MpSfiInvoiceItem>();
		List<DeliveryItem> newDeliveryItemList = new ArrayList<DeliveryItem>();
		for ( MpSfiInvoiceItem mpSfiInvoiceItem : inMpSfiInvoiceItemList ) {
			if ( mpSfiInvoiceItem.getDeliveryItemId() != null ) {
				deliveryItemIdList.add(mpSfiInvoiceItem.getDeliveryItemId());
				mpSfiInvoiceItemMap.put(mpSfiInvoiceItem.getDeliveryItemId(), mpSfiInvoiceItem);
			} else {
				newDeliveryItemList.add(createDeliveryItem(mpSfiInvoiceItem.getItemCode(), mpSfiInvoiceItem.getChargeQty(), medProfile));
			}
		}

		MedProfile medProfileInDB = null;
		if ( medProfile.getId() != null ) {
			//lock and check medprofile ProcessingFlag
			List<MedProfile> medProfileList = em.createQuery(
					"select m from MedProfile m" + // 20150420 index check : MedProfile.id : PK_MED_PROFILE
					" where m.id = :medProfileId")
					.setParameter("medProfileId", medProfile.getId())
					.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
					.getResultList();
			
			medProfileInDB = medProfileList.get(0);
			if ( Boolean.TRUE.equals(medProfileInDB.getProcessingFlag()) || Boolean.TRUE.equals(medProfileInDB.getDeliveryGenFlag()) ) {
				messageCode = "0705";	//This profile has been locked by another user.
				success = false;
				logger.debug("This profile has been locked by another user. medProfile id : #0", medProfileInDB.getId());
				return;
			}
		}
		
		//lock and check deliveryItem version
		List<DeliveryItem> deliveryItemList = new ArrayList<DeliveryItem>();
		if ( !deliveryItemIdList.isEmpty() ) {
			deliveryItemList = this.retrieveDeliveryItemListById(deliveryItemIdList, true);
			for ( DeliveryItem deliveryItem : deliveryItemList ) {
				MpSfiInvoiceItem mpSfiInvoiceItem = mpSfiInvoiceItemMap.get(deliveryItem.getId());
				if ( deliveryItem.getVersion().compareTo(mpSfiInvoiceItem.getDeliveryItemVersion()) != 0 ) {
					messageCode = "0307"; //Record has been modified by others.  Please retrieve the record again.
					success = false;
					logger.debug("delivery item has been updated by another user : #0", deliveryItem.getId());
					return; 
				}
			}
		}
		
		String chargeOrderNum = StringUtils.EMPTY;
		//update medProfile and DeliveryItem
		if ( medProfileInDB != null ) {
			medProfileInDB.setLastInvoicePrintDate(new Date());
			medProfileManager.saveChargeOrderNum(medProfileInDB);
			chargeOrderNum = medProfileInDB.getChargeOrderNum();
		} else {
			chargeOrderNum = corpPmsServiceProxy.retrieveChargeOrderNum(workstore.getHospCode(), workstore.getDefPatHospCode());;
		}
		
		for ( DeliveryItem deliveryItem : deliveryItemList ) {
			MpSfiInvoiceItem mpSfiInvoiceItem = mpSfiInvoiceItemMap.get(deliveryItem.getId());
			deliveryItem.setChargeQty(mpSfiInvoiceItem.getChargeQty());
		}

		//prepare delivery for corp
		Delivery delivery = this.createDelivery();
		delivery.setDeliveryItemList(deliveryItemList);
		delivery.getDeliveryItemList().addAll(newDeliveryItemList);
		setMedProfileChargeOrderNum(delivery.getDeliveryItemList(), chargeOrderNum);
		for ( DeliveryItem deliveryItem : deliveryItemList ) {
			deliveryItem.setMedProfile(deliveryItem.getMedProfilePoItem().getMedProfileMoItem().getMedProfileItem().getMedProfile());
		}
		List<DispOrder> dispOrderList = dispOrderConverter.convertDeliveryToDispOrderList(delivery, true, workstore);
		logger.debug("mpSfiInvoice ServiceBean - saveAndPrintInvoice dispOrderList size = #0", dispOrderList.size());
		DispOrder dispOrder = dispOrderList.get(0);
		dispOrder.getPharmOrder().setPatType(medProfile.getPatType());
		Invoice invoice  = dispOrder.getInvoiceList().get(0);

		//check if HA Staff
		boolean isHaStaff = !isExceptionDrugExist(invoice);
		
		if( !mpSfiInvoice.isIssueNewMpSfiInvoice() && !isHaStaff ){
			dispOrder.setAdminStatus(DispOrderAdminStatus.Suspended);
		}
		// prepare invoice print job list
		List<RenderAndPrintJob> printJobList = new ArrayList<RenderAndPrintJob>();
		if ( !isHaStaff ) {
			invoiceRenderer.prepareInvoicePrintJob(printJobList, dispOrder);
		}

		em.flush();
		
		//call corp
		dispOrder.getPharmOrder().clearDmInfo();
		corpPmsServiceProxy.saveDispOrderListWithVoidInvoiceListForIp(Arrays.asList(dispOrder), false, true, true, null, null);
		
		//print invoice
		if ( !isHaStaff ) {
			printAgent.renderAndPrint(printJobList);
		} else {
			messageCode = "0097";
		}
		success = true;
	}
	
	private void setMedProfileChargeOrderNum(List<DeliveryItem> deliveryItemList, String chargeOrderNum){
		for ( DeliveryItem deliveryItem : deliveryItemList ) {
			MedProfile medProfile = deliveryItem.getMedProfilePoItem().getMedProfileMoItem().getMedProfileItem().getMedProfile();
			if ( StringUtils.isBlank(medProfile.getChargeOrderNum()) ) {
				medProfile.setChargeOrderNum(chargeOrderNum);
			}
		}
	}
	
	public void reprintMpSfiInvoice() {
		//check if HA Staff
		if ( !isExceptionDrugExist(mpSfiInvoiceCurrentInvoice)) {
			messageCode = "0098";
			return;
		}
		
		invoiceRenderer.printSfiInvoice(mpSfiInvoiceCurrentInvoice, true);
	}
	
	public void clearScreen(){
		mpSfiInvoiceCurrentInvoice = null;
	}
	
	public void voidMpSfiInvoice(String invoiceNum, List<Long> deliveryItemIdList){
		List<DeliveryItem> deliveryItemList = this.retrieveDeliveryItemListById(new ArrayList<Long>(deliveryItemIdList), false);
		for ( DeliveryItem deliveryItem : deliveryItemList ) {
			deliveryItem.setChargeQty(null);
		}
		em.flush();
		corpPmsServiceProxy.voidInvoiceByInvoiceNum(Arrays.asList(invoiceNum));
	}
	
	@SuppressWarnings("unchecked")
	private List<MedProfile> retrieveMedProfile(String hkid, String caseNum){
		StringBuilder sb = new StringBuilder(
				"select o from MedProfile o" + // 20150420 index check : Patient.hkid MedCase.caseNum : I_PATIENT_02 I_MED_CASE_01
				" where o.hospCode = :hospCode");
		
		if ( StringUtils.isNoneBlank(hkid) ) {
			sb.append(" and o.patient.hkid = :hkid");
		} else {
			sb.append(" and o.medCase.caseNum = :caseNum");
		}
		
		sb.append(" order by o.createDate desc");
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("hospCode", workstore.getHospCode());
		if ( StringUtils.isNoneBlank(hkid) ) {
			query.setParameter("hkid", hkid);
		} else {
			query.setParameter("caseNum", caseNum);
		}
		
		
		return query.getResultList();
	}
	
	private void convertMpSfiInvoice(MedProfile medProfile, MpSfiInvoice mpSfiInvoice, Patient patient){
		mpSfiInvoice.setMedProfile(medProfile);
		mpSfiInvoice.setMedCase(medProfile.getMedCase());
		mpSfiInvoice.setPatient(patient);
		mpSfiInvoice.setMpSfiInvoiceItemList(new ArrayList<MpSfiInvoiceItem>());
	}
	
	private MpSfiInvoiceItem convertMpSfiInvoiceItem(DeliveryItem deliveryItem, WorkstoreDrug workstoreDrug, MpSfiInvoiceItem mpSfiInvoiceItem) {
		MedProfilePoItem medProfilePoItem = deliveryItem.getMedProfilePoItem();
		DmDrug dmDrug = workstoreDrug.getDmDrug();
		MpDispLabel mpDispLabel = labelJaxbWrapper.unmarshall(deliveryItem.getLabelXml());
		
		mpSfiInvoiceItem.setIncludeItem(false);
		mpSfiInvoiceItem.setDeliveryItemId(deliveryItem.getId());
		mpSfiInvoiceItem.setDeliveryItemVersion(deliveryItem.getVersion());
		mpSfiInvoiceItem.setItemCode(medProfilePoItem.getItemCode());
		mpSfiInvoiceItem.setFullDrugDesc(dmDrug.getFullDrugDesc());
		mpSfiInvoiceItem.setDrugName(dmDrug.getDrugName());
		mpSfiInvoiceItem.setBaseUnit(medProfilePoItem.getBaseUnit());
		mpSfiInvoiceItem.setDispDate(deliveryItem.getDelivery().getBatchDate());
		mpSfiInvoiceItem.setDeliveryItemStatus(deliveryItem.getStatus());
		mpSfiInvoiceItem.setDispQty(deliveryItem.getIssueQty());
		mpSfiInvoiceItem.setActionStatus(medProfilePoItem.getActionStatus());
		mpSfiInvoiceItem.setItemSuspend(workstoreDrug.getMsWorkstoreDrug().isLocalSuspend());
		if ( Boolean.TRUE.equals(mpDispLabel.getWardStockFlag()) ) {
			mpSfiInvoiceItem.setWardStockFlag(Boolean.TRUE);
		} else if ( mpDispLabel.getWardStockFlag() == null && Boolean.TRUE.equals(deliveryItem.getMedProfilePoItem().getWardStockFlag()) ){
			mpSfiInvoiceItem.setWardStockFlag(Boolean.TRUE);
		} else {
			mpSfiInvoiceItem.setWardStockFlag(Boolean.FALSE);
		}
		
		mpSfiInvoiceItem.setEndDate(medProfilePoItem.getEndDate());
		if ( deliveryItem.getIssueDuration() != null ) {
			mpSfiInvoiceItem.setIssueDuration(deliveryItem.getIssueDuration().toString());
		}
		mpSfiInvoiceItem.setChargeQty(deliveryItem.getIssueQty());

		DrugChargeInfo drugChargeInfo = dmDrug.getDrugChargeInfo();
		if( drugChargeInfo != null ){
			convertDrugChargeInfo(drugChargeInfo, dmDrug.getPmsFmStatus().getFmStatus(), mpSfiInvoiceItem);
		}
		return mpSfiInvoiceItem;
	}
	
	private MpSfiInvoiceItem convertDrugChargeInfo(DrugChargeInfo drugChargeInfo, String itemFmStatus, MpSfiInvoiceItem mpSfiInvoiceItem){
		Double corpDrugPrice = drugChargeInfo.getCorpDrugPrice() == null ? 0.0 : drugChargeInfo.getCorpDrugPrice();
		mpSfiInvoiceItem.setCorpDrugPrice(corpDrugPrice);
		
		DrugChargeRuleCriteria chargingRuleCriteria  = chargeCalculation.retrieveChargeRuleByPatTypeFmStatus(PharmOrderPatType.Public, itemFmStatus);
		OverrideChargeAmountRuleCriteria overrideChargeAmountRuleCriteria = new OverrideChargeAmountRuleCriteria();
		overrideChargeAmountRuleCriteria.updateByDrugChargeRuleCriteria(chargingRuleCriteria, workstore);
		mpSfiInvoiceItem.setPublicMarkupFactor(chargeCalculation.retrieveMarkupFactorByChargeRule(overrideChargeAmountRuleCriteria, drugChargeInfo));
		
		chargingRuleCriteria  = chargeCalculation.retrieveChargeRuleByPatTypeFmStatus(PharmOrderPatType.Private, itemFmStatus);
		overrideChargeAmountRuleCriteria.updateByDrugChargeRuleCriteria(chargingRuleCriteria, workstore);
		mpSfiInvoiceItem.setPrivateMarkupFactor(chargeCalculation.retrieveMarkupFactorByChargeRule(overrideChargeAmountRuleCriteria, drugChargeInfo));

		chargingRuleCriteria  = chargeCalculation.retrieveChargeRuleByPatTypeFmStatus(PharmOrderPatType.Nep, itemFmStatus);
		overrideChargeAmountRuleCriteria.updateByDrugChargeRuleCriteria(chargingRuleCriteria, workstore);
		mpSfiInvoiceItem.setNepMarkupFactor(chargeCalculation.retrieveMarkupFactorByChargeRule(overrideChargeAmountRuleCriteria, drugChargeInfo));
		return mpSfiInvoiceItem;
	}
	
	private MpSfiInvoicePrivatePatInfo convertMpSfiInvoicePrivatePatInfo(MedProfile medProfile, MpSfiInvoicePrivatePatInfo mpSfiInvoicePrivatePatInfo) {
		MedCase medCase = medProfile.getMedCase();
		Patient patient = medProfile.getPatient();
		
		mpSfiInvoicePrivatePatInfo.setBedNum(medCase.getPasBedNum());
		mpSfiInvoicePrivatePatInfo.setCaseNum(medCase.getCaseNum());
		if( medProfile.getLastInvoicePrintDate() != null ){
			mpSfiInvoicePrivatePatInfo.setLastInvoicePrintDate(new DateMidnight(medProfile.getLastInvoicePrintDate()).toDate());
		}else{
			mpSfiInvoicePrivatePatInfo.setLastInvoicePrintDate(medProfile.getLastInvoicePrintDate());
		}
		mpSfiInvoicePrivatePatInfo.setPasSpecCode(medCase.getPasSpecCode());
		mpSfiInvoicePrivatePatInfo.setPatName(patient.getName());
		mpSfiInvoicePrivatePatInfo.setPatNameChi(patient.getNameChi());
		mpSfiInvoicePrivatePatInfo.setWard(medCase.getPasWardCode());
		mpSfiInvoicePrivatePatInfo.setMedProfileId(medProfile.getId());
		if( MedProfileStatus.Inactive == medProfile.getStatus() ){
			mpSfiInvoicePrivatePatInfo.setDischargeDate(medProfile.getDischargeDate());
		}
		return mpSfiInvoicePrivatePatInfo;
	}
	
	private Delivery createDelivery(){
		Delivery delivery = new Delivery();
		delivery.setBatchDate(new Date());
		delivery.setDeliveryRequest(new DeliveryRequest());
		delivery.getDeliveryRequest().setWorkstore(workstore);
		return delivery;
	}
	
	private DeliveryItem createDeliveryItem(String itemCode, BigDecimal chargeQty, MedProfile medProfile){
		MedProfileItem medProfileItem = createMedProfileItem(itemCode, medProfile);
		medProfileItem.setMedProfile(medProfile);
		DeliveryItem deliveryItem = new DeliveryItem();
		deliveryItem.setAdjQty(BigDecimal.ZERO);
		deliveryItem.setPreIssueQty(chargeQty);
		deliveryItem.setIssueQty(chargeQty);
		deliveryItem.setIssueDuration(BigDecimal.ZERO);
		deliveryItem.setStatus(DeliveryItemStatus.Deleted);
		deliveryItem.setRefillFlag(false);
		deliveryItem.setDueDate(new Date());
		deliveryItem.setChargeQty(chargeQty);
		deliveryItem.setMedProfilePoItem(medProfileItem.getMedProfileMoItem().getMedProfilePoItemList().get(0));
		return deliveryItem;
	}
	
	private MedProfileItem createMedProfileItem(String itemCode, MedProfile medProfile){
		MedProfileItem medProfileItem = new MedProfileItem();
		MedProfileMoItem medProfileMoItem = new MedProfileMoItem();
		
		DmDrug dmDrug = dmDrugCacher.getDmDrug(itemCode);
		if ( "PDF".equals(dmDrug.getDmDrugProperty().getDisplayname().substring(0, 3)) ) {
			medProfileMoItem = createCapdMedProfileMoItem(dmDrug);
		} else {
			medProfileMoItem = createNonCapdMedProfileMoItem();
		}
		
		MedProfilePoItem medProfilePoItem = createMedProfilePoItem(dmDrug);
		medProfilePoItem.setMedProfileMoItem(medProfileMoItem);
		
		medProfileMoItem.getMedProfilePoItemList().add(medProfilePoItem);
		medProfileMoItem.setMedProfileItem(medProfileItem);
		medProfileItem.setMedProfileMoItem(medProfileMoItem);
		
		return medProfileItem;
	}
	
	private MedProfileMoItem createCapdMedProfileMoItem(DmDrug dmDrug){
		MedProfileMoItem medProfileMoItem = new MedProfileMoItem();
		
		// create vo
		CapdRxDrug capdRxDrug = new CapdRxDrug(); 
		medProfileMoItem.setRxItem(capdRxDrug);
		medProfileMoItem.setRxItemType(RxItemType.Capd);

		CapdItem capdItem = new CapdItem();
		capdRxDrug.addCapdItem(capdItem);
		
		// set MedProfileMoItem
		medProfileMoItem.setOrderDate(new Date());
		medProfileMoItem.clearMedProfilePoItemList();
		medProfileMoItem.clearMedProfileMoItemAlertList();
		
		capdItem.setItemCode(dmDrug.getItemCode());
		capdItem.setDosage(BigDecimal.valueOf(0));
		capdItem.setDosageUnit(dmDrug.getBaseUnit());
		capdItem.setDuration(0);
		capdItem.setBaseUnit(dmDrug.getBaseUnit());
		
		return medProfileMoItem;
	}
	
	private MedProfileMoItem createNonCapdMedProfileMoItem(){
		MedProfileMoItem medProfileMoItem = new MedProfileMoItem();
		
		RxDrug rxDrug = new OralRxDrug();
		medProfileMoItem.setRxItem(rxDrug);
		medProfileMoItem.setRxItemType(RxItemType.Oral);
		
		Regimen regimen = new Regimen();
		List<DoseGroup> doseGroupList = new ArrayList<DoseGroup>();
		List<Dose> doseList = new ArrayList<Dose>();
		DoseGroup doseGroup = new DoseGroup();		
		Dose dose = new Dose();
		Freq dailyFreq = new Freq();
		dailyFreq.setCode("");
		dailyFreq.setDesc("");
		dose.setDailyFreq(dailyFreq);
		dose.setSupplFreq(null);
		dose.setDosage(null);
		doseList.add(dose);
		doseGroup.setDoseList(doseList);
		doseGroup.setDuration(0);
		doseGroupList.add(doseGroup);				
		regimen.setDoseGroupList(doseGroupList);
		regimen.setType(RegimenType.Daily);
		rxDrug.setRegimen(regimen);
		
		medProfileMoItem.setOrderDate(new Date());
		medProfileMoItem.clearMedProfilePoItemList();
		medProfileMoItem.clearMedProfileMoItemAlertList();		
		
		return medProfileMoItem;
	}
	
	private MedProfilePoItem createMedProfilePoItem(DmDrug dmDrug){
		MedProfilePoItem medProfilePoItem = new MedProfilePoItem();
		medProfilePoItem.setItemCode(dmDrug.getItemCode());
		medProfilePoItem.setCalQty(BigDecimal.ZERO);
		medProfilePoItem.setAdjQty(BigDecimal.ZERO);
		medProfilePoItem.setIssueQty(BigDecimal.ZERO);
		medProfilePoItem.setBaseUnit(dmDrug.getBaseUnit());
		medProfilePoItem.setFormCode(dmDrug.getFormCode());
		medProfilePoItem.setDrugName(dmDrug.getDrugName());
		medProfilePoItem.setStrength(dmDrug.getStrength());
		medProfilePoItem.setFmStatus(dmDrug.getPmsFmStatus().getFmStatus());
		medProfilePoItem.setFormLabelDesc(dmDrug.getDmForm().getLabelDesc());
		medProfilePoItem.setVolumeText(dmDrug.getVolumeText());
		medProfilePoItem.setDmDrugLite(DmDrugLite.objValueOf(dmDrug));
		return medProfilePoItem;
	}
	
	@SuppressWarnings("unchecked")
	private List<DeliveryItem> retrieveDeliveryItemListById(List<Long> deliveryItemIdList, boolean pessimisticLock){
		if ( deliveryItemIdList.isEmpty() ) {
			return new ArrayList<DeliveryItem>();
		}
		Query query = em.createQuery(
				"select o from DeliveryItem o" + // 20150420 index check : DeliveryItem.id : PK_DELIVERY_ITEM
				" where o.id in :deliveryItemIdList")
				.setParameter("deliveryItemIdList", deliveryItemIdList);
		if ( pessimisticLock ) {
			query.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock);
		}
		return query.getResultList();
	}
	
	private Map<Long, DeliveryItem> prepareDeliveryItemMap(List<InvoiceItem> invoiceItemList){
		Set<Long> deliveryItemIdList = new HashSet<Long>();
		for ( InvoiceItem invoiceItem : invoiceItemList ) {
			Long deliveryItemId = invoiceItem.getDispOrderItem().getDeliveryItemId();
			if ( deliveryItemId != null ) {
				deliveryItemIdList.add(deliveryItemId);
			}
		}
		
		List<DeliveryItem> deliveryItemList = this.retrieveDeliveryItemListById(new ArrayList<Long>(deliveryItemIdList), false);
		Map<Long, DeliveryItem> deliveryItemMap = new HashMap<Long, DeliveryItem>();
		for ( DeliveryItem deliveryItem : deliveryItemList ) {
			if ( !deliveryItemMap.containsKey(deliveryItem.getId()) ) {
				deliveryItem.getMedProfilePoItem().getMedProfileMoItem().getMedProfileItem().getMedProfile().getPatient();
				deliveryItem.getMedProfilePoItem().getMedProfileMoItem().getMedProfileItem().getMedProfile().getMedCase();
				deliveryItemMap.put(deliveryItem.getId(), deliveryItem);
			}
		}
		
		return deliveryItemMap;
	}
	
	private Patient getPasPatient(String hkid, String caseNum){
		if ( !propMap.getValueAsBoolean(PATIENT_PAS_PATIENT_ENABLED.getName()) || !MEDPROFILE_PATIENT_PAS_PATIENT_ENABLED.get(true)) {
			logger.debug("HKPMI service error or unavailable.");
			messageCode = "0281";	//HKPMI service error or unavailable.
			return null;
		}
		Patient patient = null;
		if ( StringUtils.isNotBlank(caseNum) ) {
			Pair<String, Patient> retrievePasPatientResult = medProfileManager.retrievePasPatient(workstore, caseNum);
			if (retrievePasPatientResult != null) {
				patient = retrievePasPatientResult.getRight();
			}
		} else if ( StringUtils.isNotBlank(hkid) ) {
			patient = medProfileManager.retrievePasPatient(hkid);
		}
		
		if ( patient == null ) {
			logger.debug("Patient is not found in HKPMI");
			messageCode = "0708";	//Patient is not found in HKPMI
		}
		return patient;
	}
	
	@SuppressWarnings("unchecked")
	private List<DeliveryItem> retrieveDeliveryItemListForNewInvoice(String hkid, String caseNum, Long medProfileId){
		StringBuilder sb = new StringBuilder(
				"select o from DeliveryItem o" + // 20150420 index check : DeliveryItem.medProfile,status,chargeQty : I_DELIVERY_ITEM_01
				" where o.status in :notDeleteStatus" +
				" and o.chargeQty is null" +
				" and o.purchaseRequest is null" +
				" and o.medProfilePoItem.actionStatus <> :actionStatus" +
				" and o.medProfilePoItem.medProfileMoItem.medProfileItem.medProfile.hospCode = :hospCode");

		if ( medProfileId != null ) {
			sb.append(" and o.medProfilePoItem.medProfileMoItem.medProfileItem.medProfile.id = :medProfileId");
		} else if ( StringUtils.isNoneBlank(hkid) ) {
			sb.append(" and o.medProfilePoItem.medProfileMoItem.medProfileItem.medProfile.patient.hkid = :hkid");
		} else {
			sb.append(" and o.medProfilePoItem.medProfileMoItem.medProfileItem.medProfile.medCase.caseNum = :caseNum");
		}
		
		sb.append(" order by o.medProfilePoItem.medProfileMoItem.medProfileItem.medProfile.createDate desc");
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("notDeleteStatus", DeliveryItemStatus.Not_Deleted);
		query.setParameter("actionStatus", ActionStatus.ContinueWithOwnStock);
		query.setParameter("hospCode", workstore.getHospCode());
		
		if ( medProfileId != null ) {
			query.setParameter("medProfileId", medProfileId);
		} else if ( StringUtils.isNoneBlank(hkid) ) {
			query.setParameter("hkid", hkid);
		} else {
			query.setParameter("caseNum", caseNum);
		}
		return query.getResultList();
	}
	
	private boolean isExceptionDrugExist ( Invoice invoice ) {
		return invoiceManager.isExceptionDrugExist(invoice);
	}
	
	private void clearMessageCode() {
		messageCode = "";
	}

	public String getMessageCode() {
		return messageCode;
	}

	public boolean isSuccess() {
		return success;
	}
	
	private static class MpSfiInvoiceItemComparator implements Comparator<MpSfiInvoiceItem>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(MpSfiInvoiceItem mpSfiInvoiceItem1, MpSfiInvoiceItem mpSfiInvoiceItem2) {
			//sort by dispense date (desc) then item code (asc)
			if ( mpSfiInvoiceItem2.getDispDate() == null && mpSfiInvoiceItem1.getDispDate() == null ) {
				return new CompareToBuilder()
								.append( mpSfiInvoiceItem1.getItemCode(), mpSfiInvoiceItem2.getItemCode() )
								.toComparison();
			}
			
			if ( mpSfiInvoiceItem2.getDispDate() == null ) {
				return Integer.MIN_VALUE;
			}
			
			if ( mpSfiInvoiceItem1.getDispDate() == null ) {
				return Integer.MAX_VALUE;
			}
			
			return new CompareToBuilder()
							.append( mpSfiInvoiceItem2.getDispDate(), mpSfiInvoiceItem1.getDispDate() )
							.append( mpSfiInvoiceItem1.getItemCode(), mpSfiInvoiceItem2.getItemCode() )
							.toComparison();
		}
    }
		
	@Remove
	public void destroy() 
	{
		if ( mpSfiInvoicePrivatePatInfoList != null ) {
			mpSfiInvoicePrivatePatInfoList = null;
		}
	}
	
	private static class MpSfiInvoicePrivatePatInfoComparator implements Comparator<MpSfiInvoicePrivatePatInfo>, Serializable {

		private static final long serialVersionUID = 1L;

		public int compare(MpSfiInvoicePrivatePatInfo mpSfiInvoicePrivatePatInfo1, MpSfiInvoicePrivatePatInfo mpSfiInvoicePrivatePatInfo2) {
			int compareResult = 0;
			boolean lastInvoiceDateAfterDischarge1 = false;
			boolean lastInvoiceDateAfterDischarge2 = false;
			
			if( mpSfiInvoicePrivatePatInfo1.getDischargeDate() != null ){
				if( mpSfiInvoicePrivatePatInfo1.getLastInvoicePrintDate() != null){
					if( mpSfiInvoicePrivatePatInfo1.getLastInvoicePrintDate().compareTo( mpSfiInvoicePrivatePatInfo1.getDischargeDate()) > 0 ){
						lastInvoiceDateAfterDischarge1 = true;
					}
				}
			}
			
			if( mpSfiInvoicePrivatePatInfo2.getDischargeDate() != null ){
				if( mpSfiInvoicePrivatePatInfo2.getLastInvoicePrintDate() != null){
					if( mpSfiInvoicePrivatePatInfo2.getLastInvoicePrintDate().compareTo( mpSfiInvoicePrivatePatInfo2.getDischargeDate()) > 0 ){
						lastInvoiceDateAfterDischarge2 = true;
					}
				}
			}
			if( lastInvoiceDateAfterDischarge1 && !lastInvoiceDateAfterDischarge2 ){
				return 1;
			}else if( !lastInvoiceDateAfterDischarge1 && lastInvoiceDateAfterDischarge2 ){
				return -1;
			}
			
			compareResult = new CompareToBuilder()
								.append( mpSfiInvoicePrivatePatInfo1.getLastInvoicePrintDate(), mpSfiInvoicePrivatePatInfo2.getLastInvoicePrintDate())
								.append( StringUtils.trimToEmpty(mpSfiInvoicePrivatePatInfo1.getWard()) , StringUtils.trimToEmpty(mpSfiInvoicePrivatePatInfo2.getWard()))
								.append( StringUtils.trimToEmpty(mpSfiInvoicePrivatePatInfo1.getCaseNum()) , StringUtils.trimToEmpty(mpSfiInvoicePrivatePatInfo2.getCaseNum()))
								.toComparison();
			return compareResult;
		}
		
	}
}
