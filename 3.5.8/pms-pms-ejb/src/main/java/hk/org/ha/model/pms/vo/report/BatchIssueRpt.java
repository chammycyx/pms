package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.List;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class BatchIssueRpt implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private BatchIssueRptHdr batchIssueHdr;
	
	private List<BatchIssueRptDtl> batchIssueRptDtlList;

	public BatchIssueRptHdr getBatchIssueHdr() {
		return batchIssueHdr;
	}

	public void setBatchIssueHdr(BatchIssueRptHdr batchIssueHdr) {
		this.batchIssueHdr = batchIssueHdr;
	}

	public List<BatchIssueRptDtl> getBatchIssueRptDtlList() {
		return batchIssueRptDtlList;
	}

	public void setBatchIssueRptDtlList(List<BatchIssueRptDtl> batchIssueRptDtlList) {
		this.batchIssueRptDtlList = batchIssueRptDtlList;
	}

	
}
