package hk.org.ha.model.pms.biz.enquiry;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;
import org.springframework.util.StringUtils;

@Stateful
@Scope(ScopeType.SESSION)
@Name("sfiDrugPriceGenDateListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SfiDrugPriceGenDateListServiceBean implements SfiDrugPriceGenDateListServiceLocal {

	@Out(required=false)
	private List<Date> drugPriceGenDateList;
	
	@Out(required=false)
	private Date lastDrugPriceGenDate;
		
	@Logger
	private Log logger;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;	
	
	public void retrieveDrugPriceGenDateList(){
		lastDrugPriceGenDate = null;
		drugPriceGenDateList = dmsPmsServiceProxy.retrieveDmCdpReportGenDateList();
		
		if( drugPriceGenDateList.size() > 0 ){
			lastDrugPriceGenDate = drugPriceGenDateList.get(0);
		}
		logger.info("DrugPriceGenList:#0", StringUtils.arrayToCommaDelimitedString(drugPriceGenDateList.toArray()));
	}
	
	@Remove
	public void destroy() 
	{
		if ( drugPriceGenDateList != null ){
			drugPriceGenDateList = null;
		}
			
		if ( lastDrugPriceGenDate != null ){
			lastDrugPriceGenDate = null;
		}
	}
}
