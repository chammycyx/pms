package hk.org.ha.model.pms.vo.alert.mds;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedProperty;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "AlertHlaTest")
@ExternalizedBean(type=DefaultExternalizer.class)
public class AlertHlaTest implements Alert, Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private static final int ADDITIVE_NUM_MULTIPLIER = 10000;
	
	private static final int MANUAL_ITEM_MULTIPLIER = 10000;

	private static final int MANUAL_ITEM_RANGE = 100000000; //100010000
	
	@XmlElement(name="HospCode", required = true)
	private String hospCode;

	@XmlElement(name="OrdNo")
	private Long ordNo;
	
	@XmlElement(name="ItemNum", required = true)
	private Long itemNum;

	@XmlElement(name="CaseNum")
	private String caseNum;
	
	@XmlElement(name="DisplayName", required = true)
	private String displayName;

	@XmlElement(name="ScreenMsg", required = true)
	private String screenMsg;

	@XmlTransient
	private Long sortSeq;
	
	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public Long getOrdNo() {
		return ordNo;
	}

	public void setOrdNo(Long ordNo) {
		this.ordNo = ordNo;
	}

	public Long getItemNum() {
		return itemNum;
	}

	public void setItemNum(Long itemNum) {
		this.itemNum = itemNum;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getScreenMsg() {
		return screenMsg;
	}

	public void setScreenMsg(String screenMsg) {
		this.screenMsg = screenMsg;
	}

	public Long getSortSeq() {
		return sortSeq;
	}

	public void setSortSeq(Long sortSeq) {
		this.sortSeq = sortSeq;
	}

	@ExternalizedProperty
	public String getAlertTitle() {
		return "HLA-B*1502 Checking";
	}

	@ExternalizedProperty	
	public String getAlertDesc() {
		return screenMsg;
	}
	
	public boolean equals(Object object) {
		
		if (!(object instanceof AlertHlaTest)) {
			return false;
		}
		if (this == object) {
			return true;
		}

		AlertHlaTest otherAlert = (AlertHlaTest) object;
		
		return new EqualsBuilder()
			.append(StringUtils.trimToNull(StringUtils.lowerCase(hospCode)),    StringUtils.trimToNull(StringUtils.lowerCase(otherAlert.getHospCode())))
			.append(ordNo,                                                      otherAlert.getOrdNo())
			.append(itemNum,                                                    otherAlert.getItemNum())
			.append(StringUtils.trimToNull(StringUtils.lowerCase(displayName)), StringUtils.trimToNull(StringUtils.lowerCase(otherAlert.getDisplayName())))
			.append(StringUtils.trimToNull(StringUtils.lowerCase(screenMsg)),   StringUtils.trimToNull(StringUtils.lowerCase(otherAlert.getScreenMsg())))
			.isEquals();
	}
	
	public int hashCode() {
		return new HashCodeBuilder()
			.append(StringUtils.trimToNull(StringUtils.lowerCase(hospCode)))
			.append(ordNo)
			.append(itemNum)
			.append(StringUtils.trimToNull(StringUtils.lowerCase(displayName)))
			.append(StringUtils.trimToNull(StringUtils.lowerCase(screenMsg)))
			.toHashCode();
	}

	public void loadDrugDesc(String drugDesc) {
	}

	public void replaceItemNum(Integer oldItemNum, Integer newItemNum, String orderNum) {
		itemNum = Long.valueOf(newItemNum.intValue());
	}

	public void replaceOrderNum(String oldOrderNum, String newOrderNum) {
		hospCode = StringUtils.trimToNull(newOrderNum.substring(0, 3));
		ordNo = Long.valueOf(newOrderNum.substring(3));		
	}

	public void restoreItemNum() {
		if( itemNum.longValue() > MANUAL_ITEM_RANGE ){
			itemNum = Long.valueOf(itemNum.longValue() / MANUAL_ITEM_MULTIPLIER);
		}else if (itemNum.longValue() >= ADDITIVE_NUM_MULTIPLIER) {
			itemNum = Long.valueOf(itemNum.longValue() / ADDITIVE_NUM_MULTIPLIER);
		}
	}

	@ExternalizedProperty
	public String getKeys() {
		return null;
	}
}
