package hk.org.ha.model.pms.biz.support;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.reftable.Prop;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.persistence.reftable.WorkstationProp;
import hk.org.ha.model.pms.udt.DataType;
import hk.org.ha.model.pms.udt.YesNoFlag;
import hk.org.ha.model.pms.udt.reftable.MaintScreen;
import hk.org.ha.model.pms.vo.support.PropInfo;
import hk.org.ha.model.pms.vo.support.PropInfoItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("workstationPropListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class WorkstationPropListServiceBean implements WorkstationPropListServiceLocal {
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private PropManagerLocal propManager;
	
	@Out(required = false)
	private List<PropInfo> workstationPropInfoList;
	
	private Hospital hospital;
	private String hostName;
	
	private boolean validHostName;
	
	public void clearWorkstationPropInfoList(){
		workstationPropInfoList = null;
	}	
	
	@SuppressWarnings("unchecked")
	public List<Hospital> retrieveHospitalList() {
		return  em.createQuery(
						"select o from Hospital o" + // 20120306 index check : none
						" order by o.hospCode")
						.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<String> retrieveHostNameList(String hospCode, String prefixHostName) {
		List<String> resultList =  em.createQuery(
				"select distinct o.hostName from Workstation o" + // 20120306 index check : Workstore.hospital : FK_WORKSTORE_01
				" where o.workstore.hospCode = :hospCode" +
				" order by o.hostName")
				.setParameter("hospCode", hospCode)
				.getResultList();
		
		if ( StringUtils.isBlank(prefixHostName) ) {
			return resultList;
		}
		List<String> hostNameList = new ArrayList<String>();
		for ( String tmpHostName : resultList ) {
			if ( tmpHostName.startsWith(prefixHostName) ) {
				hostNameList.add(tmpHostName);
			}
		}
		return hostNameList;
	}
	
	private boolean validWorkstation(Hospital inHospital, String inHostName){
		List<Workstation> workstationList = retriveWorkstationListByHospCode(inHospital, inHostName);
		return !workstationList.isEmpty();
	}

	public void retrieveWorkstationPropList(Hospital inHospital, String inHostName) {
		workstationPropInfoList = new ArrayList<PropInfo>();
		validHostName = validWorkstation(inHospital, inHostName);
		
		if ( !validHostName ) {
			return; // invalid computer name
		}
		
		hospital = inHospital;
		hostName = inHostName;
		
		List<Prop> propList= propManager.retrievePropListByWorkstationPropName();
		List<Workstation> workstationList = this.retriveWorkstationListByHospCode(inHospital, inHostName);
		List<WorkstationProp> workstationPropList = propManager.retrieveWorkstationPropListByHostName(hospital.getHospCode(), hostName);
		convertWorkstationPropListToPropInfoList(propList, workstationPropList, workstationList);
		sortPropInfoList(workstationPropInfoList);
		for ( PropInfo propInfo : workstationPropInfoList ) {
			sortPropInfoItemByWorkstoreCode(propInfo.getPropInfoItemList());
		}

	}
	
	private void convertWorkstationPropListToPropInfoList(List<Prop> propList, 
															List<WorkstationProp> workstationPropList, 
															List<Workstation> workstationList) {
		
		
		for ( Prop prop : propList ) {
			PropInfo propInfo = new PropInfo();
			workstationPropInfoList.add(propInfo);
			propInfo.setDescription(prop.getDescription());
			propInfo.setType(prop.getType());
			propInfo.setValidation(prop.getValidation());
			propInfo.setPropInfoItemList(new ArrayList<PropInfoItem>());
			propInfo.setPropId(prop.getId());
			propInfo.setSupportMaintFlag(getSupportMaintFlag(prop.getMaintScreen()));
			
			for ( Workstation workstation : workstationList ) {
				Long workstationPropId = null;
				String value = prop.getDefValue();
				for ( WorkstationProp workstationProp : workstationPropList ) {
					if ( !workstationProp.getProp().getName().equals(prop.getName())) {
						continue;
					}
					if ( workstationProp.getWorkstation().getWorkstore().getHospCode().equals(workstation.getWorkstore().getHospCode()) && 
							workstationProp.getWorkstation().getWorkstore().getWorkstoreCode().equals(workstation.getWorkstore().getWorkstoreCode()) ) {
						workstationPropId = workstationProp.getId();
						value = workstationProp.getValue();
						break;
					}
				}
				
				PropInfoItem propInfoItem = new PropInfoItem();
				propInfoItem.setProp(prop);
				propInfoItem.setPropItemId(workstationPropId);
				propInfoItem.setHospCode(workstation.getWorkstore().getHospCode());
				propInfoItem.setWorkstoreCode(workstation.getWorkstore().getWorkstoreCode());
				propInfoItem.setValue(value);
				if ( propInfo.getType() == DataType.BooleanType ) {
					propInfoItem.setDisplayValue(getBooleanDisplayValue(value));
				} else {
					propInfoItem.setDisplayValue(value);
				}
				propInfo.getPropInfoItemList().add(propInfoItem);
				
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void sortPropInfoList(List<PropInfo> propInfoList){
		Collections.sort(propInfoList, new Comparator(){
			public int compare(Object o1, Object o2) {
				PropInfo item1 = (PropInfo) o1;
				PropInfo item2 = (PropInfo) o2;
				return item1.getDescription().compareTo(item2.getDescription());
			}
		});	
	}
	
	@SuppressWarnings("unchecked")
	private void sortPropInfoItemByWorkstoreCode(List<PropInfoItem> itemList) {
		Collections.sort(itemList, new Comparator(){
			public int compare(Object o1, Object o2) {
				PropInfoItem item1 = (PropInfoItem) o1;
				PropInfoItem item2 = (PropInfoItem) o2;
				return item1.getWorkstoreCode().compareTo(item2.getWorkstoreCode());
			}
		});	
	}
	
	private String getBooleanDisplayValue(String dataValue) {
		if ( YesNoFlag.dataValueOf(dataValue) != null ) {
			return YesNoFlag.dataValueOf(dataValue).getDisplayValue();
		} else {
			return StringUtils.EMPTY;
		}
	}
	
	private boolean getSupportMaintFlag(MaintScreen maintScreen) {
		return maintScreen == MaintScreen.Support;
	}
	
	public void updateWorkstationPropList(List<PropInfo> propInfoList) {
		
		if ( !propInfoList.isEmpty() ) {
			List<WorkstationProp> workstationPropList = convertToWorkstationPropList(propInfoList);
			propManager.updateWorkstationPropList(workstationPropList);
		}
		clearWorkstationPropInfoList();
	}
	
	private List<WorkstationProp> convertToWorkstationPropList(List<PropInfo> propInfoList) {
		List<WorkstationProp> workstationPropList = new ArrayList<WorkstationProp>();
		
		for ( PropInfo propInfo : propInfoList ) {
			for ( PropInfoItem propInfoItem : propInfo.getPropInfoItemList() ) {
				WorkstationProp workstationProp;
				if ( propInfoItem.getPropItemId() == null ) {
					workstationProp = new WorkstationProp();
					Prop prop = em.find(Prop.class, propInfo.getPropId());
					workstationProp.setProp(prop);
					workstationProp.setSortSeq(prop.getId().intValue());
					workstationProp.setWorkstation(retrieveWorkstation(propInfoItem.getHospCode(), propInfoItem.getWorkstoreCode(), hostName));
				} else {
					workstationProp = em.find(WorkstationProp.class, propInfoItem.getPropItemId());
				}
				workstationProp.setValue(propInfoItem.getValue());
				workstationPropList.add(workstationProp);
			}
		}
		
		return workstationPropList;
	}
	
	@SuppressWarnings("unchecked")
	private Workstation retrieveWorkstation(String hospCode, String workstoreCode, String hostName){
		List<Workstation> workstationList = em.createQuery(
				"select o from Workstation o " +  // 20120306 index check : Workstation.hostName : I_WORKSTATION_01
				" where o.workstore.hospCode = :hospCode" +
				" and o.workstore.workstoreCode = :workstoreCode" +
				" and o.hostName = :hostName")
				.setParameter("hospCode", hospCode)
				.setParameter("workstoreCode", workstoreCode)
				.setParameter("hostName", hostName)
				.getResultList();
		
		return workstationList.get(0);
	}
	
	@SuppressWarnings("unchecked")
	private List<Workstation> retriveWorkstationListByHospCode(Hospital inHospital, String inHostName){
		return em.createQuery(
				"select o from Workstation o " + // 20120306 index check : Workstation.hostName : I_WORKSTATION_01
				" where o.workstore.hospCode = :hospCode" +
				" and o.hostName = :hostName" +
				" order by o.workstore.workstoreCode")
				.setParameter("hospCode", inHospital.getHospCode())
				.setParameter("hostName", inHostName)
				.getResultList();
	}
	
	public boolean isValidHostName() {
		return validHostName;
	}

	@Remove
	public void destroy() 
	{
		if ( workstationPropInfoList != null ) {
			workstationPropInfoList = null;
		}
	}
}
