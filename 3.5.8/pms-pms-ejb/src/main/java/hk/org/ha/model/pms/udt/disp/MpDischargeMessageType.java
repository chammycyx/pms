package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum MpDischargeMessageType implements StringValuedEnum {

	// From MOE Ordering to PMS
	NewOrder("N", "newOrder", "create"),
	VetOrder("V", "vetOrder", "vet"),
	VetOrderRemark("R", "vetOrderRemark", "vetremark"),
	Discontinue("D", "discontinue", "discontinue");
	
	private final String dataValue;
	private final String displayValue;
	private final String packageName;
	
	MpDischargeMessageType (final String dataValue, final String displayValue, final String packageName) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
		this.packageName = packageName;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}

	public String getPackageName() {
		return this.packageName;
	}
	
	public static MpDischargeMessageType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MpDischargeMessageType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<MpDischargeMessageType> {

		private static final long serialVersionUID = 908893648178158318L;

		public Class<MpDischargeMessageType> getEnumClass() {
    		return MpDischargeMessageType.class;
    	}
    }
}