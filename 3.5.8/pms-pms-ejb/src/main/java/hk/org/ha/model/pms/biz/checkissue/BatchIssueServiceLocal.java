package hk.org.ha.model.pms.biz.checkissue;

import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.checkissue.BatchIssueDetail;
import hk.org.ha.model.pms.vo.report.BatchIssueRpt;

import java.util.Date;

import javax.ejb.Local;

@Local
public interface BatchIssueServiceLocal {
	
	void retrieveBatchIssueDetail(Date issueDate, Workstore workstore);
	
	void updateBatchIssueLogDetail(BatchIssueDetail updateBatchIssueDetail);
	
	void updateBatchIssueLogWorkstation(Date issueDate, Workstore workstore, BatchIssueDetail updateBatchIssueDetail, String action);
	
	void deleteBatchIssueLog(Date issueDate, Workstore workstore);
	
	boolean isResult();

	String getErrMsgCode();
	
	void printBatchIssueRpt(String rptDocType);
	
	BatchIssueRpt retrieveBatchIssueRpt();
	
	BatchIssueRpt retrieveBatchIssueExceptionRpt();
	
	BatchIssueRpt retrieveBatchIssueSfiRpt();
	
	void destroy();
	
}
