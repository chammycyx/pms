package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class SfiRefillCouponDtl implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String itemCode;
	
	private String fullDrugDesc;
	
	private Integer totalIssueQty;
	
	private Integer remainQty;
	
	private String remainDuration;
	
	private String baseUnit;

	public String getItemCode() {
		return itemCode;
	}
	
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	
	public String getFullDrugDesc() {
		return fullDrugDesc;
	}
	
	public void setFullDrugDesc(String fullDrugDesc) {
		this.fullDrugDesc = fullDrugDesc;
	}
	
	public Integer getTotalIssueQty() {
		return totalIssueQty;
	}
	
	public void setTotalIssueQty(Integer totalIssueQty) {
		this.totalIssueQty = totalIssueQty;
	}
	
	public Integer getRemainQty() {
		return remainQty;
	}
	
	public void setRemainQty(Integer remainQty) {
		this.remainQty = remainQty;
	}
	
	public String getRemainDuration() {
		return remainDuration;
	}
	
	public void setRemainDuration(String remainDuration) {
		this.remainDuration = remainDuration;
	}
	
	public String getBaseUnit() {
		return baseUnit;
	}
	
	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}
}
