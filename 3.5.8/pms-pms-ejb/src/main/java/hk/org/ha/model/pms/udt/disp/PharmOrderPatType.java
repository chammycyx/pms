package hk.org.ha.model.pms.udt.disp;

import java.util.Arrays;
import java.util.List;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PharmOrderPatType implements StringValuedEnum {
	
	Public("U", "PUBLIC"),
	Private("R", "PRIVATE"),
	Doh("D", "DOH"),
	Nep("N", "NEP");

	private final String dataValue;
	private final String displayValue;
	
	public static final List<PharmOrderPatType> Public_Nep = Arrays.asList(Public,Nep);
	
	PharmOrderPatType(final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}
	
	public static PharmOrderPatType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PharmOrderPatType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<PharmOrderPatType> {

		private static final long serialVersionUID = 1L;

		@Override
		public Class<PharmOrderPatType> getEnumClass() {
			return PharmOrderPatType.class;
		}

	}
	
}
