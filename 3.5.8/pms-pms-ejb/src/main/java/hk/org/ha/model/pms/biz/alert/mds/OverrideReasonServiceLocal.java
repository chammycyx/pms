package hk.org.ha.model.pms.biz.alert.mds;

import hk.org.ha.model.pms.vo.alert.mds.OverrideReasonResult;

import java.util.List;

import javax.ejb.Local;

@Local
public interface OverrideReasonServiceLocal {

	OverrideReasonResult acknowledgeOverrideReasonForOp();
	
	OverrideReasonResult acknowledgeOverrideReasonForIp(String ackUser);
	
	OverrideReasonResult editItemReason(List<String> overrideReason, Integer ddiAlertIndex, Boolean orderEditFlag);
	
	OverrideReasonResult editPrescReason(List<String> overrideReason, Integer alertIndex);
	
	OverrideReasonResult authenticateAcknowledgePermissionForOp(String userName, String password);
	
	OverrideReasonResult authenticateAcknowledgePermissionForIp(String userName, String password);
	
	OverrideReasonResult authenticateOverridePermission(String userName, String password);
	
	OverrideReasonResult retrieveOverridePermission();
	
	String authenticateMpPermission(String userName, String password, String permission);
		
	void destroy();
}