package hk.org.ha.model.pms.udt.dms;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum ItemStatus implements StringValuedEnum {

	Active("A", "Active"),
	Inactive("I", "Inactive");
	
    private final String dataValue;
    private final String displayValue;

    ItemStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static ItemStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(ItemStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<ItemStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<ItemStatus> getEnumClass() {
    		return ItemStatus.class;
    	}
    }
}



