package hk.org.ha.model.pms.persistence.medprofile;

import hk.org.ha.model.pms.persistence.corp.Hospital;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "MED_PROFILE_PO_ITEM_STAT")
public class MedProfilePoItemStat {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "medProfilePoItemStatSeq")
	@SequenceGenerator(name = "medProfilePoItemStatSeq", sequenceName = "SQ_MED_PROFILE_PO_ITEM_STAT", initialValue = 100000000)
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DUE_DATE", nullable = false)
	private Date dueDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_DUE_DATE")
	private Date lastDueDate;
	
	@Column(name = "WARD_CODE", length = 4, nullable = false)
    private String wardCode;
	
	@Column(name = "URGENT_FLAG", nullable = false)
	private Boolean urgentFlag;

	@Column(name = "STAT_FLAG", nullable = false)
	private Boolean statFlag;
	
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "RETURN_DATE")
    private Date returnDate;
    
	@Column(name = "PAT_HOSP_CODE", nullable = false, length = 3)
	private String patHospCode;

	@Column(name = "PAS_WARD_CODE", length = 4)
	private String pasWardCode;
	
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "HOSP_CODE", nullable = false)
    private Hospital hospital;

	@Column(name = "HOSP_CODE", nullable = false, insertable = false, updatable = false)
	private String hospCode;	
    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MED_PROFILE_ID", nullable = false)
	private MedProfile medProfile;

	@Column(name = "MED_PROFILE_ID", nullable = false, insertable = false, updatable = false)
	private Long medProfileId;	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MED_PROFILE_PO_ITEM_ID", nullable = false)
	private MedProfilePoItem medProfilePoItem;
	
	@Column(name = "MED_PROFILE_PO_ITEM_ID", nullable = false, insertable = false, updatable = false)
	private Long medProfilePoItemId;	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDueDate() {
		return dueDate == null ? null : new Date(dueDate.getTime());
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate == null ? null : new Date(dueDate.getTime());
	}

	public Date getLastDueDate() {
		return lastDueDate == null ? null : new Date(lastDueDate.getTime());
	}

	public void setLastDueDate(Date lastDueDate) {
		this.lastDueDate = lastDueDate == null ? null : new Date(lastDueDate.getTime());
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public Boolean getUrgentFlag() {
		return urgentFlag;
	}

	public void setUrgentFlag(Boolean urgentFlag) {
		this.urgentFlag = urgentFlag;
	}

	public Boolean getStatFlag() {
		return statFlag;
	}

	public void setStatFlag(Boolean statFlag) {
		this.statFlag = statFlag;
	}

	public Date getReturnDate() {
		return returnDate == null ? null : new Date(returnDate.getTime());
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate == null ? null : new Date(returnDate.getTime());
	}

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public String getPasWardCode() {
		return pasWardCode;
	}

	public void setPasWardCode(String pasWardCode) {
		this.pasWardCode = pasWardCode;
	}

	public Hospital getHospital() {
		return hospital;
	}

	public void setHospital(Hospital hospital) {
		this.hospital = hospital;
	}
	
	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public MedProfile getMedProfile() {
		return medProfile;
	}

	public void setMedProfile(MedProfile medProfile) {
		this.medProfile = medProfile;
	}
	
	public Long getMedProfileId() {
		return medProfileId;
	}

	public void setMedProfileId(Long medProfileId) {
		this.medProfileId = medProfileId;
	}

	public MedProfilePoItem getMedProfilePoItem() {
		return medProfilePoItem;
	}

	public void setMedProfilePoItem(MedProfilePoItem medProfilePoItem) {
		this.medProfilePoItem = medProfilePoItem;
	}

	public Long getMedProfilePoItemId() {
		return medProfilePoItemId;
	}

	public void setMedProfilePoItemId(Long medProfilePoItemId) {
		this.medProfilePoItemId = medProfilePoItemId;
	}
}
