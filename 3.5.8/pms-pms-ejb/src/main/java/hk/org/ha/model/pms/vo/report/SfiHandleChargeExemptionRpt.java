package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class SfiHandleChargeExemptionRpt implements Serializable {

	private static final long serialVersionUID = 1L;

	private Date  dispDate;
	
	private String ticketNum;
	
	private String caseNum;
	
	private String itemCode;
	
	private String invoiceNum;
	
	private String handleExemptBy;
	
	private Integer totalQty;
	
	private Integer issueQty;
	
	private String currentExemptCode;
	
	private String currentExemptDesc;
	
	private String nextExemptCode;
	
	private String nextExemptDesc;

	public Date getDispDate() {
		return dispDate;
	}

	public void setDispDate(Date dispDate) {
		this.dispDate = dispDate;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getInvoiceNum() {
		return invoiceNum;
	}

	public void setInvoiceNum(String invoiceNum) {
		this.invoiceNum = invoiceNum;
	}

	public String getHandleExemptBy() {
		return handleExemptBy;
	}

	public void setHandleExemptBy(String handleExemptBy) {
		this.handleExemptBy = handleExemptBy;
	}

	public Integer getTotalQty() {
		return totalQty;
	}

	public void setTotalQty(Integer totalQty) {
		this.totalQty = totalQty;
	}

	public Integer getIssueQty() {
		return issueQty;
	}

	public void setIssueQty(Integer issueQty) {
		this.issueQty = issueQty;
	}

	public String getCurrentExemptCode() {
		return currentExemptCode;
	}

	public void setCurrentExemptCode(String currentExemptCode) {
		this.currentExemptCode = currentExemptCode;
	}

	public String getCurrentExemptDesc() {
		return currentExemptDesc;
	}

	public void setCurrentExemptDesc(String currentExemptDesc) {
		this.currentExemptDesc = currentExemptDesc;
	}

	public String getNextExemptCode() {
		return nextExemptCode;
	}

	public void setNextExemptCode(String nextExemptCode) {
		this.nextExemptCode = nextExemptCode;
	}

	public String getNextExemptDesc() {
		return nextExemptDesc;
	}

	public void setNextExemptDesc(String nextExemptDesc) {
		this.nextExemptDesc = nextExemptDesc;
	}
	
	

}
