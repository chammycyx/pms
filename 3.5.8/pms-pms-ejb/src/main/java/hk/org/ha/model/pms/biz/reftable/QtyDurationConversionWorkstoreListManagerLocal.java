package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.dms.persistence.PmsPcuMapping;

import java.util.List;

import javax.ejb.Local;

@Local
public interface QtyDurationConversionWorkstoreListManagerLocal {
	
	List<PmsPcuMapping> retrievePmsPcuMapping();
}
