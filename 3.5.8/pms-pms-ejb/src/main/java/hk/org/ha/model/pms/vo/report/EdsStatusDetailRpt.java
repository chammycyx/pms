package hk.org.ha.model.pms.vo.report;

import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderAdminStatus;
import hk.org.ha.model.pms.udt.onestop.AllowCommentType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class EdsStatusDetailRpt implements Serializable {
	
	private static final long serialVersionUID = 1L;

	Date dispDate;
	
	String ticketNum;
	
	Integer itemNum;
	
	String itemCode;
	
	boolean allowModifyFlag;
	
	boolean uncollectFlag;
	
	DispOrderItemStatus status;
	
	Integer elapseTime;
	
	String suspendCode; 
	
	String suspendDesc;
	
	String suspendUpdateUser;
	
	String statusDetailUpdateUser;
	
	Date ticketIssueDate;
	
	PharmOrderAdminStatus pharmOrderAdminStatus;
	
	DispOrderAdminStatus dispOrderAdminStatus;

	AllowCommentType allowCommentType;
	
	String vetUser;
	
	String pickUser;
	
	String assembleUser;
	
	String checkUser;
	
	String issueUser;
	
	String caseNum; //for print report
	
	String hkid; //for print report
	
	String patName; //for print report
	
	MedCase medCase; //for print report
	
	String dispQty;
	
	boolean ddFlag;
	
	Boolean revokeFlag;
	
	public EdsStatusDetailRpt(Date dispDate, String ticketNum, Integer itemNum, String itemCode, 
								DispOrderItemStatus status, String suspendCode, String suspendUpdateUser,
								Date ticketIssueDate, PharmOrderAdminStatus pharmOrderAdminStatus, 
								DispOrderAdminStatus dispOrderAdminStatus, AllowCommentType allowCommentType, 
								String vetUser, String pickUser, String assembleUser, String checkUser, String issueUser,
								MedCase medCase, String patName, String hkid, String caseNum, BigDecimal dispQty, Boolean ddFlag, Boolean revokeFlag) {
		this.dispDate = dispDate;
		this.ticketNum = ticketNum;
		this.itemNum = itemNum;
		this.itemCode = itemCode;
		this.status = status;
		this.suspendCode = suspendCode;
		this.suspendUpdateUser = suspendUpdateUser;
		this.ticketIssueDate = ticketIssueDate;
		this.pharmOrderAdminStatus = pharmOrderAdminStatus;
		this.dispOrderAdminStatus = dispOrderAdminStatus;
		this.allowCommentType = allowCommentType;
		this.vetUser = vetUser;
		this.pickUser = pickUser;
		this.assembleUser = assembleUser;
		this.checkUser = checkUser;
		this.issueUser = issueUser;
		this.medCase = medCase;
		this.patName = patName;
		this.hkid = hkid;
		this.caseNum = caseNum;
		this.dispQty = dispQty.toString();
		this.ddFlag = ddFlag;
		this.revokeFlag = revokeFlag;
	}

	public String getDispQty() {
		return dispQty;
	}

	public void setDispQty(String dispQty) {
		this.dispQty = dispQty;
	}

	public Date getDispDate() {
		return dispDate;
	}

	public void setDispDate(Date dispDate) {
		this.dispDate = dispDate;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}	
	
	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public boolean isAllowModifyFlag() {
		return allowModifyFlag;
	}

	public void setAllowModifyFlag(boolean allowModifyFlag) {
		this.allowModifyFlag = allowModifyFlag;
	}

	public boolean isUncollectFlag() {
		return uncollectFlag;
	}

	public void setUncollectFlag(boolean uncollectFlag) {
		this.uncollectFlag = uncollectFlag;
	}

	public DispOrderItemStatus getStatus() {
		return status;
	}

	public void setStatus(DispOrderItemStatus status) {
		this.status = status;
	}

	public Integer getElapseTime() {
		return elapseTime;
	}

	public void setElapseTime(Integer elapseTime) {
		this.elapseTime = elapseTime;
	}

	public String getSuspendCode() {
		return suspendCode;
	}

	public void setSuspendCode(String suspendCode) {
		this.suspendCode = suspendCode;
	}

	public String getSuspendDesc() {
		return suspendDesc;
	}

	public void setSuspendDesc(String suspendDesc) {
		this.suspendDesc = suspendDesc;
	}

	public String getSuspendUpdateUser() {
		return suspendUpdateUser;
	}

	public void setSuspendUpdateUser(String suspendUpdateUser) {
		this.suspendUpdateUser = suspendUpdateUser;
	}

	public String getStatusDetailUpdateUser() {
		return statusDetailUpdateUser;
	}

	public void setStatusDetailUpdateUser(String statusDetailUpdateUser) {
		this.statusDetailUpdateUser = statusDetailUpdateUser;
	}

	public Date getTicketIssueDate() {
		return ticketIssueDate;
	}

	public void setTicketIssueDate(Date ticketIssueDate) {
		this.ticketIssueDate = ticketIssueDate;
	}

	public PharmOrderAdminStatus getPharmOrderAdminStatus() {
		return pharmOrderAdminStatus;
	}

	public void setPharmOrderAdminStatus(PharmOrderAdminStatus pharmOrderAdminStatus) {
		this.pharmOrderAdminStatus = pharmOrderAdminStatus;
	}

	public DispOrderAdminStatus getDispOrderAdminStatus() {
		return dispOrderAdminStatus;
	}

	public void setDispOrderAdminStatus(DispOrderAdminStatus dispOrderAdminStatus) {
		this.dispOrderAdminStatus = dispOrderAdminStatus;
	}

	public AllowCommentType getAllowCommentType() {
		return allowCommentType;
	}

	public void setAllowCommentType(AllowCommentType allowCommentType) {
		this.allowCommentType = allowCommentType;
	}
	
	public String getVetUser() {
		return vetUser;
	}

	public void setVetUser(String vetUser) {
		this.vetUser = vetUser;
	}

	public String getPickUser() {
		return pickUser;
	}

	public void setPickUser(String pickUser) {
		this.pickUser = pickUser;
	}

	public String getAssembleUser() {
		return assembleUser;
	}

	public void setAssembleUser(String assembleUser) {
		this.assembleUser = assembleUser;
	}

	public String getCheckUser() {
		return checkUser;
	}

	public void setCheckUser(String checkUser) {
		this.checkUser = checkUser;
	}

	public String getIssueUser() {
		return issueUser;
	}

	public void setIssueUser(String issueUser) {
		this.issueUser = issueUser;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public MedCase getMedCase() {
		return medCase;
	}

	public void setMedCase(MedCase medCase) {
		this.medCase = medCase;
	}	

	public boolean isDdFlag() {
		return ddFlag;
	}

	public void setDdFlag(boolean ddFlag) {
		this.ddFlag = ddFlag;
	}

	public Boolean getRevokeFlag() {
		return revokeFlag;
	}

	public void setRevokeFlag(Boolean revokeFlag) {
		this.revokeFlag = revokeFlag;
	}
}
