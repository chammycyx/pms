package hk.org.ha.model.pms.biz.alert.druginfo;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.alert.druginfo.DrugInfoException;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.dms.persistence.DmDrugProperty;
import hk.org.ha.model.pms.dms.vo.Drug;
import hk.org.ha.model.pms.dms.vo.DrugMds;
import hk.org.ha.model.pms.dms.vo.DrugMdsCriteria;
import hk.org.ha.model.pms.dms.vo.DrugName;
import hk.org.ha.model.pms.dms.vo.DsfSearchCriteria;
import hk.org.ha.model.pms.dms.vo.PreparationProperty;
import hk.org.ha.model.pms.dms.vo.PreparationSearchCriteria;
import hk.org.ha.model.pms.dms.vo.RenalAdj;
import hk.org.ha.model.pms.dms.vo.RouteForm;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.alert.druginfo.ClassificationNode;
import hk.org.ha.model.pms.vo.alert.druginfo.DrugInteraction;
import hk.org.ha.model.pms.vo.alert.druginfo.FoodInteraction;
import hk.org.ha.model.pms.vo.alert.druginfo.PatientEducation;
import hk.org.ha.model.pms.vo.alert.mds.RetrievePreparationPropertyListResult;
import hk.org.ha.model.pms.vo.rx.DhRxDrug;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;
import hk.org.ha.service.pms.asa.interfaces.alert.AlertServiceJmsRemote;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("drugInfoService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DrugInfoServiceBean implements DrugInfoServiceLocal {

	private final JaxbWrapper<DhRxDrug> rxJaxbWrapper = JaxbWrapper.instance("hk.org.ha.model.pms.vo.rx");
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private AlertServiceJmsRemote alertServiceProxy;
	
	@In
	private Workstore workstore;
	
	private RenalAdj renalAdj;
	
	public List<ClassificationNode> retrieveDrugClassificationList(Integer gcnSeqNum, Integer rdfgenId, Integer rgenId, String patHospCode, List<PreparationProperty> preparationPropertyList) throws DrugInfoException {
		return alertServiceProxy.retrieveDrugClassificationList(gcnSeqNum, rdfgenId, rgenId, patHospCode, constructDrugMdsList(preparationPropertyList));
	}

	public String retrieveSideEffect(Integer gcnSeqNum, Integer rdfgenId, Integer rgenId, String patHospCode, List<PreparationProperty> preparationPropertyList) throws DrugInfoException {
		return alertServiceProxy.retrieveSideEffect(gcnSeqNum, rdfgenId, rgenId, patHospCode, constructDrugMdsList(preparationPropertyList));
	}

	public String retrieveContraindication(Integer gcnSeqNum, Integer rdfgenId, Integer rgenId, String patHospCode, List<PreparationProperty> preparationPropertyList) throws DrugInfoException {
		return alertServiceProxy.retrieveContraindication(gcnSeqNum, rdfgenId, rgenId, patHospCode, constructDrugMdsList(preparationPropertyList));
	}

	public String retrievePrecaution(Integer gcnSeqNum, Integer rdfgenId, Integer rgenId, String patHospCode, List<PreparationProperty> preparationPropertyList) throws DrugInfoException {
		return alertServiceProxy.retrievePrecaution(gcnSeqNum, rdfgenId, rgenId, patHospCode, constructDrugMdsList(preparationPropertyList));
	}

	public List<DrugInteraction> retrieveDrugInteraction(Integer gcnSeqNum, Integer rdfgenId, Integer rgenId, String patHospCode, List<PreparationProperty> preparationPropertyList) throws DrugInfoException {
		return alertServiceProxy.retrieveDrugInteraction(gcnSeqNum, rdfgenId, rgenId, patHospCode, constructDrugMdsList(preparationPropertyList));
	}

	public List<FoodInteraction> retrieveFoodInteraction(Integer gcnSeqNum, Integer rdfgenId, Integer rgenId, String patHospCode, List<PreparationProperty> preparationPropertyList) throws DrugInfoException {
		return alertServiceProxy.retrieveFoodInteraction(gcnSeqNum, rdfgenId, rgenId, patHospCode, constructDrugMdsList(preparationPropertyList));
	}

	public List<PatientEducation> retrievePatientEducation(Integer gcnSeqNum, Integer rdfgenId, Integer rgenId, String patHospCode, List<PreparationProperty> preparationPropertyList) throws DrugInfoException {
		return alertServiceProxy.retrievePatientEducation(gcnSeqNum, rdfgenId, rgenId, patHospCode, constructDrugMdsList(preparationPropertyList));
	}

	public String retrieveCommonOrder(Integer gcnSeqNum, Integer rdfgenId, Integer rgenId, String patHospCode, List<PreparationProperty> preparationPropertyList) throws DrugInfoException {
		return alertServiceProxy.retrieveCommonOrder(gcnSeqNum, rdfgenId, rgenId, patHospCode, constructDrugMdsList(preparationPropertyList));
	}

	public String retrieveDosageRange(Integer gcnSeqNum, Integer rdfgenId, Integer rgenId, String patHospCode, Integer age, String ageUnit, List<PreparationProperty> preparationPropertyList, Integer selectedStrengthIndex) throws DrugInfoException {
		return alertServiceProxy.retrieveDosageRange(gcnSeqNum, rdfgenId, rgenId, patHospCode, age, ageUnit, constructDrugMdsList(preparationPropertyList), selectedStrengthIndex);
	}
	
	public String getRenalAdjCode() {
		return renalAdj.getRenalAdjCode();
	}
	
	public Date getRenalReviewDate() {
		return renalAdj.getReviewDate();
	}		
	
	public DrugMds retrieveDrugMdsByItemCode(String itemCode, boolean checkHq, boolean checkRenal) {
		
		Drug[] drugList;
		if (checkHq) {
			drugList = dmsPmsServiceProxy.getDrugMdsPropertyHq(constructDrugMdsCriteria(itemCode));
		}
		else {
			drugList = dmsPmsServiceProxy.getDrugMdsProperty(constructDrugMdsCriteria(itemCode));
		}
		
		if (checkRenal) {
			DmDrugProperty dmDrugProperty = DmDrugCacher.instance().getDmDrug(itemCode).getDmDrugProperty();
			renalAdj = dmsPmsServiceProxy.retrieveRenalAdj(dmDrugProperty.getDisplayname(), dmDrugProperty.getFormCode(), dmDrugProperty.getSaltProperty());
			renalAdj = renalAdj == null ? new RenalAdj() : renalAdj;
		}
		
		DrugMds drugMds = extractDrugMds(drugList, itemCode);
		return checkSameRouteInd(drugMds);
	}
	
	public DrugMds retrieveDrugMdsByDisplayNameFormSaltFmStatus(String displayName, String formCode, String saltProperty, String fmStatus, boolean checkHq, boolean checkRenal) {
		
		Drug[] drugList;
		if (checkHq) {
			drugList = dmsPmsServiceProxy.getDrugMdsPropertyHq(constructDrugMdsCriteria(displayName, formCode, saltProperty));
		}
		else {
			drugList = dmsPmsServiceProxy.getDrugMdsProperty(constructDrugMdsCriteria(displayName, formCode, saltProperty));
		}
		
		if (checkRenal) {
			renalAdj = dmsPmsServiceProxy.retrieveRenalAdj(displayName, formCode, saltProperty);
			renalAdj = renalAdj == null ? new RenalAdj() : renalAdj;
		}
		
		DrugMds drugMds = extractDrugMds(drugList, displayName, formCode, saltProperty, fmStatus, checkHq);
		return checkSameRouteInd(drugMds);
	}
	
	public DrugMds retrieveDrugMdsByDhRxDrug(DhRxDrug dhRxDrug, boolean checkHq, boolean checkRenal) {
		List<String> rxItemXmlList = dmsPmsServiceProxy.retrieveHaDrugMappingInfoForDhOrder(Arrays.asList(rxJaxbWrapper.marshall(dhRxDrug)));
		if (rxItemXmlList != null && rxItemXmlList.size() > 0) {
			DhRxDrug dhRxDrugOut = rxJaxbWrapper.unmarshall(rxItemXmlList.get(0));			
			if (StringUtils.isBlank(dhRxDrugOut.getItemCode())) {
				return retrieveDrugMdsByDisplayNameFormSaltFmStatus(dhRxDrugOut.getDisplayName(), dhRxDrugOut.getFormCode(), dhRxDrugOut.getSaltProperty(), dhRxDrugOut.getFmStatus(), checkHq, checkRenal);
			} else {
				return retrieveDrugMdsByItemCode(dhRxDrugOut.getItemCode(), checkHq, checkRenal);
			}
		}
		return null;
	}
	
	public List<PreparationProperty> retrievePreparationPropertyListForOrderEntry(String displayName, String formCode, String saltProperty, String fmStatus, String aliasName, String firstDisplayName, Boolean checkHq) {
		return filterPreparationList(dmsPmsServiceProxy.retrievePreparation(constructPreparationSearchCriteria(displayName, formCode, saltProperty, fmStatus, aliasName, firstDisplayName, checkHq)));
	}
	
	public List<PreparationProperty> retrievePreparationPropertyListForDrugSearch(DrugName drugName, RouteForm routeForm, PreparationProperty preparation) {
		return filterPreparationList(dmsPmsServiceProxy.retrievePreparation(constructPreparationSearchCriteria(drugName, routeForm, preparation)));
	}
	
	public RetrievePreparationPropertyListResult retrievePreparationForDhOrder(DhRxDrug dhRxDrug) {
		List<String> rxItemXmlList = dmsPmsServiceProxy.retrieveHaDrugMappingInfoForDhOrder(Arrays.asList(rxJaxbWrapper.marshall(dhRxDrug)));
		if (rxItemXmlList == null || rxItemXmlList.isEmpty()) {
			return null;
		}
		DhRxDrug dhRxDrugOut = rxJaxbWrapper.unmarshall(rxItemXmlList.get(0));
		RetrievePreparationPropertyListResult result = new RetrievePreparationPropertyListResult();
		result.setPreparationPropertyList(retrievePreparationPropertyListForOrderEntry(dhRxDrugOut.getDisplayName(), dhRxDrugOut.getFormCode(), dhRxDrugOut.getSaltProperty(), dhRxDrugOut.getFmStatus(), dhRxDrugOut.getAliasName(), dhRxDrugOut.getFirstDisplayName(), true));
		result.setRxItem(dhRxDrugOut);
		return result;
	}

	public String retrieveMonograph(Integer monoId, Integer monoType, String monoVersion, String patHospCode) throws DrugInfoException {
		return alertServiceProxy.retrieveMonograph(monoId, monoType, monoVersion, patHospCode);
	}
	
	private PreparationSearchCriteria constructPreparationSearchCriteria(DrugName drugName, RouteForm routeForm, PreparationProperty preparation) {
		return constructPreparationSearchCriteria(
				drugName.getTrueDisplayname(),
				routeForm.getFormCode(),
				routeForm.getSaltProperty(),
				preparation == null ? routeForm.getPmsFmStatus() : preparation.getPmsFmStatus(),
				routeForm.getTrueAliasname(),
				drugName.getFirstDisplayname(),
				false
			);
	}
	
	private PreparationSearchCriteria constructPreparationSearchCriteria(String displayName, String formCode, String saltProperty, String fmStatus, String aliasName, String firstDisplayName, Boolean checkHq) {
		PreparationSearchCriteria criteria = new PreparationSearchCriteria();
		criteria.setCostIncluded(Boolean.TRUE);
		criteria.setDispHospCode(workstore.getHospCode());
		criteria.setDispWorkstore(workstore.getWorkstoreCode());
		criteria.setDrugScope("I");
		criteria.setTrueDisplayname(displayName);				
		criteria.setFormCode(formCode);
		criteria.setSaltProperty(saltProperty);
		criteria.setPmsFmStatus(checkHq ? null : fmStatus);
		criteria.setSpecialtyType("O");
		criteria.setPasSpecialty("");				 			
		criteria.setPasSubSpecialty("");																
		criteria.setHqFlag(checkHq);
		criteria.setTrueAliasname(aliasName);
		criteria.setFirstDisplayname(checkHq ? null : firstDisplayName);
		return criteria;
	}

	private List<DrugMds> constructDrugMdsList(List<PreparationProperty> preparationPropertyList) {
		List<DrugMds> drugMdsList = new ArrayList<DrugMds>();
		for (PreparationProperty preparationProperty : preparationPropertyList) {
			drugMdsList.add(preparationProperty.getDrugMds());
		}
		return drugMdsList;
	}
	
	private DrugMds extractDrugMds(Drug[] drugList, String displayName, String formCode, String saltProperty, String fmStatus, boolean ignoreFm) {
		for (Drug drug : drugList) {
			if (StringUtils.equals(drug.getDrugProperty().getDisplayname(), displayName)
			 && StringUtils.equals(drug.getDrugProperty().getFormCode(), formCode)
			 && StringUtils.equals(drug.getDrugProperty().getSaltProperty(), saltProperty)
			 && (StringUtils.equals(drug.getPmsFmStatus(), fmStatus) || StringUtils.isBlank(drug.getPmsFmStatus()) || ignoreFm)) {
				return drug.getDrugMds();
			}
		}
		return null;
	}

	private DrugMds extractDrugMds(Drug[] drugList, String itemCode) {
		for (Drug drug : drugList) {
			if (StringUtils.equals(drug.getItemCode(), itemCode)) {
				return drug.getDrugMds();
			}
		}
		return null;
	}
	
	private DrugMds checkSameRouteInd(DrugMds drugMds) {
		if (drugMds != null && "N".equals(drugMds.getSameRouteInd())) {
			return null;
		}
		return drugMds;
	}
	
	private DrugMdsCriteria constructDrugMdsCriteria(String displayName, String formCode, String saltProperty) {
		
		List<DsfSearchCriteria> dsfSearchCriteriaList = constructDsfSearchCriteriaListForDrugMdsProperty(displayName, formCode, saltProperty);
		
		DrugMdsCriteria drugMdsCriteria = new DrugMdsCriteria();
		drugMdsCriteria.setDispHospCode(workstore.getHospCode());
		drugMdsCriteria.setDispWorkstore(workstore.getWorkstoreCode());
		drugMdsCriteria.setDsfSearchCriteria(dsfSearchCriteriaList.toArray(new DsfSearchCriteria[dsfSearchCriteriaList.size()]));
		
		return drugMdsCriteria;
	}
	
	private DrugMdsCriteria constructDrugMdsCriteria(String itemCode) {
		
		DrugMdsCriteria drugMdsCriteria = new DrugMdsCriteria();
		drugMdsCriteria.setDispHospCode(workstore.getHospCode());
		drugMdsCriteria.setDispWorkstore(workstore.getWorkstoreCode());
		drugMdsCriteria.setItemCode(new String[]{itemCode});
		
		return drugMdsCriteria;
	}

	private List<DsfSearchCriteria> constructDsfSearchCriteriaListForDrugMdsProperty(String displayName, String formCode, String saltProperty) {
		DsfSearchCriteria dsfCriteria = new DsfSearchCriteria();
		dsfCriteria.setDispHospCode(workstore.getHospCode());
		dsfCriteria.setDispWorkstore(workstore.getWorkstoreCode());
		dsfCriteria.setDisplayname(displayName);
		dsfCriteria.setFormCode(formCode);
		dsfCriteria.setSaltProperty(saltProperty);
		return Arrays.asList(dsfCriteria);
	}

	private List<PreparationProperty> filterPreparationList(List<PreparationProperty> preparationPropertyList) {
		Map<String, PreparationProperty> map = new LinkedHashMap<String, PreparationProperty>();
		for (PreparationProperty p : preparationPropertyList) {
			map.put(p.getItemCode(), p);
		}
		return new ArrayList<PreparationProperty>(map.values());
	}

	@Remove
	public void destroy() {
	}

}
