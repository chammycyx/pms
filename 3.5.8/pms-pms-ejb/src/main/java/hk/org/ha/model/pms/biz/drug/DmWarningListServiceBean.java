package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.reftable.WarningManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmWarningCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmWarning;
import hk.org.ha.model.pms.persistence.corp.Workstore;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("dmWarningListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DmWarningListServiceBean implements DmWarningListServiceLocal {

	@Logger
	private Log logger;

	@In
	private Workstore workstore;

	@In
	private DmWarningCacherInf dmWarningCacher;	
	
	@In
	private WarningManagerLocal warningManager;
	
	@In(required = false)
	@Out(required = false)
	private List<DmWarning> dmWarningList;		

	public void retrieveDmWarningList(String prefixWarnCode)
	{
		if(StringUtils.isEmpty(prefixWarnCode)){
			dmWarningList = dmWarningCacher.getDmWarningList();
		}else{
			dmWarningList = dmWarningCacher.getDmWarningListByWarnCode(prefixWarnCode);
		}
	}
	
	public List<DmWarning> retrieveDmWarningListForOrderEdit(String itemCode) {
		List<DmWarning> warningList = null;
		List<DmWarning> localWarningList = warningManager.retrieveLocalWarningList(itemCode, workstore.getWorkstoreGroup());
		if ( localWarningList != null ) {
			warningList = localWarningList;
		} else {
			warningList = warningManager.retrieveMoeWarmingList(itemCode);			
		}
		return warningList;
	}
	
	@Remove
	public void destroy() 
	{
		if (dmWarningList != null) {
			dmWarningList = null;
		}
	}

}
