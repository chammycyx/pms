package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum RemarkStatus implements StringValuedEnum {

	None("-", "None"),
	Confirm("C", "Pharmacy Remarks confirmed"),
	Unconfirm("U", "Unconfirm Pharmacy Remarks");

    private final String dataValue;
    private final String displayValue;

    RemarkStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static RemarkStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(RemarkStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<RemarkStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<RemarkStatus> getEnumClass() {
    		return RemarkStatus.class;
    	}
    }
}
