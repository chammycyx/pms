package hk.org.ha.model.pms.persistence;

import hk.org.ha.fmk.pms.entity.CreateDate;
import hk.org.ha.fmk.pms.entity.CreateUser;
import hk.org.ha.fmk.pms.entity.UpdateDate;
import hk.org.ha.fmk.pms.entity.UpdateUser;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

@MappedSuperclass
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@UpdateDate
	@Column(name = "UPDATE_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@XmlTransient
	// change to public to avoid exception from Reflections.setAndWrap(Reflections.java:131)	
	public Date updateDate;

	@CreateDate
	@Column(name = "CREATE_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@XmlTransient
	// change to public to avoid exception from Reflections.setAndWrap(Reflections.java:131)	
	public Date createDate;

	@UpdateUser
	@Column(name = "UPDATE_USER", nullable = false, length = 12)
	@XmlTransient
	// change to public to avoid exception from Reflections.setAndWrap(Reflections.java:131)	
	public String updateUser;

	@CreateUser
	@Column(name = "CREATE_USER", nullable = false, length = 12)
	@XmlTransient
	// change to public to avoid exception from Reflections.setAndWrap(Reflections.java:131)	
	public String createUser; 

	public Date getUpdateDate() {
		return cloneDate(updateDate);
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = cloneDate(updateDate);
	}

	public Date getCreateDate() {
		return cloneDate(createDate);
	}

	public void setCreateDate(Date createDate) {
		this.createDate = cloneDate(createDate);
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	
	private Date cloneDate(Date date) {
		return (date != null) ? new Date(date.getTime()) : null;
	}
}
