package hk.org.ha.model.pms.vo.medprofile;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;


@ExternalizedBean(type = DefaultExternalizer.class)
public class CheckConflictItem implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Long medProfileItemId;
	private Integer orgItemNum;
	private Integer itemNum;
	private Date endDate;
	private List<String> itemCodeList;
	
	public Long getMedProfileItemId() {
		return medProfileItemId;
	}
	
	public void setMedProfileItemId(Long medProfileItemId) {
		this.medProfileItemId = medProfileItemId;
	}
	
	public Integer getOrgItemNum() {
		return orgItemNum;
	}

	public void setOrgItemNum(Integer orgItemNum) {
		this.orgItemNum = orgItemNum;
	}
	
	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public Date getEndDate() {
		return endDate == null ? null : new Date(endDate.getTime());
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate == null ? null : new Date(endDate.getTime());
	}

	public List<String> getItemCodeList() {
		if (itemCodeList == null) {
			itemCodeList = new ArrayList<String>();
		}
		return itemCodeList;
	}

	public void setItemCodeList(List<String> itemCodeList) {
		this.itemCodeList = itemCodeList;
	}
}
