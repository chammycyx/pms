package hk.org.ha.model.pms.vo.vetting;

import hk.org.ha.model.pms.udt.disp.OnHandSourceType;
import hk.org.ha.model.pms.udt.vetting.CmsOrderSubType;
import hk.org.ha.model.pms.udt.vetting.CmsOrderType;
import hk.org.ha.model.pms.udt.vetting.OnHandProfileOrderType;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class OnHandProfileCriteria implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String patHospCode;
	
	private String patKey;
	
	private Date orderDate;
	
	private CmsOrderType orderType;
	
	private CmsOrderSubType orderSubType;
	
	private String caseNum;
	
	private String hkid;
	
	private Boolean pasPatientEnabled;
	
	private OnHandSourceType onHandSourceType;
	
	private Boolean corpOnHandEnabled;
	
	private OnHandProfileOrderType onHandProfileOrderType;

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public String getPatKey() {
		return patKey;
	}

	public void setPatKey(String patKey) {
		this.patKey = patKey;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public CmsOrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(CmsOrderType orderType) {
		this.orderType = orderType;
	}

	public CmsOrderSubType getOrderSubType() {
		return orderSubType;
	}

	public void setOrderSubType(CmsOrderSubType orderSubType) {
		this.orderSubType = orderSubType;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public Boolean getPasPatientEnabled() {
		return pasPatientEnabled;
	}

	public void setPasPatientEnabled(Boolean pasPatientEnabled) {
		this.pasPatientEnabled = pasPatientEnabled;
	}

	public OnHandSourceType getOnHandSourceType() {
		return onHandSourceType;
	}

	public void setOnHandSourceType(OnHandSourceType onHandSourceType) {
		this.onHandSourceType = onHandSourceType;
	}

	public Boolean getCorpOnHandEnabled() {
		return corpOnHandEnabled;
	}

	public void setCorpOnHandEnabled(Boolean corpOnHandEnabled) {
		this.corpOnHandEnabled = corpOnHandEnabled;
	}

	public OnHandProfileOrderType getOnHandProfileOrderType() {
		return onHandProfileOrderType;
	}

	public void setOnHandProfileOrderType(
			OnHandProfileOrderType onHandProfileOrderType) {
		this.onHandProfileOrderType = onHandProfileOrderType;
	}
	
}
