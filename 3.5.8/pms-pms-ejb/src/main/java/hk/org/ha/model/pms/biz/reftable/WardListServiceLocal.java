package hk.org.ha.model.pms.biz.reftable;

import javax.ejb.Local;

@Local
public interface WardListServiceLocal {

	void retrieveWardListByPrefixWardCode(String prefixWardCode);
	
	void destroy();
	
}
