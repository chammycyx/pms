package hk.org.ha.model.pms.biz.inbox;

import static hk.org.ha.model.pms.prop.Prop.INBOX_DISCHARGEORDER_DURATION_LIMIT;
import static hk.org.ha.model.pms.prop.Prop.INBOX_HISTORICALDISCHARGEORDER_DURATION_LIMIT;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.biz.reftable.MpWardManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.medprofile.DischargeMedOrderListType;
import hk.org.ha.model.pms.vo.medprofile.Ward;
import hk.org.ha.model.pms.vo.medprofile.WardGroup;

import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;
import org.springframework.util.CollectionUtils;

/**
 * Session Bean implementation class DischargeMedOrderServiceBean
 */
@Stateful
@Scope(ScopeType.SESSION)
@Name("dischargeMedOrderService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DischargeMedOrderServiceBean implements DischargeMedOrderServiceLocal {
	
	private static final String MED_ORDER_QUERY_HINT_BATCH_1 = "o.patient";
	private static final String MED_ORDER_QUERY_HINT_BATCH_2 = "o.medCase";
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private PharmInboxManagerLocal pharmInboxManager;
	
	@In
	private MpWardManagerLocal mpWardManager;
	
	@In
	private CategorySummaryLocal categorySummary;
	
	@In
	private Workstation workstation;
	
	@In
	public Workstore workstore;

	@In @Out
	private Boolean showAllWards;
	
	@In
	private Boolean showChemoProfileOnly;
	
	@Out(required = false)
	private List<MedOrder> dischargeMedOrderList;
	
	private DischargeMedOrderListType dischargeMedOrderListType;
	
	private List<Ward> wardFilterList;
	
	private List<WardGroup> wardGroupFilterList;
	
	private String hkidCaseNum;
	
    public DischargeMedOrderServiceBean() {
    }

    @Override
    public void setShowAllWards(Boolean showAllWards)
    {
    	this.showAllWards = showAllWards;
    	pharmInboxManager.setShowAllWards(workstation, showAllWards);
    	retrieveDischargeMedOrderList();
    }
    
    @Override
    public void setDischargeTypesAndFilters(DischargeMedOrderListType listType, List<Ward> wardFilterList,
    		List<WardGroup> wardGroupFilterList, String hkidCaseNum) {
    	this.dischargeMedOrderListType = listType;
    	this.wardFilterList = wardFilterList;
    	this.wardGroupFilterList = wardGroupFilterList;
    	this.hkidCaseNum = hkidCaseNum;
    	retrieveDischargeMedOrderList();
    }
    
    @Override
    public String setUrgent(Long id, Boolean urgent, Long version) {
    	
    	MedOrder medOrder = fetchMedOrder(id);
    	if (medOrder != null) {
    		if (!Boolean.TRUE.equals(medOrder.getProcessingFlag())) {
    			if (!medOrder.getVersion().equals(version))
    			{
    				return "0590";
    			}
        		medOrder.setUrgentFlag(urgent);
        		em.merge(medOrder);
        		em.flush();
        		markMedOrderUrgentFlagInSession(medOrder);
        		return null;
    		} else if (medOrder.getWorkstationId() != null) {
    			return em.find(Workstation.class, medOrder.getWorkstationId()).getHostName();
    		}
    	}
    	return "";
    }
    
    private void markMedOrderUrgentFlagInSession(MedOrder medOrder) {

    	for (Iterator<MedOrder> itr = dischargeMedOrderList.iterator(); itr.hasNext();)
		{
			MedOrder orignalMedOrder = (MedOrder) itr.next();
			if (orignalMedOrder.getId().equals(medOrder.getId())) 
			{
				int itemIndex = dischargeMedOrderList.indexOf(orignalMedOrder);
				itr.remove();
				dischargeMedOrderList.add(itemIndex, medOrder);
				break;
			}
		}
    }
    
	@SuppressWarnings("unchecked")
	private MedOrder fetchMedOrder(Long id) {
    	
    	MedOrder medOrder = (MedOrder) MedProfileUtils.getFirstItem(em.createQuery(
    			"select o from MedOrder o" + // 20150204 index check : MedOrder.id : PK_MED_ORDER
    			" where o.id in :id")
    			.setParameter("id", Arrays.asList(id))
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
				.setHint(QueryHints.BATCH, MED_ORDER_QUERY_HINT_BATCH_1)
				.setHint(QueryHints.BATCH, MED_ORDER_QUERY_HINT_BATCH_2)	
    			.getResultList());
    	
    	MedProfileUtils.lookup(medOrder, MED_ORDER_QUERY_HINT_BATCH_1);
    	MedProfileUtils.lookup(medOrder, MED_ORDER_QUERY_HINT_BATCH_2);
    	
    	return medOrder;
    }
    
    @Override
    public void retrieveDischargeMedOrderList() {
    	
		List<String> wardCodeList;
		List<String> wardCodeListForCounting;
		String hkidCaseNum = null;
    	
    	if (dischargeMedOrderListType == DischargeMedOrderListType.All) {
    		wardCodeList = mpWardManager.retrieveWardFilterList(workstore, wardFilterList, wardGroupFilterList);
    		wardCodeListForCounting = mpWardManager.retrieveWardFilterList(workstore, showAllWards);
    		hkidCaseNum = this.hkidCaseNum;
    	} else {
			wardCodeList = mpWardManager.retrieveWardFilterList(workstore, showAllWards);
			wardCodeListForCounting = wardCodeList; 
    	}

    	dischargeMedOrderList = retrieveDischargeMedOrderList(dischargeMedOrderListType == DischargeMedOrderListType.History,
    			wardCodeList, hkidCaseNum);
    	categorySummary.countCategories(null, workstore, showChemoProfileOnly, wardCodeListForCounting);
    }
    
    @SuppressWarnings("unchecked")
	private List<MedOrder> retrieveDischargeMedOrderList(boolean history, List<String> wardCodeList, String hkidCaseNum) {

		boolean wardExists = !CollectionUtils.isEmpty(wardCodeList);
		
    	Query query = em.createQuery(
    			"select o from MedOrder o" + // 20150107 index check : MedOrder.hospCode,prescType,status,preVetDate : I_MED_ORDER_08
    			" where o.prescType in :prescTypeList" +
    			" and o.status in :statusList" +
    			" and o.remarkCreateDate is null" +
    			" and o.hospCode = :hospCode" +
    			(history ? " and o.preVetDate > :startDate " : " and o.orderDate > :startDate") +
    			(hkidCaseNum != null ? " and (o.medCase.caseNum = :hkidCaseNum or o.patient.hkid = :hkidCaseNum)" :
    			" and (" + (wardExists ? "o.wardCode in :wardCodeList or" : "") +
    			"   o.wardCode is null)") + 
    			" order by " + (history ? "o.preVetDate desc" : " o.urgentFlag desc, o.orderDate"))
    			.setParameter("prescTypeList", MedOrderPrescType.MpDischarge_MpHomeLeave)
				.setParameter("statusList", history ? Arrays.asList(MedOrderStatus.PreVet) : MedOrderStatus.Outstanding_Withhold)
				.setParameter("startDate", MedProfileUtils.addDays(new Date(), 
						(history ? INBOX_HISTORICALDISCHARGEORDER_DURATION_LIMIT.get(7) * -1 : INBOX_DISCHARGEORDER_DURATION_LIMIT.get(7) * -1)))
				.setParameter("hospCode", workstore.getHospCode())
				.setHint(QueryHints.BATCH, MED_ORDER_QUERY_HINT_BATCH_1)
				.setHint(QueryHints.BATCH, MED_ORDER_QUERY_HINT_BATCH_2);
    	
    	if (hkidCaseNum != null) {
    		query.setParameter("hkidCaseNum", hkidCaseNum);
    	} else if (wardExists) {
    		query.setParameter("wardCodeList", wardCodeList);
    	}
    	List<MedOrder> medOrderList = query.getResultList();
    	
    	MedProfileUtils.lookup(medOrderList, MED_ORDER_QUERY_HINT_BATCH_1);
    	MedProfileUtils.lookup(medOrderList, MED_ORDER_QUERY_HINT_BATCH_2);
    	
    	return medOrderList;
    }
    
	@Remove
	@Override
	public void destroy() {
		dischargeMedOrderList = null;
	}	    
}
