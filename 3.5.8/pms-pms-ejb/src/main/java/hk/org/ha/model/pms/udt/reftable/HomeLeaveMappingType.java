package hk.org.ha.model.pms.udt.reftable;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum HomeLeaveMappingType implements StringValuedEnum {

	Discharge("D", "Discharge"),
	HomeLeave("H", "Home Leave");	
	
    private final String dataValue;
    private final String displayValue;
        
    HomeLeaveMappingType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static HomeLeaveMappingType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(HomeLeaveMappingType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<HomeLeaveMappingType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<HomeLeaveMappingType> getEnumClass() {
    		return HomeLeaveMappingType.class;
    	}
    }
}
