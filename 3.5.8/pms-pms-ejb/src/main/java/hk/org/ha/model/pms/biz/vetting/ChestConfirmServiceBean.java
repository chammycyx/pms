package hk.org.ha.model.pms.biz.vetting;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.drug.ItemConvertManagerLocal;
import hk.org.ha.model.pms.biz.order.MedOrderManagerLocal;
import hk.org.ha.model.pms.biz.reftable.ChestManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmDailyFrequencyCacherInf;
import hk.org.ha.model.pms.corp.cache.DmSiteCacher;
import hk.org.ha.model.pms.corp.cache.DmSupplFrequencyCacherInf;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDailyFrequency;
import hk.org.ha.model.pms.dms.persistence.DmSite;
import hk.org.ha.model.pms.dms.persistence.DmSupplFrequency;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.reftable.Chest;
import hk.org.ha.model.pms.persistence.reftable.ChestItem;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.model.pms.vo.rx.Dose;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("chestConfirmService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ChestConfirmServiceBean implements ChestConfirmServiceLocal {
	
	private static final String DAY_ARRAY_FOR_LABEL_DESC[] = {"SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"};
	
	private static final String DAY_OF_WEEK_SUPPL_FREQ_CODE = "00021";
	
	@In
	private DmSiteCacher dmSiteCacher;	
	
	@In
	private DmDailyFrequencyCacherInf dmDailyFrequencyCacher;	
	
	@In
	private DmSupplFrequencyCacherInf dmSupplFrequencyCacher;
	
	@In
	private MedOrderManagerLocal medOrderManager;
	
	@In
	private ItemConvertManagerLocal itemConvertManager;
	
	@In
	private ChestManagerLocal chestManager;

	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;
	
	@In
	private Workstore workstore;
	
	@In
	private PharmOrder pharmOrder;
	
	@Out(required = false)
	private  List<Chest> vettingChestList;
	
	@Out(required = false)
	private Chest vettingChest;
	
	public void updatePharmOrderFromChest(PharmOrder inPharmOrder){

		pharmOrder.setMaxItemNum(inPharmOrder.getMaxItemNum());
		for (PharmOrderItem inPharmOrderItem : inPharmOrder.getPharmOrderItemList()) {
			inPharmOrderItem.setOrgItemNum(pharmOrder.getAndIncreaseMaxOrgItemNum());
			Dose dose = inPharmOrderItem.getRegimen().getDoseGroupList().get(0).getDoseList().get(0);
			dose.setSiteDesc(getSiteDesc(dose.getSiteCode()));
			dose.getDailyFreq().setDesc(getFreqDesc(dose.getDailyFreq().getCode()));
			if ( dose.getSupplFreq() != null ) {
				if (DAY_OF_WEEK_SUPPL_FREQ_CODE.equals(dose.getSupplFreq().getCode())) {
					dose.getSupplFreq().setDesc(getSupplFreqDescForWeekday(dose));
				}
				else {
					dose.getSupplFreq().setDesc(getSuppFreqDesc(dose.getSupplFreq().getCode()));
				}
			}
			pharmOrder.addPharmOrderItem(inPharmOrderItem);
		}
		
		List<MedOrderItem> moiList = itemConvertManager.convertChest(inPharmOrder.getPharmOrderItemList());

		MedOrder medOrder = pharmOrder.getMedOrder();
		
		for ( MedOrderItem medOrderItem : moiList )
		{
			Integer itemNum = medOrder.getAndIncreaseMaxItemNum();
			medOrderItem.setItemNum(itemNum);
			medOrderItem.setOrgItemNum(itemNum);

			medOrder.addMedOrderItem(medOrderItem);
			
			if (medOrderItem.getStatus().equals(MedOrderItemStatus.SysDeleted) ||
				medOrderItem.getStatus().equals(MedOrderItemStatus.Deleted) ||
				medOrderItem.getPharmOrderItemList().isEmpty()) {
				continue;
			}
			boolean allHasIssueQty = true;
			for ( PharmOrderItem pharmOrderItem : medOrderItem.getPharmOrderItemList() ) {
				if (pharmOrderItem.getIssueQty() == null) {
					allHasIssueQty = false;
					break;
				}
			}
			medOrderItem.setVetFlag(allHasIssueQty);
		}
		
		pharmOrder.setChestFlag(Boolean.TRUE);
		medOrderManager.setPharmOrder(pharmOrder);
		clearChest();
	}
	
	private String getSiteDesc(String siteCode) {
		DmSite dmSite = dmSiteCacher.getSiteBySiteCode(siteCode);
		if ( dmSite != null ) {
			return dmSite.getSiteDescEng();
		} else {
			return null;
		}
	}
	
	private String getFreqDesc(String freqCode) {
		DmDailyFrequency dmDailyFreq = dmDailyFrequencyCacher.getDmDailyFrequencyByDailyFreqCode(freqCode);
		if ( dmDailyFreq != null ) {
			return dmDailyFreq.getDailyFreqDesc();
		} else {
			return null;
		}
	}
	
	private String getSuppFreqDesc(String suppFreqCode) {
		DmSupplFrequency dmSupplFreq = dmSupplFrequencyCacher.getDmSupplFrequencyBySupplFreqCode(suppFreqCode);
		if ( dmSupplFreq != null ) {
			return dmSupplFreq.getSupplFreqDesc();
		} else {
			return null;
		}
	}	
	
	private String getSupplFreqDescForWeekday(Dose dose) {

		String param = dose.getSupplFreq().getParam()[0]; 
		
		StringBuilder supplFreqDesc = new StringBuilder();

		boolean sundayExist = false;
		if (param.charAt(0) == '1') {
			// buffer Sunday and add at last
			sundayExist = true;
		}
		List<String> dayOfWeekList = new ArrayList<String>();
		for (int i = 1; i < param.length(); i++) {
			// add from Monday to Saturday
			if (param.charAt(i) == '1') {
				dayOfWeekList.add(DAY_ARRAY_FOR_LABEL_DESC[i]);
			}
		}
		if (sundayExist) {
			// add Sunday
			dayOfWeekList.add(DAY_ARRAY_FOR_LABEL_DESC[0]);
		}
		
		for (int i = 0; i < dayOfWeekList.size(); i++) {
			if (i > 0) {
				if (i == dayOfWeekList.size() - 1) {
					supplFreqDesc.append(" and ");
				} else {
					supplFreqDesc.append(", ");
				}
			}
			supplFreqDesc.append(dayOfWeekList.get(i));
		}
		
		return supplFreqDesc.toString();
	}
	
	public int retrieveChestList() {
		vettingChestList = chestManager.retrieveChestList();
		return pharmOrder.getMaxItemNum();
	}
	
	public void retrieveChest(String unitDoseName) {
		vettingChest = chestManager.retrieveChest(unitDoseName);
		
		for ( ChestItem chestItem : vettingChest.getChestItemList() ) {
			WorkstoreDrug workstoreDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, chestItem.getItemCode());
			if ( workstoreDrug != null ) {
				chestItem.setDmDrugLite(workstoreDrug.getDmDrugLite());
			}
		}
	}
	
	public void clearChest() {
		vettingChestList = null;
		vettingChest = null;
	}
	
	@Remove
	public void destroy() 
	{
		if ( vettingChestList != null ) {
			vettingChestList = null;
		}
		if ( vettingChest != null ) {
			vettingChest = null;
		}
	}
}