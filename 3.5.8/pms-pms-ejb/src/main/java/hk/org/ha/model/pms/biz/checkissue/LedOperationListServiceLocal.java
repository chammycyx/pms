package hk.org.ha.model.pms.biz.checkissue;

import hk.org.ha.model.pms.udt.machine.IqdsAction;
import hk.org.ha.model.pms.vo.checkissue.CheckIssueOrder;
import hk.org.ha.model.pms.vo.machine.IqdsMessageContent;

import java.util.Collection;
import java.util.List;

import javax.ejb.Local;

@Local
public interface LedOperationListServiceLocal {
	void retrieveIssueWindowList();
	
	void sendTicketListToIqds(List<IqdsMessageContent> iqdsMessageContentList, Collection<CheckIssueOrder> ledCheckedList, Integer issueWindowNum);
	
	void sendTicketToIqds(List<IqdsMessageContent> iqdsMessageContentList);
	
	void retrieveLedList();
	
	Integer retrieveIssueWindowNum();
	
	void retrieveLedOperationLogList(IqdsAction iqdsAction);
	
	void destroy();
	
}
