package hk.org.ha.model.pms.util;

import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.lang.time.StopWatch;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class StopWatcher {
	
	private static final Log logger = LogFactory.getLog(StopWatcher.class);
       
	public static final StopWatchThreadLocal STOP_WATCH = new StopWatchThreadLocal(); 

	public static final StopWatchStart STOP_WATCH_START = new StopWatchStart(); 

	public static boolean isDebugEnabled() {
		return logger.isDebugEnabled();
	}
       
	public static void start() {
		STOP_WATCH.get().start();
		STOP_WATCH_START.get().set(true);
	}
	
	public static void suspend() {
		if (STOP_WATCH_START.get().get()) {
			STOP_WATCH.get().suspend();
		}
	}
	
	public static void resume() {
		if (STOP_WATCH_START.get().get()) {
			STOP_WATCH.get().resume();
		}
	}
	
	public static void stop() {
		if (STOP_WATCH_START.get().get()) {
			STOP_WATCH.get().stop();
			STOP_WATCH_START.get().set(false);
		}
	}

	public static void reset() {
		if (STOP_WATCH_START.get().get()) {
			STOP_WATCH.get().reset();
		}
	}
	
	public static long getTime() {
		return STOP_WATCH.get().getTime();
	}
	
	public static class StopWatchThreadLocal extends ThreadLocal<StopWatch> {
		@Override
		protected StopWatch initialValue() {
			return new StopWatch();
		}
	}

	public static class StopWatchStart extends ThreadLocal<AtomicBoolean> {
		@Override
		protected AtomicBoolean initialValue() {
			return new AtomicBoolean();
		}
	}

}
