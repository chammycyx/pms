package hk.org.ha.model.pms.biz.vetting;

import static hk.org.ha.model.pms.prop.Prop.ALERT_EHR_PROFILE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.ALERT_PROFILE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.CHARGING_SFI_SUSPEND_ORDER_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.CHARGING_STANDARD_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_LANG;
import static hk.org.ha.model.pms.prop.Prop.ORDER_ALLOWUPDATE_DURATION;
import static hk.org.ha.model.pms.prop.Prop.SUSPEND_ORDER_SUSPENDCODE;
import static hk.org.ha.model.pms.prop.Prop.VETTING_ALERT_PROFILE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.VETTING_CANCEL_ORDER_SUSPENDCODE;
import static hk.org.ha.model.pms.prop.Prop.VETTING_DUPLICATEDOITEM_CHECK;
import static hk.org.ha.model.pms.prop.Prop.VETTING_MODIFIEDMOITEM_PRINTING_URGENT;
import static hk.org.ha.model.pms.prop.Prop.VETTING_PRINTING_URGENT_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.VETTING_URGENTRECONORDER_PRINTING_URGENT;
import hk.org.ha.fmk.pms.remote.marshall.EclipseLinkXStreamMarshaller;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.UnreachableException;
import hk.org.ha.model.pms.asa.exception.alert.AlertProfileException;
import hk.org.ha.model.pms.asa.exception.alert.ehr.EhrAllergyException;
import hk.org.ha.model.pms.asa.exception.pas.PasException;
import hk.org.ha.model.pms.biz.alert.AlertAuditManagerLocal;
import hk.org.ha.model.pms.biz.alert.AlertProfileManagerLocal;
import hk.org.ha.model.pms.biz.alert.ehr.EhrAlertManagerLocal;
import hk.org.ha.model.pms.biz.alert.mds.AlertRetrieveManagerLocal;
import hk.org.ha.model.pms.biz.alert.mds.PrescCheckManagerLocal;
import hk.org.ha.model.pms.biz.charging.ChargeCalculationInf;
import hk.org.ha.model.pms.biz.charging.DrugChargeClearanceManagerLocal;
import hk.org.ha.model.pms.biz.drug.ItemConvertManagerLocal;
import hk.org.ha.model.pms.biz.inbox.PharmInboxClientNotifierInf;
import hk.org.ha.model.pms.biz.onestop.OneStopOrderStatusManagerLocal;
import hk.org.ha.model.pms.biz.onestop.OneStopUtilsLocal;
import hk.org.ha.model.pms.biz.onestop.SuspendPrescManagerLocal;
import hk.org.ha.model.pms.biz.order.MedOrderManagerLocal;
import hk.org.ha.model.pms.biz.refill.RefillScheduleManagerLocal;
import hk.org.ha.model.pms.biz.refill.SfiRefillScheduleRemoveManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.corp.persistence.charging.PatientSfiProfile;
import hk.org.ha.model.pms.corp.vo.StartVettingResult;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.exception.vetting.VettingException;
import hk.org.ha.model.pms.pas.PasServiceWrapper;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.CapdVoucher;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.InvoiceItem;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.persistence.disp.RefillScheduleItem;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.persistence.reftable.DrsConfig;
import hk.org.ha.model.pms.persistence.reftable.DrsPatient;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.persistence.reftable.RefillConfig;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.YesNoBlankFlag;
import hk.org.ha.model.pms.udt.charging.ClearanceStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderDayEndStatus;
import hk.org.ha.model.pms.udt.disp.InvoiceDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderPatType;
import hk.org.ha.model.pms.udt.disp.RemarkItemStatus;
import hk.org.ha.model.pms.udt.disp.RemarkStatus;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
import hk.org.ha.model.pms.udt.patient.PasSpecialtyType;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.refill.DocType;
import hk.org.ha.model.pms.udt.reftable.OperationProfileType;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.udt.vetting.RegimenType;
import hk.org.ha.model.pms.vo.alert.mds.ErrorInfo;
import hk.org.ha.model.pms.vo.charging.DrugChargeRuleCriteria;
import hk.org.ha.model.pms.vo.charging.FcsPaymentStatus;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.model.pms.vo.onestop.OneStopOrderInfo;
import hk.org.ha.model.pms.vo.onestop.OneStopSummary;
import hk.org.ha.model.pms.vo.prevet.PreVetInfo;
import hk.org.ha.model.pms.vo.security.PropMap;
import hk.org.ha.model.pms.vo.vetting.MedOrderItemInfo;
import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;
import hk.org.ha.model.pms.vo.vetting.PharmOrderItemInfo;
import hk.org.ha.model.pms.vo.vetting.PrintJobManagerInfo;
import hk.org.ha.model.pms.vo.vetting.RetrievePatientResult;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;
import hk.org.ha.service.pms.asa.interfaces.alert.EhrAlertServiceJmsRemote;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Remove;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;
import org.jboss.seam.security.Identity;
import org.joda.time.DateTime;
import org.joda.time.Days;

@AutoCreate
@Stateless
@Name("orderViewManager")
@MeasureCalls
public class OrderViewManagerBean implements OrderViewManagerLocal {

	@PersistenceContext
	private EntityManager em;
	
	@Logger
	private Log logger;

	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private OrderViewInfo orderViewInfo;
	
	@In
	private RefillConfig vettingRefillConfig;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private DrsConfig vettingDrsConfig;
	
	@In(required = false)
	private Ticket ticket;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private OneStopSummary oneStopSummary;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private OneStopOrderInfo oneStopOrderInfo;
	
	@In
	private Workstore workstore;

	@In
	private Identity identity;
	
	@In
	private VettingManagerLocal vettingManager;
	
	@In
	private MedOrderManagerLocal medOrderManager;

	@In
	private ItemConvertManagerLocal itemConvertManager;
	
	@In
	private AlertProfileManagerLocal alertProfileManager;
	
	@In
	private EhrAlertManagerLocal ehrAlertManager;
	
	@In
	private DuplicateItemCheckManagerLocal duplicateItemCheckManager;
	
	@In
	private ChargeCalculationInf chargeCalculation;
	
	@In
	private RefillScheduleManagerLocal refillScheduleManager;
	
	@In
	private SfiRefillScheduleRemoveManagerLocal sfiRefillScheduleRemoveManager;

	@In
	private CapdVoucherManagerLocal capdVoucherManager;
	
	@In
	private PrescCheckManagerLocal prescCheckManager;
	
	@In
	private AlertRetrieveManagerLocal alertRetrieveManager;
	
	@In
	private DrugChargeClearanceManagerLocal drugChargeClearanceManager;
	
	@In
	private OrderEditManagerLocal orderEditManager;
	
	@In
	private SuspendPrescManagerLocal suspendPrescManager;
		
	@In
	private OneStopUtilsLocal oneStopUtils;
	
	@In
	private PrintJobManagerLocal printJobManager;
	
	@In
	private Workstation workstation;
	
	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;

	@In
	private PasServiceWrapper pasServiceWrapper;
	
	@In
	private UamInfo uamInfo;
		
	@In
	private OperationProfile activeProfile;

	@In
	private PropMap propMap;
	
	@In
	private OneStopOrderStatusManagerLocal oneStopOrderStatusManager;
	
	@In
	private AlertAuditManagerLocal alertAuditManager;
	
	@In
	private PharmInboxClientNotifierInf pharmInboxClientNotifier;
	
	@In
	private EhrAlertServiceJmsRemote ehrAlertServiceProxy;
	
	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
	
	public void retrieveOrder(Long id, OneStopOrderType orderType, Boolean requiredLockOrder) {
		
		logger.debug("ticketNum : #0", ticket == null ? "null" : ticket.getTicketNum());
		logger.info("retrieveOrder: id=#0, orderType=#1, userId=#2, workstation=#3", id, orderType.getDataValue(), uamInfo.getUserId(), workstation.getHostName());

		// init session
		createOrderViewInfo(orderType);
		vettingDrsConfig = null;

		orderViewInfo.setAlertErrorInfoList(new ArrayList<ErrorInfo>());
		prescCheckManager.clearPrescAlert(orderViewInfo);

		switch (orderType) {
			case MedOrder:
				retrieveMedOrder(id, requiredLockOrder);
				break;

			case ManualOrder:
				retrieveManualOrder();
				break;

			case DispOrder:
				retrieveDispOrder(id);
				break;

			case RefillOrder:
				retrieveRefillOrder(id, false);
				break;
			
			case RefillOrderReadOnly:
				retrieveRefillOrderByDispOrderId(id);
				break;
				
			case SfiOrder:
				retrieveSfiOrder(id);
				break;
			
			case DhOrder:
				retrieveDhOrder(id);
				break;
		}
		
		if (orderViewInfo.hasErrorMsg()) {
			return;
		}

		if (orderViewInfo.getDiscontinueMessageTlf().isEmpty() && !orderViewInfo.getAllowEditFlag()) { // prompt readonly message
			if (getPharmOrder().getMedOrder().isDhOrder() && !getPharmOrder().getMedOrder().getDhLatestFlag()) {
				orderViewInfo.addInfoMsg("0900");
			}
			else {
				orderViewInfo.addInfoMsg("0119");
			}
		}
		
		Collections.sort(getPharmOrder().getMedOrder().getMedOrderItemList(), new MedOrderItemComparator(getPharmOrder().getMedOrder().isDhOrder())); // sort MedOrderItem
		
		orderViewInfo.setRefillPrintReminderFlag(vettingRefillConfig.getPrintReminderFlag());
		
		medOrderManager.setPharmOrder(getPharmOrder());
	}
	
	@SuppressWarnings("incomplete-switch")
	public void saveOrder(OrderViewInfo inOrderViewInfo) throws VettingException {

		PharmOrder pharmOrder = getPharmOrder();
		MedOrder medOrder = pharmOrder.getMedOrder();
		DispOrder dispOrder = this.getDispOrder();
		
		PrintJobManagerInfo printJobManagerInfo = null;
		
		logger.info("saveOrder: medOrderId=#0, userId=#1", medOrder.getId(), uamInfo.getUserId());
		
		// update from front-end
		this.updateOrderFromOrderView(inOrderViewInfo);

		if (!preSaveCheck(inOrderViewInfo, medOrder, pharmOrder)) {
			return;
		}

		switch (inOrderViewInfo.getOneStopOrderType()) {
			
			case MedOrder:

				if (isOrderModified(medOrder)) {
					return;
				}
				if (!sfiItemNumCheck(medOrder)) {
					return;
				}
				if (!itemCheck(medOrder)) {
					return;
				}

				setSfiQtyForSfiStepUpDownItem(medOrder);
				pharmOrder.assignItemNum(orderViewInfo.getOrgMaxItemNum());
				pharmOrder.setDrsFlag(orderViewInfo.isDrsFlag());
				
				resetItemConvClearMoDoseAlert(medOrder);
				Collections.sort(medOrder.getMedOrderItemList(), new MedOrderItemComparator());
				
				printJobManagerInfo = vettingManager.endVetting(inOrderViewInfo, pharmOrder);
				break;

			case ManualOrder:

				if (!sfiItemNumCheck(medOrder)) {
					return;
				}
				if (!itemCheck(medOrder)) {
					return;
				}

				setSfiQtyForSfiStepUpDownItem(medOrder);
				pharmOrder.assignItemNum(orderViewInfo.getOrgMaxItemNum());
				pharmOrder.setDrsFlag(orderViewInfo.isDrsFlag());
				
				resetItemConvClearMoDoseAlert(medOrder);
				Collections.sort(medOrder.getMedOrderItemList(), new MedOrderItemComparator());
				
				printJobManagerInfo = vettingManager.endVetting(inOrderViewInfo, pharmOrder);
				break;

			case DispOrder:
				
				if (isOrderModified(medOrder)) {
					return;
				}
				if (!sfiItemNumCheck(medOrder)) {
					return;
				}
				
				if (!pharmOrder.patientHeaderEquals(em.find(PharmOrder.class, pharmOrder.getId()))) {
					pharmOrder.markDirtyFlag();
				}
				
				markDirtyFlagByStandardRefillSchedule(pharmOrder, dispOrder);
				
				if (!itemCheck(medOrder)) {
					return;
				}

				setSfiQtyForSfiStepUpDownItem(medOrder);
				orderViewInfo.setDispOrderId(null);
				orderViewInfo.setSfiInvoiceClearanceStatus(null);
				orderViewInfo.setDispOrderWoSfiItemEditFlag(updateIsDispOrderWoSfiItemEditFlag(dispOrder, pharmOrder));
				logger.debug("orderViewInfo.isDispOrderWoSfiItemEditFlag()DispOrder=#0", orderViewInfo.isDispOrderWoSfiItemEditFlag());

				if( requireSfiClearanceAction(inOrderViewInfo, dispOrder) ){
					inOrderViewInfo.setDispOrderId(dispOrder.getId());
					break;
				}
				pharmOrder.assignItemNum(orderViewInfo.getOrgMaxItemNum());
				pharmOrder.setDrsFlag(orderViewInfo.isDrsFlag());
				
				resetItemConvClearMoDoseAlert(medOrder);
				Collections.sort(medOrder.getMedOrderItemList(), new MedOrderItemComparator(medOrder.isDhOrder()));
				
				if( requireSfiItemStatusReset(inOrderViewInfo, dispOrder, pharmOrder) ){
					pharmOrder.markSfiStatusResetFlag();
				}
				
				printJobManagerInfo = vettingManager.endVetting(inOrderViewInfo, this.getDispOrder());
				break;

			case SfiOrder:
				if (isOrderModified(medOrder)) {
					return;
				}
				
				if (!pharmOrder.patientHeaderEquals(em.find(PharmOrder.class, pharmOrder.getId()))) {
					pharmOrder.markDirtyFlag();
				}

				if( dispOrder == null ){
					pharmOrder.markDirtyFlag();
				}
				
				orderViewInfo.setDispOrderId(null);
				orderViewInfo.setSfiInvoiceClearanceStatus(null);
				orderViewInfo.setDispOrderWoSfiItemEditFlag( updateIsDispOrderWoSfiItemEditFlag(dispOrder, pharmOrder));
				logger.debug("orderViewInfo.isDispOrderWoSfiItemEditFlag()SfiOrder=#0", orderViewInfo.isDispOrderWoSfiItemEditFlag());

				if( requireSfiClearanceAction(inOrderViewInfo, dispOrder) ){
					inOrderViewInfo.setDispOrderId(dispOrder.getId());
					break;
				}

				if( requireSfiItemStatusReset(inOrderViewInfo, dispOrder, pharmOrder) ){
					pharmOrder.markSfiStatusResetFlag();
				}
				printJobManagerInfo = vettingManager.endVettingForSfiRefill(inOrderViewInfo, getRefillSchedule(), medOrder.getTicket());
				break;
				
			case DhOrder:
				
				if (!itemCheck(medOrder)) {
					return;
				}
				
				if (!dhCheckVettedOrder(medOrder)) {
					return;
				}

				pharmOrder.assignItemNum(orderViewInfo.getOrgMaxItemNum());
				pharmOrder.setDrsFlag(orderViewInfo.isDrsFlag());
				
				resetItemConvClearMoDoseAlert(medOrder);
				
				printJobManagerInfo = vettingManager.endVetting(inOrderViewInfo, pharmOrder);
				break;
		}

		if (printJobManagerInfo != null) {
			try {
				printJobManager.renderAndPrint(printJobManagerInfo.getDispOrder(), printJobManagerInfo.getVoucherList(), inOrderViewInfo);
			} catch (Exception e) {
				logger.error("Unexpected error!", e);
			}
		}
		
		if (medOrder.isMpDischarge()) {
			pharmInboxClientNotifier.dispatchUpdateEvent(medOrder);
		}
		
		em.flush();
		em.clear();
	}
	
	@SuppressWarnings("unchecked")
	private boolean dhCheckVettedOrder(MedOrder medOrder) {
		
		List<DispOrder> dispOrderList = em.createQuery(
				"select o from DispOrder o" + // 20160616 index check : MedCase.caseNum : I_MED_CASE_01
				" where o.pharmOrder.medOrder.prescType = :prescType" +
				" and o.pharmOrder.medOrder.status = :status" +
				" and o.pharmOrder.medOrder.medCase.caseNum = :caseNum" +
				" and o.dayEndStatus = :dayEndStatus" +
				" and o.batchProcessingFlag = :batchProcessingFlag"
			)
			.setParameter("prescType", MedOrderPrescType.Dh)
			.setParameter("status", MedOrderStatus.Complete)
			.setParameter("caseNum", medOrder.getMedCase().getCaseNum())
			.setParameter("dayEndStatus", DispOrderDayEndStatus.None)
			.setParameter("batchProcessingFlag", false)
			.getResultList();

		if (!dispOrderList.isEmpty()) {
			orderViewInfo.addErrorMsg("0979");
		}
		
		return dispOrderList.isEmpty();
	}

	private boolean preSaveCheck(OrderViewInfo inOrderViewInfo, MedOrder medOrder, PharmOrder pharmOrder) {
		
		// check duplicate item
		if (inOrderViewInfo.getSkipDuplicateCheckFlag()) {
			duplicateItemCheckManager.clearLocalDuplicateItem();
		}
		else if (!duplicateItemCheckManager.checkLocalDuplicateItem(pharmOrder)) {
			return false;
		}
		
		// check mds
		if (alertRetrieveManager.isMdsEnable()) {
			if (orderViewInfo.getAlertOverrideFlag()) {
				for (ErrorInfo errorInfo : orderViewInfo.getAlertErrorInfoList()) {
					errorInfo.setSuppressFlag(Boolean.TRUE);
				}
				prescCheckManager.clearPrescAlert(orderViewInfo);
			}
			else if (!prescCheckManager.checkPrescAlert(orderViewInfo)) {
				return false;
			}
			if (medOrder.getDocType() == MedOrderDocType.Manual && Contexts.getSessionContext().get("mdsExceptionInfo") == null) {
				prescCheckManager.replacePrescAlert(medOrder);
			}
		}
		
		// check duplicate / invalid itemNum
		Set<Integer> moiItemNumSet = new HashSet<Integer>();
		for (MedOrderItem moi : medOrder.getMedOrderItemList()) {
			if (moiItemNumSet.contains(moi.getItemNum())) {
				throw new UnsupportedOperationException("Duplicate cms item no. " + moi.getItemNum() + ", order num = " + medOrder.getOrderNum());
			}
			moiItemNumSet.add(moi.getItemNum());

			Set<Integer> poiItemNumSet = new HashSet<Integer>();
			Set<Integer> poiOrgItemNumSet = new HashSet<Integer>();
			for (PharmOrderItem poi : moi.getPharmOrderItemList()) {
				if (poi.getItemNum() > pharmOrder.getMaxItemNum()) {
					throw new UnsupportedOperationException("Pharm item no. is larger than max item no. " + poi.getItemNum() + " > " + pharmOrder.getMaxItemNum() + ", order num = " + medOrder.getOrderNum());
				}

				if (poiItemNumSet.contains(poi.getItemNum())) {
					throw new UnsupportedOperationException("Duplicate pharm item no. " + poi.getItemNum() + ", order num = " + medOrder.getOrderNum());
				}
				poiItemNumSet.add(poi.getItemNum());

				if (poiOrgItemNumSet.contains(poi.getOrgItemNum())) {
					throw new UnsupportedOperationException("Duplicate pharm org item no. " + poi.getOrgItemNum() + ", order num = " + medOrder.getOrderNum());
				}
				poiOrgItemNumSet.add(poi.getOrgItemNum());
			}
		}
		

		// check duplicate poi itemNum, orgItemNum
		Set<Integer> poiItemNumSet = new HashSet<Integer>();
		Set<Integer> poiOrgItemNumSet = new HashSet<Integer>();
		for (PharmOrderItem poi : pharmOrder.getPharmOrderItemList()) {
			if (poiItemNumSet.contains(poi.getItemNum())) {
				throw new UnsupportedOperationException("Duplicate poi item no. " + poi.getItemNum() + ", order num = " + medOrder.getOrderNum());
			}
			else {
				poiItemNumSet.add(poi.getItemNum());
			}
			if (poiOrgItemNumSet.contains(poi.getOrgItemNum())) {
				throw new UnsupportedOperationException("Duplicate poi org item no. " + poi.getOrgItemNum() + ", order num = " + medOrder.getOrderNum());
			}
			else {
				poiOrgItemNumSet.add(poi.getOrgItemNum());
			}
		}
		
		return true;
	}
	
	private boolean itemCheck(MedOrder medOrder) {
		
		PharmOrderItem suspendedItem = null, outScopeItem = null;
		
		for (MedOrderItem moi : medOrder.getMedOrderItemList()) {
			for (PharmOrderItem poi : moi.getPharmOrderItemList()) {
				DmDrug dmDrug = DmDrugCacher.instance().getDmDrug(poi.getItemCode());
				if (!poi.isDirtyFlag() || FmStatus.FreeTextEntryItem.getDataValue().equals(moi.getFmStatus())) {
					continue;
				}
				WorkstoreDrug workstoreDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, poi.getItemCode());
				if (workstoreDrug.getMsWorkstoreDrug() != null) {
					if ("Y".equals(workstoreDrug.getMsWorkstoreDrug().getSuspend())) {
						suspendedItem = poi;
						break;
					}
					else if ((Integer.valueOf(200).compareTo(workstoreDrug.getMsWorkstoreDrug().getDrugScope()) > 0)
						  && (Integer.valueOf(100).compareTo(workstoreDrug.getMsWorkstoreDrug().getDrugScope()) > 0 || !"Y".equals(dmDrug.getDmMoeProperty().getSolventItem()))) {
						outScopeItem = poi;
						break;
					}
				}				
			}
		}
		
		if (suspendedItem != null) {
			orderViewInfo.addErrorMsg("0509", new String[]{suspendedItem.getItemCode()});
			return false;
		}
		else if (outScopeItem != null) {
			orderViewInfo.addErrorMsg("0626", new String[]{outScopeItem.getItemCode()});
			return false;
		}
		return true;
	}

	private boolean sfiItemNumCheck(MedOrder medOrder) {
		for (MedOrderItem moi : medOrder.getMedOrderItemList()) {
			if (moi.getStatus() != MedOrderItemStatus.Deleted && moi.getStatus() != MedOrderItemStatus.SysDeleted
				&& moi.isSfi() && (moi.getRegimen() == null || moi.getRegimen().getType() != RegimenType.StepUpDown)
				&& Integer.valueOf(99).compareTo(moi.getItemNum()) < 0) {
				orderViewInfo.addErrorMsg("0629", new String[]{moi.getPharmOrderItemList().get(0).getItemCode()});
				return false;
			}
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	private void markDirtyFlagByStandardRefillSchedule( PharmOrder pharmOrderIn, DispOrder dispOrderIn ){
		pharmOrderIn.reconnectRefillScheduleList();
		
		List<RefillSchedule> rsListIn = pharmOrderIn.getRefillScheduleList(DocType.Standard);
		Map<Integer, Integer> dirtyRsiMap = new HashMap<Integer, Integer>();
		List<Integer> currRsiList = new ArrayList<Integer>();
		
		for ( RefillSchedule editRs : rsListIn ){
			if( editRs.getRefillNum() == 1 || editRs.getRefillNum() == 2 ){
				if( !Boolean.valueOf(orderViewInfo.isDrsFlag()).equals(pharmOrderIn.getDrsFlag()) ){
					editRs.setPrintFlag(false);
				}
				
				for ( RefillScheduleItem editRsi : editRs.getRefillScheduleItemList() ){
					if( editRs.getRefillNum() == 1 && !currRsiList.contains(editRsi.getItemNum()) ){
						currRsiList.add(editRsi.getItemNum());
					}
					if( editRsi.isDirtyFlag() ){
						if( editRs.getRefillNum() == 1 ){
							dirtyRsiMap.put(editRsi.getItemNum(), editRsi.getItemNum());
						}
						editRs.setPrintFlag(false);
					}
				}
			}
		}
				
		for ( PharmOrderItem pharmOrderItem : pharmOrderIn.getPharmOrderItemList() ){
			if( dirtyRsiMap.containsKey(pharmOrderItem.getOrgItemNum() ) ){
				pharmOrderItem.setDirtyFlag(true);
			}
		}
		
		//Previous DispOrder RefillSchedule
		List<RefillSchedule> dispRsList = em.createQuery(
			"select o from RefillSchedule o" + // 20140204 index check : DispOrder.id : PK_DISP_ORDER
			" where o.dispOrder.id = :dispOrderId" +
			" and o.docType = :docType" +
			" and o.refillNum = 1" +
			" order by o.groupNum")
			.setParameter("dispOrderId", dispOrderIn.getId())
			.setParameter("docType", DocType.Standard)
			.getResultList();
		
		 for( RefillSchedule dispRs : dispRsList ){		 
			for( RefillScheduleItem dispRsi : dispRs.getRefillScheduleItemList() ){
				PharmOrderItem poi = pharmOrderIn.getPharmOrderItemByOrgItemNum(dispRsi.getItemNum());			
				if( poi != null && !poi.isDirtyFlag() && !currRsiList.contains(poi.getOrgItemNum()) ){
					poi.setDirtyFlag(true);
				}
			} 
		 }
		
		pharmOrderIn.disconnectRefillScheduleList();
	}
	
	private boolean updateIsDispOrderWoSfiItemEditFlag(DispOrder dispOrderIn, PharmOrder pharmOrderIn){
		if( pharmOrderIn.hasDirtySfiItem() ){
			logger.debug("updateIsDispOrderWoSfiItemEditFlag hasDirtySfiItem");
			return false;
		}
		
		if( dispOrderIn == null ){
			logger.debug("updateIsDispOrderWoSfiItemEditFlag dispOrderIn is null");
			return false;
		}
		
		boolean sfiItemExist = false;
		Integer invoiceItemCount = 0;
		for( PharmOrderItem pharmOrderItem : pharmOrderIn.getPharmOrderItemList() ){
			if( pharmOrderItem.isSfi() ){
				DispOrderItem dispOrderItem = dispOrderIn.getDispOrderItemByPoOrgItemNum(pharmOrderItem.getOrgItemNum());
				if( dispOrderItem == null ){
					return false;
				}else if( pharmOrderItem.getSfiQty() == 0 ){
					continue;
				}
				sfiItemExist = true;
				invoiceItemCount++;
			}
		}

		if( !sfiItemExist ){
			logger.debug("updateIsDispOrderWoSfiItemEditFlag sfiItem Not Exist");
			return false;
		}
		
		for( Invoice invoice : dispOrderIn.getInvoiceList() ){
			if( InvoiceDocType.Sfi.equals(invoice.getDocType()) ){
				if( invoice.getInvoiceItemList() != null && !invoiceItemCount.equals(invoice.getInvoiceItemList().size()) ){
					logger.debug("updateIsDispOrderWoSfiItemEditFlag Invoice Item Count is Change: Before[#0], After[#1]",invoice.getInvoiceItemList().size(), invoiceItemCount);
					return false;
				}
				break;
			}
		}
		return true;
	}
	
	private boolean requireSfiItemStatusReset(OrderViewInfo inOrderViewInfo, DispOrder dispOrder, PharmOrder pharmOrderIn){
		if( dispOrder == null ){
			return false;
		}
		
		if( activeProfile.getType() == OperationProfileType.Pms ){
			return false;
		}
		
		Integer sfiItemCount = 0;
		for( PharmOrderItem pharmOrderItem : pharmOrderIn.getPharmOrderItemList() ){
			if( pharmOrderItem.isSfi() ){
				if( pharmOrderItem.getSfiQty() > 0 ){
					sfiItemCount++;
				}
			}
		}
		
		Integer sfiDirtyItemCount = 0;
		for( PharmOrderItem pharmOrderItem : pharmOrderIn.getPharmOrderItemList() ){
			if( pharmOrderItem.isSfi() && pharmOrderItem.isDirtyFlag() ){
				if( pharmOrderItem.getSfiQty() > 0 ){
					sfiDirtyItemCount++;
				}
			}
		}
		
		//DirtyFlag - Delete:false, setZeroQty:true
		if( pharmOrderIn.hasDirtySfiItem()){
			if( sfiDirtyItemCount > 0 ){ //SfiItemCount Change or Newly Added Item
				return false;
			}
			//SFI Item Change to Qty 0
			logger.debug("requireSfiItemStatusReset SFI Qty is changed to zero");
			return true;
		}else{
			if( !dispOrder.getSfiFlag() ){ // non-SFI Order Skip
				return false;
			}else{
				//SFI Order - Check Item Count for Remove SFI Item
				for( Invoice invoice : dispOrder.getInvoiceList() ){
					if( InvoiceDocType.Sfi.equals(invoice.getDocType()) ){
						if( invoice.getInvoiceItemList() != null && !sfiItemCount.equals(invoice.getInvoiceItemList().size()) ){
							logger.debug("requireSfiItemStatusReset SFI Invoice[#0] item count changed from #1 to #2", invoice.getInvoiceNum(), sfiItemCount, invoice.getInvoiceItemList().size());
							return true;
						}
						break;
					}
				}
			}
		}
		return false;
	}
	
	private boolean requireSfiClearanceAction(OrderViewInfo inOrderViewInfo, DispOrder dispOrder){
		if( dispOrder == null ){
			return false;
		}
		
		logger.debug("requireSfiClearanceAction inOrderViewInfo.isSkipSfiClearanceFlag(): #0", inOrderViewInfo.isSkipSfiClearanceFlag());
		if( inOrderViewInfo.isSkipSfiClearanceFlag() ){
			return false;
		}
		
		logger.debug("requireSfiClearanceAction inOrderViewInfo.getAwaitSfiFlag(): #0", inOrderViewInfo.getAwaitSfiFlag());
		if( !inOrderViewInfo.getAwaitSfiFlag() ){
			return false;
		}
		
		if( inOrderViewInfo.isDispOrderWoSfiItemEditFlag() && dispOrder.getSfiFlag() &&
			DispOrderAdminStatus.Suspended.equals(dispOrder.getAdminStatus()) ){
			for( Invoice invoice : dispOrder.getInvoiceList() ){
				if( InvoiceDocType.Sfi.equals(invoice.getDocType()) ){
					FcsPaymentStatus fcsPaymentStatus = drugChargeClearanceManager.checkSfiDrugClearanceByInvoice(invoice, false);
					logger.debug("requireSfiClearanceAction fcsPaymentStatus.getClearanceStatus(): #0", fcsPaymentStatus.getClearanceStatus());
					if( !ClearanceStatus.PaymentClear.equals(fcsPaymentStatus.getClearanceStatus()) && !invoice.getForceProceedFlag() ){
						inOrderViewInfo.setSfiInvoiceClearanceStatus(fcsPaymentStatus.getClearanceStatus());
						return true;
					}else if( ClearanceStatus.PaymentClear.equals(fcsPaymentStatus.getClearanceStatus()) ){
						inOrderViewInfo.setSfiInvoicePaymentClearFlag(true);
						return false;
					}
					break;
				}
			}
		}
		
		return false;
	}

	public void cancelOrder(Boolean suspendFlag, String suspendCode) {
		
		logger.info("cancelOrder: medOrderId=#0, userId=#1", getPharmOrder().getMedOrder().getId(), uamInfo.getUserId());
		
		if ( orderViewInfo.getOneStopOrderType() == OneStopOrderType.DhOrder ) {
			vettingManager.releaseUnvetDhOrder(getPharmOrder().getMedOrder());
			medOrderManager.clear();
			return;
		}
		
		if (isOrderModified(getPharmOrder().getMedOrder())) {
			return;
		}

		vettingManager.releaseOrder(orderViewInfo, em.find(MedOrder.class, getPharmOrder().getMedOrder().getId()));
		MedOrder outstandingOrder = em.find(MedOrder.class, getPharmOrder().getMedOrder().getId());
		if (!StringUtils.isEmpty(outstandingOrder.getPrevOrderNum()) && (outstandingOrder.getStatus() == MedOrderStatus.Outstanding || 
				outstandingOrder.getStatus() == MedOrderStatus.PreVet || outstandingOrder.getStatus() == MedOrderStatus.Withhold))	{
			outstandingOrder.setTicket(vettingManager.retrievePrevTicket(outstandingOrder.getPrevOrderNum()));
		}


		if (getDispOrder() != null) {
			// suspend / reverse suspend
			DispOrder dispOrder = em.find(DispOrder.class, getDispOrder().getId());
			if (suspendFlag) {
				if (suspendCode == null) {
					if (orderViewInfo.getAutoSuspendFlag()) {
						suspendPrescManager.suspendPrescription(dispOrder, VETTING_CANCEL_ORDER_SUSPENDCODE.get(), true);
					} else {
						suspendPrescManager.suspendPrescription(dispOrder, dispOrder.getSuspendCode(), true);
					}
				} else {
					suspendPrescManager.suspendPrescription(dispOrder, suspendCode, true);
				}
			} else {
				suspendPrescManager.reverseSuspendByDispOrder(dispOrder, "vetting", true, false);
			}
		}
		
		if (getPharmOrder().getMedOrder().isMpDischarge()) {
			pharmInboxClientNotifier.dispatchUpdateEvent(getPharmOrder().getMedOrder());
		}
		medOrderManager.clear();
	}
	
	public void removeOrder(String logonUser, String remarkText) {
		PharmOrder ctxPharmOrder = getPharmOrder();

		logger.info("removeOrder: medOrderId=#0, userId=#1", ctxPharmOrder.getMedOrder().getId(), uamInfo.getUserId());

		if (orderViewInfo.getOneStopOrderType() == OneStopOrderType.ManualOrder) {
			return;
		}
		
		if (isOrderModified(ctxPharmOrder.getMedOrder())) {
			return;
		}
		
		MedOrder medOrder = em.find(MedOrder.class, ctxPharmOrder.getMedOrder().getId());
		medOrder.loadChild();
		
		// unlock the order
		vettingManager.releaseOrder(orderViewInfo, medOrder);

		DispOrder dispOrder = null;
		if (medOrder.getStatus() == MedOrderStatus.Complete) {
			if (medOrder.getDocType() == MedOrderDocType.Normal) {
				// check whether there is pharmacy remark delete item in the order before unvet
				// (to determine whether to include item detail in VO2)
				boolean containPharmRemarkDeleteItem = false;
				for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
					if (medOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedDelete) {
						containPharmRemarkDeleteItem = true;
						break;
					}
				}
				medOrder.setIsUnvetPharmRemarkDeleteItem(containPharmRemarkDeleteItem);
				
				// unvet order
				Long newMedOrderId = vettingManager.unvetMedOrder(medOrder.getId(), true);
				
				logger.info("removeOrder: medOrderId after unvet=#0, userId=#1", newMedOrderId, uamInfo.getUserId());
				
				medOrder = em.find(MedOrder.class, newMedOrderId);
				medOrder.loadChild();
			} else {
				dispOrder = em.find(DispOrder.class, getDispOrder().getId());
				dispOrder.loadChild();
			}
		}

		if (dispOrder != null && dispOrder.getAdminStatus() == DispOrderAdminStatus.Suspended) {
			suspendPrescManager.reverseSuspendByDispOrder(dispOrder, "vetting", true, false);
		}
		
		vettingManager.removeOrder(medOrder, dispOrder, logonUser, remarkText);

		if (medOrder.isMpDischarge()) {
			pharmInboxClientNotifier.dispatchUpdateEvent(medOrder);
		}
		
		medOrderManager.clear();
	}
	
	public void removeSfiRefillOrder(Long refillScheduleId) {
		PharmOrder ctxPharmOrder = getPharmOrder();

		if (isOrderModified(ctxPharmOrder.getMedOrder())) {
			return;
		}
		RefillSchedule inRefillSchedule = em.find(RefillSchedule.class, refillScheduleId);

		if (inRefillSchedule.getDispOrder() != null) {
			vettingManager.removeSfiRefillOrder(inRefillSchedule.getDispOrder().getId());
		}		
		medOrderManager.clear();
	}
	
	public void removeMedOrderItem(OrderViewInfo inOrderViewInfo, int itemIndex, Date remarkCreateDate, String remarkCreateUser, String remarkText) {
		PharmOrder pharmOrder = getPharmOrder();
		pharmOrder.relinkAllItem();
		MedOrder medOrder = pharmOrder.getMedOrder();
		MedOrderItem medOrderItem = medOrder.getMedOrderItemList().get(itemIndex);

		if ((medOrder.getDocType() == MedOrderDocType.Manual && !medOrder.isDhOrder())
				|| medOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedAdd
				|| inOrderViewInfo.getOneStopOrderType() == OneStopOrderType.SfiOrder ) {
			if (medOrderItem.getRemarkStatus() == RemarkStatus.Unconfirm) {
				medOrder.setHasReversedItemAddRemark(Boolean.TRUE);
			}
			
			if( inOrderViewInfo.getOneStopOrderType() == OneStopOrderType.SfiOrder ){
				for (PharmOrderItem pharmOrderItem : medOrderItem.getPharmOrderItemList()) {
					sfiRefillScheduleRemoveManager.removeSfiRefillScheduleItemByPharmOrderItemNum(pharmOrder, pharmOrderItem.getOrgItemNum());
				}
				orderViewInfo.setDispOrderWoSfiItemEditFlag(false);
			}else if (medOrder.getDocType() == MedOrderDocType.Manual || medOrderItem.getRemarkStatus() == RemarkStatus.Unconfirm ){
				for (PharmOrderItem pharmOrderItem : medOrderItem.getPharmOrderItemList()) {
					refillScheduleManager.removeRefillScheduleListByPharmOrderItemNum(pharmOrder, pharmOrderItem.getOrgItemNum());					
				}
			}
			
			medOrder.removeDeletedMedOrderItem(medOrderItem);
			
		} else {
			updateOrderFromOrderView(inOrderViewInfo);

			for (PharmOrderItem pharmOrderItem : medOrderItem.getPharmOrderItemList()) {
				refillScheduleManager.removeRefillScheduleListByPharmOrderItemNum(pharmOrder, pharmOrderItem.getOrgItemNum());					
			}
			medOrderItem.removePharmOrderItemList();

			if (medOrderItem.getRemarkStatus() == RemarkStatus.Unconfirm && medOrderItem.getPrevMoItemId() != null) {
				if (Contexts.getSessionContext().get("prevMedOrderItemRemark") != null) {
					// if deleted item has item modify remark, replace previous item to current item
					// (replacement is done after POI and refill of unused modify remark item is removed)
					MedOrderItem prevMedOrderItem = (MedOrderItem) Contexts.getSessionContext().get("prevMedOrderItemRemark");
					
					prevMedOrderItem.setMedOrder(medOrder);
					prevMedOrderItem.preSave();				
					medOrder.getMedOrderItemList().set(itemIndex, prevMedOrderItem);
					
					medOrderItem = prevMedOrderItem;
				}
			}
			
			medOrderItem.setStatus(MedOrderItemStatus.Deleted);
			medOrderItem.setVetFlag(Boolean.FALSE);

			// if pharm remark is added or modified, mark the item being modified
			medOrderItem.setChangeFlag(Boolean.TRUE);

			if (!medOrder.isDhOrder()) {
				medOrderItem.setRemarkItemStatus(RemarkItemStatus.UnconfirmedDelete);
				medOrderItem.setPrevMoItemId(null);
				medOrderItem.setRemarkCreateDate(remarkCreateDate);
				medOrderItem.setRemarkCreateUser(remarkCreateUser);
				medOrderItem.setRemarkText(remarkText);
				medOrderItem.setRemarkConfirmDate(null);
				medOrderItem.setRemarkConfirmUser(null);
			}
			
			medOrderItem.updateDdiRelatedAlert();
		}
		pharmOrder.markChestFlag();
		orderEditManager.updateMedOrderItemChargeFlagByOrderEdit(medOrderItem, pharmOrder);		
	}

	public void updateOrderFromOrderView(OrderViewInfo inOrderViewInfo) {

		if (logger.isDebugEnabled()) {
			logger.debug("inOrderViewInfo=#0", (new EclipseLinkXStreamMarshaller()).toXML(inOrderViewInfo));
		}
		
		PharmOrder pharmOrder = getPharmOrder();
		MedOrder medOrder = pharmOrder.getMedOrder();
		
		pharmOrder.relinkAllItem();

		orderViewInfo = inOrderViewInfo;
		setOrderViewInfo(inOrderViewInfo);

		// set pharmOrder info
		orderViewInfo.setPharmOrder(pharmOrder);
		
		// set dispOrder info
		DispOrder dispOrder = getDispOrder();
		if (dispOrder != null) {
			orderViewInfo.setDispOrder(dispOrder);
		}

		// set medOrderItem, pharmOrderItem, refillScheduleItem info
		for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
			MedOrderItemInfo medOrderItemInfo = orderViewInfo.getMedOrderItemInfoByItemNum(medOrderItem.getItemNum());
			if( medOrderItemInfo != null ){
				medOrderItemInfo.setMedOrderItem(medOrderItem);
			}
		}

		for (PharmOrderItem pharmOrderItem : pharmOrder.getPharmOrderItemList()) {
			PharmOrderItemInfo pharmOrderItemInfo = orderViewInfo.getPharmOrderItemInfoByItemNum(pharmOrderItem.getItemNum());
			if( pharmOrderItemInfo != null ){
				pharmOrderItemInfo.setPharmOrderItem(pharmOrderItem);
			}
		}		
	}
	
	public void updatePharmRemark(OrderViewInfo inOrderViewInfo, int itemIndex, Date remarkCreateDate, String remarkCreateUser, String remarkText) {
		
		updateOrderFromOrderView(inOrderViewInfo);

		MedOrder medOrder = getPharmOrder().getMedOrder();
		
		MedOrderItem medOrderItem = medOrder.getMedOrderItemList().get(itemIndex);
		
		medOrderItem.setChangeFlag(Boolean.TRUE);
		
		medOrderItem.setRemarkCreateDate(remarkCreateDate);
		medOrderItem.setRemarkCreateUser(remarkCreateUser);
		medOrderItem.setRemarkText(remarkText);
		medOrderItem.setRemarkConfirmDate(null);
		medOrderItem.setRemarkConfirmUser(null);
	}
	
	private void retrieveMedOrder(Long medOrderId, Boolean requiredLockOrder) {
		MedOrder medOrder = em.find(MedOrder.class, medOrderId);
		medOrder.loadChild();
		for (MedOrderItem moi : medOrder.getMedOrderItemList()) {
			moi.loadDmInfo(medOrder.isMpDischarge());
			moi.recalTotalDurationInDay();
		}
		
		String hkid = "";
		if ( oneStopSummary.getPatient() != null ) {
			hkid = oneStopSummary.getPatient().getHkid();
		}
		
		if (medOrder.isDiscontinue()) {
			orderViewInfo.setAllowEditFlag(false);
			formatDiscontinueMessage(medOrder.getDiscontinueItem());
		}
		else if (requiredLockOrder && !lockOrder(medOrder, OneStopOrderType.MedOrder, medOrderId, hkid))
		{
			return;
		} 
		else if ( oneStopSummary.getPatient() != null && StringUtils.isNotBlank(oneStopSummary.getPatient().getHkid()) ) 
		{
			PatientSfiProfile patientSfiProfile = corpPmsServiceProxy.retrievePatientSfiProfile(oneStopSummary.getPatient().getHkid());
			if ( patientSfiProfile != null ) {
				orderViewInfo.setPatientSfi(patientSfiProfile.getPatientSfi());
			}
		}

		orderViewInfo.setTicketAssociateFlag(medOrder.getTicket() != null);
		medOrder.setTicket(em.find(Ticket.class, ticket.getId()));
		
		em.flush();
		em.clear();
		
		// update info from one-stop
		updateMedOrderFromOneStop(medOrder);

		// create PharmOrder
		PharmOrder pharmOrder = createPharmOrder(medOrder);
		orderViewInfo.setOrgMaxItemNum(pharmOrder.getMaxItemNum());
		medOrderManager.setPharmOrder(pharmOrder);

		// private handle
		if (pharmOrder.getPrivateFlag() && PharmOrderPatType.Private != pharmOrder.getPatType() ) {
			for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
				medOrderItem.markPurchaseByPatient();
				medOrderItem.setChangeFlag(Boolean.TRUE);
				if( pharmOrder.getMedCase().getCaseNum() != null && oneStopUtils.isInPatCase(pharmOrder.getMedCase().getCaseNum()) ){
					medOrderItem.setCmsChargeFlag(YesNoBlankFlag.Blank);
					medOrderItem.setChargeFlag(YesNoBlankFlag.Blank);
					medOrderItem.setUnitOfCharge(0);
				}else{
					medOrderItem.setCmsChargeFlag(YesNoBlankFlag.No);
					medOrderItem.setChargeFlag(YesNoBlankFlag.No);
					medOrderItem.setUnitOfCharge(0);
				}
			}
		}
		
		//Override ChargeFlag for mismatch 
		if (!pharmOrder.getPrivateFlag()){
			for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
				if( !propMap.getValueAsBoolean(CHARGING_STANDARD_ENABLED.getName()) ){
					if( medOrderItem.getChargeFlag() != YesNoBlankFlag.Blank ){
						medOrderItem.setCmsChargeFlag(YesNoBlankFlag.No);
						medOrderItem.setChargeFlag(YesNoBlankFlag.Blank);
						medOrderItem.setChangeFlag(Boolean.TRUE);
						medOrderItem.setUnitOfCharge(0);
					}
				}else if(pharmOrder.getMedCase() != null && oneStopUtils.isInPatCase(pharmOrder.getMedCase().getCaseNum()) ){
					if( medOrderItem.getChargeFlag() != YesNoBlankFlag.Blank ){
						medOrderItem.setCmsChargeFlag(YesNoBlankFlag.Blank);
						medOrderItem.setChargeFlag(YesNoBlankFlag.Blank);
						medOrderItem.setChangeFlag(Boolean.TRUE);
						medOrderItem.setUnitOfCharge(0);
					}
				}else if( medOrder.getPrescType() != MedOrderPrescType.MpDischarge && 
						  medOrderItem.getActionStatus() == ActionStatus.DispByPharm && 
						  pharmOrder.getMedCase() != null && 
						  !pharmOrder.getMedCase().getChargeFlag() &&  
						  medOrderItem.getChargeFlag() == YesNoBlankFlag.Yes ){
					medOrderItem.setChargeFlag(YesNoBlankFlag.No);
					medOrderItem.setChangeFlag(Boolean.TRUE);
				}
			}
		}
		
		// convert item
		itemConvertManager.convertMedOrder(pharmOrder);
		
		// check duplicate dispensed item in corp
		if (VETTING_DUPLICATEDOITEM_CHECK.get(Boolean.FALSE)) {
			duplicateItemCheckManager.checkCorpDuplicateItemByPharmOrder(pharmOrder);
		}

		if (VETTING_ALERT_PROFILE_ENABLED.get() && ALERT_PROFILE_ENABLED.get(Boolean.FALSE)) {
			try {
				orderViewInfo.setAlertProfile(alertProfileManager.retrieveAlertProfileByPharmOrder(pharmOrder));
			}
			catch (AlertProfileException e) {
				orderViewInfo.addInfoMsg("0592");
				alertAuditManager.alertServiceUnavailable(pharmOrder, ticket);
			}
		}

		if (ALERT_EHR_PROFILE_ENABLED.get()) {
			try {
				orderViewInfo.setAlertProfile(ehrAlertManager.retrieveEhrAllergySummaryByPharmOrder(orderViewInfo.getAlertProfile(), pharmOrder));
			}
			catch (EhrAllergyException e) {
				orderViewInfo.addInfoMsg("0984");
			}
		}
		
		// set urgent flag
		if (VETTING_PRINTING_URGENT_ENABLED.get(Boolean.FALSE)) {
			if ((medOrder.getVetBeforeFlag() && VETTING_MODIFIEDMOITEM_PRINTING_URGENT.get(Boolean.FALSE))
					|| (medOrder.getUrgentFlag() && VETTING_URGENTRECONORDER_PRINTING_URGENT.get(Boolean.FALSE))) {
				orderViewInfo.setUrgentFlag(Boolean.TRUE);
			}
		}
		
		orderViewInfo.bindPharmOrder(pharmOrder);
		orderViewInfo.bindOneStopOrderInfo(oneStopOrderInfo, medOrder.getWorkstore());
		orderViewInfo.setMedOrderNumForCoupon(medOrder.getOrderNum());
		
		if (medOrder.getPregnancyCheckFlag()) {
			orderViewInfo.setPregnancyCheckEditableFlag(false);
		}
		
		orderViewInfo.setEhrPatient(oneStopOrderInfo.getEhrPatient());
		
		// update
		retrieveDrsConfig(pharmOrder, medOrder, true, false);
	}

	private void retrieveManualOrder() {

		MedOrder medOrder = createMedOrder();

		// associate ticket
		medOrder.setTicket(ticket);

		// update info from one-stop
		updateMedOrderFromOneStop(medOrder);

		// create PharmOrder
		PharmOrder pharmOrder = createPharmOrder(medOrder);
		orderViewInfo.setOrgMaxItemNum(pharmOrder.getMaxItemNum());

		if (VETTING_ALERT_PROFILE_ENABLED.get() && ALERT_PROFILE_ENABLED.get(Boolean.FALSE)) {
			try {
				orderViewInfo.setAlertProfile(alertProfileManager.retrieveAlertProfileByPharmOrder(pharmOrder));
			}
			catch (AlertProfileException e) {
				orderViewInfo.addInfoMsg("0592");
				alertAuditManager.alertServiceUnavailable(pharmOrder, ticket);
			}
		}
		
		if (ALERT_EHR_PROFILE_ENABLED.get()) {
			try {
				orderViewInfo.setAlertProfile(ehrAlertManager.retrieveEhrAllergySummaryByPharmOrder(orderViewInfo.getAlertProfile(), pharmOrder));
			}
			catch (EhrAllergyException e) {
				orderViewInfo.addInfoMsg("0984");
			}
		}
		
		orderViewInfo.bindPharmOrder(pharmOrder);
		orderViewInfo.bindOneStopOrderInfo(oneStopOrderInfo, medOrder.getWorkstore());

		// update
		retrieveDrsConfig(pharmOrder, medOrder, true, false);
		
		medOrderManager.setPharmOrder(pharmOrder);
		
		if ( StringUtils.isNotBlank(medOrder.getPatient().getHkid()) ) {
			PatientSfiProfile patientSfiProfile = corpPmsServiceProxy.retrievePatientSfiProfile(medOrder.getPatient().getHkid());
			if ( patientSfiProfile != null ) {
				orderViewInfo.setPatientSfi(patientSfiProfile.getPatientSfi());
			}
		}
		
		orderViewInfo.setEhrPatient(oneStopOrderInfo.getEhrPatient());
	}

	@SuppressWarnings("unchecked")
	private void retrieveDispOrder(Long dispOrderId) {
		DispOrder dispOrder = em.find(DispOrder.class, dispOrderId);
		dispOrder.loadChild();

		PharmOrder pharmOrder = dispOrder.getPharmOrder();
		for (PharmOrderItem poi : pharmOrder.getPharmOrderItemList()) {
			poi.loadDmInfo();
		}
		pharmOrder.clearDirtyFlag();
		orderViewInfo.setOrgMaxItemNum(pharmOrder.getMaxItemNum());

		MedOrder medOrder = pharmOrder.getMedOrder();
		for (MedOrderItem moi : medOrder.getMedOrderItemList()) {
			moi.loadDmInfo(medOrder.isMpDischarge());
		}
		
		if (dispOrder.getDayEndStatus() != DispOrderDayEndStatus.None
				|| dispOrder.getBatchProcessingFlag()
				|| !workstore.equals(pharmOrder.getWorkstore())
				|| oneStopOrderInfo.getIsOrderReadOnly()
				|| medOrder.isDiscontinue()
				|| (medOrder.isDhOrder() && medOrder.getStatus() == MedOrderStatus.Deleted)) { // readonly
			
			String orderMsg = oneStopOrderStatusManager.verifyDispOrderStatusBeforeSwitchToVetting(dispOrderId);
			if (orderMsg != null) {
				orderViewInfo.addErrorMsg(orderMsg);
				return;
			}
			
			if (medOrder.isDiscontinue()) {
				formatDiscontinueMessage(medOrder.getDiscontinueItem());
			}
			orderViewInfo.setAllowEditFlag(Boolean.FALSE);
		}
		else if (!isWithinBatchDuration(dispOrder.getTicketDate())) {
			orderViewInfo.addInfoMsg("0260", new String[]{String.valueOf(ORDER_ALLOWUPDATE_DURATION.get())});
			orderViewInfo.setAllowEditFlag(Boolean.FALSE);
		}
		else { // editable
			if (medOrder.isDhOrder() && !medOrder.getDhLatestFlag()) {
				orderViewInfo.setAllowEditFlag(Boolean.FALSE);
				orderViewInfo.setReadOnlyDeleteRx(true);
			}
			else {
				if (!lockOrder(medOrder, OneStopOrderType.DispOrder, dispOrderId, pharmOrder.getHkid())) {
					return;
				}
				
				// auto suspend order
				if (dispOrder.getAdminStatus() == DispOrderAdminStatus.Normal) {
					suspendPrescManager.suspendPrescription(dispOrder, SUSPEND_ORDER_SUSPENDCODE.get(), true);
					orderViewInfo.setAutoSuspendFlag(Boolean.TRUE);
				}
				else {
					orderViewInfo.setPrevSuspendCode(dispOrder.getSuspendCode());
				}
			}
		}

		//Load Standard RefillScheduleList		
		List<RefillSchedule> refillScheduleList = (List<RefillSchedule>) em.createQuery(
				"select o from RefillSchedule o" + // 20120307 index check : RefillSchedule.pharmOrder : FK_REFILL_SCHEDULE_01
				" where o.pharmOrder.id = :pharmOrderId" +
				" and o.docType = :docType" +
				" order by o.groupNum, o.refillNum")
				.setParameter("pharmOrderId", pharmOrder.getId())
				.setParameter("docType", DocType.Standard)
				.getResultList();
		
		for( RefillSchedule refillSchedule : refillScheduleList ){
			refillSchedule.loadChild();
			if( orderViewInfo.getMedOrderNumForCoupon() == null ){
				orderViewInfo.setMedOrderNumForCoupon(refillSchedule.getRefillCouponNum().substring(0, 12));
			}
		}
		
		for( PharmOrderItem poi : pharmOrder.getPharmOrderItemList() ){
			poi.setStdRefillCheckFlag(true);
		}
	
		for ( DispOrderItem doi : dispOrder.getDispOrderItemList() ) {
			doi.getPharmOrderItem().setSfiOverrideWarnFlag(doi.getSfiOverrideWarnFlag());
		}
		
		logger.debug("retrieveDispOrder refillScheduleList: #0", refillScheduleList.size());
		
		if( orderViewInfo.getMedOrderNumForCoupon() == null){
			if( MedOrderDocType.Normal == medOrder.getDocType() ){
				orderViewInfo.setMedOrderNumForCoupon(medOrder.getOrderNum());
			}else if( MedOrderDocType.Manual == medOrder.getDocType() && medOrder.getPrevOrderNum() == null ){
				orderViewInfo.setMedOrderNumForCoupon(medOrder.getOrderNum());
			}
		}
		
		// retrieve capd voucher list
		List<CapdVoucher> capdVoucherList = capdVoucherManager.retrieveCapdVoucherList(dispOrder, true);		
		for (CapdVoucher capdVoucher : capdVoucherList) {
			capdVoucher.loadChild();
		}
		
		logger.debug("retrieveDispOrder capdVoucherList: #0", capdVoucherList.size());
		
		em.flush();
		em.clear();

		medOrder.resetNumOfLabel();
		
		if( dispOrder.getSfiFlag() ){
			//For InvoiceItem exemptCode
			List<InvoiceItem> invoiceItemList = (List<InvoiceItem>) em.createQuery(
					"select o from InvoiceItem o " + // 20160616 index check : Invoice.dispOrder : FK_INVOICE_01
					"where o.invoice.docType = :docType " +
					"and o.invoice.dispOrder.id = :dispOrderId " +
					"and (o.handleExemptCode is not null or o.nextHandleExemptCode is not null) "
					)
					.setParameter("docType", InvoiceDocType.Sfi)
					.setParameter("dispOrderId", dispOrderId)
					.getResultList();
			
			for( InvoiceItem invoiceItem : invoiceItemList ){
				PharmOrderItem invExemptPoi = pharmOrder.getPharmOrderItemByOrgItemNum(invoiceItem.getDispOrderItem().getPharmOrderItem().getOrgItemNum());
				invExemptPoi.getMedOrderItem().setShowExemptCodeFlag(true);
				
				if( invoiceItem.getHandleExemptCode() != null ){
					invExemptPoi.setHandleExemptCode(invoiceItem.getHandleExemptCode());
				}
				if( invoiceItem.getNextHandleExemptCode() != null ){
					invExemptPoi.setNextHandleExemptCode(invoiceItem.getNextHandleExemptCode());
				}
				invExemptPoi.setHandleExemptUser(invoiceItem.getHandleExemptUser());
			}
		}
		
		List<RefillSchedule> sfiRefillScheduleList = (List<RefillSchedule>) em.createQuery(
				"select o from RefillSchedule o" + // 20151012 index check : PharmOrder.id : PK_PHARM_ORDER
				" where o.pharmOrder.id = :pharmOrderId" +
				" and o.docType = :docType"+ 
				" and o.refillNum = 1"+
				" order by o.groupNum")
				.setParameter("pharmOrderId", pharmOrder.getId())
				.setParameter("docType", DocType.Sfi)
				.getResultList();
		
		for( RefillSchedule sfiRefillSchedule : sfiRefillScheduleList ){
			for(RefillScheduleItem sfiRsi : sfiRefillSchedule.getRefillScheduleItemList() ){
				PharmOrderItem exemptPoi = null;
				if( sfiRsi.getHandleExemptCode() != null || sfiRsi.getNextHandleExemptCode() != null ){
					exemptPoi = pharmOrder.getPharmOrderItemByOrgItemNum(sfiRsi.getPharmOrderItem().getOrgItemNum());
					exemptPoi.getMedOrderItem().setShowExemptCodeFlag(true);
					
					if( sfiRsi.getHandleExemptCode() != null ){
						exemptPoi.setHandleExemptCode(sfiRsi.getHandleExemptCode());
					}
					if( sfiRsi.getNextHandleExemptCode() != null ){
						exemptPoi.setNextHandleExemptCode(sfiRsi.getNextHandleExemptCode());
					}
					exemptPoi.setHandleExemptUser(sfiRsi.getHandleExemptUser());
				}
			}
		}
		
		pharmOrder.disconnectRefillScheduleList();	
		dispOrder.disconnectCapdVoucherList();
		
		medOrder.setTicket(dispOrder.getTicket());
		medOrder.removeSysDetetedItem();
		
		orderViewInfo.bindPharmOrder(pharmOrder);
		orderViewInfo.bindDispOrder(dispOrder);
		orderViewInfo.bindOneStopOrderInfo(oneStopOrderInfo, medOrder.getWorkstore());

		if (medOrder.getDocType() == MedOrderDocType.Normal && pharmOrder.getPregnancyCheckFlag()) {
			orderViewInfo.setPregnancyCheckEditableFlag(false);
		}

		orderViewInfo.setUrgentFlag(VETTING_PRINTING_URGENT_ENABLED.get(Boolean.FALSE));

		if (ALERT_PROFILE_ENABLED.get(Boolean.FALSE) && VETTING_ALERT_PROFILE_ENABLED.get()) {
			try {
				orderViewInfo.setAlertProfile(alertProfileManager.retrieveAlertProfileByPharmOrder(pharmOrder));
			}
			catch (AlertProfileException e) {
				orderViewInfo.addInfoMsg("0592");
				alertAuditManager.alertServiceUnavailable(pharmOrder, dispOrder.getTicket());
			}
		}

		if (ALERT_EHR_PROFILE_ENABLED.get()) {
			try {
				orderViewInfo.setAlertProfile(ehrAlertManager.retrieveEhrAllergySummaryByPharmOrder(orderViewInfo.getAlertProfile(), pharmOrder));
			}
			catch (EhrAllergyException e) {
				orderViewInfo.addInfoMsg("0984");
			}
		}
		
		if (medOrder.isDhOrder() && medOrder.getDhPatient().isAlertExist()) {
			orderViewInfo.addInfoMsg("0972");
		}

		retrieveDrsConfig(pharmOrder, medOrder, false, false);
		
		orderViewInfo.setEhrPatient(oneStopOrderInfo.getEhrPatient());
		
		medOrderManager.setPharmOrder(pharmOrder);
		medOrderManager.setDispOrder(dispOrder);
	}
	
	@SuppressWarnings("unchecked")
	private void retrieveRefillOrderByDispOrderId(Long dispOrderId) {
		List<RefillSchedule> refillScheduleList = em.createQuery(
				"select o from RefillSchedule o" + // 20120307 index check : RefillSchedule.dispOrder : FK_REFILL_SCHEDULE_02
				" where o.dispOrder.id = :dispOrderId" +	
				" and o.docType = :docType" +
				" order by o.groupNum, o.id")
				.setParameter("dispOrderId", dispOrderId)
				.setParameter("docType", DocType.Standard)
				.getResultList();
		
		
		if( refillScheduleList.size() > 0 ){
			Long refillScheduleId = refillScheduleList.get(0).getId();
			retrieveRefillOrder(refillScheduleId, true);
		}
		
		if ( orderViewInfo != null && oneStopOrderInfo != null ) {
			orderViewInfo.setEhrPatient(oneStopOrderInfo.getEhrPatient());
		}
	}
	
	private void retrieveRefillOrder(Long refillScheduleId, boolean bypassLockChecking) {

		if(!bypassLockChecking){

			RefillSchedule refillSchedule = em.find(RefillSchedule.class, refillScheduleId);
			DispOrder dispOrder = refillSchedule.getDispOrder();
			if (dispOrder == null || (dispOrder.getDayEndStatus() == DispOrderDayEndStatus.None && isWithinBatchDuration(dispOrder.getTicketDate())))
			{
				if (!lockOrder(refillSchedule.getPharmOrder().getMedOrder(), OneStopOrderType.RefillOrder, refillScheduleId, refillSchedule.getPharmOrder().getHkid()))
				{
					return;
				}	
			}
			em.flush();
			MedOrder medOrder = refillSchedule.getPharmOrder().getMedOrder();
			orderViewInfo.setMedOrderId(medOrder.getId());
			orderViewInfo.setMedOrderVersion(medOrder.getVersion());
		}
		
		RefillSchedule refillSchedule = retrieveOrderByRefillScheduleId(refillScheduleId);
		
		if (!bypassLockChecking && refillSchedule.isDiscontinue()) {
			formatDiscontinueMessage(refillSchedule.getDiscontinueItem());
		}
		
		if (VETTING_ALERT_PROFILE_ENABLED.get() && ALERT_PROFILE_ENABLED.get(Boolean.FALSE)) {
			try {
				orderViewInfo.setAlertProfile(alertProfileManager.retrieveAlertProfileByPharmOrder(refillSchedule.getPharmOrder()));
			}
			catch (AlertProfileException e) {
				orderViewInfo.addInfoMsg("0592");
				alertAuditManager.alertServiceUnavailable(refillSchedule.getPharmOrder(), ticket);
			}
		}
		
		if (ALERT_EHR_PROFILE_ENABLED.get()) {
			try {
				orderViewInfo.setAlertProfile(ehrAlertManager.retrieveEhrAllergySummaryByPharmOrder(orderViewInfo.getAlertProfile(), refillSchedule.getPharmOrder()));
			}
			catch (EhrAllergyException e) {
				orderViewInfo.addInfoMsg("0984");
			}
		}
		
		if (alertRetrieveManager.isMdsEnable() && isCheckMdsForRefillSchedule(refillSchedule)) {
			prescCheckManager.checkPrescAlertByMedOrderItemList(orderViewInfo, refillSchedule.getMedOrderItemList(), refillSchedule.getPharmOrder().getMedOrder().isMpDischarge());
		}
		
		if( Boolean.TRUE.equals(refillSchedule.getPharmOrder().getDrsFlag()) ){
			retrieveDrsConfig(refillSchedule.getPharmOrder(), refillSchedule.getPharmOrder().getMedOrder(), false, true);
		}
		
		if ( oneStopOrderInfo != null ) {
			orderViewInfo.setEhrPatient(oneStopOrderInfo.getEhrPatient());
		}
	}

	private boolean isCheckMdsForRefillSchedule(RefillSchedule rs) {
		switch (rs.getStatus()) {
			case NotYetDispensed:
			case BeingProcessed:
				return true;
			case Dispensed:
				return rs.getDispOrder().getAdminStatus() == DispOrderAdminStatus.Suspended;
			default:
				return false;
		}
	}
	
	private void retrieveSfiOrder(Long refillScheduleId) {

		RefillSchedule refillSchedule = em.find(RefillSchedule.class, refillScheduleId);
		MedOrder medOrder = refillSchedule.getPharmOrder().getMedOrder();
		DispOrder dispOrder = refillSchedule.getDispOrder();
		
		if (dispOrder == null || (dispOrder.getDayEndStatus() == DispOrderDayEndStatus.None && workstore.equals(dispOrder.getWorkstore()) && isWithinBatchDuration(dispOrder.getTicketDate())))
		{
			if (!lockOrder(medOrder, OneStopOrderType.SfiOrder, refillScheduleId, refillSchedule.getPharmOrder().getHkid()))
			{
				return;
			}
			
			//update dispOrder also in order to prevent reverse suspend at OSE at the same time
			if (dispOrder != null)
			{
				dispOrder.setUpdateDate(new Date());
				dispOrder.setUpdateUser(uamInfo.getUserId());
			}
		}
		
		em.flush();
		em.clear();
		
		// retrieve RefillSchedule
		List<RefillSchedule> refillScheduleList = retrieveOrderBySfiRefillScheduleId(refillScheduleId);
		filterOrderByRefillScheduleList(refillScheduleList);

		if (VETTING_ALERT_PROFILE_ENABLED.get() && ALERT_PROFILE_ENABLED.get(Boolean.FALSE)) {
			try {
				orderViewInfo.setAlertProfile(alertProfileManager.retrieveAlertProfileByPharmOrder(refillScheduleList.get(0).getPharmOrder()));
			}
			catch (AlertProfileException e) {
				orderViewInfo.addInfoMsg("0592");
				alertAuditManager.alertServiceUnavailable(refillScheduleList.get(0).getPharmOrder(), ticket);
			}
		}
		
		if (ALERT_EHR_PROFILE_ENABLED.get()) {
			try {
				orderViewInfo.setAlertProfile(ehrAlertManager.retrieveEhrAllergySummaryByPharmOrder(orderViewInfo.getAlertProfile(), refillScheduleList.get(0).getPharmOrder()));
			}
			catch (EhrAllergyException e) {
				orderViewInfo.addInfoMsg("0984");
			}
		}
		
		if ( oneStopOrderInfo != null ) {
			orderViewInfo.setEhrPatient(oneStopOrderInfo.getEhrPatient());
		}
	}

	@SuppressWarnings("unchecked")
	public List<RefillSchedule> retrieveOrderBySfiRefillScheduleId(Long refillScheduleId) {

		// retrieve RefillSchedule
		RefillSchedule refillSchedule = em.find(RefillSchedule.class, refillScheduleId);
		refillSchedule.loadChild();

		// retrieve DispOrder, PharmOrder, MedOrder
		DispOrder dispOrder = refillSchedule.getDispOrder();
		PharmOrder pharmOrder = refillSchedule.getPharmOrder();
		for (PharmOrderItem poi : pharmOrder.getPharmOrderItemList()) {
			poi.loadDmInfo();
		}
		for (MedOrderItem moi : pharmOrder.getMedOrder().getMedOrderItemList()) {
			moi.loadDmInfo(pharmOrder.getMedOrder().isMpDischarge());
		}

		List<RefillSchedule> dispRefillScheduleList = new ArrayList<RefillSchedule>();
		
		if (refillSchedule.isDiscontinue()) {
			formatDiscontinueMessage(refillSchedule.getDiscontinueItem());
			orderViewInfo.setAllowEditFlag(false);
		}
		else if (oneStopOrderInfo.getIsOrderReadOnly() || (dispOrder != null && !workstore.equals(dispOrder.getWorkstore()))) {
			orderViewInfo.setAllowEditFlag(false);
		}
		else if (dispOrder != null) {
			if (!isWithinBatchDuration(dispOrder.getTicketDate())) {
				orderViewInfo.addInfoMsg("0260", new String[]{String.valueOf(ORDER_ALLOWUPDATE_DURATION.get())});
				orderViewInfo.setAllowEditFlag(Boolean.FALSE);
			}
			else if (dispOrder.getAdminStatus() == DispOrderAdminStatus.Normal) {// auto suspend order
				suspendPrescManager.suspendPrescription(dispOrder, SUSPEND_ORDER_SUSPENDCODE.get(), true);
				orderViewInfo.setAutoSuspendFlag(Boolean.TRUE);
			}
			else {
				orderViewInfo.setPrevSuspendCode(dispOrder.getSuspendCode());
			}
		}
		
		if( dispOrder != null && DocType.Sfi.equals(refillSchedule.getDocType()) ){
			
			logger.debug("retrieveOrderBySfiRefillScheduleId dispOrderId: #0", dispOrder.getId());
			
			dispRefillScheduleList = em.createQuery(
					"select o from RefillSchedule o" + // 20120307 index check : RefillSchedule.dispOrder : FK_REFILL_SCHEDULE_02
					" where o.dispOrder.id = :dispOrderId" +
					" and o.docType = :docType")
					.setParameter("dispOrderId", dispOrder.getId())															
					.setParameter("docType", DocType.Sfi)
					.getResultList();
			
			logger.debug("retrieveOrderBySfiRefillScheduleId dispRefillScheduleList size: #0", dispRefillScheduleList.size());
			
			for( RefillSchedule dispRefillSchedule : dispRefillScheduleList ){
				dispRefillSchedule.loadChild();
				em.clear();
				for (RefillScheduleItem dispRefillScheduleItem : dispRefillSchedule.getRefillScheduleItemList()) {
					PharmOrderItem pharmOrderItem = pharmOrder.getPharmOrderItemByOrgItemNum(dispRefillScheduleItem.getItemNum());
					logger.debug("retrieveOrderBySfiRefillScheduleId pharmOrderItem itemNum:#0,pharmOrderItem: #1", dispRefillScheduleItem.getItemNum(), pharmOrderItem);
					if( pharmOrderItem == null ){
						continue;
					}

					//for SfiSubsidized item calculation
					pharmOrderItem.setOrgIssueQty(pharmOrderItem.getIssueQty());
					
					pharmOrderItem.setHandleExemptCode(dispRefillScheduleItem.getHandleExemptCode());
					pharmOrderItem.setNextHandleExemptCode(dispRefillScheduleItem.getNextHandleExemptCode());
					pharmOrderItem.setHandleExemptUser(dispRefillScheduleItem.getHandleExemptUser());
					pharmOrderItem.setSfiQty(dispRefillScheduleItem.getRefillQty());
					pharmOrderItem.setIssueQty(BigDecimal.valueOf(dispRefillScheduleItem.getCalQty()));
					
					if( !StringUtils.isBlank(dispRefillScheduleItem.getHandleExemptCode()) || !StringUtils.isBlank(dispRefillScheduleItem.getNextHandleExemptCode()) ){
						pharmOrderItem.getMedOrderItem().setShowExemptCodeFlag(true);						
					}else{
						pharmOrderItem.getMedOrderItem().setShowExemptCodeFlag(false);
					}
					
					DispOrderItem dispOrderItem = dispOrder.getDispOrderItemByPoItemNum(pharmOrderItem.getItemNum());
					if( dispOrderItem != null ){
						pharmOrderItem.setWarnCode1( dispOrderItem.getWarnCode1() );
						pharmOrderItem.setWarnCode2( dispOrderItem.getWarnCode2() );
						pharmOrderItem.setWarnCode3( dispOrderItem.getWarnCode3() );
						pharmOrderItem.setWarnCode4( dispOrderItem.getWarnCode4() );
						pharmOrderItem.setSfiOverrideWarnFlag( dispOrderItem.getSfiOverrideWarnFlag());
					}
				}
			}
			ticket = dispOrder.getTicket();
		}else{		
			em.clear();
			for (RefillScheduleItem refillScheduleItem : refillSchedule.getRefillScheduleItemList()) {
				PharmOrderItem pharmOrderItem = refillScheduleItem.getPharmOrderItem();

				//for sfi subsidized item calculation
				pharmOrderItem.setOrgIssueQty(pharmOrderItem.getIssueQty());
				
				pharmOrderItem.setHandleExemptCode(refillScheduleItem.getHandleExemptCode());
				pharmOrderItem.setSfiQty(refillScheduleItem.getRefillQty());
				pharmOrderItem.setIssueQty(BigDecimal.valueOf(refillScheduleItem.getCalQty()));
				
				if( !StringUtils.isBlank(refillScheduleItem.getHandleExemptCode()) ){
					pharmOrderItem.getMedOrderItem().setShowExemptCodeFlag(true);					
				}else{
					pharmOrderItem.getMedOrderItem().setShowExemptCodeFlag(false);
				}
			}
			dispRefillScheduleList.add(refillSchedule);
		}
	
		pharmOrder.clearDirtyFlag();
		MedOrder medOrder = pharmOrder.getMedOrder();

		em.flush();
		em.clear();
		
		medOrder.removeSysDetetedItem();
		medOrder.resetNumOfLabel();

		if (activeProfile.getType() == OperationProfileType.Pms) {
			pharmOrder.setAwaitSfiFlag(true);
		}
		else {
			pharmOrder.setAwaitSfiFlag(CHARGING_SFI_SUSPEND_ORDER_ENABLED.get(Boolean.FALSE));
		}

		orderViewInfo.bindPharmOrder(pharmOrder);
		orderViewInfo.bindOneStopOrderInfo(oneStopOrderInfo, medOrder.getWorkstore());
		if (dispOrder != null) {
			orderViewInfo.setPrintLang(dispOrder.getPrintLang());
			orderViewInfo.setUrgentFlag(VETTING_PRINTING_URGENT_ENABLED.get(Boolean.FALSE));
			orderViewInfo.setDispSfiRefillFlag(true);
		}else{
			orderViewInfo.setPrintLang(refillSchedule.getPrintLang());
		}
		
		if (pharmOrder.getPregnancyCheckFlag()) {
			orderViewInfo.setPregnancyCheckEditableFlag(false);
		}
		
		
		// associate Ticket
		medOrder.setTicket(ticket);
		if (logger.isDebugEnabled()) {
			if (ticket == null) {
				logger.debug("retrieveOrderBySfiRefillScheduleId\nticket : null");
			}
			else {
				logger.debug("retrieveOrderBySfiRefillScheduleId\nticket : #0, #1", ticket.getTicketNum(), ticket.getId());
			}
		}
		
		orderViewInfo.setRefillScheduleId(refillScheduleId);
		orderViewInfo.setMedOrderNumForCoupon(refillSchedule.getRefillCouponNum().substring(3, 15));
		
		//disconnect refillScheduleList
		pharmOrder.clearRefillScheduleList();
		pharmOrder.disconnectRefillScheduleList();
		for( RefillSchedule sfiRs : dispRefillScheduleList ){
			sfiRs.connect(pharmOrder);
		}
		pharmOrder.disconnectRefillScheduleList();
		
		medOrderManager.setDispOrder(dispOrder);
		medOrderManager.setPharmOrder(pharmOrder);

		return dispRefillScheduleList;
	}
	
	public RefillSchedule retrieveOrderByRefillScheduleId(Long refillScheduleId) {

		// retrieve RefillSchedule
		RefillSchedule refillSchedule = em.find(RefillSchedule.class, refillScheduleId);
		refillSchedule.loadChild();
		
		// retrieve DispOrder, PharmOrder, MedOrder
		DispOrder dispOrder = refillSchedule.getDispOrder();
		PharmOrder pharmOrder = refillSchedule.getPharmOrder();
		for (PharmOrderItem poi : pharmOrder.getPharmOrderItemList()) {
			poi.loadDmInfo();
		}
		pharmOrder.clearDirtyFlag();
		MedOrder medOrder = pharmOrder.getMedOrder();
		for (MedOrderItem moi : medOrder.getMedOrderItemList()) {
			moi.loadDmInfo(medOrder.isMpDischarge());
		}

		em.flush();
		em.clear();
		
		orderViewInfo.bindPharmOrder(pharmOrder);
		
		logger.debug("retrieveOrderByRefillScheduleId oneStopOrderInfo #0, OneStopOrderType #1", oneStopOrderInfo,orderViewInfo.getOneStopOrderType());
		if( OneStopOrderType.RefillOrderReadOnly != orderViewInfo.getOneStopOrderType() ){
			orderViewInfo.bindOneStopOrderInfo(oneStopOrderInfo, medOrder.getWorkstore());
		}
		if (dispOrder != null) {
			orderViewInfo.bindDispOrder(dispOrder);
			medOrder.setTicket(dispOrder.getTicket());
		}else{
			orderViewInfo.setPrintLang(refillSchedule.getPrintLang());
			medOrder.setTicket(ticket);
		}
		
		if (logger.isDebugEnabled()) {
			if (ticket == null) {
				logger.debug("retrieveOrderByRefillScheduleId\nticket : null");
			}
			else {
				logger.debug("retrieveOrderByRefillScheduleId\nticket : #0, #1", ticket.getTicketNum(), ticket.getId());
			}
		}
		
		if (pharmOrder.getPregnancyCheckFlag()) {
			orderViewInfo.setPregnancyCheckEditableFlag(false);
		}

		orderViewInfo.setRefillScheduleId(refillScheduleId);
		orderViewInfo.setMedOrderNumForCoupon(refillSchedule.getRefillCouponNum().substring(0, 12));
		medOrderManager.setRefillSchedule(refillSchedule);
		medOrderManager.setDispOrder(dispOrder);
		medOrderManager.setPharmOrder(pharmOrder);

		return refillSchedule;
	}

	/* clear moi, poi NOT related to current rs */
	private void filterOrderByRefillScheduleList(List<RefillSchedule> refillScheduleList) {

		PharmOrder pharmOrder = refillScheduleList.get(0).getPharmOrder();
		MedOrder medOrder = pharmOrder.getMedOrder();
		
		// clear PharmOrderItem
		pharmOrder.getPharmOrderItemList().clear();

		// clear MedOrderItem
		medOrder.getMedOrderItemList().clear();

		List<Integer> moiOrgItemNumList = new ArrayList<Integer>();
		
		for( RefillSchedule refillSchedule : refillScheduleList ){
			for (RefillScheduleItem refillScheduleItem : refillSchedule.getRefillScheduleItemList()) {
				pharmOrder.addPharmOrderItem(refillScheduleItem.getPharmOrderItem());
				if( !moiOrgItemNumList.contains(refillScheduleItem.getPharmOrderItem().getMedOrderItem().getOrgItemNum()) ){
					medOrder.addMedOrderItem(refillScheduleItem.getPharmOrderItem().getMedOrderItem());
					moiOrgItemNumList.add(refillScheduleItem.getPharmOrderItem().getMedOrderItem().getOrgItemNum());
				}
			}
		}
	}
		
	private boolean lockOrder(MedOrder medOrder, OneStopOrderType oneStopOrderType, Long orderId, String hkid) 
	{
		String orderMsg = null;
		switch (oneStopOrderType)
		{
			case MedOrder:
				orderMsg = oneStopOrderStatusManager.verifyMedOrderStatusBeforeSwitchToVetting(orderId);
				break;
				
			case DispOrder:
				orderMsg = oneStopOrderStatusManager.verifyDispOrderStatusBeforeSwitchToVetting(orderId);
				break;
				
			case RefillOrder:
			case SfiOrder:
				orderMsg = oneStopOrderStatusManager.verifyRefillOrderStatusBeforeSwitchToVetting(orderId);
				break;
		}
		
		if (orderMsg != null)
		{
			orderViewInfo.addErrorMsg(orderMsg);
			return false;
		}
		
		
		StartVettingResult result = vettingManager.startVetting(medOrder, hkid);
		
		if (!result.getSuccessFlag()) {
			orderViewInfo.addErrorMsg(result.getMessageCode(), result.getMessageParam());
			return false;
		}
		
		if ( result.getPatientSfiProfile() != null ) {
			orderViewInfo.setPatientSfi(result.getPatientSfiProfile().getPatientSfi());
		}

		return true;
	}
	
	public void retrieveDhOrder(Long medOrderId) {

		String hkid = "";
		if ( oneStopSummary.getPatient() != null ) {
			hkid = oneStopSummary.getPatient().getHkid();
		}
		StartVettingResult result = vettingManager.startVetting(medOrderId, hkid);
		if (!result.getSuccessFlag()) {
			orderViewInfo.addErrorMsg(result.getMessageCode(), result.getMessageParam());
			return;
		}

		MedOrder medOrder = result.getMedOrder();
		medOrder.setOrderNum(null);
		medOrder.setTicket(em.find(Ticket.class, ticket.getId()));
		medOrder.setPatHospCode(StringUtils.isBlank(workstore.getDefPatHospCode())?workstore.getHospCode():workstore.getDefPatHospCode());

		// update info from one-stop
		updateMedOrderFromOneStop(medOrder);
		
		if (!medOrder.getDhLatestFlag()) {
			orderViewInfo.setAllowEditFlag(Boolean.FALSE);
		}
		
		// create PharmOrder
		PharmOrder pharmOrder = createPharmOrder(medOrder);
		orderViewInfo.setOrgMaxItemNum(pharmOrder.getMaxItemNum());
		medOrderManager.setPharmOrder(pharmOrder);
		
		// convert item
		itemConvertManager.convertMedOrder(pharmOrder);
		
		for (MedOrderItem moi : medOrder.getMedOrderItemList()) {
			if (FmStatus.FreeTextEntryItem.getDataValue().equals(moi.getFmStatus())) {
				moi.setStatus(MedOrderItemStatus.Deleted);
				moi.setVetFlag(false);
				moi.setChangeFlag(Boolean.TRUE);
				if (moi.getPharmOrderItemList() != null) {
					pharmOrder.getPharmOrderItemList().removeAll(moi.getPharmOrderItemList());
				}
				moi.setPharmOrderItemList(new ArrayList<PharmOrderItem>());
			}
		}
		
		orderEditManager.updateMedOrderItemChargeFlagByOrderEdit(null, pharmOrder);
		
		// check duplicate dispensed item in corp
		if (VETTING_DUPLICATEDOITEM_CHECK.get(Boolean.FALSE)) {
			duplicateItemCheckManager.checkCorpDuplicateItemByPharmOrder(pharmOrder);
		}

		if (VETTING_ALERT_PROFILE_ENABLED.get() && ALERT_PROFILE_ENABLED.get(Boolean.FALSE)) {
			try {
				orderViewInfo.setAlertProfile(alertProfileManager.retrieveAlertProfileByPharmOrder(pharmOrder));
			}
			catch (AlertProfileException e) {
				orderViewInfo.addInfoMsg("0592");
				alertAuditManager.alertServiceUnavailable(pharmOrder, ticket);
			}
		}
		
		if (ALERT_EHR_PROFILE_ENABLED.get()) {
			try {
				orderViewInfo.setAlertProfile(ehrAlertManager.retrieveEhrAllergySummaryByPharmOrder(orderViewInfo.getAlertProfile(), pharmOrder));
			}
			catch (EhrAllergyException e) {
				orderViewInfo.addInfoMsg("0984");
			}
		}
		
		if (medOrder.getDhPatient().isAlertExist()) {
			orderViewInfo.addInfoMsg("0972");
		}

		orderViewInfo.bindPharmOrder(pharmOrder);
		orderViewInfo.bindOneStopOrderInfo(oneStopOrderInfo, medOrder.getWorkstore());
		
		medOrderManager.setPharmOrder(getPharmOrder());

		if ( result.getPatientSfiProfile() != null ) {
			orderViewInfo.setPatientSfi(result.getPatientSfiProfile().getPatientSfi());
		}
		
		orderViewInfo.setEhrPatient(oneStopOrderInfo.getEhrPatient());
	}
	
	private void updateMedOrderFromOneStop(MedOrder medOrder) {
		medOrder.setPatient(oneStopSummary.getPatient());

		if (medOrder.getDocType() == MedOrderDocType.Manual) {
			if (medOrder.getPrescType() == MedOrderPrescType.Dh) {
				
			}
			else {
				medOrder.setPatHospCode(oneStopOrderInfo.getPatHospCode());
				medOrder.setPrescType(oneStopOrderInfo.getPrescType());
				medOrder.setSpecCode(oneStopOrderInfo.getPhsSpecCode());
			}
		}
		
		if (logger.isDebugEnabled()) {
			logger.debug("oneStopOrderInfo: prescType=#0, doctorCode=#1, phsSpecCode=#2, patType=#3",
					oneStopOrderInfo.getPrescType(), oneStopOrderInfo.getDoctorCode(),
					oneStopOrderInfo.getPhsSpecCode(), oneStopOrderInfo.getPatType());
		}
		
		if (medOrder.isMpDischarge()) {

			if (oneStopOrderInfo.getPreVetInfoList() != null) {
				medOrder.setUrgentFlag(oneStopOrderInfo.getPreVetUrgentFlag());
				medOrder.setPreVetUser(oneStopOrderInfo.getPreVetUser());
				medOrder.setPreVetDate(oneStopOrderInfo.getPreVetDate());
				medOrder.setPreVetUserName(oneStopOrderInfo.getPreVetUserName());
				
				for (PreVetInfo preVetInfo : oneStopOrderInfo.getPreVetInfoList()) {
					
					String preVetNoteText = preVetInfo.getPreVetNoteText();
					String preVetNoteUser = preVetInfo.getPreVetNoteUser();
					Date preVetNoteDate   = preVetInfo.getPreVetNoteDate();
					
					if (preVetNoteText != null && preVetNoteDate == null) {
						preVetNoteDate = Calendar.getInstance().getTime();
						preVetNoteUser = identity.getCredentials().getUsername();
					}
					
					MedOrderItem medOrderItem = medOrder.getMedOrderItemByItemNum(preVetInfo.getItemNum());
					medOrderItem.setPreVetNoteText(preVetNoteText);
					medOrderItem.setPreVetNoteUser(preVetNoteUser);
					medOrderItem.setPreVetNoteDate(preVetNoteDate);
				}
			}			
		}
	}
	
	private void retrieveDrsConfig(PharmOrder pharmOrder, MedOrder medOrder, boolean showMessage, boolean refillOrderFlag){
		Pair<DrsConfig, List<String>> drsPair = refillScheduleManager.retrieveDrsConfig(pharmOrder, medOrder, refillOrderFlag);
		
		vettingDrsConfig = drsPair.getFirst();
		
		if (showMessage) {
			if (vettingDrsConfig != null) {
				orderViewInfo.setDrsFlag(true);
			}
			orderViewInfo.setDrsMsgCodeList(drsPair.getSecond());
		}
	}
	
	private MedOrder createMedOrder() {

		MedOrder medOrder = new MedOrder();

		medOrder.setOrderDate(Calendar.getInstance().getTime());
		medOrder.setDocType(MedOrderDocType.Manual);
		medOrder.clearMedOrderItemList();
		medOrder.clearPharmOrderList();
		medOrder.setWorkstore(workstore);
		medOrder.setMedCase(null);
		medOrder.clearMedOrderFmList();

		return medOrder;
	}

	private PharmOrder createPharmOrder(MedOrder medOrder) {
		
		PharmOrder pharmOrder = new PharmOrder();
		pharmOrder.setCapdSplitXml(null);
		pharmOrder.clearPharmOrderItemList();
	
		pharmOrder.setWardCode(oneStopOrderInfo.getPhsWardCode());
		pharmOrder.setPatCatCode(oneStopOrderInfo.getPhsPatCat());
		pharmOrder.setPatType(oneStopOrderInfo.getPatType());
		pharmOrder.setSpecCode(oneStopOrderInfo.getPhsSpecCode());
	
		pharmOrder.setMedCase(oneStopOrderInfo.getMedCase());
		
		pharmOrder.setReceiptNum(oneStopOrderInfo.getReceiptNum());
		pharmOrder.setForceProceedFlag(oneStopOrderInfo.getStandardForceProceed());
		
		pharmOrder.setMedOrder(medOrder);
		pharmOrder.setWorkstore(workstore);
		pharmOrder.clearRefillScheduleList();
		pharmOrder.clearDispOrderList();

		pharmOrder.setPregnancyCheckFlag(medOrder.getPregnancyCheckFlag());
		
		pharmOrder.setWorkstationCode(workstation.getWorkstationCode());
		
		pharmOrder.setPatient(medOrder.getPatient());

		if (activeProfile.getType() == OperationProfileType.Pms) {
			pharmOrder.setAwaitSfiFlag(true);
		}
		else {
			pharmOrder.setAwaitSfiFlag(CHARGING_SFI_SUSPEND_ORDER_ENABLED.get(Boolean.FALSE));
		}
		
		pharmOrder.setOrderDate(medOrder.getOrderDate());
		if (medOrder.getTicket() != null) {
			pharmOrder.setTicketDate(medOrder.getTicket().getTicketDate());
		}
		
		medOrder.addPharmOrder(pharmOrder);
		
		pharmOrder.setPrivateFlag(isPrivatePatient(medOrder, pharmOrder));
		
		return pharmOrder;
	}
	
	private boolean isPrivatePatient(MedOrder medOrder, PharmOrder pharmOrder) {
		
		if (medOrder.isDhOrder()) {
			return false;
		}
		
		// check private patient
		DrugChargeRuleCriteria drugChargeRuleCriteria = new DrugChargeRuleCriteria();
		drugChargeRuleCriteria.setPharmOrderPatType(pharmOrder.getPatType().getDataValue());
		drugChargeRuleCriteria.setMedOrderPrescType(medOrder.getPrescType().getDataValue());
		drugChargeRuleCriteria.setSpecCode(pharmOrder.getSpecCode());
		drugChargeRuleCriteria.setDispHospCode(workstore.getHospCode());
		
		MedCase medCase;
		if (pharmOrder.getMedCase() != null) {
			medCase = pharmOrder.getMedCase();
		}
		else if (medOrder.getMedCase() != null) {
			medCase = medOrder.getMedCase();
		}
		else {
			medCase = null;
		}
		
		if (medCase != null) {
			drugChargeRuleCriteria.setPasSpecCode(medCase.getPasSpecCode());
			drugChargeRuleCriteria.setPasSubSpecCode(medCase.getPasSubSpecCode());			
			drugChargeRuleCriteria.setPasPayCode(medCase.getPasPayCode());
			drugChargeRuleCriteria.setPasPatGroupCode(medCase.getPasPatGroupCode());
			drugChargeRuleCriteria.setPatHospCode(medOrder.getPatHospCode());
			
			if(medCase.getCaseNum() != null && oneStopUtils.isInPatCase(medCase.getCaseNum()) ){
				drugChargeRuleCriteria.setInPatientCase(true);
			}
		}
		
		logger.debug("drugChargeRuleCriteria====================#0,#1,#2,#3", drugChargeRuleCriteria.getPharmOrderPatType(), drugChargeRuleCriteria.getPasSpecCode(), drugChargeRuleCriteria.getPasSubSpecCode(), drugChargeRuleCriteria.getDispHospCode());
		
		drugChargeRuleCriteria = chargeCalculation.checkPrivateHandleRule(drugChargeRuleCriteria);
		
		logger.debug("drugChargeRuleCriteria====================#0,#1", drugChargeRuleCriteria.getChargeType(), drugChargeRuleCriteria.isPrivateHandle());
		
		if (logger.isDebugEnabled()) {
			logger.debug("oneStopOrderInfo: phsWard=#0, phsPatCat=#1, patType=#2, phsSpecCode=#3",
					oneStopOrderInfo.getPhsWardCode(), oneStopOrderInfo.getPhsPatCat(),
					oneStopOrderInfo.getPatType(), oneStopOrderInfo.getPhsSpecCode());
		
			logger.debug("oneStopOrderInfo.medCase is null: #0", oneStopOrderInfo.getMedCase() == null);
			
			logger.debug("drugChargeRuleCriteria : #0", (new EclipseLinkXStreamMarshaller()).toXML(drugChargeRuleCriteria));
		}
		
		return drugChargeRuleCriteria.isPrivateHandle();
	}
	
	private void createOrderViewInfo(OneStopOrderType orderType) {

		medOrderManager.clear();
		
		orderViewInfo = new OrderViewInfo();
		orderViewInfo.setOneStopOrderType(orderType);
		setOrderViewInfo(orderViewInfo);

		// set label language
		orderViewInfo.setPrintLang(PrintLang.dataValueOf(LABEL_DISPLABEL_LANG.get(PrintLang.Chi.getDataValue())));
		
		//set NumOfCoupon
		orderViewInfo.setNumOfCoupon( vettingRefillConfig.getCouponPrintCopies() );
		
		if (activeProfile.getType() == OperationProfileType.Pms) {
			orderViewInfo.setAwaitSfiEditableFlag(false);
		}
		else {
			orderViewInfo.setAwaitSfiEditableFlag(true);
		}
	}

	private boolean isOrderModified(MedOrder medOrder) {

		MedOrder orgMedOrder = em.find(MedOrder.class, medOrder.getId());
		
		if (!orgMedOrder.getVersion().equals(medOrder.getVersion())) {
			orderViewInfo.addErrorMsg("0187");
			return true;
		}

		Map<Long, MedOrderItem> orgMedOrderItemMap = new HashMap<Long, MedOrderItem>();
		for (MedOrderItem orgMedOrderItem : orgMedOrder.getMedOrderItemList()) {
			orgMedOrderItemMap.put(orgMedOrderItem.getId(), orgMedOrderItem);
		}
		
		for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
			if (medOrderItem.getId() == null) {
				continue;
			}
			if (orgMedOrderItemMap.containsKey(medOrderItem.getId()) && medOrderItem.getVersion() != null && !medOrderItem.getVersion().equals(orgMedOrderItemMap.get(medOrderItem.getId()).getVersion())) {
				orderViewInfo.addErrorMsg("0187");
				return true;
			}
		}
		
		return false;

	}
	
	private void setSfiQtyForSfiStepUpDownItem(MedOrder mo) {
		for (MedOrderItem moi : mo.getMedOrderItemList()) {
			if (moi.isSfi() && moi.getRegimen() != null && moi.getRegimen().getType() == RegimenType.StepUpDown) {
				for (PharmOrderItem poi : moi.getPharmOrderItemList()) {
					poi.setSfiQty(Integer.valueOf(poi.getIssueQty().intValue()));
				}
			}
		}
	}

	private RefillSchedule getRefillSchedule() {
		return (RefillSchedule) Contexts.getSessionContext().get("refillSchedule");
	}
	
	private DispOrder getDispOrder() {
		return (DispOrder) Contexts.getSessionContext().get("dispOrder");
	}
	
	private PharmOrder getPharmOrder() {
		return (PharmOrder) Contexts.getSessionContext().get("pharmOrder");
	}
	
	public void setOrderViewInfo(OrderViewInfo orderViewInfo) {
		Contexts.getSessionContext().set("orderViewInfo", orderViewInfo);
	}
	
	public RetrievePatientResult retrievePatientByHkid(String hkid) {
		Patient pasPatient = null;
		try {
			pasPatient = pasServiceWrapper.retrievePasPatientByHkid(hkid, "", "", false);
			return new RetrievePatientResult(true, pasPatient);
		}
		catch (PasException e) {
			logger.debug("No patient is found in HKPMI");
			return new RetrievePatientResult(true, null);
		}
		catch (UnreachableException e) {
			logger.debug("Hkpmi service is down");
			return new RetrievePatientResult(false, null);
		}
	}
	
	public List<String> usePatientFromHkpmi(Patient patient) {
		
		PharmOrder pharmOrder = getPharmOrder();
		pharmOrder.updateByPatientEntity(patient);
		orderViewInfo.setHkid(patient.getHkid());
		
		List<String> msgCodeList = new ArrayList<String>();
		
		if (ALERT_PROFILE_ENABLED.get(Boolean.FALSE) && VETTING_ALERT_PROFILE_ENABLED.get()) {
			try {
				orderViewInfo.setAlertProfile(alertProfileManager.retrieveAlertProfileByPharmOrder(pharmOrder));
			}
			catch (AlertProfileException e) {
				msgCodeList.add("0592");
				alertAuditManager.alertServiceUnavailable(pharmOrder, ticket);
			}
		}
	
		if (ALERT_EHR_PROFILE_ENABLED.get()) {
			try {
				orderViewInfo.setAlertProfile(ehrAlertManager.retrieveEhrAllergySummaryByPharmOrder(orderViewInfo.getAlertProfile(), pharmOrder));
			}
			catch (EhrAllergyException e) {
				msgCodeList.add("0984");
			}
		}

		return msgCodeList;
	}
	
	private void formatDiscontinueMessage(List<MedOrderItem> moiList) {
		List<String> messageTlfList = itemConvertManager.formatDiscontinueMessage(moiList);
		for (String messageTlf : messageTlfList) {
			orderViewInfo.getDiscontinueMessageTlf().put(moiList.get(messageTlfList.indexOf(messageTlf)).getItemNum(), messageTlf);
		}
	}
	
	private void resetItemConvClearMoDoseAlert(MedOrder medOrder){
		for(MedOrderItem moi: medOrder.getMedOrderItemList()){
			if( moi.getRxItem() != null ){
				moi.getRxItem().setMoDoseAlertFlag(Boolean.FALSE);
			}
		}
	}
	
	private boolean isWithinBatchDuration(Date ticketDate){
		return Days.daysBetween(new DateTime(ticketDate), new DateTime()).getDays() <= ORDER_ALLOWUPDATE_DURATION.get();
	}

	@Remove
	public void destroy() {
		if (orderViewInfo != null) {
			orderViewInfo = null;
		}
		
		if( vettingDrsConfig != null ){
			vettingDrsConfig = null;
		}
	}
}
