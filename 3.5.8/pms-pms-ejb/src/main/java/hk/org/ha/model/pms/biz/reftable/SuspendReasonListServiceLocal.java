package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.persistence.reftable.SuspendReason;

import java.util.Collection;

import javax.ejb.Local;

@Local
public interface SuspendReasonListServiceLocal {

	void retrieveSuspendReasonList();
	
	void updateSuspendReasonList(Collection<SuspendReason> newSuspendList);
	
	void destroy();
	
}
