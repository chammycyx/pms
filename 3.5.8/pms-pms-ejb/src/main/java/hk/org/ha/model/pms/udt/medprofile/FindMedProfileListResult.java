package hk.org.ha.model.pms.udt.medprofile;


public enum FindMedProfileListResult {
	NoProfileFound,
	NoActiveManualProfileFound,
	Normal
}
