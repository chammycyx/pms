package hk.org.ha.model.pms.biz.info;

import hk.org.ha.model.pms.vo.info.KeyGuide;

import java.util.List;

import javax.ejb.Local;

@Local
public interface ExportKeyGuideServiceLocal {
	
	void exportKeyGuide(List<KeyGuide> exportKeyList);
	
	void generateKeyGuide();
	
	void destroy();
}
