package hk.org.ha.model.pms.biz.refill;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.persistence.disp.RefillScheduleItem;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.refill.DocType;
import hk.org.ha.model.pms.udt.refill.RefillScheduleRefillStatus;
import hk.org.ha.model.pms.udt.refill.RefillScheduleStatus;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.vo.label.DispLabel;
import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.joda.time.DateMidnight;

@AutoCreate
@Stateless
@Name("sfiRefillScheduleListManager")
@MeasureCalls
public class SfiRefillScheduleListManagerBean implements SfiRefillScheduleListManagerLocal {		
	private static final String JAXB_CONTEXT_LABEL = "hk.org.ha.model.pms.vo.label";
	
	@In
	private RefillScheduleManagerLocal refillScheduleManager;
	
	@In
	private Workstore workstore;
	
	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	public List<RefillSchedule> getSfiRefillScheduleList(DispOrder dispOrder) {
		return em.createQuery(
				"select o from RefillSchedule o" + // 20120303 index check : RefillSchedule.dispOrder : FK_REFILL_SCHEDULE_02
				" where o.dispOrder = :dispOrder" +
				" and o.docType = :docType")
				.setParameter("dispOrder", dispOrder)
				.setParameter("docType", DocType.Sfi)
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	private List<RefillSchedule> getSfiRefillScheduleListByRefillScheduleGroupNum(RefillSchedule refillScheduleIn){
		return 	em.createQuery(
				"select o from RefillSchedule o" + // 20120303 index check : RefillSchedule.pharmOrder : FK_REFILL_SCHEDULE_01
				" where o.groupNum = :groupNum" +
				" and o.pharmOrder = :pharmOrder" +
				" and o.docType = :docType")
				.setParameter("groupNum", refillScheduleIn.getGroupNum() )
				.setParameter("pharmOrder", refillScheduleIn.getPharmOrder())
				.setParameter("docType", DocType.Sfi)
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public DispOrder getOriginalDispOrderByRefillCouponNum(RefillSchedule refillSchedule) {
		List<RefillSchedule> initRefillScheduleList = em.createQuery(
				"select o from RefillSchedule o" + // 20150420 index check : RefillSchedule.trimRefillCouponNum : I_REFILL_SCHEDULE_02
				" where o.trimRefillCouponNum = :trimRefillCouponNum" +
				" and o.groupNum = :groupNum"+
				" and o.pharmOrder = :pharmOrder"+
				" and o.refillNum = 1"+
				" and o.docType = :docType")
				.setParameter("trimRefillCouponNum", refillSchedule.getTrimRefillCouponNum())
				.setParameter("groupNum", refillSchedule.getGroupNum())
				.setParameter("pharmOrder", refillSchedule.getPharmOrder())
				.setParameter("docType", DocType.Sfi)
				.getResultList();
		
		DispOrder initDispOrder = null;
		
		if( initRefillScheduleList != null && !initRefillScheduleList.isEmpty() ){
			initDispOrder = initRefillScheduleList.get(0).getDispOrder();
			initDispOrder.getDispOrderItemList();
		}
		return initDispOrder;
	}
	
	@SuppressWarnings("unchecked")
	private List getSfiRefillScheduleItemList(List<Long> pharmOrderItemIdList, boolean excludeCurrRefill){
		if( pharmOrderItemIdList.isEmpty() ){
			return null;
		}
		StringBuilder queryString = new StringBuilder(
				"select o.pharmOrderItem.orgItemNum, sum(o.refillQty) from RefillScheduleItem o" + // 20161128 index check : RefillScheduleItem.pharmOrderItemId : FK_REFILL_SCHEDULE_ITEM_01
				" where o.pharmOrderItem.id in :pharmOrderItemIdList" +
				" and o.refillSchedule.status <> :refillScheduleStatus"+
				" and o.refillSchedule.dispOrder.status not in :dispOrderStatusList");
		
		if( excludeCurrRefill ){
			queryString.append(" and o.refillSchedule.refillStatus <> :refillScheduleRefillStatus");
		}
		queryString.append(" group by o.pharmOrderItem.orgItemNum");
		
		Query query = em.createQuery(queryString.toString())
						.setParameter("pharmOrderItemIdList", pharmOrderItemIdList)
						.setParameter("refillScheduleStatus", RefillScheduleStatus.NotYetDispensed)
						.setParameter("dispOrderStatusList", DispOrderStatus.Deleted_SysDeleted);
		
		if( excludeCurrRefill ){
			query.setParameter("refillScheduleRefillStatus", RefillScheduleRefillStatus.Current);
		}
		
		return query.getResultList();
	}
	
	
	@SuppressWarnings("unchecked")
	public Map<Integer, Boolean> getPrintRefillAuxLabelFlagMap( List<Long> currRefillPoiList, List<Long> nextRefillPoiList, RefillSchedule refillSchedule){
		Map<Integer, Boolean> printRefillAuxLabelFlagMap = new HashMap<Integer, Boolean>();

		List currDispQtyList = getSfiRefillScheduleItemList(currRefillPoiList, true);
		List nextDispQtyList = getSfiRefillScheduleItemList(nextRefillPoiList, false);
		
		List sumDispQtyList = new ArrayList();
		
		if( currDispQtyList != null ){
			sumDispQtyList.addAll(currDispQtyList);
		}
		
		if( nextDispQtyList != null ){
			sumDispQtyList.addAll(nextDispQtyList);
		}
		
		DispOrder initDispOrder = null;
		
		for (Object object : sumDispQtyList) {
			Boolean printRefillAuxLabelFlag = Boolean.FALSE;
			if( Long.valueOf(0).equals(((Long)Array.get(object, 1))) ){
				if( initDispOrder == null ){
					initDispOrder = getOriginalDispOrderByRefillCouponNum(refillSchedule);
				}
				DispOrderItem initDispOrderItem = initDispOrder.getDispOrderItemByPoOrgItemNum((Integer)Array.get(object, 0));
				JaxbWrapper<DispLabel> labelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
				DispLabel dispLabel = labelJaxbWrapper.unmarshall(initDispOrderItem.getLabelXml());
				if( dispLabel.getStartDate() != null ){
					printRefillAuxLabelFlag = Boolean.TRUE;
				}
			}
			printRefillAuxLabelFlagMap.put((Integer)Array.get(object, 0), printRefillAuxLabelFlag);
		}

		return printRefillAuxLabelFlagMap;
	}
	
	
	public List<RefillSchedule> createSfiRefillScheduleList(OrderViewInfo orderViewInfo, DispOrder dispOrder, DispOrder prevDispOrder) 
	{
        List<RefillSchedule> sfiRefillList = new ArrayList<RefillSchedule>();  
        
		if (dispOrder.getRefillFlag()) 
		{
			List<RefillSchedule> rsList = this.getSfiRefillScheduleList(dispOrder);
			for (RefillSchedule rs : rsList) 
			{
				rs.loadChild();				
				// should always be one
				RefillSchedule nextRs = this.createSfiRefillScheduleByRefillSchedule(rs, dispOrder);
				if( nextRs != null ){
					nextRs.connect(dispOrder.getPharmOrder());
					sfiRefillList.add(nextRs);
				}
			}
		} 
		else
		{
			Map<Integer, List<DispOrderItem>> dispOrderItemMap = retrieveDispOrderItemListMapForSfiRefill(dispOrder.getDispOrderItemList());
			
			Boolean useMoItemNumFlag = Boolean.TRUE;
			
			if( prevDispOrder != null ){
				List<RefillSchedule> prevRefillScheduleList = this.getSfiRefillScheduleList(prevDispOrder);
				
				if( prevRefillScheduleList != null && prevRefillScheduleList.size() > 0 ){
					useMoItemNumFlag = prevRefillScheduleList.get(0).getUseMoItemNumFlag();
				}
			}
			
			if( dispOrderItemMap.values().size() > 0 ){
				if( orderViewInfo.getMedOrderNumForCoupon() == null && 
					dispOrder.getPharmOrder().getMedOrder().getDocType() == MedOrderDocType.Manual &&
					dispOrder.getPharmOrder().getMedOrder().getOrderNum() != null ){					
					String medOrderNumForCoupon = refillScheduleManager.retrieveManualMedOrderNumForCoupon(dispOrder.getPharmOrder());					
					orderViewInfo.setMedOrderNumForCoupon(medOrderNumForCoupon);
				}
			}
			
			for( List<DispOrderItem> dispOrderItemList : dispOrderItemMap.values() ){
				// current refill
				RefillSchedule rs = createInitSfiRefillSchedule(orderViewInfo, dispOrderItemList, useMoItemNumFlag);
				rs.connect(dispOrder);
				sfiRefillList.add(rs);
				
				// next refill
				RefillSchedule nrs = createNextSfiRefillSchedule(orderViewInfo, dispOrderItemList, useMoItemNumFlag);
				nrs.connect(dispOrder.getPharmOrder());
				sfiRefillList.add(nrs);
			}
		}
		
		return sfiRefillList;
	}
	
	private Map<Integer, List<DispOrderItem>> retrieveDispOrderItemListMapForSfiRefill(List<DispOrderItem> dispOrderItemList){
		Map<Integer, List<DispOrderItem>> dispOrderItemMap = new HashMap<Integer, List<DispOrderItem>>();
		Map<Integer, List<DispOrderItem>> doiWithZeroRemainQtyMap = new HashMap<Integer, List<DispOrderItem>>();
		
		for (DispOrderItem dispOrderItem : dispOrderItemList){
			MedOrderItem medOrderItem = dispOrderItem.getPharmOrderItem().getMedOrderItem();
			ActionStatus actionStatus = dispOrderItem.getPharmOrderItem().getActionStatus();
			boolean isSfi = false;
			
			if (dispOrderItem.getDispOrder().getPharmOrder().getMedOrder().getPrescType() != MedOrderPrescType.MpDischarge && 
					dispOrderItem.getDispOrder().getPharmOrder().getMedOrder().getPrescType() != MedOrderPrescType.MpHomeLeave ) {
				isSfi = (ActionStatus.PurchaseByPatient.equals(actionStatus) || ActionStatus.SafetyNet.equals(actionStatus));
			} else {
				isSfi = medOrderItem.isSfi();
			}

			if ( isSfi ) 
			{
				if( dispOrderItem.getPharmOrderItem().getRegimen().getDoseGroupList() != null && dispOrderItem.getPharmOrderItem().getRegimen().getDoseGroupList().size() > 1 ){
					continue;
				}

				if( dispOrderItem.getPharmOrderItem().getMedOrderItem().getCapdRxDrug() != null ){
					continue;
				}

				if( dispOrderItem.getPharmOrderItem().getRegimen().getDoseGroupList() != null && dispOrderItem.getPharmOrderItem().getRegimen().getDoseGroupList().size() > 1 ){
					continue;
				}

				if( dispOrderItemMap.containsKey(medOrderItem.getOrgItemNum()) ){
					List<DispOrderItem> sfiDoiList = dispOrderItemMap.get(medOrderItem.getOrgItemNum());
					sfiDoiList.add(dispOrderItem);
					dispOrderItemMap.put(medOrderItem.getOrgItemNum(), sfiDoiList);
				}else if (dispOrderItem.getPharmOrderItem().getIssueQty().compareTo(dispOrderItem.getDispQty()) > 0){
					List<DispOrderItem> sfiNewDoiList = new ArrayList<DispOrderItem>();
					sfiNewDoiList.add(dispOrderItem);
					
					if( doiWithZeroRemainQtyMap.containsKey(medOrderItem.getOrgItemNum()) ){
						sfiNewDoiList.addAll(doiWithZeroRemainQtyMap.get(medOrderItem.getOrgItemNum()));
						doiWithZeroRemainQtyMap.remove(medOrderItem.getOrgItemNum());
					}
					dispOrderItemMap.put(medOrderItem.getOrgItemNum(), sfiNewDoiList);
				}else if ( doiWithZeroRemainQtyMap.containsKey(medOrderItem.getOrgItemNum()) ) {
					List<DispOrderItem> sfiDoiList = doiWithZeroRemainQtyMap.get(medOrderItem.getOrgItemNum());
					sfiDoiList.add(dispOrderItem);
					doiWithZeroRemainQtyMap.put(medOrderItem.getOrgItemNum(), sfiDoiList);
				}else{
					List<DispOrderItem> sfiNewDoiList = new ArrayList<DispOrderItem>();
					sfiNewDoiList.add(dispOrderItem);
					doiWithZeroRemainQtyMap.put(medOrderItem.getOrgItemNum(), sfiNewDoiList);
				}
			}
		}
		return dispOrderItemMap;
	}
	
	private RefillSchedule createSfiRefillScheduleByRefillSchedule(RefillSchedule refillSchedule, DispOrder dispOrder){
		boolean createNextSfiRefill = false;
		
		for( RefillScheduleItem refillScehduleItem : refillSchedule.getRefillScheduleItemList() ){
			if (refillScehduleItem.getCalQty() > refillScehduleItem.getRefillQty()){
				createNextSfiRefill = true;
			}
		}
		
		if( !createNextSfiRefill ){
			return null;
		}
		
		PharmOrderItem pharmOrderItem = refillSchedule.getRefillScheduleItemList().get(0).getPharmOrderItem();
		String medOrderNumForCoupon = refillSchedule.getRefillCouponNum().substring(3, 15);
		RefillSchedule nextRs = createSfiRefillScheduleBase(pharmOrderItem, refillSchedule.getRefillNum()+1, medOrderNumForCoupon, refillSchedule.getUseMoItemNumFlag());
		nextRs.setPrintLang(refillSchedule.getPrintLang());
		boolean printFlag = true;
		
		for( RefillScheduleItem rsi : refillSchedule.getRefillScheduleItemList() ){
			Integer remainQty = rsi.getCalQty() - rsi.getRefillQty();
			
			RefillScheduleItem refillScheduleItem = createSfiRefillScheduleItemBase(rsi.getPharmOrderItem());
			refillScheduleItem.setCalQty(remainQty);
			refillScheduleItem.setRefillQty(remainQty);
			refillScheduleItem.setHandleExemptCode(rsi.getNextHandleExemptCode());
			refillScheduleItem.setHandleExemptUser(rsi.getHandleExemptUser());
			nextRs.addRefillScheduleItem(refillScheduleItem);
			
			DispOrderItem dispOrderItem = dispOrder.getDispOrderItemByPoOrgItemNum(rsi.getItemNum());
			if( dispOrderItem!= null && !dispOrderItem.getPrintFlag() ){
				printFlag = false;
			}
		}
		nextRs.setPrintFlag(printFlag);
		return nextRs;
	}
	
	private RefillSchedule createInitSfiRefillSchedule(OrderViewInfo orderViewInfo, List<DispOrderItem> dispOrderItemList, Boolean useMoItemNumFlag){
		PharmOrderItem pharmOrderItem = dispOrderItemList.get(0).getPharmOrderItem();
		DispOrder dispOrder = dispOrderItemList.get(0).getDispOrder();
		
		RefillSchedule initRs = createSfiRefillScheduleBase(pharmOrderItem, 1, orderViewInfo.getMedOrderNumForCoupon(), useMoItemNumFlag);
		initRs.setBeforeAdjRefillDate(dispOrder.getDispDate());
		initRs.setRefillDate(dispOrder.getDispDate());
		initRs.setRefillStatus(RefillScheduleRefillStatus.Current);
		initRs.setStatus(RefillScheduleStatus.BeingProcessed);
		if( DispOrderStatus.Issued == dispOrder.getStatus() ){
			initRs.setStatus(RefillScheduleStatus.Dispensed);
		}
		initRs.setTicket(dispOrder.getTicket());
		initRs.setPrintFlag(Boolean.FALSE);
		initRs.setPrintLang(orderViewInfo.getPrintLang());
		initRs.setPrintReminderFlag(orderViewInfo.getRefillPrintReminderFlag());
		
		for( DispOrderItem dispOrderItem : dispOrderItemList ){
			RefillScheduleItem initRsi = createSfiRefillScheduleItemBase(dispOrderItem.getPharmOrderItem());
			initRsi.setCalQty(dispOrderItem.getPharmOrderItem().getIssueQty().intValue());
			initRsi.setRefillQty(dispOrderItem.getDispQty().intValue());
			
			PharmOrderItem initPoi = dispOrderItem.getPharmOrderItem();
			if( initPoi.getHandleExemptCode() != null || initPoi.getNextHandleExemptCode() != null){
				initRsi.setHandleExemptCode(initPoi.getHandleExemptCode());
				initRsi.setNextHandleExemptCode(initPoi.getNextHandleExemptCode());
				initRsi.setHandleExemptUser(initPoi.getHandleExemptUser());
			}
			initRs.addRefillScheduleItem(initRsi);
		}
		
		return initRs;
	}
	
	private RefillSchedule createNextSfiRefillSchedule(OrderViewInfo orderViewInfo, List<DispOrderItem> dispOrderItemList, Boolean useMoItemNumFlag){
		RefillSchedule refillSchedule = null;
		boolean printFlag = true;
		
		for( DispOrderItem dispOrderItem : dispOrderItemList ){
			PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
			
			if( refillSchedule == null ){
				refillSchedule = createSfiRefillScheduleBase(pharmOrderItem,2, orderViewInfo.getMedOrderNumForCoupon(), useMoItemNumFlag);
				refillSchedule.setPrintLang(orderViewInfo.getPrintLang());
			}
						
			Integer remainQty = pharmOrderItem.getIssueQty().subtract(dispOrderItem.getDispQty()).intValue();		
			
			RefillScheduleItem refillScheduleItem = createSfiRefillScheduleItemBase(pharmOrderItem);
			refillScheduleItem.setCalQty(remainQty);
			refillScheduleItem.setRefillQty(remainQty);
			refillScheduleItem.setHandleExemptCode(pharmOrderItem.getNextHandleExemptCode());
			refillSchedule.addRefillScheduleItem(refillScheduleItem);
			
			if( !dispOrderItem.getPrintFlag() ){
				if( pharmOrderItem.getPharmOrder().getPrivateFlag()){
					if( pharmOrderItem.isDirtyFlag() ){
						printFlag = false;
					}
				}else{
					printFlag = false;
				}
			}
		}
		refillSchedule.setPrintFlag(printFlag);
		return refillSchedule;
	}
	
	private RefillSchedule createSfiRefillScheduleBase(PharmOrderItem pharmOrderItem, Integer refillNum, String medOrderNumIn, Boolean useMoItemNumFlag){
		RefillSchedule refillSchedule = new RefillSchedule();		
		Integer refillDuration = pharmOrderItem.getRegimen().getDurationInDay() + 28;
		
		if( useMoItemNumFlag ){
			refillSchedule.setGroupNum(pharmOrderItem.getMedOrderItem().getItemNum());
		}else{
			refillSchedule.setGroupNum(pharmOrderItem.getMedOrderItem().getOrgItemNum());
		}
		refillSchedule.setRefillNum(refillNum);
		refillSchedule.setUseMoItemNumFlag(useMoItemNumFlag);
		
		DateMidnight refillDate = new DateMidnight(pharmOrderItem.getPharmOrder().getMedOrder().getOrderDate());
		refillDate = refillDate.plusDays(refillDuration);
		
		refillSchedule.setBeforeAdjRefillDate(refillDate.toDate());
		refillSchedule.setRefillDate(refillDate.toDate());
				
		refillSchedule.setDocType(DocType.Sfi);
		refillSchedule.setStatus(RefillScheduleStatus.NotYetDispensed);
		refillSchedule.setRefillStatus(RefillScheduleRefillStatus.Next);
		refillSchedule.setWorkstore(pharmOrderItem.getPharmOrder().getWorkstore());
		
		MedOrder medOrder = pharmOrderItem.getMedOrderItem().getMedOrder();
		
		String medOrderNumForCoupon = medOrderNumIn;
		
		if( medOrderNumIn == null && MedOrderDocType.Normal == medOrder.getDocType() ){
			medOrderNumForCoupon = medOrder.getOrderNum();
		}
		
		String sfiRefillCoupon = generateSfiRefillCouponNum(medOrder.getPatHospCode(),
															workstore.getHospCode(),
															medOrderNumForCoupon,
															refillSchedule.getRefillNum(),
															refillSchedule.getGroupNum());
		
		refillSchedule.setRefillCouponNum(sfiRefillCoupon);
		if( medOrderNumForCoupon != null ){
			refillSchedule.setTrimRefillCouponNum(sfiRefillCoupon.substring(0, 15));
			refillSchedule.setMoOrgItemNum(pharmOrderItem.getMedOrderItem().getOrgItemNum());
		}else{
			refillSchedule.setTrimRefillCouponNum(sfiRefillCoupon);
			refillSchedule.setMoOrgItemNum(pharmOrderItem.getMedOrderItem().getOrgItemNum());
		}
		
		return refillSchedule;
	}
	
	private RefillScheduleItem createSfiRefillScheduleItemBase(PharmOrderItem pharmOrderItem){
		Integer refillDuration = pharmOrderItem.getRegimen().getDurationInDay() + 28;
		
		RefillScheduleItem refillScheduleItem = new RefillScheduleItem();
		refillScheduleItem.setRefillDuration(refillDuration);
		refillScheduleItem.setSupplyForDay(refillDuration);
		refillScheduleItem.setAdjQty(0);
		refillScheduleItem.setItemNum(pharmOrderItem.getOrgItemNum());
		refillScheduleItem.setWorkstore(pharmOrderItem.getPharmOrder().getWorkstore());
		return refillScheduleItem;
	}
	
	public static String generateSfiRefillCouponNum(String patHospCode, String dispHospCode, String medOrderNum, Integer refillNum, Integer itemNum ){
		StringBuilder refillCouponNum = new StringBuilder();
		refillCouponNum.append(patHospCode);
		if ( patHospCode.length() == 2 ){
			refillCouponNum.append(" ");
		}
		refillCouponNum.append(dispHospCode);
		if ( dispHospCode.length() == 2 ){
			refillCouponNum.append(" ");
		}
		if( medOrderNum != null && medOrderNum.length() > 3 ){
			refillCouponNum.append(medOrderNum.substring(3));
		}
		
		if( itemNum < 10 ){
			refillCouponNum.append("0");
		}
		refillCouponNum.append(itemNum);
		
		if( refillNum < 10 ){
			refillCouponNum.append("0");
		}
		refillCouponNum.append(refillNum);
		
		return refillCouponNum.toString();
	}

	public void cancelDispenseSfiRefillSchedule(List<RefillSchedule> sfiRefillScheduleList){
		for(RefillSchedule prevDispRs: sfiRefillScheduleList ){
			restoreSfiRefillScheduleListStatus(prevDispRs);
		}
	}
		
	private void restoreSfiRefillScheduleListStatus(RefillSchedule refillSchedule){
		Integer currRefillNum = refillSchedule.getRefillNum();
		PrintLang prevPrintLang = refillSchedule.getPrintLang();
		List<RefillSchedule> rsList = getSfiRefillScheduleListByRefillScheduleGroupNum(refillSchedule);			
		RefillSchedule prevRefillSchedule = null;
		
		if( RefillScheduleRefillStatus.Current.equals(refillSchedule.getRefillStatus()) ){			
			for( RefillSchedule rs : rsList ){
				if( rs.getRefillNum().equals(currRefillNum-1) ){
					rs.setRefillStatus(RefillScheduleRefillStatus.Current);
					prevRefillSchedule = rs;
					prevPrintLang = rs.getPrintLang();
					em.merge(rs);
				}else if( RefillScheduleRefillStatus.Next.equals(rs.getRefillStatus()) ) {
					em.remove(rs);					
				}
			}
		}
		
		refillSchedule.setRefillStatus(RefillScheduleRefillStatus.Next);
		refillSchedule.setStatus(RefillScheduleStatus.NotYetDispensed);
		refillSchedule.setTicket(null);
		refillSchedule.setDispOrder(null);
		refillSchedule.setPrintLang(prevPrintLang);
		refillSchedule.setPrintReminderFlag(Boolean.FALSE);
		
		Map<Integer, String> prevRsiExemptCodeMap = new HashMap<Integer, String>();
		if( prevRefillSchedule != null){
			for( RefillScheduleItem prevRsi : prevRefillSchedule.getRefillScheduleItemList() ){
				if( !StringUtils.isBlank(prevRsi.getNextHandleExemptCode()) ){
					prevRsiExemptCodeMap.put(prevRsi.getItemNum(), prevRsi.getNextHandleExemptCode());
				}
			}
		}

		for( RefillScheduleItem refillScheduleItem : refillSchedule.getRefillScheduleItemList() ){
			refillScheduleItem.setRefillQty(refillScheduleItem.getCalQty());
			refillScheduleItem.setHandleExemptCode(null);
			if( prevRsiExemptCodeMap.containsKey(refillScheduleItem.getItemNum()) ){
				refillScheduleItem.setHandleExemptCode(prevRsiExemptCodeMap.get(refillScheduleItem.getItemNum()));
			}
			refillScheduleItem.setNextHandleExemptCode(null);
			refillScheduleItem.setHandleExemptUser(null);
		}
		
		em.merge(refillSchedule);
		em.flush();
	}
		
	public void updateSfiRefillScheduleListStatus(OrderViewInfo orderViewInfoIn, RefillSchedule refillSchedule){
		
		Integer currRefillNum = refillSchedule.getRefillNum();
				
		List<RefillSchedule> rsList = getSfiRefillScheduleListByRefillScheduleGroupNum(refillSchedule);
		
		if( RefillScheduleRefillStatus.Current.equals(refillSchedule.getRefillStatus()) ){
			//Saved - find and remove the next record
			
			for( RefillSchedule rs : rsList ){
				if( RefillScheduleRefillStatus.Next.equals(rs.getRefillStatus()) ) {
					em.remove(rs);
					break;
				}
			}
			
		}else if( RefillScheduleRefillStatus.Next.equals(refillSchedule.getRefillStatus()) ){
			// New - Update Status			
			for( RefillSchedule rs : rsList ){	
				if( !rs.getRefillNum().equals( currRefillNum ) ){
					rs.setRefillStatus(RefillScheduleRefillStatus.None);
					em.merge(rs);
				}
			}
		}
		refillSchedule.setRefillStatus(RefillScheduleRefillStatus.Current);
		DispOrder dispOrder = refillSchedule.getDispOrder();
		refillSchedule.setStatus(RefillScheduleStatus.BeingProcessed);
		if( DispOrderStatus.Issued == dispOrder.getStatus() ){
			refillSchedule.setStatus(RefillScheduleStatus.Dispensed);
		}
		refillSchedule.setTicket(dispOrder.getTicket());
		refillSchedule.setPrintLang(dispOrder.getPrintLang());
		refillSchedule.setPrintReminderFlag(orderViewInfoIn.getRefillPrintReminderFlag());
		em.merge(refillSchedule);
		
		em.flush();
	}
}
