package hk.org.ha.model.pms.udt.reftable.mp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum AomScheduleType implements StringValuedEnum {

	General("G", "Gerneal"),
	Pivas("P", "Pivas");
	
	private final String dataValue;
	private final String displayValue;
	
	AomScheduleType(final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}

	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}

	public static AomScheduleType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(AomScheduleType.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<AomScheduleType> {

		private static final long serialVersionUID = 1L;

		public Class<AomScheduleType> getEnumClass() {
			return AomScheduleType.class;
		}
	}
}
