package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.PharmRemarkTemplate;

import java.util.Collection;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("pharmRemarkTemplateListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PharmRemarkTemplateListServiceBean implements PharmRemarkTemplateListServiceLocal {

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<PharmRemarkTemplate> pharmRemarkTemplateList;
	
	@In
	private Workstore workstore;
	
	@SuppressWarnings("unchecked")
	public void retrievePharmRemarkTemplateList(){
		
		pharmRemarkTemplateList = em.createQuery(
				"select o from PharmRemarkTemplate o" + // 20120303 index check : PharmRemarkTemplate.workstore : FK_PHARM_REMARK_TEMPLATE_01
				" where o.workstore = :workstore" +
				" order by o.sortSeq")
				.setParameter("workstore", workstore)
				.getResultList();
	}
	
	public void updatePharmRemarkTemplateList(Collection<PharmRemarkTemplate> newPharmRemarkTemplateList){		
	    int sortSeq = 1;
	    workstore = em.find(Workstore.class, workstore.getId());
	    
	    for (PharmRemarkTemplate pharmRemarkTemplate: newPharmRemarkTemplateList) {	    	
    		if(pharmRemarkTemplate.getId()==null && !pharmRemarkTemplate.isMarkDelete()){
    			pharmRemarkTemplate.setSortSeq(Integer.valueOf(sortSeq ++));
    			pharmRemarkTemplate.setWorkstore(workstore);
    			em.persist(pharmRemarkTemplate);
    		}else{
    			if(pharmRemarkTemplate.isMarkDelete()){
    				if( StringUtils.isBlank(pharmRemarkTemplate.getTemplate()) ){
    					pharmRemarkTemplate.setTemplate("*");
    				}
    				em.remove(em.merge(pharmRemarkTemplate));
    			}else{
    				pharmRemarkTemplate.setSortSeq(Integer.valueOf(sortSeq ++));
    				pharmRemarkTemplate.setWorkstore(workstore);
	    			em.merge(pharmRemarkTemplate);
    			}
    		}	
	    }
	    em.flush();
	}
	
	@Remove
	public void destroy() {
		if (pharmRemarkTemplateList != null) {
			pharmRemarkTemplateList = null;
		}
	}

	
	
}
