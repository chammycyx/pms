package hk.org.ha.model.pms.biz.inbox;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.medprofile.MedProfileManagerLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.udt.medprofile.FindMedProfileListResult;
import hk.org.ha.model.pms.udt.medprofile.InactiveType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileType;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("medProfileService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MedProfileServiceBean implements MedProfileServiceLocal {
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private MedProfileManagerLocal medProfileManager;
	
	@In
	private PharmInboxClientNotifierInf pharmInboxClientNotifier;
	
	@In
	private Workstore workstore;
	
	@Out(required = false)
	private List<MedProfile> medProfileList;
	
    public MedProfileServiceBean() {
    }
    
	@Override
    public FindMedProfileListResult findMedProfileList(String hkidCaseNum)
    {
		Date now = new Date();
    	medProfileList = retrieveMedProfileList(hkidCaseNum);
    	if (medProfileList.size() <= 0) {
    		return FindMedProfileListResult.NoProfileFound;
    	}
    	boolean hasAnyManualProfile = hasAnyManualProfile(medProfileList);
    	filterDuplicateMedProfiles(medProfileList);
    	filterExpiredMedProfiles(now, medProfileList);
    	medProfileManager.refreshMedProfileList(medProfileList, true, false);
    	filterDuplicateMedProfiles(medProfileList);
    	filterExpiredMedProfiles(now, medProfileList);    	
    	pharmInboxClientNotifier.dispatchUpdateEvent(workstore.getHospCode());
    	removeUnnecessaryLinkage(medProfileList);
    	return hasAnyManualProfile ? FindMedProfileListResult.Normal : FindMedProfileListResult.NoActiveManualProfileFound;
    }
	
    @SuppressWarnings("unchecked")
    private List<MedProfile> retrieveMedProfileList(String hkidCaseNum) {

    	String patKey = null;
    	
    	if (MedProfileUtils.isValidCaseNum(hkidCaseNum)) {
    		Patient patient = medProfileManager.retrievePasPatient(workstore.getDefPatHospCode(), hkidCaseNum);
    		patKey = patient == null ? null : patient.getPatKey();
    	}
    	
    	return em.createQuery(
    			"select p from MedProfile p" + // 20150206 index check : MedCase.caseNum, Patient.hkid : I_MED_CASE_01 I_PATIENT_02
    			" where p.hospCode = :hospCode" +
    			" and (p.medCase.caseNum = :hkidCaseNum" + 
    			"  or p.patient.hkid = :hkidCaseNum" +
    			"  or (p.patient.patKey = :patKey and p.type = :chemoType))" +
    			" and (p.status <> :status" +
    			"  or p.inactiveType is null" +
    			"  or p.inactiveType <> :type)" +
    			" order by p.id")
    			.setParameter("hospCode", workstore.getHospCode())
    			.setParameter("hkidCaseNum", hkidCaseNum)
    			.setParameter("patKey", patKey)
    			.setParameter("chemoType", MedProfileType.Chemo)
    			.setParameter("status", MedProfileStatus.Inactive)
    			.setParameter("type", InactiveType.Deleted)
				.setHint(QueryHints.FETCH, "p.medCase")
				.setHint(QueryHints.FETCH, "p.patient")
    			.getResultList();
    	
    }
    
    private void filterDuplicateMedProfiles(List<MedProfile> medProfileList) {

    	Set<String> patKeySet = new HashSet<String>();
    	
    	Iterator<MedProfile> iter = medProfileList.iterator();
    	while (iter.hasNext()) {
    		MedProfile medProfile = iter.next();
    		if (medProfile.getType() != MedProfileType.Chemo) {
    			continue;
    		}
    		
    		if (patKeySet.contains(medProfile.getPatient().getPatKey())) {
    			iter.remove();
    		} else {
    			patKeySet.add(medProfile.getPatient().getPatKey());
    		}
    	}
    }
    
    private void filterExpiredMedProfiles(Date now, List<MedProfile> medProfileList) {

    	Calendar calFrom = Calendar.getInstance();
    	calFrom.setTime(now);
    	calFrom.add(Calendar.WEEK_OF_MONTH, -4);
    	Date dateFrom = calFrom.getTime();
    	
    	Iterator<MedProfile> iter = medProfileList.iterator();
    	while (iter.hasNext()) {
    		MedProfile medProfile = iter.next();
    		if (medProfile.getType() != MedProfileType.Chemo && 
    			((medProfile.isDischarge() && MedProfileUtils.compare(dateFrom, medProfile.getDischargeDate()) > 0) ||
    			(medProfile.isCancelAdmission() && MedProfileUtils.compare(dateFrom, medProfile.getCancelAdmissionDate()) > 0))) {
    			iter.remove();
    		}
    	}
    }
    
    private boolean hasAnyManualProfile(List<MedProfile> medProfileList) {
    	
    	for (MedProfile medProfile: medProfileList) {
    		if (medProfile.isManualProfile()) {
    			return true;
    		}
    	}
    	return false;
    }
    
    private void removeUnnecessaryLinkage(List<MedProfile> medProfileList) {
    	
    	for (MedProfile medProfile: medProfileList) {
    		medProfile.clearMedProfileItemList();
    		medProfile.clearMedProfileStatList();
    	}
    }
    
	@Remove
	@Override
	public void destroy() {
		medProfileList = null;
	}	
}
