package hk.org.ha.model.pms.biz.vetting;

import javax.ejb.Local;

@Local
public interface VettingServiceLocal {

	void unvetOrder(Long medOrderId);

	void destroy();
}
