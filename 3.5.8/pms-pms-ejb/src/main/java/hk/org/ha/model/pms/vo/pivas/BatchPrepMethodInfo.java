package hk.org.ha.model.pms.vo.pivas;

import java.io.Serializable;
import java.math.BigDecimal;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class BatchPrepMethodInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private boolean coreFlag;
	
	private String itemCode;
	
	private String companyCode;
	
	private String companyName;
	
	private Boolean requireSolventFlag;
	
	private BigDecimal dosageQty;
	
	private String pivasDosageUnit;
	
	private BigDecimal afterVolume;
	
	private String diluentCode;
	
	private BigDecimal afterConcn;
	
	private BigDecimal volume;
	
	private BigDecimal diluentVolume;
	
	private boolean hqSuspend = false;
	
	private boolean suspend = false;
	
	private Long pivasFormulaMethodId;
	
	private String pivasDrugManufMethodDesc;

	public void setCoreFlag(boolean coreFlag) {
		this.coreFlag = coreFlag;
	}

	public boolean isCoreFlag() {
		return coreFlag;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	
	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public void setRequireSolventFlag(Boolean requireSolventFlag) {
		this.requireSolventFlag = requireSolventFlag;
	}

	public Boolean getRequireSolventFlag() {
		return requireSolventFlag;
	}

	public BigDecimal getDosageQty() {
		return dosageQty;
	}

	public void setDosageQty(BigDecimal dosageQty) {
		this.dosageQty = dosageQty;
	}

	public String getPivasDosageUnit() {
		return pivasDosageUnit;
	}

	public void setPivasDosageUnit(String pivasDosageUnit) {
		this.pivasDosageUnit = pivasDosageUnit;
	}

	public BigDecimal getAfterVolume() {
		return afterVolume;
	}

	public void setAfterVolume(BigDecimal afterVolume) {
		this.afterVolume = afterVolume;
	}

	public String getDiluentCode() {
		return diluentCode;
	}

	public void setDiluentCode(String diluentCode) {
		this.diluentCode = diluentCode;
	}

	public BigDecimal getAfterConcn() {
		return afterConcn;
	}

	public void setAfterConcn(BigDecimal afterConcn) {
		this.afterConcn = afterConcn;
	}

	public BigDecimal getVolume() {
		return volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	public BigDecimal getDiluentVolume() {
		return diluentVolume;
	}

	public void setDiluentVolume(BigDecimal diluentVolume) {
		this.diluentVolume = diluentVolume;
	}

	public void setHqSuspend(boolean hqSuspend) {
		this.hqSuspend = hqSuspend;
	}

	public boolean isHqSuspend() {
		return hqSuspend;
	}

	public void setSuspend(boolean suspend) {
		this.suspend = suspend;
	}

	public boolean isSuspend() {
		return suspend;
	}

	public void setPivasFormulaMethodId(Long pivasFormulaMethodId) {
		this.pivasFormulaMethodId = pivasFormulaMethodId;
	}

	public Long getPivasFormulaMethodId() {
		return pivasFormulaMethodId;
	}

	public String getPivasDrugManufMethodDesc() {
		return pivasDrugManufMethodDesc;
	}

	public void setPivasDrugManufMethodDesc(String pivasDrugManufMethodDesc) {
		this.pivasDrugManufMethodDesc = pivasDrugManufMethodDesc;
	}
}