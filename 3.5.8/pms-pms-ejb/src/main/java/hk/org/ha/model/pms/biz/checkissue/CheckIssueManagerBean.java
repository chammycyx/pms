package hk.org.ha.model.pms.biz.checkissue;

import static hk.org.ha.model.pms.prop.Prop.BATCH_DAYEND_ORDER_DURATION;
import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.reftable.OperationWorkstation;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderDayEndStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.checkissue.CheckIssueOrder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.exceptions.DatabaseException;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.joda.time.DateMidnight;

@AutoCreate
@Stateless
@Name("checkIssueManager")
@MeasureCalls
public class CheckIssueManagerBean implements CheckIssueManagerLocal {
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;

	private static final String CHECK_ISSUE_ORDER_CLASS_NAME = CheckIssueOrder.class.getName();
	
	private static final String SORT_BY_TICKET_NUM = "ticketNum";
	private static final String SORT_BY_CHECK_DATE = "checkDate";
	private static final int CREATE_DATE_BUFFER = 30;
	private DateFormat auditLogDf = new SimpleDateFormat("yyyy-MM-dd"); 
	
	@SuppressWarnings("unchecked")
	public Integer retrieveIssueWindowNum(Workstore workstore, Workstation workstation){ 
		Integer issueWindowNum = 0;
		List<OperationWorkstation> operationWorkstationList = em.createQuery(
				"select o from OperationWorkstation o " + // 20120214 index check : none
				" where o.operationProfile.id = o.operationProfile.workstore.operationProfileId " + 
				" and o.operationProfile.workstore = :workstore " +
				" and o.workstation = :workstation ")
				.setParameter("workstore", workstore)
				.setParameter("workstation", workstation)
				.getResultList();
		
		if ( !operationWorkstationList.isEmpty() ) { 
			issueWindowNum = operationWorkstationList.get(0).getIssueWindowNum();
		} else {
			issueWindowNum = 0;
		}
		
		return issueWindowNum;
	}
	
	@SuppressWarnings("unchecked")
	public List<CheckIssueOrder> retrieveCheckIssueOrderList(Workstore workstore, 
															Workstation workstation, 
															Date ticketDate, 
															DispOrderStatus status, 
															boolean retrieveCheckList, 
															boolean retrieveOtherCheckList, 
															String sortSeq){
		logger.debug("sortSeq:#0", sortSeq);

		StringBuilder sb = new StringBuilder(
				"select new " + CHECK_ISSUE_ORDER_CLASS_NAME + // 20160809 index check : DispOrder.workstore,orderType,ticketDate : I_DISP_ORDER_15
				"  (o.id, o.ticketDate, o.ticketNum," +
				"  o.issueWindowNum, o.urgentFlag, o.pharmOrder.medOrder.docType," +
				"  o.checkWorkstationCode)" +
				" from DispOrder o" +
				" where o.workstore = :workstore" +
				" and o.orderType = :orderType" +
				" and o.ticketDate = :ticketDate" +
				" and o.status = :status" +
				" and o.adminStatus = :dispOrderAdminStatus" +
				" and o.dayEndStatus = :dayEndStatus" + 
				" and o.batchProcessingFlag = :batchProcessingFlag");
		
		if ( retrieveCheckList ) {
			sb.append(" and o.checkWorkstationCode = :checkWorkstationCode");
		} else if ( retrieveOtherCheckList ) {
			sb.append(" and o.checkWorkstationCode <> :checkWorkstationCode");
		}
		
		String orderBy = getDispOrderListSorting(sortSeq);
		if ( StringUtils.isNotBlank(orderBy) ) {
			sb.append(orderBy);
		}
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("workstore", workstore);
		query.setParameter("orderType", OrderType.OutPatient);
		query.setParameter("ticketDate", ticketDate);
		query.setParameter("status", status);
		query.setParameter("dispOrderAdminStatus", DispOrderAdminStatus.Normal);
		query.setParameter("dayEndStatus", DispOrderDayEndStatus.None);
		query.setParameter("batchProcessingFlag", Boolean.FALSE);
		
		if ( retrieveCheckList || retrieveOtherCheckList ) {
			query.setParameter("checkWorkstationCode", workstation.getWorkstationCode());
		} 
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<CheckIssueOrder> retrieveIssuedList(Workstore workstore, Date ticketDate, String sortSeq){
		// since DispOrder table is partitioned by createDate, createDate range is applied to SQL to ensure only 1-2 
		// partition is searched for performance tuning
		// createDate range is set to 90 days + 30 days buffer
		Date createDateStart = new DateMidnight(ticketDate).minusDays(BATCH_DAYEND_ORDER_DURATION.get()).minusDays(CREATE_DATE_BUFFER).toDate();
		Date createDateEnd = new DateMidnight(ticketDate).toDateTime().plusDays(1).minus(1).toDate();
		
		StringBuilder sb = new StringBuilder(
									"select new " + CHECK_ISSUE_ORDER_CLASS_NAME + // 20120225 index check 
									"  (o.id, o.ticketDate, o.ticketNum," +
									"  o.issueWindowNum, o.urgentFlag, o.pharmOrder.medOrder.docType," +
									"  o.checkWorkstationCode)" +
									" from DispOrder o" +
									" where o.workstore = :workstore" +
									" and o.orderType = :orderType" +
									" and o.ticketDate = :ticketDate" +
									" and o.status = :status" +
									" and o.adminStatus = :dispOrderAdminStatus" +
									" and o.dayEndStatus = :dayEndStatus " +
									" and o.batchProcessingFlag = :batchProcessingFlag " +
									" and o.createDate between :createDateStart and :createDateEnd");
		
		String orderBy = getDispOrderListSorting(sortSeq);
		if ( StringUtils.isNotBlank(orderBy) ) {
			sb.append(orderBy);
		}
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("workstore", workstore);
		query.setParameter("orderType", OrderType.OutPatient);
		query.setParameter("ticketDate", ticketDate);
		query.setParameter("status", DispOrderStatus.Issued);
		query.setParameter("dispOrderAdminStatus", DispOrderAdminStatus.Normal);
		query.setParameter("dayEndStatus", DispOrderDayEndStatus.None);
		query.setParameter("batchProcessingFlag", Boolean.FALSE);
		query.setParameter("createDateStart", createDateStart);
		query.setParameter("createDateEnd", createDateEnd);
		return query.getResultList();
	}
	
	private String getDispOrderListSorting(String sortSeq){
		String orderBy = "";
		if ( StringUtils.isNotBlank(sortSeq) ) {
			String[] sortSeqArr = sortSeq.split("\\.");
			if ( SORT_BY_CHECK_DATE.equals(sortSeqArr[0]) ) {
				orderBy = " order by o.checkDate";
			} else if ( SORT_BY_TICKET_NUM.equals(sortSeqArr[0])){
				orderBy = " order by o.urgentFlag desc, o.ticketNum";
			}
			if ( sortSeqArr.length > 1 && orderBy.length()>0 ) {
				orderBy += " "+sortSeqArr[1];
			}
		}
		return orderBy;
	}
	
	@SuppressWarnings("unchecked")
	public Pair<List<DispOrder>, String> retrieveDispOrderByTicket(Date ticketDate, String ticketNum, Workstore workstore, boolean lockNoWait) {
		List<DispOrder> dispOrderList = new ArrayList<DispOrder>();
		String msgCode = "";
		try {
			StringBuilder sb = new StringBuilder(
					"select o from DispOrder o " + // 20160615 index check : DispOrder.workstore,orderType,ticketDate,ticketNum : I_DISP_ORDER_15
					"where o.workstore = :workstore " +
					"and o.ticketDate = :ticketDate " +
					"and o.ticketNum = :ticketNum " +
					"and o.status <> :dispOrderStatus " +
					"and o.orderType = :orderType ");
			
			Query query = em.createQuery(sb.toString());
			
			query.setParameter("workstore", workstore);
			query.setParameter("ticketDate", ticketDate, TemporalType.DATE);
			query.setParameter("ticketNum", ticketNum);
			query.setParameter("dispOrderStatus", DispOrderStatus.SysDeleted);
			query.setParameter("orderType", OrderType.OutPatient);
			
			if ( lockNoWait ) {
				query.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.LockNoWait);
			}
			dispOrderList = query.getResultList();;
		} catch (DatabaseException ex) {
			String message = ex.getMessage();
			if (message != null && message.toLowerCase().indexOf("nowait") != -1) {
				logger.info("Processing state cannot be release for DispOrder(ticketDate:#0, ticketNum:#1)," +
						" which is currently locked by another transaction.", auditLogDf.format(ticketDate), ticketNum);
				msgCode = "0307";
			} else {
				logger.error(ex, ex);
			}
		} 
		
		return new Pair(dispOrderList, msgCode);
	}
}
