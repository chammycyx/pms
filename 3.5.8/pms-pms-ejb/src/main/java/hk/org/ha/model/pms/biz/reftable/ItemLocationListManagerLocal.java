package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.persistence.reftable.ItemLocation;
import hk.org.ha.model.pms.udt.reftable.MachineType;
import hk.org.ha.model.pms.vo.reftable.ItemLocationInfo;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

@Local
public interface ItemLocationListManagerLocal {

	List<ItemLocationInfo> retrieveItemLocationListByItemCode(String itemCode);
	
	Map<String, ItemLocation> retrieveItemLocationListByItemCodeList(List<String> itemCodeList);
	
	List<ItemLocationInfo> retrieveItemLocationListByPickStation(int pickStationNum);
	
	Map<String, List<ItemLocation>> retrieveItemLocationListByItemCodeList(List<String> itemCodeList, MachineType machineType, List<Integer> pickStationsNumList);
	
	List<ItemLocation> retrieveItemLocationList(String itemCode, List<Integer> pickStationNumList, List<MachineType> machineTypeList);

	void destroy();
	
}
 