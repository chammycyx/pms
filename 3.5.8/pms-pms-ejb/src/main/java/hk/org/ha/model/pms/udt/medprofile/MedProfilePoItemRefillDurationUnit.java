package hk.org.ha.model.pms.udt.medprofile;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum MedProfilePoItemRefillDurationUnit implements StringValuedEnum {
	
	Day("D", "Day(s)"),
	Week("W", "Week(s)"),
	Month("M", "Month(s)"),
	Cycle("C", "Cycle(s)");
	
	private final String dataValue;
	private final String displayValue;
	
	private MedProfilePoItemRefillDurationUnit(String dataValue,
			String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}

	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}
	
	public static MedProfilePoItemRefillDurationUnit dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MedProfilePoItemRefillDurationUnit.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<MedProfilePoItemRefillDurationUnit> {
		
		private static final long serialVersionUID = 1L;

		public Class<MedProfilePoItemRefillDurationUnit> getEnumClass() {
			return MedProfilePoItemRefillDurationUnit.class;
		}
	}
}
