package hk.org.ha.model.pms.biz.inbox;
import javax.ejb.Local;

@Local
public interface ErrorStatServiceLocal {
	void setShowAllWards(Boolean showAllWards);
	void retrieveErrorStatList();
	void notifyMds(Long errorStatId, boolean notified);
	void destroy();
}
