@XmlJavaTypeAdapters({   
    @XmlJavaTypeAdapter(value=DateAdapter.class, type=Date.class)
})  
package hk.org.ha.model.pms.persistence;

import hk.org.ha.fmk.pms.eai.mail.DateAdapter;

import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;
   