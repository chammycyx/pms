package hk.org.ha.model.pms.persistence.reftable;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.udt.reftable.ChargeSpecialtyType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "CHARGE_SPECIALTY")
@Customizer(AuditCustomizer.class)
public class ChargeSpecialty extends VersionEntity {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "chargeSpecialtySeq")
	@SequenceGenerator(name = "chargeSpecialtySeq", sequenceName = "SQ_CHARGE_SPECIALTY", initialValue = 100000000)
	private Long id;
	
	@Column(name = "CHARGE_SPEC_CODE", nullable = false, length = 4)
	private String chargeSpecCode;
	
	@Column(name = "SPEC_CODE", nullable = false,  length = 4)
	private String specCode;

	@Converter(name = "ChargeSpecialty.type", converterClass = ChargeSpecialtyType.Converter.class)
    @Convert("ChargeSpecialty.type")
	@Column(name = "TYPE", nullable = false, length = 1)
	private ChargeSpecialtyType type;

	@ManyToOne
	@JoinColumn(name = "HOSP_CODE", nullable = false)
	private Hospital hospital;
	
	@Transient
	private boolean markDelete = false;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getChargeSpecCode() {
		return chargeSpecCode;
	}

	public void setChargeSpecCode(String chargeSpecCode) {
		this.chargeSpecCode = chargeSpecCode;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public ChargeSpecialtyType getType() {
		return type;
	}

	public void setType(ChargeSpecialtyType type) {
		this.type = type;
	}
	
	public Hospital getHospital() {
		return hospital;
	}
	
	public void setHospital(Hospital hospital) {
		this.hospital = hospital;
	}

	public boolean isMarkDelete() {
		return markDelete;
	}

	public void setMarkDelete(boolean markDelete) {
		this.markDelete = markDelete;
	}
}
