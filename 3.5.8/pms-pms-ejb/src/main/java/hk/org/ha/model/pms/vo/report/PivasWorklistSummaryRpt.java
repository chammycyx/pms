package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class PivasWorklistSummaryRpt implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Date pivasSupplyDate;
	
	private List<PivasWorklistSummaryRptWorklistDtl> pivasWorklistSummaryRptWorklistDtlList;
	
	private List<PivasWorklistSummaryRptRawMaterialDtl> pivasWorklistSummaryRptRawMaterialDtlList;
	
	public Date getPivasSupplyDate() {
		return pivasSupplyDate;
	}

	public void setPivasSupplyDate(Date pivasSupplyDate) {
		this.pivasSupplyDate = pivasSupplyDate;
	}

	public List<PivasWorklistSummaryRptWorklistDtl> getPivasWorklistSummaryRptWorklistDtlList() {
		return pivasWorklistSummaryRptWorklistDtlList;
	}

	public void setPivasWorklistSummaryRptWorklistDtlList(
			List<PivasWorklistSummaryRptWorklistDtl> pivasWorklistSummaryRptWorklistDtlList) {
		this.pivasWorklistSummaryRptWorklistDtlList = pivasWorklistSummaryRptWorklistDtlList;
	}

	public List<PivasWorklistSummaryRptRawMaterialDtl> getPivasWorklistSummaryRptRawMaterialDtlList() {
		return pivasWorklistSummaryRptRawMaterialDtlList;
	}

	public void setPivasWorklistSummaryRptRawMaterialDtlList(
			List<PivasWorklistSummaryRptRawMaterialDtl> pivasWorklistSummaryRptRawMaterialDtlList) {
		this.pivasWorklistSummaryRptRawMaterialDtlList = pivasWorklistSummaryRptRawMaterialDtlList;
	}

}
