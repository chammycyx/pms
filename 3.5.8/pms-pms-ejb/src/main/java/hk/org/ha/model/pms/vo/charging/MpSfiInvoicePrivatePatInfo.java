package hk.org.ha.model.pms.vo.charging;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class MpSfiInvoicePrivatePatInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String ward;
	
	private String bedNum;
	
	private String pasSpecCode;
	
	private String caseNum;
	
	private String patName;
	
	private String patNameChi;
	
	private Date lastInvoicePrintDate;
	
	private Long medProfileId;
	
	private Date dischargeDate;

	public String getWard() {
		return ward;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}

	public String getBedNum() {
		return bedNum;
	}

	public void setBedNum(String bedNum) {
		this.bedNum = bedNum;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getPatNameChi() {
		return patNameChi;
	}

	public void setPatNameChi(String patNameChi) {
		this.patNameChi = patNameChi;
	}

	public Date getLastInvoicePrintDate() {
		return lastInvoicePrintDate;
	}

	public void setLastInvoicePrintDate(Date lastInvoicePrintDate) {
		this.lastInvoicePrintDate = lastInvoicePrintDate;
	}

	public void setPasSpecCode(String pasSpecCode) {
		this.pasSpecCode = pasSpecCode;
	}

	public String getPasSpecCode() {
		return pasSpecCode;
	}

	public void setMedProfileId(Long medProfileId) {
		this.medProfileId = medProfileId;
	}

	public Long getMedProfileId() {
		return medProfileId;
	}

	public void setDischargeDate(Date dischargeDate) {
		this.dischargeDate = dischargeDate;
	}

	public Date getDischargeDate() {
		return dischargeDate;
	}
}
