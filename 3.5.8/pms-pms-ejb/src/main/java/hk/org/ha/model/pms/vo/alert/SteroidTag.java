package hk.org.ha.model.pms.vo.alert;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class SteroidTag implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement
	private String createUser;
	
	@XmlElement
	private Date createDate;
	
	@XmlElement
	private String patHospCode;
	
	@XmlElement
	private String orderNum;
	
	@XmlElement
	private Date expiryDate;
	
	@XmlElement
	private String indication;

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getIndication() {
		return indication;
	}

	public void setIndication(String indication) {
		this.indication = indication;
	}

}
