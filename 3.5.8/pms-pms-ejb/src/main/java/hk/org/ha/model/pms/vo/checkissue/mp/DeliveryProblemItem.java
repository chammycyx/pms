package hk.org.ha.model.pms.vo.checkissue.mp;

import hk.org.ha.model.pms.udt.medprofile.DeliveryItemStatus;

import java.io.Serializable;
import java.math.BigDecimal;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class DeliveryProblemItem implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Long deliveryId;
	
	private Long deliveryItemId;
	
	private String caseNum;
	
	private String wardCode;
	
	private String bedNum;
	
	private String itemCode;
	
	private BigDecimal issueQty;
	
	private String doseUnit;
	
	private String baseUnit;
	
	private String exception;
	
	private String drugName;
	
	private String formLabelDesc;
	
	private String strength;
	
	private String volumeText;
	
	private String itemDescTlf;
	
	private boolean markUnLink;
	
	private String patientName;
	
	private boolean markDelete;
	
	private boolean keepRecorded;
	
	private boolean dayendProcess;
	
	private String batchCode;
	
	private boolean havePatientChiName;
	
	private String mpSfiInvoiceNum;
	
	private DeliveryItemStatus deliveryItemStatus;
	
	public DeliveryProblemItem() {
		this.markUnLink = false;
		this.markDelete = false;
		this.keepRecorded = false;
		this.dayendProcess = false;
	}

	public Long getDeliveryItemId() {
		return deliveryItemId;
	}

	public void setDeliveryItemId(Long deliveryItemId) {
		this.deliveryItemId = deliveryItemId;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public BigDecimal getIssueQty() {
		return issueQty;
	}

	public void setIssueQty(BigDecimal issueQty) {
		this.issueQty = issueQty;
	}

	public String getDoseUnit() {
		return doseUnit;
	}

	public void setDoseUnit(String doseUnit) {
		this.doseUnit = doseUnit;
	}

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public String getDrugName() {
		return drugName;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public String getFormLabelDesc() {
		return formLabelDesc;
	}

	public void setFormLabelDesc(String formLabelDesc) {
		this.formLabelDesc = formLabelDesc;
	}

	public String getStrength() {
		return strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public String getVolumeText() {
		return volumeText;
	}

	public void setVolumeText(String volumeText) {
		this.volumeText = volumeText;
	}
	
	public String getItemDescTlf() {
		return itemDescTlf;
	}

	public void setItemDescTlf(String itemDescTlf) {
		this.itemDescTlf = itemDescTlf;
	}

	public boolean isMarkUnLink() {
		return markUnLink;
	}

	public void setMarkUnLink(boolean markUnLink) {
		this.markUnLink = markUnLink;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public boolean isMarkDelete() {
		return markDelete;
	}

	public void setMarkDelete(boolean markDelete) {
		this.markDelete = markDelete;
	}


	public boolean isDayendProcess() {
		return dayendProcess;
	}

	public void setDayendProcess(boolean dayendProcess) {
		this.dayendProcess = dayendProcess;
	}

	public boolean isKeepRecorded() {
		return keepRecorded;
	}

	public void setKeepRecorded(boolean keepRecorded) {
		this.keepRecorded = keepRecorded;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public Long getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(Long deliveryId) {
		this.deliveryId = deliveryId;
	}

	public String getBatchCode() {
		return batchCode;
	}

	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}

	public void setHavePatientChiName(boolean havePatientChiName) {
		this.havePatientChiName = havePatientChiName;
	}

	public boolean isHavePatientChiName() {
		return havePatientChiName;
	}

	public void setBedNum(String bedNum) {
		this.bedNum = bedNum;
	}

	public String getBedNum() {
		return bedNum;
	}

	public void setMpSfiInvoiceNum(String mpSfiInvoiceNum) {
		this.mpSfiInvoiceNum = mpSfiInvoiceNum;
	}

	public String getMpSfiInvoiceNum() {
		return mpSfiInvoiceNum;
	}

	public void setDeliveryItemStatus(DeliveryItemStatus deliveryItemStatus) {
		this.deliveryItemStatus = deliveryItemStatus;
	}

	public DeliveryItemStatus getDeliveryItemStatus() {
		return deliveryItemStatus;
	}
}