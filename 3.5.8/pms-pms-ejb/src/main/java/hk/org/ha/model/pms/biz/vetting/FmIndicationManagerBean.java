package hk.org.ha.model.pms.biz.vetting;

import static hk.org.ha.model.pms.prop.Prop.PATIENT_MOE_ENABLED;
import hk.org.ha.model.pms.asa.exception.moe.MoeException;
import hk.org.ha.model.pms.dms.vo.FmDrug;
import hk.org.ha.model.pms.dms.vo.FmDrugCorpInd;
import hk.org.ha.model.pms.dms.vo.PmsFmStatusSearchCriteria;
import hk.org.ha.model.pms.persistence.FmEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedOrderFm;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItemFm;
import hk.org.ha.model.pms.udt.rx.RxItemType;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.udt.vetting.FmStatusOption;
import hk.org.ha.model.pms.vo.rx.IvAdditive;
import hk.org.ha.model.pms.vo.rx.IvRxDrug;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.vetting.FmIndication;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;
import hk.org.ha.service.pms.asa.interfaces.ipmoe.IpmoeServiceJmsRemote;
import hk.org.ha.service.pms.asa.interfaces.moe.MoeServiceJmsRemote;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.log.Log;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("fmIndicationManager")
public class FmIndicationManagerBean implements FmIndicationManagerLocal {
	
	private static final String OTHERS = "OTHERS";
	
	@Logger
	private Log logger;
	
	@In
	private Workstore workstore;

	@In
	private MoeServiceJmsRemote moeServiceProxy;

	@In
	private IpmoeServiceJmsRemote ipmoeServiceProxy;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In(required = false)
	private PharmOrder pharmOrder;
	
	public MedOrderFm retrieveMedOrderFmByRxDrug(RxDrug rxDrug) throws MoeException {
		if ( ! PATIENT_MOE_ENABLED.get() ) {
			return null;
		}
		
		String itemCode = "";
		if (rxDrug.getStrengthCompulsory()) {
			itemCode = rxDrug.getItemCode();
		}
		
		MedOrderFm moeFm = moeServiceProxy.retrieveFormularyMgmt( pharmOrder.getMedOrder().getPatHospCode(), 
				pharmOrder.getMedOrder().getPatient().getPatKey(),
				itemCode,
				rxDrug.getDisplayName(),
				rxDrug.getFormCode(),
				rxDrug.getSaltProperty(),
				rxDrug.getFmStatus() );
		
		if (moeFm != null) {
			moeFm.postLoad();
		}
		
		return moeFm;
	}	

	public MedProfileMoItemFm retrieveMedProfileMoItemFmByRxDrug(MedProfile medProfile, RxDrug rxDrug) throws MoeException {
		if ( ! PATIENT_MOE_ENABLED.get() ) {
			return null;
		}
		
		String itemCode = "";
		if (rxDrug.getStrengthCompulsory()) {
			itemCode = rxDrug.getItemCode();
		}
		
		MedProfileMoItemFm moeFm = ipmoeServiceProxy.retrieveMpFormularyMgmt( medProfile.getPatHospCode(), 
				medProfile.getPatient().getPatKey(),
				itemCode,
				rxDrug.getDisplayName(),
				rxDrug.getFormCode(),
				rxDrug.getSaltProperty(),
				rxDrug.getFmStatus() );
		
		if (moeFm != null) {
			moeFm.postLoad();
		}
		
		return moeFm;
	}	
	
	public List<FmIndication> retrieveLocalFmIndicationListByMedOrderItem(MedOrderItem medOrderItem, List<MedOrderFm> medOrderFmList) {
		List<FmIndication> fmIndicationList = new ArrayList<FmIndication>();
	
		MedOrderFm foundMedOrderFm = null;
		
		switch (medOrderItem.getRxItemType()) {
			case Oral:
			case Injection:
				foundMedOrderFm = retrieveLocalMedOrderFmByRxDrug(medOrderItem.getRxDrug(), medOrderFmList);
				if (foundMedOrderFm != null) {
					FmIndication fmIndication = new FmIndication();
					fmIndication.setRxDrug(medOrderItem.getRxDrug());
					fmIndication.setFmEntity(foundMedOrderFm);
					fmIndicationList.add(fmIndication);
				}
				break;
				
			case Iv:
				for (IvAdditive ivAdditive : medOrderItem.getIvRxDrug().getIvAdditiveList()) {
					foundMedOrderFm = retrieveLocalMedOrderFmByRxDrug(ivAdditive, medOrderFmList);
					if (foundMedOrderFm != null) {
						FmIndication fmIndication = new FmIndication();
						fmIndication.setRxDrug(ivAdditive);
						fmIndication.setFmEntity(foundMedOrderFm);
						fmIndicationList.add(fmIndication);
					}
				}
				break;
		}
		
		return fmIndicationList;
	}
	
	public MedOrderFm retrieveLocalMedOrderFmByRxDrug(RxDrug rxDrug, List<MedOrderFm> medOrderFmList) {
		for (MedOrderFm medOrderFm : medOrderFmList) {
			if (rxDrug.getStrengthCompulsory() == null) {
				Long tempMedOrderId = null;
				if (medOrderFm.getMedOrder() != null) {
					tempMedOrderId = medOrderFm.getMedOrder().getId();
				}
				throw new UnsupportedOperationException("Strength compulsory is null in RxDrug, medOrderFm id = " + medOrderFm.getId() + ", medOrder id = " + tempMedOrderId +
														", displayName = " + rxDrug.getDisplayName() + ", formCode = " + rxDrug.getFormCode() + ", saltProperty = " + rxDrug.getSaltProperty() + ", fmStatus = " + rxDrug.getFmStatus()); 
			}
			
			if (rxDrug.getStrengthCompulsory()) {
				if ( StringUtils.equals(medOrderFm.getDisplayName(), rxDrug.getDisplayName()) &&
						StringUtils.equals(medOrderFm.getFormCode(), rxDrug.getFormCode()) &&
						StringUtils.equals(medOrderFm.getSaltProperty(), rxDrug.getSaltProperty()) && 
						medOrderFm.getFmStatus() == FmStatus.dataValueOf(rxDrug.getFmStatus()) &&
						StringUtils.equals(medOrderFm.getItemCode(), rxDrug.getItemCode()) ) {
					return medOrderFm;
				}
			} else {
				if ( StringUtils.equals(medOrderFm.getDisplayName(), rxDrug.getDisplayName()) &&
						StringUtils.equals(medOrderFm.getFormCode(), rxDrug.getFormCode()) &&
						StringUtils.equals(medOrderFm.getSaltProperty(), rxDrug.getSaltProperty()) &&
						medOrderFm.getFmStatus() == FmStatus.dataValueOf(rxDrug.getFmStatus()) ) {
					return medOrderFm;
				}
			}
		}
		
		return null;
	}
	
	public List<FmIndication> retrieveLocalFmIndicationListForDrugOnHand(MedOrderItem medOrderItem, List<MedOrderFm> medOrderFmList) {
		if (medOrderItem.getRxItemType() == RxItemType.Capd || FmStatus.dataValueOf(medOrderItem.getFmStatus()) == FmStatus.FreeTextEntryItem) {
			return new ArrayList<FmIndication>();
		}

		return retrieveLocalFmIndicationListByMedOrderItem(medOrderItem, medOrderFmList);
	}
	
	public List<FmIndication> retrieveLocalFmIndicationListByMedProfileMoItem(MedProfileMoItem medProfileMoItem, List<MedProfileMoItemFm> medProfileMoItemFmList) {
		List<FmIndication> fmIndicationList = new ArrayList<FmIndication>();
	
		MedProfileMoItemFm foundMedProfileMoItemFm = null;
		
		switch (medProfileMoItem.getRxItemType()) {
			case Oral:
			case Injection:
				RxDrug rxDrug = (RxDrug)medProfileMoItem.getRxItem();
				foundMedProfileMoItemFm = retrieveLocalMedProfileMoItemFmByRxDrug(rxDrug, medProfileMoItemFmList);
				if (foundMedProfileMoItemFm != null) {
					FmIndication fmIndication = new FmIndication();
					fmIndication.setRxDrug(rxDrug);
					fmIndication.setFmEntity(foundMedProfileMoItemFm);
					fmIndicationList.add(fmIndication);
				}
				break;
				
			case Iv:
				IvRxDrug ivRxDrug = (IvRxDrug)medProfileMoItem.getRxItem();
				for (IvAdditive ivAdditive : ivRxDrug.getIvAdditiveList()) {
					foundMedProfileMoItemFm = retrieveLocalMedProfileMoItemFmByRxDrug(ivAdditive, medProfileMoItemFmList);
					if (foundMedProfileMoItemFm != null) {
						FmIndication fmIndication = new FmIndication();
						fmIndication.setRxDrug(ivAdditive);
						fmIndication.setFmEntity(foundMedProfileMoItemFm);
						fmIndicationList.add(fmIndication);
					}
				}
				break;
		}
		
		return fmIndicationList;
	}
		
	private MedProfileMoItemFm retrieveLocalMedProfileMoItemFmByRxDrug(RxDrug rxDrug, List<MedProfileMoItemFm> medProfileMoItemFmList) {
		for (MedProfileMoItemFm medProfileMoItemFm : medProfileMoItemFmList) {
			if (rxDrug.getStrengthCompulsory() == null) {
				throw new UnsupportedOperationException("Strength compulsory is null in RxDrug, medOrderFm id = " + medProfileMoItemFm.getId());
			}
			
			if (rxDrug.getStrengthCompulsory()) {
				if ( StringUtils.equals(medProfileMoItemFm.getDisplayName(), rxDrug.getDisplayName()) &&
						StringUtils.equals(medProfileMoItemFm.getFormCode(), rxDrug.getFormCode()) &&
						StringUtils.equals(medProfileMoItemFm.getSaltProperty(), rxDrug.getSaltProperty()) && 
						medProfileMoItemFm.getFmStatus() == FmStatus.dataValueOf(rxDrug.getFmStatus()) &&
						StringUtils.equals(medProfileMoItemFm.getItemCode(), rxDrug.getItemCode()) ) {
					return medProfileMoItemFm;
				}
			} else {
				if ( StringUtils.equals(medProfileMoItemFm.getDisplayName(), rxDrug.getDisplayName()) &&
						StringUtils.equals(medProfileMoItemFm.getFormCode(), rxDrug.getFormCode()) &&
						StringUtils.equals(medProfileMoItemFm.getSaltProperty(), rxDrug.getSaltProperty()) &&
						medProfileMoItemFm.getFmStatus() == FmStatus.dataValueOf(rxDrug.getFmStatus()) ) {
					return medProfileMoItemFm;
				}
			}
		}
		
		return null;
	}
	
	public boolean validateFmCorpIndByDms(RxDrug rxDrug, FmEntity fmEntity) {
		// this function returns true if valid FM indication is obtained from DMS
		
		FmDrug fmDrug = getFmDrugFromDms(rxDrug);
		
		if (fmDrug == null) {
			return false;
		}

		if (fmEntity.getCorpIndCode() != null && StringUtils.equalsIgnoreCase("Y", fmDrug.getFmInstDrug().getAuthorizationRequired()) && 
				StringUtils.isEmpty(fmEntity.getAuthDoctorName())) {
			return false;
		}
		
		if (fmEntity.getCorpIndCode() == null) {			
			List<FmDrugCorpInd> fmDrugCorpIndList = Arrays.asList(fmDrug.getFmDrugCorpIndArray());
			
			FmDrugCorpInd foundFmDrugCorpInd = null;
			for (FmDrugCorpInd fmDrugCorpInd : fmDrugCorpIndList) {
				if ( fmEntity.getPrescOption().equals(FmStatusOption.dataValueOf(fmDrugCorpInd.getCorpIndicationOption())) &&
						StringUtils.trimToNull(fmDrugCorpInd.getCorpIndicationCode()) != null && 
						! "OTHERS".equals(fmDrugCorpInd.getCorpIndicationCode()) ) {
					foundFmDrugCorpInd = fmDrugCorpInd;
					break;
				}
			}

			if (foundFmDrugCorpInd == null) {
				fmEntity.setIndValidPeriodForDisplay(fmDrug.getValidPeriod());
				return true;
			} else {
				return false;
			}
			
		} else {
			List<FmDrugCorpInd> fmDrugCorpIndList = Arrays.asList(fmDrug.getFmDrugCorpIndArray());
			
			FmDrugCorpInd foundFmDrugCorpInd = null;
			for (FmDrugCorpInd fmDrugCorpInd : fmDrugCorpIndList) {
				if (fmEntity.getCorpIndCode().equals(fmDrugCorpInd.getCorpIndicationCode()) &&
						fmEntity.getPrescOption().equals(FmStatusOption.dataValueOf(fmDrugCorpInd.getCorpIndicationOption())) ) {
					foundFmDrugCorpInd = fmDrugCorpInd;
					break;
				}
			}

			if (foundFmDrugCorpInd == null) {
				return false;
			} else {
				fmEntity.setIndValidPeriodForDisplay(foundFmDrugCorpInd.getValidPeriod());
				return true;
			}
		}
	}

	public String retrievePrescibingDoctorNameFromMoeUserCode(String patHospCode, String userCode){		
		try{
			return moeServiceProxy.retrieveMoeUserNameByMoeUserCode(patHospCode, userCode);
		}catch( MoeException e){
			logger.error("Fail to retrieve Prescibing Doctor Name of [#0, #1] from CMS User Service, please check pms-asa log for detail.", patHospCode, userCode);
			return StringUtils.EMPTY;
		}
	}

	private FmDrug getFmDrugFromDms(RxDrug rxDrug) {
		PmsFmStatusSearchCriteria criteria = new PmsFmStatusSearchCriteria();
		
		criteria.setDispHospCode(workstore.getHospCode());
		criteria.setDispWorkstore(workstore.getWorkstoreCode());
		
		criteria.setTrueDisplayname(rxDrug.getDisplayName());
		criteria.setFormCode(rxDrug.getFormCode());
		criteria.setSaltProperty(rxDrug.getSaltProperty());
		if (rxDrug.getStrengthCompulsory()) {
			criteria.setItemCode(rxDrug.getItemCode());
		}
		criteria.setPmsFmStatus(rxDrug.getFmStatus());
		criteria.setRouteCode(rxDrug.getRouteCode());
		
		return dmsPmsServiceProxy.getFmIndicator(criteria);
	}
	
	public Boolean checkSpecialDrug(PmsFmStatusSearchCriteria criteria) {
		FmDrug fmDrug = dmsPmsServiceProxy.getFmIndicator(criteria);
		if ( fmDrug == null ) {			
			return false;
		}
		FmDrugCorpInd fmDrugCorpInd;		
		for( int i=0; i<fmDrug.getFmDrugCorpIndArray().length; i++ ) {
			fmDrugCorpInd = fmDrug.getFmDrugCorpIndArray()[i];					
			if ( fmDrugCorpInd.getCorpIndicationOption().indexOf(FmStatus.SpecialDrug.getDataValue()) < 0 ) {							
				continue;
			}
			if ( fmDrugCorpInd.getCorpIndicationOption() == null
					|| StringUtils.isEmpty(fmDrugCorpInd.getCorpIndicationCode())
					|| OTHERS.equals(fmDrugCorpInd.getCorpIndicationCode()) ) {							
				continue;				
			}
			return true;
		}
		return false;
	}
	
	@Remove
	public void destroy() {
	}
}
