package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Institution;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.phs.Ward;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("wardManager")
@MeasureCalls
public class WardManagerBean implements WardManagerLocal {

	@PersistenceContext
	private EntityManager em;

	@In
	private Workstore workstore;

	@SuppressWarnings("unchecked")
	public List<String> retrieveWardCodeList() {

		return em.createQuery(
				"select o.wardCode from Ward o" + // 20120227 index check : Ward.institution : FK_WARD_01
				" where o.institution = :institution" +
				" and o.status = :status" +
				" order by o.wardCode")
				.setParameter("institution", workstore.getWorkstoreGroup().getInstitution())
				.setParameter("status", RecordStatus.Active)
				.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Ward> retrieveWardListByPrefixWardCode(String prefixWardCode) {
		
		return em.createQuery(
				"select o from Ward o" + // 20120227 index check : Ward.institution : FK_WARD_01
				" where o.institution = :institution" +
				" and o.wardCode like :prefixWardCode" +
				" and o.status = :status" +
				" order by o.wardCode")
				.setParameter("institution", workstore.getWorkstoreGroup().getInstitution())
				.setParameter("prefixWardCode", prefixWardCode+"%")
				.setParameter("status", RecordStatus.Active)
				.getResultList(); 
	}
	
	@SuppressWarnings("unchecked")
	public List<Ward> retrieveWardListByInstitution( Institution institution ){
		
		return em.createQuery(
				"select o from Ward o" + // 20120227 index check : Ward.institution : FK_WARD_01
				" where o.institution = :institution" +
				" and o.status <> :status" +
				" order by o.wardCode")
				.setParameter("institution", institution)
				.setParameter("status", RecordStatus.Delete)
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Ward> retrieveWardListByInstCode( List<String> instCodeList ){
		
		return em.createQuery(
				"select o from Ward o" + // 20170418 index check : Ward.institution : FK_WARD_01
				" where o.institution.instCode in :instCodeList" +
				" and o.status <> :status" +
				" order by o.institution.instCode, o.wardCode")
				.setParameter("instCodeList", instCodeList)
				.setParameter("status", RecordStatus.Delete)
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Ward> retrieveWardList(){
		
		return em.createQuery(
				"select o from Ward o" +  // 20121112 index check : Ward.institution : FK_WARD_01
				" where o.institution = :institution" +
				" and o.status <> :status" +
				" order by o.wardCode")
				.setParameter("institution", workstore.getWorkstoreGroup().getInstitution())
				.setParameter("status", RecordStatus.Delete)
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Ward> retrieveWardListByWardCodeList(List<String> wardCodeList){
		
		return em.createQuery(
				"select o from Ward o" +  // 20121112 index check : Ward.institution : FK_WARD_01
				" where o.institution = :institution" +
				" and o.status <> :status" +
				" and o.wardCode in :wardCodeList" +
				" order by o.wardCode")
				.setParameter("institution", workstore.getWorkstoreGroup().getInstitution())
				.setParameter("status", RecordStatus.Delete)
				.setParameter("wardCodeList", wardCodeList)
				.getResultList();
	}

}
