package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.model.pms.dms.persistence.PmsIpSpecMapping;
import hk.org.ha.model.pms.vo.reftable.mp.PmsMpSpecMappingInfo;

import java.util.List;

import javax.ejb.Local;

@Local
public interface PmsMpSpecMappingListServiceLocal {

	PmsMpSpecMappingInfo retrievePmsIpSpecMappingList();
	
	PmsMpSpecMappingInfo updatePmsIpSpecMappingList(List<PmsIpSpecMapping> inPmsIpSpecMappingList);

	void clearList();
	
	void printMpSpecMapRpt(List<PmsIpSpecMapping> inPmsIpSpecMappingList);
	
	void exportMpSpecMapRpt();
	
	void sendMpSpecMapRptList(List<PmsIpSpecMapping> inPmsIpSpecMappingList);
	
	void createPmsIpSpecMapExcl(String patHospCode, String pasSpecialty, String ward);
	
	void destroy();
	
}
 