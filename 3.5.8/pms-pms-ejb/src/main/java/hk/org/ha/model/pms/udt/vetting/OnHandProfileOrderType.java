package hk.org.ha.model.pms.udt.vetting;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum OnHandProfileOrderType implements StringValuedEnum {

	ManualOrder("MO", "Manual Order"),
	MedOrder("CO", "Medication Order"),
	SfiOrder("SO", "SFI Order"),
	RefillOrder("RO", "Refill Order"),
	DhOrder("DH", "DH Order");
	
    private final String dataValue;
    private final String displayValue;

    OnHandProfileOrderType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    

	public static OnHandProfileOrderType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(OnHandProfileOrderType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<OnHandProfileOrderType> {

		private static final long serialVersionUID = 1L;

		@Override
		public Class<OnHandProfileOrderType> getEnumClass() {
    		return OnHandProfileOrderType.class;
    	}
    }
}
