package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.corp.cache.DmWarningCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmMoeProperty;
import hk.org.ha.model.pms.dms.persistence.DmWarning;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.FollowUpWarning;
import hk.org.ha.model.pms.persistence.reftable.LocalWarning;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.reftable.FollowUpWarningItem;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("followUpWarningListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class FollowUpWarningListServiceBean implements FollowUpWarningListServiceLocal {

	@PersistenceContext
	private EntityManager em;
	
	@In
	private Workstore workstore; 
	
	@In
	private DmWarningCacherInf dmWarningCacher;
	
	@In
	private PrintAgentInf printAgent;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@Out(required = false)
	private List<FollowUpWarning> followUpWarningList;
	
	@SuppressWarnings("unchecked")
	public void retrieveFollowUpWarningList() 
	{
		followUpWarningList = em.createQuery(
				"select o from FollowUpWarning o" + // 20120303 index check : FollowUpWarning.workstoreGroup : FK_FOLLOW_UP_WARNING_01
				" where o.workstoreGroup = :workstoreGroup" +
				" order by o.itemCode")
				.setParameter("workstoreGroup", workstore.getWorkstoreGroup())
				.getResultList();	
		
		if ( !followUpWarningList.isEmpty() ) 
		{
			for ( FollowUpWarning followUpWarning : followUpWarningList ) 
			{
				List<LocalWarning> localWarningList = retrieveLocalWarningCodeList(followUpWarning.getItemCode());
				
				Map<Integer, LocalWarning> localWarningMap = new HashMap<Integer, LocalWarning>();
				if ( localWarningList.isEmpty() ) 
				{
					continue;
				} 
				
				for ( LocalWarning localWarning : localWarningList ) 
				{
					localWarningMap.put(localWarning.getSortSeq(), localWarning);
				}
				
				boolean isFollowUpItem = false;
				Map<Integer, Boolean> followUpWarningSeqMap = new HashMap<Integer, Boolean>();
				
				for ( int sortSeq = 1; sortSeq <= 4; sortSeq++ ) 
				{
					followUpWarningSeqMap.put(sortSeq, false);
					
					LocalWarning localWarning = localWarningMap.get(sortSeq);
					
					if ( localWarning == null ) 
					{
						localWarning = new LocalWarning();
						localWarning.setSortSeq(sortSeq);
					}
					String hqWarnCode = getHqWarnCode(followUpWarning.getDmDrug().getDmMoeProperty(), sortSeq);
					
					if ( isFollowUpWarning(hqWarnCode, localWarning, dmWarningCacher.getDmWarningByWarnCodeWithSuspend(hqWarnCode)) ) 
					{
						isFollowUpItem = true;
						followUpWarningSeqMap.put(sortSeq, true);
					}
				}
				
				if ( isFollowUpItem ) 
				{
					for ( int sortSeq = 1; sortSeq <= 4; sortSeq++ ) 
					{
						FollowUpWarningItem followUpWarningItem = new FollowUpWarningItem();
						followUpWarningItem.setVisiable(true);
						
						LocalWarning localWarning = localWarningMap.get(sortSeq);
						
						if ( localWarning == null ) 
						{
							localWarning = new LocalWarning();
							localWarning.setSortSeq(sortSeq);
						}
						
						String hqWarnCode = getHqWarnCode(followUpWarning.getDmDrug().getDmMoeProperty(), sortSeq);
						
						DmWarning currentHqWarning = dmWarningCacher.getDmWarningByWarnCodeWithSuspend(hqWarnCode);
						DmWarning preHqWarning = dmWarningCacher.getDmWarningByWarnCodeWithSuspend(localWarning.getOrgWarnCode());
						
						if (StringUtils.isNotBlank(localWarning.getOrgWarnCode())) 
						{
							followUpWarningItem.setPreHqWarnCode(preHqWarning.getWarnCode());
							followUpWarningItem.setPreHqWarnDesc(preHqWarning.getWarnMessageEng());
						}

						if ( StringUtils.isNotBlank(hqWarnCode)) 
						{
							followUpWarningItem.setCurrentHqWarnCode(hqWarnCode);
							if ( currentHqWarning != null ) 
							{
								followUpWarningItem.setCurrentHqWarnDesc(currentHqWarning.getWarnMessageEng());
								followUpWarningItem.setCurrentHqWarnCatCode(currentHqWarning.getWarnCatCode());
								
								if ( StringUtils.equals(currentHqWarning.getWarnCatCode(), "ID") ) 
								{
									followUpWarningItem.setFollowCurrentHqWarnCode(true);
								}
								
								if ( StringUtils.equals(currentHqWarning.getUsageType(), "O") ) 
								{
									followUpWarningItem.setCurrentHqWarnIpOnly("Y");
								} 
								else 
								{
									followUpWarningItem.setCurrentHqWarnIpOnly("N");
								}
							}

						}
						
						DmWarning localDmWarning = dmWarningCacher.getDmWarningByWarnCodeWithSuspend(localWarning.getWarnCode());

						followUpWarningItem.setLocalWarning(localWarning);
						followUpWarningItem.setLocalWarnCode(localWarning.getWarnCode());
						
						if ( localDmWarning != null ) 
						{
							if ("Y".equals(localDmWarning.getSuspend())){
								followUpWarningItem.setSuspend(true);
							} else {
								followUpWarningItem.setSuspend(false);
							}
							followUpWarningItem.setLocalWarnDesc(localDmWarning.getWarnMessageEng());
							
							if ( StringUtils.equals(localDmWarning.getUsageType(), "O") )
							{
								followUpWarningItem.setLocalWarnIpOnly("Y");
							} 
							else 
							{
								followUpWarningItem.setLocalWarnIpOnly("N");
							}
						}

						followUpWarningItem.setSortSeq(localWarning.getSortSeq());
						
						if ( StringUtils.equals(followUpWarningItem.getLocalWarnCode(), followUpWarningItem.getCurrentHqWarnCode()) ) 
						{
							followUpWarningItem.setFollowCurrentHqWarnCode(true);
							followUpWarningItem.setVisiable(false);
						}
						
						followUpWarning.getFollowUpWarningItemList().add(followUpWarningItem);
					}	
				}
			}
			
			for (Iterator<FollowUpWarning> itr = followUpWarningList.iterator();itr.hasNext();) 
			{
				FollowUpWarning followUpWarning = itr.next();
				if ( followUpWarning.getFollowUpWarningItemList().isEmpty() ) 
				{
					em.remove(em.merge(followUpWarning));
					itr.remove();
				}
			}
		}
	
	}
	
	private boolean isFollowUpWarning(String hqWarnCode, LocalWarning localWarning, DmWarning currentHqWarning) {
		String orgWarnCode = "";
		String orgWarnCat = "";
		if ( localWarning != null ) {
			orgWarnCode = localWarning.getOrgWarnCode();
			orgWarnCat = localWarning.getOrgWarnCat();
		} 

		return ( !StringUtils.equals(hqWarnCode, orgWarnCode) || 
				(	
					(currentHqWarning!= null) && 
					( !StringUtils.equals(currentHqWarning.getWarnCatCode(), orgWarnCat) )&& 
					( StringUtils.equals("ID", orgWarnCat) || StringUtils.equals("ID", currentHqWarning.getWarnCatCode()) )
				)
				);
	
	}
	
	private String getHqWarnCode(DmMoeProperty dmMoeProperty, int sortSeq)
	{
		String hqWarnCode = null;
		try {
			hqWarnCode = (String) PropertyUtils.getProperty(dmMoeProperty, "warnCode"+sortSeq);
		} catch (IllegalAccessException e) {
		} catch (InvocationTargetException e) {
		} catch (NoSuchMethodException e) {
		}
		
		return hqWarnCode;
	}
	
	@SuppressWarnings("unchecked")
	private List<LocalWarning> retrieveLocalWarningCodeList(String itemCode){
		List<LocalWarning> localWarningList = em.createQuery(
												"select o from LocalWarning o" + // 20120303 index check : LocalWarning.workstoreGroup,itemCode : I_LOCAL_WARNING_01
												" where o.workstoreGroup = :workstoreGroup" +
												" and o.itemCode = :itemCode" +
												" order by o.sortSeq")
												.setParameter("workstoreGroup", workstore.getWorkstoreGroup())
												.setParameter("itemCode", itemCode)
												.getResultList();
		em.clear();
		return localWarningList;
	}
	
	public void updateFollowUpWarningList(List<FollowUpWarning> updateFollowUpWarningList) 
	{
		for ( FollowUpWarning followUpWarning : updateFollowUpWarningList ) 
		{
			followUpWarning.setMarkDelete(true);
			List<LocalWarning> localWarningList = retrieveLocalWarningCodeList(followUpWarning.getItemCode());
			
			for ( FollowUpWarningItem followUpWarningItem : followUpWarning.getFollowUpWarningItemList() ) 
			{
				LocalWarning localWarning = getLocalWarningBySortSeq(localWarningList, followUpWarningItem.getSortSeq());
				
				if ( localWarning == null ) 
				{ 
					localWarning = createLocalWarning(followUpWarning.getItemCode(), followUpWarningItem.getSortSeq());
					localWarningList.add(localWarning);
				}

				if ( followUpWarningItem.isKeepLocalWarnCode() ) 
				{
					localWarning.setWarnCode(followUpWarningItem.getLocalWarnCode());
					localWarning.setOrgWarnCode(followUpWarningItem.getCurrentHqWarnCode());
					localWarning.setOrgWarnCat(followUpWarningItem.getCurrentHqWarnCatCode());
				} 
				else if ( followUpWarningItem.isFollowCurrentHqWarnCode() ) 
				{
					localWarning.setWarnCode(followUpWarningItem.getCurrentHqWarnCode());
					localWarning.setOrgWarnCode(followUpWarningItem.getCurrentHqWarnCode());
					localWarning.setOrgWarnCat(followUpWarningItem.getCurrentHqWarnCatCode());
				} 
				else if ( !followUpWarningItem.isVisiable() || 
						( !followUpWarningItem.isKeepLocalWarnCode() && !followUpWarningItem.isFollowCurrentHqWarnCode() )) 
				{
					followUpWarning.setMarkDelete(false);
				}
			}
			
			if ( followUpWarning.isMarkDelete() && removeLocalWarning(localWarningList) ) {
				for ( LocalWarning localWarning : localWarningList ) {
					if ( localWarning.getId() != null ) {
						em.remove(em.merge(localWarning));
					}
				}
			} else {
				for ( LocalWarning localWarning : localWarningList ) {
					if ( localWarning.getId() != null ) {
						em.merge(localWarning);
					} else if ( StringUtils.isNotBlank(localWarning.getOrgWarnCode()) || StringUtils.isNotBlank(localWarning.getWarnCode())){
						em.persist(localWarning);
					}
				}
			}
			em.flush();
		}
		
		for ( FollowUpWarning followUpWarning : updateFollowUpWarningList ) {
			if ( followUpWarning.isMarkDelete() ) {
				FollowUpWarning delFollowUpWarning = em.find(FollowUpWarning.class, followUpWarning.getId());
				if ( delFollowUpWarning != null ) {
					em.remove(delFollowUpWarning);
				}
			}
		}	
		em.flush();
		
		retrieveFollowUpWarningList();
	}
	
	private LocalWarning getLocalWarningBySortSeq(List<LocalWarning> localWarningList, Integer sortSeq) {
		for ( LocalWarning localWarning : localWarningList ) {
			if ( localWarning.getSortSeq().equals(sortSeq) ) {
				return localWarning;
			}
		}
		return null;
	}
	
	private LocalWarning createLocalWarning(String itemCode, Integer sortSeq) {
		LocalWarning localWarning = new LocalWarning();
		localWarning.setItemCode(itemCode);
		localWarning.setSortSeq(sortSeq);
		localWarning.setWorkstoreGroup(workstore.getWorkstoreGroup());
		return localWarning;
	}
	
	private boolean removeLocalWarning(List<LocalWarning> localWarningList) {
		for ( LocalWarning localWarning : localWarningList ) {
			if ( !StringUtils.equals(localWarning.getWarnCode(), localWarning.getOrgWarnCode()) ) {
				return false;
			}
		}
		return true;
	}

	public List<FollowUpWarning> retrieveFollowUpWarningRptList()
	{
		List<FollowUpWarning> followUpWarningRptList = new ArrayList<FollowUpWarning>();
		
		for ( FollowUpWarning followUpWarning : followUpWarningList ) {

			FollowUpWarning followUpWarningRpt = new FollowUpWarning();
			followUpWarningRpt.setItemCode(followUpWarning.getItemCode());
			followUpWarningRpt.setDmDrug(followUpWarning.getDmDrug());
			followUpWarningRpt.setCreateDate(followUpWarning.getCreateDate());
			
			List<FollowUpWarningItem> followUpWarningRptItemList = new ArrayList<FollowUpWarningItem>();
			followUpWarningRpt.setFollowUpWarningItemList(followUpWarningRptItemList);
			
			for (int i = 1; i <=4; i++) {
				FollowUpWarningItem followUpWarningRptItem = new FollowUpWarningItem();
				for ( FollowUpWarningItem followUpWarningItem : followUpWarning.getFollowUpWarningItemList() ) {
					if (followUpWarningItem.getSortSeq() == i) {
						followUpWarningRptItem = followUpWarningItem;
					}
				}
				followUpWarningRptItemList.add(followUpWarningRptItem);
			}
			followUpWarningRptList.add(followUpWarningRpt);
		}
		return followUpWarningRptList;
		
	}
	
	public void printFollowUpWarningRpt()
	{
		List<FollowUpWarning> followUpWarningRptList = retrieveFollowUpWarningRptList();
		
		if ( followUpWarningRptList != null ) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("hospCode", workstore.getHospCode());
			parameters.put("workstoreCode", workstore.getWorkstoreCode());
			
		    printAgent.renderAndPrint(new RenderAndPrintJob(
				    "FollowUpWarningRpt",
				    new PrintOption(),
				    printerSelector.retrievePrinterSelect(PrintDocType.Report),
				    parameters,
				    followUpWarningRptList));
		}
	}
	
	@Remove
	public void destroy() 
	{		
		if (followUpWarningList != null) {
			followUpWarningList = null;
		}
	}

}
