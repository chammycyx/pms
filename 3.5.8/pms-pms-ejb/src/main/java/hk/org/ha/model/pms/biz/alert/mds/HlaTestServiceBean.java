package hk.org.ha.model.pms.biz.alert.mds;

import hk.org.ha.fmk.pms.web.MeasureCalls;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("hlaTestService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class HlaTestServiceBean implements HlaTestServiceLocal {

	@In 
	private HlaTestManagerLocal hlaTestManager;

	@Override
	public boolean hasHlaTestAlertBefore(String hkid, String patHospCode,
			String caseNum) {
		
		return hlaTestManager.hasHlaTestAlertBefore(hkid, patHospCode, caseNum);
	}
	
	@Remove
	public void destroy() {
	}
}
