package hk.org.ha.model.pms.biz.picking;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.picking.DrugPickWorkstation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("drugPickListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DrugPickListServiceBean implements DrugPickListServiceLocal {
	
	@PersistenceContext
	private EntityManager em;

	@In
	private Workstore workstore;
	
	@SuppressWarnings("unchecked")
	public List<DrugPickWorkstation> retrieveWorkstationList(){
		List<DrugPickWorkstation> drugPickWorkstationList = new ArrayList<DrugPickWorkstation>();
		
		List<Workstation> workstationList = em.createQuery(
												"select o from Workstation o" + // 20120303 index check : Workstation.workstore : FK_WORKSTATION_01
												" where o.workstore = :workstore" +
												" order by o.workstationCode")
												.setParameter("workstore", workstore)
												.getResultList();
		
		if ( !workstationList.isEmpty() ) {
			for ( Workstation workstation : workstationList ) {
				DrugPickWorkstation drugPickWorkstation = new DrugPickWorkstation();
				drugPickWorkstation.setWorkstationId(workstation.getId());
				drugPickWorkstation.setWorkstationCode(workstation.getWorkstationCode());
				drugPickWorkstation.setHostName(workstation.getHostName());
				drugPickWorkstationList.add(drugPickWorkstation);
			}
		}
		
		return drugPickWorkstationList;
	}

	@SuppressWarnings("unchecked")
	public List<Integer> retrieveAvaliableItemNumList(Date ticketDate, String ticketNum) {		
		return em.createQuery(
				"select o.itemNum from DispOrderItem o" + // 20120214 index check : Ticket.workstore,orderType,ticketDate,ticketNum : UI_TICKET_01
				" where o.dispOrder.workstore = :workstore" +
				" and o.dispOrder.ticket.orderType = :orderType" +
				" and o.dispOrder.ticket.ticketDate = :ticketDate" +
				" and o.dispOrder.ticket.ticketNum = :ticketNum" +
				" and o.dispOrder.status not in :dispOrderStatus" +
				" and o.status = :status")
				.setParameter("workstore", workstore)
				.setParameter("orderType", OrderType.OutPatient)
				.setParameter("ticketDate", ticketDate, TemporalType.DATE)
				.setParameter("ticketNum", ticketNum)
				.setParameter("dispOrderStatus", DispOrderStatus.Deleted_SysDeleted)
				.setParameter("status", DispOrderItemStatus.Vetted)
				.getResultList();
	}
	
	
	@Remove
	public void destroy() 
	{
	}
}
