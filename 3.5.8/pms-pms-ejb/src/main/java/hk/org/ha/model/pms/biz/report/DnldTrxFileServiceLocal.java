package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.exception.sftp.SftpCommandException;

import java.io.IOException;
import java.text.ParseException;

import javax.ejb.Local;

import net.lingala.zip4j.exception.ZipException;

@Local
public interface DnldTrxFileServiceLocal {

	void retrieveDnldTrxFileList() throws ParseException, SftpCommandException;
	
	void dnldDailyTrxFile(String fileName, String password) throws SftpCommandException, ZipException, IOException;

	void dnldMonthlyArchiveFile(String fileName, String password) throws ParseException, SftpCommandException, ZipException, IOException;

	void dnldTrxFile() throws IOException, ZipException, SftpCommandException;
	
	void destroy();
	
}
