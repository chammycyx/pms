package hk.org.ha.model.pms.biz.printing;

import java.util.List;

import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;

import javax.ejb.Local;

@Local
public interface InvoiceRendererLocal {
	
	void generateSfiInvoiceItemList();

	void printInvoice(DispOrder dispOrder);
	String printSfiInvoice(Invoice invoice, boolean isReprint);
	void prepareInvoicePrintJob(List<RenderAndPrintJob> printJobList, DispOrder dispOrder);
	String prepareSfiInvoicePrintJob(List<RenderAndPrintJob> printJobList, Invoice invoice, boolean isReprint);
	
	void createSfiInvoiceListForPreview(Invoice invoice, boolean isReprint);
	
	void destroy();	
	
}
