package hk.org.ha.model.pms.vo.reftable.mp;

import hk.org.ha.model.pms.dms.persistence.PmsIpDurationConv;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class MpPmsDurationConversionItem implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String workstoreCode;

	private String hospCode;
	
	private boolean sameWorkstoreGroup;
	
	private List<PmsIpDurationConv> pmsIpDurationConvList;

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setPmsIpDurationConvList(
			List<PmsIpDurationConv> pmsIpDurationConvList) {
		this.pmsIpDurationConvList = pmsIpDurationConvList;
	}

	public List<PmsIpDurationConv> getPmsIpDurationConvList() {
		if ( pmsIpDurationConvList == null ) {
			pmsIpDurationConvList = new ArrayList<PmsIpDurationConv>();
		}
		return pmsIpDurationConvList;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setSameWorkstoreGroup(boolean sameWorkstoreGroup) {
		this.sameWorkstoreGroup = sameWorkstoreGroup;
	}

	public boolean isSameWorkstoreGroup() {
		return sameWorkstoreGroup;
	}
	
}