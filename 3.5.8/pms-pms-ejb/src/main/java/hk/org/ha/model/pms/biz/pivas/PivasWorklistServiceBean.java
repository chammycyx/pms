package hk.org.ha.model.pms.biz.pivas;

import static hk.org.ha.model.pms.prop.Prop.PIVAS_CURRENTDATE_BUFFER;
import static hk.org.ha.model.pms.prop.Prop.PIVAS_DRUGCALQTY_ROUNDINGMODE;
import static hk.org.ha.model.pms.prop.Prop.PIVAS_DRUGDISPQTY;
import static hk.org.ha.model.pms.prop.Prop.PIVAS_REQUEST_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.PIVAS_SATELLITE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.PIVAS_SUPPLYDATE_BUFFER;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.util.QueryUtils;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.reftable.ActiveWardManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmDailyFrequencyCacherInf;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.corp.cache.DmWarningCacherInf;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDailyFrequency;
import hk.org.ha.model.pms.dms.persistence.DmMoeProperty;
import hk.org.ha.model.pms.dms.persistence.DmWarning;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasContainer;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrug;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrugManuf;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrugManufMethod;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormula;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaMethod;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaMethodDrug;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasWard;
import hk.org.ha.model.pms.dms.udt.pivas.PivasDrugManufStatus;
import hk.org.ha.model.pms.dms.vo.PivasFormulaSearchCriteria;
import hk.org.ha.model.pms.dms.vo.pivas.PivasBatchPrepDetail;
import hk.org.ha.model.pms.dms.vo.pivas.PivasPrepCriteria;
import hk.org.ha.model.pms.dms.vo.pivas.PivasPrepCriteriaDetail;
import hk.org.ha.model.pms.dms.vo.pivas.PivasPrepValidateResponse;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.persistence.pivas.MaterialRequest;
import hk.org.ha.model.pms.persistence.pivas.MaterialRequestItem;
import hk.org.ha.model.pms.persistence.pivas.PivasPrep;
import hk.org.ha.model.pms.persistence.pivas.PivasPrepMethod;
import hk.org.ha.model.pms.persistence.pivas.PivasPrepMethodItem;
import hk.org.ha.model.pms.persistence.pivas.PivasWorklist;
import hk.org.ha.model.pms.persistence.reftable.pivas.PivasServiceHour;
import hk.org.ha.model.pms.udt.RecordStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatus;
import hk.org.ha.model.pms.udt.pivas.MaterialRequestItemStatus;
import hk.org.ha.model.pms.udt.pivas.PivasWorklistStatus;
import hk.org.ha.model.pms.udt.pivas.PivasWorklistType;
import hk.org.ha.model.pms.udt.reftable.pivas.PivasServiceHourType;
import hk.org.ha.model.pms.udt.rx.RxItemType;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.model.pms.vo.pivas.worklist.CalculatePivasInfoCriteria;
import hk.org.ha.model.pms.vo.pivas.worklist.PivasDueOrderListInfo;
import hk.org.ha.model.pms.vo.pivas.worklist.PivasFormulaMethodInfo;
import hk.org.ha.model.pms.vo.pivas.worklist.PivasPrepDetailInfo;
import hk.org.ha.model.pms.vo.pivas.worklist.PivasSupplyDateListInfo;
import hk.org.ha.model.pms.vo.pivas.worklist.PivasWorklistResponse;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.Freq;
import hk.org.ha.model.pms.vo.rx.IvRxDrug;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.time.DateUtils;
import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("pivasWorklistService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
@SuppressWarnings("unchecked")
public class PivasWorklistServiceBean implements PivasWorklistServiceLocal {
	
	private static String VEND_SUPPLY_SOLV = "VEND_SUPPLY_SOLV";

	@PersistenceContext
	private EntityManager em;
	
	@In
	private AuditLogger auditLogger; 
	
	@In
	private Workstore workstore;

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;
	
	@In
	private DmDrugCacherInf dmDrugCacher;

	@In
	private DmWarningCacherInf dmWarningCacher;
	
	@In
	private DmDailyFrequencyCacherInf dmDailyFrequencyCacher;
	
	@In
	private PivasWorklistClientNotifierInf pivasWorklistClientNotifier;
	
	@In
	private PivasWorklistManagerLocal pivasWorklistManager;
	
	@In
	private PivasServiceHourManagerLocal pivasServiceHourManager;
	
	@In
	private ActiveWardManagerLocal activeWardManager;
	
	private static final String PROFILE_AND_ITEM_QUERY = 
			" and o.medProfileMoItem.pivasFlag = :pivasFlag" +
			// filter worklist of discharge and home leave profile 
			" and (o.medProfile.status = :mpStatus" +
			" or (o.medProfile.status = :mpHomeLeaveStatus and o.medProfile.returnDate <= :currentDate))" +
			// filter worklist of frozen (non-IPMOE) item
			" and o.medProfileMoItem.frozenFlag = :frozenFlag" +
			// filter worklist of IPMOE item which profile is transferred from IPMOE ward to non-IPMOE ward and then back to IPMOE ward
			// (because some item is discontinued after transfer by setting transfer flag = Y)
			" and o.medProfileMoItem.transferFlag = :transferFlag" +
			// filter worklist of order line item modified by CMS
			" and o.medProfileMoItem = o.medProfileMoItem.medProfileItem.medProfileMoItem" +
			// filter not verified worklist and worklist of invalid item status (Discontinued, Deleted, Ended)
			" and o.medProfileMoItem.medProfileItem.status = :mpiStatus" +
			// filter worklist of ended item 
			// (because order to be ended is marked ended by batch job every 15 mins, for ended order but not yet marked ended, item end date is used to filter)
			" and (o.medProfileMoItem.endDate > :currentDate or o.medProfileMoItem.endDate is null)";
	
	public List<PivasWorklist> retrievePivasInboxList() {
		List<PivasWorklist> pivasWorklistList = retrieveNewPivasWorklistList(null);
		
		initAndLoadUnsavedAndSavedPivasWorklist(pivasWorklistList, null);

		validatePivasWorklist(pivasWorklistList, false);
		
		pivasWorklistList = finalFilterPivasWorklist(pivasWorklistList);
		
		return pivasWorklistList;
	}

	public PivasDueOrderListInfo retrievePivasDueOrderList(Date supplyDate) {
		PivasDueOrderListInfo pivasDueOrderListInfo = new PivasDueOrderListInfo();

		// calculate next refill supply date
		Date nextSupplyDate = calculateNextRefillSupplyDate(supplyDate);
		
		if ((new Date()).compareTo(nextSupplyDate) >= 0) {
			pivasDueOrderListInfo.setErrorCode("0914");
			return pivasDueOrderListInfo;
		}

		// retrieve worklist for due order
		List<PivasWorklist> nonRefillPivasWorklistList = retrieveNewPivasWorklistList(supplyDate);
		List<PivasWorklist> refillPivasWorklistList = retrieveRefillPivasWorklistList(supplyDate, nextSupplyDate);
		
		List<PivasWorklist> pivasWorklistList = new ArrayList<PivasWorklist>();
		pivasWorklistList.addAll(nonRefillPivasWorklistList);
		pivasWorklistList.addAll(refillPivasWorklistList);
		
		// process worklist
		initAndLoadUnsavedAndSavedPivasWorklist(pivasWorklistList, supplyDate);
		
		validatePivasWorklist(pivasWorklistList, false);
		
		pivasWorklistList = finalFilterPivasWorklist(pivasWorklistList);
		
		if ( ! pivasWorklistList.isEmpty()) {
			// prepare warning message
			List<PivasWorklist> addedPivasWorklistList = retrievePreparedPivasWorklist(false);
			
			if ( ! addedPivasWorklistList.isEmpty()) {
				PivasWorklist firstAddedPivasWorklist = addedPivasWorklistList.get(0);
				if ( ! firstAddedPivasWorklist.getSupplyDate().equals(supplyDate)) {
					pivasDueOrderListInfo.setErrorCode("0916");
					
					DateFormat dateTimeFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
					List<String> errorParamList = new ArrayList<String>();
					errorParamList.add(dateTimeFormat.format(firstAddedPivasWorklist.getSupplyDate()));
					errorParamList.add(dateTimeFormat.format(supplyDate));
					
					pivasDueOrderListInfo.setErrorParamList(errorParamList);
				}
			}
		}
		
		// return result
		pivasDueOrderListInfo.setNextSupplyDate(nextSupplyDate);
		pivasDueOrderListInfo.setPivasWorklistList(pivasWorklistList);
		
		return pivasDueOrderListInfo;
	}

	public List<PivasWorklist> retrievePivasWorklistList() {
		List<PivasWorklist> pivasWorklistList = retrievePreparedPivasWorklist(true);

		// init and load vo to PIVAS worklist
		loadSavedPivasWorklist(pivasWorklistList);

		validatePivasWorklist(pivasWorklistList, false);
		
		// skip to call finalFilterPivasWorklist() at the end of this function
		// because earliest supply date is filtered in retrievePreparedPivasWorklist() at the beginning 
		// of this function, if put another filter here, following error scenario may occur:
		// after earliest date worklist is already filtered out, if all of those worklist are filtered
		// out here, then final worklist list is empty, but actually worklist with second earliest date 
		// should be included 
		// (note: since finalFilterPivasWorklist() filter worklist by PivasWorklist filter flag, and the
		//        filter flag is set in pivas quantity calculation only, but this function does not trigger
		//        pivas quantity calculation, there is no impact even finalFilterPivasWorklist() is skipped here)
		
		return pivasWorklistList;
	}

	public List<PivasWorklist> retrievePivasFindPatientList(String searchKey) {
		List<PivasWorklist> pivasWorklistList = retrievePivasWorklistListByHkidCaseNum(searchKey);
		
		initAndLoadFindPatientPivasWorklist(pivasWorklistList);
		
		validatePivasWorklist(pivasWorklistList, false);
		
		pivasWorklistList = finalFilterPivasWorklist(pivasWorklistList);
		
		return pivasWorklistList;
	}

	public PivasSupplyDateListInfo retrievePivasSupplyDateListInfo() {
		Date todayDefaultSupplyDate = calculateTodayDefaultSupplyDate();
		List<PivasServiceHour> pivasServiceHourList = pivasWorklistManager.retrievePivasSupplyDateList(workstore.getWorkstoreGroup().getWorkstoreGroupCode(), workstore.getHospital());
		
		PivasSupplyDateListInfo pivasSupplyDateListInfo = new PivasSupplyDateListInfo();
		pivasSupplyDateListInfo.setDefaultSupplyDate(todayDefaultSupplyDate);
		pivasSupplyDateListInfo.setPivasServiceHourList(pivasServiceHourList);
		
		return pivasSupplyDateListInfo;
	}

	public PivasWorklistResponse addToWorklist(List<PivasWorklist> pivasWorklistList) {
		PivasWorklistResponse addToWorklistResponse = validateAddToWorklist(pivasWorklistList);
		if (addToWorklistResponse.getErrorCode() != null) {
			return addToWorklistResponse;
		}
		
		validatePivasWorklist(pivasWorklistList, true);
		
		boolean optimisticLockError = false;
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			if ("0307".equals(pivasWorklist.getErrorCode())) {
				optimisticLockError = true;
				break;
			}
		}
		
		List<PivasWorklist> validPivasWorklistList = extractValidPivasWorklist(pivasWorklistList);
		
		for (PivasWorklist validPivasWorklist : validPivasWorklistList) {
			doUpdatePivasWorklist(validPivasWorklist, true);
		}
		
		pivasWorklistClientNotifier.dispatchUpdateEvent(workstore.getWorkstoreGroup().getWorkstoreGroupCode());
		
		PivasWorklistResponse pivasWorklistResponse = new PivasWorklistResponse();
		if (optimisticLockError) {
			pivasWorklistResponse.setErrorCode("0307");
		}
		return pivasWorklistResponse;
	}
	
	public PivasWorklistResponse deleteFromWorklist(List<PivasWorklist> pivasWorklistList) {
		Map<Long, PivasPrep> pivasPrepMap = new HashMap<Long, PivasPrep>();
		
		boolean optimisticLockError = false;
		
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			PivasPrep pivasPrep = pivasWorklist.getPivasPrep();
			MaterialRequest materialRequest = pivasWorklist.getMaterialRequest();
			MedProfile medProfile = pivasWorklist.getMedProfile();

			String errorCode = validatePivasWorklistVersion(medProfile.getId(), medProfile.getVersion());
			if (errorCode != null) {
				pivasWorklist.setErrorCode(errorCode);
				optimisticLockError = true;
				continue;
			}
			
			if ( ! pivasPrepMap.containsKey(pivasPrep.getId())) {
				// if multiple ad-hoc worklist and refill worklist of same PivasPrep exist, only save PivasPrep to DB once
				// to prevent optimistic lock exception
				pivasPrepMap.put(pivasPrep.getId(), pivasPrep);
				
				// force to set update date for optimistic lock
				pivasPrep.setUpdateDate(new Date());
				
				PivasPrep findPivasPrep = em.find(PivasPrep.class, pivasPrep.getId());
				pivasPrep.setMedProfile(findPivasPrep.getMedProfile());
				pivasPrep.setMedProfileMoItem(findPivasPrep.getMedProfileMoItem());
				
				em.merge(pivasPrep);
			}
			
			if (pivasWorklist.getAdHocFlag()) {
				pivasWorklist.setStatus(PivasWorklistStatus.Deleted);
				
				em.merge(pivasWorklist);
				
				if (materialRequest != null) {
					for (MaterialRequestItem materialRequestItem : materialRequest.getMaterialRequestItemList()) {
						MaterialRequestItem findMaterialRequestItem = em.find(MaterialRequestItem.class, materialRequestItem.getId());
						materialRequestItem.setMedProfilePoItem(findMaterialRequestItem.getMedProfilePoItem());
					}

					for (MaterialRequestItem materialRequestItem : materialRequest.getMaterialRequestItemList()) {
						materialRequestItem.setStatus(MaterialRequestItemStatus.Deleted);
						em.merge(materialRequestItem);
					}
				}
				
			} else {
				if ( ! pivasWorklist.getRefillFlag()) {
					pivasWorklist.setStatus(PivasWorklistStatus.New);
				} else {
					pivasWorklist.setStatus(PivasWorklistStatus.Refill);
				}
				
				// first dose MaterialRequest (only necessary for refill worklist)
				MaterialRequest firstDoseMaterialRequest = null;
				if (pivasWorklist.getFirstDoseMaterialRequestId() != null) {
					firstDoseMaterialRequest = em.find(MaterialRequest.class, pivasWorklist.getFirstDoseMaterialRequestId());
				}
				pivasWorklist.setFirstDoseMaterialRequest(firstDoseMaterialRequest);
				
				em.merge(pivasWorklist);
			}
		}

		pivasWorklistClientNotifier.dispatchUpdateEvent(workstore.getWorkstoreGroup().getWorkstoreGroupCode());

		PivasWorklistResponse pivasWorklistResponse = new PivasWorklistResponse();
		if (optimisticLockError) {
			pivasWorklistResponse.setErrorCode("0307");
		}
		return pivasWorklistResponse;
	}

	public PivasPrepDetailInfo retrievePivasPrepDetailInfo(Long medProfileId, Long medProfileVersion, Long medProfileMoItemId, 
			Long pivasFormulaMethodId, Long firstDoseMaterialRequestId) {
		
		PivasPrepDetailInfo pivasPrepDetailInfo = new PivasPrepDetailInfo();

		// container
		List<PivasContainer> pivasContainerList = dmsPmsServiceProxy.retrievePivasContainerList(workstore.getWorkstoreGroup().getWorkstoreGroupCode());

		// solvent and diluent 
		if (pivasFormulaMethodId != null) {
			PivasFormulaMethodInfo pivasFormulaMethodInfo = retrievePivasFormulaMethodInfoById(pivasFormulaMethodId);
			pivasPrepDetailInfo.setSolventPivasDiluentInfoList(pivasFormulaMethodInfo.getSolventPivasDiluentInfoList());
			pivasPrepDetailInfo.setDiluentPivasDiluentInfoList(pivasFormulaMethodInfo.getDiluentPivasDiluentInfoList());
		}
		
		// warning
		List<DmWarning> dmWarningList = new ArrayList<DmWarning>();
		List<DmWarning> allDmWarningList = dmWarningCacher.getDmWarningList();
		for (DmWarning dmWarning : allDmWarningList) {
			if ( "ST".equals(dmWarning.getWarnCatCode()) ) {
				dmWarningList.add(dmWarning);
			}
		}

		// supply date
		List<PivasServiceHour> pivasServiceHourList = pivasWorklistManager.retrievePivasSupplyDateList(workstore.getWorkstoreGroup().getWorkstoreGroupCode(), workstore.getHospital());		
		
		// PIVAS request item
		Map<Long, List<MedProfilePoItem>> mrMedProfilePoItemListMap = retrieveMrMedProfilePoItemListMap(Arrays.asList(medProfileMoItemId));
		List<MedProfilePoItem> mrMedProfilePoItemList = mrMedProfilePoItemListMap.get(medProfileMoItemId);
		
		pivasPrepDetailInfo.setPivasContainerList(pivasContainerList);
		pivasPrepDetailInfo.setDmWarningList(dmWarningList);
		pivasPrepDetailInfo.setPivasServiceHourList(pivasServiceHourList);
		pivasPrepDetailInfo.setMrMedProfilePoItemList(mrMedProfilePoItemList);
		
		// first dose info (for PIVAS detail popup)
		if (firstDoseMaterialRequestId != null) {
			MaterialRequest firstDoseMaterialRequest = em.find(MaterialRequest.class, firstDoseMaterialRequestId);
			firstDoseMaterialRequest.getMedProfileMoItem();
			for (MaterialRequestItem firstDoseMaterialRequestItem : firstDoseMaterialRequest.getMaterialRequestItemList()) {
				firstDoseMaterialRequestItem.getMedProfilePoItem();
			}
			
			pivasPrepDetailInfo.setFirstDoseMaterialRequest(firstDoseMaterialRequest);
		}

		// prompt error if worklist is already updated by other party
		String errorCode = validatePivasWorklistVersion(medProfileId, medProfileVersion);
		if (errorCode != null) {
			pivasPrepDetailInfo.setErrorCode(errorCode);
		}
		
		return pivasPrepDetailInfo;
	}
	
	public List<PivasFormula> retrievePivasFormulaListByDrugKey(String patHospCode, String ward, 
			Integer drugKey, String moDosageUnit, String siteCode, String diluentCode, String strength) {
		
		PivasFormulaSearchCriteria criteria = new PivasFormulaSearchCriteria();
		criteria.setPatHospCode(patHospCode);
		criteria.setWard(ward);
		criteria.setDispHospCode(workstore.getHospCode());
		criteria.setDrugKey(drugKey);
		criteria.setMoDosageUnit(moDosageUnit);
		criteria.setSiteCode(siteCode);
		criteria.setDiluentCode(diluentCode);
		criteria.setStrength(strength);
		criteria.setWorkstoreGroupCode(workstore.getWorkstoreGroup().getWorkstoreGroupCode());
		
		List<PivasFormula> pivasFormulaList = dmsPmsServiceProxy.retrievePivasFormulaListByDrugInfo(criteria);
		
		Collections.sort(pivasFormulaList, new PivasBatchPrepServiceBean.PivasFormulaComparator());
		
		return pivasFormulaList;
	}

	public PivasFormula retrievePivasFormulaById(Long pivasFormulaId) {
		List<Long> pivasFormulaIdList = new ArrayList<Long>();
		pivasFormulaIdList.add(pivasFormulaId);
		
		List<PivasFormula> pivasFormulaList = dmsPmsServiceProxy.retrievePivasFormulaListByPivasFormulaId(workstore.getHospCode(), pivasFormulaIdList);
		List<PivasFormulaMethod> corePivasFormulaMethodList = dmsPmsServiceProxy.retrieveCorePivasFormulaMethodListByPivasFormulaId(workstore.getHospCode(), pivasFormulaIdList);
		
		PivasFormula pivasFormula = pivasFormulaList.get(0);
		
		pivasFormula.setPivasFormulaMethodList(corePivasFormulaMethodList);
		
		return pivasFormula;
	}
	
	public PivasFormulaMethodInfo retrievePivasFormulaMethodInfoById(Long pivasFormulaMethodId) {
		PivasFormulaMethodInfo pivasFormulaMethodInfo = new PivasFormulaMethodInfo();
		
		PivasPrepCriteria pivasPrepCriteria = new PivasPrepCriteria();
		pivasPrepCriteria.setPivasFormulaMethodId(pivasFormulaMethodId);
		pivasPrepCriteria.setWorkstoreGroupCode(workstore.getWorkstoreGroup().getWorkstoreGroupCode());
		pivasPrepCriteria.setHospCode(workstore.getHospCode());
		pivasPrepCriteria.setSolventManufCode(null);
		pivasPrepCriteria.setDiluentManufCode(null);
		pivasPrepCriteria.setPivasContainerId(null);
		
		PivasBatchPrepDetail pivasBatchPrepDetail = dmsPmsServiceProxy.retrievePivasBatchPrepDetail(pivasPrepCriteria, true);
		
		PivasDrug pivasDrug = pivasBatchPrepDetail.getPivasFormulaMethod().getFirstPivasFormulaMethodDrug().getPivasDrugManufMethod().getPivasDrugManuf().getPivasDrug();
		pivasDrug.setDmDrug(pivasWorklistManager.retrieveValidDmDrug(pivasDrug.getItemCode(), workstore));
		
		pivasFormulaMethodInfo.setPivasFormulaMethod(pivasBatchPrepDetail.getPivasFormulaMethod());
		pivasFormulaMethodInfo.setSolventPivasDiluentInfoList(pivasWorklistManager.convertPivasSolventList(pivasBatchPrepDetail.getSolventList(), workstore));
		pivasFormulaMethodInfo.setDiluentPivasDiluentInfoList(pivasWorklistManager.convertPivasDiluentList(pivasBatchPrepDetail.getDiluentList(), workstore));
		
		return pivasFormulaMethodInfo;
	}

	public PivasWorklist recalculatePivasInfo(CalculatePivasInfoCriteria calculatePivasInfoCriteria) {
		PivasWorklist pivasWorklist = calculatePivasInfoCriteria.getPivasWorklist();
		
		// note: for normal recalculation trigger, only 1 fleld change flag of criteria is set to true, and so only 1 recalculation type is run 
		//       but if there is recalculation trigger pended in front-end when initial recalculation is running in back-end, field change flag of initial and all
		//       previous pending criteria is merged into latest pending criteria, then multiple recalculation type is run at the same time
		
		if (pivasWorklist.getAdHocFlag()) {
			// no quantity calculation is needed for find patient worklist
			if (calculatePivasInfoCriteria.getChangeSupplyDateFlag()) {
				initPivasWorklistBySupplyDateChange(pivasWorklist, false);
			}
			if (calculatePivasInfoCriteria.getChangeSatelliteFlag()) {
				initPivasWorklistBySatelliteFlagChange(pivasWorklist);
			}
			if (calculatePivasInfoCriteria.getChangeMaterialRequestItemFlag()) {
				calculateMaterialRequestItemIssueQty(pivasWorklist);
			}
			if (calculatePivasInfoCriteria.getChangeAdjQtyFlag() || calculatePivasInfoCriteria.getChangeIssueQtyFlag()) {
				calculateNumOfLabel(pivasWorklist);
				calculateDrugDispQty(pivasWorklist);
			}
		} else {
			if (calculatePivasInfoCriteria.getChangeSupplyDateFlag() || calculatePivasInfoCriteria.getChangeSatelliteFlag() ||
					calculatePivasInfoCriteria.getChangeOtherFieldFlag()) {
				calculatePivasInfo(true, pivasWorklist, calculatePivasInfoCriteria.getMrMedProfilePoItemList(), calculatePivasInfoCriteria.getChangeSupplyDateFlag(), calculatePivasInfoCriteria.getChangeSatelliteFlag());
			} 
			if (calculatePivasInfoCriteria.getChangeIssueQtyFlag()) {
				initPivasWorklistByIssueQtyChange(pivasWorklist);			
			}
			if (calculatePivasInfoCriteria.getChangeMaterialRequestItemFlag()) {
				calculateMaterialRequestItemIssueQty(pivasWorklist);
			}
			if (calculatePivasInfoCriteria.getChangeSplitCountFlag()) {
				calculateSplitDosageVolume(Arrays.asList(new PivasWorklist[]{pivasWorklist}));
				calculateNumOfLabel(pivasWorklist);
			} 
			if (calculatePivasInfoCriteria.getChangeAdjQtyFlag() || calculatePivasInfoCriteria.getChangeIssueQtyFlag()) {
				calculateNumOfLabel(pivasWorklist);			
				calculateDrugDispQty(pivasWorklist);
			} 
		}
		
		return pivasWorklist;
	}
	
	public PivasWorklistResponse updatePivasWorklist(PivasWorklist pivasWorklist, boolean addToWorklist, PivasWorklistType pivasWorklistType) {
		// refresh latest formula, formula method and vo for validation
		loadSavedPivasWorklist(Arrays.asList(new PivasWorklist[]{pivasWorklist}));

		// validate worklist
		validatePivasWorklist(Arrays.asList(new PivasWorklist[]{pivasWorklist}), true);
		
		if (pivasWorklist.getErrorCode() != null) {
			PivasWorklistResponse pivasWorklistResponse = new PivasWorklistResponse();
			pivasWorklistResponse.setErrorCode(pivasWorklist.getErrorCode());
			pivasWorklistResponse.setErrorParamList(pivasWorklist.getErrorParamList());
			pivasWorklistResponse.setErrorRemark(pivasWorklist.getErrorRemark());
			return pivasWorklistResponse;
		}
		
		// validate if add to worklist
		if (addToWorklist) {
			PivasWorklistResponse pivasWorklistResponse;
			pivasWorklistResponse = validateAddToWorklist(Arrays.asList(pivasWorklist));
			if (pivasWorklistResponse.getErrorCode() != null) {
				return pivasWorklistResponse;
			}
		}
		
		// save worklist
		doUpdatePivasWorklist(pivasWorklist, addToWorklist);

		// reload inbox and worklist tab of other workstation
		// (inbox tab and worklist tab cannot be reloaded by reload message, because active screen is edit screen and 
		//  front-end worklist view cannot receive reload message)
		pivasWorklistClientNotifier.dispatchUpdateEvent(workstore.getWorkstoreGroup().getWorkstoreGroupCode());
		
		return new PivasWorklistResponse();
	}
	
	private void doUpdatePivasWorklist(PivasWorklist pivasWorklist, boolean addToWorklist) {
		PivasPrep pivasPrep = pivasWorklist.getPivasPrep();
		MaterialRequest materialRequest = pivasWorklist.getMaterialRequest();
		
		pivasWorklist.setUpdateDate(new Date());	// force to set update date for optimistic lock

		// update status
		if (pivasWorklist.getIssueQty().add(pivasWorklist.getAdjQty()).compareTo(BigDecimal.valueOf(0)) <= 0) {
			// send PIVAS request and complete worklist if supply quantity = 0
			if (materialRequest != null) {
				for (MaterialRequestItem materialRequestItem : materialRequest.getMaterialRequestItemList()) {
					materialRequestItem.setStatus(MaterialRequestItemStatus.Outstanding);
				}
			}
			
			// update due date of PivasPrep to be searched for refill next time 
			// (for new worklist, because due date cannot be set by assign batch and print, due date of PivasPrep is null if not updated here)
			// (for refill worklist, due date of PivasPrep is same as nextDueDate of PivasWorklist and update is not necessary, but for simplicity update is also performed)
			if ( ! pivasWorklist.getAdHocFlag()) {
				pivasPrep.setDueDate(pivasWorklist.getNextDueDate());
			}
			
			pivasWorklist.setStatus(PivasWorklistStatus.Completed);
			
		} else if (addToWorklist) {
			pivasWorklist.setStatus(PivasWorklistStatus.Prepared);
		}

		// PivasPrep
		pivasPrep.setUpdateDate(new Date());	// force to set update date for optimistic lock
		
		if (pivasPrep.getId() != null) {
			PivasPrep findPivasPrep = em.find(PivasPrep.class, pivasPrep.getId());
			pivasPrep.setMedProfile(findPivasPrep.getMedProfile());
			pivasPrep.setMedProfileMoItem(findPivasPrep.getMedProfileMoItem());
			if (pivasWorklist.getAdHocFlag()) {
				// keep original latest PivasWorklist linkage for find patient extra worklist
				pivasPrep.setPivasWorklist(findPivasPrep.getPivasWorklist());
			}
			
			pivasPrep = em.merge(pivasPrep);
		} else {
			pivasPrep.setMedProfile(pivasWorklist.getMedProfile());
			pivasPrep.setMedProfileMoItem(pivasWorklist.getMedProfileMoItem());
			
			em.persist(pivasPrep);
		}

		// PivasPrepMethod
		PivasPrepMethod pivasPrepMethod = pivasWorklist.getPivasPrepMethod();
		if (pivasWorklist.getPivasPrepMethodId() != null) {
			PivasPrepMethod findPivasPrepMethod = em.find(PivasPrepMethod.class, pivasWorklist.getPivasPrepMethodId());
			if (pivasPrepMethod.compareTo(findPivasPrepMethod) != 0) {
				pivasPrepMethod.setId(null);
				for (PivasPrepMethodItem pivasPrepMethodItem : pivasPrepMethod.getPivasPrepMethodItemList()) {
					pivasPrepMethodItem.setId(null);
				}
				em.persist(pivasPrepMethod);
			} else {
				pivasPrepMethod = findPivasPrepMethod;
			}
		} else {
			em.persist(pivasPrepMethod);
		}
		
		// MaterialRequest
		if (materialRequest != null) {
			if (materialRequest.getId() != null) {
				MaterialRequest findMaterialRequest = em.find(MaterialRequest.class, materialRequest.getId());
				materialRequest.setMedProfile(findMaterialRequest.getMedProfile());
				materialRequest.setMedProfileMoItem(findMaterialRequest.getMedProfileMoItem());
				for (MaterialRequestItem materialRequestItem : materialRequest.getMaterialRequestItemList()) {
					MaterialRequestItem findMaterialRequestItem = em.find(MaterialRequestItem.class, materialRequestItem.getId());
					materialRequestItem.setMedProfilePoItem(findMaterialRequestItem.getMedProfilePoItem());
				}
				
				materialRequest = em.merge(materialRequest);
			} else {
				materialRequest.setMedProfile(pivasWorklist.getMedProfile());
				materialRequest.setMedProfileMoItem(pivasWorklist.getMedProfileMoItem());
				
				em.persist(materialRequest);
			}
			
		} else {
			MaterialRequest prevMaterialRequest = pivasWorklist.getPrevMaterialRequest();
			if (prevMaterialRequest != null && prevMaterialRequest.getId() != null) {
				for (MaterialRequestItem prevMaterialRequestItem : prevMaterialRequest.getMaterialRequestItemList()) {
					MaterialRequestItem findPrevMaterialRequestItem = em.find(MaterialRequestItem.class, prevMaterialRequestItem.getId());
					findPrevMaterialRequestItem.setStatus(MaterialRequestItemStatus.SysDeleted);
				}
			}
		}
		
		// first dose MaterialRequest
		MaterialRequest firstDoseMaterialRequest = null;
		if (pivasWorklist.getFirstDoseMaterialRequestId() != null) {
			firstDoseMaterialRequest = em.find(MaterialRequest.class, pivasWorklist.getFirstDoseMaterialRequestId());
		}
		
		// PivasWorklist
		pivasWorklist.setPivasPrep(pivasPrep);
		pivasWorklist.setPivasPrepMethod(pivasPrepMethod);
		pivasWorklist.setMaterialRequest(materialRequest);
		pivasWorklist.setFirstDoseMaterialRequest(firstDoseMaterialRequest);
		if (pivasWorklist.getId() != null) {
			pivasWorklist = em.merge(pivasWorklist);
		} else {
			em.persist(pivasWorklist);
		}
		
		em.flush();		
		
		if ( ! pivasWorklist.getAdHocFlag()) {
			// do not update latest PivasWorklist linkage for find patient extra worklist, so as to keep original latest PivasWorklist linkage
			pivasPrep.setPivasWorklist(pivasWorklist);
		}
		pivasPrepMethod.setPivasPrepId(pivasPrep.getId());
	}
	
	private List<PivasWorklist> retrieveNewPivasWorklistList(Date searchSupplyDate) {
    	StringBuilder sqlSb = new StringBuilder();
    	
    	sqlSb.append("select o from PivasWorklist o") // 20160615 index check : PivasWorklist.workstoreGroupCode,status,supplyDate : I_PIVAS_WORKLIST_01
    			.append(" where o.status = :status")
    			.append(" and o.refillFlag = :refillFlag")
    			.append(" and o.workstoreGroupCode = :workstoreGroupCode")
    			// filter MedProfile and MedProfileMoItem
    			.append(PROFILE_AND_ITEM_QUERY);
		
    	if (searchSupplyDate != null) {
        	sqlSb.append(" and o.supplyDate = :supplyDate");
    	}

    	Query query = em.createQuery(sqlSb.toString())
				.setParameter("status", PivasWorklistStatus.New)
				.setParameter("refillFlag", Boolean.FALSE)
				.setParameter("workstoreGroupCode", workstore.getWorkstoreGroup().getWorkstoreGroupCode())
				.setParameter("pivasFlag", Boolean.TRUE)
				// patient status checking
				.setParameter("mpStatus", MedProfileStatus.Active)	
				.setParameter("mpHomeLeaveStatus", MedProfileStatus.HomeLeave)
				.setParameter("currentDate", new Date())
				.setParameter("frozenFlag", Boolean.FALSE)	
				.setParameter("transferFlag", Boolean.FALSE)	
				// item status checking
				.setParameter("mpiStatus", MedProfileItemStatus.Verified);
    	
    	if (searchSupplyDate != null) {
    		query.setParameter("supplyDate", searchSupplyDate);
    	}
    	
    	query.setHint(QueryHints.BATCH, "o.medProfile.medCase")
				.setHint(QueryHints.BATCH, "o.medProfile.patient")
				.setHint(QueryHints.FETCH, "o.medProfileMoItem.medProfileItem")
				.setHint(QueryHints.BATCH, "o.pivasPrepMethod.pivasPrepMethodItemList")
				.setHint(QueryHints.BATCH, "o.materialRequest.materialRequestItemList")
				.setHint(QueryHints.BATCH, "o.pivasPrep");
    	
    	List<PivasWorklist> pivasWorklistList = query.getResultList();

		// create PivasPrep for unsaved PivasWorklist
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			if (pivasWorklist.getPivasPrep() == null) {
				PivasPrep pivasPrep = new PivasPrep();
				pivasWorklist.setPivasPrep(pivasPrep);
				
				PivasPrepMethod pivasPrepMethod = new PivasPrepMethod();
				pivasWorklist.setPivasPrepMethod(pivasPrepMethod);
				
				PivasPrepMethodItem pivasPrepMethodItem = new PivasPrepMethodItem();
				pivasPrepMethod.addPivasPrepMethodItem(pivasPrepMethodItem);
				
				pivasWorklist.setSavedFlag(Boolean.FALSE);
			} else {
				pivasWorklist.setSavedFlag(Boolean.TRUE);
			}
		}
		
		// init lazy binding
		linkPivasWorklist(pivasWorklistList);

		em.clear();

		// set reference and copy basic values to newly created worklist prep
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			if (pivasWorklist.getPivasPrep().getId() == null) {
				PivasPrep pivasPrep = pivasWorklist.getPivasPrep(); 
				pivasPrep.setWorkstoreGroup(workstore.getWorkstoreGroup());
				pivasPrep.setMedProfile(pivasWorklist.getMedProfile());
				pivasPrep.setMedProfileMoItem(pivasWorklist.getMedProfileMoItem());
				pivasPrep.setPivasFormulaId(pivasWorklist.getMedProfileMoItem().getPivasFormulaId());
			}
		}
		
		// filter out worklist
		pivasWorklistList = filterIpmoeWorklistByIpmoeWard(pivasWorklistList);
		
		pivasWorklistList = filterWorklistByPivasWard(pivasWorklistList);

		// unlink MedProfile and MedProfileItem association from PivasPrep
		unlinkPivasPrep(pivasWorklistList);
		
		return pivasWorklistList;
	}

	private List<PivasWorklist> retrieveRefillPivasWorklistList(Date searchSupplyDate, Date nextSupplyDate) {
		List<PivasPrep> rawPivasPrepList = em.createQuery(
				"select o from PivasPrep o" + // 20160809 index check : PivasPrep.workstoreGroup,status,dueDate : I_PIVAS_PREP_01
				" where o.dueDate <= :nextSupplyDate" +
				" and ((o.medProfileMoItem.endDate is null and o.calEndDate is null)" +
				// to obtain worklist which issue qty is changed to less than calculated issue qty, cannot check endDate <= next supply date 
				" or o.dueDate <= o.medProfileMoItem.endDate" +	  
				" or o.dueDate <= o.calEndDate)" +
				" and o.status in :status" +
				" and o.pivasWorklist.status in :pivasWorklistStatus" +
				" and o.singleDispFlag = :singleDispFlag" +
				" and o.workstoreGroup = :workstoreGroup" +
    			// filter MedProfile and MedProfileMoItem
    			PROFILE_AND_ITEM_QUERY)
				.setParameter("nextSupplyDate", nextSupplyDate)
				.setParameter("status", RecordStatus.Not_Delete)	
				.setParameter("pivasWorklistStatus", Arrays.asList(PivasWorklistStatus.Refill, PivasWorklistStatus.Completed))	
				.setParameter("singleDispFlag", Boolean.FALSE)
				.setParameter("workstoreGroup", workstore.getWorkstoreGroup())
				.setParameter("pivasFlag", Boolean.TRUE)	
				// patient status checking
				.setParameter("mpStatus", MedProfileStatus.Active)	
				.setParameter("mpHomeLeaveStatus", MedProfileStatus.HomeLeave)
				.setParameter("currentDate", new Date())
				.setParameter("frozenFlag", Boolean.FALSE)	
				.setParameter("transferFlag", Boolean.FALSE)	
				// item status checking
				.setParameter("mpiStatus", MedProfileItemStatus.Verified)
				.setHint(QueryHints.BATCH, "o.medProfile.medCase")
				.setHint(QueryHints.BATCH, "o.medProfile.patient")
				.setHint(QueryHints.FETCH, "o.medProfileMoItem.medProfileItem")
				.setHint(QueryHints.BATCH, "o.pivasWorklist.medProfile.medCase")
				.setHint(QueryHints.BATCH, "o.pivasWorklist.medProfile.patient")
				.setHint(QueryHints.FETCH, "o.pivasWorklist.medProfileMoItem.medProfileItem")
				.setHint(QueryHints.BATCH, "o.pivasWorklist.pivasPrepMethod.pivasPrepMethodItemList")
				.setHint(QueryHints.BATCH, "o.pivasWorklist.materialRequest.materialRequestItemList")
				.setHint(QueryHints.BATCH, "o.pivasWorklist.pivasPrep")
				.getResultList();

		// create PivasWorklist for unsaved refill worklist, and get the saved worklist
		List<PivasWorklist> pivasWorklistList = new ArrayList<PivasWorklist>();
		Map<Long, Long> pivasWorklistIdByPivasPrepIdMap = new HashMap<Long, Long>();
		Map<Long, Long> pivasPrepMethodIdByPivasPrepIdMap = new HashMap<Long, Long>();

		for (PivasPrep rawPivasPrep : rawPivasPrepList) {
			if (rawPivasPrep.getPivasWorklist().getStatus() == PivasWorklistStatus.Refill) {
				// if previous worklist is edited refill worklist, this time the worklist is init again just because supply date != search supply date,
				// so the prevPivasWorklistId is already set correctly before and this time only need to copy the saved prevPivasWorklistId
				pivasWorklistIdByPivasPrepIdMap.put(rawPivasPrep.getId(), rawPivasPrep.getPivasWorklist().getPrevPivasWorklistId());
			} else {
				// if previous worklist is already not refill, need to point prevPivasWorklistId to id of previous new/refill worklist
				pivasWorklistIdByPivasPrepIdMap.put(rawPivasPrep.getId(), rawPivasPrep.getPivasWorklist().getId());
			}
			pivasPrepMethodIdByPivasPrepIdMap.put(rawPivasPrep.getId(), rawPivasPrep.getPivasWorklist().getPivasPrepMethodId());
			
			if (rawPivasPrep.getPivasWorklist().getStatus() == PivasWorklistStatus.Refill) {
				pivasWorklistList.add(rawPivasPrep.getPivasWorklist());
			} else {
				boolean includeWorklist = false;
				
				Date prevSupplyDate = rawPivasPrep.getPivasWorklist().getSupplyDate();
				Date prevExpiryDate = rawPivasPrep.getPivasWorklist().getExpiryDate();
				Date prevNextDueDate = rawPivasPrep.getDueDate();

				if (searchSupplyDate.compareTo(prevSupplyDate) != 0) {
					includeWorklist = true;
				} else {
					// when search supply date is same as worklist prev supply date, usually should filter out the worklist
					// so that user cannot process it again
					// (but when user change PIVAS issue quantity to less than the calculated value, the next due date is adjusted
					//  to before the calculated next due date. In this case, user is allowed to search by same supply date and
					//  replenish the remaining issue quantity, so need to include worklist of such case)
					if (prevExpiryDate.compareTo(nextSupplyDate) >= 0) {
						// when prev expiry date >= next supply date, all quantity is issued by PIVAS and no PIVAS request, that means
						// normally prev next due date > next supply date
						// but in query only prev next due date <= next supply date worklist is obtained, if any worklist hits this 
						// branch, its prev next due date is set to before or equal to next supply date by reducing issue quantity
						includeWorklist = true;
					} else if (prevNextDueDate.compareTo(prevExpiryDate) <= 0) {
						// when prev expiry date < next supply date, PIVAS request may occur, then prev next due date should jump to 
						// after prev expiry date
						// if any worklist hits this branch, its prev next due date is set to before or equal to prev expiry date by
						// reducing issue quantity
						includeWorklist = true;
					}
				}
				
				if (includeWorklist) {
					PivasWorklist pivasWorklist = new PivasWorklist();
					pivasWorklist.setPivasPrep(rawPivasPrep);
					pivasWorklistList.add(pivasWorklist);
				}
			}
		}

		// init save flag
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			boolean savedRefillFlag = true;
			if (pivasWorklist.getStatus() != PivasWorklistStatus.Refill) {
				savedRefillFlag = false;
			} else if (pivasWorklist.getSupplyDate().compareTo(searchSupplyDate) != 0) {
				// all refill order with supply date not match with front-end search supply date is init again 
				savedRefillFlag = false;
			}
			
			if (savedRefillFlag) {
				pivasWorklist.setSavedFlag(Boolean.TRUE);
			} else {
				pivasWorklist.setSavedFlag(Boolean.FALSE);
			}
		}

		// init lazy binding
		linkPivasWorklist(pivasWorklistList);
		
		em.clear();

		// set reference and copy basic values to newly created worklist
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			if ( ! pivasWorklist.getSavedFlag()) {
				PivasPrep pivasPrep = pivasWorklist.getPivasPrep();

				// clear latest PivasWorklist linkage in PivasPrep, which will be reset to the new PivasWorklist in save
				pivasPrep.setPivasWorklist(null);

				PivasPrepMethod pivasPrepMethod = new PivasPrepMethod();	
				pivasWorklist.setPivasPrepMethod(pivasPrepMethod);	// clear PivasPrepMethod
				PivasPrepMethodItem pivasPrepMethodItem = new PivasPrepMethodItem();	
				pivasPrepMethod.addPivasPrepMethodItem(pivasPrepMethodItem);	// clear PivasPrepMethodItem

				// init fields
				Long prevPivasWorklistId = pivasWorklistIdByPivasPrepIdMap.get(pivasPrep.getId());
				Long prevPivasPrepMethodId = pivasPrepMethodIdByPivasPrepIdMap.get(pivasPrep.getId());
				
				pivasWorklist.setPrevPivasWorklistId(prevPivasWorklistId);
				pivasWorklist.setPivasPrepMethodId(prevPivasPrepMethodId);
				pivasWorklist.setStatus(PivasWorklistStatus.Refill);
				pivasWorklist.setRefillFlag(Boolean.TRUE);
				pivasWorklist.setDueDate(pivasWorklist.getPivasPrep().getDueDate());
				pivasWorklist.setWorkstoreGroupCode(workstore.getWorkstoreGroup().getWorkstoreGroupCode());
				pivasWorklist.setWorkstore(workstore);
				pivasWorklist.setMedProfile(pivasWorklist.getPivasPrep().getMedProfile());
				pivasWorklist.setMedProfileMoItem(pivasWorklist.getPivasPrep().getMedProfileMoItem());
				pivasWorklist.setAdjQty(BigDecimal.ZERO);
			}
		}		
		
		// filter out list which does not display in front-end
		pivasWorklistList = filterIpmoeWorklistByIpmoeWard(pivasWorklistList);
		
		pivasWorklistList = filterWorklistByPivasWard(pivasWorklistList);
		
		// unlink MedProfile and MedProfileItem association from PivasPrep
		unlinkPivasPrep(pivasWorklistList);
		
		return pivasWorklistList;
	}
	
	private List<PivasWorklist> retrievePreparedPivasWorklist(boolean loadInfo) {
		List<PivasWorklist> pivasWorklistList = em.createQuery(
				"select o from PivasWorklist o" + // 20160616 index check : PivasWorklist.workstoreGroupCode,status : I_PIVAS_WORKLIST_01
				" where o.status = :status" +
				" and o.workstoreGroupCode = :workstoreGroupCode" +
    			// filter MedProfile and MedProfileMoItem
    			PROFILE_AND_ITEM_QUERY)
				.setParameter("status", PivasWorklistStatus.Prepared)
				.setParameter("workstoreGroupCode", workstore.getWorkstoreGroup().getWorkstoreGroupCode())
				.setParameter("pivasFlag", Boolean.TRUE)	
				// patient status checking
				.setParameter("mpStatus", MedProfileStatus.Active)	
				.setParameter("mpHomeLeaveStatus", MedProfileStatus.HomeLeave)
				.setParameter("currentDate", new Date())
				.setParameter("frozenFlag", Boolean.FALSE)	
				.setParameter("transferFlag", Boolean.FALSE)	
				// item status checking
				.setParameter("mpiStatus", MedProfileItemStatus.Verified)
				.setHint(QueryHints.BATCH, "o.medProfile.medCase")
				.setHint(QueryHints.BATCH, "o.medProfile.patient")
				.setHint(QueryHints.FETCH, "o.medProfileMoItem.medProfileItem")
				.setHint(QueryHints.BATCH, "o.pivasPrepMethod.pivasPrepMethodItemList")
				.setHint(QueryHints.BATCH, "o.materialRequest.materialRequestItemList")
				.setHint(QueryHints.BATCH, "o.pivasPrep")
				.getResultList();

		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			pivasWorklist.setSavedFlag(Boolean.TRUE);
		}

		// init lazy binding
		if (loadInfo) {
			linkPivasWorklist(pivasWorklistList);
		}
		
		em.clear();

		// filter worklist
		pivasWorklistList = filterIpmoeWorklistByIpmoeWard(pivasWorklistList);
		
		pivasWorklistList = filterWorklistByPivasWard(pivasWorklistList);
		
		// note: this supply date filter must be put at the end, otherwise following error scenario may occur:
		//       if earliest date worklist is already filtered out, then all of those worklist are filtered
		//       out by another filtering criteria, then final worklist list is empty, but actually worklist 
		//       with second earliest date should be included 
		pivasWorklistList = filterWorklistBySupplyDate(pivasWorklistList);
		
		// unlink MedProfile and MedProfileItem association from PivasPrep
		unlinkPivasPrep(pivasWorklistList);
		
		return pivasWorklistList;
	}

	private List<PivasWorklist> retrievePivasWorklistListByHkidCaseNum(String hkidCaseNum) {
		// retrive all worklist of the patient which vetting end date is passed 
		List<PivasPrep> rawPivasPrepList = em.createQuery(
				"select o from PivasPrep o" + // 20160616 index check : PivasPrep.workstoreGroup,status MedCase.caseNum Patient.hkid : I_PIVAS_PREP_01 I_MED_CASE_01 I_PATIENT_02
				" where o.status in :status" +
				" and (o.medProfile.medCase.caseNum = :hkidCaseNum" +
				" or o.medProfile.patient.hkid = :hkidCaseNum)" +
				" and o.workstoreGroup = :workstoreGroup" +
    			// filter MedProfile and MedProfileMoItem
    			PROFILE_AND_ITEM_QUERY)
				.setParameter("status", RecordStatus.Not_Delete)	
				.setParameter("hkidCaseNum", hkidCaseNum)
				.setParameter("workstoreGroup", workstore.getWorkstoreGroup())
				.setParameter("pivasFlag", Boolean.TRUE)
				// patient status checking
				.setParameter("mpStatus", MedProfileStatus.Active)	
				.setParameter("mpHomeLeaveStatus", MedProfileStatus.HomeLeave)
				.setParameter("currentDate", new Date())
				.setParameter("frozenFlag", Boolean.FALSE)	
				.setParameter("transferFlag", Boolean.FALSE)	
				// item status checking
				.setParameter("mpiStatus", MedProfileItemStatus.Verified)
				.setHint(QueryHints.BATCH, "o.medProfile.medCase")
				.setHint(QueryHints.BATCH, "o.medProfile.patient")
				.setHint(QueryHints.FETCH, "o.medProfileMoItem.medProfileItem")
				.setHint(QueryHints.BATCH, "o.pivasWorklist.medProfile.medCase")
				.setHint(QueryHints.BATCH, "o.pivasWorklist.medProfile.patient")
				.setHint(QueryHints.FETCH, "o.pivasWorklist.medProfileMoItem.medProfileItem")
				.setHint(QueryHints.BATCH, "o.pivasWorklist.pivasPrepMethod.pivasPrepMethodItemList")
				.setHint(QueryHints.BATCH, "o.pivasWorklist.materialRequest.materialRequestItemList")
				.setHint(QueryHints.BATCH, "o.pivasWorklist.pivasPrep")
				.getResultList();

		// create extra PivasWorklist for all (unsaved or saved) (new, refill, prepared, completed) worklist
		List<PivasWorklist> pivasWorklistList = new ArrayList<PivasWorklist>();
		
		Map<Long, PivasWorklistStatus> pivasWorklistStatusByPivasPrepIdMap = new HashMap<Long, PivasWorklistStatus>();
		Map<Long, Long> pivasWorklistIdByPivasPrepIdMap = new HashMap<Long, Long>();
		Map<Long, Long> pivasPrepMethodIdByPivasPrepIdMap = new HashMap<Long, Long>();
		Map<Long, Date> dueDateByPivasPrepIdMap = new HashMap<Long, Date>();

		for (PivasPrep rawPivasPrep : rawPivasPrepList) {
			pivasWorklistStatusByPivasPrepIdMap.put(rawPivasPrep.getId(), rawPivasPrep.getPivasWorklist().getStatus());
			pivasWorklistIdByPivasPrepIdMap.put(rawPivasPrep.getId(), rawPivasPrep.getPivasWorklist().getId());
			pivasPrepMethodIdByPivasPrepIdMap.put(rawPivasPrep.getId(), rawPivasPrep.getPivasWorklist().getPivasPrepMethodId());
			dueDateByPivasPrepIdMap.put(rawPivasPrep.getId(), rawPivasPrep.getPivasWorklist().getDueDate());
			
			PivasWorklist pivasWorklist = new PivasWorklist();
			pivasWorklist.setPivasPrep(rawPivasPrep);
			pivasWorklistList.add(pivasWorklist);
			
			pivasWorklist.setSavedFlag(Boolean.FALSE);
		}

		// init lazy binding
		linkPivasWorklist(pivasWorklistList);
		
		em.clear();

		// set reference and copy basic values to newly created worklist
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			PivasPrep pivasPrep = pivasWorklist.getPivasPrep();
			
			// clear latest PivasWorklist linkage in PivasPrep, which will be reset to the new PivasWorklist in save
			pivasPrep.setPivasWorklist(null);

			PivasPrepMethod pivasPrepMethod = new PivasPrepMethod();	
			pivasWorklist.setPivasPrepMethod(pivasPrepMethod);	// clear PivasPrepMethod
			PivasPrepMethodItem pivasPrepMethodItem = new PivasPrepMethodItem();	
			pivasPrepMethod.addPivasPrepMethodItem(pivasPrepMethodItem);	// clear PivasPrepMethodItem

			// init fields
			PivasWorklistStatus prevPivasWorklistStatus = pivasWorklistStatusByPivasPrepIdMap.get(pivasPrep.getId());
			Long prevPivasWorklistId = pivasWorklistIdByPivasPrepIdMap.get(pivasPrep.getId());
			Long prevPivasPrepMethodId = pivasPrepMethodIdByPivasPrepIdMap.get(pivasPrep.getId());
			Date prevDueDate = dueDateByPivasPrepIdMap.get(pivasPrep.getId());
			
			pivasWorklist.setAdHocFlag(Boolean.TRUE);
			if (prevPivasWorklistStatus != PivasWorklistStatus.Completed) {
				pivasWorklist.setAllowEditFlag(Boolean.FALSE);
			}
			
			pivasWorklist.setPrevPivasWorklistId(prevPivasWorklistId);
			pivasWorklist.setPivasPrepMethodId(prevPivasPrepMethodId);
			pivasWorklist.setRefillFlag(Boolean.FALSE);	// force validation of due date against supply date and current date to follow new worklist logic
			pivasWorklist.setDueDate(prevDueDate);
			
			pivasWorklist.setWorkstoreGroupCode(workstore.getWorkstoreGroup().getWorkstoreGroupCode());
			pivasWorklist.setWorkstore(workstore);
			pivasWorklist.setMedProfile(pivasWorklist.getPivasPrep().getMedProfile());
			pivasWorklist.setMedProfileMoItem(pivasWorklist.getPivasPrep().getMedProfileMoItem());
		}		
		
		// filter out list
		pivasWorklistList = filterIpmoeWorklistByIpmoeWard(pivasWorklistList);
		
		pivasWorklistList = filterWorklistByPivasWard(pivasWorklistList);
		
		// unlink MedProfile and MedProfileItem association from PivasPrep
		unlinkPivasPrep(pivasWorklistList);
		
		return pivasWorklistList;
	}
	
	private void linkPivasWorklist(List<PivasWorklist> pivasWorklistList) {
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			pivasWorklist.getPivasPrep();
			// no need to add batch join query hint, because same MedProfileMoItem is already loaded by PivasWorklist
			pivasWorklist.getPivasPrepMethod();
			if (pivasWorklist.getPivasPrepMethod() != null && pivasWorklist.getPivasPrepMethod().getPivasPrepMethodItemList() != null) {
				pivasWorklist.getPivasPrepMethod().getPivasPrepMethodItemList().size();
			}

			if (pivasWorklist.getMedProfile() != null) {
				pivasWorklist.getMedProfile().getMedCase();
				pivasWorklist.getMedProfile().getPatient();
			} 
			if (pivasWorklist.getPivasPrep().getMedProfile() != null) {
				// note: MedProfile of PivasPrep will be unlinked later in retrieve because they are not used in front-end,
				//       they will be linked again during save
				pivasWorklist.getPivasPrep().getMedProfile().getMedCase();
				pivasWorklist.getPivasPrep().getMedProfile().getPatient();
			}
			if (pivasWorklist.getMedProfileMoItem() != null) {
				pivasWorklist.getMedProfileMoItem().getMedProfileItem();
				pivasWorklist.getMedProfileMoItem().loadDmInfo();
			} 
			if (pivasWorklist.getPivasPrep().getMedProfileMoItem() != null) {
				// note: MedProfileMoItem of PivasPrep will be unlinked later in retrieve because they are not used in front-end,
				//       they will be linked again during save
				pivasWorklist.getPivasPrep().getMedProfileMoItem().getMedProfileItem();
				pivasWorklist.getPivasPrep().getMedProfileMoItem().loadDmInfo();
			}
			
			if (pivasWorklist.getMaterialRequest() != null) {
				// note: MedProfile and MedProfileMoItem of MaterialRequest is not linked in retrieve because they are not used in front-end,
				//       they will be linked again during save
				if (pivasWorklist.getMaterialRequest().getMaterialRequestItemList() != null) {
					pivasWorklist.getMaterialRequest().getMaterialRequestItemList().size();
				}
			}
		}
	}
	
	private List<PivasWorklist> filterWorklistByPivasWard(List<PivasWorklist> rawPivasWorklistList) {
		List<PivasWorklist> pivasWorklistList = new ArrayList<PivasWorklist>();
		
		// obtain PIVAS ward map hashed by pat hosp code
		Set<String> patHospCodeSet = new HashSet<String>();
		for (PivasWorklist rawPivasWorklist : rawPivasWorklistList) {
			patHospCodeSet.add(rawPivasWorklist.getMedProfile().getPatHospCode());
		}
		
		Map<String, Map<String, PivasWard>> pivasWardMapByPatHospCodeMap = new HashMap<String, Map<String, PivasWard>>();
		
		for (String patHospCode : patHospCodeSet) {
			List<PivasWard> pivasWardList = dmsPmsServiceProxy.retrievePivasWardList(patHospCode, workstore.getWorkstoreGroup().getWorkstoreGroupCode());
			
			Map<String, PivasWard> pivasWardByPasWardCodeMap = new HashMap<String, PivasWard>(); 
			for (PivasWard pivasWard : pivasWardList) {
				pivasWardByPasWardCodeMap.put(pivasWard.getCompId().getWard(), pivasWard);
			}
			
			pivasWardMapByPatHospCodeMap.put(patHospCode, pivasWardByPasWardCodeMap);
		}
		
		// filter worklist which PAS ward code is not in PIVAS ward
		for (PivasWorklist rawPivasWorklist : rawPivasWorklistList) {
			MedProfile medProfile = rawPivasWorklist.getMedProfile();
			
			Map<String, PivasWard> pivasWardByPasWardCodeMap = pivasWardMapByPatHospCodeMap.get(medProfile.getPatHospCode());
			
			if ( medProfile.getMedCase().getCaseNum() != null && pivasWardByPasWardCodeMap.containsKey(medProfile.getMedCase().getPasWardCode()) ) {
				pivasWorklistList.add(rawPivasWorklist);
			}
		}
		
		return pivasWorklistList;
	}
	
	private List<PivasWorklist> filterIpmoeWorklistByIpmoeWard(List<PivasWorklist> rawPivasWorklistList) {
		List<PivasWorklist> pivasWorklistList = new ArrayList<PivasWorklist>();
		
		// obtain IPMOE ward map hashed by pat hosp code
		Set<String> patHospCodeSet = new HashSet<String>();
		for (PivasWorklist rawPivasWorklist : rawPivasWorklistList) {
			if ( ! rawPivasWorklist.getMedProfileMoItem().isManualItem()) {
				patHospCodeSet.add(rawPivasWorklist.getMedProfile().getPatHospCode());
			}
		}
		List<String> patHospCodeList = new ArrayList<String>();
		patHospCodeList.addAll(patHospCodeSet);
		
		Map<String, List<String>> activeWardListByPatHospCodeMap = activeWardManager.retrieveActiveWardMapByPatHospCodeList(patHospCodeList);
		
		Map<String, Set<String>> activeWardSetByPatHospCodeMap = new HashMap<String, Set<String>>();
		for (String patHospCode : activeWardListByPatHospCodeMap.keySet()) {
			List<String> activeWardList = activeWardListByPatHospCodeMap.get(patHospCode);
			Set<String> activeWardSet = new HashSet<String>();
			activeWardSet.addAll(activeWardList);
			activeWardSetByPatHospCodeMap.put(patHospCode, activeWardSet);
		}
		
		// filter IPMOE worklist which PAS ward code is not in IPMOE ward
		for (PivasWorklist rawPivasWorklist : rawPivasWorklistList) {
			if ( ! rawPivasWorklist.getMedProfileMoItem().isManualItem()) {
				Set<String> activeWardSet = activeWardSetByPatHospCodeMap.get(rawPivasWorklist.getMedProfile().getPatHospCode());
				if (activeWardSet.contains(rawPivasWorklist.getMedProfile().getMedCase().getPasWardCode())) {
					pivasWorklistList.add(rawPivasWorklist);
				}
			} else {
				// does not filter non-IPMOE worklist
				pivasWorklistList.add(rawPivasWorklist);
			}
		}
		
		return pivasWorklistList;
	}
	
	private List<PivasWorklist> filterWorklistBySupplyDate(List<PivasWorklist> rawPivasWorklistList) {
		// (used by worklist tab only) 
		// filter worklist and keep only worklist with earliest supply date 
		// (when some worklist is transfer/discharge and then cancel transfer/cancel discharge, the supply date of
		//  those worklist may not match the supply date of original worklist of worklist tab
		//  then multiple supply date occurs and worklist not of earliest supply date is filtered out to keep single supply date) 
		List<PivasWorklist> pivasWorklistList = new ArrayList<PivasWorklist>();
		
		if (rawPivasWorklistList.isEmpty()) {
			return pivasWorklistList;
		}
		
		Date earliestSupplyDate = rawPivasWorklistList.get(0).getSupplyDate();
		for (PivasWorklist rawPivasWorklist : rawPivasWorklistList) {
			if (rawPivasWorklist.getSupplyDate().compareTo(earliestSupplyDate) < 0) {
				earliestSupplyDate = rawPivasWorklist.getSupplyDate();
			}
		}
		
		for (PivasWorklist rawPivasWorklist : rawPivasWorklistList) {
			if (rawPivasWorklist.getSupplyDate().equals(earliestSupplyDate)) {
				pivasWorklistList.add(rawPivasWorklist);
			}
		}
		
		return pivasWorklistList;
	}
	
	private void unlinkPivasPrep(List<PivasWorklist> pivasWorklistList) {
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			PivasPrep pivasPrepCopy = new PivasPrep();
			BeanUtils.copyProperties(pivasWorklist.getPivasPrep(), pivasPrepCopy, new String[]{"medProfile", "medProfileMoItem"});
			pivasWorklist.setPivasPrep(pivasPrepCopy);
		}
	}
	
	private void initAndLoadUnsavedAndSavedPivasWorklist(List<PivasWorklist> pivasWorklistList, Date searchSupplyDate) {
		List<PivasWorklist> unsavedPivasWorklistList = new ArrayList<PivasWorklist>();
		List<PivasWorklist> savedPivasWorklistList = new ArrayList<PivasWorklist>();
		
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			if ( ! pivasWorklist.getSavedFlag()) {
				unsavedPivasWorklistList.add(pivasWorklist);
			} else {
				savedPivasWorklistList.add(pivasWorklist);
			}
		}
		
		// init unsaved worklist
		initAndLoadUnsavedPivasWorklist(unsavedPivasWorklistList, searchSupplyDate);
		
		// init saved worklist
		loadSavedPivasWorklist(savedPivasWorklistList);
	}

	private void initAndLoadUnsavedPivasWorklist(List<PivasWorklist> unsavedPivasWorklistList, Date searchSupplyDate) {
		loadPivasFormulaVo(unsavedPivasWorklistList, true);
		
		initCorePivasFormulaMethodAndLoadVo(unsavedPivasWorklistList);
		
		loadMedProfilePoItemListForManualProfile(unsavedPivasWorklistList);

		for (PivasWorklist unsavedPivasWorklist : unsavedPivasWorklistList) {
			if ( ! unsavedPivasWorklist.getRefillFlag()) {
				initNewPivasWorklist(unsavedPivasWorklist);
			} else {
				initRefillPivasWorklist(unsavedPivasWorklist, searchSupplyDate);
			}
			
			// init drug item code, manufacturer code, warning code and drug disp qty
			initPrepMethodDrug(unsavedPivasWorklist);
		}

		loadPrepMethodDrugInfoDrugVo(unsavedPivasWorklistList);
		
		// init solvent/diluent item code and manufacturer code (and load solvent/diluent manufacturer vo) 
		initPrepMethodSolventDiluentInfoAndLoadManufVo(unsavedPivasWorklistList);

		// calculate PIVAS information for unsaved worklist
		List<Long> medProfileMoItemIdList = new ArrayList<Long>();
		for (PivasWorklist unsavedPivasWorklist : unsavedPivasWorklistList) {
			medProfileMoItemIdList.add(unsavedPivasWorklist.getMedProfileMoItem().getId());
		}
		Map<Long, List<MedProfilePoItem>> mrMedProfilePoItemListMap = retrieveMrMedProfilePoItemListMap(medProfileMoItemIdList);
		
		for (PivasWorklist unsavedPivasWorklist : unsavedPivasWorklistList) {
			List<MedProfilePoItem> mrMedProfilePoItemList = mrMedProfilePoItemListMap.get(unsavedPivasWorklist.getMedProfileMoItem().getId());
			calculatePivasInfo(false, unsavedPivasWorklist, mrMedProfilePoItemList, true, true);
		}		
	}
	
	private void loadSavedPivasWorklist(List<PivasWorklist> savedPivasWorklistList) {
		loadPivasFormulaVo(savedPivasWorklistList, false);
		
		loadPivasFormulaMethodVoForSavedWorklist(savedPivasWorklistList);
		
		loadMedProfilePoItemListForManualProfile(savedPivasWorklistList);

		loadPrepMethodDrugInfoDrugVo(savedPivasWorklistList);
		
		loadPrepMethodSolventDiluentInfoManufVoForSavedWorklist(savedPivasWorklistList);
		
		loadSatelliteEnabledForSavedWorklist(savedPivasWorklistList);
		
		calculateSplitDosageVolume(savedPivasWorklistList);
		
		calculateNextSupplyDateForSavedWorklist(savedPivasWorklistList);
	}
	
	private void initAndLoadFindPatientPivasWorklist(List<PivasWorklist> adHocPivasWorklistList) {
		// note: all find patient extra worklist are saved worklist, because unsaved non-refill worklist is not retrieved in find patient,
		//       and unsaved refill worklist is still in Complete status and cannot be retrieved as refill worklist
		
		loadPivasFormulaVo(adHocPivasWorklistList, true);
		
		initCorePivasFormulaMethodAndLoadVo(adHocPivasWorklistList);
		
		loadMedProfilePoItemListForManualProfile(adHocPivasWorklistList);

		Date adHocSupplyDate = calculateSupplyDateForAdHocWorklist();
		
		for (PivasWorklist adHocPivasWorklist : adHocPivasWorklistList) {
			initAdHocPivasWorklist(adHocPivasWorklist, adHocSupplyDate);
			
			// init drug item code, manufacturer code, warning code and drug disp qty
			initPrepMethodDrug(adHocPivasWorklist);
		}

		loadPrepMethodDrugInfoDrugVo(adHocPivasWorklistList);
		
		// init solvent/diluent item code and manufacturer code (and load solvent/diluent manufacturer vo) 
		initPrepMethodSolventDiluentInfoAndLoadManufVo(adHocPivasWorklistList);
		
		for (PivasWorklist adHocPivasWorklist : adHocPivasWorklistList) {
			initPivasWorklistBySupplyDateChange(adHocPivasWorklist, true);
			
			calculateDrugCalQty(adHocPivasWorklist);
			calculateDrugDispQty(adHocPivasWorklist);
		}
		
		calculateSplitDosageVolume(adHocPivasWorklistList);
	}
	
	private Date calculateSupplyDateForAdHocWorklist() {
		Date supplyDate = null;
		
		List<PivasWorklist> addedPivasWorklistList = retrievePreparedPivasWorklist(false);
		
		if ( ! addedPivasWorklistList.isEmpty()) {
			PivasWorklist firstAddedPivasWorklist = addedPivasWorklistList.get(0);
			supplyDate = firstAddedPivasWorklist.getSupplyDate();
		} else {
			supplyDate = calculateTodayDefaultSupplyDate();
		}
		
		return supplyDate;
	}
	
	private Date calculateTodayDefaultSupplyDate() {
		Map<String, List<PivasServiceHour>> pivasServiceHourListByDayOfWeekMap = pivasServiceHourManager.retrievePivasServiceHourListByDayOfWeekMap(workstore.getWorkstoreGroup().getWorkstoreGroupCode(), workstore.getHospital());
		
		Date today = new Date();
		Date todayWithoutTime = DateUtils.truncate(today, Calendar.DAY_OF_MONTH);

		Set<Date> pivasHolidayDateSet = pivasServiceHourManager.retrievePivasHolidayDateSet(workstore.getWorkstoreGroup().getWorkstoreGroupCode());
		
		if ( ! pivasHolidayDateSet.contains(todayWithoutTime)) {
			int dayOfWeekValue = new DateTime(today).getDayOfWeek();
		
			List<PivasServiceHour> pivasServiceHourList = pivasServiceHourListByDayOfWeekMap.get(String.valueOf(dayOfWeekValue));
			
			if (pivasServiceHourList != null) {
				Integer supplyBufferValue = PIVAS_SUPPLYDATE_BUFFER.get(workstore.getHospital());
				
				int hour = new DateTime(today).getHourOfDay();
				int minute = new DateTime(today).getMinuteOfHour();
				int todayTime = hour * 100 + minute;
				int supplyBufferTime = supplyBufferValue * 100;
				
				PivasServiceHour foundPivasServiceHour = null;
				for (PivasServiceHour pivasServiceHour : pivasServiceHourList) {
					if (todayTime <= pivasServiceHour.getSupplyTime() + supplyBufferTime) {
						foundPivasServiceHour = pivasServiceHour;
						break;
					}
				}
				
				if (foundPivasServiceHour == null) {
					// set to last supply time if not match
					foundPivasServiceHour = pivasServiceHourList.get(pivasServiceHourList.size()-1);
				}
				
				Integer supplyTime = foundPivasServiceHour.getSupplyTime();
				int supplyHour = supplyTime / 100;
				int supplyMinutes = supplyTime % 100;
				return new DateTime(todayWithoutTime).plusHours(supplyHour).plusMinutes(supplyMinutes).toDate();
			}
		}
		
		return null;
	}
	
	private void loadPivasFormulaVo(List<PivasWorklist> pivasWorklistList, boolean initPivasPrepByFormula) {
		// load formula vo
		// (formula not exists for unsaved IPMOE order when CMS does not set formula)
		Set<Long> pivasFormulaIdSet = new HashSet<Long>();
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			if (pivasWorklist.getPivasPrep().getPivasFormulaId() != null) {
				pivasFormulaIdSet.add(pivasWorklist.getPivasPrep().getPivasFormulaId());
			}
		}
		List<Long> pivasFormulaIdList = new ArrayList<Long>();
		pivasFormulaIdList.addAll(pivasFormulaIdSet);
		
		List<PivasFormula> pivasFormulaList = new ArrayList<PivasFormula>();
		List<PivasFormulaMethod> corePivasFormulaMethodList = new ArrayList<PivasFormulaMethod>();
		if ( ! pivasFormulaIdList.isEmpty()) {
			pivasFormulaList = dmsPmsServiceProxy.retrievePivasFormulaListByPivasFormulaId(workstore.getHospCode(), pivasFormulaIdList);
			corePivasFormulaMethodList = dmsPmsServiceProxy.retrieveCorePivasFormulaMethodListByPivasFormulaId(workstore.getHospCode(), pivasFormulaIdList);
		}
		
		Map<Long, PivasFormula> pivasFormulaByIdMap = new HashMap<Long, PivasFormula>();
		for (PivasFormula pivasFormula : pivasFormulaList) {
			pivasFormulaByIdMap.put(pivasFormula.getId(), pivasFormula);
		}

		Map<Long, PivasFormulaMethod> corePivasFormulaMethodByIdMap = new HashMap<Long, PivasFormulaMethod>();
		for (PivasFormulaMethod corePivasFormulaMethod : corePivasFormulaMethodList) {
			corePivasFormulaMethodByIdMap.put(corePivasFormulaMethod.getPivasFormulaId(), corePivasFormulaMethod);
		}
		
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			Long pivasFormulaId = pivasWorklist.getPivasPrep().getPivasFormulaId();
			
			if (pivasFormulaId != null && pivasFormulaByIdMap.containsKey(pivasFormulaId)) {
				PivasFormula matchPivasFormula = pivasFormulaByIdMap.get(pivasFormulaId);
				
				// core formula method is needed to display volume and diluent volume in formula desc
				PivasFormulaMethod matchCorePivasFormulaMethod = corePivasFormulaMethodByIdMap.get(pivasFormulaId);
				List<PivasFormulaMethod> tempPivasFormulaMethodList = new ArrayList<PivasFormulaMethod>();
				tempPivasFormulaMethodList.add(matchCorePivasFormulaMethod);
				matchPivasFormula.setPivasFormulaMethodList(tempPivasFormulaMethodList);
				
				pivasWorklist.getPivasPrep().setPivasFormula(matchPivasFormula);
			}
		}

		if (initPivasPrepByFormula) {
			// init fields and vo of PivasPrep by formula
			for (PivasWorklist pivasWorklist : pivasWorklistList) {
				PivasPrep pivasPrep = pivasWorklist.getPivasPrep();
				
				if (pivasPrep.getPivasFormula() != null) {
					pivasPrep.setPrintName(pivasPrep.getPivasFormula().getPrintName());
					pivasPrep.setPivasContainerId(pivasPrep.getPivasFormula().getPivasContainer().getId());
					pivasPrep.setPivasContainer(pivasPrep.getPivasFormula().getPivasContainer());
					if ( ! pivasWorklist.getRefillFlag() && ! pivasWorklist.getAdHocFlag()) {
						pivasPrep.setUpperDoseLimit(pivasPrep.getPivasFormula().getUpperDoseLimit());
					}
					pivasPrep.setLabelOption(pivasPrep.getPivasFormula().getLabelOption());
					pivasPrep.setLabelOptionType(pivasPrep.getPivasFormula().getLabelOptionType());
					// shelf life is set by recal
				}
			}
		} else {
			// load vo of PivasPrep by formula
			Set<Long> pivasContainerIdSet = new HashSet<Long>();
			for (PivasWorklist pivasWorklist : pivasWorklistList) {
				pivasContainerIdSet.add(pivasWorklist.getPivasPrep().getPivasContainerId());
			}
			List<Long> pivasContainerIdList = new ArrayList<Long>();
			pivasContainerIdList.addAll(pivasContainerIdSet);

			List<PivasContainer> pivasContainerList = new ArrayList<PivasContainer>(); 
			if ( ! pivasContainerIdList.isEmpty()) {
				pivasContainerList = dmsPmsServiceProxy.retrievePivasContainerListByPivasContainerId(workstore.getWorkstoreGroup().getWorkstoreGroupCode(), pivasContainerIdList);
			}
			
			Map<Long, PivasContainer> pivasContainerByIdMap = new HashMap<Long, PivasContainer>();
			for (PivasContainer pivasContainer : pivasContainerList) {
				pivasContainerByIdMap.put(pivasContainer.getId(), pivasContainer);
			}
			
			for (PivasWorklist pivasWorklist : pivasWorklistList) {
				PivasPrep pivasPrep = pivasWorklist.getPivasPrep();
				if (pivasContainerByIdMap.containsKey(pivasPrep.getPivasContainerId())) {
					pivasPrep.setPivasContainer(pivasContainerByIdMap.get(pivasPrep.getPivasContainerId()));
				}
			}
		}
	}
	
	private void initCorePivasFormulaMethodAndLoadVo(List<PivasWorklist> pivasWorklistList) {
		// init core formula method and load formula method vo of unsaved worklist
		Set<Long> unsavedPivasFormulaIdSet = new HashSet<Long>();
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			if (pivasWorklist.getPivasPrep().getPivasFormulaId() != null && pivasWorklist.getPivasPrepMethod().getPivasFormulaMethodId() == null) {
				unsavedPivasFormulaIdSet.add(pivasWorklist.getPivasPrep().getPivasFormulaId());
			}
		}
		List<Long> unsavedPivasFormulaIdList = new ArrayList<Long>();
		unsavedPivasFormulaIdList.addAll(unsavedPivasFormulaIdSet);

		List<PivasFormulaMethod> corePivasFormulaMethodList = new ArrayList<PivasFormulaMethod>();
		if ( ! unsavedPivasFormulaIdList.isEmpty()) {
			corePivasFormulaMethodList = dmsPmsServiceProxy.retrieveCorePivasFormulaMethodListByPivasFormulaId(workstore.getHospCode(), unsavedPivasFormulaIdList);
		}
		
		Map<Long, PivasFormulaMethod> corePivasFormulaMethodByFormulaIdMap = new HashMap<Long, PivasFormulaMethod>();
		for (PivasFormulaMethod corePivasFormulaMethod : corePivasFormulaMethodList) {
			corePivasFormulaMethodByFormulaIdMap.put(corePivasFormulaMethod.getPivasFormulaId(), corePivasFormulaMethod);
		}
		
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			Long pivasFormulaId = pivasWorklist.getPivasPrep().getPivasFormulaId();
			if (pivasFormulaId != null && pivasWorklist.getPivasPrepMethod().getPivasFormulaMethodId() == null &&
					corePivasFormulaMethodByFormulaIdMap.containsKey(pivasFormulaId)) {
				PivasFormulaMethod corePivasFormulaMethod = corePivasFormulaMethodByFormulaIdMap.get(pivasFormulaId);
				pivasWorklist.getPivasPrepMethod().setPivasFormulaMethodId(corePivasFormulaMethod.getId());
				pivasWorklist.getPivasPrepMethod().setPivasFormulaMethod(corePivasFormulaMethod);
				
				PivasPrepMethodItem pivasPrepMethodItem = pivasWorklist.getPivasPrepMethod().getPivasPrepMethodItemList().get(0);
				PivasFormulaMethodDrug corePivasFormulaMethodDrug = corePivasFormulaMethod.getFirstPivasFormulaMethodDrug();
				pivasPrepMethodItem.setPivasFormulaMethodDrugId(corePivasFormulaMethodDrug.getId());
				
				Collections.sort(pivasWorklist.getPivasPrepMethod().getPivasPrepMethodItemList(), new PivasPrepMethodItemComparator());
			}
		}		
	}
	
	private void loadPivasFormulaMethodVoForSavedWorklist(List<PivasWorklist> pivasWorklistList) {
		// load formula method vo of saved worklist 
		Set<Long> pivasFormulaMethodIdSet = new HashSet<Long>();
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			if (pivasWorklist.getPivasPrepMethod().getPivasFormulaMethodId() != null) {
				pivasFormulaMethodIdSet.add(pivasWorklist.getPivasPrepMethod().getPivasFormulaMethodId());
			}
		}
		List<Long> pivasFormulaMethodIdList = new ArrayList<Long>();
		pivasFormulaMethodIdList.addAll(pivasFormulaMethodIdSet);

		List<PivasFormulaMethod> pivasFormulaMethodList = new ArrayList<PivasFormulaMethod>();
		if ( ! pivasFormulaMethodIdList.isEmpty()) {
			pivasFormulaMethodList = dmsPmsServiceProxy.retrievePivasFormulaMethodListByPivasFormulaMethodId(workstore.getHospCode(), pivasFormulaMethodIdList, false);
		}
		
		Map<Long, PivasFormulaMethod> pivasFormulaMethodByIdMap = new HashMap<Long, PivasFormulaMethod>();
		for (PivasFormulaMethod pivasFormulaMethod : pivasFormulaMethodList) {
			pivasFormulaMethodByIdMap.put(pivasFormulaMethod.getId(), pivasFormulaMethod);
		}
		
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			Long pivasFormulaMethodId = pivasWorklist.getPivasPrepMethod().getPivasFormulaMethodId();
			if (pivasFormulaMethodId != null && pivasFormulaMethodByIdMap.containsKey(pivasFormulaMethodId)) {
				pivasWorklist.getPivasPrepMethod().setPivasFormulaMethod(pivasFormulaMethodByIdMap.get(pivasFormulaMethodId));
			}
		}
	}
	
	private void loadMedProfilePoItemListForManualProfile(List<PivasWorklist> pivasWorklistList) {
		// load PO item list for manual item to display drug desc and instruction 
		Set<Long> manualMedProfileMoItemIdSet = new HashSet<Long>();
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			if (pivasWorklist.getMedProfileMoItem().isManualItem()) {
				manualMedProfileMoItemIdSet.add(pivasWorklist.getMedProfileMoItem().getId());
			}
		}
		
		List<MedProfilePoItem> medProfilePoItemList = QueryUtils.splitExecute(em.createQuery(
				"select o from MedProfilePoItem o" + // 20160616 index check : MedProfilePoItem.medProfileMoItemId : FK_MED_PROFILE_PO_ITEM_01
				" where o.medProfileMoItemId in :medProfileMoItemId")
				.setHint(QueryHints.FETCH, "o.medProfileMoItem"),
				"medProfileMoItemId", manualMedProfileMoItemIdSet);
		for (MedProfilePoItem medProfilePoItem : medProfilePoItemList) {
			medProfilePoItem.loadDmInfo();
		}
		
		Map<Long, MedProfilePoItem> medProfilePoItemByMoItemIdMap = new HashMap<Long, MedProfilePoItem>();
		for (MedProfilePoItem medProfilePoItem : medProfilePoItemList) {
			medProfilePoItemByMoItemIdMap.put(medProfilePoItem.getMedProfileMoItemId(), medProfilePoItem);
		}
		
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			if (pivasWorklist.getMedProfileMoItem().isManualItem()) {
				MedProfilePoItem medProfilePoItem = medProfilePoItemByMoItemIdMap.get(pivasWorklist.getMedProfileMoItem().getId());
				
				List<MedProfilePoItem> tempMedProfilePoItemList = new ArrayList<MedProfilePoItem>();
				tempMedProfilePoItemList.add(medProfilePoItem);
				
				// note: clone MedProfileMoItem to remove the indirect medProfilePoItemList, otherwise query will be run to retrieve 
				//      MedProfilePoItem by MedProfileMoItem id when setting MedProfileMoItem.medProfilePoItemList  
				MedProfileMoItem medProfileMoItemCopy = new MedProfileMoItem();
				BeanUtils.copyProperties(pivasWorklist.getMedProfileMoItem(), medProfileMoItemCopy, new String[]{"medProfilePoItemList"});
				pivasWorklist.setMedProfileMoItem(medProfileMoItemCopy);
				
				pivasWorklist.getMedProfileMoItem().setMedProfilePoItemList(tempMedProfilePoItemList);
			}
		}
	}
	
	private void initNewPivasWorklist(PivasWorklist pivasWorklist) {
		PivasPrep pivasPrep = pivasWorklist.getPivasPrep();
		PivasFormula pivasFormula = pivasWorklist.getPivasPrep().getPivasFormula();
		
		if (pivasFormula != null) {
			pivasPrep.setDrugKey(pivasFormula.getFirstPivasFormulaDrug().getDrugKey());
			pivasPrep.setPivasDosageUnit(pivasFormula.getFirstPivasFormulaDrug().getPivasDosageUnit());
		}

		
		// formula related fields of PivasPrep which can be changed even when formula is active:
		// - printName, pivasContainerId : set in loadPivasFormulaVo()
		// - shelf life : set by recal
		
		// splitCount already default to 1 in PivasWorklist constructor
		
		Freq orderDailyFreq = pivasWorklistManager.extractOrderDailyFreq(pivasWorklist.getMedProfileMoItem());
		DmDailyFrequency dmDailyFrequency = dmDailyFrequencyCacher.getDmDailyFrequencyByDailyFreqCode(orderDailyFreq.getCode());
		
		Freq orderSupplFreq = pivasWorklistManager.extractOrderSupplFreq(pivasWorklist.getMedProfileMoItem());
		
		if ("S".equals(dmDailyFrequency.getMultiplierType()) && dmDailyFrequency.getFreqInterval() != null && dmDailyFrequency.getFreqInterval() == 0) {
			pivasPrep.setFreqInterval(BigDecimal.valueOf(0));
		} else if ( orderSupplFreq != null && ! StringUtils.isEmpty(orderSupplFreq.getCode()) ) {
			// suppl freq exists
			pivasPrep.setFreqInterval(null);
		} else if ( "A".equals(dmDailyFrequency.getMultiplierType()) ) {
			String freqParamStr = orderDailyFreq.getParam()[0];
			pivasPrep.setFreqInterval(new BigDecimal(freqParamStr));
		} else if (dmDailyFrequency.getFreqInterval() != null && dmDailyFrequency.getFreqInterval() > 0) {
			pivasPrep.setFreqInterval(BigDecimal.valueOf(dmDailyFrequency.getFreqInterval()));
		} else {
			pivasPrep.setFreqInterval(null);
		}
		
		// satellite flag is set by recal
		// due date is set after PIVAS label gen
		
		if ("S".equals(dmDailyFrequency.getMultiplierType()) && dmDailyFrequency.getFreqInterval() != null && dmDailyFrequency.getFreqInterval() == 0) {
			pivasPrep.setSingleDispFlag(Boolean.TRUE);
		} else {
			pivasPrep.setSingleDispFlag(Boolean.FALSE);
		}
		
		// lastPrintDate is set by PIVAS label gen
	}

	private void initRefillPivasWorklist(PivasWorklist pivasWorklist, Date searchSupplyDate) {
		PivasPrep pivasPrep = pivasWorklist.getPivasPrep();
		
		if (searchSupplyDate == null) {
			throw new UnsupportedOperationException("Search supply date of due order tab must be exist for refill worklist, pivasWorklistId = " + pivasWorklist.getId() + ", pivasPrepId = " + pivasPrep.getId());
		}
		
		// all supply date of refill worklist must be the front-end search supply date
		pivasWorklist.setSupplyDate(searchSupplyDate);
		
		// satellite flag is set by recal
		pivasPrep.setSingleDispFlag(Boolean.FALSE);
	}

	private void initAdHocPivasWorklist(PivasWorklist pivasWorklist, Date adHocSupplyDate) {
		pivasWorklist.setSupplyDate(adHocSupplyDate);
		
		// since calculation is not performed, some fields are updated here
		pivasWorklist.setNextDueDate(null);
		pivasWorklist.setNextSupplyDate(null);
	}
	
	private void initPrepMethodDrug(PivasWorklist pivasWorklist) {
		PivasPrepMethod pivasPrepMethod = pivasWorklist.getPivasPrepMethod();
		PivasPrepMethodItem pivasPrepMethodItem = pivasWorklist.getPivasPrepMethod().getPivasPrepMethodItemList().get(0);
		
		PivasFormulaMethod pivasFormulaMethod = pivasWorklist.getPivasPrepMethod().getPivasFormulaMethod();
		if (pivasFormulaMethod == null) {
			return;
		}
		
		PivasDrug pivasDrug = pivasFormulaMethod.getFirstPivasFormulaMethodDrug().getPivasDrugManufMethod().getPivasDrugManuf().getPivasDrug();
		PivasDrugManuf pivasDrugManuf = pivasFormulaMethod.getFirstPivasFormulaMethodDrug().getPivasDrugManufMethod().getPivasDrugManuf();
		
		pivasPrepMethodItem.setDrugItemCode(pivasDrug.getItemCode()); 
		pivasPrepMethodItem.setDrugManufCode(pivasDrugManuf.getManufacturerCode());
		// drugBatchNum and drugExpiryDate is set by recal

		// warning code
		pivasPrepMethod.setWarnCode1(pivasFormulaMethod.getWarnCode1());
		pivasPrepMethod.setWarnCode2(pivasFormulaMethod.getWarnCode2());

		// drug disp qty
		if (PIVAS_DRUGDISPQTY.get(workstore.getHospital()) != null) {
			Integer defaultDrugDispQtyByRule = PIVAS_DRUGDISPQTY.get(workstore.getHospital());
			pivasPrepMethodItem.setDrugDispQty(BigDecimal.valueOf(defaultDrugDispQtyByRule));
		} else {
			pivasPrepMethodItem.setDrugDispQty(BigDecimal.valueOf(0));
		}
	}

	private boolean checkSatelliteAllowed(PivasWorklist pivasWorklist) {
		if ( ! PIVAS_SATELLITE_ENABLED.get(workstore.getHospital())) {
			return false;
		}
		
		Map<String, List<PivasServiceHour>> pivasServiceHourListByDayOfWeekMap = pivasServiceHourManager.retrievePivasServiceHourListByDayOfWeekMap(workstore.getWorkstoreGroup().getWorkstoreGroupCode(), workstore.getHospital());
		
		Set<Date> pivasHolidayDateSet = pivasServiceHourManager.retrievePivasHolidayDateSet(workstore.getWorkstoreGroup().getWorkstoreGroupCode());
		
		DateTime supplyDateDt = new DateTime(pivasWorklist.getSupplyDate());
		int dayOfWeekValue = supplyDateDt.getDayOfWeek();
		int hour = supplyDateDt.getHourOfDay();
		int minute = supplyDateDt.getMinuteOfHour();
		int supplyTime = hour * 100 + minute;
	
		boolean pivasServiceHourFound = false;
		
		List<PivasServiceHour> pivasServiceHourList = pivasServiceHourListByDayOfWeekMap.get(String.valueOf(dayOfWeekValue));
		if (pivasServiceHourList != null) {
			for (PivasServiceHour pivasServiceHour : pivasServiceHourList) {
				if (pivasServiceHour.getSupplyTime() == supplyTime && pivasServiceHour.getType() == PivasServiceHourType.Satellite) {
					pivasServiceHourFound = true;
					break;
				}
			}
		}

		if ( ! pivasHolidayDateSet.contains(pivasWorklist.getSupplyDate()) && pivasServiceHourFound) {
			return true;
		} else {
			return false;
		}
	}
	
	private void loadPrepMethodDrugInfoDrugVo(List<PivasWorklist> pivasWorklistList) {
		// load drug dmDrug
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			PivasPrepMethodItem pivasPrepMethodItem = pivasWorklist.getPivasPrepMethod().getPivasPrepMethodItemList().get(0);
			if ( ! StringUtils.isEmpty(pivasPrepMethodItem.getDrugItemCode())) {
				pivasPrepMethodItem.setDrugDmDrug(dmDrugCacher.getDmDrug(pivasPrepMethodItem.getDrugItemCode()));
			}
		}		
	}
	
	private void initPrepMethodSolventDiluentInfoAndLoadManufVo(List<PivasWorklist> pivasWorklistList) {
		// get criteria list to get default PIVAS Drug from DMS 
		List<String> solventCodeList = new ArrayList<String>();
		List<String> solventItemCodeList = new ArrayList<String>();
		List<String> diluentCodeList = new ArrayList<String>();
				
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			if (pivasWorklist.getPivasPrep().getPivasFormula() == null || pivasWorklist.getPivasPrepMethod().getPivasFormulaMethod() == null) {
				continue;
			}

			PivasFormulaMethod pivasFormulaMethod = pivasWorklist.getPivasPrepMethod().getPivasFormulaMethod();
			PivasDrugManuf pivasDrugManuf = pivasFormulaMethod.getFirstPivasFormulaMethodDrug().getPivasDrugManufMethod().getPivasDrugManuf();
			PivasDrugManufMethod pivasDrugManufMethod = pivasFormulaMethod.getFirstPivasFormulaMethodDrug().getPivasDrugManufMethod();
			
			if (pivasDrugManuf.getRequireSolventFlag() && ! StringUtils.equals(pivasDrugManufMethod.getDiluentCode(), VEND_SUPPLY_SOLV)) { 
				if ( ! StringUtils.isEmpty(pivasDrugManufMethod.getItemCode())) {
					solventItemCodeList.add(pivasDrugManufMethod.getItemCode());
				} else {
					solventCodeList.add(pivasDrugManufMethod.getDiluentCode());
				}
			}
			
		}
		
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			if (pivasWorklist.getPivasPrep().getPivasFormula() == null) {
				continue;
			}
			
			PivasFormula pivasFormula = pivasWorklist.getPivasPrep().getPivasFormula();
			
			if ( ! StringUtils.isEmpty(pivasFormula.getDiluentCode())) {
				diluentCodeList.add(pivasFormula.getDiluentCode());
			}
		}

		
		// get solvent and diluent default PIVAS Drug from DMS 
		List<PivasDrug> defaultSolventPivasDrugList = null;
		if ( ! solventCodeList.isEmpty()) {
			defaultSolventPivasDrugList = dmsPmsServiceProxy.retrieveDefaultPivasDrugMapBySolventCode(workstore.getWorkstoreGroup().getWorkstoreGroupCode(), workstore.getHospCode(), solventCodeList);
		} else {
			defaultSolventPivasDrugList = new ArrayList<PivasDrug>();
		}
		Map<String, PivasDrug> defaultPivasDrugBySolventCodeMap = new HashMap<String, PivasDrug>();
		for (PivasDrug pivasDrug : defaultSolventPivasDrugList) {
			defaultPivasDrugBySolventCodeMap.put(pivasDrug.getDiluentCode(), pivasDrug);
		}
		
		List<PivasDrug> solventPivasDrugList = null;
		if ( ! solventItemCodeList.isEmpty()) {
			solventPivasDrugList = dmsPmsServiceProxy.retrievePivasDrugListByItemCode(workstore.getWorkstoreGroup().getWorkstoreGroupCode(), workstore.getHospCode(), solventItemCodeList);
		} else {
			solventPivasDrugList = new ArrayList<PivasDrug>();
		}
		Map<String, PivasDrug> solventPivasDrugByItemCodeMap = new HashMap<String, PivasDrug>();
		for (PivasDrug pivasDrug : solventPivasDrugList) {
			solventPivasDrugByItemCodeMap.put(pivasDrug.getItemCode(), pivasDrug);
		}		
		
		List<PivasDrug> defaultDiluentPivasDrugList = null;
		if ( ! diluentCodeList.isEmpty()) {
			defaultDiluentPivasDrugList = dmsPmsServiceProxy.retrieveDefaultPivasDrugMapByDiluentCode(workstore.getWorkstoreGroup().getWorkstoreGroupCode(), workstore.getHospCode(), diluentCodeList);
		} else {
			defaultDiluentPivasDrugList = new ArrayList<PivasDrug>();
		}
		Map<String, PivasDrug> defaultPivasDrugByDiluentCodeMap = new HashMap<String, PivasDrug>();
		for (PivasDrug pivasDrug : defaultDiluentPivasDrugList) {
			defaultPivasDrugByDiluentCodeMap.put(pivasDrug.getDiluentCode(), pivasDrug);
		}
		
		
		// init solvent
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			PivasPrepMethodItem pivasPrepMethodItem = pivasWorklist.getPivasPrepMethod().getPivasPrepMethodItemList().get(0);
			
			// clear solvent
			pivasPrepMethodItem.setSolventCode(null);
			pivasPrepMethodItem.setSolventDesc(null);
			pivasPrepMethodItem.setSolventItemCode(null);
			pivasPrepMethodItem.setSolventManufCode(null);
			pivasPrepMethodItem.setSolventBatchNum(null);
			pivasPrepMethodItem.setSolventExpiryDate(null);
			pivasPrepMethodItem.setSolventPivasDrugManuf(null);
			
			if (pivasWorklist.getPivasPrep().getPivasFormula() == null || pivasWorklist.getPivasPrepMethod().getPivasFormulaMethod() == null) {
				continue;
			}

			PivasFormulaMethod pivasFormulaMethod = pivasWorklist.getPivasPrepMethod().getPivasFormulaMethod();
			PivasDrugManuf pivasDrugManuf = pivasFormulaMethod.getFirstPivasFormulaMethodDrug().getPivasDrugManufMethod().getPivasDrugManuf();
			PivasDrugManufMethod pivasDrugManufMethod = pivasFormulaMethod.getFirstPivasFormulaMethodDrug().getPivasDrugManufMethod();
			
			if (pivasDrugManuf.getRequireSolventFlag()) {
				pivasPrepMethodItem.setSolventCode(pivasDrugManufMethod.getDiluentCode());
				if (pivasDrugManufMethod.getDmDiluent() != null) {
					pivasPrepMethodItem.setSolventDesc(pivasDrugManufMethod.getDmDiluent().getPivasDiluentDesc());
				}
				
				if ( ! StringUtils.equals(pivasDrugManufMethod.getDiluentCode(), VEND_SUPPLY_SOLV)) {
					// set default solvent item code
					PivasDrug solventPivasDrug = null;
					
					if ( ! StringUtils.isEmpty(pivasDrugManufMethod.getItemCode()) ) {
						pivasPrepMethodItem.setSolventItemCode(pivasDrugManufMethod.getItemCode());
						if (solventPivasDrugByItemCodeMap.containsKey(pivasDrugManufMethod.getItemCode())) {
							solventPivasDrug = solventPivasDrugByItemCodeMap.get(pivasDrugManufMethod.getItemCode());
						}
					} else {
						if ( defaultPivasDrugBySolventCodeMap.containsKey(pivasDrugManufMethod.getDiluentCode()) ) {
							PivasDrug defaultSolventPivasDrug = defaultPivasDrugBySolventCodeMap.get(pivasDrugManufMethod.getDiluentCode()); 
							pivasPrepMethodItem.setSolventItemCode(defaultSolventPivasDrug.getItemCode());
							solventPivasDrug = defaultSolventPivasDrug;
						}
					}
					
					// set default solvent manufacturer code
					if (solventPivasDrug != null && solventPivasDrug.getPivasDrugManufList() != null) {
						int activePivasDrugManufCount = 0;
						PivasDrugManuf activePivasDrugManuf = null;
						for (PivasDrugManuf solventPivasDrugManuf : solventPivasDrug.getPivasDrugManufList()) {
							if (solventPivasDrugManuf.getStatus() == PivasDrugManufStatus.Active) {
								activePivasDrugManufCount++;
								activePivasDrugManuf = solventPivasDrugManuf;
							}
						}
						
						if (activePivasDrugManufCount == 1) {
							pivasPrepMethodItem.setSolventManufCode(activePivasDrugManuf.getManufacturerCode());
							pivasPrepMethodItem.setSolventPivasDrugManuf(activePivasDrugManuf);
						}
					}
					
					// solventBatchNum and solventExpiryDate is set by recal
				}
			}			
		}		
		
		
		// init diluent
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			PivasPrepMethod pivasPrepMethod = pivasWorklist.getPivasPrepMethod();
			
			// clear diluent
			pivasPrepMethod.setDiluentCode(null);
			pivasPrepMethod.setDiluentDesc(null);
			pivasPrepMethod.setDiluentItemCode(null);
			pivasPrepMethod.setDiluentManufCode(null);
			pivasPrepMethod.setDiluentBatchNum(null);
			pivasPrepMethod.setDiluentExpiryDate(null);
			pivasPrepMethod.setDiluentPivasDrugManuf(null);
			
			if (pivasWorklist.getPivasPrep().getPivasFormula() == null) {
				continue;
			}
			
			PivasFormula pivasFormula = pivasWorklist.getPivasPrep().getPivasFormula();
			
			if ( ! StringUtils.isEmpty(pivasFormula.getDiluentCode())) {
				// set diluent code and desc
				pivasPrepMethod.setDiluentCode(pivasFormula.getDiluentCode());
				if (pivasFormula.getDmDiluent() != null) {
					pivasPrepMethod.setDiluentDesc(pivasFormula.getDmDiluent().getPivasDiluentDesc());
				}
				
				// set default diluent item code
				PivasDrug diluentPivasDrug = null;
				
				if ( defaultPivasDrugByDiluentCodeMap.containsKey(pivasFormula.getDiluentCode()) ) {
					PivasDrug defaultDiluentPivasDrug = defaultPivasDrugByDiluentCodeMap.get(pivasFormula.getDiluentCode()); 
					pivasPrepMethod.setDiluentItemCode(defaultDiluentPivasDrug.getItemCode());
					diluentPivasDrug = defaultDiluentPivasDrug;
				}
				
				// set default diluent manufacturer code
				if (diluentPivasDrug != null && diluentPivasDrug.getPivasDrugManufList() != null) {
					int activePivasDrugManufCount = 0;
					PivasDrugManuf activePivasDrugManuf = null;
					for (PivasDrugManuf diluentPivasDrugManuf : diluentPivasDrug.getPivasDrugManufList()) {
						if (diluentPivasDrugManuf.getStatus() == PivasDrugManufStatus.Active) {
							activePivasDrugManufCount++;
							activePivasDrugManuf = diluentPivasDrugManuf;
						}
					}
					
					if (activePivasDrugManufCount == 1) {
						pivasPrepMethod.setDiluentManufCode(activePivasDrugManuf.getManufacturerCode());
						pivasPrepMethod.setDiluentPivasDrugManuf(activePivasDrugManuf);
					}
				}
				
				// diluentBatchNum and diluentExpiryDate is set by recal
			}			
		}		
	}
	
	private void loadPrepMethodSolventDiluentInfoManufVoForSavedWorklist(List<PivasWorklist> pivasWorklistList) {
		List<String> solventDiluentItemCodeList = new ArrayList<String>();
		
		// get criteria list to get default PIVAS Drug from DMS 
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			PivasPrepMethod pivasPrepMethod = pivasWorklist.getPivasPrepMethod();
			PivasPrepMethodItem pivasPrepMethodItem = pivasWorklist.getPivasPrepMethod().getPivasPrepMethodItemList().get(0);
			
			if ( ! StringUtils.isEmpty(pivasPrepMethodItem.getSolventItemCode()) &&  ! StringUtils.isEmpty(pivasPrepMethodItem.getSolventManufCode()) ) {
				solventDiluentItemCodeList.add(pivasPrepMethodItem.getSolventItemCode());
			}
			if ( ! StringUtils.isEmpty(pivasPrepMethod.getDiluentItemCode()) &&  ! StringUtils.isEmpty(pivasPrepMethod.getDiluentManufCode()) ) {
				solventDiluentItemCodeList.add(pivasPrepMethod.getDiluentItemCode());
			}
		}

		
		// get solvent and diluent PIVAS Drug from DMS 
		List<PivasDrug> pivasDrugList;
		if ( ! solventDiluentItemCodeList.isEmpty()) {
			pivasDrugList = dmsPmsServiceProxy.retrievePivasDrugListByItemCode(workstore.getWorkstoreGroup().getWorkstoreGroupCode(), workstore.getHospCode(), solventDiluentItemCodeList);
		} else {
			pivasDrugList = new ArrayList<PivasDrug>();
		}
		Map<String, PivasDrug> pivasDrugByItemCodeMap = new HashMap<String, PivasDrug>();
		for (PivasDrug pivasDrug : pivasDrugList) {
			pivasDrugByItemCodeMap.put(pivasDrug.getItemCode(), pivasDrug);
		}
		
		
		// load solvent and dilent PivasDrugManuf vo 
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			PivasPrepMethod pivasPrepMethod = pivasWorklist.getPivasPrepMethod();
			PivasPrepMethodItem pivasPrepMethodItem = pivasWorklist.getPivasPrepMethod().getPivasPrepMethodItemList().get(0);
			
			if ( ! StringUtils.isEmpty(pivasPrepMethodItem.getSolventItemCode()) &&  ! StringUtils.isEmpty(pivasPrepMethodItem.getSolventManufCode()) ) {
				if (pivasDrugByItemCodeMap.containsKey(pivasPrepMethodItem.getSolventItemCode())) {
					PivasDrug pivasDrug = pivasDrugByItemCodeMap.get(pivasPrepMethodItem.getSolventItemCode());
					for (PivasDrugManuf pivasDrugManuf : pivasDrug.getPivasDrugManufList()) {
						if ( StringUtils.equals(pivasDrugManuf.getManufacturerCode(), pivasPrepMethodItem.getSolventManufCode()) ) {
							pivasPrepMethodItem.setSolventPivasDrugManuf(pivasDrugManuf);
							break;
						}
					}
				}
			}
			
			if ( ! StringUtils.isEmpty(pivasPrepMethod.getDiluentItemCode()) &&  ! StringUtils.isEmpty(pivasPrepMethod.getDiluentManufCode()) ) {
				if (pivasDrugByItemCodeMap.containsKey(pivasPrepMethod.getDiluentItemCode())) {
					PivasDrug pivasDrug = pivasDrugByItemCodeMap.get(pivasPrepMethod.getDiluentItemCode());
					for (PivasDrugManuf pivasDrugManuf : pivasDrug.getPivasDrugManufList()) {
						if ( StringUtils.equals(pivasDrugManuf.getManufacturerCode(), pivasPrepMethod.getDiluentManufCode()) ) {
							pivasPrepMethod.setDiluentPivasDrugManuf(pivasDrugManuf);
							break;
						}
					}
				}
			}
		}
	}

	private void initPivasWorklistBySupplyDateChange(PivasWorklist pivasWorklist, boolean forceUpdateBySatelliteFlag) {
		if (pivasWorklist.getSupplyDate() != null) {
			PivasFormula pivasFormula = pivasWorklist.getPivasPrep().getPivasFormula();
			
			Boolean prevSatelliteFlag = pivasWorklist.getSatelliteFlag();
			if (checkSatelliteAllowed(pivasWorklist)) {
				pivasWorklist.setSatelliteFlag(Boolean.TRUE);
				pivasWorklist.setEnableSatelliteFlag(Boolean.TRUE);
			} else {
				pivasWorklist.setSatelliteFlag(Boolean.FALSE);
				pivasWorklist.setEnableSatelliteFlag(Boolean.FALSE);
			}
			
			// init info by satellite if worklist is init, or satellite flag is changed in recalculation
			if (forceUpdateBySatelliteFlag || prevSatelliteFlag != pivasWorklist.getSatelliteFlag()) {
				initPivasWorklistBySatelliteFlagChange(pivasWorklist);
			} else {
				if (pivasFormula != null && pivasWorklist.getShelfLife() != null) {
					Date expiryDate = new DateTime(pivasWorklist.getSupplyDate()).plusHours(pivasWorklist.getShelfLife()).toDate();
					pivasWorklist.setExpiryDate(expiryDate);
				}
			}
		}
	}
	
	private void initPivasWorklistBySatelliteFlagChange(PivasWorklist pivasWorklist) {
		PivasPrepMethod pivasPrepMethod = pivasWorklist.getPivasPrepMethod();
		PivasPrepMethodItem pivasPrepMethodItem = pivasWorklist.getPivasPrepMethod().getPivasPrepMethodItemList().get(0);
		PivasFormula pivasFormula = pivasWorklist.getPivasPrep().getPivasFormula();
		PivasFormulaMethod pivasFormulaMethod = pivasWorklist.getPivasPrepMethod().getPivasFormulaMethod();
		PivasDrugManuf pivasDrugManuf = null;
		if (pivasFormulaMethod != null) {
			pivasDrugManuf = pivasFormulaMethod.getFirstPivasFormulaMethodDrug().getPivasDrugManufMethod().getPivasDrugManuf();
		}

		pivasWorklist.setShelfLife(null);
		pivasPrepMethodItem.setDrugBatchNum(null);
		pivasPrepMethodItem.setDrugExpiryDate(null);
		pivasPrepMethodItem.setSolventBatchNum(null);
		pivasPrepMethodItem.setSolventExpiryDate(null);
		pivasPrepMethod.setDiluentBatchNum(null);
		pivasPrepMethod.setDiluentExpiryDate(null);
		
		if (pivasWorklist.getSatelliteFlag()) {
			if (pivasFormula != null) {
				pivasWorklist.setShelfLife(pivasFormula.getSatelliteShelfLife());
			}
			if (pivasDrugManuf != null) {
				pivasPrepMethodItem.setDrugBatchNum(pivasDrugManuf.getSatelliteBatchNum());
				pivasPrepMethodItem.setDrugExpiryDate(pivasDrugManuf.getSatelliteExpireDate());
			}
			if (pivasPrepMethodItem.getSolventPivasDrugManuf() != null) {
				pivasPrepMethodItem.setSolventBatchNum(pivasPrepMethodItem.getSolventPivasDrugManuf().getSatelliteBatchNum());
				pivasPrepMethodItem.setSolventExpiryDate(pivasPrepMethodItem.getSolventPivasDrugManuf().getSatelliteExpireDate());
			}
			if (pivasPrepMethod.getDiluentPivasDrugManuf() != null) {
				pivasPrepMethod.setDiluentBatchNum(pivasPrepMethod.getDiluentPivasDrugManuf().getSatelliteBatchNum());
				pivasPrepMethod.setDiluentExpiryDate(pivasPrepMethod.getDiluentPivasDrugManuf().getSatelliteExpireDate());
			}
		} else {
			if (pivasFormulaMethod != null && pivasFormulaMethod.getShelfLife() != null) {
				pivasWorklist.setShelfLife(pivasFormulaMethod.getShelfLife());
			}
			else if (pivasFormula != null) {
				pivasWorklist.setShelfLife(pivasFormula.getShelfLife());
			}
			if (pivasDrugManuf != null) {
				pivasPrepMethodItem.setDrugBatchNum(pivasDrugManuf.getBatchNum());
				pivasPrepMethodItem.setDrugExpiryDate(pivasDrugManuf.getExpireDate());
			}
			if (pivasPrepMethodItem.getSolventPivasDrugManuf() != null) {
				pivasPrepMethodItem.setSolventBatchNum(pivasPrepMethodItem.getSolventPivasDrugManuf().getBatchNum());
				pivasPrepMethodItem.setSolventExpiryDate(pivasPrepMethodItem.getSolventPivasDrugManuf().getExpireDate());
			}
			if (pivasPrepMethod.getDiluentPivasDrugManuf() != null) {
				pivasPrepMethod.setDiluentBatchNum(pivasPrepMethod.getDiluentPivasDrugManuf().getBatchNum());
				pivasPrepMethod.setDiluentExpiryDate(pivasPrepMethod.getDiluentPivasDrugManuf().getExpireDate());
			}
		}
		
		if (pivasFormula != null && pivasWorklist.getShelfLife() != null) {
			Date expiryDate = new DateTime(pivasWorklist.getSupplyDate()).plusHours(pivasWorklist.getShelfLife()).toDate();
			pivasWorklist.setExpiryDate(expiryDate);
		}
	}
	
	private void loadSatelliteEnabledForSavedWorklist(List<PivasWorklist> pivasWorklistList) {
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			if (checkSatelliteAllowed(pivasWorklist)) {
				pivasWorklist.setEnableSatelliteFlag(Boolean.TRUE);
			} else {
				pivasWorklist.setEnableSatelliteFlag(Boolean.FALSE);
			}
		}
	}
	
	private void calculateNextSupplyDateForSavedWorklist(List<PivasWorklist> pivasWorklistList) {
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			// save next supply date for front-end validation of due date
			if (pivasWorklist.getSupplyDate() != null) {
				pivasWorklist.setCheckNextSupplyDate( calculateNextRefillSupplyDate(pivasWorklist.getSupplyDate()) );
			}
			
			Date nextSupplyDate = null;
			
			if (pivasWorklist.getNextDueDate() != null) {
				// filter out find patient worklist which has no next due date 
				
				// get frequency interval
				if (validatePivasWorklistDueDateSupplyDate(pivasWorklist)) {
					// get next refill supply date
					Date orgNextSupplyDate = calculateNextRefillSupplyDate(pivasWorklist.getSupplyDate());
					Date calNextSupplyDate = null;
					if (pivasWorklist.getNextDueDate().compareTo(orgNextSupplyDate) > 0) {			
						calNextSupplyDate = pivasWorklistManager.calculateSupplyDateByDueDate(workstore.getWorkstoreGroup().getWorkstoreGroupCode(), workstore.getHospital(), pivasWorklist.getNextDueDate(), true);
					} else {
						calNextSupplyDate = orgNextSupplyDate;
					}
					
					if ( ! checkPivasWorklistEnded(pivasWorklist, calNextSupplyDate)) {
						nextSupplyDate = calNextSupplyDate;
					}
				}
			}
			
			pivasWorklist.setNextSupplyDate(nextSupplyDate);
		}		
	}
	
	private boolean validatePivasWorklistDueDateSupplyDate(PivasWorklist pivasWorklist) {
		// get current buffer
		Date currentDate = new Date();
		Integer currentBufferValue = PIVAS_CURRENTDATE_BUFFER.get(workstore.getHospital());;
		Date currentBufferDate = new DateTime(currentDate).minusHours(currentBufferValue).toDate();
		
		// get supply buffer
		Integer supplyBufferValue = PIVAS_SUPPLYDATE_BUFFER.get(workstore.getHospital());
		Date supplyBufferDate = new DateTime(pivasWorklist.getSupplyDate()).minusHours(supplyBufferValue).toDate(); 

		// get next refill supply date
		Date nextRefillSupplyDate = calculateNextRefillSupplyDate(pivasWorklist.getSupplyDate());
		
		if ( ! pivasWorklist.getRefillFlag()) {
			// no need to validate refill worklist, because 
			// 1. if this function is called by PIVAS quantity calculation, when due date is invalid, it is adjusted to before current date or supply buffer date
			// 2. if this function is called by saved worklist next supply date calculation, when due date is invalid, can still proceed to calculate next supply date
			//    by next due date for user reference. The worklist cannot be saved and added to worklist.
			// 3. since refill worklist does not allow to change issue quantity, it is not possible that this function is called by issue quantity change
			if ( pivasWorklist.getSupplyDate().compareTo(pivasWorklist.getExpiryDate()) > 0 ||
					(calculatePivasEndDate(pivasWorklist) != null && pivasWorklist.getSupplyDate().compareTo(calculatePivasEndDate(pivasWorklist)) > 0) ) {
				return false;
			}
			
			if ( pivasWorklist.getDueDate().compareTo(currentBufferDate) < 0 || 
					pivasWorklist.getDueDate().compareTo(supplyBufferDate) < 0 ||
					(pivasWorklist.getShelfLife() > 0 && (pivasWorklist.getIssueQty() == null || pivasWorklist.getAdjQty() == null || pivasWorklist.getIssueQty().add(pivasWorklist.getAdjQty()).compareTo(BigDecimal.valueOf(0)) > 0) && 
							pivasWorklist.getDueDate().compareTo(pivasWorklist.getExpiryDate()) > 0) ||
					pivasWorklist.getDueDate().compareTo(nextRefillSupplyDate) > 0 ||
					(calculatePivasEndDate(pivasWorklist) != null && pivasWorklist.getDueDate().compareTo(calculatePivasEndDate(pivasWorklist)) > 0) ) {
				return false;
			}
		}
		
		return true;
	}
	
	private Map<Long, List<MedProfilePoItem>> retrieveMrMedProfilePoItemListMap(List<Long> medProfileMoItemIdList) {
		Map<Long, List<MedProfilePoItem>> mrMedProfilePoItemListMap = new HashMap<Long, List<MedProfilePoItem>>();

		if (medProfileMoItemIdList.isEmpty()) {
			return mrMedProfilePoItemListMap;
		}
		
		List<MedProfilePoItem> medProfilePoItemList = QueryUtils.splitExecute(em.createQuery(
				"select o from MedProfilePoItem o" + // 20160616 index check : MedProfilePoItem.medProfileMoItemId : FK_MED_PROFILE_PO_ITEM_01
				" where o.medProfileMoItemId in :medProfileMoItemId")
				.setHint(QueryHints.FETCH, "o.medProfileMoItem"),
				"medProfileMoItemId", medProfileMoItemIdList);
		
		for (MedProfilePoItem medProfilePoItem : medProfilePoItemList) {
			WorkstoreDrug workstoreDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, medProfilePoItem.getItemCode());
			
			if (workstoreDrug.getMsWorkstoreDrug() != null && "A".equals(workstoreDrug.getMsWorkstoreDrug().getItemStatus()) ) {
				medProfilePoItem.loadDmInfo();	// load DmDrug
				
				List<MedProfilePoItem> mrMedProfilePoItemList;
				if (mrMedProfilePoItemListMap.containsKey(medProfilePoItem.getMedProfileMoItemId())) {
					mrMedProfilePoItemList = mrMedProfilePoItemListMap.get(medProfilePoItem.getMedProfileMoItemId());
				} else {
					mrMedProfilePoItemList = new ArrayList<MedProfilePoItem>();
					mrMedProfilePoItemListMap.put(medProfilePoItem.getMedProfileMoItemId(), mrMedProfilePoItemList);
				}
				
				mrMedProfilePoItemList.add(medProfilePoItem);
			}
		}
		
		return mrMedProfilePoItemListMap;
	}
	
	private void initPivasWorklistByIssueQtyChange(PivasWorklist pivasWorklist) {
		PivasPrep pivasPrep = pivasWorklist.getPivasPrep();
		
		// validate input
		if ( pivasPrep.getFreqInterval() == null || pivasWorklist.getShelfLife() == null || pivasWorklist.getSupplyDate() == null || 
				pivasWorklist.getDueDate() == null || pivasWorklist.getExpiryDate() == null || pivasWorklist.getIssueQty() == null ||
				 ! validatePivasWorklistDueDateSupplyDate(pivasWorklist) ) {
			pivasWorklist.setNextDueDate(null);
			pivasWorklist.setNextSupplyDate(null);
			if (pivasWorklist.getMaterialRequest() != null) {
				MaterialRequest materialRequest = pivasWorklist.getMaterialRequest();
				for (MaterialRequestItem materialRequestItem : materialRequest.getMaterialRequestItemList()) {
					materialRequestItem.setDueDate(null);
				}
			}
			return;
		}
		
		// calculate next due date and next supply date
		Integer freqIntervalInMinutes = pivasPrep.getFreqInterval().multiply(BigDecimal.valueOf(60)).setScale(0, RoundingMode.HALF_UP).intValue();
		BigDecimal issueQty = pivasWorklist.getIssueQty();
		
		int lastDoseDurationInMinutes = issueQty.subtract(BigDecimal.valueOf(1)).multiply(BigDecimal.valueOf(freqIntervalInMinutes)).intValue();
		Date lastDoseDate = new DateTime(pivasWorklist.getDueDate()).plusMinutes(lastDoseDurationInMinutes).toDate();

		if ( (pivasWorklist.getShelfLife() > 0 && lastDoseDate.compareTo(pivasWorklist.getExpiryDate()) > 0) ||
				(calculatePivasEndDate(pivasWorklist) != null && lastDoseDate.compareTo(calculatePivasEndDate(pivasWorklist)) > 0) ) {
			// invalid input
			pivasWorklist.setNextDueDate(null);
			pivasWorklist.setNextSupplyDate(null);
			if (pivasWorklist.getMaterialRequest() != null) {
				MaterialRequest materialRequest = pivasWorklist.getMaterialRequest();
				for (MaterialRequestItem materialRequestItem : materialRequest.getMaterialRequestItemList()) {
					materialRequestItem.setDueDate(null);
				}
			}			
			return;
			
		} else {
			// update next due date
			int nextDueDurationInMinutes = issueQty.multiply(BigDecimal.valueOf(freqIntervalInMinutes)).intValue();
			Date nextDueDate = new DateTime(pivasWorklist.getDueDate()).plusMinutes(nextDueDurationInMinutes).toDate();
			
			pivasWorklist.setNextDueDate(nextDueDate);
			
			// update next supply date
			Date orgNextSupplyDate = calculateNextRefillSupplyDate(pivasWorklist.getSupplyDate());
			Date calNextSupplyDate = null; 
			
			if (nextDueDate.compareTo(orgNextSupplyDate) > 0) {			
				Date extendedNextSupplyDate = pivasWorklistManager.calculateSupplyDateByDueDate(workstore.getWorkstoreGroup().getWorkstoreGroupCode(), workstore.getHospital(), nextDueDate, true);
				calNextSupplyDate = extendedNextSupplyDate;
			} else {
				calNextSupplyDate = orgNextSupplyDate;
			}
			
			if ( ! checkPivasWorklistEnded(pivasWorklist, calNextSupplyDate)) {
				pivasWorklist.setNextSupplyDate(calNextSupplyDate);
			} else {
				pivasWorklist.setNextSupplyDate(null);
			}
			
			// update PIVAS request due date
			if (pivasWorklist.getMaterialRequest() != null) {
				MaterialRequest materialRequest = pivasWorklist.getMaterialRequest();
				for (MaterialRequestItem materialRequestItem : materialRequest.getMaterialRequestItemList()) {
					materialRequestItem.setDueDate(nextDueDate);
				}
			}
		}
		
		// calculate number of label
		calculateNumOfLabel(pivasWorklist);
		
		// calculate issue qty for stock deduction 
		calculateDrugDispQty(pivasWorklist);
	}
	
	private List<PivasWorklist> finalFilterPivasWorklist(List<PivasWorklist> rawPivasWorklistList) {
		List<PivasWorklist> pivasWorklistList = new ArrayList<PivasWorklist>();
		
		for (PivasWorklist rawPivasWorklist : rawPivasWorklistList) {
			if ( ! rawPivasWorklist.getFilterFlag()) {
				pivasWorklistList.add(rawPivasWorklist);
			}
		}
		
		return pivasWorklistList;
	}
	
	private String validatePivasWorklistVersion(Long medProfileId, Long medProfileVersion) {
		List<MedProfile> medProfileList = em.createQuery(
				"select o from MedProfile o" + // 20160616 index check : MedProfile.id : PK_MED_PROFILE
				" where o.id = :id" +
				" and o.processingFlag = :processingFlag")
				.setParameter("id", medProfileId)
				.setParameter("processingFlag", Boolean.FALSE)	
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
				.getResultList();
		
		if ( ! medProfileList.isEmpty()) {
			MedProfile managedMedProfile = medProfileList.get(0);
			if ( ! managedMedProfile.getVersion().equals(medProfileVersion)) {
				return "0307";
			}
		} else {
			return "0307";
		}
		
		return null;
	}
	
	private void validatePivasWorklist(List<PivasWorklist> pivasWorklistList, boolean checkWorklistVersion) {
		// note: error code and error info are saved in PivasWorklist  
		
		List<PivasPrepCriteria> pivasPrepCriteriaList = new ArrayList<PivasPrepCriteria>();
		
		Map<String, PivasWorklist> pivasWorklistByUuidMap = new HashMap<String, PivasWorklist>();
		
		// clear error code which is set in previous validation
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			pivasWorklist.setInfoCode(null);
			pivasWorklist.setInfoParamList(null);
			pivasWorklist.setErrorCode(null);
			pivasWorklist.setErrorParamList(null);
			pivasWorklist.setErrorRemark(null);
		}		

		// check formula status exception by DMS and save result
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			PivasPrepMethod pivasPrepMethod = pivasWorklist.getPivasPrepMethod();
			
			if (pivasPrepMethod.getPivasFormulaMethod() == null) {
				continue;
			}
			
			PivasPrepCriteria pivasprepValidateCriteria = new PivasPrepCriteria();
			
			String pivasWorklistUuid = UUID.randomUUID().toString();
			pivasWorklistByUuidMap.put(pivasWorklistUuid, pivasWorklist);
			
			pivasprepValidateCriteria.setPivasWorklistUuid(pivasWorklistUuid);
			pivasprepValidateCriteria.setPivasFormulaMethodId(pivasPrepMethod.getPivasFormulaMethodId());
			pivasprepValidateCriteria.setHospCode(workstore.getHospCode());
			pivasprepValidateCriteria.setWorkstoreCode(workstore.getWorkstoreCode());
			pivasprepValidateCriteria.setWorkstoreGroupCode(workstore.getWorkstoreGroup().getWorkstoreGroupCode());
			pivasprepValidateCriteria.setDiluentItemCode(pivasPrepMethod.getDiluentItemCode());
			pivasprepValidateCriteria.setDiluentManufCode(pivasPrepMethod.getDiluentManufCode());
			pivasprepValidateCriteria.setWarnCode1(pivasPrepMethod.getWarnCode1());
			pivasprepValidateCriteria.setWarnCode2(pivasPrepMethod.getWarnCode2());
			pivasprepValidateCriteria.setPivasPrepCriteriaDetailList(new ArrayList<PivasPrepCriteriaDetail>());
			
			for ( PivasPrepMethodItem pivasPrepMethodItem : pivasPrepMethod.getPivasPrepMethodItemList() ) {
				PivasPrepCriteriaDetail criteriaDetail = new PivasPrepCriteriaDetail();
				criteriaDetail.setPivasFormulaMethodDrugId(pivasPrepMethodItem.getPivasFormulaMethodDrugId());
				criteriaDetail.setSolventItemCode(pivasPrepMethodItem.getSolventItemCode());
				criteriaDetail.setSolventManufCode(pivasPrepMethodItem.getSolventManufCode());
				pivasprepValidateCriteria.getPivasPrepCriteriaDetailList().add(criteriaDetail);
			}
			
			pivasPrepCriteriaList.add(pivasprepValidateCriteria);
		}
		
		List<PivasPrepValidateResponse> pivasPrepValidateResponseList = dmsPmsServiceProxy.validatePivasPrepMethod(pivasPrepCriteriaList);
		
		Map<PivasWorklist, PivasPrepValidateResponse> pivasPrepValidateResponseMapByPivasWorklistMap = new HashMap<PivasWorklist, PivasPrepValidateResponse>();
		for (PivasPrepValidateResponse pivasPrepValidateResponse : pivasPrepValidateResponseList) {
			PivasWorklist pivasWorklist = pivasWorklistByUuidMap.get(pivasPrepValidateResponse.getPivasWorklistUuid());
			pivasPrepValidateResponseMapByPivasWorklistMap.put(pivasWorklist, pivasPrepValidateResponse);
		}

		// check exception
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			PivasPrep pivasPrep = pivasWorklist.getPivasPrep();
			PivasPrepMethod pivasPrepMethod = pivasWorklist.getPivasPrepMethod();
			PivasPrepMethodItem pivasPrepMethodItem = pivasWorklist.getPivasPrepMethod().getPivasPrepMethodItemList().get(0);
			MedProfile medProfile = pivasWorklist.getMedProfile();
			
			boolean validWorklist; 
			
			if (checkWorklistVersion) {
				String errorCode = validatePivasWorklistVersion(medProfile.getId(), medProfile.getVersion());
				if (errorCode != null) {
					pivasWorklist.setErrorCode(errorCode);
					continue;
				}
			}
			
			validateInfoAndMaxDoseIcon(pivasWorklist);
			
			validWorklist = validateItemType(pivasWorklist);
			if ( ! validWorklist) {
				continue;
			}
			
			validWorklist = validateFormula(pivasWorklist);
			if ( ! validWorklist) {
				continue;
			}
			
			PivasPrepValidateResponse pivasPrepValidateResponse = pivasPrepValidateResponseMapByPivasWorklistMap.get(pivasWorklist);
			if (pivasPrepValidateResponse == null) {
				pivasWorklist.setErrorCode("0891");
				continue;
				
			} else {	// formula status exception from DMS checking
				if ( ! pivasPrepValidateResponse.getValidationResult() && ("0956".equals(pivasPrepValidateResponse.getErrorCode()) ||
							"0983".equals(pivasPrepValidateResponse.getErrorCode()) || "0957".equals(pivasPrepValidateResponse.getErrorCode())) ) {	
					// drug item and manufacturer error from DMS checking in highest priority
					pivasWorklist.setErrorCode(pivasPrepValidateResponse.getErrorCode());
					pivasWorklist.setErrorParamList(pivasPrepValidateResponse.getErrorParamList());
					pivasWorklist.setErrorRemark(pivasPrepValidateResponse.getRemark());
					continue;
				}
				
				// check solvent code non-empty
				if (pivasWorklist.getPivasPrepMethod().getPivasFormulaMethod() != null) {
					PivasFormulaMethod pivasFormulaMethod = pivasWorklist.getPivasPrepMethod().getPivasFormulaMethod();
					PivasDrugManuf pivasDrugManuf = pivasFormulaMethod.getFirstPivasFormulaMethodDrug().getPivasDrugManufMethod().getPivasDrugManuf();
					PivasDrugManufMethod pivasDrugManufMethod = pivasFormulaMethod.getFirstPivasFormulaMethodDrug().getPivasDrugManufMethod();
					
					if (pivasDrugManuf.getRequireSolventFlag() && ! StringUtils.equals(pivasDrugManufMethod.getDiluentCode(), VEND_SUPPLY_SOLV)) { 
						if (StringUtils.isEmpty(pivasPrepMethodItem.getSolventItemCode())) {
							pivasWorklist.setErrorCode("0881");
							continue;
						}
					}
				}

				// check diluent code non-empty
				if (pivasPrep.getPivasFormula() != null) {
					PivasFormula pivasFormula = pivasPrep.getPivasFormula();
					
					if ( ! StringUtils.isEmpty(pivasFormula.getDiluentCode())) {
						if (StringUtils.isEmpty(pivasPrepMethod.getDiluentItemCode())) {
							pivasWorklist.setErrorCode("0882");
							continue;
						}
					}
				}
				
				// other exception from DMS checking
				if ( ! pivasPrepValidateResponse.getValidationResult()) {
					pivasWorklist.setErrorCode(pivasPrepValidateResponse.getErrorCode());
					pivasWorklist.setErrorParamList(pivasPrepValidateResponse.getErrorParamList());
					pivasWorklist.setErrorRemark(pivasPrepValidateResponse.getRemark());
					continue;
				}
			}
			
			validWorklist = validateFormulaExpiryDate(pivasWorklist);
			if ( ! validWorklist) {
				continue;
			}
			
			validWorklist = validatePivasSchedule(pivasWorklist);
			if ( ! validWorklist) {
				continue;
			}

			validWorklist = validatePivasDetail(pivasWorklist);
			if ( ! validWorklist) {
				continue;
			}
			
			validWorklist = validateMaterialRequest(pivasWorklist);
			if ( ! validWorklist) {
				continue;
			}
		}
	}
	
	private boolean validateItemType(PivasWorklist pivasWorklist) {
		MedProfile medProfile = pivasWorklist.getMedProfile();
		MedProfileMoItem medProfileMoItem = pivasWorklist.getMedProfileMoItem();
		
		if (medProfileMoItem.getRxItemType() != RxItemType.Iv) {
			List<DoseGroup> doseGroupList;
			
			if (medProfileMoItem.isManualItem()) {
				MedProfilePoItem medProfilePoItem = medProfileMoItem.getMedProfilePoItemList().get(0);
				doseGroupList = medProfilePoItem.getRegimen().getDoseGroupList();
			} else {
				RxDrug rxDrug = (RxDrug)medProfileMoItem.getRxItem();
				doseGroupList = rxDrug.getRegimen().getDoseGroupList();
			}

			if (doseGroupList.size() > 1 || doseGroupList.get(0).getDoseList().size() > 1) {
				pivasWorklist.setErrorCode("0947");
				pivasWorklist.setAllowEditFlag(Boolean.FALSE);
				return false;
			}
		}
		
		if (medProfileMoItem.getRxItemType() == RxItemType.Iv) {
			IvRxDrug ivRxDrug = (IvRxDrug)medProfileMoItem.getRxItem();
			
			if (ivRxDrug.getIvAdditiveList().size() > 1 || ivRxDrug.getIvFluidList().size() > 1) {
				pivasWorklist.setErrorCode("0948");
				pivasWorklist.setAllowEditFlag(Boolean.FALSE);
				return false;
			}
		}
		
		if (StringUtils.isEmpty(medProfile.getWardCode())) {
			pivasWorklist.setErrorCode("0982");
			pivasWorklist.setAllowEditFlag(Boolean.FALSE);
			return false;
		}
		
		return true;
	}

	private void validateInfoAndMaxDoseIcon(PivasWorklist pivasWorklist) {
		PivasPrep pivasPrep = pivasWorklist.getPivasPrep();
		
		BigDecimal orderDosage = pivasWorklistManager.extractOrderDosage(pivasWorklist.getMedProfileMoItem());

		if (pivasWorklist.getDueDate() != null) {
			Date currentDate = new Date();
			Integer currentBufferValue = PIVAS_CURRENTDATE_BUFFER.get(workstore.getHospital());;
			Date currentBufferDate = new DateTime(currentDate).minusHours(currentBufferValue).toDate();

			// validate due date against current date
			boolean validDueDateCurrentDate = false;
			if ( ! pivasWorklist.getRefillFlag()) {
				if (pivasWorklist.getDueDate().compareTo(currentBufferDate) >= 0) {
					validDueDateCurrentDate = true;
				}
			} else {
				if (pivasWorklist.getDueDate().compareTo(currentDate) >= 0) {
					validDueDateCurrentDate = true;
				}
			}
			
			// validate due date against supply date
			boolean validDueDateSupplyDate = false;
			if (pivasWorklist.getSupplyDate() != null) {
				Integer supplyBufferValue = PIVAS_SUPPLYDATE_BUFFER.get(workstore.getHospital());
				Date supplyBufferDate = new DateTime(pivasWorklist.getSupplyDate()).minusHours(supplyBufferValue).toDate();
				
				if (pivasWorklist.getDueDate().compareTo(supplyBufferDate) >= 0) {
					validDueDateSupplyDate = true;
				}
			}
			
			// if due date is valid against current date and supply date, check to show info icon 
			if (validDueDateCurrentDate && validDueDateSupplyDate) {
				if (pivasWorklist.getDueDate().compareTo(currentDate) < 0) {
					pivasWorklist.setInfoCode("0961");
				}
				if (pivasWorklist.getSupplyDate() != null && pivasWorklist.getDueDate().compareTo(pivasWorklist.getSupplyDate()) < 0) {
					pivasWorklist.setInfoCode("0961");
				}
			}
		}
		
		if (orderDosage != null && pivasPrep.getUpperDoseLimit() != null && 
				orderDosage.compareTo(pivasPrep.getUpperDoseLimit()) > 0) {
			pivasWorklist.setExceedDoseLimitCode("0962");
		}
	}
	
	private boolean validateFormula(PivasWorklist pivasWorklist) {
		PivasPrep pivasPrep = pivasWorklist.getPivasPrep();
		
		if (pivasPrep.getPivasFormulaId() == null) {
			pivasWorklist.setErrorCode("0949");
			return false;
		}

		PivasFormula pivasFormula = pivasPrep.getPivasFormula();
		
		String dosageUnit = pivasWorklistManager.extractOrderDosageUnit(pivasWorklist.getMedProfileMoItem());
		String siteCode = pivasWorklistManager.extractOrderSiteCode(pivasWorklist.getMedProfileMoItem());
		
		if ( ! pivasFormula.getFirstPivasFormulaDrug().getPivasDosageUnit().equals(dosageUnit) ||
				! pivasFormula.getSiteCode().equals(siteCode)) {
			pivasWorklist.setErrorCode("0955");
			return false;
		}
		
		return true;
	}
	
	private boolean validateFormulaExpiryDate(PivasWorklist pivasWorklist) {
		PivasPrep pivasPrep = pivasWorklist.getPivasPrep();
		PivasPrepMethod pivasPrepMethod = pivasWorklist.getPivasPrepMethod();
		PivasPrepMethodItem pivasPrepMethodItem = pivasWorklist.getPivasPrepMethod().getPivasPrepMethodItemList().get(0);
		
		PivasFormula pivasFormula = pivasPrep.getPivasFormula();
		PivasFormulaMethod pivasFormulaMethod = pivasPrepMethod.getPivasFormulaMethod();
		PivasDrugManuf pivasDrugManuf = pivasFormulaMethod.getFirstPivasFormulaMethodDrug().getPivasDrugManufMethod().getPivasDrugManuf();
		PivasDrugManufMethod pivasDrugManufMethod = pivasFormulaMethod.getFirstPivasFormulaMethodDrug().getPivasDrugManufMethod();
		
		if (StringUtils.isEmpty(pivasPrepMethodItem.getDrugBatchNum()) || pivasPrepMethodItem.getDrugExpiryDate() == null) {
			pivasWorklist.setErrorCode("0927");
			return false;
		}
		
		if (pivasDrugManuf.getRequireSolventFlag()) { 
			boolean vendorSupplySolvent = StringUtils.equals(pivasDrugManufMethod.getDiluentCode(), VEND_SUPPLY_SOLV);
				 
			if ( StringUtils.isEmpty(pivasPrepMethodItem.getSolventBatchNum()) ) {
				if ( ! vendorSupplySolvent || pivasPrepMethodItem.getSolventExpiryDate() != null) {
					pivasWorklist.setErrorCode("0927");
					return false;
				}
			}
			
			if (pivasPrepMethodItem.getSolventExpiryDate() == null) {
				if ( ! vendorSupplySolvent || ! StringUtils.isEmpty(pivasPrepMethodItem.getSolventBatchNum()) ) {
					pivasWorklist.setErrorCode("0927");
					return false;
				}
			}
		}
		
		if ( ! StringUtils.isEmpty(pivasFormula.getDiluentCode()) ) {
			if (StringUtils.isEmpty(pivasPrepMethod.getDiluentBatchNum()) || pivasPrepMethod.getDiluentExpiryDate() == null) {
				pivasWorklist.setErrorCode("0927");
				return false;
			}
		}
		
		if (pivasPrepMethodItem.getDrugExpiryDate() != null && pivasWorklist.getExpiryDate() != null && pivasPrepMethodItem.getDrugExpiryDate().compareTo(pivasWorklist.getExpiryDate()) <= 0) {
			pivasWorklist.setErrorCode("0927");
			return false;
		}
		
		if (pivasPrepMethodItem.getSolventExpiryDate() != null && pivasWorklist.getExpiryDate() != null && pivasFormulaMethod != null && pivasFormulaMethod.getFirstPivasFormulaMethodDrug().getPivasDrugManufMethod().getPivasDrugManuf().getRequireSolventFlag() &&
				pivasPrepMethodItem.getSolventExpiryDate().compareTo(pivasWorklist.getExpiryDate()) <= 0) {
			pivasWorklist.setErrorCode("0927");
			return false;
		}
		
		if (pivasPrepMethod.getDiluentExpiryDate() != null && pivasWorklist.getExpiryDate() != null && pivasFormula != null && pivasFormula.getDiluentCode() != null && 
				pivasPrepMethod.getDiluentExpiryDate().compareTo(pivasWorklist.getExpiryDate()) <= 0) {
			pivasWorklist.setErrorCode("0927");
			return false;
		}
		
		return true;
	}

	private boolean validatePivasSchedule(PivasWorklist pivasWorklist) {
		if (pivasWorklist.getShelfLife() == null) {
			pivasWorklist.setErrorCode("0980");
			return false;
		}
		
		if (pivasWorklist.getDueDate() != null) {
			Date currentDate = new Date();
			Integer currentBufferValue = PIVAS_CURRENTDATE_BUFFER.get(workstore.getHospital());;
			Date currentBufferDate = new DateTime(currentDate).minusHours(currentBufferValue).toDate();
			
			if ( ! pivasWorklist.getRefillFlag()) {
				if (pivasWorklist.getDueDate().compareTo(currentBufferDate) < 0) {
					pivasWorklist.setErrorCode("0934");
					return false;
				}
			} else {
				if (pivasWorklist.getDueDate().compareTo(currentDate) < 0) {
					pivasWorklist.setErrorCode("0934");
					return false;
				}
			}

			if (pivasWorklist.getSupplyDate() != null) {
				Integer supplyBufferValue = PIVAS_SUPPLYDATE_BUFFER.get(workstore.getHospital());
				Date supplyBufferDate = new DateTime(pivasWorklist.getSupplyDate()).minusHours(supplyBufferValue).toDate();
				
				if (pivasWorklist.getDueDate().compareTo(supplyBufferDate) < 0) {
					// refill worklist may also be invalid if supply buffer rule value is reduced 
					pivasWorklist.setErrorCode("0934");
					return false;
				}
			}
		}

		return true;
	}
	
	private boolean validatePivasDetail(PivasWorklist pivasWorklist) {
		PivasPrep pivasPrep = pivasWorklist.getPivasPrep();
		
		if (pivasPrep.getFreqInterval() == null) {
			pivasWorklist.setErrorCode("0963");
			return false;
		}
		
		if (pivasWorklist.getIssueQty().compareTo(BigDecimal.valueOf(999)) > 0) {
			pivasWorklist.setErrorCode("0964");
			return false;
		}

		if (pivasPrep.getVolume().compareTo(BigDecimal.valueOf(9999.99)) > 0) {
			pivasWorklist.setErrorCode("0965");
			return false;
		}
		
		return true;
	}
	
	private boolean validateMaterialRequest(PivasWorklist pivasWorklist) {
		if (pivasWorklist.getIssueQty().equals(BigDecimal.valueOf(0)) && pivasWorklist.getAdjQty().equals(BigDecimal.valueOf(0)) &&
				pivasWorklist.getMaterialRequest() == null) {
			pivasWorklist.setErrorCode("0966");
			return false;
		}
		
		if (pivasWorklist.getMaterialRequest() != null) {
			MaterialRequestItem materialRequestItem = pivasWorklist.getMaterialRequest().getMaterialRequestItemList().get(0);
			if (materialRequestItem.getItemCode() == null) {
				pivasWorklist.setErrorCode("0967");
				return false;
			}
			
			WorkstoreDrug workstoreDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, materialRequestItem.getItemCode());
			if (workstoreDrug.getMsWorkstoreDrug() == null || ! "A".equals(workstoreDrug.getMsWorkstoreDrug().getItemStatus()) ) {
				pivasWorklist.setErrorCode("0967");
				return false;
			}
		}
		
		return true;
	}
	
	private List<PivasWorklist> extractValidPivasWorklist(List<PivasWorklist> pivasWorklistList) {
		List<PivasWorklist> validPivasWorklistList = new ArrayList<PivasWorklist>();
		
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			boolean validPivasWorklist = true;

			PivasPrep pivasPrep = pivasWorklist.getPivasPrep();
			MedProfileMoItem medProfileMoItem = pivasWorklist.getMedProfileMoItem();
			
			if (pivasWorklist.getErrorCode() != null || medProfileMoItem.getSuspendCode() != null || medProfileMoItem.getPendingCode() != null) {
				validPivasWorklist = false;
			} else if (pivasWorklist.getExceedDoseLimitCode() != null && pivasPrep.getCheckDoseLimitFlag()) {
				validPivasWorklist = false;
			} else if (pivasWorklist.getInfoCode() == "0961" && (( ! pivasWorklist.getRefillFlag() && pivasPrep.getCheckDueDateFlag()) || pivasWorklist.getAdHocFlag()) ) {
				// for new worklist, user need to confirm once before adding to worklist (checkDueDateFlag is used only in this case)
				// for refill worklist, always skip user confirmation before adding to worklist
				// for ad-hoc worklist, always require user confirmation before adding to worklist (for safety only since this function is not used by ad-hoc worklist)  
				validPivasWorklist = false;
			}
			
			if (validPivasWorklist) {
				validPivasWorklistList.add(pivasWorklist);
			}
		}
		
		return validPivasWorklistList;
	}
	
	private PivasWorklistResponse validateAddToWorklist(List<PivasWorklist> pivasWorklistList) {
		PivasWorklistResponse pivasWorklistResponse = new PivasWorklistResponse();

		Date worklistSupplyDate = null;
		
		// check supply date match with worklist one
		List<PivasWorklist> addedPivasWorklistList = retrievePreparedPivasWorklist(false);
		
		if ( ! addedPivasWorklistList.isEmpty()) {
			worklistSupplyDate = addedPivasWorklistList.get(0).getSupplyDate();
		}
		
		if (worklistSupplyDate != null) {
			for (PivasWorklist pivasWorklist : pivasWorklistList) {
				if ( ! pivasWorklist.getSupplyDate().equals(worklistSupplyDate)) {
					pivasWorklistResponse.setErrorCode("0919");
					
					DateFormat dateTimeFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
					List<String> errorParamList = new ArrayList<String>();
					errorParamList.add(dateTimeFormat.format(worklistSupplyDate));
					errorParamList.add(dateTimeFormat.format(pivasWorklist.getSupplyDate()));
					
					pivasWorklistResponse.setErrorParamList(errorParamList);
					
					return pivasWorklistResponse;
				}
			}
		}
		
		return pivasWorklistResponse;
	}

	private void calculatePivasInfo(boolean recalculate, PivasWorklist pivasWorklist, List<MedProfilePoItem> mrMedProfilePoItemList, boolean changeSupplyDateFlag, boolean changeSatelliteFlag) {
		MedProfileMoItem medProfileMoItem = pivasWorklist.getMedProfileMoItem();
		PivasPrep pivasPrep = pivasWorklist.getPivasPrep();
		PivasFormula pivasFormula = pivasWorklist.getPivasPrep().getPivasFormula();
		
		
		// volume (by formula change)
		if (pivasFormula != null) {
			BigDecimal orderDosage = pivasWorklistManager.extractOrderDosage(pivasWorklist.getMedProfileMoItem());
			
			if ( "ML".equals(pivasFormula.getFirstPivasFormulaDrug().getPivasDosageUnit()) ) {
				pivasPrep.setVolume(orderDosage.divide(pivasFormula.getRate(), 2, RoundingMode.HALF_UP));
			} else {
				pivasPrep.setVolume(orderDosage.divide(pivasFormula.getConcn(), 2, RoundingMode.HALF_UP));
			}
		}
		
		// drug disp qty (by formula method change)
		calculateDrugCalQty(pivasWorklist);

		// split dosage and volume (by split count change)
		calculateSplitDosageVolume(Arrays.asList(new PivasWorklist[]{pivasWorklist}));

		// end date for IPMOE dose specified order (by freq interval change)
		// (note: since due date is input by user and can be after start date (especially in case of first dose prescribed),
		//        the calculated end date is before due date + # of dose * freq interval, and the total PIVAS dose maybe smaller than specified dose.
		//        But this is rare case and is ignored)
		Integer dispQty = pivasWorklistManager.extractOrderDispQty(pivasWorklist.getMedProfileMoItem());
		
		if ( ! pivasWorklist.getMedProfileMoItem().isManualItem() && pivasWorklist.getDueDate() != null && 
				dispQty != null && dispQty > 0 && 
				pivasPrep.getFreqInterval() != null && pivasPrep.getFreqInterval().compareTo(BigDecimal.valueOf(0)) > 0 ) {
			int freqIntervalInMinutes = pivasPrep.getFreqInterval().multiply(BigDecimal.valueOf(60)).setScale(0, RoundingMode.HALF_UP).intValue();
			int durationInMinutes = dispQty * freqIntervalInMinutes;
			Date calEndDate = new DateTime(medProfileMoItem.getStartDate()).plusMinutes(durationInMinutes).minusMinutes(1).toDate();
			pivasPrep.setCalEndDate(calEndDate);
		} else {
			pivasPrep.setCalEndDate(null);
		}
		
		// init expiry date and satellite related info (by supply date or satellite) 
		if (changeSupplyDateFlag) {
			initPivasWorklistBySupplyDateChange(pivasWorklist, ! recalculate);
		} else if (changeSatelliteFlag) {
			initPivasWorklistBySatelliteFlagChange(pivasWorklist);
		}
		
		// issue qty and next due date
		calculatePivasQuantity(pivasWorklist, mrMedProfilePoItemList);
	}
	
	private void calculateDrugCalQty(PivasWorklist pivasWorklist) {
		PivasPrepMethodItem pivasPrepMethodItem = pivasWorklist.getPivasPrepMethod().getPivasPrepMethodItemList().get(0);
		
		if (pivasPrepMethodItem.getDrugDmDrug() == null) {
			return;
		}

		BigDecimal orderDosage = pivasWorklistManager.extractOrderDosage(pivasWorklist.getMedProfileMoItem());
		
		DmMoeProperty dmMoeProperty = pivasPrepMethodItem.getDrugDmDrug().getDmMoeProperty();
		if (orderDosage != null && dmMoeProperty.getDduToMduRatio() != null && dmMoeProperty.getBaseunitToDduRatio() != null) {
			BigDecimal dduToMduRatio = BigDecimal.valueOf(dmMoeProperty.getDduToMduRatio());
			BigDecimal baseunitToDduRatio = BigDecimal.valueOf(dmMoeProperty.getBaseunitToDduRatio());
			BigDecimal drugCalQtyPerDose = orderDosage.divide( dduToMduRatio.multiply(baseunitToDduRatio), 4, RoundingMode.HALF_UP );
			
			pivasPrepMethodItem.setDrugCalQty(drugCalQtyPerDose);
		}
	}

	private void calculateDrugDispQty(PivasWorklist pivasWorklist) {
		PivasPrepMethodItem pivasPrepMethodItem = pivasWorklist.getPivasPrepMethod().getPivasPrepMethodItemList().get(0);
		
		if (pivasPrepMethodItem.getDrugCalQty() == null || pivasWorklist.getIssueQty() == null || pivasWorklist.getAdjQty() == null) {	
			// for safety only
			return;
		}
		
		if (PIVAS_DRUGDISPQTY.get(workstore.getHospital()) != null) {
			// if default value is set by rule value, no need to recalculate
			return;
		}
		
		Integer roundingModeByRule = PIVAS_DRUGCALQTY_ROUNDINGMODE.get(workstore.getHospital());
		BigDecimal roundDrugCalQty = pivasPrepMethodItem.getDrugCalQty().setScale(0, roundingModeByRule);
		
		BigDecimal supplyQty = pivasWorklist.getIssueQty().add(pivasWorklist.getAdjQty());
		BigDecimal drugDispQty = roundDrugCalQty.multiply(supplyQty);
		
		pivasPrepMethodItem.setDrugDispQty(drugDispQty);
	}
	
	private void calculateSplitDosageVolume(List<PivasWorklist> pivasWorklistList) {
		for (PivasWorklist pivasWorklist : pivasWorklistList) {
			PivasPrep pivasPrep = pivasWorklist.getPivasPrep();
			PivasFormula pivasFormula = pivasWorklist.getPivasPrep().getPivasFormula();
	
			BigDecimal orderDosage = pivasWorklistManager.extractOrderDosage(pivasWorklist.getMedProfileMoItem());
			
			BigDecimal splitCount = BigDecimal.valueOf(pivasPrep.getSplitCount());
			pivasPrep.setSplitDosage(orderDosage.divide(splitCount, 4, RoundingMode.HALF_UP));
			if (pivasFormula != null) {
				pivasPrep.setSplitVolume(pivasPrep.getVolume().divide(splitCount, 2, RoundingMode.HALF_UP));
			}
		}
	}
	
	private void calculatePivasQuantity(PivasWorklist pivasWorklist, List<MedProfilePoItem> mrMedProfilePoItemList) {
		PivasPrep pivasPrep = pivasWorklist.getPivasPrep();
		
		// save next supply date for front-end validation of due date
		if (pivasWorklist.getSupplyDate() != null) {
			pivasWorklist.setCheckNextSupplyDate( calculateNextRefillSupplyDate(pivasWorklist.getSupplyDate()) );
		}
		
		// validate input
		if ( pivasPrep.getFreqInterval() == null || pivasWorklist.getShelfLife() == null || pivasWorklist.getSupplyDate() == null || 
				pivasWorklist.getDueDate() == null || pivasWorklist.getExpiryDate() == null || ! validatePivasWorklistDueDateSupplyDate(pivasWorklist) ) {
			pivasWorklist.setNextDueDate(null);
			pivasWorklist.setNextSupplyDate(null);
			if (pivasWorklist.getMaterialRequest() != null) {
				MaterialRequest materialRequest = pivasWorklist.getMaterialRequest();
				for (MaterialRequestItem materialRequestItem : materialRequest.getMaterialRequestItemList()) {
					materialRequestItem.setDueDate(null);
				}
			}			
			return;
		}
		
		// get current gen time
		Date currentGenDate = new Date();
		
		// calculate adjust supply date for refill worklist
		// (note: for refill worklist retrieve, supply date is already init by search supply date in initPivasWorklistForRefillWorklist()
		//        for front-end recal, supply date of refill worklist cannot be changed
		//        so no need to init here)
		
		// get next refill supply date
		Date nextRefillSupplyDate = calculateNextRefillSupplyDate(pivasWorklist.getSupplyDate());
		
		// get supply buffer
		Integer supplyBufferValue = PIVAS_SUPPLYDATE_BUFFER.get(workstore.getHospital());
		Date supplyBufferDate = new DateTime(pivasWorklist.getSupplyDate()).minusHours(supplyBufferValue).toDate(); 

		// adjust due date for refill worklist if necessary
		if (pivasWorklist.getRefillFlag()) {
			if (pivasPrep.getFreqInterval().compareTo(BigDecimal.valueOf(0)) <= 0) {
				throw new UnsupportedOperationException("Freq interval 0 worklist must be single so should not be refill worklist, pivasWorklistId = " + pivasWorklist.getId() + ", pivasPrepId = " + pivasPrep.getId());
			}
			
			Date adjustedDueDate = pivasWorklist.getDueDate();
			
			Integer freqIntervalInMinutes = pivasPrep.getFreqInterval().multiply(BigDecimal.valueOf(60)).setScale(0, RoundingMode.HALF_UP).intValue();
			
			while (adjustedDueDate.compareTo(supplyBufferDate) < 0 || adjustedDueDate.compareTo(currentGenDate) < 0) {
				adjustedDueDate = new DateTime(adjustedDueDate).plusMinutes(freqIntervalInMinutes).toDate();
			}
			
			if ( (calculatePivasEndDate(pivasWorklist) != null && adjustedDueDate.compareTo(calculatePivasEndDate(pivasWorklist)) > 0) ||
					adjustedDueDate.compareTo(nextRefillSupplyDate) > 0 ) {
				// filter out from worklist list if due date is already after end date or next refill supply date
				pivasWorklist.setFilterFlag(Boolean.TRUE);
				return;
			}
			
			if ( ! adjustedDueDate.equals(pivasWorklist.getDueDate())) {
				pivasWorklist.setDueDate(adjustedDueDate);
			}
		}
		
		// calculate PIVAS issue qty, PIVAS request issue qty and next due date 
		int issueQty = 0;
		int mrIssueQty = 0;
		
		Integer freqIntervalInMinutes = pivasPrep.getFreqInterval().multiply(BigDecimal.valueOf(60)).setScale(0, RoundingMode.HALF_UP).intValue();
		Date nextDueDate = pivasWorklist.getDueDate();	// init next due date for worklist with all PIVAS request quentity

		if (pivasPrep.getFreqInterval().compareTo(BigDecimal.valueOf(0)) <= 0) {
			// all freqInterval = 0 worklist must be single, so even next due date is set to due date, the single worklist is filtered by 
			// refill worklist retrieve next time
			issueQty = 1;
			
		} else {
			int count = 0;

			Date checkDueDate = pivasWorklist.getDueDate();
			
			Date endDueDate;
			if (pivasPrep.getSingleDispFlag() && calculatePivasEndDate(pivasWorklist) != null) {
				endDueDate = calculatePivasEndDate(pivasWorklist);
			} else if (calculatePivasEndDate(pivasWorklist) != null && calculatePivasEndDate(pivasWorklist).compareTo(nextRefillSupplyDate) <= 0) {
				endDueDate = calculatePivasEndDate(pivasWorklist);
			} else {
				endDueDate = nextRefillSupplyDate;
			}

			while ( count < PivasWorklistManagerBean.MAX_SEARCH_DAY && checkDueDate.compareTo(endDueDate) <= 0) { 
				// continue to loop when:
				// 1. checking date <= next refill supply date, which ensures the next due date is after next refill supply date, and 
				// 2. checking date <= order end date, which ensures next due date does not exceed PIVAS end date  
				count++;
	
				if ( (pivasWorklist.getShelfLife() > 0 && checkDueDate.compareTo(pivasWorklist.getExpiryDate()) <= 0) || 
						(pivasWorklist.getShelfLife() == 0 && issueQty == 0) ) {
					// if shelf life = 0, set issueQty to 1 and other quantity as mrIssueQty
					issueQty++;
					nextDueDate = new DateTime(checkDueDate).plusMinutes(freqIntervalInMinutes).toDate();
				} else {
					mrIssueQty++;
				}
				
				checkDueDate = new DateTime(checkDueDate).plusMinutes(freqIntervalInMinutes).toDate();
			}
			
			if (count >= PivasWorklistManagerBean.MAX_SEARCH_DAY) {
				throw new UnsupportedOperationException("Count is over 365 days when calculating issue quantity, pivasWorklist id = " + pivasWorklist.getId() + ", pivasPrep id = " + pivasPrep.getId());
			}
		}
		
		// update quantity and next schedule 
		pivasWorklist.setNextDueDate(nextDueDate);
		if ( ! checkPivasWorklistEnded(pivasWorklist, nextRefillSupplyDate)) {
			pivasWorklist.setNextSupplyDate(nextRefillSupplyDate);
		} else {
			pivasWorklist.setNextSupplyDate(null);
		}
		
		pivasWorklist.setIssueQty(BigDecimal.valueOf(issueQty));
		pivasWorklist.setMrIssueQty(BigDecimal.valueOf(mrIssueQty));
		
		// PIVAS request
		if (PIVAS_REQUEST_ENABLED.get(workstore.getHospital()) && pivasWorklist.getMrIssueQty().compareTo(BigDecimal.valueOf(0)) > 0) {
			MaterialRequest materialRequest = null;
			MaterialRequestItem materialRequestItem = null;

			if (pivasWorklist.getPrevMaterialRequest() != null) {
				// if calculation is triggered by edit screen and saved MaterialRequest exists, get from backup and relink
				// (user may change PIVAS request from Y to N and then N to Y, and delink the saved MaterialRequest from PivasWorklist)
				materialRequest = pivasWorklist.getPrevMaterialRequest();
				materialRequestItem = pivasWorklist.getPrevMaterialRequest().getMaterialRequestItemList().get(0);
				// no need to link Hospital, MedProfile, MedProfileMoItem since they are not changed  
				
			} else {
				materialRequest = new MaterialRequest();
				materialRequestItem = new MaterialRequestItem();
				materialRequest.addMaterialRequestItem(materialRequestItem);
				materialRequestItem.setMaterialRequest(materialRequest);
				
				materialRequest.setHospital(pivasWorklist.getWorkstore().getHospital());
				materialRequestItem.setHospital(pivasWorklist.getWorkstore().getHospital());
			}

			pivasWorklist.setMaterialRequest(materialRequest);
			
			materialRequestItem.setStatus(MaterialRequestItemStatus.None);
			materialRequestItem.setDueDate(pivasWorklist.getNextDueDate());

			if (mrMedProfilePoItemList != null && mrMedProfilePoItemList.size() == 1) {
				MedProfilePoItem mrMedProfilePoItem = mrMedProfilePoItemList.get(0);
				
				materialRequestItem.setMedProfilePoItem(mrMedProfilePoItem);
				
				materialRequestItem.setItemCode(mrMedProfilePoItem.getItemCode());
				materialRequestItem.setBaseUnit(mrMedProfilePoItem.getBaseUnit());
				
				calculateMaterialRequestItemIssueQty(pivasWorklist);
				
			} else {
				materialRequestItem.setMedProfilePoItem(null);
				materialRequestItem.setItemCode(null);
				materialRequestItem.setBaseUnit(null);
				materialRequestItem.setIssueQty(BigDecimal.valueOf(0));
			}
		} else {
			pivasWorklist.setMaterialRequest(null);
		}
		
		// calculate number of label
		calculateNumOfLabel(pivasWorklist);
		
		// calculate issue qty for stock deduction 
		calculateDrugDispQty(pivasWorklist);
			
		return;
	}

	private Date calculateNextRefillSupplyDate(Date supplyDate) {
		Map<String, List<PivasServiceHour>> pivasServiceHourListByDayOfWeekMap = pivasServiceHourManager.retrievePivasServiceHourListByDayOfWeekMap(workstore.getWorkstoreGroup().getWorkstoreGroupCode(), workstore.getHospital());

		// retrieve holiday map
		
		Set<Date> pivasHolidayDateSet = pivasServiceHourManager.retrievePivasHolidayDateSet(workstore.getWorkstoreGroup().getWorkstoreGroupCode());
		
		// search for next supply date
		Date supplyDateWithoutTime = DateUtils.truncate(supplyDate, Calendar.DAY_OF_MONTH);
		DateTime searchDateDt = new DateTime(supplyDateWithoutTime);
		int hour = new DateTime(supplyDate).getHourOfDay();
		int minutes = new DateTime(supplyDate).getMinuteOfHour();
		Integer searchTime = hour * 100 + minutes;
		
		int count = 0;
		boolean nextSupplyDateFound = false;
		Date nextSupplyDate = null;
		
		while (count < PivasWorklistManagerBean.MAX_SEARCH_DAY) {
			count++;
			
			if ( ! pivasHolidayDateSet.contains(searchDateDt.toDate())) {
				int dayOfWeekValue = searchDateDt.getDayOfWeek();
				
				List<PivasServiceHour> dayPivasServiceHourList = pivasServiceHourListByDayOfWeekMap.get(String.valueOf(dayOfWeekValue));
				
				if (dayPivasServiceHourList != null) {
					for (PivasServiceHour pivasServiceHour : dayPivasServiceHourList) {
						if (pivasServiceHour.getType() == PivasServiceHourType.Refill) {
							if (searchTime == null || (searchTime != null && pivasServiceHour.getSupplyTime() > searchTime) ) {
								nextSupplyDateFound = true;
								int supplyHour = pivasServiceHour.getSupplyTime() / 100;
								int supplyMinutes = pivasServiceHour.getSupplyTime() % 100;
								nextSupplyDate = searchDateDt.plusHours(supplyHour).plusMinutes(supplyMinutes).toDate();
								break;
							}
						}
					}
					
					if (nextSupplyDateFound) {
						// exit looping
						break;
					}
				}
			}
			
			searchDateDt = searchDateDt.plusDays(1);
			searchTime = null;	// since next search date is after supply date, all time can be searched from next search date 
		}

		if (count >= PivasWorklistManagerBean.MAX_SEARCH_DAY) {
			throw new UnsupportedOperationException("Count is over 365 days when calculating next refill supply date, supplyDate = " + supplyDate);
		}
		
		return nextSupplyDate;
	}
	
	private Date calculatePivasEndDate(PivasWorklist pivasWorklist) {
		if (pivasWorklist.getMedProfileMoItem().getEndDate() != null) {
			return pivasWorklist.getMedProfileMoItem().getEndDate();
		} else if (pivasWorklist.getPivasPrep().getCalEndDate() != null) {
			return pivasWorklist.getPivasPrep().getCalEndDate();
		} else {
			return null;
		}
	}
	
	private boolean checkPivasWorklistEnded(PivasWorklist pivasWorklist, Date nextRefillSupplyDate) {
		PivasPrep pivasPrep = pivasWorklist.getPivasPrep();
		
		if ( pivasPrep.getSingleDispFlag() || pivasPrep.getFreqInterval().compareTo(BigDecimal.valueOf(0)) <= 0 ||	// for safety, all freqInterval = 0 worklist must be single
				(calculatePivasEndDate(pivasWorklist) != null && calculatePivasEndDate(pivasWorklist).compareTo(nextRefillSupplyDate) <= 0) ) {
			return true;
		} else {
			return false;
		}
		
	}

	private void calculateMaterialRequestItemIssueQty(PivasWorklist pivasWorklist) {
		if (pivasWorklist.getMrIssueQty() == null || pivasWorklist.getMaterialRequest() == null) {
			return;
		}
		
		MaterialRequestItem materialRequestItem = pivasWorklist.getMaterialRequest().getMaterialRequestItemList().get(0);
		
		DmMoeProperty dmMoeProperty = DmDrugCacher.instance().getDmDrug(materialRequestItem.getItemCode()).getDmMoeProperty();
		BigDecimal orderDosage = pivasWorklistManager.extractOrderDosage(pivasWorklist.getMedProfileMoItem()); 
		
		if (orderDosage == null || dmMoeProperty.getDduToMduRatio() == null || dmMoeProperty.getBaseunitToDduRatio() == null) {
			return;
		}

		BigDecimal dduToMduRatio = BigDecimal.valueOf(dmMoeProperty.getDduToMduRatio());
		BigDecimal baseunitToDduRatio = BigDecimal.valueOf(dmMoeProperty.getBaseunitToDduRatio());
		BigDecimal issueQtyPerDose = orderDosage.divide( dduToMduRatio.multiply(baseunitToDduRatio), 4, RoundingMode.HALF_UP );
		BigDecimal issueQty = issueQtyPerDose.setScale(0, RoundingMode.CEILING).multiply(pivasWorklist.getMrIssueQty());
		
		materialRequestItem.setIssueQty(issueQty);
	}
	
	private void calculateNumOfLabel(PivasWorklist pivasWorklist) {
		PivasPrep pivasPrep = pivasWorklist.getPivasPrep();
		PivasFormula pivasFormula = pivasWorklist.getPivasPrep().getPivasFormula();
		
		if (pivasFormula == null) {
			return;
		}
		
		int numOfLabel = pivasWorklist.getIssueQty().intValue() + pivasWorklist.getAdjQty().intValue();
		
		switch (pivasFormula.getLabelOptionType()) {
			case ExtraProductLabel:
				numOfLabel = numOfLabel + pivasFormula.getLabelOption();
				break;
			case ExtraSetLabel:
				numOfLabel = numOfLabel * (pivasFormula.getLabelOption() + 1);
				break;
			default:
				break;
		}
		numOfLabel = numOfLabel * pivasPrep.getSplitCount();
		
		pivasWorklist.setNumOfLabel(numOfLabel);
	}
	
	@Remove
	public void destroy() {

	}

	public static class PivasPrepMethodItemComparator implements Comparator<PivasPrepMethodItem>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(PivasPrepMethodItem pivasPrepMethodItem1, PivasPrepMethodItem pivasPrepMethodItem2) {
			return new CompareToBuilder()
						.append( pivasPrepMethodItem1.getDrugItemCode(), pivasPrepMethodItem2.getDrugItemCode() )
						.append( pivasPrepMethodItem1.getDrugManufCode(), pivasPrepMethodItem2.getDrugManufCode() )
						.append( pivasPrepMethodItem1.getDrugBatchNum(), pivasPrepMethodItem2.getDrugBatchNum() )
						.append( pivasPrepMethodItem1.getDrugExpiryDate(), pivasPrepMethodItem2.getDrugExpiryDate() )
						.append( pivasPrepMethodItem1.getSolventItemCode(), pivasPrepMethodItem2.getSolventItemCode() )
						.append( pivasPrepMethodItem1.getSolventManufCode(), pivasPrepMethodItem2.getSolventManufCode() )
						.append( pivasPrepMethodItem1.getSolventBatchNum(), pivasPrepMethodItem2.getSolventBatchNum() )
						.append( pivasPrepMethodItem1.getSolventExpiryDate(), pivasPrepMethodItem2.getSolventExpiryDate() )
						.append( pivasPrepMethodItem1.getDrugCalQty(), pivasPrepMethodItem2.getDrugCalQty() )
						.append( pivasPrepMethodItem1.getDrugDispQty(), pivasPrepMethodItem2.getDrugDispQty() )
						.toComparison();
		}
	}
}
