package hk.org.ha.model.pms.cache;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.dms.persistence.Company;

import java.util.List;

public interface CompanyCacherInf extends BaseCacherInf {

	Company getCompanyByCompanyCode(String companyCode);
	
	List<Company> getCompanyList();
	
	List<Company> getManufacturerList();
	
	List<Company> getCompanyListByCompanyCode(String prefixCompanyCode);
	
	List<Company> getManufacturerListByCompanyCode(String prefixCompanyCode);
}
