package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.persistence.corp.Institution;
import hk.org.ha.model.pms.persistence.phs.Ward;

import java.util.List;

import javax.ejb.Local;

@Local
public interface WardManagerLocal {
	
	List<String> retrieveWardCodeList();
	
	List<Ward> retrieveWardListByPrefixWardCode(String prefixWardCode);

	List<Ward> retrieveWardListByInstitution(Institution institution);
	
	List<Ward> retrieveWardListByInstCode( List<String> instCodeList );
	
	List<Ward> retrieveWardList();
	
	List<Ward> retrieveWardListByWardCodeList(List<String> wardCodeList);
}
