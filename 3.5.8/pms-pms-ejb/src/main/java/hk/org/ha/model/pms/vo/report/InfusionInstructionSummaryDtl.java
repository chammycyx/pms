package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class InfusionInstructionSummaryDtl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String infusionInstruction;
	
	private String specialInstruction;
	
	private List<InfusionInstructionSummaryItemDtl> infusionInstructionSummaryItemDtlList;

	public void setInfusionInstruction(String infusionInstruction) {
		this.infusionInstruction = infusionInstruction;
	}

	public String getInfusionInstruction() {
		return infusionInstruction;
	}

	public void setSpecialInstruction(String specialInstruction) {
		this.specialInstruction = specialInstruction;
	}

	public String getSpecialInstruction() {
		return specialInstruction;
	}

	public void setInfusionInstructionSummaryItemDtlList(
			List<InfusionInstructionSummaryItemDtl> infusionInstructionSummaryItemDtlList) {
		this.infusionInstructionSummaryItemDtlList = infusionInstructionSummaryItemDtlList;
	}

	public List<InfusionInstructionSummaryItemDtl> getInfusionInstructionSummaryItemDtlList() {
		return infusionInstructionSummaryItemDtlList;
	}
}
