package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.exception.sftp.SftpCommandException;

import javax.ejb.Local;

@Local
public interface HaDrugFormularyMgmtRptServiceLocal {

	void clearFmRptList();
	
	void retrieveFmRptList() throws SftpCommandException;
	
	void destroy();
	
}
