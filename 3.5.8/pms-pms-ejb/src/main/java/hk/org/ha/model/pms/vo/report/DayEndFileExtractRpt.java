package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class DayEndFileExtractRpt implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private DayEndFileExtractRptHdr dayEndFileExtractRptHdr;	
	private DayEndFileExtractRptDtl dayEndFileExtractRptDtl;
	public DayEndFileExtractRptHdr getDayEndFileExtractRptHdr() {
		return dayEndFileExtractRptHdr;
	}
	public void setDayEndFileExtractRptHdr(
			DayEndFileExtractRptHdr dayEndFileExtractRptHdr) {
		this.dayEndFileExtractRptHdr = dayEndFileExtractRptHdr;
	}
	public DayEndFileExtractRptDtl getDayEndFileExtractRptDtl() {
		return dayEndFileExtractRptDtl;
	}
	public void setDayEndFileExtractRptDtl(
			DayEndFileExtractRptDtl dayEndFileExtractRptDtl) {
		this.dayEndFileExtractRptDtl = dayEndFileExtractRptDtl;
	}

}
