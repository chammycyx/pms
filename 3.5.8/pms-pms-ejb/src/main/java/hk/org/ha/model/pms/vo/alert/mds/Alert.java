package hk.org.ha.model.pms.vo.alert.mds;

public interface Alert {

	String getAlertDesc();
	
	String getAlertTitle();

	Long getSortSeq();
	
	void setSortSeq(Long sortSeq);

	void loadDrugDesc(String drugDesc);
	
	void replaceItemNum(Integer oldItemNum, Integer newItemNum, String orderNum);
	
	void replaceOrderNum(String oldOrderNum, String newOrderNum);

	void restoreItemNum();
	
	String getKeys();
}
