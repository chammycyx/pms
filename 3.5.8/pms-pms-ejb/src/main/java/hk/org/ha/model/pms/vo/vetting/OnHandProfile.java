package hk.org.ha.model.pms.vo.vetting;

import java.io.Serializable;
import java.util.Date;

import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.udt.disp.RemarkStatus;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class OnHandProfile implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Date orderDate;	// for sorting
	
	private Long orderNum;	// for sorting
	
	private Integer orgItemNum;	// for sorting

	private String refNum;	// for sorting
	
	private Boolean remarkPendingFlag;	// for filtering
	
	private Boolean chargeableFlag;	//	used to show chargeable icon, not included in MedOrderItem 
	
	private Boolean mpDischargeFlag;	// for IP

	private RemarkStatus remarkOrderStatus;	// for IP

	private String itemStatus;	// for both OP and IP
	
	private Boolean remainingDayFlag;	// for both OP and IP
	
	private String specialInstruction;	// for OP display
	
	private String specialNote;	// for OP display
	
	private String drugOnHandOrderInfo;	// for OP display	
	
	private String drugOnHandDetail;	// for OP display	

	private String discontinueText;	// for OP display	
	
	private MedOrderItem medOrderItem;
	
	private String patHospCode;	// for getting FM
	
	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Long getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(Long orderNum) {
		this.orderNum = orderNum;
	}

	public Integer getOrgItemNum() {
		return orgItemNum;
	}

	public void setOrgItemNum(Integer orgItemNum) {
		this.orgItemNum = orgItemNum;
	}

	public String getRefNum() {
		return refNum;
	}

	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}

	public Boolean getRemarkPendingFlag() {
		return remarkPendingFlag;
	}

	public void setRemarkPendingFlag(Boolean remarkPendingFlag) {
		this.remarkPendingFlag = remarkPendingFlag;
	}

	public Boolean getChargeableFlag() {
		return chargeableFlag;
	}

	public void setChargeableFlag(Boolean chargeableFlag) {
		this.chargeableFlag = chargeableFlag;
	}

	public Boolean getMpDischargeFlag() {
		return mpDischargeFlag;
	}

	public void setMpDischargeFlag(Boolean mpDischargeFlag) {
		this.mpDischargeFlag = mpDischargeFlag;
	}

	public RemarkStatus getRemarkOrderStatus() {
		return remarkOrderStatus;
	}

	public void setRemarkOrderStatus(RemarkStatus remarkOrderStatus) {
		this.remarkOrderStatus = remarkOrderStatus;
	}

	public String getItemStatus() {
		return itemStatus;
	}

	public void setItemStatus(String itemStatus) {
		this.itemStatus = itemStatus;
	}

	public Boolean getRemainingDayFlag() {
		return remainingDayFlag;
	}

	public void setRemainingDayFlag(Boolean remainingDayFlag) {
		this.remainingDayFlag = remainingDayFlag;
	}

	public String getSpecialInstruction() {
		return specialInstruction;
	}

	public void setSpecialInstruction(String specialInstruction) {
		this.specialInstruction = specialInstruction;
	}

	public String getSpecialNote() {
		return specialNote;
	}

	public void setSpecialNote(String specialNote) {
		this.specialNote = specialNote;
	}

	public String getDrugOnHandOrderInfo() {
		return drugOnHandOrderInfo;
	}

	public void setDrugOnHandOrderInfo(String drugOnHandOrderInfo) {
		this.drugOnHandOrderInfo = drugOnHandOrderInfo;
	}

	public String getDrugOnHandDetail() {
		return drugOnHandDetail;
	}

	public void setDrugOnHandDetail(String drugOnHandDetail) {
		this.drugOnHandDetail = drugOnHandDetail;
	}

	public String getDiscontinueText() {
		return discontinueText;
	}

	public void setDiscontinueText(String discontinueText) {
		this.discontinueText = discontinueText;
	}

	public MedOrderItem getMedOrderItem() {
		return medOrderItem;
	}

	public void setMedOrderItem(MedOrderItem medOrderItem) {
		this.medOrderItem = medOrderItem;
	}

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}	
}

