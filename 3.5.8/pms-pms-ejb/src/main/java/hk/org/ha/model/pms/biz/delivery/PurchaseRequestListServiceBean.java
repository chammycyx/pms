package hk.org.ha.model.pms.biz.delivery;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.PurchaseRequest;
import hk.org.ha.model.pms.udt.medprofile.RetrievePurchaseRequestOption;

import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("purchaseRequestListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PurchaseRequestListServiceBean implements PurchaseRequestListServiceLocal {

	@In
	private DueOrderPrintManagerLocal dueOrderPrintManager;
	
	@In
	private Workstore workstore;
	
    public PurchaseRequestListServiceBean() {
    }
    
    @Override
    public List<PurchaseRequest> retrievePurchaseRequestList(RetrievePurchaseRequestOption option) {
    	return dueOrderPrintManager.retrievePurchaseRequestList(workstore, new Date(), option);
    }
    
    @Remove
    @Override
	public void destroy() {
	}    
}
