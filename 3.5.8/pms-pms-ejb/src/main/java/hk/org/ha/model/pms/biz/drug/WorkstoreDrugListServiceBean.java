package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.vo.rx.DhRxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("workstoreDrugListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class WorkstoreDrugListServiceBean implements WorkstoreDrugListServiceLocal {

	@In
	private WorkstoreDrugListManagerLocal workstoreDrugListManager;
	
	@Override
	public void retrieveWorkstoreDrugList(String prefixItemCode,
			boolean showNonSuspendItemOnly, boolean fdn) {
		workstoreDrugListManager.retrieveWorkstoreDrugList(prefixItemCode, showNonSuspendItemOnly, fdn);
	}

	@Override
	public void retrieveWorkstoreDrugListByDisplayName(String displayName) {
		workstoreDrugListManager.retrieveWorkstoreDrugListByDisplayName(displayName);
	}

	@Override
	public void retrieveWorkstoreDrugListByDrugKey(Integer drugKey, boolean ignoreDrugScope) {
		workstoreDrugListManager.retrieveWorkstoreDrugListByDrugKey(drugKey, ignoreDrugScope);
	}

	@Override
	public void retrieveWorkstoreDrugListByRxItem(RxItem rxItem,
			Boolean displayNameOnly) {
		workstoreDrugListManager.retrieveWorkstoreDrugListByRxItem(rxItem, displayNameOnly);
	}

	@Override
	public void retrieveWorkstoreDrugListByDhRxDrug(DhRxDrug dhRxDrug, String itemCodePrefix) {
		workstoreDrugListManager.retrieveWorkstoreDrugListByDhRxDrug(dhRxDrug, itemCodePrefix);
		
	}

	@Override
	@Remove
	public void destroy() {
	}
	
}
