package hk.org.ha.model.pms.biz.pivas;

import static hk.org.ha.model.pms.prop.Prop.PIVAS_SATELLITE_ENABLED;
import hk.org.ha.fmk.pms.cache.CacheResult;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmDailyFrequencyCacherInf;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.reftable.pivas.PivasHoliday;
import hk.org.ha.model.pms.persistence.reftable.pivas.PivasServiceHour;
import hk.org.ha.model.pms.udt.reftable.pivas.PivasServiceHourType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang3.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("pivasServiceHourManager")
@MeasureCalls
@SuppressWarnings("unchecked")
public class PivasServiceHourManagerBean implements PivasServiceHourManagerLocal {
		
	public static int MAX_SEARCH_DAY = 365;
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;

	@In
	private DmDailyFrequencyCacherInf dmDailyFrequencyCacher;
	
	@CacheResult(timeout = "60000")
	public Map<String, List<PivasServiceHour>> retrievePivasServiceHourListByDayOfWeekMap(String workstoreGroupCode, Hospital hospital) {
		List<PivasServiceHour> rawPivasServiceHourList = em.createQuery(
				"select o from PivasServiceHour o" + // 20160615 index check : PivasServiceHour.workstoreGroup : UI_PIVAS_SERVICE_HOUR
				" where o.workstoreGroup.workstoreGroupCode = :workstoreGroupCode")
				.setParameter("workstoreGroupCode", workstoreGroupCode)
				.getResultList();

		List<PivasServiceHour> pivasServiceHourList = new ArrayList<PivasServiceHour>();
		for ( PivasServiceHour rawPivasServiceHour : rawPivasServiceHourList ) {
			if ( ! PIVAS_SATELLITE_ENABLED.get(hospital) && rawPivasServiceHour.getType() == PivasServiceHourType.Satellite) {
				// filter out satellite type supply date if satellite rule value is off
				continue;
			}
			
			pivasServiceHourList.add(rawPivasServiceHour);
		}
		
		for ( PivasServiceHour pivasServiceHour : pivasServiceHourList ) {
			if ( pivasServiceHour.getSupplyTime() != null ) {
				pivasServiceHour.setSupplyTimeStr(StringUtils.leftPad(pivasServiceHour.getSupplyTime().toString(), 4, "0"));
			}
		}
		
		Map<String, List<PivasServiceHour>> pivasServiceHourListByDayOfWeekMap = new HashMap<String, List<PivasServiceHour>>();
		
		for (PivasServiceHour pivasServiceHour : pivasServiceHourList) {
			List<PivasServiceHour> tempPivasServiceHourList;
			if (pivasServiceHourListByDayOfWeekMap.containsKey(pivasServiceHour.getDayOfWeek().getDataValue())) {
				tempPivasServiceHourList = pivasServiceHourListByDayOfWeekMap.get(pivasServiceHour.getDayOfWeek().getDataValue());
			} else {
				tempPivasServiceHourList = new ArrayList<PivasServiceHour>();
				pivasServiceHourListByDayOfWeekMap.put(pivasServiceHour.getDayOfWeek().getDataValue(), tempPivasServiceHourList);
			}
			tempPivasServiceHourList.add(pivasServiceHour);
		}
		
		for (String dayOfWeek : pivasServiceHourListByDayOfWeekMap.keySet()) {
			List<PivasServiceHour> tempPivasServiceHourList = pivasServiceHourListByDayOfWeekMap.get(dayOfWeek);
			Collections.sort(tempPivasServiceHourList, new PivasServiceHourComparator());
		}
		
		return pivasServiceHourListByDayOfWeekMap;
	}
	
	@CacheResult(timeout = "60000")
	public Set<Date> retrievePivasHolidayDateSet(String workstoreGroupCode) {
		List<PivasHoliday> pivasHolidayList = em.createQuery(
				"select o from PivasHoliday o" + // 20160615 index check : PivasHoliday.workstoreGroup : UI_PIVAS_HOLIDAY
				" where o.workstoreGroup.workstoreGroupCode = :workstoreGroupCode")
				.setParameter("workstoreGroupCode", workstoreGroupCode)
				.getResultList();

		Set<Date> pivasHolidayDateSet = new HashSet<Date>();
		for (PivasHoliday pivasHoliday : pivasHolidayList) {
			pivasHolidayDateSet.add(pivasHoliday.getHoliday());
		}
		
		return pivasHolidayDateSet;
	}
	
	private static class PivasServiceHourComparator implements Comparator<PivasServiceHour>, Serializable {
		private static final long serialVersionUID = 1L;

		@Override
		public int compare(PivasServiceHour o1, PivasServiceHour o2) {
			return new CompareToBuilder()
						.append( o1.getDayOfWeek().getDataValue(), o2.getDayOfWeek().getDataValue() )
						.append( o1.getSupplyTime(), o2.getSupplyTime() )
						.toComparison();
		}
    }
}
