package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class SfiRefillCoupon implements Serializable {

	private static final long serialVersionUID = 1L;

	private SfiRefillCouponHdr sfiRefillCouponHdr;
	
	private List<SfiRefillCouponDtl> sfiRefillCouponDtlList;

	private SfiRefillCouponFtr sfiRefillCouponFtr;

	public SfiRefillCouponHdr getSfiRefillCouponHdr() {
		return sfiRefillCouponHdr;
	}

	public void setSfiRefillCouponHdr(SfiRefillCouponHdr sfiRefillCouponHdr) {
		this.sfiRefillCouponHdr = sfiRefillCouponHdr;
	}

	public List<SfiRefillCouponDtl> getSfiRefillCouponDtlList() {
		return sfiRefillCouponDtlList;
	}

	public void setSfiRefillCouponDtlList(List<SfiRefillCouponDtl> sfiRefillCouponDtlList) {
		this.sfiRefillCouponDtlList = sfiRefillCouponDtlList;
	}

	public SfiRefillCouponFtr getSfiRefillCouponFtr() {
		return sfiRefillCouponFtr;
	}

	public void setSfiRefillCouponFtr(SfiRefillCouponFtr sfiRefillCouponFtr) {
		this.sfiRefillCouponFtr = sfiRefillCouponFtr;
	}
}