package hk.org.ha.model.pms.biz.vetting.mp;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MpRenalEcrclLog;
import hk.org.ha.model.pms.persistence.medprofile.MpRenalMessage;
import hk.org.ha.model.pms.persistence.medprofile.PurchaseRequest;
import hk.org.ha.model.pms.persistence.medprofile.Replenishment;

import java.util.List;

import javax.ejb.Local;

@Local
public interface MedProfileVettingServiceLocal {
	void vetMedProfile(MedProfile medProfile);
	Boolean checkProfileHasLabelGenerationWithinToday();
	void releaseMedProfile();
	String updateMedProfile(MedProfile medProfile,
			List<MedProfileItem> medProfileItemList,
			List<Replenishment> replenishmentList,
			List<PurchaseRequest> purchaseRequestList,
			List<MpRenalEcrclLog> mpRenalEcrclLogList,
			List<MpRenalMessage> mpRenalMessageList);
	void destroy();
}
