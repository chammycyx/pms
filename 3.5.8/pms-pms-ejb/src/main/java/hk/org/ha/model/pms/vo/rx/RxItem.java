package hk.org.ha.model.pms.vo.rx;

import hk.org.ha.model.pms.dms.biz.formatengine.FormatHints;
import hk.org.ha.model.pms.dms.biz.formatengine.ParsedOrderLineFormatterBase;
import hk.org.ha.model.pms.dms.biz.formatengine.ParsedOrderLineFormatterFactory;
import hk.org.ha.model.pms.dms.udt.formatengine.FormatType;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.RemarkItemStatus;
import hk.org.ha.model.pms.udt.disp.RemarkStatus;
import hk.org.ha.model.pms.udt.vetting.FixedConcnSource;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class RxItem implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@XmlElement
	private Integer itemNum;

	@XmlElement(name="Condition")
	private List<Condition> conditionList;

	@XmlElement
	private String drugOrderHtml;
	
	@XmlElement
	private String drugOrderTlf;
	
	@XmlElement
	private String drugOrderXml;
	
	@XmlTransient
	private String formattedDrugOrderTlf;
	
	@XmlElement
	private Boolean discontinueFlag;
	
	@XmlElement
	private String discontinueReason;
	
	@XmlElement
	private String discontinueHospCode;
	
	@XmlElement
	private Date discontinueDate;
	
	@XmlElement
	private String mrAnnotation;
	
	@XmlElement
	private Boolean fixPeriodFlag;

	@XmlElement
	private Boolean singleUseFlag;

	@XmlElement
	private Boolean externalUseFlag;
	
	@XmlElement
	private String specialInstruction;
	
	@XmlElement
	private Boolean fmFlag;	
	
	@XmlElement
	private Boolean alertFlag;
	
	@XmlElement
	private Boolean hlaFlag;
	
	@XmlElement
	private Date orderDate;
	
	@XmlElement
	private RemarkStatus remarkStatus;

	@XmlElement
	private RemarkItemStatus remarkItemStatus;
	
	@XmlElement
	private String commentText;
	
	@XmlElement
	private String commentUser;

	@XmlElement
	private Date commentDate;
	
	@XmlElement
	private String patHospCode;

	@XmlElement
	private String refNum;

	@XmlElement
	private MedOrderDocType docType;

	@XmlElement
	private String caseNum;

	@XmlElement
	private String pasSpecCode;

	@XmlElement
	private String pasSubSpecCode;

	@XmlElement
	private Boolean manualItemFlag;

	@XmlElement
	private Boolean pivasFormulaFlag;
	
	@XmlElement
	private String interfaceVersion;
	
	@XmlElement
	private Boolean moDoseAlertFlag;
	
	@XmlElement
	private CycleInfo cycleInfo;

	public RxItem() {
    	singleUseFlag = Boolean.FALSE;
    	fixPeriodFlag = Boolean.FALSE;
		externalUseFlag = Boolean.FALSE;
    	alertFlag = Boolean.FALSE;
    	hlaFlag = Boolean.FALSE;
    	pivasFormulaFlag = Boolean.FALSE;
    	moDoseAlertFlag = Boolean.FALSE;
	}
	
	public Integer getItemNum() {
		return itemNum;
	}
	
	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}
	
	public List<Condition> getConditionList() {
		return conditionList;
	}

	public void setConditionList(List<Condition> conditionList) {
		this.conditionList = conditionList;
	}

	public String getDrugOrderHtml() {
		return drugOrderHtml;
	}	

	public void setDrugOrderHtml(String drugOrderHtml) {
		this.drugOrderHtml = drugOrderHtml;
	}
	
	public String getDrugOrderTlf() {
		return drugOrderTlf;
	}

	public void setDrugOrderTlf(String drugOrderTlf) {
		this.drugOrderTlf = drugOrderTlf;
	}

	public String getDrugOrderXml() {
		return drugOrderXml;
	}

	public void setDrugOrderXml(String drugOrderXml) {
		this.drugOrderXml = drugOrderXml;
	}

	public String getFormattedDrugOrderTlf() {
		return formattedDrugOrderTlf;
	}

	public void setFormattedDrugOrderTlf(String formattedDrugOrderTlf) {
		this.formattedDrugOrderTlf = formattedDrugOrderTlf;
	}

	public Boolean getDiscontinueFlag() {
		return discontinueFlag;
	}

	public void setDiscontinueFlag(Boolean discontinueFlag) {
		this.discontinueFlag = discontinueFlag;
	}

	public String getDiscontinueReason() {
		return discontinueReason;
	}

	public void setDiscontinueReason(String discontinueReason) {
		this.discontinueReason = discontinueReason;
	}
	
	public String getDiscontinueHospCode() {
		return discontinueHospCode;
	}

	public void setDiscontinueHospCode(String discontinueHospCode) {
		this.discontinueHospCode = discontinueHospCode;
	}

	public Date getDiscontinueDate() {
		return (discontinueDate == null)? null : new Date(discontinueDate.getTime());
	}

	public void setDiscontinueDate(Date discontinueDate) {
		this.discontinueDate = (discontinueDate == null) ? null : new Date(discontinueDate.getTime());
	}
	
	public String getMrAnnotation() {
		return mrAnnotation;
	}

	public void setMrAnnotation(String mrAnnotation) {
		this.mrAnnotation = mrAnnotation;
	}

	public Boolean getFixPeriodFlag() {
		return fixPeriodFlag;
	}

	public void setFixPeriodFlag(Boolean fixPeriodFlag) {
		this.fixPeriodFlag = fixPeriodFlag;
	}

	public Boolean getExternalUseFlag() {
		return externalUseFlag;
	}

	public Boolean getSingleUseFlag() {
		return singleUseFlag;
	}

	public void setSingleUseFlag(Boolean singleUseFlag) {
		this.singleUseFlag = singleUseFlag;
	}

	public void setExternalUseFlag(Boolean externalUseFlag) {
		this.externalUseFlag = externalUseFlag;
	}

	public String getSpecialInstruction() {
		return specialInstruction;
	}

	public void setSpecialInstruction(String specialInstruction) {
		this.specialInstruction = specialInstruction;
	}

	public Boolean getFmFlag() {
		return fmFlag;
	}

	public void setFmFlag(Boolean fmFlag) {
		this.fmFlag = fmFlag;
	}

	public Boolean getAlertFlag() {
		return alertFlag;
	}

	public void setAlertFlag(Boolean alertFlag) {
		this.alertFlag = alertFlag;
	}

	public Boolean getHlaFlag() {
		return hlaFlag;
	}
	
	public void setHlaFlag(Boolean hlaFlag) {
		this.hlaFlag = hlaFlag;
	}
	
	public void clearPharmDrugList() {
		if (this.isRxDrug()) {
			((RxDrug) this).getPharmDrugList().clear();
		} else if (this.isIvRxDrug()){
			for (IvAdditive item : ((IvRxDrug) this).getIvAdditiveList()) {
				item.getPharmDrugList().clear();
			}
			for (IvFluid item : ((IvRxDrug) this).getIvFluidList()) {
				item.getPharmDrugList().clear();
			}
		} else if (this.isCapdRxDrug()) {
			for (CapdItem item : ((CapdRxDrug) this).getCapdItemList()) {
				item.getPharmDrugList().clear();
			}
		}
	}
		
	public boolean isRxDrug() {
		return RxDrug.class.isAssignableFrom(this.getClass());
	}
	
	public RxDrug asRxDrug() {
		return (RxDrug) this;
	}
	
	public boolean isOralRxDrug() {
		return this instanceof OralRxDrug;
	}

	public boolean isInjectionRxDrug() {
		return this instanceof InjectionRxDrug;
	}

	public boolean isIvRxDrug() {
		return this instanceof IvRxDrug;
	}
	
	public boolean isCapdRxDrug() {
		return this instanceof CapdRxDrug;
	}
	
	public CapdRxDrug asCapdRxDrug() {
		return (CapdRxDrug) this;
	}
	
	public boolean isDhRxDrug() {
		return this instanceof DhRxDrug;
	}

	public DhRxDrug asDhRxDrug() {
		return (DhRxDrug) this;
	}
	
	public String getFullDrugDesc() {
		return null;
	}
	
	public String getDisplayNameSaltForm() {
		return null;
	}
	
	public String getMdsDisplayName() {
		return null;
	}
	
	public String getMdsOrderDesc() {
		return null;
	}
	
	public void buildTlf(){
		if( StringUtils.isNotEmpty( drugOrderXml ) ){
			ParsedOrderLineFormatterBase formatter = ParsedOrderLineFormatterFactory.getParsedOrderLineFormatter(interfaceVersion);
			formatter.setHint(FormatHints.FORMAT_TYPE, FormatType.TLF);
			formatter.setHint(FormatHints.LINE_WIDTH, 45);
			formatter.setHint(FormatHints.HIDE_STRIKE_THROUGH, false);
			String result = formatter.format(drugOrderXml);
			formattedDrugOrderTlf = result;
		}else if( StringUtils.isNotEmpty( drugOrderTlf ) ){
			formattedDrugOrderTlf = drugOrderTlf;
		}
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public RemarkStatus getRemarkStatus() {
		return remarkStatus;
	}

	public void setRemarkStatus(RemarkStatus remarkStatus) {
		this.remarkStatus = remarkStatus;
	}

	public RemarkItemStatus getRemarkItemStatus() {
		return remarkItemStatus;
	}

	public void setRemarkItemStatus(RemarkItemStatus remarkItemStatus) {
		this.remarkItemStatus = remarkItemStatus;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public String getCommentUser() {
		return commentUser;
	}

	public void setCommentUser(String commentUser) {
		this.commentUser = commentUser;
	}

	public Date getCommentDate() {
		return commentDate;
	}

	public void setCommentDate(Date commentDate) {
		this.commentDate = commentDate;
	}	
	
	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public String getRefNum() {
		return refNum;
	}

	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}

	public MedOrderDocType getDocType() {
		return docType;
	}

	public void setDocType(MedOrderDocType docType) {
		this.docType = docType;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getPasSpecCode() {
		return pasSpecCode;
	}

	public void setPasSpecCode(String pasSpecCode) {
		this.pasSpecCode = pasSpecCode;
	}

	public String getPasSubSpecCode() {
		return pasSubSpecCode;
	}

	public void setPasSubSpecCode(String pasSubSpecCode) {
		this.pasSubSpecCode = pasSubSpecCode;
	}

	public Boolean getPivasFormulaFlag() {
		return pivasFormulaFlag;
	}

	public void setPivasFormulaFlag(Boolean pivasFormulaFlag) {
		this.pivasFormulaFlag = pivasFormulaFlag;
	}
	
	public boolean getCanPrintLabelInstruction() {
		
		if (getConditionList() != null && getConditionList().size() > 0) {
			return false;
		}
		
		if (this instanceof IvRxDrug) {
			
			IvRxDrug ivRxDrug = (IvRxDrug) this;
			
			if (ivRxDrug.getLineDesc() != null || isAdminRateValid(ivRxDrug.getStartAdminRate()) || 
				(ivRxDrug.getIvFluidList() != null && ivRxDrug.getIvFluidList().size() > 0))
				return false;
			
			for (IvAdditive ivAdditive: ivRxDrug.getIvAdditiveList()) {
				if (ivAdditive.getSpecialInstruction() != null)
					return false;
			}
			
		} else if (this instanceof RxDrug) {
			
			RxDrug rxDrug = (RxDrug) this;
			
			if (rxDrug.getSpecialInstruction() != null)
				return false;
			
			if (rxDrug.getRegimen() != null) {
				for (DoseGroup doseGroup: rxDrug.getRegimen().getDoseGroupList()) {
					for (Dose dose: doseGroup.getDoseList()) {
						if (dose.getDoseFluid() != null || isAdminRateValid(dose.getStartAdminRate()))
							return false;
					}
				}
			}
		}
		
		return true;
		
	}
	
	private boolean isAdminRateValid(AdminRate adminRate) {
		
		if (adminRate == null)
			return false;
		
		if ("D".equals(adminRate.getRateFormat()) && adminRate.getRateDuration() != null && adminRate.getRateDuration() > 0 ) {
			return true;
		} else if ("V".equals(adminRate.getRateFormat()) && adminRate.getRateVolume() != null && adminRate.getRateVolume().compareTo(BigDecimal.ZERO) != 0 ) {
			return true;
		}
		
		return false;
	}
	
	public FixedConcnSource getFixedConcnSource() {
		if (this instanceof InjectionRxDrug) {
			InjectionRxDrug injectionRxDrug = (InjectionRxDrug) this;
			return injectionRxDrug.getFixedConcnSource();
			
		} else if (this instanceof IvRxDrug) {
			IvRxDrug ivRxDrug = (IvRxDrug) this;
			
			for (IvAdditive ivAdditive: ivRxDrug.getIvAdditiveList()) {
				// only 1 additive for fixed concentration item
				return ivAdditive.getFixedConcnSource();
			}
		}
		
		return FixedConcnSource.None;
	}

	public Boolean getManualItemFlag() {
		return manualItemFlag;
	}

	public void setManualItemFlag(Boolean manualItemFlag) {
		this.manualItemFlag = manualItemFlag;
	}

	public String getInterfaceVersion() {
		return interfaceVersion;
	}

	public void setInterfaceVersion(String interfaceVersion) {
		this.interfaceVersion = interfaceVersion;
	}

	public Boolean getMoDoseAlertFlag() {
		return moDoseAlertFlag;
	}

	public void setMoDoseAlertFlag(Boolean moDoseAlertFlag) {
		this.moDoseAlertFlag = moDoseAlertFlag;
	}

	public CycleInfo getCycleInfo() {
		return cycleInfo;
	}

	public void setCycleInfo(CycleInfo cycleInfo) {
		this.cycleInfo = cycleInfo;
	}
}
