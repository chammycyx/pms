package hk.org.ha.model.pms.udt.report;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PivasRptDispMethod implements StringValuedEnum {

	All("A", "Patient / Batch Dispensing"),
	PatientDisp("B", "Patient Dispensing"),
	BatchDisp("C", "Batch Dispensing");
	
    private final String dataValue;
    private final String displayValue;

    PivasRptDispMethod(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static PivasRptDispMethod dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PivasRptDispMethod.class, dataValue);
	}
        
	public static class Converter extends StringValuedEnumConverter<PivasRptDispMethod> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<PivasRptDispMethod> getEnumClass() {
    		return PivasRptDispMethod.class;
    	}
    }
}
