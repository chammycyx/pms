package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DispOrderBatchProcessingType implements StringValuedEnum {

	None("-", "None"),
	DayEnd("D", "DayEnd"),
	Uncollect("C", "Uncollected"),
	Suspend("S", "Suspend"),
	Unsuspend("U", "Unsuspend");
	
    private final String dataValue;
    private final String displayValue;

    DispOrderBatchProcessingType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }  
    
    public static DispOrderBatchProcessingType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DispOrderBatchProcessingType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<DispOrderBatchProcessingType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<DispOrderBatchProcessingType> getEnumClass() {
    		return DispOrderBatchProcessingType.class;
    	}
    }
}
