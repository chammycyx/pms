package hk.org.ha.model.pms.vo.rx;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type = DefaultExternalizer.class)
public class Condition implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement
	private Integer conditionNum;
	
	@XmlElement
	private String conditionStatus;
		
	@XmlElement
	private String otherCondition;
	
	@XmlElement
	private String conditionType;
	
	@XmlElement
	private Integer conditionGroupId;

	@XmlElement
	private Integer conditionId;
	
	@XmlElement
	private BigDecimal dosage;
	
	@XmlElement
	private String dosageUnit;
	
	@XmlElement
	private String conditionDesc;
	
	@XmlElement(required = true)
	private String conditionText;

	@XmlElement(name="Param")
	private List<Param> paramList;
	
	public Integer getConditionNum() {
		return conditionNum;
	}

	public void setConditionNum(Integer conditionNum) {
		this.conditionNum = conditionNum;
	}

	public String getConditionStatus() {
		return conditionStatus;
	}

	public void setConditionStatus(String conditionStatus) {
		this.conditionStatus = conditionStatus;
	}

	public String getOtherCondition() {
		return otherCondition;
	}

	public void setOtherCondition(String otherCondition) {
		this.otherCondition = otherCondition;
	}

	public String getConditionType() {
		return conditionType;
	}

	public void setConditionType(String conditionType) {
		this.conditionType = conditionType;
	}

	public Integer getConditionGroupId() {
		return conditionGroupId;
	}

	public void setConditionGroupId(Integer conditionGroupId) {
		this.conditionGroupId = conditionGroupId;
	}

	public Integer getConditionId() {
		return conditionId;
	}

	public void setConditionId(Integer conditionId) {
		this.conditionId = conditionId;
	}

	public BigDecimal getDosage() {
		return dosage;
	}

	public void setDosage(BigDecimal dosage) {
		this.dosage = dosage;
	}

	public String getDosageUnit() {
		return dosageUnit;
	}

	public void setDosageUnit(String dosageUnit) {
		this.dosageUnit = dosageUnit;
	}

	public String getConditionDesc() {
		return conditionDesc;
	}

	public void setConditionDesc(String conditionDesc) {
		this.conditionDesc = conditionDesc;
	}

	public String getConditionText() {
		return conditionText;
	}

	public void setConditionText(String conditionText) {
		this.conditionText = conditionText;
	}

	public List<Param> getParamList() {
		return paramList;
	}

	public void setParamList(List<Param> paramList) {
		this.paramList = paramList;
	}
}
