package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.reftable.WardFilterManagerLocal;
import hk.org.ha.model.pms.biz.reftable.WardManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Institution;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.phs.Ward;
import hk.org.ha.model.pms.persistence.reftable.WardFilter;
import hk.org.ha.model.pms.vo.reftable.WardCodeMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("wardFilterService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class WardFilterServiceBean implements WardFilterServiceLocal {
	
	@Out
	private List<WardFilter> wardFilterList;
	
	@Out
	private WardCodeMap wardCodeMap;
	
	@Out
	private List<Workstore> wardFilterWorkstoreList;
	
	@In
	private Workstore workstore;
	
	@In
	private WardFilterManagerLocal wardFilterManager;
	
	@In
	private WardManagerLocal wardManager;
	
	public void retrieveWardFilterList() {
		wardFilterList = wardFilterManager.retrieveWardFilterList();
		
		wardFilterWorkstoreList = workstore.getHospital().getWorkstoreList();
		
		HashSet<Institution> institutionSet = new HashSet<Institution>();
		for( Workstore tmpWorkstore : wardFilterWorkstoreList ){
			institutionSet.add(tmpWorkstore.getWorkstoreGroup().getInstitution());
		}
		
		List<Institution> institutionList = new ArrayList<Institution>(institutionSet);
		
		Map<String, List<String>> properties = new HashMap<String, List<String>>();
		
		for( Institution institution : institutionList ){
			List<String> wardCodeList = new ArrayList<String>();
			for( Ward ward : wardManager.retrieveWardListByInstitution(institution) ){
				wardCodeList.add( ward.getWardCode() );
			}
			properties.put(institution.getInstCode(), wardCodeList);
		}
		
		wardCodeMap = new WardCodeMap();
		wardCodeMap.setWardCodeMap(properties);	
		
	}
	
	public void updateWardFilterList(List<WardFilter> newWardFilterList) {
		wardFilterManager.updateWardFilterList(newWardFilterList);
	}

	@Remove
	public void destroy() {
		if (wardFilterList != null) {
			wardFilterList = null;
		}
		if (wardCodeMap != null) {
			wardCodeMap = null;
		}
	}
}
