package hk.org.ha.model.pms.udt.disp;

import java.io.Serializable;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class RemarkItemStatusAdapter extends XmlAdapter<String, RemarkItemStatus> implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public String marshal(RemarkItemStatus v) {
		return (v == null) ? null : v.getDataValue();
	}

	public RemarkItemStatus unmarshal(String v) {
		return RemarkItemStatus.dataValueOf(v);
	}

}
