package hk.org.ha.model.pms.vo.alert.druginfo;

import java.io.Serializable;

public class PatientEducation implements Serializable {

	private static final long serialVersionUID = 1L;

	private String monograph;
	
	private Boolean allowPrintFlag;
	
	private String html;

	public String getMonograph() {
		return monograph;
	}

	public void setMonograph(String monograph) {
		this.monograph = monograph;
	}

	public Boolean getAllowPrintFlag() {
		return allowPrintFlag;
	}

	public void setAllowPrintFlag(Boolean allowPrintFlag) {
		this.allowPrintFlag = allowPrintFlag;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}
}
