package hk.org.ha.model.pms.biz.reftable.mp;
import java.util.Date;
import java.util.List;

import hk.org.ha.model.pms.persistence.reftable.AomSchedule;
import hk.org.ha.model.pms.udt.reftable.mp.AomScheduleType;

import javax.ejb.Local;

@Local
public interface AomScheduleManagerLocal {
	AomSchedule retrieveAomScheduleByDailyCode(String dailyFreqCode, AomScheduleType type);
	Date getNextAdminDueDate(List<String> dailyFreqCodeList, AomScheduleType type);
}