package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum OnHandSourceType implements StringValuedEnum {

	Local("L", "Local Hospital"),
	Corporate("C", "Corporate"); 
	
    private final String dataValue;
    private final String displayValue;

    OnHandSourceType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static OnHandSourceType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(OnHandSourceType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<OnHandSourceType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<OnHandSourceType> getEnumClass() {
    		return OnHandSourceType.class;
    	}
    }
}
