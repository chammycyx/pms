package hk.org.ha.model.pms.persistence.medprofile;

import hk.org.ha.fmk.pms.entity.CreateDate;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.model.pms.udt.medprofile.ErrorLogType;
import hk.org.ha.model.pms.vo.medprofile.ErrorInfo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "MED_PROFILE_ERROR_LOG")
public class MedProfileErrorLog {
	
	private static final String JAXB_CONTEXT_MEDPROFILE = "hk.org.ha.model.pms.vo.medprofile";	
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MedProfileErrorLogSeq")
	@SequenceGenerator(name = "MedProfileErrorLogSeq", sequenceName = "SQ_MED_PROFILE_ERROR_LOG", initialValue = 100000000)
	private Long id;
		
    @Converter(name = "MedProfileErrorLog.type", converterClass = ErrorLogType.Converter.class)
    @Convert("MedProfileErrorLog.type")
	@Column(name = "TYPE", nullable = false, length = 1)
	private ErrorLogType type;
    
	@Lob
	@Column(name = "ERROR_XML")
	private String errorXml;
	
	@Column(name = "MESSAGE", length = 100)
	private String message;
	
	@Column(name = "HIDE_ERROR_STAT_FLAG", nullable = false)
	private Boolean hideErrorStatFlag;
	
	@Column(name = "HIDE_FOLLOW_UP_FLAG", nullable = false)
	private Boolean hideFollowUpFlag;
	
	@CreateDate
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_DATE", nullable = false)
	private Date createDate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MED_PROFILE_ITEM_ID", nullable = false)
	private MedProfileItem medProfileItem;

	@Column(name = "DELIVERY_REQUEST_ID")
	private Long deliveryRequestId;
	
	@Transient
	private ErrorInfo errorInfo;

	public MedProfileErrorLog() {
		hideErrorStatFlag = Boolean.FALSE;
		hideFollowUpFlag = Boolean.FALSE;
	}
	
	@PostLoad
	public void postLoad() {
    	if (errorXml != null) {
    		JaxbWrapper<ErrorInfo> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_MEDPROFILE);
    		errorInfo = rxJaxbWrapper.unmarshall(errorXml);
    	}
	}
	
	@PrePersist
    @PreUpdate
    public void preSave() {
		if (errorInfo != null) {
			JaxbWrapper<ErrorInfo> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_MEDPROFILE);
			errorXml = rxJaxbWrapper.marshall(errorInfo);
		}
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ErrorLogType getType() {
		return type;
	}

	public void setType(ErrorLogType type) {
		this.type = type;
	}
	
	public String getErrorXml() {
		return errorXml;
	}

	public void setErrorXml(String errorXml) {
		this.errorXml = errorXml;
	}
	
	public ErrorInfo getErrorInfo() {
		return errorInfo;
	}

	public void setErrorInfo(ErrorInfo errorInfo) {
		this.errorInfo = errorInfo;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public Boolean getHideErrorStatFlag() {
		return hideErrorStatFlag;
	}

	public void setHideErrorStatFlag(Boolean hideErrorStatFlag) {
		this.hideErrorStatFlag = hideErrorStatFlag;
	}
	
	public Boolean getHideFollowUpFlag() {
		return hideFollowUpFlag;
	}

	public void setHideFollowUpFlag(Boolean hideFollowUpFlag) {
		this.hideFollowUpFlag = hideFollowUpFlag;
	}

	public Date getCreateDate() {
		return createDate == null ? null : new Date(createDate.getTime());
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate == null ? null : new Date(createDate.getTime());
	}

	public MedProfileItem getMedProfileItem() {
		return medProfileItem;
	}

	public void setMedProfileItem(MedProfileItem medProfileItem) {
		this.medProfileItem = medProfileItem;
	}

	public Long getDeliveryRequestId() {
		return deliveryRequestId;
	}

	public void setDeliveryRequestId(Long deliveryRequestId) {
		this.deliveryRequestId = deliveryRequestId;
	}
}
