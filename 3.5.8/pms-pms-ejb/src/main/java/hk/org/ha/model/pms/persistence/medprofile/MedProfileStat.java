package hk.org.ha.model.pms.persistence.medprofile;

import static hk.org.ha.model.pms.prop.Prop.MEDPROFILE_ITEM_ADMINTIME_BUFFER;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.udt.medprofile.ErrorLogType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatAdminType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatChargeType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatOrderType;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.vo.rx.CapdRxDrug;
import hk.org.ha.model.pms.vo.rx.IvAdditive;
import hk.org.ha.model.pms.vo.rx.IvFluid;
import hk.org.ha.model.pms.vo.rx.IvRxDrug;
import hk.org.ha.model.pms.vo.rx.Regimen;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "MED_PROFILE_STAT")
public class MedProfileStat {
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "medProfileStatSeq")
	@SequenceGenerator(name = "medProfileStatSeq", sequenceName = "SQ_MED_PROFILE_STAT", initialValue = 100000000)
	private Long id;
	
    @Converter(name = "MedProfileStat.orderType", converterClass = MedProfileStatOrderType.Converter.class)
    @Convert("MedProfileStat.orderType")
	@Column(name = "ORDER_TYPE", nullable = false, length = 1)
	private MedProfileStatOrderType orderType;

    @Converter(name = "MedProfileStat.adminType", converterClass = MedProfileStatAdminType.Converter.class)
    @Convert("MedProfileStat.adminType")
	@Column(name = "ADMIN_TYPE", nullable = false, length = 1)
	private MedProfileStatAdminType adminType;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DUE_DATE")
	private Date dueDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FIRST_ITEM_DATE")
	private Date firstItemDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_ITEM_DATE")
	private Date lastItemDate;
	
	@Column(name = "STAT_FLAG", nullable = false)
	private Boolean statFlag;
	
	@Column(name = "URGENT_FLAG", nullable = false)
	private Boolean urgentFlag;

    @Converter(name = "MedProfileStat.chargeType", converterClass = MedProfileStatChargeType.Converter.class)
    @Convert("MedProfileStat.chargeType")
	@Column(name = "CHARGE_TYPE", nullable = false, length = 1)
	private MedProfileStatChargeType chargeType;
	
	@Column(name = "REPLENISH_FLAG", nullable = false)
	private Boolean replenishFlag;
	
	@Column(name = "AMEND_FLAG", nullable = false)
	private Boolean amendFlag;
	
	@Column(name = "CHEMO_FLAG", nullable = false)
	private Boolean chemoFlag;
		
	@Column(name = "SUSPEND_FLAG", nullable = false)
	private Boolean suspendFlag;
	
	@Column(name = "PENDING_FLAG", nullable = false)
	private Boolean pendingFlag;
	
	@Column(name = "MDS_ERROR_FLAG", nullable = false)
	private Boolean mdsErrorFlag;
	
	@Column(name = "UNIT_DOSE_EXCEPT_FLAG")
	private Boolean unitDoseExceptFlag;
	
	@Column(name = "WARD_STOCK_FLAG", nullable = false)
	private Boolean wardStockFlag;
	
	@Column(name = "SELF_OWN_FLAG", nullable = false)
	private Boolean selfOwnFlag;
	
	@Column(name = "REFILL_EXCEPT_FLAG", nullable = false)
	private Boolean refillExceptFlag;
	
	@Column(name = "OVERRIDE_PENDING_FLAG", nullable = false)
	private Boolean overridePendingFlag;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MED_PROFILE_ID", nullable = false)
	private MedProfile medProfile;
	
	@Column(name = "MED_PROFILE_ID", nullable = false, insertable = false, updatable = false)
	private Long medProfileId;		
	
	private transient Set<String> wardAdminFreqCodeSet;
	
	private transient Set<String> aomFreqCodeSet;
	
	private transient Date calculationStartDate;
	
	private transient Boolean selfOwnFlagInited;

	public MedProfileStat() {
		reset();
	}
	
	public MedProfileStat(MedProfileStatAdminType adminType, MedProfileStatOrderType orderType,
			MedProfile medProfile) {
		this();
		this.adminType = adminType;
		this.orderType = orderType;
		this.medProfile = medProfile;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MedProfileStatOrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(MedProfileStatOrderType orderType) {
		this.orderType = orderType;
	}

	public MedProfileStatAdminType getAdminType() {
		return adminType;
	}

	public void setAdminType(MedProfileStatAdminType adminType) {
		this.adminType = adminType;
	}

	public Date getDueDate() {
		return dueDate == null ? null : new Date(dueDate.getTime());
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate == null ? null : new Date(dueDate.getTime());
	}

	public Date getFirstItemDate() {
		return firstItemDate == null ? null : new Date(firstItemDate.getTime());
	}

	public void setFirstItemDate(Date firstItemDate) {
		this.firstItemDate = firstItemDate == null ? null : new Date(firstItemDate.getTime());
	}

	public Date getLastItemDate() {
		return lastItemDate == null ? null : new Date(lastItemDate.getTime());
	}

	public void setLastItemDate(Date lastItemDate) {
		this.lastItemDate = lastItemDate == null ? null : new Date(lastItemDate.getTime());
	}
	
	public Boolean getStatFlag() {
		return statFlag;
	}

	public void setStatFlag(Boolean statFlag) {
		this.statFlag = statFlag;
	}

	public Boolean getUrgentFlag() {
		return urgentFlag;
	}

	public void setUrgentFlag(Boolean urgentFlag) {
		this.urgentFlag = urgentFlag;
	}

	public MedProfileStatChargeType getChargeType() {
		return chargeType;
	}

	public void setChargeType(MedProfileStatChargeType chargeType) {
		this.chargeType = chargeType;
	}

	public Boolean getReplenishFlag() {
		return replenishFlag;
	}

	public void setReplenishFlag(Boolean replenishFlag) {
		this.replenishFlag = replenishFlag;
	}

	public Boolean getAmendFlag() {
		return amendFlag;
	}

	public void setAmendFlag(Boolean amendFlag) {
		this.amendFlag = amendFlag;
	}
	
	public Boolean getChemoFlag() {
		return chemoFlag;
	}

	public void setChemoFlag(Boolean chemoFlag) {
		this.chemoFlag = chemoFlag;
	}

	public Boolean getSuspendFlag() {
		return suspendFlag;
	}

	public void setSuspendFlag(Boolean suspendFlag) {
		this.suspendFlag = suspendFlag;
	}

	public Boolean getPendingFlag() {
		return pendingFlag;
	}

	public void setPendingFlag(Boolean pendingFlag) {
		this.pendingFlag = pendingFlag;
	}
	
	public Boolean getOverridePendingFlag() {
		return overridePendingFlag;
	}

	public void setOverridePendingFlag(Boolean overridePendingFlag) {
		this.overridePendingFlag = overridePendingFlag;
	}

	public Boolean getMdsErrorFlag() {
		return mdsErrorFlag;
	}

	public void setMdsErrorFlag(Boolean mdsErrorFlag) {
		this.mdsErrorFlag = mdsErrorFlag;
	}
	
	public Boolean getUnitDoseExceptFlag() {
		return unitDoseExceptFlag;
	}

	public void setUnitDoseExceptFlag(Boolean unitDoseExceptFlag) {
		this.unitDoseExceptFlag = unitDoseExceptFlag;
	}

	public Boolean getWardStockFlag() {
		return wardStockFlag;
	}

	public void setWardStockFlag(Boolean wardStockFlag) {
		this.wardStockFlag = wardStockFlag;
	}

	public Boolean getSelfOwnFlag() {
		return selfOwnFlag;
	}

	public void setSelfOwnFlag(Boolean selfOwnFlag) {
		this.selfOwnFlag = selfOwnFlag;
	}

	public Boolean getRefillExceptFlag() {
		return refillExceptFlag;
	}

	public void setRefillExceptFlag(Boolean refillExceptFlag) {
		this.refillExceptFlag = refillExceptFlag;
	}

	public MedProfile getMedProfile() {
		return medProfile;
	}

	public void setMedProfile(MedProfile medProfile) {
		this.medProfile = medProfile;
	}
	
	public Long getMedProfileId() {
		return medProfileId;
	}

	public void setMedProfileId(Long medProfileId) {
		this.medProfileId = medProfileId;
	}

	public Set<String> getWardAdminFreqCodeSet() {
		if (wardAdminFreqCodeSet == null) {
			wardAdminFreqCodeSet = new HashSet<String>();
		}
		return wardAdminFreqCodeSet;
	}

	public void setWardAdminFreqCodeSet(Set<String> wardAdminFreqCodeSet) {
		this.wardAdminFreqCodeSet = wardAdminFreqCodeSet;
	}
	
	public Set<String> getAomFreqCodeSet() {
		if (aomFreqCodeSet == null) {
			aomFreqCodeSet = new HashSet<String>();
		}
		return aomFreqCodeSet;
	}
	
	public void setAomFreqCodeSet(Set<String> aomFreqCodeSet) {
		this.aomFreqCodeSet = aomFreqCodeSet;
	}
	
	public Date getCalculationStartDate() {
		return calculationStartDate;
	}

	public void setCalculationStartDate(Date calculationStartDate) {
		this.calculationStartDate = calculationStartDate;
	}

	public void reset() {
		
		calculationStartDate = null;
		selfOwnFlagInited = Boolean.FALSE;
		statFlag = Boolean.FALSE;
		dueDate = null;
		firstItemDate = null;
		lastItemDate = null;
		urgentFlag = Boolean.FALSE;
		chargeType = MedProfileStatChargeType.None;
		replenishFlag = Boolean.FALSE;
		amendFlag = Boolean.FALSE;
		chemoFlag = Boolean.FALSE;
		suspendFlag = Boolean.FALSE;
		pendingFlag = Boolean.FALSE;
		overridePendingFlag = Boolean.FALSE;
		mdsErrorFlag = Boolean.FALSE;
		unitDoseExceptFlag = Boolean.FALSE;
		wardStockFlag = Boolean.FALSE;
		selfOwnFlag = Boolean.FALSE;
		refillExceptFlag = Boolean.FALSE;
	}
	
	public void update(MedProfileItem medProfileItem, boolean replenishFlag) {

		MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
		
		updateNextAdminDate(medProfileMoItem);
		updateFirstItemDate(medProfileMoItem);
		updateLastItemDate(medProfileMoItem);
		updateReplenishFlag(replenishFlag);
		updateFlags(medProfileItem);
	}
	
	public void update(MedProfileItem medProfileItem, List<Replenishment> replenishmentList) {
		
		for (Replenishment replenishment: replenishmentList) {
			updateNextAdminDate(replenishment);
			updateFirstItemDate(replenishment);
			updateLastItemDate(replenishment);
		}
		updateReplenishFlag(replenishmentList.size() > 0);
		updateFlags(medProfileItem);
	}
	
	public void updateFlags(MedProfileItem medProfileItem) {
		
		MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
		
		updateUrgentFlag(medProfileMoItem);
		updateChargeType(medProfileMoItem);
		updateAmendFlag(medProfileItem);
		updatePendingFlag(medProfileMoItem);
		updateSuspendFlag(medProfileMoItem);
		updateOverridePendingFlag(medProfileMoItem);
		updateMdsErrorFlag(medProfileMoItem);
		updateUnitDoseExceptFlag(medProfileMoItem);
		updateChemoFlag(medProfileMoItem);
		updateSelfOwnFlag(medProfileMoItem);
		updateRefillExceptFlag(medProfileItem);
	}
	
	private void updateNextAdminDate(Replenishment replenishment) {
		
		if (replenishment.getRequestDate() == null) {
			return;
		}
		
		boolean isContinueWithOwnStockItem = isContinueWithOwnStockItem(replenishment.getMedProfileMoItem());
		if (Boolean.TRUE.equals(selfOwnFlagInited)) {
			if (Boolean.TRUE.equals(selfOwnFlag) && !isContinueWithOwnStockItem) {
				setDueDate(null);
			} else if (!Boolean.TRUE.equals(selfOwnFlag) && isContinueWithOwnStockItem) {
				return;
			}
		}
		
		Calendar adjustedDueCalendar = Calendar.getInstance();
		adjustedDueCalendar.setTime(replenishment.getRequestDate());
		adjustedDueCalendar.add(Calendar.MINUTE, 
				MEDPROFILE_ITEM_ADMINTIME_BUFFER.get(medProfile.getWorkstore(), 0));
		
		Date adjustedDueDate = adjustedDueCalendar.getTime();
		
		if (dueDate == null || dueDate.compareTo(adjustedDueDate) > 0) {
			setDueDate(adjustedDueDate);
		}
	}
	
	private void updateFirstItemDate(Replenishment replenishment) {
		
		if (firstItemDate == null || (replenishment.getCreateDate() != null &&
				firstItemDate.compareTo(replenishment.getCreateDate()) > 0)) {
			setFirstItemDate(replenishment.getCreateDate());
		}
	}
	
	private void updateLastItemDate(Replenishment replenishment) {
		
		if (lastItemDate == null || (replenishment.getCreateDate() != null &&
				lastItemDate.compareTo(replenishment.getCreateDate()) < 0)) {
			setLastItemDate(replenishment.getCreateDate());
		}
	}
	
	private void updateNextAdminDate(MedProfileMoItem medProfileMoItem) {
		
		boolean isContinueWithOwnStockItem = isContinueWithOwnStockItem(medProfileMoItem);
		if (Boolean.TRUE.equals(selfOwnFlagInited)) {
			if (Boolean.TRUE.equals(selfOwnFlag) && !isContinueWithOwnStockItem) {
				statFlag = Boolean.FALSE;
				setDueDate(null);
			} else if (!Boolean.TRUE.equals(selfOwnFlag) && isContinueWithOwnStockItem) {
				return;
			}
		}
		
		if (medProfileMoItem.isManualItem()) {
			
			for (MedProfilePoItem medProfilePoItem: medProfileMoItem.getMedProfilePoItemList()) {
				String freqCode = lookupFreqCode(medProfilePoItem.getRegimen());
				Date adjustedDueDate = calculateAdjustedDueDate(freqCode, medProfilePoItem.getDueDate(), medProfileMoItem.isManualItem());
				updateNextAdminDate(freqCode, adjustedDueDate);
			}
		} else {
			RxItem rxItem = medProfileMoItem.getRxItem();
			Date adjustedDueDate = medProfileMoItem.getStartDate();
			String freqCode = lookupFreqCode(rxItem);
			adjustedDueDate = calculateAdjustedDueDate(
					(rxItem instanceof IvRxDrug) ? null : freqCode, adjustedDueDate, medProfileMoItem.isManualItem());
			updateNextAdminDate(freqCode, adjustedDueDate);
		}
	}
	
	private void updateNextAdminDate(String freqCode, Date adjustedDueDate) {
		
		boolean currentStatFlag = "00036".equals(freqCode);
		
		if (currentStatFlag && !Boolean.TRUE.equals(statFlag)) {
			setDueDate(adjustedDueDate);
			statFlag = Boolean.TRUE;
		}
		if ((currentStatFlag || !Boolean.TRUE.equals(statFlag)) && 
				(dueDate == null || (adjustedDueDate != null && dueDate.compareTo(adjustedDueDate) > 0))) {
			setDueDate(adjustedDueDate);
		}
	}
	
	private String lookupFreqCode(RxItem rxItem) {

		String freqCode = null;
		
		try {
			if (rxItem instanceof RxDrug) {
				freqCode = lookupFreqCode(((RxDrug) rxItem).getRegimen());
			} else if (rxItem instanceof IvRxDrug){
				freqCode = ((IvRxDrug) rxItem).getDailyFreq().getCode();
			}
		} catch (Exception ex) {
		}
		
		return freqCode;
	}
	
	private String lookupFreqCode(Regimen regimen) {

		try {
			return regimen.getDoseGroupList().get(0).getDoseList().get(0).getDailyFreq().getCode();
		} catch (Exception ex) {
		}
		return null; 
	}
	
	private void updateFirstItemDate(MedProfileMoItem medProfileMoItem) {
		
		if (!medProfileMoItem.isManualItem() && (firstItemDate == null || (medProfileMoItem.getCreateDate() != null &&
				firstItemDate.compareTo(medProfileMoItem.getCreateDate()) > 0))) {
			setFirstItemDate(medProfileMoItem.getCreateDate());
		}
	}
	
	private void updateLastItemDate(MedProfileMoItem medProfileMoItem) {
		
		if (!medProfileMoItem.isManualItem() && (lastItemDate == null || (medProfileMoItem.getCreateDate() != null &&
				lastItemDate.compareTo(medProfileMoItem.getCreateDate()) < 0))) {
			setLastItemDate(medProfileMoItem.getCreateDate());
		}
	}
	
	private void updateUrgentFlag(MedProfileMoItem medProfileMoItem) {
		
		if (Boolean.TRUE.equals(medProfileMoItem.getUrgentFlag()) && (medProfileMoItem.getFirstPrintDate() == null ||
				MedProfileUtils.isSameDay(medProfileMoItem.getFirstPrintDate(), calculationStartDate))) {
			urgentFlag = Boolean.TRUE;
		}
	}
	
	private void updateChargeType(MedProfileMoItem medProfileMoItem) {
		
		if (Boolean.TRUE.equals(medProfileMoItem.getMedProfileItem().getMedProfile().getPrivateFlag())) {
			chargeType = MedProfileStatChargeType.Private;
			return;
		}
		
		RxItem rxItem = medProfileMoItem.getRxItem();
		
		if (medProfileMoItem.isManualItem()) {
			
			for (MedProfilePoItem medProfilePoItem: medProfileMoItem.getMedProfilePoItemList()) {
				if (isSfi(medProfilePoItem.getActionStatus())) {
					chargeType = MedProfileStatChargeType.Sfi;
				}
			}
		} else if (rxItem instanceof RxDrug) {
			
			RxDrug rxDrug = (RxDrug) rxItem;
			if (isSfi(rxDrug.getActionStatus())) {
				chargeType = MedProfileStatChargeType.Sfi;
			}
			
		} else if (rxItem instanceof IvRxDrug) {
			
			IvRxDrug ivRxDrug = (IvRxDrug) rxItem;
			for (IvAdditive ivAdditive: ivRxDrug.getIvAdditiveList()) {
				if (isSfi(ivAdditive.getActionStatus())) {
					chargeType = MedProfileStatChargeType.Sfi;
				}
			}
			for (IvFluid ivFluid: ivRxDrug.getIvFluidList()) {
				if (isSfi(ivFluid.getActionStatus())) {
					chargeType = MedProfileStatChargeType.Sfi;
				}
			}
		}
	}
	
	private boolean isSfi(ActionStatus actionStatus) {
		return actionStatus == ActionStatus.PurchaseByPatient || actionStatus == ActionStatus.SafetyNet;
	}
	
	private void updateReplenishFlag(Boolean replenishFlag) {
		
		if (Boolean.TRUE.equals(replenishFlag)) {
			this.replenishFlag = Boolean.TRUE;
		}
	}
	
	private void updateAmendFlag(MedProfileItem medProfileItem) {

		if (Boolean.TRUE.equals(medProfileItem.getAmendFlag())) {
			amendFlag = Boolean.TRUE;
		}
	}
	
	private void updatePendingFlag(MedProfileMoItem medProfileMoItem) {
		
		if (medProfileMoItem.getPendingCode() != null) {
			pendingFlag = Boolean.TRUE;
		}
	}
	
	private void updateSuspendFlag(MedProfileMoItem medProfileMoItem) {
		
		if (medProfileMoItem.getSuspendCode() != null) {
			suspendFlag = Boolean.TRUE;
		}
	}
	
	private void updateOverridePendingFlag(MedProfileMoItem medProfileMoItem) {
		
		if (medProfileMoItem.getPendingCode() != null && medProfileMoItem.getOverrideDate() != null) {
			overridePendingFlag = Boolean.TRUE;
		}
	}
	
	private void updateMdsErrorFlag(MedProfileMoItem medProfileMoItem) {

		if (medProfileMoItem.getMdsSuspendReason() != null) {
			mdsErrorFlag = Boolean.TRUE;
		}
	}
	
	private void updateUnitDoseExceptFlag(MedProfileMoItem medProfileMoItem){
		
		if (Boolean.TRUE.equals(medProfileMoItem.getUnitDoseExceptFlag())) {
			unitDoseExceptFlag = Boolean.TRUE;
		}
	}
	
	private void updateChemoFlag(MedProfileMoItem medProfileMoItem) {

		RxItem rxItem = medProfileMoItem.getRxItem();
		
		if (medProfileMoItem.isManualItem()) {
			
			for (MedProfilePoItem medProfilePoItem: medProfileMoItem.getMedProfilePoItemList()) {
				updateChemoFlag(medProfilePoItem.getItemCode());
			}
		} else if (rxItem instanceof RxDrug) {
			
			RxDrug rxDrug = (RxDrug) rxItem;
			updateChemoFlag(rxDrug);
			
		} else if (rxItem instanceof IvRxDrug) {
			
			IvRxDrug ivRxDrug = (IvRxDrug) rxItem;
			for (IvAdditive ivAdditive: ivRxDrug.getIvAdditiveList()) {
				updateChemoFlag(ivAdditive);
			}
		}
	}
	
	private void updateChemoFlag(String itemCode) {
		
		updateChemoDrug(DmDrugCacher.instance().getDmDrug(itemCode));
	}
	
	private void updateChemoFlag(RxDrug rxDrug) {
		
		List<DmDrug> dmDrugList = DmDrugCacher.instance().getDmDrugListByDisplayNameFormSaltFmStatus(medProfile.getWorkstore(),
				rxDrug.getDisplayName(), rxDrug.getFormCode(), rxDrug.getSaltProperty(), rxDrug.getFmStatus());
		
		for (DmDrug dmDrug : dmDrugList) {
			updateChemoDrug(dmDrug);
		}
	}
	
	private void updateChemoDrug(DmDrug dmDrug) {
		
		if (dmDrug != null && dmDrug.getDmMoeProperty() != null && "Y".equalsIgnoreCase(dmDrug.getDmMoeProperty().getChemoItem())) {
			chemoFlag = Boolean.TRUE;
		}
	}
	
	private void updateSelfOwnFlag(MedProfileMoItem medProfileMoItem) {
		
		updateSelfOwnFlag(isContinueWithOwnStockItem(medProfileMoItem));
	}
	
	private void updateSelfOwnFlag(Boolean value) {
		
		if (Boolean.FALSE.equals(selfOwnFlagInited)) {
			selfOwnFlag = Boolean.TRUE;
			selfOwnFlagInited = Boolean.TRUE;
		}
		selfOwnFlag = Boolean.TRUE.equals(selfOwnFlag) && Boolean.TRUE.equals(value);
	}
	
	private boolean isContinueWithOwnStockItem(MedProfileMoItem medProfileMoItem) {

		Boolean result = null;
		RxItem rxItem = medProfileMoItem.getRxItem();
		
		if (medProfileMoItem.isManualItem()) {
			
			for (MedProfilePoItem medProfilePoItem: medProfileMoItem.getMedProfilePoItemList()) {
				result = (result == null ? true : result) && medProfilePoItem.getActionStatus() == ActionStatus.ContinueWithOwnStock;
			}
		} else if (rxItem instanceof RxDrug) {
			
			RxDrug rxDrug = (RxDrug) rxItem;
			result = rxDrug.getActionStatus() == ActionStatus.ContinueWithOwnStock;
			
		} else if (rxItem instanceof IvRxDrug) {
			
			IvRxDrug ivRxDrug = (IvRxDrug) rxItem;
			for (IvAdditive ivAdditive: ivRxDrug.getIvAdditiveList()) {
				result = (result == null ? true : result) && ivAdditive.getActionStatus() == ActionStatus.ContinueWithOwnStock;
			}
			for (IvFluid ivFluid: ivRxDrug.getIvFluidList()) {
				result = (result == null ? true : result) && ivFluid.getActionStatus() == ActionStatus.ContinueWithOwnStock;
			}
		} else if (rxItem instanceof CapdRxDrug) {
			
			CapdRxDrug capdRxDrug = (CapdRxDrug) rxItem;
			result = capdRxDrug.getActionStatus() == ActionStatus.ContinueWithOwnStock;
		}
		
		return Boolean.TRUE.equals(result);
	}
	
	private void updateRefillExceptFlag(MedProfileItem medProfileItem) {
		
		MedProfileErrorLog medProfileErrorLog = medProfileItem.getMedProfileErrorLog();
		if (medProfileErrorLog != null && medProfileErrorLog.getType() != ErrorLogType.WardTransfer) {
			refillExceptFlag = Boolean.TRUE;
		}
	}
	
	private Date calculateAdjustedDueDate(String freqCode, Date dueDate, boolean isManualItem) {

		Set<String> freqCodeSet = isManualItem ? aomFreqCodeSet : wardAdminFreqCodeSet;
		
		if (dueDate == null || (freqCode != null && freqCodeSet.contains(freqCode))) {
			return dueDate;
		}
		
		Calendar adjustedDueCalendar = Calendar.getInstance();
		adjustedDueCalendar.setTime(dueDate);
		adjustedDueCalendar.add(Calendar.MINUTE, 
				MEDPROFILE_ITEM_ADMINTIME_BUFFER.get(medProfile.getWorkstore(), 0));
		
		return adjustedDueCalendar.getTime();
	}	
}
