package hk.org.ha.model.pms.vo.report;

import java.awt.Color;
import java.io.Serializable;

import net.sf.jasperreports.engine.JRChart;
import net.sf.jasperreports.engine.JRChartCustomizer;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BarRenderer3D;

public class EdsActivityChart implements JRChartCustomizer, Serializable
{
	private static final long serialVersionUID = 1L;

	public void customize(JFreeChart chart, JRChart jasperChart)
	{
		Color colorAqua = new Color(0, 255, 255);
		
		CategoryPlot plot = chart.getCategoryPlot();
		plot.setRangeGridlinesVisible(false);
		
		BarRenderer3D renderer = (BarRenderer3D) chart.getCategoryPlot().getRenderer();
		renderer.setSeriesPaint(0, Color.red);
		renderer.setSeriesPaint(1, Color.green);
		renderer.setSeriesPaint(2, Color.blue);
		renderer.setSeriesPaint(3, Color.yellow);
		renderer.setSeriesPaint(4, colorAqua);
		renderer.setItemMargin(0);
		renderer.setShadowXOffset(3);
		renderer.setShadowYOffset(7);
		
		ValueAxis axisV = chart.getCategoryPlot().getRangeAxis();
        axisV.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        axisV.setLowerBound(0);
	}
}
