package hk.org.ha.model.pms.biz.ehr;

import hk.org.ha.fmk.pms.sys.SysProfile;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asg.vo.ehr.EhrReturnResult;
import hk.org.ha.model.pms.vo.ehr.EhrPatient;
import hk.org.ha.model.pms.vo.ehr.EhrssViewerUrl;
import hk.org.ha.service.biz.pms.asg.interfaces.EhrServiceJmsRemote;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;
import org.jboss.seam.security.Identity;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("ehrService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class EhrServiceBean implements EhrServiceLocal {
	
	@Logger
	private Log logger; 
	
	@In
	private EhrServiceJmsRemote ehrServiceProxy;
	
	@In(required = false)
	private Long ehrUid;
	
	@In
	private Identity identity;
	
	@In
	private SysProfile sysProfile;
	
	private final static String EHR_SUCCESS_STATUS_CODE = "10000";

	public EhrssViewerUrl retrieveEhrssViewerUrl(EhrPatient ehrPatient){
		EhrssViewerUrl ehrssViewerUrl = new EhrssViewerUrl();
		if ( ehrUid == null ) {
			logger.info("No ehrUID userName:#0", identity.getCredentials().getUsername());
			ehrssViewerUrl.setSuccess(false);
			ehrssViewerUrl.setErrorMsg("1072");
			return ehrssViewerUrl;
		}
		
		try {
			EhrReturnResult result = ehrServiceProxy.retrieveEhrViewerUrl(ehrUid.toString(), ehrPatient);
			if ( result == null ) { 
				ehrssViewerUrl.setSuccess(false);
				ehrssViewerUrl.setErrorMsg("1071");
				return ehrssViewerUrl;
			}
			
			if ( !EHR_SUCCESS_STATUS_CODE.equals(result.getStatus()) ) {
				ehrssViewerUrl.setSuccess(false);
				ehrssViewerUrl.setErrorMsg("1071");
				return ehrssViewerUrl;
			}
			
			String url = buildUrl(result.getUrl(), result.getTicket());
			logger.info("ehrss viewer url:#0", url);
			
			ehrssViewerUrl.setSuccess(true);
			ehrssViewerUrl.setUrl(url);
			return ehrssViewerUrl;
		} catch (Exception e) {
			ehrssViewerUrl.setSuccess(false);
			ehrssViewerUrl.setErrorMsg("1071");
			return ehrssViewerUrl;
		}
	}
	
	private String buildUrl(String url, String ehrUrlTicket) throws UnsupportedEncodingException {
		try {
			return URLDecoder.decode(url + "?" + ehrUrlTicket, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw e;
		}
	}

	@Remove
	public void destroy() {
		if ( ehrUid != null ) {
			ehrUid = null;
		}
	}

}
