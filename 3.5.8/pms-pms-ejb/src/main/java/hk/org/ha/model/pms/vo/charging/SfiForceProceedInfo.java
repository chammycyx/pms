package hk.org.ha.model.pms.vo.charging;

import java.io.Serializable;
import java.util.Date;

import hk.org.ha.model.pms.persistence.disp.Invoice;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class SfiForceProceedInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private boolean forceProceedFlag;
	
	private String forceProceedReason;	
	
	private String forceProceedBy;
	
	private String forceProceedPw;
	
	private String invoiceNum;
	
	private String receiptNum;

	private String actionBy;
	
	private Long dispOrderId;
	
	//IPSFI Use
	private String hkid;
	
	private String caseNum;
	
	private String mpInvoiceNum;
	
	private String patHospCode;

	public void setInvoice(Invoice invoice){
		if( Boolean.TRUE.equals(forceProceedFlag) ){
			invoice.setForceProceedFlag(Boolean.TRUE);
			invoice.setForceProceedUser(forceProceedBy);
			invoice.setForceProceedDate(new Date());
			if (StringUtils.isNotBlank(forceProceedReason)){
				invoice.setForceProceedCode(forceProceedReason);						
			}else if( StringUtils.isNotBlank( invoiceNum ) ){
				invoice.setManualInvoiceNum( invoiceNum );
			}else if( StringUtils.isNotBlank( receiptNum ) ){
				invoice.setManualReceiptNum( receiptNum );
			}
		}
	}
	
	public boolean isForceProceedFlag() {
		return forceProceedFlag;
	}

	public void setForceProceedFlag(boolean forceProceedFlag) {
		this.forceProceedFlag = forceProceedFlag;
	}

	public String getForceProceedReason() {
		return forceProceedReason;
	}

	public void setForceProceedReason(String forceProceedReason) {
		this.forceProceedReason = forceProceedReason;
	}

	public String getForceProceedBy() {
		return forceProceedBy;
	}

	public void setForceProceedBy(String forceProceedBy) {
		this.forceProceedBy = forceProceedBy;
	}

	public String getForceProceedPw() {
		return forceProceedPw;
	}

	public void setForceProceedPw(String forceProceedPw) {
		this.forceProceedPw = forceProceedPw;
	}

	public String getInvoiceNum() {
		return invoiceNum;
	}

	public void setInvoiceNum(String invoiceNum) {
		this.invoiceNum = invoiceNum;
	}

	public String getReceiptNum() {
		return receiptNum;
	}

	public void setReceiptNum(String receiptNum) {
		this.receiptNum = receiptNum;
	}

	public String getActionBy() {
		return actionBy;
	}

	public void setActionBy(String actionBy) {
		this.actionBy = actionBy;
	}

	public Long getDispOrderId() {
		return dispOrderId;
	}
	
	public void setDispOrderId(Long dispOrderId) {
		this.dispOrderId = dispOrderId;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getMpInvoiceNum() {
		return mpInvoiceNum;
	}

	public void setMpInvoiceNum(String mpInvoiceNum) {
		this.mpInvoiceNum = mpInvoiceNum;
	}

	public String getPatHospCode() {
		return patHospCode;
	}	

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}
}
