package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.vo.report.WorkloadStatRpt;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface WorkloadStatRptServiceLocal {

	void retrieveWorkloadStatList(Integer pastDay, Date dateFrom, Date dateTo);
	
	List<WorkloadStatRpt> constructWorkloadStatList(Integer pastDay, Date todayWithTime, Date dateFromWithTime, Date dateToWithTime);
	
	void generateWorkloadStatRpt();
	
	void printWorkloadStatRpt();
	
	void exportWorkloadStatRpt();
	
	void destroy();
	
}
