package hk.org.ha.model.pms.biz.delivery;
import javax.ejb.Local;

@Local
public interface OverdueSummaryServiceLocal {
	void countOverdueItems(boolean explict, boolean retrieveSfiItemCountOnly);
	void destroy();
}
