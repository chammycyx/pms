package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.corp.cache.DmWarningCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmMoeProperty;
import hk.org.ha.model.pms.dms.persistence.DmWarning;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.FollowUpWarning;
import hk.org.ha.model.pms.persistence.reftable.LocalWarning;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsRptServiceJmsRemote;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("followUpWarningManager")
public class FollowUpWarningManagerBean implements FollowUpWarningManagerLocal {

	@PersistenceContext
	private EntityManager em;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private DmWarningCacherInf dmWarningCacher;
	
	@In
	private DmsRptServiceJmsRemote dmsRptServiceProxy;
	
	@SuppressWarnings("unchecked")
	public List<FollowUpWarning> retrieveFollowUpWarningList(Workstore workstore) {
		return em.createQuery(
				"select f from FollowUpWarning f" + // 20120312 index check : FollowUpWarning.workstoreGroup : FK_FOLLOW_UP_WARNING_01
				" where f.workstoreGroup = :workstoreGroup")
				.setParameter("workstoreGroup", workstore.getWorkstoreGroup())
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public void syncFollowUpWarning(List<String> itemCodeList) 
	{
		List<LocalWarning> localWarningList = em.createQuery(
				"select o from LocalWarning o" + // 20120303 index check : none
				" order by o.itemCode, o.workstoreGroup.workstoreGroupCode")
				.getResultList();
		
		List<FollowUpWarning> followUpWarningList = em.createQuery(
				"select o from FollowUpWarning o") // 20120303 index check : none
				.getResultList();
		
		Map<String, FollowUpWarning> followUpWarningMap = new HashMap<String, FollowUpWarning>();
		for (FollowUpWarning followUpWarning:followUpWarningList) {
			DmDrug dmDrug = dmDrugCacher.getDmDrug(followUpWarning.getItemCode());
			if ( dmDrug == null ) {
				em.remove(followUpWarning);
				continue;
			}

			followUpWarningMap.put(followUpWarning.getWorkstoreGroup().getWorkstoreGroupCode() + followUpWarning.getItemCode(), 
								   followUpWarning);
		}
		
		Map<String, Map<Integer, LocalWarning>> localWarningMap = new HashMap<String, Map<Integer, LocalWarning>>();
		for ( LocalWarning localWarning : localWarningList ) {
			DmDrug dmDrug = dmDrugCacher.getDmDrug(localWarning.getItemCode());
			if ( dmDrug == null ) {
				em.remove(localWarning);
				continue;
			}
			if ( localWarningMap.get(localWarning.getWorkstoreGroup()+localWarning.getItemCode()) == null ) {
				localWarningMap.put(localWarning.getWorkstoreGroup()+localWarning.getItemCode(), new HashMap<Integer, LocalWarning>());
			} 
			Map<Integer, LocalWarning> map = localWarningMap.get(localWarning.getWorkstoreGroup()+localWarning.getItemCode());
			map.put(localWarning.getSortSeq(), localWarning);
			localWarningMap.put(localWarning.getWorkstoreGroup()+localWarning.getItemCode(), map);
		}
		
		em.flush();
		em.clear();
		
		FollowUpWarning followUpWarning = null;
		List<String> overwriteLicalWarningItemList = new ArrayList<String>();
		List<LocalWarning> updateLicalWarningItemCatList = new ArrayList<LocalWarning>();
		
		List<String> followUpWarningItemList = new ArrayList<String>();
		List<FollowUpWarning> updateFollowUpWarningList = new ArrayList<FollowUpWarning>();
		 
		for (LocalWarning localWarning:localWarningList) {
			if ((followUpWarning == null || 
					(!localWarning.getItemCode().equals(followUpWarning.getItemCode()) ||
						!localWarning.getWorkstoreGroup().equals(followUpWarning.getWorkstoreGroup())) ) &&
				itemCodeList.contains(localWarning.getItemCode()) ) 
			{
				followUpWarning = new FollowUpWarning();
				followUpWarning.setWorkstoreGroup(localWarning.getWorkstoreGroup());
				followUpWarning.setItemCode(localWarning.getItemCode());
				
				if ( overwriteLocalWarnCode(localWarning.getItemCode(), localWarningMap.get(localWarning.getWorkstoreGroup()+localWarning.getItemCode())) ) 
				{
					overwriteLicalWarningItemList.add(localWarning.getWorkstoreGroup()+localWarning.getItemCode());
					if ( followUpWarningMap.containsKey(localWarning.getWorkstoreGroup().getWorkstoreGroupCode() + localWarning.getItemCode()) ) 
					{
						FollowUpWarning delFollowUpWarning = followUpWarningMap.get(localWarning.getWorkstoreGroup().getWorkstoreGroupCode() + localWarning.getItemCode());
						em.remove(em.merge(delFollowUpWarning));
					}
				} 
				else if ( isFollowUpWarningItem(localWarning.getItemCode(), localWarningMap.get(localWarning.getWorkstoreGroup()+localWarning.getItemCode()), updateLicalWarningItemCatList) )
				{
					if ( followUpWarningMap.containsKey(localWarning.getWorkstoreGroup().getWorkstoreGroupCode() + localWarning.getItemCode()) ) 
					{
						FollowUpWarning updateFollowUpWarning = followUpWarningMap.get(localWarning.getWorkstoreGroup().getWorkstoreGroupCode() + localWarning.getItemCode());
						followUpWarningItemList.add(updateFollowUpWarning.getItemCode());
						updateFollowUpWarningList.add(updateFollowUpWarning);
					} 
					else 
					{
						followUpWarningItemList.add(followUpWarning.getItemCode());
						updateFollowUpWarningList.add(followUpWarning);
					}
				}
			}
		}
		
		if ( !followUpWarningItemList.isEmpty() ) {
			Map<String, Date> modifyDateMap = dmsRptServiceProxy.retrieveFollowUpWarningItemModifyDate(followUpWarningItemList);
			for ( FollowUpWarning updateFollowUpWarning : updateFollowUpWarningList ) {
				Date modifyDate = modifyDateMap.get(updateFollowUpWarning.getItemCode());
				if ( modifyDate == null ) {
					modifyDate = new Date();
				}
				updateFollowUpWarning.setModifyDate(modifyDate);
				if ( updateFollowUpWarning.getId() == null ) {
					em.persist(updateFollowUpWarning);
				} else {
					em.merge(updateFollowUpWarning);
				}
			}
		}
		
		for ( String key : overwriteLicalWarningItemList ) {
			Map<Integer, LocalWarning> map = localWarningMap.get(key);
			for ( Map.Entry<Integer, LocalWarning> item : map.entrySet() ) {
				LocalWarning delLocalWarning = em.find(LocalWarning.class, item.getValue().getId());
				em.remove(delLocalWarning);
			}
		}
		
		for ( LocalWarning localWarning : updateLicalWarningItemCatList ) {
			em.merge(localWarning);
		}
		
		em.flush();
	}    
	
	private boolean isFollowUpWarningItem(String itemCode, Map<Integer, LocalWarning> localWarningMap, List<LocalWarning> updateLicalWarningItemCatList)
	{
		DmDrug dmDrug = dmDrugCacher.getDmDrug(itemCode);
		if ( dmDrug == null ) 
		{
			return false;
		}
		
		boolean isFollowUpWarningItem =  false;
		List<LocalWarning> updateCatCode = new ArrayList<LocalWarning>();
		for ( int sortSeq = 1; sortSeq <= 4; sortSeq++ ) 
		{
			String hqWarnCode = getHqWarnCode(dmDrug.getDmMoeProperty(), sortSeq);
			DmWarning hqDmWarning = dmWarningCacher.getDmWarningByWarnCode(hqWarnCode);
			LocalWarning localWarning = localWarningMap.get(sortSeq);
			
			if ( hqDmWarning != null && localWarning != null )
			{
				if ( !StringUtils.equals(hqDmWarning.getWarnCode(), localWarning.getOrgWarnCode()) ) 
				{
					isFollowUpWarningItem = true;
					break;
				} 
				else if ( !StringUtils.equals(hqDmWarning.getWarnCatCode(), localWarning.getOrgWarnCat()) ) 
				{
					localWarning.setOrgWarnCat(hqDmWarning.getWarnCatCode());
					updateCatCode.add(localWarning);
				}
			} 
			else if ( hqDmWarning == null && localWarning != null ) 
			{
				if ( StringUtils.isNotBlank(localWarning.getOrgWarnCode()) ) {
					isFollowUpWarningItem = true;
					break;
				} 
				else if ( StringUtils.isNotBlank(localWarning.getOrgWarnCat()) ) 
				{
					localWarning.setOrgWarnCat(StringUtils.EMPTY);
					updateCatCode.add(localWarning);
				}
			}
			else if ( hqDmWarning != null && localWarning == null ) 
			{
				isFollowUpWarningItem = true;
				break;
			}
		}
		
		if ( !isFollowUpWarningItem ) {
			updateLicalWarningItemCatList.addAll(updateCatCode);
		}
		
		return isFollowUpWarningItem;
	}
	
	private boolean overwriteLocalWarnCode(String itemCode, Map<Integer, LocalWarning> localWarningMap) 
	{
		DmDrug dmDrug = dmDrugCacher.getDmDrug(itemCode);
		if ( dmDrug == null ) 
		{
			return false;
		}
		
		boolean followCurrentHqWarnCode =  true;
		for ( int sortSeq = 1; sortSeq <= 4; sortSeq++ ) 
		{
			String hqWarnCode = getHqWarnCode(dmDrug.getDmMoeProperty(), sortSeq);
			DmWarning hqDmWarning = dmWarningCacher.getDmWarningByWarnCode(hqWarnCode);
			LocalWarning localWarning = localWarningMap.get(sortSeq);
			
			if ( hqDmWarning != null && StringUtils.equals(hqDmWarning.getWarnCatCode(), "ID") ) {//Indication warning code added to item code 
				if ( localWarning == null ) {
					return true;
				} else {
					if ( !StringUtils.equals(hqDmWarning.getWarnCode(), localWarning.getOrgWarnCode()) || 
						 !StringUtils.equals(hqDmWarning.getWarnCatCode(), localWarning.getOrgWarnCat()) ) {
						return true;
					}
				}
				
			} else if ( localWarning!=null && StringUtils.equals(localWarning.getOrgWarnCat(), "ID")) { //Indication warning code remove from item code 
				if ( hqDmWarning == null ) {
					return true;
				} else {
					if ( !StringUtils.equals(hqDmWarning.getWarnCode(), localWarning.getOrgWarnCode()) || 
						 !StringUtils.equals(hqDmWarning.getWarnCatCode(), localWarning.getOrgWarnCat()) ) {
						return true;
					}
				}
				
			} else if ( followCurrentHqWarnCode ) {
				if ( hqDmWarning != null && localWarning != null ) {
					if ( !StringUtils.equals(localWarning.getWarnCode(), hqDmWarning.getWarnCode()) ) {
						followCurrentHqWarnCode = false;
					}
				} else if ( (hqDmWarning == null && localWarning != null) || 
							( hqDmWarning != null && localWarning == null) ) {
					followCurrentHqWarnCode = false;;
				}
			}
		}
		
		return followCurrentHqWarnCode;
	}
	
	private String getHqWarnCode(DmMoeProperty dmMoeProperty, int sortSeq)
	{
		String hqWarnCode = null;
		try {
			hqWarnCode = (String) PropertyUtils.getProperty(dmMoeProperty, "warnCode"+sortSeq);
		} catch (IllegalAccessException e) {
		} catch (InvocationTargetException e) {
		} catch (NoSuchMethodException e) {
		}
		
		return hqWarnCode;
	}
}
