package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.dms.persistence.PmsDurationConversion;
import hk.org.ha.model.pms.dms.persistence.PmsQtyConversion;

import java.util.List;

import javax.ejb.Local;
 
@Local
public interface QtyDurationConversionRptServiceLocal {
	
	List<PmsQtyConversion> retrievePmsQtyConversionListForReport();

	List<PmsDurationConversion> retrievePmsDurationConversionListForReport();
	
	void printQtyDurationConversionRpt();
	
	void retrieveQtyConversionRptList();
	
	void retrieveDurConversionRptList();
	
	void exportQtyConversionRpt();
	
	void exportDurationConversionRpt();

	void destroy();
	
} 
 