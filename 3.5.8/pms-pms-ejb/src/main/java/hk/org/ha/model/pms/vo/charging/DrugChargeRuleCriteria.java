package hk.org.ha.model.pms.vo.charging;

import java.io.Serializable;

public class DrugChargeRuleCriteria implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String dispHospCode;
	
	private String pharmOrderPatType;
	
	private String medOrderPrescType;	
	
	private String pasSpecCode;
	
	private String pasSubSpecCode;
	
	private String patHospCode;
	
	private String specCode;
	
	private String pasPayCode;
	
	private String pasPatGroupCode;
	
	private String chargeType;
	
	private String fmStatus;
	
	private boolean inPatientCase;
	
	private boolean privateHandle;

	private boolean includeMarkup;
	
	private boolean includeHandling;

	public DrugChargeRuleCriteria(){
		inPatientCase = false;
		privateHandle = false;
		includeMarkup = true;
		includeHandling = true;
	}
	
	public String getDispHospCode() {
		return dispHospCode;
	}

	public void setDispHospCode(String dispHospCode) {
		this.dispHospCode = dispHospCode;
	}

	public String getPharmOrderPatType() {
		return pharmOrderPatType;
	}

	public void setPharmOrderPatType(String pharmOrderPatType) {
		this.pharmOrderPatType = pharmOrderPatType;
	}

	public String getMedOrderPrescType() {
		return medOrderPrescType;
	}

	public void setMedOrderPrescType(String medOrderPrescType) {
		this.medOrderPrescType = medOrderPrescType;
	}

	public String getPasSpecCode() {
		return pasSpecCode;
	}

	public void setPasSpecCode(String pasSpecCode) {
		this.pasSpecCode = pasSpecCode;
	}

	public String getPasSubSpecCode() {
		return pasSubSpecCode;
	}

	public void setPasSubSpecCode(String pasSubSpecCode) {
		this.pasSubSpecCode = pasSubSpecCode;
	}

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public String getPasPayCode() {
		return pasPayCode;
	}

	public void setPasPayCode(String pasPayCode) {
		this.pasPayCode = pasPayCode;
	}

	public String getPasPatGroupCode() {
		return pasPatGroupCode;
	}

	public void setPasPatGroupCode(String pasPatGroupCode) {
		this.pasPatGroupCode = pasPatGroupCode;
	}

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public String getFmStatus() {
		return fmStatus;
	}

	public void setFmStatus(String fmStatus) {
		this.fmStatus = fmStatus;
	}

	public boolean isInPatientCase() {
		return inPatientCase;
	}
	
	public void setInPatientCase(boolean inPatientCase) {
		this.inPatientCase = inPatientCase;
	}
	
	public boolean isPrivateHandle() {
		return privateHandle;
	}

	public void setPrivateHandle(boolean privateHandle) {
		this.privateHandle = privateHandle;
	}

	public boolean isIncludeMarkup() {
		return includeMarkup;
	}

	public void setIncludeMarkup(boolean includeMarkup) {
		this.includeMarkup = includeMarkup;
	}

	public boolean isIncludeHandling() {
		return includeHandling;
	}

	public void setIncludeHandling(boolean includeHandling) {
		this.includeHandling = includeHandling;
	}
}
