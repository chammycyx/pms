package hk.org.ha.model.pms.biz.enquiry;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.charging.DrugChargeClearanceManagerLocal;
import hk.org.ha.model.pms.biz.onestop.OneStopUtilsLocal;
import hk.org.ha.model.pms.biz.printing.InvoiceRendererLocal;
import hk.org.ha.model.pms.biz.validation.SearchTextValidatorLocal;
import hk.org.ha.model.pms.corp.biz.charging.InvoiceHelper;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.InvoiceItem;
import hk.org.ha.model.pms.udt.disp.InvoiceDocType;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.enquiry.SfiInvoiceEnqPatType;
import hk.org.ha.model.pms.udt.validation.SearchTextType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.charging.FcsPaymentStatus;
import hk.org.ha.model.pms.vo.enquiry.SfiInvoiceInfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.joda.time.DateTime;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("sfiInvoiceEnqService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SfiInvoiceEnqServiceBean implements SfiInvoiceEnqServiceLocal {

	@PersistenceContext
	private EntityManager em;

	@Out(required=false)
	private List<SfiInvoiceInfo> sfiInvoiceSummaryList;
	
	@Out(required=false)
	private List<SfiInvoiceInfo> sfiInvoiceSummaryWoVoidList;
	
	@Out(required=false)
	private SfiInvoiceInfo sfiInvoiceEnqSummary;
		
	@In
	private DrugChargeClearanceManagerLocal drugChargeClearanceManager;
	
	@In
	private SearchTextValidatorLocal searchTextValidator;
				
	@In
	private OneStopUtilsLocal oneStopUtils;
	
	@In
	private InvoiceRendererLocal invoiceRenderer;
	
	@In
	private Workstore workstore;
	
	@In
	private InvoiceHelper invoiceHelper;
		
	@In
	private SfiInvoiceEnqManagerLocal sfiInvoiceEnqManager;
	
	private static final String PARAM_INV_DOCTYPE = "invoiceDocType";
	private static final String PARAM_INV_STATUS = "invoiceStatusList";	
	private static final String PARAM_MEDORDER_ORDERTYPE = "medOrderType";
	private static final List<InvoiceStatus> nonSysDeletedInvoiceStatusList = Arrays.asList(InvoiceStatus.SysDeleted);
	private static final List<InvoiceStatus> nonActiveInvoiceStatusList = Arrays.asList(InvoiceStatus.Void,InvoiceStatus.SysDeleted);
	private static final String PARAM_WORKSTORE = "workstore";
	
	private boolean retrieveSuccess = false;
	
	private String errMsg;
	
	public void retrieveSfiInvoiceByInvoiceNumber(String invoiceNum, boolean includeVoidInvoice){				
		errMsg = StringUtils.EMPTY;
		retrieveSuccess = false;
		
		SearchTextType searchTextType = searchTextValidator.checkSearchInputType(invoiceNum);
		
		if( SearchTextType.SfiInvoice.equals(searchTextType) ){
			List<Invoice> invoiceList = this.retrieveSfiInvoiceListByInvoiceNum(invoiceNum, includeVoidInvoice);
			
			if( invoiceList.size() > 0 ){
				for( Invoice invoice : invoiceList ){
					invoice.loadChild();
				}
				convertInvoiceListToInvoiceSummary(invoiceList, includeVoidInvoice);			
				retrieveSuccess = true;
			}
		}else{
			errMsg = "0088";
		}
	}
	
	public void retrieveSfiInvoiceByInvoiceDate(Date invoiceDate, boolean includeVoidInvoice){				
		errMsg = StringUtils.EMPTY;
		retrieveSuccess = false;
		
		if (invoiceDate.compareTo(new DateTime().toDate()) <= 0 ){		
			
			List<Invoice> invoiceList = retrieveSfiInvoiceListByInvoiceDate(invoiceDate, includeVoidInvoice);
			
			if( invoiceList.size() > 0 ){
				for( Invoice invoice : invoiceList ){
					invoice.loadChild();
				}
				convertInvoiceListToInvoiceSummary(invoiceList, includeVoidInvoice);			
				retrieveSuccess = true;
			}
		}else{
			errMsg = "0086";
		}
	}
	
	public void retrieveSfiInvoiceByMoeOrderNumber(String moeOrderNum, boolean includeVoidInvoice){
		errMsg = StringUtils.EMPTY;
		retrieveSuccess = false;
		
		SearchTextType searchTextType = searchTextValidator.checkSearchInputType(moeOrderNum);
		
		if( SearchTextType.OrderNum.equals(searchTextType) ){			
			String searchOrderNum = oneStopUtils.convertOrderNum(moeOrderNum);

			List<Invoice> invoiceList = retrieveSfiInvoiceListByMoeOrderNum(searchOrderNum, includeVoidInvoice);
			
			if( invoiceList.size() > 0 ){
				for( Invoice invoice : invoiceList ){
					invoice.loadChild();
				}
				convertInvoiceListToInvoiceSummary(invoiceList, includeVoidInvoice);			
				retrieveSuccess = true;
			}
		}else{
			errMsg = "0085";
		}
	}
	
	public void retrieveSfiInvoiceByCaseNum(String caseNum, boolean includeVoidInvoice){
		errMsg = StringUtils.EMPTY;
		retrieveSuccess = false;		
		
		SearchTextType searchTextType = searchTextValidator.checkSearchInputType(caseNum);		
		
		if( SearchTextType.Case.equals(searchTextType) ){
			List<Invoice> invoiceList = retrieveSfiInvoiceListByCaseNum(caseNum, includeVoidInvoice);
			
			if( invoiceList.size() > 0 ){
				for( Invoice invoice : invoiceList ){
					invoice.loadChild();
				}
				convertInvoiceListToInvoiceSummary(invoiceList, includeVoidInvoice);			
				retrieveSuccess = true;
			}
		}else{
			errMsg = "0068";
		}
	}
	
	public void retrieveSfiInvoiceByHkid(String hkid, boolean includeVoidInvoice){
		errMsg = StringUtils.EMPTY;
		retrieveSuccess = false;
		
		SearchTextType searchTextType = searchTextValidator.checkSearchInputType(hkid);		
		
		if( SearchTextType.Hkid.equals(searchTextType) ){
			List<Invoice> invoiceList = this.retrieveSfiInvoiceListByHkid(hkid, includeVoidInvoice);
			
			if( invoiceList.size() > 0 ){
				for( Invoice invoice : invoiceList ){
					invoice.loadChild();
				}
				convertInvoiceListToInvoiceSummary(invoiceList, includeVoidInvoice);			
				retrieveSuccess = true;
			}
		}else{
			errMsg = "0033";
		}
	}
	
	public void retrieveSfiInvoiceByTicket(Date ticketDate, String ticketNum, boolean includeVoidInvoice){
		errMsg = StringUtils.EMPTY;
		retrieveSuccess = false;
		
		if( ticketDate.compareTo(new DateTime().toDate()) <= 0 ){			
			List<Invoice> invoiceList = this.retrieveSfiInvoiceListByTicket(ticketNum, ticketDate, includeVoidInvoice);
			
			if( invoiceList.size() > 0 ){
				for( Invoice invoice : invoiceList ){
					invoice.loadChild();
				}
				convertInvoiceListToInvoiceSummary(invoiceList, includeVoidInvoice);			
				retrieveSuccess = true;
			}
		}else{
			errMsg = "0087";				
		}
	}	
	
	private List<Invoice> retrieveSfiInvoiceListByInvoiceNum(String invoiceNum, boolean includeVoidInvoice){
		List<InvoiceItem> invoiceItemList = invoiceHelper.retrieveSfiInvoiceItemListByInvoiceNum(em, invoiceNum, includeVoidInvoice, OrderType.OutPatient, workstore, SfiInvoiceEnqPatType.Both);
		return convertToInvoiceList(invoiceItemList);
	}
	
	private List<Invoice> retrieveSfiInvoiceListByInvoiceDate(Date invoiceDate, boolean includeVoidInvoice){
		List<InvoiceItem> invoiceItemList = invoiceHelper.retrieveSfiInvoiceItemListByInvoiceDate(em, invoiceDate, includeVoidInvoice, OrderType.OutPatient, workstore, SfiInvoiceEnqPatType.Both);
		return convertToInvoiceList(invoiceItemList);
	}
	
	@SuppressWarnings("unchecked")
	private List<Invoice> retrieveSfiInvoiceListByMoeOrderNum(String orderNum, boolean includeVoidInvoice){

		return (List<Invoice>) em.createQuery(
				"select o from Invoice o" + // 20120215 index check : MedOrder.orderNum : I_MED_ORDER_01
				" where o.docType = :invoiceDocType" +
				" and o.status not in :invoiceStatusList" +
				" and o.dispOrder.workstore = :workstore" +
				" and o.dispOrder.pharmOrder.medOrder.orderNum = :orderNum" +
				" and o.dispOrder.pharmOrder.medOrder.docType = :medOrderDocType" +
				" and o.dispOrder.pharmOrder.medOrder.orderType = :medOrderType" +
				" and o.dispOrder.pharmOrder.medOrder.status <> :medOrderStatus")
				.setParameter("orderNum", orderNum)
				.setParameter("medOrderDocType", MedOrderDocType.Normal)
				.setParameter("medOrderStatus", MedOrderStatus.SysDeleted)
				.setParameter(PARAM_INV_DOCTYPE, InvoiceDocType.Sfi)
				.setParameter(PARAM_INV_STATUS, includeVoidInvoice ? nonSysDeletedInvoiceStatusList : nonActiveInvoiceStatusList )
				.setParameter(PARAM_MEDORDER_ORDERTYPE, OrderType.OutPatient)
				.setParameter(PARAM_WORKSTORE, workstore)
				.getResultList();
	}
	
	private List<Invoice> retrieveSfiInvoiceListByCaseNum(String caseNum, boolean includeVoidInvoice){		
		List<InvoiceItem> invoiceItemList = invoiceHelper.retrieveSfiInvoiceItemListByCaseNum(em, caseNum, includeVoidInvoice, OrderType.OutPatient, workstore, SfiInvoiceEnqPatType.Both);
		return convertToInvoiceList(invoiceItemList);
	}
	
	private List<Invoice> retrieveSfiInvoiceListByHkid(String hkid, boolean includeVoidInvoice){
		List<InvoiceItem> invoiceItemList = invoiceHelper.retrieveSfiInvoiceItemListByHkid(em, hkid, includeVoidInvoice, OrderType.OutPatient, workstore, SfiInvoiceEnqPatType.Both);
		return convertToInvoiceList(invoiceItemList);
	}
	
	private List<Invoice> convertToInvoiceList(List<InvoiceItem> invoiceItemList){
		Set<Invoice> invoiceSet = new HashSet<Invoice>();
		for ( InvoiceItem invoiceItem : invoiceItemList ) {
			invoiceSet.add(invoiceItem.getInvoice());
		}
		return new ArrayList<Invoice>(invoiceSet);
	}
	
	@SuppressWarnings("unchecked")
	private List<Invoice> retrieveSfiInvoiceListByTicket(String ticketNum, Date ticketDate, boolean includeVoidInvoice){
		
		return (List<Invoice>) em.createQuery(
				"select o from Invoice o" + // 20120215 index check : Ticket.workstore,orderType,ticketDate,ticketNum : UI_TICKET_01
				" where o.dispOrder.ticket.ticketDate = :ticketDate" +
				" and o.dispOrder.ticket.ticketNum = :ticketNum" +
				" and o.dispOrder.workstore = :workstore" +
				" and o.docType = :invoiceDocType" +
				" and o.status not in :invoiceStatusList" +
				" and o.dispOrder.pharmOrder.medOrder.orderType = :medOrderType" +
				" and o.dispOrder.ticket.orderType = :ticketOrderType")
				.setParameter("ticketDate", ticketDate, TemporalType.DATE)
				.setParameter("ticketNum", ticketNum)
				.setParameter(PARAM_WORKSTORE, workstore)
				.setParameter(PARAM_INV_DOCTYPE, InvoiceDocType.Sfi)
				.setParameter(PARAM_INV_STATUS, includeVoidInvoice ? nonSysDeletedInvoiceStatusList : nonActiveInvoiceStatusList )
				.setParameter(PARAM_MEDORDER_ORDERTYPE, OrderType.OutPatient)
				.setParameter("ticketOrderType", OrderType.OutPatient)
				.getResultList();
	}
	
	private void convertInvoiceListToInvoiceSummary(List<Invoice> invoiceList, boolean includeVoidInvoice){
		List<SfiInvoiceInfo> tempSfiInvoiceSummary = sfiInvoiceEnqManager.convertInvoiceListToInvoiceSummaryForSfiInvoiceEnq(invoiceList);
		if(includeVoidInvoice){
			sfiInvoiceSummaryList = tempSfiInvoiceSummary;
		}else{
			sfiInvoiceSummaryWoVoidList = tempSfiInvoiceSummary;
		}
	}
	
	public boolean isRetrieveSuccess(){
		return retrieveSuccess;
	}
			
	public void retrieveSfiInvoicePaymentStatus(Long invoiceId){
		sfiInvoiceEnqSummary = new SfiInvoiceInfo();
		
		Invoice sfiInvoice = em.find(Invoice.class, invoiceId);
		FcsPaymentStatus fcsPaymentStatus = drugChargeClearanceManager.checkSfiDrugClearanceByInvoice(sfiInvoice);
		sfiInvoiceEnqSummary.setChargeAmount(fcsPaymentStatus.getChargeAmount());
		sfiInvoiceEnqSummary.setReceiptAmount(fcsPaymentStatus.getReceiptAmount());
		sfiInvoiceEnqSummary.setReceiptMessage(fcsPaymentStatus.getReceiptMessage());
		sfiInvoiceEnqSummary.setReceiptNum(fcsPaymentStatus.getReceiptNum());
		
	}
		
	public void retrieveSfiInvoiceDetail(Long invoiceId, boolean isPreview){
		errMsg = StringUtils.EMPTY;
		
		Invoice sfiInvoice = em.find(Invoice.class, invoiceId);
		
		if( sfiInvoice != null ){
			if( isPreview ){
				invoiceRenderer.createSfiInvoiceListForPreview(sfiInvoice, true);
				return;
			}else{
				errMsg = invoiceRenderer.printSfiInvoice(sfiInvoice, true);
			}			
		}
	}
	
	public String getErrMsg() {
		return errMsg;
	}
	
	@Remove
	public void destroy(){
		if ( sfiInvoiceSummaryList != null ){
			sfiInvoiceSummaryList = null;
		}
		
		if ( sfiInvoiceSummaryWoVoidList != null ){
			sfiInvoiceSummaryWoVoidList = null;
		}
		
		if ( sfiInvoiceEnqSummary != null ){
			sfiInvoiceEnqSummary = null;
		}
	}
}
