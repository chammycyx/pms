package hk.org.ha.model.pms.biz.vetting;
import hk.org.ha.model.pms.vo.rx.RxItem;

import javax.ejb.Local;

@Local
public interface DrugConversionServiceLocal {

	void retrieveDrugConversion(RxItem rxItem);
	
	void destroy();
}
