package hk.org.ha.model.pms.biz.onestop;

import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.onestop.MoeFilterOption;
import hk.org.ha.model.pms.vo.onestop.OneStopSummary;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

@Local
public interface OneStopPrescManagerLocal {
	
	Map<String, Map<Long, OneStopSummary>> retrieveOrdersPrescMap(Date orderDate, Workstore workstore, Boolean retrieveAll);
	
	List<OneStopSummary> retrieveUnvetPrescList(Date orderDate, MoeFilterOption moeFilterOption, Workstore workstore, Boolean retrieveAll);
	
	List<OneStopSummary> filterUnvetRefillCancelledList(Map<Long, OneStopSummary> sourceMap, MoeFilterOption moeFilterOption);
}
