package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class DelCapdVoucherRptDtl implements Serializable {

	private static final long serialVersionUID = 1L;

	private Date dispDate;
	
	private String ticketNum;
	
	private String caseNum;
	
	private String patName;
	
	private String hkid;
	
	private String voucherNum;
	
	private String capdRegNum;

	private List<DelCapdVoucherRptItemDtl> delCapdVoucherRptItemDtlList;

	public Date getDispDate() {
		return dispDate;
	}

	public void setDispDate(Date dispDate) {
		this.dispDate = dispDate;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getVoucherNum() {
		return voucherNum;
	}

	public void setVoucherNum(String voucherNum) {
		this.voucherNum = voucherNum;
	}

	public String getCapdRegNum() {
		return capdRegNum;
	}

	public void setCapdRegNum(String capdRegNum) {
		this.capdRegNum = capdRegNum;
	}

	public List<DelCapdVoucherRptItemDtl> getDelCapdVoucherRptItemDtlList() {
		return delCapdVoucherRptItemDtlList;
	}

	public void setDelCapdVoucherRptItemDtlList(
			List<DelCapdVoucherRptItemDtl> delCapdVoucherRptItemDtlList) {
		this.delCapdVoucherRptItemDtlList = delCapdVoucherRptItemDtlList;
	}



}
