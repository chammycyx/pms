package hk.org.ha.model.pms.udt.vetting;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum FmStatusOption implements StringValuedEnum {
	
	SpecialDrug("XD", "Special Drug"),
	SelfFinancedItem("CD", "Self-Financed Item"),
	SafetyNetItem("CN", "Self-Financed Item with Safety Net");

	private final String dataValue;
	private final String displayValue;
	
	FmStatusOption (final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}
	
	public static FmStatusOption dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(FmStatusOption.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<FmStatusOption> {

		private static final long serialVersionUID = 1L;

		public Class<FmStatusOption> getEnumClass() {
    		return FmStatusOption.class;
    	}
    }
}
