package hk.org.ha.model.pms.biz.alert.mds;

import static hk.org.ha.model.pms.prop.Prop.*;
import hk.org.ha.fmk.pms.remote.marshall.EclipseLinkXStreamMarshaller;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.AlertEntity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

@AutoCreate
@Scope(ScopeType.APPLICATION)
@Name("alertComparator")
@MeasureCalls
public class AlertComparator implements AlertComparatorInf {

	@Logger
	private Log logger;

	public List<AlertEntity> constructOutStandingAlertList(Collection<? extends AlertEntity> currentAlertList, Collection<? extends AlertEntity> prevAlertList, boolean allowSubSetPatAlertFlag) {
		return constructOutStandingAlertList(currentAlertList, prevAlertList, allowSubSetPatAlertFlag, true, MEDPROFILE_ALERT_HLA_ENABLED.get(Boolean.FALSE));
	}
	
	private List<AlertEntity> constructOutStandingAlertList(
			Collection<? extends AlertEntity> currentAlertList,
			Collection<? extends AlertEntity> prevAlertList,
			boolean allowSubSetPatAlertFlag,
			boolean copyOverrideReasonFlag,
			boolean medProfileAlertHlaEnabled) {
		
		Map<Integer, List<AlertEntity>> currentPatMap = new LinkedHashMap<Integer, List<AlertEntity>>();
		Map<Integer, List<AlertEntity>> currentDdiMap = new LinkedHashMap<Integer, List<AlertEntity>>();
		
		Map<Integer, List<AlertEntity>> prevPatMap = new LinkedHashMap<Integer, List<AlertEntity>>();
		Map<Integer, List<AlertEntity>> prevDdiMap = new LinkedHashMap<Integer, List<AlertEntity>>();
		
		int prevPatCount = 0;
		int currentPatCount = 0;
		
		for (AlertEntity alertEntity : currentAlertList) {
			Map<Integer, List<AlertEntity>> currentMap;
			if (alertEntity.isPatAlert(medProfileAlertHlaEnabled)) {
				currentMap = currentPatMap;
				currentPatCount++;
			}
			else if (alertEntity.isDdiAlert()) {
				currentMap = currentDdiMap;
			}
			else {
				continue;
			}
			Integer key = Integer.valueOf(alertEntity.getAlert().hashCode());
			if (currentMap.containsKey(key)) {
				currentMap.get(key).add(alertEntity);
			}
			else {
				currentMap.put(key, new ArrayList<AlertEntity>(Arrays.asList(alertEntity)));
			}
		}
		for (AlertEntity alertEntity : prevAlertList) {
			Map<Integer, List<AlertEntity>> prevMap;
			if (alertEntity.isPatAlert(medProfileAlertHlaEnabled)) {
				prevMap = prevPatMap;
				prevPatCount++;
			}
			else if (alertEntity.isDdiAlert()) {
				prevMap = prevDdiMap;
			}
			else {
				continue;
			}
			Integer key = Integer.valueOf(alertEntity.getAlert().hashCode());
			if (prevMap.containsKey(key)) {
				prevMap.get(key).add(alertEntity);
			}
			else {
				prevMap.put(key, new ArrayList<AlertEntity>(Arrays.asList(alertEntity)));
			}
		}

		List<AlertEntity> outStandingList = new ArrayList<AlertEntity>();
		
		// compare ddi alert
		for (Entry<Integer, List<AlertEntity>> entry : currentDdiMap.entrySet()) {
			if (prevDdiMap.containsKey(entry.getKey())) {
				if (copyOverrideReasonFlag) {
					copyOverrideReasonForList(prevDdiMap.get(entry.getKey()), entry.getValue());
				}
				else {
					copyAckUserForList(prevDdiMap.get(entry.getKey()), entry.getValue());
				}
			}
			else {
				outStandingList.addAll(entry.getValue());
			}
		}
		if (logger.isDebugEnabled()) {
			if (!outStandingList.isEmpty()) {
				logger.debug("prevDdiMap:\n");
				for (Entry<Integer, List<AlertEntity>> entry : prevDdiMap.entrySet()) {
					logger.debug("#0:\ncount=#1\n#2", entry.getKey(), entry.getValue().size(), (new EclipseLinkXStreamMarshaller()).toXML(entry.getValue().get(0).getAlert()));
				}
				
				logger.debug("currentDdiMap:\n");
				for (Entry<Integer, List<AlertEntity>> entry : currentDdiMap.entrySet()) {
					logger.debug("#0:\ncount=#1\n#2", entry.getKey(), entry.getValue().size(), (new EclipseLinkXStreamMarshaller()).toXML(entry.getValue().get(0).getAlert()));
				}
			}
		}

		// compare patient-specific alert
		if (currentPatMap.isEmpty()
		 || (prevPatCount > currentPatCount && allowSubSetPatAlertFlag && isAlertMapSubset(prevPatMap, currentPatMap))
		 || (prevPatCount == currentPatCount && isAlertMapEqual(prevPatMap, currentPatMap))
		) {
			if (!prevPatMap.isEmpty()) {
				AlertEntity prevPat = (new ArrayList<List<AlertEntity>>(prevPatMap.values())).get(0).get(0);
				for (List<AlertEntity> currentPatList : currentPatMap.values()) {
					for (AlertEntity currentPat : currentPatList) {
						if (copyOverrideReasonFlag) {
							copyOverrideReason(prevPat, currentPat);
						}
						else {
							copyAckUser(prevPat, currentPat);
						}
					}
				}
			}
		}
		else {
			if (logger.isDebugEnabled()) {
				logger.debug("prevPatMap:\n");
				for (Entry<Integer, List<AlertEntity>> entry : prevPatMap.entrySet()) {
					logger.debug("#0:\ncount=#1\n#2", entry.getKey(), entry.getValue().size(), (new EclipseLinkXStreamMarshaller()).toXML(entry.getValue().get(0).getAlert()));
				}
				
				logger.debug("currentPatMap:\n");
				for (Entry<Integer, List<AlertEntity>> entry : currentPatMap.entrySet()) {
					logger.debug("#0:\ncount=#1\n#2", entry.getKey(), entry.getValue().size(), (new EclipseLinkXStreamMarshaller()).toXML(entry.getValue().get(0).getAlert()));
				}
			}
			for (List<AlertEntity> currentPatList :currentPatMap.values()) {
				outStandingList.addAll(currentPatList);
			}
		}

		return outStandingList;
	}
	
	public boolean isAlertListEqual(Collection<? extends AlertEntity> alertList1, Collection<? extends AlertEntity> alertList2) {

		Map<Integer, List<AlertEntity>> alertMap1 = new LinkedHashMap<Integer, List<AlertEntity>>();
		Map<Integer, List<AlertEntity>> alertMap2 = new LinkedHashMap<Integer, List<AlertEntity>>();
		
		for (AlertEntity alertEntity : alertList1) {
			if (alertEntity.isPatAlert() || alertEntity.isDdiAlert()) {
				Integer key = alertEntity.getAlert().hashCode();
				if (alertMap1.containsKey(key)) {
					alertMap1.get(key).add(alertEntity);
				}
				else {
					alertMap1.put(key, new ArrayList<AlertEntity>(Arrays.asList(alertEntity)));
				}
			}
		}

		for (AlertEntity alertEntity : alertList2) {
			if (alertEntity.isPatAlert() || alertEntity.isDdiAlert()) {
				Integer key = alertEntity.getAlert().hashCode();
				if (alertMap2.containsKey(key)) {
					alertMap2.get(key).add(alertEntity);
				}
				else {
					alertMap2.put(key, new ArrayList<AlertEntity>(Arrays.asList(alertEntity)));
				}
			}
		}

		return isAlertMapEqual(alertMap1, alertMap2);
	}
	
	public boolean copyAlertOverride(boolean medProfileAlertHlaEnabled, Collection<? extends AlertEntity> mainList, Collection<? extends AlertEntity> subList) {
		return constructOutStandingAlertList(subList, mainList, false, false, medProfileAlertHlaEnabled).isEmpty();
	}
	
	private boolean isAlertMapSubset(Map<Integer, List<AlertEntity>> prevAlertMap, Map<Integer, List<AlertEntity>> currentAlertMap) {
		for (Entry<Integer, List<AlertEntity>> currentAlertEntry : currentAlertMap.entrySet()) {
			Integer currentAlertKey = currentAlertEntry.getKey();
			if (!prevAlertMap.containsKey(currentAlertKey)) {
				return false;
			}
			List<AlertEntity> currentAlertList = currentAlertEntry.getValue();
			List<AlertEntity> prevAlertList    = prevAlertMap.get(currentAlertKey);
			if (currentAlertList.size() > prevAlertList.size()) {
				return false;
			}
		}
		return true;
	}
	
	private boolean isAlertMapEqual(Map<Integer, List<AlertEntity>> alertMap1, Map<Integer, List<AlertEntity>> alertMap2) {
		if (alertMap1.keySet().size() != alertMap2.keySet().size()) {
			return false;
		}
		for (Entry<Integer, List<AlertEntity>> entry : alertMap1.entrySet()) {
			if (!alertMap2.containsKey(entry.getKey())) {
				return false;
			}
			if (entry.getValue().size() != alertMap2.get(entry.getKey()).size()) {
				return false;
			}
		}
		return true;
	}
	
	private void copyOverrideReasonForList(List<AlertEntity> fromList, List<AlertEntity> toList) {
		int copyCount = fromList.size() > toList.size() ? toList.size() : fromList.size();
		for (int i=0; i<copyCount; i++) {
			copyOverrideReason(fromList.get(i), toList.get(i));
		}
	}
	
	private void copyOverrideReason(AlertEntity fromAlert, AlertEntity toAlert) {
		toAlert.setOverrideReason(fromAlert.getOverrideReason());
		toAlert.setAckDate(fromAlert.getAckDate());
		toAlert.setAckUser(fromAlert.getAckUser());
	}
	
	private void copyAckUserForList(List<AlertEntity> fromList, List<AlertEntity> toList) {
		int copyCount = fromList.size() > toList.size() ? toList.size() : fromList.size();
		for (int i=0; i<copyCount; i++) {
			copyAckUser(fromList.get(i), toList.get(i));
		}
	}

	private void copyAckUser(AlertEntity fromAlert, AlertEntity toAlert) {
		toAlert.setAckDate(fromAlert.getAckDate());
		toAlert.setAckUser(fromAlert.getAckUser());
	}
}
