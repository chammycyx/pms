package hk.org.ha.model.pms.udt.report;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PrescRptStatus implements StringValuedEnum {

	Vetted("Vetted", "Vetted"),
	Picked("Picked", "Picked"),
	Assembled("Assembled", "Assembled"),
	Checked("Checked", "Checked"),
	Issued("Issued", "Issued"),
	Suspended("Suspended", "Suspended"), 
	Deleted("Deleted", "Deleted"),
	NoLabel("NoLabel", "No Label"), 
	Capd("Capd", "CAPD"), 
	DelCapd("DelCapd", "Del (CAPD)");
	
    private final String dataValue;
    private final String displayValue;

    PrescRptStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static PrescRptStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PrescRptStatus.class, dataValue);
	}
}
