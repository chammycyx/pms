package hk.org.ha.model.pms.biz.enquiry;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileActionLog;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.udt.medprofile.ActionLogType;
import hk.org.ha.model.pms.vo.enquiry.OrderPendingSuspendLogEnq;
import hk.org.ha.model.pms.vo.rx.RxItem;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("orderPendingSuspendLogEnqService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class OrderPendingSuspendLogEnqServiceBean implements OrderPendingSuspendLogEnqServiceLocal {
	
	private static final String JAXB_CONTEXT_RX = "hk.org.ha.model.pms.vo.rx";
	
	private OrderPendingSuspendLogEnqListComparator orderPendingSuspendLogEnqListComparator = new OrderPendingSuspendLogEnqListComparator();
	
	@In
	private OrderPendingSuspendLogEnqManagerLocal orderPendingSuspendLogEnqManager;
	
	@Out(required=false)
	private List<OrderPendingSuspendLogEnq> orderPendingSuspendLogEnqList;
	
	public void retrieveOrderPendingSuspendLogEnqList(Date fromDate, Date toDate, String caseNum, String filter) {
		
		orderPendingSuspendLogEnqList = new ArrayList<OrderPendingSuspendLogEnq>();
		
		List<MedProfileActionLog> orderPendingSuspendLogEnqTempList = orderPendingSuspendLogEnqManager.retrieveOrderPendingSuspendLogEnqListByCriteria(fromDate, toDate, caseNum, filter);
		
		for ( MedProfileActionLog medProfileActionLog : orderPendingSuspendLogEnqTempList ){

			orderPendingSuspendLogEnqList.add(constructOrderPendingSuspendLogEnq(medProfileActionLog));
			
		}
		
		Collections.sort(orderPendingSuspendLogEnqList, orderPendingSuspendLogEnqListComparator);
	}
	
	private OrderPendingSuspendLogEnq constructOrderPendingSuspendLogEnq(MedProfileActionLog medProfileActionLog){

		JaxbWrapper<RxItem> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
		
		MedProfileMoItem medProfileMoItem = medProfileActionLog.getMedProfileMoItem();
		MedProfile medProfile = medProfileActionLog.getMedProfileMoItem().getMedProfileItem().getMedProfile();
		Workstore workstore = medProfileActionLog.getMedProfileMoItem().getMedProfileItem().getMedProfile().getWorkstore();
		MedCase medcase = medProfileActionLog.getMedProfileMoItem().getMedProfileItem().getMedProfile().getMedCase();
		RxItem rxItem = rxJaxbWrapper.unmarshall(medProfileMoItem.getRxItemXml());
		rxItem.buildTlf();

		OrderPendingSuspendLogEnq orderPendingSuspendLogEnq = new OrderPendingSuspendLogEnq();
		
		orderPendingSuspendLogEnq.setActionDate(medProfileActionLog.getCreateDate());
		orderPendingSuspendLogEnq.setCaseNum(medcase.getCaseNum());
		orderPendingSuspendLogEnq.setWard(medProfileActionLog.getWardCode());
		
		//Format for IP only
		orderPendingSuspendLogEnq.setOrderDesc(rxItem.getFormattedDrugOrderTlf());
		orderPendingSuspendLogEnq.setActionByName(medProfileActionLog.getActionUserName());
		orderPendingSuspendLogEnq.setActionByUser(medProfileActionLog.getActionUser());
		orderPendingSuspendLogEnq.setAction(medProfileActionLog.getType().getDataValue());
		orderPendingSuspendLogEnq.setReason(medProfileActionLog.getMessage());
		orderPendingSuspendLogEnq.setSupplReason(medProfileActionLog.getSupplMessage());
		orderPendingSuspendLogEnq.setRemark(medProfileActionLog.getMessage());
		orderPendingSuspendLogEnq.setDoctor(medProfileActionLog.getActionUser());
		orderPendingSuspendLogEnq.setDoctorUser(medProfileActionLog.getActionUserName());
		orderPendingSuspendLogEnq.setMsgByDoctor(medProfileActionLog.getMessage());
		orderPendingSuspendLogEnq.setPrescHosp(medProfile.getPatHospCode());
		orderPendingSuspendLogEnq.setHosp(workstore.getHospCode());
		orderPendingSuspendLogEnq.setWorkstore(workstore.getWorkstoreCode());
		orderPendingSuspendLogEnq.setItemNum(medProfileMoItem.getItemNum());
		if ( medProfileActionLog.getType() == ActionLogType.Suspend || medProfileActionLog.getType() == ActionLogType.Pending) {
			orderPendingSuspendLogEnq.setSuspendPendCode(medProfileActionLog.getActionCode());
		}
		return orderPendingSuspendLogEnq;
		
	}

	@Remove
	public void destroy(){
		if ( orderPendingSuspendLogEnqList != null ){
			orderPendingSuspendLogEnqList = null;
		}
	}
	
	private static class OrderPendingSuspendLogEnqListComparator implements Comparator<OrderPendingSuspendLogEnq>, Serializable
    {
		private static final long serialVersionUID = 1L;

		@Override
          public int compare(OrderPendingSuspendLogEnq o1, OrderPendingSuspendLogEnq o2) {
                return new CompareToBuilder()
                	  .append( o1.getActionDate(), o2.getActionDate())
                      .toComparison();
          }
    }
  
}
