package hk.org.ha.model.pms.biz.order;

import hk.org.ha.model.pms.biz.order.AutoDispConfig.AutoDispWorkstation;
import hk.org.ha.model.pms.biz.order.AutoDispConfig.AutoDispWorkstore;
import hk.org.ha.model.pms.persistence.corp.Workstore;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;


@Startup
@Name("autoDispScheduler")
@Scope(ScopeType.APPLICATION)
public class AutoDispScheduler implements AutoDispSchedulerInf {

	private Map<Workstore,WorkstoreScheduler> workstoreSchMap = Collections.synchronizedMap(new HashMap<Workstore,WorkstoreScheduler>());
	
	@In
	private AutoDispConfig autoDispConfig;
	
	@Create
	public void init() {
		for (AutoDispWorkstore ws : autoDispConfig.getAutoDispWorkstoreList()) {
			
			List<String> workstationKeyList = new ArrayList<String>();			
			for (AutoDispWorkstation workstation : ws.getAutoDispWorkstationList()) {
				workstationKeyList.add(workstation.getKey());
			}
			
			workstoreSchMap.put(
					ws.getWorkstore(), 
					new WorkstoreScheduler(workstationKeyList)
			);
		}
	}
		
	public void scheduleMedOrder(Workstore workstore, String orderNum) {	
		
		if (!configurated(workstore)) {
			return;
		}

		workstoreSchMap.get(workstore).scheduleMedOrder(orderNum);
	}
	
	public void checkInWorkstation(Workstore workstore, String workstationKey) {

		if (!configurated(workstore)) {
			return;
		}
		
		workstoreSchMap.get(workstore).checkInWorkstation(workstationKey);
	}
	
	public String checkOutWorkstation(Workstore workstore) {	
		
		if (!configurated(workstore)) {
			return null;
		}
		
		return workstoreSchMap.get(workstore).checkOutWorkstation();
	}
		
	public String getNextOrderNumToRun(Workstore workstore) {	

		if (!configurated(workstore)) {
			return null;
		}

		return workstoreSchMap.get(workstore).getNextOrderNumToRun();
	}
	
	public boolean configurated(Workstore workstore) {
		return autoDispConfig.isEnable() && 
			workstoreSchMap.containsKey(workstore);
	}
		
	public static class WorkstoreScheduler
	{
		private List<String> inactiveList = new ArrayList<String>();
		private List<String> activeList = new ArrayList<String>();

		private List<String> orderQueueList = new ArrayList<String>();
						
		public WorkstoreScheduler(List<String> workstationKeyList) {
			super();
			this.inactiveList.addAll(workstationKeyList);
		}

		public void checkInWorkstation(String workstationKey) {
			synchronized (this) {
				if (activeList.remove(workstationKey)) {
					inactiveList.add(workstationKey);
				}
			}
		}
		
		public String checkOutWorkstation() {	
			synchronized (this) {
				if (!inactiveList.isEmpty()) {
					String t = inactiveList.remove(0);
					activeList.add(t);
					return t;
				} else {
					return null;
				}
			}
		}
		
		public void scheduleMedOrder(String orderNum) {
			synchronized (orderQueueList) {
				orderQueueList.add(orderNum);
			}
		}
				
		public String getNextOrderNumToRun() {	
			
			synchronized (orderQueueList) {
				if (!orderQueueList.isEmpty()) {
					return orderQueueList.remove(0);
				} else {
					return null;
				}
			}
		}
	}
	
}
