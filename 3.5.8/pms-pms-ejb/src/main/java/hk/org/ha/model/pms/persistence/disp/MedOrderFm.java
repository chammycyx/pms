package hk.org.ha.model.pms.persistence.disp;

import hk.org.ha.model.pms.corp.cache.DmCorpIndicationCacher;
import hk.org.ha.model.pms.persistence.FmEntity;
import hk.org.ha.model.pms.util.StopWatcher;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.jboss.seam.security.Identity;

@Entity
@Table(name = "MED_ORDER_FM")
public class MedOrderFm extends FmEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "medOrderFmSeq")
	@SequenceGenerator(name = "medOrderFmSeq", sequenceName = "SQ_MED_ORDER_FM", initialValue = 100000000)
	private Long id;
		
	@ManyToOne
	@JoinColumn(name = "MED_ORDER_ID")
	private MedOrder medOrder;

    @PostLoad
    public void postLoad() {
    	if (StopWatcher.isDebugEnabled()) {
    		StopWatcher.resume();
    	}
    
    	if (Identity.instance().isLoggedIn()) {
    		loadDmCorpIndicationCacher();
    	}
		
    	if (StopWatcher.isDebugEnabled()) {
    		StopWatcher.suspend();
    	}
    }
    
    public void loadDmCorpIndicationCacher()
    {
    	if (this.getCorpIndCode() != null) {
    		setDmCorpIndication(DmCorpIndicationCacher.instance().getDmCorpIndication(this.getCorpIndCode()));
    	}
    }
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MedOrder getMedOrder() {
		return medOrder;
	}

	public void setMedOrder(MedOrder medOrder) {
		this.medOrder = medOrder;
	}
}
