package hk.org.ha.model.pms.biz.drug;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.vo.RouteForm;
import hk.org.ha.model.pms.dms.vo.RouteFormSearchCriteria;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("routeFormListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class RouteFormListServiceBean implements RouteFormListServiceLocal {

	@Logger
	private Log logger;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private Workstore workstore;
	
	public List<RouteForm> retrieveRouteFormList(RouteFormSearchCriteria criteria) {
		criteria.setDispHospCode(workstore.getHospCode());
		criteria.setDispWorkstore(workstore.getWorkstoreCode());
		logger.debug("retrieveRouteFormList - RouteFormSearchCriteria | #0", criteria.toString());
		List<RouteForm> routeFormList = dmsPmsServiceProxy.retrieveRouteForm( criteria );
		if ( routeFormList != null ) {
			logger.debug("routeFormList size #0", routeFormList.size());			
		}
		return routeFormList;
	}	
	
	@Remove
	public void destroy() {
	}
}
