package hk.org.ha.model.pms.udt.vetting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum ActionStatus implements StringValuedEnum {

	DispByPharm("Y", "Dispense by Pharmacy", 1),
	PurchaseByPatient("P", "Purchase by Patient", 4),
	SafetyNet("S", "Safety Net", 2),
	DispInClinic("C", "Dispense in Clinic", 3),
	ContinueWithOwnStock("N", "Continue with Own Stock", 5);
	
	public static final List<ActionStatus> PurchaseByPatient_SafetyNet = Arrays.asList(PurchaseByPatient, SafetyNet); 
	
    private final String dataValue;
    private final String displayValue;
    private final int rank;

    ActionStatus(final String dataValue, final String displayValue, final int rank){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
        this.rank = rank;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    } 
    
    public int getRank() {
        return this.rank;
    }

	public static ActionStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(ActionStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<ActionStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<ActionStatus> getEnumClass() {
    		return ActionStatus.class;
    	}
    }

	public static List<ActionStatus> getCompatActionStatusByPrescOption(FmStatusOption prescOption) {
		List<ActionStatus> actionStatusList = new ArrayList<ActionStatus>();
		
		switch (prescOption) {
			case SpecialDrug:
				actionStatusList.add(ActionStatus.DispByPharm);
				actionStatusList.add(ActionStatus.ContinueWithOwnStock);
				actionStatusList.add(ActionStatus.DispInClinic);
				return actionStatusList;
			case SelfFinancedItem:
				actionStatusList.add(ActionStatus.PurchaseByPatient);
				actionStatusList.add(ActionStatus.ContinueWithOwnStock);
				return actionStatusList;
			case SafetyNetItem:
				actionStatusList.add(ActionStatus.SafetyNet);
				actionStatusList.add(ActionStatus.ContinueWithOwnStock);
				return actionStatusList;
		}
		
		return null;
	}
	
	public static ActionStatus getDefaultActionStatusByPrescOption(FmStatusOption prescOption) {
		switch (prescOption) {
			case SpecialDrug:
				return ActionStatus.DispByPharm;
			case SelfFinancedItem:
				return ActionStatus.PurchaseByPatient;
			case SafetyNetItem:
				return ActionStatus.SafetyNet;
		}
		
		return null;
	}
	
	public static ActionStatus getActionStatusByFmStatus(String fmStatusStr, Boolean specialDrugFlag) {
		FmStatus fmStatus = FmStatus.dataValueOf(fmStatusStr);
		
		switch (fmStatus) {
			case GeneralDrug:
				return ActionStatus.DispByPharm;
			case SpecialDrug:
				return ActionStatus.DispByPharm;
			case SafetyNetItem:
				if (specialDrugFlag) {
					return ActionStatus.DispByPharm;
				} else {
					return ActionStatus.PurchaseByPatient; 
				}
			case SelfFinancedItemC:
			case SelfFinancedItemD:
				return ActionStatus.PurchaseByPatient;
			case UnRegisteredDrug:
				return ActionStatus.DispByPharm;
			case SampleItem:
				return ActionStatus.DispByPharm;
		}
		
		return null;
	}
}
