package hk.org.ha.model.pms.biz.alert.mds;

import hk.org.ha.model.pms.persistence.AlertEntity;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.vo.alert.mds.MdsResult;

import java.util.Collection;
import java.util.List;

import javax.ejb.Local;

@Local
public interface AlertRetrieveManagerLocal {

	List<AlertEntity> retrieveAlertByMedOrderItem(MedOrderItem medOrderItem, boolean checkDdiFlag);

	MdsResult retrieveAlertByMedOrderItemList(Collection<MedOrderItem> medOrderItemList, boolean checkDdiFlag, boolean checkSteroid);
	
	MdsResult retrieveAlertByMedProfileItemList(MedProfile medProfile, Collection<MedProfileItem> medProfileItemList, boolean checkDdiFlag);
	
	boolean isMdsEnable();
	
	MdsResult retrieveMpAlert(Collection<MedProfileItem> medProfileItems, MedProfile medProfile, boolean checkDdi);
}
