package hk.org.ha.model.pms.vo.pivas.worklist;

import hk.org.ha.model.pms.dms.persistence.DmWarning;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasContainer;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.persistence.pivas.MaterialRequest;
import hk.org.ha.model.pms.persistence.reftable.pivas.PivasServiceHour;
import hk.org.ha.model.pms.vo.pivas.PivasDiluentInfo;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PivasPrepDetailInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String errorCode;
	
	private MaterialRequest firstDoseMaterialRequest;
	
	private List<PivasContainer> pivasContainerList;
	
	private List<PivasDiluentInfo> solventPivasDiluentInfoList;

	private List<PivasDiluentInfo> diluentPivasDiluentInfoList;
	
	private List<DmWarning> dmWarningList;

	private List<PivasServiceHour> pivasServiceHourList;
	
	private List<MedProfilePoItem> mrMedProfilePoItemList;

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public MaterialRequest getFirstDoseMaterialRequest() {
		return firstDoseMaterialRequest;
	}

	public void setFirstDoseMaterialRequest(MaterialRequest firstDoseMaterialRequest) {
		this.firstDoseMaterialRequest = firstDoseMaterialRequest;
	}	

	public List<PivasContainer> getPivasContainerList() {
		return pivasContainerList;
	}

	public void setPivasContainerList(List<PivasContainer> pivasContainerList) {
		this.pivasContainerList = pivasContainerList;
	}

	public List<PivasDiluentInfo> getSolventPivasDiluentInfoList() {
		return solventPivasDiluentInfoList;
	}

	public void setSolventPivasDiluentInfoList(List<PivasDiluentInfo> solventPivasDiluentInfoList) {
		this.solventPivasDiluentInfoList = solventPivasDiluentInfoList;
	}

	public List<PivasDiluentInfo> getDiluentPivasDiluentInfoList() {
		return diluentPivasDiluentInfoList;
	}

	public void setDiluentPivasDiluentInfoList(List<PivasDiluentInfo> diluentPivasDiluentInfoList) {
		this.diluentPivasDiluentInfoList = diluentPivasDiluentInfoList;
	}

	public List<DmWarning> getDmWarningList() {
		return dmWarningList;
	}

	public void setDmWarningList(List<DmWarning> dmWarningList) {
		this.dmWarningList = dmWarningList;
	}

	public List<PivasServiceHour> getPivasServiceHourList() {
		return pivasServiceHourList;
	}

	public void setPivasServiceHourList(List<PivasServiceHour> pivasServiceHourList) {
		this.pivasServiceHourList = pivasServiceHourList;
	}

	public List<MedProfilePoItem> getMrMedProfilePoItemList() {
		return mrMedProfilePoItemList;
	}

	public void setMrMedProfilePoItemList(List<MedProfilePoItem> mrMedProfilePoItemList) {
		this.mrMedProfilePoItemList = mrMedProfilePoItemList;
	}	
}