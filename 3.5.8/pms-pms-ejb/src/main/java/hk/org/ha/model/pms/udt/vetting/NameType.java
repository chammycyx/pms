package hk.org.ha.model.pms.udt.vetting;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum NameType implements StringValuedEnum {
	
	Blank(" ", "(Blank)"),
	DisplayName("D", "Display Name"),
	TradeName("T", "Trade Name");

	private final String dataValue;
	private final String displayValue;
	
	NameType (final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}
	
	public static NameType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(NameType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<NameType> {
		private static final long serialVersionUID = 1L;

		public Class<NameType> getEnumClass() {
    		return NameType.class;
    	}
    }
}
