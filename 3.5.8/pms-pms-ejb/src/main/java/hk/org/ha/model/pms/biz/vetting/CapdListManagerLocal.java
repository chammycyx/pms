package hk.org.ha.model.pms.biz.vetting;

import hk.org.ha.model.pms.dms.vo.Capd;

import java.util.List;

import javax.ejb.Local;

@Local
public interface CapdListManagerLocal {

	List<String> retrieveCapdSupplierSystemList();
	
	List<String> retrieveCapdCalciumStrengthList(String supplierSystem);
	
	List<Capd> retrieveCapdConcentrationList(String supplierSystem, String calciumStrength);
	
	List<Capd> retrieveCapdConcentrationListByItemCode(String itemCode);
}
