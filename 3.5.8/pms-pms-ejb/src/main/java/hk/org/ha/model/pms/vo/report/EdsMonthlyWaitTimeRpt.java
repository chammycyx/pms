package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class EdsMonthlyWaitTimeRpt implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private EdsWaitTimeAuditRptHdr edsWaitTimeAuditRptHdr;	
	private List<EdsDayOfWeekElapseTime> edsMonthlyWaitTimeRptDtlList;
	
	public void setEdsWaitTimeAuditRptHdr(EdsWaitTimeAuditRptHdr edsWaitTimeAuditRptHdr) {
		this.edsWaitTimeAuditRptHdr = edsWaitTimeAuditRptHdr;
	}
	public EdsWaitTimeAuditRptHdr getEdsWaitTimeAuditRptHdr() {
		return edsWaitTimeAuditRptHdr;
	}
	public void setEdsMonthlyWaitTimeRptDtlList(
			List<EdsDayOfWeekElapseTime> edsMonthlyWaitTimeRptDtlList) {
		this.edsMonthlyWaitTimeRptDtlList = edsMonthlyWaitTimeRptDtlList;
	}
	public List<EdsDayOfWeekElapseTime> getEdsMonthlyWaitTimeRptDtlList() {
		return edsMonthlyWaitTimeRptDtlList;
	}
}
