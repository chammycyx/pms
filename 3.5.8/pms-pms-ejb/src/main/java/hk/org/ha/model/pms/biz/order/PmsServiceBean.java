package hk.org.ha.model.pms.biz.order;

import static hk.org.ha.model.pms.prop.Prop.ALERT_HLA_DRUG_DISPLAYNAME;
import static hk.org.ha.model.pms.prop.Prop.PATIENT_PAS_PATIENT_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.VETTING_MODIFIEDMOITEM_PRINTING_URGENT;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.util.StringArray;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.UnreachableException;
import hk.org.ha.model.pms.asa.exception.pas.PasException;
import hk.org.ha.model.pms.biz.ApplicationProp;
import hk.org.ha.model.pms.biz.inbox.PharmInboxClientNotifierInf;
import hk.org.ha.model.pms.biz.medprofile.MedProfileOrderManagerLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileOrderProcessorLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileOrderSchedulerInf;
import hk.org.ha.model.pms.biz.order.AutoDispConfig.AutoDispWorkstation;
import hk.org.ha.model.pms.biz.vetting.UnvetManagerLocal;
import hk.org.ha.model.pms.dms.vo.conversion.ConvertParam;
import hk.org.ha.model.pms.pas.PasServiceWrapper;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderFm;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedOrderItemAlert;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileOrder;
import hk.org.ha.model.pms.persistence.reftable.SuspendStat;
import hk.org.ha.model.pms.persistence.reftable.WorkloadStat;
import hk.org.ha.model.pms.udt.disp.DiscontinueStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderBatchProcessingType;
import hk.org.ha.model.pms.udt.disp.DispOrderDayEndStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.disp.OpTrxType;
import hk.org.ha.model.pms.udt.disp.RemarkItemStatus;
import hk.org.ha.model.pms.udt.disp.RemarkStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileOrderType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileType;
import hk.org.ha.model.pms.udt.rx.RxItemType;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.alert.mds.AlertDdim;
import hk.org.ha.model.pms.vo.rx.CapdItem;
import hk.org.ha.model.pms.vo.rx.IvAdditive;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.springframework.beans.BeanUtils;

@Stateless
@Name("pmsService")
@MeasureCalls
public class PmsServiceBean implements PmsServiceLocal {

	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private UnvetManagerLocal unvetManager;
	
	@In
	private OrderManagerLocal orderManager;

	@In
	private MedProfileOrderManagerLocal medProfileOrderManager;
		
	@In
	private MedProfileOrderProcessorLocal medProfileOrderProcessor;

	@In
	private MedProfileOrderSchedulerInf medProfileOrderScheduler;
	
	@In
	private PasServiceWrapper pasServiceWrapper;

	@In
	private PharmInboxClientNotifierInf pharmInboxClientNotifier;
	
	private final static String UNCHECKED = "unchecked";

	private static final String JAXB_CONTEXT_RX = "hk.org.ha.model.pms.vo.rx";
	
	private static final String MED_PROFILE_QUERY_HINT_FETCH_1 = "o.workstore";

	// Auto Dispensing	
	@In
	private AutoDispSchedulerInf autoDispScheduler;
		
	@In
	private AutoDispProcessorLocal autoDispProcessor;
	
	@In
	private AutoDispConfig autoDispConfig;
	
	@In
	private ApplicationProp applicationProp;
	
	public void receiveNewOrder(MedOrder remoteMedOrder) {
		MedOrder prevMedOrder = null;
		Boolean unvetRequired = false;

		remoteMedOrder.markHlaFlag(ALERT_HLA_DRUG_DISPLAYNAME.get(remoteMedOrder.getWorkstore().getHospital()));

		// set patient to MedOrder of discharge order for IPMOE display
		if (remoteMedOrder.isMpDischarge()) {
			Patient _patient = retreivePatientForDischargeOrder(remoteMedOrder);
			Patient patient = corpPmsServiceProxy.savePatient(_patient);
			remoteMedOrder.setPatient( orderManager.savePatient(patient) );
		}
		
		// insert MedOrder
		// (vet before flag is already updated in corp)
		MedOrder medOrder = orderManager.insertMedOrder(remoteMedOrder);

		// cancel previous order and associate ticket of previous order to new order
		if (medOrder.getPrevOrderNum() != null) {
			// lock the order by pessimistic lock
			orderManager.lockMedOrder(medOrder.getPrevOrderNum());
			
			prevMedOrder = orderManager.retrieveMedOrder(medOrder.getPrevOrderNum(), Arrays.asList(MedOrderStatus.SysDeleted));
			
			if (prevMedOrder == null) {
				// if isRevampPrevOrder is null, that means pms-corp already receive the previous order but pms-pms not yet receive the previous order
				if (remoteMedOrder.getIsRevampPrevOrder() == null || remoteMedOrder.getIsRevampPrevOrder()) {
					throw new UnsupportedOperationException("Unable to process outdated order message: Previous order " + medOrder.getPrevOrderNum() + " not found for new order " + medOrder.getOrderNum());
				} else {
					logger.warn("Legacy order message received: Previous order #0 is legacy order for new order #1", medOrder.getPrevOrderNum(), medOrder.getOrderNum());
				}
				
			} else {
				if (prevMedOrder.getVetDate() != null) {
					// vetted order
					DispOrder prevDispOrder = retreiveNonRefillDispOrder(prevMedOrder.getOrderNum());

					if (prevDispOrder.getDayEndStatus() != DispOrderDayEndStatus.None) {
						// if pass day-end, mark the MO to delete
						// otherwise cancel and unvet order in pms
						// (it is not possible that day-end is processing for an allow modify order, so no need to check 
						//  batchProcessingFlag)
						prevMedOrder.markDeleted(MedOrderStatus.Deleted, false);
						
						// note: no need to mark RefillSchedule to SysDeleted, since the mark MedOrder deleted call is not cascade
					} else {
						unvetRequired = true;
					}
					
					// associate dispensed ticket if not yet process day-end
					if (prevDispOrder.getDayEndStatus() == DispOrderDayEndStatus.None) {
						medOrder.setTicket(prevDispOrder.getTicket());
						prevMedOrder.setTicket(null);
					}
					
				} else {
					// unvet order
					prevMedOrder.markDeleted(MedOrderStatus.Deleted, false);

					// note: no need to mark RefillSchedule to SysDeleted, since the mark MedOrder deleted call is not cascade
					
					// associate previous associated ticket if exist
					if (prevMedOrder.getTicket() != null) {
						medOrder.setTicket(prevMedOrder.getTicket());
						prevMedOrder.setTicket(null);
					}
				}
				
				// set urgent flag for modified discharge order (only save in pms-pms)
				if ( medOrder.isMpDischarge() && VETTING_MODIFIEDMOITEM_PRINTING_URGENT.get(medOrder.getWorkstore(), Boolean.FALSE) && medOrder.getVetBeforeFlag() ) {
					medOrder.setUrgentFlag(Boolean.TRUE);
				}
			}			
		}		
		
		em.flush();
		em.clear();
		
		if (unvetRequired) {
			unvetManager.unvetMedOrder(prevMedOrder.getId(), true, false);
		}		

		// notify inbox to refresh if discharge new order is received
		if (medOrder.isMpDischarge()) {
			pharmInboxClientNotifier.dispatchUpdateEvent(medOrder);
		}
		
		
		Workstore workstore = medOrder.getWorkstore();		
		if (autoDispScheduler.configurated(workstore)) {

			autoDispScheduler.scheduleMedOrder(workstore, medOrder.getOrderNum());	
			
			String workstationKey = autoDispScheduler.checkOutWorkstation(workstore);
			if (workstationKey != null) {
				
				AutoDispWorkstation workstation = autoDispConfig.getAutoDispWorkstation(workstore, workstationKey);				
				
				autoDispProcessor.processMedOrder(
						Long.valueOf(workstation.getStartupDelayTime()),
						workstore, 
						workstationKey);
			}
		}
	}
	
	public void receiveUpdateOrder(OpTrxType opTrxType, MedOrder remoteMedOrder) {
		Boolean unvetRequired = false;

		// lock the order by pessimistic lock
		orderManager.lockMedOrder(remoteMedOrder.getOrderNum());
		
		// retrieve order
		MedOrder medOrder = orderManager.retrieveMedOrder(remoteMedOrder.getOrderNum(), Arrays.asList(MedOrderStatus.SysDeleted));
		if (medOrder == null) {
			// legacy order is already ignored in pms-corp, so all order must be revamp order
			throw new UnsupportedOperationException("Unable to process outdated order message: Update order " + remoteMedOrder.getOrderNum() + " (trx code = " + opTrxType.getDataValue() + ") not found");
		}
		
		if (opTrxType == OpTrxType.OpDeleteMoeOrder) {
			if (medOrder.getVetDate() != null) {
				// vetted order
				DispOrder dispOrder = retreiveNonRefillDispOrder(medOrder.getOrderNum());

				if (dispOrder.getDayEndStatus() != DispOrderDayEndStatus.None) {
					// if pass day-end, mark the MO to delete
					// otherwise cancel and unvet order in pms
					// (it is not possible that day-end is processing for an allow modify order, so no need to 
					//  check batchProcessingFlag)
					medOrder.markDeleted(MedOrderStatus.Deleted, false);
					
					// note: no need to mark RefillSchedule to SysDeleted, since the mark MedOrder deleted call is not cascade
				} else {
					unvetRequired = true;
				}
			} else {
				// unvet order
				medOrder.markDeleted(MedOrderStatus.Deleted, false);
				
				// note: no need to mark RefillSchedule to SysDeleted, since the mark MedOrder deleted call is not cascade
			}
			
		} else if (opTrxType == OpTrxType.OpChangePrescType) {
			if (medOrder.getStatus() != MedOrderStatus.Complete) {
				logger.warn("Unable to process outdated order message: Update order #0 (trx code = #1) is received when order status is #2", medOrder.getOrderNum(), opTrxType.getDataValue(), medOrder.getStatus());
				return;
			}
			if ( medOrder.getPrescTypeUpdateDate() != null && remoteMedOrder.getCmsUpdateDate().compareTo(medOrder.getPrescTypeUpdateDate()) <= 0 ) {
				logger.warn("Unable to process outdated order message: Update order #0 (trx code = #1) is out of date, cms update date = #2, order presc type update date = #3", 
						medOrder.getOrderNum(), opTrxType.getDataValue(), remoteMedOrder.getCmsUpdateDate(), medOrder.getPrescTypeUpdateDate());
				return;
			}
			
			if ( medOrder.getPrescTypeUpdateDate() == null || remoteMedOrder.getCmsUpdateDate().compareTo(medOrder.getPrescTypeUpdateDate()) > 0 ) {
				medOrder.setPrescType(remoteMedOrder.getPrescType());
				medOrder.setPrescTypeUpdateDate(remoteMedOrder.getCmsUpdateDate());
			}
			
		} else if (opTrxType == OpTrxType.OpAddPostDispComment) {
			Map<Integer, MedOrderItem> medOrderItemHm = createMedOrderItemHash(medOrder.getMedOrderItemList());
			
			if (medOrder.getStatus() != MedOrderStatus.Complete) {
				logger.warn("Unable to process outdated order message: Update order #0 (trx code = #1) is received when order status is #2", medOrder.getOrderNum(), opTrxType.getDataValue(), medOrder.getStatus());
				return;
			}
			
			for (MedOrderItem remoteMedOrderItem : remoteMedOrder.getMedOrderItemList()) {
				if (medOrderItemHm.containsKey(remoteMedOrderItem.getItemNum())) {
					MedOrderItem tempMedOrderItem = medOrderItemHm.get(remoteMedOrderItem.getItemNum());
					
					if ( tempMedOrderItem.getCommentDate() == null || (remoteMedOrderItem.getCommentDate() != null && 
							remoteMedOrderItem.getCommentDate().compareTo(tempMedOrderItem.getCommentDate()) > 0) ) {
						medOrderItemHm.get(remoteMedOrderItem.getItemNum()).setCommentText(remoteMedOrderItem.getCommentText());
						medOrderItemHm.get(remoteMedOrderItem.getItemNum()).setCommentDate(remoteMedOrderItem.getCommentDate());
						medOrderItemHm.get(remoteMedOrderItem.getItemNum()).setCommentUser(remoteMedOrderItem.getCommentUser());
					} else {
						logger.warn("Unable to process outdated order message item: Update order #0 (trx code = #1) item num = #2 is out of date, message item comment date = #3, " + 
								"local item comment date = #4", medOrder.getOrderNum(), opTrxType.getDataValue(), tempMedOrderItem.getItemNum(), remoteMedOrderItem.getCommentDate(), tempMedOrderItem.getCommentDate());
					}
				}
			}
			
			boolean commentExist = false;
			for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
				if (medOrderItem.getCommentText() != null) {
					commentExist = true;
					break;
				}
			}
			medOrder.setCommentFlag(commentExist); 
		
			if ( medOrder.getPrescTypeUpdateDate() == null || remoteMedOrder.getCmsUpdateDate().compareTo(medOrder.getPrescTypeUpdateDate()) > 0 ) {
				medOrder.setPrescType(remoteMedOrder.getPrescType());
				medOrder.setPrescTypeUpdateDate(remoteMedOrder.getCmsUpdateDate());
			}
			
		} else if (opTrxType == OpTrxType.OpResumeAdvanceOrder) {
			if (medOrder.getStatus() != MedOrderStatus.MoeSuspended) {
				logger.warn("Unable to process outdated order message: Update order #0 (trx code = #1) is received when order status is #2", medOrder.getOrderNum(), opTrxType.getDataValue(), medOrder.getStatus());
				return;
			}
			
			medOrder.setStatus(MedOrderStatus.Outstanding);
		}
		
		if ( remoteMedOrder.getCmsUpdateDate().compareTo(medOrder.getCmsUpdateDate()) > 0 ) {
			medOrder.setCmsUpdateDate(remoteMedOrder.getCmsUpdateDate());
		}
		
		em.flush();
		em.clear();
		
		if (unvetRequired) {
			unvetManager.unvetMedOrder(medOrder.getId(), true, false);
		}
		
		// notify inbox to refresh if discharge update order DR6 and RO6 is received
		if ( medOrder.isMpDischarge() && (opTrxType == OpTrxType.OpDeleteMoeOrder ||
				opTrxType == OpTrxType.OpResumeAdvanceOrder) ) {
			pharmInboxClientNotifier.dispatchUpdateEvent(medOrder);
		}
	}

	public void receiveConfirmRemark(MedOrder remoteMedOrder) {
		String orderNum = remoteMedOrder.getOrderNum();
		
		// lock the order by pessimistic lock
		orderManager.lockMedOrder(orderNum);
		
		// retrieve order
		MedOrder medOrder = orderManager.retrieveMedOrder(orderNum, Arrays.asList(MedOrderStatus.SysDeleted));
		if (medOrder == null) {
			// legacy order is already ignored in pms-corp, so all order must be revamp order
			throw new UnsupportedOperationException("Unable to process outdated order message: Confirm remark " + remoteMedOrder.getOrderNum() + " not found");
		}

		if (remoteMedOrder.getStatus() == MedOrderStatus.Deleted) {
			// confirm order delete remark
			if (medOrder.getStatus() != MedOrderStatus.Outstanding && medOrder.getStatus() != MedOrderStatus.PreVet &&
					medOrder.getStatus() != MedOrderStatus.Withhold) {
				logger.warn("Unable to process outdated order message: Confirm remark (order) #0 is received when order status is #1", medOrder.getOrderNum(), medOrder.getStatus());
				return;
			}
			if (medOrder.getRemarkCreateDate() == null) {
				logger.warn("Unable to process outdated order message: Confirm remark #0 is received but order has no delete order remark", medOrder.getOrderNum());
				return;
			}
			
			medOrder.setStatus(remoteMedOrder.getStatus());
			
			medOrder.setRemarkConfirmDate(remoteMedOrder.getRemarkConfirmDate());
			medOrder.setRemarkConfirmUser(remoteMedOrder.getRemarkConfirmUser());
			medOrder.setRemarkStatus(RemarkStatus.Confirm);
			
		} else {
			// confirm item delete remark
			
			if (medOrder.getStatus() != MedOrderStatus.Complete) {
				logger.warn("Unable to process outdated order message: Confirm remark (item) #0 is received when order status is #1", medOrder.getOrderNum(), medOrder.getStatus());
				return;
			}
			
			medOrder.loadChild();
			orderManager.loadOrderList(medOrder);		

			// get local medOrderItem map
			Map<Integer, MedOrderItem> medOrderItemHm = createMedOrderItemHash(medOrder.getMedOrderItemList());
			
			// get remote MedOrderItem list (exclude dummy MedOrderItem which is included in CMS chargeDtl only but not exists in ordDtl)
			List<MedOrderItem> remoteMedOrderItemList = new ArrayList<MedOrderItem>();
			for (MedOrderItem remoteMedOrderItem : remoteMedOrder.getMedOrderItemList()) {
				if (remoteMedOrderItem.getOrgItemNum() != null) {
					remoteMedOrderItemList.add(remoteMedOrderItem);
				}
			}
			
			// get dummy remote MedOrderItem list which is included in CMS chargeDtl only but not exists in ordDtl
			List<MedOrderItem> remoteChargeDtlItemList = new ArrayList<MedOrderItem>();
			for (MedOrderItem remoteMedOrderItem : remoteMedOrder.getMedOrderItemList()) {
				if (remoteMedOrderItem.getOrgItemNum() == null) {
					remoteChargeDtlItemList.add(remoteMedOrderItem);
				}
			}
			
			// group by org item num
			Map<Integer, List<MedOrderItem>> remoteMedOrderItemGroupHm = new HashMap<Integer, List<MedOrderItem>>();
			for (MedOrderItem remoteMedOrderItem : remoteMedOrderItemList) {
				List<MedOrderItem> remoteMedOrderItemListByOrgItemNum = null;
				
				if (remoteMedOrderItemGroupHm.containsKey(remoteMedOrderItem.getOrgItemNum())) {
					remoteMedOrderItemListByOrgItemNum = remoteMedOrderItemGroupHm.get(remoteMedOrderItem.getOrgItemNum());
				} else {
					remoteMedOrderItemListByOrgItemNum = new ArrayList<MedOrderItem>();
					remoteMedOrderItemGroupHm.put(remoteMedOrderItem.getOrgItemNum(), remoteMedOrderItemListByOrgItemNum);
				}
				
				remoteMedOrderItemListByOrgItemNum.add(remoteMedOrderItem);
			}
				
			// declare list to store all MOI which is not in current order (to update their status later) 
			List<Integer> otherMedOrderItemNumList = new ArrayList<Integer>();
			
			// declare list to store all confirm delete MOI (to remove their DDIM linkage later) 
			List<Long> deleteItemNumList = new ArrayList<Long>();
			
			// declare list to store all confirm original (L) item for confirm modify and delete (to remove FM later) 
			List<MedOrderItem> remoteConfirmOriginalItemList = new ArrayList<MedOrderItem>();
			
			// update pharm remark confirm (L) item of current order
			for (Map.Entry<Integer, List<MedOrderItem>> entry : remoteMedOrderItemGroupHm.entrySet()) {
				
				Integer orgItemNum = entry.getKey();
				List<MedOrderItem> remoteMedOrderItemListByOrgItemNum = entry.getValue();
				
				// search for the item to confirm
				MedOrderItem remoteConfirmMedOrderItem = null;
				MedOrderItem localConfirmMedOrderItem = null;
				for (MedOrderItem tempMedOrderItem : remoteMedOrderItemListByOrgItemNum) {
					if ( medOrderItemHm.containsKey(tempMedOrderItem.getItemNum()) ) {
						remoteConfirmMedOrderItem = tempMedOrderItem;
						localConfirmMedOrderItem = medOrderItemHm.get(tempMedOrderItem.getItemNum());
						break;
					}
				}
				
				if (remoteConfirmMedOrderItem == null && localConfirmMedOrderItem == null) {
					logger.warn("Unable to process outdated order message item: Confirm remark #0 is received but the item is removed " + 
							"from order, org item num = #1", orderNum, orgItemNum);
					continue;
				}
				
				switch (localConfirmMedOrderItem.getRemarkItemStatus()) {
					case UnconfirmedAdd:
					case UnconfirmedModify:
						if (remoteConfirmMedOrderItem.getRemarkItemStatus() != RemarkItemStatus.Confirmed) {
							logger.warn("Unable to process outdated order message item: Confirm remark #0 is received when local item is unconfirmed add/modify " + 
									"but the message item remark status is not Confirmed, item num = #1, org item num = #2, message item remark status = #3, " +
									"local item remark status = #4", orderNum, localConfirmMedOrderItem.getItemNum(), orgItemNum, 
									remoteConfirmMedOrderItem.getRemarkItemStatus(), localConfirmMedOrderItem.getRemarkItemStatus());
							continue;
						}
						
						// ignore item if there is no confirm original (L) item
						MedOrderItem remoteConfirmOriginalMedOrderItem = null;
						for (MedOrderItem tempMedOrderItem : remoteMedOrderItemListByOrgItemNum) {
							if (tempMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.ConfirmedOriginal) {
								remoteConfirmOriginalMedOrderItem = tempMedOrderItem;
								break;
							}
						}
						if (remoteConfirmOriginalMedOrderItem == null) {
							logger.warn("Unable to process outdated order message item: Confirm remark #0 is received when local item is unconfirmed add/modify " +
									"but there is no original item (with item remark status L) in message, item num = #1, org item num = #2, message item remark status = #3, " +
									"local item remark status = #4", orderNum, localConfirmMedOrderItem.getItemNum(), orgItemNum, 
									remoteConfirmMedOrderItem.getRemarkItemStatus(), localConfirmMedOrderItem.getRemarkItemStatus());
							continue;
						}
						
						// if pharm remark modify, add confirm original (L) item to list (to remove FM later)
						if (localConfirmMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedModify) {
							remoteConfirmOriginalItemList.add(remoteConfirmOriginalMedOrderItem);
						}
						
						// update remark info for confirmed (C) item
						localConfirmMedOrderItem.setRemarkConfirmDate(remoteConfirmOriginalMedOrderItem.getRemarkConfirmDate());
						localConfirmMedOrderItem.setRemarkConfirmUser(remoteConfirmOriginalMedOrderItem.getRemarkConfirmUser());
						localConfirmMedOrderItem.setRemarkItemStatus(remoteConfirmMedOrderItem.getRemarkItemStatus());
						
						// for confirm original (L) and confirm old original (H) item, add to list to update remark status in other order later
						for (MedOrderItem tempMedOrderItem : remoteMedOrderItemListByOrgItemNum) {
							if (tempMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.ConfirmedOriginal || 
									tempMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.ConfirmedOldOriginal) {
								otherMedOrderItemNumList.add(tempMedOrderItem.getItemNum()); 
							}
						}
						
						break;
						
					case UnconfirmedDelete:
						if (remoteConfirmMedOrderItem.getRemarkItemStatus() != RemarkItemStatus.ConfirmedOriginal) {
							logger.warn("Unable to process outdated order message item: Confirm remark #0 is received when local item is unconfirmed delete " + 
									"but the message item remark status is not L, item num = #1, org item num = #2, message item remark status = #3, " +
									"local item remark status = #4", orderNum, localConfirmMedOrderItem.getItemNum(), orgItemNum, 
									remoteConfirmMedOrderItem.getRemarkItemStatus(), localConfirmMedOrderItem.getRemarkItemStatus());
							continue;
						}

						// ignore item if there is confirm (C) item
						MedOrderItem remoteRemarkStautsConfirmMoi = null;
						for (MedOrderItem tempMedOrderItem : remoteMedOrderItemListByOrgItemNum) {
							if (tempMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.Confirmed) {
								remoteRemarkStautsConfirmMoi = tempMedOrderItem;
								break;
							}
						}
						if (remoteRemarkStautsConfirmMoi != null) {
							logger.warn("Unable to process outdated order message item: Confirm remark #0 is received when local item is unconfirmed delete " +
									"but the message is confirm modify item (both remark status L/C exists in message), item num = #1, org item num = #2, message item remark status = #3, " +
									"local item remark status = #4", orderNum, localConfirmMedOrderItem.getItemNum(), orgItemNum, 
									remoteConfirmMedOrderItem.getRemarkItemStatus(), localConfirmMedOrderItem.getRemarkItemStatus());
							continue;
						}
						
						// update remark info for confirmed (L) item
						localConfirmMedOrderItem.setRemarkConfirmDate(remoteConfirmMedOrderItem.getRemarkConfirmDate());
						localConfirmMedOrderItem.setRemarkConfirmUser(remoteConfirmMedOrderItem.getRemarkConfirmUser());
						localConfirmMedOrderItem.setRemarkItemStatus(remoteConfirmMedOrderItem.getRemarkItemStatus());

						// update MOI status for confirmed (L) item
						localConfirmMedOrderItem.setStatus(MedOrderItemStatus.SysDeleted);
						
						// add to pharm remark delete MOI list (to remove DDIM linkage later)
						deleteItemNumList.add(Long.valueOf(remoteConfirmMedOrderItem.getItemNum().intValue()));

						// add pharm remark delete (L) item to list (to remove FM later)
						remoteConfirmOriginalItemList.add(remoteConfirmMedOrderItem);
						
						// for confirm old original (H) item, add to list to update remark status in other order later
						for (MedOrderItem tempMedOrderItem : remoteMedOrderItemListByOrgItemNum) {
							if (tempMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.ConfirmedOldOriginal) {
								otherMedOrderItemNumList.add(tempMedOrderItem.getItemNum()); 
							}
						}
						
						break;
						
					default:
						if (localConfirmMedOrderItem != null && remoteConfirmMedOrderItem != null) {
							logger.warn("Unable to process outdated order message item: Confirm remark #0 is received but local item remark status " + 
									"is not unconfirmed add/modify/delete, item num = #1, org item num = #2, message item remark status = #3, " +
									"local item remark status = #4", orderNum, localConfirmMedOrderItem.getItemNum(), orgItemNum, 
									remoteConfirmMedOrderItem.getRemarkItemStatus(), localConfirmMedOrderItem.getRemarkItemStatus());
						}
						continue;
				}
			}
			
			
			// update pharm remark confirm original (L) and confirm old original (H) item of other order
			List<MedOrderItem> otherMedOrderItemList = orderManager.retrieveMedOrderItemByItemNum(orderNum, otherMedOrderItemNumList);
			Map<Integer, MedOrderItem> remoteMedOrderItemHm = createMedOrderItemHash(remoteMedOrderItemList);
			for (MedOrderItem otherMedOrderItem : otherMedOrderItemList) {
				if (remoteMedOrderItemHm.containsKey(otherMedOrderItem.getItemNum())) {
					otherMedOrderItem.setRemarkItemStatus(remoteMedOrderItemHm.get(otherMedOrderItem.getItemNum()).getRemarkItemStatus());
				}
			}
			

			// update cmsConfirmChargeFlag and cmsConfirmUnitOfCharge of MedOrderItem and CapdItem for all items included in Confirm Remark
			// (note: the 2 confirm charge variables are used to sent back to CMS, and resume charge variables in unvet)
			// (note: there are 3 kinds of items that need to update confirm charge variables:
			//        1. pharmacy remark items
			//        2. other items with same displayname, form, salt with remark items, and CMS send both ordDtl and chargeDtl for that item
			//        3. other items with same displayname, form, salt with remark items, and CMS send only chargeDtl for that item)
			// -----------------------
			// update confirm charge variables of type 1 and type 2 item
			List<Integer> remoteMedOrderItemNumList = new ArrayList<Integer>(); 
			for (MedOrderItem remoteMedOrderItem : remoteMedOrderItemList) {
				remoteMedOrderItemNumList.add(remoteMedOrderItem.getItemNum());
			}
			List<MedOrderItem> localMedOrderItemList = orderManager.retrieveMedOrderItemByItemNum(orderNum, remoteMedOrderItemNumList);
			for (MedOrderItem localMedOrderItem : localMedOrderItemList) {
				if (remoteMedOrderItemHm.containsKey(localMedOrderItem.getItemNum())) {
					MedOrderItem remoteMedOrderItem = remoteMedOrderItemHm.get(localMedOrderItem.getItemNum());
					
					localMedOrderItem.setCmsConfirmChargeFlag(remoteMedOrderItem.getCmsConfirmChargeFlag());
					localMedOrderItem.setCmsConfirmUnitOfCharge(remoteMedOrderItem.getCmsConfirmUnitOfCharge());
					
					if (localMedOrderItem.getRxItemType() == RxItemType.Capd) { 
						for (int i=0; i<remoteMedOrderItem.getCapdRxDrug().getCapdItemList().size(); i++) {
							CapdItem remoteCapdItem = remoteMedOrderItem.getCapdRxDrug().getCapdItemList().get(i);
							CapdItem localCapdItem = localMedOrderItem.getCapdRxDrug().getCapdItemList().get(i);
							localCapdItem.setCmsConfirmChargeFlag(remoteCapdItem.getCmsConfirmChargeFlag());
							localCapdItem.setCmsConfirmUnitOfCharge(remoteCapdItem.getCmsConfirmUnitOfCharge());
						}
					}
				}
			}

			// update confirm charge variables of type 3 item
			List<Integer> remoteChargeDtlItemNumList = new ArrayList<Integer>();
			Map<Integer, MedOrderItem> remoteChargeDtlItemHm = new HashMap<Integer, MedOrderItem>();
			for (MedOrderItem remoteChargeDtlItem : remoteChargeDtlItemList) {
				remoteChargeDtlItemNumList.add(remoteChargeDtlItem.getItemNum());
				remoteChargeDtlItemHm.put(remoteChargeDtlItem.getItemNum(), remoteChargeDtlItem);
			}			
			List<MedOrderItem> localChargeDtlItemList = orderManager.retrieveMedOrderItemByItemNum(orderNum, remoteChargeDtlItemNumList);
			for (MedOrderItem localChargeDtlItem : localChargeDtlItemList) {
				if (remoteChargeDtlItemHm.containsKey(localChargeDtlItem.getItemNum())) {
					MedOrderItem remoteChargeDtlItem = remoteChargeDtlItemHm.get(localChargeDtlItem.getItemNum());
					
					localChargeDtlItem.setCmsConfirmChargeFlag(remoteChargeDtlItem.getCmsConfirmChargeFlag());
					localChargeDtlItem.setCmsConfirmUnitOfCharge(remoteChargeDtlItem.getCmsConfirmUnitOfCharge());
				}
			}			
			
			
			// delete ddim for confirmed delete (L) item
			for (Long deleteItemNum : deleteItemNumList) {
				for (MedOrderItem moi : medOrder.getMedOrderItemList()) {
					Set<MedOrderItemAlert> deleteMoiAlertSet = new HashSet<MedOrderItemAlert>();
					for (MedOrderItemAlert moiAlert : moi.getDdiAlertList()) {
						AlertDdim alertDdim = (AlertDdim) moiAlert.getAlert();
						Long tempOrderNum = Long.valueOf(medOrder.getOrderNum().substring(3));
						if ((deleteItemNum.equals(alertDdim.getItemNum1()) && tempOrderNum.equals(alertDdim.getOrdNo1()))
						 || (deleteItemNum.equals(alertDdim.getItemNum2()) && tempOrderNum.equals(alertDdim.getOrdNo2()))) {
							deleteMoiAlertSet.add(moiAlert);
						}
					}
					
					moi.getMedOrderItemAlertList().removeAll(deleteMoiAlertSet);
					moi.markAlertFlag();
					
					if (deleteMoiAlertSet.size() > 0 && medOrder.isMpDischarge()) {
						// update drug order html for discharge order line display
						JaxbWrapper<RxItem> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
						String rxItemXml = rxJaxbWrapper.marshall(moi.getRxItem());
						
						ConvertParam convertParam = new ConvertParam();
						convertParam.setOrderType("D");
						//Format for discharge only
						List<String> rxItemXmlList = dmsPmsServiceProxy.formatDrugOrderXml( new ArrayList<String>(Arrays.asList(rxItemXml)), convertParam );
						
						RxItem rxItem = rxJaxbWrapper.unmarshall(rxItemXmlList.get(0));
						
						if (rxItem == null) {
							throw new UnsupportedOperationException("Cannot unmarshall updated RxItem after calling DMS formatting engine, orderNum = " + orderNum + ", itemNum = " + moi.getItemNum());
						}
						
						rxItem.buildTlf();
						
						moi.setRxItem(rxItem);
					}
				}
			}
			
			
			// get local confirm original item from remote confirm original item
			List<Integer> remoteConfirmOriginalItemNumList = new ArrayList<Integer>();
			for (MedOrderItem tempItem : remoteConfirmOriginalItemList) {
				remoteConfirmOriginalItemNumList.add(tempItem.getItemNum());
			}
			List<MedOrderItem> localConfirmOriginalItemList = orderManager.retrieveMedOrderItemByItemNum(orderNum, remoteConfirmOriginalItemNumList);
			
			// delete unused FM for confirmed delete and confirmed replace item, and set prev FM ind for those items			
			Set<MedOrderFm> checkRemoveMedOrderFmSet = new HashSet<MedOrderFm>();
			for (MedOrderItem remoteConfirmOriginalItem : remoteConfirmOriginalItemList) {
				remoteConfirmOriginalItem.loadDmInfo();
				List<MedOrderFm> medOrderFmList = retrieveLocalMedOrderFmListByMedOrderItem(remoteConfirmOriginalItem, medOrder.getMedOrderFmList());
				if (medOrderFmList.size() > 0) {
					checkRemoveMedOrderFmSet.addAll(medOrderFmList);					
					for (MedOrderItem localConfirmOriginalItem : localConfirmOriginalItemList) {
						if (remoteConfirmOriginalItem.getItemNum().equals(localConfirmOriginalItem.getItemNum())) {
							// if the item has any FM, mark the flag to show corresponding FM indicator in pharmacy remark old item part
							localConfirmOriginalItem.setConfirmPrevFmIndFlag(Boolean.TRUE);
						}
					}
				}
			}
			if ( ! checkRemoveMedOrderFmSet.isEmpty()) {
				Map<Long, MedOrderFm> usedMedOrderFmMap = new HashMap<Long, MedOrderFm>();
				for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
					if (medOrderItem.getStatus() != MedOrderItemStatus.SysDeleted) {
						medOrderItem.loadDmInfo();
						List<MedOrderFm> medOrderFmList = retrieveLocalMedOrderFmListByMedOrderItem(medOrderItem, medOrder.getMedOrderFmList());
						for (MedOrderFm medOrderFm : medOrderFmList) {
							if ( ! usedMedOrderFmMap.containsKey(medOrderFm.getId())) {
								usedMedOrderFmMap.put(medOrderFm.getId(), medOrderFm);
							}
						}
					}
				}
				List<MedOrderFm> unusedMedOrderFmList = new ArrayList<MedOrderFm>();
				for (MedOrderFm medOrderFm : checkRemoveMedOrderFmSet) {
					if ( ! usedMedOrderFmMap.containsKey(medOrderFm.getId())) {
						unusedMedOrderFmList.add(medOrderFm);
					}
				}
				medOrder.getMedOrderFmList().removeAll(unusedMedOrderFmList);
			}
			
			
			// update MedOrder info
			boolean allConfirm = true;
			medOrder = orderManager.retrieveMedOrder(orderNum, Arrays.asList(MedOrderStatus.SysDeleted));
			
			medOrder.setItemRemarkConfirmedFlag(Boolean.TRUE);	// set to status that at least one item has been confirmed
			
			for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
				if (medOrderItem.getRemarkStatus() == RemarkStatus.Unconfirm) {
					allConfirm = false;
					break;
				}
			}
			if (allConfirm) {
				// update remark status
				medOrder.setRemarkStatus(RemarkStatus.Confirm);
			}
			
			if (remoteMedOrder.getMaxItemNum() > medOrder.getMaxItemNum()) {
				// update maximum item number for remark item add
				// (does not always replace maximum item number because only confirmed item is included in confirm remark) 
				medOrder.setMaxItemNum(remoteMedOrder.getMaxItemNum());
			}
		}
		
		if ( remoteMedOrder.getCmsUpdateDate().compareTo(medOrder.getCmsUpdateDate()) > 0 ) {
			medOrder.setCmsUpdateDate(remoteMedOrder.getCmsUpdateDate());
		}
		
		em.flush();
	}

	public void receiveDiscontinue(OpTrxType opTrxType, MedOrder remoteMedOrder) {
		String orderNum = remoteMedOrder.getOrderNum();

		// lock the order by pessimistic lock
		orderManager.lockMedOrder(orderNum);
		
		// retrieve order
		MedOrder medOrder = orderManager.retrieveMedOrder(orderNum, Arrays.asList(MedOrderStatus.SysDeleted));
		if (medOrder == null) {
			throw new UnsupportedOperationException("Unable to process outdated order message: Discontinue order " + remoteMedOrder.getOrderNum() + " (trx code = " + opTrxType.getDataValue() + ") not found");
		}

		
		// no need to check cluster enabled, it is already checked in CORP
		
		MedOrderItem remoteMedOrderItem = remoteMedOrder.getMedOrderItemList().get(0);
		MedOrderItem medOrderItem = medOrder.getMedOrderItemByItemNum(remoteMedOrderItem.getItemNum());
		
		if (medOrderItem == null) {
			logger.warn("Unable to process outdated order message item: Discontinue order #0 (trx code = #1) item num = #2 item not found, discontinue date = #3", 
					medOrder.getOrderNum(), opTrxType.getDataValue(), remoteMedOrderItem.getItemNum(), remoteMedOrderItem.getDiscontinueDate());
			return;
		}

		if (medOrderItem.getDiscontinueStatus() == DiscontinueStatus.Confirmed) {
			logger.info("Unable to process outdated order message item: Discontinue order #0 (trx code = #1) item num = #2 is already confirmed discontinue, discontinue date = #3", 
					medOrder.getOrderNum(), opTrxType.getDataValue(), remoteMedOrderItem.getItemNum(), remoteMedOrderItem.getDiscontinueDate());
			return;
		}
		
		RxItem rxItem = medOrderItem.getRxItem();
		
		if (rxItem.getDiscontinueDate() != null && remoteMedOrderItem.getDiscontinueDate().compareTo(rxItem.getDiscontinueDate()) <= 0) {
			logger.warn("Unable to process outdated order message item: Discontinue order #0 (trx code = #1) item num = #2 is out of date, message discontinue date = #3, local discontinue date = #4", 
					medOrder.getOrderNum(), opTrxType.getDataValue(), remoteMedOrderItem.getItemNum(), remoteMedOrderItem.getDiscontinueDate(), rxItem.getDiscontinueDate());
			return;
		}

		
		medOrderItem.setDiscontinueStatus(remoteMedOrderItem.getDiscontinueStatus());
		
		if (opTrxType == OpTrxType.OpDiscontinue) {
			rxItem.setDiscontinueFlag(Boolean.TRUE);

			rxItem.setDiscontinueReason( remoteMedOrderItem.getDiscontinueReason() );
			rxItem.setDiscontinueHospCode( remoteMedOrderItem.getDiscontinueHospCode() );
			
		} else if (opTrxType == OpTrxType.OpCancelDiscontinue) {
			rxItem.setDiscontinueFlag(Boolean.FALSE);
			
			rxItem.setDiscontinueReason(null);
			rxItem.setDiscontinueHospCode(null);
		}
		rxItem.setDiscontinueDate( remoteMedOrderItem.getDiscontinueDate() );
		
		em.flush();
	}

	@SuppressWarnings("unchecked")
	private List<MedProfileOrder> multiplyMedProfileOrderForPasMessage(MedProfileOrder sourceMedProfileOrder, boolean searchByCaseNum, boolean searchByPatKey) {
		if ( ! searchByPatKey && ! searchByCaseNum) {
			throw new UnsupportedOperationException("No key to search MedProfile for PAS message");
		}
		
		List<MedProfileOrder> medProfileOrderList = new ArrayList<MedProfileOrder>();
		
    	StringBuilder sql = new StringBuilder();
		
    	sql.append("select o from MedProfile o"); // 20171023 index check : MedProfile.medCase MedProfile.patKey : FK_MED_PROFILE_01 FK_MED_PROFILE_02
    	sql.append(" where o.patHospCode = :patHospCode");
    	if (searchByCaseNum && searchByPatKey) {
        	sql.append(" and (o.medCase.caseNum = :caseNum");
        	sql.append(" or (o.patient.patKey = :patKey and o.type = :type))");
    	} else if (searchByCaseNum) {
        	sql.append(" and o.medCase.caseNum = :caseNum");
    	} else if (searchByPatKey) {
        	sql.append(" and (o.patient.patKey = :patKey and o.type = :type)");
    	}
    	
		Query query = em.createQuery(sql.toString());

		query.setParameter("patHospCode", sourceMedProfileOrder.getPatHospCode());
    	if (searchByCaseNum && searchByPatKey) {
			query.setParameter("caseNum", sourceMedProfileOrder.getCaseNum());
			query.setParameter("patKey", sourceMedProfileOrder.getPatKey());
			query.setParameter("type", MedProfileType.Chemo);
    	} else if (searchByCaseNum) {
			query.setParameter("caseNum", sourceMedProfileOrder.getCaseNum());
		} else if (searchByPatKey) {
			query.setParameter("patKey", sourceMedProfileOrder.getPatKey());
			query.setParameter("type", MedProfileType.Chemo);
		}
		query.setHint(QueryHints.FETCH, MED_PROFILE_QUERY_HINT_FETCH_1);
		query.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock);
		
		List<MedProfile> medProfileList = query.getResultList();
		
		for (MedProfile medProfile : medProfileList) {
			
			MedProfileOrder destMedProfileOrder = new MedProfileOrder();
			BeanUtils.copyProperties(sourceMedProfileOrder, destMedProfileOrder);	
			
			destMedProfileOrder.setOrderNum(medProfile.getOrderNum());
			destMedProfileOrder.setWorkstore(medProfile.getWorkstore());
			
			medProfileOrderList.add(destMedProfileOrder);
		}
		
		if (medProfileList.isEmpty()) {
			logger.info("No med profile found for PAS message: searchByCaseNum=#0, caseNum=#1, searchByPatKey=#2, patKey=#3", searchByCaseNum, sourceMedProfileOrder.getCaseNum(), searchByPatKey, sourceMedProfileOrder.getPatKey());
		}
		
		return medProfileOrderList;
	}
	
	private boolean isPasMessage(MedProfileOrderType type) {
		return type == MedProfileOrderType.CancelDischarge || 
		type == MedProfileOrderType.CancelTransfer ||
		type == MedProfileOrderType.WardTransfer ||
		type == MedProfileOrderType.Discharge ||
		type == MedProfileOrderType.CancelAdmission ||
		type == MedProfileOrderType.PatientRegistrationMp;
	}
	
	@Override
	public void receiveMedProfileOrder(MedProfileOrder medProfileOrder) {
		MedProfileOrderType medProfileOrderType = MedProfileOrderType.dataValueOf(medProfileOrder.getTypeString());
		
		if (medProfileOrderType == null) {
			logger.warn("Ignore IPMOE message because it is not supported by this pms-pms version (type = #0, orderNum = #1, caseNum = #2)", 
					medProfileOrder.getTypeString(), medProfileOrder.getOrderNum(), medProfileOrder.getCaseNum());
			return;
		} else {
			// convert type enum from typeString since it is cleared in corp for compatibility issue of new message type
			medProfileOrder.setType(medProfileOrderType);
		}
		
		if (medProfileOrder.getType() == MedProfileOrderType.PatientRegistrationMp) {
			receiveMedProfileOrderList(multiplyMedProfileOrderForPasMessage(medProfileOrder, false, true));
		} else if (medProfileOrder.getType() == MedProfileOrderType.CancelDischarge) {
			receiveMedProfileOrderList(multiplyMedProfileOrderForPasMessage(medProfileOrder, true, true));
		} else if (isPasMessage(medProfileOrder.getType())) {
			receiveMedProfileOrderList(multiplyMedProfileOrderForPasMessage(medProfileOrder, true, false));
		} else {
			receiveMedProfileOrderList(Arrays.asList(medProfileOrder));
		}
	}
	
	private void receiveMedProfileOrderList(List<MedProfileOrder> medProfileOrderList) {
			
		for (MedProfileOrder medProfileOrder : medProfileOrderList) {			
			if (MedProfileOrderType.AddAdmin_ModifyAdmin.contains(medProfileOrder.getType())) {
				medProfileOrder.setMedProfileMoItemList(null);
			}
			
			medProfileOrderManager.saveMedProfileOrder(medProfileOrder);
			
			try {
				if (!MedProfileOrderType.AddAdmin_ModifyAdmin.contains(medProfileOrder.getType()) &&
						medProfileOrderScheduler.scheduleMedProfileOrder(medProfileOrder.getOrderNum()) &&
						medProfileOrderScheduler.shouldStartRunning()) {
					
					logger.info("Processing MedProfileOrder scheduled");
					medProfileOrderProcessor.processMedProfileOrderByScheduler(Long.valueOf(1000L), medProfileOrderScheduler.getNextOrderNumToRun());
				}
			} catch (Exception ex) {
				logger.error(ex, ex);
			}
		}
			
	}
	
	public void receiveChemoOrder(MedOrder medOrder, List<MedProfileOrder> medProfileOrderList) {
		if (medProfileOrderList != null) {
			for (MedProfileOrder medProfileOrder : medProfileOrderList) {
				receiveMedProfileOrder(medProfileOrder);
			}
		}
	}

	@SuppressWarnings(UNCHECKED)
	private Patient retreivePatientForDischargeOrder(MedOrder medOrder) {
		// note: no need to consider friend club hospital for discharge order
		List<MedProfile> medProfileList = (List<MedProfile>) em.createQuery(
				"select o from MedProfile o " + // 20131213 index check : MedProfile.medCase : FK_MED_PROFILE_02
				" where o.patHospCode = :patHospCode" +
				" and o.medCase.caseNum = :caseNum")
				.setParameter("patHospCode", medOrder.getPatHospCode())
				.setParameter("caseNum", medOrder.getMedCase().getCaseNum())
				.getResultList();

		Patient patient = null;
		
		if (medProfileList.isEmpty()) {
			if (PATIENT_PAS_PATIENT_ENABLED.get(medOrder.getWorkstore(), false)) {
				try {
					patient = pasServiceWrapper.retrievePasPatientByCaseNum(medOrder.getPatHospCode(), medOrder.getMedCase().getCaseNum(), false);
				} catch (PasException e) {
					logger.debug("No patient infomation is found by case: #0" , medOrder.getMedCase().getCaseNum());
				} catch (UnreachableException e) {
					logger.debug("Hkpmi service is down");
				}
			}
		} else {
			Collections.sort(medProfileList, new MedProfileComparator());
			
			patient = medProfileList.get(medProfileList.size()-1).getPatient();
		}
		
		return patient;
	}
	
	@SuppressWarnings(UNCHECKED)
	private DispOrder retreiveNonRefillDispOrder(String orderNum) {
		List<DispOrder> dispOrderList = (List<DispOrder>) em.createQuery(
				"select o from DispOrder o " + // 20120228 index check : MedOrder.orderNum : I_MED_ORDER_01
				" where o.pharmOrder.medOrder.orderNum = :orderNum" +
				" and o.status not in :status" +
				" and o.refillFlag = :refillFlag")
				.setParameter("orderNum", orderNum)
				.setParameter("status", DispOrderStatus.Deleted_SysDeleted)
				.setParameter("refillFlag", Boolean.FALSE)
				.getResultList();

		if (dispOrderList.isEmpty()) {
			throw new UnsupportedOperationException("Vetted order with no non-refill DispOrder, orderNum = " + orderNum);
		} else {
			return dispOrderList.get(0);
		}
	}
	
	private Map<Integer, MedOrderItem> createMedOrderItemHash(List<MedOrderItem> medOrderItemList) {
		Map<Integer, MedOrderItem> medOrderItemHm = new HashMap<Integer, MedOrderItem>();
		
		for (MedOrderItem medOrderItem : medOrderItemList) {
			medOrderItemHm.put(medOrderItem.getItemNum(), medOrderItem);
		}
		
		return medOrderItemHm;
	}	
	
	public List<MedOrderFm> retrieveLocalMedOrderFmListByMedOrderItem(MedOrderItem medOrderItem, List<MedOrderFm> medOrderFmList) {
		List<MedOrderFm> foundMedOrderFmList = new ArrayList<MedOrderFm>();

		if (medOrderItem.getRxItemType() == RxItemType.Capd || FmStatus.dataValueOf(medOrderItem.getFmStatus()) == FmStatus.FreeTextEntryItem) {
			return foundMedOrderFmList;
		}
		
		MedOrderFm foundMedOrderFm = null;
		
		switch (medOrderItem.getRxItemType()) {
			case Oral:
			case Injection:
				foundMedOrderFm = retrieveLocalMedOrderFmByRxDrug(medOrderItem.getRxDrug(), medOrderFmList);
				if (foundMedOrderFm != null) {
					foundMedOrderFmList.add(foundMedOrderFm);
				}
				break;
				
			case Iv:
				for (IvAdditive ivAdditive : medOrderItem.getIvRxDrug().getIvAdditiveList()) {
					foundMedOrderFm = retrieveLocalMedOrderFmByRxDrug(ivAdditive, medOrderFmList);
					if (foundMedOrderFm != null) {
						foundMedOrderFmList.add(foundMedOrderFm);
					}
				}
				break;
		}
		
		return foundMedOrderFmList;
	}
	
	public MedOrderFm retrieveLocalMedOrderFmByRxDrug(RxDrug rxDrug, List<MedOrderFm> medOrderFmList) {
		for (MedOrderFm medOrderFm : medOrderFmList) {
			if (rxDrug.getStrengthCompulsory()) {
				if ( StringUtils.equals(medOrderFm.getDisplayName(), rxDrug.getDisplayName()) &&
						StringUtils.equals(medOrderFm.getFormCode(), rxDrug.getFormCode()) &&
						StringUtils.equals(medOrderFm.getSaltProperty(), rxDrug.getSaltProperty()) && 
						medOrderFm.getFmStatus() == FmStatus.dataValueOf(rxDrug.getFmStatus()) &&
						StringUtils.equals(medOrderFm.getItemCode(), rxDrug.getItemCode()) ) {
					return medOrderFm;
				}
			} else {
				if ( StringUtils.equals(medOrderFm.getDisplayName(), rxDrug.getDisplayName()) &&
						StringUtils.equals(medOrderFm.getFormCode(), rxDrug.getFormCode()) &&
						StringUtils.equals(medOrderFm.getSaltProperty(), rxDrug.getSaltProperty()) &&
						medOrderFm.getFmStatus() == FmStatus.dataValueOf(rxDrug.getFmStatus()) ) {
					return medOrderFm;
				}
			}
		}
		
		return null;
	}	
	
	public void updatePatient(List<MedOrder> corpMedOrderList) {
		for (MedOrder corpMedOrder: corpMedOrderList) {
			MedOrder clusterMedOrder = orderManager.retrieveMedOrder(corpMedOrder.getOrderNum());
			
			if (clusterMedOrder != null) {
				Patient patient = orderManager.savePatient(corpMedOrder.getPatient());
				clusterMedOrder.setPrevPatientId(corpMedOrder.getPrevPatientId());
				clusterMedOrder.setPatient(patient);
			}
		}
	}
	
	public void markProcessingFlagType(String clusterCode, List<Long> dispOrderIdList, Boolean batchProcessingFlag, DispOrderBatchProcessingType batchProcessingType, int chunkSize) {
		
		logger.info("markProcessingFlagType (pms-pms): start clusterCode = #0, batchProcessingFlag = #1, batchProcessingType = #2, dispOrderIdList size = #3 , chunkSize = #4", 
				clusterCode, 
				batchProcessingFlag,
				batchProcessingType,
				dispOrderIdList.size(),
				chunkSize);
		
		for (int start = 0, end = 0; start < dispOrderIdList.size(); start = end) {
			end = start + chunkSize;
			if (end > dispOrderIdList.size()) {
				end = dispOrderIdList.size();
			}
			
			DispOrderBatchProcessingType actualBatchProcessingType = null;
			if (batchProcessingFlag) {
				actualBatchProcessingType = batchProcessingType;
			} else {
				actualBatchProcessingType = DispOrderBatchProcessingType.None;
			}
			
			
			// generate sql query string
			String commonSqlStr = 
				"update DispOrder o" + // 20120228 index check : DispOrder.id : PK_DISP_ORDER
				" set o.batchProcessingFlag = :batchProcessingFlag," +
				" o.batchProcessingType = :batchProcessingType," +
				" o.version = o.version + 1" +
				" where o.id in :dispOrderIdList ";
			
			String skipCompletedSqlStr = ""; 
			switch (batchProcessingType) {
				// if the order is already completed batch process, skip marking PROCESSING
				// (handle the case when cluster receive batch complete message before marking processing message)
				case DayEnd:
					skipCompletedSqlStr = " and o.dayEndStatus <> :oldDayEndStatus ";
					break;
				case Uncollect:		// only non day end uncollect order is marked processing 
					skipCompletedSqlStr = " and (o.batchRemark not like :uncollectRemark or o.batchRemark is null) ";
					break;
				case Suspend:
				case Unsuspend:
					skipCompletedSqlStr = " and o.adminStatus <> o.externalAdminStatus ";
					break;
			}
			
			String finalSqlStr = commonSqlStr + skipCompletedSqlStr;
			
			// generate sql query
			Query commonQuery = em.createQuery(finalSqlStr)
									.setParameter("batchProcessingFlag", batchProcessingFlag)
									.setParameter("batchProcessingType", actualBatchProcessingType)
									.setParameter("dispOrderIdList", dispOrderIdList.subList(start, end));
			
			Query finalQuery = null;
			switch (batchProcessingType) {
			case DayEnd:
					finalQuery = commonQuery.setParameter("oldDayEndStatus", DispOrderDayEndStatus.Completed);
					break;
				case Uncollect:
					finalQuery = commonQuery.setParameter("uncollectRemark", "%" + applicationProp.getFcsUncollectTrxFile() + "%");
					break;
				default:
					finalQuery = commonQuery;
					break;
			}
			
			// execute query
			finalQuery.executeUpdate();

			
			logger.debug("markProcessingFlagType (pms-pms): set status batchProcessingFlag = #0, batchProcessingType = #1, from list index = #2 to index = #3", 
					batchProcessingFlag, batchProcessingType, start, end - 1);
		}
		
		em.clear();
	}

	public void updateDayEndCompleteInfoForDayEnd(String clusterCode, List<Long> dispOrderIdList, Date batchDate, String batchRemark, int chunkSize) {

		logger.info("updateDayEndCompleteInfoForDayEnd (pms-pms): start" +
				"clusterCode = #0," +
				" dispOrderIdList size = #1",
				clusterCode,
				dispOrderIdList.size());
		
		Map<Workstore, WorkloadStat> workloadStatMap = new HashMap<Workstore, WorkloadStat>();
		Map<StringArray, SuspendStat> suspendStatMap = new HashMap<StringArray, SuspendStat>();
		
		
		for (int start = 0, end = 0; start < dispOrderIdList.size(); start = end) {
			end = start + chunkSize;
			if (end > dispOrderIdList.size()) {
				end = dispOrderIdList.size();
			}
			
			List<Long> dispOrderIdSubList = dispOrderIdList.subList(start, end);
			
			// calculate statistic data
			List<DispOrder> dispOrderSubList = retrieveDispOrderList(dispOrderIdSubList);
			refreshCacheRefillSchedule(dispOrderIdSubList);
			
			for (DispOrder dispOrder : dispOrderSubList) {
				calculateWorkloadStatistic(workloadStatMap, dispOrder, batchDate);
				calculateSuspendStatistic(suspendStatMap, dispOrder, batchDate);
			}

			logger.info("updateDayEndCompleteInfoForDayEnd (pms-pms): calculate workload and suspend statistic " +
					", from list index = " + start + " to index = " + (end - 1)); 
			
			// update day end info
			em.createQuery(
					"update DispOrder o" + // 20120228 index check : DispOrder.id : PK_DISP_ORDER
					" set o.dayEndStatus = :dayEndStatus," +
					" o.batchProcessingFlag = :batchProcessingFlag," +
					" o.batchProcessingType = :batchProcessingType," +
					" o.dayEndBatchDate = :dayEndBatchDate," +
					" o.batchRemark = concat(o.batchRemark, :batchRemark)," +
					" o.updateDate = :currentDate," +
					" o.version = o.version + 1" +
					" where o.id in :dispOrderIdList ")
					.setParameter("dayEndStatus", DispOrderDayEndStatus.Completed)
					.setParameter("batchProcessingFlag", Boolean.FALSE)
					.setParameter("batchProcessingType", DispOrderBatchProcessingType.None)
					.setParameter("dayEndBatchDate", batchDate)
					.setParameter("batchRemark", batchRemark)
					.setParameter("currentDate", new Date())
					.setParameter("dispOrderIdList", dispOrderIdSubList)
					.executeUpdate();
			
			logger.info("updateDayEndCompleteInfoForDayEnd (pms-pms): update day end info " +
					"batchDate = " + batchDate + ", batchRemark = " + batchRemark + 
					", from list index = " + start + " to index = " + (end - 1)); 

			em.flush();
			em.clear();
		}
		
		// insert workload statistic
		for (Map.Entry<Workstore, WorkloadStat> entry: workloadStatMap.entrySet()) {
			em.persist(entry.getValue());
		}
		
		// insert suspend statistic
		for (Map.Entry<StringArray, SuspendStat> entry: suspendStatMap.entrySet()) {
			em.persist(entry.getValue());
		}
		
	}

	public void updateDayEndCompleteInfoForUncollect(String clusterCode, List<Long> dispOrderIdListForDayEnd, List<Long> dispOrderIdListForNonDayEnd, String batchRemark, int chunkSize) {
		logger.info("updateDayEndCompleteInfoForUncollect (pms-pms): start " + 
				"clusterCode = #0, batchRemark = #1, dispOrderIdListForDayEnd size = #2, " +
				"dispOrderIdListForNonDayEnd size = #3, chunkSize = #4",
				clusterCode, batchRemark, dispOrderIdListForDayEnd.size(), dispOrderIdListForDayEnd.size(), chunkSize);
		
		for (int start = 0, end = 0; start < dispOrderIdListForDayEnd.size(); start = end) {
			end = start + chunkSize;
			if (end > dispOrderIdListForDayEnd.size()) {
				end = dispOrderIdListForDayEnd.size();
			}
			
			em.createQuery(
					"update DispOrder o" + // 20120228 index check : DispOrder.id : PK_DISP_ORDER
					" set o.dayEndStatus = :dayEndStatus," +
					" o.batchRemark = concat(o.batchRemark, :batchRemark)," +
					" o.updateDate = :currentDate," +
					" o.version = o.version + 1" +
					" where o.id in :dispOrderIdList ")
					.setParameter("dayEndStatus", DispOrderDayEndStatus.Uncollected)
					.setParameter("batchRemark", batchRemark)
					.setParameter("currentDate", new Date())
					.setParameter("dispOrderIdList", dispOrderIdListForDayEnd.subList(start, end))
					.executeUpdate();
			
			logger.info("updateDayEndCompleteInfoForUncollect (pms-pms): update day end info for day end order batchRemark = #0, from list index = #1 to index =#2",
					batchRemark,
					start, 
					(end - 1)); 
		}
		
		for (int start = 0, end = 0; start < dispOrderIdListForNonDayEnd.size(); start = end) {
			end = start + chunkSize;
			if (end > dispOrderIdListForNonDayEnd.size()) {
				end = dispOrderIdListForNonDayEnd.size();
			}
			
			em.createQuery(
					"update DispOrder o" + // 20120228 index check : DispOrder.id : PK_DISP_ORDER
					" set o.batchProcessingFlag = :batchProcessingFlag," +
					" o.batchProcessingType = :batchProcessingType," +
					" o.batchRemark = concat(o.batchRemark, :batchRemark)," +
					" o.updateDate = :currentDate," +
					" o.version = o.version + 1" +
					" where o.id in :dispOrderIdList ")
					.setParameter("batchProcessingFlag", Boolean.FALSE)
					.setParameter("batchProcessingType", DispOrderBatchProcessingType.None)
					.setParameter("batchRemark", batchRemark)
					.setParameter("currentDate", new Date())
					.setParameter("dispOrderIdList", dispOrderIdListForNonDayEnd.subList(start, end))
					.executeUpdate();
			
			logger.info("updateDayEndCompleteInfoForUncollect (pms-pms): update day end info for non day end order " +
					"batchRemark = " + batchRemark + 
					", from list index = " + start + " to index = " + (end - 1)); 
		}		
	}
	
	public void updateDayEndCompleteInfoForSendSuspend(String clusterCode, List<Long> dispOrderIdListForSuspend, List<Long> dispOrderIdListForUnsuspend, String batchRemarkForSuspend, String batchRemarkForUnsuspend, int chunkSize) {
		
		logger.info("updateDayEndCompleteInfoForSendSuspend (pms-pms): start " + 
				"batchRemarkForSuspend #0, batchRemarkForUnsuspend = #1, suspend dispOrderIdList size = #2 " +
				"unsuspend dispOrderIdList size = #3",
				batchRemarkForSuspend, batchRemarkForUnsuspend, dispOrderIdListForSuspend.size(),
				dispOrderIdListForUnsuspend.size(), chunkSize);
		
		for (int start = 0, end = 0; start < dispOrderIdListForSuspend.size(); start = end) {
			end = start + chunkSize;
			if (end > dispOrderIdListForSuspend.size()) {
				end = dispOrderIdListForSuspend.size();
			}
			
			em.createQuery(
					"update DispOrder o" + // 20120228 index check : DispOrder.id : PK_DISP_ORDER
					" set o.batchProcessingFlag = :batchProcessingFlag," +
					" o.batchProcessingType = :batchProcessingType," +
					" o.batchRemark = concat(o.batchRemark, :batchRemark)," +
					" o.externalAdminStatus = :externalAdminStatus," +
					" o.updateDate = :currentDate," +
					" o.version = o.version + 1" +
					" where o.id in :dispOrderIdList")
					.setParameter("batchProcessingFlag", Boolean.FALSE)
					.setParameter("batchProcessingType", DispOrderBatchProcessingType.None)
					.setParameter("batchRemark", batchRemarkForSuspend)
					.setParameter("externalAdminStatus", DispOrderAdminStatus.Suspended)
					.setParameter("currentDate", new Date())
					.setParameter("dispOrderIdList", dispOrderIdListForSuspend.subList(start, end))
					.executeUpdate();

			if (logger.isDebugEnabled()) {
				logger.debug("updateDayEndCompleteInfoForSendSuspend (pms-pms): update day end info for suspend order " +
					"batchRemarkForSuspend = " + batchRemarkForSuspend + 
					", from list index = " + start + " to index = " + (end - 1)); 
			}
		}
		
		for (int start = 0, end = 0; start < dispOrderIdListForUnsuspend.size(); start = end) {
			end = start + chunkSize;
			if (end > dispOrderIdListForUnsuspend.size()) {
				end = dispOrderIdListForUnsuspend.size();
			}
			
			em.createQuery(
					"update DispOrder o" + // 20120228 index check : DispOrder.id : PK_DISP_ORDER
					" set o.batchProcessingFlag = :batchProcessingFlag," +
					" o.batchProcessingType = :batchProcessingType," +
					" o.batchRemark = concat(o.batchRemark, :batchRemark)," +
					" o.externalAdminStatus = :externalAdminStatus," +
					" o.updateDate = :currentDate," +
					" o.version = o.version + 1" +
					" where o.id in :dispOrderIdList")
					.setParameter("batchProcessingFlag", Boolean.FALSE)
					.setParameter("batchProcessingType", DispOrderBatchProcessingType.None)
					.setParameter("batchRemark", batchRemarkForUnsuspend)
					.setParameter("externalAdminStatus", DispOrderAdminStatus.Normal)
					.setParameter("currentDate", new Date())
					.setParameter("dispOrderIdList", dispOrderIdListForUnsuspend.subList(start, end))
					.executeUpdate();

			if (logger.isDebugEnabled()) {
				logger.debug("updateDayEndCompleteInfoForSendSuspend (pms-pms): update day end info for unsuspend order " +
					"batchRemarkForUnsuspend = " + batchRemarkForUnsuspend + 
					", from list index = " + start + " to index = " + (end - 1)); 
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void markDhLatestFlag(String clusterCode, String caseNum) {
		List<MedOrder> medOrderList = em.createQuery(
				"select o from MedOrder o " + // 20160615 index check : MedCase.caseNum : I_MED_CASE_01
				"where o.medCase.caseNum = :caseNum "
			)
			.setParameter("caseNum", caseNum)
			.getResultList();

		for (MedOrder medOrder : medOrderList) {
			medOrder.setDhLatestFlag(false);
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<DispOrder> retrieveDispOrderList(List<Long> dispOrderIdList) {
		List<DispOrder> dispOrderList = em.createQuery(
				"select o from DispOrder o" + // 20120228 index check : DispOrder.id : PK_DISP_ORDER
				" where o.id in :dispOrderIdList")
				.setParameter("dispOrderIdList", dispOrderIdList)
				.setHint(QueryHints.BATCH, "o.pharmOrder.medOrder")
				.setHint(QueryHints.BATCH, "o.dispOrderItemList")
				.getResultList();
		
		for (DispOrder dispOrder : dispOrderList) {
			dispOrder.getDispOrderItemList().size();
			dispOrder.getPharmOrder().getMedOrder();
		}
		
		return dispOrderList;
	}
	
	@SuppressWarnings("unchecked")
	private void refreshCacheRefillSchedule(List<Long> dispOrderIdList) {
		List<RefillSchedule> refillScheduleList = em.createQuery(
				"select o from RefillSchedule o" + // 20120228 index check : DispOrder.id : PK_DISP_ORDER
				" where o.dispOrder.id in :dispOrderIdList")
				.setParameter("dispOrderIdList", dispOrderIdList)
				.setHint(QueryHints.BATCH, "o.refillScheduleItemList")
				.getResultList();
		
		for (RefillSchedule refillSchedule: refillScheduleList) {
			refillSchedule.getRefillScheduleItemList().size();			
			refillSchedule.getDispOrder().addRefillSchedule(refillSchedule);
		}
	}
	
	private void calculateWorkloadStatistic(Map<Workstore, WorkloadStat> workloadStatMap, DispOrder dispOrder, Date batchDate) {
		// workload statistics - count order, orderItem, refillSchedule and refillScheduleItem
		
		MedOrder medOrder = dispOrder.getPharmOrder().getMedOrder();
		
		if (medOrder.getOrderType() == OrderType.OutPatient) {
			Workstore workstore = dispOrder.getWorkstore();
			WorkloadStat workloadStat = workloadStatMap.get(workstore);
			
			if (workloadStat == null) {
				workloadStat = new WorkloadStat();
				workloadStat.setOrderCount(0);
				workloadStat.setOrderItemCount(0);
				workloadStat.setRefillCount(0);
				workloadStat.setRefillItemCount(0);
				workloadStat.setWorkstore(workstore);
				workloadStat.setDayEndBatchDate(batchDate);
				workloadStatMap.put(workstore, workloadStat);
			}
			
			if (dispOrder.getRefillFlag() == Boolean.TRUE && dispOrder.getSfiFlag() == Boolean.FALSE) {
				workloadStat.setRefillCount(workloadStat.getRefillCount() + dispOrder.getRefillScheduleList().size());
				for (RefillSchedule refillSchedule: dispOrder.getRefillScheduleList()) {
					workloadStat.setRefillItemCount(workloadStat.getRefillItemCount() + refillSchedule.getRefillScheduleItemList().size());//refillItemCount += refillSchedule.getRefillScheduleItemList().size();
				}
			} else {
				workloadStat.setOrderCount(workloadStat.getOrderCount() + 1);
				workloadStat.setOrderItemCount(workloadStat.getOrderItemCount() + dispOrder.getDispOrderItemList().size());
			}
		}
	}
	
	private void calculateSuspendStatistic(Map<StringArray, SuspendStat> suspendStatMap, DispOrder dispOrder, Date batchDate) {
		// suspend stat
		
		MedOrder medOrder = dispOrder.getPharmOrder().getMedOrder();
		if (medOrder.getOrderType() == OrderType.OutPatient) {
			if (StringUtils.isNotBlank(dispOrder.getSuspendCode())) {
				Workstore workstore = dispOrder.getWorkstore();
				StringArray key = new StringArray(workstore.getHospCode(), workstore.getWorkstoreCode(), dispOrder.getSuspendCode());
				SuspendStat suspendStat = suspendStatMap.get(key);
				
				if (suspendStat == null) {
					suspendStat = new SuspendStat();
					suspendStat.setWorkstore(workstore);
					suspendStat.setDayEndBatchDate(batchDate);
					suspendStat.setSuspendCode(dispOrder.getSuspendCode());
					suspendStat.setRecordCount(Integer.valueOf(0));
					suspendStatMap.put(key, suspendStat);
				}
				
				suspendStat.setRecordCount(suspendStat.getRecordCount() + 1);
			}
		}
	}
	
	private String convertOrderNum(String patHospCode, Integer orderNum) {
		StringBuilder formatOrdNum = new StringBuilder();	
		formatOrdNum.append(StringUtils.trimToNull(patHospCode) );
		
		if (patHospCode.length() == 2 ){
			formatOrdNum.append(" ");
		}
		
		formatOrdNum.append(orderNum);
		return formatOrdNum.toString();
	}
	
	private static class MedProfileComparator implements Comparator<MedProfile>, Serializable {
		private static final long serialVersionUID = 1L;

		public int compare(MedProfile mp1, MedProfile mp2) {
			return mp1.getCreateDate().compareTo(mp2.getCreateDate()); 
		}
	}	
}
