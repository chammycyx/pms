package hk.org.ha.model.pms.persistence.reftable;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "DRS_PATIENT_CONTACT")
@Customizer(AuditCustomizer.class)
public class DrsPatientContact extends VersionEntity {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "drsPatientContactSeq")
	@SequenceGenerator(name = "drsPatientContactSeq", sequenceName = "SQ_DRS_PATIENT_CONTACT", initialValue = 100000000)
	private Long id;
	
	@Column(name = "DESCRIPTION", nullable = false, length=30)
	private String description;
	
	@Column(name = "PHONE", nullable = false, length=10)
	private String phone;
	
	@Column(name = "PHONE_EXT", length=4)
	private String phoneExt;
	
	@Column(name = "SMS_FLAG", nullable = false)
	private Boolean smsFlag;
	
	@Column(name = "VOICE_FLAG", nullable = false)
	private Boolean voiceFlag;
	
    @ManyToOne
    @JoinColumn(name = "DRS_PATIENT_ID", nullable = false)
    private DrsPatient drsPatient;

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhoneExt() {
		return phoneExt;
	}

	public void setPhoneExt(String phoneExt) {
		this.phoneExt = phoneExt;
	}

	public Boolean getSmsFlag() {
		return smsFlag;
	}

	public void setSmsFlag(Boolean smsFlag) {
		this.smsFlag = smsFlag;
	}

	public Boolean getVoiceFlag() {
		return voiceFlag;
	}

	public void setVoiceFlag(Boolean voiceFlag) {
		this.voiceFlag = voiceFlag;
	}

	public DrsPatient getDrsPatient() {
		return drsPatient;
	}

	public void setDrsPatient(DrsPatient drsPatient) {
		this.drsPatient = drsPatient;
	}
}
