package hk.org.ha.model.pms.vo.machine;

import java.io.Serializable;

import hk.org.ha.model.pms.udt.machine.IqdsAction;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class IqdsMessageContent implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String hostname;
	private int portNum;
	private IqdsAction iqdsAction;
	private String ticketNum;
	private String issueWindowNum;
	private String hospCode;
	private String workstoreCode;
	private Boolean touchMedEnabled;

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public int getPortNum() {
		return portNum;
	}

	public void setPortNum(int portNum) {
		this.portNum = portNum;
	}

	public IqdsAction getIqdsAction() {
		return iqdsAction;
	}

	public void setIqdsAction(IqdsAction iqdsAction) {
		this.iqdsAction = iqdsAction;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public String getIssueWindowNum() {
		return issueWindowNum;
	}

	public void setIssueWindowNum(String issueWindowNum) {
		this.issueWindowNum = issueWindowNum;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public Boolean getTouchMedEnabled() {
		return touchMedEnabled;
	}

	public void setTouchMedEnabled(Boolean touchMedEnabled) {
		this.touchMedEnabled = touchMedEnabled;
	}

}
