package hk.org.ha.model.pms.persistence.reftable;

import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "WARD_GROUP_ITEM")
public class WardGroupItem extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wardGroupItemSeq")
	@SequenceGenerator(name = "wardGroupItemSeq", sequenceName = "SQ_WARD_GROUP_ITEM", initialValue = 100000000)
	private Long id;

	@Column(name = "WARD_CODE", nullable = false, length = 4)
	private String wardCode;
	
	@ManyToOne
	@JoinColumn(name = "WARD_GROUP_ID", nullable = false)
	private WardGroup wardGroup;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public WardGroup getWardGroup() {
		return wardGroup;
	}

	public void setWardGroup(WardGroup wardGroup) {
		this.wardGroup = wardGroup;
	}
    
}
