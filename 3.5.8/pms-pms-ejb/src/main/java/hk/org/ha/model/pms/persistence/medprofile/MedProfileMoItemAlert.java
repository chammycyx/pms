package hk.org.ha.model.pms.persistence.medprofile;

import hk.org.ha.model.pms.persistence.AlertEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "MED_PROFILE_MO_ITEM_ALERT")
public class MedProfileMoItemAlert extends AlertEntity {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "medProfileMoItemAlertSeq")
	@SequenceGenerator(name = "medProfileMoItemAlertSeq", sequenceName = "SQ_MED_PROFILE_MO_ITEM_ALERT", initialValue = 100000000)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "MED_PROFILE_MO_ITEM_ID", nullable = false)
	private MedProfileMoItem medProfileMoItem;

	public MedProfileMoItemAlert() {
	}
	
	public MedProfileMoItemAlert(AlertEntity alertEntity) {
		this();
		setAlertType(alertEntity.getAlertType());
		setAlertXml(alertEntity.getAlertXml());
		setOverrideReason(alertEntity.getOverrideReason());
		setAckUser(alertEntity.getAckUser());
		setAckDate(alertEntity.getAckDate());
		setAlert(alertEntity.getAlert());
		setAdditiveNum(alertEntity.getAdditiveNum());
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MedProfileMoItem getMedProfileMoItem() {
		return medProfileMoItem;
	}

	public void setMedProfileMoItem(MedProfileMoItem medProfileMoItem) {
		this.medProfileMoItem = medProfileMoItem;
	}

	public MedProfileMoItem getMedItem() {
		return medProfileMoItem;
	}
}
