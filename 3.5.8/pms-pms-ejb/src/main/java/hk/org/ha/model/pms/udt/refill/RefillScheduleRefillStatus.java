package hk.org.ha.model.pms.udt.refill;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

import java.util.Arrays;
import java.util.List;

public enum RefillScheduleRefillStatus implements StringValuedEnum {

	None("-", "None"),
	Previous("P", "Previous Schedule"),
	Current("C", "Current Schedule"),
	Next("N", "Next Schedule");
	
    private final String dataValue;
    private final String displayValue;

    public static final List<RefillScheduleRefillStatus> Current_Next = Arrays.asList(Current,Next);
    
    RefillScheduleRefillStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    public String getDataValue() {
        return this.dataValue;
    }

    public String getDisplayValue() {
        return this.displayValue;
    }  
    
    public static RefillScheduleRefillStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(RefillScheduleRefillStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<RefillScheduleRefillStatus> {

		private static final long serialVersionUID = 1L;

		public Class<RefillScheduleRefillStatus> getEnumClass() {
    		return RefillScheduleRefillStatus.class;
    	}
    }
}
