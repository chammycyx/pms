package hk.org.ha.model.pms.vo.drug;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.vo.Bnf;
import hk.org.ha.model.pms.dms.vo.DrugCommonDosage;
import hk.org.ha.model.pms.dms.vo.Ingredient;
import hk.org.ha.model.pms.dms.vo.PreparationProperty;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
import hk.org.ha.model.pms.vo.rx.Site;
import hk.org.ha.model.pms.vo.security.PropMap;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class DrugSearchReturn implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private DrugSearchSource drugSearchSource;	// common use
	
	private String itemCodeByItemCodeSearch; 	// used by drugSearch()
	
	private List<Map<String, Object>> drugSearchNameMapList;	// used by drugSearch()
	
	private List<Map<String, Object>> drugSearchBnfMapList;	// used by drugSearchByBnf()
	
	private List<Map<String, Object>> drugSearchBnfNodeMapList;	// used by retrieveBnfTreeDetail()
	
	private List<Map<String, Object>> drugSearchDetailMapList;	// used by retrieveDrugDetail()
	
	private List<Bnf> drugSearchDrugBnfList;	// used by retrieveDrugSearchBnf()
	
	private List<Ingredient> drugSearchDrugIngredientList;	// used by retrieveDrugSearchIngredient()
	
	private List<String> drugSearchRouteDescList;	// used by retrieveFreeTextEntry()
	
	private List<String> drugSearchFormDescList;	// used by retrieveFreeTextEntry()

	private List<Site> drugSearchSiteList;	// used by retrieveMpFreeTextEntry()
	
	private List<PreparationProperty> drugSearchPreparationList;	// used by setSelectedMpDrugByVetting()
	
	private PropMap drugSearchPreparationDescMap;		// used by setSelectedMpDrugByVetting()
	
	private DmDrug drugSearchDmDrug;	// used by setSelectedDrugByDosageConversion()
	
	private DrugCommonDosage drugSearchCommonDosage;	// used by setSelectedDrugByDosageConversion()

	private MedOrderItem drugSearchMedOrderItem;		// used by setSelectedDrugByVetting()

	private List<MedProfileItem> drugSearchMedProfileItemList;		// used by setSelectedMpDrugByVetting()
	
	public DrugSearchSource getDrugSearchSource() {
		return drugSearchSource;
	}

	public void setDrugSearchSource(DrugSearchSource drugSearchSource) {
		this.drugSearchSource = drugSearchSource;
	}

	public String getItemCodeByItemCodeSearch() {
		return itemCodeByItemCodeSearch;
	}

	public void setItemCodeByItemCodeSearch(String itemCodeByItemCodeSearch) {
		this.itemCodeByItemCodeSearch = itemCodeByItemCodeSearch;
	}

	public List<Map<String, Object>> getDrugSearchNameMapList() {
		return drugSearchNameMapList;
	}

	public void setDrugSearchNameMapList(List<Map<String, Object>> drugSearchNameMapList) {
		this.drugSearchNameMapList = drugSearchNameMapList;
	}

	public List<Map<String, Object>> getDrugSearchBnfMapList() {
		return drugSearchBnfMapList;
	}

	public void setDrugSearchBnfMapList(
			List<Map<String, Object>> drugSearchBnfMapList) {
		this.drugSearchBnfMapList = drugSearchBnfMapList;
	}

	public List<Map<String, Object>> getDrugSearchBnfNodeMapList() {
		return drugSearchBnfNodeMapList;
	}

	public void setDrugSearchBnfNodeMapList(List<Map<String, Object>> drugSearchBnfNodeMapList) {
		this.drugSearchBnfNodeMapList = drugSearchBnfNodeMapList;
	}

	public List<Map<String, Object>> getDrugSearchDetailMapList() {
		return drugSearchDetailMapList;
	}

	public void setDrugSearchDetailMapList(List<Map<String, Object>> drugSearchDetailMapList) {
		this.drugSearchDetailMapList = drugSearchDetailMapList;
	}

	public List<Bnf> getDrugSearchDrugBnfList() {
		return drugSearchDrugBnfList;
	}

	public void setDrugSearchDrugBnfList(List<Bnf> drugSearchDrugBnfList) {
		this.drugSearchDrugBnfList = drugSearchDrugBnfList;
	}

	public List<Ingredient> getDrugSearchDrugIngredientList() {
		return drugSearchDrugIngredientList;
	}

	public void setDrugSearchDrugIngredientList(List<Ingredient> drugSearchDrugIngredientList) {
		this.drugSearchDrugIngredientList = drugSearchDrugIngredientList;
	}

	public List<String> getDrugSearchRouteDescList() {
		return drugSearchRouteDescList;
	}

	public void setDrugSearchRouteDescList(List<String> drugSearchRouteDescList) {
		this.drugSearchRouteDescList = drugSearchRouteDescList;
	}

	public List<String> getDrugSearchFormDescList() {
		return drugSearchFormDescList;
	}

	public void setDrugSearchFormDescList(List<String> drugSearchFormDescList) {
		this.drugSearchFormDescList = drugSearchFormDescList;
	}

	public List<Site> getDrugSearchSiteList() {
		return drugSearchSiteList;
	}

	public void setDrugSearchSiteList(List<Site> drugSearchSiteList) {
		this.drugSearchSiteList = drugSearchSiteList;
	}

	public List<PreparationProperty> getDrugSearchPreparationList() {
		return drugSearchPreparationList;
	}

	public void setDrugSearchPreparationList(List<PreparationProperty> drugSearchPreparationList) {
		this.drugSearchPreparationList = drugSearchPreparationList;
	}

	public PropMap getDrugSearchPreparationDescMap() {
		return drugSearchPreparationDescMap;
	}

	public void setDrugSearchPreparationDescMap(PropMap drugSearchPreparationDescMap) {
		this.drugSearchPreparationDescMap = drugSearchPreparationDescMap;
	}

	public DmDrug getDrugSearchDmDrug() {
		return drugSearchDmDrug;
	}

	public void setDrugSearchDmDrug(DmDrug drugSearchDmDrug) {
		this.drugSearchDmDrug = drugSearchDmDrug;
	}

	public DrugCommonDosage getDrugSearchCommonDosage() {
		return drugSearchCommonDosage;
	}

	public void setDrugSearchCommonDosage(DrugCommonDosage drugSearchCommonDosage) {
		this.drugSearchCommonDosage = drugSearchCommonDosage;
	}

	public MedOrderItem getDrugSearchMedOrderItem() {
		return drugSearchMedOrderItem;
	}

	public void setDrugSearchMedOrderItem(MedOrderItem drugSearchMedOrderItem) {
		this.drugSearchMedOrderItem = drugSearchMedOrderItem;
	}

	public List<MedProfileItem> getDrugSearchMedProfileItemList() {
		return drugSearchMedProfileItemList;
	}

	public void setDrugSearchMedProfileItemList(List<MedProfileItem> drugSearchMedProfileItemList) {
		this.drugSearchMedProfileItemList = drugSearchMedProfileItemList;
	}
}
