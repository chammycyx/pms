package hk.org.ha.model.pms.udt.reftable;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

import java.util.Arrays;
import java.util.List;

public enum MachineType implements StringValuedEnum {
	
	Manual("M", "Manual"),
	BakerCell("B", "Baker Cell"),
	ScriptPro("S", "Script Pro"),
	ATDPS("A", "ATDPS");

	private final String dataValue;
	private final String displayValue;
	
    public static final List<MachineType> Manual_ATDPS = Arrays.asList(Manual,ATDPS);
    public static final List<MachineType> Manual_BakerCell_ScriptPro = Arrays.asList(Manual,BakerCell,ScriptPro);
    public static final List<MachineType> Manual_BakerCell = Arrays.asList(Manual,BakerCell);
	
	MachineType(final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}
	
	public static MachineType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MachineType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<MachineType> {

		private static final long serialVersionUID = 1L;

		public Class<MachineType> getEnumClass() {
			return MachineType.class;
		}

	}
	
}
