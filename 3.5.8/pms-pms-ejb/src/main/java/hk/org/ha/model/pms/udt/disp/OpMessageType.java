package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum OpMessageType implements StringValuedEnum {

	// From MOE to PMS
	NewOrder("N", "newOrder", "create", "OP_NEW_ORDER"),
	UpdateOrder("U", "updateOrder", "update", "OP_UPDATE_ORDER"),
	ConfirmRemark("C", "confirmRemark", "remark", "OP_CONFIRM_REMARK"),
	Discontinue("D", "discontinue", "discontinue", ""),
	AllowModify("A", "allowModify", "allowmodify", ""),
	PrnProperty("P", "prnProperty", "prnproperty", "");
	
	private final String dataValue;
	private final String displayValue;
	private final String packageName;
	private final String headerMsgType;
	
	OpMessageType (final String dataValue, final String displayValue, final String packageName, final String headerMsgType) {
		this.dataValue = dataValue;
		this.displayValue = displayValue; // xsd name
		this.packageName = packageName;
		this.headerMsgType = headerMsgType;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}

	public String getPackageName() {
		return this.packageName;
	}
	
	public String getHeaderMsgType() {
		return this.headerMsgType;
	}

	public static OpMessageType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(OpMessageType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<OpMessageType> {

		private static final long serialVersionUID = 1L;

		public Class<OpMessageType> getEnumClass() {
    		return OpMessageType.class;
    	}
    }
}