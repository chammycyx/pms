package hk.org.ha.model.pms.biz.onestop;

import java.util.Date;
import java.util.Map;

import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.charging.DrugChargeClearanceManagerLocal;
import hk.org.ha.model.pms.biz.order.DispOrderManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.charging.ClearanceStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderDayEndStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.InvoiceDocType;
import hk.org.ha.model.pms.udt.disp.PharmOrderAdminStatus;
import hk.org.ha.model.pms.udt.onestop.AllowCommentType;
import hk.org.ha.model.pms.vo.charging.FcsPaymentStatus;
import hk.org.ha.model.pms.vo.charging.SfiForceProceedInfo;
import hk.org.ha.model.pms.vo.onestop.MoStatusCount;
import hk.org.ha.model.pms.vo.security.PropMap;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.joda.time.DateTime;
import static hk.org.ha.model.pms.prop.Prop.CDDH_LEGACY_PERSISTENCE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.ONESTOP_SUSPENDORDERCOUNT_DURATION_LIMIT;

@Stateful
@Scope(ScopeType.SESSION)
@Name("allowModifyPrescService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class AllowModifyPrescServiceBean implements AllowModifyPrescServiceLocal {

	@PersistenceContext
	private EntityManager em; 
	
	private FcsPaymentStatus fcsPaymentStatus;
	
	private Boolean sfiForceProceed;
	
	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
	
	@In(required = false) 
	@Out(required=false)
	private SfiForceProceedInfo sfiForceProceedInfo;
	
	@In
	private DispOrderManagerLocal dispOrderManager;
	
	@In
	private OneStopCountManagerLocal oneStopCountManager;
	
	@In
	private DrugChargeClearanceManagerLocal drugChargeClearanceManager;
	
	@In
	private OneStopOrderStatusManagerLocal oneStopOrderStatusManager;
	
	@In
	private AuditLogger auditLogger;
			
	@In
	private UamInfo uamInfo;
	
	@In
	private Workstore workstore;

	@In
	private Workstation workstation;
	
	@In
	private PropMap propMap;

	@In(required=false)
	@Out(required=false)
	private String reAuthUserId;
	
	@Out(required = false)
	private MoStatusCount moStatusCount;
	
	private String statusErrorMsg;
	
	public String checkAllowModifyOrderStatus(Long dispOrderId, Long dispOrderVersion)
	{
		retrieveUnvetAndSuspendCount();
		statusErrorMsg = oneStopOrderStatusManager.verifySuspendAllowModifyUncollectStatus(dispOrderId, dispOrderVersion);
		return statusErrorMsg;
	}
	
	public void allowModifyWithSuspend(Long dispOrderId,  Long dispOrderVersion, String suspendCode) {
		
		if (checkAllowModifyOrderStatus(dispOrderId, dispOrderVersion) == null)
		{		
			DispOrder dispOrder = em.find(DispOrder.class, dispOrderId);
			dispOrder.loadChild();		
			dispOrder.getPharmOrder().getMedOrder().setCmsUpdateDate(new DateTime().toDate());
			dispOrder.getPharmOrder().setAdminStatus(PharmOrderAdminStatus.AllowModify);
			
			if (dispOrder.getDayEndStatus() == DispOrderDayEndStatus.None && ! dispOrder.getBatchProcessingFlag()) {
				dispOrder.setAdminStatus(DispOrderAdminStatus.Suspended);
				dispOrder.setSuspendCode(suspendCode);
			}
			
			forceUpdateDispOrder(dispOrder);
			writeAuditLog("#0519", dispOrderId, suspendCode);
			em.flush();
			dispOrder.getPharmOrder().clearDmInfo();
			corpPmsServiceProxy.updateAdminStatus(dispOrder, dispOrder.getAdminStatus(), PharmOrderAdminStatus.AllowModify, 
					CDDH_LEGACY_PERSISTENCE_ENABLED.get(false), CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED.get(false), null);
		}
		
		retrieveUnvetAndSuspendCount();
	}
	
	public void allowModifyWithPostComment(Long dispOrderId,  Long dispOrderVersion, AllowCommentType allowCommentType) {
		
		if (checkAllowModifyOrderStatus(dispOrderId, dispOrderVersion) == null)
		{
			DispOrder dispOrder = em.find(DispOrder.class, dispOrderId);
			dispOrder.loadChild();
			dispOrder.getPharmOrder().setAllowCommentType(allowCommentType);
			dispOrder.getPharmOrder().setAllowCommentBeforeFlag(Boolean.TRUE);
			dispOrder.getPharmOrder().getMedOrder().setCmsUpdateDate(new DateTime().toDate());
			forceUpdateDispOrder(dispOrder);
			writeAuditLog("#0519", dispOrderId, null);
			em.flush();
			dispOrder.getPharmOrder().clearDmInfo();
			corpPmsServiceProxy.updateAllowCommentType(dispOrder, allowCommentType);
		}
		
		retrieveUnvetAndSuspendCount();
	}
	
	public void reverseAllowModifyPrescription(Long dispOrderId, Long dispOrderVersion) {
		
		if (checkAllowModifyOrderStatus(dispOrderId, dispOrderVersion) == null)
		{
			DispOrder dispOrder = em.find(DispOrder.class, dispOrderId);
			dispOrder.loadChild();
			fcsPaymentStatus = null;
			sfiForceProceed = false;
			
			if (dispOrder.getPharmOrder().getAllowCommentType() != AllowCommentType.None)
			{
				dispOrder.getPharmOrder().getMedOrder().setCmsUpdateDate(new DateTime().toDate());
				dispOrder.getPharmOrder().setAllowCommentType(AllowCommentType.None);
				forceUpdateDispOrder(dispOrder);
				em.flush();
				dispOrder.getPharmOrder().clearDmInfo();
				corpPmsServiceProxy.updateAllowCommentType(dispOrder, AllowCommentType.None);
				dispOrder.getPharmOrder().loadDmInfo();
				writeAuditLog("#0518", dispOrderId, null);
				retrieveUnvetAndSuspendCount();
				return;
			}
			
			if (dispOrder.getSfiFlag() && dispOrder.getDayEndStatus() == DispOrderDayEndStatus.None && ! dispOrder.getBatchProcessingFlag()) {
				for (Invoice invoice : dispOrder.getInvoiceList()) {
					if (invoice.getDocType().equals(InvoiceDocType.Sfi)){
						fcsPaymentStatus = drugChargeClearanceManager.checkSfiDrugClearanceByInvoice(invoice);
						if (fcsPaymentStatus.getClearanceStatus() == ClearanceStatus.PaymentClear || invoice.getForceProceedFlag()) {
							dispOrder.getPharmOrder().getMedOrder().setCmsUpdateDate(new DateTime().toDate());							
							dispOrder.getPharmOrder().setAdminStatus(PharmOrderAdminStatus.Normal);
							dispOrder.setAdminStatus(DispOrderAdminStatus.Normal);
							if (dispOrder.getStatus() == DispOrderStatus.Issued) {
								dispOrder.setIssueDate(new Date());
							}
							forceUpdateDispOrder(dispOrder);
							
							// set large layout flag for label reprint and CDDH label preview (for PMS mode)
							// (note: since the flag is needed to sync to CORP for CDDH label preview, it is set here instead of in print disp order function)
							Map<Long, Boolean> largeLayoutFlagMap = dispOrderManager.updateDispOrderLargeLayoutFlag(dispOrder);
							
							writeAuditLog("#0518", dispOrderId, null);
							em.flush();
							dispOrder.getPharmOrder().clearDmInfo();
							corpPmsServiceProxy.updateAdminStatus(dispOrder, DispOrderAdminStatus.Normal, PharmOrderAdminStatus.Normal, 
									CDDH_LEGACY_PERSISTENCE_ENABLED.get(false), CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED.get(false), largeLayoutFlagMap);
							dispOrder.getPharmOrder().loadDmInfo();
							dispOrderManager.printDispOrder(dispOrder, true);
							sfiForceProceed = true;
							break;
						}
					}
				}
				retrieveUnvetAndSuspendCount();
				return;
			}
			
			dispOrder.getPharmOrder().getMedOrder().setCmsUpdateDate(new DateTime().toDate());
			dispOrder.getPharmOrder().setAdminStatus(PharmOrderAdminStatus.Normal);
			dispOrder.setAdminStatus(DispOrderAdminStatus.Normal);
			if (dispOrder.getStatus() == DispOrderStatus.Issued && dispOrder.getDayEndStatus() == DispOrderDayEndStatus.None && ! dispOrder.getBatchProcessingFlag()) {
				dispOrder.setIssueDate(new Date());
			}
			forceUpdateDispOrder(dispOrder);
			
			Map<Long, Boolean> largeLayoutFlagMap = null;
			if (dispOrder.getDayEndStatus() == DispOrderDayEndStatus.None && ! dispOrder.getBatchProcessingFlag()) {
				// set large layout flag for label reprint and CDDH label preview (for PMS mode)
				// (note: since the flag is needed to sync to CORP for CDDH label preview, it is set here instead of in print disp order function)
				// (note: follow condition of print disp order)
				largeLayoutFlagMap = dispOrderManager.updateDispOrderLargeLayoutFlag(dispOrder);
			}
			
			writeAuditLog("#0518", dispOrderId, null);
			em.flush();
			dispOrder.getPharmOrder().clearDmInfo();
			corpPmsServiceProxy.updateAdminStatus(dispOrder, DispOrderAdminStatus.Normal, PharmOrderAdminStatus.Normal, 
					CDDH_LEGACY_PERSISTENCE_ENABLED.get(false), CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED.get(false), largeLayoutFlagMap);
			dispOrder.getPharmOrder().loadDmInfo();
			if (dispOrder.getDayEndStatus() == DispOrderDayEndStatus.None && ! dispOrder.getBatchProcessingFlag()) {
				dispOrderManager.printDispOrder(dispOrder, true);
			}
			
			retrieveUnvetAndSuspendCount();
		}
	}

	public void sfiPrescriptionAllowModifyDeferProceed(Long dispOrderId, Long dispOrderVersion) {
		
		if (checkAllowModifyOrderStatus(dispOrderId, dispOrderVersion) == null)
		{
			DispOrder dispOrder = em.find(DispOrder.class, dispOrderId);
			dispOrder.loadChild();
			dispOrder.getPharmOrder().getMedOrder().setCmsUpdateDate(new DateTime().toDate());
			dispOrder.getPharmOrder().setAdminStatus(PharmOrderAdminStatus.Normal);
			dispOrder.setAdminStatus(DispOrderAdminStatus.Suspended);
			forceUpdateDispOrder(dispOrder);
			writeAuditLog("#0518", dispOrderId, null);
			em.flush();
			dispOrder.getPharmOrder().clearDmInfo();
			corpPmsServiceProxy.updateAdminStatus(dispOrder, DispOrderAdminStatus.Suspended, PharmOrderAdminStatus.Normal, 
					CDDH_LEGACY_PERSISTENCE_ENABLED.get(false), CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED.get(false), null);
		}
		
		retrieveUnvetAndSuspendCount();
	}
	
	public void sfiPrescriptionAllowModifyForceProceed(Long dispOrderId, Long dispOrderVersion) {
		
		if (checkAllowModifyOrderStatus(dispOrderId, dispOrderVersion) == null)
		{
			DispOrder dispOrder = em.find(DispOrder.class, dispOrderId);
			dispOrder.loadChild();
			Invoice sfiInvoice = null;
					
			if ( sfiForceProceedInfo != null ) {
				for (Invoice invoice : dispOrder.getInvoiceList() ) {
					if (invoice.getDocType().equals(InvoiceDocType.Sfi)){
						sfiForceProceedInfo.setInvoice(invoice);
						sfiInvoice = invoice;
					}
				}	
			}
			
			
			dispOrder.getPharmOrder().setAdminStatus(PharmOrderAdminStatus.Normal);
			dispOrder.setAdminStatus(DispOrderAdminStatus.Normal);
			if (dispOrder.getStatus() == DispOrderStatus.Issued) {
				dispOrder.setIssueDate(new Date());
			}
			dispOrder.getPharmOrder().getMedOrder().setCmsUpdateDate(new DateTime().toDate());
			forceUpdateDispOrder(dispOrder);
			
			// set large layout flag for label reprint and CDDH label preview (for PMS mode)
			// (note: since the flag is needed to sync to CORP for CDDH label preview, it is set here instead of in print disp order function)
			Map<Long, Boolean> largeLayoutFlagMap = dispOrderManager.updateDispOrderLargeLayoutFlag(dispOrder);
			
			writeAuditLog("#0518", dispOrderId, null);
			em.flush();
			dispOrder.getPharmOrder().clearDmInfo();
			corpPmsServiceProxy.updateAdminStatusForceProceed(dispOrder, DispOrderAdminStatus.Normal, PharmOrderAdminStatus.Normal, 
					CDDH_LEGACY_PERSISTENCE_ENABLED.get(false), sfiInvoice, CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED.get(false), largeLayoutFlagMap);
			dispOrder.getPharmOrder().loadDmInfo();
			dispOrderManager.printDispOrder(dispOrder, true);
			sfiForceProceedInfo = null;
		}
		
		retrieveUnvetAndSuspendCount();
	}
	
	private void forceUpdateDispOrder(DispOrder dispOrder)
	{
		dispOrder.setUpdateDate(new DateTime().toDate());
		dispOrder.setUpdateUser(uamInfo.getUserId());
	}
	
	private void writeAuditLog(String msgCode, Long dispOrderId, String suspendCode)
	{
		if (msgCode.equals("#0519"))
		{
			auditLogger.log(msgCode, (reAuthUserId==null?uamInfo.getUserId():reAuthUserId), 
					workstation.getHostName(), dispOrderId, suspendCode);
		}
		else
		{
			auditLogger.log(msgCode, (reAuthUserId==null?uamInfo.getUserId():reAuthUserId), 
					workstation.getHostName(), dispOrderId);
		}
		
		if (reAuthUserId != null)
		{
			reAuthUserId = null;
		}
	}
	
	private void retrieveUnvetAndSuspendCount() {
		if (oneStopCountManager.isCounterTimeOut(moStatusCount))
		{
			oneStopCountManager.retrieveUnvetAndSuspendCount(workstore, propMap.getValueAsInteger(ONESTOP_SUSPENDORDERCOUNT_DURATION_LIMIT.getName()));		
		}
		
		moStatusCount = OneStopCountManagerBean.getMoStatusCountByWorkstore(workstore);
	}
	
	public String getStatusErrorMsg() {
		return statusErrorMsg;
	}
	
	public FcsPaymentStatus getFcsPaymentStatus() {
		return fcsPaymentStatus; 
	}
	
	public Boolean getSfiForceProceed() {
		return sfiForceProceed;
	}

	@Remove
	public void destroy(){
		if (moStatusCount != null)
		{
			moStatusCount = null;
		}
	}
}