package hk.org.ha.model.pms.udt.vetting;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum OrderType implements StringValuedEnum {

	InPatient("I", "In Patient"),
	OutPatient("O", "Out Patient");
	
	private final String dataValue;
	private final String displayValue;
	
	OrderType (final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}
	
	public static OrderType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(OrderType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<OrderType> {

		private static final long serialVersionUID = 1L;

		public Class<OrderType> getEnumClass() {
    		return OrderType.class;
    	}
    }
}
