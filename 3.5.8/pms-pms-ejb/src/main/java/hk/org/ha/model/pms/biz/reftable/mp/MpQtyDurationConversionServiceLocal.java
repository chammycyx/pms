package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.model.pms.dms.persistence.PmsIpDurationConv;
import hk.org.ha.model.pms.dms.persistence.PmsPcuMapping;
import hk.org.ha.model.pms.dms.persistence.PmsIpQtyConv;
import hk.org.ha.model.pms.dms.persistence.PmsQtyConversionAdj;
import hk.org.ha.model.pms.dms.persistence.PmsQtyConversionInsu;
import hk.org.ha.model.pms.vo.reftable.mp.MpQtyDurationConversionInfo;

import java.util.List;

import javax.ejb.Local;
 
@Local
public interface MpQtyDurationConversionServiceLocal {
	
	MpQtyDurationConversionInfo retrieveMpQtyDurationConversionList(String itemCode, PmsPcuMapping pmsPcuMapping);
	
	String updateMpQtyDurationConversionList(List<PmsIpQtyConv> inPmsQtyConversionList, boolean qtyApplyAll, 
											List<PmsIpDurationConv> inPmsDurationConversionList, boolean durationApplyAll,
											PmsQtyConversionInsu inPmsQtyConversionInsu, boolean insuApplyAll,
											PmsQtyConversionAdj updatePmsQtyConversionAdj);
	
	void destroy();
	
} 
 