package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.dms.persistence.DmWarning;
import hk.org.ha.model.pms.persistence.reftable.LocalWarning;
import hk.org.ha.model.pms.vo.report.LocalWarningRpt;

import java.util.List;

import javax.ejb.Local;

@Local
public interface LocalWarningListServiceLocal {
	
	void retrieveLocalWarningList(String itemCode);
	
	List<DmWarning> retrieveDmWarningValidateList();
	
	void updateLocalWarningList(List<LocalWarning> updateLocalWarningList);
	
	boolean isFollowUp(); 
	
	boolean isValidItemCode();
	
	List<LocalWarningRpt> retrieveLocalWarningRptList();
	
	void printLocalWarningRpt();
	
	void destroy(); 
	
}
