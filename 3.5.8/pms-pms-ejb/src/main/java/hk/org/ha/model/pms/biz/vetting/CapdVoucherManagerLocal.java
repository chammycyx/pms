package hk.org.ha.model.pms.biz.vetting;

import hk.org.ha.model.pms.persistence.disp.CapdVoucher;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.vetting.CapdVoucherPrintOut;

import java.util.List;

import javax.ejb.Local;

@Local
public interface CapdVoucherManagerLocal {
	
	List<CapdVoucher> createCapdVoucherList(DispOrder dispOrder, List<CapdVoucher> prevCvList);
	
	void printCapdVoucher(PrintLang printLang, List<CapdVoucher> capdVoucherList, boolean isReprint);
	void prepareCapdVoucherPrintJob(List<RenderAndPrintJob> printJobList, PrintLang printLang, List<CapdVoucher> capdVoucherList, boolean isReprint);

	CapdVoucherPrintOut retrieveCapdVoucherPrintOut (CapdVoucher capdVoucher, boolean isReprint, PrintLang printLang);
		
	List<CapdVoucher> retrieveCapdVoucherList(DispOrder dispOrder);
	
	List<CapdVoucher> retrieveCapdVoucherList(DispOrder dispOrder, boolean includeDeleted);
	
	void prepareCapdVoucherPrintJob(List<RenderAndPrintJob> printJobList, DispOrder dispOrder);
	
	void updateCapdVoucherStatusToIssue(List<CapdVoucher> capdVoucherList);
	
}
