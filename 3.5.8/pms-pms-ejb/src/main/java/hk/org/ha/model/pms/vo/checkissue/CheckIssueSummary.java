package hk.org.ha.model.pms.vo.checkissue;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class CheckIssueSummary implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private PrescDetail prescDetail;
	
	private CheckIssueListSummary checkIssueListSummary;

	public PrescDetail getPrescDetail() {
		return prescDetail;
	}

	public void setPrescDetail(PrescDetail prescDetail) {
		this.prescDetail = prescDetail;
	}

	public CheckIssueListSummary getCheckIssueListSummary() {
		return checkIssueListSummary;
	}

	public void setCheckIssueListSummary(CheckIssueListSummary checkIssueListSummary) {
		this.checkIssueListSummary = checkIssueListSummary;
	}
	
}