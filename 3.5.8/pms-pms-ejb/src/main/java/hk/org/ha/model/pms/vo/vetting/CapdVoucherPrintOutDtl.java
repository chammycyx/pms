package hk.org.ha.model.pms.vo.vetting;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class CapdVoucherPrintOutDtl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String itemCode1;
	private String fullDrugDesc1;
	private String itemCode2;
	private String fullDrugDesc2;
	private String itemCode3;
	private String fullDrugDesc3;	
	private String dispQty1;
	private String moDosageUnit1;
	private String volume1;
	private String volumeUnit1;
	private String strengthValue1;
	private String strengthUnit1;
	private String ingredient1;
	private String calciumStrengthUnit1;
	private String connectSystem1;
	private String dispQty2;
	private String moDosageUnit2;
	private String volume2;
	private String volumeUnit2;
	private String strengthValue2;
	private String strengthUnit2;
	private String ingredient2;
	private String calciumStrengthUnit2;
	private String connectSystem2;
	private String dispQty3;
	private String moDosageUnit3;
	private String volume3;
	private String volumeUnit3;
	private String strengthValue3;
	private String strengthUnit3;
	private String ingredient3;
	private String calciumStrengthUnit3;
	private String connectSystem3;
	
	public String getItemCode1() {
		return itemCode1;
	}
	public void setItemCode1(String itemCode1) {
		this.itemCode1 = itemCode1;
	}
	public String getFullDrugDesc1() {
		return fullDrugDesc1;
	}
	public void setFullDrugDesc1(String fullDrugDesc1) {
		this.fullDrugDesc1 = fullDrugDesc1;
	}
	public String getItemCode2() {
		return itemCode2;
	}
	public void setItemCode2(String itemCode2) {
		this.itemCode2 = itemCode2;
	}
	public String getFullDrugDesc2() {
		return fullDrugDesc2;
	}
	public void setFullDrugDesc2(String fullDrugDesc2) {
		this.fullDrugDesc2 = fullDrugDesc2;
	}
	public String getItemCode3() {
		return itemCode3;
	}
	public void setItemCode3(String itemCode3) {
		this.itemCode3 = itemCode3;
	}
	public String getFullDrugDesc3() {
		return fullDrugDesc3;
	}
	public void setFullDrugDesc3(String fullDrugDesc3) {
		this.fullDrugDesc3 = fullDrugDesc3;
	}
	public String getDispQty1() {
		return dispQty1;
	}
	public void setDispQty1(String dispQty1) {
		this.dispQty1 = dispQty1;
	}
	public String getMoDosageUnit1() {
		return moDosageUnit1;
	}
	public void setMoDosageUnit1(String moDosageUnit1) {
		this.moDosageUnit1 = moDosageUnit1;
	}
	public String getVolume1() {
		return volume1;
	}
	public void setVolume1(String volume1) {
		this.volume1 = volume1;
	}
	public String getVolumeUnit1() {
		return volumeUnit1;
	}
	public void setVolumeUnit1(String volumeUnit1) {
		this.volumeUnit1 = volumeUnit1;
	}
	public String getStrengthValue1() {
		return strengthValue1;
	}
	public void setStrengthValue1(String strengthValue1) {
		this.strengthValue1 = strengthValue1;
	}
	public String getStrengthUnit1() {
		return strengthUnit1;
	}
	public void setStrengthUnit1(String strengthUnit1) {
		this.strengthUnit1 = strengthUnit1;
	}
	public String getIngredient1() {
		return ingredient1;
	}
	public void setIngredient1(String ingredient1) {
		this.ingredient1 = ingredient1;
	}
	public String getCalciumStrengthUnit1() {
		return calciumStrengthUnit1;
	}
	public void setCalciumStrengthUnit1(String calciumStrengthUnit1) {
		this.calciumStrengthUnit1 = calciumStrengthUnit1;
	}
	public String getConnectSystem1() {
		return connectSystem1;
	}
	public void setConnectSystem1(String connectSystem1) {
		this.connectSystem1 = connectSystem1;
	}
	public String getDispQty2() {
		return dispQty2;
	}
	public void setDispQty2(String dispQty2) {
		this.dispQty2 = dispQty2;
	}
	public String getMoDosageUnit2() {
		return moDosageUnit2;
	}
	public void setMoDosageUnit2(String moDosageUnit2) {
		this.moDosageUnit2 = moDosageUnit2;
	}
	public String getVolume2() {
		return volume2;
	}
	public void setVolume2(String volume2) {
		this.volume2 = volume2;
	}
	public String getVolumeUnit2() {
		return volumeUnit2;
	}
	public void setVolumeUnit2(String volumeUnit2) {
		this.volumeUnit2 = volumeUnit2;
	}
	public String getStrengthValue2() {
		return strengthValue2;
	}
	public void setStrengthValue2(String strengthValue2) {
		this.strengthValue2 = strengthValue2;
	}
	public String getStrengthUnit2() {
		return strengthUnit2;
	}
	public void setStrengthUnit2(String strengthUnit2) {
		this.strengthUnit2 = strengthUnit2;
	}
	public String getIngredient2() {
		return ingredient2;
	}
	public void setIngredient2(String ingredient2) {
		this.ingredient2 = ingredient2;
	}
	public String getCalciumStrengthUnit2() {
		return calciumStrengthUnit2;
	}
	public void setCalciumStrengthUnit2(String calciumStrengthUnit2) {
		this.calciumStrengthUnit2 = calciumStrengthUnit2;
	}
	public String getConnectSystem2() {
		return connectSystem2;
	}
	public void setConnectSystem2(String connectSystem2) {
		this.connectSystem2 = connectSystem2;
	}
	public String getDispQty3() {
		return dispQty3;
	}
	public void setDispQty3(String dispQty3) {
		this.dispQty3 = dispQty3;
	}
	public String getMoDosageUnit3() {
		return moDosageUnit3;
	}
	public void setMoDosageUnit3(String moDosageUnit3) {
		this.moDosageUnit3 = moDosageUnit3;
	}
	public String getVolume3() {
		return volume3;
	}
	public void setVolume3(String volume3) {
		this.volume3 = volume3;
	}
	public String getVolumeUnit3() {
		return volumeUnit3;
	}
	public void setVolumeUnit3(String volumeUnit3) {
		this.volumeUnit3 = volumeUnit3;
	}
	public String getStrengthValue3() {
		return strengthValue3;
	}
	public void setStrengthValue3(String strengthValue3) {
		this.strengthValue3 = strengthValue3;
	}
	public String getStrengthUnit3() {
		return strengthUnit3;
	}
	public void setStrengthUnit3(String strengthUnit3) {
		this.strengthUnit3 = strengthUnit3;
	}
	public String getIngredient3() {
		return ingredient3;
	}
	public void setIngredient3(String ingredient3) {
		this.ingredient3 = ingredient3;
	}
	public String getCalciumStrengthUnit3() {
		return calciumStrengthUnit3;
	}
	public void setCalciumStrengthUnit3(String calciumStrengthUnit3) {
		this.calciumStrengthUnit3 = calciumStrengthUnit3;
	}
	public String getConnectSystem3() {
		return connectSystem3;
	}
	public void setConnectSystem3(String connectSystem3) {
		this.connectSystem3 = connectSystem3;
	}
}
