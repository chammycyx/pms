package hk.org.ha.model.pms.biz.medprofile;

import static hk.org.ha.model.pms.prop.Prop.VETTING_MEDPROFILE_TPN_DESKTOPNAME;
import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.UnreachableException;
import hk.org.ha.model.pms.asa.exception.tpn.TpnException;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.TpnRequest;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.medprofile.TpnRequestStatus;
import hk.org.ha.service.pms.asa.interfaces.tpn.TpnServiceJmsRemote;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("tpnRequestService")
@RemoteDestination
@MeasureCalls
@SuppressWarnings("unchecked")
public class TpnRequestServiceBean implements TpnRequestServiceLocal {
	private static final String NEW_REQUEST = "N";

	private static final String MODIFY_REQUEST = "M";
	
	private static final String DUPLICATE_REQUEST = "S";
	
	private static String TPN_LOGIN_PREFIX;
	
	@PersistenceContext
	private EntityManager em;
	
	@In(required=false)
	@Out(required=false)
	private List<TpnRequest> tpnRequestList;
	
	@In
	private Workstation workstation;
	
	@In
	private Workstore workstore;
	
	@In
	public TpnServiceJmsRemote tpnServiceProxy;
	
	@In(required=false)
	@Out(required=false)
	private Long tpnSessionId;
	
	@In(required=false)
	@Out(required=false)
	private TpnRequest tpnRequest;
	
	@Logger
	private Log logger;
	
	public TpnRequestServiceBean() {		
	}
	
	static {
		TPN_LOGIN_PREFIX = System.getProperty("tpn.login.prefix","");
	}
	
	@Override
	public String newTpnRecord(int duration) {
		TpnRequest newTpnRequest = new TpnRequest();
		newTpnRequest.setDuration(duration);
		newTpnRequest.setStatus(TpnRequestStatus.Initial);
		newTpnRequest.setModifyFlag(true);
		
		MedProfile medProfile = (MedProfile)Contexts.getSessionContext().get("medProfile");

		em.persist(newTpnRequest);
		em.flush();

		newTpnRequest.setCddhStatus(TpnRequest.CDDH_STATUS_NEW);
		tpnRequest = newTpnRequest;
		
		String tpnUrl = null;
		try{
			Pair<String, Long> ws = tpnServiceProxy.newTpnRecord(
					tpnSessionId, tpnRequest.getId(), medProfile,
					workstore.getHospCode(), TPN_LOGIN_PREFIX + workstation.getHostName(),
					VETTING_MEDPROFILE_TPN_DESKTOPNAME.get(),
					"CORP", workstation.getHostName());
			if(ws.getFirst() != null && ws.getSecond() != null){
				tpnUrl = ws.getFirst() + "?method=Launch&SessionID=" + ws.getSecond() + "&DisplayNavigation=false";
				tpnSessionId = ws.getSecond();
			}else{
				tpnUrl = null;
				tpnSessionId = null;
			}
		} catch(UnreachableException e){
			logger.error(e);
			return null;
		} catch(TpnException e){
			logger.error(e);
			return null;
		}
		
		return tpnUrl;
	}
	
	@Override
	public String duplicateTpnRecord(int duration, Long pnId) {
		TpnRequest duplicateTpnRequest = new TpnRequest();
		duplicateTpnRequest.setDuration(duration);
		duplicateTpnRequest.setStatus(TpnRequestStatus.Initial);
		duplicateTpnRequest.setModifyFlag(true);
		
		MedProfile medProfile = (MedProfile)Contexts.getSessionContext().get("medProfile");
		
		em.persist(duplicateTpnRequest);
		em.flush();
		
		duplicateTpnRequest.setCddhStatus(TpnRequest.CDDH_STATUS_NEW);
		tpnRequest = duplicateTpnRequest;
		
		String tpnUrl;
		try{
			Pair<String, Long> ws = tpnServiceProxy.duplicateTpnRecord(
					tpnSessionId, tpnRequest.getId(), pnId, medProfile,  
					workstore.getHospCode(), TPN_LOGIN_PREFIX + workstation.getHostName(),
					VETTING_MEDPROFILE_TPN_DESKTOPNAME.get(),
					"CORP", workstation.getHostName());
			if(ws.getFirst() != null && ws.getSecond() != null){
				tpnUrl = ws.getFirst() + "?method=Launch&SessionID=" + ws.getSecond() + "&DisplayNavigation=false";
				tpnSessionId = ws.getSecond();
			}else{
				tpnUrl = null;
				tpnSessionId = null;
			}
		} catch(UnreachableException e){
			logger.error(e);
			return null;
		} catch(TpnException e){
			logger.error(e);
			return null;
		}

		return tpnUrl;
	}
	
	@Override
	public boolean deleteTpnRecord(int selectedIndex, String deleteReason) {
		TpnRequest tpnRequest = tpnRequestList.get(selectedIndex);
		Pair<Boolean, Long> ws;
		
		try{
			ws = tpnServiceProxy.deleteTpnRecord(
					tpnSessionId, tpnRequest.getId(), deleteReason,
					TPN_LOGIN_PREFIX + workstation.getHostName(),
					"CORP", workstation.getHostName());
			tpnSessionId = ws.getSecond();
		} catch(UnreachableException e){
			logger.error(e);
			return false;
		} catch(TpnException e){
			logger.error(e);
			return false;
		}
		
		if(ws.getFirst()){
			tpnRequest.setStatus(TpnRequestStatus.Deleted);
			tpnRequest.setModifyFlag(true);
			if (TpnRequest.CDDH_STATUS_NEW.equals(tpnRequest.getCddhStatus())) {
				tpnRequest.setCddhStatus(null);
			}
			else {
				tpnRequest.setCddhStatus(TpnRequest.CDDH_STATUS_DELETE);
			}
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public void modifyTpnRecord(int duration, int selectedIndex) {
		TpnRequest modifyTpnRequest = tpnRequestList.get(selectedIndex);
		modifyTpnRequest.setDuration(duration);
		modifyTpnRequest.setModifyFlag(true);
		modifyTpnRequest.setCddhStatus(TpnRequest.CDDH_STATUS_MODIFY);
		tpnRequest = modifyTpnRequest;
	}
	
	@Override
	public String modifyRegimen(int selectedIndex) {
		TpnRequest tpnRequest = tpnRequestList.get(selectedIndex);
		MedProfile medProfile = (MedProfile)Contexts.getSessionContext().get("medProfile");
		
		String tpnUrl;
		try{
			Pair<String, Long> ws = tpnServiceProxy.modifyTpnRecord(
					tpnSessionId, tpnRequest.getId(), medProfile, workstore.getHospCode(),
					TPN_LOGIN_PREFIX + workstation.getHostName(), VETTING_MEDPROFILE_TPN_DESKTOPNAME.get(),
					"CORP", workstation.getHostName());
			
			if(ws.getFirst() != null && ws.getSecond() != null){
				tpnUrl = ws.getFirst() + "?method=Launch&SessionID=" + ws.getSecond() + "&DisplayNavigation=false";
				tpnSessionId = ws.getSecond();
			}else{
				tpnUrl = null;
				tpnSessionId = null;
			}
		} catch(UnreachableException e){
			logger.error(e);
			return null;
		} catch(TpnException e){
			logger.error(e);
			return null;
		}
		
		return tpnUrl;		
	}
	
	@Override
	public String viewTpnRecord(int selectedIndex) {
		TpnRequest tpnRequest = tpnRequestList.get(selectedIndex);
		MedProfile medProfile = (MedProfile)Contexts.getSessionContext().get("medProfile");
		
		String tpnUrl;
		try{
			Pair<String, Long> ws = tpnServiceProxy.viewTpnRecord(
					tpnSessionId, tpnRequest.getId(), medProfile, workstore.getHospCode(),
					TPN_LOGIN_PREFIX + workstation.getHostName(), VETTING_MEDPROFILE_TPN_DESKTOPNAME.get(),
					"CORP", workstation.getHostName());
			
			if(ws.getFirst() != null && ws.getSecond() != null){
				tpnUrl = ws.getFirst() + "?method=Launch&SessionID=" + ws.getSecond() + "&DisplayNavigation=false";
				tpnSessionId = ws.getSecond();
			}else{
				tpnUrl = null;
				tpnSessionId = null;
			}
		} catch(UnreachableException e){
			logger.error(e);
			return null;
		} catch(TpnException e){
			logger.error(e);
			return null;
		}
		
		return tpnUrl;
	}
	
	@Override
	public boolean readTpnRecord(Long pnId, String mode, int duration, int selectedIndex) {
		boolean isValidateRecord = false;
		
		try{
			isValidateRecord = tpnServiceProxy.validateTpnRecord(tpnSessionId, pnId);
		} catch(UnreachableException e){
			logger.error(e);
			return false;
		} catch(TpnException e){
			logger.error(e);
			return false;
		}
		
		if(isValidateRecord){
			if(mode.equals(NEW_REQUEST) || mode.equals(DUPLICATE_REQUEST)){
				tpnRequest.setStatus(TpnRequestStatus.Processed);
				tpnRequestList.add(0, tpnRequest);
			}
			
			if(mode.equals(MODIFY_REQUEST)){
				modifyTpnRecord(duration, selectedIndex);
			}
			
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public void retrieveTpnRequestList() {
		MedProfile medProfile = (MedProfile)Contexts.getSessionContext().get("medProfile");
		if(medProfile != null){
			tpnRequestList = em.createQuery(
					"select o from TpnRequest o" + // 20160615 index check : TpnRequest.medProfile : FK_TPN_REQUEST_01
					" where o.medProfile = :medProfile" +
					" and o.status != 'I'"+
					" order by o.createDate desc")
					.setParameter("medProfile", medProfile)
					.getResultList();
			
			em.clear();			
		}else{
			tpnRequestList = new ArrayList<TpnRequest>();
		}
	}
	
	@Remove
	@Override
	public void destroy() {
		tpnRequestList = null;
    }
}