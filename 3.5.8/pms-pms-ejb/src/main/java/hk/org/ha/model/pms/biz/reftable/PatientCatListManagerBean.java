package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("patientCatListManager")
@MeasureCalls
public class PatientCatListManagerBean implements PatientCatListManagerLocal {
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private Workstore workstore;
	
	@SuppressWarnings("unchecked")
	public List<String> retrievePatientCatList() 
	{
		return em.createQuery(
					"select o.patCatCode from PatientCat o" + // 20171023 index check : PatientCat.institution : FK_PATIENT_CAT_01
					" where o.institution = :institution" +
					" and o.status = :status" +
					" order by o.patCatCode")
					.setParameter("institution", workstore.getWorkstoreGroup().getInstitution())
					.setParameter("status", RecordStatus.Active)
					.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<String> retrievePatientCatListWithSuspend() 
	{
		return em.createQuery(
					"select o.patCatCode from PatientCat o" + // 20120227 index check : PatientCat.institution : FK_PATIENT_CAT_01
					" where o.institution = :institution" +
					" and o.status <> :status" +
					" order by o.patCatCode")
					.setParameter("institution", workstore.getWorkstoreGroup().getInstitution())
					.setParameter("status", RecordStatus.Delete)
					.getResultList();
	}
}
