package hk.org.ha.model.pms.biz.medprofile;

import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.uam.vo.AuthenticateCriteria;
import hk.org.ha.model.pms.vo.medprofile.AuthenticateResult;
import hk.org.ha.service.biz.pms.uam.interfaces.UamServiceJmsRemote;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Stateful
@Scope(ScopeType.SESSION)
@Name("privilegeAuthenticateService")
@RemoteDestination
@MeasureCalls
public class PrivilegeAuthenticateServiceBean implements PrivilegeAuthenticateServiceLocal {

	@In
	private UamInfo uamInfo;

	@In
	private UamServiceJmsRemote uamServiceProxy;
	
    public PrivilegeAuthenticateServiceBean() {
    }
    
    @Override
	public AuthenticateResult authenticate(String userName, String password, String right) {

		AuthenticateCriteria authenticateCriteria = new AuthenticateCriteria();
		authenticateCriteria.setProfileCode(uamInfo.getProfileCode());
		authenticateCriteria.setUserName(userName);
		authenticateCriteria.setPassword(password);
		authenticateCriteria.setTarget(right);
		authenticateCriteria.setAction("Y");

		return new AuthenticateResult(uamServiceProxy.authenticateWithUserInfo(authenticateCriteria));
	}
    
    @Remove
	@Override
	public void destroy() {
    }
}
