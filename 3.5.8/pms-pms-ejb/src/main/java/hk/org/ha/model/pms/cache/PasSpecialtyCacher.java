package hk.org.ha.model.pms.cache;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.util.ObjectArray;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.pas.PasException;
import hk.org.ha.model.pms.asa.exception.UnreachableException;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.patient.PasSpecialtyType;
import hk.org.ha.model.pms.vo.patient.PasSpecialty;
import hk.org.ha.service.pms.asa.interfaces.pas.PasServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.NestableRuntimeException;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("pasSpecialtyCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class PasSpecialtyCacher extends BaseCacher implements PasSpecialtyCacherInf {

	@In(required = false)
	private Workstore workstore;

	@In
	public PasServiceJmsRemote pasServiceProxy;

	private Map<ObjectArray, InnerCacher> cacherMap = new HashMap<ObjectArray, InnerCacher>();

	public void clear() { 
		synchronized (this) {
			cacherMap.clear();
		}
	}	
	
	public PasSpecialty getPasSpecialty(String specCode, PasSpecialtyType specType) {
		for(PasSpecialty pasSpecialty: getPasSpecialty(false)) {
			if( StringUtils.equals(pasSpecialty.getPasSpecCode(), specCode) && pasSpecialty.getType()== specType) {
				return pasSpecialty;
			}
		}
		
		return null;
	}
	
	public List<PasSpecialty> getPasSpecialtyList() {
		return this.getPasSpecialty(false);
	}
	
	public List<PasSpecialty> getPasSpecialtyListByPatHospCode(String patHospCode) {
		return this.getPasSpecialtyByPatHospCode(false, patHospCode);
	}

	public List<PasSpecialty> getPasSpecialtyWithSubSpecialtyList() {
		return this.getPasSpecialty(true);
	}

	public List<PasSpecialty> getPasSpecialtyWithSubSpecialtyListByPatHospCode(String patHospCode) {
		return this.getPasSpecialtyByPatHospCode(true, patHospCode);
	}
	
	public List<PasSpecialty> getPasSpecialtyListBySpecCode(String prefixSpecCode, PasSpecialtyType specType) {
		return this.getPasSpecialtyBySpecCode(prefixSpecCode, specType, false);
	}
	
	private List<PasSpecialty> getPasSpecialty(boolean withSubSpecialty) {
		return (List<PasSpecialty>) this.getCacher( workstore.getDefPatHospCode(), withSubSpecialty ).getAll();
	}
	
	private List<PasSpecialty> getPasSpecialtyByPatHospCode(boolean withSubSpecialty, String patHospCode) {
		return (List<PasSpecialty>) this.getCacher( patHospCode, withSubSpecialty ).getAll();
	}
	
	private List<PasSpecialty> getPasSpecialtyBySpecCode(String prefixSpecCode, PasSpecialtyType specType, boolean withSubSpecialty) {
		List<PasSpecialty> pasSpecialtyList = new ArrayList<PasSpecialty>();
		
		for(PasSpecialty pasSpecialty: getPasSpecialty(withSubSpecialty)) {
			if(pasSpecialty.getPasSpecCode().startsWith(prefixSpecCode) && (pasSpecialty.getType()==specType || specType==PasSpecialtyType.Blank)) {
				pasSpecialtyList.add(pasSpecialty);
			}
		}
		
		return pasSpecialtyList;
	}
	
	private InnerCacher getCacher(String patHospCode, boolean withSubSpecialty) {
		synchronized (this) {
			ObjectArray key = new ObjectArray(patHospCode, withSubSpecialty);
			InnerCacher cacher = cacherMap.get(key);
			if (cacher == null) {
				cacher = new InnerCacher(patHospCode, withSubSpecialty, this.getExpireTime());
				cacherMap.put(key, cacher);
			}
			return cacher;
		}
	}

	private class InnerCacher extends AbstractCacher<String, PasSpecialty> {
		private String patHospCode = null;
		private boolean withSubSpecialty = false;

		public InnerCacher(String patHospCode, boolean withSubSpecialty, int expireTime) {
			super(expireTime);
			this.patHospCode = patHospCode;
			this.withSubSpecialty = withSubSpecialty;
		}

		public PasSpecialty create(String specCode) {
			throw new UnsupportedOperationException();
		}
		
		public Collection<PasSpecialty> createAll() {
			try {
				if(withSubSpecialty) {
					return pasServiceProxy.retrievePasSpecialtyWithSubSpecialty(patHospCode);
				} else {
					return pasServiceProxy.retrievePasSpecialty(patHospCode);
				}
			} catch (PasException e) {
				throw new NestableRuntimeException(e);
			} catch (UnreachableException e) {
				throw new NestableRuntimeException(e);
			}
		}

		public String retrieveKey(PasSpecialty pasSpecialty) {
			return pasSpecialty.getPasSpecCode();
		}
	}

	public static PasSpecialtyCacher instance()
	{
		  if (!Contexts.isApplicationContextActive())
	      {
	         throw new IllegalStateException("No active application scope");
	      }
	      return (PasSpecialtyCacher) Component.getInstance("pasSpecialtyCacher", ScopeType.APPLICATION);
    }	
}
