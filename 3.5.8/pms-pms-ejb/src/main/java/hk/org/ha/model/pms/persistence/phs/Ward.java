package hk.org.ha.model.pms.persistence.phs;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Institution;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "WARD")
@IdClass(WardPK.class)
public class Ward extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "INST_CODE", length = 3)
	private String instCode;

	@Id
	@Column(name = "WARD_CODE", length = 4)
	private String wardCode;
	
	@Column(name = "DESCRIPTION", length = 40)
	private String description;

    @Converter(name = "Ward.status", converterClass = RecordStatus.Converter.class)
    @Convert("Ward.status")
	@Column(name = "STATUS", nullable = false, length = 1)
	private RecordStatus status;

    @ManyToOne
    @JoinColumn(name = "INST_CODE", nullable = false, insertable = false, updatable = false)
    private Institution institution;
    
	@OneToMany(mappedBy = "ward")
	private List<WardStock> wardStockList;

	public Ward() {
    	status = RecordStatus.Active;
    }
	
	public Ward(String instCode, String wardCode) {
		this();
		this.instCode = instCode;
		this.wardCode = wardCode;
	}
	
	public Ward(String instCode, String wardCode, String description, String status) {
		this(instCode, wardCode);
    	this.description = description;
    	if( "Y".equals(status) ){
    		this.status = RecordStatus.Suspend;
    	}else{
    		this.status = RecordStatus.Active;
    	}
	}	
    
    public String getInstCode() {
		return instCode;
	}

	public void setInstCode(String instCode) {
		this.instCode = instCode;
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public RecordStatus getStatus() {
		return status;
	}

	public void setStatus(RecordStatus status) {
		this.status = status;
	}

	public Institution getInstitution() {
		return institution;
	}
	
	public List<WardStock> getWardStockList() {
		return wardStockList;
	}

	public void setWardStockList(List<WardStock> wardStockList) {
		this.wardStockList = wardStockList;
	}
	
	public WardPK getId(){
		return new WardPK(instCode, wardCode);
	}

	@Override
	public int hashCode() {		
		return new HashCodeBuilder()
			.append(instCode)
			.append(wardCode)
			.toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		Ward other = (Ward) obj;
		return new EqualsBuilder()
			.append(instCode, other.getInstCode())
			.append(wardCode, other.getWardCode())
			.isEquals();
	}	
}
