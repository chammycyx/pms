package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class SfiRefillCouponFtr implements Serializable {

	private static final long serialVersionUID = 1L;

	private String refillCouponRemarkEng;

	private String refillCouponRemarkChi;
	
	private Date lastUpdateTime;
	
	private String refillCouponNum;
	
	private String nextExemptReason;
	
	private String userId;
	
	public String getRefillCouponRemarkEng() {
		return refillCouponRemarkEng;
	}
	
	public void setRefillCouponRemarkEng(String refillCouponRemarkEng) {
		this.refillCouponRemarkEng = refillCouponRemarkEng;
	}
	
	public String getRefillCouponRemarkChi() {
		return refillCouponRemarkChi;
	}

	public void setRefillCouponRemarkChi(String refillCouponRemarkChi) {
		this.refillCouponRemarkChi = refillCouponRemarkChi;
	}

	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}
	
	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	
	public String getRefillCouponNum() {
		return refillCouponNum;
	}
	
	public void setRefillCouponNum(String refillCouponNum) {
		this.refillCouponNum = refillCouponNum;
	}
	
	public String getNextExemptReason() {
		return nextExemptReason;
	}
	
	public void setNextExemptReason(String nextExemptReason) {
		this.nextExemptReason = nextExemptReason;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}
