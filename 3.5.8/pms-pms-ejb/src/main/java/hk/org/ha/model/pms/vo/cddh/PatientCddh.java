package hk.org.ha.model.pms.vo.cddh;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="PatientCddh")
@ExternalizedBean(type=DefaultExternalizer.class)
public class PatientCddh implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@XmlElement(name="patientCddhPharmItem")
	private List<PatientCddhPharmItem> patientCddhPharmItemList;

	public PatientCddh() {
	}

	public List<PatientCddhPharmItem> getPatientCddhPharmItemList() {
		if (patientCddhPharmItemList == null) {
			patientCddhPharmItemList = new ArrayList<PatientCddhPharmItem>();
		}
		return patientCddhPharmItemList;
	}

	public void setPatientCddhPharmItemList(List<PatientCddhPharmItem> patientCddhPharmItemList) {
		this.patientCddhPharmItemList = patientCddhPharmItemList;
	}
	
	public void addPatientCddhPharmItem(PatientCddhPharmItem patientCddhPharmItem) {
		getPatientCddhPharmItemList().add(patientCddhPharmItem);
	}
}
