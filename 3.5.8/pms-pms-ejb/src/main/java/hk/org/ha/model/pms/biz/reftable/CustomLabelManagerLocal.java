package hk.org.ha.model.pms.biz.reftable;

import java.util.List;

import javax.ejb.Local;

import hk.org.ha.model.pms.persistence.reftable.CustomLabel;
import hk.org.ha.model.pms.udt.label.CustomLabelType;

@Local
public interface CustomLabelManagerLocal {

	CustomLabel retrieveCustomLabel(String name, CustomLabelType type);
	
	List<CustomLabel> retrieveCustomLabelByLabelNum(String labelNum);
	
	void updateCustomLabel(CustomLabel customLabel);
	
	void removeCustomLabel(String name, CustomLabelType type);
	
	List<CustomLabel> retrieveCustomLabelList(CustomLabelType type);
	
	void updateCustomLabelList(List<CustomLabel> customLabelList);
}
