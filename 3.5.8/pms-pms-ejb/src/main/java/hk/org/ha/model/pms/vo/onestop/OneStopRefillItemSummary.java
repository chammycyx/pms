package hk.org.ha.model.pms.vo.onestop;

import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.disp.PharmOrderPatType;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderStatus;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class OneStopRefillItemSummary implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Long id;
	
	private OneStopOrderType oneStopOrderType;
	
	private String refillCouponNum;
	
	private Date refillDate;
	
	private String groupNum;
	
	private String numOfRefill;
	
	private PharmOrderPatType patType;
	
	private String patHospCode;
	
	private String doctorCode;
	
	private String doctorName;
	
	private String phsSpecCode;
	
	private String orignalPhsSpecCode;
	
	private String phsWardCode;
	
	private String patCatCode;
	
	private MedOrderPrescType prescType;
	
	private String orderNum;
	
	private Date orderDate;
				
	private String ticketNum;
	
	private String ticketWks;
	
	private String ticketHospCode;
	
	private MedCase medCase;
		
	private Long refillScheduleId;
	
	private Date ticketDate;
	
	private String dispWorkstore;
	
	private String dispHospCode;
	
	private Long dispOrderId;
	
	private Boolean isOrderSuspended;
	
	private Boolean isOrignalOrderLocked;
	
	private Boolean isPreviousOrderIssued;
	
	private Boolean isOrignalOrderDeleted;
	
	private Boolean isOrignalOrderSuspended;
	
	private Boolean dayEnd;
	
	private Date dispDate;
	
	private Boolean privateFlag;
	
	private String invoiceNum;
	
	private Long dispOrderVersion;
	
	private Long medOrderVersion;
	
	private Patient patient;
	
	private OneStopOrderStatus oneStopOrderStatus;
	
	private Integer refillCount;
	
	private Boolean unlockOrder;
	
	private String lockWorkstationName;
	
	private Long prevDispOrderId;
	
	private Long prevDispOrderVersion;
		
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
		this.isOrderSuspended = false;
		this.isOrignalOrderLocked = false;
		this.isOrignalOrderDeleted = false;
		this.isOrignalOrderSuspended = false;
		this.isPreviousOrderIssued = true;
		this.dayEnd = false;
		this.unlockOrder = false;
	}

	public OneStopOrderType getOneStopOrderType() {
		return oneStopOrderType;
	}

	public void setOneStopOrderType(OneStopOrderType oneStopOrderType) {
		this.oneStopOrderType = oneStopOrderType;
	}

	public Date getRefillDate() {
		return refillDate;
	}

	public void setRefillDate(Date refillDate) {
		this.refillDate = refillDate;
	}

	public String getGroupNum() {
		return groupNum;
	}

	public void setGroupNum(String groupNum) {
		this.groupNum = groupNum;
	}

	public String getNumOfRefill() {
		return numOfRefill;
	}

	public void setNumOfRefill(String numOfRefill) {
		this.numOfRefill = numOfRefill;
	}

	public PharmOrderPatType getPatType() {
		return patType;
	}

	public void setPatType(PharmOrderPatType patType) {
		this.patType = patType;
	}

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}
	
	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getPhsSpecCode() {
		return phsSpecCode;
	}

	public void setPhsSpecCode(String phsSpecCode) {
		this.phsSpecCode = phsSpecCode;
	}

	public String getPhsWardCode() {
		return phsWardCode;
	}

	public void setPhsWardCode(String phsWardCode) {
		this.phsWardCode = phsWardCode;
	}

	public String getPatCatCode() {
		return patCatCode;
	}

	public void setPatCatCode(String patCatCode) {
		this.patCatCode = patCatCode;
	}

	public MedOrderPrescType getPrescType() {
		return prescType;
	}

	public void setPrescType(MedOrderPrescType prescType) {
		this.prescType = prescType;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public String getTicketWks() {
		return ticketWks;
	}

	public void setTicketWks(String ticketWks) {
		this.ticketWks = ticketWks;
	}
	
	public String getTicketHospCode() {
		return ticketHospCode;
	}

	public void setTicketHospCode(String ticketHospCode) {
		this.ticketHospCode = ticketHospCode;
	}

	public void setRefillCouponNum(String refillCouponNum) {
		this.refillCouponNum = refillCouponNum;
	}

	public String getRefillCouponNum() {
		return refillCouponNum;
	}

	public void setMedCase(MedCase medCase) {
		this.medCase = medCase;
	}

	public MedCase getMedCase() {
		return medCase;
	}

	public Long getRefillScheduleId() {
		return refillScheduleId;
	}

	public void setRefillScheduleId(Long refillScheduleId) {
		this.refillScheduleId = refillScheduleId;
	}

	public Date getTicketDate() {
		return ticketDate;
	}

	public void setTicketDate(Date ticketDate) {
		this.ticketDate = ticketDate;
	}

	public String getDispWorkstore() {
		return dispWorkstore;
	}

	public void setDispWorkstore(String dispWorkstore) {
		this.dispWorkstore = dispWorkstore;
	}

	public Long getDispOrderId() {
		return dispOrderId;
	}

	public void setDispOrderId(Long dispOrderId) {
		this.dispOrderId = dispOrderId;
	}

	public Boolean getIsOrderSuspended() {
		return isOrderSuspended;
	}

	public void setIsOrderSuspended(Boolean isOrderSuspended) {
		this.isOrderSuspended = isOrderSuspended;
	}

	public Boolean getDayEnd() {
		return dayEnd;
	}

	public void setDayEnd(Boolean dayEnd) {
		this.dayEnd = dayEnd;
	}

	public Date getDispDate() {
		return dispDate;
	}

	public void setDispDate(Date dispDate) {
		this.dispDate = dispDate;
	}

	public Boolean getPrivateFlag() {
		return privateFlag;
	}

	public void setPrivateFlag(Boolean privateFlag) {
		this.privateFlag = privateFlag;
	}

	public Boolean getIsOrignalOrderLocked() {
		return isOrignalOrderLocked;
	}

	public void setIsOrignalOrderLocked(Boolean isOrignalOrderLocked) {
		this.isOrignalOrderLocked = isOrignalOrderLocked;
	}

	public Boolean getIsPreviousOrderIssued() {
		return isPreviousOrderIssued;
	}

	public void setIsPreviousOrderIssued(Boolean isPreviousOrderIssued) {
		this.isPreviousOrderIssued = isPreviousOrderIssued;
	}

	public Boolean getIsOrignalOrderDeleted() {
		return isOrignalOrderDeleted;
	}

	public void setIsOrignalOrderDeleted(Boolean isOrignalOrderDeleted) {
		this.isOrignalOrderDeleted = isOrignalOrderDeleted;
	}

	public Boolean getIsOrignalOrderSuspended() {
		return isOrignalOrderSuspended;
	}

	public void setIsOrignalOrderSuspended(Boolean isOrignalOrderSuspended) {
		this.isOrignalOrderSuspended = isOrignalOrderSuspended;
	}

	public String getInvoiceNum() {
		return invoiceNum;
	}

	public void setInvoiceNum(String invoiceNum) {
		this.invoiceNum = invoiceNum;
	}

	public void setDispOrderVersion(Long dispOrderVersion) {
		this.dispOrderVersion = dispOrderVersion;
	}

	public Long getDispOrderVersion() {
		return dispOrderVersion;
	}
	
	public Long getMedOrderVersion() {
		return medOrderVersion;
	}

	public void setMedOrderVersion(Long medOrderVersion) {
		this.medOrderVersion = medOrderVersion;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public OneStopOrderStatus getOneStopOrderStatus() {
		return oneStopOrderStatus;
	}

	public void setOneStopOrderStatus(OneStopOrderStatus oneStopOrderStatus) {
		this.oneStopOrderStatus = oneStopOrderStatus;
	}

	public void setRefillCount(Integer refillCount) {
		this.refillCount = refillCount;
	}

	public Integer getRefillCount() {
		return refillCount;
	}

	public void setOrignalPhsSpecCode(String orignalPhsSpecCode) {
		this.orignalPhsSpecCode = orignalPhsSpecCode;
	}

	public String getOrignalPhsSpecCode() {
		return orignalPhsSpecCode;
	}
	
	public Boolean getUnlockOrder() {
		return unlockOrder;
	}

	public void setUnlockOrder(Boolean unlockOrder) {
		this.unlockOrder = unlockOrder;
	}

	public String getLockWorkstationName() {
		return lockWorkstationName;
	}

	public void setLockWorkstationName(String lockWorkstationName) {
		this.lockWorkstationName = lockWorkstationName;
	}
	
	public Long getPrevDispOrderId() {
		return prevDispOrderId;
	}
	
	public void setPrevDispOrderId(Long prevDispOrderId) {
		this.prevDispOrderId = prevDispOrderId;
	}

	public Long getPrevDispOrderVersion() {
		return prevDispOrderVersion;
	}

	public void setPrevDispOrderVersion(Long prevDispOrderVersion) {
		this.prevDispOrderVersion = prevDispOrderVersion;
	}

	public String getDispHospCode() {
		return dispHospCode;
	}

	public void setDispHospCode(String dispHospCode) {
		this.dispHospCode = dispHospCode;
	}
}