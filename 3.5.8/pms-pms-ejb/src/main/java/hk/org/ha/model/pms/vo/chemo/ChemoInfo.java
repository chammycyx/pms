package hk.org.ha.model.pms.vo.chemo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Chemo")
@ExternalizedBean(type=DefaultExternalizer.class)
public class ChemoInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@XmlElement(name="ChemoItem")
	private List<ChemoItem> chemoItemList;
	
	public ChemoInfo() {
	}
	
	public ChemoInfo(List<ChemoItem> chemoItemList) {
		setChemoItemList(chemoItemList);
	}

	public List<ChemoItem> getChemoItemList() {
		if (chemoItemList == null) {
			chemoItemList = new ArrayList<ChemoItem>();
		}
		return chemoItemList;
	}

	public void setChemoItemList(List<ChemoItem> chemoItemList) {
		this.chemoItemList = chemoItemList;
	}
}
