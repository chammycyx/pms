package hk.org.ha.model.pms.biz.vetting.mp;

import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MpRenalEcrclLog;
import hk.org.ha.model.pms.persistence.medprofile.MpRenalMessage;

import java.util.List;

import javax.ejb.Local;

@Local
public interface RenalAdjManagerLocal {

	List<MpRenalEcrclLog> retrieveMpRenalEcrclLog(Long medProfileId);

	List<MpRenalMessage> retrieveMpRenalMessage(Long id);

	void save(List<MpRenalEcrclLog> mpRenalEcrclLogList, List<MpRenalMessage> mpRenalMessageList, MedProfile medProfile);
}
