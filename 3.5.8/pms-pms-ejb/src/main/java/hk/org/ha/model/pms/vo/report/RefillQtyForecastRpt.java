package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class RefillQtyForecastRpt implements Serializable {

	private static final long serialVersionUID = 1L;

	private String itemCode;
	
	private String fullDrugDesc;
	
	private Long totalRefillQtyCount;
	
	private String baseUnit;
	
	public RefillQtyForecastRpt(String itemCode, String fullDrugDesc,
			Long totalRefillQtyCount, String baseUnit) {
		super();
		this.itemCode = itemCode;
		this.fullDrugDesc = fullDrugDesc;
		this.totalRefillQtyCount = totalRefillQtyCount;
		this.baseUnit = baseUnit;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getFullDrugDesc() {
		return fullDrugDesc;
	}

	public void setFullDrugDesc(String fullDrugDesc) {
		this.fullDrugDesc = fullDrugDesc;
	}

	public Long getTotalRefillQtyCount() {
		return totalRefillQtyCount;
	}

	public void setTotalRefillQtyCount(Long totalRefillQtyCount) {
		this.totalRefillQtyCount = totalRefillQtyCount;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public String getBaseUnit() {
		return baseUnit;
	}
	
	


}
