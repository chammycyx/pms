package hk.org.ha.model.pms.biz.report;

import static hk.org.ha.model.pms.prop.Prop.REPORT_WORKSTORE_CLOSINGHOUR;
import static hk.org.ha.model.pms.prop.Prop.REPORT_WORKSTORE_OPENINGHOUR;
import static hk.org.ha.model.pms.prop.Prop.PICKING_AUTO_ENABLED;
import hk.org.ha.fmk.pms.util.JavaBeanResult;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderStatus;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.EdsActivityRpt;
import hk.org.ha.model.pms.vo.report.EdsActivityRptDtl;
import hk.org.ha.model.pms.vo.report.EdsActivityRptHdr;
import hk.org.ha.model.pms.vo.report.EdsElapseTime;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.joda.time.DateTime;

@Stateful
@Scope(ScopeType.SESSION)
@Name("edsActivityRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class EdsActivityRptServiceBean implements EdsActivityRptServiceLocal {
	@PersistenceContext
	private EntityManager em;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent; 
		
	@In
	private Workstore workstore;

	private boolean retrieveSuccess;
	
	private List<EdsActivityRpt> edsActivityRptList;
	
	private String currentRpt = null;
	
	private List<EdsActivityRpt> reportObj = null;

	public void retrieveEdsActivityRpt(Integer selectedTab, Date date) {
		reportObj = retrieveEdsActivityRptList(date); 
		if (selectedTab == 0){
			currentRpt = "EdsActivityChart";
		}
		else if (selectedTab == 1){
			currentRpt = "EdsActivityRpt";
		}
		
		if ( !reportObj.isEmpty() ) {
			retrieveSuccess = true;
		}
		else{
			retrieveSuccess = false;
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<EdsElapseTime> retrieveEdsElapseTime(final String fromElapseTime, final String elapseTime, Date date) {
		DateTime fromDate = new DateTime(date);
		DateTime toDate = fromDate.plusDays(1);
		
		Query entryQuery = em.createNativeQuery(
				"select" + // 20150317 index check : DispOrder.workstore,createDate/ticketCreateDate/vetDate/pickDate/assembleDate/checkDate/issueDate : I_DISP_ORDER_13 I_DISP_ORDER_16 I_DISP_ORDER_17 I_DISP_ORDER_04 I_DISP_ORDER_05 I_DISP_ORDER_06 I_DISP_ORDER_07
				"  to_char(" + elapseTime + ", 'HH24') as timeRange," +
				"  round(avg(time_diff('MI'," + fromElapseTime + ", " + elapseTime + "))) as avgTime" +
				" from pharm_order p, disp_order d" +		
				" where d.pharm_order_id = p.id" +
				" and " + fromElapseTime + " between ?1 and ?2" +
				" and " + elapseTime + " between ?1 and ?2" +
				" and d.workstore_code = ?3" +
				" and d.hosp_code = ?4" +
				" and p.status not in (?5,?6)" +
				" and d.admin_status <> ?7" +
				" and d.order_type = ?8" +
				" group by to_char(" + elapseTime + ", 'HH24')" +
				" order by to_char(" + elapseTime + ", 'HH24')" );

		JavaBeanResult.setQueryResultClass(entryQuery, EdsElapseTime.class);
		
		return (List<EdsElapseTime>) entryQuery.setParameter(1, fromDate.toDate())
						.setParameter(2, toDate.toDate())
						.setParameter(3, workstore.getWorkstoreCode())
						.setParameter(4, workstore.getHospCode())
						.setParameter(5, PharmOrderStatus.Deleted.getDataValue())
						.setParameter(6, PharmOrderStatus.SysDeleted.getDataValue())
						.setParameter(7, DispOrderAdminStatus.Suspended.getDataValue())
					    .setParameter(8, OrderType.OutPatient.getDataValue())
						.getResultList();
	}
	
	public List<EdsActivityRpt> retrieveEdsActivityRptList(Date date) {
		edsActivityRptList = new ArrayList<EdsActivityRpt>();
		
		//retrieve vetting avgTime
		List<EdsElapseTime> entryResultList = retrieveEdsElapseTime("d.ticket_create_date", "d.vet_date", date); 
		
		Map<String, EdsElapseTime> entryMap = new HashMap<String, EdsElapseTime>();
		for (EdsElapseTime edsWaitTimeRptDtl : entryResultList) {
			entryMap.put(edsWaitTimeRptDtl.getTimeRange(), edsWaitTimeRptDtl);
		}
		
		//retrieve pick avgTime
		List<EdsElapseTime> pickResultList = retrieveEdsElapseTime("d.create_date","d.pick_date", date);
		
		Map<String, EdsElapseTime> pickMap = new HashMap<String, EdsElapseTime>();
		for (EdsElapseTime edsWaitTimeRptDtl : pickResultList) {
			pickMap.put(edsWaitTimeRptDtl.getTimeRange(), edsWaitTimeRptDtl);
		}
		
		//retrieve assemble avgTime
		List<EdsElapseTime> assembleResultList = retrieveEdsElapseTime("d.create_date","d.assemble_date", date);
		
		Map<String, EdsElapseTime> assembleMap = new HashMap<String, EdsElapseTime>();
		for (EdsElapseTime edsWaitTimeRptDtl : assembleResultList) {
			assembleMap.put(edsWaitTimeRptDtl.getTimeRange(), edsWaitTimeRptDtl);
		}
		
		//retrieve checking avgTime
		List<EdsElapseTime> checkResultList = retrieveEdsElapseTime("d.create_date","d.check_date", date);
		
		Map<String, EdsElapseTime> checkMap = new HashMap<String, EdsElapseTime>();
		for (EdsElapseTime edsWaitTimeRptDtl : checkResultList) {
			checkMap.put(edsWaitTimeRptDtl.getTimeRange(), edsWaitTimeRptDtl);
		}
		
		//retrieve issue avtTime
		List<EdsElapseTime> issueResultList = retrieveEdsElapseTime("d.create_date","d.issue_date", date);
		
		Map<String, EdsElapseTime> issueMap = new HashMap<String, EdsElapseTime>();
		for (EdsElapseTime edsWaitTimeRptDtl : issueResultList) {
			issueMap.put(edsWaitTimeRptDtl.getTimeRange(), edsWaitTimeRptDtl);
		}
		
		//construct rptList
		List <EdsActivityRptDtl> edsActivityRptDtlList = new ArrayList<EdsActivityRptDtl>();
		int openingHour = REPORT_WORKSTORE_OPENINGHOUR.get();
		int closingHour = REPORT_WORKSTORE_CLOSINGHOUR.get();

		for(int i = openingHour; i < closingHour; i++) {
			EdsActivityRptDtl edsActivityRptDtl = new EdsActivityRptDtl();
			String startTime = StringUtils.leftPad(String.valueOf(i),2,'0');
			String endTime = StringUtils.leftPad(String.valueOf(i+1),2,'0');
			
			edsActivityRptDtl.setTimeRange(startTime + "-" + endTime);
			
			if (entryMap.containsKey(startTime)) {
				edsActivityRptDtl.setDataEntryAvgTime( entryMap.get(startTime).getAvgTime() );
			} else {
				edsActivityRptDtl.setDataEntryAvgTime(0);
			}

			if (pickMap.containsKey(startTime) && !PICKING_AUTO_ENABLED.get()) {
				edsActivityRptDtl.setPickAvgTime( pickMap.get(startTime).getAvgTime() );
			} else {
				edsActivityRptDtl.setPickAvgTime(0);
			}
			
			if (assembleMap.containsKey(startTime)) {
				edsActivityRptDtl.setAssembleAvgTime( assembleMap.get(startTime).getAvgTime() );
			} else {
				edsActivityRptDtl.setAssembleAvgTime(0);
			}
			
			if (checkMap.containsKey(startTime)) {
				edsActivityRptDtl.setCheckAvgTime( checkMap.get(startTime).getAvgTime() );
			} else {
				edsActivityRptDtl.setCheckAvgTime(0);
			}
			
			if (issueMap.containsKey(startTime)) {
				edsActivityRptDtl.setIssueAvgTime( issueMap.get(startTime).getAvgTime() );
			} else {
				edsActivityRptDtl.setIssueAvgTime(0);
			}

			edsActivityRptDtlList.add(edsActivityRptDtl);
		}
		
		EdsActivityRpt edsActivityRpt = new EdsActivityRpt();
		edsActivityRpt.setEdsActivityRptDtlList(edsActivityRptDtlList);
		
		EdsActivityRptHdr edsActivityRptHdr = new EdsActivityRptHdr();
		
		OperationProfile op = em.find(OperationProfile.class, workstore.getOperationProfileId());
		edsActivityRptHdr.setActiveProfileName(op.getName());
		edsActivityRptHdr.setHospCode(workstore.getHospCode());
		edsActivityRptHdr.setWorkstoreCode(workstore.getWorkstoreCode());
		edsActivityRptHdr.setDate(date);
		edsActivityRpt.setEdsActivityRptHdr(edsActivityRptHdr);
		
		edsActivityRptList.add(edsActivityRpt);
		
		return edsActivityRptList;
	}

	public void generateEdsActivityRpt() {
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		if ("EdsActivityChart".equals(currentRpt)) {
			parameters.put("_PNG_ZOOM_RATIO", Float.valueOf(1f));
			printAgent.renderAndRedirect(new RenderJob(
					currentRpt + "Preview", 
					new PrintOption(), 
					parameters, 
					reportObj.get(0).getEdsActivityRptDtlList()));
		} else {
			printAgent.renderAndRedirect(new RenderJob(
					currentRpt, 
					new PrintOption(), 
					parameters, 
					reportObj));
		}
	}
	
	public void printEdsActivityRpt(){
		
		printAgent.renderAndPrint(new RenderAndPrintJob(
				currentRpt, 
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				new HashMap<String, Object>(), 
				reportObj));
	}

	@Remove
	public void destroy() {
		if(edsActivityRptList!=null){
			edsActivityRptList=null;
		}
	}

	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}

	
}
