package hk.org.ha.model.pms.biz.delivery;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.pivas.MaterialRequestItem;

import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("materialRequestItemListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MaterialRequestItemListServiceBean implements MaterialRequestItemListServiceLocal {

	@In
	private DueOrderPrintManagerLocal dueOrderPrintManager;
	
	@In
	private Workstore workstore;
	
	
    public MaterialRequestItemListServiceBean() {
    }
    
    @Override
	public List<MaterialRequestItem> retrieveMaterialRequestItemList() {
		
    	Date now = new Date();
    	Date startDate = MedProfileUtils.getOverdueStartDate(now);
		
    	return dueOrderPrintManager.retrieveMaterialRequestItemList(workstore, startDate, now);
    }
    
    @Remove
	@Override
	public void destroy() {
	}
}
