package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.WardFilter;
import hk.org.ha.model.pms.vo.report.WardFilterRpt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateless
@Name("wardFilterManager")
@Restrict("#{identity.loggedIn}")
@MeasureCalls
public class WardFilterManagerBean implements WardFilterManagerLocal {

	@PersistenceContext
	private EntityManager em;
	
	@In
	private Workstore workstore;
	
	@SuppressWarnings("unchecked")
	public List<WardFilter> retrieveWardFilterList() {
		String hospCode = workstore.getHospCode();
		
		List<WardFilter> wardFilterList = em.createQuery(
				"select o from WardFilter o" + // 20120312 index check : Workstore.hospital : FK_WORKSTORE_01
				" where o.workstore.hospCode = :hospCode" +
				" order by o.createDate, o.id")
				.setParameter("hospCode", hospCode)
				.setHint(QueryHints.FETCH, "o.workstore.workstoreGroup")
				.getResultList();
		
		Collections.sort(wardFilterList);
		
		return wardFilterList;
	}
	
	public void updateWardFilterList(List<WardFilter> newWardFilterList) {
		Map<Long, WardFilter> wardFilterMap = new HashMap<Long, WardFilter>();
		List<WardFilter> orgWardFilterList = retrieveWardFilterList();
		
		for(WardFilter wardFilter : orgWardFilterList){
			wardFilterMap.put(wardFilter.getId(), wardFilter);
		}
		
		for(WardFilter wardFilter : newWardFilterList){
			if ( wardFilter.isNew() ) {
				em.persist(wardFilter);
			} else {
				em.merge(wardFilter);
				wardFilterMap.remove(wardFilter.getId());
			}			
		}
		
		for(WardFilter wardFilter : wardFilterMap.values()){
			em.remove(wardFilter);
		}
		
		em.flush();		
		
	}
	
	@SuppressWarnings("unchecked")
	public List<WardFilterRpt> genWardFilterRptList() {
		List<WardFilter> wardFilterList = em.createQuery(
				"select o from WardFilter o" + // 20120312 index check : Workstore.hospital : FK_WORKSTORE_01
				" where o.workstore.hospCode = :hospCode" +
				" order by o.createDate, o.id")
				.setParameter("hospCode", workstore.getHospCode())
				.setHint(QueryHints.FETCH, "o.workstore")
				.getResultList();
		
		Collections.sort(wardFilterList);
		
		List<WardFilterRpt> wardFilterRptList = new ArrayList<WardFilterRpt>();
		
		for(WardFilter wardFilter : wardFilterList){
			WardFilterRpt wardFilterRpt = new WardFilterRpt();
			wardFilterRpt.setWorkstore( wardFilter.getWorkstore().getWorkstoreCode() );
			wardFilterRpt.setMonFlag( wardFilter.getMonFlag()?"Y":"N" );
			wardFilterRpt.setTueFlag( wardFilter.getTueFlag()?"Y":"N" );
			wardFilterRpt.setWedFlag( wardFilter.getWedFlag()?"Y":"N" );
			wardFilterRpt.setThuFlag( wardFilter.getThuFlag()?"Y":"N" );
			wardFilterRpt.setFriFlag( wardFilter.getFriFlag()?"Y":"N" );
			wardFilterRpt.setSatFlag( wardFilter.getSatFlag()?"Y":"N" );
			wardFilterRpt.setSunFlag( wardFilter.getSunFlag()?"Y":"N" );
			wardFilterRpt.setStartTime( wardFilter.getStartTime() );
			wardFilterRpt.setEndTime( wardFilter.getEndTime() );
			wardFilterRpt.setWardCode( wardFilter.getWardCode());
			wardFilterRpt.setUpdateUser( wardFilter.getUpdateUser() );
			wardFilterRpt.setUpdateDate( wardFilter.getUpdateDate() );
			wardFilterRpt.setCreateUser( wardFilter.getCreateUser() );
			wardFilterRpt.setCreateDate( wardFilter.getCreateDate() );
			wardFilterRptList.add( wardFilterRpt );
		}
		
		return wardFilterRptList;
	}
}
