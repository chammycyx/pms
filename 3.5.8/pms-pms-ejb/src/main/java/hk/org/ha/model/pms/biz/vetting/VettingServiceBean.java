package hk.org.ha.model.pms.biz.vetting;

import static hk.org.ha.model.pms.prop.Prop.ONESTOP_SUSPENDORDERCOUNT_DURATION_LIMIT;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.onestop.OneStopCountManagerBean;
import hk.org.ha.model.pms.biz.onestop.OneStopCountManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.onestop.MoStatusCount;
import hk.org.ha.model.pms.vo.security.PropMap;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("vettingService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class VettingServiceBean implements VettingServiceLocal {

	@In
	private OneStopCountManagerLocal oneStopCountManager;
	
	@In
	private Workstore workstore;
	
	@Out(required = false)
	private MoStatusCount moStatusCount;
	
	@In
	private VettingManagerLocal vettingManager;
	
	@In
	private PropMap propMap;
	
	public void unvetOrder(Long medOrderId) {
		vettingManager.unvetOrder(medOrderId);
		if (oneStopCountManager.isCounterTimeOut(moStatusCount))
		{
			oneStopCountManager.retrieveUnvetAndSuspendCount(workstore, propMap.getValueAsInteger(ONESTOP_SUSPENDORDERCOUNT_DURATION_LIMIT.getName()));
		}
		
		moStatusCount = OneStopCountManagerBean.getMoStatusCountByWorkstore(workstore);
	}
	
	@Remove
	public void destroy() {
		if (moStatusCount != null)
		{
			moStatusCount = null;
		}
	}
}
