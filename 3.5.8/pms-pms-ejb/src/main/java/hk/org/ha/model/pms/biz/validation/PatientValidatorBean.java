package hk.org.ha.model.pms.biz.validation;

import static hk.org.ha.model.pms.prop.Prop.ONESTOP_PATIENT_UNKNOWN_VALUE;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.udt.validation.SearchTextType;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.ejb.Remove;
import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("patientValidator")
@MeasureCalls
public class PatientValidatorBean implements PatientValidatorLocal {
	
	private static final String HKID_CHECK_SUM[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "0"};

	private static final Pattern hkidPattern = Pattern.compile("^[A-Za-z][A-Za-z]?[0-9][0-9][0-9][0-9][0-9][0-9].$");
	private static final Pattern caseNumPattern1 = Pattern.compile("^[A-Za-z][A-Za-z][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][A-Za-z0-9]{0,2}$");
	private static final Pattern caseNumPattern2 = Pattern.compile("^[A-Za-z][A-Za-z][A-Za-z0-9 ]{2}[0-9][0-9][0-9][0-9][0-9][0-9][0-9][A-Za-z0-9]?$");
	private static final Pattern caseNumWithDhpPattern = Pattern.compile("^DHP|CIM[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$");
	private static final Pattern capdVoucherNumPattern = Pattern.compile("^[A-Za-z][A-Za-z][A-Za-z ][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$");
		
	private Map<String, Pattern> specialPatientPatternMap = new HashMap<String, Pattern>();
	
	public Pattern getSpecialPatientPattern() {
		String patientUnknownValue = ONESTOP_PATIENT_UNKNOWN_VALUE.get();
		Pattern specialPatientPattern = specialPatientPatternMap.get(patientUnknownValue);
		if (specialPatientPattern == null) {
			specialPatientPattern = Pattern.compile("\\" + patientUnknownValue);
			specialPatientPatternMap.put(patientUnknownValue, specialPatientPattern);
		}
		return specialPatientPattern;
	}
	
	public SearchTextType checkSearchType(String searchText) {
		if (isSpecialPatientPattern(searchText)) {
			return SearchTextType.ManualPatient;
		} else if (isHkid(searchText)) {
			return checkHkidDigit(searchText);
		} else if (isCaseNum(searchText)) {
			return SearchTextType.Case;
		} else if (isCapd(searchText)) {
			return SearchTextType.Capd;
		} else {
			return SearchTextType.Unknown;
		}
	}
	
	private Boolean isSpecialPatientPattern(String searchText) {
		if (searchText.length() == 1 && this.getSpecialPatientPattern().matcher(searchText).find()) {
			return true;
		}
		
		return false;
	}
		
	private Boolean isHkid(String hkid) {
		if ((hkid.length() == 8 || hkid.length() == 9) && hkidPattern.matcher(hkid).find()) {
			return true;
		} 
			
		return false;	
	}
	
	private SearchTextType checkHkidDigit(String searchHkid) {
		int digitSum;
		int checkSum;
		
		String hkid = StringUtils.leftPad(searchHkid, 9);
		
		if (" ".equals(hkid.substring(0,1))) {
			digitSum = 36 * 9;
		} else {
			digitSum = ((int) hkid.charAt(0) - (int) 'A' + 10) * 9;
		}
		
		if (" ".equals(hkid.substring(1,2))) {
			digitSum = 36 * 8;
		} else {
			digitSum += ((int) hkid.charAt(1) - (int) 'A' + 10) * 8;
			digitSum += ((int) hkid.charAt(2) - (int) '0') * 7;
			digitSum += ((int) hkid.charAt(3) - (int) '0') * 6;
			digitSum += ((int) hkid.charAt(4) - (int) '0') * 5;
			digitSum += ((int) hkid.charAt(5) - (int) '0') * 4;
			digitSum += ((int) hkid.charAt(6) - (int) '0') * 3;
			digitSum += ((int) hkid.charAt(7) - (int) '0') * 2;
		}
				
		checkSum = 11 - (digitSum % 11);
				
		if (hkid.substring(hkid.length()-1).equals(HKID_CHECK_SUM[checkSum-1])) {
			return SearchTextType.Hkid;
		}
			
		return SearchTextType.HkidFail;
	}
	
	private Boolean isCaseNum(String caseNum) {
		if (caseNum.length() == 11 && (caseNumPattern1.matcher(caseNum).find() || caseNumWithDhpPattern.matcher(caseNum).find())) {
			return true;
		}
		
		if (caseNum.length() == 12 && caseNumPattern2.matcher(caseNum).find()) {
			return true;
		} 
		
		return false;
	}
	
	private Boolean isCapd(String capdVoucherNum) {
		if (capdVoucherNum.length() == 11 && capdVoucherNumPattern.matcher(capdVoucherNum).find()) {
			return true;
		}				
		return false;
	}
	
	@Remove
	public void destroy() {
		
	}
}