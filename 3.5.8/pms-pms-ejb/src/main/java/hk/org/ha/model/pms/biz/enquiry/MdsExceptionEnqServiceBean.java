package hk.org.ha.model.pms.biz.enquiry;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.medprofile.DeliveryRequestStatus;
import hk.org.ha.model.pms.udt.medprofile.ErrorLogType;
import hk.org.ha.model.pms.vo.enquiry.DeliveryRequestInfo;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.joda.time.DateTime;

@Stateful
@Scope(ScopeType.SESSION)
@Name("mdsExceptionEnqService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MdsExceptionEnqServiceBean implements MdsExceptionEnqServiceLocal {

	private static final String DELIVERY_REQUEST_INFO_CLASS_NAME = DeliveryRequestInfo.class.getName();
	
	@PersistenceContext
	private EntityManager em;
	
	@In 
	private Workstore workstore;
	
	private static final Integer pastDay = Integer.valueOf(3);
	
	public List<DeliveryRequestInfo> retrieveDeliveryRequestInfoList() {
		return retrieveDeliveryRequestInfoListByPastDay(pastDay);
	}
	
	@SuppressWarnings("unchecked")
	private List<DeliveryRequestInfo> retrieveDeliveryRequestInfoListByPastDay(Integer pastDay) {
		
		DateTime dt = new DateTime(new Date());
		Date startDate = dt.minusDays(pastDay.intValue()).toDate();
		Date endDate = dt.plusDays(1).toDate();

		List<DeliveryRequestInfo> deliveryRequestInfoList = em.createQuery(
				"select new " + DELIVERY_REQUEST_INFO_CLASS_NAME + // 20120307 index check : MedProfileErrorLog.deliveryRequestId : FK_MED_PROFILE_ERROR_LOG_01
				"  (d.id, e.createDate, d.wardCodeCsv, d.caseNum)" +
				" from MedProfileErrorLog e, DeliveryRequest d" +
				" where e.deliveryRequestId = d.id" +
				" and d.status = :completedStatus" +
				" and d.workstore = :workstore" +
				" and e.createDate between :startDate and :endDate" +
				" and e.type = :errorLogType" +
				" order by e.createDate desc")
				.setParameter("completedStatus", DeliveryRequestStatus.Completed)
				.setParameter("workstore", workstore)
				.setParameter("startDate", startDate, TemporalType.DATE)
				.setParameter("endDate", endDate, TemporalType.DATE)
				.setParameter("errorLogType", ErrorLogType.MdsAlert)
				.getResultList();

		Map<Long, DeliveryRequestInfo> deliveryRequestInfoMap = new LinkedHashMap<Long, DeliveryRequestInfo>();
		for (DeliveryRequestInfo deliveryRequestInfo : deliveryRequestInfoList) {
			deliveryRequestInfoMap.put(deliveryRequestInfo.getDeliveryRequestId(), deliveryRequestInfo);
		}
		
		return new ArrayList<DeliveryRequestInfo>(deliveryRequestInfoMap.values());
	}

	@Remove
	public void destroy() {
	}

}
