package hk.org.ha.model.pms.vo.label;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ChestLabelList")
@ExternalizedBean(type=DefaultExternalizer.class)
public class ChestLabelList implements Serializable {
 
	private static final long serialVersionUID = 1L;
	
	@XmlElement(name="ChestLabelList", required = true)	
	private List<ChestLabel> chestLabels;

	public void setChestLabels(List<ChestLabel> chestLabels) {
		this.chestLabels = chestLabels;
	}

	public List<ChestLabel> getChestLabels() {
		return chestLabels;
	}

}