package hk.org.ha.model.pms.vo.alert.mds;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class AdrLog implements Serializable {

	private static final long serialVersionUID = 1L;

	private String adr;

	private String hiclSeqNum;

	private List<String> hicSeqNumList;

	public String getAdr() {
		return adr;
	}

	public void setAdr(String adr) {
		this.adr = adr;
	}

	public String getHiclSeqNum() {
		return hiclSeqNum;
	}

	public void setHiclSeqNum(String hiclSeqNum) {
		this.hiclSeqNum = hiclSeqNum;
	}

	public List<String> getHicSeqNumList() {
		return hicSeqNumList;
	}

	public void setHicSeqNumList(List<String> hicSeqNumList) {
		this.hicSeqNumList = hicSeqNumList;
	}

}
