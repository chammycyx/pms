package hk.org.ha.model.pms.biz.onestop;

import static hk.org.ha.model.pms.prop.Prop.CDDH_LEGACY_PERSISTENCE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_LARGELAYOUT_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.MACHINE_IQDS_ENABLED;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.charging.DrugChargeClearanceManagerLocal;
import hk.org.ha.model.pms.biz.checkissue.IqdsSenderLocal;
import hk.org.ha.model.pms.biz.order.DispOrderManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.udt.charging.ClearanceStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.InvoiceDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.PharmOrderAdminStatus;
import hk.org.ha.model.pms.udt.machine.IqdsAction;
import hk.org.ha.model.pms.vo.charging.FcsPaymentStatus;
import hk.org.ha.model.pms.vo.charging.SfiForceProceedInfo;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.joda.time.DateTime;

@AutoCreate
@Stateless
@Name("suspendPrescManager")
@MeasureCalls
public class SuspendPrescManagerBean implements SuspendPrescManagerLocal {

	private final static String UNCOLLECT = "UC";
	
	@PersistenceContext
	private EntityManager em; 
	
	@In
	private DispOrderManagerLocal dispOrderManager;
		
	@In
	private DrugChargeClearanceManagerLocal drugChargeClearanceManager;

	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
		
	@In
	private IqdsSenderLocal iqdsSender;
	
	@In
	private UamInfo uamInfo;
	
	public void verifySuspendPrescription(Long dispOrderId, Long dispOrderVersion, String suspendCode, String caller) {		
		DispOrder dispOrder = em.find(DispOrder.class, dispOrderId);		
		
		if (!"oneStopEntry".equals(caller) && !dispOrder.getVersion().equals(dispOrderVersion))
		{
			throw new OptimisticLockException();
		}
				
		suspendPrescription(dispOrder, suspendCode, true);
	}
	
	public ReversePrescriptionSuspendResult reversePrescriptionSuspend(Long dispOrderId, Long dispOrderVersion, String caller) {
		ReversePrescriptionSuspendResult result = new ReversePrescriptionSuspendResult();
		DispOrder dispOrder = em.find(DispOrder.class, dispOrderId);
		
		if (!"oneStopEntry".equals(caller) && !dispOrder.getVersion().equals(dispOrderVersion)) {
				throw new OptimisticLockException();
		}
				
		if (dispOrder.getSfiFlag()) {
			for (Invoice invoice : dispOrder.getInvoiceList()) {
				if (invoice.getDocType().equals(InvoiceDocType.Sfi)){					
					result.setFcsPaymentStatus(drugChargeClearanceManager.checkSfiDrugClearanceByInvoice(invoice));
					if (result.getFcsPaymentStatus().getClearanceStatus() == ClearanceStatus.PaymentClear || invoice.getForceProceedFlag()) {
						this.reverseSuspendAndPrintLabel(dispOrder, caller);
						result.setSfiForceProceed(true);
						break;
					}
				}
			}			
			return result;
		}
		
		this.reverseSuspendAndPrintLabel(dispOrder, caller);		
		return result;
	}
	
	
	public void suspendPrescription(DispOrder dispOrder, String suspendCode, boolean removeIqdsTicketNum) {		
		dispOrder.setAdminStatus(DispOrderAdminStatus.Suspended);
		dispOrder.setSuspendCode(suspendCode);
		markRevokeFlagForUncollectOrder(dispOrder);
		
		em.flush();
		
		if (dispOrder.getStatus() == DispOrderStatus.Issued) {
			corpPmsServiceProxy.updateDispOrderAdminStatus(dispOrder.getId(), DispOrderAdminStatus.Suspended, dispOrder.getIssueDate(), 
					CDDH_LEGACY_PERSISTENCE_ENABLED.get(false), CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED.get(false), null);
		}
		
		if ( removeIqdsTicketNum && 
				(dispOrder.getStatus() == DispOrderStatus.Checked || dispOrder.getStatus() == DispOrderStatus.Issued) &&
				dispOrder.getTicket().getTicketDate().compareTo((new DateTime()).toDateMidnight().toDate()) == 0 &&
				MACHINE_IQDS_ENABLED.get() ) {
			
			Workstore ticketWk = dispOrder.getTicket().getWorkstore();
			String hospCode = ticketWk == null ? null: ticketWk.getHospCode();
			String workstoreCode = ticketWk == null ? null: ticketWk.getWorkstoreCode();
			
			iqdsSender.sendTicketToIqds(hospCode, workstoreCode, dispOrder.getTicket().getTicketNum(), dispOrder.getIssueWindowNum(), IqdsAction.RemoveTicketNum);
		}
	}
	
	public void reverseSuspendAndPrintLabel(DispOrder dispOrder, String caller) {		
		reverseSuspendByDispOrder(dispOrder, caller, true, true);
		dispOrderManager.printDispOrder(dispOrder, true);
	}
	
	public void reverseSuspendByDispOrder(DispOrder dispOrder, String caller, boolean updateCorp, boolean updateLargeLayoutFlag) {
		dispOrder.setAdminStatus(DispOrderAdminStatus.Normal);
				
		markRevokeFlagForUncollectOrder(dispOrder);
		
		if (dispOrder.getStatus() == DispOrderStatus.Issued) {
			dispOrder.setIssueDate(new Date());
		}
		
		Map<Long, Boolean> largeLayoutFlagMap = null;
		if (updateLargeLayoutFlag) {
			// set large layout flag for label reprint and CDDH label preview (for PMS mode)
			// (note: since the flag is needed to sync to CORP for CDDH label preview, it is set here instead of in print disp order function)
			largeLayoutFlagMap = dispOrderManager.updateDispOrderLargeLayoutFlag(dispOrder);
		}
		
		em.flush();
		
		if ( updateCorp && dispOrder.getStatus() == DispOrderStatus.Issued) {
			corpPmsServiceProxy.updateDispOrderAdminStatus(dispOrder.getId(), DispOrderAdminStatus.Normal, dispOrder.getIssueDate(),
					CDDH_LEGACY_PERSISTENCE_ENABLED.get(false), CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED.get(false), largeLayoutFlagMap);
		}
	}
	
	public void sfiPrescriptionSuspendDeferProceed(Long dispOrderId, String caller) {
		DispOrder dispOrder = em.find(DispOrder.class, dispOrderId);
		reverseSuspendAndPrintLabel(dispOrder, caller);
	}
	
	public void sfiPrescriptionSuspendForceProceed(Long dispOrderId, String caller, SfiForceProceedInfo sfiForceProceedInfo) {
		DispOrder dispOrder = em.find(DispOrder.class, dispOrderId);
		dispOrder.loadChild();
		Invoice sfiInvoice = null;
		
		if ( sfiForceProceedInfo != null ) {
			for (Invoice invoice : dispOrder.getInvoiceList() ) {
				if (invoice.getDocType().equals(InvoiceDocType.Sfi)){
					sfiForceProceedInfo.setInvoice(invoice);	
					sfiInvoice = invoice;
				}
			}
		}
		
		dispOrder.setAdminStatus(DispOrderAdminStatus.Normal);
		markRevokeFlagForUncollectOrder(dispOrder);
		
		if (dispOrder.getStatus() == DispOrderStatus.Issued) {
			dispOrder.setIssueDate(new Date());
		}
		
		// set large layout flag for label reprint and CDDH label preview (for PMS mode)
		// (note: since the flag is needed to sync to CORP for CDDH label preview, it is set here instead of in print disp order function)
		Map<Long, Boolean> largeLayoutFlagMap = dispOrderManager.updateDispOrderLargeLayoutFlag(dispOrder);
		
		em.flush();
		dispOrderManager.printDispOrder(dispOrder, true);
		dispOrder.getPharmOrder().clearDmInfo();
		corpPmsServiceProxy.updateAdminStatusForceProceed(dispOrder, DispOrderAdminStatus.Normal, 
				PharmOrderAdminStatus.Normal, CDDH_LEGACY_PERSISTENCE_ENABLED.get(false), sfiInvoice, CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED.get(false), largeLayoutFlagMap);
	}
	
	public static class ReversePrescriptionSuspendResult {
		private FcsPaymentStatus fcsPaymentStatus = null;
		private boolean sfiForceProceed = false;
		public FcsPaymentStatus getFcsPaymentStatus() {
			return fcsPaymentStatus;
		}
		public void setFcsPaymentStatus(FcsPaymentStatus fcsPaymentStatus) {
			this.fcsPaymentStatus = fcsPaymentStatus;
		}
		public boolean isSfiForceProceed() {
			return sfiForceProceed;
		}
		public void setSfiForceProceed(boolean sfiForceProceed) {
			this.sfiForceProceed = sfiForceProceed;
		}
	}
	
	public DispOrder markRevokeFlagForUncollectOrder(DispOrder dispOrder){
		MedOrderDocType docType = null;
		if ( dispOrder.getPharmOrder() == null || dispOrder.getPharmOrder().getMedOrder() == null ) {
			DispOrder dispOrderForUncollect = em.find(DispOrder.class, dispOrder.getId());
			docType = dispOrderForUncollect.getPharmOrder().getMedOrder().getDocType();
		} else {
			docType = dispOrder.getPharmOrder().getMedOrder().getDocType();
		}

		if( docType == MedOrderDocType.Manual || dispOrder.getRefillFlag() == Boolean.TRUE ){
			if( dispOrder.getRevokeFlag() == null || dispOrder.getRevokeFlag() == Boolean.FALSE ){
				if( dispOrder.getAdminStatus() == DispOrderAdminStatus.Suspended && UNCOLLECT.equals(dispOrder.getSuspendCode()) ){
					dispOrder.setRevokeFlag(Boolean.TRUE);
					dispOrder.setRevokeDate(new DateTime().toDate());
					dispOrder.setRevokeUser(uamInfo.getUserId());
				}
			}else if( dispOrder.getRevokeFlag() == Boolean.TRUE ){
				if( dispOrder.getAdminStatus() == DispOrderAdminStatus.Suspended && UNCOLLECT.equals(dispOrder.getSuspendCode()) ){
					//retain
				}else{
					dispOrder.setRevokeFlag(Boolean.FALSE);
					dispOrder.setRevokeDate(null);
					dispOrder.setRevokeUser(null);
				}
			}
		}else if( docType == MedOrderDocType.Normal ){
			if( dispOrder.getRevokeFlag() == null || dispOrder.getRevokeFlag() == Boolean.FALSE ){
				if( dispOrder.getPharmOrder().getAdminStatus() == PharmOrderAdminStatus.Revoke ){
					dispOrder.setRevokeFlag(Boolean.TRUE);
					dispOrder.setRevokeDate(new DateTime().toDate());
					dispOrder.setRevokeUser(uamInfo.getUserId());
				}
			}else if( dispOrder.getRevokeFlag() == Boolean.TRUE ){
				if( dispOrder.getPharmOrder().getAdminStatus() == PharmOrderAdminStatus.Revoke ){
					//retain
				}else{
					dispOrder.setRevokeFlag(Boolean.FALSE);
					dispOrder.setRevokeDate(null);
					dispOrder.setRevokeUser(null);
				}
			}
		}
		return dispOrder;
	}
}
