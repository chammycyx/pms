package hk.org.ha.model.pms.vo.printing;

public class PrintJobInfo {

	byte[] data;
	PrinterSelect printerSelect;
	String printerName;
	String printInfo;
	String printDocType;
		
	String lotNum;
	int totalItem;
	int currentItem;
	

	public PrintJobInfo(byte[] data,
			PrinterSelect printerSelect, String printerName, String printInfo, String printDocType) {
		super();
		this.data = data;
		this.printerSelect = printerSelect;
		this.printerName = printerName;
		this.printInfo = printInfo;
		this.printDocType = printDocType;
	}

	public PrintJobInfo(byte[] data,
			PrinterSelect printerSelect, String printerName, String printInfo, String printDocType, String lotNum, int totalItem, int currentItem) {
		this(data, printerSelect, printerName, printInfo);
		this.lotNum = lotNum;
		this.totalItem = totalItem;
		this.currentItem = currentItem;
	}
	
	public PrintJobInfo(byte[] data,
			PrinterSelect printerSelect, String printerName, String printInfo) {
		this(data, printerSelect, printerName, printInfo, null);
	}
	
	public byte[] getData() {
		return data;
	}
	
	public PrinterSelect getPrinterSelect() {
		return printerSelect;
	}

	public String getPrinterName() {
		return printerName;
	}

	public String getPrintInfo() {
		return printInfo;
	}

	public String getPrintDocType() {
		return printDocType;
	}

	public String getLotNum() {
		return lotNum;
	}

	public void setLotNum(String lotNum) {
		this.lotNum = lotNum;
	}

	public int getTotalItem() {
		return totalItem;
	}

	public void setTotalItem(int totalItem) {
		this.totalItem = totalItem;
	}

	public int getCurrentItem() {
		return currentItem;
	}

	public void setCurrentItem(int currentItem) {
		this.currentItem = currentItem;
	}
	
	
}
