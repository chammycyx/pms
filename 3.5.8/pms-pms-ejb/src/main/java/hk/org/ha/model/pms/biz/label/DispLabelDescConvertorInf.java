package hk.org.ha.model.pms.biz.label;

import hk.org.ha.model.pms.vo.label.ConvertDispLabelDescCriteria;

public interface DispLabelDescConvertorInf {
	void checkConvertDispLabelDescRule(ConvertDispLabelDescCriteria convertDispLabelDescCriteria);
	void destroy();
}
