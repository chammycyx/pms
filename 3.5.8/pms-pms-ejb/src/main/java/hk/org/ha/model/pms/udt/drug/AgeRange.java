package hk.org.ha.model.pms.udt.drug;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum AgeRange implements StringValuedEnum {

	Adult("U", "Adult"),
	Pediatric("O", "Paed."),
	All("A", "All");
	
	
    private final String dataValue;
    private final String displayValue;
        
    AgeRange(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static AgeRange dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(AgeRange.class, dataValue);
	}
        
	public static class Converter extends StringValuedEnumConverter<AgeRange> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<AgeRange> getEnumClass() {
    		return AgeRange.class;
    	}
    }
}
