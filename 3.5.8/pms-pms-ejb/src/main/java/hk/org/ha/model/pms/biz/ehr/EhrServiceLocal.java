package hk.org.ha.model.pms.biz.ehr;

import hk.org.ha.model.pms.vo.ehr.EhrPatient;
import hk.org.ha.model.pms.vo.ehr.EhrssViewerUrl;

import javax.ejb.Local;

@Local
public interface EhrServiceLocal {
	
	EhrssViewerUrl retrieveEhrssViewerUrl(EhrPatient ehrPatient);
	
	void destroy();
	
}