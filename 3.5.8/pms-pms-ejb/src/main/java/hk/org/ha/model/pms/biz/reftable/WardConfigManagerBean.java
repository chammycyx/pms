package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
import hk.org.ha.model.pms.persistence.reftable.WardConfig;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("wardConfigManager")
@MeasureCalls
public class WardConfigManagerBean implements WardConfigManagerLocal {

	@PersistenceContext
	private EntityManager em;
	
    public WardConfigManagerBean() {
    }
    
    @SuppressWarnings("unchecked")
	@Override
    public WardConfig retrieveWardConfig(WorkstoreGroup workstoreGroup, String wardCode) {
    	
    	List<WardConfig> wardConfigList = em.createQuery(
    			"select c from WardConfig c" + // 20120831 index check : WardConfig.workstoreGroup : FK_WARD_CONFIG_01
    			" where c.workstoreGroup = :workstoreGroup" +
    			" and c.wardCode = :wardCode")
    			.setParameter("workstoreGroup", workstoreGroup)
    			.setParameter("wardCode", wardCode)
    			.getResultList();
    	
    	return wardConfigList.isEmpty() ? null : wardConfigList.get(0);
    }
    
    
    @SuppressWarnings("unchecked")
	public List<WardConfig> retrieveMpWardConfigList(WorkstoreGroup workstoreGroup) {
    	return em.createQuery(
    			"select c from WardConfig c" + // 20120831 index check : WardConfig.workstoreGroup : FK_WARD_CONFIG_01
    			" where c.workstoreGroup = :workstoreGroup" +
    			" and c.mpWardFlag = true" +
    			" order by c.wardCode")
    			.setParameter("workstoreGroup", workstoreGroup)
    			.getResultList();
    }
    
    @SuppressWarnings("unchecked")
	public List<WardConfig> retrieveWardConfigList(WorkstoreGroup workstoreGroup, List<String> wardCodeList) {
    	return em.createQuery(
	    			"select c from WardConfig c" + // 20140826 index check : WardConfig.workstoreGroup, WardConfig.wardCode : UI_WARD_CONFIG_01
	    			" where c.workstoreGroup = :workstoreGroup" +
	    			" and c.wardCode in :wardCodeList")
	    			.setParameter("workstoreGroup", workstoreGroup)
	    			.setParameter("wardCodeList", wardCodeList)
	    			.getResultList();
    }
}
