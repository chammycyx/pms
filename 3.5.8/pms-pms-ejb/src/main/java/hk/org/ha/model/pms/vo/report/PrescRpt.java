package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class PrescRpt implements Serializable {

	private static final long serialVersionUID = 1L;

	private String ticketNum;
	
	private String orderType;
	
	private String caseNum;
	
	private String patName;
	
	private String patNameChi;
	
	private String hkid;
	
	private String refNum;
	
	private Date ticketDate;
	
	private List<PrescRptDtl> prescRptDtlList;

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getRefNum() {
		return refNum;
	}

	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}

	public void setPrescRptDtlList(List<PrescRptDtl> prescRptDtlList) {
		this.prescRptDtlList = prescRptDtlList;
	}

	public List<PrescRptDtl> getPrescRptDtlList() {
		return prescRptDtlList;
	}

	public void setPatNameChi(String patNameChi) {
		this.patNameChi = patNameChi;
	}

	public String getPatNameChi() {
		return patNameChi;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setTicketDate(Date ticketDate) {
		this.ticketDate = ticketDate;
	}

	public Date getTicketDate() {
		return ticketDate;
	}
}
