package hk.org.ha.model.pms.vo.report;

import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.PrnPercentage;
import hk.org.ha.model.pms.udt.vetting.RegimenDurationUnit;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class ManualPrescRpt implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String hospCode;
	
	private String workstoreCode;
	
	private String patHospCode;
	
	private String caseNum;
	
	private String hkid;
	
	private String name;
	
	private String orderNum;
	
	private Date orderDate;
	
	private MedOrderStatus medOrderStatus;
	
	private String doctorCode;
	
	private String pasSpecCode;
	
	private String pasSubSpecCode;
	
	private Long pasApptSeq;
	
	private String pasWardCode;
	
	private String specCode;
	
	private String wardCode;
	
	private String ticketNum;
	
	private Date ticketDate;
	
	private String workstationCode;
	
	private String itemCode;
	
	private String displayName;
	
	private String saltProperty;
	
	private String routeDesc;
	
	private String formDesc;
	
	private String formCode;
	
	private String strength;
	
	private BigDecimal volume;
	
	private String volumeUnit;
	
	private String extraInfo;
	
	private BigDecimal dosage;
	
	private String dosageUnit;
	
	private String dailyFreqDesc;
	
	private String supplFreqDesc;
	
	private Boolean prn;
	
	private PrnPercentage prnPercent;
	
	private String siteCode;
	
	private String supplSiteDesc;
	
	private String adminTimeDesc;
	
	private Integer duration;
	
	private RegimenDurationUnit durationUnit;
	
	private Integer dispQty;
	
	private String specialInstruction;
	
	private ActionStatus actionStatus;
	
	private String fmStatus;
	
	private Boolean singleUseFlag;
	
	private String supplierSystem;
	
	private String calciumStrength;
	
	private String fullConcentration;
	
	private boolean alertFlag;
	
	private String updateBy;
	
	private Date updateDate;
	
	private boolean multDoseFlag;
	
	private Boolean dangerDrugFlag;
	
	private Boolean externalUseFlag;
	
	private String tradeName;
	
	private String baseUnit;
	
	private Integer itemNum;
	
	private String doseBaseUnit;
	
	private String capdBaseUnit;

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public MedOrderStatus getMedOrderStatus() {
		return medOrderStatus;
	}

	public void setMedOrderStatus(MedOrderStatus medOrderStatus) {
		this.medOrderStatus = medOrderStatus;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public String getPasSpecCode() {
		return pasSpecCode;
	}

	public void setPasSpecCode(String pasSpecCode) {
		this.pasSpecCode = pasSpecCode;
	}

	public String getPasSubSpecCode() {
		return pasSubSpecCode;
	}

	public void setPasSubSpecCode(String pasSubSpecCode) {
		this.pasSubSpecCode = pasSubSpecCode;
	}

	public Long getPasApptSeq() {
		return pasApptSeq;
	}

	public void setPasApptSeq(Long pasApptSeq) {
		this.pasApptSeq = pasApptSeq;
	}

	public String getPasWardCode() {
		return pasWardCode;
	}

	public void setPasWardCode(String pasWardCode) {
		this.pasWardCode = pasWardCode;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public Date getTicketDate() {
		return ticketDate;
	}

	public void setTicketDate(Date ticketDate) {
		this.ticketDate = ticketDate;
	}

	public String getWorkstationCode() {
		return workstationCode;
	}

	public void setWorkstationCode(String workstationCode) {
		this.workstationCode = workstationCode;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getSaltProperty() {
		return saltProperty;
	}

	public void setSaltProperty(String saltProperty) {
		this.saltProperty = saltProperty;
	}

	public String getRouteDesc() {
		return routeDesc;
	}

	public void setRouteDesc(String routeDesc) {
		this.routeDesc = routeDesc;
	}

	public String getFormDesc() {
		return formDesc;
	}

	public void setFormDesc(String formDesc) {
		this.formDesc = formDesc;
	}

	public String getFormCode() {
		return formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	public String getStrength() {
		return strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public BigDecimal getVolume() {
		return volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	public String getVolumeUnit() {
		return volumeUnit;
	}

	public void setVolumeUnit(String volumeUnit) {
		this.volumeUnit = volumeUnit;
	}

	public String getExtraInfo() {
		return extraInfo;
	}

	public void setExtraInfo(String extraInfo) {
		this.extraInfo = extraInfo;
	}

	public String getDailyFreqDesc() {
		return dailyFreqDesc;
	}

	public void setDailyFreqDesc(String dailyFreqDesc) {
		this.dailyFreqDesc = dailyFreqDesc;
	}

	public String getSupplFreqDesc() {
		return supplFreqDesc;
	}

	public void setSupplFreqDesc(String supplFreqDesc) {
		this.supplFreqDesc = supplFreqDesc;
	}

	public Boolean getPrn() {
		return prn;
	}

	public void setPrn(Boolean prn) {
		this.prn = prn;
	}

	public PrnPercentage getPrnPercent() {
		return prnPercent;
	}

	public void setPrnPercent(PrnPercentage prnPercent) {
		this.prnPercent = prnPercent;
	}

	public String getSiteCode() {
		return siteCode;
	}

	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

	public String getSupplSiteDesc() {
		return supplSiteDesc;
	}

	public void setSupplSiteDesc(String supplSiteDesc) {
		this.supplSiteDesc = supplSiteDesc;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public RegimenDurationUnit getDurationUnit() {
		return durationUnit;
	}

	public void setDurationUnit(RegimenDurationUnit durationUnit) {
		this.durationUnit = durationUnit;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public String getSpecialInstruction() {
		return specialInstruction;
	}

	public void setSpecialInstruction(String specialInstruction) {
		this.specialInstruction = specialInstruction;
	}

	public ActionStatus getActionStatus() {
		return actionStatus;
	}

	public void setActionStatus(ActionStatus actionStatus) {
		this.actionStatus = actionStatus;
	}

	public String getFmStatus() {
		return fmStatus;
	}

	public void setFmStatus(String fmStatus) {
		this.fmStatus = fmStatus;
	}

	public Boolean getSingleUseFlag() {
		return singleUseFlag;
	}

	public void setSingleUseFlag(Boolean singleUseFlag) {
		this.singleUseFlag = singleUseFlag;
	}

	public String getSupplierSystem() {
		return supplierSystem;
	}

	public void setSupplierSystem(String supplierSystem) {
		this.supplierSystem = supplierSystem;
	}

	public String getCalciumStrength() {
		return calciumStrength;
	}

	public void setCalciumStrength(String calciumStrength) {
		this.calciumStrength = calciumStrength;
	}

	public String getFullConcentration() {
		return fullConcentration;
	}

	public void setFullConcentration(String fullConcentration) {
		this.fullConcentration = fullConcentration;
	}

	public boolean isAlertFlag() {
		return alertFlag;
	}

	public void setAlertFlag(boolean alertFlag) {
		this.alertFlag = alertFlag;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public boolean isMultDoseFlag() {
		return multDoseFlag;
	}

	public void setMultDoseFlag(boolean multDoseFlag) {
		this.multDoseFlag = multDoseFlag;
	}

	public Boolean getDangerDrugFlag() {
		return dangerDrugFlag;
	}

	public void setDangerDrugFlag(Boolean dangerDrugFlag) {
		this.dangerDrugFlag = dangerDrugFlag;
	}

	public String getTradeName() {
		return tradeName;
	}

	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}

	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public void setExternalUseFlag(Boolean externalUseFlag) {
		this.externalUseFlag = externalUseFlag;
	}

	public Boolean getExternalUseFlag() {
		return externalUseFlag;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDosage(BigDecimal dosage) {
		this.dosage = dosage;
	}

	public BigDecimal getDosage() {
		return dosage;
	}

	public void setDosageUnit(String dosageUnit) {
		this.dosageUnit = dosageUnit;
	}

	public String getDosageUnit() {
		return dosageUnit;
	}

	public void setDispQty(Integer dispQty) {
		this.dispQty = dispQty;
	}

	public Integer getDispQty() {
		return dispQty;
	}

	public void setDoseBaseUnit(String doseBaseUnit) {
		this.doseBaseUnit = doseBaseUnit;
	}

	public String getDoseBaseUnit() {
		return doseBaseUnit;
	}

	public void setCapdBaseUnit(String capdBaseUnit) {
		this.capdBaseUnit = capdBaseUnit;
	}

	public String getCapdBaseUnit() {
		return capdBaseUnit;
	}

	public void setAdminTimeDesc(String adminTimeDesc) {
		this.adminTimeDesc = adminTimeDesc;
	}

	public String getAdminTimeDesc() {
		return adminTimeDesc;
	}
}
