package hk.org.ha.model.pms.persistence.disp;

import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedProperty;

@Entity
@Table(name = "CAPD_VOUCHER_ITEM")
public class CapdVoucherItem extends VersionEntity {

	private static final long serialVersionUID = 1L;
	
	private static final char SPACE = ' '; 

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "capdVoucherItemSeq")
	@SequenceGenerator(name = "capdVoucherItemSeq", sequenceName = "SQ_CAPD_VOUCHER_ITEM", initialValue = 100000000)
	private Long id;

	@Column(name = "QTY", nullable = false, length = 6)
	private Integer qty;
		
	@ManyToOne
	@JoinColumn(name = "CAPD_VOUCHER_ID", nullable = false)
	private CapdVoucher capdVoucher;	

	@ManyToOne
	@JoinColumn(name = "DISP_ORDER_ITEM_ID", nullable = false)
	private DispOrderItem dispOrderItem;
	
	@Column(name = "ITEM_NUM", nullable = false)
	private Integer itemNum;
	
	@Transient
	private String capdDrugDesc;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public CapdVoucher getCapdVoucher() {
		return capdVoucher;
	}

	public void setCapdVoucher(CapdVoucher capdVoucher) {
		this.capdVoucher = capdVoucher;
	}

	public DispOrderItem getDispOrderItem() {
		return dispOrderItem;
	}

	public void setDispOrderItem(DispOrderItem dispOrderItem) {
		this.dispOrderItem = dispOrderItem;
	}

	public String getDrugDesc() {

		StringBuilder sb = new StringBuilder();

		if (dispOrderItem != null) {
			
			PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
			pharmOrderItem.postLoadAll();
					
			sb.append(DmDrugCacher.instance().getDmDrug(pharmOrderItem.getItemCode()).getDrugName());

			if (StringUtils.isNotBlank(pharmOrderItem.getConnectSystem())) {
				sb.append(SPACE);
				sb.append(pharmOrderItem.getConnectSystem());
			}
		}

		this.setCapdDrugDesc(sb.toString());
		return sb.toString();
	}

	public String getCapdDrugDesc() {
		return capdDrugDesc;
	}
	
	public void setCapdDrugDesc(String capdDrugDesc) {
		this.capdDrugDesc = capdDrugDesc;
	}

	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}
}
