package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class RefillCoupon implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private RefillCouponHdr refillCouponHdr;
	
	private List<RefillCouponDtl> refillCouponDtlList;

	private RefillCouponFtr refillCouponFtr;

	public RefillCouponHdr getRefillCouponHdr() {
		return refillCouponHdr;
	}

	public void setRefillCouponHdr(RefillCouponHdr refillCouponHdr) {
		this.refillCouponHdr = refillCouponHdr;
	}

	public List<RefillCouponDtl> getRefillCouponDtlList() {
		return refillCouponDtlList;
	}

	public void setRefillCouponDtlList(List<RefillCouponDtl> refillCouponDtlList) {
		this.refillCouponDtlList = refillCouponDtlList;
	}

	public RefillCouponFtr getRefillCouponFtr() {
		return refillCouponFtr;
	}

	public void setRefillCouponFtr(RefillCouponFtr refillCouponFtr) {
		this.refillCouponFtr = refillCouponFtr;
	}

}
