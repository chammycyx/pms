package hk.org.ha.model.pms.vo.printing;


import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RenderJob implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String docName;
	private PrintOption printOption;
	private Map<String, Object> parameters;
	private List<?> objectList;
	
	public RenderJob(String docName, PrintOption printOption, Map<String, Object> parameters,
			List<?> objectList) {
		super();
		this.docName = docName;
		this.printOption = printOption;
		if (parameters == null) {
			this.parameters = new HashMap<String, Object>();
		} else {
			this.parameters = parameters;
		}
		this.objectList = objectList;
	}

	public RenderJob(String docName, PrintOption printOption,
			List<?> objectList) {
		this(docName, printOption, null, objectList);
	}
	
	public String getDocName() {
		return docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	public PrintOption getPrintOption() {
		return printOption;
	}

	public void setPrintOption(PrintOption printOption) {
		this.printOption = printOption;
	}

	public Map<String, Object> getParameters() {
		return parameters;
	}

	public void setParameters(Map<String, Object> parameters) {
		this.parameters = parameters;
	}

	public List<?> getObjectList() {
		return objectList;
	}

	public void setObjectList(List<?> objectList) {
		this.objectList = objectList;
	}
}
