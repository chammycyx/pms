package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.SuspendReason;

import java.util.Collection;
import java.util.List;

import javax.ejb.Local;

@Local
public interface SuspendReasonManagerLocal {

	String retrieveSuspendReasonDesc(Workstore workstore, String suspendCode);
	
	List<SuspendReason> retrieveSuspendReasonList();
	
	void updateSuspendReasonList(Collection<SuspendReason> newSuspendList);
}
