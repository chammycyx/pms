package hk.org.ha.model.pms.biz.order;

import static hk.org.ha.model.pms.prop.Prop.LABEL_MPDISPLABEL_INACTIVEWARD_OWNSTOCKITEM_PRINTING_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.LABEL_MPDISPLABEL_INACTIVEWARD_ZEROQTYNONOWNSTOCKITEM_PRINTING_ENABLED;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.util.ObjectArray;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.alert.mds.AlertMsgManagerLocal;
import hk.org.ha.model.pms.biz.charging.InvoiceManagerLocal;
import hk.org.ha.model.pms.biz.label.DispLabelBuilderLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.biz.reftable.ItemLocationListManagerLocal;
import hk.org.ha.model.pms.biz.reftable.RefTableManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmDailyFrequencyCacherInf;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.corp.cache.DmWarningCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDailyFrequency;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmForm;
import hk.org.ha.model.pms.dms.persistence.DmMoeProperty;
import hk.org.ha.model.pms.dms.persistence.DmRoute;
import hk.org.ha.model.pms.dms.persistence.DmWarning;
import hk.org.ha.model.pms.exception.delivery.DueOrderPrintException;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.InvoiceItem;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedOrderItemAlert;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.persistence.medprofile.Delivery;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItemAlert;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.persistence.medprofile.PurchaseRequest;
import hk.org.ha.model.pms.persistence.medprofile.TpnRequest;
import hk.org.ha.model.pms.persistence.pivas.PivasPrepMethod;
import hk.org.ha.model.pms.persistence.pivas.PivasPrepMethodItem;
import hk.org.ha.model.pms.persistence.pivas.PivasWorklist;
import hk.org.ha.model.pms.persistence.reftable.ItemLocation;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.alert.mds.MpDdiOverrideReason;
import hk.org.ha.model.pms.udt.alert.mds.MpPatOverrideReason;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.InvoiceDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderPatType;
import hk.org.ha.model.pms.udt.disp.PharmOrderStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileType;
import hk.org.ha.model.pms.udt.rx.RxItemType;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.udt.vetting.RegimenDurationUnit;
import hk.org.ha.model.pms.udt.vetting.RegimenType;
import hk.org.ha.model.pms.vo.drug.DmDrugLite;
import hk.org.ha.model.pms.vo.label.BaseLabel;
import hk.org.ha.model.pms.vo.label.MpDispLabel;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.DoseFluid;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.Freq;
import hk.org.ha.model.pms.vo.rx.IvAdditive;
import hk.org.ha.model.pms.vo.rx.IvFluid;
import hk.org.ha.model.pms.vo.rx.IvRxDrug;
import hk.org.ha.model.pms.vo.rx.OralRxDrug;
import hk.org.ha.model.pms.vo.rx.Regimen;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.ejb.Stateless;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.contexts.Contexts;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;

/**
 * Session Bean implementation class DispOrderConverter
 */
@AutoCreate
@Stateless
@Name("dispOrderConverter")
@MeasureCalls
public class DispOrderConverterBean implements DispOrderConverterLocal {

	private static final String JAXB_CONTEXT_RX = "hk.org.ha.model.pms.vo.rx";
	private static final String JAXB_CONTEXT_LABEL = "hk.org.ha.model.pms.vo.label";
	
	private static final String DAILY_FREQ_CODE_AS_DIRECT = "00037";
	private static final String DAILY_FREQ_CODE_STOP = "00039";
	
	private static final String TPN_ITEM_CODE = "TPN 01";
	private static final String TPN_ITEM_DESC = "TPN Preparation";
	private static final String TPN_DAILY_FREQ_CODE = "00001";
	private static final String TPN_DAILY_FREQ_DESC = "DAILY";
	private static final String TPN_DAILY_FREQ_MULTIPLIER_TYPE = "-";
	
	private static final String[] IGNORE_PROPERTIES = new String[]{"id","version"};
	private static final char ASTERISK = '*';
	private static final char COMMA = ',';
	private static final char SPACE = ' ';
	public static final String NEW_LINE = "\n";
	
	@In(required = false)
	private UamInfo uamInfo;
	
	@In
	private ItemLocationListManagerLocal itemLocationListManager;
	
	@In
	private DmWarningCacherInf dmWarningCacher;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private DmDailyFrequencyCacherInf dmDailyFrequencyCacher; 
	
	@In
	private InvoiceManagerLocal invoiceManager;
	
	@In
	private AlertMsgManagerLocal alertMsgManager;
	
	@In
	private RefTableManagerLocal refTableManager;
	
	@In
	private DispLabelBuilderLocal dispLabelBuilder;
	
	// for corp
	public List<DispOrder> convertDeliveryToDispOrderList(Delivery delivery, boolean mpSfiInvoice, Workstore workstore) throws DueOrderPrintException {
		
		JaxbWrapper<BaseLabel> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
		
		List<DispOrder> dispOrderList = new ArrayList<DispOrder>();
		Map<MedCase, PharmOrder> pharmOrderMap = new HashMap<MedCase, PharmOrder>();
		Map<MedCase, MedOrder> medOrderMap = new HashMap<MedCase, MedOrder>();
		Map<ObjectArray, DispOrder> dispOrderMap = new HashMap<ObjectArray, DispOrder>();
		
		Ticket ticket = new Ticket();
		ticket.setTicketNum(delivery.getBatchNum());
		ticket.setTicketDate(delivery.getBatchDate());
		ticket.setOrderType(OrderType.InPatient);
		
		List<String> itemCodeList = new ArrayList<String>();
		for(DeliveryItem deliveryItem:delivery.getDeliveryItemList()) {
			try{
				Boolean isReplenishment = deliveryItem.getReplenishmentItem() != null;
				
				Boolean singleDispFlag = Boolean.FALSE;
				Boolean wardStockFlag = Boolean.FALSE;
				Integer numOfLabel = 1;
				Integer refillDurationInDays = null;
				String chargeSpecCode = null;
				Date lastDueDate = null;
				
				MedProfileMoItem medProfileMoItem = deliveryItem.getMedProfileMoItem();
				MedProfileItem medProfileItem = medProfileMoItem.getMedProfileItem();
				MedProfile medProfile = medProfileItem.getMedProfile();	
				
				if (deliveryItem.resolveMedProfilePoItem() != null) {
					MedProfilePoItem medProfilePoItem = deliveryItem.resolveMedProfilePoItem();
					singleDispFlag = medProfilePoItem.getSingleDispFlag();
					wardStockFlag = medProfilePoItem.getWardStockFlag();
					numOfLabel = medProfilePoItem.getNumOfLabel();
					refillDurationInDays = medProfilePoItem.getRefillDurationInDays();
					chargeSpecCode = medProfilePoItem.getChargeSpecCode();
					lastDueDate = medProfilePoItem.getLastDueDate();
				} else if ( deliveryItem.getPivasWorklist() != null ) {
					chargeSpecCode = medProfile.getSpecCode();
				}

				// find the DispOrder 
				MedCase medCase = deliveryItem.getMedProfile().getMedCase();
				ObjectArray dispOrderKey = null;
				if ( mpSfiInvoice ) {
					dispOrderKey = new ObjectArray(medCase, false);
				}else if( deliveryItem.getPurchaseRequest() != null ){
					dispOrderKey = new ObjectArray(medCase, deliveryItem.getPurchaseRequest().getId());
				}else {
					dispOrderKey = new ObjectArray(medCase, deliveryItem.getRefillFlag());
				}

				DispOrder dispOrder = null;
				boolean newDispOrderRequired;
				if (dispOrderMap.containsKey(dispOrderKey)) {
					dispOrder = dispOrderMap.get(dispOrderKey);
					newDispOrderRequired = false;
					
				} else {
					dispOrder = new DispOrder();
					Date now = new Date();
					dispOrder.setTicket(ticket);
					dispOrder.setTicketCreateDate(now);
					dispOrder.setTicketDate(ticket.getTicketDate());
					dispOrder.setTicketNum(ticket.getTicketNum());
					dispOrder.setOrderType(ticket.getOrderType());
					dispOrder.setDispDate(delivery.getBatchDate());
					dispOrder.setStatus(DispOrderStatus.Issued);
					dispOrder.setVetDate(now);
					dispOrder.setIssueDate(now);
					if (uamInfo == null) {
						dispOrder.setIssueUser(deliveryItem.getMedProfileMoItem().getUpdateUser());
					} else {
						dispOrder.setIssueUser(uamInfo.getUserId());
					}
					
					dispOrder.setRefillFlag(deliveryItem.getRefillFlag());
					
					dispOrderMap.put(dispOrderKey, dispOrder);
					newDispOrderRequired = true;
				}
				
				// DispOrderItem
				DispOrderItem dispOrderItem = this.convertToDispOrderItem(deliveryItem);
				itemCodeList.add(deliveryItem.getItemCode());
				if (deliveryItem.getPivasWorklist() != null) {
					PivasPrepMethodItem pivasPrepMethodItem = MedProfileUtils.getFirstItem(deliveryItem.getPivasWorklist().getPivasPrepMethod().getPivasPrepMethodItemList());
					dispOrderItem.setDispQty(pivasPrepMethodItem.getDrugDispQty());
				} else {
					dispOrderItem.setDispQty(deliveryItem.getIssueQty());
					if (isReplenishment) {
						if (wardStockFlag) {
							dispOrderItem.setReDispFlag(true);
						}
						dispOrderItem.setReplenishItemUpdateUser(deliveryItem.getReplenishmentItem().getUpdateUser());
					}
				}
				dispOrderItem.setReplenishFlag(isReplenishment);
				dispOrderItem.setStartDate(deliveryItem.getDueDate());
				DateTime endDateTime = new DateTime(deliveryItem.getDueDate()).plus(new BigDecimal(TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS)).multiply(deliveryItem.getIssueDuration()).longValue());
				dispOrderItem.setEndDate(endDateTime.toDate());
				
				// PharmOrderItem
				PharmOrderItem pharmOrderItem = this.convertToPharmOrderItem(deliveryItem, workstore);
				dispOrderItem.setPharmOrderItem(pharmOrderItem);
				
				boolean printLabelFlag = false;
				
				if (deliveryItem.getPivasWorklist() != null) {
					printLabelFlag = true;
				} else if ((dispOrderItem.getPharmOrderItem().getActionStatus() == ActionStatus.ContinueWithOwnStock &&
					(LABEL_MPDISPLABEL_INACTIVEWARD_OWNSTOCKITEM_PRINTING_ENABLED.get(workstore, Boolean.FALSE) && deliveryItem.getMedProfile().getInactivePasWardFlag())) ||
					(dispOrderItem.getPharmOrderItem().getActionStatus() != ActionStatus.ContinueWithOwnStock && 
					((LABEL_MPDISPLABEL_INACTIVEWARD_ZEROQTYNONOWNSTOCKITEM_PRINTING_ENABLED.get(workstore, Boolean.FALSE) && deliveryItem.getMedProfile().getInactivePasWardFlag()) || dispOrderItem.getDispQty().compareTo(BigDecimal.ZERO) > 0))) {
						if ( (!dispOrderItem.getPharmOrderItem().getWardStockFlag() || isReplenishment ) &&
								!dispOrderItem.getPharmOrderItem().getDangerDrugFlag()){
							printLabelFlag = true;
						} else if (dispOrderItem.getReDispFlag()) {
							printLabelFlag = true;
						}
				}
								
				if( printLabelFlag ){
					dispOrderItem.setStatus(DispOrderItemStatus.Issued);
				}else{
					dispOrderItem.setStatus(DispOrderItemStatus.KeepRecord);
				}
				
				dispOrderItem.setCreateDate(delivery.getCreateDate());
				dispOrderItem.setDispOrder(dispOrder);
				
				if (deliveryItem.getIssueDuration() != null) {
					dispOrderItem.setDurationInDay( deliveryItem.getIssueDuration().setScale(0, RoundingMode.CEILING).intValue() );	
				}
				
				if (numOfLabel > 1) {
					if ( deliveryItem.getIssueDuration() == null 
							|| deliveryItem.getIssueDuration().intValue() == 0 
							|| refillDurationInDays == null 
							|| refillDurationInDays == 0 ) {
						dispOrderItem.setTotalNumOfLabel(numOfLabel);
					}else{
						dispOrderItem.setTotalNumOfLabel(
								deliveryItem.getIssueDuration()
									.divide( BigDecimal.valueOf(refillDurationInDays), BigDecimal.ROUND_UP )
									.intValue() * numOfLabel
						);
					}
				}else{
					dispOrderItem.setTotalNumOfLabel( 1 );
				}
				
				dispOrderItem.setDeliveryItemId(deliveryItem.getId());
				dispOrderItem.setSingleDispFlag(isReplenishment ? true : singleDispFlag);
				
				dispOrder.getDispOrderItemList().add(dispOrderItem);
				
				dispOrderItem.setUrgentFlag(medProfileMoItem.getUrgentFlag() && 
					lastDueDate == null &&
					(medProfileMoItem.getFirstPrintDate() == null ||
					new DateMidnight(medProfileMoItem.getFirstPrintDate()).compareTo(new DateMidnight()) == 0 ));
	
				// MedOrderItem
				MedOrderItem medOrderItem = this.convertToMedOrderItem(medProfileItem, medProfileItem.getMedProfileMoItem());
				pharmOrderItem.setMedOrderItem(medOrderItem);
				
				ticket.setWorkstore(delivery.getDeliveryRequest().getWorkstore());
				dispOrder.setPrintLang(medProfile.getPrintLang());
				dispOrder.setSpecCode(medProfile.getSpecCode());
				dispOrder.setWardCode(medProfile.getWardCode());
				dispOrder.setPatCatCode(medProfile.getPatCatCode());
				dispOrderItem.setChargeSpecCode(chargeSpecCode);
				
				// find the PharmOrder and link to DispOrder
				PharmOrder pharmOrder = null;
				PurchaseRequest purchaseRequest = deliveryItem.getPurchaseRequest();
				if( isPasPayCodeChanged(purchaseRequest, medProfile.getMedCase()) ){
					MedCase profileMedCase = (MedCase)org.apache.commons.beanutils.BeanUtils.cloneBean(medProfile.getMedCase());
					profileMedCase.setId(null);
					profileMedCase.setPasPatGroupCode(purchaseRequest.getPasPatGroupCode());
					profileMedCase.setPasPayCode(purchaseRequest.getPasPayCode());
					
					pharmOrder = convertToPharmOrder(medProfile, profileMedCase);
					pharmOrder.setTicketDate(delivery.getBatchDate());
					pharmOrder.setOrderDate(delivery.getBatchDate());
					
				}else if (pharmOrderMap.containsKey(medProfile.getMedCase())) {
					pharmOrder = pharmOrderMap.get(medProfile.getMedCase());
				} else {
					pharmOrder = convertToPharmOrder(medProfile);
					pharmOrder.setTicketDate(delivery.getBatchDate());
					pharmOrder.setOrderDate(delivery.getBatchDate());
					pharmOrderMap.put(medProfile.getMedCase(), pharmOrder);
				}
				dispOrder.setPharmOrder(pharmOrder);
				pharmOrderItem.setPharmOrder(pharmOrder);
				pharmOrder.getPharmOrderItemList().add(pharmOrderItem);
				pharmOrder.setDoctorCode(medProfileItem.getMedProfileMoItem().getDoctorCode());
				pharmOrder.setDoctorName(medProfileItem.getMedProfileMoItem().getDoctorName());
				
				if( delivery.getDeliveryRequest() != null ){
					dispOrder.setWorkstore( delivery.getDeliveryRequest().getWorkstore() );
					pharmOrder.setWorkstationCode( delivery.getDeliveryRequest().getWorkstationCode() );
				}
	
				// find the MedOrder and link to PharmOrder
				MedOrder medOrder = null;
				if( isPasPayCodeChanged(purchaseRequest, medProfile.getMedCase()) ){
					MedCase profileMedCase = (MedCase)org.apache.commons.beanutils.BeanUtils.cloneBean(medProfile.getMedCase());
					profileMedCase.setId(null);
					profileMedCase.setPasPatGroupCode(purchaseRequest.getPasPatGroupCode());
					profileMedCase.setPasPayCode(purchaseRequest.getPasPayCode());
					
					medOrder = convertToMedOrder(medProfile, profileMedCase);
					medOrder.setOrderDate(delivery.getBatchDate());
				}else if (medOrderMap.containsKey(medProfile.getMedCase())) {
					medOrder = medOrderMap.get(medProfile.getMedCase());
				} else {
					medOrder = convertToMedOrder(medProfile);
					medOrder.setOrderDate(delivery.getBatchDate());
					medOrderMap.put(medProfile.getMedCase(), medOrder);
				}
				dispOrder.getPharmOrder().setMedOrder(medOrder);
				medOrderItem.setMedOrder(medOrder);
				medOrder.getMedOrderItemList().add(medOrderItem);
				medOrder.setDoctorCode(medProfileItem.getMedProfileMoItem().getDoctorCode());
							
				// set all itemNum (need to set it here otherwise pharmOrder.getMedOrder() is still null)
				if ( medProfileItem.getMedProfileMoItem().getItemNum() != null ) {
					medOrderItem.setItemNum(medProfileItem.getMedProfileMoItem().getItemNum());
					medOrderItem.setOrgItemNum(medProfileItem.getMedProfileMoItem().getOrgItemNum());
					if ( medOrderItem.getItemNum().compareTo(medOrder.getMaxItemNum()) > 0 ) {
						medOrder.setMaxItemNum(medOrderItem.getItemNum());
					}
				}
				
				pharmOrderItem.setItemNum(pharmOrder.getAndIncreaseMaxItemNum());
				pharmOrderItem.setOrgItemNum(pharmOrderItem.getItemNum());			
				dispOrderItem.setItemNum(pharmOrderItem.getItemNum());
				dispOrderItem.setChargeQty(deliveryItem.getChargeQty()); //for mpSfiInvoice
				
				if( purchaseRequest != null ){
					if( purchaseRequest.getInvoiceDate() != null ){
						dispOrderItem.setOncologyFlag(purchaseRequest.getOncologyFlag());
						dispOrderItem.setLifestyleFlag(purchaseRequest.getLifestyleFlag());
					}

					Invoice invoice;
					if(Boolean.TRUE.equals(deliveryItem.getPurchaseRequest().getDirectPrintFlag()))
					{
						invoice = invoiceManager.createInvoiceForPurchaseRequest(Arrays.asList(dispOrderItem), deliveryItem.getPurchaseRequest());
					}
					else
					{
						invoice = invoiceManager.createInvoiceFromPurchaseRequestForPrint(dispOrderItem, deliveryItem.getPurchaseRequest());
					}
					
					dispOrder.addInvoice(invoice);
					dispOrder.setSfiFlag(true);
				}
				
				if ( mpSfiInvoice ) {
					pharmOrderItem.loadDmInfo();
				}
				
				// finally add in
				if (newDispOrderRequired) {
					dispOrderList.add(dispOrder);
				}
				
				if (deliveryItem.getPivasWorklist() != null) {
					dispOrderItem.setBaseLabel(dispLabelBuilder.convertToMpDispLabel(dispOrderItem));
					dispOrderItem.setLabelXml(rxJaxbWrapper.marshall(dispOrderItem.getBaseLabel()));
				}
				
			} catch (Exception e) {
				if (deliveryItem != null) {
					MedProfile medProfile = deliveryItem.getMedProfile();
					if (medProfile != null && medProfile.getMedCase() != null) {
						throw new DueOrderPrintException(e, delivery.getWardCode(), medProfile.getMedCase().getCaseNum());
					} else {
						throw new DueOrderPrintException(e, delivery.getWardCode(), "");
					}
				} else {
					throw new DueOrderPrintException(e, "", "");
				}
			}
		}
		
		if (mpSfiInvoice) {
			String chargeOrderNum = StringUtils.EMPTY;
			for ( DeliveryItem deliveryItem: delivery.getDeliveryItemList() ) {
				if ( StringUtils.isBlank(chargeOrderNum) ) {
					chargeOrderNum = deliveryItem.getMedProfile().getChargeOrderNum();
					break;
				}
			}
			DispOrder dispOrder = dispOrderList.get(0);
			//InvoiceItem
			Invoice invoice = invoiceManager.createInvoiceForMpSfiInvoice(dispOrder.getDispOrderItemList(), chargeOrderNum);
			dispOrder.addInvoice(invoice);
			dispOrder.setTicket(null);
			dispOrder.setSfiFlag(true);
			dispOrder.setStatus(DispOrderStatus.SysDeleted);
			dispOrder.getPharmOrder().setStatus(PharmOrderStatus.SysDeleted);
			dispOrder.getPharmOrder().getMedOrder().setStatus(MedOrderStatus.SysDeleted);
		}
		
		for (MedOrder medOrder : medOrderMap.values()) {
			for ( MedOrderItem medOrderItem : medOrder.getMedOrderItemList() ) { // for mpSfiInvoice(private patient) dummy record
				if ( medOrderItem.getItemNum() == null ) {
					Integer itemNum = medOrder.getAndIncreaseMaxItemNum();
					medOrderItem.setItemNum(itemNum);
					medOrderItem.setOrgItemNum(itemNum);
				}
			} 
			medOrder.updateMaxItemNum();
		}
		
		Map<String, ItemLocation> itemLocationMap = itemLocationListManager.retrieveItemLocationListByItemCodeList(itemCodeList);
		if (itemLocationMap != null) {
			for (DispOrder dispOrder:dispOrderList) {
				setBinNumForDispOrderItem(itemLocationMap, dispOrder);
			}
		}
		
		return dispOrderList;
	}
	
	private boolean isPasPayCodeChanged(PurchaseRequest purchaseRequest, MedCase medCase){
		if( purchaseRequest != null && purchaseRequest.getInvoiceDate() != null ){
			if( !StringUtils.equals(purchaseRequest.getPasPatGroupCode(), medCase.getPasPatGroupCode()) ){
				return true;
			}
			
			if( !StringUtils.equals(purchaseRequest.getPasPayCode(), medCase.getPasPayCode()) ){
				return true;
			}
		}
		return false;
	}
	
	public Invoice createInvoiceFromPurchaseRequestSnapshot(PurchaseRequest purchaseRequest, Workstore workstore) {
		DispOrder tempOrder = convertPurchaseRequestToDispOrder(purchaseRequest, workstore, true);

		tempOrder.setWorkstore(purchaseRequest.getWorkstore());
		tempOrder.getPharmOrder().getMedOrder();
		tempOrder.getDispOrderItemList().get(0).setDispQty(purchaseRequest.getIssueQty());
		tempOrder.setStatus(DispOrderStatus.Vetted);
		tempOrder.setAdminStatus(DispOrderAdminStatus.Suspended);

		Invoice tempInvoice = invoiceManager.createInvoiceFromPurchaseRequestForEnq(tempOrder.getDispOrderItemList().get(0), purchaseRequest);
		
		updateInvoiceWithPrUamInfo(tempOrder, tempInvoice, true, purchaseRequest);
		
		return tempInvoice;
		
	}
	
	private void updateInvoiceWithPrUamInfo(DispOrder tempOrder, Invoice tempInvoice, boolean isReprint, PurchaseRequest purchaseRequest)
	{
		Date now = new Date();
		
		if( isReprint ){
			tempOrder.setDispDate(purchaseRequest.getInvoiceDate());
			tempInvoice.setCreateDate(purchaseRequest.getUpdateDate());
			tempInvoice.setUpdateDate(purchaseRequest.getUpdateDate());
		}else{
			tempOrder.setDispDate(now);
			tempInvoice.setCreateDate(now);
			tempInvoice.setUpdateDate(now);
		}
		
		if( uamInfo != null ){
			tempInvoice.setUpdateUser(uamInfo.getUserId());
			tempInvoice.setCreateUser(uamInfo.getUserId());
		}
		
		for(InvoiceItem tempInvoiceItem : tempInvoice.getInvoiceItemList()){
			tempInvoiceItem.setCreateDate(now);
			tempInvoiceItem.setUpdateDate(now);
			
			if( uamInfo != null ){
				tempInvoiceItem.setUpdateUser(uamInfo.getUserId());
				tempInvoiceItem.setCreateUser(uamInfo.getUserId());
			}
		}
	}
	
	public Invoice convertMedProfileItemToInvoice(PurchaseRequest purchaseRequest, boolean isReprint, Workstore workstore){
		DispOrder tempOrder = convertPurchaseRequestToDispOrder(purchaseRequest, workstore, false);

		tempOrder.setWorkstore(purchaseRequest.getWorkstore());
		tempOrder.getPharmOrder().getMedOrder();
		tempOrder.getDispOrderItemList().get(0).setDispQty(purchaseRequest.getIssueQty());
		tempOrder.setStatus(DispOrderStatus.Vetted);
		tempOrder.setAdminStatus(DispOrderAdminStatus.Suspended);

		Invoice tempInvoice = invoiceManager.createInvoiceForPurchaseRequest(tempOrder.getDispOrderItemList(), purchaseRequest);
		
		updateInvoiceWithPrUamInfo(tempOrder, tempInvoice, isReprint, purchaseRequest);
		
		return tempInvoice;
	}
	
	public DispOrder convertMedProfileItemToDispOrder(MedProfileItem medProfileItem, MedProfileMoItem medProfileMoItem, Workstore workstore) {
		MedProfile medProfile = (MedProfile) Contexts.getSessionContext().get("medProfile");
		return convertMedProfileItemToDispOrder(medProfile, medProfileItem, medProfileMoItem, false, workstore);
	}
	
	private DispOrder convertPurchaseRequestToDispOrder(PurchaseRequest purchaseRequest, Workstore workstore, boolean forEnq){
		MedProfileMoItem medProfileMoItem = purchaseRequest.getMedProfilePoItem().getMedProfileMoItem();
		MedProfileItem medProfileItem = medProfileMoItem.getMedProfileItem();
		MedProfile medProfile = medProfileItem.getMedProfile();
		
		DispOrder dispOrder = new DispOrder();
		dispOrder.setWardCode(medProfile.getWardCode());
		dispOrder.setPatCatCode(medProfile.getPatCatCode());
		dispOrder.setSpecCode(medProfile.getSpecCode());
		dispOrder.setPrintLang(medProfile.getPrintLang());
		dispOrder.setOrderType(OrderType.InPatient);

		PharmOrder pharmOrder = convertToPharmOrder(medProfile);
		MedOrder medOrder = convertToMedOrder(medProfile);

		if(forEnq)
		{
			MedCase overrideMedCase = new MedCase();
			overrideMedCase.setCaseNum(medProfile.getMedCase().getCaseNum());
			overrideMedCase.setPasPatGroupCode(purchaseRequest.getPasPatGroupCode());
			overrideMedCase.setPasPayCode(purchaseRequest.getPasPayCode());
			pharmOrder = convertToPharmOrder(medProfile, overrideMedCase);
			medOrder = convertToMedOrder(medProfile, overrideMedCase);
			pharmOrder.setPatType(PharmOrderPatType.Public);
		}
		else
		{
			pharmOrder = convertToPharmOrder(medProfile);
			medOrder = convertToMedOrder(medProfile);
		}

		MedOrderItem medOrderItem = convertToMedOrderItem(medProfileItem, medProfileMoItem);
	
		medOrderItem.setOrgItemNum(medProfileMoItem.getOrgItemNum());

		medOrder.getMedOrderItemList().add(medOrderItem);
		
		List<String> itemCodeList = new ArrayList<String>();
		MedProfilePoItem medProfilePoItem = purchaseRequest.getMedProfilePoItem();
		PharmOrderItem pharmOrderItem = convertToPharmOrderItem(medProfilePoItem, workstore);
		pharmOrderItem.setOrgItemNum(medProfileItem.getOrgItemNum());
		pharmOrderItem.setItemNum(medProfileMoItem.getItemNum());
		pharmOrderItem.setMedOrderItem(medOrderItem);
		pharmOrderItem.setPharmOrder(pharmOrder);
		if( pharmOrderItem.getDmDrugLite() == null && pharmOrderItem.getItemCode() != null ){
			pharmOrderItem.setDmDrugLite( DmDrugLite.objValueOf(pharmOrderItem.getItemCode()));
		}
		
		pharmOrder.getPharmOrderItemList().add(pharmOrderItem);

		DispOrderItem dispOrderItem = convertToDispOrderItem(medProfilePoItem);
		dispOrderItem.setUrgentFlag(medProfileMoItem.getUrgentFlag());
		dispOrderItem.setItemNum(pharmOrderItem.getItemNum());
		if( medProfileMoItem.getCanSetLabelInstruction() ){
			dispOrderItem.setPrintMpDispLabelInstructionFlag(medProfileMoItem.getPrintInstructionFlag());
		}
		dispOrderItem.setDispOrder(dispOrder);
		dispOrderItem.setPharmOrderItem(pharmOrderItem);
		dispOrderItem.setSingleDispFlag(medProfilePoItem.getSingleDispFlag());
		dispOrder.getDispOrderItemList().add(dispOrderItem);
		itemCodeList.add(medProfilePoItem.getItemCode());
		
		Map<String, ItemLocation> itemLocationMap = itemLocationListManager.retrieveItemLocationListByItemCodeList(itemCodeList);
		if (itemLocationMap != null) {
			setBinNumForDispOrderItem(itemLocationMap, dispOrder);
		}

		pharmOrder.setMedOrder(medOrder);
		dispOrder.setPharmOrder(pharmOrder);
			
		return dispOrder;
	}
	
	private DispOrder convertMedProfileItemToDispOrder(MedProfile medProfile, MedProfileItem medProfileItem, MedProfileMoItem medProfileMoItem, Boolean forInvoicePrint, Workstore workstore) {
		DispOrder dispOrder = new DispOrder();
		dispOrder.setWardCode(medProfile.getWardCode());
		dispOrder.setPatCatCode(medProfile.getPatCatCode());
		dispOrder.setSpecCode(medProfile.getSpecCode());
		dispOrder.setPrintLang(medProfile.getPrintLang());
		dispOrder.setOrderType(OrderType.InPatient);
		
		PharmOrder pharmOrder = convertToPharmOrder(medProfile);
		MedOrder medOrder = convertToMedOrder(medProfile);
		
		MedOrderItem medOrderItem = convertToMedOrderItem(medProfileItem, medProfileMoItem);
		
		if( forInvoicePrint ){
			medOrderItem.setOrgItemNum(medProfileMoItem.getOrgItemNum());
		}
		medOrder.getMedOrderItemList().add(medOrderItem);	

		List<String> itemCodeList = new ArrayList<String>();
		for (MedProfilePoItem medProfilePoItem : medProfileMoItem.getMedProfilePoItemList()) {
			PharmOrderItem pharmOrderItem = convertToPharmOrderItem(medProfilePoItem, workstore);
			pharmOrderItem.setOrgItemNum(medProfileItem.getOrgItemNum());
			pharmOrderItem.setItemNum(medProfileMoItem.getItemNum());
			pharmOrderItem.setMedOrderItem(medOrderItem);
			pharmOrderItem.setPharmOrder(pharmOrder);
			if( forInvoicePrint && pharmOrderItem.getDmDrugLite() == null && pharmOrderItem.getItemCode() != null ){
				pharmOrderItem.setDmDrugLite(DmDrugLite.objValueOf(pharmOrderItem.getItemCode()));
			}
			
			pharmOrder.getPharmOrderItemList().add(pharmOrderItem);

			DispOrderItem dispOrderItem = convertToDispOrderItem(medProfilePoItem);
			dispOrderItem.setUrgentFlag(medProfileMoItem.getUrgentFlag());
			dispOrderItem.setItemNum(pharmOrderItem.getItemNum());
			if( medProfileMoItem.getCanSetLabelInstruction() ){
				dispOrderItem.setPrintMpDispLabelInstructionFlag(medProfileMoItem.getPrintInstructionFlag());
			}
			dispOrderItem.setDispOrder(dispOrder);
			dispOrderItem.setPharmOrderItem(pharmOrderItem);
			dispOrderItem.setSingleDispFlag(medProfilePoItem.getSingleDispFlag());
			dispOrder.getDispOrderItemList().add(dispOrderItem);
			itemCodeList.add(medProfilePoItem.getItemCode());
		}
		
		Map<String, ItemLocation> itemLocationMap = itemLocationListManager.retrieveItemLocationListByItemCodeList(itemCodeList);
		if (itemLocationMap != null) {
			setBinNumForDispOrderItem(itemLocationMap, dispOrder);
		}
		
		if( forInvoicePrint ){
			pharmOrder.setMedOrder(medOrder);
			dispOrder.setPharmOrder(pharmOrder);
		}
		return dispOrder;
	}
	
	private void setBinNumForDispOrderItem(Map<String, ItemLocation> itemLocationMap, DispOrder dispOrder) {
		for (DispOrderItem dispOrderItem:dispOrder.getDispOrderItemList()) {
			ItemLocation itemLocation = itemLocationMap.get(dispOrderItem.getPharmOrderItem().getItemCode());
			if (itemLocation != null) {
				dispOrderItem.setBinNum(itemLocation.getBinNum());
			}
		}
	}
	
	private PharmOrder convertToPharmOrder(MedProfile medProfile) {
		return this.convertToPharmOrder(medProfile, null);
	}
	
	private PharmOrder convertToPharmOrder(MedProfile medProfile, MedCase medCaseForOverride) {
		PharmOrder pharmOrder = new PharmOrder();
		if( medCaseForOverride != null ){
			pharmOrder.setMedCase(medCaseForOverride);
		}else{
			pharmOrder.setMedCase(medProfile.getMedCase());
		}
		pharmOrder.setPatient(medProfile.getPatient());
		pharmOrder.setWardCode(medProfile.getWardCode());
		pharmOrder.setPatCatCode(medProfile.getPatCatCode());
		pharmOrder.setSpecCode(medProfile.getSpecCode());
		pharmOrder.setOtherDoc(medProfile.getOtherDoc());
		if( medProfile.getPrivateFlag() != null && medProfile.getPrivateFlag() ){
			pharmOrder.setPatType(PharmOrderPatType.Private);
		}else{
			pharmOrder.setPatType(PharmOrderPatType.Public);
		}
		pharmOrder.setPrivateFlag(medProfile.getPrivateFlag());
		
		return pharmOrder;
	}
	
	private MedOrder convertToMedOrder(MedProfile medProfile, MedCase medCaseForOverride) {
		MedOrder medOrder = new MedOrder();
		medOrder.setOrderType(OrderType.InPatient);
		if( medCaseForOverride != null ){
			medOrder.setMedCase(medCaseForOverride);
		}else{
			medOrder.setMedCase(medProfile.getMedCase());
		}
		medOrder.setPatient(medProfile.getPatient());
		medOrder.setOrderNum(medProfile.getOrderNum());
		medOrder.setWardCode(medProfile.getWardCode());
		medOrder.setSpecCode(medProfile.getSpecCode());
		medOrder.setPatHospCode(medProfile.getPatHospCode());
//		medOrder.setMaxItemNum(Integer.valueOf(0));
		medOrder.setCmsUpdateDate(new Date());
		medOrder.setPrescType(MedOrderPrescType.In);
		if( medProfile.isManualProfile() ){
			medOrder.setDocType(MedOrderDocType.Manual);
		}else{
			medOrder.setDocType(MedOrderDocType.Normal);
		}
		medOrder.setIsChemoOrder(medProfile.getType() == MedProfileType.Chemo);
		return medOrder;
	}
	
	private MedOrder convertToMedOrder(MedProfile medProfile) {
		return this.convertToMedOrder(medProfile, null);
	}

	private MedOrderItem convertToMedOrderItem(MedProfileItem medProfileItem, MedProfileMoItem medProfileMoItem) {
		MedOrderItem medOrderItem = new MedOrderItem(medProfileMoItem.getRxItem());
		medOrderItem.setAlertFlag(medProfileMoItem.getAlertFlag());
		medOrderItem.setModifyFlag(medProfileMoItem.getModifyFlag());
		medOrderItem.setNumOfLabel(Integer.valueOf(1));
		
		Set<String> overrideReasonSet = new HashSet<String>();	
		StringBuilder alertDesc = new StringBuilder();
		String alertRemark = "";
		
		alertMsgManager.sortAlertEntityList(medProfileMoItem.getMedProfileMoItemAlertList());
		
		for (MedProfileMoItemAlert medProfileMoItemAlert: medProfileMoItem.getMedProfileMoItemAlertList()) {
			MedOrderItemAlert medOrderItemAlert = new MedOrderItemAlert();
			BeanUtils.copyProperties(medProfileMoItemAlert, medOrderItemAlert, IGNORE_PROPERTIES);
			medOrderItem.addMedOrderItemAlert(medOrderItemAlert);
			for (String medProfileOverrideReason:medProfileMoItemAlert.getOverrideReason()){
				if ( medProfileOverrideReason.charAt(medProfileOverrideReason.length()-1) == ASTERISK ) {
					alertRemark = (medProfileOverrideReason.substring(0, medProfileOverrideReason.length()-1));
				} else if ( isAlertRemark(medProfileMoItemAlert, medProfileOverrideReason) ) {
					alertRemark = medProfileOverrideReason;
				} else if ( !overrideReasonSet.contains(medProfileOverrideReason) ) {
					overrideReasonSet.add(medProfileOverrideReason);
					if ( alertDesc.length() > 0 ) {
						alertDesc.append(COMMA).append(SPACE);
					}
					alertDesc.append(medProfileOverrideReason);
				}					
			}
		}
		
		medOrderItem.setAlertDesc(alertDesc.toString());
		medOrderItem.setAlertRemark(alertRemark);
		
		medOrderItem.setIsMultipleChemoItem(medProfileMoItem.getChemoInfo() != null && medProfileMoItem.getChemoInfo().getChemoItemList() != null && medProfileMoItem.getChemoInfo().getChemoItemList().size() > 1);
		
		return medOrderItem;
	}
	
	private boolean isAlertRemark(MedProfileMoItemAlert medProfileMoItemAlert, String medProfileOverrideReason){
		if ( medProfileMoItemAlert.isDdiAlert() ){
			for ( MpDdiOverrideReason mpDdiOverrideReason : MpDdiOverrideReason.mpDdiOverrideReasonList ) {
				if ( StringUtils.equals(medProfileOverrideReason, mpDdiOverrideReason.getDisplayValue())) {
					return false;
				}
			}
		} else if ( medProfileMoItemAlert.isPatAlert() ) {
			for ( MpPatOverrideReason mpPatOverrideReason : MpPatOverrideReason.mpPatOverrideReasonList ) {
				if ( StringUtils.equals(medProfileOverrideReason, mpPatOverrideReason.getDisplayValue())) {
					return false;
				}
			}
		}
		return true;
	}
	
	//For PatientDrugProfile, DrugRefillList, MpTrxRpt
	public PharmOrderItem convertToPharmOrderItemForInstructionList(MedProfilePoItem medProfilePoItem, Workstore workstore) {
		PharmOrderItem poi = convertToPharmOrderItem(medProfilePoItem, workstore);
		JaxbWrapper<Regimen> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
		Regimen regimen = rxJaxbWrapper.unmarshall(poi.getRegimenXml());
		poi.setRegimen(regimen);
		return poi;
	}
	
	private PharmOrderItem convertToPharmOrderItem(DeliveryItem deliveryItem, Workstore workstore) {
		return deliveryItem.getPivasWorklist() != null ?
				convertToPharmOrderItem(deliveryItem.getItemCode(), deliveryItem.getPivasWorklist(), workstore) :
				convertToPharmOrderItem(deliveryItem.resolveMedProfilePoItem(), workstore); 
	}
	
	private PharmOrderItem convertToPharmOrderItem(MedProfilePoItem medProfilePoItem, Workstore workstore) {
		JaxbWrapper<Regimen> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);

		PharmOrderItem pharmOrderItem = new PharmOrderItem();
		
		pharmOrderItem.setItemCode(medProfilePoItem.getItemCode());
		
		//For EPR message
		Regimen regimen = medProfilePoItem.getRegimen();
		
		if ( regimen != null ) {
			String regimenXml = rxJaxbWrapper.marshall(regimen);
			pharmOrderItem.setRegimenXml(regimenXml);
		}
		
		pharmOrderItem.setFmStatus(medProfilePoItem.getFmStatus());
		pharmOrderItem.setTradeName(medProfilePoItem.getTradeName());
		pharmOrderItem.setRouteCode(medProfilePoItem.getRouteCode());
		pharmOrderItem.setRouteDesc(medProfilePoItem.getRouteDesc());
		pharmOrderItem.setBaseUnit(medProfilePoItem.getBaseUnit());
		pharmOrderItem.setDoseQty(medProfilePoItem.getDoseQty());
		pharmOrderItem.setDoseUnit(medProfilePoItem.getDoseUnit());
		pharmOrderItem.setCalQty(medProfilePoItem.getCalQty());
		pharmOrderItem.setAdjQty(medProfilePoItem.getAdjQty());
		pharmOrderItem.setIssueQty(medProfilePoItem.getIssueQty());
		pharmOrderItem.setWarnCode1(medProfilePoItem.getWarnCode1());
		pharmOrderItem.setWarnCode2(medProfilePoItem.getWarnCode2());
		pharmOrderItem.setWarnCode3(medProfilePoItem.getWarnCode3());
		pharmOrderItem.setWarnCode4(medProfilePoItem.getWarnCode4());
		pharmOrderItem.setDrugName(medProfilePoItem.getDrugName());
		pharmOrderItem.setFormCode(medProfilePoItem.getFormCode());
		pharmOrderItem.setFormLabelDesc(medProfilePoItem.getFormLabelDesc());
		pharmOrderItem.setStrength(medProfilePoItem.getStrength());
		pharmOrderItem.setVolumeText(medProfilePoItem.getVolumeText());
		pharmOrderItem.setSynonym(refTableManager.getFdnByItemCodeOrderTypeWorkstore(medProfilePoItem.getItemCode(), OrderType.InPatient, workstore));
		pharmOrderItem.setActionStatus(medProfilePoItem.getActionStatus());
		pharmOrderItem.setDangerDrugFlag(medProfilePoItem.getDangerDrugFlag());
		pharmOrderItem.setWardStockFlag(medProfilePoItem.getWardStockFlag());
		pharmOrderItem.setRemarkText(medProfilePoItem.getRemarkText());
		
		DmDrug dmDrug = DmDrugCacher.instance().getDmDrug(medProfilePoItem.getItemCode());
		if( dmDrug != null && dmDrug.getDmDrugProperty() != null ){
			pharmOrderItem.setDupChkDisplayName(dmDrug.getDmDrugProperty().getDisplayname());
			pharmOrderItem.setDmDrugLite(DmDrugLite.objValueOf(dmDrug));
		}
		
		return pharmOrderItem;
	}
	
	private PharmOrderItem convertToPharmOrderItem(String itemCode, PivasWorklist pivasWorklist, Workstore workstore) {

		MedProfileMoItem medProfileMoItem = pivasWorklist.getMedProfileMoItem();
		PivasPrepMethod pivasPrepMethod = pivasWorklist.getPivasPrepMethod();
		DmDrug dmDrug = dmDrugCacher.getDmDrug(itemCode);
		DmForm dmForm = dmDrug.getDmForm();
		DmRoute dmRoute = dmForm.getDmRoute();
		DmMoeProperty dmMoeProperty = dmDrug.getDmMoeProperty();
		
		JaxbWrapper<Object> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
		RxItem rxItem = (RxItem) rxJaxbWrapper.unmarshall(rxJaxbWrapper.marshall(medProfileMoItem.getRxItem()));
		
		Regimen regimen = null;
		RxDrug rxDrug = null;
		ActionStatus actionStatus = null;
		
		if (medProfileMoItem.isManualItem()) {
			MedProfilePoItem medProfilePoItem = MedProfileUtils.getFirstItem(medProfileMoItem.getMedProfilePoItemList());
			if (medProfilePoItem != null) {
				actionStatus = medProfilePoItem.getActionStatus();
				regimen = convertRegimen(medProfilePoItem.getRegimen(), dmDrug);
			}
		} else if (rxItem instanceof IvRxDrug) {
			IvRxDrug ivRxDrug = (IvRxDrug) rxItem;
			rxDrug = MedProfileUtils.getFirstItem(ivRxDrug.getIvAdditiveList());
			regimen = constructRegimen(ivRxDrug, dmDrug);
		} else if (rxItem instanceof RxDrug) {
			rxDrug = (RxDrug) rxItem;
			regimen = convertRegimen(rxDrug.getRegimen(), dmDrug);
		}
		
		PharmOrderItem pharmOrderItem = new PharmOrderItem();
		if (regimen != null) {
			pharmOrderItem.setRegimenXml(rxJaxbWrapper.marshall(regimen));
		}
		pharmOrderItem.setItemCode(dmDrug.getItemCode());
		pharmOrderItem.setFmStatus(dmDrug.getPmsFmStatus().getFmStatus());
		pharmOrderItem.setBaseUnit(dmDrug.getBaseUnit());
		pharmOrderItem.setDoseQty(BigDecimal.ZERO);
		pharmOrderItem.setCalQty(BigDecimal.ZERO);
		pharmOrderItem.setAdjQty(BigDecimal.ZERO);
		pharmOrderItem.setIssueQty(BigDecimal.ZERO);
		pharmOrderItem.setWarnCode1(pivasPrepMethod.getWarnCode1());
		pharmOrderItem.setWarnCode2(pivasPrepMethod.getWarnCode2());
		pharmOrderItem.setDrugName(dmDrug.getDrugName());
		pharmOrderItem.setFormCode(dmDrug.getFormCode());
		pharmOrderItem.setFormLabelDesc(dmForm.getLabelDesc());
		pharmOrderItem.setRouteCode(rxDrug == null ? (dmRoute == null ? null : dmRoute.getRouteCode()) : rxDrug.getRouteCode());
		pharmOrderItem.setRouteDesc(rxDrug == null ? (dmRoute == null ? null : dmRoute.getRouteDesc()) : rxDrug.getRouteDesc());
		pharmOrderItem.setActionStatus(rxDrug == null ? actionStatus : rxDrug.getActionStatus());
		pharmOrderItem.setStrength(dmDrug.getStrength());
		pharmOrderItem.setVolumeText(dmDrug.getVolumeText());
		pharmOrderItem.setSynonym(refTableManager.getFdnByItemCodeOrderTypeWorkstore(dmDrug.getItemCode(), OrderType.InPatient, workstore));
		pharmOrderItem.setWardStockFlag(Boolean.FALSE);
		pharmOrderItem.setDoseUnit(dmMoeProperty == null ? null : dmMoeProperty.getDispenseDosageUnit());
		pharmOrderItem.setTradeName(dmMoeProperty == null ? null : dmMoeProperty.getTradename());
		pharmOrderItem.setDupChkDisplayName(dmDrug.getDmDrugProperty() == null ? null : dmDrug.getDmDrugProperty().getDisplayname());
		pharmOrderItem.setDmDrugLite(dmDrug.getDrugChargeInfo() == null ? null : DmDrugLite.objValueOf(dmDrug));
		
		return pharmOrderItem;
	}
	
	private Regimen convertRegimen(Regimen regimen, DmDrug dmDrug) {
		
		if (regimen == null) {
			return regimen;
		}
		
		for (DoseGroup doseGroup: regimen.getDoseGroupList()) {
			for (Dose dose: doseGroup.getDoseList()) {
				dose.setDailyFreq(constructFreq(dose.getDailyFreq()));
				determineDosageByDmDrug(dose, dmDrug);
			}
		}
		return regimen;
	}
	
	private Regimen constructRegimen(IvRxDrug ivRxDrug, DmDrug dmDrug) {
		
		Regimen regimen = new Regimen();
		regimen.setType(RegimenType.Daily);
		regimen.setDoseGroupList(Arrays.asList(constructDoseGroup(ivRxDrug, dmDrug)));
		
		return regimen;
	}
	
	private DoseGroup constructDoseGroup(IvRxDrug ivRxDrug, DmDrug dmDrug) {
		
		DoseGroup doseGroup = new DoseGroup();
		doseGroup.setDoseList(Arrays.asList(constructDose(ivRxDrug, dmDrug)));
		doseGroup.setDuration(ivRxDrug.getDuration());
		doseGroup.setDurationUnit(ivRxDrug.getDurationUnit());
		doseGroup.setStartDate(ivRxDrug.getStartDate());
		doseGroup.setEndDate(ivRxDrug.getEndDate());
		doseGroup.setValidPeriod(ivRxDrug.getValidPeriod());
		doseGroup.setValidPeriodUnit(ivRxDrug.getValidPeriodUnit());
		doseGroup.setReviewDate(ivRxDrug.getReviewDate());
		
		return doseGroup;
	}
	
	private Dose constructDose(IvRxDrug ivRxDrug, DmDrug dmDrug) {
		
		IvAdditive ivAdditive = MedProfileUtils.getFirstItem(ivRxDrug.getIvAdditiveList());
		
		Dose dose = new Dose();
		dose.setDailyFreq(constructFreq(ivRxDrug.getDailyFreq()));
		dose.setSiteCode(ivRxDrug.getSiteCode());
		dose.setSiteDesc(ivRxDrug.getSiteDesc());
		dose.setSupplSiteDesc(ivRxDrug.getSupplSiteDesc());
		dose.setAdminTimeCode(ivRxDrug.getAdminTimeCode());
		dose.setAdminTimeDesc(ivRxDrug.getAdminTimeDesc());
		dose.setBaseUnit(ivRxDrug.getBaseUnit());
		dose.setDispQty(ivRxDrug.getDispQty());
		dose.setStartAdminRate(ivRxDrug.getStartAdminRate());
		dose.setEndAdminRate(ivRxDrug.getEndAdminRate());
		dose.setDoseFluid(constructDoseFluid(MedProfileUtils.getFirstItem(ivRxDrug.getIvFluidList()))); 
		if (ivAdditive != null) {
			dose.setDosage(ivAdditive.getDosage());
			dose.setDosageEnd(ivAdditive.getDosageEnd());
			dose.setDosageUnit(ivAdditive.getDosageUnit());
		}
		determineDosageByDmDrug(dose, dmDrug);
		
		return dose;
	}
	
	private Freq constructFreq(Freq freq) {
		
		if (freq == null || StringUtils.isEmpty(freq.getCode())) {
			DmDailyFrequency dmDailyFrequency = dmDailyFrequencyCacher.getDmDailyFrequencyByDailyFreqCode(DAILY_FREQ_CODE_AS_DIRECT);
			freq = new Freq();
			freq.setCode(DAILY_FREQ_CODE_AS_DIRECT);
			freq.setDesc(dmDailyFrequency.getDailyFreqDesc());
			freq.setMultiplierType(dmDailyFrequency.getMultiplierType());
		}
		return freq;
	}
	
	private DoseFluid constructDoseFluid(IvFluid ivFluid) {
		
		if (ivFluid == null) {
			return null;
		}
		
		DoseFluid doseFluid = new DoseFluid();
		doseFluid.setFluidVolume(ivFluid.getFluidVolume());
		doseFluid.setFluidVolumeUnit(ivFluid.getFluidVolumeUnit());
		
		return doseFluid;
	}
	
	private void determineDosageByDmDrug(Dose dose, DmDrug dmDrug) {
		
		if (StringUtils.equalsIgnoreCase(dose.getDosageUnit(), dmDrug.getDmMoeProperty().getDispenseDosageUnit())) {
			return;
		}
			
		if (dose.getDosage() != null && ObjectUtils.compare(dmDrug.getDmMoeProperty().getDduToMduRatio(), 0d) > 0) {
			BigDecimal dduToMduRatio = BigDecimal.valueOf(dmDrug.getDmMoeProperty().getDduToMduRatio());
			dose.setDosage(divide(dose.getDosage(), dduToMduRatio));
			if (ObjectUtils.compare(dose.getDosageEnd(), BigDecimal.ZERO) > 0) {
				dose.setDosageEnd(divide(dose.getDosageEnd(), dduToMduRatio));
			}
		} else {
			dose.setDosage(BigDecimal.ZERO);
		}
		if (DAILY_FREQ_CODE_STOP.equals(dose.getDailyFreq().getCode())) {
			dose.setDosage(null);
		}
		dose.setDosageUnit(dmDrug.getDmMoeProperty().getDispenseDosageUnit());
	}
	
	private BigDecimal divide(BigDecimal value1, BigDecimal value2){
		if(value1 == null || value2 == null || BigDecimal.ZERO.compareTo(value2) == 0){
			return BigDecimal.ZERO;
		}
		return value1.divide(value2, 4, RoundingMode.HALF_UP);
	}	
	
	private String getWarnDesc(String warnCode) {
		String warnDesc = null;
		if (warnCode == null) {
			return null;
		}
		
		DmWarning dmWarning = dmWarningCacher.getDmWarningByWarnCode(warnCode);
		if (dmWarning != null) {
			warnDesc = dmWarning.getWarnMessageEng();
		}
		
		return warnDesc;
	}
	
	private DispOrderItem convertToDispOrderItem(DeliveryItem deliveryItem) {
		return deliveryItem.getPivasWorklist() != null ?
				convertToDispOrderItem(deliveryItem.getItemCode(), deliveryItem.getPivasWorklist()) :
				convertToDispOrderItem(deliveryItem.resolveMedProfilePoItem());
	}
	
	private DispOrderItem convertToDispOrderItem(MedProfilePoItem medProfilePoItem) {
		DispOrderItem dispOrderItem = new DispOrderItem();
		dispOrderItem.setWarnCode1(medProfilePoItem.getWarnCode1());
		dispOrderItem.setWarnCode2(medProfilePoItem.getWarnCode2());
		dispOrderItem.setWarnCode3(medProfilePoItem.getWarnCode3());
		dispOrderItem.setWarnCode4(medProfilePoItem.getWarnCode4());
		dispOrderItem.setWarnDesc1(getWarnDesc(dispOrderItem.getWarnCode1()));
		dispOrderItem.setWarnDesc2(getWarnDesc(dispOrderItem.getWarnCode2()));
		dispOrderItem.setWarnDesc3(getWarnDesc(dispOrderItem.getWarnCode3()));
		dispOrderItem.setWarnDesc4(getWarnDesc(dispOrderItem.getWarnCode4()));
		dispOrderItem.setReDispFlag(medProfilePoItem.getReDispFlag());
		dispOrderItem.setNumOfLabel(medProfilePoItem.getNumOfLabel());		
		
		DmDrug dmDrug = dmDrugCacher.getDmDrug(medProfilePoItem.getItemCode());
		if (dmDrug != null && dmDrug.getDmProcureSummary() != null) {
			dispOrderItem.setUnitPrice(BigDecimal.valueOf(dmDrug.getDmProcureSummary().getAverageUnitPrice()));
		}
		if (dmDrug != null && dmDrug.getDrugChargeInfo() != null) {
			dispOrderItem.setLifestyleFlag(dmDrug.getDrugChargeInfo().isLifestyleDrug());
			dispOrderItem.setOncologyFlag(dmDrug.getDrugChargeInfo().isOncologyDrug());
		}
		
		dispOrderItem.setPrintMpDispLabelInstructionFlag(medProfilePoItem.getMedProfileMoItem().getPrintInstructionFlag());
		dispOrderItem.setVerifyUser(medProfilePoItem.getMedProfileMoItem().getMedProfileItem().getVerifyUser());
		
		return dispOrderItem;		
	}
	
	private DispOrderItem convertToDispOrderItem(String itemCode, PivasWorklist pivasWorklist) {
		
		MedProfileMoItem medProfileMoItem = pivasWorklist.getMedProfileMoItem();
		PivasPrepMethod pivasPrepMethod = pivasWorklist.getPivasPrepMethod();
		
		DispOrderItem dispOrderItem = new DispOrderItem();
		dispOrderItem.setWarnCode1(pivasPrepMethod.getWarnCode1());
		dispOrderItem.setWarnCode2(pivasPrepMethod.getWarnCode2());
		dispOrderItem.setWarnDesc1(getWarnDesc(dispOrderItem.getWarnCode1()));
		dispOrderItem.setWarnDesc2(getWarnDesc(dispOrderItem.getWarnCode2()));
		dispOrderItem.setReDispFlag(Boolean.FALSE);
		dispOrderItem.setNumOfLabel(1);
		dispOrderItem.setPivasFlag(Boolean.TRUE);
		
		DmDrug dmDrug = dmDrugCacher.getDmDrug(itemCode);
		if (dmDrug != null && dmDrug.getDmProcureSummary() != null) {
			dispOrderItem.setUnitPrice(BigDecimal.valueOf(dmDrug.getDmProcureSummary().getAverageUnitPrice()));
		}
		if (dmDrug != null && dmDrug.getDrugChargeInfo() != null) {
			dispOrderItem.setLifestyleFlag(dmDrug.getDrugChargeInfo().isLifestyleDrug());
			dispOrderItem.setOncologyFlag(dmDrug.getDrugChargeInfo().isOncologyDrug());
		}
		
		dispOrderItem.setPrintMpDispLabelInstructionFlag(medProfileMoItem.getPrintInstructionFlag());
		dispOrderItem.setVerifyUser(medProfileMoItem.getMedProfileItem().getVerifyUser());
		
		return dispOrderItem;		
	}
	
	public List<Invoice> convertPurchaseRequestToVoidInvoiceList(List<PurchaseRequest> voidPurchaseRequestList, Workstore workstore){
		if( voidPurchaseRequestList == null ){
			return null;
		}
		
		List<Invoice> voidInvoiceList = new ArrayList<Invoice>();
		Date now = new Date();
		
		for(PurchaseRequest voidPr : voidPurchaseRequestList){
			
			Invoice voidInvoice = new Invoice();
			voidInvoice.setInvoiceNum(voidPr.getInvoiceNum());
			voidInvoice.setCreateDate(voidPr.getInvoiceDate());
			voidInvoice.setUpdateDate(now);
			if( uamInfo != null ){
				voidInvoice.setUpdateUser(uamInfo.getUserId());
			}else{
				voidInvoice.setUpdateUser(voidPr.getUpdateUser());
			}
			voidInvoice.setDocType(InvoiceDocType.Sfi);
			
			DispOrder voidDispOrder = new DispOrder();
			voidDispOrder.setWorkstore(workstore);
			voidDispOrder.addInvoice(voidInvoice);			
			
			PharmOrder voidPharmOrder = new PharmOrder();
			voidDispOrder.setPharmOrder(voidPharmOrder);
			voidPharmOrder.addDispOrder(voidDispOrder);
			
			MedOrder voidMedOrder = new MedOrder();
			voidMedOrder.setPatHospCode(voidPr.getMedProfile().getPatHospCode());
			voidMedOrder.addPharmOrder(voidPharmOrder);
			
			voidInvoiceList.add(voidInvoice);
		}
		return voidInvoiceList;		
	}

	public DispOrder convertTpnRequestListToDispOrder(List<TpnRequest> tpnRequestList, Workstore workstore, Workstation workstation) {

		Date now = new Date();
		
		MedProfile medProfile = (MedProfile) Contexts.getSessionContext().get("medProfile");
		DispOrder dispOrder = new DispOrder();
		dispOrder.setWardCode(medProfile.getWardCode());
		dispOrder.setPatCatCode(medProfile.getPatCatCode());
		dispOrder.setSpecCode(medProfile.getSpecCode());
		dispOrder.setPrintLang(medProfile.getPrintLang());
		dispOrder.setOrderType(OrderType.InPatient);
		dispOrder.setStatus(DispOrderStatus.Issued);
		dispOrder.setDispDate(now);
		dispOrder.setVetDate(now);
		dispOrder.setIssueDate(now);
		dispOrder.setIssueUser(uamInfo.getUserId());
		dispOrder.setWorkstore(workstore);
		
		PharmOrder pharmOrder = convertToPharmOrder(medProfile);
		pharmOrder.addDispOrder(dispOrder);
		pharmOrder.setWorkstore(workstore);
		pharmOrder.setTicketDate(now);
		pharmOrder.setOrderDate(now);
		pharmOrder.setWorkstationCode(workstation.getWorkstationCode());
		dispOrder.setPharmOrder(pharmOrder);
		
		MedOrder medOrder = convertToMedOrder(medProfile);
		medOrder.setOrderNum(UUID.randomUUID().toString());
		medOrder.setOrderDate(now);
		medOrder.setWorkstore(workstore);
		medOrder.addPharmOrder(pharmOrder);

		for (TpnRequest tpnRequest : tpnRequestList) {

			MedOrderItem medOrderItem = createMedOrderItemFromTpnRequest(tpnRequest, tpnRequestList.indexOf(tpnRequest));
			medOrder.addMedOrderItem(medOrderItem);

			PharmOrderItem pharmOrderItem = medOrderItem.getPharmOrderItemList().get(0);
			pharmOrder.addPharmOrderItem(pharmOrderItem);
			
			DispOrderItem dispOrderItem = pharmOrderItem.getDispOrderItemList().get(0);
			dispOrder.addDispOrderItem(dispOrderItem);
		}
		
		return dispOrder;
	}

	private MedOrderItem createMedOrderItemFromTpnRequest(TpnRequest tpnRequest, int index) {
		
		MedOrderItem medOrderItem = new MedOrderItem();
		medOrderItem.setItemNum(index);
		medOrderItem.setOrgItemNum(index);
		medOrderItem.setRxItemType(RxItemType.Oral);
		medOrderItem.setStatus(MedOrderItemStatus.Completed);
		
		OralRxDrug oralRxDrug = new OralRxDrug();
		oralRxDrug.setItemCode(TPN_ITEM_CODE);
		oralRxDrug.setOrderDate(tpnRequest.getCreateDate());
		oralRxDrug.setDisplayName(TPN_ITEM_DESC);
		oralRxDrug.setFirstDisplayName(TPN_ITEM_DESC);
		oralRxDrug.setFmStatus(FmStatus.GeneralDrug.getDataValue());
		oralRxDrug.setActionStatus(ActionStatus.DispByPharm);
		medOrderItem.setRxDrug(oralRxDrug);

		Regimen regimen = new Regimen();
		regimen.setType(RegimenType.Daily);
		regimen.setDurationInDay(tpnRequest.getDuration());
		oralRxDrug.setRegimen(regimen);
		
		DoseGroup doseGroup = new DoseGroup();
		doseGroup.setDuration(tpnRequest.getDuration());
		doseGroup.setDurationUnit(RegimenDurationUnit.Day);
		doseGroup.setStartDate(tpnRequest.getCreateDate());
		DateTime endDateTime = new DateTime(tpnRequest.getCreateDate()).plus(new BigDecimal(TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS)).multiply(BigDecimal.valueOf(tpnRequest.getDuration()-1)).longValue());
		doseGroup.setEndDate(endDateTime.toDate());

		regimen.setDoseGroupList(Arrays.asList(doseGroup));
		
		Dose dose = new Dose();
		dose.setDosage(BigDecimal.ZERO);
		dose.setDosageUnit("D");
		dose.setSiteDesc(StringUtils.EMPTY);
		doseGroup.setDoseList(Arrays.asList(dose));

		Freq freq = new Freq();
		freq.setCode(TPN_DAILY_FREQ_CODE);
		freq.setDesc(TPN_DAILY_FREQ_DESC);
		freq.setMultiplierType(TPN_DAILY_FREQ_MULTIPLIER_TYPE);
		dose.setDailyFreq(freq);
		
		PharmOrderItem pharmOrderItem = new PharmOrderItem();
		pharmOrderItem.setItemCode(TPN_ITEM_CODE);
		pharmOrderItem.setItemNum(index);
		pharmOrderItem.setOrgItemNum(index);
		pharmOrderItem.setIssueQty(BigDecimal.ZERO);
		pharmOrderItem.setAdjQty(BigDecimal.ZERO);
		pharmOrderItem.setCalQty(BigDecimal.ZERO);
		pharmOrderItem.setDoseQty(BigDecimal.ZERO);
		pharmOrderItem.setSfiQty(0);
		pharmOrderItem.setActionStatus(ActionStatus.DispByPharm);
		pharmOrderItem.setStatus(PharmOrderItemStatus.Completed);
		pharmOrderItem.setRegimen(regimen);
		medOrderItem.addPharmOrderItem(pharmOrderItem);
		
		DispOrderItem dispOrderItem = new DispOrderItem();
		dispOrderItem.setItemNum(index);
		dispOrderItem.setNumOfLabel(0);
		dispOrderItem.setDispQty(BigDecimal.ZERO);
		dispOrderItem.setStatus(DispOrderItemStatus.KeepRecord);
		dispOrderItem.setDurationInDay(regimen.getDurationInDay());
		dispOrderItem.setStartDate(doseGroup.getStartDate());
		dispOrderItem.setEndDate(doseGroup.getEndDate());
		dispOrderItem.setTpnRequestId(tpnRequest.getId());
		dispOrderItem.setCreateDate(tpnRequest.getCreateDate());
		
		MpDispLabel label = new MpDispLabel();
		label.setItemDesc(TPN_ITEM_DESC);
		dispOrderItem.setBaseLabel(label);
		
		pharmOrderItem.addDispOrderItem(dispOrderItem);
		
		return medOrderItem;
	}
}
