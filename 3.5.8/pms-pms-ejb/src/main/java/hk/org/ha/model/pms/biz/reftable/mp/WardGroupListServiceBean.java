package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.reftable.WardConfigManagerLocal;
import hk.org.ha.model.pms.biz.reftable.WardGroupManagerLocal;
import hk.org.ha.model.pms.biz.reftable.WardManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.phs.Ward;
import hk.org.ha.model.pms.persistence.reftable.WardConfig;
import hk.org.ha.model.pms.persistence.reftable.WardGroup;
import hk.org.ha.model.pms.persistence.reftable.WardGroupItem;
import hk.org.ha.model.pms.vo.reftable.mp.WardGroupInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("wardGroupListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class WardGroupListServiceBean implements WardGroupListServiceLocal {
	
	@PersistenceContext
	private EntityManager em;

	@In
	private Workstore workstore;

	@In
	private WardManagerLocal wardManager;

	@In
	private WardConfigManagerLocal wardConfigManager;

	@In
	private WardGroupManagerLocal wardGroupManager;
	
	private boolean saveSuccess;
	
	private String errCode;
	
	public WardGroupInfo retrieveWardGroupList() 
	{
		WardGroupInfo wardGroupInfo = new WardGroupInfo();
		wardGroupInfo.setWardGroupWardList(retrieveWardGroupWardList());
		List<WardGroup> wardGroupList = wardGroupManager.retrieveWardGroupList(workstore.getWorkstoreGroup());
		
		for(WardGroup wg : wardGroupList){
			wg.getWardGroupItemList().size();
		}
		wardGroupInfo.setWardGroupList(wardGroupList);
		return wardGroupInfo;
	}
	
	private List<Ward> retrieveWardGroupWardList(){
		List<WardConfig> mpWardConfig = wardConfigManager.retrieveMpWardConfigList(workstore.getWorkstoreGroup());
		
		if ( mpWardConfig.isEmpty() ) {
			return new ArrayList<Ward>();
		}
		
		List<String> wardCodeList = new ArrayList<String>();
		for ( WardConfig wardConfig : mpWardConfig) { 
			wardCodeList.add(wardConfig.getWardCode());
		}
		
		return wardManager.retrieveWardListByWardCodeList(wardCodeList);
	}
	
	public void updateWardGroup(WardGroup inWardGroup, List<Ward> selectedWardList){
		
		List<WardGroupItem> delWardGroupItemList = new ArrayList<WardGroupItem>(inWardGroup.getWardGroupItemList());
		
		Map<String, WardGroupItem> wardGroupItemMap = new HashMap<String, WardGroupItem>();
		for ( WardGroupItem item : inWardGroup.getWardGroupItemList() ){
			wardGroupItemMap.put(item.getWardCode(), item);
		}
		
		for ( Ward selectedWard : selectedWardList ) {
			if ( wardGroupItemMap.containsKey(selectedWard.getWardCode()) ) {
				delWardGroupItemList.remove(wardGroupItemMap.get(selectedWard.getWardCode()));
			} else {
				WardGroupItem wardGroupItem = new WardGroupItem();
				wardGroupItem.setWardCode(selectedWard.getWardCode());
				wardGroupItem.setWardGroup(inWardGroup);
				inWardGroup.getWardGroupItemList().add(wardGroupItem);
			}
		}
		
		for (WardGroupItem delItem : delWardGroupItemList){
			inWardGroup.getWardGroupItemList().remove(delItem);
		}
		
		em.merge(inWardGroup);
		em.flush();
	}
	
	public void createWardGroup(String wardGroupCode){
		saveSuccess = true;
		errCode = "";
		
		int count = ((Long)em.createQuery(
				"select count(o) from WardGroup o" + // 20120303 index check : WardGroup.workstoreGroup,wardGroupCode : UI_WARD_GROUP_01
				" where o.wardGroupCode = :wardGroupCode" +
				" and o.workstoreGroup = :workstoreGroup")
				.setParameter("wardGroupCode", wardGroupCode)
				.setParameter("workstoreGroup", workstore.getWorkstoreGroup())
				.getSingleResult()).intValue();
											
		if ( count > 0 ) {
			saveSuccess = false;
			errCode = "0006";
			return;
		}
		
		WardGroup wardGroup = new WardGroup();
		wardGroup.setWardGroupCode(wardGroupCode);
		wardGroup.setWorkstoreGroup(workstore.getWorkstoreGroup());
		em.persist(wardGroup);
		em.flush();
	}
	
	public WardGroupInfo removeWardGroup(WardGroup wardGroup){
		em.remove(em.merge(wardGroup));
		em.flush();
		
		WardGroupInfo wardGroupInfo = retrieveWardGroupList();
		return wardGroupInfo;
	}

	public boolean isSaveSuccess() {
		return saveSuccess;
	}

	public String getErrCode() {
		return errCode;
	}

	@Remove
	public void destroy() 
	{
	}
}
