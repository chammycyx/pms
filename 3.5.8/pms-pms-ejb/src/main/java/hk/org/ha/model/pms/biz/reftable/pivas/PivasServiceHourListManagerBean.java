package hk.org.ha.model.pms.biz.reftable.pivas;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
import hk.org.ha.model.pms.persistence.reftable.pivas.PivasServiceHour;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.apache.commons.lang3.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("pivasServiceHourListManager")
@MeasureCalls
public class PivasServiceHourListManagerBean implements PivasServiceHourListManagerLocal {		
	
	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	public List<PivasServiceHour> retrievePivasServiceHourList(WorkstoreGroup workstoreGroup) {
		List<PivasServiceHour> pivasServiceHourList =  em.createQuery(
														"select o from PivasServiceHour o" + // 20160616 index check : PivasServiceHour.workstoreGroup : UI_PIVAS_SERVICE_HOUR
														" where o.workstoreGroup = :workstoreGroup" +
														" order by o.dayOfWeek, o.supplyTime, o.type")
														.setParameter("workstoreGroup", workstoreGroup)
														.getResultList();
		
		for ( PivasServiceHour pivasServiceHour : pivasServiceHourList ) {
			if ( pivasServiceHour.getSupplyTime() != null ) {
				pivasServiceHour.setSupplyTimeStr(StringUtils.leftPad(pivasServiceHour.getSupplyTime().toString(), 4, "0"));
			}
		}
		return pivasServiceHourList;
		
	}

	public void updatePivasServiceHourList(List<PivasServiceHour> updatePivasServiceHourList, List<PivasServiceHour> deletePivasServiceHourList, WorkstoreGroup workstoreGroup) {

		if ( !deletePivasServiceHourList.isEmpty() ) {
			for ( PivasServiceHour pivasServiceHour : deletePivasServiceHourList ) {
				if ( !pivasServiceHour.isNew() ) {
					em.remove(em.merge(pivasServiceHour));
				}
			}
			em.flush();
		}
		
		if ( !updatePivasServiceHourList.isEmpty() ) {
			try {
				for ( PivasServiceHour pivasServiceHour : updatePivasServiceHourList ) {
					pivasServiceHour.setSupplyTime(Integer.parseInt(pivasServiceHour.getSupplyTimeStr()));
					if ( pivasServiceHour.isNew() ) {
						pivasServiceHour.setWorkstoreGroup(workstoreGroup);
						em.persist(pivasServiceHour);
					} else {
						em.merge(pivasServiceHour);
					}
				}
				em.flush();
			} catch (PersistenceException e) {
				if (e.getMessage().contains("ORA-00001")) {
					throw new OptimisticLockException();
				} else {
					throw e;
				}
			}
		}
		
	}
	
}
