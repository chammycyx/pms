package hk.org.ha.model.pms.biz.label;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.prop.Prop;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.vo.label.BaseLabel;
import hk.org.ha.model.pms.vo.label.DispLabel;
import hk.org.ha.model.pms.vo.label.MpDispLabel;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderJob;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
/**
 * Session Bean implementation class LabelServiceBean
 */
@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("labelService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class LabelServiceBean implements LabelServiceLocal {

	private static final String MP_DISP_LABEL = "MpDispLabel";
	private static final String MP_DISP_LABEL_NO_INST = "MpDispLabelNoInst";
	
	@In
	private LabelManagerLocal labelManager;
	
	@In
	private DispLabelBuilderLocal dispLabelBuilder;
	
	@In
	private PrintAgentInf printAgent;
	
	private List<DispLabel> dispLabelList;
	
	private List<MpDispLabel> mpDispLabelList;
	
	private BaseLabel baseLabel;

	public void getDispLabelPreview( PharmOrder pharmOrder, PrintLang printLang, String printType ) {
		
		dispLabelList = labelManager.getDispLabelPreview( pharmOrder, printLang, printType );
		
	}	
	
	public void getMpDispLabelPreview( MedProfileItem medProfileItem, MedProfileMoItem medProfileMoItem) {
		
		mpDispLabelList = labelManager.getMpDispLabelPreview(medProfileItem, medProfileMoItem);
		
	}
	
	public void printDispLabelForOrderEdit() {
		Map<String, Object> parameters = dispLabelBuilder.getDispLabelParam(dispLabelList.get(0));
		parameters.put("_PNG_ZOOM_RATIO", Float.valueOf(1.3f));
		PrintOption printOption = dispLabelList.get(0).getPrintOption();
		printOption.setDocType("PDF".equals(Prop.LABEL_PREVIEW_FORMAT.get()) ? ReportProvider.PDF : ReportProvider.PNG);
		printAgent.renderAndRedirect(new RenderJob(
				"DispLabel", 
				printOption, 
				parameters, 
				dispLabelList));
	}
	
	public void printMpDispLabelForOrderEdit() {
		Map<String, Object> parameters = dispLabelBuilder.getMpDispLabelParam(mpDispLabelList.get(0));
		parameters.put("_PNG_ZOOM_RATIO", Float.valueOf(1.3f));
		String labelType = MP_DISP_LABEL;
		if( !mpDispLabelList.get(0).getShowInstructionFlag() ){
			labelType = MP_DISP_LABEL_NO_INST;
		}
		
		PrintOption printOption = mpDispLabelList.get(0).getPrintOption();
		printOption.setDocType("PDF".equals(Prop.LABEL_PREVIEW_FORMAT.get()) ? ReportProvider.PDF : ReportProvider.PNG);
		
		printAgent.renderAndRedirect(new RenderJob(
				labelType, 
				printOption, 
				parameters, 
				mpDispLabelList));
	}
	
	public void getCddhDispLabel(List<String> dispOrderItemKey){
		
		baseLabel = labelManager.getCddhDispLabel(dispOrderItemKey);
		
	}
	
	public void previewDispLabel() {
		DispLabel dispLabel = (DispLabel) baseLabel;
		Map<String, Object> parameters = dispLabelBuilder.getDispLabelParam(dispLabel);
		parameters.put("USER_CODE", dispLabel.getUserCode());
		parameters.put("_PNG_ZOOM_RATIO", Float.valueOf(1.3f));
		PrintOption printOption = dispLabel.getPrintOption();
		printOption.setDocType("PDF".equals(Prop.LABEL_PREVIEW_FORMAT.get()) ? ReportProvider.PDF : ReportProvider.PNG);
		printAgent.renderAndRedirect(new RenderJob(
				"DispLabel", 
				printOption, 
				parameters, 
				Arrays.asList(dispLabel)));
	}
	
	public void previewMpDispLabel() {
		MpDispLabel mpDispLabel = (MpDispLabel) baseLabel;
		Map<String, Object> parameters = dispLabelBuilder.getMpDispLabelParam(mpDispLabel);
		parameters.put("USER_CODE", mpDispLabel.getUserCode());
		parameters.put("_PNG_ZOOM_RATIO", Float.valueOf(1.3f));
		parameters.put("_PNG_ZOOM_RATIO", Float.valueOf(1.3f));
		String labelType = MP_DISP_LABEL;
		if( !mpDispLabel.getShowInstructionFlag() ){
			labelType = MP_DISP_LABEL_NO_INST;
		}
		PrintOption printOption = mpDispLabel.getPrintOption();
		printOption.setDocType("PDF".equals(Prop.LABEL_PREVIEW_FORMAT.get()) ? ReportProvider.PDF : ReportProvider.PNG);
		printAgent.renderAndRedirect(new RenderJob(
				labelType, 
				printOption, 
				parameters, 
				Arrays.asList(mpDispLabel)));
	}
	
	@Remove
	public void destroy() {
		if( dispLabelList != null ){
			dispLabelList = null;
		}
		if( mpDispLabelList != null ){
			mpDispLabelList = null;
		}
		if( baseLabel != null ){
			baseLabel = null;
		}
	}

}