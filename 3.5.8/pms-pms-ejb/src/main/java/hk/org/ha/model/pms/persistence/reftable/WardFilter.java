package hk.org.ha.model.pms.persistence.reftable;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "WARD_FILTER")
@Customizer(AuditCustomizer.class)
public class WardFilter extends VersionEntity implements Comparable<WardFilter> {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wardFilterSeq")
	@SequenceGenerator(name = "wardFilterSeq", sequenceName = "SQ_WARD_FILTER", initialValue = 100000000)
	private Long id;
	
	@Column(name = "MON_FLAG", nullable = false)
	private Boolean monFlag;
	
	@Column(name = "TUE_FLAG", nullable = false)
	private Boolean tueFlag;
	
	@Column(name = "WED_FLAG", nullable = false)
	private Boolean wedFlag;
	
	@Column(name = "THU_FLAG", nullable = false)
	private Boolean thuFlag;
	
	@Column(name = "FRI_FLAG", nullable = false)
	private Boolean friFlag;
	
	@Column(name = "SAT_FLAG", nullable = false)
	private Boolean satFlag;
	
	@Column(name = "SUN_FLAG", nullable = false)
	private Boolean sunFlag;
	
	@Column(name = "START_TIME", nullable = false, length = 4)
	private String startTime;
	
	@Column(name = "END_TIME", nullable = false, length = 4)
	private String endTime;
	
	@Column(name = "WARD_CODE", nullable = false, length = 4)
	private String wardCode;
	
	@ManyToOne
	@JoinColumns(value = {
			@JoinColumn(name = "HOSP_CODE", referencedColumnName = "HOSP_CODE", nullable = false),
			@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;
	
	@Transient
	private Boolean markDelete;
		
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getMonFlag() {
		return monFlag;
	}

	public void setMonFlag(Boolean monFlag) {
		this.monFlag = monFlag;
	}

	public Boolean getTueFlag() {
		return tueFlag;
	}

	public void setTueFlag(Boolean tueFlag) {
		this.tueFlag = tueFlag;
	}

	public Boolean getWedFlag() {
		return wedFlag;
	}

	public void setWedFlag(Boolean wedFlag) {
		this.wedFlag = wedFlag;
	}

	public Boolean getThuFlag() {
		return thuFlag;
	}

	public void setThuFlag(Boolean thuFlag) {
		this.thuFlag = thuFlag;
	}

	public Boolean getFriFlag() {
		return friFlag;
	}

	public void setFriFlag(Boolean friFlag) {
		this.friFlag = friFlag;
	}

	public Boolean getSatFlag() {
		return satFlag;
	}

	public void setSatFlag(Boolean satFlag) {
		this.satFlag = satFlag;
	}

	public Boolean getSunFlag() {
		return sunFlag;
	}

	public void setSunFlag(Boolean sunFlag) {
		this.sunFlag = sunFlag;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}
	
	public void setMarkDelete(Boolean markDelete){
		this.markDelete = markDelete;
	}
	public Boolean getMarkDelete() {
		return markDelete;
	}
	
	// helper methods
    public boolean isNew() {
    	return this.getId() == null;
    }

	@Override
	public int compareTo(WardFilter wardFilter) {
		return new CompareToBuilder()
				.append( this.getWorkstore().getWorkstoreCode(), wardFilter.getWorkstore().getWorkstoreCode() )
				.append( this.getWardCode(), wardFilter.getWardCode() )
				.append( !this.getMonFlag(), !wardFilter.getMonFlag() )
				.append( !this.getTueFlag(), !wardFilter.getTueFlag() )
				.append( !this.getWedFlag(), !wardFilter.getWedFlag() )
				.append( !this.getThuFlag(), !wardFilter.getThuFlag() )
				.append( !this.getFriFlag(), !wardFilter.getFriFlag() )
				.append( !this.getSatFlag(), !wardFilter.getSatFlag() )
				.append( !this.getSunFlag(), !wardFilter.getSunFlag() )
				.append( this.getStartTime(), wardFilter.getStartTime() )
				.append( this.getEndTime(), wardFilter.getEndTime() )
				.toComparison();
	}

}
