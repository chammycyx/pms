package hk.org.ha.model.pms.vo.label;

import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.printing.PrintType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.printing.PrintOption;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "DispLabel")
@ExternalizedBean(type=DefaultExternalizer.class)
public class DispLabel extends BaseLabel {
	
	private static final long serialVersionUID = 1L;

	@XmlElement(required = true)	
	private String ticketNum;
	
	@XmlElement(required = true)	
	private String patCatCode;
	
	@XmlElement(required = true)	
	private String userCode;
	
	@XmlElement(required = true)	
	private Integer itemNum;

	@XmlElement(required = true)	
	private Date startDate;
	
	@XmlElement(required = true)	
	private Date ticketDate;
	
	@XmlElement(required = true)
	private Boolean fastQueueFlag;

	@XmlElement(required = true)	
	private String deltaChangeType;
	
	public DispLabel() {
	}	

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public String getPatCatCode() {
		return patCatCode;
	}

	public void setPatCatCode(String patCatCode) {
		this.patCatCode = patCatCode;
	}

	public String getUserCode() {
		return userCode;
	}
	
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	
	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public int getCharLimit() {
		int charLimit;
		PrintOption printOption = super.getPrintOption();
		if (printOption.getPrintType() == PrintType.Normal) {
			if (printOption.getPrintLang() == PrintLang.Eng) {
				charLimit = 55;
			} else {
				charLimit = 42;
			}
		} else {
			if (printOption.getPrintLang() == PrintLang.Eng) {
				charLimit = 47;
			} else {
				charLimit = 32;
			}
		}
		return charLimit;
	}
	
	public OrderType getOrderType() {
		return OrderType.OutPatient;
	}

	public void setTicketDate(Date ticketDate) {
		this.ticketDate = ticketDate;
	}

	public Date getTicketDate() {
		return ticketDate;
	}

	public void setFastQueueFlag(Boolean fastQueueFlag) {
		this.fastQueueFlag = fastQueueFlag;
	}

	public Boolean getFastQueueFlag() {
		return fastQueueFlag;
	}

	public String getDeltaChangeType() {
		return deltaChangeType;
	}

	public void setDeltaChangeType(String deltaChangeType) {
		this.deltaChangeType = deltaChangeType;
	}
}