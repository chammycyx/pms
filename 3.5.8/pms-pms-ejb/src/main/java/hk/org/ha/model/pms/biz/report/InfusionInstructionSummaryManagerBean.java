package hk.org.ha.model.pms.biz.report;

import static hk.org.ha.model.pms.prop.Prop.ALERT_EHR_PROFILE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.ALERT_PROFILE_ENABLED;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.alert.AlertProfileException;
import hk.org.ha.model.pms.asa.exception.alert.ehr.EhrAllergyException;
import hk.org.ha.model.pms.biz.alert.AlertAuditManagerLocal;
import hk.org.ha.model.pms.biz.alert.AlertProfileManagerLocal;
import hk.org.ha.model.pms.biz.alert.ehr.EhrAlertManagerLocal;
import hk.org.ha.model.pms.biz.printing.PrintLabelManagerLocal;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.biz.vetting.MedOrderItemComparator;
import hk.org.ha.model.pms.dms.biz.formatengine.FormatHints;
import hk.org.ha.model.pms.dms.biz.formatengine.ParsedOrderLineFormatter;
import hk.org.ha.model.pms.dms.biz.formatengine.ParsedOrderLineFormatterBase;
import hk.org.ha.model.pms.dms.biz.formatengine.ParsedOrderLineFormatterFactory;
import hk.org.ha.model.pms.dms.udt.formatengine.FormatType;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.prop.Prop;
import hk.org.ha.model.pms.udt.alert.AlertProfileStatus;
import hk.org.ha.model.pms.udt.alert.ehr.EhrAlertStatus;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.vo.alert.AlertProfile;
import hk.org.ha.model.pms.vo.label.DispLabel;
import hk.org.ha.model.pms.vo.label.InfusionInstructionSummaryReminderLabel;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.PrinterSelect;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.report.InfusionInstructionSummary;
import hk.org.ha.model.pms.vo.report.InfusionInstructionSummaryDtl;
import hk.org.ha.model.pms.vo.report.InfusionInstructionSummaryHdr;
import hk.org.ha.model.pms.vo.report.InfusionInstructionSummaryItemDtl;
import hk.org.ha.model.pms.vo.rx.RxItem;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("infusionInstructionSummaryManager")
@MeasureCalls
public class InfusionInstructionSummaryManagerBean implements InfusionInstructionSummaryManagerLocal {
	
	@In
	private PrinterSelectorLocal printerSelector;

	@In
	private Workstore workstore;

	@In
	private AlertAuditManagerLocal alertAuditManager; 

	@In 
	private AlertProfileManagerLocal alertProfileManager;

	@In
	private EhrAlertManagerLocal ehrAlertManager;

	@In
	private PrintLabelManagerLocal printLabelManager;
	
	private DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
	private DateFormat tf = new SimpleDateFormat("HH:mm"); 
	
	private static final String STANDARD_REMINDER_DOCNAME = "RefillCouponReminder";
	private static final String SFI_REMINDER_DOCNAME = "SfiRefillCouponReminder";
	private static final String DISP_LABEL_DOCNAME = "DispLabel";
	private static final int INSTRUCTION_LINE_WIDTH = 90;
	private static final String SPACE = " ";
	
	public boolean checkIfPrintInfusionInstructionSummary(DispOrderItem dispOrderItem){
		if (Boolean.FALSE.equals(Prop.VETTING_REPORT_INFUSIONINSTRUCTIONSUMMARY_PRINT.get(workstore))) {
			return false;
		}
		
		MedOrderItem medOrderItem = dispOrderItem.getPharmOrderItem().getMedOrderItem();
		medOrderItem.loadDmInfo();
		if ( !medOrderItem.isInfusion() ) {
			return false;
		}
		
		PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
		if ( pharmOrderItem.getActionStatus() == ActionStatus.DispByPharm || 
				pharmOrderItem.getActionStatus() == ActionStatus.SafetyNet ) {
			return true;
		} else if (	pharmOrderItem.getActionStatus() == ActionStatus.PurchaseByPatient && 
						dispOrderItem.getDispQty().compareTo(BigDecimal.ZERO) > 0 ) {
			return true;
		}

		return false;
	}
	
	private InfusionInstructionSummary createInfusionInstructionSummary(DispOrder dispOrder, PrintOption printOption){
		InfusionInstructionSummary infusionInstructionSummary = new InfusionInstructionSummary();
		infusionInstructionSummary.setInfusionInstructionSummaryHdr(convertInfusionInstructionSummaryHdr(dispOrder));
		infusionInstructionSummary.setInfusionInstructionSummaryDtlList(this.convertInfusionInstructionSummaryDtl(dispOrder, printOption));
		
		StringBuilder summaryFtr = new StringBuilder();
		summaryFtr.append("Printed by Department of Pharmacy, ");
		summaryFtr.append(workstore.getHospCode());
		summaryFtr.append(" / ");
		summaryFtr.append(workstore.getWorkstoreCode());
		summaryFtr.append(" on ");
		summaryFtr.append(df.format(new Date()));
		summaryFtr.append(" at ");
		summaryFtr.append(tf.format(new Date()));
		
		infusionInstructionSummary.setInfusionInstructionSummaryFtr(summaryFtr.toString());
		return infusionInstructionSummary;
	}
	
	private List<InfusionInstructionSummaryDtl> convertInfusionInstructionSummaryDtl(DispOrder dispOrder, PrintOption printOption){
		List<InfusionInstructionSummaryDtl> dtlList = new ArrayList<InfusionInstructionSummaryDtl>();
		Map<MedOrderItem, List<DispOrderItem>> dispOrderItemMap = new HashMap<MedOrderItem, List<DispOrderItem>>();
		List<MedOrderItem> medOrderItemList = new ArrayList<MedOrderItem>();
		
		for ( DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList() ) {
			MedOrderItem medOrderItem = dispOrderItem.getPharmOrderItem().getMedOrderItem();
			medOrderItem.loadDmInfo();
			if ( medOrderItem.isInfusion() ) {
				if ( !dispOrderItemMap.containsKey(medOrderItem) ) {
					dispOrderItemMap.put(medOrderItem, new ArrayList<DispOrderItem>());
					medOrderItemList.add(medOrderItem);
				}
				dispOrderItemMap.get(medOrderItem).add(dispOrderItem);
			}
		}
		
		Collections.sort(medOrderItemList, new MedOrderItemComparator());
		
		for ( MedOrderItem medOrderItem : medOrderItemList ) {
			if ( !dispOrderItemMap.containsKey(medOrderItem)) {
				continue;
			}
			
			List<DispOrderItem> dispOrderItemList = dispOrderItemMap.get(medOrderItem);
			RxItem rxItem = medOrderItem.getRxItem();

			InfusionInstructionSummaryDtl summaryDtl = new InfusionInstructionSummaryDtl();
			List<InfusionInstructionSummaryItemDtl> dtlItemList = new ArrayList<InfusionInstructionSummaryItemDtl>();
			
			if ( StringUtils.isNotBlank(rxItem.getDrugOrderXml()) ) {
				ParsedOrderLineFormatterBase formatter = ParsedOrderLineFormatterFactory.getParsedOrderLineFormatter(rxItem.getInterfaceVersion());
				formatter.setHint(FormatHints.FORMAT_TYPE, FormatType.TXT);
				formatter.setHint(FormatHints.LINE_WIDTH, INSTRUCTION_LINE_WIDTH);
				formatter.setHint(FormatHints.HIDE_STRIKE_THROUGH, false);
				formatter.setHint(FormatHints.EXCLUDE_TOKEN_IDS, new String[]{"order.specialInstruction"});
				summaryDtl.setInfusionInstruction(formatter.format(rxItem.getDrugOrderXml()));
			} 
			
			if ( StringUtils.isNotBlank(rxItem.getSpecialInstruction()) ) {
				summaryDtl.setSpecialInstruction(formatSpecialInstruction(rxItem.getSpecialInstruction()));
			}
			
			for ( DispOrderItem dispOrderItem : dispOrderItemList ) {
				InfusionInstructionSummaryItemDtl summaryDtlItem = new InfusionInstructionSummaryItemDtl();
				PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
				
				summaryDtlItem.setItemCode(pharmOrderItem.getItemCode());
				if ( dispOrderItem.getBaseLabel() != null ) {
					DispLabel dispLabel = (DispLabel)dispOrderItem.getBaseLabel();
					dispLabel.setPrintOption(printOption);
					
					summaryDtlItem.setLabelDesc(dispLabel.getItemDesc());
					summaryDtlItem.setLabelInstructionText(dispLabel.getInstructionText());
					summaryDtlItem.setWarningText(dispLabel.getWarningText());
				}
				summaryDtlItem.setIssueQty(dispOrderItem.getDispQty().intValue());
				summaryDtlItem.setBaseUnit(pharmOrderItem.getBaseUnit());
				summaryDtlItem.setPrintLang(printOption.getPrintLang());
				dtlItemList.add(summaryDtlItem);
			}
			
			summaryDtl.setInfusionInstructionSummaryItemDtlList(dtlItemList);
			dtlList.add(summaryDtl);
		}
		
		return dtlList;
	}
	
	private String formatSpecialInstruction(String specialInstructionStr){
		specialInstructionStr.replaceAll("\r", "");
		List<StringBuilder> tempResultList = new ArrayList<StringBuilder>();
		
		for( String specInst : specialInstructionStr.split("\n") ){
			StringBuilder tempResult = new StringBuilder();
			
			for( String s : specInst.split(SPACE) ){
				int spaceLength = ( tempResult.length() > 0 )?1:0;
				if( tempResult.length() + s.length() + spaceLength <= 90 ){
					if( tempResult.length() > 0 ){
						tempResult.append(SPACE);
					}
					tempResult.append(s);
				}else{
					tempResultList.add(tempResult);
					tempResult = new StringBuilder();
					tempResult.append(s);
				}
			}
			if ( tempResult.length() > 0 ) {
				tempResultList.add(tempResult);
			}
		}
		
		StringBuilder result = new StringBuilder();
		for ( StringBuilder sb : tempResultList ) {
			if ( result.length() > 0 ) {
				result.append("\n");
			}
			result.append(sb);
		}
		return result.toString();
	}
	
	private InfusionInstructionSummaryHdr convertInfusionInstructionSummaryHdr(DispOrder dispOrder) {
		InfusionInstructionSummaryHdr summaryHdr = new InfusionInstructionSummaryHdr();
		Ticket ticket = dispOrder.getTicket();
		Patient patient = dispOrder.getPharmOrder().getPatient();
		MedCase medCase = dispOrder.getPharmOrder().getMedCase();
		
		summaryHdr.setTicketNum(ticket.getTicketNum());
		summaryHdr.setTicketDate(ticket.getTicketDate());
		summaryHdr.setPatName(patient.getName());
		summaryHdr.setPatNameChi(patient.getNameChi());
		summaryHdr.setGender(patient.getSex().getDisplayValue());
		
		if ( patient.getAge() != null ) {
			summaryHdr.setAge(patient.getAge()+patient.getDateUnit().substring(0, 1));
		}
		
		if ( medCase != null ) {
			summaryHdr.setPasSpecCode(medCase.getPasSpecCode());
			summaryHdr.setPasSubSpecCode(medCase.getPasSubSpecCode());
			summaryHdr.setCaseNum(StringUtils.leftPad(medCase.getCaseNum(),12));
		}
		
		this.convertAlertProfile(summaryHdr, dispOrder);
		
		return summaryHdr;
	}
	
	private void convertAlertProfile(InfusionInstructionSummaryHdr summaryHdr, DispOrder dispOrder){
		AlertProfile alertProfile = null;
		try {
			if (ALERT_PROFILE_ENABLED.get(Boolean.FALSE)) {
				alertProfile= alertProfileManager.retrieveAlertProfileByPharmOrder(dispOrder.getPharmOrder());
			}
			
			summaryHdr.setAllergyServerAvailableFlag(false);
			
			if ( alertProfile != null ) {
				summaryHdr.setAlertProfileStatus(alertProfile.getStatus());
				summaryHdr.setAllergyServerAvailableFlag(true);
				
				if ( !alertProfile.getStatus().equals(AlertProfileStatus.Error) ) {
					//set Drug Allergy
					summaryHdr.setDrugAllergenDesc(StringUtils.trimToNull(alertProfile.getAllergyDesc()));

					//set Adverse Drug Reaction
					summaryHdr.setAdrAlertDesc(StringUtils.trimToNull(alertProfile.getAdrDesc()));
					
					//set other Alert
					if ( alertProfile.getG6pdAlert() != null ) {
						summaryHdr.setOtherAllergyDesc(StringUtils.trimToNull(alertProfile.getG6pdAlert().getAlertDesc()));
					}
				}
			}
			
		} catch (AlertProfileException e) {
			summaryHdr.setAllergyServerAvailableFlag(false);
			alertAuditManager.alertServiceUnavailable(dispOrder.getPharmOrder(), dispOrder.getTicket());
		}
		
		summaryHdr.setEhrAllergyException(true);
		if (ALERT_EHR_PROFILE_ENABLED.get()) {
			try {
				alertProfile = ehrAlertManager.retrieveEhrAllergySummaryByPharmOrder(alertProfile, dispOrder.getPharmOrder());
				if (alertProfile != null && alertProfile.getEhrAllergySummary() != null && alertProfile.getEhrAllergySummary().getStatus() != EhrAlertStatus.Error) {
					summaryHdr.setEhrAllergyException(false);
				}
			}
			catch (EhrAllergyException e) {}
		}

		if (alertProfile != null && alertProfile.getEhrAllergySummary() != null) {
			summaryHdr.setEhrAllergy(alertProfile.getEhrAllergySummary().getAllergy());
			summaryHdr.setEhrAdr(alertProfile.getEhrAllergySummary().getAdr());
		}
	}
	
	public void prepareInfusionInstructionSummaryPrintJobWithReminder(List<RenderAndPrintJob> printJobList, PrintLang printLang, DispOrder dispOrder) 
	{
		//Summary report
		PrintOption labelPrintOption = new PrintOption();
		labelPrintOption.setPrintLang(printLang);
		
		InfusionInstructionSummary infusionInstructionSummary = this.createInfusionInstructionSummary(dispOrder, labelPrintOption);	
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("hospCode", workstore.getHospCode());
		parameters.put("workstoreCode", workstore.getWorkstoreCode());
		
		printJobList.add(new RenderAndPrintJob(
					    		"InfusionInstructionSummary",
					    		new PrintOption(),
					    		printerSelector.retrievePrinterSelect(PrintDocType.Report),
							    parameters,
							    Arrays.asList(infusionInstructionSummary),
							    "[InfusionInstructionSummary] "
								));
		
		//Reminder label
		InfusionInstructionSummaryReminderLabel reminderLabel = this.createSummaryReminderLabel(dispOrder);
		
		PrinterSelect reminderPrinterSelect = null;
		
		List<RenderAndPrintJob> reversePrintJobList = new ArrayList<RenderAndPrintJob>();
		reversePrintJobList.addAll(printJobList);
		Collections.reverse(reversePrintJobList);

		for( RenderAndPrintJob renderPrintJob : reversePrintJobList ){
			if( STANDARD_REMINDER_DOCNAME.equals(renderPrintJob.getDocName()) ||
				SFI_REMINDER_DOCNAME.equals(renderPrintJob.getDocName()) ||
				DISP_LABEL_DOCNAME.equals(renderPrintJob.getDocName()) ){
				reminderPrinterSelect = new PrinterSelect(renderPrintJob.getPrinterSelect());
				break;
			}
		}
		
		if( reminderPrinterSelect == null ){
			reminderPrinterSelect = printLabelManager.retrieveDefaultPrinterSelectForCouponReminder(dispOrder.getUrgentFlag());			
		}
		
		reminderPrinterSelect.setCopies(1);
		
		printJobList.add(new RenderAndPrintJob(
					    		"InfusionInstructionSummaryReminder",
					    		new PrintOption(),
					    		reminderPrinterSelect,
					    		new HashMap<String, Object>(),
							    Arrays.asList(reminderLabel),
							    "[InfusionInstructionSummaryReminder] "
								));
	}	
	
	private InfusionInstructionSummaryReminderLabel createSummaryReminderLabel(DispOrder dispOrder){
		InfusionInstructionSummaryReminderLabel reminderLabel = new InfusionInstructionSummaryReminderLabel();
		Ticket ticket = dispOrder.getTicket();
		MedCase medCase = dispOrder.getPharmOrder().getMedCase();
		
		reminderLabel.setDispDate(ticket.getTicketDate());
		reminderLabel.setTicketNum(ticket.getTicketNum());
		reminderLabel.setPatName(dispOrder.getPharmOrder().getPatient().getName());
		if ( medCase != null ) {
			reminderLabel.setCaseNum(medCase.getCaseNum());
		}
		
		reminderLabel.setOrderNum(dispOrder.getPharmOrder().getMedOrder().getOrderNum());
		reminderLabel.setHospCode(workstore.getHospCode());
		reminderLabel.setWorkstoreCode(workstore.getWorkstoreCode());
		
		return reminderLabel;
	}
}