package hk.org.ha.model.pms.udt.printing;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PrintType implements StringValuedEnum {

	Normal("N", "Normal"),
	LargeFont("LF", "LargeFont"),	
	LargeLayout("LL", "LargeLayout");	
	
    private final String dataValue;
    private final String displayValue;

    PrintType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    

    public static PrintType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PrintType.class, dataValue);
	}
    
	public static class Converter extends StringValuedEnumConverter<PrintType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<PrintType> getEnumClass() {
    		return PrintType.class;
    	}
    }
}



