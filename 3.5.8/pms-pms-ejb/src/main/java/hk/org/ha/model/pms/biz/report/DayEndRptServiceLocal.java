package hk.org.ha.model.pms.biz.report;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface DayEndRptServiceLocal {

	void retrieveDayEndRpt(Integer selectedTab, String workstoreCode, Date date);
	
	void generateDayEndRpt();
	
	void printDayEndRpt();
	
	List<?> getReportObj();
	
	void destroy();
	
}
