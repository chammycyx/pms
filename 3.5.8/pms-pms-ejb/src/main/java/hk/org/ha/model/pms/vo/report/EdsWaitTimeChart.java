package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import net.sf.jasperreports.engine.JRChart;
import net.sf.jasperreports.engine.JRChartCustomizer;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;

public class EdsWaitTimeChart implements JRChartCustomizer, Serializable
{
	private static final long serialVersionUID = 1L;

	public void customize(JFreeChart chart, JRChart jasperChart)
	{
		ValueAxis axisV = chart.getCategoryPlot().getRangeAxis();
        axisV.setLowerBound(0);
	}
}
