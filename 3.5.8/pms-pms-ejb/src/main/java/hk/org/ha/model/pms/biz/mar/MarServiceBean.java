package hk.org.ha.model.pms.biz.mar;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.mar.MarException;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.service.pms.asa.interfaces.mar.MarServiceJmsRemote;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import net.sf.jasperreports.engine.JRDataSource;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("marService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MarServiceBean implements MarServiceLocal {

	@Logger
	private Log logger;	
	
	@In
	private ReportProvider<JRDataSource> reportProvider;	
	
	@In
	public MarServiceJmsRemote marServiceProxy;
	
	@In
	public MedProfile medProfile;
	
	private byte[] reportContent;
	
	@Out(required = false)
	public Boolean errorOnRetrieveMarRpt;
	
	@Out(required = false)
	public Boolean prescriptionNotFound;
	
    public MarServiceBean() {
    }
	
    @Override
	public void retrieveMarRpt() throws MarException {
    	
    	errorOnRetrieveMarRpt = Boolean.FALSE;
    	prescriptionNotFound = Boolean.FALSE;
    	try {
    		reportContent = marServiceProxy.retrieveMarContent(medProfile.getOrderNum(), medProfile.getMedCase().getCaseNum());
    		prescriptionNotFound = reportContent == null;
    	} catch (Exception ex) {
			logger.info(ex, ex);
			errorOnRetrieveMarRpt = Boolean.TRUE;
    	}
	}
    
    @Override
    public void showMarRpt() {
    	
    	String contentId = reportProvider.storeReport("marReport", reportContent, ReportProvider.PDF);
		reportProvider.redirectReport(contentId);
		reportContent = null;
    }

	@Remove
	@Override
	public void destroy() {
	}	    
}
