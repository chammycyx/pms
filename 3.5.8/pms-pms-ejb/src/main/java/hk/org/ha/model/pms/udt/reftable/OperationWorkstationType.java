package hk.org.ha.model.pms.udt.reftable;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum OperationWorkstationType implements StringValuedEnum {
	
	TicketGeneration("T", "Ticket Generation"),
	DataEntry("E", "Data Entry"),
	Picking("P", "Picking"),
	Assemble("A", "Assemble"),
	CheckAndIssue("C", "Check & Issue"),
	MultiFunction("#", "Multi Function"), 
	SendOut("S", "Send Out");

	private final String dataValue;
	private final String displayValue;
	
	OperationWorkstationType(final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}
	
	public static OperationWorkstationType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(OperationWorkstationType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<OperationWorkstationType> {

		private static final long serialVersionUID = 1L;

		public Class<OperationWorkstationType> getEnumClass() {
			return OperationWorkstationType.class;
		}

	}
	
}
