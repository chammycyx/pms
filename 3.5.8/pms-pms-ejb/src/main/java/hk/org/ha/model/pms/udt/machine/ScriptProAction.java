package hk.org.ha.model.pms.udt.machine;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum ScriptProAction implements StringValuedEnum {

	New ("N", "New"),
	Suspend("S", "Suspend"),
	Delete("D", "Delete");
	
	private final String dataValue;
	private final String displayValue;
	
	ScriptProAction (final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}
	
	public static ScriptProAction dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(ScriptProAction.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<ScriptProAction> {

		private static final long serialVersionUID = 1L;

		public Class<ScriptProAction> getEnumClass() {
    		return ScriptProAction.class;
    	}
    }
}
