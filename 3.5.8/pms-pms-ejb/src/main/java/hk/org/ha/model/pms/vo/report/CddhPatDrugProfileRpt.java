package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class CddhPatDrugProfileRpt implements Serializable {

	private static final long serialVersionUID = 1L;

	private CddhPatDrugProfileRptHdr cddhPatDrugProfileRptHdr;
	
	private List<CddhPatDrugProfileRptDtl> cddhPatDrugProfileRptDtlList;

	public CddhPatDrugProfileRptHdr getCddhPatDrugProfileRptHdr() {
		return cddhPatDrugProfileRptHdr;
	}

	public void setCddhPatDrugProfileRptHdr(
			CddhPatDrugProfileRptHdr cddhPatDrugProfileRptHdr) {
		this.cddhPatDrugProfileRptHdr = cddhPatDrugProfileRptHdr;
	}

	public List<CddhPatDrugProfileRptDtl> getCddhPatDrugProfileRptDtlList() {
		return cddhPatDrugProfileRptDtlList;
	}

	public void setCddhPatDrugProfileRptDtlList(List<CddhPatDrugProfileRptDtl> cddhPatDrugProfileRptDtlList) {
		this.cddhPatDrugProfileRptDtlList = cddhPatDrugProfileRptDtlList;
	}
	
}
