package hk.org.ha.model.pms.biz.report;

import static hk.org.ha.model.pms.prop.Prop.*;
import hk.org.ha.fmk.pms.util.JavaBeanResult;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.EdsStatusCount;
import hk.org.ha.model.pms.vo.report.EdsWorkloadRpt;
import hk.org.ha.model.pms.vo.report.EdsWorkloadRptDtl;
import hk.org.ha.model.pms.vo.report.EdsWorkloadRptHdr;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("edsWorkloadRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class EdsWorkloadRptServiceBean implements EdsWorkloadRptServiceLocal {
	@PersistenceContext
	private EntityManager em;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent; 
	
	@In
	private Workstore workstore;
	
	private boolean retrieveSuccess;
	
	private List<EdsWorkloadRpt> edsWorkloadRptList;
	
	private String currentRpt = null;
	
	private List<EdsWorkloadRpt> reportObj = null;
	
	private static final String RECEIVE_COUNT = "receive";
	private static final String ISSUE_COUNT = "issue";
	private static final String ORDER_LEVEL = "order";
	private static final String ITEM_LEVEL = "item";
	
	public void retrieveEdsWorkloadRpt(Integer selectedTab, Date date) {
		reportObj = retrieveEdsWorkloadRptList(date); 
		
		if (selectedTab == 0){
			currentRpt = "EdsWorkloadByRxChart";
		}
		else if (selectedTab == 1){
			currentRpt = "EdsWorkloadByItemChart";		
		}
		else if (selectedTab == 2){
			currentRpt = "EdsWorkloadRpt";
		}
		
		if ( !reportObj.isEmpty() ) {
			retrieveSuccess = true;
		}
		else{
			retrieveSuccess = false;
		}
	}
	
	public List<EdsWorkloadRpt> retrieveEdsWorkloadRptList(Date date) {
		edsWorkloadRptList = new ArrayList<EdsWorkloadRpt>();
		
		//Retrieve Rx Received
		Map<String, Long> orderMap = new HashMap<String, Long>();
		List<EdsStatusCount> orderStatusReceiveCountList = this.retrieveEdsStatusCount(date, ORDER_LEVEL, RECEIVE_COUNT); 
		for (EdsStatusCount e : orderStatusReceiveCountList) {
			orderMap.put( e.getTimeRange(), e.getCount());
		}
		
		//Retrieve Rx issued
		Map<String, Long> orderIssuedMap = new HashMap<String, Long>();
		List<EdsStatusCount> orderStatusIssueCountList = this.retrieveEdsStatusCount(date, ORDER_LEVEL, ISSUE_COUNT); 
		for (EdsStatusCount e : orderStatusIssueCountList) {
			orderIssuedMap.put( e.getTimeRange(), e.getCount());
		}
					
		//Retrieve item
		Map<String, Long> itemMap = new HashMap<String, Long>();
		List<EdsStatusCount> itemStatusReceiveCountList = this.retrieveEdsStatusCount(date, ITEM_LEVEL, RECEIVE_COUNT);  
		for (EdsStatusCount e : itemStatusReceiveCountList) {
			itemMap.put( e.getTimeRange(), e.getCount());
		}
				
		//Retrieve issued item 
		Map<String, Long> itemIssuedMap = new HashMap<String, Long>();
		List<EdsStatusCount> itemStatusIssueCountList = this.retrieveEdsStatusCount(date, ITEM_LEVEL, ISSUE_COUNT); 
		for (EdsStatusCount e : itemStatusIssueCountList) {
			itemIssuedMap.put( e.getTimeRange(), e.getCount());
		}
		
		// generate rpt List
		int openingHour = REPORT_WORKSTORE_OPENINGHOUR.get();	 
		int closingHour = REPORT_WORKSTORE_CLOSINGHOUR.get();

		List<EdsWorkloadRptDtl> edsWorkloadRptDtlList = new ArrayList<EdsWorkloadRptDtl>();		
		for(int i = openingHour; i < closingHour; i++) {
			EdsWorkloadRptDtl edsWorkloadRptDtl = new EdsWorkloadRptDtl();
			
			String startTime = StringUtils.leftPad(String.valueOf(i),2,'0');
			String endTime = StringUtils.leftPad(String.valueOf(i+1),2,'0');
			
			edsWorkloadRptDtl.setTimeRange(startTime + "-" + endTime);
			if (orderMap.containsKey(startTime)) {
				edsWorkloadRptDtl.setPrescReceivedCount(orderMap.get(startTime).intValue());
				
			}
			else {
				edsWorkloadRptDtl.setPrescReceivedCount(0);
			}
			
			if (orderIssuedMap.containsKey(startTime)) {
				edsWorkloadRptDtl.setPrescIssuedCount(orderIssuedMap.get(startTime).intValue());
				
			}
			else {
				edsWorkloadRptDtl.setPrescIssuedCount(0);
			}
			
			if (itemMap.containsKey(startTime)) {
				edsWorkloadRptDtl.setItemReceivedCount(itemMap.get(startTime).intValue());
				
			}
			else {
				edsWorkloadRptDtl.setItemReceivedCount(0);
			}
			
			if (itemIssuedMap.containsKey(startTime)) {
				edsWorkloadRptDtl.setItemIssuedCount(itemIssuedMap.get(startTime).intValue());
				
			}
			else {
				edsWorkloadRptDtl.setItemIssuedCount(0);
			}

			edsWorkloadRptDtlList.add(edsWorkloadRptDtl);
		}
		
		EdsWorkloadRpt edsWorkloadRpt = new EdsWorkloadRpt();
		edsWorkloadRpt.setEdsWorkloadRptDtlList(edsWorkloadRptDtlList);
		EdsWorkloadRptHdr edsWorkloadRptHdr = new EdsWorkloadRptHdr();
		
		OperationProfile op = em.find(OperationProfile.class, workstore.getOperationProfileId());
		edsWorkloadRptHdr.setActiveProfileName(op.getName());
		edsWorkloadRptHdr.setHospCode(workstore.getHospCode());
		edsWorkloadRptHdr.setWorkstoreCode(workstore.getWorkstoreCode());
		edsWorkloadRptHdr.setDate(date);
		edsWorkloadRpt.setEdsWorkloadRptHdr(edsWorkloadRptHdr);
		
		edsWorkloadRptList.add(edsWorkloadRpt);
		return edsWorkloadRptList;
	}

	private String constructRptQuery(String reportLevel, String reportType) {
		StringBuilder query = new StringBuilder();
		
		if(RECEIVE_COUNT.equals(reportType)) {
			query.append("select to_char(odo.create_date, 'HH24'), null ,count(*)"); // 20120313 index check : DispOrder.workstore,dispDate : I_DISP_ORDER_03
		} else if(ISSUE_COUNT.equals(reportType)) {
			query.append("select to_char(do.issue_date, 'HH24'), null ,count(*)"); // 20120313 index check : DispOrder.workstore,dispDate : I_DISP_ORDER_03
		}
		
		if(ORDER_LEVEL.equals(reportLevel)) {
			query.append(" from disp_order do, pharm_order po, med_order mo, disp_order odo");
		} else {
			query.append(" from disp_order_item doi, disp_order do, pharm_order po, med_order mo, disp_order odo");
		}
		
		query.append(" where do.pharm_order_id = po.id")
			 .append(" and po.med_order_id = mo.id")
			 .append(" and do.org_disp_order_id =  odo.id(+)")
			 .append(" and odo.workstore_code = ?1")
			 .append(" and odo.hosp_code = ?2")
			 .append(" and do.admin_status <> 'S'")
			 .append(" and mo.order_type = 'O'")
			 .append(" and odo.disp_date = ?3");
				
		if(ITEM_LEVEL.equals(reportLevel)) {
			query.append(" and doi.disp_order_id = do.id");
		}
		
		if(RECEIVE_COUNT.equals(reportType)) {
			query.append(" and po.status not in ('D','X')");
		} else if(ISSUE_COUNT.equals(reportType)) {
			query.append(" and do.status = 'I'")
				 .append(" and odo.disp_date = trunc(do.issue_date)");
		}
		
		if(RECEIVE_COUNT.equals(reportType)) {
			query.append(" group by to_char(odo.create_date, 'HH24')")
				 .append(" order by to_char(odo.create_date, 'HH24')");
		} else if(ISSUE_COUNT.equals(reportType)) {
			query.append(" group by to_char(do.issue_date, 'HH24')")
				 .append(" order by to_char(do.issue_date, 'HH24')");
		}
		
		return query.toString();
	}
	
	@SuppressWarnings("unchecked")
	private List<EdsStatusCount> retrieveEdsStatusCount(final Date date, String reportLevel, String reportType) {
		Query orderQuery = em.createNativeQuery( this.constructRptQuery(reportLevel, reportType) );
		
		JavaBeanResult.setQueryResultClass(orderQuery,  EdsStatusCount.class);
		
		return orderQuery.setParameter(1, workstore.getWorkstoreCode())
				.setParameter(2, workstore.getHospCode())
				.setParameter(3, date, TemporalType.DATE)
				.getResultList(); 
	}	
	
	public void generateEdsWorkloadRpt() {

		Map<String, Object> parameters = new HashMap<String, Object>();
		
		if ("EdsWorkloadByItemChart".equals(currentRpt) || "EdsWorkloadByRxChart".equals(currentRpt)) {
			parameters.put("_PNG_ZOOM_RATIO", Float.valueOf(1f));
			printAgent.renderAndRedirect(new RenderJob(
					currentRpt + "Preview", 
					new PrintOption(), 
					parameters, 
					reportObj.get(0).getEdsWorkloadRptDtlList()));
		} else {
			printAgent.renderAndRedirect(new RenderJob(
					currentRpt, 
					new PrintOption(), 
					parameters, 
					reportObj));
		}
	}
	
	public void printEdsWorkloadRpt(){

		printAgent.renderAndPrint(new RenderAndPrintJob(
				currentRpt, 
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report),
				new HashMap<String, Object>(), 
				reportObj));
		
	}

	@Remove
	public void destroy() {
		if(edsWorkloadRptList!=null){
			edsWorkloadRptList=null;
		}
	}

	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}
	
}
