package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.List;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class DelCapdVoucherRpt implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private DelCapdVoucherRptHdr delCapdVoucherRptHdr;
	
	private List<DelCapdVoucherRptDtl> delCapdVoucherRptDtlList;

	private DelCapdVoucherRptFtr delCapdVoucherRptFtr;

	public DelCapdVoucherRptHdr getDelCapdVoucherRptHdr() {
		return delCapdVoucherRptHdr;
	}

	public void setDelCapdVoucherRptHdr(
			DelCapdVoucherRptHdr delCapdVoucherRptHdr) {
		this.delCapdVoucherRptHdr = delCapdVoucherRptHdr;
	}

	public List<DelCapdVoucherRptDtl> getDelCapdVoucherRptDtlList() {
		return delCapdVoucherRptDtlList;
	}

	public void setDelCapdVoucherRptDtlList(
			List<DelCapdVoucherRptDtl> delCapdVoucherRptDtlList) {
		this.delCapdVoucherRptDtlList = delCapdVoucherRptDtlList;
	}

	public DelCapdVoucherRptFtr getDelCapdVoucherRptFtr() {
		return delCapdVoucherRptFtr;
	}

	public void setDelCapdVoucherRptFtr(
			DelCapdVoucherRptFtr delCapdVoucherRptFtr) {
		this.delCapdVoucherRptFtr = delCapdVoucherRptFtr;
	}
	
}