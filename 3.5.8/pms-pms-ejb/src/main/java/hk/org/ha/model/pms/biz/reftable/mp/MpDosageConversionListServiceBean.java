package hk.org.ha.model.pms.biz.reftable.mp;

import flexjson.JSONSerializer;
import flexjson.transformer.DateTransformer;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.PmsIpDosageConv;
import hk.org.ha.model.pms.dms.persistence.PmsIpDosageConvSet;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.reftable.mp.DosageConversionType;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.NoResultException;
import javax.persistence.OptimisticLockException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("mpDosageConversionListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MpDosageConversionListServiceBean implements MpDosageConversionListServiceLocal {

	private static final String PRESCRIBE_DOSAGE_FROM = "Prescribe dosage from";
	private static final String PRESCRIBE_DOSAGE_TO = "Prescribe dosage to";
	private static final String MAX_DOSAGE_VALUE = "9999999.999";
		
	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
	
	private static final JSONSerializer serializer = new JSONSerializer().transform(new DateTransformer(DATE_FORMAT), Date.class);
	
	@In
	private AuditLogger auditLogger;
	
	private List<PmsIpDosageConvSet> pmsIpDosageConvSetList;
	
	@Out(required = false)
	private PmsIpDosageConvSet tempPmsIpDosageConvSet;
	
	@In
	private Workstore workstore;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;
	
	private List<WorkstoreDrug> extendDmDrugList;
	
	private List<WorkstoreDrug> originalDmDrugList;
	
	private boolean saveSuccess = true;
	
	private String errorCode = "";
	
	private String[] errorParam = {""};
	
	private boolean retrieveSuccess;
	
	public List<PmsIpDosageConvSet> retrievePmsIpDosageConvSetList(DmDrug dmDrug) {
		retrieveSuccess = true;
		
		try{
			pmsIpDosageConvSetList = dmsPmsServiceProxy.retrievePmsIpDosageConvSetList(
					workstore.getHospCode(), 
					workstore.getWorkstoreCode(),
					dmDrug.getDrugKey(), 
					dmDrug.getPmsFmStatus().getFmStatus());
			retrieveDmDrugListByDrugKey(dmDrug);
			retrieveDmDrugListByDisplayname(dmDrug);
			
			for( PmsIpDosageConvSet pmsIpDosageConvSet : pmsIpDosageConvSetList ){
				pmsIpDosageConvSet.setDmDrug(dmDrug);
				setPmsIpDosageConv( pmsIpDosageConvSet );
				for( PmsIpDosageConv pmsIpDosageConv : pmsIpDosageConvSet.getPmsIpDosageConvList() ){
					pmsIpDosageConv.setPmsIpDosageConvSet( pmsIpDosageConvSet );
				}
			}
			
			getTempPmsIpDosageConvSet(dmDrug);
		} catch (NoResultException e){
			retrieveSuccess = false;
		}
		
		return pmsIpDosageConvSetList;
	}
	
	private void getTempPmsIpDosageConvSet(DmDrug dmDrug){
		tempPmsIpDosageConvSet = new PmsIpDosageConvSet();
		tempPmsIpDosageConvSet.setMoDosageFrom( 0.0 );
		tempPmsIpDosageConvSet.setMoDosageTo( 0.0 );
		tempPmsIpDosageConvSet.setMoDosageUnit(dmDrug.getDmMoeProperty().getMoDosageUnit());
		tempPmsIpDosageConvSet.setDmDrug(dmDrug);
		
		List<PmsIpDosageConv> tempPmsIpDosageConvList = new ArrayList<PmsIpDosageConv>();
		
		List<String> originalDmDrugItemCodeList = new ArrayList<String>();
		if( !originalDmDrugList.isEmpty() ){
			for(WorkstoreDrug workstoreDrug : originalDmDrugList){
				originalDmDrugItemCodeList.add(workstoreDrug.getDmDrug().getItemCode());
			}
		}
		
		if( !extendDmDrugList.isEmpty() ){
			for(WorkstoreDrug workstoreDrug : extendDmDrugList){
				if( StringUtils.isEmpty(workstoreDrug.getDmDrug().getDmMoeProperty().getDispenseDosageUnit()) ){
					continue;
				}
				
				DmDrug findDmDrug = workstoreDrug.getDmDrug();
				PmsIpDosageConv pmsIpDosageConv = new PmsIpDosageConv();
				pmsIpDosageConv.setDmDrug(findDmDrug);
				pmsIpDosageConv.setItemCode( findDmDrug.getItemCode() );
				pmsIpDosageConv.setFixed(null);
				
				if( findDmDrug.getDmMoeProperty() != null ){
					pmsIpDosageConv.setDispenseDosageUnit( findDmDrug.getDmMoeProperty().getDispenseDosageUnit() );
					pmsIpDosageConv.setFullDrugDesc( findDmDrug.getFullDrugDesc() );
				}
				pmsIpDosageConv.setSuspend( !workstoreDrug.getMsWorkstoreDrug().isAvailable() );
				if( !originalDmDrugItemCodeList.contains( pmsIpDosageConv.getItemCode() ) ){
					pmsIpDosageConv.setExtendedItem(true);
				}
				tempPmsIpDosageConvList.add(pmsIpDosageConv);
			}
		}
		
		tempPmsIpDosageConvSet.setPmsIpDosageConvList(tempPmsIpDosageConvList);
	}
	
	private void setPmsIpDosageConv(PmsIpDosageConvSet pmsIpDosageConvSet){
		for(PmsIpDosageConv p :pmsIpDosageConvSet.getPmsIpDosageConvList()){
			WorkstoreDrug workstoreDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, p.getItemCode());
			if( workstoreDrug != null ){
				p.setDmDrug(workstoreDrug.getDmDrug());	
				if( workstoreDrug.getMsWorkstoreDrug() != null ){
					p.setMsWorkstoreDrug( workstoreDrug.getMsWorkstoreDrug() );
					p.setSuspend( !workstoreDrug.getMsWorkstoreDrug().isAvailable() );
				}
			}else{
				p.setDmDrug( null );
			}
		}
	}
	
	private void retrieveDmDrugListByDrugKey(DmDrug dmDrug) {
		originalDmDrugList = workstoreDrugCacher.getWorkstoreDrugListByDrugKey(
				workstore,
				dmDrug.getDrugKey());
		
		Collections.sort(originalDmDrugList, new WorkstoreDrugComparator());
	}
	
	private void retrieveDmDrugListByDisplayname(DmDrug dmDrug) {
		extendDmDrugList = workstoreDrugCacher.getWorkstoreDrugListByDisplayName(
				workstore,
				dmDrug.getDmDrugProperty().getDisplayname());
		
		Collections.sort(extendDmDrugList, new WorkstoreDrugComparator());
	}
	
	public void updatePmsIpDosageConvSetList(List<PmsIpDosageConvSet> pmsIpDosageConvSetList, List<PmsIpDosageConvSet> markDeletePmsIpDosageConvSetList) {
		if ( isValidDosageConversionSetList(pmsIpDosageConvSetList) ) {
			List<PmsIpDosageConvSet> savePmsIpDosageConvSetList = new ArrayList<PmsIpDosageConvSet>(); 
			savePmsIpDosageConvSetList.addAll(pmsIpDosageConvSetList);
			savePmsIpDosageConvSetList.addAll(markDeletePmsIpDosageConvSetList);
			
			for(PmsIpDosageConvSet pmsIpDosageConvSet : savePmsIpDosageConvSetList){
				pmsIpDosageConvSet.setDispHospCode(workstore.getHospCode());
				pmsIpDosageConvSet.setDispWorkstore(workstore.getWorkstoreCode());
			}
		
			try{
				saveSuccess = true;
				dmsPmsServiceProxy.updatePmsIpDosageConvSet(savePmsIpDosageConvSetList);
			}catch(Exception e){
				if( e.getCause() instanceof OptimisticLockException ){
					errorCode = "0307";
				}else{
					errorCode = "0729";
				}
				saveSuccess = false;
			}
			auditLogger.log("#0992", serializer.exclude("dmDrug","pmsIpDosageConvList.dmDrug").include("pmsIpDosageConvList").serialize(savePmsIpDosageConvSetList));
		}
	}
	
	private Boolean isValidDosageConversionSetList( List<PmsIpDosageConvSet> pmsIpDosageConvSetList ){
		int countManageSet = 1;
		for(PmsIpDosageConvSet pmsIpDosageConvSet : pmsIpDosageConvSetList){
			if( pmsIpDosageConvSet.isMarkDelete() ){
				continue;
			}
			if( !StringUtils.equals(pmsIpDosageConvSet.getMoDosageUnit(), pmsIpDosageConvSet.getDmDrug().getDmMoeProperty().getMoDosageUnit() ) ){
				saveSuccess = false;
				errorCode = "0090";
				return false;
			}
			for(PmsIpDosageConv pdc :pmsIpDosageConvSet.getPmsIpDosageConvList()){
				if  ( !pdc.isMarkDelete() && !StringUtils.equals(pdc.getDmDrug().getDmMoeProperty().getDispenseDosageUnit(), pdc.getDispenseDosageUnit()) ) {
					saveSuccess = false;
					errorCode = "0558";
					return false;
				}
			}
			if( pmsIpDosageConvSet.getMoDosageFrom() > 9999999.999 ){
				saveSuccess = false;
				errorCode = "0120";
				errorParam = new String[]{PRESCRIBE_DOSAGE_FROM, MAX_DOSAGE_VALUE};
				return false;
			}
			if( pmsIpDosageConvSet.getMoDosageFrom() < 0 ){
				saveSuccess = false;
				errorCode = "0117";
				errorParam = new String[]{PRESCRIBE_DOSAGE_FROM};
				return false;
			}
			if( pmsIpDosageConvSet.getMoDosageTo() != null && pmsIpDosageConvSet.getMoDosageTo() > 9999999.999 ){
				saveSuccess = false;
				errorCode = "0120";
				errorParam = new String[]{PRESCRIBE_DOSAGE_TO, MAX_DOSAGE_VALUE};
				return false;
			}
			if( ( pmsIpDosageConvSet.getMoDosageFrom() != null && pmsIpDosageConvSet.getMoDosageFrom()%1 != 0 
					&& ( pmsIpDosageConvSet.getMoDosageFrom()%1 < 0.001 || pmsIpDosageConvSet.getMoDosageFrom()%1 > 0.999 ) ) ||
				( pmsIpDosageConvSet.getMoDosageTo() != null && pmsIpDosageConvSet.getMoDosageTo()%1 != 0 
						&& ( pmsIpDosageConvSet.getMoDosageTo()%1 < 0.001 || pmsIpDosageConvSet.getMoDosageTo()%1 > 0.999 ) ) ){
				saveSuccess = false;
				errorCode = "0275";
				return false;
			}
			int countItem = 0;
			int countVariable = 0;
			for( PmsIpDosageConv pdc :pmsIpDosageConvSet.getPmsIpDosageConvList() ){
				countItem++;
				if( StringUtils.isEmpty( pdc.getFixed() ) ){
					saveSuccess = false;
					errorCode = "0123";
					if( pmsIpDosageConvSet.getMoDosageTo() != null && pmsIpDosageConvSet.getMoDosageTo()>0 ){
						errorParam = new String[]{ ""+pmsIpDosageConvSet.getMoDosageFrom(), ""+pmsIpDosageConvSet.getMoDosageTo() };
					}else{
						errorParam = new String[]{ ""+pmsIpDosageConvSet.getMoDosageFrom() };
					}
					return false;
				}
				if( DosageConversionType.Variable.getDataValue().equals( pdc.getFixed() ) ){
					countVariable++;
				}else{
					if( pdc.getDispenseDosage() > 9999999.999 ){
						saveSuccess = false;
						errorCode = "0120";
						errorParam = new String[]{"Dispensing dosage", "999999.999"};
						return false;
					}
					if( pdc.getDispenseDosage() == null || pdc.getDispenseDosage() < 0 ){
						saveSuccess = false;
						errorCode = "0117";
						errorParam = new String[]{"Dispensing dosage"};
						return false;
					}
					if( pdc.getDispenseDosage()%1 != 0 && ( pdc.getDispenseDosage()%1 < 0.001 || pdc.getDispenseDosage()%1 > 0.999 ) ){
						saveSuccess = false;
						errorCode = "0275";
						return false;
					}
				}
			}
			if( countItem == 0 ){
				saveSuccess = false;
				errorCode = "0323";
				return false;
			}
			if( countVariable != 1 ){
				saveSuccess = false;
				errorCode = "0094";
				return false;
			}
			
			int countTargetSet = 0;
			for(PmsIpDosageConvSet pdcs : pmsIpDosageConvSetList){
				countTargetSet++;
				if( countManageSet == countTargetSet ){
					continue;
				}	
				if( pmsIpDosageConvSet.getMoDosageTo() == null || pmsIpDosageConvSet.getMoDosageTo() <= 0 ){
					if( pdcs.getMoDosageTo() == null || pdcs.getMoDosageTo() <= 0 ){
						if( pmsIpDosageConvSet.getMoDosageFrom().compareTo(pdcs.getMoDosageFrom()) != 0 ){
							continue;
						}
					}else{
						if( pmsIpDosageConvSet.getMoDosageFrom() < pdcs.getMoDosageFrom() ||
							pmsIpDosageConvSet.getMoDosageFrom() >= pdcs.getMoDosageTo() ){
							continue;
						}
					}
				}else{
					if( pdcs.getMoDosageTo() == null || pdcs.getMoDosageTo() <= 0 ){
						if( pmsIpDosageConvSet.getMoDosageTo() <= pdcs.getMoDosageFrom() ||
							pmsIpDosageConvSet.getMoDosageFrom() > pdcs.getMoDosageFrom() ){
							continue;
						}
					}else{
						if( pmsIpDosageConvSet.getMoDosageTo() <= pdcs.getMoDosageFrom() ||
							pmsIpDosageConvSet.getMoDosageFrom() >= pdcs.getMoDosageTo() ){
							continue;
						}
					}
				}				
				saveSuccess = false;
				errorCode = "0325";
				return false;
			}
			countManageSet++;
		}
		saveSuccess = true;
		errorCode = "";
		return true;
		
	}
	
	public boolean isRetrieveSuccess(){
		return retrieveSuccess;
	}

	public boolean isSaveSuccess() {
		return saveSuccess;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String[] getErrorParam() {
		return errorParam;
	}

	public static class WorkstoreDrugComparator implements Comparator<WorkstoreDrug>, Serializable {

		private static final long serialVersionUID = 1L;
		
		@Override
		public int compare(WorkstoreDrug wsDrug1, WorkstoreDrug wsDrug2) {
			CompareToBuilder wsDrugCompareTobuilder = new CompareToBuilder();
			
			wsDrugCompareTobuilder.append( wsDrug1.getDmDrug().getDrugName(), wsDrug2.getDmDrug().getDrugName());
			
			//Form Label Desc
			String wsDrug1LabelDesc = null;
			if( wsDrug1.getDmDrug().getDmForm() != null ){
				wsDrug1LabelDesc = wsDrug1.getDmDrug().getDmForm().getLabelDesc();
			}
			String wsDrug2LabelDesc = null;
			if( wsDrug2.getDmDrug().getDmForm() != null ){
				wsDrug2LabelDesc = wsDrug2.getDmDrug().getDmForm().getLabelDesc();
			}
			wsDrugCompareTobuilder.append(wsDrug1LabelDesc, wsDrug2LabelDesc);
			
			//Strength
			String wsDrug1StrengthUnit = null;
			Double wsDrug1StrengthValue = null;
			if( wsDrug1.getDmDrug().getDmMoeProperty() != null ){
				wsDrug1StrengthUnit = wsDrug1.getDmDrug().getDmMoeProperty().getStrengthUnit();
				wsDrug1StrengthValue = wsDrug1.getDmDrug().getDmMoeProperty().getStrengthValue();
			}
			String wsDrug2StrengthUnit = null;
			Double wsDrug2StrengthValue = null;
			if( wsDrug2.getDmDrug().getDmMoeProperty() != null ){
				wsDrug2StrengthUnit = wsDrug2.getDmDrug().getDmMoeProperty().getStrengthUnit();
				wsDrug2StrengthValue = wsDrug2.getDmDrug().getDmMoeProperty().getStrengthValue();
			}
			wsDrugCompareTobuilder.append( wsDrug1StrengthUnit, wsDrug2StrengthUnit);
			wsDrugCompareTobuilder.append( wsDrug1StrengthValue, wsDrug2StrengthValue);
			
			//Volume
			wsDrugCompareTobuilder.append( wsDrug1.getDmDrug().getVolumeUnit(), wsDrug2.getDmDrug().getVolumeUnit());
			wsDrugCompareTobuilder.append( wsDrug1.getDmDrug().getVolumeValue(), wsDrug2.getDmDrug().getVolumeValue());
			
			//ItemCode
			wsDrugCompareTobuilder.append( wsDrug1.getDmDrug().getItemCode(), wsDrug2.getDmDrug().getItemCode());
			
			return wsDrugCompareTobuilder.toComparison();
		}
	}
	
	@Remove
	public void destroy() {
		if (originalDmDrugList != null) {
			originalDmDrugList = null;
		}
		if (extendDmDrugList != null) {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
			extendDmDrugList = null;
		}
		if (pmsIpDosageConvSetList != null) {
			pmsIpDosageConvSetList = null;
		}
		if (tempPmsIpDosageConvSet != null) {
			tempPmsIpDosageConvSet = null;
		}
		if ( StringUtils.isNotEmpty(errorCode) ){
			errorCode = "";
		}
		if (errorParam.length > 0){
			errorParam = new String[]{};
		}
	}	
}
