package hk.org.ha.model.pms.vo.charging;

import hk.org.ha.model.pms.udt.YesNoBlankFlag;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class MoiChargeInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer medOrderItemIndex;
	
	private Integer unitOfCharge;
		
	private YesNoBlankFlag cmsChargeFlag;
	
	private YesNoBlankFlag chargeFlag;
	
	private Integer durationInDay;
	
	private boolean changeFlag;
	
	private List<MoiCapdChargeInfo> moiCapdChargeInfoList;
		
	public MoiChargeInfo(){
		this.unitOfCharge = 0;
		this.cmsChargeFlag = YesNoBlankFlag.No;
		this.chargeFlag = YesNoBlankFlag.No;
		this.setChangeFlag(false);
	}
	
	public Integer getMedOrderItemIndex() {
		return medOrderItemIndex;
	}

	public void setMedOrderItemIndex(Integer medOrderItemIndex) {
		this.medOrderItemIndex = medOrderItemIndex;
	}

	public Integer getUnitOfCharge() {
		return unitOfCharge;
	}

	public void setUnitOfCharge(Integer unitOfCharge) {
		this.unitOfCharge = unitOfCharge;
	}

	public void setCmsChargeFlag(YesNoBlankFlag moeChargeFlag) {
		this.cmsChargeFlag = moeChargeFlag;
	}

	public YesNoBlankFlag getCmsChargeFlag() {
		return cmsChargeFlag;
	}

	public void setChargeFlag(YesNoBlankFlag chargeFlag) {
		this.chargeFlag = chargeFlag;
	}

	public YesNoBlankFlag getChargeFlag() {
		return chargeFlag;
	}

	public void setDurationInDay(Integer durationInDay) {
		this.durationInDay = durationInDay;
	}

	public Integer getDurationInDay() {
		return durationInDay;
	}
	
	public void setMoiCapdChargeInfoList(List<MoiCapdChargeInfo> moiCapdChargeInfoList) {
		this.moiCapdChargeInfoList = moiCapdChargeInfoList;
	}

	public List<MoiCapdChargeInfo> getMoiCapdChargeInfoList() {
		if( moiCapdChargeInfoList == null ){
			moiCapdChargeInfoList = new ArrayList<MoiCapdChargeInfo>();
		}
		return moiCapdChargeInfoList;
	}
	
	public void addMoiCapdChargeInfo(MoiCapdChargeInfo moiCapdChargeInfo) {		
		getMoiCapdChargeInfoList().add(moiCapdChargeInfo);
	}

	public void setChangeFlag(boolean changeFlag) {
		this.changeFlag = changeFlag;
	}

	public boolean isChangeFlag() {
		return changeFlag;
	}
}
