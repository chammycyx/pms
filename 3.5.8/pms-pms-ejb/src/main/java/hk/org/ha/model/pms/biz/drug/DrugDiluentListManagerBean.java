package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.cache.CacheResult;
import hk.org.ha.fmk.pms.remote.marshall.EclipseLinkXStreamMarshaller;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.dms.vo.Diluent;
import hk.org.ha.model.pms.dms.vo.DiluentCriteria;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.InjectionRxDrug;
import hk.org.ha.model.pms.vo.rx.IvFluid;
import hk.org.ha.model.pms.vo.rx.IvRxDrug;
import hk.org.ha.model.pms.vo.rx.Regimen;
import hk.org.ha.model.pms.vo.rx.RxItem;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("drugDiluentListManager")
@MeasureCalls
public class DrugDiluentListManagerBean implements DrugDiluentListManagerLocal {

	@Logger
	private Log logger;

	@In
	private Workstore workstore;

	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	private WorkstoreDrugComparator workstoreDrugComparator = new WorkstoreDrugComparator();
	
	@CacheResult(timeout = "360000")
	public List<WorkstoreDrug> getDrugDiluentList(RxItem rxItem) {

		if ( rxItem instanceof IvRxDrug ) {
			return getDrugDiluentListByIvRxDrug((IvRxDrug) rxItem);
		}

		if ( rxItem instanceof InjectionRxDrug ) {
			return getDrugListByInjectionRxDrug((InjectionRxDrug) rxItem);
		}		

		return null;
	}

	private List<WorkstoreDrug> getDrugDiluentListByIvRxDrug(IvRxDrug ivRxDrug) {

		if ( ivRxDrug.getIvFluidList() == null || ivRxDrug.getIvFluidList().size() == 0 ) {
			return null;
		}
	
		Map<String, Boolean> availableDiluentCodeMap = new HashMap<String, Boolean>();
		for ( IvFluid ivFluid : ivRxDrug.getIvFluidList() ) {
			availableDiluentCodeMap.put(ivFluid.getDiluentCode(), Boolean.TRUE);
		}
		return buildDiluentDrugList(availableDiluentCodeMap);
	}

	private List<WorkstoreDrug> getDrugListByInjectionRxDrug(InjectionRxDrug rxItem) {
		Map<String, Boolean> availableDiluentCodeMap = new HashMap<String, Boolean>();
		Regimen regimen = ((InjectionRxDrug) rxItem).getRegimen();
		for (DoseGroup doseGroup:regimen.getDoseGroupList()) {
			for (Dose dose:doseGroup.getDoseList()) {
				if (dose.getDoseFluid()!=null) {
					availableDiluentCodeMap.put(dose.getDoseFluid().getDiluentCode(), Boolean.TRUE);
				}
			}
		}
		
		return buildDiluentDrugList(availableDiluentCodeMap); 
	}

	private List<WorkstoreDrug> buildDiluentDrugList(Map<String, Boolean> availableDiluentCodeMap) {
		List<WorkstoreDrug> drugList = new ArrayList<WorkstoreDrug>();
		List<Diluent> diluentList = getDiluentListByWorkstore( workstore );
		logger.debug("buildDiluentDrugList - diluentList.size() | #0", diluentList.size());
		
		for ( Diluent diluent : diluentList ) {
			
			if ( availableDiluentCodeMap.get(diluent.getDiluentCode()) == null ) {
				logger.debug("getDrugDiluentListByIvRxDrug - diluent.getDiluentCode |#0| not exist",diluent.getDiluentCode());
				continue;
			}
			if ( diluent.getDrugs().length == 0 ) {
				logger.debug("getDrugDiluentListByIvRxDrug - diluent.getDrugs().length |#0",diluent.getDrugs().length);
				continue;
			}
			for ( int i=0; i<diluent.getDrugs().length; i++ ) {
				drugList.add( workstoreDrugCacher.getWorkstoreDrug(workstore, diluent.getDrugs()[i].getItemCode()) );
			}
		}		
		retrieveDrugDiluentListWithExtraInfo(drugList);
		
		return drugList;
	}
	
	private void retrieveDrugDiluentListWithExtraInfo(List<WorkstoreDrug> drugList) {
		for ( WorkstoreDrug drug : drugList ) {
			drug.getDmDrug().getFullDrugDesc();
			drug.getDmDrug().getVolumeText();
		}
		Collections.sort(drugList,workstoreDrugComparator);
	}	
	
	private List<Diluent> getDiluentListByWorkstore(Workstore workstore) {
		DiluentCriteria criteria = new DiluentCriteria();
		criteria.setDispHospCode(workstore.getHospCode());
		criteria.setDispWorkstore(workstore.getWorkstoreCode());	
		logger.debug("retrieveDiluentDrugListByRxItem - criteria |#0",(new EclipseLinkXStreamMarshaller()).toXML(criteria));
		return dmsPmsServiceProxy.getAvailableDiluent(criteria);
	}
}
