package hk.org.ha.model.pms.biz.reftable.pivas;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.cache.DrugSiteMappingCacherInf;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.corp.cache.DmSiteCacherInf;
import hk.org.ha.model.pms.corp.cache.DmWarningCacherInf;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDiluent;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmDrugProperty;
import hk.org.ha.model.pms.dms.persistence.DmSite;
import hk.org.ha.model.pms.dms.persistence.DmWarning;
import hk.org.ha.model.pms.dms.persistence.MsWorkstoreDrug;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrug;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrugManuf;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrugManufMethod;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormula;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaConfig;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaDrug;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaMethod;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaMethodDrug;
import hk.org.ha.model.pms.dms.udt.pivas.PivasDrugManufStatus;
import hk.org.ha.model.pms.dms.udt.pivas.PivasFormulaMethodStatus;
import hk.org.ha.model.pms.dms.udt.pivas.PivasFormulaType;
import hk.org.ha.model.pms.dms.vo.Drug;
import hk.org.ha.model.pms.dms.vo.DrugRouteCriteria;
import hk.org.ha.model.pms.dms.vo.DrugSiteMapping;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.drug.DmDrugLite;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.reftable.pivas.PivasDrugInfo;
import hk.org.ha.model.pms.vo.reftable.pivas.PivasDrugListInfo;
import hk.org.ha.model.pms.vo.reftable.pivas.PivasFormulaInfo;
import hk.org.ha.model.pms.vo.report.PivasFormulaMaintRpt;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("pivasFormulaService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PivasFormulaServiceBean implements PivasFormulaServiceLocal {

	@In
	private Workstore workstore;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private DrugSiteMappingCacherInf drugSiteMappingCacher;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private DmSiteCacherInf dmSiteCacher;
	
	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;

	@In
	private DmWarningCacherInf dmWarningCacher;
	
	@In
	private PrintAgentInf printAgent; 

	@Logger
	private Log logger;
	
	private String errorCode;
	
	private List<PivasFormulaMaintRpt> pivasFormulaMaintRptList;
	
	private static PivasDrugInfoComparator pivasDrugInfoComparator = new PivasDrugInfoComparator();
	
	private static PivasFormulaMethodComparator pivasFormulaMethodComparator = new PivasFormulaMethodComparator();

	public List<PivasFormula> retrievePivasFormulaList(Integer drugKey, String siteCode, String diluentCode, String pivasDosageUnit, BigDecimal concn, String strength, BigDecimal rate) {
		return sortPivasFormulaMethod(resolveDmDrug(dmsPmsServiceProxy.retrievePivasFormulaList(
				workstore.getWorkstoreGroup().getWorkstoreGroupCode(), drugKey, siteCode, diluentCode, pivasDosageUnit, concn, strength, rate, workstore.getHospCode())));
	}
	
	private List<PivasFormula> resolveDmDrug(List<PivasFormula> pivasFormulaList) {
		
		for (PivasFormula pivasFormula: pivasFormulaList) {
			if (pivasFormula.getType() != PivasFormulaType.Merge) {
				continue;
			}
			
			for (PivasFormulaMethod pivasFormulaMethod: pivasFormula.getPivasFormulaMethodList()) {
				for (PivasFormulaMethodDrug pivasFormulaMethodDrug: pivasFormulaMethod.getPivasFormulaMethodDrugList()) {
					PivasDrug pivasDrug = pivasFormulaMethodDrug.getPivasDrugManufMethod().getPivasDrugManuf().getPivasDrug();
					pivasDrug.setDmDrug(dmDrugCacher.getDmDrug(pivasDrug.getItemCode()));
				}
			}
		}
		return pivasFormulaList;
	}
	
	private List<PivasFormula> sortPivasFormulaMethod(List<PivasFormula> pivasFormulaList) {

		for (PivasFormula pivasFormula: pivasFormulaList) {
			if (pivasFormula.getType() == PivasFormulaType.Merge) {
				Collections.sort(pivasFormula.getPivasFormulaMethodList(), pivasFormulaMethodComparator);
			}
		}
		return pivasFormulaList;
	}
	
	public PivasFormula updatePivasFormula(PivasFormula pivasFormula) {

		errorCode = null;
		
		PivasFormulaMethod coreMethod = pivasFormula.getCorePivasFormulaMethod();
		if (coreMethod != null) {
			errorCode = checkItemSuspend(coreMethod, pivasFormula.getType());
		}
		
		if (errorCode != null) {
			return null;
		}
		
		if (pivasFormula.getId() == null) {
			pivasFormula.setWorkstoreGroupCode(workstore.getWorkstoreGroup().getWorkstoreGroupCode());
		}
		
		PivasFormula result = dmsPmsServiceProxy.updatePivasFormula(pivasFormula, workstore.getHospCode());
		if (pivasFormula.getType() == PivasFormulaType.Merge) {
			if (result == null) {
				errorCode = "0954";
			}
			return null;
		} else {
			return result;
		}
	}
	
	private String checkItemSuspend(PivasFormulaMethod coreMethod, PivasFormulaType type) {

		if (type == PivasFormulaType.Merge) {
		
			for (PivasFormulaMethodDrug pivasFormulaMethodDrug: coreMethod.getPivasFormulaMethodDrugList()) {
				String itemCode = pivasFormulaMethodDrug.getPivasDrugManufMethod().getPivasDrugManuf().getPivasDrug().getItemCode();
				MsWorkstoreDrug msWorkstoreDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, itemCode).getMsWorkstoreDrug();
				WorkstoreDrug workstoreDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, itemCode);
				if (msWorkstoreDrug == null || !"A".equals(msWorkstoreDrug.getItemStatus()) || workstoreDrug == null || workstoreDrug.getDmDrug() == null || "Y".equals(workstoreDrug.getDmDrug().getHqSuspend())) {
					return "0841";
				}
			}
			return null;
			
		} else {

			String itemCode = coreMethod.getFirstPivasFormulaMethodDrug().getPivasDrugManufMethod().getPivasDrugManuf().getPivasDrug().getItemCode();
			MsWorkstoreDrug msWorkstoreDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, itemCode).getMsWorkstoreDrug();
			WorkstoreDrug workstoreDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, itemCode);
			if (msWorkstoreDrug == null || !"A".equals(msWorkstoreDrug.getItemStatus()) || workstoreDrug == null || workstoreDrug.getDmDrug() == null || "Y".equals(workstoreDrug.getDmDrug().getHqSuspend())) {
				return "0841";
			}
			else {
				return null;
			}
		}
	}
	
	public Map<String, DmDrugLite> retrieveDmDrugLiteMap(List<String> itemCodeList) {
		
		Map<String, DmDrugLite> resultMap = new HashMap<String, DmDrugLite>();
		
		for (String itemCode: itemCodeList) {
			WorkstoreDrug workstoreDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, itemCode);
			if (workstoreDrug != null) {
				resultMap.put(itemCode, workstoreDrug.getDmDrugLite());
			}
		}
		
		return resultMap;
	}
	
	public boolean checkAnotherActivePivasMergeFormulaExists(PivasFormula pivasFormula) {
		return dmsPmsServiceProxy.checkAnotherActivePivasMergeFormulaExists(pivasFormula);
	}

	public void deletePivasFormula(PivasFormula pivasFormula) {
		dmsPmsServiceProxy.deletePivasFormula(pivasFormula);
	}

	public List<PivasFormula> retrieveDrugInfoListByDisplayName(String displayName) {
		return dmsPmsServiceProxy.retrieveDrugInfoListByDisplayName(displayName, workstore.getWorkstoreGroup().getWorkstoreGroupCode());
	}

	public PivasFormulaInfo retrievePivasFormulaInfo(Integer drugKey, boolean includeNonPivasSite) {

		PivasFormulaInfo pivasFormulaInfo = new PivasFormulaInfo();

		Set<String> pivasSiteCode = new HashSet<String>();
		Set<String> pivasDosageUnit = new TreeSet<String>();
		Set<String> strength = new TreeSet<String>();
		
		boolean noSolventRequiredExist = false;
		for (PivasDrug drug : dmsPmsServiceProxy.retrievePivasDrugList(workstore.getWorkstoreGroup().getWorkstoreGroupCode(), drugKey, workstore.getHospCode())) {
			boolean activeManufExist = false;
			for (PivasDrugManuf manuf : drug.getPivasDrugManufList()) {
				if (manuf.getStatus() != PivasDrugManufStatus.Active) {
					continue;
				}
				activeManufExist = true;
				if (manuf.getRequireSolventFlag()) {
					for (PivasDrugManufMethod method : manuf.getPivasDrugManufMethodList()) {
						pivasSiteCode.addAll(method.getSiteCodes());
					}
				}
				else {
					noSolventRequiredExist = true;
				}
			}
			if (activeManufExist) {
				pivasDosageUnit.add(drug.getPivasDosageUnit());
				strength.add(StringUtils.trimToEmpty(dmDrugCacher.getDmDrug(drug.getItemCode()).getStrength()));
				logger.debug("pdu=#0, strength=#1", drug.getPivasDosageUnit(), dmDrugCacher.getDmDrug(drug.getItemCode()).getStrength());
			}
		}
		
		List<DmSite> whiteDmSiteList = new ArrayList<DmSite>();
		List<DmSite> greyDmSiteList = new ArrayList<DmSite>();
		
		for (Drug drug : retrieveDrugSiteMappingList(drugKey)) {
			logger.debug("itemCode=#0", drug.getItemCode());
			for (DrugSiteMapping drugSiteMapping : drug.getDrugSiteMappings()) {
				if (!"Y".equals(drugSiteMapping.getParenteralSite())) {
					logger.debug("#0, Not ParenteralSite", drugSiteMapping.getSiteCode());
					continue;
				}
				if (!includeNonPivasSite && !noSolventRequiredExist && !pivasSiteCode.contains(drugSiteMapping.getSiteCode())) {
					logger.debug("#0, Not pivas site", drugSiteMapping.getSiteCode());
					continue;
				}
				DmSite dmSite = dmSiteCacher.getSiteBySiteCode(drugSiteMapping.getSiteCode());
				if (drugSiteMapping.getGreyListLevel() == null) {
					logger.debug("#0, white", drugSiteMapping.getSiteCode());
					if (whiteDmSiteList.contains(dmSite)) {
						continue;
					}
					whiteDmSiteList.add(dmSite);
				}
				else {
					logger.debug("#0, grey", drugSiteMapping.getSiteCode());
					if (greyDmSiteList.contains(dmSite)) {
						continue;
					}
					greyDmSiteList.add(dmSite);
				}
			}
		}
		greyDmSiteList.removeAll(whiteDmSiteList);
		
		List<DmDiluent> dmDiluentList = new ArrayList<DmDiluent>();
		for (DmDiluent dmDiluent: dmsPmsServiceProxy.retrieveDmDiluentListByDrugKey()) {
			if ( ("N".equals(dmDiluent.getSuspend())) && ("Y".equals(dmDiluent.getDiluentFlag())) && (dmDiluent.getPivasDiluentDesc() != null) ) {
				dmDiluentList.add(dmDiluent);
			}
		}

		pivasFormulaInfo.setWhiteList(whiteDmSiteList);
		pivasFormulaInfo.setGreyList(greyDmSiteList);
		pivasFormulaInfo.setDmDiluentList(dmDiluentList);
		pivasFormulaInfo.setPivasDosageUnitList(new ArrayList<String>(pivasDosageUnit));
		pivasFormulaInfo.setStrengthList(new ArrayList<String>(strength));

		return pivasFormulaInfo;

	}
	
	private List<Drug> retrieveDrugSiteMappingList(Integer drugKey) {

		List<DmDrug> dmDrugList = dmDrugCacher.getDmDrugListByDrugKey(workstore, drugKey);
		logger.debug("dmDrugList.size()=#0", dmDrugList.size());
		if (dmDrugList.isEmpty()) {
			return new ArrayList<Drug>();
		}
		
		DmDrugProperty dmDrugProperty = dmDrugList.get(0).getDmDrugProperty();
		
		DrugRouteCriteria drugRouteCriteria = new DrugRouteCriteria();
		drugRouteCriteria.setDispHospCode(workstore.getHospCode());
		drugRouteCriteria.setDispWorkstore(workstore.getWorkstoreCode());
		drugRouteCriteria.setTrueDisplayname(dmDrugProperty.getDisplayname());
		drugRouteCriteria.setSaltProperty(dmDrugProperty.getSaltProperty());
		drugRouteCriteria.setFormCode(dmDrugProperty.getFormCode());

		return dmsPmsServiceProxy.getDrugRouteForIp(drugRouteCriteria);
	}
	
	private List<DrugSiteMapping> retrieveDrugSiteMappingList(String itemCode) {

		DrugRouteCriteria drugRouteCriteria = new DrugRouteCriteria();
		drugRouteCriteria.setDispHospCode(workstore.getHospCode());
		drugRouteCriteria.setDispWorkstore(workstore.getWorkstoreCode());
		drugRouteCriteria.setItemCode(itemCode);
		
		return drugSiteMappingCacher.getDrugSiteMappingListByDrugRouteCriteria(drugRouteCriteria);
	}

	public PivasDrugListInfo retrievePivasDrugListInfoByDrugKeyStrengthDiluentCode(Integer drugKey, String strength, String diluentCode, String siteCode) {

		List<PivasDrugInfo> pivasDrugInfoList = new ArrayList<PivasDrugInfo>();

		Map<String, PivasDrug> pivasDrugByItemCodeMap = new HashMap<String, PivasDrug>();
		for (PivasDrug pivasDrug : dmsPmsServiceProxy.retrievePivasDrugListByDrugKeyDiluentCode(workstore.getWorkstoreGroup().getWorkstoreGroupCode(), workstore.getHospCode(), drugKey, diluentCode)) {
			pivasDrugByItemCodeMap.put(pivasDrug.getItemCode(), pivasDrug);
		}

		for (WorkstoreDrug workstoreDrug : workstoreDrugCacher.getWorkstoreDrugListByDrugKey(workstore, drugKey)) {
			if (!pivasDrugByItemCodeMap.containsKey(workstoreDrug.getDmDrug().getItemCode()) ) {
				continue;
			}
			if (strength != null && !strength.equals(StringUtils.trimToEmpty(workstoreDrug.getDmDrug().getStrength()))) {
				continue;
			}
			logger.debug("siteCode=#0", siteCode);
			if (siteCode != null && !isPivasDrugSiteMatch(pivasDrugByItemCodeMap.get(workstoreDrug.getDmDrug().getItemCode()), siteCode)) {
				continue;
			}
			
			PivasDrugInfo pivasDrugInfo = new PivasDrugInfo();
			pivasDrugInfo.setMsWorkstoreDrug(workstoreDrug.getMsWorkstoreDrug());
			pivasDrugInfo.setPivasDrug(pivasDrugByItemCodeMap.get(workstoreDrug.getDmDrug().getItemCode()));
			pivasDrugInfo.getPivasDrug().setDmDrug(workstoreDrug.getDmDrug());
			pivasDrugInfoList.add(pivasDrugInfo);
		}

		Collections.sort(pivasDrugInfoList, pivasDrugInfoComparator);

		for (PivasDrugInfo pivasDrugInfo: pivasDrugInfoList) {
			Collections.sort(pivasDrugInfo.getPivasDrug().getPivasDrugManufList(), new Comparator<PivasDrugManuf>(){
				public int compare(PivasDrugManuf pivasDrugManuf1, PivasDrugManuf pivasDrugManuf2) {
					return pivasDrugManuf1.getManufacturerCode().compareTo(pivasDrugManuf2.getManufacturerCode());
				}
			});	
		}
		
		List<DmWarning> dmWarningList = new ArrayList<DmWarning>();
		for (DmWarning dmWarning : dmWarningCacher.getDmWarningList()) {
			if ( "ST".equals(dmWarning.getWarnCatCode()) ) {
				dmWarningList.add(dmWarning);
			}
		}
		
		Collections.sort(dmWarningList, new Comparator<DmWarning>(){
			public int compare(DmWarning warn1, DmWarning warn2) {
				return warn1.getWarnMessageEng().compareTo(warn2.getWarnMessageEng());
			}
		});	
		
		PivasDrugListInfo info = new PivasDrugListInfo();
		info.setPivasDrugInfoList(pivasDrugInfoList);
		info.setDmWarningList(dmWarningList);
		return info;
	}
	
	private boolean isPivasDrugSiteMatch(PivasDrug pivasDrug, String siteCode) {
		
		for (DrugSiteMapping drugSiteMapping : retrieveDrugSiteMappingList(pivasDrug.getItemCode())) {
			if (siteCode.equals(drugSiteMapping.getSiteCode())) {
				return true;
			}
		}
		return false;
	}

	public Boolean[] checkPivasItemSuspend(String itemCode) {
		MsWorkstoreDrug msWorkstoreDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, itemCode).getMsWorkstoreDrug();
		DmDrug dmDrug = dmDrugCacher.getDmDrug(itemCode);
		Boolean[] rtn = new Boolean[2];
		rtn[0] = msWorkstoreDrug != null && !msWorkstoreDrug.isLocalSuspend();
		rtn[1] = "N".equals(dmDrug.getHqSuspend());
		return rtn;
	}
	
	public void retrievePivasFormulaForExport() {

		pivasFormulaMaintRptList = new ArrayList<PivasFormulaMaintRpt>();
		
		Pair<List<PivasFormula>, List<PivasFormulaConfig>> result = dmsPmsServiceProxy.retrievePivasFormulaListForExport(workstore.getWorkstoreGroup().getWorkstoreGroupCode());
		
		Map<Long, List<PivasFormulaConfig>> configListbyFormulaIdMap = new HashMap<Long, List<PivasFormulaConfig>>();
		
		for (PivasFormulaConfig config : result.getSecond()) {
			Long formulaId = config.getPivasFormulaId();
			List<PivasFormulaConfig> configList = configListbyFormulaIdMap.get(formulaId);
			
			if (configList == null) {
				configList = new ArrayList<PivasFormulaConfig>();
			}
			configList.add(config);
			configListbyFormulaIdMap.put(formulaId, configList);
		}
		
		for (PivasFormula formula : result.getFirst()) {
			for (PivasFormulaConfig config : configListbyFormulaIdMap.get(formula.getId())) {
				for (PivasFormulaMethod formulaMethod : formula.getPivasFormulaMethodList()) {
					for (PivasFormulaMethodDrug formulaMethodDrug : formulaMethod.getPivasFormulaMethodDrugList()) {
						PivasFormulaDrug formulaDrug = formulaMethodDrug.getPivasFormulaDrug();
						PivasDrugManufMethod drugManufMethod = formulaMethodDrug.getPivasDrugManufMethod();
						PivasDrugManuf drugManuf = drugManufMethod.getPivasDrugManuf();
						PivasDrug pivasDrug = drugManuf.getPivasDrug();
						
						boolean isMergePivasFormulaType = (formula.getType() == PivasFormulaType.Merge ? true : false);
						
						PivasFormulaMaintRpt rpt = new PivasFormulaMaintRpt();
						
						rpt.setWorkstoreGroupCode(formula.getWorkstoreGroupCode());
						rpt.setPivasFormulaType(formula.getType().getDisplayValue());
						rpt.setPivasFormulaStatus(formula.getStatus().getDisplayValue());
						rpt.setPivasFormulaMethodStatus(formulaMethod.getStatus().getDisplayValue());
						rpt.setTargetProductItemCode(formula.getItemCode());
						rpt.setDrugKey(formulaDrug.getDrugKey());
						rpt.setPrintName(formula.getPrintName());
						rpt.setSiteCode(formula.getSiteCode());
						rpt.setDiluentDesc(formula.getDmDiluent() == null ? null : formula.getDmDiluent().getPivasDiluentDesc());
						rpt.setPivasDosageUnit(isMergePivasFormulaType ? null : formulaDrug.getPivasDosageUnit());
						rpt.setConcn(formula.getConcn());
						rpt.setRate(formula.getRate());
						rpt.setFormulaFinalVolume(formula.getFinalVolume());
						rpt.setStrength(isMergePivasFormulaType ? null : formulaDrug.getStrength());
						rpt.setBatchPrepFlag(formula.getBatchPrepFlag());
						rpt.setTargetType(formula.getTargetType().getDisplayValue());
						rpt.setContainerName(formula.getPivasContainer().getContainerName());
						rpt.setUpperDoseLimit(formula.getUpperDoseLimit());
						rpt.setDispHospCode(config.getDispHospCode());
						rpt.setShelfLife(formulaMethod.getShelfLife() == null ? config.getShelfLife() : formulaMethod.getShelfLife());
						rpt.setSatelliteShelfLife(config.getSatelliteShelfLife());
						rpt.setLabelOption(config.getLabelOption());
						rpt.setLabelOptionType(config.getLabelOptionType().getDisplayValue());
						rpt.setCoreFlag(formulaMethod.getCoreFlag());
						rpt.setVolume(formulaMethodDrug.getVolume());
						rpt.setDiluentVolume(formulaMethod.getDiluentVolume());
						rpt.setAfterConcn(formulaMethodDrug.getAfterConcn());
						rpt.setAfterVolume(formulaMethodDrug.getAfterVolume());
						rpt.setWarnCode1(formulaMethod.getWarnCode1());
						rpt.setWarnCode2(formulaMethod.getWarnCode2());
						rpt.setWarnCode3(formulaMethod.getWarnCode3());
						rpt.setWarnCode4(formulaMethod.getWarnCode4());
						rpt.setFormulaMethodDrugDosage(formulaMethodDrug.getDosage());
						rpt.setFormulaDrugPivasDosageUnit(formulaDrug.getPivasDosageUnit());
						rpt.setItemCode(pivasDrug.getItemCode());
						rpt.setManufacturerCode(drugManuf.getManufacturerCode());
						rpt.setRequireSolventFlag(drugManuf.getRequireSolventFlag());
						rpt.setSolventDesc(drugManufMethod.getDmDiluent() == null ? null : drugManufMethod.getDmDiluent().getPivasDiluentDesc());
						rpt.setDosageQty(drugManufMethod.getDosageQty());
						rpt.setDrugPivasDosageUnit(pivasDrug.getPivasDosageUnit());
						rpt.setSolventVolume(drugManufMethod.getVolume());
						rpt.setReconVolume(drugManufMethod.getAfterVolume());
						rpt.setReconConcn(drugManufMethod.getAfterConcn());
						rpt.setFormulaId(formula.getId());
						rpt.setFormulaUpdateUser(formula.getUpdateUser());
						rpt.setFormulaUpdateDate(formula.getUpdateDate());
						rpt.setFormulaMethodId(formulaMethod.getId());
						rpt.setFormulaMethodUpdateUser(formulaMethod.getUpdateUser());
						rpt.setFormulaMethodUpdateDate(formulaMethod.getUpdateDate());
						rpt.setFormulaConfigId(config.getId());
						rpt.setDrugStatus(pivasDrug.getStatus().getDisplayValue());
						rpt.setDrugManufStatus(drugManuf.getStatus().getDisplayValue());
						rpt.setDrugManufMethodId(drugManufMethod.getId());
						rpt.setDrugManufMethodStatus(drugManufMethod.getStatus().getDisplayValue());
						rpt.setFormulaDrugId(formulaDrug.getId());
						rpt.setDisplayname(formulaDrug.getDisplayname());
						rpt.setSaltProperty(formulaDrug.getSaltProperty());
						rpt.setFormCode(formulaDrug.getFormCode());
						rpt.setDiluentCode(formula.getDiluentCode());
						rpt.setExpiryDateTitle(formula.getExpiryDateTitle().getDisplayValue());
						
						pivasFormulaMaintRptList.add(rpt);
					}
				}
			}
		}
		Collections.sort(pivasFormulaMaintRptList, new PivasFormulaMaintRptComparator());
	}

	public void exportPivasFormula() {
		PrintOption po = new PrintOption();
		po.setDocType(ReportProvider.XLS);
		printAgent.renderAndRedirect(new RenderJob(
				"PivasFormulaXls", 
				po, 
				new HashMap<String, Object>(), 
				pivasFormulaMaintRptList));
	}

	public String getErrorCode() {
		return errorCode;
	}

	private static class PivasDrugInfoComparator implements Comparator<PivasDrugInfo>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(PivasDrugInfo pivasDrugInfo1, PivasDrugInfo pivasDrugInfo2) {
			int compareResult = 0;

			compareResult = Boolean.valueOf((!"A".equals(pivasDrugInfo1.getPivasDrug().getDmDrug().getHqSuspend()))).compareTo(Boolean.valueOf((!"A".equals(pivasDrugInfo2.getPivasDrug().getDmDrug().getHqSuspend()))));
		
				if ( compareResult == 0 ) {
					compareResult = Boolean.valueOf((!"A".equals(pivasDrugInfo1.getMsWorkstoreDrug().getItemStatus()))).compareTo(Boolean.valueOf((!"A".equals(pivasDrugInfo2.getMsWorkstoreDrug().getItemStatus()))));

					if ( compareResult == 0 ) {
						compareResult = pivasDrugInfo1.getPivasDrug().getDmDrug().getItemCode().compareTo(pivasDrugInfo2.getPivasDrug().getDmDrug().getItemCode());
					}
				}
				return compareResult;
		}
	}
	
	private static class PivasFormulaMethodComparator implements Comparator<PivasFormulaMethod> {
		@Override
		public int compare(PivasFormulaMethod pivasFormulaMethod1, PivasFormulaMethod pivasFormulaMethod2) {
			
			int compareResult = ObjectUtils.compare(pivasFormulaMethod1.getCoreFlag(), pivasFormulaMethod2.getCoreFlag()) * -1;
			if (compareResult != 0) {
				return compareResult;
			}
			
			compareResult = ObjectUtils.compare(resolveStatusValue(pivasFormulaMethod1.getStatus()), resolveStatusValue(pivasFormulaMethod2.getStatus()));
			if (compareResult != 0) {
				return compareResult;
			}
			
			compareResult = ObjectUtils.compare(pivasFormulaMethod1.getDiluentVolume(), pivasFormulaMethod2.getDiluentVolume());
			if (compareResult != 0) {
				return compareResult;
			}
			
			return ObjectUtils.compare(pivasFormulaMethod1.getShelfLife(), pivasFormulaMethod2.getShelfLife());
		}
		
		private int resolveStatusValue(PivasFormulaMethodStatus status) {
			switch (status) {
				case Active:
					return 0;
				case Pending:
					return 1;
				case Suspend:
					return 2;
				case Deleted:
					return 3;
				case SysDeleted:
					return 4;
			}
			return 5;
		}
	}
	
	@Remove
	public void destroy() {
	}
}
