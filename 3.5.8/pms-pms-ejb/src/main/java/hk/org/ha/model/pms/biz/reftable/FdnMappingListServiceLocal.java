package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.persistence.reftable.FdnMapping;

import java.util.Collection;

import javax.ejb.Local;

@Local
public interface FdnMappingListServiceLocal {

	void retrieveFdnMappingList(String itemCode);
	
	void updateFdnMappingList(Collection<FdnMapping> fdnMappingList, String itemCode);
	
	void destroy();
	
}
