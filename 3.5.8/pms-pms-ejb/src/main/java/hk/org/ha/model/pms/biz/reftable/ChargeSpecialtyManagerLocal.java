package hk.org.ha.model.pms.biz.reftable;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.reftable.ChargeSpecialtyType;

import java.util.Map;

import javax.ejb.Local;

@Local
public interface ChargeSpecialtyManagerLocal {

	Map<ChargeSpecialtyType, String> retrieveChargeSpecialtyCodeMap(
			Workstore workstore, String specCode);

}
