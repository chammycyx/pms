package hk.org.ha.model.pms.persistence.pivas;

import hk.org.ha.model.pms.dms.persistence.pivas.PivasContainer;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormula;
import hk.org.ha.model.pms.dms.udt.pivas.LabelOptionType;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "PIVAS_PREP")
public class PivasPrep extends VersionEntity {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pivasPrepSeq")
	@SequenceGenerator(name = "pivasPrepSeq", sequenceName = "SQ_PIVAS_PREP", initialValue = 100000000)
	private Long id;

	@Column(name = "PIVAS_FORMULA_ID", nullable = false)
	private Long pivasFormulaId;
	
	@Column(name = "DRUG_KEY", nullable = false)
	private Integer drugKey;
	
	@Column(name = "PIVAS_DOSAGE_UNIT", nullable = false, length = 15)
	private String pivasDosageUnit;
	
	@Column(name = "VOLUME", precision = 19, scale = 4)
	private BigDecimal volume;
	
	@Column(name = "PRINT_NAME", nullable = false, length = 59)
	private String printName;
	
	@Column(name = "PIVAS_CONTAINER_ID", nullable = false, length = 19)
	private Long pivasContainerId;

	@Column(name = "UPPER_DOSE_LIMIT", precision = 19, scale = 4)
	private BigDecimal upperDoseLimit;
	
	@Column(name = "LABEL_OPTION")
	private Integer labelOption;
	
	@Converter(name = "PivasFormula.labelOptionType", converterClass = LabelOptionType.Converter.class)
    @Convert("PivasFormula.labelOptionType")
	@Column(name="LABEL_OPTION_TYPE", length = 3)
	private LabelOptionType labelOptionType;
	
	@Column(name = "SPLIT_COUNT", nullable = false)
	private Integer splitCount;

	@Column(name = "DUE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dueDate;

	@Column(name = "CAL_END_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date calEndDate;
	
	@Column(name = "FREQ_INTERVAL", nullable = false, precision = 19, scale = 4)
	private BigDecimal freqInterval;

	@Column(name = "SINGLE_DISP_FLAG", nullable = false)
	private Boolean singleDispFlag;

	@Column(name = "LAST_PRINT_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastPrintDate;

	@Column(name = "CHECK_DOSE_LIMIT_FLAG", nullable = false)
	private Boolean checkDoseLimitFlag;

	@Column(name = "CHECK_DUE_DATE_FLAG", nullable = false)
	private Boolean checkDueDateFlag;
	
	@Converter(name = "PivasPrep.status", converterClass = RecordStatus.Converter.class)
    @Convert("PivasPrep.status")
	@Column(name="STATUS", nullable = false, length = 1)
	private RecordStatus status;
	
	@ManyToOne
	@JoinColumn(name = "WORKSTORE_GROUP_CODE", nullable = false)
	private WorkstoreGroup workstoreGroup;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MED_PROFILE_ID", nullable = false)
	private MedProfile medProfile;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MED_PROFILE_MO_ITEM_ID", nullable = false)
	private MedProfileMoItem medProfileMoItem;
	
	@Column(name = "MED_PROFILE_MO_ITEM_ID", nullable = false, insertable = false, updatable = false)
	private Long medProfileMoItemId;	
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PIVAS_WORKLIST_ID")
	private PivasWorklist pivasWorklist;
	
	@Transient
	private BigDecimal splitDosage;

	@Transient
	private BigDecimal splitVolume;

	@Transient
	private PivasContainer pivasContainer;
	
	@Transient
	private PivasFormula pivasFormula;
	
	public PivasPrep() {
		status = RecordStatus.Active;
		labelOptionType = LabelOptionType.None;
		splitCount = 1;
		singleDispFlag = Boolean.FALSE;
		checkDoseLimitFlag = Boolean.TRUE;
		checkDueDateFlag = Boolean.TRUE;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPivasFormulaId() {
		return pivasFormulaId;
	}

	public void setPivasFormulaId(Long pivasFormulaId) {
		this.pivasFormulaId = pivasFormulaId;
	}

	public Integer getDrugKey() {
		return drugKey;
	}

	public void setDrugKey(Integer drugKey) {
		this.drugKey = drugKey;
	}

	public String getPivasDosageUnit() {
		return pivasDosageUnit;
	}

	public void setPivasDosageUnit(String pivasDosageUnit) {
		this.pivasDosageUnit = pivasDosageUnit;
	}

	public BigDecimal getVolume() {
		return volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	public String getPrintName() {
		return printName;
	}

	public void setPrintName(String printName) {
		this.printName = printName;
	}

	public Long getPivasContainerId() {
		return pivasContainerId;
	}

	public void setPivasContainerId(Long pivasContainerId) {
		this.pivasContainerId = pivasContainerId;
	}

	public BigDecimal getUpperDoseLimit() {
		return upperDoseLimit;
	}

	public void setUpperDoseLimit(BigDecimal upperDoseLimit) {
		this.upperDoseLimit = upperDoseLimit;
	}

	public Integer getLabelOption() {
		return labelOption;
	}

	public void setLabelOption(Integer labelOption) {
		this.labelOption = labelOption;
	}

	public LabelOptionType getLabelOptionType() {
		return labelOptionType;
	}

	public void setLabelOptionType(LabelOptionType labelOptionType) {
		this.labelOptionType = labelOptionType;
	}

	public Integer getSplitCount() {
		return splitCount;
	}

	public void setSplitCount(Integer splitCount) {
		this.splitCount = splitCount;
	}

	public Date getDueDate() {
		return dueDate == null ? null : new Date(dueDate.getTime());
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate == null ? null : new Date(dueDate.getTime());
	}

	public Date getCalEndDate() {
		return calEndDate == null ? null : new Date(calEndDate.getTime());
	}

	public void setCalEndDate(Date calEndDate) {
		this.calEndDate = calEndDate == null ? null : new Date(calEndDate.getTime());
	}

	public BigDecimal getFreqInterval() {
		return freqInterval;
	}

	public void setFreqInterval(BigDecimal freqInterval) {
		this.freqInterval = freqInterval;
	}

	public Boolean getSingleDispFlag() {
		return singleDispFlag;
	}

	public void setSingleDispFlag(Boolean singleDispFlag) {
		this.singleDispFlag = singleDispFlag;
	}

	public Date getLastPrintDate() {
		return lastPrintDate == null ? null : new Date(lastPrintDate.getTime());
	}

	public void setLastPrintDate(Date lastPrintDate) {
		this.lastPrintDate = lastPrintDate == null ? null : new Date(lastPrintDate.getTime());
	}

	public Boolean getCheckDoseLimitFlag() {
		return checkDoseLimitFlag;
	}

	public void setCheckDoseLimitFlag(Boolean checkDoseLimitFlag) {
		this.checkDoseLimitFlag = checkDoseLimitFlag;
	}

	public Boolean getCheckDueDateFlag() {
		return checkDueDateFlag;
	}

	public void setCheckDueDateFlag(Boolean checkDueDateFlag) {
		this.checkDueDateFlag = checkDueDateFlag;
	}

	public RecordStatus getStatus() {
		return status;
	}

	public void setStatus(RecordStatus status) {
		this.status = status;
	}

	public WorkstoreGroup getWorkstoreGroup() {
		return workstoreGroup;
	}

	public void setWorkstoreGroup(WorkstoreGroup workstoreGroup) {
		this.workstoreGroup = workstoreGroup;
	}

	public MedProfile getMedProfile() {
		return medProfile;
	}

	public void setMedProfile(MedProfile medProfile) {
		this.medProfile = medProfile;
	}

	public MedProfileMoItem getMedProfileMoItem() {
		return medProfileMoItem;
	}

	public void setMedProfileMoItem(MedProfileMoItem medProfileMoItem) {
		this.medProfileMoItem = medProfileMoItem;
	}
	
	public Long getMedProfileMoItemId() {
		return medProfileMoItemId;
	}

	public void setMedProfileMoItemId(Long medProfileMoItemId) {
		this.medProfileMoItemId = medProfileMoItemId;
	}

	public PivasWorklist getPivasWorklist() {
		return pivasWorklist;
	}

	public void setPivasWorklist(PivasWorklist pivasWorklist) {
		this.pivasWorklist = pivasWorklist;
	}

	public BigDecimal getSplitDosage() {
		return splitDosage;
	}

	public void setSplitDosage(BigDecimal splitDosage) {
		this.splitDosage = splitDosage;
	}

	public BigDecimal getSplitVolume() {
		return splitVolume;
	}

	public void setSplitVolume(BigDecimal splitVolume) {
		this.splitVolume = splitVolume;
	}

	public PivasContainer getPivasContainer() {
		return pivasContainer;
	}

	public void setPivasContainer(PivasContainer pivasContainer) {
		this.pivasContainer = pivasContainer;
	}

	public PivasFormula getPivasFormula() {
		return pivasFormula;
	}

	public void setPivasFormula(PivasFormula pivasFormula) {
		this.pivasFormula = pivasFormula;
	}	
}
