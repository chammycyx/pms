package hk.org.ha.model.pms.biz.vetting.mp;
import javax.ejb.Local;

@Local
public interface InactiveListServiceLocal {
	void retrieveInactiveList();
	void destroy();
}
