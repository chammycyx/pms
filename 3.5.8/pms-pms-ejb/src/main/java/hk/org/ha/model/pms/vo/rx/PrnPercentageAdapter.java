package hk.org.ha.model.pms.vo.rx;

import java.io.Serializable;

import hk.org.ha.model.pms.udt.vetting.PrnPercentage;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class PrnPercentageAdapter extends XmlAdapter<String, PrnPercentage> implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public String marshal(PrnPercentage v) { 
		return (v == null) ? null : v.getDataValue();
	}

	public PrnPercentage unmarshal(String v) {
		return PrnPercentage.dataValueOf(v);
	}

}
