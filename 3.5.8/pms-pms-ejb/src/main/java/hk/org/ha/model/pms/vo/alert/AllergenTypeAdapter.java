package hk.org.ha.model.pms.vo.alert;

import java.io.Serializable;

import hk.org.ha.model.pms.udt.alert.AllergenType;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class AllergenTypeAdapter extends XmlAdapter<String, AllergenType> implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public String marshal(AllergenType v) { 
		return v.getDataValue();
	}

	public AllergenType unmarshal(String v) {
		return AllergenType.dataValueOf(v);
	}

}
