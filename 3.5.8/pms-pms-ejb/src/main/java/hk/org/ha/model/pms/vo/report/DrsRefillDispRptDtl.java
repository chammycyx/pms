package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

public class DrsRefillDispRptDtl implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Integer itemNum;
	
	private String fullDrugDesc;
	
	private Integer refillQty;

	private Integer actualQty;
	
	private String baseUnit;
	
	private String displayInstruction;
	
	private String remark;
	
	private String drugWarning;

	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public String getFullDrugDesc() {
		return fullDrugDesc;
	}

	public void setFullDrugDesc(String fullDrugDesc) {
		this.fullDrugDesc = fullDrugDesc;
	}

	public Integer getRefillQty() {
		return refillQty;
	}

	public void setRefillQty(Integer refillQty) {
		this.refillQty = refillQty;
	}

	public Integer getActualQty() {
		return actualQty;
	}

	public void setActualQty(Integer actualQty) {
		this.actualQty = actualQty;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public String getDisplayInstruction() {
		return displayInstruction;
	}

	public void setDisplayInstruction(String displayInstruction) {
		this.displayInstruction = displayInstruction;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getDrugWarning() {
		return drugWarning;
	}

	public void setDrugWarning(String drugWarning) {
		this.drugWarning = drugWarning;
	}

}
