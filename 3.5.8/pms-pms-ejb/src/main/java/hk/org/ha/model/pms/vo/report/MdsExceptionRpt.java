package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.List;

public class MdsExceptionRpt implements Serializable {

	private static final long serialVersionUID = 1L;

	private MdsExceptionRptHdr mdsExceptionRptHdr;
	
	private List<MdsExceptionRptDtl> mdsExceptionRptDtlList;
	
	private MdsExceptionRptFtr mdsExceptionRptFtr;

	public MdsExceptionRptHdr getMdsExceptionRptHdr() {
		return mdsExceptionRptHdr;
	}

	public void setMdsExceptionRptHdr(MdsExceptionRptHdr mdsExceptionRptHdr) {
		this.mdsExceptionRptHdr = mdsExceptionRptHdr;
	}

	public List<MdsExceptionRptDtl> getMdsExceptionRptDtlList() {
		return mdsExceptionRptDtlList;
	}

	public void setMdsExceptionRptDtlList(List<MdsExceptionRptDtl> mdsExceptionRptDtlList) {
		this.mdsExceptionRptDtlList = mdsExceptionRptDtlList;
	}

	public MdsExceptionRptFtr getMdsExceptionRptFtr() {
		return mdsExceptionRptFtr;
	}

	public void setMdsExceptionRptFtr(MdsExceptionRptFtr mdsExceptionRptFtr) {
		this.mdsExceptionRptFtr = mdsExceptionRptFtr;
	}
}
