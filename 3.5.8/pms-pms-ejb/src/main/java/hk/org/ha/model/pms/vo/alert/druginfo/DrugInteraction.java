package hk.org.ha.model.pms.vo.alert.druginfo;

import java.io.Serializable;

public class DrugInteraction implements Comparable<DrugInteraction>, Serializable {

	private static final long serialVersionUID = 1L;

	private String interactionDesc;
	
	private String severity;
	
	private String severityCode;
	
	private Integer monoId;
	
	private Integer monoType;
	
	private Boolean showMonoFlag;
	
	private String html;
	
	private Integer sortSeq;

	public String getInteractionDesc() {
		return interactionDesc;
	}

	public void setInteractionDesc(String interactionDesc) {
		this.interactionDesc = interactionDesc;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public String getSeverityCode() {
		return severityCode;
	}

	public void setSeverityCode(String severityCode) {
		this.severityCode = severityCode;
	}

	public Integer getMonoId() {
		return monoId;
	}

	public void setMonoId(Integer monoId) {
		this.monoId = monoId;
	}

	public Integer getMonoType() {
		return monoType;
	}

	public void setMonoType(Integer monoType) {
		this.monoType = monoType;
	}

	public Boolean getShowMonoFlag() {
		return showMonoFlag;
	}

	public void setShowMonoFlag(Boolean showMonoFlag) {
		this.showMonoFlag = showMonoFlag;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}

	public Integer getSortSeq() {
		return sortSeq;
	}

	public void setSortSeq(Integer sortSeq) {
		this.sortSeq = sortSeq;
	}

	public int compareTo(DrugInteraction o) {
		if (!severityCode.equals(o.getSeverityCode())) {
			return severityCode.compareTo(o.getSeverityCode());
		}
		return interactionDesc.compareTo(o.getInteractionDesc());
	}
}
