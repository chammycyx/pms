package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.asa.exception.pas.PasException;
import hk.org.ha.model.pms.dms.persistence.PmsRouteSortSpec;
import hk.org.ha.model.pms.udt.patient.PasSpecialtyType;

import java.util.List;

import javax.ejb.Local;

@Local
public interface RouteFormDisplaySeqListServiceLocal {

	void retrieveRouteFormSortSpecList(String specCode, PasSpecialtyType specType) throws PasException;
	
	void retrievePasSpecList(String prefixSpecCode, PasSpecialtyType specType) throws PasException;
	
	void retrieveRouteFormDisplaySeq(String specCode, String type);
	
	void retrieveRouteFormDefaultSeqList(String specCode, PasSpecialtyType specType);
	
	void updateRouteSortSpecList(List<PmsRouteSortSpec> newPmsRouteSortSpecList);
	
	void destroy();
	
}
