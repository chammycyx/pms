package hk.org.ha.model.pms.persistence.medprofile;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.medprofile.DeliveryItemStatus;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "REPLENISHMENT_ITEM")
public class ReplenishmentItem extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "replenishmentItemSeq")
	@SequenceGenerator(name = "replenishmentItemSeq", sequenceName = "SQ_REPLENISHMENT_ITEM", initialValue = 100000000)
	private Long id;

	@Column(name = "ISSUE_QTY", precision = 19, scale = 4)
	private BigDecimal issueQty;
	
	@Column(name = "DIRECT_PRINT_FLAG", nullable = false)
	private Boolean directPrintFlag;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DUE_DATE")
	private Date dueDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "PRINT_DATE")
	private Date printDate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "REPLENISHMENT_ID", nullable = false)
	private Replenishment replenishment;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MED_PROFILE_PO_ITEM_ID", nullable = false)
	private MedProfilePoItem medProfilePoItem;
	
	@Transient
	private DeliveryItemStatus deliveryItemStatus;
	
	@Transient
	private String deliveryWardCode;
	
	@Transient
	private String labelWardCode;
	
	@Transient
	private String labelBedNum;
	
	@Transient
	private Date deliveryDate;
	
	public ReplenishmentItem() {
		directPrintFlag = Boolean.FALSE;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getIssueQty() {
		return issueQty;
	}

	public void setIssueQty(BigDecimal issueQty) {
		this.issueQty = issueQty;
	}

	public Boolean getDirectPrintFlag() {
		return directPrintFlag;
	}

	public void setDirectPrintFlag(Boolean directPrintFlag) {
		this.directPrintFlag = directPrintFlag;
	}

	public Date getDueDate() {
		return dueDate == null ? null : new Date(dueDate.getTime());
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate == null ? null : new Date(dueDate.getTime());
	}

	public Date getPrintDate() {
		return printDate == null ? null : new Date(printDate.getTime());
	}

	public void setPrintDate(Date printDate) {
		this.printDate = printDate == null ? null : new Date(printDate.getTime());
	}
	
	public Replenishment getReplenishment() {
		return replenishment;
	}

	public void setReplenishment(Replenishment replenishment) {
		this.replenishment = replenishment;
	}

	public MedProfilePoItem getMedProfilePoItem() {
		return medProfilePoItem;
	}

	public void setMedProfilePoItem(MedProfilePoItem medProfilePoItem) {
		this.medProfilePoItem = medProfilePoItem;
	}

	public void setDeliveryItemStatus(DeliveryItemStatus deliveryItemStatus) {
		this.deliveryItemStatus = deliveryItemStatus;
	}

	public DeliveryItemStatus getDeliveryItemStatus() {
		return deliveryItemStatus;
	}

	public void setDeliveryWardCode(String deliveryWardCode) {
		this.deliveryWardCode = deliveryWardCode;
	}

	public String getDeliveryWardCode() {
		return deliveryWardCode;
	}

	public void setLabelWardCode(String labelWardCode) {
		this.labelWardCode = labelWardCode;
	}

	public String getLabelWardCode() {
		return labelWardCode;
	}

	public void setLabelBedNum(String labelBedNum) {
		this.labelBedNum = labelBedNum;
	}

	public String getLabelBedNum() {
		return labelBedNum;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	@Transient
	public boolean isNoLabelItem() {
		return issueQty == null || BigDecimal.ZERO.compareTo(issueQty) == 0;
	}
}
