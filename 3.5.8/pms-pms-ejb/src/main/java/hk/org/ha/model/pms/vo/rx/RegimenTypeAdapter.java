package hk.org.ha.model.pms.vo.rx;

import java.io.Serializable;

import hk.org.ha.model.pms.udt.vetting.RegimenType;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class RegimenTypeAdapter extends XmlAdapter<String, RegimenType> implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public String marshal(RegimenType v) {
		return (v == null) ? null : v.getDataValue();
	}

	public RegimenType unmarshal(String v) {
		return RegimenType.dataValueOf(v);
	}

}
