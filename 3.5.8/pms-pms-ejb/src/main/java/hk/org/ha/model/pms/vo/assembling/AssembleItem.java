package hk.org.ha.model.pms.vo.assembling;

import java.io.Serializable;
import java.util.Date;

public class AssembleItem implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Date ticketDate;
	
	private String ticketNum;
	
	private Integer itemNum;
	
	private String message;
	
	private String messageCode;
	
	private Boolean urgentFlag;
	
	private boolean prescWithRefill;
	
	private boolean completeAssembling; 
	
	private Long dispOrderId;
	
	private boolean reprintFlag;
	
	private boolean fastQueueFlag;

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public Integer getItemNum() {
		return itemNum;
	}

	public void setPrescWithRefill(boolean prescWithRefill) {
		this.prescWithRefill = prescWithRefill;
	}

	public boolean isPrescWithRefill() {
		return prescWithRefill;
	}

	public void setCompleteAssembling(boolean completeAssembling) {
		this.completeAssembling = completeAssembling;
	}

	public boolean isCompleteAssembling() {
		return completeAssembling;
	}

	public void setUrgentFlag(Boolean urgentFlag) {
		this.urgentFlag = urgentFlag;
	}

	public Boolean getUrgentFlag() {
		return urgentFlag;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public void setDispOrderId(Long dispOrderId) {
		this.dispOrderId = dispOrderId;
	}

	public Long getDispOrderId() {
		return dispOrderId;
	}

	public void setReprintFlag(boolean reprintFlag) {
		this.reprintFlag = reprintFlag;
	}

	public boolean isReprintFlag() {
		return reprintFlag;
	}

	public void setTicketDate(Date ticketDate) {
		this.ticketDate = ticketDate;
	}

	public Date getTicketDate() {
		return ticketDate;
	}

	public void setFastQueueFlag(boolean fastQueueFlag) {
		this.fastQueueFlag = fastQueueFlag;
	}

	public boolean getFastQueueFlag() {
		return fastQueueFlag;
	}

}