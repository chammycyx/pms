package hk.org.ha.model.pms.biz.order;

import hk.org.ha.fmk.pms.util.ConfigHelper;
import hk.org.ha.model.pms.persistence.corp.Workstore;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

@AutoCreate
@Scope(ScopeType.APPLICATION)
@Name("autoDispConfig")
public class AutoDispConfig {
	
	private static final String AUTO_DISP_CONFIG = "auto-disp-config.properties";

	@Logger
	private Log logger;
	
	private Map<Workstore,AutoDispWorkstore> workstoreMap = new HashMap<Workstore,AutoDispWorkstore>();	
	
	private boolean enable = false;
	
	@Create
	public void init() {

		try {
			Properties props = new Properties();		
			props.load(ConfigHelper.getResourceAsStream(AUTO_DISP_CONFIG));

			if (Boolean.valueOf(System.getProperty("pms.autoDisp","false")).booleanValue()) {
				enable = true;
			} else {
				enable = Boolean.valueOf(props.getProperty("enable","false")).booleanValue();
			}
			
			this.build(props);
			
		} catch (IOException e) {
			logger.info("#0 not found!", AUTO_DISP_CONFIG);
		}
		
		logger.info("AutoDispConfig enable=#0", enable);
	}
	
	private void build(Properties props) {
		String[] workstores = props.getProperty("workstore").split(",");
		
		for (String workstore : workstores) {
			AutoDispWorkstore ws = new AutoDispWorkstore(workstore);
			ws.build(props);
			workstoreMap.put(ws.getWorkstore(), ws);
		}
	}
	
	public AutoDispWorkstore getAutoDispWorkstore(Workstore workstore) {
		return workstoreMap.get(workstore);
	}
	
	public AutoDispWorkstation getAutoDispWorkstation(Workstore workstore, String workstationKey) {
		AutoDispWorkstore ws = workstoreMap.get(workstore);
		return (ws != null) ? ws.getAutoDispWorkstation(workstationKey) : null;
	}
	
	public List<AutoDispWorkstore> getAutoDispWorkstoreList() {
		return new ArrayList<AutoDispWorkstore>(workstoreMap.values());
	}
	
	
	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}	
	
	public static class AutoDispWorkstore {
		
		private Map<String,AutoDispWorkstation> workstationMap = new LinkedHashMap<String,AutoDispWorkstation>();	
		
		private String key;
		
		private Workstore workstore;
		
		public AutoDispWorkstore(String key) {
			super();
			this.key = key;
		}
		
		private void build(Properties props) {
			this.setWorkstore(new Workstore(
					props.getProperty(key + ".hosp-code"),
					props.getProperty(key + ".workstore-code")));
			
			String[] workstations = props.getProperty(key + ".workstation").split(",");
			for (String workstation : workstations) {
				AutoDispWorkstation ws = new AutoDispWorkstation(this, workstation);
				ws.build(props);
				workstationMap.put(workstation, ws);
			}
		}
				
		public List<AutoDispWorkstation> getAutoDispWorkstationList() {
			return new ArrayList<AutoDispWorkstation>(workstationMap.values());
		}
		
		public AutoDispWorkstation getAutoDispWorkstation(String workstationeKey) {
			return workstationMap.get(workstationeKey);
		}
				
		public String getKey() {
			return key;
		}

		public void setWorkstore(Workstore workstore) {
			this.workstore = workstore;
		}

		public Workstore getWorkstore() {
			return workstore;
		}
	}
	
	public static class AutoDispWorkstation {
		
		private AutoDispWorkstore parent;

		private String key;
		
		private String workstationCode;
		
		private String userId;
		
		private long startupDelayTime = 0;
		private long preOrderSleepTime = 0;

		public AutoDispWorkstation(AutoDispWorkstore parent, String key) {
			super();
			this.parent = parent;
			this.key = key;
		}

		private void build(Properties props) {
			this.workstationCode = props.getProperty(this.key + ".workstation-code");
			this.userId = props.getProperty(this.key + ".user-id");
			this.startupDelayTime = Long.valueOf((String) props.getProperty(this.key + ".start-up-delay-time")).longValue();
			this.preOrderSleepTime = Long.valueOf((String) props.getProperty(this.key + ".per-order-sleep-time")).longValue();
		}
		
		public AutoDispWorkstore getParent() {
			return parent;
		}

		public String getKey() {
			return key;
		}
				
		public String getWorkstationCode() {
			return workstationCode;
		}

		public void setWorkstationCode(String workstationCode) {
			this.workstationCode = workstationCode;
		}

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

		public long getStartupDelayTime() {
			return startupDelayTime;
		}

		public void setStartupDelayTime(long startupDelayTime) {
			this.startupDelayTime = startupDelayTime;
		}

		public long getPreOrderSleepTime() {
			return preOrderSleepTime;
		}

		public void setPreOrderSleepTime(long preOrderSleepTime) {
			this.preOrderSleepTime = preOrderSleepTime;
		}

		@Override
		public String toString() {
			return new StringBuffer()
			.append("[")
			.append(parent.getKey())
			.append("/")
			.append(this.getKey())
			.append(",")
			.append(parent.getWorkstore().getHospCode())
			.append("/")
			.append(parent.getWorkstore().getWorkstoreCode())
			.append("/")
			.append(this.getWorkstationCode())
			.append("]")
			.toString();
		}
	}
}
