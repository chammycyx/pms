package hk.org.ha.model.pms.persistence.reftable;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "REFILL_HOLIDAY")
@Customizer(AuditCustomizer.class)
public class RefillHoliday extends VersionEntity {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "refillHolidaySeq")
	@SequenceGenerator(name = "refillHolidaySeq", sequenceName = "SQ_REFILL_HOLIDAY", initialValue = 100000000)
	private Long id;
	
	@Column(name = "HOLIDAY", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date holiday;

	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE", referencedColumnName = "HOSP_CODE", nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getHoliday() {
		if (holiday == null) {
			return null;
		}
		else {
			return new Date(holiday.getTime());
		}
	}

	public void setHoliday(Date holiday) {
		if (holiday == null) {
			this.holiday = null;
		}
		else {
			this.holiday = new Date(holiday.getTime());
		}
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}	
	
	// helper methods
    public boolean isNew() {
    	return this.getId() == null;
    }	
}
