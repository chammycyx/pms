package hk.org.ha.model.pms.biz.enquiry;

import hk.org.ha.model.pms.vo.charging.DrugPriceInfo;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface SfiDrugPriceEnqServiceLocal {

	void retrieveSfiDrugPrice(String itemCode, Date retrieveMonth);
	void retrieveSfiDrugPriceHistory(List<DrugPriceInfo> itemCodeList, Date retrieveMonth);
	void destroy();	
}
