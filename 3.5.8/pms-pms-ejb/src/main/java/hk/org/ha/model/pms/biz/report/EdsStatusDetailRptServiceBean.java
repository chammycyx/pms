package hk.org.ha.model.pms.biz.report;

import static hk.org.ha.model.pms.prop.Prop.*;
import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.SuspendReason;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderDayEndStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderAdminStatus;
import hk.org.ha.model.pms.udt.onestop.AllowCommentType;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.EdsStatusDetailRpt;
import hk.org.ha.model.pms.vo.report.SuspendPrescRpt;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.joda.time.DateTime;
@Stateful
@Scope(ScopeType.SESSION)
@Name("edsStatusDetailRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class EdsStatusDetailRptServiceBean implements EdsStatusDetailRptServiceLocal {
	
	private static final String EDS_STATUS_DETAIL_RPT_CLASS_NAME = EdsStatusDetailRpt.class.getName();

	@PersistenceContext
	private EntityManager em;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent;
	
	@In
	private Workstore workstore;
	
	@Out(required = false)
	private List<EdsStatusDetailRpt> edsStatusDetailRptPrescStatusDtlList;
	
	@Out(required = false)
	private List<EdsStatusDetailRpt> edsStatusDetailRptSuspendItemList;
	
	private List<EdsStatusDetailRpt> exportEdsStatusDetailRptSuspendItemList;
	
	public void retrieveEdsStatusDetailRptList(Integer dayRange){
		edsStatusDetailRptPrescStatusDtlList = this.retrievePrescStatusDtlList();
		edsStatusDetailRptSuspendItemList = this.retrieveSuspendItemList(dayRange);
	}
		
	@SuppressWarnings("unchecked")
	private List<EdsStatusDetailRpt> retrievePrescStatusDtlList(){		
		
		List<EdsStatusDetailRpt> edsStatusDetailRptList = em.createQuery(
				"select new " + EDS_STATUS_DETAIL_RPT_CLASS_NAME + // 20150408 index check : DispOrder.workstore,orderType,ticketDate : I_DISP_ORDER_15
				" (o.dispOrder.ticketDate, o.dispOrder.ticketNum, " +
				" o.pharmOrderItem.itemNum, o.pharmOrderItem.itemCode, " +
				" o.status, o.dispOrder.suspendCode, " +
				" o.dispOrder.pharmOrder.updateUser, o.dispOrder.ticketCreateDate, " +
				" o.dispOrder.pharmOrder.adminStatus, o.dispOrder.adminStatus," +
				" o.dispOrder.pharmOrder.allowCommentType, " +
				" o.dispOrder.createUser, o.pickUser, o.assembleUser, o.dispOrder.checkUser, o.dispOrder.issueUser, " +
				" o.dispOrder.pharmOrder.medCase, o.dispOrder.pharmOrder.name, o.dispOrder.pharmOrder.hkid, " +
				" o.dispOrder.pharmOrder.medCase.caseNum, o.dispQty, o.pharmOrderItem.dangerDrugFlag, o.dispOrder.revokeFlag) " +
				" from DispOrderItem o" +
				" where o.dispOrder.workstore = :workstore" +
				" and o.dispOrder.orderType = :orderType" +
				" and o.dispOrder.ticketDate between :startDate and :endDate" +
				" and o.dispOrder.status not in :dispOrderStatus" +
				" and o.dispOrder.adminStatus = :adminStatus" +
				" and o.dispOrder.dayEndStatus = :dayEndStatus" +
				" and o.dispOrder.batchProcessingFlag = :batchProcessingFlag" +
				" and o.status not in :dispOrderItemStatusList" +
				" order by o.dispOrder.ticketDate desc, " +
				" o.dispOrder.ticketNum, " +
				" o.pharmOrderItem.itemNum ")
				.setParameter("workstore", workstore)
				.setParameter("orderType", OrderType.OutPatient)
				.setParameter("startDate", new DateTime().minusDays(REPORT_EDSSTATUSDTL_ORDER_DURATION_LIMIT.get()).toDate(), TemporalType.DATE)
				.setParameter("endDate", new Date(), TemporalType.DATE)
				.setParameter("dispOrderStatus", DispOrderStatus.Deleted_SysDeleted_Issued)
				.setParameter("adminStatus", DispOrderAdminStatus.Normal)
				.setParameter("dayEndStatus", DispOrderDayEndStatus.None)
				.setParameter("batchProcessingFlag", Boolean.FALSE)
				.setParameter("dispOrderItemStatusList", DispOrderItemStatus.KeepRecord_Deleted)
				.getResultList();
		
		List<EdsStatusDetailRpt> removeList = new ArrayList<EdsStatusDetailRpt>();

		if ( !edsStatusDetailRptList.isEmpty() ) {
			for ( EdsStatusDetailRpt edsStatusDetailRpt : edsStatusDetailRptList ) {
				this.setEdsStatusDetailRpt(edsStatusDetailRpt);
				if (edsStatusDetailRpt.getElapseTime().compareTo(WORKSTORE_OUTSTANDINGORDER_ELAPSETIME.get()) < 0) {
					removeList.add(edsStatusDetailRpt);
				}
			}
		}
		
		edsStatusDetailRptList.removeAll(removeList);
		return edsStatusDetailRptList;
	}
	
	@SuppressWarnings("unchecked")
	private List<EdsStatusDetailRpt> retrieveSuspendItemList(Integer dayRange){					
		StringBuilder sb = new StringBuilder( 
				"select new " + EDS_STATUS_DETAIL_RPT_CLASS_NAME + // 20150408 index check : DispOrder.workstore,orderType,ticketDate : I_DISP_ORDER_15
				" (o.dispOrder.ticketDate, o.dispOrder.ticketNum, " +
				" o.pharmOrderItem.itemNum, o.pharmOrderItem.itemCode, " +
				" o.status, o.dispOrder.suspendCode, " +
				" o.dispOrder.pharmOrder.updateUser, o.dispOrder.ticketCreateDate, " +
				" o.dispOrder.pharmOrder.adminStatus, o.dispOrder.adminStatus," +
				" o.dispOrder.pharmOrder.allowCommentType, " +
				" o.dispOrder.createUser, o.pickUser, o.assembleUser, o.dispOrder.checkUser, o.dispOrder.issueUser, " +
				" o.dispOrder.pharmOrder.medCase, o.dispOrder.pharmOrder.name, o.dispOrder.pharmOrder.hkid, " +
				" o.dispOrder.pharmOrder.medCase.caseNum, o.dispQty, o.pharmOrderItem.dangerDrugFlag, o.dispOrder.revokeFlag ) " +
				" from DispOrderItem o" +
				" where o.dispOrder.workstore = :workstore" +
				" and o.dispOrder.orderType = :orderType" );
		
		if ( dayRange != null && dayRange >= 0 ) {
			sb.append(" and o.dispOrder.ticketDate < :nextday and o.dispOrder.ticketDate > :beforeDate " );
		} else {
			sb.append(" and o.dispOrder.ticketDate between :startDate and :endDate");
		}
		
		sb.append(
					" and o.dispOrder.status not in :dispOrderStatus" +
					" and o.dispOrder.adminStatus <> :adminStatus" +
					" and o.status not in :dispOrderItemStatusList");				
		
		sb.append(" order by o.dispOrder.ticketDate desc, " +
					" o.dispOrder.ticketNum, " +
					" o.pharmOrderItem.itemNum ");
		
		Query query = em.createQuery(sb.toString());
		
		query.setParameter("workstore", workstore);
		query.setParameter("orderType", OrderType.OutPatient);
		
		if ( dayRange != null && dayRange >=0 ) {
			DateTime today = new DateTime();
			query.setParameter("nextday", today.plusDays(1).toDate() ,TemporalType.DATE);
			query.setParameter("beforeDate", today.minusDays(dayRange+1).toDate() ,TemporalType.DATE);
		} else {
			query.setParameter("startDate", new DateTime().minusDays(REPORT_EDSSTATUSDTL_ORDER_DURATION_LIMIT.get()).toDate(), TemporalType.DATE);
			query.setParameter("endDate", new Date(), TemporalType.DATE);
		}
		
		query.setParameter("dispOrderStatus", DispOrderStatus.Deleted_SysDeleted);
		query.setParameter("adminStatus", DispOrderAdminStatus.Normal);
		query.setParameter("dispOrderItemStatusList", DispOrderItemStatus.KeepRecord_Deleted);
		
		List<EdsStatusDetailRpt> edsStatusDetailRptList = query.getResultList();
		
		if ( !edsStatusDetailRptList.isEmpty() ) {
			for ( EdsStatusDetailRpt edsStatusDetailRpt : edsStatusDetailRptList ) {
				this.setEdsStatusDetailRpt(edsStatusDetailRpt);
			}
		}
		
		return edsStatusDetailRptList;
	}
	
	private void setEdsStatusDetailRpt(EdsStatusDetailRpt edsStatusDetailRpt){
		
		if ( edsStatusDetailRpt.getPharmOrderAdminStatus().equals(PharmOrderAdminStatus.AllowModify) || 
				!edsStatusDetailRpt.getAllowCommentType().equals(AllowCommentType.None)) {
			edsStatusDetailRpt.setAllowModifyFlag(true);
		} else {
			edsStatusDetailRpt.setAllowModifyFlag(false);
		}
		
		if ( edsStatusDetailRpt.getRevokeFlag() == Boolean.TRUE || edsStatusDetailRpt.getPharmOrderAdminStatus().equals(PharmOrderAdminStatus.Revoke) ) {
			edsStatusDetailRpt.setUncollectFlag(true);
		} else {
			edsStatusDetailRpt.setUncollectFlag(false);
		}
		
		if ( StringUtils.isNotBlank(edsStatusDetailRpt.getSuspendCode()) ) {
			@SuppressWarnings("unchecked")
			List<SuspendReason> suspendReasonList = em.createQuery(
					"select o from SuspendReason o" + // 20120305 index check : SuspendReason.hospital,suspendCode : UI_SUSPEND_REASON_01
					" where o.hospital = :hospital" +
					" and o.suspendCode = :suspendCode")
					.setParameter("hospital", workstore.getHospital())
					.setParameter("suspendCode", edsStatusDetailRpt.getSuspendCode())
					.getResultList();
			
			SuspendReason suspendReason = null;
			if (!suspendReasonList.isEmpty()) {
				suspendReason = suspendReasonList.get(0);
			}
			if ( suspendReason != null ) {
				edsStatusDetailRpt.setSuspendDesc(suspendReason.getDescription());
			}
		}
		
		if ( edsStatusDetailRpt.getMedCase() != null ) {
			edsStatusDetailRpt.setCaseNum(edsStatusDetailRpt.getMedCase().getCaseNum());
		}
		
		edsStatusDetailRpt.setStatusDetailUpdateUser(this.getUpdateUser(edsStatusDetailRpt.getStatus(), edsStatusDetailRpt));
		
		edsStatusDetailRpt.setElapseTime((int)this.getTimeDiff(edsStatusDetailRpt.getTicketIssueDate()));
	}
	
	private long getTimeDiff(Date ticketIssueDate){
		Calendar currentCal = Calendar.getInstance();
		Calendar ticketIssueCal = Calendar.getInstance();
		
		currentCal.setTime(new Date());
		ticketIssueCal.setTime(ticketIssueDate);
		
		return ( (currentCal.getTimeInMillis() - ticketIssueCal.getTimeInMillis()) / (60*1000) );
	}
	
	private String getUpdateUser(DispOrderItemStatus status, EdsStatusDetailRpt edsStatusDetailRpt){
		String updateUser = "";
		switch (status)
		{
			case Vetted:
				updateUser = edsStatusDetailRpt.getVetUser();
				break;
			case Picked:
				updateUser = edsStatusDetailRpt.getPickUser();
				break;
			case Assembled:
				updateUser = edsStatusDetailRpt.getAssembleUser();
				break;
			case Checked:
				updateUser = edsStatusDetailRpt.getCheckUser();
				break;
			case Issued:
				updateUser = edsStatusDetailRpt.getIssueUser();
				break;
		}
		
		return updateUser;
	}
	
	public List<SuspendPrescRpt> retrieveSuspendPrescRptList(List<EdsStatusDetailRpt> printEdsStatusDetailRptList){
		List<SuspendPrescRpt> suspendPrescRptList = new ArrayList<SuspendPrescRpt>();
		
		for ( EdsStatusDetailRpt edsStatusDetailRpt : printEdsStatusDetailRptList ) {
			SuspendPrescRpt suspendPrescRpt = new SuspendPrescRpt();
			
			suspendPrescRpt.setDispDate(edsStatusDetailRpt.getDispDate());
			suspendPrescRpt.setTicketNum(edsStatusDetailRpt.getTicketNum());
			suspendPrescRpt.setCaseNum(edsStatusDetailRpt.getCaseNum());
			suspendPrescRpt.setItemCode(edsStatusDetailRpt.getItemCode());
			suspendPrescRpt.setDispQty(edsStatusDetailRpt.getDispQty());
			suspendPrescRpt.setDdFlag(edsStatusDetailRpt.isDdFlag());
			suspendPrescRpt.setAllowModifyFlag(edsStatusDetailRpt.isAllowModifyFlag());
			suspendPrescRpt.setUncollectFlag(edsStatusDetailRpt.isUncollectFlag());
			suspendPrescRpt.setSuspendUpdateUser(edsStatusDetailRpt.getSuspendUpdateUser());
			
			suspendPrescRptList.add(suspendPrescRpt);
		}
		
		return suspendPrescRptList;
	}
	
	public void printSuspendPrescRpt(List<EdsStatusDetailRpt> printEdsStatusDetailRptList)
	{
		List<SuspendPrescRpt> suspendPrescRptList = retrieveSuspendPrescRptList(printEdsStatusDetailRptList);
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("hospCode", workstore.getHospCode());
		parameters.put("workstoreCode", workstore.getWorkstoreCode());

		    printAgent.renderAndPrint(new RenderAndPrintJob(
		    		"SuspendPrescRpt",
				    new PrintOption(),
				    printerSelector.retrievePrinterSelect(PrintDocType.Report),
				    parameters,
				    suspendPrescRptList));
	}
	
	public void exportFilteredEdsStatusDetailRpt(List<EdsStatusDetailRpt> printEdsStatusDetailRptList) {
		exportEdsStatusDetailRptSuspendItemList = printEdsStatusDetailRptList;
	}
	
	public void exportEdsStatusDetailRpt() {	
		Map<String, Object> parameters = new HashMap<String, Object>();
		PrintOption po = new PrintOption();
		po.setDocType(ReportProvider.XLS);
		printAgent.renderAndRedirect(new RenderJob(
				"SuspendPrescXls", 
				po, 
				parameters, 
				exportEdsStatusDetailRptSuspendItemList));

	}
	
	@Remove
	public void destroy() {		
		if ( edsStatusDetailRptPrescStatusDtlList != null ) {
			edsStatusDetailRptPrescStatusDtlList = null;
		}
		
		if ( edsStatusDetailRptSuspendItemList != null ) {
			edsStatusDetailRptSuspendItemList = null;
		}
	}
}
