package hk.org.ha.model.pms.biz.delivery;

import hk.org.ha.model.pms.persistence.pivas.MaterialRequestItem;

import java.util.List;

import javax.ejb.Local;

@Local
public interface MaterialRequestItemListServiceLocal {
	List<MaterialRequestItem> retrieveMaterialRequestItemList();
	void destroy();	
}
