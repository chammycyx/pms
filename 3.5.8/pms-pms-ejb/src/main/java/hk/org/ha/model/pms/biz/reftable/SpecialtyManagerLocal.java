package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.persistence.corp.Institution;
import hk.org.ha.model.pms.persistence.phs.Specialty;

import java.util.List;

import javax.ejb.Local;

@Local
public interface SpecialtyManagerLocal {
	
	List<String> retrieveSpecialtyCodeList();
	
	List<Specialty> retrieveSpecialtyList();
	
	List<Specialty> retrieveSpecialtyListWithoutDeleteStatus();
	
	List<Specialty> retrieveSpecialtyListWithActiveStatus();
	
	List<Specialty> retrieveSpecialtyListByInstCode(List<String> instCodeList);
}
