package hk.org.ha.model.pms.biz.report;

import java.io.IOException;
import java.util.Date;

import javax.ejb.Local;

import net.lingala.zip4j.exception.ZipException;

@Local
public interface ManualPrescRptServiceLocal {
	
	void retrieveManualPrescRptList(Date dateFrom, String password, Boolean exportCurrentMonth);
	
	void retrieveManualPrescSummaryList(Date dateFrom, String password, Boolean exportCurrentMonth);

	void exportManualPrescSummaryRpt() throws IOException, ZipException;
	
	void exportManualPrescRpt() throws IOException, ZipException;
	
	boolean isRetrieveSuccess();
	
	void destroy();
	
}
