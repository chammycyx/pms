package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.vo.DsfSearchCriteria;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.vetting.FmStatus;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("dmDrugListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DmDrugListServiceBean implements DmDrugListServiceLocal {

	@Logger
	private Log logger;

	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private Workstore workstore;
	
	@Out(required = false)
	private List<DmDrug> dmDrugList;
	
	public void retrieveDmDrugList(String prefixItemCode, boolean showNonSuspendItemOnly)
	{
		if (showNonSuspendItemOnly) {
			dmDrugList = dmDrugCacher.getDmDrugList(workstore, prefixItemCode, Boolean.FALSE, null);
		} else {
			dmDrugList = dmDrugCacher.getDmDrugList(workstore, prefixItemCode);
		}
		
		if ( dmDrugList == null ) {
			logger.debug("dmDrugList is null");
			return;
		}
		
		if ( logger.isDebugEnabled() ) {
			logger.debug("dmDrugList size:|#0", dmDrugList.size());
		}
	}
	
	public void retrieveDmDrugListByFullDrugDesc(String prefixFullDrugDesc)
	{	
		dmDrugList = dmDrugCacher.getDmDrugListByFullDrugDesc(workstore, prefixFullDrugDesc);		
		if ( dmDrugList == null ) {
			logger.debug("dmDrugList is null");
			return;
		}		

		if ( logger.isDebugEnabled() ) {
			logger.debug("dmDrugList size #0", dmDrugList.size());
		}
	}
		
	public void retrieveDmDrugListByDsfSearchCriteriaFmStatus(DsfSearchCriteria criteria, String status) {
		if ( logger.isDebugEnabled() ) {
			logger.debug("retrieveDmDrugListByDsfSearchCriteriaFmStatus - dispHospCode | #0", criteria.getDispHospCode());
			logger.debug("retrieveDmDrugListByDsfSearchCriteriaFmStatus - dispWorkstore | #0", criteria.getDispWorkstore());
			logger.debug("retrieveDmDrugListByDsfSearchCriteriaFmStatus - displayName | #0", criteria.getDisplayname());
			logger.debug("retrieveDmDrugListByDsfSearchCriteriaFmStatus - formCode | #0", criteria.getFormCode());
			logger.debug("retrieveDmDrugListByDsfSearchCriteriaFmStatus - saltProperty | #0", criteria.getSaltProperty());
			logger.debug("retrieveDmDrugListByDsfSearchCriteriaFmStatus - fmStatus | #0", status);
		}
		dmDrugList = getDmDrugListByDsfSearchCriteriaFmStatus(criteria, status);		
		
		if ( dmDrugList != null && logger.isDebugEnabled() ) {
			logger.debug("dmDrugList size #0", dmDrugList.size());			
		}
	}
	
	private List<DmDrug> getDmDrugListByDsfSearchCriteriaFmStatus(DsfSearchCriteria criteria, String status) {		
		return dmDrugCacher.getDmDrugListByDisplayNameFormSaltFmStatus(
				workstore,
				criteria.getDisplayname(), 
				criteria.getFormCode(), 
				criteria.getSaltProperty(), 
				status);
	}
	
	public void retrieveDmDrugListByDisplayNameFmStatus(String displayName, FmStatus status) {		
		if ( logger.isDebugEnabled() ) {
			logger.debug("retrieveDmDrugListByDisplayNameFmStatus - displayName | #0", displayName);		
			logger.debug("retrieveDmDrugListByDisplayNameFmStatus - fmStatus | #0", status.getDisplayValue());
		}
		dmDrugList = getDmDrugListByDisplayNameFmStatus(workstore, displayName, status);		
		if ( dmDrugList != null && logger.isDebugEnabled() ) {
			logger.debug("dmDrugList size #0", dmDrugList.size());
		}
	}	
	
	private List<DmDrug> getDmDrugListByDisplayNameFmStatus(Workstore workstore, String displayName, FmStatus status) {			
		return dmDrugCacher.getDmDrugListByDisplayNameFmStatus(workstore, displayName, status.getDataValue());
	}
	
	public void retrieveDmDrugListByDrugKeyFmStatus(Integer drugKey, String fmStatus) {
		if ( logger.isDebugEnabled() ) {
			logger.debug("retrieveDmDrugListByDrugKeyFmStatus - drugKey | #0", drugKey);
			logger.debug("retrieveDmDrugListByDrugKeyFmStatus - fmStatus | #0", fmStatus);
		}
		dmDrugList = dmDrugCacher.getDmDrugListByDrugKeyFmStatus(workstore, drugKey, fmStatus);
		if ( dmDrugList != null && logger.isDebugEnabled() ) {
			logger.debug("dmDrugList size #0", dmDrugList.size());
		}
	}
	
	public void retrieveDmDrugListByDrugKey(Integer drugKey) {
		if ( logger.isDebugEnabled() ) {
			logger.debug("retrieveDmDrugListByDrugKey - drugKey | #0", drugKey);			
		}
		dmDrugList = dmDrugCacher.getDmDrugListByDrugKey(workstore, drugKey);
		if ( dmDrugList != null && logger.isDebugEnabled() ) {
			logger.debug("dmDrugList size #0", dmDrugList.size());
		}
	}
	
	@Remove
	public void destroy() 
	{
		if (dmDrugList != null) {
			dmDrugList = null;
		}
	}
}
