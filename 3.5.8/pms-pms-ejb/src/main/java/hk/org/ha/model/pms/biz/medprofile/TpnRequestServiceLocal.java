package hk.org.ha.model.pms.biz.medprofile;

import javax.ejb.Local;

@Local
public interface TpnRequestServiceLocal {
	String newTpnRecord(int duration);
	String duplicateTpnRecord(int duration, Long pnId);
	boolean deleteTpnRecord(int selectedIndex, String deleteReason);
	void modifyTpnRecord(int duration, int selectedIndex);
	String modifyRegimen(int selectedIndex);
	String viewTpnRecord(int selectedIndex);
	boolean readTpnRecord(Long pnId, String mode, int duration, int selectedIndex);
	void retrieveTpnRequestList();
	void destroy();
}