package hk.org.ha.model.pms.biz.drug;

import static hk.org.ha.model.pms.prop.Prop.VETTING_MEDPROFILE_ATDPS_REFILL_DURATION;
import hk.org.ha.fmk.pms.remote.marshall.EclipseLinkXStreamMarshaller;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.medprofile.MedProfileItemConverterLocal;
import hk.org.ha.model.pms.biz.reftable.ItemDispConfigManagerLocal;
import hk.org.ha.model.pms.biz.reftable.mp.AomScheduleManagerLocal;
import hk.org.ha.model.pms.biz.vetting.CapdListManagerLocal;
import hk.org.ha.model.pms.biz.vetting.FmIndicationManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.corp.cache.DmSiteCacherInf;
import hk.org.ha.model.pms.corp.cache.SiteFormVerbCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmRegimen;
import hk.org.ha.model.pms.dms.persistence.DmSite;
import hk.org.ha.model.pms.dms.vo.Capd;
import hk.org.ha.model.pms.dms.vo.CommonOrder;
import hk.org.ha.model.pms.dms.vo.DrugName;
import hk.org.ha.model.pms.dms.vo.FormVerb;
import hk.org.ha.model.pms.dms.vo.PivasFormula;
import hk.org.ha.model.pms.dms.vo.PmsFmStatusSearchCriteria;
import hk.org.ha.model.pms.dms.vo.PreparationProperty;
import hk.org.ha.model.pms.dms.vo.PreparationSearchCriteria;
import hk.org.ha.model.pms.dms.vo.RelatedDrugSearchCriteria;
import hk.org.ha.model.pms.dms.vo.RouteForm;
import hk.org.ha.model.pms.dms.vo.RouteFormSearchCriteria;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItemFm;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.udt.drug.DrugScope;
import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
import hk.org.ha.model.pms.udt.medprofile.MedProfilePoItemRefillDurationUnit;
import hk.org.ha.model.pms.udt.reftable.mp.AomScheduleType;
import hk.org.ha.model.pms.udt.rx.RxItemType;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.CmsOrderSubType;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.udt.vetting.NameType;
import hk.org.ha.model.pms.udt.vetting.PrnPercentage;
import hk.org.ha.model.pms.udt.vetting.RegimenDurationUnit;
import hk.org.ha.model.pms.udt.vetting.RegimenType;
import hk.org.ha.model.pms.vo.drug.DrugSearchReturn;
import hk.org.ha.model.pms.vo.rx.CapdItem;
import hk.org.ha.model.pms.vo.rx.CapdRxDrug;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.Freq;
import hk.org.ha.model.pms.vo.rx.InjectionRxDrug;
import hk.org.ha.model.pms.vo.rx.OralRxDrug;
import hk.org.ha.model.pms.vo.rx.Regimen;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.security.PropMap;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;

@AutoCreate
@Stateless
@Name("drugSearchMpManager")
@MeasureCalls
public class DrugSearchMpManagerBean implements DrugSearchMpManagerLocal {
	
	@Logger
	private Log logger;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private MedProfileItemConverterLocal medProfileItemConverter;
	
	@In
	private FmIndicationManagerLocal fmIndicationManager; 
	
	@In
	private CapdListManagerLocal capdListManager;
	
	@In
	private SiteFormVerbCacherInf siteFormVerbCacher;
	
	@In
	private Workstore workstore;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private DmSiteCacherInf dmSiteCacher;
	
	@In
	private ItemDispConfigManagerLocal itemDispConfigManager;
	
	@In
	private AomScheduleManagerLocal aomScheduleManager;
	
	public DrugSearchReturn createMpFreeTextEntryDrug(DrugSearchSource drugSearchSource, RxDrug rxDrug, MedProfile medProfile, List<Integer> atdpsPickStationsNumList) {
		List<MedProfileMoItem> drugSearchMedProfileMoItemList = new ArrayList<MedProfileMoItem>();
		
		// create MedProfileMoItem
		MedProfileMoItem medProfileMoItem = new MedProfileMoItem();
		drugSearchMedProfileMoItemList.add(medProfileMoItem);

		rxDrug.setManualItemFlag(Boolean.TRUE);
		medProfileMoItem.setRxItem(rxDrug);

		// set MedProfileMoItem
		medProfileMoItem.setRxItemType(RxItemType.Oral);
		medProfileMoItem.setOrderDate(new Date());
		
		medProfileMoItem.clearMedProfilePoItemList();
		medProfileMoItem.clearMedProfileMoItemAlertList();

		// get vo
		Regimen regimen = rxDrug.getRegimen();
		DoseGroup doseGroup = regimen.getDoseGroupList().get(0);
		Dose dose = doseGroup.getDoseList().get(0);
		Freq dailyFreq = dose.getDailyFreq();
		
		// set vo
		rxDrug.setFmStatus("T");

		regimen.setType(RegimenType.Daily);		// default regimen type
		
		dose.setDispQty(null);		// default disp qty

		dailyFreq.setCode("");
		dailyFreq.setDesc("");
		
		
		// item conversion
		proceedItemConversion(medProfileMoItem, medProfile, atdpsPickStationsNumList); 

		
		// load DmInfo (must be loaded after item conversion)
		medProfileMoItem.loadDmInfo();	
		
		// create MedProfileItemList
		List<MedProfileItem> drugSearchMedProfileItemList = new ArrayList<MedProfileItem>();
		
		for (MedProfileMoItem drugSearchMedProfileMoItem : drugSearchMedProfileMoItemList) {
			MedProfileItem drugSearchMedProfileItem = new MedProfileItem();
			
			drugSearchMedProfileMoItem.setMedProfileItem(drugSearchMedProfileItem);
			drugSearchMedProfileItem.setMedProfileMoItem(drugSearchMedProfileMoItem);
			
			drugSearchMedProfileItemList.add(drugSearchMedProfileItem);
			
			// set item num
			Integer itemNum = medProfile.getAndIncreaseMaxItemNum();
			drugSearchMedProfileMoItem.setItemNum(itemNum);
			drugSearchMedProfileMoItem.setOrgItemNum(itemNum);
			drugSearchMedProfileItem.setOrgItemNum(itemNum);
			drugSearchMedProfileItem.setCreateDate(new Date());
		}
		
		
		DrugSearchReturn drugSearchReturn = new DrugSearchReturn();
		drugSearchReturn.setDrugSearchSource(drugSearchSource);
		drugSearchReturn.setDrugSearchMedProfileItemList(drugSearchMedProfileItemList);
		
		return drugSearchReturn;
	}
	
	public DrugSearchReturn createMpCapdDrug(DrugSearchSource drugSearchSource, String supplierSystem, String calciumStrength, Capd capd, MedProfile medProfile, List<Integer> atdpsPickStationsNumList) {
		List<MedProfileMoItem> drugSearchMedProfileMoItemList = new ArrayList<MedProfileMoItem>();

		// create CAPD MedProfileMoItem
		MedProfileMoItem medProfileMoItem = createCapdMedProfileMoItem(capd);
		drugSearchMedProfileMoItemList.add(medProfileMoItem);
		
		// item conversion
		proceedItemConversion(medProfileMoItem, medProfile, atdpsPickStationsNumList); 
		
		// load DmInfo (must be loaded after item conversion)
		medProfileMoItem.loadDmInfo();	
		
		// create MedProfileItemList
		List<MedProfileItem> drugSearchMedProfileItemList = new ArrayList<MedProfileItem>();
		
		for (MedProfileMoItem drugSearchMedProfileMoItem : drugSearchMedProfileMoItemList) {
			MedProfileItem drugSearchMedProfileItem = new MedProfileItem();
			
			drugSearchMedProfileMoItem.setMedProfileItem(drugSearchMedProfileItem);
			drugSearchMedProfileItem.setMedProfileMoItem(drugSearchMedProfileMoItem);
			
			drugSearchMedProfileItemList.add(drugSearchMedProfileItem);
			
			// set item num
			Integer itemNum = medProfile.getAndIncreaseMaxItemNum();
			drugSearchMedProfileMoItem.setItemNum(itemNum);
			drugSearchMedProfileMoItem.setOrgItemNum(itemNum);
			drugSearchMedProfileItem.setOrgItemNum(itemNum);
			drugSearchMedProfileItem.setCreateDate(new Date());
		}
		
		
		DrugSearchReturn drugSearchReturn = new DrugSearchReturn();
		drugSearchReturn.setDrugSearchSource(drugSearchSource);
		drugSearchReturn.setDrugSearchMedProfileItemList(drugSearchMedProfileItemList);
		
		return drugSearchReturn;
	}
	
	public DrugSearchReturn createSelectedMpDrugByVetting(DrugSearchSource drugSearchSource, DrugName drugName, 
															RouteForm routeForm, PreparationProperty preparation, 
															CommonOrder commonOrder, PivasFormula pivasFormula, DmRegimen dmRegimen, 
															String pasSpecialty, String pasSubSpecialty, String itemCode,
															MedProfile medProfile, List<Integer> atdpsPickStationsNumList) {
		// for strength compul drug, if select preparation, objects commonDosage, dmRegimen are null
		// for strength compul drug, if select commonDosage, object dmRegimen is null
		// for strength compul drug, if select dmRegimen, object commonDosage is null
		// for non-strength compul drug, if select routeForm, objects preparation, commonDosage, dmRegimen are null
		// for non-strength compul drug, if select commonDosage, objects preparation, dmRegimen are null
		// for non-strength compul drug, if select dmRegimen, objects preparation, commonDosage are null

		List<MedProfileMoItem> drugSearchMedProfileMoItemList = new ArrayList<MedProfileMoItem>();

		if (preparation != null) {
			// if both preparation exist, create item and perform item conversion

			MedProfileMoItem medProfileMoItem = createMedProfileMoItem(drugName, routeForm, preparation, commonOrder, pivasFormula, dmRegimen);
			proceedItemConversion(medProfileMoItem, medProfile, atdpsPickStationsNumList);		// perform item conversion 
			drugSearchMedProfileMoItemList.add(medProfileMoItem);

		} else {
			if (commonOrder == null) {
				// if both preparation and common dosage is null, obtain preparation from DMS
				// - if more than 1 preparation obtained, prompt preparation list
				// - otherwise create item and perform item conversion
				
				List<PreparationProperty> drugSearchPreparationList = retrievePreparationPropertyList(drugName, routeForm, pasSpecialty, pasSubSpecialty);
				
				if (drugSearchPreparationList == null || drugSearchPreparationList.size() == 0) {
					throw new UnsupportedOperationException("PreparationProperty list of non-strength compulsory drug is empty, case num = " + medProfile.getMedCase().getCaseNum());
				}
				
				// obtain preparation if the search is item code search
				PreparationProperty preparationByItemCodeSearch = null;
				if (itemCode != null) {
					for (PreparationProperty drugSearchPreparation : drugSearchPreparationList) {
						if (itemCode.equals(drugSearchPreparation.getItemCode())) {
							preparationByItemCodeSearch = drugSearchPreparation;
							break;
						}
					}
				}
				
				if (drugSearchPreparationList.size() > 1 && preparationByItemCodeSearch == null) {
					// prompt user to select preparation if more than 1 preparation is found
					DrugSearchReturn drugSearchReturn = new DrugSearchReturn();
					drugSearchReturn.setDrugSearchSource(drugSearchSource);
					drugSearchReturn.setDrugSearchPreparationList(drugSearchPreparationList);
					
					PropMap drugSearchPreparationDescMap = new PropMap();
					for (PreparationProperty drugSearchPreparation : drugSearchPreparationList) {
						DmDrug dmDrug = dmDrugCacher.getDmDrug(drugSearchPreparation.getItemCode());
						drugSearchPreparationDescMap.add(drugSearchPreparation.getItemCode(), dmDrug.getFullDrugDesc());
					}
					drugSearchReturn.setDrugSearchPreparationDescMap(drugSearchPreparationDescMap);
					
					return drugSearchReturn;
					
				} else {
					if (preparationByItemCodeSearch != null) {
						// create item with the preparation if item code search
						preparation = preparationByItemCodeSearch;
					} else {
						// create item with the preparation if only 1 preparation is found
						preparation = drugSearchPreparationList.get(0);
					}

					MedProfileMoItem medProfileMoItem = createMedProfileMoItem(drugName, routeForm, preparation, commonOrder, pivasFormula, dmRegimen);
					proceedItemConversion(medProfileMoItem, medProfile, atdpsPickStationsNumList);		// perform item conversion 
					drugSearchMedProfileMoItemList.add(medProfileMoItem);
				}
				
			} else {
				// note: since PIVAS formula and common order is exclusive, no need to handle PIVAS formula in this branch
				
				// if preparation is null but common order exists, try item conversion first to see item can be converted without preparation
				// - if item conversion fails, obtain preparation from DMS 
				// -- if more than 1 preparation obtained, prompt preparation list
				// -- otherwise create item and perform item conversion
				// - if 1 PO item is converted, use the converted item
				// - if more than 1 PO item is converted, link each converted MedProfilePoItem to individual MedProfileMoItem
				
				MedProfileMoItem convertMedProfileMoItem = createMedProfileMoItem(drugName, routeForm, preparation, commonOrder, pivasFormula, dmRegimen);

				// perform item conversion (link to dummy MedProfileItem and real MedProfile)
				proceedItemConversion(convertMedProfileMoItem, medProfile, atdpsPickStationsNumList); 


				// get item conversion result
				List<MedProfilePoItem> medProfilePoItemList = convertMedProfileMoItem.getMedProfilePoItemList();

				if (medProfilePoItemList == null || medProfilePoItemList.isEmpty()) {
					// get preparation from DMS if item conversion fails
					List<PreparationProperty> drugSearchPreparationList = retrievePreparationPropertyList(drugName, routeForm, pasSpecialty, pasSubSpecialty);
					
					if (drugSearchPreparationList == null || drugSearchPreparationList.size() == 0) {
						throw new UnsupportedOperationException("PreparationProperty list of non-strength compulsory drug is empty, case num = " + medProfile.getMedCase().getCaseNum());
					}
					
					if (drugSearchPreparationList.size() > 1) {
						// prompt user to select preparation if only 1 preparation is found
						DrugSearchReturn drugSearchReturn = new DrugSearchReturn();
						drugSearchReturn.setDrugSearchSource(drugSearchSource);
						drugSearchReturn.setDrugSearchPreparationList(drugSearchPreparationList);
						
						PropMap drugSearchPreparationDescMap = new PropMap();
						for (PreparationProperty drugSearchPreparation : drugSearchPreparationList) {
							DmDrug dmDrug = dmDrugCacher.getDmDrug(drugSearchPreparation.getItemCode());
							drugSearchPreparationDescMap.add(drugSearchPreparation.getItemCode(), dmDrug.getFullDrugDesc());
						}
						drugSearchReturn.setDrugSearchPreparationDescMap(drugSearchPreparationDescMap);
						
						return drugSearchReturn;
						
					} else {
						// create item again with the preparation if only 1 preparation is found
						preparation = drugSearchPreparationList.get(0);
						
						MedProfileMoItem medProfileMoItem = createMedProfileMoItem(drugName, routeForm, preparation, commonOrder, pivasFormula, dmRegimen);
						proceedItemConversion(medProfileMoItem, medProfile, atdpsPickStationsNumList);		// perform item conversion 
						drugSearchMedProfileMoItemList.add(medProfileMoItem);
					}
					
				} else if (medProfilePoItemList.size() == 1) {
					// use the converted item directly if only 1 PO item is converted
					drugSearchMedProfileMoItemList.add(convertMedProfileMoItem);
					
				} else {
					// link each converted MedProfilePoItem to individual MedProfileMoItem if more than 1 PO item is converted
					for (MedProfilePoItem medProfilePoItem : medProfilePoItemList) {
						medProfilePoItem.setMedProfileMoItem(null);		// clear linkage
						
						MedProfileMoItem medProfileMoItem = createMedProfileMoItem(drugName, routeForm, preparation, commonOrder, pivasFormula, dmRegimen);
						medProfileMoItem.addMedProfilePoItem(medProfilePoItem);
						medProfilePoItem.setMedProfileMoItem(medProfileMoItem);
						
						drugSearchMedProfileMoItemList.add(medProfileMoItem);
					}
				}
			}
		}
		

		// load DmInfo
		for (MedProfileMoItem mpmi : drugSearchMedProfileMoItemList) {
			// must be loaded after item conversion
			mpmi.loadDmInfo();
		}
		
		// add FM info
		for (MedProfileMoItem mpmi : drugSearchMedProfileMoItemList) {
			addMedProfileMoItemFm(medProfile, mpmi, drugSearchMedProfileMoItemList);

			// marshall rxItemXml by latest RxItem vo for use in recalculateQty()  
			// (note: for multiple PO items case, since the MedProfileMoItem is re-created after item conversion, rxItemXml is null)
			mpmi.preSave();
			
			if (mpmi.getMedProfilePoItemList().size() > 0) {
				// recal to update MedProfilePoItem actionStatus if MedProfileMoItem actionStatus is updated in addMedProfileMoItemFm() 
				MedProfilePoItem mppi = mpmi.getMedProfilePoItemList().get(0);
				recalculateQty(medProfile, mpmi, mppi);
				
				// update key field of converted MedProfilePoItem to MedProfileMoItem for alert use
				if (mpmi.getRxItem().isRxDrug()) {
					DmDrug dmDrug = DmDrugCacher.instance().getDmDrug(mppi.getItemCode());
					RxDrug rxDrug = (RxDrug)mpmi.getRxItem();
					rxDrug.setDisplayName(dmDrug.getDmDrugProperty().getDisplayname());
					rxDrug.setFormCode(mppi.getFormCode());
					rxDrug.setFormDesc(mppi.getDmFormLite().getMoeDesc());
					rxDrug.setSaltProperty(dmDrug.getDmDrugProperty().getSaltProperty());
					rxDrug.setItemCode(mppi.getItemCode());
				}
				
			} else {
				// each MedProfileMoItem must contain only 1 MedProfilePoItem, otherwise throw exception
				String errorDisplayName = drugName.getTrueDisplayname();
				String errorFormCode = routeForm.getFormCode();
				String errorSaltProperty = routeForm.getSaltProperty();
				String errorItemCode = null;
				if (preparation != null) {
					errorItemCode = preparation.getItemCode();
				}
				throw new UnsupportedOperationException("Item conversion of drug search fails and there is no pharmacy line, displayname = " + errorDisplayName + 
						", formCode = " + errorFormCode + ", saltProperty = " + errorSaltProperty + ", itemCode = " + errorItemCode);
			}
		}

		// create MedProfileItem for each MedProfileMoItem
		List<MedProfileItem> drugSearchMedProfileItemList = new ArrayList<MedProfileItem>();
		
		for (MedProfileMoItem drugSearchMedProfileMoItem : drugSearchMedProfileMoItemList) {
			MedProfileItem drugSearchMedProfileItem = new MedProfileItem();
			
			drugSearchMedProfileMoItem.setMedProfileItem(drugSearchMedProfileItem);
			drugSearchMedProfileItem.setMedProfileMoItem(drugSearchMedProfileMoItem);
			
			drugSearchMedProfileItemList.add(drugSearchMedProfileItem);
			
			// set item num
			Integer itemNum = medProfile.getAndIncreaseMaxItemNum();
			drugSearchMedProfileMoItem.setItemNum(itemNum);
			drugSearchMedProfileMoItem.setOrgItemNum(itemNum);
			drugSearchMedProfileItem.setOrgItemNum(itemNum);
			drugSearchMedProfileItem.setCreateDate(new Date());
		}
		
		// set group num if more than 1 drug item
		if (drugSearchMedProfileItemList.size() > 1) {
			Integer groupNum = drugSearchMedProfileItemList.get(0).getOrgItemNum();
			for (MedProfileItem mpi : drugSearchMedProfileItemList) {
				mpi.setGroupNum(groupNum);
			}
		}
		
		logger.debug(new EclipseLinkXStreamMarshaller().toXML(drugSearchMedProfileItemList));
		
		DrugSearchReturn drugSearchReturn = new DrugSearchReturn();
		drugSearchReturn.setDrugSearchSource(drugSearchSource);
		drugSearchReturn.setDrugSearchMedProfileItemList(drugSearchMedProfileItemList);

		return drugSearchReturn;
	}
	
	private MedProfileMoItem createMedProfileMoItem(DrugName drugName, RouteForm routeForm, PreparationProperty preparation, CommonOrder commonOrder, PivasFormula pivasFormula, DmRegimen dmRegimen) {
		if ( "PDF".equals(drugName.getTrueDisplayname().substring(0, 3)) && preparation != null) {
			List<Capd> capdConcentrationList = capdListManager.retrieveCapdConcentrationListByItemCode(preparation.getItemCode());
			if (capdConcentrationList == null || capdConcentrationList.isEmpty()) {
				throw new UnsupportedOperationException("PDF item not found");
			}

			return createCapdMedProfileMoItem(capdConcentrationList.get(0));
			
		} else {
			return createNonCapdMedProfileMoItem(drugName, routeForm, preparation, commonOrder, pivasFormula, dmRegimen);
		}
	}
	
	private MedProfileMoItem createCapdMedProfileMoItem(Capd capd) {
		MedProfileMoItem medProfileMoItem = new MedProfileMoItem();
		
		// create vo
		CapdRxDrug capdRxDrug = new CapdRxDrug(); 
		capdRxDrug.setManualItemFlag(Boolean.TRUE);
		medProfileMoItem.setRxItem(capdRxDrug);
		medProfileMoItem.setRxItemType(RxItemType.Capd);

		CapdItem capdItem = new CapdItem();
		capdRxDrug.addCapdItem(capdItem);
		
		// set MedProfileMoItem
		medProfileMoItem.setOrderDate(new Date());
		
		medProfileMoItem.clearMedProfilePoItemList();
		medProfileMoItem.clearMedProfileMoItemAlertList();
		
		// set vo
		capdRxDrug.setSupplierSystem(capd.getSupplierSystem());
		capdRxDrug.setCalciumStrength(capd.getCalciumStrength());
		
		capdItem.setItemCode(capd.getItemCode());
		capdItem.setSupplierCode(capd.getSupplierCode());
		capdItem.setConcentration(capd.getTrueConcentration());
		capdItem.setFullConcentration(capd.getConcentration());
		capdItem.setDosage(BigDecimal.valueOf(0));
		capdItem.setDosageUnit(capd.getBaseUnit());
		capdItem.setDuration(0);
		
		capdItem.setBaseUnit(capd.getBaseUnit());
		capdItem.setConnectSystem(capd.getConnectSystem());
		
		return medProfileMoItem;
	}

	private MedProfileMoItem createNonCapdMedProfileMoItem(DrugName drugName, RouteForm routeForm, PreparationProperty preparation, CommonOrder commonOrder, PivasFormula pivasFormula, DmRegimen dmRegimen) {
		MedProfileMoItem medProfileMoItem = new MedProfileMoItem();
		
		// create vo
		boolean createInjectionRxDrug = false;
		if (commonOrder != null && commonOrder.getSiteCode() != null) {
			DmSite dmSite = dmSiteCacher.getSiteBySiteCode(commonOrder.getSiteCode());
			if ("IJ".equals(dmSite.getSiteCategoryCode()) || "IF".equals(dmSite.getSiteCategoryCode()) || "CF".equals(dmSite.getSiteCategoryCode())) {
				// create InjectionRxDrug for non-oral item to pass item conversion 
				createInjectionRxDrug = true;
			}
		}
		RxDrug rxDrug;
		if (createInjectionRxDrug) {
			rxDrug = new InjectionRxDrug();
		} else {
			rxDrug = new OralRxDrug();
		}
		
		rxDrug.setManualItemFlag(Boolean.TRUE);
		medProfileMoItem.setRxItem(rxDrug);
		medProfileMoItem.setRxItemType(RxItemType.Oral);

		Regimen regimen = new Regimen();
		List<DoseGroup> doseGroupList = new ArrayList<DoseGroup>();
		List<Dose> doseList = new ArrayList<Dose>();
		DoseGroup doseGroup = new DoseGroup();		
		Dose dose = new Dose();
		
		doseList.add(dose);
		doseGroup.setDoseList(doseList);
		doseGroupList.add(doseGroup);				
		regimen.setDoseGroupList(doseGroupList);
		rxDrug.setRegimen(regimen);

		
		// set MedProfileMoItem
		// (item number is set after creating MedProfileItem)
		medProfileMoItem.setOrderDate(new Date());
		
		medProfileMoItem.clearMedProfilePoItemList();
		medProfileMoItem.clearMedProfileMoItemAlertList();		

		
		// set vo
		
		// set OralRxDrug (from DrugName and RouteForm)
		rxDrug.setDisplayName( drugName.getTrueDisplayname() );
		rxDrug.setFirstDisplayName( drugName.getFirstDisplayname() );
		rxDrug.setSecondDisplayName( drugName.getSecondDisplayname() );
		rxDrug.setFormCode( routeForm.getFormCode() );
		rxDrug.setFormDesc( routeForm.getMoeDesc() );
		rxDrug.setSaltProperty( routeForm.getSaltProperty() );
		if (preparation == null) {			
			rxDrug.setBaseUnit( routeForm.getGroupBaseUnit() );
			rxDrug.setFmStatus( routeForm.getPmsFmStatus() );
		}
		if ("Y".equals(routeForm.getDangerousDrug())) {
			rxDrug.setDangerDrugFlag(Boolean.TRUE);
		} else {
			rxDrug.setDangerDrugFlag(Boolean.FALSE);
		}
		if (preparation == null) {
			if ("Y".equals(routeForm.getSpecRestrict())) {
				rxDrug.setRestrictFlag(Boolean.TRUE);
			} else {
				rxDrug.setRestrictFlag(Boolean.FALSE);
			}
		}
		rxDrug.setAliasName( routeForm.getTrueAliasname() );
		rxDrug.setRouteCode( routeForm.getRouteCode() );
		rxDrug.setRouteDesc( routeForm.getFullRouteDesc() );
		if (routeForm.getDisplayRouteDesc() != null) {
			rxDrug.setDisplayRouteDescFlag(Boolean.TRUE);
		} else {
			rxDrug.setDisplayRouteDescFlag(Boolean.FALSE);
		}
		rxDrug.setNameType( NameType.DisplayName );	// always set nameType to DisplayName since there is no order line
		
		if (preparation == null) {
			if ( routeForm.getGroupMaxDuration() != null && routeForm.getGroupMaxDuration().intValue() > 0 ) {		
				rxDrug.setSingleUseFlag(Boolean.TRUE);
			}
		}
		
		// set Dose (from routeForm)
		if (preparation == null) {
			dose.setDosageUnit(routeForm.getGroupMoDosageUnit());
		}
		dose.setAdminTimeCode(routeForm.getAdminTimeCode());
		dose.setAdminTimeDesc(routeForm.getAdminTimeEng());
		if (preparation == null) {			
			dose.setBaseUnit(routeForm.getGroupBaseUnit());			
		}

		// set OralRxDrug (from Preparation)
		if (preparation != null) {
			rxDrug.setItemCode( preparation.getItemCode() );
			rxDrug.setBaseUnit( preparation.getBaseUnit() );
			rxDrug.setFmStatus( preparation.getPmsFmStatus() );
			if ("Y".equals(preparation.getSpecRestrict())) {
				rxDrug.setRestrictFlag(Boolean.TRUE);
			} else {
				rxDrug.setRestrictFlag(Boolean.FALSE);
			}
			rxDrug.setStrength( preparation.getStrength() );
			if (preparation.getVolumeValue() != null) {
				rxDrug.setVolume( new BigDecimal(preparation.getVolumeValue()) );
			}
			rxDrug.setVolumeUnit( preparation.getVolumeUnit() );
			rxDrug.setExtraInfo( preparation.getExtraInfo() );
			if ("Y".equals(preparation.getExternalInd())) {
				rxDrug.setExternalUseFlag(Boolean.TRUE);
			} else if ("N".equals(preparation.getExternalInd())) {
				rxDrug.setExternalUseFlag(Boolean.FALSE);
			}
			if ( preparation.getMaxDuration() != null && preparation.getMaxDuration().intValue() > 0 ) {				
				rxDrug.setSingleUseFlag(Boolean.TRUE);
			}
			
			// set Dose (from preparation)
			dose.setDosageUnit(preparation.getGroupMoDosageUnit());
			dose.setBaseUnit(preparation.getBaseUnit());
		}
		
		// set regimen type from DrugCommonDosage or DmRegimen
		if (commonOrder != null) {
			regimen.setType(RegimenType.dataValueOf(commonOrder.getRegimenType()));
		} else if (dmRegimen != null) {
			regimen.setType(RegimenType.dataValueOf(dmRegimen.getRegimenCode()));
		} else {
			regimen.setType(RegimenType.Daily);		// default regimen type
		}

		
		// set Regimen from DrugCommonDosage
		if (commonOrder != null) {
			// if common dosage is not null
			
			dose.setSiteCode(commonOrder.getSiteCode());
			dose.setSiteDesc(commonOrder.getSiteDescEng());
			dose.setIpmoeDesc(commonOrder.getIpmoeDesc());
			dose.setSupplSiteDesc(commonOrder.getSupplSiteDesc());

			logger.info("SiteCode:#0, SiteDesc:#1, IpmoeDesc:#2", dose.getSiteCode(), dose.getSiteDesc(), dose.getIpmoeDesc());
			
			if (commonOrder.getDoseValueFrom() != null) {
				if (commonOrder.getDoseValueFrom() == 0) {
					dose.setDosage(null);
				} else {
					dose.setDosage(new BigDecimal(commonOrder.getDoseValueFrom()));
				}
			}		

			if (commonOrder.getDoseValueTo() != null) {
				if (commonOrder.getDoseValueTo() == 0) {
					dose.setDosage(null);
				} else {
					dose.setDosage(new BigDecimal(commonOrder.getDoseValueTo()));
				}
			}		
			
			// set daily frequency desc
			StringBuilder freqDesc = new StringBuilder();
			if (commonOrder.getDailyFreq().getDailyFreqBlk1() != null) {
				freqDesc.append(commonOrder.getDailyFreq().getDailyFreqBlk1());
			}
			if (commonOrder.getFreqValue1() != null && commonOrder.getFreqValue1() != 0) {
				if (freqDesc.toString().length() > 0) {
					freqDesc.append(" ");
				}
				freqDesc.append(commonOrder.getFreqValue1().intValue());
			}
			if (commonOrder.getDailyFreq().getDailyFreqBlk2() != null) {
				if (freqDesc.toString().length() > 0) {
					freqDesc.append(" ");
				}
				freqDesc.append(commonOrder.getDailyFreq().getDailyFreqBlk2());
			}
			
			Freq dailyFreq = new Freq();
			dailyFreq.setCode( commonOrder.getFreqCode() );
			dailyFreq.setDesc(freqDesc.toString());
			if (commonOrder.getFreqValue1() != null && commonOrder.getFreqValue1().intValue() != 0) {
				String[] dailyFreqParamArr = new String[1];
				dailyFreqParamArr[0] = String.valueOf(commonOrder.getFreqValue1().intValue());
				dailyFreq.setParam(dailyFreqParamArr);
			}
			dose.setDailyFreq(dailyFreq);

			Freq supplFrequency = null;
			if (commonOrder.getSupplFreq() != null) {
				// set suppl frequency desc
				StringBuilder supFreqDesc = new StringBuilder();
				if (commonOrder.getSupplFreq().getSupplFreqBlk1() != null) {
					supFreqDesc.append(commonOrder.getSupplFreq().getSupplFreqBlk1());
				}
				if (commonOrder.getSupplFreqValue1() != null && commonOrder.getSupplFreqValue1() != 0) {
					if (supFreqDesc.toString().length() > 0) {
						supFreqDesc.append(" ");
					}
					supFreqDesc.append(commonOrder.getSupplFreqValue1().intValue());
				}
				if (commonOrder.getSupplFreq().getSupplFreqBlk2() != null) {
					if (supFreqDesc.toString().length() > 0) {
						supFreqDesc.append(" ");
					}
					supFreqDesc.append(commonOrder.getSupplFreq().getSupplFreqBlk2());
				}
				if (commonOrder.getSupplFreqValue2() != null && commonOrder.getSupplFreqValue2() != 0) {
					if (supFreqDesc.toString().length() > 0) {
						supFreqDesc.append(" ");
					}
					supFreqDesc.append(commonOrder.getSupplFreqValue2().intValue());
				}
				if (commonOrder.getSupplFreq().getSupplFreqBlk3() != null) {
					if (supFreqDesc.toString().length() > 0) {
						supFreqDesc.append(" ");
					}
					supFreqDesc.append(commonOrder.getSupplFreq().getSupplFreqBlk3());
				}			
			
				// set suppl freq
				supplFrequency = new Freq();
				supplFrequency.setCode( commonOrder.getSupplFreqCode() );
				supplFrequency.setDesc(supFreqDesc.toString());
				supplFrequency.setMultiplierType("-");
				if (commonOrder.getSupplFreqValue1() != null) {
					String[] supplFreqParamArr;
					if (commonOrder.getSupplFreqValue2() != null) {
						supplFreqParamArr = new String[2];
					} else {
						supplFreqParamArr = new String[1];
					}
					
					if ( commonOrder.getSupplFreqValue1().intValue() != 0 ) {
						supplFreqParamArr[0] = String.valueOf(commonOrder.getSupplFreqValue1().intValue());
					}
					if (commonOrder.getSupplFreqValue2() != null && commonOrder.getSupplFreqValue2().intValue() != 0) {
						supplFreqParamArr[1] = String.valueOf(commonOrder.getSupplFreqValue2().intValue());
					}
					supplFrequency.setParam(supplFreqParamArr);
				}
			}
			dose.setSupplFreq(supplFrequency);
									
			if ("Y".equals(commonOrder.getPrn())) {
				dose.setPrn(Boolean.TRUE);							
			}				
			
		} else if (pivasFormula != null) {
			dose.setSiteCode(pivasFormula.getSiteCode());
			dose.setSiteDesc(pivasFormula.getSiteDescEng());
			dose.setIpmoeDesc(pivasFormula.getIpmoeDesc());
			// no suppl site for PIVAS formula
			
			dose.setDosage(null);
			
			Freq dailyFreq = new Freq();
			dailyFreq.setCode("");
			dailyFreq.setDesc("");
			dose.setDailyFreq(dailyFreq);
			
			dose.setSupplFreq(null);
			
			medProfileMoItem.setPivasFormulaId(pivasFormula.getPivasFormulaId());
			medProfileMoItem.setPivasFlag(Boolean.TRUE);

		} else {
			// if common dosage is null
			FormVerb formVerb = siteFormVerbCacher.getDefaultFormVerbByFormCode( routeForm.getFormCode() );
			if (formVerb != null) {
				DmSite dmSite = dmSiteCacher.getSiteBySiteCode(formVerb.getSiteCode());
				dose.setSiteCode(dmSite.getSiteCode());
				dose.setSiteDesc(dmSite.getSiteDescEng());
				dose.setIpmoeDesc(dmSite.getIpmoeDesc());
				dose.setSiteCategoryCode(dmSite.getSiteCategoryCode());
				dose.setDmSite(dmSite);				
				if (formVerb.getSupplementarySite() != null) {				
					dose.setSupplSiteDesc(formVerb.getSupplementarySite());
				} 
				logger.info("SiteCode:#0, SiteDesc:#1, IpmoeDesc:#2, SupplSiteDesc:#3", dose.getSiteCode(), dose.getSiteDesc(), dose.getIpmoeDesc(), dose.getSupplSiteDesc());
			}
			
			dose.setDosage(null);
			
			Freq dailyFreq = new Freq();
			dailyFreq.setCode("");
			dailyFreq.setDesc("");
			dose.setDailyFreq(dailyFreq);
			
			dose.setSupplFreq(null);
		}

		// set default duration for item conversion
		doseGroup.setDuration(0);
		
		// set default duration unit
		if ( regimen.getType() == RegimenType.Weekly ) { 
			doseGroup.setDurationUnit(RegimenDurationUnit.Week);
		} else if ( regimen.getType() == RegimenType.Monthly ) {
			doseGroup.setDurationUnit(RegimenDurationUnit.Month);
		} else if ( regimen.getType() == RegimenType.Cyclic ) {
			doseGroup.setDurationUnit(RegimenDurationUnit.Cycle);
		}
		
		// set default dosage if form code is EYD
		if (commonOrder == null && "EYD".equals(rxDrug.getFormCode())) {
			dose.setDosage(new BigDecimal(1));			
		}	
		
		// set default prn and prn percent
		if (commonOrder == null || "N".equals(commonOrder.getPrn())) {
			dose.setPrn(Boolean.FALSE);
			dose.setPrnPercent(PrnPercentage.Hundred);
		}
		
		// set default disp qty
		dose.setDispQty(null);		
		if ( "Y".equals(routeForm.getDangerousDrug()) ) {
			dose.setBaseUnit(String.valueOf("DOSE"));
		}
		
		// add steps if regimen type is step-up/down
		if (regimen.getType() == RegimenType.StepUpDown) {
			for (int i=0; i<3; i++) {
				DoseGroup copyDoseGroup = new DoseGroup();
				BeanUtils.copyProperties(doseGroup, copyDoseGroup);
				regimen.getDoseGroupList().add(copyDoseGroup);
			}
		}
		
		return medProfileMoItem;
	}
	
	private void proceedItemConversion(MedProfileMoItem medProfileMoItem, MedProfile medProfile, List<Integer> atdpsPickStationsNumList) {
		// link to dummy MedProfileItem and real MedProfile
		MedProfileItem tempMedProfileItem = new MedProfileItem();
		tempMedProfileItem.setMedProfile(medProfile);
		medProfileMoItem.setMedProfileItem(tempMedProfileItem);
		
		// item conversion
    	List<MedProfileMoItem> tempMedProfileMoItemList = new ArrayList<MedProfileMoItem>();
    	tempMedProfileMoItemList.add(medProfileMoItem);
		medProfileItemConverter.convertItemList(workstore, tempMedProfileMoItemList, atdpsPickStationsNumList);
		
		// unlink afterward
		medProfileMoItem.setMedProfileItem(null);	
		
		// loadDmInfo for MedProfilePoItem
		for (MedProfilePoItem medProfilePoItem : medProfileMoItem.getMedProfilePoItemList()) {
			medProfilePoItem.setMedProfileMoItem(medProfileMoItem);
			medProfilePoItem.loadDmInfo();
			
			AomScheduleType aomScheduleType = Boolean.TRUE.equals(medProfileMoItem.getPivasFlag()) ? AomScheduleType.Pivas : AomScheduleType.General;
			medProfilePoItem.setDueDate(aomScheduleManager.getNextAdminDueDate(medProfilePoItem.getRegimen().getDailyFreqCodeList(), aomScheduleType));
			
			if (!FmStatus.FreeTextEntryItem.getDataValue().equals(medProfilePoItem.getFmStatus())) {
				medProfilePoItem.setItemDispConfig(itemDispConfigManager.retrieveItemDispConfig(
						medProfilePoItem.getItemCode(), medProfilePoItem.getWorkstore().getWorkstoreGroup(), false));
			}
		}
	}

	private List<PreparationProperty> retrievePreparationPropertyList(DrugName drugName, RouteForm routeForm, String pasSpecialty, String pasSubSpecialty) {
		
		PreparationSearchCriteria preparationSearchCriteria = new PreparationSearchCriteria();
		
		preparationSearchCriteria.setCostIncluded(Boolean.TRUE);
		preparationSearchCriteria.setDispHospCode(workstore.getHospCode());
		preparationSearchCriteria.setDispWorkstore(workstore.getWorkstoreCode());
		preparationSearchCriteria.setDrugScope("I");
		preparationSearchCriteria.setTrueDisplayname(drugName.getTrueDisplayname());
		preparationSearchCriteria.setFormCode(routeForm.getFormCode());
		preparationSearchCriteria.setSaltProperty(routeForm.getSaltProperty());
		preparationSearchCriteria.setPmsFmStatus(routeForm.getPmsFmStatus());
		preparationSearchCriteria.setSpecialtyType(CmsOrderSubType.InPatient.getDataValue());
		preparationSearchCriteria.setPasSpecialty(pasSpecialty);
		preparationSearchCriteria.setPasSubSpecialty(pasSubSpecialty);
		preparationSearchCriteria.setHqFlag(Boolean.FALSE);
		
		return dmsPmsServiceProxy.retrievePreparation( preparationSearchCriteria );
	}
	
	public void addMedProfileMoItemFm(MedProfile medProfile, MedProfileMoItem medProfileMoItem, List<MedProfileMoItem> shareMedProfileMoItemList) {
		if ( ! (medProfileMoItem.getRxItem() instanceof RxDrug)) {
			if (medProfileMoItem.getRxItemType() == RxItemType.Capd) {
				return;
			} else { 
				throw new UnsupportedOperationException("RxItem of MedProfileMoItem is not RxDrug, medProfileMoItem id = " + medProfileMoItem.getId());
			}
		}
		
		RxDrug rxDrug = (RxDrug)medProfileMoItem.getRxItem();
		
		if (FmStatus.dataValueOf(rxDrug.getFmStatus()) == FmStatus.FreeTextEntryItem) {
			return;
		}
		
		if (medProfile.getPrivateFlag()) {
			rxDrug.setActionStatus(ActionStatus.PurchaseByPatient);
			
		} else {
			FmStatus fmStatus = FmStatus.dataValueOf(rxDrug.getFmStatus());
			
			boolean validMedProfileMoItemFm = false;
			MedProfileMoItemFm medProfileMoItemFm = null;
			
			switch (fmStatus) {
				case SpecialDrug:
				case SelfFinancedItemC:
				case SelfFinancedItemD:
				case SafetyNetItem:
					// share FM for all MedProfileMoItem created in same IP drug search  
					if (shareMedProfileMoItemList != null) {
						for (MedProfileMoItem shareMedProfileMoItem : shareMedProfileMoItemList) {
							if ( medProfileMoItem != shareMedProfileMoItem && shareMedProfileMoItem.getMedProfileMoItemFmList() != null && 
									! shareMedProfileMoItem.getMedProfileMoItemFmList().isEmpty() ) {
								MedProfileMoItemFm shareMedProfileMoItemFm = shareMedProfileMoItem.getMedProfileMoItemFmList().get(0);
								medProfileMoItemFm = new MedProfileMoItemFm();
								BeanUtils.copyProperties(shareMedProfileMoItemFm, medProfileMoItemFm, new String[]{"medProfileMoItem"});
								break;
							}
						}
					}

					if (medProfileMoItemFm != null) {
						validMedProfileMoItemFm = true;
						
					} else {
						try {
							medProfileMoItemFm = fmIndicationManager.retrieveMedProfileMoItemFmByRxDrug(medProfile, rxDrug);
						} catch (Exception e) {
							logger.warn("drug search fails to call retrieveFm web service", e);
						}

						if (medProfileMoItemFm != null) {
							// validate FM corp ind and set indValidPeriodForDisplay by DMS valid period 
							boolean validFmCorpIndByDms = fmIndicationManager.validateFmCorpIndByDms(rxDrug, medProfileMoItemFm);
							if (validFmCorpIndByDms) {
								Integer validPeriod = medProfileMoItemFm.getIndValidPeriodForDisplay();
								DateTime authDateTime = new DateTime(medProfileMoItemFm.getFmCreateDate()).plusMonths(validPeriod.intValue());
								
								if (authDateTime.isAfter(new DateTime())) {
									validMedProfileMoItemFm = true;
									medProfileMoItemFm.setIndValidPeriod(validPeriod);	// update valid period by DMS value
									medProfileMoItemFm.setAuthDate(authDateTime.toDate());	// for display (for MOE item, authDate is loaded in postLoad)
								}
								
							}
						}
					}
					
					break;
			}
			
			if (validMedProfileMoItemFm) {
				medProfileMoItem.addMedProfileMoItemFm(medProfileMoItemFm);
				
				medProfileMoItem.setFmFlag(Boolean.TRUE);
				rxDrug.setFmFlag(Boolean.TRUE);
				
				// set default action status from presc option
				rxDrug.setActionStatus(ActionStatus.getDefaultActionStatusByPrescOption(medProfileMoItemFm.getPrescOption()));
				
			} else {
				PmsFmStatusSearchCriteria pmsFmStatusSearchCriteria = new PmsFmStatusSearchCriteria(); 
				pmsFmStatusSearchCriteria.setDispHospCode(workstore.getHospCode());
				pmsFmStatusSearchCriteria.setDispWorkstore(workstore.getWorkstoreCode());
				
				if (rxDrug.getStrengthCompulsory()) {
					pmsFmStatusSearchCriteria.setItemCode(rxDrug.getItemCode());
				} else {
					pmsFmStatusSearchCriteria.setTrueDisplayname(rxDrug.getDisplayName());
					pmsFmStatusSearchCriteria.setFormCode(rxDrug.getFormCode());
					pmsFmStatusSearchCriteria.setSaltProperty(rxDrug.getSaltProperty());
				}
				pmsFmStatusSearchCriteria.setPmsFmStatus(rxDrug.getFmStatus());
				
				// get special drug
				Boolean specialDrugFlag = fmIndicationManager.checkSpecialDrug(pmsFmStatusSearchCriteria);
				
				// set default action status from fmStatus
				rxDrug.setActionStatus(ActionStatus.getActionStatusByFmStatus(rxDrug.getFmStatus(), specialDrugFlag));
			}
		}		
	}
	
	public void recalculateQty(MedProfile medProfile, MedProfileMoItem medProfileMoItem, MedProfilePoItem medProfilePoItem) {
		if ( ! (medProfileMoItem.getRxItem() instanceof RxDrug)) {
			if (medProfileMoItem.getRxItemType() == RxItemType.Capd) {
				return;
			} else { 
				throw new UnsupportedOperationException("RxItem of MedProfileMoItem is not RxDrug, medProfileMoItem id = " + medProfileMoItem.getId());
			}
		}
		
		RxDrug rxDrug = (RxDrug)medProfileMoItem.getRxItem();
		
		if (FmStatus.dataValueOf(rxDrug.getFmStatus()) == FmStatus.FreeTextEntryItem) {
			return;
		}
				
		boolean needRecal = false;
		//Set default ATDPS refill duration when it is unit dose item and the refill duration is in day unit
		if (Boolean.TRUE.equals(medProfile.getAtdpsWardFlag()) 
				&& Boolean.TRUE.equals(medProfilePoItem.getUnitDoseItem()) 
				&& medProfilePoItem.getRefillDurationUnit() == MedProfilePoItemRefillDurationUnit.Day) {
			medProfilePoItem.setRefillDuration(VETTING_MEDPROFILE_ATDPS_REFILL_DURATION.get(1));
			needRecal = true;
		}
		if (rxDrug.getActionStatus() != medProfilePoItem.getActionStatus()) {
			medProfilePoItem.setActionStatus(rxDrug.getActionStatus());
			needRecal = true;
		}
		
		if (needRecal) {
			MedProfilePoItem recalculatedPoi = medProfileItemConverter.recalculateQty(workstore, medProfile, Arrays.asList(medProfilePoItem)).get(0);
			medProfilePoItem.setCalQty(recalculatedPoi.getCalQty());
			medProfilePoItem.setIssueQty(recalculatedPoi.getCalQty());
			medProfilePoItem.setAdjQty(recalculatedPoi.getAdjQty());
			medProfilePoItem.setTotalIssueQty(recalculatedPoi.getTotalIssueQty());
			medProfilePoItem.setDoseQty(recalculatedPoi.getDoseQty());
		}
		
	}
	
	public MedProfileItem createMedProfileItemByItemCode(String itemCode, MedProfile medProfile, List<Integer> atdpsPickStationsNumList, boolean includeFm) {
		MedProfileItem medProfileItem = new MedProfileItem();
		
		MedProfileMoItem medProfileMoItem = createMedProfileMoItemByItemCode(itemCode, medProfile, atdpsPickStationsNumList, includeFm);
		
		medProfileMoItem.setMedProfileItem(medProfileItem);
		medProfileItem.setMedProfileMoItem(medProfileMoItem);
		
		return medProfileItem;
	}
	
	public MedProfileMoItem createMedProfileMoItemByItemCode(String itemCode, MedProfile medProfile, List<Integer> atdpsPickStationsNumList, boolean includeFm) {
		// used by CARS2 non-IPMOE item edit to change to another drug by item code lookup

		String pasSpecialty;
		String pasSubSpecialty;
		if (medProfile.getMedCase().getPasSpecCode() != null) {
			pasSpecialty = medProfile.getMedCase().getPasSpecCode();
		} else {
			pasSpecialty = "dummy";
		}
		if (medProfile.getMedCase().getPasSubSpecCode() != null) {
			pasSubSpecialty = medProfile.getMedCase().getPasSubSpecCode();
		} else {
			pasSubSpecialty = "";
		}
		
		// set criteria
		RelatedDrugSearchCriteria relatedDrugSearchCriteria = new RelatedDrugSearchCriteria();
		relatedDrugSearchCriteria.setItemCode(itemCode);
		relatedDrugSearchCriteria.setDrugScope(DrugScope.IpDrugList.getDataValue());
		relatedDrugSearchCriteria.setDispHospCode(workstore.getHospCode());
		relatedDrugSearchCriteria.setDispWorkstore(workstore.getWorkstoreCode());
		
		RouteFormSearchCriteria routeFormSearchCriteria = new RouteFormSearchCriteria();
		routeFormSearchCriteria.setPasSpecialty(pasSpecialty);
		routeFormSearchCriteria.setPasSubSpecialty(pasSubSpecialty);
		routeFormSearchCriteria.setSpecialtyType("I");
		routeFormSearchCriteria.setItemCode(itemCode);
		routeFormSearchCriteria.setDrugScope(DrugScope.IpDrugList.getDataValue()); 
		routeFormSearchCriteria.setDispHospCode(workstore.getHospCode());
		routeFormSearchCriteria.setDispWorkstore(workstore.getWorkstoreCode());
		
		// get DrugName
		List<DrugName> drugNameList = dmsPmsServiceProxy.retrieveRelatedDrug(relatedDrugSearchCriteria);
		
		DrugName drugName = drugNameList.get(0);
		
		routeFormSearchCriteria.setFirstDisplayname(drugName.getFirstDisplayname());
		routeFormSearchCriteria.setSecondDisplayname(drugName.getSecondDisplayname());
		routeFormSearchCriteria.setTrueDisplayname(drugName.getTrueDisplayname());
		routeFormSearchCriteria.setNameType(drugName.getNameType());
		routeFormSearchCriteria.setPmsFmStatus(drugName.getPmsFmStatus());
		routeFormSearchCriteria.setConcatIngredients(drugName.getConcatIngredients());
		
		// get RouteForm
		List<RouteForm> routeFormList = dmsPmsServiceProxy.retrieveDrugSearchDetail(routeFormSearchCriteria);
		
		RouteForm routeForm = routeFormList.get(0);
		
		// get PreparationProperty
		PreparationProperty preparation = null;
		if (routeForm.getPreparationProperties() != null && routeForm.getPreparationProperties().length > 0) {
			preparation = routeForm.getPreparationProperties()[0];
			
		} else { 
			List<PreparationProperty> drugSearchPreparationList = retrievePreparationPropertyList(drugName, routeForm, pasSpecialty, pasSubSpecialty);
			
			if (drugSearchPreparationList == null || drugSearchPreparationList.size() == 0) {
				throw new UnsupportedOperationException("PreparationProperty list of non-strength compulsory drug is empty, case num = " + medProfile.getMedCase().getCaseNum());
			}
			
			for (PreparationProperty drugSearchPreparation : drugSearchPreparationList) {
				if (itemCode.equals(drugSearchPreparation.getItemCode())) {
					preparation = drugSearchPreparation;
					break;
				}
			}
		}
		
		// create MedProfileMoItem
		MedProfileMoItem medProfileMoItem = createMedProfileMoItem(drugName, routeForm, preparation, null, null, null);
		
		// item conversion
		proceedItemConversion(medProfileMoItem, medProfile, atdpsPickStationsNumList); 
		
		// load DmInfo (must be loaded after item conversion)
		medProfileMoItem.loadDmInfo();
		
		// add FM info
		if (includeFm) {
			addMedProfileMoItemFm(medProfile, medProfileMoItem, null);
		}
		
		if (medProfileMoItem.getMedProfilePoItemList().size() > 0) {
			// recal to update MedProfilePoItem actionStatus if MedProfileMoItem actionStatus is updated in addMedProfileMoItemFm() 
			MedProfilePoItem medProfilePoItem = medProfileMoItem.getMedProfilePoItemList().get(0);
			recalculateQty(medProfile, medProfileMoItem, medProfilePoItem);

			// update key field of converted MedProfilePoItem to MedProfileMoItem for alert use 
			if (medProfileMoItem.getRxItem().isRxDrug()) {
				DmDrug dmDrug = DmDrugCacher.instance().getDmDrug(medProfilePoItem.getItemCode());
				RxDrug rxDrug = (RxDrug)medProfileMoItem.getRxItem();
				rxDrug.setDisplayName(dmDrug.getDmDrugProperty().getDisplayname());
				rxDrug.setFormCode(medProfilePoItem.getFormCode());
				rxDrug.setFormDesc(medProfilePoItem.getDmFormLite().getMoeDesc());
				rxDrug.setSaltProperty(dmDrug.getDmDrugProperty().getSaltProperty());
				rxDrug.setItemCode(medProfilePoItem.getItemCode());
			}
			
		} else {
			// each MedProfileMoItem must contain only 1 MedProfilePoItem, otherwise throw exception
			String errorDisplayName = drugName.getTrueDisplayname();
			String errorFormCode = routeForm.getFormCode();
			String errorSaltProperty = routeForm.getSaltProperty();
			String errorItemCode = null;
			if (preparation != null) {
				errorItemCode = preparation.getItemCode();
			}
			throw new UnsupportedOperationException("Item conversion of drug search fails and there is no pharmacy line, displayname = " + errorDisplayName + 
					", formCode = " + errorFormCode + ", saltProperty = " + errorSaltProperty + ", itemCode = " + errorItemCode);
		}
		
		logger.debug(new EclipseLinkXStreamMarshaller().toXML(medProfileMoItem));
		
		return medProfileMoItem;
	}
	
	@Remove
	public void destroy() 
	{
	}
}