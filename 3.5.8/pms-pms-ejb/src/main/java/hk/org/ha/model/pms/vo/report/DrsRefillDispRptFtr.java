package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class DrsRefillDispRptFtr implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String refillCouponNum;

	public String getRefillCouponNum() {
		return refillCouponNum;
	}

	public void setRefillCouponNum(String refillCouponNum) {
		this.refillCouponNum = refillCouponNum;
	}

}
