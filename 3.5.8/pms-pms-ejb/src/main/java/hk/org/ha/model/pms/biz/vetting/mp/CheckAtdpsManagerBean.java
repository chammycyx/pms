package hk.org.ha.model.pms.biz.vetting.mp;

import static hk.org.ha.model.pms.prop.Prop.MACHINE_ATDPS_FRACTIONALDOSE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.MACHINE_ATDPS_MULTIDOSE_UNEVENDOSAGE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.MACHINE_ATDPS_PACKINGMETHOD;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.RegimenType;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.IvAdditive;
import hk.org.ha.model.pms.vo.rx.IvRxDrug;
import hk.org.ha.model.pms.vo.rx.Regimen;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("checkAtdpsManager")
@MeasureCalls
public class CheckAtdpsManagerBean implements CheckAtdpsManagerLocal {

	@Logger
	private Log logger;
	
	private final static String UNIT_DOSE = "unitDose";
	
	@Override
	public boolean isValidAtdpsItem(MedProfileMoItem medProfileMoItem, MedProfilePoItem medProfilePoItem, List<Integer> atdpsPickStationsNumList, boolean checkDuration, boolean callByVetting) {
		if ( atdpsPickStationsNumList.isEmpty() ) {
			logger.debug("atdpsPickStationsNumList is empty : #0", medProfilePoItem.getItemCode());
			return false;
		}
		
		return isValidAtdpsItem((Workstore) Contexts.getSessionContext().get("workstore"), medProfileMoItem, medProfilePoItem, checkDuration, callByVetting);
	}

	@Override
	public boolean isValidAtdpsItem(Workstore workstore, MedProfileMoItem medProfileMoItem, MedProfilePoItem medProfilePoItem, boolean checkDuration, boolean callByVetting) {
		Regimen regimen = medProfilePoItem.getRegimen();
		
		//ATDPS proceeds in daily regimen only
		if (regimen.getType() != RegimenType.Daily) {
			logger.debug("ATDPS proceeds in daily regimen only : #0 : #1", medProfilePoItem.getItemCode(), regimen.getType());
			return false;
		}
		
		if ( (checkDuration && UNIT_DOSE.equals(MACHINE_ATDPS_PACKINGMETHOD.get(workstore)) || regimen.getDoseGroupList().get(0).getDoseList().size() > 1) ) {
			//isStatDose 
			if ( "00036".equals(regimen.getDoseGroupList().get(0).getDoseList().get(0).getDailyFreq().getCode()) ) {
				logger.debug("StatDose item : #0", medProfilePoItem.getItemCode());
				return false;
			}	
			//isAtOnce 
			if ( "00034".equals(regimen.getDoseGroupList().get(0).getDoseList().get(0).getDailyFreq().getCode()) ) {
				logger.debug("AtOnce item : #0", medProfilePoItem.getItemCode());
				return false;
			}	
		}	
		
		for (DoseGroup doseGroup : regimen.getDoseGroupList()) {			
			for (Dose dose : doseGroup.getDoseList() ) {
				//isDoseSpecified
				if ( (checkDuration && UNIT_DOSE.equals(MACHINE_ATDPS_PACKINGMETHOD.get(workstore))) || doseGroup.getDoseList().size() > 1 ) {
					if ( Boolean.TRUE.equals(medProfileMoItem.getDoseSpecifiedFlag()) && medProfilePoItem.getSingleDispFlag() == true ) {
						logger.debug("DoseSpecified item : #0 : #1", medProfilePoItem.getItemCode(), dose.getDispQty());
						return false;
					}
				}
				//contains supplementary frequency
				if ( dose.getSupplFreq() != null && StringUtils.isNotBlank(dose.getSupplFreq().getCode()) ) {
					logger.debug("item contains supplementary frequency : #0 : #1", medProfilePoItem.getItemCode(), dose.getSupplFreq().getCode());
					return false;
				}
			}
		}
		
		//isOwnStockOrFreeTextEntry
		boolean isOwnStockOrFreeTextEntry = false;
		RxItem rxItem = medProfilePoItem.getMedProfileMoItem().getRxItem();
		if ( rxItem.isRxDrug() ) {
			RxDrug rxDrug = (RxDrug)rxItem ;
			logger.debug("rxDrug ActionStatus : #0 : #1", medProfilePoItem.getItemCode(), rxDrug.getActionStatus());
			logger.debug("rxDrug FmStatus : #0 : #1", medProfilePoItem.getItemCode(), rxDrug.getFmStatus());
			isOwnStockOrFreeTextEntry = ( rxDrug.getActionStatus() == ActionStatus.ContinueWithOwnStock || "T".equals(rxDrug.getFmStatus()) );
		} else if ( rxItem.isIvRxDrug() ) {
			IvRxDrug ivRxDrug = (IvRxDrug)rxItem;
			for (IvAdditive ivAdditive : ivRxDrug.getIvAdditiveList() ) {
				if (ivAdditive.getActionStatus() == ActionStatus.ContinueWithOwnStock) {
					logger.debug("ivAdditive ActionStatus : #0 : #1", medProfilePoItem.getItemCode(), ivAdditive.getActionStatus());
					isOwnStockOrFreeTextEntry = true;
					break;
				}
			}
		}
		if ( isOwnStockOrFreeTextEntry ) {
			logger.debug("OwnStock Or FreeTextEntry : #0", medProfilePoItem.getItemCode());
			return false;
		}

		boolean printLabelFlag = false;
		if ( medProfilePoItem.getActionStatus() != ActionStatus.ContinueWithOwnStock && !"NOT 01".equals(medProfilePoItem.getItemCode())) {
			if ( !medProfilePoItem.getWardStockFlag() && !medProfilePoItem.getDangerDrugFlag() ){
				printLabelFlag = true;
			} else if (medProfilePoItem.getReDispFlag()) {
				printLabelFlag = true;
			} 
		}

		if ( !printLabelFlag ) { 
			return false;
		}
		
		if (callByVetting) {
			if (isFractional(regimen) && !(MACHINE_ATDPS_FRACTIONALDOSE_ENABLED.get())) {
				return false;
			} else if (isUneven(regimen) && !(MACHINE_ATDPS_MULTIDOSE_UNEVENDOSAGE_ENABLED.get())) {
				return false;  
			}
		}
		return true;
	}	
	
	
	
	//isFractionalDosage
	private boolean isFractional(Regimen regimen) {
		for (DoseGroup doseGroup : regimen.getDoseGroupList()) {			
			for (Dose dose : doseGroup.getDoseList()) {
				if (dose.getDosage() != null) {
					if (dose.getDosage().multiply(BigDecimal.valueOf(10000)).intValue() % 10000 > 0) {					
						return true;
					}
				}
			}
		}
		return false;
	}
	
	//isMultipleDoseWithUnevenDosage
	private boolean isUneven(Regimen regimen) { 
		Dose firstDose = regimen.getDoseGroupList().get(0).getDoseList().get(0);
		double firstDosage = firstDose.getDosage() == null?0:firstDose.getDosage().setScale(4,RoundingMode.HALF_UP).doubleValue();
		for (DoseGroup doseGroup : regimen.getDoseGroupList()) {			
			for (Dose dose : doseGroup.getDoseList() ) {
				double dosage = dose.getDosage() == null?0:dose.getDosage().setScale(4,RoundingMode.HALF_UP).doubleValue();
				if (firstDosage != dosage) {
					return true;
				}	
			}
		}
		return false;
	}
}
