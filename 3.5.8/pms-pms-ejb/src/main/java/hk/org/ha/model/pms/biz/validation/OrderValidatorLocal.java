package hk.org.ha.model.pms.biz.validation;

import hk.org.ha.model.pms.udt.validation.SearchTextType;

import javax.ejb.Local;

@Local
public interface OrderValidatorLocal {
	
	SearchTextType checkSearchType(String searchText);
}