package hk.org.ha.model.pms.persistence.medprofile;

import hk.org.ha.model.pms.corp.cache.DmCorpIndicationCacher;
import hk.org.ha.model.pms.persistence.FmEntity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.jboss.seam.security.Identity;
import org.joda.time.DateTime;

@Entity
@Table(name = "MED_PROFILE_MO_ITEM_FM")
public class MedProfileMoItemFm extends FmEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "medProfileMoItemFmSeq")
	@SequenceGenerator(name = "medProfileMoItemFmSeq", sequenceName = "SQ_MED_PROFILE_MO_ITEM_FM", initialValue = 100000000)
	private Long id;
		
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MED_PROFILE_MO_ITEM_ID", nullable = false)
	private MedProfileMoItem medProfileMoItem;

	@Transient
	private Date authDate;
	
    @PostLoad
    public void postLoad() {
    	if (Identity.instance().isLoggedIn()) {
    		if (this.getCorpIndCode() != null) {
    			setDmCorpIndication(DmCorpIndicationCacher.instance().getDmCorpIndication(this.getCorpIndCode()));
    		}
    	}
    	
    	DateTime authDateTime = new DateTime(getFmCreateDate()).plusMonths(getIndValidPeriod());
    	authDate = authDateTime.toDate();
    }
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MedProfileMoItem getMedProfileMoItem() {
		return medProfileMoItem;
	}

	public void setMedProfileMoItem(MedProfileMoItem medProfileMoItem) {
		this.medProfileMoItem = medProfileMoItem;
	}

	public Date getAuthDate() {
		return (authDate == null) ? null : new Date(authDate.getTime());
	}

	public void setAuthDate(Date authDate) {
		this.authDate = (authDate == null) ? null : new Date(authDate.getTime());
	}	
}
