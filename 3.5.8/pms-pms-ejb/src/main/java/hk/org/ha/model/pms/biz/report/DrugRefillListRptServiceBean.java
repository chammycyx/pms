package hk.org.ha.model.pms.biz.report;

import static hk.org.ha.model.pms.prop.Prop.LABEL_DIVIDERLABEL_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.LABEL_MPDISPLABEL_SORT;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.delivery.DueOrderAsyncPrintManagerLocal;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.biz.reftable.WardConfigManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.Delivery;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryRequest;
import hk.org.ha.model.pms.persistence.reftable.WardConfig;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.medprofile.DeliveryRequestOrderType;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.medprofile.Ward;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.DrugRefillListRptInfo;
import hk.org.ha.model.pms.vo.report.MpTrxRpt;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;
@Stateful
@Scope(ScopeType.SESSION)
@Name("drugRefillListRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DrugRefillListRptServiceBean implements DrugRefillListRptServiceLocal {

	@Logger
	private Log logger;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent;
	
	@In 
	private Workstore workstore;
	
	@In 
	private Workstation workstation;
	
	@In
	private WardConfigManagerLocal wardConfigManager;
	
	@In
	private DueOrderAsyncPrintManagerLocal dueOrderAsyncPrintManager;
	
	@In
	private MpTrxRptPrintManagerLocal mpTrxRptPrintManager;
	
	private List<MpTrxRpt> drugRefillListRptList;
	
	private boolean retrieveSuccess;
	
    private Map<String, Object> parameters = new HashMap<String, Object>();
    
    public DrugRefillListRptInfo retrieveDrugRefillListRptInfo(){
    	DrugRefillListRptInfo refillListRptInfo = new DrugRefillListRptInfo();
    	List<String> mpWardCodeList = retrieveMpWardCodeList();
    	refillListRptInfo.setWardCodeList(mpWardCodeList);
    	
    	return refillListRptInfo;
    }
    
    private List<String> retrieveMpWardCodeList(){
    	List<String> mpWardCodeList = new ArrayList<String>();
    	List<WardConfig> wardConfigList = wardConfigManager.retrieveMpWardConfigList(workstore.getWorkstoreGroup());
    	
		for ( WardConfig wardConfig : wardConfigList ) {
			mpWardCodeList.add(wardConfig.getWardCode());
		}
    	
    	return mpWardCodeList;
    }
		
    public void retrieveDrugRefillList(DrugRefillListRptInfo drugRefillListRptInfo){
    	retrieveSuccess = false; 
    	drugRefillListRptList = new ArrayList<MpTrxRpt>();
    	 
		DeliveryRequest deliveryReq = new DeliveryRequest();
		Date deliveryReqDate = new Date();
		deliveryReq.setOrderType(DeliveryRequestOrderType.Both);
		deliveryReq.setNewOrderFlag(Boolean.TRUE);
		deliveryReq.setUrgentFlag(Boolean.TRUE);
		deliveryReq.setStatFlag(Boolean.TRUE);
		deliveryReq.setRefillFlag(Boolean.TRUE);
		deliveryReq.setCaseNum(drugRefillListRptInfo.getCaseNum());
		deliveryReq.setDueDate(drugRefillListRptInfo.getDueDate());
		deliveryReq.setGenerateDay(drugRefillListRptInfo.getRefillGenerateDay());
    	 
    	deliveryReq.setWardList(retrieveWardListByWardCodeList(drugRefillListRptInfo.getWardCodeList()));
    	deliveryReq.setCreateDate(deliveryReqDate);
		deliveryReq.setWorkstore(workstore);
		deliveryReq.setWorkstationCode(workstation.getWorkstationCode());		
    	deliveryReq.setSortName(LABEL_MPDISPLABEL_SORT.get());    	
		deliveryReq.setDividerLabelFlag(LABEL_DIVIDERLABEL_ENABLED.get(false));
		
		List<Delivery> deliveryList = dueOrderAsyncPrintManager.retrieveDeliveryListForDrugRefillList(deliveryReq, drugRefillListRptInfo.getIncludeDDnWardStockFlag());
				 
		logger.info("deliveryList SizE:#0", deliveryList.size());
		
		if( deliveryList.size() > 0 ){
			drugRefillListRptList = mpTrxRptPrintManager.retrieveMpTrxRptListByDeliveryList(deliveryList);
			
			for(MpTrxRpt drugRefillRpt : drugRefillListRptList){
				drugRefillRpt.getMpTrxRptHdr().setDrugRefillListFlag(true);
			}
			retrieveSuccess = true;
		}
    }
    
    private List<Ward> retrieveWardListByWardCodeList(List<String> wardCodeList){
    	List<Ward> wardList = new ArrayList<Ward>();
    
    	for(String wardCode: wardCodeList){
    		Ward ward = new Ward();
    		ward.setWardCode(wardCode);
    		wardList.add(ward);
    	}
    	
    	return wardList; 
    }
	
	public void generateDrugRefillListRpt() {
		parameters.put("hospCode", workstore.getHospCode());
		printAgent.renderAndRedirect(new RenderJob(
				"MpTrxRpt", 
				new PrintOption(), 
				parameters, 
				drugRefillListRptList));
	}

	public void printDrugRefillListRpt(){
		printAgent.renderAndPrint(new RenderAndPrintJob(
				"MpTrxRpt", 
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				parameters, 
				drugRefillListRptList));
	}
	
	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}

	@Remove
	public void destroy() {
		if ( drugRefillListRptList != null ) {
			drugRefillListRptList = null;
		}
	}

}
