package hk.org.ha.model.pms.persistence.disp;


import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.DupChkDrugType;
import hk.org.ha.model.pms.udt.disp.PharmOrderItemStatus;
import hk.org.ha.model.pms.udt.label.DeltaChangeType;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.util.StopWatcher;
import hk.org.ha.model.pms.vo.drug.DmDrugLite;
import hk.org.ha.model.pms.vo.drug.DmFormLite;
import hk.org.ha.model.pms.vo.drug.DmWarningLite;
import hk.org.ha.model.pms.vo.rx.Regimen;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.granite.messaging.amf.io.util.externalizer.annotation.IgnoredProperty;
import org.jboss.seam.contexts.Contexts;

@Entity
@Table(name = "PHARM_ORDER_ITEM")
public class PharmOrderItem extends VersionEntity {

	private static final long serialVersionUID = 1L;

	private static final String JAXB_CONTEXT_RX = "hk.org.ha.model.pms.vo.rx";
			
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pharmOrderItemSeq")
	@SequenceGenerator(name = "pharmOrderItemSeq", sequenceName = "SQ_PHARM_ORDER_ITEM", initialValue = 100000000)
	private Long id;

	@Column(name = "ITEM_CODE", nullable = false, length = 6)
	private String itemCode;
		
	@Lob
	@Column(name = "REGIMEN_XML")
	private String regimenXml;
	
	@Column(name = "DOSE_QTY", precision = 19, scale = 4)
	private BigDecimal doseQty;
	
	@Column(name = "DOSE_UNIT", length = 15)
	private String doseUnit;
	
	@Column(name = "CAL_QTY", nullable = false, precision = 19, scale = 4)
	private BigDecimal calQty;
	
	@Column(name = "ADJ_QTY", nullable = false, precision = 19, scale = 4)
	private BigDecimal adjQty;
	
	@Column(name = "ISSUE_QTY", nullable = false, precision = 19, scale = 4)
	private BigDecimal issueQty;
	
	@Column(name = "SFI_QTY")
	private Integer sfiQty;
	
	@Column(name = "BASE_UNIT", length = 4)
	private String baseUnit;
	
	@Converter(name = "PharmOrderItem.status", converterClass = PharmOrderItemStatus.Converter.class)
    @Convert("PharmOrderItem.status")
	@Column(name="STATUS", nullable = false, length = 1)
	private PharmOrderItemStatus status;

	@Column(name = "WARN_CODE_1", length = 2)
	private String warnCode1;
	
	@Column(name = "WARN_CODE_2", length = 2)
	private String warnCode2;
	
	@Column(name = "WARN_CODE_3", length = 2)
	private String warnCode3;
	
	@Column(name = "WARN_CODE_4", length = 2)
	private String warnCode4;
	
	@Column(name = "DRUG_KEY")
	private Integer drugKey;
	
	@Column(name = "DRUG_NAME", length = 59)
	private String drugName;
	
	@Column(name = "TRADE_NAME", length = 20)
	private String tradeName;
	
	@Column(name = "FORM_CODE", length = 3)
	private String formCode;
	
	@Column(name = "FORM_LABEL_DESC", length = 30)
	private String formLabelDesc;

	@Column(name = "ROUTE_CODE", length = 7)
	private String routeCode;
	
	@Column(name = "ROUTE_DESC", length = 20)
	private String routeDesc;
	
	@Column(name = "STRENGTH", length = 59)
	private String strength;
		
	@Column(name = "VOLUME_TEXT", length = 23)
	private String volumeText;

	@Column(name = "ITEM_NUM", nullable = false)
	private Integer itemNum;
	
	@Column(name = "ORG_ITEM_NUM", nullable = false)
	private Integer orgItemNum;
	
	@Column(name = "CONNECT_SYSTEM", length = 20)
	private String connectSystem;
	
	@Column(name = "CAPD_VOUCHER_FLAG")
	private Boolean capdVoucherFlag;
	
	@Column(name = "MO_ISSUE_QTY")
	private Integer moIssueQty;
	
	@Column(name = "MO_BASE_UNIT", length = 4)
	private String moBaseUnit;
	
	@Column(name = "DUP_CHK_DISPLAY_NAME", length = 70)
	private String dupChkDisplayName;
	
	@Converter(name = "PharmOrderItem.dupChkDrugType", converterClass = DupChkDrugType.Converter.class)
    @Convert("PharmOrderItem.dupChkDrugType")
	@Column(name="DUP_CHK_DRUG_TYPE", nullable = false, length = 1)
	private DupChkDrugType dupChkDrugType;

	@Converter(name = "PharmOrderItem.actionStatus", converterClass = ActionStatus.Converter.class)
	@Convert("PharmOrderItem.actionStatus")
	@Column(name = "ACTION_STATUS", nullable = false, length = 1)
	private ActionStatus actionStatus;

	@Column(name = "FM_STATUS", length = 1)
	private String fmStatus;
	
	@Column(name = "DANGER_DRUG_FLAG")
	private Boolean dangerDrugFlag;	

	@Column(name = "WARD_STOCK_FLAG", nullable = false)
	private Boolean wardStockFlag;
	
	@Column(name = "REMARK_TEXT", length = 125)
	private String remarkText;
	
	@Column(name = "ORG_FORM_CODE", length = 3)
	private String orgFormCode;
	
	@Column(name = "ADDITIVE_NUM")
	private Integer additiveNum;
	
	@Column(name = "FLUID_NUM")
	private Integer fluidNum;

	@Column(name = "CHEST_FLAG")
	private Boolean chestFlag;
	
	@ManyToOne
	@JoinColumn(name = "PHARM_ORDER_ID", nullable = false)
	private PharmOrder pharmOrder;
	
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "MED_ORDER_ITEM_ID")
    private MedOrderItem medOrderItem;
    
	@Transient
	private List<RefillScheduleItem> refillScheduleItemList;

	@Transient
	private List<DispOrderItem> dispOrderItemList;
	
	@Transient
	private DmDrugLite dmDrugLite = null;
	
	@Deprecated
	@IgnoredProperty
	@Transient
	private DmDrug dmDrug = null;// dummy for version 3.2.4 object reference compatibility in xml unmarshalling
	
	@Transient
	private DmFormLite dmFormLite = null;
	
	@Transient
	private Regimen regimen;
	
	@Transient
	private DmWarningLite dmWarningLite1 = null;
	
	@Transient
	private DmWarningLite dmWarningLite2 = null;
	
	@Transient
	private DmWarningLite dmWarningLite3 = null;
	
	@Transient
	private DmWarningLite dmWarningLite4 = null;
	
	@Transient
	private String handleExemptCode;

	@Transient
	private String nextHandleExemptCode;
	
	@Transient
	private String handleExemptUser;
	
	@Transient
	private Boolean stdRefillCheckFlag;
	
	@Transient
	private Boolean dirtyFlag;
		
	@Transient
	private String synonym;
	
	@Transient
	private DispOrderItemStatus dispOrderItemStatus;
	
	@Transient
	private Boolean systemDeducedFlag;

	@Transient
	private Boolean sfiChargeFlag;
	
	@Transient
	private String drugGroup;
	
	@Transient
	private Boolean sfiItemStatusResetFlag;
	
	@Transient
	private BigDecimal orgIssueQty;
	
	@Transient
	private Boolean sfiOverrideWarnFlag;
	
	public PharmOrderItem() {
		itemNum = Integer.valueOf(0);
		capdVoucherFlag = Boolean.FALSE;
		stdRefillCheckFlag = Boolean.FALSE;
		dupChkDrugType = DupChkDrugType.Other;
		dirtyFlag = Boolean.TRUE;
		status = PharmOrderItemStatus.Outstanding;
		actionStatus = ActionStatus.DispByPharm;
		dangerDrugFlag = Boolean.FALSE; 
		wardStockFlag = Boolean.FALSE;
		sfiChargeFlag = Boolean.FALSE;
		chestFlag = Boolean.FALSE;
		sfiItemStatusResetFlag = Boolean.FALSE;
		sfiOverrideWarnFlag = Boolean.FALSE;
	}
	
	@PostLoad
    public void postLoad() {
    	if (StopWatcher.isDebugEnabled()) {
    		StopWatcher.resume();
    	}
		
		if (regimen == null && regimenXml != null) {
			JaxbWrapper<Regimen> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
			regimen = rxJaxbWrapper.unmarshall(regimenXml);
		}
		
		if (chestFlag == null) {
			chestFlag = getPharmOrder().getChestFlag();
		}

    	if (StopWatcher.isDebugEnabled()) {
    		StopWatcher.suspend();
    	}
	}
	
    @PrePersist
    @PreUpdate
    public void preSave() {    	
    	if (regimen != null) {
			JaxbWrapper<Regimen> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
    		regimenXml = rxJaxbWrapper.marshall(regimen);
    	}
    }
	
    public void clearDmInfo() {
    	if (dmDrugLite != null) {
    		dmDrugLite = null;
    	}
    	if (dmFormLite != null) {
    		dmFormLite = null;
    	}
    	if (dmWarningLite1 != null) {
    		dmWarningLite1 = null;
    	}
    	if (dmWarningLite2 != null) {
    		dmWarningLite2 = null;
    	}
    	if (dmWarningLite3 != null) {
    		dmWarningLite3 = null;
    	}
    	if (dmWarningLite4 != null) {
    		dmWarningLite4 = null;
    	}
    	if ( regimen != null ) {
        	regimen.clearDmInfo();
    	}
    }
    
	public void loadDmInfo() {

    	if (dmDrugLite == null && itemCode != null) {
    		dmDrugLite = DmDrugLite.objValueOf(itemCode);
    		if ( orgFormCode == null || "".equals(orgFormCode) ) {
    			//FormCode is under DmForm property
    			orgFormCode = dmDrugLite.getFormCode();
    		}
    	}
    	
    	if (dmFormLite == null && formCode != null) {
    		dmFormLite = DmFormLite.objValueOf(formCode);
    	}
    	
      	if (dmWarningLite1 == null && warnCode1 != null) {
      		dmWarningLite1 = DmWarningLite.objValueOf(warnCode1);
    	}
    	
      	if (dmWarningLite2 == null && warnCode2 != null) {
      		dmWarningLite2 = DmWarningLite.objValueOf(warnCode2);
    	}

    	if (dmWarningLite3 == null && warnCode3 != null) {
    		dmWarningLite3 = DmWarningLite.objValueOf(warnCode3);
    	}
    	
    	if (dmWarningLite4 == null && warnCode4 != null) {
    		dmWarningLite4 = DmWarningLite.objValueOf(warnCode4);
    	}
    	
		if (regimen != null) {
			regimen.loadDmInfo();

			if (orgFormCode != null && formCode != null) {
				
				MedOrder medOrder = null;
				if (getMedOrderItem() != null 
						&& getMedOrderItem().getMedOrder() != null) {
					medOrder = getMedOrderItem().getMedOrder();
				} else {
					PharmOrder pharmOrder = (PharmOrder)Contexts.getSessionContext().get("pharmOrder");
					if (pharmOrder != null) {
						medOrder = pharmOrder.getMedOrder();
					}
				}
				
				if (medOrder != null) {
					if (medOrder.isMpDischarge()) {
						regimen.loadDischargePharmLineList(itemCode, orgFormCode, formCode);
					} else {
						regimen.loadOpPharmLineList(orgFormCode, formCode);
					}
				}
	    	}
		}
	}
	
	public void postLoadAll() {
		postLoad();
		loadDmInfo();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getRegimenXml() {
		return regimenXml;
	}

	public void setRegimenXml(String regimenXml) {
		this.regimenXml = regimenXml;
	}

	public BigDecimal getDoseQty() {
		return doseQty;
	}

	public void setDoseQty(BigDecimal doseQty) {
		this.doseQty = doseQty;
	}

	public String getDoseUnit() {
		return doseUnit;
	}

	public void setDoseUnit(String doseUnit) {
		this.doseUnit = doseUnit;
	}

	public BigDecimal getCalQty() {
		return calQty;
	}

	public void setCalQty(BigDecimal calQty) {
		this.calQty = calQty;
	}

	public BigDecimal getAdjQty() {
		return adjQty;
	}

	public void setAdjQty(BigDecimal adjQty) {
		this.adjQty = adjQty;
	}

	public BigDecimal getIssueQty() {
		return issueQty;
	}

	public void setIssueQty(BigDecimal issueQty) {
		this.issueQty = issueQty;
	}

	public Integer getSfiQty() {
		return sfiQty;
	}

	public void setSfiQty(Integer sfiQty) {
		this.sfiQty = sfiQty;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public PharmOrderItemStatus getStatus() {
		return status;
	}

	public void setStatus(PharmOrderItemStatus status) {
		this.status = status;
	}

	public String getWarnCode1() {
		return warnCode1;
	}

	public void setWarnCode1(String warnCode1) {
		this.warnCode1 = warnCode1;
	}

	public String getWarnCode2() {
		return warnCode2;
	}

	public void setWarnCode2(String warnCode2) {
		this.warnCode2 = warnCode2;
	}

	public String getWarnCode3() {
		return warnCode3;
	}

	public void setWarnCode3(String warnCode3) {
		this.warnCode3 = warnCode3;
	}

	public String getWarnCode4() {
		return warnCode4;
	}

	public void setWarnCode4(String warnCode4) {
		this.warnCode4 = warnCode4;
	}

	public Integer getDrugKey() {
		return drugKey;
	}

	public void setDrugKey(Integer drugKey) {
		this.drugKey = drugKey;
	}
	
	public String getDrugName() {
		return StringUtils.trimToEmpty(drugName);
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public String getTradeName() {
		return tradeName;
	}

	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}
	
	public String getFormCode() {
		return formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	public String getFormLabelDesc() {
		return formLabelDesc;
	}

	public void setFormLabelDesc(String formLabelDesc) {
		this.formLabelDesc = formLabelDesc;
	}

	public String getRouteCode() {
		return routeCode;
	}

	public void setRouteCode(String routeCode) {
		this.routeCode = routeCode;
	}

	public String getRouteDesc() {
		return routeDesc;
	}

	public void setRouteDesc(String routeDesc) {
		this.routeDesc = routeDesc;
	}

	public String getStrength() {
		return strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public String getVolumeText() {
		return volumeText;
	}

	public void setVolumeText(String volumeText) {
		this.volumeText = volumeText;
	}

	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public Integer getOrgItemNum() {
		return orgItemNum;
	}

	public void setOrgItemNum(Integer orgItemNum) {
		this.orgItemNum = orgItemNum;
	}

	public String getConnectSystem() {
		return connectSystem;
	}

	public void setConnectSystem(String connectSystem) {
		this.connectSystem = connectSystem;
	}

	public Boolean getCapdVoucherFlag() {
		return capdVoucherFlag;
	}

	public void setCapdVoucherFlag(Boolean capdVoucherFlag) {
		this.capdVoucherFlag = capdVoucherFlag;
	}

	public Integer getMoIssueQty() {
		return moIssueQty;
	}

	public void setMoIssueQty(Integer moIssueQty) {
		this.moIssueQty = moIssueQty;
	}

	public String getMoBaseUnit() {
		return moBaseUnit;
	}

	public void setMoBaseUnit(String moBaseUnit) {
		this.moBaseUnit = moBaseUnit;
	}

	public String getDupChkDisplayName() {
		return dupChkDisplayName;
	}

	public void setDupChkDisplayName(String dupChkDisplayName) {
		this.dupChkDisplayName = dupChkDisplayName;
	}

	public DupChkDrugType getDupChkDrugType() {
		return dupChkDrugType;
	}

	public void setDupChkDrugType(DupChkDrugType dupChkDrugType) {
		this.dupChkDrugType = dupChkDrugType;
	}

	public PharmOrder getPharmOrder() {
		return pharmOrder;
	}
	
	public void setPharmOrder(PharmOrder pharmOrder) {
		this.pharmOrder = pharmOrder;
	}

	public MedOrderItem getMedOrderItem() {
		if (_medOrderItem != null) {
			return _medOrderItem;
		}
		return medOrderItem;
	}

	public void setMedOrderItem(MedOrderItem medOrderItem) {
		this.medOrderItem = medOrderItem;
	}

	public List<DispOrderItem> getDispOrderItemList() {
		if (dispOrderItemList == null) {
			dispOrderItemList = new ArrayList<DispOrderItem>();
		}
		return dispOrderItemList;
	}

	public void setDispOrderItemList(List<DispOrderItem> dispOrderItemList) {
		this.dispOrderItemList = dispOrderItemList;
	}
	
	public void addDispOrderItem(DispOrderItem dispOrderItem) {
		dispOrderItem.setPharmOrderItem(this);
		getDispOrderItemList().add(dispOrderItem);
	}
	
	public void clearDispOrderItemList() {
		dispOrderItemList = new ArrayList<DispOrderItem>();
	}

	public List<RefillScheduleItem> getRefillScheduleItemList() {
		if (refillScheduleItemList == null) {
			refillScheduleItemList = new ArrayList<RefillScheduleItem>();
		}
		return refillScheduleItemList;
	}

	public void setRefillScheduleItemList(List<RefillScheduleItem> refillScheduleItemList) {
		this.refillScheduleItemList = refillScheduleItemList;
	}
	
	public void addRefillScheduleItem(RefillScheduleItem refillScheduleItem) {
		refillScheduleItem.setPharmOrderItem(this);
		getRefillScheduleItemList().add(refillScheduleItem);
	}
	
	public void clearRefillScheduleItemList() {
		refillScheduleItemList = new ArrayList<RefillScheduleItem>();
	}

	public Regimen getRegimen() {
		return regimen;
	}

	public void setRegimen(Regimen regimen) {
		this.regimen = regimen;
	}

	public void setDmDrugLite(DmDrugLite dmDrugLite) {
		this.dmDrugLite = dmDrugLite;
	}

	public DmDrugLite getDmDrugLite() {
		return dmDrugLite;
	}
	
	public void setDmFormLite(DmFormLite dmFormLite) {
		this.dmFormLite = dmFormLite;
	}

	public DmFormLite getDmFormLite() {
		return dmFormLite;
	}

	@Deprecated
	public void setDmDrug(DmDrug dmDrug) {
		this.dmDrug = dmDrug;
	}

	@Deprecated
	public DmDrug getDmDrug() {
		return dmDrug;
	}
	
	public DmWarningLite getDmWarningLite1() {
		return dmWarningLite1;
	}

	public void setDmWarningLite1(DmWarningLite dmWarningLite1) {
		this.dmWarningLite1 = dmWarningLite1;
	}

	public DmWarningLite getDmWarningLite2() {
		return dmWarningLite2;
	}

	public void setDmWarningLite2(DmWarningLite dmWarningLite2) {
		this.dmWarningLite2 = dmWarningLite2;
	}

	public DmWarningLite getDmWarningLite3() {
		return dmWarningLite3;
	}

	public void setDmWarningLite3(DmWarningLite dmWarningLite3) {
		this.dmWarningLite3 = dmWarningLite3;
	}

	public DmWarningLite getdmWarningLite4() {
		return dmWarningLite4;
	}

	public void setDmWarningLite4(DmWarningLite dmWarningLite4) {
		this.dmWarningLite4 = dmWarningLite4;
	}

	public String getHandleExemptCode() {
		return handleExemptCode;
	}

	public void setHandleExemptCode(String handleExemptCode) {
		this.handleExemptCode = handleExemptCode;
	}

	public String getNextHandleExemptCode() {
		return nextHandleExemptCode;
	}

	public void setNextHandleExemptCode(String nextHandleExemptCode) {
		this.nextHandleExemptCode = nextHandleExemptCode;
	}
	
	public void setDirtyFlag(Boolean dirtyFlag) {
		this.dirtyFlag = dirtyFlag;
	}

	public Boolean isDirtyFlag() {
		return dirtyFlag;
	}

	public void setStdRefillCheckFlag(Boolean stdRefillCheckFlag) {
		this.stdRefillCheckFlag = stdRefillCheckFlag;
	}

	public Boolean isStdRefillCheckFlag() {
		return stdRefillCheckFlag;
	}
	
	@Transient
	private transient MedOrderItem _medOrderItem;
	
	public void disconnectMedOrderItem() {
		_medOrderItem = this.getMedOrderItem();
		this.setMedOrderItem(null);
	}

	public void reconnectMedOrderItem() {
		this.setMedOrderItem(_medOrderItem);
		_medOrderItem = null;
	}
	
	@Transient
	private transient List<RefillScheduleItem> _refillScheduleItemList;
	
	public void disconnectRefillScheduleItemList() {
		_refillScheduleItemList = this.getRefillScheduleItemList();
		this.clearRefillScheduleItemList();
	}

	public void reconnectRefillScheduleItemList() {
		this.setRefillScheduleItemList(_refillScheduleItemList);
		_refillScheduleItemList = null;
	}
	
	public String getFullDrugDesc() {
		return DmDrug.buildFullDrugDesc(StringUtils.trimToEmpty(drugName), formCode, formLabelDesc, strength, volumeText);
	}

	public void setActionStatus(ActionStatus actionStatus) {
		this.actionStatus = actionStatus;
	}

	public ActionStatus getActionStatus() {
		return actionStatus;
	}

	public String getFmStatus() {
		return fmStatus;
	}

	public void setFmStatus(String fmStatus) {
		this.fmStatus = fmStatus;
	}

	public Boolean getDangerDrugFlag() {
		return dangerDrugFlag;
	}

	public void setDangerDrugFlag(Boolean dangerDrugFlag) {
		this.dangerDrugFlag = dangerDrugFlag;
	}

	public void setSynonym(String synonym) {
		this.synonym = synonym;
	}

	public String getSynonym() {
		return synonym;
	}

	public DispOrderItemStatus getDispOrderItemStatus() {
		return dispOrderItemStatus;
	}

	public void setDispOrderItemStatus(DispOrderItemStatus dispOrderItemStatus) {
		this.dispOrderItemStatus = dispOrderItemStatus;
	}

	public String getOrgFormCode() {
		return orgFormCode;
	}

	public void setOrgFormCode(String orgFormCode) {
		this.orgFormCode = orgFormCode;
	}

	public void setAdditiveNum(Integer additiveNum) {
		this.additiveNum = additiveNum;
	}

	public Integer getAdditiveNum() {
		return additiveNum;
	}

	public void setFluidNum(Integer fluidNum) {
		this.fluidNum = fluidNum;
	}

	public Integer getFluidNum() {
		return fluidNum;
	}

	public Boolean getChestFlag() {
		return chestFlag;
	}

	public void setChestFlag(Boolean chestFlag) {
		this.chestFlag = chestFlag;
	}

	public Boolean getWardStockFlag() {
		return wardStockFlag;
	}

	public void setWardStockFlag(Boolean wardStockFlag) {
		this.wardStockFlag = wardStockFlag;
	}

	public String getRemarkText() {
		return remarkText;
	}

	public void setRemarkText(String remarkText) {
		this.remarkText = remarkText;
	}

	public void setHandleExemptUser(String handleExemptUser) {
		this.handleExemptUser = handleExemptUser;
	}

	public String getHandleExemptUser() {
		return handleExemptUser;
	}

	public void setSystemDeducedFlag(Boolean systemDeducedFlag) {
		this.systemDeducedFlag = systemDeducedFlag;
	}

	public Boolean getSystemDeducedFlag() {
		return systemDeducedFlag;
	}
	
	public boolean isSfi() {
		return (
			(ActionStatus.PurchaseByPatient == getActionStatus() || ActionStatus.SafetyNet == getActionStatus()) &&
			(getMedOrderItem().getCapdRxDrug() == null) &&
			(!FmStatus.FreeTextEntryItem.getDataValue().equals(getFmStatus()))
		);
	}

	public void setSfiChargeFlag(Boolean sfiChargeFlag) {
		this.sfiChargeFlag = sfiChargeFlag;
	}

	public Boolean getSfiChargeFlag() {
		return sfiChargeFlag;
	}

	public String getDrugGroup() {
		return drugGroup;
	}

	public void setDrugGroup(String drugGroup) {
		this.drugGroup = drugGroup;
	}

	public Boolean getSfiItemStatusResetFlag() {
		return sfiItemStatusResetFlag;
	}

	public void setSfiItemStatusResetFlag(Boolean sfiItemStatusResetFlag) {
		this.sfiItemStatusResetFlag = sfiItemStatusResetFlag;
	}

	public BigDecimal getOrgIssueQty() {
		return orgIssueQty;
	}

	public void setOrgIssueQty(BigDecimal orgIssueQty) {
		this.orgIssueQty = orgIssueQty;
	}

	public Boolean getSfiOverrideWarnFlag() {
		return sfiOverrideWarnFlag;
	}

	public void setSfiOverrideWarnFlag(Boolean sfiOverrideWarnFlag) {
		this.sfiOverrideWarnFlag = sfiOverrideWarnFlag;
	}
}
