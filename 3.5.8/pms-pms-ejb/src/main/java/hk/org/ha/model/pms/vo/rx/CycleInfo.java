package hk.org.ha.model.pms.vo.rx;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type = DefaultExternalizer.class)
public class CycleInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement
	private Integer cycle;
	
	@XmlElement
	private List<Integer> treatmentDays;

	public Integer getCycle() {
		return cycle;
	}

	public void setCycle(Integer cycle) {
		this.cycle = cycle;
	}

	public List<Integer> getTreatmentDays() {
		return treatmentDays;
	}

	public void setTreatmentDays(List<Integer> treatmentDays) {
		this.treatmentDays = treatmentDays;
	}	
}
