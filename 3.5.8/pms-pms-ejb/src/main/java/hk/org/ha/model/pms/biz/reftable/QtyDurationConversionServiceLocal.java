package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.dms.persistence.PmsDurationConversion;
import hk.org.ha.model.pms.dms.persistence.PmsPcuMapping;
import hk.org.ha.model.pms.dms.persistence.PmsQtyConversion;
import hk.org.ha.model.pms.vo.reftable.QtyDurationConversionInfo;

import java.util.List;

import javax.ejb.Local;
 
@Local
public interface QtyDurationConversionServiceLocal {
	
	QtyDurationConversionInfo retrieveQtyDurationConversionList(String itemCode, PmsPcuMapping pmsPcuMapping);
	
	String updateQtyDurationConversionList(List<PmsQtyConversion> updatePmsQtyConversionList, boolean qtyApplyAll, 
											List<PmsDurationConversion> updatePmsDurationConversionList, boolean durationApplyAll);
	
	void destroy();
	
} 
 