package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.cache.PasSpecialtyCacherInf;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.PmsRestrictItem;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.patient.PasSpecialty;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("pmsRestrictItemSpecBySpecListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PmsRestrictItemSpecBySpecListServiceBean implements PmsRestrictItemSpecBySpecListServiceLocal {

	private List<PasSpecialty> pasSpecialtyDisplayList;

	@Out(required = false)
	private List<PmsRestrictItem> pmsRestrictItemListBySpecialty;

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private DmDrugCacherInf dmDrugCacher;

	@In
	private Workstore workstore;

	@In
	private PasSpecialtyCacherInf pasSpecialtyCacher;

	public List<PasSpecialty> retrievePasSpecDisplayList() {
		pasSpecialtyDisplayList = pasSpecialtyCacher.getPasSpecialtyWithSubSpecialtyList();
		
		return pasSpecialtyDisplayList;
	}
	
	public void retrievePmsRestrictItemListBySpecialty(String specType, String specCode, String subSpecCode)
	{		
		pmsRestrictItemListBySpecialty = dmsPmsServiceProxy.retrievePmsRestrictItemListBySpecialty(workstore.getWorkstoreGroup().getWorkstoreGroupCode(), 
																										specType, 
																										specCode, 
																										subSpecCode);
		
		for(PmsRestrictItem pmsRestrictItem:pmsRestrictItemListBySpecialty){
			pmsRestrictItem.setDmDrug( dmDrugCacher.getDmDrug(pmsRestrictItem.getItemCode()) );
		}
	}

	@Remove
	public void destroy() 
	{
		
		if (pasSpecialtyDisplayList != null) {
			pasSpecialtyDisplayList = null;
		}

		if (pmsRestrictItemListBySpecialty != null) {
			pmsRestrictItemListBySpecialty = null;
		}
	}

}
