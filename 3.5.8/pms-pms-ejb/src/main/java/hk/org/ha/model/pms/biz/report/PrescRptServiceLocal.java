package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.vo.report.PrescRpt;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface PrescRptServiceLocal {
	
	List<PrescRpt> retrievePrescRptList(Date rptDate, String ticketNum, String caseNum, String hkid, String itemCode, String nameTxt);
	
	void generatePrescRpt();

	void printPrescRpt();
	
	void destroy();
	
}
 