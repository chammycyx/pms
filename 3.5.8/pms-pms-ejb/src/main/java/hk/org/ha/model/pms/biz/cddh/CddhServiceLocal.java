package hk.org.ha.model.pms.biz.cddh;

import hk.org.ha.model.pms.vo.cddh.CddhCriteria;
import hk.org.ha.model.pms.vo.report.CddhPatDrugProfileRpt;

import java.util.List;

import javax.ejb.Local;

@Local
public interface CddhServiceLocal {
	void retrieveDispOrderItem(CddhCriteria cddhCriteria);
	void createCddhCriteria();
	void retrieveDispOrderLikePatName(CddhCriteria cddhCriteria);
	String getErrMsg();
	void setErrMsg(String errMsg);
	void printCddhPatDrugProfileRpt(CddhPatDrugProfileRpt cddhPatDrugProfileRpt);
	void retrieveLegacyDispLabel(List<String> dispOrderItemKey);
	void destroy();
}
