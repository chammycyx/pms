package hk.org.ha.model.pms.vo.patient;

import java.io.Serializable;

public class PasWard implements Serializable {

	private static final long serialVersionUID = 1L;

	private String pasWardCode;
	
	private String pasWardDesc;

	private String pasHospCode;
	
	public void setPasWardCode(String pasWardCode) {
		this.pasWardCode = pasWardCode;
	}

	public String getPasWardCode() {
		return pasWardCode;
	}

	public void setPasWardDesc(String pasWardDesc) {
		this.pasWardDesc = pasWardDesc;
	}

	public String getPasWardDesc() {
		return pasWardDesc;
	}

	public String getPasHospCode() {
		return pasHospCode;
	}

	public void setPasHospCode(String pasHospCode) {
		this.pasHospCode = pasHospCode;
	}
}
