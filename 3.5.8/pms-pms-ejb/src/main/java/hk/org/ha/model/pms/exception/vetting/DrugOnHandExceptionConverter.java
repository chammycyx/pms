package hk.org.ha.model.pms.exception.vetting;

import hk.org.ha.model.pms.asa.exception.moe.DrugOnHandException;

import java.util.Map;

import org.granite.messaging.service.ExceptionConverter;
import org.granite.messaging.service.ServiceException;

public class DrugOnHandExceptionConverter implements ExceptionConverter {

	public static final String DRUG_ON_HAND_EXCEPTION = "DrugOnHandException";
	
	@Override
	public boolean accepts(Throwable t, Throwable finalException) {
		return t.getClass().equals(DrugOnHandException.class);
	}

	@Override
	public ServiceException convert(Throwable t, String detail, Map<String, Object> extendedData) {
		ServiceException se; 		
		se = new ServiceException(DRUG_ON_HAND_EXCEPTION, t.getMessage(), detail, t);
		se.getExtendedData().putAll(extendedData);
		return se;
	}
}
