package hk.org.ha.model.pms.vo.report;

import hk.org.ha.model.pms.persistence.reftable.WardGroup;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class PatientListRptInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private List<String> wardCodeList;
	
	private List<WardGroup> wardGroupList;
	
	private List<String> mpWardCodeList;

	public List<String> getWardCodeList() {
		return wardCodeList;
	}

	public void setWardCodeList(List<String> wardCodeList) {
		this.wardCodeList = wardCodeList;
	}

	public List<WardGroup> getWardGroupList() {
		return wardGroupList;
	}

	public void setWardGroupList(List<WardGroup> wardGroupList) {
		this.wardGroupList = wardGroupList;
	}

	public void setMpWardCodeList(List<String> mpWardCodeList) {
		this.mpWardCodeList = mpWardCodeList;
	}

	public List<String> getMpWardCodeList() {
		return mpWardCodeList;
	}
}
