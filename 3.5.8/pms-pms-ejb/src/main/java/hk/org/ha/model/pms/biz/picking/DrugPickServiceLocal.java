package hk.org.ha.model.pms.biz.picking;

import hk.org.ha.model.pms.vo.picking.PickItemListSummary;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface DrugPickServiceLocal {
	
	void checkUserCode(String userCode);

	PickItemListSummary updateDispOrderItemStatusToPicked(Date ticketDate, String ticketNum, Integer itemNum, String userCode, Long workstationId);
	
	void reprintDispLabel(List<Long> dispOrderItemId, String printLang);
	
	PickItemListSummary retrievePickingList(Long workstationId);
	
	boolean isUpdateSuccess();
	
	String getResultMsg();
	
	void destroy();
	
}