package hk.org.ha.model.pms.persistence.reftable;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.fmk.pms.util.StringCollectionConverter;
import hk.org.ha.model.pms.corp.cache.DmDailyFrequencyCacher;
import hk.org.ha.model.pms.dms.persistence.DmDailyFrequency;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.udt.reftable.mp.AomScheduleType;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;
import org.jboss.seam.security.Identity;

@Entity
@Table(name = "AOM_SCHEDULE")
@Customizer(AuditCustomizer.class)
public class AomSchedule extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "aomScheduleSeq")
	@SequenceGenerator(name = "aomScheduleSeq", sequenceName = "SQ_AOM_SCHEDULE", initialValue = 100000000)
	private Long id;
	
	@Column(name = "DAILY_FREQ_CODE", nullable = false, length = 7)
	private String dailyFreqCode;
	
	@Converter(name = "AomSchedule.adminTime", converterClass = StringCollectionConverter.class)
	@Convert("AomSchedule.adminTime")
	@Column(name = "ADMIN_TIME", length = 1000)
	private List<String> adminTime;

	@ManyToOne
	@JoinColumn(name = "HOSP_CODE", nullable = false)
	private Hospital hospital;
	
	@Converter(name = "AomSchedule.type", converterClass = AomScheduleType.Converter.class)
    @Convert("AomSchedule.type")
	@Column(name = "TYPE", nullable = false, length = 1)
	private AomScheduleType type;
	
	@Transient
	private DmDailyFrequency dmDailyFrequency;
	
	@PostLoad
	public void postLoad() {
		if (dailyFreqCode != null && Identity.instance().isLoggedIn()) {
			setDmDailyFrequency(DmDailyFrequencyCacher.instance().getDmDailyFrequencyByDailyFreqCode(dailyFreqCode));
		}
	}	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDailyFreqCode() {
		return dailyFreqCode;
	}

	public void setDailyFreqCode(String dailyFreqCode) {
		this.dailyFreqCode = dailyFreqCode;
	}

	public List<String> getAdminTime() {
		return adminTime;
	}

	public void setAdminTime(List<String> adminTime) {
		this.adminTime = adminTime;
	}

	public Hospital getHospital() {
		return hospital;
	}

	public void setHospital(Hospital hospital) {
		this.hospital = hospital;
	}

	public AomScheduleType getType() {
		return type;
	}

	public void setType(AomScheduleType type) {
		this.type = type;
	}

	public void setDmDailyFrequency(DmDailyFrequency dmDailyFrequency) {
		this.dmDailyFrequency = dmDailyFrequency;
	}

	public DmDailyFrequency getDmDailyFrequency() {
		return dmDailyFrequency;
	}	
}
