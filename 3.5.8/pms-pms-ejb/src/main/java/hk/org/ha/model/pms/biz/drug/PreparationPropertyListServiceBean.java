package hk.org.ha.model.pms.biz.drug;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.vo.PreparationProperty;
import hk.org.ha.model.pms.dms.vo.PreparationSearchCriteria;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("preparationPropertyListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PreparationPropertyListServiceBean implements PreparationPropertyListServiceLocal {

	@Logger
	private Log logger;
	
	@Out(required = false)
	private List<PreparationProperty> preparationPropertyList;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
		
	public void retrievePreparationPropertyList(PreparationSearchCriteria criteria) {
		preparationPropertyList = dmsPmsServiceProxy.retrievePreparation( criteria );
		if ( preparationPropertyList != null ) {
			logger.debug("preparationPropertyList size #0", preparationPropertyList.size());			
		}
	}	
	
	@Remove
	public void destroy() {
		if ( preparationPropertyList != null ) {			
			preparationPropertyList = null;
		}
		
	}
}
