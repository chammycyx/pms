package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.security.Uam;
import hk.org.ha.model.pms.persistence.corp.HospitalClusterTask;

import java.util.Date;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@AutoCreate
@Stateless
@Name("hospitalClusterTaskManager")
@Scope(ScopeType.STATELESS)
public class HospitalClusterTaskManagerBean implements HospitalClusterTaskManagerLocal {

	private static final long GRACE_TIME_OFFSET = 500L;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private Uam uam;
			
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean lockHospitalClusterTask(String taskName, Date date) {				
		return lockHospitalClusterTask(taskName, date, 0L);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean lockHospitalClusterTask(String taskName, Date date, long graceTime) {
				
		HospitalClusterTask hospitalClusterTask = (HospitalClusterTask) em.createQuery(
				"select o from HospitalClusterTask o" + // 20120303 index check : HospitalClusterTask.clusterCode,taskName : PK_HOSPITAL_CLUSTER_TASK
				" where o.clusterCode = :clusterCode" +
				" and o.taskName = :taskName" +
				" and o.taskName is not null") // don't delete : magic code fix bug for https://bugs.eclipse.org/bugs/show_bug.cgi?id=347592
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
				.setParameter("clusterCode", uam.getClusterCode())
				.setParameter("taskName", taskName)
				.getSingleResult();
		
		long tmpGraceTime = graceTime;
		if (tmpGraceTime > GRACE_TIME_OFFSET) {
			tmpGraceTime -= GRACE_TIME_OFFSET;
		}
		
		if (hospitalClusterTask.getTaskDate() == null || 
				date.after(new Date(hospitalClusterTask.getTaskDate().getTime() + tmpGraceTime))) {
			hospitalClusterTask.setTaskDate(date);
			return true;
		}
		return false;
	}
	
	
}
