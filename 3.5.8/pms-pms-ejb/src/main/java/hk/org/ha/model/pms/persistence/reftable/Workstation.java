package hk.org.ha.model.pms.persistence.reftable;

import flexjson.JSON;
import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.reftable.PrintType;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Customizer;
import org.eclipse.persistence.annotations.PrivateOwned;

@Entity
@Table(name = "WORKSTATION")
@Customizer(AuditCustomizer.class)
public class Workstation extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "workstationSeq")
	/*lower the inital value to avoid the auto delete by using clean_db() */
	@SequenceGenerator(name = "workstationSeq", sequenceName = "SQ_WORKSTATION", initialValue = 1000000)
	private Long id;

	@Column(name = "WORKSTATION_CODE", nullable = false, length = 4)
	private String workstationCode;
	
	@Column(name = "HOST_NAME", nullable = false, length = 15)
	private String hostName;
	
	@Column(name = "COMMON_ACCOUNT_FLAG", nullable = false)
	private Boolean commonAccountFlag;
	
	@Column(name = "CHEMO_FLAG")
	private Boolean chemoFlag;
	
	@Column(name = "ADMIN_LEVEL", nullable = false)
	private Integer adminLevel;

	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE", referencedColumnName = "HOSP_CODE", nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;

    @PrivateOwned
    @OneToMany(mappedBy="workstation", cascade=CascadeType.ALL)
    private List<WorkstationPrint> workstationPrintList;

    @Transient
	private WorkstationPrint otherLabelPrint;
    
	@Transient
	private WorkstationPrint reportPrint;

	@Transient
	private WorkstationPrint sfiInvoicePrint;

	@Transient
	private WorkstationPrint sfiRefillCouponPrint;

	@Transient
	private WorkstationPrint refillCouponPrint;

	@Transient
	private WorkstationPrint capdVoucherPrint;
	
	@Transient
	private WorkstationPrint pivasLargeLabelPrint;
	
	@Transient
	private WorkstationPrint pivasColorLabelPrint;
	
    public Workstation() {
    	commonAccountFlag = Boolean.FALSE;
    	chemoFlag = Boolean.FALSE;
    	adminLevel = Integer.valueOf(0);
    }
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWorkstationCode() {
		return workstationCode;
	}

	public void setWorkstationCode(String workstationCode) {
		this.workstationCode = workstationCode;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public Boolean getCommonAccountFlag() {
		return commonAccountFlag;
	}

	public void setCommonAccountFlag(Boolean commonAccountFlag) {
		this.commonAccountFlag = commonAccountFlag;
	}

	public Boolean getChemoFlag() {
		return chemoFlag;
	}

	public void setChemoFlag(Boolean chemoFlag) {
		this.chemoFlag = chemoFlag;
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}
	
	@JSON
	public List<WorkstationPrint> getWorkstationPrintList() {
		if (workstationPrintList == null) {
			workstationPrintList = new ArrayList<WorkstationPrint>();
		}
		return workstationPrintList;
	}
	
	public void setWorkstationPrintList(List<WorkstationPrint> workstationPrintList) {
		this.workstationPrintList = workstationPrintList;
	}
	
	public void addWorkstationPrint(WorkstationPrint workstationPrint) {
		workstationPrint.setWorkstation(this);
		getWorkstationPrintList().add(workstationPrint);
	}
	
	public void clearWorkstationPrintList() {
		workstationPrintList = new ArrayList<WorkstationPrint>();
	}

	public WorkstationPrint getOtherLabelPrint() {
		return otherLabelPrint;
	}

	public void setOtherLabelPrint(WorkstationPrint otherLabelPrint) {
		this.otherLabelPrint = otherLabelPrint;
	}

	public WorkstationPrint getReportPrint() {
		return reportPrint;
	}

	public void setReportPrint(WorkstationPrint reportPrint) {
		this.reportPrint = reportPrint;
	}

	public WorkstationPrint getSfiRefillCouponPrint() {
		return sfiRefillCouponPrint;
	}

	public void setSfiRefillCouponPrint(WorkstationPrint sfiRefillCouponPrint) {
		this.sfiRefillCouponPrint = sfiRefillCouponPrint;
	}

	public WorkstationPrint getSfiInvoicePrint() {
		return sfiInvoicePrint;
	}

	public void setSfiInvoicePrint(WorkstationPrint sfiInvoicePrint) {
		this.sfiInvoicePrint = sfiInvoicePrint;
	}

	public WorkstationPrint getRefillCouponPrint() {
		return refillCouponPrint;
	}

	public void setRefillCouponPrint(WorkstationPrint refillCouponPrint) {
		this.refillCouponPrint = refillCouponPrint;
	}

	public WorkstationPrint getCapdVoucherPrint() {
		return capdVoucherPrint;
	}

	public void setCapdVoucherPrint(WorkstationPrint capdVoucherPrint) {
		this.capdVoucherPrint = capdVoucherPrint;
	}

	public WorkstationPrint getPivasLargeLabelPrint() {
		return pivasLargeLabelPrint;
	}

	public void setPivasLargeLabelPrint(WorkstationPrint pivasLargeLabelPrint) {
		this.pivasLargeLabelPrint = pivasLargeLabelPrint;
	}

	public WorkstationPrint getPivasColorLabelPrint() {
		return pivasColorLabelPrint;
	}

	public void setPivasColorLabelPrint(WorkstationPrint pivasColorLabelPrint) {
		this.pivasColorLabelPrint = pivasColorLabelPrint;
	}
	
	public void loadPrintInfo() {
		for (WorkstationPrint workstationPrint : this.getWorkstationPrintList()) {
			workstationPrint.getWorkstation();
		}
		otherLabelPrint = findWorkstationPrint(PrintDocType.OtherLabel);
		reportPrint = findWorkstationPrint(PrintDocType.Report);
		sfiInvoicePrint = findWorkstationPrint(PrintDocType.SfiInvoice);
		sfiRefillCouponPrint = findWorkstationPrint(PrintDocType.SfiRefillCoupon);
		refillCouponPrint = findWorkstationPrint(PrintDocType.RefillCoupon);
		capdVoucherPrint = findWorkstationPrint(PrintDocType.CapdVoucher);
		pivasLargeLabelPrint = findWorkstationPrint(PrintDocType.PivasLargeLabel);
		pivasColorLabelPrint = findWorkstationPrint(PrintDocType.PivasColorLabel);
	}
	
	public WorkstationPrint findWorkstationPrint(PrintDocType docType) {
		for (WorkstationPrint workstationPrint : this.getWorkstationPrintList()) {
			if (workstationPrint.getDocType() == docType) {
				return workstationPrint;
			}
		}
		WorkstationPrint ret = new WorkstationPrint(docType, PrintType.None);
		ret.setWorkstation(this);
		return ret;
	}

	public void updatePrintInfo() {
		List<WorkstationPrint> workstationPrintList = buildPrintInfo();
		this.getWorkstationPrintList().clear();
		for (WorkstationPrint workstationPrint : workstationPrintList) {
			this.addWorkstationPrint(workstationPrint);
		}
	}
	
	public List<WorkstationPrint> buildPrintInfo() {
		List<WorkstationPrint> workstationPrintList = new ArrayList<WorkstationPrint>();
		
		this.checkThanAddWorkstationPrint(workstationPrintList, otherLabelPrint);
		this.checkThanAddWorkstationPrint(workstationPrintList, reportPrint);
		this.checkThanAddWorkstationPrint(workstationPrintList, sfiRefillCouponPrint);
		this.checkThanAddWorkstationPrint(workstationPrintList, sfiInvoicePrint);
		this.checkThanAddWorkstationPrint(workstationPrintList, refillCouponPrint);
		this.checkThanAddWorkstationPrint(workstationPrintList, capdVoucherPrint);
		this.checkThanAddWorkstationPrint(workstationPrintList, pivasLargeLabelPrint);
		this.checkThanAddWorkstationPrint(workstationPrintList, pivasColorLabelPrint);
		
		return workstationPrintList;
	}
		
	private void checkThanAddWorkstationPrint(List<WorkstationPrint> workstationPrintList, 
			WorkstationPrint workstationPrint) {
		if (workstationPrint != null && workstationPrint.getType() != PrintType.None) {
			workstationPrintList.add(workstationPrint);
		}
	}

	public void setAdminLevel(Integer adminLevel) {
		this.adminLevel = adminLevel;
	}

	public Integer getAdminLevel() {
		return adminLevel;
	}
}
