package hk.org.ha.model.pms.biz.checkissue;

import javax.ejb.Local;

@Local
public interface BatchIssueListServiceLocal {
	
	void retrieveBatchIssueLogList();
	
	void destroy();
	
}
