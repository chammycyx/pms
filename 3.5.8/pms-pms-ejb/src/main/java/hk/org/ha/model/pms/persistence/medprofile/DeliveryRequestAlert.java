package hk.org.ha.model.pms.persistence.medprofile;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.alert.ehr.EhrAlertStatus;
import hk.org.ha.model.pms.vo.alert.AlertProfile;
import hk.org.ha.model.pms.vo.alert.mds.MedProfileCheckResult;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "DELIVERY_REQUEST_ALERT")
public class DeliveryRequestAlert extends VersionEntity {
	
	private static final long serialVersionUID = 1L;

	private static final String JAXB_CONTEXT_ALERT = "hk.org.ha.model.pms.vo.alert";
	private static final String JAXB_CONTEXT_MDS_ALERT = "hk.org.ha.model.pms.vo.alert.mds";
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "deliveryRequestAlertSeq")
	@SequenceGenerator(name = "deliveryRequestAlertSeq", sequenceName = "SQ_DELIVERY_REQUEST_ALERT", initialValue = 100000000)
	private Long id;
	
	@Column(name = "CASE_NUM", length = 12)
	private String caseNum;
	
	@Column(name = "MED_PROFILE_ID")
	private Long medProfileId;
	
	@Lob
	@Column(name = "ALERT_PROFILE_XML")
	private String alertProfileXml;

	@Lob
	@Column(name = "MDS_CHECK_RESULT_XML")
	private String mdsCheckResultXml;
	
    @Column(name = "PREGNANCY_CHECK_FLAG", nullable = false)
    private Boolean pregnancyCheckFlag;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DELIVERY_REQUEST_ID", nullable = false)
	private DeliveryRequest deliveryRequest;
	
	@Column(name = "ALERT_PROFILE_FLAG", nullable = false)
	private Boolean alertProfileFlag;
	
	@Column(name = "EHR_ALERT_PROFILE_FLAG")
	private Boolean ehrAlertProfileFlag;
	
	@Column(name = "MDS_CHECK_FLAG", nullable = false)
	private Boolean mdsCheckFlag;
    
    @Column(name = "PRIVATE_FLAG", nullable = false)
    private Boolean privateFlag;
	
	@Column(name = "DDI_CHECK_FLAG")
	private Boolean ddiCheckFlag;
    
	@Transient
	private AlertProfile alertProfile;
	
	@Transient
	private MedProfileCheckResult mdsCheckResult;
	
	public DeliveryRequestAlert() {
		pregnancyCheckFlag = Boolean.FALSE;
		ddiCheckFlag = Boolean.TRUE;
	}
	
	public DeliveryRequestAlert(MedProfile medProfile) {
		this(medProfile, Boolean.TRUE);
	}
	
	public DeliveryRequestAlert(MedProfile medProfile, Boolean mdsCheckFlag) {
		this.caseNum = medProfile.getMedCase().getCaseNum();
		this.medProfileId = medProfile.getId();
		this.pregnancyCheckFlag = medProfile.getPregnancyCheckFlag();
		this.alertProfile = medProfile.getAlertProfile();
		this.alertProfileFlag = alertProfile != null && !alertProfile.isEhrAllergyOnly();
		this.ehrAlertProfileFlag = alertProfile != null && alertProfile.getEhrAllergySummary() != null && alertProfile.getEhrAllergySummary().getStatus() != EhrAlertStatus.Error;
		this.mdsCheckFlag = mdsCheckFlag;
		this.privateFlag = medProfile.getPrivateFlag();
		this.ddiCheckFlag = Boolean.TRUE;
	}
	

	@PostLoad
	public void postLoad() {
		if (alertProfileXml != null) {
			JaxbWrapper<AlertProfile> alertJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_ALERT);
			alertProfile = (AlertProfile) alertJaxbWrapper.unmarshall(alertProfileXml);
		}
		if (mdsCheckResultXml != null) {
			JaxbWrapper<MedProfileCheckResult> mdsAlertJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_MDS_ALERT);
			mdsCheckResult = mdsAlertJaxbWrapper.unmarshall(mdsCheckResultXml);
		}
		
	}

	@PrePersist
    @PreUpdate
    public void preSave() {
		if (alertProfile != null) {
			JaxbWrapper<AlertProfile> alertJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_ALERT);
			alertProfileXml = alertJaxbWrapper.marshall(alertProfile);
		}
		if (mdsCheckResult != null) {
			JaxbWrapper<MedProfileCheckResult> mdsAlertJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_MDS_ALERT);
			mdsCheckResultXml = mdsAlertJaxbWrapper.marshall(mdsCheckResult);
		}
		
    }	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}
	
	public Long getMedProfileId() {
		return medProfileId;
	}

	public void setMedProfileId(Long medProfileId) {
		this.medProfileId = medProfileId;
	}

	public String getAlertProfileXml() {
		return alertProfileXml;
	}

	public void setAlertProfileXml(String alertProfileXml) {
		this.alertProfileXml = alertProfileXml;
	}

	public String getMdsCheckResultXml() {
		return mdsCheckResultXml;
	}

	public void setMdsCheckResultXml(String mdsCheckResultXml) {
		this.mdsCheckResultXml = mdsCheckResultXml;
	}
	
	public Boolean getPregnancyCheckFlag() {
		return pregnancyCheckFlag;
	}

	public void setPregnancyCheckFlag(Boolean pregnancyCheckFlag) {
		this.pregnancyCheckFlag = pregnancyCheckFlag;
	}

	public DeliveryRequest getDeliveryRequest() {
		return deliveryRequest;
	}

	public void setDeliveryRequest(DeliveryRequest deliveryRequest) {
		this.deliveryRequest = deliveryRequest;
	}

	public AlertProfile getAlertProfile() {
		return alertProfile;
	}

	public void setAlertProfile(AlertProfile alertProfile) {
		this.alertProfile = alertProfile;
	}

	public MedProfileCheckResult getMdsCheckResult() {
		return mdsCheckResult;
	}

	public void setMdsCheckResult(MedProfileCheckResult mdsCheckResult) {
		this.mdsCheckResult = mdsCheckResult;
	}

	public Boolean getAlertProfileFlag() {
		return alertProfileFlag;
	}

	public void setAlertProfileFlag(Boolean alertProfileFlag) {
		this.alertProfileFlag = alertProfileFlag;
	}

	public Boolean getEhrAlertProfileFlag() {
		return ehrAlertProfileFlag;
	}

	public void setEhrAlertProfileFlag(Boolean ehrAlertProfileFlag) {
		this.ehrAlertProfileFlag = ehrAlertProfileFlag;
	}

	public Boolean getMdsCheckFlag() {
		return mdsCheckFlag;
	}

	public void setMdsCheckFlag(Boolean mdsCheckFlag) {
		this.mdsCheckFlag = mdsCheckFlag;
	}

	public void setPrivateFlag(Boolean privateFlag) {
		this.privateFlag = privateFlag;
	}

	public Boolean getPrivateFlag() {
		return privateFlag;
	}

	public void setDdiCheckFlag(Boolean ddiCheckFlag) {
		this.ddiCheckFlag = ddiCheckFlag;
	}
	
	public Boolean getDdiCheckFlag() {
		return ddiCheckFlag;
	}

}
