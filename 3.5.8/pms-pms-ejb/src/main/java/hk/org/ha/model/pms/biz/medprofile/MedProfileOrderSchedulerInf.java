package hk.org.ha.model.pms.biz.medprofile;

public interface MedProfileOrderSchedulerInf {

	boolean scheduleMedProfileOrder(String orderNum);
	
	boolean shouldStartRunning();
	
	void stopRunning();
	
	String getNextOrderNumToRun();
}
