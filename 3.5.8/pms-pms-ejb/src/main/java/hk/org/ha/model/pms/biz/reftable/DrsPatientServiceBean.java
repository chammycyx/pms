package hk.org.ha.model.pms.biz.reftable;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.UnreachableException;
import hk.org.ha.model.pms.asa.exception.pas.PasException;
import hk.org.ha.model.pms.corp.biz.order.OrderHelper;
import hk.org.ha.model.pms.pas.PasServiceWrapper;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.reftable.DrsPatient;
import hk.org.ha.model.pms.vo.security.PropMap;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("drsPatientService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DrsPatientServiceBean implements DrsPatientServiceLocal {
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private Workstore workstore;
	
	@In
	private PasServiceWrapper pasServiceWrapper;
	
	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
	
	@In
	private OrderHelper orderHelper;
	
	@In
	private PropMap propMap;
		
	@SuppressWarnings("unchecked")
	public DrsPatient retrieveDrsPatient(String hkidTxt) {
		List<DrsPatient> drsPatientList = em.createQuery(
				"select o from DrsPatient o" + // 20171023 index check : DrsPatient.hospital,patient : UI_DRS_PATIENT_01
				" where o.hospital = :hospital" +
				" and o.patient.hkid = :hkidTxt")
				.setParameter("hospital", workstore.getHospital())
				.setParameter("hkidTxt", hkidTxt)
				.getResultList();
		
		if(!drsPatientList.isEmpty()){
			Patient updatedPatient = retrievePasAndSavePatient(hkidTxt);
			
			if( updatedPatient != null ){
				drsPatientList.get(0).setPatient(updatedPatient);
			}
			drsPatientList.get(0).getDrsPatientContactList().size();
			return drsPatientList.get(0);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	private Patient retrievePasAndSavePatient(String hkidTxt) {
		Patient pasPatient = null;
		
		if( !StringUtils.equals(propMap.getValue("patient.pas.patient.enabled"),"Y") ){
			return null;
		}
		
		try{
			pasPatient = pasServiceWrapper.retrievePasPatientByHkid(hkidTxt, "", "", false);
		}catch (PasException e) {
			logger.info("DRS Patient Maintenance - No patient is found in HKPMI");
		}catch (UnreachableException e) {
			logger.info("DRS Patient Maintenance - Hkpmi service is down");
		}
		
		if (pasPatient != null) {
			List<Long> idList = em.createQuery(
					"select p.id from Patient p" + // 20120225 index check : Patient.patKey : I_PATIENT_01
					" where p.patKey = :patKey" +
					" order by p.id")
					.setParameter("patKey", pasPatient.getPatKey())
					.getResultList();
			if (!idList.isEmpty()) {
				pasPatient.setId(idList.get(0));
			}
			Patient corpPatient = corpPmsServiceProxy.savePatient(pasPatient);
			return orderHelper.savePatient(em, corpPatient);
		}
		return null;
	}
	
	public void updateDrsPatient(DrsPatient drsPatientIn){
		em.merge(drsPatientIn);
		em.flush();
	}
	
	@Remove
	public void destroy() {
	}
}
