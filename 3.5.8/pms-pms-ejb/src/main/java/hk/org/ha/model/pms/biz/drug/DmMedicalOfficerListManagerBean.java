package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmMedicalOfficerCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmMedicalOfficer;
import hk.org.ha.model.pms.persistence.corp.Workstore;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("dmMedicalOfficerListManager")
@MeasureCalls
public class DmMedicalOfficerListManagerBean implements DmMedicalOfficerListManagerLocal {

	@Logger
	private Log logger;

	@In
	private Workstore workstore;

	@In
	private DmMedicalOfficerCacherInf dmMedicalOfficerCacher;

	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private List<DmMedicalOfficer> dmMedicalOfficerList;
	
	public List<DmMedicalOfficer> retrieveDmMedicalOfficerList() 
	{		
		return dmMedicalOfficerCacher.getDmMedicalOfficerList(workstore);	
	}
	
	public void retrieveDmMedicalOfficerList(String prefixMedicalOfficerCode) 
	{
		logger.debug("retrieveDmMedicalOfficerList");
				
		if(StringUtils.isBlank(prefixMedicalOfficerCode)){
			dmMedicalOfficerList = dmMedicalOfficerCacher.getDmMedicalOfficerList(workstore);
		}else{
			dmMedicalOfficerList = dmMedicalOfficerCacher.getDmMedicalOfficerListByMedicalOfficerCode(workstore, prefixMedicalOfficerCode);
		}
		
	}

	@Remove
	public void destroy() 
	{
		if (dmMedicalOfficerList != null) {
			dmMedicalOfficerList = null;
		}
	}
	
}
