package hk.org.ha.model.pms.biz.printing;

import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_DISPBARCODE_VISIBLE;
import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_HOSPITAL_NAMECHI;
import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_HOSPITAL_NAMEENG;
import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_INSTRUCTION_VISIBLE;
import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_LARGEFONT_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_LARGELAYOUT_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.LABEL_MPDISPLABEL_HKID_VISIBLE;
import static hk.org.ha.model.pms.prop.Prop.LABEL_MPDISPLABEL_WARDRTNBARCODE_VISIBLE;
import static hk.org.ha.model.pms.prop.Prop.MACHINE_BAKERCELL_PORT;
import static hk.org.ha.model.pms.prop.Prop.MACHINE_SCRIPTPRO_MESSAGE_ENCODING;
import static hk.org.ha.model.pms.prop.Prop.MACHINE_SCRIPTPRO_PORT;
import static hk.org.ha.model.pms.prop.Prop.TICKETGEN_FASTQUEUE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.VETTING_URGENTORDER_PRINTTOURGENTPRINTER;
import static hk.org.ha.model.pms.prop.Prop.VETTING_URGENTFASTQUEUEORDER_PRINTTOURGENTPRINTER;
import static hk.org.ha.model.pms.prop.Prop.MACHINE_SCRIPTPRO_MESSAGE_DELTACHANGEINDICATOR_REQUIRED;
import hk.org.ha.fmk.pms.remote.marshall.EclipseLinkXStreamMarshaller;
import hk.org.ha.fmk.pms.util.QueryUtils;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.label.LabelManagerLocal;
import hk.org.ha.model.pms.biz.reftable.RefTableManagerLocal;
import hk.org.ha.model.pms.biz.ticketgen.TicketManagerLocal;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.reftable.FdnMapping;
import hk.org.ha.model.pms.persistence.reftable.ItemLocation;
import hk.org.ha.model.pms.persistence.reftable.Machine;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.persistence.reftable.OperationWorkstation;
import hk.org.ha.model.pms.persistence.reftable.PickStation;
import hk.org.ha.model.pms.persistence.reftable.Prepack;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.prop.PropHolder;
import hk.org.ha.model.pms.prop.Scope;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.machine.MachinePrintLang;
import hk.org.ha.model.pms.udt.machine.ScriptProAction;
import hk.org.ha.model.pms.udt.machine.ScriptProPrintType;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.printing.PrintType;
import hk.org.ha.model.pms.udt.reftable.MachineType;
import hk.org.ha.model.pms.udt.reftable.PrinterConnectionType;
import hk.org.ha.model.pms.udt.reftable.PrinterType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.udt.vetting.RegimenType;
import hk.org.ha.model.pms.vo.label.BaseLabel;
import hk.org.ha.model.pms.vo.label.DispLabel;
import hk.org.ha.model.pms.vo.label.Warning;
import hk.org.ha.model.pms.vo.machine.BakerCellMessageContent;
import hk.org.ha.model.pms.vo.machine.ScriptProMessageContent;
import hk.org.ha.model.pms.vo.printing.PrintLabelDetail;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.PrinterSelect;
import hk.org.ha.model.pms.vo.rx.Regimen;
import hk.org.ha.service.pms.asa.interfaces.machine.MachineServiceJmsRemote;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.ejb.Remove;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.jboss.seam.security.Identity;
import org.joda.time.DateMidnight;

@AutoCreate
@Stateless
@Name("printLabelManager")
@MeasureCalls
public class PrintLabelManagerBean implements PrintLabelManagerLocal{

	@Logger
	private Log logger; 
	
	@PersistenceContext
	private EntityManager em; 
	
	@In
	private Identity identity;

	@In 
	private Workstore workstore;
	
	@In
	private Workstation workstation;
	
	@In
	private RefTableManagerLocal refTableManager;
	
	@In
	private LabelManagerLocal labelManager;
	
	@In
	private TicketManagerLocal ticketManager;
			
	@In
	private MachineServiceJmsRemote machineServiceProxy;
		
	private DateFormat sdf = new SimpleDateFormat("yyyyMMdd"); 
	private Map<Integer, OperationWorkstation> operationWorkstationMap = new HashMap<Integer, OperationWorkstation>();
	private Map<Machine, OperationWorkstation> operationWorkstationWithMachineMap;
	
	public void retrievePrintLabelDetailOnEdsMode(List<DispOrderItem> dispOrderItemList, PrintLang printLang)
	{
		OperationProfile activeProfile = refTableManager.retrieveActiveProfile(workstore);
		operationWorkstationWithMachineMap = null;
		
		OperationWorkstation fastQueueOperationWorkstation = getPrinterOperationWorkstation(PrinterType.Fast);
		PickStation fastQueuePickStation = getFastQueuePickingStation(fastQueueOperationWorkstation);
		Workstation fastQueuePrinterWorkstation = getFastQueueDefaultPrinterWorkstation();
		
		List<String> itemCodeList = this.getItemCodeListByDispOrderItemList(dispOrderItemList);
		List<ItemLocation> itemLocationFullList = this.retrieveItemLocationByItemCodeList(itemCodeList, 
																						activeProfile);

		Map<String, ItemLocation> defaultFastQueueItemLocationMap = this.retrieveDefFastQueueItemLocationByItemCodeList(itemCodeList, activeProfile, fastQueuePickStation);
		Map<String, OperationWorkstation> fastQueuePrinterOwMap = this.getFastQueueOperationWorkstationMap(activeProfile);
		
		List<Prepack> prepackFullList = this.retrievePrepackList(itemCodeList);
		
		operationWorkstationMap = retrieveOperationWorkstationMapWithMachine(activeProfile.getId());
		
		Workstation defaultPrinter = getDefaultPrinterWorkstation();
		DispOrder dispOrder = dispOrderItemList.get(0).getDispOrder();
				
		boolean normalPrintToUrgentPrint = VETTING_URGENTORDER_PRINTTOURGENTPRINTER.get(Boolean.TRUE);
		boolean fastPrintToUrgentPrint = VETTING_URGENTFASTQUEUEORDER_PRINTTOURGENTPRINTER.get(Boolean.TRUE);
		boolean isFastQueueTicket = isFastQueueTicket(dispOrder.getTicketDate(), dispOrder.getTicketNum());
		boolean useDefFastQueuePrinter = ( isFastQueueTicket && fastQueuePrinterWorkstation != null );
		boolean useFastQueueItemLocationPrinter = ( isFastQueueTicket || (TICKETGEN_FASTQUEUE_ENABLED.get(Boolean.FALSE) && dispOrder.getUrgentFlag()) );
		boolean printUrgentPrinter = ( (isFastQueueTicket && fastPrintToUrgentPrint) || (!isFastQueueTicket && normalPrintToUrgentPrint) );
		
		List<MachineType> itemLocationMachineList = this.getItemLocationMachineTypeList(isFastQueueTicket,
																							dispOrder.getUrgentFlag(), 
																							fastQueuePrinterWorkstation != null,
																							normalPrintToUrgentPrint,
																							fastPrintToUrgentPrint);
		
		for ( DispOrderItem dispOrderItem : dispOrderItemList ) 
		{
			if (dispOrderItem.getPrintFlag() || dispOrderItem.getStatus() == DispOrderItemStatus.KeepRecord) {
				continue;
			}
			
			PrintLabelDetail printLabelDetail = new PrintLabelDetail();
			printLabelDetail.setDefaultPrinterWorkstation(defaultPrinter);
			
			boolean itemLocationPrinterAvaliable = false;
			ItemLocation itemLocation = null;
			ItemLocation defaultFastQueueItemLocation = defaultFastQueueItemLocationMap.get(dispOrderItem.getPharmOrderItem().getItemCode());
			Map<ItemLocation, Boolean> itemLocationResultMap = getLabelItemLocation(itemLocationFullList, 
																						prepackFullList, 
																						dispOrderItem, 
																						printLang, 
																						activeProfile, 
																						itemLocationMachineList,
																						fastQueuePickStation,
																						useFastQueueItemLocationPrinter);
			if ( !itemLocationResultMap.isEmpty() ) {
				Map.Entry<ItemLocation, Boolean> entry = itemLocationResultMap.entrySet().iterator().next();
				itemLocation = entry.getKey();
				itemLocationPrinterAvaliable = entry.getValue();
			}
			
			printLabelDetail.setItemLocation(itemLocation);
			
			OperationWorkstation pickingOperationWorkstation = null;
			if ( itemLocation != null ) {
				pickingOperationWorkstation = getMachineActiveOperationWorkstation(printLabelDetail.getItemLocation().getMachine(), activeProfile.getId());
			}
			
			OperationWorkstation printerOperationWorkstation = null;
			if ( itemLocationPrinterAvaliable ) {
				printerOperationWorkstation = pickingOperationWorkstation;
			}
			
			boolean useItemLocationPrint = false;
			if ( useDefFastQueuePrinter ) {
				printLabelDetail.setPrinterWorkstation(fastQueuePrinterWorkstation);
				//Override Item Location to Default Fast Queue Printer
				if( defaultFastQueueItemLocation != null ){
					printLabelDetail.setItemLocation(defaultFastQueueItemLocation);
				}
			}else if( dispOrder.getUrgentFlag() && printUrgentPrinter ){
				printLabelDetail.setPrinterWorkstation(getPrinterOperationWorkstation(PrinterType.Urgent).getWorkstation());
			}else{
				useItemLocationPrint = true;
			}
			
			if( useItemLocationPrint ){
				if ( itemLocationPrinterAvaliable ) {
					boolean isScriptPro = (printLabelDetail.getItemLocation().getMachine().getType() == MachineType.ScriptPro);
					Workstation printerWorkstation = getPrintLabelDetailPrinterWorkstation(printerOperationWorkstation, isScriptPro, useFastQueueItemLocationPrinter);
				
					if ( printerWorkstation == null ) {
						printerWorkstation = printLabelDetail.getDefaultPrinterWorkstation();
					}
					printLabelDetail.setPrinterWorkstation(printerWorkstation);
					
					if ( printLabelDetail.getItemLocation().getMachine().getType() == MachineType.BakerCell ) {
						sendBakerCellSocketMessage(dispOrderItem, printLabelDetail.getItemLocation().getBakerCellNum(), printLabelDetail.getItemLocation().getMachine().getHostName());
					} else if ( isScriptPro ) {
						sendScriptProSocketMessage(dispOrderItem, printLabelDetail.getItemLocation().getBinNum(), printLabelDetail.getItemLocation().getMachine().getHostName(), printLang);
						printLabelDetail.getDispOrderItemIdMap().put(dispOrderItem.getId(), dispOrderItem.getId());
					}
				}else{
					printLabelDetail.setPrinterWorkstation(printLabelDetail.getDefaultPrinterWorkstation());
				}
			}
			
			if( printLabelDetail.getItemLocation() == null && defaultFastQueueItemLocation != null ){
				printLabelDetail.setItemLocation(defaultFastQueueItemLocation);
			}
			
			if( useFastQueueItemLocationPrinter && isFastQueuePrinterAvailable(printLabelDetail.getPrinterWorkstation(), fastQueuePrinterOwMap) ){
				printLabelDetail.setPrintFastQueueLabelPrinterFlag(Boolean.TRUE);
			}
			printLabelDetail.setWorkstation(printLabelDetail.getPrinterWorkstation());
			dispOrderItem.setPrintLabelDetail(printLabelDetail);
		}
	}
	
	public PrintLabelDetail retrievePrintLabelDetailOnNonPeakMode(List<DispOrderItem> dispOrderItemList, PrintLang printLang)
	{
		OperationProfile activeProfile = refTableManager.retrieveActiveProfile(workstore);
		operationWorkstationWithMachineMap = null;
		
		OperationWorkstation fastQueueOperationWorkstation = getPrinterOperationWorkstation(PrinterType.Fast);
		Workstation fastQueuePrinterWorkstation = getFastQueueDefaultPrinterWorkstation();
		PickStation fastQueuePickStation = getFastQueuePickingStation(fastQueueOperationWorkstation);
		
		List<String> itemCodeList = this.getItemCodeListByDispOrderItemList(dispOrderItemList);
		List<ItemLocation> itemLocationFullList = this.retrieveItemLocationByItemCodeList(itemCodeList, activeProfile);
		
		Map<String, ItemLocation> defaultFastQueueItemLocationMap = this.retrieveDefFastQueueItemLocationByItemCodeList(itemCodeList, activeProfile, fastQueuePickStation);
		Map<String, OperationWorkstation> fastQueuePrinterOwMap = this.getFastQueueOperationWorkstationMap(activeProfile);
		
		List<Prepack> prepackFullList = this.retrievePrepackList(itemCodeList);
		operationWorkstationMap = retrieveOperationWorkstationMapWithMachine(activeProfile.getId());
		PrintLabelDetail printLabelDetail = new PrintLabelDetail();
		printLabelDetail.setDefaultPrinterWorkstation(getDefaultPrinterWorkstation());		
		
		DispOrder dispOrder = dispOrderItemList.get(0).getDispOrder();
		
		boolean normalPrintToUrgentPrint = VETTING_URGENTORDER_PRINTTOURGENTPRINTER.get(Boolean.TRUE);
		boolean fastPrintToUrgentPrint = VETTING_URGENTFASTQUEUEORDER_PRINTTOURGENTPRINTER.get(Boolean.TRUE);
		boolean isFastQueueTicket = isFastQueueTicket(dispOrder.getTicketDate(), dispOrder.getTicketNum());
		boolean useDefFastQueuePrinter = ( isFastQueueTicket && fastQueuePrinterWorkstation != null );
		boolean useFastQueueItemLocationPrinter = ( isFastQueueTicket || (TICKETGEN_FASTQUEUE_ENABLED.get(Boolean.FALSE) && dispOrder.getUrgentFlag()) );
		boolean printUrgentPrinter = ( (isFastQueueTicket && fastPrintToUrgentPrint) || (!isFastQueueTicket && normalPrintToUrgentPrint) );
		
		List<MachineType> itemLocationMachineList = this.getItemLocationMachineTypeList(isFastQueueTicket,
																							dispOrder.getUrgentFlag(), 
																							fastQueuePrinterWorkstation != null,
																							normalPrintToUrgentPrint,
																							fastPrintToUrgentPrint);
		
		List<ItemLocation> itemLocationList = new ArrayList<ItemLocation>();
		List<ItemLocation> bakerCellList = new ArrayList<ItemLocation>();
		Map<DispOrderItem, ItemLocation> callMachineMap = new HashMap<DispOrderItem, ItemLocation>();
		Map<ItemLocation, Workstation> itemLocationPrinterMap = new HashMap<ItemLocation, Workstation>();
		
		for (DispOrderItem dispOrderItem : dispOrderItemList) 
		{
			if (dispOrderItem.getPrintFlag() || dispOrderItem.getStatus() == DispOrderItemStatus.KeepRecord) {
				continue;
			}
			
			boolean itemLocationPrinterAvaliable = false;
			ItemLocation itemLocation = null;
			ItemLocation defaultFastQueueItemLocation = defaultFastQueueItemLocationMap.get(dispOrderItem.getPharmOrderItem().getItemCode());			
			Map<ItemLocation, Boolean> itemLocationResultMap = getLabelItemLocation(itemLocationFullList, 
																						prepackFullList, 
																						dispOrderItem, 
																						printLang, 
																						activeProfile, 
																						itemLocationMachineList,
																						fastQueuePickStation,
																						useFastQueueItemLocationPrinter);
			if ( !itemLocationResultMap.isEmpty() ) {
				Map.Entry<ItemLocation, Boolean> entry = itemLocationResultMap.entrySet().iterator().next();
				itemLocation = entry.getKey();
				itemLocationPrinterAvaliable = entry.getValue();
			}
			
			if( useDefFastQueuePrinter ){
				//Override Item Location to Default Fast Queue Printer
				if( defaultFastQueueItemLocation != null ){
					dispOrderItem.replaceBinNum(defaultFastQueueItemLocation.getBinNum());
				}else if( itemLocation != null ){
					dispOrderItem.replaceBinNum(itemLocation.getBinNum());
				}
			}else{
				if ( itemLocation != null ) {
					dispOrderItem.replaceBinNum(itemLocation.getBinNum());
	
					OperationWorkstation pickStation = getMachineActiveOperationWorkstation(itemLocation.getMachine(), activeProfile.getId());
					
					if ( itemLocationPrinterAvaliable ) {
						if ( itemLocation.getMachine().getType() != MachineType.Manual ) 
						{
							callMachineMap.put(dispOrderItem, itemLocation);
						}
	
						boolean isScriptPro = (itemLocation.getMachine().getType() == MachineType.ScriptPro);
						Workstation printerWorkstation = this.getPrintLabelDetailPrinterWorkstation(pickStation, isScriptPro, useFastQueueItemLocationPrinter);
						
						if ( printerWorkstation != null ) 
						{
							itemLocationPrinterMap.put(itemLocation, printerWorkstation);
							itemLocationList.add(itemLocation);
	
							if ( itemLocation.getMachine().getType() == MachineType.BakerCell ) 
							{
								bakerCellList.add(itemLocation);
							}
						}
					}
				}else if( defaultFastQueueItemLocation != null ){
					dispOrderItem.replaceBinNum(defaultFastQueueItemLocation.getBinNum());
				}
			}
		}
		
		boolean useItemLocationPrint = false;
		
		if ( useDefFastQueuePrinter ) {
			printLabelDetail.setPrinterWorkstation(fastQueuePrinterWorkstation);
		} else if( dispOrder.getUrgentFlag() && printUrgentPrinter ){
			printLabelDetail.setPrinterWorkstation(getPrinterOperationWorkstation(PrinterType.Urgent).getWorkstation());
		}else{
			useItemLocationPrint = true;
		}
		
		if ( useItemLocationPrint ) {
			ItemLocation printerItemLocation = null;
			if ( !bakerCellList.isEmpty() ) {
				if ( bakerCellList.size() > 1) {
					int index = getRandomListIndex(bakerCellList);
					printerItemLocation = bakerCellList.get(index);
				} else {
					printerItemLocation = bakerCellList.get(0);
				}
			} else if ( !itemLocationList.isEmpty() ){
				if ( itemLocationList.size() > 1) {
					int index = getRandomListIndex(itemLocationList);
					printerItemLocation = itemLocationList.get(index);
				} else {
					printerItemLocation = itemLocationList.get(0);
				}
			}
			
			Workstation printerWorkstation = null;
			if ( printerItemLocation != null ) 
			{
				printerWorkstation = itemLocationPrinterMap.get(printerItemLocation);
			}
			if ( printerWorkstation != null ) 
			{
				printLabelDetail.setPrinterWorkstation(printerWorkstation);
			} 
			else 
			{
				printLabelDetail.setPrinterWorkstation(printLabelDetail.getDefaultPrinterWorkstation());
			}
			
			if ( !callMachineMap.isEmpty() ) 
			{
				for ( Map.Entry<DispOrderItem, ItemLocation> machineLocation : callMachineMap.entrySet() ) {
					this.sendToMachine(printLabelDetail, machineLocation.getValue(), machineLocation.getKey(), printLang);
				}
			}
		}
		
		if( useFastQueueItemLocationPrinter && isFastQueuePrinterAvailable(printLabelDetail.getPrinterWorkstation(), fastQueuePrinterOwMap) ){
			printLabelDetail.setPrintFastQueueLabelPrinterFlag(Boolean.TRUE);
		}

		printLabelDetail.setWorkstation(printLabelDetail.getPrinterWorkstation());
		return printLabelDetail;
	}
	
	private List<String> getItemCodeListByDispOrderItemList(List<DispOrderItem> dispOrderItemList){
		List<String> itemCodeList = new ArrayList<String>();
		for ( DispOrderItem dispOrderItem : dispOrderItemList ) {
			itemCodeList.add(dispOrderItem.getPharmOrderItem().getItemCode());
		}
		return itemCodeList;
	}
	
	public void retrievePrintLabelDetailOnPmsMode(List<DispOrderItem> dispOrderItemList, PrintLang printLang)
	{
		OperationProfile activeProfile = refTableManager.retrieveActiveProfile(workstore);
		operationWorkstationWithMachineMap = null;
		
		OperationWorkstation fastQueueOperationWorkstation = getPrinterOperationWorkstation(PrinterType.Fast);
		Workstation fastQueuePrinterWorkstation = getFastQueueDefaultPrinterWorkstation();
		PickStation fastQueuePickStation = getFastQueuePickingStation(fastQueueOperationWorkstation);
		
		List<String> itemCodeList = this.getItemCodeListByDispOrderItemList(dispOrderItemList);
		List<ItemLocation> itemLocationFullList = this.retrieveItemLocationByItemCodeList(itemCodeList, activeProfile);
		
		Map<String, ItemLocation> defaultFastQueueItemLocationMap = this.retrieveDefFastQueueItemLocationByItemCodeList(itemCodeList, activeProfile, fastQueuePickStation);
		Map<String, OperationWorkstation> fastQueuePrinterOwMap = this.getFastQueueOperationWorkstationMap(activeProfile);

		List<Prepack> prepackFullList = this.retrievePrepackList(itemCodeList);
		operationWorkstationMap = retrieveOperationWorkstationMapWithMachine(activeProfile.getId());
		Workstation defaultPrinter = getDefaultPrinterWorkstation();
		
		DispOrder dispOrder = dispOrderItemList.get(0).getDispOrder();
		
		boolean normalPrintToUrgentPrint = VETTING_URGENTORDER_PRINTTOURGENTPRINTER.get(Boolean.TRUE);
		boolean fastPrintToUrgentPrint = VETTING_URGENTFASTQUEUEORDER_PRINTTOURGENTPRINTER.get(Boolean.TRUE);
		boolean isFastQueueTicket = isFastQueueTicket(dispOrder.getTicketDate(), dispOrder.getTicketNum());
		boolean useDefFastQueuePrinter = ( isFastQueueTicket && fastQueuePrinterWorkstation != null );
		boolean useFastQueueItemLocationPrinter = ( isFastQueueTicket || (TICKETGEN_FASTQUEUE_ENABLED.get(Boolean.FALSE) && dispOrder.getUrgentFlag()) );
		boolean printUrgentPrinter = ( (isFastQueueTicket && fastPrintToUrgentPrint) || (!isFastQueueTicket && normalPrintToUrgentPrint) );
		
		List<MachineType> itemLocationMachineList = this.getItemLocationMachineTypeList(isFastQueueTicket,
																							dispOrder.getUrgentFlag(), 
																							fastQueuePrinterWorkstation != null,
																							normalPrintToUrgentPrint,
																							fastPrintToUrgentPrint);
		
		for ( DispOrderItem dispOrderItem : dispOrderItemList ) {
			if (dispOrderItem.getPrintFlag() || dispOrderItem.getStatus() == DispOrderItemStatus.KeepRecord) {
				continue;
			}
			PrintLabelDetail printLabelDetail = new PrintLabelDetail();
			printLabelDetail.setDefaultPrinterWorkstation(defaultPrinter);		
			
			boolean machineAvaliable = false;
			ItemLocation itemLocation = null;
			ItemLocation defaultFastQueueItemLocation = defaultFastQueueItemLocationMap.get(dispOrderItem.getPharmOrderItem().getItemCode());
			Map<ItemLocation, Boolean> itemLocationResultMap = getLabelItemLocation(itemLocationFullList, 
																						prepackFullList, 
																						dispOrderItem, 
																						printLang, 
																						activeProfile, 
																						itemLocationMachineList,
																						fastQueuePickStation,
																						useFastQueueItemLocationPrinter);
			
			if ( !itemLocationResultMap.isEmpty() ) {
				Map.Entry<ItemLocation, Boolean> entry = itemLocationResultMap.entrySet().iterator().next();
				itemLocation = entry.getKey();
				machineAvaliable = entry.getValue();
			}
			
			printLabelDetail.setItemLocation(itemLocation);
			
			boolean useItemLocationPrint = false;
			if ( useDefFastQueuePrinter ) {
				printLabelDetail.setPrinterWorkstation(fastQueuePrinterWorkstation);
				//Override Item Location to Default Fast Queue Printer
				if( defaultFastQueueItemLocation != null ){
					printLabelDetail.setItemLocation(defaultFastQueueItemLocation);
				}
			}else if( dispOrder.getUrgentFlag() && printUrgentPrinter ){
				printLabelDetail.setPrinterWorkstation(getPrinterOperationWorkstation(PrinterType.Urgent).getWorkstation());
			}else{
				useItemLocationPrint = true;
			}

			if( useItemLocationPrint ){
				// set localWorkstation to printerWorkstation
				OperationWorkstation localOperationWorkstation =  getLocalOperationWorkstation(activeProfile);
				Workstation printerWorkstation = getPrintLabelDetailPrinterWorkstation(localOperationWorkstation, false, useFastQueueItemLocationPrinter);
				if ( printerWorkstation == null ) {
					printerWorkstation = printLabelDetail.getDefaultPrinterWorkstation();
				}
				printLabelDetail.setPrinterWorkstation(printerWorkstation);
				
				if ( printLabelDetail.getItemLocation() != null ){
					if ( machineAvaliable && printLabelDetail.getItemLocation().getMachine().getType() != MachineType.Manual ){
						this.sendToMachine(printLabelDetail, printLabelDetail.getItemLocation(), dispOrderItem, printLang);
					}
				}
			}
			
			if( printLabelDetail.getItemLocation() == null && defaultFastQueueItemLocation != null ){
				printLabelDetail.setItemLocation(defaultFastQueueItemLocation);
			}
			if( useFastQueueItemLocationPrinter && isFastQueuePrinterAvailable(printLabelDetail.getPrinterWorkstation(), fastQueuePrinterOwMap) ){
				printLabelDetail.setPrintFastQueueLabelPrinterFlag(Boolean.TRUE);
			}
			printLabelDetail.setWorkstation(printLabelDetail.getPrinterWorkstation());
			dispOrderItem.setPrintLabelDetail(printLabelDetail);
		}
	}
	
	private void sendToMachine(PrintLabelDetail printLabelDetail, ItemLocation itemLocation, DispOrderItem dispOrderItem, PrintLang printLang){
			if ( itemLocation.getMachine().getType() == MachineType.BakerCell ) {
				sendBakerCellSocketMessage(dispOrderItem, itemLocation.getBakerCellNum(), itemLocation.getMachine().getHostName());
			} else {
				sendScriptProSocketMessage(dispOrderItem, itemLocation.getBinNum(), itemLocation.getMachine().getHostName(), printLang);
				printLabelDetail.getDispOrderItemIdMap().put(dispOrderItem.getId(), dispOrderItem.getId());
			}
	}
	
	private OperationWorkstation getLocalOperationWorkstation(OperationProfile activeProfile){
		OperationWorkstation localOperationWorkstation = null;
		for ( OperationWorkstation operationWorkstation : activeProfile.getOperationWorkstationList() ) {
			if ( StringUtils.equals(operationWorkstation.getWorkstation().getWorkstationCode(), workstation.getWorkstationCode())) {
				localOperationWorkstation = operationWorkstation;
				break;
			}
		}
		return localOperationWorkstation;
	}

	private Workstation getPrintLabelDetailPrinterWorkstation(OperationWorkstation operationWorkstation, boolean isScriptPro, boolean fastQueuePrinter){
		
		Workstation tmpWorkstation = operationWorkstation.getWorkstation();
		if ( !isScriptPro ){
			if(operationWorkstation.getPrinterConnectionType() == PrinterConnectionType.No ) {
				if( fastQueuePrinter && operationWorkstation.getRedirectFastQueueStation() != null ){
					tmpWorkstation = getRedirectPrintStation(operationWorkstation, fastQueuePrinter);
				}else{
					tmpWorkstation = getRedirectPrintStation(operationWorkstation, false);
				}
			}else if( operationWorkstation.getPrinterConnectionType() == PrinterConnectionType.Yes ){
				if( fastQueuePrinter && Boolean.TRUE != operationWorkstation.getFastQueuePrintFlag() && operationWorkstation.getRedirectFastQueueStation() != null ){
					tmpWorkstation = getRedirectPrintStation(operationWorkstation, fastQueuePrinter);
				}
			}
		}

		return tmpWorkstation;
	}
	
	@SuppressWarnings("unchecked")
	private List<ItemLocation> retrieveItemLocationByItemCodeList(List<String> itemCodeList, OperationProfile activeProfile){
		List<ItemLocation> itemLocationList = QueryUtils.splitExecute(em.createQuery(
							"select o from ItemLocation o, OperationWorkstation m" + // 20120303 index check : ItemLocation.workstore,itemCode : I_ITEM_LOCATION_02
							" where o.workstore = :workstore" +
							" and o.itemCode in :itemCodeList " +
							" and o.machine.pickStation = m.machine.pickStation" +
							" and m.operationProfile.id = :activeId" +
							" and o.machine.type <> :atdpsType")
							.setParameter("workstore", workstore)
							.setParameter("activeId", activeProfile.getId())
							.setParameter("atdpsType", MachineType.ATDPS)
							.setHint(QueryHints.FETCH, "o.machine.pickStation"), "itemCodeList", itemCodeList);
		return itemLocationList;
	}
	
	@SuppressWarnings("unchecked")
	private Map<String, ItemLocation> retrieveDefFastQueueItemLocationByItemCodeList(List<String> itemCodeList, OperationProfile activeProfile, PickStation defaultFastQueuePickStation){
		if( defaultFastQueuePickStation == null ){
			return new HashMap<String, ItemLocation>();
		}
		
		List<ItemLocation> itemLocationList = QueryUtils.splitExecute(em.createQuery(
				"select o from ItemLocation o, OperationWorkstation m" + // 20170418 index check : ItemLocation.workstore,itemCode : I_ITEM_LOCATION_02
				" where o.workstore = :workstore" +
				" and o.itemCode in :itemCodeList " +
				" and o.machine.pickStation = m.machine.pickStation" +
				" and o.machine.pickStation = :fastQueuePickStation" +
				" and m.operationProfile.id = :activeId" +
				" and o.machine.type = :manualType")
				.setParameter("workstore", workstore)
				.setParameter("activeId", activeProfile.getId())
				.setParameter("manualType", MachineType.Manual)
				.setParameter("fastQueuePickStation", defaultFastQueuePickStation)
				.setHint(QueryHints.FETCH, "o.machine.pickStation"), "itemCodeList", itemCodeList);
		
		Map<String, ItemLocation> fastItemLocationMap = new HashMap<String, ItemLocation>();
		for ( ItemLocation itemLocation : itemLocationList ) {
			fastItemLocationMap.put(itemLocation.getItemCode(), itemLocation);
		}
		return fastItemLocationMap;
	}
	
	private List<ItemLocation> retrieveItemLocationByItemCode(List<ItemLocation> itemLocationList, String itemCode, OperationProfile activeProfile, List<MachineType> supportMachineList, PickStation fastQueuePickingStation) 
	{
		List<ItemLocation> resultList = new ArrayList<ItemLocation>();
		for ( ItemLocation itemLocation : itemLocationList ) {
			if ( itemLocation.getItemCode().equals(itemCode) ) {
				resultList.add(itemLocation);
			}
		}
		
		for (Iterator<ItemLocation> itr = resultList.iterator();itr.hasNext();) {
			ItemLocation itemLocation = itr.next();
			if ( !machineSetInOperationWorkstation(itemLocation.getMachine(), activeProfile.getId(), supportMachineList) ) {
				itr.remove();
			}else if(itemLocation.getMachine().getPickStation() == fastQueuePickingStation){
				itr.remove();
			}
		}
		
		return resultList;
	}
	
	private boolean machineSetInOperationWorkstation(Machine machine, Long activeProfileId, List<MachineType> supportMachineList){	
		if ( machine.getType() == MachineType.Manual ) {
			return true;
		}
		
		if( !supportMachineList.contains(machine.getType()) ){
			return false;
		}
		
		OperationWorkstation operationWorkstation = getMachineActiveOperationWorkstation( machine, activeProfileId );
		
		if ( operationWorkstation == null ) {	
			return false;
		}

		return ( machine.getType() == operationWorkstation.getMachine().getType() );
	}
	
	@SuppressWarnings("unchecked")
	private List<OperationWorkstation> getActiveOperationWorkstationWithMachineList(Long activeProfileId) {
		return em.createQuery(
				"select o from OperationWorkstation o" + // 20120831 index check : OperationWorkstation.operationProfile : FK_OPERATION_WORKSTATION_01
				" where o.operationProfile.id = :activeProfileId" +
				" and o.machine is not null")
				.setParameter("activeProfileId", activeProfileId)
				.setHint(QueryHints.FETCH, "o.machine.pickStation")
				.getResultList();
	}
	
	private OperationWorkstation getActiveOperationWorkstationByMachine(Machine machine, Long activeProfileId) {
		if ( operationWorkstationWithMachineMap == null ) {
			operationWorkstationWithMachineMap = new HashMap<Machine, OperationWorkstation>();
			List<OperationWorkstation> resultList = getActiveOperationWorkstationWithMachineList(activeProfileId);

			if ( !resultList.isEmpty() ) {
				for ( OperationWorkstation operationWorkstation : resultList ) {
					operationWorkstationWithMachineMap.put(operationWorkstation.getMachine(), operationWorkstation);
				}
			}
		}
		
		return operationWorkstationWithMachineMap.get(machine);
	}
	
	private OperationWorkstation getMachineActiveOperationWorkstation(Machine machine, Long activeProfileId) {
		OperationWorkstation operationWorkstation = getActiveOperationWorkstationByMachine(machine, activeProfileId);
		if ( operationWorkstation != null ) {
			return operationWorkstation;
		}

		if ( machine.getType() == MachineType.Manual ) {
			return retrieveOperationWorkstationByPickStationNum(machine.getPickStation().getPickStationNum(), activeProfileId);
		}

		return null;
	}
	
	private OperationWorkstation retrieveOperationWorkstationByPickStationNum(int pickStationNum, Long activeProfileId){
		if ( operationWorkstationMap == null ) {
			retrieveOperationWorkstationMapWithMachine(activeProfileId);
		}

		return operationWorkstationMap.get(pickStationNum);
	}
	
	private Map<Integer, OperationWorkstation> retrieveOperationWorkstationMapWithMachine(Long activeProfileId){
		Map<Integer, OperationWorkstation> operationWorkstationMap = new HashMap<Integer, OperationWorkstation>();
		List<OperationWorkstation> resultList = getActiveOperationWorkstationWithMachineList(activeProfileId);
		
		if ( !resultList.isEmpty() ) {
			for ( OperationWorkstation operationWorkstation : resultList ) {
				operationWorkstationMap.put(operationWorkstation.getMachine().getPickStation().getPickStationNum(), operationWorkstation);
			}
		}
		return operationWorkstationMap;
	}
	
	@SuppressWarnings("unchecked")
	private List<Prepack> retrievePrepackList(List<String> itemCodeList){
		return em.createQuery(
				"select o from Prepack o" + // 20120303 index check : Prepack.workstore,itemCode : UI_PREPACK_01
				" where o.itemCode in :itemCodeList" +
				" and o.workstore = :workstore")
				.setParameter("itemCodeList", itemCodeList)
				.setParameter("workstore", workstore)
				.getResultList();
	}
	
	private boolean hasPrepack(List<Prepack> prepackFullList, String itemCode, BigDecimal dispQty, OperationProfile activeProfile) {
		boolean hasPrePack = false;
		List<Prepack> prepackList = new ArrayList<Prepack>();
		for ( Prepack prepack : prepackFullList ) {
			if ( prepack.getItemCode().equals(itemCode) ) {
				prepackList.add(prepack);
			}
		}
		
		if (!prepackList.isEmpty()) {
			for (Prepack prepack : prepackList) {
				for (Integer prepackQty : prepack.getPrepackQty()) {
					if ( BigDecimal.valueOf(prepackQty).compareTo(dispQty) == 0 ) {
						hasPrePack = true;
						break;
					}
				}
			}
		}
		return hasPrePack;
	}
	
	private Map<ItemLocation, Boolean> selectItemLocationByMachine(DispOrderItem dispOrderItem, PrintLang printLang, List<ItemLocation> itemLocationList, Long activeProfileId, boolean useFastQueueItemLocationPrinter)
	{
		Map<ItemLocation, Boolean> result = new HashMap<ItemLocation, Boolean>();
		List<ItemLocation> labelPrinterManualItemLocationList = new ArrayList<ItemLocation>();
		for ( ItemLocation itemLocation : itemLocationList ) 
		{
			if ( itemLocation.getMachine().getType() == MachineType.ScriptPro ) 
			{
				OperationWorkstation operationWorkstation = this.getMachineActiveOperationWorkstation(itemLocation.getMachine(), activeProfileId);

				if ( !operationWorkstation.getEnableFlag() ) {
					continue;
				}
				if ( BigDecimal.valueOf(itemLocation.getMaxQty()).compareTo(dispOrderItem.getDispQty()) < 0 ) {  //maxQty < dispQty
					continue;
				}
				if ( !validForScriptPro(dispOrderItem, printLang) ) {
					continue;
				} 
				result.put(itemLocation, true);
				return result;
			} 
			else if ( itemLocation.getMachine().getType() == MachineType.BakerCell ) 
			{
				OperationWorkstation operationWorkstation = this.getMachineActiveOperationWorkstation(itemLocation.getMachine(), activeProfileId);
				if ( !operationWorkstation.getEnableFlag() ) {
					continue;
				}
				if ( BigDecimal.valueOf(itemLocation.getMaxQty()).compareTo(dispOrderItem.getDispQty()) < 0 ) {  //maxQty < dispQty
					continue;
				}
				result.put(itemLocation, true);
				return result;
			} 
			else if ( itemLocation.getMachine().getType() == MachineType.Manual ) 
			{
				if( useFastQueueItemLocationPrinter ){
					OperationWorkstation operationWorkstation = this.getMachineActiveOperationWorkstation(itemLocation.getMachine(), activeProfileId);
					
					if( Boolean.TRUE == operationWorkstation.getFastQueuePrintFlag() || operationWorkstation.getRedirectFastQueueStation() != null){
						result.put(itemLocation, true);
						return result;
					}else{
						labelPrinterManualItemLocationList.add(itemLocation);
					}
				}else{
					result.put(itemLocation, true);
					return result;
				}
			}
		}
		
		if( !labelPrinterManualItemLocationList.isEmpty() ){
			result.put(labelPrinterManualItemLocationList.get(0), true);
			return result;
		}
		result.put(itemLocationList.get(0), false);
		return result;
	}
	
	private boolean validForScriptPro(DispOrderItem dispOrderItem, PrintLang printLang){
		if ( dispOrderItem.getBaseLabel() != null ) {
			PrintOption printOption = new PrintOption();
			printOption.setPrintLang(printLang);
			if (LABEL_DISPLABEL_LARGELAYOUT_ENABLED.get(Boolean.FALSE)) {
				printOption.setPrintType(PrintType.LargeLayout);
			} else if (LABEL_DISPLABEL_LARGEFONT_ENABLED.get(Boolean.FALSE)) {
				printOption.setPrintType(PrintType.LargeFont);
			} else {
				printOption.setPrintType(PrintType.Normal);
			}
			dispOrderItem.getBaseLabel().setPrintOption(printOption);
		} 

		//skip step up down and multDose
		if ( dispOrderItem.getPharmOrderItem().getRegimen() != null ) {
			Regimen regimen = dispOrderItem.getPharmOrderItem().getRegimen();
			if ( regimen.getType() == RegimenType.StepUpDown ) {
				return false;
			}
			
			//multDose
			if ( regimen.getDoseGroupList().get(0).getDoseList().size() > 1) {
				return false;
			}
		}

		return isSinglePageLabel(dispOrderItem);
	}
	
	//check total page of label
	private boolean isSinglePageLabel(DispOrderItem dispOrderItem){

		Date startDate = dispOrderItem.getStartDate();
		Date ticketDate = dispOrderItem.getDispOrder().getTicket().getTicketDate();
		Date orderDate = new DateMidnight(dispOrderItem.getDispOrder().getPharmOrder().getMedOrder().getOrderDate()).toDate();
		
		if( dispOrderItem.getDispOrder().getPharmOrder().getMedOrder().getDocType() == MedOrderDocType.Manual ){
			if (!startDate.equals(ticketDate) ){
				return false;
			}
		}else if( !startDate.equals(orderDate) ){
			return false;
		}
		
		BaseLabel baseLabel = dispOrderItem.getBaseLabel();
		int warningCount = baseLabel.getWarningText().split("\n").length;

		if ( LABEL_DISPLABEL_LARGELAYOUT_ENABLED.get(Boolean.FALSE) || LABEL_DISPLABEL_LARGEFONT_ENABLED.get(Boolean.FALSE) ) {
			int instructionRowCount = baseLabel.getInstructionLowerCaseText().split("\n").length;

			if ( (warningCount + instructionRowCount) %4 > 0 ) {
				return (((warningCount + instructionRowCount) /4) +1 ) <= 1;
			} 
			return (((warningCount + instructionRowCount) /4) ) <= 1;
		} 	
		
		int instructionRowCount = baseLabel.getInstructionText().split("\n").length;
		
		if ( instructionRowCount >= 3 && warningCount > 0 ) {
			return false;
		}
		
		if ( instructionRowCount > 6 ) {
			return false;
		}

		return true;
	}
	
	private Map<ItemLocation,Boolean> getLabelItemLocation(List<ItemLocation> itemLocationFullList, 
																					List<Prepack> prepackFullList,
																					DispOrderItem dispOrderItem, 
																					PrintLang printLang, 
																					OperationProfile activeProfile,
																					List<MachineType> supportMachineList,
																					PickStation fastQueuePickingStation,
																					boolean useFastQueueItemLocationPrinter) {
		Map<ItemLocation, Boolean> resultMap = new HashMap<ItemLocation, Boolean>();
		
		boolean hasPrePack = hasPrepack(prepackFullList, dispOrderItem.getPharmOrderItem().getItemCode(), dispOrderItem.getDispQty(), activeProfile);
		
		boolean useManual = false;
		if( supportMachineList.size() == 1 && supportMachineList.get(0) == MachineType.Manual ){
			useManual = true;
		}
		
		List<ItemLocation> itemLocationList = retrieveItemLocationByItemCode(itemLocationFullList, 
								dispOrderItem.getPharmOrderItem().getItemCode(), 
								activeProfile, 
								supportMachineList,
								fastQueuePickingStation); // filtering

		if ( !itemLocationList.isEmpty() ) {
			List<ItemLocation> sortedItemLocationList = sortItemLocationListByProcessTypePriority(itemLocationList, useManual, hasPrePack);
			resultMap = selectItemLocationByMachine(dispOrderItem, printLang, sortedItemLocationList, activeProfile.getId(), useFastQueueItemLocationPrinter);
		}

		return resultMap;
	}
	
	private Workstation getRedirectPrintStation(OperationWorkstation printerOperationWorkstation, boolean fastQueueRedirect){
		Workstation availableWorkstation = null;
		if ( printerOperationWorkstation == null ) {
			return availableWorkstation;
		}
		
		String redirectWorkstationCode = printerOperationWorkstation.getRedirectPrintStation();
		if( fastQueueRedirect ){
			redirectWorkstationCode = printerOperationWorkstation.getRedirectFastQueueStation();
		}
		
		//check redirect printing station
		if ( StringUtils.isNotBlank(redirectWorkstationCode) ) {
			
			OperationProfile activeProfile = refTableManager.retrieveActiveProfile(workstore);

			OperationWorkstation redirectOperationWorkstation  = null;			
			for ( OperationWorkstation operationWorkstation : activeProfile.getOperationWorkstationList() ) {
				if (StringUtils.equals(operationWorkstation.getWorkstation().getWorkstationCode(), redirectWorkstationCode)) {
					redirectOperationWorkstation = operationWorkstation;
					break;
				}
			}

			if(redirectOperationWorkstation!= null) {
				if ( redirectOperationWorkstation.getPrinterConnectionType() == PrinterConnectionType.No ){
					availableWorkstation = null;
				} else {
					availableWorkstation = redirectOperationWorkstation.getWorkstation();
				}
			}
		}
		
		return availableWorkstation;
	}
	
	private Workstation getDefaultPrinterWorkstation(){
		OperationWorkstation operationWorkstationDefaultPrinter = getPrinterOperationWorkstation(PrinterType.Default);
		return operationWorkstationDefaultPrinter.getWorkstation();
	}
	
	@SuppressWarnings("unchecked")
	private OperationWorkstation getPrinterOperationWorkstation(PrinterType printerType){
		OperationWorkstation operationWorkstation= null;
		
		List<PrinterType> printerTypeList = new ArrayList<PrinterType>();
		printerTypeList.add(printerType);
		printerTypeList.add(PrinterType.DefaultAndUrgentAndFast);
		
		if ( printerType == PrinterType.Default ) {
			printerTypeList.add(PrinterType.DefaultAndFast);
			printerTypeList.add(PrinterType.DefaultAndUrgent);
		} else if ( printerType == PrinterType.Urgent ) {
			printerTypeList.add(PrinterType.DefaultAndUrgent);
			printerTypeList.add(PrinterType.UrgentAndFast);
		} else if ( printerType == PrinterType.Fast ) {
			printerTypeList.add(PrinterType.DefaultAndFast);
			printerTypeList.add(PrinterType.UrgentAndFast);
		}
		
		List<OperationWorkstation> operationWorkstationList = em.createQuery(
				"select o from OperationWorkstation o" + // 20120303 index check : operationProfile.workstore : FK_OPERATION_PROFILE_01
				" where o.operationProfile.id = o.operationProfile.workstore.operationProfileId" +
				" and o.operationProfile.workstore = :workstore" +
				" and o.printerType in :printerType")
				.setParameter("workstore", workstore)
				.setParameter("printerType", printerTypeList)
				.getResultList();
		if ( !operationWorkstationList.isEmpty() ){
			operationWorkstation = operationWorkstationList.get(0);
		}
		
		return operationWorkstation;
	}
	
	private int getRandomListIndex(List list){
		Random generator = new Random();
		return generator.nextInt(list.size());
	}
	
	private List<ItemLocation> sortItemLocationListByProcessTypePriority(List<ItemLocation> itemLocationList, boolean isUrgent, boolean isPrepack){
		Map<ItemLocation, Integer> itemLocationMap = new HashMap<ItemLocation, Integer>();
		for ( ItemLocation itemLocation : itemLocationList ) {
			itemLocationMap.put(itemLocation, processTypePriority(itemLocation.getMachine().getType(), isUrgent, isPrepack));
		}
		List<Map.Entry<ItemLocation, Integer>> sortList = new ArrayList<Map.Entry<ItemLocation, Integer>>(itemLocationMap.entrySet());
		
		Collections.sort(sortList, new MachineTypeComparator());
		
		List<ItemLocation> resultList = new ArrayList<ItemLocation>();
		for ( Map.Entry<ItemLocation, Integer> item : sortList ) {
			resultList.add(item.getKey());
		}
		
		return resultList;
	}
	
	public static class MachineTypeComparator implements Comparator<Map.Entry<ItemLocation, Integer>>, Serializable {

		private static final long serialVersionUID = 1L;
		
		@Override
		public int compare(Map.Entry<ItemLocation, Integer> item1, Map.Entry<ItemLocation, Integer> item2) {
			return (item1.getValue().compareTo(item2.getValue()));
		}		
	}

	private int processTypePriority(MachineType type, boolean urgent, boolean isPrepack) {
		int result = 3;
		if (urgent || isPrepack) {
			switch (type) {
				case Manual:
					result = 1;
					break;
				case ScriptPro:
					result = 2;
					break;
				case BakerCell:
					result = 3;
					break;
				default:
					result = 1;
			}
		} else {
			switch (type) {
				case ScriptPro:
					result = 1;
					break;
				case BakerCell:
					result = 2;
					break;
				case Manual:
					result = 3;
					break;
				default:
					result = 3;
			}
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	private List<OperationWorkstation> retrieveOperationWorkstatinListByWorkstation(Workstation inWorkstation) {
		return em.createQuery(
				"select o from OperationWorkstation o" + // 20120307 index check : OperationWorkstation.workstation : FK_OPERATION_WORKSTATION_02
				" where o.operationProfile.id = o.operationProfile.workstore.operationProfileId" +
				" and o.operationProfile.workstore = :workstore" +
				" and o.workstation = :workstation")
				.setParameter("workstore", workstore)
				.setParameter("workstation", inWorkstation)
				.getResultList();
	}
	
	public PrinterSelect retrievePrinterSelectOnLocalWorkstation(){
		PrinterSelect printerSelect = new PrinterSelect();
		printerSelect.setDefaultPrinterWorkstation(getDefaultPrinterWorkstation());
		List<OperationWorkstation> operationWorkstationList = retrieveOperationWorkstatinListByWorkstation(workstation);
		
		if ( !operationWorkstationList.isEmpty() ) {
			OperationWorkstation operationWorkstation = operationWorkstationList.get(0);
			if ( operationWorkstation.getPrinterConnectionType() == PrinterConnectionType.No ) {
				printerSelect.setPrinterWorkstation(getRedirectPrintStation(operationWorkstation, false));
			} else {
				printerSelect.setPrinterWorkstation(operationWorkstation.getWorkstation());
			}
		}
		
		return printerSelect;
	}
	
	private void sendBakerCellSocketMessage(DispOrderItem dispOrderItem, String bakerCellNum, String hostName){
		logger.info("Send message to BakerCell itemCode = #0, bakerCellNum = #1, portNum = #2", dispOrderItem.getPharmOrderItem().getItemCode(), bakerCellNum, MACHINE_BAKERCELL_PORT.get());
		BakerCellMessageContent bakerCellMessageContent = new BakerCellMessageContent();
		bakerCellMessageContent.setBakerCellNum(StringUtils.trimToEmpty(bakerCellNum));
		bakerCellMessageContent.setHostname(hostName);
		bakerCellMessageContent.setPortNum(MACHINE_BAKERCELL_PORT.get());
		bakerCellMessageContent.setIssueQty(dispOrderItem.getDispQty().toString());
		bakerCellMessageContent.setItemCode(dispOrderItem.getPharmOrderItem().getItemCode());
		bakerCellMessageContent.setPatName(dispOrderItem.getDispOrder().getPharmOrder().getMedOrder().getPatient().getName());
		bakerCellMessageContent.setTicketNum(dispOrderItem.getDispOrder().getTicket().getTicketNum());
		
		try {
			machineServiceProxy.sendBakerCellSocketMessage(bakerCellMessageContent);
		} catch (Exception e) {
			logger.error("Fail to send message to BakerCell #0", e);
		}
	}
	
	private void sendScriptProSocketMessage(DispOrderItem dispOrderItem, String binNum, String hostName, PrintLang printLang){		
		Patient patient = dispOrderItem.getDispOrder().getPharmOrder().getMedOrder().getPatient();
		ScriptProMessageContent scriptProMessageContent = new ScriptProMessageContent();
		scriptProMessageContent.setScriptProAction(ScriptProAction.New);
		scriptProMessageContent.setHostname(hostName);
		scriptProMessageContent.setPortNum(MACHINE_SCRIPTPRO_PORT.get());
		scriptProMessageContent.setHospCode(dispOrderItem.getDispOrder().getWorkstore().getHospCode());
		scriptProMessageContent.setWorkstoreCode(dispOrderItem.getDispOrder().getWorkstore().getWorkstoreCode());
		scriptProMessageContent.setLangType(getLangType(printLang, patient.getCcCode()));
		scriptProMessageContent.setItemNum(dispOrderItem.getItemNum().toString());
		scriptProMessageContent.setTicketNum(dispOrderItem.getDispOrder().getTicket().getTicketNum());
		scriptProMessageContent.setPrintType(ScriptProPrintType.New);
		scriptProMessageContent.setPatCatCode(StringUtils.trimToEmpty(dispOrderItem.getDispOrder().getPatCatCode()));
		scriptProMessageContent.setUtf8PatientNameFlag("UTF-8".equals(MACHINE_SCRIPTPRO_MESSAGE_ENCODING.get()));
	
		if (dispOrderItem.getPharmOrderItem().getRegimen().getType() == RegimenType.StepUpDown)
		{
			scriptProMessageContent.setRegimentType("S");
		}
		else if (dispOrderItem.getPharmOrderItem().getRegimen().getDoseGroupList().get(0).getDoseList().size() > 1)
		{
			scriptProMessageContent.setRegimentType("M");
		}
		else
		{
			scriptProMessageContent.setRegimentType("N");
		}
				
		scriptProMessageContent.setBinNum(binNum);
		scriptProMessageContent.setUpdateDate(new Date());

		scriptProMessageContent.setItemCode(dispOrderItem.getPharmOrderItem().getItemCode());
		scriptProMessageContent.setTradeName(StringUtils.trimToEmpty(dispOrderItem.getPharmOrderItem().getMedOrderItem().getTradeName()));
		scriptProMessageContent.setFormCode(StringUtils.trimToEmpty(dispOrderItem.getPharmOrderItem().getFormCode()));
		
		
		if ( printLang == PrintLang.Eng ) {
			scriptProMessageContent.setHospName(LABEL_DISPLABEL_HOSPITAL_NAMEENG.get());
			scriptProMessageContent.setPatName(patient.getName());	
		} else {
			scriptProMessageContent.setHospName(LABEL_DISPLABEL_HOSPITAL_NAMECHI.get());
			if ( StringUtils.isNotBlank(patient.getNameChi()) ) {
				scriptProMessageContent.setPatName(StringUtils.trimToEmpty(patient.getNameChi()));
				scriptProMessageContent.setCcCode(patient.getCcCode());
			} else {
				scriptProMessageContent.setPatName(patient.getName());	
			}
		}

		DispLabel dispLabel = (DispLabel)dispOrderItem.getBaseLabel();
		PrintOption printOption = new PrintOption();
		printOption.setPrintLang(printLang);
		if (LABEL_DISPLABEL_LARGELAYOUT_ENABLED.get(Boolean.FALSE)) {
			printOption.setPrintType(PrintType.LargeLayout);
		} else if (LABEL_DISPLABEL_LARGEFONT_ENABLED.get(Boolean.FALSE)) {
			printOption.setPrintType(PrintType.LargeFont);
		} else {
			printOption.setPrintType(PrintType.Normal);
		}
		dispLabel.setPrintOption(printOption);
		
		scriptProMessageContent.setRequireDeltaChangeIndFlag(MACHINE_SCRIPTPRO_MESSAGE_DELTACHANGEINDICATOR_REQUIRED.get(Boolean.FALSE));
 
		scriptProMessageContent.setDeltaChangeType(dispLabel.getDeltaChangeType());
		
		scriptProMessageContent.setInstruction(getScripProLabelInstructionText(dispLabel.getInstructionText(), dispLabel.getPrintOption().getPrintLang()));
		
		int i=1;
	    for (Warning warning : dispLabel.getWarningList()) {
	    	if (warning.getLang().equals(printLang.getDataValue()) && warning.getLineList() != null) {
	    		for (String warnText : warning.getLineList() ) {
		    		try {
						PropertyUtils.setProperty(scriptProMessageContent, "warnDesc"+i, warnText);
						i++;
					} catch (IllegalAccessException e) {
					} catch (InvocationTargetException e) {
					} catch (NoSuchMethodException e) {
					}
	    		}
	    		break;
	    	}
	    }
	    
	    for ( int j=i;j<=4;j++){
	    	try {
				PropertyUtils.setProperty(scriptProMessageContent, "warnDesc"+j, "");
				j++;
			} catch (IllegalAccessException e) {
			} catch (InvocationTargetException e) {
			} catch (NoSuchMethodException e) {
			}
	    }
		
		scriptProMessageContent.setDispDate(sdf.format(dispOrderItem.getDispOrder().getDispDate()));	
		
		if ( dispOrderItem.getDispOrder().getPharmOrder().getMedCase() != null ) {
			DispOrder dispOrder = dispOrderItem.getDispOrder();
			MedCase medCase = dispOrderItem.getDispOrder().getPharmOrder().getMedCase();
			scriptProMessageContent.setCaseNum(StringUtils.trimToEmpty(medCase.getCaseNum()));
			scriptProMessageContent.setPasWard(StringUtils.trimToEmpty(dispOrder.getWardCode()));
			scriptProMessageContent.setPasSpecCode(StringUtils.trimToEmpty(dispOrder.getSpecCode()));
			scriptProMessageContent.setPasBedNum(StringUtils.trimToEmpty(medCase.getPasBedNum()));
		}

		scriptProMessageContent.setDoctorCode(StringUtils.trimToEmpty(dispOrderItem.getDispOrder().getPharmOrder().getDoctorCode()));
		scriptProMessageContent.setUserCode(identity.getCredentials().getUsername());
		scriptProMessageContent.setDispQty(dispOrderItem.getDispQty().toString());
		scriptProMessageContent.setDispDosageUnit(StringUtils.trimToEmpty(dispOrderItem.getPharmOrderItem().getBaseUnit()));
		scriptProMessageContent.setFdnFlag(
				getFdnFlag(dispOrderItem.getDispOrder().getTicket().getOrderType(),
						dispOrderItem.getPharmOrderItem().getItemCode()));

		if ( LABEL_DISPLABEL_DISPBARCODE_VISIBLE.get() ) {
			scriptProMessageContent.setPrintBarcodeFlag("Y");		
		} else {
			scriptProMessageContent.setPrintBarcodeFlag("N");
		}
		if ( LABEL_DISPLABEL_INSTRUCTION_VISIBLE.get() ) {
			scriptProMessageContent.setPrintInstructionFlag("Y");		
		} else {
			scriptProMessageContent.setPrintInstructionFlag("N");
		}
		
		if ( dispOrderItem.getDispOrder().getPharmOrder().getMedOrder().getPrescType() == MedOrderPrescType.In &&
				LABEL_MPDISPLABEL_HKID_VISIBLE.get()) {
			scriptProMessageContent.setHkid(dispOrderItem.getDispOrder().getPharmOrder().getMedOrder().getPatient().getHkid());
		} else {
			scriptProMessageContent.setHkid("");
		}
		
		if ( dispOrderItem.getDispOrder().getPharmOrder().getMedOrder().getPrescType() == MedOrderPrescType.In &&
				LABEL_MPDISPLABEL_WARDRTNBARCODE_VISIBLE.get()) {  
			scriptProMessageContent.setPrintWardBarcodeFlag("Y");
		} else {
			scriptProMessageContent.setPrintWardBarcodeFlag("N");
		}
		
		PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
		Map<String,String> fdnMappingMap = refTableManager.getFdnMappingMapByPharmOrderItemList(
				dispOrderItem.getDispOrder().getPharmOrder().getPharmOrderItemList(), OrderType.OutPatient, workstore);

		if (pharmOrderItem.getDrugName() != null) {			
			scriptProMessageContent.setDrugName(DmDrug.buildFullDrugDesc(
				labelManager.retreiveDisplayDrugName(fdnMappingMap, pharmOrderItem),
				pharmOrderItem.getFormCode(),
				pharmOrderItem.getFormLabelDesc(),
				pharmOrderItem.getStrength(),
				pharmOrderItem.getVolumeText()));
		}

		try {
			logger.info("sendScriptProSocketMessage itemCode = #0, binNum = #1, hostName = #2, portNum = #3, langType = #4", 
														dispOrderItem.getPharmOrderItem().getItemCode(), 
														binNum, 
														hostName, 
														MACHINE_SCRIPTPRO_PORT.get(), 
														scriptProMessageContent.getLangType());
			machineServiceProxy.sendScriptProSocketMessage(scriptProMessageContent);
		} catch (Exception e) {
			logger.error("Fail to send message to ScriptPro #0", e);
		}
	}
	
	private String getLangType(PrintLang printLang, List<String> ccCode){
		if ( printLang == PrintLang.Chi ) {
			for ( String str : ccCode ) {
				if ( str.contains("S") ) {
					return MachinePrintLang.SimplifiedChi.getShortFormValue();
				}
			}
		}
		return printLang.getShortFormValue();
	}
	
	@SuppressWarnings("unchecked")
	private String getFdnFlag(OrderType orderType, String itemCode){
		List<FdnMapping> fdnMappingList = em.createQuery(
				"select o from FdnMapping o" + // 20120312 index check : FdnMapping.workstore,orderType,itemCode : UI_FDN_MAPPING_01
				" where o.workstore = :workstore" +
				" and o.orderType = :orderType" +
				" and o.itemCode = :itemCode")
				.setParameter("workstore", workstore)
				.setParameter("orderType", orderType)
				.setParameter("itemCode", itemCode)
				.getResultList();
		if (fdnMappingList.size() == 0) {
			return "N";
		} else {
			return "Y";
		}
	}
	
	private String getScripProLabelInstructionText(String instructionText, PrintLang printLang){
		StringBuffer sb = new StringBuffer();
		for (String str : instructionText.split("\n") ) {
			if (printLang == PrintLang.Eng) {
				if (sb.length() > 0) {
					sb.append(' ');
				}
				sb.append(StringUtils.trimToEmpty(str));
			}
			else {
				sb.append(str);	
			}
		}
		return sb.toString();
	}
	
	public PrinterSelect retrieveDefaultPrinterSelectForCouponReminder(boolean isUrgentFlag){
		PrinterSelect printerSelect = new PrinterSelect();

		OperationWorkstation operationWorkstationDefaultPrinter = null;
		if( isUrgentFlag ){
			operationWorkstationDefaultPrinter = getPrinterOperationWorkstation(PrinterType.Urgent);
		}else{
			operationWorkstationDefaultPrinter = getPrinterOperationWorkstation(PrinterType.Default);			
		}
		printerSelect.setDefaultPrinterWorkstation(operationWorkstationDefaultPrinter.getWorkstation());
		printerSelect.setPrinterWorkstation(operationWorkstationDefaultPrinter.getWorkstation());
		return printerSelect;
	}
	
	public List<DispOrderItem> updateBaseLabelBinNumForReprint(List<DispOrderItem> dispOrderItemList){
		
		OperationProfile activeProfile = refTableManager.retrieveActiveProfile(workstore);
		List<String> itemCodeList = this.getItemCodeListByDispOrderItemList(dispOrderItemList);
		Map<String, MachineType> binNumMap = this.getBinNumListByDispOrderItemList(dispOrderItemList);

		OperationWorkstation fastQueueOperationWorkstation = getPrinterOperationWorkstation(PrinterType.Fast);
		PickStation fastQueuePickStation = getFastQueuePickingStation(fastQueueOperationWorkstation);		
		
		List<ItemLocation> itemLocationFullList = this.retrieveItemLocationByItemCodeList(itemCodeList,activeProfile);
		Map<String, ItemLocation> defaultFastQueueItemLocationMap = this.retrieveDefFastQueueItemLocationByItemCodeList(itemCodeList, activeProfile, fastQueuePickStation);

		List<Prepack> prepackFullList = this.retrievePrepackList(itemCodeList);
		
		for ( DispOrderItem dispOrderItem : dispOrderItemList ) {
			
			if( dispOrderItem.getPrintFlag() ){
				if ( StringUtils.isBlank(dispOrderItem.getBinNum()) ) {
					continue;
				}
				
				MachineType machineType = binNumMap.get(dispOrderItem.getBinNum());
				if ( machineType != null && machineType == MachineType.Manual ) {
					continue;
				}	
			}

			ItemLocation defaultFastQueueItemLocation = defaultFastQueueItemLocationMap.get(dispOrderItem.getPharmOrderItem().getItemCode());
			Map<ItemLocation, Boolean> itemLocationResultMap = getLabelItemLocation(itemLocationFullList, 
																					prepackFullList, 
																					dispOrderItem, 
																					dispOrderItem.getDispOrder().getPrintLang(), 
																					activeProfile, 
																					Arrays.asList(MachineType.Manual),
																					fastQueuePickStation,
																					false);

			if ( !itemLocationResultMap.isEmpty() ) {
				Map.Entry<ItemLocation, Boolean> entry = itemLocationResultMap.entrySet().iterator().next();
				dispOrderItem.getBaseLabel().setBinNum(entry.getKey().getBinNum());
				
				//for handle unexpected printing exception, fill up the missing binNum and printFlag
				//no label item should be filtered by reprint front end
				if( !dispOrderItem.getPrintFlag() ){
					dispOrderItem.setPrintFlag(true);
					dispOrderItem.replaceBinNum( entry.getKey().getBinNum() );	
				}
			}else if( defaultFastQueueItemLocation != null ){
				dispOrderItem.getBaseLabel().setBinNum(defaultFastQueueItemLocation.getBinNum());
				
				//for handle unexpected printing exception, fill up the missing binNum and printFlag
				//no label item should be filtered by reprint front end
				if( !dispOrderItem.getPrintFlag() ){
					dispOrderItem.setPrintFlag(true);
					dispOrderItem.replaceBinNum( defaultFastQueueItemLocation.getBinNum() );	
				}
			}
			
		}
		return dispOrderItemList;
	}
	
	@SuppressWarnings("unchecked")
	private Map<String, MachineType> getBinNumListByDispOrderItemList(List<DispOrderItem> dispOrderItemList) {
		HashMap<String, MachineType> binNumMap = new HashMap<String, MachineType>();
		Set<String> binNumSet = new HashSet<String>();
		
		for ( DispOrderItem dispOrderItem : dispOrderItemList ) {
			if ( StringUtils.isNotBlank(dispOrderItem.getBinNum()) ) { 
				binNumSet.add(dispOrderItem.getBinNum());
			}
		}
		
		if ( !binNumSet.isEmpty() ){
			List<ItemLocation> itemLocationList = em.createQuery(
					"select o from ItemLocation o" + // 20120303 index check : ItemLocation.workstore,binNum : FK_ITEM_LOCATION_03
					" where o.binNum in :binNumSet" +
					" and o.workstore = :workstore")
					.setParameter("binNumSet", binNumSet)
					.setParameter("workstore", workstore)
					.getResultList();
			
			for (ItemLocation itemLocation : itemLocationList){
				binNumMap.put(itemLocation.getBinNum(), itemLocation.getMachine().getType());
			}
		}
		return binNumMap;
	}
	
	private boolean isFastQueueTicket(Date ticketDate, String ticketNum) {
		return ticketManager.isFastQueueTicket(ticketDate, ticketNum);
	}
	
	private Workstation getFastQueueDefaultPrinterWorkstation() {
		Workstation fastQueuePrinterWorkstation = null;
		OperationWorkstation fastQueueOw = getPrinterOperationWorkstation(PrinterType.Fast);
		if ( fastQueueOw != null ) {
			fastQueuePrinterWorkstation = fastQueueOw.getWorkstation();
		}
		return fastQueuePrinterWorkstation;
	}
	
	private PickStation getFastQueuePickingStation(OperationWorkstation fastQueueOw) {
		PickStation fastQueuePickingStation = null;
		if ( fastQueueOw != null && fastQueueOw.getMachine() != null ) {
			fastQueuePickingStation = fastQueueOw.getMachine().getPickStation();
		}
		return fastQueuePickingStation;
	}
	
	private Map<String, OperationWorkstation> getFastQueueOperationWorkstationMap(OperationProfile activeProfile){
		Map<String, OperationWorkstation> fastQueueOwMap = new HashMap<String, OperationWorkstation>();
		
		for ( OperationWorkstation operationWorkstation : activeProfile.getOperationWorkstationList() ) {
			if( Boolean.TRUE == operationWorkstation.getFastQueuePrintFlag() ){
				fastQueueOwMap.put(operationWorkstation.getWorkstation().getWorkstationCode(), operationWorkstation);
			}
		}
		
		return fastQueueOwMap;
	}
	
	private Boolean isFastQueuePrinterAvailable(Workstation printerWorkstation, Map<String, OperationWorkstation> fqOwMap){
		
		OperationWorkstation printerOperationWorkstation = null;
		
		if( fqOwMap.get(printerWorkstation.getWorkstationCode()) != null ){
			printerOperationWorkstation = fqOwMap.get(printerWorkstation.getWorkstationCode());
		}
		
		return (printerOperationWorkstation != null);
	}

	private List<MachineType> getItemLocationMachineTypeList(boolean isFastQueueTicket, boolean isUrgentFlag, boolean defFastQueuePrinterEnabled, boolean normalPrintToUrgentPrint, boolean fastPrintToUrgentPrint){
		List<MachineType> supportMachineList = MachineType.Manual_BakerCell_ScriptPro;
		
		//Set MachineType
		if( isFastQueueTicket ){
			if( defFastQueuePrinterEnabled ){
				supportMachineList = Arrays.asList(MachineType.Manual);
			}else if( isUrgentFlag ){
				if( fastPrintToUrgentPrint ){
					supportMachineList = Arrays.asList(MachineType.Manual);
				}else{
					supportMachineList = MachineType.Manual_BakerCell;
				}
			}else{
				supportMachineList = MachineType.Manual_BakerCell;
			}
		}else if( isUrgentFlag ){
			if( normalPrintToUrgentPrint ){
				supportMachineList = Arrays.asList(MachineType.Manual);
			}else{
				supportMachineList = MachineType.Manual_BakerCell;
			}
		}
		return supportMachineList;
	}
	
	@Remove
	public void destroy(){
		if ( operationWorkstationMap != null ) {
			operationWorkstationMap = null;
		}
	}
}
