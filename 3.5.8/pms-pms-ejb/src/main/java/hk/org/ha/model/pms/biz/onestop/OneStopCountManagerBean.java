package hk.org.ha.model.pms.biz.onestop;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import hk.org.ha.model.pms.biz.reftable.WorkstoreKeyable;
import hk.org.ha.fmk.pms.cache.CacheResult;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.onestop.MoStatusCount;

import static hk.org.ha.model.pms.prop.Prop.ONESTOP_UNVETORDERCOUNT_DURATION_LIMIT;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.joda.time.DateTime;

@AutoCreate
@Stateless
@Name("oneStopCountManager")
@MeasureCalls
public class OneStopCountManagerBean implements OneStopCountManagerLocal {
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
		
	private static final Map<Workstore,MoStatusCount> moStatusCountMap = new HashMap<Workstore,MoStatusCount>();
	
	private final static String WORKSTORE = "workstore";
	private final static String STATUS = "status";
	private final static String REVOKE_FLAG = "revokeFlag";
	private final static String ADMIN_STATUS = "adminStatus";
	private final static String DOC_TYPE = "docType";
	private final static String ORDER_TYPE = "orderType";
	private final static String ORDER_DATE = "orderDate";
	private final static String DISP_DATE = "dispDate";
		
	public static MoStatusCount getMoStatusCountByWorkstore(Workstore workstore) {
		synchronized (OneStopCountManagerBean.class) {
			MoStatusCount moStatusCount = moStatusCountMap.get(workstore);		
			if (moStatusCount == null) {
				moStatusCount = new MoStatusCount();
				moStatusCount.setLastCallingTime(new Date());
				moStatusCountMap.put(workstore, moStatusCount);
			}
			return moStatusCount;
		}
	}
	
	@SuppressWarnings("unchecked")
	@CacheResult(timeout="30000", keyable=WorkstoreKeyable.class)
	public void retrieveUnvetAndSuspendCount(Workstore workstore, int suspendCountDurationLimit) 
	{
		Date suspendOrderBeginDate = getDefaultSuspendOrderBeginDate(suspendCountDurationLimit).toDate();
		int unvetCount = ((Long) em.createQuery(
				"select count(o) from MedOrder o " + // 20140925 index check : MedOrder.workstore,status,orderType,orderDate : I_MED_ORDER_06
				" where o.workstore = :workstore" +
				" and o.orderType = :orderType" +
				" and o.orderDate >= :orderDate" +
				" and o.status in :status" +
				" and o.docType = :docType")
				.setParameter(WORKSTORE, workstore)
				.setParameter(ORDER_TYPE, OrderType.OutPatient)
				.setParameter(ORDER_DATE, getDefaultUnvetOrderBeginDate().toDate(), TemporalType.DATE)
				.setParameter(STATUS, MedOrderStatus.Outstanding_Withhold_PreVet)
				.setParameter(DOC_TYPE, MedOrderDocType.Normal)
				.getSingleResult()).intValue();
		
		List<DispOrder> dispOrderList = (List<DispOrder>) em.createQuery(
				"select o from DispOrder o" + // 20160809 index check : DispOrder.workstore,adminStatus,status,dispDate : I_DISP_ORDER_14
				" where o.workstore = :workstore" + 
				" and o.adminStatus = :adminStatus" + 				
				" and o.status in :status" + 
				" and o.revokeFlag = :revokeFlag" +
				" and o.dispDate >= :dispDate" +
				" and o.ticket.orderType = :orderType")
				.setParameter(WORKSTORE, workstore)
				.setParameter(ADMIN_STATUS, DispOrderAdminStatus.Suspended)			
				.setParameter(STATUS, DispOrderStatus.Not_Deleted_SysDeleted)
				.setParameter(REVOKE_FLAG, Boolean.FALSE)
				.setParameter(DISP_DATE, suspendOrderBeginDate, TemporalType.DATE)
				.setParameter(ORDER_TYPE, OrderType.OutPatient)
				.setHint(QueryHints.FETCH, "o.ticket")
				.getResultList();
		
		for (Iterator<DispOrder> itr = dispOrderList.iterator(); itr.hasNext();)
		{
			DispOrder dispOrder = (DispOrder) itr.next();
			
			if (dispOrder.getTicket().getTicketDate().before(suspendOrderBeginDate))
			{
				itr.remove();
			}
		}
				
		synchronized (OneStopCountManagerBean.class) {
			MoStatusCount moStatusCount = getMoStatusCountByWorkstore(workstore);
			moStatusCount.setUnvetCount(unvetCount);
			moStatusCount.setSuspendCount(dispOrderList.size());
			moStatusCount.setLastCallingTime(new Date());
		}
		
		logger.debug("retrieveUnvetAndSuspendCount: Hospital=#0, Workstore=#1, Unvet count=#2, Suspend count=#3 ",
				 workstore.getHospCode(),
				 workstore.getWorkstoreCode(),
				 unvetCount,
				 dispOrderList.size());
	}
	
	private DateTime getDefaultUnvetOrderBeginDate() {
		return new DateTime().minusDays(ONESTOP_UNVETORDERCOUNT_DURATION_LIMIT.get());
	}
	
	public DateTime getDefaultSuspendOrderBeginDate(int suspendCountDurationLimit) {
		return new DateTime().minusDays(suspendCountDurationLimit).toDateMidnight().toDateTime();
	}
	
	public Boolean isCounterTimeOut(MoStatusCount moStatusCount) {
		long timeDiff = 0L;
		
		if (moStatusCount != null) {
			timeDiff = new Date().getTime() - moStatusCount.getLastCallingTime().getTime();
		}
		
		if (moStatusCount == null || timeDiff > 30000L) {
			return true;
		}
		
		return false;
	}
}