package hk.org.ha.model.pms.biz.drug;
import hk.org.ha.model.pms.vo.rx.Site;

import java.util.List;

import javax.ejb.Local;

@Local
public interface SiteListServiceLocal {
	List<Site> retrieveSiteListByFormCode(String formCode);
	void destroy();
}
