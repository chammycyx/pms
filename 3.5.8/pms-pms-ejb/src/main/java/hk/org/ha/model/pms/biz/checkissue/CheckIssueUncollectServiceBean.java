package hk.org.ha.model.pms.biz.checkissue;

import static hk.org.ha.model.pms.prop.Prop.CHECKISSUE_UNCOLLECT_ORDER_SUSPENDCODE;
import static hk.org.ha.model.pms.prop.Prop.MACHINE_IQDS_ENABLED;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.onestop.SuspendPrescManagerLocal;
import hk.org.ha.model.pms.biz.onestop.UncollectPrescManagerLocal;
import hk.org.ha.model.pms.biz.sys.SystemMessageManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderDayEndStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.PharmOrderAdminStatus;
import hk.org.ha.model.pms.udt.machine.IqdsAction;
import hk.org.ha.model.pms.vo.checkissue.CheckIssueUncollectSummary;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("checkIssueUncollectService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CheckIssueUncollectServiceBean implements CheckIssueUncollectServiceLocal {

	@In
	private AuditLogger auditLogger;
	
	@In
	private Workstore workstore;
	
	@In
	private IqdsSenderLocal iqdsSender;
	
	@In
	private CheckIssueManagerLocal checkIssueManager;
	
	@In
	private UncollectPrescManagerLocal uncollectPrescManager;
	
	@In
	private SuspendPrescManagerLocal suspendPrescManager;
	
	@In
	private SystemMessageManagerLocal systemMessageManager;
	
	private DateFormat df = new SimpleDateFormat("yyyy/MM/dd"); 
	private DateFormat auditLogDf = new SimpleDateFormat("yyyy-MM-dd"); 
	private final static String CHECK_ISSUE = "checkIssue";

	@Override
	public CheckIssueUncollectSummary uncollectPrescription(Date ticketDate, String ticketNum, Integer issueWindowNum) {
		CheckIssueUncollectSummary checkIssueUncollectSummary = new CheckIssueUncollectSummary();
		checkIssueUncollectSummary.setTicketDate(ticketDate);
		checkIssueUncollectSummary.setTicketNum(ticketNum);
		
		Pair<List<DispOrder>, String> dispOrderContent = checkIssueManager.retrieveDispOrderByTicket(ticketDate, ticketNum, workstore, false);
		List<DispOrder> dispOrderList = dispOrderContent.getFirst();
		if ( !dispOrderList.isEmpty() ) {
			DispOrder dispOrder = dispOrderList.get(0);
			
			if ( dispOrder.getStatus() == DispOrderStatus.Deleted ) {
				//Invalid ticket number
				checkIssueUncollectSummary.setMessage(systemMessageManager.retrieveMessageDesc("0002")); 
				return checkIssueUncollectSummary;
			}
			
			MedOrderDocType docType = dispOrder.getPharmOrder().getMedOrder().getDocType();
			
			if ( dispOrder.getRevokeFlag() == Boolean.TRUE ) {
				//Cannot mark uncollected : Prescription is already marked as uncollected
				checkIssueUncollectSummary.setMessage(systemMessageManager.retrieveMessageDesc("0771"));
				return checkIssueUncollectSummary;
			}
			
			boolean uncollectSuccess = false;
			boolean sendToIqds = false;
			if ( MACHINE_IQDS_ENABLED.get() && 
					(issueWindowNum > 0 && df.format(new Date()).equals(df.format(dispOrder.getTicket().getTicketDate())))) {
				sendToIqds = true;
			}

			if ( docType == MedOrderDocType.Normal && dispOrder.getRefillFlag() == Boolean.FALSE ) {
				uncollectSuccess = uncollectMoeOrder(dispOrder, checkIssueUncollectSummary);
			} else {
				//manual order && refill order
				uncollectSuccess = uncollectManualOrder(dispOrder, checkIssueUncollectSummary, !sendToIqds);
			}
			
			if ( uncollectSuccess && sendToIqds ) {
				iqdsSender.sendTicketToIqds(workstore.getHospCode(), workstore.getWorkstoreCode(), dispOrder.getTicket().getTicketNum(), issueWindowNum, IqdsAction.SendTicketNum);
				checkIssueUncollectSummary.setIssueWindowNum(issueWindowNum);
			}
		} else {
			//Invalid ticket number
			checkIssueUncollectSummary.setMessage(systemMessageManager.retrieveMessageDesc("0002")); 
		}
		
		return checkIssueUncollectSummary;
	}
	
	private boolean uncollectMoeOrder(DispOrder dispOrder, CheckIssueUncollectSummary checkIssueUncollectSummary){
		if ( dispOrder.getPharmOrder().getAdminStatus() == PharmOrderAdminStatus.AllowModify ) {
			//Cannot mark uncollected : MOE order is marked as allow modify
			checkIssueUncollectSummary.setMessage(systemMessageManager.retrieveMessageDesc("0769"));
			return false;
		}
		
		uncollectPrescManager.uncollectPrescription(dispOrder, CHECKISSUE_UNCOLLECT_ORDER_SUSPENDCODE.get(), CHECK_ISSUE);
		if ( dispOrder.getRefrigItemFlag() == Boolean.TRUE ) {
			checkIssueUncollectSummary.setRefrigItemFlag(true);
		}
		setUncollectedSuccessMsg(dispOrder, checkIssueUncollectSummary);
		auditLogger.log("#0774", auditLogDf.format(dispOrder.getTicketDate()), dispOrder.getTicketNum(), dispOrder.getId(), MedOrderDocType.Normal, CHECKISSUE_UNCOLLECT_ORDER_SUSPENDCODE.get());
		
		return true;
	}
	
	private boolean uncollectManualOrder(DispOrder dispOrder, CheckIssueUncollectSummary checkIssueUncollectSummary, boolean removeTicketNum){
		
		if ( dispOrder.getDayEndStatus() != DispOrderDayEndStatus.None ) {
			if ( dispOrder.getRefillFlag() == Boolean.TRUE ) {
				//Cannot mark uncollected : Refill coupon has been processed by dayend
				checkIssueUncollectSummary.setMessage(systemMessageManager.retrieveMessageDesc("0782"));
			} else {
				//Cannot mark uncollected : Manual prescription has been processed by dayend
				checkIssueUncollectSummary.setMessage(systemMessageManager.retrieveMessageDesc("0783"));
			}
			return false;
		}
		
		if ( dispOrder.getAdminStatus() == DispOrderAdminStatus.Suspended) {
			if ( dispOrder.getRefillFlag() == Boolean.TRUE ) {
				//Cannot mark uncollected : Refill coupon has already been marked as suspend
				checkIssueUncollectSummary.setMessage(systemMessageManager.retrieveMessageDesc("0784"));
			} else {
				//Cannot mark uncollected : Manual prescription has already been marked as suspend
				checkIssueUncollectSummary.setMessage(systemMessageManager.retrieveMessageDesc("0770"));
			}
			return false;
		}
		
		suspendPrescManager.suspendPrescription(dispOrder, CHECKISSUE_UNCOLLECT_ORDER_SUSPENDCODE.get(), removeTicketNum);
		if ( dispOrder.getRefrigItemFlag() == Boolean.TRUE ) {
			checkIssueUncollectSummary.setRefrigItemFlag(true);
		}
		setUncollectedSuccessMsg(dispOrder, checkIssueUncollectSummary);
		auditLogger.log("#0774", auditLogDf.format(dispOrder.getTicketDate()), dispOrder.getTicketNum(), dispOrder.getId(), MedOrderDocType.Manual, CHECKISSUE_UNCOLLECT_ORDER_SUSPENDCODE.get());
		
		return true;
	}
	
	private void setUncollectedSuccessMsg(DispOrder dispOrder, CheckIssueUncollectSummary checkIssueUncollectSummary){
		if ( checkIssueUncollectSummary.getRefrigItemFlag() ) {
			//Uncollected : Contains refrigerated item
			checkIssueUncollectSummary.setMessage(systemMessageManager.retrieveMessageDesc("0768"));
		} else {
			//Uncollected
			checkIssueUncollectSummary.setMessage(systemMessageManager.retrieveMessageDesc("0767"));
		}
	}

	@Remove
	public void destroy() {
	}
}
