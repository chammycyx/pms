package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.persistence.reftable.PickStation;

import java.util.List;

import javax.ejb.Local;


import java.util.Collection;

@Local
public interface PickStationListManagerLocal {

	List<PickStation> retrievePickStationList();

	void updatePickStationList(Collection<PickStation> updatePickStationList);
}
 