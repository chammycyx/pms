package hk.org.ha.model.pms.vo.reftable;

import hk.org.ha.model.pms.dms.persistence.PmsQtyConversion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class PmsQtyConversionItem implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String workstoreCode;
	
	private String hospCode;
	
	private boolean sameWorkstoreGroup; 
	
	private List<PmsQtyConversion> pmsQtyConversionList;

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setPmsQtyConversionList(List<PmsQtyConversion> pmsQtyConversionList) {
		this.pmsQtyConversionList = pmsQtyConversionList;
	}

	public List<PmsQtyConversion> getPmsQtyConversionList() {
		if ( pmsQtyConversionList == null ) {
			pmsQtyConversionList = new ArrayList<PmsQtyConversion>();
		}
		return pmsQtyConversionList;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setSameWorkstoreGroup(boolean sameWorkstoreGroup) {
		this.sameWorkstoreGroup = sameWorkstoreGroup;
	}

	public boolean isSameWorkstoreGroup() {
		return sameWorkstoreGroup;
	}
	
}