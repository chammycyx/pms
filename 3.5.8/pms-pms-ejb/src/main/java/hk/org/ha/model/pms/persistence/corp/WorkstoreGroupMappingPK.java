package hk.org.ha.model.pms.persistence.corp;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class WorkstoreGroupMappingPK implements Serializable {

	private static final long serialVersionUID = 1L;

	private String workstoreGroupCode;
	private String patHospCode;
	
	public WorkstoreGroupMappingPK() {
	}

	public WorkstoreGroupMappingPK(String workstoreGroupCode, String patHospCode) {
		this.workstoreGroupCode = workstoreGroupCode;
		this.patHospCode = patHospCode;
	}

	public String getWorkstoreGroupCode() {
		return workstoreGroupCode;
	}

	public void setWorkstoreGroupCode(String workstoreGroupCode) {
		this.workstoreGroupCode = workstoreGroupCode;
	}

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}
	
	public boolean equals(Object other) {
		return EqualsBuilder.reflectionEquals(this, other);
	}
	
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
}
