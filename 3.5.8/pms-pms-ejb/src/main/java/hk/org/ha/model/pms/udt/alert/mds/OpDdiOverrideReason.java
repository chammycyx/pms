package hk.org.ha.model.pms.udt.alert.mds;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum OpDdiOverrideReason implements StringValuedEnum {

	NoAlternative("NA", "No other alternative available"),
	Discontinued ("DI", "The on hand drug has been discontinued"),
	NoProblem    ("NP", "Patient is taking this drug combination without problem");

	private final String dataValue;
    private final String displayValue;
    
    OpDdiOverrideReason(final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}

	public static OpDdiOverrideReason dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(OpDdiOverrideReason.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<OpDdiOverrideReason> {
		
		private static final long serialVersionUID = 1L;

		public Class<OpDdiOverrideReason> getEnumClass() {
    		return OpDdiOverrideReason.class;
    	}
    }
}
