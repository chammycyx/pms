package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.persistence.reftable.RefillQtyAdj;

import java.util.Collection;
import java.util.List;

import javax.ejb.Local;

@Local
public interface RefillQtyAdjListServiceLocal {

	List<RefillQtyAdj> retrieveRefillQtyAdjList();
	
	void updateRefillQtyAdjList(Collection<RefillQtyAdj> updateRefillQtyAdjList, 
								Collection<RefillQtyAdj> delRefillQtyAdjList);
	
	void destroy();
	
}
