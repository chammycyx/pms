package hk.org.ha.model.pms.vo.report;

import hk.org.ha.model.pms.udt.alert.AlertProfileStatus;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class MedSummaryRptHdr implements Serializable {

	private static final long serialVersionUID = 1L;

	private String patName;
	
	private String patNameChi;
	
	private String gender;
	
	private String age;
	
	private String pasSpecCode;
	
	private String hkid;

	private Date orderDate;
	
	private Date dispDate;
	
	private String caseNum;
	
	private String ticketNum;
	
	private String drugAllergenDesc;
	
	private String adrAlertDesc;
	
	private String otherAllergyDesc;
	
	private String ehrAllergy;
	
	private String ehrAdr;
	
	private String pasSubSpecCode;
	
	private String hospName;
	
	private Boolean allergyServerAvailableFlag;
	
	private Boolean ehrAllergyException;
	
	private String patInforXmlBarCode;
	
	private Boolean barCodeEnable;
	
	private AlertProfileStatus alertProfileStatus;

	public String getAdrAlertDesc() {
		return adrAlertDesc;
	}

	public void setAdrAlertDesc(String adrAlertDesc) {
		this.adrAlertDesc = adrAlertDesc;
	}

	public String getOtherAllergyDesc() {
		return otherAllergyDesc;
	}

	public void setOtherAllergyDesc(String otherAllergyDesc) {
		this.otherAllergyDesc = otherAllergyDesc;
	}

	public String getEhrAllergy() {
		return ehrAllergy;
	}

	public void setEhrAllergy(String ehrAllergy) {
		this.ehrAllergy = ehrAllergy;
	}

	public String getEhrAdr() {
		return ehrAdr;
	}

	public void setEhrAdr(String ehrAdr) {
		this.ehrAdr = ehrAdr;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Date getDispDate() {
		return dispDate;
	}

	public void setDispDate(Date dispDate) {
		this.dispDate = dispDate;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getPatName() {
		return patName;
	}

	public String getDrugAllergenDesc() {
		return drugAllergenDesc;
	}

	public void setDrugAllergenDesc(String drugAllergenDesc) {
		this.drugAllergenDesc = drugAllergenDesc;
	}

	public String getPasSubSpecCode() {
		return pasSubSpecCode;
	}

	public void setPasSubSpecCode(String pasSubSpecCode) {
		this.pasSubSpecCode = pasSubSpecCode;
	}

	public void setPasSpecCode(String pasSpecCode) {
		this.pasSpecCode = pasSpecCode;
	}

	public String getPasSpecCode() {
		return pasSpecCode;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getGender() {
		return gender;
	}

	public void setHospName(String hospName) {
		this.hospName = hospName;
	}

	public String getHospName() {
		return hospName;
	}

	public void setPatNameChi(String patNameChi) {
		this.patNameChi = patNameChi;
	}

	public String getPatNameChi() {
		return patNameChi;
	}

	public void setAllergyServerAvailableFlag(Boolean allergyServerAvailableFlag) {
		this.allergyServerAvailableFlag = allergyServerAvailableFlag;
	}

	public Boolean getAllergyServerAvailableFlag() {
		if ( allergyServerAvailableFlag == null ) {
			allergyServerAvailableFlag = false;
		}
		return allergyServerAvailableFlag;
	}
	
	public void setEhrAllergyException(Boolean ehrAllergyException) {
		this.ehrAllergyException = ehrAllergyException;
	}

	public Boolean getEhrAllergyException() {
		return ehrAllergyException;
	}

	public void setPatInforXmlBarCode(String patInforXmlBarCode) {
		this.patInforXmlBarCode = patInforXmlBarCode;
	}

	public String getPatInforXmlBarCode() {
		return patInforXmlBarCode;
	}

	public void setBarCodeEnable(Boolean barCodeEnable) {
		this.barCodeEnable = barCodeEnable;
	}

	public Boolean getBarCodeEnable() {
		if ( barCodeEnable == null ) {
			barCodeEnable = false;
		}
		return barCodeEnable;
	}

	public void setAlertProfileStatus(AlertProfileStatus alertProfileStatus) {
		this.alertProfileStatus = alertProfileStatus;
	}

	public AlertProfileStatus getAlertProfileStatus() {
		return alertProfileStatus;
	}
}
