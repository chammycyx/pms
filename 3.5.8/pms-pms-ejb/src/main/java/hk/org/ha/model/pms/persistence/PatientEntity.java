package hk.org.ha.model.pms.persistence;

import hk.org.ha.fmk.pms.util.StringCollectionConverter;
import hk.org.ha.model.pms.udt.Gender;
import hk.org.ha.model.pms.vo.ehr.EhrPatient;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedProperty;

@MappedSuperclass
@ExternalizedBean(type=DefaultExternalizer.class)
public abstract class PatientEntity extends BaseEntity {

	private static final long serialVersionUID = 1L;

	private static final String DAY_UNIT = "day(s)";
	
	private static final String MONTH_UNIT = "month(s)";
	
	private static final String YEAR_UNIT = "year(s)";
	
	@Column(name = "PAT_KEY", length = 8)
	private String patKey;

	@Column(name = "HKID", length = 12)
	private String hkid;
	
	@Column(name = "DOB")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dob;
	
	@Converter(name = "PatientEntity.sex", converterClass = Gender.Converter.class)
    @Convert("PatientEntity.sex")
	@Column(name = "SEX", length = 1)
	private Gender sex;
	
	@Column(name = "NAME", length = 48)
	private String name;
	
	// 6 x 3 for UTF-8
	@Column(name = "NAME_CHI", length = 18)
	private String nameChi;
	
	@Column(name = "PHONE", length = 10)
	private String phone;
	
	@Column(name = "OFFICE_PHONE", length = 10)
	private String officePhone;
	
	@Column(name = "OFFICE_PHONE_EXT", length = 4)
	private String officePhoneExt;
	
	@Column(name = "OTHER_PHONE", length = 10)
	private String otherPhone;
	
	@Column(name = "OTHER_PHONE_EXT", length = 4)
	private String otherPhoneExt;

	@Column(name = "ADDRESS", length = 435)
	private String address;

	@Column(name = "DEATH_FLAG", nullable = false)
	private Boolean deathFlag;
	
	@Column(name = "OTHER_DOC", length = 12)
	private String otherDoc;
	
	@Converter(name = "PatientEntity.ccCode", converterClass = StringCollectionConverter.class)
	@Convert("PatientEntity.ccCode")
	@Column(name = "CC_CODE", length = 100)
	private List<String> ccCode;
	
	@Transient
	private EhrPatient ehrPatient;
	
	public PatientEntity() {
		deathFlag = Boolean.FALSE;
	}
	
	public String getPatKey() {
		return patKey;
	}

	public void setPatKey(String patKey) {
		this.patKey = patKey;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public Date getDob() {
		if (dob == null) {
			return null;
		}
		else {
			return new Date(dob.getTime());
		}
	}

	public void setDob(Date dob) {
		if (dob == null) {
			this.dob = null;
		}
		else {
			this.dob = new Date(dob.getTime());
		}
	}

	public Gender getSex() {
		return sex;
	}

	public void setSex(Gender sex) {
		this.sex = sex;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameChi() {
		return nameChi;
	}

	public void setNameChi(String nameChi) {
		this.nameChi = nameChi;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getOfficePhone() {
		return officePhone;
	}

	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}

	public String getOfficePhoneExt() {
		return officePhoneExt;
	}

	public void setOfficePhoneExt(String officePhoneExt) {
		this.officePhoneExt = officePhoneExt;
	}

	public String getOtherPhone() {
		return otherPhone;
	}

	public void setOtherPhone(String otherPhone) {
		this.otherPhone = otherPhone;
	}

	public String getOtherPhoneExt() {
		return otherPhoneExt;
	}

	public void setOtherPhoneExt(String otherPhoneExt) {
		this.otherPhoneExt = otherPhoneExt;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Boolean getDeathFlag() {
		return deathFlag;
	}
	
	public void setDeathFlag(Boolean deathFlag) {
		this.deathFlag = deathFlag;
	}

	public String getOtherDoc() {
		return otherDoc;
	}

	public void setOtherDoc(String otherDoc) {
		this.otherDoc = otherDoc;
	}
	
	public List<String> getCcCode() {
		if (ccCode == null) {
			return new ArrayList<String>();
		}
		else {
			return new ArrayList<String>(ccCode);
		}
	}

	public void setCcCode(List<String> ccCode) {
		if (ccCode == null) {
			this.ccCode = new ArrayList<String>();
		}
		else {
			this.ccCode = new ArrayList<String>(ccCode);
		}
	}

	public EhrPatient getEhrPatient() {
		return ehrPatient;
	}

	public void setEhrPatient(EhrPatient ehrPatient) {
		this.ehrPatient = ehrPatient;
	}

	@ExternalizedProperty
	public String getAge() {
		String dateUnit = getDateUnit();
		if (dateUnit == null) {
			return null;
		}

		Calendar today = Calendar.getInstance();
		clearTimestamp(today);
		
		Calendar dobNormal = Calendar.getInstance();
		dobNormal.setTime(dob); 
		
		Integer yearDiff = today.get(Calendar.YEAR) - dobNormal.get(Calendar.YEAR);
		Integer monthDiff = today.get(Calendar.MONTH) - dobNormal.get(Calendar.MONTH);
		Integer dayDiff = today.get(Calendar.DATE) - dobNormal.get(Calendar.DATE);

		if (dayDiff < 0) {
			monthDiff = monthDiff - 1;
		}
		
		if (monthDiff < 0) {
			yearDiff = yearDiff - 1;
			monthDiff = monthDiff + 12;
		}

		if (DAY_UNIT.equals(dateUnit)) {
			return Long.toString((today.getTimeInMillis() - dobNormal.getTimeInMillis()) / (24 * 60 * 60 * 1000));
		}
		else if (MONTH_UNIT.equals(dateUnit)) {
			return Integer.toString((yearDiff * 12) + monthDiff);
		}
		else if (YEAR_UNIT.equals(dateUnit)) {
			return Integer.toString(yearDiff);
		}
		else {
			return null;
		}
	}
	
	@ExternalizedProperty
	public String getDateUnit() {
		if (dob == null) {
			return null;
		}
		
		Calendar dobAfter3m = Calendar.getInstance();
		dobAfter3m.setTime(dob);
		dobAfter3m.add(Calendar.MONTH, 3);
		clearTimestamp(dobAfter3m);

		Calendar today = Calendar.getInstance();
		clearTimestamp(today);
		
		Calendar dobAfter3y = Calendar.getInstance();
		dobAfter3y.setTime(dob);
		dobAfter3y.add(Calendar.YEAR, 3);
		clearTimestamp(dobAfter3y);
		
		if (dobAfter3m.after(today) || dobAfter3m.equals(today)) {
			return DAY_UNIT;
		} else if (dobAfter3m.before(today) && dobAfter3y.after(today)) {
			return MONTH_UNIT;
		} else {
			return YEAR_UNIT;
		}
	}
	
	private void clearTimestamp(Calendar calendar) {
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
	}
	
	public void updateByPatientEntity(PatientEntity patientEntity) {
		patKey    = patientEntity.getPatKey();
		hkid      = patientEntity.getHkid();
		dob       = patientEntity.getDob();
		sex       = patientEntity.getSex();
		name      = patientEntity.getName();
		nameChi   = patientEntity.getNameChi();
		phone     = patientEntity.getPhone();
		address   = patientEntity.getAddress();
		deathFlag = patientEntity.getDeathFlag();
		otherDoc  = patientEntity.getOtherDoc();
		ccCode    = patientEntity.getCcCode();
	}
}
