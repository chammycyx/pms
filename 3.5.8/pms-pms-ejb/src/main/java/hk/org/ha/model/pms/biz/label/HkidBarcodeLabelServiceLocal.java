package hk.org.ha.model.pms.biz.label;

import hk.org.ha.model.pms.vo.label.HkidBarcodeLabel;

import javax.ejb.Local;

@Local
public interface HkidBarcodeLabelServiceLocal {  

	void retrieveHkidBarcodeLabel(String name);
	
	void updateHkidBarcodeLabel(HkidBarcodeLabel hkidBarcodeLabel);
	
	void printHkidBarcodeLabel(HkidBarcodeLabel hkidBarcodeLabel);
	
	void removeHkidBarcodeLabel(String name);

	void destroy(); 
}