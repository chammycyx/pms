package hk.org.ha.model.pms.persistence.reftable;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
import hk.org.ha.model.pms.vo.reftable.FollowUpWarningItem;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.Customizer;
import org.jboss.seam.security.Identity;

@Entity
@Table(name = "FOLLOW_UP_WARNING")
@Customizer(AuditCustomizer.class)
public class FollowUpWarning extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "followUpWarningSeq")
	@SequenceGenerator(name = "followUpWarningSeq", sequenceName = "SQ_FOLLOW_UP_WARNING", initialValue = 100000000)
	private Long id;
	
	@Column(name = "ITEM_CODE", nullable = false, length = 6)
	private String itemCode;
	
	@Column(name = "MODIFY_DATE", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date modifyDate;
	
	@ManyToOne
	@JoinColumn(name = "WORKSTORE_GROUP_CODE", nullable = false)
	private WorkstoreGroup workstoreGroup;

	@Transient
	private DmDrug dmDrug;	
	
	@Transient
	private boolean markDelete;	
	
	@Transient
	private List<FollowUpWarningItem> followUpWarningItemList;	

	@PostLoad
	public void postLoad() {
		if (itemCode != null && Identity.instance().isLoggedIn()) {
			dmDrug = DmDrugCacher.instance().getDmDrug(itemCode);
		}
	}		

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public WorkstoreGroup getWorkstoreGroup() {
		return workstoreGroup;
	}

	public void setWorkstoreGroup(WorkstoreGroup workstoreGroup) {
		this.workstoreGroup = workstoreGroup;
	}

	public void setDmDrug(DmDrug dmDrug) {
		this.dmDrug = dmDrug;
	}

	public DmDrug getDmDrug() {
		return dmDrug;
	}

	public void setMarkDelete(boolean markDelete) {
		this.markDelete = markDelete;
	}

	public boolean isMarkDelete() {
		return markDelete;
	}

	public void setFollowUpWarningItemList(List<FollowUpWarningItem> followUpWarningItemList) {
		this.followUpWarningItemList = followUpWarningItemList;
	}

	public List<FollowUpWarningItem> getFollowUpWarningItemList() {
		if ( followUpWarningItemList == null ){
			followUpWarningItemList = new ArrayList<FollowUpWarningItem>();
		}
		return followUpWarningItemList;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}
	
}
