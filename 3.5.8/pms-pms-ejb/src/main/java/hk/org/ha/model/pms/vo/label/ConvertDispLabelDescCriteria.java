package hk.org.ha.model.pms.vo.label;

import java.io.Serializable;

public class ConvertDispLabelDescCriteria implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String regimen;
	
	private String freqCode;
	
	private String freqValue;
	
	private String supplFreqCode;
	
	private String chiSiteDesc;
	
	private String engSiteDesc;
	
	private String chiConvFreqDesc;
	
	private String engConvFreqDesc;
	
	private String chiConvSupplFreqDesc;
	
	private String engConvSupplFreqDesc;
	
	private String chiConvSiteDesc;
	
	private String engConvSiteDesc;

	public String getRegimen() {
		return regimen;
	}

	public void setRegimen(String regimen) {
		this.regimen = regimen;
	}

	public String getFreqCode() {
		return freqCode;
	}

	public void setFreqCode(String freqCode) {
		this.freqCode = freqCode;
	}

	public String getFreqValue() {
		return freqValue;
	}

	public void setFreqValue(String freqValue) {
		this.freqValue = freqValue;
	}

	public String getSupplFreqCode() {
		return supplFreqCode;
	}

	public void setSupplFreqCode(String supplFreqCode) {
		this.supplFreqCode = supplFreqCode;
	}

	public String getChiSiteDesc() {
		return chiSiteDesc;
	}

	public void setChiSiteDesc(String chiSiteDesc) {
		this.chiSiteDesc = chiSiteDesc;
	}

	public String getEngSiteDesc() {
		return engSiteDesc;
	}

	public void setEngSiteDesc(String engSiteDesc) {
		this.engSiteDesc = engSiteDesc;
	}

	public String getChiConvFreqDesc() {
		return chiConvFreqDesc;
	}

	public void setChiConvFreqDesc(String chiConvFreqDesc) {
		this.chiConvFreqDesc = chiConvFreqDesc;
	}

	public String getEngConvFreqDesc() {
		return engConvFreqDesc;
	}

	public void setEngConvFreqDesc(String engConvFreqDesc) {
		this.engConvFreqDesc = engConvFreqDesc;
	}

	public String getChiConvSupplFreqDesc() {
		return chiConvSupplFreqDesc;
	}

	public void setChiConvSupplFreqDesc(String chiConvSupplFreqDesc) {
		this.chiConvSupplFreqDesc = chiConvSupplFreqDesc;
	}

	public String getEngConvSupplFreqDesc() {
		return engConvSupplFreqDesc;
	}

	public void setEngConvSupplFreqDesc(String engConvSupplFreqDesc) {
		this.engConvSupplFreqDesc = engConvSupplFreqDesc;
	}

	public String getChiConvSiteDesc() {
		return chiConvSiteDesc;
	}

	public void setChiConvSiteDesc(String chiConvSiteDesc) {
		this.chiConvSiteDesc = chiConvSiteDesc;
	}

	public String getEngConvSiteDesc() {
		return engConvSiteDesc;
	}

	public void setEngConvSiteDesc(String engConvSiteDesc) {
		this.engConvSiteDesc = engConvSiteDesc;
	}
	
	
}
