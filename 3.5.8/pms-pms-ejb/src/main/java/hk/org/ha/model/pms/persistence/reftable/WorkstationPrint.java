package hk.org.ha.model.pms.persistence.reftable;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.reftable.PrintType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "WORKSTATION_PRINT")
public class WorkstationPrint extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "workstationPrintSeq")
	@SequenceGenerator(name = "workstationPrintSeq", sequenceName = "SQ_WORKSTATION_PRINT", initialValue = 100000000)
	private Long id;	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "WORKSTATION_ID", nullable = false)
	private Workstation workstation;

	@Column(name = "PRINT_DOC_TYPE", nullable = false, length = 2)
	private PrintDocType docType;
	
	@Column(name = "PRINT_TYPE", nullable = false, length = 1)
	private PrintType type;
	
	@Column(name = "VALUE", length = 100)
	private String value;
	
	public WorkstationPrint() {
	}
	
	public WorkstationPrint(PrintDocType docType, PrintType type) {
		this.docType = docType;
		this.type = type;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Workstation getWorkstation() {
		return workstation;
	}

	public void setWorkstation(Workstation workstation) {
		this.workstation = workstation;
	}

	public PrintDocType getDocType() {
		return docType;
	}

	public void setDocType(PrintDocType docType) {
		this.docType = docType;
	}

	public PrintType getType() {
		return type;
	}

	public void setType(PrintType type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
