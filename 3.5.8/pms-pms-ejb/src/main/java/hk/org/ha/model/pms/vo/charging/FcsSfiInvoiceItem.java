package hk.org.ha.model.pms.vo.charging;

import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.InvoiceItem;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class FcsSfiInvoiceItem implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer orgMoeItemNum;
	private Integer orgDispItemNum;
	private Integer dispItemNum;
	private BigDecimal amount;
	private BigDecimal handleAmount;
	private BigDecimal handleExemptAmount;
	private String handleExemptCode;
	private String handleExemptUser;
	private BigDecimal markupAmount;
	private ActionStatus actionStatus;
	private Date updateDate;
	private String updateUser;
	private String fmStatus;
	private String itemCode;
	private Boolean oncologyFlag;
	private Boolean lifestyleFlag;
	private String fullDrugDesc;
	private BigDecimal dispQty;
	private String baseUnit;
	
	public FcsSfiInvoiceItem() {
		// default constructor
	}
	
	public FcsSfiInvoiceItem(InvoiceItem invoiceItem) {

		DispOrderItem dispOrderItem = invoiceItem.getDispOrderItem();
		PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
		MedOrderItem medOrderItem = pharmOrderItem.getMedOrderItem();
		
		orgMoeItemNum = medOrderItem.getOrgItemNum();
		orgDispItemNum = pharmOrderItem.getOrgItemNum();
		dispItemNum = pharmOrderItem.getItemNum();
		amount = invoiceItem.getAmount();
		handleAmount = invoiceItem.getHandleAmount().subtract(invoiceItem.getHandleExemptAmount());
		handleExemptAmount = invoiceItem.getHandleExemptAmount();
		handleExemptCode = invoiceItem.getHandleExemptCode();
		handleExemptUser = invoiceItem.getHandleExemptUser();
		markupAmount = invoiceItem.getMarkupAmount();
		actionStatus = pharmOrderItem.getActionStatus();
		oncologyFlag = dispOrderItem.getOncologyFlag();
		lifestyleFlag = dispOrderItem.getLifestyleFlag();
		
		updateDate = invoiceItem.getUpdateDate();
		updateUser = invoiceItem.getUpdateUser();
		fmStatus = medOrderItem.getFmStatus();
		itemCode = pharmOrderItem.getItemCode();
		fullDrugDesc = pharmOrderItem.getFullDrugDesc();
		if( invoiceItem.getChargeQty() != null ){
			dispQty = invoiceItem.getChargeQty();
		}else{
			dispQty = dispOrderItem.getDispQty();
		}
		baseUnit = pharmOrderItem.getBaseUnit();
	}
	
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getHandleAmount() {
		return handleAmount;
	}
	public void setHandleAmount(BigDecimal handleAmount) {
		this.handleAmount = handleAmount;
	}
	public BigDecimal getHandleExemptAmount() {
		return handleExemptAmount;
	}
	public void setHandleExemptAmount(BigDecimal handleExemptAmount) {
		this.handleExemptAmount = handleExemptAmount;
	}
	public String getHandleExemptCode() {
		return handleExemptCode;
	}
	public void setHandleExemptCode(String handleExemptCode) {
		this.handleExemptCode = handleExemptCode;
	}
	public String getHandleExemptUser() {
		return handleExemptUser;
	}
	public void setHandleExemptUser(String handleExemptUser) {
		this.handleExemptUser = handleExemptUser;
	}
	public BigDecimal getMarkupAmount() {
		return markupAmount;
	}
	public void setMarkupAmount(BigDecimal markupAmount) {
		this.markupAmount = markupAmount;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	public ActionStatus getActionStatus() {
		return actionStatus;
	}
	public void setActionStatus(ActionStatus actionStatus) {
		this.actionStatus = actionStatus;
	}

	public String getFmStatus() {
		return fmStatus;
	}

	public void setFmStatus(String fmStatus) {
		this.fmStatus = fmStatus;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public Boolean getOncologyFlag() {
		return oncologyFlag;
	}

	public void setOncologyFlag(Boolean oncologyFlag) {
		this.oncologyFlag = oncologyFlag;
	}

	public Boolean getLifestyleFlag() {
		return lifestyleFlag;
	}

	public void setLifestyleFlag(Boolean lifestyleFlag) {
		this.lifestyleFlag = lifestyleFlag;
	}

	public Integer getOrgMoeItemNum() {
		return orgMoeItemNum;
	}

	public void setOrgMoeItemNum(Integer orgMoeItemNum) {
		this.orgMoeItemNum = orgMoeItemNum;
	}

	public Integer getOrgDispItemNum() {
		return orgDispItemNum;
	}

	public void setOrgDispItemNum(Integer orgDispItemNum) {
		this.orgDispItemNum = orgDispItemNum;
	}

	public Integer getDispItemNum() {
		return dispItemNum;
	}

	public void setDispItemNum(Integer dispItemNum) {
		this.dispItemNum = dispItemNum;
	}

	public void setFullDrugDesc(String fullDrugDesc) {
		this.fullDrugDesc = fullDrugDesc;
	}

	public String getFullDrugDesc() {
		return fullDrugDesc;
	}

	public void setDispQty(BigDecimal dispQty) {
		this.dispQty = dispQty;
	}

	public BigDecimal getDispQty() {
		return dispQty;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public String getBaseUnit() {
		return baseUnit;
	}
}
