package hk.org.ha.model.pms.vo.enquiry;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class OrderPendingSuspendLogEnq implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Date actionDate;
	
	private String caseNum;
	
	private String ward;
	
	private String orderDesc;
	
	private String actionByName;
	
	private String actionByUser;
	
	private String action;
	
	private String reason;
	
	private String supplReason;
	
	private String remark;
	
	private String doctor;
	
	private String doctorUser;

	private String msgByDoctor;
	
	private String prescHosp;
	
	private String hosp;
	
	private String workstore;
	
	private Number itemNum;
	
	private String suspendPendCode;

	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}

	public Date getActionDate() {
		return actionDate;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setActionByName(String actionByName) {
		this.actionByName = actionByName;
	}

	public String getActionByName() {
		return actionByName;
	}
	
	public void setActionByUser(String actionByUser) {
		this.actionByUser = actionByUser;
	}

	public String getActionByUser() {
		return actionByUser;
	}

	public void setOrderDesc(String orderDesc) {
		this.orderDesc = orderDesc;
	}

	public String getOrderDesc() {
		return orderDesc;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getAction() {
		return action;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReason() {
		return reason;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getRemark() {
		return remark;
	}

	public void setDoctor(String doctor) {
		this.doctor = doctor;
	}

	public String getDoctor() {
		return doctor;
	}
	
	public void setDoctorUser(String doctorUser) {
		this.doctorUser = doctorUser;
	}

	public String getDoctorUser() {
		return doctorUser;
	}
	
	public void setMsgByDoctor(String msgByDoctor) {
		this.msgByDoctor = msgByDoctor;
	}

	public String getMsgByDoctor() {
		return msgByDoctor;
	}
	
	public void setPrescHosp(String prescHosp) {
		this.prescHosp = prescHosp;
	}

	public String getPrescHosp() {
		return prescHosp;
	}
	
	public void setHosp(String hosp) {
		this.hosp = hosp;
	}

	public String getHosp() {
		return hosp;
	}
	public void setWorkstore(String workstore) {
		this.workstore = workstore;
	}

	public String getWorkstore() {
		return workstore;
	}

	public void setItemNum(Number itemNum) {
		this.itemNum = itemNum;
	}

	public Number getItemNum() {
		return itemNum;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}

	public String getWard() {
		return ward;
	}

	public void setSupplReason(String supplReason) {
		this.supplReason = supplReason;
	}

	public String getSupplReason() {
		return supplReason;
	}

	public void setSuspendPendCode(String suspendPendCode) {
		this.suspendPendCode = suspendPendCode;
	}

	public String getSuspendPendCode() {
		return suspendPendCode;
	}

}
