package hk.org.ha.model.pms.udt.vetting;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DmCapdVoucherMapType implements StringValuedEnum {

	BaseUnit("BU", "Base Unit"),
	VolumeUnit("VU", "Volume Unit"),
	Ingredient("IG", "Ingredient"),
	CalciumStrengthUnit("SU", "Calcium Strength Unit"),
	ConnectSystem("CS", "Connect System");
	
    private final String dataValue;
    private final String displayValue;

    DmCapdVoucherMapType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    

	public static DmCapdVoucherMapType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DmCapdVoucherMapType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<DmCapdVoucherMapType> {
		
		private static final long serialVersionUID = 1L;

		@Override
    	public Class<DmCapdVoucherMapType> getEnumClass() {
    		return DmCapdVoucherMapType.class;
    	}
    }
}
