package hk.org.ha.model.pms.biz.printing;

import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.vo.printing.PrintLabelDetail;
import hk.org.ha.model.pms.vo.printing.PrinterSelect;

import java.util.List;

import javax.ejb.Local;

@Local
public interface PrintLabelManagerLocal {
	
	void retrievePrintLabelDetailOnEdsMode(List<DispOrderItem> dispOrderItemList, PrintLang printLang);
	
	PrintLabelDetail retrievePrintLabelDetailOnNonPeakMode(List<DispOrderItem> dispOrderItemList, PrintLang printLang);
	
	void retrievePrintLabelDetailOnPmsMode(List<DispOrderItem> dispOrderItemList, PrintLang printLang);

	PrinterSelect retrievePrinterSelectOnLocalWorkstation();
	
	PrinterSelect retrieveDefaultPrinterSelectForCouponReminder(boolean isUrgentFlag);
	
	List<DispOrderItem> updateBaseLabelBinNumForReprint(List<DispOrderItem> dispOrderItemList);
	
	void destroy(); 
	
}
