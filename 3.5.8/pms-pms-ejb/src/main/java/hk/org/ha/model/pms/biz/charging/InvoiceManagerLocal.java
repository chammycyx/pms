package hk.org.ha.model.pms.biz.charging;

import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.medprofile.PurchaseRequest;
import hk.org.ha.model.pms.vo.charging.SaveInvoiceListInfo;
import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;
import java.util.List;

import javax.ejb.Local;

@Local
public interface InvoiceManagerLocal {
	
	void retrieveInvoice(Long invoiceId);
	
	SaveInvoiceListInfo createInvoiceListFromDispOrder(OrderViewInfo orderViewInfo, DispOrder dispOrder, List<Invoice> voidInvoiceList);
	
	Invoice createInvoiceFromPurchaseRequestForPrint(DispOrderItem sfiDispOrderItem, PurchaseRequest purchaseRequest);
	
	Invoice createInvoiceFromPurchaseRequestForEnq(DispOrderItem sfiDispOrderItem, PurchaseRequest purchaseRequest );
	
	Invoice createInvoiceForPurchaseRequest(List<DispOrderItem> sfiDispOrderItemList, PurchaseRequest purchaseRequest);
	
	Invoice createInvoiceForMpSfiInvoice(List<DispOrderItem> sfiDispOrderItemList, String chargeOrderNum);
	
	boolean isExceptionDrugExistExcludeZeroAmount( Object clearanceObject );	
	boolean isExceptionDrugExist( Object clearanceObject );
	void updateInvoicePrintFlag(Invoice invoice);
}
