package hk.org.ha.model.pms.vo.machine;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import hk.org.ha.model.pms.udt.machine.ScriptProPrintType;
import hk.org.ha.model.pms.udt.machine.ScriptProAction;

public class ScriptProMessageContent implements Serializable {

	private static final long serialVersionUID = 1L;

	private String hostname;
	
	private int portNum;
	
	private String hospCode;
	
	private String hospName;
	
	private String workstoreCode;
	
	private String langType;
	
	private String itemNum;
	
	private String ticketNum;
	
	private ScriptProPrintType printType;
	
	private String itemCode;
	
	private String drugName;
	
	private String tradeName;
	
	private String instruction;
	
	private String formCode;
	
	private String dispDate;
	
	private String patName;
	 
	private String hkid;
	
	private String caseNum;
	
	private String pasWard; 
	
	private String pasSpecCode;
	
	private String pasBedNum;
	
	private String patCatCode;
	
	private String doctorCode;
	
	private String userCode;
	
	private String regimentType;
	
	private String dispQty;
	
	private String dispDosageUnit;
	
	private String warnDesc1;
	
	private String warnDesc2;
	
	private String warnDesc3;
	
	private String warnDesc4;
	
	private String fdnFlag;
	
	private String binNum;
	
	private List<String> ccCode;
	
	private String deltaChangeType;
	
	private boolean utf8PatientNameFlag;
	
	private Boolean requireDeltaChangeIndFlag;

	public String getPatCatCode() {
		return patCatCode;
	}

	public void setPatCatCode(String patCatCode) {
		this.patCatCode = patCatCode;
	}

	public String getRegimentType() {
		return regimentType;
	}

	public void setRegimentType(String regimentType) {
		this.regimentType = regimentType;
	}

	public String getBinNum() {
		return binNum;
	}

	public void setBinNum(String binNum) {
		this.binNum = binNum;
	}

	public List<String> getCcCode() {
		return ccCode;
	}

	public void setCcCode(List<String> ccCode) {
		this.ccCode = ccCode;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	private String printInstructionFlag;
	
	private String printBarcodeFlag;
	
	private String printWardBarcodeFlag;
	
	private Date updateDate;
	
	private ScriptProAction scriptProAction;

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public int getPortNum() {
		return portNum;
	}

	public void setPortNum(int portNum) {
		this.portNum = portNum;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getHospName() {
		return hospName;
	}

	public void setHospName(String hospName) {
		this.hospName = hospName;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public String getLangType() {
		return langType;
	}

	public void setLangType(String langType) {
		this.langType = langType;
	}

	public String getItemNum() {
		return itemNum;
	}

	public void setItemNum(String itemNum) {
		this.itemNum = itemNum;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public ScriptProPrintType getPrintType() {
		return printType;
	}

	public void setPrintType(ScriptProPrintType printType) {
		this.printType = printType;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getDrugName() {
		return drugName;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public String getTradeName() {
		return tradeName;
	}

	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}

	public String getInstruction() {
		return instruction;
	}

	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}

	public String getDispDate() {
		return dispDate;
	}

	public void setDispDate(String dispDate) {
		this.dispDate = dispDate;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getPasWard() {
		return pasWard;
	}

	public void setPasWard(String pasWard) {
		this.pasWard = pasWard;
	}

	public String getPasSpecCode() {
		return pasSpecCode;
	}

	public void setPasSpecCode(String pasSpecCode) {
		this.pasSpecCode = pasSpecCode;
	}

	public String getPasBedNum() {
		return pasBedNum;
	}

	public void setPasBedNum(String pasBedNum) {
		this.pasBedNum = pasBedNum;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getDispQty() {
		return dispQty;
	}

	public void setDispQty(String dispQty) {
		this.dispQty = dispQty;
	}

	public String getDispDosageUnit() {
		return dispDosageUnit;
	}

	public void setDispDosageUnit(String dispDosageUnit) {
		this.dispDosageUnit = dispDosageUnit;
	}

	public String getWarnDesc1() {
		return warnDesc1;
	}

	public void setWarnDesc1(String warnDesc1) {
		this.warnDesc1 = warnDesc1;
	}

	public String getWarnDesc2() {
		return warnDesc2;
	}

	public void setWarnDesc2(String warnDesc2) {
		this.warnDesc2 = warnDesc2;
	}

	public String getWarnDesc3() {
		return warnDesc3;
	}

	public void setWarnDesc3(String warnDesc3) {
		this.warnDesc3 = warnDesc3;
	}

	public String getWarnDesc4() {
		return warnDesc4;
	}

	public void setWarnDesc4(String warnDesc4) {
		this.warnDesc4 = warnDesc4;
	}

	public String getFdnFlag() {
		return fdnFlag;
	}

	public void setFdnFlag(String fdnFlag) {
		this.fdnFlag = fdnFlag;
	}

	public String getPrintInstructionFlag() {
		return printInstructionFlag;
	}

	public void setPrintInstructionFlag(String printInstructionFlag) {
		this.printInstructionFlag = printInstructionFlag;
	}

	public String getPrintBarcodeFlag() {
		return printBarcodeFlag;
	}

	public void setPrintBarcodeFlag(String printBarcodeFlag) {
		this.printBarcodeFlag = printBarcodeFlag;
	}

	public ScriptProAction getScriptProAction() {
		return scriptProAction;
	}

	public void setScriptProAction(ScriptProAction scriptProAction) {
		this.scriptProAction = scriptProAction;
	}

	public String getFormCode() {
		return formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	public void setPrintWardBarcodeFlag(String printWardBarcodeFlag) {
		this.printWardBarcodeFlag = printWardBarcodeFlag;
	}

	public String getPrintWardBarcodeFlag() {
		return printWardBarcodeFlag;
	}

	public boolean getUtf8PatientNameFlag() {
		return utf8PatientNameFlag;
	}

	public void setUtf8PatientNameFlag(boolean utf8PatientNameFlag) {
		this.utf8PatientNameFlag = utf8PatientNameFlag;
	}

	public String getDeltaChangeType() {
		return deltaChangeType;
	}

	public void setDeltaChangeType(String deltaChangeType) {
		this.deltaChangeType = deltaChangeType;
	}

	public Boolean getRequireDeltaChangeIndFlag() {
		return requireDeltaChangeIndFlag;
	}

	public void setRequireDeltaChangeIndFlag(Boolean requireDeltaChangeIndFlag) {
		this.requireDeltaChangeIndFlag = requireDeltaChangeIndFlag;
	}
}
