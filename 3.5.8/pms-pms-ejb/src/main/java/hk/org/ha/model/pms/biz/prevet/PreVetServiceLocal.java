package hk.org.ha.model.pms.biz.prevet;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.udt.prevet.PreVetMode;
import hk.org.ha.model.pms.vo.prevet.PreVetAuth;

import javax.ejb.Local;

@Local
public interface PreVetServiceLocal {
	
	String retrieveDischargeMedOrder(String orderNum, Patient patient, PreVetMode preVetMode, Long medOrderId);
	String preVetMedOrder(MedOrder medOrder, PreVetMode preVetMode, String userId, String userDisplayName);
	void releaseMedOrder(MedOrder medOrder, PreVetMode preVetMode);
	PreVetAuth preVetAuthnication(String userId, String password, String target);
	String removeOrder(String logonUser, String remarkText, MedOrder medOrder);
	void destroy();
}
