package hk.org.ha.model.pms.persistence;

import flexjson.JSON;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.jboss.seam.security.Identity;

@Entity
@Table(name = "INFO_LINK")
public class InfoLink {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "infoLinkSeq")
	@SequenceGenerator(name = "infoLinkSeq", sequenceName = "SQ_INFO_LINK", initialValue = 100000000)
	private Long id;
	
	@Column(name = "NAME", nullable = false, length = 100)
	private String name;
	
	@Column(name = "URL", nullable = false, length = 100)
	private String url;
	
	@Column(name = "SORT_SEQ", nullable = false, length = 10)
	private Long sortSeq;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public void setSortSeq(Long sortSeq) {
		this.sortSeq = sortSeq;
	}

	public Long getSortSeq() {
		return sortSeq;
	}

}
