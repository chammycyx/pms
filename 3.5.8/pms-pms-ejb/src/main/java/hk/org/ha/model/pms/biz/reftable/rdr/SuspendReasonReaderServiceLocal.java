package hk.org.ha.model.pms.biz.reftable.rdr;

import javax.ejb.Local;

@Local
public interface SuspendReasonReaderServiceLocal {

	void retrieveSuspendReasonList();

	void destroy();
	
}
