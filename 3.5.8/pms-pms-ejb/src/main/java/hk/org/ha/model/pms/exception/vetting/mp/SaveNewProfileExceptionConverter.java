package hk.org.ha.model.pms.exception.vetting.mp;

import java.util.Map;

import org.granite.messaging.service.ExceptionConverter;
import org.granite.messaging.service.ServiceException;

public class SaveNewProfileExceptionConverter implements ExceptionConverter {

	public static final String SAVE_NEW_PROFILE_EXCEPTION = "SaveNewProfileException";
	
	public boolean accepts(Throwable t, Throwable finalException) {
		return t.getClass().equals(SaveNewProfileException.class);
	}

	public ServiceException convert(Throwable t, String detail, Map<String, Object> extendedData) {		
		ServiceException se = new ServiceException(SAVE_NEW_PROFILE_EXCEPTION, t.getMessage(), detail, t);
		se.getExtendedData().putAll(extendedData);
		se.getExtendedData().put("type", ((SaveNewProfileException) t).getType());
		return se;
	}

}
