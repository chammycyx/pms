package hk.org.ha.model.pms.udt.disp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;
import hk.org.ha.model.pms.udt.medprofile.MedProfileOrderType;

public enum MpTrxType implements StringValuedEnum {

	// From MOE Ordering to PMS
	MpCreateOrderFirstItem("ND1", "Create Order for First Item", MpMessageType.NewOrder, MedProfileOrderType.Create, Boolean.TRUE),
	MpCreateOrderOtherItem("ND2", "Create Order for Other Item", MpMessageType.NewOrder, MedProfileOrderType.Add, Boolean.TRUE),
	MpModifyOrder("AD1", "Modify Order", MpMessageType.UpdateOrder, MedProfileOrderType.Modify, Boolean.TRUE),
	MpDeleteOrder("SU1", "Delete Order", MpMessageType.DeleteOrder, MedProfileOrderType.Delete, Boolean.TRUE),
	MpDiscontinueOrder("SU2", "Discontinue Order", MpMessageType.DiscontinueOrder, MedProfileOrderType.Discontinue, Boolean.TRUE),
	MpUpdatePregnancy("HU1", "Update Pregnancy Checking Indicator", MpMessageType.UpdatePregnancy, MedProfileOrderType.CheckPregnancy, Boolean.TRUE),
	MpAmendSchedule("RS1", "Amend Schedule", MpMessageType.AmendSchedule, MedProfileOrderType.AmendSchedule, Boolean.TRUE),
	MpCustomSchedule("RS2", "Custom Schedule", MpMessageType.CustomSchedule, MedProfileOrderType.CustomSchedule, Boolean.TRUE),
	MpMarkAnnotation("MA1", "MR Mark Annotation", MpMessageType.MarkAnnotation, MedProfileOrderType.MarkAnnotation, Boolean.TRUE),
	MpModifyMds("AD2", "Modify IPMOE Item for MDS Update", MpMessageType.ModifyMds, MedProfileOrderType.ModifyMds, Boolean.TRUE),
	MpModifyReviewDate("AD3", "Modify IPMOE Item for Review Date", MpMessageType.ModifyReviewDate, MedProfileOrderType.ModifyReviewDate, Boolean.TRUE),
	
	// From MOE Admin to PMS
	MpAddAdminRecord("NA1", "Add Admin Record", MpMessageType.AdminRecord, MedProfileOrderType.AddAdmin, Boolean.TRUE),
	MpModifyAdminRecord("AA1", "Modify Admin Record", MpMessageType.AdminRecord, MedProfileOrderType.ModifyAdmin, Boolean.TRUE),
	MpUrgentDispensing("UD1", "Create Request for Urgent Dispensing", MpMessageType.Urgent, MedProfileOrderType.Urgent, Boolean.TRUE),
	MpDrugReplenishment("RR1", "Create Request for Drug Replenishment", MpMessageType.Replenishment, MedProfileOrderType.Replenishment, Boolean.TRUE),
	MpOverrideOnHoldOrder("PO3", "Override On Hold Order Request", MpMessageType.PendOrder, MedProfileOrderType.OverridePending, Boolean.TRUE),
	MpCompleteAdmin("CA1", "Complete Admin", MpMessageType.CompleteAdmin, MedProfileOrderType.CompleteAdmin, Boolean.TRUE),
	MpAddWithholdRecord("WH1", "Add Withhold Record", MpMessageType.Withhold, MedProfileOrderType.AddWithhold, Boolean.TRUE),
	MpModifyWithholdRecord("WH2", "Modify Withhold Record", MpMessageType.Withhold, MedProfileOrderType.ModifyWithhold, Boolean.TRUE),
	MpEndWithholdRecord("WH3", "End Withhold Record", MpMessageType.Withhold, MedProfileOrderType.EndWithhold, Boolean.TRUE),

	// From PMS to MOE Ordering
	MpVetOrder("SU3", "Vet Order", MpMessageType.VetOrder, null, Boolean.FALSE),
	MpUpdateDispensing("SU5", "Update Dispensing", MpMessageType.UpdateDispensing, null, Boolean.FALSE),
	
	// From PMS to MOE Admin
	MpOnHoldOrder("PO1", "Pharmacy On Hold Order Request", MpMessageType.PendOrder, null, Boolean.FALSE),
	MpClearOnHoldStatus("PO2", "Pharmacy Clear Pending Status", MpMessageType.PendOrder, null, Boolean.FALSE),
	MpUrgentDispenseUpdate("UD2", "Urgent Dispense Update", MpMessageType.Urgent, null, Boolean.FALSE),
	MpDrugReplenishmentUpdate("RR2", "Drug Replenishment Update", MpMessageType.Replenishment, null, Boolean.FALSE),
	
	// For MedProfileTrxLog use
	MpSaveProfile("_SP", "Save Profile", null, null, Boolean.FALSE);
	
	public static final List<MpTrxType> MpVetOrder_MpUrgentDispenseUpdate = Arrays.asList(MpVetOrder, MpUrgentDispenseUpdate);
	
	public static final List<MpTrxType> MpVetOrder_MpUpdateDispensing = Arrays.asList(MpVetOrder, MpUpdateDispensing);
	
	private final String dataValue;
	private final String displayValue;
	private final MpMessageType messageType;
	private final MedProfileOrderType orderType;
	private final Boolean toPmsFlag;
	
	MpTrxType (final String dataValue, final String displayValue, final MpMessageType messageType, final MedProfileOrderType orderType, Boolean toPmsFlag) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
		this.messageType = messageType;
		this.orderType = orderType;
		this.toPmsFlag = toPmsFlag;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}

	public MpMessageType getMessageType() {
		return this.messageType;
	}
	
	public MedProfileOrderType getOrderType() {
		return this.orderType;
	}

	public Boolean getToPmsFlag() {
		return this.toPmsFlag;
	}
	
	public static MpTrxType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MpTrxType.class, dataValue);
	}

	public static List<MpTrxType> getCmsToPmsTrxType() {
		List<MpTrxType> resultList = new ArrayList<MpTrxType>();
		for (MpTrxType type:MpTrxType.values()) {
			if (type.getToPmsFlag()) {
				resultList.add(type);
			}
		}
		return resultList;
	}
	
	public static List<MpTrxType> getPmsToCmsTrxType() {
		List<MpTrxType> resultList = new ArrayList<MpTrxType>();
		for (MpTrxType type:MpTrxType.values()) {
			if (!type.getToPmsFlag()) {
				if (type != MpSaveProfile) {
					resultList.add(type);
				}
			}
		}
		return resultList;
	}
	
	public static class Converter extends StringValuedEnumConverter<MpTrxType> {

		private static final long serialVersionUID = 1L;

		public Class<MpTrxType> getEnumClass() {
    		return MpTrxType.class;
    	}
    }
}
