package hk.org.ha.model.pms.vo.enquiry;

import hk.org.ha.model.pms.udt.charging.ClearanceStatus;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class SfiInvoiceInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Long invoiceId;
	
	private String invoiceNum;
	
	private Date invoiceDate;
	
	private String invoiceStatus;
	
	private String orderNum;
	
	private String patName;
	
	private String hkid;
	
	private String caseNum;
	
	private String dispOrderStatus;
	
	private String pasPayCode;
	
	private String pasPatGroupCode;
	
	private String ticketNum;
	
	private Double totalAmount;
	
	private String forceProceedInvoiceNum;
	
	private String forceProceedReceiptNum;
	
	private String forceProceedReason;
	
	private String forceProceedFlag;
	
	private String forceProceedBy;
	
	private Double totalHandleExemptAmount;
	
	private String chargeAmount;
	
	private String receiptAmount;
	
	private String receiptNum;
	
	private String receiptMessage;

	private boolean allowReprint;
	
	private String patHospCode;
	
	private String displayForceProceedReason;
	
	//For IPSFI Invoice Enquiry
	private String deliveryItemStatus;
	
	private InvoiceStatus invoiceStatusForClearance;
	
	private Long purchaseReqId;
	
	public SfiInvoiceInfo() {
		setAllowReprint(true);
	}

	public Long getInvoiceId() {
		return invoiceId;
	}
	
	public void setInvoiceId(Long invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getInvoiceNum() {
		return invoiceNum;
	}

	public void setInvoiceNum(String invoiceNum) {
		this.invoiceNum = invoiceNum;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getInvoiceStatus() {
		return invoiceStatus;
	}

	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getDispOrderStatus() {
		return dispOrderStatus;
	}

	public void setDispOrderStatus(String dispOrderStatus) {
		this.dispOrderStatus = dispOrderStatus;
	}

	public String getPasPayCode() {
		return pasPayCode;
	}

	public void setPasPayCode(String pasPayCode) {
		this.pasPayCode = pasPayCode;
	}

	public String getPasPatGroupCode() {
		return pasPatGroupCode;
	}

	public void setPasPatGroupCode(String pasPatGroupCode) {
		this.pasPatGroupCode = pasPatGroupCode;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getForceProceedInvoiceNum() {
		return forceProceedInvoiceNum;
	}
	
	public void setForceProceedInvoiceNum(String forceProceedInvoiceNum) {
		this.forceProceedInvoiceNum = forceProceedInvoiceNum;
	}

	public String getForceProceedReceiptNum() {
		return forceProceedReceiptNum;
	}
	
	public void setForceProceedReceiptNum(String forceProceedReceiptNum) {
		this.forceProceedReceiptNum = forceProceedReceiptNum;
	}
	
	public String getForceProceedReason() {
		return forceProceedReason;
	}

	public void setForceProceedReason(String forceProceedReason) {
		this.forceProceedReason = forceProceedReason;
	}

	public String getForceProceedFlag() {
		return forceProceedFlag;
	}

	public void setForceProceedFlag(String forceProceedFlag) {
		this.forceProceedFlag = forceProceedFlag;
	}

	public String getForceProceedBy() {
		return forceProceedBy;
	}

	public void setForceProceedBy(String forceProceedBy) {
		this.forceProceedBy = forceProceedBy;
	}

	public Double getTotalHandleExemptAmount() {
		return totalHandleExemptAmount;
	}

	public void setTotalHandleExemptAmount(Double totalHandleExemptAmount) {
		this.totalHandleExemptAmount = totalHandleExemptAmount;
	}

	public String getChargeAmount() {
		return chargeAmount;
	}

	public void setChargeAmount(String chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	public String getReceiptAmount() {
		return receiptAmount;
	}

	public void setReceiptAmount(String receiptAmount) {
		this.receiptAmount = receiptAmount;
	}

	public String getReceiptNum() {
		return receiptNum;
	}

	public void setReceiptNum(String receiptNum) {
		this.receiptNum = receiptNum;
	}

	public String getReceiptMessage() {
		return receiptMessage;
	}

	public void setReceiptMessage(String receiptMessage) {
		this.receiptMessage = receiptMessage;
	}

	public boolean isAllowReprint() {
		return allowReprint;
	}

	public void setAllowReprint(boolean allowReprint) {
		this.allowReprint = allowReprint;
	}

	public String getPatHospCode() {
		return patHospCode;
	}
	
	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public void setDisplayForceProceedReason(String displayForceProceedReason) {
		this.displayForceProceedReason = displayForceProceedReason;
	}

	public String getDisplayForceProceedReason() {
		return displayForceProceedReason;
	}

	public void setDeliveryItemStatus(String deliveryItemStatus) {
		this.deliveryItemStatus = deliveryItemStatus;
	}

	public String getDeliveryItemStatus() {
		return deliveryItemStatus;
	}

	public void setInvoiceStatusForClearance(InvoiceStatus invoiceStatusForClearance) {
		this.invoiceStatusForClearance = invoiceStatusForClearance;
	}

	public InvoiceStatus getInvoiceStatusForClearance() {
		return invoiceStatusForClearance;
	}

	public void setPurchaseReqId(Long purchaseReqId) {
		this.purchaseReqId = purchaseReqId;
	}

	public Long getPurchaseReqId() {
		return purchaseReqId;
	}
}
