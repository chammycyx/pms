package hk.org.ha.model.pms.biz.inbox;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.biz.reftable.MpWardManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileErrorStat;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;
import org.jboss.seam.security.Identity;

@Stateful
@Scope(ScopeType.SESSION)
@Name("errorStatService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ErrorStatServiceBean implements ErrorStatServiceLocal {
	
	private static final String MED_PROFILE_ERROR_STAT_QUERY_HINT_FETCH_1 = "e.medProfile.medCase";
	private static final String MED_PROFILE_ERROR_STAT_QUERY_HINT_FETCH_2 = "e.medProfile.patient";

	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private Identity identity;
	
	@In
	private PharmInboxManagerLocal pharmInboxManager;
	
	@In
	private MpWardManagerLocal mpWardManager;
	
	@In
	private CategorySummaryLocal categorySummary;
	
	@In
	private Workstation workstation;
	
	@In
	public Workstore workstore;

	@In @Out
	private Boolean showAllWards;
	
	@In
	private Boolean showChemoProfileOnly;
	
	@Out(required = false)
	private List<MedProfileErrorStat> errorStatList;
	
    public ErrorStatServiceBean() {
    }
    
    @Override
    public void setShowAllWards(Boolean showAllWards)
    {
    	this.showAllWards = showAllWards;
    	pharmInboxManager.setShowAllWards(workstation, showAllWards);
    	retrieveErrorStatList();
    }
    
    @Override
	public void retrieveErrorStatList() {
    	
    	List<String> wardCodeList;
    	if (Boolean.TRUE.equals(showAllWards)) {
    		wardCodeList = mpWardManager.retrieveWardCodeList(workstore.getWorkstoreGroup());
		} else {
			wardCodeList = mpWardManager.retrieveActiveWardCodeList(workstore);
		}
    	
    	errorStatList = retrieveErrorStatList(wardCodeList);
    	categorySummary.countCategories(null, workstore, showChemoProfileOnly, wardCodeList);
    }
    
    @Override
    public void notifyMds(Long errorStatId, boolean notified) {

    	MedProfileErrorStat errorStat = retrieveLockedErrorStat(errorStatId);
    	if (errorStat != null) {
	    	errorStat.setCheckUser(notified ? identity.getCredentials().getUsername(): null);
	    	errorStat.setCheckDate(notified ? new Date(): null);
	    	em.merge(errorStat);
    	}
    	retrieveErrorStatList();
    }
    
    @SuppressWarnings("unchecked")
	private MedProfileErrorStat retrieveLockedErrorStat(Long id) {
    	
    	return (MedProfileErrorStat) MedProfileUtils.getFirstItem(em.createQuery(
    			"select e from MedProfileErrorStat e" + // 20120604 index check : MedProfileErrorStat.id : PK_MED_PROFILE_ERROR_STAT
    			" where e.id = :id and e.id is not null") // don't delete : magic code fix bug for https://bugs.eclipse.org/bugs/show_bug.cgi?id=347592
    			.setParameter("id", id)
    			.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
				.setHint(QueryHints.FETCH, MED_PROFILE_ERROR_STAT_QUERY_HINT_FETCH_1)
				.setHint(QueryHints.FETCH, MED_PROFILE_ERROR_STAT_QUERY_HINT_FETCH_2)    			
    			.getResultList());
    }
    
    @SuppressWarnings("unchecked")
	private List<MedProfileErrorStat> retrieveErrorStatList(List<String> wardCodeList) {
    	
    	if (wardCodeList == null || wardCodeList.size() <= 0) {
    		return new ArrayList<MedProfileErrorStat>();
    	}
    	
    	Query query = em.createQuery(
    			"select e from MedProfileErrorStat e" + // 20140926 index check : none
    			" where (e.medProfile.wardCode in :wardCodeList or e.medProfile.wardCode is null)" +
    			" and e.medProfile.hospCode = :hospCode" +
    			" and e.medProfile.status <> :inactiveStatus")
    			.setParameter("wardCodeList", wardCodeList)
    			.setParameter("hospCode", workstore.getHospCode())
    			.setParameter("inactiveStatus", MedProfileStatus.Inactive)
				.setHint(QueryHints.FETCH, MED_PROFILE_ERROR_STAT_QUERY_HINT_FETCH_1)
				.setHint(QueryHints.FETCH, MED_PROFILE_ERROR_STAT_QUERY_HINT_FETCH_2);
    	
    	return query.getResultList();
    }
    
    
	@Remove
	@Override
	public void destroy() {
		errorStatList = null;
	}	
}
