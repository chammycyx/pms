package hk.org.ha.model.pms.persistence.medprofile;

import hk.org.ha.fmk.pms.entity.CreateDate;
import hk.org.ha.fmk.pms.entity.CreateUser;
import hk.org.ha.model.pms.udt.disp.MpTrxType;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "MED_PROFILE_TRX_LOG")
public class MedProfileTrxLog {
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MedProfileTrxLogSeq")
	@SequenceGenerator(name = "MedProfileTrxLogSeq", sequenceName = "SQ_MED_PROFILE_TRX_LOG", initialValue = 100000000)
	private Long id;
	
	@CreateDate
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_DATE", nullable = false)
	// change to public to avoid exception from Reflections.setAndWrap(Reflections.java:131)
	public Date createDate;
	
	@CreateUser
	@Column(name = "CREATE_USER", length = 12)
	// change to public to avoid exception from Reflections.setAndWrap(Reflections.java:131)	
	public String createUser; 
	
	@Column(name = "REF_NUM", length = 50, nullable = false)
	private String refNum;

	@Converter(name = "MedProfileTrxLog.type", converterClass = MpTrxType.Converter.class)
    @Convert("MedProfileTrxLog.type")
	@Column(name = "TYPE", nullable = false, length = 3)
	private MpTrxType type;
		
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MED_PROFILE_ID", nullable = false)
	private MedProfile medProfile;
	
	@Column(name = "MED_PROFILE_ID", nullable = false, insertable = false, updatable = false)
	private Long medProfileId;	

	@Column(name = "WORKSTATION_ID")
	private Long workstationId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate == null ? null : new Date(createDate.getTime());
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate == null ? null : new Date(createDate.getTime());
	}
	
	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getRefNum() {
		return refNum;
	}

	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}

	public MpTrxType getType() {
		return type;
	}

	public void setType(MpTrxType type) {
		this.type = type;
	}
	
	public Long getWorkstationId() {
		return workstationId;
	}

	public void setWorkstationId(Long workstationId) {
		this.workstationId = workstationId;
	}

	public MedProfile getMedProfile() {
		return medProfile;
	}

	public void setMedProfile(MedProfile medProfile) {
		this.medProfile = medProfile;
	}
	
	public Long getMedProfileId() {
		return medProfileId;
	}

	public void setMedProfileId(Long medProfileId) {
		this.medProfileId = medProfileId;
	}
}
