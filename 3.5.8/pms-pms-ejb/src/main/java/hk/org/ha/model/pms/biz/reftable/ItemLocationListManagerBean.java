package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.util.QueryUtils;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.ItemLocation;
import hk.org.ha.model.pms.persistence.reftable.Prepack;
import hk.org.ha.model.pms.udt.reftable.MachineType;
import hk.org.ha.model.pms.vo.reftable.ItemLocationInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("itemLocationListManager")
@MeasureCalls
public class ItemLocationListManagerBean implements ItemLocationListManagerLocal {

	@PersistenceContext
	private EntityManager em;
	
	@In
	private Workstore workstore;

	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@SuppressWarnings("unchecked")
	public List<ItemLocationInfo> retrieveItemLocationListByItemCode(String itemCode)
	{
		List<ItemLocationInfo> itemLocationInfoList = new ArrayList<ItemLocationInfo>();
		DmDrug dmDrug = dmDrugCacher.getDmDrug(itemCode);
		
		if (dmDrug!=null) {
			List<Prepack> prepackList = em.createQuery(
					"select o from Prepack o" + // 20120613 index check : Prepack.workstore,itemCode : UI_PREPACK_01
					" where o.workstore = :workstore" +
					" and o.itemCode = :itemCode")
					.setParameter("workstore", workstore)
					.setParameter("itemCode", itemCode)
					.getResultList();
			
			String prepackQtyText = null;
			
			if( !prepackList.isEmpty() ){
				Prepack prepack = prepackList.get(0);
				prepackQtyText = getPrepackQtyText(prepack);
			}
			
			List<ItemLocation> itemLocationList = em.createQuery(
					"select o from ItemLocation o" + // 20120303 index check : ItemLocation.workstore,itemCode : I_ITEM_LOCATION_02
					" where o.itemCode = :itemCode" +
					" and o.workstore = :workstore" +
					" order by o.itemCode")
					.setParameter("itemCode", itemCode)
					.setParameter("workstore", workstore)
					.setHint(QueryHints.FETCH, "o.machine")
					.getResultList();
			
			for(ItemLocation itemLocation:itemLocationList){
				ItemLocationInfo itemLocationInfo = new ItemLocationInfo();
				itemLocationInfo.setItemCode(itemLocation.getItemCode());
				itemLocationInfo.setItemLocationId(itemLocation.getId());
				itemLocationInfo.setPickStationNum(itemLocation.getMachine().getPickStation().getPickStationNum());
				itemLocationInfo.setBinNum(itemLocation.getBinNum());
				itemLocationInfo.setMachineType(itemLocation.getMachine().getType());
				itemLocationInfo.setMaxQty(itemLocation.getMaxQty());
				itemLocationInfo.setBakerCellNum(itemLocation.getBakerCellNum());
				itemLocationInfo.setMachineList(itemLocation.getMachine().getPickStation().getMachineList());
				
				if( prepackQtyText != null ){
					itemLocationInfo.setPrepackQty( prepackQtyText );
				}
				
				itemLocationInfoList.add(itemLocationInfo);
			}
		}
		return itemLocationInfoList;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, ItemLocation> retrieveItemLocationListByItemCodeList(List<String> itemCodeList)
	{
		Map<String, ItemLocation> itemLocationMap = null;
		List<DmDrug> dmDrugList = new ArrayList<DmDrug>();

		for (String itemCode:itemCodeList) {
			DmDrug dmDrug = dmDrugCacher.getDmDrug(itemCode);
			if (dmDrug != null) {
				dmDrugList.add(dmDrug);
			}
		}
		
		if (dmDrugList.size() > 0) {
			List<ItemLocation> itemLocationList = QueryUtils.splitExecute(
					em.createQuery(
							"select o from ItemLocation o" + // 20120303 index check : ItemLocation.workstore,itemCode : I_ITEM_LOCATION_02
							" where o.itemCode in :itemCodeList" +
							" and o.workstore = :workstore" +
							" and o.machine.type = :type" +
							" order by o.itemCode")
							.setParameter("workstore", workstore)
							.setParameter("type", MachineType.Manual)
							.setHint(QueryHints.FETCH, "o.machine.pickStation")
							,
					"itemCodeList", itemCodeList
					);
			
			if (itemLocationList.size() > 0) {
				itemLocationMap = new HashMap<String, ItemLocation>();
				for(ItemLocation itemLocation:itemLocationList){
					itemLocation.getMachine().getPickStation();
					itemLocationMap.put(itemLocation.getItemCode(), itemLocation);
				}
			}
		}
		return itemLocationMap;
	}
	
	@SuppressWarnings("unchecked")
	public List<ItemLocationInfo> retrieveItemLocationListByPickStation(int pickStationNum) 
	{
		List<ItemLocationInfo> itemLocationInfoList = new ArrayList<ItemLocationInfo>();
		
		List<ItemLocation> itemLocationList = em.createQuery(
				"select o from ItemLocation o" + // 20120303 index check : PickStation.workstore,pickStationNum : UI_PICK_STATION_01
				" where o.machine.pickStation.pickStationNum = :pickStationNum" +
				" and o.machine.pickStation.workstore = :workstore" +
				" order by o.itemCode")
				.setParameter("workstore", workstore)
				.setParameter("pickStationNum", pickStationNum)
				.setHint(QueryHints.FETCH, "o.machine")
				.getResultList();
		
		List<Prepack> prepackList = em.createQuery(
				"select o from Prepack o" + // 20120613 index check : Prepack.workstore : FK_PREPACK_01
				" where o.workstore = :workstore")
				.setParameter("workstore", workstore)
				.getResultList();
		
		Map<String, Prepack> prepackMap = new HashMap<String, Prepack>();
		for( Prepack prepack : prepackList ){
			prepackMap.put(prepack.getItemCode(), prepack);
		}
		
		for(ItemLocation itemLocation:itemLocationList){
			ItemLocationInfo itemLocationInfo = new ItemLocationInfo();
			itemLocationInfo.setItemCode(itemLocation.getItemCode());
			itemLocationInfo.setItemLocationId(itemLocation.getId());
			itemLocationInfo.setPickStationNum(itemLocation.getMachine().getPickStation().getPickStationNum());
			itemLocationInfo.setBinNum(itemLocation.getBinNum());
			itemLocationInfo.setMachineType(itemLocation.getMachine().getType());
			itemLocationInfo.setMaxQty(itemLocation.getMaxQty());
			itemLocationInfo.setBakerCellNum(itemLocation.getBakerCellNum());
			itemLocationInfo.setMachineList(itemLocation.getMachine().getPickStation().getMachineList());
			
			if( prepackMap.get(itemLocation.getItemCode()) != null ){
				itemLocationInfo.setPrepackQty( getPrepackQtyText(prepackMap.get(itemLocation.getItemCode())) );
			}
			itemLocationInfoList.add(itemLocationInfo);
		}
		
		return itemLocationInfoList;
	}
	
	private String getPrepackQtyText(Prepack prepack){
		List<String> prepackQtyList = new ArrayList<String>();
		for( Integer prepackQty : prepack.getPrepackQty() ){
			prepackQtyList.add(prepackQty.toString());
		}
		return "Prepack Qty: " + StringUtils.join( prepackQtyList.toArray(), ", " );
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, List<ItemLocation>> retrieveItemLocationListByItemCodeList(List<String> itemCodeList, MachineType machineType, List<Integer> pickStationsNumList)
	{
		Map<String, List<ItemLocation>> itemLocationMap = new HashMap<String, List<ItemLocation>>();
		
		if ( !itemCodeList.isEmpty() ) {
			StringBuilder sb = new StringBuilder(
					"select o from ItemLocation o" + // 20120303 index check : ItemLocation.workstore,itemCode : I_ITEM_LOCATION_02
					" where o.itemCode in :itemCodeList" +
					" and o.workstore = :workstore" +
					" and o.machine.type = :machineType");
			
			if ( !pickStationsNumList.isEmpty() ) {
				sb.append(" and o.machine.pickStation.pickStationNum in :pickStationsNumList");
			}
			sb.append(" order by o.itemCode");
			
			Query query = em.createQuery(sb.toString())
							.setParameter("workstore", workstore)
							.setParameter("machineType", machineType)
							.setHint(QueryHints.FETCH, "o.machine.pickStation");
			
			if ( !pickStationsNumList.isEmpty() ) {
				query.setParameter("pickStationsNumList", pickStationsNumList);
			}
			
			List<ItemLocation> itemLocationList = QueryUtils.splitExecute(query,
																			"itemCodeList", itemCodeList
																			);
			
			if (itemLocationList.size() > 0) {
				for(ItemLocation itemLocation:itemLocationList){
					if ( !itemLocationMap.containsKey(itemLocation.getItemCode()) ) {
						itemLocationMap.put(itemLocation.getItemCode(), new ArrayList<ItemLocation>());
					}
					itemLocation.getMachine().getPickStation();
					itemLocationMap.get(itemLocation.getItemCode()).add(itemLocation);
				}
			}
		}
		return itemLocationMap;
	}
	
	@SuppressWarnings("unchecked")
	public List<ItemLocation> retrieveItemLocationList(String itemCode, List<Integer> pickStationNumList, List<MachineType> machineTypeList){
		
		if( pickStationNumList == null || pickStationNumList.isEmpty() ){
			return new ArrayList<ItemLocation>();
		}else{
			return em.createQuery(
					"select o from ItemLocation o" + // 20140826 index check : ItemLocation.workstore,itemCode : I_ITEM_LOCATION_02
					" where o.itemCode = :itemCode" +
					" and o.workstore = :workstore" +
					" and o.machine.type in :machineTypeList" +
					" and o.machine.pickStation.pickStationNum in :pickStationNumList")
					.setParameter("itemCode", itemCode)
					.setParameter("workstore", workstore)
					.setParameter("machineTypeList", machineTypeList)
					.setParameter("pickStationNumList", pickStationNumList)
					.getResultList();
		}
						
	}
	
	@Remove
	public void destroy() 
	{

	}
}
