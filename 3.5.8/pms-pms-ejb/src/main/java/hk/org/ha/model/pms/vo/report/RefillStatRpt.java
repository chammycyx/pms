package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class RefillStatRpt implements Serializable {

	private static final long serialVersionUID = 1L;

	private String workstoreCode;
	
	private String specCode;
	
	private Integer processedRefillCount;
	
	private Double processedRefillDrugCost;
	
	private Integer outstandRefillCount;
	
	private Double outstandRefillDrugCost;

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public Integer getOutstandRefillCount() {
		return outstandRefillCount;
	}

	public void setOutstandRefillCount(Integer outstandRefillCount) {
		this.outstandRefillCount = outstandRefillCount;
	}

	public Double getOutstandRefillDrugCost() {
		return outstandRefillDrugCost;
	}

	public void setOutstandRefillDrugCost(Double outstandRefillDrugCost) {
		this.outstandRefillDrugCost = outstandRefillDrugCost;
	}

	public void setProcessedRefillCount(Integer processedRefillCount) {
		this.processedRefillCount = processedRefillCount;
	}

	public Integer getProcessedRefillCount() {
		return processedRefillCount;
	}

	public void setProcessedRefillDrugCost(Double processedRefillDrugCost) {
		this.processedRefillDrugCost = processedRefillDrugCost;
	}

	public Double getProcessedRefillDrugCost() {
		return processedRefillDrugCost;
	}



	


}
