package hk.org.ha.model.pms.biz.delivery;

import java.util.List;

import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.DeliverySchedule;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryScheduleItem;
import hk.org.ha.model.pms.vo.delivery.ScheduleTemplateInfo;

import javax.ejb.Local;

@Local
public interface DeliveryScheduleManagerLocal {
	List<DeliverySchedule> retrieveDeliveryScheduleList(Hospital hospital);
	DeliverySchedule retrieveDeliveryScheduleTemplate(Workstore workstore, ScheduleTemplateInfo templateInfo);
	void setDeliveryScheduleTemplate(Workstore workstore, String templateName, boolean resetValue);
	List<DeliveryScheduleItem> retrieveDeliveryScheduleItemList(Workstore workstore, DeliverySchedule deliverySchedule);
	void destroy();
}
