package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.PmsIpDosageConvSet;

import java.util.List;

import javax.ejb.Local;

@Local
public interface MpDosageConversionListServiceLocal {

	List<PmsIpDosageConvSet> retrievePmsIpDosageConvSetList(DmDrug dmDrug);
	
	void updatePmsIpDosageConvSetList(List<PmsIpDosageConvSet> pmsIpDosageConvSetList, List<PmsIpDosageConvSet> markDeletePmsIpDosageConvSetList);
	
	boolean isRetrieveSuccess();

	boolean isSaveSuccess();

	String getErrorCode();

	String[] getErrorParam();
	
	void destroy();
}
