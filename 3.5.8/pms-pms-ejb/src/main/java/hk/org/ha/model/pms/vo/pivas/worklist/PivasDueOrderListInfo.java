package hk.org.ha.model.pms.vo.pivas.worklist;

import hk.org.ha.model.pms.persistence.pivas.PivasWorklist;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PivasDueOrderListInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String errorCode;

	private List<String> errorParamList;
	
	private Date nextSupplyDate;

	private List<PivasWorklist> pivasWorklistList;
	
	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	
	public List<String> getErrorParamList() {
		return errorParamList;
	}

	public void setErrorParamList(List<String> errorParamList) {
		this.errorParamList = errorParamList;
	}

	public Date getNextSupplyDate() {
		return nextSupplyDate;
	}

	public void setNextSupplyDate(Date nextSupplyDate) {
		this.nextSupplyDate = nextSupplyDate;
	}
	
	public List<PivasWorklist> getPivasWorklistList() {
		return pivasWorklistList;
	}

	public void setPivasWorklistList(List<PivasWorklist> pivasWorklistList) {
		this.pivasWorklistList = pivasWorklistList;
	}
}