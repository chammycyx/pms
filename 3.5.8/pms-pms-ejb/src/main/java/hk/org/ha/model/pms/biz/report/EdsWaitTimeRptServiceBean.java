package hk.org.ha.model.pms.biz.report;

import static hk.org.ha.model.pms.prop.Prop.REPORT_WORKSTORE_CLOSINGHOUR;
import static hk.org.ha.model.pms.prop.Prop.REPORT_WORKSTORE_OPENINGHOUR;
import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.util.JavaBeanResult;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.udt.disp.PharmOrderStatus;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.EdsElapseTime;
import hk.org.ha.model.pms.vo.report.EdsWaitTimeExportRpt;
import hk.org.ha.model.pms.vo.report.EdsWaitTimeRpt;
import hk.org.ha.model.pms.vo.report.EdsWaitTimeRptHdr;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.joda.time.DateTime;

@Stateful
@Scope(ScopeType.SESSION)
@Name("edsWaitTimeRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class EdsWaitTimeRptServiceBean implements EdsWaitTimeRptServiceLocal {
	@PersistenceContext
	private EntityManager em;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent; 
	
	@In
	private Workstore workstore;
	
	private boolean retrieveSuccess;
	
	private List<EdsWaitTimeRpt> edsWaitTimeRptList;
	
	private String currentRpt = null;
	
	private List<EdsWaitTimeRpt> reportObj = null;
	
	private DateFormat fileNameDf = new SimpleDateFormat("yyyyMMdd"); 
	
	private final static String EDS_WAIT_TIME_RPT_FILENAME = "WaitingTimeReport";
	
	private Date rptDate;
	
	private boolean retrieveExportSuccess;

	public void retrieveEdsWaitTimeRpt(Integer selectedTab, Date date) {
		reportObj = retrieveEdsWaitTimeRptList(date); 

		if (selectedTab == 0){
			currentRpt = "EdsWaitTimeChart";
		}
		else if (selectedTab == 1){
			currentRpt = "EdsWaitTimeRpt";
		}
		
		if ( !reportObj.isEmpty() ) {
			retrieveSuccess = true;
		}
		else{
			retrieveSuccess = false;
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public List<EdsWaitTimeRpt> retrieveEdsWaitTimeRptList(Date date) {
		edsWaitTimeRptList = new ArrayList<EdsWaitTimeRpt>();
		EdsWaitTimeRpt edsWaitTimeRpt = new EdsWaitTimeRpt();
		
		DateTime fromDate = new DateTime(date);
		DateTime toDate = fromDate.plusDays(1);
		rptDate = date;
		
		Query query = em.createNativeQuery(
				"SELECT to_char(d.check_date, 'HH24') AS timeRange, round(avg(time_diff('MI',t.create_date,d.check_date))) as avgTime " + // 20140128 index check : DispOrder.workstore,checkDate : I_DISP_ORDER_06
				"FROM pharm_order p, disp_order d, ticket t, med_order m " +		
				"WHERE d.pharm_order_id = p.id " +
				"AND d.ticket_id = t.id " +
				"AND p.med_order_id = m.id " +
				"AND d.check_date IS NOT NULL " +
				"AND d.check_date BETWEEN ?1 AND ?2 " +
				"AND d.workstore_code = ?3 " +
				"AND d.hosp_code = ?4 " +
				"AND p.status not in (?5,?6) " +
				"AND t.ticket_date = ?7 " +
				"AND m.order_type = 'O' " +
				"GROUP BY to_char(d.check_date, 'HH24') " +
				"ORDER BY to_char(d.check_date, 'HH24')" );
		
		JavaBeanResult.setQueryResultClass(query,  EdsElapseTime.class);
		
		List<EdsElapseTime> resultList = query.setParameter(1, fromDate.toDate())
												  .setParameter(2, toDate.toDate())
												  .setParameter(3, workstore.getWorkstoreCode())
												  .setParameter(4, workstore.getHospCode())
												  .setParameter(5, PharmOrderStatus.Deleted.getDataValue())
												  .setParameter(6, PharmOrderStatus.SysDeleted.getDataValue())
												  .setParameter(7, date)
												  .getResultList();
		
		Map<String, Integer> resultMap = new HashMap<String, Integer>();
		
		for (EdsElapseTime e : resultList) {
			resultMap.put(e.getTimeRange(), e.getAvgTime());
		}

		List <EdsElapseTime> edsWaitTimeRptDtlList = new ArrayList<EdsElapseTime>();
		int openingHour = REPORT_WORKSTORE_OPENINGHOUR.get();
		int closingHour = REPORT_WORKSTORE_CLOSINGHOUR.get();

		for(int i = openingHour; i < closingHour; i++) {
			EdsElapseTime edsWaitTimeRptDtl = null;
			String startTime = StringUtils.leftPad(String.valueOf(i),2,'0');
			String endTime = StringUtils.leftPad(String.valueOf(i+1),2,'0');
			
			if (resultMap.containsKey(startTime)) {
				edsWaitTimeRptDtl = new EdsElapseTime( startTime + "-" + endTime, resultMap.get(startTime));
			}
			else {
				edsWaitTimeRptDtl = new EdsElapseTime( startTime + "-" + endTime, 0);
			}

			edsWaitTimeRptDtlList.add(edsWaitTimeRptDtl);
		}
		
		edsWaitTimeRpt.setEdsWaitTimeRptDtlList(edsWaitTimeRptDtlList);
		
		EdsWaitTimeRptHdr edsWaitTimeRptHdr = new EdsWaitTimeRptHdr();
	
		OperationProfile op = em.find(OperationProfile.class, workstore.getOperationProfileId());
		
		edsWaitTimeRptHdr.setActiveProfileName(op.getName());
		edsWaitTimeRptHdr.setHospCode(workstore.getHospCode());
		edsWaitTimeRptHdr.setWorkstoreCode(workstore.getWorkstoreCode());
		edsWaitTimeRptHdr.setDate(date);
		edsWaitTimeRpt.setEdsWaitTimeRptHdr(edsWaitTimeRptHdr);
		
		edsWaitTimeRptList.add(edsWaitTimeRpt);
		
		return edsWaitTimeRptList;
	}

	public void generateEdsWaitTimeRpt() {

		Map<String, Object> parameters = new HashMap<String, Object>();
		if ("EdsWaitTimeChart".equals(currentRpt)) {
			parameters.put("_PNG_ZOOM_RATIO", Float.valueOf(1f));
			printAgent.renderAndRedirect(new RenderJob(
					currentRpt + "Preview", 
					new PrintOption(), 
					parameters, 
					reportObj.get(0).getEdsWaitTimeRptDtlList()));
		} else {
			printAgent.renderAndRedirect(new RenderJob(
					currentRpt, 
					new PrintOption(), 
					parameters, 
					reportObj));
		}
	}
	
	public void printEdsWaitTimeRpt(){

		printAgent.renderAndPrint(new RenderAndPrintJob(
				currentRpt, 
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				new HashMap<String, Object>(), 
				reportObj));
		
	}
	
	private String getRptFileName(String rptName, Date rptDate){
		return rptName+fileNameDf.format(rptDate);
	}
	
	public void exportEdsWaitTimeRpt() {	
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("hospCode", workstore.getHospCode());
		parameters.put("workstoreCode", workstore.getWorkstoreCode());
		
		String filename = getRptFileName(EDS_WAIT_TIME_RPT_FILENAME, rptDate);
		
		PrintOption po = new PrintOption();
		po.setDocType(ReportProvider.XLS);
		printAgent.renderAndRedirect(new RenderJob(
				"EdsWaitTimeXls", 
				po, 
				parameters, 
				reportObj.get(0).getEdsWaitTimeRptDtlList()), filename);

	}
	
	@Remove
	public void destroy() {
		if(edsWaitTimeRptList!=null){
			edsWaitTimeRptList=null;
		}
	}

	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}
	
	public boolean isRetrieveExportSuccess() {
		return retrieveExportSuccess;
	}
	
}
