package hk.org.ha.model.pms.udt.report;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum ReportFilterOption implements StringValuedEnum {

	All("All","All"),
	Blank("(Blank)","(Blank)");
	
    private final String dataValue;
    private final String displayValue;

    ReportFilterOption(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static ReportFilterOption dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(ReportFilterOption.class, dataValue);
	}
        
	public static class Converter extends StringValuedEnumConverter<ReportFilterOption> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<ReportFilterOption> getEnumClass() {
    		return ReportFilterOption.class;
    	}
    }
}
