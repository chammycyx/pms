package hk.org.ha.model.pms.biz.delivery;

import hk.org.ha.model.pms.persistence.medprofile.Delivery;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;

import javax.ejb.Local;

@Local
public interface DeliveryManagerLocal {

	void destroy();
	
	TreeMap<Long, Delivery> retrieveDeliveryNumOfLabel(TreeMap<Long, Delivery> deliveryMap);
	
	TreeMap<Long, Delivery> retrieveDeliveryNumOfLabelForLabelReprint(TreeMap<Long, Delivery> deliveryMap, String caseNum, String trxId);
	
	List<DeliveryItem> retrieveDeliveryItemList(String wardCode, String batchCode, String caseNum, Date dispenseDate, String deliveryId);
	
	List<DeliveryItem> retrieveDeliveryItemListForLabelReprint(String wardCode, String batchCode, String caseNum, Date dispenseDate, String deliveryId);
	
	List<DeliveryItem> retrieveDeliveryItemListByBatchNum(String batchNum, Date dispenseDate, String caseNum);
	
	List<DeliveryItem> retrieveDeliveryItemListByBatchNumForLabelReprint(String batchNum, Date dispenseDate, String caseNum, String trxId );
	
	Date calculateTargetDeliveryDate(List<DeliveryItem> deliveryItemList);
	
	List<DeliveryItem> retrieveDeliveryItemListByBatchNumForReprint(String batchNum, Date dispenseDate);

	List<DeliveryItem> retrieveDeliveryItemListById(
			Collection<Long> deliveryItemIdList);
	
	
}
