package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.PmsDosageConversionSet;
import hk.org.ha.model.pms.udt.vetting.OrderType;

import java.util.List;

import javax.ejb.Local;

@Local
public interface DosageConversionListServiceLocal {

	void retrievePmsDosageConversionSetList(DmDrug dmDrug);
	
	PmsDosageConversionSet retrievePmsDosageConversionByMoDosage(DmDrug dmDrug, double moDosage, String moDosageUnit);
	
	void updateDosageConversion(PmsDosageConversionSet pmsDosageConversionSet);
	
	boolean isRetrieveSuccess();

	boolean isSaveSuccess();

	String getErrorCode();

	String[] getErrorParam();
	
	void destroy();
}
