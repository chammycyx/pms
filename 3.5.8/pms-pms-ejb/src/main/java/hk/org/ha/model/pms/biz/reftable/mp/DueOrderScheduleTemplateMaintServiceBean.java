package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.reftable.MpWardManagerLocal;
import hk.org.ha.model.pms.biz.reftable.WardGroupManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.DeliverySchedule;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryScheduleItem;
import hk.org.ha.model.pms.persistence.reftable.WardGroupItem;
import hk.org.ha.model.pms.udt.RecordStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryScheduleItemWardType;
import hk.org.ha.model.pms.vo.medprofile.Ward;
import hk.org.ha.model.pms.vo.medprofile.WardGroup;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang3.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("dueOrderScheduleTemplateMaintService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DueOrderScheduleTemplateMaintServiceBean implements DueOrderScheduleTemplateMaintServiceLocal {

	@PersistenceContext
	private EntityManager em;

	@In
	private MpWardManagerLocal mpWardManager;
	
	@In
	private WardGroupManagerLocal wardGroupManager;
	
	@In
	private Workstore workstore;
	
	@Out(required = false)
	private List<Ward> dueOrderScheduleTemplateMaintWardSelectionList;
	
	@Out(required = false)
	private List<WardGroup> dueOrderScheduleTemplateMaintWardGroupSelectionList;
	
	private static DeliveryScheduleComparator deliveryScheduleComparator = new DeliveryScheduleComparator();
	private static DeliveryScheduleItemComparator deliveryScheduleItemComparator = new DeliveryScheduleItemComparator();
	
	@Override
	public void retrieveWardAndWardGroupList() {
		
		dueOrderScheduleTemplateMaintWardSelectionList = mpWardManager.retrieveActiveWardList(workstore.getWorkstoreGroup());
		
		// wardGroup that without any wardGroupItem is included in this wardGroupList
		// wardCode from inactive/non-mpWard is also included in wardGroup.wardCodeList
		List<hk.org.ha.model.pms.persistence.reftable.WardGroup> wardGroupList = wardGroupManager.retrieveWardGroupList(workstore.getWorkstoreGroup());
		em.clear();

		dueOrderScheduleTemplateMaintWardGroupSelectionList = new ArrayList<WardGroup>();
		
		// set transient wardCodeList
		for (hk.org.ha.model.pms.persistence.reftable.WardGroup wardGroup: wardGroupList) {
			List<String> wardCodeList = new ArrayList<String>();
			for (WardGroupItem wardGroupItem: wardGroup.getWardGroupItemList()) {
				wardCodeList.add(wardGroupItem.getWardCode());
			}
			dueOrderScheduleTemplateMaintWardGroupSelectionList.add(new WardGroup(wardGroup.getWardGroupCode(), wardCodeList));
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DeliverySchedule> retrieveDeliveryScheduleList() {
		List<DeliverySchedule> deliveryScheduleList = em.createQuery(
				"select o from DeliverySchedule o" + // 20171023 index check : DeliverySchedule.hospital,status : I_DELIVERY_SCHEDULE_02
				" where o.status = :status" +
				" and o.hospital = :hospital")
				.setParameter("status", RecordStatus.Active)
				.setParameter("hospital", workstore.getHospital())
				.getResultList();
		
		for (DeliverySchedule deliverySchedule:deliveryScheduleList) {
			deliverySchedule.getHospital();
		}
		
		Collections.sort(deliveryScheduleList,  deliveryScheduleComparator);
		
		return deliveryScheduleList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DeliveryScheduleItem> retrieveDeliveryScheduleItemList(Long deliveryScheduleId) {
		
		List<DeliveryScheduleItem> deliveryScheduleItemList = em.createQuery(
				"select o from DeliveryScheduleItem o" + // 20171023 index check : DeliveryScheduleItem.deliverySchedule : FK_DELIVERY_SCHEDULE_ITEM_01
				" where o.status = :status" + 
				" and o.deliverySchedule.id = :deliveryScheduleId" +
				" and o.deliverySchedule.status = :status")
				.setParameter("status", RecordStatus.Active)
				.setParameter("deliveryScheduleId", deliveryScheduleId)
				.getResultList();
		
		for ( DeliveryScheduleItem deliveryScheduleItem: deliveryScheduleItemList) {
			deliveryScheduleItem.setScheduleTimeStr(StringUtils.leftPad(deliveryScheduleItem.getScheduleTime().toString(), 4, "0"));
			deliveryScheduleItem.setDueHourStr(StringUtils.leftPad(deliveryScheduleItem.getDueHour().toString(), 2, "0"));
		}
		
		Collections.sort(deliveryScheduleItemList,  deliveryScheduleItemComparator);
		
		return deliveryScheduleItemList;
	}
	
	@Override
	public boolean checkDeliveryScheduleNameExists(String deliveryScheduleName) {
		
		int count = ((Long)em.createQuery(
				"select count(o) from DeliverySchedule o" +  // 20171023 index check : DeliverySchedule.hospital,name : I_DELIVERY_SCHEDULE_01
				" where o.name = :name" +
				" and o.status = :status" +
				" and o.hospital = :hospital")
				.setParameter("name", deliveryScheduleName)
				.setParameter("status", RecordStatus.Active)
				.setParameter("hospital", workstore.getHospital())
				.getSingleResult()).intValue();
		
		if (count > 0) {
			return true;
		}
		return false;
	}
	
	@Override
	public List<DeliveryScheduleItem> copyDeliverySchedule(Long deliveryScheduleItemId) {
		
		List<DeliveryScheduleItem> deliveryScheduleItemList = retrieveDeliveryScheduleItemList(deliveryScheduleItemId);
		em.clear();
		
		// clear baseEntity
		for ( DeliveryScheduleItem deliveryScheduleItem: deliveryScheduleItemList) {
			deliveryScheduleItem.setId(null);
			deliveryScheduleItem.setDeliverySchedule(null);
			deliveryScheduleItem.setCreateDate(null);
			deliveryScheduleItem.setCreateUser(null);
			deliveryScheduleItem.setUpdateDate(null);
			deliveryScheduleItem.setUpdateUser(null);
		}
		
		// remove invalid wardCode/wardGroup from csv
		for (DeliveryScheduleItem deliveryScheduleItem: deliveryScheduleItemList) {
			
			if (deliveryScheduleItem.getWardType() == DeliveryScheduleItemWardType.Ward) {
				
				if (dueOrderScheduleTemplateMaintWardSelectionList.size() == 0) {
					// no ward in current ward list
					deliveryScheduleItem.setWardCodeCsv("");
					continue;
				}
				
				List<String> wardCodeListInDB = convertCsvStringtoList(deliveryScheduleItem.getWardCodeCsv());
				Set<String> currentActiveWardCodeTreeSet = new TreeSet<String>();
				
				for (Ward activeWard :dueOrderScheduleTemplateMaintWardSelectionList) {
					currentActiveWardCodeTreeSet.add(activeWard.getWardCode());
				}
				
				// activeWardCode is sorted in TreeSet
				StringBuilder updatedWardCodeCsvSb = new StringBuilder();
				for (String wardCode:wardCodeListInDB) {
					if (currentActiveWardCodeTreeSet.contains(wardCode)) {
						if (updatedWardCodeCsvSb.length() > 0) {
							updatedWardCodeCsvSb.append(",");
						}
						updatedWardCodeCsvSb.append(wardCode);
					}
				}

				deliveryScheduleItem.setWardCodeCsv(updatedWardCodeCsvSb.toString());
				
			} else if (deliveryScheduleItem.getWardType() == DeliveryScheduleItemWardType.WardGroup) {	
				
				if (dueOrderScheduleTemplateMaintWardGroupSelectionList.size() == 0) {
					// no wardGroup in current wardGroup list
					deliveryScheduleItem.setWardGroupCsv("");
					continue;
				}
				
				List<String> wardGroupCodeListInDB = convertCsvStringtoList(deliveryScheduleItem.getWardGroupCsv());
				Set<String> currentWardGroupTreeSet = new TreeSet<String>();
				
				for (WardGroup wardGroup :dueOrderScheduleTemplateMaintWardGroupSelectionList) {
					currentWardGroupTreeSet.add(wardGroup.getWardGroupCode());
				}
				
				// wardGroupCode is sorted in TreeSet
				StringBuilder updatedWardGroupCsvSb = new StringBuilder();
				for (String wardGroupCode:wardGroupCodeListInDB) {
					if (currentWardGroupTreeSet.contains(wardGroupCode)) {
						if (updatedWardGroupCsvSb.length() > 0) {
							updatedWardGroupCsvSb.append(",");
						}
						updatedWardGroupCsvSb.append(wardGroupCode);
					}
				}

				deliveryScheduleItem.setWardGroupCsv(updatedWardGroupCsvSb.toString());
			} // else DeliveryScheduleItemWardType.AllWard
		}
		
		return deliveryScheduleItemList;
	}
    
	@Override
	public List<DeliverySchedule> updateDeliverySchedule(DeliverySchedule updateDeliverySchedule, List<DeliveryScheduleItem> updateDeliveryScheduleList) {
		
		for (DeliveryScheduleItem deliveryScheduleItem: updateDeliveryScheduleList) {
			deliveryScheduleItem.setScheduleTime(Integer.parseInt(deliveryScheduleItem.getScheduleTimeStr()));
			deliveryScheduleItem.setDueHour(Integer.parseInt(deliveryScheduleItem.getDueHourStr()));
		}
		
		if (updateDeliverySchedule.isNew()) {
			updateDeliverySchedule.setHospital(workstore.getHospital());
			
			em.persist(updateDeliverySchedule);
			
			// deliveryScheduleItemList is not private owned
			for (DeliveryScheduleItem updatedDeliveryScheuleItem: updateDeliveryScheduleList) {
				em.persist(updatedDeliveryScheuleItem); 
			}
		} else {
			em.merge(updateDeliverySchedule);
			
			// deliveryScheduleItemList is not private owned
			for (DeliveryScheduleItem updatedDeliveryScheuleItem: updateDeliveryScheduleList) {
				// persist is required for new dsi
				if (updatedDeliveryScheuleItem.isNew()) {
					em.persist(updatedDeliveryScheuleItem); 
				} else {
					em.merge(updatedDeliveryScheuleItem); 
				}
			}
		}
		
		em.flush();
		em.clear();
		
		return retrieveDeliveryScheduleList();
	}
	
	
	@Override
	public List<DeliverySchedule> deleteDeliverySchedule(Long delDeliveryScheduleId) {
		
		DeliverySchedule delDeliverySchedule = em.find(DeliverySchedule.class, delDeliveryScheduleId);
		
		delDeliverySchedule.setStatus(RecordStatus.Delete);
		for (DeliveryScheduleItem deleteDeliveryScheduleItem :delDeliverySchedule.getDeliveryScheduleItemList()) {
			deleteDeliveryScheduleItem.setStatus(RecordStatus.Delete);
		}
		
		em.merge(delDeliverySchedule);
		
		for (DeliveryScheduleItem delDeliveryScheuleItem:delDeliverySchedule.getDeliveryScheduleItemList()) {
			em.merge(delDeliveryScheuleItem); 
		}
		
		em.flush();
		em.clear();
		
		return retrieveDeliveryScheduleList();
	}

    private List<String> convertCsvStringtoList(String csv) {
    	List<String> csvList = new ArrayList<String>();
    	String[] csvArray = StringUtils.split(csv, ",");
    	
    	for (String str: csvArray) {
    		csvList.add(str);
        }
    	return csvList;
    }
	
	private static class DeliveryScheduleComparator implements Comparator<DeliverySchedule>, Serializable {
		
		private static final long serialVersionUID = 1L;

		@Override
		public int compare(DeliverySchedule deliverySchedule1, DeliverySchedule deliverySchedule2) {
			int compareResult = 0;

			compareResult = new CompareToBuilder()
											.append( deliverySchedule1.getName(), deliverySchedule2.getName() )
											// use id to ensure all records are distinct
											.append( deliverySchedule1.getId(), deliverySchedule1.getId() )
											.toComparison();
			
			return compareResult;
		}
	}
	
	private static class DeliveryScheduleItemComparator implements Comparator<DeliveryScheduleItem>, Serializable {
		
		private static final long serialVersionUID = 1L;

		@Override
		public int compare(DeliveryScheduleItem deliveryScheduleItem1, DeliveryScheduleItem deliveryScheduleItem2) {
			int compareResult = 0;

			compareResult = new CompareToBuilder()
											.append( deliveryScheduleItem1.getScheduleTime(), deliveryScheduleItem2.getScheduleTime() )
											.append( deliveryScheduleItem1.getDueOnType(), deliveryScheduleItem2.getDueOnType() )
											.append( deliveryScheduleItem1.getDueHour(), deliveryScheduleItem2.getDueHour() )
											// use id to ensure all records are distinct
											.append( deliveryScheduleItem1.getId(), deliveryScheduleItem2.getId() )
											.toComparison();
			
			return compareResult;
		}
	}

	@Remove
	@Override
	public void destroy() {
		dueOrderScheduleTemplateMaintWardSelectionList = null;
		dueOrderScheduleTemplateMaintWardGroupSelectionList = null;
	}
}
