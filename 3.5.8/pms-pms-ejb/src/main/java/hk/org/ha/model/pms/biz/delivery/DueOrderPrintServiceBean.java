package hk.org.ha.model.pms.biz.delivery;

import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryRequest;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryRequestGroup;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryScheduleItem;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.medprofile.DeliveryRequestGroupStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryRequestStatus;
import hk.org.ha.model.pms.vo.security.PropMap;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;


@Stateful
@Scope(ScopeType.SESSION)
@Name("dueOrderPrintService")
@Restrict("#{identity.loggedIn}")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@RemoteDestination
@MeasureCalls
public class DueOrderPrintServiceBean implements DueOrderPrintServiceLocal {

	@Logger
	private Log logger;
	
	@In
	private AuditLogger auditLogger;
	
	@In
	private Workstation workstation;
	
	@In
	private List<Integer> atdpsPickStationsNumList;
	
	@In
	private UamInfo uamInfo;
	
	@In
	private PropMap propMap;
	
	@In
	private DueOrderPrintManagerLocal dueOrderPrintManager;
	
	@In
	private DueOrderAsyncPrintManagerLocal dueOrderAsyncPrintManager;
	
	@Out(required = false)
	protected Integer scheduleSuccessCount;

	@Out(required = false)
	protected Integer scheduleErrorCount;
	
	@Out(required = false)
	protected Integer successCount;

	@Out(required = false)
	protected Integer errorCount;
	
	@Out(required = false)
	protected Boolean profileNotFound;
	
	@Out(required = false)
	protected String invalidWardCode;
	
	private DeliveryRequest request;
	
	private DeliveryRequestGroup requestGroup;
	
    public DueOrderPrintServiceBean() {
    }
    
    @Override
    public DeliveryRequestGroupStatus assignBatchAndPrintByDeliveryScheduleItem(DeliveryScheduleItem deliveryScheduleItem, List<DeliveryRequest> deliveryRequestList){
    	DeliveryRequestGroup requestGroup = dueOrderPrintManager.processDeliveryRequestGroup(workstation, deliveryScheduleItem);
    	
    	for(DeliveryRequest request:deliveryRequestList){
    		auditAssignBatchAndPrint(request);
    		request.setDeliveryRequestGroup(requestGroup);
    		request.setAtdpsPickStationsNumList(atdpsPickStationsNumList);
    	}
    	resetScheduleTemplateCounter();
    	this.requestGroup = requestGroup;
    	
    	dueOrderAsyncPrintManager.assignBatchAndPrintByDeliveryScheduleItem(workstation, uamInfo, propMap, requestGroup, deliveryRequestList);
    	
    	return DeliveryRequestGroupStatus.Processing;
    }
    
    @Override
    public DeliveryRequestGroupStatus retrieveDeliveryRequestGroupStatus(long timeout){
    	DeliveryRequestGroup reqGroup = this.requestGroup;
    	
    	if(reqGroup == null){
    		return null;
    	}else if(reqGroup.isCompleted(timeout)){
    		updateScheduleCounter(reqGroup);
    		throwIfExceptionOccurs(reqGroup);
    		return DeliveryRequestGroupStatus.Active;
    	}else{
    		return reqGroup.getId() == null ? null : DeliveryRequestGroupStatus.Processing;
    	}
    }
    
    @Override
    public DeliveryRequestStatus assignBatchAndPrint(DeliveryRequest request) {
    	
    	auditAssignBatchAndPrint(request);
    	resetCounterAndState();
    	
    	this.request = request;
    	this.request.setAtdpsPickStationsNumList(atdpsPickStationsNumList);
    	dueOrderAsyncPrintManager.assignBatchAndPrint(workstation, uamInfo, propMap, request);
    	return DeliveryRequestStatus.Processing;
    }
    
    @Override
    public DeliveryRequestStatus retrieveAssignBatchAndPrintStatus(long timeout) {
    	
    	DeliveryRequest request = this.request;
    	
    	if (request == null) {
    		return null;
    	} else if (request.isCompleted(timeout)) {
    		updateCounterAndState(request);
    		throwIfExceptionOccurs(request);
    		return DeliveryRequestStatus.Completed;
    	} else {
    		return request.getId() == null ? null : DeliveryRequestStatus.Processing;
    	}
    }
    
    private void resetScheduleTemplateCounter() {
    	scheduleSuccessCount = Integer.valueOf(0);
    	scheduleErrorCount = Integer.valueOf(0);
    }
    
    private void updateScheduleCounter(DeliveryRequestGroup requestGroup) {
    	scheduleSuccessCount = requestGroup.getSuccessCount();
    	scheduleErrorCount = requestGroup.getErrorCount();
    }
    
    private void resetCounterAndState() {
    	
    	successCount = Integer.valueOf(0);
    	errorCount = Integer.valueOf(0);
    	profileNotFound = Boolean.FALSE;
    	invalidWardCode = null;
    }
    
    private void updateCounterAndState(DeliveryRequest request) {
    	
    	successCount = request.getSuccessCount();
    	errorCount = request.getErrorCount();
    	profileNotFound = request.getProfileNotFoundFlag();
    	invalidWardCode = request.getInvalidWardCode();
    }
    
    private void auditAssignBatchAndPrint(DeliveryRequest request) {
    	
    	if (request.isBatchPrint()) {
        	auditLogger.log("#0353", request.getCaseNum(), request.getHkid());    		
    	} else {
	    	auditLogger.log("#0354", request.getNewOrderFlag(), request.getUrgentFlag(), request.getStatFlag(),
	    			request.getRefillFlag(), request.getDueDate(), request.getGenerateDay() , request.getRefillDueDate(),
	    			request.getCaseNum(), request.getHkid(), request.getWardCodeCsv());
    	}
    }
    
    private void throwIfExceptionOccurs(DeliveryRequest request) {
    	
    	if (request == null) {
    		return;
    	}
    	Throwable t = request.getThrowable();
    	
    	if (t instanceof RuntimeException) {
    		throw (RuntimeException) t;
    	} else if (t != null) {
    		throw new RuntimeException(t);
    	}
    }
    
    private void throwIfExceptionOccurs(DeliveryRequestGroup requestGroup) {
    	
    	if (requestGroup == null) {
    		return;
    	}
    	Throwable t = requestGroup.getThrowable();
    	
    	if (t instanceof RuntimeException) {
    		throw (RuntimeException) t;
    	} else if (t != null) {
    		throw new RuntimeException(t);
    	}
    }
    
    @Remove
	@Override
	public void destroy() {
	}
}
