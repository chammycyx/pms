package hk.org.ha.model.pms.vo.reftable;
import hk.org.ha.model.pms.udt.reftable.OperationWorkstationType;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PmsSessionInfo  implements Serializable {

	private static final long serialVersionUID = 1L;

	private String userId;
	
	private String workstationCode;
	
	private String hostName;
	
	private OperationWorkstationType workstationType;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWorkstationCode() {
		return workstationCode;
	}

	public void setWorkstationCode(String workstationCode) {
		this.workstationCode = workstationCode;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public void setWorkstationType(OperationWorkstationType workstationType) {
		this.workstationType = workstationType;
	}

	public OperationWorkstationType getWorkstationType() {
		return workstationType;
	}
}
