package hk.org.ha.model.pms.biz.sys;

import hk.org.ha.fmk.pms.web.MeasureCalls;

import java.util.Date;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("timeService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class TimeServiceBean implements TimeServiceLocal {

    /**
     * Default constructor. 
     */
    public TimeServiceBean() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public Date getCurrentServerTime() {
    	return new Date();
    }
    
    @Remove
	@Override
	public void destroy() {
    }
}
