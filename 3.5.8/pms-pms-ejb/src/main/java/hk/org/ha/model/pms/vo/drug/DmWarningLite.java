package hk.org.ha.model.pms.vo.drug;


import hk.org.ha.model.pms.corp.cache.DmWarningCacher;
import hk.org.ha.model.pms.dms.persistence.DmWarning;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
import org.springframework.beans.BeanUtils;

@ExternalizedBean(type=DefaultExternalizer.class)
public class DmWarningLite implements Serializable {
	private static final long serialVersionUID = 1L;
	private String usageType;
	private String warnMessageEng;
	private String warnMessageChi;
	private String warnCode;
	private String warnCatCode;

	public static DmWarningLite objValueOf(String warnCode) {
		return objValueOf(DmWarningCacher.instance().getDmWarningByWarnCode(warnCode));
	}
	
	public static DmWarningLite objValueOf(DmWarning dmWarning) {
		if (dmWarning != null) {			
			DmWarningLite dmWarningLite = new DmWarningLite();
			BeanUtils.copyProperties(dmWarning, dmWarningLite);
			return dmWarningLite;
		}
		return null;
	}
	
	public void setUsageType(String usageType) {
		this.usageType = usageType;
	}
	public String getUsageType() {
		return usageType;
	}
	public void setWarnMessageEng(String warnMessageEng) {
		this.warnMessageEng = warnMessageEng;
	}
	public String getWarnMessageEng() {
		return warnMessageEng;
	}
	public void setWarnMessageChi(String warnMessageChi) {
		this.warnMessageChi = warnMessageChi;
	}
	public String getWarnMessageChi() {
		return warnMessageChi;
	}
	public void setWarnCode(String warnCode) {
		this.warnCode = warnCode;
	}
	public String getWarnCode() {
		return warnCode;
	}
	public void setWarnCatCode(String warnCatCode) {
		this.warnCatCode = warnCatCode;
	}
	public String getWarnCatCode() {
		return warnCatCode;
	}
}
