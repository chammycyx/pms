package hk.org.ha.model.pms.persistence;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@MappedSuperclass
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class VersionEntity 
	extends BaseEntity
{
	private static final long serialVersionUID = 1L;

	@Version
	@Column(name="VERSION",nullable=false)
	@XmlElement
	private Long version;
	
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}	
}
