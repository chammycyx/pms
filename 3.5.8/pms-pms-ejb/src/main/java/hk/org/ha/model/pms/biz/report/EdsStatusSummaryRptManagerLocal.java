package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;

import java.util.Date;
import java.util.Map;

import javax.ejb.Local;

@Local
public interface EdsStatusSummaryRptManagerLocal {
	
	Map<DispOrderStatus, Long> retrieveDispOrderStatusCount(Date inputDate);
	
	Map<DispOrderItemStatus, Long> retrieveDispOrderItemStatusCount(Date inputDate);	
	
	int retrieveVettedOrderCount(Date inputDate);
	
	int retrieveOutstandOrderCount(Date inputDate);
}
