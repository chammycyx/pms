package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.dms.persistence.DmWarning;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;

import java.util.List;

import javax.ejb.Local;

@Local
public interface WarningManagerLocal {
	List<DmWarning> retrieveLocalWarningList(String itemCode, WorkstoreGroup workstoreGroup);	
	List<DmWarning> retrieveMoeWarmingList(String itemCode);	
}
