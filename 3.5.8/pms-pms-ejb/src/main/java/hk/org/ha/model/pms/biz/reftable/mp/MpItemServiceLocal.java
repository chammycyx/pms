package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.model.pms.persistence.reftable.FdnMapping;
import hk.org.ha.model.pms.persistence.reftable.ItemDispConfig;
import hk.org.ha.model.pms.persistence.reftable.ItemSpecialty;

import java.util.Collection;

import javax.ejb.Local;

@Local
public interface MpItemServiceLocal {

	void retrieveMpItem(String itemCode);
	
	void updateMpItem(Collection<FdnMapping> fdnMappingList, Collection<ItemSpecialty> itemSpecialtyList, 
			ItemDispConfig itemDispConfig, String itemCode);
	
	void clearItemMaint();
	
	void destroy();
	
}
