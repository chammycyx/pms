package hk.org.ha.model.pms.udt.medprofile;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum EndType implements StringValuedEnum {

	EndTimePassed("T", "EndTimePassed"),
	CompleteAdmin("C", "CompleteAdmin"),
	EndNow("E", "EndNow"),
	WardTransfered("W","WardTransfered");
	
    private final String dataValue;
    private final String displayValue;
	
    private EndType(String dataValue, String displayValue) {
    	this.dataValue = dataValue;
    	this.displayValue = displayValue;
    }
    
	@Override
	public String getDataValue() {
		return dataValue;
	}

	@Override
	public String getDisplayValue() {
		return displayValue;
	}
	
	public static EndType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(EndType.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<EndType> {
		
		private static final long serialVersionUID = 1L;

		@Override
    	public Class<EndType> getEnumClass() {
			return EndType.class;
		}
	}
}
