package hk.org.ha.model.pms.persistence.medprofile;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "DELIVERY_SCHEDULE")
@Customizer(AuditCustomizer.class)
public class DeliverySchedule extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "deliveryScheduleSeq")
	@SequenceGenerator(name = "deliveryScheduleSeq", sequenceName = "SQ_DELIVERY_SCHEDULE", initialValue = 100000000)
	private Long id;
	
	@Column(name = "NAME", length = 50)
	private String name;
	
	@Column(name = "DESCRIPTION", length = 100)
	private String description;
	
    @Converter(name = "DeliverySchedule.status", converterClass = RecordStatus.Converter.class)
    @Convert("DeliverySchedule.status")
	@Column(name="STATUS", nullable = false, length = 1)
	private RecordStatus status;
	
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "HOSP_CODE")
    private Hospital hospital;
        
	@OneToMany(mappedBy = "deliverySchedule")
	private List<DeliveryScheduleItem> deliveryScheduleItemList;
		
	public DeliverySchedule() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public RecordStatus getStatus() {
		return status;
	}

	public void setStatus(RecordStatus status) {
		this.status = status;
	}

	public Hospital getHospital() {
		return hospital;
	}

	public void setHospital(Hospital hospital) {
		this.hospital = hospital;
	}

	public List<DeliveryScheduleItem> getDeliveryScheduleItemList() {
		if (deliveryScheduleItemList == null) {
			deliveryScheduleItemList = new ArrayList<DeliveryScheduleItem>();
		}
		return deliveryScheduleItemList;
	}

	public void setDeliveryScheduleItemList(List<DeliveryScheduleItem> deliveryScheduleItemList) {
		this.deliveryScheduleItemList = deliveryScheduleItemList;
	}
	
	// helper methods
	public boolean isNew() {
		return this.getId() == null;
	}
}
