package hk.org.ha.model.pms.vo.rx;

import hk.org.ha.model.pms.corp.cache.DmSiteCacher;
import hk.org.ha.model.pms.corp.cache.DmSupplSiteCacher;
import hk.org.ha.model.pms.dms.persistence.DmSite;
import hk.org.ha.model.pms.dms.persistence.DmSupplSite;

import java.io.Serializable;


import org.apache.commons.lang.StringUtils;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class Site implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String siteCode;	
	
	private String supplSiteDesc;		
	
	private String siteLabelDesc;
	
	private Boolean isSupplSite;
	
	private String siteCategoryCode;
	
	private String ipmoeDesc;
	
	private String siteDescEng;
	
	private DmSite dmSite;//dummy for version 3.2.3 object reference compatibility in xml unmarshalling	
	
	private DmSupplSite dmSupplSite;//dummy for version 3.2.3 object reference compatibility in xml unmarshalling
	
	private void buildSite(DmSite dmSite, DmSupplSite dmSupplSite, boolean isOp) {
		
		if (dmSite != null) {
			this.siteCode = dmSite.getSiteCode();		
			this.siteCategoryCode = dmSite.getSiteCategoryCode();
			this.ipmoeDesc = dmSite.getIpmoeDesc();
			this.siteDescEng = dmSite.getSiteDescEng();
		}
		
		if (dmSupplSite != null) {
			this.isSupplSite = Boolean.TRUE;
			this.supplSiteDesc = dmSupplSite.getCompId().getSupplSiteEng();
			this.siteLabelDesc = dmSupplSite.getCompId().getSupplSiteEng();
		} else {
			this.isSupplSite = Boolean.FALSE;
			this.supplSiteDesc = null;
			if (isOp) {
				this.siteLabelDesc = dmSite.getSiteCode();
			} else {
				if (StringUtils.isBlank(dmSite.getIpmoeDesc())) {
					this.siteLabelDesc = StringUtils.capitalize(dmSite.getSiteDescEng().toLowerCase());
				} else {
					this.siteLabelDesc = dmSite.getIpmoeDesc();
				}
			}
		}
	}
	
	public Site() {
		
	}
	
	public Site(DmSite dmSite, DmSupplSite dmSupplSite, boolean isOp) {
		buildSite(dmSite, dmSupplSite, isOp);
	}
	
	public Site(String siteCode, String supplSiteDesc, boolean isOp) {
		DmSite dmSite = DmSiteCacher.instance().getSiteBySiteCode(siteCode);
		DmSupplSite dmSupplSite = null;		
		if (!StringUtils.isBlank(supplSiteDesc)) {
			dmSupplSite = DmSupplSiteCacher.instance().getSupplSiteBySiteCodeSupplSiteEng(siteCode, supplSiteDesc);
		}		
		buildSite(dmSite, dmSupplSite, isOp);
	}
	
	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}
	
	public String getSiteCode() {
		return siteCode;
	}
	
	public void setSupplSiteDesc(String supplSiteDesc) {
		this.supplSiteDesc = supplSiteDesc;
	}
	
	public String getSupplSiteDesc() {
		return supplSiteDesc;
	}

	public void setSiteLabelDesc(String siteLabelDesc) {
		this.siteLabelDesc = siteLabelDesc;
	}

	public String getSiteLabelDesc() {
		return siteLabelDesc;
	}

	public void setIsSupplSite(Boolean isSupplSite) {
		this.isSupplSite = isSupplSite;
	}

	public Boolean getIsSupplSite() {
		return isSupplSite;
	}

	public void setSiteCategoryCode(String siteCategoryCode) {
		this.siteCategoryCode = siteCategoryCode;
	}

	public String getSiteCategoryCode() {
		return siteCategoryCode;
	}

	public void setIpmoeDesc(String ipmoeDesc) {
		this.ipmoeDesc = ipmoeDesc;
	}

	public String getIpmoeDesc() {
		return ipmoeDesc;
	}

	public void setSiteDescEng(String siteDescEng) {
		this.siteDescEng = siteDescEng;
	}

	public String getSiteDescEng() {
		return siteDescEng;
	}

	public void setDmSite(DmSite dmSite) {
		this.dmSite = dmSite;
	}

	public DmSite getDmSite() {
		return dmSite;
	}

	public void setDmSupplSite(DmSupplSite dmSupplSite) {
		this.dmSupplSite = dmSupplSite;
	}

	public DmSupplSite getDmSupplSite() {
		return dmSupplSite;
	}	
}
