package hk.org.ha.model.pms.udt.medprofile;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum MedProfileTransferAlertType implements StringValuedEnum {

	None("-", "None"),
	Review("R", "Review"),
	Notify("N", "Notify");
	
    private final String dataValue;
    private final String displayValue;
    
	private MedProfileTransferAlertType(String dataValue, String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}

	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}
	
    public static MedProfileTransferAlertType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MedProfileTransferAlertType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<MedProfileTransferAlertType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<MedProfileTransferAlertType> getEnumClass() {
    		return MedProfileTransferAlertType.class;
    	}
    }
}
