package hk.org.ha.model.pms.biz.reftable;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

@Local
public interface ActiveWardManagerLocal {

	boolean isActiveWard(String patHospCode, String pasWardCode);
	boolean isActiveWard(String patHospCode, String pasWardCode, Date date);
	void initInvalidPasWardFlag(MedProfile medProfile);
	void initInvalidPasWardFlag(MedProfile medProfile, Date date);
	Map<String, List<String>> retrieveActiveWardMapByPatHospCodeList(List<String> patHospCodeList);
}
