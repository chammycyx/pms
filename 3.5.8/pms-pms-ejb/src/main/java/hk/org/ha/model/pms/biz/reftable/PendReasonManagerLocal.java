package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.persistence.reftable.PendingReason;

import java.util.Collection;
import java.util.List;

import javax.ejb.Local;

@Local
public interface PendReasonManagerLocal {

	List<PendingReason> retrievePendingReasonList();
	
	void updatePendingReasonList(Collection<PendingReason> newPendingList);
	
}
