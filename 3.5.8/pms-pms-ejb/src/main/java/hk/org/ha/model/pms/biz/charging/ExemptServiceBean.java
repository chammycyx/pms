package hk.org.ha.model.pms.biz.charging;

import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.sys.SysProfile;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.uam.udt.AuthorizeStatus;
import hk.org.ha.model.pms.uam.vo.AuthenticateCriteria;
import hk.org.ha.model.pms.vo.charging.HandleExemptAuthInfo;
import hk.org.ha.service.biz.pms.uam.interfaces.UamServiceJmsRemote;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.drools.util.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("exemptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ExemptServiceBean implements ExemptServiceLocal {
	
	@Logger
	private Log logger;
	
	@In
	private UamServiceJmsRemote uamServiceProxy;
	
	@In
	private UamInfo uamInfo;

	@In
	private SysProfile sysProfile;
	
	public HandleExemptAuthInfo validateHandleExemptAuthInfo(HandleExemptAuthInfo inHandleExemptAuthInfo){
			
		AuthenticateCriteria authenticateCriteria = new AuthenticateCriteria();
		authenticateCriteria.setProfileCode(uamInfo.getProfileCode());
		authenticateCriteria.setUserName(inHandleExemptAuthInfo.getExemptAuthBy());
		authenticateCriteria.setPassword(inHandleExemptAuthInfo.getExemptAuthPw());
		authenticateCriteria.setTarget("sfiExemptReason");
		authenticateCriteria.setAction("Y");
		AuthorizeStatus authStatus = uamServiceProxy.authenticate(authenticateCriteria);
		
		logger.debug("validateHandleExemptAuthInfo: User Role #0, OR Authorize Status #1", uamInfo.getUserRole(), authStatus);		
		
		HandleExemptAuthInfo handleExemptAuthInfo = new HandleExemptAuthInfo();
		handleExemptAuthInfo.setExemptAuthBy( inHandleExemptAuthInfo.getExemptAuthBy() );
		handleExemptAuthInfo.setExemptAuthPw(StringUtils.EMPTY);
		
		if( (sysProfile.isDebugEnabled() && !sysProfile.isTesting()) || AuthorizeStatus.Succeed.equals(authStatus) ){
			handleExemptAuthInfo.setErrMsg(StringUtils.EMPTY);
		}else if ( AuthorizeStatus.Invalid.equals(authStatus) ){		
			handleExemptAuthInfo.setErrMsg("0091");
		}else{
			handleExemptAuthInfo.setErrMsg("0236");
		}
		return handleExemptAuthInfo;
	}
	
	@Remove
	public void destroy() {
	}
}
