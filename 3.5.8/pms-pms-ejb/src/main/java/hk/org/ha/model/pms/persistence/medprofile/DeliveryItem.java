package hk.org.ha.model.pms.persistence.medprofile;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.pivas.MaterialRequestItem;
import hk.org.ha.model.pms.persistence.pivas.PivasWorklist;
import hk.org.ha.model.pms.udt.medprofile.DeliveryItemStatus;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "DELIVERY_ITEM")
public class DeliveryItem extends VersionEntity {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "deliveryItemSeq")
	@SequenceGenerator(name = "deliveryItemSeq", sequenceName = "SQ_DELIVERY_ITEM", initialValue = 100000000)
	private Long id;
	
	@Lob
	@Column(name = "LABEL_XML")
	private String labelXml;
	
	@Column(name = "ISSUE_QTY", nullable = false, precision = 19, scale = 4)
	private BigDecimal issueQty;
	
	@Column(name = "ISSUE_DURATION", nullable = false, precision = 19, scale = 4)
	private BigDecimal issueDuration;
	
    @Converter(name = "DeliveryItem.status", converterClass = DeliveryItemStatus.Converter.class)
    @Convert("DeliveryItem.status")
	@Column(name = "STATUS", nullable = false, length = 1)
	private DeliveryItemStatus status;
	
	@Column(name = "REFILL_FLAG", nullable = false)
	private Boolean refillFlag;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DUE_DATE", nullable = false)
	private Date dueDate;
	
	@Column(name = "ITEM_CODE", length = 6)
	private String itemCode;	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DELIVERY_ID", nullable = false)
	private Delivery delivery;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MED_PROFILE_PO_ITEM_ID")
	private MedProfilePoItem medProfilePoItem;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "REPLENISHMENT_ITEM_ID")
	private ReplenishmentItem replenishmentItem;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PURCHASE_REQUEST_ID")
	private PurchaseRequest purchaseRequest;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MATERIAL_REQUEST_ITEM_ID")
	private MaterialRequestItem materialRequestItem;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PIVAS_WORKLIST_ID")
	private PivasWorklist pivasWorklist;
	
	@Column(name = "PIVAS_WORKLIST_ID", insertable = false, updatable = false)
	private Long pivasWorklistId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MED_PROFILE_ID", nullable = false)
	private MedProfile medProfile;
	
	@Column(name = "BIN_NUM", length = 4)
	private String binNum;
	
	@Column(name = "ADJ_QTY", precision = 19, scale = 4)
	private BigDecimal adjQty;
	
	@Column(name = "CHARGE_QTY", precision = 19, scale = 6)
	private BigDecimal chargeQty;
	
	@Column(name = "PRE_ISSUE_QTY", precision = 19, scale = 4)
	private BigDecimal preIssueQty;
	
	@Transient
	private Date nextDueDate;
	
	@Transient
	private String labelWardCode;
	
	@Transient
	private String labelBedNum;
	
	public DeliveryItem() {
		refillFlag = Boolean.FALSE;
	}
	
	@PrePersist
    public void prePersist() {
		if (medProfilePoItem != null) {
			itemCode = medProfilePoItem.getItemCode();
		}
    }	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLabelXml() {
		return labelXml;
	}

	public void setLabelXml(String labelXml) {
		this.labelXml = labelXml;
	}
	
	public BigDecimal getIssueQty() {
		return issueQty;
	}

	public void setIssueQty(BigDecimal issueQty) {
		this.issueQty = issueQty;
	}
	
	public BigDecimal getIssueDuration() {
		return issueDuration;
	}

	public void setIssueDuration(BigDecimal issueDuration) {
		this.issueDuration = issueDuration;
	}

	public DeliveryItemStatus getStatus() {
		return status;
	}

	public void setStatus(DeliveryItemStatus status) {
		this.status = status;
	}
	
	public Boolean getRefillFlag() {
		return refillFlag;
	}

	public void setRefillFlag(Boolean refillFlag) {
		this.refillFlag = refillFlag;
	}
	
	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	
	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public Delivery getDelivery() {
		return delivery;
	}

	public void setDelivery(Delivery delivery) {
		this.delivery = delivery;
	}

	public MedProfilePoItem getMedProfilePoItem() {
		return medProfilePoItem;
	}

	public void setMedProfilePoItem(MedProfilePoItem medProfilePoItem) {
		this.medProfilePoItem = medProfilePoItem;
	}

	public ReplenishmentItem getReplenishmentItem() {
		return replenishmentItem;
	}

	public void setReplenishmentItem(
			ReplenishmentItem replenishmentItem) {
		this.replenishmentItem = replenishmentItem;
	}

	public BigDecimal getPreIssueQty() {
		return preIssueQty;
	}

	public void setPreIssueQty(BigDecimal preIssueQty) {
		this.preIssueQty = preIssueQty;
	}

	public Date getNextDueDate() {
		return nextDueDate == null ? null : new Date(nextDueDate.getTime());
	}

	public void setNextDueDate(Date nextDueDate) {
		this.nextDueDate = nextDueDate == null ? null : new Date(nextDueDate.getTime());
	}
	
	public String getLabelWardCode() {
		return labelWardCode;
	}

	public void setLabelWardCode(String labelWardCode) {
		this.labelWardCode = labelWardCode;
	}
	
	public String getLabelBedNum() {
		return labelBedNum;
	}

	public void setLabelBedNum(String labelBedNum) {
		this.labelBedNum = labelBedNum;
	}

	@Transient
	public MedProfileMoItem getMedProfileMoItem() {
		
		MedProfilePoItem medProfilePoItem = resolveMedProfilePoItem();
		if (medProfilePoItem != null) {
			return medProfilePoItem.getMedProfileMoItem();
		}
		
		PivasWorklist pivasWorklist = getPivasWorklist();
		return pivasWorklist == null ? null : pivasWorklist.getMedProfileMoItem();
	}
	
	public MedProfilePoItem resolveMedProfilePoItem() {
		
		MedProfilePoItem medProfilePoItem = getMedProfilePoItem();
		ReplenishmentItem replenishmentItem = getReplenishmentItem();
		
		if (medProfilePoItem != null) {
			return medProfilePoItem;
		} else if (replenishmentItem != null) {
			return replenishmentItem.getMedProfilePoItem();
		}
		return null;
	}	

	public void setBinNum(String binNum) {
		this.binNum = binNum;
	}

	public String getBinNum() {
		return binNum;
	}

	public void setAdjQty(BigDecimal adjQty) {
		this.adjQty = adjQty;
	}

	public BigDecimal getAdjQty() {
		return adjQty;
	}

	public void setChargeQty(BigDecimal chargeQty) {
		this.chargeQty = chargeQty;
	}

	public BigDecimal getChargeQty() {
		return chargeQty;
	}

	public void setPurchaseRequest(PurchaseRequest purchaseRequest) {
		this.purchaseRequest = purchaseRequest;
	}

	public PurchaseRequest getPurchaseRequest() {
		return purchaseRequest;
	}
	
	public MaterialRequestItem getMaterialRequestItem() {
		return materialRequestItem;
	}

	public void setMaterialRequestItem(MaterialRequestItem materialRequestItem) {
		this.materialRequestItem = materialRequestItem;
	}

	public PivasWorklist getPivasWorklist() {
		return pivasWorklist;
	}

	public void setPivasWorklist(PivasWorklist pivasWorklist) {
		this.pivasWorklist = pivasWorklist;
	}

	public Long getPivasWorklistId() {
		return pivasWorklistId;
	}

	public void setPivasWorklistId(Long pivasWorklistId) {
		this.pivasWorklistId = pivasWorklistId;
	}

	public void setMedProfile(MedProfile medProfile) {
		this.medProfile = medProfile;
	}

	public MedProfile getMedProfile() {
		return medProfile;
	}
}
