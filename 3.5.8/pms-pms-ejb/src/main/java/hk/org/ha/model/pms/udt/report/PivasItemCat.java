package hk.org.ha.model.pms.udt.report;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PivasItemCat implements StringValuedEnum {

	Drug("A", "Drug"),
	Solvent("B", "Solvent"),
	Diluent("C", "Diluent");
	
    private final String dataValue;
    private final String displayValue;

    PivasItemCat(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static PivasItemCat dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PivasItemCat.class, dataValue);
	}
        
	public static class Converter extends StringValuedEnumConverter<PivasItemCat> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<PivasItemCat> getEnumClass() {
    		return PivasItemCat.class;
    	}
    }
}
