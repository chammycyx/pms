package hk.org.ha.model.pms.persistence.corp;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;


@Entity
@Table(name = "ACTIVE_WARD")
@IdClass(ActiveWardPK.class)
public class ActiveWard extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PAT_HOSP_CODE", nullable = false, length = 3)
	private String patHospCode;
	
	@Id
	@Column(name = "PAS_WARD_CODE", nullable = false, length = 4)
	private String pasWardCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "EFFECTIVE_DATE", nullable = false)
	private Date effectiveDate;
	
	@Converter(name = "ActiveWard.status", converterClass = RecordStatus.Converter.class)
    @Convert("ActiveWard.status")
	@Column(name="STATUS", nullable = false, length = 1)
	private RecordStatus status;
	
	public ActiveWard() {
		status = RecordStatus.Active;
	}

	public ActiveWard(String patHospCode, String pasWardCode,
			Date effectiveDate) {
		this();
		this.patHospCode = patHospCode;
		this.pasWardCode = pasWardCode;
		this.effectiveDate = effectiveDate;
	}

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public String getPasWardCode() {
		return pasWardCode;
	}

	public void setPasWardCode(String pasWardCode) {
		this.pasWardCode = pasWardCode;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	
	public ActiveWardPK getId(){
		return new ActiveWardPK(this.patHospCode, this.pasWardCode);
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder()
			.append(patHospCode)
			.append(pasWardCode)
			.append(effectiveDate)
			.toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		ActiveWard other = (ActiveWard) obj;
		return new EqualsBuilder()
			.append(patHospCode,   other.getPatHospCode())
			.append(pasWardCode,   other.getPasWardCode())
			.append(effectiveDate, other.getEffectiveDate())
			.isEquals();
	}

	public void setStatus(RecordStatus status) {
		this.status = status;
	}

	public RecordStatus getStatus() {
		return status;
	}
}
