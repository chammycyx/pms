package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class DayEndDefDoctorRpt implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private DayEndDefDoctorRptHdr dayEndDefDoctorRptHdr;	
	private List<DayEndDefDoctorRptDtl> dayEndDefDoctorRptDtlList;
	private DayEndDefDoctorRptFtr dayEndDefDoctorRptFtr;
	public DayEndDefDoctorRptHdr getDayEndDefDoctorRptHdr() {
		return dayEndDefDoctorRptHdr;
	}
	public void setDayEndDefDoctorRptHdr(DayEndDefDoctorRptHdr dayEndDefDoctorRptHdr) {
		this.dayEndDefDoctorRptHdr = dayEndDefDoctorRptHdr;
	}
	public List<DayEndDefDoctorRptDtl> getDayEndDefDoctorRptDtlList() {
		return dayEndDefDoctorRptDtlList;
	}
	public void setDayEndDefDoctorRptDtlList(
			List<DayEndDefDoctorRptDtl> dayEndDefDoctorRptDtlList) {
		this.dayEndDefDoctorRptDtlList = dayEndDefDoctorRptDtlList;
	}
	public DayEndDefDoctorRptFtr getDayEndDefDoctorRptFtr() {
		return dayEndDefDoctorRptFtr;
	}
	public void setDayEndDefDoctorRptFtr(DayEndDefDoctorRptFtr dayEndDefDoctorRptFtr) {
		this.dayEndDefDoctorRptFtr = dayEndDefDoctorRptFtr;
	}	
}
