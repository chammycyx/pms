package hk.org.ha.model.pms.persistence.reftable;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.YesNoCancelFlag;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "REFILL_CONFIG")
@Customizer(AuditCustomizer.class)
public class RefillConfig extends VersionEntity {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "refillConfigSeq")
	@SequenceGenerator(name = "refillConfigSeq", sequenceName = "SQ_REFILL_CONFIG", initialValue = 100000000)
	private Long id;
	
	@Column(name = "ENABLE_SELECTION_FLAG", nullable = false)
	private Boolean enableSelectionFlag;
	
	@Column(name = "THRESHOLD", nullable = false, length = 3)
	private Integer threshold;
	
	@Column(name = "INTERVAL", nullable = false, length = 3)
	private Integer interval;
	
	@Column(name = "LAST_INTERVAL_MAX_DAY", nullable = false, length = 3)
	private Integer lastIntervalMaxDay;
	
	@Column(name = "ENABLE_QTY_ADJ_FLAG", nullable = false)
	private Boolean enableQtyAdjFlag;
	
	@Column(name = "LATE_REFILL_ALERT_DAY", nullable = false, length = 3)
	private Integer lateRefillAlertDay;
	
	@Column(name = "ABORT_REFILL_DAY", nullable = false, length = 3)
	private Integer abortRefillDay;
	
	@Column(name = "MANU_RX_NO_PAS_SPEC_FLAG", nullable = false)
	private Boolean manuRxNoPasSpecFlag;
	
	@Column(name = "ENABLE_PHARM_CLINIC_FLAG", nullable = false)
	private Boolean enablePharmClinicFlag;
	
	@Column(name = "PRINT_REMINDER_FLAG", nullable = false)
	private Boolean printReminderFlag;
	
	@Converter(name = "YesNoCancelFlag.defaultLateRefillAlertBtn", converterClass = YesNoCancelFlag.Converter.class)
    @Convert("YesNoCancelFlag.defaultLateRefillAlertBtn")
	@Column(name = "DEFAULT_LATE_REFILL_ALERT_BTN", nullable = false, length = 1)
	private YesNoCancelFlag defaultLateRefillAlertBtn;
	
	@Column(name = "ENABLE_ADJ_HOLIDAY_FLAG", nullable = false)
	private Boolean enableAdjHolidayFlag;
	
	@Column(name = "ENABLE_ADJ_SUNDAY_FLAG", nullable = false)
	private Boolean enableAdjSundayFlag;
	
	@Column(name = "COUPON_PRINT_COPIES", nullable = false, length = 1)
	private Integer couponPrintCopies;

    @OneToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE", referencedColumnName = "HOSP_CODE", nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
    private Workstore workstore;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getEnableSelectionFlag() {
		return enableSelectionFlag;
	}

	public void setEnableSelectionFlag(Boolean enableSelectionFlag) {
		this.enableSelectionFlag = enableSelectionFlag;
	}

	public Integer getThreshold() {
		return threshold;
	}

	public void setThreshold(Integer threshold) {
		this.threshold = threshold;
	}

	public Integer getInterval() {
		return interval;
	}

	public void setInterval(Integer interval) {
		this.interval = interval;
	}

	public Integer getLastIntervalMaxDay() {
		return lastIntervalMaxDay;
	}

	public void setLastIntervalMaxDay(Integer lastIntervalMaxDay) {
		this.lastIntervalMaxDay = lastIntervalMaxDay;
	}

	public Boolean getEnableQtyAdjFlag() {
		return enableQtyAdjFlag;
	}

	public void setEnableQtyAdjFlag(Boolean enableQtyAdjFlag) {
		this.enableQtyAdjFlag = enableQtyAdjFlag;
	}

	public Integer getLateRefillAlertDay() {
		return lateRefillAlertDay;
	}

	public void setLateRefillAlertDay(Integer lateRefillAlertDay) {
		this.lateRefillAlertDay = lateRefillAlertDay;
	}

	public Integer getAbortRefillDay() {
		return abortRefillDay;
	}

	public void setAbortRefillDay(Integer abortRefillDay) {
		this.abortRefillDay = abortRefillDay;
	}

	public Boolean getManuRxNoPasSpecFlag() {
		return manuRxNoPasSpecFlag;
	}

	public void setManuRxNoPasSpecFlag(Boolean manuRxNoPasSpecFlag) {
		this.manuRxNoPasSpecFlag = manuRxNoPasSpecFlag;
	}

	public Boolean getEnablePharmClinicFlag() {
		return enablePharmClinicFlag;
	}

	public void setEnablePharmClinicFlag(Boolean enablePharmClinicFlag) {
		this.enablePharmClinicFlag = enablePharmClinicFlag;
	}

	public Boolean getPrintReminderFlag() {
		return printReminderFlag;
	}

	public void setPrintReminderFlag(Boolean printReminderFlag) {
		this.printReminderFlag = printReminderFlag;
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}

	public void setDefaultLateRefillAlertBtn(YesNoCancelFlag defaultLateRefillAlertBtn) {
		this.defaultLateRefillAlertBtn = defaultLateRefillAlertBtn;
	}

	public YesNoCancelFlag getDefaultLateRefillAlertBtn() {
		return defaultLateRefillAlertBtn;
	}

	public void setEnableAdjHolidayFlag(Boolean enableAdjHolidayFlag) {
		this.enableAdjHolidayFlag = enableAdjHolidayFlag;
	}

	public Boolean getEnableAdjHolidayFlag() {
		return enableAdjHolidayFlag;
	}

	public void setEnableAdjSundayFlag(Boolean enableAdjSundayFlag) {
		this.enableAdjSundayFlag = enableAdjSundayFlag;
	}

	public Boolean getEnableAdjSundayFlag() {
		return enableAdjSundayFlag;
	}

	public void setCouponPrintCopies(Integer couponPrintCopies) {
		this.couponPrintCopies = couponPrintCopies;
	}

	public Integer getCouponPrintCopies() {
		return couponPrintCopies;
	}
}
