package hk.org.ha.model.pms.biz.inbox;

import hk.org.ha.fmk.pms.jms.MessageCreator;
import hk.org.ha.fmk.pms.jms.MessageProducerInf;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedOrder;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.naming.NamingException;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;


@AutoCreate
@Name("pharmInboxClientNotifier")
@Scope(ScopeType.APPLICATION)
public class PharmInboxClientNotifier implements PharmInboxClientNotifierInf {

	@Logger
	private Log logger;

	@In
    private MessageProducerInf pharmInboxGravityMessageProducer;   
    
    public void notifyClient(Serializable object, Map<String, Object> headerParams) {
    	String errorString = "Could not publish notification";
        try {
        	pharmInboxGravityMessageProducer.send(new InnerMessageCreator(object, headerParams));
        } catch (JMSException e) {
        	logger.error(errorString, e);
        } catch (NamingException e) {
        	logger.error(errorString, e);
        }
    }
    
    public void dispatchUpdateEvent(String hospCode) {
    	
		Map<String, Object> headerParams = new HashMap<String, Object>();
		headerParams.put("HOSP_CODE", hospCode);
		
		notifyClient("NOTIFICATION FROM PharmInboxClientNotifier",
				headerParams);
    }
    
	public void dispatchUpdateEvent(Set<String> hospCodeSet) {
		
		for (String hospCode: hospCodeSet) {
			dispatchUpdateEvent(hospCode);
		}
	}    

	public void dispatchUpdateEvent(MedOrder medOrder) {
		try {
			dispatchUpdateEvent(medOrder.getWorkstore());
			logger.info("Notification has been sent for discharge order #0", medOrder.getOrderNum());
		} catch (Exception ex) {
			logger.error(ex, ex);
		}
	}
	
	public void dispatchUpdateEvent(Workstore workstore) {
		
		Map<String, Object> headerParams = new HashMap<String, Object>();
		
		headerParams.put("HOSP_CODE", workstore.getHospCode());
		headerParams.put("WORKSTORE_CODE", workstore.getWorkstoreCode());
		
		notifyClient("NOTIFICATION FROM PharmInboxClientNotifier",
				headerParams);
	}
    
    public static class InnerMessageCreator implements MessageCreator {

    	private Serializable object;
    	private Map<String, Object> headerParams;
    	
    	public InnerMessageCreator(Serializable object, Map<String, Object> headerParams) {
    		this.object = object;
    		this.headerParams = headerParams;
    	}
    	
		public Message createMessage(Session session) throws JMSException {
			
			Message message = session.createObjectMessage(object);
			
			for (Entry<String, Object> entry: headerParams.entrySet()) {
				message.setObjectProperty(entry.getKey(), entry.getValue());
			}
			return message;
		}
    }
}
