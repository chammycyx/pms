package hk.org.ha.model.pms.persistence.reftable;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.fmk.pms.util.IntegerCollectionConverter;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;
import org.jboss.seam.security.Identity;

@Entity
@Table(name = "PREPACK")
@Customizer(AuditCustomizer.class)
public class Prepack extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "prepackSeq")
	@SequenceGenerator(name = "prepackSeq", sequenceName = "SQ_PREPACK", initialValue = 100000000)
	private Long id;
	
	@Column(name = "ITEM_CODE", nullable = false, length = 6)
	private String itemCode;
	
	@Converter(name = "Prepack.prepackQty", converterClass = IntegerCollectionConverter.class)
	@Convert("Prepack.prepackQty")
	@Column(name = "PREPACK_QTY", length = 1000)
	private List<Integer> prepackQty;

	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE",      referencedColumnName = "HOSP_CODE",      nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;

	@Transient
	private DmDrug dmDrug;	
	
	@Transient
	private boolean markDelete = false;	
	
	@PostLoad
	public void postLoad() {
		if (itemCode != null && Identity.instance().isLoggedIn()) {
			dmDrug = DmDrugCacher.instance().getDmDrug(itemCode);
		}
	}	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public List<Integer> getPrepackQty() {
		return prepackQty;
	}

	public void setPrepackQty(List<Integer> prepackQty) {
		this.prepackQty = prepackQty;
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}

	public void setMarkDelete(boolean markDelete) {
		this.markDelete = markDelete;
	}

	public boolean isMarkDelete() {
		return markDelete;
	}

	public void setDmDrug(DmDrug dmDrug) {
		this.dmDrug = dmDrug;
	}

	public DmDrug getDmDrug() {
		return dmDrug;
	}
}
