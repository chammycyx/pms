package hk.org.ha.model.pms.biz.onestop;

import java.util.Date;

import javax.ejb.Local;

@Local
public interface ReprintServiceLocal {
	
	void clear();
	
	void retrieveRefillScheduleListForReprint(Long id);
	
	void retrieveDispOrderByTicketNum(Date ticketDate, String ticketNum, Boolean triggerByOtherScreen);
		
	void destroy();
		
}
