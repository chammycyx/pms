package hk.org.ha.model.pms.vo.vetting.mp;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class MedProfileVettingContext implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String oneTimeMdsAckPermissionUser;

	private boolean oneTimePendPermission;
	
	private boolean oneTimeCancelPendPermission;
	
	public String getOneTimeMdsAckPermissionUser() {
		return oneTimeMdsAckPermissionUser;
	}

	public void setOneTimeMdsAckPermissionUser(String oneTimeMdsAckPermissionUser) {
		this.oneTimeMdsAckPermissionUser = oneTimeMdsAckPermissionUser;
	}

	public boolean isOneTimePendPermission() {
		return oneTimePendPermission;
	}

	public void setOneTimePendPermission(boolean oneTimePendPermission) {
		this.oneTimePendPermission = oneTimePendPermission;
	}

	public boolean isOneTimeCancelPendPermission() {
		return oneTimeCancelPendPermission;
	}

	public void setOneTimeCancelPendPermission(boolean oneTimeCancelPendPermission) {
		this.oneTimeCancelPendPermission = oneTimeCancelPendPermission;
	}
}
