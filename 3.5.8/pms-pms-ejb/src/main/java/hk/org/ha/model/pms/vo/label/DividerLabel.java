package hk.org.ha.model.pms.vo.label;

import hk.org.ha.model.pms.vo.printing.PrintOption;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class DividerLabel implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String hospName;
	
	private String batchNum;
	
	private Date batchDate;
 
	private String workstoreCode;
 
	private Date printDate;

	private Integer numOfItems;

	private Integer numOfLabels;
	
	private transient PrintOption printOption;
	
	public PrintOption getPrintOption() {
		return printOption;
	}

	public void setPrintOption(PrintOption printOption) {
		this.printOption = printOption;
	}

	public String getHospName() {
		return hospName;
	}

	public void setHospName(String hospName) {
		this.hospName = hospName;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public Date getPrintDate() {
		return printDate;
	}

	public void setPrintDate(Date printDate) {
		this.printDate = printDate;
	}

	public Integer getNumOfItems() {
		return numOfItems;
	}

	public void setNumOfItems(Integer numOfItems) {
		this.numOfItems = numOfItems;
	}

	public Integer getNumOfLabels() {
		return numOfLabels;
	}

	public void setNumOfLabels(Integer numOfLabels) {
		this.numOfLabels = numOfLabels;
	}

	public void setBatchDate(Date batchDate) {
		this.batchDate = batchDate;
	}

	public Date getBatchDate() {
		return batchDate;
	}

}