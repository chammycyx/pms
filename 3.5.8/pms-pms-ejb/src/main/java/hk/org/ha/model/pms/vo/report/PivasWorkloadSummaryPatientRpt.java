package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class PivasWorkloadSummaryPatientRpt implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private PivasRptHdr pivasRptHdr;
	
	private List<PivasWorkloadSummaryPatientRptDtl> pivasWorkloadSummaryPatientRptDtlList;
	
	private List<PivasWorkloadSummaryPatientRptWardSummaryDtl> pivasWorkloadSummaryPatientRptWardSummaryDtlList;
	
	private List<PivasWorkloadSummaryPatientRptDtl> pivasWorkloadSummaryPatientRptXlsList;
	
	public PivasRptHdr getPivasRptHdr() {
		return pivasRptHdr;
	}

	public void setPivasRptHdr(PivasRptHdr pivasRptHdr) {
		this.pivasRptHdr = pivasRptHdr;
	}

	public List<PivasWorkloadSummaryPatientRptDtl> getPivasWorkloadSummaryPatientRptDtlList() {
		return pivasWorkloadSummaryPatientRptDtlList;
	}

	public void setPivasWorkloadSummaryPatientRptDtlList(
			List<PivasWorkloadSummaryPatientRptDtl> pivasWorkloadSummaryPatientRptDtlList) {
		this.pivasWorkloadSummaryPatientRptDtlList = pivasWorkloadSummaryPatientRptDtlList;
	}

	public List<PivasWorkloadSummaryPatientRptWardSummaryDtl> getPivasWorkloadSummaryPatientRptWardSummaryDtlList() {
		return pivasWorkloadSummaryPatientRptWardSummaryDtlList;
	}

	public void setPivasWorkloadSummaryPatientRptWardSummaryDtlList(
			List<PivasWorkloadSummaryPatientRptWardSummaryDtl> pivasWorkloadSummaryPatientRptWardSummaryDtlList) {
		this.pivasWorkloadSummaryPatientRptWardSummaryDtlList = pivasWorkloadSummaryPatientRptWardSummaryDtlList;
	}

	public List<PivasWorkloadSummaryPatientRptDtl> getPivasWorkloadSummaryPatientRptXlsList() {
		return pivasWorkloadSummaryPatientRptXlsList;
	}

	public void setPivasWorkloadSummaryPatientRptXlsList(
			List<PivasWorkloadSummaryPatientRptDtl> pivasWorkloadSummaryPatientRptXlsList) {
		this.pivasWorkloadSummaryPatientRptXlsList = pivasWorkloadSummaryPatientRptXlsList;
	}
	
}
