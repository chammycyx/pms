package hk.org.ha.model.pms.biz.inbox;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.biz.reftable.MpWardManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

/**
 * Session Bean implementation class CancelDischargeTransferServiceBean
 */
@Stateful
@Scope(ScopeType.SESSION)
@Name("cancelDischargeTransferService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CancelDischargeTransferServiceBean implements CancelDischargeTransferServiceLocal {

	private static final String MED_PROFILE_QUERY_HINT_FETCH_1 = "o.medCase";
	private static final String MED_PROFILE_QUERY_HINT_FETCH_2 = "o.patient";	
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private CategorySummaryLocal categorySummary;
	
	@In
	private MpWardManagerLocal mpWardManager;
	
	@In
	private Workstore workstore;
	
	@In
	private Boolean showAllWards;
	
	@In
	private Boolean showChemoProfileOnly;
	
	@Out(required = false)
	protected List<MedProfile> cancelDischargeTransferList; 
	
    public CancelDischargeTransferServiceBean() {
        // TODO Auto-generated constructor stub
    }
    
	@Override
    @SuppressWarnings("unchecked")
    public void retrieveCancelDischargeTransferList() {
    	
    	cancelDischargeTransferList = em.createQuery(
    			"select o from MedProfile o" + // 20140128 index check : MedProfile.hospCode,cancelDischargeDate MedProfile.hospCode,confirmCancelDischargeDate : I_MED_PROFILE_04 I_MED_PROFILE_05
    			" where o.hospCode = :hospCode" +
    			" and o.cancelDischargeDate is not null" +
    			" and (o.confirmCancelDischargeDate > :startDate" +
    			"   or o.confirmCancelDischargeDate is null)" +
    			" and (o.wardCode is null or o.wardCode in " +
		    	"  (select w.wardCode from Ward w" +
		    	"   where w.institution = :institution))" +    			
    			" order by o.cancelDischargeDate")
    			.setParameter("hospCode", workstore.getHospCode())
    			.setParameter("institution", workstore.getWorkstoreGroup().getInstitution())
    			.setParameter("startDate", MedProfileUtils.getTodayBegin())
    			.setHint(QueryHints.FETCH, MED_PROFILE_QUERY_HINT_FETCH_1)
    			.setHint(QueryHints.FETCH, MED_PROFILE_QUERY_HINT_FETCH_2)
    			.getResultList();
    	
    	categorySummary.countCategories(null, workstore, showChemoProfileOnly, mpWardManager.retrieveWardFilterList(workstore, showAllWards));
    }

	@Remove
	@Override
	public void destroy() {
		cancelDischargeTransferList = null;
	}	    
}
