package hk.org.ha.model.pms.vo.cddh;

import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.udt.Gender;
import hk.org.ha.model.pms.udt.YesNoBlankFlag;
import hk.org.ha.model.pms.udt.cddh.ItemDescFilterMode;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class CddhCriteria implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String patKey;
	
	private String hkid;
	
	private String caseNum;
	
	private String patientHospcode;
	
	private String patientName;

	private Date dispDateStart;
	
	private Date dispDateEnd;

	private Date defaultDispDateStart;
	
	private Date defaultDispDateEnd;
	
	private String nameOnLabel;
	
	private Date labelDob;
	 
	private Date labelDispDateStart;
	
	private Date labelDispDateEnd;
	
	private String labelHospcode;
	
	private String itemCode;
	
	private YesNoBlankFlag dangerDrugFlag;
	
	private String itemDesc;
	
	private ItemDescFilterMode itemDescFilterMode;
	
	private boolean except;

	// for searching by patName
	private Gender gender;
	private String fromAge;
	private String toAge;

	// for name on label, exception search
	private Long dispOrderId;
	private String dispHospCode;
	private String dispOrderPatName;
	private Date patListDispDate;
	
	// property value from propCacher
	private Boolean legacyCddhIncluded;
	private Boolean hkpmiEnabled;
	
	private List<Patient> patList;
	
	private String loginHospCode;
	
	//for auditlog purpose
	private String loginWorkstoreCode;
	private String userCode;
	private String workstationCode;
	private Boolean retrieveFromPmsFlag;
	
		
	public CddhCriteria() {
		except = false;
	}
	
	public List<Patient> getPatList() {
		return patList;
	}

	public void setPatList(List<Patient> patList) {
		this.patList = patList;
	}

	public Date getPatListDispDate() {
		return patListDispDate;
	}

	public void setPatListDispDate(Date patListDispDate) {
		this.patListDispDate = patListDispDate;
	}

	public String getDispOrderPatName() {
		return dispOrderPatName;
	}

	public void setDispOrderPatName(String dispOrderPatName) {
		this.dispOrderPatName = dispOrderPatName;
	}

	public String getDispHospCode() {
		return dispHospCode;
	}

	public void setDispHospCode(String dispHospCode) {
		this.dispHospCode = dispHospCode;
	}

	public String getPatKey() {
		return patKey;
	}

	public void setPatKey(String patKey) {
		this.patKey = patKey;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getHkid() {
		return hkid;
	}
	
	public void setHkid(String hkid) {
		this.hkid = hkid;
	}
	
	public String getCaseNum() {
		return caseNum;
	}
	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}
	
	public String getPatientHospcode() {
		return patientHospcode;
	}
	
	public void setPatientHospcode(String patientHospcode) {
		this.patientHospcode = patientHospcode;
	}
	
	public String getPatientName() {
		return patientName;
	}
	
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	
	public Date getDispDateStart() {
		return dispDateStart;
	}
	
	public void setDispDateStart(Date dispDateStart) {
		this.dispDateStart = dispDateStart;
	}
	
	public Date getDispDateEnd() {
		return dispDateEnd;
	}
	
	public void setDispDateEnd(Date dispDateEnd) {
		this.dispDateEnd = dispDateEnd;
	}
	
	public Date getDefaultDispDateStart() {
		return defaultDispDateStart;
	}

	public void setDefaultDispDateStart(Date defaultDispDateStart) {
		this.defaultDispDateStart = defaultDispDateStart;
	}

	public Date getDefaultDispDateEnd() {
		return defaultDispDateEnd;
	}

	public void setDefaultDispDateEnd(Date defaultDispDateEnd) {
		this.defaultDispDateEnd = defaultDispDateEnd;
	}

	public String getNameOnLabel() {
		return nameOnLabel;
	}
	
	public void setNameOnLabel(String nameOnLabel) {
		this.nameOnLabel = nameOnLabel;
	}
	
	public Date getLabelDob() {
		return labelDob;
	}
	
	public void setLabelDob(Date labelDob) {
		this.labelDob = labelDob;
	}
	
	public Date getLabelDispDateStart() {
		return labelDispDateStart;
	}
	
	public void setLabelDispDateStart(Date labelDispDateStart) {
		this.labelDispDateStart = labelDispDateStart;
	}
	
	public Date getLabelDispDateEnd() {
		return labelDispDateEnd;
	}
	
	public void setLabelDispDateEnd(Date labelDispDateEnd) {
		this.labelDispDateEnd = labelDispDateEnd;
	}
	
	public String getLabelHospcode() {
		return labelHospcode;
	}
	
	public void setLabelHospcode(String labelHospcode) {
		this.labelHospcode = labelHospcode;
	}

	public YesNoBlankFlag getDangerDrugFlag() {
		return dangerDrugFlag;
	}

	public void setDangerDrugFlag(YesNoBlankFlag dangerDrugFlag) {
		this.dangerDrugFlag = dangerDrugFlag;
	}
	
	public Long getDispOrderId() {
		return dispOrderId;
	}

	public void setDispOrderId(Long dispOrderId) {
		this.dispOrderId = dispOrderId;
	}
	
	public boolean isExcept() {
		return except;
	}

	public void setExcept(boolean except) {
		this.except = except;
	}
	
	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getFromAge() {
		return fromAge;
	}

	public void setFromAge(String fromAge) {
		this.fromAge = fromAge;
	}

	public String getToAge() {
		return toAge;
	}

	public void setToAge(String toAge) {
		this.toAge = toAge;
	}

	public boolean isLegacyCddhIncluded() {
		return legacyCddhIncluded;
	}

	public void setLegacyCddhIncluded(boolean legacyCddhIncluded) {
		this.legacyCddhIncluded = legacyCddhIncluded;
	}

	public boolean isHkpmiEnabled() {
		return hkpmiEnabled;
	}

	public void setHkpmiEnabled(boolean hkpmiEnabled) {
		this.hkpmiEnabled = hkpmiEnabled;
	}

	public String getItemDesc() {
		return itemDesc;
	}
	
	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	public ItemDescFilterMode getItemDescFilterMode() {
		return itemDescFilterMode;
	}

	public void setItemDescFilterMode(ItemDescFilterMode itemDescFilterMode) {
		this.itemDescFilterMode = itemDescFilterMode;
	}

	public String getLoginHospCode() {
		return loginHospCode;
	}

	public void setLoginHospCode(String loginHospCode) {
		this.loginHospCode = loginHospCode;
	}

	public String getLoginWorkstoreCode() {
		return loginWorkstoreCode;
	}

	public void setLoginWorkstoreCode(String loginWorkstoreCode) {
		this.loginWorkstoreCode = loginWorkstoreCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getWorkstationCode() {
		return workstationCode;
	}

	public void setWorkstationCode(String workstationCode) {
		this.workstationCode = workstationCode;
	}
	
	public Boolean getRetrieveFromPmsFlag() {
		return (retrieveFromPmsFlag == null ? Boolean.TRUE : retrieveFromPmsFlag);
	}

	public void setRetrieveFromPmsFlag(Boolean retrieveFromPmsFlag) {
		this.retrieveFromPmsFlag = retrieveFromPmsFlag;
	}
}
