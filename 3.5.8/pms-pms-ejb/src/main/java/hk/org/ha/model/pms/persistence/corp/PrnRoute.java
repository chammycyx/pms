package hk.org.ha.model.pms.persistence.corp;

import flexjson.JSON;
import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.RecordStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "PRN_ROUTE")
@Customizer(AuditCustomizer.class)
@IdClass(PrnRoutePK.class)
public class PrnRoute extends VersionEntity {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "HOSP_CODE", nullable = false, length=3)
	private String hospCode;
	
	@Id
	@Column(name = "WORKSTORE_CODE", nullable = false, length=4)
	private String workstoreCode;
	
	@Id
	@Column(name = "ROUTE_CODE", nullable = false, length = 30)
	private String routeCode;

	@Column(name = "ROUTE_DESC", nullable = false, length = 20)
	private String routeDesc;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE", referencedColumnName = "HOSP_CODE", nullable = false, insertable = false, updatable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false, insertable = false, updatable = false)
	})
	private Workstore workstore;
	
	@Converter(name = "PrnRoute.status", converterClass = RecordStatus.Converter.class)
    @Convert("PrnRoute.status")
	@Column(name="STATUS", nullable = false, length = 1)
	private RecordStatus status;
	
	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public String getRouteCode() {
		return routeCode;
	}	

	public void setRouteCode(String routeCode) {
		this.routeCode = routeCode;
	}

	@JSON(include=false)
	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}

	public RecordStatus getStatus() {
		return status;
	}

	public void setStatus(RecordStatus status) {
		this.status = status;
	}

	public String getRouteDesc() {
		return routeDesc;
	}

	public void setRouteDesc(String routeDesc) {
		this.routeDesc = routeDesc;
	}
	
}
