package hk.org.ha.model.pms.biz.delivery;

import static hk.org.ha.model.pms.prop.Prop.LABEL_MPDISPLABEL_LARGEFONT_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.LABEL_MPDISPLABEL_WARDRTNBARCODE_VISIBLE;
import static hk.org.ha.model.pms.prop.Prop.LABEL_PIVASWORKSHEET_LARGEFONT_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.MACHINE_ATDPS_MESSAGE_ENCODING;
import static hk.org.ha.model.pms.prop.Prop.MACHINE_ATDPS_PORT;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.checkissue.mp.CheckManagerLocal;
import hk.org.ha.model.pms.biz.label.DispLabelBuilderLocal;
import hk.org.ha.model.pms.biz.label.LabelManagerLocal;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.biz.reftable.ItemLocationListManagerLocal;
import hk.org.ha.model.pms.biz.reftable.MpWardManagerLocal;
import hk.org.ha.model.pms.biz.reftable.RefTableManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.MsWorkstoreDrug;
import hk.org.ha.model.pms.exception.delivery.DueOrderPrintException;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.medprofile.Delivery;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.persistence.medprofile.ReplenishmentItem;
import hk.org.ha.model.pms.persistence.reftable.ItemLocation;
import hk.org.ha.model.pms.persistence.reftable.Machine;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.persistence.reftable.OperationWorkstation;
import hk.org.ha.model.pms.udt.machine.AtdpsPrintType;
import hk.org.ha.model.pms.udt.medprofile.DeliveryItemStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatus;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.printing.PrintStatus;
import hk.org.ha.model.pms.udt.printing.PrintType;
import hk.org.ha.model.pms.udt.reftable.MachineType;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.report.ReportFilterOption;
import hk.org.ha.model.pms.vo.label.LabelContainer;
import hk.org.ha.model.pms.vo.label.LabelReprintMedProfileMoItem;
import hk.org.ha.model.pms.vo.label.MpDispLabel;
import hk.org.ha.model.pms.vo.label.MpDispLabelQrCode;
import hk.org.ha.model.pms.vo.label.PivasProductLabel;
import hk.org.ha.model.pms.vo.machine.AtdpsMessage;
import hk.org.ha.model.pms.vo.machine.AtdpsMessageContent;
import hk.org.ha.model.pms.vo.medprofile.Ward;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.PrinterSelect;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.rx.RxItem;
import hk.org.ha.service.pms.asa.interfaces.machine.MachineServiceJmsRemote;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;
import org.joda.time.DateTime;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("labelReprintService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class LabelReprintServiceBean implements LabelReprintServiceLocal {

	private static final String JAXB_CONTEXT_LABEL = "hk.org.ha.model.pms.vo.label";
	private static final String JAXB_CONTEXT_RX = "hk.org.ha.model.pms.vo.rx";	
	private static final String MP_DISP_LABEL = "MpDispLabel";
	private static final String MP_DISP_LABEL_NO_INST = "MpDispLabelNoInst";
	private static final String ATDPS_BINNUM = "-";
	private static final String MDS_OVERRIDE_REASON_LABEL = "MdsOverrideReasonLabel";

	@Logger
	private Log logger;
	
	@In
	private PrintAgentInf printAgent;
	
	@In
	private DispLabelBuilderLocal dispLabelBuilder;
	
	@In
	private MpWardManagerLocal mpWardManager;
	
	@In
	private DeliveryManagerLocal deliveryManager;
	
	@In 
	private Workstore workstore;
	
	@In
	private AuditLogger auditLogger; 
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private LabelManagerLocal labelManager;
	
	@In
	private CheckManagerLocal checkManager;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
			
	@In
	private MachineServiceJmsRemote machineServiceProxy;
			
	@In
	private List<Integer> atdpsPickStationsNumList;
	
	@In
	private ItemLocationListManagerLocal itemLocationListManager;
	
	@In
	private RefTableManagerLocal refTableManager;
	
	@In
	private OperationProfile activeProfile;
	
	private List<Ward> labelReprintWardSelectionList;
	
	@Out(required = false)
	private List<Delivery> labelReprintDeliveryList = null;
	
	@Out(required = false)
	private List<MpDispLabel> mpDispLabelList = null;
	
	@Out(required = false)
	private boolean isCheckedBatch = false;
	
	private Delivery delivery;
	
	private TransactionListBatchNumComparator transactionListBatchNumComparator = new TransactionListBatchNumComparator();
	
	private MpDispLabelComparator mpDispLabelComparator = new MpDispLabelComparator();
	
	private TempMpDispLabelComparator tempMpDispLabelComparator = new TempMpDispLabelComparator();
	
	private DateFormat df = new SimpleDateFormat("yyyyMMdd"); 
	
	private List<Machine> activeMachineList;
	
	public List<Ward> retrieveWardSelectionList() {
		labelReprintWardSelectionList = mpWardManager.retrieveActiveWardList(workstore.getWorkstoreGroup());
		labelReprintWardSelectionList.add(0, new Ward(ReportFilterOption.All.getDataValue()));
		return labelReprintWardSelectionList;
	}
	
	public void retrieveDeliveryList(String wardCode, String batchCode, String caseNum, Date dispenseDate, String trxId) {
		TreeMap<Long, Delivery> deliveryMap = new TreeMap<Long, Delivery>();
		
		List<DeliveryItem> deliveryItemList = deliveryManager.retrieveDeliveryItemListForLabelReprint(wardCode, batchCode, caseNum, dispenseDate, trxId);
		for ( DeliveryItem deliveryItem : deliveryItemList ){
			deliveryMap.put(deliveryItem.getDelivery().getId(),deliveryItem.getDelivery());
		}
				
		deliveryMap = deliveryManager.retrieveDeliveryNumOfLabelForLabelReprint(deliveryMap, caseNum, trxId);
		
		labelReprintDeliveryList = new ArrayList<Delivery>();
		labelReprintDeliveryList.addAll(deliveryMap.values());
		
		Collections.sort(labelReprintDeliveryList, transactionListBatchNumComparator);
		
		mpDispLabelList = new ArrayList<MpDispLabel>();
		
		auditLogger.log("#0290", wardCode, caseNum, batchCode);
	}
	
	public void retrieveMpDispLabelListByBatchNum(String batchNum, Date dispenseDate, String caseNum, String trxId) {
		
		List<DeliveryItem> deliveryItemList = deliveryManager.retrieveDeliveryItemListByBatchNumForLabelReprint(batchNum, dispenseDate, caseNum, trxId);
		delivery = new Delivery();
		
		if (deliveryItemList.size() > 0) {
			delivery = deliveryItemList.get(0).getDelivery();
			delivery.setDeliveryItemList(deliveryItemList);
			isCheckedBatch = ( delivery.getStatus() == DeliveryStatus.Checked || delivery.getStatus() == DeliveryStatus.Delivered );
		}
		
		constructMpDispLabelList(delivery);
	}

	public void reprintMpDispLabel(List<MpDispLabel> mpDispLabels) {
		
		List<LabelContainer> labelContainers = retrieveLabelContainers(mpDispLabels);
		
		List<RenderAndPrintJob> renderAndPrintJobList = new ArrayList<RenderAndPrintJob>();
		List<Long> transactionIdList = new ArrayList<Long>();

		reprintStandardMpDispLabel(filterMpDispLabelList(mpDispLabels, false), transactionIdList, renderAndPrintJobList);
		reprintPivasLabel(labelContainers, transactionIdList, renderAndPrintJobList);
		
		auditLogger.log("#0291", df.format(delivery.getBatchDate()) + delivery.getBatchNum(), transactionIdList, delivery.getWardCode());
		
		labelManager.printMpDispLabel(renderAndPrintJobList);
	}
	
	private void reprintStandardMpDispLabel(List<MpDispLabel> mpDispLabels, List<Long> transactionIdList, List<RenderAndPrintJob> renderAndPrintJobList) {
		
		if (mpDispLabels.size() <= 0) {
			return;
		}
		
		PrinterSelect printerSelect = printerSelector.retrievePrinterSelect(PrintDocType.OtherLabel);
		
		Map<String, Object> parameters = dispLabelBuilder.getMpDispLabelParam(mpDispLabels.get(0));
		parameters.put("_PNG_ZOOM_RATIO", Float.valueOf(1.3f));
		
		for (MpDispLabel mpDispLabel: mpDispLabels) {

			mpDispLabel.setMaskedUserCode(dispLabelBuilder.getMaskedUserCode(mpDispLabel.getUserCode()));
			PrintOption printOption = new PrintOption();
			String labelType = MP_DISP_LABEL;
			
			if( mpDispLabel.getShowInstructionFlag() && Boolean.TRUE.equals(mpDispLabel.isCanPrintInstructionFlag()) ){
				
				printOption.setPrintLang( mpDispLabel.getPrintLang() );
				if( LABEL_MPDISPLABEL_LARGEFONT_ENABLED.get(Boolean.FALSE) ){
					printOption.setPrintType( PrintType.LargeFont );
					printerSelect.setReverse(true);
				}else{
					printOption.setPrintType( PrintType.Normal );
					printerSelect.setReverse(false);
				}
			}else{
				
				labelType = MP_DISP_LABEL_NO_INST;
				printOption.setPrintLang( PrintLang.Eng );
				printOption.setPrintType( PrintType.Normal );
				printerSelect.setReverse(false);
			}
			
			printOption.setPrintStatus(PrintStatus.Reprint);
			
			renderAndPrintJobList.add(new RenderAndPrintJob(labelType,
					printOption,
					printerSelect,
					parameters,
					Arrays.asList(mpDispLabel)));
			
			
			if (StringUtils.isNotBlank(mpDispLabel.getPatOverrideReason()) || StringUtils.isNotBlank(mpDispLabel.getDdiOverrideReason())) {
				renderAndPrintJobList.add(new RenderAndPrintJob(MDS_OVERRIDE_REASON_LABEL,
						new PrintOption(printOption.getPrintType(), PrintLang.Eng, printOption.getPrintStatus()),
						printerSelect,
						parameters,
						Arrays.asList(mpDispLabel)));
			}
					
			transactionIdList.add(mpDispLabel.getMpDispLabelQrCode().getE());
		}
	}
	
	private void reprintPivasLabel(List<LabelContainer> labelContainers, List<Long> transactionIdList, List<RenderAndPrintJob> renderAndPrintJobList) {
		
		if (labelContainers.size() <= 0) {
			return;
		}
		
		PrintOption worksheetPrintOption = new PrintOption(LABEL_PIVASWORKSHEET_LARGEFONT_ENABLED.get(Boolean.FALSE) ? PrintType.LargeFont : PrintType.Normal, PrintLang.Eng, PrintStatus.Reprint);
		PrintOption printOption = new PrintOption(PrintType.Normal, PrintLang.Eng, PrintStatus.Reprint);
		
		try {
				labelManager.buildPivasLabelPrintJobList(labelContainers, renderAndPrintJobList, worksheetPrintOption, printOption, transactionIdList, true);
		} catch (DueOrderPrintException e) {
			throw new RuntimeException(e);
		}
	}
	
	private List<MpDispLabel> filterMpDispLabelList(List<MpDispLabel> mpDispLabelList, boolean pivasFlag) {
		
		List<MpDispLabel> resultList = new ArrayList<MpDispLabel>();
		
		for (MpDispLabel mpDispLabel: mpDispLabelList) {
			if (mpDispLabel.isPivasFlag() == pivasFlag) {
				resultList.add(mpDispLabel);
			}
		}
		
		return resultList;
	}
	
	private List<LabelContainer> retrieveLabelContainers(List<MpDispLabel> mpDispLabels) {
		
		JaxbWrapper<Object> labelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);		
		
		List<Long> deliveryItemIdList = new ArrayList<Long>();
		
		for (MpDispLabel mpDispLabel: mpDispLabels) {
			if (mpDispLabel.isPivasFlag()) {
				deliveryItemIdList.add(mpDispLabel.getDeliveryItemId());
			}
		}
		
		List<DeliveryItem> deliveryItemList = deliveryManager.retrieveDeliveryItemListById(deliveryItemIdList);
		
    	Map<Long, DeliveryItem> deliveryItemMap = new HashMap<Long, DeliveryItem>();
    	for (DeliveryItem deliveryItem: deliveryItemList) {
    		deliveryItemMap.put(deliveryItem.getId(), deliveryItem);
    	}
    	
    	List<LabelContainer> labelContainerList = new ArrayList<LabelContainer>();
    	
    	for (Long id: deliveryItemIdList) {
    		DeliveryItem deliveryItem = deliveryItemMap.get(id);
    		if (deliveryItem == null || deliveryItem.getLabelXml() == null) {
    			continue;
    		}
    		Object label = labelJaxbWrapper.unmarshall(deliveryItem.getLabelXml());
    		if (label instanceof LabelContainer) {
    			labelContainerList.add((LabelContainer) label);
    		}
    	}
    	
		return labelContainerList;
	}
	
	public void reprintDeliveryLabel() {
		printAgent.renderAndPrint(labelManager.printDeliveryLabel(delivery, true));
		auditLogger.log("#0292",  df.format(delivery.getBatchDate())+ delivery.getBatchNum(), delivery.getWardCode());
	}
	
	public void reprintDividerLabel() {
		labelManager.printDividerLabel(delivery, true);
	}
	
	public void resendAtdpsMessage(List<MpDispLabel> mpDispLabelList){
		Map<String, List<ItemLocation>> itemLocationMap = getItemLocationMap(mpDispLabelList);
		
		for (MpDispLabel mpDispLabel : mpDispLabelList) {
			if ( "UTF-8".equals(MACHINE_ATDPS_MESSAGE_ENCODING.get()) ) {
				resendAtdpsMessage(mpDispLabel, mpDispLabel.getAtdpsXmlList(), itemLocationMap);
			} else {
				resendAtdpsMessageContent(mpDispLabel, mpDispLabel.getAtdpsInfoEngList(), itemLocationMap);
			}
		}
	}
	
	private void resendAtdpsMessage(MpDispLabel mpDispLabel, List<AtdpsMessage> atdpsMessageList, Map<String, List<ItemLocation>> itemLocationMap) {
		if ( atdpsMessageList == null || atdpsMessageList.isEmpty() ) {
			return;
		}

		Machine machine = getMachine(itemLocationMap, mpDispLabel.getItemCode(), atdpsMessageList.get(0).getPickStationNum());
		for ( AtdpsMessage atdpsMessage : atdpsMessageList ) {
			if ( machine != null ) {
				atdpsMessage.setHostname(machine.getHostName());
				atdpsMessage.setPickStationNum(machine.getPickStation().getPickStationNum());
			}
			atdpsMessage.setPortNum(MACHINE_ATDPS_PORT.get());
			atdpsMessage.setReprintFlag(true);
			atdpsMessage.setPrintWardReturnFlag(LABEL_MPDISPLABEL_WARDRTNBARCODE_VISIBLE.get()); 
			atdpsMessage.setPrintInstructFlag(false);
			if ( atdpsMessage.getAdjQty() == null ) {
				atdpsMessage.setAdjQty("0");
			}
			if ( StringUtils.isNotBlank(mpDispLabel.getBatchNum()) ) {
				atdpsMessage.setBatchCode(mpDispLabel.getBatchNum());
			}
			atdpsMessage.setInstruction(StringUtils.EMPTY);
    		try{
    			logger.info("sendAtdpsSocketMessage itemCode = #0, trxId = #1, pickStationId = #2, hostName = #3, portNum = #4, wardCode = #5, batchCode = #6, reprintFlag = #7, MACHINE_ATDPS_MESSAGE_ENCODING = #8",
		    					atdpsMessage.getItemCode(), 
		    					mpDispLabel.getDeliveryItemId(),
		    					atdpsMessage.getPickStationNum(),
		    					atdpsMessage.getHostname(), 
								MACHINE_ATDPS_PORT.get(), 
								atdpsMessage.getWardCode(), 
								atdpsMessage.getBatchCode(), 
								atdpsMessage.isReprintFlag(), 
								MACHINE_ATDPS_MESSAGE_ENCODING.get());
    			auditLogger.log("#0758", atdpsMessage.getWardCode(), atdpsMessage.getBatchCode(), mpDispLabel.getDeliveryItemId());
    			atdpsMessage.setPickStationNum(null);
    			machineServiceProxy.sendAtdpsSocketMessage(atdpsMessage);
	    	} catch (Exception e) {
				logger.error("Fail to send message to ATDPS #0", e);
			}
    	}
	}
	
	private void resendAtdpsMessageContent(MpDispLabel mpDispLabel, List<AtdpsMessageContent> atdpsMessageContentList, Map<String, List<ItemLocation>> itemLocationMap){
		if ( atdpsMessageContentList == null || atdpsMessageContentList.isEmpty() ) {
			return;
		}

		Machine machine = getMachine(itemLocationMap, mpDispLabel.getItemCode(), atdpsMessageContentList.get(0).getPickStationNum());
		for ( AtdpsMessageContent atdpsMessageContent : atdpsMessageContentList ) {
			if ( machine != null ) {
				atdpsMessageContent.setHostname(machine.getHostName());
				atdpsMessageContent.setPickStationNum(machine.getPickStation().getPickStationNum());
			}
			atdpsMessageContent.setPortNum(MACHINE_ATDPS_PORT.get());
			atdpsMessageContent.setPrintType(AtdpsPrintType.Reprint.getDataValue());
			atdpsMessageContent.setReprintFlag("Y");
			atdpsMessageContent.setPrintWardBarcode(LABEL_MPDISPLABEL_WARDRTNBARCODE_VISIBLE.get()?"Y":"N"); 
			atdpsMessageContent.setPrintInstruct("N");
			atdpsMessageContent.setShowInstructionFlag(false);
			if ( atdpsMessageContent.getAdjQty() == null ) {
				atdpsMessageContent.setAdjQty("0");
			}
			if ( StringUtils.isNotBlank(mpDispLabel.getBatchNum()) ) {
				atdpsMessageContent.setBatchNum(mpDispLabel.getBatchNum().substring(2));
				atdpsMessageContent.setBatchCode(mpDispLabel.getBatchNum());
			}
			atdpsMessageContent.setInstruction(StringUtils.EMPTY);
    		try{
    			logger.info("sendAtdpsSocketMessage itemCode = #0, trxId = #1, pickStationId = #2, hostName = #3, portNum = #4, wardCode = #5, batchCode = #6, reprintFlag = #7, MACHINE_ATDPS_MESSAGE_ENCODING = #8",
							atdpsMessageContent.getItemCode(), 
							atdpsMessageContent.getTrxId(),
							atdpsMessageContent.getPickStationNum(),
							atdpsMessageContent.getHostname(), 
							MACHINE_ATDPS_PORT.get(), 
							atdpsMessageContent.getWard(), 
							atdpsMessageContent.getBatchCode(), 
							atdpsMessageContent.getReprintFlag(), 
							MACHINE_ATDPS_MESSAGE_ENCODING.get());
    			auditLogger.log("#0758", atdpsMessageContent.getWard(), atdpsMessageContent.getBatchCode(), atdpsMessageContent.getTrxId());
    			machineServiceProxy.sendAtdpsSocketMessage(atdpsMessageContent);
	    	} catch (Exception e) {
				logger.error("Fail to send message to ATDPS #0", e);
			}
    	}
	}
	
	private Machine getMachine(Map<String, List<ItemLocation>> itemLocationMap, String itemCode, Integer prePickStationsNum){
		Machine targetMachine = null;
		List<ItemLocation> itemLocationList = itemLocationMap.get(itemCode);
		if ( itemLocationList != null && !itemLocationList.isEmpty() && prePickStationsNum != null ) {
			for ( ItemLocation itemLocation : itemLocationList ) {
				Machine machine = itemLocation.getMachine();
				if ( machine == null ) {
					continue;
				}
				if ( prePickStationsNum.compareTo(machine.getPickStation().getPickStationNum()) == 0 ){
					targetMachine = machine;
					break;
				} 
			}
		}
		if ( targetMachine == null ) {
			if ( activeMachineList == null ) {
				buildActiveMachineList();
			}
			if ( !activeMachineList.isEmpty() ) {
				targetMachine = activeMachineList.get(0);
			}
		}
		return targetMachine;
	}
	
	private void buildActiveMachineList(){
		activeMachineList = new ArrayList<Machine>();
		List<OperationWorkstation> atdpsWorkstationList = refTableManager.getActiveAtdpsWorkstation(activeProfile);
		for ( OperationWorkstation ow : atdpsWorkstationList ) {
			if ( ow.getMachine() != null ) {
				activeMachineList.add(ow.getMachine());
			}
		}
	}
	
	private Map<String, List<ItemLocation>> getItemLocationMap(List<MpDispLabel> mpDispLabelList){
		Set<String> itemCodeSet = new HashSet<String>();
		for (MpDispLabel mpDispLabel : mpDispLabelList) {
			itemCodeSet.add(mpDispLabel.getItemCode());
		}
		return itemLocationListManager.retrieveItemLocationListByItemCodeList(new ArrayList<String>(itemCodeSet), 
																				MachineType.ATDPS, 
																				atdpsPickStationsNumList);
	}
	
	private void constructMpDispLabelList(Delivery delivery) {
		JaxbWrapper<RxItem> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
		JaxbWrapper<MpDispLabelQrCode> dispLabelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);

		mpDispLabelList = new ArrayList<MpDispLabel>();
		
		List<DeliveryItem> deliveryItemList = delivery.getDeliveryItemList();
		HashSet<Long> prevMoItemIdSet = new HashSet<Long>();
		Map<Long, DeliveryItem> deliveryItemMap = new HashMap<Long, DeliveryItem>();
		Map<Long, ReplenishmentItem> deliveryReplenishItemForZeroQtyMap = new HashMap<Long, ReplenishmentItem>();
		Map<Long, MedProfilePoItem> poItemMap = new HashMap<Long, MedProfilePoItem>();

		LabelReprintMedProfileMoItem labelReprintMedProfileMoItem = new LabelReprintMedProfileMoItem();
		Map<Long, LabelReprintMedProfileMoItem> labelReprintMoItemMap = new HashMap<Long, LabelReprintMedProfileMoItem>();
		
    	for (DeliveryItem deliveryItem : deliveryItemList){
			
    		MedProfilePoItem medProfilePoItem = deliveryItem.resolveMedProfilePoItem();
    		if (medProfilePoItem != null ) {
        		deliveryItemMap.put(medProfilePoItem.getId(), deliveryItem);
    		}

    		if( deliveryItem.getReplenishmentItem() != null && deliveryItem.getReplenishmentItem().getReplenishment() != null){
    			List<ReplenishmentItem> rrList = deliveryItem.getReplenishmentItem().getReplenishment().getReplenishmentItemList(); 
     			for(ReplenishmentItem rr : rrList){
    				if(rr.getIssueQty().setScale(0).equals(BigDecimal.ZERO) ){
    					deliveryReplenishItemForZeroQtyMap.put(rr.getMedProfilePoItem().getId(), rr);
    				}
    			}
    		}
    	}
    	
    	for (DeliveryItem deliveryItem : deliveryItemList) {
    		if ((deliveryItem.resolveMedProfilePoItem() != null)) {
    			
    			MedProfileMoItem medProfileMoItem = deliveryItem.getMedProfileMoItem();
    			
    			if (!prevMoItemIdSet.contains(medProfileMoItem.getId())){
	    			prevMoItemIdSet.add(medProfileMoItem.getId());
	    			
	    			for (MedProfilePoItem medProfilePoItem : medProfileMoItem.getMedProfilePoItemList()){
	    				DeliveryItem deliveryItemMapDeliveryItem = deliveryItemMap.get(medProfilePoItem.getId());
				
    					if ( deliveryItemMapDeliveryItem == null ){               		
    						if(!deliveryReplenishItemForZeroQtyMap.containsKey(medProfilePoItem.getId())){ //no delivery item in MO (not due /complete)
    							poItemMap.put(medProfilePoItem.getId(), medProfilePoItem);
    						}	    						
    					}
	    			}
    			}
    		}
    	}
		Set<String> binNumSet = new HashSet<String>();
		for (DeliveryItem deliveryItem:deliveryItemList) {
			binNumSet.add(deliveryItem.getBinNum());
		}
		
		for (DeliveryItem deliveryItem : deliveryItemList) {
			MpDispLabel mpDispLabel = constructBaseMpDispLabel(deliveryItem);
			MedProfileMoItem medProfileMoItem = deliveryItem.getMedProfileMoItem();
			MedProfilePoItem medProfilePoItem = deliveryItem.resolveMedProfilePoItem();
			mpDispLabel.setDeliveryItemId(deliveryItem.getId());
			mpDispLabel.setItemNum(medProfileMoItem.getItemNum());
			
			if (deliveryItem.getReplenishmentItem() != null) {
				mpDispLabel.setItemType("REPLENISH");
			} else {
				if (deliveryItem.getRefillFlag()) {
					mpDispLabel.setItemType("REFILL");
				} else {
					mpDispLabel.setItemType("NEW");
				}
			}
			
			if ( ATDPS_BINNUM.equals(deliveryItem.getBinNum())) {
				mpDispLabel.setAllowAtdpsFlag(true);
			}
			
			if (mpDispLabel.getDrugOrderTlf() == null && !medProfileMoItem.isManualItem()) {
				//Format for IP only
				RxItem rxItem = rxJaxbWrapper.unmarshall(medProfileMoItem.getRxItemXml());
				rxItem.buildTlf();
				mpDispLabel.setDrugOrderTlf(rxItem.getFormattedDrugOrderTlf());
			}

			String mpDispLabelQrCodeXml = mpDispLabel.getQrCodeXml().replaceAll("DRUG>", "MpDispLabelQrCode>");
			MpDispLabelQrCode mpDispLabelQrCode = null;
			try {
				mpDispLabelQrCode = dispLabelJaxbWrapper.unmarshall(mpDispLabelQrCodeXml);
				mpDispLabel.setMpDispLabelQrCode(mpDispLabelQrCode);
			} catch(Exception ex) {
				mpDispLabel.setMpDispLabelQrCode(null);
			}
			
			MsWorkstoreDrug msWorkstoreDrug = dmDrugCacher.getMsWorkstoreDrug(workstore, deliveryItem.getItemCode());
			if (msWorkstoreDrug != null && !"Y".equalsIgnoreCase(msWorkstoreDrug.getSuspend())
					&& medProfileMoItem.getMedProfileItem().getStatus() == MedProfileItemStatus.Verified) {
				mpDispLabel.setAllowReprintFlag(true);
			}
		
			if (medProfileMoItem.getMedProfileItem().getMedProfile().getStatus() == MedProfileStatus.HomeLeave) {
				mpDispLabel.setHomeLeaveFlag(true);
			}
			
			if (!medProfileMoItem.getCanSetLabelInstruction()) {
				mpDispLabel.setCanPrintInstructionFlag(false);
			}
			
			//For label reprint
			mpDispLabel.setShowInstructionFlag(false);
			mpDispLabel.setPrintLang(PrintLang.Eng);

			int batchDay = new DateTime(delivery.getBatchDate()).getDayOfMonth();
			mpDispLabel.setBatchNum( (( batchDay < 10 )?"0":"") + batchDay + delivery.getBatchNum() );
			
			String exception = checkManager.getProblemItemException(deliveryItem);
			
			if ( StringUtils.isNotEmpty(exception) ) {
				mpDispLabel.setException(exception);
			}
			
			if ( DeliveryItemStatus.Unlink.equals( deliveryItem.getStatus())){
				mpDispLabel.setException(DeliveryItemStatus.Unlink.getDisplayValue());
			}
			
			mpDispLabel.setOrderEditRank(medProfilePoItem == null ? 1L : medProfilePoItem.getId());
			mpDispLabel.setOrderLineRank(medProfileMoItem.getId());
			
			List<MpDispLabel> allMpDispLabelList = new ArrayList<MpDispLabel>();	
			
			
			if( labelReprintMoItemMap.containsKey(mpDispLabel.getOrderLineRank()) ){
				labelReprintMedProfileMoItem = labelReprintMoItemMap.get(mpDispLabel.getOrderLineRank());
				allMpDispLabelList = labelReprintMedProfileMoItem.getMpDispLabelList();
			}else{
				labelReprintMedProfileMoItem = new LabelReprintMedProfileMoItem();
				labelReprintMedProfileMoItem.setBedNum(mpDispLabel.getBedNum());
				labelReprintMedProfileMoItem.setCaseNum(mpDispLabel.getCaseNum());
				labelReprintMedProfileMoItem.setMedProfileMoIdForReprint(mpDispLabel.getOrderLineRank());
				labelReprintMedProfileMoItem.setMedProfilePoIdForReprint(mpDispLabel.getOrderEditRank());
			}
			
			allMpDispLabelList.add(mpDispLabel); // Add Original Delivery Item Label
			labelReprintMedProfileMoItem.setMpDispLabelList(allMpDispLabelList);
			
			labelReprintMoItemMap.put(mpDispLabel.getOrderLineRank(), labelReprintMedProfileMoItem);
		}
		
		for (MedProfilePoItem tempPoId:poItemMap.values()){
			MpDispLabel mpDispLabel = new MpDispLabel();
			mpDispLabel.setItemType("OTHERS");
			mpDispLabel.setOrderEditRank(tempPoId.getId());	
			mpDispLabel.setOrderLineRank(tempPoId.getMedProfileMoItem().getId());	
		}
		
		List<LabelReprintMedProfileMoItem> itemList = new ArrayList <LabelReprintMedProfileMoItem>();
		itemList.addAll(labelReprintMoItemMap.values());
		Collections.sort(itemList, mpDispLabelComparator);

		mpDispLabelList = new ArrayList<MpDispLabel>();

		for( LabelReprintMedProfileMoItem newMoItem:itemList){
			List<MpDispLabel> tempLabelList = newMoItem.getMpDispLabelList();
			
			Collections.sort(tempLabelList, tempMpDispLabelComparator);
			
			for (MpDispLabel mpLabel:tempLabelList) {
				if (mpLabel.getItemType() != "OTHERS") {
					mpDispLabelList.add(mpLabel);
				}
			}
		}
	}
	
	public MpDispLabel constructBaseMpDispLabel(DeliveryItem deliveryItem) {
		
		JaxbWrapper<Object> labelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
		Object label = deliveryItem.getLabelXml() == null ? null : labelJaxbWrapper.unmarshall(deliveryItem.getLabelXml());
		if (label instanceof MpDispLabel) {
			return (MpDispLabel) label;
		}
		
		LabelContainer labelContainer = (LabelContainer) label; 
		MedProfileMoItem medProfileMoItem = deliveryItem.getMedProfileMoItem(); 
		
		MpDispLabel mpDispLabel = new MpDispLabel();

		if (labelContainer != null) {
			PivasProductLabel productLabel = labelContainer.getPivasProductLabel();
			mpDispLabel.setBedNum(productLabel.getBedNum());
			mpDispLabel.setCaseNum(productLabel.getCaseNum());
			mpDispLabel.setPatName(productLabel.getPatName());
			mpDispLabel.setPatNameChi(productLabel.getPatNameChi());
			mpDispLabel.setWard(productLabel.getWard());
			mpDispLabel.setDrugOrderTlf(productLabel.getDeliveryItemDescTlf());
		} else {
			MedProfile medProfile = deliveryItem.getMedProfile();
			MedCase medCase = medProfile.getMedCase();
			Patient patient = medProfile.getPatient();
			mpDispLabel.setBedNum(medCase.getPasBedNum());
			mpDispLabel.setCaseNum(medCase.getCaseNum());
			mpDispLabel.setPatName(patient.getName());
			mpDispLabel.setPatNameChi(patient.getNameChi());
			mpDispLabel.setWard(deliveryItem.getDelivery().getWardCode());
		}
		mpDispLabel.setItemNum(medProfileMoItem.getItemNum());
		mpDispLabel.setItemCode(deliveryItem.getItemCode());
		mpDispLabel.setIssueQty(deliveryItem.getIssueQty().intValue());
		mpDispLabel.setBaseUnit("DOSE");
		mpDispLabel.setCanPrintInstructionFlag(false);
		mpDispLabel.setDeliveryItemId(deliveryItem.getId());
		mpDispLabel.setQrCodeXml("");
		mpDispLabel.setPivasFlag(true);
		
		return mpDispLabel;
	}
	
	@Remove
	public void destroy() {
		if( isCheckedBatch ){
			isCheckedBatch = false;
		}
	}
	
	private static class TransactionListBatchNumComparator implements Comparator<Delivery>, Serializable {
		private static final long serialVersionUID = 1L;
		@Override
          public int compare(Delivery o1, Delivery o2) {
			
				int compareInt = 0;
				
				compareInt = new CompareToBuilder()
	          	  .append(o2.getTrxIdDeliveryItem().isEmpty(), o1.getTrxIdDeliveryItem().isEmpty() )
	          	  .toComparison();
				
				if( compareInt == 0){
					  compareInt = new CompareToBuilder()
                	  .append( o2.getBatchNum(), o1.getBatchNum() )
                      .toComparison();
				}
              
                return compareInt;
          }
    }
	
	private static class MpDispLabelComparator implements Comparator<LabelReprintMedProfileMoItem>, Serializable {
		private static final long serialVersionUID = 1L;
		@Override
		public int compare(LabelReprintMedProfileMoItem o1, LabelReprintMedProfileMoItem o2) {
			int compareResult = 0;
			
			String bedNum1 = StringUtils.trimToEmpty(o1.getBedNum());
			String bedNum2 = StringUtils.trimToEmpty(o2.getBedNum());
			compareResult = bedNum1.compareTo(bedNum2);
			
			if (compareResult==0) {
				String caseNum1 = StringUtils.trimToEmpty(o1.getCaseNum());
				String caseNum2 = StringUtils.trimToEmpty(o2.getCaseNum());
				compareResult = caseNum1.compareTo(caseNum2);
			}
					
			if (compareResult==0) {
				compareResult = compareItemType(o1.getMpDispLabelList()).compareTo(
							compareItemType(o2.getMpDispLabelList()));
			}
			
			if (compareResult==0) {
				Long moId1 = o1.getMedProfileMoIdForReprint();
				Long moId2 = o2.getMedProfileMoIdForReprint();
				compareResult = moId1.compareTo(moId2);
			}
			
			return compareResult;
			
		}
		
		private Integer compareItemType(List<MpDispLabel> mpDispLabelList){
			int count = 0;
			for(MpDispLabel mpDispLabel : mpDispLabelList){
				if(mpDispLabel.getItemType()=="NEW" && count == 0){
					count = 1;
				}else if ((mpDispLabel.getItemType()=="OTHERS") && (count == 0 || count == 1)){
					count = 2;
				}else if(mpDispLabel.getItemType()=="REFILL"){
					count = 3;
				}else if(mpDispLabel.getItemType()=="REPLENISH"){
					count = 4;
				}
			}
			return Integer.valueOf(count);
		}
	}
	
	private static class TempMpDispLabelComparator implements Comparator<MpDispLabel>, Serializable {
		private static final long serialVersionUID = 1L;
		@Override
		public int compare(MpDispLabel o1, MpDispLabel o2) {
			int compareResult = 0;
			
				Long orderEditRank1 = o1.getOrderEditRank();
				Long orderEditRank2 = o2.getOrderEditRank();
				compareResult = orderEditRank1.compareTo(orderEditRank2);
			
			return compareResult;
		}
	}

	
}