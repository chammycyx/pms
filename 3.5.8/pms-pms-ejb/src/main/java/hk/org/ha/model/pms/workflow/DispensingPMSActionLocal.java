package hk.org.ha.model.pms.workflow;

import javax.ejb.Local;

@Local
public interface DispensingPMSActionLocal {
	
	String checkClearance();
	
	String forceProceed();
	
	void retrieveMDSInfo();
	
	String mdsModified();
	
	void createPharmarcyOrder();
	
	String hasRefill();
	
	void createDispensingOrder();
	
	void createInvoices();
	
	String hasSFI();
	
	void printLabels();
}
