package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.vo.report.UncollectPrescRpt;

import java.util.List;

import javax.ejb.Local;

@Local
public interface UncollectPrescRptServiceLocal {
	
	Integer getMaxDayRange();
	
	List<UncollectPrescRpt> retrieveUncollectPrescRptList(Integer dayRange);
	
	void printUncollectPrescRpt(List<UncollectPrescRpt> uncollectPrescRptList);
	
	void destroy();
}
