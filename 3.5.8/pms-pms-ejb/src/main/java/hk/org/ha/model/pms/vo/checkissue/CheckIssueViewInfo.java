package hk.org.ha.model.pms.vo.checkissue;

import hk.org.ha.model.pms.udt.disp.TicketMatchDocType;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class CheckIssueViewInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Date ticketDate;
	
	private String ticketNum;
	
	private boolean autoCheckIssue;
	
	private Integer issueWindowNum; 
	
	private boolean autoRefresh; 
	
	private boolean refreshIssuedList;
	
	private Date checkIssueListDate; 
	
	private Date otherCheckListDate; 
	
	private Date issuedListDate;
	
	private boolean fastQueueAllowCheckFlag;
	
	private Long dispOrderVersion;
	
	private Long dispOrderId;
	
	private boolean firstRetrieveFlag;
	
	private TicketMatchDocType firstTicketMatchDocType;
	
	private String firstTicketBarcode;
	
	private TicketMatchDocType secondTicketMatchDocType;
	
	private String ticketMatchForceProceedReason;
		
	public Date getTicketDate() {
		return ticketDate;
	}

	public void setTicketDate(Date ticketDate) {
		this.ticketDate = ticketDate;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public boolean isAutoCheckIssue() {
		return autoCheckIssue;
	}

	public void setAutoCheckIssue(boolean autoCheckIssue) {
		this.autoCheckIssue = autoCheckIssue;
	}

	public Integer getIssueWindowNum() {
		return issueWindowNum;
	}

	public void setIssueWindowNum(Integer issueWindowNum) {
		this.issueWindowNum = issueWindowNum;
	}

	public boolean isAutoRefresh() {
		return autoRefresh;
	}

	public void setAutoRefresh(boolean autoRefresh) {
		this.autoRefresh = autoRefresh;
	}

	public boolean isRefreshIssuedList() {
		return refreshIssuedList;
	}

	public void setRefreshIssuedList(boolean refreshIssuedList) {
		this.refreshIssuedList = refreshIssuedList;
	}

	public Date getCheckIssueListDate() {
		return checkIssueListDate;
	}

	public void setCheckIssueListDate(Date checkIssueListDate) {
		this.checkIssueListDate = checkIssueListDate;
	}

	public Date getOtherCheckListDate() {
		return otherCheckListDate;
	}

	public void setOtherCheckListDate(Date otherCheckListDate) {
		this.otherCheckListDate = otherCheckListDate;
	}

	public Date getIssuedListDate() {
		return issuedListDate;
	}

	public void setIssuedListDate(Date issuedListDate) {
		this.issuedListDate = issuedListDate;
	}
	
	public void setFastQueueAllowCheckFlag(boolean fastQueueAllowCheckFlag) {
		this.fastQueueAllowCheckFlag = fastQueueAllowCheckFlag;
	}

	public boolean getFastQueueAllowCheckFlag() {
		return fastQueueAllowCheckFlag;
	}

	public void setDispOrderVersion(Long dispOrderVersion) {
		this.dispOrderVersion = dispOrderVersion;
	}

	public Long getDispOrderVersion() {
		return dispOrderVersion;
	}

	public void setDispOrderId(Long dispOrderId) {
		this.dispOrderId = dispOrderId;
	}

	public Long getDispOrderId() {
		return dispOrderId;
	}

	public boolean isFirstRetrieveFlag() {
		return firstRetrieveFlag;
	}

	public void setFirstRetrieveFlag(boolean firstRetrieveFlag) {
		this.firstRetrieveFlag = firstRetrieveFlag;
	}

	public TicketMatchDocType getFirstTicketMatchDocType() {
		return firstTicketMatchDocType;
	}

	public void setFirstTicketMatchDocType(TicketMatchDocType firstTicketMatchDocType) {
		this.firstTicketMatchDocType = firstTicketMatchDocType;
	}

	public String getFirstTicketBarcode() {
		return firstTicketBarcode;
	}

	public void setFirstTicketBarcode(String firstTicketBarcode) {
		this.firstTicketBarcode = firstTicketBarcode;
	}

	public TicketMatchDocType getSecondTicketMatchDocType() {
		return secondTicketMatchDocType;
	}

	public void setSecondTicketMatchDocType(TicketMatchDocType secondTicketMatchDocType) {
		this.secondTicketMatchDocType = secondTicketMatchDocType;
	}

	public String getTicketMatchForceProceedReason() {
		return ticketMatchForceProceedReason;
	}

	public void setTicketMatchForceProceedReason(
			String ticketMatchForceProceedReason) {
		this.ticketMatchForceProceedReason = ticketMatchForceProceedReason;
	}
	
	
}