package hk.org.ha.model.pms.udt.alert;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DrugType implements StringValuedEnum {
	
	Previous("OP", "Previous ADR"),
	Other("O", "Other ADR"),
	Drug("D", "Drug ADR");

    private final String dataValue;
    private final String displayValue;
    
	DrugType(final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}

	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}

	public static DrugType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DrugType.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<DrugType> {
		
		private static final long serialVersionUID = 1L;

		public Class<DrugType> getEnumClass() {
    		return DrugType.class;
    	}
    }
	
}
