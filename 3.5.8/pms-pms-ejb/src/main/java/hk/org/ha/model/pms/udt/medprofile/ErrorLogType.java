package hk.org.ha.model.pms.udt.medprofile;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum ErrorLogType implements StringValuedEnum {

	Suspend("S", "Drug suspension"),
	MissingSpecialty("M", "Specialty mapping missing"),
	MdsAlert("A", "MDS alert"),
	WardTransfer("W", "Ward transfer"),
	Frozen("F", "Frozen item exists");
	
    private final String dataValue;
    private final String displayValue;
    
	private ErrorLogType(String dataValue, String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}

	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}
	
    public static ErrorLogType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(ErrorLogType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<ErrorLogType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<ErrorLogType> getEnumClass() {
    		return ErrorLogType.class;
    	}
    }
}
