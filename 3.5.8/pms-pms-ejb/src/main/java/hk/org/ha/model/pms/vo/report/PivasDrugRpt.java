package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class PivasDrugRpt implements Serializable {

	private static final long serialVersionUID = 1L;

	private String workstoreGroupCode;

	private String pivasDrugStatus;
	
	private String pivasDrugManufStatus;
	
	private Integer drugKey;
	
	private String itemCode;
	
	private String dmDrugFullDrugDesc;
	
	private String pivasDosageUnit;
	
	private String diluentDesc;
	
	private String diluentActionType;
	
	private String solventActionType;
	
	private String manufacturerCode;
	
	private Boolean requireSolventFlag;
	
	private String warnCode1;
	
	private String warnCode2;

	private String dispHospCode;
	
	private String batchNum;
	
	private Date expireDate;

	private String satelliteBatchNum;
	
	private Date satelliteExpireDate;
	
	private String solventDesc;
	
	private String solventItemCode;
	
	private BigDecimal dosageQty;
	
	private BigDecimal volume;
	
	private BigDecimal afterVolume;
	
	private BigDecimal afterConcn;
	
	private String siteCodes;
	
	private Long pivasDrugId;
	
	private String pivasDrugUpdateUser;
	
	private Date pivasDrugUpdateDate;
	
	private Long pivasDrugManufId;
	
	private String pivasDrugManufUpdateUser;
	
	private Date pivasDrugManufUpdateDate;
	
	private Long pivasDrugManufMethodId;
	
	private String pivasDrugManufMethodStatus;
	
	private String pivasDrugManufMethodUpdateUser;
	
	private Date pivasDrugManufMethodUpdateDate;

	public String getWorkstoreGroupCode() {
		return workstoreGroupCode;
	}

	public void setWorkstoreGroupCode(String workstoreGroupCode) {
		this.workstoreGroupCode = workstoreGroupCode;
	}

	public String getPivasDrugStatus() {
		return pivasDrugStatus;
	}

	public void setPivasDrugStatus(String pivasDrugStatus) {
		this.pivasDrugStatus = pivasDrugStatus;
	}

	public String getPivasDrugManufStatus() {
		return pivasDrugManufStatus;
	}

	public void setPivasDrugManufStatus(String pivasDrugManufStatus) {
		this.pivasDrugManufStatus = pivasDrugManufStatus;
	}

	public Integer getDrugKey() {
		return drugKey;
	}

	public void setDrugKey(Integer drugKey) {
		this.drugKey = drugKey;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getDmDrugFullDrugDesc() {
		return dmDrugFullDrugDesc;
	}

	public void setDmDrugFullDrugDesc(String dmDrugFullDrugDesc) {
		this.dmDrugFullDrugDesc = dmDrugFullDrugDesc;
	}

	public String getPivasDosageUnit() {
		return pivasDosageUnit;
	}

	public void setPivasDosageUnit(String pivasDosageUnit) {
		this.pivasDosageUnit = pivasDosageUnit;
	}

	public String getDiluentDesc() {
		return diluentDesc;
	}

	public void setDiluentDesc(String diluentDesc) {
		this.diluentDesc = diluentDesc;
	}

	public String getDiluentActionType() {
		return diluentActionType;
	}

	public void setDiluentActionType(String diluentActionType) {
		this.diluentActionType = diluentActionType;
	}

	public String getSolventActionType() {
		return solventActionType;
	}

	public void setSolventActionType(String solventActionType) {
		this.solventActionType = solventActionType;
	}

	public String getManufacturerCode() {
		return manufacturerCode;
	}

	public void setManufacturerCode(String manufacturerCode) {
		this.manufacturerCode = manufacturerCode;
	}

	public Boolean getRequireSolventFlag() {
		return requireSolventFlag;
	}

	public void setRequireSolventFlag(Boolean requireSolventFlag) {
		this.requireSolventFlag = requireSolventFlag;
	}

	public String getWarnCode1() {
		return warnCode1;
	}

	public void setWarnCode1(String warnCode1) {
		this.warnCode1 = warnCode1;
	}

	public String getWarnCode2() {
		return warnCode2;
	}

	public void setWarnCode2(String warnCode2) {
		this.warnCode2 = warnCode2;
	}

	public String getDispHospCode() {
		return dispHospCode;
	}

	public void setDispHospCode(String dispHospCode) {
		this.dispHospCode = dispHospCode;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public String getSatelliteBatchNum() {
		return satelliteBatchNum;
	}

	public void setSatelliteBatchNum(String satelliteBatchNum) {
		this.satelliteBatchNum = satelliteBatchNum;
	}

	public Date getSatelliteExpireDate() {
		return satelliteExpireDate;
	}

	public void setSatelliteExpireDate(Date satelliteExpireDate) {
		this.satelliteExpireDate = satelliteExpireDate;
	}

	public String getSolventDesc() {
		return solventDesc;
	}

	public void setSolventDesc(String solventDesc) {
		this.solventDesc = solventDesc;
	}

	public String getSolventItemCode() {
		return solventItemCode;
	}

	public void setSolventItemCode(String solventItemCode) {
		this.solventItemCode = solventItemCode;
	}

	public BigDecimal getDosageQty() {
		return dosageQty;
	}

	public void setDosageQty(BigDecimal dosageQty) {
		this.dosageQty = dosageQty;
	}

	public BigDecimal getVolume() {
		return volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	public BigDecimal getAfterVolume() {
		return afterVolume;
	}

	public void setAfterVolume(BigDecimal afterVolume) {
		this.afterVolume = afterVolume;
	}

	public BigDecimal getAfterConcn() {
		return afterConcn;
	}

	public void setAfterConcn(BigDecimal afterConcn) {
		this.afterConcn = afterConcn;
	}

	public String getSiteCodes() {
		return siteCodes;
	}

	public void setSiteCodes(String siteCodes) {
		this.siteCodes = siteCodes;
	}

	public Long getPivasDrugId() {
		return pivasDrugId;
	}

	public void setPivasDrugId(Long pivasDrugId) {
		this.pivasDrugId = pivasDrugId;
	}

	public String getPivasDrugUpdateUser() {
		return pivasDrugUpdateUser;
	}

	public void setPivasDrugUpdateUser(String pivasDrugUpdateUser) {
		this.pivasDrugUpdateUser = pivasDrugUpdateUser;
	}

	public Date getPivasDrugUpdateDate() {
		return pivasDrugUpdateDate;
	}

	public void setPivasDrugUpdateDate(Date pivasDrugUpdateDate) {
		this.pivasDrugUpdateDate = pivasDrugUpdateDate;
	}

	public Long getPivasDrugManufId() {
		return pivasDrugManufId;
	}

	public void setPivasDrugManufId(Long pivasDrugManufId) {
		this.pivasDrugManufId = pivasDrugManufId;
	}

	public String getPivasDrugManufUpdateUser() {
		return pivasDrugManufUpdateUser;
	}

	public void setPivasDrugManufUpdateUser(String pivasDrugManufUpdateUser) {
		this.pivasDrugManufUpdateUser = pivasDrugManufUpdateUser;
	}

	public Date getPivasDrugManufUpdateDate() {
		return pivasDrugManufUpdateDate;
	}

	public void setPivasDrugManufUpdateDate(Date pivasDrugManufUpdateDate) {
		this.pivasDrugManufUpdateDate = pivasDrugManufUpdateDate;
	}

	public Long getPivasDrugManufMethodId() {
		return pivasDrugManufMethodId;
	}

	public void setPivasDrugManufMethodId(Long pivasDrugManufMethodId) {
		this.pivasDrugManufMethodId = pivasDrugManufMethodId;
	}

	public String getPivasDrugManufMethodStatus() {
		return pivasDrugManufMethodStatus;
	}

	public void setPivasDrugManufMethodStatus(String pivasDrugManufMethodStatus) {
		this.pivasDrugManufMethodStatus = pivasDrugManufMethodStatus;
	}

	public String getPivasDrugManufMethodUpdateUser() {
		return pivasDrugManufMethodUpdateUser;
	}

	public void setPivasDrugManufMethodUpdateUser(String pivasDrugManufMethodUpdateUser) {
		this.pivasDrugManufMethodUpdateUser = pivasDrugManufMethodUpdateUser;
	}

	public Date getPivasDrugManufMethodUpdateDate() {
		return pivasDrugManufMethodUpdateDate;
	}

	public void setPivasDrugManufMethodUpdateDate(Date pivasDrugManufMethodUpdateDate) {
		this.pivasDrugManufMethodUpdateDate = pivasDrugManufMethodUpdateDate;
	}

}
