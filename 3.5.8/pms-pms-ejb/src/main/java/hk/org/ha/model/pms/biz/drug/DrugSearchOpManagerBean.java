package hk.org.ha.model.pms.biz.drug;

import static hk.org.ha.model.pms.prop.Prop.ALERT_HLA_DRUG_DISPLAYNAME;
import static hk.org.ha.model.pms.prop.Prop.ALERT_HLA_ENABLED;
import hk.org.ha.fmk.pms.remote.marshall.EclipseLinkXStreamMarshaller;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.alert.mds.AlertRetrieveManagerLocal;
import hk.org.ha.model.pms.biz.alert.mds.HlaTestManagerLocal;
import hk.org.ha.model.pms.biz.vetting.FmIndicationManagerLocal;
import hk.org.ha.model.pms.cache.DrugSiteMappingCacher;
import hk.org.ha.model.pms.corp.cache.DmSiteCacherInf;
import hk.org.ha.model.pms.corp.cache.MsFmStatusCacher;
import hk.org.ha.model.pms.corp.cache.SiteFormVerbCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmDrugProperty;
import hk.org.ha.model.pms.dms.persistence.DmMoeProperty;
import hk.org.ha.model.pms.dms.persistence.DmRegimen;
import hk.org.ha.model.pms.dms.persistence.MsFmStatus;
import hk.org.ha.model.pms.dms.vo.DrugCommonDosage;
import hk.org.ha.model.pms.dms.vo.DrugName;
import hk.org.ha.model.pms.dms.vo.DrugRouteCriteria;
import hk.org.ha.model.pms.dms.vo.FormVerb;
import hk.org.ha.model.pms.dms.vo.PmsFmStatusSearchCriteria;
import hk.org.ha.model.pms.dms.vo.PreparationProperty;
import hk.org.ha.model.pms.dms.vo.RouteForm;
import hk.org.ha.model.pms.persistence.AlertEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedOrderFm;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedOrderItemAlert;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.RemarkItemStatus;
import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
import hk.org.ha.model.pms.udt.rx.RxItemType;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.udt.vetting.NameType;
import hk.org.ha.model.pms.udt.vetting.PrnPercentage;
import hk.org.ha.model.pms.udt.vetting.RegimenDurationUnit;
import hk.org.ha.model.pms.udt.vetting.RegimenType;
import hk.org.ha.model.pms.vo.drug.DrugSearchReturn;
import hk.org.ha.model.pms.vo.rx.CapdRxDrug;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.Freq;
import hk.org.ha.model.pms.vo.rx.IvAdditive;
import hk.org.ha.model.pms.vo.rx.OralRxDrug;
import hk.org.ha.model.pms.vo.rx.Regimen;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.rx.Site;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;

@AutoCreate
@Stateless
@Name("drugSearchOpManager")
@MeasureCalls
public class DrugSearchOpManagerBean implements DrugSearchOpManagerLocal {
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private FmIndicationManagerLocal fmIndicationManager; 
	
	@In
	private Workstore workstore;
	
	@In
	private SiteFormVerbCacherInf siteFormVerbCacher;
	
	@In
	private DmSiteCacherInf dmSiteCacher;
	
	@In
	private AlertRetrieveManagerLocal alertRetrieveManager;
	
	@In
	private HlaTestManagerLocal hlaTestManager;
	
	@In
	private DrugRouteManagerLocal drugRouteManager;
	

	public DrugSearchReturn createFreeTextEntryDrug(DrugSearchSource drugSearchSource, RxDrug rxDrug, Integer carryDuration, RegimenDurationUnit carryDurationUnit, PharmOrder pharmOrder) {
		MedOrderItem drugSearchMedOrderItem = new MedOrderItem(rxDrug);
		
		Regimen regimen = new Regimen();
		List<DoseGroup> doseGroupList = new ArrayList<DoseGroup>();
		List<Dose> doseList = new ArrayList<Dose>();
		DoseGroup doseGroup = new DoseGroup();		
		Dose dose = new Dose();
		
		doseList.add(dose);
		doseGroup.setDoseList(doseList);
		doseGroupList.add(doseGroup);				
		regimen.setDoseGroupList(doseGroupList);
		rxDrug.setRegimen(regimen);

		Integer itemNum = pharmOrder.getMedOrder().getAndIncreaseMaxItemNum();
		
		drugSearchMedOrderItem.setItemNum(itemNum);
		drugSearchMedOrderItem.setOrgItemNum(itemNum);
		drugSearchMedOrderItem.setDangerDrugFlag(Boolean.FALSE);
		drugSearchMedOrderItem.setFixPeriodFlag(Boolean.FALSE);
		drugSearchMedOrderItem.setSingleUseFlag(Boolean.FALSE);
		drugSearchMedOrderItem.setRestrictFlag(Boolean.FALSE);
		drugSearchMedOrderItem.setFmStatus("T");

		regimen.setType(RegimenType.Daily);		// default regimen type
		
		if ( pharmOrder.getMedOrder().getDocType() == MedOrderDocType.Manual ) {
			doseGroup.setStartDate(pharmOrder.getMedOrder().getTicket().getTicketDate());
			// set duration
			if (carryDuration != null && carryDurationUnit != null) {
				regimen.carryDurationInfo(carryDuration, carryDurationUnit);
			}			
		} else {
			doseGroup.setStartDate(pharmOrder.getMedOrder().getOrderDate());
		}		
		
		dose.setDispQty(null);		// default disp qty
		
		drugSearchMedOrderItem.preSave();
		drugSearchMedOrderItem.setRxDrug(null);
		drugSearchMedOrderItem.postLoad();
		drugSearchMedOrderItem.loadDmInfo(pharmOrder.getMedOrder().isMpDischarge());
		
		drugSearchMedOrderItem.clearPharmOrderItemList();
		drugSearchMedOrderItem.setMedOrderItemAlertList(new ArrayList<MedOrderItemAlert>());
		
		DrugSearchReturn drugSearchReturn = new DrugSearchReturn();
		drugSearchReturn.setDrugSearchSource(drugSearchSource);
		drugSearchReturn.setDrugSearchMedOrderItem(drugSearchMedOrderItem);
		return drugSearchReturn;
	}
	
	public DrugSearchReturn createCapdDrug(DrugSearchSource drugSearchSource, PharmOrder pharmOrder) {
		MedOrderItem drugSearchMedOrderItem = new MedOrderItem(new CapdRxDrug());
		
		Integer itemNum = pharmOrder.getMedOrder().getAndIncreaseMaxItemNum();
		drugSearchMedOrderItem.setItemNum(itemNum);
		drugSearchMedOrderItem.setOrgItemNum(itemNum);

		drugSearchMedOrderItem.clearPharmOrderItemList();
		drugSearchMedOrderItem.setMedOrderItemAlertList(new ArrayList<MedOrderItemAlert>());
		
		DrugSearchReturn drugSearchReturn = new DrugSearchReturn();
		drugSearchReturn.setDrugSearchSource(drugSearchSource);
		drugSearchReturn.setDrugSearchMedOrderItem(drugSearchMedOrderItem);
		return drugSearchReturn;
	}

	public DrugSearchReturn createSelectedDrugByDosageConversion(DrugSearchSource drugSearchSource, DrugName drugName, RouteForm routeForm, PreparationProperty preparation, DrugCommonDosage commonDosage, DmRegimen dmRegimen) {
		DmDrug drugSearchDmDrug = new DmDrug();

		MsFmStatus msFmStatus = MsFmStatusCacher.instance().getMsFmStatusByFmStatus(routeForm.getPmsFmStatus());
		drugSearchDmDrug.setPmsFmStatus(msFmStatus);
		drugSearchDmDrug.setMoeRouteFormDesc(routeForm.getMoeRouteFormDesc());
		
		DmDrugProperty dmDrugProperty = new DmDrugProperty();
		dmDrugProperty.setDisplayname(drugName.getTrueDisplayname());
		dmDrugProperty.setFormCode(routeForm.getFormCode());
		dmDrugProperty.setSaltProperty(routeForm.getSaltProperty());
		drugSearchDmDrug.setDmDrugProperty(dmDrugProperty);
		
		DmMoeProperty dmMoeProperty = new DmMoeProperty();
		if (preparation != null) {
			dmMoeProperty.setMoDosageUnit(preparation.getGroupMoDosageUnit());
		} else {
			dmMoeProperty.setMoDosageUnit(routeForm.getGroupMoDosageUnit());
		}
		drugSearchDmDrug.setDmMoeProperty(dmMoeProperty);
		
		drugSearchDmDrug.setDrugKey(routeForm.getDrugKey());
		if (preparation != null) {
			drugSearchDmDrug.setItemCode( preparation.getItemCode() );
			drugSearchDmDrug.setStrength( preparation.getStrength() );
			drugSearchDmDrug.setVolumeValue( preparation.getVolumeValue() );
			drugSearchDmDrug.setVolumeUnit( preparation.getVolumeUnit() );
		}

		DrugCommonDosage drugSearchCommonDosage = null;	// init drugSearchCommonDosage
		if (commonDosage != null) {
			if (commonDosage.getDosageValue() != null) {
				drugSearchCommonDosage = commonDosage;
			}
		}
		
		logger.info("dosage conversion drug search source info: fmStatus = [#0] | displayname = [#1] | formDesc = [#2] | saltProperty = [#3] | dosage = [#4] | MO dosage unit = [#5]", 
						routeForm.getPmsFmStatus(), drugName.getTrueDisplayname(), routeForm.getMoeRouteFormDesc(),
						routeForm.getSaltProperty(), (commonDosage != null ? commonDosage.getDosageValue() : "no common dosage"),
						routeForm.getGroupMoDosageUnit());

		logger.info("dosage conversion drug search target info: fmStatus = [#0] | displayname = [#1] | formDesc = [#2] | saltProperty = [#3] | itemCode = [#4] | dosage = [#5] | MO dosage unit = [#6]", 
					(drugSearchDmDrug.getPmsFmStatus() != null ? drugSearchDmDrug.getPmsFmStatus().getFmStatus() : "no FM status"), 
					drugSearchDmDrug.getDmDrugProperty().getDisplayname(), drugSearchDmDrug.getMoeRouteFormDesc(), drugSearchDmDrug.getDmDrugProperty().getSaltProperty(),
					drugSearchDmDrug.getItemCode(),
					(drugSearchCommonDosage != null ? drugSearchCommonDosage.getDosageValue() : "no common dosage"),
					drugSearchDmDrug.getDmMoeProperty().getMoDosageUnit());
		
		logger.debug(new EclipseLinkXStreamMarshaller().toXML(drugSearchDmDrug));
		logger.debug(new EclipseLinkXStreamMarshaller().toXML(drugSearchCommonDosage));
		
		DrugSearchReturn drugSearchReturn = new DrugSearchReturn();
		drugSearchReturn.setDrugSearchSource(drugSearchSource);
		drugSearchReturn.setDrugSearchDmDrug(drugSearchDmDrug);
		drugSearchReturn.setDrugSearchCommonDosage(drugSearchCommonDosage);
		return drugSearchReturn;
	}
	
	public DrugSearchReturn createSelectedDrugByVetting(DrugSearchSource drugSearchSource, DrugName drugName, RouteForm routeForm, PreparationProperty preparation, DrugCommonDosage commonDosage, DmRegimen dmRegimen, Integer carryDuration, RegimenDurationUnit carryDurationUnit, String specialInstruction, PharmOrder pharmOrder) {
		// for strength compul drug, if select preparation, objects commonDosage, dmRegimen are null
		// for strength compul drug, if select commonDosage, object dmRegimen is null
		// for strength compul drug, if select dmRegimen, object commonDosage is null
		// for non-strength compul drug, if select routeForm, objects preparation, commonDosage, dmRegimen are null
		// for non-strength compul drug, if select commonDosage, objects preparation, dmRegimen are null
		// for non-strength compul drug, if select dmRegimen, objects preparation, commonDosage are null
		OralRxDrug oralRxDrug = new OralRxDrug(); 
		
		MedOrderItem drugSearchMedOrderItem = new MedOrderItem(oralRxDrug);
		
		Regimen regimen = new Regimen();
		List<DoseGroup> doseGroupList = new ArrayList<DoseGroup>();
		List<Dose> doseList = new ArrayList<Dose>();
		DoseGroup doseGroup = new DoseGroup();		
		Dose dose = new Dose();
		
		doseList.add(dose);
		doseGroup.setDoseList(doseList);
		doseGroupList.add(doseGroup);				
		regimen.setDoseGroupList(doseGroupList);
		oralRxDrug.setRegimen(regimen);


		// set MedOrderItem
		Integer itemNum = pharmOrder.getMedOrder().getAndIncreaseMaxItemNum();
		drugSearchMedOrderItem.setItemNum(itemNum);
		drugSearchMedOrderItem.setOrgItemNum(itemNum);
		
		// set OralRxDrug (from DrugName and RouteForm)
		oralRxDrug.setDisplayName( drugName.getTrueDisplayname() );
		oralRxDrug.setFirstDisplayName( drugName.getFirstDisplayname() );
		oralRxDrug.setSecondDisplayName( drugName.getSecondDisplayname() );
		oralRxDrug.setFormCode( routeForm.getFormCode() );
		oralRxDrug.setFormDesc( routeForm.getMoeDesc() );
		oralRxDrug.setSaltProperty( routeForm.getSaltProperty() );
		if (preparation == null) {			
			oralRxDrug.setBaseUnit( routeForm.getGroupBaseUnit() );
			oralRxDrug.setFmStatus( routeForm.getPmsFmStatus() );
		}
		if ("Y".equals(routeForm.getDangerousDrug())) {
			oralRxDrug.setDangerDrugFlag(Boolean.TRUE);
		} else {
			oralRxDrug.setDangerDrugFlag(Boolean.FALSE);
		}
		if (preparation == null) {
			if ("Y".equals(routeForm.getSpecRestrict())) {
				oralRxDrug.setRestrictFlag(Boolean.TRUE);
			} else {
				oralRxDrug.setRestrictFlag(Boolean.FALSE);
			}
		}
		oralRxDrug.setAliasName( routeForm.getTrueAliasname() );
		oralRxDrug.setRouteCode( routeForm.getRouteCode() );
		oralRxDrug.setRouteDesc( routeForm.getFullRouteDesc() );
		if (routeForm.getDisplayRouteDesc() != null) {
			oralRxDrug.setDisplayRouteDescFlag(Boolean.TRUE);
		} else {
			oralRxDrug.setDisplayRouteDescFlag(Boolean.FALSE);
		}
		oralRxDrug.setNameType( NameType.dataValueOf(drugName.getNameType()) );
		if (NameType.dataValueOf(drugName.getNameType()) == NameType.TradeName) {
			oralRxDrug.setTradeName( routeForm.getTrueAliasname() );
		}
		oralRxDrug.setSpecialInstruction(specialInstruction);
		
		if (preparation == null) {
			if ( routeForm.getGroupMaxDuration() != null && routeForm.getGroupMaxDuration().intValue() > 0 ) {		
				oralRxDrug.setSingleUseFlag(Boolean.TRUE);
			}
		}
		
		// set Dose (from routeForm)
		if (preparation == null) {
			dose.setDosageUnit(routeForm.getGroupMoDosageUnit());
		}
		FormVerb formVerb = siteFormVerbCacher.getDefaultFormVerbByFormCode( routeForm.getFormCode() );
		
		
		if (formVerb != null && isValidSiteCodeForDischarge(pharmOrder, oralRxDrug, formVerb.getSiteCode())) {
			dose.setSiteCode(formVerb.getSiteCode());
			dose.setSiteDesc(dmSiteCacher.getSiteBySiteCode(formVerb.getSiteCode()).getSiteDescEng());
			dose.setIpmoeDesc(dmSiteCacher.getSiteBySiteCode(formVerb.getSiteCode()).getIpmoeDesc());
			dose.setSiteCategoryCode(dmSiteCacher.getSiteBySiteCode(formVerb.getSiteCode()).getSiteCategoryCode());
			dose.setDmSite(dmSiteCacher.getSiteBySiteCode(formVerb.getSiteCode()));
			if (formVerb.getSupplementarySite() != null) {				
				dose.setSupplSiteDesc(formVerb.getSupplementarySite());
			} 
		}
		dose.setAdminTimeCode(routeForm.getAdminTimeCode());
		dose.setAdminTimeDesc(routeForm.getAdminTimeEng());
		if (preparation == null) {			
			dose.setBaseUnit(routeForm.getGroupBaseUnit());			
		}
				
		if (preparation != null) {
			// set OralRxDrug (from Preparation)
			oralRxDrug.setItemCode( preparation.getItemCode() );
			oralRxDrug.setBaseUnit( preparation.getBaseUnit() );
			oralRxDrug.setFmStatus( preparation.getPmsFmStatus() );
			if ("Y".equals(preparation.getSpecRestrict())) {
				oralRxDrug.setRestrictFlag(Boolean.TRUE);
			} else {
				oralRxDrug.setRestrictFlag(Boolean.FALSE);
			}
			oralRxDrug.setStrength( preparation.getStrength() );
			oralRxDrug.setStrengthValue( preparation.getStrengthValue() == null ? null : BigDecimal.valueOf(preparation.getStrengthValue()) );
			oralRxDrug.setStrengthUnit( preparation.getStrengthUnit() );
			if (preparation.getVolumeValue() != null) {
				oralRxDrug.setVolume( new BigDecimal(preparation.getVolumeValue()) );
			}
			oralRxDrug.setVolumeUnit( preparation.getVolumeUnit() );
			oralRxDrug.setExtraInfo( preparation.getExtraInfo() );
			if ("Y".equals(preparation.getExternalInd())) {
				oralRxDrug.setExternalUseFlag(Boolean.TRUE);
			} else if ("N".equals(preparation.getExternalInd())) {
				oralRxDrug.setExternalUseFlag(Boolean.FALSE);
			}
			if ( preparation.getMaxDuration() != null && preparation.getMaxDuration().intValue() > 0 ) {				
				oralRxDrug.setSingleUseFlag(Boolean.TRUE);
			}
			
			// set Dose (from preparation)
			dose.setDosageUnit(preparation.getGroupMoDosageUnit());
			dose.setBaseUnit(preparation.getBaseUnit());
		}


		// set regimen type from DrugCommonDosage or DmRegimen
		if (commonDosage != null) {
			regimen.setType(RegimenType.dataValueOf(commonDosage.getRegimen()));
		} else if (dmRegimen != null) {
			regimen.setType(RegimenType.dataValueOf(dmRegimen.getRegimenCode()));
		} else {
			regimen.setType(RegimenType.Daily);		// default regimen type
		}

		
		// set Regimen from DrugCommonDosage
		if (commonDosage != null) {
			if (commonDosage.getDosageValue() != null) {
				if (commonDosage.getDosageValue() == 0) {
					dose.setDosage(null);
				} else {
					dose.setDosage(new BigDecimal(commonDosage.getDosageValue()));
				}
			}		

			// set daily frequency desc
			StringBuilder freqDesc = new StringBuilder();
			if (commonDosage.getDailyFreq().getDailyFreqBlk1() != null) {
				freqDesc.append(commonDosage.getDailyFreq().getDailyFreqBlk1());
			}
			if (commonDosage.getFreqValue() != null && commonDosage.getFreqValue() != 0) {
				if (freqDesc.toString().length() > 0) {
					freqDesc.append(" ");
				}
				freqDesc.append(commonDosage.getFreqValue());
			}
			if (commonDosage.getDailyFreq().getDailyFreqBlk2() != null) {
				if (freqDesc.toString().length() > 0) {
					freqDesc.append(" ");
				}
				freqDesc.append(commonDosage.getDailyFreq().getDailyFreqBlk2());
			}
			
			Freq dailyFreq = new Freq();
			dailyFreq.setCode( commonDosage.getFreqCode() );
			dailyFreq.setDesc(freqDesc.toString());
			if (commonDosage.getFreqValue() != null && commonDosage.getFreqValue().intValue() != 0) {
				String[] dailyFreqParamArr = new String[1];
				dailyFreqParamArr[0] = commonDosage.getFreqValue().toString();
				dailyFreq.setParam(dailyFreqParamArr);
			}
			dose.setDailyFreq(dailyFreq);

			Freq supplFrequency = null;
			if (commonDosage.getSupplFreq() != null) {
				// set suppl frequency desc
				StringBuilder supFreqDesc = new StringBuilder();
				if (commonDosage.getSupplFreq().getSupplFreqBlk1() != null) {
					supFreqDesc.append(commonDosage.getSupplFreq().getSupplFreqBlk1());
				}
				if (commonDosage.getSupplFreqVal1() != null && commonDosage.getSupplFreqVal1() != 0) {
					if (supFreqDesc.toString().length() > 0) {
						supFreqDesc.append(" ");
					}
					supFreqDesc.append(commonDosage.getSupplFreqVal1());
				}
				if (commonDosage.getSupplFreq().getSupplFreqBlk2() != null) {
					if (supFreqDesc.toString().length() > 0) {
						supFreqDesc.append(" ");
					}
					supFreqDesc.append(commonDosage.getSupplFreq().getSupplFreqBlk2());
				}
				if (commonDosage.getSupplFreqVal2() != null && commonDosage.getSupplFreqVal2() != 0) {
					if (supFreqDesc.toString().length() > 0) {
						supFreqDesc.append(" ");
					}
					supFreqDesc.append(commonDosage.getSupplFreqVal2());
				}
				if (commonDosage.getSupplFreq().getSupplFreqBlk3() != null) {
					if (supFreqDesc.toString().length() > 0) {
						supFreqDesc.append(" ");
					}
					supFreqDesc.append(commonDosage.getSupplFreq().getSupplFreqBlk3());
				}			
			
				// set suppl freq
				supplFrequency = new Freq();
				supplFrequency.setCode( commonDosage.getSupplFreqCode() );
				supplFrequency.setDesc(supFreqDesc.toString());
				supplFrequency.setMultiplierType("-");
				if (commonDosage.getSupplFreqVal1() != null) {
					String[] supplFreqParamArr;
					if (commonDosage.getSupplFreqVal2() != null) {
						supplFreqParamArr = new String[2];
					} else {
						supplFreqParamArr = new String[1];
					}
					
					if ( commonDosage.getSupplFreqVal1().intValue() != 0 ) {
						supplFreqParamArr[0] = commonDosage.getSupplFreqVal1().toString();
					}
					if (commonDosage.getSupplFreqVal2() != null && commonDosage.getSupplFreqVal2().intValue() != 0) {
						supplFreqParamArr[1] = commonDosage.getSupplFreqVal2().toString();
					}
					supplFrequency.setParam(supplFreqParamArr);
				}
			}
			dose.setSupplFreq(supplFrequency);
									
			if ("Y".equals(commonDosage.getPrn())) {
				dose.setPrn(Boolean.TRUE);							
			}						
		}

		// set default duration unit
		if ( regimen.getType() == RegimenType.Weekly ) { 
			doseGroup.setDurationUnit(RegimenDurationUnit.Week);
		} else if ( regimen.getType() == RegimenType.Monthly ) {
			doseGroup.setDurationUnit(RegimenDurationUnit.Month);
		} else if ( regimen.getType() == RegimenType.Cyclic ) {
			doseGroup.setDurationUnit(RegimenDurationUnit.Cycle);
		}
		
		// set default dosage if form code is EYD
		if (commonDosage == null && "EYD".equals(oralRxDrug.getFormCode())) {
			dose.setDosage(new BigDecimal(1));			
		}	
		
		// set default prn and prn percent
		if (commonDosage == null || "N".equals(commonDosage.getPrn())) {
			dose.setPrn(Boolean.FALSE);
			dose.setPrnPercent(PrnPercentage.Hundred);
		}
		
		// set default start date
		if ( pharmOrder.getMedOrder().getDocType() == MedOrderDocType.Manual ) {
			doseGroup.setStartDate(pharmOrder.getMedOrder().getTicket().getTicketDate());
			// set duration
			if (carryDuration != null && carryDurationUnit != null) {
				regimen.carryDurationInfo(carryDuration, carryDurationUnit);			
			}
		} else {
			doseGroup.setStartDate(pharmOrder.getMedOrder().getOrderDate());			
		}		

		// set default disp qty
		dose.setDispQty(null);		
		if ( "Y".equals(routeForm.getDangerousDrug()) ) {
			dose.setBaseUnit(String.valueOf("DOSE"));
		}
		
		// add steps if regimen type is step-up/down
		if (regimen.getType() == RegimenType.StepUpDown) {
			for (int i=0; i<3; i++) {
				DoseGroup copyDoseGroup = new DoseGroup();
				BeanUtils.copyProperties(doseGroup, copyDoseGroup);
				regimen.getDoseGroupList().add(copyDoseGroup);
			}
		}
		
		// load DmInfo regimen detail
		drugSearchMedOrderItem.preSave();
		drugSearchMedOrderItem.setRxDrug(null);		
		drugSearchMedOrderItem.postLoad();
		drugSearchMedOrderItem.loadDmInfo(pharmOrder.getMedOrder().isMpDischarge());
		
		// set default action status and add FM to MedOrder if necessary
		addMedOrderFm(drugSearchMedOrderItem, pharmOrder);

		drugSearchMedOrderItem.clearPharmOrderItemList();
		drugSearchMedOrderItem.clearMedOrderItemAlertList();

		// add alert info
		if (alertRetrieveManager.isMdsEnable()) {
			for (AlertEntity alertEntity : alertRetrieveManager.retrieveAlertByMedOrderItem(drugSearchMedOrderItem, false)) {
				drugSearchMedOrderItem.addMedOrderItemAlert(new MedOrderItemAlert(alertEntity));
			}
			drugSearchMedOrderItem.markAlertFlag();
		}

		if (ALERT_HLA_ENABLED.get(Boolean.FALSE)
				&& ALERT_HLA_DRUG_DISPLAYNAME.get().equals(drugSearchMedOrderItem.getDisplayName())) {
			drugSearchMedOrderItem.addMedOrderItemAlert(new MedOrderItemAlert(hlaTestManager.constructHlaTestAlert(drugSearchMedOrderItem)));
			drugSearchMedOrderItem.getRxItem().setHlaFlag(Boolean.TRUE);
		}
		
		logger.debug(new EclipseLinkXStreamMarshaller().toXML(drugSearchMedOrderItem));
		
		DrugSearchReturn drugSearchReturn = new DrugSearchReturn();
		drugSearchReturn.setDrugSearchSource(drugSearchSource);
		drugSearchReturn.setDrugSearchMedOrderItem(drugSearchMedOrderItem);
		return drugSearchReturn;
	}	
	
	@SuppressWarnings("unchecked")
	private boolean isValidSiteCodeForDischarge(PharmOrder pharmOrder, OralRxDrug oralRxDrug, String siteCode) {

		if (pharmOrder == null || pharmOrder.getMedOrder() == null || !pharmOrder.getMedOrder().isMpDischarge()) {
			return true;
		}
		
		DrugRouteCriteria criteria = new DrugRouteCriteria();
		criteria.setDispHospCode(workstore.getHospCode());
		criteria.setDispWorkstore(workstore.getWorkstoreCode());
		criteria.setTrueDisplayname(oralRxDrug.getDisplayName());
		criteria.setFormCode(oralRxDrug.getFormCode());
		criteria.setSaltProperty(oralRxDrug.getSaltProperty());

		for (Site site : DrugSiteMappingCacher.instance().getDischargeOrderSiteList(criteria)) {
			if (StringUtils.equalsIgnoreCase(site.getSiteCode(), siteCode)) {
				return true;
			}
		}

		for (Map<String, Object> siteMap : drugRouteManager.retrieveIpOthersSiteMapListByDrugRouteCriteria(criteria)) {
			if (siteMap.get("site") == null || !(siteMap.get("site") instanceof Site)) {
				continue;
			}
			Site site = (Site) siteMap.get("site");
			if (StringUtils.equalsIgnoreCase(site.getSiteCode(), siteCode)) {
				return true;
			}
			if (siteMap.get("children") == null || !(siteMap.get("children") instanceof List)) {
				continue;
			}
			for (Map<String,Object> supplSiteMap : (List<Map<String,Object>>) siteMap.get("children")) {
				Site supplSite = (Site) supplSiteMap.get("site");
				if (StringUtils.equalsIgnoreCase(supplSite.getSupplSiteDesc(), siteCode)){
					return true;
				}
			}
		}
		
		return false;
	}
	
	private void addMedOrderFm(MedOrderItem drugSearchMedOrderItem, PharmOrder pharmOrder) {
		if (pharmOrder.getPrivateFlag()) {
			drugSearchMedOrderItem.getRxDrug().setActionStatus(ActionStatus.PurchaseByPatient);
			
		} else {
			FmStatus fmStatus = FmStatus.dataValueOf(drugSearchMedOrderItem.getRxDrug().getFmStatus());
			
			boolean validMedOrderFm = false;
			MedOrderFm medOrderFm = null;
			
			switch (fmStatus) {
				case SpecialDrug:
				case SelfFinancedItemC:
				case SelfFinancedItemD:
				case SafetyNetItem:
					// note: CAPD and free text item does not enter here because they don't call addMedOrderFm()
					medOrderFm = fmIndicationManager.retrieveLocalMedOrderFmByRxDrug(drugSearchMedOrderItem.getRxDrug(), pharmOrder.getMedOrder().getMedOrderFmList());

					boolean medOrderFmUsed = false;
					if (medOrderFm != null) {
						// remove order local FM and retrieve FM again by web service if the FM is not shared by any active and unconfirmed item
						// (include all non-CAPD items in current order with status NOT SysDeleted, and uncofirmed modify previous item) 
						List<MedOrderItem> checkMedOrderItemList = new ArrayList<MedOrderItem>();
						checkMedOrderItemList.addAll(pharmOrder.getMedOrder().getMedOrderItemList());
						for (MedOrderItem medOrderItem : pharmOrder.getMedOrder().getMedOrderItemList()) {
							if (medOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedModify) {
								MedOrderItem prevMedOrderItem = em.find(MedOrderItem.class, medOrderItem.getPrevMoItemId());
								prevMedOrderItem.loadDmInfo(pharmOrder.getMedOrder().isMpDischarge());
								checkMedOrderItemList.add(prevMedOrderItem);
							}
						}

						for (MedOrderItem checkMedOrderItem : checkMedOrderItemList) {
							if (checkMedOrderItem.getStatus() != MedOrderItemStatus.SysDeleted) {
								// note: CAPD and free text item can enter here because all active order line item are checked, 
								//       so need to prevent searching for local FM if item is CAPD or free text
								if (checkMedOrderItem.getRxItemType() == RxItemType.Oral || checkMedOrderItem.getRxItemType() == RxItemType.Injection) {
									if ( ! FmStatus.FreeTextEntryItem.getDataValue().equals(checkMedOrderItem.getFmStatus())) {
										List<MedOrderFm> findMedOrderFmList = new ArrayList<MedOrderFm>();
										findMedOrderFmList.add(medOrderFm);
										if (fmIndicationManager.retrieveLocalMedOrderFmByRxDrug(checkMedOrderItem.getRxDrug(), findMedOrderFmList) != null) {
											medOrderFmUsed = true;
											break;
										}
									}
								} else if (checkMedOrderItem.getRxItemType() == RxItemType.Iv) {
									List<MedOrderFm> findMedOrderFmList = new ArrayList<MedOrderFm>();
									findMedOrderFmList.add(medOrderFm);
									for (IvAdditive ivAdditive : checkMedOrderItem.getIvRxDrug().getIvAdditiveList()) {
										if (fmIndicationManager.retrieveLocalMedOrderFmByRxDrug(ivAdditive, findMedOrderFmList) != null) {
											medOrderFmUsed = true;
											break;
										}
									}
									if (medOrderFmUsed) {
										break;
									}
								}
							}
						}
						
						if ( ! medOrderFmUsed) {
							pharmOrder.getMedOrder().getMedOrderFmList().remove(medOrderFm);
						}
					}
					
					if (medOrderFm != null && medOrderFmUsed) {
						validMedOrderFm = true;
						
					} else {
						try {
							medOrderFm = fmIndicationManager.retrieveMedOrderFmByRxDrug(drugSearchMedOrderItem.getRxDrug());
						} catch (Exception e) {
							logger.warn("drug search fails to call retrieveFm web service", e);
						}

						if (medOrderFm != null) {
							// validate FM corp ind and set indValidPeriodForDisplay by DMS valid period 
							boolean validFmCorpIndByDms = fmIndicationManager.validateFmCorpIndByDms(drugSearchMedOrderItem.getRxDrug(), medOrderFm);
							if (validFmCorpIndByDms) {
								Integer validPeriod = medOrderFm.getIndValidPeriodForDisplay();
								DateTime authDateTime = new DateTime(medOrderFm.getFmCreateDate()).plusMonths(validPeriod.intValue());
								
								if (authDateTime.isAfter(new DateTime())) {
									validMedOrderFm = true;
									medOrderFm.setIndValidPeriod(validPeriod);	// update valid period by DMS value
									pharmOrder.getMedOrder().addMedOrderFm(medOrderFm);
								}
								
							}
						}
					}
					
					break;
			}
			
			if (validMedOrderFm) {
				drugSearchMedOrderItem.setFmIndFlag(Boolean.TRUE);
				
				// set default action status from presc option
				drugSearchMedOrderItem.getRxDrug().setActionStatus(ActionStatus.getDefaultActionStatusByPrescOption(medOrderFm.getPrescOption()));
				
			} else {
				PmsFmStatusSearchCriteria pmsFmStatusSearchCriteria = new PmsFmStatusSearchCriteria(); 
				pmsFmStatusSearchCriteria.setDispHospCode(workstore.getHospCode());
				pmsFmStatusSearchCriteria.setDispWorkstore(workstore.getWorkstoreCode());
				
				if (drugSearchMedOrderItem.getRxDrug().getStrengthCompulsory()) {
					pmsFmStatusSearchCriteria.setItemCode(drugSearchMedOrderItem.getRxDrug().getItemCode());
				} else {
					pmsFmStatusSearchCriteria.setTrueDisplayname(drugSearchMedOrderItem.getRxDrug().getDisplayName());
					pmsFmStatusSearchCriteria.setFormCode(drugSearchMedOrderItem.getRxDrug().getFormCode());
					pmsFmStatusSearchCriteria.setSaltProperty(drugSearchMedOrderItem.getRxDrug().getSaltProperty());
				}
				pmsFmStatusSearchCriteria.setPmsFmStatus(drugSearchMedOrderItem.getRxDrug().getFmStatus());
				
				// get special drug
				Boolean specialDrugFlag = fmIndicationManager.checkSpecialDrug(pmsFmStatusSearchCriteria);
				
				// set default action status from fmStatus
				drugSearchMedOrderItem.getRxDrug().setActionStatus(ActionStatus.getActionStatusByFmStatus(drugSearchMedOrderItem.getRxDrug().getFmStatus(), specialDrugFlag));
			}
		}		
	}
	
	@Remove
	public void destroy() 
	{
	}
}