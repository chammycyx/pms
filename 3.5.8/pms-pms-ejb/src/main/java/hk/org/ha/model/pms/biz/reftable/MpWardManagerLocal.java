package hk.org.ha.model.pms.biz.reftable;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
import hk.org.ha.model.pms.persistence.phs.WardStock;
import hk.org.ha.model.pms.vo.medprofile.Ward;
import hk.org.ha.model.pms.vo.medprofile.WardGroup;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

@Local
public interface MpWardManagerLocal {

	List<WardGroup> retrieveWardGroupList(WorkstoreGroup workstoreGroup);
	List<Ward> retrieveWardList(WorkstoreGroup workstoreGroup);
	List<String> retrieveWardCodeList(WorkstoreGroup workstoreGroup);
	List<String> retrieveWardCodeList(List<WardGroup> wardGroupList, WorkstoreGroup workstoreGroup);
	List<String> retrieveActiveWardCodeList(Workstore workstore);
	boolean isActiveWard(Workstore workstore, String wardCode);
	Map<String, WardStock> retrieveWardStockMap(String instCode, String wardCode);
	List<Ward> retrieveActiveWardList(WorkstoreGroup workstoreGroup);
	List<String> retrieveWardFilterList(Workstore workstore,
			List<Ward> wardFilterList, List<WardGroup> wardGroupFilterList);
	List<String> retrieveWardFilterList(Workstore workstore,
			Boolean showAllWards);
}
