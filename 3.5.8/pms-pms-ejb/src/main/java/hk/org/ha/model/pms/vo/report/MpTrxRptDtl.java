package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class MpTrxRptDtl implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private List<MpTrxRptItemDtl> mpTrxRptItemDtlList;

	private String ward;

	private String specCode;
	
	private String bedNum;

	private String caseNum;

	private String patName;

	private String patNameChi;

	private String hkid;
	
	private Boolean printPatHkidFlag;

	private String sex;

	private String age;

	private String bodyWeight;

	private String drugAllergenDesc;

	private String adrAlertDesc;

	private String otherAllergyDesc;

	private Boolean checkPregnancyFlag;
	
	private String alertProfileStatus;
	
	private Boolean noKnownDrugFlag;
	
	private Boolean privateFlag;
	
	private String drugSensitivityDesc;
	
	private String ehrAllergy;
	
	private String ehrAdr;
	
	public List<MpTrxRptItemDtl> getMpTrxRptItemDtlList() {
		return mpTrxRptItemDtlList;
	}

	public void setMpTrxRptItemDtlList(
			List<MpTrxRptItemDtl> mpTrxRptItemDtlList) {
		this.mpTrxRptItemDtlList = mpTrxRptItemDtlList;
	}

	public String getWard() {
		return ward;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public String getBedNum() {
		return bedNum;
	}

	public void setBedNum(String bedNum) {
		this.bedNum = bedNum;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getPatNameChi() {
		return patNameChi;
	}

	public void setPatNameChi(String patNameChi) {
		this.patNameChi = patNameChi;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public Boolean getPrintPatHkidFlag() {
		return printPatHkidFlag;
	}

	public void setPrintPatHkidFlag(Boolean printPatHkidFlag) {
		this.printPatHkidFlag = printPatHkidFlag;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getBodyWeight() {
		return bodyWeight;
	}

	public void setBodyWeight(String bodyWeight) {
		this.bodyWeight = bodyWeight;
	}

	public String getDrugAllergenDesc() {
		return drugAllergenDesc;
	}

	public void setDrugAllergenDesc(String drugAllergenDesc) {
		this.drugAllergenDesc = drugAllergenDesc;
	}

	public String getAdrAlertDesc() {
		return adrAlertDesc;
	}

	public void setAdrAlertDesc(String adrAlertDesc) {
		this.adrAlertDesc = adrAlertDesc;
	}

	public String getOtherAllergyDesc() {
		return otherAllergyDesc;
	}

	public void setOtherAllergyDesc(String otherAllergyDesc) {
		this.otherAllergyDesc = otherAllergyDesc;
	}

	public Boolean getCheckPregnancyFlag() {
		return checkPregnancyFlag;
	}

	public void setCheckPregnancyFlag(Boolean checkPregnancyFlag) {
		this.checkPregnancyFlag = checkPregnancyFlag;
	}

	
	public String getAlertProfileStatus() {
		return alertProfileStatus;
	}

	public void setAlertProfileStatus(String alertProfileStatus) {
		this.alertProfileStatus = alertProfileStatus;
	}

	public Boolean getNoKnownDrugFlag() {
		return noKnownDrugFlag;
	}

	public void setNoKnownDrugFlag(Boolean noKnownDrugFlag) {
		this.noKnownDrugFlag = noKnownDrugFlag;
	}

	public void setPrivateFlag(Boolean privateFlag) {
		this.privateFlag = privateFlag;
	}

	public Boolean getPrivateFlag() {
		return privateFlag;
	}

	public void setDrugSensitivityDesc(String drugSensitivityDesc) {
		this.drugSensitivityDesc = drugSensitivityDesc;
	}

	public String getDrugSensitivityDesc() {
		return drugSensitivityDesc;
	}	

	public String getEhrAllergy() {
		return ehrAllergy;
	}

	public void setEhrAllergy(String ehrAllergy) {
		this.ehrAllergy = ehrAllergy;
	}

	public String getEhrAdr() {
		return ehrAdr;
	}

	public void setEhrAdr(String ehrAdr) {
		this.ehrAdr = ehrAdr;
	}
}
