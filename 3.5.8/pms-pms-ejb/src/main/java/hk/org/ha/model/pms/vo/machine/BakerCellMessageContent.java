package hk.org.ha.model.pms.vo.machine;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class BakerCellMessageContent implements Serializable {

	private static final long serialVersionUID = 1L;

	private String hostname;
	
	private int portNum;
	
	private String bakerCellNum;
	
	private String issueQty;
	
	private String ticketNum;
	
	private String patName;
	
	private String itemCode;

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public int getPortNum() {
		return portNum;
	}

	public void setPortNum(int portNum) {
		this.portNum = portNum;
	}

	public String getIssueQty() {
		return issueQty;
	}

	public void setIssueQty(String issueQty) {
		this.issueQty = issueQty;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getBakerCellNum() {
		return bakerCellNum;
	}

	public void setBakerCellNum(String bakerCellNum) {
		this.bakerCellNum = bakerCellNum;
	}
}
