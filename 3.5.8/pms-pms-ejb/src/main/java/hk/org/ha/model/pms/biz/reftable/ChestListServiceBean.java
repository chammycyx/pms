package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.reftable.Chest;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("chestListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ChestListServiceBean implements ChestListServiceLocal {
	
	@In
	private ChestManagerLocal chestManager;
	
	@Out(required = false)
	private  List<Chest> chestList;
	
	public void retrieveChestList(String prefixUnitDose) 
	{
		if ( StringUtils.isNotBlank(prefixUnitDose) ) {
			chestList =  chestManager.retrieveChestListByPrefixUnitDose(prefixUnitDose);
		} else {
			chestList =  chestManager.retrieveChestList();
		}
	}

	@Remove
	public void destroy() 
	{
		if ( chestList != null ) {
			chestList = null;
		}
	}
}
