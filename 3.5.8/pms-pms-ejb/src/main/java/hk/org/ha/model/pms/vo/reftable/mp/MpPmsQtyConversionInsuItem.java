package hk.org.ha.model.pms.vo.reftable.mp;

import hk.org.ha.model.pms.dms.persistence.PmsQtyConversionInsu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class MpPmsQtyConversionInsuItem implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String workstoreCode;
	
	private String hospCode;
	
	private boolean sameWorkstoreGroup; 
	
	private List<PmsQtyConversionInsu> pmsQtyConversionInsuList;

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setSameWorkstoreGroup(boolean sameWorkstoreGroup) {
		this.sameWorkstoreGroup = sameWorkstoreGroup;
	}

	public boolean isSameWorkstoreGroup() {
		return sameWorkstoreGroup;
	}

	public void setPmsQtyConversionInsuList(List<PmsQtyConversionInsu> pmsQtyConversionInsuList) {
		this.pmsQtyConversionInsuList = pmsQtyConversionInsuList;
	}

	public List<PmsQtyConversionInsu> getPmsQtyConversionInsuList() {
		if ( pmsQtyConversionInsuList == null ) {
			pmsQtyConversionInsuList = new ArrayList<PmsQtyConversionInsu>();
		}
		return pmsQtyConversionInsuList;
	}
	
}