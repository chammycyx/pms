package hk.org.ha.model.pms.persistence.pivas;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.model.pms.corp.cache.DmSiteCacher;
import hk.org.ha.model.pms.dms.persistence.Company;
import hk.org.ha.model.pms.dms.persistence.DmDiluent;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmSite;
import hk.org.ha.model.pms.dms.persistence.DmWarning;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasContainer;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaMethod;
import hk.org.ha.model.pms.dms.udt.pivas.ExpiryDateTitle;
import hk.org.ha.model.pms.dms.udt.pivas.PivasFormulaType;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.udt.pivas.PivasBatchPrepReprintType;
import hk.org.ha.model.pms.udt.pivas.PivasBatchPrepStatus;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.label.LabelContainer;
import hk.org.ha.model.pms.vo.label.PivasMergeFormulaProductLabel;
import hk.org.ha.model.pms.vo.label.PivasMergeFormulaWorksheet;
import hk.org.ha.model.pms.vo.label.PivasProductLabel;
import hk.org.ha.model.pms.vo.label.PivasWorksheet;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.PrivateOwned;

@Entity
@Table(name = "PIVAS_BATCH_PREP")
public class PivasBatchPrep extends VersionEntity {

	private static final long serialVersionUID = 1L;
	
	private static final String JAXB_CONTEXT_LABEL = "hk.org.ha.model.pms.vo.label";

	@Id
	@Column(name = "LOT_NUM", nullable = false, length = 14)
	private String lotNum;

	@Column(name = "PIVAS_FORMULA_METHOD_ID")
	private Long pivasFormulaMethodId;
	
	@Column(name = "MANUFACTURE_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date manufactureDate;
	
	@Column(name = "VOLUME", precision = 19, scale = 4)
	private BigDecimal volume;
	
	@Column(name = "PRINT_NAME", nullable = false, length = 200)
	private String printName;
	
	@Column(name = "SHELF_LIFE", nullable = false)
	private Integer shelfLife;
	
	@Column(name = "PREP_EXPIRY_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date prepExpiryDate;

	@Column(name = "PREP_QTY", precision = 19, scale = 4)
	private BigDecimal prepQty;
	
	@Column(name = "NUM_OF_LABEL", nullable = false, length = 3)
	private Integer numOfLabel;
	
	@Column(name = "PIVAS_CONTAINER_ID", nullable = false, length = 19)
	private Long pivasContainerId;

	@Column(name = "DILUENT_CODE", length = 25)
	private String diluentCode;
	
	@Column(name = "DILUENT_DESC", length = 100)
	private String diluentDesc;
	
	@Column(name = "DILUENT_ITEM_CODE", length = 20)
	private String diluentItemCode;

	@Column(name = "DILUENT_MANUF_CODE", length = 20)
	private String diluentManufCode;

	@Column(name = "DILUENT_BATCH_NUM", length = 20)
	private String diluentBatchNum;
	
	@Column(name = "DILUENT_EXPIRY_DATE")
	@Temporal(TemporalType.DATE)
	private Date diluentExpiryDate;

	@Column(name = "WARN_CODE1", length = 4)
	private String warnCode1;
	
	@Column(name = "WARN_CODE2", length = 4)
	private String warnCode2;
	
	@Column(name = "WARN_CODE3", length = 4)
	private String warnCode3;
	
	@Column(name = "WARN_CODE4", length = 4)
	private String warnCode4;
	
	@Column(name = "CONCN", precision = 19, scale = 4)
	private BigDecimal concn;
	
	@Lob
	@Column(name = "LABEL_XML", nullable = false)
	private String labelXml;

	@Converter(name = "PivasBatchPrep.status", converterClass = PivasBatchPrepStatus.Converter.class)
    @Convert("PivasBatchPrep.status")
	@Column(name="STATUS", nullable = false, length = 1)
	private PivasBatchPrepStatus status;
	
	@PrivateOwned
    @OneToMany(mappedBy="pivasBatchPrep", cascade=CascadeType.ALL)
    private List<PivasBatchPrepItem> pivasBatchPrepItemList;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "HOSP_CODE", nullable = false)
	private Hospital hospital;

	@Column(name = "HOSP_CODE", nullable = false, insertable = false, updatable = false)
	private String hospCode;	
	
	@Column(name = "SITE_CODE", nullable = false)
	private String siteCode;
	
	@Converter(name = "PivasBatchPrep.pivasFormulaType", converterClass = PivasFormulaType.Converter.class)
    @Convert("PivasBatchPrep.pivasFormulaType")
	@Column(name="PIVAS_FORMULA_TYPE", nullable = false, length = 1)
	private PivasFormulaType pivasFormulaType;
	
	@Column(name = "ITEM_CODE", nullable = false, length = 10)
	private String itemCode;
	
	@Column(name = "DRUG_KEY", nullable = true)
	private Integer drugKey;
	
	@Converter(name = "PivasBatchPrep.expiryDateTitle", converterClass = ExpiryDateTitle.Converter.class)
    @Convert("PivasBatchPrep.expiryDateTitle")
	@Column(name = "EXPIRY_DATE_TITLE", nullable = false, length = 1)
	private ExpiryDateTitle expiryDateTitle;
	
	@Transient
	private PivasWorksheet pivasWorksheet;
	
	@Transient
	private PivasProductLabel pivasProductLabel;
	
	@Transient
	private PivasMergeFormulaWorksheet pivasMergeFormulaWorksheet;
	
	@Transient
	private PivasMergeFormulaProductLabel pivasMergeFormulaProductLabel;
	
	@Transient
	private PivasFormulaMethod pivasFormulaMethod;
	
	@Transient
	private DmWarning dmWarning1;
	
	@Transient
	private DmWarning dmWarning2;
	
	@Transient
	private DmWarning dmWarning3;
	
	@Transient
	private DmWarning dmWarning4;
	
	@Transient
	private DmDrug diluentDmDrug;

	@Transient
	private DmSite dmSite;

	@Transient
	private Company diluentCompany;

	@Transient
	private PivasContainer pivasContainer;

	@Transient
	private DmDiluent dmDiluent;
	
	@Transient
	private PrintDocType printDocType;
	
	@Transient
	private PivasBatchPrepReprintType reprintType;
	
	@Transient
	private Integer reprintNumOfLabel;

	@Deprecated
	@Transient
	private Integer pivasBatchPrepItemDrugKey;
	
	@Deprecated
	@Transient
	private BigDecimal dosageQty;
	
	@Deprecated
	@Transient
	private String pivasDosageUnit;
	
	@Deprecated
	@Transient
	private PivasBatchPrepItem pivasBatchPrepItem;
	
	public PivasBatchPrep() {
	}
	
	@PostLoad
	public void postLoad() {
		if (labelXml != null) {
			JaxbWrapper<LabelContainer> labelContainerJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
			LabelContainer pivasLabel = labelContainerJaxbWrapper.unmarshall(labelXml);
			if ( pivasLabel != null ) {
				if (pivasFormulaType != PivasFormulaType.Merge) {
					pivasWorksheet = pivasLabel.getPivasWorksheet();
					pivasProductLabel = pivasLabel.getPivasProductLabel();
				} else {
					pivasMergeFormulaWorksheet = pivasLabel.getPivasMergeFormulaWorksheet();
					pivasMergeFormulaProductLabel = pivasLabel.getPivasMergeFormulaProductLabel();
				}
			}
		}
		
		dmSite = DmSiteCacher.instance().getSiteBySiteCode(siteCode);
	}
	
	@PrePersist
	@PreUpdate
	public void preSave() {
		if ( pivasWorksheet != null || pivasProductLabel != null || pivasMergeFormulaWorksheet != null || pivasMergeFormulaProductLabel != null ) {
			JaxbWrapper<LabelContainer> labelContainerJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
			LabelContainer pivasLabel = new LabelContainer();
			if (pivasFormulaType != PivasFormulaType.Merge) {
				pivasLabel.setPivasWorksheet(pivasWorksheet);
				pivasLabel.setPivasProductLabel(pivasProductLabel);
			} else {
				pivasLabel.setPivasMergeFormulaWorksheet(pivasMergeFormulaWorksheet);
				pivasLabel.setPivasMergeFormulaProductLabel(pivasMergeFormulaProductLabel);
			}
			labelXml = labelContainerJaxbWrapper.marshall(pivasLabel);
		}
	}

	public Long getPivasFormulaMethodId() {
		return pivasFormulaMethodId;
	}

	public void setPivasFormulaMethodId(Long pivasFormulaMethodId) {
		this.pivasFormulaMethodId = pivasFormulaMethodId;
	}

	public Date getManufactureDate() {
		return manufactureDate;
	}

	public void setManufactureDate(Date manufactureDate) {
		this.manufactureDate = manufactureDate;
	}

	@Deprecated
	public BigDecimal getDosageQty() {
		return dosageQty;
	}

	@Deprecated
	public void setDosageQty(BigDecimal dosageQty) {
		this.dosageQty = dosageQty;
	}

	@Deprecated
	public String getPivasDosageUnit() {
		return pivasDosageUnit;
	}

	@Deprecated
	public void setPivasDosageUnit(String pivasDosageUnit) {
		this.pivasDosageUnit = pivasDosageUnit;
	}

	public BigDecimal getVolume() {
		return volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	public String getPrintName() {
		return printName;
	}

	public void setPrintName(String printName) {
		this.printName = printName;
	}

	public Integer getShelfLife() {
		return shelfLife;
	}

	public void setShelfLife(Integer shelfLife) {
		this.shelfLife = shelfLife;
	}

	public Date getPrepExpiryDate() {
		return prepExpiryDate;
	}

	public void setPrepExpiryDate(Date prepExpiryDate) {
		this.prepExpiryDate = prepExpiryDate;
	}

	public BigDecimal getPrepQty() {
		return prepQty;
	}

	public void setPrepQty(BigDecimal prepQty) {
		this.prepQty = prepQty;
	}

	public Integer getNumOfLabel() {
		return numOfLabel;
	}

	public void setNumOfLabel(Integer numOfLabel) {
		this.numOfLabel = numOfLabel;
	}

	public Long getPivasContainerId() {
		return pivasContainerId;
	}

	public void setPivasContainerId(Long pivasContainerId) {
		this.pivasContainerId = pivasContainerId;
	}

	public String getDiluentCode() {
		return diluentCode;
	}

	public void setDiluentCode(String diluentCode) {
		this.diluentCode = diluentCode;
	}

	public String getDiluentDesc() {
		return diluentDesc;
	}

	public void setDiluentDesc(String diluentDesc) {
		this.diluentDesc = diluentDesc;
	}

	public String getDiluentItemCode() {
		return diluentItemCode;
	}

	public void setDiluentItemCode(String diluentItemCode) {
		this.diluentItemCode = diluentItemCode;
	}

	public String getDiluentManufCode() {
		return diluentManufCode;
	}

	public void setDiluentManufCode(String diluentManufCode) {
		this.diluentManufCode = diluentManufCode;
	}

	public Date getDiluentExpiryDate() {
		return diluentExpiryDate;
	}

	public void setDiluentExpiryDate(Date diluentExpiryDate) {
		this.diluentExpiryDate = diluentExpiryDate;
	}

	public String getWarnCode1() {
		return warnCode1;
	}

	public void setWarnCode1(String warnCode1) {
		this.warnCode1 = warnCode1;
	}

	public String getWarnCode2() {
		return warnCode2;
	}

	public void setWarnCode2(String warnCode2) {
		this.warnCode2 = warnCode2;
	}

	public String getWarnCode3() {
		return warnCode3;
	}

	public void setWarnCode3(String warnCode3) {
		this.warnCode3 = warnCode3;
	}

	public String getWarnCode4() {
		return warnCode4;
	}

	public void setWarnCode4(String warnCode4) {
		this.warnCode4 = warnCode4;
	}

	public PivasBatchPrepStatus getStatus() {
		return status;
	}

	public void setStatus(PivasBatchPrepStatus status) {
		this.status = status;
	}

	public void setHospital(Hospital hospital) {
		this.hospital = hospital;
	}

	public Hospital getHospital() {
		return hospital;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getHospCode() {
		return hospCode;
	}

	public PivasFormulaType getPivasFormulaType() {
		return pivasFormulaType;
	}

	public void setPivasFormulaType(PivasFormulaType pivasFormulaType) {
		this.pivasFormulaType = pivasFormulaType;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public ExpiryDateTitle getExpiryDateTitle() {
		return expiryDateTitle;
	}

	public void setExpiryDateTitle(ExpiryDateTitle expiryDateTitle) {
		this.expiryDateTitle = expiryDateTitle;
	}

	public void setPivasFormulaMethod(PivasFormulaMethod pivasFormulaMethod) {
		this.pivasFormulaMethod = pivasFormulaMethod;
	}

	public PivasFormulaMethod getPivasFormulaMethod() {
		return pivasFormulaMethod;
	}

	public DmWarning getDmWarning1() {
		return dmWarning1;
	}

	public void setDmWarning1(DmWarning dmWarning1) {
		this.dmWarning1 = dmWarning1;
	}

	public DmWarning getDmWarning2() {
		return dmWarning2;
	}

	public void setDmWarning2(DmWarning dmWarning2) {
		this.dmWarning2 = dmWarning2;
	}

	public DmWarning getDmWarning3() {
		return dmWarning3;
	}

	public void setDmWarning3(DmWarning dmWarning3) {
		this.dmWarning3 = dmWarning3;
	}

	public DmWarning getDmWarning4() {
		return dmWarning4;
	}

	public void setDmWarning4(DmWarning dmWarning4) {
		this.dmWarning4 = dmWarning4;
	}

	public PivasContainer getPivasContainer() {
		return pivasContainer;
	}

	public void setPivasContainer(PivasContainer pivasContainer) {
		this.pivasContainer = pivasContainer;
	}

	public Company getDiluentCompany() {
		return diluentCompany;
	}

	public void setDiluentCompany(Company diluentCompany) {
		this.diluentCompany = diluentCompany;
	}

	public BigDecimal getConcn() {
		return concn;
	}

	public void setConcn(BigDecimal concn) {
		this.concn = concn;
	}

	public String getLotNum() {
		return lotNum;
	}

	public void setLotNum(String lotNum) {
		this.lotNum = lotNum;
	}

	public String getDiluentBatchNum() {
		return diluentBatchNum;
	}

	public void setDiluentBatchNum(String diluentBatchNum) {
		this.diluentBatchNum = diluentBatchNum;
	}

	public Integer getDrugKey() {
		return drugKey;
	}

	public void setDrugKey(Integer drugKey) {
		this.drugKey = drugKey;
	}

	public List<PivasBatchPrepItem> getPivasBatchPrepItemList() {
		if ( pivasBatchPrepItemList == null ) {
			pivasBatchPrepItemList = new ArrayList<PivasBatchPrepItem>();
		}
		return pivasBatchPrepItemList;
	}

	public void setPivasBatchPrepItemList(List<PivasBatchPrepItem> pivasBatchPrepItemList) {
		this.pivasBatchPrepItemList = pivasBatchPrepItemList;
	}

	public DmDrug getDiluentDmDrug() {
		return diluentDmDrug;
	}

	public void setDiluentDmDrug(DmDrug diluentDmDrug) {
		this.diluentDmDrug = diluentDmDrug;
	}

	public PivasWorksheet getPivasWorksheet() {
		return pivasWorksheet;
	}

	public void setPivasWorksheet(PivasWorksheet pivasWorksheet) {
		this.pivasWorksheet = pivasWorksheet;
	}

	public String getLabelXml() {
		return labelXml;
	}

	public void setLabelXml(String labelXml) {
		this.labelXml = labelXml;
	}

	public PivasProductLabel getPivasProductLabel() {
		return pivasProductLabel;
	}

	public void setPivasProductLabel(PivasProductLabel pivasProductLabel) {
		this.pivasProductLabel = pivasProductLabel;
	}

	public PivasMergeFormulaWorksheet getPivasMergeFormulaWorksheet() {
		return pivasMergeFormulaWorksheet;
	}

	public void setPivasMergeFormulaWorksheet(
			PivasMergeFormulaWorksheet pivasMergeFormulaWorksheet) {
		this.pivasMergeFormulaWorksheet = pivasMergeFormulaWorksheet;
	}

	public PivasMergeFormulaProductLabel getPivasMergeFormulaProductLabel() {
		return pivasMergeFormulaProductLabel;
	}

	public void setPivasMergeFormulaProductLabel(
			PivasMergeFormulaProductLabel pivasMergeFormulaProductLabel) {
		this.pivasMergeFormulaProductLabel = pivasMergeFormulaProductLabel;
	}

	public DmDiluent getDmDiluent() {
		return dmDiluent;
	}

	public void setDmDiluent(DmDiluent dmDiluent) {
		this.dmDiluent = dmDiluent;
	}
	
	public String getSiteCode() {
		return siteCode;
	}

	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

	public DmSite getDmSite() {
		return dmSite;
	}

	public void setDmSite(DmSite dmSite) {
		this.dmSite = dmSite;
	}
	
	public PrintDocType getPrintDocType() {
		return printDocType;
	}

	public void setPrintDocType(PrintDocType printDocType) {
		this.printDocType = printDocType;
	}

	public PivasBatchPrepReprintType getReprintType() {
		return reprintType;
	}

	public void setReprintType(PivasBatchPrepReprintType reprintType) {
		this.reprintType = reprintType;
	}

	public Integer getReprintNumOfLabel() {
		return reprintNumOfLabel;
	}

	public void setReprintNumOfLabel(Integer reprintNumOfLabel) {
		this.reprintNumOfLabel = reprintNumOfLabel;
	}

	@Deprecated
	public Integer getPivasBatchPrepItemDrugKey() {
		return pivasBatchPrepItemDrugKey;
	}

	@Deprecated
	public void setPivasBatchPrepItemDrugKey(Integer pivasBatchPrepItemDrugKey) {
		this.pivasBatchPrepItemDrugKey = pivasBatchPrepItemDrugKey;
	}

	@Deprecated
	public PivasBatchPrepItem getPivasBatchPrepItem() {
		return pivasBatchPrepItem;
	}

	@Deprecated
	public void setPivasBatchPrepItem(PivasBatchPrepItem pivasBatchPrepItem) {
		this.pivasBatchPrepItem = pivasBatchPrepItem;
	}

	@Deprecated
	public void loadPivasBatchPrepItem() {
		pivasBatchPrepItem = this.getPivasBatchPrepItemList().get(0);
		pivasBatchPrepItemDrugKey = pivasBatchPrepItem.getDrugKey();
		dosageQty = pivasBatchPrepItem.getDosageQty();
		pivasDosageUnit = pivasBatchPrepItem.getPivasDosageUnit();
	}

	@Deprecated
	public PivasBatchPrepItem savePivasBatchPrepItem() {
		if (pivasBatchPrepItem == null) {
			pivasBatchPrepItem = new PivasBatchPrepItem();
			pivasBatchPrepItem.setPivasBatchPrep(this);
		}
		pivasBatchPrepItem.setDrugKey(pivasBatchPrepItemDrugKey);
		pivasBatchPrepItem.setDosageQty(dosageQty);
		pivasBatchPrepItem.setPivasDosageUnit(pivasDosageUnit);
		return pivasBatchPrepItem;
	}

}
