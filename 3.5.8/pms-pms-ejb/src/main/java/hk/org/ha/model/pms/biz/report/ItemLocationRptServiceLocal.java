package hk.org.ha.model.pms.biz.report;

import java.util.List;

import hk.org.ha.model.pms.persistence.reftable.ItemLocation;
import hk.org.ha.model.pms.vo.report.ItemLocationRpt;

import javax.ejb.Local;

@Local
public interface ItemLocationRptServiceLocal {
	
	void printItemLocationRpt(List<ItemLocationRpt> printItemLocationList);
	
	void sendExportItemLocationRpt(List<ItemLocationRpt> printItemLocationList);

	void retrieveItemLocationRpt(ItemLocationRpt itemLocationRpt); 
	
	List<ItemLocation> retrieveItemLocationList(ItemLocationRpt itemLocationRpt);
	
	List<ItemLocationRpt> constructItemLocationRptList (ItemLocationRpt itemLocationRpt, List<ItemLocation> itemLocationList);
	
	void clearCbxList();
	
	void exportItemLocationRpt();

	void destroy();
	
}
