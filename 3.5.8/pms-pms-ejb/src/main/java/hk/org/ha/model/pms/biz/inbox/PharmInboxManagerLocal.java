package hk.org.ha.model.pms.biz.inbox;
import hk.org.ha.model.pms.persistence.reftable.Workstation;

import javax.ejb.Local;

@Local
public interface PharmInboxManagerLocal {

	void setShowAllWards(Workstation workstation, Boolean showAllWards);
	Boolean getShowAllWards(Workstation workstation);
	Boolean getShowChemoProfileOnly(Workstation workstation,
			Boolean currentValue);
}
