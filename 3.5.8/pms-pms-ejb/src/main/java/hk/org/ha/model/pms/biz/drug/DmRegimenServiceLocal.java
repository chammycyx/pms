package hk.org.ha.model.pms.biz.drug;

import javax.ejb.Local;

@Local
public interface DmRegimenServiceLocal {
	
	void retrieveDmRegimen(String regimenCode);
	
	boolean isRetrieveSuccess();
	
	void destroy();
	
}
 