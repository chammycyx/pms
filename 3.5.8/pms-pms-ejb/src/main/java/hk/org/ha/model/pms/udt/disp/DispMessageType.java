package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DispMessageType implements StringValuedEnum {

	// From MOE to PMS
	PmsDispOrder("P", "pmsDispOrder", "order"),
	PmsDispOrderStatus("D", "pmsDispOrderStatus", "status");
	
	private final String dataValue;
	private final String displayValue;
	private final String packageName;
	
	DispMessageType (final String dataValue, final String displayValue, final String packageName) {
		this.dataValue = dataValue;
		this.displayValue = displayValue; // xsd name
		this.packageName = packageName;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}

	public String getPackageName() {
		return this.packageName;
	}
	
	public static DispMessageType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DispMessageType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<DispMessageType> {

		private static final long serialVersionUID = 1L;

		public Class<DispMessageType> getEnumClass() {
    		return DispMessageType.class;
    	}
    }
}