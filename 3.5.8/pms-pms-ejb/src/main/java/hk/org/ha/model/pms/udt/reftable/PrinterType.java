package hk.org.ha.model.pms.udt.reftable;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PrinterType implements StringValuedEnum {

	Normal("N", "Normal"),
	Default("D", "Default"),
	Urgent("U", "Urgent"),
	Fast("F", "Fast Queue"),
	DefaultAndUrgent("DU", "Default and Urgent"),
	DefaultAndFast("DF", "Default and Fast Queue"),
	UrgentAndFast("UF", "Urgent and Fast Queue"),
	DefaultAndUrgentAndFast("DUF", "Default, Urgent and Fast Queue");

    private final String dataValue;
    private final String displayValue;

    PrinterType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }
    
    public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}
	
	public static PrinterType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PrinterType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<PrinterType> {

		private static final long serialVersionUID = 1L;

		public Class<PrinterType> getEnumClass() {
			return PrinterType.class;
		}
		
	}
}
