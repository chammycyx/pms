package hk.org.ha.model.pms.persistence.disp;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.vetting.CapdVoucherStatus;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.PrivateOwned;

@Entity
@Table(name = "CAPD_VOUCHER")
public class CapdVoucher extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "capdVoucherSeq")
	@SequenceGenerator(name = "capdVoucherSeq", sequenceName = "SQ_CAPD_VOUCHER", initialValue = 100000000)
	private Long id;

	@Column(name = "VOUCHER_NUM", nullable = false, length = 11)
	private String voucherNum;
	
	@Column(name = "MANUAL_VOUCHER_NUM", length = 11)
	private String manualVoucherNum;
	
	@Column(name = "SUPPLIER_CODE", nullable = false, length = 4)
	private String supplierCode;
	
	@Converter(name = "CapdVoucher.status", converterClass = CapdVoucherStatus.Converter.class)
    @Convert("CapdVoucher.status")
	@Column(name = "STATUS", nullable = false, length = 1)
	private CapdVoucherStatus status;
	
	@PrivateOwned 
	@OneToMany(mappedBy = "capdVoucher", cascade = CascadeType.ALL)
	private List<CapdVoucherItem> capdVoucherItemList;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "DISP_ORDER_ID", nullable = false)
    private DispOrder dispOrder;
    	
    @Transient
    private Boolean reprint;
    
    @Transient
    private DispOrderAdminStatus adminStatus;
        
	public CapdVoucher() {
		status = CapdVoucherStatus.Issued;
		reprint = false;
	}
		
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CapdVoucherStatus getStatus() {
		return status;
	}

	public void setStatus(CapdVoucherStatus status) {
		this.status = status;
	}

	public List<CapdVoucherItem> getCapdVoucherItemList() {
		if (capdVoucherItemList == null) {
			capdVoucherItemList = new ArrayList<CapdVoucherItem>();
		}
		return capdVoucherItemList;
	}

	public void setCapdVoucherItemList(List<CapdVoucherItem> capdVoucherItemList) {
		this.capdVoucherItemList = capdVoucherItemList;
	}
	
	public void addCapdVoucherItem(CapdVoucherItem capdVoucherItem) {
		capdVoucherItem.setCapdVoucher(this);
		getCapdVoucherItemList().add(capdVoucherItem);
	}
	
	public void clearCapdVoucherItemList() {
		capdVoucherItemList = new ArrayList<CapdVoucherItem>();
	}

	public DispOrder getDispOrder() {
		return dispOrder;
	}

	public void setDispOrder(DispOrder dispOrder) {
		this.dispOrder = dispOrder;
	}

	public void setVoucherNum(String voucherNum) {
		this.voucherNum = voucherNum;
	}

	public String getVoucherNum() {
		return voucherNum;
	}

	public void setManualVoucherNum(String manualVoucherNum) {
		this.manualVoucherNum = manualVoucherNum;
	}

	public String getManualVoucherNum() {
		return manualVoucherNum;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	public String getSupplierCode() {
		return supplierCode;
	}
	
	public Boolean getReprint() {
		return reprint;
	}

	public void setReprint(Boolean reprint) {
		this.reprint = reprint;
	}

	public DispOrderAdminStatus getAdminStatus() {
		return adminStatus;
	}

	public void setAdminStatus(DispOrderAdminStatus adminStatus) {
		this.adminStatus = adminStatus;
	}	
	
	@Transient
	private transient boolean childLoaded = false;
	
	public void loadChild() {
		loadChild(true);
	}
	
	public void loadChild(boolean includeCapdSplit) {
		
		if (getId() == null) return;

		if (!childLoaded) {			
			childLoaded = true;
			
			if (this.getDispOrder() != null) {
				this.getDispOrder().loadChild();	
				if (includeCapdSplit) {
					this.getDispOrder().getPharmOrder().loadCapdSplit();				
				}
				this.getDispOrder().addCapdVoucher(this);
			}
			
			for (CapdVoucherItem item : this.getCapdVoucherItemList()) {
				item.getCapdVoucher();
				if (item.getDispOrderItem() != null) {
					item.getDispOrderItem().addCapdVoucherItem(item);
				}
			}
		}
	}
	
	public void clearId() {		
		this.setId(null);				
		for (CapdVoucherItem item : this.getCapdVoucherItemList()) {
			item.setId(null);
		}
	}	
	
	public void replace(DispOrder dispOrder) {
		
		for (CapdVoucherItem cvi : this.getCapdVoucherItemList()) {
			DispOrderItem doi = dispOrder.getDispOrderItemByPoItemNum(
					cvi.getDispOrderItem().getPharmOrderItem().getItemNum());
			
			if (doi != null) {
				doi.addCapdVoucherItem(cvi);
			}
		}		
		
		dispOrder.addCapdVoucher(this);
	}
	
	@Transient
	private transient DispOrderItem _dispOrderItem;
	
	public void disconnect() {
		
		if (this.getDispOrder() != null) 
		{			
			this.getDispOrder().getCapdVoucherList().remove(this);
			this.setDispOrder(null);
		}

		for (CapdVoucherItem cvi : this.getCapdVoucherItemList()) 
		{
			if (cvi.getDispOrderItem() != null) {
				_dispOrderItem = cvi.getDispOrderItem();
				cvi.getDispOrderItem().getCapdVoucherItemList().remove(cvi);
				cvi.setDispOrderItem(null);
			}
		}
	}
	
	public void connect(DispOrder dispOrder) {
		
		for (CapdVoucherItem cvi : this.getCapdVoucherItemList()) 
		{
			DispOrderItem doi = dispOrder.getDispOrderItemByPoItemNum(
					_dispOrderItem.getPharmOrderItem().getItemNum());
			_dispOrderItem = null;
			
			if (doi != null) {
				doi.addCapdVoucherItem(cvi);
			}
		}		
		
		dispOrder.addCapdVoucher(this);
	}

}
