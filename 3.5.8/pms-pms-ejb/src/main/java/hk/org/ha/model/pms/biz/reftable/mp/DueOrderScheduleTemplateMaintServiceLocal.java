package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.model.pms.persistence.medprofile.DeliverySchedule;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryScheduleItem;

import java.util.List;

import javax.ejb.Local;

@Local
public interface DueOrderScheduleTemplateMaintServiceLocal {
	
	void retrieveWardAndWardGroupList();
	
	List<DeliverySchedule> retrieveDeliveryScheduleList();
	
	List<DeliveryScheduleItem> retrieveDeliveryScheduleItemList(Long deliveryScheduleId);
	
	boolean checkDeliveryScheduleNameExists(String deliveryScheduleName);
	
	List<DeliveryScheduleItem> copyDeliverySchedule(Long deliveryScheduleItemId);
	
	List<DeliverySchedule> updateDeliverySchedule(DeliverySchedule updateDeliverySchedule, List<DeliveryScheduleItem> updateDeliveryScheduleList);
	
	List<DeliverySchedule> deleteDeliverySchedule(Long delDeliveryScheduleId);
	
	void destroy();

}
