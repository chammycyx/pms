package hk.org.ha.model.pms.vo.label;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SpecialLabel")
@ExternalizedBean(type=DefaultExternalizer.class)
public class SpecialLabel implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@XmlElement(required = true)
	private String labelNum;
 
	@XmlElement(name="SpecialLabelInstruction", required = true)	
	private List<SpecialLabelInstruction> specialLabelInstructionList;
	
	private String description;
	
	@Transient
	private String line1;
	
	@Transient
	private String line2;
	
	@Transient
	private String line3;
	
	@Transient
	private String line4;
	
	@Transient
	private String line5;
	
	@Transient
	private String line6;
	
	@Transient
	private String line7;
	
	@Transient
	private Integer copies;
	
	@Transient
	private boolean printHeaderFooterFlag;
	
	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public String getLine3() {
		return line3;
	}

	public void setLine3(String line3) {
		this.line3 = line3;
	}

	public String getLine4() {
		return line4;
	}

	public void setLine4(String line4) {
		this.line4 = line4;
	}

	public String getLine5() {
		return line5;
	}

	public void setLine5(String line5) {
		this.line5 = line5;
	}

	public String getLine6() {
		return line6;
	}

	public void setLine6(String line6) {
		this.line6 = line6;
	}

	public String getLine7() {
		return line7;
	}

	public void setLine7(String line7) {
		this.line7 = line7;
	}

	public String getLabelNum() {
		return labelNum;
	}

	public void setLabelNum(String labelNum) {
		this.labelNum = labelNum;
	}

	public Integer getCopies() {
		return copies;
	}

	public void setCopies(Integer copies) {
		this.copies = copies;
	}

	public void setSpecialLabelInstructionList(
			List<SpecialLabelInstruction> specialLabelInstructionList) {
		this.specialLabelInstructionList = specialLabelInstructionList;
	}

	public List<SpecialLabelInstruction> getSpecialLabelInstructionList() {
		return specialLabelInstructionList;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine1() {
		return line1;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setPrintHeaderFooterFlag(boolean printHeaderFooterFlag) {
		this.printHeaderFooterFlag = printHeaderFooterFlag;
	}

	public boolean isPrintHeaderFooterFlag() {
		return printHeaderFooterFlag;
	}


}