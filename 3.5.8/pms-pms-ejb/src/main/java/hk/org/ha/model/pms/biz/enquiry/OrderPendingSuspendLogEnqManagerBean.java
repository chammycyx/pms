package hk.org.ha.model.pms.biz.enquiry;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileActionLog;
import hk.org.ha.model.pms.udt.medprofile.ActionLogType;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.joda.time.DateTime;

@AutoCreate
@Stateless
@Name("orderPendingSuspendLogEnqManager")
@MeasureCalls
public class OrderPendingSuspendLogEnqManagerBean implements OrderPendingSuspendLogEnqManagerLocal {

	@PersistenceContext
	private EntityManager em;
		
	@In
	private Workstore workstore;

	@SuppressWarnings("unchecked")
	public List<MedProfileActionLog> retrieveOrderPendingSuspendLogEnqListByCriteria(Date fromDate, Date toDate, String caseNum, String filter){
		
		DateTime dt = new DateTime(toDate);
		Date exactDateTo = dt.plusDays(1).toDate();
		
		StringBuilder sql = new StringBuilder(
				"select o from MedProfileActionLog o" + // 20130223 index check : MedProfileActionLog.medProfileMoItem : FK_MED_PROFILE_ACTION_LOG_01
				" where o.medProfileMoItem.medProfileItem.medProfile.workstore.hospital = :hospital");

		if (fromDate!=null && toDate!=null) {
			sql.append(" and o.createDate between :fromDate and :toDate");
		}
			
		if (StringUtils.isNotBlank(caseNum)) {
			sql.append(" and o.medProfileMoItem.medProfileItem.medProfile.medCase.caseNum = :caseNum");
		}
		
		if ( "S".equals(filter) || "P".equals(filter) ) {
			sql.append(" and o.type in :actionLogTypeList");
		}
		
		Query query = em.createQuery(sql.toString());
		
		query.setParameter("hospital", workstore.getHospital());

		if (fromDate!=null && toDate!=null) {
			query.setParameter("fromDate", fromDate, TemporalType.DATE);
			query.setParameter("toDate", exactDateTo, TemporalType.DATE);
		}
			
		if (StringUtils.isNotBlank(caseNum)) {
			query.setParameter("caseNum", caseNum);
		}
		
		if (filter!=null) {
			if ("S".equals(filter)){
				query.setParameter("actionLogTypeList", ActionLogType.suspend_Status);
			} else if ("P".equals(filter)){
				query.setParameter("actionLogTypeList", ActionLogType.pending_Status);
			}
		}
		
		List<MedProfileActionLog> medProfileActionLogList = (List<MedProfileActionLog>)query.getResultList();

		return medProfileActionLogList;

	}
		

	@Override
	public void destroy() {
		
	}

}
