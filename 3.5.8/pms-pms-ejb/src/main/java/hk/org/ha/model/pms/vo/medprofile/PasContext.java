package hk.org.ha.model.pms.vo.medprofile;

import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.dms.vo.PmsSpecialtyMap;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.udt.disp.MedCasePasPatInd;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatus;

import java.util.Date;

public class PasContext extends PasParams {
	
	private static final long serialVersionUID = 1L;	

	private boolean patientInfoChanged;
	private boolean wardInfoChanged;
	private Patient patient;
	private MedCase medCase;
	private BodyInfo bodyInfo;
	private PmsSpecialtyMap pmsSpecialtyMap;
	private boolean errorOnRetrieveItemCutOffDate;
	private Date itemCutOffDate;

	public boolean isPatientInfoChanged() {
		return patientInfoChanged;
	}

	public boolean isWardInfoChanged() {
		return wardInfoChanged;
	}
	
	public Date getDischargeDate() {
		return medCase == null ? null : medCase.getDischargeDate();
	}

	@Override
	public String getPatKey() {
		return patient == null ? super.getPatKey() : patient.getPatKey();
	}
	
	@Override
	public String getHkid() {
		return patient == null ? super.getHkid() : patient.getHkid();
	}
	
	@Override
	public String getCaseNum() {
		return medCase == null ? super.getCaseNum() : medCase.getCaseNum();
	}

	@Override	
	public String getPasSpecCode() {
		return medCase == null ? super.getPasSpecCode() : medCase.getPasSpecCode();
	}
	
	@Override	
	public String getPasSubSpecCode() {
		return medCase == null ? super.getPasSubSpecCode() : medCase.getPasSubSpecCode();
	}
	
	@Override	
	public String getPasBedNum() {
		return medCase == null ? super.getPasBedNum() : medCase.getPasBedNum();
	}

	@Override	
	public String getPasWardCode() {
		return medCase == null ? super.getPasWardCode() : medCase.getPasWardCode();
	}

	@Override
	public Boolean getPrivateFlag() {
		return medCase == null ? Boolean.TRUE.equals(super.getPrivateFlag()) : medCase.getPasPatInd() == MedCasePasPatInd.Private;
	}
	
	@Override
	public String getSpecCode() {
		return pmsSpecialtyMap == null ? super.getSpecCode() : pmsSpecialtyMap.getPhsSpecialty();
	}

	@Override
	public String getWardCode() {
		return pmsSpecialtyMap == null ? super.getWardCode() : pmsSpecialtyMap.getPhsWard();
	}
	
	public Patient getPatient() {
		return patient;
	}
	
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	
	public MedCase getMedCase() {
		return medCase;
	}

	public void setMedCase(MedCase medCase) {
		this.medCase = medCase;
	}

	public BodyInfo getBodyInfo() {
		return bodyInfo;
	}
	
	public void setBodyInfo(BodyInfo bodyInfo) {
		this.bodyInfo = bodyInfo;
	}
	
	public PmsSpecialtyMap getPmsSpecialtyMap() {
		return pmsSpecialtyMap;
	}
	
	public void setPmsSpecialtyMap(PmsSpecialtyMap pmsSpecialtyMap) {
		this.pmsSpecialtyMap = pmsSpecialtyMap;
	}
	
	public boolean isErrorOnRetrieveItemCutOffDate() {
		return errorOnRetrieveItemCutOffDate;
	}

	public void setErrorOnRetrieveItemCutOffDate(
			boolean errorOnRetrieveItemCutOffDate) {
		this.errorOnRetrieveItemCutOffDate = errorOnRetrieveItemCutOffDate;
	}

	public Date getItemCutOffDate() {
		return itemCutOffDate;
	}

	public void setItemCutOffDate(Date itemCutOffDate) {
		this.itemCutOffDate = itemCutOffDate;
	}

	public void markChanges(MedProfile medProfile) {
		
		patientInfoChanged = false;
		wardInfoChanged = false;
		
		if (medProfile.getMedCase() != null && medCase != null) {
			patientInfoChanged |= medProfile.isDischarge() != (medCase.getDischargeDate() != null);
			wardInfoChanged |= MedProfileUtils.compare(
					medProfile.getMedCase().getPasSpecCode(), medCase.getPasSpecCode()) != 0;
			wardInfoChanged |= MedProfileUtils.compare(
					medProfile.getMedCase().getPasWardCode(), medCase.getPasWardCode()) != 0;
			wardInfoChanged |= MedProfileUtils.compare(
					medProfile.getMedCase().getPasBedNum(), medCase.getPasBedNum()) != 0;
		}
	}
}
