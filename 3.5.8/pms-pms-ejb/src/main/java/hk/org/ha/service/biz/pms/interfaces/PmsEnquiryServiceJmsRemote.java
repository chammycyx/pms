package hk.org.ha.service.biz.pms.interfaces;

import hk.org.ha.model.pms.corp.vo.moe.DispenseStatusInfo;

import java.util.List;

public interface PmsEnquiryServiceJmsRemote {

	boolean allowDeleteMpItem(String clusterCode, String orderNum, Integer itemNum);

	List<DispenseStatusInfo> retrieveDispenseStatusInfoList(String clusterCode, String orderNum, Integer itemNum);
}
