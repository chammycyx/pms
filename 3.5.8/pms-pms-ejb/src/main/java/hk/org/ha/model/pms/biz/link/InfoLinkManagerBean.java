package hk.org.ha.model.pms.biz.link;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import hk.org.ha.model.pms.persistence.InfoLink;

@AutoCreate
@Stateless
@Name("infoLinkManager")
@MeasureCalls
public class InfoLinkManagerBean implements InfoLinkManagerLocal {
	
	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	public List<InfoLink> retrieveInfoLink() {
		return em.createQuery(
				"select o from InfoLink o" +  // 20150206 index check : none
				" order by o.sortSeq")
				.getResultList();
	}
}
