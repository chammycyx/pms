package hk.org.ha.model.pms.biz.vetting.mp;

import static hk.org.ha.model.pms.prop.Prop.REPORT_MPORDERPENDINGFORM_REMARK;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.biz.report.MpTrxRptBuilderLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.rx.RxItem;
import hk.org.ha.model.pms.vo.vetting.mp.OrderPendingRpt;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

@AutoCreate
@Stateless
@Name("orderPendingRptManager")
@MeasureCalls
public class OrderPendingRptManagerBean implements OrderPendingRptManagerLocal {

	@In
	private Workstore workstore;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private MpTrxRptBuilderLocal mpTrxRptBuilder;
	
	private static final String DECIMAL_FORMAT = "###.##";
	
	private static final String PRESCRIBE_DATE_PATTERN = "dd-MMM-yyyy HH:mm";
	
	private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern(PRESCRIBE_DATE_PATTERN);

	@Override
	public List<RenderAndPrintJob> printPendingOrderRpt(MedProfile medProfile) {

		DecimalFormat df = new DecimalFormat(DECIMAL_FORMAT);
		
		List<RenderAndPrintJob> renderAndPrintJobList = new ArrayList<RenderAndPrintJob>();
		
		if( medProfile != null && medProfile.getMedProfileItemList() != null ){
    		for( MedProfileItem medProfileItem : medProfile.getMedProfileItemList() ){
    			if( medProfileItem.getMedProfileMoItem() != null && medProfileItem.getMedProfileMoItem().getPrintOrderPendingForm() ){
    				
					OrderPendingRpt orderPendingRpt = new OrderPendingRpt();
					
					if( medProfile.getMedCase() != null ){
						orderPendingRpt.setCaseNum(medProfile.getMedCase().getCaseNum());
						orderPendingRpt.setBedNum(medProfile.getMedCase().getPasBedNum());
					}
					if( medProfile.getPatient() != null ){
						orderPendingRpt.setName(medProfile.getPatient().getName());
						orderPendingRpt.setNameChi(medProfile.getPatient().getNameChi());
						if( medProfile.getPatient().getSex() != null ){
							orderPendingRpt.setSex(medProfile.getPatient().getSex().getDataValue());	
						}
						if( medProfile.getPatient().getAge() != null ){
							orderPendingRpt.setAge(medProfile.getPatient().getAge()+medProfile.getPatient().getDateUnit().substring(0, 1));
						}
					}
					if( medProfile.getBodyWeight() != null ){
						orderPendingRpt.setWeight(df.format(medProfile.getBodyWeight()).toString());
					}
					orderPendingRpt.setSpecialty(medProfile.getSpecCode());
					orderPendingRpt.setWard(medProfile.getWardCode());
						
					RxItem rxItem = medProfileItem.getMedProfileMoItem().getRxItem();
					if( rxItem != null ){
						rxItem.buildTlf();
						orderPendingRpt.setDrugOrderTlf(mpTrxRptBuilder.convertDrugOrderTlf(rxItem.getFormattedDrugOrderTlf()));
					}
					orderPendingRpt.setPrescribeDate( DATE_FORMATTER.print(new DateTime(medProfileItem.getMedProfileMoItem().getOrderDate() )) );
					StringBuilder sb = new StringBuilder();
					sb.append(medProfileItem.getMedProfileMoItem().getDoctorName());
					if( StringUtils.isNotBlank( medProfileItem.getMedProfileMoItem().getDoctorRankCode() )){
						sb.append(" (");
						sb.append(medProfileItem.getMedProfileMoItem().getDoctorRankCode());
						sb.append(")");
					}
					orderPendingRpt.setPrescribeUser(sb.toString());
					
					if( StringUtils.isNotBlank(medProfileItem.getMedProfileMoItem().getPendingUserName()) ){
						orderPendingRpt.setPendUser(medProfileItem.getMedProfileMoItem().getPendingUserName());
					}else{
						orderPendingRpt.setPendUser(medProfileItem.getMedProfileMoItem().getPendingUser());
					}
					orderPendingRpt.setPendReason(medProfileItem.getMedProfileMoItem().getPendingDesc());
					orderPendingRpt.setPendRemark(medProfileItem.getMedProfileMoItem().getPendingSupplDesc());
					
					orderPendingRpt.setRemark(REPORT_MPORDERPENDINGFORM_REMARK.get());
					
					Map<String, Object> parameters = new HashMap<String, Object>();
					parameters.put("hospCode", workstore.getHospCode());
					parameters.put("workstoreCode", workstore.getWorkstoreCode());
					
					renderAndPrintJobList.add(new RenderAndPrintJob(
							"OrderPending", 
							new PrintOption(), 
							printerSelector.retrievePrinterSelect(PrintDocType.Report), 
							parameters, 
							Arrays.asList(orderPendingRpt)));
    			}
    		}
		}
		
		return renderAndPrintJobList;
		
	}

}
