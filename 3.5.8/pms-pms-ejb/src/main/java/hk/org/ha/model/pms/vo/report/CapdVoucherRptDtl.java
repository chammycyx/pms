package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@SuppressWarnings("unchecked")
@ExternalizedBean(type = DefaultExternalizer.class)
public class CapdVoucherRptDtl implements Comparable, Serializable {
		
	private static final long serialVersionUID = 1L;

	private Date dispDate;
	
	private String caseNum;
	
	private String patName;
	
	private String hkid;
	
	private String patNameChi;
	
	private List<CapdVoucherDtl> capdVoucherDtlList;

	public CapdVoucherRptDtl(Date dispDate, String caseNum, String patName,
			String hkid) {
		super();
		this.dispDate = dispDate;
		this.caseNum = caseNum;
		this.patName = patName;
		this.hkid = hkid;
	}

	public Date getDispDate() {
		return dispDate;
	}

	public void setDispDate(Date dispDate) {
		this.dispDate = dispDate;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public void setCapdVoucherDtlList(List<CapdVoucherDtl> capdVoucherDtlList) {
		this.capdVoucherDtlList = capdVoucherDtlList;
	}

	public List<CapdVoucherDtl> getCapdVoucherDtlList() {
		return capdVoucherDtlList;
	}

	public void addCapdVoucherDtl(CapdVoucherDtl dtl) {
		if (capdVoucherDtlList == null) {
			capdVoucherDtlList = new ArrayList<CapdVoucherDtl>();
		}
		capdVoucherDtlList.add(dtl);
	}
	
	public int hashCode() {
		return new HashCodeBuilder()
				.append(dispDate)
				.append(caseNum)
				.append(patName)
				.append(hkid)
				.toHashCode();
	}

	public boolean equals(Object obj) {
		CapdVoucherRptDtl o = (CapdVoucherRptDtl) obj;
		return new EqualsBuilder()
					.append(dispDate, o.dispDate)
					.append(caseNum, o.caseNum)
					.append(patName, o.patName)
					.append(hkid, o.hkid)
					.isEquals();
	}

	public int compareTo(Object obj) {
		CapdVoucherRptDtl o = (CapdVoucherRptDtl) obj;
		return new CompareToBuilder()
				.append(dispDate, o.dispDate)
				.append(caseNum, o.caseNum)
				.append(patName, o.patName)
				.append(hkid, o.hkid)
				.toComparison();
	}

	public void setPatNameChi(String patNameChi) {
		this.patNameChi = patNameChi;
	}

	public String getPatNameChi() {
		return patNameChi;
	}
}
