package hk.org.ha.model.pms.vo.cddh;

import hk.org.ha.model.pms.vo.rx.Freq;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class PatientCddhDose implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@XmlElement
	private BigDecimal dosage;

	@XmlElement
	private String dosageUnit;
	
	@XmlElement(required = true)
	private Boolean prn;
	
	@XmlElement(required = true)
	private String siteCode;

	@XmlElement
	private String supplSiteDesc;	
	
	@XmlElement(name="DailyFreq")
	private Freq dailyFreq;
	
	@XmlElement(name="SupplFreq")
	private Freq supplFreq;	
	
	public PatientCddhDose() {
	}

	public BigDecimal getDosage() {
		return dosage;
	}

	public void setDosage(BigDecimal dosage) {
		this.dosage = dosage;
	}

	public String getDosageUnit() {
		return dosageUnit;
	}

	public void setDosageUnit(String dosageUnit) {
		this.dosageUnit = dosageUnit;
	}

	public Boolean getPrn() {
		return prn;
	}

	public void setPrn(Boolean prn) {
		this.prn = prn;
	}

	public String getSiteCode() {
		return siteCode;
	}

	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

	public String getSupplSiteDesc() {
		return supplSiteDesc;
	}

	public void setSupplSiteDesc(String supplSiteDesc) {
		this.supplSiteDesc = supplSiteDesc;
	}

	public Freq getDailyFreq() {
		return dailyFreq;
	}

	public void setDailyFreq(Freq dailyFreq) {
		this.dailyFreq = dailyFreq;
	}

	public Freq getSupplFreq() {
		return supplFreq;
	}

	public void setSupplFreq(Freq supplFreq) {
		this.supplFreq = supplFreq;
	}
}
