package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.fmk.pms.util.StringArray;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.dh.DmDhDrugVerMapping;
import hk.org.ha.model.pms.dms.udt.conversion.DhOrderMapperLookupType;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.model.pms.vo.rx.CapdRxDrug;
import hk.org.ha.model.pms.vo.rx.DhRxDrug;
import hk.org.ha.model.pms.vo.rx.IvAdditive;
import hk.org.ha.model.pms.vo.rx.IvRxDrug;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("workstoreDrugListManager")
@MeasureCalls
public class WorkstoreDrugListManagerBean implements WorkstoreDrugListManagerLocal {

	@Logger
	private Log logger;

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private Workstore workstore;

	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;
	
	private static final String JAXB_CONTEXT_RX = "hk.org.ha.model.pms.vo.rx";
	
	private WorkstoreDrugComparator workstoreDrugComparator = new WorkstoreDrugComparator();
			
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private List<WorkstoreDrug> workstoreDrugList;
	
	@Out(scope=ScopeType.SESSION, required=false)
	private DmDrug dhDmDrug;
	
	@Out(scope=ScopeType.SESSION, required=false)
	private String latestMappingStatus;
	
	private List<WorkstoreDrug> getWorkstoreDrugListWithExtraInfo(List<WorkstoreDrug> drugList, boolean ignoreDrugScope) {
		List<WorkstoreDrug> resultList = new ArrayList<WorkstoreDrug>();
		for ( WorkstoreDrug drug : drugList ) {
			drug.getDmDrug().getFullDrugDesc();
			drug.getDmDrug().getVolumeText();
			if ( drug.getMsWorkstoreDrug() != null ) {
				if ( !ignoreDrugScope && Integer.valueOf(200).compareTo(drug.getMsWorkstoreDrug().getDrugScope()) > 0 ) {
					continue;
				}
				resultList.add(drug);
			}
		}
		Collections.sort(resultList,workstoreDrugComparator);
		return resultList;
	}
	
	public void retrieveWorkstoreDrugListByDisplayName(String displayName) {		
		if ( logger.isDebugEnabled() ) {
			logger.debug("retrieveDrugListByDisplayName - displayName | #0", displayName);					
		}
		workstoreDrugList = workstoreDrugCacher.getWorkstoreDrugListByDisplayName(workstore, displayName);
		workstoreDrugList = getWorkstoreDrugListWithExtraInfo(workstoreDrugList, false);
		if ( workstoreDrugList != null && logger.isDebugEnabled() ) {
			logger.debug("drugList size #0", workstoreDrugList.size());
		}
	}
	
	public void retrieveWorkstoreDrugListByDrugKey(Integer drugKey, boolean ignoreDrugScope) {
		if ( logger.isDebugEnabled() ) {
			logger.debug("retrieveDrugListByDrugKey - drugKey | #0", drugKey);
		}		
		workstoreDrugList = getWorkstoreDrugListWithExtraInfo(
				workstoreDrugCacher.getWorkstoreDrugListByDrugKey(workstore, drugKey), 
				ignoreDrugScope);
		if ( workstoreDrugList != null && logger.isDebugEnabled() ) {
			logger.debug("drugList size #0", workstoreDrugList.size());
		}
	}	
	
	public void retrieveWorkstoreDrugList(String prefixItemCode, boolean showNonSuspendItemOnly, boolean fdn){
		
		logger.debug("retrieveDrugList - prefixItemCode=#0, showNonSuspendItemOnly=#1, fdn=#2", prefixItemCode, showNonSuspendItemOnly, fdn);

		if ( fdn ) {
			workstoreDrugList = workstoreDrugCacher.getWorkstoreDrugList(workstore, prefixItemCode, null, Boolean.TRUE);
		} else {
				if (showNonSuspendItemOnly) {
				workstoreDrugList = workstoreDrugCacher.getWorkstoreDrugList(workstore, prefixItemCode, Boolean.FALSE, null);
			} else {
				workstoreDrugList = workstoreDrugCacher.getWorkstoreDrugList(workstore, prefixItemCode);
			}
		}
		
		if ( workstoreDrugList == null ) {
			logger.debug("drugList is null");
			return;
		}
		
		workstoreDrugList = getWorkstoreDrugListWithExtraInfo(workstoreDrugList, false);
		
		if ( logger.isDebugEnabled() ) {
			logger.debug("drugList size:|#0", workstoreDrugList.size());
		}
	}
	
	private List<WorkstoreDrug> getDefaultDrugListByIvRxDrug(IvRxDrug ivRxDrug) {
		List<WorkstoreDrug> resultDrugList = new ArrayList<WorkstoreDrug>();
		List<WorkstoreDrug> list;				
		Map<StringArray, Boolean> displayNameFormSaltFmStatusMap = new HashMap<StringArray,Boolean>();
		
		for ( IvAdditive ivAdditive : ivRxDrug.getIvAdditiveList() ) {
			
			StringArray key = new StringArray(ivAdditive.getDisplayName(), 
					ivAdditive.getFormCode(), 
					ivAdditive.getSaltProperty(), 
					ivAdditive.getFmStatus());
			if ( displayNameFormSaltFmStatusMap.get(key) != null ) {
				continue;
			}
			displayNameFormSaltFmStatusMap.put(key, Boolean.TRUE);
			list = workstoreDrugCacher.getWorkstoreDrugListByDisplayNameFormSaltFmStatus(
					workstore,
					ivAdditive.getDisplayName(), 
					ivAdditive.getFormCode(), 
					ivAdditive.getSaltProperty(), 
					ivAdditive.getFmStatus());				
			resultDrugList.addAll( list );
		}
		workstoreDrugList = getWorkstoreDrugListWithExtraInfo(resultDrugList, false);
		return workstoreDrugList;
	}
	
	private List<WorkstoreDrug> getExtendedDrugListByIvRxDrug(IvRxDrug ivRxDrug) {
		List<WorkstoreDrug> resultDrugList = new ArrayList<WorkstoreDrug>();
		List<WorkstoreDrug> list;		
		Map<String, Boolean> displayNameMap = new HashMap<String, Boolean>();
		
		for ( IvAdditive ivAdditive : ivRxDrug.getIvAdditiveList() ) {
			
			String key = ivAdditive.getDisplayName();
			if ( displayNameMap.get(key) != null ) {
				continue;
			}
			displayNameMap.put(key, Boolean.TRUE);
			list = workstoreDrugCacher.getWorkstoreDrugListByDisplayName(workstore, ivAdditive.getDisplayName());
			resultDrugList.addAll( list );
		}
		workstoreDrugList = getWorkstoreDrugListWithExtraInfo(resultDrugList, false);
		return workstoreDrugList;
	}
	
	public List<WorkstoreDrug> getWorkstoreDrugListByRxItem( RxItem rxItem, Boolean displayNameOnly ) {				
		
		if (rxItem.isIvRxDrug()) {			
			
			if ( displayNameOnly ) {
				return getExtendedDrugListByIvRxDrug((IvRxDrug)rxItem);
			} else {
				return getDefaultDrugListByIvRxDrug((IvRxDrug)rxItem);
			}			
		} else if (rxItem.isRxDrug()) {
			
			RxDrug rxDrug = (RxDrug)rxItem;
			List<WorkstoreDrug> resultDrugList;
			if ( displayNameOnly ) {
				resultDrugList = workstoreDrugCacher.getWorkstoreDrugListByDisplayName(workstore, rxDrug.getDisplayName());
			} else {
				resultDrugList = workstoreDrugCacher.getWorkstoreDrugListByDisplayNameFormSaltFmStatus(
						workstore,
						rxDrug.getDisplayName(), 
						rxDrug.getFormCode(), 
						rxDrug.getSaltProperty(), 
						rxDrug.getFmStatus());
			}
			workstoreDrugList = getWorkstoreDrugListWithExtraInfo(resultDrugList, false);
			return workstoreDrugList;
		} else if (rxItem.isCapdRxDrug()) {
			List<WorkstoreDrug> resultDrugList;
			CapdRxDrug capdRxDrug = (CapdRxDrug)rxItem;
			DmDrug dmDrug = DmDrugCacher.instance().getDmDrug(capdRxDrug.getCapdItemList().get(0).getItemCode());
			if (displayNameOnly) {
				resultDrugList = workstoreDrugCacher.getWorkstoreDrugListByDisplayName(workstore, dmDrug.getDmDrugProperty().getDisplayname());
			} else {
				resultDrugList = workstoreDrugCacher.getWorkstoreDrugListByDisplayNameFormSaltFmStatus(
						workstore,
						dmDrug.getDmDrugProperty().getDisplayname(), 
						dmDrug.getDmDrugProperty().getFormCode(), 
						dmDrug.getDmDrugProperty().getSaltProperty(), 
						FmStatus.GeneralDrug.getDataValue());
			}
			workstoreDrugList = getWorkstoreDrugListWithExtraInfo(resultDrugList, false);
			return workstoreDrugList;
		} else {
			throw new RuntimeException("Unexpected Error!");
		}
	}
	
	public void retrieveWorkstoreDrugListByRxItem( RxItem rxItem, Boolean displayNameOnly ) {
		workstoreDrugList = getWorkstoreDrugListByRxItem(rxItem, displayNameOnly);
	}

	private List<WorkstoreDrug> filterByDs(DmDrug dmDrug) {
		List<WorkstoreDrug> drugList = new ArrayList<WorkstoreDrug>(workstoreDrugCacher.getWorkstoreDrugListByDisplayName(workstore, dmDrug.getDmDrugProperty().getDisplayname()));
		Iterator<WorkstoreDrug> iterator = drugList.iterator();
		WorkstoreDrug drug;
		while (iterator.hasNext()) {
			drug = iterator.next();
			//Salt
			if (!StringUtils.equals(drug.getDmDrug().getDmDrugProperty().getSaltProperty(), dmDrug.getDmDrugProperty().getSaltProperty())) {
				iterator.remove();
				continue;
			}										
		}
		return drugList;
	}
	
	private List<WorkstoreDrug> filterByDsfStrength(DmDrug dmDrug) {
		List<WorkstoreDrug> drugList = new ArrayList<WorkstoreDrug>(workstoreDrugCacher.getWorkstoreDrugListByDrugKey(workstore, dmDrug.getDrugKey()));
		Iterator<WorkstoreDrug> iterator = drugList.iterator();
		WorkstoreDrug drug;
		while (iterator.hasNext()) {
			drug = iterator.next();
			//Strength
			if (!StringUtils.equals(drug.getDmDrug().getStrength(), dmDrug.getStrength())) {
				iterator.remove();
				continue;
			}	
		}
		return drugList;
	}
	
	public void retrieveWorkstoreDrugListByDhRxDrug(DhRxDrug dhRxDrug, String itemCodePrefix) {
		Pair<List<WorkstoreDrug>, DmDrug> mappingResult = getWorkstoreDrugListDmDrugByDhRxDrug(dhRxDrug);
		List<WorkstoreDrug> dhDrugList = mappingResult.getFirst();
		if ( dhDrugList == null ) {
			workstoreDrugList = addSiteAndExtraInfo(workstoreDrugCacher.getWorkstoreDrugList(workstore, itemCodePrefix), null, null);
		} else {
			workstoreDrugList = dhDrugList;
		}
		dhDmDrug = mappingResult.getSecond();
	}

	public List<WorkstoreDrug> addSiteAndExtraInfo(List<WorkstoreDrug> drugList, String siteCode, String supplSiteDesc) {
		for (WorkstoreDrug workstoreDrug : drugList) {
			workstoreDrug.setSiteCode(siteCode);
			workstoreDrug.setSupplSiteDesc(supplSiteDesc);
		}		
		return getWorkstoreDrugListWithExtraInfo(drugList, false);
	}
	
	public Pair<List<WorkstoreDrug>, DmDrug> getWorkstoreDrugListDmDrugByDhRxDrug(DhRxDrug dhRxDrug) {				
		
		Pair<DhOrderMapperLookupType, DmDhDrugVerMapping> pair = dmsPmsServiceProxy.retrieveDhHaDrugMappingInfo(JaxbWrapper.instance(JAXB_CONTEXT_RX).marshall(dhRxDrug));
		
		latestMappingStatus = null;
		
		if (pair.getFirst() == DhOrderMapperLookupType.Nil) {
			latestMappingStatus = "N";
			return new Pair<List<WorkstoreDrug>, DmDrug>(null, null);
		}
		
		DmDrug dmDrug = null;
		if (pair.getSecond() != null) {
			dmDrug = DmDrugCacher.instance().getDmDrug(pair.getSecond().getItemCode());
		}
		if (dmDrug == null) {
			logger.error("dmDrug[#0] is null", pair.getSecond().getItemCode());
			return new Pair<List<WorkstoreDrug>, DmDrug>(new ArrayList<WorkstoreDrug>(), null);
		}

		latestMappingStatus = pair.getSecond().getMapType();
		switch (pair.getFirst()) {
			case Dsf:
				return new Pair<List<WorkstoreDrug>, DmDrug>(addSiteAndExtraInfo( workstoreDrugCacher.getWorkstoreDrugListByDrugKey(workstore, dmDrug.getDrugKey()), null, null ), dmDrug);
			case Ds:
				return new Pair<List<WorkstoreDrug>, DmDrug>(addSiteAndExtraInfo( filterByDs(dmDrug), null, null), dmDrug);
			case DsfStrength:
				return new Pair<List<WorkstoreDrug>, DmDrug>(addSiteAndExtraInfo( filterByDsfStrength(dmDrug), null, null), dmDrug);
			case DsfAndSite:
				return new Pair<List<WorkstoreDrug>, DmDrug>(addSiteAndExtraInfo( workstoreDrugCacher.getWorkstoreDrugListByDrugKey(workstore, dmDrug.getDrugKey()), pair.getSecond().getSiteCode(), pair.getSecond().getSupplSiteDesc() ), dmDrug);
			case DsfStrengthAndSite:
				return new Pair<List<WorkstoreDrug>, DmDrug>(addSiteAndExtraInfo( filterByDsfStrength(dmDrug), pair.getSecond().getSiteCode(), pair.getSecond().getSupplSiteDesc() ), dmDrug);
			default:
				return new Pair<List<WorkstoreDrug>, DmDrug>(null, dmDrug);
		}
	}
	
	public List<WorkstoreDrug> getWorkstoreDrugListByDrugKey(Integer drugKey, boolean ignoreDrugScope) {
		retrieveWorkstoreDrugListByDrugKey(drugKey, ignoreDrugScope);
		return workstoreDrugList;
	}
}
