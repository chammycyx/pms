package hk.org.ha.model.pms.biz.charging;

import static hk.org.ha.model.pms.prop.Prop.*;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.vo.pms.DrugChargeInfo;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.disp.PharmOrderPatType;
import hk.org.ha.model.pms.vo.charging.DrugChargeRuleCriteria;
import hk.org.ha.model.pms.vo.charging.OverrideChargeAmountRuleCriteria;

import javax.ejb.Remove;
import org.drools.RuleBase;
import org.drools.StatelessSession;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Scope(ScopeType.APPLICATION)
@Name("chargeCalculation")
@Restrict("#{identity.loggedIn}")
@MeasureCalls
public class ChargeCalculationBean implements ChargeCalculationInf {
	
	@In(create=true)
	private RuleBase privateHandleRuleBase;
	
	@In(create=true)
	private RuleBase sfiItemChargeRuleBase;
	
	@In(create=true)
	private RuleBase overrideChargeAmtRuleBase;
	
	private transient StatelessSession privateHandleWorkingMemory = null;
	
	private transient StatelessSession sfiItemChargeWorkingMemory = null;
	
	private transient StatelessSession overrideChargeAmtWorkingMemory = null;
		
	@Create
	public void create() {
		if ( privateHandleRuleBase != null) {
			privateHandleWorkingMemory = privateHandleRuleBase.newStatelessSession();
		}
		
		if ( sfiItemChargeRuleBase != null) {
			sfiItemChargeWorkingMemory = sfiItemChargeRuleBase.newStatelessSession();
		}
		
		if ( overrideChargeAmtRuleBase != null ){
			overrideChargeAmtWorkingMemory = overrideChargeAmtRuleBase.newStatelessSession();
		}
	}
		
	public Double retrieveMarkupFactorByChargeRule(OverrideChargeAmountRuleCriteria overrideChargeAmountRuleCriteria, DrugChargeInfo drugChargeInfo){
		return retrieveMarkupFactorByChargeRule(overrideChargeAmountRuleCriteria, drugChargeInfo.getPublicMarkupFactor(), drugChargeInfo.getPrivateMarkupFactor() );
	}
	
	private Double retrieveMarkupFactorByChargeRule(OverrideChargeAmountRuleCriteria overrideChargeAmountRuleCriteria, Double publicMarkup, Double privateMarkup){
		
		if ( overrideChargeAmtWorkingMemory != null){
			overrideChargeAmtWorkingMemory.execute(overrideChargeAmountRuleCriteria);
		}
		
		if( overrideChargeAmountRuleCriteria.isIncludeMarkup() ){		
			if( overrideChargeAmountRuleCriteria.isMarkUpOverride() ){
				return overrideChargeAmountRuleCriteria.getMarkUpOverrideValue();
			}else if ( PharmOrderPatType.Private.getDataValue().equals(overrideChargeAmountRuleCriteria.getChargeType()) ){
				return (privateMarkup == null ? 1.0 : privateMarkup);
			}else{
				return (publicMarkup == null ? 1.0 : publicMarkup);
			}
		}else{
			return 1.0;
		}
	}
	
	public Double retrieveHandlingAmtByChargeRule(DrugChargeRuleCriteria chargingRuleCriteria, DrugChargeInfo drugChargeInfo){		
		return retrieveHandlingAmtByChargeRule(chargingRuleCriteria, drugChargeInfo.getPublicHandleCharge(), drugChargeInfo.getPrivateHandleCharge());
	}
	
	private Double retrieveHandlingAmtByChargeRule(DrugChargeRuleCriteria chargingRuleCriteria, Double publicHandling, Double privateHandling){	
		if( chargingRuleCriteria.isIncludeHandling() ){		
			if ( PharmOrderPatType.Private.getDataValue().equals(chargingRuleCriteria.getChargeType()) ){
				return privateHandling;
			}else{
				return publicHandling;
			}
		}else{
			return 0.0;
		}
	}
	
	public DrugChargeRuleCriteria checkPrivateHandleRule(DrugChargeRuleCriteria chargingRuleCriteria){
		if( privateHandleWorkingMemory != null ){		
			privateHandleWorkingMemory.execute(chargingRuleCriteria);
		}	
		return chargingRuleCriteria;		
	}
	
	public DrugChargeRuleCriteria checkSfiItemChargeRule(DrugChargeRuleCriteria chargingRuleCriteria){
		
		if( sfiItemChargeWorkingMemory != null ){		
			sfiItemChargeWorkingMemory.execute(chargingRuleCriteria);
		}
		return chargingRuleCriteria;
	}
		
	public Integer retrieveMaxProfitLimit(DrugChargeRuleCriteria chargingRuleCriteria){
		Integer maxProfitLimit = 999999;
		
		if( CHARGING_SFI_OPMARKUP_ENABLED.get(false) &&
			( MedOrderPrescType.MpDischarge.getDataValue().equals(chargingRuleCriteria.getMedOrderPrescType()) ||
			  MedOrderPrescType.Out.getDataValue().equals(chargingRuleCriteria.getMedOrderPrescType()) )
		){
			maxProfitLimit = CHARGING_SFI_OPMARKUP_MAX.get(999999);			
		}else if (  CHARGING_SFI_IPMARKUP_ENABLED.get(false) &&
					( MedOrderPrescType.MpHomeLeave.getDataValue().equals(chargingRuleCriteria.getMedOrderPrescType()) ||
					  MedOrderPrescType.In.getDataValue().equals(chargingRuleCriteria.getMedOrderPrescType()) )		
		){
			maxProfitLimit = CHARGING_SFI_IPMARKUP_MAX.get(999999);	
		}
		return maxProfitLimit;
	}
	
	public boolean isMarkupProfitLimitEnable(DrugChargeRuleCriteria chargingRuleCriteria){
		if( !PharmOrderPatType.Private.getDataValue().equals(chargingRuleCriteria.getPharmOrderPatType()) && 
				!PharmOrderPatType.Nep.getDataValue().equals(chargingRuleCriteria.getPharmOrderPatType())     ){
				return false;
			}
			
			if( "M".equals(chargingRuleCriteria.getFmStatus()) || "S".equals(chargingRuleCriteria.getFmStatus()) ){						
				return false;
			}
			
			Integer maxProfitLimit = retrieveMaxProfitLimit(chargingRuleCriteria);
			
			if( maxProfitLimit.compareTo(999999) == 0 ){
				return false;
			}
			
		return true;
	}
	
	public DrugChargeRuleCriteria retrieveChargeRuleByPatTypeFmStatus(PharmOrderPatType pharmOrderPatType, String fmStatus){
		DrugChargeRuleCriteria chargingRuleCriteria = new DrugChargeRuleCriteria();
		chargingRuleCriteria.setPharmOrderPatType(pharmOrderPatType.getDataValue());
		chargingRuleCriteria.setFmStatus(fmStatus);
		chargingRuleCriteria.setMedOrderPrescType(MedOrderPrescType.Out.getDataValue());
		chargingRuleCriteria = checkPrivateHandleRule(chargingRuleCriteria);				
		chargingRuleCriteria = checkSfiItemChargeRule(chargingRuleCriteria);
		return chargingRuleCriteria;
	}
	
	@Remove
	public void destroy() 
	{
		
	}
}
