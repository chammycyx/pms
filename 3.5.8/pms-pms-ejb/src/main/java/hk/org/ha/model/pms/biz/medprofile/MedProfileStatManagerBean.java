package hk.org.ha.model.pms.biz.medprofile;

import hk.org.ha.fmk.pms.util.QueryUtils;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.inbox.PharmInboxClientNotifierInf;
import hk.org.ha.model.pms.biz.reftable.ActiveWardManagerLocal;
import hk.org.ha.model.pms.dms.persistence.PmsIpSpecMapping;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileErrorLog;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileErrorStat;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItemStat;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileStat;
import hk.org.ha.model.pms.persistence.medprofile.Replenishment;
import hk.org.ha.model.pms.udt.medprofile.ErrorLogType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileMoItemStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatAdminType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatOrderType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileType;
import hk.org.ha.model.pms.udt.medprofile.ReplenishmentStatus;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

/**
 * Session Bean implementation class MedProfileStatManagerBean
 */
@AutoCreate
@Stateless
@Name("medProfileStatManager")
@MeasureCalls
public class MedProfileStatManagerBean implements MedProfileStatManagerLocal {

	@Logger
	private Log logger;	

	@PersistenceContext
	private EntityManager em;
	
	@In
	private ActiveWardManagerLocal activeWardManager;
	
	@In
	private PharmInboxClientNotifierInf pharmInboxClientNotifier;
	
    public MedProfileStatManagerBean() {
    }
    
    @Override
    public void recalculateAllStat(MedProfile medProfile, boolean dispatchUpdateEvent) {
    	recalculateMedProfileErrorStat(medProfile);
    	recalculateMedProfilePoItemStat(medProfile);
    	recalculateMedProfileStat(medProfile, dispatchUpdateEvent);
    }

	@Override
	public void recalculateMedProfileStat(MedProfile medProfile, boolean dispatchUpdateEvent) {
		
		if (medProfile == null || medProfile.getId() == null) {
			return;
		}
		
		List<MedProfile> medProfileList = Arrays.asList(medProfile);
		if (medProfile.getType() == MedProfileType.Chemo) {
			medProfileList = fetchChemoMedProfiles(medProfile);
			medProfile = medProfileList.isEmpty() ? medProfile : medProfileList.get(0);
		}
		
		activeWardManager.initInvalidPasWardFlag(medProfile);
		List<MedProfileItem> medProfileItemList = fetchProcessingMedProfileItems(medProfileList, MedProfileItemStatus.Unvet_Vetted_Verified,
				Boolean.TRUE.equals(medProfile.getInactivePasWardFlag()), false);
		List<MedProfileStat> medProfileStatList = fetchMedProfileStats(medProfile);
		List<Replenishment> replenishementList = fetchReplenishmentList(medProfile);
		Set<String> wardAdminFreqCodeSet = fetchWardAdminFreqCodeSet(medProfile.getPatHospCode(), medProfile.getMedCase().getPasWardCode());
		Set<String> aomFreqCodeSet = fetchAomFreqCodeSet(medProfile.getWorkstore().getHospital());
		
		Date now = new Date();
		
		for (MedProfileStatAdminType adminType: MedProfileStatAdminType.values()) {
			
			for (MedProfileStatOrderType orderType: MedProfileStatOrderType.values()) {
				
				MedProfileStat medProfileStat = findMedProfileStat(medProfileStatList, adminType, orderType);
				if (medProfileStat == null) {
					medProfileStat = new MedProfileStat(adminType, orderType, medProfile);
				}
				
				medProfileStat.reset();
				medProfileStat.setWardAdminFreqCodeSet(wardAdminFreqCodeSet);
				medProfileStat.setAomFreqCodeSet(aomFreqCodeSet);
				medProfileStat.setCalculationStartDate(now);
				
				boolean keepMedProfileStat = medProfile.getStatus() != MedProfileStatus.Inactive &&
						recalculateMedProfileStat(medProfileStat, medProfileItemList, replenishementList);

				if (!keepMedProfileStat) {
					if (medProfileStat.getId() != null) {
						em.remove(medProfileStat);
					}
					medProfileStatList.remove(medProfileStat);					
				} else if (medProfileStat.getId() == null) {
					em.persist(medProfileStat);
				} else {
					em.merge(medProfileStat);
				}
			}
		}
		
		if (dispatchUpdateEvent) {
			dispatchUpdateEvent(medProfile);
		}
	}
	
	@Override
	public void clearMissingSpecialtyMappingException(List<PmsIpSpecMapping> pmsIpSpecMappingList) {

		if (pmsIpSpecMappingList == null) {
			return;
		}
		
		Map<Pair<String, String>, Set<String>> pasSpecialtyMap = MedProfileUtils.buildPasSpecialtyMap(pmsIpSpecMappingList, false);
		
		Set<String> updatedHospCodeSet = new HashSet<String>();
		List<MedProfile> medProfileList = fetchMedProfiles(pasSpecialtyMap);
		
		for (MedProfile medProfile: medProfileList) {
			if (clearMissingSpecialtyMappingException(medProfile)) {
				updatedHospCodeSet.add(medProfile.getWorkstore().getHospCode());
			}
		}
		pharmInboxClientNotifier.dispatchUpdateEvent(updatedHospCodeSet);
	}
	
	private boolean clearMissingSpecialtyMappingException(MedProfile medProfile) {

		boolean success = false;
		
		for (MedProfileItem medProfileItem: medProfile.getMedProfileItemList()) {
			success |= clearMissingSpecialtyMappingException(medProfileItem);
		}
		
		if (success) {
			recalculateMedProfileErrorStat(medProfile);
		}
		return success;
	}
	
	private boolean clearMissingSpecialtyMappingException(MedProfileItem medProfileItem) {

		MedProfileErrorLog errorLog = medProfileItem.getMedProfileErrorLog();
		if (MedProfileItemStatus.Unvet_Vetted_Verified.contains(medProfileItem.getStatus()) &&
				errorLog != null && errorLog.getType() == ErrorLogType.MissingSpecialty) {
			errorLog.setHideErrorStatFlag(Boolean.TRUE);
			em.merge(errorLog);
			return true;
		}
		return false;
	}
	
	private boolean recalculateMedProfileStat(MedProfileStat medProfileStat,
			List<MedProfileItem> medProfileItemList, List<Replenishment> replenishmentList) {
		
		MedProfileItemStatus status;
		
		switch (medProfileStat.getAdminType()) {
			case NewArrival:
				status = MedProfileItemStatus.Unvet;
				break;
			case Verify:
				status = MedProfileItemStatus.Vetted;
				break;
			default:
				return false;
		}
		
		switch (medProfileStat.getOrderType()) {
			case Normal:
				return recalculateNormal(status, medProfileStat, medProfileItemList, replenishmentList);
			case Urgent:
				return recalculateUrgent(status, medProfileStat, medProfileItemList, replenishmentList);
			case PendingSuspend:
				return recalculatePendingSuspend(status, medProfileStat, medProfileItemList, replenishmentList);
			case All:
				return recalculateAll(status, medProfileStat, medProfileItemList, replenishmentList);
		}
	
		return false;
	}
	
	@Override
	public void recalculateMedProfileErrorStat(MedProfile medProfile) {
		
		if (medProfile == null || medProfile.getId() == null) {
			return;
		}
		
		List<MedProfile> medProfileList = Arrays.asList(medProfile);
		if (medProfile.getType() == MedProfileType.Chemo) {
			medProfileList = fetchChemoMedProfiles(medProfile);
			medProfile = medProfileList.isEmpty() ? medProfile : medProfileList.get(0);
		}
		
		activeWardManager.initInvalidPasWardFlag(medProfile);
		
		MedProfileErrorStat medProfileErrorStat = fetchMedProfileErrorStat(medProfile);
		if (medProfileErrorStat == null) {
			medProfileErrorStat = new MedProfileErrorStat();
		}
		
		List<MedProfileItem> medProfileItemList = fetchProcessingMedProfileItems(medProfileList, Arrays.asList(MedProfileItemStatus.Verified),
				Boolean.TRUE.equals(medProfile.getInactivePasWardFlag()), true);
		
		boolean keepMedProfileErrorStat = medProfile.getStatus() != MedProfileStatus.Inactive &&
					recalculateMedProfileErrorStat(medProfileErrorStat, medProfileItemList);
		
		if (!keepMedProfileErrorStat) {
			if (medProfileErrorStat.getId() != null) {
				em.remove(medProfileErrorStat);
				medProfile.setMedProfileErrorStat(null);
			}
		} else if (medProfileErrorStat.getId() == null) {
			medProfileErrorStat.setMedProfile(medProfile);
			medProfile.setMedProfileErrorStat(medProfileErrorStat);
			em.persist(medProfileErrorStat);
		} else {
			em.merge(medProfileErrorStat);
		}		
	}
	
	private boolean recalculateMedProfileErrorStat(MedProfileErrorStat medProfileErrorStat,
			List<MedProfileItem> medProfileItemList) {
		
		boolean keepMedProfileErrorStat = false;
		
		medProfileErrorStat.reset();
		
		for (MedProfileItem medProfileItem: medProfileItemList) {
			
			MedProfileErrorLog medProfileErrorLog = medProfileItem.getMedProfileErrorLog();
			
			if (medProfileItem.getStatus() == MedProfileItemStatus.Verified &&
				medProfileErrorLog != null && !Boolean.TRUE.equals(medProfileErrorLog.getHideErrorStatFlag()) &&
				medProfileErrorLog.getType() != ErrorLogType.WardTransfer &&
				(medProfileErrorLog.getType() == ErrorLogType.Frozen || !Boolean.TRUE.equals(medProfileItem.getMedProfileMoItem().getFrozenFlag())) &&
				medProfileItem.getMedProfileMoItem().getPendingCode() == null &&
				medProfileItem.getMedProfileMoItem().getSuspendCode() == null) {
				
				medProfileErrorStat.update(medProfileItem);
				keepMedProfileErrorStat = true;				
			}
		}
		
		return keepMedProfileErrorStat;
	}
	
	private boolean recalculateNormal(MedProfileItemStatus status, MedProfileStat medProfileStat,
			List<MedProfileItem> medProfileItemList, List<Replenishment> replenishmentList) {
		
		boolean keepMedProfileStat = false;
		
		for (MedProfileItem medProfileItem: medProfileItemList) {
			
			MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
			List<Replenishment> relatedReplenishmentList = findReplenishments(replenishmentList, medProfileMoItem.getId());
			
			if (medProfileStat.getAdminType() == MedProfileStatAdminType.NewArrival &&
					relatedReplenishmentList.size() > 0) {
				medProfileStat.update(medProfileItem, relatedReplenishmentList);
				keepMedProfileStat |= medProfileMoItem.isNormalItem(medProfileStat.getCalculationStartDate());
			} 
			
			if (medProfileItem.getStatus() == status && medProfileMoItem.isNormalItem(medProfileStat.getCalculationStartDate())) {
				medProfileStat.update(medProfileItem, relatedReplenishmentList.size() > 0);
				keepMedProfileStat = true;
			}
		}
		
		return keepMedProfileStat;
	}
	
	private boolean recalculateUrgent(MedProfileItemStatus status, MedProfileStat medProfileStat,
			List<MedProfileItem> medProfileItemList, List<Replenishment> replenishmentList) {

		boolean keepMedProfileStat = false;
		
		for (MedProfileItem medProfileItem: medProfileItemList) {
			
			MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
			List<Replenishment> relatedReplenishmentList = findReplenishments(replenishmentList, medProfileMoItem.getId());

			if (medProfileMoItem.isUrgentItem(medProfileStat.getCalculationStartDate())) {
				
				if (medProfileStat.getAdminType() == MedProfileStatAdminType.NewArrival &&
						relatedReplenishmentList.size() > 0) {
					medProfileStat.update(medProfileItem, relatedReplenishmentList);
					keepMedProfileStat = true;
				} 
				
				if (medProfileItem.getStatus() == status) {
					medProfileStat.update(medProfileItem, relatedReplenishmentList.size() > 0);
					keepMedProfileStat = true;
				}
			}
		}
		
		return keepMedProfileStat;
	}
	
	private boolean recalculatePendingSuspend(MedProfileItemStatus status, MedProfileStat medProfileStat,
			List<MedProfileItem> medProfileItemList, List<Replenishment> replenishmentList) {

		boolean keepMedProfileStat = false;
		
		for (MedProfileItem medProfileItem: medProfileItemList) {
			
			MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
			List<Replenishment> relatedReplenishmentList = findReplenishments(replenishmentList, medProfileMoItem.getId());
			
			if (medProfileMoItem.isPendingSuspendItem()) {
				
				if (medProfileStat.getAdminType() == MedProfileStatAdminType.NewArrival &&
						relatedReplenishmentList.size() > 0) {
					medProfileStat.update(medProfileItem, relatedReplenishmentList);
					keepMedProfileStat = true;
				} 
				
				if (medProfileItem.getStatus() == status || 
						(medProfileItem.getStatus() == MedProfileItemStatus.Verified && medProfileStat.getAdminType() == MedProfileStatAdminType.Verify &&
						Boolean.TRUE.equals(medProfileMoItem.getUnitDoseExceptFlag()))) {
					medProfileStat.update(medProfileItem, relatedReplenishmentList.size() > 0);
					keepMedProfileStat = true;
				}
			}
		}
		
		return keepMedProfileStat;
	}
	
	private boolean recalculateAll(MedProfileItemStatus status, MedProfileStat medProfileStat,
			List<MedProfileItem> medProfileItemList, List<Replenishment> replenishmentList) {

		boolean keepMedProfileStat = false;
		
		for (MedProfileItem medProfileItem: medProfileItemList) {

			MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
			List<Replenishment> relatedReplenishmentList = findReplenishments(replenishmentList, medProfileMoItem.getId());			
			
			if (medProfileStat.getAdminType() == MedProfileStatAdminType.NewArrival &&
					relatedReplenishmentList.size() > 0) {
				medProfileStat.update(medProfileItem, relatedReplenishmentList);
				keepMedProfileStat = true;
			} 
			
			if (medProfileItem.getStatus() == status ||
					(Boolean.TRUE.equals(medProfileMoItem.getUnitDoseExceptFlag()) && medProfileStat.getAdminType() == MedProfileStatAdminType.Verify &&
					status == MedProfileItemStatus.Verified)) {
				medProfileStat.update(medProfileItem, relatedReplenishmentList.size() > 0);
				keepMedProfileStat = true;
			}
		}
		
		return keepMedProfileStat;
	}
	
	@Override
	public void recalculateMedProfilePoItemStat(MedProfile medProfile) {
		
		if (medProfile == null || medProfile.getId() == null) {
			return;
		}
		
		activeWardManager.initInvalidPasWardFlag(medProfile);
		
		List<MedProfileItem> medProfileItemList = fetchProcessingMedProfileItems(Arrays.asList(medProfile), Arrays.asList(MedProfileItemStatus.Verified),
				Boolean.TRUE.equals(medProfile.getInactivePasWardFlag()), false);
		Map<Long, MedProfilePoItemStat> medProfilePoItemStatMap = buildMedProfilePoItemStatMap(fetchMedProfilePoItemStat(medProfile));
		
		for (MedProfileItem medProfileItem: medProfileItemList) {
			for (MedProfilePoItem medProfilePoItem: medProfileItem.getMedProfileMoItem().getMedProfilePoItemList()) {
				
				if (!shouldRecordMedProfilePoItemStat(medProfile, medProfileItem, medProfilePoItem)) {
					continue;
				}
				
				MedProfilePoItemStat medProfilePoItemStat = medProfilePoItemStatMap.get(medProfilePoItem.getId());
				if (medProfilePoItemStat == null) {
					medProfilePoItemStat = new MedProfilePoItemStat();
				}
				updateMedProfilePoItemStat(medProfile, medProfileItem, medProfilePoItem, medProfilePoItemStat);
				
				if (medProfilePoItemStat.getId() == null) {
					em.persist(medProfilePoItemStat);
				} else {
					medProfilePoItemStatMap.remove(medProfilePoItem.getId());
				}
			}
		}
		
		removeMedProfilePoItemStatCollection(medProfilePoItemStatMap.values());
	}
	
	private Map<Long, MedProfilePoItemStat> buildMedProfilePoItemStatMap(List<MedProfilePoItemStat> medProfilePoItemStatList) {
		
		Map<Long, MedProfilePoItemStat> resultMap = new TreeMap<Long, MedProfilePoItemStat>();
		
		for (MedProfilePoItemStat medProfilePoItemStat: medProfilePoItemStatList) {
			resultMap.put(medProfilePoItemStat.getMedProfilePoItem().getId(), medProfilePoItemStat);
		}
		return resultMap;
	}
	
	private boolean shouldRecordMedProfilePoItemStat(MedProfile medProfile, MedProfileItem medProfileItem, MedProfilePoItem medProfilePoItem) {
		
		MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
		
		if (medProfile.getStatus() != MedProfileStatus.Active && (medProfile.getStatus() != MedProfileStatus.HomeLeave || medProfile.getReturnDate() == null)) {
			return false;
		}
		
		if (medProfile.getWardCode() == null) {
			return false;
		}
		
		if (medProfileItem.getStatus() != MedProfileItemStatus.Verified || medProfileMoItem.getStatus() == MedProfileMoItemStatus.SysDeleted) {
			return false;
		}
		
    	if (medProfilePoItem.getLastDueDate() != null &&
    			(Boolean.TRUE.equals(medProfilePoItem.getSingleDispFlag()) || MedProfileUtils.compare(medProfilePoItem.getRefillDuration(), Integer.valueOf(0)) <= 0)) {
    		return false;
    	}
		
		if (ObjectUtils.compare(medProfilePoItem.getDueDate(), MedProfileUtils.getOverdueStartDate(new Date())) < 0 ||
				ObjectUtils.compare(medProfilePoItem.getDueDate(), medProfileMoItem.getEndDate(), true) >= 0) {
			return false;
		}
		
		if (Boolean.TRUE.equals(medProfileMoItem.getTransferFlag())) {
			return false;
		}
		
		if (Boolean.TRUE.equals(medProfileMoItem.getFrozenFlag())) {
			return false;
		}
		
		if (medProfileMoItem.getSuspendCode() != null || medProfileMoItem.getPendingCode() != null) {
			return false;
		}
		
		if (Boolean.TRUE.equals(medProfileMoItem.getPivasFlag())) {
			return false;
		}
		
		if (medProfilePoItem.isNoLabelItem()) {
			return false;
		}
		
		if (!Boolean.TRUE.equals(medProfile.getPrivateFlag()) && medProfilePoItem.isSfiItem()) {
			return false;
		}
		
		if (ObjectUtils.compare(medProfilePoItem.getRemainIssueQty(), BigDecimal.ZERO, true) <= 0) {
			return false;
		}
		
		return true;
	}
	
	private void updateMedProfilePoItemStat(MedProfile medProfile, MedProfileItem medProfileItem, MedProfilePoItem medProfilePoItem,
			MedProfilePoItemStat medProfilePoItemStat) {
		
		medProfilePoItemStat.setWardCode(medProfile.getWardCode());
		medProfilePoItemStat.setDueDate(medProfilePoItem.getDueDate());
		medProfilePoItemStat.setLastDueDate(medProfilePoItem.getLastDueDate());
		medProfilePoItemStat.setReturnDate(medProfile.getStatus() == MedProfileStatus.HomeLeave ? medProfile.getReturnDate() : null);
		medProfilePoItemStat.setUrgentFlag(medProfileItem.getMedProfileMoItem().getUrgentFlag());
		medProfilePoItemStat.setStatFlag(medProfilePoItem.getStatFlag());
		medProfilePoItemStat.setPatHospCode(medProfile.getPatHospCode());
		medProfilePoItemStat.setPasWardCode(medProfile.getMedCase().getPasWardCode());
		medProfilePoItemStat.setHospital(medProfile.getWorkstore().getHospital());
		medProfilePoItemStat.setMedProfile(medProfile);
		medProfilePoItemStat.setMedProfilePoItem(medProfilePoItem);
	}
	
	private void removeMedProfilePoItemStatCollection(Collection<MedProfilePoItemStat> medProfilePoItemStatCollection) {
		
		for (MedProfilePoItemStat medProfilePoItemStat: medProfilePoItemStatCollection) {
			em.remove(medProfilePoItemStat);
		}
	}
	
	private void dispatchUpdateEvent(MedProfile medProfile) {
		
		try {
			Map<String, Object> headerParams = new HashMap<String, Object>();
			
			if (medProfile != null && medProfile.getWorkstore() != null) {
				headerParams.put("HOSP_CODE", medProfile.getWorkstore().getHospCode());
				headerParams.put("WORKSTORE_CODE", medProfile.getWorkstore().getWorkstoreCode());
			}
			
			pharmInboxClientNotifier.notifyClient("NOTIFICATION FROM MedProfileOrderProcessorBean",
					headerParams);

			logger.info("Notification has been sent");
		} catch (Exception ex) {
			logger.error(ex, ex);
		}
	}

	private MedProfileStat findMedProfileStat(List<MedProfileStat> medProfileStatList,
			MedProfileStatAdminType adminType, MedProfileStatOrderType orderType) {

		for (MedProfileStat medProfileStat: medProfileStatList) {
			if (adminType == medProfileStat.getAdminType() &&
				orderType == medProfileStat.getOrderType()) {
				return medProfileStat;
			}
		}
		
		return null;
	}
		
	@SuppressWarnings("unchecked")
	private List<MedProfile> fetchMedProfiles(Map<Pair<String, String>, Set<String>> pasSpecialtyMap) {
		
		List<MedProfile> medProfileList = new ArrayList<MedProfile>();
		
		for (Entry<Pair<String, String>, Set<String>> entry: pasSpecialtyMap.entrySet()) {
			medProfileList.addAll(QueryUtils.splitExecute(em.createQuery(
					"select p from MedProfile p" +  // 20160809 index check : MedProfile.patHospCode,status : I_MED_PROFILE_07
					" where p.patHospCode = :patHospCode" +
					" and p.medCase.pasWardCode = :pasWardCode" +
					" and p.medCase.pasSpecCode in :pasSpecCodeSet" +
					" and p.status in :notInactiveStatus")
					.setParameter("patHospCode", entry.getKey().getLeft())
					.setParameter("pasWardCode", entry.getKey().getRight())
					.setParameter("notInactiveStatus", MedProfileStatus.Not_Inactive)
					.setHint(QueryHints.BATCH, "p.medCase")
					.setHint(QueryHints.BATCH, "p.medProfileItemList.medProfileErrorLog")
					.setHint(QueryHints.BATCH, "p.medProfileItemList.medProfileMoItem.medProfilePoItemList"),
					"pasSpecCodeSet", entry.getValue()));
		}
		return medProfileList;
	}
	
	@SuppressWarnings("unchecked")
	private List<MedProfile> fetchChemoMedProfiles(MedProfile medProfile) {
		
		return em.createQuery(
				"select p from MedProfile p" + // 20171023 index check : MedProfile.medCase : FK_MED_PROFILE_02
				" where p.hospCode = :hospCode" +
				" and p.medCase.caseNum = :caseNum" +
				" and p.type = :type" +
				" order by p.id")
				.setParameter("hospCode", medProfile.getHospCode())
				.setParameter("caseNum", medProfile.getMedCase().getCaseNum())
				.setParameter("type", MedProfileType.Chemo)
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	private List<MedProfileItem> fetchProcessingMedProfileItems(List<MedProfile> medProfileList, List<MedProfileItemStatus> statusList,
			boolean manualItemOnly, boolean includeFrozenItem) {
		
		return QueryUtils.splitExecute(em.createQuery(
				"select i from MedProfileItem i" + // 20120225 index check : MedProfileItem.medProfile : FK_MED_PROFILE_ITEM_01
				" where i.status in :statusList" +
				" and (i.medProfileMoItem.endDate > :currentDate or i.medProfileMoItem.endDate is null)" +
				" and (i.medProfileMoItem.transferFlag = false or i.medProfileMoItem.transferFlag is null)" +
				(includeFrozenItem ? "" : " and (i.medProfileMoItem.frozenFlag = false or i.medProfileMoItem.frozenFlag is null)") +
				(manualItemOnly ? " and i.medProfileMoItem.itemNum >= 10000" : "") +
				" and i.medProfileMoItem.cmmFormulaCode is null" +
				" and i.medProfileId in :medProfileIdList" +
				" order by i.id")
				.setParameter("statusList", statusList)
				.setParameter("currentDate", new Date())
				.setHint(QueryHints.BATCH, "i.medProfileErrorLog")
				.setHint(QueryHints.BATCH, "i.medProfileMoItem")
				, "medProfileIdList",  MedProfileUtils.constructMedProfileIdList(medProfileList));
	}
	
	@SuppressWarnings("unchecked")
	private List<MedProfileStat> fetchMedProfileStats(MedProfile medProfile) {
		
		List<MedProfileStat> medProfileStatList = em.createQuery(
				"select s from MedProfileStat s" + // 20120225 index check : MedProfileStat.medProfile : FK_MED_PROFILE_STAT_01
				" where s.medProfile = :medProfile" +
				" order by s.id")
				.setParameter("medProfile", medProfile)
				.getResultList();
		medProfile.setMedProfileStatList(medProfileStatList);
		return medProfileStatList;
	}
	
	@SuppressWarnings("unchecked")
	private MedProfileErrorStat fetchMedProfileErrorStat(MedProfile medProfile) {

		return (MedProfileErrorStat) MedProfileUtils.getFirstItem(em.createQuery(
				"select m.medProfileErrorStat from MedProfile m" + // 20150206 index check : MedProfileErrorStat.medProfile : FK_MED_PROFILE_ERROR_STAT_01
				" where m = :medProfile" +
				" order by m.id")
				.setParameter("medProfile", medProfile)
				.getResultList());
	}

	@SuppressWarnings("unchecked")
	private List<MedProfilePoItemStat> fetchMedProfilePoItemStat(MedProfile medProfile) {
		
		return em.createQuery(
				"select s from MedProfilePoItemStat s" + // 20151012 index check : MedProfile.id : PK_MED_PROFILE
				" where s.medProfile = :medProfile" +
				" order by s.id")
				.setParameter("medProfile", medProfile)
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	private Set<String> fetchWardAdminFreqCodeSet(String patHospCode, String pasWardCode) {
		
		return new HashSet<String>(em.createQuery(
				"select distinct w.freqCode from WardAdminFreq w" + // 20120225 index check : WardAdminFreq.patHospCode,pasWardCode : PK_WARD_ADMIN_FREQ
				" where w.patHospCode = :patHospCode" +
				" and (w.pasWardCode = :pasWardCode" +
				" or w.pasWardCode = :wildCard)")
				.setParameter("patHospCode", patHospCode)
				.setParameter("pasWardCode", pasWardCode)
				.setParameter("wildCard", "*")
				.getResultList());
	}
	
	@SuppressWarnings("unchecked")
	private Set<String> fetchAomFreqCodeSet(Hospital hospital) {
		
		return new HashSet<String>(em.createQuery(
				"select distinct o.dailyFreqCode from AomSchedule o" + // 20150209 index check : AomSchedule.hospital : FK_AOM_SCHEDULE_01
				" where o.hospital = :hospital")
				.setParameter("hospital", hospital)
				.getResultList());
	}
	
	@SuppressWarnings("unchecked")
	private List<Replenishment> fetchReplenishmentList(MedProfile medProfile) {
		
		return em.createQuery(
				"select r from Replenishment r" + // 20120312 index check : Replenishment.medProfile : FK_REPLENISHMENT_01
				" where r.medProfile = :medProfile" +
				" and r.status = :outstandingStatus")
				.setParameter("medProfile", medProfile)
				.setParameter("outstandingStatus", ReplenishmentStatus.Outstanding)
				.setHint(QueryHints.FETCH, "r.replenishmentItemList")
				.setHint(QueryHints.FETCH, "r.medProfileMoItem.medProfileItem")
				.getResultList();
	}
	
	private List<Replenishment> findReplenishments(
			List<Replenishment> replenishmentList, Long medProfileMoItemId) {
		
		List<Replenishment> resultReplenishmentList = new ArrayList<Replenishment>();
		
		for (Replenishment replenishment: replenishmentList) {
			if (replenishment.getMedProfileMoItem().getId().equals(medProfileMoItemId)) {
				resultReplenishmentList.add(replenishment);
			}
		}
		
		return resultReplenishmentList;
	}
	
	
}
