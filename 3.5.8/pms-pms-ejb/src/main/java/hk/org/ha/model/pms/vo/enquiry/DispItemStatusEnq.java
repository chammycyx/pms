package hk.org.ha.model.pms.vo.enquiry;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class DispItemStatusEnq implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String wardCode;
	
	private String caseNum;
	
	private String itemCode;
	
	private String itemDesc;
	
	private String itemDescTlf;
	
	private String itemStatus;
	
	private String orderStatus;
	
	private String batchCode;
	
	private Date batchDate;
	
	private Date dueDate;

	private Date labelGenDate;
	
	private Date labelGenDateFrom;
	
	private Date labelGenDateTo;
	
	private String trxId;
	
	private Integer dispQty;
	
	private String baseUnit;
	
	private Date allItemInProcessFromDate;
	
	private Date allItemInProcessToDate;

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemDesc() {
		return itemDesc;
	}

	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}
	
	public String getItemDescTlf() {
		return itemDescTlf;
	}

	public void setItemDescTlf(String itemDescTlf) {
		this.itemDescTlf = itemDescTlf;
	}

	public String getItemStatus() {
		return itemStatus;
	}

	public void setItemStatus(String itemStatus) {
		this.itemStatus = itemStatus;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getBatchCode() {
		return batchCode;
	}

	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}

	public Date getBatchDate() {
		return batchDate;
	}

	public void setBatchDate(Date batchDate) {
		this.batchDate = batchDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public String getTrxId() {
		return trxId;
	}

	public void setTrxId(String trxId) {
		this.trxId = trxId;
	}

	public Date getLabelGenDate() {
		return labelGenDate;
	}

	public void setLabelGenDate(Date labelGenDate) {
		this.labelGenDate = labelGenDate;
	}

	public Date getLabelGenDateFrom() {
		return labelGenDateFrom;
	}

	public void setLabelGenDateFrom(Date labelGenDateFrom) {
		this.labelGenDateFrom = labelGenDateFrom;
	}

	public Date getLabelGenDateTo() {
		return labelGenDateTo;
	}

	public void setLabelGenDateTo(Date labelGenDateTo) {
		this.labelGenDateTo = labelGenDateTo;
	}

	public Integer getDispQty() {
		return dispQty;
	}

	public void setDispQty(Integer dispQty) {
		this.dispQty = dispQty;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public Date getAllItemInProcessFromDate() {
		return allItemInProcessFromDate;
	}

	public void setAllItemInProcessFromDate(Date allItemInProcessFromDate) {
		this.allItemInProcessFromDate = allItemInProcessFromDate;
	}

	public Date getAllItemInProcessToDate() {
		return allItemInProcessToDate;
	}

	public void setAllItemInProcessToDate(Date allItemInProcessToDate) {
		this.allItemInProcessToDate = allItemInProcessToDate;
	}

}
