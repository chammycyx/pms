package hk.org.ha.model.pms.biz.security;

import static hk.org.ha.model.pms.prop.Prop.ALERT_MDS_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.ALERT_PROFILE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.ONESTOP_PATIENT_PATCATCODE_REQUIRED;
import static hk.org.ha.model.pms.prop.Prop.PATIENT_MOE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.SECURITY_IDLE_SCREEN_LOCK;
import static hk.org.ha.model.pms.prop.Prop.SECURITY_IDLE_TIMEOUT;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.security.Uam;
import hk.org.ha.fmk.pms.sys.SysProfile;
import hk.org.ha.fmk.pms.sys.entity.SystemMessage;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asg.exception.ehr.EhrException;
import hk.org.ha.model.pms.biz.ApplicationProp;
import hk.org.ha.model.pms.biz.drug.DmAdminTimeListManagerLocal;
import hk.org.ha.model.pms.biz.drug.DmDailyFrequencyListManagerLocal;
import hk.org.ha.model.pms.biz.drug.DmFormVerbMappingManagerLocal;
import hk.org.ha.model.pms.biz.drug.DmMedicalOfficerListManagerLocal;
import hk.org.ha.model.pms.biz.drug.DmRegimenListManagerLocal;
import hk.org.ha.model.pms.biz.drug.DmSiteListManagerLocal;
import hk.org.ha.model.pms.biz.drug.DmSupplFrequencyListManagerLocal;
import hk.org.ha.model.pms.biz.drug.DmSupplSiteListManagerLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileOrderProcessorLocal;
import hk.org.ha.model.pms.biz.printing.PrintLabelManagerLocal;
import hk.org.ha.model.pms.biz.reftable.FollowUpWarningManagerLocal;
import hk.org.ha.model.pms.biz.reftable.PatientCatListManagerLocal;
import hk.org.ha.model.pms.biz.reftable.PrnRouteManagerLocal;
import hk.org.ha.model.pms.biz.reftable.QtyDurationConversionWorkstoreListManagerLocal;
import hk.org.ha.model.pms.biz.reftable.RefTableManagerLocal;
import hk.org.ha.model.pms.biz.reftable.RefillConfigManagerLocal;
import hk.org.ha.model.pms.biz.reftable.SpecialtyManagerLocal;
import hk.org.ha.model.pms.biz.reftable.WardManagerLocal;
import hk.org.ha.model.pms.biz.reftable.pivas.PivasContainerListManagerLocal;
import hk.org.ha.model.pms.biz.report.WorkstoreListManagerLocal;
import hk.org.ha.model.pms.biz.sys.SystemMessageManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.corp.cache.DmRouteCacher;
import hk.org.ha.model.pms.dms.persistence.DmMedicalOfficer;
import hk.org.ha.model.pms.dms.persistence.DmMedicalOfficerPK;
import hk.org.ha.model.pms.dms.persistence.DmRoute;
import hk.org.ha.model.pms.dms.persistence.DmWarning;
import hk.org.ha.model.pms.dms.persistence.PmsPcuMapping;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasContainer;
import hk.org.ha.model.pms.persistence.corp.PrnRoute;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.ChargeReason;
import hk.org.ha.model.pms.persistence.reftable.FollowUpWarning;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.persistence.reftable.OperationWorkstation;
import hk.org.ha.model.pms.persistence.reftable.Prop;
import hk.org.ha.model.pms.persistence.reftable.RefillConfig;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.prop.PropHolder;
import hk.org.ha.model.pms.vo.printing.PrinterSelect;
import hk.org.ha.model.pms.vo.security.PropMap;
import hk.org.ha.model.pms.vo.sys.PostLogonResult;
import hk.org.ha.model.pms.vo.sys.SysMsgMap;
import hk.org.ha.service.biz.pms.asg.interfaces.EhrServiceJmsRemote;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.security.Identity;

/**
 * Session Bean implementation class PostLogonServiceBean
 */
@AutoCreate
@Stateful
@Name("postLogonService")
@Scope(ScopeType.SESSION)
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PostLogonServiceBean implements PostLogonServiceLocal {

	private final static String ACTIVE_OPERATION_NAME = "active.operation.name";
	private final static String ACTIVE_OPERATION_MODE = "active.operation.mode";
	private final static String PERMISSION_SEND_PHARM_REMARK = "permission.sendPharmRemark";
	private final static String PERMISSION_ITEMAVAILABILITYMAINT_LOCAL_SUSPEND = "permission.itemAvailabilityMaintLocalSuspend";
	private final static String PERMISSION_ITEMAVAILABILITYMAINT_SFI = "permission.itemAvailabilityMaintSfi";
	private final static String PERMISSION_ITEMAVAILABILITYMAINT_SAMPLE_ITEM = "permission.itemAvailabilityMaintSampleItem";
	private final static String PERMISSION_ITEMAVAILABILITYMAINT_DRUG_SCOPE = "permission.itemAvailabilityMaintDrugScope";
	private final static String PERMISSION_SPEC_MAP_SFI_MAINT_SFI = "permission.specMapSfiMaintSfi";
	private final static String PERMISSION_SPEC_MAP_SFI_MAINT_SFI_SN = "permission.specMapSfiMaintSfiSn";
	private final static String PERMISSION_UNVET_PRESC = "permission.unvetPresc";
	private final static String PERMISSION_ALLOW_MODIFY = "permission.allowModify";
	private final static String PERMISSION_ONE_STOP_ENTRY_UNCOLLECT = "permission.oneStopEntryUncollect";
	private final static String PERMISSION_PRINT_CDDH_RPT = "permission.printCddhRpt";
	private final static String PERMISSION_EDIT_CDDH_REMARK = "permission.editCddhRemark";
	private final static String PERMISSION_DISP_METHOD_CONFIG = "permission.dispMethodConfig";
	private static final String FM_RPT_CORP = "permission.fmRptCorp";
	private static final String FM_RPT_CLUSTER = "permission.fmRptCluster";
	private static final String FM_RPT_HOSP = "permission.fmRptHosp";
	private static final String FM_RPT_CLUSTER_HOSP = "fmRptClusterHospital";
	private final static String PERMISSION_PRE_VET = "permission.preVet";
	private static final String EHR_EHRSS_EHR_ACCESS_FAQ_URL = "ehr.ehrss.ehr-access-faq-url";
	private static final String EHR_UID = "ehr.uid";

	@PersistenceContext
	private EntityManager em;
	
	@In
	private AuditLogger auditLogger;
	
	@In 
	private AccessControlLocal accessControl;	
	
	@In
	private PrintLabelManagerLocal printLabelManager;
	
	@In
	private SpecialtyManagerLocal specialtyManager;
	
	@In
	private WardManagerLocal wardManager;
	
	@In
	private PatientCatListManagerLocal patientCatListManager;
		
	@In
	private DmMedicalOfficerListManagerLocal dmMedicalOfficerListManager;
	
	@In
	private DmDailyFrequencyListManagerLocal dmDailyFrequencyListManager;
	
	@In
	private DmSupplFrequencyListManagerLocal dmSupplFrequencyListManager;
	
	@In
	private DmSiteListManagerLocal dmSiteListManager;
	
	@In
	private DmSupplSiteListManagerLocal dmSupplSiteListManager;
	
	@In
	private DmRegimenListManagerLocal dmRegimenListManager;	
	
	@In
	private DmAdminTimeListManagerLocal dmAdminTimeListManager;
	
	@In
	private DmFormVerbMappingManagerLocal dmFormVerbMappingManager;
	
	@In
	private PrnRouteManagerLocal prnRouteManager;
		
	@In
	private FollowUpWarningManagerLocal followUpWarningManager;
	
	@In
	private RefTableManagerLocal refTableManager;
	
	@In
	private RefillConfigManagerLocal refillConfigManager;
	
	@In
	private WorkstoreListManagerLocal workstoreListManager;

	@In
	private MedProfileOrderProcessorLocal medProfileOrderProcessor;
	
	@In
	private QtyDurationConversionWorkstoreListManagerLocal qtyDurationConversionWorkstoreListManager;
	
	@In
	private PivasContainerListManagerLocal pivasContainerListManager;
	
	@In
	private SystemMessageManagerLocal systemMessageManager;
	
	@In
	private EhrServiceJmsRemote ehrServiceProxy;
	
	@In
	private Identity identity;

	@In 
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private ApplicationProp applicationProp;
			
	@Out(required = false)
	private PropMap permissionMap;

	@Out(required = false)
	private List<PmsPcuMapping> pmsPcuMappingList;
		
	@Out(required = false)
	private PrinterSelect printerSelect;	
	
	@Out(required=false)
	private List<String> oneStopSpecialtyList;
	
	@Out(required=false)
	private List<String> oneStopWardList;
	
	@Out(required=false)
	private List<String> oneStopPatientCatList;
	
	@Out(required=false)
	private List<String> patientCatCodeList;
	
	@Out(required=false)
	private List<DmMedicalOfficer> oneStopMedicalOfficerList;
	
	@Out(required=false)
	private List<ChargeReason> exemptChargeReasonList;
	
	@Out(required=false)
	private RefillConfig vettingRefillConfig;
	
	@Out(required=false)
	private List<PivasContainer> pivasContainerList;
	
	@Out(required=false)
	private PropMap propMap = new PropMap();
	
	@Out(required=false)
	private PropMap formVerbMap;
	
	@Out(required=false)
	private PropMap mpFormVerbMap;
	
	@Out(required=false)
	private PropMap prnRouteDescMap = new PropMap();
	
	@Out(required = false)
	private List<String> reminderList;
	
	@Out(required = false)
	private List<Workstore> workstoreList;

	@In @Out
	private SysProfile sysProfile;

	@Out(required=false)
	private List<DmRoute> dmRouteList;
	
	@SuppressWarnings("unused")
	@Out(required = false)
	private int idleLimit;
	
	@Out(required = false)
	private List<DmWarning> dmWarningList;
	
	@Out(required = false)
	private List<Integer> atdpsPickStationsNumList;
	
	@Out(required = false)
	private Long ehrUid;
	
	private boolean enablePreLoadCacher = false;
	
	private Workstore workstore;
	
	private OperationProfile activeProfile;
		
	public boolean isEnablePreLoadCacher() {
		return enablePreLoadCacher;
	}

	public void setEnablePreLoadCacher(boolean enablePreLoadCacher) {
		this.enablePreLoadCacher = enablePreLoadCacher;
	}

	public PostLogonResult postLogon(String hospCode, String workstoreCode) throws IOException {			
				
		SysMsgMap sysMsgMap = this.retrieveSystemMessage();
		
		if (accessControl.retrieveUserAccessControl(hospCode, workstoreCode)) {
			if (enablePreLoadCacher) {
				this.preLoadCacher();
			}				
			this.loadSessionData();
			this.buildReminderList();
			medProfileOrderProcessor.releaseProcessingMedProfiles(
					((Workstation) Contexts.getSessionContext().get("workstation")).getId(), true);
		}
		
		PostLogonResult result = new PostLogonResult();
		result.setSysMsgMap(sysMsgMap);
		return result;
	}
	
	private void preLoadCacher() {
		dmDrugCacher.getDmDrugList();
	}
	
	private void loadSessionData() {

    	Uam uam = (Uam) Component.getInstance(Uam.class, true);    	
    	
    	permissionMap = new PropMap(uam.getUamInfo().getPermissionMap());

		workstore = (Workstore) Contexts.getSessionContext().get("workstore");
		activeProfile = (OperationProfile) Contexts.getSessionContext().get("activeProfile");
		
		printerSelect = printLabelManager.retrievePrinterSelectOnLocalWorkstation();
    	Contexts.getSessionContext().set("printerSelect", printerSelect);
    	    	    	
		oneStopSpecialtyList = specialtyManager.retrieveSpecialtyCodeList();
		oneStopWardList = wardManager.retrieveWardCodeList();
		oneStopWardList.add(0, "");

		oneStopPatientCatList = patientCatListManager.retrievePatientCatList();
		patientCatCodeList = patientCatListManager.retrievePatientCatListWithSuspend();
		if (!ONESTOP_PATIENT_PATCATCODE_REQUIRED.get()) {
			oneStopPatientCatList.add(0, "");
		}
		
		List<DmMedicalOfficer> tempOneStopMedicalOfficerList = dmMedicalOfficerListManager.retrieveDmMedicalOfficerList();
		oneStopMedicalOfficerList = new ArrayList<DmMedicalOfficer>();
		DmMedicalOfficerPK dmMedicalOfficerPk = new DmMedicalOfficerPK();
		DmMedicalOfficer medicalOfficer = new DmMedicalOfficer();
		medicalOfficer.setCompId(dmMedicalOfficerPk);
		dmMedicalOfficerPk.setMedicalOfficerCode("");
		medicalOfficer.setMedicalOfficerName("");
		medicalOfficer.setRank("");
		oneStopMedicalOfficerList.add(medicalOfficer);
		
		for (DmMedicalOfficer dmMedicalOfficer : tempOneStopMedicalOfficerList) {
			oneStopMedicalOfficerList.add(dmMedicalOfficer);
		}
		
		exemptChargeReasonList = refTableManager.retrieveExemptChargeReasonList();
		vettingRefillConfig = refillConfigManager.retrieveRefillConfig();

		pivasContainerList = pivasContainerListManager.retrievePivasContainerList();
		
		ehrUid = retrieveEhrUid(identity.getCredentials().getUsername());
		
		retrieveProperties();
		retrieveFormVerbProperties();
		retrieveMpFormVerbProperties();
		retrieveRoutePrnProperties();
		
		dmDailyFrequencyListManager.retrieveDmDailyFrequencyList();
		dmSupplFrequencyListManager.retrieveDmSupplFrequencyList();
		dmSiteListManager.retrieveDmSiteList();		
		dmSiteListManager.retrieveOpTypeDmSiteList();
		dmSupplSiteListManager.retrieveDmSupplSiteList();
		dmRegimenListManager.retrieveDmRegimenList(null);		
		dmAdminTimeListManager.retrieveDmAdminTimeList(null);
		
		idleLimit = (SECURITY_IDLE_SCREEN_LOCK.get(true) ? SECURITY_IDLE_TIMEOUT.get(10) : -1);
		auditLogger.log("#1004", uam.getUamInfo().getUserId(), workstore.getWorkstoreCode());
		
		workstoreList = workstoreListManager.retrieveWorkstoreList();
		pmsPcuMappingList = qtyDurationConversionWorkstoreListManager.retrievePmsPcuMapping();
		dmRouteList = DmRouteCacher.instance().getRouteList();
		
		dmWarningList = new ArrayList<DmWarning>();
		
		List<OperationWorkstation> atdpsWorkstationList = refTableManager.getActiveAtdpsWorkstation(activeProfile);
		atdpsPickStationsNumList = new ArrayList<Integer>();
		for ( OperationWorkstation ow : atdpsWorkstationList ) {
			if ( ow.getMachine() != null ) {
				atdpsPickStationsNumList.add(ow.getMachine().getPickStation().getPickStationNum());
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private SysMsgMap retrieveSystemMessage() {

		List<SystemMessage> systemMessageList = em.createQuery(
				"select o from SystemMessage o") // 20120306 index check : none
				.getResultList();
		
		Map<String, SystemMessage> properties = new HashMap<String, SystemMessage>();
		for (SystemMessage systemMessage : systemMessageList) {
			properties.put(systemMessage.getMessageCode(), systemMessage);
		}
		
		SysMsgMap sysMsgMap = new SysMsgMap();
		sysMsgMap.setMsg(properties);
		return sysMsgMap;
	}
	
	private Boolean checkPermission(String name) {
		return Boolean.valueOf(sysProfile.isDebugEnabled() || 
				identity.hasPermission(name, "Y"));
	}
	
	private Boolean checkClusterHospitalPermission(String name) {
		if (name.equals("fmRptCluster"))
		{
			return Boolean.valueOf(sysProfile.isDebugEnabled() || (permissionMap.getValue(FM_RPT_CLUSTER_HOSP) != null &&
				permissionMap.getValue(FM_RPT_CLUSTER_HOSP).equals("Both")));
		}
		else
		{
			return Boolean.valueOf(sysProfile.isDebugEnabled() || (permissionMap.getValue(FM_RPT_CLUSTER_HOSP) != null &&
				permissionMap.getValue(FM_RPT_CLUSTER_HOSP).equals("Hospital")));
		}
	}
	
	private void retrieveProperties() {
				
		propMap.add(ACTIVE_OPERATION_NAME, activeProfile.getName());
		propMap.add(ACTIVE_OPERATION_MODE, activeProfile.getType().getDisplayValue());
		propMap.add(PERMISSION_SEND_PHARM_REMARK, this.checkPermission("sendPharmRemark").toString());	
		propMap.add(PERMISSION_ITEMAVAILABILITYMAINT_LOCAL_SUSPEND, this.checkPermission("itemAvailabilityMaintLocalSuspend").toString());	
		propMap.add(PERMISSION_ITEMAVAILABILITYMAINT_SFI, this.checkPermission("itemAvailabilityMaintSfi").toString());	
		propMap.add(PERMISSION_ITEMAVAILABILITYMAINT_SAMPLE_ITEM, this.checkPermission("itemAvailabilityMaintSampleItem").toString());		
		propMap.add(PERMISSION_ITEMAVAILABILITYMAINT_DRUG_SCOPE, this.checkPermission("itemAvailabilityMaintDrugScope").toString());
		propMap.add(PERMISSION_SPEC_MAP_SFI_MAINT_SFI, this.checkPermission("specMapSfiMaintSfi").toString());
		propMap.add(PERMISSION_SPEC_MAP_SFI_MAINT_SFI_SN, this.checkPermission("specMapSfiMaintSfiSn").toString());
		propMap.add(PERMISSION_UNVET_PRESC, this.checkPermission("unvetPresc").toString());
		propMap.add(PERMISSION_ALLOW_MODIFY, this.checkPermission("allowModify").toString());
		propMap.add(PERMISSION_ONE_STOP_ENTRY_UNCOLLECT, this.checkPermission("oneStopEntryUncollect").toString());		
		propMap.add(PERMISSION_PRINT_CDDH_RPT, this.checkPermission("printCddhRpt").toString());
		propMap.add(PERMISSION_EDIT_CDDH_REMARK, this.checkPermission("editCddhRemark").toString());
		propMap.add(PERMISSION_DISP_METHOD_CONFIG, this.checkPermission("dispMethodConfig").toString());
		propMap.add(FM_RPT_CORP, this.checkPermission("fmRptCorp").toString());
		propMap.add(FM_RPT_CLUSTER, this.checkClusterHospitalPermission("fmRptCluster").toString());
		propMap.add(FM_RPT_HOSP, this.checkClusterHospitalPermission("fmRptHospital").toString());
		propMap.add(PERMISSION_PRE_VET, this.checkPermission("preVet").toString());
		propMap.add(EHR_EHRSS_EHR_ACCESS_FAQ_URL, applicationProp.getEhrEhrssEhrAccessFaqUrl());
		propMap.add(EHR_UID, ehrUid!=null?ehrUid.toString():null);

		this.cachePropInSession();
	}
	
	@SuppressWarnings("unchecked")
	private void cachePropInSession() {
		// load all the data which need to cache in session
		List<String> propList = 
			(List<String>) em.createQuery(
					"select o.name from Prop o" + // 20121112 index check : none
					" where o.cacheInSessionFlag = :cacheInSessionFlag" +
					" order by o.name")
					.setParameter("cacheInSessionFlag", Boolean.TRUE)
					.getResultList();
		
		PropHolder propHolder = null;
		for (String propName : propList) {
			propHolder = PropHolder.getPropHolder(propName);
			if (propHolder != null) {
				propMap.add(propHolder.getPropEntity());
			}
		}		
	}
		
	private void retrieveFormVerbProperties() {
		formVerbMap = dmFormVerbMappingManager.getFormVerbProperties();
	}
	
	private void retrieveMpFormVerbProperties() {
		mpFormVerbMap = dmFormVerbMappingManager.getMpFormVerbProperties();
	}
	
	private void retrieveRoutePrnProperties() {
		List<PrnRoute> prnRouteList = prnRouteManager.getPrnRouteListByWorkstore(workstore);
		if ( prnRouteList == null ) {
			return;
		}		
		for ( PrnRoute prnRoute : prnRouteList ) {			
			prnRouteDescMap.add(DmRouteCacher.instance().getRouteByRouteCode(prnRoute.getRouteCode()).getFullRouteDesc(), "true");
		}
	}
	
	private void buildReminderList() {
		List<Prop> propList = new ArrayList<Prop>();
		reminderList = new ArrayList<String>();
		propList.addAll(corporatePropWarningList());
		propList.addAll(hospitalPropWarningList());
		propList.addAll(workstorePropWarningList());
		Collections.sort(propList, new ComparatorForPropList());
		
		for (Prop prop : propList)
		{
			String[] strArray = prop.getReminderMessage().split("\\n");
			List<String> splitList = Arrays.asList(strArray);
			for (String splitValue : splitList) 
			{     
				reminderList.add(convertPropMessage(splitValue));
			}
		}
		
		List<FollowUpWarning> followUpWarningList = followUpWarningManager.retrieveFollowUpWarningList(workstore);
		if (followUpWarningList != null && !followUpWarningList.isEmpty()) {
			reminderList.add(systemMessageManager.retrieveMessageDesc("0528"));
		}
	}
	
	public static class ComparatorForPropList implements Comparator<Prop>, Serializable {

		private static final long serialVersionUID = 1L;

		private int getSortSeq(String ruleName) {
			if (ruleName.equals(ALERT_PROFILE_ENABLED.getName()))
			{
				return 1;
			} 
			else if (ruleName.equals(ALERT_MDS_ENABLED.getName())) 
			{
				return 2;
			} 
			else if (ruleName.equals(PATIENT_MOE_ENABLED.getName()))
			{
				return 3;
			} 
			
			return 4;
		}
		
		@Override
		public int compare(Prop p1, Prop p2) {
			Integer sortSeq1 = Integer.valueOf(getSortSeq(p1.getName()));
			Integer sortSeq2 = Integer.valueOf(getSortSeq(p2.getName()));
			return sortSeq1.compareTo(sortSeq2);
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<Prop> corporatePropWarningList() {
		return em.createQuery(
				"select o.prop from CorporateProp o" + // 20130125 index check : CorporateProp.prop : UI_CORPORATE_PROP_01
				" where o.value = :value" +
                " and o.prop.reminderMessage is not null")
                .setParameter("value", "N")
                .getResultList();
    }
	
	@SuppressWarnings("unchecked")
	private List<Prop> hospitalPropWarningList() {
		return em.createQuery(
				"select o.prop from HospitalProp o" + // 20130125 index check : HospitalProp.prop : FK_HOSPITAL_PROP_02
                " where o.value = :value" +
                " and o.hospital = :hospital " +
                " and o.prop.reminderMessage is not null")
                .setParameter("value", "N")
                .setParameter("hospital", ((Workstore) Contexts.getSessionContext().get("workstore")).getHospital())
                .getResultList();
	}

	@SuppressWarnings("unchecked")
	private List<Prop> workstorePropWarningList() {
		return em.createQuery(
				"select o.prop from WorkstoreProp o" + // 20130125 index check : WorkstoreProp.prop : FK_WORKSTORE_PROP_02
                " where o.value = :value" +
                " and o.workstore = :workstore " +
                " and o.prop.reminderMessage is not null")
                .setParameter("value", "N")
                .setParameter("workstore", ((Workstore) Contexts.getSessionContext().get("workstore")))
                .getResultList();
	}
	
	private String convertPropMessage(String propMsg)
    {
          propMsg = "<font color='#FF0000'><b>" + propMsg + "</b></font>";
          return propMsg;
    }

	private Long retrieveEhrUid(String userName){
		try {
			if ( !permissionMap.getValueAsBool("ehrssViewer", false) ) {
				return null;
			}
			return ehrServiceProxy.retrieveEhrUid(userName);
		} catch (EhrException e) {
			return null;
		}
	}
	
	@Remove
	public void destroy() {
		if (permissionMap != null) {
			permissionMap = null;
		}
		if (oneStopSpecialtyList != null) {
			oneStopSpecialtyList = null;
		}
		if (oneStopWardList != null) {
			oneStopWardList = null;
		}
		if (oneStopPatientCatList != null) {
			oneStopPatientCatList = null;
		}
		if (patientCatCodeList != null) {
			patientCatCodeList = null;
		}
		if (oneStopMedicalOfficerList != null) {
			oneStopMedicalOfficerList = null;
		}
		if (exemptChargeReasonList != null) {
			exemptChargeReasonList = null;
		}
		if (vettingRefillConfig != null) {
			vettingRefillConfig = null;
		}
		if (pivasContainerList != null) {
			pivasContainerList = null;
		}
		if (propMap != null) {
			propMap = null;
		}
		if (formVerbMap != null) {
			formVerbMap = null;
		}
		if (sysProfile !=null) {
			sysProfile = null;
		}
		if (dmWarningList != null) {
			dmWarningList = null;
		}
		if ( workstoreList != null ) {
			workstoreList = null;
		}
		if ( mpFormVerbMap != null ) {
			mpFormVerbMap = null;
		}
		if ( pmsPcuMappingList != null ) {
			pmsPcuMappingList = null;
		}
		if ( dmRouteList != null ) {
			dmRouteList = null;
		}
		if ( atdpsPickStationsNumList != null ) {
			atdpsPickStationsNumList = null;
		}
	}

}
