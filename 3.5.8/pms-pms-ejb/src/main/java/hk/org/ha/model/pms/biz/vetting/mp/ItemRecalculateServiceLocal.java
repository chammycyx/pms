package hk.org.ha.model.pms.biz.vetting.mp;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;

import javax.ejb.Local;

@Local
public interface ItemRecalculateServiceLocal {
	MedProfilePoItem recalculateQty(MedProfilePoItem medProfilePoItem);
	void destroy();
}
