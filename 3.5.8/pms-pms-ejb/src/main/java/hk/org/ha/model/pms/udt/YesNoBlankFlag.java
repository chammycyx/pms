package hk.org.ha.model.pms.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum YesNoBlankFlag implements StringValuedEnum {

	Blank(" ","(Blank)"),
	Yes("Y", "Yes"),
	No("N", "No");
	
    private final String dataValue;
    private final String displayValue;

    YesNoBlankFlag(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    

    public static YesNoBlankFlag dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(YesNoBlankFlag.class, dataValue);
	}
    
	public static class Converter extends StringValuedEnumConverter<YesNoBlankFlag> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<YesNoBlankFlag> getEnumClass() {
    		return YesNoBlankFlag.class;
    	}
    }
}



