package hk.org.ha.model.pms.udt.medprofile;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum TpnRequestStatus implements StringValuedEnum {
	Initial("I", "Initial"),
	Processed("P", "Processed"),
	Deleted("D", "Deleted");
	
	private final String dataValue;
	private final String displayValue;

	private TpnRequestStatus(String dataValue,
			String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}
	
	public static TpnRequestStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(TpnRequestStatus.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<TpnRequestStatus> {
		
		private static final long serialVersionUID = 1L;

		public Class<TpnRequestStatus> getEnumClass() {
			return TpnRequestStatus.class;
		}
	}
	
}
