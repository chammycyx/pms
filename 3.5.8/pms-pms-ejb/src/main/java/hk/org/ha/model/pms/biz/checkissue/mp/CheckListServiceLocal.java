package hk.org.ha.model.pms.biz.checkissue.mp;

import hk.org.ha.model.pms.vo.checkissue.mp.DrugCheckViewListSummary;

import javax.ejb.Local;

@Local
public interface CheckListServiceLocal {
	
	DrugCheckViewListSummary retrieveDrugCheckViewListSummary();
	
	void destroy();
	
}