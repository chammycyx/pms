package hk.org.ha.model.pms.vo.alert.mds;

import hk.org.ha.model.pms.udt.alert.DiseaseType;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedProperty;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "AlertDdcm")
@ExternalizedBean(type=DefaultExternalizer.class)
public class AlertDdcm implements Alert, Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private static final char NEWLINE = '\n';

	private static final int ADDITIVE_NUM_MULTIPLIER = 10000;
	
	private static final int MANUAL_ITEM_MULTIPLIER = 10000;
	
	private static final int MANUAL_ITEM_RANGE = 100000000; //100010000

	@XmlElement(name="HospCode", required = true)
	private String hospCode;

	@XmlElement(name="OrdNo")
	private Long ordNo;
	
	@XmlElement(name="ItemNum", required = true)
	private Long itemNum;

	@XmlElement(name="DiseaseType", required = true)
	private DiseaseType diseaseType;
	
	@XmlElement(name="ConditionId")
	private String conditionId;
 
	@XmlElement(name="ConditionType")
	private Integer conditionType;

	@XmlElement(name="HitConditionId", required = true)
	private String hitConditionId;

	@XmlElement(name="ScreenMsg", required = true)
	private String screenMsg;

	@XmlElement(name="SeverityLevel", required = true)
	private Integer severityLevel;

	@XmlTransient
	private String drugName;
	
	@XmlTransient
	private Boolean suppressFlag;
	
	@XmlTransient
	private Long sortSeq;
	
	public AlertDdcm() {
		suppressFlag = Boolean.FALSE;
	}
	
	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public Long getOrdNo() {
		return ordNo;
	}

	public void setOrdNo(Long ordNo) {
		this.ordNo = ordNo;
	}

	public Long getItemNum() {
		return itemNum;
	}

	public void setItemNum(Long itemNum) {
		this.itemNum = itemNum;
	}

	public DiseaseType getDiseaseType() {
		return diseaseType;
	}

	public void setDiseaseType(DiseaseType diseaseType) {
		this.diseaseType = diseaseType;
	}

	public String getConditionId() {
		return conditionId;
	}

	public void setConditionId(String conditionId) {
		this.conditionId = conditionId;
	}

	public Integer getConditionType() {
		return conditionType;
	}

	public void setConditionType(Integer conditionType) {
		this.conditionType = conditionType;
	}

	public String getHitConditionId() {
		return hitConditionId;
	}

	public void setHitConditionId(String hitConditionId) {
		this.hitConditionId = hitConditionId;
	}

	public String getScreenMsg() {
		return screenMsg;
	}

	public void setScreenMsg(String screenMsg) {
		this.screenMsg = screenMsg;
	}

	public Integer getSeverityLevel() {
		return severityLevel;
	}

	public void setSeverityLevel(Integer severityLevel) {
		this.severityLevel = severityLevel;
	}
	
	public String getDrugName() {
		return drugName;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public Boolean getSuppressFlag() {
		return suppressFlag;
	}

	public void setSuppressFlag(Boolean suppressFlag) {
		this.suppressFlag = suppressFlag;
	}

	public Long getSortSeq() {
		return sortSeq;
	}

	public void setSortSeq(Long sortSeq) {
		this.sortSeq = sortSeq;
	}

	@ExternalizedProperty
	public String getAlertTitle() {
		if (diseaseType == DiseaseType.G6pd) {
			return "G6PD Deficiency Contraindication Checking";
		}
		else if (diseaseType == DiseaseType.Pregnancy) {
			return "Pregnancy Contraindication Checking";
		}
		else {
			return null;
		}
	}
	
	@ExternalizedProperty
	public String getAlertDesc() {
		return screenMsg;
	}
	
	public boolean equals(Object object) {
		
		if (!(object instanceof AlertDdcm)) {
			return false;
		}
		if (this == object) {
			return true;
		}

		AlertDdcm otherAlert = (AlertDdcm) object;
		
		return new EqualsBuilder()
			.append(StringUtils.trimToNull(StringUtils.lowerCase(hospCode)),       StringUtils.trimToNull(StringUtils.lowerCase(otherAlert.getHospCode())))
			.append(ordNo,                                                         otherAlert.getOrdNo())
			.append(itemNum,                                                       otherAlert.getItemNum())
			.append(diseaseType,                                                   otherAlert.getDiseaseType())
			.append(StringUtils.trimToNull(StringUtils.lowerCase(hitConditionId)), StringUtils.trimToNull(StringUtils.lowerCase(otherAlert.getHitConditionId())))
			.append(StringUtils.trimToNull(StringUtils.lowerCase(screenMsg)),      StringUtils.trimToNull(StringUtils.lowerCase(otherAlert.getScreenMsg())))
			.append(severityLevel,                                                 otherAlert.getSeverityLevel())
			.isEquals();
	}
	
	public int hashCode() {
		return new HashCodeBuilder()
			.append(StringUtils.trimToNull(StringUtils.lowerCase(hospCode)))
			.append(ordNo)
			.append(itemNum)
			.append(diseaseType)
			.append(StringUtils.trimToNull(StringUtils.lowerCase(hitConditionId)))
			.append(StringUtils.trimToNull(StringUtils.lowerCase(screenMsg)))
			.append(severityLevel)
			.toHashCode();
	}

	public void loadDrugDesc(String drugDesc) {
	}

	public void replaceItemNum(Integer oldItemNum, Integer newItemNum, String orderNum) {
		itemNum = Long.valueOf(newItemNum.intValue());
	}

	public void replaceOrderNum(String oldOrderNum, String newOrderNum) {
		hospCode = StringUtils.trimToNull(newOrderNum.substring(0, 3));
		ordNo = Long.valueOf(newOrderNum.substring(3));		
	}

	public void restoreItemNum() {
		if( itemNum.longValue() > MANUAL_ITEM_RANGE ){
			itemNum = Long.valueOf(itemNum.longValue() / MANUAL_ITEM_MULTIPLIER);
		}else if (itemNum.longValue() >= ADDITIVE_NUM_MULTIPLIER) {
			itemNum = Long.valueOf(itemNum.longValue() / ADDITIVE_NUM_MULTIPLIER);
		}
	}

	@ExternalizedProperty
	public String getKeys() {
		return AlertDdcm.class.getName()
				+ NEWLINE + diseaseType.getDisplayValue()
				+ NEWLINE + StringUtils.trimToEmpty(StringUtils.lowerCase(hitConditionId))
				+ NEWLINE + StringUtils.trimToEmpty(StringUtils.lowerCase(screenMsg))
				+ NEWLINE + severityLevel.toString();
	}
}