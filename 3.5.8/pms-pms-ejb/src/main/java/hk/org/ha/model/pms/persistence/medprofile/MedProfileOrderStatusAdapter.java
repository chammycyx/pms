package hk.org.ha.model.pms.persistence.medprofile;

import hk.org.ha.model.pms.udt.medprofile.MedProfileOrderStatus;

import java.io.Serializable;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class MedProfileOrderStatusAdapter extends XmlAdapter<String, MedProfileOrderStatus> implements Serializable {

	private static final long serialVersionUID = 1L;

	public String marshal( MedProfileOrderStatus v ) {		
		return (v == null) ? null : v.getDataValue();
	}
	
	public MedProfileOrderStatus unmarshal( String v ) {		
		return MedProfileOrderStatus.dataValueOf(v);
	}
}