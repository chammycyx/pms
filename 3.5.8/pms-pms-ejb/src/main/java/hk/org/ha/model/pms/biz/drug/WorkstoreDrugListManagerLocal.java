package hk.org.ha.model.pms.biz.drug;
import java.util.List;

import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.model.pms.vo.rx.DhRxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;

import javax.ejb.Local;

@Local
public interface WorkstoreDrugListManagerLocal {

	void retrieveWorkstoreDrugListByDisplayName(String displayName);
	void retrieveWorkstoreDrugListByDrugKey(Integer drugKey, boolean ignoreDrugScope);
	void retrieveWorkstoreDrugList(String prefixItemCode, boolean showNonSuspendItemOnly, boolean fdn);
	void retrieveWorkstoreDrugListByRxItem(RxItem rxItem, Boolean displayNameOnly);
	void retrieveWorkstoreDrugListByDhRxDrug(DhRxDrug dhRxDrug, String itemCodePrefix);	
	List<WorkstoreDrug> getWorkstoreDrugListByRxItem( RxItem rxItem, Boolean displayNameOnly );
	List<WorkstoreDrug> getWorkstoreDrugListByDrugKey( Integer drugKey, boolean ignoreDrugScope );
	Pair<List<WorkstoreDrug>, DmDrug> getWorkstoreDrugListDmDrugByDhRxDrug(DhRxDrug dhRxDrug);	
}
