package hk.org.ha.model.pms.biz.delivery;

import static hk.org.ha.model.pms.prop.Prop.DELIVERY_SCHEDULE_TEMPLATE;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryRequest;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryRequestGroup;
import hk.org.ha.model.pms.persistence.medprofile.DeliverySchedule;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryScheduleItem;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.persistence.reftable.WorkstoreProp;
import hk.org.ha.model.pms.udt.RecordStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryRequestGroupStatus;
import hk.org.ha.model.pms.vo.delivery.ScheduleTemplateInfo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.joda.time.DateMidnight;


@AutoCreate
@Stateless
@Name("deliveryScheduleManager")
@MeasureCalls
public class DeliveryScheduleManagerBean implements DeliveryScheduleManagerLocal {		
	@Logger
	private Log logger;
	
	@In
	private AuditLogger auditLogger;
	
	@PersistenceContext
	private EntityManager em;
	
	private DateFormat timeFormat = new SimpleDateFormat("HH:mm");
        
	@SuppressWarnings("unchecked")
	@Override
    public List<DeliverySchedule> retrieveDeliveryScheduleList(Hospital hospital) {
    	
    	return em.createQuery(
    			"select d from DeliverySchedule d" + // 20171023 index check : DeliverySchedule.hospital : FK_DELIVERY_SCHEDULE_01
    			" where d.hospital = :hospital" +
    			" and d.status = :status" +
    			" order by d.name, d.id")
    			.setParameter("hospital", hospital)
    			.setParameter("status", RecordStatus.Active)
    			.getResultList();
    }
	
	
	@SuppressWarnings("unchecked")
	public DeliverySchedule retrieveDeliveryScheduleTemplate(Workstore workstore, ScheduleTemplateInfo templateInfo) {
		WorkstoreProp defaultProp = retrieveWorkstoreDeliveryScheduleTemplate(workstore, false);
		
		if( defaultProp.getValue() == null) {
			templateInfo.setEmptyDefaultTemplateNameFlag(true);
			return null;
		}
		
		List<DeliverySchedule> scheduleTemplateList = (List<DeliverySchedule>) em.createQuery(
										    			"select d from DeliverySchedule d" + // 20171023 index check : DeliverySchedule.hospital,name : I_DELIVERY_SCHEDULE_01
										    			" where d.name = :name" +
										    			" and d.hospital = :hospital" +
										    			" and d.status = :status" +
										    			" order by d.id desc")
										    			.setParameter("name", defaultProp.getValue())
										    			.setParameter("hospital", workstore.getHospital())
										    			.setParameter("status", RecordStatus.Active)
										    			.getResultList();
    	
    	if( !scheduleTemplateList.isEmpty() ){
    		return scheduleTemplateList.get(0);    		
    	}
    	return null;
	}
	
	private WorkstoreProp retrieveWorkstoreDeliveryScheduleTemplate(Workstore workstore, boolean pessimisticLock){
		if (workstore == null) {
    		return null;
    	}  
    	
    	try{
    		Query query =  em.createQuery("select o from WorkstoreProp o" + // 20171023 index check : WorkstoreProp.workstore,prop : UI_WORKSTORE_PROP_01
								    		" where o.workstore = :workstore" +
							    			" and o.prop.name = :name")
							    			.setParameter("workstore", workstore)
							    			.setParameter("name", DELIVERY_SCHEDULE_TEMPLATE.getName());
    		
			if( pessimisticLock ){
				query.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock);
			}
			return (WorkstoreProp) query.getSingleResult();
    	}catch(Exception ex){
    		logger.error("No WorkstoreProp Record DELIVERY_SCHEDULE_TEMPLATE for Workstore[#0,#1]",  ex, workstore.getHospCode(), workstore.getWorkstoreCode());
    		return null;
    	}
	}
	
    @Override
    public void setDeliveryScheduleTemplate(Workstore workstore, String templateName, boolean resetValue)
    {
    	if (workstore == null) {
    		return;
    	}  
    	
    	WorkstoreProp prop = retrieveWorkstoreDeliveryScheduleTemplate(workstore, true);
    	
    	String previousPropValue = prop.getValue();
    	
    	if( resetValue ){
    		if( new DateMidnight().compareTo(new DateMidnight(prop.getUpdateDate())) > 0 ){
    			prop.setValue(templateName);
    			prop.setUpdateDate(new Date());
    			em.merge(prop);
    	    	auditLogger.log("#1050",previousPropValue, templateName);
    		}else if( previousPropValue == null && templateName != null ){
    			prop.setValue(templateName);
    			prop.setUpdateDate(new Date());
    			em.merge(prop);
    			auditLogger.log("#1050",previousPropValue, templateName);
    		}
    	}else{
    		prop.setValue(templateName);
    		prop.setUpdateDate(new Date());
    		em.merge(prop);
    		auditLogger.log("#1051",previousPropValue, templateName);
    	}
    }
    
    public List<DeliveryScheduleItem> retrieveDeliveryScheduleItemList(Workstore workstore, DeliverySchedule deliverySchedule){
    	DeliverySchedule latestDeliverySchedule = em.find(DeliverySchedule.class, deliverySchedule.getId());
    	if( latestDeliverySchedule != null && latestDeliverySchedule.getStatus() == RecordStatus.Active ){
    		
    		List<DeliveryScheduleItem> latestDeliveryScheduleItemList = retrieveDeliveryScheduleItemList(latestDeliverySchedule);
    		
    		Map<Long, DeliveryScheduleItem> requestGroupInfoMap = retrieveTodayDeliveryRequestGroupInfo(workstore, latestDeliveryScheduleItemList);
    		
    		for( DeliveryScheduleItem deliveryScheduleItem : latestDeliveryScheduleItemList ){
    			
    			deliveryScheduleItem.setScheduleTimeStr(StringUtils.leftPad(deliveryScheduleItem.getScheduleTime().toString(), 4, "0"));
    			
    			DeliveryScheduleItem requestDsi = requestGroupInfoMap.get(deliveryScheduleItem.getId());  				
    			
    			if( requestDsi != null ){
    				deliveryScheduleItem.setDeliveryRequestCreateDateStr(requestDsi.getDeliveryRequestCreateDateStr());
    				deliveryScheduleItem.setDeliveryRequestCreateUser(requestDsi.getDeliveryRequestCreateUser());
    				deliveryScheduleItem.setDeliveryRequestWorkstation(requestDsi.getDeliveryRequestWorkstation());
    				deliveryScheduleItem.setMultiDeliveryFlag(requestDsi.getMultiDeliveryFlag());
    			}
    		}
    		return latestDeliveryScheduleItemList;
    	}
    	return null;
    }
    
    @SuppressWarnings("unchecked")
	private List<DeliveryScheduleItem> retrieveDeliveryScheduleItemList(DeliverySchedule deliverySchedule){
    	List<DeliveryScheduleItem> scheduleTemplateList = (List<DeliveryScheduleItem>) em.createQuery(
			"select d from DeliveryScheduleItem d" + // 20171023 index check : DeliveryScheduleItem.deliverySchedule : FK_DELIVERY_SCHEDULE_ITEM_01
			" where d.deliverySchedule = :deliverySchedule" +
			" and d.status = :status" +
			" order by d.id desc")
			.setParameter("deliverySchedule", deliverySchedule)
			.setParameter("status", RecordStatus.Active)
			.getResultList();
    	
    	return scheduleTemplateList;
    }
    
	public Map<Long, DeliveryScheduleItem> retrieveTodayDeliveryRequestGroupInfo(Workstore workstore, List<DeliveryScheduleItem> deliveryScheduleItemList){
		List<Long> deliveryScheduleItemIdList = new ArrayList<Long>();
		Map<Long, DeliveryScheduleItem> deliveryScheduleItemMap = new HashMap<Long, DeliveryScheduleItem>();
		Map<String, String> workstationCodeMap = null;
		
		for( DeliveryScheduleItem deliveryScheduleItem : deliveryScheduleItemList ){
			deliveryScheduleItemIdList.add(deliveryScheduleItem.getId());
			deliveryScheduleItemMap.put(deliveryScheduleItem.getId(), deliveryScheduleItem);
		}
		
		Map<Long, List<DeliveryRequestGroup>> dsiRequestGroupMap = buildDeliveryScheduleItemRequestGroupMap(workstore, deliveryScheduleItemIdList);
    		
		for(Long dsiId: dsiRequestGroupMap.keySet()){
			List<DeliveryRequestGroup> dsiGroupList = dsiRequestGroupMap.get(dsiId);
			DeliveryScheduleItem updateDsi = deliveryScheduleItemMap.get(dsiId);
			
			if( dsiGroupList.size() > 1 ){
				updateDsi.setMultiDeliveryFlag(Boolean.TRUE);
			}
			
			for( DeliveryRequestGroup deliveryRequestGroup : dsiGroupList ){
				if( deliveryRequestGroup.getStatus() == DeliveryRequestGroupStatus.Active ){
					updateDsi.setDeliveryRequestCreateDateStr(timeFormat.format(deliveryRequestGroup.getCreateDate()));
					updateDsi.setDeliveryRequestCreateUser(deliveryRequestGroup.getCreateUser());
					
					List<DeliveryRequest> deliveryRequestList = deliveryRequestGroup.getDeliveryRequestList();
					
    				if( deliveryRequestList != null && deliveryRequestList.size() > 0 ){
    					if( workstationCodeMap == null ){
    						workstationCodeMap = buildWorkstationCodeToHostnameMap(workstore);
    					}
    					updateDsi.setDeliveryRequestWorkstation(workstationCodeMap.get(deliveryRequestList.get(0).getWorkstationCode()));
    				}
    				deliveryScheduleItemMap.put(updateDsi.getId(), updateDsi);
				}
			}
		}
    	return deliveryScheduleItemMap;
    }
    
    @SuppressWarnings("unchecked")
	private Map<Long, List<DeliveryRequestGroup>> buildDeliveryScheduleItemRequestGroupMap(Workstore workstore, List<Long> dsiIdList){
    	Map<Long, List<DeliveryRequestGroup>> dsiRequestGroupMap = new HashMap<Long, List<DeliveryRequestGroup>>();
    	
    	List<DeliveryRequestGroup> deliveryRequestGroupList = em.createQuery(
			"select g from DeliveryRequestGroup g" + // 20171023 index check : DeliveryRequestGroup.workstore,createDate : I_DELIVERY_REQUEST_GROUP_01
			" where g.workstore = :workstore" +		  			
			" and g.createDate between :fromDate and :toDate" +
			" and g.deliveryScheduleItem.id in :deliveryScheduleItemIdList" +
			" order by g.deliveryScheduleItem.id")			
			.setParameter("workstore", workstore)
			.setParameter("fromDate", new DateMidnight().toDate())
			.setParameter("toDate", new DateMidnight().plusDays(1).toDate())
			.setParameter("deliveryScheduleItemIdList", dsiIdList)
			.getResultList();
    	
    	if( !deliveryRequestGroupList.isEmpty() ){
    		for( DeliveryRequestGroup deliveryRequestGroup : deliveryRequestGroupList ){
    			List<DeliveryRequestGroup>  newGroupList = new ArrayList<DeliveryRequestGroup>();
				
    			if( dsiRequestGroupMap.get(deliveryRequestGroup.getDeliveryScheduleItemId()) != null ){
    				newGroupList = dsiRequestGroupMap.get(deliveryRequestGroup.getDeliveryScheduleItemId());
    			}
    			newGroupList.add(deliveryRequestGroup);
				dsiRequestGroupMap.put(deliveryRequestGroup.getDeliveryScheduleItemId(), newGroupList);
    		}
    	}
    	return dsiRequestGroupMap;
    }
    
	@SuppressWarnings("unchecked")
	private Map<String, String> buildWorkstationCodeToHostnameMap(Workstore workstore){
    	Map<String, String> workstationCodeMap = null;
    	
		List<Workstation> workstationList = em.createQuery(
													"select o from Workstation o" + // 20120303 index check : Workstation.workstore : FK_WORKSTATION_01
													" where o.workstore = :workstore" +
													" order by o.workstationCode")
													.setParameter("workstore", workstore)
													.getResultList();
    	
    	if( !workstationList.isEmpty() ){
    		workstationCodeMap = new HashMap<String, String>();
    		
    		for(Workstation workstation: workstationList){
    			workstationCodeMap.put(workstation.getWorkstationCode(), workstation.getHostName());
    		}
    	}
    	return workstationCodeMap;
    }
	
    @Remove
	@Override
	public void destroy() {
	}
}
