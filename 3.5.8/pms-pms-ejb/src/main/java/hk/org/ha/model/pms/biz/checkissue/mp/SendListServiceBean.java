package hk.org.ha.model.pms.biz.checkissue.mp;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.Delivery;
import hk.org.ha.model.pms.udt.medprofile.DeliveryStatus;
import hk.org.ha.model.pms.vo.checkissue.mp.SentDelivery;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;

@Stateful
@Scope(ScopeType.SESSION)
@Name("sendListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SendListServiceBean implements SendListServiceLocal {
	
	@PersistenceContext
	private EntityManager em;

	@In
	private Workstore workstore;
	
	@Out(required = false)
	private List<SentDelivery> sentDeliveryList;
	
	@SuppressWarnings("unchecked")
	public void retrieveSentList() {

		DateTime toDate = new DateTime();
		DateMidnight fromDate = toDate.toDateMidnight();
		
		sentDeliveryList = new ArrayList<SentDelivery>();
		List<Delivery> deliveryList = em.createQuery(
				"select o from Delivery o" + // 20120317 index check : Delivery.hospital,deliveryDate : I_DELIVERY_03
				" where o.hospital = :hospital" +
				" and o.deliveryDate between :fromDate and :toDate" +
				" and o.status = :status" +
				" order by o.deliveryDate desc")
				.setParameter("hospital", workstore.getHospital())
				.setParameter("fromDate", fromDate.toDate())
				.setParameter("toDate", toDate.toDate())
				.setParameter("status", DeliveryStatus.Delivered)
				.getResultList();
		
		if ( !deliveryList.isEmpty() ) {
			for ( Delivery delivery : deliveryList ) {
				SentDelivery sentDelivery = new SentDelivery();
				sentDelivery.setDeliveryId(delivery.getId());
				sentDelivery.setBatchDate(delivery.getBatchDate());
				sentDelivery.setBatchNum(delivery.getBatchNum());
				sentDelivery.setWardCode(delivery.getWardCode());
				sentDelivery.setDeliveryDate(delivery.getDeliveryDate());
				sentDelivery.setPrintMode(delivery.getPrintMode());
				sentDelivery.setStatus(delivery.getStatus().getDisplayValue());
				sentDeliveryList.add(sentDelivery);
			}
		}
	}
	
	@Remove
	public void destroy() {
		if ( sentDeliveryList != null ) {
			sentDeliveryList = null;
		}
	}

}
