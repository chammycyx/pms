package hk.org.ha.model.pms.biz.vetting;

import static hk.org.ha.model.pms.prop.Prop.VETTING_CAPD_VOUCHER_ITEM_MAX;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.phs.CapdSupplierItem;
import hk.org.ha.model.pms.udt.RecordStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.vo.rx.CapdItem;
import hk.org.ha.model.pms.vo.rx.CapdRxDrug;
import hk.org.ha.model.pms.vo.vetting.CapdManualVoucher;
import hk.org.ha.model.pms.vo.vetting.CapdSplit;
import hk.org.ha.model.pms.vo.vetting.CapdSplitItem;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("capdSplitService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CapdSplitServiceBean implements CapdSplitServiceLocal {

	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	 
	@Out(required = false)
	private CapdSplit capdSplit;
	
	@In
	private PharmOrder pharmOrder;

	private final static char SPACE = ' '; 

	private static final String JAXB_CONTEXT_VETTING = "hk.org.ha.model.pms.vo.vetting";	
	
	private String errMsg;
	private String errItemCode;
	
	public void retrieveCapdSplit(){
		errMsg = "";
		if ( pharmOrder == null ) {
			return;
		}
		
		pharmOrder.loadChild();
		
		MedOrder medOrder = pharmOrder.getMedOrder();
		
		pharmOrder.loadCapdSplit();
		
		Set<String> supplierSet = new TreeSet<String>();
		
		if ( pharmOrder.getCapdSplit() != null){
			capdSplit = pharmOrder.getCapdSplit();
		} else {
			capdSplit = new CapdSplit();
			capdSplit.setCapdManualVoucherList(new ArrayList<CapdManualVoucher>());
			capdSplit.getCapdManualVoucherList().add(new CapdManualVoucher());
		}
		
		removeCapdSplitItemByPharmOrderOrgItemNum(capdSplit);

		List<CapdSplitItem> delCapdSplitItemList = new ArrayList<CapdSplitItem>();
		List<CapdSplitItem> newCapdSplitItemList = new ArrayList<CapdSplitItem>();
		
		for ( MedOrderItem medOrderItem : medOrder.getMedOrderItemList() ) {
			if ( medOrderItem.getCapdRxDrug() == null || medOrderItem.getPharmOrderItemList().size() == 0 ) {
				continue;
			}
			CapdRxDrug capdRxDrug = medOrderItem.getCapdRxDrug();
			List<PharmOrderItem> pharmOrderItemList = medOrderItem.getPharmOrderItemList();

			if (medOrderItem.getStatus().equals(MedOrderItemStatus.SysDeleted) ||
				medOrderItem.getStatus().equals(MedOrderItemStatus.Deleted) ||
				!medOrderItem.getActionStatus().equals(ActionStatus.DispByPharm)) {
				for (CapdSplitItem capdSplitItem : capdSplit.getCapdSplitItemList() ) {
					if ( capdSplitItem.getMedOrderItemNum().equals(medOrderItem.getItemNum())) {
						delCapdSplitItemList.add(capdSplitItem);
					}
				}
			} else if ( capdSplit.getCapdSplitItemList().size()==0 || capdRxDrug.getUpdateGroupNumFlag() ) {
				for (CapdSplitItem capdSplitItem : capdSplit.getCapdSplitItemList() ) {
					if ( capdSplitItem.getMedOrderItemNum().equals(medOrderItem.getItemNum())) {
						capdSplitItem.setGenVoucherFlag(Boolean.TRUE);
						delCapdSplitItemList.add(capdSplitItem);
					}
				}
				int capdItemNum = 0;
				for ( CapdItem capdItem : capdRxDrug.getCapdItemList() ) {
					PharmOrderItem pharmOrderItem = pharmOrderItemList.get(capdItemNum);
					CapdSplitItem capdSplitItem = new CapdSplitItem();
					capdSplitItem.setItemNum(pharmOrderItem.getOrgItemNum());
					capdSplitItem.setGenVoucherFlag(Boolean.TRUE);
					capdSplitItem.setPrintFlag(Boolean.TRUE);
					capdSplitItem.setMedOrderItemNum(medOrderItem.getItemNum());
					
					if ( StringUtils.isBlank(capdItem.getSupplierCode()) ){
						CapdSupplierItem capdSupplierItem = em.find(CapdSupplierItem.class, capdItem.getItemCode());
						if ( capdSupplierItem != null && capdSupplierItem.getStatus() == RecordStatus.Active && capdSupplierItem.getSupplier().getStatus() == RecordStatus.Active) {
							capdItem.setSupplierCode(capdSupplierItem.getSupplier().getSupplierCode());
						} else {
							errMsg = "0700";
							errItemCode = capdItem.getItemCode();
							return;
						}
					}

					capdSplitItem.setSupplierCode(capdItem.getSupplierCode());
					supplierSet.add(capdItem.getSupplierCode());
					capdSplitItem.setMedOrderItemNum(medOrderItem.getItemNum());
					capdSplitItem.setOrderQty(pharmOrderItem.getCalQty());
					capdSplitItem.setBaseUnit(capdItem.getBaseUnit());
					capdSplitItem = this.setCapdSplitItemQty(capdSplitItem, pharmOrderItem.getIssueQty().intValue());
					capdSplitItem.setGroupNum(null);
					capdSplitItem.setItemCode(capdItem.getItemCode());
					capdSplitItem.setFullDrugDesc(getCapdDrugDesc(pharmOrderItem));
					newCapdSplitItemList.add(capdSplitItem);
					capdItemNum++;
				}
			} else {
				//check dispQty
				for (CapdSplitItem capdSplitItem : capdSplit.getCapdSplitItemList() ) {
					if ( !capdSplitItem.getMedOrderItemNum().equals(medOrderItem.getItemNum())) {
						continue;
					}
					PharmOrderItem pharmOrderItem = pharmOrder.getPharmOrderItemByOrgItemNum(capdSplitItem.getItemNum());
					if ( !capdSplitItem.getDispQty().equals(pharmOrderItem.getIssueQty().intValue()) ) {
						capdSplitItem = setCapdSplitItemQty(capdSplitItem, pharmOrderItem.getIssueQty().intValue());
					}
				}
			}
		}
		
		
		for ( CapdSplitItem delCapdSplitItem : delCapdSplitItemList ) {
			capdSplit.getCapdSplitItemList().remove(delCapdSplitItem);
		}
		
		for ( CapdSplitItem newCapdSplitItem : newCapdSplitItemList ) {
			capdSplit.getCapdSplitItemList().add(newCapdSplitItem);
		}

		int groupNum = getMaxGroupNum(capdSplit)+1;
		int assignGroupNum = 1;
		
		for (String supplierCode : supplierSet) { 
			for (CapdSplitItem capdSplitItem : capdSplit.getCapdSplitItemList()) {
				if (StringUtils.equals(supplierCode, capdSplitItem.getSupplierCode()) && capdSplitItem.getGroupNum() == null ) {
					if (assignGroupNum > VETTING_CAPD_VOUCHER_ITEM_MAX.get(3)){
						groupNum++;
						assignGroupNum = 1;
					}
					
					capdSplitItem.setGroupNum(groupNum);
					assignGroupNum++;
				}
			}
			groupNum++;
			assignGroupNum = 1;
		}
		
		capdSplit = sortCapdSplitItemList(capdSplit);
		
	}
	
	private String getCapdDrugDesc(PharmOrderItem pharmOrderItem){
		StringBuilder sb = new StringBuilder();

		sb.append(DmDrugCacher.instance().getDmDrug(pharmOrderItem.getItemCode()).getDrugName());

		if (StringUtils.isNotBlank(pharmOrderItem.getConnectSystem())) {
			sb.append(SPACE);
			sb.append(pharmOrderItem.getConnectSystem());
		}

		return sb.toString();
	}
	
	private CapdSplitItem setCapdSplitItemQty(CapdSplitItem capdSplitItem, Integer dispQty){
		capdSplitItem.setDispQty(dispQty);
		capdSplitItem.setGenVoucherFlag(Boolean.TRUE);
		capdSplitItem.setQty01(dispQty);
		capdSplitItem.setQty02(0);
		capdSplitItem.setQty03(0);
		capdSplitItem.setQty04(0);
		capdSplitItem.setQty05(0);
		capdSplitItem.setQty06(0);
		capdSplitItem.setQty07(0);
		capdSplitItem.setQty08(0);
		capdSplitItem.setQty09(0);
		capdSplitItem.setQty10(0);
		return capdSplitItem;
	}
	
	private int getMaxGroupNum(CapdSplit capdSplit){
		int maxGroupNum = 0;
		for ( CapdSplitItem capdSplitItem : capdSplit.getCapdSplitItemList() ) {
			if( capdSplitItem.getGroupNum() != null && maxGroupNum < capdSplitItem.getGroupNum()) {
				maxGroupNum = capdSplitItem.getGroupNum();
			}
		}
		return maxGroupNum;
	}
	
	@SuppressWarnings("unchecked")
	private CapdSplit sortCapdSplitItemList(CapdSplit capdSplit){
		Collections.sort(capdSplit.getCapdSplitItemList(), new Comparator(){
			public int compare(Object o1, Object o2) {
				CapdSplitItem item1 = (CapdSplitItem) o1;
				CapdSplitItem item2 = (CapdSplitItem) o2;
				return item1.getItemNum().compareTo(item2.getItemNum());
			}
		});	
		
		Collections.sort(capdSplit.getCapdSplitItemList(), new Comparator(){
			public int compare(Object o1, Object o2) {
				CapdSplitItem item1 = (CapdSplitItem) o1;
				CapdSplitItem item2 = (CapdSplitItem) o2;
				return item1.getGroupNum().compareTo(item2.getGroupNum());
			}
		});
		return capdSplit;
	}
	
	public void updateCapdSplit(CapdSplit updateCapdSplit, boolean setGenNewVoucherFlag){
		MedOrder medOrder = pharmOrder.getMedOrder();
		
		for ( CapdSplitItem capdSplitItem : updateCapdSplit.getCapdSplitItemList() ) {
			if ( !capdSplitItem.getPrintFlag() ) {
				capdSplitItem.setGroupNum(0);
			}
		}
		
		if ( setGenNewVoucherFlag ) {
			CapdSplit orgCapdSplit = null;
			if ( pharmOrder.getCapdSplitXml() != null ){
				JaxbWrapper<CapdSplit> vettingJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_VETTING);
				orgCapdSplit = vettingJaxbWrapper.unmarshall(pharmOrder.getCapdSplitXml());
				updateCapdSplit = compareCapdSplit(updateCapdSplit, orgCapdSplit);
			}
		}

		pharmOrder.setCapdSplit(updateCapdSplit);
		
		for ( MedOrderItem medOrderItem : medOrder.getMedOrderItemList() ) {
			if ( medOrderItem.getCapdRxDrug() != null ) {
				medOrderItem.getCapdRxDrug().setUpdateCapdItemQty(Boolean.FALSE);
				medOrderItem.getCapdRxDrug().setUpdateGroupNumFlag(Boolean.FALSE);
			}
		}
	}
	
	private CapdSplit compareCapdSplit(CapdSplit capdSplit, CapdSplit orgCapdSplit){		
		capdSplit = sortCapdSplitItemList(capdSplit);
		
		if ( orgCapdSplit != null ) {	
			Set<Integer> groupNumList = new HashSet<Integer>();
			
			//reset GenVoucherFlag
			for ( CapdSplitItem capdSplitItem: capdSplit.getCapdSplitItemList() ) {
				capdSplitItem.setGenVoucherFlag(Boolean.FALSE);
			}
			
			Integer curGroupNum = 0;
			for ( CapdSplitItem capdSplitItem : capdSplit.getCapdSplitItemList() ) {
				if ( curGroupNum == 0 || !curGroupNum.equals(capdSplitItem.getGroupNum()) ) {
					curGroupNum = capdSplitItem.getGroupNum();
					if (capdSplitItem.getGenVoucherFlag()){
						continue;
					}
					
					List<CapdSplitItem> newCapdSplitItemList = getCapdSplitItemListByGroup(capdSplit, curGroupNum);
					CapdSplitItem orgCapdSplitItem = retrieveCapdSplitItemByItemNum(orgCapdSplit.getCapdSplitItemList(), newCapdSplitItemList.get(0).getItemNum());
					
					if ( orgCapdSplitItem == null ) {
						groupNumList.add(curGroupNum);
						continue;
					}

					
					List<CapdSplitItem> orgCapdSplitItemList = getCapdSplitItemListByGroup(orgCapdSplit, orgCapdSplitItem.getGroupNum());
					if ( newCapdSplitItemList.size() != orgCapdSplitItemList.size() ){
						groupNumList.add(curGroupNum);
						continue;
					} 
					
					for ( CapdSplitItem newCapdSplitItem: newCapdSplitItemList ) {
						orgCapdSplitItem = retrieveCapdSplitItemByItemNum(orgCapdSplitItemList, newCapdSplitItem.getItemNum());
						if (orgCapdSplitItem==null) {
							groupNumList.add(curGroupNum);
							break;
						}
					}
				}
			}
			
			for (Integer groupNum : groupNumList) {
				setGroupPrintVoucherFlag(capdSplit, groupNum);
			}
			
			groupNumList = new HashSet<Integer>();
			
			for ( CapdSplitItem capdSplitItem: capdSplit.getCapdSplitItemList() ) {
				if ( capdSplitItem.getGenVoucherFlag() ) {
					continue;
				}
				
				CapdSplitItem orgCapdSplitItem = retrieveCapdSplitItemByItemNum(orgCapdSplit.getCapdSplitItemList(), capdSplitItem.getItemNum());

				if ( orgCapdSplitItem != null ){
					if ( !isEqualCapdSplitItem(orgCapdSplitItem, 
												capdSplitItem, 
												orgCapdSplit.getCapdManualVoucherList().get(0), 
												capdSplit.getCapdManualVoucherList().get(0)) ) {
						capdSplitItem.setMarkUpdate(true);
					} else {
						capdSplitItem.setMarkUpdate(false);
					}
				} else {
					//new CapdSplitItem
					capdSplitItem.setMarkUpdate(true);
				}

				if ( !capdSplitItem.getPrintFlag() ) {
					capdSplitItem.setGroupNum(0);
				}
				
				if ( capdSplitItem.isMarkUpdate() ){
					groupNumList.add(capdSplitItem.getGroupNum());
				}
			}

			for (Integer groupNum : groupNumList) {
				setGroupPrintVoucherFlag(capdSplit, groupNum);
			}
			
		}
		
		return capdSplit;
		
	}
	
	private List<CapdSplitItem> getCapdSplitItemListByGroup(CapdSplit capdSplit, Integer groupNum){
		List<CapdSplitItem> capdSplitItemList = new ArrayList<CapdSplitItem>();
		for ( CapdSplitItem capdSplitItem: capdSplit.getCapdSplitItemList() ) {
			if ( groupNum.equals(capdSplitItem.getGroupNum()) ){
				capdSplitItemList.add(capdSplitItem);
			} else if (capdSplitItem.getGroupNum() > groupNum){
				break;
			}
		}
		return capdSplitItemList;
	}
	
	private CapdSplitItem retrieveCapdSplitItemByItemNum(List<CapdSplitItem> capdSplitItemList, Integer itemNum){
		for ( CapdSplitItem capdSplitItem : capdSplitItemList ) {
			if (capdSplitItem.getItemNum().equals(itemNum)){
				return capdSplitItem;
			}
		}
		return null;
	}
	
	private String retrieveCapdManualVoucherNum(CapdManualVoucher capdManualVoucher, Integer qtyNum) {
		try {
			if ( qtyNum < 10 ) {
				return (String)PropertyUtils.getProperty(capdManualVoucher, "num0"+qtyNum);
			} else {
				return (String)PropertyUtils.getProperty(capdManualVoucher, "num"+qtyNum);
			}
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return "";
	}
	
	private void setGroupPrintVoucherFlag(CapdSplit capdSplit, Integer groupNum){
		for ( CapdSplitItem capdSplitItem : capdSplit.getCapdSplitItemList() ) {
			if ( capdSplitItem.getGroupNum().equals(groupNum) ) {
				capdSplitItem.setGenVoucherFlag(Boolean.TRUE);
			}else if ( capdSplitItem.getGroupNum() > groupNum ){
				break;
			}
		}
	}
	
	private boolean isEqualCapdSplitItem(CapdSplitItem orgCapdSplitItem, CapdSplitItem newCapdSplitItem, CapdManualVoucher orgCapdManualVoucher, CapdManualVoucher capdManualVoucher){
		boolean isEquals = true;
		
		if (!orgCapdSplitItem.getDispQty().equals(newCapdSplitItem.getDispQty())){
			isEquals = false;
		}

		if (!orgCapdSplitItem.getPrintFlag().equals(newCapdSplitItem.getPrintFlag())){
			isEquals = false;
		}
		
		if ( isEquals ) {
			String col = "";
			for (int i=1;i<=10;i++){
				if (i<10){
					col = "qty0";
				} else {
					col = "qty";
				}
				try {
					Integer orgQty = (Integer) PropertyUtils.getProperty(orgCapdSplitItem, col+i);
					Integer newQty = (Integer) PropertyUtils.getProperty(newCapdSplitItem, col+i);
					if ( !orgQty.equals(newQty)){
						isEquals = false;
						break;
					}
					
					if ( newQty > 0 ) {
						String orgCapdManualVoucherNum = retrieveCapdManualVoucherNum(orgCapdManualVoucher, i); 
						String capdManualVoucherNum = retrieveCapdManualVoucherNum(capdManualVoucher, i); 
						if ( !StringUtils.equals(capdManualVoucherNum, orgCapdManualVoucherNum) ) {
							isEquals = false;
							break;
						} 
					}
				} catch (IllegalAccessException e) {
					isEquals = false;
				} catch (InvocationTargetException e) {
					isEquals = false;
				} catch (NoSuchMethodException e) {
					isEquals = false;
				}
			}
		}

		
		return isEquals;
	}
	
	private void removeCapdSplitItemByPharmOrderOrgItemNum(CapdSplit capdSplit){
		for (Iterator<CapdSplitItem> itr = capdSplit.getCapdSplitItemList().iterator();itr.hasNext();) {
			CapdSplitItem capdSplitItem = itr.next();
			PharmOrderItem poi = pharmOrder.getPharmOrderItemByOrgItemNum(capdSplitItem.getItemNum());
			if ( poi == null ) {
				itr.remove();
			}
		}	
	}
	
	public void clearCapdSplit(){
		pharmOrder.setCapdSplitXml(null);
		pharmOrder.setCapdSplit(null);
	}

	public String getErrMsg() {
		return errMsg;
	}

	public String getErrItemCode() {
		return errItemCode;
	}
		
	@Remove
	public void destroy() 
	{
		if ( capdSplit != null ) {
			capdSplit = null;
		}
	}
}
