package hk.org.ha.model.pms.biz.report;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("workstoreListManager")
@MeasureCalls
public class WorkstoreListManagerBean implements WorkstoreListManagerLocal {
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private Workstore workstore;
	
	private List<Workstore> workstoreList;

	@SuppressWarnings("unchecked")
	public List<Workstore> retrieveWorkstoreList() 
	{
		workstoreList = em.createQuery(
				"select o from Workstore o" + // 20120306 index check : Workstore.hospital : FK_WORKSTORE_01
				" where o.hospCode = :hospCode " +
				" order by o.workstoreCode")				
				.setParameter("hospCode", workstore.getHospCode())
				.getResultList();
		
		return workstoreList;
	}
	
	@Remove
	public void destroy() 
	{
		if (workstoreList != null) {
			workstoreList = null;
		}
	}
}

