package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class DnldTrxFile implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String fileName;

	private Date createDate;
	
	private String displayName;

	public DnldTrxFile(String fileName, Date createDate) {
		super();
		this.setFileName(fileName);
		this.setCreateDate(createDate);
	}
	
	public DnldTrxFile(String fileName, Date createDate, String displayName) {
		super();
		this.setFileName(fileName);
		this.setCreateDate(createDate);
		this.setDisplayName(displayName);
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
}
