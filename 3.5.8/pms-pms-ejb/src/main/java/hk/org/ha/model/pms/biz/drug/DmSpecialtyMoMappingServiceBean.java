package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.cache.DmSpecialtyMoMappingCacher;
import hk.org.ha.model.pms.dms.persistence.DmSpecialtyMoMapping;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("dmSpecialtyMoMappingService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DmSpecialtyMoMappingServiceBean implements DmSpecialtyMoMappingServiceLocal {

	@Logger
	private Log logger;

	@In
	private DmSpecialtyMoMappingCacher dmSpecialtyMoMappingCacher;

	@Out(required = false)
	private DmSpecialtyMoMapping dmSpecialtyMoMapping;
	
	private boolean retrieveSuccess;
	
	public void retrieveDmSpecialtyMoMapping(String specialtyCode)
	{
		logger.debug("retrieveDmSpecialtyMoMapping #0", specialtyCode);

		retrieveSuccess = false;		
		dmSpecialtyMoMapping = dmSpecialtyMoMappingCacher.getDmSpecialtyMoMappingBySpecialtyCode(specialtyCode);
		if (dmSpecialtyMoMapping != null) {
			retrieveSuccess = true;
		}
		
		logger.debug("retrieveDmSpecialtyMoMapping result #0", retrieveSuccess);
	}

	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}
	
	@Remove
	public void destroy() 
	{
		if (dmSpecialtyMoMapping != null) {
			dmSpecialtyMoMapping = null;
		}
	}

}
