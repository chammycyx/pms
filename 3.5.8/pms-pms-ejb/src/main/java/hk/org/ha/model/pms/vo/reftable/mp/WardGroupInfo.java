package hk.org.ha.model.pms.vo.reftable.mp;

import hk.org.ha.model.pms.persistence.phs.Ward;
import hk.org.ha.model.pms.persistence.reftable.WardGroup;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class WardGroupInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<WardGroup> wardGroupList;
	
	private List<Ward> wardGroupWardList;

	public void setWardGroupList(List<WardGroup> wardGroupList) {
		this.wardGroupList = wardGroupList;
	}

	public List<WardGroup> getWardGroupList() {
		return wardGroupList;
	}

	public void setWardGroupWardList(List<Ward> wardGroupWardList) {
		this.wardGroupWardList = wardGroupWardList;
	}

	public List<Ward> getWardGroupWardList() {
		return wardGroupWardList;
	}

}