package hk.org.ha.model.pms.vo.charging;


import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
import hk.org.ha.model.pms.udt.charging.ClearanceStatus;

@ExternalizedBean(type=DefaultExternalizer.class)
public class FcsPaymentStatus implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String invoiceNum;
	
	private String chargeAmount;
	
	private String receiptAmount;
	
	private String receiptNum;
	
	private String receiptStatus;
	
	private String receiptMessage;
	
	private ClearanceStatus clearanceStatus;

	public FcsPaymentStatus(){
		setChargeAmount("0");
		setReceiptAmount("-1");
		setReceiptMessage("Receipt not found");
		setReceiptNum("Receipt not found");
	}
	
	public ClearanceStatus getClearanceStatus() {
		return clearanceStatus;
	}

	public void setClearanceStatus(ClearanceStatus clearanceStatus) {
		this.clearanceStatus = clearanceStatus;
	}

	public String getChargeAmount() {
		return chargeAmount;
	}

	public void setChargeAmount(String chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	public String getReceiptAmount() {
		return receiptAmount;
	}

	public void setReceiptAmount(String receiptAmount) {
		this.receiptAmount = receiptAmount;
	}

	public String getReceiptNum() {
		return receiptNum;
	}

	public void setReceiptNum(String receiptNum) {
		this.receiptNum = receiptNum;
	}

	public String getReceiptStatus() {
		return receiptStatus;
	}

	public void setReceiptStatus(String receiptStatus) {
		this.receiptStatus = receiptStatus;
	}

	public String getReceiptMessage() {
		return receiptMessage;
	}

	public void setReceiptMessage(String receiptMessage) {
		this.receiptMessage = receiptMessage;
	}

	public String getInvoiceNum() {
		return invoiceNum;
	}

	public void setInvoiceNum(String invoiceNum) {
		this.invoiceNum = invoiceNum;
	}
}
