package hk.org.ha.model.pms.vo.report;

import hk.org.ha.model.pms.udt.printing.PrintLang;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class InfusionInstructionSummaryItemDtl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String itemCode;
	
	private String labelDesc;
	
	private String labelInstructionText;
	
	private Integer issueQty;
	
	private String baseUnit;
	
	private String warningText;
	
	private PrintLang printLang;

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getLabelDesc() {
		return labelDesc;
	}

	public void setLabelDesc(String labelDesc) {
		this.labelDesc = labelDesc;
	}

	public String getLabelInstructionText() {
		return labelInstructionText;
	}

	public void setLabelInstructionText(String labelInstructionText) {
		this.labelInstructionText = labelInstructionText;
	}

	public Integer getIssueQty() {
		return issueQty;
	}

	public void setIssueQty(Integer issueQty) {
		this.issueQty = issueQty;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public String getWarningText() {
		return warningText;
	}

	public void setWarningText(String warningText) {
		this.warningText = warningText;
	}

	public void setPrintLang(PrintLang printLang) {
		this.printLang = printLang;
	}

	public PrintLang getPrintLang() {
		return printLang;
	}
	
}
