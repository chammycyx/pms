package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.cache.DrugSiteMappingCacherInf;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.corp.cache.DmFormCacher;
import hk.org.ha.model.pms.corp.cache.DmSupplSiteCacher;
import hk.org.ha.model.pms.dms.persistence.DmSite;
import hk.org.ha.model.pms.dms.persistence.DmSupplSite;
import hk.org.ha.model.pms.dms.vo.DrugRouteCriteria;
import hk.org.ha.model.pms.dms.vo.DrugSiteMapping;
import hk.org.ha.model.pms.dms.vo.SupplSite;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.rx.Regimen;
import hk.org.ha.model.pms.vo.rx.Site;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("drugRouteManager")
@MeasureCalls
public class DrugRouteManagerBean implements DrugRouteManagerLocal {

	@Logger
	private Log logger;

	@In
	private List<DmSite> dmSiteList;
	
	@In
	private List<DmSite> opTypeDmSiteList;

	@In
	private DrugSiteMappingCacherInf drugSiteMappingCacher;
	
	@In
	private Workstore workstore;
	
	private PmsDmSiteComparator freeTextSiteComparator = new PmsDmSiteComparator();
	private PmsDmSupplSiteComparator freeTextSupplSiteComparator = new PmsDmSupplSiteComparator();
	private Regimen.PmsSiteComparator pmsSiteComparator = new Regimen.PmsSiteComparator();
	
	private DrugRouteCriteria constructDrugRouteCriteria(String itemCode) {
		DrugRouteCriteria criteria = new DrugRouteCriteria();
		criteria.setDispHospCode(workstore.getHospCode());
		criteria.setDispWorkstore(workstore.getWorkstoreCode());
		criteria.setItemCode(itemCode);
		return criteria;
	}
	
	private List<Map<String, Object>> buildMpFreeTextOthersSiteMapList() {
		logger.debug("buildMpFreeTextOthersSiteMapList");
		List<Map<String, Object>> resultSiteMapList = new ArrayList<Map<String, Object>>();
		Map<String, Object> mpSiteMap;
		Map<String, Object> supplSiteMap;
		List<Map<String,Object>> supplSiteMapList;
		List<DmSupplSite> dmSupplSiteList;
		List<DmSupplSite> sortedSupplSiteList;
		
		List<DmSite> sortedDmSiteList = new ArrayList<DmSite>();
		sortedDmSiteList.addAll(dmSiteList);
		Collections.sort(sortedDmSiteList, freeTextSiteComparator);
		
		for (DmSite dmSite:sortedDmSiteList) {
			mpSiteMap = new HashMap<String, Object>();
			if ("B".equals(dmSite.getUsageType()) || "I".equals(dmSite.getUsageType())) {
				Site site = new Site(dmSite, null, false);
				mpSiteMap.put("site", site);
				mpSiteMap.put("treeNode", site.getSiteLabelDesc());
			
				resultSiteMapList.add(mpSiteMap);
			
				dmSupplSiteList = DmSupplSiteCacher.instance().getSupplSiteListBySiteCode(dmSite.getSiteCode());
				if (dmSupplSiteList != null && dmSupplSiteList.size() > 0) {
					
					sortedSupplSiteList = new ArrayList<DmSupplSite>();
					supplSiteMapList = new ArrayList<Map<String, Object>>();
					
					sortedSupplSiteList.addAll(dmSupplSiteList);
					Collections.sort(sortedSupplSiteList,freeTextSupplSiteComparator);
					
					for (DmSupplSite dmSupplSite:sortedSupplSiteList) {
						Site supplSite = new Site(dmSite, dmSupplSite, false);
						supplSiteMap = new HashMap<String, Object>();
						supplSiteMap.put("site", supplSite);
						supplSiteMap.put("treeNode", supplSite.getSiteLabelDesc());
						supplSiteMapList.add(supplSiteMap);
					}
					mpSiteMap.put("children", supplSiteMapList);
				}
			}
		}
		return resultSiteMapList;
	}
	
	private List<Map<String,Object>> contructSupplSiteListMapByDrugSiteMapping(DrugSiteMapping drugSiteMapping) {
		SupplSite[] supplSiteArray = drugSiteMapping.getSite().getSupplSites();
		List<Map<String,Object>> supplSiteMapList = new ArrayList<Map<String, Object>>();	
		for (int k=0; k<supplSiteArray.length; k++) {
			Map<String, Object> supplSiteMap = new HashMap<String, Object>();
			Site supplSite = new Site(supplSiteArray[k].getSiteCode(), supplSiteArray[k].getSupplSiteEng(), false);
			supplSiteMap.put("site", supplSite);
			supplSiteMap.put("treeNode", supplSite.getSupplSiteDesc());
			supplSiteMapList.add(supplSiteMap);	
		}	
		return supplSiteMapList;
	}
	
	private Map<String, Object> constructSiteMap(DrugSiteMapping drugSiteMapping) {				
		Map<String, Object> mpSiteMap = new HashMap<String, Object>();
		if ( drugSiteMapping.getSite() != null ) {
			Site site = new Site(drugSiteMapping.getSite().getSiteCode(), null, false);
			mpSiteMap.put("site", site);				
			mpSiteMap.put("treeNode", site.getSiteLabelDesc());
			SupplSite[] supplSiteArray = drugSiteMapping.getSite().getSupplSites();
			if ( supplSiteArray != null && supplSiteArray.length > 0 ) {
				mpSiteMap.put("children", contructSupplSiteListMapByDrugSiteMapping(drugSiteMapping));
			}
		}
		
		return mpSiteMap;
	}
	
	private List<Map<String, Object>> buildOthersSiteMapList(List<DrugSiteMapping> drugSiteMappingList, DrugRouteCriteria criteria, Boolean manualEntryFlag) {
		List<Map<String, Object>> resultSiteMapList = new ArrayList<Map<String, Object>>();
		if ( drugSiteMappingList == null ) {
			return resultSiteMapList;
		}
		logger.debug("buildOthersSiteMapList drugSiteMappingList - size | #0", drugSiteMappingList.size());
		
		boolean parenteralFlag = false;
		
		boolean isDischargeMo = StringUtils.isNotEmpty(criteria.getTrueDisplayname()) || StringUtils.isNotEmpty(criteria.getFormCode()) || StringUtils.isNotEmpty(criteria.getSaltProperty());
		
		if (StringUtils.isEmpty(criteria.getItemCode())) {
			parenteralFlag = DmFormCacher.instance().getFormByFormCode(criteria.getFormCode()).getRank().startsWith("9");
		} else {
			parenteralFlag = DmDrugCacher.instance().getDmDrug(criteria.getItemCode()).getDmForm().getRank().startsWith("9");
		}
		for (DrugSiteMapping drugSiteMapping : drugSiteMappingList ) {
			if (Boolean.FALSE.equals(manualEntryFlag)) {
				if (!isDischargeMo && "Y".equals(drugSiteMapping.getParenteralSite())) {
					continue;
				}
				if ("CF".equals(drugSiteMapping.getSiteCategoryCode()) || "IF".equals(drugSiteMapping.getSiteCategoryCode())) {
					continue;
				}
			}			
			if ( !parenteralFlag ) {
				if ( drugSiteMapping.getGreyListLevel() != null && drugSiteMapping.getGreyListLevel().intValue() == 2 ) {
					resultSiteMapList.add(constructSiteMap( drugSiteMapping ));	
				}								
			} else {
				if (drugSiteMapping.getGreyListLevel() == null) {
					continue;
				}
				resultSiteMapList.add(constructSiteMap( drugSiteMapping ));
			}
		}			
	
		return resultSiteMapList;
	}
	
	//Construct manual entry others site list
	private List<Map<String, Object>> buildManualEntryOthersSiteMapList(List<DrugSiteMapping> drugSiteMappingList, DrugRouteCriteria criteria) {					
		return buildOthersSiteMapList(drugSiteMappingList, criteria, Boolean.TRUE);
	}
	
	//Construct others site list
	private List<Map<String, Object>> buildMpOthersSiteMapList(List<DrugSiteMapping> drugSiteMappingList, DrugRouteCriteria criteria) {
		return buildOthersSiteMapList(drugSiteMappingList, criteria, Boolean.FALSE);
	}
	
	private void addOpParenteralSite(List<Map<String, Object>> ipSiteMapList, String itemCode) {		
		if (StringUtils.isEmpty(itemCode) 
				|| "NOT 01".equals(itemCode)
				|| DmDrugCacher.instance().getDmDrug(itemCode).getDmForm().getRank().charAt(0) == '9') {
			Map<String, Object> mpSiteMap;
			for (DmSite dmSite:opTypeDmSiteList) {
				mpSiteMap = new HashMap<String, Object>();			
				Site site = new Site(dmSite,null,false);
				mpSiteMap.put("site", site);				
				mpSiteMap.put("treeNode", site.getSiteLabelDesc());
				ipSiteMapList.add(mpSiteMap);
			}
		}
	}
	
	private List<Site> getFreeTextSiteList() {
		List<Site> siteList = new ArrayList<Site>();
		for (DmSite dmSite:dmSiteList) {
			if ("B".equals(dmSite.getUsageType()) || "I".equals(dmSite.getUsageType())) {
				siteList.add(new Site(dmSite,null,false));
			}
		}	
		Collections.sort(siteList, pmsSiteComparator);
		Site site = new Site();
		site.setSiteCode( "OTHERS" );
		site.setSupplSiteDesc( "OTHERS" );
		site.setSiteLabelDesc( "OTHERS" );
		siteList.add( site );
		return siteList;
	}
	
	public List<Site> retrieveIpSiteListByItemCode(String itemCode) {	
		if ( logger.isDebugEnabled() ) {			
			logger.debug("retrieveIpSiteListByItemCode - itemCode:#0,hospCode:#1,workstoreCode:#2", itemCode, workstore.getHospCode(), workstore.getWorkstoreCode());
		}
		if ("NOT 01".equals(itemCode)) {
			return getFreeTextSiteList();
		}		
		List<Site> siteList = drugSiteMappingCacher.getIpSiteList(constructDrugRouteCriteria(itemCode));
		if (siteList.isEmpty()) {
			siteList.add(buildOtherSite());
		}
		return siteList;
	}
	
	private Site buildOtherSite() {
		Site site = new Site();
		site.setSiteCode( "OTHERS" );
		site.setSupplSiteDesc( "OTHERS" );
		site.setSiteLabelDesc( "OTHERS" );
		site.setIsSupplSite(Boolean.FALSE);		
		return site;		
	}
	
	public List<Site> retrieveManualEntrySiteListByItemCode(String itemCode) {
		if ( logger.isDebugEnabled() ) {			
			logger.debug("retrieveManualEntrySiteListByItemCode - itemCode:#0,hospCode:#1,workstoreCode:#2", itemCode, workstore.getHospCode(), workstore.getWorkstoreCode());
		}
		if ("NOT 01".equals(itemCode)) {
			return getFreeTextSiteList();
		}
		return drugSiteMappingCacher.getManualEntrySiteList(constructDrugRouteCriteria(itemCode));
	}
	
	//For both IPMOE and Discharge
	public List<Map<String, Object>> retrieveIpOthersSiteMapListByDrugRouteCriteria(DrugRouteCriteria criteria) {
		criteria.setDispHospCode(workstore.getHospCode());
		criteria.setDispWorkstore(workstore.getWorkstoreCode());
		if ( logger.isDebugEnabled() ) {			
			logger.debug("retrieveIpSiteMapListByDrugRouteCriteria - ItemCode:#0, DispHospCode:#1, DispWorkstore:#2, TrueDisplayname:#3, FormCode:#4, SaltProperty:#5", 
					criteria.getItemCode(), criteria.getDispHospCode(), criteria.getDispWorkstore(), criteria.getTrueDisplayname(), criteria.getFormCode(), criteria.getSaltProperty());
		}
		if (StringUtils.isEmpty(criteria.getItemCode()) &&
				StringUtils.isEmpty(criteria.getTrueDisplayname()) &&
				StringUtils.isEmpty(criteria.getFormCode()) &&
				StringUtils.isEmpty(criteria.getSaltProperty())) {//Retrieve discharge/manual entry free text site list pop up content
			return buildMpFreeTextOthersSiteMapList();	
		} else {
			return buildMpOthersSiteMapList(drugSiteMappingCacher.getDrugSiteMappingListByDrugRouteCriteria(criteria), criteria);
		}
	}
	
	public List<Map<String, Object>> retrieveManualEntryOthersSiteMapListByDrugRouteCriteria(DrugRouteCriteria criteria) {
		
		List<Map<String, Object>> ipSiteMapList;
		
		criteria.setDispHospCode(workstore.getHospCode());
		criteria.setDispWorkstore(workstore.getWorkstoreCode());
		if (StringUtils.isEmpty(criteria.getItemCode()) &&
				StringUtils.isEmpty(criteria.getTrueDisplayname()) &&
				StringUtils.isEmpty(criteria.getFormCode()) &&
				StringUtils.isEmpty(criteria.getSaltProperty())) {//Retrieve discharge/manual entry free text site list pop up content
			ipSiteMapList = buildMpFreeTextOthersSiteMapList();	
		} else {
			ipSiteMapList = buildManualEntryOthersSiteMapList(drugSiteMappingCacher.getDrugSiteMappingListByDrugRouteCriteria(criteria), criteria);
		}
		
		addOpParenteralSite(ipSiteMapList, criteria.getItemCode());
		return ipSiteMapList;
	}
	
	private static class PmsDmSiteComparator implements Comparator<DmSite>, Serializable {
		private static final long serialVersionUID = 1L;

		@Override
		public int compare(DmSite dmSite1, DmSite dmSite2) {			
			return (new CompareToBuilder().append(dmSite1.getIpmoeDesc(), dmSite2.getIpmoeDesc())
					.append(dmSite1.getSiteDescEng(), dmSite2.getSiteDescEng())).toComparison();
		}
	}
	
	private static class PmsDmSupplSiteComparator implements Comparator<DmSupplSite>, Serializable {
		private static final long serialVersionUID = 1L;

		@Override
		public int compare(DmSupplSite s1, DmSupplSite s2) {			
			return (new CompareToBuilder().append(s1.getRank(), s2.getRank())).toComparison();
		}
		
	}
}
