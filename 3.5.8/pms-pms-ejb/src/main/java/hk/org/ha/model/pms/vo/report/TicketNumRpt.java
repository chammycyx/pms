package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.List;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class TicketNumRpt implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private TicketNumRptHdr ticketNumRptHdr;
	
	private List<TicketNumRptDtl> ticketNumRptDtlList;

	public TicketNumRptHdr getTicketNumRptHdr() {
		return ticketNumRptHdr;
	}

	public void setTickeNumRptHdr(TicketNumRptHdr ticketNumRptHdr) {
		this.ticketNumRptHdr = ticketNumRptHdr;
	}

	public List<TicketNumRptDtl> getTicketNumRptDtlList() {
		return ticketNumRptDtlList;
	}

	public void setTicketNumRptDtlList(List<TicketNumRptDtl> ticketNumRptDtlList) {
		this.ticketNumRptDtlList = ticketNumRptDtlList;
	}

	
}
