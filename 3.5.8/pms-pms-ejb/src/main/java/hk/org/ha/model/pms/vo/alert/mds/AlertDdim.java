package hk.org.ha.model.pms.vo.alert.mds;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedProperty;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "AlertDdim")
@ExternalizedBean(type=DefaultExternalizer.class)
public class AlertDdim implements Alert, Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private static final char NEWLINE = '\n';

	private static final char[] DELIMITER = new char[] {' ' , '~' , '&' , '-' , '[' , '(' , '<'};
	
	private static final String SEPARATOR_AND     = " and ";
	private static final String SEPARATOR_MAY     = "may";
	private static final String SEPARATOR_BRASKET = "[";

	private static final int ADDITIVE_NUM_MULTIPLIER = 10000;
	
	private static final int MANUAL_ITEM_MULTIPLIER = 10000;
	
	private static final int MANUAL_ITEM_RANGE = 100000000; //100010000

	@XmlElement(name="HospCode1", required = true)
	private String hospCode1;
	
	@XmlElement(name="OrdNo1")
	private Long ordNo1;
 
	@XmlElement(name="ItemNum1", required = true)
	private Long itemNum1;
 
	@XmlElement(name="Rgenid1", required = true)
	private Long rgenid1;
	
	@XmlElement(name="HospCode2", required = true)
	private String hospCode2;
	
	@XmlElement(name="OrdNo2")
	private Long ordNo2;
 
	@XmlElement(name="ItemNum2", required = true)
	private Long itemNum2;
 
	@XmlElement(name="Rgenid2", required = true)
	private Long rgenid2;
		
	@XmlElement(name="InteractionId", required = true)
	private Long interationId;
	
	@XmlElement(name="InteractionDesc", required = true)
	private String interactionDesc;
	
	@XmlElement(name="ClinicalEffectCode1")
	private String clinicalEffectCode1;
	
	@XmlElement(name="ClinicalEffectText1")
	private String clinicalEffectText1;
	
	@XmlElement(name="ClinicalEffectCode2")
	private String clinicalEffectCode2;
	
	@XmlElement(name="ClinicalEffectText2")
	private String clinicalEffectText2;
	
	@XmlElement(name="SeverityLevel", required = true)
	private Integer severityLevel;
	
	@XmlElement(name="ScreenMsg", required = true)
	private String screenMsg;
	
	@XmlElement(name="MonographId")
	private Long monographId;
	
	@XmlElement(name="MonographText")
	private String monographText;
	
	@XmlElement(name="UserCategory")
	private String userCategory;
	
	@XmlElement(name="UserInteraction")
	private Boolean userInteraction;

	@XmlElement(name="DrugName1")
	private String drugName1;
	
	@XmlElement(name="DrugName2")
	private String drugName2;
	
	@XmlElement(name="crossDdiIpmoeOrderDesc")
	private String crossDdiIpmoeOrderDesc;

	@XmlElement(name="CopyFlag")
	private Boolean copyFlag;
	
	@XmlTransient
	private Boolean suppressFlag;
	
	@XmlTransient
	private Long sortSeq;
	
	public AlertDdim() {
		copyFlag     = Boolean.FALSE;
		suppressFlag = Boolean.FALSE;
	}
	
	public String getHospCode1() {
		return hospCode1;
	}

	public void setHospCode1(String hospCode1) {
		this.hospCode1 = hospCode1;
	}

	public Long getOrdNo1() {
		return ordNo1;
	}

	public void setOrdNo1(Long ordNo1) {
		this.ordNo1 = ordNo1;
	}

	public Long getItemNum1() {
		return itemNum1;
	}

	public void setItemNum1(Long itemNum1) {
		this.itemNum1 = itemNum1;
	}

	public Long getRgenid1() {
		return rgenid1;
	}

	public void setRgenid1(Long rgenid1) {
		this.rgenid1 = rgenid1;
	}

	public String getHospCode2() {
		return hospCode2;
	}

	public void setHospCode2(String hospCode2) {
		this.hospCode2 = hospCode2;
	}

	public Long getOrdNo2() {
		return ordNo2;
	}

	public void setOrdNo2(Long ordNo2) {
		this.ordNo2 = ordNo2;
	}

	public Long getItemNum2() {
		return itemNum2;
	}

	public void setItemNum2(Long itemNum2) {
		this.itemNum2 = itemNum2;
	}

	public Long getRgenid2() {
		return rgenid2;
	}

	public void setRgenid2(Long rgenid2) {
		this.rgenid2 = rgenid2;
	}

	public Long getInterationId() {
		return interationId;
	}

	public void setInterationId(Long interationId) {
		this.interationId = interationId;
	}

	public String getInteractionDesc() {
		return interactionDesc;
	}

	public void setInteractionDesc(String interactionDesc) {
		this.interactionDesc = interactionDesc;
	}

	public String getClinicalEffectCode1() {
		return clinicalEffectCode1;
	}

	public void setClinicalEffectCode1(String clinicalEffectCode1) {
		this.clinicalEffectCode1 = clinicalEffectCode1;
	}

	public String getClinicalEffectText1() {
		return clinicalEffectText1;
	}

	public void setClinicalEffectText1(String clinicalEffectText1) {
		this.clinicalEffectText1 = clinicalEffectText1;
	}

	public String getClinicalEffectCode2() {
		return clinicalEffectCode2;
	}

	public void setClinicalEffectCode2(String clinicalEffectCode2) {
		this.clinicalEffectCode2 = clinicalEffectCode2;
	}

	public String getClinicalEffectText2() {
		return clinicalEffectText2;
	}

	public void setClinicalEffectText2(String clinicalEffectText2) {
		this.clinicalEffectText2 = clinicalEffectText2;
	}

	public Integer getSeverityLevel() {
		return severityLevel;
	}

	public void setSeverityLevel(Integer severityLevel) {
		this.severityLevel = severityLevel;
	}

	public String getScreenMsg() {
		return screenMsg;
	}

	public void setScreenMsg(String screenMsg) {
		this.screenMsg = screenMsg;
	}

	public Long getMonographId() {
		return monographId;
	}

	public void setMonographId(Long monographId) {
		this.monographId = monographId;
	}
	
	public String getMonographText() {
		return monographText;
	}

	public void setMonographText(String monographText) {
		this.monographText = monographText;
	}

	public String getUserCategory() {
		return userCategory;
	}

	public void setUserCategory(String userCategory) {
		this.userCategory = userCategory;
	}

	public Boolean getUserInteraction() {
		return userInteraction;
	}

	public void setUserInteraction(Boolean userInteraction) {
		this.userInteraction = userInteraction;
	}

	public String getDrugName1() {
		return drugName1;
	}

	public void setDrugName1(String drugName1) {
		this.drugName1 = drugName1;
	}

	public String getDrugName2() {
		return drugName2;
	}

	public void setDrugName2(String drugName2) {
		this.drugName2 = drugName2;
	}

	public String getCrossDdiIpmoeOrderDesc() {
		return crossDdiIpmoeOrderDesc;
	}

	public void setCrossDdiIpmoeOrderDesc(String crossDdiIpmoeOrderDesc) {
		this.crossDdiIpmoeOrderDesc = crossDdiIpmoeOrderDesc;
	}

	public Boolean getCopyFlag() {
		return copyFlag;
	}

	public void setCopyFlag(Boolean copyFlag) {
		this.copyFlag = copyFlag;
	}

	public Boolean getSuppressFlag() {
		return suppressFlag;
	}

	public void setSuppressFlag(Boolean suppressFlag) {
		this.suppressFlag = suppressFlag;
	}

	public Long getSortSeq() {
		return sortSeq;
	}

	public void setSortSeq(Long sortSeq) {
		this.sortSeq = sortSeq;
	}

	@ExternalizedProperty
	public String getAlertTitle() {

		StringBuilder sb = new StringBuilder();

		String displayDrugName1 = StringUtils.splitByWholeSeparator(screenMsg, SEPARATOR_AND)[0].toUpperCase();
		String displayDrugName2 = StringUtils.splitByWholeSeparator(StringUtils.splitByWholeSeparator(screenMsg, SEPARATOR_AND)[1], SEPARATOR_MAY)[0].toUpperCase();

		String patHospCode1, patHospCode2;
		
		if (displayDrugName1.indexOf(SEPARATOR_BRASKET) >= 0) {
			patHospCode1 = SEPARATOR_BRASKET + StringUtils.split(displayDrugName1, SEPARATOR_BRASKET)[1];
			displayDrugName1 = StringUtils.split(displayDrugName1, SEPARATOR_BRASKET)[0];
		}
		else {
			patHospCode1 = StringUtils.EMPTY;
		}
		
		if (displayDrugName2.indexOf(SEPARATOR_BRASKET) >= 0) {
			patHospCode2 = SEPARATOR_BRASKET + StringUtils.split(displayDrugName2, SEPARATOR_BRASKET)[1];
			displayDrugName2 = StringUtils.split(displayDrugName2, SEPARATOR_BRASKET)[0];
		}
		else {
			patHospCode2 = StringUtils.EMPTY;
		}
		
		sb.append("Contraindicated Drug Combination / Important Interaction: ");
		sb.append(WordUtils.capitalizeFully(displayDrugName1, DELIMITER));
		sb.append(patHospCode1);
		sb.append(" and ");
		sb.append(WordUtils.capitalizeFully(displayDrugName2, DELIMITER));
		sb.append(patHospCode2);

		return sb.toString();
	}

	@ExternalizedProperty
	public String getAlertDesc() {
		return screenMsg;
	}
	
	public boolean isCrossDdi(){
		return ((itemNum1 > 10000 && itemNum2 <= 10000 ) || ( itemNum1 <=10000 && itemNum2 > 10000 ));
	}

	public boolean isOnHand() {
		return !ordNo1.equals(ordNo2);
	}
	
	public boolean equals(Object object) {
		
		if (!(object instanceof AlertDdim)) {
			return false;
		}
		if (this == object) {
			return true;
		}

		AlertDdim otherAlert = (AlertDdim) object;
		
		EqualsBuilder eb = new EqualsBuilder()
			.append(interationId,                                                       otherAlert.getInterationId())
			.append(severityLevel,                                                      otherAlert.getSeverityLevel())
			.append(StringUtils.trimToNull(StringUtils.lowerCase(userCategory)),        StringUtils.trimToNull(StringUtils.lowerCase(otherAlert.getUserCategory())))
			.append(userInteraction,                                                    otherAlert.getUserInteraction());
		
		if (ordNo1.compareTo(ordNo2) > 0) {
			return eb
				.append(StringUtils.trimToNull(StringUtils.lowerCase(hospCode1)),           StringUtils.trimToNull(StringUtils.lowerCase(otherAlert.getHospCode1())))
				.append(StringUtils.trimToNull(StringUtils.lowerCase(hospCode2)),           StringUtils.trimToNull(StringUtils.lowerCase(otherAlert.getHospCode2())))
				.append(ordNo1,                                                             otherAlert.getOrdNo1())
				.append(ordNo2,                                                             otherAlert.getOrdNo2())
				.append(itemNum1,                                                           otherAlert.getItemNum1())
				.append(itemNum2,                                                           otherAlert.getItemNum2())
				.append(rgenid1,                                                            otherAlert.getRgenid1())
				.append(rgenid2,                                                            otherAlert.getRgenid2())
				.isEquals();
		}
		else {
			return eb
				.append(StringUtils.trimToNull(StringUtils.lowerCase(hospCode2)),           StringUtils.trimToNull(StringUtils.lowerCase(otherAlert.getHospCode2())))
				.append(StringUtils.trimToNull(StringUtils.lowerCase(hospCode1)),           StringUtils.trimToNull(StringUtils.lowerCase(otherAlert.getHospCode1())))
				.append(ordNo2,                                                             otherAlert.getOrdNo2())
				.append(ordNo1,                                                             otherAlert.getOrdNo1())
				.append(itemNum2,                                                           otherAlert.getItemNum2())
				.append(itemNum1,                                                           otherAlert.getItemNum1())
				.append(rgenid2,                                                            otherAlert.getRgenid2())
				.append(rgenid1,                                                            otherAlert.getRgenid1())
				.isEquals();
		}
	}
	
	public int hashCode() {
		HashCodeBuilder hcb = new HashCodeBuilder()
			.append(interationId)
			.append(severityLevel)
			.append(StringUtils.trimToNull(StringUtils.lowerCase(userCategory)))
			.append(userInteraction);
			
		if (ordNo1.compareTo(ordNo2) > 0) {
			return hcb
				.append(StringUtils.trimToNull(StringUtils.lowerCase(hospCode1)))
				.append(StringUtils.trimToNull(StringUtils.lowerCase(hospCode2)))
				.append(ordNo1)
				.append(ordNo2)
				.append(itemNum1)
				.append(itemNum2)
				.append(rgenid1)
				.append(rgenid2)
				.toHashCode();
		}
		else {
			return hcb
				.append(StringUtils.trimToNull(StringUtils.lowerCase(hospCode2)))
				.append(StringUtils.trimToNull(StringUtils.lowerCase(hospCode1)))
				.append(ordNo2)
				.append(ordNo1)
				.append(itemNum2)
				.append(itemNum1)
				.append(rgenid2)
				.append(rgenid1)
				.toHashCode();
		}
	}

	public void loadDrugDesc(String drugDesc) {
	}
	
	public String getOrderNum1() {
		if (Long.valueOf(0).equals(ordNo1)) {
			return null;
		}
		
		StringBuilder sb = new StringBuilder(hospCode1);
		if (sb.length() == 2) {
			sb.append(' ');
		}
		
		sb.append(ordNo1);
		return sb.toString();
	}

	public String getOrderNum2() {
		if (Long.valueOf(0).equals(ordNo2)) {
			return null;
		}
		
		StringBuilder sb = new StringBuilder(hospCode2);
		if (sb.length() == 2) {
			sb.append(' ');
		}
		
		sb.append(ordNo2);
		return sb.toString();
	}

	private void setOrderNum1(String orderNum) {
		if (orderNum == null) {
			ordNo1 = Long.valueOf(0);
			hospCode1 = null;
		}
		else {
			ordNo1 = Long.valueOf(orderNum.substring(3));
			hospCode1 = StringUtils.trimToNull(orderNum.substring(0, 3));
		}
	}
	
	private void setOrderNum2(String orderNum) {
		if (orderNum == null) {
			ordNo2 = Long.valueOf(0);
			hospCode2 = null;
		}
		else {
			ordNo2 = Long.valueOf(orderNum.substring(3));
			hospCode2 = StringUtils.trimToNull(orderNum.substring(0, 3));
		}
	}
	
	public void replaceItemNum(Integer oldItemNum, Integer newItemNum, String orderNum) {
		if (StringUtils.equals(getOrderNum1(), orderNum) && oldItemNum.intValue() == itemNum1.intValue()) {
			itemNum1 = Long.valueOf(newItemNum.intValue());
		}
		if (StringUtils.equals(getOrderNum2(), orderNum) && oldItemNum.intValue() == itemNum2.intValue()) {
			itemNum2 = Long.valueOf(newItemNum.intValue());
		}
	}

	public void replaceOrderNum(String oldOrderNum, String newOrderNum) {
		if (StringUtils.equals(getOrderNum1(), oldOrderNum)) {
			setOrderNum1(newOrderNum);
		}
		if (StringUtils.equals(getOrderNum2(), oldOrderNum)) {
			setOrderNum2(newOrderNum);
		}
	}

	public void restoreItemNum() {
		if( itemNum1.longValue() > MANUAL_ITEM_RANGE ){
			itemNum1 = Long.valueOf(itemNum1.longValue() / MANUAL_ITEM_MULTIPLIER);
		}else if (itemNum1.longValue() >= ADDITIVE_NUM_MULTIPLIER) {
			itemNum1 = Long.valueOf(itemNum1.longValue() / ADDITIVE_NUM_MULTIPLIER);
		}
		if( itemNum2.longValue() > MANUAL_ITEM_RANGE ){
			itemNum2 = Long.valueOf(itemNum2.longValue() / MANUAL_ITEM_MULTIPLIER);
		}else if (itemNum2.longValue() >= ADDITIVE_NUM_MULTIPLIER) {
			itemNum2 = Long.valueOf(itemNum2.longValue() / ADDITIVE_NUM_MULTIPLIER);
		}
	}

	@ExternalizedProperty
	public String getKeys() {
		
		String header = AlertDdim.class.getName()
						+ NEWLINE + interationId.toString()
						+ NEWLINE + severityLevel.toString()
						+ NEWLINE + StringUtils.trimToEmpty(StringUtils.lowerCase(userCategory))
						+ NEWLINE + userInteraction.toString();

		String body1 = NEWLINE + rgenid1.toString()
						+ NEWLINE + rgenid2.toString();

		String body2 = NEWLINE + rgenid2.toString()
						+ NEWLINE + rgenid1.toString();

		if (body1.compareTo(body2) > 0) {
			return header + body1;
		}
		else {
			return header + body2;
		}
	}
}
