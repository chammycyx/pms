package hk.org.ha.model.pms.biz.onestop;

import hk.org.ha.model.pms.vo.onestop.RequireAppointmentRule;

import javax.ejb.Local;

public interface PatSpecNoApptMappingInf {
	
	void create();
	
	Boolean checkPatSpecApptRequire(RequireAppointmentRule requireAppointmentRule);
	
	void destroy();
}