package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.persistence.reftable.ChargeSpecialty;

import java.util.Collection;

import javax.ejb.Local;

@Local
public interface ChargeSpecialtyListServiceLocal {

	void retrieveChargeSpecialtyList();
	
	void saveChargeSpecialtyList(Collection<ChargeSpecialty> newSfiChargeSpecialtyList, Collection<ChargeSpecialty> newChargeSnSpecialtyList);
	
	void printSpecMapSfiRpt();
	
	void printSpecMapSnRpt();
	
	void destroy();
	
}
