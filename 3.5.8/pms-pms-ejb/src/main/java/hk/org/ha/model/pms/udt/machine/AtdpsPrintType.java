package hk.org.ha.model.pms.udt.machine;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum AtdpsPrintType implements StringValuedEnum {

	New("N", "Original"),
	Reprint("R","Reprint");
	
	private final String dataValue;
	private final String displayValue;
	
	AtdpsPrintType (final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}
	
	public static AtdpsPrintType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(AtdpsPrintType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<AtdpsPrintType> {

		private static final long serialVersionUID = 1L;

		public Class<AtdpsPrintType> getEnumClass() {
    		return AtdpsPrintType.class;
    	}
    }
}
