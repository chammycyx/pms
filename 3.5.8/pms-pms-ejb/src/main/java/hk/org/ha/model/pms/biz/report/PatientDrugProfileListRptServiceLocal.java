package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.vo.report.DrugRefillListRptInfo;
import hk.org.ha.model.pms.vo.report.PatientDrugProfileListRptInfo;

import javax.ejb.Local;

@Local
public interface PatientDrugProfileListRptServiceLocal {
	
	PatientDrugProfileListRptInfo retrievePatientDrugProfileListRptInfo();
	
	void retrievePatientDrugProfileListRpt(PatientDrugProfileListRptInfo patientDrugProfileListRptInfo);
	
	void generatePatientDrugProfileListRpt();

	void printPatientDrugProfileListRpt();
	
	boolean isRetrieveSuccess();
	
	void destroy();
	
}
