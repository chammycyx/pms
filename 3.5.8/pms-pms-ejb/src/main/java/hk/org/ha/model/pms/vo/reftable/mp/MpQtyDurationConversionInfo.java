package hk.org.ha.model.pms.vo.reftable.mp;

import hk.org.ha.model.pms.dms.persistence.PmsIpDurationConv;
import hk.org.ha.model.pms.dms.persistence.PmsIpQtyConv;
import hk.org.ha.model.pms.dms.persistence.PmsQtyConversionAdj;
import hk.org.ha.model.pms.dms.persistence.PmsQtyConversionInsu;
import hk.org.ha.model.pms.vo.drug.DmDrugLite;
import hk.org.ha.model.pms.vo.reftable.mp.MpPmsDurationConversionItem;
import hk.org.ha.model.pms.vo.reftable.mp.MpPmsQtyConversionItem;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class MpQtyDurationConversionInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<PmsIpQtyConv> qtyConversionList;

	private List<MpPmsQtyConversionItem> qtyConversionOtherWorkstoreList;
	
	private List<PmsIpDurationConv> durationConversionList;
	
	private List<MpPmsDurationConversionItem> durationConversionOtherWorkstoreList;

	private DmDrugLite qtyDurationConversionDmDrugLite;

	private List<MpPmsQtyConversionInsuItem> pmsQtyConversionInsuOtherWorkstoreList;
	
	private PmsQtyConversionInsu pmsQtyConversionInsu;
	
	private PmsQtyConversionAdj pmsQtyConversionAdj;

	public List<PmsIpQtyConv> getQtyConversionList() {
		return qtyConversionList;
	}

	public void setQtyConversionList(List<PmsIpQtyConv> qtyConversionList) {
		this.qtyConversionList = qtyConversionList;
	}

	public List<MpPmsQtyConversionItem> getQtyConversionOtherWorkstoreList() {
		return qtyConversionOtherWorkstoreList;
	}

	public void setQtyConversionOtherWorkstoreList(
			List<MpPmsQtyConversionItem> qtyConversionOtherWorkstoreList) {
		this.qtyConversionOtherWorkstoreList = qtyConversionOtherWorkstoreList;
	}

	public List<PmsIpDurationConv> getDurationConversionList() {
		return durationConversionList;
	}

	public void setDurationConversionList(
			List<PmsIpDurationConv> durationConversionList) {
		this.durationConversionList = durationConversionList;
	}

	public List<MpPmsDurationConversionItem> getDurationConversionOtherWorkstoreList() {
		return durationConversionOtherWorkstoreList;
	}

	public void setDurationConversionOtherWorkstoreList(
			List<MpPmsDurationConversionItem> durationConversionOtherWorkstoreList) {
		this.durationConversionOtherWorkstoreList = durationConversionOtherWorkstoreList;
	}

	public DmDrugLite getQtyDurationConversionDmDrugLite() {
		return qtyDurationConversionDmDrugLite;
	}

	public void setQtyDurationConversionDmDrugLite(
			DmDrugLite qtyDurationConversionDmDrugLite) {
		this.qtyDurationConversionDmDrugLite = qtyDurationConversionDmDrugLite;
	}

	public List<MpPmsQtyConversionInsuItem> getPmsQtyConversionInsuOtherWorkstoreList() {
		return pmsQtyConversionInsuOtherWorkstoreList;
	}

	public void setPmsQtyConversionInsuOtherWorkstoreList(
			List<MpPmsQtyConversionInsuItem> pmsQtyConversionInsuOtherWorkstoreList) {
		this.pmsQtyConversionInsuOtherWorkstoreList = pmsQtyConversionInsuOtherWorkstoreList;
	}

	public PmsQtyConversionInsu getPmsQtyConversionInsu() {
		return pmsQtyConversionInsu;
	}

	public void setPmsQtyConversionInsu(PmsQtyConversionInsu pmsQtyConversionInsu) {
		this.pmsQtyConversionInsu = pmsQtyConversionInsu;
	}

	public PmsQtyConversionAdj getPmsQtyConversionAdj() {
		return pmsQtyConversionAdj;
	}

	public void setPmsQtyConversionAdj(PmsQtyConversionAdj pmsQtyConversionAdj) {
		this.pmsQtyConversionAdj = pmsQtyConversionAdj;
	}
	
	

}