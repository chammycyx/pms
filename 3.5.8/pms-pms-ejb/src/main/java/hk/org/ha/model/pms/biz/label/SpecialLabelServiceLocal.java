package hk.org.ha.model.pms.biz.label;

import hk.org.ha.model.pms.vo.label.SpecialLabel;

import javax.ejb.Local;

@Local
public interface SpecialLabelServiceLocal {
	
	public void retrieveSpecialLabel(String labelNum);
	
	void updateSpecialLabel(SpecialLabel specialLabel);

	void removeSpecialLabel(String labelNum);
	
	void destroy();
}
