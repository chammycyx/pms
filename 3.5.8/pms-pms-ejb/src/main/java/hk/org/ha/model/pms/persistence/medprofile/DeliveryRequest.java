package hk.org.ha.model.pms.persistence.medprofile;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.pivas.MaterialRequestItem;
import hk.org.ha.model.pms.udt.medprofile.DeliveryItemStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryRequestOrderType;
import hk.org.ha.model.pms.udt.medprofile.DeliveryRequestStatus;
import hk.org.ha.model.pms.vo.charging.FcsPaymentStatus;
import hk.org.ha.model.pms.vo.medprofile.Ward;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.lang3.ObjectUtils;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.PrivateOwned;

@Entity
@Table(name = "DELIVERY_REQUEST")
public class DeliveryRequest extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "deliveryRequestSeq")
	@SequenceGenerator(name = "deliveryRequestSeq", sequenceName = "SQ_DELIVERY_REQUEST", initialValue = 100000000)
	private Long id;
	
	@Lob
	@Column(name = "WARD_CODE_CSV")
	private String wardCodeCsv;
	
	@Column(name = "CASE_NUM", length = 12)
	private String caseNum;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DUE_DATE")
	private Date dueDate;
	
	@Column(name = "GENERATE_DAY", length = 1)
	private Integer generateDay;
	
	@Column(name = "WORKSTATION_CODE", length = 4)
	private String workstationCode;
	
	@Column(name = "SORT_NAME", length = 10)
	private String sortName;
	
	@Column(name = "DIVIDER_LABEL_FLAG", nullable = false)
	private Boolean dividerLabelFlag;
	
	@Converter(name = "DeliveryRequest.orderType", converterClass = DeliveryRequestOrderType.Converter.class)
	@Convert("DeliveryRequest.orderType")
	@Column(name = "ORDER_TYPE", length = 1)
	private DeliveryRequestOrderType orderType;
	
	@Converter(name = "DeliveryRequest.status", converterClass = DeliveryRequestStatus.Converter.class)
	@Convert("DeliveryRequest.status")
	@Column(name = "STATUS", length = 1)
	private DeliveryRequestStatus status;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE",      referencedColumnName = "HOSP_CODE",      nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;
	
	@ManyToOne
	@JoinColumn(name = "DELIVERY_REQUEST_GROUP_ID")
	private DeliveryRequestGroup deliveryRequestGroup; 
		
	@Column(name = "HOSP_CODE", nullable = false, insertable = false, updatable = false)
	private String hospCode;	
	
	@PrivateOwned
	@OneToMany(mappedBy = "deliveryRequest", cascade = CascadeType.ALL)
	private List<DeliveryRequestAlert> deliveryRequestAlertList;
	
	@Transient
	private Boolean newOrderFlag;

	@Transient
	private Boolean urgentFlag;

	@Transient
	private Boolean statFlag;

	@Transient
	private Boolean refillFlag;
	
	@Transient
	private Boolean materialRequestFlag;
	
	@Transient
	private Boolean sfiFlag;
	
	@Transient
	private Boolean pivasFlag;
	
	@Transient
	private String hkid;

	@Transient
	private List<Ward> wardList;
	
	@Transient
	private List<Long> medProfileIdList;
	
	@Transient
	private List<Long> medProfilePoItemIdList;
	
	@Transient
	private List<Long> purchaseRequestIdList;
	
	@Transient
	private Boolean forceProceedFlag;
	
	@Transient
	private String forceProceedCode;
	
	@Transient
	private String forceProceedUser;
	
	@Transient
	private Date forceProceedDate;
	
	@Transient
	private String manualInvoiceNum;
	
	@Transient
	private String manualReceiptNum;
	
	@Transient
	private Date supplyDate;
	
	@Transient
	private List<MaterialRequestItem> materialRequestItemList;
	
	private transient List<Integer> atdpsPickStationsNumList;
	
	private transient Map<String, FcsPaymentStatus> paymentStatusMap;
	
	private transient Set<Long> medProfilePoItemIdSet;
	
	private transient Set<Long> purchaseRequestIdSet;
	
	private transient Map<Long, MaterialRequestItem> materialRequestItemMap;
	
	private transient Map<Long, Map<Long, MaterialRequestItem>> materialRequestItemMapByMedProfileId;
	
	private transient Set<String> dailyRefillItemCodeSet;
	
	private transient Integer errorCount;
	
	private transient Integer normalItemErrorCount;

	private transient Integer sfiItemErrorCount;
	
	private transient Integer prevNormalItemErrorCount;

	private transient Integer prevSfiItemErrorCount;
	
	private transient Boolean profileNotFoundFlag;
	
	private transient String invalidWardCode;
	
	private transient List<Delivery> deliveryList;
	
	private transient List<RenderAndPrintJob> pendingRenderAndPrintJobList;
	
	private transient Throwable throwable;
	
	private transient final Object mutex = new Object();
	
	public DeliveryRequest() {
		dividerLabelFlag = Boolean.FALSE;
		errorCount = Integer.valueOf(0);
		prevNormalItemErrorCount = Integer.valueOf(0);
		prevSfiItemErrorCount = Integer.valueOf(0);
		normalItemErrorCount = Integer.valueOf(0);
		sfiItemErrorCount = Integer.valueOf(0);
		profileNotFoundFlag = Boolean.FALSE;
		orderType = DeliveryRequestOrderType.None;
		status = DeliveryRequestStatus.Completed;
		forceProceedFlag = Boolean.FALSE;
	}

	@PrePersist
    @PreUpdate
    public void preSave() {
		Set<Ward> wardList = getSortedWardSet();
		if (wardList.size() > 0) {
			wardCodeCsv = toCsvString(wardList);
		}
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWardCodeCsv() {
		if (wardCodeCsv == null) {
			preSave();
		}
		return wardCodeCsv;
	}

	public void setWardCodeCsv(String wardCodeCsv) {
		this.wardCodeCsv = wardCodeCsv;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public Date getDueDate() {
		return dueDate == null ? null : new Date(dueDate.getTime());
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate == null ? null : new Date(dueDate.getTime());
	}

	public Integer getGenerateDay() {
		return generateDay;
	}

	public void setGenerateDay(Integer generateDay) {
		this.generateDay = generateDay;
	}
	
	public String getWorkstationCode() {
		return workstationCode;
	}

	public void setWorkstationCode(String workstationCode) {
		this.workstationCode = workstationCode;
	}
	
	public String getSortName() {
		return sortName;
	}

	public void setSortName(String sortName) {
		this.sortName = sortName;
	}
	
	public Boolean getDividerLabelFlag() {
		return dividerLabelFlag;
	}

	public void setDividerLabelFlag(Boolean dividerLabelFlag) {
		this.dividerLabelFlag = dividerLabelFlag;
	}
	
	public DeliveryRequestOrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(DeliveryRequestOrderType orderType) {
		this.orderType = orderType;
	}
	
	public DeliveryRequestStatus getStatus() {
		return status;
	}

	public void setStatus(DeliveryRequestStatus status) {
		this.status = status;
	}
	
	public void setStatusWithNotification(DeliveryRequestStatus status) {
		
		synchronized (mutex) {
			this.status = status;
			mutex.notifyAll();
		}
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}
	
	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public List<DeliveryRequestAlert> getDeliveryRequestAlertList() {
		if (deliveryRequestAlertList == null) {
			deliveryRequestAlertList = new ArrayList<DeliveryRequestAlert>();
		}
		return deliveryRequestAlertList;
	}

	public void setDeliveryRequestAlertList(
			List<DeliveryRequestAlert> deliveryRequestAlertList) {
		this.deliveryRequestAlertList = deliveryRequestAlertList;
	}
	
	public void addDeliveryRequestAlert(DeliveryRequestAlert deliveryRequestAlert) {
		if (deliveryRequestAlert != null) {
			deliveryRequestAlert.setDeliveryRequest(this);
			getDeliveryRequestAlertList().add(deliveryRequestAlert);
		}
	}
	
	public boolean isBatchPrint() {
		return !getMedProfileIdList().isEmpty();
	}
	
	public Boolean getNewOrderFlag() {
		return newOrderFlag;
	}

	public void setNewOrderFlag(Boolean newOrderFlag) {
		this.newOrderFlag = newOrderFlag;
	}

	public Boolean getUrgentFlag() {
		return urgentFlag;
	}

	public void setUrgentFlag(Boolean urgentFlag) {
		this.urgentFlag = urgentFlag;
	}

	public Boolean getStatFlag() {
		return statFlag;
	}

	public void setStatFlag(Boolean statFlag) {
		this.statFlag = statFlag;
	}

	public Boolean getRefillFlag() {
		return refillFlag;
	}

	public void setRefillFlag(Boolean refillFlag) {
		this.refillFlag = refillFlag;
	}
	
	public Boolean getMaterialRequestFlag() {
		return materialRequestFlag;
	}

	public void setMaterialRequestFlag(Boolean materialRequestFlag) {
		this.materialRequestFlag = materialRequestFlag;
	}

	public Boolean getSfiFlag() {
		return sfiFlag;
	}

	public void setSfiFlag(Boolean sfiFlag) {
		this.sfiFlag = sfiFlag;
	}

	public Boolean getPivasFlag() {
		return pivasFlag;
	}

	public void setPivasFlag(Boolean pivasFlag) {
		this.pivasFlag = pivasFlag;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public List<Ward> getWardList() {
		if (wardList == null) {
			wardList = new ArrayList<Ward>();
		}
		return wardList;
	}

	public void setWardList(List<Ward> wardList) {
		this.wardList = wardList;
	}
	
	public Set<Ward> getSortedWardSet() {
		return toTreeSet(getWardList());
	}
	
	public List<Integer> getAtdpsPickStationsNumList() {
		return atdpsPickStationsNumList;
	}

	public void setAtdpsPickStationsNumList(List<Integer> atdpsPickStationsNumList) {
		this.atdpsPickStationsNumList = atdpsPickStationsNumList;
	}

	public List<Long> getMedProfileIdList() {
		if (medProfileIdList == null) {
			medProfileIdList = new ArrayList<Long>();
		}
		return medProfileIdList;
	}

	public void setMedProfileIdList(List<Long> medProfileIdList) {
		this.medProfileIdList = medProfileIdList;
	}
	
	public List<Long> getMedProfilePoItemIdList() {
		if (medProfilePoItemIdList == null) {
			medProfilePoItemIdList = new ArrayList<Long>();
		}
		return medProfilePoItemIdList;
	}

	public void setMedProfilePoItemIdList(List<Long> medProfilePoItemIdList) {
		this.medProfilePoItemIdList = medProfilePoItemIdList;
	}
	
	public Set<Long> getMedProfilePoItemIdSet() {
		if (medProfilePoItemIdSet == null || medProfilePoItemIdSet.size() != getMedProfilePoItemIdList().size()) {
			medProfilePoItemIdSet = toHashSet(getMedProfilePoItemIdList());
		}
		return medProfilePoItemIdSet;
	}

	public List<Long> getPurchaseRequestIdList() {
		if (purchaseRequestIdList == null) {
			purchaseRequestIdList = new ArrayList<Long>();
		}
		return purchaseRequestIdList;
	}

	public void setPurchaseRequestIdList(List<Long> purchaseRequestIdList) {
		this.purchaseRequestIdList = purchaseRequestIdList;
	}
	
	public Set<Long> getPurchaseRequestIdSet() {
		if (purchaseRequestIdSet == null || purchaseRequestIdSet.size() != getPurchaseRequestIdList().size()) {
			purchaseRequestIdSet = toHashSet(getPurchaseRequestIdList());
		}
		return purchaseRequestIdSet;
	}
	
	public Map<String, FcsPaymentStatus> getPaymentStatusMap() {
		if (paymentStatusMap == null) {
			paymentStatusMap = new HashMap<String, FcsPaymentStatus>();
		}
		return paymentStatusMap;
	}

	public void setPaymentStatusMap(Map<String, FcsPaymentStatus> paymentStatusMap) {
		this.paymentStatusMap = paymentStatusMap;
	}

	public Set<String> getDailyRefillItemCodeSet() {
		if (dailyRefillItemCodeSet == null) {
			dailyRefillItemCodeSet = new HashSet<String>();
		}
		return dailyRefillItemCodeSet;
	}

	public void setDailyRefillItemCodeSet(Set<String> dailyRefillItemCodeSet) {
		this.dailyRefillItemCodeSet = dailyRefillItemCodeSet;
	}
	
	public Integer getSuccessCount() {
		
		int successCount = 0;
		
		for (Delivery delivery: getDeliveryList()) {
			for (DeliveryItem deliveryItem: delivery.getDeliveryItemList()) {
				successCount += deliveryItem.getStatus() == DeliveryItemStatus.Printed ? 1 : 0;
			}
		}
		
		return successCount;
	}
	
	public void markErrorCounts() {
		prevNormalItemErrorCount = normalItemErrorCount;
		prevSfiItemErrorCount = sfiItemErrorCount;
	}
	
	public Integer getErrorCount() {
		return errorCount == null ? 0 : errorCount.intValue();
	}

	public void setErrorCount(Integer errorCount) {
		this.errorCount = errorCount;
	}
	
	public void incrementErrorCount(Collection<DeliveryItem> deliveryItemCollection) {

		errorCount = getErrorCount() + deliveryItemCollection.size();
		
		for (DeliveryItem deliveryItem: deliveryItemCollection) {
			if (deliveryItem.getPurchaseRequest() != null) {
				sfiItemErrorCount = ObjectUtils.defaultIfNull(sfiItemErrorCount, 0) + 1;
			} else {
				normalItemErrorCount = ObjectUtils.defaultIfNull(normalItemErrorCount, 0) + 1;
			}
		}
	}
	
	public boolean hasNewNormalItemError() {
		return ObjectUtils.compare(prevNormalItemErrorCount, normalItemErrorCount) < 0;
	}
	
	public boolean hasNewSfiItemError() {
		return ObjectUtils.compare(prevSfiItemErrorCount, sfiItemErrorCount) < 0;
	}

	public Boolean getProfileNotFoundFlag() {
		return profileNotFoundFlag;
	}

	public void setProfileNotFoundFlag(Boolean profileNotFoundFlag) {
		this.profileNotFoundFlag = profileNotFoundFlag;
	}
	
	public String getInvalidWardCode() {
		return invalidWardCode;
	}

	public void setInvalidWardCode(String invalidWardCode) {
		this.invalidWardCode = invalidWardCode;
	}

	public Boolean getForceProceedFlag() {
		return forceProceedFlag;
	}

	public void setForceProceedFlag(Boolean forceProceedFlag) {
		this.forceProceedFlag = forceProceedFlag;
	}
	
	public String getForceProceedCode() {
		return forceProceedCode;
	}

	public void setForceProceedCode(String forceProceedCode) {
		this.forceProceedCode = forceProceedCode;
	}

	public String getForceProceedUser() {
		return forceProceedUser;
	}

	public void setForceProceedUser(String forceProceedUser) {
		this.forceProceedUser = forceProceedUser;
	}

	public Date getForceProceedDate() {
		return forceProceedDate == null ? null : new Date(forceProceedDate.getTime());
	}

	public void setForceProceedDate(Date forceProceedDate) {
		this.forceProceedDate = forceProceedDate == null ? null : new Date(forceProceedDate.getTime());
	}
	
	public String getManualInvoiceNum() {
		return manualInvoiceNum;
	}
	
	public void setManualInvoiceNum(String manualInvoiceNum) {
		this.manualInvoiceNum = manualInvoiceNum;
	}

	public String getManualReceiptNum() {
		return manualReceiptNum;
	}

	public void setManualReceiptNum(String manualReceiptNum) {
		this.manualReceiptNum = manualReceiptNum;
	}
	
	public Date getSupplyDate() {
		return supplyDate;
	}

	public void setSupplyDate(Date supplyDate) {
		this.supplyDate = supplyDate;
	}
	
	public List<MaterialRequestItem> getMaterialRequestItemList() {
		if (materialRequestItemList == null) {
			materialRequestItemList = new ArrayList<MaterialRequestItem>();
		}
		return materialRequestItemList;
	}

	public void setMaterialRequestItemList(
			List<MaterialRequestItem> materialRequestItemList) {
		this.materialRequestItemList = materialRequestItemList;
	}
	
	public Map<Long, MaterialRequestItem> getMaterialRequestItemMap() {
		updateMaterialRequestRelatedMap();
		return materialRequestItemMap;
	}
	
	public Map<Long, Map<Long, MaterialRequestItem>> getMaterialRequestItemMapByMedProfileId() {
		updateMaterialRequestRelatedMap();
		return materialRequestItemMapByMedProfileId;
	}
	
	private void updateMaterialRequestRelatedMap() {

		if (this.materialRequestItemMap == null || this.materialRequestItemMap.size() != getMaterialRequestItemList().size()) {
			
			Map<Long, MaterialRequestItem> materialRequestItemMap = new HashMap<Long, MaterialRequestItem>();
			Map<Long, Map<Long, MaterialRequestItem>> materialRequestItemMapByMedProfileId = new HashMap<Long, Map<Long, MaterialRequestItem>>();
			
			for (MaterialRequestItem materialRequestItem: getMaterialRequestItemList()) {
				
				materialRequestItemMap.put(materialRequestItem.getId(), materialRequestItem);
				
				Long medProfileId = materialRequestItem.getMaterialRequest().getMedProfileId();
				Map<Long, MaterialRequestItem> partialMap = materialRequestItemMapByMedProfileId.get(medProfileId);
				if (partialMap == null) {
					partialMap = new HashMap<Long, MaterialRequestItem>();
					materialRequestItemMapByMedProfileId.put(medProfileId, partialMap);
				}
				partialMap.put(materialRequestItem.getId(), materialRequestItem);
			}
			
			synchronized (mutex) {
				this.materialRequestItemMap = materialRequestItemMap;
				this.materialRequestItemMapByMedProfileId = materialRequestItemMapByMedProfileId;
			}
		}
	}
	
	public List<Delivery> getDeliveryList() {
		if (deliveryList == null) {
			deliveryList = new ArrayList<Delivery>();
		}
		return deliveryList;
	}

	public void setDeliveryList(List<Delivery> deliveryList) {
		this.deliveryList = deliveryList;
	}
	
	public List<RenderAndPrintJob> getPendingRenderAndPrintJobList() {
		if (pendingRenderAndPrintJobList == null) {
			pendingRenderAndPrintJobList = new ArrayList<RenderAndPrintJob>();
		}
		return pendingRenderAndPrintJobList;
	}
	
	public void setPendingRenderAndPrintJobList(List<RenderAndPrintJob> pendingRenderAndPrintJobList) {
		this.pendingRenderAndPrintJobList = pendingRenderAndPrintJobList;
	}

	public DeliveryRequestGroup getDeliveryRequestGroup() {
		return deliveryRequestGroup;
	}

	public void setDeliveryRequestGroup(DeliveryRequestGroup deliveryRequestGroup) {
		this.deliveryRequestGroup = deliveryRequestGroup;
	}

	public Throwable getThrowable() {
		return throwable;
	}

	public void setThrowable(Throwable throwable) {
		this.throwable = throwable;
	}

	public Date getRefillDueDate() {
		if (dueDate == null || generateDay == null)
			return null;
		else
			return new Date(dueDate.getTime() + 86400000 * (generateDay.intValue() - 1));
	}
	
	public boolean isCaseNumOrHkidSelected() {
		return caseNum != null || hkid != null;
	}

	public boolean containsAnyNew() {
		return Boolean.TRUE.equals(newOrderFlag) || Boolean.TRUE.equals(urgentFlag) || Boolean.TRUE.equals(statFlag);
	}
	
	public boolean isNormalGenerationRequest() {
		return containsAnyNew() || Boolean.TRUE.equals(refillFlag);
	}
	
	public boolean isSfiGenerationRequest() {
		return Boolean.TRUE.equals(sfiFlag);
	}
	
	public boolean isSortByLocation() {
		return sortName != null && sortName.length() > 1 && sortName.charAt(1) == 'L';
	}
	
	public boolean shouldSortByWhole() {
		return Boolean.TRUE.equals(pivasFlag) || isSortByLocation();
	}
	
	public boolean isCompleted(long timeout) {
		
		synchronized (mutex) {
			if (status == DeliveryRequestStatus.Completed) {
				return true;
			}
			try {
				mutex.wait(timeout);
			} catch (Exception ex) {
			}
			return status == DeliveryRequestStatus.Completed;
		}
	}
	
    private String toCsvString(Collection<Ward> list) {
    	
    	StringBuilder sb = new StringBuilder();
    	if (list != null) {
	    	for (Ward ward: list) {
	    		if (sb.length() > 0)
	    			sb.append(",");
	    		sb.append(ward.getWardCode());
	    	}
    	}
    	return sb.toString();
    }
    
    private <E> Set<E> toHashSet(List<E> list) {
    	Set<E> resultSet = new HashSet<E>();
    	resultSet.addAll(list);
    	return resultSet;
    }
    
    private <E> Set<E> toTreeSet(List<E> list) {
    	Set<E> resultSet = new TreeSet<E>();
    	resultSet.addAll(list);
    	return resultSet;
    }
}
