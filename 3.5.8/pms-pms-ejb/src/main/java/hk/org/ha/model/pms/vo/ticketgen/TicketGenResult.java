package hk.org.ha.model.pms.vo.ticketgen;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.udt.charging.ClearanceStatus;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class TicketGenResult implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private ClearanceStatus stdDrugClearance;
	
	private String orderNumforDisplay;
	
	private String infoMessageCode;

	private String errorMessageCode;
	
	private Integer totalUnitOfCharge;
	
	private Ticket ticket;

	public TicketGenResult() {
		super();
		infoMessageCode = StringUtils.EMPTY;
		errorMessageCode = StringUtils.EMPTY;
		totalUnitOfCharge = Integer.valueOf(0);
	}
	
	public ClearanceStatus getStdDrugClearance() {
		return stdDrugClearance;
	}

	public void setStdDrugClearance(ClearanceStatus stdDrugClearance) {
		this.stdDrugClearance = stdDrugClearance;
	}

	public String getOrderNumforDisplay() {
		return orderNumforDisplay;
	}

	public void setOrderNumforDisplay(String orderNumforDisplay) {
		this.orderNumforDisplay = orderNumforDisplay;
	}

	public Ticket getTicket() {
		return ticket;
	}
	
	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public void setInfoMessageCode(String infoMessageCode) {
		this.infoMessageCode = infoMessageCode;
	}

	public String getInfoMessageCode() {
		return infoMessageCode;
	}

	public void setErrorMessageCode(String errorMessageCode) {
		this.errorMessageCode = errorMessageCode;
	}

	public String getErrorMessageCode() {
		return errorMessageCode;
	}

	public void setTotalUnitOfCharge(Integer totalUnitOfCharge) {
		this.totalUnitOfCharge = totalUnitOfCharge;
	}

	public Integer getTotalUnitOfCharge() {
		return totalUnitOfCharge;
	}

}
