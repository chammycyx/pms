package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.vo.report.EdsWaitTimeRpt;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface EdsWaitTimeRptServiceLocal {

	void retrieveEdsWaitTimeRpt(Integer selectedTab, Date date);
	
	List<EdsWaitTimeRpt> retrieveEdsWaitTimeRptList(Date date);
	
	void generateEdsWaitTimeRpt();
	
	void printEdsWaitTimeRpt();
	
	void exportEdsWaitTimeRpt();
	
	void destroy();
	
}
