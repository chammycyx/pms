package hk.org.ha.model.pms.biz.drug;

import java.math.BigDecimal;
import java.util.List;

import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.vo.rx.RxItem;

import javax.ejb.Local;

@Local
public interface ItemConvertServiceLocal {

	void convertMedOrderItem(MedOrderItem medOrderItem);
		
	void recalPharmOrderItem(PharmOrderItem inPharmOrderItem);
	
	List<BigDecimal> calSubTotalDosage(RxItem rxItem, Integer doseGroupIndex, Integer doseIndex);
	
	void destroy();
}
