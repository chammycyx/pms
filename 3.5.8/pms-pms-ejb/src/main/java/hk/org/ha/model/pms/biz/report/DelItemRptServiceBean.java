package hk.org.ha.model.pms.biz.report;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.CapdVoucher;
import hk.org.ha.model.pms.persistence.disp.CapdVoucherItem;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.prop.Prop;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.report.PrescRptStatus;
import hk.org.ha.model.pms.udt.vetting.CapdVoucherStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.label.DispLabel;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.DelCapdVoucherRpt;
import hk.org.ha.model.pms.vo.report.DelCapdVoucherRptDtl;
import hk.org.ha.model.pms.vo.report.DelCapdVoucherRptFtr;
import hk.org.ha.model.pms.vo.report.DelCapdVoucherRptHdr;
import hk.org.ha.model.pms.vo.report.DelCapdVoucherRptItemDtl;
import hk.org.ha.model.pms.vo.report.DelDispItemRpt;
import hk.org.ha.model.pms.vo.report.DelDispItemRptDtl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;


@Stateful
@Scope(ScopeType.SESSION)
@Name("delItemRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DelItemRptServiceBean implements DelItemRptServiceLocal {
	@PersistenceContext
	private EntityManager em;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent; 
	
	@In 
	private Workstore workstore;

	private boolean retrieveSuccess;
	
	private List<DelDispItemRpt> delDispItemRptList;
	
	private List<DelCapdVoucherRpt> delCapdVoucherRptList;
	
	private String currentRpt = null;
	
	private List<?> reportObj = null;
	
	private Map<String, Object> parameters = new HashMap<String, Object>();
	
	private DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");

	public void retrieveDelItemRpt(Integer selectedTab, String workstoreCode, Date dispDate) {
		parameters.put("hospCode", workstore.getHospCode());		
		parameters.put("workstoreCode", workstore.getWorkstoreCode());
		parameters.put("hospName", Prop.HOSPITAL_NAMEENG.get());
		parameters.put("dispDate", df.format(dispDate));
		
		if(selectedTab == 0){
			currentRpt = "DelDispItemRpt";
			reportObj = retrieveDelDispItemRptList(workstoreCode, dispDate);
		}
		else if(selectedTab == 1){
			currentRpt = "DelCapdVoucherRpt";
			reportObj = retrieveDelCapdVoucherRptList(workstoreCode, dispDate);
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<DelDispItemRpt> retrieveDelDispItemRptList(String workstoreCode, Date dispDate) {
		delDispItemRptList = new ArrayList<DelDispItemRpt>();
		
		List<DispOrderItem> dispOrderItemList = em.createQuery(
							"SELECT o FROM DispOrderItem o" + // 20121112 index check : DispOrder.workstore,dispDate : I_DISP_ORDER_03
							" WHERE o.dispOrder.workstore.hospCode = :hospCode" +
							" AND o.dispOrder.workstore.workstoreCode = :workstoreCode" +
							" AND o.dispOrder.pharmOrder.medOrder.orderType = :orderType" +
							" AND o.status = :dispOrderItemStatusDeleted" +
							" AND o.status <> :dispOrderItemStatus" +
							" AND o.pharmOrderItem.itemCode <> 'SAMP01'" +
							" AND o.dispOrder.dispDate = :dispDate" +
							" ORDER BY o.dispOrder.dispDate, o.dispOrder.ticket.ticketNum")
							.setParameter("hospCode", workstore.getHospCode())
							.setParameter("workstoreCode", workstoreCode)
							.setParameter("orderType", OrderType.OutPatient)
							.setParameter("dispOrderItemStatusDeleted", DispOrderItemStatus.Deleted)
							.setParameter("dispOrderItemStatus", DispOrderItemStatus.KeepRecord)
							.setParameter("dispDate", dispDate, TemporalType.DATE)
							.setHint(QueryHints.FETCH, "o.pharmOrderItem")
							.setHint(QueryHints.BATCH, "o.dispOrder.pharmOrder.medOrder.patient")
							.setHint(QueryHints.BATCH, "o.dispOrder.pharmOrder.medCase")
							.setHint(QueryHints.BATCH, "o.dispOrder.ticket")
							.setHint(QueryHints.BATCH, "o.dispOrder.workstore")
							.getResultList();
		
		delDispItemRptList = genDelDispItemRptList(dispOrderItemList, dispDate);
		
		if ( !delDispItemRptList.isEmpty() ) {
			retrieveSuccess = true;
		}
		else{
			retrieveSuccess = false;
		}
		return delDispItemRptList;
	}
	
	private List<DelDispItemRpt> genDelDispItemRptList(List<DispOrderItem> dispOrderItemList, Date dispDate) {
		delDispItemRptList = new ArrayList<DelDispItemRpt>();
		String preTicketNum = StringUtils.EMPTY;
		DelDispItemRpt delDispItemRpt = new DelDispItemRpt();
		
		for ( DispOrderItem dispOrderItem : dispOrderItemList ) {
			PharmOrder po = dispOrderItem.getDispOrder().getPharmOrder();
			Ticket ticket = dispOrderItem.getDispOrder().getTicket();
			
			if ( !StringUtils.trimToEmpty(preTicketNum).equals(StringUtils.trimToEmpty(dispOrderItem.getDispOrder().getTicket().getTicketNum())) ) {
				delDispItemRpt = new DelDispItemRpt();
				
				if (ticket!= null) {
					delDispItemRpt.setTicketNum(dispOrderItem.getDispOrder().getTicket().getTicketNum());
				}
				delDispItemRpt.setPatType(dispOrderItem.getDispOrder().getPharmOrder().getMedOrder().getOrderType().getDataValue());
				
				MedCase medCase = po.getMedCase();
				if (medCase != null) {
					delDispItemRpt.setCaseNum(medCase.getCaseNum());
				}
				
				delDispItemRpt.setHkid(po.getPatient().getHkid());
				delDispItemRpt.setPatName(po.getMedOrder().getPatient().getName());
				delDispItemRpt.setPatNameChi(po.getMedOrder().getPatient().getNameChi());
				
				if (!po.getMedOrder().getDocType().equals(MedOrderDocType.Manual)) {
					delDispItemRpt.setRefNum(po.getMedOrder().getRefNum());
				}

				delDispItemRpt.setDelDispItemRptDtlList(new ArrayList<DelDispItemRptDtl>());
				delDispItemRptList.add(delDispItemRpt);
				
				preTicketNum = dispOrderItem.getDispOrder().getTicket().getTicketNum();
			}
			
			DelDispItemRptDtl delDispItemRptDtl = new DelDispItemRptDtl();
			
			delDispItemRptDtl.setItemCode(dispOrderItem.getPharmOrderItem().getItemCode());
			
			if ( dispOrderItem.getBaseLabel() != null ) {
				DispLabel dispLabel = (DispLabel)dispOrderItem.getBaseLabel();
				dispLabel.setPrintOption(new PrintOption());
				delDispItemRptDtl.setLabelDesc(dispOrderItem.getBaseLabel().getItemDesc());
				delDispItemRptDtl.setLabelInstructionText(dispOrderItem.getBaseLabel().getInstructionText());
			} 
			
			delDispItemRptDtl.setDurationInDay(dispOrderItem.getDurationInDay());
			delDispItemRptDtl.setDispQty(dispOrderItem.getDispQty().intValue());
			delDispItemRptDtl.setStatus(this.getStatus(dispOrderItem));
			delDispItemRptDtl.setDoctorCode(dispOrderItem.getDispOrder().getPharmOrder().getDoctorCode());
			
			delDispItemRpt.getDelDispItemRptDtlList().add(delDispItemRptDtl);
		}
	
		return delDispItemRptList;
	}
	
	private String getStatus(DispOrderItem dispOrderItem){
		String status = "";
		
		if (dispOrderItem.getDispOrder().getStatus() == DispOrderStatus.SysDeleted || dispOrderItem.getDispOrder().getStatus() == DispOrderStatus.Deleted) {
			if (dispOrderItem.getPharmOrderItem().getMedOrderItem().getCapdRxDrug() != null && dispOrderItem.getPharmOrderItem().getCapdVoucherFlag()) {
				status = PrescRptStatus.DelCapd.getDisplayValue();
			}else {
				status = PrescRptStatus.Deleted.getDisplayValue();
			}
		} else if ( !dispOrderItem.getDispOrder().getAdminStatus().equals(DispOrderAdminStatus.Normal) ) {//Suspended
			status = PrescRptStatus.Suspended.getDisplayValue();
		} else if ( dispOrderItem.getPharmOrderItem().getMedOrderItem().getCapdRxDrug() != null && dispOrderItem.getPharmOrderItem().getCapdVoucherFlag() ){
			status = PrescRptStatus.Capd.getDisplayValue();
		} else {
			status = dispOrderItem.getStatus().getDisplayValue();
			switch (dispOrderItem.getStatus()) {
				case Vetted:
					status = PrescRptStatus.Vetted.getDisplayValue();
					break;
				case Picked:
					status = PrescRptStatus.Picked.getDisplayValue();
					break;
				case Assembled:
					status = PrescRptStatus.Assembled.getDisplayValue();
					break;
				case Checked:
					status = PrescRptStatus.Checked.getDisplayValue();
					break;
				case Issued:
					status = PrescRptStatus.Issued.getDisplayValue();
					break;
				case KeepRecord:
					status = "No Label";
					break;
			}
		}
		
		return status;
	}
	
	@SuppressWarnings("unchecked")
	private List<DelCapdVoucherRpt> retrieveDelCapdVoucherRptList(String workstoreCode, Date dispDate) {
		
		delCapdVoucherRptList = new ArrayList<DelCapdVoucherRpt>();

		List<DispOrderStatus> doStatusList = new ArrayList<DispOrderStatus>();
		doStatusList.add(DispOrderStatus.Deleted);
		doStatusList.add(DispOrderStatus.SysDeleted);
		
		em.createQuery(
				"SELECT o FROM DispOrderItem o" + // 20121112 index check : DispOrder.workstore,dispDate : I_DISP_ORDER_03
				" WHERE o.dispOrder.workstore.hospCode = :hospCode" +
				" AND o.dispOrder.workstore.workstoreCode = :workstoreCode" +
				" AND o.dispOrder.pharmOrder.medOrder.orderType = :orderType" +
				" AND o.dispOrder.dispDate = :dispDate" +
				" AND o.dispOrder.status in :doStatusList" +
				" AND o.pharmOrderItem.capdVoucherFlag = :capdVoucherFlag")
				.setParameter("hospCode", workstore.getHospCode())
				.setParameter("workstoreCode", workstoreCode)
				.setParameter("orderType", OrderType.OutPatient)
				.setParameter("dispDate", dispDate, TemporalType.DATE)
				.setParameter("doStatusList", doStatusList)
				.setParameter("capdVoucherFlag", true)
				.setHint(QueryHints.FETCH, "o.pharmOrderItem")
				.setHint(QueryHints.BATCH, "o.dispOrder.pharmOrder.medOrder.patient")
				.setHint(QueryHints.BATCH, "o.dispOrder.pharmOrder.medCase")
				.setHint(QueryHints.BATCH, "o.dispOrder.ticket")
				.setHint(QueryHints.BATCH, "o.dispOrder.workstore")
				.getResultList();
		
		List<CapdVoucher> capdVoucherList = em.createQuery(
							"SELECT o FROM CapdVoucher o" + // 20121112 index check : DispOrder.workstore,dispDate : I_DISP_ORDER_03
							" WHERE o.dispOrder.workstore.hospCode = :hospCode" +
							" AND o.dispOrder.workstore.workstoreCode = :workstoreCode" +
							" AND o.dispOrder.pharmOrder.medOrder.orderType = :orderType" +
							" AND o.dispOrder.dispDate = :dispDate" +
							" AND ( o.dispOrder.status = :dispOrderStatusDeleted" +
							" OR ( o.status = :capdVoucherStatus AND o.dispOrder.status <> :dispOrderStatusSysDeleted )" +
							" OR o.dispOrder.pharmOrder.medOrder.unvetFlag = :unvetFlag )" +
							" ORDER BY o.voucherNum," +
							" o.dispOrder.dispDate, " +
							" o.dispOrder.ticket.ticketNum ")
							.setParameter("hospCode", workstore.getHospCode())
							.setParameter("workstoreCode", workstoreCode)
							.setParameter("orderType", OrderType.OutPatient)
							.setParameter("dispDate", dispDate, TemporalType.DATE)
							.setParameter("dispOrderStatusDeleted", DispOrderStatus.Deleted)
							.setParameter("capdVoucherStatus", CapdVoucherStatus.Deleted)
							.setParameter("dispOrderStatusSysDeleted", DispOrderStatus.SysDeleted)
							.setParameter("unvetFlag", true)
							.setHint(QueryHints.BATCH, "o.capdVoucherItemList")
							.getResultList();

		int capdVoucherDelCount = 0;
		int capdItemDelCount = 0;
		
		DelCapdVoucherRpt delCapdVoucherRpt = new DelCapdVoucherRpt();
		delCapdVoucherRptList.add(delCapdVoucherRpt);
		
		List<DelCapdVoucherRptDtl> delCapdVoucherRptDtlList = new ArrayList<DelCapdVoucherRptDtl>();
		delCapdVoucherRpt.setDelCapdVoucherRptDtlList(delCapdVoucherRptDtlList);
			 
		DelCapdVoucherRptHdr delCapdVoucherRptHdr = new DelCapdVoucherRptHdr();
		delCapdVoucherRptHdr.setHospCode(workstore.getHospCode());
		delCapdVoucherRptHdr.setWorkstoreCode(workstoreCode);
		delCapdVoucherRptHdr.setHospName(Prop.HOSPITAL_NAMEENG.get());
		delCapdVoucherRptHdr.setDispDate(dispDate);
		delCapdVoucherRpt.setDelCapdVoucherRptHdr(delCapdVoucherRptHdr);
		
		List<DelCapdVoucherRptItemDtl> delCapdVoucherRptItemDtlList = new ArrayList<DelCapdVoucherRptItemDtl>();
		String preTicketVoucherNum = StringUtils.EMPTY;
		
		for (CapdVoucher capdVoucher:capdVoucherList) {
			if ( !StringUtils.trimToEmpty(preTicketVoucherNum).equals(StringUtils.trimToEmpty(capdVoucher.getDispOrder().getTicket().getTicketNum())+StringUtils.trimToEmpty(capdVoucher.getVoucherNum())) ) {
				capdVoucherDelCount++;
				delCapdVoucherRptItemDtlList = new ArrayList<DelCapdVoucherRptItemDtl>();
				DelCapdVoucherRptDtl delCapdVoucherRptDtl = new DelCapdVoucherRptDtl();
				delCapdVoucherRptDtl.setDispDate(capdVoucher.getDispOrder().getDispDate());
				delCapdVoucherRptDtl.setTicketNum(capdVoucher.getDispOrder().getTicket().getTicketNum());
				
				MedCase medCase = capdVoucher.getDispOrder().getPharmOrder().getMedCase();
				if (medCase != null) {
					delCapdVoucherRptDtl.setCaseNum(medCase.getCaseNum());
				}
				
				Patient patient = capdVoucher.getDispOrder().getPharmOrder().getMedOrder().getPatient();
				delCapdVoucherRptDtl.setPatName(patient.getName());
				delCapdVoucherRptDtl.setHkid(patient.getHkid());
				
				delCapdVoucherRptDtl.setVoucherNum(capdVoucher.getVoucherNum());
				
				delCapdVoucherRptDtlList.add(delCapdVoucherRptDtl);
				delCapdVoucherRptDtl.setDelCapdVoucherRptItemDtlList(delCapdVoucherRptItemDtlList);
				preTicketVoucherNum = StringUtils.trimToEmpty(capdVoucher.getDispOrder().getTicket().getTicketNum())+StringUtils.trimToEmpty(capdVoucher.getVoucherNum());
			}
			
			for (CapdVoucherItem capdVoucherItem:capdVoucher.getCapdVoucherItemList()){
				DelCapdVoucherRptItemDtl delCapdVoucherRptItemDtl = new DelCapdVoucherRptItemDtl();
				delCapdVoucherRptItemDtl.setItemCode(capdVoucherItem.getDispOrderItem().getPharmOrderItem().getItemCode());
				delCapdVoucherRptItemDtl.setDrugDesc(capdVoucherItem.getDrugDesc());
				delCapdVoucherRptItemDtl.setDispQty(capdVoucherItem.getQty());
				delCapdVoucherRptItemDtl.setBaseUnit(capdVoucherItem.getDispOrderItem().getPharmOrderItem().getBaseUnit());
				
				delCapdVoucherRptItemDtlList.add(delCapdVoucherRptItemDtl);
				
				capdItemDelCount++;
			}
			
		}

		DelCapdVoucherRptFtr delCapdVoucherRptFtr = new DelCapdVoucherRptFtr();
		delCapdVoucherRptFtr.setCapdVoucherDelCount(capdVoucherDelCount);
		delCapdVoucherRptFtr.setCapdItemDelCount(capdItemDelCount);
		
		delCapdVoucherRpt.setDelCapdVoucherRptFtr(delCapdVoucherRptFtr);
		
		if ( !delCapdVoucherRptList.isEmpty() ) {
			retrieveSuccess = true;
		}
		else{
			retrieveSuccess = false;
		}
		return delCapdVoucherRptList;
	}
	
	public void generateDelItemRpt() {
		
		printAgent.renderAndRedirect(new RenderJob(
				currentRpt, 
				new PrintOption(), 
				new HashMap<String, Object>(parameters),
				getReportObj()));

	}
	
	public void printDelItemRpt(){

		printAgent.renderAndPrint(new RenderAndPrintJob(
				currentRpt, 
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				new HashMap<String, Object>(parameters),
				getReportObj()));
		
	}

	@Remove
	public void destroy() {
		
		if(delDispItemRptList!=null){
			delDispItemRptList=null;
		}
		
		if(delCapdVoucherRptList!=null){
			delCapdVoucherRptList=null;
		}
		
	}

	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}

	public List<?> getReportObj() {
		return reportObj;
	}
	
}
