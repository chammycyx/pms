package hk.org.ha.model.pms.biz.delivery;
import javax.ejb.Local;

@Local
public interface FollowUpItemListServiceLocal {
	void retrieveFollowUpItemList();
	void destroy();
}
