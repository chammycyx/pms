package hk.org.ha.model.pms.biz.reftable;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.RefillHoliday;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateless
@Name("refillHolidayManager")
@Restrict("#{identity.loggedIn}")
@MeasureCalls
public class RefillHolidayManagerBean implements RefillHolidayManagerLocal {

	@PersistenceContext
	private EntityManager em;
	
	@In
	private Workstore workstore;

	@SuppressWarnings("unchecked")
	public List<RefillHoliday> retrieveRefillHolidayList()
	{
		return em.createQuery(
				"select o from RefillHoliday o" + // 20120303 index check : RefillHoliday.workstore : FK_REFILL_HOLIDAY_01
				" where o.workstore = :workstore" +
				" order by o.holiday")
			.setParameter("workstore", workstore)
			.getResultList();
	}
}
