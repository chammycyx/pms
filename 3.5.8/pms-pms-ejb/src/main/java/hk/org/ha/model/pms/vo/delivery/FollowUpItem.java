package hk.org.ha.model.pms.vo.delivery;

import hk.org.ha.model.pms.udt.medprofile.DeliveryRequestOrderType;
import hk.org.ha.model.pms.udt.medprofile.ErrorLogType;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class FollowUpItem implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Date dueDate;
	
	private DeliveryRequestOrderType orderType;
	
	private Integer generateDay;
	
	private String caseNum;
	
	private ErrorLogType type;	

	public FollowUpItem() {
		super();
	}

	public FollowUpItem(Date dueDate, DeliveryRequestOrderType orderType, Integer generateDay, String caseNum, ErrorLogType type) {
		super();
		this.dueDate = dueDate;
		this.orderType = orderType;
		this.generateDay = generateDay;
		this.caseNum = caseNum;
		this.type = type;
	}
	
	public Date getDueDate() {
		return dueDate == null ? null : new Date(dueDate.getTime());
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate == null ? null : new Date(dueDate.getTime());
	}
	
	public DeliveryRequestOrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(DeliveryRequestOrderType orderType) {
		this.orderType = orderType;
	}

	public Integer getGenerateDay() {
		return generateDay;
	}

	public void setGenerateDay(Integer generateDay) {
		this.generateDay = generateDay;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public ErrorLogType getType() {
		return type;
	}

	public void setType(ErrorLogType type) {
		this.type = type;
	}
}
