package hk.org.ha.model.pms.udt.support;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PropType implements StringValuedEnum {

	CorporateProp("CorporateProp", "Corporate Property"),
	HospitalProp("HospitalProp", "Hospital Property"), 
	WorkstoreProp("WorkstoreProp", "Workstore Property"), 
	WorkstationProp("WorkstationProp", "Workstation Property"),
	OperationProp("OperationProp", "Operation Property");	
	
    private final String dataValue;
    private final String displayValue;

    PropType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    

    public static PropType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PropType.class, dataValue);
	}
    
	public static class Converter extends StringValuedEnumConverter<PropType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<PropType> getEnumClass() {
    		return PropType.class;
    	}
    }
}



