package hk.org.ha.model.pms.biz.enquiry;

import static hk.org.ha.model.pms.prop.Prop.CHARGING_SFI_SUSPEND_ORDER_SUSPENDCODE;
import static hk.org.ha.model.pms.prop.Prop.CHARGING_SFI_WAIVE_MEDCASE_PASPATGROUPCODE;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.InvoiceItem;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.persistence.medprofile.PurchaseRequest;
import hk.org.ha.model.pms.persistence.reftable.ChargeReason;
import hk.org.ha.model.pms.udt.YesNoFlag;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.PharmOrderPatType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.enquiry.SfiInvoiceInfo;
import hk.org.ha.model.pms.vo.security.PropMap;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("sfiInvoiceEnqManager")
@MeasureCalls
public class SfiInvoiceEnqManagerBean implements SfiInvoiceEnqManagerLocal {

	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
		
	@In
	private PropMap propMap;

	private static final String INVOICE_STATUS_ACTIVE = "Active";
	private static final String INVOICE_STATUS_VOIDED = "Voided";
	private static final String DISPORDER_STATUS_SFI_SUSPEND= "Await SFI payment";
	private static final String FORCE_PROCEED_INVOICE = "Standalone Inv No.: ";
	private static final String FORCE_PROCEED_RECEIPT = "Receipt No.: ";
	private static final String MP_SFI_INVOICE_NUM = "0000";
	
	public List<SfiInvoiceInfo> convertInvoiceListToInvoiceSummaryForSfiInvoiceEnq(List<Invoice> invoiceList){
		return new ArrayList<SfiInvoiceInfo>(convertInvoiceListToInvoiceSummary(invoiceList).values());
	}
		
	public Map<String, SfiInvoiceInfo> convertInvoiceListToInvoiceSummary(List<Invoice> invoiceList){
		Map<String, SfiInvoiceInfo> sfiInvoiceSummaryMap = new HashMap<String, SfiInvoiceInfo>();
		Map<String, String> forceProceedReasonMap = new HashMap<String, String>(); 
		
		for( Invoice invoice: invoiceList ){
			DispOrder dispOrder = invoice.getDispOrder();
			PharmOrder pharmOrder = dispOrder.getPharmOrder();
			
			SfiInvoiceInfo sfiInvoiceInfo = new SfiInvoiceInfo();
			sfiInvoiceInfo.setInvoiceId(invoice.getId());
			
			if( DispOrderAdminStatus.Suspended == dispOrder.getAdminStatus() && DispOrderStatus.Deleted != dispOrder.getStatus() && DispOrderStatus.SysDeleted != dispOrder.getStatus() ){
					
				if( StringUtils.equals(dispOrder.getSuspendCode(),CHARGING_SFI_SUSPEND_ORDER_SUSPENDCODE.get()) ){
					sfiInvoiceInfo.setDispOrderStatus(DISPORDER_STATUS_SFI_SUSPEND);
				}else{
					sfiInvoiceInfo.setDispOrderStatus(dispOrder.getAdminStatus().getDisplayValue());
				}
			}else{
				sfiInvoiceInfo.setDispOrderStatus(invoice.getDispOrder().getStatus().getDisplayValue());
			}
						
			if( invoice.getForceProceedFlag() ){
				sfiInvoiceInfo.setForceProceedFlag(YesNoFlag.Yes.getDataValue());
				StringBuilder displayReason = new StringBuilder();
				
				if( StringUtils.isNotBlank(invoice.getManualInvoiceNum())){
					sfiInvoiceInfo.setForceProceedInvoiceNum(invoice.getManualInvoiceNum());
					displayReason.append(FORCE_PROCEED_INVOICE).append(invoice.getManualInvoiceNum());
					sfiInvoiceInfo.setDisplayForceProceedReason(displayReason.toString());
				}else if( StringUtils.isNotBlank(invoice.getManualReceiptNum()) ){
					sfiInvoiceInfo.setForceProceedReceiptNum(invoice.getManualReceiptNum());
					displayReason.append(FORCE_PROCEED_RECEIPT).append(invoice.getManualReceiptNum());
					sfiInvoiceInfo.setDisplayForceProceedReason(displayReason.toString());
				}else{
					if( forceProceedReasonMap.get(invoice.getForceProceedCode()) == null ){
						String forceProceedReaonDesc = retrieveForceProceedChargeReasonByReasonCode(invoice.getForceProceedCode());
						sfiInvoiceInfo.setForceProceedReason(forceProceedReaonDesc);
						forceProceedReasonMap.put(invoice.getForceProceedCode(), forceProceedReaonDesc);
					}else{
						sfiInvoiceInfo.setForceProceedReason(forceProceedReasonMap.get(invoice.getForceProceedCode()));
					}
					sfiInvoiceInfo.setDisplayForceProceedReason(sfiInvoiceInfo.getForceProceedReason());
				}
				sfiInvoiceInfo.setForceProceedBy(invoice.getForceProceedUser());				
				
			}else{
				sfiInvoiceInfo.setForceProceedFlag(YesNoFlag.No.getDataValue());
			}
			sfiInvoiceInfo.setHkid(pharmOrder.getMedOrder().getPatient().getHkid());
			if( invoice.getInvoiceDate() == null ){
				sfiInvoiceInfo.setInvoiceDate(invoice.getCreateDate());
			}else{
				sfiInvoiceInfo.setInvoiceDate(invoice.getInvoiceDate());
			}
			sfiInvoiceInfo.setInvoiceNum(invoice.getInvoiceNum());			
			if( InvoiceStatus.Void.equals(invoice.getStatus()) ){
				sfiInvoiceInfo.setInvoiceStatus(INVOICE_STATUS_VOIDED);
				sfiInvoiceInfo.setDispOrderStatus(DispOrderStatus.Deleted.getDisplayValue());
				sfiInvoiceInfo.setAllowReprint(false);
			}else{
				sfiInvoiceInfo.setInvoiceStatus(INVOICE_STATUS_ACTIVE);
			}
			
			if( PharmOrderPatType.Doh.equals( pharmOrder.getPatType() ) ){						
				sfiInvoiceInfo.setAllowReprint(false);
			}
			
			if( MedOrderDocType.Normal.equals(pharmOrder.getMedOrder().getDocType()) ){
				sfiInvoiceInfo.setOrderNum("MOE"+pharmOrder.getMedOrder().getOrderNum());
			}
			
			sfiInvoiceInfo.setPatHospCode( pharmOrder.getMedOrder().getPatHospCode() );
			
			if( pharmOrder.getMedCase() != null ){
				sfiInvoiceInfo.setCaseNum(pharmOrder.getMedCase().getCaseNum());
				sfiInvoiceInfo.setPasPayCode(getDisplayPasPayCode(pharmOrder.getMedCase().getPasPayCode(), pharmOrder.getMedCase().getPasPatGroupCode()));
				sfiInvoiceInfo.setPasPatGroupCode(pharmOrder.getMedCase().getPasPatGroupCode());
			}
			
			if( StringUtils.isEmpty(sfiInvoiceInfo.getPasPayCode()) ){
				sfiInvoiceInfo.setPasPayCode("Nil");
			}
			
			sfiInvoiceInfo.setPatName( pharmOrder.getPatient().getName() );
			if ( pharmOrder.getMedOrder().getOrderType() == OrderType.OutPatient || invoice.getDispOrder().getTicket() != null ) {
				sfiInvoiceInfo.setTicketNum( invoice.getDispOrder().getTicket().getTicketNum() );
			}
			sfiInvoiceInfo.setTotalAmount( invoice.getTotalAmount().doubleValue() );			
			
			BigDecimal totalExemptAmount = BigDecimal.ZERO;			
			for( InvoiceItem invoiceItem : invoice.getInvoiceItemList() ){				
				totalExemptAmount = totalExemptAmount.add(invoiceItem.getHandleExemptAmount());
			}
			sfiInvoiceInfo.setTotalHandleExemptAmount(totalExemptAmount.doubleValue());				
			if ( invoice.getReceiveAmount() != null ) {
				sfiInvoiceInfo.setReceiptAmount(invoice.getReceiveAmount().toString());
			}
			sfiInvoiceInfo.setReceiptNum(invoice.getReceiptNum());
			
			//for MpSfiInvioceEnq
			if ( isPurchaseRequestInvoice(pharmOrder.getMedOrder(), dispOrder.getTicket())  ) { // PurchaseRequest
				Long deliveryItemId = dispOrder.getDispOrderItemList().get(0).getDeliveryItemId();
				DeliveryItem deliveryItem = em.find(DeliveryItem.class, deliveryItemId);
				if ( deliveryItem != null ) {
					sfiInvoiceInfo.setDeliveryItemStatus(deliveryItem.getStatus().getDisplayValue());
				}
			}
			sfiInvoiceInfo.setInvoiceStatusForClearance(invoice.getStatus());
			
			sfiInvoiceSummaryMap.put(sfiInvoiceInfo.getInvoiceNum(), sfiInvoiceInfo);
		}
		
		return sfiInvoiceSummaryMap;
	}
	
	public Map<String, SfiInvoiceInfo> convertPurchaseRequestListToInvoiceSummary(List<PurchaseRequest> purchaseRequestList){
		Map<String, SfiInvoiceInfo> sfiInvoiceSummaryMap = new HashMap<String, SfiInvoiceInfo>();
		Map<String, String> forceProceedReasonMap = new HashMap<String, String>(); 

		for ( PurchaseRequest purchaseRequest : purchaseRequestList){
			SfiInvoiceInfo sfiInvoiceInfo = new SfiInvoiceInfo();
			Patient patient = purchaseRequest.getMedProfile().getPatient();
			MedCase medCase = purchaseRequest.getMedProfile().getMedCase();
			
			sfiInvoiceInfo.setPurchaseReqId(purchaseRequest.getId());
			sfiInvoiceInfo.setInvoiceNum(purchaseRequest.getInvoiceNum());
			sfiInvoiceInfo.setInvoiceDate(purchaseRequest.getInvoiceDate());
			sfiInvoiceInfo.setPatName(patient.getName());
			sfiInvoiceInfo.setHkid(patient.getHkid());
			sfiInvoiceInfo.setCaseNum(medCase.getCaseNum());
			sfiInvoiceInfo.setPasPayCode(getDisplayPasPayCode(purchaseRequest.getPasPayCode(), purchaseRequest.getPasPatGroupCode()));
			sfiInvoiceInfo.setPasPatGroupCode(purchaseRequest.getPasPatGroupCode());
			sfiInvoiceInfo.setTotalAmount(purchaseRequest.getTotalAmount().doubleValue());
			
			if( purchaseRequest.getForceProceedFlag() ){
				sfiInvoiceInfo.setForceProceedFlag(YesNoFlag.Yes.getDataValue());
				StringBuilder displayReason = new StringBuilder();
				
				if( StringUtils.isNotBlank(purchaseRequest.getManualInvoiceNum())){
					sfiInvoiceInfo.setForceProceedInvoiceNum(purchaseRequest.getManualInvoiceNum());
					displayReason.append(FORCE_PROCEED_INVOICE).append(purchaseRequest.getManualInvoiceNum());
					sfiInvoiceInfo.setDisplayForceProceedReason(displayReason.toString());
				}else if( StringUtils.isNotBlank(purchaseRequest.getManualReceiptNum()) ){
					sfiInvoiceInfo.setForceProceedReceiptNum(purchaseRequest.getManualReceiptNum());
					displayReason.append(FORCE_PROCEED_RECEIPT).append(purchaseRequest.getManualReceiptNum());
					sfiInvoiceInfo.setDisplayForceProceedReason(displayReason.toString());
				}else{
					if( forceProceedReasonMap.get(purchaseRequest.getForceProceedCode()) == null ){
						String forceProceedReaonDesc = retrieveForceProceedChargeReasonByReasonCode(purchaseRequest.getForceProceedCode());
						sfiInvoiceInfo.setForceProceedReason(forceProceedReaonDesc);
						forceProceedReasonMap.put(purchaseRequest.getForceProceedCode(), forceProceedReaonDesc);
					}else{
						sfiInvoiceInfo.setForceProceedReason(forceProceedReasonMap.get(purchaseRequest.getForceProceedCode()));
					}
					sfiInvoiceInfo.setDisplayForceProceedReason(sfiInvoiceInfo.getForceProceedReason());
				}
				sfiInvoiceInfo.setForceProceedBy(purchaseRequest.getForceProceedUser());				
				
			}else{
				sfiInvoiceInfo.setForceProceedFlag(YesNoFlag.No.getDataValue());
			}
			
			if( InvoiceStatus.Void.equals(purchaseRequest.getInvoiceStatus()) ){
				sfiInvoiceInfo.setInvoiceStatus(INVOICE_STATUS_VOIDED);
				sfiInvoiceInfo.setAllowReprint(false);
			}else{
				sfiInvoiceInfo.setInvoiceStatus(INVOICE_STATUS_ACTIVE);
			}

			sfiInvoiceInfo.setReceiptNum(purchaseRequest.getReceiptNum());
			sfiInvoiceInfo.setInvoiceStatusForClearance(purchaseRequest.getInvoiceStatus());
			sfiInvoiceInfo.setPatHospCode(purchaseRequest.getMedProfile().getPatHospCode());
			sfiInvoiceSummaryMap.put(sfiInvoiceInfo.getInvoiceNum(), sfiInvoiceInfo);
		}

		return sfiInvoiceSummaryMap;
	}
	
	public Map<String, SfiInvoiceInfo> updateSfiInvoiceInfoFcsStatusPaymentInfo(Map<String, SfiInvoiceInfo> sfiInvoiceSummaryMap, Map<String, PurchaseRequest> purchaseRequestMap){
		
		for ( Map.Entry<String, PurchaseRequest> entry : purchaseRequestMap.entrySet() ) {
			String invoiceNum = entry.getKey();
			PurchaseRequest purchaseRequest = entry.getValue();
			logger.debug("updateSfiInvoiceInfoFcsStatusPaymentInfo : invoiceNum:#0|purchaseRequestId:#1", invoiceNum, purchaseRequest.getId());
			if ( sfiInvoiceSummaryMap.containsKey(invoiceNum) ) {
				SfiInvoiceInfo sfiInvoiceInfo = sfiInvoiceSummaryMap.get(invoiceNum);
				sfiInvoiceInfo.setChargeAmount(purchaseRequest.getChargeAmount());
				if( InvoiceStatus.Void.equals(purchaseRequest.getInvoiceStatus()) ){
					sfiInvoiceInfo.setInvoiceStatus(INVOICE_STATUS_VOIDED);
					sfiInvoiceInfo.setAllowReprint(false);
				}else{
					sfiInvoiceInfo.setInvoiceStatus(INVOICE_STATUS_ACTIVE);
				}
				sfiInvoiceInfo.setReceiptNum(purchaseRequest.getReceiptNum());
				if ( purchaseRequest.getReceiveAmount() != null ) {
					sfiInvoiceInfo.setReceiptAmount(purchaseRequest.getReceiveAmount().toString());
				}
				sfiInvoiceInfo.setReceiptMessage(purchaseRequest.getReceiptMessage());
				sfiInvoiceInfo.setReceiptNum(purchaseRequest.getReceiptNum());
				sfiInvoiceInfo.setInvoiceStatusForClearance(purchaseRequest.getInvoiceStatus());
				logger.debug("updateSfiInvoiceInfoFcsStatusPaymentInfo : invoiceNum:#0|purchaseRequestId:#1 found and updated", invoiceNum, purchaseRequest.getId());
			}
		}
		
		return sfiInvoiceSummaryMap;
	}
	
	private String getDisplayPasPayCode(String pasPayCode, String pasPatGroupCode){
		StringBuilder displayPasPayCode = new StringBuilder();
		if( !StringUtils.isEmpty(pasPayCode) ){
			displayPasPayCode.append(pasPayCode);
		}
		
		if( StringUtils.equals(CHARGING_SFI_WAIVE_MEDCASE_PASPATGROUPCODE.get(), pasPatGroupCode) ){
			displayPasPayCode.append(" (HA)");
		}else if( StringUtils.equals(pasPatGroupCode, "GS") ){
			displayPasPayCode.append(" (GS)");
		}
		return displayPasPayCode.toString();
	}
	
	@SuppressWarnings("unchecked")
	private String retrieveForceProceedChargeReasonByReasonCode(String reasonCode){
		List<ChargeReason> chargeReasonList = (List<ChargeReason>) em.createQuery(
				"select o from ChargeReason o " + // 20120215 index check : ChargeReason.reasonCode,type : UI_CHARGE_REASON_01
				"where o.reasonCode = :reasonCode " +
				"and o.type = 'F'")
				.setParameter("reasonCode", reasonCode)
				.getResultList();
				
		if( chargeReasonList.size() > 0 ){
			return chargeReasonList.get(0).getDescription();
		}else{
			return reasonCode;
		}
	}
	
	private boolean isPurchaseRequestInvoice(MedOrder medOrder, Ticket ticket){
		return ( medOrder.getOrderType() == OrderType.InPatient && 
					!StringUtils.equals(MP_SFI_INVOICE_NUM, ticket.getTicketNum()) );
	}

}
