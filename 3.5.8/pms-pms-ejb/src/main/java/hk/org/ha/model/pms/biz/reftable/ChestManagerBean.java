package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.Chest;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("chestManager")
public class ChestManagerBean implements ChestManagerLocal {
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private Workstore workstore;
	
	@SuppressWarnings("unchecked")
	public List<Chest> retrieveChestList() 
	{
		return (List<Chest>) em.createQuery(
				"select o from Chest o" + // 20120303 index check : Chest.workstore : FK_CHEST_01
				" where o.workstore = :workstore" +
				" order by o.unitDoseName")
				.setParameter("workstore", workstore).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Chest> retrieveChestListByPrefixUnitDose(String prefixUnitDose) {
		return (List<Chest>) em.createQuery(
				"select o from Chest o" + // 20121112 index check : Chest.workstore : FK_CHEST_01
				" where o.workstore = :workstore" +
				" and o.unitDoseName like :prefixUnitDose" +
				" order by o.unitDoseName")
				.setParameter("workstore", workstore)
				.setParameter("prefixUnitDose", prefixUnitDose+"%")
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public Chest retrieveChest(String unitDoseName) {
		List<Chest> chestList =  em.createQuery(
									"select o from Chest o" + // 20120303 index check : Chest.workstore,unitDoseName : UI_CHEST_01
									" where o.workstore = :workstore" + 
									" and o.unitDoseName = :unitDoseName")
									.setParameter("workstore", workstore)
									.setParameter("unitDoseName", unitDoseName)
									.getResultList();
		if ( !chestList.isEmpty() ) {
			Chest chest = chestList.get(0);
			chest.getChestItemList().size();
			return chest;
		} else {
			return null;
		}
	}
}
