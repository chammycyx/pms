package hk.org.ha.model.pms.persistence.reftable.pivas;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
import hk.org.ha.model.pms.udt.DayOfWeek;
import hk.org.ha.model.pms.udt.reftable.pivas.PivasServiceHourType;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "PIVAS_SERVICE_HOUR")
@Customizer(AuditCustomizer.class)
public class PivasServiceHour extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pivasServiceHourSeq")
	@SequenceGenerator(name = "pivasServiceHourSeq", sequenceName = "SQ_PIVAS_SERVICE_HOUR", initialValue = 100000000)
	private Long id;

	@Converter(name = "PivasServiceHour.dayOfWeek", converterClass = DayOfWeek.Converter.class)
	@Convert("PivasServiceHour.dayOfWeek")
	@Column(name = "DAY_OF_WEEK", nullable = false, length = 1)
	private DayOfWeek dayOfWeek;
	
	@Column(name = "SUPPLY_TIME", length = 4)
	private Integer supplyTime;
	
	@Converter(name = "PivasServiceHour.type", converterClass = PivasServiceHourType.Converter.class)
    @Convert("PivasServiceHour.type")
	@Column(name="TYPE", nullable = false, length = 1)
	private PivasServiceHourType type;

	@ManyToOne
	@JoinColumn(name = "WORKSTORE_GROUP_CODE", nullable = false)
	private WorkstoreGroup workstoreGroup;
	
	@Transient
	private String supplyTimeStr;
	
	@Transient
	private Date supplyDate;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DayOfWeek getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(DayOfWeek dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public Integer getSupplyTime() {
		return supplyTime;
	}

	public void setSupplyTime(Integer supplyTime) {
		this.supplyTime = supplyTime;
	}

	public WorkstoreGroup getWorkstoreGroup() {
		return workstoreGroup;
	}

	public void setWorkstoreGroup(WorkstoreGroup workstoreGroup) {
		this.workstoreGroup = workstoreGroup;
	}

	public void setType(PivasServiceHourType type) {
		this.type = type;
	}

	public PivasServiceHourType getType() {
		return type;
	}

	public void setSupplyTimeStr(String supplyTimeStr) {
		this.supplyTimeStr = supplyTimeStr;
	}

	public String getSupplyTimeStr() {
		return supplyTimeStr;
	}
	
	public Date getSupplyDate() {
		return supplyDate;
	}

	public void setSupplyDate(Date supplyDate) {
		this.supplyDate = supplyDate;
	}

	// helper methods
    public boolean isNew() {
    	return this.getId() == null;
    }	

}
