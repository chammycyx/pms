package hk.org.ha.model.pms.udt.reftable;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum FdnType implements StringValuedEnum {

	DefaultName("DN", "Default Name"),
	GenericSubstitute("GS", "Generic Substitute"),
	FloatDrugName("FDN", "Floating Drug Name");	
	
    private final String dataValue;
    private final String displayValue;
        
    FdnType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    } 
    
    public static FdnType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(FdnType.class, dataValue);
	}
        
	public static class Converter extends StringValuedEnumConverter<FdnType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<FdnType> getEnumClass() {
    		return FdnType.class;
    	}
    }
}
