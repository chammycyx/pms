package hk.org.ha.model.pms.udt.medprofile;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DeliveryRequestStatus implements StringValuedEnum {

	Processing("P", "Processing"),
	Completed("C", "Completed");
	
    private final String dataValue;
    private final String displayValue;

    private DeliveryRequestStatus(String dataValue, String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
    }
	
	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}

	public static DeliveryRequestStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DeliveryRequestStatus.class, dataValue);
	}
    
	public static class Converter extends StringValuedEnumConverter<DeliveryRequestStatus> {
		
		private static final long serialVersionUID = 1L;
		
		public Class<DeliveryRequestStatus> getEnumClass() {
			return DeliveryRequestStatus.class;
		}
	}
}
