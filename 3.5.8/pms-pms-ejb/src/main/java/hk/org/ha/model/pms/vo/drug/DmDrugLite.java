package hk.org.ha.model.pms.vo.drug;

import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.corp.cache.DmRouteCacher;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmMoeProperty;
import hk.org.ha.model.pms.dms.persistence.DmRoute;
import hk.org.ha.model.pms.dms.vo.pms.DrugChargeInfo;
import hk.org.ha.model.pms.udt.vetting.FmStatus;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class DmDrugLite implements Serializable {

	private static final long serialVersionUID = 1L;

	private String itemCode;
	
	private String fullDrugDesc;
	
	private String suspend;
	
	private String hqSuspend;
	
	private String fmStatusDesc;
	
	private String drugScopeDesc;
	
	private String dispenseDosageUnit;
	
	private String drugName;
	
	private String baseUnit;
	
	private String itemStatus;

	private String commodityGroup;
	
	private String commodityType;
	
	private String formCode;
	
	private String displayname;
	
	private String routeDesc;
	
	private String routeCode;
	
	private String solventItem;
	
	private double averageUnitPrice;
	
	private String theraGroup;
	
	private String drugGroup;
	
	private String adminTimeCode;
	
	private String dosageCompul;
	
	private DrugChargeInfo drugChargeInfo;
	
	private Double dduToMduRatio;
	
	private String moDosageUnit;
	
	private String strength;
	
	private String fmStatus;
	
	private int drugKey;
	
	private String saltProperty;
	
	private boolean lifestyleDrugIndicator;
	
	private boolean oncologyDrugIndicator;
	
	private String diluentCode;
	
	private String dangerousDrug;
	
	private Double volumeValue;
	
	private String volumeUnit;
	
	private String extraInfo;
	
	private DmMoeProperty dmMoeProperty;
	
	public static DmDrugLite objValueOf(String itemCode) {
		return objValueOf(DmDrugCacher.instance().getDmDrug(itemCode));
	}
	
	public static DmDrugLite objValueOf(DmDrug dmDrug) {
		if (dmDrug != null) {
			
			DmRoute dmRoute = DmRouteCacher.instance().getRouteByRouteCode(dmDrug.getDmForm().getRouteCode());
			
			DmDrugLite dmDrugLite = new DmDrugLite();
			dmDrugLite.setAdminTimeCode(dmDrug.getDmDrugProperty().getAdminTimeCode());
			dmDrugLite.setAverageUnitPrice(dmDrug.getDmProcureSummary().getAverageUnitPrice());
			dmDrugLite.setBaseUnit(dmDrug.getBaseUnit());
			dmDrugLite.setCommodityGroup(dmDrug.getCommodityGroup());
			dmDrugLite.setCommodityType(dmDrug.getCommodityType());
			dmDrugLite.setDangerousDrug(dmDrug.getDangerousDrug());
			dmDrugLite.setDduToMduRatio(dmDrug.getDmMoeProperty().getDduToMduRatio());
			dmDrugLite.setDiluentCode(dmDrug.getDmMoeProperty().getDiluentCode());
			dmDrugLite.setDispenseDosageUnit(dmDrug.getDmMoeProperty().getDispenseDosageUnit());
			dmDrugLite.setDisplayname(dmDrug.getDmDrugProperty().getDisplayname());
			dmDrugLite.setDmMoeProperty(dmDrug.getDmMoeProperty());
			dmDrugLite.setDosageCompul(dmDrug.getDmDrugProperty().getDosageCompul());
			dmDrugLite.setDrugChargeInfo(dmDrug.getDrugChargeInfo());
			dmDrugLite.setDrugGroup(dmDrug.getDrugGroup());
			if (dmDrug.getDrugKey() != null) {
				dmDrugLite.setDrugKey(dmDrug.getDrugKey().intValue());
			}
			dmDrugLite.setDrugName(dmDrug.getDrugName());
			dmDrugLite.setDrugScopeDesc(null);//only used in workstore drug lookup
			dmDrugLite.setExtraInfo(dmDrug.getDmMoeProperty().getExtraInfo());
			dmDrugLite.setFmStatus(dmDrug.getPmsFmStatus().getFmStatus());
			dmDrugLite.setFmStatusDesc(FmStatus.dataValueOf(dmDrug.getPmsFmStatus().getFmStatus()).getDisplayValue());//Wo
			dmDrugLite.setFormCode(dmDrug.getFormCode());
			dmDrugLite.setFullDrugDesc(dmDrug.getFullDrugDesc());
			dmDrugLite.setHqSuspend(dmDrug.getHqSuspend());
			dmDrugLite.setItemCode(dmDrug.getItemCode());
			dmDrugLite.setItemStatus(null);//for workstoreDrug.itemStatus
			dmDrugLite.setLifestyleDrugIndicator(dmDrug.getDrugChargeInfo().getLifestyleDrugIndicator());
			dmDrugLite.setMoDosageUnit(dmDrug.getDmMoeProperty().getMoDosageUnit());
			dmDrugLite.setOncologyDrugIndicator(dmDrug.getDrugChargeInfo().getOncologyDrugIndicator());
			dmDrugLite.setRouteCode(dmRoute.getRouteCode());
			dmDrugLite.setRouteDesc(dmRoute.getRouteDesc());
			dmDrugLite.setSaltProperty(dmDrug.getDmDrugProperty().getSaltProperty());
			dmDrugLite.setSolventItem(dmDrug.getDmMoeProperty().getSolventItem());
			dmDrugLite.setStrength(dmDrug.getStrength());
			dmDrugLite.setSuspend(null);//for workstoreDrug.suspend
			dmDrugLite.setTheraGroup(dmDrug.getTheraGroup());
			dmDrugLite.setVolumeUnit(dmDrug.getVolumeUnit());
			dmDrugLite.setVolumeValue(dmDrug.getVolumeValue());
			
			return dmDrugLite;
		}
		return null;
	}
	
	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getFullDrugDesc() {
		return fullDrugDesc;
	}

	public void setFullDrugDesc(String fullDrugDesc) {
		this.fullDrugDesc = fullDrugDesc;
	}

	public String getSuspend() {
		return suspend;
	}

	public void setSuspend(String suspend) {
		this.suspend = suspend;
	}

	public String getHqSuspend() {
		return hqSuspend;
	}

	public void setHqSuspend(String hqSuspend) {
		this.hqSuspend = hqSuspend;
	}

	public String getFmStatusDesc() {
		return fmStatusDesc;
	}

	public void setFmStatusDesc(String fmStatusDesc) {
		this.fmStatusDesc = fmStatusDesc;
	}

	public String getDrugScopeDesc() {
		return drugScopeDesc;
	}

	public void setDrugScopeDesc(String drugScopeDesc) {
		this.drugScopeDesc = drugScopeDesc;
	}

	public String getDispenseDosageUnit() {
		return dispenseDosageUnit;
	}

	public void setDispenseDosageUnit(String dispenseDosageUnit) {
		this.dispenseDosageUnit = dispenseDosageUnit;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public String getDrugName() {
		return drugName;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setItemStatus(String itemStatus) {
		this.itemStatus = itemStatus;
	}

	public String getItemStatus() {
		return itemStatus;
	}
	
	public String getCommodityGroup() {
		return commodityGroup;
	}

	public void setCommodityGroup(String commodityGroup) {
		this.commodityGroup = commodityGroup;
	}

	public String getCommodityType() {
		return commodityType;
	}

	public void setCommodityType(String commodityType) {
		this.commodityType = commodityType;
	}

	public String getFormCode() {
		return formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	public String getDisplayname() {
		return displayname;
	}

	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}

	public String getRouteDesc() {
		return routeDesc;
	}

	public void setRouteDesc(String routeDesc) {
		this.routeDesc = routeDesc;
	}

	public String getRouteCode() {
		return routeCode;
	}

	public void setRouteCode(String routeCode) {
		this.routeCode = routeCode;
	}

	public String getSolventItem() {
		return solventItem;
	}

	public void setSolventItem(String solventItem) {
		this.solventItem = solventItem;
	}

	public double getAverageUnitPrice() {
		return averageUnitPrice;
	}

	public void setAverageUnitPrice(double averageUnitPrice) {
		this.averageUnitPrice = averageUnitPrice;
	}

	public String getTheraGroup() {
		return theraGroup;
	}

	public void setTheraGroup(String theraGroup) {
		this.theraGroup = theraGroup;
	}

	public String getDrugGroup() {
		return drugGroup;
	}

	public void setDrugGroup(String drugGroup) {
		this.drugGroup = drugGroup;
	}

	public String getAdminTimeCode() {
		return adminTimeCode;
	}

	public void setAdminTimeCode(String adminTimeCode) {
		this.adminTimeCode = adminTimeCode;
	}

	public DrugChargeInfo getDrugChargeInfo() {
		return drugChargeInfo;
	}

	public void setDrugChargeInfo(DrugChargeInfo drugChargeInfo) {
		this.drugChargeInfo = drugChargeInfo;
	}

	public String getDosageCompul() {
		return dosageCompul;
	}

	public void setDosageCompul(String dosageCompul) {
		this.dosageCompul = dosageCompul;
	}

	public Double getDduToMduRatio() {
		return dduToMduRatio;
	}

	public void setDduToMduRatio(Double dduToMduRatio) {
		this.dduToMduRatio = dduToMduRatio;
	}

	public String getMoDosageUnit() {
		return moDosageUnit;
	}

	public void setMoDosageUnit(String moDosageUnit) {
		this.moDosageUnit = moDosageUnit;
	}

	public String getStrength() {
		return strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public String getFmStatus() {
		return fmStatus;
	}

	public void setFmStatus(String fmStatus) {
		this.fmStatus = fmStatus;
	}

	public int getDrugKey() {
		return drugKey;
	}

	public void setDrugKey(int drugKey) {
		this.drugKey = drugKey;
	}

	public String getSaltProperty() {
		return saltProperty;
	}

	public void setSaltProperty(String saltProperty) {
		this.saltProperty = saltProperty;
	}

	public boolean getLifestyleDrugIndicator() {
		return lifestyleDrugIndicator;
	}

	public void setLifestyleDrugIndicator(boolean lifestyleDrugIndicator) {
		this.lifestyleDrugIndicator = lifestyleDrugIndicator;
	}

	public boolean getOncologyDrugIndicator() {
		return oncologyDrugIndicator;
	}

	public void setOncologyDrugIndicator(boolean oncologyDrugIndicator) {
		this.oncologyDrugIndicator = oncologyDrugIndicator;
	}

	public String getDiluentCode() {
		return diluentCode;
	}
	
	public void setDiluentCode(String diluentCode) {
		this.diluentCode = diluentCode;
	}

	public String getDangerousDrug() {
		return dangerousDrug;
	}

	public void setDangerousDrug(String dangerousDrug) {
		this.dangerousDrug = dangerousDrug;
	}

	public Double getVolumeValue() {
		return volumeValue;
	}

	public void setVolumeValue(Double volumeValue) {
		this.volumeValue = volumeValue;
	}

	public String getVolumeUnit() {
		return volumeUnit;
	}

	public void setVolumeUnit(String volumeUnit) {
		this.volumeUnit = volumeUnit;
	}

	public String getExtraInfo() {
		return extraInfo;
	}

	public void setExtraInfo(String extraInfo) {
		this.extraInfo = extraInfo;
	}

	public DmMoeProperty getDmMoeProperty() {
		return dmMoeProperty;
	}

	public void setDmMoeProperty(DmMoeProperty dmMoeProperty) {
		this.dmMoeProperty = dmMoeProperty;
	}
}
