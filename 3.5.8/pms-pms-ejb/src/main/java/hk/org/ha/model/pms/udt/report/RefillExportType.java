package hk.org.ha.model.pms.udt.report;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum RefillExportType implements StringValuedEnum {

	RefillItemExport("RefillItemXls", "RefillItemXls"),
	WorkloadForecastExport("RefillWorkloadForecastXls", "RefillWorkloadForecastXls"),
	RefillQtyForecastExport("RefillQtyForecastXls", "RefillQtyForecastXls"),
	RefillStatExport("RefillStatXls", "RefillStatXls"),
	ExpireRefillExport("RefillExpiryXls", "RefillExpiryXls"),
	PharmClinicRefillCouponExport("PharmClinicRefillCouponXls", "PharmClinicRefillCouponXls");
	
    private final String dataValue;
    private final String displayValue;

    RefillExportType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static RefillExportType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(RefillExportType.class, dataValue);
	}
        
	public static class Converter extends StringValuedEnumConverter<RefillExportType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<RefillExportType> getEnumClass() {
    		return RefillExportType.class;
    	}
    }
}
