package hk.org.ha.model.pms.biz.reftable;

import javax.ejb.Local;

import org.jboss.seam.annotations.async.Asynchronous;
import org.jboss.seam.annotations.async.Duration;
import org.jboss.seam.annotations.async.IntervalDuration;

@Local
public interface ItemBinUploaderLocal {

	@Asynchronous
	void uploadItemBinByTimer(@Duration Long duration, @IntervalDuration Long intervalDuration);
	
}
