package hk.org.ha.model.pms.vo.vetting.mp;

import hk.org.ha.model.pms.udt.medprofile.DeliveryItemStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class MpPharmInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long medProfileItemId;
	private Long medProfileMoItemId;
	private String medProfilePoItemUuid;
	private Long medProfilePoItemId;
	private String itemCode;
	//For Full Drug Desc
	private String drugName;
	private String formLabelDesc;
	private String strength;
	private String volumeText;
	private String drugSynonym;
	
	private String baseUnit;
	private Boolean lifestyleFlag;
	private Boolean oncologyFlag;
	private Boolean safetyNetFlag;
	private MedProfileItemStatus medProfileItemStatus;
	private DeliveryItemStatus deliveryItemStatus;
	
	public void setMedProfileItemId(Long medProfileItemId) {
		this.medProfileItemId = medProfileItemId;
	}

	public Long getMedProfileItemId() {
		return medProfileItemId;
	}

	public void setMedProfileMoItemId(Long medProfileMoItemId) {
		this.medProfileMoItemId = medProfileMoItemId;
	}

	public Long getMedProfileMoItemId() {
		return medProfileMoItemId;
	}
	
	public Long getMedProfilePoItemId() {
		return medProfilePoItemId;
	}
	
	public void setMedProfilePoItemId(Long medProfilePoItemId) {
		this.medProfilePoItemId = medProfilePoItemId;
	}
	
	public String getItemCode() {
		return itemCode;
	}
	
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	
	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public String getDrugName() {
		return drugName;
	}

	public void setFormLabelDesc(String formLabelDesc) {
		this.formLabelDesc = formLabelDesc;
	}

	public String getFormLabelDesc() {
		return formLabelDesc;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public String getStrength() {
		return strength;
	}

	public void setVolumeText(String volumeText) {
		this.volumeText = volumeText;
	}

	public String getVolumeText() {
		return volumeText;
	}
	
	public void setDrugSynonym(String drugSynonym) {
		this.drugSynonym = drugSynonym;
	}

	public String getDrugSynonym() {
		return drugSynonym;
	}

	public String getBaseUnit() {
		return baseUnit;
	}
	
	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public Boolean getLifestyleFlag() {
		return lifestyleFlag;
	}

	public void setLifestyleFlag(Boolean lifestyleFlag) {
		this.lifestyleFlag = lifestyleFlag;
	}

	public Boolean getOncologyFlag() {
		return oncologyFlag;
	}

	public void setOncologyFlag(Boolean oncologyFlag) {
		this.oncologyFlag = oncologyFlag;
	}

	public Boolean getSafetyNetFlag() {
		return safetyNetFlag;
	}

	public void setSafetyNetFlag(Boolean safetyNetFlag) {
		this.safetyNetFlag = safetyNetFlag;
	}

	public void setMedProfilePoItemUuid(String medProfilePoItemUuid) {
		this.medProfilePoItemUuid = medProfilePoItemUuid;
	}

	public String getMedProfilePoItemUuid() {
		return medProfilePoItemUuid;
	}

	public void setMedProfileItemStatus(MedProfileItemStatus medProfileItemStatus) {
		this.medProfileItemStatus = medProfileItemStatus;
	}

	public MedProfileItemStatus getMedProfileItemStatus() {
		return medProfileItemStatus;
	}

	public void setDeliveryItemStatus(DeliveryItemStatus deliveryItemStatus) {
		this.deliveryItemStatus = deliveryItemStatus;
	}

	public DeliveryItemStatus getDeliveryItemStatus() {
		return deliveryItemStatus;
	}
}
