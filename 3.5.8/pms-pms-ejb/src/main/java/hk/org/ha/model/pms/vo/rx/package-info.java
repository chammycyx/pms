@XmlJavaTypeAdapters({   
    @XmlJavaTypeAdapter(value=DateAdapter.class, type=Date.class),
    @XmlJavaTypeAdapter(value=ActionStatusAdapter.class, type=ActionStatus.class),
    @XmlJavaTypeAdapter(value=NameTypeAdapter.class, type=NameType.class),
    @XmlJavaTypeAdapter(value=DupChkDrugTypeAdapter.class, type=DupChkDrugType.class),
    @XmlJavaTypeAdapter(value=RegimenDurationUnitAdapter.class, type=RegimenDurationUnit.class),
    @XmlJavaTypeAdapter(value=RegimenTypeAdapter.class, type=RegimenType.class),
    @XmlJavaTypeAdapter(value=PrnPercentageAdapter.class, type=PrnPercentage.class),
    @XmlJavaTypeAdapter(value=YesNoBlankFlagAdapter.class, type=YesNoBlankFlag.class),
    @XmlJavaTypeAdapter(value=RemarkStatusAdapter.class, type=RemarkStatus.class),
    @XmlJavaTypeAdapter(value=RemarkItemStatusAdapter.class, type=RemarkItemStatus.class),
    @XmlJavaTypeAdapter(value=MedOrderDocTypeAdapter.class, type=MedOrderDocType.class),
    @XmlJavaTypeAdapter(value=FixedConcnSourceAdapter.class, type=FixedConcnSource.class)
})  
package hk.org.ha.model.pms.vo.rx;

import hk.org.ha.fmk.pms.eai.mail.DateAdapter;
import hk.org.ha.model.pms.persistence.disp.MedOrderDocTypeAdapter;
import hk.org.ha.model.pms.udt.YesNoBlankFlag;
import hk.org.ha.model.pms.udt.disp.DupChkDrugType;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.RemarkItemStatus;
import hk.org.ha.model.pms.udt.disp.RemarkItemStatusAdapter;
import hk.org.ha.model.pms.udt.disp.RemarkStatus;
import hk.org.ha.model.pms.udt.disp.RemarkStatusAdapter;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.FixedConcnSource;
import hk.org.ha.model.pms.udt.vetting.NameType;
import hk.org.ha.model.pms.udt.vetting.PrnPercentage;
import hk.org.ha.model.pms.udt.vetting.RegimenDurationUnit;
import hk.org.ha.model.pms.udt.vetting.RegimenType;
import hk.org.ha.model.pms.vo.YesNoBlankFlagAdapter;

import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;
   