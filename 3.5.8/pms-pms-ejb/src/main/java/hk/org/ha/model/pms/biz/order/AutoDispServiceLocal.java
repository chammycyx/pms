package hk.org.ha.model.pms.biz.order;

import javax.ejb.Local;

@Local
public interface AutoDispServiceLocal {

	void genTicket();
	
	void retrieveOrder(String orderNum);
	
	void retrievePatientAndMedCase(String patientHkid, String patientName);
		
	void checkTicket();
	
	void startVetting() throws Exception;

	void removeItemsIfNotConverted() throws Exception;
	
	void endVetting() throws Exception;
	
	void drugPick();
	
	void assembling();

	void checkOrder() throws Exception;
	
	void issueOrder() throws Exception;
	
	void cddhSearch(String patientHkid, String patientName);
	
	void drugSearch(String drugName);
	
	void destroy();	
}
