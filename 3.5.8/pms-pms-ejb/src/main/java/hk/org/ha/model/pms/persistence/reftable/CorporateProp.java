package hk.org.ha.model.pms.persistence.reftable;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.PropEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "CORPORATE_PROP")
@Customizer(AuditCustomizer.class)
public class CorporateProp extends PropEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "corporatePropSeq")
	@SequenceGenerator(name = "corporatePropSeq", sequenceName = "SQ_CORPORATE_PROP", initialValue = 100000000)
	private Long id;
		
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
