package hk.org.ha.model.pms.udt.alert;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum AlertProfileStatus implements StringValuedEnum {
	
	NoAllergy("G", "No known drug allergy"),
	NoRecord("R", "No record"),
	RecordExist("X", "Drug allergy exists"),
	Error("E", "Error");

    private final String dataValue;
    private final String displayValue;
    
	AlertProfileStatus(final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}

	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}

	public static AlertProfileStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(AlertProfileStatus.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<AlertProfileStatus> {
		
		private static final long serialVersionUID = 1L;

		public Class<AlertProfileStatus> getEnumClass() {
    		return AlertProfileStatus.class;
    	}
    }
}
