package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class RefillWorkloadForecastRpt implements Serializable {

	private static final long serialVersionUID = 1L;

	private Date refillDate;
	
	private Long refillCount;
	
	private Long refillItemCount;
	
	public RefillWorkloadForecastRpt(Date refillDate, Long refillCount,
			Long refillItemCount) {
		super();
		this.refillDate = refillDate;
		this.refillCount = refillCount;
		this.refillItemCount = refillItemCount;
	}

	public Date getRefillDate() {
		return refillDate;
	}

	public void setRefillDate(Date refillDate) {
		this.refillDate = refillDate;
	}

	public void setRefillItemCount(Long refillItemCount) {
		this.refillItemCount = refillItemCount;
	}

	public Long getRefillItemCount() {
		return refillItemCount;
	}

	public void setRefillCount(Long refillCount) {
		this.refillCount = refillCount;
	}

	public Long getRefillCount() {
		return refillCount;
	}


}
