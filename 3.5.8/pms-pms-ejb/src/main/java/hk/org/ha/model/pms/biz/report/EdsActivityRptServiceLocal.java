package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.vo.report.EdsActivityRpt;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface EdsActivityRptServiceLocal {

	void retrieveEdsActivityRpt(Integer selectedTab, Date date);
	
	List<EdsActivityRpt> retrieveEdsActivityRptList(Date date);
	
	void generateEdsActivityRpt();
	
	void printEdsActivityRpt();
	
	void destroy();
	
}
