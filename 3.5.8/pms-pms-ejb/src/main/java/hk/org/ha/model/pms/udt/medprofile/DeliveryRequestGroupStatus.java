package hk.org.ha.model.pms.udt.medprofile;

import java.util.Arrays;
import java.util.List;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DeliveryRequestGroupStatus implements StringValuedEnum {

	Active("A", "Active"),
	Processing("P", "Processing"),
	Inactive("D", "Inactive");
	
    private final String dataValue;
    private final String displayValue;

    public static final List<DeliveryRequestGroupStatus> Active_Processing = Arrays.asList(Active, Processing);
    
    private DeliveryRequestGroupStatus(String dataValue, String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
    }
	
	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}

	public static DeliveryRequestGroupStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DeliveryRequestGroupStatus.class, dataValue);
	}
    
	public static class Converter extends StringValuedEnumConverter<DeliveryRequestGroupStatus> {
		
		private static final long serialVersionUID = 1L;
		
		public Class<DeliveryRequestGroupStatus> getEnumClass() {
			return DeliveryRequestGroupStatus.class;
		}
	}
}
