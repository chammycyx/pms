package hk.org.ha.model.pms.biz.vetting;

import static hk.org.ha.model.pms.prop.Prop.VETTING_SENDPHARMREMARK_ACTION_FULL_AUTHENTICATE;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.vo.conversion.ConvertParam;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedOrderItemAlert;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.reftable.PharmRemarkTemplate;
import hk.org.ha.model.pms.uam.udt.AuthorizeStatus;
import hk.org.ha.model.pms.uam.vo.AuthenticateCriteria;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.RemarkItemStatus;
import hk.org.ha.model.pms.udt.disp.RemarkStatus;
import hk.org.ha.model.pms.udt.rx.RxItemType;
import hk.org.ha.model.pms.vo.rx.CapdItem;
import hk.org.ha.model.pms.vo.rx.CapdItemRemark;
import hk.org.ha.model.pms.vo.rx.CapdRxDrug;
import hk.org.ha.model.pms.vo.rx.CapdRxDrugRemark;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.DoseGroupRemark;
import hk.org.ha.model.pms.vo.rx.DoseRemark;
import hk.org.ha.model.pms.vo.rx.Freq;
import hk.org.ha.model.pms.vo.rx.OralRxDrug;
import hk.org.ha.model.pms.vo.rx.Regimen;
import hk.org.ha.model.pms.vo.rx.RegimenRemark;
import hk.org.ha.model.pms.vo.rx.RxItem;
import hk.org.ha.model.pms.vo.security.PropMap;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;
import hk.org.ha.service.biz.pms.uam.interfaces.UamServiceJmsRemote;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;
import org.springframework.beans.BeanUtils;

@AutoCreate 
@Stateful
@Scope(ScopeType.SESSION)
@Name("pharmRemarkService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PharmRemarkServiceBean implements PharmRemarkServiceLocal {

	private final static String PERMISSION_SEND_PHARM_REMARK = "permission.sendPharmRemark";
	
	@PersistenceContext
	private EntityManager em;

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@Logger
	private Log logger;
	
	@In
	private UamInfo uamInfo;
	
	@In
	private UamServiceJmsRemote uamServiceProxy;

	@In
	private Workstore workstore;

	@In
	private PropMap propMap;
	
	@In(required = false)
	private PharmOrder pharmOrder;
	
	@Out(required = false)
	private Boolean pharmRemarkRight;
	
	@Out(required = false)
	private RemarkItemStatus pharmRemarkType;

	@Out(required = false)
	private MedOrderItem prevMedOrderItemRemark;	// used in item update remark only
	
	@Out(required = false)
	private MedOrderItem medOrderItemRemark;	// used in item add and update remark

	@Out(required = false)
	private List<PharmRemarkTemplate> pharmRemarkTemplateOptionList; 

	@Out(required = false)
	private Boolean pharmRemarkExist;

	@Out(required = false)
	private Boolean pharmRemarkModifyFallback;
	
	@Out(required = false)
	private String pharmRemarkLogonFailMsg;
	
	@Out(required = false)
	private Date pharmRemarkDate;

	@Out(required = false)
	private Integer pharmRemarkItemNum;

	private static final String JAXB_CONTEXT_RX = "hk.org.ha.model.pms.vo.rx";
	
	public void addPharmRemarkOrder() {
		pharmRemarkRight = propMap.getValueAsBoolean(PERMISSION_SEND_PHARM_REMARK) && ! VETTING_SENDPHARMREMARK_ACTION_FULL_AUTHENTICATE.get();
		pharmRemarkTemplateOptionList = retrievePharmRemarkTemplateList();
	}
	
	public void checkPharmRemarkItem(String sourceAction, Integer medOrderSelectedIndex, MedOrderItem medOrderItem) {
		pharmRemarkRight = propMap.getValueAsBoolean(PERMISSION_SEND_PHARM_REMARK) && ! VETTING_SENDPHARMREMARK_ACTION_FULL_AUTHENTICATE.get();

		// initalize variables
		prevMedOrderItemRemark = null;
		medOrderItemRemark = null;
		pharmRemarkModifyFallback = false;
		pharmRemarkItemNum = null;
		
		if ("remove".equals(sourceAction)) {
			pharmRemarkType = RemarkItemStatus.UnconfirmedDelete;
		} else if ("convert".equals(sourceAction)) {
			if (medOrderItem.getRemarkStatus() == RemarkStatus.Unconfirm) {
				// if item is unconfirmed, since the item is saved, cannot use id = null to check for item add or item update
				if (medOrderItem.getPrevMoItemId() == null) {
					// modify an pharmacy remark add item 
					pharmRemarkType = RemarkItemStatus.UnconfirmedAdd;
				} else {
					pharmRemarkType = RemarkItemStatus.UnconfirmedModify;
				}
			} else {
				if (medOrderItem.getId() == null) {
					pharmRemarkType = RemarkItemStatus.UnconfirmedAdd;
				} else {
					pharmRemarkType = RemarkItemStatus.UnconfirmedModify;
				}
			}
		}
		
		if (pharmRemarkType == RemarkItemStatus.UnconfirmedAdd || pharmRemarkType == RemarkItemStatus.UnconfirmedModify) {
			medOrderItemRemark = new MedOrderItem(new OralRxDrug());
			copyMedOrderItemRemark(medOrderItem, medOrderItemRemark);

			medOrderItemRemark.clearPharmOrderItemList();
			medOrderItemRemark.setMedOrder(null);
			medOrderItemRemark.setMedOrderItemAlertList(new ArrayList<MedOrderItemAlert>());
		}
		
		if (pharmRemarkType == RemarkItemStatus.UnconfirmedAdd) { 
			pharmRemarkExist = Boolean.TRUE;
		} else if (pharmRemarkType == RemarkItemStatus.UnconfirmedDelete) {
			pharmRemarkExist = Boolean.TRUE;
			// get previous item if the deleted item has item modify remark
			prevMedOrderItemRemark = getPrevMedOrderItemForDelete(medOrderItem);
		} else if (pharmRemarkType == RemarkItemStatus.UnconfirmedModify) { 
			prevMedOrderItemRemark = getPrevMedOrderItem(medOrderItem, true);
			pharmRemarkExist = compareMedOrderItem(prevMedOrderItemRemark, medOrderItem, medOrderItemRemark, false);
			
			if ( ! pharmRemarkExist && medOrderItem.getRemarkStatus() == RemarkStatus.Unconfirm) {
				pharmRemarkModifyFallback = true;
				pharmRemarkItemNum = pharmOrder.getMedOrder().getAndIncreaseMaxItemNum();
				
			} else if ( ! pharmRemarkExist && medOrderItem.getVersion() == null) {
				// replace with same item and input same regimen
				// (though new item number is assigned, there is no remark, so treat as reverse remark case to set MedOrderItem.changeFlag to true)
				// (as a result, remark status of old item is changed to D and remark status of replaced item is A, which follows legacy behavouir) 
				pharmRemarkModifyFallback = true;
				pharmRemarkItemNum = pharmOrder.getMedOrder().getAndIncreaseMaxItemNum();
			}
		}
		
		pharmRemarkTemplateOptionList = retrievePharmRemarkTemplateList();
	}
	
	public void retrievePharmRemarkItem(String sourceAction, MedOrderItem medOrderItem) {
		pharmRemarkRight = propMap.getValueAsBoolean(PERMISSION_SEND_PHARM_REMARK) && ! VETTING_SENDPHARMREMARK_ACTION_FULL_AUTHENTICATE.get();
		
		// initalize variables
		prevMedOrderItemRemark = null;
		medOrderItemRemark = null;
		
		if (medOrderItem.getPrevMoItemId() == null) {
			if (medOrderItem.getStatus() == MedOrderItemStatus.Deleted) {
				pharmRemarkType = RemarkItemStatus.UnconfirmedDelete;
			} else {
				pharmRemarkType = RemarkItemStatus.UnconfirmedAdd;
			}
		} else {
			pharmRemarkType = RemarkItemStatus.UnconfirmedModify;
		}
		
		if (pharmRemarkType == RemarkItemStatus.UnconfirmedAdd || pharmRemarkType == RemarkItemStatus.UnconfirmedModify) {
			medOrderItemRemark = new MedOrderItem(new OralRxDrug());
			copyMedOrderItemRemark(medOrderItem, medOrderItemRemark);

			medOrderItemRemark.clearPharmOrderItemList();
			medOrderItemRemark.setMedOrder(null);
			medOrderItemRemark.setMedOrderItemAlertList(new ArrayList<MedOrderItemAlert>());
		}
		
		if (pharmRemarkType == RemarkItemStatus.UnconfirmedModify) {
			prevMedOrderItemRemark = getPrevMedOrderItem(medOrderItem, false);
			compareMedOrderItem(prevMedOrderItemRemark, medOrderItem, medOrderItemRemark, false);
		}
		
		pharmRemarkTemplateOptionList = retrievePharmRemarkTemplateList();
	}

	public void validatePharmRemarkLogon(String user, String password) {
		AuthenticateCriteria authenticateCriteria = new AuthenticateCriteria();
		authenticateCriteria.setProfileCode(uamInfo.getProfileCode());
		authenticateCriteria.setUserName(user);
		authenticateCriteria.setPassword(password);
		authenticateCriteria.setTarget("sendPharmRemark");
		authenticateCriteria.setAction("Y");
		AuthorizeStatus authStatus = uamServiceProxy.authenticate(authenticateCriteria);
		
		pharmRemarkLogonFailMsg = null;
		if (AuthorizeStatus.Succeed.equals(authStatus) ){
			pharmRemarkLogonFailMsg = null;
		} else {
			if ( AuthorizeStatus.Invalid.equals(authStatus) ){			
				pharmRemarkLogonFailMsg = "0091";
			} else {
				pharmRemarkLogonFailMsg = "0194";
			}
		}
	}
	
	public void retrievePharmRemarkDateItemNum(Boolean itemNumRequired) {
		pharmRemarkDate = new Date();
		pharmRemarkItemNum = null;
		if (itemNumRequired) {
			pharmRemarkItemNum = pharmOrder.getMedOrder().getAndIncreaseMaxItemNum();
		}
	}

	private MedOrderItem getPrevMedOrderItemForDelete(MedOrderItem medOrderItem) {
		MedOrderItem prevMedOrderItem = null;
		if (medOrderItem.getRemarkStatus() == RemarkStatus.Unconfirm && medOrderItem.getPrevMoItemId() != null) {
			// fallback to previous item if deleting a item with modify remark
			prevMedOrderItem = em.find(MedOrderItem.class, medOrderItem.getPrevMoItemId());
			
			prevMedOrderItem.getMedOrder().markFmIndFlag();
			
			if (prevMedOrderItem.getMedOrder().isMpDischarge()) {
				// update drug order html for discharge order display
				JaxbWrapper<RxItem> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
				String rxItemXml = rxJaxbWrapper.marshall(prevMedOrderItem.getRxItem());
				
				ConvertParam convertParam = new ConvertParam();
				convertParam.setOrderType("D");
				//Format for discharge only
				List<String> rxItemXmlList = dmsPmsServiceProxy.formatDrugOrderXml( new ArrayList<String>(Arrays.asList(rxItemXml)), convertParam );
				RxItem rxItem = rxJaxbWrapper.unmarshall(rxItemXmlList.get(0));
				if (rxItem == null) {
					throw new UnsupportedOperationException("Cannot unmarshall updated RxItem after calling DMS formatting engine, orderNum = " + prevMedOrderItem.getMedOrder().getOrderNum() + ", itemNum = " + prevMedOrderItem.getItemNum());
				}
				rxItem.buildTlf();
				prevMedOrderItem.setRxItem(rxItem);
			}

			em.flush();
			em.clear();

			prevMedOrderItem.clearMedOrderItemAlertList();
			for (MedOrderItemAlert moia : medOrderItem.getMedOrderItemAlertList()) {
				moia.replaceItemNum(medOrderItem.getItemNum(), prevMedOrderItem.getItemNum(), pharmOrder.getMedOrder().getOrderNum());
				prevMedOrderItem.addMedOrderItemAlert(moia);
			}
			
			prevMedOrderItem.loadDmInfo();
			prevMedOrderItem.getPharmOrderItemList();
			prevMedOrderItem.setMedOrder(null);
		}
		
		return prevMedOrderItem;
	}
	
	private MedOrderItem getPrevMedOrderItem(MedOrderItem medOrderItem, boolean isAddPharmRemark) {
		MedOrderItem prevMedOrderItem;
		if (medOrderItem.getRemarkStatus() == RemarkStatus.None) {
			prevMedOrderItem = em.find(MedOrderItem.class, medOrderItem.getId());
			if (prevMedOrderItem.getRemarkStatus() == RemarkStatus.Unconfirm && prevMedOrderItem.getPrevMoItemId() != null) {
				// if current item is fallback remark but not yet save Rx, get the correct previous item
				prevMedOrderItem = em.find(MedOrderItem.class, prevMedOrderItem.getPrevMoItemId());
			}
			
		} else if (medOrderItem.getRemarkStatus() == RemarkStatus.Unconfirm) {
			prevMedOrderItem = em.find(MedOrderItem.class, medOrderItem.getPrevMoItemId());
			
		} else { // if (medOrderItem.getRemarkStatus() == RemarkStatus.Confirm)
			if (isAddPharmRemark) {
				prevMedOrderItem = em.find(MedOrderItem.class, medOrderItem.getId());
			} else {
				prevMedOrderItem = em.find(MedOrderItem.class, medOrderItem.getPrevMoItemId());
			}
		}

		prevMedOrderItem.getMedOrder().markFmIndFlag();
		
		if (prevMedOrderItem.getMedOrder().isMpDischarge()) {
			// update drug order html for discharge order display
			JaxbWrapper<RxItem> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
			String rxItemXml = rxJaxbWrapper.marshall(prevMedOrderItem.getRxItem());
			
			ConvertParam convertParam = new ConvertParam();
			convertParam.setOrderType("D");
			//Format for discharge only
			List<String> rxItemXmlList = dmsPmsServiceProxy.formatDrugOrderXml( new ArrayList<String>(Arrays.asList(rxItemXml)), convertParam );
			RxItem rxItem = rxJaxbWrapper.unmarshall(rxItemXmlList.get(0));
			if (rxItem == null) {
				throw new UnsupportedOperationException("Cannot unmarshall updated RxItem after calling DMS formatting engine, orderNum = " + prevMedOrderItem.getMedOrder().getOrderNum() + ", itemNum = " + prevMedOrderItem.getItemNum());
			}
			rxItem.buildTlf();
			prevMedOrderItem.setRxItem(rxItem);
		}

		prevMedOrderItem.loadDmInfo();
		prevMedOrderItem.getMedOrderItemAlertList().size();
		
		return prevMedOrderItem;
	}
	
	@SuppressWarnings("unchecked")
	private List<PharmRemarkTemplate> retrievePharmRemarkTemplateList() {
		return (List<PharmRemarkTemplate>) em.createQuery(
				"select o from PharmRemarkTemplate o" + // 20120312 index check : PharmRemarkTemplate.workstore : FK_PHARM_REMARK_TEMPLATE_01
				" where o.workstore = :workstore" + 
				" order by o.sortSeq")
				.setParameter("workstore", workstore)
				.getResultList();
	}
	
	private void copyMedOrderItemRemark(MedOrderItem srcMedOrderItem, MedOrderItem outMedOrderItemRemark) {
		BeanUtils.copyProperties(srcMedOrderItem, outMedOrderItemRemark, new String[]{ "regimen", "capdRxDrug", "regimenRemark", "capdRxDrugRemark", "medOrderItemAlertList"});
		
		if (srcMedOrderItem.getCapdRxDrug() != null) {
			CapdRxDrug srcCapdRxDrug = srcMedOrderItem.getCapdRxDrug();
			CapdRxDrugRemark outCapdRxDrugRemark = new CapdRxDrugRemark();
			
			BeanUtils.copyProperties(srcCapdRxDrug, outCapdRxDrugRemark, new String[]{ "capdItemList", "capdItemRemarkList"});
			outMedOrderItemRemark.setCapdRxDrugRemark(outCapdRxDrugRemark);
			
			List<CapdItemRemark> outCapdItemRemarkList = new ArrayList<CapdItemRemark>();
			for (int i=0; i<srcCapdRxDrug.getCapdItemList().size(); i++) {
				CapdItem srcCapdItem = srcCapdRxDrug.getCapdItemList().get(i);
				CapdItemRemark outCapdItemRemark = new CapdItemRemark();
				
				BeanUtils.copyProperties(srcCapdItem, outCapdItemRemark);
				outCapdItemRemarkList.add(outCapdItemRemark);
			}			
			
			outCapdRxDrugRemark.setCapdItemRemarkList(outCapdItemRemarkList);
			
		} else if (srcMedOrderItem.getRegimen() != null) {
			Regimen srcRegimen = srcMedOrderItem.getRegimen();
			RegimenRemark outRegimenRemark = new RegimenRemark();
			BeanUtils.copyProperties(srcRegimen, outRegimenRemark, new String[]{ "doseGroupList", "doseGroupRemarkList"});
			outMedOrderItemRemark.setRegimenRemark(outRegimenRemark);
		
			List<DoseGroupRemark> outDoseGroupRemarkList = new ArrayList<DoseGroupRemark>();
			for (int i=0; i<srcRegimen.getDoseGroupList().size(); i++) {
				DoseGroup srcDoseGroup = srcRegimen.getDoseGroupList().get(i);
				DoseGroupRemark outDoseGroupRemark = new DoseGroupRemark();
				
				BeanUtils.copyProperties(srcDoseGroup, outDoseGroupRemark, new String[]{ "doseList", "doseRemarkList"});
				outDoseGroupRemarkList.add(outDoseGroupRemark);
				
	
				List<DoseRemark> outDoseRemarkList = new ArrayList<DoseRemark>();
				for (int j=0; j<srcDoseGroup.getDoseList().size(); j++) {
					Dose srcDose = srcDoseGroup.getDoseList().get(j);
					DoseRemark outDoseRemark = new DoseRemark();
					
					BeanUtils.copyProperties(srcDose, outDoseRemark);
					outDoseRemarkList.add(outDoseRemark);
				}	
				outDoseGroupRemark.setDoseRemarkList(outDoseRemarkList);
			}
			
			outRegimenRemark.setDoseGroupRemarkList(outDoseGroupRemarkList);
		}
	}

	private boolean compareMedOrderItem(MedOrderItem oldMedOrderItem, MedOrderItem newMedOrderItem, MedOrderItem outMedOrderItemRemark, boolean allRemark) {
		boolean isPharmRemarkExist = false;
		
		if ( allRemark || ! valuesEqual(oldMedOrderItem.getSingleUseFlag(), newMedOrderItem.getSingleUseFlag()) ) {
			isPharmRemarkExist = true;
			outMedOrderItemRemark.setSingleUseFlagRemark(Boolean.TRUE);
		}
		if ( allRemark || ! valuesEqual(oldMedOrderItem.getFixPeriodFlag(), newMedOrderItem.getFixPeriodFlag()) ) {
			isPharmRemarkExist = true;
			outMedOrderItemRemark.setFixPeriodFlagRemark(Boolean.TRUE);
		}
		if ( allRemark || ! stringEqualCaseSensitive(oldMedOrderItem.getSpecialInstruction(), newMedOrderItem.getSpecialInstruction()) ) {
			isPharmRemarkExist = true;
			outMedOrderItemRemark.setSpecialInstructionRemark(Boolean.TRUE);
		}
		if ( allRemark || ! stringEqualCaseSensitive(oldMedOrderItem.getCommentText(), newMedOrderItem.getCommentText()) ) {
			isPharmRemarkExist = true;
			outMedOrderItemRemark.setCommentRemark(Boolean.TRUE);
		}
		if ( allRemark || ! valuesEqual(oldMedOrderItem.getActionStatus(), newMedOrderItem.getActionStatus()) ) {
			isPharmRemarkExist = true;
			outMedOrderItemRemark.setActionStatusRemark((Boolean.TRUE));
		}

		if ( ! allRemark && checkSwitchCapdNonCapdDrugRemark(oldMedOrderItem, newMedOrderItem)) {
			allRemark = true;	// set remark to  drug name and all regimen 
			isPharmRemarkExist = true;
		}

		if ( allRemark || checkNonCapdDrugInfoRemark(oldMedOrderItem, newMedOrderItem)) {
			isPharmRemarkExist = true;
			outMedOrderItemRemark.setDrugNameRemark(Boolean.TRUE);
		}
		
		if (newMedOrderItem.getCapdRxDrug() != null) {
			// if old item is non-CAPD regimen, oldMedOrderItem.getCapdRxDrug() can be null
			if ( compareCapdRxDrug(oldMedOrderItem.getCapdRxDrug(), newMedOrderItem.getCapdRxDrug(), outMedOrderItemRemark.getCapdRxDrugRemark(), allRemark) ) {
				isPharmRemarkExist = true;
			}			
		} else {
			// if old item is CAPD regimen (not possible case), oldMedOrderItem.getRegimen() can be null
			if ( compareRegimen(oldMedOrderItem.getRegimen(), newMedOrderItem.getRegimen(), outMedOrderItemRemark.getRegimenRemark(), allRemark) ) {
				isPharmRemarkExist = true;
			}
		}
		
		return isPharmRemarkExist;
	}

	private boolean checkSwitchCapdNonCapdDrugRemark(MedOrderItem oldMedOrderItem, MedOrderItem newMedOrderItem) {
		// if one is CAPD item and another is not CAPD item, set remark to all regimen
		if ( (oldMedOrderItem.getRxItemType() == RxItemType.Capd && newMedOrderItem.getRxItemType() != RxItemType.Capd) ||
				(oldMedOrderItem.getRxItemType() != RxItemType.Capd && newMedOrderItem.getRxItemType() == RxItemType.Capd) ) {
			if ( logger.isDebugEnabled() ) {
				logger.debug("pharmacy remark difference: one of item is CAPD item and another item is not CAPD item");
			}
			return true;
		}
		return false;
	}
	
	private boolean checkNonCapdDrugInfoRemark(MedOrderItem oldMedOrderItem, MedOrderItem newMedOrderItem) {
		int result;
		
		if (oldMedOrderItem.getRxItemType() != RxItemType.Capd && newMedOrderItem.getRxItemType() != RxItemType.Capd) {
			// check whether preparation is changed
			result = new CompareToBuilder().append( StringUtils.trimToEmpty(oldMedOrderItem.getStrength()).toUpperCase(), StringUtils.trimToEmpty(newMedOrderItem.getStrength()).toUpperCase() )
											.append( StringUtils.trimToEmpty(oldMedOrderItem.getExtraInfo()).toUpperCase(), StringUtils.trimToEmpty(newMedOrderItem.getExtraInfo()).toUpperCase() )
											.append( StringUtils.trimToEmpty(oldMedOrderItem.getItemCode()).toUpperCase(), StringUtils.trimToEmpty(newMedOrderItem.getItemCode()).toUpperCase() )
											.toComparison();
			if (result != 0) {
				if ( logger.isDebugEnabled() ) {
					logger.debug("pharmacy remark difference: preparation (old value: strength = [#0], extraInfo = [#1], itemCode = [#2], " +
										" new value: strength = [#3], extraInfo = [#4], itemCode = [#5])", 
										oldMedOrderItem.getStrength(), oldMedOrderItem.getExtraInfo(), oldMedOrderItem.getItemCode(),
										newMedOrderItem.getStrength(), newMedOrderItem.getExtraInfo(), newMedOrderItem.getItemCode());
				}
				return true;
			}
			
			// check whether drug name is changed
			result = new CompareToBuilder().append( StringUtils.trimToEmpty(oldMedOrderItem.getDisplayName()).toUpperCase(), StringUtils.trimToEmpty(newMedOrderItem.getDisplayName()).toUpperCase() )
											.append( StringUtils.trimToEmpty(oldMedOrderItem.getFormCode()).toUpperCase(), StringUtils.trimToEmpty(newMedOrderItem.getFormCode()).toUpperCase() )
											.append( StringUtils.trimToEmpty(oldMedOrderItem.getFormDesc()).toUpperCase(), StringUtils.trimToEmpty(newMedOrderItem.getFormDesc()).toUpperCase() )
											.append( StringUtils.trimToEmpty(oldMedOrderItem.getSaltProperty()).toUpperCase(), StringUtils.trimToEmpty(newMedOrderItem.getSaltProperty()).toUpperCase() )
											.append( oldMedOrderItem.getVolume(), newMedOrderItem.getVolume() )
											.append( StringUtils.trimToEmpty(oldMedOrderItem.getVolumeUnit()).toUpperCase(), StringUtils.trimToEmpty(newMedOrderItem.getVolumeUnit()).toUpperCase() )
											.append( StringUtils.trimToEmpty(oldMedOrderItem.getExtraInfo()).toUpperCase(), StringUtils.trimToEmpty(newMedOrderItem.getExtraInfo()).toUpperCase() )
											.append( StringUtils.trimToEmpty(oldMedOrderItem.getFmStatus()).toUpperCase(), StringUtils.trimToEmpty(newMedOrderItem.getFmStatus()).toUpperCase() )
											.toComparison();
			if (result != 0) {
				if ( logger.isDebugEnabled() ) {
					logger.debug("pharmacy remark difference: drug name (old value: nameType = [#0], aliasName = [#1], displayname = [#2], saltProperty = [#3], " +
										"formCode = [#4], strength = [#5], volume = [#6], volumeUnit = [#7], extraInfo = [#8], fmStatus = [#9])", 
										oldMedOrderItem.getNameType(), oldMedOrderItem.getAliasName(), oldMedOrderItem.getDisplayName(), oldMedOrderItem.getSaltProperty(), 
										oldMedOrderItem.getFormCode(), oldMedOrderItem.getStrength(), oldMedOrderItem.getVolume(), oldMedOrderItem.getVolumeUnit(), 
										oldMedOrderItem.getExtraInfo(), oldMedOrderItem.getFmStatus());
					logger.debug("pharmacy remark difference: drug name (new value: nameType = [#0], aliasName = [#1], displayname = [#2], saltProperty = [#3], " +
										"formCode = [#4], strength = [#5], volume = [#6], volumeUnit = [#7], extraInfo = [#8], fmStatus = [#9])",
										newMedOrderItem.getNameType(), newMedOrderItem.getAliasName(), newMedOrderItem.getDisplayName(), newMedOrderItem.getSaltProperty(), 
										newMedOrderItem.getFormCode(), newMedOrderItem.getStrength(), newMedOrderItem.getVolume(), newMedOrderItem.getVolumeUnit(), 
										newMedOrderItem.getExtraInfo(), newMedOrderItem.getFmStatus());
				}
				return true;
			}
		}
		
		return false;
	}

	private boolean compareCapdRxDrug(CapdRxDrug oldCapdRxDrug, CapdRxDrug newCapdRxDrug, CapdRxDrugRemark capdRxDrugRemark, boolean allRemark) {
		boolean isPharmRemarkExist = false;
		
		if ( allRemark || ! stringEqualCaseInsensitiveTrim(oldCapdRxDrug.getSupplierSystem(), newCapdRxDrug.getSupplierSystem()) ) {
			isPharmRemarkExist = true;
			capdRxDrugRemark.setSupplierSystemRemark(Boolean.TRUE);
		}

		if ( allRemark || ! stringEqualCaseInsensitiveTrim(oldCapdRxDrug.getCalciumStrength(), newCapdRxDrug.getCalciumStrength()) ) {
			isPharmRemarkExist = true;
			capdRxDrugRemark.setCalciumStrengthRemark(Boolean.TRUE);
		}
		
		List<CapdItem> inCapdItemList = null;
		if (oldCapdRxDrug != null) {
			inCapdItemList = oldCapdRxDrug.getCapdItemList();
		}
		
		if ( compareCapdItemList(inCapdItemList, newCapdRxDrug.getCapdItemList(), capdRxDrugRemark.getCapdItemRemarkList(), allRemark) ) {
			isPharmRemarkExist = true;
		}
		
		return isPharmRemarkExist;
	}
	
	private boolean compareCapdItemList(List<CapdItem> oldCapdItemList, List<CapdItem> newCapdItemList, List<CapdItemRemark> capdItemRemarkList, boolean allRemark) {
		boolean isPharmRemarkExist = false;
		
		for (int i=0; i<newCapdItemList.size(); i++) {
			boolean allCapdItemRemark = allRemark;
			
			CapdItem oldCapdItem = null;
			if (oldCapdItemList != null && i <= oldCapdItemList.size()-1) {
				oldCapdItem = oldCapdItemList.get(i);
			} else {
				allCapdItemRemark = true;
				if ( logger.isDebugEnabled() ) {
					logger.debug("pharmacy remark difference: new CAPD dose #0", i+1);
				}
			}
			CapdItem newCapdItem = newCapdItemList.get(i);
			CapdItemRemark capdItemRemark = capdItemRemarkList.get(i);

			if ( allCapdItemRemark || ! stringEqualCaseInsensitiveTrim(oldCapdItem.getFullConcentration(), newCapdItem.getFullConcentration()) ) {
				isPharmRemarkExist = true;
				capdItemRemark.setFullConcentrationRemark(Boolean.TRUE);
			}
			if ( allCapdItemRemark || ! valuesEqual(oldCapdItem.getDosage(), newCapdItem.getDosage()) ) {
				isPharmRemarkExist = true;
				capdItemRemark.setDosageRemark(Boolean.TRUE);
			}
			if ( allCapdItemRemark || ! stringEqualCaseInsensitiveTrim(oldCapdItem.getDosageUnit(), newCapdItem.getDosageUnit()) ) {
				isPharmRemarkExist = true;
				capdItemRemark.setDosageUnitRemark(Boolean.TRUE);
			}
			if ( allCapdItemRemark ) {
				isPharmRemarkExist = true;
				capdItemRemark.setDailyFreqRemark(Boolean.TRUE);
			}
			if ( allCapdItemRemark || ! valuesEqual(oldCapdItem.getPrn(), newCapdItem.getPrn()) ) {
				isPharmRemarkExist = true;
				capdItemRemark.setPrnRemark(Boolean.TRUE);
			}
			if ( allCapdItemRemark || ! valuesEqual(oldCapdItem.getPrn(), newCapdItem.getPrn()) ||
					(newCapdItem.getPrn() && ! valuesEqual(oldCapdItem.getPrnPercent(), newCapdItem.getPrnPercent())) ) {
				// check prn percent only when prn is true
				isPharmRemarkExist = true;
				capdItemRemark.setPrnPercentRemark(Boolean.TRUE);
			}
			if ( allCapdItemRemark || ( ! valuesEqual(oldCapdItem.getDuration(), newCapdItem.getDuration()) && 
					! valuesEqual(oldCapdItem.getDurationInDay(), newCapdItem.getDurationInDay()) ) ) {
				isPharmRemarkExist = true;
				capdItemRemark.setDurationRemark(Boolean.TRUE);
			}
			if ( allCapdItemRemark || ( ! valuesEqual(oldCapdItem.getDurationUnit(), newCapdItem.getDurationUnit()) &&
					! valuesEqual(oldCapdItem.getDurationInDay(), newCapdItem.getDurationInDay()) ) ) {
				isPharmRemarkExist = true;
				capdItemRemark.setDurationUnitRemark(Boolean.TRUE);
			}
			if ( allCapdItemRemark || ! valuesEqual(oldCapdItem.getStartDate(), newCapdItem.getStartDate()) ) {
				isPharmRemarkExist = true;
				capdItemRemark.setStartDateRemark(Boolean.TRUE);
				if ( ! allCapdItemRemark && logger.isDebugEnabled() && oldCapdItem != null ) {
					logger.debug("pharmacy remark difference (CAPD dose #0): startDate (old value = [#1], new value = [#2])", i+1, oldCapdItem.getStartDate(), newCapdItem.getStartDate());
				}
			}
			if ( allCapdItemRemark || ! valuesEqual(oldCapdItem.getEndDate(), newCapdItem.getEndDate()) ) {
				isPharmRemarkExist = true;
				capdItemRemark.setEndDateRemark(Boolean.TRUE);
				if ( ! allCapdItemRemark && logger.isDebugEnabled() && oldCapdItem != null ) {
					logger.debug("pharmacy remark difference (CAPD dose #0): endDate (old value = [#1], new value = [#2])", i+1, oldCapdItem.getEndDate(), newCapdItem.getEndDate());
				}
			}
			if ( allCapdItemRemark || ! valuesEqual(oldCapdItem.getDispQty(), newCapdItem.getDispQty()) ) {
				isPharmRemarkExist = true;
				capdItemRemark.setDispQtyRemark(Boolean.TRUE);
			}
			if ( allCapdItemRemark || ! stringEqualCaseInsensitiveTrim(oldCapdItem.getBaseUnit(), newCapdItem.getBaseUnit()) ) {
				isPharmRemarkExist = true;
				capdItemRemark.setBaseUnitRemark(Boolean.TRUE);
			}
		}
			
		if (oldCapdItemList != null && oldCapdItemList.size() > newCapdItemList.size()) {
			// some capd item is deleted from tail of capd item list but remaining capd item is the same
			isPharmRemarkExist = true;
		}
		
		return isPharmRemarkExist;
	}
	
	private boolean compareRegimen(Regimen oldRegimen, Regimen newRegimen, RegimenRemark regimenRemark, boolean allRemark) {
		boolean isPharmRemarkExist = false;
		
		if ( allRemark || ! valuesEqual(oldRegimen.getType(), newRegimen.getType()) ) {
			allRemark = true;
			isPharmRemarkExist = true;
			regimenRemark.setTypeRemark(Boolean.TRUE);
		}
			
		List<DoseGroup> inDoseGroupList = null;
		if (oldRegimen != null) {
			inDoseGroupList = oldRegimen.getDoseGroupList();
		}
		
		if ( compareDoseGroupList(inDoseGroupList, newRegimen.getDoseGroupList(), regimenRemark.getDoseGroupRemarkList(), allRemark) ) {
			isPharmRemarkExist = true;
		}
		
		return isPharmRemarkExist;
	}
	
	private boolean compareDoseGroupList(List<DoseGroup> oldDoseGroupList, List<DoseGroup> newDoseGroupList, List<DoseGroupRemark> doseGroupRemarkList, boolean allRemark) {
		boolean isPharmRemarkExist = false;

		for (int i=0; i<newDoseGroupList.size(); i++) {
			boolean allGroupRemark = allRemark;
			
			DoseGroup oldDoseGroup = null;
			if (oldDoseGroupList != null && i <= oldDoseGroupList.size()-1) {
				oldDoseGroup = oldDoseGroupList.get(i);
			} else {
				allGroupRemark = true;
				if ( logger.isDebugEnabled() ) {
					logger.debug("pharmacy remark difference: new dose group #0", i+1);
				}
			}
			DoseGroup newDoseGroup = newDoseGroupList.get(i);
			DoseGroupRemark doseGroupRemark = doseGroupRemarkList.get(i);

			if ( allGroupRemark ||  ( ! valuesEqual(oldDoseGroup.getDuration(), newDoseGroup.getDuration()) &&
					! valuesEqual(oldDoseGroup.getDurationInDay(), newDoseGroup.getDurationInDay()) ) ) {
				isPharmRemarkExist = true;
				doseGroupRemark.setDurationRemark(Boolean.TRUE);
			}
			if ( allGroupRemark ||  ( ! valuesEqual(oldDoseGroup.getDurationUnit(), newDoseGroup.getDurationUnit()) &&
					! valuesEqual(oldDoseGroup.getDurationInDay(), newDoseGroup.getDurationInDay()) ) ) {
				isPharmRemarkExist = true;
				doseGroupRemark.setDurationUnitRemark(Boolean.TRUE);
			}
			if ( allGroupRemark || ! valuesEqual(oldDoseGroup.getStartDate(), newDoseGroup.getStartDate()) ) {
				isPharmRemarkExist = true;
				doseGroupRemark.setStartDateRemark(Boolean.TRUE);
				if ( ! allGroupRemark && logger.isDebugEnabled() && oldDoseGroup != null ) {
					logger.debug("pharmacy remark difference (dose group #0): startDate (old value = [#1], new value = [#2])", i+1, oldDoseGroup.getStartDate(), newDoseGroup.getStartDate());
				}
			}
			if ( allGroupRemark || ! valuesEqual(oldDoseGroup.getEndDate(), newDoseGroup.getEndDate()) ) {
				isPharmRemarkExist = true;
				doseGroupRemark.setEndDateRemark(Boolean.TRUE);
				if ( ! allGroupRemark && logger.isDebugEnabled() && oldDoseGroup != null ) {
					logger.debug("pharmacy remark difference (dose group #0): endDate (old value = [#1], new value = [#2])", i+1, oldDoseGroup.getEndDate(), newDoseGroup.getEndDate());
				}
			}
			
			List<Dose> inDoseList = null;
			if (oldDoseGroup != null) {
				inDoseList = oldDoseGroup.getDoseList();
			}
			
			if ( compareDoseList(inDoseList, newDoseGroup.getDoseList(), doseGroupRemark.getDoseRemarkList(), allGroupRemark) ) {
				isPharmRemarkExist = true;
			}
		}
		
		if (oldDoseGroupList != null && oldDoseGroupList.size() > newDoseGroupList.size()) {
			// some dose group is deleted from tail of dose group list but remaining dose group is the same
			isPharmRemarkExist = true;
		}
		
		return isPharmRemarkExist;
	}
	
	private boolean compareDoseList(List<Dose> oldDoseList, List<Dose> newDoseList, List<DoseRemark> doseRemarkList, boolean allRemark) {
		boolean isPharmRemarkExist = false;
		
		for (int i=0; i<newDoseList.size(); i++) {
			boolean allDoseRemark = allRemark;
			
			Dose oldDose = null;
			if (oldDoseList != null && i <= oldDoseList.size()-1) {
				oldDose = oldDoseList.get(i);
			} else {
				allDoseRemark = true;
				if ( logger.isDebugEnabled() ) {
					logger.debug("pharmacy remark difference: new dose #0", i+1);
				}
			}
			Dose newDose = newDoseList.get(i);
			DoseRemark doseRemark = doseRemarkList.get(i);

			if ( allDoseRemark || ! valuesEqual(oldDose.getDosage(), newDose.getDosage()) ) {
				isPharmRemarkExist = true;
				doseRemark.setDosageRemark(Boolean.TRUE);
			}
			if ( allDoseRemark || ! stringEqualCaseInsensitiveTrim(oldDose.getDosageUnit(), newDose.getDosageUnit()) ) {
				isPharmRemarkExist = true;
				doseRemark.setDosageUnitRemark(Boolean.TRUE);
			}
			if ( allDoseRemark || ! valuesEqual(oldDose.getPrn(), newDose.getPrn()) ) {
				isPharmRemarkExist = true;
				doseRemark.setPrnRemark(Boolean.TRUE);
			}
			if ( allDoseRemark || ! valuesEqual(oldDose.getPrn(), newDose.getPrn()) || 
					(newDose.getPrn() && ! valuesEqual(oldDose.getPrnPercent(), newDose.getPrnPercent())) ) {
				// check prn percent only when prn is true
				isPharmRemarkExist = true;
				doseRemark.setPrnPercentRemark(Boolean.TRUE);
				if ( ! allDoseRemark && logger.isDebugEnabled() && oldDose != null ) {
					logger.debug("pharmacy remark difference (dose #0): prnPercent (old value = [#1], new value = [#2])", i+1, oldDose.getPrnPercent(), newDose.getPrnPercent());
				}
			}
			if ( allDoseRemark || ! stringEqualCaseInsensitiveTrim(oldDose.getSiteCode(), newDose.getSiteCode()) || 
					 ! stringEqualCaseInsensitiveTrim(oldDose.getSupplSiteDesc(), newDose.getSupplSiteDesc()) ) {
				isPharmRemarkExist = true;
				doseRemark.setSiteRemark(Boolean.TRUE);
			}
			if ( allDoseRemark || ! compareFreq(oldDose.getDailyFreq(), newDose.getDailyFreq()) ) {
				isPharmRemarkExist = true;
				doseRemark.setDailyFreqRemark(Boolean.TRUE);
			}
			if ( allDoseRemark || ! compareFreq(oldDose.getSupplFreq(), newDose.getSupplFreq()) ) {
				isPharmRemarkExist = true;
				doseRemark.setSupplFrequencyRemark(Boolean.TRUE);
			}
			if ( allDoseRemark || ! stringEqualCaseInsensitiveTrim(oldDose.getAdminTimeCode(), newDose.getAdminTimeCode()) ||
					! stringEqualCaseInsensitiveTrim(oldDose.getAdminTimeDesc(), newDose.getAdminTimeDesc()) ) {
				isPharmRemarkExist = true;
				doseRemark.setAdminTimeCodeRemark(Boolean.TRUE);
			}
			if ( allDoseRemark || ! valuesEqual(oldDose.getDispQty(), newDose.getDispQty()) ) {
				isPharmRemarkExist = true;
				doseRemark.setDispQtyRemark(Boolean.TRUE);
				if ( ! allDoseRemark && logger.isDebugEnabled() && oldDose != null ) {
					logger.debug("pharmacy remark difference (dose #0): dispQty (old value = [#1], new value = [#2])", i+1, oldDose.getDispQty(), newDose.getDispQty());
				}
			}
			if ( allDoseRemark || ! stringEqualCaseInsensitiveTrim(oldDose.getBaseUnit(), newDose.getBaseUnit()) ) {
				// if disp qty is empty before and after modify, skip checking base unit (base unit will fallback after save item) 
				isPharmRemarkExist = true;
				doseRemark.setBaseUnitRemark(Boolean.TRUE);
				if ( ! allDoseRemark && logger.isDebugEnabled() && oldDose != null ) {
					logger.debug("pharmacy remark difference (dose #0): baseUnit (old value = [#1], new value = [#2])", i+1, oldDose.getBaseUnit(), newDose.getBaseUnit());
				}
			}
		}
		
		if (oldDoseList != null && oldDoseList.size() > newDoseList.size()) {
			// some dose is deleted from tail of dose list but remaining dose is the same
			isPharmRemarkExist = true;
		}
			
		return isPharmRemarkExist;
	}
	
	private boolean compareFreq(Freq oldFreq, Freq newFreq) {
		boolean oldFreqCodeExist = false;
		boolean newFreqCodeExist = false;
		if (oldFreq != null && oldFreq.getCode() != null) {
			oldFreqCodeExist = true;
		}
		if (newFreq != null && newFreq.getCode() != null) {
			newFreqCodeExist = true;
		}
		if ( ! oldFreqCodeExist && ! newFreqCodeExist) {
			return true;
		}
		if (oldFreqCodeExist != newFreqCodeExist) {
			return false;
		}
		
		if ( ! stringEqualCaseInsensitiveTrim(oldFreq.getCode(), newFreq.getCode()) ) {
			return false;
		}

		if ( ! "E".equals(oldFreq.getMultiplierType()) || ! "E".equals(newFreq.getMultiplierType()) ) {
			if ( ! stringEqualCaseInsensitiveTrim(oldFreq.getDesc(), newFreq.getDesc()) ) {
				return false;
			}
		}
		
		boolean oldFreqParamExist = false;
		boolean newFreqParamExist = false;
		if (oldFreq.getParam() != null && oldFreq.getParam().length > 0) {
			oldFreqParamExist = true;
		}
		if (newFreq.getParam() != null && newFreq.getParam().length > 0) {
			newFreqParamExist = true;
		}		
		if ( ! oldFreqParamExist && ! newFreqParamExist) {
			return true;
		}
		if (oldFreqParamExist != newFreqParamExist) {
			return false;
		}		
		
		if (oldFreq.getParam().length != newFreq.getParam().length) {
			return false;
		}
		
		for (int i=0; i<newFreq.getParam().length; i++) {
			if ( ! stringEqualCaseInsensitiveTrim(oldFreq.getParam()[i], newFreq.getParam()[i]) ) {
				return false;
			}
		}
		
		return true;
	}
	
	private boolean valuesEqual(Object oldValue, Object newValue) {
		int result = new CompareToBuilder().append(oldValue, newValue)
									 .toComparison();
		return result == 0;
	}

	private boolean stringEqualCaseSensitive(String oldValue, String newValue) {
		return stringEqual(oldValue, newValue, true, true);
	}

	private boolean stringEqualCaseInsensitiveTrim(String oldValue, String newValue) {
		return stringEqual(oldValue, newValue, true, false);
	}
	
	private boolean stringEqual(String oldValue, String newValue, boolean trim, boolean caseSensitive) {
		if (trim) {
			oldValue = StringUtils.trimToEmpty(oldValue);
			newValue = StringUtils.trimToEmpty(newValue);
		} else { 
			oldValue = StringUtils.defaultString(oldValue);
			newValue = StringUtils.defaultString(newValue);
		}
		
		if ( ! caseSensitive) {
			oldValue = oldValue.toUpperCase();
			newValue = newValue.toUpperCase();
		}
		
		int result = new CompareToBuilder().append(oldValue, newValue)
									 .toComparison();
		return result == 0;
	}
	
	@Remove
	public void destroy() {
		if (pharmRemarkTemplateOptionList != null) {
			pharmRemarkTemplateOptionList = null;
		}
		if (pharmRemarkExist != null) {
			pharmRemarkExist = null;
		}
		if (pharmRemarkModifyFallback != null) {
			pharmRemarkModifyFallback = null;
		}
		if (pharmRemarkLogonFailMsg != null) {
			pharmRemarkLogonFailMsg = null;
		}
		if (pharmRemarkDate != null) {
			pharmRemarkDate = null;
		}
		if (pharmRemarkItemNum != null) {
			pharmRemarkItemNum = null;
		}
	}
}
