package hk.org.ha.model.pms.biz.vetting;

import static hk.org.ha.model.pms.prop.Prop.ALERT_HLA_DRUG_DISPLAYNAME;
import static hk.org.ha.model.pms.prop.Prop.CDDH_LEGACY_PERSISTENCE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.CHARGING_STANDARD_CHARGEABLEUNIT;
import static hk.org.ha.model.pms.prop.Prop.CHARGING_STANDARD_REFILL_PARTIALPAYMENT_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.VETTING_CAPD_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.VETTING_DH_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.WORKFLOW_EDS_BPM_ENABLED;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.charging.InvoiceManagerLocal;
import hk.org.ha.model.pms.biz.onestop.SuspendPrescManagerLocal;
import hk.org.ha.model.pms.biz.order.DispOrderManagerLocal;
import hk.org.ha.model.pms.biz.order.OrderManagerLocal;
import hk.org.ha.model.pms.biz.refill.RefillScheduleListManagerBean;
import hk.org.ha.model.pms.biz.refill.RefillScheduleUtilsLocal;
import hk.org.ha.model.pms.biz.refill.SfiRefillScheduleListManagerBean;
import hk.org.ha.model.pms.biz.refill.SfiRefillScheduleListManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.corp.vo.EndVettingResult;
import hk.org.ha.model.pms.corp.vo.StartVettingResult;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.exception.vetting.VettingException;
import hk.org.ha.model.pms.persistence.RemarkEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.CapdVoucher;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedOrderItemAlert;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.persistence.disp.RefillScheduleItem;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.YesNoBlankFlag;
import hk.org.ha.model.pms.udt.alert.mds.AlertType;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.disp.RemarkStatus;
import hk.org.ha.model.pms.udt.label.DeltaChangeType;
import hk.org.ha.model.pms.udt.refill.DocType;
import hk.org.ha.model.pms.udt.refill.RefillScheduleRefillStatus;
import hk.org.ha.model.pms.udt.refill.RefillScheduleStatus;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.CapdVoucherStatus;
import hk.org.ha.model.pms.vo.charging.SaveInvoiceListInfo;
import hk.org.ha.model.pms.vo.label.DispLabel;
import hk.org.ha.model.pms.vo.security.PropMap;
import hk.org.ha.model.pms.vo.vetting.CapdSplit;
import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;
import hk.org.ha.model.pms.vo.vetting.PrintJobManagerInfo;
import hk.org.ha.model.pms.workflow.DispensingPmsModeLocal;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;

@AutoCreate
@Stateless
@Name("vettingManager")
@MeasureCalls
public class VettingManagerBean implements VettingManagerLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
				
	@In
	private OrderManagerLocal orderManager;

	@In
	private DispOrderManagerLocal dispOrderManager;
	
	@In
	private InvoiceManagerLocal invoiceManager;
	
	@In
	private CapdVoucherManagerLocal capdVoucherManager;
		
	@In
	private SfiRefillScheduleListManagerLocal sfiRefillScheduleListManager;
		
	@In
	private SuspendPrescManagerLocal suspendPrescManager;
	
	@In
	private RefillScheduleUtilsLocal refillScheduleUtils;
	
	@In
	private UnvetManagerLocal unvetManager;
	
	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
		
	@In
	private DispensingPmsModeLocal dispensingPmsMode;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private AuditLogger auditLogger;
		
	@In
	private UamInfo uamInfo;

	@In
	private Workstore workstore;
	
	@In
	private Workstation workstation;

	@In
	private PropMap propMap;
			
	private final static String ACTIVE_OPERATION_MODE = "active.operation.mode";
	
	public StartVettingResult startVetting(Long medOrderId, String hkid) {
		StartVettingResult result = corpPmsServiceProxy.startVetting(null, medOrderId, workstore.getHospCode(), hkid);
		if (result.getSuccessFlag()) {
			MedOrder medOrder = result.getMedOrder();
			medOrder.setWorkstationId(workstation.getId());
			medOrder.setWorkstore(workstore); //PMSB-4635

			for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
				for (MedOrderItemAlert medOrderItemAlert : medOrderItem.getMedOrderItemAlertList()) {
					if (medOrderItemAlert.getAlertType() == AlertType.OpHla) {
						medOrder.setHlaFlag(true);
					}
				}
			}
			medOrder.loadDmInfo();		
		}
		return result;
	}
	
	public StartVettingResult startVetting(MedOrder medOrder, String hkid) {

		// request ownership from corp
		StartVettingResult result = corpPmsServiceProxy.startVetting(
				medOrder.isDhOrder() ? medOrder.getMedCase().getCaseNum() : medOrder.getOrderNum(),
				medOrder.getId(),
				workstore.getHospCode(),
				hkid
			);
		if (!result.getSuccessFlag()) {
			return result;
		}

		medOrder.setProcessingFlag(Boolean.TRUE); // mark processing
		medOrder.setWorkstationId(workstation.getId());

		// update local MedOrder
		if (result.getMedOrder() != null && medOrder.getStatus() == MedOrderStatus.Outstanding) {
			
			if (medOrder.getId() != null) {
				MedOrder oldMedOrder = em.find(MedOrder.class, medOrder.getId());
				orderManager.loadOrderList(oldMedOrder);
				oldMedOrder.markSysDeleted();
				// no need to mark RefillSchedule to SysDeleted, since oldMedOrder must be unvet order
			}
			
			Ticket ticket = medOrder.getTicket();
			result.getMedOrder().loadDmInfo();
			medOrder = orderManager.insertMedOrder(result.getMedOrder());
			em.clear();
			medOrder.setTicket(ticket);
			setMedOrder(medOrder);
		}
		
		if (medOrder.isDhOrder() && result.getUpdateFlag()) {
			medOrder.setDhLatestFlag(false);
		}

		return result;
	}
		
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public PrintJobManagerInfo endVetting(OrderViewInfo orderViewInfo, Object order) throws VettingException {		
		
		DispOrder dispOrder = null;
		PharmOrder pharmOrder = null;
		if (order instanceof DispOrder) {
			dispOrder = (DispOrder) order;
			pharmOrder = dispOrder.getPharmOrder();
		} else {
			pharmOrder = (PharmOrder) order;
		}
				
		/////////////////////////////////////////////////////
		// Retrieve all necessary info, 
		// and delete the previous PharmOrder
		/////////////////////////////////////////////////////

		// MedOrder in context
		MedOrder medOrder = pharmOrder.getMedOrder(); 
		medOrder.markPharmRemarkStatus();		
		if (!medOrder.isDhOrder()) {
			medOrder.markHlaFlag(ALERT_HLA_DRUG_DISPLAYNAME.get());
		}
		medOrder.setWithholdUser(null);
		medOrder.setWithholdUserName(null);
		medOrder.setWithholdDate(null);

		// retrieve the standard RefillSchedule list, which may need to save and update MedOrderItem unitOfCharge
		List<RefillSchedule> rsList = this.buildRefillScheduleListIfNeed(pharmOrder);

		// detect changes in orderline
		Boolean remarkFlag = medOrder.hasChanged();
		
		// retrieve capd voucher list
		List<CapdVoucher> cvList = this.retrieveCapdVoucherList(dispOrder);
		
		// mark delete and detach everything
		List<Invoice> voidInvoiceList = new ArrayList<Invoice>();
		DispOrder prevDispOrder = markOrderSysDeleted(pharmOrder, remarkFlag);
		if (prevDispOrder != null) {
			voidInvoiceList.addAll(prevDispOrder.getInvoiceList());
		}

		// save the previous for later use
		Long prevPharmOrderId = pharmOrder.getId(); 

		// unlink MedOrder, MedCase
		pharmOrder.disconnectMedOrder();		
		
		/////////////////////////////////////////////////////
		// Start Saving into Database
		/////////////////////////////////////////////////////
		
		// save PharmOrder
		this.savePharmOrder(pharmOrder);
				
		// save DispOrder
		dispOrder = this.saveDispOrder(pharmOrder, rsList, prevDispOrder, orderViewInfo.getSaveSuspendCode());				
		
		// save standard RefillSchedule
		this.saveRefillSchedule(orderViewInfo, rsList, dispOrder, prevPharmOrderId);
		
		// save Invoice
		this.saveInvoice(orderViewInfo, dispOrder, voidInvoiceList);

		// set large layout flag for label reprint and CDDH label preview
		// (note: since the flag is needed to sync to CORP for CDDH label preview, it is set here instead of in render and print function)
		// (note: the flag should be set after save invoice, because suspend flag may be updated during save invoice)
		dispOrderManager.updateDispOrderLargeLayoutFlag(dispOrder);
		
		Map<Integer,Boolean> sfiRsReDispFlagMap = refillScheduleUtils.retrieveRefillScheduleReDispFlagMap(prevPharmOrderId, DocType.Sfi);
		
		// save sfi RefillSchedule	
		this.saveSfiRefillSchedule(orderViewInfo, dispOrder, sfiRsReDispFlagMap, prevDispOrder);
		
		// save CapdVoucher
		List<CapdVoucher> voucherList = this.saveCapdVoucher(orderViewInfo, dispOrder, cvList);		
				
		//update DispOrder RevokeFlag
		suspendPrescManager.markRevokeFlagForUncollectOrder(dispOrder);

		// detach all
		em.flush();
		em.clear();
		
		logger.info("Refill Schedule(s) do/po count : #0/#1 ", 
				dispOrder.getRefillScheduleList().size(), 
				pharmOrder.getRefillScheduleList().size());
				
		logger.info("Capd Voucher(s) count : #0 ", 
				dispOrder.getCapdVoucherList().size());

		
		/////////////////////////////////////////////////////
		// Sync PharmOrder/DispOrder/Invoice/CapdVoucher to Corp
		/////////////////////////////////////////////////////

		// link MedOrder, MedCase
		pharmOrder.reconnectMedOrder();		
				
		// disconnect all RefillSchedule no need send to corp
		pharmOrder.disconnectRefillScheduleList();
		dispOrder.disconnectRefillScheduleList();
		
		// set last update date
		dispOrder.getPharmOrder().getMedOrder().setCmsUpdateDate(new DateTime().toDate());
								
		// send MedOrder, PharmOrder to corp
		dispOrder.getPharmOrder().clearDmInfo();
		EndVettingResult result = corpPmsServiceProxy.endVetting(
						dispOrder, 
						prevPharmOrderId,
						workstore.getHospCode(), 
						remarkFlag,
						CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED.get(false),
						CDDH_LEGACY_PERSISTENCE_ENABLED.get(false),
						VETTING_DH_ENABLED.get(false));
		dispOrder.getPharmOrder().loadDmInfo();


		if (!result.getSuccessFlag()) {
			orderViewInfo.addErrorMsg(result.getMessageCode(), result.getMessageParam());
			throw new VettingException(orderViewInfo.getErrorMsgCodeList().get(0), orderViewInfo.getErrorMsgParamList().get(0));
		}
		
		/////////////////////////////////////////////////////
		// Create and Link the MedOrder from Corp
		/////////////////////////////////////////////////////
		
		// mapping from poi to moi		
		Map<Integer, Integer> medOrderItemMap = result.getPharmOrder().createMedOrderItemMap();

		// save MedOrder
		medOrder = this.saveMedOrder(medOrder, result.getPharmOrder().getMedOrder(), remarkFlag);
		
		// update PharmOrder
		pharmOrder = this.updatePharmOrder(medOrder, result.getPharmOrder(), medOrderItemMap);
		
		//update RefillScheduleCouponNum
		this.updateRefillScheduleCouponNum(medOrder, pharmOrder, prevPharmOrderId);
		
		// sync delta change type from CORP disp order to PMS disp order 
		// (because delta change type is checked and updated in CORP)
		updatePharmDispOrderItemDeltaChangeType(result.getDeltaChangeTypeMap(), dispOrder.getId());
		
		// clear the ticket and processingFlag
		medOrder.setProcessingFlag(Boolean.FALSE);
		medOrder.setWorkstationId(null);
		medOrder.setTicket(null);
		
		//detach all
		em.flush();
		em.clear();
		
		// link the latest pharmOrder back to dispOrder 
		dispOrder.replace(pharmOrder);		
		
		if (this.isBpmMode()) {
			dispensingPmsMode.startDispensingFlow(dispOrder.getId());
		}
		
		PrintJobManagerInfo printJobManagerInfo = new PrintJobManagerInfo();
		printJobManagerInfo.setDispOrder(dispOrder);
		printJobManagerInfo.setVoucherList(voucherList);
		return printJobManagerInfo;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public PrintJobManagerInfo endVettingForSfiRefill(OrderViewInfo orderViewInfo, RefillSchedule refillSchedule, Ticket ticket) throws VettingException {
		
		RefillSchedule inRefillSchedule = em.find(RefillSchedule.class, orderViewInfo.getRefillScheduleId());
		Long prevDispOrderId = null;
		DispOrder prevDispOrder = null;
		List<Invoice> voidInvoiceList = new ArrayList<Invoice>();
		List<RefillSchedule> prevSfiRsList = new ArrayList<RefillSchedule>();
		
		// mark delete
		if (inRefillSchedule.getDispOrder() != null) {
			prevDispOrderId = inRefillSchedule.getDispOrder().getId();
			prevDispOrder = em.find(DispOrder.class, prevDispOrderId);
			prevSfiRsList = sfiRefillScheduleListManager.getSfiRefillScheduleList(prevDispOrder);			
			voidInvoiceList = prevDispOrder.markSysDeleted();
			
			em.flush();
			em.clear();
		}
		
		ticket = em.find(Ticket.class, ticket.getId());
		
		//reconnect PharmOrderItem and refillSchedule
		PharmOrder pharmOrder = (PharmOrder)Contexts.getSessionContext().get("pharmOrder");
		pharmOrder.reconnectRefillScheduleList();
		
		Map<Integer, Boolean> sfiRsReDispFlagMap = new HashMap<Integer, Boolean>();
		
		if( !prevSfiRsList.isEmpty() ){
			sfiRsReDispFlagMap = refillScheduleUtils.retrieveRefillScheduleReDispFlagMap(pharmOrder.getId(), DocType.Sfi);
		}
		
		// update RefillSchedule
		List<RefillSchedule> currSfiRsList = this.updateRefillScheduleListForSfi(pharmOrder);
		
		//restore prevRefillSchedule
		restorePrevRefillScheduleListForSfi(currSfiRsList, prevSfiRsList);

		// save DispOrder		
		DispOrder dispOrder = this.saveDispOrderForSfiRefillList(currSfiRsList, ticket, prevDispOrder);		
		
		// save Invoice
		this.saveInvoice(orderViewInfo, dispOrder, voidInvoiceList);

		// set large layout flag for label reprint and CDDH label preview
		// (note: since the flag is needed to sync to CORP for CDDH label preview, it is set here instead of in render and print function)
		// (note: the flag should be set after save invoice, because suspend flag may be updated during save invoice)
		dispOrderManager.updateDispOrderLargeLayoutFlag(dispOrder);
		
		//update SfiScheduleStatus to None
		for(RefillSchedule currSfiRs : currSfiRsList){
			currSfiRs.setDispOrder(dispOrder);
			sfiRefillScheduleListManager.updateSfiRefillScheduleListStatus(orderViewInfo, currSfiRs);
		}
				
		//save if exist SfiRefillSchedule
		this.saveSfiRefillSchedule(orderViewInfo, dispOrder, sfiRsReDispFlagMap, prevDispOrder);
		
		//update PharmOrder refillCount
		PharmOrder managedPharmOrder = em.find(PharmOrder.class, pharmOrder.getId());
		Integer refillCount = managedPharmOrder.getRefillCount() + ( currSfiRsList.size() - prevSfiRsList.size() );
		managedPharmOrder.setRefillCount(refillCount);
		
		//update DispOrder RevokeFlag
		suspendPrescManager.markRevokeFlagForUncollectOrder(dispOrder);

		//detach all
		em.flush();
		em.clear();
		
		refillScheduleUtils.restorePharmOrderItemNum(dispOrder);
		
		logger.info("Refill Schedule(s) do/po count : #0/#1 ", 
				dispOrder.getRefillScheduleList().size(), 
				dispOrder.getPharmOrder().getRefillScheduleList().size());
		
		// send DispOrder to corp
		EndVettingResult result = dispOrderManager.refillDispensing(dispOrder, new ArrayList<MedOrderItem>());
		dispOrder.getPharmOrder().loadDmInfo();

		if (!result.getSuccessFlag()) {
			orderViewInfo.addErrorMsg(result.getMessageCode(), result.getMessageParam());
			throw new VettingException(orderViewInfo.getErrorMsgCodeList().get(0), orderViewInfo.getErrorMsgParamList().get(0));
		}
			
		MedOrder medOrder = em.find(MedOrder.class, managedPharmOrder.getMedOrder().getId());
		
		// clear the ticket and processingFlag
		medOrder.setProcessingFlag(Boolean.FALSE);
		medOrder.setWorkstationId(null);
		
		// sync delta change type from CORP disp order to PMS disp order 
		// (because delta change type is checked and updated in CORP)
		updatePharmDispOrderItemDeltaChangeType(result.getDeltaChangeTypeMap(), dispOrder.getId());
		
		//detach all
		em.flush();
		em.clear();
		
		if (this.isBpmMode()) {
			dispensingPmsMode.startDispensingFlow(dispOrder.getId());
		}
		
		PrintJobManagerInfo printJobManagerInfo = new PrintJobManagerInfo();
		printJobManagerInfo.setDispOrder(dispOrder);
		printJobManagerInfo.setVoucherList(null);
		return printJobManagerInfo;
	}	
	
	private boolean isBpmMode() {
		return "EDS".equals(propMap.getValue(ACTIVE_OPERATION_MODE)) && WORKFLOW_EDS_BPM_ENABLED.get();
	}

	
	public void releaseOrder(OrderViewInfo orderViewInfo, MedOrder medOrder) {
		
		if (orderViewInfo != null && !orderViewInfo.getTicketAssociateFlag()) {
			medOrder.setTicket(null);
		}

		medOrder.setProcessingFlag(Boolean.FALSE);
		medOrder.setWorkstationId(null);

		if (medOrder.isDhOrder()) {
			return;
		}
		
		corpPmsServiceProxy.cancelVetting(
				medOrder.getOrderNum(), 
				workstore.getHospCode());
	}
	
	public void releaseUnvetDhOrder(MedOrder medOrder){
		corpPmsServiceProxy.cancelVetting(
								medOrder.getMedCase().getCaseNum(), 
								workstore.getHospCode());
	}
	
	@SuppressWarnings("unchecked")
	public Ticket retrievePrevTicket(String prevOrderNum)
    {
    	List<DispOrder> dispOrderList = em.createQuery(
    			"select o from DispOrder o" + // 20140314 index check : MedOrder.orderNum : I_MED_ORDER_01
    			" where o.pharmOrder.medOrder.orderNum = :prevOrderNum" +
    			" and o.refillFlag = :refillFlag" +
    			" order by o.pharmOrder.medOrder.createDate desc")
    			.setParameter("prevOrderNum", prevOrderNum)
    			.setParameter("refillFlag", Boolean.FALSE)
    			.setHint(QueryHints.BATCH, "o.ticket")
    			.getResultList();
    	
		if (dispOrderList.isEmpty())
		{
			List<MedOrder> medOrderList = (List<MedOrder>) em.createQuery(
					"select o from MedOrder o" + // 20140314 index check : MedOrder.orderNum : I_MED_ORDER_01
					" where o.orderNum = :prevOrderNum" +
					" order by o.createDate desc")
					.setParameter("prevOrderNum", prevOrderNum)
					.getResultList();

			if (medOrderList.isEmpty()) {
				return null;
			} else {
				MedOrder medOrder = medOrderList.get(0);
				return (StringUtils.isBlank(medOrder.getPrevOrderNum())?null:retrievePrevTicket(medOrder.getPrevOrderNum()));
			}
		}
		else
		{
			return dispOrderList.get(0).getTicket();
		}
    }
	
	public void unvetOrder(Long medOrderId) {
		unvetMedOrder(medOrderId, false);
		auditLogger.log("#0523", uamInfo.getUserId(), workstation.getHostName(), medOrderId);
	}

	public Long unvetMedOrder(Long medOrderId, boolean removeOrderByPms) {
		List<Long> returnList = unvetManager.unvetMedOrder(medOrderId, false, removeOrderByPms);
		
        if (this.isBpmMode())
    	{
    		Contexts.getSessionContext().set("dispOrderId", returnList.get(1));
    		dispensingPmsMode.unvetOrder();
    	}
        
        Long updatedMedOrderId = returnList.get(0);
        return updatedMedOrderId;
	}
	
	public void removeOrder(MedOrder medOrder, DispOrder manualDispOrder, String logonUser, String remarkText) {

		RemarkEntity remarkEntity = null;
		
		if (medOrder.getDocType() == MedOrderDocType.Manual) {
			medOrder.markDeleted();
			refillScheduleUtils.markSysDeletedByMedOrder(medOrder);
		} else {
			// mark order to unconfirm
			medOrder.setRemarkStatus(RemarkStatus.Unconfirm);
	
			Date remarkCreateDate = new Date();
			
			medOrder.setRemarkText(remarkText);
			medOrder.setRemarkCreateDate(remarkCreateDate);
			medOrder.setRemarkCreateUser(logonUser);
			medOrder.setRemarkConfirmDate(null);
			medOrder.setRemarkConfirmUser(null);
			
			remarkEntity = new RemarkEntity();
			remarkEntity.setRemarkText(remarkText);
			remarkEntity.setRemarkCreateDate(remarkCreateDate);
			remarkEntity.setRemarkCreateUser(logonUser);
			remarkEntity.setRemarkConfirmDate(null);
			remarkEntity.setRemarkConfirmUser(null);
			
			// clear withhold info for discharge order since the order has completed prevet process
			medOrder.setWithholdUser(null);
			medOrder.setWithholdUserName(null);
			medOrder.setWithholdDate(null);			
		}
		
		medOrder.setCmsUpdateDate(new DateTime().toDate());
		
		DispOrder dispOrder;
		if (medOrder.getDocType() == MedOrderDocType.Manual) {
			dispOrder = manualDispOrder;
		} else {
			dispOrder = new DispOrder();
			PharmOrder pharmOrder = new PharmOrder();
			dispOrder.setPharmOrder(pharmOrder);
			pharmOrder.setMedOrder(medOrder);
		}
		
		dispOrder.getPharmOrder().clearDmInfo();
		corpPmsServiceProxy.removeOrder(dispOrder, remarkEntity, CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED.get(false), CDDH_LEGACY_PERSISTENCE_ENABLED.get(false));
		dispOrder.getPharmOrder().loadDmInfo();
	}
		
	public void removeSfiRefillOrder(Long dispOrderId){
		DispOrder dispOrder = em.find(DispOrder.class, dispOrderId);
		
		List<RefillSchedule> cancelDispRsList = sfiRefillScheduleListManager.getSfiRefillScheduleList(dispOrder);

		sfiRefillScheduleListManager.cancelDispenseSfiRefillSchedule(cancelDispRsList);
		
		dispOrderManager.cancelDispensing(dispOrder);
		
		//update PharmOrder refillCount
		PharmOrder managedPharmOrder = em.find(PharmOrder.class, dispOrder.getPharmOrder().getId());
		Integer refillCount = managedPharmOrder.getRefillCount() - cancelDispRsList.size();
		managedPharmOrder.setRefillCount(refillCount);
		
		//unlock
		MedOrder managedMedOrder = managedPharmOrder.getMedOrder();
		managedMedOrder.setProcessingFlag(false);
		managedMedOrder.setWorkstationId(null);
	}
	
	private void restorePrevRefillScheduleListForSfi( List<RefillSchedule> currSfiRsList, List<RefillSchedule> prevSfiRsList ){
		if( prevSfiRsList.isEmpty() ){
			return;
		}
		
		List<Long> sfiRsIdList = new ArrayList<Long>();
		for( RefillSchedule currRs : currSfiRsList ){
			sfiRsIdList.add(currRs.getId());
		}
		
		List<RefillSchedule> restoreList = new ArrayList<RefillSchedule>();
		for( RefillSchedule prevRs : prevSfiRsList ){
			if( !sfiRsIdList.contains(prevRs.getId()) ){
				restoreList.add(prevRs);
			}
		}
		sfiRefillScheduleListManager.cancelDispenseSfiRefillSchedule(restoreList);
	}
	
	private DispOrder markOrderSysDeleted(PharmOrder pharmOrder, Boolean remarkFlag) {
		List<DispOrder> delDispOrderList = orderManager.markOrderSysDeleted(pharmOrder, pharmOrder.getId(), remarkFlag);
		for (DispOrder delDispOrder : delDispOrderList) {
			if (!delDispOrder.getRefillFlag()) {
				return delDispOrder;
			}
		}
		return null;
	}
	
	private List<RefillSchedule> buildRefillScheduleListIfNeed(PharmOrder pharmOrder) 
	{
		//Reconnect RefillScheduleList to PharmOrder
		pharmOrder.reconnectRefillScheduleList();

		List<RefillSchedule> rsList = pharmOrder.getRefillScheduleList(DocType.Standard);		
		
		List<RefillSchedule> resultRsList = new ArrayList<RefillSchedule>();
		
		// disconnect from PharmOrder
		for (RefillSchedule refillSchedule : rsList) {
			refillSchedule.disconnect();				
			if( refillSchedule.getNumOfRefill() > 1 ){
				resultRsList.add(refillSchedule);
			}
		}
		
		MedOrder medOrder = pharmOrder.getMedOrder();		
		List<Integer> moiOrgItemNumRefillList = new ArrayList<Integer>();
		int daysPerChargeableUnit = propMap.getValueAsInt(CHARGING_STANDARD_CHARGEABLEUNIT.getName(), 112);
		
		if( MedOrderDocType.Manual == medOrder.getDocType() ||
			(MedOrderDocType.Normal == medOrder.getDocType() && propMap.getValueAsBool(CHARGING_STANDARD_REFILL_PARTIALPAYMENT_ENABLED.getName(), false)) ){		
			
			for( RefillSchedule resultRs : resultRsList ){
				if( resultRs.getRefillNum() == 1 ){
					resultRs.connect(pharmOrder);					
					for( RefillScheduleItem resultRsi : resultRs.getRefillScheduleItemList() ){
						MedOrderItem resultMoi = resultRsi.getPharmOrderItem().getMedOrderItem();
						if( !moiOrgItemNumRefillList.contains(resultMoi.getOrgItemNum()) && 
								resultMoi.getRegimen() != null && resultMoi.getRegimen().calTotalDurationInDay() > daysPerChargeableUnit ){
							BigDecimal unitOfCharge = BigDecimal.valueOf(resultRsi.getRefillDuration()).divide(BigDecimal.valueOf(daysPerChargeableUnit), RoundingMode.CEILING);
							if( unitOfCharge.intValue() != resultMoi.getUnitOfCharge() ){
								resultMoi.setUnitOfCharge(unitOfCharge.intValue());
								resultMoi.setChangeFlag(Boolean.TRUE);								
							}
							moiOrgItemNumRefillList.add(resultMoi.getOrgItemNum());
						}
					}
					resultRs.disconnect();
					break;
				}
			}			
		}
		
		for( MedOrderItem medOrderItem : medOrder.getMedOrderItemList() ){
			if (medOrder.getPrescType() != MedOrderPrescType.MpDischarge ) {
				if( ActionStatus.DispByPharm == medOrderItem.getActionStatus() && !moiOrgItemNumRefillList.contains(medOrderItem.getOrgItemNum()) && medOrderItem.getCapdRxDrug() == null ){
					if( medOrderItem.getRegimen().calTotalDurationInDay() > daysPerChargeableUnit && YesNoBlankFlag.Yes == medOrderItem.getCmsChargeFlag() ){
						BigDecimal unitOfCharge = BigDecimal.valueOf(medOrderItem.getRegimen().getDurationInDay()).divide(BigDecimal.valueOf(daysPerChargeableUnit), RoundingMode.CEILING);
						if( unitOfCharge.intValue() != medOrderItem.getUnitOfCharge() ){
							medOrderItem.setUnitOfCharge(unitOfCharge.intValue());
							medOrderItem.setChangeFlag(Boolean.TRUE);
						}
					}
					moiOrgItemNumRefillList.add(medOrderItem.getOrgItemNum());
				}
			} else {
				medOrderItem.setUnitOfCharge(Integer.valueOf(0));
			}
		}
		
		return resultRsList;
	}
	
	private List<CapdVoucher> retrieveCapdVoucherList(DispOrder dispOrder) 
	{
		List<CapdVoucher> cvList = new ArrayList<CapdVoucher>();
		if (dispOrder != null) {

			//Reconnect CapdVoucher to DispOrder
			dispOrder.reconnectCapdVoucherList();

			cvList.addAll(dispOrder.getCapdVoucherList());
		}
		
		return cvList;
	}
	
	private void savePharmOrder(PharmOrder pharmOrder) {

		// always save a new copy
		pharmOrder.clearId();		
		pharmOrder.calulateAdjQty();		
		pharmOrder.markCompleted();
		pharmOrder.markCapdVoucherFlag();
		
		// save pharmOrder
		em.persist(pharmOrder);
		
		em.flush();
	}
	
	private MedOrder saveMedOrder(MedOrder localMedOrder, MedOrder remoteMedOrder, Boolean remarkFlag) {

		Ticket ticket = localMedOrder.getTicket();			
		
		if (remarkFlag || localMedOrder.getId() == null || localMedOrder.getOrderNum() == null) 
		{
			localMedOrder = orderManager.insertMedOrder(remoteMedOrder);
			localMedOrder.loadChild();
		}
		else 
		{
			localMedOrder = em.find(MedOrder.class, localMedOrder.getId());
			localMedOrder.loadChild();
			localMedOrder.setPatient(orderManager.savePatient(remoteMedOrder.getPatient()));
			this.updateMedOrderStatus(localMedOrder, remoteMedOrder);
			localMedOrder.setCmsUpdateDate(remoteMedOrder.getCmsUpdateDate());
			localMedOrder.setVetBeforeFlag(remoteMedOrder.getVetBeforeFlag());
			localMedOrder.setVetDate(remoteMedOrder.getVetDate());
			localMedOrder.setUrgentFlag(remoteMedOrder.getUrgentFlag());
			localMedOrder.setPreVetUser(remoteMedOrder.getPreVetUser());
			localMedOrder.setPreVetDate(remoteMedOrder.getPreVetDate());
			localMedOrder.setPreVetUserName(remoteMedOrder.getPreVetUserName());
			for (MedOrderItem localMoi : localMedOrder.getMedOrderItemList()) {
				MedOrderItem remoteMedOrderItem = remoteMedOrder.getMedOrderItemByItemNum(localMoi.getItemNum());
				if (remoteMedOrderItem == null) {
					continue;
				}
				localMoi.setRxItem(remoteMedOrderItem.getRxItem());
				localMoi.setPreVetNoteText(remoteMedOrderItem.getPreVetNoteText());
				localMoi.setPreVetNoteUser(remoteMedOrderItem.getPreVetNoteUser());
				localMoi.setPreVetNoteDate(remoteMedOrderItem.getPreVetNoteDate());
			}
			localMedOrder.setWithholdUser(null);
			localMedOrder.setWithholdUserName(null);
			localMedOrder.setWithholdDate(null);
		}

		localMedOrder.setTicket(em.find(Ticket.class, ticket.getId()));

		em.flush();
		
		logger.info("MedOrder #0 (id=#1) saved", localMedOrder.getOrderNum(), localMedOrder.getId());
		
		return localMedOrder;
	}
	
	private PharmOrder updatePharmOrder(MedOrder medOrder, PharmOrder remotePharmOrder, Map<Integer, Integer> itemMap) {

		PharmOrder localPharmOrder = em.find(PharmOrder.class, remotePharmOrder.getId());
		localPharmOrder.loadChild();
		
		localPharmOrder.setMedCase(orderManager.saveMedCase(remotePharmOrder.getMedCase()));
		
		localPharmOrder.replace(medOrder, itemMap);

		em.flush();
		
		return localPharmOrder;
	}
	
	@SuppressWarnings("unchecked")
	private void updateRefillScheduleCouponNum(MedOrder medOrder, PharmOrder localPharmOrder, Long prevPharmOrderId){
		if( MedOrderDocType.Normal == medOrder.getDocType() ){
			return;
		}
		
		if( prevPharmOrderId != null ){
			return;
		}
		
		String medOrderNum = medOrder.getOrderNum();
		
		List<RefillSchedule> rsList = (List<RefillSchedule>) em.createQuery(
				"select o from RefillSchedule o" + // 20120307 index check : RefillSchedule.pharmOrder : FK_REFILL_SCHEDULE_01
				" where o.pharmOrder.id = :pharmOrderId")
				.setParameter("pharmOrderId", localPharmOrder.getId())
				.getResultList();
		
		Map<Integer,Integer> stdRefillNumOfGroup = new HashMap<Integer,Integer>();
		
		for( RefillSchedule localRefillSchedule : rsList ){
			 
			if( DocType.Standard.equals(localRefillSchedule.getDocType()) ){
				localRefillSchedule.setRefillCouponNum(RefillScheduleListManagerBean.generateRefillCouponNum(
														workstore.getHospCode(), 
														medOrderNum, 
														localRefillSchedule.getRefillNum(),
														localRefillSchedule.getGroupNum()));
				localRefillSchedule.setTrimRefillCouponNum(localRefillSchedule.getRefillCouponNum().substring(0, 14));
			
				if( !stdRefillNumOfGroup.containsKey(localRefillSchedule.getGroupNum()) ){
					stdRefillNumOfGroup.put(localRefillSchedule.getGroupNum(),localRefillSchedule.getGroupNum());
				}
				
			}else if(  DocType.Sfi.equals(localRefillSchedule.getDocType()) ){
			
				localRefillSchedule.setRefillCouponNum(SfiRefillScheduleListManagerBean.generateSfiRefillCouponNum(
														medOrder.getPatHospCode(),
														workstore.getHospCode(),
														medOrderNum, 
														localRefillSchedule.getRefillNum(),
														localRefillSchedule.getGroupNum()));
				
				localRefillSchedule.setTrimRefillCouponNum(localRefillSchedule.getRefillCouponNum().substring(0, 15));
			}
		}
		//numOfCoupon For standard refill
		localPharmOrder.setNumOfGroup(stdRefillNumOfGroup.size());
		em.flush();
	}

	public void updatePharmDispOrderItemDeltaChangeType(Map<Long, DeltaChangeType> deltaChangeTypeMap, Long dispOrderId) {
		DispOrder localDispOrder = em.find(DispOrder.class, dispOrderId);
		updatePharmDispOrderItemDeltaChangeType(deltaChangeTypeMap, localDispOrder);
	}
	
	public void updatePharmDispOrderItemDeltaChangeType(Map<Long, DeltaChangeType> deltaChangeTypeMap, DispOrder dispOrder) {
		if (deltaChangeTypeMap == null || deltaChangeTypeMap.isEmpty()) {
			return;
		}
		
		for (DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList()) {
			if (deltaChangeTypeMap.containsKey(dispOrderItem.getId())) {
				DeltaChangeType corpDeltaChangeType = deltaChangeTypeMap.get(dispOrderItem.getId());
				String corpDeltaChangeTypeStr = null;
				if (corpDeltaChangeType != null) {
					corpDeltaChangeTypeStr = corpDeltaChangeType.getDataValue();
				}
				
				String localDeltaChangeTypeStr = ((DispLabel)dispOrderItem.getBaseLabel()).getDeltaChangeType();
				
				if ( dispOrderItem.getBaseLabel() != null && ! StringUtils.equals(localDeltaChangeTypeStr, corpDeltaChangeTypeStr) ) {
					// note: to prevent dummy marshaling step for performance, skip update if CORP value has no change
					dispOrderItem.setDeltaChangeType(corpDeltaChangeTypeStr);
				}
			}
		}
	}

	private DispOrder saveDispOrder(PharmOrder pharmOrder, List<RefillSchedule> refillScheduleList, DispOrder prevDispOrder, String suspendCode) {
		
		DispOrder dispOrder = null;
		
		Ticket ticket = em.find(Ticket.class, pharmOrder.getMedOrder().getTicket().getId());

		// now if we still have refill schedule
		if (!refillScheduleList.isEmpty()) 
		{				
			// create the DispOrder from RefillSchedule;
			dispOrder = dispOrderManager.createDispOrderForInitialRefill(pharmOrder, refillScheduleList, ticket);			
		} 
		else 
		{
			// create the DispOrder from PharmOrder;
			dispOrder = dispOrderManager.createDispOrderFromPharmOrder(pharmOrder, ticket);
		}
		
		// suspend order
		if (suspendCode != null) {
			dispOrder.setAdminStatus(DispOrderAdminStatus.Suspended);
			dispOrder.setSuspendCode(suspendCode);
		}
		
		if (prevDispOrder != null) {
			dispOrderManager.updateDispOrderByPrevDispOrder(dispOrder, prevDispOrder);
		}
		
		em.persist(dispOrder);
		em.flush();
		
		if (prevDispOrder == null) {
			dispOrder.setOrgDispOrderId(dispOrder.getId());
		}
		
		em.flush();

		return dispOrder;
	}
	
	private DispOrder saveDispOrderForSfiRefillList(List<RefillSchedule> inRefillScheduleList, Ticket ticket, DispOrder prevDispOrder) {
		refillScheduleUtils.assignItemNumForRefillSave(inRefillScheduleList, prevDispOrder);
		
		DispOrder dispOrder = dispOrderManager.createDispOrderForSfiRefill(inRefillScheduleList, ticket);
		
		dispOrder.replace(inRefillScheduleList.get(0).getPharmOrder());
				
		if (prevDispOrder != null) {				
			dispOrderManager.updateDispOrderByPrevDispOrder(dispOrder, prevDispOrder);
		}

		em.persist(dispOrder);
		em.flush();
		
		if (prevDispOrder == null) {
			dispOrder.setOrgDispOrderId(dispOrder.getId());
		}

		for(RefillSchedule refillSchedule : inRefillScheduleList){
			logger.debug("saveDispOrderForSfiRefillList inRefillScheduleList refillSchedule #0", refillSchedule.getRefillStatus());
			refillSchedule.connect(dispOrder);
		}
		
		em.flush();

		return dispOrder;
	}

	private List<RefillSchedule> updateRefillScheduleListForSfi(PharmOrder pharmOrder){		
		List<RefillSchedule> sfiRsList = pharmOrder.getRefillScheduleList(DocType.Sfi);
		for( RefillSchedule sfiRs : sfiRsList ){
			for( RefillScheduleItem sfiRsi : sfiRs.getRefillScheduleItemList() ){
				if( sfiRsi.getPharmOrderItem() != null ){
					PharmOrderItem poi = sfiRsi.getPharmOrderItem();
					sfiRsi.setRefillQty( poi.getSfiQty() );
					sfiRsi.setHandleExemptCode( poi.getHandleExemptCode());
					sfiRsi.setNextHandleExemptCode( poi.getNextHandleExemptCode());
					sfiRsi.setHandleExemptUser( poi.getHandleExemptUser());
				}
			}
		}
		logger.debug("updateRefillScheduleListForSfi rsList Size : #0", sfiRsList.size());
		
		return sfiRsList;	
	}
	
	private void saveRefillSchedule(OrderViewInfo orderViewInfo, List<RefillSchedule> refillScheduleList, DispOrder dispOrder, Long prevPharmOrderId) {
		Map<Integer, Boolean> standardRsReDispFlagMap = refillScheduleUtils.retrieveRefillScheduleReDispFlagMap(prevPharmOrderId, DocType.Standard);
		
		for (RefillSchedule refillSchedule : refillScheduleList) 
		{
			refillSchedule.clearId();
			
			// connect to PharmOrder/DispOrder for save
			if (RefillScheduleRefillStatus.Current.equals(refillSchedule.getRefillStatus())) {
				if (dispOrder.getStatus() == DispOrderStatus.Issued) {
					refillSchedule.setStatus(RefillScheduleStatus.Dispensed);
				} else {
					refillSchedule.setStatus(RefillScheduleStatus.BeingProcessed);
				}
				refillSchedule.connect(dispOrder);
				refillSchedule.setPrintFlag(true);
				refillSchedule.setPrintLang(orderViewInfo.getPrintLang());
				
				for ( RefillScheduleItem refillScheduleItem : refillSchedule.getRefillScheduleItemList() ){					
					DmDrug dmDrug = DmDrugCacher.instance().getDmDrug(refillScheduleItem.getPharmOrderItem().getItemCode());
					if( dmDrug == null ){
						dmDrug = dmDrugCacher.getDmDrug(refillScheduleItem.getPharmOrderItem().getItemCode());
					}
					
					if( dmDrug.getDmProcureSummary() != null ){
						refillScheduleItem.setUnitPrice(BigDecimal.valueOf(dmDrug.getDmProcureSummary().getAverageUnitPrice()));
					}
				}

			} else {
				refillSchedule.connect(dispOrder.getPharmOrder());
				refillSchedule.setPrintLang(orderViewInfo.getPrintLang());				
			}
		}
		
		for(RefillSchedule refillSchedule : refillScheduleList){
			if( RefillScheduleRefillStatus.Next.equals(refillSchedule.getRefillStatus()) ){
				refillSchedule.setNumOfCoupon(orderViewInfo.getNumOfCoupon());
				if( !refillSchedule.getPrintFlag() && standardRsReDispFlagMap.containsKey(refillSchedule.getGroupNum()) ){
					refillSchedule.setReDispFlag(Boolean.TRUE);
				}
			}else if( RefillScheduleRefillStatus.Current.equals(refillSchedule.getRefillStatus()) ){
				refillSchedule.setTicket(dispOrder.getTicket());
			}
			
			for( RefillScheduleItem refillScheduleItem : refillSchedule.getRefillScheduleItemList() ){
				DateMidnight refillEndDate = new DateMidnight(refillSchedule.getBeforeAdjRefillDate());
				refillEndDate = refillEndDate.plusDays(refillScheduleItem.getRefillDuration());
				refillScheduleItem.setRefillEndDate(refillEndDate.toDate());
			}
			
			em.persist(refillSchedule);
		}

		em.flush();
		
		logger.info("Total #0 of Standard Refill Schedule(s) inserted", refillScheduleList.size()); 
	}
	
	private List<RefillSchedule> saveSfiRefillSchedule(OrderViewInfo orderViewInfo, DispOrder dispOrder, Map<Integer, Boolean> sfiRsReDispFlagMap, DispOrder prevDispOrder)
	{
		Boolean haveSfiItem = false;
	
		if( dispOrder.getSfiFlag() || dispOrder.getRefillFlag() ){
			haveSfiItem = true;
		}
		
		if( !haveSfiItem ){
			for( DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList() ){
				if( dispOrderItem.getPharmOrderItem().getMedOrderItem().isSfi() ){
					haveSfiItem = true;
					break;
				}
			}
		}

		if( !haveSfiItem ){
			return new ArrayList<RefillSchedule>();
		}
		
		List<RefillSchedule> sfiRefillScheduleList = sfiRefillScheduleListManager.createSfiRefillScheduleList(orderViewInfo, dispOrder, prevDispOrder);
		
		for (RefillSchedule rs : sfiRefillScheduleList) {
			if( !rs.getPrintFlag() && sfiRsReDispFlagMap.containsKey(rs.getGroupNum()) ){
				rs.setReDispFlag(Boolean.TRUE);
			}
			em.persist(rs);
		}
		em.flush();
		
		logger.info("Total #0 of SFI Refill Schedule(s) inserted", sfiRefillScheduleList.size()); 
				
		return sfiRefillScheduleList;
	}
	
	@SuppressWarnings("unchecked")
	private List<CapdVoucher> saveCapdVoucher(OrderViewInfo orderViewInfo, DispOrder dispOrder, List<CapdVoucher> cvList) {

		List<CapdVoucher> newVoucherList = new ArrayList<CapdVoucher>();

		dispOrder.getPharmOrder().loadCapdSplit();
		CapdSplit capdSplit = dispOrder.getPharmOrder().getCapdSplit();
		
		if (VETTING_CAPD_ENABLED.get()) {
			
			List<CapdVoucher> voucherList = capdVoucherManager.createCapdVoucherList(dispOrder, cvList);
			
			int keepCount = 0;
			int createCount = 0;
			int deleteCount = 0;
			List<String> newVoucherNumList = new ArrayList<String>();

			for (CapdVoucher capdVoucher : voucherList) {
				if (capdVoucher.getId() == null) {
					newVoucherList.add(capdVoucher);
					newVoucherNumList.add(capdVoucher.getVoucherNum());
					createCount++;
				} else {
					if (capdVoucher.getStatus() == CapdVoucherStatus.Deleted) {
						deleteCount++;
					} else {
						keepCount++;
						if ( capdVoucher.getStatus() == CapdVoucherStatus.Suspended ) {
							newVoucherList.add(capdVoucher);
						}
					}
					capdVoucher.clearId();
				}
				em.persist(capdVoucher);
			}
			
			Collections.sort(newVoucherNumList, new Comparator(){
				public int compare(Object obj1, Object obj2) {
					String cv1 = (String) obj1;
					String cv2 = (String) obj2;
					return cv1.compareTo(cv2);
				}
			});	
			
			// full list of new Issued voucherNum
			if (!newVoucherNumList.isEmpty()) {
				StringBuilder sb = new StringBuilder();
				for (String voucherNum : newVoucherNumList) {
					if (sb.length() > 0) {
						sb.append(',');
						sb.append(' ');
					}
					sb.append(voucherNum);
				}
				orderViewInfo.addInfoMsg("0173", new String[] {sb.toString()});
			}
			em.flush();

			logger.info("Total #0/#1/#2 of CAPD Voucher keep/create/delete", keepCount, createCount, deleteCount);

			// strange behavior cannot update the CapdSplit, clear the first level and reload the pharmOrder
			em.clear();			
			if (capdSplit != null) {
				PharmOrder tempPo = em.find(PharmOrder.class, dispOrder.getPharmOrder().getId());
				tempPo.setCapdSplit(capdSplit);
				tempPo.preSave();
				em.flush();
			}
		}

		return newVoucherList;
	}

	private SaveInvoiceListInfo saveInvoice(OrderViewInfo orderViewInfo, DispOrder dispOrder, List<Invoice> voidInvoiceList) {

		SaveInvoiceListInfo saveInvoiceListInfo = invoiceManager.createInvoiceListFromDispOrder(orderViewInfo, dispOrder, voidInvoiceList);
		if (saveInvoiceListInfo.getAlertMsg() != null) {
			orderViewInfo.addInfoMsg(saveInvoiceListInfo.getAlertMsg());
		}

		logger.debug("saveInvoiceListInfo.isSuspendOrder() : #0, saveInvoiceListInfo.getSuspendCode() : #1, saveInvoiceListInfo.getAlertMsg() : #2",
				saveInvoiceListInfo.isSuspendOrder(),
				saveInvoiceListInfo.getSuspendCode(),
				saveInvoiceListInfo.getAlertMsg());

		if (saveInvoiceListInfo.isSuspendOrder()) {
			dispOrder.setAdminStatus(DispOrderAdminStatus.Suspended);
			dispOrder.setSuspendCode(saveInvoiceListInfo.getSuspendCode());
		}
		
		em.flush();
		
		for (Invoice invoice : saveInvoiceListInfo.getInvoiceList()) {
			logger.info("Invoice #0 with amount #1 inserted", invoice.getInvoiceNum(), invoice.getTotalAmount());
		}

		return saveInvoiceListInfo;
	}
	
	private void updateMedOrderStatus(MedOrder localMedOrder, MedOrder remoteMedOrder) {

		Map<Long, MedOrderItemStatus> statusMap = new HashMap<Long, MedOrderItemStatus>();
		for (MedOrderItem medOrderItem : remoteMedOrder.getMedOrderItemList()) {
			statusMap.put(medOrderItem.getId(), medOrderItem.getStatus());
		}

		localMedOrder.setStatus(remoteMedOrder.getStatus());
		for (MedOrderItem medOrderItem : localMedOrder.getMedOrderItemList()) {
			medOrderItem.setStatus(statusMap.get(medOrderItem.getId()));
		}
	}
	
	private void setMedOrder(MedOrder medOrder) {
		Contexts.getSessionContext().set("medOrder", medOrder);
	}
	
}
