package hk.org.ha.model.pms.udt.report;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum RefillItemRptStatus implements StringValuedEnum {

	All("A", "All"),
	BeingProcessed("B", "Being Processed"),
	Dispensed("C", "Completed"),
	Delete("D", "Deleted"),
	ForceComplete("E", "Force Completed"),
	NotYetDispensed("F", "Not Yet Dispensed"),
	Suspended("G", "Suspended");

    private final String dataValue;
    private final String displayValue;

    RefillItemRptStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static RefillItemRptStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(RefillItemRptStatus.class, dataValue);
	}
        
	public static class Converter extends StringValuedEnumConverter<RefillItemRptStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<RefillItemRptStatus> getEnumClass() {
    		return RefillItemRptStatus.class;
    	}
    }
}
