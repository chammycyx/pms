package hk.org.ha.model.pms.vo.vetting;

import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.vo.drug.DmWarningLite;

import java.io.Serializable;
import java.math.BigDecimal;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PharmOrderItemInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private BigDecimal issueQty;

	private Integer sfiQty;
	
	private String handleExemptCode;
	
	private String nextHandleExemptCode;
	
	private String handleExemptUser;
	
	private String warnCode1;

	private String warnCode2;
	
	private String warnCode3;
	
	private String warnCode4;
	
	private Integer itemNum;
	
	private Boolean dirtyFlag;
	
	private Boolean stdRefillCheckFlag;
	
	private Long refillScheduleItemId;

	private Boolean sfiOverrideWarnFlag;
	
	public void setPharmOrderItem(PharmOrderItem pharmOrderItem) {

		DmWarningLite dmWarningLite1 = null, dmWarningLite2 = null, dmWarningLite3 = null, dmWarningLite4 = null;
		if (warnCode1 != null) {
			dmWarningLite1 = DmWarningLite.objValueOf(warnCode1);
		}
		if (warnCode2 != null) {
			dmWarningLite2 = DmWarningLite.objValueOf(warnCode2);
		}
		if (warnCode3 != null) {
			dmWarningLite3 = DmWarningLite.objValueOf(warnCode3);
		}
		if (warnCode4 != null) {
			dmWarningLite4 = DmWarningLite.objValueOf(warnCode4);
		}

		pharmOrderItem.setIssueQty(issueQty);
		pharmOrderItem.setSfiQty(sfiQty);
		pharmOrderItem.setWarnCode1(warnCode1);
		pharmOrderItem.setWarnCode2(warnCode2);
		pharmOrderItem.setWarnCode3(warnCode3);
		pharmOrderItem.setWarnCode4(warnCode4);
		pharmOrderItem.setDmWarningLite1(dmWarningLite1);
		pharmOrderItem.setDmWarningLite2(dmWarningLite2);
		pharmOrderItem.setDmWarningLite3(dmWarningLite3);
		pharmOrderItem.setDmWarningLite4(dmWarningLite4);
		pharmOrderItem.setItemNum(itemNum);

		pharmOrderItem.setHandleExemptCode(handleExemptCode);
		pharmOrderItem.setNextHandleExemptCode(nextHandleExemptCode);
		pharmOrderItem.setHandleExemptUser(handleExemptUser);
		
		pharmOrderItem.setDirtyFlag(dirtyFlag);
		pharmOrderItem.setStdRefillCheckFlag(stdRefillCheckFlag);
		
		pharmOrderItem.setSfiOverrideWarnFlag(sfiOverrideWarnFlag);
	}
	
	public BigDecimal getIssueQty() {
		return issueQty;
	}

	public void setIssueQty(BigDecimal issueQty) {
		this.issueQty = issueQty;
	}

	public Integer getSfiQty() {
		return sfiQty;
	}

	public void setSfiQty(Integer sfiQty) {
		this.sfiQty = sfiQty;
	}

	public String getHandleExemptCode() {
		return handleExemptCode;
	}

	public void setHandleExemptCode(String handleExemptCode) {
		this.handleExemptCode = handleExemptCode;
	}

	public String getNextHandleExemptCode() {
		return nextHandleExemptCode;
	}

	public void setNextHandleExemptCode(String nextHandleExemptCode) {
		this.nextHandleExemptCode = nextHandleExemptCode;
	}

	public String getHandleExemptUser() {
		return handleExemptUser;
	}

	public void setHandleExemptUser(String handleExemptUser) {
		this.handleExemptUser = handleExemptUser;
	}
	
	public String getWarnCode1() {
		return warnCode1;
	}

	public void setWarnCode1(String warnCode1) {
		this.warnCode1 = warnCode1;
	}

	public String getWarnCode2() {
		return warnCode2;
	}

	public void setWarnCode2(String warnCode2) {
		this.warnCode2 = warnCode2;
	}

	public String getWarnCode3() {
		return warnCode3;
	}

	public void setWarnCode3(String warnCode3) {
		this.warnCode3 = warnCode3;
	}

	public String getWarnCode4() {
		return warnCode4;
	}

	public void setWarnCode4(String warnCode4) {
		this.warnCode4 = warnCode4;
	}

	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public Boolean getDirtyFlag() {
		return dirtyFlag;
	}

	public void setDirtyFlag(Boolean dirtyFlag) {
		this.dirtyFlag = dirtyFlag;
	}
	
	public Long getRefillScheduleItemId() {
		return refillScheduleItemId;
	}
	
	public void setRefillScheduleItemId(Long refillScheduleItemId) {
		this.refillScheduleItemId = refillScheduleItemId;
	}

	public Boolean getStdRefillCheckFlag() {
		return stdRefillCheckFlag;
	}

	public void setStdRefillCheckFlag(Boolean stdRefillCheckFlag) {
		this.stdRefillCheckFlag = stdRefillCheckFlag;
	}

	public Boolean getSfiOverrideWarnFlag() {
		return sfiOverrideWarnFlag;
	}

	public void setSfiOverrideWarnFlag(Boolean sfiOverrideWarnFlag) {
		this.sfiOverrideWarnFlag = sfiOverrideWarnFlag;
	}
}
