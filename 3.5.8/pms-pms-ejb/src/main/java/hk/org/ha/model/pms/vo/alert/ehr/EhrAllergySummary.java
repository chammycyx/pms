package hk.org.ha.model.pms.vo.alert.ehr;

import hk.org.ha.model.pms.udt.alert.ehr.EhrAlertStatus;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class EhrAllergySummary implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement
	private String ehrNum;
	
	@XmlElement
	private String allergy;
	
	@XmlElement
	private String adr;

	@XmlElement
	private EhrAlertStatus status;

	public EhrAllergySummary() {
	}

	public EhrAllergySummary(EhrAlertStatus status) {
		this.status = status;
	}
	
	public EhrAllergySummary(EhrAlertStatus status, String ehrNum, String allergy, String adr) {
		this(status);
		this.ehrNum = ehrNum;
		this.allergy = allergy;
		this.adr = adr;
	}

	public String getEhrNum() {
		return ehrNum;
	}

	public void setEhrNum(String ehrNum) {
		this.ehrNum = ehrNum;
	}

	public String getAllergy() {
		return allergy;
	}

	public void setAllergy(String allergy) {
		this.allergy = allergy;
	}

	public String getAdr() {
		return adr;
	}

	public void setAdr(String adr) {
		this.adr = adr;
	}

	public EhrAlertStatus getStatus() {
		return status;
	}

	public void setStatus(EhrAlertStatus status) {
		this.status = status;
	}
}
