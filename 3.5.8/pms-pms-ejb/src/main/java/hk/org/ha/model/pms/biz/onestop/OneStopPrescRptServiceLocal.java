package hk.org.ha.model.pms.biz.onestop;

import hk.org.ha.model.pms.udt.onestop.MoeFilterOption;
import java.util.Date;

import javax.ejb.Local;

@Local
public interface OneStopPrescRptServiceLocal {
	
	void printOSEPrescRptData(Date orderDate, MoeFilterOption moeFilterOption);
	
	void destroy();
}
