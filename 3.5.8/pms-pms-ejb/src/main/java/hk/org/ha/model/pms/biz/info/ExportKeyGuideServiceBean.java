package hk.org.ha.model.pms.biz.info;

import java.util.List;
import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.vo.info.KeyGuide;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderJob;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("exportKeyGuideService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ExportKeyGuideServiceBean implements ExportKeyGuideServiceLocal {
	
	@In
	private PrintAgentInf printAgent;
	
	private List<KeyGuide> exportKeyGuideList;
	
	public void exportKeyGuide(List<KeyGuide> exportKeyList) {
		exportKeyGuideList = exportKeyList;
	}
	
	public void generateKeyGuide() {
		PrintOption po = new PrintOption();
		po.setDocType(ReportProvider.XLS);
		printAgent.renderAndRedirect(new RenderJob(
		    "ShortcutKeyGuideXls",
		    po,
		    null,
		    exportKeyGuideList));
	}
	
	@Remove
	public void destroy() {
		
	}
}
