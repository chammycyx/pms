package hk.org.ha.model.pms.biz.pivas;

import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.reftable.pivas.PivasServiceHour;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Local;

@Local
public interface PivasServiceHourManagerLocal {
	
	Map<String, List<PivasServiceHour>> retrievePivasServiceHourListByDayOfWeekMap(String workstoreGroupCode, Hospital hospital);
		
	Set<Date> retrievePivasHolidayDateSet(String workstoreGroupCode);
}
