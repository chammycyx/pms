package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.udt.report.RefillItemRptStatus;
import hk.org.ha.model.pms.udt.report.RefillReportType;
import hk.org.ha.model.pms.vo.report.PharmClinicRefillCouponRpt;
import hk.org.ha.model.pms.vo.report.RefillExpiryRpt;
import hk.org.ha.model.pms.vo.report.RefillItemRpt;
import hk.org.ha.model.pms.vo.report.RefillQtyForecastRpt;
import hk.org.ha.model.pms.vo.report.RefillStatRpt;
import hk.org.ha.model.pms.vo.report.RefillWorkloadForecastRpt;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import net.lingala.zip4j.exception.ZipException;

@Local
public interface RefillRptServiceLocal {
	
	void retrieveRefillRpt(RefillReportType reportType, String workstoreCode, Date refillDueDateFrom, Date refillDueDateTo, String itemCode, RefillItemRptStatus status, Boolean drsPatientFlag);

	List<RefillItemRpt> retrieveRefillItemRptList(String workstoreCode, Date dateFrom, Date dateTo, String itemCode, RefillItemRptStatus status, Boolean drsPatientFlag);
	
	List<RefillWorkloadForecastRpt> retrieveRefillWorkloadRptList(String workstoreCode, Date dateFrom, Date dateTo);
	
	List<RefillQtyForecastRpt> retrieveRefillQtyForecastRptList(String workstoreCode, Date dateFrom, Date dateTo, String itemCode);
	
	List<RefillStatRpt> retrieveRefillStatRptList(String workstoreCode, Date dateFrom, Date dateTo);
	
	List<RefillExpiryRpt> retrieveRefillExpiryRpt(String workstoreCode, Date dateFrom, Date dateTo);
	
	List<PharmClinicRefillCouponRpt> retrievePharmClinicRefillCouponRpt(String workstoreCode, Date dateFrom, Date dateTo);

	void generateRefillRpt();
	
	void setRefillRptPassword(String password);
	
	void exportRefillRpt() throws IOException, ZipException;
	
	void printRefillRpt();
	
	void destroy();
	
}
