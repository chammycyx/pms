package hk.org.ha.model.pms.vo.info;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class KeyGuide implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String screenFunction;

	private String actionDesc;
	
	private String actionShortcutKey;
	
	public void setScreenFunction(String screenFunction){
		this.screenFunction = screenFunction;
	}
	
	public String getScreenFunction() {
		return screenFunction;
	}

	public void setActionDesc(String actionDesc) {
		this.actionDesc = actionDesc;
	}

	public String getActionDesc() {
		return actionDesc;
	}

	public void setActionShortcutKey(String actionShortcutKey) {
		this.actionShortcutKey = actionShortcutKey;
	}

	public String getActionShortcutKey() {
		return actionShortcutKey;
	}

}