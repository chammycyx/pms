package hk.org.ha.model.pms.biz.report;

import hk.org.ha.fmk.pms.util.QueryUtils;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.biz.reftable.WardConfigManagerLocal;
import hk.org.ha.model.pms.biz.reftable.WardGroupManagerLocal;
import hk.org.ha.model.pms.biz.reftable.WardManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.reftable.WardConfig;
import hk.org.ha.model.pms.persistence.reftable.WardGroup;
import hk.org.ha.model.pms.persistence.reftable.WardGroupItem;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatus;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.PatientListRpt;
import hk.org.ha.model.pms.vo.report.PatientListRptDtl;
import hk.org.ha.model.pms.vo.report.PatientListRptInfo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
@Stateful
@Scope(ScopeType.SESSION)
@Name("patientListRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PatientListRptServiceBean implements PatientListRptServiceLocal {
	@PersistenceContext
	private EntityManager em;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent;
	
	@In 
	private Workstore workstore;
	
	@In
	private WardManagerLocal wardManager;
	
	@In
	private WardConfigManagerLocal wardConfigManager;
	
	@In
	private WardGroupManagerLocal wardGroupManager;

	private List<PatientListRpt> patientListRptList;
	
	private boolean retrieveSuccess;
	
    private Map<String, Object> parameters = new HashMap<String, Object>();
    
	private static PatientListComparator patientListComparator = new PatientListComparator();
	private static PatientListDtlComparator patientListDtlComparator = new PatientListDtlComparator();
    
    public PatientListRptInfo retrievePatientListRptInfo(){
    	PatientListRptInfo patientListInfo = new PatientListRptInfo();
    	patientListInfo.setWardCodeList(retrieveWardCodeList());
    	List<String> mpWardCodeList = retrieveMpWardCodeList(patientListInfo.getWardCodeList());
    	patientListInfo.setMpWardCodeList(mpWardCodeList);
    	patientListInfo.setWardGroupList(retrieveWardGroupList(mpWardCodeList));
    	
    	return patientListInfo;
    }
    
    private List<String> retrieveWardCodeList(){
    	return wardManager.retrieveWardCodeList();
    }
    
    private List<String> retrieveMpWardCodeList(List<String> wardCodeList){
    	List<String> mpWardCodeList = new ArrayList<String>();
    	List<WardConfig> wardConfigList = wardConfigManager.retrieveMpWardConfigList(workstore.getWorkstoreGroup());
    	
		for ( WardConfig wardConfig : wardConfigList ) {
			if ( wardCodeList.contains(wardConfig.getWardCode()) ) {
				mpWardCodeList.add(wardConfig.getWardCode());
			}
		}
    	
    	return mpWardCodeList;
    }
    
    private List<WardGroup> retrieveWardGroupList(List<String> mpWardCodeList){
    	List<WardGroup> wardGroupList = wardGroupManager.retrieveWardGroupList(workstore.getWorkstoreGroup());
    	em.clear();
    	
    	List<WardGroup> mpWardGroupList = new ArrayList<WardGroup>();
    	
		for (WardGroup wardGroup : wardGroupList ) {
			if ( wardGroup.getWardGroupItemList().isEmpty() ) {
				continue;
			}
			List<WardGroupItem> wardGroupItemList = new ArrayList<WardGroupItem>(wardGroup.getWardGroupItemList());
			
			for ( WardGroupItem wardGroupItem : wardGroup.getWardGroupItemList() ) {
				if ( !mpWardCodeList.contains(wardGroupItem.getWardCode()) ) {
					wardGroupItemList.remove(wardGroupItem);
				}
			}
			wardGroup.setWardGroupItemList(wardGroupItemList);
			mpWardGroupList.add(wardGroup);
		}
    	
    	return mpWardGroupList;
    }
	
	@SuppressWarnings("unchecked")
	public List<PatientListRpt>  retrievePatientList(List<String> wardCodeList) {
		retrieveSuccess = false;
		patientListRptList = new ArrayList<PatientListRpt>();
		List<MedProfile> medProfileList = QueryUtils.splitExecute(em.createQuery(
				"select o from MedProfile o" + // 20160809 index check : MedProfile.hospCode,wardCode,status : I_MED_PROFILE_06
				" where o.wardCode in :wardCodeList" +
				" and o.status in :notInactiveStatus" +
				" and o.hospCode = :hospCode" +
				" order by o.wardCode")
				.setParameter("notInactiveStatus", MedProfileStatus.Not_Inactive)
				.setParameter("hospCode", workstore.getHospCode()), 
				"wardCodeList", wardCodeList
				);
		
		if ( medProfileList.isEmpty() ) {
			retrieveSuccess = false;
			return patientListRptList;
		}
		
		Map<String, List<PatientListRptDtl>> wardCodeMap = new HashMap<String, List<PatientListRptDtl>>();
		for ( MedProfile medProfile : medProfileList ) {
			if ( !wardCodeMap.containsKey(medProfile.getWardCode()) ) {
				wardCodeMap.put(medProfile.getWardCode(), new ArrayList<PatientListRptDtl>());
			}
			PatientListRptDtl patientListDtl = new PatientListRptDtl();
			patientListDtl.setWardCode(StringUtils.trimToEmpty(medProfile.getWardCode()));
			patientListDtl.setSpecCode(medProfile.getSpecCode());
			patientListDtl.setPatCatCode(medProfile.getPatCatCode());
			patientListDtl.setAdmissionDate(medProfile.getAdmissionDate());
			
			Patient patient = medProfile.getPatient();
			patientListDtl.setPatName(patient.getName());
			patientListDtl.setPatNameChi(patient.getNameChi());
			patientListDtl.setHkid(patient.getHkid());
			patientListDtl.setDob(patient.getDob());
			if ( patient.getSex() != null ) {
				patientListDtl.setSex(patient.getSex().getDisplayValue());
			}
			
			if ( medProfile.getMedCase() != null ) {
				MedCase medCase = medProfile.getMedCase();
				patientListDtl.setBedNum(StringUtils.trimToEmpty(medCase.getPasBedNum()));
				patientListDtl.setCaseNum(medCase.getCaseNum());
			}
			wardCodeMap.get(medProfile.getWardCode()).add(patientListDtl);
		}
		
		for ( Map.Entry<String, List<PatientListRptDtl>> entry : wardCodeMap.entrySet() )  {
			String wardCode = entry.getKey();
			List<PatientListRptDtl> patientListDtlList = entry.getValue();
			PatientListRpt patientList = new PatientListRpt();
			patientList.setWardCode(wardCode);
			patientList.setPatientListDtlList(patientListDtlList);
			
			Collections.sort(patientList.getPatientListDtlList(), patientListDtlComparator);
			patientListRptList.add(patientList);
		}
		
		Collections.sort(patientListRptList, patientListComparator);
		retrieveSuccess = true;
		return patientListRptList;
	}
	
	public void generatePatientListRpt() {
		parameters.put("hospCode", workstore.getHospCode());
		printAgent.renderAndRedirect(new RenderJob(
				"PatientListRpt", 
				new PrintOption(), 
				parameters, 
				patientListRptList));
	}

	public void printPatientListRpt(){
		printAgent.renderAndPrint(new RenderAndPrintJob(
				"PatientListRpt", 
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				parameters, 
				patientListRptList));
	}
	
	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}
	   
	private static class PatientListComparator implements Comparator<PatientListRpt>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(PatientListRpt patientList1, PatientListRpt patientList2) {
			String wardCode1 = StringUtils.trimToEmpty(patientList1.getWardCode());
			String wardCode2 = StringUtils.trimToEmpty(patientList2.getWardCode());

			return wardCode1.compareTo(wardCode2);
		}
    }
	
   private static class PatientListDtlComparator implements Comparator<PatientListRptDtl>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(PatientListRptDtl patientListDtl1, PatientListRptDtl patientListDtl2) {
			int compareResult = 0;
			String bedNum1 = StringUtils.trimToEmpty(patientListDtl1.getBedNum());
			String bedNum2 = StringUtils.trimToEmpty(patientListDtl2.getBedNum());
			compareResult = bedNum1.compareTo(bedNum2);

			if (compareResult == 0) {
				String caseNum1 = StringUtils.trimToEmpty(patientListDtl1.getCaseNum());
				String caseNum2 = StringUtils.trimToEmpty(patientListDtl2.getCaseNum());
				compareResult = caseNum1.compareTo(caseNum2);
			}
			
			if (compareResult == 0) {
				String hkid1 = StringUtils.trimToEmpty(patientListDtl1.getHkid());
				String hkid2 = StringUtils.trimToEmpty(patientListDtl2.getHkid());
				compareResult = hkid1.compareTo(hkid2);
			}

			return compareResult;
		}
    }
	
	@Remove
	public void destroy() {
		if ( patientListRptList != null ) {
			patientListRptList = null;
		}
	}

}
