package hk.org.ha.model.pms.vo.disp;

import java.io.Serializable;

public class DayEndDispOrder implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String clusterCode;
	
	private Long dispOrderId;

	private String orderNum;
	
	public DayEndDispOrder(String clusterCode, Long dispOrderId) {
		this.clusterCode = clusterCode;
		this.dispOrderId = dispOrderId;
	}

	public DayEndDispOrder(String clusterCode, Long dispOrderId, String orderNum) {
		this.clusterCode = clusterCode;
		this.dispOrderId = dispOrderId;
		this.orderNum = orderNum;
	}
	
	public String getClusterCode() {
		return clusterCode;
	}

	public void setClusterCode(String clusterCode) {
		this.clusterCode = clusterCode;
	}
	
	public Long getDispOrderId() {
		return dispOrderId;
	}
	
	public void setDispOrderId(Long dispOrderId) {
		this.dispOrderId = dispOrderId;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}	
}