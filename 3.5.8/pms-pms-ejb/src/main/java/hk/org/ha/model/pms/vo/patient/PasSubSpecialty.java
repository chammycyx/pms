package hk.org.ha.model.pms.vo.patient;

import java.io.Serializable;

public class PasSubSpecialty implements Serializable {

	private static final long serialVersionUID = 1L;

	private String pasSubSpecCode;
	
	private String pasSubDescription;
	
	private Boolean allowPrescribe;
	
	public Boolean getAllowPrescribe() {
		return allowPrescribe;
	}

	public void setAllowPrescribe(Boolean allowPrescribe) {
		this.allowPrescribe = allowPrescribe;
	}

	public PasSubSpecialty() {
	}

	public String getPasSubSpecCode() {
		return pasSubSpecCode;
	}

	public void setPasSubSpecCode(String pasSubSpecCode) {
		this.pasSubSpecCode = pasSubSpecCode;
	}

	public String getPasSubDescription() {
		return pasSubDescription;
	}

	public void setPasSubDescription(String pasSubDescription) {
		this.pasSubDescription = pasSubDescription;
	}
	
	@Override
	public String toString() {
		return "PasSubSpecialty [allowPrescribe=" + allowPrescribe
				+ ", pasSubSpecCode=" + pasSubSpecCode + "]";
	}
	
}

