package hk.org.ha.model.pms.udt.onestop;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum OneStopOrderType implements StringValuedEnum {
	
	ManualOrder("MO", "Manual Order"),
	MedOrder("CO", "Medication Order"),
	DispOrder("DO", "Dispensing Order"),
	RefillOrder("RO", "Refill Order"),
	RefillOrderReadOnly("RR", "Refill Order Read-only"),
	SfiOrder("SO", "SFI Order"),
	DhOrder("DH", "DH Order");
	
    private final String dataValue;
    private final String displayValue;
        
    OneStopOrderType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static OneStopOrderType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(OneStopOrderType.class, dataValue);
	}
        
	public static class Converter extends StringValuedEnumConverter<OneStopOrderType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<OneStopOrderType> getEnumClass() {
    		return OneStopOrderType.class;
    	}
    }
}
