package hk.org.ha.model.pms.vo.label;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "MpDispLabelQrCode")
@ExternalizedBean(type=DefaultExternalizer.class)
public class MpDispLabelQrCode implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public static final Integer NORMAL_ORDER = 1;
	
	public static final Integer CHEMO_ORDER = 2;

	@XmlAttribute(name="s", required = true)
	private Integer type;
	
	//True display name
	@XmlElement(name = "A", required = true)	
	private String a;
	
	//Salt
	@XmlElement(name = "B", required = true)	
	private String b;
	
	//Strength value and unit
	@XmlElement(name = "C", required = true)	
	private String c;
	
	//FormCode
	@XmlElement(name = "D", required = true)	
	private String d;
	
	//Dispensing transaction ID
	@XmlElement(name = "E", required = true)	
	private Long e;
	
	//Item code
	@XmlElement(name = "F", required = true)	
	private String f;
	
	//Ward code
	@XmlElement(name = "G", required = true)	
	private String g;
	
	//Batch number
	@XmlElement(name = "H", required = true)	
	private String h;
	
	//Expiry date
	@XmlElement(name = "I", required = true)	
	private String i;
	
	//Lot number
	@XmlElement(name = "J", required = true)	
	private String j;

	@XmlElement(name = "K")
	private String k;
	
	@XmlElement(name = "L")
	private String l;

	@XmlElement(name = "M")
	private String m;
	
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getA() {
		return a;
	}

	public void setA(String a) {
		this.a = a;
	}

	public String getB() {
		return b;
	}

	public void setB(String b) {
		this.b = b;
	}

	public String getC() {
		return c;
	}

	public void setC(String c) {
		this.c = c;
	}

	public String getD() {
		return d;
	}

	public void setD(String d) {
		this.d = d;
	}

	public Long getE() {
		return e;
	}

	public void setE(Long e) {
		this.e = e;
	}

	public String getF() {
		return f;
	}

	public void setF(String f) {
		this.f = f;
	}

	public String getG() {
		return g;
	}

	public void setG(String g) {
		this.g = g;
	}

	public String getH() {
		return h;
	}

	public void setH(String h) {
		this.h = h;
	}

	public String getI() {
		return i;
	}

	public void setI(String i) {
		this.i = i;
	}

	public String getJ() {
		return j;
	}

	public void setJ(String j) {
		this.j = j;
	}

	public String getK() {
		return k;
	}

	public void setK(String k) {
		this.k = k;
	}

	public String getL() {
		return l;
	}

	public void setL(String l) {
		this.l = l;
	}

	public String getM() {
		return m;
	}

	public void setM(String m) {
		this.m = m;
	}
}