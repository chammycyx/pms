package hk.org.ha.model.pms.persistence.reftable;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "ITEM_DISP_CONFIG")
@Customizer(AuditCustomizer.class)
public class ItemDispConfig extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "itemDispConfigSeq")
	@SequenceGenerator(name = "itemDispConfigSeq", sequenceName = "SQ_ITEM_DISP_CONFIG", initialValue = 100000000)
	private Long id;
	
	@Column(name = "ITEM_CODE", nullable = false,  length = 6)
	private String itemCode;
	
	@Column(name = "DAILY_REFILL_FLAG", nullable = false)
	private Boolean dailyRefillFlag;
	
	@Column(name = "SINGLE_DISP_FLAG", nullable = false)
	private Boolean singleDispFlag;
	
	@Column(name = "WARNING_MESSAGE")
	private String warningMessage;
	
	@ManyToOne
	@JoinColumn(name = "WORKSTORE_GROUP_CODE", nullable = false)
	private WorkstoreGroup workstoreGroup;
	
	@Transient
	private Integer defaultDispDays;
	@Transient
	private Boolean wardStockWithout2DCodeFlag;
	@Transient
	private Boolean ddWithout2DCodeFlag;
	
	//pms-4044
	@Transient
	private Integer reviewDay;
	@Transient
	private Boolean reviewRepeatFlag ;
	@Transient
	private String reviewMessage; 
	@Transient
	private String workstoreGroupCode;

	@Transient
	private Boolean enableReviewInfoFlag ;
	
	public ItemDispConfig() {
		dailyRefillFlag = Boolean.FALSE;
		singleDispFlag = Boolean.FALSE;
		defaultDispDays = Integer.valueOf(0);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public Integer getDefaultDispDays() {
		return defaultDispDays;
	}

	public void setDefaultDispDays(Integer defaultDispDays) {
		this.defaultDispDays = defaultDispDays;
	}

	public Boolean getSingleDispFlag() {
		return singleDispFlag;
	}

	public void setSingleDispFlag(Boolean singleDispFlag) {
		this.singleDispFlag = singleDispFlag;
	}

	public String getWarningMessage() {
		return warningMessage;
	}

	public void setWarningMessage(String warningMessage) {
		this.warningMessage = warningMessage;
	}

	public WorkstoreGroup getWorkstoreGroup() {
		return workstoreGroup;
	}

	public void setWorkstoreGroup(WorkstoreGroup workstoreGroup) {
		this.workstoreGroup = workstoreGroup;
	}

	public Boolean getDailyRefillFlag() {
		return dailyRefillFlag;
	}

	public void setDailyRefillFlag(Boolean dailyRefillFlag) {
		this.dailyRefillFlag = dailyRefillFlag;
	}

	public Boolean getWardStockWithout2DCodeFlag() {
		return wardStockWithout2DCodeFlag;
	}

	public void setWardStockWithout2DCodeFlag(Boolean wardStockWithout2DCodeFlag) {
		this.wardStockWithout2DCodeFlag = wardStockWithout2DCodeFlag;
	}

	public Boolean getDdWithout2DCodeFlag() {
		return ddWithout2DCodeFlag;
	}

	public void setDdWithout2DCodeFlag(Boolean ddWithout2DCodeFlag) {
		this.ddWithout2DCodeFlag = ddWithout2DCodeFlag;
	}

	public Integer getReviewDay() {
		return reviewDay;
	}

	public void setReviewDay(Integer reviewDay) {
		this.reviewDay = reviewDay;
	}

	public Boolean getReviewRepeatFlag() {
		return reviewRepeatFlag;
	}

	public void setReviewRepeatFlag(Boolean reviewRepeatFlag) {
		this.reviewRepeatFlag = reviewRepeatFlag;
	}

	public String getReviewMessage() {
		return reviewMessage;
	}

	public void setReviewMessage(String reviewMessage) {
		this.reviewMessage = reviewMessage;
	}

	public String getWorkstoreGroupCode() {
		return workstoreGroupCode;
	}

	public void setWorkstoreGroupCode(String workstoreGroupCode) {
		this.workstoreGroupCode = workstoreGroupCode;
	}

	public Boolean getEnableReviewInfoFlag() {
		return enableReviewInfoFlag;
	}

	public void setEnableReviewInfoFlag(Boolean enableReviewInfoFlag) {
		this.enableReviewInfoFlag = enableReviewInfoFlag;
	}

}
