package hk.org.ha.model.pms.vo.mr;

import hk.org.ha.model.pms.udt.mr.MrType;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class MedProfileMrItemDetail implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Boolean allowRepeatFlag;
	
	private String annotation;
	
	private String dcReason;
	
	private Date mrDate;
	
	private Date mrEndDate;
	
	private String mrHospCode;
	
	private Integer mrSeqNum;
	
	private MrType mrType;
	
	private String remark;
	
	private String userCode;
	
	private String userName;
	
    private String userRank;

	private MedProfileMrItem medProfileMrItem;
    
	public Boolean getAllowRepeatFlag() {
		return allowRepeatFlag;
	}

	public void setAllowRepeatFlag(Boolean allowRepeatFlag) {
		this.allowRepeatFlag = allowRepeatFlag;
	}

	public String getAnnotation() {
		return annotation;
	}

	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}

	public String getDcReason() {
		return dcReason;
	}

	public void setDcReason(String dcReason) {
		this.dcReason = dcReason;
	}

	public Date getMrDate() {
		return mrDate == null ? null : new Date(mrDate.getTime());
	}

	public void setMrDate(Date mrDate) {
		this.mrDate = mrDate == null ? null : new Date(mrDate.getTime());
	}

	public Date getMrEndDate() {
		return mrEndDate == null ? null : new Date(mrEndDate.getTime());
	}

	public void setMrEndDate(Date mrEndDate) {
		this.mrEndDate = mrEndDate == null ? null : new Date(mrEndDate.getTime());
	}

	public String getMrHospCode() {
		return mrHospCode;
	}

	public void setMrHospCode(String mrHospCode) {
		this.mrHospCode = mrHospCode;
	}

	public Integer getMrSeqNum() {
		return mrSeqNum;
	}

	public void setMrSeqNum(Integer mrSeqNum) {
		this.mrSeqNum = mrSeqNum;
	}

	public MrType getMrType() {
		return mrType;
	}

	public void setMrType(MrType mrType) {
		this.mrType = mrType;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserRank() {
		return userRank;
	}

	public void setUserRank(String userRank) {
		this.userRank = userRank;
	}
	
	public MedProfileMrItem getMedProfileMrItem() {
		return medProfileMrItem;
	}

	public void setMedProfileMrItem(MedProfileMrItem medProfileMrItem) {
		this.medProfileMrItem = medProfileMrItem;
	}			
}

