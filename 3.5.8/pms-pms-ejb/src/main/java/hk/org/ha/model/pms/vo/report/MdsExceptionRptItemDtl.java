package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

public class MdsExceptionRptItemDtl implements Serializable {

	private static final long serialVersionUID = 1L;

	private String drugDisplayName;
	
	private String alertTitle;
	
	private String alertDesc;

	public String getDrugDisplayName() {
		return drugDisplayName;
	}

	public void setDrugDisplayName(String drugDisplayName) {
		this.drugDisplayName = drugDisplayName;
	}

	public String getAlertTitle() {
		return alertTitle;
	}

	public void setAlertTitle(String alertTitle) {
		this.alertTitle = alertTitle;
	}

	public String getAlertDesc() {
		return alertDesc;
	}

	public void setAlertDesc(String alertDesc) {
		this.alertDesc = alertDesc;
	}
}
