package hk.org.ha.model.pms.biz.inbox;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.medprofile.MedProfileManagerLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileOrderProcessorLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileStatManagerLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileStatTrxManagerLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.udt.medprofile.ManualTransferResult;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatus;
import hk.org.ha.model.pms.udt.medprofile.WardTransferType;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("medProfileSupportService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MedProfileSupportServiceBean implements MedProfileSupportServiceLocal {
	
	private static final String MED_PROFILE_QUERY_HINT_BATCH_1 = "o.medProfileItemList.medProfileMoItem";

	@PersistenceContext
	private EntityManager em;
	
	@In
	private MedProfileManagerLocal medProfileManager;
	
	@In
	private MedProfileStatManagerLocal medProfileStatManager;
	
	@In
	private MedProfileStatTrxManagerLocal medProfileStatTrxManager;
	
	@In
	private MedProfileOrderProcessorLocal medProfileOrderProcessor;
	
	@In
	private Workstore workstore;
	
    public MedProfileSupportServiceBean() {
    }

    @Override
    public ManualTransferResult manualTransfer(MedProfile medProfile, boolean transferFlag, Date transferDate,
    		boolean applyImmediately, Date itemCutOffDate) {
    
    	MedProfile medProfileInDB = fetchLockedMedProfile(medProfile.getId());
    	
    	if (medProfileInDB == null) {
    		return ManualTransferResult.NoProfileFound;
    	} else if (MedProfileUtils.compare(medProfile.getVersion(), medProfileInDB.getVersion()) != 0) {
    		return ManualTransferResult.OptimisticLock;
    	} else if (Boolean.TRUE.equals(medProfileInDB.getProcessingFlag())) {
    		return ManualTransferResult.ProcessingByOthers;
    	}
    	
    	medProfileInDB.setTransferFlag(transferFlag);
    	if (transferDate != null) {
    		medProfileInDB.setTransferDate(transferDate);
    	}
    	if (applyImmediately) {
    		medProfileManager.markItemTransferInfo(medProfileInDB, medProfileInDB.getMedProfileItemList(), itemCutOffDate,
    				WardTransferType.Manual);
    		medProfileStatManager.recalculateAllStat(medProfileInDB, true);
    	}
    	em.persist(medProfileInDB);
    	
    	return ManualTransferResult.Success;
    }

    @Override
    public void forceProcessPendingOrders(MedProfile medProfile) {
    	medProfileOrderProcessor.processMedProfileOrder(medProfile.getOrderNum(), true);
    }
    
    @SuppressWarnings("unchecked")
    @Override
	public void recalculateDashboardRelatedStat() {

    	Map<Long, MedProfile> medProfileMap = new TreeMap<Long, MedProfile>();
    	
    	appendToMedProfileMap(medProfileMap, em.createQuery(
    			"select p from MedProfile p" + // 20151012 index check : MedProfile.id MedProfilePoItemStat.hospCode : PK_MED_PROFILE FK_MED_PROFILE_PO_ITEM_STAT_01
    			" where p.id in" +
    			" (select distinct s.medProfileId from MedProfilePoItemStat s" +
    			"  where s.hospCode = :hospCode)")
    			.setParameter("hospCode", workstore.getHospCode())
    			.getResultList());

    	appendToMedProfileMap(medProfileMap, em.createQuery(
    			"select p from MedProfile p" + // 20160809 index check : MedProfile.hospCode,status : I_MED_PROFILE_08
    			" where p.status in :activeStatuses" +
    			" and p.hospCode = :hospCode")
    			.setParameter("hospCode", workstore.getHospCode())
    			.setParameter("activeStatuses", MedProfileStatus.Not_Inactive)
    			.getResultList());    	
    	
    	medProfileStatTrxManager.recalculateStat(medProfileMap.values(), false, false, true);
    }
    
    private void appendToMedProfileMap(Map<Long, MedProfile> medProfileMap, List<MedProfile> medProfileList) {
    	
    	for (MedProfile medProfile: medProfileList) {
    		medProfileMap.put(medProfile.getId(), medProfile);
    	}
    }
    
	@SuppressWarnings("unchecked")
	private MedProfile fetchLockedMedProfile(Long id) {
    	
    	return (MedProfile) MedProfileUtils.getFirstItem(em.createQuery(
    			"select o from MedProfile o" + // 20150206 index check : MedProfile.id : PK_MED_PROFILE
    			" where o.id = :id and o.id is not null") // don't delete : magic code fix bug for https://bugs.eclipse.org/bugs/show_bug.cgi?id=347592
    			.setParameter("id", id)
    			.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
    			.setHint(QueryHints.BATCH, MED_PROFILE_QUERY_HINT_BATCH_1)
    			.getResultList());
    }
    
	@Remove    
	@Override
	public void destroy() {
	}
}
