package hk.org.ha.model.pms.biz.onestop;

import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.vo.onestop.OneStopOrderItemSummary;
import hk.org.ha.model.pms.vo.onestop.OneStopRefillItemSummary;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

@Local
public interface OneStopUtilsLocal {
		
	String convertOrderNum(String orderNum);
	
	List<String> retrieveFriendClubListAndDefaultPatHospCode();
	
	Boolean isInPatCase(String caseNum);
	
	Map<String, Object> retrieveTicketParam(String ticketNum);
	
	String convertRefillNum(String refillNum);
	
	String startDate(Boolean displayAllMoe);
	
	String endDate(Boolean displayAllMoe);
	
	OneStopOrderItemSummary convertSummaryFromUnvetOrder(MedOrder medOrder);
	
	OneStopOrderItemSummary convertSummaryFromVettedOrder(DispOrder dispOrder);
	
	OneStopOrderItemSummary constructDispendedOrderStatus(OneStopOrderItemSummary oneStopOrderItemSummary, DispOrder dispOrder, PharmOrder pharmOrder, MedOrder medOrder);
	
	List<OneStopRefillItemSummary> convertRefillSfiRefillOrder(List<RefillSchedule> refillScheduleList, Boolean isSfiRefillSearch);
	
	Map<String, String> phsSpecialtyWardMapping(String dispHospCode, String dispWorkstoreCode, String patHospCode, String mappingType, 
												String pasSpecCode, String pasSubSpecCode, String pasWardCode, String patType, boolean useDefaultAssociation);
	
}