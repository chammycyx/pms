package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class PivasWorkloadSummaryPatientRptWardSummaryDtl implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String ward;
	
	private Integer totalNumOfDose;
	
	public String getWard() {
		return ward;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}

	public Integer getTotalNumOfDose() {
		return totalNumOfDose;
	}

	public void setTotalNumOfDose(Integer totalNumOfDose) {
		this.totalNumOfDose = totalNumOfDose;
	}
	
}
