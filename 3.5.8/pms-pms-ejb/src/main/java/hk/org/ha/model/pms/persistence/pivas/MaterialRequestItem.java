package hk.org.ha.model.pms.persistence.pivas;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.udt.pivas.MaterialRequestItemStatus;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "MATERIAL_REQUEST_ITEM")
public class MaterialRequestItem extends VersionEntity {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "materialRequestItemSeq")
	@SequenceGenerator(name = "materialRequestItemSeq", sequenceName = "SQ_MATERIAL_REQUEST_ITEM", initialValue = 100000000)
	private Long id;
	
	@Converter(name = "MaterialRequestItem.status", converterClass = MaterialRequestItemStatus.Converter.class)
    @Convert("MaterialRequestItem.status")
	@Column(name="STATUS", nullable = false, length = 1)
	private MaterialRequestItemStatus status;	

	@Column(name = "DUE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dueDate;

	@Column(name = "ISSUE_QTY", nullable = false, precision = 19, scale = 4)
	private BigDecimal issueQty;

	@Column(name = "BASE_UNIT", length = 4)
	private String baseUnit;

	@Column(name = "ITEM_CODE", length = 6, nullable = false)
	private String itemCode;

	@ManyToOne
	@JoinColumn(name = "HOSP_CODE", nullable = false)
	private Hospital hospital;
	
	@Column(name = "HOSP_CODE", nullable = false, insertable = false, updatable = false)
	private String hospCode;	
	
	@ManyToOne
	@JoinColumn(name = "MATERIAL_REQUEST_ID", nullable = false)
	private MaterialRequest materialRequest;
	
	@Column(name = "MATERIAL_REQUEST_ID", nullable = false, insertable = false, updatable = false)
	private Long materialRequestId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MED_PROFILE_PO_ITEM_ID", nullable = false)
	private MedProfilePoItem medProfilePoItem;

	public MaterialRequestItem() {
		status = MaterialRequestItemStatus.None;
		issueQty = new BigDecimal(0);
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MaterialRequestItemStatus getStatus() {
		return status;
	}

	public void setStatus(MaterialRequestItemStatus status) {
		this.status = status;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public BigDecimal getIssueQty() {
		return issueQty;
	}

	public void setIssueQty(BigDecimal issueQty) {
		this.issueQty = issueQty;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public Hospital getHospital() {
		return hospital;
	}

	public void setHospital(Hospital hospital) {
		this.hospital = hospital;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public MaterialRequest getMaterialRequest() {
		return materialRequest;
	}

	public void setMaterialRequest(MaterialRequest materialRequest) {
		this.materialRequest = materialRequest;
	}
	
	public Long getMaterialRequestId() {
		return materialRequestId;
	}

	public void setMaterialRequestId(Long materialRequestId) {
		this.materialRequestId = materialRequestId;
	}

	public MedProfilePoItem getMedProfilePoItem() {
		return medProfilePoItem;
	}

	public void setMedProfilePoItem(MedProfilePoItem medProfilePoItem) {
		this.medProfilePoItem = medProfilePoItem;
	}
}
