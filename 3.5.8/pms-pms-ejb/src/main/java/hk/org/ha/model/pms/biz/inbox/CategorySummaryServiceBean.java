package hk.org.ha.model.pms.biz.inbox;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.reftable.MpWardManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatAdminType;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("categorySummaryService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
@SuppressWarnings("unused")
public class CategorySummaryServiceBean implements CategorySummaryServiceLocal {

	@In
	private PharmInboxManagerLocal pharmInboxManager;	
	
	@In
	private CategorySummaryLocal categorySummary;
	
	@In
	private MpWardManagerLocal mpWardManager;

	@In
	private Workstore workstore;
	
	@In
	private Workstation workstation;
	
	@In @Out
	private Boolean showAllWards;
	
	@In @Out
	private Boolean showChemoProfileOnly;
	
    public CategorySummaryServiceBean() {
    }
    
    @Create
    @Override
    public void init() {
    	showAllWards = pharmInboxManager.getShowAllWards(workstation);
    	showChemoProfileOnly = pharmInboxManager.getShowChemoProfileOnly(workstation, showChemoProfileOnly);
    }
    
    @Override
    public void setShowChemoProfileOnly(Boolean showChemoProfileOnly) {
    	this.showChemoProfileOnly = showChemoProfileOnly;
    	countCategories();
    }
    
    @Override
	public void countCategories() {
    	categorySummary.countCategories(null, workstore, showChemoProfileOnly, mpWardManager.retrieveWardFilterList(workstore, showAllWards));    	
    }
    
	@Remove
	@Override
	public void destroy() {
	}
}
