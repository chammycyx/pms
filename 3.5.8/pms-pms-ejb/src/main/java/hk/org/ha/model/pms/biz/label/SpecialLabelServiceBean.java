package hk.org.ha.model.pms.biz.label;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.reftable.CustomLabelManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.CustomLabel;
import hk.org.ha.model.pms.udt.label.CustomLabelType;
import hk.org.ha.model.pms.vo.label.SpecialLabel;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("specialLabelService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SpecialLabelServiceBean implements SpecialLabelServiceLocal {

	@In 
	private Workstore workstore;
	
	@In
	private CustomLabelManagerLocal customLabelManager;
	
	@Out(required = false)
	private SpecialLabel specialLabel;
	
	
	public void retrieveSpecialLabel(String labelNum) {
		specialLabel = null;
		CustomLabel customlabel = customLabelManager.retrieveCustomLabel(labelNum, CustomLabelType.Special);
		if (customlabel != null) {
			specialLabel = customlabel.getSpecialLabel();
		}
	}
	
	public void updateSpecialLabel(SpecialLabel specialLabel){

		CustomLabel customLabel = new CustomLabel();
		customLabel.setSpecialLabel(specialLabel);
		customLabel.setType(CustomLabelType.Special);
		customLabel.setWorkstore(workstore);
		customLabel.setName(specialLabel.getLabelNum());
		customLabel.setDescription(specialLabel.getDescription());
		customLabelManager.updateCustomLabel(customLabel);
	}
	
	public void removeSpecialLabel(String labelNum){
		customLabelManager.removeCustomLabel(labelNum, CustomLabelType.Special);
	}
	
	
	@Remove
	public void destroy() {
		
	}
}
