package hk.org.ha.model.pms.biz.vetting;

import static hk.org.ha.model.pms.prop.Prop.VETTING_CAPD_ENABLED;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.order.DispOrderManagerLocal;
import hk.org.ha.model.pms.biz.printing.InvoiceRendererLocal;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.refill.RefillCouponRendererLocal;
import hk.org.ha.model.pms.persistence.disp.CapdVoucher;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("printJobManager")
@MeasureCalls
public class PrintJobManagerBean implements PrintJobManagerLocal {

	@PersistenceContext
	private EntityManager em;

	@In
	private InvoiceRendererLocal invoiceRenderer;
	
	@In
	private DispOrderManagerLocal dispOrderManager;
	
	@In
	private CapdVoucherManagerLocal capdVoucherManager;
	
	@In
	private RefillCouponRendererLocal refillCouponRenderer;
	
	@In
	private PrintAgentInf printAgent;

	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void renderAndPrint(DispOrder dispOrder, List<CapdVoucher> voucherList, OrderViewInfo orderViewInfo) {
		// prepare the print job list
		List<RenderAndPrintJob> printJobList = new ArrayList<RenderAndPrintJob>();
		this.preparePrintJobList(printJobList, dispOrder, voucherList, orderViewInfo);

		// print it out
		printAgent.renderAndPrint(printJobList);
	}
	
	private void preparePrintJobList(List<RenderAndPrintJob> printJobList, DispOrder dispOrder, List<CapdVoucher> voucherList, OrderViewInfo orderViewInfo) {

		if (dispOrder.getSfiFlag()) {
			invoiceRenderer.prepareInvoicePrintJob(printJobList, dispOrder);
		}
		
		if (dispOrder.getAdminStatus() == DispOrderAdminStatus.Normal) {

			// need update the dispOrder
			DispOrder tempDispOrder = em.find(DispOrder.class, dispOrder.getId());
			tempDispOrder.loadChild(false);
			
			for (MedOrderItem moi : tempDispOrder.getPharmOrder().getMedOrder().getMedOrderItemList()) {
				if (orderViewInfo.getMedOrderItemInfoByItemNum(moi.getItemNum()) == null) {
					continue;
				}
				moi.setNumOfLabel(orderViewInfo.getMedOrderItemInfoByItemNum(moi.getItemNum()).getNumOfLabel());
			}
						
			// print label, capd and infusionInstructionSummary
			dispOrderManager.prepareDispOrderPrintJob(printJobList, tempDispOrder);

			//print refillCoupon
			refillCouponRenderer.prepareRefillCouponPrintJob(printJobList, tempDispOrder, false);			

			// print capd
			if (voucherList != null && !voucherList.isEmpty() && 
					VETTING_CAPD_ENABLED.get()) {
				capdVoucherManager.prepareCapdVoucherPrintJob(printJobList, dispOrder.getPrintLang(), voucherList, false);
				capdVoucherManager.updateCapdVoucherStatusToIssue(voucherList);
			}
		}
	}	
	
	@Remove
	public void destroy() {
	}
}
