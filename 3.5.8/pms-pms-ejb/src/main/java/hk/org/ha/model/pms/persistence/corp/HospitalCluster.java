package hk.org.ha.model.pms.persistence.corp;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.RecordStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "HOSPITAL_CLUSTER")
public class HospitalCluster extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "CLUSTER_CODE", length = 3)
	private String clusterCode;
	
    @Converter(name = "HospitalCluster.status", converterClass = RecordStatus.Converter.class)
    @Convert("HospitalCluster.status")
	@Column(name = "STATUS", nullable = false, length = 1)
	private RecordStatus status;
    
	public HospitalCluster() {
		status = RecordStatus.Active;
	}
	
	public HospitalCluster(String clusterCode) {
		this();
		this.clusterCode = clusterCode;
	}

	public String getClusterCode() {
		return clusterCode;
	}

	public void setClusterCode(String clusterCode) {
		this.clusterCode = clusterCode;
	}

	public RecordStatus getStatus() {
		return status;
	}

	public void setStatus(RecordStatus status) {
		this.status = status;
	}
}
