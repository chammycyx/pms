package hk.org.ha.model.pms.biz.report;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.persistence.reftable.SuspendStat;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.joda.time.DateTime;

@Stateful
@Scope(ScopeType.SESSION)
@Name("suspendStatRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SuspendStatRptServiceBean implements SuspendStatRptServiceLocal {
	@PersistenceContext
	private EntityManager em;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent; 
	
	@In 
	private Workstore workstore;

	private List<SuspendStat> suspendStatList;
	
	private boolean retrieveSuccess;

	private Map<String, Object> parameters = new HashMap<String, Object>();

	public void retrieveSuspendStatList(Integer pastDay, Date dateFrom, Date dateTo) {
		constructSuspendStatList(pastDay, new Date(), dateFrom, dateTo);
	}
	
	@SuppressWarnings("unchecked")
	public List<SuspendStat> constructSuspendStatList(Integer pastDay, Date today, Date dateFrom, Date dateTo) {
		Date startDate = null;
		Date endDate = null;
		
		if (dateFrom !=null && dateTo !=null){
			DateTime df = new DateTime(dateFrom);
			startDate = df.toDate();
			
			DateTime dt = new DateTime(dateTo);
			endDate = dt.plusDays(1).toDate();
		}else if(pastDay!=null){
			DateTime dt = new DateTime(today);
			startDate = dt.minusDays(pastDay.intValue()).toDate();
			endDate = dt.plusDays(1).toDate();
		}
		
		suspendStatList = em.createQuery(
				"select o from SuspendStat o" + // 20120306 index check : SuspendStat.workstore,dayEndBatchDate : UI_SUSPEND_STAT_01
				" where o.workstore = :workstore" +
				" and o.dayEndBatchDate between :startDate and :endDate"+
				" order by o.dayEndBatchDate desc")
				.setParameter("workstore", workstore)
				.setParameter("startDate", startDate, TemporalType.DATE)
				.setParameter("endDate", endDate, TemporalType.DATE)
				.getResultList();
		
		if ( suspendStatList.isEmpty() ) {
			retrieveSuccess = false;
		} else{
			retrieveSuccess = true;
		}
		
		return suspendStatList;
	}
	
	public void generateSuspendStatRpt() {
		OperationProfile activeProfile = em.find(OperationProfile.class, workstore.getOperationProfileId());
		
		
		parameters.put("hospCode", workstore.getHospCode());
		parameters.put("workstoreCode", workstore.getWorkstoreCode());	
		parameters.put("operationMode", activeProfile.getName());	
		
		printAgent.renderAndRedirect(new RenderJob(
				"SuspendStatRpt", 
				new PrintOption(), 
				parameters, 
				suspendStatList));
	}
	
	public void printSuspendStatRpt(){

		printAgent.renderAndPrint(new RenderAndPrintJob(
				"SuspendStatRpt", 
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				parameters, 
				suspendStatList));
		
	}

	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}
	
	@Remove
	public void destroy() {
		if(suspendStatList!=null){
			suspendStatList=null;
		}
		
	}

}
