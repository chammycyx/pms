package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.vo.rx.PharmDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("itemConvertService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ItemConvertServiceBean implements ItemConvertServiceLocal {
	
	@In(required = false)
	private PharmOrder pharmOrder;
	
	@In
	private ItemConvertManagerLocal itemConvertManager;
	
	@SuppressWarnings("unused")
	@Out(required = false)
	private List<PharmOrderItem> pharmOrderItemList;
	
	@SuppressWarnings("unused")
	@Out(required = false)
	private BigDecimal pharmOrderItemDoseQty;
	
	@SuppressWarnings("unused")
	@Out(required = false)
	private BigDecimal pharmOrderItemCalQty;

	@SuppressWarnings("unused")
	@Out(required = false)
	private Integer itemConvertMedOrderItemNum;
	
	@Out(required = false)
	private RxItem rxItemForOrderEdit;
	
	public void convertMedOrderItem(MedOrderItem medOrderItem) {

		pharmOrderItemList = itemConvertManager.convertMedOrderItem(pharmOrder, medOrderItem);
		rxItemForOrderEdit = medOrderItem.getRxItem();
		if (pharmOrder.getMedOrder().getDocType() == MedOrderDocType.Manual && medOrderItem.getId() != null) {
			itemConvertMedOrderItemNum = pharmOrder.getMedOrder().getAndIncreaseMaxItemNum();			
		}
	}
		
	public void recalPharmOrderItem(PharmOrderItem pharmOrderItem) {

		PharmDrug pharmDrug = itemConvertManager.recalPharmOrderItem(pharmOrderItem);
		if (pharmDrug != null) {
			pharmOrderItemCalQty = pharmDrug.getCalQty();
			pharmOrderItemDoseQty = pharmDrug.getDoseQty();
		}
	}
	
	@Override
	public List<BigDecimal> calSubTotalDosage(RxItem rxItem, Integer doseGroupIndex, Integer doseIndex) {
		return itemConvertManager.calSubTotalDosage(rxItem, doseGroupIndex, doseIndex);
	}
	
	@Remove
	public void destroy() {
		if (rxItemForOrderEdit != null) {
			rxItemForOrderEdit = null;
		}
	}

}
