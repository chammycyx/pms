package hk.org.ha.model.pms.biz.vetting;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.vo.Capd;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("capdListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CapdListServiceBean implements CapdListServiceLocal {
	
	@In
	private CapdListManagerLocal capdListManager;
	
	@Out(required = false)
	private List<String> capdSupplierSystemList;
	
	@Out(required = false)
	private List<String> capdCalciumStrengthList;
	
	@Out(required = false)
	private List<Capd> capdConcentrationList;

	
	public void clearCapdList(){
		capdSupplierSystemList = null;
		capdCalciumStrengthList = null;
		capdConcentrationList = null;
	}
	
	public void retrieveCapdList(String supplierSystem, String calciumStrength){
		retrieveCapdSupplierSystemList();
		if ( StringUtils.isNotBlank(supplierSystem) ){
			retrieveCapdCalciumStrengthList(supplierSystem);
			if ( StringUtils.isNotBlank(calciumStrength )) {
				retrieveCapdConcentrationList(supplierSystem, calciumStrength);
			}
		}
	}
	
	public void retrieveCapdSupplierSystemList(){
		capdSupplierSystemList = capdListManager.retrieveCapdSupplierSystemList();
	}
	
	public void retrieveCapdCalciumStrengthList(String supplierSystem){
		capdCalciumStrengthList = capdListManager.retrieveCapdCalciumStrengthList(supplierSystem);
		capdConcentrationList = null;
	}
	
	public void retrieveCapdConcentrationList(String supplierSystem, String calciumStrength){
		capdConcentrationList = capdListManager.retrieveCapdConcentrationList(supplierSystem, calciumStrength);
	}

	@Remove
	public void destroy() 
	{
		if (capdSupplierSystemList != null) {
			capdSupplierSystemList = null;
		}
		
		if (capdCalciumStrengthList != null) {
			capdCalciumStrengthList = null;
		}
		
		if (capdConcentrationList != null) {
			capdConcentrationList = null;
		}
	}
}
