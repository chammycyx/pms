package hk.org.ha.model.pms.biz.support;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.biz.support.PropHelper;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.reftable.CorporateProp;
import hk.org.ha.model.pms.persistence.reftable.HospitalProp;
import hk.org.ha.model.pms.persistence.reftable.OperationProp;
import hk.org.ha.model.pms.persistence.reftable.Prop;
import hk.org.ha.model.pms.persistence.reftable.WorkstationProp;
import hk.org.ha.model.pms.persistence.reftable.WorkstoreProp;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("propManager")
@MeasureCalls
public class PropManagerBean implements PropManagerLocal {

	@PersistenceContext
	private EntityManager em;
	
	@In
	private PropHelper propHelper;
	
	public List<CorporateProp> retrieveCorporatePropList() {
		return propHelper.retrieveCorporatePropList(em);
	}

	public List<HospitalProp> retrieveHospitalPropList() {
		return propHelper.retrieveHospitalPropList(em);
	}
	
	public List<WorkstoreProp> retrieveWorkstorePropList(Hospital hospital) {
		return propHelper.retrieveWorkstorePropList(em, hospital);
	}
	
	public List<Prop> retrievePropListByWorkstationPropName() {
		return propHelper.retrievePropListByWorkstationPropName(em);
	}
	
	public List<WorkstationProp> retrieveWorkstationPropListByHostName(String hospCode, String hostName) {
		return propHelper.retrieveWorkstationPropListByHostName(em, hospCode, hostName);
	}
	
	public List<OperationProp> retrieveOperationPropList(Long operationProfileId){
		return propHelper.retrieveOperationPropList(em, operationProfileId);
	}
	
	public void updateCorporatePropList(List<CorporateProp> corporatePropList) {
		propHelper.updateCorporatePropList(em, corporatePropList, "#0314");
	}
	
	public void updateHospitalPropList(List<HospitalProp> hospitalPropList) {
		propHelper.updateHospitalPropList(em, hospitalPropList);
	}
	
	public void updateWorkstorePropList(List<WorkstoreProp> workstorePropList) {
		propHelper.updateWorkstorePropList(em, workstorePropList);
	}
	
	public void updateWorkstationPropList(List<WorkstationProp> workstationPropList) {
		propHelper.updateWorkstationPropList(em, workstationPropList);
	}
	
	public void updateOperationPropList(List<OperationProp> operationPropList) {
		propHelper.updateOperationPropList(em, operationPropList);
	}
}
