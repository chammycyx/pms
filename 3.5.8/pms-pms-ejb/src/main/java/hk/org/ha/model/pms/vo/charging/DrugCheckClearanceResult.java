package hk.org.ha.model.pms.vo.charging;

import java.io.Serializable;
import java.util.List;
import hk.org.ha.model.pms.udt.charging.ClearanceStatus;

public class DrugCheckClearanceResult implements Serializable {

	private static final long serialVersionUID = 1L;

	private ClearanceStatus stdDrugClearance;
	
	private ClearanceStatus sfiDrugClearance;
	
	private String sfiInvNum;
		
	//For Check and Issue
	private boolean forceProceedFlag;

	//For OneStop and Check&Issue
	private String systemMessageCode;
	
	//For DrugCheckClearanceEnq
	private List<String> sysMsgCodeList;

	public ClearanceStatus getStdDrugClearance() {
		return stdDrugClearance;
	}

	public void setStdDrugClearance(ClearanceStatus stdDrugClearance) {
		this.stdDrugClearance = stdDrugClearance;
	}

	public ClearanceStatus getSfiDrugClearance() {
		return sfiDrugClearance;
	}

	public void setSfiDrugClearance(ClearanceStatus sfiDrugClearance) {
		this.sfiDrugClearance = sfiDrugClearance;
	}

	public String getSfiInvNum() {
		return sfiInvNum;
	}

	public void setSfiInvNum(String sfiInvNum) {
		this.sfiInvNum = sfiInvNum;
	}
	
	public boolean isForceProceedFlag() {
		return forceProceedFlag;
	}

	public void setForceProceedFlag(boolean forceProceedFlag) {
		this.forceProceedFlag = forceProceedFlag;
	}

	public String getSystemMessageCode() {
		return systemMessageCode;
	}

	public void setSystemMessageCode(String systemMessageCode) {
		this.systemMessageCode = systemMessageCode;
	}

	public void setSysMsgCodeList(List<String> sysMsgCodeList) {
		this.sysMsgCodeList = sysMsgCodeList;
	}

	public List<String> getSysMsgCodeList() {
		return sysMsgCodeList;
	}
}
