package hk.org.ha.model.pms.vo.rx;

import java.io.Serializable;

import hk.org.ha.model.pms.udt.vetting.ActionStatus;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class ActionStatusAdapter extends XmlAdapter<String, ActionStatus> implements Serializable {

	private static final long serialVersionUID = 1L;

	public String marshal( ActionStatus v ) {		
		return (v == null) ? null : v.getDataValue();
	}
	
	public ActionStatus unmarshal( String v ) {		
		return ActionStatus.dataValueOf(v);
	}

}