package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmSiteCacher;
import hk.org.ha.model.pms.dms.persistence.DmSite;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("dmSiteService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DmSiteServiceBean implements DmSiteServiceLocal {

	@Logger
	private Log logger;

	@In
	private DmSiteCacher dmSiteCacher;

	@Out(required = false)
	private DmSite dmSite;
	
	private boolean retrieveSuccess;
	
	public void retrieveDmSite(String siteCode)
	{
		logger.debug("retrieveDmSite #0", siteCode);

		retrieveSuccess = false;		
		dmSite = dmSiteCacher.getSiteBySiteCode(siteCode);
		if (dmSite != null) {
			retrieveSuccess = true;
		}
		
		logger.debug("retrieveDmSite result #0", retrieveSuccess);
	}

	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}
	
	@Remove
	public void destroy() 
	{
		if (dmSite != null) {
			dmSite = null;
		}
	}

}
