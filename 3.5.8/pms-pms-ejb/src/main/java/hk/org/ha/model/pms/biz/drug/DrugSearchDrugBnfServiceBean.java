package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.vo.Bnf;
import hk.org.ha.model.pms.dms.vo.BnfSearchCriteria;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
import hk.org.ha.model.pms.vo.drug.DrugSearchReturn;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("drugSearchDrugBnfService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DrugSearchDrugBnfServiceBean implements DrugSearchDrugBnfServiceLocal {
	
	@Logger
	private Log logger;
	
	private static final char[] DELIMITER = new char[] {' ' , '~' , '&' , '-' , '[' , '(' , '<', '+', '.'}; 
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private Workstore workstore;

	
	public DrugSearchReturn retrieveDrugSearchBnf(DrugSearchSource drugSearchSource, BnfSearchCriteria bnfSearchCriteria) {
		bnfSearchCriteria.setDispHospCode(workstore.getHospCode());
		bnfSearchCriteria.setDispWorkstore(workstore.getWorkstoreCode());

		List<Bnf> drugSearchDrugBnfList = dmsPmsServiceProxy.retrieveBnfByDisplayname(bnfSearchCriteria);
		Collections.sort(drugSearchDrugBnfList, new BnfComparator());
		for (Bnf bnf : drugSearchDrugBnfList) {
			bnf.setBnfDesc(WordUtils.capitalizeFully(bnf.getBnfDesc(), DELIMITER));
		}

		DrugSearchReturn drugSearchReturn = new DrugSearchReturn();
		drugSearchReturn.setDrugSearchSource(drugSearchSource);
		drugSearchReturn.setDrugSearchDrugBnfList(drugSearchDrugBnfList);
		return drugSearchReturn;
	}

	public static class BnfComparator implements Comparator<Bnf>, Serializable {

		private static final long serialVersionUID = 1L;
		
		@Override
		public int compare(Bnf bnf1, Bnf bnf2) {
			return new CompareToBuilder().append( bnf1.getBnfDesc(), bnf2.getBnfDesc() )
										 .toComparison();
		}		
	}
	
	@Remove
	public void destroy() 
	{
	}
}