package hk.org.ha.model.pms.vo.drug;

import hk.org.ha.model.pms.corp.cache.DmFormCacher;
import hk.org.ha.model.pms.dms.persistence.DmForm;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
import org.springframework.beans.BeanUtils;

@ExternalizedBean(type=DefaultExternalizer.class)
public class DmFormLite implements Serializable {
	private static final long serialVersionUID = 1L;
	private String formCode;
	private String labelDesc;
	private String formDescEng;
	private String moeDesc;
	
	public static DmFormLite objValueOf(String formCode) {
		return objValueOf(DmFormCacher.instance().getFormByFormCode(formCode));
	}
	
	public static DmFormLite objValueOf(DmForm dmForm) {
		if (dmForm != null) {
			DmFormLite dmFormLite = new DmFormLite();
			BeanUtils.copyProperties(dmForm, dmFormLite);
			return dmFormLite;
		}
		return null;
	}
	
	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}
	public String getFormCode() {
		return formCode;
	}
	public void setLabelDesc(String labelDesc) {
		this.labelDesc = labelDesc;
	}
	public String getLabelDesc() {
		return labelDesc;
	}
	public void setFormDescEng(String formDescEng) {
		this.formDescEng = formDescEng;
	}
	public String getFormDescEng() {
		return formDescEng;
	}
	public void setMoeDesc(String moeDesc) {
		this.moeDesc = moeDesc;
	}
	public String getMoeDesc() {
		return moeDesc;
	}
}
