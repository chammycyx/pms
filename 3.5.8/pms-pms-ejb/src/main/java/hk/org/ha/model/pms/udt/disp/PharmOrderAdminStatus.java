package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PharmOrderAdminStatus implements StringValuedEnum {

	Normal("N", "Normal"),
	AllowModify("A", "Allow Modify"),
	Revoke("U", "Revoke");

    private final String dataValue;
    private final String displayValue;

    PharmOrderAdminStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static PharmOrderAdminStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PharmOrderAdminStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<PharmOrderAdminStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<PharmOrderAdminStatus> getEnumClass() {
    		return PharmOrderAdminStatus.class;
    	}
    }
}
