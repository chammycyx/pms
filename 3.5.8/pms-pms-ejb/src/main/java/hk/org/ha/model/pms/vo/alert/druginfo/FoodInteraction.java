package hk.org.ha.model.pms.vo.alert.druginfo;

import java.io.Serializable;

public class FoodInteraction implements Comparable<FoodInteraction>, Serializable {

	private static final long serialVersionUID = 1L;

	private String interactor;
	
	private String significance;
	
	private String significanceLevel;

	private Integer monoId;
	
	private Integer monoType;
	
	private Boolean showMonoFlag;
	
	private String html;

	private Integer sortSeq;

	public String getInteractor() {
		return interactor;
	}

	public void setInteractor(String interactor) {
		this.interactor = interactor;
	}

	public String getSignificance() {
		return significance;
	}

	public void setSignificance(String significance) {
		this.significance = significance;
	}

	public String getSignificanceLevel() {
		return significanceLevel;
	}

	public void setSignificanceLevel(String significanceLevel) {
		this.significanceLevel = significanceLevel;
	}
	
	public Integer getMonoId() {
		return monoId;
	}

	public void setMonoId(Integer monoId) {
		this.monoId = monoId;
	}

	public Integer getMonoType() {
		return monoType;
	}

	public void setMonoType(Integer monoType) {
		this.monoType = monoType;
	}

	public Boolean getShowMonoFlag() {
		return showMonoFlag;
	}

	public void setShowMonoFlag(Boolean showMonoFlag) {
		this.showMonoFlag = showMonoFlag;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}
	
	public Integer getSortSeq() {
		return sortSeq;
	}

	public void setSortSeq(Integer sortSeq) {
		this.sortSeq = sortSeq;
	}
	
	public int compareTo(FoodInteraction o) {
		return sortSeq.compareTo(o.getSortSeq());
	}
}
