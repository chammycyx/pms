package hk.org.ha.model.pms.biz.printing;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.persistence.reftable.WorkstationPrint;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.reftable.PrintType;
import hk.org.ha.model.pms.vo.printing.PrinterSelect;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;

@AutoCreate
@Stateless
@Name("printerSelector")
@MeasureCalls
public class PrinterSelectorBean implements PrinterSelectorLocal {

	@PersistenceContext
	private EntityManager em;

	@In
	private Workstore workstore; 
	
	@In
	private Workstation workstation;

	@In(scope=ScopeType.SESSION, required=false) 
	@Out(scope=ScopeType.SESSION, required=false)
	private Map<PrintDocType, PrinterSelect> printerSelectMap;	
	
	private Map<PrintDocType, PrinterSelect> getPrinterSelectMap() {
		if (printerSelectMap == null) {
			printerSelectMap = new HashMap<PrintDocType, PrinterSelect>();
			printerSelectMap.put(PrintDocType.OtherLabel, retrievePrintSelectByPrintDocType(PrintDocType.OtherLabel));
			printerSelectMap.put(PrintDocType.Report, retrievePrintSelectByPrintDocType(PrintDocType.Report));
			printerSelectMap.put(PrintDocType.SfiInvoice, retrievePrintSelectByPrintDocType(PrintDocType.SfiInvoice));
			printerSelectMap.put(PrintDocType.SfiRefillCoupon, retrievePrintSelectByPrintDocType(PrintDocType.SfiRefillCoupon));
			printerSelectMap.put(PrintDocType.RefillCoupon, retrievePrintSelectByPrintDocType(PrintDocType.RefillCoupon));
			printerSelectMap.put(PrintDocType.CapdVoucher, retrievePrintSelectByPrintDocType(PrintDocType.CapdVoucher));
			printerSelectMap.put(PrintDocType.PivasLargeLabel, retrievePrintSelectByPrintDocType(PrintDocType.PivasLargeLabel));
			printerSelectMap.put(PrintDocType.PivasColorLabel, retrievePrintSelectByPrintDocType(PrintDocType.PivasColorLabel));
		}		
		return printerSelectMap;
	}
	
	public PrinterSelect retrievePrinterSelect(PrintDocType printDocType){		
		PrinterSelect printerSelect = this.getPrinterSelectMap().get(printDocType);
		return new PrinterSelect(printerSelect);
	}
	
	@SuppressWarnings("unchecked")
	private PrinterSelect retrievePrintSelectByPrintDocType(PrintDocType printDocType){
		PrinterSelect printSelect = new PrinterSelect();
		printSelect.setPrintDocType(printDocType);
		Workstation printerWorkstation = workstation;
		
		boolean findPrinter = false;
		Map<String, Workstation> workstationMap = new HashMap<String, Workstation>();
		
		while (!findPrinter ) {

			printerWorkstation.loadPrintInfo();
			WorkstationPrint wp = getWorkstationPrintByPrintDocType(printDocType, printerWorkstation);
			
			if ( wp == null || wp.getType() != PrintType.Redirect) {
				findPrinter = true;
			} else {
				List<Workstation> workstationList = em.createQuery(
						"select o from Workstation o " + // 20120303 index check : Workstation.workstore,workstationCode : UI_WORKSTATION_01
						" where o.workstore = :workstore " +
						" and o.workstationCode = :workstationCode")
						.setParameter("workstore", workstore)
						.setParameter("workstationCode", wp.getValue())
						.getResultList();

				if ( !workstationList.isEmpty() ) {
					printerWorkstation = workstationList.get(0);
					
					if (workstationMap.get(printerWorkstation.getWorkstationCode()) == null ) {
						workstationMap.put(printerWorkstation.getWorkstationCode(), printerWorkstation);
					} else {
						findPrinter = true;
						printerWorkstation = workstation;
					}
				}
			}
		}
		
		printSelect.setPrinterWorkstation(printerWorkstation);
		return printSelect;
	}
	
	private WorkstationPrint getWorkstationPrintByPrintDocType(PrintDocType printDocType, Workstation workstation){
		switch (printDocType) {
			case OtherLabel:
				return workstation.getOtherLabelPrint();
			case Report:
				return workstation.getReportPrint();
			case SfiInvoice:
				return workstation.getSfiInvoicePrint();
			case SfiRefillCoupon:
				return workstation.getSfiRefillCouponPrint();
			case RefillCoupon:
				return workstation.getRefillCouponPrint();
			case CapdVoucher:
				return workstation.getCapdVoucherPrint();
			case PivasLargeLabel:
				return workstation.getPivasLargeLabelPrint();
			case PivasColorLabel:
				return workstation.getPivasColorLabelPrint();
		}
		return null;
	}
}
