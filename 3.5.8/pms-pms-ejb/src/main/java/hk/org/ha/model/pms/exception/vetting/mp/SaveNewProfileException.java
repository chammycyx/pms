package hk.org.ha.model.pms.exception.vetting.mp;

import javax.ejb.ApplicationException;

@ApplicationException(rollback=true)
public class SaveNewProfileException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	private String type;

	public SaveNewProfileException(String type) {
		this.type = type;
	}	
	
	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
}
