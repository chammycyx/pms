package hk.org.ha.model.pms.biz.enquiry;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.onestop.OneStopUtilsLocal;
import hk.org.ha.model.pms.biz.validation.SearchTextValidatorLocal;
import hk.org.ha.model.pms.udt.validation.SearchTextType;
import hk.org.ha.model.pms.vo.charging.DrugCheckClearanceResult;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("drugChargeClearanceEnqService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DrugChargeClearanceEnqServiceBean implements DrugChargeClearanceEnqServiceLocal {

	public static final String INVALID_FORMAT = "0411";
	private final static String TICKET_NUM = "ticketNum";
	private final static String START_DATE = "startDate";	
	private final static String ERROR_MESSAGE = "errMsg";
	
	@Out(required=false)
	private DrugCheckClearanceResult drugClearCheckResult;
		
	@In
	private SearchTextValidatorLocal searchTextValidator;
	
	@In
	private OneStopUtilsLocal oneStopUtils;
	
	@In
	private DrugChargeClearanceEnqManagerLocal drugChargeClearanceEnqManager;
	
	@Override
	public void retrieveDrugClearance(String searchCriteria) {

		SearchTextType searchTextType = searchTextValidator.checkSearchInputType(searchCriteria);

		switch(searchTextType){
			case OrderNum:
				drugClearCheckResult = drugChargeClearanceEnqManager.checkClearanceByMoeOrderNo(oneStopUtils.convertOrderNum(searchCriteria));
				break;
			case SfiInvoice:
				drugClearCheckResult = drugChargeClearanceEnqManager.checkClearanceByInvoiceNumber(searchCriteria);
				break;
			case Ticket:
				Map<String, Object> ticketParamMap = oneStopUtils.retrieveTicketParam(searchCriteria);
				if ( !ticketParamMap.containsKey(ERROR_MESSAGE) && ticketParamMap.get(TICKET_NUM) != null && ticketParamMap.get(START_DATE) != null ){
					drugClearCheckResult = drugChargeClearanceEnqManager.checkClearanceByTicketNum((String)ticketParamMap.get(TICKET_NUM), (Date) ticketParamMap.get(START_DATE));
					break;
				}
			default:
				drugClearCheckResult = new DrugCheckClearanceResult();		
				drugClearCheckResult.setSysMsgCodeList(Arrays.asList(INVALID_FORMAT));
				break;
		}
	}
	
	@Override
	public void checkClearanceForOnestop(Long medOrderId) {
		drugClearCheckResult = drugChargeClearanceEnqManager.checkClearanceForOnestop(medOrderId);
	}

	@Override
	public void checkClearanceForOnestopManual(String patHospCode, Long orderNum, Long pasApptSeq) {
		drugClearCheckResult = drugChargeClearanceEnqManager.checkClearanceForOnestopManual(patHospCode, orderNum, pasApptSeq);
	}	
		
	@Remove
	public void destroy() 
	{
		if ( drugClearCheckResult != null ){
			drugClearCheckResult = null;
		}
	}


}
