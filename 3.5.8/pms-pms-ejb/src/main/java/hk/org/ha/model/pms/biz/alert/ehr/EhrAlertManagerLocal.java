package hk.org.ha.model.pms.biz.alert.ehr;

import hk.org.ha.model.pms.asa.exception.alert.ehr.EhrAllergyException;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.vo.alert.AlertProfile;

import javax.ejb.Local;

@Local
public interface EhrAlertManagerLocal {

	AlertProfile retrieveEhrAllergySummaryByPharmOrder(AlertProfile alertProfile, PharmOrder pharmOrder) throws EhrAllergyException;

	AlertProfile retrieveEhrAllergySummaryByMedProfile(AlertProfile alertProfile, MedProfile medProfile) throws EhrAllergyException;

	AlertProfile retrieveEhrAllergySummaryByMedOrder(AlertProfile alertProfile, MedOrder medOrder, Patient patient) throws EhrAllergyException;
	
	AlertProfile retrieveEhrAllergySummary(AlertProfile alertProfile, String patHospCode, String patKey, String hkid) throws EhrAllergyException;
}
