package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.vo.report.CapdVoucherRpt;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface CapdVoucherRptServiceLocal {

	List<CapdVoucherRpt> retrieveCapdVoucherRptList(Date dateFrom, Date dateTo, String hkid, String patName, String voucherNum);
	
	void generateCapdVoucherRpt();

	void printCapdVoucherRpt();
	
	void destroy();
	
}
