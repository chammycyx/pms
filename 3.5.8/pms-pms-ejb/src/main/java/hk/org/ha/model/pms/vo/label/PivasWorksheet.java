package hk.org.ha.model.pms.vo.label;

import hk.org.ha.model.pms.vo.printing.PrintOption;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "PivasWorksheet")
@ExternalizedBean(type=DefaultExternalizer.class)
public class PivasWorksheet implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@XmlElement(required = false)
	private String printName;
	
	@XmlElement(required = false)
	private String dosageQty;
	
	@XmlElement(required = false)
	private String volume;
	
	@XmlElement(required = false)
	private String diluentVolume;
	
	@XmlElement(required = true)
	private String pivasDosageUnit;
	
	@XmlElement(required = true)
	private String siteDesc;
	
	@XmlElement(required = true)
	private String solventVolume;
	
	@XmlElement(required = true)
	private Date manufactureDate;
	
	@XmlElement(required = true)
	private Date prepExpiryDate;
	
	@XmlElement(required = false)
	private String lotNum;
	
	@XmlElement(required = true)
	private String drugItemCode;
	
	@XmlElement(required = false)
	private String drugVolume;
	
	@XmlElement(required = true)
	private String drugManufCode;
	
	@XmlElement(required = true)
	private String drugBatchNum;
	
	@XmlElement(required = true)
	private Date drugExpiryDate;
	
	@XmlElement(required = false)
	private String solventItemCode;
	
	@XmlElement(required = false)
	private String solventManufCode;
	
	@XmlElement(required = false)
	private String solventBatchNum;
	
	@XmlElement(required = false)
	private Date solventExpiryDate;
	
	@XmlElement(required = false)
	private String diluentItemCode;
	
	@XmlElement(required = false)
	private String diluentManufCode;
	
	@XmlElement(required = false)
	private String diluentBatchNum;
	
	@XmlElement(required = false)
	private Date diluentExpiryDate;
	
	@XmlElement(required = true)
	private String prepQty;
	
	@XmlElement(required = true)
	private String containerName;
	
	@XmlElement(required = true)
	private String afterConcn;
	
	@XmlElement(required = true)
	private String drugStrength;
	
	@XmlElement(required = true)
	private String drugVolumeValue;
	
	@XmlElement(required = true)
	private String drugVolumeUnit;
	
	@XmlElement(required = false)
	private String drugTruncatedStrengthVolume;
	
	@XmlElement(required = false)
	private String diluentStrength;
	
	@XmlElement(required = false)
	private String diluentVolumeValue;
	
	@XmlElement(required = false)
	private String diluentVolumeUnit;
	
	@XmlElement(required = false)
	private String diluentTruncatedStrengthVolume;
	
	@XmlElement(required = false)
	private String solventStrength;
	
	@XmlElement(required = false)
	private String solventVolumeValue;
	
	@XmlElement(required = false)
	private String solventVolumeUnit;
	
	@XmlElement(required = false)
	private String solventTruncatedStrengthVolume;
	
	@XmlElement(required = true)
	private String finalVolume;
	
	@XmlElement(required = true)
	private String saltProperty;
	
	@XmlElement(required = true)
	private String formulaPrintName;
	
	@XmlElement(required = true)
	private String formCode;
	
	@XmlElement(required = true)
	private boolean vendSupplySolvFlag;
	
	@XmlElement(required = true)
	private String ratio;
	
	@XmlElement(required = true)
	private String diluentDesc;
	
	@XmlElement(required = true)
	private String solventDesc;
	
	@XmlElement(required = true)
	private String concn;
	
	@XmlElement(required = false)
	private String patNameChi;
	
	@XmlElement(required = false)
	private String patName;
	
	@XmlElement(required = false)
	private String caseNum;
	
	@XmlElement(required = false)
	private String ward;
	
	@XmlElement(required = false)
	private String bedNum;
	
	@XmlElement(required = false)
	private String batchNum;
	
	@XmlElement(required = false)
	private String hkid;
	
	@XmlElement(required = false)
	private String trxId;
	
	@XmlElement(required = false)
	private String orderDosage;
	
	@XmlElement(required = false)
	private String orderDosageSplit;
	
	@XmlElement(required = false)
	private String finalVolumeSplit;
	
	@XmlElement(required = false)
	private String freq;
	
	@XmlElement(required = false)
	private String supplFreq;
	
	@XmlElement(required = false)
	private String splitCount;
	
	@XmlElement(required = false)
	private boolean splitFlag;
	
	@XmlElement(required = false)
	private boolean shelfLifeZeroFlag;
	
	// save for PIVAS report
	@XmlElement(required = false)
	private String drugItemDesc;
	
	@XmlElement(required = false)
	private String solventItemDesc;
	
	@XmlElement(required = false)
	private String diluentItemDesc;
	
	@XmlElement(required = false)
	private Boolean refillFlag;
	
	@XmlElement(required = false)
	private Boolean adhocFlag;
	
	@XmlElement(required = false)
	private String issueQty;
	
	@XmlElement(required = false)
	private String adjQty;
	
	@XmlElement(required = false)
	private String drugCalQty;
	
	@XmlElement(required = false)
	private String drugDispQty;
	
	@XmlElement(required = false)
	private String drugBaseUnit;
	
	private transient PrintOption printOption;
	
	public PivasWorksheet() {
		splitFlag = Boolean.FALSE;
		shelfLifeZeroFlag = Boolean.FALSE;
		refillFlag = Boolean.FALSE;
		adhocFlag = Boolean.FALSE;
	}
	
	public String getDiluentDesc() {
		return diluentDesc;
	}
	public void setDiluentDesc(String diluentDesc) {
		this.diluentDesc = diluentDesc;
	}
	public String getSolventDesc() {
		return solventDesc;
	}
	public void setSolventDesc(String solventDesc) {
		this.solventDesc = solventDesc;
	}
	public String getRatio() {
		return ratio;
	}
	public void setRatio(String ratio) {
		this.ratio = ratio;
	}
	public String getPrintName() {
		return printName;
	}
	public void setPrintName(String printName) {
		this.printName = printName;
	}
	public String getDosageQty() {
		return dosageQty;
	}
	public void setDosageQty(String dosageQty) {
		this.dosageQty = dosageQty;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	public String getDiluentVolume() {
		return diluentVolume;
	}
	public void setDiluentVolume(String diluentVolume) {
		this.diluentVolume = diluentVolume;
	}
	public String getPivasDosageUnit() {
		return pivasDosageUnit;
	}
	public void setPivasDosageUnit(String pivasDosageUnit) {
		this.pivasDosageUnit = pivasDosageUnit;
	}
	public String getSolventVolume() {
		return solventVolume;
	}
	public void setSolventVolume(String solventVolume) {
		this.solventVolume = solventVolume;
	}
	public Date getManufactureDate() {
		return manufactureDate;
	}
	public void setManufactureDate(Date manufactureDate) {
		this.manufactureDate = manufactureDate;
	}
	public Date getPrepExpiryDate() {
		return prepExpiryDate;
	}
	public void setPrepExpiryDate(Date prepExpiryDate) {
		this.prepExpiryDate = prepExpiryDate;
	}
	public String getLotNum() {
		return lotNum;
	}
	public void setLotNum(String lotNum) {
		this.lotNum = lotNum;
	}
	public String getDrugItemCode() {
		return drugItemCode;
	}
	public void setDrugItemCode(String drugItemCode) {
		this.drugItemCode = drugItemCode;
	}
	public String getDrugVolume() {
		return drugVolume;
	}
	public void setDrugVolume(String drugVolume) {
		this.drugVolume = drugVolume;
	}
	public String getDrugManufCode() {
		return drugManufCode;
	}
	public void setDrugManufCode(String drugManufCode) {
		this.drugManufCode = drugManufCode;
	}
	public String getDrugBatchNum() {
		return drugBatchNum;
	}
	public void setDrugBatchNum(String drugBatchNum) {
		this.drugBatchNum = drugBatchNum;
	}
	public Date getDrugExpiryDate() {
		return drugExpiryDate;
	}
	public void setDrugExpiryDate(Date drugExpiryDate) {
		this.drugExpiryDate = drugExpiryDate;
	}
	public String getSolventItemCode() {
		return solventItemCode;
	}
	public void setSolventItemCode(String solventItemCode) {
		this.solventItemCode = solventItemCode;
	}
	public String getSolventManufCode() {
		return solventManufCode;
	}
	public void setSolventManufCode(String solventManufCode) {
		this.solventManufCode = solventManufCode;
	}
	public String getSolventBatchNum() {
		return solventBatchNum;
	}
	public void setSolventBatchNum(String solventBatchNum) {
		this.solventBatchNum = solventBatchNum;
	}
	public Date getSolventExpiryDate() {
		return solventExpiryDate;
	}
	public void setSolventExpiryDate(Date solventExpiryDate) {
		this.solventExpiryDate = solventExpiryDate;
	}
	public String getDiluentItemCode() {
		return diluentItemCode;
	}
	public void setDiluentItemCode(String diluentItemCode) {
		this.diluentItemCode = diluentItemCode;
	}
	public String getDiluentManufCode() {
		return diluentManufCode;
	}
	public void setDiluentManufCode(String diluentManufCode) {
		this.diluentManufCode = diluentManufCode;
	}
	public String getDiluentBatchNum() {
		return diluentBatchNum;
	}
	public void setDiluentBatchNum(String diluentBatchNum) {
		this.diluentBatchNum = diluentBatchNum;
	}
	public Date getDiluentExpiryDate() {
		return diluentExpiryDate;
	}
	public void setDiluentExpiryDate(Date diluentExpiryDate) {
		this.diluentExpiryDate = diluentExpiryDate;
	}
	public String getPrepQty() {
		return prepQty;
	}
	public void setPrepQty(String prepQty) {
		this.prepQty = prepQty;
	}
	public String getContainerName() {
		return containerName;
	}
	public void setContainerName(String containerName) {
		this.containerName = containerName;
	}
	public String getAfterConcn() {
		return afterConcn;
	}
	public void setAfterConcn(String afterConcn) {
		this.afterConcn = afterConcn;
	}
	public String getDrugStrength() {
		return drugStrength;
	}
	public void setDrugStrength(String drugStrength) {
		this.drugStrength = drugStrength;
	}
	public String getDrugVolumeValue() {
		return drugVolumeValue;
	}
	public void setDrugVolumeValue(String drugVolumeValue) {
		this.drugVolumeValue = drugVolumeValue;
	}
	public String getDrugVolumeUnit() {
		return drugVolumeUnit;
	}
	public void setDrugVolumeUnit(String drugVolumeUnit) {
		this.drugVolumeUnit = drugVolumeUnit;
	}
	public String getDrugTruncatedStrengthVolume() {
		return drugTruncatedStrengthVolume;
	}
	public void setDrugTruncatedStrengthVolume(String drugTruncatedStrengthVolume) {
		this.drugTruncatedStrengthVolume = drugTruncatedStrengthVolume;
	}
	public String getDiluentStrength() {
		return diluentStrength;
	}
	public void setDiluentStrength(String diluentStrength) {
		this.diluentStrength = diluentStrength;
	}
	public String getDiluentVolumeValue() {
		return diluentVolumeValue;
	}
	public void setDiluentVolumeValue(String diluentVolumeValue) {
		this.diluentVolumeValue = diluentVolumeValue;
	}
	public String getDiluentVolumeUnit() {
		return diluentVolumeUnit;
	}
	public void setDiluentVolumeUnit(String diluentVolumeUnit) {
		this.diluentVolumeUnit = diluentVolumeUnit;
	}
	public String getDiluentTruncatedStrengthVolume() {
		return diluentTruncatedStrengthVolume;
	}
	public void setDiluentTruncatedStrengthVolume(
			String diluentTruncatedStrengthVolume) {
		this.diluentTruncatedStrengthVolume = diluentTruncatedStrengthVolume;
	}
	public String getSolventStrength() {
		return solventStrength;
	}
	public void setSolventStrength(String solventStrength) {
		this.solventStrength = solventStrength;
	}
	public String getSolventVolumeValue() {
		return solventVolumeValue;
	}
	public void setSolventVolumeValue(String solventVolumeValue) {
		this.solventVolumeValue = solventVolumeValue;
	}
	public String getSolventVolumeUnit() {
		return solventVolumeUnit;
	}
	public void setSolventVolumeUnit(String solventVolumeUnit) {
		this.solventVolumeUnit = solventVolumeUnit;
	}
	public String getSolventTruncatedStrengthVolume() {
		return solventTruncatedStrengthVolume;
	}
	public void setSolventTruncatedStrengthVolume(
			String solventTruncatedStrengthVolume) {
		this.solventTruncatedStrengthVolume = solventTruncatedStrengthVolume;
	}
	public String getFinalVolume() {
		return finalVolume;
	}
	public void setFinalVolume(String finalVolume) {
		this.finalVolume = finalVolume;
	}
	public String getSiteDesc() {
		return siteDesc;
	}
	public void setSiteDesc(String siteDesc) {
		this.siteDesc = siteDesc;
	}
	public String getSaltProperty() {
		return saltProperty;
	}
	public void setSaltProperty(String saltProperty) {
		this.saltProperty = saltProperty;
	}
	public String getFormulaPrintName() {
		return formulaPrintName;
	}
	public void setFormulaPrintName(String formulaPrintName) {
		this.formulaPrintName = formulaPrintName;
	}
	public String getFormCode() {
		return formCode;
	}
	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}
	public boolean isVendSupplySolvFlag() {
		return vendSupplySolvFlag;
	}
	public void setVendSupplySolvFlag(boolean vendSupplySolvFlag) {
		this.vendSupplySolvFlag = vendSupplySolvFlag;
	}
	public String getConcn() {
		return concn;
	}
	public void setConcn(String concn) {
		this.concn = concn;
	}
	public String getCaseNum() {
		return caseNum;
	}
	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}
	public String getWard() {
		return ward;
	}
	public void setWard(String ward) {
		this.ward = ward;
	}
	public String getBatchNum() {
		return batchNum;
	}
	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}
	public String getHkid() {
		return hkid;
	}
	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public boolean isSplitFlag() {
		return splitFlag;
	}
	public void setSplitFlag(boolean splitFlag) {
		this.splitFlag = splitFlag;
	}
	public PrintOption getPrintOption() {
		return printOption;
	}
	public void setPrintOption(PrintOption printOption) {
		this.printOption = printOption;
	}
	public String getSplitCount() {
		return splitCount;
	}
	public void setSplitCount(String splitCount) {
		this.splitCount = splitCount;
	}
	public String getFinalVolumeSplit() {
		return finalVolumeSplit;
	}
	public void setFinalVolumeSplit(String finalVolumeSplit) {
		this.finalVolumeSplit = finalVolumeSplit;
	}
	public String getFreq() {
		return freq;
	}

	public void setFreq(String freq) {
		this.freq = freq;
	}

	public String getSupplFreq() {
		return supplFreq;
	}

	public void setSupplFreq(String supplFreq) {
		this.supplFreq = supplFreq;
	}

	public String getPatNameChi() {
		return patNameChi;
	}
	public void setPatNameChi(String patNameChi) {
		this.patNameChi = patNameChi;
	}
	public String getPatName() {
		return patName;
	}
	public void setPatName(String patName) {
		this.patName = patName;
	}
	public String getBedNum() {
		return bedNum;
	}
	public void setBedNum(String bedNum) {
		this.bedNum = bedNum;
	}
	public String getTrxId() {
		return trxId;
	}
	public void setTrxId(String trxId) {
		this.trxId = trxId;
	}
	public boolean isShelfLifeZeroFlag() {
		return shelfLifeZeroFlag;
	}
	public void setShelfLifeZeroFlag(boolean shelfLifeZeroFlag) {
		this.shelfLifeZeroFlag = shelfLifeZeroFlag;
	}
	public String getOrderDosage() {
		return orderDosage;
	}
	public void setOrderDosage(String orderDosage) {
		this.orderDosage = orderDosage;
	}
	public String getOrderDosageSplit() {
		return orderDosageSplit;
	}
	public void setOrderDosageSplit(String orderDosageSplit) {
		this.orderDosageSplit = orderDosageSplit;
	}
	public String getDrugItemDesc() {
		return drugItemDesc;
	}
	public void setDrugItemDesc(String drugItemDesc) {
		this.drugItemDesc = drugItemDesc;
	}
	public String getSolventItemDesc() {
		return solventItemDesc;
	}
	public void setSolventItemDesc(String solventItemDesc) {
		this.solventItemDesc = solventItemDesc;
	}
	public String getDiluentItemDesc() {
		return diluentItemDesc;
	}
	public void setDiluentItemDesc(String diluentItemDesc) {
		this.diluentItemDesc = diluentItemDesc;
	}
	public Boolean getRefillFlag() {
		return refillFlag;
	}
	public void setRefillFlag(Boolean refillFlag) {
		this.refillFlag = refillFlag;
	}
	public Boolean getAdhocFlag() {
		return adhocFlag;
	}
	public void setAdhocFlag(Boolean adhocFlag) {
		this.adhocFlag = adhocFlag;
	}
	public String getIssueQty() {
		return issueQty;
	}
	public void setIssueQty(String issueQty) {
		this.issueQty = issueQty;
	}
	public String getAdjQty() {
		return adjQty;
	}
	public void setAdjQty(String adjQty) {
		this.adjQty = adjQty;
	}
	public String getDrugCalQty() {
		return drugCalQty;
	}
	public void setDrugCalQty(String drugCalQty) {
		this.drugCalQty = drugCalQty;
	}
	public String getDrugDispQty() {
		return drugDispQty;
	}
	public void setDrugDispQty(String drugDispQty) {
		this.drugDispQty = drugDispQty;
	}
	public String getDrugBaseUnit() {
		return drugBaseUnit;
	}
	public void setDrugBaseUnit(String drugBaseUnit) {
		this.drugBaseUnit = drugBaseUnit;
	}
}
