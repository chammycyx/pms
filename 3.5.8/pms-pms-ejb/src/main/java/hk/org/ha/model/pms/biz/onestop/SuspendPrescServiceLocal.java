package hk.org.ha.model.pms.biz.onestop;

import hk.org.ha.model.pms.vo.charging.FcsPaymentStatus;

import javax.ejb.Local;

@Local
public interface SuspendPrescServiceLocal {
	
	String checkSuspendOrderStatus(Long dispOrderId, Long dispOrderVersion);
	
	void verifySuspendPrescription(Long dispOrderId, Long dispOrderVersion, String suspendCode, String caller);
	
	void reversePrescriptionSuspend(Long pharmOrderId, Long dispOrderVersion, String caller);
	
	void sfiPrescriptionSuspendForceProceed(Long dispOrderId, Long dispOrderVersion, String caller);
	
	void sfiPrescriptionSuspendDeferProceed(Long dispOrderId, Long dispOrderVersion, String caller);
	
	FcsPaymentStatus getFcsPaymentStatus();
	
	Boolean getSfiForceProceed();
	
	String getStatusErrorMsg();
		
	void destroy();
	
}
