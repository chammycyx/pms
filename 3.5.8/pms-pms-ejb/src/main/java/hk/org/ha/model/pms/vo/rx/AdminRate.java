package hk.org.ha.model.pms.vo.rx;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class AdminRate implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement(required = true)
	private String rateFormat;

	@XmlElement
	private Integer rateDuration;
	
	@XmlElement
	private String rateDurationUnit;
	
	@XmlElement
	private BigDecimal rateVolume;
	
	@XmlElement
	private String rateVolumeUnit;
	
	@XmlElement
	private String rateVolumePerUnit;

	public String getRateFormat() {
		return rateFormat;
	}

	public void setRateFormat(String rateFormat) {
		this.rateFormat = rateFormat;
	}

	public Integer getRateDuration() {
		return rateDuration;
	}

	public void setRateDuration(Integer rateDuration) {
		this.rateDuration = rateDuration;
	}

	public String getRateDurationUnit() {
		return rateDurationUnit;
	}

	public void setRateDurationUnit(String rateDurationUnit) {
		this.rateDurationUnit = rateDurationUnit;
	}

	public BigDecimal getRateVolume() {
		return rateVolume;
	}

	public void setRateVolume(BigDecimal rateVolume) {
		this.rateVolume = rateVolume;
	}

	public String getRateVolumeUnit() {
		return rateVolumeUnit;
	}

	public void setRateVolumeUnit(String rateVolumeUnit) {
		this.rateVolumeUnit = rateVolumeUnit;
	}

	public String getRateVolumePerUnit() {
		return rateVolumePerUnit;
	}

	public void setRateVolumePerUnit(String rateVolumePerUnit) {
		this.rateVolumePerUnit = rateVolumePerUnit;
	}	
}
