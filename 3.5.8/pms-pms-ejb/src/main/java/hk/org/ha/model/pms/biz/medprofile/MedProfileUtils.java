package hk.org.ha.model.pms.biz.medprofile;

import static hk.org.ha.model.pms.prop.Prop.DELIVERY_OVERDUEITEM_DURATION_LIMIT;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.PmsIpSpecMapping;
import hk.org.ha.model.pms.dms.vo.Capd;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
import hk.org.ha.model.pms.vo.rx.CapdItem;
import hk.org.ha.model.pms.vo.rx.CapdRxDrug;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.Fluid;
import hk.org.ha.model.pms.vo.rx.Freq;
import hk.org.ha.model.pms.vo.rx.InjectionRxDrug;
import hk.org.ha.model.pms.vo.rx.IvAdditive;
import hk.org.ha.model.pms.vo.rx.IvRxDrug;
import hk.org.ha.model.pms.vo.rx.Regimen;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.UUID;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.joda.time.DateMidnight;

public class MedProfileUtils {

	private static final Pattern CASE_NUM_PATTERN_1 = Pattern.compile("^[A-Za-z][A-Za-z][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][A-Za-z0-9]{0,2}$");
	private static final Pattern CASE_NUM_PATTERN_2 = Pattern.compile("^[A-Za-z][A-Za-z][A-Za-z0-9 ]{2}[0-9][0-9][0-9][0-9][0-9][0-9][0-9][A-Za-z0-9]?$");
	
	
	public static final String AT_ONCE_FREQ_CODE = "00034";
	public static final String ONCE_FREQ_CODE = "00035";
	public static final String STAT_FREQ_CODE = "00036";
	
	
	public static Date addDays(Date date, int days) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		cal.add(Calendar.DATE, days);
		
		return cal.getTime();
	}
	
	public static Date addHours(Date date, int hours) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		cal.add(Calendar.HOUR, hours);
		
		return cal.getTime();
	}
	
	public static Date addMinutes(Date date, int minutes) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		cal.add(Calendar.MINUTE, minutes);
		
		return cal.getTime();
	}
	
	public static Date trimSeconds(Date date) {

		if (date == null) {
			return null;
		}
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);		
		
		return cal.getTime();
	}
	
	public static Date getNextDayBegin() {
		return getNextDayBegin(new Date());
	}
	
	public static Date getNextDayBegin(Date date) {
		
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(date);
    	
    	cal.add(Calendar.DATE, 1);
    	cal.set(Calendar.HOUR_OF_DAY, 0);
    	cal.set(Calendar.MINUTE, 0);
    	cal.set(Calendar.SECOND, 0);
    	cal.set(Calendar.MILLISECOND, 0);
		
		return cal.getTime();
	}
	
	public static Date getTodayBegin() {
		return getDayBegin(null);
	}
	
	public static Date getDayBegin(Date date) {

    	Calendar cal = Calendar.getInstance();
    	
    	if (date != null) {
    		cal.setTime(date);
    	}
    	
    	cal.set(Calendar.HOUR_OF_DAY, 0);
    	cal.set(Calendar.MINUTE, 0);
    	cal.set(Calendar.SECOND, 0);
    	cal.set(Calendar.MILLISECOND, 0);
		
		return cal.getTime();
	}
	
	public static boolean isToday(Date date) {
		return isSameDay(new Date(), date);
	}
	
	public static boolean isSameDay(Date date1, Date date2) {
		
		if (date1 == null || date2 == null) {
			return false;
		}
		
		return new DateMidnight(date1).compareTo(new DateMidnight(date2)) == 0;
	}
	
	public static boolean isValidCaseNum(String caseNum) {
		return CASE_NUM_PATTERN_1.matcher(caseNum).find() || CASE_NUM_PATTERN_2.matcher(caseNum).find();
	}
	
	public static Date combineDateTime(Date date, Date time) {

		Calendar dateCal = Calendar.getInstance();
		dateCal.setTime(date);
		
		Calendar dateTimeCal = Calendar.getInstance();
		dateTimeCal.setTime(time);
		dateTimeCal.set(dateCal.get(Calendar.YEAR), dateCal.get(Calendar.MONTH), dateCal.get(Calendar.DATE));
		
		return dateTimeCal.getTime();
	}
	
    public static void lookup(Object obj, String path) {
    	lookup(obj, nextTokenPos(path, 0), path);
    }
    
	private static void lookup(Object obj, int pos, String path) {

		if (obj instanceof Collection<?>) {
			
			Collection<?> collection = (Collection<?>) obj;
			for (Object child: collection) {
				lookup(child, pos, path);
			}
		} else if (obj != null && pos != -1) {
			
			try {
				int newPos = nextTokenPos(path, pos);
	    		String pathToken = newPos == -1 ? path.substring(pos) : path.substring(pos, newPos - 1);
    			Method method = obj.getClass().getMethod("get" + StringUtils.capitalize(pathToken));
    			Object child = method.invoke(obj);
				lookup(child, newPos, path);
    		} catch (Exception ex) {
    		}
		}
    }
    
    private static int nextTokenPos(String path, int startPos) {
    	int nextPos = path.indexOf(".", startPos);
    	if (nextPos != -1) {
    		nextPos++;
    	}
    	return nextPos;
    }
    
    public static Date getOverdueStartDate(Date now) {
    	Calendar startCal = Calendar.getInstance();
    	startCal.setTime(now);
    	startCal.add(Calendar.DAY_OF_MONTH, DELIVERY_OVERDUEITEM_DURATION_LIMIT.get(30).intValue() * -1);
    	return startCal.getTime();
    }

    public static boolean isManualProfile(String orderNum) {
    	return isManualProfile(orderNum, true);
    }
    
    public static boolean isManualProfile(String orderNum, boolean def) {
    	return orderNum == null ? def : orderNum.length() > 12;
    }
    
    public static List<Long> constructMedProfileIdList(List<MedProfile> medProfileList) {
    	List<Long> idList = new ArrayList<Long>();
    	for (MedProfile medProfile: medProfileList) {
    		idList.add(medProfile.getId());
    	}
    	return idList;
    }
    
    public static void markMealFlag(Workstore workstore, List<MedProfileItem> medProfileItemList) {
    	
    	if (medProfileItemList == null) {
    		return;
    	}
    	
    	for (MedProfileItem medProfileItem : medProfileItemList) {
    		markMealFlag(workstore, medProfileItem.getMedProfileMoItem());
    	}
    }
    
    public static void markMealFlag(Workstore workstore, MedProfileMoItem medProfileMoItem) {

    	RxItem rxItem = medProfileMoItem.getRxItem();
    	
    	boolean mealFlag = false;
    	
		if (rxItem instanceof RxDrug) {
			mealFlag = calculateMealFlag(workstore, (RxDrug) rxItem);
		} else if (rxItem instanceof IvRxDrug) {
			mealFlag = calculateMealFlag(workstore, (IvRxDrug) rxItem);
		}
		medProfileMoItem.setMealFlag(mealFlag);
    }
    
    public static boolean calculateMealFlag(Workstore workstore, RxDrug rxDrug) {

		List<DmDrug> dmDrugList = DmDrugCacher.instance().getDmDrugListByDisplayNameFormSaltFmStatus(workstore,
				rxDrug.getDisplayName(), rxDrug.getFormCode(), rxDrug.getSaltProperty(), rxDrug.getFmStatus());
		
		Regimen regimen = rxDrug.getRegimen();
		
		if (dmDrugList.isEmpty() || regimen == null || regimen.getDoseGroupList() == null) {
			return false;
		}
			
		DmDrug dmDrug = dmDrugList.get(0);
		
		for (DoseGroup doseGroup: regimen.getDoseGroupList()) {
			
			if (doseGroup.getDoseList() == null) {
				continue;
			}
			
			for (Dose dose: doseGroup.getDoseList()) {
				if (dose.getAdminTimeCode() != null && dmDrug.getDmDrugProperty() != null &&
						!dose.getAdminTimeCode().equals(dmDrug.getDmDrugProperty().getAdminTimeCode())) {
					return true;
				}
			}
		}
		
		return false;
    }
    
    public static boolean calculateMealFlag(Workstore workstore, IvRxDrug ivRxDrug) {
    	
    	if (ivRxDrug == null || ivRxDrug.getIvAdditiveList() == null) {
    		return false;
    	}
    		
    	for (IvAdditive ivAdditive: ivRxDrug.getIvAdditiveList()) {
    		
    		List<DmDrug> dmDrugList = DmDrugCacher.instance().getDmDrugListByDisplayNameFormSaltFmStatus(workstore,
    				ivAdditive.getDisplayName(), ivAdditive.getFormCode(), ivAdditive.getSaltProperty(), ivAdditive.getFmStatus());

    		if (dmDrugList.isEmpty()) {
    			return false;
    		}
    			
    		DmDrug dmDrug = dmDrugList.get(0);
    		
			if (ivRxDrug.getAdminTimeCode() != null && dmDrug.getDmDrugProperty() != null &&
					!ivRxDrug.getAdminTimeCode().equals(dmDrug.getDmDrugProperty().getAdminTimeCode())) {
				return true;
			}
    	}
    	
    	return false;
    }
    
	public static boolean isStatDose(Regimen regimen) {
		Freq freq = regimen.getFirstDose().getDailyFreq();
		return freq != null && STAT_FREQ_CODE.equals(freq.getCode());
	}
	
	public static boolean isOnceDose(Regimen regimen) {
		Freq freq = regimen.getFirstDose().getDailyFreq();
		return freq != null && ONCE_FREQ_CODE.equals(freq.getCode());
	}
	
	public static boolean isAtOnceDose(Regimen regimen) {
		Freq freq = regimen.getFirstDose().getDailyFreq();
		return freq != null && AT_ONCE_FREQ_CODE.equals(freq.getCode());
	}
	
	public static MedCase lookupPasMedCase(Patient patient, String caseNum) {
		
		if (patient == null) {
			return null;
		}
		
		for (MedCase medCase: patient.getMedCaseList()) {
			if (caseNum.equals(medCase.getCaseNum())) {
				return medCase;
			}
		}
		return null;
	}
	
	public static void loadDmInfoAndInitUuidAndNewlyConvertedPoItem(List<MedProfileItem> medProfileItemList) {
		
		for (MedProfileItem medProfileItem: medProfileItemList) {
			MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
			medProfileMoItem.loadDmInfo();
			for (MedProfilePoItem medProfilePoItem: medProfileItem.getMedProfileMoItem().getMedProfilePoItemList()) {
    			medProfilePoItem.loadDmInfo();
    			medProfilePoItem.setUuid(UUID.randomUUID().toString());
    			if (medProfileItem.getStatus() == MedProfileItemStatus.Unvet && medProfileMoItem.getChemoInfo() != null && medProfileMoItem.getChemoInfo().getChemoItemList().size() > 1
    					&& !medProfileMoItem.isTreatmentDaySelected()) {
    				medProfilePoItem.setDoseQty(BigDecimal.ZERO);
    				medProfilePoItem.setRefillDuration(null);
    				medProfilePoItem.setIssueQty(BigDecimal.ZERO);
    			}
			}
		}
	}
	
	public static boolean equalDrugs(RxItem rxItem1, RxItem rxItem2, boolean strictCompare) {
		
		if (strictCompare && rxItem1 instanceof InjectionRxDrug && rxItem2 instanceof InjectionRxDrug) {
			return equalDrugs((InjectionRxDrug) rxItem1, (InjectionRxDrug) rxItem2);
		} else if (rxItem1 instanceof RxDrug && rxItem2 instanceof RxDrug) {
			return equalDrugs((RxDrug) rxItem1, (RxDrug) rxItem2, strictCompare);
		} else if (rxItem1 instanceof IvRxDrug && rxItem2 instanceof IvRxDrug) {
			return equalDrugs((IvRxDrug) rxItem1, (IvRxDrug) rxItem2, strictCompare);
		} else if (rxItem1 instanceof CapdRxDrug && rxItem2 instanceof CapdRxDrug) {
			return equalDrugs((CapdRxDrug) rxItem1, (CapdRxDrug) rxItem2);
		} else {
			return false;
		}
	}
	
	private static boolean equalDrugs(InjectionRxDrug rxDrug1, InjectionRxDrug rxDrug2) {
		
		return equalDrugs((RxDrug) rxDrug1, (RxDrug) rxDrug2, true) &&
			equalDrugFluidCapdItemLists(extractFluidList(rxDrug1), extractFluidList(rxDrug2));
	}
    
    private static boolean equalDrugs(IvRxDrug rxDrug1, IvRxDrug rxDrug2, boolean strictCompare) {
    	
    	if (rxDrug1.getIvAdditiveList().size() != rxDrug2.getIvAdditiveList().size() ||
    			rxDrug1.getIvFluidList().size() != rxDrug2.getIvFluidList().size()) {
    		return false;
    	}
    	
		if (strictCompare) {
			if (!equalDrugFluidCapdItemLists(rxDrug1.getIvAdditiveList(), rxDrug2.getIvAdditiveList())) {
				return false;
			}
		} else {
	    	for (int i = 0; i < rxDrug1.getIvAdditiveList().size(); i++) {
				if (!equalDrugs(rxDrug1.getIvAdditiveList().get(i), rxDrug2.getIvAdditiveList().get(i), strictCompare)) {
					return false;
				}
	    	}
		}
		
		if (strictCompare) {
			if (!equalDrugFluidCapdItemLists(rxDrug2.getIvFluidList(), rxDrug1.getIvFluidList())) {
				return false;
			}
		} else {
	    	for (int i = 0; i < rxDrug1.getIvFluidList().size(); i++) {
				if (!equalFluids(rxDrug1.getIvFluidList().get(i), rxDrug2.getIvFluidList().get(i))) {
					return false;
				}
	    	}
		}
    	return true;
    }
    
    private static boolean equalDrugs(CapdRxDrug rxDrug1, CapdRxDrug rxDrug2) {
    	
    	if (compare(rxDrug1.getSupplierSystem(), rxDrug2.getSupplierSystem()) != 0 ||
    		compare(rxDrug1.getCalciumStrength(), rxDrug2.getCalciumStrength()) != 0) {
    		return false;
    	}

    	return equalDrugFluidCapdItemLists(rxDrug1.getCapdItemList(), rxDrug2.getCapdItemList());
    }
    
    private static boolean equalDrugFluidCapdItemLists(List<? extends Object> list1, List<? extends Object> list2) {
    	
    	if (list1.size() != list2.size()) {
    		return false;
    	}
    	
    	List<Object> clonedRxDrugList = new ArrayList<Object>();
    	clonedRxDrugList.addAll(list2);
    	
    	for (Object item1: list1) {
    		Iterator<Object> iter = clonedRxDrugList.iterator();
    		boolean matched = false;
    		while (iter.hasNext()) {
    			Object item2 = iter.next();
    			if (equalDrugFluidCapdItems(item1, item2)) {
    				matched = true;
    				iter.remove();
    				break;
    			}
    		}
    		if (!matched) {
    			return false;
    		}
    	}
    	return true;
    }
    
    private static boolean equalDrugs(RxDrug rxDrug1, RxDrug rxDrug2, boolean strictCompare) {
    	
    	return compare(rxDrug1.getDisplayName(), rxDrug2.getDisplayName()) == 0 &&
    		compare(rxDrug1.getFormCode(), rxDrug2.getFormCode()) == 0 &&
    		compare(rxDrug1.getSaltProperty(), rxDrug2.getSaltProperty()) == 0 &&
    		(!strictCompare || 
    		(compare(rxDrug1.getStrength(), rxDrug2.getStrength()) == 0 &&
    		compare(rxDrug1.getVolume(), rxDrug2.getVolume()) == 0 &&
    		compare(rxDrug1.getVolumeUnit(), rxDrug2.getVolumeUnit()) == 0));
    }
    
    private static boolean equalFluids(Fluid fluid1, Fluid fluid2) {
    	return (fluid1.getDiluentCode() == null && fluid2.getDiluentCode() == null) || 
    		compare(fluid1.getDiluentName(), fluid2.getDiluentName()) == 0;
    }
    
    private static boolean equalCapdItems(CapdItem item1, CapdItem item2) {
    	return equalCapds(item1.getCapd(), item2.getCapd());
    }
    
    private static boolean equalCapds(Capd capd1, Capd capd2) {
    	
    	if (capd1 == null || capd2 == null) {
    		return capd1 == capd2;
    	}
    	
    	return compare(capd1.getIngredient(), capd2.getIngredient()) == 0 &&
			compare(capd1.getStrengthValue(), capd2.getStrengthValue()) == 0 &&
			compare(capd1.getStrengthUnit(), capd2.getStrengthUnit()) == 0 &&
			compare(capd1.getVolumeValue(), capd2.getVolumeValue()) == 0 &&
			compare(capd1.getVolumeUnit(), capd2.getVolumeUnit()) == 0;
    }
    
    private static boolean equalDrugFluidCapdItems(Object o1, Object o2) {
    
    	if (o1 instanceof Fluid && o2 instanceof Fluid) {
    		return equalFluids((Fluid) o1, (Fluid) o2);
    	} else if (o1 instanceof RxDrug && o2 instanceof RxDrug) {
    		return equalDrugs((RxDrug) o1, (RxDrug) o2, true);
    	} else if (o1 instanceof CapdItem && o2 instanceof CapdItem) {
    		return equalCapdItems((CapdItem) o1, (CapdItem) o2);
    	}
    	return false;
    }
    
    private static List<Fluid> extractFluidList(RxDrug rxDrug) {
    	
    	List<Fluid> fluidList = new ArrayList<Fluid>();
    	
    	Regimen regimen = rxDrug.getRegimen();
    	if (regimen == null) {
    		return fluidList;
    	}
    	
    	for (DoseGroup doseGroup: regimen.getDoseGroupList()) {
    		for (Dose dose: doseGroup.getDoseList()) {
    			Fluid doseFluid = dose.getDoseFluid();
    			if (doseFluid != null) {
    				fluidList.add(doseFluid);
    			}
    		}
    	}
    	return fluidList;
    }
    
    public static Date extractStartDate(RxItem rxItem) {
    	return extractStartDate(rxItem, false);
    }
    
	public static Date extractStartDate(RxItem rxItem, boolean original) {
		
		if (rxItem instanceof RxDrug) {
			return extractStartDate((RxDrug) rxItem, original);
		} else if (rxItem instanceof IvRxDrug) {
			return extractStartDate((IvRxDrug) rxItem, original);
		} else if (rxItem instanceof CapdRxDrug) {
			return extractStartDate((CapdRxDrug) rxItem, original);
		}
		return null;
	}
	
	private static Date extractStartDate(RxDrug rxDrug, boolean original) {
		return rxDrug.getRegimen() == null || rxDrug.getRegimen().getDoseGroupList().isEmpty() ? null :
			(original ? rxDrug.getRegimen().getDoseGroupList().get(0).getOrgStartDate() : rxDrug.getRegimen().getDoseGroupList().get(0).getStartDate());
	}
	
	private static Date extractStartDate(IvRxDrug ivRxDrug, boolean original) {
		return original ? ivRxDrug.getOrgStartDate() : ivRxDrug.getStartDate();
	}
	
	private static Date extractStartDate(CapdRxDrug capdRxDrug, boolean original) {
		
		Date result = null;
		for (CapdItem capdItem: capdRxDrug.getCapdItemList()) {
			Date startDate = original ? capdItem.getOrgStartDate() : capdItem.getStartDate();
			if (compare(result, startDate) > 0) {
				result = startDate;
			}
		}
		return result;
	}
	
	public static void updateStartDate(Regimen regimen, Date startDate) {
		
		if (regimen == null) {
			return;
		}
		
		for (DoseGroup doseGroup: regimen.getDoseGroupList()) {
			doseGroup.setStartDate(startDate);
		}
	}
	
	public static Map<Pair<String, String>, Set<String>> buildPasSpecialtyMap(List<PmsIpSpecMapping> pmsIpSpecMappingList,
			boolean includeDeleteRecord) {
		
		Map<Pair<String, String>, Set<String>> pasSpecialtyMap = new TreeMap<Pair<String, String>, Set<String>>();
		
		for (PmsIpSpecMapping pmsIpSpecMapping: pmsIpSpecMappingList) {
			
			if (!includeDeleteRecord && pmsIpSpecMapping.isMarkDelete()) {
				continue;
			}
			Pair<String, String> key = Pair.of(pmsIpSpecMapping.getPatHospCode(), pmsIpSpecMapping.getWard());
			Set<String> pasSpecCodeSet = pasSpecialtyMap.get(key);
			if (pasSpecCodeSet == null) {
				pasSpecCodeSet = new TreeSet<String>();
				pasSpecialtyMap.put(key, pasSpecCodeSet);
			}
			pasSpecCodeSet.add(pmsIpSpecMapping.getPasSpecialty());
		}
		return pasSpecialtyMap; 
	}
    
    public static boolean isEmpty(String str) {
    	return str == null || str.isEmpty();
    }
    
    public static <E> E getFirstItem(List<E> itemList) {
    	
    	if (itemList == null || itemList.isEmpty()) {
    		return null;
    	} else {
    		return itemList.get(0);
    	}
    }
    
    public static <E> E getLastItem(List<E> itemList) {

    	if (itemList == null || itemList.isEmpty()) {
    		return null;
    	} else {
    		return itemList.get(itemList.size() - 1);
    	}
    }
    
    public static <E extends Comparable<E>> int compare(E c1, E c2) {
    	if (c1 == c2) {
    		return 0;
    	} else if (c1 == null) {
    		return -1;
    	} else if (c2 == null) {
    		return 1;
    	} else {
    		return c1.compareTo(c2);
    	}
    }
    
    public static <E extends Comparable<E>> E max(E c1, E c2) {
    	return compare(c1, c2) > 0 ? c1 : c2;
    }

    public static <E extends Comparable<E>> E min(E c1, E c2) {
    	return compare(c1, c2) < 0 ? c1 : c2;
    }
    
    public static <E extends Comparable<E>> boolean equalsButNotNull(E c1, E c2) {
    	return c1 != null && compare(c1, c2) == 0;
    }
    
    public static int compareString(String s1, String s2) {
    	return compare(s1 == null ? "" : s1, s2 == null ? "" : s2);
    }
    
	public static boolean isValidNonZeroValue(Integer integer) {
		return isValidNonZeroValue(integer , Integer.valueOf(0));
	}
	
	public static boolean isValidNonZeroValue(BigDecimal bigDecimal) {
		return isValidNonZeroValue(bigDecimal , BigDecimal.ZERO);
	}
	
	public static <E extends Comparable<E>> boolean isValidNonZeroValue(E value, E zero) {
		return value != null && value.compareTo(zero) != 0;
	}
	
	public static BigDecimal add(BigDecimal n1, BigDecimal n2) {
		return add(n1, n2, 0);
	}
	
	public static BigDecimal add(BigDecimal n1, BigDecimal n2, int defaultScale) {
		
		BigDecimal result = BigDecimal.valueOf(0, defaultScale);
		if (n1 != null) {
			result = result.add(n1);
		}
		if (n2 != null) {
			result = result.add(n2);
		}
		return result;
	}	
	
	public static String join(Object... args) {
		return StringUtils.join(args, ",");
	}
	
	public static <E> void append(Collection<E> collection, E element) {
		if (element != null) {
			collection.add(element);
		}
	}
}
