package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DispOrderDayEndStatus implements StringValuedEnum {

	None("-", "None"),
	Completed("C", "Completed"),
	Uncollected("U", "Uncollected");
	
    private final String dataValue;
    private final String displayValue;

    DispOrderDayEndStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }  
    
    public static DispOrderDayEndStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DispOrderDayEndStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<DispOrderDayEndStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<DispOrderDayEndStatus> getEnumClass() {
    		return DispOrderDayEndStatus.class;
    	}
    }
}
