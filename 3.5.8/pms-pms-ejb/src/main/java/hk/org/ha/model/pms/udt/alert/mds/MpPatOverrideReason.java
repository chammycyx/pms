package hk.org.ha.model.pms.udt.alert.mds;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

import java.util.Arrays;
import java.util.List;

public enum MpPatOverrideReason implements StringValuedEnum {

	NA("NA", "No other alternative available"),
	DE("DE", "Desensitizing the patient with this drug"),
	DO("DO", "Doubts about the reported history of drug allergy"),
	AL("AL", "Patient is taking this drug without allergic response"),
	AD("AD", "Patient can tolerate the adverse drug reaction without any problem"),
	TO("TO", "Patient can tolerate the drug at lower dose"),
	CD("CD", "Confirmed with doctor (Enter doctor's name in Remarks)"),
	NT("NT", "Not true allergy"),
	OJ("OJ", "Overridden by pharmacist's judgment");
	
	public static final List<MpPatOverrideReason> mpPatOverrideReasonList = Arrays.asList(NA, DE, DO, AL, AD, TO, CD, NT, OJ);
	
	private final String dataValue;
    private final String displayValue;

    MpPatOverrideReason(final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
    
	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}

	public static MpPatOverrideReason dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MpPatOverrideReason.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<MpPatOverrideReason> {
		
		private static final long serialVersionUID = 1L;

		public Class<MpPatOverrideReason> getEnumClass() {
    		return MpPatOverrideReason.class;
    	}
    }
}
