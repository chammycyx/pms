package hk.org.ha.model.pms.persistence.medprofile;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.medprofile.EndType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PostLoad;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.lang3.ObjectUtils;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "MED_PROFILE_ITEM")
public class MedProfileItem extends VersionEntity {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "medProfileItemSeq")
	@SequenceGenerator(name = "medProfileItemSeq", sequenceName = "SQ_MED_PROFILE_ITEM", initialValue = 100000000)
	private Long id;
	
	@Column(name = "ORG_ITEM_NUM", nullable = false)
	private Integer orgItemNum;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ADMIN_DATE")
	private Date adminDate;

    @Converter(name = "MedProfileItem.status", converterClass = MedProfileItemStatus.Converter.class)
    @Convert("MedProfileItem.status")
	@Column(name = "STATUS", nullable = false, length = 1)
	private MedProfileItemStatus status;
    
    @Transient
    private MedProfileItemStatus orgStatus;
    
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "STATUS_UPDATE_DATE")
    private Date statusUpdateDate;

    @Converter(name = "MedProfileItem.endType", converterClass = EndType.Converter.class)
    @Convert("MedProfileItem.endType")
	@Column(name = "END_TYPE", length = 1)
	private EndType endType;
    
	@Column(name = "REPLENISH_FLAG", nullable = false)
	private Boolean replenishFlag;
	
	@Column(name = "AMEND_FLAG", nullable = false)
	private Boolean amendFlag;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "VERIFY_DATE")
    private Date verifyDate;
    
    @Column(name = "VERIFY_USER", length = 12)
    private String verifyUser;

    @Column(name = "DELIVERY_GEN_FLAG", nullable = false)
    private Boolean deliveryGenFlag;
    
    @Column(name = "GROUP_NUM")
	private Integer groupNum;
    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MED_PROFILE_ID", nullable = false)
	private MedProfile medProfile;
	
	@Column(name = "MED_PROFILE_ID", nullable = false, insertable = false, updatable = false)
	private Long medProfileId;	
	
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MED_PROFILE_MO_ITEM_ID")
    private MedProfileMoItem medProfileMoItem;
    
	@Column(name = "MED_PROFILE_MO_ITEM_ID", insertable = false, updatable = false)
	private Long medProfileMoItemId;	
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MED_PROFILE_ERROR_LOG_ID")
    private MedProfileErrorLog medProfileErrorLog;
        
	@Transient
	private Boolean modified;
	
	@Transient
	private Integer verifyIndex;
	
	@Transient
	private Integer newOrgItemNum;
	
	@Transient
	private Boolean discontModified;
	
	private transient boolean errorLogged;
	
    public MedProfileItem() {
    	replenishFlag = Boolean.FALSE;
    	amendFlag = Boolean.FALSE;
    	status = MedProfileItemStatus.Unvet;
    	orgStatus = status;
    	modified = Boolean.FALSE;
    	deliveryGenFlag = Boolean.FALSE;
    }
    
	@PostLoad
	public void postLoad() {
		orgStatus = status;
	}
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getOrgItemNum() {
		return orgItemNum;
	}

	public void setOrgItemNum(Integer orgItemNum) {
		this.orgItemNum = orgItemNum;
	}

	public Date getAdminDate() {
		return adminDate == null ? null : new Date(adminDate.getTime());
	}

	public void setAdminDate(Date adminDate) {
		this.adminDate = adminDate == null ? null : new Date(adminDate.getTime());
	}

	public MedProfileItemStatus getStatus() {
		return status;
	}

	public void setStatus(MedProfileItemStatus status) {
		this.status = status;
	}
	
	public MedProfileItemStatus getOrgStatus() {
		return orgStatus;
	}

	public void setOrgStatus(MedProfileItemStatus orgStatus) {
		this.orgStatus = orgStatus;
	}

	public Date getStatusUpdateDate() {
		return statusUpdateDate;
	}

	public void setStatusUpdateDate(Date statusUpdateDate) {
		this.statusUpdateDate = statusUpdateDate;
	}
	
	public EndType getEndType() {
		return endType;
	}

	public void setEndType(EndType endType) {
		this.endType = endType;
	}

	public Boolean getReplenishFlag() {
		return replenishFlag;
	}

	public void setReplenishFlag(Boolean replenishFlag) {
		this.replenishFlag = replenishFlag;
	}

	public Boolean getAmendFlag() {
		return amendFlag;
	}

	public void setAmendFlag(Boolean amendFlag) {
		this.amendFlag = amendFlag;
	}

	public Date getVerifyDate() {
		return verifyDate == null ? null : new Date(verifyDate.getTime());
	}

	public void setVerifyDate(Date verifyDate) {
		this.verifyDate = verifyDate == null ? null : new Date(verifyDate.getTime());
	}
	
	public Boolean getDeliveryGenFlag() {
		return deliveryGenFlag;
	}
	
	public void setDeliveryGenFlag(Boolean deliveryGenFlag) {
		this.deliveryGenFlag = deliveryGenFlag;
	}

	public void setGroupNum(Integer groupNum) {
		this.groupNum = groupNum;
	}

	public Integer getGroupNum() {
		return groupNum;
	}

	public MedProfile getMedProfile() {
		return medProfile;
	}

	public void setMedProfile(MedProfile medProfile) {
		this.medProfile = medProfile;
	}

	public Long getMedProfileId() {
		return medProfileId;
	}

	public void setMedProfileId(Long medProfileId) {
		this.medProfileId = medProfileId;
	}

	public MedProfileMoItem getMedProfileMoItem() {
		return medProfileMoItem;
	}

	public void setMedProfileMoItem(MedProfileMoItem medProfileMoItem) {
		this.medProfileMoItem = medProfileMoItem;
	}
	
	public Long getMedProfileMoItemId() {
		return medProfileMoItemId;
	}

	public void setMedProfileMoItemId(Long medProfileMoItemId) {
		this.medProfileMoItemId = medProfileMoItemId;
	}

	public MedProfileErrorLog getMedProfileErrorLog() {
		return medProfileErrorLog;
	}

	public void setMedProfileErrorLog(MedProfileErrorLog medProfileErrorLog) {
		this.medProfileErrorLog = medProfileErrorLog;
	}

	public Boolean getModified() {
		return modified;
	}

	public void setModified(Boolean modified) {
		this.modified = modified;
	}
	
	public Integer getVerifyIndex() {
		return verifyIndex;
	}

	public void setVerifyIndex(Integer verifyIndex) {
		this.verifyIndex = verifyIndex;
	}

	public void setNewOrgItemNum(Integer newOrgItemNum) {
		this.newOrgItemNum = newOrgItemNum;
	}

	public Integer getNewOrgItemNum() {
		return newOrgItemNum;
	}

	public void setDiscontModified(Boolean discontModified) {
		this.discontModified = discontModified;
	}

	public Boolean getDiscontModified() {
		return discontModified;
	}

	@Transient
	public boolean isUnvetAndNotAdminYet() {
		return status == MedProfileItemStatus.Unvet && adminDate == null;
	}
	
	@Transient
	public boolean isComplete(Date now) {
		return !MedProfileItemStatus.Unvet_Vetted_Verified.contains(status) || ObjectUtils.compare(getMedProfileMoItem().getEndDate(), now, true) < 0; 
	}
	
	@Transient
	public boolean isInactive(Date now) {
		return isComplete(now) || Boolean.TRUE.equals(getMedProfileMoItem().getTransferFlag());
	}
	
	public void hideErrorStat() {
		if (medProfileErrorLog != null) {
			medProfileErrorLog.setHideErrorStatFlag(Boolean.TRUE);
		}
	}

	public void setVerifyUser(String verifyUser) {
		this.verifyUser = verifyUser;
	}

	public String getVerifyUser() {
		return verifyUser;
	}

	public boolean isErrorLogged() {
		return errorLogged;
	}

	public void setErrorLogged(boolean errorLogged) {
		this.errorLogged = errorLogged;
	}
}
