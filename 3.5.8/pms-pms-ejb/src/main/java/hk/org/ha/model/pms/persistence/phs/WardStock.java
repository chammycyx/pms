package hk.org.ha.model.pms.persistence.phs;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.RecordStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "WARD_STOCK")
@IdClass(WardStockPK.class)
public class WardStock extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "INST_CODE", nullable = false)
	private String instCode;

	@Id
	@Column(name = "WARD_CODE", nullable = false, length = 4)
	private String wardCode;
	
	@Id
	@Column(name = "ITEM_CODE", nullable = false, length = 6)
	private String itemCode;
	
    @Converter(name = "WardStock.status", converterClass = RecordStatus.Converter.class)
    @Convert("WardStock.status")
	@Column(name="STATUS", nullable = false, length = 1)
	private RecordStatus status;

    @ManyToOne
    @JoinColumns({
    	@JoinColumn(name = "INST_CODE", referencedColumnName = "INST_CODE", nullable = false, insertable = false, updatable = false),
    	@JoinColumn(name = "WARD_CODE", referencedColumnName = "WARD_CODE", nullable = false, insertable = false, updatable = false)
    })
    private Ward ward;

	public WardStock() {
    	status = RecordStatus.Active;
    }
	
	public WardStock(String instCode, String wardCode, String itemCode) {
		this();
		this.instCode = instCode;
		this.wardCode = wardCode;
		this.itemCode = itemCode;
	}
	    
    public String getInstCode() {
		return instCode;
	}

	public void setInstCode(String instCode) {
		this.instCode = instCode;
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}
	
	public String getItemCode() {
		return itemCode;
	}
	
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public RecordStatus getStatus() {
		return status;
	}

	public void setStatus(RecordStatus status) {
		this.status = status;
	}

	public Ward getWard() {
		return ward;
	}
	
	public WardStockPK getId() {
		return new WardStockPK(instCode, wardCode, itemCode);
	}
	
	@Override
	public int hashCode() {		
		return new HashCodeBuilder()
			.append(instCode)
			.append(wardCode)
			.append(itemCode)
			.toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		WardStock other = (WardStock) obj;
		return new EqualsBuilder()
			.append(instCode, other.getInstCode())
			.append(wardCode, other.getWardCode())
			.append(itemCode, other.getItemCode())
			.isEquals();
	}	
}
