package hk.org.ha.model.pms.biz.order;

import java.util.List;
import java.util.Set;

import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;

import javax.ejb.Local;

@Local
public interface OrderManagerLocal  {

	MedOrder retrieveMedOrder(String orderNum);

	MedOrder retrieveMedOrder(String orderNum, List<MedOrderStatus> excludeStatusList);
	
	void lockMedOrder(String orderNum);
	
	Ticket saveTicket(Ticket remoteTicket);

	MedCase saveMedCase(MedCase remoteMedCase);

	Patient savePatient(Patient remotePatient);
	
	MedOrder insertMedOrder(MedOrder medOrder);
	
	MedOrder insertMedOrderByUnvet(MedOrder medOrder);
	
	List<DispOrder> markOrderSysDeleted(PharmOrder pharmOrder, Long prevPharmOrderId, Boolean remarkFlag);
	
	void loadPharmOrderList(MedOrder medOrder);
	
	void loadDispOrderList(PharmOrder pharmOrder);

	void loadOrderList(MedOrder medOrder);	

	List<MedOrderItem> retrieveMedOrderItemByItemNum(String orderNum, List<Integer> medOrderItemNumList);
	
	Set<DispOrder> retrieveDispOrderList(List<Invoice> voidInvoiceList);
}
