package hk.org.ha.model.pms.biz.order;

import hk.org.ha.model.pms.corp.vo.EndVettingResult;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

@Local
public interface DispOrderManagerLocal {
	
	DispOrder retrieveDispOrderByTicket(Date ticketDate, String ticketNum, Workstore workstore);
	
	void retrieveRefillScheduleList(DispOrder dispOrder);
	
	List<MedOrderItem> retrieveMedOrderItemList(DispOrder dispOrder);
	
	List<MedOrderItem> retrievePrescItemListForOneStop(Date ticketDate, String ticketNum, String workstoreCode);		

	
	DispOrder retrieveDispOrderById(Long id);
	
	DispOrder createDispOrderFromPharmOrderForLabel(PharmOrder pharmOrder);
	
	DispOrder createDispOrderFromPharmOrder(PharmOrder pharmOrder, Ticket ticket);
	
	DispOrder createDispOrderForSfiRefill(List<RefillSchedule> inRefillScheduleList, Ticket ticket);
	
	DispOrder createDispOrderForRefill(RefillSchedule refillSchedule, Ticket ticket);
	
	DispOrder createDispOrderForInitialRefill(PharmOrder pharmOrder, List<RefillSchedule> refillScheduleList, Ticket ticket);
	
	void updateDispOrderByPrevDispOrder(DispOrder dispOrder, DispOrder prevDispOrder);
	
	Map<Long, Boolean> updateDispOrderLargeLayoutFlag(DispOrder dispOrder);

	void prepareDispOrderPrintJob(List<RenderAndPrintJob> printList, DispOrder dispOrder);

	void printDispOrder(DispOrder dispOrder, boolean printCapdVoucher);
	
	void endDispensing(DispOrder dispOrder, Invoice sfiInvoice, boolean reverseUncollect);
	
	void endDispensingForBatchIsuse(List<DispOrder> dispOrderList);
	
	void updateStatusForEndDispensing(DispOrder dispOrder);
	
	void cancelDispensing(DispOrder dispOrder);
	
	EndVettingResult refillDispensing(DispOrder dispOrder, List<MedOrderItem> confirmDisconIdList);
	
	EndVettingResult refillDispensing(DispOrder dispOrder, List<MedOrderItem> confirmDisconIdList, boolean releaseOrder, Map<Long, Boolean> largeLayoutFlagMap);	
}
