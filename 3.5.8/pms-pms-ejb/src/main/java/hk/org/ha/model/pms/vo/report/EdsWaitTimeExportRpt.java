package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class EdsWaitTimeExportRpt implements Serializable {

	private static final long serialVersionUID = 1L;

	private String ticketNum;
	
	private String ticketDate;

	private String vetDate;
	
	private String pickDate;
	
	private String assembleDate;
	
	private String checkDate;
	
	private String issueDate;

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}
	
	public String getTicketDate() {
		return ticketDate;
	}

	public void setTicketDate(String ticketDate) {
		this.ticketDate = ticketDate;
	}

	public String getVetDate() {
		return vetDate;
	}

	public void setVetDate(String vetDate) {
		this.vetDate = vetDate;
	}

	public String getPickDate() {
		return pickDate;
	}

	public void setPickDate(String pickDate) {
		this.pickDate = pickDate;
	}

	public String getAssembleDate() {
		return assembleDate;
	}

	public void setAssembleDate(String assembleDate) {
		this.assembleDate = assembleDate;
	}

	public String getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}
}
