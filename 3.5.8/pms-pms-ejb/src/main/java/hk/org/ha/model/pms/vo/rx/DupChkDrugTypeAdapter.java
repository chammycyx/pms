package hk.org.ha.model.pms.vo.rx;

import java.io.Serializable;

import hk.org.ha.model.pms.udt.disp.DupChkDrugType;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class DupChkDrugTypeAdapter extends XmlAdapter<String, DupChkDrugType> implements Serializable {

	private static final long serialVersionUID = 1L;

	public String marshal( DupChkDrugType v ) {		
		return (v == null) ? null : v.getDataValue();
	}
	
	public DupChkDrugType unmarshal( String v ) {		
		return DupChkDrugType.dataValueOf(v);
	}

}