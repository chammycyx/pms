package hk.org.ha.model.pms.biz.pivas;

import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormula;
import hk.org.ha.model.pms.persistence.pivas.PivasWorklist;
import hk.org.ha.model.pms.udt.pivas.PivasWorklistType;
import hk.org.ha.model.pms.vo.pivas.worklist.CalculatePivasInfoCriteria;
import hk.org.ha.model.pms.vo.pivas.worklist.PivasDueOrderListInfo;
import hk.org.ha.model.pms.vo.pivas.worklist.PivasFormulaMethodInfo;
import hk.org.ha.model.pms.vo.pivas.worklist.PivasPrepDetailInfo;
import hk.org.ha.model.pms.vo.pivas.worklist.PivasSupplyDateListInfo;
import hk.org.ha.model.pms.vo.pivas.worklist.PivasWorklistResponse;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface PivasWorklistServiceLocal {
	
	List<PivasWorklist> retrievePivasInboxList();
	
	PivasDueOrderListInfo retrievePivasDueOrderList(Date supplyDate);
	
	List<PivasWorklist> retrievePivasWorklistList();
	
	List<PivasWorklist> retrievePivasFindPatientList(String searchKey);
	
	PivasSupplyDateListInfo retrievePivasSupplyDateListInfo();

	PivasWorklistResponse addToWorklist(List<PivasWorklist> pivasWorklistList);

	PivasWorklistResponse deleteFromWorklist(List<PivasWorklist> pivasWorklistList);
	
	PivasPrepDetailInfo retrievePivasPrepDetailInfo(Long medProfileId, Long medProfileVersion, Long medProfileMoItemId, 
			Long pivasFormulaMethodId, Long firstDoseMaterialRequestId);
	
	List<PivasFormula> retrievePivasFormulaListByDrugKey(String patHospCode, String ward, 
			Integer drugKey, String moDosageUnit, String siteCode, String diluentCode, String strength);
	
	PivasFormula retrievePivasFormulaById(Long pivasFormulaId);
	
	PivasFormulaMethodInfo retrievePivasFormulaMethodInfoById(Long pivasFormulaMethodId);
	
	PivasWorklist recalculatePivasInfo(CalculatePivasInfoCriteria calculatePivasInfoCriteria);
	
	PivasWorklistResponse updatePivasWorklist(PivasWorklist pivasWorklist, boolean addToWorklist, PivasWorklistType pivasWorklistType);
	
	void destroy();
}