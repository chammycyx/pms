package hk.org.ha.model.pms.biz.reftable;

import flexjson.JSONSerializer;
import flexjson.transformer.DateTransformer;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.PmsWorkstoreRedirect;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.security.Identity;

@Stateful
@Scope(ScopeType.SESSION)
@Name("pmsWorkstoreRedirectListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PmsWorkstoreRedirectListServiceBean implements PmsWorkstoreRedirectListServiceLocal {
	
	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";	
	private static final JSONSerializer serializer = new JSONSerializer().transform(new DateTransformer(DATE_FORMAT), Date.class);
	
	@Out(required = false)
	private List<PmsWorkstoreRedirect> pmsWorkstoreRedirectList;
	
	@In
	private AuditLogger auditLogger;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

	@In
	private Workstore workstore;
	
	@In
	private Identity identity;
	
	public void retrievePmsWorkstoreRedirectList() 
	{
		pmsWorkstoreRedirectList = dmsPmsServiceProxy.retrievePmsWorkstoreRedirectList(workstore.getHospCode());
	}
	
	public void updatePmsWorkstoreRedirectList(List<PmsWorkstoreRedirect> updatePmsWorkstoreRedirectList) 
	{
		dmsPmsServiceProxy.updatePmsWorkstoreRedirectList(updatePmsWorkstoreRedirectList, identity.getCredentials().getUsername());
		auditLogger.log( "#1000", serializer.serialize(updatePmsWorkstoreRedirectList) );
		
		retrievePmsWorkstoreRedirectList();
	}
	
	@Remove
	public void destroy() 
	{
		if (pmsWorkstoreRedirectList != null) {
			pmsWorkstoreRedirectList = null;
		}
	}

}
