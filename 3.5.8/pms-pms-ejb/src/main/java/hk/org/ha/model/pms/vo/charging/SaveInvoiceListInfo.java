package hk.org.ha.model.pms.vo.charging;

import hk.org.ha.model.pms.persistence.disp.Invoice;

import java.io.Serializable;
import java.util.List;

public class SaveInvoiceListInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private List<Invoice> invoiceList;
	
	private boolean suspendOrder;	
	
	private String suspendCode;
	
	private String alertMsg;

	public List<Invoice> getInvoiceList() {
		return invoiceList;
	}

	public void setInvoiceList(List<Invoice> invoiceList) {
		this.invoiceList = invoiceList;
	}

	public boolean isSuspendOrder() {
		return suspendOrder;
	}

	public void setSuspendOrder(boolean suspendOrder) {
		this.suspendOrder = suspendOrder;
	}

	public String getSuspendCode() {
		return suspendCode;
	}

	public void setSuspendCode(String suspendCode) {
		this.suspendCode = suspendCode;
	}

	public String getAlertMsg() {
		return alertMsg;
	}

	public void setAlertMsg(String alertMsg) {
		this.alertMsg = alertMsg;
	}
}
