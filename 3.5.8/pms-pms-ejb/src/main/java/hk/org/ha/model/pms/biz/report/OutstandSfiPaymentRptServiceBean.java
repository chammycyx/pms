package hk.org.ha.model.pms.biz.report;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.charging.DrugChargeClearanceManagerLocal;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.reftable.ChargeReason;
import hk.org.ha.model.pms.udt.charging.ClearanceStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.InvoiceDocType;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.PharmOrderPatType;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.charging.FcsPaymentStatus;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.OutstandSfiPaymentRpt;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.joda.time.DateTime;

@Stateful
@Scope(ScopeType.SESSION)
@Name("outstandSfiPaymentRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls

public class OutstandSfiPaymentRptServiceBean implements OutstandSfiPaymentRptServiceLocal {

	@In
	private Workstore workstore;
	
	@In
	private PrintAgentInf printAgent;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private DrugChargeClearanceManagerLocal drugChargeClearanceManager;
	
	@PersistenceContext
	private EntityManager em;
	
	private List<Invoice> invoiceList;
	
	private List<OutstandSfiPaymentRpt> outstandSfiPaymentRptList;
	
	private Date reportDispDate;
	
	private boolean retrieveSuccess;
	
	@SuppressWarnings("unchecked")
	private void updateInvoicePaymentsStatus(Date dispDate){

		List<Invoice> sfiInvoiceList = em.createQuery( 
				"select o from Invoice o" + // 20121130 index check : Ticket.workstore, orderType, ticketDate : UI_TICKET_01
				" where o.dispOrder.ticket.workstore = :workstore" +
				" and o.dispOrder.ticket.orderType = :orderType" +
				" and o.dispOrder.ticket.ticketDate =:dispDate" +
				" and o.dispOrder.pharmOrder.patType <> :patType" +
				" and o.dispOrder.status = :dispOrderStatus" +
				" and o.docType = :docType" +
				" and o.status not in :invoiceStatusList")
				.setParameter("workstore", workstore)
				.setParameter("orderType", OrderType.OutPatient)
				.setParameter("dispDate", dispDate, TemporalType.DATE)	
				.setParameter("patType", PharmOrderPatType.Doh)
				.setParameter("dispOrderStatus", DispOrderStatus.Issued)
				.setParameter("docType", InvoiceDocType.Sfi)
				.setParameter("invoiceStatusList", Arrays.asList(InvoiceStatus.Void, InvoiceStatus.SysDeleted))
				.getResultList();
			
		if ( !sfiInvoiceList.isEmpty() ){			
			
			List<Invoice> updateSfiInvoiceList = new ArrayList<Invoice>();
			
			for( Invoice invoice : sfiInvoiceList ){
				
				if( invoice.getFcsCheckDate() == null ||
					invoice.getDispOrder().getDispDate().compareTo(new Date()) == 0 ||
					( invoice.getFcsCheckDate().compareTo(invoice.getUpdateDate()) < 0 && 
					  invoice.getDispOrder().getDispDate().compareTo(new Date()) < 0 )				
				){
					String patHospCode = workstore.getDefPatHospCode();					
					patHospCode = invoice.getDispOrder().getPharmOrder().getMedOrder().getPatHospCode();
					invoice.setPatHospCode(patHospCode);					
					updateSfiInvoiceList.add(invoice);
				}				
			}
			Map<String, FcsPaymentStatus> fcsPaymentStatusMap = drugChargeClearanceManager.checkSfiClearance(updateSfiInvoiceList, null);
						
			for( Invoice invoice : updateSfiInvoiceList ){		
				FcsPaymentStatus invoicePaymentStatus  = fcsPaymentStatusMap.get(invoice.getInvoiceNum());
				
				if( invoicePaymentStatus != null ){
					Invoice tempInvoice = em.find(Invoice.class, invoice.getId());
					
					tempInvoice.setReceiptNum(invoicePaymentStatus.getReceiptNum());					
					tempInvoice.setReceiveAmount(new BigDecimal(invoicePaymentStatus.getReceiptAmount()));
					
					if( invoicePaymentStatus.getClearanceStatus()==ClearanceStatus.PaymentClear ){
						tempInvoice.setStatus(InvoiceStatus.Settled);					
					}else{
						tempInvoice.setStatus(InvoiceStatus.Outstanding);
					}
					tempInvoice.setFcsCheckDate(new DateTime().toDate());
					em.merge(tempInvoice);				
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Invoice> retrieveOutstandSfiPaymentRpt(Date dispDate){
		reportDispDate = new Date(dispDate.getTime());
		outstandSfiPaymentRptList = new ArrayList<OutstandSfiPaymentRpt>();
		
		updateInvoicePaymentsStatus(dispDate);
				
		invoiceList = em.createQuery( 
				"select o from Invoice o" + // 20121130 index check : Ticket.workstore, orderType, ticketDate: UI_TICKET_01
				" where o.dispOrder.ticket.workstore = :workstore" +
				" and o.dispOrder.ticket.orderType = :orderType" +
				" and o.dispOrder.ticket.ticketDate = :dispDate" + 
				" and o.docType = :docType" + 
				" and o.status = :invoiceStatus" +
				" and o.dispOrder.pharmOrder.patType <> :patType" +
				" and o.dispOrder.status = :dispOrderStatus" +
				" and o.dispOrder.adminStatus <> :dispOrderAdminStatus")
				.setParameter("workstore", workstore)
				.setParameter("orderType", OrderType.OutPatient)
				.setParameter("dispDate", dispDate, TemporalType.DATE)	
				.setParameter("docType", InvoiceDocType.Sfi)
				.setParameter("invoiceStatus", InvoiceStatus.Outstanding)
				.setParameter("patType", PharmOrderPatType.Doh)
				.setParameter("dispOrderStatus", DispOrderStatus.Issued)
				.setParameter("dispOrderAdminStatus", DispOrderAdminStatus.Suspended)
				.getResultList();
		
		List<ChargeReason> chargeReasonList = em.createQuery(
				"select o from ChargeReason o" + // 20120306 index check : none
				" where o.type = 'F'")
				.getResultList();
		
		Map<String, String> chargeReasonMap = new HashMap<String, String>();
		for (ChargeReason cr : chargeReasonList) {
			chargeReasonMap.put(cr.getReasonCode(), cr.getDescription());
		}
		
		for (Invoice i: invoiceList) {
			OutstandSfiPaymentRpt outStandSfiPaymentRpt = new OutstandSfiPaymentRpt();
			PharmOrder po = i.getDispOrder().getPharmOrder();
			
			outStandSfiPaymentRpt.setTicketNum(i.getDispOrder().getTicket().getTicketNum());
			
			if (!po.getMedOrder().getDocType().equals( MedOrderDocType.Manual) ) {
				outStandSfiPaymentRpt.setRefNum(po.getMedOrder().getRefNum());	
			}
			
			if (po.getMedCase() != null) {
				outStandSfiPaymentRpt.setCaseNum(po.getMedCase().getCaseNum());
			}
			
			outStandSfiPaymentRpt.setPatName(i.getDispOrder().getPharmOrder().getName());
			outStandSfiPaymentRpt.setInvoiceNum(i.getInvoiceNum());
			
			if (chargeReasonMap.containsKey(i.getForceProceedCode())) {
				outStandSfiPaymentRpt.setForceProceedReason(chargeReasonMap.get(i.getForceProceedCode()));
			}
			
			outStandSfiPaymentRpt.setHkid(po.getMedOrder().getPatient().getHkid());
			outStandSfiPaymentRpt.setName(po.getMedOrder().getPatient().getName());
			outstandSfiPaymentRptList.add(outStandSfiPaymentRpt);
		}
		
		if ( invoiceList.isEmpty() ){
			retrieveSuccess = false;
		} else {
			retrieveSuccess = true;
		}		
		return invoiceList;
	}
	
	private Map<String, Object> parameters = new HashMap<String, Object>();
	public void generateOutstandSfiPaymentRpt() {
		
		parameters.put("hospCode", workstore.getHospCode());
		parameters.put("workstoreCode", workstore.getWorkstoreCode());
		parameters.put("dispDate", reportDispDate);
		
		printAgent.renderAndRedirect(new RenderJob(
				"OutstandSfiPaymentRpt", 				
				new PrintOption(),  				
				parameters, 
				outstandSfiPaymentRptList));

	}

	public void printOutstandSfiPaymentRpt(){
		
		printAgent.renderAndPrint(new RenderAndPrintJob(
				"OutstandSfiPaymentRpt", 				
				new PrintOption(),  				
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				parameters, 
				outstandSfiPaymentRptList));
	}
	
	public boolean isRetrieveSuccess(){
		return retrieveSuccess;
	}
	
	@Remove
	public void destroy() {

	}
}
