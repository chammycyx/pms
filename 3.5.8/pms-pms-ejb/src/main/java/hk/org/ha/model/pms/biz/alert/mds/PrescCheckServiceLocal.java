package hk.org.ha.model.pms.biz.alert.mds;

import java.util.List;

import javax.ejb.Local;

@Local
public interface PrescCheckServiceLocal {

	void overridePrescAlert(List<Integer> itemNumList);
	
	void destory();

}
