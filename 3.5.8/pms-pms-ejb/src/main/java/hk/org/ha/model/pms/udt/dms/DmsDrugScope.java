package hk.org.ha.model.pms.udt.dms;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DmsDrugScope implements StringValuedEnum {

	Pharmacy("100", "Pharmacy Only Item"),
	IpDrug("200", "For In-Patient Use Only"),
	OpDrug("300", "For In-Patient and Out-Patient Use"); 
		
    private final String dataValue;
    private final String displayValue;

    DmsDrugScope(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static DmsDrugScope dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DmsDrugScope.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<DmsDrugScope> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<DmsDrugScope> getEnumClass() {
    		return DmsDrugScope.class;
    	}
    }
}



