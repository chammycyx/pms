package hk.org.ha.model.pms.biz.onestop;

import javax.ejb.Local;

@Local
public interface OneStopAllowModifyUncollectValidationServiceLocal {
	
	void validateAllowModifyUncollectUser(String user, String password, String target);
	
	String getAllowModifyUncollectFailMsg();
	
	void destroy();
	
}
