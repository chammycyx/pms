package hk.org.ha.model.pms.vo.vetting;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class CapdVoucherPrintOut implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private CapdVoucherPrintOutHdr capdVoucherPrintOutHdr;
	
	private CapdVoucherPrintOutDtl capdVoucherPrintOutDtl;

	public CapdVoucherPrintOutHdr getCapdVoucherPrintOutHdr() {
		return capdVoucherPrintOutHdr;
	}

	public void setCapdVoucherPrintOutHdr(
			CapdVoucherPrintOutHdr capdVoucherPrintOutHdr) {
		this.capdVoucherPrintOutHdr = capdVoucherPrintOutHdr;
	}

	public CapdVoucherPrintOutDtl getCapdVoucherPrintOutDtl() {
		return capdVoucherPrintOutDtl;
	}

	public void setCapdVoucherPrintOutDtl(
			CapdVoucherPrintOutDtl capdVoucherPrintOutDtl) {
		this.capdVoucherPrintOutDtl = capdVoucherPrintOutDtl;
	}
}
