//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.3 in JDK 1.6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2010.03.23 at 02:28:14 PM CST 
//


package hk.org.ha.model.pms.vo.security;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "applMenus")
public class SysMenu implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@XmlElement(name="menu", required = true)
    private List<SysMenuDtl> menu;

	public List<SysMenuDtl> getMenu() {
		if (menu == null) {
            menu = new ArrayList<SysMenuDtl>();
        }
        return this.menu;
    }
	public void setMenu(List<SysMenuDtl> menu) {
		this.menu = menu;
	}
}