package hk.org.ha.model.pms.biz.onestop;

import static hk.org.ha.model.pms.prop.Prop.ONESTOP_SUSPENDORDERCOUNT_DURATION_LIMIT;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.vo.onestop.MoStatusCount;
import hk.org.ha.model.pms.vo.security.PropMap;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("unlockOrderService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class UnlockOrderServiceBean implements UnlockOrderServiceLocal {

	@PersistenceContext
	private EntityManager em;
		
	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
	
	@In
	private OneStopCountManagerLocal oneStopCountManager;
	
	@In
	private OneStopOrderStatusManagerLocal oneStopOrderStatusManager;
	
	@In
	private AuditLogger auditLogger;
	
	@In
	private UamInfo uamInfo;

	@In
	private Workstore workstore;
	
	@In
	private Workstation workstation;
	
	@In
	private PropMap propMap;
	
	@In(required = false)
    @Out(required = false)
    private String errMsg;
		
	@Out(required = false)
	private String latestLockWorkstaion;
	
	@Out(required = false)
	private MoStatusCount moStatusCount;	
	
	public void checkOrderIsLocked(Long id, Long medOrderVersion, Long refillId, Boolean requiredUnlock) {
		errMsg = null;
		latestLockWorkstaion = "";
		MedOrder medOrder = retrieveLockMedOrder(id);
		retrieveUnvetAndSuspendCount();
		
		if (!medOrder.getVersion().equals(medOrderVersion))//check order status
		{
			errMsg = oneStopOrderStatusManager.retrieveLatestOrderStatusFromLockingCheck(id, refillId);
			return;
		}
				
		if (medOrder.getProcessingFlag() && medOrder.getStatus() != MedOrderStatus.SysDeleted && requiredUnlock) {
			if (!checkOrderIsLockedByOtherWorkstation(medOrder)) {
				unlockOrder(medOrder);
			} else {
				errMsg = "0110";
			}
			
			return;
		}
	}
	
	public void validateMedOrder(Long id, Long medOrderVersion, Long refillId)
	{
		errMsg = null;
		MedOrder medOrder = retrieveLockMedOrder(id);
		if (!medOrder.getVersion().equals(medOrderVersion))//check order status
		{
			errMsg = oneStopOrderStatusManager.retrieveLatestOrderStatusFromLockingCheck(id, refillId);
		}
	}
	
	public void unlockOrderByRequest(Long id, Long medOrderVersion, Long refillId) {
		errMsg = null;
		latestLockWorkstaion = "";
		retrieveUnvetAndSuspendCount();
		MedOrder medOrder = retrieveLockMedOrder(id);
		
		if (!checkSameOrderVersion(medOrder, medOrderVersion)) {
			errMsg = oneStopOrderStatusManager.retrieveLatestOrderStatusFromLockingCheck(id, refillId);
            return;
		}
		
		unlockOrder(medOrder);
	}
	
	private MedOrder retrieveLockMedOrder(Long id) {		
		return em.find(MedOrder.class, id);
	}
	
	private Boolean checkSameOrderVersion(MedOrder medOrder, Long medOrderVersion) {
		if (medOrder.getVersion().equals(medOrderVersion)) {
			return true;
		}
		
		if (medOrder.getStatus() == MedOrderStatus.SysDeleted || medOrder.getWorkstationId() == null) {
			return true;
		}
		
		latestLockWorkstaion = retrieveWorkstation(medOrder.getWorkstationId()).getHostName();
		return false;
	}
	
	private Boolean checkOrderIsLockedByOtherWorkstation(MedOrder medOrder) {
		if (medOrder.getWorkstationId().equals(workstation.getId())) {
			return false;
		}
		
		if (medOrder.getWorkstationId() == null) {
			return false;
		}
		
		latestLockWorkstaion = retrieveWorkstation(medOrder.getWorkstationId()).getHostName();
		return true;
	}
	
	private void unlockOrder(MedOrder medOrder) {
		corpPmsServiceProxy.cancelVetting(medOrder.getOrderNum(), workstation.getWorkstore().getHospCode());
        medOrder.setProcessingFlag(false);
        if (medOrder.getStatus() == MedOrderStatus.Outstanding && 
        	medOrder.getTicket() != null)
        {
        	medOrder.setTicket(null);
        }
        
        em.merge(medOrder);
        em.flush();
        errMsg = "0112";
        auditLogger.log("#0522", uamInfo.getUserId(), workstation.getHostName(), medOrder.getId());
    }
	
	private Workstation retrieveWorkstation(Long workstationId) {
		return em.find(Workstation.class, workstationId);
	}
	
	private void retrieveUnvetAndSuspendCount() {
		if (oneStopCountManager.isCounterTimeOut(moStatusCount))
		{	
			oneStopCountManager.retrieveUnvetAndSuspendCount(workstore, propMap.getValueAsInteger(ONESTOP_SUSPENDORDERCOUNT_DURATION_LIMIT.getName()));		
		}
		
		moStatusCount = OneStopCountManagerBean.getMoStatusCountByWorkstore(workstore);
	}
	
	@Remove
	public void destroy(){
		if (errMsg != null) {
			errMsg = null;
		}
		
		if (latestLockWorkstaion != null && !latestLockWorkstaion.equals("")) {
			latestLockWorkstaion = "";
		}
		
		if (moStatusCount != null)
		{
			moStatusCount = null;
		}
	}
}
