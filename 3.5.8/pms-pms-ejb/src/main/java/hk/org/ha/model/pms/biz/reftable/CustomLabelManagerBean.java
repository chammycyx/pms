package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.CustomLabel;
import hk.org.ha.model.pms.udt.label.CustomLabelType;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("customLabelManager")
@MeasureCalls
public class CustomLabelManagerBean implements CustomLabelManagerLocal{
	@In
	private Workstore workstore;
	
	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	public CustomLabel retrieveCustomLabel(String name, CustomLabelType type){
		
		List<CustomLabel> customLabelList = em.createQuery(
				"select o from CustomLabel o" + // 20120303 index check : CustomLabel.workstore,name : UI_CUSTOM_LABEL_01
				" where o.name =:name" +
				" and o.type =:type" +
				" and o.workstore =:workstore")
				.setParameter("name", name)
				.setParameter("type", type)
				.setParameter("workstore", workstore)
				.getResultList();
		
		if(!customLabelList.isEmpty()) {
			return customLabelList.get(0);
		} else {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<CustomLabel> retrieveCustomLabelByLabelNum(String labelNum){

		return em.createQuery(
				"select o from CustomLabel o" + // 20121112 index check : CustomLabel.workstore : FK_CUSTOM_LABEL_01
				" where o.name like :name" +
				" and o.type =:type" +
				" and o.workstore =:workstore" +
				" order by o.name")
				.setParameter("name", labelNum + "%")
				.setParameter("type", CustomLabelType.Special)
				.setParameter("workstore", workstore)
				.getResultList();
		
	}
	
	public void updateCustomLabel(CustomLabel customLabel){
		CustomLabel cl = retrieveCustomLabel(customLabel.getName(), customLabel.getType());
		
		if(cl != null) {	
			cl.setLabelXml(customLabel.getLabelXml());
			cl.setDescription(customLabel.getDescription());
			em.merge(cl);
		} else {
			em.persist(customLabel);
		}
		em.flush();
	}
	
	public void removeCustomLabel(String name, CustomLabelType type){
		CustomLabel customLabel = retrieveCustomLabel(name, type);
		
		if(customLabel != null){	
			em.remove(customLabel);
		}
		em.flush();
	}
	
	@SuppressWarnings("unchecked")
	public List<CustomLabel> retrieveCustomLabelList(CustomLabelType type) 
	{
		List<CustomLabel> customLabelList = em.createQuery(
				"select o from CustomLabel o" + // 20120303 index check : CustomLabel.workstore,name : UI_CUSTOM_LABEL_01
				" where o.type =:type" +
				" and o.workstore =:workstore" +
				" order by o.name")
				.setParameter("type", type)
				.setParameter("workstore", workstore)
				.getResultList();	

		return customLabelList;
	}
	
	public void updateCustomLabelList(List<CustomLabel> customLabelList){
		for (CustomLabel customLabel : customLabelList){
			updateCustomLabel(customLabel);
		}
	}
	}
