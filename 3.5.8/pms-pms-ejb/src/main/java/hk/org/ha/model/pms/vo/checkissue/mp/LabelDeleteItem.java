package hk.org.ha.model.pms.vo.checkissue.mp;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class LabelDeleteItem implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Long deliveryItemId;
	
	private String caseNum;
	
	private String batchNum;
	
	private Date batchDate;
	
	private String ward;
	
	private String pasBedNum;
	
	private String itemCode;
	
	private BigDecimal issueQty;
	
	private String baseUnit;
	
	private String doseUnit;
	
	private String status;
	
	private String drugName;
	
	private String formLabelDesc;
	
	private String strength;
	
	private String volumeText;
	
	private boolean markDelete;

	public LabelDeleteItem() {
		this.markDelete = false;
	}

	public Long getDeliveryItemId() {
		return deliveryItemId;
	}

	public void setDeliveryItemId(Long deliveryItemId) {
		this.deliveryItemId = deliveryItemId;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public Date getBatchDate() {
		return batchDate;
	}

	public void setBatchDate(Date batchDate) {
		this.batchDate = batchDate;
	}

	public String getWard() {
		return ward;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}

	public String getPasBedNum() {
		return pasBedNum;
	}

	public void setPasBedNum(String pasBedNum) {
		this.pasBedNum = pasBedNum;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public BigDecimal getIssueQty() {
		return issueQty;
	}

	public void setIssueQty(BigDecimal issueQty) {
		this.issueQty = issueQty;
	}

	public String getDoseUnit() {
		return doseUnit;
	}

	public void setDoseUnit(String doseUnit) {
		this.doseUnit = doseUnit;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isMarkDelete() {
		return markDelete;
	}

	public void setMarkDelete(boolean markDelete) {
		this.markDelete = markDelete;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public String getDrugName() {
		return drugName;
	}

	public void setFormLabelDesc(String formLabelDesc) {
		this.formLabelDesc = formLabelDesc;
	}

	public String getFormLabelDesc() {
		return formLabelDesc;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public String getStrength() {
		return strength;
	}

	public void setVolumeText(String volumeText) {
		this.volumeText = volumeText;
	}

	public String getVolumeText() {
		return volumeText;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}	
	
	
}