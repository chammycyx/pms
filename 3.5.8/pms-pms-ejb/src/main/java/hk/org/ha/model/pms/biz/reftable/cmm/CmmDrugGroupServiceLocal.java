package hk.org.ha.model.pms.biz.reftable.cmm;

import hk.org.ha.model.pms.dms.persistence.cmm.CmmDrugGroup;

import java.util.List;

import javax.ejb.Local;

@Local
public interface CmmDrugGroupServiceLocal {

	List<CmmDrugGroup> retrieveCmmDrugGroupList(String prefixDisplayName);
	
	void updateCmmDrugGroupList(List<CmmDrugGroup> cmmDrugGroupList);
	
	void retrieveCmmDrugGroupRptList();
	
	void exportCmmDrugGroupRptList();
	
	void destroy();

}
