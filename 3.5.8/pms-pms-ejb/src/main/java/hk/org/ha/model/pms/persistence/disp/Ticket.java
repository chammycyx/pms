package hk.org.ha.model.pms.persistence.disp;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.vetting.OrderType;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "TICKET")
public class Ticket extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ticketSeq")
	@SequenceGenerator(name = "ticketSeq", sequenceName = "SQ_TICKET", initialValue = 100000000)
	private Long id;
	
	@Column(name = "TICKET_NUM", nullable = false, length = 4)
	private String ticketNum;
	
	@Column(name = "TICKET_DATE", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date ticketDate;
	
	@Converter(name = "Ticket.orderType", converterClass = OrderType.Converter.class)
    @Convert("Ticket.orderType")
	@Column(name="ORDER_TYPE", nullable = false, length = 1)
	private OrderType orderType;
	
	@Column(name = "ORDER_NUM", length = 13)
	private String orderNum;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE", referencedColumnName = "HOSP_CODE", nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;

	public Ticket() {
		this.orderType = OrderType.OutPatient;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public Date getTicketDate() {
		if (ticketDate == null) {
			return null;
		}
		else {
			return new Date(ticketDate.getTime());
		}
	}

	public void setTicketDate(Date ticketDate) {
		if (ticketDate == null) {
			this.ticketDate = null;
		}
		else {
			this.ticketDate = new Date(ticketDate.getTime());
		}
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}

	public OrderType getOrderType() {
		return orderType;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public String getOrderNum() {
		return orderNum;
	}
}
