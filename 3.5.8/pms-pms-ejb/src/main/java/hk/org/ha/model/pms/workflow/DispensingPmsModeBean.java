package hk.org.ha.model.pms.workflow;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;

import javax.ejb.Stateless;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.bpm.CreateProcess;
import org.jboss.seam.annotations.bpm.ResumeProcess;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;
import org.jbpm.graph.exe.ProcessInstance;

/**
 * Session Bean implementation class ProductService
 */
@AutoCreate
@Stateless
@Name("dispensingPmsMode")
@Restrict("#{identity.loggedIn}")
@MeasureCalls
public class DispensingPmsModeBean implements DispensingPmsModeLocal {

	@Logger
	private Log logger;
		
    @In(required=false) 
    private ProcessInstance processInstance;
    
    @In(scope=ScopeType.BUSINESS_PROCESS, required=false)
    @Out(scope=ScopeType.BUSINESS_PROCESS, required=false)
    private Long dispOrderId;
        
	@CreateProcess(definition="dispensing-pms-mode", processKey="#{dispOrderId}")
    public void startDispensingFlow(Long dispOrderId) {
		this.dispOrderId = dispOrderId;
		logger.debug("updateDispOrderStatus DispOrder Id: #0", dispOrderId);
	}
	
	@ResumeProcess(definition="dispensing-pms-mode", processKey="#{dispOrderId}")
	public void updateToPickedStatus(DispOrder dispOrder) {
		dispOrder.setStatus(DispOrderStatus.Picked);
		logger.debug("updateDispOrderStatus DispOrder Id: #0  status: #1", dispOrderId, dispOrder.getStatus());
		processInstance.signal("picked");
	}
	
	@ResumeProcess(definition="dispensing-pms-mode", processKey="#{dispOrderId}")
	public void updateToAssembleStatus(DispOrder dispOrder) {
		dispOrder.setStatus(DispOrderStatus.Assembled);
		logger.debug("updateDispOrderStatus DispOrder Id: #0  status: #1", dispOrderId, dispOrder.getStatus());
		processInstance.signal("assembled");
	}
	
	@ResumeProcess(definition="dispensing-pms-mode", processKey="#{dispOrderId}")
	public void updateToCheckedStatus(DispOrder dispOrder) {
		dispOrder.setStatus(DispOrderStatus.Checked);
		logger.debug("updateDispOrderStatus DispOrder Id: #0  status: #1", dispOrderId, dispOrder.getStatus());
		processInstance.signal("checked");
	}
	
	@ResumeProcess(definition="dispensing-pms-mode", processKey="#{dispOrderId}")
	public void updateToIssuedStatus(DispOrder dispOrder) {
		dispOrder.setStatus(DispOrderStatus.Issued);
		logger.debug("updateDispOrderStatus DispOrder Id: #0  status: #1", dispOrderId, dispOrder.getStatus());
		processInstance.signal("issued");
	}
	
	@ResumeProcess(definition="dispensing-pms-mode", processKey="#{dispOrderId}")
	public void unvetOrder() {
		logger.debug("updateDispOrderStatus DispOrder Id: #0", dispOrderId);
		processInstance.signal("unvetted");
	}
}
