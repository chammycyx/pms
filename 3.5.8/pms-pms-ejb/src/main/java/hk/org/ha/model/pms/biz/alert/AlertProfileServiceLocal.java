package hk.org.ha.model.pms.biz.alert;

import hk.org.ha.model.pms.asa.exception.alert.AlertProfileException;
import hk.org.ha.model.pms.persistence.PatientEntity;
import hk.org.ha.model.pms.vo.alert.AlertProfileHistory;

import javax.ejb.Local;

@Local
public interface AlertProfileServiceLocal {
	
	AlertProfileHistory retrieveAlertProfileHistory(String caseNum, String patHospCode, PatientEntity patientEntity) throws AlertProfileException;
	
	void destroy();

}
