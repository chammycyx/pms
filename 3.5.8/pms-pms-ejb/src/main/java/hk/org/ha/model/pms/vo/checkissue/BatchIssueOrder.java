package hk.org.ha.model.pms.vo.checkissue;

import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class BatchIssueOrder implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String ticketNum;
	
	private String refNum;
	
	private String patName;
	
	private String patNameChi;
	
	private String hkid;
	
	private String caseNum;
	
	private Date ticketDate;
	
	private DispOrderStatus status;
	
	private boolean sfiFlag;
	
	private String pasSpecCode;
	
	private String pasSubSpecCode;
	
	private String specCode;
	
	private String ward;
	
	private String patCatCode;
	
	private DispOrder dispOrder;

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public String getRefNum() {
		return refNum;
	}

	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getPatNameChi() {
		return patNameChi;
	}

	public void setPatNameChi(String patNameChi) {
		this.patNameChi = patNameChi;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public DispOrderStatus getStatus() {
		return status;
	}

	public void setStatus(DispOrderStatus status) {
		this.status = status;
	}

	public boolean isSfiFlag() {
		return sfiFlag;
	}

	public void setSfiFlag(boolean sfiFlag) {
		this.sfiFlag = sfiFlag;
	}

	public String getPasSpecCode() {
		return pasSpecCode;
	}

	public void setPasSpecCode(String pasSpecCode) {
		this.pasSpecCode = pasSpecCode;
	}

	public String getPasSubSpecCode() {
		return pasSubSpecCode;
	}

	public void setPasSubSpecCode(String pasSubSpecCode) {
		this.pasSubSpecCode = pasSubSpecCode;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public String getWard() {
		return ward;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}

	public String getPatCatCode() {
		return patCatCode;
	}

	public void setPatCatCode(String patCatCode) {
		this.patCatCode = patCatCode;
	}

	public DispOrder getDispOrder() {
		return dispOrder;
	}

	public void setDispOrder(DispOrder dispOrder) {
		this.dispOrder = dispOrder;
	}

	public void setTicketDate(Date ticketDate) {
		this.ticketDate = ticketDate;
	}

	public Date getTicketDate() {
		return ticketDate;
	}

}