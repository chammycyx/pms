package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.PmsCommonOrderStatus;
import hk.org.ha.model.pms.persistence.corp.HospitalMapping;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.model.pms.vo.reftable.mp.CommonOrderStatusInfo;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("commonOrderStatusListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CommonOrderStatusListServiceBean implements CommonOrderStatusListServiceLocal {
	
	@In
	private Workstore workstore;
	
	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@Override
	public CommonOrderStatusInfo retrieveCommonOrderStatusInfo(DmDrug dmDrug) {
		
		CommonOrderStatusInfo commonOrderStatusInfo = new CommonOrderStatusInfo();
		
		if( dmDrug == null ){
			return commonOrderStatusInfo;
		}
		
		List<WorkstoreDrug> workstoreDrugList = new ArrayList<WorkstoreDrug>();
		
		if( StringUtils.isNotEmpty(dmDrug.getItemCode()) ){
			WorkstoreDrug wd = workstoreDrugCacher.getWorkstoreDrug(workstore, dmDrug.getItemCode());
			if( wd != null ){
				workstoreDrugList.add(wd);
			}
		}else if( dmDrug.getDrugKey() != null ){
			workstoreDrugList = workstoreDrugCacher.getWorkstoreDrugListByDrugKey(workstore, dmDrug.getDrugKey());
		}
		
		commonOrderStatusInfo.setWorkstoreDrugList(workstoreDrugList);
		
		List<HospitalMapping> hospitalMappingList = workstore.getHospital().getHospitalMappingList();
			
		if ( !hospitalMappingList.isEmpty() ) {
			for (Iterator<HospitalMapping> itr = hospitalMappingList.iterator();itr.hasNext();) {
				HospitalMapping hospitalMapping = itr.next();
				if ( !hospitalMapping.getPrescribeFlag() ) {
					itr.remove();
				}
			}	
		}
			
		commonOrderStatusInfo.setHospitalMappingList(hospitalMappingList);
		
		return commonOrderStatusInfo;
	}


	@Override
	public CommonOrderStatusInfo retrievePmsCommonOrderStatusList( String patHospCode, String itemCode, Integer drugKey ) {
		
		CommonOrderStatusInfo commonOrderStatusInfo = new CommonOrderStatusInfo();
		commonOrderStatusInfo.setPmsCommonOrderStatusList( dmsPmsServiceProxy.retrievePmsCommonOrderStatusList(patHospCode, itemCode, drugKey) );
		
		return commonOrderStatusInfo;
		
	}

	@Override
	public void updateCommonOrderStatusList( List<PmsCommonOrderStatus> pmsCommonOrderStatusList ) {
		
		dmsPmsServiceProxy.updatePmsCommonOrderStatusList(pmsCommonOrderStatusList);
		
	}

	@Remove
	public void destroy() 
	{
		
	}

}
