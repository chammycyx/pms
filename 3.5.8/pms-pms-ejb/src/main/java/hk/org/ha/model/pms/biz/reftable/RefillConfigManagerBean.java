package hk.org.ha.model.pms.biz.reftable;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.RefillConfig;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateless
@Name("refillConfigManager")
@Restrict("#{identity.loggedIn}")
@MeasureCalls
public class RefillConfigManagerBean implements RefillConfigManagerLocal {

	@PersistenceContext
	private EntityManager em;
	
	@In
	private Workstore workstore;
	
	@SuppressWarnings("unchecked")
	public RefillConfig retrieveRefillConfig() 
	{
		List<RefillConfig> refillConfigList = em.createQuery(
				"select o from RefillConfig o" + // 20120303 index check : RefillConfig.workstore : UI_REFILL_CONFIG_01
				" where o.workstore = :workstore")
				.setParameter("workstore", workstore)
				.getResultList();
		
		if ( !refillConfigList.isEmpty() ) {
			return refillConfigList.get(0);
		} else {
			RefillConfig refillConfig = new RefillConfig();
			refillConfig.setWorkstore(workstore); 
			return refillConfig;
		}

	}
}
