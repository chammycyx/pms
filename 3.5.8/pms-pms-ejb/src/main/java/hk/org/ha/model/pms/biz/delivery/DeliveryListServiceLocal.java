package hk.org.ha.model.pms.biz.delivery;
import javax.ejb.Local;

@Local
public interface DeliveryListServiceLocal {
	void retrieveTodayDeliveryList();
	void destroy();
}
