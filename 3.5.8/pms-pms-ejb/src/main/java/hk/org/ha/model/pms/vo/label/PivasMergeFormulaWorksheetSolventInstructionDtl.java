package hk.org.ha.model.pms.vo.label;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "PivasMergeFormulaWorksheetSolventInstructionDtl")
@ExternalizedBean(type=DefaultExternalizer.class)
public class PivasMergeFormulaWorksheetSolventInstructionDtl implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@XmlElement(required = true)
	private Integer sortSeq;
	
	@XmlElement(required = true)
	private Boolean mutipleItemCodeWithSameDrugKeyExist;
	
	@XmlElement(required = true)
	private Integer drugKey;
	
	@XmlElement(required = true)
	private String drugItemCode;
	
	@XmlElement(required = true)
	private String drugDisplayName;
	
	@XmlElement(required = false)
	private String drugSaltProperty;
	
	@XmlElement(required = false)
	private String drugStrength;
	
	@XmlElement(required = true)
	private String dosageQty;
	
	@XmlElement(required = true)
	private String pivasDosageUnit;
	
	@XmlElement(required = true)
	private String solventVolume;
	
	@XmlElement(required = true)
	private String solventDesc;
	
	// save for PIVAS report
	@XmlElement(required = false)
	private String solventItemCode;
	
	// save for PIVAS report
	@XmlElement(required = true)
	private Boolean vendSupplySolvFlag;
	
	public PivasMergeFormulaWorksheetSolventInstructionDtl() {
		mutipleItemCodeWithSameDrugKeyExist = Boolean.FALSE;
	}

	public Integer getSortSeq() {
		return sortSeq;
	}

	public void setSortSeq(Integer sortSeq) {
		this.sortSeq = sortSeq;
	}

	public Boolean getMutipleItemCodeWithSameDrugKeyExist() {
		return mutipleItemCodeWithSameDrugKeyExist;
	}

	public void setMutipleItemCodeWithSameDrugKeyExist(
			Boolean mutipleItemCodeWithSameDrugKeyExist) {
		this.mutipleItemCodeWithSameDrugKeyExist = mutipleItemCodeWithSameDrugKeyExist;
	}

	public Integer getDrugKey() {
		return drugKey;
	}

	public void setDrugKey(Integer drugKey) {
		this.drugKey = drugKey;
	}

	public String getDrugItemCode() {
		return drugItemCode;
	}

	public void setDrugItemCode(String drugItemCode) {
		this.drugItemCode = drugItemCode;
	}

	public String getDrugDisplayName() {
		return drugDisplayName;
	}

	public void setDrugDisplayName(String drugDisplayName) {
		this.drugDisplayName = drugDisplayName;
	}

	public String getDrugSaltProperty() {
		return drugSaltProperty;
	}

	public void setDrugSaltProperty(String drugSaltProperty) {
		this.drugSaltProperty = drugSaltProperty;
	}

	public String getDrugStrength() {
		return drugStrength;
	}

	public void setDrugStrength(String drugStrength) {
		this.drugStrength = drugStrength;
	}

	public String getDosageQty() {
		return dosageQty;
	}

	public void setDosageQty(String dosageQty) {
		this.dosageQty = dosageQty;
	}

	public String getPivasDosageUnit() {
		return pivasDosageUnit;
	}

	public void setPivasDosageUnit(String pivasDosageUnit) {
		this.pivasDosageUnit = pivasDosageUnit;
	}

	public String getSolventVolume() {
		return solventVolume;
	}

	public String getSolventDesc() {
		return solventDesc;
	}

	public void setSolventDesc(String solventDesc) {
		this.solventDesc = solventDesc;
	}

	public void setSolventVolume(String solventVolume) {
		this.solventVolume = solventVolume;
	}

	public String getSolventItemCode() {
		return solventItemCode;
	}

	public void setSolventItemCode(String solventItemCode) {
		this.solventItemCode = solventItemCode;
	}

	public Boolean getVendSupplySolvFlag() {
		return vendSupplySolvFlag;
	}

	public void setVendSupplySolvFlag(Boolean vendSupplySolvFlag) {
		this.vendSupplySolvFlag = vendSupplySolvFlag;
	}
}
