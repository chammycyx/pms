package hk.org.ha.model.pms.vo.rx;

import hk.org.ha.model.pms.udt.vetting.FixedConcnSource;

import java.io.Serializable;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class FixedConcnSourceAdapter extends XmlAdapter<String, FixedConcnSource> implements Serializable {

	private static final long serialVersionUID = 1L;

	public String marshal( FixedConcnSource v ) {		
		return (v == null) ? null : v.getDataValue();
	}
	
	public FixedConcnSource unmarshal( String v ) {		
		return FixedConcnSource.dataValueOf(v);
	}

}