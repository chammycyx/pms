package hk.org.ha.model.pms.prop;

import hk.org.ha.model.pms.cache.PropCacherBean;
import hk.org.ha.model.pms.persistence.PropEntity;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.Workstation;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class PropHolder<V> {
	
	private static final Map<String, PropHolder<?>> PROP_HOLDER_MAP = Collections.synchronizedMap(new HashMap<String, PropHolder<?>>());
	
	private String name;
	private Scope scope;

	public static PropHolder<?> getPropHolder(String name) {
		return PROP_HOLDER_MAP.get(name);
	}

	public PropHolder(String name) {
		this(name, Scope.OPERATION, false);
	}
	
	public PropHolder(String name, Scope scope) {
		this(name, scope, false);		
	}
	public PropHolder(String name, Scope scope, boolean sessionCache) {
		this.name = name;
		this.scope = scope;
		PROP_HOLDER_MAP.put(name, this);
	}
	
	public String getName() {
		return name;
	}

	public Scope getScope() {
		return scope;
	}

	public boolean isEquals(V value) {
		return this.get().equals(value);
	}

	public V get(V def) {
		V v = this.get();
		return (v != null) ? v : def;
	}	
	
	public V get(Hospital hospital, V def) {
		V v = this.get(hospital);
		return (v != null) ? v : def;
	}	

	public V get(Workstore workstore, V def) {
		V v = this.get(workstore);
		return (v != null) ? v : def;
	}	

	public V get(Workstation workstation, V def) {
		V v = this.get(workstation);
		return (v != null) ? v : def;
	}	
	
	public V get() {
		return getConvertedValue(this.getPropEntity());
	}	

	public V get(Hospital hospital) {		
		return getConvertedValue(this.getPropEntity(hospital));
	}	

	public V get(Workstore workstore) {		
		return getConvertedValue(this.getPropEntity(workstore));
	}	
		
	public V get(Workstation workstation) {		
		return getConvertedValue(this.getPropEntity(workstation));
	}	

	public PropEntity getPropEntity(Hospital hospital) {
		PropSessionCache propSessionMap = PropSessionCache.instance();				
		PropEntity propEntity = propSessionMap.get(name);		
		if (propEntity == null) {
			propEntity = PropCacherBean.instance().getPropEntity(hospital, name);
			if (propEntity.getProp().getCacheInSessionFlag()) {
				propSessionMap.put(name, propEntity);
			}
		}
		return propEntity;
	}

	public PropEntity getPropEntity(Workstore workstore) {
		PropSessionCache propSessionMap = PropSessionCache.instance();				
		PropEntity propEntity = propSessionMap.get(name);		
		if (propEntity == null) {
			propEntity = PropCacherBean.instance().getPropEntity(workstore, name);
			if (propEntity.getProp().getCacheInSessionFlag()) {
				propSessionMap.put(name, propEntity);
			}
		}
		return propEntity;
	}
	
	public PropEntity getPropEntity(Workstation workstation) {
		PropSessionCache propSessionMap = PropSessionCache.instance();				
		PropEntity propEntity = propSessionMap.get(name);		
		if (propEntity == null) {
			propEntity = PropCacherBean.instance().getPropEntity(workstation, name);
			if (propEntity.getProp().getCacheInSessionFlag()) {
				propSessionMap.put(name, propEntity);
			}
		}
		return propEntity;
	}

	public PropEntity getPropEntity() {
		PropSessionCache propSessionMap = PropSessionCache.instance();				
		PropEntity propEntity = propSessionMap.get(name);		
		if (propEntity == null) {
			propEntity = PropCacherBean.instance().getPropEntity(scope, name);
			if (propEntity.getProp().getCacheInSessionFlag()) {
				propSessionMap.put(name, propEntity);
			}
		}
		return propEntity;
	}
		
	@SuppressWarnings("unchecked")
	private V getConvertedValue(PropEntity propEntity) {
		if (propEntity.getValue() != null) {
			return (V) propEntity.getConvertedValue();
		} else {
			return null;
		}
	}	
	
}
