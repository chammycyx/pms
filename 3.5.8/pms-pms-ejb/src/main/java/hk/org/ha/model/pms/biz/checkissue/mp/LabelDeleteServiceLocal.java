package hk.org.ha.model.pms.biz.checkissue.mp;

import hk.org.ha.model.pms.vo.checkissue.mp.LabelDelete;

import java.util.Date;

import javax.ejb.Local;

@Local
public interface LabelDeleteServiceLocal {
	
	void retrieveLabelDeleteItemList(Date trxDate, String caseNum, String batchNum, String trxId);
	
	void updateDeliveryItemStatusToDeleted(LabelDelete inLabelDelete);
	
	String getMsgCode();
	
	void destroy();
	
}