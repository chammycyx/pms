package hk.org.ha.model.pms.biz.alert.mds;

import hk.org.ha.fmk.pms.web.MeasureCalls;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("prescCheckService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PrescCheckServiceBean implements PrescCheckServiceLocal {

	@In
	private PrescCheckManagerLocal prescCheckManager;
	
	public void overridePrescAlert(List<Integer> itemNumList) {

		prescCheckManager.overridePrescAlert(itemNumList);
	}
	
	@Remove
	public void destory() {
	}
}
