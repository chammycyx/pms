package hk.org.ha.model.pms.vo.reftable.pivas;

import hk.org.ha.model.pms.dms.persistence.DmSite;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PivasSiteInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private Boolean selected;
	
	private Boolean whiteSite;
	
	private DmSite dmSite;

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public Boolean getWhiteSite() {
		return whiteSite;
	}

	public void setWhiteSite(Boolean whiteSite) {
		this.whiteSite = whiteSite;
	}

	public DmSite getDmSite() {
		return dmSite;
	}

	public void setDmSite(DmSite dmSite) {
		this.dmSite = dmSite;
	}

}