package hk.org.ha.model.pms.biz.reftable;

import flexjson.JSONSerializer;
import flexjson.transformer.DateTransformer;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.MsFmStatus;
import hk.org.ha.model.pms.dms.persistence.MsWorkstoreDrug;
import hk.org.ha.model.pms.dms.persistence.MsWorkstoreDrugHistory;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.corp.WorkstorePK;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.security.Identity;

@Stateful
@Scope(ScopeType.SESSION)
@Name("msWorkstoreDrugListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MsWorkstoreDrugListServiceBean implements MsWorkstoreDrugListServiceLocal {
	
	@Out(required = false)
	private List<MsWorkstoreDrug> msWorkstoreDrugList;
	
	@Out(required = false)
	private List<MsWorkstoreDrugHistory> msWorkstoreDrugHistoryList;
	
	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
	
	private static final int SLEEP_TIME = 1000;
	
	private JSONSerializer serializer = new JSONSerializer().transform(new DateTransformer(DATE_FORMAT), Date.class);	
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private AuditLogger auditLogger;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

	@In
	private DmDrugCacherInf dmDrugCacher;

	@In
	private Workstore workstore;
	
	@In
	private Identity identity;
	
	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
	
	private MsWorkstoreDrug currentItem;
	
	private MsWorkstoreDrugHistoryComparator workstoreDrugHistoryComparator = new MsWorkstoreDrugHistoryComparator();
	
	public void retrieveMsWorkstoreDrugList(String itemCode) 
	{
		msWorkstoreDrugList = null;
		msWorkstoreDrugHistoryList = null;
		DmDrug dmDrug = dmDrugCacher.getDmDrug(itemCode);
		
		if (dmDrug!=null) {
			msWorkstoreDrugList = dmsPmsServiceProxy.retrieveMsWorkstoreDrugList(
					workstore.getHospCode(), workstore.getWorkstoreCode(), itemCode);
			
			currentItem = null;
			
			for ( MsWorkstoreDrug msWorkstoreDrug : msWorkstoreDrugList ) 
			{
				if ( currentItem == null ) {
					currentItem = msWorkstoreDrug;
					msWorkstoreDrugHistoryList = msWorkstoreDrug.getMsWorkstoreDrugHistoryList();
					Collections.sort(msWorkstoreDrugHistoryList,workstoreDrugHistoryComparator);
				}
				
				msWorkstoreDrug.setMsWorkstoreDrugHistoryList(new ArrayList<MsWorkstoreDrugHistory>());
				
				msWorkstoreDrug.getDmDrug().getFullDrugDesc();

				MsFmStatus msFmStatus = new MsFmStatus();
				msFmStatus.setFmStatusDesc(msWorkstoreDrug.getDmDrug().getDmMoeProperty().getMsFmStatus().getFmStatusDesc());
				msWorkstoreDrug.getDmDrug().setPmsFmStatus(msFmStatus);
			}
		}
		
	}
	
	public void updateMsWorkstoreDrugList(List<MsWorkstoreDrug> updateMsWorkstoreDrugList, String remark) 
	{
		MsWorkstoreDrug updateItem = updateMsWorkstoreDrugList.get(0);

		if(!haveMsWorkstoreDrugListChange(updateItem))
		{
			return;
		}				
		
		dmsPmsServiceProxy.updateMsWorkstoreDrugList(updateMsWorkstoreDrugList, remark, identity.getCredentials().getUsername());
		auditLogger.log("#0997", serializer.exclude("dmDrug").serialize(updateMsWorkstoreDrugList));
		
		if (!updateMsWorkstoreDrugList.isEmpty()) 
		{
			for ( MsWorkstoreDrug msWorkstoreDrug : updateMsWorkstoreDrugList ) {
				WorkstorePK workstorePK = new WorkstorePK(msWorkstoreDrug.getCompId().getDispHospCode(), msWorkstoreDrug.getCompId().getDispWorkstore());
				Workstore updateWorkstore = em.find(Workstore.class, workstorePK);
				if ( updateWorkstore != null ) {
					corpPmsServiceProxy.purgeMsWorkstoreDrug(updateWorkstore);
				}
			}
			try 
			{
				Thread.sleep(SLEEP_TIME);
			} 
			catch (InterruptedException e) {}
		}
	}
	
	private boolean haveMsWorkstoreDrugListChange(MsWorkstoreDrug updateItem)
	{
		return ( !StringUtils.equals(currentItem.getSuspend(), updateItem.getSuspend()) ||
				!StringUtils.equals(currentItem.getSfiDeactivate(),updateItem.getSfiDeactivate()) ||
				!StringUtils.equals(currentItem.getSampleActivate(),updateItem.getSampleActivate()) ||
				currentItem.getDrugScope().compareTo(updateItem.getDrugScope()) != 0) ||
				!StringUtils.equals(currentItem.getRemark(), updateItem.getRemark());
	}
	
	public static class MsWorkstoreDrugHistoryComparator implements Comparator<MsWorkstoreDrugHistory>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(MsWorkstoreDrugHistory history1, MsWorkstoreDrugHistory history2) {
			return history2.getUpdateDate().compareTo(history1.getUpdateDate());
		}
		
	}
	
	@Remove
	public void destroy() 
	{
		if (msWorkstoreDrugList != null) {
			msWorkstoreDrugList = null;
		}
		if (msWorkstoreDrugHistoryList != null) {
			msWorkstoreDrugHistoryList = null;
		}
	}

}
