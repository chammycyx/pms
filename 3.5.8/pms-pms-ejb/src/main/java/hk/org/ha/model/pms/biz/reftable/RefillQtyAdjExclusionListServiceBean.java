package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.RefillQtyAdjExclusion;

import java.util.Collection;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("refillQtyAdjExclusionListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class RefillQtyAdjExclusionListServiceBean implements RefillQtyAdjExclusionListServiceLocal {

	@PersistenceContext
	private EntityManager em;
	
	@In
	private Workstore workstore;
	
	@SuppressWarnings("unchecked")
	public List<RefillQtyAdjExclusion> retrieveRefillQtyAdjExclusionList()
	{
		return em.createQuery(
				"select o from RefillQtyAdjExclusion o" + // 20120303 index check : RefillQtyAdjExclusion.workstore : FK_REFILL_QTY_ADJ_EXCLUSION_01
				" where o.workstore = :workstore " +
				" order by o.itemCode")
			.setParameter("workstore", workstore)
			.getResultList();
	}
	
	public void updateRefillQtyAdjExclusionList(Collection<RefillQtyAdjExclusion> updateRefillQtyAdjExclusionList, 
													Collection<RefillQtyAdjExclusion> delRefillQtyAdjExclusionList)
	{
		workstore = em.find(Workstore.class, workstore.getId());
		
		if(!delRefillQtyAdjExclusionList.isEmpty())
		{
			for(RefillQtyAdjExclusion delRefillQtyAdjExclusion:delRefillQtyAdjExclusionList)
			{
				if ( delRefillQtyAdjExclusion.getId() != null ) {
					em.remove(em.merge(delRefillQtyAdjExclusion));
				}
			}
		}
		em.flush();
		
		for(RefillQtyAdjExclusion refillQtyAdjExclusion:updateRefillQtyAdjExclusionList)
		{
			if(StringUtils.isNotBlank(refillQtyAdjExclusion.getItemCode()))
			{
				if(refillQtyAdjExclusion.getId()==null){
					refillQtyAdjExclusion.setWorkstore(workstore);
					em.persist(refillQtyAdjExclusion);
				}
			}
		}
		
		em.flush();
		em.clear();
	}

	@Remove
	public void destroy() 
	{
	}
	
}
