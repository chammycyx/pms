package hk.org.ha.model.pms.vo.charging;

import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;

import java.io.Serializable;
import java.util.Date;

public class FcsMpSfiInvoiceInfo extends FcsSfiInvoiceInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer dispOrderNum;

	//forceProceedInfo
	private Boolean forceProceedFlag;
	private String forceProceedUser;
	private String forceProceedCode;
	private String manualInvoiceNum;
	private String manualReceiptNum;
	private Date forceProceedDate;
	
	// for disp_status
	private DispOrderStatus dispOrderStatus;
	private DispOrderAdminStatus dispOrderAdminStatus;
	
	//Item
	private Integer orgDispItemNum;
	private Integer dispItemNum;
	
	public FcsMpSfiInvoiceInfo() {
		// fcsSfiInvoiceInfo default constructor
	}
	
	public FcsMpSfiInvoiceInfo(Invoice invoice) {
		super(invoice);
		DispOrder dispOrder = invoice.getDispOrder();
		
		forceProceedFlag = invoice.getForceProceedFlag();
		forceProceedUser = invoice.getForceProceedUser();
		forceProceedCode = invoice.getForceProceedCode();
		manualInvoiceNum = invoice.getManualInvoiceNum();
		manualReceiptNum = invoice.getManualReceiptNum();
		forceProceedDate = invoice.getForceProceedDate();
		
		dispOrderNum = dispOrder.getDispOrderNum();
		
		dispOrderStatus = DispOrderStatus.Vetted;
		dispOrderAdminStatus = dispOrder.getAdminStatus();
		
		if( dispOrder.getDispOrderItemList() != null && dispOrder.getDispOrderItemList().size() > 0 ){
			DispOrderItem dispOrderItem = dispOrder.getDispOrderItemList().get(0);
			orgDispItemNum = dispOrderItem.getPharmOrderItem().getOrgItemNum();
			dispItemNum = dispOrderItem.getPharmOrderItem().getItemNum();
		}
	}
	
	public Integer getDispOrderNum() {
		return dispOrderNum;
	}

	public void setDispOrderNum(Integer dispOrderNum) {
		this.dispOrderNum = dispOrderNum;
	}

	public Boolean getForceProceedFlag() {
		return forceProceedFlag;
	}

	public void setForceProceedFlag(Boolean forceProceedFlag) {
		this.forceProceedFlag = forceProceedFlag;
	}

	public String getForceProceedUser() {
		return forceProceedUser;
	}

	public void setForceProceedUser(String forceProceedUser) {
		this.forceProceedUser = forceProceedUser;
	}

	public String getForceProceedCode() {
		return forceProceedCode;
	}

	public void setForceProceedCode(String forceProceedCode) {
		this.forceProceedCode = forceProceedCode;
	}

	public String getManualInvoiceNum() {
		return manualInvoiceNum;
	}

	public void setManualInvoiceNum(String manualInvoiceNum) {
		this.manualInvoiceNum = manualInvoiceNum;
	}

	public String getManualReceiptNum() {
		return manualReceiptNum;
	}

	public void setManualReceiptNum(String manualReceiptNum) {
		this.manualReceiptNum = manualReceiptNum;
	}

	public Date getForceProceedDate() {
		return forceProceedDate;
	}

	public void setForceProceedDate(Date forceProceedDate) {
		this.forceProceedDate = forceProceedDate;
	}

	public DispOrderStatus getDispOrderStatus() {
		return dispOrderStatus;
	}

	public void setDispOrderStatus(DispOrderStatus dispOrderStatus) {
		this.dispOrderStatus = dispOrderStatus;
	}

	public DispOrderAdminStatus getDispOrderAdminStatus() {
		return dispOrderAdminStatus;
	}

	public void setDispOrderAdminStatus(DispOrderAdminStatus dispOrderAdminStatus) {
		this.dispOrderAdminStatus = dispOrderAdminStatus;
	}

	public Integer getOrgDispItemNum() {
		return orgDispItemNum;
	}

	public void setOrgDispItemNum(Integer orgDispItemNum) {
		this.orgDispItemNum = orgDispItemNum;
	}

	public Integer getDispItemNum() {
		return dispItemNum;
	}

	public void setDispItemNum(Integer dispItemNum) {
		this.dispItemNum = dispItemNum;
	}
}
