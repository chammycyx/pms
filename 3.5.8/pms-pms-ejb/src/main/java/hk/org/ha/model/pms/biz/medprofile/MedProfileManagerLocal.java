package hk.org.ha.model.pms.biz.medprofile;
import hk.org.ha.model.pms.dms.vo.PmsSpecialtyMap;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileOrder;
import hk.org.ha.model.pms.persistence.medprofile.Replenishment;
import hk.org.ha.model.pms.udt.disp.MpTrxType;
import hk.org.ha.model.pms.udt.medprofile.InactiveType;
import hk.org.ha.model.pms.udt.medprofile.WardTransferType;
import hk.org.ha.model.pms.vo.medprofile.PasContext;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Local;

import org.apache.commons.lang3.tuple.Pair;

@Local
public interface MedProfileManagerLocal {

	PasContext retrieveContext(MedProfile medProfile);
	PasContext retrieveContext(MedProfile medProfile, boolean updateBodyInfo, boolean updateItemTransferInfo);	
	void applyChanges(PasContext context, MedProfile medProfile);
	void updatePasInfo(MedProfile medProfile);
	void initializePasInfo(MedProfile medProfile,
			MedProfileOrder medProfileOrder);
	void mapSpecialty(MedProfile medProfile);
	void mapSpecialty(MedProfile medProfile, PmsSpecialtyMap pmsSpecialtyMap);
	void sendMessages(MpTrxType type, MedProfile medProfile,
			List<MedProfileMoItem> medProfileMoItemList);
	void sendMessagesWithNewTransaction(MpTrxType type, MedProfile medProfile,
			List<MedProfileMoItem> medProfileMoItemList);
	void directSendReplenishmentUpdateMessages(Hospital hospital,
			List<Replenishment> replenishmentList);
	void directSendMessages(Hospital hospital, MpTrxType type,
			List<MedProfileMoItem> medProfileMoItemList);
	boolean isTrxLogExists(MpTrxType type, MedProfile medProfile,
			Integer itemNum);	
	boolean isTrxLogExists(MpTrxType type, MedProfile medProfile,
			String refNum);
	void markProfileAsInactive(MedProfile medProfile, InactiveType inactiveType, Date dischargeDate, Date cancelAdmissionDate);	
	void initializeProfileState(MedProfile medProfile);
	void updatePasWardCodeRelatedSetting(Workstore workstore,
			String orgPasWardCode, MedProfile medProfile, boolean cancelWardTransfer);
	boolean updateSpecCodeAndWardCodeRelatedSetting(Workstore workstore, String orgSpecCode, String orgWardCode, boolean forceUpdateWardStock,
			MedProfile medProfile, List<MedProfileItem> medProfileItemList);
	Set<String> retrieveSentRefNumSet(MpTrxType type, MedProfile medProfile, List<String> itemNumList);
	Map<Long, Set<String>> retrieveSentRefNumMap(MpTrxType type, Set<Long> medProfileIdSet, List<String> refmNumList);
	Patient retrievePasPatient(String patHospCode, String caseNum);
	Patient retrievePasPatient(String hkid);
	Pair<String, Patient> retrievePasPatient(Workstore workstore, String caseNum);	
	Date retrieveItemCutOffDate(MedProfile medProfile);
	void refreshMedProfile(MedProfile medProfile, boolean refreshPasInfo,
			boolean refreshMrInfo);
	void refreshMedProfileList(List<MedProfile> medProfileList,
			boolean refreshPasInfo, boolean refreshMrInfo);
	Set<String> retrieveAtdpsItemCodeSet(Workstore workstore);	
	List<MedProfileItem> markItemTransferInfo(MedProfile medProfile, List<MedProfileItem> medProfileItemList, Date itemCutOffDate,
			WardTransferType type);
	void datebackTransferInfo(MedProfile medProfile, MedProfileItem medProfileItem, MedProfileMoItem medProfileMoItem, Date now);
	void savePatientAndMedCase(MedProfile medProfile);
	void saveChargeOrderNum(MedProfile medProfile);
	void removeNonProcessedPurchaseRequestListByMedProfileMoItem(MedProfile medProfile, MedProfileMoItem medProfileMoItem);
	void removeUnfinishedPivasRelatedRecords(MedProfileMoItem medProfileMoItem);
	void removeUnfinishedMaterialRequestItemsAndPivasOrderFromWorklist(MedProfile medProfile);	
	List<MedProfile> retrieveMedProfileList(String caseNum, String hkid, Workstore workstore);
	List<MedProfileItem> filterActiveMedProfileItemList(List<MedProfileItem> medProfileItemList, Date now);
}
