package hk.org.ha.model.pms.vo.rx;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class DoseGroupRemark extends DoseGroup implements Serializable {		
	
	private static final long serialVersionUID = 1L;

	private Boolean durationRemark;
	
	private Boolean durationUnitRemark;
	
	private Boolean startDateRemark;

	private Boolean endDateRemark;
	
	public DoseGroupRemark() {
		durationRemark = Boolean.FALSE;
		durationUnitRemark = Boolean.FALSE;
		startDateRemark = Boolean.FALSE;
		endDateRemark = Boolean.FALSE;
	}
	
	public Boolean getDurationRemark() {
		return durationRemark;
	}

	public void setDurationRemark(Boolean durationRemark) {
		this.durationRemark = durationRemark;
	}

	public Boolean getDurationUnitRemark() {
		return durationUnitRemark;
	}

	public void setDurationUnitRemark(Boolean durationUnitRemark) {
		this.durationUnitRemark = durationUnitRemark;
	}

	public Boolean getStartDateRemark() {
		return startDateRemark;
	}

	public void setStartDateRemark(Boolean startDateRemark) {
		this.startDateRemark = startDateRemark;
	}

	public Boolean getEndDateRemark() {
		return endDateRemark;
	}

	public void setEndDateRemark(Boolean endDateRemark) {
		this.endDateRemark = endDateRemark;
	}

	public List<DoseRemark> getDoseRemarkList() {
		List<Dose> tempDoseList = getDoseList();
		if (tempDoseList == null) {
			return null;
		}
		
		List<DoseRemark> doseRemarkList = new ArrayList<DoseRemark>();
		for (Dose tempDose : tempDoseList) {
			doseRemarkList.add((DoseRemark)tempDose);
		}
		
		return doseRemarkList;
	}

	public void setDoseRemarkList(List<DoseRemark> doseRemarkList) {
		if (doseRemarkList == null) {
			setDoseList(null);
		}
		
		List<Dose> tempDoseList = new ArrayList<Dose>();
		for (DoseRemark doseRemark : doseRemarkList) {
			tempDoseList.add(doseRemark);
		}
		setDoseList(tempDoseList);
	}
	
}

