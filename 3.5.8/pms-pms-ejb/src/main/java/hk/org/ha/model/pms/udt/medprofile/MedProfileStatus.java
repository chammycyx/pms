package hk.org.ha.model.pms.udt.medprofile;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

import java.util.Arrays;
import java.util.List;

public enum MedProfileStatus implements StringValuedEnum {

	Active("A", "Active"),
	PendingDischarge("P", "Pending Discharge"),
	HomeLeave("H", "Home Leave"),
	Inactive("D", "Discharge");
	
    public static final List<MedProfileStatus> Not_Inactive = Arrays.asList(
    		StringValuedEnumReflect.getValuesExclude(MedProfileStatus.class,Inactive));

    private final String dataValue;
    private final String displayValue;
    
	private MedProfileStatus(String dataValue, String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}

	@Override
	public String getDataValue() {
		return dataValue;
	}

	@Override
	public String getDisplayValue() {
		return displayValue;
	}

    public static MedProfileStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MedProfileStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<MedProfileStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<MedProfileStatus> getEnumClass() {
    		return MedProfileStatus.class;
    	}
    }
}
