package hk.org.ha.service.biz.pms.interfaces;

import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileOrder;
import hk.org.ha.model.pms.udt.disp.DispOrderBatchProcessingType;
import hk.org.ha.model.pms.udt.disp.OpTrxType;
import java.util.Date;
import java.util.List;

import org.osoa.sca.annotations.OneWay;

public interface PmsServiceJmsRemote {

	// call by pms-corp
	@OneWay
	void receiveNewOrder(MedOrder remoteMedOrder);
	
	@OneWay
	void receiveUpdateOrder(OpTrxType opTrxType, MedOrder remoteMedOrder);
	
	@OneWay
	void receiveConfirmRemark(MedOrder remoteMedOrder);
	
	@OneWay
	void receiveDiscontinue(OpTrxType opTrxType, MedOrder remoteMedOrder);
	
	@OneWay
	void receiveMedProfileOrder(MedProfileOrder medProfileOrder);
	
	@OneWay
	void receiveChemoOrder(MedOrder medOrder, List<MedProfileOrder> medProfileOrderList);
	
	@OneWay
	void updatePatient(List<MedOrder> medOrderList);
	
	@OneWay
	void markProcessingFlagType(String clusterCode, List<Long> dispOrderIdList, Boolean batchProcessingFlag, DispOrderBatchProcessingType batchProcessingType, int chunkSize);
	 
	@OneWay
	void updateDayEndCompleteInfoForDayEnd(String clusterCode, List<Long> dispOrderIdList, Date batchDate, String batchRemark, int chunkSize);

	@OneWay
	void updateDayEndCompleteInfoForUncollect(String clusterCode, List<Long> dispOrderIdListForDayEnd, List<Long> dispOrderIdListForNonDayEnd, String batchRemark, int chunkSize);
	
	@OneWay
	void updateDayEndCompleteInfoForSendSuspend(String clusterCode, List<Long> dispOrderIdListForSuspend, List<Long> dispOrderIdListForUnsuspend, String batchRemarkForSuspend, String batchRemarkForUnsuspend, int chunkSize);
	
	@OneWay
	void markDhLatestFlag(String clusterCode, String caseNum);
}
