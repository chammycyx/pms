package hk.org.ha.model.pms.vo.label;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedProperty;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ChestLabel")
@ExternalizedBean(type=DefaultExternalizer.class)
public class ChestLabel implements Serializable {
 
	private static final long serialVersionUID = 1L;

	@XmlElement(name="ChestLabelItem", required = true)	
	private List<ChestLabelItem> chestLabelItemList;
	
	@XmlElement(required = true)	
	private String patName;

	@XmlElement(required = true)	
	private String patNameChi;
	
	@XmlElement(required = true)	
	private String hospName;	
	
	@XmlElement(required = true)	
	private String hospNameChi;
	
	@XmlElement(required = true)	
	private String specCode;
	
	@XmlElement(required = true)	
	private Date doseDate;
	
	@XmlElement(required = true)	
	private Date dispDate;
	
	@XmlElement(required = true)	
	private String ticketNum;
	
	@XmlElement(required = true)	
	private String caseNum;
	
	@XmlElement(required = true)	
	private String ward;
	
	@XmlElement(required = true)	
	private String bedNum;
	
	@XmlElement(required = true)	
	private String patCatCode;
	
	@XmlElement(required = true)	
	private String userCode;
	
	@XmlElement(required = true)	
	private String doctorCode;

	@XmlElement(required = true)	
	private String hospCode;
	
	@XmlTransient
	private String printLang;

	@XmlElement(required = true)	
	private Boolean largeLayoutFlag;
	
	public ChestLabel() {
		largeLayoutFlag = Boolean.FALSE;
	}

	public String getHospName() {
		return hospName;
	}

	public void setHospName(String hospName) {
		this.hospName = hospName;
	}

	public String getHospNameChi() {
		return hospNameChi;
	}

	public void setHospNameChi(String hospNameChi) {
		this.hospNameChi = hospNameChi;
	}

	public Date getDispDate() {
		return dispDate;
	}

	public void setDispDate(Date dispDate) {
		this.dispDate = dispDate;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getWard() {
		return ward;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}

	public String getBedNum() {
		return bedNum;
	}

	public void setBedNum(String bedNum) {
		this.bedNum = bedNum;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getPrintLang() {
		return printLang;
	}

	public void setPrintLang(String printLang) {
		this.printLang = printLang;
	}

	public Boolean getLargeLayoutFlag() {
		return largeLayoutFlag;
	}

	public void setLargeLayoutFlag(Boolean largeLayoutFlag) {
		this.largeLayoutFlag = largeLayoutFlag;
	}

	@ExternalizedProperty
	public String getItemDescText() {
		StringBuilder itemDescText = new StringBuilder();
		for (ChestLabelItem chestLabelItem : chestLabelItemList) {
			if (itemDescText.length() > 0) {
				itemDescText.append("\n");
			}
			itemDescText.append(chestLabelItem.getItemDesc());
	    }
		return itemDescText.toString();
	}
	
	@ExternalizedProperty
	public String getDoseQtyText() {
		StringBuilder doseQtyText = new StringBuilder();
	    for (ChestLabelItem chestLabelItem : chestLabelItemList) {
			if (doseQtyText.length() > 0) {
				doseQtyText.append("\n");
			}
			doseQtyText.append(chestLabelItem.getDoseQty());
	    }
		return doseQtyText.toString();
	}
	
	@ExternalizedProperty
	public String getDoseUnitText() {
		StringBuilder doseUnitText = new StringBuilder();
	    for (ChestLabelItem chestLabelItem : chestLabelItemList) {
			if (doseUnitText.length() > 0) {
				doseUnitText.append("\n");
			}
	    	doseUnitText.append(chestLabelItem.getDoseUnit());
	    }
		return doseUnitText.toString();
	}
	
	@ExternalizedProperty
	public String getItemCodeText() {
		StringBuilder itemCodeText = new StringBuilder();
	    for (ChestLabelItem chestLabelItem : chestLabelItemList) {
			if (itemCodeText.length() > 0) {
				itemCodeText.append("\n");
			}
	    	itemCodeText.append(chestLabelItem.getItemCode());
	    }
		return itemCodeText.toString();
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getPatNameChi() {
		return patNameChi;
	}

	public void setPatNameChi(String patNameChi) {
		this.patNameChi = patNameChi;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public String getPatCatCode() {
		return patCatCode;
	}

	public void setPatCatCode(String patCatCode) {
		this.patCatCode = patCatCode;
	}

	public void setChestLabelItemList(List<ChestLabelItem> chestLabelItemList) {
		this.chestLabelItemList = chestLabelItemList;
	}

	public List<ChestLabelItem> getChestLabelItemList() {
		return chestLabelItemList;
	}

	public void setDoseDate(Date doseDate) {
		this.doseDate = doseDate;
	}

	public Date getDoseDate() {
		return doseDate;
	}
}