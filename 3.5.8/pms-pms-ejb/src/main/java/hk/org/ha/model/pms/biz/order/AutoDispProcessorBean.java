package hk.org.ha.model.pms.biz.order;

import hk.org.ha.model.pms.biz.order.AutoDispConfig.AutoDispWorkstation;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.udt.reftable.OperationProfileType;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("autoDispProcessor")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class AutoDispProcessorBean implements AutoDispProcessorLocal {

	@Logger
	private Log logger;	
	
	@In
	private AutoDispSchedulerInf autoDispScheduler;

	@In
	private AutoDispManagerLocal autoDispManager;
	
	@In
	private OrderManagerLocal orderManager;
			
	@In
	private AutoDispConfig autoDispConfig;
	
	@Override
	public void processMedOrder(Long startupDelay, Workstore workstore, String workstationKey) {

		AutoDispWorkstation workstation = autoDispConfig.getAutoDispWorkstation(workstore, workstationKey);

		logger.info("AutoDisp #0", workstation);
		
		try {

			autoDispManager.login(workstation.getUserId(), workstation.getParent().getWorkstore(), workstation.getWorkstationCode());
			
			int errorCount = 0;
			String nextOrderNum = autoDispScheduler.getNextOrderNumToRun(workstore);
			while (nextOrderNum != null) {
				
				logger.info("AutoDisp #0 orderNum:#1", 
						workstation, 
						nextOrderNum);
				
				long startTime = System.currentTimeMillis();
				
				try {
					MedOrder medOrder = null;					
					do {					
						medOrder = orderManager.retrieveMedOrder(nextOrderNum);
						if (medOrder == null) {
							logger.info("AutoDisp #0 orderNum:#1 still not commit, retry after 2000ms", workstation, nextOrderNum);
							Thread.sleep(2000L);
						}
					} while (medOrder == null);
					
					processMedOrder(medOrder, workstation);
					errorCount = 0;
				} catch (Exception e1) {
					logger.error("AutoDisp #0 orderNum:#1 ERROR OCCUR!", e1, workstation, nextOrderNum);
					errorCount++;
					if (errorCount == 10) {
						throw new Exception("Error continuously occurs 10 times.", e1);
					}
				}
				
				logger.info("AutoDisp #0 orderNum:#1 timeused:#2", 
						workstation, 
						nextOrderNum, 
						System.currentTimeMillis()-startTime);		
				
				nextOrderNum = autoDispScheduler.getNextOrderNumToRun(workstore);

				if (nextOrderNum != null &&
						workstation.getPreOrderSleepTime() > 0) {
					
					logger.info("AutoDisp #0 sleep for #1 ...",
							workstation,
							workstation.getPreOrderSleepTime());
					try {
						Thread.sleep(workstation.getPreOrderSleepTime());
					} catch (InterruptedException e) {
					}
				}
			};
			
			autoDispManager.logout();

			logger.info("AutoDisp #0 completed.", workstation);
			
		} catch (Exception e) {
			
			logger.error("AutoDisp #0 stopped.", e, workstation);	
			
		} finally {
			
			autoDispScheduler.checkInWorkstation(workstore, workstationKey);
		}
	}
	
	private void processMedOrder(MedOrder medOrder, AutoDispWorkstation workstation) throws Exception {
						
		try {
			OperationProfile activeProfile = autoDispManager.getActiveProfile(medOrder.getWorkstore());
			
			autoDispManager.genTicket();	
			
			autoDispManager.retrieveOrder(medOrder.getOrderNum());
			
			autoDispManager.retrievePatientAndMedCase("A1234563", "Tester," + workstation.getUserId());
			
			autoDispManager.checkTicket();
			
			autoDispManager.startVetting();
			
			autoDispManager.removeItemsIfNotConverted();

			autoDispManager.endVetting();
			
			if (activeProfile.getType() != OperationProfileType.Pms) {
			
				autoDispManager.drugPick();
				
				autoDispManager.assembling();
				
				autoDispManager.checkOrder();
				
				autoDispManager.issueOrder();
			}
		} catch (Exception e) {
			logger.error("Unexpected Error!", e);
			throw e;
		}
	}
}
