package hk.org.ha.model.pms.biz.reftable;

import javax.ejb.Local;

@Local
public interface CompanyListServiceLocal {
	
	void retrieveCompanyList(String prefixCompanyCode);
	
	void retrieveManufacturerList(String prefixCompanyCode);
	
	void destroy();
	
}
 