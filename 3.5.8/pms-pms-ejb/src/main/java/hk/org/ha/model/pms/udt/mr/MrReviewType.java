package hk.org.ha.model.pms.udt.mr;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum MrReviewType implements StringValuedEnum {
	
	Annotation("A", "Annotation"),
	Discontinue("D", "Discontinue");

	private final String dataValue;
	private final String displayValue;

	MrReviewType(final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}
	
	public static MrReviewType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MrReviewType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<MrReviewType> {

		private static final long serialVersionUID = 1L;

		public Class<MrReviewType> getEnumClass() {
    		return MrReviewType.class;
    	}
    }
}
