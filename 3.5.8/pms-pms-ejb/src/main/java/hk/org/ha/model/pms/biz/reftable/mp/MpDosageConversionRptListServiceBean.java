package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.corp.cache.DmFormCacherInf;
import hk.org.ha.model.pms.corp.cache.MsFmStatusCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmForm;
import hk.org.ha.model.pms.dms.persistence.MsFmStatus;
import hk.org.ha.model.pms.dms.persistence.MsWorkstoreDrug;
import hk.org.ha.model.pms.dms.persistence.PmsIpDosageConv;
import hk.org.ha.model.pms.dms.persistence.PmsIpDosageConvSet;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.reftable.mp.DosageConversionType;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.MpDosageConversionRpt;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsRptServiceJmsRemote;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("mpDosageConversionRptListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MpDosageConversionRptListServiceBean implements MpDosageConversionRptListServiceLocal {
	
	@In
	private Workstore workstore;
	
	@In
	private DmsRptServiceJmsRemote dmsRptServiceProxy;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private MsFmStatusCacherInf msFmStatusCacher;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent; 
	
	@In
	private DmFormCacherInf dmFormCacher;
	
	private List<MpDosageConversionRpt> mpDosageConversionRptList;
	
	private Map<String, Object> parameters = new HashMap<String, Object>();
	
	private boolean retrieveSuccess;
	
	public void genMpDosageConversionRptList() {
		parameters.put("hospCode", workstore.getHospCode());
		parameters.put("workstoreCode", workstore.getWorkstoreCode());	

		List<PmsIpDosageConvSet> pmsIpDosageConvSetList = dmsRptServiceProxy.retrievePmsIpDosageConvSetList(workstore.getHospCode(), workstore.getWorkstoreCode());
		mpDosageConversionRptList = new ArrayList<MpDosageConversionRpt>();
			
		for (PmsIpDosageConvSet pmsIpDosageConvSet : pmsIpDosageConvSetList) {
			StringBuilder drug = new StringBuilder();
			
			if( pmsIpDosageConvSet.getDmDrugProperty() != null ){
				drug.append(pmsIpDosageConvSet.getDmDrugProperty().getDisplayname()).append(" ");   
				
				if (pmsIpDosageConvSet.getDmDrugProperty().getSaltProperty()!=null) {
					drug.append(pmsIpDosageConvSet.getDmDrugProperty().getSaltProperty()).append(" ");
				}
				
				DmForm dmForm = dmFormCacher.getFormByFormCode(pmsIpDosageConvSet.getDmDrugProperty().getFormCode());
	
		    	if ( "Y".equals(dmForm.getDisplayRouteDescIndicator()) && dmForm.getDmRoute()!=null ){
					drug.append(StringUtils.trimToEmpty( StringUtils.trimToEmpty(dmForm.getDmRoute().getFullRouteDesc())))
						.append(" ")
						.append(StringUtils.trimToEmpty( dmForm.getMoeDesc()));
				} else {
					drug.append(StringUtils.trimToEmpty( dmForm.getMoeDesc()));
				}
			
				MsFmStatus msFmStatus = msFmStatusCacher.getMsFmStatusByFmStatus(pmsIpDosageConvSet.getPmsFmStatus());
				if (msFmStatus != null && !FmStatus.GeneralDrug.getDataValue().equals(msFmStatus.getFmStatus())) {
					drug.append(" ").append("<").append(msFmStatus.getFmStatusDesc()).append(">");
				}
			}
			
			for( PmsIpDosageConv pmsIpDosageConv : pmsIpDosageConvSet.getPmsIpDosageConvList() ){
				MpDosageConversionRpt mpDosageConversionRpt = new MpDosageConversionRpt();
				mpDosageConversionRpt.setDrug(drug.toString());

				mpDosageConversionRpt.setMoDosageFrom( pmsIpDosageConvSet.getMoDosageFrom() );

				if( pmsIpDosageConvSet.getMoDosageTo() != null && pmsIpDosageConvSet.getMoDosageTo() > 0 ){
					mpDosageConversionRpt.setMoDosageTo( pmsIpDosageConvSet.getMoDosageTo() );
				}

				mpDosageConversionRpt.setFixed( DosageConversionType.dataValueOf(pmsIpDosageConv.getFixed()).getDisplayValue() );
								
				if (StringUtils.isNumeric( StringUtils.left(pmsIpDosageConvSet.getMoDosageUnit(),1))) {
					mpDosageConversionRpt.setMoDosageUnitMultiply("x ");
				} else {
					mpDosageConversionRpt.setMoDosageUnitMultiply("");
				}

				mpDosageConversionRpt.setMoDosageUnit( pmsIpDosageConvSet.getMoDosageUnit() );
				mpDosageConversionRpt.setItemCode(pmsIpDosageConv.getItemCode());	
				
				if(pmsIpDosageConv.getDmDrug() != null){
					mpDosageConversionRpt.setFullDrugDesc(pmsIpDosageConv.getDmDrug().getFullDrugDesc());
					
					MsWorkstoreDrug msWorkstoreDrug = dmDrugCacher.getMsWorkstoreDrug(workstore, pmsIpDosageConv.getDmDrug().getItemCode());
					if (msWorkstoreDrug != null) {
						mpDosageConversionRpt.setSuspend(msWorkstoreDrug.getSuspend());
					}else {
						mpDosageConversionRpt.setSuspend("Y");
					}
				}
				
				if( pmsIpDosageConv.getDispenseDosage() != null ){
					mpDosageConversionRpt.setDispDosage( pmsIpDosageConv.getDispenseDosage() );
					
					if (StringUtils.isNumeric( StringUtils.left(pmsIpDosageConv.getDispenseDosageUnit(),1))) {
						mpDosageConversionRpt.setDispDosageUnitMultiply("x ");
					} else {
						mpDosageConversionRpt.setDispDosageUnitMultiply("");
					}

					mpDosageConversionRpt.setDispDosageUnit(pmsIpDosageConv.getDispenseDosageUnit());
				}
				
				mpDosageConversionRptList.add(mpDosageConversionRpt);
			}
		}
		Collections.sort(mpDosageConversionRptList, new MpDosageConversionRptComparator());
		
		if ( mpDosageConversionRptList.isEmpty() ) {
			retrieveSuccess = false;
		} else{
			retrieveSuccess = true;
		}
	}
	
	public void exportMpDosageConversionRpt() {	
		if (retrieveSuccess) {
			PrintOption po = new PrintOption();
			po.setDocType(ReportProvider.XLS);
			printAgent.renderAndRedirect(new RenderJob(
					"MpDosageConversionXls", 
					po, 
					parameters, 
					mpDosageConversionRptList));
		}
	}
	
	public void printMpDosageConversionRpt(){
		genMpDosageConversionRptList();
		printAgent.renderAndPrint(new RenderAndPrintJob(
				"MpDosageConversionRpt", 
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				parameters, 
				mpDosageConversionRptList));
	}
	
	public static class MpDosageConversionRptComparator implements Comparator<MpDosageConversionRpt>, Serializable {

		private static final long serialVersionUID = 1L;
		
		@Override
		public int compare(MpDosageConversionRpt o1, MpDosageConversionRpt o2) {			
			return new CompareToBuilder().append( o1.getDrug(), o2.getDrug())
										.append( o1.getMoDosageFrom(), o2.getMoDosageFrom())
										.append( o1.getMoDosageUnit(), o2.getMoDosageUnit())
										.append( o1.getItemCode(), o2.getItemCode())
										.toComparison();			
		}
	}
	
	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}
	
	@Remove
	public void destroy() {
		if (mpDosageConversionRptList != null){
			mpDosageConversionRptList = null;
		}
	}	
}
