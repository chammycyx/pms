package hk.org.ha.model.pms.vo.drug;

import hk.org.ha.model.pms.corp.cache.DmAdminTimeCacher;
import hk.org.ha.model.pms.dms.persistence.DmAdminTime;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
import org.springframework.beans.BeanUtils;

@ExternalizedBean(type=DefaultExternalizer.class)
public class DmAdminTimeLite implements Serializable {
	private static final long serialVersionUID = 1L;
	private String adminTimeCode;
	private String adminTimeEng;
	private String adminTimeChi;
	
	public static DmAdminTimeLite objValueOf(String adminTimeCode) {
		return objValueOf(DmAdminTimeCacher.instance().getDmAdminTimeByAdminTimeCode(adminTimeCode));
	}
	
	public static DmAdminTimeLite objValueOf(DmAdminTime dmAdminTime) {
		if (dmAdminTime != null) {
			DmAdminTimeLite dmAdminTimeLite = new DmAdminTimeLite();
			BeanUtils.copyProperties(dmAdminTime, dmAdminTimeLite);
			return dmAdminTimeLite;
		}
		return null;
	}
	
	public void setAdminTimeCode(String adminTimeCode) {
		this.adminTimeCode = adminTimeCode;
	}
	public String getAdminTimeCode() {
		return adminTimeCode;
	}
	public void setAdminTimeEng(String adminTimeEng) {
		this.adminTimeEng = adminTimeEng;
	}
	public String getAdminTimeEng() {
		return adminTimeEng;
	}
	public void setAdminTimeChi(String adminTimeChi) {
		this.adminTimeChi = adminTimeChi;
	}
	public String getAdminTimeChi() {
		return adminTimeChi;
	}
}
