package hk.org.ha.model.pms.vo.alert.ehr;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class EhrAllergyDetail implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<AllergenDetail> allergenDetailList;
	
	private List<AdrDetail> adrDetailList;

	public List<AllergenDetail> getAllergenDetailList() {
		return allergenDetailList;
	}

	public void setAllergenDetailList(List<AllergenDetail> allergenDetailList) {
		this.allergenDetailList = allergenDetailList;
	}

	public List<AdrDetail> getAdrDetailList() {
		return adrDetailList;
	}

	public void setAdrDetailList(List<AdrDetail> adrDetailList) {
		this.adrDetailList = adrDetailList;
	}
}
