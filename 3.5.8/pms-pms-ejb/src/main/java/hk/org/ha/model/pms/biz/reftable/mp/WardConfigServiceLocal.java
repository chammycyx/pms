package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.model.pms.persistence.reftable.WardConfig;

import javax.ejb.Local;

@Local
public interface WardConfigServiceLocal {

	void retrieveWardConfig(String wardCode);
	
	void updateWardConfig(WardConfig wardConfig);
	
	void refreshMedProfiles(WardConfig wardConfig, boolean refreshPasInfo,
			boolean refreshMrInfo);
	
	void destroy();
}
