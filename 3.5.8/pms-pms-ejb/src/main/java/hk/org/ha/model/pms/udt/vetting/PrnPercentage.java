package hk.org.ha.model.pms.udt.vetting;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PrnPercentage implements StringValuedEnum {
	
	Zero("0", " "),
	TwentyFive("25", "25%"),
	Fifty("50", "50%"),
	SeventyFive("75", "75%"),
	Hundred("100", "100%");
	
	private final String dataValue;
	private final String displayValue;
	
	PrnPercentage (final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}

	public static PrnPercentage dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PrnPercentage.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<PrnPercentage> {

		private static final long serialVersionUID = 1L;

		public Class<PrnPercentage> getEnumClass() {
    		return PrnPercentage.class;
    	}
    }
}
