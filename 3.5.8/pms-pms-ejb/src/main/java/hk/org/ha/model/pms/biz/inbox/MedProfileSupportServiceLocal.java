package hk.org.ha.model.pms.biz.inbox;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.udt.medprofile.ManualTransferResult;

import java.util.Date;

import javax.ejb.Local;

@Local
public interface MedProfileSupportServiceLocal {

	ManualTransferResult manualTransfer(MedProfile medProfile, boolean transferFlag, Date transferDate,
    		boolean applyImmediately, Date itemCutOffDate);
	void forceProcessPendingOrders(MedProfile medProfile);
	void recalculateDashboardRelatedStat();
	void destroy();
}
