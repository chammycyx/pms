package hk.org.ha.model.pms.biz.prevet;

import static hk.org.ha.model.pms.prop.Prop.ALERT_EHR_PROFILE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.ALERT_PROFILE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.PATIENT_MOE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.PATIENT_PAS_PATIENT_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.VETTING_DRUGONHAND_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.VETTING_DRUGONHAND_ONHANDSOURCETYPE;
import static hk.org.ha.model.pms.prop.Prop.VETTING_MEDPROFILE_ALERT_PROFILE_ENABLED;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.sys.SysProfile;
import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.fmk.pms.util.QueryUtils;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.alert.ehr.EhrAllergyException;
import hk.org.ha.model.pms.biz.alert.AlertProfileManagerLocal;
import hk.org.ha.model.pms.biz.alert.ehr.EhrAlertManagerLocal;
import hk.org.ha.model.pms.biz.inbox.PharmInboxClientNotifier;
import hk.org.ha.model.pms.biz.medprofile.MedProfileItemConverterLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileManagerLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.biz.onestop.OneStopOrderStatusManagerLocal;
import hk.org.ha.model.pms.biz.vetting.FmIndicationManagerLocal;
import hk.org.ha.model.pms.biz.vetting.VettingManagerLocal;
import hk.org.ha.model.pms.pas.PasServiceWrapper;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderFm;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.uam.udt.AuthorizeStatus;
import hk.org.ha.model.pms.uam.vo.AuthenticateCriteria;
import hk.org.ha.model.pms.uam.vo.AuthenticateResult;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.disp.OnHandSourceType;
import hk.org.ha.model.pms.udt.disp.RemarkItemStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileMoItemStatus;
import hk.org.ha.model.pms.udt.prevet.PreVetMode;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.OnHandProfileOrderType;
import hk.org.ha.model.pms.vo.alert.AlertProfile;
import hk.org.ha.model.pms.vo.ehr.EhrPatient;
import hk.org.ha.model.pms.vo.prevet.PreVetAuth;
import hk.org.ha.model.pms.vo.rx.CapdRxDrug;
import hk.org.ha.model.pms.vo.rx.IvAdditive;
import hk.org.ha.model.pms.vo.rx.IvFluid;
import hk.org.ha.model.pms.vo.rx.IvRxDrug;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;
import hk.org.ha.model.pms.vo.security.PropMap;
import hk.org.ha.model.pms.vo.vetting.OnHandProfile;
import hk.org.ha.model.pms.vo.vetting.OnHandProfileCriteria;
import hk.org.ha.service.biz.pms.uam.interfaces.UamServiceJmsRemote;
import hk.org.ha.service.pms.asa.interfaces.moe.MoeServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;
import org.jboss.seam.security.Identity;
import org.springframework.util.CollectionUtils;

@Stateful
@Scope(ScopeType.SESSION)
@Name("preVetService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PreVetServiceBean implements PreVetServiceLocal {

	private static final String MED_ORDER_QUERY_HINT_BATCH_1 = "o.patient";
	private static final String MED_ORDER_QUERY_HINT_BATCH_2 = "o.medCase";
	private static final String MED_ORDER_QUERY_HINT_BATCH_3 = "o.medOrderItemList.medOrderItemAlertList";
	private static final String MED_ORDER_QUERY_HINT_BATCH_4 = "o.ticket";
	private static final String MED_ORDER_QUERY_HINT_COMMON_1 = "o.medOrderFmList";
	
	
	private static final String MED_PROFILE_MO_ITEM_QUERY_HINT_FETCH_1 = "o.medProfileItem.medProfile";
	
	private static final MedOrderItemComparator MED_ORDER_ITEM_COMPARATOR = new MedOrderItemComparator();
	
	private static final OnHandDrugItemComparator ON_HAND_DRUG_ITEM_COMPARATOR = new OnHandDrugItemComparator();
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private MoeServiceJmsRemote moeServiceProxy;
	
	@In
	private MedProfileManagerLocal medProfileManager;
	
	@In
	private MedProfileItemConverterLocal medProfileItemConverter;
	
	@In
	private AlertProfileManagerLocal alertProfileManager;
	
	@In
	private EhrAlertManagerLocal ehrAlertManager;
	
	@In
	private FmIndicationManagerLocal fmIndicationManager; 
	
	@In
	private PharmInboxClientNotifier pharmInboxClientNotifier;
	
	@In
	private Identity identity;
	
	@In
	private Workstation workstation;
	
	@In
	private UamInfo uamInfo;
	
	@In
	private UamServiceJmsRemote uamServiceProxy;
	
	@In
	private SysProfile sysProfile;
	
	@Out(required = false)
	private Patient dischargePatient;
	
	@Out(required = false)
	private MedOrder dischargeMedOrder;
	
	@Out(required = false)
	private List<Object> dischargeMedProfileMoItemList;
	
	@Out(required = false)
	protected AlertProfile dischargeAlertProfile;
	
	@Out(required = false)
	protected boolean alertServiceError;
	
	@Out(required = false)
	protected boolean ehrAlertServiceError;
	
	@Out(required = false)
	protected boolean drugOnHandServiceError;
	
	@Out(required = false)
	private boolean orderLocked;
	
	@Out(required = false)
	protected boolean readOnly;
	
	@Out(required = false)
	protected String lockedWorkstationHostName;
	
	@Out(required = false)
	private OnHandSourceType preVetOnHandSourceType;
	
	@In
	private OneStopOrderStatusManagerLocal oneStopOrderStatusManager;
		
	@In
	private VettingManagerLocal vettingManager;
	
	@In
	private PasServiceWrapper pasServiceWrapper;

	@In
	private PropMap propMap;
	
    public PreVetServiceBean() {
    }
        
    @Override
    public String retrieveDischargeMedOrder(String orderNum, Patient patient, PreVetMode preVetMode, Long medOrderId) {
    	
    	String orderStatusMsg = null;
    	//check order status for OneStopEnty only before switch to Pre-Vet View
    	if (preVetMode == PreVetMode.OneStopEntry)
    	{
    		orderStatusMsg = oneStopOrderStatusManager.verifyMedOrderStatusBeforeSwitchToVetting(medOrderId);
    		if (!StringUtils.isBlank(orderStatusMsg))
        	{
        		return orderStatusMsg;
        	}
    	}
    	    	
    	dischargeMedOrder = fetchMedOrder(orderNum);
    	orderLocked = false;
    	readOnly = false;
    	lockedWorkstationHostName = null;
    	
    	if (preVetMode == PreVetMode.OneStopEntry)
    	{
    		orderStatusMsg = vettingManager.startVetting(dischargeMedOrder, patient.getHkid()).getMessageCode();
    		
    		if (!StringUtils.isBlank(orderStatusMsg))
        	{
        		return orderStatusMsg;
        	}
    	}
    	else if (preVetMode == PreVetMode.Inbox)
    	{
    		orderLocked = Boolean.TRUE.equals(dischargeMedOrder.getProcessingFlag()) &&
				!workstation.getId().equals(dischargeMedOrder.getWorkstationId());
    		readOnly = orderLocked || !MedOrderStatus.Outstanding_Withhold.contains(dischargeMedOrder.getStatus());
    		lockedWorkstationHostName = orderLocked ? retrieveWorkstationHostName(dischargeMedOrder.getWorkstationId()) : null;
    		
    		if (!dischargeMedOrder.getProcessingFlag())
    		{
    			dischargeMedOrder.setProcessingFlag(Boolean.TRUE); // mark processing
    			dischargeMedOrder.setWorkstationId(workstation.getId());
    		}
    	}
    	
    	dischargePatient = retrievePasPatient(dischargeMedOrder, patient);
    	if ( dischargePatient != null ) {
    		dischargePatient.setEhrPatient(retrieveEhrPatient(dischargeMedOrder, dischargePatient));
    	}
    	dischargeAlertProfile = retrieveAlertProfile(dischargeMedOrder, dischargePatient);
    	dischargeMedProfileMoItemList = retrieveOnHandAndMarItemList(dischargeMedOrder, dischargePatient);
    	em.clear();
    	medProfileItemConverter.formatDischargeMoItemList(dischargeMedOrder.getMedOrderItemList());
    	sortAndDiff(dischargeMedOrder.getMedOrderItemList(), dischargeMedProfileMoItemList);
    	return orderStatusMsg;    	
    }
    
    @Override
    public String preVetMedOrder(MedOrder medOrder, PreVetMode preVetMode, String userId, String userDisplayName) {
    	String orderStatusMsg = oneStopOrderStatusManager.verifyMedOrderStatusFromPreVetOperation(medOrder);
    	
    	if (!StringUtils.isBlank(orderStatusMsg))
    	{
    		return orderStatusMsg;
    	}
    	
    	Date now = new Date();
    	
    	if (preVetMode == PreVetMode.OneStopEntry)
    	{
    		vettingManager.releaseOrder(null, medOrder);
    		if (!StringUtils.isBlank(medOrder.getPrevOrderNum()))
			{
				medOrder.setTicket(vettingManager.retrievePrevTicket(medOrder.getPrevOrderNum()));
			}
			else
			{
				medOrder.setTicket(null);
			}
    	}
    	else
    	{
    		medOrder.setProcessingFlag(Boolean.FALSE);
    		medOrder.setWorkstationId(null);
    	}
    	
    	if (medOrder.getStatus() == MedOrderStatus.Withhold)
    	{
    		medOrder.setWithholdUser(userId);
    		medOrder.setWithholdUserName((StringUtils.isBlank(userDisplayName)?null:userDisplayName));
    		medOrder.setWithholdDate(now);
    	}
    	else if (medOrder.getStatus() == MedOrderStatus.PreVet) {
    		medOrder.setPreVetUser(userId);
    		medOrder.setPreVetUserName((StringUtils.isBlank(userDisplayName)?null:userDisplayName));
    		medOrder.setPreVetDate(now);
    	}
    	
    	for (MedOrderItem medOrderItem: medOrder.getMedOrderItemList()) {
    		if (StringUtils.isNotEmpty(medOrderItem.getPreVetNoteText()) && medOrderItem.getPreVetNoteDate() == null) {
    			medOrderItem.setPreVetNoteDate(now);
    			medOrderItem.setPreVetNoteUser(identity.getCredentials().getUsername());
    		}
    	}
    	
    	em.merge(medOrder);
    	em.flush();
    	pharmInboxClientNotifier.dispatchUpdateEvent(medOrder);
    	
    	return orderStatusMsg;
    }
    
    @Override
    public void releaseMedOrder(MedOrder medOrder, PreVetMode preVetMode) 
    {
    	MedOrder medOrderInDB = fetchMedOrder(medOrder.getOrderNum());
    	if (medOrderInDB.getVersion().equals(medOrder.getVersion())) 
    	{
    		if (preVetMode == PreVetMode.OneStopEntry)
    		{
    			vettingManager.releaseOrder(null, medOrderInDB);
    			if (!StringUtils.isBlank(medOrderInDB.getPrevOrderNum()))
    			{
    				medOrder.setTicket(vettingManager.retrievePrevTicket(medOrderInDB.getPrevOrderNum()));
    			}
    			else
    			{
    				medOrder.setTicket(null);
    			}
    		}
    		else
    		{
    			if (workstation.getId().equals(dischargeMedOrder.getWorkstationId()))
    		    {
    				medOrderInDB.setProcessingFlag(Boolean.FALSE);
        			medOrderInDB.setWorkstationId(null);
        			em.merge(medOrderInDB);
    		    }	
    		}
    	}
    }
        
    private AlertProfile retrieveAlertProfile(MedOrder medOrder, Patient patient) {
    	
    	alertServiceError = false;
    	ehrAlertServiceError = false;
    	
    	AlertProfile alertProfile = null;
    	if (patient != null) {
        	if (ALERT_PROFILE_ENABLED.get(Boolean.FALSE) &&
    				VETTING_MEDPROFILE_ALERT_PROFILE_ENABLED.get(Boolean.FALSE)) {
    			try {
    				alertProfile = alertProfileManager.retrieveAlertProfileByMedOrder(medOrder, patient);
    			} catch (Exception ex) {
    				logger.error(ex, ex);
    				alertServiceError = true;
    			}
    			
    		}

    		if (ALERT_EHR_PROFILE_ENABLED.get()) {
    	    	try {
    				alertProfile = ehrAlertManager.retrieveEhrAllergySummaryByMedOrder(alertProfile, medOrder, patient);
    			} catch (EhrAllergyException e) {
    				logger.error(e, e);
    				ehrAlertServiceError = true;
    			}
    		}
    	}
		return alertProfile;
    }    
    
    private void sortAndDiff(List<MedOrderItem> medOrderItemList, List<Object> itemList) {
    	
    	Collections.sort(medOrderItemList, MED_ORDER_ITEM_COMPARATOR);
    	
    	List<Object> clonedItemList = new ArrayList<Object>();
    	clonedItemList.addAll(itemList);
    	itemList.clear();
    	
    	for (MedOrderItem medOrderItem: medOrderItemList) {
    		
	    	for (Object item: itemList) {
	    		if (medOrderItem.getStatus() != MedOrderItemStatus.SysDeleted && equalDrugs(medOrderItem, item)) {
	    			markItemRelationship(item, medOrderItem);
	    		}
	    	}
    		
    		Iterator<Object> iter = clonedItemList.iterator();
    		while (iter.hasNext()) {
    			Object item = iter.next();
    			if (medOrderItem.getStatus() != MedOrderItemStatus.SysDeleted && equalDrugs(medOrderItem, item)) {
    				iter.remove();
	    			markItemRelationship(item, medOrderItem);
    				itemList.add(item);
    			}
    		}
    	}
    	itemList.addAll(clonedItemList);
    }
    
    private void markItemRelationship(Object item, MedOrderItem medOrderItem) {
    	
    	medOrderItem.setUniqueDrugFlag(Boolean.FALSE);
    	
    	if (item instanceof MedOrderItem) {
			((MedOrderItem) item).setUniqueDrugFlag(Boolean.FALSE);
			((MedOrderItem) item).addRelatedMedOrderItemId(medOrderItem.getId());
    	} else if (item instanceof MedProfileMoItem){
			((MedProfileMoItem) item).setUniqueDrugFlag(Boolean.FALSE);
			((MedProfileMoItem) item).addRelatedMedOrderItemId(medOrderItem.getId());
    	}
    }
    
    private boolean equalDrugs(Object item1, Object item2) {
    	return MedProfileUtils.equalDrugs(resolveRxItem(item1), resolveRxItem(item2), true);
    }
    
    private RxItem resolveRxItem(Object item) {
    	
    	if (item instanceof MedOrderItem) {
    		return ((MedOrderItem) item).getRxItem();
    	} else if (item instanceof MedProfileMoItem) {
    		return ((MedProfileMoItem) item).getRxItem();
    	}
    	return null;
    }
    
    private String retrieveWorkstationHostName(Long workstationId) {
    	return em.find(Workstation.class, workstationId).getHostName();
    }
    
    @SuppressWarnings("unchecked")
	private MedOrder fetchMedOrder(String orderNum) {
    	
    	MedOrder medOrder = (MedOrder) MedProfileUtils.getFirstItem(em.createQuery(
    			"select o from MedOrder o" + // 20150206 index check : MedOrder.orderNum : I_MED_ORDER_01
    			" where o.orderNum = :orderNum" +
    			" and o.status <> :status" +
    			" order by o.createDate desc")
    			.setParameter("orderNum", orderNum)
    			.setParameter("status", MedOrderStatus.SysDeleted)
				.setHint(QueryHints.BATCH, MED_ORDER_QUERY_HINT_BATCH_1)
				.setHint(QueryHints.BATCH, MED_ORDER_QUERY_HINT_BATCH_2)
				.setHint(QueryHints.BATCH, MED_ORDER_QUERY_HINT_BATCH_3)
				.setHint(QueryHints.BATCH, MED_ORDER_QUERY_HINT_BATCH_4)
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
    			.getResultList());
    	
    	MedProfileUtils.lookup(medOrder, MED_ORDER_QUERY_HINT_BATCH_1);
    	MedProfileUtils.lookup(medOrder, MED_ORDER_QUERY_HINT_BATCH_2);
    	MedProfileUtils.lookup(medOrder, MED_ORDER_QUERY_HINT_BATCH_3);
    	MedProfileUtils.lookup(medOrder, MED_ORDER_QUERY_HINT_BATCH_4);
    	if (medOrder != null) {
    		medOrder.loadChild();
    	}
    	
    	return medOrder;
    }
    
    private List<Object> retrieveOnHandAndMarItemList(MedOrder medOrder, Patient patient) {
    	
    	drugOnHandServiceError = false;
    	
    	Date refDate = new Date();
    	
    	if (medOrder.getMedCase() != null) {
    		MedCase pasMedCase = MedProfileUtils.lookupPasMedCase(patient, medOrder.getMedCase().getCaseNum());
    		if (pasMedCase != null && pasMedCase.getDischargeDate() != null) {
    			refDate = pasMedCase.getDischargeDate();
    		}
    	}
    	
    	List<Object> resultList = new ArrayList<Object>();
    	
    	resultList.addAll(fetchMedProfileMoItemList(medOrder, refDate));
    	
    	if (patient != null && VETTING_DRUGONHAND_ENABLED.get(Boolean.FALSE) && PATIENT_MOE_ENABLED.get(Boolean.FALSE)) {
    		
    		try {
    			
    			OnHandProfileCriteria onHandProfileCriteria = new OnHandProfileCriteria();
    			onHandProfileCriteria.setCaseNum(medOrder.getMedCase().getCaseNum());
    			if ( OnHandSourceType.Corporate.getDataValue().equals(propMap.getValue(VETTING_DRUGONHAND_ONHANDSOURCETYPE.getName())) ) {
					onHandProfileCriteria.setCorpOnHandEnabled(Boolean.TRUE);
				} else {
					onHandProfileCriteria.setCorpOnHandEnabled(Boolean.FALSE);
				}
    			onHandProfileCriteria.setHkid(patient.getHkid());
    			onHandProfileCriteria.setOnHandProfileOrderType(OnHandProfileOrderType.MedOrder); //Discharge order should be MoeOrder
    			onHandProfileCriteria.setOnHandSourceType(medOrder.getOnHandSourceType());
    			onHandProfileCriteria.setOrderDate(MedProfileUtils.getDayBegin(refDate));
    			onHandProfileCriteria.setOrderSubType(medOrder.getCmsOrderSubType());
    			onHandProfileCriteria.setOrderType(medOrder.getCmsOrderType());
    			onHandProfileCriteria.setPasPatientEnabled(propMap.getValueAsBoolean(PATIENT_PAS_PATIENT_ENABLED.getName()));
    			onHandProfileCriteria.setPatHospCode(medOrder.getPatHospCode());
    			onHandProfileCriteria.setPatKey(patient.getPatKey());
    			
    			Pair<OnHandSourceType, List<OnHandProfile>> result = moeServiceProxy.retrieveDrugOnHandProfileWithMpRx(onHandProfileCriteria);
	    				
    			preVetOnHandSourceType = result.getFirst();
    			List<MedOrderItem> onHandItemList = prepareOnHandItemList(result.getSecond());
	    		
	    		Collections.sort(onHandItemList, ON_HAND_DRUG_ITEM_COMPARATOR);
	    		medProfileItemConverter.formatDischargeMoItemList(onHandItemList);
	    		resultList.addAll(onHandItemList);
	    		
    		} catch (Exception ex) {
    			drugOnHandServiceError = true;
    		}
    	}
    	
    	return resultList;
    }
    
    private List<MedOrderItem> prepareOnHandItemList(List<OnHandProfile> onHandProfileList) {
    	
    	Map<String, List<MedOrderFm>> fmListMap = retrieveMedOrderFmMap(onHandProfileList);
    	
    	List<MedOrderItem> onHandItemList = new ArrayList<MedOrderItem>();
    	
    	for (OnHandProfile onHandProfile: onHandProfileList) {
    		
    		String orderNum = constructOrderNum(onHandProfile);
    		MedOrderItem onHandItem = onHandProfile.getMedOrderItem();
    		onHandItem.setPatHospCode(onHandProfile.getPatHospCode());
    		onHandItem.loadDmInfo(Boolean.TRUE.equals(onHandProfile.getMpDischargeFlag()));
    		
    		List<MedOrderFm> fmList = fmListMap.get(orderNum);
    		
    		if (!CollectionUtils.isEmpty(fmList) &&
    				!CollectionUtils.isEmpty(fmIndicationManager.retrieveLocalFmIndicationListForDrugOnHand(onHandItem, fmList))) {
				onHandItem.setFmIndFlag(true);
    		}
			onHandItem.setMpDischargeFlag(onHandProfile.getMpDischargeFlag());
			onHandItemList.add(onHandItem);
    	}
    	return onHandItemList;
    }
    
    @SuppressWarnings("unchecked")
	private Map<String, List<MedOrderFm>> retrieveMedOrderFmMap(List<OnHandProfile> onHandProfileList) {

    	List<MedOrder> medOrderList = QueryUtils.splitExecute(em.createQuery(
    			"select o from MedOrder o" + // 20150206 index check : MedOrder.orderNum : I_MED_ORDER_01
    			" where o.orderNum in :orderNumSet")
    			.setHint(QueryHints.FETCH, MED_ORDER_QUERY_HINT_COMMON_1),
    			"orderNumSet" , constructOrderNumSet(onHandProfileList));
    	
    	Map<String, List<MedOrderFm>> fmListMap = new HashMap<String, List<MedOrderFm>>();
    	
    	for (MedOrder medOrder: medOrderList) {
    		fmListMap.put(medOrder.getOrderNum(), medOrder.getMedOrderFmList());
    	}
    	
    	return fmListMap;
    }
    
    private Set<String> constructOrderNumSet(List<OnHandProfile> onHandProfileList) {
    	
    	Set<String> orderNumSet = new HashSet<String>();
    	for (OnHandProfile onHandProfile: onHandProfileList) {
    		orderNumSet.add(constructOrderNum(onHandProfile));    		
    	}
    	return orderNumSet;
    }
    
    private String constructOrderNum(OnHandProfile onHandProfile) {
    	return StringUtils.rightPad(onHandProfile.getPatHospCode(), 3) + onHandProfile.getOrderNum().toString();
    }
    
	@SuppressWarnings("unchecked")
	private List<MedProfileMoItem> fetchMedProfileMoItemList(MedOrder medOrder, Date date) {
    	
    	if (medOrder == null || medOrder.getMedCase() == null) {
    		return null;
    	}
    	
    	List<MedProfileMoItem> medProfileMoItemList = em.createQuery(
    			"select o from MedProfileMoItem o" + // 20150206 index check : MedCase.caseNum : I_MED_CASE_01
    			" where o.medProfileItem.medProfile.medCase.caseNum = :caseNum" +
    			" and o.itemNum < 10000" +
    			" and (o.endDate > :dischargeDate or o.endDate is null)" +
    			" and (o.transferFlag = false or o.transferFlag is null)" +
    			" and o.status <> :sysDeletedStatus" +
    			" order by o.startDate desc")
    			.setParameter("caseNum", medOrder.getMedCase().getCaseNum())
    			.setParameter("dischargeDate", date)
    			.setParameter("sysDeletedStatus", MedProfileMoItemStatus.SysDeleted)
				.setHint(QueryHints.FETCH, MED_PROFILE_MO_ITEM_QUERY_HINT_FETCH_1)
    			.getResultList();
    	
    	return medProfileMoItemList;
    }
	
	private Patient retrievePasPatient(MedOrder medOrder, Patient patient) {		
		return patient != null ? patient : (medOrder == null ?
				null : medProfileManager.retrievePasPatient(medOrder.getPatHospCode(), medOrder.getMedCase().getCaseNum()));
	}
	
	private EhrPatient retrieveEhrPatient(MedOrder medOrder, Patient patient) {
		if ( medOrder == null || patient == null) {
			return null;
		}
		return pasServiceWrapper.retrieveEhrPatient(medOrder.getPatHospCode(), patient.getPatKey(), patient.getHkid());
	}
    
	public PreVetAuth preVetAuthnication(String userId, String password, String target) {
		String authFailMsg;
		AuthenticateCriteria authenticateCriteria = new AuthenticateCriteria();
		authenticateCriteria.setProfileCode(uamInfo.getProfileCode());
		authenticateCriteria.setUserName(userId);
		authenticateCriteria.setPassword(password);
		authenticateCriteria.setTarget(target);
		authenticateCriteria.setAction("Y");
		AuthenticateResult authenticateResult = uamServiceProxy.authenticateWithUserInfo(authenticateCriteria);
		
		if (sysProfile.isDebugEnabled() || AuthorizeStatus.Succeed.equals(authenticateResult.getStatus()) ){
			authFailMsg = null;
			
		} else {
			if ( AuthorizeStatus.Invalid.equals(authenticateResult.getStatus()) ){			
				authFailMsg = "0091";
			} else {
				authFailMsg = "0691";
			}
		}
		
		logger.debug("authFailMsg #0", authFailMsg);
		return new PreVetAuth(authFailMsg, userId, authenticateResult.getDisplayName());
	}
	
	public String removeOrder(String logonUser, String remarkText, MedOrder inMedOrder) {
        logger.info("removeOrder (pre-vet): medOrderId=#0, userId=#1", inMedOrder.getId(), uamInfo.getUserId());
        
        String orderStatusMsg = oneStopOrderStatusManager.verifyMedOrderStatusFromPreVetOperation(inMedOrder);
        if (!StringUtils.isBlank(orderStatusMsg))
        {
        	return orderStatusMsg;
        }
        
        MedOrder medOrder = em.find(MedOrder.class, inMedOrder.getId());
        medOrder.loadChild();
        
        // unlock the order
        vettingManager.releaseOrder(null, medOrder);
        
        vettingManager.removeOrder(medOrder, null, logonUser, remarkText);
        
        pharmInboxClientNotifier.dispatchUpdateEvent(medOrder);     
        
        return null;
	}

		
	@Remove
	@Override
	public void destroy() {
		dischargePatient = null;
		dischargeMedOrder = null;
		dischargeMedProfileMoItemList = null;
	}
	
	public static class MedOrderItemComparator implements Comparator<MedOrderItem> {

		public static final ActionStatus DEFAULT_ACTION_STATUS_FOR_IV = ActionStatus.DispByPharm; 
		
		@Override
		public int compare(MedOrderItem o1, MedOrderItem o2) {
			
			int result = lookupRemarkItemStatusValue(o1) - lookupRemarkItemStatusValue(o2);
			if (result != 0) {
				return result;
			}
			
			result = lookupActionStatusValue(o1) - lookupActionStatusValue(o2);
			if (result != 0) {
				return result;
			}
			
			return MedProfileUtils.compare(o1.getOrgItemNum(), o2.getOrgItemNum());
		}
		
		private int lookupRemarkItemStatusValue(MedOrderItem medOrderItem) {
			return medOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedDelete ? 1 : 0;
		}
		
		private int lookupActionStatusValue(MedOrderItem medOrderItem) {
			
			RxItem rxItem = medOrderItem.getRxItem();
			
			if (rxItem instanceof IvRxDrug) {
				return lookupActionStatusValue(medOrderItem, (IvRxDrug) rxItem);
			}
			if (rxItem instanceof RxDrug) {
				return lookupActionStatusValue(((RxDrug) rxItem).getActionStatus());
			}
			if (rxItem instanceof CapdRxDrug) {
				return lookupActionStatusValue(((CapdRxDrug) rxItem).getActionStatus());
			}
			return lookupActionStatusValue((ActionStatus) null);
		}
		
		private int lookupActionStatusValue(MedOrderItem medOrderItem, IvRxDrug ivRxDrug) {
			
			medOrderItem.loadDmInfo(true);
			
			boolean initValues = false;
			ActionStatus actionStatus = null;
			String sfiCat = null;
			
			for (IvAdditive ivAdditive: ivRxDrug.getIvAdditiveList()) {
				if (initValues) {
					if (actionStatus != ivAdditive.getActionStatus() || MedProfileUtils.compare(sfiCat, ivAdditive.getSfiCat()) != 0) {
						return lookupActionStatusValue(DEFAULT_ACTION_STATUS_FOR_IV);
					}
				} else {
					initValues = true;
					actionStatus = ivAdditive.getActionStatus();
					sfiCat = ivAdditive.getSfiCat();
				}
			}
			for (IvFluid ivFluid: ivRxDrug.getIvFluidList()) {
				if (initValues) {
					if (ivFluid.getDiluentCode() != null && (actionStatus != ivFluid.getActionStatus() || sfiCat != null)) {
						return lookupActionStatusValue(DEFAULT_ACTION_STATUS_FOR_IV);
					}
				} else {
					initValues = true;
					actionStatus = ivFluid.getActionStatus();
				}
			}
			return lookupActionStatusValue(actionStatus == null ? DEFAULT_ACTION_STATUS_FOR_IV : actionStatus);
		}
		
		private int lookupActionStatusValue(ActionStatus actionStatus) {
			
			if (actionStatus != null) {
				switch (actionStatus) {
					case DispByPharm:
						return 0;
					case SafetyNet:
						return 1;
					case DispInClinic:
						return 2;
					case PurchaseByPatient:
						return 3;
					case ContinueWithOwnStock:
						return 4;
				}
			}
			return 5; 
		}
	}
	
	public static class OnHandDrugItemComparator implements Comparator<MedOrderItem> {

		@Override
		public int compare(MedOrderItem o1, MedOrderItem o2) {
			return MedProfileUtils.compare(MedProfileUtils.extractStartDate(o1.getRxItem()),
					MedProfileUtils.extractStartDate(o2.getRxItem())) * -1;
		}
	}
}
