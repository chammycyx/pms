package hk.org.ha.model.pms.vo.label;

import hk.org.ha.model.pms.dms.udt.pivas.ExpiryDateTitle;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "PivasMergeFormulaProductLabel")
@ExternalizedBean(type=DefaultExternalizer.class)
public class PivasMergeFormulaProductLabel implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@XmlElement(required = true)
	private String lotNum;
	
	@XmlElement(required = true)
	private String printName;
	
	@XmlElement(required = true)
	private String siteDesc;
	
	@XmlElement(required = false)
	private String warnCode1;
	
	@XmlElement(required = false)
	private String warnCode2;
	
	@XmlElement(required = false)
	private String warnCode3;
	
	@XmlElement(required = false)
	private String warnCode4;
	
	@XmlElement(required = true)
	private ExpiryDateTitle expiryDateTitle;
	
	@XmlElement(required = true)
	private Date prepExpiryDate;
	
	@XmlElement(required = true)
	private Date manufactureDate;
	
	// save for PIVAS report
	@XmlElement(required = false)
	private String targetProductItemCode;
	
	// save for PIVAS report
	@XmlElement(required = false)
	private Integer targetProductItemDrugKey;
	
	// save for PIVAS report
	@XmlElement(required = true)
	private Integer numOfLabelSet;
	
	@XmlTransient
	private boolean reprintFlag;
	
	@XmlElement(required = true)
	private List<PivasMergeFormulaProductLabelDrugDtl> pivasMergeFormulaProductLabelDrugDtlList;
	
	@XmlElement(required = true)
	private String finalVolume;
	
	public PivasMergeFormulaProductLabel() {
		expiryDateTitle = ExpiryDateTitle.UseOnOrBefore;
		reprintFlag = Boolean.FALSE;
	}

	public String getLotNum() {
		return lotNum;
	}

	public void setLotNum(String lotNum) {
		this.lotNum = lotNum;
	}

	public String getPrintName() {
		return printName;
	}

	public void setPrintName(String printName) {
		this.printName = printName;
	}

	public String getSiteDesc() {
		return siteDesc;
	}

	public void setSiteDesc(String siteDesc) {
		this.siteDesc = siteDesc;
	}

	public String getWarnCode1() {
		return warnCode1;
	}

	public void setWarnCode1(String warnCode1) {
		this.warnCode1 = warnCode1;
	}

	public String getWarnCode2() {
		return warnCode2;
	}

	public void setWarnCode2(String warnCode2) {
		this.warnCode2 = warnCode2;
	}

	public String getWarnCode3() {
		return warnCode3;
	}

	public void setWarnCode3(String warnCode3) {
		this.warnCode3 = warnCode3;
	}

	public String getWarnCode4() {
		return warnCode4;
	}

	public void setWarnCode4(String warnCode4) {
		this.warnCode4 = warnCode4;
	}

	public ExpiryDateTitle getExpiryDateTitle() {
		return expiryDateTitle;
	}

	public void setExpiryDateTitle(ExpiryDateTitle expiryDateTitle) {
		this.expiryDateTitle = expiryDateTitle;
	}

	public Date getPrepExpiryDate() {
		return prepExpiryDate;
	}

	public void setPrepExpiryDate(Date prepExpiryDate) {
		this.prepExpiryDate = prepExpiryDate;
	}

	public Date getManufactureDate() {
		return manufactureDate;
	}

	public void setManufactureDate(Date manufactureDate) {
		this.manufactureDate = manufactureDate;
	}

	public String getTargetProductItemCode() {
		return targetProductItemCode;
	}

	public void setTargetProductItemCode(String targetProductItemCode) {
		this.targetProductItemCode = targetProductItemCode;
	}

	public Integer getTargetProductItemDrugKey() {
		return targetProductItemDrugKey;
	}

	public void setTargetProductItemDrugKey(Integer targetProductItemDrugKey) {
		this.targetProductItemDrugKey = targetProductItemDrugKey;
	}

	public Integer getNumOfLabelSet() {
		return numOfLabelSet;
	}

	public void setNumOfLabelSet(Integer numOfLabelSet) {
		this.numOfLabelSet = numOfLabelSet;
	}

	public boolean isReprintFlag() {
		return reprintFlag;
	}

	public void setReprintFlag(boolean reprintFlag) {
		this.reprintFlag = reprintFlag;
	}

	public List<PivasMergeFormulaProductLabelDrugDtl> getPivasMergeFormulaProductLabelDrugDtlList() {
		return pivasMergeFormulaProductLabelDrugDtlList;
	}

	public void setPivasMergeFormulaProductLabelDrugDtlList(List<PivasMergeFormulaProductLabelDrugDtl> pivasMergeFormulaProductLabelDrugDtlList) {
		this.pivasMergeFormulaProductLabelDrugDtlList = pivasMergeFormulaProductLabelDrugDtlList;
	}

	public String getFinalVolume() {
		return finalVolume;
	}

	public void setFinalVolume(String finalVolume) {
		this.finalVolume = finalVolume;
	}
	
}
