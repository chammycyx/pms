package hk.org.ha.model.pms.vo.alert;

import java.io.Serializable;

import hk.org.ha.model.pms.udt.alert.DrugType;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class DrugTypeAdapter extends XmlAdapter<String, DrugType> implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public String marshal(DrugType v) { 
		return v.getDataValue();
	}

	public DrugType unmarshal(String v) {
		return DrugType.dataValueOf(v);
	}

}
