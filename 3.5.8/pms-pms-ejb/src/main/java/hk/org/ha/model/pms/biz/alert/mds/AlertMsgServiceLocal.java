package hk.org.ha.model.pms.biz.alert.mds;

import hk.org.ha.model.pms.persistence.disp.MedItemInf;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;
import hk.org.ha.model.pms.vo.alert.mds.MedProfileAlertMsg;

import javax.ejb.Local;

@Local
public interface AlertMsgServiceLocal {

	MedProfileAlertMsg retrieveAlertMsgByItemNum(Integer itemNum);
	
	AlertMsg retrieveAlertMsgForDrugSearch();
	
	MedProfileAlertMsg retrieveAlertMsgByMedOrderItem(MedOrderItem medOrderItem);
	
	MedProfileAlertMsg retrieveMedProfileAlertMsg(MedItemInf medItem);
	
	AlertMsg retrieveMpAlertMsg(MedProfileMoItem medProfileMoItem);

	void destroy();

}
