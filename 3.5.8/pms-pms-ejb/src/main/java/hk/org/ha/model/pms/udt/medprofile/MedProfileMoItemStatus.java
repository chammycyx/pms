package hk.org.ha.model.pms.udt.medprofile;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum MedProfileMoItemStatus implements StringValuedEnum {
	
	Normal("N", "Normal"),
	SysDeleted("X", "SysDeleted");
	
	private final String dataValue;
	private final String displayValue;

	private MedProfileMoItemStatus(String dataValue,
			String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}
	
	public static MedProfileMoItemStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MedProfileMoItemStatus.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<MedProfileMoItemStatus> {
		
		private static final long serialVersionUID = 1L;

		public Class<MedProfileMoItemStatus> getEnumClass() {
			return MedProfileMoItemStatus.class;
		}
	}

}
