package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class EdsElapseTime implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String timeRange;
	
	private Integer avgTime;

	public EdsElapseTime(String timeRange, Integer avgTime) {
		super();
		this.timeRange = timeRange;
		this.avgTime = avgTime;
	}

	public String getTimeRange() {
		return timeRange;
	}

	public void setTimeRange(String timeRange) {
		this.timeRange = timeRange;
	}

	public Integer getAvgTime() {
		return avgTime;
	}

	public void setAvgTime(Integer avgTime) {
		this.avgTime = avgTime;
	}
}
