package hk.org.ha.service.biz.pms.interfaces;

import hk.org.ha.fmk.pms.remote.annotations.Marshaller;
import hk.org.ha.fmk.pms.remote.annotations.MarshallerType;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.corp.ActiveWard;
import hk.org.ha.model.pms.persistence.corp.WardAdminFreq;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.phs.CapdSupplierItem;
import hk.org.ha.model.pms.persistence.phs.PatientCat;
import hk.org.ha.model.pms.persistence.phs.Specialty;
import hk.org.ha.model.pms.persistence.phs.Ward;

import java.util.Date;
import java.util.List;

import org.jboss.seam.annotations.async.Asynchronous;
import org.osoa.sca.annotations.OneWay;

public interface PmsSubscriberJmsRemote {

	@OneWay
	@Asynchronous
	void initCache();

	@OneWay
	@Asynchronous
	void initCacheByCluster(String clusterCode);
			
	@OneWay
	@Asynchronous
	@Marshaller(MarshallerType.JAVA)
	void initCacheWithDmDrugList(List<DmDrug> dmDrugList);

	@OneWay
	@Asynchronous
	@Marshaller(MarshallerType.JAVA)
	void initCacheByClusterWithDmDrugList(String clusterCode, List<DmDrug> dmDrugList);
	
	@OneWay
	@Asynchronous
	void initJmsServiceProxy();
	
	@OneWay
	@Asynchronous
	void initJmsServiceProxyByCluster(String clusterCode);
	
	@OneWay
	@Asynchronous
	void purgeMsWorkstoreDrug(Workstore workstore);
	
	@OneWay
	@Asynchronous
	void updateFollowUpWarningItemCode(List<String> itemCodeList, Date updateTime);
	
	@OneWay
	@Asynchronous
	void createWaitTimeRptStat(Date batchDate, String clusterCode, Date createTime);
	
	@OneWay
	@Asynchronous
	void invalidateSession(Workstore workstore);	
	
	@OneWay
	@Asynchronous
	void updatePatientCatForPms(List<PatientCat> patientCatList, Date updateTime);
	
	@OneWay
	@Asynchronous
	void updateWardForPms(List<Ward> wardList, Date updateTime);

	@OneWay
	@Asynchronous
	void updateSpecialtyForPms(List<Specialty> specialtyList, Date updateTime);
	
	@OneWay
	@Asynchronous
	void updateCapdSupplierItemForPms(List<CapdSupplierItem> capdSupplierItemList, Date updateTime);
	
	@OneWay
	@Asynchronous
	void updateWardAdminFreqForPms(List<WardAdminFreq> wardAdminFreqList, List<String> errorPatHospCodeList, Date updateTime);
	
	@OneWay
	@Asynchronous
	void updateActiveWardForPms(List<ActiveWard> activeWardList, List<String> errorPatHospCodeList, Date updateTime);
	
	@OneWay
	@Asynchronous
	void updateWardAdminFreqForPms(List<WardAdminFreq> wardAdminFreqList, Date updateTime);
	
	@OneWay
	@Asynchronous
	void requestPmsSessionInfo(String requestId, Workstore workstore);	
	
	@OneWay
	@Asynchronous
	void requestPmsSignatureCountResult(String requestId, Date batchDate, Date updateTime);
}
