package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PatientStatus implements StringValuedEnum {

	Active("A", "Active"),
	Inactive("I", "Inactive");
	
    private final String dataValue;
    private final String displayValue;

    PatientStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static PatientStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PatientStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<PatientStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<PatientStatus> getEnumClass() {
    		return PatientStatus.class;
    	}
    }
}



