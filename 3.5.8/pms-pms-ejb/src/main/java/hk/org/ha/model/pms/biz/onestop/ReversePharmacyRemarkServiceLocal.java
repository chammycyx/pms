package hk.org.ha.model.pms.biz.onestop;

import javax.ejb.Local;

@Local
public interface ReversePharmacyRemarkServiceLocal {
		
	void destroy();
	
	void reversePharmacyRemark(Long id, Long medOrderVersion);
	
}
