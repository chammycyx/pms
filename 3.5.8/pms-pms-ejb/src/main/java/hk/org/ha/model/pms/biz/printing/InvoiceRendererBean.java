package hk.org.ha.model.pms.biz.printing;

import static hk.org.ha.model.pms.prop.Prop.CHARGING_SFI_INVOICE_COPIES;
import static hk.org.ha.model.pms.prop.Prop.CHARGING_SFI_INVOICE_EXTRA_COPIES;
import static hk.org.ha.model.pms.prop.Prop.CHARGING_SFI_INVOICE_EXTRA_ITEMTYPE;
import static hk.org.ha.model.pms.prop.Prop.CHARGING_SFI_INVOICE_EXTRA_MEDCASE_PASPATGROUPCODE;
import static hk.org.ha.model.pms.prop.Prop.CHARGING_SFI_INVOICE_REMARKCHI;
import static hk.org.ha.model.pms.prop.Prop.CHARGING_SFI_INVOICE_REMARKENG;
import static hk.org.ha.model.pms.prop.Prop.CHARGING_SFI_WAIVE_MEDCASE_PASPATGROUPCODE;
import static hk.org.ha.model.pms.prop.Prop.HOSPITAL_NAMEENG;
import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.charging.InvoiceManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.InvoiceItem;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.YesNoFlag;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.disp.PharmOrderItemType;
import hk.org.ha.model.pms.udt.disp.PharmOrderPatType;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.PrinterSelect;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.SfiInvoice;
import hk.org.ha.model.pms.vo.report.SfiInvoiceDtl;
import hk.org.ha.model.pms.vo.report.SfiInvoiceFtr;
import hk.org.ha.model.pms.vo.report.SfiInvoiceHdr;
import hk.org.ha.model.pms.vo.security.PropMap;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateless;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("invoiceRenderer")
@MeasureCalls
public class InvoiceRendererBean implements InvoiceRendererLocal {
	
	@In
	private PrintAgentInf printAgent;

	@In
	private Workstation workstation;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private InvoiceManagerLocal invoiceManager;
	
	@In
	private UamInfo uamInfo;
	
	@In
	private PropMap propMap;
	
	private List<SfiInvoice> sfiInvoiceList;
	
	private static final String HA_PAS_PAT_GROUP_CODE = "HA";
	private static final String GS_PAS_PAT_GROUP_CODE = "GS";
	private static final String EMPTY_PAS_PAY_CODE = "Nil";
			
	public void generateSfiInvoiceItemList() {
		
		if (sfiInvoiceList.size() > 0) {
			
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("lessExemptedAmount", 
					sfiInvoiceList.get(0).getSfiInvoiceHdr().getLessExemptedAmount());
			
		    PrintOption printOption = new PrintOption();
		    printOption.setDocType(ReportProvider.PNG);
		    printAgent.renderAndRedirect(new RenderJob(
				    "SfiInvoiceEnquiry",
				    printOption,
				    parameters,
				    Arrays.asList(sfiInvoiceList.get(0))));
		    
		}
		
	}
	public void createSfiInvoiceListForPreview(Invoice invoice, boolean isReprint) {
		SfiInvoice sfiInvoice = convertInvoiceToSfiInvoiceForPrint(invoice, isReprint);
		
		sfiInvoiceList = new ArrayList<SfiInvoice>();
		sfiInvoiceList.add(sfiInvoice);
	}
	
	public void printInvoice(DispOrder dispOrder){
		for( Invoice tmpInvoice : dispOrder.getInvoiceList() ){
			if( !tmpInvoice.getPrintFlag() ){
				this.printSfiInvoice(tmpInvoice, false);
			}
		}
	}
	
	public String printSfiInvoice(Invoice invoice, boolean isReprint) {

		List<RenderAndPrintJob> printJobList = new ArrayList<RenderAndPrintJob>();

		String ret = this.prepareSfiInvoicePrintJob(printJobList, invoice, isReprint);
		
		printAgent.renderAndPrint(printJobList);
		
		return ret;
	}

	public void prepareInvoicePrintJob(List<RenderAndPrintJob> printJobList, DispOrder dispOrder){
		for( Invoice tmpInvoice : dispOrder.getInvoiceList() ){
			if( !tmpInvoice.getPrintFlag() ){
				this.prepareSfiInvoicePrintJob(printJobList, tmpInvoice, false);
			}
		}
	}
	
	public String prepareSfiInvoicePrintJob(List<RenderAndPrintJob> printJobList, Invoice invoice, boolean isReprint){
		if( isReprint ){
			if( PharmOrderPatType.Doh.equals(invoice.getDispOrder().getPharmOrder().getPatType()) ){
				return StringUtils.EMPTY;
			}else if( !invoiceManager.isExceptionDrugExist(invoice) ){
				return "0098";
			}
		}
		SfiInvoice sfiInvoice = convertInvoiceToSfiInvoiceForPrint(invoice, isReprint);
		
		PrinterSelect invoicePrinterSelect = printerSelector.retrievePrinterSelect(PrintDocType.SfiInvoice);
		invoicePrinterSelect.setCopies(1);
		
		printJobList.add(new RenderAndPrintJob(
				    	    "SfiInvoice",
				    	    new PrintOption(),
				    	    invoicePrinterSelect,
				    	    Arrays.asList(sfiInvoice),
				    	    "[SfiInvoice] TicketNum:" + sfiInvoice.getSfiInvoiceHdr().getTicketNum() + 
				    	    ", SfiInvNum:" + sfiInvoice.getSfiInvoiceHdr().getSfiInvNum() +
				    	    ", isReprint:" + isReprint));
		
		if ( !isReprint ){
			Integer noOfPrintOut = CHARGING_SFI_INVOICE_COPIES.get(1);
			noOfPrintOut = calculateNumOfInvoiceCopies(sfiInvoice, noOfPrintOut);
			
			if( noOfPrintOut > 1 ){
				SfiInvoice copyInvoice = convertInvoiceToSfiInvoiceForPrint(invoice, true);				
				
				try {
					PrinterSelect invoiceCopyPrinterSelect = (PrinterSelect) BeanUtils.cloneBean(invoicePrinterSelect);
					invoiceCopyPrinterSelect.setCopies(noOfPrintOut-1);
					
					printJobList.add(new RenderAndPrintJob(
							"SfiInvoice",
							new PrintOption(),
							invoiceCopyPrinterSelect,
							Arrays.asList(copyInvoice),
							"[SfiInvoice] TicketNum:" + sfiInvoice.getSfiInvoiceHdr().getTicketNum() + 
				    	    ", SfiInvNum:" + sfiInvoice.getSfiInvoiceHdr().getSfiInvNum() +
				    	    ", isReprint:" + isReprint
				    	    ));
				} catch (Exception e) {
				}
			}
			//updateInvoicePrintFlag
			invoiceManager.updateInvoicePrintFlag(invoice);
		}
		return StringUtils.EMPTY;
	}
	
	private SfiInvoice convertInvoiceToSfiInvoiceForPrint(Invoice invoice, boolean isReprint){
		boolean sameDrugKeyExist = false;
		boolean lifeStyleDrugExist = false;
		BigDecimal totalAmount = BigDecimal.ZERO;
		
		invoice.getInvoiceItemList();
		
		List<SfiInvoiceDtl> sfiInvoiceDtlList = new ArrayList<SfiInvoiceDtl>();
		Map<Integer,SfiInvoiceDtl> sfiInvoiceDtlMap = new HashMap<Integer,SfiInvoiceDtl>();
		OrderType orderType = invoice.getDispOrder().getOrderType();
		
		for ( InvoiceItem invoiceItem: invoice.getInvoiceItemList() ){
			if ( orderType == OrderType.InPatient && 
					invoiceItem.getChargeQty().compareTo(BigDecimal.ZERO) == 0 ){
				continue;
			}
			SfiInvoiceDtl sfiInvoiceDtl = new SfiInvoiceDtl();
			
			DispOrderItem dispOrderItem = invoiceItem.getDispOrderItem();
			PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
			
			sfiInvoiceDtl.setItemcode( pharmOrderItem.getItemCode() );			
			sfiInvoiceDtl.setDrugDesc( pharmOrderItem.getFullDrugDesc() );
			
			if ( invoiceItem.getChargeQty() != null ) {
				sfiInvoiceDtl.setIssueQty( String.valueOf(invoiceItem.getChargeQty().intValue()) );
			} else {
				sfiInvoiceDtl.setIssueQty( String.valueOf(dispOrderItem.getDispQty().intValue()) );
			}
			sfiInvoiceDtl.setBaseUnit(pharmOrderItem.getBaseUnit());
			sfiInvoiceDtl.setDrugChargeAmount(invoiceItem.getAmount().doubleValue());
			totalAmount = totalAmount.add(invoiceItem.getAmount());
			
			//Indicator			
			if( dispOrderItem.getLifestyleFlag() ){
				sfiInvoiceDtl.setLifeStyleDrugInd(true);
				lifeStyleDrugExist = true;
			}
			if( dispOrderItem.getOncologyFlag() ){
				sfiInvoiceDtl.setOncologyDrugInd(true);
			}
			if( "M".equals(pharmOrderItem.getFmStatus()) ){
				sfiInvoiceDtl.setSaftyNetInd(true);
			}
			
			sfiInvoiceDtl.setDrugName(pharmOrderItem.getDrugName());
			sfiInvoiceDtl.setFormCode(pharmOrderItem.getFormCode());
			sfiInvoiceDtl.setActionStatus( pharmOrderItem.getActionStatus().getDataValue() );
			sfiInvoiceDtl.setOrgItemNum( pharmOrderItem.getMedOrderItem().getOrgItemNum() );
			sfiInvoiceDtlList.add(sfiInvoiceDtl);				
			
			if( !sameDrugKeyExist ){
				if( !sfiInvoiceDtlMap.containsKey(pharmOrderItem.getDrugKey()) ) {
					sfiInvoiceDtlMap.put(pharmOrderItem.getDrugKey(), sfiInvoiceDtl);					
				}else{
					sameDrugKeyExist = true;
				}
			}
		}
		
		if( invoice.getInvoiceItemList().size() > 1 ){
			sfiInvoiceDtlList = sortSfiInvoiceDtlByDrugNameForm(sfiInvoiceDtlList);
		}
		
		//Header Info
		SfiInvoiceHdr sfiInvoiceHdr = constructSfiInvoiceHdr(invoice, isReprint);
		
		Double lessExemptedAmount = calculateLessExemptAmount(sfiInvoiceHdr, sfiInvoiceDtlList, totalAmount);
		if( lessExemptedAmount.compareTo(Double.valueOf(0)) > 0 ){
			sfiInvoiceHdr.setLessExemptedAmount(lessExemptedAmount);
			sfiInvoiceHdr.setLifeStyleDrugExist(lifeStyleDrugExist);			
		}
		
		//Footer Info
		MedOrder medOrder = invoice.getDispOrder().getPharmOrder().getMedOrder();
		
		SfiInvoiceFtr sfiInvoiceFtr = new SfiInvoiceFtr();
		sfiInvoiceFtr.setAttnChi(CHARGING_SFI_INVOICE_REMARKCHI.get());
		sfiInvoiceFtr.setAttnEng(CHARGING_SFI_INVOICE_REMARKENG.get());
		if( !MedOrderPrescType.In.equals(medOrder.getPrescType()) ){
			if (MedOrderDocType.Normal.equals(medOrder.getDocType()) ) {
				sfiInvoiceFtr.setOrderNum("MOE"+medOrder.getOrderNum());
			}
		}
		
		if( isReprint ){
			sfiInvoiceFtr.setLastUpdDate(invoice.getUpdateDate());
		}else{
			sfiInvoiceFtr.setLastUpdDate(new Date());
		}
		sfiInvoiceFtr.setUserCode(uamInfo.getUserId());
		
		SfiInvoice sfiInvoice = new SfiInvoice();
		sfiInvoice.setSfiInvoiceHdr(sfiInvoiceHdr);
		sfiInvoice.setSfiInvoiceFtr(sfiInvoiceFtr);
		sfiInvoice.setSfiInvoiceDtlList(sfiInvoiceDtlList);
		
		return sfiInvoice;
	}
	
	private Integer calculateNumOfInvoiceCopies( SfiInvoice sfiInvoice , Integer defaultNumOfCopies ){
		if( !StringUtils.equals(CHARGING_SFI_INVOICE_EXTRA_MEDCASE_PASPATGROUPCODE.get(), sfiInvoice.getSfiInvoiceHdr().getPasPatGroupCode()) ){
			return defaultNumOfCopies;
		}
		boolean containsLifeStyleItem = false;
		boolean containsOncologyItem = false;
		boolean containsSafetyNetItem = false;
		
		for( SfiInvoiceDtl sfiInvoiceDtl : sfiInvoice.getSfiInvoiceDtlList() ){
			if( sfiInvoiceDtl.getLifeStyleDrugInd() ){
				containsLifeStyleItem = true;
			}
			
			if( sfiInvoiceDtl.getOncologyDrugInd() ){
				containsOncologyItem = true;
			}
			
			if( sfiInvoiceDtl.getSaftyNetInd() ){
				containsSafetyNetItem = true;
			}
		}
		
		Integer totalCopies = defaultNumOfCopies;
		
		if( StringUtils.equals(CHARGING_SFI_INVOICE_EXTRA_ITEMTYPE.get(),PharmOrderItemType.Lifestyle.getDataValue()) && containsLifeStyleItem ){
			totalCopies = totalCopies + CHARGING_SFI_INVOICE_EXTRA_COPIES.get(0);
			return totalCopies;
		}
		
		if( StringUtils.equals(CHARGING_SFI_INVOICE_EXTRA_ITEMTYPE.get(),PharmOrderItemType.Oncology.getDataValue()) && containsOncologyItem ){
			totalCopies = totalCopies + CHARGING_SFI_INVOICE_EXTRA_COPIES.get(0);
			return totalCopies;
		}
		
		if( StringUtils.equals(CHARGING_SFI_INVOICE_EXTRA_ITEMTYPE.get(),PharmOrderItemType.SafetyNet.getDataValue()) && containsSafetyNetItem ){
			totalCopies = totalCopies + CHARGING_SFI_INVOICE_EXTRA_COPIES.get(0);
			return totalCopies;
		}
		return totalCopies;
	}
	
	private Double calculateLessExemptAmount(SfiInvoiceHdr sfiInvoiceHdr, List<SfiInvoiceDtl> sfiInvoiceDtlList, BigDecimal totalAmount){
		Double totalLifeStyleAmount = Double.valueOf(0);		
		
		if( !StringUtils.equals( CHARGING_SFI_WAIVE_MEDCASE_PASPATGROUPCODE.get(), sfiInvoiceHdr.getPasPatGroupCode()) ){
			return totalLifeStyleAmount;
		}
		
		for( SfiInvoiceDtl sfiInvoiceDtl : sfiInvoiceDtlList ){
			if( sfiInvoiceDtl.getLifeStyleDrugInd() ){
				totalLifeStyleAmount = totalLifeStyleAmount + sfiInvoiceDtl.getDrugChargeAmount();
			}
		}
				
		return (totalAmount.doubleValue() - totalLifeStyleAmount);
	}
	
	private List<SfiInvoiceDtl> sortSfiInvoiceDtlByDrugNameForm(List<SfiInvoiceDtl> sfiInvoiceDtlList ){
		Collections.sort(sfiInvoiceDtlList, new comparatorForSfiInvoiceDtlByDf());
		
		StringBuilder duplicateDrugKey = new StringBuilder();
		String duplicateItemCode = null;
		List<SfiInvoiceDtl> tempSfiInvoiceDtlList = null;
		Double tempSameDrugKeyAmount = 0.0;		
		List<SfiInvoiceDtl> returnSfiInvoiceDtlList = new ArrayList<SfiInvoiceDtl>(); 
		
		for( int i=0; i< sfiInvoiceDtlList.size(); i++ ){	
			SfiInvoiceDtl sfiInvoiceDtl = sfiInvoiceDtlList.get(i);
			
			if( !new StringBuilder().append(sfiInvoiceDtl.getDrugName()).append(sfiInvoiceDtl.getFormCode()).toString().equals(duplicateDrugKey.toString())){			
				duplicateDrugKey.setLength(0); 
				duplicateDrugKey.append(sfiInvoiceDtl.getDrugName()).append(sfiInvoiceDtl.getFormCode());
				duplicateItemCode = sfiInvoiceDtl.getItemcode();
				
				if( tempSfiInvoiceDtlList != null ){
					tempSameDrugKeyAmount = tempSameDrugKeyAmount + returnSfiInvoiceDtlList.get(returnSfiInvoiceDtlList.size()-1).getDrugChargeAmount();
					returnSfiInvoiceDtlList.get(returnSfiInvoiceDtlList.size()-1).setDrugChargeAmount(tempSameDrugKeyAmount);				
					returnSfiInvoiceDtlList.addAll(tempSfiInvoiceDtlList);
					tempSfiInvoiceDtlList = null;
					tempSameDrugKeyAmount = 0.0;
				}
				
				sfiInvoiceDtl.setSameDrugKeyInd(false);
				returnSfiInvoiceDtlList.add(sfiInvoiceDtl);
				
			}else{
				
				DmDrug dmDrug = dmDrugCacher.getDmDrug(sfiInvoiceDtl.getItemcode());
				if( dmDrug != null && YesNoFlag.No.getDataValue().equals(dmDrug.getDmDrugProperty().getStrengthCompul()) ){
					if( tempSfiInvoiceDtlList == null ){
						tempSfiInvoiceDtlList = new ArrayList<SfiInvoiceDtl>();
					}
					sfiInvoiceDtl.setSameDrugKeyInd(true);
					tempSameDrugKeyAmount = tempSameDrugKeyAmount + sfiInvoiceDtl.getDrugChargeAmount(); 
					sfiInvoiceDtl.setDrugChargeAmount(0.0);		
					tempSfiInvoiceDtlList.add(sfiInvoiceDtl);					
				}else{
					if( StringUtils.equals(duplicateItemCode, sfiInvoiceDtl.getItemcode()) ){
						if( tempSfiInvoiceDtlList == null ){
							tempSfiInvoiceDtlList = new ArrayList<SfiInvoiceDtl>();
						}
						sfiInvoiceDtl.setSameDrugKeyInd(true);
						tempSameDrugKeyAmount = tempSameDrugKeyAmount + sfiInvoiceDtl.getDrugChargeAmount(); 
						sfiInvoiceDtl.setDrugChargeAmount(0.0);
						tempSfiInvoiceDtlList.add(sfiInvoiceDtl);
					}else{
						duplicateDrugKey.setLength(0);
						duplicateDrugKey.append(sfiInvoiceDtl.getDrugName()).append(sfiInvoiceDtl.getFormCode());
						duplicateItemCode = sfiInvoiceDtl.getItemcode();
					
						if( tempSfiInvoiceDtlList != null ){
							tempSameDrugKeyAmount = tempSameDrugKeyAmount + returnSfiInvoiceDtlList.get(returnSfiInvoiceDtlList.size()-1).getDrugChargeAmount();
							returnSfiInvoiceDtlList.get(returnSfiInvoiceDtlList.size()-1).setDrugChargeAmount(tempSameDrugKeyAmount);				
							returnSfiInvoiceDtlList.addAll(tempSfiInvoiceDtlList);
							tempSfiInvoiceDtlList = null;
							tempSameDrugKeyAmount = 0.0;
						}
						sfiInvoiceDtl.setSameDrugKeyInd(false);
						returnSfiInvoiceDtlList.add(sfiInvoiceDtl);
					}
				}
			}
			
			if( i == sfiInvoiceDtlList.size()-1 && tempSfiInvoiceDtlList != null ){								
				tempSameDrugKeyAmount = tempSameDrugKeyAmount + returnSfiInvoiceDtlList.get(returnSfiInvoiceDtlList.size()-1).getDrugChargeAmount();					
				returnSfiInvoiceDtlList.get(returnSfiInvoiceDtlList.size()-1).setDrugChargeAmount(tempSameDrugKeyAmount);				
				returnSfiInvoiceDtlList.addAll(tempSfiInvoiceDtlList);
				tempSfiInvoiceDtlList = null;
				tempSameDrugKeyAmount = 0.0;
			}
			
		}		
		return returnSfiInvoiceDtlList;
	}
	
	private SfiInvoiceHdr constructSfiInvoiceHdr(Invoice invoice, boolean isReprint){
		SfiInvoiceHdr sfiInvoiceHdr = new SfiInvoiceHdr();
		
		PharmOrder pharmOrder = invoice.getDispOrder().getPharmOrder();		
		MedCase pharmMedCase = pharmOrder.getMedCase();
		
		if( pharmMedCase != null ){
			sfiInvoiceHdr.setCaseNum(formatPrintCaseNum(pharmMedCase.getCaseNum()));
			sfiInvoiceHdr.setPasPatGroupCode( pharmMedCase.getPasPatGroupCode() );

			StringBuilder displayPasPayCode = new StringBuilder();
			if( !StringUtils.isEmpty(pharmMedCase.getPasPayCode()) ){
				displayPasPayCode.append(pharmMedCase.getPasPayCode());
			}
			
			if( StringUtils.equals(CHARGING_SFI_WAIVE_MEDCASE_PASPATGROUPCODE.get(), pharmMedCase.getPasPatGroupCode()) ){
				displayPasPayCode.append(" (").append(HA_PAS_PAT_GROUP_CODE).append(")");
				
			}else if( StringUtils.equals(pharmMedCase.getPasPatGroupCode(), GS_PAS_PAT_GROUP_CODE) ){
				displayPasPayCode.append(" (").append(GS_PAS_PAT_GROUP_CODE).append(")");
			}
			
			sfiInvoiceHdr.setPasPayCode(displayPasPayCode.toString());
		}
		
		if( StringUtils.isEmpty(sfiInvoiceHdr.getPasPayCode()) ){
			sfiInvoiceHdr.setPasPayCode(EMPTY_PAS_PAY_CODE);
		}
		
		if( StringUtils.equals(pharmOrder.getPatient().getHkid(), "?") ){
			sfiInvoiceHdr.setHkid(StringUtils.EMPTY);
		}else{
			sfiInvoiceHdr.setHkid( pharmOrder.getPatient().getHkid() );
		}
		
		sfiInvoiceHdr.setHospCodeDesc(HOSPITAL_NAMEENG.get());		
		sfiInvoiceHdr.setReprintFlag(isReprint);
		
		sfiInvoiceHdr.setName(pharmOrder.getPatient().getName());
		sfiInvoiceHdr.setNameChi(pharmOrder.getPatient().getNameChi());
		
		sfiInvoiceHdr.setPatType(pharmOrder.getPatType());
		sfiInvoiceHdr.setPrescType(pharmOrder.getMedOrder().getPrescType());
		
		sfiInvoiceHdr.setPrintWorkStation(workstation.getHostName());
		
		if ( invoice.getInvoiceDate() != null ) {
			sfiInvoiceHdr.setSfiInvDate(invoice.getInvoiceDate());
		} else {
			sfiInvoiceHdr.setSfiInvDate(invoice.getCreateDate());
		}
		sfiInvoiceHdr.setSfiInvNum(invoice.getInvoiceNum());
		
		if( OrderType.InPatient != invoice.getDispOrder().getOrderType() && invoice.getDispOrder().getTicket() != null ){
			sfiInvoiceHdr.setTicketNum(invoice.getDispOrder().getTicket().getTicketNum());
		}
		sfiInvoiceHdr.setOrderType(invoice.getOrderType().getDataValue());
		
		return sfiInvoiceHdr;
	}

	private static String formatPrintCaseNum(String caseNum){
		if( StringUtils.isEmpty(caseNum) ){
			return StringUtils.EMPTY;
		}
		
		StringBuilder sb = new StringBuilder();
		
		if( caseNum.length() == 12 ){
			sb.append(caseNum.substring(0, 4));
			sb.append("-");
			sb.append(caseNum.substring(4,6));
			sb.append("-");
			sb.append(caseNum.substring(6,11));
			sb.append("(");
			sb.append(caseNum.substring(11,12));
			sb.append(")");
		}else if( caseNum.length() == 11 ){
			sb.append(caseNum.substring(0, 3));
			sb.append("-");
			sb.append(caseNum.substring(3, 5));
			sb.append("-");
			sb.append(caseNum.substring(5, 10));
			sb.append("(");
			sb.append(caseNum.substring(10, 11));
			sb.append(")");
		}else{
			return caseNum;
		}
		return sb.toString();
	}
	
	@Remove
	public void destroy(){		

	}	
	
	public static class comparatorForSfiInvoiceDtlByDf implements Comparator<SfiInvoiceDtl>, Serializable
    {
		private static final long serialVersionUID = 1L;
        
		@Override
		public int compare(SfiInvoiceDtl o1, SfiInvoiceDtl o2) {
			return new CompareToBuilder()
				.append( o1.getDrugName(), o2.getDrugName() )
				.append( o1.getFormCode(), o2.getFormCode() )                
				.append( o1.getItemcode(), o2.getItemcode() )
				.toComparison();
		}
    }
	
	public static class comparatorForSfiInvoiceDtlByActionStatus implements Comparator<SfiInvoiceDtl>, Serializable
    {
		private static final long serialVersionUID = 1L;
		
          @Override
          public int compare(SfiInvoiceDtl o1, SfiInvoiceDtl o2) {
                return new CompareToBuilder()
                	  .append( o1.getOrgItemNum(), o2.getOrgItemNum() )
                      .toComparison();
          }
    }
}
