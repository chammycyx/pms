package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class SfiInvoiceDtl implements Serializable {

	private static final long serialVersionUID = 1L;

	private Boolean oncologyDrugInd;
	
	private Boolean lifeStyleDrugInd;

	private Boolean saftyNetInd;
	
	private Boolean sameDrugKeyInd;

	private String itemcode;

	private String drugDesc;

	private String issueQty;

	private String baseUnit;

	private Double drugChargeAmount;

	private String drugName;
	
	private String formCode;
		
	private String actionStatus;
	
	private Integer orgItemNum;

	public SfiInvoiceDtl(){
		oncologyDrugInd = false;
		lifeStyleDrugInd = false;
		saftyNetInd = false;
		sameDrugKeyInd = false;		
	}
	
	public Boolean getOncologyDrugInd() {
		return oncologyDrugInd;
	}

	public void setOncologyDrugInd(Boolean oncologyDrugInd) {
		this.oncologyDrugInd = oncologyDrugInd;
	}

	public Boolean getLifeStyleDrugInd() {
		return lifeStyleDrugInd;
	}

	public void setLifeStyleDrugInd(Boolean lifeStyleDrugInd) {
		this.lifeStyleDrugInd = lifeStyleDrugInd;
	}

	public Boolean getSaftyNetInd() {
		return saftyNetInd;
	}

	public void setSaftyNetInd(Boolean saftyNetInd) {
		this.saftyNetInd = saftyNetInd;
	}

	public Boolean getSameDrugKeyInd() {
		return sameDrugKeyInd;
	}

	public void setSameDrugKeyInd(Boolean sameDrugKeyInd) {
		this.sameDrugKeyInd = sameDrugKeyInd;
	}

	public String getItemcode() {
		return itemcode;
	}

	public void setItemcode(String itemcode) {
		this.itemcode = itemcode;
	}

	public String getDrugDesc() {
		return drugDesc;
	}

	public void setDrugDesc(String drugDesc) {
		this.drugDesc = drugDesc;
	}

	public String getIssueQty() {
		return issueQty;
	}

	public void setIssueQty(String issueQty) {
		this.issueQty = issueQty;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public Double getDrugChargeAmount() {
		return drugChargeAmount;
	}

	public void setDrugChargeAmount(Double drugChargeAmount) {
		this.drugChargeAmount = drugChargeAmount;
	}

	public String getDrugName() {
		return drugName;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public String getFormCode() {
		return formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	public String getActionStatus() {
		return actionStatus;
	}

	public void setActionStatus(String actionStatus) {
		this.actionStatus = actionStatus;
	}

	public Integer getOrgItemNum() {
		return orgItemNum;
	}

	public void setOrgItemNum(Integer orgItemNum) {
		this.orgItemNum = orgItemNum;
	}

}
