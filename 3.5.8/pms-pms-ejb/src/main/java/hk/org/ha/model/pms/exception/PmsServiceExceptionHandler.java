package hk.org.ha.model.pms.exception;

import hk.org.ha.model.pms.exception.vetting.mp.ConcurrentUpdateException;
import hk.org.ha.model.pms.exception.vetting.mp.SaveNewProfileException;

import org.granite.logging.Logger;
import org.granite.messaging.service.ServiceException;
import org.granite.messaging.service.ServiceInvocationContext;
import org.granite.messaging.service.security.SecurityServiceException;
import org.granite.tide.seam21.Seam21ServiceExceptionHandler;

public class PmsServiceExceptionHandler extends Seam21ServiceExceptionHandler {

	private static final Logger log = Logger.getLogger(PmsServiceExceptionHandler.class);
	
	private static final long serialVersionUID = 1L;
	
	public PmsServiceExceptionHandler() {
		super();
	}

	public PmsServiceExceptionHandler(boolean logException) {
		super(logException);
	}

	@Override
    public ServiceException handleInvocationException(ServiceInvocationContext context, Throwable e) {

		if (getLogException()) {
	    	if (e instanceof SecurityServiceException || 
	    			e instanceof ConcurrentUpdateException ||
	    			e instanceof SaveNewProfileException)
	            log.debug(e, "Could not process remoting message: %s", context.getMessage());
	        else
	            log.error(e, "Could not process remoting message: %s", context.getMessage());
    	}

        return getServiceException(context, e);
	}
	
}
