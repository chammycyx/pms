package hk.org.ha.model.pms.udt.vetting;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DailyFreqSort implements StringValuedEnum{
	Rank("rank", "Sort by rank"),
	DailyFreqDesc("dailyFreqDesc", "Sort by daily frequency description");
	
	private final String dataValue;
	private final String displayValue;
	
	DailyFreqSort (final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}

	public static DailyFreqSort dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DailyFreqSort.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<DailyFreqSort> {

		private static final long serialVersionUID = 1L;

		public Class<DailyFreqSort> getEnumClass() {
    		return DailyFreqSort.class;
    	}
    }
}
 