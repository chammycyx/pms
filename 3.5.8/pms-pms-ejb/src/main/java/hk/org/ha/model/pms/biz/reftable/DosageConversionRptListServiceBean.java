package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.corp.cache.DmFormCacherInf;
import hk.org.ha.model.pms.corp.cache.MsFmStatusCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmForm;
import hk.org.ha.model.pms.dms.persistence.MsFmStatus;
import hk.org.ha.model.pms.dms.persistence.MsWorkstoreDrug;
import hk.org.ha.model.pms.dms.persistence.PmsDosageConversion;
import hk.org.ha.model.pms.dms.persistence.PmsDosageConversionSet;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.DosageConversionRpt;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsRptServiceJmsRemote;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("dosageConversionRptListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DosageConversionRptListServiceBean implements DosageConversionRptListServiceLocal {
	
	@In
	private Workstore workstore;
	
	@In
	private DmsRptServiceJmsRemote dmsRptServiceProxy;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private MsFmStatusCacherInf msFmStatusCacher;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent; 
	
	@In
	private DmFormCacherInf dmFormCacher;
	
	private List<DosageConversionRpt> dosageConversionRptList;
	
	private Map<String, Object> parameters = new HashMap<String, Object>();
	
	private boolean retrieveSuccess;
	
	public void genDosageConversionRptList() {
		parameters.put("hospCode", workstore.getHospCode());
		parameters.put("workstoreCode", workstore.getWorkstoreCode());	

		List<PmsDosageConversionSet> pmsDosageConversionSetList = dmsRptServiceProxy.retrievePmsDosageConversionSetList(workstore.getHospCode(), workstore.getWorkstoreCode());
		dosageConversionRptList = new ArrayList<DosageConversionRpt>();
		
		for (PmsDosageConversionSet pmsDosageConversionSet : pmsDosageConversionSetList) {
			StringBuilder drug = new StringBuilder();
			if( pmsDosageConversionSet.getDmDrugProperty() != null ){
				drug.append(pmsDosageConversionSet.getDmDrugProperty().getDisplayname()).append(" ");   
				
				if (pmsDosageConversionSet.getDmDrugProperty().getSaltProperty()!=null) {
					drug.append(pmsDosageConversionSet.getDmDrugProperty().getSaltProperty()).append(" ");
				}
	
				DmForm dmForm = dmFormCacher.getFormByFormCode(pmsDosageConversionSet.getDmDrugProperty().getFormCode());
				
		    	if ( "Y".equals(dmForm.getDisplayRouteDescIndicator()) && dmForm.getDmRoute()!=null ){
					drug.append(StringUtils.trimToEmpty( StringUtils.trimToEmpty(dmForm.getDmRoute().getFullRouteDesc())))
						.append(" ")
						.append(StringUtils.trimToEmpty( dmForm.getMoeDesc()));
				} else {
					drug.append(StringUtils.trimToEmpty( dmForm.getMoeDesc()));
				}
			
				MsFmStatus msFmStatus = msFmStatusCacher.getMsFmStatusByFmStatus(pmsDosageConversionSet.getPmsFmStatus());
				if (msFmStatus != null && !FmStatus.GeneralDrug.getDataValue().equals(msFmStatus.getFmStatus())) {
					drug.append(" ").append("<").append(msFmStatus.getFmStatusDesc()).append(">");
				}
			}
			
			for( PmsDosageConversion pmsDosageConversion : pmsDosageConversionSet.getPmsDosageConversionList() ){
				DosageConversionRpt dosageConversionRpt = new DosageConversionRpt();
				dosageConversionRpt.setDrug(drug.toString());
				dosageConversionRpt.setMoDosage( pmsDosageConversionSet.getMoDosage() );
				
				if (StringUtils.isNumeric( StringUtils.left(pmsDosageConversionSet.getMoDosageUnit(),1))) {
					dosageConversionRpt.setMoDosageUnitMultiply("x ");
				} else {
					dosageConversionRpt.setMoDosageUnitMultiply("");
				}

				dosageConversionRpt.setMoDosageUnit( pmsDosageConversionSet.getMoDosageUnit() );
				dosageConversionRpt.setItemCode(pmsDosageConversion.getItemCode());	
				
				if(pmsDosageConversion.getDmDrug() != null){
					dosageConversionRpt.setFullDrugDesc(pmsDosageConversion.getDmDrug().getFullDrugDesc());
					
					MsWorkstoreDrug msWorkstoreDrug = dmDrugCacher.getMsWorkstoreDrug(workstore, pmsDosageConversion.getDmDrug().getItemCode());
					if (msWorkstoreDrug != null) {
						dosageConversionRpt.setSuspend(msWorkstoreDrug.getSuspend());
					}else {
						dosageConversionRpt.setSuspend("Y");
					}
				}
				
				if( pmsDosageConversion.getDispenseDosage() != null ){
					dosageConversionRpt.setDispDosage( pmsDosageConversion.getDispenseDosage() );
					
					if (StringUtils.isNumeric( StringUtils.left(pmsDosageConversion.getDispenseDosageUnit(),1))) {
						dosageConversionRpt.setDispDosageUnitMultiply("x ");
					} else {
						dosageConversionRpt.setDispDosageUnitMultiply("");
					}

					dosageConversionRpt.setDispDosageUnit(pmsDosageConversion.getDispenseDosageUnit());
				}
				
				dosageConversionRptList.add(dosageConversionRpt);
			}
		}
		Collections.sort(dosageConversionRptList, new DosageConversionRptComparator());
		
		if ( dosageConversionRptList.isEmpty() ) {
			retrieveSuccess = false;
		} else{
			retrieveSuccess = true;
		}
	}

	public void exportDosageConversionRpt() {
		if (retrieveSuccess) {
			PrintOption po = new PrintOption();
			po.setDocType(ReportProvider.XLS);
			printAgent.renderAndRedirect(new RenderJob(
					"DosageConversionXls", 
					po, 
					parameters, 
					dosageConversionRptList));
		}
		

	}
	
	public void printDosageConversionRpt(){
		genDosageConversionRptList();
		PrintOption po = new PrintOption();
		po.setDocType(ReportProvider.PDF);
		printAgent.renderAndPrint(new RenderAndPrintJob(
				"DosageConversionRpt", 
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				parameters, 
				dosageConversionRptList));
	}
	
	public static class DosageConversionRptComparator implements Comparator<DosageConversionRpt>, Serializable {

		private static final long serialVersionUID = 1L;
		
		@Override
		public int compare(DosageConversionRpt dosageConverRpt1, DosageConversionRpt dosageConverRpt2) {			
			return new CompareToBuilder().append( dosageConverRpt1.getDrug(), dosageConverRpt2.getDrug())
										.append( dosageConverRpt1.getMoDosage(), dosageConverRpt2.getMoDosage())
										.append( dosageConverRpt1.getMoDosageUnit(), dosageConverRpt2.getMoDosageUnit())
										.append( dosageConverRpt1.getItemCode(), dosageConverRpt2.getItemCode())
										.toComparison();			
		}
	}
	
	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}
	
	@Remove
	public void destroy() {
		if (dosageConversionRptList != null){
			dosageConversionRptList = null;
		}
	}	
}
