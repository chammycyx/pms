package hk.org.ha.model.pms.biz.reftable.pivas;

import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
import hk.org.ha.model.pms.persistence.reftable.pivas.PivasServiceHour;

import java.util.List;

import javax.ejb.Local;

@Local
public interface PivasServiceHourListManagerLocal {
	List<PivasServiceHour> retrievePivasServiceHourList(WorkstoreGroup workstoreGroup);
	
	void updatePivasServiceHourList(List<PivasServiceHour> updatePivasServiceHourList, 
										List<PivasServiceHour> deletePivasServiceHourList, 
										WorkstoreGroup workstoreGroup);
}
