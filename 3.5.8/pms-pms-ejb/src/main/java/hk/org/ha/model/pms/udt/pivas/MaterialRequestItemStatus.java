package hk.org.ha.model.pms.udt.pivas;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

import java.util.Arrays;
import java.util.List;

public enum MaterialRequestItemStatus implements StringValuedEnum {

	None("-", "None"),
	Outstanding("O", "Outstanding"),
	Processed("P", "Processed"),
	Deleted("D", "Deleted"),
	SysDeleted("X", "SysDeleted");
	
	private final String dataValue;
	private final String displayValue;
	
	public static final List<MaterialRequestItemStatus> None_Outstanding = Arrays.asList(None, Outstanding);

	private MaterialRequestItemStatus(String dataValue,
			String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}
	
	public static MaterialRequestItemStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MaterialRequestItemStatus.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<MaterialRequestItemStatus> {
		
		private static final long serialVersionUID = 1L;

		public Class<MaterialRequestItemStatus> getEnumClass() {
			return MaterialRequestItemStatus.class;
		}
	}
	
}
