package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmMedicalOfficerCacher;
import hk.org.ha.model.pms.dms.persistence.DmMedicalOfficer;
import hk.org.ha.model.pms.persistence.corp.Workstore;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("dmMedicalOfficerService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DmMedicalOfficerServiceBean implements DmMedicalOfficerServiceLocal {

	@Logger
	private Log logger;
	
	@In
	private Workstore workstore;

	@In
	private DmMedicalOfficerCacher dmMedicalOfficerCacher;

	@Out(required = false)
	private DmMedicalOfficer dmMedicalOfficer;
	
	private boolean retrieveSuccess;
	
	public void retrieveDmMedicalOfficer(String medicalOfficerCode)
	{
		logger.debug("retrieveDmMedicalOfficer #0", medicalOfficerCode);

		retrieveSuccess = false;		
		dmMedicalOfficer = dmMedicalOfficerCacher.getDmMedicalOfficerByMedicalOfficerCode(workstore, medicalOfficerCode);
		if (dmMedicalOfficer != null) {
			retrieveSuccess = true;
		}
		
		logger.debug("retrieveDmMedicalOfficer result #0", retrieveSuccess);
	}

	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}
	
	@Remove
	public void destroy() 
	{
		if (dmMedicalOfficer != null) {
			dmMedicalOfficer = null;
		}
	}

}
