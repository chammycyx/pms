package hk.org.ha.model.pms.udt.reftable;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PrinterConnectionType implements StringValuedEnum {

	Yes("Y", "Yes"),
	No("N", "No"),
	Private("R", "Private");
 
    private final String dataValue;
    private final String displayValue;

    PrinterConnectionType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }
    
    public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}

	public static PrinterConnectionType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PrinterConnectionType.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<PrinterConnectionType> {

		private static final long serialVersionUID = 1L;

		public Class<PrinterConnectionType> getEnumClass() {
			return PrinterConnectionType.class;
		}
		
	}
}
