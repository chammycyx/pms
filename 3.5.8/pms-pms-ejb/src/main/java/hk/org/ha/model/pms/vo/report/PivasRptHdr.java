package hk.org.ha.model.pms.vo.report;

import hk.org.ha.model.pms.udt.YesNoBlankFlag;
import hk.org.ha.model.pms.udt.report.PivasRptDispMethod;
import hk.org.ha.model.pms.udt.report.PivasRptItemCatFilterOption;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class PivasRptHdr implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Date supplyDateFrom;
	
	private Date supplyDateTo;
	
	private YesNoBlankFlag satelliteFlag;
	
	private String hkidCaseNum;
	
	private PivasRptDispMethod pivasRptDispMethod;
	
	private PivasRptItemCatFilterOption pivasRptItemCatFilterOption;
	
	private String itemCode;
	
	private Boolean drugKeyFlag;
	
	private String manufCode;
	
	private String batchNum;

	
	public Date getSupplyDateFrom() {
		return supplyDateFrom;
	}

	public void setSupplyDateFrom(Date supplyDateFrom) {
		this.supplyDateFrom = supplyDateFrom;
	}

	public Date getSupplyDateTo() {
		return supplyDateTo;
	}

	public void setSupplyDateTo(Date supplyDateTo) {
		this.supplyDateTo = supplyDateTo;
	}

	public YesNoBlankFlag getSatelliteFlag() {
		return satelliteFlag;
	}

	public void setSatelliteFlag(YesNoBlankFlag satelliteFlag) {
		this.satelliteFlag = satelliteFlag;
	}

	public String getHkidCaseNum() {
		return hkidCaseNum;
	}

	public void setHkidCaseNum(String hkidCaseNum) {
		this.hkidCaseNum = hkidCaseNum;
	}

	public PivasRptDispMethod getPivasRptDispMethod() {
		return pivasRptDispMethod;
	}

	public void setPivasRptDispMethod(PivasRptDispMethod pivasRptDispMethod) {
		this.pivasRptDispMethod = pivasRptDispMethod;
	}

	public PivasRptItemCatFilterOption getPivasRptItemCatFilterOption() {
		return pivasRptItemCatFilterOption;
	}

	public void setPivasRptItemCatFilterOption(
			PivasRptItemCatFilterOption pivasRptItemCatFilterOption) {
		this.pivasRptItemCatFilterOption = pivasRptItemCatFilterOption;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public Boolean getDrugKeyFlag() {
		return drugKeyFlag;
	}

	public void setDrugKeyFlag(Boolean drugKeyFlag) {
		this.drugKeyFlag = drugKeyFlag;
	}

	public String getManufCode() {
		return manufCode;
	}

	public void setManufCode(String manufCode) {
		this.manufCode = manufCode;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}
}
