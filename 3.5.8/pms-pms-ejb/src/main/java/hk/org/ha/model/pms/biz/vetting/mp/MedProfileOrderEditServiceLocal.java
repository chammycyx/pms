package hk.org.ha.model.pms.biz.vetting.mp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.Replenishment;
import hk.org.ha.model.pms.persistence.pivas.PivasWorklist;
import hk.org.ha.model.pms.vo.chemo.ChemoItem;
import hk.org.ha.model.pms.vo.pivas.worklist.PivasItemEditInfo;
import hk.org.ha.model.pms.vo.pivas.worklist.PivasSupplyDateListInfo;

import javax.ejb.Local;

@Local
public interface MedProfileOrderEditServiceLocal {

	void cancelFromOrderEdit(Long id, Integer itemNum);
	void saveItemFromOrderEdit(MedProfileItem medProfileItemIn);
	Replenishment retrieveReplenishmentFromBackendContext(Long id);
	List<MedProfileItem> cancelFromManualEntryItemEdit(List<Integer> itemNumList);
	List<MedProfileItem> saveItemFromManualEntryItemEdit(List<MedProfileItem> mpiList);
	MedProfile updateMedProfile(MedProfile medProfileIn, boolean patientInfoUpdated);
	void updateManualEntryVettingAssociation(Map<String, Integer> map);
	MedProfileItem reactivateDiscontinuedItem(MedProfileItem mpi);
	MedProfileMoItem changeItemCodeFromManualEntryItemEdit(String itemCode, String displayName);
	Date retrieveNextAdminDueDate(List<String> dailyFreqCodeList, Boolean pivasFlag);
	void updateBackendMedProfileItemList(List<MedProfileItem> mpiList);
	Object[] calculatePivasDueDateAndSupplyDate(String dailyFreqCode, Date dueDate);
	PivasSupplyDateListInfo retrievePivasSupplyDateListInfoByDueDate(Date dueDate);
	PivasItemEditInfo retrievePivasItemEditInfo(MedProfileMoItem medProfileMoItem);
	void updatePivasDetailToContext(PivasWorklist pivasWorklistIn);
	List<MedProfileItem> splitChemoMedProfileItem(MedProfileItem medProfileItem, List<ChemoItem> chemoItemList);
	void destroy();
	
}
