package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class PivasWorklistSummaryRptWorklistDtl implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String patName;
	
	private String patNameChi;
	
	private String caseNum;
	
	private String specCode;
	
	private String ward;
	
	private String bedNum;
	
	private String sex;
	
	private String age;
	
	private String bodyWeight;
	
	private Date pivasDueDate;
	
	private String prepDesc;
	
	private String drugItemCode;
	
	private String solventItemCode;
	
	private String diluentItemCode;
	
	private String drugManufCode;
	
	private String solventManufCode;
	
	private String diluentManufCode;
	
	private String drugBatchNum;
	
	private String solventBatchNum;
	
	private String diluentBatchNum;
	
	private Date drugExpiryDate;
	
	private Date solventExpiryDate;
	
	private Date diluentExpiryDate;
	
	private String supplyDesc;
	
	private String pivasRequestDesc;
	
	private String vendSupplySolvDesc;
	
	private Long pivasWorklistId;
	
	private boolean vendSupplySolvFlag;

	public PivasWorklistSummaryRptWorklistDtl() {
		vendSupplySolvFlag = Boolean.FALSE;
	}
	
	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getPatNameChi() {
		return patNameChi;
	}

	public void setPatNameChi(String patNameChi) {
		this.patNameChi = patNameChi;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public String getWard() {
		return ward;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}

	public String getBedNum() {
		return bedNum;
	}

	public void setBedNum(String bedNum) {
		this.bedNum = bedNum;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getBodyWeight() {
		return bodyWeight;
	}

	public void setBodyWeight(String bodyWeight) {
		this.bodyWeight = bodyWeight;
	}

	public Date getPivasDueDate() {
		return pivasDueDate;
	}

	public void setPivasDueDate(Date pivasDueDate) {
		this.pivasDueDate = pivasDueDate;
	}

	public String getPrepDesc() {
		return prepDesc;
	}

	public void setPrepDesc(String prepDesc) {
		this.prepDesc = prepDesc;
	}

	public String getDrugItemCode() {
		return drugItemCode;
	}

	public void setDrugItemCode(String drugItemCode) {
		this.drugItemCode = drugItemCode;
	}

	public String getSolventItemCode() {
		return solventItemCode;
	}

	public void setSolventItemCode(String solventItemCode) {
		this.solventItemCode = solventItemCode;
	}

	public String getDiluentItemCode() {
		return diluentItemCode;
	}

	public void setDiluentItemCode(String diluentItemCode) {
		this.diluentItemCode = diluentItemCode;
	}

	public String getDrugManufCode() {
		return drugManufCode;
	}

	public void setDrugManufCode(String drugManufCode) {
		this.drugManufCode = drugManufCode;
	}

	public String getSolventManufCode() {
		return solventManufCode;
	}

	public void setSolventManufCode(String solventManufCode) {
		this.solventManufCode = solventManufCode;
	}

	public String getDiluentManufCode() {
		return diluentManufCode;
	}

	public void setDiluentManufCode(String diluentManufCode) {
		this.diluentManufCode = diluentManufCode;
	}

	public String getDrugBatchNum() {
		return drugBatchNum;
	}

	public void setDrugBatchNum(String drugBatchNum) {
		this.drugBatchNum = drugBatchNum;
	}

	public String getSolventBatchNum() {
		return solventBatchNum;
	}

	public void setSolventBatchNum(String solventBatchNum) {
		this.solventBatchNum = solventBatchNum;
	}

	public String getDiluentBatchNum() {
		return diluentBatchNum;
	}

	public void setDiluentBatchNum(String diluentBatchNum) {
		this.diluentBatchNum = diluentBatchNum;
	}

	public Date getDrugExpiryDate() {
		return drugExpiryDate;
	}

	public void setDrugExpiryDate(Date drugExpiryDate) {
		this.drugExpiryDate = drugExpiryDate;
	}

	public Date getSolventExpiryDate() {
		return solventExpiryDate;
	}

	public void setSolventExpiryDate(Date solventExpiryDate) {
		this.solventExpiryDate = solventExpiryDate;
	}

	public Date getDiluentExpiryDate() {
		return diluentExpiryDate;
	}

	public void setDiluentExpiryDate(Date diluentExpiryDate) {
		this.diluentExpiryDate = diluentExpiryDate;
	}

	public String getSupplyDesc() {
		return supplyDesc;
	}

	public void setSupplyDesc(String supplyDesc) {
		this.supplyDesc = supplyDesc;
	}

	public String getPivasRequestDesc() {
		return pivasRequestDesc;
	}

	public void setPivasRequestDesc(String pivasRequestDesc) {
		this.pivasRequestDesc = pivasRequestDesc;
	}

	public String getVendSupplySolvDesc() {
		return vendSupplySolvDesc;
	}

	public void setVendSupplySolvDesc(String vendSupplySolvDesc) {
		this.vendSupplySolvDesc = vendSupplySolvDesc;
	}

	public Long getPivasWorklistId() {
		return pivasWorklistId;
	}

	public void setPivasWorklistId(Long pivasWorklistId) {
		this.pivasWorklistId = pivasWorklistId;
	}

	public boolean isVendSupplySolvFlag() {
		return vendSupplySolvFlag;
	}

	public void setVendSupplySolvFlag(boolean vendSupplySolvFlag) {
		this.vendSupplySolvFlag = vendSupplySolvFlag;
	}

}
