package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.reftable.SuspendReason;

import java.util.Collection;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("suspendReasonListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SuspendReasonListServiceBean implements SuspendReasonListServiceLocal {
		
	@Out(required = false)
	private List<SuspendReason> suspendReasonList;
	
	@In
	private SuspendReasonManagerLocal suspendReasonManager;
	
	public void retrieveSuspendReasonList() {
		suspendReasonList = suspendReasonManager.retrieveSuspendReasonList();
	}
	
	public void updateSuspendReasonList(Collection<SuspendReason> newSuspendList) {
		suspendReasonManager.updateSuspendReasonList(newSuspendList);
	}

	@Remove
	public void destroy() {
		if (suspendReasonList != null) {
			suspendReasonList = null;
		}
	}
}
