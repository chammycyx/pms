package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.vetting.OrderType;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("fdnMappingService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class FdnMappingServiceBean implements FdnMappingServiceLocal {

	@In(required=false)
	private Workstore workstore;
	
	@In
	private RefTableManagerLocal refTableManager;	
		
	public String retrieveFloatingDrugName(String itemCode, OrderType orderType) 
	{
		return refTableManager.getFdnByItemCodeOrderTypeWorkstore(itemCode, orderType, workstore);
	}	
			
	@Remove
	public void destroy() {
	}
}
