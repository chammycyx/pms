package hk.org.ha.model.pms.biz.enquiry;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.util.QueryUtils;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.medprofile.Delivery;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.udt.medprofile.DeliveryItemStatus;
import hk.org.ha.model.pms.vo.enquiry.DispItemStatusEnq;
import hk.org.ha.model.pms.vo.label.LabelContainer;
import hk.org.ha.model.pms.vo.label.MpDispLabel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.joda.time.DateTime;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("dispItemStatusEnqService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DispItemStatusEnqServiceBean implements DispItemStatusEnqServiceLocal {

	@PersistenceContext
	private EntityManager em;
	
	@In 
	private Workstore workstore;

	@Out(required=false)
	private List<DispItemStatusEnq> dispItemStatusEnqList;
	
	private static final List<DeliveryItemStatus> NON_PROCESSING_DELIVERY_ITEM_STATUS_LIST = Arrays.asList(DeliveryItemStatus.Delivered,
																									DeliveryItemStatus.KeepRecord,
																									DeliveryItemStatus.Deleted);

	private static final char[] DELIMITER = new char[] {' ' , '~' , '&' , '-' , '[' , '(' , '<'}; 

	private static final String JAXB_CONTEXT_LABEL = "hk.org.ha.model.pms.vo.label";

	private static JaxbWrapper<Object> labelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
	
	private boolean retrieveSuccess = false;

	public void retrieveDispItemStatusEnqList(DispItemStatusEnq dispItemStatusEnqCriteria) {
		
		dispItemStatusEnqList = new ArrayList<DispItemStatusEnq>();
		
		List<DeliveryItem> deliveryItemList = retrieveDeliveryItemList(dispItemStatusEnqCriteria);	
		Map<Long, Integer> latestItemNumMap = retrieveLatestItemNumMap(deliveryItemList);
		for ( DeliveryItem deliveryItem : deliveryItemList ){
			dispItemStatusEnqList.add(constructDispItemStatusEnq(latestItemNumMap, deliveryItem));			
		}
		
    	Collections.sort(dispItemStatusEnqList, new DispItemStatusEnqComparator());

		if (dispItemStatusEnqList.size()>0) {
			retrieveSuccess = true;
		} else {
			dispItemStatusEnqList = null;
			retrieveSuccess = false;
		}
	}
	
	private DispItemStatusEnq constructDispItemStatusEnq(Map<Long, Integer> latestItemNumMap, DeliveryItem deliveryItem){

		Delivery delivery = deliveryItem.getDelivery();

		
		MedProfileMoItem medProfileMoItem = deliveryItem.getMedProfileMoItem();
		MedProfileItem medProfileItem = medProfileMoItem.getMedProfileItem();
		MedProfile medProfile = deliveryItem.getMedProfile();
		MedCase medCase = medProfile.getMedCase();
		
		DispItemStatusEnq dispItemStatusEnq = new DispItemStatusEnq();
		
		dispItemStatusEnq.setLabelGenDate(deliveryItem.getCreateDate());
		dispItemStatusEnq.setBatchCode(delivery.getBatchNum());
		dispItemStatusEnq.setCaseNum(medCase.getCaseNum());
		//PMS-2730
		dispItemStatusEnq.setDueDate(deliveryItem.getDueDate());
		
		dispItemStatusEnq.setItemCode(deliveryItem.getItemCode());

		Object label = labelJaxbWrapper.unmarshall(deliveryItem.getLabelXml());
		MedProfilePoItem medProfilePoItem = deliveryItem.resolveMedProfilePoItem();
		
		if (deliveryItem.getPivasWorklist() != null) {
			dispItemStatusEnq.setItemDescTlf(label instanceof LabelContainer ? ((LabelContainer) label).getPivasProductLabel().getDeliveryItemDescTlf() : null);
			dispItemStatusEnq.setBaseUnit("DOSE");
		} else if (medProfilePoItem != null){
			dispItemStatusEnq.setItemDesc(buildDrugDesc(
											medProfilePoItem.getDrugName(), 
											medProfilePoItem.getFormLabelDesc(), 
											medProfilePoItem.getStrength(),
											medProfilePoItem.getVolumeText(),
											label instanceof MpDispLabel ? ((MpDispLabel) label).getFloatingDrugName() : null));
			dispItemStatusEnq.setBaseUnit(medProfilePoItem.getBaseUnit());
		}
		
		dispItemStatusEnq.setItemStatus(deliveryItem.getStatus().getDisplayValue());
		dispItemStatusEnq.setOrderStatus(
				MedProfileUtils.compare(latestItemNumMap.get(medProfileItem.getMedProfileMoItemId()), medProfileMoItem.getItemNum()) != 0 ?
				"Modified" : medProfileItem.getStatus().getDisplayValue());
		dispItemStatusEnq.setTrxId(deliveryItem.getId().toString());
		dispItemStatusEnq.setWardCode(delivery.getWardCode());
		dispItemStatusEnq.setBatchDate(delivery.getBatchDate());
		dispItemStatusEnq.setDispQty(deliveryItem.getIssueQty().intValue());
		
		return dispItemStatusEnq;
	}
	
	private String buildDrugDesc(String drugName, String formLabelDesc, String strength, String volumeText, String synonym){
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(WordUtils.capitalizeFully(drugName, DELIMITER));
		if (StringUtils.isNotBlank(formLabelDesc)){
			sb.append(" ").append(WordUtils.capitalizeFully(formLabelDesc, DELIMITER));
		}
		if (StringUtils.isNotBlank(strength)){
			sb.append(" ").append(WordUtils.capitalizeFully(strength, DELIMITER));
		}
		if (StringUtils.isNotBlank(volumeText)){
			sb.append(" ").append(volumeText.toLowerCase());
		}
		if (StringUtils.isNotBlank(synonym)){
			sb.append(" (").append(WordUtils.capitalizeFully(synonym, DELIMITER)).append(")");
		}
		
		return sb.toString();
		
	}

    private static class DispItemStatusEnqComparator implements Comparator<DispItemStatusEnq>, Serializable {

    	private static final long serialVersionUID = 1L;

    	@Override
		public int compare(DispItemStatusEnq dispItemStatusEnq1, DispItemStatusEnq dispItemStatusEnq2) {
			int compareResult = 0;
			Date batchDate1 = dispItemStatusEnq1.getBatchDate();
			Date batchDate2 = dispItemStatusEnq2.getBatchDate();
			compareResult = batchDate1.compareTo(batchDate2);

			if (compareResult == 0) {
				String batchCode1 = dispItemStatusEnq1.getBatchCode();
				String batchCode2 = dispItemStatusEnq2.getBatchCode();
				compareResult = batchCode1.compareTo(batchCode2);
			}

			if (compareResult == 0) {
				String wardCode1 = dispItemStatusEnq1.getWardCode();
				String wardCode2 = dispItemStatusEnq2.getWardCode();
				compareResult = wardCode1.compareTo(wardCode2);
			}

			if (compareResult == 0) {
				String caseNum1 = StringUtils.trimToEmpty(dispItemStatusEnq1.getCaseNum());
				String caseNum2 = StringUtils.trimToEmpty(dispItemStatusEnq2.getCaseNum());
				compareResult = caseNum1.compareTo(caseNum2);
			}

			if (compareResult == 0) {
				String itemCode1 = dispItemStatusEnq1.getItemCode();
				String itemCode2 = dispItemStatusEnq2.getItemCode();
				compareResult = itemCode1.compareTo(itemCode2);
			}

			if (compareResult == 0) {
				Date labelGenDate1 = dispItemStatusEnq1.getLabelGenDate();
				Date labelGenDate2 = dispItemStatusEnq2.getLabelGenDate();
				compareResult = labelGenDate1.compareTo(labelGenDate2);
			}
			
			return compareResult;
		}
    }
	
	private List<DeliveryItem> retrieveDeliveryItemList(DispItemStatusEnq dispItemStatusEnq) {
		
		if (dispItemStatusEnq.getCaseNum() != null) {
			
			return retrieveDeliveryItemListByCaseNum(dispItemStatusEnq);
		
		} else if (dispItemStatusEnq.getTrxId() != null) {
			
			return retrieveDeliveryItemListByTrxId(dispItemStatusEnq);
			
		} else if (dispItemStatusEnq.getLabelGenDateFrom() != null) {
			
			return retrieveDeliveryItemListByLabelGenDate(dispItemStatusEnq);
			
		} else {
			
			return retrieveDeliveryItemListInProcessing(dispItemStatusEnq);
			
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<DeliveryItem> retrieveDeliveryItemListByCaseNum(DispItemStatusEnq dispItemStatusEnq) {
		
		StringBuilder sql = new StringBuilder(
				"select o from DeliveryItem o"); // 20120314 index check : MedCase.caseNum : I_MED_CASE_01
		
		sql.append(" where o.medProfile.medCase.caseNum = :caseNum");
		
		sql.append(" and o.delivery.hospital = :hospital");
		
		Query query =  em.createQuery(sql.toString());
		
		query.setParameter("caseNum", dispItemStatusEnq.getCaseNum());

		query.setParameter("hospital", workstore.getHospital());

		updateCommonQueryHints(query);
		
		return (List<DeliveryItem>) query.getResultList();
	}
	@SuppressWarnings("unchecked")
	private List<DeliveryItem> retrieveDeliveryItemListByTrxId(DispItemStatusEnq dispItemStatusEnq) {

		if (StringUtils.isNotBlank(dispItemStatusEnq.getTrxId()) && StringUtils.isNumeric(dispItemStatusEnq.getTrxId())) {
			
			Query query =  em.createQuery(
					"select o from DeliveryItem o" + // 20120314 index check : DeliveryItem.id : PK_DELIVERY_ITEM
					" where o.id = :deliveryItemId" +
					" and o.delivery.hospital = :hospital")
					.setParameter("deliveryItemId", Long.parseLong(dispItemStatusEnq.getTrxId()))
					.setParameter("hospital", workstore.getHospital());
	
			updateCommonQueryHints(query);
			
			return (List<DeliveryItem>) query.getResultList();
			
		} else {
			
			return new ArrayList<DeliveryItem>();
		}
		
	}
	@SuppressWarnings("unchecked")
	private List<DeliveryItem> retrieveDeliveryItemListByLabelGenDate(DispItemStatusEnq dispItemStatusEnq) {
		
		StringBuilder sql = new StringBuilder(
				"select o from DeliveryItem o" + // 20120314 index check : Delivery.hospCode,batchDate,batchNum : I_DELIVERY_01
				" where o.delivery.hospital = :hospital" +
				" and o.delivery.batchDate = :batchDate" +
				" and o.delivery.createDate between :labelGenDateFrom and :labelGenDateTo");
		
		if (dispItemStatusEnq.getBatchCode() != null) {
			sql.append(" and o.delivery.batchNum = :batchNum");
			sql.append(" and o.status <> :deliveryItemStatusUnlink");
		}
		
		if (dispItemStatusEnq.getWardCode() != null) {
			sql.append(" and o.delivery.wardCode = :wardCode");
		}
		
		if (dispItemStatusEnq.getItemCode() != null) {
			sql.append(" and o.itemCode = :itemCode");
		}
		
		Query query =  em.createQuery(sql.toString());

		query.setParameter("hospital", workstore.getHospital());
		query.setParameter("batchDate", dispItemStatusEnq.getLabelGenDateFrom(), TemporalType.DATE);
		query.setParameter("labelGenDateFrom", dispItemStatusEnq.getLabelGenDateFrom());
		query.setParameter("labelGenDateTo", dispItemStatusEnq.getLabelGenDateTo());
		
		if (dispItemStatusEnq.getBatchCode() != null) {
			query.setParameter("batchNum", dispItemStatusEnq.getBatchCode());
			query.setParameter("deliveryItemStatusUnlink", DeliveryItemStatus.Unlink);
		}

		if (dispItemStatusEnq.getWardCode() != null) {
			query.setParameter("wardCode", dispItemStatusEnq.getWardCode());
		}
		if (dispItemStatusEnq.getItemCode() != null) {
			query.setParameter("itemCode", dispItemStatusEnq.getItemCode());
		}

		updateCommonQueryHints(query);
		
		return (List<DeliveryItem>) query.getResultList();
	}
	
	
	@SuppressWarnings("unchecked")
	private List<DeliveryItem> retrieveDeliveryItemListInProcessing(DispItemStatusEnq dispItemStatusEnq) {

		DateTime dateTo = new DateTime(dispItemStatusEnq.getAllItemInProcessToDate()).plusDays(1);
		DateTime deliveryItemDateFrom = new DateTime(dispItemStatusEnq.getAllItemInProcessFromDate());
		DateTime deliveryDateFrom = deliveryItemDateFrom.minusDays(1);

		Query query =  em.createQuery(
				"select o from DeliveryItem o" + // 20120314 index check : Delivery.hospital,batchDate : I_DELIVERY_01
				" where o.delivery.hospital = :hospital" +
				" and o.delivery.batchDate between :deliveryDateFrom and :deliveryDateTo" +
				" and o.delivery.createDate between :deliveryDateFrom and :deliveryDateTo" +
				" and o.status not in :deliveryItemStatusList" +
				" and o.createDate between :deliveryItemDateFrom and :deliveryItemDateTo")
				.setParameter("hospital", workstore.getHospital())
				.setParameter("deliveryDateFrom", deliveryDateFrom.toDate(), TemporalType.DATE)
				.setParameter("deliveryDateTo", dateTo.toDate(), TemporalType.DATE)
				.setParameter("deliveryItemDateFrom", deliveryItemDateFrom.toDate(), TemporalType.DATE)
				.setParameter("deliveryItemDateTo", dateTo.toDate(), TemporalType.DATE)
				.setParameter("deliveryItemStatusList", NON_PROCESSING_DELIVERY_ITEM_STATUS_LIST);
		
		updateCommonQueryHints(query);
		
		return (List<DeliveryItem>) query.getResultList();
	}
	
	private void updateCommonQueryHints(Query query) {

		query.setHint(QueryHints.FETCH, "o.delivery")
			.setHint(QueryHints.LEFT_FETCH, "o.medProfilePoItem.medProfileMoItem.medProfileItem.medProfile.medCase")
			.setHint(QueryHints.LEFT_FETCH, "o.pivasWorklist.pivasPrep.medProfileMoItem.medProfileItem");
	}
	
	@SuppressWarnings("unchecked")
	private Map<Long, Integer> retrieveLatestItemNumMap(List<DeliveryItem> deliveryItemList) {
		
		Set<Long> idSet = new HashSet<Long>();
		
		for (DeliveryItem deliveryItem: deliveryItemList) {
			idSet.add(deliveryItem.getMedProfileMoItem().getMedProfileItem().getMedProfileMoItemId());
		}

		List<Object[]> resultList = QueryUtils.splitExecute(em.createQuery(
				"select distinct o.id, o.itemNum from MedProfileMoItem o" + // 20151012 index check : MedProfileMoItem.id : PK_MED_PROFILE_MO_ITEM
				" where o.id in :idSet"),
				"idSet", idSet);
		
		Map<Long, Integer> resultMap = new HashMap<Long, Integer>();
		for (Object[] item: resultList) {
			resultMap.put((Long) item[0], (Integer) item[1]);
		}
		
		return resultMap;
	}

	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}
	
	@Remove
	public void destroy(){
		if ( dispItemStatusEnqList != null ){
			dispItemStatusEnqList = null;
		}
	}
}
