package hk.org.ha.model.pms.vo.alert.mds;

import hk.org.ha.model.pms.persistence.AlertEntity;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.udt.disp.OnHandSourceType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MdsResult implements Serializable {

	private static final long serialVersionUID = 1L;

	private Map<Integer, List<AlertEntity>> alertEntityMap;

	private List<ErrorInfo> errorInfoList;
	
	private MdsLogInfo mdsLogInfo;
	
	private List<String> skipAllergyList;
	
	private List<String> skipAdrList;
	
	private List<MedOrderItem> skipPrescItemList;
	
	private List<MedOrderItem> skipOnHandItemList;
	
	private List<MedOrderItem> changePrescItemList;
	
	private List<MedOrderItem> changeOnHandItemList;
	
	private List<SteroidAlert> steroidAlertList;
	
	private boolean onHandExceptionFlag;
	
	private OnHandSourceType ddimOnHandSourceType;
	
	public Map<Integer, List<AlertEntity>> getAlertEntityMap() {
		if (alertEntityMap == null) {
			alertEntityMap = new HashMap<Integer, List<AlertEntity>>();
		}
		return alertEntityMap;
	}

	public void setAlertEntityMap(Map<Integer, List<AlertEntity>> alertEntityMap) {
		this.alertEntityMap = alertEntityMap;
	}
	
	public void addAlertEntity(Integer itemNum, AlertEntity alertEntity) {
		List<AlertEntity> alertEntityList;
		if (getAlertEntityMap().containsKey(itemNum)) {
			alertEntityList = getAlertEntityMap().get(itemNum);
		}
		else {
			alertEntityList = new ArrayList<AlertEntity>();
			getAlertEntityMap().put(itemNum, alertEntityList);
		}
		alertEntityList.add(alertEntity);
	}
	
	public List<ErrorInfo> getErrorInfoList() {
		if (errorInfoList == null) {
			errorInfoList = new ArrayList<ErrorInfo>();
		}
		return errorInfoList;
	}

	public void setErrorInfoList(List<ErrorInfo> errorInfoList) {
		this.errorInfoList = errorInfoList;
	}
	
	public void addErrorInfo(ErrorInfo errorInfo) {
		getErrorInfoList().add(errorInfo);
	}

	public MdsLogInfo getMdsLogInfo() {
		return mdsLogInfo;
	}

	public void setMdsLogInfo(MdsLogInfo mdsLogInfo) {
		this.mdsLogInfo = mdsLogInfo;
	}

	public List<String> getSkipAllergyList() {
		if (skipAllergyList == null) {
			skipAllergyList = new ArrayList<String>();
		}
		return skipAllergyList;
	}

	public void setSkipAllergyList(List<String> skipAllergyList) {
		this.skipAllergyList = skipAllergyList;
	}

	public List<String> getSkipAdrList() {
		if (skipAdrList == null) {
			skipAdrList = new ArrayList<String>();
		}
		return skipAdrList;
	}

	public void setSkipAdrList(List<String> skipAdrList) {
		this.skipAdrList = skipAdrList;
	}

	public List<MedOrderItem> getSkipPrescItemList() {
		if (skipPrescItemList == null) {
			skipPrescItemList = new ArrayList<MedOrderItem>();
		}
		return skipPrescItemList;
	}

	public void setSkipPrescItemList(List<MedOrderItem> skipPrescItemList) {
		this.skipPrescItemList = skipPrescItemList;
	}

	public List<MedOrderItem> getSkipOnHandItemList() {
		if (skipOnHandItemList == null) {
			skipOnHandItemList = new ArrayList<MedOrderItem>();
		}
		return skipOnHandItemList;
	}

	public void setSkipOnHandItemList(List<MedOrderItem> skipOnHandItemList) {
		this.skipOnHandItemList = skipOnHandItemList;
	}

	public List<MedOrderItem> getChangePrescItemList() {
		if (changePrescItemList == null) {
			changePrescItemList = new ArrayList<MedOrderItem>();
		}
		return changePrescItemList;
	}

	public void setChangePrescItemList(List<MedOrderItem> changePrescItemList) {
		this.changePrescItemList = changePrescItemList;
	}

	public List<MedOrderItem> getChangeOnHandItemList() {
		if (changeOnHandItemList == null) {
			changeOnHandItemList = new ArrayList<MedOrderItem>();
		}
		return changeOnHandItemList;
	}

	public void setChangeOnHandItemList(List<MedOrderItem> changeOnHandItemList) {
		this.changeOnHandItemList = changeOnHandItemList;
	}
	
	public List<SteroidAlert> getSteroidAlertList() {
		if (steroidAlertList == null) {
			steroidAlertList = new ArrayList<SteroidAlert>();
		}
		return steroidAlertList;
	}

	public void setSteroidAlertList(List<SteroidAlert> steroidAlertList) {
		this.steroidAlertList = steroidAlertList;
	}
	
	public void addSteroidAlert(SteroidAlert steroidAlert) {
		getSteroidAlertList().add(steroidAlert);
	}

	public boolean getOnHandExceptionFlag() {
		return onHandExceptionFlag;
	}

	public void setOnHandExceptionFlag(boolean onHandExceptionFlag) {
		this.onHandExceptionFlag = onHandExceptionFlag;
	}

	public OnHandSourceType getDdimOnHandSourceType() {
		return ddimOnHandSourceType;
	}

	public void setDdimOnHandSourceType(OnHandSourceType ddimOnHandSourceType) {
		this.ddimOnHandSourceType = ddimOnHandSourceType;
	}
}
