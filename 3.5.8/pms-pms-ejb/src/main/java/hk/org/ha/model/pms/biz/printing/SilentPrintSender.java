package hk.org.ha.model.pms.biz.printing;

import hk.org.ha.fmk.pms.jms.AmqSslContext;
import hk.org.ha.fmk.pms.jms.MessageCreator;
import hk.org.ha.fmk.pms.jms.MessageProducer;
import hk.org.ha.fmk.pms.util.DefaultTimeoutMap;
import hk.org.ha.fmk.pms.util.TimeoutMap;
import hk.org.ha.fmk.pms.util.TimeoutMapEntry;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.exception.printing.PrinterExcessThreadLimitException;
import hk.org.ha.model.pms.exception.printing.PrinterHangException;
import hk.org.ha.model.pms.exception.printing.PrintingException;
import hk.org.ha.model.pms.vo.printing.PrintJobInfo;

import java.io.IOException;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.naming.NamingException;

import org.apache.activemq.broker.SslContext;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Context;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;

import commonj.timers.TimerManager;
import commonj.work.Work;
import commonj.work.WorkManager;

@AutoCreate
@Name("silentPrintSender")
@Scope(ScopeType.EVENT)
@MeasureCalls
public class SilentPrintSender implements SilentPrintSenderInf, Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private static final String PROTOCOL = "tcp";
	private static final String SECURE_PROTOCOL = "ssl";
		
	private static final String PRINTER_URL = "printerUrl";
	private static final String SILENT_PRINT_MESSAGE_PRODUCER = "silentPrintMessageProducer";

	private static final Map<String,Object> hostNameMap = new HashMap<String,Object>();
	
	@Logger
	private Log logger;
			
	private long printerCacheTimeout = 120000L;
	private long printerCachePurgePollInternal = 30000L;
	
	private long checkPrinterAvaliableTimeout = 4000L;
	private long checkPrinterHangTimeout = 15000L;
	
	private int printerThreadLimit = 2;
	
	private long connectionTimeout = 10000L;
	private long socketTimeout = 10000L;
		
	private int printerPort = 61616;
	private int securePrinterPort = 0;	
	private AmqSslContext amqSslContext = null;
	
	private int retryCount = 10;
	private long retryDelayTime = 2000L;

	@In
	private TimerManager timerManager;
		
	@In
	private WorkManager workManager;

	private static TimeoutMap<String, MessageProducerHolder> messageProducerTimeoutMap = null;
	
	private long getPrinterCacheTimeout() {
		return printerCacheTimeout;
	}

	public void setPrinterCacheTimeout(long printerCacheTimeout) {
		this.printerCacheTimeout = printerCacheTimeout;
	}

	private long getPrinterCachePurgePollInternal() {
		return printerCachePurgePollInternal;
	}

	public void setPrinterCachePurgePollInternal(long printerCachePurgePollInternal) {
		this.printerCachePurgePollInternal = printerCachePurgePollInternal;
	}
	
	public long getCheckPrinterAvaliableTimeout() {
		return checkPrinterAvaliableTimeout;
	}

	public void setCheckPrinterAvaliableTimeout(long checkPrinterAvaliableTimeout) {
		this.checkPrinterAvaliableTimeout = checkPrinterAvaliableTimeout;
	}
	
	public long getCheckPrinterHangTimeout() {
		return checkPrinterHangTimeout;
	}

	public void setCheckPrinterHangTimeout(long checkPrinterHangTimeout) {
		this.checkPrinterHangTimeout = checkPrinterHangTimeout;
	}

	public int getPrinterThreadLimit() {
		return printerThreadLimit;
	}

	public void setPrinterThreadLimit(int printerThreadLimit) {
		this.printerThreadLimit = printerThreadLimit;
	}

	public long getConnectionTimeout() {
		return connectionTimeout;
	}

	public void setConnectionTimeout(long connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	public long getSocketTimeout() {
		return socketTimeout;
	}

	public void setSocketTimeout(long socketTimeout) {
		this.socketTimeout = socketTimeout;
	}

	public int getPrinterPort() {
		return printerPort;
	}

	public void setPrinterPort(int printerPort) {
		this.printerPort = printerPort;
	}
	
	public int getSecurePrinterPort() {
		return securePrinterPort;
	}

	public void setSecurePrinterPort(int securePrinterPort) {
		this.securePrinterPort = securePrinterPort;		
	}

	public AmqSslContext getAmqSslContext() {
		return amqSslContext;
	}

	public void setAmqSslContext(AmqSslContext amqSslContext) {
		this.amqSslContext = amqSslContext;
	}

	@Create
	public void init() {
		synchronized (SilentPrintSender.class) {
			if (messageProducerTimeoutMap == null) {
				
				logger.info("Create TimeoutMap for MessageProducer timeout:#0 purgePollInterval:#1", 
						this.getPrinterCacheTimeout(), 
						this.getPrinterCachePurgePollInternal());
						
				messageProducerTimeoutMap = new MessageProducerTimeoutMap(
						logger, workManager, timerManager, this.getPrinterCachePurgePollInternal());
			}
		}
	}	

	private String getPrinterUrl(String printerProtocol, String printerHostName) {
		return printerProtocol + "://" + printerHostName + ":" + this.getPrinterPortByProtocol(printerProtocol) + 
			"?connectionTimeout=" + this.getConnectionTimeout() + 
			"&soTimeout=" + this.getSocketTimeout();
	}
	
	private int getPrinterPortByProtocol(String protocol) {
		return (protocol.equals(SECURE_PROTOCOL) ? securePrinterPort : printerPort);
	}
						
	public boolean sendToPrinter(final PrintJobInfo printJobInfo) {
				
		String hostName = printJobInfo.getPrinterSelect().getResolvedPrinterWorkstation().getHostName();		

		logger.info("sendToPrinter [#0] info #1, type #2, size #3, printer #4, copies #5, reverse #6, lot #7/#8/#9", 
				hostName,
				printJobInfo.getPrintInfo(),
				(printJobInfo.getPrintDocType() != null ? printJobInfo.getPrintDocType() : printJobInfo.getPrinterSelect().getPrintDocType()),
				printJobInfo.getData().length, 
				printJobInfo.getPrinterName(),
				printJobInfo.getPrinterSelect().getCopies(), 
				printJobInfo.getPrinterSelect().isReverse(),
				printJobInfo.getLotNum(),
				printJobInfo.getCurrentItem(),
				printJobInfo.getTotalItem());
		
		
		Object lock = null;
		synchronized (hostNameMap) {
			lock = hostNameMap.get(hostName);
			if (lock == null) {
				lock = new Object();
				hostNameMap.put(hostName, lock);
			}
		}

		MessageProducerHolder messageProducerHolder = null;	
		synchronized (lock) {
			messageProducerHolder = createSilentPrintMessageProducer(hostName);
		}

		final String errorString = "unable to print [#0] url #1";	
		final String printerStatString = "printer statistic [#0] #1";
		
		if (messageProducerHolder != null) {
			
			logger.info(printerStatString, hostName, messageProducerHolder.getInfo());

			String printerUrl = messageProducerHolder.getPrinterUrl();
			boolean removeHolderImmediately = false;
			Long startTime = null;				
			try {								
				
				int retry = retryCount;
				do {
					try {
						startTime = messageProducerHolder.start();
						break;
					} catch (PrinterExcessThreadLimitException e) {
						if (retry-- > 0) {
							logger.info("printer busy [#0], retry after #1ms, retryCount:#2/#3", 
									hostName, retryDelayTime, retryCount-retry, retryCount);
							try {
								Thread.sleep(retryDelayTime);
							} catch (InterruptedException e1) {
							}
						} else {
							throw e;
						}
					}
				} while (true);
				
				logger.info("sendToPrinter [#0] url #1", hostName, printerUrl);
				long startSendTime = System.currentTimeMillis();
				
				messageProducerHolder.getMessageProducer().send(new MessageCreator() {
					public Message createMessage(Session session) throws JMSException {			
						BytesMessage msg = session.createBytesMessage();
						msg.setIntProperty("PRINT_COPIES", printJobInfo.getPrinterSelect().getCopies());
						msg.setBooleanProperty("PRINT_REVERSE", printJobInfo.getPrinterSelect().isReverse());
						msg.setStringProperty("PRINTER_NAME", printJobInfo.getPrinterName());
						msg.setStringProperty("PRINT_INFO", printJobInfo.getPrintInfo());
						if (printJobInfo.getPrintDocType() != null) {
							msg.setStringProperty("PRINT_DOC_TYPE", printJobInfo.getPrintDocType());
						}
						if (printJobInfo.getLotNum() != null) {
							msg.setStringProperty("LOT_NUM", printJobInfo.getLotNum());
							msg.setIntProperty("TOTAL_ITEM", printJobInfo.getTotalItem());
							msg.setIntProperty("CURRENT_ITEM", printJobInfo.getCurrentItem());
						}
						msg.writeBytes(printJobInfo.getData());
						return msg;
					}
				});

				logger.info("sendToPrinter [#0] url #1 successfully (#2ms)", hostName, printerUrl, (System.currentTimeMillis() - startSendTime));
				
				return true;
			} catch (JMSException e) {
				logger.error(errorString, e, hostName, printerUrl);
				removeHolderImmediately = true;
			} catch (NamingException e) {
				logger.error(errorString, e, hostName, printerUrl);
				removeHolderImmediately = true;
			} catch (PrintingException e) {
				logger.error(errorString + ", printer exception detected #2", e, hostName, printerUrl, messageProducerHolder.getInfo());
			} finally {
				if (startTime != null && !messageProducerHolder.end(startTime)) {
					logger.warn(printerStatString + ", fail to mark end", hostName, messageProducerHolder.getInfo());
				}
			}

			if (removeHolderImmediately) {
				messageProducerTimeoutMap.remove(hostName);
			}
		}
		return false;
	}
	
	private MessageProducerHolder createSilentPrintMessageProducer(String hostName) {
			
		MessageProducerHolder holder = null;
		if (securePrinterPort > 0 && amqSslContext != null && amqSslContext.isCreateSuccess()) {
			SslContext.setCurrentSslContext(amqSslContext);
			holder = createSilentPrintMessageProducer(SECURE_PROTOCOL, hostName);
		}
		if (holder == null) {
			holder = createSilentPrintMessageProducer(PROTOCOL, hostName);
		}		
		return holder;
	}

	private MessageProducerHolder createSilentPrintMessageProducer(String protocol, String hostName) {
				
		MessageProducerHolder holder = messageProducerTimeoutMap.get(hostName);			
		if (holder != null) {
			logger.debug("MessageProducer [#0] found",hostName);
			return holder;
		} else {
			if (checkPrinterAvaliable(protocol, hostName)) {				
				
				String printerUrl = this.getPrinterUrl(protocol, hostName);

				Context context = Contexts.getEventContext();
				context.set(PRINTER_URL, printerUrl);
				MessageProducer messageProducer = (MessageProducer) Component.getInstance(SILENT_PRINT_MESSAGE_PRODUCER, ScopeType.EVENT);
				context.remove(PRINTER_URL);
				context.remove(SILENT_PRINT_MESSAGE_PRODUCER);

				holder = new MessageProducerHolder(
						messageProducer, printerUrl, this.getCheckPrinterHangTimeout(), this.getPrinterThreadLimit());
				
				messageProducerTimeoutMap.put(hostName, holder, this.getPrinterCacheTimeout());
				logger.debug("MessageProducer [#0] cached",hostName);			
				return holder;
			} else {
				return null;
			}
		}
	}	

	private boolean checkPrinterAvaliable(String protocol, String hostName) 
	{
		int port = getPrinterPortByProtocol(protocol);
		try {
			InetAddress ia = InetAddress.getByName(hostName);
			try {
				Socket socket = new Socket();
				socket.connect(new InetSocketAddress(ia.getHostAddress(), port), (int) this.getCheckPrinterAvaliableTimeout());
				socket.close();			
				return true;
			} catch (IOException e) {
				logger.warn("printer station #0/#1/#2:#3 not reachable!", hostName, ia.getHostAddress(), ia.getCanonicalHostName(), port);
			}		
		} catch (UnknownHostException e) {
			logger.warn("printer station #0:#1 not found!", hostName, port);
		}
		return false;
	}
		
	private static class MessageProducerHolder {
		
		private MessageProducer messageProducer;
		private String printerUrl;
		private long checkPrinterHangTimeout;
		private int printerThreadLimit;
		
		private Set<Long> startTimeSet = new TreeSet<Long>();
		private int finishedCount = 0;

		public MessageProducerHolder(MessageProducer messageProducer, String printerUrl, 
				long checkPrinterHangTimeout, int printerThreadLimit) {
			super();
			this.messageProducer = messageProducer;
			this.printerUrl = printerUrl;
			this.checkPrinterHangTimeout = checkPrinterHangTimeout;
			this.printerThreadLimit = printerThreadLimit;
		}
		
		public MessageProducer getMessageProducer() {
			return messageProducer;
		}
		
		public String getPrinterUrl() {
			return printerUrl;
		}

		public synchronized Long start() throws PrintingException {
			shouldStart();			
			Long startTime;
			do {
				startTime = Long.valueOf(System.currentTimeMillis());
			} while (startTimeSet.contains(startTime));			
			startTimeSet.add(startTime);
			return startTime;
		}

		public synchronized boolean end(Long startTime) {	
			if (startTimeSet.remove(startTime)) {
				finishedCount++;
				return true;
			} else {
				return false;
			}
		}
		
		public synchronized int getPendingCount() {
			return startTimeSet.size(); 
		}
		
		private void shouldStart() throws PrintingException {
			int count = startTimeSet.size();
			if (count > 0) {
				Long startTime = startTimeSet.iterator().next();
				if (!(startTime.longValue() > System.currentTimeMillis() - checkPrinterHangTimeout)) {
					throw new PrinterHangException();
				}
				if (count >= printerThreadLimit) {
					throw new PrinterExcessThreadLimitException();
				}
			}
		}
		
		public synchronized String getInfo() {
			StringBuilder sb = new StringBuilder();
			int size = startTimeSet.size();
			sb.append("pending:");
			sb.append(size);
			sb.append(", finished:");
			sb.append(finishedCount);
			if (size > 0) {
				sb.append(", details:");
				sb.append(startTimeSet);
			}
			return sb.toString();
		}
	}
	
	public static class MessageProducerTimeoutMap extends DefaultTimeoutMap<String, MessageProducerHolder> {

		private WorkManager workManager;
		private Log logger;
		
		public MessageProducerTimeoutMap(Log logger, WorkManager workManager, TimerManager executor,
				long requestMapPollTimeMillis) {
			super(executor, requestMapPollTimeMillis);
			this.logger = logger;
			this.workManager = workManager;
		}
		
		@Override
		protected boolean isValidForEviction(
				TimeoutMapEntry<String, MessageProducerHolder> entry) {
			boolean ret = entry.getValue().getPendingCount() == 0;
			if (!ret) logger.info("MessageProducer [#0] still alive #1",entry.getKey(), entry.getValue().getInfo());
			return ret;
		}

		@Override
		public void onEviction(final String key,
				final MessageProducerHolder holder) {							
			logger.info("MessageProducer [#0] about to destroy, active:#1",key, this.size());
			
			//ActiveMQConnection.close() may block the thread infinitely, run as async
			workManager.schedule(new Work(){
				@Override
				public boolean isDaemon() {
					return false;
				}
				@Override
				public void release() {
				}
				@Override
				public void run() {
					try {
						holder.getMessageProducer().destroy();
						logger.info("MessageProducer [#0] destroyed, active:#1",key, MessageProducerTimeoutMap.this.size());
					} catch (JMSException e) {
						logger.error("MessageProducer [#0] unable to destroy, active:#1", e, key, MessageProducerTimeoutMap.this.size());
					}									
				}});
		}		
	}
	
}
