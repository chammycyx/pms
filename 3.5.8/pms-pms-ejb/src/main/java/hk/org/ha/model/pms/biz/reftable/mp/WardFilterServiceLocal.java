package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.model.pms.persistence.reftable.WardFilter;

import java.util.List;

import javax.ejb.Local;

@Local
public interface WardFilterServiceLocal {

	void retrieveWardFilterList();
	
	void updateWardFilterList(List<WardFilter> newWardFilterList);
	
	void destroy();
	
}
