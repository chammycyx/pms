package hk.org.ha.model.pms.biz.vetting;

import static hk.org.ha.model.pms.prop.Prop.PATIENT_PAS_PATIENT_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.VETTING_DRUGONHAND_ONHANDSOURCETYPE;
import hk.org.ha.fmk.pms.remote.marshall.EclipseLinkXStreamMarshaller;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.moe.DrugOnHandException;
import hk.org.ha.model.pms.dms.vo.conversion.ConvertParam;
import hk.org.ha.model.pms.dms.vo.formatengine.FormatParam;
import hk.org.ha.model.pms.persistence.disp.MedOrderFm;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedOrderItemAlert;
import hk.org.ha.model.pms.udt.disp.OnHandSourceType;
import hk.org.ha.model.pms.vo.rx.RxItem;
import hk.org.ha.model.pms.vo.security.PropMap;
import hk.org.ha.model.pms.vo.vetting.FmIndication;
import hk.org.ha.model.pms.vo.vetting.OnHandProfile;
import hk.org.ha.model.pms.vo.vetting.OnHandProfileCriteria;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;
import hk.org.ha.service.pms.asa.interfaces.moe.MoeServiceJmsRemote;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;
import org.joda.time.DateMidnight;

@AutoCreate 
@Stateful
@Scope(ScopeType.SESSION)
@Name("drugOnHandListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DrugOnHandListServiceBean implements DrugOnHandListServiceLocal {
	
	private static final String JAXB_CONTEXT_RX = "hk.org.ha.model.pms.vo.rx";
	
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private FmIndicationManagerLocal fmIndicationManager; 
	
	@In
	private MoeServiceJmsRemote moeServiceProxy;
	
	@Out(required = false)
	private List<OnHandProfile> onHandProfileList; 
	
	@Out(required = false)
	private OnHandSourceType onHandProfileListSourceType;
	
	@In
	private PropMap propMap;
	
	private final static String UNCHECKED = "unchecked";
	
	
	@SuppressWarnings(UNCHECKED)
	public void retrieveDrugOnHandProfileList(OnHandProfileCriteria onHandProfileCriteria) throws DrugOnHandException {

		if ( OnHandSourceType.Corporate.getDataValue().equals(propMap.getValue(VETTING_DRUGONHAND_ONHANDSOURCETYPE.getName())) ) {
			onHandProfileCriteria.setCorpOnHandEnabled(Boolean.TRUE);
		} else {
			onHandProfileCriteria.setCorpOnHandEnabled(Boolean.FALSE);
		}
		onHandProfileCriteria.setOrderDate((new DateMidnight()).toDateTime().toDate()); // note: time is truncated from order date
		onHandProfileCriteria.setPasPatientEnabled(propMap.getValueAsBoolean(PATIENT_PAS_PATIENT_ENABLED.getName()));

		Pair<OnHandSourceType, List<OnHandProfile>> result = moeServiceProxy.retrieveDrugOnHandProfileWithMpRx(onHandProfileCriteria);
		onHandProfileListSourceType = result.getFirst();
		onHandProfileList = result.getSecond();
		
		try {
			// obtain FM list for all order number in on hand list
			Map<String, List<MedOrderFm>> onHandMedOrderFmMap = new HashMap<String, List<MedOrderFm>>(); 
			for (OnHandProfile onHandProfile : onHandProfileList) {
				String orderNum = StringUtils.rightPad(onHandProfile.getPatHospCode(), 3) + onHandProfile.getOrderNum().toString();
				
				if ( ! onHandMedOrderFmMap.containsKey(orderNum)) {
					List<MedOrderFm> onHandMedOrderFmList = (List<MedOrderFm>) em.createQuery(
							"select o.medOrderFmList from MedOrder o" + // 20120225 index check : MedOrder.orderNum : I_MED_ORDER_05
							" where o.orderNum = :orderNum")
							.setParameter("orderNum", orderNum)
							.getResultList();
					
					onHandMedOrderFmMap.put(orderNum, onHandMedOrderFmList);
				}
			}
			
			for (OnHandProfile onHandProfile : onHandProfileList) {
				logger.debug(new EclipseLinkXStreamMarshaller().toXML(onHandProfile));
	
				MedOrderItem onHandMedOrderItem = onHandProfile.getMedOrderItem();
				
				// init on hand profile
				onHandMedOrderItem.preSave();
				onHandMedOrderItem.setCapdRxDrug(null);
				onHandMedOrderItem.setRxDrug(null);
				
				onHandMedOrderItem.postLoad();

				// load strengthCompulsory for retrieving FM indicator
				onHandMedOrderItem.loadDmInfo(onHandProfile.getMpDischargeFlag());
				
				// determine whether to show FM indicator by FM list in PMS DB
				String orderNum = StringUtils.rightPad(onHandProfile.getPatHospCode(), 3) + onHandProfile.getOrderNum().toString();
				List<FmIndication> fmIndicationList = fmIndicationManager.retrieveLocalFmIndicationListForDrugOnHand(onHandMedOrderItem, onHandMedOrderFmMap.get(orderNum));
				if (fmIndicationList != null && ! fmIndicationList.isEmpty()) {
					onHandMedOrderItem.setFmIndFlag(true);
				}
				
				// get drug and regimen desc by formatting engine for discharge order
				if (onHandProfile.getMpDischargeFlag()) {
					// prepare on hand specific data for formatting engine
					RxItem rawRxItem = onHandMedOrderItem.getRxItem();
					rawRxItem.setRemarkStatus(onHandProfile.getRemarkOrderStatus());
					rawRxItem.setRemarkItemStatus(onHandMedOrderItem.getRemarkItemStatus());
					rawRxItem.setCommentText(onHandMedOrderItem.getCommentText());
					rawRxItem.setCommentUser(onHandMedOrderItem.getCommentUser());
					rawRxItem.setCommentDate(onHandMedOrderItem.getCommentDate());
					
					// prpare order level data for formatting engine
					rawRxItem.setPatHospCode(onHandMedOrderItem.getMedOrder().getPatHospCode());
					rawRxItem.setRefNum(onHandMedOrderItem.getMedOrder().getRefNum());
					rawRxItem.setDocType(onHandMedOrderItem.getMedOrder().getDocType());
					rawRxItem.setCaseNum(onHandMedOrderItem.getMedOrder().getMedCase().getCaseNum());
					rawRxItem.setPasSpecCode(onHandMedOrderItem.getMedOrder().getMedCase().getPasSpecCode());
					rawRxItem.setPasSubSpecCode(onHandMedOrderItem.getMedOrder().getMedCase().getPasSubSpecCode());
					
					// generate drug order html by formatting engine for discharge order display
					JaxbWrapper<RxItem> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
					String rxItemXml = rxJaxbWrapper.marshall(rawRxItem);
					
					ConvertParam convertParam = new ConvertParam();
					convertParam.setOrderType("D");
					FormatParam formatParam = new FormatParam();
					formatParam.setShowDrugOnHandDetails(true);

					List<String> rxItemXmlList = dmsPmsServiceProxy.formatDrugOrderXml( new ArrayList<String>(Arrays.asList(rxItemXml)), convertParam, formatParam);
					RxItem formattedRxItem = rxJaxbWrapper.unmarshall(rxItemXmlList.get(0));
					if (formattedRxItem == null) {
						throw new UnsupportedOperationException("Cannot unmarshall updated RxItem after calling DMS formatting engine, orderNum = " + onHandProfile.getOrderNum() + ", itemNum = " + onHandMedOrderItem.getItemNum());
					}
					formattedRxItem.buildTlf();
					onHandMedOrderItem.setRxItem(formattedRxItem);
					
					// need to load DM info again after calling formatting engine, because fields obtained by loadDmInfo is transient and is lost after calling formatting engine
					onHandMedOrderItem.loadDmInfo(onHandProfile.getMpDischargeFlag());
				}
				
				onHandMedOrderItem.clearPharmOrderItemList();
				onHandMedOrderItem.setMedOrderItemAlertList(new ArrayList<MedOrderItemAlert>());
	
				onHandMedOrderItem.getMedOrder().setMedOrderItemList(new ArrayList<MedOrderItem>());
				onHandMedOrderItem.getMedOrder().setPatient(null);
				onHandMedOrderItem.getMedOrder().setWorkstore(null);
				onHandMedOrderItem.getMedOrder().setMedCase(null);
				onHandMedOrderItem.getMedOrder().setMedOrderFmList(new ArrayList<MedOrderFm>());
				
				onHandMedOrderItem.getMedOrder().getTicket().setWorkstore(null);				
			}
		} catch (Exception e) {
			throw new DrugOnHandException(e);
		}
	}
	
	@Remove
	public void destroy() {
		if (onHandProfileList != null) {
			onHandProfileList = null;
		}
		
		if ( onHandProfileListSourceType != null ) {
			onHandProfileListSourceType = null;
		}
	}
}
