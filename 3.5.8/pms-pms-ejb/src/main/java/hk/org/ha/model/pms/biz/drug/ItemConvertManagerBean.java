package hk.org.ha.model.pms.biz.drug;

import static hk.org.ha.model.pms.prop.Prop.VETTING_DRUG_FORM_ALLOWHALFTABLET;
import static hk.org.ha.model.pms.prop.Prop.VETTING_REGIMEN_PRN_DURATION;
import hk.org.ha.fmk.pms.remote.marshall.EclipseLinkXStreamMarshaller;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.vetting.OrderEditManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmFormCacherInf;
import hk.org.ha.model.pms.corp.cache.DmRouteCacherInf;
import hk.org.ha.model.pms.corp.cache.DmWarningCacherInf;
import hk.org.ha.model.pms.dms.biz.formatengine.FormatHints;
import hk.org.ha.model.pms.dms.biz.formatengine.ParsedOrderLineFormatterBase;
import hk.org.ha.model.pms.dms.biz.formatengine.ParsedOrderLineFormatterFactory;
import hk.org.ha.model.pms.dms.persistence.DmForm;
import hk.org.ha.model.pms.dms.persistence.DmRoute;
import hk.org.ha.model.pms.dms.udt.formatengine.FormatType;
import hk.org.ha.model.pms.dms.vo.conversion.ConvertParam;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.phs.CapdSupplierItem;
import hk.org.ha.model.pms.persistence.reftable.LocalWarning;
import hk.org.ha.model.pms.udt.RecordStatus;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.udt.vetting.PrnPercentage;
import hk.org.ha.model.pms.vo.drug.DmDrugLite;
import hk.org.ha.model.pms.vo.drug.DmFormLite;
import hk.org.ha.model.pms.vo.drug.DmWarningLite;
import hk.org.ha.model.pms.vo.rx.CapdItem;
import hk.org.ha.model.pms.vo.rx.CapdRxDrug;
import hk.org.ha.model.pms.vo.rx.DhRxDrug;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.IvAdditive;
import hk.org.ha.model.pms.vo.rx.IvFluid;
import hk.org.ha.model.pms.vo.rx.IvRxDrug;
import hk.org.ha.model.pms.vo.rx.OralRxDrug;
import hk.org.ha.model.pms.vo.rx.PharmDrug;
import hk.org.ha.model.pms.vo.rx.Regimen;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;
import hk.org.ha.model.pms.vo.security.PropMap;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;
import org.springframework.beans.BeanUtils;

@AutoCreate
@Stateless
@Name("itemConvertManager")
@MeasureCalls
public class ItemConvertManagerBean implements ItemConvertManagerLocal {

	private static final String JAXB_CONTEXT_RX = "hk.org.ha.model.pms.vo.rx";
	
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
		
	@In
	private Workstore workstore;

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private DmFormCacherInf dmFormCacher;
	
	@In
	private DmRouteCacherInf dmRouteCacher;
	
	@In
	private DmWarningCacherInf dmWarningCacher;
	
	@In
	private PropMap prnRouteDescMap;
		
	@In
	private OrderEditManagerLocal orderEditManager;
	
	public void convertMedOrder(PharmOrder pharmOrder) {

		List<RxItem> rxItemList = convertRxItemList(
				constructRxItemListFromMedOrderItemList(pharmOrder.getMedOrder().getMedOrderItemList()),
				constructConvertParam(pharmOrder.getMedOrder().isMpDischarge())
			);

		for (RxItem rxItem : rxItemList) {

			MedOrderItem medOrderItem = pharmOrder.getMedOrder().getMedOrderItemByItemNum(rxItem.getItemNum());
			
			List<PharmOrderItem> poiList = constructPharmOrderItemListFromRxItem(rxItem);
			medOrderItem.setPharmOrderItemList(poiList);

			if( medOrderItem.getRxItem() != null ){
				medOrderItem.getRxItem().setMoDoseAlertFlag(rxItem.getMoDoseAlertFlag());
			}
			
			for (PharmOrderItem poi : poiList) {
				pharmOrder.addPharmOrderItem(poi);
				poi.setMedOrderItem(medOrderItem);

				poi.setItemNum(pharmOrder.getAndIncreaseMaxItemNum());
				poi.setOrgItemNum(pharmOrder.getAndIncreaseMaxOrgItemNum());
				
				if ( checkAmendIssueQtyToZero(rxItem, poi) ) {
					poi.setIssueQty(BigDecimal.ZERO);
					poi.setCalQty(BigDecimal.ZERO);
				}
			}
			
			if (pharmOrder.getMedOrder().isMpDischarge()) {
				rxItem.clearPharmDrugList();
				medOrderItem.setRxItem(rxItem);
				medOrderItem.preSave();
				medOrderItem.loadDmInfo(true);//true for loading MP site list
				medOrderItem.recalTotalDurationInDay();
			}
			else if (rxItem.isDhRxDrug()) {
				DhRxDrug dhRxDrug = rxItem.asDhRxDrug();
				medOrderItem.getDhRxDrug().setMapFlag(dhRxDrug.getMapFlag());
				medOrderItem.getDhRxDrug().setMapStatus(dhRxDrug.getMapStatus());
				dhRxDrug.clearPharmDrugList();
				RxDrug rxDrug = new OralRxDrug();
				try {
					PropertyUtils.copyProperties(rxDrug, dhRxDrug);
				}
				catch (Exception e) {
					throw new RuntimeException(e);
				}
				medOrderItem.setRxItem(rxDrug);
				medOrderItem.preSave();
				medOrderItem.loadDmInfo();
			}
		}
	}
	
	public List<PharmOrderItem> convertMedOrderItem(PharmOrder pharmOrder, MedOrderItem medOrderItem) {
		
		//Change the OralRxDrug to InjectionRxDrug if route/site is parenteral
		if (pharmOrder.getMedOrder().isMpDischarge()) {
			if (!medOrderItem.isInfusion()) {
				orderEditManager.reconstructRxItemForDischarge(medOrderItem);
			}
		}
		
		RxItem rxItem = convertRxItem(
				constructRxItemFromMedOrderItem(medOrderItem),
				constructConvertParam(pharmOrder.getMedOrder().isMpDischarge())
			);

		List<PharmOrderItem> poiList = constructPharmOrderItemListFromRxItem(rxItem);
		if (poiList.isEmpty()) {
			poiList.add(new PharmOrderItem());
		}
		medOrderItem.setPharmOrderItemList(poiList);
		if( medOrderItem.getRxItem() != null ){
			medOrderItem.getRxItem().setMoDoseAlertFlag(rxItem.getMoDoseAlertFlag());
		}
		for (PharmOrderItem poi : poiList) {
			poi.setMedOrderItem(medOrderItem);

			poi.setItemNum(pharmOrder.getAndIncreaseMaxItemNum());
			poi.setOrgItemNum(pharmOrder.getAndIncreaseMaxOrgItemNum());
			poi.setSystemDeducedFlag(Boolean.TRUE);
			
			if ( checkAmendIssueQtyToZero(rxItem, poi) ) {
				poi.setIssueQty(BigDecimal.ZERO);
				poi.setCalQty(BigDecimal.ZERO);
			}
		}
		
		if (pharmOrder.getMedOrder().isMpDischarge()) {
			rxItem.clearPharmDrugList();
			medOrderItem.setRxItem(rxItem);
			medOrderItem.preSave();
			medOrderItem.loadDmInfo(true);
		}
		else if (rxItem.isDhRxDrug()) {
			DhRxDrug dhRxDrug = rxItem.asDhRxDrug();
			medOrderItem.getDhRxDrug().setMapFlag(dhRxDrug.getMapFlag());
			medOrderItem.getDhRxDrug().setMapStatus(dhRxDrug.getMapStatus());
			dhRxDrug.clearPharmDrugList();
			RxDrug rxDrug = new OralRxDrug();
			try {
				PropertyUtils.copyProperties(rxDrug, dhRxDrug);
			}
			catch (Exception e) {
				throw new RuntimeException(e);
			}
			medOrderItem.setRxItem(rxDrug);
			medOrderItem.preSave();
			medOrderItem.loadDmInfo();
		}

		return poiList;
	}
	
	public List<MedOrderItem> convertChest(List<PharmOrderItem> poiList) {

		List<MedOrderItem> moiList = new ArrayList<MedOrderItem>();
		
		ConvertParam convertParam = constructConvertParam();
		convertParam.setIsChest(Boolean.TRUE);
		List<RxItem> outRxItemList = convertRxItemList(
				constructRxItemListFromPharmOrderItemList(poiList),
				convertParam
			);

		for (int i = 0; i < outRxItemList.size(); i++) {			
			
			RxItem rxItem = outRxItemList.get(i);
			PharmOrderItem pharmOrderItem = poiList.get(i);
			
			RxDrug rxDrug = (RxDrug) rxItem;

			updatePharmOrderItemByRxDrug(pharmOrderItem, rxDrug);
			pharmOrderItem.setChestFlag(Boolean.TRUE);

			MedOrderItem medOrderItem = constructMedOrderItemFromRxDrug(rxDrug);
			medOrderItem.setPharmOrderItemList(Arrays.asList(pharmOrderItem));
			medOrderItem.clearMedOrderItemAlertList();

			pharmOrderItem.setMedOrderItem(medOrderItem);
			moiList.add(medOrderItem);
		}
		
		return moiList;
	}
	
	public PharmDrug recalPharmOrderItem(PharmOrderItem inPharmOrderItem) {

		RxItem inRxItem = constructRxItemFromMedOrderItem(inPharmOrderItem.getMedOrderItem());
		PharmDrug inPharmDrug = constructPharmDrugFromPharmOrderItem(inPharmOrderItem);
		RxDrug inRxDrug;
		if (inRxItem.isIvRxDrug()) {
			OralRxDrug rxDrug = new OralRxDrug();
			rxDrug.setActionStatus(inPharmOrderItem.getActionStatus());
			rxDrug.setPharmDrugList(Arrays.asList(inPharmDrug));
			inRxDrug = rxDrug;
		} else {			
			inRxDrug = (RxDrug)inRxItem;
			inRxDrug.setPharmDrugList(Arrays.asList(inPharmDrug));
		}
		
		PharmOrder pharmOrder = (PharmOrder)Contexts.getSessionContext().get("pharmOrder");
		ConvertParam convertParam = constructConvertParam();		
		convertParam.setNoOfPharmDrug(inPharmOrderItem.getMedOrderItem().getNumOfSystemDeducedItem());
		convertParam.setIsReCalQty(Boolean.TRUE);// indicate re-calculate of the pharmOrderItem
		if (pharmOrder.getMedOrder().isMpDischarge()) {
			convertParam.setOrderType("D");
		}
		return ((RxDrug)convertRxItem(inRxDrug, convertParam)).getPharmDrugList().get(0);
	}
	
	public List<BigDecimal> calSubTotalDosage(RxItem rxItem, Integer doseGroupIndex, Integer doseIndex) {
		JaxbWrapper<RxItem> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
		String rxItemXml = rxJaxbWrapper.marshall(rxItem);
		return (List<BigDecimal>) dmsPmsServiceProxy.calSubTotalDosage(rxItemXml, doseGroupIndex, doseIndex);
	}
	
	private void updateIssueQty(PharmOrderItem pharmOrderItem, BigDecimal calQty) {
		
		if (calQty != null && calQty.compareTo(calQty.setScale(0,RoundingMode.DOWN)) == 0) {
			pharmOrderItem.setIssueQty(calQty);
		}
		else {
			pharmOrderItem.setIssueQty(null);
		}
		pharmOrderItem.setSfiQty(0);
	}

	private void updatePharmOrderItemByRxDrug(PharmOrderItem orgPoi, RxDrug rxDrug) {
		
		PharmOrderItem newPoi = constructPharmOrderItemFromRxDrug(rxDrug.getPharmDrugList().get(0), rxDrug);
		
		BeanUtils.copyProperties(
				newPoi,
				orgPoi,
				new String[] {
						"id", "issueQty", "sfiQty", "itemNum", "orgItemNum",
						"pharmOrder", "medOrderItem", "refillScheduleItemList", "dispOrderItemList"
					}
			);
		
		if (newPoi.getIssueQty() != null) {
			orgPoi.setIssueQty(newPoi.getIssueQty());
		}
	}
	
	private void updateWarnCodeByItemCode(PharmOrderItem pharmOrderItem, String itemCode) {
	
		// retrieve warnCode
		@SuppressWarnings("unchecked")
		List<LocalWarning> localWarningList = em.createQuery(
				"select o from LocalWarning o" + // 20120215 index check : LocalWarning.workstoreGroup : FK_LOCAL_WARNING_01
				" where o.workstoreGroup = :workstoreGroup" +
				" and o.itemCode = :itemCode")
				.setParameter("workstoreGroup", workstore.getWorkstoreGroup())
				.setParameter("itemCode", itemCode)
				.getResultList();

		List<String> warnCodeList;
		if (localWarningList.isEmpty()) { // use default warning
			warnCodeList = new ArrayList<String>( Arrays.asList(new String[] {
					pharmOrderItem.getWarnCode1(),
					pharmOrderItem.getWarnCode2(),
					pharmOrderItem.getWarnCode3(),
					pharmOrderItem.getWarnCode4()
				}));
		}
		else { // use local warning
			warnCodeList = new ArrayList<String>( Arrays.asList(new String[] {null, null, null, null}));
			for (LocalWarning localWarning : localWarningList) {
				warnCodeList.set(localWarning.getSortSeq()-1, localWarning.getWarnCode());
			}
		}
		
		// retrieve dmWarning
		List<DmWarningLite> dmWarningLiteList = new ArrayList<DmWarningLite>();
		for (String warnCode : warnCodeList) {
			if (StringUtils.isBlank(warnCode)) {
				dmWarningLiteList.add(null);
			}
			else {
				dmWarningLiteList.add(DmWarningLite.objValueOf(dmWarningCacher.getDmWarningByWarnCode(warnCode)));
			}
		}
		
		// remove usageType = "O"
		List<Integer> removeIndexList = new ArrayList<Integer>();
		for (DmWarningLite dmWarningLite : dmWarningLiteList) {
			if (dmWarningLite != null && StringUtils.equals("O", dmWarningLite.getUsageType())) {
				removeIndexList.add(0, Integer.valueOf(dmWarningLiteList.indexOf(dmWarningLite)));
			}
		}
		for (Integer removeIndex : removeIndexList) {
			dmWarningLiteList.remove(removeIndex.intValue());
			warnCodeList.remove(removeIndex.intValue());
			dmWarningLiteList.add(null);
			warnCodeList.add(null);
		}
		
		// set pharmOrderItem
		pharmOrderItem.setWarnCode1(warnCodeList.get(0));
		pharmOrderItem.setWarnCode2(warnCodeList.get(1));
		pharmOrderItem.setWarnCode3(warnCodeList.get(2));
		pharmOrderItem.setWarnCode4(warnCodeList.get(3));
		pharmOrderItem.setDmWarningLite1(dmWarningLiteList.get(0));
		pharmOrderItem.setDmWarningLite2(dmWarningLiteList.get(1));
		pharmOrderItem.setDmWarningLite3(dmWarningLiteList.get(2));
		pharmOrderItem.setDmWarningLite4(dmWarningLiteList.get(3));
	}

	private List<RxItem> constructRxItemListFromMedOrderItemList(List<MedOrderItem> medOrderItemList) {
		List<RxItem> rxItemList = new ArrayList<RxItem>();
		for (MedOrderItem medOrderItem : medOrderItemList) {
			rxItemList.add(constructRxItemFromMedOrderItem(medOrderItem));
		}
		return rxItemList;
	}
	
	private RxItem constructRxItemFromMedOrderItem(MedOrderItem medOrderItem) {
		if (medOrderItem.getDhRxDrug() != null) {
			return constructDhDrugFromMedOrderItem(medOrderItem);
		}
		else if (medOrderItem.getRxDrug() != null) {
			return constructRxDrugFromMedOrderItem(medOrderItem);
		}
		else if (medOrderItem.getCapdRxDrug() != null) {
			return constructCapdRxDrugFromMedOrderItem(medOrderItem);
		}
		else if (medOrderItem.getIvRxDrug() != null) {
			return constructIvRxDrugFromMedOrderItem(medOrderItem);
		}
		else {
			throw new RuntimeException("unknow rxItem");
		}
	}
	
	private DhRxDrug constructDhDrugFromMedOrderItem(MedOrderItem medOrderItem) {
		DhRxDrug dhRxDrug = medOrderItem.getDhRxDrug();
		dhRxDrug.setItemNum(medOrderItem.getItemNum());
		return dhRxDrug;
	}
	
	private RxDrug constructRxDrugFromMedOrderItem(MedOrderItem medOrderItem) {
		medOrderItem.getRxDrug().setItemNum(medOrderItem.getItemNum());
		medOrderItem.getRxDrug().setMoDoseAlertFlag(Boolean.FALSE);
		return medOrderItem.getRxDrug();
	}
	
	private CapdRxDrug constructCapdRxDrugFromMedOrderItem(MedOrderItem medOrderItem) {
		
		CapdRxDrug capdRxDrug = medOrderItem.getCapdRxDrug();
		capdRxDrug.setItemNum(medOrderItem.getItemNum());
		for (CapdItem capdItem : capdRxDrug.getCapdItemList()) {
			CapdSupplierItem capdSupplierItem = em.find(CapdSupplierItem.class, capdItem.getItemCode());
			if (capdSupplierItem != null && capdSupplierItem.getStatus() == RecordStatus.Active && capdSupplierItem.getSupplier().getStatus() == RecordStatus.Active) {
				capdItem.setSupplierCode(capdSupplierItem.getSupplier().getSupplierCode());
			}
		}

		return capdRxDrug;
	}
	
	private IvRxDrug constructIvRxDrugFromMedOrderItem(MedOrderItem medOrderItem) {
		IvRxDrug ivRxDrug = medOrderItem.getIvRxDrug();
		ivRxDrug.setItemNum(medOrderItem.getItemNum());
		return ivRxDrug;
	}
	
	private PharmDrug constructPharmDrugFromPharmOrderItem(PharmOrderItem poi) {
		
		PharmDrug pharmDrug = new PharmDrug();		
		BeanUtils.copyProperties(poi, pharmDrug, new String[]{"doseQty"});
		pharmDrug.setDrugType(poi.getDupChkDrugType());
		pharmDrug.setDisplayName(poi.getDupChkDisplayName());

		if (poi.getDoseQty() != null) {
			pharmDrug.setDoseQty(poi.getDoseQty());
		}
		
		return pharmDrug;
	}
	
	private RxItem convertRxItem(RxItem rxItem, ConvertParam param) {
		return convertRxItemList(Arrays.asList(rxItem), param).get(0);
	}
	
	private List<RxItem> convertRxItemList(List<RxItem> inRxItemList, ConvertParam param) {
		if (logger.isDebugEnabled()) {
			logger.debug("convertDrug input:\nconvertParam=#0\ninRxItemList=#1",
					(new EclipseLinkXStreamMarshaller()).toXML(param),
					(new EclipseLinkXStreamMarshaller()).toXML(inRxItemList)
				);
		}
		
		JaxbWrapper<RxItem> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);

		List<String> inRxItemXmlList = new ArrayList<String>();
		for (RxItem inRxItem : inRxItemList) {
			String inRxItemXml = rxJaxbWrapper.marshall(inRxItem);
			inRxItemXmlList.add(inRxItemXml);
		}

		List<String> outRxItemXmlList = dmsPmsServiceProxy.convertDrug(inRxItemXmlList, param);
		
		
		List<RxItem> outRxItemList = new ArrayList<RxItem>();
		for (String outRxItemXml : outRxItemXmlList) {
			RxItem outRxItem = rxJaxbWrapper.unmarshall(outRxItemXml);
			formatRxItem(outRxItem);
			outRxItemList.add(outRxItem);
			
			if (logger.isDebugEnabled()) {
				logger.debug("outRxItem=\n#0", (new EclipseLinkXStreamMarshaller()).toXML(outRxItem));
			}
		}
		return outRxItemList;
	}
	
	private void formatRxItem(RxItem rxItem) {
		ParsedOrderLineFormatterBase formatter =  ParsedOrderLineFormatterFactory.getParsedOrderLineFormatter(rxItem.getInterfaceVersion());
		formatter.setHint(FormatHints.FORMAT_TYPE, FormatType.TLF);
		formatter.setHint(FormatHints.LINE_WIDTH, 45);
		formatter.setHint(FormatHints.HIDE_STRIKE_THROUGH, true);
		
		rxItem.setFormattedDrugOrderTlf(formatter.format(rxItem.getDrugOrderXml()));
		
		if (rxItem.isRxDrug()) {
			RxDrug rxDrug = (RxDrug) rxItem;
			rxDrug.setFormattedDrugNameTlf(formatter.format(rxDrug.getDrugNameXml()));
		}
		else if (rxItem.isIvRxDrug()) {
			IvRxDrug ivRxDrug = (IvRxDrug) rxItem;
			ivRxDrug.setFormattedFluidNameTlf(formatter.format(ivRxDrug.getFluidNameXml()));
		}
	}

	private List<PharmOrderItem> constructPharmOrderItemListFromRxItem(RxItem rxItem) {
		
		if (rxItem.isRxDrug()) {
			return constructPharmOrderItemListFromRxDrug((RxDrug) rxItem);
		}
		else if (rxItem.isCapdRxDrug()) {
			return constructPharmOrderItemListFromCapdRxDrug((CapdRxDrug) rxItem);
		}
		else if (rxItem.isIvRxDrug()) {
			return constructPharmOrderItemListFromIvRxDrug((IvRxDrug) rxItem);
		}
		else {
			throw new RuntimeException("unknow rxItem class " + rxItem.getClass().getName());
		}
	}
	
	private List<PharmOrderItem> constructPharmOrderItemListFromRxDrug(RxDrug rxDrug) {
		List<PharmOrderItem> poiList = new ArrayList<PharmOrderItem>();
		for (PharmDrug pharmDrug : rxDrug.getPharmDrugList()) {
			poiList.add(constructPharmOrderItemFromRxDrug(pharmDrug, rxDrug));
		}
		return poiList;
	}
	
	private PharmOrderItem constructPharmOrderItemFromRxDrug(PharmDrug pharmDrug, RxDrug rxDrug) {
		return constructPharmOrderItemFromPharmDrug(pharmDrug, rxDrug.getActionStatus(), rxDrug.getFmStatus());
	}
	
	private MedOrderItem constructMedOrderItemFromRxDrug(RxDrug rxDrug) {

		MedOrderItem medOrderItem = new MedOrderItem(rxDrug);

		if (rxDrug.getRegimen() != null) {
			logger.debug("regimen=\n#0", (new EclipseLinkXStreamMarshaller()).toXML(medOrderItem.getRegimen()));
			medOrderItem.loadDmInfo();
			medOrderItem.getRegimen().calTotalDurationInDay();
		}

		return medOrderItem;
	}
	
	private PharmOrderItem constructPharmOrderItemFromPharmDrug(PharmDrug pharmDrug, ActionStatus actionStatus) {
		return constructPharmOrderItemFromPharmDrug(pharmDrug, actionStatus, FmStatus.GeneralDrug.getDataValue());
	}
	
	private PharmOrderItem constructPharmOrderItemFromPharmDrug(PharmDrug pharmDrug, ActionStatus actionStatus, String fmStatus) {
		
		PharmOrderItem pharmOrderItem = new PharmOrderItem();
		pharmOrderItem.setActionStatus(actionStatus);
		pharmOrderItem.setFmStatus(fmStatus);
		
		pharmOrderItem.clearDispOrderItemList();

		BeanUtils.copyProperties(pharmDrug, pharmOrderItem, new String[]{"doseQty"});
		pharmOrderItem.setDupChkDrugType(pharmDrug.getDrugType());
		pharmOrderItem.setDupChkDisplayName(pharmDrug.getDisplayName());
		pharmOrderItem.setOrgFormCode(pharmOrderItem.getFormCode());
		
		if (pharmDrug.getDoseQty() != null) {
			pharmOrderItem.setDoseQty(pharmDrug.getDoseQty());
		}
		
		pharmOrderItem.loadDmInfo();
		pharmOrderItem.getRegimen().calTotalDurationInDay();

		pharmOrderItem.setDmDrugLite(DmDrugLite.objValueOf(pharmOrderItem.getItemCode()));
		
		if (pharmOrderItem.getFormCode() != null) {
			pharmOrderItem.setDmFormLite(DmFormLite.objValueOf(pharmOrderItem.getFormCode()));
		}

		updateIssueQty(pharmOrderItem, pharmOrderItem.getCalQty());
		updateWarnCodeByItemCode(pharmOrderItem, pharmOrderItem.getItemCode());
		
		return pharmOrderItem;
	}
	
	private List<PharmOrderItem> constructPharmOrderItemListFromCapdRxDrug(CapdRxDrug capdRxDrug) {

		List<PharmOrderItem> poiList = new ArrayList<PharmOrderItem>();
		for (CapdItem capdItem : capdRxDrug.getCapdItemList()) 
		{
			for (PharmDrug pharmDrug : capdItem.getPharmDrugList()) 
			{				
				PharmOrderItem pharmOrderItem = constructPharmOrderItemFromCapdRxDrug(pharmDrug, capdRxDrug);				
				pharmOrderItem.setConnectSystem(capdItem.getConnectSystem());
				if (capdRxDrug.getActionStatus() != ActionStatus.DispByPharm) {
					pharmOrderItem.setIssueQty(BigDecimal.ZERO);
				}
				poiList.add(pharmOrderItem);
			}
		}
		return poiList;
	}
	
	private PharmOrderItem constructPharmOrderItemFromCapdRxDrug(PharmDrug pharmDrug, CapdRxDrug capdRxDrug) {
		return constructPharmOrderItemFromPharmDrug(pharmDrug, capdRxDrug.getActionStatus());
	}
	
	private List<PharmOrderItem> constructPharmOrderItemListFromIvRxDrug(IvRxDrug ivRxDrug) {
		List<PharmOrderItem> poiList = new ArrayList<PharmOrderItem>();
		for (IvAdditive ivAdditive : ivRxDrug.getIvAdditiveList()) {
			for (PharmDrug pharmDrug : ivAdditive.getPharmDrugList()) {
				poiList.add(constructPharmOrderItemFromPharmDrug(pharmDrug, ivAdditive.getActionStatus(), ivAdditive.getFmStatus()));
			}
		}
		for (IvFluid ivFluid : ivRxDrug.getIvFluidList()) {
			for (PharmDrug pharmDrug : ivFluid.getPharmDrugList()) {
				poiList.add(constructPharmOrderItemFromPharmDrug(pharmDrug, ivFluid.getActionStatus()));
			}
		}
		return poiList;
	}
	
	private List<RxItem> constructRxItemListFromPharmOrderItemList(List<PharmOrderItem> pharmOrderItemList) {
		List<RxItem> rxItemList = new ArrayList<RxItem>();
		for (PharmOrderItem pharmOrderItem : pharmOrderItemList) {
			rxItemList.add(constructRxDrugFromPharmOrderItem(pharmOrderItem));
		}
		return rxItemList;
	}
	
	private OralRxDrug constructRxDrugFromPharmOrderItem(PharmOrderItem pharmOrderItem) {
		OralRxDrug oralRxDrug = new OralRxDrug();
		oralRxDrug.setPharmDrugList(Arrays.asList(constructPharmDrugFromPharmOrderItem(pharmOrderItem)));
		return oralRxDrug;
	}
	
	private ConvertParam constructConvertParam() {
		return constructConvertParam(false);
	}
	
	private ConvertParam constructConvertParam(boolean isMpDischarge) {
		ConvertParam param = new ConvertParam();
		param.setDispHospCode(workstore.getHospCode());
		param.setDispWorkstore(workstore.getWorkstoreCode());
		param.setIsHalfTab(VETTING_DRUG_FORM_ALLOWHALFTABLET.get(Boolean.TRUE));
		param.setIsChest(Boolean.FALSE);
		param.setIsReCalQty(Boolean.FALSE);
		param.setOrderType(isMpDischarge ? "D" : "O");
		return param;
	}

	private <N extends Number> boolean isPositive(N value) {
		if (value == null) {
			return false;
		} else if (value.doubleValue() > 0) {
			return true;
		}
		return false;
	}
	
	private boolean checkAmendIssueQtyToZero(RxItem rxItem, PharmOrderItem poi) {
		if ( !rxItem.isDhRxDrug() ) {
			return false;
		}
		if ( rxItem.asDhRxDrug().getMapFlag() ) {
			return false;
		}
		if ( isPositive(poi.getMoIssueQty()) && !StringUtils.isBlank(poi.getMoBaseUnit()) ) {
			return false;
		}
		
		if (poi.getRegimen() == null) {
			return false;
		}
		if (poi.getRegimen().getDoseGroupList() == null || poi.getRegimen().getDoseGroupList().isEmpty()) {
			return false;
		}
		
		Dose firstDose = poi.getRegimen().getFirstDose();
		if ( isPositive(firstDose.getDispQty()) && !StringUtils.isBlank(firstDose.getBaseUnit()) ) {
			return false;
		}
		return true;
	}
	
	public List<String> formatDiscontinueMessage(List<MedOrderItem> medOrderItemList) {
		
		JaxbWrapper<RxItem> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
		
		List<String> rxItemXmlList = new ArrayList<String>();
		
		for (MedOrderItem moi : medOrderItemList) {
			rxItemXmlList.add(rxJaxbWrapper.marshall(moi.getRxItem()));
		}
		
		ConvertParam convertParam = new ConvertParam();
		convertParam.setOrderType("D");

		rxItemXmlList = dmsPmsServiceProxy.formatDrugOrderXml(rxItemXmlList, convertParam);

		ParsedOrderLineFormatterBase formatter = null;
		
		List<String> tlfList = new ArrayList<String>();
		for (String rxItemXml : rxItemXmlList) {
			RxItem rxItem = rxJaxbWrapper.unmarshall(rxItemXml);
			if(formatter==null)
			{
				formatter = ParsedOrderLineFormatterFactory.getParsedOrderLineFormatter(rxItem.getInterfaceVersion());
				formatter.setHint(FormatHints.FORMAT_TYPE, FormatType.TLF);
				formatter.setHint(FormatHints.LINE_WIDTH, 678);
				formatter.setHint(FormatHints.HIDE_STRIKE_THROUGH, true);
			}
			tlfList.add(formatter.format(rxItem.getDrugOrderXml()));
		}
		
		return tlfList;
	}

}
