package hk.org.ha.model.pms.udt;

import org.eclipse.persistence.sessions.Session;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DayOfWeek implements StringValuedEnum {

	Monday("1", "Monday"),
	Tuesday("2", "Tuesday"),
	Wednesday("3", "Wednesday"),
	Thursday("4", "Thursday"),
	Friday("5", "Friday"),
	Saturday("6", "Saturday"),
	Sunday("7", "Sunday");
	
    private final String dataValue;
    private final String displayValue;

    DayOfWeek(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    } 

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static DayOfWeek dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DayOfWeek.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<DayOfWeek> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<DayOfWeek> getEnumClass() {
    		return DayOfWeek.class;
    	}
		
		@Override
		public Object convertObjectValueToDataValue(Object objectValue, Session session) { 
			return Integer.valueOf((String) super.convertObjectValueToDataValue(objectValue, session));
		}
    }
}



