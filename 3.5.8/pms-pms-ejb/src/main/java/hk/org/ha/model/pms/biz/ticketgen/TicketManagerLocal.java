package hk.org.ha.model.pms.biz.ticketgen;

import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
import hk.org.ha.model.pms.udt.ticketgen.TicketType;

import java.util.Date;

import javax.ejb.Local;

@Local
public interface TicketManagerLocal {
	
	Ticket retrieveTicket(String ticketNum, Date ticketDate);
		
	Ticket generateTicket(Date ticketDate, TicketType ticketType, String customTicketNumText, String moeOrderNum);
	
	Pair<String, Ticket> validateTicket(Date ticketDate, String ticketNum, Ticket ticketIn, Long medOrderId, OneStopOrderType orderType, boolean oneStopCheck);
	
	boolean isFastQueueTicket(Date ticketDate, String ticketNum);
}
