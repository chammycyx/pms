package hk.org.ha.model.pms.biz.alert;

import static hk.org.ha.model.pms.prop.Prop.ALERT_MDS_G6PD_CODE;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.alert.AlertProfileException;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.vo.alert.AlertProfile;
import hk.org.ha.service.pms.asa.interfaces.alert.AlertServiceJmsRemote;

import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.security.Identity;

@AutoCreate
@Stateless
@Name("alertProfileManager")
@MeasureCalls
public class AlertProfileManagerBean implements AlertProfileManagerLocal {

	@In
	private AlertServiceJmsRemote alertServiceProxy;
	
	@In
	private Workstation workstation;

	@In
	private Identity identity;

	public AlertProfile retrieveAlertProfileByPharmOrder(PharmOrder pharmOrder) throws AlertProfileException {
		
		MedOrder medOrder = pharmOrder.getMedOrder();
		String caseNum = StringUtils.EMPTY;
		
		if (pharmOrder.getMedCase() != null) {
			caseNum = pharmOrder.getMedCase().getCaseNum();
		}

		AlertProfile alertProfile = alertServiceProxy.retrieveAlertProfile(
				pharmOrder, 
				caseNum, 
				medOrder.getPatHospCode(), 
				identity.getCredentials().getUsername(),
				workstation.getHostName(),
				ALERT_MDS_G6PD_CODE.get()
			);
		if (pharmOrder.getMedOrder().isDhOrder() && alertProfile != null) {
			alertProfile.setSteroidTag(null);
		}
		return alertProfile;
	}

	public AlertProfile retrieveAlertProfileByMedProfile(MedProfile medProfile) throws AlertProfileException {
		
		return alertServiceProxy.retrieveAlertProfile(
				medProfile.getPatient(),
				medProfile.getMedCase().getCaseNum(),
				medProfile.getPatHospCode(),
				identity.getCredentials().getUsername(),
				workstation.getHostName(),
				ALERT_MDS_G6PD_CODE.get()
			);
	}
	
	@Override
	public AlertProfile retrieveAlertProfileByMedOrder(MedOrder medOrder, Patient patient) throws AlertProfileException {
		
		return alertServiceProxy.retrieveAlertProfile(
				patient, 
				medOrder.getMedCase() == null ? StringUtils.EMPTY : medOrder.getMedCase().getCaseNum(), 
				medOrder.getPatHospCode(), 
				identity.getCredentials().getUsername(),
				workstation.getHostName(),
				ALERT_MDS_G6PD_CODE.get()
			);
	}
	
	public AlertProfile retrieveAlertProfile(String patHospCode, String caseNum, Patient patient) throws AlertProfileException {
		
		return alertServiceProxy.retrieveAlertProfile(
				patient,
				caseNum,
				patHospCode,
				identity.getCredentials().getUsername(),
				workstation.getHostName(),
				ALERT_MDS_G6PD_CODE.get()
			);
	}
}
