package hk.org.ha.model.pms.biz.enquiry;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmFmDrug;
import hk.org.ha.model.pms.dms.vo.FmDrug;
import hk.org.ha.model.pms.dms.vo.FmDrugCorpInd;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.udt.vetting.FmStatusOption;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("specialDrugEnqService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SpecialDrugEnqServiceBean implements SpecialDrugEnqServiceLocal {
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@Logger
	private Log logger;
	
	@Out(required = false)
	private DmFmDrug dmFmDrug;
	
	@In
	private Workstore workstore;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@Out(required = false)
	private List<FmDrugCorpInd> sDrugList;
	
	@Out(required = false)
	private List<FmDrugCorpInd> sfiList;
	
	@Out(required = false)
	private List<FmDrugCorpInd> safetyNetList;
	
	public void retrieveDmFmDrug(String itemCode){
		List<DmFmDrug> fmDrugList = dmsPmsServiceProxy.retrieveDmFmDrugByItemCode(workstore.getHospCode(), workstore.getWorkstoreCode(), itemCode);
		
		if (fmDrugList.size() == 1 && fmDrugList.get(0).getDmFmInstDrug() != null){
			dmFmDrug = fmDrugList.get(0);
			dmFmDrug.setDmDrug(null);
			DmDrug dmDrug = dmDrugCacher.getDmDrug(itemCode);
			dmFmDrug.setDmDrug(dmDrug);
			
			dmFmDrug.getDmDrug().getPmsFmStatus().setFmStatusDesc(FmStatus.dataValueOf(dmFmDrug.getDmDrug().getPmsFmStatus().getFmStatus()).getDisplayValue());
			
			if(dmDrug!=null){
				retrieveFmIndicator(itemCode, dmDrug.getPmsFmStatus().getFmStatus() );
			}
		}else {
			dmFmDrug = null;
		}
	}
	
	public void retrieveDmFmDrugByFullDrugDesc(String fullDrugDesc){
		List<DmFmDrug> fmDrugList = dmsPmsServiceProxy.retrieveDmFmDrugByFullDrugDesc(workstore.getHospCode(), workstore.getWorkstoreCode(), fullDrugDesc);
		
		if (fmDrugList.size() == 1){
			dmFmDrug = fmDrugList.get(0);
			dmFmDrug.setDmDrug(null);
			DmDrug dmDrug = dmDrugCacher.getDmDrug(dmFmDrug.getItemCode());
			dmFmDrug.setDmDrug(dmDrug);		
			
			if(dmDrug!=null){
				retrieveFmIndicator(dmFmDrug.getItemCode(), dmDrug.getPmsFmStatus().getFmStatus().toString());
			}
		}else {
			dmFmDrug = null;
		}
	}
	
	private void retrieveFmIndicator(String itemCode, String pmsFmStatus){
		
		FmDrug fmDrug = dmsPmsServiceProxy.retrieveFmIndicator(workstore.getHospCode(), workstore.getWorkstoreCode(), itemCode, pmsFmStatus);
		
		sDrugList = new ArrayList<FmDrugCorpInd>();
		sfiList = new ArrayList<FmDrugCorpInd>();
		safetyNetList = new ArrayList<FmDrugCorpInd>();
		
		if(fmDrug!=null){
			for (FmDrugCorpInd fmDrugCorpInd :fmDrug.getFmDrugCorpIndArray()){
				logger.debug("retrieveFmIndicator: corpIndicationOption=#0, corpIndicationCode=#1, corpIndicationDesc=#2, ",
							 fmDrugCorpInd.getCorpIndicationOption(),
							 fmDrugCorpInd.getCorpIndicationCode(),
							 fmDrugCorpInd.getCorpIndicationDesc());
				
				if( StringUtils.isBlank(fmDrugCorpInd.getCorpIndicationCode()) || "OTHERS".equals(fmDrugCorpInd.getCorpIndicationCode()) ) {
					continue;
				}
				
				if( FmStatusOption.SpecialDrug.getDataValue().equals(fmDrugCorpInd.getCorpIndicationOption()) ){
					sDrugList.add(fmDrugCorpInd);
				}else if ( FmStatusOption.SelfFinancedItem.getDataValue().equals(fmDrugCorpInd.getCorpIndicationOption()) ){
					sfiList.add(fmDrugCorpInd);
				}else if ( FmStatusOption.SafetyNetItem.getDataValue().equals(fmDrugCorpInd.getCorpIndicationOption()) ){
					safetyNetList.add(fmDrugCorpInd);
				}
			}
		}
	}

	@Remove
	public void destroy() {
		if (sDrugList != null) {
			sDrugList = null;
		}
		
		if (sfiList != null) {
			sfiList = null;
		}
		
		if (safetyNetList != null) {
			safetyNetList = null;
		}
	}
}
