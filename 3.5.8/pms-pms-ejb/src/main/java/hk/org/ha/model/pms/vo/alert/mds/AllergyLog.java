package hk.org.ha.model.pms.vo.alert.mds;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class AllergyLog implements Serializable {

	private static final long serialVersionUID = 1L;

	private String allergenName;

	private String hiclSeqNum;

	private List<String> hicSeqNumList;

	public String getAllergenName() {
		return allergenName;
	}

	public void setAllergenName(String allergenName) {
		this.allergenName = allergenName;
	}

	public String getHiclSeqNum() {
		return hiclSeqNum;
	}

	public void setHiclSeqNum(String hiclSeqNum) {
		this.hiclSeqNum = hiclSeqNum;
	}

	public List<String> getHicSeqNumList() {
		return hicSeqNumList;
	}

	public void setHicSeqNumList(List<String> hicSeqNumList) {
		this.hicSeqNumList = hicSeqNumList;
	}
}
