package hk.org.ha.model.pms.biz.order;

import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;

import javax.ejb.Local;

@Local
public interface MedOrderManagerLocal {

	void setRefillSchedule(RefillSchedule inRefillSchedule);

	void setDispOrder(DispOrder inDispOrder);

	void setPharmOrder(PharmOrder inPharmOrder);

	void clear();
	
}
