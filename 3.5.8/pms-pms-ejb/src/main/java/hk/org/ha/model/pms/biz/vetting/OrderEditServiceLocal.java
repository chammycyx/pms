package hk.org.ha.model.pms.biz.vetting;
import java.util.List;

import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.vo.rx.DhRxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;

import javax.ejb.Local;

@Local
public interface OrderEditServiceLocal {

	String retrieveFormattedDrugOrderTlf(RxItem rxItem);
	
	void retrieveMedOrderItemForOrderEdit(int index);
	
	List<Integer> updateMedOrderFromOrderEdit(int index, MedOrderItem medOrderItemIn, boolean skipDuplicateCheckFlag);

	Boolean isDhMappingUpToDate(int index);
	
	void destroy();
}
