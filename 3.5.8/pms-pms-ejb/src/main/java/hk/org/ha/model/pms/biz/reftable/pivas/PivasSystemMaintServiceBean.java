package hk.org.ha.model.pms.biz.reftable.pivas;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.reftable.pivas.PivasSystemMaintInfo;
import hk.org.ha.model.pms.vo.reftable.pivas.PivasSystemMaintUpdateSummary;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("pivasSystemMaintService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PivasSystemMaintServiceBean implements PivasSystemMaintServiceLocal {
	
	@In
	private Workstore workstore;
	
	@In
	private PivasHolidayListManagerLocal pivasHolidayListManager;

	@In
	private PivasServiceHourListManagerLocal pivasServiceHourListManager;

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@Override
	public PivasSystemMaintInfo retrievePivasSystemMaintInfo(String year) {
		PivasSystemMaintInfo pivasSystemMaintInfo = new PivasSystemMaintInfo();
		pivasSystemMaintInfo.setPivasServiceHourList(pivasServiceHourListManager.retrievePivasServiceHourList(workstore.getWorkstoreGroup()));
		pivasSystemMaintInfo.setPivasHolidayList(pivasHolidayListManager.retrievePivasHolidayList(workstore.getWorkstoreGroup(), year));
		pivasSystemMaintInfo.setPivasContainerList(dmsPmsServiceProxy.retrievePivasContainerListForSystemMaint(workstore.getWorkstoreGroup().getWorkstoreGroupCode()));
		return pivasSystemMaintInfo;
	}

	public PivasSystemMaintInfo updatePivasSystemMaint(PivasSystemMaintUpdateSummary pivasSystemMaintUpdateSummary) {
		pivasServiceHourListManager.updatePivasServiceHourList(pivasSystemMaintUpdateSummary.getUpdatePivasServiceHourList(), 
																	pivasSystemMaintUpdateSummary.getDeletePivasServiceHourList(), 
																	workstore.getWorkstoreGroup());
		pivasHolidayListManager.updatePivasHolidayList(pivasSystemMaintUpdateSummary.getUpdatePivasHolidayList(), 
														pivasSystemMaintUpdateSummary.getDeletePivasHolidayList(), 
														workstore.getWorkstoreGroup());
		dmsPmsServiceProxy.updatePivasContainerList(pivasSystemMaintUpdateSummary.getUpdatePivasContainerList(), workstore.getWorkstoreGroup().getWorkstoreGroupCode());
		return retrievePivasSystemMaintInfo(pivasSystemMaintUpdateSummary.getPivasHolidaySelectedYear());
	}

	@Remove
	public void destroy() {
	}
}



