package hk.org.ha.model.pms.biz.inbox;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileTrxLog;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.disp.MpTrxType;
import hk.org.ha.model.pms.udt.medprofile.InactiveType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatus;

import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("pharmInboxHistoryService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PharmInboxHistoryServiceBean implements PharmInboxHistoryServiceLocal {
	
	private static final String MED_PROFILE_QUERY_HINT_FETCH_1 = "o.medProfile.medCase";
	private static final String MED_PROFILE_QUERY_HINT_FETCH_2 = "o.medProfile.patient";	
	
	@PersistenceContext
	private EntityManager em;	
	
	@In
	private Workstation workstation;
	
	@Out(required = false)
	protected List<MedProfileTrxLog> medProfileTrxLogList;
	
    public PharmInboxHistoryServiceBean() {
    }
    
    @SuppressWarnings("unchecked")
	public void retrieveMedProfileTrxLogList() {
    
    	Date startDate = MedProfileUtils.addDays(new Date(), -1);

    	medProfileTrxLogList = em.createQuery(
    			"select o from MedProfileTrxLog o" + // 20130911 index check : MedProfileTrxLog.workstationId,createDate : I_MED_PROFILE_TRX_LOG_01
    			" where o.workstationId = :workstationId" +
    			" and o.medProfile.hospCode = :hospCode" +
    			" and o.type = :type" +
    			" and o.createDate > :startDate" +
    			" and (o.medProfile.status <> :status" +
    			"  or o.medProfile.inactiveType is null" +
    			"  or o.medProfile.inactiveType <> :inactiveType)" +
    			" order by o.createDate desc")
    			.setParameter("workstationId", workstation.getId())
    			.setParameter("hospCode", workstation.getWorkstore().getHospCode())
    			.setParameter("type", MpTrxType.MpSaveProfile)
    			.setParameter("startDate", startDate)
    			.setParameter("status", MedProfileStatus.Inactive)
    			.setParameter("inactiveType", InactiveType.Deleted)
    			.setHint(QueryHints.FETCH, MED_PROFILE_QUERY_HINT_FETCH_1)
    			.setHint(QueryHints.FETCH, MED_PROFILE_QUERY_HINT_FETCH_2)
    			.setMaxResults(30)
    			.getResultList();
    }
    
	@Remove
	@Override
	public void destroy() {
		medProfileTrxLogList = null;
	}
}
