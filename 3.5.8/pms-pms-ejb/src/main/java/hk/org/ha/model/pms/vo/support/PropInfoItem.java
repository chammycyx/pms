package hk.org.ha.model.pms.vo.support;

import hk.org.ha.model.pms.persistence.reftable.Prop;
import hk.org.ha.model.pms.persistence.reftable.Workstation;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PropInfoItem implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String hospCode;
	
	private String workstoreCode;
	
	private Workstation workstation;
	
	private String value;
	
	private String displayValue;
	
	private Prop prop;
	
	private Integer sortSeq;
	
	private boolean supportMaintFlag;
	
	private Long propItemId;
	
	private String description;

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public Workstation getWorkstation() {
		return workstation;
	}

	public void setWorkstation(Workstation workstation) {
		this.workstation = workstation;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDisplayValue() {
		return displayValue;
	}

	public void setDisplayValue(String displayValue) {
		this.displayValue = displayValue;
	}

	public Prop getProp() {
		return prop;
	}

	public void setProp(Prop prop) {
		this.prop = prop;
	}

	public Integer getSortSeq() {
		return sortSeq;
	}

	public void setSortSeq(Integer sortSeq) {
		this.sortSeq = sortSeq;
	}

	public boolean isSupportMaintFlag() {
		return supportMaintFlag;
	}

	public void setSupportMaintFlag(boolean supportMaintFlag) {
		this.supportMaintFlag = supportMaintFlag;
	}

	public void setPropItemId(Long propItemId) {
		this.propItemId = propItemId;
	}

	public Long getPropItemId() {
		return propItemId;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}
	
}