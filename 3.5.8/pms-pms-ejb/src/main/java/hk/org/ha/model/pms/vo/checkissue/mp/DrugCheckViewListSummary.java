package hk.org.ha.model.pms.vo.checkissue.mp;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class DrugCheckViewListSummary implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private List<CheckDeliveryInfo> checkedDeliveryList;
	
	private List<CheckDeliveryInfo> readyForCheckDeliveryList;

	public List<CheckDeliveryInfo> getCheckedDeliveryList() {
		return checkedDeliveryList;
	}

	public void setCheckedDeliveryList(List<CheckDeliveryInfo> checkedDeliveryList) {
		this.checkedDeliveryList = checkedDeliveryList;
	}

	public List<CheckDeliveryInfo> getReadyForCheckDeliveryList() {
		return readyForCheckDeliveryList;
	}

	public void setReadyForCheckDeliveryList(
			List<CheckDeliveryInfo> readyForCheckDeliveryList) {
		this.readyForCheckDeliveryList = readyForCheckDeliveryList;
	}
}