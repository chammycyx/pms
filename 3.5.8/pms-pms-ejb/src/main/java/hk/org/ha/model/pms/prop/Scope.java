package hk.org.ha.model.pms.prop;

public enum Scope {
	OPERATION,
	WORKSTORE,
	WORKSTATION,
	HOSPITAL,
	CORPORATE
}
