package hk.org.ha.model.pms.biz.medprofile;
import java.util.List;

import hk.org.ha.model.pms.dms.persistence.PmsIpSpecMapping;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;

import javax.ejb.Local;

import org.jboss.seam.annotations.async.Asynchronous;

@Local
public interface MedProfileStatManagerLocal {

	void recalculateAllStat(MedProfile medProfile, boolean dispatchUpdateEvent);
	void recalculateMedProfileErrorStat(MedProfile medProfile);
	void recalculateMedProfileStat(MedProfile medProfile,
			boolean dispatchUpdateEvent);
	void recalculateMedProfilePoItemStat(MedProfile medProfile);	
	@Asynchronous
	void clearMissingSpecialtyMappingException(List<PmsIpSpecMapping> pmsIpSpecMappingList);
}
