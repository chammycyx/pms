package hk.org.ha.model.pms.persistence.reftable;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.reftable.OperationWorkstationType;
import hk.org.ha.model.pms.udt.reftable.PrinterConnectionType;
import hk.org.ha.model.pms.udt.reftable.PrinterType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

@Entity
@Table(name = "OPERATION_WORKSTATION")
public class OperationWorkstation extends VersionEntity {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "operationWorkstationSeq")
	/*lower the inital value to avoid the auto delete by using clean_db() */
	@SequenceGenerator(name = "operationWorkstationSeq", sequenceName = "SQ_OPERATION_WORKSTATION", initialValue = 1000000)
	private Long id;
	
	@Converter(name = "OperationWorkstation.type", converterClass = OperationWorkstationType.Converter.class)
    @Convert("OperationWorkstation.type")
	@Column(name = "TYPE", nullable = false, length = 1)
	private OperationWorkstationType type;
	
	@Column(name = "ISSUE_WINDOW_NUM", length = 2)
	private Integer issueWindowNum;
		
	@Converter(name = "OperationWorkstation.printerType", converterClass = PrinterType.Converter.class)
    @Convert("OperationWorkstation.printerType")
	@Column(name = "PRINTER_TYPE", nullable = false, length = 3)
	private PrinterType printerType;
	
	@Converter(name = "OperationWorkstation.printerConnectionType", converterClass = PrinterConnectionType.Converter.class)
    @Convert("OperationWorkstation.printerConnectionType")
	@Column(name = "PRINTER_CONNECTION_TYPE", nullable = false, length = 1)
	private PrinterConnectionType printerConnectionType;
	
	@Column(name = "REDIRECT_PRINT_STATION", length = 4)
	private String redirectPrintStation;

	@ManyToOne
	@JoinColumn(name = "OPERATION_PROFILE_ID", nullable = false)
	private OperationProfile operationProfile;

    @JoinFetch(JoinFetchType.INNER)
	@ManyToOne
	@JoinColumn(name = "WORKSTATION_ID", nullable = false)
	private Workstation workstation;

	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "MACHINE_ID")
    private Machine machine;

	@Column(name = "ENABLE_FLAG", nullable = false, length = 1)
	private Boolean enableFlag;
	
	@Column(name = "REDIRECT_FAST_QUEUE_STATION", length = 4)
	private String redirectFastQueueStation;

	@Column(name = "FAST_QUEUE_PRINT_FLAG", length = 1)
	private Boolean fastQueuePrintFlag;
	
	public OperationWorkstation() {
		enableFlag = Boolean.FALSE;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public OperationWorkstationType getType() {
		return type;
	}

	public void setType(OperationWorkstationType type) {
		this.type = type;
	}

	public Integer getIssueWindowNum() {
		return issueWindowNum;
	}

	public void setIssueWindowNum(Integer issueWindowNum) {
		this.issueWindowNum = issueWindowNum;
	}

	public String getRedirectPrintStation() {
		return redirectPrintStation;
	}

	public void setRedirectPrintStation(String redirectPrintStation) {
		this.redirectPrintStation = redirectPrintStation;
	}

	public OperationProfile getOperationProfile() {
		return operationProfile;
	}

	public void setOperationProfile(OperationProfile operationProfile) {
		this.operationProfile = operationProfile;
	}

	public Workstation getWorkstation() {
		return workstation;
	}

	public void setWorkstation(Workstation workstation) {
		this.workstation = workstation;
	}

	public Machine getMachine() {
		return machine;
	}

	public void setMachine(Machine machine) {
		this.machine = machine;
	}

	public void setPrinterConnectionType(PrinterConnectionType printerConnectionType) {
		this.printerConnectionType = printerConnectionType;
	}

	public PrinterConnectionType getPrinterConnectionType() {
		return printerConnectionType;
	}

	public Boolean getEnableFlag() {
		return enableFlag;
	}

	public void setEnableFlag(Boolean enableFlag) {
		this.enableFlag = enableFlag;
	}

	public void setPrinterType(PrinterType printerType) {
		this.printerType = printerType;
	}

	public PrinterType getPrinterType() {
		return printerType;
	}

	public String getRedirectFastQueueStation() {
		return redirectFastQueueStation;
	}

	public void setRedirectFastQueueStation(String redirectFastQueueStation) {
		this.redirectFastQueueStation = redirectFastQueueStation;
	}

	public Boolean getFastQueuePrintFlag() {
		return fastQueuePrintFlag;
	}

	public void setFastQueuePrintFlag(Boolean fastQueuePrintFlag) {
		this.fastQueuePrintFlag = fastQueuePrintFlag;
	}
}
