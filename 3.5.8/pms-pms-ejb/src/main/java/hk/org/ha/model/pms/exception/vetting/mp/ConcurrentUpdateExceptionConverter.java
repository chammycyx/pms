package hk.org.ha.model.pms.exception.vetting.mp;

import java.util.Map;

import org.granite.messaging.service.ExceptionConverter;
import org.granite.messaging.service.ServiceException;

public class ConcurrentUpdateExceptionConverter implements ExceptionConverter {
	
	public static final String CONCURRENT_UPDATE_EXCEPTION = "ConcurrentUpdateException";

	@Override
	public boolean accepts(Throwable t, Throwable finalException) {
		return t.getClass().equals(ConcurrentUpdateException.class);
	}

	@Override
	public ServiceException convert(Throwable t, String detail, Map<String, Object> extendedData) {
		
		ServiceException se = new ServiceException(CONCURRENT_UPDATE_EXCEPTION, t.getMessage(), detail, t);
		se.getExtendedData().putAll(extendedData);
		se.getExtendedData().put("type", ((ConcurrentUpdateException) t).getType());
		return se;
	}
}
