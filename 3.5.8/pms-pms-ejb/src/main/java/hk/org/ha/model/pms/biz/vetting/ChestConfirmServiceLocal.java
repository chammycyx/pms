package hk.org.ha.model.pms.biz.vetting;

import hk.org.ha.model.pms.persistence.disp.PharmOrder;

import javax.ejb.Local;

@Local
public interface ChestConfirmServiceLocal {
	
	void updatePharmOrderFromChest(PharmOrder pharmOrder);
	
	int retrieveChestList();
	
	void retrieveChest(String unitDoseName);
	
	void clearChest();
	
	void destroy();
	
}
