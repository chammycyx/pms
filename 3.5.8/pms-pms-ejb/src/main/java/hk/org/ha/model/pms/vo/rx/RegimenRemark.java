package hk.org.ha.model.pms.vo.rx;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class RegimenRemark extends Regimen implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Boolean typeRemark;
	
	public RegimenRemark() {
		super();
		typeRemark = Boolean.FALSE;
	}
    
	public Boolean getTypeRemark() {
		return typeRemark;
	}

	public void setTypeRemark(Boolean typeRemark) {
		this.typeRemark = typeRemark;
	}

	public List<DoseGroupRemark> getDoseGroupRemarkList() {
		List<DoseGroup> tempDoseGroupList = getDoseGroupList();
		if (tempDoseGroupList == null) {
			return null;
		}
		
		List<DoseGroupRemark> doseGroupRemarkList = new ArrayList<DoseGroupRemark>();
		for (DoseGroup tempDoseGroup : tempDoseGroupList) {
			doseGroupRemarkList.add((DoseGroupRemark)tempDoseGroup);
		}
		
		return doseGroupRemarkList;
	}
	
	public void setDoseGroupRemarkList(List<DoseGroupRemark> doseGroupRemarkList) {
		if (doseGroupRemarkList == null) {
			setDoseGroupList(null);
		}
		
		List<DoseGroup> tempDoseGroupList = new ArrayList<DoseGroup>();
		for (DoseGroupRemark doseGroupRemark : doseGroupRemarkList) {
			tempDoseGroupList.add(doseGroupRemark);
		}
		setDoseGroupList(tempDoseGroupList);
	}
}
