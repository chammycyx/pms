package hk.org.ha.model.pms.persistence.medprofile;

import java.util.List;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.medprofile.DeliveryRequestGroupStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "DELIVERY_REQUEST_GROUP")
public class DeliveryRequestGroup extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "deliveryRequestGroupSeq")
	@SequenceGenerator(name = "deliveryRequestGroupSeq", sequenceName = "SQ_DELIVERY_REQUEST_GROUP", initialValue = 100000000)
	private Long id;
		
    @Converter(name = "DeliveryRequestGroup.status", converterClass = DeliveryRequestGroupStatus.Converter.class)
    @Convert("DeliveryRequestGroup.status")
	@Column(name="STATUS", nullable = false, length = 1)
	private DeliveryRequestGroupStatus status;
	
	@ManyToOne
	@JoinColumn(name = "DELIVERY_SCHEDULE_ITEM_ID", nullable = false)
	private DeliveryScheduleItem deliveryScheduleItem;
    		
	@Column(name = "DELIVERY_SCHEDULE_ITEM_ID", nullable = false, insertable = false, updatable = false)
	private Long deliveryScheduleItemId;

	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE",      referencedColumnName = "HOSP_CODE",      nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;

	@OneToMany(mappedBy = "deliveryRequestGroup")
    private List<DeliveryRequest> deliveryRequestList;
	
	private transient final Object mutex = new Object();
	
	private transient Integer errorCount;
	
	private transient Integer successCount;
	
	private transient Throwable throwable;
	
	public DeliveryRequestGroup() {
		errorCount = Integer.valueOf(0);
		successCount = Integer.valueOf(0);
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DeliveryRequestGroupStatus getStatus() {
		return status;
	}

	public void setStatus(DeliveryRequestGroupStatus status) {
		this.status = status;
	}

	public DeliveryScheduleItem getDeliveryScheduleItem() {
		return deliveryScheduleItem;
	}

	public void setDeliveryScheduleItem(DeliveryScheduleItem deliveryScheduleItem) {
		this.deliveryScheduleItem = deliveryScheduleItem;
	}

	public Long getDeliveryScheduleItemId() {
		return deliveryScheduleItemId;
	}

	public void setDeliveryScheduleItemId(Long deliveryScheduleItemId) {
		this.deliveryScheduleItemId = deliveryScheduleItemId;
	}
	
	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}
	
	public List<DeliveryRequest> getDeliveryRequestList() {
		return deliveryRequestList;
	}

	public void setDeliveryRequestList(List<DeliveryRequest> deliveryRequestList) {
		this.deliveryRequestList = deliveryRequestList;
	}

	public boolean isCompleted(long timeout) {
		synchronized (mutex) {
			if(status == DeliveryRequestGroupStatus.Active){
				return true;
			}
			try{
				mutex.wait(timeout);
			}catch(Exception ex){	
			}
			return status == DeliveryRequestGroupStatus.Active;
		}
	}
	
	public void setStatusWithNotification(DeliveryRequestGroupStatus status){
		synchronized (mutex) {
			this.status = status;
			mutex.notifyAll();
		}
	}

	public void addErrorCount(Integer addCount){
		this.errorCount += addCount;
	}
	
	public Integer getErrorCount() {
		return errorCount;
	}

	public void setErrorCount(Integer errorCount) {
		this.errorCount = errorCount;
	}

	public void addSuccessCount(Integer addCount){
		this.successCount += addCount;
	}
	
	public Integer getSuccessCount() {
		return successCount;
	}

	public void setSuccessCount(Integer successCount) {
		this.successCount = successCount;
	}

	public Throwable getThrowable() {
		return throwable;
	}

	public void setThrowable(Throwable throwable) {
		this.throwable = throwable;
	}
}
