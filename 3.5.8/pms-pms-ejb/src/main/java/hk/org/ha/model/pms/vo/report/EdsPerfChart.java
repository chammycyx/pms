package hk.org.ha.model.pms.vo.report;

import java.awt.Color;
import java.io.Serializable;

import net.sf.jasperreports.engine.JRChart;
import net.sf.jasperreports.engine.JRChartCustomizer;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BarRenderer3D;

public class EdsPerfChart implements JRChartCustomizer, Serializable
{
	private static final long serialVersionUID = 1L;

	public void customize(JFreeChart chart, JRChart jasperChart)
	{
		CategoryPlot plot = chart.getCategoryPlot();
		plot.setRangeGridlinesVisible(false);

		BarRenderer3D renderer = (BarRenderer3D) plot.getRenderer();
		renderer.setSeriesPaint(0, Color.red);
		renderer.setMaximumBarWidth(0.08);
		
		ValueAxis axisV = plot.getRangeAxis();
        axisV.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        axisV.setLowerBound(0);
		
        CategoryAxis axisC = plot.getDomainAxis();
        axisC.setMaximumCategoryLabelLines(2);
	}
}
