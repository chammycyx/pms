package hk.org.ha.model.pms.biz.drug;

import java.util.List;
import java.util.Map;

import hk.org.ha.model.pms.dms.vo.DrugRouteCriteria;
import hk.org.ha.model.pms.vo.rx.Site;

import javax.ejb.Local;

@Local
public interface DrugRouteManagerLocal {

	List<Site> retrieveIpSiteListByItemCode(String itemCode);
	List<Site> retrieveManualEntrySiteListByItemCode(String itemCode);
	List<Map<String, Object>> retrieveIpOthersSiteMapListByDrugRouteCriteria(DrugRouteCriteria criteria);	
	List<Map<String, Object>> retrieveManualEntryOthersSiteMapListByDrugRouteCriteria(DrugRouteCriteria criteria);

}
