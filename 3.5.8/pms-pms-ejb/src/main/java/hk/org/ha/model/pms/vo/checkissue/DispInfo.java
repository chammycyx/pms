package hk.org.ha.model.pms.vo.checkissue;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class DispInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Long dispOrderId;
	
	private Date pickDate;
	
	private Date assembleDate;
	
	private String checkUser;
	
	private Date checkDate;
	
	private String issueUser;
	
	private Date issueDate;
	
	private Map<Long, DispItemInfo> dispItemInfoMap;

	public Long getDispOrderId() {
		return dispOrderId;
	}

	public void setDispOrderId(Long dispOrderId) {
		this.dispOrderId = dispOrderId;
	}
	
	public String getCheckUser() {
		return checkUser;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public void setCheckUser(String checkUser) {
		this.checkUser = checkUser;
	}

	public Date getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}

	public void setDispItemInfoMap(Map<Long, DispItemInfo> dispItemInfoMap) {
		this.dispItemInfoMap = dispItemInfoMap;
	}

	public Map<Long, DispItemInfo> getDispItemInfoMap() {
		return dispItemInfoMap;
	}

	public void setPickDate(Date pickDate) {
		this.pickDate = pickDate;
	}

	public Date getPickDate() {
		return pickDate;
	}

	public void setAssembleDate(Date assembleDate) {
		this.assembleDate = assembleDate;
	}

	public Date getAssembleDate() {
		return assembleDate;
	}

	public void setIssueUser(String issueUser) {
		this.issueUser = issueUser;
	}

	public String getIssueUser() {
		return issueUser;
	}

}