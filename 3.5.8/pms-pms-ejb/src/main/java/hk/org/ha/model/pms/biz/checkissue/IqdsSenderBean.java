package hk.org.ha.model.pms.biz.checkissue;

import static hk.org.ha.model.pms.prop.Prop.MACHINE_IQDS_HOSTNAME;
import static hk.org.ha.model.pms.prop.Prop.MACHINE_IQDS_PORT;
import static hk.org.ha.model.pms.prop.Prop.MACHINE_IQDS_TOUCHMED_ENABLED;

import java.util.ArrayList;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.machine.MachineException;
import hk.org.ha.model.pms.udt.machine.IqdsAction;
import hk.org.ha.model.pms.vo.machine.IqdsMessageContent;
import hk.org.ha.service.pms.asa.interfaces.machine.MachineServiceJmsRemote;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

/**
 * Session Bean implementation class IqdsSenderBean
 */
@AutoCreate
@Stateless
@Name("iqdsSender")
@MeasureCalls
public class IqdsSenderBean implements IqdsSenderLocal {

	@Logger
	private Log logger; 
	
	@In
	private MachineServiceJmsRemote machineServiceProxy;
	
	public void sendTicketToIqds(String hospCode, String workstoreCode, String ticketNum, Integer issueWindowNum, IqdsAction action) {
		
		List<IqdsMessageContent> iqdsMessageContentList = new ArrayList<IqdsMessageContent>();
		IqdsMessageContent iqdsMessageContent = new IqdsMessageContent();
		iqdsMessageContent.setTicketNum(ticketNum);
		if ( issueWindowNum != null ) {
			iqdsMessageContent.setIssueWindowNum(issueWindowNum.toString());
		}
		iqdsMessageContent.setHospCode(hospCode);
		iqdsMessageContent.setWorkstoreCode(workstoreCode);
		iqdsMessageContent.setIqdsAction(action);
		iqdsMessageContentList.add(iqdsMessageContent);
		
		this.sendTicketToIqds(iqdsMessageContentList);
	}
	
	public void sendTicketToIqds(List<IqdsMessageContent> iqdsMessageContentList) {
		
		for(IqdsMessageContent iqdsMessageContent: iqdsMessageContentList) {
			try {
				iqdsMessageContent.setHostname(MACHINE_IQDS_HOSTNAME.get());
				iqdsMessageContent.setPortNum(MACHINE_IQDS_PORT.get(5999));
				iqdsMessageContent.setTouchMedEnabled(MACHINE_IQDS_TOUCHMED_ENABLED.get());
				
				logger.debug("sendTicketToIqds: hostname=#0, portNum=#1, issueWindowNum=#2, ticketNum=#3" ,
							 iqdsMessageContent.getHostname(),
							 iqdsMessageContent.getPortNum(),
							 iqdsMessageContent.getIssueWindowNum(),
							 iqdsMessageContent.getTicketNum());
				
				machineServiceProxy.sendIqdsSocketMessage(iqdsMessageContent);
			} catch (MachineException e) {
				logger.error("Fail to send message to IQDS");
			}
		}
	}
}
