package hk.org.ha.model.pms.biz.order;

import hk.org.ha.service.biz.pms.interfaces.PmsEnquiryServiceJmsRemote;

import javax.ejb.Local;

@Local
public interface PmsEnquiryServiceLocal extends PmsEnquiryServiceJmsRemote {
}
