package hk.org.ha.model.pms.biz.drug;
import java.util.List;

import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;

import javax.ejb.Local;

@Local
public interface DrugDiluentListManagerLocal {
	List<WorkstoreDrug> getDrugDiluentList(RxItem rxItem);
}
