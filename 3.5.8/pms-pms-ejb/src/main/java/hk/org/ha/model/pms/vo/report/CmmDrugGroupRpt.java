package hk.org.ha.model.pms.vo.report;

import hk.org.ha.model.pms.dms.persistence.cmm.CmmDrugGroup;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class CmmDrugGroupRpt implements Serializable {

	private static final long serialVersionUID = 1L;

	private int groupSeqNum;
	
	private String displayName;
	
	private String saltProperty;
	
	private String formDesc;
	
	public CmmDrugGroupRpt() {
	}
	
	public CmmDrugGroupRpt(CmmDrugGroup cmmDrugGroup) {
		displayName = cmmDrugGroup.getDisplayName();
		saltProperty = cmmDrugGroup.getSaltProperty();
		formDesc = cmmDrugGroup.getForm();
	}

	public int getGroupSeqNum() {
		return groupSeqNum;
	}

	public void setGroupSeqNum(int groupSeqNum) {
		this.groupSeqNum = groupSeqNum;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getSaltProperty() {
		return saltProperty;
	}

	public void setSaltProperty(String saltProperty) {
		this.saltProperty = saltProperty;
	}

	public String getFormDesc() {
		return formDesc;
	}

	public void setFormDesc(String formDesc) {
		this.formDesc = formDesc;
	}
}
