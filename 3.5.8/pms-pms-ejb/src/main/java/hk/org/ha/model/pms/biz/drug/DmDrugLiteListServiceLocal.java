package hk.org.ha.model.pms.biz.drug;
import javax.ejb.Local;

@Local
public interface DmDrugLiteListServiceLocal {

	void retrieveDmDrugLiteList(String prefixItemCode, boolean showNonSuspendItemOnly, boolean fdn);
	
	void retrieveDmDrugLiteListByFullDrugDesc(String prefixFullDrugDesc);
	
	void retrieveDmDrugLiteListForChestItem(String prefixItemCode);
	
	void destroy();
}
