package hk.org.ha.model.pms.udt.refill;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum RefillScheduleStatus implements StringValuedEnum {

	NotYetDispensed("N", "Not Yet Dispensed"),
	BeingProcessed("B", "Being Processed"),
	Dispensed("D", "Dispensed"),
	ForceComplete("F", "Force Complete"),
	Abort("A", "Deleted"),
	SysDeleted("X", "SysDeleted");
	
    private final String dataValue;
    private final String displayValue;

    RefillScheduleStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    public String getDataValue() {
        return this.dataValue;
    }

    public String getDisplayValue() {
        return this.displayValue;
    }  
    
    public static RefillScheduleStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(RefillScheduleStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<RefillScheduleStatus> {

		private static final long serialVersionUID = 1L;

		public Class<RefillScheduleStatus> getEnumClass() {
    		return RefillScheduleStatus.class;
    	}
    }
}
