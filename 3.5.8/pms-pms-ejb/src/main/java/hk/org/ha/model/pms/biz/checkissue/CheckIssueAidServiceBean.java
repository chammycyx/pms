package hk.org.ha.model.pms.biz.checkissue;

import static hk.org.ha.model.pms.prop.Prop.CHECKISSUE_REFRESH;
import static hk.org.ha.model.pms.prop.Prop.CHECK_HOSTNAME;
import static hk.org.ha.model.pms.prop.Prop.ONESTOP_SUSPENDORDERCOUNT_DURATION_LIMIT;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.onestop.OneStopCountManagerBean;
import hk.org.ha.model.pms.biz.onestop.OneStopCountManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.Prop;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.persistence.reftable.WorkstationProp;
import hk.org.ha.model.pms.persistence.reftable.WorkstoreProp;
import hk.org.ha.model.pms.vo.onestop.MoStatusCount;
import hk.org.ha.model.pms.vo.security.PropMap;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("checkIssueAidService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CheckIssueAidServiceBean implements CheckIssueAidServiceLocal {
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private OneStopCountManagerLocal oneStopCountManager;
	
	@In
	private CheckIssueManagerLocal checkIssueManager;

	@In
	private Workstation workstation;

	@In
	private Workstore workstore;
		
	@In @Out(required=false)
	public PropMap propMap;

	@Out(required = false)
	private Integer issueWindowNum; 
	
	@Out(required = false)
	private MoStatusCount moStatusCount;
	
	public void retrieveIssueWindowNum(){ 
		issueWindowNum = checkIssueManager.retrieveIssueWindowNum(workstore, workstation);
	}
	
	@SuppressWarnings("unchecked")
	public void updateCheckWorkstationRuleCode(){
		List<WorkstoreProp> workstorePropList = em.createQuery(
				"select o from WorkstoreProp o " + // 20120214 index check : Prop.name : UI_PROP_01
				" where o.prop.name = :name " +
				" and o.workstore = :workstore ")
				.setParameter("name", CHECK_HOSTNAME.getName())
				.setParameter("workstore", workstore)
				.getResultList();
		
		if ( !workstorePropList.isEmpty() ) {
			WorkstoreProp workstoreProp = workstorePropList.get(0);
			workstoreProp.setValue(workstation.getHostName());
			em.merge(workstoreProp);
			em.flush();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void updateRefresh(boolean value){
		List<WorkstationProp> workstationPropList = em.createQuery(
				"select o from WorkstationProp o " + // 20120214 index check : Prop.name : UI_PROP_01
				" where o.prop.name = :name " +
				" and o.workstation = :workstation")
				.setParameter("name", CHECKISSUE_REFRESH.getName())
				.setParameter("workstation", workstation)
				.getResultList();
		
		if ( !workstationPropList.isEmpty() ) {
			WorkstationProp workstationProp = workstationPropList.get(0);
			if (value) {
				workstationProp.setValue("Y");
			} else {
				workstationProp.setValue("N");
			}
			em.merge(workstationProp);
			em.flush();
			propMap.add(CHECKISSUE_REFRESH.getName(), workstationProp.getValue());
		} else {
			List<Prop> propList = em.createQuery(
				"select o from Prop o " + // 20120214 index check : Prop.name : UI_PROP_01
				" where o.name = :name ")
				.setParameter("name", CHECKISSUE_REFRESH.getName())
				.getResultList();
			if ( !propList.isEmpty() ) {
				WorkstationProp workstationProp = new WorkstationProp();
				workstationProp.setProp(propList.get(0));
				if (value) {
					workstationProp.setValue("Y");
				} else {
					workstationProp.setValue("N");
				}
				workstationProp.setWorkstation(workstation);
				em.persist(workstationProp);
				em.flush();
				propMap.add(CHECKISSUE_REFRESH.getName(), workstationProp.getValue());
			}
		}
	}
	
	public void retrieveOutstandingMoeOrderCount(){
		if (oneStopCountManager.isCounterTimeOut(moStatusCount))
		{
			oneStopCountManager.retrieveUnvetAndSuspendCount(workstore, propMap.getValueAsInteger(ONESTOP_SUSPENDORDERCOUNT_DURATION_LIMIT.getName()));
		}
		
		moStatusCount = OneStopCountManagerBean.getMoStatusCountByWorkstore(workstore);
	}
	

	
	@Remove
	public void destroy() {
		if (issueWindowNum != null ) { 
			issueWindowNum = null;
		}
		if (moStatusCount != null) {
			moStatusCount = null;
		}
	}

}
