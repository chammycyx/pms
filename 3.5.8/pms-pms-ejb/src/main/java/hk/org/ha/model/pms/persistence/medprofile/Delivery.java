package hk.org.ha.model.pms.persistence.medprofile;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.udt.medprofile.DeliveryItemStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryType;
import hk.org.ha.model.pms.udt.medprofile.PrintMode;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "DELIVERY")
public class Delivery extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "deliverySeq")
	@SequenceGenerator(name = "deliverySeq", sequenceName = "SQ_DELIVERY", initialValue = 100000000)
	private Long id;
	
	@Column(name = "WARD_CODE", length = 4)
	private String wardCode;
	
	@Column(name = "BATCH_NUM", nullable = false, length = 4)
	private String batchNum;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "BATCH_DATE", nullable = false)
	private Date batchDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DELIVERY_DATE")
	private Date deliveryDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CHECK_DATE")
	private Date checkDate;
	
    @Converter(name = "Delivery.printMode", converterClass = PrintMode.Converter.class)
    @Convert("Delivery.printMode")
	@Column(name = "PRINT_MODE", nullable = false, length = 1)
	private PrintMode printMode;

    @Converter(name = "Delivery.status", converterClass = DeliveryStatus.Converter.class)
    @Convert("Delivery.status")
	@Column(name = "STATUS", nullable = false, length = 1)
	private DeliveryStatus status;

    @Converter(name = "Delivery.type", converterClass = DeliveryType.Converter.class)
    @Convert("Delivery.type")
	@Column(name = "TYPE", nullable = false, length = 1)
	private DeliveryType type;
    
    @Column(name = "ERROR_FLAG", nullable = false)
    private Boolean errorFlag; 
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DELIVERY_REQUEST_ID")
    private DeliveryRequest deliveryRequest;
    
    @OneToMany(mappedBy = "delivery")
    private List<DeliveryItem> deliveryItemList;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "HOSP_CODE")
    private Hospital hospital;
    
    //for transaction report to check when the batch is updated
    @Column(name = "MODIFY_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifyDate;
    
    @Column(name = "PRINTED_COUNT")
	private Integer printedCount;
    
	@Transient
	private Integer numOfLabel;    
	
	//for label reprint to show DeliveryItemStatus via trxId
	@Transient
	private String trxIdDeliveryItem;
    
	public Delivery() {
		printMode = PrintMode.None;
		status = DeliveryStatus.Delivered;
		type = DeliveryType.Normal;
		errorFlag = Boolean.FALSE;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public Date getBatchDate() {
		return batchDate == null ? null : new Date(batchDate.getTime());
	}

	public void setBatchDate(Date batchDate) {
		this.batchDate = batchDate == null ? null : new Date(batchDate.getTime());
	}
	
	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public PrintMode getPrintMode() {
		return printMode;
	}

	public void setPrintMode(PrintMode printMode) {
		this.printMode = printMode;
	}

	public DeliveryStatus getStatus() {
		return status;
	}

	public void setStatus(DeliveryStatus status) {
		this.status = status;
	}
	
	public DeliveryType getType() {
		return type;
	}

	public void setType(DeliveryType type) {
		this.type = type;
	}
	
	public Boolean getErrorFlag() {
		return errorFlag;
	}

	public void setErrorFlag(Boolean errorFlag) {
		this.errorFlag = errorFlag;
	}

	public DeliveryRequest getDeliveryRequest() {
		return deliveryRequest;
	}

	public void setDeliveryRequest(DeliveryRequest deliveryRequest) {
		this.deliveryRequest = deliveryRequest;
	}

	public List<DeliveryItem> getDeliveryItemList() {
		if (deliveryItemList == null) {
			deliveryItemList = new ArrayList<DeliveryItem>();
		}
		return deliveryItemList;
	}

	public void setDeliveryItemList(List<DeliveryItem> deliveryItemList) {
		this.deliveryItemList = deliveryItemList;
	}
	
	public Hospital getHospital() {
		return hospital;
	}

	public void setHospital(Hospital hospital) {
		this.hospital = hospital;
	}

	public Integer getPrintedCount() {
		return printedCount;
	}

	public void setPrintedCount(Integer printedCount) {
		this.printedCount = printedCount;
	}
	
	public void updatePrintedCount() {
		int count = 0;
		for (DeliveryItem deliveryItem: deliveryItemList) {
			if (deliveryItem.getStatus() == DeliveryItemStatus.Printed) {
				count++;
			}
		}
		setPrintedCount(count);
	}

	public Integer getNumOfLabel() {
		return numOfLabel;
	}

	public void setNumOfLabel(Integer numOfLabel) {
		this.numOfLabel = numOfLabel;
	}

	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}

	public Date getCheckDate() {
		return checkDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getTrxIdDeliveryItem() {
		return trxIdDeliveryItem;
	}

	public void setTrxIdDeliveryItem(String trxIdDeliveryItem) {
		this.trxIdDeliveryItem = trxIdDeliveryItem;
	}
	
	public boolean isPivasBatch() {
		DeliveryItem deliveryItem = getDeliveryItemList().get(0);
		return deliveryItem != null && deliveryItem.getPivasWorklist() != null;
	}
}
