package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.persistence.reftable.Chest;
import hk.org.ha.model.pms.vo.drug.DmDrugLite;

import javax.ejb.Local;

@Local
public interface ChestServiceLocal {
	
	DmDrugLite retrieveChestDmDrugLite(String itemCode);
	
	Chest createChest();

	Chest retrieveChest(String unitDoseName);
	
	void updateChest(Chest chest);
	
	void deleteChest(Chest chest);
	
	boolean isRetrieveSuccess();
	
	boolean isSaveSuccess();
	
	void destroy();
	
}
