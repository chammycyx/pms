package hk.org.ha.model.pms.biz.label;

import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.corp.cache.DmFormDosageUnitMappingCacherInf;
import hk.org.ha.model.pms.corp.cache.DmFormVerbMappingCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDailyFrequency;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmFormDosageUnitMapping;
import hk.org.ha.model.pms.dms.persistence.DmFormVerbMapping;
import hk.org.ha.model.pms.dms.persistence.DmSupplFrequency;
import hk.org.ha.model.pms.dms.persistence.DmSupplSite;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.disp.RefillScheduleItem;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.label.ConvertDispLabelDescCriteria;
import hk.org.ha.model.pms.vo.label.Instruction;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.Freq;
import hk.org.ha.model.pms.vo.rx.Regimen;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("instructionBuilder")
@Scope(ScopeType.APPLICATION)
public class InstructionBuilder {
	
	private static final BigDecimal BD_0_3333 = new BigDecimal("0.3333");
	private static final BigDecimal BD_0_3330 = new BigDecimal("0.3330");
	private static final BigDecimal BD_0_333  = new BigDecimal("0.333");
	private static final BigDecimal BD_0_5000 = new BigDecimal("0.5000");
	
	private static final String LABEL_ZN_TW_PROPERTIES = "label_zh_TW.properties";
	
	@In(required = false)
	private DmFormDosageUnitMappingCacherInf dmFormDosageUnitMappingCacher;
	
	@In(required = false)
	private DmFormVerbMappingCacherInf dmFormVerbMappingCacher;
	
	@In
	private DispLabelDescConvertorInf dispLabelDescConvertor;
	
	private static Properties labelPropZhTW;
	
	private static final String SUPPL_FREQ_CODE_WEEKDAY = "00021";
	private static final String SPACE = " ";
	private static final String OPEN_BRACKET = "(";
	private static final String CLOSE_BRACKET = ")";
	private static final String NA = "n/a";
	private static final String LINE_BREAK_SEPARATOR = "^";
	private static final String COMMMA_SPACE = ", ";
	private static final String COMMMA_SPACE_WIHT_SEPARATOR = ",^ ";
	private static final String DECIMAL_FORMAT = "#######.####";

	private static final String DAY_ARRAY[] = {"mon","tue","wed","thu","fri","sat","sun"};
	private static final String NUM_ARRAY[] = {"zero","one","two","three","four","five","six","seven","eight","nine","ten"};
	private static final String ARABIC_DOSE_UNIT[] = {"oz","gram","inch","kg","KU","litre","MCI","MEQ","mg","ug","ml","mm","MMOL"};
	private static final String DISP_LABEL_INSTRUCTION_TYPE_ENG[] = {"engFormVerbDesc", "engSupplSiteDesc", "engAdminTimeDesc", "engDosageDesc", "engFreqDesc", "engSupplFreqDesc", "engPrnDesc", "engStepDurDesc", "engLineEndDesc"};
	private static final String DISP_LABEL_INSTRUCTION_TYPE_CHI[] = {"chiAdminTimeDesc", "chiFormVerbDesc", "chiSupplSiteDesc", "chiPrnDesc", "chiSupplFreqDesc", "chiFreqDesc", "chiDosageDesc", "chiStepDurDesc", "chiLineEndDesc"};
	private static final String REFILL_COUPON_INSTRUCTION_TYPE_ENG[] = {"engFormVerbDesc", "engAdminTimeDesc", "engSupplSiteDesc", "engDosageDesc", "engFreqDesc", "engSupplFreqDesc", "engPrnDesc"};
	private static final String DAY_ARRAY_FOR_LABEL_DESC[] = {"Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"};
	
	@Create
	public void create() throws IOException {
		synchronized (InstructionBuilder.class) {
			if (labelPropZhTW == null) {
				
				InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(LABEL_ZN_TW_PROPERTIES);
				if (inputStream == null) {
					inputStream = InstructionBuilder.class.getClassLoader().getResourceAsStream(LABEL_ZN_TW_PROPERTIES);
				}
				
				if (inputStream == null) {
					throw new IOException(LABEL_ZN_TW_PROPERTIES + " not found from classloaders!");
				}
				
				InputStreamReader isr = null;
				labelPropZhTW = new Properties();
				try {
					isr = new InputStreamReader(inputStream,"big5");
					labelPropZhTW.load(isr);
				} finally {
					if (isr != null) {
						isr.close();
					}
				}
			}
		}
	}
	
	public Properties getProperties() {
		return labelPropZhTW;
	}
	
	public List<Instruction> buildInstructionListForDispLabel(DispOrderItem dispOrderItem) {
		PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();

		Instruction instructionEn = new Instruction();
		instructionEn.setLang(PrintLang.Eng.getDataValue());
		instructionEn.setLineList(new ArrayList<String>());
		
		Instruction instructionZh = new Instruction();
		instructionZh.setLang(PrintLang.Chi.getDataValue());
		instructionZh.setLineList(new ArrayList<String>());
		
		Regimen regimen = pharmOrderItem.getRegimen();
		
		int maxDoseGroupNum = regimen.getDoseGroupList().size();
		int doseGroupCounter = 0;
		boolean mealTimeFlag = false;
		
		int doseLineCounter = 0;
		boolean gtThousand = false;
		
		for (DoseGroup doseGroup:regimen.getDoseGroupList()) {
			for (Dose dose:doseGroup.getDoseList()) {
				doseLineCounter++;
				if((dose.getDosage() != null && dose.getDosage().doubleValue() >= 1000) || 
					(dose.getDosageEnd() != null && dose.getDosageEnd().doubleValue() >= 1000)){
					gtThousand = true;
				}
			}
		}
		
		boolean arabicFlag = (doseLineCounter > 1 && gtThousand);
		
		for (DoseGroup doseGroup:regimen.getDoseGroupList()) {
			++doseGroupCounter;
			int doseCounter = 0;
			List<String> instructionLineListEn = new ArrayList<String>();
			List<String> instructionLineListZh = new ArrayList<String>();
			for (Dose dose:doseGroup.getDoseList()) {
				++doseCounter;

				Map<String, String> instructionLineMap = constructDispLabelInstructionLineMap(maxDoseGroupNum, 
																							 doseCounter,
																							 doseGroup,
																							 doseGroupCounter,
																							 dose,
																							 dispOrderItem,
																							 arabicFlag);
				
				instructionLineListEn.add(buildInstructionLine(instructionLineMap, DISP_LABEL_INSTRUCTION_TYPE_ENG));
				instructionLineListZh.add(buildInstructionLine(instructionLineMap, DISP_LABEL_INSTRUCTION_TYPE_CHI));
				
				if (!mealTimeFlag && !dispOrderItem.isLegacy() && dose.getDmAdminTimeLite() != null) {
					instructionEn.setMealTimeDesc(buildEngMealTimeDesc(dose, dispOrderItem.getPharmOrderItem().getFormCode(), true));
					instructionZh.setMealTimeDesc(buildChiMealTimeDesc(dose, dispOrderItem.getPharmOrderItem().getFormCode()));
				}
			}
			
			instructionEn.getLineList().addAll(updateLineListForFormVerbGrouping(instructionLineListEn, maxDoseGroupNum > 1, PrintLang.Eng.getDataValue()));
			instructionZh.getLineList().addAll(updateLineListForFormVerbGrouping(instructionLineListZh, maxDoseGroupNum > 1, PrintLang.Chi.getDataValue()));
		}
		
		List<Instruction> instructionList = new ArrayList<Instruction>();
		instructionList.add(updateInstructionFormat(instructionEn, maxDoseGroupNum));
		instructionList.add(updateInstructionFormat(instructionZh, maxDoseGroupNum));
		
		return instructionList;
	}
	
	private Map<String, String> constructDispLabelInstructionLineMap(
			int maxDoseGroupNum, int doseCounter, DoseGroup doseGroup, int doseGroupCounter, Dose dose, DispOrderItem dispOrderItem, boolean arabicFlag) {
		PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
		Regimen regimen = pharmOrderItem.getRegimen();
		int maxDoseNum = doseGroup.getDoseList().size();
		
		Map<String, String> instructionLineMap = new HashMap<String, String>();
			
		if (maxDoseGroupNum > 1 && maxDoseNum == doseCounter) {
			instructionLineMap.put("engStepDurDesc", getEngStepDurDesc(doseGroup, dispOrderItem.isLegacy()));
			instructionLineMap.put("chiStepDurDesc", getChiStepDurDesc(doseGroup, dispOrderItem.isLegacy()));
		}
		
		if (maxDoseGroupNum > 1 && maxDoseGroupNum == doseGroupCounter && maxDoseNum == doseCounter) {
			instructionLineMap.put("engLineEndDesc", ".");
			instructionLineMap.put("chiLineEndDesc", labelPropZhTW.getProperty("end"));
		} else if (maxDoseGroupNum > 1 && maxDoseGroupNum > doseGroupCounter && maxDoseNum == doseCounter){
			instructionLineMap.put("engLineEndDesc", "THEN");
			instructionLineMap.put("chiLineEndDesc", labelPropZhTW.getProperty("then"));
		} else if (maxDoseNum > 1 && maxDoseNum > doseCounter) {
			instructionLineMap.put("engLineEndDesc", "AND");
			instructionLineMap.put("chiLineEndDesc", labelPropZhTW.getProperty("and"));
		}

		if (doseGroup.getDoseList().get(0).getDmDailyFrequency() != null &&
			StringUtils.equals(doseGroup.getDoseList().get(0).getDmDailyFrequency().getDailyFreqCode(),"00039")) {
			instructionLineMap.put("chiFreqDesc", getChiFreqDesc(dose));
			instructionLineMap.put("engFreqDesc", getEngFreqDesc(dose));
			return instructionLineMap;
		}
		if ( isCapd(dispOrderItem) && OrderType.OutPatient == dispOrderItem.getDispOrder().getOrderType()) {
			instructionLineMap.put("engDosageDesc", getEngCapdDosageDesc(dose));
			instructionLineMap.put("chiDosageDesc", getChiCapdDosageDesc(dose));
		} else {
			if (dispOrderItem.isLegacy()) { // support non-integer dosage value
				if (dose.getLegacyDosage() != null) {
					instructionLineMap.put("engDosageDesc", getEngDosageDesc(dose, dispOrderItem));
				}
			} else {
				if (dose.getDosage() != null && StringUtils.isNotBlank(dose.getDosageUnit()) ) {
					instructionLineMap.put("engDosageDesc", getEngDosageDesc(dose, dispOrderItem));
					instructionLineMap.put("chiDosageDesc", getChiDosageDesc(dose, pharmOrderItem.getFormCode(), arabicFlag));
				}
			}
			
			ConvertDispLabelDescCriteria cld = new ConvertDispLabelDescCriteria();
			cld.setRegimen(regimen.getType().getDataValue());
			cld.setFreqCode(dose.getDailyFreq().getCode());

			if (dose.getDailyFreq().getParam() != null && dose.getDailyFreq().getParam().length > 0){
				cld.setFreqValue(dose.getDailyFreq().getParam()[0]);
			} else {
				cld.setFreqValue("0");
			}
			
			if ( dose.getSupplFreq() == null ) {
				cld.setSupplFreqCode(StringUtils.EMPTY);		
			} else {
				cld.setSupplFreqCode(dose.getSupplFreq().getCode());
			}

			cld.setEngSiteDesc(dose.getSiteDesc());

			dispLabelDescConvertor.checkConvertDispLabelDescRule(cld);

			if (cld.getChiConvFreqDesc() == null || StringUtils.equals(cld.getChiConvFreqDesc(), NA)) {
				instructionLineMap.put("chiFreqDesc", getChiFreqDesc(dose));	
			} else {
				instructionLineMap.put("chiFreqDesc", labelPropZhTW.getProperty(cld.getChiConvFreqDesc()));
			}
			
			if (cld.getEngConvFreqDesc() == null || StringUtils.equals(cld.getEngConvFreqDesc(), NA)) {
				instructionLineMap.put("engFreqDesc", getEngFreqDesc(dose));	
			} else {
				instructionLineMap.put("engFreqDesc", cld.getEngConvFreqDesc());
			}
			
			if (cld.getChiConvSupplFreqDesc() == null || StringUtils.equals(cld.getChiConvSupplFreqDesc(), NA)) {
				instructionLineMap.put("chiSupplFreqDesc", getChiSupplFreqDesc(dose));
			} else {
				instructionLineMap.put("chiSupplFreqDesc", labelPropZhTW.getProperty(cld.getChiConvSupplFreqDesc()));
			}
			
			if (cld.getEngConvSupplFreqDesc() == null || StringUtils.equals(cld.getEngConvSupplFreqDesc(), NA)) {
				instructionLineMap.put("engSupplFreqDesc", getEngSupplFreqDesc(dose, true));
			} else {
				instructionLineMap.put("engSupplFreqDesc", cld.getEngConvSupplFreqDesc());
			}
			
			if (cld.getChiConvSiteDesc() == null || StringUtils.equals(cld.getChiConvSiteDesc(), NA)) {
				instructionLineMap.put("chiSupplSiteDesc", getChiSupplSiteDesc(dose));	
			} else {
				instructionLineMap.put("chiSupplSiteDesc", labelPropZhTW.getProperty(cld.getChiConvSiteDesc()));
			}
			
			if (cld.getEngConvSiteDesc() == null || StringUtils.equals(cld.getEngConvSiteDesc(), NA)) {
				instructionLineMap.put("engSupplSiteDesc", getEngSupplSiteDesc(dose));	
			} else {
				instructionLineMap.put("engSupplSiteDesc", cld.getEngConvSiteDesc());
			}
		}
		
		if (dose.getDmAdminTimeLite() != null) {
			instructionLineMap.put("engAdminTimeDesc", dose.getDmAdminTimeLite().getAdminTimeEng());
			instructionLineMap.put("chiAdminTimeDesc", dose.getDmAdminTimeLite().getAdminTimeChi());
		}
		
		instructionLineMap.put("engPrnDesc", getEngPrnDesc(dose));
		instructionLineMap.put("chiPrnDesc", getChiPrnDesc(dose));
		
		if (dispOrderItem.isLegacy()) {
			instructionLineMap.put("engFormVerbDesc", dose.getFormVerb());
		} else {
			instructionLineMap.put("engFormVerbDesc", getEngFormVerbDesc(pharmOrderItem.getFormCode(), dose));
			instructionLineMap.put("chiFormVerbDesc", getChiFormVerbDesc(pharmOrderItem.getFormCode(), dose));	
		}

		return instructionLineMap;
	}
	
	private String buildInstructionLine(Map<String,String> instructionLineMap, String[] descTypeArray) {
		StringBuilder instructionLine = new StringBuilder();
		int counter = 0;
		
		for (String descType:descTypeArray) {
			if (counter++ == 3) {
				instructionLine.append("|");
			}
			
			if (StringUtils.isNotBlank(instructionLineMap.get(descType))) {
				if (instructionLine.length()>0 && 
					!StringUtils.equals(instructionLine.toString(), "|") && 
					!StringUtils.equals(instructionLineMap.get(descType), ".") &&
					!"chiStepDurDesc".equals(descType) &&
					!"chiLineEndDesc".equals(descType)){
					
					instructionLine.append(LINE_BREAK_SEPARATOR);
				}
				
				instructionLine.append(instructionLineMap.get(descType));
			}
		}
		
		return instructionLine.toString();
	}
	
	private List<String> updateLineListForFormVerbGrouping(List<String> lineList, boolean isStepUpDown, String lang){
		List<String> groupTypeLineList = new ArrayList<String>();
		List<String> normalTypeLineList = new ArrayList<String>();
		String verb = StringUtils.EMPTY;
		boolean isSameVerb = true;
		boolean isEn = PrintLang.Eng.getDataValue().equals(lang);
		for (String line:lineList) {
			//prepare normal type lineList
			normalTypeLineList.add(StringUtils.remove(line,'|'));

			//prepare group type lineList
			String lineArray[] = line.split("\\|");
			if (lineArray.length == 0) {
				continue;
			}
			if (lineArray.length > 1 && ("STOP".equals(StringUtils.left(lineArray[1], 4)) ||
					labelPropZhTW.getProperty("stop").equals(StringUtils.left(lineArray[1], 2)))) {
				isSameVerb = false;
			} else if (StringUtils.isBlank(verb)) {
				verb = lineArray[0];
				groupTypeLineList.add(lineArray[0].concat(LINE_BREAK_SEPARATOR));
			} else if (!StringUtils.equals(verb, lineArray[0])) {
				isSameVerb = false;
			}
			if (lineArray.length > 1) {
				if (isEn && StringUtils.isNotBlank(verb)) {
					groupTypeLineList.add("@".concat(lineArray[1]));					
				} else {
					groupTypeLineList.add(lineArray[1]);
				}
			}
		}
		
		if ((isStepUpDown || lineList.size() > 1) && isSameVerb) {
			return groupTypeLineList;
		} else {
			return normalTypeLineList;
		}
	}
	
	private Instruction updateInstructionFormat(Instruction instruction, int stepNum) {
		if (stepNum == 1) {
			return instruction;
		}
		
		boolean isSameVerb = true;
		String formVerb = null;
		int groupedFormVerbCounter = 0;

		for (String line:instruction.getLineList()) {
			if (line.endsWith(LINE_BREAK_SEPARATOR)) {
				groupedFormVerbCounter++;
				if (formVerb == null) {
					formVerb = line;
				} else if (!StringUtils.equals(formVerb,line)) {
					isSameVerb = false;
				}
			} else if ("STOP".equals(StringUtils.left(line, 4)) ||
					labelPropZhTW.getProperty("stop").equals(StringUtils.left(line, 2))){
				groupedFormVerbCounter++;
			}
		}
		if (groupedFormVerbCounter == stepNum && isSameVerb) {
			while(instruction.getLineList().contains(formVerb)) {
				instruction.getLineList().remove(formVerb);
			}
			instruction.getLineList().add(0, formVerb);
		}
		
		return instruction;
	}
	
	public String buildInstructionListForRefillCoupon(RefillScheduleItem refillScheduleItem) {		
		Instruction instructionEn = new Instruction();
		instructionEn.setLang(PrintLang.Eng.getDataValue());
		StringBuilder instructionListEn = new StringBuilder();
		
		PharmOrderItem pharmOrderItem = refillScheduleItem.getPharmOrderItem();
		Regimen regimen = pharmOrderItem.getRegimen();
		
		regimen.loadDmInfo();
		
		for (DoseGroup doseGroup:regimen.getDoseGroupList()) {
			int doseCounter = 0;			
			
			for (Dose dose:doseGroup.getDoseList()) {
				++doseCounter;
				Map<String, String> instructionLineMap = constructRefillCouponInstructionMap(doseGroup, dose, pharmOrderItem );
								
				StringBuilder instructionLine = new StringBuilder();
				
				for (String descType:REFILL_COUPON_INSTRUCTION_TYPE_ENG) {
					if (StringUtils.isNotBlank(instructionLineMap.get(descType))) {
						if (instructionLine.length()>0) {
							instructionLine.append(SPACE);
						}
						instructionLine.append(instructionLineMap.get(descType));							
					}
				}
				
				if( doseCounter == 1 ){
					instructionLine.append(" <FOR ");
					instructionLine.append(refillScheduleItem.getRefillDuration());
					instructionLine.append(" DAYS>");
					instructionListEn.append(instructionLine.toString());
				} else{
					instructionListEn.append(instructionLine.toString());
				}
				
				instructionListEn.append("\n");				
			}
		}
		
		return instructionListEn.toString();
	}
	
	private Map<String, String> constructRefillCouponInstructionMap(DoseGroup doseGroup, Dose dose, PharmOrderItem pharmOrderItem) {
		Map<String, String> instructionLineMap = new HashMap<String, String>();
		
		if (StringUtils.equals(doseGroup.getDoseList().get(0).getDmDailyFrequency().getDailyFreqCode(),"00039")) {					
			instructionLineMap.put("engFreqDesc", getEngFreqDesc(dose));					
			return instructionLineMap;
		}
		
		instructionLineMap.put("engDosageDesc", getEngRefillCouponDosageDesc(dose, doseGroup.getDoseList().size() > 1));				
		
		ConvertDispLabelDescCriteria cld = new ConvertDispLabelDescCriteria();
		cld.setRegimen(pharmOrderItem.getRegimen().getType().getDataValue());
		cld.setFreqCode(dose.getDailyFreq().getCode());
		
		if (dose.getDailyFreq().getParam() != null && dose.getDailyFreq().getParam().length > 0){
			cld.setFreqValue(dose.getDailyFreq().getParam()[0]);
		} else {
			cld.setFreqValue("0");
		}
		
		cld.setSupplFreqCode(dose.getSupplFreq()==null?StringUtils.EMPTY:dose.getSupplFreq().getCode());
		cld.setEngSiteDesc(dose.getSiteDesc());

		dispLabelDescConvertor.checkConvertDispLabelDescRule(cld);

		if (cld.getEngConvFreqDesc() == null || StringUtils.equals(cld.getEngConvFreqDesc(), NA)) {
			instructionLineMap.put("engFreqDesc", getEngFreqDesc(dose));	
		} else {
			instructionLineMap.put("engFreqDesc", cld.getEngConvFreqDesc());
		}
		
		if (cld.getEngConvSupplFreqDesc() == null || StringUtils.equals(cld.getEngConvSupplFreqDesc(), NA)) {
			instructionLineMap.put("engSupplFreqDesc", getEngSupplFreqDesc(dose, true));
		} else {
			instructionLineMap.put("engSupplFreqDesc", cld.getEngConvSupplFreqDesc());
		}
		
		if (cld.getEngConvSiteDesc() == null || StringUtils.equals(cld.getEngConvSiteDesc(), NA)) {
			instructionLineMap.put("engSupplSiteDesc", getEngSupplSiteDesc(dose));	
		} else {
			instructionLineMap.put("engSupplSiteDesc", cld.getEngConvSiteDesc());
		}
		
		
		if (dose.getDmAdminTimeLite() != null) {
			instructionLineMap.put("engAdminTimeDesc", dose.getDmAdminTimeLite().getAdminTimeEng());					
		} 
		
		StringBuilder prnDesc = new StringBuilder();
		if( dose.getPrn() ){
			prnDesc.append("PRN[").append(dose.getPrnPercent().getDisplayValue()).append("]");
		}
		
		instructionLineMap.put("engPrnDesc", prnDesc.toString());
		instructionLineMap.put("engFormVerbDesc", getEngFormVerbDesc(pharmOrderItem.getFormCode(), dose));

		return instructionLineMap;
	}
	
	private String buildEngMealTimeDesc(Dose dose, String formCode, boolean lengthCheck) {
		StringBuilder mealTimeDesc = new StringBuilder();
		String formVerbDesc = getEngFormVerbDesc(formCode, dose);
		String dmAdminTime = StringUtils.lowerCase(dose.getDmAdminTimeLite().getAdminTimeEng());
		
		if( StringUtils.isNotBlank(formVerbDesc) ){
			formVerbDesc = StringUtils.capitalize(StringUtils.lowerCase(formVerbDesc));
			mealTimeDesc.append(formVerbDesc);
			if( lengthCheck && (mealTimeDesc.length() + dmAdminTime.length() + 1 > 45) ){
				mealTimeDesc.append("\n");
			}else{
				mealTimeDesc.append(SPACE);
			}
		}
		
		mealTimeDesc.append(dmAdminTime);

		return mealTimeDesc.toString();
	}
	
	private String buildChiMealTimeDesc(Dose dose, String formCode) {
		StringBuilder mealTimeDesc = new StringBuilder();
		mealTimeDesc.append(dose.getDmAdminTimeLite().getAdminTimeChi());
		String formVerbDesc = getChiFormVerbDesc(formCode, dose);
		if( StringUtils.isNotBlank(formVerbDesc) ){
			mealTimeDesc.append(formVerbDesc);
		}
		return mealTimeDesc.toString();
	}
	
	private String getEngPrnDesc(Dose dose) {
		if (dose.getPrn()) {
			return "WHEN NECESSARY";
		} else {
			return StringUtils.EMPTY;
		}
	}
	
	private String getChiPrnDesc(Dose dose) {		
		if (dose.getPrn()) {
			return labelPropZhTW.getProperty("necessary");
		} else {
			return StringUtils.EMPTY;
		}
	}
	
	private String getEngFormVerbDesc(String formCode, Dose dose) {
		DmFormVerbMapping dmFormVerbMapping = dmFormVerbMappingCacher.getDmFormVerbMappingByFormCodeSiteCode(formCode, dose.getSiteCode());
		if (dmFormVerbMapping != null) {
			return dmFormVerbMapping.getVerbEng();	
		} else {
			return null;
		}
	}
	
	private String getChiFormVerbDesc(String formCode, Dose dose) {
		DmFormVerbMapping dmFormVerbMapping = dmFormVerbMappingCacher.getDmFormVerbMappingByFormCodeSiteCode(formCode, dose.getSiteCode());
		if (dmFormVerbMapping != null) {
			return dmFormVerbMapping.getVerbChi();	
		} else {
			return null;
		}
	}
	
	private String getEngSupplSiteDesc(Dose dose) {
		if (StringUtils.isBlank(dose.getSupplSiteDesc())) {
			if (dose.getDmSite() == null || StringUtils.isBlank(dose.getDmSite().getSiteDescEng())) {
				return StringUtils.EMPTY;
			} else if (dose.getDmSite().getSiteDescEng().equals("INTRATHECALLY")){
				return "**" + dose.getDmSite().getSiteDescEng() + "**";	
			} else {
				return dose.getDmSite().getSiteDescEng();
			}
		} else {
			return dose.getSupplSiteDesc();
		}
	}
	
	private String getChiSupplSiteDesc(Dose dose) {
		DmSupplSite dmSupplSite = dose.getDmSupplSite();
		if (dmSupplSite == null || StringUtils.isBlank(dmSupplSite.getSupplSiteChi())) {
			if (dose.getDmSite() == null || dose.getDmSite().getSiteDescChi() == null) {
				return StringUtils.EMPTY;
			} else if (dose.getDmSite().getSiteDescChi().equals(labelPropZhTW.getProperty("intrathecally"))){
				return "**" + dose.getDmSite().getSiteDescChi() + "**";
			} else {
				return dose.getDmSite().getSiteDescChi();
			}
		} else {
			return dmSupplSite.getSupplSiteChi();
		}
	}
	
	private String getEngFreqDesc(Dose dose) {
		DmDailyFrequency dmDailyFrequency = dose.getDmDailyFrequency();

		if (dmDailyFrequency == null) {
			return StringUtils.EMPTY;
		}
		
		StringBuilder freqDesc = new StringBuilder();
		
		if (StringUtils.isNotBlank(dmDailyFrequency.getLabelFreqBlk1Eng())) {
			freqDesc.append(dmDailyFrequency.getLabelFreqBlk1Eng()).append(SPACE);
		}
		
		if (dmDailyFrequency.getNumOfInputValue() != null && dmDailyFrequency.getNumOfInputValue() > 0 && 
				dose.getDailyFreq() != null && dose.getDailyFreq().getParam() != null) {
			for (String freqValue:dose.getDailyFreq().getParam()) {
				if (StringUtils.isNotBlank(freqValue)) {
					freqDesc.append(freqValue).append(SPACE);
				}
			}
		}
		
		if (StringUtils.isNotBlank(dmDailyFrequency.getLabelFreqBlk2Eng())) {
			freqDesc.append(dmDailyFrequency.getLabelFreqBlk2Eng()).append(SPACE);
		}

		if (freqDesc.length() == 0) {
			return StringUtils.EMPTY;
		} else {
			return freqDesc.deleteCharAt(freqDesc.length()-1).toString();
		}
	}
	
	private String getChiFreqDesc(Dose dose) {
		StringBuilder freqDesc = new StringBuilder();
		DmDailyFrequency dmDailyFrequency = dose.getDmDailyFrequency();
		
		if (dmDailyFrequency == null) {
			return StringUtils.EMPTY;
		}
		
		if (StringUtils.isNotBlank(dmDailyFrequency.getLabelFreqBlk1Chi())) {
			freqDesc.append(dmDailyFrequency.getLabelFreqBlk1Chi());
		}
		
		if (dmDailyFrequency.getNumOfInputValue() != null && dmDailyFrequency.getNumOfInputValue() > 0 && 
				dose.getDailyFreq() != null	&& dose.getDailyFreq().getParam() != null) {
			for (String freqValue:dose.getDailyFreq().getParam()) {
				if (StringUtils.isNotBlank(freqValue)) {
					if (isNum(freqValue)) {
						if (!dmDailyFrequency.getLabelFreqBlk1Chi().equals(labelPropZhTW.getProperty("morning")) && 
						    !dmDailyFrequency.getLabelFreqBlk1Chi().equals(labelPropZhTW.getProperty("afternoon")) &&
						    Integer.parseInt(freqValue) == 2) {
							   freqDesc.append(labelPropZhTW.getProperty("dosageTwo"));
						   } else {
							   freqDesc.append(integerToChi(Integer.parseInt(freqValue)));
						   }
					} else {
						freqDesc.append(freqValue);
					}
				}
			}
		}
		
		if (StringUtils.isNotBlank(dmDailyFrequency.getLabelFreqBlk2Chi())) {
			freqDesc.append(dmDailyFrequency.getLabelFreqBlk2Chi());
		}
		
		return freqDesc.toString();
	}
	
	private String getEngSupplFreqDesc(Dose dose, boolean addBracket) {
		DmSupplFrequency dmSupplFrequency = dose.getDmSupplFrequency();
		
		if (dmSupplFrequency == null || dmSupplFrequency.getSupplFreqCode() == null) {
			return StringUtils.EMPTY;
		}
		
		if (dmSupplFrequency.getSupplFreqCode().equals(SUPPL_FREQ_CODE_WEEKDAY)) {
			return getEngSupplFreqDescForWeekday(dose.getSupplFreq().getParam()[0]);
		}
		
		StringBuilder supplFreqDesc = new StringBuilder();
		
		if (StringUtils.isNotBlank(dmSupplFrequency.getLabelFreqBlk1Eng())) {
			supplFreqDesc.append(dmSupplFrequency.getLabelFreqBlk1Eng()).append(SPACE);
		}
		
		if (dmSupplFrequency.getNumOfInputValue() > 0 && dose.getSupplFreq().getParam() != null 
				&& dose.getSupplFreq().getParam().length > 0) {
			supplFreqDesc.append(dose.getSupplFreq().getParam()[0]).append(SPACE);
		}
		
		if (StringUtils.isNotBlank(dmSupplFrequency.getLabelFreqBlk2Eng())) {
			supplFreqDesc.append(dmSupplFrequency.getLabelFreqBlk2Eng()).append(SPACE);
		}
		
		if (dmSupplFrequency.getNumOfInputValue() > 1 && dose.getSupplFreq().getParam() != null 
				&& dose.getSupplFreq().getParam().length > 1) {
			supplFreqDesc.append(dose.getSupplFreq().getParam()[1]).append(SPACE);
		}
		
		if (StringUtils.isNotBlank(dmSupplFrequency.getLabelFreqBlk3Eng())) {
			supplFreqDesc.append(dmSupplFrequency.getLabelFreqBlk3Eng()).append(SPACE);
		}

		if(supplFreqDesc.length()==0) {
			return StringUtils.EMPTY;
		} else if (addBracket) {
			supplFreqDesc.insert(0, OPEN_BRACKET);
			supplFreqDesc.deleteCharAt(supplFreqDesc.length()-1).append(CLOSE_BRACKET);
		
			return supplFreqDesc.toString();
		} else {
			supplFreqDesc.deleteCharAt(supplFreqDesc.length()-1);
			return supplFreqDesc.toString();
		}
	}
	
	private String getEngSupplFreqDescForWeekday(String param) {

		StringBuilder supplFreqDesc = new StringBuilder();
		supplFreqDesc.append(OPEN_BRACKET).append("ON EVERY").append(SPACE);
		
		boolean firstDay = true;
		int checkIndex;
		for (int i = 0; i < param.length(); i++) {
			checkIndex = (i+1)%7;
			if (param.charAt(checkIndex) == '1') {
				if (!firstDay) {
					supplFreqDesc.append(COMMMA_SPACE);
				} else {
					firstDay = false;
				}
				supplFreqDesc.append(DAY_ARRAY[i]);
			}
		}
		
		supplFreqDesc.append(CLOSE_BRACKET);
		
		return supplFreqDesc.toString().toUpperCase();
	}
	
	private String getChiSupplFreqDesc(Dose dose) {
		DmSupplFrequency dmSupplFrequency;
		if (dose.getDmSupplFrequency() == null) {
			return StringUtils.EMPTY;
		} else {
			dmSupplFrequency = dose.getDmSupplFrequency();
		}
		
		if (dmSupplFrequency.getSupplFreqCode() == null) {
			return StringUtils.EMPTY;
		}
		
		if (dmSupplFrequency.getSupplFreqCode().equals("00021")) {
			return getChiSupplFreqDescForWeekday(dose.getSupplFreq().getParam()[0]);
		}
		
		StringBuilder supplFreqDesc = new StringBuilder(); 
			
		supplFreqDesc.append(OPEN_BRACKET);
		
		if (StringUtils.isNotBlank(dmSupplFrequency.getLabelFreqBlk1Chi())) {
			supplFreqDesc.append(dmSupplFrequency.getLabelFreqBlk1Chi());
		}
		
		if (dmSupplFrequency.getNumOfInputValue() > 0 && dose.getSupplFreq().getParam() != null && dose.getSupplFreq().getParam().length > 0) {
			if (!supplFreqDesc.toString().endsWith(labelPropZhTW.getProperty("the")) && dose.getSupplFreq().getParam()[0].equals("2")) {
				supplFreqDesc.append(labelPropZhTW.getProperty("dosageTwo"));
			} else {
				if (isNum(dose.getSupplFreq().getParam()[0])) {
					supplFreqDesc.append(integerToChi(Integer.parseInt(dose.getSupplFreq().getParam()[0])));
				} else {
					supplFreqDesc.append(dose.getSupplFreq().getParam()[0]);
				}
			}
		}
		
		if (StringUtils.isNotBlank(dmSupplFrequency.getLabelFreqBlk2Chi())) {
			supplFreqDesc.append(dmSupplFrequency.getLabelFreqBlk2Chi());
		}
		
		if (dmSupplFrequency.getNumOfInputValue() > 1 && dose.getSupplFreq().getParam() != null && dose.getSupplFreq().getParam().length > 1) {
			if (!supplFreqDesc.toString().endsWith(labelPropZhTW.getProperty("the")) && dose.getSupplFreq().getParam()[1].equals("2")) {
				supplFreqDesc.append(labelPropZhTW.getProperty("dosageTwo"));
			} else {
				if (isNum(dose.getSupplFreq().getParam()[1])) {
					supplFreqDesc.append(integerToChi(Integer.parseInt(dose.getSupplFreq().getParam()[1])));
				} else {
					supplFreqDesc.append(dose.getSupplFreq().getParam()[1]);
				}
			}

		}
		
		if (StringUtils.isNotBlank(dmSupplFrequency.getLabelFreqBlk3Chi())) {
			supplFreqDesc.append(dmSupplFrequency.getLabelFreqBlk3Chi());
		}
		
		supplFreqDesc.append(CLOSE_BRACKET);
		
		return supplFreqDesc.toString();
	}
	
	private String getChiSupplFreqDescForWeekday(String param) {
		StringBuilder supplFreqDesc = new StringBuilder();
		
		supplFreqDesc.append(OPEN_BRACKET).append(labelPropZhTW.getProperty("forWeek"));
		
		boolean firstDay = true;
		int checkIndex;
		
		for (int i = 0; i < param.length(); i++) {
			checkIndex = (i+1)%7;
			if (param.charAt(checkIndex) == '1') {
				if (!firstDay) {
					supplFreqDesc.append(labelPropZhTW.getProperty("smallComma"));
				} else {
					firstDay = false;
				}
				supplFreqDesc.append(labelPropZhTW.getProperty(DAY_ARRAY[i]));
			}
		}
		supplFreqDesc.append(labelPropZhTW.getProperty("onTheDay") + CLOSE_BRACKET);
		
		return supplFreqDesc.toString();
	}

	private String getChiDosageDesc(Dose dose, String formCode, boolean arabicFlag) {
		DmFormDosageUnitMapping dosageUnitMapping = dmFormDosageUnitMappingCacher.getDmFormDosageUnitMappingByFormCodeDosageUnit(formCode, dose.getDosageUnit());
		StringBuilder dosageDesc = new StringBuilder();
		String ddu = StringUtils.EMPTY;
		
		if (dosageUnitMapping != null) {
			ddu = dosageUnitMapping.getDosageUnitChi();
		}
		
		if (BigDecimal.ZERO.compareTo(dose.getDosage()) == 0 || StringUtils.isBlank(ddu)) {
			return StringUtils.EMPTY;
		} else {
			boolean halfTabletFlag = checkHalfTablet(dose.getDosage(), ddu) || checkHalfTablet(dose.getDosageEnd(), ddu);
			dosageDesc.append(COMMMA_SPACE_WIHT_SEPARATOR).append(LINE_BREAK_SEPARATOR).append(labelPropZhTW.getProperty("everytime"));
			dosageDesc.append(getChiDosageValueDesc(dose.getDosage(), ddu, arabicFlag, halfTabletFlag));
			if (dose.getDosageEnd() != null && dose.getDosageEnd().doubleValue() > 0.0) {
				dosageDesc.append(labelPropZhTW.getProperty("to")).append(getChiDosageValueDesc(dose.getDosageEnd(),ddu, arabicFlag, halfTabletFlag));
			}
			if (!halfTabletFlag || arabicFlag) {
				dosageDesc.append(ddu);
			}
		}
		
		return dosageDesc.toString();
	}
	
	private String getChiDosageValueDesc(BigDecimal dosage, String ddu, boolean arabicFlag, boolean halfTabletFlag) {
		DecimalFormat df = new DecimalFormat(DECIMAL_FORMAT);
		
		StringBuilder dosageDesc = new StringBuilder();
		if (arabicFlag) {
			dosageDesc.append(df.format(dosage).toString());
		} else if (StringUtils.equals(ddu, labelPropZhTW.getProperty("pack")) || StringUtils.equals(ddu, labelPropZhTW.getProperty("bag"))) {
			if ( checkOneThird(dosage) ) {
				dosageDesc.append(labelPropZhTW.getProperty("oneOverThree"));
			} else {
				dosageDesc.append(dosageToChi(dosage, ddu));
			}
		} else if (chkDoseUnitForUseArabic(ddu)) {
			dosageDesc.append("<").append(df.format(dosage).toString()).append(">");
		} else {
			dosageDesc.append(dosageToChi(dosage, ddu));
			if (halfTabletFlag && !dosageDesc.toString().contains(labelPropZhTW.getProperty("tablet"))) {
				dosageDesc.append(ddu);
			}
		}
		
		return dosageDesc.toString();
	}
	
    // Please note that CDDH has called this function in DA by custom created DmDrug, please confirm with Stephen Hui when changing this function
    private boolean isCapd(DispOrderItem dispOrderItem){
		PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
		if ( dispOrderItem.isLegacy() ) {
			return pharmOrderItem.getCapdVoucherFlag();               
		}

		DmDrug dmDrug = DmDrugCacher.instance().getDmDrug(pharmOrderItem.getItemCode());
		return dmDrug != null && StringUtils.equals(dmDrug.getTheraGroup(), "PDF");
    }
	
	private String getEngDosageDesc(Dose dose, DispOrderItem dispOrderItem) {
		StringBuilder dosageDesc = new StringBuilder();	
		String ddu = dose.getDosageUnit();
		
		// prepare for dosage unit when legacy
		if (dispOrderItem.isLegacy()) {
			if (OrderType.OutPatient == dispOrderItem.getDispOrder().getPharmOrder().getMedOrder().getOrderType()) {
				if ("PN".equals(dispOrderItem.getItemCatCode()) ||
						"CI".equals(dispOrderItem.getItemCatCode()) ||
						"CY".equals(dispOrderItem.getItemCatCode()) ||
						isCapd(dispOrderItem) ) {
					ddu = dispOrderItem.getPharmOrderItem().getBaseUnit();
				}
			} 
		}

		if ("5ML SPOONFUL(S)".equals(ddu)) {
			ddu = "x " + ddu;
		}
		
		// prepare for dosage
		if (dispOrderItem.isLegacy()) {
			dosageDesc.append(dose.getLegacyDosage());
			if ( StringUtils.isNotBlank(ddu) ) {
				dosageDesc.append(SPACE).append(ddu);
			}
		} else {
			if (dose.getDosage().doubleValue() > 0.0 && StringUtils.isNotBlank(ddu)) {
				dosageDesc.append(getEngDosageValueDesc(dose.getDosage(), ddu));
				if (dose.getDosageEnd() != null && dose.getDosageEnd().doubleValue() > 0.0) {
					dosageDesc.append(SPACE).append("TO").append(SPACE).append(getEngDosageValueDesc(dose.getDosageEnd(), ddu));
				}
				dosageDesc.append(SPACE).append(ddu);
			}
		}
		
		return dosageDesc.toString();
	}
	
	private String getEngDosageValueDesc(BigDecimal dosage, String ddu) {
		DecimalFormat df = new DecimalFormat(DECIMAL_FORMAT);
		
		StringBuilder dosageValueDesc = new StringBuilder();
		if ( ( dosage.setScale(4, RoundingMode.DOWN).equals(BD_0_5000)) &&
				(ddu.equals("BAG(S)") || ddu.equals("PACK(S)") || ddu.equals("SACHET(S)") || ddu.equals("TABLET(S)"))) {
			dosageValueDesc.append("HALF");
		} else {
			if ( checkOneThird(dosage) &&
				(ddu.equals("BAG(S)") || ddu.equals("PACK(S)") || ddu.equals("SACHET(S)"))) {
				dosageValueDesc.append("<1/3>");
			} else {
				dosageValueDesc.append("<").append(df.format(dosage).toString()).append(">");
			}
		}
		
		return dosageValueDesc.toString();
	}
	
	private String getEngRefillCouponDosageDesc(Dose dose, boolean isMultipleDose) {
		DecimalFormat df = new DecimalFormat(DECIMAL_FORMAT);
		
		StringBuilder dosageDesc = new StringBuilder();
		String ddu = dose.getDosageUnit();

		if ("5ML SPOONFUL(S)".equals(ddu)){
			ddu = "x " + ddu;
		}
		
		if( dose.getDosage() != null ){
			if (dose.getDosage().doubleValue() > 0.0 && StringUtils.isNotBlank(ddu)) {
				if (isMultipleDose) {
					dosageDesc.append("[").append(df.format(dose.getDosage()).toString()).append("]");
				} else {
					dosageDesc.append(df.format(dose.getDosage()).toString());
				}
				dosageDesc.append(SPACE).append(ddu);
			}
		}
		
		return dosageDesc.toString();
	}
	
	private String getEngCapdDosageDesc(Dose dose) {
		DecimalFormat df = new DecimalFormat(DECIMAL_FORMAT);

		if ( dose.getDosage() == null ) {
			return " DAILY"; 
		}
		if (dose.getDosage().compareTo(BigDecimal.ONE) == 0 ) {
			return "<" + df.format(dose.getDosage()).toString() + "> BAG DAILY"; 
		} else {
			return "<" + df.format(dose.getDosage()).toString() + "> BAG(S) DAILY";
		}
	}

	private String getChiCapdDosageDesc(Dose dose) {
		if ( dose.getDosage() == null ) {
			return labelPropZhTW.getProperty("everyday"); 
		}
		return labelPropZhTW.getProperty("everyday") + dosageToChi(dose.getDosage(), StringUtils.EMPTY) + labelPropZhTW.getProperty("pack"); 
	}
	
	private String getEngStepDurDesc(DoseGroup doseGroup, boolean legacy) {
		if (legacy) {
			return "FOR " + doseGroup.getDuration() + SPACE + doseGroup.getDurationUnit().getDisplayValue();
		} else {
			if (doseGroup != null && doseGroup.getDmRegimen() != null){
				String stepDur;
				stepDur = String.valueOf(doseGroup.getDuration() * doseGroup.getDmRegimen().getRegimenMultiplier());
				return "FOR " + stepDur + " DAY(S)";
			} else {
				return "";
			}
		}
	}
	
	private String getChiStepDurDesc(DoseGroup doseGroup, boolean legacy) {
		if (legacy) {
			return StringUtils.EMPTY;
		} else {
			if (doseGroup != null && doseGroup.getDmRegimen() != null){
				String stepDur;
				stepDur = String.valueOf(doseGroup.getDuration() * doseGroup.getDmRegimen().getRegimenMultiplier());
				return COMMMA_SPACE_WIHT_SEPARATOR + labelPropZhTW.getProperty("forDur") + stepDur + labelPropZhTW.getProperty("day");
			} else {
				return "";
			}
		}
		
	}
	
	private boolean checkHalfTablet(BigDecimal dosage, String ddu) {
		if (dosage == null) {
			return false;
		}
		double decimalValue =  dosage.doubleValue() - dosage.intValue();
		return StringUtils.equals(ddu, labelPropZhTW.getProperty("tablet")) && 
			   dosage.compareTo(BigDecimal.TEN) == -1 && 
			   decimalValue != 0 && decimalValue % 0.5 == 0;
	}
	
	private String dosageToChi(BigDecimal dosage, String ddu) {
		DecimalFormat df = new DecimalFormat(DECIMAL_FORMAT);
		
		StringBuilder dosageDesc = new StringBuilder();
	
		double decimalValue =  dosage.doubleValue() - dosage.intValue();
		if (decimalValue == 0 && dosage.intValue() == 2) {
			dosageDesc.append(labelPropZhTW.getProperty("dosageTwo"));
		} else if (dosage.compareTo(BigDecimal.valueOf(0.5))==0) {
			dosageDesc.append(labelPropZhTW.getProperty("dosageHalf"));
		} else if (decimalValue != 0 && decimalValue % 0.25 != 0) {
			dosageDesc.append(df.format(dosage).toString());
		} else if (checkHalfTablet(dosage, ddu)) {
			if (dosage.compareTo(BigDecimal.valueOf(2.5))==0) {
				dosageDesc.append(labelPropZhTW.getProperty("dosageTwo")).append(ddu).append(labelPropZhTW.getProperty("dosageHalf"));
			} else {
				dosageDesc.append(integerToChi(dosage.intValue())).append(ddu).append(labelPropZhTW.getProperty("dosageHalf"));
			}
		} else if (dosage.intValue() > 0 && decimalValue > 0) {
			if (dosage.doubleValue() >= 1000) {
				dosageDesc.append(df.format(dosage.doubleValue()).toString());
			}else{
				dosageDesc.append(integerToChi(dosage.intValue())).append(labelPropZhTW.getProperty("with")).append(decimalToChi(decimalValue));
			}
		} else if (dosage.intValue() > 0) {
			dosageDesc.append(integerToChi(dosage.intValue()));
		} else {
			dosageDesc.append(decimalToChi(decimalValue));
		}
		
		return dosageDesc.toString();
	}
	
	private String integerToChi(Integer inputNum) {
		DecimalFormat df = new DecimalFormat(DECIMAL_FORMAT);
		
		StringBuilder chiNum = new StringBuilder();
		
		if (inputNum >= 1000) {
			chiNum.append(df.format(inputNum).toString());
		} else {
			Integer hundredDigit = inputNum.intValue() / 100;
			Integer tenDigit = inputNum.intValue() % 100 / 10;
			Integer unitDigit = inputNum.intValue() % 10;
			
			if (hundredDigit > 0) {
				chiNum.append(digitToChi(hundredDigit))
					  .append(labelPropZhTW.getProperty("hundred"));
				if (tenDigit == 0 && unitDigit > 0) {
					chiNum.append(labelPropZhTW.getProperty("zero"));
				}
			}
			
			if (tenDigit > 0) {
				if (tenDigit == 1 && hundredDigit == 0) {
					chiNum.append(labelPropZhTW.getProperty("ten"));
				} else {
					chiNum.append(digitToChi(tenDigit))
						  .append(labelPropZhTW.getProperty("ten"));
				}
			}
			
			chiNum.append(digitToChi(unitDigit)); 
		}
		
		return chiNum.toString();
	}
	
	private String digitToChi(Integer inputNum) {
		if (inputNum != 0) {
			return labelPropZhTW.getProperty(NUM_ARRAY[inputNum]);
		}
		
		return StringUtils.EMPTY;
	}
	
	private String decimalToChi(Double inputNum) {
		if (inputNum.compareTo(0.25)==0) {
			return labelPropZhTW.getProperty("quarter");
		} else if (inputNum.compareTo(0.5)==0) {
			return labelPropZhTW.getProperty("half");
		} else if (inputNum.compareTo(0.75)==0) {
			return labelPropZhTW.getProperty("threeQua");
		}
		
		return StringUtils.EMPTY;
	}
	
	private boolean isNum(String s) {
		try {
			Double.parseDouble(s);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}

	private boolean chkDoseUnitForUseArabic(String ddu) {
		for (String arabicDoseUnit:ARABIC_DOSE_UNIT) {
			if (ddu.equals(labelPropZhTW.getProperty(arabicDoseUnit))) {
				return true;
			}
		}
		return false;
	}
	
	public static InstructionBuilder instance() {
		if (!Contexts.isApplicationContextActive()) {
			throw new IllegalStateException("No active application scope");
		}
		return (InstructionBuilder) Component.getInstance("instructionBuilder",
				ScopeType.APPLICATION);
	}
		
	private static boolean checkOneThird(BigDecimal b) {
		
		BigDecimal tb = b;
		if (tb.scale() > 4) {
			tb = tb.setScale(4, RoundingMode.DOWN);		
		} 
				
		if (tb.scale() == 4) {
			return (tb.equals(BD_0_3333) || tb.equals(BD_0_3330));
		} else if (tb.scale() == 3) {
			return tb.equals(BD_0_333);
		} else {
			return false;
		}
	}

	public DispOrder buildEngMealTimeDescForAtdps(DispOrder dispOrder){
	
		if( dispOrder.getDispOrderItemList() != null ){
			for( DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList() ){
				PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
				if( pharmOrderItem != null ){
					Regimen regimen = pharmOrderItem.getRegimen();
					if ( regimen == null ) {
						pharmOrderItem.postLoad();
					}
					if( regimen != null ){
						for (DoseGroup doseGroup:regimen.getDoseGroupList()) {
							for (Dose dose:doseGroup.getDoseList()) {
								if( dose.getDmAdminTimeLite() != null ){
									dose.setMealTimeInfo( 
											StringUtils.left(
													buildEngMealTimeDesc(dose, dispOrderItem.getPharmOrderItem().getFormCode(), false),
													75)
											);	
								}
							}
						}
					}
				}
			}
		}
		
		return dispOrder;
	}
	
	
	//For ATDPS message
	public List<String> buildInstructionForAtdps(DispOrderItem dispOrderItem, PrintLang printLang){

		List<String> instructionList = new ArrayList<String>();

		boolean isCapd = isCapd(dispOrderItem);
		PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
		if( pharmOrderItem != null ){
			Regimen regimen = pharmOrderItem.getRegimen();
			String formCode = pharmOrderItem.getFormCode();
			if ( regimen == null ) {
				pharmOrderItem.postLoad();
			}
			if( regimen != null ){
				StringBuilder sb;
				for (DoseGroup doseGroup:regimen.getDoseGroupList()) {
					
					boolean gtThousand = false;
					for (Dose dose:doseGroup.getDoseList()) {
						if((dose.getDosage() != null && dose.getDosage().doubleValue() >= 1000) || 
							(dose.getDosageEnd() != null && dose.getDosageEnd().doubleValue() >= 1000)){
							gtThousand = true;
						}
					}
					boolean arabicFlag = (doseGroup.getDoseList().size() > 1 && gtThousand);

					for (Dose dose:doseGroup.getDoseList()) {
						ConvertDispLabelDescCriteria cld = new ConvertDispLabelDescCriteria();
						cld.setRegimen(regimen.getType().getDataValue());
						cld.setFreqCode(dose.getDailyFreq().getCode());

						if (dose.getDailyFreq().getParam() != null && dose.getDailyFreq().getParam().length > 0){
							cld.setFreqValue(dose.getDailyFreq().getParam()[0]);
						} else {
							cld.setFreqValue("0");
						}

						if ( dose.getSupplFreq() == null ) {
							cld.setSupplFreqCode(StringUtils.EMPTY);		
						} else {
							cld.setSupplFreqCode(dose.getSupplFreq().getCode());
						}

						cld.setEngSiteDesc(dose.getSiteDesc());

						dispLabelDescConvertor.checkConvertDispLabelDescRule(cld);

						//FormVerb
						String formVerbLabelDesc = StringUtils.EMPTY;
						if( StringUtils.isNotBlank(formCode) && StringUtils.isNotBlank(dose.getSiteCode()) ){
							if( printLang == PrintLang.Chi ){
								formVerbLabelDesc = getChiFormVerbDesc(pharmOrderItem.getFormCode(), dose);
							}else{
								formVerbLabelDesc = getEngFormVerbDesc(formCode, dose);
							}
						}
						
						//Site
						String siteLabelDesc = StringUtils.EMPTY;
						if( printLang == PrintLang.Chi ){
							if (cld.getChiConvSiteDesc() == null || StringUtils.equals(cld.getChiConvSiteDesc(), NA)) {
								siteLabelDesc = getChiSupplSiteDesc(dose);	
							} else {
								siteLabelDesc = labelPropZhTW.getProperty(cld.getChiConvSiteDesc());
							}
						}else{
							if (cld.getEngConvSiteDesc() == null || StringUtils.equals(cld.getEngConvSiteDesc(), NA)) {
								siteLabelDesc = getEngSupplSiteDesc(dose);
							} else {
								siteLabelDesc = cld.getEngConvSiteDesc();
							}
						}
						
						String adminTimeLabelDesc = StringUtils.EMPTY;
						if (dose.getDmAdminTimeLite() != null) {
							if( printLang == PrintLang.Chi ){
								adminTimeLabelDesc = dose.getDmAdminTimeLite().getAdminTimeChi();
							}else{
								adminTimeLabelDesc = dose.getDmAdminTimeLite().getAdminTimeEng();
							}
						}
						
						//Dosage
						String dosageLabelDesc = StringUtils.EMPTY;
						if( isCapd ){
							if( printLang == PrintLang.Chi ){
								dosageLabelDesc = getChiCapdDosageDesc(dose);
							}else{
								dosageLabelDesc = getEngCapdDosageDesc(dose);	
							}
						}else{
							if (dose.getDosage() != null && StringUtils.isNotBlank(dose.getDosageUnit()) ) {
								if( printLang == PrintLang.Chi ){
									dosageLabelDesc = getChiDosageDesc(dose, pharmOrderItem.getFormCode(), arabicFlag);											
								}else{
									dosageLabelDesc = getEngDosageDesc(dose, dispOrderItem);	
								}
							}
						}
						
						//Freq
						String freqLabelDesc = StringUtils.EMPTY;
						Freq dailyFreq = dose.getDailyFreq();
						if( dailyFreq != null ){
							if( printLang == PrintLang.Chi ){
								if (cld.getChiConvFreqDesc() == null || StringUtils.equals(cld.getChiConvFreqDesc(), NA)) {
									freqLabelDesc = getChiFreqDesc(dose);	
								} else {
									freqLabelDesc = labelPropZhTW.getProperty(cld.getChiConvFreqDesc());
								}
							}else{
								if (cld.getEngConvFreqDesc() == null || StringUtils.equals(cld.getEngConvFreqDesc(), NA)) {
									freqLabelDesc = getEngFreqDesc(dose);	
								} else {
									freqLabelDesc = cld.getEngConvFreqDesc();
								}
							}
						}

						//SuppFreq
						String supplFreqLabelDesc = StringUtils.EMPTY;
						Freq supplFreq = dose.getSupplFreq();
						if( supplFreq != null ){
							if( printLang == PrintLang.Chi ){
								if (cld.getChiConvSupplFreqDesc() == null || StringUtils.equals(cld.getChiConvSupplFreqDesc(), NA)) {
									supplFreqLabelDesc = getChiSupplFreqDesc(dose);
								} else {
									supplFreqLabelDesc = labelPropZhTW.getProperty(cld.getChiConvSupplFreqDesc());
								}
							}else{
								if (cld.getEngConvSupplFreqDesc() == null || StringUtils.equals(cld.getEngConvSupplFreqDesc(), NA)) {
									supplFreqLabelDesc = getEngSupplFreqDesc(dose, true);
								} else {
									supplFreqLabelDesc = cld.getEngConvSupplFreqDesc();
								}										
							}
						}
						
						//PRN	
						String prnLabelDesc = StringUtils.EMPTY;
						if( dose.getPrn() ){
							if( printLang == PrintLang.Chi ){
								prnLabelDesc = getChiPrnDesc(dose);
							}else{
								prnLabelDesc = getEngPrnDesc(dose);										
							}
						}
						
						sb = new StringBuilder();
						
						if( printLang == PrintLang.Chi ){
							String[] chiElements = {adminTimeLabelDesc,
									formVerbLabelDesc, 
									siteLabelDesc, 
									prnLabelDesc, 
									supplFreqLabelDesc,
									freqLabelDesc,
									dosageLabelDesc, 
									};
							for( String s : chiElements ){
								if( StringUtils.isNotBlank(s) ){
									sb.append( s );	
								}
							}
						}else{
							String[] engElements = {formVerbLabelDesc, 
									siteLabelDesc, 
									adminTimeLabelDesc, 
									dosageLabelDesc, 
									freqLabelDesc,
									supplFreqLabelDesc, 
									prnLabelDesc};
							for( String s : engElements ){
								if( StringUtils.isNotBlank(s) ){
									if( sb.length() != 0 ){
										sb.append(SPACE);
									}
									sb.append( s );	
								}
							}							
						}
						instructionList.add(
								StringUtils.left(
										StringUtils.replace(sb.toString(), LINE_BREAK_SEPARATOR, StringUtils.EMPTY),
										110)
								);
					}
				}			
			}
		}
		return instructionList;
	}
	
	//For DA and EPR message
	public DispOrder buildLabelDesc(DispOrder dispOrder){
		DmDrug dmDrug;
		if( dispOrder.getDispOrderItemList() != null ){
			for( DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList() ){
				PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
				if( pharmOrderItem != null ){
					Regimen regimen = pharmOrderItem.getRegimen();
					if( regimen != null ){
						buildRegimenLabelDesc( regimen, pharmOrderItem.getFormCode() );								
					}
					dmDrug = DmDrugCacher.instance().getDmDrug(pharmOrderItem.getItemCode());
					if( dmDrug != null ){
						pharmOrderItem.setDrugGroup( StringUtils.trimToNull(dmDrug.getDrugGroup()) );
					}
				}
			}
		}
		
		return dispOrder;
	}
	
	//For DA and EPR message
	private void buildRegimenLabelDesc(Regimen regimen, String formCode){
		
		for (DoseGroup doseGroup:regimen.getDoseGroupList()) {
			for (Dose dose:doseGroup.getDoseList()) {
				ConvertDispLabelDescCriteria cld = new ConvertDispLabelDescCriteria();
				cld.setRegimen(regimen.getType().getDataValue());
				cld.setFreqCode(dose.getDailyFreq().getCode());

				if (dose.getDailyFreq().getParam() != null && dose.getDailyFreq().getParam().length > 0){
					cld.setFreqValue(dose.getDailyFreq().getParam()[0]);
				} else {
					cld.setFreqValue("0");
				}

				if ( dose.getSupplFreq() == null ) {
					cld.setSupplFreqCode(StringUtils.EMPTY);		
				} else {
					cld.setSupplFreqCode(dose.getSupplFreq().getCode());
				}

				cld.setEngSiteDesc(dose.getSiteDesc());

				dispLabelDescConvertor.checkConvertDispLabelDescRule(cld);

				Freq dailyFreq = dose.getDailyFreq();
				if( dailyFreq != null ){
					if (cld.getEngConvFreqDesc() == null || StringUtils.equals(cld.getEngConvFreqDesc(), NA)) {
						dailyFreq.setLabelDesc( getEngFreqDesc(dose) );	
					} else {
						dailyFreq.setLabelDesc( cld.getEngConvFreqDesc() );
					}
					dailyFreq.setLabelDesc( StringUtils.trimToNull(dailyFreq.getLabelDesc()) );
				}

				Freq supplFreq = dose.getSupplFreq();
				if( supplFreq != null ){
					if (supplFreq.getParam() != null 
							&& supplFreq.getParam().length >= 1 
							&& "E".equals(supplFreq.getMultiplierType())) {
						supplFreq.setLabelDesc(getSupplFreqDescForWeekday(dose));
					} else if (cld.getEngConvSupplFreqDesc() == null || StringUtils.equals(cld.getEngConvSupplFreqDesc(), NA)) {
						supplFreq.setLabelDesc( getEngSupplFreqDesc(dose, false) );
					} else {
						supplFreq.setLabelDesc( cld.getEngConvSupplFreqDesc() );
					}
					supplFreq.setLabelDesc( StringUtils.trimToNull(supplFreq.getLabelDesc()) );
				}

				if (cld.getEngConvSiteDesc() == null || StringUtils.equals(cld.getEngConvSiteDesc(), NA)) {
					dose.setSiteLabelDesc( getEngSupplSiteDesc(dose) );
				} else {
					dose.setSiteLabelDesc( cld.getEngConvSiteDesc() );
				}
				dose.setSiteLabelDesc( StringUtils.trimToNull(dose.getSiteLabelDesc()) ); 

				if (dose.getDmAdminTimeLite() != null) {
					dose.setAdminTimeLabelDesc( StringUtils.trimToNull(dose.getDmAdminTimeLite().getAdminTimeEng()) );
				}
				
				if( StringUtils.isNotBlank(formCode) && StringUtils.isNotBlank(dose.getSiteCode()) ){
					dose.setFormVerb( StringUtils.trimToNull(getEngFormVerbDesc(formCode, dose)) );
				}
			}
		}

	}
	
	//For DA and EPR message
	private String getSupplFreqDescForWeekday(Dose dose) {

		String param = dose.getSupplFreq().getParam()[0]; 
		
		StringBuilder supplFreqDesc = new StringBuilder();
		supplFreqDesc.append("On every ");

		boolean sundayExist = false;
		if (param.charAt(0) == '1') {
			// buffer Sunday and add at last
			sundayExist = true;
		}
		List<String> dayOfWeekList = new ArrayList<String>();
		for (int i = 1; i < param.length(); i++) {
			// add from Monday to Saturday
			if (param.charAt(i) == '1') {
				dayOfWeekList.add(DAY_ARRAY_FOR_LABEL_DESC[i]);
			}
		}
		if (sundayExist) {
			// add Sunday
			dayOfWeekList.add(DAY_ARRAY_FOR_LABEL_DESC[0]);
		}
		
		for (int i = 0; i < dayOfWeekList.size(); i++) {
			if (i > 0) {
				if (i == dayOfWeekList.size() - 1) {
					supplFreqDesc.append(" and ");
				} else {
					supplFreqDesc.append(", ");
				}
			}
			supplFreqDesc.append(dayOfWeekList.get(i));
		}
		
		return supplFreqDesc.toString();
	}
}
