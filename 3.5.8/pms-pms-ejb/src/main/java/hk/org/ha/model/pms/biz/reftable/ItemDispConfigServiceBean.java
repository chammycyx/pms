package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.ItemDispConfig;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("itemDispConfigService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ItemDispConfigServiceBean implements ItemDispConfigServiceLocal {

	@In
	private ItemDispConfigManagerLocal itemDispConfigManager;
	
	@In
	private Workstore workstore;
	
	public ItemDispConfig retrieveItemDispConfig(String itemCode) 
	{
		return itemDispConfigManager.retrieveItemDispConfig(itemCode, workstore.getWorkstoreGroup(), true);
	}
	
	@Remove
	public void destroy() 
	{
	}
}
