package hk.org.ha.model.pms.biz.refill;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.persistence.disp.RefillScheduleItem;
import hk.org.ha.model.pms.udt.disp.DiscontinueStatus;
import hk.org.ha.model.pms.udt.refill.DocType;
import hk.org.ha.model.pms.udt.refill.RefillScheduleRefillStatus;
import hk.org.ha.model.pms.udt.refill.RefillScheduleStatus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.beanutils.BeanComparator;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;
import org.jboss.seam.security.Identity;

@AutoCreate
@Stateless
@Name("refillScheduleUtils")
@MeasureCalls
public class RefillScheduleUtilsBean implements RefillScheduleUtilsLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	public Map<Integer, Boolean> retrieveRefillScheduleReDispFlagMap(Long pharmOrderId, DocType rsDocType){
		Map<Integer, Boolean> rsReDispFlagMap = new HashMap<Integer, Boolean>();
		
		List<RefillSchedule> prevRsList = em.createQuery(
												"select o from RefillSchedule o" + // 20120303 index check : RefillSchedule.pharmOrder : FK_REFILL_SCHEDULE_01
												" where o.pharmOrder.id = :pharmOrderId" +
												" and o.docType = :docType"+
												" and o.refillStatus = :refillStatus")
												.setParameter("pharmOrderId", pharmOrderId)
												.setParameter("docType", rsDocType)
												.setParameter("refillStatus", RefillScheduleRefillStatus.Next)
												.getResultList();
		
		for( RefillSchedule prevRs : prevRsList ){
			if( prevRs.getPrintFlag() ){
				rsReDispFlagMap.put(prevRs.getGroupNum(), Boolean.TRUE);
			}
		}		
		return rsReDispFlagMap;
	}
	
	public void restorePharmOrderItemNum(DispOrder dispOrder){
		PharmOrder corpPharmOrder = em.find(PharmOrder.class, dispOrder.getPharmOrder().getId());
		em.clear();
		for( PharmOrderItem pharmOrderItem : corpPharmOrder.getPharmOrderItemList() ){
			pharmOrderItem.clearDispOrderItemList();
		}
		
		for( DispOrderItem doi : dispOrder.getDispOrderItemList() ){
			PharmOrderItem poi = corpPharmOrder.getPharmOrderItemByOrgItemNum(doi.getPharmOrderItem().getOrgItemNum());
			poi.addDispOrderItem(doi);
		}
		corpPharmOrder.clearDispOrderList();
		corpPharmOrder.addDispOrder(dispOrder);
	}
	
	@SuppressWarnings("unchecked")
	public void assignItemNumForRefillSave(List<RefillSchedule> currRefillScheduleList, DispOrder prevDispOrder){
		Integer maxDispItemNum = 0;
		if( prevDispOrder != null ){
			for( DispOrderItem prevDoi : prevDispOrder.getDispOrderItemList() ){
				if( prevDoi.getItemNum() > maxDispItemNum ){
					maxDispItemNum = prevDoi.getItemNum();
				}
			}
		}
				
		PharmOrder currPharmOrder = currRefillScheduleList.get(0).getPharmOrder();		
		if( DocType.Standard == currRefillScheduleList.get(0).getDocType() ){
			currPharmOrder.clearDirtyFlag();
			em.clear();
			List<PharmOrderItem> poiRsList = new ArrayList<PharmOrderItem>();
			
			for( RefillScheduleItem currRsi : currRefillScheduleList.get(0).getRefillScheduleItemList() ){
				PharmOrderItem currPoi = currPharmOrder.getPharmOrderItemByOrgItemNum(currRsi.getItemNum());

				if( currPoi != null ){
					if( currPoi.getMedOrderItem().getDiscontinueStatus() != null && currPoi.getMedOrderItem().getDiscontinueStatus() != DiscontinueStatus.None ){
						continue;
					}
					
					if( prevDispOrder == null ){
						currPoi.setDirtyFlag(true);
					}else{
						DispOrderItem dispOrderItem = prevDispOrder.getDispOrderItemByPoOrgItemNum(currRsi.getItemNum());
						
						if( dispOrderItem != null ){
							currPoi.setItemNum(dispOrderItem.getItemNum());
							
							if( !currRsi.getRefillQty().equals(Integer.valueOf(dispOrderItem.getDispQty().intValue())) ||
								!currRsi.getRefillDuration().equals(dispOrderItem.getDurationInDay()) ){
								currPoi.setDirtyFlag(true);
							}
						}
					}
					poiRsList.add(currPoi);
				}
			}
			currPharmOrder.clearPharmOrderItemList();
			currPharmOrder.setPharmOrderItemList(poiRsList);
			
			Collections.sort(currPharmOrder.getPharmOrderItemList(), new BeanComparator("itemNum"));
			
			for( PharmOrderItem pharmOrderItem : currPharmOrder.getPharmOrderItemList() ){
				if( pharmOrderItem.isDirtyFlag() ){
					maxDispItemNum = maxDispItemNum + 1;
					pharmOrderItem.setItemNum(maxDispItemNum);
				}
			}
		}else{
			if( prevDispOrder != null ){
				for( RefillSchedule currRs : currRefillScheduleList ){
					for( RefillScheduleItem currRsi: currRs.getRefillScheduleItemList() ){
						DispOrderItem dispOrderItem = prevDispOrder.getDispOrderItemByPoOrgItemNum(currRsi.getItemNum());
						
						if( dispOrderItem != null ){
							PharmOrderItem currPoi = currPharmOrder.getPharmOrderItemByOrgItemNum(currRsi.getItemNum());
							currPoi.setItemNum(dispOrderItem.getItemNum());
						}
					}
				}
			}
			currPharmOrder.assignItemNum(maxDispItemNum);
		}
		
		if( logger.isDebugEnabled() ){
			for( PharmOrderItem newPoi : currPharmOrder.getPharmOrderItemList() ){
				logger.debug("assignItemNumForRefillSave====================================#0, #1, #2", newPoi.getItemCode(), newPoi.getItemNum(), newPoi.isDirtyFlag()); 
			}
		}
	}

	public void markSysDeletedByMedOrder(MedOrder medOrder) {
		for (PharmOrder pharmOrder : medOrder.getPharmOrderList()) {
			markSysDeletedByPharmOrder(pharmOrder);
		}
	}
	
	private void markSysDeletedByPharmOrder(PharmOrder pharmOrder) {
		String userName = "system";
		if (Contexts.isSessionContextActive()) {
			userName = Identity.instance().getCredentials().getUsername();
		} 
		
		em.createQuery(
				"update RefillSchedule o" + 
				" set o.status = :status," +
				" o.updateUser = :updateUser," +
				" o.updateDate = :updateDate," +
				" o.version = o.version + 1" +
				" where o.pharmOrder.id = :pharmOrderId ")
				.setParameter("status", RefillScheduleStatus.SysDeleted)
				.setParameter("updateUser", userName)
				.setParameter("updateDate", new Date())
				.setParameter("pharmOrderId", pharmOrder.getId())
				.executeUpdate();
	}
}
