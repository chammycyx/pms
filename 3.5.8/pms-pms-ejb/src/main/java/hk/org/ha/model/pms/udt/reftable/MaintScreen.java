package hk.org.ha.model.pms.udt.reftable;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum MaintScreen implements StringValuedEnum {

	None("-", "None"),
	Workstore("W", "Workstore"),
	IPWorkstore("I", "IP Workstore"),
	OperationMode("O", "Operation mode"),
	RefillConfig("R", "Refill config"),
	CheckAndIssue("C", "Check and issue"),
	Support("S", "Support");

    private final String dataValue;
    private final String displayValue;

    MaintScreen(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }
    
    public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}
	
	public static MaintScreen dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MaintScreen.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<MaintScreen> {

		private static final long serialVersionUID = 1L;

		public Class<MaintScreen> getEnumClass() {
			return MaintScreen.class;
		}
		
	}
}
