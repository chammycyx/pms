package hk.org.ha.model.pms.udt.reftable;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum OperationProfileType implements StringValuedEnum {
	
	Eds("E", "EDS"),
	Pms("P", "PMS"),
	NonPeak("N", "Non-Peak");

	private final String dataValue;
	private final String displayValue;
	
	OperationProfileType(final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}
	
	public static OperationProfileType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(OperationProfileType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<OperationProfileType> {

		private static final long serialVersionUID = 1L;

		public Class<OperationProfileType> getEnumClass() {
			return OperationProfileType.class;
		}

	}
	
}
