package hk.org.ha.model.pms.biz.onestop;

import static hk.org.ha.model.pms.prop.Prop.CHARGING_SFI_SUSPEND_ORDER_SUSPENDCODE;
import static hk.org.ha.model.pms.prop.Prop.ONESTOP_SUSPENDORDERCOUNT_DURATION_LIMIT;
import static hk.org.ha.model.pms.prop.Prop.PATIENT_PAS_APPT_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.PATIENT_PAS_PATIENT_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.ONESTOP_PATIENT_PATCATCODE_REQUIRED;
import static hk.org.ha.model.pms.prop.Prop.ONESTOP_PATIENT_PATCATCODE;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.util.QueryUtils;
import hk.org.ha.fmk.pms.util.Resources;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.charging.DrugChargeClearanceManagerLocal;
import hk.org.ha.model.pms.biz.validation.SearchTextValidatorLocal;
import hk.org.ha.model.pms.corp.biz.onestop.OneStopHelper;
import hk.org.ha.model.pms.persistence.corp.HospitalMapping;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.CapdVoucher;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.persistence.reftable.SuspendReason;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.prop.Prop;
import hk.org.ha.model.pms.udt.RecordStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderDayEndStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderPatType;
import hk.org.ha.model.pms.udt.disp.PharmOrderStatus;
import hk.org.ha.model.pms.udt.onestop.MoeFilterOption;
import hk.org.ha.model.pms.udt.onestop.MoeSortOption;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderStatus;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderStatusIndicator;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
import hk.org.ha.model.pms.udt.refill.DocType;
import hk.org.ha.model.pms.udt.refill.RefillScheduleRefillStatus;
import hk.org.ha.model.pms.udt.refill.RefillScheduleStatus;
import hk.org.ha.model.pms.udt.validation.SearchTextType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.onestop.MoStatusCount;
import hk.org.ha.model.pms.vo.onestop.OneStopOrderInfo;
import hk.org.ha.model.pms.vo.onestop.OneStopOrderItemSummary;
import hk.org.ha.model.pms.vo.onestop.OneStopRefillItemSummary;
import hk.org.ha.model.pms.vo.onestop.OneStopSummary;
import hk.org.ha.model.pms.vo.onestop.PrescriptionSortCriteria;
import hk.org.ha.model.pms.vo.onestop.RequireAppointmentRule;
import hk.org.ha.model.pms.vo.security.PropMap;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("oneStopService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class OneStopServiceBean implements OneStopServiceLocal {
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	private List<OneStopOrderItemSummary> prescriptionOrderList;
	
	@Out(required = false)
	private String errMsg;
	
	@Out(required = false)
	private MoStatusCount moStatusCount;
	
	@Out(required = false)
	private Patient defaultPatient;
	
	@Out(required = false)
	private MedCase defaultAppointment;
	
	@Out(required = false)
	private List<MedCase> appointmentList;
	
	@Out(required = false)
	private List<OneStopSummary> suspendOrderList;
	
	@Out(required = false)
	private List<OneStopSummary> moeUnvetOrderList;

	@Out(required = false)
	private List<OneStopSummary> moeRefillOrderList;

	@Out(required = false)
	private List<OneStopSummary> moeCancelOrderList;
	
	@In
	private UamInfo uamInfo;
	
	@In
	private Workstation workstation;
	
	@In
	private AuditLogger auditLogger;
	
	@In
	private SearchTextValidatorLocal searchTextValidator;
	
	@In
	private OneStopUtilsLocal oneStopUtils;
	
	@In
	private OneStopCountManagerLocal oneStopCountManager;
	
	@In
	private OneStopPasManagerLocal oneStopPasManager;
	
	@In
	private OneStopOrderStatusManagerLocal oneStopOrderStatusManager;
	
	@In
	private OneStopPrescManagerLocal oneStopPrescManager;
	
	@In
	private PropMap propMap;
	
	@In
	private DrugChargeClearanceManagerLocal drugChargeClearanceManager;
	
	@In
	private Workstore workstore;
	
	@In
	private PatSpecNoApptMappingInf patSpecNoApptMapping;
	
	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
	
	@In
	private OneStopHelper oneStopHelper;
		
	@In(required = false)
	@Out(required = false)
	private OneStopOrderInfo oneStopOrderInfo;
	
	@In(required = false)
	@Out(required = false)
	private OneStopSummary oneStopSummary;
	
	@Out(required = false)
	private OneStopOrderItemSummary latestOneStopOrderItemSummary;
	
	@Out(required = false)
	private OneStopRefillItemSummary latestOneStopRefillItemSummary;
	
	@Out(required = false)
	private List<OneStopRefillItemSummary> latestOneStopRefillItemSummaryList;

	private Map<Long, OneStopSummary> originalUnvetOneStopSummaryMap;
	private Map<Long, OneStopSummary> originalCancelledOneStopSummaryMap;
	private Map<Long, OneStopSummary> originalRefillOneStopSummaryMap;
	
	private SearchTextType searchTextType;
	private List<OneStopSummary> refNumOrderList;
	private List<OneStopOrderItemSummary> caseNumUnvetList;
	private List<OneStopOrderItemSummary> caseNumVettedList;
	private String vettingMsg;
	
	private static final String UNCHECKED = "unchecked";
	private static final String HOSP_CODE = "hospCode";
	private static final String PAT_HOSP_CODE = "patHospCode";
	private static final String WORKSTORE = "workstore";
	private static final String STATUS = "status";
	private static final String STATUS_1 = "status1";
	private static final String ORDER_DATE = "orderDate";
	private static final String ADMIN_STATUS = "adminStatus";
	private static final String DOC_TYPE = "docType";
	private static final String ORDER_NUM = "orderNum";
	private static final String REF_NUM = "refNum";
	private static final String ID = "id";
	private static final String START_DATE = "startDate";
	private static final String END_DATE = "endDate";
	private static final String HKID = "hkid";
	private static final String CASE_NUM = "caseNum";
	private static final String PRESC_TYPE = "prescType";
	private static final String REFILL_FLAG = "refillFlag";
	private static final String TICKET_NUM = "ticketNum";
	private static final String ORDER_TYPE = "orderType";
	private static final String MED_ORDER_STATUS = "medOrderStatus";
	private static final String PHARM_ORDER_STATUS = "pharmOrderStatus";
	private static final String DISP_ORDER_STATUS = "dispOrderStatus";
	private static final String REFILL_COUPON_NUM = "refillCouponNum";
	private static final String TRIM_SFI_REFILL_COUPON_NUM = "trimSfiRefillCouponNum";
	private static final String SFI_GROUP_NUM = "groupNum";
	private static final String SFI_MO_ORIGINAL_ITEM_NUM = "moOrgItemNum";
	private static final String REFILL_STATUS = "refillStatus";
	private static final String PHARM_ORDER_ID = "pharmOrderId";
	private static final String ERROR_MESSAGE = "errMsg";
	private static final String PATIENT = "patient";
	private static final String HKPMI_MEDCASE_LIST = "hkpmiMedCaseList";
	private static final String PHS_SPECIALTY = "phsSpecailty";
	private static final String PHS_WARD = "phsWard";
	private static final String NO_HKPMI_PATIENT_FOUND = "noHkpmiPatientFound";
	private static final String REFILL_TYPE = "refillType";
	private static final String DAY_END_STATUS = "dayEndStatus";
	private static final String BATCH_PROCESSING_FLAG = "batchProcessingFlag";
	private static final String TODAY = "today";
	private static final String TOMORROW = "tomorrow";
	private static final String PAS_PAT_HOSP_CODE_CASE_NUM_DELIMITER = "|";
	
	private static final int UNVET_ORDER_DISPLAY_LIMIT = 18;
	private static final int CANCELLED_ORDER_DISPLAY_LIMIT = 5;
	
	private DateFormat pasDateFormat = new SimpleDateFormat("yyyyMMdd 00:00"); 
				
	public void clearOrder() {
		oneStopSummary = null;
		refNumOrderList = null;
		errMsg = null;
		appointmentList = null;
		caseNumUnvetList = null;
		caseNumVettedList = null;
		oneStopOrderInfo = new OneStopOrderInfo();
		defaultAppointment = null;
		defaultPatient = null;		
		latestOneStopOrderItemSummary = null;
		latestOneStopRefillItemSummary = null;
		latestOneStopRefillItemSummaryList = null;
		
		if (Contexts.getSessionContext().get("errMsg") != null) {
			Contexts.getSessionContext().remove("errMsg");
		}
		
		retrieveUnvetAndSuspendCount();
	}
	
	public void retrieveUnvetAndSuspendCount() {
		if (oneStopCountManager.isCounterTimeOut(moStatusCount))
		{	
			oneStopCountManager.retrieveUnvetAndSuspendCount(workstore, propMap.getValueAsInteger(ONESTOP_SUSPENDORDERCOUNT_DURATION_LIMIT.getName()));		
		}
		
		moStatusCount = OneStopCountManagerBean.getMoStatusCountByWorkstore(workstore);
	}
	
	public void retrieveOrder(String searchText, Boolean displayAllMoe) {
		clearOrder();
		searchTextType = searchTextValidator.checkSearchInputType(searchText);
		logger.debug("searchTextType is: #0", searchTextType);
		
		switch(searchTextType) {
			case OrderNum:
				logger.info("retrieve order by order num #0", searchText);
				retrieveOrderByOrderNum(oneStopUtils.convertOrderNum(searchText));
				oneStopOrderInfo.setPatHospCode(StringUtils.isBlank(workstore.getDefPatHospCode())?workstore.getHospCode():workstore.getDefPatHospCode());
				break;
			case OrderRefNum:
				logger.info("retrieve order by ref num #0", searchText);
				retrieveOrderByRefNum(searchText);
				oneStopOrderInfo.setPatHospCode(StringUtils.isBlank(workstore.getDefPatHospCode())?workstore.getHospCode():workstore.getDefPatHospCode());
				break;
			case ManualPatient: 
				oneStopSummary = new OneStopSummary();
				logger.info("retrieve order by unknown patient #0", searchText);
				oneStopSummary.setSearchTextType(searchTextType);
				oneStopOrderInfo.setPatHospCode(StringUtils.isBlank(workstore.getDefPatHospCode())?workstore.getHospCode():workstore.getDefPatHospCode());
				break;
			case Hkid:
				oneStopSummary = new OneStopSummary();
				logger.info("retrieve order by hkid");
				oneStopSummary.setSearchTextType(searchTextType);
				break;
			case Ticket:
				logger.info("retrieve order by ticket num #0", searchText);
				retrieveOrderByTicketNum(searchText);
				oneStopOrderInfo.setPatHospCode(StringUtils.isBlank(workstore.getDefPatHospCode())?workstore.getHospCode():workstore.getDefPatHospCode());
				break;
			case Case:
				logger.info("retrieve order by case #0", searchText);
				retrieveOrderByCaseNum(searchText, displayAllMoe);
				break;
			case Refill:
				logger.info("retrieve order by std refill num #0", searchText);
				retrieveOrderByRefillCouponNum(oneStopUtils.convertRefillNum(searchText));
				oneStopOrderInfo.setPatHospCode(StringUtils.isBlank(workstore.getDefPatHospCode())?workstore.getHospCode():workstore.getDefPatHospCode());
				break;
			case SfiRefill:
				logger.info("retrieve order by sfi refill num #0", searchText);
				retrieveOrderBySfiRefillCouponNum(searchText);
				oneStopOrderInfo.setPatHospCode(StringUtils.isBlank(workstore.getDefPatHospCode())?workstore.getHospCode():workstore.getDefPatHospCode());
				break;
			case HkidFail:
				errMsg = "0033";
				break;
			case Capd:
				logger.info("retrieve order by capd voucher num #0", searchText);
				retrieveOrderByCapdVoucherNum(searchText);
				break;
			case SfiInvoice:
			case Unknown:
				errMsg = "0034";
				break;
			default:
				break;
		}
		
		if (errMsg != null)
		{
			logger.info("retrieveOrder with errMsg code: #0", errMsg);
		}
	}
		
	//Fulfill not implement yet until phase two
	private void retrieveOrderByOrderNum(String orderNum) {
		
		List<OneStopOrderItemSummary> oneStopOrderItemSummaryList = new ArrayList<OneStopOrderItemSummary>();
		List<OneStopRefillItemSummary> oneStopRefillItemSummaryList = new ArrayList<OneStopRefillItemSummary>();
		
		List<MedOrder> medOrderList = retrieveUnvetOrderByOrderNum(orderNum);
				
		//pharmOrderList.isEmpty() is used to check for unvet cancel
		if (!medOrderList.isEmpty()) 
		{
			oneStopOrderItemSummaryList.add(oneStopUtils.convertSummaryFromUnvetOrder(medOrderList.get(0)));
		} 
		else 
		{
			//retrieve vetted order
			List<DispOrder> dispOrderList = retrieveVettedOrderByOrderNum(orderNum);
					
			for (DispOrder dispOrder : dispOrderList) 
			{
				oneStopOrderItemSummaryList.add(oneStopUtils.convertSummaryFromVettedOrder(dispOrder));
			}
		}
		
		convertOneStopSummary(oneStopOrderItemSummaryList, oneStopRefillItemSummaryList);
	}
	
	@SuppressWarnings(UNCHECKED)
	private List<MedOrder> retrieveUnvetOrderByOrderNum(String orderNum) {
		Query query = em.createQuery(
				"select o from MedOrder o" + // 20120225 index check : MedOrder.orderNum : I_MED_ORDER_01
				" where o.orderNum = :orderNum" +
				" and o.status in :status" +
				" and o.patHospCode in :patHospCode" +
				" and o.docType = :docType" +
				" and o.orderType = :orderType" +
				" and o.vetDate is null")
				.setParameter(ORDER_NUM, orderNum)
				.setParameter(STATUS, MedOrderStatus.Outstanding_Deleted_Withhold_PreVet)
				.setParameter(PAT_HOSP_CODE, oneStopUtils.retrieveFriendClubListAndDefaultPatHospCode())
				.setParameter(DOC_TYPE, MedOrderDocType.Normal)
				.setParameter(ORDER_TYPE, OrderType.OutPatient);
			
		return (List<MedOrder>) query.getResultList();
	}
	
	@SuppressWarnings(UNCHECKED)
	private List<DispOrder> retrieveVettedOrderByOrderNum(String orderNum) 
	{
		return (List<DispOrder>) em.createQuery(
				"select o from DispOrder o" + // 20120225 index check : MedOrder.orderNum : I_MED_ORDER_01
				" where o.pharmOrder.medOrder.orderNum = :orderNum" +
				" and o.pharmOrder.medOrder.status in :status" +
				" and o.pharmOrder.medOrder.patHospCode in :patHospCode" +
				" and o.pharmOrder.medOrder.docType = :docType" +
				" and o.refillFlag = :refillFlag" +
				" and o.pharmOrder.status <> :pharmOrderStatus" +
				" and o.pharmOrder.medOrder.orderType = :orderType")
				.setParameter(ORDER_NUM, orderNum)
				.setParameter(STATUS, MedOrderStatus.Complete_Deleted)
				.setParameter(PAT_HOSP_CODE, oneStopUtils.retrieveFriendClubListAndDefaultPatHospCode())
				.setParameter(DOC_TYPE, MedOrderDocType.Normal)
				.setParameter(REFILL_FLAG, Boolean.FALSE)
				.setParameter(PHARM_ORDER_STATUS, PharmOrderStatus.SysDeleted)
				.setParameter(ORDER_TYPE, OrderType.OutPatient)
				.getResultList();
	}
	
	//Fulfill not implement yet until phase two
	private void retrieveOrderByRefNum(String refNumIn) {
		List<OneStopRefillItemSummary> oneStopRefillItemSummaryList = new ArrayList<OneStopRefillItemSummary>();
		refNumOrderList = new ArrayList<OneStopSummary>();
		String refNum;
		DateTime dateTime = new DateTime().minusYears(1);
		
		if (refNumIn.length() == 5)  {
			refNum = refNumIn.toLowerCase(Locale.ENGLISH);
		} else {
			refNum = refNumIn;
		}
		
		//Unvet Order
		List<MedOrder> medOrderList = retrieveUnvetOrderByRefNum(refNum, dateTime);
		
		for (MedOrder medOrder : medOrderList) 
		{
			OneStopSummary unvetOneStopSummary = new OneStopSummary();
			List<OneStopOrderItemSummary> oneStopOrderItemSummaryList = new ArrayList<OneStopOrderItemSummary>();
			unvetOneStopSummary.setSearchTextType(searchTextType);
			oneStopOrderItemSummaryList.add(oneStopUtils.convertSummaryFromUnvetOrder(medOrder));
			unvetOneStopSummary.setOneStopOrderItemSummaryList(oneStopOrderItemSummaryList);
			unvetOneStopSummary.setOneStopRefillItemSummaryList(oneStopRefillItemSummaryList);
			refNumOrderList.add(unvetOneStopSummary);
		}
		//Vetted order
		List<DispOrder> dispOrderList = retrieveVettedOrderByRefNum(refNum, dateTime);
		
		for (DispOrder dispOrder : dispOrderList) 
		{
			OneStopSummary dispOneStopSummary = new OneStopSummary();
			List<OneStopOrderItemSummary> oneStopOrderItemSummaryList = new ArrayList<OneStopOrderItemSummary>();
			oneStopOrderItemSummaryList.add(oneStopUtils.convertSummaryFromVettedOrder(dispOrder));
			dispOneStopSummary.setSearchTextType(searchTextType);
			dispOneStopSummary.setPatient(dispOrder.getPharmOrder().getMedOrder().getPatient());
			dispOneStopSummary.setOneStopOrderItemSummaryList(oneStopOrderItemSummaryList);
			dispOneStopSummary.setOneStopRefillItemSummaryList(oneStopRefillItemSummaryList);
			refNumOrderList.add(dispOneStopSummary);
		}
				
		//Decision to display the data on popup or not, if only one record is found, change the refNumResultList to null to display the data
		//directly on the UI.	
		if (refNumOrderList.isEmpty()) 
		{
			errMsg ="0005";
			logger.info("notification that no order is found using reference number with errMsg code: #0", errMsg);
			return;
		}
		
		if (refNumOrderList.size() == 1) 
		{
			oneStopSummary = refNumOrderList.get(0);
			refNumOrderList = null;
		}
	}
	
	@SuppressWarnings(UNCHECKED)
	private List<MedOrder> retrieveUnvetOrderByRefNum(String refNum, DateTime dateTime) 
	{
		return (List<MedOrder>) em.createQuery(
				"select o from MedOrder o" + // 20120305 index check : MedOrder.refNum,orderDate : I_MED_ORDER_05
				" where o.refNum = :refNum" +
				" and o.orderDate >= :orderDate" +
				" and o.status in :status" + 
				" and o.patHospCode = :patHospCode" +
				" and o.docType = :docType" +
				" and o.orderType = :orderType" +
				" and o.vetDate is null")
				.setParameter(REF_NUM, refNum)
				.setParameter(ORDER_DATE, dateTime.toDate(), TemporalType.DATE)
				.setParameter(STATUS, MedOrderStatus.Outstanding_Deleted_Withhold_PreVet)
				.setParameter(PAT_HOSP_CODE, workstore.getDefPatHospCode())
				.setParameter(DOC_TYPE, MedOrderDocType.Normal)
				.setParameter(ORDER_TYPE, OrderType.OutPatient)
				.getResultList();
	}
	
	@SuppressWarnings(UNCHECKED)
	private List<DispOrder> retrieveVettedOrderByRefNum(String refNum, DateTime dateTime) 
	{
		return (List<DispOrder>) em.createQuery(
				"select o from DispOrder o" + // 20120305 index check : MedOrder.refNum,orderDate : I_MED_ORDER_05
				" where o.pharmOrder.medOrder.refNum = :refNum" +
				" and o.pharmOrder.medOrder.orderDate >= :orderDate" +
				" and o.pharmOrder.medOrder.status in :status" +
				" and o.pharmOrder.medOrder.patHospCode = :patHospCode" +
				" and o.pharmOrder.medOrder.docType = :docType" +
				" and o.refillFlag = :refillFlag" +
				" and o.pharmOrder.status <> :pharmOrderStatus" +
				" and o.pharmOrder.medOrder.orderType = :orderType")
				.setParameter(REF_NUM, refNum)
				.setParameter(ORDER_DATE, dateTime.toDate(), TemporalType.DATE)
				.setParameter(STATUS, MedOrderStatus.Complete_Deleted)
				.setParameter(PAT_HOSP_CODE, workstore.getDefPatHospCode())
				.setParameter(DOC_TYPE, MedOrderDocType.Normal)
				.setParameter(REFILL_FLAG, Boolean.FALSE)
				.setParameter(PHARM_ORDER_STATUS, PharmOrderStatus.SysDeleted)
				.setParameter(ORDER_TYPE, OrderType.OutPatient)
				.getResultList();
	}
		
	//Fulfill not implement yet until phase two
	private void retrieveOrderByTicketNum(String ticketNum) 
	{
		List<OneStopOrderItemSummary> oneStopOrderItemSummaryList = new ArrayList<OneStopOrderItemSummary>();
		Map<String, Object> ticketParm = oneStopUtils.retrieveTicketParam(ticketNum);
		
		if (ticketParm.containsKey(ERROR_MESSAGE)) {
			errMsg = ticketParm.get(ERROR_MESSAGE).toString();
			logger.info("retireve order by ticket fail with errMsg code: #0", errMsg);
			return;
		}
		
		List<MedOrder> medOrderList = retrieveUnvetOrderByTicketNum(ticketParm);				
		for (MedOrder medOrder : medOrderList)
		{
			oneStopOrderItemSummaryList.add(oneStopUtils.convertSummaryFromUnvetOrder(medOrder));
		}
		
		List<DispOrder> dispOrderList = retrieveVettedOrderByTicketNum(ticketParm);	
		
		for (DispOrder dispOrder : dispOrderList) 
		{
			oneStopOrderItemSummaryList.add(oneStopUtils.convertSummaryFromVettedOrder(dispOrder));
		}

		List<RefillSchedule> finalRefillScheduleList = new ArrayList<RefillSchedule>();
		List<RefillSchedule> stdRefillScheduleList = new ArrayList<RefillSchedule>();
		List<Long> pharmOrderList = retrievePharmOrderListForStdRefillOrderByTicketNum(ticketParm);
		if (!pharmOrderList.isEmpty())
		{
			stdRefillScheduleList = retrieveStdRefillOrderByTicketNumForVettedOrder(pharmOrderList);
			
			if (stdRefillScheduleList.isEmpty())
			{
				stdRefillScheduleList = retrievePreviousStdRefillOrderBeforeAbortOrder(pharmOrderList);
			}
		}
			
		for (RefillSchedule refillSchedule : stdRefillScheduleList) {
			finalRefillScheduleList.add(refillSchedule);
		}
		
		//use to display specific Sfi order if ticket is found on refill schedule
		List<RefillSchedule> sfiRefillScheduleList = retrieveVettedSfiOrderByTicketNum(ticketParm);
		if (!sfiRefillScheduleList.isEmpty()) {
			
			for (RefillSchedule refillSchedule : sfiRefillScheduleList) {
				
				if (refillSchedule.getRefillNum() == 1)
				{
					finalRefillScheduleList.add(refillSchedule);
					continue;
				}
				
				if (refillSchedule.getTicket() != null && ticketParm.get(TICKET_NUM).equals(refillSchedule.getTicket().getTicketNum()) && 
					refillSchedule.getTicket().getTicketDate().equals(ticketParm.get(START_DATE)))
				{
					finalRefillScheduleList.add(refillSchedule);
					break;
				}
			}
		}
				
		List<OneStopRefillItemSummary> oneStopRefillItemSummaryList = oneStopUtils.convertRefillSfiRefillOrder(finalRefillScheduleList, false);
		convertOneStopSummary(oneStopOrderItemSummaryList, oneStopRefillItemSummaryList);
	}

	@SuppressWarnings(UNCHECKED)
	private List<MedOrder> retrieveUnvetOrderByTicketNum(Map<String, Object> ticketParm) 
	{
		return (List<MedOrder>) em.createQuery(
				"select o from MedOrder o" + // 20120225 index check : Ticket.workstore,orderType,ticketDate,ticketNum : UI_TICKET_01
				" where o.ticket.workstore = :workstore" +
				" and o.ticket.orderType = :orderType" +
				" and o.ticket.ticketDate >= :startDate" +
				" and (o.ticket.ticketDate < :endDate or :endDate is null)" +
				" and o.ticket.ticketNum = :ticketNum" +
				" and o.status in :status" +
				" and o.docType = :docType" +
				" and o.vetDate is null")
				.setParameter(WORKSTORE, workstore)
				.setParameter(ORDER_TYPE, OrderType.OutPatient)
				.setParameter(START_DATE, (Date) ticketParm.get(START_DATE), TemporalType.DATE)
				.setParameter(END_DATE, (Date) ticketParm.get(END_DATE), TemporalType.DATE)
				.setParameter(TICKET_NUM, ticketParm.get(TICKET_NUM))
				.setParameter(STATUS, MedOrderStatus.Outstanding_Deleted_Withhold_PreVet)
				.setParameter(DOC_TYPE, MedOrderDocType.Normal)
				.getResultList();
	}
	
	@SuppressWarnings(UNCHECKED)
	private List<DispOrder> retrieveVettedOrderByTicketNum(Map<String, Object> ticketParm) 
	{
		return (List<DispOrder>) em.createQuery(
				"select o from DispOrder o" + // 20120225 index check : Ticket.workstore,orderType,ticketDate,ticketNum : UI_TICKET_01
				" where o.ticket.workstore = :workstore" +
				" and o.ticket.orderType = :orderType" +
				" and o.ticket.ticketDate >= :startDate" +
				" and (o.ticket.ticketDate < :endDate or :endDate is null)" +
				" and o.ticket.ticketNum = :ticketNum" +
				" and o.pharmOrder.medOrder.status in :status" +
				" and o.refillFlag = :refillFlag" +
				" and o.pharmOrder.status <> :pharmOrderStatus")
				.setParameter(WORKSTORE, workstore)
				.setParameter(ORDER_TYPE, OrderType.OutPatient)
				.setParameter(START_DATE, (Date) ticketParm.get(START_DATE), TemporalType.DATE)
				.setParameter(END_DATE, (Date) ticketParm.get(END_DATE), TemporalType.DATE)
				.setParameter(TICKET_NUM, ticketParm.get(TICKET_NUM))
				.setParameter(STATUS, MedOrderStatus.Complete_Deleted)
				.setParameter(REFILL_FLAG, Boolean.FALSE)
				.setParameter(PHARM_ORDER_STATUS, PharmOrderStatus.SysDeleted)
				.getResultList();
	}
	
	@SuppressWarnings(UNCHECKED)
	private List<Long> retrievePharmOrderListForStdRefillOrderByTicketNum(Map<String, Object> ticketParm)
	{
		return (List<Long>) em.createQuery(
					"select distinct o.pharmOrder.id from RefillSchedule o" + // 20120225 index check : Ticket.workstore,orderType,ticketDate,ticketNum : UI_TICKET_01
					" where o.ticket.workstore = :workstore" +
					" and o.ticket.orderType = :orderType" +
					" and o.ticket.ticketDate >= :startDate" +
					" and (o.ticket.ticketDate < :endDate or :endDate is null)" +
					" and o.ticket.ticketNum = :ticketNum" +
					" and o.docType = :docType" +
					" and o.pharmOrder.status not in :pharmOrderStatus" +
					" order by o.groupNum, o.refillNum")
					.setParameter(WORKSTORE, workstore)
					.setParameter(ORDER_TYPE, OrderType.OutPatient)
					.setParameter(START_DATE, (Date) ticketParm.get(START_DATE), TemporalType.DATE)
					.setParameter(END_DATE, (Date) ticketParm.get(END_DATE), TemporalType.DATE)
					.setParameter(TICKET_NUM, ticketParm.get(TICKET_NUM))
					.setParameter(DOC_TYPE, DocType.Standard)
					.setParameter(PHARM_ORDER_STATUS, PharmOrderStatus.Deleted_SysDeleted)
					.getResultList();
	}
	
	@SuppressWarnings(UNCHECKED)
	private List<RefillSchedule> retrieveStdRefillOrderByTicketNumForVettedOrder(List<Long> pharmOrderIdList) 
	{	
		return (List<RefillSchedule>) em.createQuery(
				"select o from RefillSchedule o" + // 20120225 index check : RefillSchedule.pharmOrder : FK_REFILL_SCHEDULE_01
				" where o.pharmOrder.id in :pharmOrderId" +
				" and (o.refillStatus in :refillStatus or o.refillNum = 1)" +
				" and o.status not in :status" +
				" and o.docType = :docType" +
				" order by o.groupNum, o.refillNum")
				.setParameter(PHARM_ORDER_ID, pharmOrderIdList)
				.setParameter(REFILL_STATUS, Arrays.asList(RefillScheduleRefillStatus.Current, RefillScheduleRefillStatus.Next))
				.setParameter(STATUS, Arrays.asList(RefillScheduleStatus.Abort))
				.setParameter(DOC_TYPE, DocType.Standard)
				.getResultList();
	}
		
	@SuppressWarnings(UNCHECKED)
	private List<RefillSchedule> retrievePreviousStdRefillOrderBeforeAbortOrder(List<Long> pharmOrderIdList)
	{
		return (List<RefillSchedule>) em.createQuery(
				"select o from RefillSchedule o" + // 20120225 index check : RefillSchedule.pharmOrder : FK_REFILL_SCHEDULE_01
				" where o.pharmOrder.id in :pharmOrderId" +
				" and (o.refillStatus = :refillStatus or o.refillNum = 1)" +
				" and o.docType = :docType" +
				" and o.pharmOrder.status not in :pharmOrderStatus" +
				" and o.dispOrder.dayEndStatus = :dayEndStatus" +
				" and o.dispOrder.batchProcessingFlag = :batchProcessingFlag" +
				" order by o.groupNum, o.refillNum")
				.setParameter(PHARM_ORDER_ID, pharmOrderIdList)
				.setParameter(REFILL_STATUS, RefillScheduleRefillStatus.Previous)
				.setParameter(DOC_TYPE, DocType.Standard)
				.setParameter(PHARM_ORDER_STATUS, PharmOrderStatus.Deleted_SysDeleted)
				.setParameter(DAY_END_STATUS, DispOrderDayEndStatus.None)
				.setParameter(BATCH_PROCESSING_FLAG, Boolean.FALSE)
				.getResultList();
	}
	
	@SuppressWarnings(UNCHECKED)
	private List<RefillSchedule> retrieveVettedSfiOrderByTicketNum(Map<String, Object> ticketParm) 
	{
		List<Long> pharmOrderIdList = (List<Long>) em.createQuery(
				"select distinct o.pharmOrder.id from RefillSchedule o" + // 20120225 index check : Ticket.workstore,orderType,ticketDate,ticketNum : UI_TICKET_01
				" where o.ticket.workstore = :workstore" +
				" and o.ticket.orderType = :orderType" +
				" and o.ticket.ticketDate >= :startDate" +
				" and (o.ticket.ticketDate < :endDate or :endDate is null)" +
				" and o.ticket.ticketNum = :ticketNum" +
				" and o.docType = :docType" +
				" and o.pharmOrder.status not in :pharmOrderStatus")
				.setParameter(WORKSTORE, workstore)
				.setParameter(ORDER_TYPE, OrderType.OutPatient)
				.setParameter(START_DATE, (Date) ticketParm.get(START_DATE), TemporalType.DATE)
				.setParameter(END_DATE, (Date) ticketParm.get(END_DATE), TemporalType.DATE)
				.setParameter(TICKET_NUM, ticketParm.get(TICKET_NUM))
				.setParameter(DOC_TYPE, DocType.Sfi)
				.setParameter(PHARM_ORDER_STATUS, PharmOrderStatus.Deleted_SysDeleted)
				.getResultList();
		
		if (!pharmOrderIdList.isEmpty()) {
			return (List<RefillSchedule>) em.createQuery(
					"select o from RefillSchedule o" + // 20120225 index check : RefillSchedule.pharmOrder : FK_REFILL_SCHEDULE_01
					" where o.pharmOrder.id in :pharmOrderId" +
					" and o.docType = :docType" +
					" and o.pharmOrder.status not in :pharmOrderStatus" +
					" order by o.groupNum, o.refillNum")
					.setParameter(PHARM_ORDER_ID, pharmOrderIdList)
					.setParameter(DOC_TYPE, DocType.Sfi)
					.setParameter(PHARM_ORDER_STATUS, PharmOrderStatus.Deleted_SysDeleted)
					.getResultList();
		}
		
		return new ArrayList<RefillSchedule>();
	}
	
	@SuppressWarnings(UNCHECKED)
	private void retrieveOrderByRefillCouponNum(String refillCouponNum) 
	{
		List<OneStopOrderItemSummary> oneStopOrderItemSummaryList = new ArrayList<OneStopOrderItemSummary>();
		List<OneStopRefillItemSummary> oneStopRefillItemSummaryList = new ArrayList<OneStopRefillItemSummary>();
		
		List<Long> pharmOrderIdList = (List<Long>) em.createQuery(
				"select distinct o.pharmOrder.id from RefillSchedule o" + // 20120225 index check : RefillSchedule.refillCouponNum : I_REFILL_SCHEDULE_01
				" where o.refillCouponNum = :refillCouponNum" +
				" and o.docType = :docType" +
				" and o.pharmOrder.workstore.hospCode = :hospCode" +
				" and o.pharmOrder.status not in :pharmOrderStatus" +
				" order by o.groupNum, o.refillNum")
				.setParameter(REFILL_COUPON_NUM, refillCouponNum)
				.setParameter(DOC_TYPE, DocType.Standard)
				.setParameter(HOSP_CODE, workstore.getHospCode())
				.setParameter(PHARM_ORDER_STATUS, PharmOrderStatus.Deleted_SysDeleted)
				.getResultList();
		
		if (!pharmOrderIdList.isEmpty()) 
		{		
			List<RefillSchedule> refillScheduleList = (List<RefillSchedule>) em.createQuery(
					"select o from RefillSchedule o" + // 20120225 index check : RefillSchedule.pharmOrder : FK_REFILL_SCHEDULE_01
					" where o.pharmOrder.id in :pharmOrderId" +
					" and (o.refillStatus in :refillStatus or o.refillNum = 1)" +
					" and o.status not in :status" +
					" and o.docType = :docType" +
					" order by o.groupNum, o.refillNum")
					.setParameter(PHARM_ORDER_ID, pharmOrderIdList)
					.setParameter(REFILL_STATUS, Arrays.asList(RefillScheduleRefillStatus.Current, RefillScheduleRefillStatus.Next))
					.setParameter(STATUS, Arrays.asList(RefillScheduleStatus.Abort))
					.setParameter(DOC_TYPE, DocType.Standard)
					.getResultList();
			 
			if (refillScheduleList.isEmpty())
			{
				refillScheduleList = retrievePreviousStdRefillOrderBeforeAbortOrder(pharmOrderIdList);
			}
			
			if (!refillScheduleList.isEmpty())
			{
				oneStopRefillItemSummaryList = oneStopUtils.convertRefillSfiRefillOrder(refillScheduleList, false);
			}
		}
			
		convertOneStopSummary(oneStopOrderItemSummaryList, oneStopRefillItemSummaryList);
	}
	
	@SuppressWarnings(UNCHECKED)
	private void retrieveOrderBySfiRefillCouponNum(String sfiRefillCouponNum)
	{
		List<OneStopOrderItemSummary> oneStopOrderItemSummaryList = new ArrayList<OneStopOrderItemSummary>();
		List<OneStopRefillItemSummary> oneStopRefillItemSummaryList;
		String trimSfiRefillCouponNum = sfiRefillCouponNum.substring(0, 15);
		String sfiGroupNum = sfiRefillCouponNum.substring(15, 17);
		
		List<RefillSchedule> refillScheduleList = (List<RefillSchedule>) em.createQuery(
				"select o from RefillSchedule o" + // 20120225 index check : RefillSchedule.trimRefillCouponNum : I_REFILL_SCHEDULE_02
				" where o.trimRefillCouponNum = :trimSfiRefillCouponNum" +
				" and o.groupNum = :groupNum" +
				" and o.docType = :docType" +
				" and o.pharmOrder.workstore.hospCode = :hospCode" +
				" and o.pharmOrder.status not in :pharmOrderStatus" +
				" and (o.refillStatus in :refillStatus or o.refillNum = 1) " +
				" order by o.groupNum, o.refillNum")
				.setParameter(TRIM_SFI_REFILL_COUPON_NUM, trimSfiRefillCouponNum)
				.setParameter(SFI_GROUP_NUM, Integer.parseInt(sfiGroupNum))
				.setParameter(DOC_TYPE, DocType.Sfi)
				.setParameter(HOSP_CODE, workstore.getHospCode())
				.setParameter(PHARM_ORDER_STATUS, PharmOrderStatus.Deleted_SysDeleted)
				.setParameter(REFILL_STATUS, RefillScheduleRefillStatus.Current_Next)
				.getResultList();
		
		if (refillScheduleList.isEmpty())
		{
			refillScheduleList = (List<RefillSchedule>) em.createQuery(
					"select o from RefillSchedule o" + // 20120225 index check : RefillSchedule.trimRefillCouponNum : I_REFILL_SCHEDULE_02
					" where o.trimRefillCouponNum = :trimSfiRefillCouponNum" +
					" and o.moOrgItemNum = :moOrgItemNum" +
					" and o.docType = :docType" +
					" and o.pharmOrder.workstore.hospCode = :hospCode" +
					" and o.pharmOrder.status not in :pharmOrderStatus" +
					" and (o.refillStatus in :refillStatus or o.refillNum = 1) " +
					" order by o.groupNum, o.refillNum")
					.setParameter(TRIM_SFI_REFILL_COUPON_NUM, trimSfiRefillCouponNum)
					.setParameter(SFI_MO_ORIGINAL_ITEM_NUM, Integer.parseInt(sfiGroupNum))
					.setParameter(DOC_TYPE, DocType.Sfi)
					.setParameter(HOSP_CODE, workstore.getHospCode())
					.setParameter(PHARM_ORDER_STATUS, PharmOrderStatus.Deleted_SysDeleted)
					.setParameter(REFILL_STATUS, RefillScheduleRefillStatus.Current_Next)
					.getResultList();
		}
		
		if (!refillScheduleList.isEmpty()) 
		{
			oneStopOrderItemSummaryList.add(oneStopUtils.convertSummaryFromVettedOrder(refillScheduleList.get(0).getDispOrder()));
			oneStopRefillItemSummaryList = oneStopUtils.convertRefillSfiRefillOrder(refillScheduleList, true);
			convertOneStopSummary(oneStopOrderItemSummaryList, oneStopRefillItemSummaryList);
		}
		else 
		{
			retrieveOrder(convertSfiRefillCouponNumToOrderNum(sfiRefillCouponNum), false);
		}
	}
	
	//Fulfill not implement yet until phase two
	public void retrieveOrderByHkid(String hkid, Boolean displayAllMoe, Boolean callPasAppointment) {
		String patHospCode = StringUtils.isBlank(workstore.getDefPatHospCode())?workstore.getHospCode():workstore.getDefPatHospCode();
		
		List<OneStopOrderItemSummary> oneStopUnvetSummaryItemList = genUnvetSummaryItemList(hkid, displayAllMoe);
		
		List<DispOrder> dispMoeOrderList = retrieveVettedOrderByHkid(hkid, displayAllMoe);

		List<DispOrder> dispManualOrderList = retrieveVettedManualOrderByHkid(hkid, displayAllMoe);
		
		List<DispOrder> dispDhDeleteOrderList = retrieveVettedDeleteDhOrderByHkid(hkid, displayAllMoe);

		List<OneStopOrderItemSummary> oneStopVettedSummaryItemList = genVettedSummaryItemList(dispMoeOrderList, dispManualOrderList, dispDhDeleteOrderList);

		
		List<OneStopRefillItemSummary> oneStopRefillItemSummaryList = genRefillItemSummaryList(hkid);

		if (defaultPatient != null) {
			handlePatientInfo(patHospCode, oneStopUnvetSummaryItemList, callPasAppointment);
			oneStopOrderInfo.setEhrPatient(defaultPatient.getEhrPatient());
		} else {
			defaultPatient = new Patient();
		}
		
		oneStopOrderInfo.setPatHospCode(patHospCode);

		List<OneStopOrderItemSummary> oneStopOrderItemSummaryList = oneStopUnvetSummaryItemList;
		oneStopOrderItemSummaryList.addAll(oneStopVettedSummaryItemList);
		convertOneStopSummary(oneStopOrderItemSummaryList, oneStopRefillItemSummaryList);
	}

	private List<OneStopOrderItemSummary> genVettedSummaryItemList(List<DispOrder> dispMoeOrderList, List<DispOrder> dispManualOrderList, List<DispOrder> dispDhDeleteOrderList) {
		List<OneStopOrderItemSummary> oneStopVettedSummaryItemList = new ArrayList<OneStopOrderItemSummary>();
		
		for (DispOrder dispOrder : dispMoeOrderList) {
			oneStopVettedSummaryItemList.add(oneStopUtils.convertSummaryFromVettedOrder(dispOrder));
		}
		
		for (DispOrder dispOrder : dispManualOrderList) {
			oneStopVettedSummaryItemList.add(oneStopUtils.convertSummaryFromVettedOrder(dispOrder));
		}

		for (DispOrder dispOrder : dispDhDeleteOrderList) {
			oneStopVettedSummaryItemList.add(oneStopUtils.convertSummaryFromVettedOrder(dispOrder));
		}
				
		return oneStopVettedSummaryItemList;
	}
	
	@SuppressWarnings(UNCHECKED)
	private List<OneStopOrderItemSummary> genUnvetSummaryItemList(String hkid,  Boolean displayAllMoe) {
		List<OneStopOrderItemSummary> oneStopUnvetSummaryItemList = new ArrayList<OneStopOrderItemSummary>();
		
		if (propMap.getValueAsBoolean(PATIENT_PAS_PATIENT_ENABLED.getName()))  {
			Map<String, Object> hkpmiResult = oneStopPasManager.retrieveHkPmiPatientWithCaseListByHkid(hkid, displayAllMoe);
			if (hkpmiResult.containsKey(PATIENT)) {
				defaultPatient = (Patient) hkpmiResult.get(PATIENT);
			}
			
			if (hkpmiResult.containsKey(HKPMI_MEDCASE_LIST)) {
				List<MedOrder> medOrderList = retrieveUnvetOrderByHkid(displayAllMoe, (List<String>) hkpmiResult.get(HKPMI_MEDCASE_LIST));
				
				for (MedOrder medOrder : medOrderList) {
					if (medOrder.getPharmOrderList().isEmpty()) {
						oneStopUnvetSummaryItemList.add(oneStopUtils.convertSummaryFromUnvetOrder(medOrder));
					}
				}
			}
		}
		
		return oneStopUnvetSummaryItemList;
	}
	
	private List<OneStopRefillItemSummary> genRefillItemSummaryList (String hkid) {
		List<OneStopRefillItemSummary> oneStopRefillItemSummaryList = new ArrayList<OneStopRefillItemSummary>();
		List<RefillSchedule> stdRefillScheduleList = retrieveStdRefillOrderByHkid(hkid);
		List<RefillSchedule> sfiRefillScheduleList = retrieveSfiRefillOrderByHkid(hkid);
				
		if (!stdRefillScheduleList.isEmpty()) {
			oneStopRefillItemSummaryList.addAll(oneStopUtils.convertRefillSfiRefillOrder(stdRefillScheduleList, false));
		}
		
		if (!sfiRefillScheduleList.isEmpty()) {			
			oneStopRefillItemSummaryList.addAll(oneStopUtils.convertRefillSfiRefillOrder(sfiRefillScheduleList, false));
		}
		
		return oneStopRefillItemSummaryList;
	}
	
	private String convertSfiRefillCouponNumToOrderNum(String sfiRefillCouponNum) {
		return "MOE" + sfiRefillCouponNum.substring(0, 3) + sfiRefillCouponNum.substring(6, 15);
	}
	
	@SuppressWarnings(UNCHECKED)
	private void retrieveOrderByCapdVoucherNum(String capdVoucherNum)
	{
		oneStopSummary = new OneStopSummary();
		oneStopSummary.setSearchTextType(searchTextType);
		
				
		List<CapdVoucher> capdVoucherNumList = em.createQuery(
				"select o from CapdVoucher o " + // 20150206 index check : CapdVoucher.voucherNum : I_CAPD_VOUCHER_01
				" where o.voucherNum = :voucherNum" +
				" and o.dispOrder.ticket.workstore = :workstore")
				.setParameter("voucherNum", capdVoucherNum)
				.setParameter(WORKSTORE, workstore)
				.setHint(QueryHints.FETCH, "o.dispOrder")
				.setHint(QueryHints.FETCH, "o.dispOrder.ticket")
				.getResultList();
		
		if (!capdVoucherNumList.isEmpty())
		{
			List<OneStopOrderItemSummary> oneStopOrderItemSummaryList = new ArrayList<OneStopOrderItemSummary>();
			OneStopOrderItemSummary oneStopOrderItemSummary = new OneStopOrderItemSummary();
			Ticket ticket = capdVoucherNumList.get(0).getDispOrder().getTicket();
			oneStopOrderItemSummary.setTicketDate(ticket.getTicketDate());
			oneStopOrderItemSummary.setTicketNum(ticket.getTicketNum());
			oneStopOrderItemSummaryList.add(oneStopOrderItemSummary);
			oneStopSummary.setOneStopOrderItemSummaryList(oneStopOrderItemSummaryList);
		}
		else
		{
			errMsg = "0715";
			logger.info("notification that no order is found (search by capd voucher num) with errMsg code: #0", errMsg);
		}
		
	}
	
	private void handlePatientInfo(String patHospCode, List<OneStopOrderItemSummary> oneStopUnvetSummaryItemList, Boolean callPasAppointment) {
		if (propMap.getValueAsBoolean(PATIENT_PAS_APPT_ENABLED.getName())) {
			//retrieve appointment list from pas and map the appointment list, exclude DOH cases
			if (callPasAppointment)
			{
				appointmentList = oneStopPasManager.retrieveOpasAppointmentList(patHospCode, defaultPatient.getPatKey());
				logger.debug("Appointment list size: #0", (appointmentList == null?"empty":appointmentList.size()));
			}
			
			if (appointmentList != null) {
				Collections.sort(appointmentList, new ComparatorForAppointmentList());
				for (MedCase medCase : appointmentList) {
					phsSpecAndWardMappingForAppointment(medCase, patHospCode);
				}
			}
			
			for (OneStopOrderItemSummary oneStopOrderItemSummary : oneStopUnvetSummaryItemList) {
				//retrive unevt ip order ha status
				if (oneStopUtils.isInPatCase(oneStopOrderItemSummary.getMedCase().getCaseNum())) {
					if (propMap.getValueAsBoolean(PATIENT_PAS_PATIENT_ENABLED.getName())) {
						retrieveUnvetInPatAttendAndHaStaffByCaseNumAndHkid(oneStopOrderItemSummary);
					}
					
				} else { //retrieve unvet op order appointment and status
					//check appointment exists by checking appointment sequence is -1, -1 means no appointment
					if (checkPasApptSeqExits(oneStopOrderItemSummary.getMedCase())) {
						retrieveUnvetOutPatAttendAndHaStaffByAppointmentSequence(oneStopOrderItemSummary);
					} else {
						retrieveUnvetOutPatAttendAndHaStaffWithoutAppointmentSequence(oneStopOrderItemSummary);
					}
				}
				
				//map unvet phs specialty
				phsSpecAndWardMappingForOrder(oneStopOrderItemSummary);
			}
		} else {
			if (propMap.getValueAsBoolean(PATIENT_PAS_PATIENT_ENABLED.getName())) {
				for (OneStopOrderItemSummary oneStopOrderItemSummary : oneStopUnvetSummaryItemList) {
					//get unvet ip order attendance
					if (oneStopUtils.isInPatCase(oneStopOrderItemSummary.getMedCase().getCaseNum()))  {
						retrieveUnvetInPatAttendAndHaStaffByCaseNumAndHkid(oneStopOrderItemSummary);
					}
					
					//map unvet phs specialty
					phsSpecAndWardMappingForOrder(oneStopOrderItemSummary);
				}
			}
		}
	}
	
	private boolean checkPasApptSeqExits(MedCase medCase) {
		return (medCase.getPasApptSeq() != null && 
				medCase.getPasApptSeq().longValue() != -1L);
	}
	
	@SuppressWarnings(UNCHECKED)
	private List<MedOrder> retrieveUnvetOrderByHkid(Boolean displayAllMoe, List<String> hkpmiMedCaseList) 
	{		
		StringBuilder sb = new StringBuilder(
				"select o from MedOrder o" + // 20120225 index check : MedCase.caseNum : I_MED_CASE_01
				" where o.medCase.caseNum in :hkpmiMedCaseList" +
				" and o.patHospCode in :patHospCode" +
				" and o.docType = :docType" +
				" and o.orderType = :orderType" +
				" and o.vetDate is null");
		
		if (!displayAllMoe)
		{
			sb.append(" and (o.status in :status or (o.status = :status1 and o.ticket is not null))");
			sb.append(" and o.orderDate >= :orderDate");
		}
		else
		{
			sb.append(" and o.status in :status");
		}
	
		Query q = em.createQuery(sb.toString());
		q.setParameter(PAT_HOSP_CODE, oneStopUtils.retrieveFriendClubListAndDefaultPatHospCode());		
		q.setParameter(DOC_TYPE, MedOrderDocType.Normal);
		q.setParameter(ORDER_TYPE, OrderType.OutPatient);
		
		if (!displayAllMoe) 
		{
			q.setParameter(STATUS, MedOrderStatus.Outstanding_Deleted_Withhold_PreVet);
			q.setParameter(STATUS_1, MedOrderStatus.Deleted);
			q.setParameter(ORDER_DATE, new DateTime().minusDays(7).toDate(), TemporalType.DATE);
		}
		else
		{
			q.setParameter(STATUS, MedOrderStatus.Outstanding_Deleted_Withhold_PreVet);
		}

		return QueryUtils.splitExecute(q, HKPMI_MEDCASE_LIST, hkpmiMedCaseList);		
	}
	
	@SuppressWarnings(UNCHECKED)
	private List<DispOrder> retrieveVettedOrderByHkid(String hkid, Boolean displayAllMoe) 
	{
		StringBuilder sb = new StringBuilder(
				"select o from DispOrder o" + // 20120225 index check : PharmOrder.hkid : I_PHARM_ORDER_01
				" where o.pharmOrder.hkid = :hkid" +
				" and o.refillFlag = :refillFlag" +
				" and o.pharmOrder.medOrder.patHospCode in :patHospCode" +
				" and o.pharmOrder.medOrder.status in :status" +
				" and o.pharmOrder.medOrder.docType = :docType" +
				" and o.pharmOrder.status <> :pharmOrderStatus" +
				" and o.pharmOrder.medOrder.orderType = :orderType");
		
		if (!displayAllMoe) {
			sb.append(" and (o.pharmOrder.medOrder.orderDate >= :orderDate or" +
					" (o.pharmOrder.medOrder.vetDate >= :today and o.pharmOrder.medOrder.vetDate < :tomorrow))");
		}
		
		Query q = em.createQuery(sb.toString());
		q.setParameter(HKID, hkid);
		q.setParameter(REFILL_FLAG, Boolean.FALSE);
		q.setParameter(PAT_HOSP_CODE, oneStopUtils.retrieveFriendClubListAndDefaultPatHospCode());
		q.setParameter(STATUS, MedOrderStatus.Complete_Deleted);
		q.setParameter(DOC_TYPE, MedOrderDocType.Normal);
		q.setParameter(PHARM_ORDER_STATUS, PharmOrderStatus.SysDeleted);
		q.setParameter(ORDER_TYPE, OrderType.OutPatient);
		
		if (!displayAllMoe) 
		{
			q.setParameter(ORDER_DATE, new DateTime().minusDays(7).toDate(), TemporalType.DATE);
			q.setParameter(TODAY, new DateTime().toDate(), TemporalType.DATE);
			q.setParameter(TOMORROW, new DateTime().plusDays(1).toDate(), TemporalType.DATE);
		}
		
		return q.getResultList();
	}
	
	@SuppressWarnings(UNCHECKED)
	private List<DispOrder> retrieveVettedManualOrderByHkid(String hkid, Boolean displayAllMoe) 
	{
		StringBuilder sb = new StringBuilder(
				"select o from DispOrder o" + // 20120225 index check : PharmOrder.hkid : I_PHARM_ORDER_01
				" where o.pharmOrder.hkid = :hkid" +
				" and o.refillFlag = :refillFlag" +
				" and o.pharmOrder.workstore.hospCode = :hospCode" +
				" and o.pharmOrder.medOrder.status in :status" +
				" and o.pharmOrder.medOrder.docType = :docType" +
				" and o.pharmOrder.status <> :pharmOrderStatus" +
				" and o.pharmOrder.medOrder.orderType = :orderType");
		
		if (!displayAllMoe) 
		{
			sb.append(" and o.pharmOrder.medOrder.orderDate >= :orderDate");
		}
		
		Query q = em.createQuery(sb.toString());
		q.setParameter(HKID, hkid);
		q.setParameter(REFILL_FLAG, Boolean.FALSE);
		q.setParameter(HOSP_CODE, workstore.getHospCode());
		q.setParameter(DOC_TYPE, MedOrderDocType.Manual);
		q.setParameter(PHARM_ORDER_STATUS, PharmOrderStatus.SysDeleted);
		q.setParameter(ORDER_TYPE, OrderType.OutPatient);
		
		if (!displayAllMoe) 
		{
			q.setParameter(STATUS, Arrays.asList(MedOrderStatus.Complete));
			q.setParameter(ORDER_DATE, new DateTime().minusDays(7).toDate(), TemporalType.DATE);
		}
		else
		{
			q.setParameter(STATUS, MedOrderStatus.Complete_Deleted);
		}
		
		return (List<DispOrder>) q.getResultList();
	}
	
	@SuppressWarnings(UNCHECKED)
	private List<DispOrder> retrieveVettedDeleteDhOrderByHkid(String hkid, Boolean displayAllMoe) 
	{
		if ( displayAllMoe ) { // Delete Dh Order will retrieve in retrieveVettedManualOrderByHkid
			return new ArrayList<DispOrder>();
		}
		
		StringBuilder sb = new StringBuilder(
				"select o from DispOrder o" + // 20120225 index check : PharmOrder.hkid : I_PHARM_ORDER_01
				" where o.pharmOrder.hkid = :hkid" +
				" and o.refillFlag = :refillFlag" +
				" and o.pharmOrder.workstore.hospCode = :hospCode" +
				" and o.pharmOrder.medOrder.status in :status" +
				" and o.pharmOrder.medOrder.docType = :docType" +
				" and o.pharmOrder.status <> :pharmOrderStatus" +
				" and o.pharmOrder.medOrder.orderType = :orderType" +
				" and o.pharmOrder.medOrder.prescType = :prescType");
		
		if (!displayAllMoe) 
		{
			sb.append(" and o.pharmOrder.medOrder.orderDate >= :orderDate");
		}
		
		Query q = em.createQuery(sb.toString());
		q.setParameter(HKID, hkid);
		q.setParameter(REFILL_FLAG, Boolean.FALSE);
		q.setParameter(HOSP_CODE, workstore.getHospCode());
		q.setParameter(DOC_TYPE, MedOrderDocType.Manual);
		q.setParameter(PHARM_ORDER_STATUS, PharmOrderStatus.SysDeleted);
		q.setParameter(ORDER_TYPE, OrderType.OutPatient);
		q.setParameter(PRESC_TYPE, MedOrderPrescType.Dh);
		q.setParameter(STATUS, Arrays.asList(MedOrderStatus.Deleted));
		
		if (!displayAllMoe) 
		{
			q.setParameter(ORDER_DATE, new DateTime().minusDays(7).toDate(), TemporalType.DATE);
		}
		
		return (List<DispOrder>) q.getResultList();
	}
	
	@SuppressWarnings(UNCHECKED)
	private List<RefillSchedule> retrieveStdRefillOrderByHkid(String hkid) 
	{
		List<Long> pharmOrderIdList = (List<Long>) em.createQuery(
				"select distinct o.pharmOrder.id from RefillSchedule o" + // 20120304 index check : PharmOrder.hkid : I_PHARM_ORDER_01
				" where o.pharmOrder.hkid = :hkid" +
				" and o.pharmOrder.workstore.hospCode = :hospCode" +
				" and o.pharmOrder.status not in :pharmOrderStatus" +
				" order by o.groupNum, o.refillNum")
				.setParameter(HKID, hkid)
				.setParameter(HOSP_CODE, workstore.getHospCode())
				.setParameter(PHARM_ORDER_STATUS, PharmOrderStatus.Deleted_SysDeleted)
				.getResultList();
		
		if (!pharmOrderIdList.isEmpty()) 
		{			
			List<RefillSchedule> refillScheduleList = (List<RefillSchedule>) em.createQuery(
					"select o from RefillSchedule o" + // 20120225 index check : RefillSchedule.pharmOrder : FK_REFILL_SCHEDULE_01
					" where o.pharmOrder.id in :pharmOrderId" +
					" and (o.refillStatus in :refillStatus or o.refillNum = 1)" +
					" and o.status not in :status" +
					" and o.docType = :docType" +
					" order by o.groupNum, o.refillNum")
					.setParameter(PHARM_ORDER_ID, pharmOrderIdList)
					.setParameter(REFILL_STATUS, Arrays.asList(RefillScheduleRefillStatus.Current, RefillScheduleRefillStatus.Next))
					.setParameter(STATUS, Arrays.asList(RefillScheduleStatus.Abort))
					.setParameter(DOC_TYPE, DocType.Standard)
					.getResultList();
			
			if (refillScheduleList.isEmpty())
			{
				refillScheduleList = retrievePreviousStdRefillOrderBeforeAbortOrder(pharmOrderIdList);
			}
			
			return refillScheduleList;
		}
		
		return new ArrayList<RefillSchedule>();
	}
	
	@SuppressWarnings(UNCHECKED)
	private List<RefillSchedule> retrieveSfiRefillOrderByHkid(String hkid) 
	{
		List<Long> pharmOrderIdList = (List<Long>) em.createQuery(
				"select distinct o.pharmOrder.id from RefillSchedule o" + // 20120304 index check : PharmOrder.hkid : I_PHARM_ORDER_01
				" where o.pharmOrder.hkid = :hkid" +
				" and o.pharmOrder.workstore.hospCode = :hospCode" +
				" and o.pharmOrder.status not in :pharmOrderStatus" +
				" order by o.groupNum, o.refillNum")
				.setParameter(HKID, hkid)
				.setParameter(HOSP_CODE, workstore.getHospCode())
				.setParameter(PHARM_ORDER_STATUS, PharmOrderStatus.Deleted_SysDeleted)
				.getResultList();
		
		if (!pharmOrderIdList.isEmpty()) 
		{
			return (List<RefillSchedule>) em.createQuery(
					"select o from RefillSchedule o" + // 20120225 index check : RefillSchedule.pharmOrder : FK_REFILL_SCHEDULE_01
					" where o.pharmOrder.id in :pharmOrderId" +
					" and o.docType = :docType" +
					" and (o.refillStatus in :refillStatus or o.refillNum = 1)" +
					" order by o.groupNum, o.refillNum")
					.setParameter(PHARM_ORDER_ID, pharmOrderIdList)
					.setParameter(DOC_TYPE, DocType.Sfi)
					.setParameter(REFILL_STATUS, RefillScheduleRefillStatus.Current_Next)
					.getResultList();
		}
		
		return new ArrayList<RefillSchedule>();
	}
		
	//Fulfill not implement yet until phase two
	public void retrieveOrderByCaseNum(String caseNum, Boolean displayAllMoe) 
	{
		List<OneStopOrderItemSummary> oneStopOrderItemSummaryList = new ArrayList<OneStopOrderItemSummary>();
		List<OneStopRefillItemSummary> oneStopRefillItemSummaryList = new ArrayList<OneStopRefillItemSummary>();
		caseNumVettedList = new ArrayList<OneStopOrderItemSummary>();
		
		
		if (oneStopHelper.isDohCase(caseNum))
		{
			oneStopOrderItemSummaryList = corpPmsServiceProxy.retrieveOrderByCaseNum(caseNum);
		}
		else
		{
			caseNumUnvetList = new ArrayList<OneStopOrderItemSummary>();
						
			List<MedOrder> medOrderList = retrieveUnvetOrderByCaseNum(caseNum, displayAllMoe);
			
			for (MedOrder medOrder : medOrderList) 
			{
				if (medOrder.getPharmOrderList().isEmpty()) 
				{
					caseNumUnvetList.add(oneStopUtils.convertSummaryFromUnvetOrder(medOrder));
				}
			}
			
			List<DispOrder> dispMoeOrderList = retrieveVettedOrderByCaseNum(caseNum, displayAllMoe);
			
			for (DispOrder dispOrder : dispMoeOrderList) 
			{
				caseNumVettedList.add(oneStopUtils.convertSummaryFromVettedOrder(dispOrder));
			}
			
			oneStopOrderItemSummaryList.addAll(caseNumUnvetList);
		}	
			
		List<DispOrder> dispManualOrderList = retrieveVettedManualOrderByCaseNum(caseNum, displayAllMoe);
		
		for (DispOrder dispOrder : dispManualOrderList) 
		{
			caseNumVettedList.add(oneStopUtils.convertSummaryFromVettedOrder(dispOrder));
		}
					
		List<RefillSchedule> stdRefillScheduleList = retrieveStdRefillOrderByCaseNum(caseNum);
		List<RefillSchedule> sfiRefillScheduleList = retrieveSfiRefillOrderByCaseNum(caseNum);
			
		if (!stdRefillScheduleList.isEmpty()) 
		{
			oneStopRefillItemSummaryList.addAll(oneStopUtils.convertRefillSfiRefillOrder(stdRefillScheduleList, false));
		}
			
		if (!sfiRefillScheduleList.isEmpty()) 
		{
			oneStopRefillItemSummaryList.addAll(oneStopUtils.convertRefillSfiRefillOrder(sfiRefillScheduleList, false));
		}
				
		oneStopOrderItemSummaryList.addAll(caseNumVettedList);
		
		convertOneStopSummary(oneStopOrderItemSummaryList, oneStopRefillItemSummaryList);
	}
			
	@SuppressWarnings(UNCHECKED)
	private List<MedOrder> retrieveUnvetOrderByCaseNum(String caseNum, Boolean displayAllMoe) 
	{
		StringBuilder sb = new StringBuilder(
				"select o from MedOrder o" + // 20120225 index check : MedCase.caseNum : I_MED_CASE_01
				" where o.medCase.caseNum = :caseNum" +
				" and o.patHospCode in :patHospCode" +
				" and o.docType = :docType" +
				" and o.orderType = :orderType" +
				" and o.vetDate is null");

		if (!displayAllMoe) 
		{
			sb.append(" and (o.status in :status or (o.status = :status1 and o.ticket is not null))");
			sb.append(" and o.orderDate >= :orderDate");
		}
		else
		{
			sb.append(" and o.status in :status");
		}

		Query q = em.createQuery(sb.toString());
		q.setParameter(CASE_NUM, caseNum);		
		q.setParameter(PAT_HOSP_CODE, oneStopUtils.retrieveFriendClubListAndDefaultPatHospCode());
		q.setParameter(DOC_TYPE, MedOrderDocType.Normal);
		q.setParameter(ORDER_TYPE, OrderType.OutPatient);
		
		if (!displayAllMoe) {
			q.setParameter(STATUS, MedOrderStatus.Outstanding_Deleted_Withhold_PreVet);
			q.setParameter(STATUS_1, MedOrderStatus.Deleted);
			q.setParameter(ORDER_DATE, new DateTime().minusDays(7).toDate(), TemporalType.DATE);
		}
		else
		{
			q.setParameter(STATUS, MedOrderStatus.Outstanding_Deleted_Withhold_PreVet);
		}

		return (List<MedOrder>) q.getResultList();
	}
	
	@SuppressWarnings(UNCHECKED)
	private List<DispOrder> retrieveVettedOrderByCaseNum(String caseNum, Boolean displayAllMoe) 
	{
		StringBuilder sb = new StringBuilder(
				"select o from DispOrder o" + // 20120225 index check : MedCase.caseNum : I_MED_CASE_01
				" where o.pharmOrder.medCase.caseNum = :caseNum" +
				" and o.pharmOrder.medOrder.status in :status" +
				" and o.refillFlag = :refillFlag" +
				" and o.pharmOrder.medOrder.patHospCode in :patHospCode" +
				" and o.pharmOrder.medOrder.docType = :docType" +
				" and o.pharmOrder.status <> :pharmOrderStatus" +
				" and o.pharmOrder.medOrder.orderType = :orderType");

		if (!displayAllMoe) {
			sb.append(" and (o.pharmOrder.medOrder.orderDate >= :orderDate or" +
					" (o.pharmOrder.medOrder.vetDate > :today and o.pharmOrder.medOrder.vetDate < :tomorrow))");
		}
		
		Query q = em.createQuery(sb.toString());
		q.setParameter(CASE_NUM, caseNum);
		q.setParameter(STATUS, MedOrderStatus.Complete_Deleted);
		q.setParameter(REFILL_FLAG, Boolean.FALSE);
		q.setParameter(PAT_HOSP_CODE, oneStopUtils.retrieveFriendClubListAndDefaultPatHospCode());
		q.setParameter(DOC_TYPE, MedOrderDocType.Normal);
		q.setParameter(PHARM_ORDER_STATUS, PharmOrderStatus.SysDeleted);
		q.setParameter(ORDER_TYPE, OrderType.OutPatient);
		
		if (!displayAllMoe) {
			q.setParameter(ORDER_DATE, new DateTime().minusDays(7).toDate(), TemporalType.DATE);
			q.setParameter(TODAY, new DateTime().toDate(), TemporalType.DATE);
			q.setParameter(TOMORROW, new DateTime().plusDays(1).toDate(), TemporalType.DATE);
		}
		
		return (List<DispOrder>) q.getResultList();
	}
	
	@SuppressWarnings(UNCHECKED)
	private List<DispOrder> retrieveVettedManualOrderByCaseNum(String caseNum, Boolean displayAllMoe) 
	{
		StringBuilder sb = new StringBuilder(
				"select o from DispOrder o" + // 20120225 index check : MedCase.caseNum : I_MED_CASE_01
				" where o.pharmOrder.medCase.caseNum = :caseNum" +
				" and o.pharmOrder.medOrder.status in :status" +
				" and o.refillFlag = :refillFlag" +
				" and o.pharmOrder.workstore.hospCode = :hospCode" +
				" and o.pharmOrder.medOrder.docType = :docType" +
				" and o.pharmOrder.status <> :pharmOrderStatus" +
				" and o.pharmOrder.medOrder.orderType = :orderType");

		if (!displayAllMoe) {
			sb.append(" and o.pharmOrder.medOrder.orderDate >= :orderDate");
		}
		
		if (oneStopHelper.isDohCase(caseNum))
		{
			sb.append(" and o.pharmOrder.medOrder.prescType <> :prescType");
		}
		
		Query q = em.createQuery(sb.toString());
		q.setParameter(CASE_NUM, caseNum);
		q.setParameter(REFILL_FLAG, Boolean.FALSE);
		q.setParameter(HOSP_CODE, workstore.getHospCode());
		q.setParameter(DOC_TYPE, MedOrderDocType.Manual);
		q.setParameter(PHARM_ORDER_STATUS, PharmOrderStatus.SysDeleted);
		q.setParameter(ORDER_TYPE, OrderType.OutPatient);
		
		if (!displayAllMoe) {
			q.setParameter(STATUS, Arrays.asList(MedOrderStatus.Complete));
			q.setParameter(ORDER_DATE, new DateTime().minusDays(7).toDate(), TemporalType.DATE);
		}
		else {
			q.setParameter(STATUS, MedOrderStatus.Complete_Deleted);
		}
		
		if (oneStopHelper.isDohCase(caseNum))
		{
			q.setParameter(PRESC_TYPE, MedOrderPrescType.Dh);
		}
		
		return (List<DispOrder>) q.getResultList();
	}
	
	@SuppressWarnings(UNCHECKED)
	private List<RefillSchedule> retrieveStdRefillOrderByCaseNum(String caseNum)
	{
		List<Long> pharmOrderIdList = (List<Long>) em.createQuery(
				"select distinct o.pharmOrder.id from RefillSchedule o" + // 20120225 index check : MedCase.caseNum : I_MED_CASE_01
				" where o.pharmOrder.medCase.caseNum = :caseNum" +
				" and o.pharmOrder.workstore.hospCode = :hospCode" +
				" and o.pharmOrder.status not in :pharmOrderStatus" +
				" order by o.groupNum, o.refillNum")
				.setParameter(CASE_NUM, caseNum)
				.setParameter(HOSP_CODE, workstore.getHospCode())
				.setParameter(PHARM_ORDER_STATUS, PharmOrderStatus.Deleted_SysDeleted)
				.getResultList();
	
		if (!pharmOrderIdList.isEmpty()) 
		{
			List<RefillSchedule> refillScheduleList = (List<RefillSchedule>) em.createQuery(
					"select o from RefillSchedule o" + // 20120225 index check : RefillSchedule.pharmOrder : FK_REFILL_SCHEDULE_01
					" where o.pharmOrder.id in :pharmOrderId" +
					" and (o.refillStatus in :refillStatus or o.refillNum = 1)" +
					" and o.status not in :status" +
					" and o.docType = :docType" +
					" order by o.groupNum, o.refillNum")
					.setParameter(PHARM_ORDER_ID, pharmOrderIdList)
					.setParameter(REFILL_STATUS, Arrays.asList(RefillScheduleRefillStatus.Current, RefillScheduleRefillStatus.Next))
					.setParameter(STATUS, Arrays.asList(RefillScheduleStatus.Abort))
					.setParameter(DOC_TYPE, DocType.Standard)
					.getResultList();
			
			if (refillScheduleList.isEmpty())
			{
				refillScheduleList = retrievePreviousStdRefillOrderBeforeAbortOrder(pharmOrderIdList);
			}
			
			return refillScheduleList;
		}
		
		return new ArrayList<RefillSchedule>();
	}
	
	@SuppressWarnings(UNCHECKED)
	private List<RefillSchedule> retrieveSfiRefillOrderByCaseNum(String caseNum)
	{
		List<Long> pharmOrderIdList = (List<Long>) em.createQuery(
				"select distinct o.pharmOrder.id from RefillSchedule o" + // 20120225 index check : MedCase.caseNum : I_MED_CASE_01
				" where o.pharmOrder.medCase.caseNum = :caseNum" +
				" and o.pharmOrder.workstore.hospCode = :hospCode" +
				" and o.pharmOrder.status not in :pharmOrderStatus" +
				" order by o.groupNum, o.refillNum")
				.setParameter(CASE_NUM, caseNum)
				.setParameter(HOSP_CODE, workstore.getHospCode())
				.setParameter(PHARM_ORDER_STATUS, PharmOrderStatus.Deleted_SysDeleted)
				.getResultList();

		if (!pharmOrderIdList.isEmpty()) 
		{
			return (List<RefillSchedule>) em.createQuery(
					"select o from RefillSchedule o" + // 20120225 index check : RefillSchedule.pharmOrder : FK_REFILL_SCHEDULE_01
					" where o.pharmOrder.id in :pharmOrderId" +
					" and o.docType = :docType" +
					" and (o.refillStatus in :refillStatus or o.refillNum = 1)" +
					" order by o.groupNum, o.refillNum")
					.setParameter(PHARM_ORDER_ID, pharmOrderIdList)
					.setParameter(DOC_TYPE, DocType.Sfi)
					.setParameter(REFILL_STATUS, RefillScheduleRefillStatus.Current_Next)
					.getResultList();
		}
		
		return new ArrayList<RefillSchedule>();
	}
		
	public void retrieveHkPmiPatientAndAppointmentByCaseNum(String caseNum, List<OneStopOrderItemSummary> caseNumUnvetList, List<OneStopOrderItemSummary> caseNumVettedList) 
	{
		String patHospCode = StringUtils.isBlank(workstore.getDefPatHospCode())?workstore.getHospCode():workstore.getDefPatHospCode();
		
		if (oneStopHelper.isDohCase(caseNum)) 
		{
			oneStopOrderInfo.setPatHospCode(patHospCode);
			if (oneStopSummary.getOneStopOrderItemSummaryList().size() == 0 && oneStopSummary.getOneStopRefillItemSummaryList().size() == 0)
			{
				defaultPatient = new Patient();
				errMsg = NO_HKPMI_PATIENT_FOUND;
				logger.info("no need to call hkpmi for DOH case, use hkid search instead");
			}
			else
			{
				if (!oneStopSummary.getOneStopOrderItemSummaryList().isEmpty())
				{
					String hkid = oneStopSummary.getOneStopOrderItemSummaryList().get(0).getPatient().getHkid();
					if (!StringUtils.isBlank(hkid))
					{
						defaultPatient = oneStopPasManager.retrieveHkPmiPatientByHkid(hkid);
						if (defaultPatient == null)
						{
							defaultPatient = oneStopSummary.getOneStopOrderItemSummaryList().get(0).getPatient();
						}
						
						for (OneStopOrderItemSummary oneStopOrderItemSummary : oneStopSummary.getOneStopOrderItemSummaryList()) 
						{
							if (oneStopOrderItemSummary.getDispOrderId() == null) 
							{
								oneStopOrderItemSummary.setPatient(defaultPatient);
							}
							else 
							{
								DispOrder dispOrder = em.find(DispOrder.class, oneStopOrderItemSummary.getDispOrderId());
								if (dispOrder != null) 
								{
									oneStopOrderItemSummary = oneStopUtils.constructDispendedOrderStatus(oneStopOrderItemSummary, dispOrder, dispOrder.getPharmOrder(), dispOrder.getPharmOrder().getMedOrder());
								}
							}
						}
					}
					else
					{
						defaultPatient = oneStopSummary.getOneStopOrderItemSummaryList().get(0).getPatient();						
						for (OneStopOrderItemSummary oneStopOrderItemSummary : oneStopSummary.getOneStopOrderItemSummaryList()) 
						{
							if (oneStopOrderItemSummary.getDispOrderId() != null) 
							{
								DispOrder dispOrder = em.find(DispOrder.class, oneStopOrderItemSummary.getDispOrderId());
								if (dispOrder != null) 
								{
									oneStopOrderItemSummary = oneStopUtils.constructDispendedOrderStatus(oneStopOrderItemSummary, dispOrder, dispOrder.getPharmOrder(), dispOrder.getPharmOrder().getMedOrder());
								}
							}
						}
					}

					Collections.sort(oneStopSummary.getOneStopOrderItemSummaryList(), new ComparatorForOrderList(workstore));
				}
				else if (!oneStopSummary.getOneStopRefillItemSummaryList().isEmpty())
				{
					Collections.sort(oneStopSummary.getOneStopRefillItemSummaryList(), new ComparatorForRefillList());
					defaultPatient = oneStopSummary.getOneStopRefillItemSummaryList().get(0).getPatient();
				}
			}
			
			if ( defaultPatient != null ) {
				oneStopOrderInfo.setEhrPatient(defaultPatient.getEhrPatient());
			}
			
			return;
		}
		
		RequireAppointmentRule requireAppointmentRule = new RequireAppointmentRule();

		if (propMap.getValueAsBoolean(PATIENT_PAS_PATIENT_ENABLED.getName()))
		{
			defaultPatient = oneStopPasManager.retrieveHkpmiPatientByCaseNum(patHospCode, caseNum);
			if (defaultPatient != null) {
				requireAppointmentRule.setPatHospCode(patHospCode);
				requireAppointmentRule.setPasSpecCode(defaultPatient.getMedCaseList().get(0).getPasSpecCode());
				oneStopOrderInfo.setNoAppointmentRequired(patSpecNoApptMapping.checkPatSpecApptRequire(requireAppointmentRule));
			}
				
			//If patient not found in hkpmi, loop for friend club
			if (defaultPatient == null)
			{
				for (HospitalMapping hospitalMapping : workstore.getHospital().getHospitalMappingList())
				{
					logger.debug("calling patient info from pas using case num #0, with friend code patHospCode #1", caseNum, hospitalMapping.getPatHospCode());
					defaultPatient = oneStopPasManager.retrieveHkpmiPatientByCaseNum(hospitalMapping.getPatHospCode(), caseNum);
					if (defaultPatient != null)
					{
						patHospCode = hospitalMapping.getPatHospCode();
						requireAppointmentRule.setPatHospCode(patHospCode);
						requireAppointmentRule.setPasSpecCode(defaultPatient.getMedCaseList().get(0).getPasSpecCode());
						oneStopOrderInfo.setNoAppointmentRequired(patSpecNoApptMapping.checkPatSpecApptRequire(requireAppointmentRule));
						break;
					}
				}
									
				if (defaultPatient == null && caseNumUnvetList.isEmpty() && caseNumVettedList.isEmpty() && oneStopSummary.getOneStopRefillItemSummaryList().isEmpty())
				{
					errMsg = NO_HKPMI_PATIENT_FOUND;
					logger.info("no patient found from hkpmi and database using case num, use hkid search instead");
				}
			}
		} 
		else if (caseNumUnvetList.isEmpty() && caseNumVettedList.isEmpty() && oneStopSummary.getOneStopRefillItemSummaryList().isEmpty()) 
		{
			errMsg = NO_HKPMI_PATIENT_FOUND;
			logger.info("hkpmi flag is off and no data is found from database using case num, use hkid search instead");
			return;
		}
		
		if (defaultPatient != null) {
			retrievePasAppointmentForCaseNum(patHospCode, caseNum, caseNumUnvetList);
			
		} else if (!caseNumUnvetList.isEmpty()) {
			retrievePasAppotintmentForUnvetOrderByCaseNumWithoutPMIPatient(caseNumUnvetList);
		}
				
		if (oneStopSummary.getOneStopOrderItemSummaryList() == null && oneStopSummary.getOneStopRefillItemSummaryList() == null) {
			errMsg = "0080";
		}
		
		if (defaultAppointment == null && propMap.getValueAsBoolean(PATIENT_PAS_PATIENT_ENABLED.getName()) && defaultPatient != null) 
		{
			if (!defaultPatient.getMedCaseList().isEmpty())
			{
				phsSpecAndWardMappingForCaseNumWithoutAssocaitaion(defaultPatient.getMedCaseList().get(0), patHospCode);
			}
		}
		
		if (defaultPatient == null) {
			defaultPatient = new Patient();
		} else {
			oneStopOrderInfo.setEhrPatient(defaultPatient.getEhrPatient());
		}
		
		if (oneStopSummary.getOneStopOrderItemSummaryList() != null)
		{	
			oneStopSummary.getOneStopOrderItemSummaryList().clear();
			oneStopSummary.getOneStopOrderItemSummaryList().addAll(caseNumUnvetList);
			oneStopSummary.getOneStopOrderItemSummaryList().addAll(caseNumVettedList);
			Collections.sort(oneStopSummary.getOneStopOrderItemSummaryList(), new ComparatorForOrderList(workstore));
		}
		
		oneStopOrderInfo.setPatHospCode(patHospCode);
	}
	
	private void retrievePasAppotintmentForUnvetOrderByCaseNumWithoutPMIPatient(List<OneStopOrderItemSummary> caseNumUnvetList)
	{
		if (!propMap.getValueAsBoolean(PATIENT_PAS_APPT_ENABLED.getName()))
		{
			logger.debug("Opas is down and no appointment is retrieve");
			for (OneStopOrderItemSummary oneStopOrderItemSummary : caseNumUnvetList)
			{
				phsSpecAndWardMappingForOrder(oneStopOrderItemSummary);
			}
			return;
		}
		
		//check appointment exists by checking appointment sequence is -1, -1 means no appointment
		for (OneStopOrderItemSummary oneStopOrderItemSummary : caseNumUnvetList)
		{
			if (!oneStopUtils.isInPatCase(oneStopOrderItemSummary.getMedCase().getCaseNum()))
			{
				if (checkPasApptSeqExits(oneStopOrderItemSummary.getMedCase()))
				{
					retrieveUnvetOutPatAttendAndHaStaffByAppointmentSequence(oneStopOrderItemSummary);
				} 
				else 
				{
					retrieveUnvetOutPatAttendAndHaStaffWithoutAppointmentSequence(oneStopOrderItemSummary);
				}
			}
			
			phsSpecAndWardMappingForOrder(oneStopSummary.getOneStopOrderItemSummaryList().get(0));
		}
	}
	
	private void retrievePasAppointmentForCaseNum(String patHospCode, String caseNum, List<OneStopOrderItemSummary> caseNumUnvetList) {		
		if (oneStopUtils.isInPatCase(caseNum)) {
			defaultAppointment = defaultPatient.getMedCaseList().get(0);
			phsSpecAndWardMappingForCaseNumWithoutAssocaitaion(defaultAppointment, patHospCode);
		}
		
		if (propMap.getValueAsBoolean(PATIENT_PAS_APPT_ENABLED.getName()))
		{	
			//retrieve appointment list from pas if caseNum is not IP case
			if (!oneStopUtils.isInPatCase(caseNum)) {
				List<MedCase> filteredAppointmentList = new ArrayList<MedCase>();
				//retrieve appointment
				appointmentList = oneStopPasManager.retrieveOpasAppointmentList(patHospCode, defaultPatient.getPatKey());
				logger.debug("Appointment list size: #0", (appointmentList == null?"empty":appointmentList.size()));
				if (appointmentList != null) {
					Collections.sort(appointmentList, new ComparatorForAppointmentList());
					for (MedCase medCase : appointmentList) {
						phsSpecAndWardMappingForAppointment(medCase, patHospCode);
						if (caseNum.equals(medCase.getCaseNum())) 
						{
							filteredAppointmentList.add(medCase);
						}
					}
				}
			
				logger.debug("FilteredAppointmentList list size: #0", filteredAppointmentList.size());
				
				//auto associate medcase if casenum match with appointment list			
				if (filteredAppointmentList.size() == 1)
				{
					defaultAppointment = filteredAppointmentList.get(0);
				}
				else if (!filteredAppointmentList.isEmpty())
				{
					Collections.sort(filteredAppointmentList, new ComparatorForDefaultAppointment());
					Boolean associateAppointment = Boolean.FALSE;
					MedCase latestAppointment = filteredAppointmentList.get(0);
			
					for (MedCase medCase : filteredAppointmentList)
					{
						if (latestAppointment.getChargeFlag() != medCase.getChargeFlag()) 
						{
							associateAppointment = Boolean.FALSE;
							break;
						}
						
						if (latestAppointment.getChargeFlag())
						{												
							if (latestAppointment.equalsMedCase(medCase) &&
								drugChargeClearanceManager.checkStdDrugClearance(patHospCode, Long.valueOf(-1), latestAppointment.getPasApptSeq()) == drugChargeClearanceManager.checkStdDrugClearance(patHospCode, Long.valueOf(-1), medCase.getPasApptSeq()))
							{
								associateAppointment = Boolean.TRUE;
							}
							else
							{
								associateAppointment = Boolean.FALSE;
							}
						}
						else if (!latestAppointment.getChargeFlag())
						{
							if (latestAppointment.getPasSpecCode().equals(medCase.getPasSpecCode()) &&
								latestAppointment.getPasSubSpecCode().equals(medCase.getPasSubSpecCode()) &&
								latestAppointment.getChargeFlag().equals(medCase.getChargeFlag()))
							{
								associateAppointment = Boolean.TRUE;
							}
							else
							{
								associateAppointment = Boolean.FALSE;
							}
						}
					}
				
					if (associateAppointment)
					{
						defaultAppointment = filteredAppointmentList.get(0);
					}
				}
			}
			
			for (OneStopOrderItemSummary oneStopOrderItemSummary : caseNumUnvetList)
			{
				//retrive unevt ip order ha status
				if (oneStopUtils.isInPatCase(oneStopOrderItemSummary.getMedCase().getCaseNum())) 
				{
					if (propMap.getValueAsBoolean(PATIENT_PAS_PATIENT_ENABLED.getName())) 
					{
						retrieveUnvetInPatAttendAndHaStaffByCaseNumAndHkid(oneStopOrderItemSummary);
					}
					
				}
				else //retrieve unvet op order appointment and status
				{
					//check appointment exists by checking appointment sequence is -1, -1 means no appointment
					if (checkPasApptSeqExits(oneStopOrderItemSummary.getMedCase())) 
					{
						retrieveUnvetOutPatAttendAndHaStaffByAppointmentSequence(oneStopOrderItemSummary);
					} 
					else 
					{
						retrieveUnvetOutPatAttendAndHaStaffWithoutAppointmentSequence(oneStopOrderItemSummary);
					}
				}
				
				//map unvet phs specialty
				phsSpecAndWardMappingForOrder(oneStopOrderItemSummary);
			}
		}
		else //no opas flow, check in-patient case only
		{
			for (OneStopOrderItemSummary oneStopOrderItemSummary : caseNumUnvetList)
			{
				if (oneStopUtils.isInPatCase(oneStopOrderItemSummary.getMedCase().getCaseNum()))
				{
					if (propMap.getValueAsBoolean(PATIENT_PAS_PATIENT_ENABLED.getName()))
					{
						//call pas again to retrive In Pat info
						retrieveUnvetInPatAttendAndHaStaffByCaseNumAndHkid(oneStopOrderItemSummary);
					}	
				}
				
				phsSpecAndWardMappingForOrder(oneStopOrderItemSummary);
			}
		}
	}
	
	public void retrieveHkPmiPatientForVettedOrder() 
	{
		if (propMap.getValueAsBoolean(PATIENT_PAS_PATIENT_ENABLED.getName()))
		{
			String hkid;
			if (!oneStopSummary.getOneStopOrderItemSummaryList().isEmpty())
			{
				hkid = oneStopSummary.getOneStopOrderItemSummaryList().get(0).getPatient().getHkid();
			}
			else
			{
				hkid = oneStopSummary.getOneStopRefillItemSummaryList().get(0).getPatient().getHkid();
			}
			
			if (!StringUtils.isBlank(hkid))
			{
				defaultPatient = oneStopPasManager.retrieveHkPmiPatientByHkid(hkid);
				if (defaultPatient == null)
				{
					defaultPatient = new Patient();
					defaultPatient.setHkid(hkid);
				} else {
					oneStopOrderInfo.setEhrPatient(defaultPatient.getEhrPatient());
				}
			}
		}
	}
	
	public void retrieveHkPmiPatientByOrderNumOrRefNumOrTickNum() 
	{
		if (!propMap.getValueAsBoolean(PATIENT_PAS_PATIENT_ENABLED.getName())) 
		{
			logger.debug("Hkpmi is down and only call opas to retrieve appointment information for order");
			defaultPatient = new Patient();
			retrieveOpasAppointmentByOrderNumOrRefNumOrTickNum();
			return;
		}
	
		defaultPatient = oneStopPasManager.retrieveHkpmiPatientByCaseNum(oneStopSummary.getOneStopOrderItemSummaryList().get(0).getPatHospCode(), 
				  												  		 oneStopSummary.getOneStopOrderItemSummaryList().get(0).getMedCase().getCaseNum());
		if (defaultPatient != null)
		{
			oneStopOrderInfo.setEhrPatient(defaultPatient.getEhrPatient());
			if (oneStopUtils.isInPatCase(oneStopSummary.getOneStopOrderItemSummaryList().get(0).getMedCase().getCaseNum())) 
			{
				if (!defaultPatient.getMedCaseList().isEmpty()) 
				{
					oneStopSummary.getOneStopOrderItemSummaryList().get(0).getMedCase().setPasPayCode(defaultPatient.getMedCaseList().get(0).getPasPayCode());
					oneStopSummary.getOneStopOrderItemSummaryList().get(0).getMedCase().setPasPatGroupCode(defaultPatient.getMedCaseList().get(0).getPasPatGroupCode());
				}
				
				phsSpecAndWardMappingForOrder(oneStopSummary.getOneStopOrderItemSummaryList().get(0));
				return;
			}

			logger.debug("Patient is found in hkpmi: #0 - #1 and call opas to retrieve appointment information for order", defaultPatient.getName(), defaultPatient.getHkid());	
			retrieveOpasAppointmentByOrderNumOrRefNumOrTickNum();
		} 
		else 
		{
			logger.debug("no appointment information retrieved from order's apptSeq");
			errMsg = NO_HKPMI_PATIENT_FOUND;
		}
	}
			
	private void retrieveOpasAppointmentByOrderNumOrRefNumOrTickNum() 
	{
		if (!propMap.getValueAsBoolean(PATIENT_PAS_APPT_ENABLED.getName()))
		{
			logger.debug("Opas is down and no appointment is retrieve");
			phsSpecAndWardMappingForOrder(oneStopSummary.getOneStopOrderItemSummaryList().get(0));
			return;
		}
		
		//check appointment exists by checking appointment sequence is -1, -1 means no appointment
		if (!oneStopUtils.isInPatCase(oneStopSummary.getOneStopOrderItemSummaryList().get(0).getMedCase().getCaseNum()))
		{
			if (checkPasApptSeqExits(oneStopSummary.getOneStopOrderItemSummaryList().get(0).getMedCase()))
			{
				retrieveUnvetOutPatAttendAndHaStaffByAppointmentSequence(oneStopSummary.getOneStopOrderItemSummaryList().get(0));
			} 
			else 
			{
				retrieveUnvetOutPatAttendAndHaStaffWithoutAppointmentSequence(oneStopSummary.getOneStopOrderItemSummaryList().get(0));
			}
		}
		
		phsSpecAndWardMappingForOrder(oneStopSummary.getOneStopOrderItemSummaryList().get(0));
	}
	 
	private void convertOneStopSummary(List<OneStopOrderItemSummary> oneStopOrderItemSummaryList, List<OneStopRefillItemSummary> oneStopRefillItemSummaryList) 
	{
		if (!oneStopRefillItemSummaryList.isEmpty() || !oneStopOrderItemSummaryList.isEmpty()) 
		{
			oneStopSummary = new OneStopSummary();
			oneStopSummary.setSearchTextType(searchTextType);
			Collections.sort(oneStopRefillItemSummaryList, new ComparatorForRefillList());
			oneStopSummary.setOneStopRefillItemSummaryList(oneStopRefillItemSummaryList);
			Collections.sort(oneStopOrderItemSummaryList, new ComparatorForOrderList(workstore));
			oneStopSummary.setOneStopOrderItemSummaryList(oneStopOrderItemSummaryList);
		} 
		else 
		{	
			switch(searchTextType) {
				case Hkid:
					if (Contexts.getSessionContext().get("errMsg") != null) 
					{
						errMsg = (String) Contexts.getSessionContext().get("errMsg");
					}
					else if (defaultPatient.getHkid() != null) 
					{
						errMsg = "0080";
						logger.info("notification that no order is found but patient is found from hkpmi using hkid with errMsg code: #0", errMsg);
					}
					else 
					{
						errMsg = "0020";
						logger.info("no patient found from hkpmi using hkid with errMsg code: #0", errMsg);
					}
					break;
				case Case:
					oneStopSummary = new OneStopSummary();
					List<OneStopOrderItemSummary> dummyOneStopOrderItemSummaryList = new ArrayList<OneStopOrderItemSummary>();
					List<OneStopRefillItemSummary> dummyOneStopRefillItemSummaryList = new ArrayList<OneStopRefillItemSummary>();
					oneStopSummary.setOneStopOrderItemSummaryList(dummyOneStopOrderItemSummaryList);
					oneStopSummary.setOneStopRefillItemSummaryList(dummyOneStopRefillItemSummaryList);
					oneStopSummary.setSearchTextType(searchTextType);
					errMsg = "0080";
					logger.info("notification that no order is found but patient is found from hkpmi using case num with errMsg code: #0", errMsg);
					break;
					
				default:
					oneStopSummary = new OneStopSummary();
					oneStopSummary.setOneStopOrderItemSummaryList(oneStopOrderItemSummaryList);
					oneStopSummary.setOneStopRefillItemSummaryList(oneStopRefillItemSummaryList);
					oneStopSummary.setSearchTextType(searchTextType);
					errMsg = "0005";
					logger.info("notification that no order is found (search by order num, refill coupon num) with errMsg code: #0", errMsg);
					break;
			}
		}
	}
	
	private void retrieveUnvetInPatAttendAndHaStaffByCaseNumAndHkid(OneStopOrderItemSummary oneStopOrderItemSummary) {
		
		//call pas again to retrive in pat info
		Patient pasPatient = oneStopPasManager.retrieveHkpmiPatientByCaseNum(oneStopOrderItemSummary.getPatHospCode(), 
																			 oneStopOrderItemSummary.getMedCase().getCaseNum());
		
		if (pasPatient != null && !pasPatient.getMedCaseList().isEmpty()) 
		{
			oneStopOrderItemSummary.getMedCase().setPasPayCode(pasPatient.getMedCaseList().get(0).getPasPayCode());
			oneStopOrderItemSummary.getMedCase().setPasPatGroupCode(pasPatient.getMedCaseList().get(0).getPasPatGroupCode());
		}
	}
	
	private void retrieveUnvetOutPatAttendAndHaStaffByAppointmentSequence(OneStopOrderItemSummary oneStopOrderItemSummary) {			
				
		MedCase medCase = oneStopPasManager.retriveAppointmentAttendanceAndStatusByAppointmentSequence(oneStopOrderItemSummary.getPatHospCode(),
																									oneStopOrderItemSummary.getMedCase().getCaseNum(),
																									oneStopOrderItemSummary.getMedCase().getPasApptSeq().toString());
		if (medCase != null)
		{
			oneStopOrderItemSummary.getMedCase().setPasAttendFlag(medCase.getPasAttendFlag());
			oneStopOrderItemSummary.getMedCase().setPasPayCode(medCase.getPasPayCode());
			oneStopOrderItemSummary.getMedCase().setPasPatGroupCode(medCase.getPasPatGroupCode());
		}
	}
	
	private void retrieveUnvetOutPatAttendAndHaStaffWithoutAppointmentSequence(OneStopOrderItemSummary oneStopOrderItemSummary) {
		List<MedCase> medCaseList = oneStopPasManager.retriveAppointmentAttendanceAndStatusWithoutAppointmentSequence(oneStopOrderItemSummary.getPatHospCode(), 
																													  oneStopOrderItemSummary.getMedCase().getCaseNum(), 
																													  oneStopOrderItemSummary.getMedCase().getPasSpecCode(),
																													  oneStopOrderItemSummary.getMedCase().getPasSubSpecCode(), 
																													  pasDateFormat.format(oneStopOrderItemSummary.getOrderDate()));
		MedCase medCaseIn = oneStopOrderItemSummary.getMedCase();
		
		for (MedCase medCase : medCaseList) {
			if ( medCase.getCaseNum().equals(medCaseIn.getCaseNum()) &&
				 medCase.getPasSpecCode().equals(medCaseIn.getPasSpecCode()) && 
				 (medCase.getPasSubSpecCode() != null && medCaseIn.getPasSubSpecCode().equals(medCaseIn.getPasSubSpecCode())) &&
			     new DateMidnight(medCase.getPasApptDate()).equals(new DateMidnight(oneStopOrderItemSummary.getOrderDate()))
			){
				oneStopOrderItemSummary.getMedCase().setPasAttendFlag(medCase.getPasAttendFlag());
				oneStopOrderItemSummary.getMedCase().setPasPayCode(medCase.getPasPayCode());
				oneStopOrderItemSummary.getMedCase().setPasPatGroupCode(medCase.getPasPatGroupCode());
				break;
			}
		}
	}
	
	private void phsSpecAndWardMappingForCaseNumWithoutAssocaitaion(MedCase medCase, String patHospCode) {
		Map<String, String> specWardMapping = oneStopUtils.phsSpecialtyWardMapping(workstore.getHospCode(), workstore.getWorkstoreCode(),
																				   patHospCode,
																				   oneStopUtils.isInPatCase(medCase.getCaseNum())?"I":"O",
																				   medCase.getPasSpecCode(), "", medCase.getPasWardCode(), "N", false);
		
		oneStopOrderInfo.setPhsSpecCode(specWardMapping.get(PHS_SPECIALTY));
		oneStopOrderInfo.setPhsWardCode(specWardMapping.get(PHS_WARD));
	}
	
	private void phsSpecAndWardMappingForOrder(OneStopOrderItemSummary oneStopOrderItemSummary) 
	{
		if (oneStopOrderItemSummary.getOneStopOrderStatus() != OneStopOrderStatus.Cancelled && oneStopOrderItemSummary.getOneStopOrderStatus() != OneStopOrderStatus.Vetted)
		{
			Map<String, String> specWardMapping = oneStopUtils.phsSpecialtyWardMapping(workstore.getHospCode(), workstore.getWorkstoreCode(),
																							  oneStopOrderItemSummary.getPatHospCode(),
																							  oneStopUtils.isInPatCase(oneStopOrderItemSummary.getMedCase().getCaseNum())?"I":"O",
																							  oneStopOrderItemSummary.getMedCase().getPasSpecCode(),
																							  oneStopOrderItemSummary.getMedCase().getPasSubSpecCode(),
																							  oneStopOrderItemSummary.getMedCase().getPasWardCode(),
																							  oneStopOrderItemSummary.getPatType() == PharmOrderPatType.Private?"Y":"N", true);	
			
			oneStopOrderItemSummary.setPhsSpecCode(specWardMapping.get(PHS_SPECIALTY));
			oneStopOrderItemSummary.setOrignalPhsSpecCode(specWardMapping.get(PHS_SPECIALTY));
			oneStopOrderItemSummary.setPhsWardCode(specWardMapping.get(PHS_WARD));
		}
	}
	
	private void phsSpecAndWardMappingForAppointment(MedCase medCase, String patHospCode) 
	{
		Map<String, String> specWardMapping = oneStopUtils.phsSpecialtyWardMapping(workstore.getHospCode(), workstore.getWorkstoreCode(),
				  																		  patHospCode, "O", medCase.getPasSpecCode(), 
				  																		  medCase.getPasSubSpecCode(), medCase.getPasWardCode(), 
				  																		  "N", true);
		
		medCase.setPhsSpecCode(specWardMapping.get(PHS_SPECIALTY));
		medCase.setPhsWardCode(specWardMapping.get(PHS_WARD));
	}
	
	@SuppressWarnings(UNCHECKED)
	public void retrievePrescriptionListFromLastThreeDays(OneStopOrderType oneStopOrderType, Long medOrderId, Long medOrderVersion, 
														  Long dispOrderId, Long refillId, String hkid) 
	{
		if (medOrderId != null)
		{
			vettingMsg = null;
			switch (oneStopOrderType)
			{
				case MedOrder:
					vettingMsg = oneStopOrderStatusManager.verifyMedOrderStatusBeforeSwitchToVetting(medOrderId);
					break;
					
				case DispOrder:
					vettingMsg = oneStopOrderStatusManager.verifyDispOrderStatusBeforeSwitchToVetting(dispOrderId);
					break;
					
				case RefillOrder:
				case SfiOrder:
					vettingMsg = oneStopOrderStatusManager.verifyRefillOrderStatusBeforeSwitchToVetting(refillId);
					break;
			}
							
			if (vettingMsg != null)
			{
				prescriptionOrderList = null;
				return;
			}
		}
			
		prescriptionOrderList = new ArrayList<OneStopOrderItemSummary>();
		
		StringBuilder sb = new StringBuilder(
				"select o from DispOrder o" + // 20120304 index check : PharmOrder.hkid : I_PHARM_ORDER_01			
				" where o.pharmOrder.hkid = :hkid" + 
				" and o.pharmOrder.workstore.hospCode = :hospCode" +
				" and o.ticket.ticketDate >= :startDate" +
				" and o.ticket.ticketDate < :endDate" +
				" and o.pharmOrder.medOrder.prescType not in :prescType" +
				" and o.pharmOrder.status not in :pharmOrderStatus" +
				" and o.pharmOrder.medOrder.orderType = :orderType" +
				" and o.pharmOrder.medOrder.status not in :medOrderStatus" +
				" and o.status not in :dispOrderStatus");

		if (dispOrderId != null)
		{
			sb.append(" and o.id not in :id");
		}

		Query q = em.createQuery(sb.toString());
		q.setParameter(HKID, hkid);
		q.setParameter(HOSP_CODE, workstore.getHospCode());
		q.setParameter(START_DATE, new DateTime().minusDays(3).toDate(), TemporalType.DATE);
		q.setParameter(END_DATE, new DateTime().plusDays(1).toDate(), TemporalType.DATE);
		q.setParameter(PRESC_TYPE, Arrays.asList(MedOrderPrescType.In));
		q.setParameter(PHARM_ORDER_STATUS, PharmOrderStatus.Deleted_SysDeleted);
		q.setParameter(ORDER_TYPE, OrderType.OutPatient);
		q.setParameter(MED_ORDER_STATUS, MedOrderStatus.Deleted_SysDeleted);		
		q.setParameter(DISP_ORDER_STATUS, DispOrderStatus.Deleted_SysDeleted);

		if (dispOrderId != null) 
		{
			q.setParameter(ID, Arrays.asList(dispOrderId));
		}

		List<DispOrder> resultList = q.getResultList();

		for (DispOrder dispOrder : resultList) 
		{
			prescriptionOrderList.add(oneStopUtils.convertSummaryFromVettedOrder(dispOrder));
		}
		
		Collections.sort(prescriptionOrderList, new ComparatorForPrescriptionList());
	}
	
	public void refreshPharmacyOrderStatus(Long id) 
	{
		latestOneStopOrderItemSummary = oneStopUtils.convertSummaryFromUnvetOrder(em.find(MedOrder.class, id));
		for (Iterator<OneStopOrderItemSummary> itr = oneStopSummary.getOneStopOrderItemSummaryList().iterator(); itr.hasNext();) 
		{
			OneStopOrderItemSummary orignalOrderSummaryItem = (OneStopOrderItemSummary) itr.next();
			if (orignalOrderSummaryItem.getId().equals(id)) {
				int itemIndex = oneStopSummary.getOneStopOrderItemSummaryList().indexOf(orignalOrderSummaryItem);
				latestOneStopOrderItemSummary.getMedCase().setPasPayCode(orignalOrderSummaryItem.getMedCase().getPasPayCode());
				latestOneStopOrderItemSummary.getMedCase().setPasAttendFlag(orignalOrderSummaryItem.getMedCase().getPasAttendFlag());
				latestOneStopOrderItemSummary.getMedCase().setPasPatGroupCode(orignalOrderSummaryItem.getMedCase().getPasPatGroupCode());
				phsSpecAndWardMappingForOrder(latestOneStopOrderItemSummary);
				if (propMap.getValueAsBoolean(ONESTOP_PATIENT_PATCATCODE_REQUIRED.getName()))
				{
					oneStopOrderInfo.setPhsPatCat(propMap.getValue(ONESTOP_PATIENT_PATCATCODE.getName()));
				}
				oneStopOrderInfo.setDoctorCode(orignalOrderSummaryItem.getDoctorCode());
				oneStopOrderInfo.setDoctorName(orignalOrderSummaryItem.getDoctorName());
				oneStopOrderInfo.setPhsSpecCode(latestOneStopOrderItemSummary.getPhsSpecCode());
				oneStopOrderInfo.setPhsWardCode(latestOneStopOrderItemSummary.getPhsWardCode());
				itr.remove();
				oneStopSummary.getOneStopOrderItemSummaryList().add(itemIndex, latestOneStopOrderItemSummary);
				break;
			}
		}
	}
	
	@SuppressWarnings(UNCHECKED)
	public void refreshOrderStatus(OneStopOrderType oneStopOrderType, Long id, Long refillOrderId) 
	{
		switch (oneStopOrderType) 
		{
			case MedOrder:
				latestOneStopOrderItemSummary = oneStopUtils.convertSummaryFromUnvetOrder(em.find(MedOrder.class, id));
				
				for (Iterator<OneStopOrderItemSummary> itr = oneStopSummary.getOneStopOrderItemSummaryList().iterator(); itr.hasNext();)
				{
					OneStopOrderItemSummary orignalOrderSummaryItem = (OneStopOrderItemSummary) itr.next();
					if (orignalOrderSummaryItem.getId().equals(id)) 
					{
						int itemIndex = oneStopSummary.getOneStopOrderItemSummaryList().indexOf(orignalOrderSummaryItem);
						latestOneStopOrderItemSummary.getMedCase().setPasPayCode(orignalOrderSummaryItem.getMedCase().getPasPayCode());
						latestOneStopOrderItemSummary.getMedCase().setPasAttendFlag(orignalOrderSummaryItem.getMedCase().getPasAttendFlag());
						latestOneStopOrderItemSummary.getMedCase().setPasPatGroupCode(orignalOrderSummaryItem.getMedCase().getPasPatGroupCode());
						latestOneStopOrderItemSummary.setPhsSpecCode(orignalOrderSummaryItem.getPhsSpecCode());
						latestOneStopOrderItemSummary.setOrignalPhsSpecCode(orignalOrderSummaryItem.getPhsSpecCode());
						latestOneStopOrderItemSummary.setPhsWardCode(orignalOrderSummaryItem.getPhsWardCode());
						itr.remove();
						oneStopSummary.getOneStopOrderItemSummaryList().add(itemIndex, latestOneStopOrderItemSummary);
						break;
					}
				}
				break;
			case DispOrder:
				DispOrder dispOrder = em.find(DispOrder.class, id);
				latestOneStopOrderItemSummary = oneStopUtils.convertSummaryFromVettedOrder(dispOrder);
				for (Iterator<OneStopOrderItemSummary> itr = oneStopSummary.getOneStopOrderItemSummaryList().iterator(); itr.hasNext();)
				{
					OneStopOrderItemSummary orignalOrderSummaryItem = (OneStopOrderItemSummary) itr.next();
					if (orignalOrderSummaryItem.getDispOrderId() != null && orignalOrderSummaryItem.getDispOrderId().equals(id)) 
					{
						int itemIndex = oneStopSummary.getOneStopOrderItemSummaryList().indexOf(orignalOrderSummaryItem);
						itr.remove();
						oneStopSummary.getOneStopOrderItemSummaryList().add(itemIndex, latestOneStopOrderItemSummary);
						break;
					}
				}
				
				if (!oneStopSummary.getOneStopRefillItemSummaryList().isEmpty())
				{
					List<OneStopRefillItemSummary> refillStdSfiItemSummaryList = new ArrayList<OneStopRefillItemSummary>(); 
					latestOneStopRefillItemSummaryList = new ArrayList<OneStopRefillItemSummary>(); 
					List<RefillSchedule> stdRefillScheduleList = (List<RefillSchedule>) em.createQuery(
							"select o from RefillSchedule o" + // 20150420 index check : PharmOrder.id : PK_PHARM_ORDER
							" where o.pharmOrder.id = :id" +
							" and o.docType = :refillType" +
							" order by o.groupNum, o.refillNum")
							.setParameter("id", dispOrder.getPharmOrder().getId())
							.setParameter(REFILL_TYPE, DocType.Standard)
							.getResultList();
					
					List<RefillSchedule> sfiRefillScheduleList = (List<RefillSchedule>) em.createQuery(
							"select o from RefillSchedule o" + // 20150420 index check : PharmOrder.id : PK_PHARM_ORDER
							" where o.pharmOrder.id = :id" +
							" and o.docType = :refillType" +
							" order by o.groupNum, o.refillNum")
							.setParameter("id", dispOrder.getPharmOrder().getId())
							.setParameter(REFILL_TYPE, DocType.Sfi)
							.getResultList();
										
					if (!stdRefillScheduleList.isEmpty())
					{
						refillStdSfiItemSummaryList.addAll(oneStopUtils.convertRefillSfiRefillOrder(stdRefillScheduleList, false));
					}
					
					if (!sfiRefillScheduleList.isEmpty())
					{
						refillStdSfiItemSummaryList.addAll(oneStopUtils.convertRefillSfiRefillOrder(sfiRefillScheduleList, true));
					}
					
					for (OneStopRefillItemSummary oneStopRefillItemSummary : refillStdSfiItemSummaryList)
					{
						for (Iterator<OneStopRefillItemSummary> itr = oneStopSummary.getOneStopRefillItemSummaryList().iterator(); itr.hasNext();)
						{
							OneStopRefillItemSummary orignalRefillSummaryItem = (OneStopRefillItemSummary) itr.next();
							if (orignalRefillSummaryItem.getRefillCouponNum().equals(oneStopRefillItemSummary.getRefillCouponNum()))
							{
								int itemIndex = oneStopSummary.getOneStopRefillItemSummaryList().indexOf(orignalRefillSummaryItem);
								itr.remove();
								oneStopSummary.getOneStopRefillItemSummaryList().add(itemIndex, oneStopRefillItemSummary);
								latestOneStopRefillItemSummaryList.add(oneStopRefillItemSummary);
								break;
							}
						}
					}
				}				
				break;				
			case RefillOrder:
			case SfiOrder:
				latestOneStopRefillItemSummaryList = new ArrayList<OneStopRefillItemSummary>(); 
				List<RefillSchedule> refillScheduleList;
				List<RefillSchedule> stdRefillScheduleList = new ArrayList<RefillSchedule>();
				List<RefillSchedule> sfiRefillScheduleList = new ArrayList<RefillSchedule>();
				List<OneStopRefillItemSummary> refillStdSfiItemSummaryList = new ArrayList<OneStopRefillItemSummary>();
				RefillSchedule refillSchedule = em.find(RefillSchedule.class, refillOrderId);
				
				refillScheduleList = (List<RefillSchedule>) em.createQuery(
						"select o from RefillSchedule o" + // 20120303 index check : RefillSchedule.dispOrder : FK_REFILL_SCHEDULE_02
						" where o.pharmOrder.id = :id" +
						" order by o.groupNum, o.refillNum")
						.setParameter("id", refillSchedule.getPharmOrder().getId())
						.getResultList();
				
				for (RefillSchedule currRefillSchedule : refillScheduleList)
				{
					if (currRefillSchedule.getDocType() == DocType.Standard)
					{
						stdRefillScheduleList.add(currRefillSchedule);
					}
					else 
					{
						sfiRefillScheduleList.add(currRefillSchedule);
					}
				}				
				
				if (!stdRefillScheduleList.isEmpty())
				{
					refillStdSfiItemSummaryList.addAll(oneStopUtils.convertRefillSfiRefillOrder(stdRefillScheduleList, false));
				}
				
				if (!sfiRefillScheduleList.isEmpty())
				{
					refillStdSfiItemSummaryList.addAll(oneStopUtils.convertRefillSfiRefillOrder(sfiRefillScheduleList, true));
				}

				for (OneStopRefillItemSummary oneStopRefillItemSummary : refillStdSfiItemSummaryList)
				{
					for (Iterator<OneStopRefillItemSummary> itr = oneStopSummary.getOneStopRefillItemSummaryList().iterator(); itr.hasNext();)
					{
						OneStopRefillItemSummary orignalRefillSummaryItem = (OneStopRefillItemSummary) itr.next();
						if (orignalRefillSummaryItem.getRefillCouponNum().equals(oneStopRefillItemSummary.getRefillCouponNum()))
						{
							int itemIndex = oneStopSummary.getOneStopRefillItemSummaryList().indexOf(orignalRefillSummaryItem);
							itr.remove();
							oneStopSummary.getOneStopRefillItemSummaryList().add(itemIndex, oneStopRefillItemSummary);
							latestOneStopRefillItemSummaryList.add(oneStopRefillItemSummary);
							
							if (refillOrderId.equals(oneStopRefillItemSummary.getRefillScheduleId()))
							{
								latestOneStopRefillItemSummary = oneStopRefillItemSummary;
							}
							break;
						}
					}
				}
				
				if (!oneStopSummary.getOneStopOrderItemSummaryList().isEmpty())
				{
					List<DispOrder> dispOrderList = em.createQuery(
							"select o from DispOrder o" + // 20151012 index check : PharmOrder.id : PK_PHARM_ORDER
							" where o.pharmOrder.id = :id") 
							.setParameter("id", refillSchedule.getPharmOrder().getId())
							.getResultList();
					
					for (DispOrder currDispOrder : dispOrderList)
					{
						if (latestOneStopOrderItemSummary != null)
						{
							break;
						}
						
						for (Iterator<OneStopOrderItemSummary> itr = oneStopSummary.getOneStopOrderItemSummaryList().iterator(); itr.hasNext();)
						{
							OneStopOrderItemSummary orignalOrderSummaryItem = (OneStopOrderItemSummary) itr.next();
							if (orignalOrderSummaryItem.getDispOrderId().equals(currDispOrder.getId()))
							{
								latestOneStopOrderItemSummary = oneStopUtils.convertSummaryFromVettedOrder(currDispOrder);
								int itemIndex = oneStopSummary.getOneStopOrderItemSummaryList().indexOf(orignalOrderSummaryItem);
								itr.remove();
								oneStopSummary.getOneStopOrderItemSummaryList().add(itemIndex, latestOneStopOrderItemSummary);
								break;
							}
						}	
					}
				}
				break;
			default:
				break;
		}	
	}
	
	public static class ComparatorForPrescriptionList implements Comparator<OneStopOrderItemSummary>, Serializable {

		private static final long serialVersionUID = 1L;

		public int compare(OneStopOrderItemSummary s1, OneStopOrderItemSummary s2) {
			
			int result = descCompareObj(s1.getTicketDate(), s2.getTicketDate());

			if (result != 0) {
				return result;
			}
			
			result = descCompareObj(s1.getTicketNum(), s2.getTicketNum());
			
			if (result != 0) {
				return result;
			}
			
			return compareObj(s1.getDispWorkStore(), s2.getDispWorkStore());
		}
	}
	
	public static class ComparatorForOrderList implements Comparator<OneStopOrderItemSummary>, Serializable {
		
		private static final long serialVersionUID = 1L;

		private Workstore workstore;
		
		public ComparatorForOrderList(Workstore wks) {
			this.workstore = wks;
		}
		
		public int compare(OneStopOrderItemSummary s1, OneStopOrderItemSummary s2) {
			
			if (s1.getPrescType() == MedOrderPrescType.Dh && s2.getPrescType() != MedOrderPrescType.Dh)
			{
				return 1;
			}
			
			if (s1.getPrescType() != MedOrderPrescType.Dh && s2.getPrescType() == MedOrderPrescType.Dh)
			{
				return -1;
			}
			
			if (s1.getPrescType() == MedOrderPrescType.Dh && s2.getPrescType() == MedOrderPrescType.Dh)
			{
				int result = descCompareObj(s1.getOrderNum(), s2.getOrderNum());
				
				if (result != 0) {
					return result;
				}

				return descCompareObj(s1.getRefNum(), s2.getRefNum());
				
			} else {
			
				int result = Boolean.valueOf(compareObj(s1.getTicketWks(), workstore.getWorkstoreCode()) != 0)
						.compareTo(compareObj(s2.getTicketWks(), workstore.getWorkstoreCode()) != 0);
				
				if (result != 0) {
					return result;
				}
			
				result = compareObj(s1.getTicketWks(), s2.getTicketWks());
				
				if (result != 0) {
					return result;
				}
				
				result = descCompareObj(s1.getTicketDate(), s2.getTicketDate());
	
				if (result != 0) {
					return result;
				}
				
				return descCompareObj(s1.getOrderNum(), s2.getOrderNum());
			}
		}
	}
		
	public static class ComparatorForRefillList implements Comparator<OneStopRefillItemSummary>, Serializable {
		
		private static final long serialVersionUID = 1L;

		public int compare(OneStopRefillItemSummary s1, OneStopRefillItemSummary s2) {						
			
			if (s1.getOneStopOrderStatus() == OneStopOrderStatus.Dispensed && s2.getOneStopOrderStatus() != OneStopOrderStatus.Dispensed) 
			{
				return 1;
			} 
			else if (s1.getOneStopOrderStatus() != OneStopOrderStatus.Dispensed && s2.getOneStopOrderStatus() == OneStopOrderStatus.Dispensed) 
			{
				return -1;
			}
				
			int result = compareObj(s1.getTicketDate(), s2.getTicketDate());
			
			if (result != 0) 
			{
				return result;
			}
			
			result = compareObj(s1.getOrderDate(), s2.getOrderDate());
			
			if (result != 0) 
			{
				return result;
			}
			
			return compareObj(s1.getOrderNum(), s2.getOrderNum());
		}
	}
		
	public static <E extends Comparable<E>> int compareObj(E s1, E s2) {
		
		if (s1 == s2) {
			return 0;
		} else if (s1 == null) {
			return 1;
		} else if (s2 == null) { 
			return -1;
		} else {
			return s1.compareTo(s2);
		}
	}
	
	public static <E extends Comparable<E>> int descCompareObj(E s1, E s2) {

		if (s1 == s2) {
			return 0;
		} else if (s1 == null) {
			return 1;
		} else if (s2 == null) { 
			return -1;
		} else {
			return s2.compareTo(s1);
		}
		
	}
	
	public static class ComparatorForAppointmentList implements Comparator<MedCase>, Serializable {

		private static final long serialVersionUID = 1L;

		public int compare(MedCase medCase1, MedCase medCase2) {
						
			int result = descCompareObj(medCase1.getPasApptDate(), medCase2.getPasApptDate());
			if (result != 0) 
			{
				return result;
			}
			
			result = compareObj(medCase1.getPasSpecCode(), medCase2.getPasSpecCode());
			
			if (result != 0) 
			{
				return result;
			}
			
			return compareObj(medCase1.getPasSubSpecCode(), medCase2.getPasSubSpecCode());
		}
	}
	
	public static class ComparatorForDefaultAppointment implements Comparator<MedCase>, Serializable {

		private static final long serialVersionUID = 1L;

		public int compare(MedCase medCase1, MedCase medCase2) {
			// TODO Auto-generated method stub
			if (medCase1.getCreateDate() != null) {
				if (medCase1.getCreateDate().before(medCase2.getCreateDate())) {
					return 1;
				} else if (medCase1.getCreateDate().equals((medCase2.getCreateDate()))) {
					return 0;
				} else {
					return -1;
				}
			} else {
				if (medCase1.getPasApptDate().before(medCase2.getPasApptDate())) {
					return 1;
				} else if (medCase1.getPasApptDate().equals(medCase2.getPasApptDate())) {
					return 0;
				} else {
					return -1;
				}
			}
		}
	}
	
	@SuppressWarnings(UNCHECKED)
	public void retrieveSuspendOrder(Date suspendStartDate) {
		if (oneStopOrderInfo == null) {
			oneStopOrderInfo = new OneStopOrderInfo();	// empty object, for front-end use
		}

		suspendOrderList = new ArrayList<OneStopSummary>();

		// load suspendReason mapping data to 1st level cache
		List<SuspendReason> suspendReasonList = em.createQuery(
				"select o from SuspendReason o" + // 20120225 index check : SuspendReason.hospital : FK_SUSPEND_REASON_01
		" where o.hospital = :hospital")
		.setParameter("hospital", workstore.getHospital())
		.getResultList();

		Map<String, SuspendReason> suspendReasonMap = new HashMap<String, SuspendReason>();
		for (SuspendReason suspendReason : suspendReasonList)
		{
			if (suspendReasonMap.get(suspendReason.getSuspendCode()) == null)
			{
				suspendReasonMap.put(suspendReason.getSuspendCode(), suspendReason);
			}
		}
		
		Date suspendEndDate = (new DateTime(suspendStartDate)).plusDays(Prop.ONESTOP_SUSPENDORDERCOUNT_DURATION_LIMIT.get().intValue()).toDateMidnight().toDateTime().toDate();
		List<DispOrder> dispOrderList = (List<DispOrder>) em.createQuery(
				"select o from DispOrder o" + // 20140930 index check : DispOrder.workstore,adminStatus,status,dispDate : I_DISP_ORDER_14
				" where o.workstore = :workstore" + 
				" and o.adminStatus = :adminStatus" + 				
				" and o.status not in :status" + 
				" and o.dispDate >= :startDispDate" +
				" and o.dispDate < :endDispDate" +
				" and o.ticket.orderType = :orderType")
		.setParameter(WORKSTORE, workstore)
		.setParameter(ADMIN_STATUS, DispOrderAdminStatus.Suspended)			
		.setParameter(STATUS, DispOrderStatus.Deleted_SysDeleted)
		.setParameter("startDispDate", suspendStartDate, TemporalType.DATE)
		.setParameter("endDispDate", suspendEndDate, TemporalType.DATE)
		.setParameter(ORDER_TYPE, OrderType.OutPatient)
		.setHint(QueryHints.FETCH, "o.ticket")
		.getResultList();

		for (Iterator<DispOrder> itr = dispOrderList.iterator(); itr.hasNext();)
		{
			DispOrder dispOrder = (DispOrder) itr.next();
			Date tickDate = dispOrder.getTicket().getTicketDate();
			if (tickDate.before(suspendStartDate) && tickDate.after(suspendEndDate))
			{
				itr.remove();
			}
		}
		
		for (DispOrder dispOrder : dispOrderList)
		{
			OneStopSummary oneStopSummaryVet = new OneStopSummary();
			List<OneStopOrderItemSummary> oneStopOrderItemSummaryList = new ArrayList<OneStopOrderItemSummary>();
			OneStopOrderItemSummary oneStopOrderItemSummary = oneStopUtils.convertSummaryFromVettedOrder(dispOrder);
			if (Boolean.TRUE.equals(dispOrder.getRevokeFlag())) {
				oneStopOrderItemSummary.setOneStopOrderStatusInd(OneStopOrderStatusIndicator.Uncollect);
			}

			// set suspend code and desc
			if (dispOrder.getSuspendCode() != null) {
				if (CHARGING_SFI_SUSPEND_ORDER_SUSPENDCODE.get().equals(dispOrder.getSuspendCode())) {
					oneStopOrderItemSummary.setSuspendDesc("Await SFI payment");
				} else {
					SuspendReason suspendReason = suspendReasonMap.get(dispOrder.getSuspendCode());
					if (suspendReason != null && suspendReason.getStatus() != RecordStatus.Delete)
					{
						oneStopOrderItemSummary.setSuspendDesc(suspendReason.getDescription());
					}
				}
			}
			
			oneStopOrderItemSummaryList.add(oneStopOrderItemSummary);
			oneStopSummaryVet.setOneStopOrderItemSummaryList(oneStopOrderItemSummaryList);
			oneStopSummaryVet.setPatient(dispOrder.getPharmOrder().getMedOrder().getPatient());
			suspendOrderList.add(oneStopSummaryVet);
		}
		
		Collections.sort(suspendOrderList, new OneStopSummaryByDispDateTicketNum());
		retrieveUnvetAndSuspendCount();
	}
	
	public void retrieveMoeOrder(Date orderDate, MoeFilterOption moeFilterOption, List<PrescriptionSortCriteria> prescriptionSortCriteriaList) {
		
		Map<String, Map<Long, OneStopSummary>> retrieveOrdersPrescMap = oneStopPrescManager.retrieveOrdersPrescMap(orderDate, workstore, true);
		originalUnvetOneStopSummaryMap = retrieveOrdersPrescMap.get("unvet");
		originalCancelledOneStopSummaryMap = retrieveOrdersPrescMap.get("cancel");
		originalRefillOneStopSummaryMap = retrieveOrdersPrescMap.get("refill");

		filterMoeList(moeFilterOption, prescriptionSortCriteriaList);
		
		retrieveUnvetAndSuspendCount();
	}
	
	private Map<String, Patient> retrievePatientNameForUnvetCancelledMoeOrder(List<String> patHospCodeCaseNumList){
		Map<String, Patient> patientMap = new HashMap<String, Patient>();
		
		if( patHospCodeCaseNumList.isEmpty() ){
			return patientMap;
		}
		
		Map<String, String> patientNameMap = oneStopPasManager.retrieveHkpmiPatientNameListByCaseNumList(patHospCodeCaseNumList);

		if( patientNameMap == null ){
			return patientMap;
		}

		for( String caseNumKey : patientNameMap.keySet() ){
			Patient patient = new Patient();
			patient.setName(patientNameMap.get(caseNumKey));
			patientMap.put(caseNumKey, patient);
		}
		return patientMap;
	}
	
	public void filterMoeList(MoeFilterOption moeFilterOption, List<PrescriptionSortCriteria> prescriptionSortCriteriaList)
	{
		moeUnvetOrderList = new ArrayList<OneStopSummary>();
		moeCancelOrderList = new ArrayList<OneStopSummary>();
		moeRefillOrderList = new ArrayList<OneStopSummary>();
		moeUnvetOrderList = oneStopPrescManager.filterUnvetRefillCancelledList(originalUnvetOneStopSummaryMap, moeFilterOption);
		moeCancelOrderList = oneStopPrescManager.filterUnvetRefillCancelledList(originalCancelledOneStopSummaryMap, moeFilterOption);
		moeRefillOrderList = oneStopPrescManager.filterUnvetRefillCancelledList(originalRefillOneStopSummaryMap, moeFilterOption);
		
		for (PrescriptionSortCriteria prescriptionSortCriteria : prescriptionSortCriteriaList)
		{
			sortMoeList(prescriptionSortCriteria);
		}
		
		List<String> moeUnvetCancelledOrderCaseNumList = new ArrayList<String>();
		Map<Long, String> patHospCodeCaseNumMap = new HashMap<Long, String>();
		
		int unvetOrderEndIndex = (moeUnvetOrderList.size() > UNVET_ORDER_DISPLAY_LIMIT ? UNVET_ORDER_DISPLAY_LIMIT : moeUnvetOrderList.size());
		for (int i = 0; i < unvetOrderEndIndex; i++)
		{
			Long id = moeUnvetOrderList.get(i).getOneStopOrderItemSummaryList().get(0).getId();
			if (originalUnvetOneStopSummaryMap.get(id).getPatient() == null)
			{
				OneStopSummary oneStopSummary = originalUnvetOneStopSummaryMap.get(id);
				OneStopOrderItemSummary oneStopOrderItemSummary = oneStopSummary.getOneStopOrderItemSummaryList().get(0);				
				String patHospCodeCaseNumKey = oneStopOrderItemSummary.getPatHospCode() + PAS_PAT_HOSP_CODE_CASE_NUM_DELIMITER + oneStopOrderItemSummary.getMedCase().getCaseNum();
				if( !moeUnvetCancelledOrderCaseNumList.contains(patHospCodeCaseNumKey) ){
					moeUnvetCancelledOrderCaseNumList.add(patHospCodeCaseNumKey);
				}
				patHospCodeCaseNumMap.put(id, patHospCodeCaseNumKey);
			}
		}
		
		int cancelledOrderEndIndex = (moeCancelOrderList.size() > CANCELLED_ORDER_DISPLAY_LIMIT ? CANCELLED_ORDER_DISPLAY_LIMIT : moeCancelOrderList.size());		
		for (int i = 0; i < cancelledOrderEndIndex; i++)
		{
			Long id = moeCancelOrderList.get(i).getOneStopOrderItemSummaryList().get(0).getId();
			if (originalCancelledOneStopSummaryMap.get(id).getPatient() == null)
			{
				OneStopSummary oneStopSummary = originalCancelledOneStopSummaryMap.get(id);
				OneStopOrderItemSummary oneStopOrderItemSummary = oneStopSummary.getOneStopOrderItemSummaryList().get(0);				
				String patHospCodeCaseNumKey = oneStopOrderItemSummary.getPatHospCode() + PAS_PAT_HOSP_CODE_CASE_NUM_DELIMITER + oneStopOrderItemSummary.getMedCase().getCaseNum();
				if( !moeUnvetCancelledOrderCaseNumList.contains(patHospCodeCaseNumKey) ){
					moeUnvetCancelledOrderCaseNumList.add(patHospCodeCaseNumKey);
				}
				patHospCodeCaseNumMap.put(id, patHospCodeCaseNumKey);
			}
		}
		
		Map<String, Patient> moeUnvetCancelledOrderPatientMap = retrievePatientNameForUnvetCancelledMoeOrder(moeUnvetCancelledOrderCaseNumList);
		
		for (int i = 0; i < unvetOrderEndIndex; i++)
		{
			Long id = moeUnvetOrderList.get(i).getOneStopOrderItemSummaryList().get(0).getId();
			if (originalUnvetOneStopSummaryMap.get(id).getPatient() == null)
			{
				updatePasPatientInfoForUnvetCancelledMoeOrderList(i, moeUnvetOrderList, originalUnvetOneStopSummaryMap, moeUnvetCancelledOrderPatientMap, patHospCodeCaseNumMap);
			}
		}

		for (int i = 0; i < cancelledOrderEndIndex; i++)
		{
			Long id = moeCancelOrderList.get(i).getOneStopOrderItemSummaryList().get(0).getId();
			if (originalCancelledOneStopSummaryMap.get(id).getPatient() == null)
			{
				updatePasPatientInfoForUnvetCancelledMoeOrderList(i, moeCancelOrderList, originalCancelledOneStopSummaryMap, moeUnvetCancelledOrderPatientMap, patHospCodeCaseNumMap);
			}
		}
	}
	
	private void updatePasPatientInfoForUnvetCancelledMoeOrderList(int index, List<OneStopSummary> sourceList, Map<Long, OneStopSummary> sourceMap, Map<String, Patient> sourcePatientMap, Map<Long, String> sourceCaseNumKeyMap)
	{
		Long id = sourceList.get(index).getOneStopOrderItemSummaryList().get(0).getId();
		OneStopSummary oneStopSummary = sourceMap.get(id);
		String patHospCodeCaseNumKey = sourceCaseNumKeyMap.get(id);
		if(sourcePatientMap.get(patHospCodeCaseNumKey) == null){
			oneStopSummary.setPatient(new Patient());
		}else{
			oneStopSummary.setPatient(sourcePatientMap.get(patHospCodeCaseNumKey));
		}
	}
	
	public void retrievePagedPatient(List<Long> oneStopOrderItemSummaryIdList, String caller)
	{
		List<String> moeUnvetCancelledOrderCaseNumList = new ArrayList<String>();
		Map<Long, String> patHospCodeCaseNumMap = new HashMap<Long, String>();
		
		for (Long id : oneStopOrderItemSummaryIdList)
		{
			OneStopSummary oneStopSummary;
			if (caller.equals("Unvet"))
			{
				oneStopSummary = originalUnvetOneStopSummaryMap.get(id);
			}
			else
			{
				oneStopSummary = originalCancelledOneStopSummaryMap.get(id);
			}
			
			OneStopOrderItemSummary oneStopOrderItemSummary = oneStopSummary.getOneStopOrderItemSummaryList().get(0);
			if (oneStopSummary.getPatient() == null)
			{
				String patHospCodeCaseNumKey = oneStopOrderItemSummary.getPatHospCode() + PAS_PAT_HOSP_CODE_CASE_NUM_DELIMITER + oneStopOrderItemSummary.getMedCase().getCaseNum();
				if( !moeUnvetCancelledOrderCaseNumList.contains(patHospCodeCaseNumKey) ){
					moeUnvetCancelledOrderCaseNumList.add(patHospCodeCaseNumKey);
				}
				patHospCodeCaseNumMap.put(id, patHospCodeCaseNumKey);
			}
		}
		
		Map<String, Patient> orderPatientMap = retrievePatientNameForUnvetCancelledMoeOrder(moeUnvetCancelledOrderCaseNumList);
		
		for (Long id : oneStopOrderItemSummaryIdList)
		{
			OneStopSummary oneStopSummary;
			
			if (caller.equals("Unvet"))
			{
				oneStopSummary = originalUnvetOneStopSummaryMap.get(id);
			}
			else
			{
				oneStopSummary = originalCancelledOneStopSummaryMap.get(id);
			}
			
			if (oneStopSummary.getPatient() == null)
			{
				if(  patHospCodeCaseNumMap.get(id) == null ||  orderPatientMap.get((patHospCodeCaseNumMap.get(id))) == null ){
					oneStopSummary.setPatient(new Patient());
				}else{
					oneStopSummary.setPatient(orderPatientMap.get((patHospCodeCaseNumMap.get(id))));
				}
			}
		}
	}
	
	public void sortMoeList(PrescriptionSortCriteria prescriptionSortCriteria)
	{
		List<OneStopSummary> sortList;
		Map<Long, OneStopSummary> refMap;
		int maxDisplayRow;
		if (prescriptionSortCriteria.getGrid().equals("Unvet"))
		{
			sortList = moeUnvetOrderList;
			maxDisplayRow = (sortList.size() > UNVET_ORDER_DISPLAY_LIMIT ? UNVET_ORDER_DISPLAY_LIMIT : sortList.size());
			refMap = originalUnvetOneStopSummaryMap;
		}
		else if (prescriptionSortCriteria.getGrid().equals("Refill"))
		{
			sortList = moeRefillOrderList;
			refMap = originalRefillOneStopSummaryMap;
			maxDisplayRow = 0;
		}
		else
		{
			sortList = moeCancelOrderList;
			maxDisplayRow = (sortList.size() > CANCELLED_ORDER_DISPLAY_LIMIT ? CANCELLED_ORDER_DISPLAY_LIMIT : sortList.size());
			refMap = originalCancelledOneStopSummaryMap;
		}
		
		Collections.sort(sortList, new ComparatorForPrescription(prescriptionSortCriteria.getMoeSortOption(), 
				prescriptionSortCriteria.isDescending()));
		
		
		List<String> sortCaseNumList = new ArrayList<String>();
		Map<Long, String> patHospCodeCaseNumMap = new HashMap<Long, String>();
		
		for (int i = 0; i < maxDisplayRow; i++)
		{
			Long id = sortList.get(i).getOneStopOrderItemSummaryList().get(0).getId();
			if (refMap.get(id).getPatient() == null)
			{
				OneStopSummary oneStopSummary = refMap.get(id);
				OneStopOrderItemSummary oneStopOrderItemSummary = oneStopSummary.getOneStopOrderItemSummaryList().get(0);				
				String patHospCodeCaseNumKey = oneStopOrderItemSummary.getPatHospCode() + PAS_PAT_HOSP_CODE_CASE_NUM_DELIMITER + oneStopOrderItemSummary.getMedCase().getCaseNum();
				patHospCodeCaseNumMap.put(id, patHospCodeCaseNumKey);
				if( !sortCaseNumList.contains(patHospCodeCaseNumKey) ){
					sortCaseNumList.add(patHospCodeCaseNumKey);
				}
			}
		}
		
		Map<String, Patient> orderPatientMap = retrievePatientNameForUnvetCancelledMoeOrder(sortCaseNumList);
		
		for (int i = 0; i < maxDisplayRow; i++)
		{
			Long id = sortList.get(i).getOneStopOrderItemSummaryList().get(0).getId();
			if (refMap.get(id).getPatient() == null)
			{
				updatePasPatientInfoForUnvetCancelledMoeOrderList(i, sortList, refMap, orderPatientMap, patHospCodeCaseNumMap);
			}
		}
	}
	
	public void writeHkidUnknownDigitLog(String hkid)
	{		
		auditLogger.log("#0717", uamInfo.getUserId(), new DateTime(), workstation.getHostName(), hkid);
	}
	
	public static class ComparatorForPrescription implements Comparator<OneStopSummary>, Serializable 
	{
		private static final long serialVersionUID = 1L;
		private MoeSortOption moeSortOption;
		private boolean descending;
		
		public ComparatorForPrescription(MoeSortOption moeSortOption, boolean descending)
		{
			this.moeSortOption = moeSortOption;
			this.descending = descending;
		}
		
		public int compare(OneStopSummary o1, OneStopSummary o2) 
		{			
			switch (moeSortOption)
			{
				case RefNum:
					if (!descending)
					{
						return compareObj(o1.getOneStopOrderItemSummaryList().get(0).getRefNum(), 
								o2.getOneStopOrderItemSummaryList().get(0).getRefNum());
					}
					else
					{
						return descCompareObj(o1.getOneStopOrderItemSummaryList().get(0).getRefNum(), 
								o2.getOneStopOrderItemSummaryList().get(0).getRefNum());
					}
										
				case OrderDate:
					if (!descending)
					{
						return compareObj(o1.getOneStopOrderItemSummaryList().get(0).getOrderDate(), 
								o2.getOneStopOrderItemSummaryList().get(0).getOrderDate());
					}
					else
					{
						return descCompareObj(o1.getOneStopOrderItemSummaryList().get(0).getOrderDate(), 
								o2.getOneStopOrderItemSummaryList().get(0).getOrderDate());
					}
					
				case Spec:
					if (!descending)
					{
						return compareObj(o1.getOneStopOrderItemSummaryList().get(0).getMedCase().getPasSpecCode(), 
								o2.getOneStopOrderItemSummaryList().get(0).getMedCase().getPasSpecCode());
					}
					else
					{
						return descCompareObj(o1.getOneStopOrderItemSummaryList().get(0).getMedCase().getPasSpecCode(), 
								o2.getOneStopOrderItemSummaryList().get(0).getMedCase().getPasSpecCode());
					}
					
				case Ward:
					if (!descending)
					{
						return compareObj(o1.getOneStopOrderItemSummaryList().get(0).getMedCase().getPasWardCode(), 
								o2.getOneStopOrderItemSummaryList().get(0).getMedCase().getPasWardCode());
					}
					else
					{
						return descCompareObj(o1.getOneStopOrderItemSummaryList().get(0).getMedCase().getPasWardCode(), 
								o2.getOneStopOrderItemSummaryList().get(0).getMedCase().getPasWardCode());
					}
				
				case TicketNum:
					if (!descending)
					{
						return compareObj(o1.getOneStopOrderItemSummaryList().get(0).getTicketNum(), 
								o2.getOneStopOrderItemSummaryList().get(0).getTicketNum());
					}
					else
					{
						return descCompareObj(o1.getOneStopOrderItemSummaryList().get(0).getTicketNum(), 
								o2.getOneStopOrderItemSummaryList().get(0).getTicketNum());
					}
			
				default:
					return 0;	
			}
		}
	}
			
	public static class OneStopSummaryByDispDateTicketNum implements Comparator<OneStopSummary>, Serializable {

		private static final long serialVersionUID = 1L;

		public int compare( OneStopSummary oneStopSummary1, OneStopSummary oneStopSummary2 ) {
			OneStopOrderItemSummary o1 = oneStopSummary1.getOneStopOrderItemSummaryList().get(0);
			OneStopOrderItemSummary o2 = oneStopSummary2.getOneStopOrderItemSummaryList().get(0);
			
			int dispDateCompare = o1.getTicketDate().compareTo(o2.getTicketDate());
			if (dispDateCompare != 0) {
				return -1 * dispDateCompare;	// sort by disp date in DESC order
			}

			return o1.getTicketNum().compareTo(o2.getTicketNum());	// sort by ticknum is ASC order
		}
	}
	
	public List<OneStopSummary> getRefNumOrderList() 
	{
		return refNumOrderList;
	}
	
	public List<OneStopOrderItemSummary> getCaseNumVettedList() 
	{
		return caseNumVettedList;
	}
	
	public List<OneStopOrderItemSummary> getCaseNumUnvetList() 
	{
		return caseNumUnvetList;
	}

	public void setCaseNumUnvetList(List<OneStopOrderItemSummary> caseNumUnvetList) {
		this.caseNumUnvetList = caseNumUnvetList;
	}

	public void setCaseNumVettedList(List<OneStopOrderItemSummary> caseNumVettedList) {
		this.caseNumVettedList = caseNumVettedList;
	}
	
	public List<OneStopOrderItemSummary> getPrescriptionOrderList() {
		return prescriptionOrderList;
	}
	
	public String getVettingMsg() {
		return vettingMsg;
	}
	
	public String updatePreVetValue(Long medOrderId, OneStopSummary preVetOneStopSummary, OneStopOrderInfo preVetOneStopOrderInfo)
	{
		oneStopSummary = preVetOneStopSummary;
		oneStopOrderInfo = preVetOneStopOrderInfo;
		Contexts.getSessionContext().set("oneStopSummary", preVetOneStopSummary);
		Contexts.getSessionContext().set("oneStopOrderInfo", preVetOneStopOrderInfo);
		String orderStatusMsg = oneStopOrderStatusManager.verifyMedOrderStatusBeforeSwitchToVetting(medOrderId);
		if (!StringUtils.isBlank(orderStatusMsg))
		{
			clearOrder();
		}
		return orderStatusMsg;
	}
	
	@Remove
	public void destroy() 
	{
		if (oneStopSummary != null) {
			oneStopSummary = null;
		}
		
		if (appointmentList != null) {
			appointmentList = null;
		}
		
		if (oneStopOrderInfo != null) {
			oneStopOrderInfo = null;
		}
						
		if (defaultAppointment != null) {
			defaultAppointment = null;
		}
		
		if (moStatusCount != null) {
			moStatusCount = null;
		}
		
		if (defaultPatient != null) {
			defaultPatient = null;
		}
		
		if (suspendOrderList != null) {
			suspendOrderList = null;
		}
		
		if (errMsg != null) {
			errMsg = null;
		}
		
		if (latestOneStopOrderItemSummary != null)
		{
			latestOneStopOrderItemSummary = null;
		}
		
		if (latestOneStopRefillItemSummary != null)
		{
			latestOneStopRefillItemSummary = null;
		}
		
		if (latestOneStopRefillItemSummaryList != null)
		{
			latestOneStopRefillItemSummaryList = null;
		}
	}
}