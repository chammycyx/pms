package hk.org.ha.model.pms.biz.reftable.rdr;

import javax.ejb.Local;

@Local
public interface PendReasonReaderServiceLocal {

	void retrievePendingReasonList();
	
	void destroy();
	
}
