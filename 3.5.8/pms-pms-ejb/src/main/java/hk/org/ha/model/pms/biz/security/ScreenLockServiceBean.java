package hk.org.ha.model.pms.biz.security;

import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.sys.SysProfile;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.uam.udt.AuthorizeStatus;
import hk.org.ha.model.pms.uam.vo.AuthenticateCriteria;
import hk.org.ha.service.biz.pms.uam.interfaces.UamServiceJmsRemote;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import org.drools.util.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.web.ServletContexts;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("screenLockService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ScreenLockServiceBean implements ScreenLockServiceLocal {

	@In
	private UamInfo uamInfo;
	
	@In
	private SysProfile sysProfile;
	
	@In
	private UamServiceJmsRemote uamServiceProxy;
	
	private String errMsg; 
	
	private int sessionTimeout;
		
	public void validateIdleUser(String userName, String password) {
		setErrMsg(StringUtils.EMPTY);
		AuthorizeStatus authorizeStatus = authentication(userName, password);
		if (authorizeStatus.equals(AuthorizeStatus.Invalid)) {
			setErrMsg("0257");
			setSessionTimeout(syncLockScreenSessionTimeout());
		}
	}
	
	private AuthorizeStatus authentication(String userName, String password) {
		if (sysProfile.isDebugEnabled()) {
			return AuthorizeStatus.Succeed;
		}
		AuthenticateCriteria authenticateCriteria = new AuthenticateCriteria();
		authenticateCriteria.setProfileCode(uamInfo.getProfileCode());
		authenticateCriteria.setUserName(userName);
		authenticateCriteria.setPassword(password);

		return uamServiceProxy.authenticate(authenticateCriteria);
	}
	
	public int syncLockScreenSessionTimeout()
	{
		//get session timeout in second and minus 1 mins
		return (ServletContexts.instance().getRequest().getSession().getMaxInactiveInterval() -60);
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public String getErrMsg() {
		return errMsg;
	}
	
	public void setSessionTimeout(int sessionTimeout) {
		this.sessionTimeout = sessionTimeout;
	}
	
	public int getSessionTimeout() {
		return sessionTimeout;
	}

	@Remove
    public void destroy() {

	}
	
}
