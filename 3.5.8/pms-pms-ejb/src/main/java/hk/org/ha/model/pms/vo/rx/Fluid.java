package hk.org.ha.model.pms.vo.rx;

import hk.org.ha.model.pms.udt.vetting.ActionStatus;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class Fluid implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement(required = true)
	private String diluentName;
	
	@XmlElement
	private String diluentConcentration;
	
	@XmlElement
	private String diluentAbbr;

	@XmlElement
	private BigDecimal fluidVolume;
	
	@XmlElement
	private String fluidVolumeUnit;
	
	@XmlElement
	private Integer fluidQty;
	
	@XmlElement(required = true)
	private String baseUnit;

	@XmlElement(required = true)
	private String diluentCode;
	
	@XmlElement
	private BigDecimal finalVolume;
	
	@XmlElement
	private String finalVolumeUnit;

	@XmlElement(required = true)
	private String dilutionMethod;
	
	@XmlElement(required = true)
	private ActionStatus actionStatus;

	@XmlElement(name="PharmDrug")
	private List<PharmDrug> pharmDrugList;
	
	public String getDiluentName() {
		return diluentName;
	}

	public void setDiluentName(String diluentName) {
		this.diluentName = diluentName;
	}

	public String getDiluentConcentration() {
		return diluentConcentration;
	}

	public void setDiluentConcentration(String diluentConcentration) {
		this.diluentConcentration = diluentConcentration;
	}

	public String getDiluentAbbr() {
		return diluentAbbr;
	}

	public void setDiluentAbbr(String diluentAbbr) {
		this.diluentAbbr = diluentAbbr;
	}

	public BigDecimal getFluidVolume() {
		return fluidVolume;
	}

	public void setFluidVolume(BigDecimal fluidVolume) {
		this.fluidVolume = fluidVolume;
	}

	public String getFluidVolumeUnit() {
		return fluidVolumeUnit;
	}

	public void setFluidVolumeUnit(String fluidVolumeUnit) {
		this.fluidVolumeUnit = fluidVolumeUnit;
	}

	public Integer getFluidQty() {
		return fluidQty;
	}

	public void setFluidQty(Integer fluidQty) {
		this.fluidQty = fluidQty;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public String getDiluentCode() {
		return diluentCode;
	}

	public void setDiluentCode(String diluentCode) {
		this.diluentCode = diluentCode;
	}

	public BigDecimal getFinalVolume() {
		return finalVolume;
	}

	public void setFinalVolume(BigDecimal finalVolume) {
		this.finalVolume = finalVolume;
	}

	public String getFinalVolumeUnit() {
		return finalVolumeUnit;
	}

	public void setFinalVolumeUnit(String finalVolumeUnit) {
		this.finalVolumeUnit = finalVolumeUnit;
	}

	public String getDilutionMethod() {
		return dilutionMethod;
	}

	public void setDilutionMethod(String dilutionMethod) {
		this.dilutionMethod = dilutionMethod;
	}

	public ActionStatus getActionStatus() {
		return actionStatus;
	}

	public void setActionStatus(ActionStatus actionStatus) {
		this.actionStatus = actionStatus;
	}	

	public List<PharmDrug> getPharmDrugList() {
		if (pharmDrugList == null) {
			pharmDrugList = new ArrayList<PharmDrug>();
		}
		return pharmDrugList;
	}

	public void setPharmDrugList(List<PharmDrug> pharmDrugList) {
		this.pharmDrugList = pharmDrugList;
	}	
	
	public void addPharmDrug(PharmDrug pharmDrug) {
		this.getPharmDrugList().add(pharmDrug);
	}	
}
