package hk.org.ha.model.pms.persistence.disp;

import hk.org.ha.model.pms.udt.disp.MedOrderDocType;

import java.io.Serializable;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class MedOrderDocTypeAdapter extends XmlAdapter<String, MedOrderDocType> implements Serializable {

	private static final long serialVersionUID = 1L;

	public String marshal( MedOrderDocType v ) {		
		return (v == null) ? null : v.getDataValue();
	}
	
	public MedOrderDocType unmarshal( String v ) {		
		return MedOrderDocType.dataValueOf(v);
	}

}