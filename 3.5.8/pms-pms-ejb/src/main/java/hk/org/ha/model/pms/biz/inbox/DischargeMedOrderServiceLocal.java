package hk.org.ha.model.pms.biz.inbox;
import hk.org.ha.model.pms.udt.medprofile.DischargeMedOrderListType;
import hk.org.ha.model.pms.vo.medprofile.Ward;
import hk.org.ha.model.pms.vo.medprofile.WardGroup;

import java.util.List;

import javax.ejb.Local;

@Local
public interface DischargeMedOrderServiceLocal {
	void setShowAllWards(Boolean showAllWards);
	void setDischargeTypesAndFilters(DischargeMedOrderListType listType, List<Ward> wardFilterList,
			List<WardGroup> wardGroupFilterList, String caseNum);
	String setUrgent(Long id, Boolean urgent, Long version);
	void retrieveDischargeMedOrderList();	
	void destroy();
}
