package hk.org.ha.model.pms.biz.label;

import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.medprofile.Delivery;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.vo.label.BaseLabel;
import hk.org.ha.model.pms.vo.label.DispLabel;
import hk.org.ha.model.pms.vo.label.LabelContainer;
import hk.org.ha.model.pms.vo.label.MpDispLabel;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import org.apache.commons.lang3.tuple.MutableTriple;

@Local
public interface LabelManagerLocal  {

	List<DispLabel> getDispLabelPreview( PharmOrder pharmOrder, PrintLang printLang, String printType );
	
	List<MpDispLabel> getMpDispLabelPreview( MedProfileItem medProfileItem, MedProfileMoItem medProfileMoItem );
	
	BaseLabel getCddhDispLabel(List<String> dispOrderItemKey);
	
	MutableTriple<List<RenderAndPrintJob>, List<DispOrder>, List<Object>> genMpDispLabelPrintJobList(List<Delivery> deliveryList, List<Integer> atdpsPickStationsNumList);
	
	MutableTriple<List<RenderAndPrintJob>, List<DispOrder>, List<Object>> genPivasLabelPrintJobList(List<Delivery> deliveryList);
	
	void buildPivasLabelPrintJobList(List<LabelContainer> labelContainerList, List<RenderAndPrintJob> renderAndPrintJobList, PrintOption worksheetPrintOption, PrintOption printOption, List<Long> transactionIdList, boolean isReprint);
	
	void printMpDispLabel(List<RenderAndPrintJob> renderAndPrintJobList);
	
	RenderAndPrintJob printDeliveryLabel(Delivery delivery, boolean isReprint);
	
	MutableTriple<List<RenderAndPrintJob>, List<DispOrder>, List<Object>> printMpDispLabelList(Delivery delivery, List<Integer> atdpsPickStationsNumList);
	
	MutableTriple<List<RenderAndPrintJob>, List<DispOrder>, List<Object>> printMpDispLabelList(List<Delivery> deliveryList, List<Integer> atdpsPickStationsNumList);
	
	MutableTriple<List<RenderAndPrintJob>, List<DispOrder>, List<Object>> printPivasLabelList(List<Delivery> deliveryList);
	
	String retreiveDisplayDrugName(Map<String,String> fdnMappingMap, PharmOrderItem pharmOrderItem);

	void printDividerLabel(Delivery delivery, boolean isReprint);
	
	void getMpDispLabel(Delivery delivery);
}
