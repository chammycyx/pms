package hk.org.ha.model.pms.vo.vetting.mp;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.io.Serializable;
import java.text.DecimalFormat;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.category.CategoryDataset;

import net.sf.jasperreports.engine.JRChart;
import net.sf.jasperreports.engine.JRChartCustomizer;

public class RenalChartCustomizer implements JRChartCustomizer, Serializable {

	private static final long serialVersionUID = 1L;

	public void customize(JFreeChart chart, JRChart jasperChart) {

		CategoryPlot categoryPlot = chart.getCategoryPlot();
		
		NumberAxis numberAxis = (NumberAxis) categoryPlot.getRangeAxis();
		numberAxis.setVisible(false);
		numberAxis.setUpperMargin(0.2);
		numberAxis.setLowerMargin(0.2);
		numberAxis.setAutoRangeIncludesZero(false);
		
		CategoryAxis categoryAxis = categoryPlot.getDomainAxis();
		categoryAxis.setMaximumCategoryLabelLines(2);
		
		categoryPlot.setRenderer(new Renderer(chart.getTitle().getText().charAt(0) == ' '));
	}
	
	private class Renderer extends LineAndShapeRenderer {

		private static final long serialVersionUID = 1L;

		private boolean highlightLastPoint = false;
		
		public Renderer(boolean highlightLastPoint) {
			super();
			this.highlightLastPoint = highlightLastPoint;
			setBaseItemLabelsVisible(true);
			setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator("{2} mL/min", new DecimalFormat("#.#")));
			setBaseItemLabelFont(new Font("Arial", Font.PLAIN, 12));
		}
		
	    protected void drawItemLabel(Graphics2D g2, PlotOrientation orientation,
	            CategoryDataset dataset, int row, int column, double x, double y, boolean negative) {
			setBaseItemLabelPaint(highlightLastPoint && dataset.getColumnCount() == column + 1 ? Color.BLUE : Color.BLACK);
	    	super.drawItemLabel(g2, orientation, dataset, row, column, x, y, negative);
	    }
	}
}
