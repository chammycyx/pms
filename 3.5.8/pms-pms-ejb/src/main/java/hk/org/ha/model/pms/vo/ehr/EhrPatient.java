package hk.org.ha.model.pms.vo.ehr;

import hk.org.ha.model.pms.udt.Gender;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class EhrPatient implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String hkid;
	
	private String patKey;
	
	private String surname;
	
	private String givenName;
	
	private Date dob;
	
	private Gender sex;
	
	private String documentType;
	
	private Boolean nonHaIndicator;
	
	private String ehrNum;
	
	private Date ehrStartDate;
	
	private Date ehrEndDate;
	
	private String otherDocNo;

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getPatKey() {
		return patKey;
	}

	public void setPatKey(String patKey) {
		this.patKey = patKey;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Gender getSex() {
		return sex;
	}

	public void setSex(Gender sex) {
		this.sex = sex;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public Boolean getNonHaIndicator() {
		return nonHaIndicator;
	}

	public void setNonHaIndicator(Boolean nonHaIndicator) {
		this.nonHaIndicator = nonHaIndicator;
	}

	public String getEhrNum() {
		return ehrNum;
	}

	public void setEhrNum(String ehrNum) {
		this.ehrNum = ehrNum;
	}

	public Date getEhrStartDate() {
		return ehrStartDate;
	}

	public void setEhrStartDate(Date ehrStartDate) {
		this.ehrStartDate = ehrStartDate;
	}

	public Date getEhrEndDate() {
		return ehrEndDate;
	}

	public void setEhrEndDate(Date ehrEndDate) {
		this.ehrEndDate = ehrEndDate;
	}

	public String getOtherDocNo() {
		return otherDocNo;
	}

	public void setOtherDocNo(String otherDocNo) {
		this.otherDocNo = otherDocNo;
	}
}
