package hk.org.ha.model.pms.persistence.medprofile;

import hk.org.ha.model.pms.persistence.VersionEntity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PostLoad;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

@MappedSuperclass
public abstract class MedProfileOrderEntity extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "WARD_CODE", length = 4)
    private String wardCode;

    @Column(name = "SPEC_CODE", length = 4)
    private String specCode;
    
    @Column(name = "PAT_CAT_CODE", length = 2)
    private String patCatCode;
        
    @Column(name = "BODY_WEIGHT", precision = 5, scale = 2)
    private BigDecimal bodyWeight;
    
    @Column(name = "BODY_HEIGHT", precision = 5, scale = 2)
    private BigDecimal bodyHeight;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "BODY_INFO_UPDATE_DATE")
    private Date bodyInfoUpdateDate;
    
    private transient boolean orgWardCodeInited;
    private transient String orgWardCode;

    public MedProfileOrderEntity() {
    }
    
    @PostLoad
	public void postLoad() {
    	orgWardCodeInited = true;
    	orgWardCode = wardCode;
    }
    
	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		if (!orgWardCodeInited) {
			orgWardCode = wardCode;
			orgWardCodeInited = true;
		}
		this.wardCode = wardCode;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}
	
	public String getPatCatCode() {
		return patCatCode;
	}

	public void setPatCatCode(String patCatCode) {
		this.patCatCode = patCatCode;
	}
	
	public BigDecimal getBodyWeight() {
		return bodyWeight;
	}

	public void setBodyWeight(BigDecimal bodyWeight) {
		this.bodyWeight = bodyWeight;
	}

	public BigDecimal getBodyHeight() {
		return bodyHeight;
	}

	public void setBodyHeight(BigDecimal bodyHeight) {
		this.bodyHeight = bodyHeight;
	}

	public Date getBodyInfoUpdateDate() {
		return bodyInfoUpdateDate == null ? null : new Date(bodyInfoUpdateDate.getTime());
	}

	public void setBodyInfoUpdateDate(Date bodyInfoUpdateDate) {
		this.bodyInfoUpdateDate = bodyInfoUpdateDate == null ? null : new Date(bodyInfoUpdateDate.getTime());
	}

	@Transient
	public boolean isWardCodeChanged() {
		return orgWardCodeInited && !StringUtils.equals(orgWardCode, wardCode);
	}
}
