package hk.org.ha.model.pms.vo.picking;

import hk.org.ha.model.pms.udt.reftable.MachineType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PickItem implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long dispOrderId;
	
	private Long dispOrderItemId;
	
	private Date ticketDate;
	
	private String ticketNum;
	
	private Integer itemNum;
	
	private String itemCode;
	
	private String binNum;
	
	private Integer dispQty;
	
	private String baseUnit;
	 
	private MachineType machineType;
	
	private String suspendCode;
	
	private String orderNum;
	
	private Long dispOrderVersion;
	
	private boolean markReprint;

	public PickItem(Long dispOrderId, Long dispOrderItemId, Date ticketDate, String ticketNum, 
						Integer itemNum, String itemCode, BigDecimal dispQty, String baseUnit, 
						String suspendCode, String orderNum, String binNum, Long dispOrderVersion) {
		
		this.dispOrderId = dispOrderId;
		this.dispOrderItemId = dispOrderItemId;
		this.ticketDate = ticketDate;
		this.ticketNum = ticketNum;
		this.itemNum = itemNum;
		this.itemCode = itemCode;
		this.dispQty = dispQty.intValue();
		this.baseUnit = baseUnit;
		this.suspendCode = suspendCode;
		this.orderNum = orderNum;
		this.binNum = binNum;
		this.dispOrderVersion = dispOrderVersion;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public Integer getDispQty() {
		return dispQty;
	}

	public void setDispQty(Integer dispQty) {
		this.dispQty = dispQty;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public void setDispOrderItemId(Long dispOrderItemId) {
		this.dispOrderItemId = dispOrderItemId;
	}

	public Long getDispOrderItemId() {
		return dispOrderItemId;
	}

	public void setDispOrderId(Long dispOrderId) {
		this.dispOrderId = dispOrderId;
	}

	public Long getDispOrderId() {
		return dispOrderId;
	}
	
	public void setSuspendCode(String suspendCode) {
		this.suspendCode = suspendCode;
	}

	public String getSuspendCode() {
		return suspendCode;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setBinNum(String binNum) {
		this.binNum = binNum;
	}

	public String getBinNum() {
		return binNum;
	}

	public void setMachineType(MachineType machineType) {
		this.machineType = machineType;
	}

	public MachineType getMachineType() {
		return machineType;
	}

	public void setMarkReprint(boolean markReprint) {
		this.markReprint = markReprint;
	}

	public boolean isMarkReprint() {
		return markReprint;
	}

	public void setDispOrderVersion(Long dispOrderVersion) {
		this.dispOrderVersion = dispOrderVersion;
	}

	public Long getDispOrderVersion() {
		return dispOrderVersion;
	}

	public void setTicketDate(Date ticketDate) {
		this.ticketDate = ticketDate;
	}

	public Date getTicketDate() {
		return ticketDate;
	}
}