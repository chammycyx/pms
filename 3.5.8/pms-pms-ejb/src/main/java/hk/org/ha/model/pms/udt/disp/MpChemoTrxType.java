package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;
import hk.org.ha.model.pms.udt.medprofile.MedProfileOrderType;

public enum MpChemoTrxType implements StringValuedEnum {

	// From MOE Ordering to PMS
	MpChangeChemoOrder("CP1", "Add, modify, delete item and override mds for chemo profile", MpChemoMessageType.ChangeOrder, null, Boolean.TRUE);
	
	private final String dataValue;
	private final String displayValue;
	private final MpChemoMessageType messageType;
	private final MedProfileOrderType orderType;
	private final Boolean toPmsFlag;
	
	MpChemoTrxType (final String dataValue, final String displayValue, final MpChemoMessageType messageType, final MedProfileOrderType orderType, Boolean toPmsFlag) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
		this.messageType = messageType;
		this.orderType = orderType;
		this.toPmsFlag = toPmsFlag;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}

	public MpChemoMessageType getMessageType() {
		return this.messageType;
	}
	
	public MedProfileOrderType getOrderType() {
		return this.orderType;
	}

	public Boolean getToPmsFlag() {
		return this.toPmsFlag;
	}
	
	public static MpChemoTrxType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MpChemoTrxType.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<MpChemoTrxType> {

		private static final long serialVersionUID = 1L;

		public Class<MpChemoTrxType> getEnumClass() {
    		return MpChemoTrxType.class;
    	}
    }
}
