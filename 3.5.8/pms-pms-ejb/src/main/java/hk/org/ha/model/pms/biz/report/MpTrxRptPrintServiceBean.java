package hk.org.ha.model.pms.biz.report;

import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.delivery.DeliveryManagerLocal;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.persistence.medprofile.Delivery;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.MpTrxRpt;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("mpTrxRptPrintService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MpTrxRptPrintServiceBean implements MpTrxRptPrintServiceLocal {

	@In
	private MpTrxRptPrintManagerLocal mpTrxRptPrintManager;
	
	@In
	private AuditLogger auditLogger; 
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent; 
	
	@In
	private DeliveryManagerLocal deliveryManager;
	
	
	private List<MpTrxRpt> mpTrxRptList;

	private boolean reprintUpdateFlag = false;
	
	private DateFormat df = new SimpleDateFormat("yyyymm"); 	
	
	private Date dispenseDateForSearch = new Date();
	
	
	public void retrieveMpTrxRptListByBatchNum(String batchNum, Date dispenseDate) {
		dispenseDateForSearch = dispenseDate;
		mpTrxRptList = mpTrxRptPrintManager.retrieveMpTrxRptListByBatchNum( batchNum, dispenseDate );
		reprintUpdateFlag = retrieveDeliveryModifyDate(batchNum);
		
		
		
		
	}

	public void generateMpTrxRpt() {
	
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		if(reprintUpdateFlag){
			parameters.put("reprintUpdateFlag", true);
		}else{
			parameters.put("reprintFlag", true);

		}
		
		
		printAgent.renderAndRedirect(new RenderJob(
				"MpTrxRpt", 
				new PrintOption(), 
				parameters, 
				mpTrxRptList));
		
	}

	public void printMpTrxRptForReport(){
	
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		if(reprintUpdateFlag){
			parameters.put("reprintUpdateFlag", true);
		}else{
			parameters.put("reprintFlag", true);

		}
		
		printAgent.renderAndPrint(new RenderAndPrintJob(
				"MpTrxRpt", 
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				parameters, 
				mpTrxRptList));
		
		auditLogger.log("#0603",   df.format(dispenseDateForSearch)+ mpTrxRptList.get(0).getMpTrxRptHdr().getBatchCode());
		
	}
	
	private boolean retrieveDeliveryModifyDate(String batchNum){
		
		List<DeliveryItem> deliveryItemList = deliveryManager.retrieveDeliveryItemListByBatchNum(batchNum, null, null);
		
		
		if (deliveryItemList.size() > 0) {
			Delivery delivery = deliveryItemList.get(0).getDelivery();
			
			if(delivery.getModifyDate() != null){
				return true;
			}
		}
		return false;
		
	} 
	
	
	@Remove
	public void destroy() {
		
		if(mpTrxRptList!=null){
			mpTrxRptList=null;
		}
	}
}