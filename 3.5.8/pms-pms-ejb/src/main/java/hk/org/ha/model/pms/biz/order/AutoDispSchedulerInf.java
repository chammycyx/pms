package hk.org.ha.model.pms.biz.order;

import hk.org.ha.model.pms.persistence.corp.Workstore;


public interface AutoDispSchedulerInf {

	boolean configurated(Workstore workstore);
	
	void checkInWorkstation(Workstore workstore, String workstationKey);

	String checkOutWorkstation(Workstore workstore);
	
	void scheduleMedOrder(Workstore workstore, String orderNum);

	String getNextOrderNumToRun(Workstore workstore);
}
