package hk.org.ha.model.pms.persistence.reftable;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "DRS_CONFIG")
@Customizer(AuditCustomizer.class)
public class DrsConfig extends VersionEntity {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "drsConfigSeq")
	@SequenceGenerator(name = "drsConfigSeq", sequenceName = "SQ_DRS_CONFIG", initialValue = 100000000)
	private Long id;
	
	@Column(name = "TYPE", nullable = false, length = 1)
	private String type;
	
	@Column(name = "PAS_SPEC_CODE", nullable = false, length = 4)
	private String pasSpecCode;
	
	@Column(name = "MR_REMINDER_FLAG", nullable = false)
	private Boolean mrReminderFlag;
	
	@Column(name = "ITEM_COUNT", nullable = false, length = 3)
	private Integer itemCount;
	
	@Column(name = "THRESHOLD", nullable = false, length = 3)
	private Integer threshold;
	
	@Column(name = "INTERVAL", nullable = false, length = 3)
	private Integer interval;
	
	@Column(name = "LAST_INTERVAL_MAX_DAY", nullable = false, length = 3)
	private Integer lastIntervalMaxDay;
	
    @OneToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE", referencedColumnName = "HOSP_CODE", nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
    private Workstore workstore;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPasSpecCode() {
		return pasSpecCode;
	}

	public void setPasSpecCode(String pasSpecCode) {
		this.pasSpecCode = pasSpecCode;
	}

	public Boolean getMrReminderFlag() {
		return mrReminderFlag;
	}

	public void setMrReminderFlag(Boolean mrReminderFlag) {
		this.mrReminderFlag = mrReminderFlag;
	}

	public Integer getItemCount() {
		return itemCount;
	}

	public void setItemCount(Integer itemCount) {
		this.itemCount = itemCount;
	}

	public Integer getThreshold() {
		return threshold;
	}

	public void setThreshold(Integer threshold) {
		this.threshold = threshold;
	}

	public Integer getInterval() {
		return interval;
	}

	public void setInterval(Integer interval) {
		this.interval = interval;
	}

	public Integer getLastIntervalMaxDay() {
		return lastIntervalMaxDay;
	}

	public void setLastIntervalMaxDay(Integer lastIntervalMaxDay) {
		this.lastIntervalMaxDay = lastIntervalMaxDay;
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}
}
