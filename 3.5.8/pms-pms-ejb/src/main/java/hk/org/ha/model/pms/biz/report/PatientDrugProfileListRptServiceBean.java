package hk.org.ha.model.pms.biz.report;

import static hk.org.ha.model.pms.prop.Prop.ALERT_EHR_PROFILE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.ALERT_PROFILE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.MEDPROFILE_ALERT_HLA_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.REPORT_TRX_HKID_VISIBLE;
import static hk.org.ha.model.pms.prop.Prop.VETTING_MEDPROFILE_ALERT_PROFILE_ENABLED;
import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.alert.AlertProfileException;
import hk.org.ha.model.pms.asa.exception.alert.ehr.EhrAllergyException;
import hk.org.ha.model.pms.biz.alert.AlertProfileManagerLocal;
import hk.org.ha.model.pms.biz.alert.ehr.EhrAlertManagerLocal;
import hk.org.ha.model.pms.biz.alert.mds.AlertMsgManagerLocal;
import hk.org.ha.model.pms.biz.label.InstructionBuilder;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.biz.order.DispOrderConverterLocal;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.biz.reftable.RefTableManagerLocal;
import hk.org.ha.model.pms.biz.reftable.WardConfigManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmRegimenCacherInf;
import hk.org.ha.model.pms.dms.biz.formatengine.FormatHints;
import hk.org.ha.model.pms.dms.biz.formatengine.ParsedOrderLineFormatter;
import hk.org.ha.model.pms.dms.biz.formatengine.ParsedOrderLineFormatterBase;
import hk.org.ha.model.pms.dms.biz.formatengine.ParsedOrderLineFormatterFactory;
import hk.org.ha.model.pms.dms.persistence.DmRegimen;
import hk.org.ha.model.pms.dms.udt.formatengine.FormatType;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItemAlert;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.persistence.reftable.WardConfig;
import hk.org.ha.model.pms.prop.Prop;
import hk.org.ha.model.pms.udt.alert.AlertProfileStatus;
import hk.org.ha.model.pms.udt.alert.AllergenType;
import hk.org.ha.model.pms.udt.alert.ehr.EhrAlertStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatus;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.alert.AlertProfile;
import hk.org.ha.model.pms.vo.alert.PatAdr;
import hk.org.ha.model.pms.vo.alert.PatAllergy;
import hk.org.ha.model.pms.vo.alert.mds.AlertDdim;
import hk.org.ha.model.pms.vo.label.Instruction;
import hk.org.ha.model.pms.vo.label.MpDispLabel;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.MpTrxRpt;
import hk.org.ha.model.pms.vo.report.MpTrxRptDtl;
import hk.org.ha.model.pms.vo.report.MpTrxRptHdr;
import hk.org.ha.model.pms.vo.report.MpTrxRptItemDtl;
import hk.org.ha.model.pms.vo.report.MpTrxRptPoItemDtl;
import hk.org.ha.model.pms.vo.report.MpTrxRptPoItemHdr;
import hk.org.ha.model.pms.vo.report.PatientDrugProfileListRptInfo;
import hk.org.ha.model.pms.vo.rx.CapdRxDrug;
import hk.org.ha.model.pms.vo.rx.IvRxDrug;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;

import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
@Stateful
@Scope(ScopeType.SESSION)
@Name("patientDrugProfileListRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PatientDrugProfileListRptServiceBean implements PatientDrugProfileListRptServiceLocal {

	private static final String MED_PROFILE_ITEM_QUERY_HINT_BATCH_1 = "i.medProfileMoItem.medProfilePoItemList";
	private static final String MED_PROFILE_ITEM_QUERY_HINT_BATCH_LOOKUP_1 = "i.medProfileMoItem.medProfilePoItemList.medProfileMoItem";
	private static final String MED_PROFILE_ITEM_QUERY_HINT_BATCH_2 = "i.medProfileMoItem.medProfileMoItemFmList";
	private static final String MED_PROFILE_ITEM_QUERY_HINT_BATCH_3 = "i.medProfileMoItem.medProfileMoItemAlertList";
	private static final String MED_PROFILE_ITEM_QUERY_HINT_BATCH_4 = "i.medProfileErrorLog";
	private static final String MED_PROFILE_ITEM_QUERY_HINT_BATCH_5 = "i.medProfile.patient";
	private static final String MED_PROFILE_ITEM_QUERY_HINT_BATCH_6 = "i.medProfile.medCase";

	private static final String DAYS = "day(s)";
	private static final int MAX_DRUG_ORDER_TLF_LENGTH = 55;
	
	private static final char ASTERISK = '*';
	private static final String NEW_LINE = "\n";
	private static final String TLF_BREAK_CHAR = "<span breakOpportunity=\"any\"> </span>";
	private static final char[] DELIMITER = new char[] {' ' , '~' , '&' , '-' , '[' , '(' , '<'}; 
	
	@PersistenceContext
	private EntityManager em;
	
	@In 
	private Workstore workstore;
	
	@In
	private PrintAgentInf printAgent;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private WardConfigManagerLocal wardConfigManager;

	@In
	private AlertMsgManagerLocal alertMsgManager;
	
	@In
	private AlertProfileManagerLocal alertProfileManager;
	
	@In
	private EhrAlertManagerLocal ehrAlertManager;

	@In
	private DmRegimenCacherInf dmRegimenCacher;
	
	@In
	private RefTableManagerLocal refTableManager;
	
	@In
	private DispOrderConverterLocal dispOrderConverter;
	
	@In
	private InstructionBuilder instructionBuilder;
	
	private List<MpTrxRpt> patientDrugProfileRptList;
	
	private boolean retrieveSuccess;
	
    private Map<String, Object> parameters = new HashMap<String, Object>();
    
    public PatientDrugProfileListRptInfo retrievePatientDrugProfileListRptInfo(){
    	PatientDrugProfileListRptInfo patientDrugProfileListRptInfo = new PatientDrugProfileListRptInfo();
    	List<String> mpWardCodeList = retrieveMpWardCodeList();
    	patientDrugProfileListRptInfo.setWardCodeList(mpWardCodeList);
    	
    	return patientDrugProfileListRptInfo;
    }
    
    private List<String> retrieveMpWardCodeList(){
    	List<String> mpWardCodeList = new ArrayList<String>();
    	List<WardConfig> wardConfigList = wardConfigManager.retrieveMpWardConfigList(workstore.getWorkstoreGroup());
    	
		for ( WardConfig wardConfig : wardConfigList ) {
			mpWardCodeList.add(wardConfig.getWardCode());
		}
    	
    	return mpWardCodeList;
    }
    
    public void retrievePatientDrugProfileListRpt(PatientDrugProfileListRptInfo patientDrugProfileListRptInfo){
    	retrieveSuccess = false; 
    	patientDrugProfileRptList = new ArrayList<MpTrxRpt>();

		boolean searchByHkidCaseNum;
		String hkidCaseNum = null;
    	String wardCode = null;
    	if ( ! StringUtils.isEmpty(patientDrugProfileListRptInfo.getHkidCaseNum())) {
    		searchByHkidCaseNum = true;
    		hkidCaseNum = patientDrugProfileListRptInfo.getHkidCaseNum();
    	} else {
    		searchByHkidCaseNum = false;
    		wardCode = patientDrugProfileListRptInfo.getWardCodeList().get(0);
    	}


		List<MedProfileItem> medProfileItemList = retrieveMedProfileItemList(searchByHkidCaseNum, hkidCaseNum, wardCode, patientDrugProfileListRptInfo.getPrintEndedItemFlag(), patientDrugProfileListRptInfo.getStartItemEndedDate());

		if (medProfileItemList.size() > 0) {
			patientDrugProfileRptList.add( convertMpTrxRpt(medProfileItemList) );
			
			for (MpTrxRpt patientDrugProfileRpt : patientDrugProfileRptList) {
				patientDrugProfileRpt.getMpTrxRptHdr().setPatientDrugProfileListFlag(Boolean.TRUE);
			}
			retrieveSuccess = true;
		}
    }

    @SuppressWarnings("unchecked")
	private List<MedProfileItem> retrieveMedProfileItemList(boolean searchByHkidCaseNum, String hkidCaseNum, String wardCode, Boolean printEndedItemFlag, Date startItemEndedDate) {
    	StringBuilder sqlSb = new StringBuilder();
    	
    	sqlSb.append("select i from MedProfileItem i") // 20160809 index check : MedCase.caseName Patient.hkid MedProfile.hospCode,wardCode,status : I_MED_CASE_01 I_PATIENT_02 I_MED_PROFILE_06
    			.append(" where i.medProfile.status in :mpStatus")
				.append(" and i.medProfile.hospCode = :hospCode")
				.append(" and (i.medProfileMoItem.transferFlag = false or i.medProfileMoItem.transferFlag is null)");
    	if (searchByHkidCaseNum) {
    		sqlSb.append(" and (i.medProfile.medCase.caseNum = :hkidCaseNum");
    		sqlSb.append(" or i.medProfile.patient.hkid = :hkidCaseNum)");
    	} else {
    		sqlSb.append(" and i.medProfile.wardCode = :wardCode");
    	}
    	if (printEndedItemFlag) {
    		sqlSb.append(" and (i.status = :mpiStatusVerify");
    		sqlSb.append(" or (i.status in :mpiStatusEnded and (:startItemEndedDate is null or i.medProfileMoItem.endDate is null or i.medProfileMoItem.endDate >= :startItemEndedDate)))");
    	} else {
        	sqlSb.append(" and i.status = :mpiStatusVerify");
    	}
   	
    	Query query = em.createQuery(sqlSb.toString()) 
						.setParameter("mpStatus", MedProfileStatus.Not_Inactive)
						.setParameter("hospCode", workstore.getHospCode());
    	if (searchByHkidCaseNum) {
    		query = query.setParameter("hkidCaseNum", hkidCaseNum);
    	} else {
    		query = query.setParameter("wardCode", wardCode);
    	}
    	if (printEndedItemFlag) {
        	List<MedProfileItemStatus> mpiStatusEndedList = new ArrayList<MedProfileItemStatus>();
    		mpiStatusEndedList.add(MedProfileItemStatus.Discontinued);
    		mpiStatusEndedList.add(MedProfileItemStatus.Ended);
    		
        	query = query.setParameter("mpiStatusVerify", MedProfileItemStatus.Verified);
        	query = query.setParameter("mpiStatusEnded", mpiStatusEndedList);
        	query = query.setParameter("startItemEndedDate", startItemEndedDate);
    	} else {
        	query = query.setParameter("mpiStatusVerify", MedProfileItemStatus.Verified);
    	}
    	
    	List<MedProfileItem> medProfileItemList = query.setHint(QueryHints.BATCH, MED_PROFILE_ITEM_QUERY_HINT_BATCH_1)
		    			.setHint(QueryHints.BATCH, MED_PROFILE_ITEM_QUERY_HINT_BATCH_2)
		    			.setHint(QueryHints.BATCH, MED_PROFILE_ITEM_QUERY_HINT_BATCH_3)
		    			.setHint(QueryHints.BATCH, MED_PROFILE_ITEM_QUERY_HINT_BATCH_4)
		    			.setHint(QueryHints.BATCH, MED_PROFILE_ITEM_QUERY_HINT_BATCH_5)
		    			.setHint(QueryHints.BATCH, MED_PROFILE_ITEM_QUERY_HINT_BATCH_6)
						.getResultList();
    	
    	
    	MedProfileUtils.lookup(medProfileItemList, MED_PROFILE_ITEM_QUERY_HINT_BATCH_LOOKUP_1);
    	MedProfileUtils.lookup(medProfileItemList, MED_PROFILE_ITEM_QUERY_HINT_BATCH_2);
    	MedProfileUtils.lookup(medProfileItemList, MED_PROFILE_ITEM_QUERY_HINT_BATCH_3);
    	MedProfileUtils.lookup(medProfileItemList, MED_PROFILE_ITEM_QUERY_HINT_BATCH_4);
    	MedProfileUtils.lookup(medProfileItemList, MED_PROFILE_ITEM_QUERY_HINT_BATCH_5);
    	MedProfileUtils.lookup(medProfileItemList, MED_PROFILE_ITEM_QUERY_HINT_BATCH_6);
    	
    	for (MedProfileItem medProfileItem: medProfileItemList) {
    		medProfileItem.getMedProfileMoItem().loadDmInfo();
    		for (MedProfilePoItem medProfilePoItem: medProfileItem.getMedProfileMoItem().getMedProfilePoItemList()) {
    			medProfilePoItem.loadDmInfo();
    		}
    	}
    	
    	em.clear();
    	
    	return medProfileItemList;
    }
    
    private MpTrxRpt convertMpTrxRpt(List<MedProfileItem> medProfileItemList) {
    	MpTrxRpt mpTrxRpt = new MpTrxRpt();
    	
    	// group medProfileItemList by case number or HKID
    	Map<String, List<MedProfileItem>> medProfileItemByCaseNumOrHkidMap = new HashMap<String, List<MedProfileItem>>();
    	for (MedProfileItem medProfileItem : medProfileItemList) {
    		String caseNumHkid = medProfileItem.getMedProfile().getMedCase().getCaseNum();
    		if (caseNumHkid == null) {
    			caseNumHkid = medProfileItem.getMedProfile().getPatient().getHkid();
    		}
    		List<MedProfileItem> tempMedProfileItemList = new ArrayList<MedProfileItem>(); 
    		if (medProfileItemByCaseNumOrHkidMap.containsKey(caseNumHkid)) {
    			tempMedProfileItemList = medProfileItemByCaseNumOrHkidMap.get(caseNumHkid);
    		} else {
    			tempMedProfileItemList = new ArrayList<MedProfileItem>();
    			medProfileItemByCaseNumOrHkidMap.put(caseNumHkid, tempMedProfileItemList);
    		}
    		tempMedProfileItemList.add(medProfileItem);
    	}

    	boolean alertProfileFlag = false;
    	boolean ehrAlertProfileFlag = false;
    	
    	// set MpTrxRptDtl by case number group
    	List<MpTrxRptDtl> mpTrxRptDtlList = new ArrayList<MpTrxRptDtl>();
    	for (List<MedProfileItem> groupedMedProfileItemList : medProfileItemByCaseNumOrHkidMap.values()) {
    		// groupedMedProfileItemList contains all MedProfileItem of a case number
        	AlertProfile alertProfile = retrieveAlertProfile(groupedMedProfileItemList.get(0).getMedProfile());
        	alertProfile = retrieveEhrAlertProfile(groupedMedProfileItemList.get(0).getMedProfile(), alertProfile);
	    	mpTrxRptDtlList.add( convertMpTrxRptDtl(groupedMedProfileItemList, alertProfile) );
	    	
	    	if (alertProfile != null) {
	    		if (!alertProfile.isEhrAllergyOnly()) {
	    			alertProfileFlag = true;
	    		}
	    		if (alertProfile.getEhrAllergySummary() != null && alertProfile.getEhrAllergySummary().getStatus() != EhrAlertStatus.Error) {
	    			ehrAlertProfileFlag = true;
	    		}
	    	}
	    	
    	}
    	mpTrxRpt.setMpTrxRptDtlList(mpTrxRptDtlList);

    	// sort report item
    	sortMpTrxRptDtlList(mpTrxRpt.getMpTrxRptDtlList());
    	
    	// after sorting, set separate line of last MpTrxRptItemDtl to invisible 
    	updateLastMpTrxRptItemDtlLineSeparator(mpTrxRpt.getMpTrxRptDtlList());
    	
    	// set MpTrxRptHdr
    	mpTrxRpt.setMpTrxRptHdr( convertMpTrxRptHdr(medProfileItemList.get(0).getMedProfile().getWardCode(), alertProfileFlag, ehrAlertProfileFlag) );

    	return mpTrxRpt;
    }
    
    private AlertProfile retrieveAlertProfile(MedProfile medProfile) {
    	try {
    		if (ALERT_PROFILE_ENABLED.get(Boolean.FALSE) &&
    				VETTING_MEDPROFILE_ALERT_PROFILE_ENABLED.get(Boolean.FALSE)) {
    			return alertProfileManager.retrieveAlertProfileByMedProfile(medProfile);
    		}
    		return null;
    		
    	} catch (AlertProfileException ex) {
    		throw new RuntimeException(ex);
    	}
    }
    
    private AlertProfile retrieveEhrAlertProfile(MedProfile medProfile, AlertProfile alertProfile) {
		if (ALERT_EHR_PROFILE_ENABLED.get()) {
	    	try {
				return ehrAlertManager.retrieveEhrAllergySummaryByMedProfile(alertProfile, medProfile);
			}
	    	catch (EhrAllergyException e) {
	    		throw new RuntimeException(e);
			}
		}
		else {
			return alertProfile;
		}
    }

    private void sortMpTrxRptDtlList(List<MpTrxRptDtl> mpTrxRptDtlList) {
    	// sort by patient
    	Collections.sort(mpTrxRptDtlList, new MpTrxRptDtlComparator());
    	
    	for (MpTrxRptDtl mpTrxRptDtl : mpTrxRptDtlList) {
    		// sort MpTrxRptPoItemDtl for MpTrxRptItemDtl sorting
    		for (MpTrxRptItemDtl mpTrxRptItemDtl : mpTrxRptDtl.getMpTrxRptItemDtlList()) {
				Collections.sort(mpTrxRptItemDtl.getMpTrxRptPoItemDtlList(), new MpTrxRptPoItemDtlComparatorForMpTrxRptItemDtlSorting());
    		}
    		
    		// sort by order line
    		Collections.sort(mpTrxRptDtl.getMpTrxRptItemDtlList(), new MpTrxRptItemDtlComparator());
    		
    		// sort by pharm line
    		for (MpTrxRptItemDtl mpTrxRptItemDtl : mpTrxRptDtl.getMpTrxRptItemDtlList()) {
				Collections.sort(mpTrxRptItemDtl.getMpTrxRptPoItemDtlList(), new MpTrxRptPoItemDtlComparator());
    		}
    	}
    }
    
    private void updateLastMpTrxRptItemDtlLineSeparator(List<MpTrxRptDtl> mpTrxRptDtlList) {
    	for (MpTrxRptDtl mpTrxRptDtl : mpTrxRptDtlList) {
        	// set separate line of last MpTrxRptItemDtl to invisible 
    		if ( ! mpTrxRptDtl.getMpTrxRptItemDtlList().isEmpty() ) {
    			mpTrxRptDtl.getMpTrxRptItemDtlList().get( mpTrxRptDtl.getMpTrxRptItemDtlList().size()-1 ).setSeparateLineIndicator(false);
    		}
    	}
    }
    
    private MpTrxRptHdr convertMpTrxRptHdr(String wardCode, boolean alertProfileFlag, boolean ehrAlertProfileFlag) {
    	MpTrxRptHdr mpTrxRptHdr = new MpTrxRptHdr();
    	
    	mpTrxRptHdr.setWardCode(wardCode);
    	mpTrxRptHdr.setMdsCheckFlag(Boolean.TRUE);
    	mpTrxRptHdr.setAlertProfileFlag(alertProfileFlag);
    	mpTrxRptHdr.setEhrAlertProfileFlag(ehrAlertProfileFlag);
    	mpTrxRptHdr.setDdiCheckFlag(Boolean.TRUE);
    	
    	return mpTrxRptHdr;
    }
    
    private MpTrxRptDtl convertMpTrxRptDtl(List<MedProfileItem> medProfileItemList, AlertProfile alertProfile) {
    	MpTrxRptDtl mpTrxRptDtl = new MpTrxRptDtl();
    	
    	MedProfile medProfile = medProfileItemList.get(0).getMedProfile();
    	MedCase medCase = medProfile.getMedCase();
    	Patient patient = medProfile.getPatient();

		convertMpTrxRptDtlAlertProfile(alertProfile, mpTrxRptDtl);
    	
		mpTrxRptDtl.setBedNum(medCase.getPasBedNum());
		mpTrxRptDtl.setCaseNum(medCase.getCaseNum());
		mpTrxRptDtl.setWard(medProfile.getWardCode());
		mpTrxRptDtl.setSpecCode(medProfile.getSpecCode());
		mpTrxRptDtl.setCheckPregnancyFlag(medProfile.getPregnancyCheckFlag());
		
		mpTrxRptDtl.setPatName(patient.getName());
		mpTrxRptDtl.setPatNameChi(patient.getNameChi());
		mpTrxRptDtl.setHkid(patient.getHkid());
		if( medCase.getCaseNum() != null ){
			mpTrxRptDtl.setPrintPatHkidFlag(REPORT_TRX_HKID_VISIBLE.get(false));
		} else {
			mpTrxRptDtl.setPrintPatHkidFlag(true);
		}
		mpTrxRptDtl.setPrivateFlag(medProfile.getPrivateFlag());
		mpTrxRptDtl.setDrugSensitivityDesc(medProfile.getDrugSensitivity());
		
		if (patient.getSex() != null) {
			mpTrxRptDtl.setSex(patient.getSex().getDataValue());
		}
		if (patient.getAge() != null) {
			mpTrxRptDtl.setAge(patient.getAge()+patient.getDateUnit().substring(0, 1));
		}
		
		if (medProfile.getBodyWeight() != null){
			DecimalFormat df = new DecimalFormat("0.##");
			String bodyWeight = df.format(medProfile.getBodyWeight().stripTrailingZeros());
			mpTrxRptDtl.setBodyWeight(bodyWeight);
		}
    	
		// set MpTrxRptItemDtl list
    	List<MpTrxRptItemDtl> mpTrxRptItemDtlList = new ArrayList<MpTrxRptItemDtl>();
    	for (MedProfileItem medProfileItem : medProfileItemList) {
    		mpTrxRptItemDtlList.add( convertMpTrxRptItemDtl(medProfileItem) );
    	}
    	mpTrxRptDtl.setMpTrxRptItemDtlList(mpTrxRptItemDtlList);

    	return mpTrxRptDtl;
    }
    
    private void convertMpTrxRptDtlAlertProfile(AlertProfile alertProfile, MpTrxRptDtl mpTrxRptDtl) {
    	if (alertProfile == null) {
    		return;
    	}
    	
		mpTrxRptDtl.setAlertProfileStatus(alertProfile.getStatus().toString());
		
		if ( ! alertProfile.getStatus().equals(AlertProfileStatus.Error) ) {
			//set Drug Allergy
			StringBuilder allergenSb = new StringBuilder();
			for ( PatAllergy patAllergy : alertProfile.getPatAllergyList()) {
				if (patAllergy.getAllergenType() == AllergenType.NonDrug
						 || patAllergy.getAllergenType() == AllergenType.NoKnownDrugAllergy
						 || patAllergy.getAllergenType() == AllergenType.XP) {
							continue;
				}
				
				if ( StringUtils.isNotBlank(allergenSb.toString()) ) {
					allergenSb.append(" , ");
				}
				allergenSb.append(patAllergy.getAllergenName());
			}
			

			if (StringUtils.trimToNull(allergenSb.toString()) == null){
				mpTrxRptDtl.setNoKnownDrugFlag(alertProfile.getLastVerifyDate() != null? true: false);
			}
			
		
			mpTrxRptDtl.setDrugAllergenDesc(StringUtils.trimToNull(allergenSb.toString()));

			//set Adverse Drug Reaction
			StringBuilder adrSb = new StringBuilder();
			for ( PatAdr patAdr : alertProfile.getPatAdrList()) {
				if ( StringUtils.isNotBlank(adrSb.toString()) ) {
					adrSb.append(" , ");
				}
				adrSb.append(patAdr.getDrugName());
				
			}
			mpTrxRptDtl.setAdrAlertDesc(StringUtils.trimToNull(adrSb.toString()));
			
			//set other Alert
			if ( alertProfile.getG6pdAlert() != null ) {
				mpTrxRptDtl.setOtherAllergyDesc(StringUtils.trimToNull(alertProfile.getG6pdAlert().getAlertDesc()));
			}
		}
		
		if (alertProfile.getEhrAllergySummary() != null) {
			mpTrxRptDtl.setEhrAllergy(alertProfile.getEhrAllergySummary().getAllergy());
			mpTrxRptDtl.setEhrAdr(alertProfile.getEhrAllergySummary().getAdr());
		}
    }
    
    private MpTrxRptItemDtl convertMpTrxRptItemDtl(MedProfileItem medProfileItem) {
    	MpTrxRptItemDtl mpTrxRptItemDtl = new MpTrxRptItemDtl();
    	
    	MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
    	RxItem rxItem = medProfileMoItem.getRxItem();
    	
    	if( medProfileItem.getOrgItemNum() > 10000 ){
    		if( Prop.REPORT_PATIENTDRUGPROFILELIST_DOSAGEINSTRUCTION_VISIBLE.get(false) ){
	    		PharmOrderItem poiForInstruction = dispOrderConverter.convertToPharmOrderItemForInstructionList(medProfileMoItem.getMedProfilePoItemList().get(0), workstore);
	    		poiForInstruction.loadDmInfo();
	    		DispOrderItem doiForInstruction = new DispOrderItem();
	    		doiForInstruction.setLegacy(false);
	    		doiForInstruction.setPharmOrderItem(poiForInstruction);
	    		DispOrder dispOrder = new DispOrder();
	    		doiForInstruction.setDispOrder(dispOrder);
	    		List<Instruction> instructionList = instructionBuilder.buildInstructionListForDispLabel(doiForInstruction);
	    		MpDispLabel mpDispLabel = new MpDispLabel();
	    		PrintOption printOption = new PrintOption();
				printOption.setPrintLang(PrintLang.Eng);
	    		mpDispLabel.setInstructionList(instructionList);
	    		mpDispLabel.setPrintOption(printOption);
	    		mpTrxRptItemDtl.setDrugOrder(mpDispLabel.getInstructionText());
    		}
    	}else{
			//Format for IP only
	    	if( rxItem.getDrugOrderXml() != null ){
	    		mpTrxRptItemDtl.setDrugOrder(convertDrugOrderXml(rxItem.getDrugOrderXml(), rxItem.getInterfaceVersion()));
	    	}else{
	    		mpTrxRptItemDtl.setDrugOrder(convertDrugOrderTlf(rxItem.getFormattedDrugOrderTlf()));
	    	}
    	}
		
		mpTrxRptItemDtl.setStartDate(medProfileMoItem.getStartDate());
		mpTrxRptItemDtl.setEndDate(medProfileMoItem.getEndDate());
		
		if ( rxItem.isCapdRxDrug() ) {
			CapdRxDrug capdRxDrug = (CapdRxDrug) rxItem;
			mpTrxRptItemDtl.setReviewDate(capdRxDrug.getCapdItemList().get(0).getReviewDate());
		} else if (rxItem.isRxDrug()) {
			RxDrug rxDrug = (RxDrug) rxItem;
			mpTrxRptItemDtl.setReviewDate(rxDrug.getRegimen().getDoseGroupList().get(0).getReviewDate());
		} else if (rxItem.isIvRxDrug()) {
			IvRxDrug ivRxDrug = (IvRxDrug)rxItem;
			mpTrxRptItemDtl.setReviewDate(ivRxDrug.getReviewDate());
		}
		
		mpTrxRptItemDtl.setSeparateLineIndicator(true);
		mpTrxRptItemDtl.setMedProfileMoItemId(medProfileMoItem.getId());
		mpTrxRptItemDtl.setMedProfileMoItemOrgItemNum(medProfileMoItem.getOrgItemNum());
    	
		// set MpTrxRptPoItemHdr list
		mpTrxRptItemDtl.setMpTrxRptPoItemHdrList( convertMpTrxRptPoItemHdrList(medProfileMoItem.getMedProfileMoItemAlertList()) );
			
		// set MpTrxRptPoItemDtl list
		List<MpTrxRptPoItemDtl> mpTrxRptPoItemDtlList = new ArrayList<MpTrxRptPoItemDtl>();
    	for (MedProfilePoItem medProfilePoItem : medProfileMoItem.getMedProfilePoItemList()) {
    		mpTrxRptPoItemDtlList.add( convertMpTrxRptItemDtl(medProfilePoItem, medProfilePoItem.getMedProfileMoItem().isManualItem()) );
    	}
    	mpTrxRptItemDtl.setMpTrxRptPoItemDtlList(mpTrxRptPoItemDtlList);
    	
    	mpTrxRptItemDtl.setPivasFlag(medProfileMoItem.getPivasFlag());
		
    	return mpTrxRptItemDtl;
    }
    
    private List<MpTrxRptPoItemHdr> convertMpTrxRptPoItemHdrList(List<MedProfileMoItemAlert> medProfileMoItemAlertList) {
    	List<MpTrxRptPoItemHdr> mpTrxRptPoItemHdrList = new ArrayList<MpTrxRptPoItemHdr>();
    	
		StringBuilder ddimOverrideReason = new StringBuilder();
		StringBuilder drcmOverrideReason = new StringBuilder();
		StringBuilder otherOverrideReason = new StringBuilder();
		
		List<MedProfileMoItemAlert> localMedProfileMoItemAlertList = new ArrayList<MedProfileMoItemAlert>();
		localMedProfileMoItemAlertList.addAll(medProfileMoItemAlertList);
		
		alertMsgManager.sortAlertEntityList(localMedProfileMoItemAlertList);
		
		Map<String,Boolean> overrideReasonMap = new HashMap<String,Boolean>();		
		Map<String,Boolean> otherReasonMap = new HashMap<String,Boolean>();		
		Set<String> ivDrugDdimPairKeySet = new HashSet<String>();
		boolean isDdiAlertExist = false; //Same ddi alert message in one MO
		boolean isDrcAlertExist = false; //Same drc alert message in one MO
		boolean isOtherAlertExist = false; //Same patient specified alert message in one MO 
		
		for (MedProfileMoItemAlert medProfileMoItemAlert : localMedProfileMoItemAlertList){
			MpTrxRptPoItemHdr mpTrxRptPoItemHdr = new MpTrxRptPoItemHdr();
			
			if (medProfileMoItemAlert.isDdiAlert()) {
				isDdiAlertExist = false;
				AlertDdim alertDdim = (AlertDdim) medProfileMoItemAlert.getAlert();
				
				if ( alertDdim.getItemNum1().equals(alertDdim.getItemNum2()) ) {
					// since orderNum1 and orderNum2 of IP DDIM must be the same, only check whether item num is different
					List<String> keyList = new ArrayList<String>();
					keyList.add(String.valueOf(alertDdim.getOrderNum1()));
					keyList.add(String.valueOf(alertDdim.getItemNum1()));
					keyList.add(String.valueOf(alertDdim.hashCode()));
					keyList.add(String.valueOf(medProfileMoItemAlert.getSortSeq()));
					String key = StringUtils.join(keyList.toArray(), "|");
					
					if (ivDrugDdimPairKeySet.contains(key)) {
						isDdiAlertExist = true;
					} else {
						ivDrugDdimPairKeySet.add(key);
					}
				}
				
				if ( ! isDdiAlertExist) {
					mpTrxRptPoItemHdr.setOverrideReasonHeader("!Drug-drug interaction MDS overriding reason:");
					
					for (String medProfileOverrideReason:medProfileMoItemAlert.getOverrideReason()){
						
						if (medProfileOverrideReason.charAt(medProfileOverrideReason.length()-1) == ASTERISK) {
							medProfileOverrideReason = (medProfileOverrideReason.substring(0, medProfileOverrideReason.length()-1));
						}
						ddimOverrideReason.append(medProfileOverrideReason).append(NEW_LINE);
					}
					
					mpTrxRptPoItemHdr.setOverrideReasonDetail(ddimOverrideReason.toString());
					ddimOverrideReason = new StringBuilder(); 
				}
				
			} else {
				if (medProfileMoItemAlert.isDrcAlert() && ! isDrcAlertExist) {
					isDrcAlertExist = true;
					mpTrxRptPoItemHdr.setOverrideReasonHeader("!Dosage range MDS overriding reason:");
						
					for (String medProfileOverrideReason:medProfileMoItemAlert.getOverrideReason()) {
						if (medProfileOverrideReason.charAt(medProfileOverrideReason.length()-1) == ASTERISK) {
							medProfileOverrideReason = (medProfileOverrideReason.substring(0, medProfileOverrideReason.length()-1));
						}
						if ( overrideReasonMap.get(medProfileOverrideReason) == null ) {
							overrideReasonMap.put(medProfileOverrideReason, Boolean.TRUE);
							drcmOverrideReason.append(medProfileOverrideReason).append(NEW_LINE);
						}
					}
					mpTrxRptPoItemHdr.setOverrideReasonDetail(drcmOverrideReason.toString());
					drcmOverrideReason = new StringBuilder(); 
					
				} else if (medProfileMoItemAlert.isPatAlert(MEDPROFILE_ALERT_HLA_ENABLED.get()) && ! isOtherAlertExist) {
					isOtherAlertExist = true;
					
					mpTrxRptPoItemHdr.setOverrideReasonHeader("!Patient specific MDS overriding reason:");
						
					for (String medProfileOverrideReason:medProfileMoItemAlert.getOverrideReason()) {
					
						if (medProfileOverrideReason.charAt(medProfileOverrideReason.length()-1) == ASTERISK) {
							medProfileOverrideReason = (medProfileOverrideReason.substring(0, medProfileOverrideReason.length()-1));
						}
						if ( otherReasonMap.get(medProfileOverrideReason) == null ) {
							otherReasonMap.put(medProfileOverrideReason, Boolean.TRUE);
							otherOverrideReason.append(medProfileOverrideReason).append(NEW_LINE);
						}
					}
				
					mpTrxRptPoItemHdr.setOverrideReasonDetail(otherOverrideReason.toString());
					otherOverrideReason = new StringBuilder(); 
				}
				
			}
			
			if ( ! StringUtils.isEmpty(mpTrxRptPoItemHdr.getOverrideReasonHeader()) && ! StringUtils.isEmpty(mpTrxRptPoItemHdr.getOverrideReasonDetail()) ) {
				mpTrxRptPoItemHdrList.add(mpTrxRptPoItemHdr);
			}
		}
		
    	return mpTrxRptPoItemHdrList;
    }
    
    private MpTrxRptPoItemDtl convertMpTrxRptItemDtl(MedProfilePoItem medProfilePoItem, boolean isManualItem) {
    	MpTrxRptPoItemDtl mpTrxRptPoItemDtl = new MpTrxRptPoItemDtl();
    	
    	// TODO: check whether it is needed to set MpTrxRptPoItemDtll.rank
    	// TODO: confirm whether it is needed to set MpTrxRptPoItemDtll.itemType
    	mpTrxRptPoItemDtl.setMedProfilePoItemId(medProfilePoItem.getId());
    	mpTrxRptPoItemDtl.setDeleted(Boolean.FALSE);
		mpTrxRptPoItemDtl.setLastDueDate(medProfilePoItem.getLastDueDate());
		
		if( StringUtils.isNotBlank(medProfilePoItem.getRemarkText()) ){
			mpTrxRptPoItemDtl.setRemark(medProfilePoItem.getRemarkText());
		}
		
		if (medProfilePoItem.getIssueQty() != null && medProfilePoItem.getBaseUnit() != null) {
			if (medProfilePoItem.getAdjQty() != null) {
				mpTrxRptPoItemDtl.setAdjQty( convertAdjQty(medProfilePoItem.getAdjQty()) );
				mpTrxRptPoItemDtl.setIssueQty(Integer.toString(medProfilePoItem.getIssueQty().intValue()));
				mpTrxRptPoItemDtl.setDispQty(convertDispQty(medProfilePoItem.getIssueQty(), medProfilePoItem.getAdjQty())+ " " +medProfilePoItem.getBaseUnit());
			} else {
				mpTrxRptPoItemDtl.setDispQty(medProfilePoItem.getIssueQty().intValue()+ " " +medProfilePoItem.getBaseUnit());
			}	
		}
    	
		if( ! medProfilePoItem.getMedProfileMoItem().getDoseSpecifiedFlag()){
			if (medProfilePoItem.getRefillDuration() != null && medProfilePoItem.getRefillDuration().doubleValue() > 0){
				DmRegimen dmRegimen = dmRegimenCacher.getRegimenByRegimenCode(medProfilePoItem.getRefillDurationUnit().getDataValue());
				Integer durationInDays = medProfilePoItem.getRefillDuration() * dmRegimen.getRegimenMultiplier();
				mpTrxRptPoItemDtl.setDuration(durationInDays + " " + DAYS);
			}
		}

		mpTrxRptPoItemDtl.setActionStatus(medProfilePoItem.getActionStatus());
		
		if( isManualItem ){
			mpTrxRptPoItemDtl.setStartDate(medProfilePoItem.getFirstDueDate());
			mpTrxRptPoItemDtl.setEndDate(medProfilePoItem.getMedProfileMoItem().getEndDate());
		}
		
		mpTrxRptPoItemDtl.setItemCode(medProfilePoItem.getItemCode());
		mpTrxRptPoItemDtl.setLabelDesc(buildDrugDesc(
													medProfilePoItem.getDrugName(), 
													medProfilePoItem.getFormLabelDesc(), 
													medProfilePoItem.getStrength(),
													medProfilePoItem.getVolumeText(),
													refTableManager.getFdnByItemCodeOrderTypeWorkstore(medProfilePoItem.getItemCode(), OrderType.InPatient, workstore)));
		
    	return mpTrxRptPoItemDtl;
    }
    

	private String convertDrugOrderXml(String drugOrderXml, String interfaceVersion) {
		if( StringUtils.isBlank( drugOrderXml ) ){
			return "";
		}

		ParsedOrderLineFormatterBase formatter =  ParsedOrderLineFormatterFactory.getParsedOrderLineFormatter(interfaceVersion);
		formatter.setHint(FormatHints.FORMAT_TYPE, FormatType.TXT);
		formatter.setHint(FormatHints.LINE_WIDTH, MAX_DRUG_ORDER_TLF_LENGTH);
		formatter.setHint(FormatHints.HIDE_STRIKE_THROUGH, false);
		formatter.setHint(FormatHints.EXCLUDE_TXT_ONLY, true);
		return formatter.format(drugOrderXml);
	}
    
	private String convertDrugOrderTlf(String drugOrderTlf) {
		if ( StringUtils.isBlank(drugOrderTlf) ) {
			return "";
		}
		
		String[] drugOrderTlfArrayList = StringUtils.splitByWholeSeparator(drugOrderTlf, TLF_BREAK_CHAR);
		
		List<StringBuilder> outputStringBuilderList = new ArrayList<StringBuilder>();
		
		StringBuilder sb = new StringBuilder();
		outputStringBuilderList.add(sb);
		
		for (int i = 0; i < drugOrderTlfArrayList.length; i++) {
			
			drugOrderTlfArrayList[i] = removeDrugOrderTlfTag(drugOrderTlfArrayList[i]);
			
			if (StringUtils.isNotBlank(drugOrderTlfArrayList[i])) {
				if (sb.length() > 0) {
					int currentLineLength;
					String currentLineString = sb.toString();
					if (currentLineString.contains(NEW_LINE)) {
						currentLineLength = currentLineString.substring(currentLineString.lastIndexOf(NEW_LINE)+1).length();
					} else {
						currentLineLength = sb.length();
					}
					
					int nextLineLength;
					if (drugOrderTlfArrayList[i].contains(NEW_LINE)) {
						nextLineLength = drugOrderTlfArrayList[i].substring(0, drugOrderTlfArrayList[i].indexOf(NEW_LINE)).length();
					} else {
						nextLineLength = drugOrderTlfArrayList[i].length();
					}

					if (currentLineLength + nextLineLength + 1 > MAX_DRUG_ORDER_TLF_LENGTH && ! drugOrderTlfArrayList[i].startsWith(NEW_LINE)) {
						sb = new StringBuilder();
						
						outputStringBuilderList.add(sb);
					} else {
						sb.append(" ");
					}
				}
				
				sb.append(drugOrderTlfArrayList[i]);
			}
		}

		StringBuilder outputStringBuilder = new StringBuilder();
		for ( StringBuilder stringBuilder : outputStringBuilderList ) {
			if (outputStringBuilder.length() > 0) {
				outputStringBuilder.append(NEW_LINE);
			}
			outputStringBuilder.append(stringBuilder.toString());
		}
		
		String result = outputStringBuilder.toString();
		
		if ( result.endsWith(NEW_LINE) ) {
			result = result.substring(0, result.length()-NEW_LINE.length());
		}
		
		return result;
	}
    
	private static String removeDrugOrderTlfTag(String drugOrderTlf) {
		return drugOrderTlf.replace("<br/>",NEW_LINE)
							.replace("</div>",NEW_LINE)
							.replace("&nbsp;"," ")
							.replace("&ndash;","-")
							.replaceAll("<[^>]*>","")
							.replace("&lt;","<")
							.replace("&gt;",">")
							.replace("&apos;","'")
							.replace("&quot","\"")
							.replace("&amp;","&");
		
	}
	
	private String convertAdjQty(BigDecimal adjQty){
		String str = null;
		
		if ( adjQty != null && adjQty.compareTo(BigDecimal.ZERO) != 0 ) {
			if ( adjQty.compareTo(BigDecimal.ZERO) > 0 ) {
				return "+"+adjQty.toString();
			} else {
				return adjQty.toString();
			}
		}
		
		return str;
	}

	private String convertDispQty(BigDecimal issueQty, BigDecimal adjQty) {
		if ( adjQty != null && issueQty != null) {
			return Integer.toString(issueQty.add(adjQty).intValue());
		}
		return null;
	}
	
	private String buildDrugDesc(String drugName, String formLabelDesc, String strength, String volumeText, String synonym){
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(WordUtils.capitalizeFully(drugName, DELIMITER));
		if (StringUtils.isNotBlank(formLabelDesc)){
			sb.append(" ").append(WordUtils.capitalizeFully(formLabelDesc, DELIMITER));
		}
		if (StringUtils.isNotBlank(strength)){
			sb.append(" ").append(WordUtils.capitalizeFully(strength, DELIMITER));
		}
		if (StringUtils.isNotBlank(volumeText)){
			sb.append(" ").append(volumeText.toLowerCase());
		}
		if (StringUtils.isNotBlank(synonym)){
			sb.append(" (").append(synonym.toUpperCase()).append(")");
		}
		
		return sb.toString();
		
	}
    
	public void generatePatientDrugProfileListRpt() {
		parameters.put("hospCode", workstore.getHospCode());
		parameters.put(ReportProvider.PNG_IMAGE_TYPE, BufferedImage.TYPE_BYTE_GRAY);
		printAgent.renderAndRedirect(new RenderJob(
				"MpTrxRpt", 
				new PrintOption(), 
				parameters, 
				patientDrugProfileRptList));
	}

	public void printPatientDrugProfileListRpt(){
		printAgent.renderAndPrint(new RenderAndPrintJob(
				"MpTrxRpt", 
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				parameters, 
				patientDrugProfileRptList));
	}
	
	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}

	@Remove
	public void destroy() {
		if ( patientDrugProfileRptList != null ) {
			patientDrugProfileRptList = null;
		}
	}

    private static class MpTrxRptDtlComparator implements Comparator<MpTrxRptDtl>, Serializable {
		private static final long serialVersionUID = 1L;

		@Override
		public int compare(MpTrxRptDtl mpTrxRptDtl1, MpTrxRptDtl mpTrxRptDtl2) {
			return new CompareToBuilder()
							.append( mpTrxRptDtl1.getBedNum(), mpTrxRptDtl2.getBedNum() )
							.append( mpTrxRptDtl1.getCaseNum(), mpTrxRptDtl2.getCaseNum() )
							.append( mpTrxRptDtl1.getHkid(), mpTrxRptDtl2.getHkid() )
							.toComparison();
		}
    }

    private static class MpTrxRptPoItemDtlComparatorForMpTrxRptItemDtlSorting implements Comparator<MpTrxRptPoItemDtl>, Serializable {
		private static final long serialVersionUID = 1L;

		@Override
		public int compare(MpTrxRptPoItemDtl mpTrxRptPoItemDtl1, MpTrxRptPoItemDtl mpTrxRptPoItemDtl2) {
			String itemCode1 = mpTrxRptPoItemDtl1.getItemCode();
			String itemCode2 = mpTrxRptPoItemDtl2.getItemCode();
			return itemCode1.compareTo(itemCode2);
		}
    }

    private static int compareMpTrxItemDtlByItemNum(Integer item1, Integer item2){
    	if( item1 >= 10000 && item2 >= 10000 ){
    		return 0;
    	}else if(item1 < 10000 && item2 < 10000 ){
    		return 0;
    	}
    	return item1.compareTo(item2);
    }
    
    private static class MpTrxRptItemDtlComparator implements Comparator<MpTrxRptItemDtl>, Serializable {
		private static final long serialVersionUID = 1L;
		
		@Override
		public int compare(MpTrxRptItemDtl mpTrxRptItemDtl1, MpTrxRptItemDtl mpTrxRptItemDtl2) {
			int compareResult = 0;
			
			compareResult = compareMpTrxItemDtlByItemNum(mpTrxRptItemDtl1.getMedProfileMoItemOrgItemNum(), mpTrxRptItemDtl2.getMedProfileMoItemOrgItemNum());
			
			if (compareResult == 0) {
				compareResult = getItemCode(mpTrxRptItemDtl1.getMpTrxRptPoItemDtlList()).compareTo(
						getItemCode(mpTrxRptItemDtl2.getMpTrxRptPoItemDtlList()));
			}
			
			if (compareResult == 0) {
				Long medProfileMoItemId1 = mpTrxRptItemDtl1.getMedProfileMoItemId();
				Long medProfileMoItemId2 = mpTrxRptItemDtl2.getMedProfileMoItemId();
				compareResult = medProfileMoItemId1.compareTo(medProfileMoItemId2) ;
			}
			
			return compareResult;
		}
		
		private String getItemCode(List<MpTrxRptPoItemDtl> mpTrxRptPoItemDtlList){
			StringBuilder sb = new StringBuilder();
			for (MpTrxRptPoItemDtl mpTrxRptPoItemDtl : mpTrxRptPoItemDtlList){
				sb.append(mpTrxRptPoItemDtl.getItemCode());
			}
			return sb.toString();
		}
    }

    private static class MpTrxRptPoItemDtlComparator implements Comparator<MpTrxRptPoItemDtl>, Serializable {
		private static final long serialVersionUID = 1L;

		@Override
		public int compare(MpTrxRptPoItemDtl mpTrxRptPoItemDtl1, MpTrxRptPoItemDtl mpTrxRptPoItemDtl2) {
			int compareResult = 0;
			
			if (compareResult == 0) {
				Long medProfilePoItemId1 = mpTrxRptPoItemDtl1.getMedProfilePoItemId();
				Long medProfilePoItemId2 = mpTrxRptPoItemDtl2.getMedProfilePoItemId();
				compareResult = medProfilePoItemId1.compareTo(medProfilePoItemId2) ;
			}
			
			return compareResult;
		}
    }    
}
