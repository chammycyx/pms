package hk.org.ha.model.pms.biz;
import javax.ejb.Local;

import org.jboss.seam.annotations.async.Asynchronous;
import org.jboss.seam.annotations.async.Duration;

@Local
public interface PmsStartupLocal {

	@Asynchronous
	void startup(@Duration Long delayDuration);

}
