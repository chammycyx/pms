package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class RefillItemRptSummary implements Serializable {

	private static final long serialVersionUID = 1L;

	private int beingProcessedRefillCount;
	
	private int completedRefillCount;
	
	private int forceCompletedRefillCount;
	
	private int notYetDispRefillCount;
	
	private int suspendRefillCount;
	
	private int deletedRefillCount;
	
	private int beingProcessedItemCount;
	
	private int completedItemCount;
	
	private int forceCompletedItemCount;
	
	private int notYetDispItemCount;
	
	private int suspendItemCount;
	
	private int deletedItemCount;
	
	private double beingProcessedDrugCost;
	
	private double completedDrugCost;
	
	private double forceCompletedDrugCost;
	
	private double notYetDispDrugCost;
	
	private double suspendDrugCost;
	
	private double deletedDrugCost;
	
	private String totalRefillQty;


	public int getBeingProcessedRefillCount() {
		return beingProcessedRefillCount;
	}

	public void setBeingProcessedRefillCount(int beingProcessedRefillCount) {
		this.beingProcessedRefillCount = beingProcessedRefillCount;
	}

	public int getCompletedRefillCount() {
		return completedRefillCount;
	}

	public void setCompletedRefillCount(int completedRefillCount) {
		this.completedRefillCount = completedRefillCount;
	}

	public int getForceCompletedRefillCount() {
		return forceCompletedRefillCount;
	}

	public void setForceCompletedRefillCount(int forceCompletedRefillCount) {
		this.forceCompletedRefillCount = forceCompletedRefillCount;
	}

	public int getNotYetDispRefillCount() {
		return notYetDispRefillCount;
	}

	public void setNotYetDispRefillCount(int notYetDispRefillCount) {
		this.notYetDispRefillCount = notYetDispRefillCount;
	}

	public int getSuspendRefillCount() {
		return suspendRefillCount;
	}

	public void setSuspendRefillCount(int suspendRefillCount) {
		this.suspendRefillCount = suspendRefillCount;
	}

	public int getDeletedRefillCount() {
		return deletedRefillCount;
	}

	public void setDeletedRefillCount(int deletedRefillCount) {
		this.deletedRefillCount = deletedRefillCount;
	}

	public int getBeingProcessedItemCount() {
		return beingProcessedItemCount;
	}

	public void setBeingProcessedItemCount(int beingProcessedItemCount) {
		this.beingProcessedItemCount = beingProcessedItemCount;
	}

	public int getCompletedItemCount() {
		return completedItemCount;
	}

	public void setCompletedItemCount(int completedItemCount) {
		this.completedItemCount = completedItemCount;
	}

	public int getForceCompletedItemCount() {
		return forceCompletedItemCount;
	}

	public void setForceCompletedItemCount(int forceCompletedItemCount) {
		this.forceCompletedItemCount = forceCompletedItemCount;
	}

	public int getNotYetDispItemCount() {
		return notYetDispItemCount;
	}

	public void setNotYetDispItemCount(int notYetDispItemCount) {
		this.notYetDispItemCount = notYetDispItemCount;
	}

	public int getSuspendItemCount() {
		return suspendItemCount;
	}

	public void setSuspendItemCount(int suspendItemCount) {
		this.suspendItemCount = suspendItemCount;
	}

	public int getDeletedItemCount() {
		return deletedItemCount;
	}

	public void setDeletedItemCount(int deletedItemCount) {
		this.deletedItemCount = deletedItemCount;
	}

	public double getBeingProcessedDrugCost() {
		return beingProcessedDrugCost;
	}

	public void setBeingProcessedDrugCost(double beingProcessedDrugCost) {
		this.beingProcessedDrugCost = beingProcessedDrugCost;
	}

	public double getCompletedDrugCost() {
		return completedDrugCost;
	}

	public void setCompletedDrugCost(double completedDrugCost) {
		this.completedDrugCost = completedDrugCost;
	}

	public double getForceCompletedDrugCost() {
		return forceCompletedDrugCost;
	}

	public void setForceCompletedDrugCost(double forceCompletedDrugCost) {
		this.forceCompletedDrugCost = forceCompletedDrugCost;
	}

	public double getNotYetDispDrugCost() {
		return notYetDispDrugCost;
	}

	public void setNotYetDispDrugCost(double notYetDispDrugCost) {
		this.notYetDispDrugCost = notYetDispDrugCost;
	}

	public double getSuspendDrugCost() {
		return suspendDrugCost;
	}

	public void setSuspendDrugCost(double suspendDrugCost) {
		this.suspendDrugCost = suspendDrugCost;
	}

	public double getDeletedDrugCost() {
		return deletedDrugCost;
	}

	public void setDeletedDrugCost(double deletedDrugCost) {
		this.deletedDrugCost = deletedDrugCost;
	}

	public void setTotalRefillQty(String totalRefillQty) {
		this.totalRefillQty = totalRefillQty;
	}

	public String getTotalRefillQty() {
		return totalRefillQty;
	}

}