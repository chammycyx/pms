package hk.org.ha.model.pms.biz.alert.mds;

import static hk.org.ha.model.pms.prop.Prop.*;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.AlertEntity;
import hk.org.ha.model.pms.persistence.disp.MedItemInf;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;
import hk.org.ha.model.pms.vo.alert.mds.MedProfileAlertMsg;
import hk.org.ha.model.pms.vo.rx.IvAdditive;
import hk.org.ha.model.pms.vo.rx.IvRxDrug;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.contexts.Contexts;

@Stateful
@Scope(ScopeType.SESSION)
@Name("alertMsgService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class AlertMsgServiceBean implements AlertMsgServiceLocal {
	
	@In(required = false)
	private MedOrderItem drugSearchMedOrderItem;
	
	@In
	private AlertMsgManagerLocal alertMsgManager;
	
	public MedProfileAlertMsg retrieveAlertMsgByItemNum(Integer itemNum) {
		return alertMsgManager.retrieveMedProfileAlertMsg(getPharmOrder().getMedOrder().getMedOrderItemByItemNum(itemNum));
	}
	
	public AlertMsg retrieveAlertMsgForDrugSearch() {
		return alertMsgManager.retrieveAlertMsg(drugSearchMedOrderItem);
	}
	
	public MedProfileAlertMsg retrieveAlertMsgByMedOrderItem(MedOrderItem medOrderItem) {
		return alertMsgManager.retrieveMedProfileAlertMsg(medOrderItem);
	}

	public MedProfileAlertMsg retrieveMedProfileAlertMsg(MedItemInf medItem) {

		for (AlertEntity alertEntity : medItem.getAlertList()) {
			alertEntity.getMedItem();
		}
		
		List<AlertMsg> alertMsgList;
		if (medItem.getRxItem().isIvRxDrug()) {
			alertMsgList = constructAlertMsgForIvRxDrug(medItem);
		}
		else {
			alertMsgList = Arrays.asList(constructAlertMsgForNonIvRxDrug(medItem));
		}
		
		MedProfileAlertMsg medProfileAlertMsg = new MedProfileAlertMsg();
		medProfileAlertMsg.setOrderDesc(medItem.getMdsOrderDesc());
		medProfileAlertMsg.setIvRxDrug(medItem.getRxItem().isIvRxDrug());
		medProfileAlertMsg.setAlertMsgList(alertMsgList);
		
		return medProfileAlertMsg;
	}
	
	public AlertMsg retrieveMpAlertMsg(MedProfileMoItem medProfileMoItem) {
		return alertMsgManager.retrieveMpAlertMsg(medProfileMoItem);
	}

	private AlertMsg constructAlertMsgForNonIvRxDrug(MedItemInf medItem) {
		List<AlertEntity> patAlertList = new ArrayList<AlertEntity>();
		for (AlertEntity alertEntity : medItem.getPatAlertList(MEDPROFILE_ALERT_HLA_ENABLED.get())) {
			if (!alertEntity.getOverrideReason().isEmpty()) {
				patAlertList.add(alertEntity);
			}
		}
		alertMsgManager.sortAlertEntityList(patAlertList);
		
		return new AlertMsg(
				medItem.getItemNum(),
				medItem.getMdsOrderDesc(),
				medItem.getDisplayNameSaltForm(),
				patAlertList,
				new ArrayList<AlertEntity>(medItem.getDdiAlertList()),
				new ArrayList<AlertEntity>(medItem.getDrcAlertList())
		);
	}
	
	private List<AlertMsg> constructAlertMsgForIvRxDrug(MedItemInf medItem) {
		
		List<AlertMsg> alertMsgList = new ArrayList<AlertMsg>();
		IvRxDrug ivRxDrug = (IvRxDrug) medItem.getRxItem();
		
		List<? extends AlertEntity> itemPatAlertList = medItem.getPatAlertList(MEDPROFILE_ALERT_HLA_ENABLED.get());
		List<? extends AlertEntity> itemDdiAlertList = medItem.getDdiAlertList();
		List<? extends AlertEntity> itemDrcAlertList = medItem.getDrcAlertList();
		
		alertMsgManager.sortAlertEntityList(itemPatAlertList);
		
		for (IvAdditive ivAdditive : ivRxDrug.getIvAdditiveList()) {
			
			List<AlertEntity> additivePatAlertList = getAlertListByAdditiveNum(itemPatAlertList, ivAdditive.getAdditiveNum());
			List<AlertEntity> additiveDdiAlertList = getAlertListByAdditiveNum(itemDdiAlertList, ivAdditive.getAdditiveNum());
			List<AlertEntity> additiveDrcAlertList = getAlertListByAdditiveNum(itemDrcAlertList, ivAdditive.getAdditiveNum());
			
			if (additivePatAlertList.isEmpty() && additiveDdiAlertList.isEmpty() && additiveDrcAlertList.isEmpty()) {
				continue;
			}
			
			alertMsgList.add(new AlertMsg(
					medItem.getItemNum(),
					ivAdditive.getMdsOrderDesc(),
					ivAdditive.getDisplayNameSaltForm(),
					new ArrayList<AlertEntity>(additivePatAlertList),
					new ArrayList<AlertEntity>(additiveDdiAlertList),
					new ArrayList<AlertEntity>(additiveDrcAlertList)
			));
		}

		return alertMsgList;
	}

	private List<AlertEntity> getAlertListByAdditiveNum(List<? extends AlertEntity> inList, Integer additiveNum) {
		List<AlertEntity> outList = new ArrayList<AlertEntity>();
		for (AlertEntity alert : inList) {
			if (alert.getAdditiveNum() != null && additiveNum != null && alert.getAdditiveNum().equals(additiveNum)) {
				outList.add(alert);
			}
		}
		return outList;
	}
	
	private PharmOrder getPharmOrder() {
		return (PharmOrder) Contexts.getSessionContext().get("pharmOrder");
	}
	
	@Remove
	public void destroy() {
	}	
}
