package hk.org.ha.model.pms.biz.onestop;

import static hk.org.ha.model.pms.prop.Prop.CDDH_LEGACY_PERSISTENCE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.order.DispOrderManagerLocal;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderDayEndStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderAdminStatus;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import java.util.Date;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.joda.time.DateTime;

@AutoCreate
@Stateless
@Name("uncollectPrescManager")
@MeasureCalls
public class UncollectPrescManagerBean implements UncollectPrescManagerLocal {

	@PersistenceContext
	private EntityManager em; 
	
	@In
	private DispOrderManagerLocal dispOrderManager;
	
	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
	
	@In
	private AuditLogger auditLogger;
	
	@In
	private UamInfo uamInfo;
	
	@In
	private Workstation workstation;
	
	@In(required=false)
	private String reAuthUserId;
	
	private final static String ONE_STOP_ENTRY = "oneStopEntry";
	
	public void uncollectPrescription(DispOrder dispOrder, String suspendCode, String caller) {
		dispOrder.loadChild();
		dispOrder.getPharmOrder().getMedOrder().setCmsUpdateDate(new DateTime().toDate());
		dispOrder.getPharmOrder().setAdminStatus(PharmOrderAdminStatus.Revoke);

		markRevokeFlagForUncollectMOEOrder(dispOrder, dispOrder.getPharmOrder().getAdminStatus());
		
		if (dispOrder.getDayEndStatus() == DispOrderDayEndStatus.None && ! dispOrder.getBatchProcessingFlag()) {
			dispOrder.setAdminStatus(DispOrderAdminStatus.Suspended);
			dispOrder.setSuspendCode(suspendCode);
		}
		
		forceUpdateDispOrder(dispOrder);
		if ( ONE_STOP_ENTRY.equals(caller) ) {
			writeAuditLog("#0520", dispOrder.getId(), suspendCode);
		}
		em.flush();
		dispOrder.getPharmOrder().clearDmInfo();
		corpPmsServiceProxy.updateAdminStatus(dispOrder, dispOrder.getAdminStatus(), 
				PharmOrderAdminStatus.Revoke, CDDH_LEGACY_PERSISTENCE_ENABLED.get(false), CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED.get(false), null);
	}
	
	public DispOrder markRevokeFlagForUncollectMOEOrder(DispOrder dispOrder, PharmOrderAdminStatus adminStatus){
		if ( (dispOrder.getRevokeFlag() == null || dispOrder.getRevokeFlag() == Boolean.FALSE) && adminStatus == PharmOrderAdminStatus.Revoke ) {
			dispOrder.setRevokeFlag(Boolean.TRUE);
			dispOrder.setRevokeDate(new DateTime().toDate());
			dispOrder.setRevokeUser(uamInfo.getUserId());
		} else if ( dispOrder.getRevokeFlag() == Boolean.TRUE ) {
			dispOrder.setRevokeFlag(Boolean.FALSE);
			dispOrder.setRevokeDate(null);
			dispOrder.setRevokeUser(null);
		}
		
		return dispOrder;
	}
	
	public DispOrder forceUpdateDispOrder(DispOrder dispOrder)
	{
		dispOrder.setUpdateDate(new DateTime().toDate());
		dispOrder.setUpdateUser(uamInfo.getUserId());
		return dispOrder;
	}
	
	public void writeAuditLog(String msgCode, Long dispOrderId, String suspendCode)
	{
		if (msgCode.equals("#0520"))
		{
			auditLogger.log(msgCode, (reAuthUserId==null?uamInfo.getUserId():reAuthUserId), 
					workstation.getHostName(), dispOrderId, suspendCode);
		} 
		else
		{
			auditLogger.log(msgCode, (reAuthUserId==null?uamInfo.getUserId():reAuthUserId), 
					workstation.getHostName(), dispOrderId);
		}
		
		if (reAuthUserId != null)
		{
			reAuthUserId = null;
		}
	}
	
	public void reverseUncollectPrescription(DispOrder dispOrder, String caller, boolean updateCorp, boolean updateLargeLayoutFlag) {
		dispOrder.getPharmOrder().getMedOrder().setCmsUpdateDate(new DateTime().toDate());
		dispOrder.getPharmOrder().setAdminStatus(PharmOrderAdminStatus.Normal);
		dispOrder = markRevokeFlagForUncollectMOEOrder(dispOrder, dispOrder.getPharmOrder().getAdminStatus());
		dispOrder.setAdminStatus(DispOrderAdminStatus.Normal);
		if (dispOrder.getStatus() == DispOrderStatus.Issued && dispOrder.getDayEndStatus() == DispOrderDayEndStatus.None && ! dispOrder.getBatchProcessingFlag()) {
			dispOrder.setIssueDate(new Date());
		}
		dispOrder = forceUpdateDispOrder(dispOrder);

		Map<Long, Boolean> largeLayoutFlagMap = null;
		if (updateLargeLayoutFlag && dispOrder.getDayEndStatus() == DispOrderDayEndStatus.None && ! dispOrder.getBatchProcessingFlag()) {
			// set large layout flag for label reprint and CDDH label preview (for PMS mode)
			// (note: since the flag is needed to sync to CORP for CDDH label preview, it is set here instead of in print disp order function)
			// (note: follow condition of print disp order)
			largeLayoutFlagMap = dispOrderManager.updateDispOrderLargeLayoutFlag(dispOrder);
		}
		
		if ( ONE_STOP_ENTRY.equals(caller) ) {
			writeAuditLog("#0521", dispOrder.getId(), null);
		}
		
		em.flush();
		dispOrder.getPharmOrder().clearDmInfo();
		if ( updateCorp ) {
			corpPmsServiceProxy.updateAdminStatus(dispOrder, DispOrderAdminStatus.Normal, PharmOrderAdminStatus.Normal, 
													CDDH_LEGACY_PERSISTENCE_ENABLED.get(false), CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED.get(false), largeLayoutFlagMap);
		}
		dispOrder.getPharmOrder().loadDmInfo();
	}	
}
