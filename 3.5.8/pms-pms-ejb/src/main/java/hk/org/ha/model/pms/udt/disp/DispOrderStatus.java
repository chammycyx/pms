package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

import java.util.Arrays;
import java.util.List;

public enum DispOrderStatus implements StringValuedEnum {

	Vetted("V", "Vetted", 1),
	Picking("PI", "Vetted", 2), // Picking status display Vetted
	Picked("P", "Picked", 3),
	Assembling("AI", "Picked", 4), // Assembling status display Picked
	Assembled("A", "Assembled", 5),
	Checked("C", "Checked", 6),
	Issued("I", "Issued", 7),	
	Deleted("D", "Deleted"),
	SysDeleted("X", "SysDeleted");
	
    public static final List<DispOrderStatus> Deleted_SysDeleted = Arrays.asList(Deleted,SysDeleted);
    public static final List<DispOrderStatus> Deleted_SysDeleted_Issued = Arrays.asList(Deleted,SysDeleted,Issued);
    
    public static final List<DispOrderStatus> Not_Deleted_SysDeleted = Arrays.asList(
    		StringValuedEnumReflect.getValuesExclude(DispOrderStatus.class,Deleted,SysDeleted));

    private final String dataValue;
    private final String displayValue;
    private final Integer seqNum;

    DispOrderStatus(final String dataValue, final String displayValue, final Integer seqNum){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
        this.seqNum = seqNum;
    }

    DispOrderStatus(final String dataValue, final String displayValue){
    	this(dataValue, displayValue, null);
    }
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }  
    
    public Integer getSeqNum() {
    	return this.seqNum;
    }
    
    public boolean isLaterThan(DispOrderStatus status) {
    	return seqNum != null && status.getSeqNum() != null && seqNum.compareTo(status.getSeqNum()) > 0;
    }
    
    public boolean isLaterThanEqualTo(DispOrderStatus status) {
    	return isLaterThan(status) || (status == this);
    }
    
    public static DispOrderStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DispOrderStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<DispOrderStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<DispOrderStatus> getEnumClass() {
    		return DispOrderStatus.class;
    	}
    }
}
