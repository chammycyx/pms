package hk.org.ha.model.pms.vo.printing;

import hk.org.ha.model.pms.persistence.reftable.ItemLocation;
import hk.org.ha.model.pms.persistence.reftable.Workstation;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class PrintLabelDetail extends PrinterSelect implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private ItemLocation itemLocation;
	
	private Workstation workstation;
	
	private Map<Long, Long> dispOrderItemIdMap;
	
	private Boolean printFastQueueLabelPrinterFlag;
	
	public ItemLocation getItemLocation() {
		return itemLocation;  
	} 

	public void setItemLocation(ItemLocation itemLocation) {
		this.itemLocation = itemLocation;
	}

	public Workstation getWorkstation() {
		return workstation;
	}

	public void setWorkstation(Workstation workstation) {
		this.workstation = workstation;
	}

	public void setDispOrderItemIdMap(Map<Long, Long> dispOrderItemIdMap) {
		this.dispOrderItemIdMap = dispOrderItemIdMap;
	}

	public Map<Long, Long> getDispOrderItemIdMap() {
		if( dispOrderItemIdMap == null ) {
			dispOrderItemIdMap = new HashMap<Long, Long>();
		}
		return dispOrderItemIdMap;
	}

	public Boolean getPrintFastQueueLabelPrinterFlag() {
		return printFastQueueLabelPrinterFlag;
	}

	public void setPrintFastQueueLabelPrinterFlag(
			Boolean printFastQueueLabelPrinterFlag) {
		this.printFastQueueLabelPrinterFlag = printFastQueueLabelPrinterFlag;
	}
}