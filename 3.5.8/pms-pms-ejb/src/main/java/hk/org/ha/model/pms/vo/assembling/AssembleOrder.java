package hk.org.ha.model.pms.vo.assembling;

import hk.org.ha.model.pms.udt.disp.MedOrderDocType;

import java.io.Serializable;
import java.util.Date;

public class AssembleOrder implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Date ticketDate;
	
	private String ticketNum;
	
	private Boolean urgentFlag;
	
	private MedOrderDocType docType;
	
	public AssembleOrder (Date ticketDate, String ticketNum, Boolean urgentFlag, MedOrderDocType docType) {
		this.ticketDate = ticketDate;
		this.ticketNum = ticketNum;
		this.urgentFlag = urgentFlag;
		this.docType = docType;
	}
	
	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public void setUrgentFlag(Boolean urgentFlag) {
		this.urgentFlag = urgentFlag;
	}

	public Boolean getUrgentFlag() {
		return urgentFlag;
	}

	public void setDocType(MedOrderDocType docType) {
		this.docType = docType;
	}

	public MedOrderDocType getDocType() {
		return docType;
	}

	public void setTicketDate(Date ticketDate) {
		this.ticketDate = ticketDate;
	}

	public Date getTicketDate() {
		return ticketDate;
	}

}