package hk.org.ha.model.pms.biz.onestop;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.udt.disp.RemarkStatus;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.joda.time.DateTime;

@Stateful
@Scope(ScopeType.SESSION)
@Name("reversePharmacyRemarkService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ReversePharmacyRemarkServiceBean implements ReversePharmacyRemarkServiceLocal {

	@PersistenceContext
	private EntityManager em;

	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
		
	public void reversePharmacyRemark(Long id, Long medOrderVersion) {
		
		MedOrder medOrder = em.find(MedOrder.class, id);
		
		if (!medOrder.getVersion().equals(medOrderVersion)) {
			throw new OptimisticLockException();
		}
		
		medOrder.setCmsUpdateDate(new DateTime().toDate());
		medOrder.setRemarkStatus(RemarkStatus.None);
		medOrder.setRefPharmOrderId(null);
		medOrder.setRemarkText(null);
		medOrder.setRemarkCreateUser(null);
		medOrder.setRemarkCreateDate(null);
		em.flush();
		
		medOrder.clearDmInfo();
		corpPmsServiceProxy.reversePharmacyRemark(medOrder);
	}
		
	@Remove
	public void destroy(){
		
	}
}
