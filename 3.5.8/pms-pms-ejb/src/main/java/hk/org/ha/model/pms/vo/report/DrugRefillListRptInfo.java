package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class DrugRefillListRptInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Date dueDate;
	private Integer refillGenerateDay;
	private String caseNum;
	private boolean includeDDnWardStockFlag;	
	private List<String> wardCodeList;

	
	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Integer getRefillGenerateDay() {
		return refillGenerateDay;
	}

	public void setRefillGenerateDay(Integer refillGenerateDay) {
		this.refillGenerateDay = refillGenerateDay;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public boolean getIncludeDDnWardStockFlag() {
		return includeDDnWardStockFlag;
	}
	
	public void setIncludeDDnWardStockFlag(boolean includeDDnWardStockFlag) {
		this.includeDDnWardStockFlag = includeDDnWardStockFlag;
	}
	
	public List<String> getWardCodeList() {
		return wardCodeList;
	}

	public void setWardCodeList(List<String> wardCodeList) {
		this.wardCodeList = wardCodeList;
	}

}
