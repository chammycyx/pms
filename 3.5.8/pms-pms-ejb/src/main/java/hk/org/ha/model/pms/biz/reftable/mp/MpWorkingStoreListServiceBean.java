package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.support.PropManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.WorkstoreProp;
import hk.org.ha.model.pms.udt.DataType;
import hk.org.ha.model.pms.udt.YesNoFlag;
import hk.org.ha.model.pms.udt.reftable.MaintScreen;
import hk.org.ha.model.pms.vo.support.PropInfoItem;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("mpWorkingStoreListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MpWorkingStoreListServiceBean implements MpWorkingStoreListServiceLocal {

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<PropInfoItem> mpWorkingStorePropList;
		
	@In
	private Workstore workstore;
	
	@In
	private PropManagerLocal propManager;
	
	@In 
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
			
	@SuppressWarnings("unchecked")
	public void retrieveMpWorkStorePropList() {
		mpWorkingStorePropList = new ArrayList<PropInfoItem>();
		convertWorkingStorePropListToWorkingStorePropInfoList(em.createQuery(
					"select o from WorkstoreProp o " + // 20121112 index check : WorkstoreProp.workstore : FK_WORKSTORE_PROP_01
					"where o.workstore = :workstore " +
					"and o.prop.maintScreen = :type ")
					.setParameter("workstore", workstore)
					.setParameter("type", MaintScreen.IPWorkstore)
					.getResultList());
		
	}	
	
	private void convertWorkingStorePropListToWorkingStorePropInfoList(List<WorkstoreProp> WorkingStorePropList) {		
		for ( WorkstoreProp workingStoreProp : WorkingStorePropList ) {
			PropInfoItem workingStorePropInfo = new PropInfoItem();
			mpWorkingStorePropList.add(workingStorePropInfo);
			workingStorePropInfo.setProp(workingStoreProp.getProp());
			workingStorePropInfo.setSortSeq(workingStoreProp.getSortSeq());
			workingStorePropInfo.setSupportMaintFlag(Boolean.FALSE);
			
			workingStorePropInfo.setPropItemId(workingStoreProp.getId());
			workingStorePropInfo.setValue(workingStoreProp.getValue());
			if ( workingStorePropInfo.getProp().getType() == DataType.BooleanType ) {
				workingStorePropInfo.setDisplayValue(getBooleanDisplayValue(workingStoreProp.getValue()));
			} else {
				workingStorePropInfo.setDisplayValue(workingStoreProp.getValue());
			}
		}
	}
	
	public void updateMpWorkingStorePropList(List<PropInfoItem> mpWorkingStorePropList) {
		List<WorkstoreProp> workstorePropList = new ArrayList<WorkstoreProp>();
		List<WorkstoreProp> syncWorkstorePropList = new ArrayList<WorkstoreProp>();
		
		for ( PropInfoItem propInfoItem : mpWorkingStorePropList ) {
			WorkstoreProp workstoreProp = em.find(WorkstoreProp.class, propInfoItem.getPropItemId());
			workstoreProp.setValue(propInfoItem.getValue());
			workstorePropList.add(workstoreProp);
			if ( workstoreProp.getProp().getSyncFlag() ) {
				syncWorkstorePropList.add(workstoreProp);
			}
		}
		
		if ( !workstorePropList.isEmpty() ) {
			propManager.updateWorkstorePropList(workstorePropList);
		}
		
		if ( !syncWorkstorePropList.isEmpty() ) {
			corpPmsServiceProxy.updateWorkstoreProp(workstorePropList, false);
		}
		retrieveMpWorkStorePropList();
	}
	
	private String getBooleanDisplayValue(String dataValue) {
		return YesNoFlag.dataValueOf(dataValue).getDisplayValue();
	}
	
	@Remove
	public void destroy() {
		if (mpWorkingStorePropList != null) {
			mpWorkingStorePropList = null;
		}
	}
}