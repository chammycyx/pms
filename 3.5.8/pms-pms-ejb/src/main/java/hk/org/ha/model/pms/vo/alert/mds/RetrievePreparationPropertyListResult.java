package hk.org.ha.model.pms.vo.alert.mds;

import hk.org.ha.model.pms.dms.vo.PreparationProperty;
import hk.org.ha.model.pms.vo.rx.RxItem;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class RetrievePreparationPropertyListResult implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<PreparationProperty> preparationPropertyList;
	
	private RxItem rxItem;

	public List<PreparationProperty> getPreparationPropertyList() {
		return preparationPropertyList;
	}

	public void setPreparationPropertyList(List<PreparationProperty> preparationPropertyList) {
		this.preparationPropertyList = preparationPropertyList;
	}

	public RxItem getRxItem() {
		return rxItem;
	}

	public void setRxItem(RxItem rxItem) {
		this.rxItem = rxItem;
	}
}
