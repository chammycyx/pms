package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.cache.DmSpecialtyMoMappingCacher;
import hk.org.ha.model.pms.dms.persistence.DmSpecialtyMoMapping;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("dmSpecailtyMoMappingListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DmSpecialtyMoMappingListServiceBean implements DmSpecialtyMoMappingListServiceLocal {

	@Logger
	private Log logger;

	@In
	private DmSpecialtyMoMappingCacher dmSpecialtyMoMappingCacher;

	@Out(required = false)
	private List<DmSpecialtyMoMapping> dmSpecialtyMoMappingList;	
	
	public void retrieveDmSpecialtyMoMappingList(String prefixSpecialtyCode) 
	{
		logger.debug("retrieveDmSpecialtyMoMappingList");
				
		if(StringUtils.isBlank(prefixSpecialtyCode)){
			dmSpecialtyMoMappingList = dmSpecialtyMoMappingCacher.getDmSpecialtyMoMappingList();
		}else{
			dmSpecialtyMoMappingList = dmSpecialtyMoMappingCacher.getDmSpecialtyMoMappingListBySpecialtyCode(prefixSpecialtyCode);
		}
	}

	@Remove
	public void destroy() 
	{
		if (dmSpecialtyMoMappingList != null) {
			dmSpecialtyMoMappingList = null;
		}
	}
	
}
