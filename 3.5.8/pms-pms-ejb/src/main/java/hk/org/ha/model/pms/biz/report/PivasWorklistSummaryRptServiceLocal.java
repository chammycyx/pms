package hk.org.ha.model.pms.biz.report;


import hk.org.ha.model.pms.persistence.pivas.PivasWorklist;

import java.util.List;
import javax.ejb.Local;

@Local
public interface PivasWorklistSummaryRptServiceLocal {
	
	void printPivasWorklistSummaryRpt(List<PivasWorklist> pivasWorklistList);
	
	void destroy();
	
}
