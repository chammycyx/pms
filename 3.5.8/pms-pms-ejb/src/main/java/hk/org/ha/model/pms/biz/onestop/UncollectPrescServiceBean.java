package hk.org.ha.model.pms.biz.onestop;

import static hk.org.ha.model.pms.prop.Prop.CDDH_LEGACY_PERSISTENCE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.ONESTOP_SUSPENDORDERCOUNT_DURATION_LIMIT;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.charging.DrugChargeClearanceManagerLocal;
import hk.org.ha.model.pms.biz.order.DispOrderManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.udt.charging.ClearanceStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderDayEndStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.InvoiceDocType;
import hk.org.ha.model.pms.udt.disp.PharmOrderAdminStatus;
import hk.org.ha.model.pms.vo.charging.FcsPaymentStatus;
import hk.org.ha.model.pms.vo.charging.SfiForceProceedInfo;
import hk.org.ha.model.pms.vo.onestop.MoStatusCount;
import hk.org.ha.model.pms.vo.security.PropMap;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import java.util.Date;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.joda.time.DateTime;

@Stateful
@Scope(ScopeType.SESSION)
@Name("uncollectPrescService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class UncollectPrescServiceBean implements UncollectPrescServiceLocal{

	@PersistenceContext
	private EntityManager em; 
	
	private FcsPaymentStatus fcsPaymentStatus;
	
	private Boolean sfiForceProceed;
	
	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
	
	@In
	private OneStopCountManagerLocal oneStopCountManager;
	
	@In(required = false) 
	@Out(required = false)
	private SfiForceProceedInfo sfiForceProceedInfo;
	
	@In
	private DispOrderManagerLocal dispOrderManager;
	
	@In
	private OneStopOrderStatusManagerLocal oneStopOrderStatusManager;
	
	@In
	private UncollectPrescManagerLocal uncollectPrescManager;
	
	@In
	private DrugChargeClearanceManagerLocal drugChargeClearanceManager;
	
	@In
	private Workstore workstore;

	@In
	private PropMap propMap;
	
	@Out(required = false)
	private MoStatusCount moStatusCount;
	
	private String statusErrorMsg;
	
	private final static String ONE_STOP_ENTRY = "oneStopEntry";
	
	public String checkUncollectOrderStatus(Long dispOrderId, Long dispOrderVersion)
	{
		retrieveUnvetAndSuspendCount();
		statusErrorMsg = oneStopOrderStatusManager.verifySuspendAllowModifyUncollectStatus(dispOrderId, dispOrderVersion);
		return statusErrorMsg;
	}
	
	public void uncollectPrescription(Long dispOrderId, Long dispOrderVersion, String suspendCode) {
		
		if (checkUncollectOrderStatus(dispOrderId, dispOrderVersion) == null)
		{
			DispOrder dispOrder = em.find(DispOrder.class, dispOrderId);
			uncollectPrescManager.uncollectPrescription(dispOrder, suspendCode, ONE_STOP_ENTRY);
		}
		
		retrieveUnvetAndSuspendCount();
	}
	
	public void reverseUncollectPrescription(Long dispOrderId, Long dispOrderVersion) {
		
		if (checkUncollectOrderStatus(dispOrderId, dispOrderVersion) == null)
		{
			DispOrder dispOrder = em.find(DispOrder.class, dispOrderId);
			dispOrder.loadChild();
			
			fcsPaymentStatus = null;
			sfiForceProceed = false;
				
			if (dispOrder.getSfiFlag() && dispOrder.getDayEndStatus() == DispOrderDayEndStatus.None && ! dispOrder.getBatchProcessingFlag()) {
				for (Invoice invoice : dispOrder.getInvoiceList()) {
					if (invoice.getDocType().equals(InvoiceDocType.Sfi)){
						fcsPaymentStatus = drugChargeClearanceManager.checkSfiDrugClearanceByInvoice(invoice);
						if (fcsPaymentStatus.getClearanceStatus() == ClearanceStatus.PaymentClear || invoice.getForceProceedFlag()) {
							dispOrder.getPharmOrder().getMedOrder().setCmsUpdateDate(new DateTime().toDate());
							dispOrder.getPharmOrder().setAdminStatus(PharmOrderAdminStatus.Normal);
							uncollectPrescManager.markRevokeFlagForUncollectMOEOrder(dispOrder, dispOrder.getPharmOrder().getAdminStatus());
							dispOrder.setAdminStatus(DispOrderAdminStatus.Normal);
							if (dispOrder.getStatus() == DispOrderStatus.Issued) {
								dispOrder.setIssueDate(new Date());
							}
							uncollectPrescManager.forceUpdateDispOrder(dispOrder);
							
							// set large layout flag for label reprint and CDDH label preview (for PMS mode)
							// (note: since the flag is needed to sync to CORP for CDDH label preview, it is set here instead of in print disp order function)
							Map<Long, Boolean> largeLayoutFlagMap = dispOrderManager.updateDispOrderLargeLayoutFlag(dispOrder);
							
							uncollectPrescManager.writeAuditLog("#0521", dispOrderId, null);
							em.flush();
							dispOrder.getPharmOrder().clearDmInfo();
							corpPmsServiceProxy.updateAdminStatus(dispOrder, DispOrderAdminStatus.Normal, PharmOrderAdminStatus.Normal, 
									CDDH_LEGACY_PERSISTENCE_ENABLED.get(false), CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED.get(false), largeLayoutFlagMap);
							dispOrder.getPharmOrder().loadDmInfo();
							dispOrderManager.printDispOrder(dispOrder, true);
							sfiForceProceed = true;
							break;
						}
					}
				}
				
				retrieveUnvetAndSuspendCount();
				return;
			}

			uncollectPrescManager.reverseUncollectPrescription(dispOrder, ONE_STOP_ENTRY, true, true);
			
			if (dispOrder.getDayEndStatus() == DispOrderDayEndStatus.None && ! dispOrder.getBatchProcessingFlag()) {
				dispOrderManager.printDispOrder(dispOrder, true);
			}
			
			retrieveUnvetAndSuspendCount();
		}
	}
	
	public void sfiPrescriptionUncollectDeferProceed(Long dispOrderId, Long dispOrderVersion) {
		
		if (checkUncollectOrderStatus(dispOrderId, dispOrderVersion) == null)
		{
			DispOrder dispOrder = em.find(DispOrder.class, dispOrderId);
			dispOrder.loadChild();
			dispOrder.getPharmOrder().getMedOrder().setCmsUpdateDate(new DateTime().toDate());
			dispOrder.getPharmOrder().setAdminStatus(PharmOrderAdminStatus.Normal);
			dispOrder = uncollectPrescManager.markRevokeFlagForUncollectMOEOrder(dispOrder, dispOrder.getPharmOrder().getAdminStatus());
			dispOrder.setAdminStatus(DispOrderAdminStatus.Suspended);
			dispOrder = uncollectPrescManager.forceUpdateDispOrder(dispOrder);
			uncollectPrescManager.writeAuditLog("#0521", dispOrderId, null);
			em.flush();
			dispOrder.getPharmOrder().clearDmInfo();
			corpPmsServiceProxy.updateAdminStatus(dispOrder, DispOrderAdminStatus.Suspended, PharmOrderAdminStatus.Normal, 
					CDDH_LEGACY_PERSISTENCE_ENABLED.get(false), CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED.get(false), null);
		}
		
		retrieveUnvetAndSuspendCount();
	}
	
	public void sfiPrescriptionUncollectForceProceed(Long dispOrderId, Long dispOrderVersion) {
		
		if (checkUncollectOrderStatus(dispOrderId, dispOrderVersion) == null)
		{
			DispOrder dispOrder = em.find(DispOrder.class, dispOrderId);
			dispOrder.loadChild();
			Invoice sfiInvoice = null;
			
			if ( sfiForceProceedInfo != null ) {
				for (Invoice invoice : dispOrder.getInvoiceList() ) {
					if (invoice.getDocType().equals(InvoiceDocType.Sfi)){
						sfiForceProceedInfo.setInvoice(invoice);
						sfiInvoice = invoice;
					}
				}			
			}
						
			dispOrder.getPharmOrder().setAdminStatus(PharmOrderAdminStatus.Normal);
			dispOrder = uncollectPrescManager.markRevokeFlagForUncollectMOEOrder(dispOrder, dispOrder.getPharmOrder().getAdminStatus());
			dispOrder.setAdminStatus(DispOrderAdminStatus.Normal);
			if (dispOrder.getStatus() == DispOrderStatus.Issued) {
				dispOrder.setIssueDate(new Date());
			}
			dispOrder.getPharmOrder().getMedOrder().setCmsUpdateDate(new DateTime().toDate());
			dispOrder = uncollectPrescManager.forceUpdateDispOrder(dispOrder);
			
			// set large layout flag for label reprint and CDDH label preview (for PMS mode)
			// (note: since the flag is needed to sync to CORP for CDDH label preview, it is set here instead of in print disp order function)
			Map<Long, Boolean> largeLayoutFlagMap = dispOrderManager.updateDispOrderLargeLayoutFlag(dispOrder);
			
			uncollectPrescManager.writeAuditLog("#0521", dispOrderId, null);
			em.flush();
			dispOrder.getPharmOrder().clearDmInfo();
			corpPmsServiceProxy.updateAdminStatusForceProceed(dispOrder, DispOrderAdminStatus.Normal, 
					PharmOrderAdminStatus.Normal, CDDH_LEGACY_PERSISTENCE_ENABLED.get(false), sfiInvoice, CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED.get(false), largeLayoutFlagMap);
			dispOrder.getPharmOrder().loadDmInfo();
			dispOrderManager.printDispOrder(dispOrder, true);
			sfiForceProceedInfo = null;
		}
		
		retrieveUnvetAndSuspendCount();
	}	
	
	private void retrieveUnvetAndSuspendCount() {
		if (oneStopCountManager.isCounterTimeOut(moStatusCount))
		{	
			oneStopCountManager.retrieveUnvetAndSuspendCount(workstore, propMap.getValueAsInteger(ONESTOP_SUSPENDORDERCOUNT_DURATION_LIMIT.getName()));		
		}
		
		moStatusCount = OneStopCountManagerBean.getMoStatusCountByWorkstore(workstore);
	}
	
	public String getStatusErrorMsg() {
		return statusErrorMsg;
	}
	
	public FcsPaymentStatus getFcsPaymentStatus() {
		return fcsPaymentStatus; 
	}
	
	public Boolean getSfiForceProceed() {
		return sfiForceProceed;
	}

	@Remove
	public void destroy(){
		
		if (moStatusCount != null)
		{
			moStatusCount = null;
		}
	}
}
