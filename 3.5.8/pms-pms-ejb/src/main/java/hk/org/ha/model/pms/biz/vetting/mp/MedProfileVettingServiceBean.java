package hk.org.ha.model.pms.biz.vetting.mp;

import static hk.org.ha.model.pms.prop.Prop.ALERT_EHR_PROFILE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.ALERT_PROFILE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.MEDPROFILE_ITEM_ADMINTIME_BUFFER;
import static hk.org.ha.model.pms.prop.Prop.VETTING_MEDPROFILE_ALERT_PROFILE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.VETTING_MEDPROFILE_ATDPS_REFILL_DURATION;
import static hk.org.ha.model.pms.prop.Prop.VETTING_MEDPROFILE_IPMOE_ADMINSTATUS_ENABLED;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.sys.SysProfile;
import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.fmk.pms.util.QueryUtils;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.alert.ehr.EhrAllergyException;
import hk.org.ha.model.pms.biz.alert.AlertProfileManagerLocal;
import hk.org.ha.model.pms.biz.alert.ehr.EhrAlertManagerLocal;
import hk.org.ha.model.pms.biz.charging.DrugChargeClearanceManagerLocal;
import hk.org.ha.model.pms.biz.delivery.DueOrderPrintManagerLocal;
import hk.org.ha.model.pms.biz.inbox.ManualProfileManagerLocal;
import hk.org.ha.model.pms.biz.inbox.PharmInboxClientNotifierInf;
import hk.org.ha.model.pms.biz.medprofile.MedProfileItemConverterLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileManagerLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileOrderProcessorLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileStatManagerLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.biz.medprofile.TpnRequestManagerLocal;
import hk.org.ha.model.pms.biz.pivas.PivasWorklistClientNotifierInf;
import hk.org.ha.model.pms.biz.pivas.PivasWorklistManagerLocal;
import hk.org.ha.model.pms.biz.reftable.ActiveWardManagerLocal;
import hk.org.ha.model.pms.biz.reftable.ItemLocationListManagerLocal;
import hk.org.ha.model.pms.biz.reftable.RefTableManagerLocal;
import hk.org.ha.model.pms.biz.reftable.SuspendReasonManagerLocal;
import hk.org.ha.model.pms.biz.reftable.WardConfigManagerLocal;
import hk.org.ha.model.pms.dms.persistence.PmsDispenseConfig;
import hk.org.ha.model.pms.exception.vetting.mp.ConcurrentUpdateException;
import hk.org.ha.model.pms.exception.vetting.mp.SaveNewProfileException;
import hk.org.ha.model.pms.pas.PasServiceWrapper;
import hk.org.ha.model.pms.persistence.PropEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileActionLog;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileErrorLog;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItemAlert;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItemFm;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileTrxLog;
import hk.org.ha.model.pms.persistence.medprofile.MpRenalEcrclLog;
import hk.org.ha.model.pms.persistence.medprofile.MpRenalMessage;
import hk.org.ha.model.pms.persistence.medprofile.PurchaseRequest;
import hk.org.ha.model.pms.persistence.medprofile.Replenishment;
import hk.org.ha.model.pms.persistence.medprofile.ReplenishmentItem;
import hk.org.ha.model.pms.persistence.medprofile.TpnRequest;
import hk.org.ha.model.pms.persistence.pivas.MaterialRequestItem;
import hk.org.ha.model.pms.persistence.pivas.PivasWorklist;
import hk.org.ha.model.pms.persistence.reftable.ItemDispConfig;
import hk.org.ha.model.pms.persistence.reftable.ItemLocation;
import hk.org.ha.model.pms.persistence.reftable.ItemSpecialty;
import hk.org.ha.model.pms.persistence.reftable.WardConfig;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.prop.Prop;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.disp.MpTrxType;
import hk.org.ha.model.pms.udt.medprofile.ActionLogType;
import hk.org.ha.model.pms.udt.medprofile.ErrorLogType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileMoItemStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileProcessType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileType;
import hk.org.ha.model.pms.udt.medprofile.ReplenishmentStatus;
import hk.org.ha.model.pms.udt.medprofile.WardTransferType;
import hk.org.ha.model.pms.udt.pivas.MaterialRequestItemStatus;
import hk.org.ha.model.pms.udt.pivas.PivasWorklistStatus;
import hk.org.ha.model.pms.udt.reftable.MachineType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.alert.AlertProfile;
import hk.org.ha.model.pms.vo.charging.FcsPaymentStatus;
import hk.org.ha.model.pms.vo.medprofile.PasContext;
import hk.org.ha.model.pms.vo.rx.IvRxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;
import hk.org.ha.model.pms.vo.vetting.mp.ManualEntryInfo;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;
import hk.org.ha.service.pms.asa.interfaces.ipmoe.IpmoeServiceJmsRemote;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.internal.jpa.EntityManagerImpl;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;
import org.jboss.seam.security.Identity;
import org.springframework.beans.BeanUtils;

@Stateful
@Scope(ScopeType.SESSION)
@Name("medProfileVettingService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MedProfileVettingServiceBean implements MedProfileVettingServiceLocal {
	
	private static final String MED_PROFILE_QUERY_HINT_1 = "p.medCase";
	private static final String MED_PROFILE_QUERY_HINT_2 = "p.patient";
	private static final String MED_PROFILE_QUERY_HINT_3 = "p.workstore.workstoreGroup";
	
	private static final String MED_PROFILE_ITEM_QUERY_HINT_FETCH_1 = "i.medProfile.medCase";
	private static final String MED_PROFILE_ITEM_QUERY_HINT_FETCH_2 = "i.medProfile.patient";
	
	private static final String MED_PROFILE_ITEM_QUERY_HINT_BATCH_1 = "i.medProfileMoItem.medProfilePoItemList";
	private static final String MED_PROFILE_ITEM_QUERY_HINT_BATCH_LOOKUP_1 = "i.medProfileMoItem.medProfilePoItemList.medProfileMoItem";
	private static final String MED_PROFILE_ITEM_QUERY_HINT_BATCH_2 = "i.medProfileMoItem.medProfileMoItemFmList";
	private static final String MED_PROFILE_ITEM_QUERY_HINT_BATCH_3 = "i.medProfileMoItem.medProfileMoItemAlertList";
	private static final String MED_PROFILE_ITEM_QUERY_HINT_BATCH_4 = "i.medProfileErrorLog";
	
	private static final String PERMISSION_INPATIENT_VERIFY_ITEM = "ipVerifyItem";
	private static final String PERMISSION_INPATIENT_VET_ITEM = "ipVetItem";
	private static final String PERMISSION_INPATIENT_PEND_ITEM = "ipPendItem";
	private static final String PERMISSION_INPATIENT_CANCEL_PEND_ITEM = "ipCancelPendItem";
	
	private static final String REPLENISHMENT_QUERY_HINT_FETCH_1 = "r.replenishmentItemList.medProfilePoItem";
	private static final String REPLENISHMENT_QUERY_HINT_FETCH_2 = "r.medProfileMoItem.medProfileItem.medProfileMoItem";
	
	private static MpTrxTypeComparator MP_TRX_TYPE_COMPARATOR = new MpTrxTypeComparator();
	private static ReplenishmentItemComparator REPLENISHMENT_ITEM_COMPARATOR = new ReplenishmentItemComparator();

	@Logger
	private Log logger;
	
	@In
	private AuditLogger auditLogger;
	
	@PersistenceContext
	private EntityManager em;

	@In
	private MedProfileManagerLocal medProfileManager;
	
	@In
	private ManualProfileManagerLocal manualProfileManager;
	
	@In
	private MedProfileItemConverterLocal medProfileItemConverter;
	
	@In
	private MedProfileOrderProcessorLocal medProfileOrderProcessor;
	
	@In
	private MedProfileStatManagerLocal medProfileStatManager;
	
	@In
	private AlertProfileManagerLocal alertProfileManager;
	
	@In
	private EhrAlertManagerLocal ehrAlertManager;

	@In
	private ActiveWardManagerLocal activeWardManager;
	
	@In
	private DueOrderPrintManagerLocal dueOrderPrintManager;
	
	@In
	private DrugChargeClearanceManagerLocal drugChargeClearanceManager;
	
	@In
	private SuspendReasonManagerLocal suspendReasonManager;
	
	@In
	private CheckAtdpsManagerLocal checkAtdpsManager;
	
	@In
	private RefTableManagerLocal refTableManager;
	
	@In
	private PharmInboxClientNotifierInf pharmInboxClientNotifier;
	
	@In
	private IpmoeServiceJmsRemote ipmoeServiceProxy;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private Identity identity;
	
	@In
	private UamInfo uamInfo;
		
	@In
	private SysProfile sysProfile;
	
	@In
	private Workstore workstore;
	
	@In
	private Workstation workstation;
	
	@In
	private List<Integer> atdpsPickStationsNumList;
	
	@In
	private WardConfigManagerLocal wardConfigManager;
	
	@In
	private RenalAdjManagerLocal renalAdjManager;
	
	@In
	private PivasWorklistManagerLocal pivasWorklistManager;
	
	@In
	private TpnRequestManagerLocal tpnRequestManager;
	
	@In
	private PivasWorklistClientNotifierInf pivasWorklistClientNotifier;
	
	@In
	private ItemLocationListManagerLocal itemLocationListManager;
	
	@In
	private PasServiceWrapper pasServiceWrapper;
	
	@In(required = false)
	@Out(required = false)
	private Long tpnSessionId;
	
	@Out(required = false)
	private MedProfileType medProfileType;
	
	@Out(required = false)
	private MedProfile medProfile;
	
	@Out(required = false)
	private List<MedProfile> chemoMedProfileList;
	
	@Out(required = false)
	private List<MedProfileItem> medProfileItemList;
	
	@Out(required = false)
	private AlertProfile alertProfile;

	@Out(required = false)
	private Boolean protectedMode;
	
	@Out(required = false)
	private Boolean readOnly;
	
	@Out(required = false)
	private Boolean profileLocked;
	
	@Out(required = false)
	private Boolean canVet;
	
	@Out(required = false)
	private Boolean canVerify;
	
	@Out(required = false)
	private Boolean canPend;
	
	@Out(required = false)
	private Boolean canCancelPend;
	
	@Out(required = false)
	private Boolean errorOnRetrieveItemCutOffDate;
	
	@Out(required = false)
	private List<PurchaseRequest> purchaseRequestList;
	
	@Out(required = false)
	private List<Replenishment> replenishmentList;

	@Out(required = false)
	private Date startVettingDate;
		
	@Out(required = false)
	private ManualEntryInfo manualEntryInfo;
	
	@Out(required = false)
	private List<MpRenalEcrclLog> mpRenalEcrclLogList;
	
	@Out(required = false)
	private List<MpRenalMessage> mpRenalMessageList;

	@In(required = false) 
	@Out(required = false)
	private List<PivasWorklist> pivasWorklistList;
	
	@In(required=false)
	@Out(required=false)
	private List<TpnRequest> tpnRequestList;
	
	@Out(required = false)
	private boolean alertServiceError;
	
	@Out(required = false)
	private boolean ehrAlertServiceError;
	
	private void init() {
    	
    	boolean debug = sysProfile.isDebugEnabled();
    	
    	canVerify = debug || identity.hasPermission(PERMISSION_INPATIENT_VERIFY_ITEM, "Y");
    	canVet = Boolean.TRUE.equals(canVerify) || identity.hasPermission(PERMISSION_INPATIENT_VET_ITEM, "Y");
    	canPend = debug || identity.hasPermission(PERMISSION_INPATIENT_PEND_ITEM, "Y");
    	canCancelPend = debug || identity.hasPermission(PERMISSION_INPATIENT_CANCEL_PEND_ITEM, "Y");

    	medProfileItemList = null;
    	
    	tpnRequestList = new ArrayList<TpnRequest>();
    	
    	manualEntryInfo = new ManualEntryInfo();
    	
    	mpRenalEcrclLogList = new ArrayList<MpRenalEcrclLog>();
    	mpRenalMessageList = new ArrayList<MpRenalMessage>();
    	
    	pivasWorklistList = new ArrayList<PivasWorklist>();
    }	
	
    @Override
	public void vetMedProfile(MedProfile medProfile) {
    	
    	init();
    	medProfileType = medProfile.getType();
    	List<MedProfile> medProfileList = retrieveRelevantMedProfileList(medProfile);
    	medProfile = findMainMedProfile(medProfileList);
    	chemoMedProfileList = constructChemoMedProfileList(medProfileList);
    	PasContext context = retrieveContext(medProfile);
		alertProfile = retrieveAlertProfile(context, medProfile);
    	Set<Integer> adminItemNumSet = retrieveGivenAdminStatusItemNumSet(medProfile);
    	Pair<List<PurchaseRequest>, List<PurchaseRequest>> purchaseRequestForUpdate = retrievePurchaseRequestForUpdate(medProfile);
    	List<PurchaseRequest> fcsPurchaseReqList = purchaseRequestForUpdate.getFirst();
    	List<PurchaseRequest> endItemPurchaseReqList = purchaseRequestForUpdate.getSecond();
    	Map<String,FcsPaymentStatus> fcsPaymentMap = dueOrderPrintManager.retrieveDrugChargeClearance(fcsPurchaseReqList);
    	
    	medProfileList = retrieveLockedMedProfileList(medProfileList);
    	chemoMedProfileList = constructChemoMedProfileList(medProfileList);
    	this.medProfile = findMainMedProfile(medProfileList);
    	
    	if (this.medProfile != null) {

    		profileLocked = Boolean.TRUE.equals(this.medProfile.getProcessingFlag()) && 
					!workstation.getId().equals(this.medProfile.getWorkstationId());

    		applyChanges(profileLocked, context, this.medProfile);
    		
    		this.medProfile.clearMedProfileItemList();
    		
    		updateProfileTransferInfo(profileLocked, this.medProfile);
    		
    		errorOnRetrieveItemCutOffDate = context != null && context.isErrorOnRetrieveItemCutOffDate();
    		protectedMode = profileLocked || this.medProfile.getStatus() != MedProfileStatus.Active ||
    				this.medProfile.getWardCode() == null || errorOnRetrieveItemCutOffDate;
    		readOnly = protectedMode || this.medProfile.getStatus() != MedProfileStatus.Active || this.medProfile.getType() == MedProfileType.Chemo;
    		
    		markMedProfileProcessing(!profileLocked, this.medProfile);

    		if (profileLocked) {
    			updateWorkstationHostName(this.medProfile);
    		} else {
    			drugChargeClearanceManager.updatePaymentStatus(fcsPaymentMap, fcsPurchaseReqList, true);
    			updateEndItemPurchaseRequest(endItemPurchaseReqList);
    		}
    		
    		retrieveVettingInfo(profileLocked, this.medProfile, medProfileList, adminItemNumSet);
    		retrieveRenalAdj(this.medProfile);
    		updateUnitDoseRelatedInfo(this.medProfile, this.medProfileItemList);
    		mapDrugSynonym(this.medProfileItemList);
    		retrieveEhrPatient(this.medProfile);
    	}
    }
    
    @SuppressWarnings("unchecked")
    public Boolean checkProfileHasLabelGenerationWithinToday() {
		List<DeliveryItem> deliveryItemList = ((List<DeliveryItem>)em.createQuery(
    			"select o from DeliveryItem o" + // 20161128 index check : DeliveryItem.medProfileId : FK_DELIVERY_ITEM_05
    			" where o.medProfile.id = :medProfileId" +
    			" and o.createDate >= :todayBegin" +
    			" and o.createDate < :tomorrowBegin")    			
    			.setParameter("medProfileId", medProfile.getId())
    			.setParameter("todayBegin", MedProfileUtils.getTodayBegin())
    			.setParameter("tomorrowBegin", MedProfileUtils.getNextDayBegin())
    			.getResultList());
    	
    	return Boolean.valueOf(deliveryItemList.size() > 0);    		    	    	    
    }
    
    private PasContext retrieveContext(MedProfile medProfile) {
    	
    	if (medProfile.getId() == null || !em.contains(medProfile)) {
    		return null;
    	}
    	
    	return medProfileManager.retrieveContext(medProfile, true, true);
    }
    
    private void applyChanges(boolean profileLocked, PasContext context, MedProfile medProfile) {
    	
    	if (profileLocked || medProfile == null) {
    		return;
    	}
    	
    	if (context != null) {
    		medProfileManager.applyChanges(context, medProfile);
    	} else if ( medProfile.getId() == null && 
    			medProfile.getMedCase() != null && medProfile.getMedCase().getPasWardCode() == null) {
    		medProfileManager.updateSpecCodeAndWardCodeRelatedSetting(workstore, null, null, false, medProfile, medProfile.getMedProfileItemList());
    	}    	
    }
    
    private void markMedProfileProcessing(boolean acquireLock, MedProfile medProfile) {
    	
    	if (acquireLock && medProfile.getId() != null) {
    		medProfile.setProcessingFlag(true);
    		medProfile.setWorkstationId(workstation.getId());
    		pharmInboxClientNotifier.dispatchUpdateEvent(this.medProfile.getHospCode());
    		logger.info("MedProfile(id=#0, orderNum=#1) has been locked by Workstation(id=#2)", medProfile.getId(), medProfile.getOrderNum(), workstation.getId());
    	}
    }
    
    private void retrieveVettingInfo(boolean profileLocked, MedProfile medProfile, List<MedProfile> medProfileList, Set<Integer> adminItemNumSet) {
    	
    	startVettingDate = new Date();

		setMedProfileAtdpsWardFlag(medProfile);
		medProfileItemList = retrieveMedProfileItemList(profileLocked, medProfile, medProfileList, adminItemNumSet, startVettingDate);
		replenishmentList = retrieveReplenishmentList(medProfile);
		purchaseRequestList = retrievePurchaseRequestList(medProfile, InvoiceStatus.ActiveInvoiceStatus);
		
		Map<Long, MedProfileMoItem> moItemHashMap = new HashMap<Long, MedProfileMoItem>();
		Map<Long, MedProfilePoItem> poItemHashMap = new HashMap<Long, MedProfilePoItem>();
		for (MedProfileItem mpi:medProfileItemList) {
			moItemHashMap.put(mpi.getMedProfileMoItem().getId(), mpi.getMedProfileMoItem());
			for (MedProfilePoItem poi:mpi.getMedProfileMoItem().getMedProfilePoItemList()) {
				poItemHashMap.put(poi.getId(), poi);
			}
		}
		em.flush();
		em.clear();
		
		relinkReplenishment(replenishmentList, moItemHashMap, poItemHashMap);
		
		for( PurchaseRequest pr: purchaseRequestList ){
			if( pr.getInvoiceStatus() == null ){
				pr.setInvoiceStatus(InvoiceStatus.None);
			}
			
			boolean isEndItem = false;
			if( isNonProcessEndedItem(pr, startVettingDate) ){
				isEndItem = true;
			}
			
			String drugSynonymString = null;
			if( pr.getMedProfilePoItem() != null ){
				drugSynonymString = refTableManager.getFdnByItemCodeOrderTypeWorkstore(pr.getMedProfilePoItem().getItemCode(), OrderType.InPatient, workstore);
			}
			pr.loadMpPharmInfo();
			pr.getMpPharmInfo().setDrugSynonym(drugSynonymString);
			if( !"Modified".equals(pr.getItemStatus())  && isEndItem ){
				pr.getMpPharmInfo().setMedProfileItemStatus(MedProfileItemStatus.Ended);
				pr.setItemStatus(MedProfileItemStatus.Ended.getDisplayValue());
			}
			
			if( !medProfile.getPrivateFlag() ){
    			if( pr.isActiveOutstandingPurchaseRequest() ){
    				if( poItemHashMap.get(pr.getMpPharmInfo().getMedProfilePoItemId()) != null ){
    					MedProfilePoItem mpPoi = poItemHashMap.get(pr.getMpPharmInfo().getMedProfilePoItemId());
    					mpPoi.setPurchaseRequestExistFlag(true);
    				}
    			}
			}
		}
    }
    
    @SuppressWarnings("unchecked")
	private Set<Integer> retrieveGivenAdminStatusItemNumSet(MedProfile medProfile) {
    	
    	if (!VETTING_MEDPROFILE_IPMOE_ADMINSTATUS_ENABLED.get(Boolean.FALSE) || medProfile.isManualProfile()) {
    		return Collections.EMPTY_SET;
    	}
    	
    	List<Integer> itemNumList = em.createQuery(
    			"select distinct i.medProfileMoItem.itemNum from MedProfileItem i" + // 20161129 index check : MedProfileItem.medProfileId : FK_MED_PROFILE_ITEM_01
    			" where i.medProfileId = :medProfileId" +
    			" and i.status in :activeStatusList" +
    			" and i.adminDate is null" +
    			" and i.medProfileMoItem.itemNum < 10000" +
    			" order by i.medProfileMoItem.itemNum")
    			.setParameter("medProfileId", medProfile.getId())
    			.setParameter("activeStatusList", MedProfileItemStatus.Unvet_Vetted_Verified)
    			.getResultList();
    	
    	if (itemNumList.size() <= 0) {
    		return Collections.EMPTY_SET;
    	}
    	
    	try {
    		return ipmoeServiceProxy.retrieveGivenAdminStatusItemNumSet(medProfile.getOrderNum(), itemNumList);
    	} catch (Exception e) {
    		logger.error(e, e);
    	}
    	
		return Collections.EMPTY_SET;
    }
    
    private Pair<List<PurchaseRequest>, List<PurchaseRequest>> retrievePurchaseRequestForUpdate(MedProfile medProfileIn){
    	List<PurchaseRequest> fcsUpdatePurchaseRequestList = new ArrayList<PurchaseRequest>();
    	List<PurchaseRequest> endItemUpdatePurchaseRequestList = new ArrayList<PurchaseRequest>();
    	Date currentDate = new Date();
    	
    	List<PurchaseRequest> prList = retrievePurchaseRequestList(medProfileIn, InvoiceStatus.None_Outstanding);
    	for(PurchaseRequest pr: prList){
    		if( pr.getInvoiceStatus() == InvoiceStatus.Outstanding ){
    			fcsUpdatePurchaseRequestList.add(pr);
    		}else if( isNonProcessEndedItem(pr, currentDate) ){
    			endItemUpdatePurchaseRequestList.add(pr);
    		}
    	}
    	return new Pair<List<PurchaseRequest>, List<PurchaseRequest>>(fcsUpdatePurchaseRequestList, endItemUpdatePurchaseRequestList);
    }
    
    private boolean isNonProcessEndedItem(PurchaseRequest pr, Date startVettingDate){
    	if( pr.getMedProfilePoItem().getEndDate() == null ){
    		return false;
    	}
    	if( startVettingDate.compareTo(pr.getMedProfilePoItem().getEndDate()) >= 0 ){
    		MedProfileItem medProfileItem = pr.getMedProfilePoItem().getMedProfileMoItem().getMedProfileItem();
    		
    		if( pr.getMedProfilePoItem().getMedProfileMoItem().getStatus() == MedProfileMoItemStatus.SysDeleted ){
    			return false;
    		}
    		
    		if( medProfileItem.getStatus() == MedProfileItemStatus.Deleted ||  
    		    medProfileItem.getStatus() == MedProfileItemStatus.Discontinued ||
    		    medProfileItem.getStatus() == MedProfileItemStatus.Ended
    		    ){
    			return false;
    		}
    		logger.debug("isNonProcessEndedItem - invoiceNum:#0, itemEndDate:#1", pr.getInvoiceNum(), pr.getMedProfilePoItem().getEndDate());
    		return true;
    	}
    	return false;
    }
    
    private void updateEndItemPurchaseRequest(List<PurchaseRequest> endItemPrList){
    	logger.info("updateEndItemPurchaseRequest :#0", endItemPrList.size());
    	for(PurchaseRequest pr: endItemPrList){
    		pr.setInvoiceStatus(InvoiceStatus.SysDeleted);
    	}
    	em.flush();
    }
    
	private void relinkReplenishment(List<Replenishment> replenishmentList, Map<Long, MedProfileMoItem> moItemHashMap, Map<Long, MedProfilePoItem> poItemHashMap) {
    	MedProfileMoItem moi;
		for (Replenishment r:replenishmentList) {
			moi = r.getMedProfileMoItem();
			moi.postLoad();
			if (moItemHashMap.get(moi.getId()) != null) {
				//Relink replenishment to medProfileMoItem
				r.setMedProfileMoItem(moItemHashMap.get(moi.getId()));								
				for (ReplenishmentItem rItem:r.getReplenishmentItemList()) {
					//Relink replenishmentItem to medProfilePoItem
					if (poItemHashMap.get(rItem.getMedProfilePoItem().getId()) != null) {
						rItem.setMedProfilePoItem(poItemHashMap.get(rItem.getMedProfilePoItem().getId()));
					}
				}
				Collections.sort(r.getReplenishmentItemList(), REPLENISHMENT_ITEM_COMPARATOR);
			} else {
				for (ReplenishmentItem rItem:r.getReplenishmentItemList()) {
					rItem.getMedProfilePoItem().loadDmInfo();
				}			
			}
		}
    }
    
    private void retrieveRenalAdj(MedProfile medProfile) {
    	if (medProfile.getId() != null) {
    		mpRenalEcrclLogList = renalAdjManager.retrieveMpRenalEcrclLog(medProfile.getId());
    		mpRenalMessageList = renalAdjManager.retrieveMpRenalMessage(medProfile.getId());
    	}
    }
    
    private AlertProfile retrieveAlertProfile(PasContext context, MedProfile medProfile) {

    	alertServiceError = false;
		ehrAlertServiceError = false;
    	AlertProfile alertProfile = null;
    	Patient patient = ObjectUtils.defaultIfNull(context == null ? null : context.getPatient(), medProfile.getPatient());

		if (ALERT_PROFILE_ENABLED.get(Boolean.FALSE) && VETTING_MEDPROFILE_ALERT_PROFILE_ENABLED.get(Boolean.FALSE)) {
			try {
				alertProfile = alertProfileManager.retrieveAlertProfile(medProfile.getPatHospCode(), medProfile.getMedCase().getCaseNum(), patient);
			} catch (Exception ex) {
				alertServiceError = true;
				logger.error(ex, ex);
			}
		}
		
		if (ALERT_EHR_PROFILE_ENABLED.get()) {
			try {
				alertProfile = ehrAlertManager.retrieveEhrAllergySummary(alertProfile, medProfile.getPatHospCode(), patient.getPatKey(), patient.getHkid());
			}
			catch (EhrAllergyException e) {
				ehrAlertServiceError = true;
				logger.error(e, e);
			}
		}		
		return alertProfile;
    }
    
    private void updateUnitDoseRelatedInfo(MedProfile medProfile, List<MedProfileItem> medProfileItemList) {

    	if (atdpsPickStationsNumList == null || atdpsPickStationsNumList.isEmpty()) {
    		return;
    	}
    	
    	Integer atdpsDefaultRefillDuraion = VETTING_MEDPROFILE_ATDPS_REFILL_DURATION.get(1);
    	Date now = new Date();
    	
    	Set<String> atdpsItemCodeSet = medProfileManager.retrieveAtdpsItemCodeSet(workstore);
    	Map<String, PmsDispenseConfig> dispenseConfigMap = new HashMap<String, PmsDispenseConfig>();
    	
    	List<MedProfilePoItem> medProfilePoItemList2Recal = new ArrayList<MedProfilePoItem>();
    	
    	for (MedProfileItem medProfileItem: medProfileItemList) {
    		
    		MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
    		if (medProfileMoItem == null || em.contains(medProfileItem) || em.contains(medProfileMoItem)) {
    			continue;
    		}
    		
    		if (medProfileItem.getStatus() == MedProfileItemStatus.Vetted && !Boolean.TRUE.equals(medProfileItem.getMedProfileMoItem().getPivasFlag())) {
    			setMedProfilePoItemUnitDoseFlag(medProfileItem.getMedProfileMoItem());
    		} else {
    			setMedProfilePoItemUnitDoseItem(medProfileItem.getMedProfileMoItem());
    		}
    		
    		for (MedProfilePoItem medProfilePoItem: medProfileMoItem.getMedProfilePoItemList()) {
    			if (Boolean.TRUE.equals(medProfileMoItem.getUnitDoseExceptFlag())) {
        			if (!atdpsItemCodeSet.contains(medProfilePoItem.getItemCode()) ||
        					!checkAtdpsManager.isValidAtdpsItem(workstore, medProfileMoItem, medProfilePoItem, true, true)) {
        				continue;
        			}
        			
        			Integer refillDuration = Boolean.TRUE.equals(medProfile.getAtdpsWardFlag()) ? atdpsDefaultRefillDuraion :
        						retrieveRefillDuration(dispenseConfigMap, medProfilePoItem.getItemCode());

    				updateUnitDoseFlag(medProfile.getAtdpsWardFlag(), medProfileItem, medProfilePoItem);
        			
        			if (MedProfileUtils.compare(refillDuration, medProfilePoItem.getRefillDurationInDays()) != 0) {
        				medProfileItem.setModified(Boolean.TRUE);
        				updateDueDateForVerifiedRefillItem(MedProfileUtils.max(now, medProfileMoItem.getStartDate()), medProfileItem, medProfilePoItem);
        				medProfilePoItem.setRefillDuration(refillDuration);
        				medProfilePoItemList2Recal.add(medProfilePoItem);
        			}
        		} else if (medProfileItem.getStatus() == MedProfileItemStatus.Unvet && Boolean.TRUE.equals(medProfilePoItem.getUnitDoseFlag()) &&
        				(!Boolean.TRUE.equals(medProfileMoItem.getDoseSpecifiedFlag()) || !Boolean.TRUE.equals(medProfilePoItem.getSingleDispFlag()))) {
        			
        			medProfilePoItem.setRefillDuration(atdpsDefaultRefillDuraion);
        			medProfilePoItemList2Recal.add(medProfilePoItem);
        		}
    		}
    		revertStatusForUnitDoseExceptionItem(medProfileItem, medProfileMoItem);
    	}
    	recalculateForAtdpsAffectedItems(workstore, medProfile, medProfilePoItemList2Recal);
    }
    
    private void updateUnitDoseFlag(Boolean unitDoseFlag, MedProfileItem medProfileItem, MedProfilePoItem medProfilePoItem) {
    	
		if (ObjectUtils.notEqual(unitDoseFlag, medProfilePoItem.getUnitDoseFlag())) {
			medProfileItem.setModified(Boolean.TRUE);			
			medProfilePoItem.setUnitDoseFlag(unitDoseFlag);
		}
    }
    
    private void updateDueDateForVerifiedRefillItem(Date dueDate, MedProfileItem medProfileItem, MedProfilePoItem medProfilePoItem) {

		if (medProfileItem.getStatus() == MedProfileItemStatus.Verified && medProfilePoItem.getLastDueDate() != null) {
			medProfilePoItem.setDueDate(dueDate);
		}
    }
    
    private Integer retrieveRefillDuration(Map<String, PmsDispenseConfig> dispenseConfigMap, String itemCode) {

    	if (!dispenseConfigMap.containsKey(itemCode)) {
    		dispenseConfigMap.put(itemCode, dmsPmsServiceProxy.retrievePmsDispenseConfig(itemCode, workstore.getWorkstoreGroup().getWorkstoreGroupCode()));
    	}
    	
    	PmsDispenseConfig dispenseConfig = dispenseConfigMap.get(itemCode);
    	return dispenseConfig == null ? null : dispenseConfig.getDefaultDispenseDay();
    }
    
    private void revertStatusForUnitDoseExceptionItem(MedProfileItem medProfileItem, MedProfileMoItem medProfileMoItem) {
    	
		if (Boolean.TRUE.equals(medProfileMoItem.getUnitDoseExceptFlag()) && medProfileItem.getStatus() == MedProfileItemStatus.Verified) {
    		medProfileItem.setModified(Boolean.TRUE);
    		medProfileItem.setStatus(MedProfileItemStatus.Vetted);
    	}
    }
    
    private void recalculateForAtdpsAffectedItems(Workstore workstore, MedProfile medProfile, List<MedProfilePoItem> medProfilePoItemList) {
    	
    	if (medProfilePoItemList.size() > 0) {
    		medProfilePoItemList = medProfileItemConverter.recalculateQty(workstore, medProfile, medProfilePoItemList);
    		for (MedProfilePoItem medProfilePoItem: medProfilePoItemList) {
    			medProfilePoItem.setIssueQty(medProfilePoItem.getCalQty());
    		}
    	}
    }
    
    private void mapDrugSynonym(List<MedProfileItem> medProfileItemList) {
    	
    	Map<String, String> fdnMap = refTableManager.getFdnByItemCodeListOrderTypeWorkstore(
    			buildVettedVerifiedItemCodeSet(medProfileItemList), OrderType.InPatient, workstore);
    	
    	for (MedProfileItem medProfileItem: medProfileItemList) {
    		MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
    		if (medProfileMoItem == null || !MedProfileItemStatus.Vetted_Verified.contains(medProfileItem.getStatus())) {
    			continue;
    		}
    		
    		for (MedProfilePoItem medProfilePoItem: medProfileMoItem.getMedProfilePoItemList()) {
    			if (!em.contains(medProfilePoItem)) {
    				medProfilePoItem.setDrugSynonym(fdnMap.get(medProfilePoItem.getItemCode()));
    			}
    		}
    	}
    }
    
    private Set<String> buildVettedVerifiedItemCodeSet(List<MedProfileItem> medProfileItemList) {

    	Set<String> itemCodeSet = new HashSet<String>();
    	
    	for (MedProfileItem medProfileItem: medProfileItemList) {
    		MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
    		if (medProfileMoItem == null || !MedProfileItemStatus.Vetted_Verified.contains(medProfileItem.getStatus())) {
    			continue;
    		}
    		
    		for (MedProfilePoItem medProfilePoItem: medProfileMoItem.getMedProfilePoItemList()) {
    			itemCodeSet.add(medProfilePoItem.getItemCode());
    		}
    	}
    	return itemCodeSet;
    }
    
    private void createMedProfileItem(MedProfile medProfile, List<MedProfileItem> medProfileItemList) {
    	MedProfileMoItem medProfileMoItem;	
    	medProfile = em.find(MedProfile.class, medProfile.getId());
    	
    	for (MedProfileItem medProfileItem:medProfileItemList) {
			if ( medProfileItem.getId() != null ) {
				continue;//Skip IPMOE item
			}
			medProfileMoItem = medProfileItem.getMedProfileMoItem();
			medProfileItem.setMedProfileMoItem(null);
			medProfileItem.setMedProfile(medProfile);
			
			em.persist(medProfileItem);
			em.flush();
			
			medProfileMoItem.setMedProfileItem(medProfileItem);
			
			em.persist(medProfileMoItem);
			em.flush();
			
			medProfileItem.setMedProfileMoItem(medProfileMoItem);			
			
			linkChildren(medProfileMoItem);
		}
    }
    
    private void lockHospPropForNewProfile(MedProfile medProfile) {
    	if (medProfile.getId() == null) {
    		PropEntity propEntity = Prop.VETTING_MEDPROFILE_MANUAL_CREATE_TOKEN.getPropEntity(workstore.getHospital());
    		em.createQuery(
					"select o.id from HospitalProp o" + // 20150507 index check : HospitalProp.hospCode,propId : UI_HOSPITAL_PROP_01
					" where o.hospCode = :hospCode" +
					" and o.propId = :propId")
					.setParameter("hospCode", workstore.getHospCode())
					.setParameter("propId", propEntity.getPropId())
					.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
					.getResultList();
    	}
    }
    
	private void checkExistingMedProfile(MedProfile medProfile) {
		if (medProfileManager.retrieveMedProfileList(medProfile.getMedCase().getCaseNum(), medProfile.getPatient().getHkid(), workstore).size() > 0) {
			throw new SaveNewProfileException("saveMedProfileForManualEntry");
		}
    }
    
    @Override
    public String updateMedProfile(MedProfile medProfile, List<MedProfileItem> medProfileItemList, List<Replenishment> replenishmentList, List<PurchaseRequest> purchaseRequestList,
    		List<MpRenalEcrclLog> mpRenalEcrclLogList, List<MpRenalMessage> mpRenalMessageList) {

    	try {
	    	Context context = new Context(medProfile);
	    	String printResult = StringUtils.EMPTY;
	    		
    		if (medProfile.isManualProfile()) {
    			lockHospPropForNewProfile(medProfile);
    			if (medProfile.getId() == null) {
    	    		
    	    		checkExistingMedProfile(medProfile);
    	    		
    	    		medProfileManager.savePatientAndMedCase(medProfile);
    	    		medProfile.setWorkstore(workstore);
    	    		em.persist(medProfile);
    	    		em.flush();
    	    	} else {
    	    		updateMedProfileRelatedInfo(medProfile, true);
    	    	}
    		} else {
	    		// lock all corresponding medProfile at once
				em.createQuery(
						"select o.id from MedProfile o" + // 20130309 index check : MedProfile.id : PK_MED_PROFILE
						" where o.id = :medProfileId")
						.setParameter("medProfileId", medProfile.getId())
						.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
						.getResultList();
    		}
    		
    		logMedProfileStatusChange(medProfile);

    		//manual item
    		medProfile.setMaxItemNum(((MedProfile)Contexts.getSessionContext().get("medProfile")).getMaxItemNum());
			createMedProfileItem(medProfile, medProfileItemList);
			
	    	updateMedProfileRelatedInfo(medProfile, false);
	    	updateMedProfileItemList(context, medProfile, medProfileItemList);
	    	updateReplenishmentList(context, medProfile, replenishmentList);
	    	em.flush();
	    	updatePivasWorklistList(context, medProfile, medProfileItemList);
			em.flush();
			
			updateTpnStatus(medProfile);
			
	    	updateCancelDischargeInfo(context, medProfile);
	    	if( !purchaseRequestList.isEmpty() ){
				medProfileManager.saveChargeOrderNum(medProfile);					
			}
	    	
	    	boolean inactivePasWardFlag = medProfile.getInactivePasWardFlag();
	    	medProfile = em.merge(medProfile);
	    	recordSaveProfileTrx(medProfile);
	    	em.flush();
	    	
	    	medProfile.initInactivePasWardFlag(inactivePasWardFlag);
	    	sendMessagesToCMS(medProfile, context);
	    	
	    	Pair<List<PurchaseRequest>,List<PurchaseRequest>> reqPair = updatePurchaseRequestListForIpmoe(context, medProfile, purchaseRequestList, medProfileItemList);
	    		    	
	    	renalAdjManager.save(mpRenalEcrclLogList, mpRenalMessageList, medProfile);
	    	
	    	medProfile.setMedProfileItemList(medProfileItemList);
	    	
	    	medProfile.setAlertProfile(alertProfile);
	    	context.restoreState(medProfile);
	    	context.validate();
	    	printResult = dueOrderPrintManager.directPrint(workstation, medProfile, atdpsPickStationsNumList, reqPair.getFirst(), reqPair.getSecond());
	    	medProfileStatManager.recalculateAllStat(medProfile, true);		    	
	    	
	    	medProfileOrderProcessor.processMedProfileOrder(medProfile.getOrderNum(), false);

	    	if(medProfile.getStatus() == MedProfileStatus.HomeLeave) {
				medProfileManager.removeUnfinishedMaterialRequestItemsAndPivasOrderFromWorklist(medProfile);
				em.flush();
			}
	    	
	    	this.medProfile = null;
	    	this.chemoMedProfileList = null;	    	
	    	this.medProfileItemList = null;
	    	this.replenishmentList = null;
	    	return printResult;
    	} catch (OptimisticLockException ex) {
    		
    		logger.warn(ex, ex);
    		throw new ConcurrentUpdateException("DUE_ORDER_PRINT");
    	}
    }
    
    private void logMedProfileStatusChange(MedProfile medProfile) {  		
		if (this.medProfile.getStatus() != medProfile.getStatus()) {
	  		logger.info("MedProfile patient status change|MedProfileId [#0]|OrderNum [#1]|OrgMedProfileStatus [#2]|MedProfileStatus [#3]|ReturnDate [#4]|ResumeDate [#5]",
    				medProfile.getId(),
    				medProfile.getOrderNum(),
    				this.medProfile.getStatus(),
    				medProfile.getStatus(),
    				medProfile.getReturnDate(),
    				medProfile.getResumeActiveDate());
		}
    }
    
    private void updateTpnStatus(MedProfile medProfile) {
    	tpnSessionId = tpnRequestManager.updateTpnRequest(medProfile, tpnRequestList, tpnSessionId);
    }
    
    private void updateCancelDischargeInfo(Context context, MedProfile medProfile) {
    	if (medProfile.getConfirmCancelDischargeUser() != null && medProfile.getConfirmCancelDischargeDate() == null) {
    		medProfile.setConfirmCancelDischargeDate(context.getDate());
    		auditLogger.log("#0692", medProfile.getOrderNum(), medProfile.getMedCase().getCaseNum(), false);
    	}
    }
    
    @Override
    public void releaseMedProfile() {
    	
    	if (medProfile == null) {
    		return;
    	}
		
		medProfileOrderProcessor.releaseProcessingMedProfile(medProfile.getId(), null, workstation.getId(), false);
		medProfileOrderProcessor.processMedProfileOrder(medProfile.getOrderNum(), false);
		
    	this.medProfile = null;
    	this.chemoMedProfileList = null;
    	this.medProfileItemList = null;
    	this.replenishmentList = null;
    }
    
    private void recordSaveProfileTrx(MedProfile medProfile) {
    	
    	MedProfileTrxLog trxLog = new MedProfileTrxLog();
    	trxLog.setMedProfile(medProfile);
    	trxLog.setType(MpTrxType.MpSaveProfile);
    	trxLog.setWorkstationId(workstation.getId());
    	em.persist(trxLog);
    }
    
    private void updateMedProfileRelatedInfo(MedProfile medProfile, boolean updatePatientInfo) {
    	
    	if (medProfile.getWorkstore() != null && StringUtils.equals(medProfile.getWorkstore().getHospCode(), workstore.getHospCode())) {
    		medProfile.setWorkstore(workstore);
    	}
    	if (medProfile.getWorkstationId() != null) {
    		logger.info("MedProfile(id=#0, orderNum=#1) has been released by Workstation(id=#2)", medProfile.getId(), medProfile.getOrderNum(), medProfile.getWorkstationId());
    	}
    	medProfile.setProcessingFlag(false);
    	medProfile.setWorkstationId(null);
    	if (updatePatientInfo) {
    		em.merge(medProfile.getPatient());
    	}
    }
    
    private void updateMedProfileItemList(Context context, MedProfile medProfile, List<MedProfileItem> medProfileItemList) {
    	
    	if (medProfileItemList == null) {
    		return;
    	}
    	
    	for (MedProfileItem medProfileItem: medProfileItemList) {
    		updateMedProfileItem(context, medProfile, medProfileItem);
    	}
    }
    
    private void updateFirstDoseMaterialRequest(PivasWorklist pivasWorklist, MedProfile medProfile, MedProfileItem mpi) {
    	if (pivasWorklist.getFirstDoseMaterialRequest() == null) {
    		return;
    	}
    	
    	pivasWorklist.getFirstDoseMaterialRequest().setMedProfile(medProfile);
    	pivasWorklist.getFirstDoseMaterialRequest().setMedProfileMoItem(mpi.getMedProfileMoItem());
    	for (MedProfilePoItem mppi : mpi.getMedProfileMoItem().getMedProfilePoItemList()) {
    		for (MaterialRequestItem requestItem : pivasWorklist.getFirstDoseMaterialRequest().getMaterialRequestItemList()) {
    			if (!mppi.getId().equals(requestItem.getMedProfilePoItem().getId()) && !StringUtils.equals(requestItem.getMedProfilePoItem().getUuid(), mppi.getUuid())) {
    				continue;
    			}
    			requestItem.setMedProfilePoItem(mppi);    			
    		}
    	}
    	em.persist(pivasWorklist.getFirstDoseMaterialRequest());    	
    	em.flush();
    }
    
    private void proceedResumeDispInPmsIpDisp(Long medProfileItemId) {
    	logger.info("proceedResumeDispInPmsIpDisp-medProfileItemId:#0", medProfileItemId);
    	pivasWorklistManager.resumeDispInPmsIp(medProfileItemId);
    }
  
    private void markSysDeleteToWorklistInDB(PivasWorklist pivasWorklistInDB) {
    	if (pivasWorklistInDB.getFirstDoseMaterialRequest() != null) {
			for (MaterialRequestItem requestItem : pivasWorklistInDB.getFirstDoseMaterialRequest().getMaterialRequestItemList()) {
				requestItem.setStatus(MaterialRequestItemStatus.SysDeleted);
			}
		}
		pivasWorklistInDB.setStatus(PivasWorklistStatus.SysDeleted);
    }
    
    private void updateCurrentPivasWorklistInDB(PivasWorklist worklist) {
    	PivasWorklist pivasWorklistInDB = pivasWorklistManager.retrievePivasWorklistByMedProfileItemId(worklist.getMedProfileMoItem().getMedProfileItemId());    		
    	if (pivasWorklistInDB != null) {
	    	if (Boolean.TRUE.equals(worklist.getResumeDispInPmsIpActionFlag()) || (worklist.getId() == null && pivasWorklistInDB.getPivasPrep() != null)) {
				proceedResumeDispInPmsIpDisp(worklist.getMedProfileMoItem().getMedProfileItemId());
			} else if (worklist.getId() == null){
				markSysDeleteToWorklistInDB(pivasWorklistInDB);	    			
			}
    	}    
    }
    
    //true: clear, false: no clear action
    private boolean clearRedundantPivasWorklist(List<MedProfileItem> medProfileItemList) {
    	List<Long> medProfileItemIdList = new ArrayList<Long>();
    	for (MedProfileItem mpi : medProfileItemList) {
    		medProfileItemIdList.add(mpi.getId());
    	}
    	
    	if (medProfileItemIdList.size() == 0) {
    		return false;    		
    	}
    	
    	List<PivasWorklist> redundantWorklistList = pivasWorklistManager.retrieveRedundantWorklist(medProfileItemIdList);
    	for (PivasWorklist redundantWorklist : redundantWorklistList) {
    		logger.info("Clear redundant pivas worklist [pivasWorklistId:#0, medProfileItemId:#1, medProfileItemStatus:#2]", redundantWorklist.getId(), redundantWorklist.getMedProfileMoItem().getMedProfileItemId(), redundantWorklist.getMedProfileMoItem().getMedProfileItem().getStatus().getDisplayValue());
    		markSysDeleteToWorklistInDB(redundantWorklist);
    	}
    	if (redundantWorklistList.size() > 0) {
    		return true;
    	}
    	return false;
    }
    
    private MedProfileItem getManagedMedProfileItemForWorklist(PivasWorklist worklist, List<MedProfileItem> medProfileItemList) {
    	MedProfileMoItem targetWorklistMpmi = worklist.getMedProfileMoItem();
    	for (MedProfileItem mpi : medProfileItemList) {
			if (mpi.getStatus() != MedProfileItemStatus.Vetted && mpi.getStatus() != MedProfileItemStatus.Verified) {
				continue;
			}
			if (!targetWorklistMpmi.getOrgItemNum().equals(mpi.getMedProfileMoItem().getOrgItemNum()) || !targetWorklistMpmi.getItemNum().equals(mpi.getMedProfileMoItem().getItemNum())) {
				continue;
			}
			
			return em.find(MedProfileItem.class, mpi.getId());
    	}    
    	return null;
    }
    
	private void updatePivasWorklistList(Context context, MedProfile medProfile, List<MedProfileItem> medProfileItemList) {
    	MedProfileItem managedMpi;
    	for (PivasWorklist worklist : pivasWorklistList) {
    		logger.info("worklist.id:#0, worklist.status:#1", worklist.getId(), worklist.getStatus().getDisplayValue());
    		updateCurrentPivasWorklistInDB(worklist);
			if (worklist.getStatus() == PivasWorklistStatus.Deleted) {
				continue;
			}
			if (worklist.getId() != null) {
				continue;
			}
			
	    	//relink to managed entity and create new set of pivasWorklist
    		worklist.setWorkstore(workstore);
    		worklist.setWorkstoreGroupCode(workstore.getWorkstoreGroup().getWorkstoreGroupCode());
    		worklist.setMedProfile(medProfile);	    	
    					    			    			
    		managedMpi = getManagedMedProfileItemForWorklist(worklist, medProfileItemList);
    		if (managedMpi != null) {    			
    			managedMpi.getMedProfileMoItem().setPivasFlag(Boolean.TRUE);
    	    	worklist.setMedProfileMoItem(managedMpi.getMedProfileMoItem());
    			updateFirstDoseMaterialRequest(worklist, medProfile, managedMpi);
    			em.persist(worklist);
    		}
    	}
    	boolean clearFlag = clearRedundantPivasWorklist(medProfileItemList);
    	if (clearFlag || pivasWorklistList.size() > 0) {
    		pivasWorklistClientNotifier.dispatchUpdateEvent(workstore.getWorkstoreGroup().getWorkstoreGroupCode());
    	}
    }
    
    private void updateMedProfileItem(Context context, MedProfile medProfile, MedProfileItem medProfileItem) {

		MedProfileItem medProfileItemInDB = retrieveMedProfileItem(medProfileItem.getId());
		if (medProfileItemInDB == null) {
			throw new IllegalArgumentException("MedProfileItem(" + medProfileItem.getId() + ") not exists");
		}
		
		MedProfileMoItem medProfileMoItemInDB = medProfileItemInDB.getMedProfileMoItem();
		MedProfileItem orgMedProfileItem = createSnapshot(medProfileItemInDB);
		
		if (medProfileItemInDB.getStatus() != medProfileItem.getStatus()) {
			medProfileItem.setStatusUpdateDate(context.getDate());
		}
		if (medProfileItem.getStatus() == MedProfileItemStatus.Verified && medProfileItem.getVerifyIndex() != null) {
			medProfileItem.setVerifyDate(context.calculateVerifyDate(medProfileItem.getVerifyIndex()));
			medProfileItem.setVerifyUser(identity.getCredentials().getUsername());
		}

    	MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
    	prepareMedProfilePoItemList(medProfileItem);
    	resetModifyFlag(medProfileItem);
    	
    	medProfileItem.setMedProfile(medProfileItemInDB.getMedProfile());
		medProfileItem.setMedProfileMoItem(medProfileMoItemInDB);
		medProfileItem.setMedProfileErrorLog(medProfileItemInDB.getMedProfileErrorLog());
		em.merge(medProfileItem);
		
		hideMdsErrorStat(medProfileItem);
		medProfileItem.setMedProfileMoItem(medProfileMoItem);

		boolean replaceMedProfileMoItemFlag = medProfileMoItem.getId() == null; 
		if (replaceMedProfileMoItemFlag) {
			replaceMedProfileMoItem(medProfileMoItemInDB, medProfileMoItem);  			
		} else {
			medProfileMoItem = updateMedProfileMoItem(medProfile, medProfileMoItemInDB, medProfileMoItem);
		}
		em.flush();
		
		if (medProfileMoItem.getMedProfileItem() == null) {
			throw new RuntimeException("Linkage from MedProfileMoItem(" + medProfileMoItem.getId() + ") to MedProfileItem is broken");
		}

		logMedProfileAction(medProfile, orgMedProfileItem.getMedProfileMoItem(), medProfileMoItem);
		checkAndAddUpdateDispenseMessage(context, replaceMedProfileMoItemFlag, medProfileMoItem);
		checkAndAddVettedOrderMessage(context, orgMedProfileItem, medProfileItem);			
		checkAndAddPendingOrderMessage(context, orgMedProfileItem.getMedProfileMoItem(), medProfileMoItem);
    }
    
    private void resetModifyFlag(MedProfileItem medProfileItem) {
		if (medProfileItem.getStatus() == MedProfileItemStatus.Unvet && Boolean.TRUE.equals(medProfileItem.getMedProfileMoItem().getModifyFlag())) {
			medProfileItem.getMedProfileMoItem().setModifyFlag(Boolean.FALSE);
		}
	}

	private void hideMdsErrorStat(MedProfileItem medProfileItem) {
    	
    	MedProfileErrorLog medProfileErrorLog = medProfileItem.getMedProfileErrorLog();
    	if (medProfileErrorLog != null && medProfileErrorLog.getType() == ErrorLogType.MdsAlert) {
    		medProfileErrorLog.setHideErrorStatFlag(Boolean.TRUE);
    	}
    }
    
    private MedProfileItem createSnapshot(MedProfileItem medProfileItem) {
    	
    	MedProfileItem medProfileItemSnapshot = new MedProfileItem();
    	MedProfileMoItem medProfileMoItemSnapshot = new MedProfileMoItem();
    	
    	BeanUtils.copyProperties(medProfileItem, medProfileItemSnapshot,
    			new String[]{"medProfile", "medProfileErrorLog", "medProfileMoItem"});
    	BeanUtils.copyProperties(medProfileItem.getMedProfileMoItem(), medProfileMoItemSnapshot,
    			new String[]{"medProfileItem", "medProfileMoItemAlert", "medProfileMoItemFm", "medProfilePoItem"});
    	
    	medProfileItemSnapshot.setMedProfileMoItem(medProfileMoItemSnapshot);
    	medProfileMoItemSnapshot.setMedProfileItem(medProfileItemSnapshot);
    	
    	return medProfileItemSnapshot;
    }
    
    private void prepareMedProfilePoItemList(MedProfileItem medProfileItem) {
    	
    	Iterator<MedProfilePoItem> iter = medProfileItem.getMedProfileMoItem().getMedProfilePoItemList().iterator();
    	while (iter.hasNext()) {
    		MedProfilePoItem medProfilePoItem = iter.next();
    		if (medProfileItem.getStatus() == MedProfileItemStatus.Unvet && medProfilePoItem.getId() == null) {
    			iter.remove();
    		} else if (medProfilePoItem.getIssueQty() == null) {
    			medProfilePoItem.setIssueQty(BigDecimal.ZERO);
    		}
    	}
    }
    
    private MedProfileMoItem updateMedProfileMoItem(MedProfile medProfile,
    		MedProfileMoItem medProfileMoItemInDB, MedProfileMoItem medProfileMoItem) {
    	
    	linkChildren(medProfileMoItem);
    	updatePendingInfo(medProfile, medProfileMoItemInDB, medProfileMoItem);
    	
    	if (medProfileMoItem.discardPoItemChanges()) {
    		medProfileMoItem.setMedProfilePoItemList(medProfileMoItemInDB.getMedProfilePoItemList());
    	}
    	RxItem rxItem = medProfileMoItem.getRxItem();
		medProfileMoItem = em.merge(medProfileMoItem);
		medProfileMoItem.setRxItem(rxItem);
		return medProfileMoItem;
    }
    
    private void replaceMedProfileMoItem(MedProfileMoItem medProfileMoItemInDB, MedProfileMoItem medProfileMoItem) {

    	MedProfileItem medProfileItemInDB = medProfileMoItemInDB.getMedProfileItem();
    	
    	if (medProfileMoItem.discardPoItemChanges()) {
    		copyMedProfilePoItemList(medProfileMoItemInDB, medProfileMoItem);
    	}
    	linkWorkstore(medProfileMoItem);
		medProfileMoItem.setMedProfileItem(medProfileItemInDB);
		em.persist(medProfileMoItem);
		
		medProfileMoItemInDB.setStatus(MedProfileMoItemStatus.SysDeleted);
		em.merge(medProfileMoItemInDB);

		medProfileItemInDB.setMedProfileMoItem(medProfileMoItem);
		em.merge(medProfileItemInDB);
    }
    
    private void copyMedProfilePoItemList(MedProfileMoItem srcMedProfileMoItem, MedProfileMoItem destMedProfileMoItem) {

    	List<MedProfilePoItem> medProfilePoItemListCopy = new ArrayList<MedProfilePoItem>();
    	
    	for (MedProfilePoItem medProfilePoItem: srcMedProfileMoItem.getMedProfilePoItemList()) {
    		MedProfilePoItem medProfilePoItemCopy = new MedProfilePoItem();
    		BeanUtils.copyProperties(medProfilePoItem, medProfilePoItemCopy,
    				new String[]{"id", "medProfileMoItem", "version"});
    		medProfilePoItemCopy.setMedProfileMoItem(destMedProfileMoItem);
    		medProfilePoItemListCopy.add(medProfilePoItemCopy);
    	}
    	
    	destMedProfileMoItem.setMedProfilePoItemList(medProfilePoItemListCopy);
    }
    
    private void updatePendingInfo(MedProfile medProfile, MedProfileMoItem orgMedProfileMoItem, MedProfileMoItem medProfileMoItem) {
    	
    	if (!isPendingStateChanged(orgMedProfileMoItem, medProfileMoItem)) {
        	medProfileMoItem.setPendingUser(orgMedProfileMoItem.getPendingUser());
        	medProfileMoItem.setPendingUserName(orgMedProfileMoItem.getPendingUserName());
        	medProfileMoItem.setPendingDate(orgMedProfileMoItem.getPendingDate());
    	} else if (medProfileMoItem.getPendingCode() == null) {
        	medProfileMoItem.setPendingUser(null);
        	medProfileMoItem.setPendingUserName(null);
        	medProfileMoItem.setPendingDate(null);
    	} else {
        	medProfileMoItem.setPendingUser(identity.getCredentials().getUsername());
        	medProfileMoItem.setPendingUserName(uamInfo.getDisplayName());
    		if (medProfile.getPendingUserName() != null) {
	        	medProfileMoItem.setPendingUser(medProfile.getPendingUserName());
	        	medProfileMoItem.setPendingUserName(medProfile.getPendingDisplayName());
    		}
        	medProfileMoItem.setPendingDate(new Date());
    	}
    }
    
    private void logMedProfileAction(MedProfile medProfile, MedProfileMoItem orgMedProfileMoItem, MedProfileMoItem medProfileMoItem) {
    
    	if (isSuspendStateChanged(orgMedProfileMoItem, medProfileMoItem)) {
    		
        	MedProfileActionLog medProfileActionLog = constructMedProfileActionLog(medProfile, medProfileMoItem);
    		if (medProfileMoItem.getSuspendCode() == null) {
    			medProfileActionLog.setType(ActionLogType.ResumeSuspend);
    		} else {
    			medProfileActionLog.setType(ActionLogType.Suspend);
    			medProfileActionLog.setActionCode(medProfileMoItem.getSuspendCode());			
    			medProfileActionLog.setSupplMessage(medProfileMoItem.getSuspendSupplDesc());  			
    			medProfileActionLog.setMessage(suspendReasonManager.retrieveSuspendReasonDesc(
    					workstore, medProfileMoItem.getSuspendCode()));
    		}
    		em.persist(medProfileActionLog);
    	}
    	
    	if (isPendingStateChanged(orgMedProfileMoItem, medProfileMoItem)) {
    		
    		MedProfileActionLog medProfileActionLog = constructMedProfileActionLog(medProfile, medProfileMoItem);
    		if (medProfileMoItem.getPendingCode() == null) {
    			medProfileActionLog.setType(ActionLogType.ResumePending);
    			medProfileActionLog.setMessage(medProfileMoItem.getResumePendingRemark());
    			if (medProfile.getResumePendingUserName() != null) {
	    			medProfileActionLog.setActionUser(medProfile.getResumePendingUserName());
	    			medProfileActionLog.setActionUserName(medProfile.getResumePendingDisplayName());
    			}
    		} else {
    			medProfileActionLog.setType(ActionLogType.Pending);
    			medProfileActionLog.setActionCode(medProfileMoItem.getPendingCode());
    			medProfileActionLog.setMessage(medProfileMoItem.getPendingDesc());
    			medProfileActionLog.setSupplMessage(medProfileMoItem.getPendingSupplDesc());
    			if (medProfile.getPendingUserName() != null) {
	    			medProfileActionLog.setActionUser(medProfile.getPendingUserName());
	    			medProfileActionLog.setActionUserName(medProfile.getPendingDisplayName());
    			}
    		}
    		em.persist(medProfileActionLog);
    	}
    }
    
    private boolean isSuspendStateChanged(MedProfileMoItem orgMedProfileMoItem, MedProfileMoItem medProfileMoItem) {
    	return MedProfileUtils.compare(orgMedProfileMoItem.getSuspendCode(), medProfileMoItem.getSuspendCode()) != 0 ||
    			MedProfileUtils.compareString(orgMedProfileMoItem.getSuspendSupplDesc(), medProfileMoItem.getSuspendSupplDesc()) != 0;
    }
    
    private boolean isPendingStateChanged(MedProfileMoItem orgMedProfileMoItem, MedProfileMoItem medProfileMoItem) {
    	return MedProfileUtils.compare(orgMedProfileMoItem.getPendingCode(), medProfileMoItem.getPendingCode()) != 0 ||
				MedProfileUtils.compareString(orgMedProfileMoItem.getPendingSupplDesc(), medProfileMoItem.getPendingSupplDesc()) != 0;
    }
    
    private MedProfileActionLog constructMedProfileActionLog(MedProfile medProfile, MedProfileMoItem medProfileMoItem) {

		MedProfileActionLog medProfileActionLog = new MedProfileActionLog();
		medProfileActionLog.setMedProfileMoItem(medProfileMoItem);
		medProfileActionLog.setWardCode(medProfile.getWardCode());
		medProfileActionLog.setActionUser(identity.getCredentials().getUsername());
		medProfileActionLog.setActionUserName(uamInfo.getDisplayName());
		medProfileActionLog.setCreateDate(new Date());
		
		return medProfileActionLog;
    }
    
    private void checkAndAddUpdateDispenseMessage(Context context, boolean addMessage, MedProfileMoItem medProfileMoItem) {
    	
    	if (addMessage) {
    		context.addMessageRequest(MpTrxType.MpUpdateDispensing, medProfileMoItem);
    	}
    }
    
    private void checkAndAddVettedOrderMessage(Context context, MedProfileItem orgMedProfileItem, MedProfileItem medProfileItem) {
    	
    	MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
    	
    	if (medProfileMoItem.getFirstPrintDate() != null) {
    		return;
    	}
    	MedProfileItemStatus orgMedProfileItemStatus = orgMedProfileItem.getStatus();
    	MedProfileItemStatus medProfileItemStatus = medProfileItem.getStatus();

    	MedProfileProcessType processType = medProfileMoItem.isNoLabelOrder() ?
    			MedProfileProcessType.dataValueOf(Prop.MEDPROFILE_NOLABELITEM_SENDSTATUSMSG.get()) : 
    			MedProfileProcessType.dataValueOf(Prop.MEDPROFILE_NORMALITEM_SENDSTATUSMSG.get());
    			
    	if ((processType == MedProfileProcessType.Vet && orgMedProfileItemStatus == MedProfileItemStatus.Unvet &&
    			MedProfileItemStatus.Vetted_Verified.contains(medProfileItemStatus)) ||
    			(processType == MedProfileProcessType.Verify && MedProfileItemStatus.Unvet_Vetted.contains(orgMedProfileItemStatus) &&
    			medProfileItemStatus == MedProfileItemStatus.Verified)) {
    		
    		logger.debug("Sending VetOrder Message to CMS for MedProfileMoItem(#0)", medProfileMoItem.getId());
    		context.addMessageRequest(MpTrxType.MpVetOrder, medProfileMoItem);
    		
    		if (medProfileMoItem.getUrgentNum() != null) {
        		logger.debug("Sending UrgentDispenseUpdate Message to CMS for MedProfileMoItem(#0)", medProfileMoItem.getId());
        		context.addMessageRequest(MpTrxType.MpUrgentDispenseUpdate, medProfileMoItem);
    		}
    	}
    }
    
    private void checkAndAddPendingOrderMessage(Context context, MedProfileMoItem orgMedProfileMoItem, MedProfileMoItem medProfileMoItem) {
    	
    	if (isPendingStateChanged(orgMedProfileMoItem, medProfileMoItem)) {
        	if (medProfileMoItem.getPendingCode() == null) {
        		logger.debug("Sending ClearPendingOrder Message to CMS for MedProfileMoItem(#0)", medProfileMoItem.getId());
        		context.addMessageRequest(MpTrxType.MpClearOnHoldStatus, medProfileMoItem);
        	} else {
        		logger.debug("Sending PendingOrder Message to CMS for MedProfileMoItem(#0)", medProfileMoItem.getId());
        		context.addMessageRequest(MpTrxType.MpOnHoldOrder, medProfileMoItem);
        	}
    	}
    	
    }

    private void sendMessagesToCMS(MedProfile medProfile, Context context) {
    	
    	retrieveLockedMedProfile(medProfile);
    	for (Entry<MpTrxType, List<MedProfileMoItem>> entry : context.getMessageRequestMap().entrySet()) {
    		medProfileManager.sendMessages(entry.getKey(), medProfile, entry.getValue());
    	}
    	
    	if (!context.getReplenishmentRequestUpdateList().isEmpty()) {
    		medProfileManager.directSendReplenishmentUpdateMessages(workstore.getHospital(), context.getReplenishmentRequestUpdateList());
    	}
    }
    
    private void linkChildren(MedProfileMoItem medProfileMoItem) {
    	
    	List<MedProfileMoItemAlert> medProfileMoItemAlertList = medProfileMoItem.getMedProfileMoItemAlertList();
    	List<MedProfileMoItemFm> medProfileMoItemFmList = medProfileMoItem.getMedProfileMoItemFmList();
    	List<MedProfilePoItem> medProfilePoItemList = medProfileMoItem.getMedProfilePoItemList();
    	
		for (MedProfileMoItemAlert medProfileMoItemAlert: medProfileMoItemAlertList) {
			medProfileMoItemAlert.setMedProfileMoItem(medProfileMoItem);
		}
    	
		for (MedProfileMoItemFm medProfileMoItemFm: medProfileMoItemFmList) {
			medProfileMoItemFm.setMedProfileMoItem(medProfileMoItem);
		}
    	
		linkWorkstore(medProfileMoItem);
		
		for (MedProfilePoItem medProfilePoItem: medProfilePoItemList) {
			medProfilePoItem.setMedProfileMoItem(medProfileMoItem);
			if (medProfilePoItem.getId() == null) {
				em.persist(medProfilePoItem);
			}
		}
    }
    
    private void linkWorkstore(MedProfileMoItem medProfileMoItem) {
		for (MedProfilePoItem medProfilePoItem: medProfileMoItem.getMedProfilePoItemList()) {
			if (medProfilePoItem.getWorkstore() == null) {
				medProfilePoItem.setWorkstore(workstore);
			}
		}
    }
    
    private MedProfile findMainMedProfile(List<MedProfile> medProfileList) {
    	
    	for (MedProfile medProfile: medProfileList) {
    		if (medProfile.getType() != MedProfileType.Chemo) {
    			return medProfile;
    		}
    	}
    	
    	return medProfileList.isEmpty() ? null : medProfileList.get(0);
    }
    
    private List<MedProfile> constructChemoMedProfileList(List<MedProfile> medProfileList) {

    	List<MedProfile> chemoMedProfileList = new ArrayList<MedProfile>();
    	
    	for (MedProfile medProfile: medProfileList) {
    		if (medProfile.getType() == MedProfileType.Chemo) {
    			chemoMedProfileList.add(medProfile);
    		}
    	}
    	
    	return chemoMedProfileList;
    }
            
	@SuppressWarnings("unchecked")
	private List<MedProfile> retrieveRelevantMedProfileList(MedProfile medProfile) {
		
		if (medProfile.getId() == null) {
			return Arrays.asList(medProfile);
		}
		
		String patKey = medProfile.isManualProfile() ? null : medProfile.getPatient().getPatKey();
		String caseNum = medProfile.getType() == MedProfileType.Chemo ? medProfile.getMedCase().getCaseNum() : null;
		
		StringBuffer querySb = new StringBuffer()
				.append("select p from MedProfile p") // 20171023 index check : MedProfile.medCase MedProfile.patKey : FK_MED_PROFILE_01 FK_MED_PROFILE_02
				.append(" where p.hospCode = :hospCode")
				.append(" and (p.id = :id");
		if (patKey != null) {
			querySb.append(" or (p.type = :chemoType and p.patient.patKey = :patKey)");
		}
		if (caseNum != null) {
			querySb.append(" or p.medCase.caseNum = :caseNum");
		}
		querySb.append(") order by p.createDate desc");
		
		Query query = em.createQuery(querySb.toString());
		query.setHint(QueryHints.FETCH, MED_PROFILE_QUERY_HINT_1)
			.setHint(QueryHints.FETCH, MED_PROFILE_QUERY_HINT_2)
			.setHint(QueryHints.FETCH, MED_PROFILE_QUERY_HINT_3)
			.setParameter("hospCode", workstore.getHospCode())
			.setParameter("id", medProfile.getId());
		if (patKey != null) {
			query.setParameter("chemoType", MedProfileType.Chemo);
			query.setParameter("patKey", patKey);
		}
		if (caseNum != null) {
			query.setParameter("caseNum", caseNum);
		}
		
		List<MedProfile> resultList = filterRedundantNormalProfile(query.getResultList());
		if (resultList.isEmpty()) {
			return Arrays.asList(medProfile);
		} else {
			if (caseNum != null && findMainMedProfile(resultList) == null) {
				resultList.add(0, manualProfileManager.constructManualProfile(null, caseNum, workstore));
			}
			return resultList;
		}
	}
	
	private List<MedProfile> filterRedundantNormalProfile(List<MedProfile> medProfileList) {
		
		boolean normalProfileFound = false;
		List<MedProfile> resultList = new ArrayList<MedProfile>();
		for (MedProfile medProfile: medProfileList) {
			if (medProfile.getType() == MedProfileType.Chemo || !normalProfileFound) {
				resultList.add(medProfile);
				normalProfileFound |= medProfile.getType() != MedProfileType.Chemo;
			}
		}
		return resultList;
	}
    
    @SuppressWarnings("unchecked")
	private MedProfile retrieveLockedMedProfile(MedProfile medProfile) {
    	
    	if (medProfile.getId() == null) {
    		return medProfile;
    	}
    	
    	medProfile = (MedProfile) MedProfileUtils.getFirstItem(em.createQuery(
    			"select p from MedProfile p" + // 20120306 index check : MedProfile.id : PK_MED_PROFILE_ITEM
    			" where p.id = :id and p.id is not null") // don't delete : magic code fix bug for https://bugs.eclipse.org/bugs/show_bug.cgi?id=347592
    			.setParameter("id", medProfile.getId())
    			.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
    			.setHint(QueryHints.BATCH, MED_PROFILE_QUERY_HINT_1)
    			.setHint(QueryHints.BATCH, MED_PROFILE_QUERY_HINT_2)
    			.setHint(QueryHints.BATCH, MED_PROFILE_QUERY_HINT_3)
    			.getResultList());
    	
    	MedProfileUtils.lookup(medProfile, MED_PROFILE_QUERY_HINT_1);
    	MedProfileUtils.lookup(medProfile, MED_PROFILE_QUERY_HINT_2);
    	MedProfileUtils.lookup(medProfile, MED_PROFILE_QUERY_HINT_3);
    	
    	return medProfile;
    }
    
    @SuppressWarnings("unchecked")
	private List<MedProfile> retrieveLockedMedProfileList(List<MedProfile> medProfileList) {
    	
    	List<MedProfile> resultList = new ArrayList<MedProfile>();
    	
    	Set<Long> idSet = new TreeSet<Long>();    	
    	for (MedProfile medProfile: medProfileList) {
    		if (medProfile.getId() != null) {
    			idSet.add(medProfile.getId());
    		} else {
    			resultList.add(medProfile);
    		}
    	}

    	if (!idSet.isEmpty()) {
    		resultList.addAll(em.createQuery(
	    			"select p from MedProfile p" + // 20171023 index check : MedProfile.id : PK_MED_PROFILE
	    			" where p.id in :idSet")
	    			.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
	    			.setHint(QueryHints.BATCH, MED_PROFILE_QUERY_HINT_1)
	    			.setHint(QueryHints.BATCH, MED_PROFILE_QUERY_HINT_2)
	    			.setHint(QueryHints.BATCH, MED_PROFILE_QUERY_HINT_3)
	    			.setParameter("idSet", idSet)
	    			.getResultList());
    	}
    	return resultList;
    }
    
    @SuppressWarnings("unchecked")
    private MedProfileItem retrieveMedProfileItem(Long id) {
    	
    	return (MedProfileItem) MedProfileUtils.getFirstItem(em.createQuery(
    			"select i from MedProfileItem i" + // 20120306 index check : MedProfileItem.id : PK_MED_PROFILE_ITEM
    			" where i.id = :id")
    			.setParameter("id", id)
    			.setHint(QueryHints.FETCH, MED_PROFILE_ITEM_QUERY_HINT_FETCH_1)
    			.setHint(QueryHints.FETCH, MED_PROFILE_ITEM_QUERY_HINT_FETCH_2)
    			.getResultList());
    }
    
    @SuppressWarnings("unchecked")
	private List<MedProfileItem> retrieveMedProfileItemList(boolean profileLocked, MedProfile medProfile, List<MedProfile> medProfileList, Set<Integer> adminItemNumSet, Date startVettingDate) {
    	
    	List<MedProfileItem> medProfileItemList = em.createQuery(
    			"select i from MedProfileItem i" + // 20120306 index check : MedProfileItem.medProfile : FK_MED_PROFILE_ITEM_01
    			" where i.status in :activeStatusList" +
    			" and (i.medProfileMoItem.endDate >= :startVettingDate or i.medProfileMoItem.endDate is null)" +
    			" and i.medProfileId in :medProfileIdList" +
    			" order by i.medProfileMoItem.statFlag desc, i.medProfileMoItem.urgentFlag desc, i.medProfileMoItem.startDate, i.medProfileMoItem.orderDate")
    			.setParameter("activeStatusList", MedProfileItemStatus.Unvet_Vetted_Verified)
    			.setParameter("startVettingDate", startVettingDate)
    			.setParameter("medProfileIdList", MedProfileUtils.constructMedProfileIdList(medProfileList))
    			.setHint(QueryHints.BATCH, MED_PROFILE_ITEM_QUERY_HINT_BATCH_1)
    			.setHint(QueryHints.BATCH, MED_PROFILE_ITEM_QUERY_HINT_BATCH_2)
    			.setHint(QueryHints.BATCH, MED_PROFILE_ITEM_QUERY_HINT_BATCH_3)
    			.setHint(QueryHints.BATCH, MED_PROFILE_ITEM_QUERY_HINT_BATCH_4)
    			.getResultList();
    	
    	MedProfileUtils.lookup(medProfileItemList, MED_PROFILE_ITEM_QUERY_HINT_BATCH_LOOKUP_1);
    	MedProfileUtils.lookup(medProfileItemList, MED_PROFILE_ITEM_QUERY_HINT_BATCH_2);
    	MedProfileUtils.lookup(medProfileItemList, MED_PROFILE_ITEM_QUERY_HINT_BATCH_3);
    	MedProfileUtils.lookup(medProfileItemList, MED_PROFILE_ITEM_QUERY_HINT_BATCH_4);

    	if (!profileLocked) {
    		medProfileItemList = medProfileManager.markItemTransferInfo(medProfile, medProfileItemList, medProfile.getCurrentItemCutOffDate(),
    				WardTransferType.Normal);
    		updateAdminDate(medProfileItemList, adminItemNumSet, startVettingDate);
    		medProfileStatManager.recalculateMedProfilePoItemStat(medProfile);
    	}
    	medProfileItemList = medProfileManager.filterActiveMedProfileItemList(medProfileItemList, startVettingDate);
    	MedProfileUtils.markMealFlag(workstore, medProfileItemList);
    	convertMedProfileItemList(workstore, medProfileItemList);
    	mapSpecialControlFlag(workstore, medProfileItemList);
    	mapItemDispConfig(workstore, medProfileItemList);
    	return medProfileItemList;
    }
    
    private void updateAdminDate(List<MedProfileItem> medProfileItemList, Set<Integer> adminItemNumSet, Date startVettingDate) {
    	
    	for (MedProfileItem medProfileItem: medProfileItemList) {
    		if (medProfileItem.getAdminDate() == null && adminItemNumSet.contains(medProfileItem.getMedProfileMoItem().getItemNum())) {
    			medProfileItem.setAdminDate(startVettingDate);
    		}
    	}
    }
    
    private void convertMedProfileItemList(Workstore workstore, List<MedProfileItem> medProfileItemList) {
    	
    	em.flush();
    	List<MedProfileMoItem> unvettedMedProfileMoItemList = new ArrayList<MedProfileMoItem>();
    	
    	for (MedProfileItem medProfileItem: medProfileItemList) {
    		if (medProfileItem.getStatus() == MedProfileItemStatus.Unvet) {
    			if ( medProfileItem.getMedProfileMoItem().getRxItem() != null ) {
    				medProfileItem.getMedProfileMoItem().getRxItem().setManualItemFlag(Boolean.FALSE);
    			}
    			unvettedMedProfileMoItemList.add(medProfileItem.getMedProfileMoItem());
    		}
    		detach(medProfileItem.getMedProfileMoItem());
    	}
    	
    	medProfileItemConverter.convertItemList(workstore, unvettedMedProfileMoItemList, atdpsPickStationsNumList);
    	
    	MedProfileUtils.loadDmInfoAndInitUuidAndNewlyConvertedPoItem(medProfileItemList);
    }
    
    private void mapItemDispConfig(Workstore workstore, List<MedProfileItem> medProfileItemList) {
    	
    	Map<String, ItemDispConfig> itemDispConfigMap = retrieveItemDispConfigMap(workstore, constructItemCodeSet(medProfileItemList));
    	for (MedProfileItem medProfileItem: medProfileItemList) {
    		for (MedProfilePoItem medProfilePoItem: medProfileItem.getMedProfileMoItem().getMedProfilePoItemList()) {
    			medProfilePoItem.setItemDispConfig(itemDispConfigMap.get(medProfilePoItem.getItemCode()));
    		}
    	}
    }
    
    private void mapSpecialControlFlag(Workstore workstore, List<MedProfileItem> medProfileItemList) {

    	Map<String, Set<String>> itemSpecialtyMap = retrieveItemSpecialtyMap(workstore, constructItemCodeSet(medProfileItemList));
    	for (MedProfileItem medProfileItem: medProfileItemList) {
    		for (MedProfilePoItem medProfilePoItem: medProfileItem.getMedProfileMoItem().getMedProfilePoItemList()) {
    			Set<String> specCodeSet = itemSpecialtyMap.get(medProfilePoItem.getItemCode());
    			medProfilePoItem.setSpecialControlFlag(specCodeSet == null || medProfilePoItem.getChargeSpecCode() == null ?
    					Boolean.FALSE : !specCodeSet.contains(medProfilePoItem.getChargeSpecCode()));
    		}
    	}
    }
    
    private Set<String> constructItemCodeSet(List<MedProfileItem> medProfileItemList) {
    	
    	Set<String> itemCodeSet = new HashSet<String>();
    	for (MedProfileItem medProfileItem: medProfileItemList) {
    		for (MedProfilePoItem medProfilePoItem: medProfileItem.getMedProfileMoItem().getMedProfilePoItemList()) {
    			itemCodeSet.add(medProfilePoItem.getItemCode());
    		}
    	}
    	return itemCodeSet;
    }

    private void updateWorkstationHostName(MedProfile medProfile) {
    	
    	if (medProfile.getWorkstationId() != null) {
        	Workstation workstation = em.find(Workstation.class, medProfile.getWorkstationId());
        	medProfile.setWorkstationHostName(workstation.getHostName());
    	}
    }
    
    private void updateProfileTransferInfo(boolean profileLocked, MedProfile medProfile) {
    	
    	boolean inactivePasWard = !activeWardManager.isActiveWard(medProfile.getPatHospCode(), medProfile.getMedCase().getPasWardCode());
		medProfile.initInactivePasWardFlag(inactivePasWard);

		if (profileLocked) {
    		return;
    	}
		
		if (inactivePasWard) {
    		medProfile.setTransferFlag(true);
    	}
    }
    
    @SuppressWarnings("unchecked")
	private List<Replenishment> retrieveReplenishmentList(MedProfile medProfileIn) {
    	
    	List<Replenishment> replenishmentList = em.createQuery(
    			"select r from Replenishment r" + // 20120306 index check : Replenishment.medProfile : FK_REPLENISHMENT_01
    			" where r.medProfile = :medProfile" +
    			" and r.status != :replenishDeleteStatus" +  
    			" and (r.medProfileMoItem.transferFlag = false or r.medProfileMoItem.transferFlag is null)" +
    			" order by r.status, r.requestDate")
    			.setParameter("medProfile", medProfileIn)
    			.setParameter("replenishDeleteStatus", ReplenishmentStatus.SysDeleted)    			
    			.setHint(QueryHints.LEFT_FETCH, REPLENISHMENT_QUERY_HINT_FETCH_1)
    			.setHint(QueryHints.LEFT_FETCH, REPLENISHMENT_QUERY_HINT_FETCH_2)
    			.getResultList();
    	
    	MedProfileUtils.lookup(replenishmentList, REPLENISHMENT_QUERY_HINT_FETCH_1);
    	MedProfileUtils.lookup(replenishmentList, REPLENISHMENT_QUERY_HINT_FETCH_2);
    	
    	return replenishmentList;
    }
    
    @SuppressWarnings("unchecked")
	private List<PurchaseRequest> retrievePurchaseRequestList(MedProfile medProfileIn, List<InvoiceStatus> invoiceStatusList){
    	List<PurchaseRequest> purchaseReqList = em.createQuery(
    			"select o from PurchaseRequest o" + // 20160616 index check : PurchaseRequest.medProfile : FK_PURCHASE_REQUEST_01
    			" where (o.invoiceStatus is null or o.invoiceStatus in :invoiceStatusList)" +
    			" and o.medProfile = :medProfile ")
    			.setParameter("invoiceStatusList", invoiceStatusList)
    			.setParameter("medProfile", medProfileIn)
    			.getResultList();
    	logger.info("retrievePurchaseRequestList #0", purchaseReqList.size());
    	return purchaseReqList;
    }
    
    @SuppressWarnings("unchecked")
    private Map<String, Set<String>> retrieveItemSpecialtyMap(Workstore workstore, Set<String> itemCodeSet) {
    	
    	List<ItemSpecialty> itemSpecialtyList = QueryUtils.splitExecute(em.createQuery(
    			"select i from ItemSpecialty i" + // 20120306 index check : ItemSpecialty.workstoreGroup : FK_ITEM_SPECIALTY_01
    			" where i.workstoreGroup = :workstoreGroup" +
    			" and i.itemCode in :itemCodeList")
    			.setParameter("workstoreGroup", workstore.getWorkstoreGroup()), "itemCodeList", itemCodeSet);
    	
    	Map<String, Set<String>> itemSpecialtyMap = new HashMap<String, Set<String>>();
    	for (ItemSpecialty itemSpecialty: itemSpecialtyList) {
    		Set<String> specCodeSet = itemSpecialtyMap.get(itemSpecialty.getItemCode());
    		if (specCodeSet == null) {
    			specCodeSet = new HashSet<String>();
    			itemSpecialtyMap.put(itemSpecialty.getItemCode(), specCodeSet);
    		}
    		specCodeSet.add(itemSpecialty.getSpecCode());
    	}
    	return itemSpecialtyMap;
    }
    
    @SuppressWarnings("unchecked")
	private Map<String, ItemDispConfig> retrieveItemDispConfigMap(Workstore workstore, Set<String> itemCodeSet) {
    	
    	List<ItemDispConfig> itemDispConfigList = QueryUtils.splitExecute(em.createQuery(
    	    	"select i from ItemDispConfig i" + // 20120306 index check : ItemDispConfig.workstoreGroup : FK_ITEM_DISP_CONFIG_01
    	    	" where i.workstoreGroup = :workstoreGroup" +
    	    	" and i.itemCode in :itemCodeList")
    	    	.setParameter("workstoreGroup", workstore.getWorkstoreGroup()), "itemCodeList", itemCodeSet);
    	
    	Map<String, ItemDispConfig> itemDispConfigMap = new HashMap<String, ItemDispConfig>();
    	for (ItemDispConfig itemDispConfig: itemDispConfigList) {
    		itemDispConfigMap.put(itemDispConfig.getItemCode(), itemDispConfig);
    	}
    	return itemDispConfigMap;
    }
        
    private void updateReplenishmentToLatestMoiRef(Replenishment replenishment, MedProfileItem medProfileItemInDB) {
		replenishment.setMedProfileMoItem(medProfileItemInDB.getMedProfileMoItem());
		replenishment.setReplenishmentItemList(new ArrayList<ReplenishmentItem>());
		MedProfileItem latestMedProfileItem = replenishment.getMedProfileMoItem().getMedProfileItem();			
		for ( MedProfilePoItem poi : latestMedProfileItem.getMedProfileMoItem().getMedProfilePoItemList() ) {
			ReplenishmentItem replenishmentItem = new ReplenishmentItem();
			replenishmentItem.setMedProfilePoItem(poi);
			replenishment.addReplenishmentItem(replenishmentItem);
			em.persist(replenishmentItem);
		}			
		em.merge(replenishment);
		logger.info("Relink replenishment:#0, status:#1 with latest MedProfileMoItem from front-end", replenishment.getId(), replenishment.getStatus().getDisplayValue());
    }
    
    private List<PurchaseRequest> relinkPurchaseRequestList(Context context, MedProfile medProfile, List<PurchaseRequest> purchaseRequestList, List<MedProfileItem> medProfileItemList){
    	List<PurchaseRequest> relinkedPrList = new ArrayList<PurchaseRequest>();
    	
    	Map<String, MedProfilePoItem> poItemUuidHashMap = new HashMap<String, MedProfilePoItem>();
		for (MedProfileItem mpi:medProfileItemList) {
			for (MedProfilePoItem poi:mpi.getMedProfileMoItem().getMedProfilePoItemList()) {
				poItemUuidHashMap.put(poi.getUuid(), poi);
			}
		}
    	
		for (PurchaseRequest purchaseRequest : purchaseRequestList) {			
			MedProfilePoItem mpPoi = null;
			
			if( purchaseRequest.getInvoiceStatus() != InvoiceStatus.SysDeleted ){
				if( medProfileItemList.isEmpty() ){
					mpPoi = em.find(MedProfilePoItem.class, purchaseRequest.getMpPharmInfo().getMedProfilePoItemId());
					if( mpPoi != null && mpPoi.getMedProfileMoItem().getStatus() == MedProfileMoItemStatus.SysDeleted ){
						if( purchaseRequest.getInvoiceStatus() != InvoiceStatus.Void ){
							mpPoi = null;
						}
					}
				}else{
					mpPoi = poItemUuidHashMap.get(purchaseRequest.getMpPharmInfo().getMedProfilePoItemUuid());
					if( mpPoi == null && purchaseRequest.getMpPharmInfo().getMedProfilePoItemId() != null ){
						mpPoi = em.find(MedProfilePoItem.class, purchaseRequest.getMpPharmInfo().getMedProfilePoItemId());
						if( mpPoi != null && mpPoi.getMedProfileMoItem().getStatus() == MedProfileMoItemStatus.SysDeleted ){
							if( purchaseRequest.getInvoiceStatus() != InvoiceStatus.Void ){
								mpPoi = null;
							}
						}
					}
				}
			}
			
			if( mpPoi == null ){
				if(purchaseRequest.getId() != null){
					purchaseRequest.setMarkDelete(true);
					purchaseRequest.setInvoiceStatus(InvoiceStatus.SysDeleted);
					relinkedPrList.add(purchaseRequest);
				}
			}else{
				purchaseRequest.setMedProfile(medProfile);
				purchaseRequest.setMedProfilePoItem(mpPoi);
				relinkedPrList.add(purchaseRequest);
			}
		}
    	return relinkedPrList;
    }
    
    
    @SuppressWarnings("unchecked")
	private Pair<List<PurchaseRequest>,List<PurchaseRequest>> updatePurchaseRequestListForIpmoe(Context context, MedProfile medProfile, List<PurchaseRequest> purchaseRequestList, List<MedProfileItem> medProfileItemList){
    	if( purchaseRequestList != null ){
    		logger.info("updatePurchaseRequestListForIPMOE - purchaseRequestList() | #0", purchaseRequestList.size());
    	}
    	
    	if (purchaseRequestList == null || purchaseRequestList.size() == 0) {
			return new Pair(null,null);
		}    	
    	
		MedProfile medProfileInDB = em.find(MedProfile.class, medProfile.getId());
		
		List<PurchaseRequest> voidedPurchaseRequestList = null;
		List<PurchaseRequest> newInvoicePurchaseRequestList = null;
		List<PurchaseRequest> relinkedPrList = relinkPurchaseRequestList(context, medProfileInDB, purchaseRequestList, medProfileItemList);
		
		for (PurchaseRequest purchaseRequest : relinkedPrList) {
			if ( purchaseRequest.getId() == null ) {
				addPurchaseRequest(context, purchaseRequest);
			}else if( purchaseRequest.getMarkDelete() ){
				if( purchaseRequest.getInvoiceDate() != null ){
					voidPurchaseRequest(context,purchaseRequest);
					
					if( voidedPurchaseRequestList == null ){
						voidedPurchaseRequestList = new ArrayList<PurchaseRequest>();
					}
					voidedPurchaseRequestList.add(purchaseRequest);
				}else{
					deletePurchaseRequest(purchaseRequest);
				}
			}else if( purchaseRequest.getId() != null ){
				logger.info("updatePurchaseRequestListForIpmoe of old verified HA purchase request:#0, #1", purchaseRequest.getId(), purchaseRequest.getInvoiceStatus());
				updatePurchaseRequest(context, purchaseRequest);
			}
			
			if( !purchaseRequest.getMarkDelete() &&
				purchaseRequest.getInvoiceDate() == null &&
				!purchaseRequest.getDirectPrintFlag() &&
				MedProfileItemStatus.Verified == purchaseRequest.getMedProfilePoItem().getMedProfileMoItem().getMedProfileItem().getStatus()){
				
				if( newInvoicePurchaseRequestList == null ){
					newInvoicePurchaseRequestList = new ArrayList<PurchaseRequest>();
				}
				newInvoicePurchaseRequestList.add(purchaseRequest);
			}
		}
		return new Pair(newInvoicePurchaseRequestList, voidedPurchaseRequestList);
    }
    
    @SuppressWarnings("unchecked")
	private Pair<List<PurchaseRequest>,List<PurchaseRequest>> updatePurchaseRequestList(Context context, MedProfile medProfile, List<PurchaseRequest> purchaseRequestList){
    	if (purchaseRequestList == null) {
			return new Pair(null,null);
		}    	
    	logger.info("updatePurchaseRequestList - purchaseRequestList() | #0", purchaseRequestList.size());
		
		if( !purchaseRequestList.isEmpty() ){
			medProfileManager.saveChargeOrderNum(medProfile);
			em.flush();
		}
		
		MedProfile medProfileInDB = em.find(MedProfile.class, medProfile.getId());
		
		List<PurchaseRequest> voidedPurchaseRequestList = null;
		List<PurchaseRequest> newInvoicePurchaseRequestList = null;
		
		for (PurchaseRequest purchaseRequest : purchaseRequestList) {
			if( purchaseRequest.getMarkDelete() ){
				if( purchaseRequest.getInvoiceDate() != null ){
					purchaseRequest.setMedProfile(medProfileInDB);
					voidPurchaseRequest(context,purchaseRequest);
					
					if( voidedPurchaseRequestList == null ){
						voidedPurchaseRequestList = new ArrayList<PurchaseRequest>();
					}
					voidedPurchaseRequestList.add(purchaseRequest);
				}else{
					deletePurchaseRequest(purchaseRequest);
				}
				continue;
			}else if ( purchaseRequest.getId() == null ) {
				MedProfilePoItem mppoi = em.find(MedProfilePoItem.class, purchaseRequest.getMedProfilePoItem().getId());				
				purchaseRequest.setMedProfilePoItem(mppoi);
				purchaseRequest.setMedProfile(medProfileInDB);
				addPurchaseRequest(context,purchaseRequest);
			}else if( purchaseRequest.getId() != null ){
				logger.info("updatePurchaseRequestListForNonIpmoe of old verified HA purchase request:#0, #1", purchaseRequest.getId(), purchaseRequest.getInvoiceStatus());
				MedProfilePoItem mppoi = em.find(MedProfilePoItem.class, purchaseRequest.getMedProfilePoItem().getId());				
				purchaseRequest.setMedProfilePoItem(mppoi);
				purchaseRequest.setMedProfile(medProfileInDB);
				updatePurchaseRequest(context,purchaseRequest);
			}
			
			if( !purchaseRequest.getMarkDelete() &&
				purchaseRequest.getInvoiceDate() == null &&
				!purchaseRequest.getDirectPrintFlag() &&
				MedProfileItemStatus.Verified == purchaseRequest.getMedProfilePoItem().getMedProfileMoItem().getMedProfileItem().getStatus()){
				
				if( newInvoicePurchaseRequestList == null ){
					newInvoicePurchaseRequestList = new ArrayList<PurchaseRequest>();
				}
				newInvoicePurchaseRequestList.add(purchaseRequest);
			}
		}
		return new Pair(newInvoicePurchaseRequestList, voidedPurchaseRequestList);
    }
    
	private void deletePurchaseRequest( PurchaseRequest purchaseRequestIn ) {
		if (purchaseRequestIn.getId() != null) {
			PurchaseRequest purchaseRequestInDb = em.find(PurchaseRequest.class, purchaseRequestIn.getId());
			purchaseRequestInDb.setInvoiceStatus(InvoiceStatus.SysDeleted);
		}
	}

	private void addPurchaseRequest(Context context, PurchaseRequest purchaseRequestIn ) {
		purchaseRequestIn.setWorkstore(workstore);
		if( purchaseRequestIn.getInvoiceStatus() == null ){
			purchaseRequestIn.setInvoiceStatus(InvoiceStatus.None);
		}
		em.persist( purchaseRequestIn );
		em.flush();
	}
	
	private void updatePurchaseRequest(Context context, PurchaseRequest purchaseRequestIn ){
		em.merge( purchaseRequestIn );	
		em.flush();
	}
	
	private void voidPurchaseRequest(Context context, PurchaseRequest purchaseRequestIn ) {
		purchaseRequestIn.setInvoiceStatus(InvoiceStatus.Void);
		em.merge( purchaseRequestIn );	
		em.flush();
	}
    
	private void updateReplenishmentList(Context context, MedProfile medProfile, List<Replenishment> replenishmentList) {
		
		if (replenishmentList == null) {
			return;
		}
		
		logger.info("updateReplenishmentList - replenishmentList.size() | #0", replenishmentList.size());
		
		MedProfile medProfileInDB = em.find(MedProfile.class, medProfile.getId());
		
		MedProfileMoItem replenishmentMoi;
		for (Replenishment replenishment : replenishmentList) {
			
			replenishment.setMedProfile(medProfileInDB);
			replenishmentMoi = replenishment.getMedProfileMoItem();
			
			//Auto process of replenishment request for simple/complex infusion order
			if (replenishmentMoi.getRxItem().isIvRxDrug()
					&& (((IvRxDrug)replenishmentMoi.getRxItem()).getIvAdditiveList() == null || ((IvRxDrug)replenishmentMoi.getRxItem()).getIvAdditiveList().size() == 0)
					&& (replenishmentMoi.getMedProfilePoItemList() == null || replenishmentMoi.getMedProfilePoItemList().size() == 0)) {
				if (replenishment.getStatus() == null || replenishment.getStatus() == ReplenishmentStatus.Outstanding) {
					replenishment.setStatus(ReplenishmentStatus.Processed);
					replenishment.setReplenishmentItemList(null);					
					em.merge(replenishment);
					try{
						replenishment.getMedProfileMoItem().getMedProfileItem().setMedProfile(medProfileInDB);
						insertTrxLogAndSendMessageToCorp(context, MpTrxType.MpDrugReplenishmentUpdate, Arrays.asList(replenishment));
					}catch(Exception e){
						logger.error(e.getMessage(),e);
					}
					logger.debug("Auto process of replenishment request for simple/complex infusion order");
				}
			} else {
			
				MedProfileItem medProfileItemInDB = em.find(MedProfileItem.class, replenishmentMoi.getMedProfileItem().getId());
				if (medProfileItemInDB == null) {
					throw new IllegalArgumentException("MedProfileItem (" + replenishmentMoi.getMedProfileItem().getId() + " not exists");
				}
				
				if ( replenishment.getId() == null ) {		
					//Reset replenishment
					logger.info("Reset replenishment:#0", replenishment.getId());
					addReplenishment(context, replenishment);
					deleteReplenishment(replenishment);
				} else {
	
					if ( Boolean.TRUE.equals(replenishment.getDispFlag()) &&
							replenishment.getStatus() == ReplenishmentStatus.Outstanding ) {
						//Process replenishment
						updateReplenishment(context, medProfileInDB, replenishment);
						logger.info("Process replenishment:#0", replenishment.getId());
					} else {			
						
						Replenishment replenishmentInDB = em.find(Replenishment.class, replenishment.getId());
						
						logger.info("replenishment.getMedProfileMoItem().getId():#0, " +
								"medProfileItemInDB.getMedProfileMoItem().getId():#1, " +
								"replenishmentInDB.getMedProfileMoItem().getId():#2", replenishment.getMedProfileMoItem().getId(), 
								medProfileItemInDB.getMedProfileMoItem().getId(), replenishmentInDB.getMedProfileMoItem().getId());
						
						if (replenishment.getStatus() == ReplenishmentStatus.Processed) {
							logger.info("Ignore replenishment:#0, status:#1", replenishment.getId(), replenishment.getStatus().getDisplayValue());
						} else {
							if (!medProfileItemInDB.getMedProfileMoItem().getItemNum().equals(replenishmentMoi.getItemNum())) {
								replenishmentInDB.setStatus(ReplenishmentStatus.SysDeleted);
								logger.info("Mark system delete to replenishment:#0, status:#1 where the related medProfileMoItem is modified by CMS", replenishment.getId(), replenishment.getStatus().getDisplayValue());
							} else {
								updateReplenishmentToLatestMoiRef(replenishment, medProfileItemInDB);
							}
						}
					}
				}
			}
		}
		em.flush();
	}

	private void deleteReplenishment( Replenishment replenishment ) {
		if (replenishment.getOrgId() != null) {
			Replenishment replenishmentInDb = em.find(Replenishment.class, replenishment.getOrgId());
			replenishmentInDb.setStatus( ReplenishmentStatus.SysDeleted );					
		}
	}
	
	private void addReplenishment(Context context, Replenishment replenishmentIn ) {
		MedProfileItem medProfileItemInDB = em.find(MedProfileItem.class, replenishmentIn.getMedProfileMoItem().getMedProfileItem().getId());
		
		Map<Long,MedProfilePoItem> idToPoiMap = new HashMap<Long,MedProfilePoItem>();
		Date dueDate = null;
		
		for (MedProfilePoItem poi:medProfileItemInDB.getMedProfileMoItem().getMedProfilePoItemList()) {
			idToPoiMap.put(poi.getId(), poi);
		}
		replenishmentIn.setMedProfileMoItem(medProfileItemInDB.getMedProfileMoItem());
		replenishmentIn.setMedProfile(medProfileItemInDB.getMedProfile());
		for (ReplenishmentItem ri:replenishmentIn.getReplenishmentItemList()) {
			ri.setMedProfilePoItem(idToPoiMap.get(ri.getMedProfilePoItem().getId()));
			ri.setReplenishment(replenishmentIn);
			ri.setId(null);
			ri.setPrintDate(null);
		}
		
		if ( replenishmentIn.getStatus() == ReplenishmentStatus.Outstanding && Boolean.TRUE.equals(replenishmentIn.getDispFlag()) ) {
			dueDate = calculateReplenishmentDueDate(medProfileItemInDB.getMedProfile());
			replenishmentIn.setStatus( ReplenishmentStatus.Processed );
			for ( ReplenishmentItem replenishmentItem : replenishmentIn.getReplenishmentItemList() ) {
				replenishmentItem.setDueDate(dueDate);			
			}
			//send cms message 
			List<Replenishment> replenishmentList = new ArrayList<Replenishment>();
			replenishmentList.add(replenishmentIn);
			try{
				insertTrxLogAndSendMessageToCorp(context, MpTrxType.MpDrugReplenishmentUpdate,replenishmentList);
			}catch(Exception e){
				logger.error(e);
			}
		}			
		
		em.persist( replenishmentIn );	
		em.flush();
	}
	
	private void updateReplenishment(Context context,MedProfile medProfile, Replenishment replenishmentIn) {
		
		replenishmentIn.setStatus(ReplenishmentStatus.Processed);
		replenishmentIn.setMedProfile(medProfile);
		Date dueDate = calculateReplenishmentDueDate(medProfile);

		for (ReplenishmentItem replenishmentItem : replenishmentIn.getReplenishmentItemList()) {
			replenishmentItem.setReplenishment(replenishmentIn);
			if (replenishmentItem.getDueDate() == null) {
				replenishmentItem.setDueDate(dueDate);
			}
		}
		
		replenishmentIn = em.merge(replenishmentIn);	
		
		//send cms message 
		List<Replenishment> replenishmentList = new ArrayList<Replenishment>();
		replenishmentList.add(replenishmentIn);
		
		MedProfile mp =	replenishmentIn.getMedProfileMoItem().getMedProfileItem().getMedProfile();
		mp.getPatient();
		mp.getMedCase();
		
		try{
			insertTrxLogAndSendMessageToCorp(context , MpTrxType.MpDrugReplenishmentUpdate,replenishmentList);
		}catch(Exception e){
			logger.error(e);
		}
	}
	
	private Date calculateReplenishmentDueDate(MedProfile medProfile) {
		
		Calendar dueCalendar = Calendar.getInstance();
		dueCalendar.add(Calendar.MINUTE, 
				MEDPROFILE_ITEM_ADMINTIME_BUFFER.get(medProfile.getWorkstore(), 0));
		return dueCalendar.getTime();
	}
	
	private void detach(Object obj) {
		((EntityManagerImpl) em.getDelegate()).detach(obj);
	}	

	@Remove
	@Override
	public void destroy() {
		if (medProfile != null) {
			medProfileOrderProcessor.releaseProcessingMedProfile(medProfile.getId(), null, workstation.getId(), false);
		}
		if (chemoMedProfileList != null) {
			chemoMedProfileList = null;
		}
		if (manualEntryInfo != null) {
			manualEntryInfo = null;			
		}
		if (replenishmentList != null) {
			replenishmentList = null;
		}
	}
	
	protected static class Context {
		
		private Boolean mdsCheckFlag;
		
		private Date date;
		
		private Map<MpTrxType, List<MedProfileMoItem>> messageRequestMap = new TreeMap<MpTrxType, List<MedProfileMoItem>>(MP_TRX_TYPE_COMPARATOR);
		
		private List<Replenishment> replenishmentRequestUpdateList;
		
		public Context(MedProfile medProfile) {
			mdsCheckFlag = medProfile.getMdsCheckFlag();
		}

		public Date getDate() {
			if (date == null)
				date = new Date();
			return date;
		}
		
		public void addMessageRequest(MpTrxType type, MedProfileMoItem medProfileMoItem) {
			
			List<MedProfileMoItem> list = messageRequestMap.get(type);
			if (list == null) {
				list = new ArrayList<MedProfileMoItem>();
				messageRequestMap.put(type, list);
			}
			list.add(medProfileMoItem);
		}
		
		public Map<MpTrxType, List<MedProfileMoItem>> getMessageRequestMap() {
			return messageRequestMap;
		}
		
		public Date calculateVerifyDate(Integer verifyIndex) {
			Date date = getDate();
			return new Date(date.getTime() + (verifyIndex == null ? 0 : verifyIndex.intValue()) * 1000L);
		}
		
		public void restoreState(MedProfile medProfile) {
			medProfile.setMdsCheckFlag(mdsCheckFlag);
		}

		public List<Replenishment> getReplenishmentRequestUpdateList() {
			
			if(replenishmentRequestUpdateList == null){
				replenishmentRequestUpdateList = new ArrayList<Replenishment>();
			}
			return replenishmentRequestUpdateList;
		}

		public void setReplenishmentRequestUpdateList(
				List<Replenishment> replenishmentRequestUpdateList) {
			this.replenishmentRequestUpdateList = replenishmentRequestUpdateList;
		}
		
		public void validate() {
			
			for (List<MedProfileMoItem> medProfileMoItemList: getMessageRequestMap().values()) {
				for (MedProfileMoItem medProfileMoItem: medProfileMoItemList) {
					validate(medProfileMoItem);
				}
			}
			
			for (Replenishment replenishment: getReplenishmentRequestUpdateList()) {
				validate(replenishment);
			}
		}
		
		private void validate(MedProfileMoItem medProfileMoItem) {
			if (medProfileMoItem == null || medProfileMoItem.getMedProfileItem() == null) {
				throw new IllegalStateException();
			}
			validate(medProfileMoItem.getMedProfileItem().getMedProfile());
		}
		
		private void validate(Replenishment replenishment) {
			validate(replenishment.getMedProfileMoItem());
		}
		
		private void validate(MedProfile medProfile) {
			if (medProfile == null || medProfile.getWorkstore() == null || medProfile.getWorkstore().getHospital() == null) {
				throw new IllegalStateException();
			}
		}
	}
	
	private static class MpTrxTypeComparator implements Comparator<MpTrxType> {
		
		@Override
		public int compare(MpTrxType o1, MpTrxType o2) {
			
			if (o1 == o2) {
				return 0;
			} else if (o1 == MpTrxType.MpUpdateDispensing) {
				return -1;
			} else if (o2 == MpTrxType.MpUpdateDispensing) {
				return 1;
			} else {
				return o1.compareTo(o2);
			}
		}
	}
	
	private static class ReplenishmentItemComparator implements Comparator<ReplenishmentItem> {

		@Override
		public int compare(ReplenishmentItem o1, ReplenishmentItem o2) {
			return o1.getId().compareTo(o2.getId());
		}
	}
	
	private void insertTrxLogAndSendMessageToCorp(Context context, MpTrxType mpTrxType,List<Replenishment> replenishmentList ){
		
			Set<Long> medProfileIdList = new HashSet<Long>();	
			List<String> refNumList = new ArrayList<String>();	
			for (Replenishment rr :replenishmentList){
				medProfileIdList.add(rr.getMedProfileMoItem().getMedProfileItem().getMedProfile().getId());
				refNumList.add(MedProfileUtils.join(rr.getMedProfileMoItem().getItemNum(), rr.getReplenishNum()));
			}
			
			//same mp in replenishment
			MedProfile mp = replenishmentList.get(0).getMedProfileMoItem().getMedProfileItem().getMedProfile();
			
			Set<String> duiplicatedRefNumSet = medProfileManager.retrieveSentRefNumSet(mpTrxType, mp, refNumList);
			
			// insert all the trx log
			List<Replenishment> rrListToSent = new ArrayList<Replenishment>();
			rrListToSent.addAll(replenishmentList);
			for (Replenishment rr :replenishmentList){
				String refNum = MedProfileUtils.join(rr.getMedProfileMoItem().getItemNum(), rr.getReplenishNum());
				
				if(!duiplicatedRefNumSet.contains(refNum)){
					MedProfileTrxLog mptLog = new MedProfileTrxLog();
					mptLog.setRefNum(refNum);
					mptLog.setMedProfile(rr.getMedProfileMoItem().getMedProfileItem().getMedProfile());
					mptLog.setType(mpTrxType);
					em.persist(mptLog);
				}else{
					rrListToSent.remove(rr);
				}
			}			
			em.flush();
			
			context.getReplenishmentRequestUpdateList().addAll(rrListToSent);
			
	}
	
	private void setMedProfileAtdpsWardFlag(MedProfile medProfile){
		WardConfig wardConfig = wardConfigManager.retrieveWardConfig(workstore.getWorkstoreGroup(), medProfile.getWardCode());
		if ( wardConfig != null ) {
			medProfile.setAtdpsWardFlag(wardConfig.getAtdpsWardFlag());
		} else {
			medProfile.setAtdpsWardFlag(false);
		}
	}
	
	private void setMedProfilePoItemUnitDoseFlag(MedProfileMoItem medProfileMoItem){
		for (MedProfilePoItem medProfilePoItem : medProfileMoItem.getMedProfilePoItemList()) {
			medProfilePoItem.setUnitDoseFlag(medProfileItemConverter.isUnitDose(workstore, medProfile, medProfileMoItem, medProfilePoItem, atdpsPickStationsNumList));
		}
	}
	
	private void setMedProfilePoItemUnitDoseItem(MedProfileMoItem medProfileMoItem){
		for (MedProfilePoItem medProfilePoItem : medProfileMoItem.getMedProfilePoItemList()) {
			List<ItemLocation> itemLocationList = itemLocationListManager.retrieveItemLocationList(medProfilePoItem.getItemCode(),
																										atdpsPickStationsNumList, 
																										Arrays.asList(MachineType.ATDPS));
			medProfilePoItem.setUnitDoseItem(!itemLocationList.isEmpty());
		}
	}
    
	private void retrieveEhrPatient(MedProfile medProfile){
    	if ( medProfile.getPatient() == null ) {
    		return;
    	}
		medProfile.setEhrPatient(pasServiceWrapper.retrieveEhrPatient(medProfile.getPatHospCode(), 
																					medProfile.getPatient().getPatKey(), 
																					medProfile.getPatient().getHkid()));
    }
}
