package hk.org.ha.model.pms.biz.charging;

import static hk.org.ha.model.pms.prop.Prop.CHARGING_FCS_ENABLED;
import hk.org.ha.fmk.pms.util.QueryUtils;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.fcs.FcsException;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.PurchaseRequest;
import hk.org.ha.model.pms.udt.charging.ClearanceStatus;
import hk.org.ha.model.pms.udt.disp.InvoiceDocType;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.vo.charging.FcsDowntimePaymentInfo;
import hk.org.ha.model.pms.vo.charging.FcsPaymentStatus;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;
import hk.org.ha.service.pms.asa.interfaces.fcs.FcsServiceJmsRemote;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.joda.time.DateTime;

@AutoCreate
@Stateless
@Name("drugChargeClearanceManager")
@MeasureCalls
public class DrugChargeClearanceManagerBean implements DrugChargeClearanceManagerLocal {
		
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In
	private FcsServiceJmsRemote fcsServiceProxy;
	
	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;

	@In
	private InvoiceManagerLocal invoiceManager;
	
	private boolean isByPassFcsChecking(Object cleranceObj){
		boolean byPassFcsChecking = false;
		BigDecimal totalAmount = BigDecimal.ZERO;
		Workstore workstore;
		
		if( cleranceObj instanceof Invoice ){
			totalAmount = ((Invoice)cleranceObj).getTotalAmount();
			workstore = ((Invoice)cleranceObj).getWorkstore();
		}else if(cleranceObj instanceof PurchaseRequest){
			totalAmount = ((PurchaseRequest)cleranceObj).getTotalAmount();
			workstore = ((PurchaseRequest)cleranceObj).getWorkstore();
		}else {
			return false;
		}
		
		
		if( BigDecimal.ZERO.compareTo(totalAmount) == 0 ){
			byPassFcsChecking = true;
		}else{
			logger.debug("isByPassFcsChecking===============PropName:#0, value: #1", CHARGING_FCS_ENABLED.getName(), CHARGING_FCS_ENABLED.get(workstore, false));

			if(!CHARGING_FCS_ENABLED.get(workstore, false)){
				byPassFcsChecking = !invoiceManager.isExceptionDrugExistExcludeZeroAmount(cleranceObj);
			}else{
				byPassFcsChecking = (!invoiceManager.isExceptionDrugExistExcludeZeroAmount(cleranceObj) && invoiceManager.isExceptionDrugExist(cleranceObj));
			}
		}
		return byPassFcsChecking;

	}
	
	private FcsPaymentStatus checkSfiDrugClearance(String patHospCode, String sfiInvoiceNum, boolean updateResult) {
		FcsPaymentStatus fcsPaymentStatus = new FcsPaymentStatus();		
		
		if( !CHARGING_FCS_ENABLED.get(false)){
			fcsPaymentStatus.setInvoiceNum(sfiInvoiceNum);
			fcsPaymentStatus.setClearanceStatus(ClearanceStatus.Error);
			return fcsPaymentStatus;
		}
		
		try {
			fcsPaymentStatus = fcsServiceProxy.retrieveSfiPaymentStatus(patHospCode, sfiInvoiceNum);
			if( updateResult ){
				updateInvoiceFcsStatus( fcsPaymentStatus );
			}
			return fcsPaymentStatus;
		}catch (FcsException e) {			
			fcsPaymentStatus.setInvoiceNum(sfiInvoiceNum);
			fcsPaymentStatus.setClearanceStatus(ClearanceStatus.Error);
			return fcsPaymentStatus;
		}
	}
	
	public FcsPaymentStatus checkSfiDrugClearanceByInvoice(Invoice invoice, boolean updateResult){
		FcsPaymentStatus fcsPaymentStatus = new FcsPaymentStatus();
		fcsPaymentStatus.setInvoiceNum(invoice.getInvoiceNum());		
		
		if( isByPassFcsChecking(invoice) ){
			fcsPaymentStatus.setClearanceStatus(ClearanceStatus.PaymentClear);
			return fcsPaymentStatus;
		}
		String patHospCode = invoice.getDispOrder().getPharmOrder().getMedOrder().getPatHospCode();
		return checkSfiDrugClearance(patHospCode,invoice.getInvoiceNum(), updateResult);
	}
	
	public FcsPaymentStatus checkSfiDrugClearanceByInvoice(Invoice invoice){
		return checkSfiDrugClearanceByInvoice(invoice, true);
	}

	@SuppressWarnings("unchecked")
	private void updateInvoiceFcsStatus(FcsPaymentStatus fcsPaymentStatus){
		if( fcsPaymentStatus == null ){
			return;
		}
		
		List<Invoice> invoiceList = (List<Invoice>) em.createQuery(
				"select o from Invoice o " + // 20120214 index check : Invoice.invoiceNum : I_INVOICE_01
				"where o.invoiceNum = :invoiceNum " + 
				"and o.docType = :invoiceDocType and o.status not in :invoiceStatusList")
				.setParameter("invoiceNum", fcsPaymentStatus.getInvoiceNum())
				.setParameter("invoiceDocType", InvoiceDocType.Sfi)
				.setParameter("invoiceStatusList", Arrays.asList(InvoiceStatus.Void,InvoiceStatus.SysDeleted) )
				.getResultList();
		
		if( invoiceList.size() > 0 ){
			Invoice invoice = invoiceList.get(0);
			
			boolean requiredSave = true;
			
			if( StringUtils.equals(fcsPaymentStatus.getReceiptNum(), invoice.getReceiptNum()) ){
				requiredSave = false;
			}
			
			if( invoice.getReceiveAmount() != null && fcsPaymentStatus.getReceiptAmount() != null &&
				(new BigDecimal(fcsPaymentStatus.getReceiptAmount())).compareTo(invoice.getReceiveAmount()) == 0 ){
				requiredSave = false;
			}
			
			if( InvoiceStatus.Settled.equals(invoice.getStatus()) && ClearanceStatus.PaymentClear.equals(fcsPaymentStatus.getClearanceStatus()) ){
				requiredSave = false;
			}
			
			if( InvoiceStatus.Outstanding.equals(invoice.getStatus()) && !ClearanceStatus.PaymentClear.equals(fcsPaymentStatus.getClearanceStatus()) ){
				requiredSave = false;
			}
			
			if( requiredSave ){
				invoice.setReceiptNum(fcsPaymentStatus.getReceiptNum());
				invoice.setReceiveAmount(new BigDecimal(fcsPaymentStatus.getReceiptAmount()));
				invoice.setFcsCheckDate(new DateTime().toDate());
				
				if( ClearanceStatus.PaymentClear.equals(fcsPaymentStatus.getClearanceStatus()) ){
					invoice.setStatus(InvoiceStatus.Settled);
				}else{
					invoice.setStatus(InvoiceStatus.Outstanding);
				}
				em.merge(invoice);
			}			
		}
	}
	
	public ClearanceStatus checkStdDrugClearanceByDispOrder(DispOrder dispOrder){
		return checkStdDrugClearanceByOrder(dispOrder);
	}

	public ClearanceStatus checkStdDrugClearanceByMedOrder(MedOrder medOrder){
		return checkStdDrugClearanceByOrder(medOrder);
	}
	
	private ClearanceStatus checkStdDrugClearanceByOrder(Object order){
		DispOrder dispOrder = null;
		MedOrder medOrder = null;
		MedCase medCase = null;
		if (order instanceof DispOrder) {
			dispOrder = (DispOrder) order;
			medOrder = dispOrder.getPharmOrder().getMedOrder();
			medCase = dispOrder.getPharmOrder().getMedCase();
		} else {
			medOrder = (MedOrder) order;
			medCase = medOrder.getMedCase();
		}
		
		String patHospCode = medOrder.getPatHospCode();
		Long moeOrderNum = null;
		Long pasApptSeq = null;
		
		if( medCase != null ){
			if( !medCase.getChargeFlag() ){
				return ClearanceStatus.PaymentClear;
			}
			pasApptSeq = medCase.getPasApptSeq();	
		}
		
		if( MedOrderDocType.Normal == medOrder.getDocType() && medOrder.getOrderNum() != null && medOrder.getOrderNum().length() > 4 ){
			try {
				moeOrderNum = Long.parseLong(medOrder.getOrderNum().substring(3));
			}catch ( NumberFormatException e ){
				moeOrderNum = null;
			}
		}
		return checkStdDrugClearance(patHospCode, moeOrderNum, pasApptSeq);
	}
	
	public ClearanceStatus checkStdDrugClearance(String patHospCode, Long orderNumIn, Long pasApptSeqIn ){
		if( !CHARGING_FCS_ENABLED.get(false)){
			return ClearanceStatus.Error;
		}
		
		Long orderNum = orderNumIn;
		Long pasApptSeq = pasApptSeqIn;
		
		if( orderNum == null ){
			orderNum = Long.valueOf(-1);
		}
		
		if( pasApptSeq == null ){
			pasApptSeq = Long.valueOf(-1);
		}
		
		try {
			return fcsServiceProxy.retrieveStdDrugPaymentStatus(patHospCode, orderNum, pasApptSeq);							
		}catch (FcsException e) {			
			return ClearanceStatus.Error;
		}
	}
	
	public Map<String,FcsPaymentStatus> checkSfiClearance(List<Invoice> invoiceList, List<PurchaseRequest> purchaseRequestList){
		List<Invoice> checkFcsInvoiceList = new ArrayList<Invoice>();
		Map<String, FcsPaymentStatus> fcsPaymentMap = new HashMap<String, FcsPaymentStatus>();
		Workstore workstore = null;
		
		if ( invoiceList != null ) {
			for( Invoice invoice : invoiceList ){
				if( workstore == null ){
					workstore = invoice.getWorkstore();
				}
				if( isByPassFcsChecking(invoice) ){
					FcsPaymentStatus byPassFcsPaymentStatus = new FcsPaymentStatus();
					byPassFcsPaymentStatus.setInvoiceNum(invoice.getInvoiceNum());
					byPassFcsPaymentStatus.setClearanceStatus(ClearanceStatus.PaymentClear);
					fcsPaymentMap.put(invoice.getInvoiceNum(), byPassFcsPaymentStatus);
				}else{
					if ( StringUtils.isBlank(invoice.getPatHospCode()) ) {
						invoice.setPatHospCode(invoice.getDispOrder().getPharmOrder().getMedOrder().getPatHospCode());	
					}
					checkFcsInvoiceList.add(invoice);
				}
			}
		}
		
		if ( purchaseRequestList != null ) {
			for( PurchaseRequest pr : purchaseRequestList ){
				if( workstore == null ){
					workstore = pr.getWorkstore();
				}
				
				if( isByPassFcsChecking(pr) ){
					FcsPaymentStatus byPassFcsPaymentStatus = new FcsPaymentStatus();
					byPassFcsPaymentStatus.setInvoiceNum(pr.getInvoiceNum());
					byPassFcsPaymentStatus.setClearanceStatus(ClearanceStatus.PaymentClear);
					fcsPaymentMap.put(pr.getInvoiceNum(), byPassFcsPaymentStatus);
				}else{
					Invoice checkInvoice = new Invoice();
					checkInvoice.setInvoiceNum(pr.getInvoiceNum());
					checkInvoice.setPatHospCode(pr.getMedProfile().getPatHospCode());
					checkFcsInvoiceList.add(checkInvoice);
				}
			}
		}
		logger.debug("checkSfiClearanceByInvoiceList============Number of invoiceInput: #0, purchaseRequestList: #1, byPassFcsInvoice #2, checkFcsInvoice #3", 
																						( invoiceList==null?"null":invoiceList.size()), 
																						(purchaseRequestList==null?"null":purchaseRequestList.size()),
																						fcsPaymentMap.size(), 
																						checkFcsInvoiceList.size());
		
		List<FcsPaymentStatus> fcsPaymentStatusList = new ArrayList<FcsPaymentStatus>();
		if( checkFcsInvoiceList.size() > 0 ){
			if( !CHARGING_FCS_ENABLED.get(workstore, false)){
				return fcsPaymentMap;
			}

			try {
				fcsPaymentStatusList.addAll(fcsServiceProxy.retrieveSfiPaymentStatusList(checkFcsInvoiceList));
				
				for(FcsPaymentStatus fcsPaymentStatus : fcsPaymentStatusList){
					fcsPaymentMap.put(fcsPaymentStatus.getInvoiceNum(), fcsPaymentStatus);
				}
			}catch (FcsException e) {

			}
		}
		
		logger.debug("checkSfiClearanceByInvoiceList============Number of invoice return: #0", fcsPaymentStatusList.size());
		
		return fcsPaymentMap;
	}
	
	public FcsDowntimePaymentInfo retrieveDowntimePaymentStatus(	String patHospCode, String invoiceNumIn, 
																String hkid, String caseNum,
																String searchType, String searchText){
		if( !CHARGING_FCS_ENABLED.get(false)){
			return new FcsDowntimePaymentInfo();
		}
		
		try {
			String dispHospCode = invoiceNumIn.substring(1,4);
			Long invoiceYear = null;
			Long invoiceNum = null;
			
			invoiceYear = Long.valueOf(invoiceNumIn.substring(4,6));
			invoiceNum = Long.valueOf(invoiceNumIn.substring(6));
			
			return fcsServiceProxy.retrieveDowntimePaymentInfo(patHospCode, dispHospCode, invoiceNum, invoiceYear, hkid, caseNum, searchType, searchText);							
		}catch ( FcsException e){
			return new FcsDowntimePaymentInfo();
		}
	}
	
    @Override
    public void updateDrugChargeClearance(List<PurchaseRequest> purchaseRequestList) {
    	
    	purchaseRequestList = buildClearanceCheckList(purchaseRequestList, true);
    	Map<String, FcsPaymentStatus> paymentStatusMap = checkSfiClearance(null, purchaseRequestList);
    	purchaseRequestList = buildPaidList(paymentStatusMap, purchaseRequestList);
    	purchaseRequestList = fetchPurchaseRequestListForUpdate(purchaseRequestList);
    	updatePaymentStatus(paymentStatusMap, purchaseRequestList, false);
    }
    
    @Override
    public List<PurchaseRequest> buildClearanceCheckList(List<PurchaseRequest> purchaseRequestList, boolean checkProfileCurrentProcessing) {
    	
    	List<PurchaseRequest> clearanceCheckList = new ArrayList<PurchaseRequest>();
    	for (PurchaseRequest purchaseRequest: purchaseRequestList) {
    		MedProfile medProfile = purchaseRequest.getMedProfilePoItem().getMedProfileMoItem().getMedProfileItem().getMedProfile();
    		if ((!checkProfileCurrentProcessing || 
    				(!Boolean.TRUE.equals(medProfile.getProcessingFlag()) && !Boolean.TRUE.equals(medProfile.getDeliveryGenFlag()))) &&
    				purchaseRequest.clearanceCheckRequired()) {
    			clearanceCheckList.add(purchaseRequest);
    		}
    	}
    	return clearanceCheckList;
    }
    
    private List<PurchaseRequest> buildPaidList(Map<String, FcsPaymentStatus> paymentStatusMap, List<PurchaseRequest> purchaseRequestList)
    {
    	List<PurchaseRequest> paidList = new ArrayList<PurchaseRequest>();
    	for (PurchaseRequest purchaseRequest: purchaseRequestList) {
    		FcsPaymentStatus paymentStatus = paymentStatusMap.get(purchaseRequest.getInvoiceNum());
    		if (paymentStatus != null && paymentStatus.getClearanceStatus() == ClearanceStatus.PaymentClear) {
    			paidList.add(purchaseRequest);
    		}
    	}
    	return paidList;
    }
    
    @SuppressWarnings("unchecked")
	private List<PurchaseRequest> fetchPurchaseRequestListForUpdate(List<PurchaseRequest> purchaseRequestList) {
    	
    	Set<Long> medProfileIdSet = new TreeSet<Long>();
    	for (PurchaseRequest purchaseRequest: purchaseRequestList) {
    		medProfileIdSet.add(purchaseRequest.getMedProfilePoItem().getMedProfileMoItem().getMedProfileItem().getMedProfile().getId());
    	}
    	
    	medProfileIdSet = new TreeSet<Long>(QueryUtils.splitExecute(em.createQuery(
    			"select o.id from MedProfile o" + // 20150420 index check : MedProfile.id : PK_MED_PROFILE
    			" where o.id in :idSet" +
    			" and o.processingFlag = false and o.deliveryGenFlag = false")
    			.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock),
    			"idSet", medProfileIdSet));

    	Set<Long> purchaseRequestIdSet = new TreeSet<Long>();
    	for (PurchaseRequest purchaseRequest: purchaseRequestList) {
    		if (medProfileIdSet.contains(purchaseRequest.getMedProfilePoItem().getMedProfileMoItem().getMedProfileItem().getMedProfile().getId())) {
    			purchaseRequestIdSet.add(purchaseRequest.getId());
    		}
    	}
    	
    	List<PurchaseRequest> purchaseRequestListForUpdate = QueryUtils.splitExecute(em.createQuery(
    			"select o from PurchaseRequest o" + // 20150420 index check : PurchaseRequest.id : PK_PURCHASE_REQUEST
    			" where o.id in :idSet" +
    			" and o.invoiceStatus = :outstandingInvoiceStatus")
    			.setParameter("outstandingInvoiceStatus", InvoiceStatus.Outstanding),
    			"idSet", purchaseRequestIdSet);
    	
    	return purchaseRequestListForUpdate;
    }
    
    @Override
    public void updatePaymentStatus(Map<String, FcsPaymentStatus> paymentStatusMap, List<PurchaseRequest> purchaseRequestList, Boolean updateCorpInvoice){
    	Date now = new Date();
    	Map<String, FcsPaymentStatus> updatePaymentInvoiceMap = new HashMap<String, FcsPaymentStatus>();
    	
    	for (PurchaseRequest purchaseRequest: purchaseRequestList) {
    		FcsPaymentStatus paymentStatus = paymentStatusMap.get(purchaseRequest.getInvoiceNum());
    		if (paymentStatus == null || paymentStatus.getClearanceStatus() != ClearanceStatus.PaymentClear
    				|| purchaseRequest.getInvoiceStatus() != InvoiceStatus.Outstanding) {
    			continue;
    		}
    		
    		try {
    			BigDecimal receiveAmount = new BigDecimal(paymentStatus.getReceiptAmount());
    		
				purchaseRequest.setInvoiceStatus(InvoiceStatus.Settled);
				purchaseRequest.setReceiptNum(paymentStatus.getReceiptNum());
				purchaseRequest.setReceiveAmount(receiveAmount);
				purchaseRequest.setFcsCheckDate(now);
				if (purchaseRequest.getDueDate() == null) {
					purchaseRequest.setDueDate(purchaseRequest.getFcsCheckDate());
				}
				
				if( updateCorpInvoice ){
					updatePaymentInvoiceMap.put(purchaseRequest.getInvoiceNum(), paymentStatus);
	    		}
				
    		} catch (NumberFormatException ex) {
    			logger.error("Error parsing receiptAmount(#0) returned from FCS for PurchaseRequest(invoiceNum=#1)",
    					paymentStatus.getReceiptAmount(), purchaseRequest.getInvoiceNum());
    		}
    	}
    	
    	if( updateCorpInvoice && updatePaymentInvoiceMap.size() > 0 ){
    		corpPmsServiceProxy.updateInvoicePaymentStatus(new ArrayList<Long>(), updatePaymentInvoiceMap);
    	}
    }
}
