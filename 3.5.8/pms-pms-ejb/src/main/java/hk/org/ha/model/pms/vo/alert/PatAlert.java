package hk.org.ha.model.pms.vo.alert;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class PatAlert implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement
	private String alertDesc;
	
	@XmlElement
	private String additionalInfo;
	
	@XmlElement
	private Date validFromDate;
	
	@XmlElement
	private Date validToDate;
	
	@XmlElement
	private Boolean classifiedFlag;
	
	@XmlElement
	private Date updateDate;
	
	@XmlElement
	private String updateUser;
	
	@XmlElement
	private String updateUserRank;
	
	@XmlElement
	private String hospCode;
	
	@XmlElement
	private String textColor;

	public String getAlertDesc() {
		return alertDesc;
	}

	public void setAlertDesc(String alertDesc) {
		this.alertDesc = alertDesc;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public Date getValidFromDate() {
		if (validFromDate == null) {
			return null;
		}
		else {
			return new Date(validFromDate.getTime());
		}
	}

	public void setValidFromDate(Date validFromDate) {
		if (validFromDate == null) {
			this.validFromDate = null;
		}
		else {
			this.validFromDate = new Date(validFromDate.getTime());
		}
	}

	public Date getValidToDate() {
		if (validToDate == null) {
			return null;
		}
		else {
			return new Date(validToDate.getTime());
		}
	}

	public void setValidToDate(Date validToDate) {
		if (validToDate == null) {
			this.validToDate = null;
		}
		else {
			this.validToDate = new Date(validToDate.getTime());
		}
	}

	public Boolean getClassifiedFlag() {
		return classifiedFlag;
	}

	public void setClassifiedFlag(Boolean classifiedFlag) {
		this.classifiedFlag = classifiedFlag;
	}

	public Date getUpdateDate() {
		if (updateDate == null) {
			return null;
		}
		else {
			return new Date(updateDate.getTime());
		}
	}

	public void setUpdateDate(Date updateDate) {
		if (updateDate == null) {
			this.updateDate = null;
		}
		else {
			this.updateDate = new Date(updateDate.getTime());
		}
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getUpdateUserRank() {
		return updateUserRank;
	}

	public void setUpdateUserRank(String updateUserRank) {
		this.updateUserRank = updateUserRank;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getTextColor() {
		return textColor;
	}

	public void setTextColor(String textColor) {
		this.textColor = textColor;
	}
}
