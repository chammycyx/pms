package hk.org.ha.model.pms.biz.security;

import javax.ejb.Local;

@Local
public interface ScreenLockServiceLocal {
	
	void validateIdleUser(String userName, String password);
	
	void setErrMsg(String errMsg);
	
	String getErrMsg();
	
	int syncLockScreenSessionTimeout();
	
	int getSessionTimeout();
	
	void destroy();
}
