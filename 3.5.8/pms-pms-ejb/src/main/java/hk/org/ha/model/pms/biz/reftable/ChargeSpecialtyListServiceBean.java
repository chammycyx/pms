package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.ChargeSpecialty;
import hk.org.ha.model.pms.udt.reftable.ChargeSpecialtyType;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("chargeSpecialtyListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ChargeSpecialtyListServiceBean implements ChargeSpecialtyListServiceLocal {

	@PersistenceContext
	private EntityManager em;
		
	@Out(required = false)
	private List<ChargeSpecialty> chargeSpecialtyList;
	
	@Out(required = false)
	private List<ChargeSpecialty> chargeSpecialtySnList;

	@In
	private Workstore workstore;
	
	@In
	private PrintAgentInf printAgent; 
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	Map<String, Object> parameters = new HashMap<String, Object>();
	
	
	private List<ChargeSpecialty> originalChargeSpecialtyList;
	
	private List<ChargeSpecialty> originalChargeSpecialtySnList;
	
	@SuppressWarnings("unchecked")
	public void retrieveChargeSpecialtyList() 
	{
		chargeSpecialtyList = new ArrayList<ChargeSpecialty>();
		chargeSpecialtySnList = new ArrayList<ChargeSpecialty>();
		originalChargeSpecialtyList = new ArrayList<ChargeSpecialty>();
		originalChargeSpecialtySnList = new ArrayList<ChargeSpecialty>();
		
		List<ChargeSpecialty> csList = em.createQuery(
				"select o from ChargeSpecialty o" + // 20120328 index check : ChargeSpecialty.hospital : I_CHARGE_SPECIALTY_01
				" where o.hospital = :hospital" +
				" order by o.specCode")
				.setParameter("hospital", workstore.getHospital())
				.getResultList();
		
		for (ChargeSpecialty cs : csList) {
			if (cs.getType() == ChargeSpecialtyType.Sfi) {
				chargeSpecialtyList.add(cs);
				
			}else if (cs.getType() == ChargeSpecialtyType.SafetyNet) {
				chargeSpecialtySnList.add(cs);
			}
		}
		
		originalChargeSpecialtyList.addAll(chargeSpecialtyList);
		originalChargeSpecialtySnList.addAll(chargeSpecialtySnList);
	}
	
	public void saveChargeSpecialtyList(Collection<ChargeSpecialty> newSfiChargeSpecialtyList, Collection<ChargeSpecialty> newSnChargeSpecialtyList) {
		updateChargeSpecialtyList(newSfiChargeSpecialtyList);		
		updateChargeSpecialtyList(newSnChargeSpecialtyList);
	}
	
	private void updateChargeSpecialtyList(Collection<ChargeSpecialty> newChargeSpecialtyList) {
		workstore = em.find(Workstore.class, workstore.getId());
		for (ChargeSpecialty chargeSpecialty: newChargeSpecialtyList) {
				
	    		if(chargeSpecialty.getId()==null){
	    			if(!chargeSpecialty.isMarkDelete()){
		    			chargeSpecialty.setHospital(workstore.getHospital());
		    			em.persist(chargeSpecialty);
	    			}
	    		} else {
		    			if (chargeSpecialty.isMarkDelete()){
		    				em.remove(em.merge(chargeSpecialty));
		    			} else {
		    				chargeSpecialty.setHospital(workstore.getHospital());
			    			em.merge(chargeSpecialty);
		    			}
	    		}
		    }
		em.flush();
	}
	
	public void printSpecMapSfiRpt(){
		parameters.clear();
		parameters.put("hospCode", workstore.getHospCode());	
		
		printAgent.renderAndPrint(new RenderAndPrintJob(
				"SpecMapSfiRpt", 
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				parameters, 
				originalChargeSpecialtyList));
	}
	
	public void printSpecMapSnRpt(){
		parameters.clear();
		parameters.put("hospCode", workstore.getHospCode());	
		
		printAgent.renderAndPrint(new RenderAndPrintJob(
				"SpecMapSnRpt", 
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				parameters, 
				originalChargeSpecialtySnList));
	}


	@Remove
	public void destroy() 
	{
		if (chargeSpecialtyList != null) {
			chargeSpecialtyList = null;
		}
		
		if (chargeSpecialtySnList != null) {
			chargeSpecialtySnList = null;
		}
	}
}
