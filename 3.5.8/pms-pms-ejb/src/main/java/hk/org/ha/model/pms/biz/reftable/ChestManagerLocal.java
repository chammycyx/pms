package hk.org.ha.model.pms.biz.reftable;
import java.util.List;

import hk.org.ha.model.pms.persistence.reftable.Chest;

import javax.ejb.Local;

@Local
public interface ChestManagerLocal {

	List<Chest> retrieveChestList();
	
	List<Chest> retrieveChestListByPrefixUnitDose(String prefixUnitDose);

	Chest retrieveChest(String unitDoseName);
}
