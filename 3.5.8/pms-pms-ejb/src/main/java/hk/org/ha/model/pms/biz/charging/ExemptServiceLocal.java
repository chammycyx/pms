package hk.org.ha.model.pms.biz.charging;

import hk.org.ha.model.pms.vo.charging.HandleExemptAuthInfo;

import javax.ejb.Local;

@Local
public interface ExemptServiceLocal {

	HandleExemptAuthInfo validateHandleExemptAuthInfo(HandleExemptAuthInfo inHandleExemptAuthInfo);
	
	void destroy();

}
