package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.exception.sftp.SftpCommandException;

import java.io.IOException;

import javax.ejb.Local;

import net.lingala.zip4j.exception.ZipException;

@Local
public interface HaDrugFormularyMgmtRptDnldServiceLocal {
	
	void zipFmRpt(String fileName, String password) throws IOException, SftpCommandException;
	
	void dnldFmRpt() throws ZipException, IOException;
	
	void destroy();
	
}
