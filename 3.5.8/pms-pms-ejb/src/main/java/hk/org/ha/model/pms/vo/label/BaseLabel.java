package hk.org.ha.model.pms.vo.label;

import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.printing.PrintType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.printing.PrintOption;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedProperty;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "BaseLabel")
@ExternalizedBean(type=DefaultExternalizer.class)
public abstract class BaseLabel implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private static final String SPACE = " ";
	private static final String LINE_BREAK_SEPARATOR = "^";
	private static final String NON_BREAK_TEXT = "<1234567890>.";
	
	@XmlElement(required = true)
	private String itemCode;

	@XmlElement(required = true)
	private String itemDesc;
 
	@XmlElement(name="Instruction", required = true)	
	private List<Instruction> instructionList;

	@XmlElement(name="Warning", required = true)	
	private List<Warning> warningList;
	
	@XmlElement(required = true)	
	private String patName;

	@XmlElement(required = true)	
	private String patNameChi;
	
	@XmlElement(required = true)	
	private String hospName;	
	
	@XmlElement(required = true)	
	private String hospNameChi;
	
	@XmlElement(required = true)	
	private String specCode;

	@XmlElement(required = true)	
	private Integer issueQty;
	
	@XmlElement(required = true)
	private String baseUnit;
	
	@XmlElement(required = true)	
	private Date dispDate;
	
	@XmlElement(required = true)	
	private String caseNum;
	
	@XmlElement(required = true)	
	private String ward;
	
	@XmlElement(required = true)	
	private String bedNum;
	
	@XmlElement(required = true)	
	private String binNum;

	@XmlElement(required = true)	
	private Boolean largeLayoutFlag;
	
	private transient PrintOption printOption;

	public BaseLabel() {
		largeLayoutFlag = Boolean.FALSE;
	}
	
	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemDesc() {
		return itemDesc;
	}

	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getPatNameChi() {
		return patNameChi;
	}

	public void setPatNameChi(String patNameChi) {
		this.patNameChi = patNameChi;
	}

	public String getHospName() {
		return hospName;
	}

	public void setHospName(String hospName) {
		this.hospName = hospName;
	}

	public String getHospNameChi() {
		return hospNameChi;
	}

	public void setHospNameChi(String hospNameChi) {
		this.hospNameChi = hospNameChi;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public Integer getIssueQty() {
		return issueQty;
	}

	public void setIssueQty(Integer issueQty) {
		this.issueQty = issueQty;
	}

	public Date getDispDate() {
		return dispDate;
	}

	public void setDispDate(Date dispDate) {
		this.dispDate = dispDate;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getWard() {
		return ward;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}
	
	public String getBedNum() {
		return bedNum;
	}

	public void setBedNum(String bedNum) {
		this.bedNum = bedNum;
	}

	public List<Instruction> getInstructionList() {
		if (instructionList == null) {
			instructionList = new ArrayList<Instruction>();
		}
		return instructionList;
	}

	public void setInstructionList(List<Instruction> instructionList) {
		this.instructionList = instructionList;
	}
	
	public List<Warning> getWarningList() {
		if (warningList == null) {
			warningList = new ArrayList<Warning>();
		}
		return warningList;
	}

	public void setWarningList(List<Warning> warningList) {
		this.warningList = warningList;
	}
	
	public PrintOption getPrintOption() {
		return printOption;
	}

	public void setPrintOption(PrintOption printOption) {
		this.printOption = printOption;
	}

	public void setBinNum(String binNum) {
		this.binNum = binNum;
	}

	public String getBinNum() {
		return binNum;
	}

	public Boolean getLargeLayoutFlag() {
		return largeLayoutFlag;
	}

	public void setLargeLayoutFlag(Boolean largeLayoutFlag) {
		this.largeLayoutFlag = largeLayoutFlag;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public String getBaseUnit() {
		return baseUnit;
	}
	
	public boolean isOpDispLabel() {
		return this instanceof DispLabel;
	}
	
	public boolean isMpDispLabel() {
		return this instanceof MpDispLabel;
	}
	
	@ExternalizedProperty
	public String getMealTimeDesc() {
		if (printOption == null) {
			return "";
		}
		for (Instruction instruction : getInstructionList()) {
			if (instruction.getLang().equals(printOption.getPrintLang().getDataValue()) &&  
	    			instruction.getMealTimeDesc() != null) {
				return instruction.getMealTimeDesc();
			}
		}
		return "";
	}
	
	@ExternalizedProperty
	public String getWarningText() {
		if (printOption == null) {
			return "";
		}
	    for (Warning warning : getWarningList()) {
	    	if (warning.getLang().equals(printOption.getPrintLang().getDataValue()) && 
	    			warning.getLineList() != null) {
	    		if ( printOption.getPrintType() == PrintType.Normal && getOrderType() == OrderType.OutPatient ){
	    			return StringUtils.join(warning.getLineList().toArray(), '\n');
	    		} else {
	    			StringBuilder sb = new StringBuilder();
	    			for (String warningLine : warning.getLineList()) {
	    				if (StringUtils.isNotBlank(warningLine)) {
		    				if (sb.length() > 0) {
		    					sb.append('\n');
		    				}
	    					sb.append(warningLine);
	    				}
	    			}
	    			return sb.toString();
	    		}
	    	}
	    }
		return "";
	}

	@ExternalizedProperty
	public String getInstructionText() {
	    for (Instruction instruction : getInstructionList()) {
	    	if (printOption == null) {
	    		return "";
	    	}
	    	if (instruction.getLang().equals(printOption.getPrintLang().getDataValue()) &&  
	    			instruction.getLineList() != null) {
	    		if( getOrderType() == OrderType.OutPatient ){
	    			instruction = reformDoubleDoseInstruction(instruction);
	    		}
			    List<String> tempLineList = new ArrayList<String>();
	    		for (String line : instruction.getLineList()) { 
	    			if (StringUtils.isNotBlank(StringUtils.trim(line.replaceAll("@", "").replaceAll("\\^", "")))) {
	    				tempLineList.add(formatLine(line));
	    			}
	    		}
	    		return StringUtils.join(tempLineList.toArray(), '\n');
	    	}
	    }
		return "";
	}

	@ExternalizedProperty
	public String getInstructionLowerCaseText() {
	    for (Instruction instruction : getInstructionList()) {
	    	if (printOption == null) {
	    		return "";
	    	}
	    	if (instruction.getLang().equals(printOption.getPrintLang().getDataValue()) &&  
	    			instruction.getLineList() != null) {
			    List<String> tempLineList = new ArrayList<String>();
	    		for (String line : instruction.getLineList()) { 
	    			if (StringUtils.isNotBlank(StringUtils.trim(line.replaceAll("@", "").replaceAll("\\^", "")))) {
	    				String formatLine = formatLine(line);
	    				tempLineList.add(StringUtils.upperCase(StringUtils.substring(formatLine, 0, 1)) + 
	    						StringUtils.lowerCase(StringUtils.substring(formatLine, 1)));
	    			}
	    		}
	    		
			    List<String> outLineList = new ArrayList<String>();
	    		int charLimit = getCharLimit();
	            for (String line : StringUtils.split(StringUtils.join(tempLineList.toArray(), '\n'),'\n')) {
	    			if (getInstructionLength(line) > charLimit) {
	    				if (printOption.getPrintLang() == PrintLang.Eng){
	    					String[] lineBlocks = line.split(SPACE);
	    					StringBuilder sb = new StringBuilder();
	    					int curLength = 0;
	    					for( String lineBlock : lineBlocks ){
	    						if( curLength <= charLimit ){
	    							if( curLength > 0 ){
	    								sb.append(SPACE+lineBlock);
	    								curLength += lineBlock.length() + 1;
	    							}else{
	    								sb.append(lineBlock);
	    								curLength += lineBlock.length();
	    							}
	    						}else{
	    							sb.append("\n"+lineBlock);
	    							curLength = lineBlock.length();
	    						}
	    					}
	    					outLineList.add( sb.toString() );
	    				} else {
	    					outLineList.add(StringUtils.substring(line, 0, charLimit/2) + "\n" + StringUtils.substring(line, charLimit/2));
	    				}
	    			} else {
    					outLineList.add(line);
	    			}
	            }
	    		
	    		return StringUtils.join(outLineList.toArray(), '\n');
	    	}
	    }
		return "";
	}
	
	private String formatLine(String s) 
	{
		int charLimit = getCharLimit();
		String blockStringArray[] = getBlockStringArray(s);
        String curLine = "";
        int blockStringLength = 0, curLineLength = 0;
        
	    List<String> lineList = new ArrayList<String>();
		for (String blockString:blockStringArray) {
			blockStringLength = getInstructionLength(blockString);
			curLineLength = getInstructionLength(curLine);
			int spaceDigit = (printOption.getPrintLang() == PrintLang.Eng)?1:0;
			if (curLineLength + blockStringLength + spaceDigit <= charLimit) {
				if (StringUtils.isNotBlank(curLine) && printOption.getPrintLang() == PrintLang.Eng){
					curLine = curLine + SPACE;
				}					
			} else {
				lineList.add(curLine);
				curLine = "";
			}
			if (StringUtils.isNotEmpty(curLine)){
				curLine = curLine + blockString.replaceAll("@", SPACE);
			}else{
				if( (printOption.getPrintType() == PrintType.LargeLayout || printOption.getPrintType() == PrintType.LargeFont)
						&& ( "@".equals(StringUtils.substring(blockString, 0, 1))
						|| " ".equals(StringUtils.substring(blockString, 0, 1)) ) ){
					curLine = StringUtils.substring(blockString, 1).replaceAll("@", SPACE);
				}else{
					curLine = blockString.replaceAll("@", SPACE);
				}
			}
        }	        
        if (curLine.length() > 0) {
			lineList.add(curLine);
        }
        
        return StringUtils.join(lineList.toArray(), '\n');
	}

	private String[] getBlockStringArray(String s){
		if (printOption.getPrintType() == PrintType.Normal && getOrderType() == OrderType.OutPatient) {
			if (printOption.getPrintLang() == PrintLang.Eng) {
				s = s.replaceAll("\\^", SPACE);
				return s.split(SPACE);
			} else {
				s = s.replaceAll("\\^", "");
				
				StringBuilder sb = new StringBuilder();
				char prevChar = 0;
		        for(int i = 0; i< s.length(); i++) {
		        	char c = s.charAt(i);
		        	if (sb.length() > 0 &&
		        			!(StringUtils.contains(NON_BREAK_TEXT, prevChar) &&
		        			StringUtils.contains(NON_BREAK_TEXT, c))) {
		        		sb.append("^");
		        	}
		            sb.append(c);
		        	prevChar = c;
		        }
				return sb.toString().replace("^,", ",").split("\\^");
			}
		} else {
			return s.split("\\^");
		}
	}
	
	abstract int getCharLimit();
	
	private int getInstructionLength(String instruction){
		int count = 0;
		for (int i = 0; i < instruction.length() ; i++) {
			if ((int)instruction.charAt(i) < 128) {
				count++;
			} else {
				count += + 2;
			}
		}
		if (printOption.getPrintLang() == PrintLang.Chi) {
			count -= StringUtils.countMatches(instruction, "^");
		}
		return count;
	}

	abstract OrderType getOrderType();
	
	private Instruction reformDoubleDoseInstruction(Instruction instruction) {
		int charLimit = getCharLimit();
		
		String lineArray[] = instruction.getLineList().toArray(new String[instruction.getLineList().size()]);
		
		if (lineArray.length == 3 && lineArray[0].endsWith(LINE_BREAK_SEPARATOR)) { 
			//same meal time + site + suppl site
			
			String verbDesc = lineArray[0].substring(0, lineArray[0].length() - 1);
			String line1 = null;
			String line2 = null;
			if (lineArray[1].startsWith("@")) {
				line1 = lineArray[1].substring(1, lineArray[1].length());
			} else {
				line1 = lineArray[1].substring(0, lineArray[1].length());
			}
			if (lineArray[2].startsWith("@")) {
				line2 = lineArray[2].substring(1, lineArray[2].length());
			} else {
				line2 = lineArray[2].substring(0, lineArray[2].length());
			}
			
			int verbDescLength = getInstructionLength(verbDesc);
			int line1Length = getInstructionLength(line1);
			int line2Length = getInstructionLength(line2);
			
			if(verbDescLength + line1Length <= charLimit 
					&& verbDescLength + line2Length <= charLimit) {
				List<String> lineList = new ArrayList<String>();
				lineList.add(verbDesc.concat(line1));
				if (printOption.getPrintLang() == PrintLang.Eng) {
					lineList.add(StringUtils.leftPad("", verbDesc.length()+1, "@").concat(line2));	
				} else {
					lineList.add(StringUtils.leftPad("", verbDescLength/2 , "\u3000").concat(line2));
				}
				instruction.setLineList(lineList);
			} else {
				String fullInstruction = "";
				if (printOption.getPrintLang() == PrintLang.Eng) {
					if (line2.startsWith(LINE_BREAK_SEPARATOR)) {
						fullInstruction = formatLine(verbDesc.concat(line1).concat(line2));
					}else{
						fullInstruction = formatLine(verbDesc.concat(line1).concat(" ").concat(line2));
					}
				}else{
					fullInstruction = formatLine(verbDesc.concat(line1).concat("\u3000").concat(line2));
				}
				
				int lineCount = StringUtils.countMatches(fullInstruction, "\n");
				
				if (lineCount < 2) {
					List<String> lineList = new ArrayList<String>();
					if (printOption.getPrintLang() == PrintLang.Eng) {
						if (line2.startsWith(LINE_BREAK_SEPARATOR)) {
							lineList.add(verbDesc.concat(line1).concat(line2));
						} else {
							//stop
							lineList.add(verbDesc.concat(line1).concat(" ").concat(line2));
						}
					} else {
						lineList.add(verbDesc.concat(line1).concat("\u3000").concat(line2));
					}
					instruction.setLineList(lineList);
				}
			}
		}else if (lineArray.length == 2){ 
			//different meal time + site + suppl site
			
			int line1Length = getInstructionLength(lineArray[0]);
			int line2Length = getInstructionLength(lineArray[1]);
			
			if( line1Length <= charLimit && line2Length <= charLimit) {
				List<String> lineList = new ArrayList<String>();
				lineList.add(lineArray[0]);
				lineList.add(lineArray[1]);	
				instruction.setLineList(lineList);
			} else {
				String fullInstruction = "";
				if (printOption.getPrintLang() == PrintLang.Eng) {
					fullInstruction = formatLine(lineArray[0].concat(lineArray[1]));
				} else {
					fullInstruction = formatLine(lineArray[0].concat("\u3000").concat(lineArray[1]));
				}
				
				int lineCount = StringUtils.countMatches(fullInstruction, "\n");
				
				if (lineCount < 2) {
					List<String> lineList = new ArrayList<String>();
					if (printOption.getPrintLang() == PrintLang.Eng) {
						lineList.add(lineArray[0].concat(" ").concat(lineArray[1]));
					} else {
						lineList.add(lineArray[0].concat("\u3000").concat(lineArray[1]));
					}
					instruction.setLineList(lineList);
				}
			}
		}else if (lineArray.length == 4
				 && lineArray[0].endsWith(LINE_BREAK_SEPARATOR)
				 && lineArray[2].endsWith(LINE_BREAK_SEPARATOR)){ 
			//different meal time + site + suppl site ; StepUpDown
			
			String line1 = lineArray[0];
			String line2 = lineArray[1];
			String line3 = lineArray[2];
			String line4 = lineArray[3];
			if (lineArray[1].startsWith("@^")) {
				line2 = lineArray[1].substring(2, lineArray[1].length());
			}
			if (lineArray[3].startsWith("@^")) {
				line4 = lineArray[3].substring(2, lineArray[3].length());
			}
			
			int line1Length = getInstructionLength(line1);
			int line2Length = getInstructionLength(line2);
			int line3Length = getInstructionLength(line3);
			int line4Length = getInstructionLength(line4);
			
			if( line1Length+line2Length <= charLimit && line3Length+line4Length <= charLimit) {
				List<String> lineList = new ArrayList<String>();
				lineList.add(line1.concat(line2));
				lineList.add(line3.concat(line4));	
				instruction.setLineList(lineList);
			} else {
				String fullInstruction = "";
				if (printOption.getPrintLang() == PrintLang.Eng) {
					fullInstruction = formatLine(line1.concat(line2).concat(" ").concat(line3).concat(line4));
				} else {
					fullInstruction = formatLine(line1.concat(line2).concat("\u3000").concat(line3).concat(line4));
				}
				
				int lineCount = StringUtils.countMatches(fullInstruction, "\n");
				
				if (lineCount < 2) {
					List<String> lineList = new ArrayList<String>();
					if (printOption.getPrintLang() == PrintLang.Eng) {
						lineList.add(line1.concat(line2).concat(" ").concat(line3).concat(line4));
					} else {
						lineList.add(line1.concat(line2).concat("\u3000").concat(line3).concat(line4));
					}
					instruction.setLineList(lineList);
				}
			}
		}
		
		return instruction;
	}
}