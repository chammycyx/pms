package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.model.pms.persistence.phs.Ward;
import hk.org.ha.model.pms.persistence.reftable.WardGroup;
import hk.org.ha.model.pms.vo.reftable.mp.WardGroupInfo;

import java.util.List;

import javax.ejb.Local;

@Local
public interface WardGroupListServiceLocal {

	WardGroupInfo retrieveWardGroupList();
	
	void updateWardGroup(WardGroup inWardGroup, List<Ward> selectedWardList);

	void createWardGroup(String wardGroupCode);
	
	WardGroupInfo removeWardGroup(WardGroup wardGroup);
	
	boolean isSaveSuccess();
	
	String getErrCode();
	
	void destroy();
	
}
