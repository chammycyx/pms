package hk.org.ha.model.pms.biz.label;

import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_HOSPITAL_NAMECHI;
import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_HOSPITAL_NAMEENG;
import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_UPDATEUSER_CHARACTER_MASK;
import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_UPDATEUSER_VISIBLE;
import static hk.org.ha.model.pms.prop.Prop.LABEL_MPDISPLABEL_HKID_VISIBLE;
import static hk.org.ha.model.pms.prop.Prop.LABEL_PIVASPRODUCTLABEL_PATIENT_NAMECHI_VISIBLE;
import static hk.org.ha.model.pms.prop.Prop.LABEL_PIVASWORKSHEET_PATIENT_NAMECHI_VISIBLE;
import static hk.org.ha.model.pms.prop.Prop.LABEL_PIVASOUTERBAGLABEL_PATIENT_NAMECHI_VISIBLE;
import static hk.org.ha.model.pms.prop.Prop.LABEL_PIVASOUTERBAGLABEL_HKID_VISIBLE;
import static hk.org.ha.model.pms.prop.Prop.LABEL_PIVASWORKSHEET_HKID_VISIBLE;
import static hk.org.ha.model.pms.prop.Prop.LABEL_PIVASPRODUCTLABEL_FIRSTDOSEINFO_VISIBLE;
import static hk.org.ha.model.pms.prop.Prop.LABEL_PIVASOUTERBAGLABEL_FIRSTDOSEINFO_VISIBLE;
import static hk.org.ha.model.pms.prop.Prop.LABEL_PIVASMDSOVERRIDINGLABEL_PATIENT_NAMECHI_VISIBLE;
import static hk.org.ha.model.pms.prop.Prop.LABEL_PIVASPRODUCTLABEL_FREQ_VISIBLE;
import static hk.org.ha.model.pms.prop.Prop.LABEL_PIVASWORKSHEET_FREQ_VISIBLE;
import static hk.org.ha.model.pms.prop.Prop.LABEL_PIVASOUTERBAGLABEL_FREQ_VISIBLE;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.pivas.PivasWorklistManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.corp.cache.DmFormCacherInf;
import hk.org.ha.model.pms.corp.cache.DmWarningCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmForm;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasContainer;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrugManufMethod;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormula;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaDrug;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaMethod;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaMethodDrug;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedOrderItemAlert;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.pivas.PivasPrep;
import hk.org.ha.model.pms.persistence.pivas.PivasPrepMethod;
import hk.org.ha.model.pms.persistence.pivas.PivasPrepMethodItem;
import hk.org.ha.model.pms.persistence.pivas.PivasWorklist;
import hk.org.ha.model.pms.udt.pivas.PivasProductLabelType;
import hk.org.ha.model.pms.udt.rx.RxItemType;
import hk.org.ha.model.pms.vo.label.MdsOverrideReasonLabel;
import hk.org.ha.model.pms.vo.label.PivasProductLabel;
import hk.org.ha.model.pms.vo.label.PivasWorksheet;
import hk.org.ha.model.pms.vo.label.PivasOuterBagLabel;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.Freq;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import org.apache.commons.lang3.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.joda.time.DateTime;

@AutoCreate
@Stateless
@Name("pivasLabelBuilder")
@MeasureCalls
public class PivasLabelBuilderBean implements PivasLabelBuilderLocal {
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private DmFormCacherInf dmFormCacher;
	
	@In
	private DmWarningCacherInf dmWarningCacher;
	
	@In
	private PivasWorklistManagerLocal pivasWorklistManager;
	
	@In(required = false)
	private UamInfo uamInfo;
	
	private static final String COMMMA_SPACE = ", ";
	private static final String SPACE = " ";
	private static final String SUPPL_FREQ_CODE_WEEKDAY = "00021";
	private static final String DAY_ARRAY[] = {"mon","tue","wed","thu","fri","sat","sun"};
	private static final String VEND_SUPPLY_SOLV = "VEND_SUPPLY_SOLV";
	
	private static final String _PNG_ZOOM_RATIO = "_PNG_ZOOM_RATIO";
	private static final String SHOW_PATIENT_NAMECHI = "SHOW_PATIENT_NAMECHI";
	private static final String SHOW_FIRST_DOSE_DATE_TIME = "SHOW_FIRST_DOSE_DATE_TIME";
	private static final String SHOW_FREQ = "SHOW_FREQ";
	private static final String SHOW_HKID = "SHOW_HKID";
	private static final String SHOW_UPDATE_USER = "SHOW_UPDATE_USER";
	private static final String USER_CODE = "USER_CODE";
	private static final String PIVAS_FLAG = "PIVAS_FLAG";
	
	public Map<String, Object> getPivasProductLabelParam(PivasProductLabel pivasProductLabel, String hospCode) {
		Map<String, Object> params = new HashMap<String, Object>();
		
		params.put("hospCode", hospCode);
		params.put(_PNG_ZOOM_RATIO, Float.valueOf(1.3f));
				
		if (StringUtils.isNotBlank(pivasProductLabel.getPatNameChi())) {
			params.put(SHOW_PATIENT_NAMECHI, LABEL_PIVASPRODUCTLABEL_PATIENT_NAMECHI_VISIBLE.get(false));
		} else {
			params.put(SHOW_PATIENT_NAMECHI, false);
		}

		if (pivasProductLabel.getFirstDoseDateTime() != null) {
			params.put(SHOW_FIRST_DOSE_DATE_TIME, LABEL_PIVASPRODUCTLABEL_FIRSTDOSEINFO_VISIBLE.get(false));
		} else {
			params.put(SHOW_FIRST_DOSE_DATE_TIME, false);
		}
		
		params.put(SHOW_FREQ, LABEL_PIVASPRODUCTLABEL_FREQ_VISIBLE.get(true));
		
		return params;
	}
	
	
	public Map<String, Object> getPivasWorksheetParam(PivasWorksheet pivasWorksheet, String hospCode) {
		Map<String, Object> params = new HashMap<String, Object>();
		
		params.put("hospCode", hospCode);
		params.put(_PNG_ZOOM_RATIO, Float.valueOf(1.3f));
		
		if (StringUtils.isBlank(pivasWorksheet.getCaseNum())) {
			params.put(SHOW_HKID, true);
		} else {
			params.put(SHOW_HKID, LABEL_PIVASWORKSHEET_HKID_VISIBLE.get(false));
		}

		if (StringUtils.isNotBlank(pivasWorksheet.getPatNameChi())) {
			params.put(SHOW_PATIENT_NAMECHI, LABEL_PIVASWORKSHEET_PATIENT_NAMECHI_VISIBLE.get(false));
		} else {
			params.put(SHOW_PATIENT_NAMECHI, false);
		}
		
		params.put(SHOW_FREQ, LABEL_PIVASWORKSHEET_FREQ_VISIBLE.get(true));
		
		return params;
	}
	
	
	public Map<String, Object> getPivasOuterBagLabelParam(PivasOuterBagLabel pivasOuterBagLabel, String hospCode) {
		Map<String, Object> params = new HashMap<String, Object>();
		
		params.put("hospCode", hospCode);
		params.put(_PNG_ZOOM_RATIO, Float.valueOf(1.3f));

		if (StringUtils.isBlank(pivasOuterBagLabel.getCaseNum())) {
			params.put(SHOW_HKID, true);
		} else {
			params.put(SHOW_HKID, LABEL_PIVASOUTERBAGLABEL_HKID_VISIBLE.get(false));
		}
		
		if (StringUtils.isNotBlank(pivasOuterBagLabel.getPatNameChi())) {
			params.put(SHOW_PATIENT_NAMECHI, LABEL_PIVASOUTERBAGLABEL_PATIENT_NAMECHI_VISIBLE.get(false));
		} else {
			params.put(SHOW_PATIENT_NAMECHI, false);
		}
		
		if (pivasOuterBagLabel.getFirstDoseDateTime() != null) {
			params.put(SHOW_FIRST_DOSE_DATE_TIME, LABEL_PIVASOUTERBAGLABEL_FIRSTDOSEINFO_VISIBLE.get(false));
		} else {
			params.put(SHOW_FIRST_DOSE_DATE_TIME, false);
		}
		
		params.put(SHOW_FREQ, LABEL_PIVASOUTERBAGLABEL_FREQ_VISIBLE.get(true));

		return params;
	}
	
	public Map<String, Object> getMdsOverrideReasonLabelParam(MdsOverrideReasonLabel mdsOverrideReasonLabel, String hospCode) {
		Map<String, Object> params = new HashMap<String, Object>();
		
		params.put("hospCode", hospCode);
		params.put(_PNG_ZOOM_RATIO, Float.valueOf(1.3f));
	
		if (StringUtils.isBlank(mdsOverrideReasonLabel.getCaseNum())) {
			params.put(SHOW_HKID, true);
		} else {
			params.put(SHOW_HKID, LABEL_MPDISPLABEL_HKID_VISIBLE.get(false));
		}
		
		params.put(SHOW_UPDATE_USER, LABEL_DISPLABEL_UPDATEUSER_VISIBLE.get(false));
			
		params.put(USER_CODE, mdsOverrideReasonLabel.getUserCode());
		
		if (StringUtils.isNotBlank(mdsOverrideReasonLabel.getPatNameChi())) {
			params.put(SHOW_PATIENT_NAMECHI, LABEL_PIVASMDSOVERRIDINGLABEL_PATIENT_NAMECHI_VISIBLE.get(false));
		} else {
			params.put(SHOW_PATIENT_NAMECHI, false);
		}
		
		params.put(PIVAS_FLAG, true);
		
		return params;
	}
	
	public PivasProductLabel convertToPivasProductLabel(DispOrderItem dispOrderItem, DeliveryItem deliveryItem, PivasFormulaMethod pivasFormulaMethod, PivasContainer pivasContainer) {
		PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
		PivasWorklist pivasWorklist = deliveryItem.getPivasWorklist();
		PivasPrep pivasPrep = pivasWorklist.getPivasPrep();
		PivasPrepMethod pivasPrepMethod = pivasWorklist.getPivasPrepMethod();
		MedProfileMoItem medProfileMoItem = pivasWorklist.getMedProfileMoItem();
		
		PivasFormula pivasFormula = pivasFormulaMethod.getPivasFormula();
				
		PivasProductLabel pivasProductLabel = new PivasProductLabel();
		
		pivasProductLabel.setFormulaPrintName(pivasFormula.getPrintName());		
		pivasProductLabel.setSiteDesc(pivasFormula.getDmSite().getIpmoeDesc());
		pivasProductLabel.setPrepExpiryDate(pivasWorklist.getExpiryDate());
		
		if (pivasPrepMethod.getWarnCode1() != null) {
			pivasProductLabel.setWarnCode1(dmWarningCacher.getDmWarningByWarnCode(pivasPrepMethod.getWarnCode1()).getWarnMessageEng());
		}
		
		if (pivasPrepMethod.getWarnCode2() != null) {
			pivasProductLabel.setWarnCode2(dmWarningCacher.getDmWarningByWarnCode(pivasPrepMethod.getWarnCode2()).getWarnMessageEng());
		}
		
		pivasProductLabel.setConcn(convertToString(pivasFormula.getConcn()));
		pivasProductLabel.setPivasDosageUnit(pivasWorklistManager.extractOrderDosageUnit(medProfileMoItem));
		pivasProductLabel.setManufactureDate(pivasWorklist.getSupplyDate());
		pivasProductLabel.setTrxId(convertToString(dispOrderItem.getDeliveryItemId()));

		if(!pivasWorklist.getAdHocFlag() && !pivasWorklist.getRefillFlag())
		{
			if(pivasPrep.getSingleDispFlag())
			{
				pivasProductLabel.setLabelType(PivasProductLabelType.NewSingle.getDataValue());
			}
			else
			{
				pivasProductLabel.setLabelType(PivasProductLabelType.New.getDataValue());
			}
		}
		else if(pivasWorklist.getAdHocFlag())
		{
			pivasProductLabel.setLabelType(PivasProductLabelType.Replenish.getDataValue());
		}
		
		if (dispOrderItem.getDispOrder().getTicket() != null) {
			int batchDay = new DateTime(dispOrderItem.getDispOrder().getTicket().getTicketDate()).getDayOfMonth();
			pivasProductLabel.setBatchNum( (( batchDay < 10 )?"0":"") + batchDay + dispOrderItem.getDispOrder().getTicket().getTicketNum());
		}
		
		if (pharmOrderItem.getPharmOrder().getMedCase() != null) {
			pivasProductLabel.setCaseNum(pharmOrderItem.getPharmOrder().getMedCase().getCaseNum());
			pivasProductLabel.setBedNum(pharmOrderItem.getPharmOrder().getMedCase().getPasBedNum());
		}
		
		pivasProductLabel.setWard(pharmOrderItem.getPharmOrder().getWardCode());
		pivasProductLabel.setPatName(pharmOrderItem.getPharmOrder().getPatient().getName());
		pivasProductLabel.setPatNameChi(StringUtils.trim(pharmOrderItem.getPharmOrder().getPatient().getNameChi()));

		if (pivasWorklist.getShelfLife() != null && pivasWorklist.getShelfLife() <= 0) {
			pivasProductLabel.setShelfLifeZeroFlag(true);
		} else {
			pivasProductLabel.setShelfLifeZeroFlag(false);
		}

		if (pivasPrepMethod.getDiluentDesc() != null) {
			pivasProductLabel.setDiluentDesc(pivasPrepMethod.getDiluentDesc());
		}
		
		pivasProductLabel.setContainerName(pivasContainer.getContainerName());
		pivasProductLabel.setFirstDoseDateTime(pivasWorklist.getDueDate());
		
		Freq freq = pivasWorklistManager.extractOrderDailyFreq(medProfileMoItem);
		if (freq != null) {
			pivasProductLabel.setFreq(freq.getDesc());
		}
		
		pivasProductLabel.setSupplFreq(extractOrderSupplFreqDesc(medProfileMoItem));
		
		BigDecimal orderDosage = pivasWorklistManager.extractOrderDosage(medProfileMoItem);
		pivasProductLabel.setOrderDosage(convertToString(orderDosage));
		
		pivasProductLabel.setFinalVolume(convertToString(pivasPrep.getVolume()));
		pivasProductLabel.setSplitCount(convertToString(pivasPrep.getSplitCount()));
		
		if (pivasPrep.getSplitCount() != null && pivasPrep.getSplitCount() > 1) {
			pivasProductLabel.setSplitFlag(true);
			pivasProductLabel.setNumOfLabelSet(pivasWorklist.getNumOfLabel() / pivasPrep.getSplitCount());
			
			if (pivasProductLabel.getOrderDosage() != null) {
				pivasProductLabel.setOrderDosageSplit(calOrderDosageSplit(orderDosage, pivasPrep.getSplitCount()));
			}
			
			if (pivasPrep.getVolume() != null) {
				pivasProductLabel.setFinalVolumeSplit(calFinalVolumeSplit(pivasPrep.getVolume(), pivasPrep.getSplitCount()));
			}
		} else {
			pivasProductLabel.setSplitFlag(false);
			pivasProductLabel.setNumOfLabelSet(pivasWorklist.getNumOfLabel());
		}
		
		// save for PIVAS report
		MedProfile medProfile = pivasWorklist.getMedProfile();
		Patient patient = medProfile.getPatient();
		
		pivasProductLabel.setSpecCode(medProfile.getSpecCode());
		
		if ( patient.getSex() != null ) {
			pivasProductLabel.setSex(patient.getSex().getDataValue());
		}
		
		if ( patient.getAge() != null ) {
			pivasProductLabel.setAge(patient.getAge()+patient.getDateUnit().substring(0, 1));
		}
		
		if (medProfile.getBodyWeight() != null) {
			DecimalFormat df = new DecimalFormat("0.##");
			String bodyWeight = df.format(medProfile.getBodyWeight().stripTrailingZeros());
			pivasProductLabel.setBodyWeight(bodyWeight);
		}
		
		pivasProductLabel.setDob(patient.getDob());
		
		return pivasProductLabel;
	}
	
	
	public PivasWorksheet convertToPivasWorksheet(DispOrderItem dispOrderItem, DeliveryItem deliveryItem, PivasFormulaMethod pivasFormulaMethod, PivasContainer pivasContainer) {
		PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
		PivasWorklist pivasWorklist = deliveryItem.getPivasWorklist();
		PivasPrep pivasPrep = pivasWorklist.getPivasPrep();
		PivasPrepMethod pivasPrepMethod = pivasWorklist.getPivasPrepMethod();
		MedProfileMoItem medProfileMoItem = pivasWorklist.getMedProfileMoItem();	
		
		PivasFormula pivasFormula = pivasFormulaMethod.getPivasFormula();
		PivasPrepMethodItem pivasPrepMethodItem = pivasPrepMethod.getPivasPrepMethodItemList().get(0);
		
		PivasFormulaMethodDrug pivasFormulaMethodDrug = pivasFormulaMethod.getPivasFormulaMethodDrugList().get(0);
		PivasDrugManufMethod pivasDrugManufMethod = pivasFormulaMethodDrug.getPivasDrugManufMethod();
		
		PivasWorksheet pivasWorksheet = new PivasWorksheet();
		
		pivasWorksheet.setFormulaPrintName(pivasFormula.getPrintName());
		
		BigDecimal orderDosage = pivasWorklistManager.extractOrderDosage(medProfileMoItem);
		pivasWorksheet.setOrderDosage(convertToString(orderDosage));
		
		pivasWorksheet.setDosageQty(convertToString(pivasDrugManufMethod.getDosageQty()));
		pivasWorksheet.setSiteDesc(pivasFormula.getDmSite().getIpmoeDesc());
		pivasWorksheet.setPrepExpiryDate(pivasWorklist.getExpiryDate());
		pivasWorksheet.setPivasDosageUnit(pivasWorklistManager.extractOrderDosageUnit(medProfileMoItem));
		pivasWorksheet.setManufactureDate(pivasWorklist.getSupplyDate());		
		pivasWorksheet.setTrxId(convertToString(dispOrderItem.getDeliveryItemId()));
		pivasWorksheet.setHkid(pivasPrep.getMedProfile().getPatient().getHkid());
	
		if (dispOrderItem.getDispOrder().getTicket() != null) {
			int batchDay = new DateTime(dispOrderItem.getDispOrder().getTicket().getTicketDate()).getDayOfMonth();
			pivasWorksheet.setBatchNum( (( batchDay < 10 )?"0":"") + batchDay + dispOrderItem.getDispOrder().getTicket().getTicketNum());
		}
		
		if (pharmOrderItem.getPharmOrder().getMedCase() != null) {
			pivasWorksheet.setCaseNum(pharmOrderItem.getPharmOrder().getMedCase().getCaseNum());
			pivasWorksheet.setBedNum(pharmOrderItem.getPharmOrder().getMedCase().getPasBedNum());
		}
		
		pivasWorksheet.setWard(pharmOrderItem.getPharmOrder().getWardCode());
		pivasWorksheet.setPatName(pharmOrderItem.getPharmOrder().getPatient().getName());
		pivasWorksheet.setPatNameChi(StringUtils.trim(pharmOrderItem.getPharmOrder().getPatient().getNameChi()));

		if (pivasWorklist.getShelfLife() != null && pivasWorklist.getShelfLife() <= 0) {
			pivasWorksheet.setShelfLifeZeroFlag(true);
		} else {
			pivasWorksheet.setShelfLifeZeroFlag(false);
		}
		
		pivasWorksheet.setContainerName(pivasContainer.getContainerName());
		pivasWorksheet.setVendSupplySolvFlag(VEND_SUPPLY_SOLV.equals(pivasPrepMethodItem.getSolventCode()));
		
		Freq freq = pivasWorklistManager.extractOrderDailyFreq(medProfileMoItem);
		if (freq != null) {
			pivasWorksheet.setFreq(freq.getDesc());
		}
		
		pivasWorksheet.setSupplFreq(extractOrderSupplFreqDesc(medProfileMoItem));
		
		if (pivasPrepMethodItem.getSolventDesc() != null){
			pivasWorksheet.setSolventDesc(pivasPrepMethodItem.getSolventDesc());
		}
		
		if (pivasPrepMethod.getDiluentDesc() != null){
			pivasWorksheet.setDiluentDesc(pivasPrepMethod.getDiluentDesc());
		}	

		pivasWorksheet.setVolume(convertToString(pivasPrep.getVolume()));
		pivasWorksheet.setSolventVolume(convertToString(pivasDrugManufMethod.getVolume()));
		pivasWorksheet.setDiluentVolume(convertToString(pivasFormulaMethod.getDiluentVolume()));
		
		pivasWorksheet.setDrugItemCode(pivasPrepMethodItem.getDrugItemCode());
		
		pivasWorksheet.setDrugVolume(convertToString(pivasFormulaMethodDrug.getVolume()));
		
		pivasWorksheet.setDrugManufCode(pivasPrepMethodItem.getDrugManufCode());
		pivasWorksheet.setDrugBatchNum(pivasPrepMethodItem.getDrugBatchNum());
		pivasWorksheet.setDrugExpiryDate(pivasPrepMethodItem.getDrugExpiryDate());
		
		pivasWorksheet.setSolventItemCode(pivasPrepMethodItem.getSolventItemCode());
		pivasWorksheet.setSolventManufCode(pivasPrepMethodItem.getSolventManufCode());
		pivasWorksheet.setSolventBatchNum(pivasPrepMethodItem.getSolventBatchNum());
		pivasWorksheet.setSolventExpiryDate(pivasPrepMethodItem.getSolventExpiryDate());
		
		pivasWorksheet.setDiluentItemCode(pivasPrepMethod.getDiluentItemCode());
		pivasWorksheet.setDiluentManufCode(pivasPrepMethod.getDiluentManufCode());
		pivasWorksheet.setDiluentBatchNum(pivasPrepMethod.getDiluentBatchNum());
		pivasWorksheet.setDiluentExpiryDate(pivasPrepMethod.getDiluentExpiryDate());
		
		DmDrug drugDmDrug = dmDrugCacher.getDmDrug(pivasDrugManufMethod.getPivasDrugManuf().getPivasDrug().getItemCode());
		
		pivasWorksheet.setFinalVolume(convertToString(pivasPrep.getVolume()));
		pivasWorksheet.setFormCode(StringUtils.trimToEmpty(drugDmDrug.getDmDrugProperty().getFormCode()));
		pivasWorksheet.setSaltProperty(StringUtils.trimToEmpty(drugDmDrug.getDmDrugProperty().getSaltProperty()));
		
		String drugStrength = StringUtils.trimToEmpty(drugDmDrug.getStrength());
		String drugVolumeValue = convertToString(drugDmDrug.getVolumeValue());
		String drugVolumeUnit = StringUtils.trimToEmpty(drugDmDrug.getVolumeUnit());
		
		pivasWorksheet.setDrugStrength(drugStrength);
		pivasWorksheet.setDrugVolumeValue(drugVolumeValue);
		pivasWorksheet.setDrugVolumeUnit(drugVolumeUnit);
		pivasWorksheet.setDrugTruncatedStrengthVolume(getTruncatedStrengthVolume(drugStrength, drugVolumeValue, drugVolumeUnit));
		// save for PIVAS report
		pivasWorksheet.setDrugItemDesc(drugDmDrug.getFullDrugDesc());
		pivasWorksheet.setDrugBaseUnit(drugDmDrug.getBaseUnit());

		if ( StringUtils.isNoneBlank(pivasPrepMethod.getDiluentItemCode()) ) {
			DmDrug diluentDmDrug = dmDrugCacher.getDmDrug(pivasPrepMethod.getDiluentItemCode());
			String diluentStrength = StringUtils.trimToEmpty(diluentDmDrug.getStrength());
			String diluentVolumeValue = convertToString(diluentDmDrug.getVolumeValue());
			String diluentVolumeUnit = StringUtils.trimToEmpty(diluentDmDrug.getVolumeUnit());
			
			pivasWorksheet.setDiluentStrength(diluentStrength);
			pivasWorksheet.setDiluentVolumeValue(diluentVolumeValue);
			pivasWorksheet.setDiluentVolumeUnit(diluentVolumeUnit);
			pivasWorksheet.setDiluentTruncatedStrengthVolume(getTruncatedStrengthVolume(diluentStrength, diluentVolumeValue, diluentVolumeUnit));
			// save for PIVAS report
			pivasWorksheet.setDiluentItemDesc(diluentDmDrug.getFullDrugDesc());
		}

		if ( StringUtils.isNoneBlank(pivasPrepMethodItem.getSolventItemCode()) ) {
			DmDrug solventDmDrug = dmDrugCacher.getDmDrug(pivasPrepMethodItem.getSolventItemCode());
			String solventStrength = StringUtils.trimToEmpty(solventDmDrug.getStrength()); 
			String solventVolumeValue = convertToString(solventDmDrug.getVolumeValue());
			String solventVolumeUnit = StringUtils.trimToEmpty(solventDmDrug.getVolumeUnit());
			
			pivasWorksheet.setSolventStrength(solventStrength);
			pivasWorksheet.setSolventVolumeValue(solventVolumeValue);
			pivasWorksheet.setSolventVolumeUnit(solventVolumeUnit);
			pivasWorksheet.setSolventTruncatedStrengthVolume(getTruncatedStrengthVolume(solventStrength, solventVolumeValue, solventVolumeUnit));
			// save for PIVAS report
			pivasWorksheet.setSolventItemDesc(solventDmDrug.getFullDrugDesc());
		}

		if (pivasFormulaMethodDrug.getVolume() != null && pivasFormulaMethod.getDiluentVolume() != null) {
			pivasWorksheet.setRatio(convertToString(pivasFormulaMethodDrug.getVolume().add(pivasFormulaMethod.getDiluentVolume())));
		}

		pivasWorksheet.setConcn(convertToString(pivasFormula.getConcn()));
		pivasWorksheet.setAfterConcn(convertToString(pivasDrugManufMethod.getAfterConcn()));
		pivasWorksheet.setSplitCount(convertToString(pivasPrep.getSplitCount()));
		
		if (pivasPrep.getSplitCount() != null && pivasPrep.getSplitCount() > 1) {
			pivasWorksheet.setSplitFlag(true);
			pivasWorksheet.setPrepQty(convertToString( deliveryItem.getIssueQty().multiply(new BigDecimal(pivasPrep.getSplitCount()))) );			
			
			if (pivasWorksheet.getOrderDosage() != null) {
				pivasWorksheet.setOrderDosageSplit(calOrderDosageSplit(orderDosage, pivasPrep.getSplitCount()));
			}
			
			if (pivasPrep.getVolume() != null) {
				pivasWorksheet.setFinalVolumeSplit(calFinalVolumeSplit(pivasPrep.getVolume(), pivasPrep.getSplitCount()));
			}
		} else {
			pivasWorksheet.setSplitFlag(false);
			pivasWorksheet.setPrepQty(convertToString(deliveryItem.getIssueQty()));
		}
		
		// save for PIVAS report
		pivasWorksheet.setIssueQty(convertToString(pivasWorklist.getIssueQty()));
		pivasWorksheet.setAdjQty(convertToString(pivasWorklist.getAdjQty()));
		pivasWorksheet.setRefillFlag(pivasWorklist.getRefillFlag());
		pivasWorksheet.setAdhocFlag(pivasWorklist.getAdHocFlag());
		pivasWorksheet.setDrugCalQty(convertToString(pivasPrepMethodItem.getDrugCalQty()));
		pivasWorksheet.setDrugDispQty(convertToString(pivasPrepMethodItem.getDrugDispQty()));

		return pivasWorksheet;
	}
	
	
	public PivasOuterBagLabel convertToPivasOuterBagLabel(DispOrderItem dispOrderItem, DeliveryItem deliveryItem, PivasFormulaMethod pivasFormulaMethod, PivasContainer pivasContainer) {
		PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
		PivasWorklist pivasWorklist = deliveryItem.getPivasWorklist();
		PivasPrep pivasPrep = pivasWorklist.getPivasPrep();
		PivasPrepMethod pivasPrepMethod = pivasWorklist.getPivasPrepMethod();
		MedProfileMoItem medProfileMoItem = pivasWorklist.getMedProfileMoItem();
		
		PivasFormula pivasFormula = pivasFormulaMethod.getPivasFormula();

		PivasOuterBagLabel pivasOuterBagLabel = new PivasOuterBagLabel();
		
		pivasOuterBagLabel.setFormulaPrintName(pivasFormula.getPrintName());
		pivasOuterBagLabel.setSiteDesc(pivasFormula.getDmSite().getIpmoeDesc());
		pivasOuterBagLabel.setPrepExpiryDate(pivasWorklist.getExpiryDate());
		pivasOuterBagLabel.setConcn(convertToString(pivasFormula.getConcn()));
		pivasOuterBagLabel.setPivasDosageUnit(pivasWorklistManager.extractOrderDosageUnit(medProfileMoItem));
		pivasOuterBagLabel.setTrxId(convertToString(dispOrderItem.getDeliveryItemId()));
		if (dispOrderItem.getDispOrder().getTicket() != null) {
			int batchDay = new DateTime(dispOrderItem.getDispOrder().getTicket().getTicketDate()).getDayOfMonth();
			pivasOuterBagLabel.setBatchNum( (( batchDay < 10 )?"0":"") + batchDay + dispOrderItem.getDispOrder().getTicket().getTicketNum());
		}
		
		if (pharmOrderItem.getPharmOrder().getMedCase() != null) {
			pivasOuterBagLabel.setCaseNum(pharmOrderItem.getPharmOrder().getMedCase().getCaseNum());
			pivasOuterBagLabel.setBedNum(pharmOrderItem.getPharmOrder().getMedCase().getPasBedNum());
		}
		pivasOuterBagLabel.setWard(pharmOrderItem.getPharmOrder().getWardCode());		
		pivasOuterBagLabel.setPatName(pharmOrderItem.getPharmOrder().getPatient().getName());
		pivasOuterBagLabel.setPatNameChi(StringUtils.trim(pharmOrderItem.getPharmOrder().getPatient().getNameChi()));
		pivasOuterBagLabel.setContainerName(pivasContainer.getContainerName());
		pivasOuterBagLabel.setManufactureDate(pivasWorklist.getSupplyDate());
		pivasOuterBagLabel.setHkid(pivasPrep.getMedProfile().getPatient().getHkid());

		if (pivasWorklist.getShelfLife() != null && pivasWorklist.getShelfLife() <= 0) {
			pivasOuterBagLabel.setShelfLifeZeroFlag(true);
		} else {
			pivasOuterBagLabel.setShelfLifeZeroFlag(false);
		}
		
		pivasOuterBagLabel.setFinalVolume(convertToString(pivasPrep.getVolume()));
		
		if (pivasPrepMethod.getDiluentDesc() != null){
			pivasOuterBagLabel.setDiluentDesc(pivasPrepMethod.getDiluentDesc());
		}
		
		pivasOuterBagLabel.setFirstDoseDateTime(pivasWorklist.getDueDate());
		
		Freq freq = pivasWorklistManager.extractOrderDailyFreq(medProfileMoItem);
		if (freq != null) {
			pivasOuterBagLabel.setFreq(freq.getDesc());
		}
		
		pivasOuterBagLabel.setSupplFreq(extractOrderSupplFreqDesc(medProfileMoItem));
		
		BigDecimal orderDosage = pivasWorklistManager.extractOrderDosage(medProfileMoItem);
		pivasOuterBagLabel.setOrderDosage(convertToString(orderDosage));
		pivasOuterBagLabel.setSplitCount(convertToString(pivasPrep.getSplitCount()));

		if (pivasOuterBagLabel.getOrderDosage() != null && pivasOuterBagLabel.getSplitCount() != null) {
			pivasOuterBagLabel.setOrderDosageSplit(calOrderDosageSplit(orderDosage, pivasPrep.getSplitCount()));
		}

		if (pivasPrep.getVolume() != null && pivasOuterBagLabel.getSplitCount() != null) {
			pivasOuterBagLabel.setFinalVolumeSplit(calFinalVolumeSplit(pivasPrep.getVolume(), pivasPrep.getSplitCount()));
		}

		pivasOuterBagLabel.setNumOfLabel(Integer.valueOf(deliveryItem.getIssueQty().intValue()));
		
		return pivasOuterBagLabel;
	}	
	
	
	public MdsOverrideReasonLabel convertToMdsOverrideReasonLabel(DispOrderItem dispOrderItem, DeliveryItem deliveryItem, PivasFormulaMethod pivasFormulaMethod) {
		
		MdsOverrideReasonLabel mdsOverrideReasonLabel = new MdsOverrideReasonLabel();
		PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();

		List<String> patOverrideReason = new ArrayList<String>();
		List<String> ddiOverrideReason = new ArrayList<String>();

		if (!pharmOrderItem.getMedOrderItem().getPatAlertList().isEmpty()) {
			patOverrideReason.add(pharmOrderItem.getMedOrderItem().getPatAlertList().get(0).getOverrideReasonDesc());
		}
		
		for (MedOrderItemAlert moiAlert : pharmOrderItem.getMedOrderItem().getDdiAlertList()) {
			ddiOverrideReason.add(moiAlert.getOverrideReasonDesc());
		}
			
		if (patOverrideReason.size() == 0 && ddiOverrideReason.size() == 0 ) {
			return null;
		}
			
		mdsOverrideReasonLabel.setPatOverrideReason(StringUtils.replaceChars(StringUtils.join(patOverrideReason.toArray(), ",\n"), '\r', '\n'));
		mdsOverrideReasonLabel.setDdiOverrideReason(StringUtils.replaceChars(StringUtils.join(ddiOverrideReason.toArray(), ",\n"), '\r', '\n'));
		
		mdsOverrideReasonLabel.setPatName(pharmOrderItem.getPharmOrder().getPatient().getName());
		mdsOverrideReasonLabel.setPatNameChi(StringUtils.trim(pharmOrderItem.getPharmOrder().getPatient().getNameChi()));
		
		PivasFormulaDrug pivasFormulaDrug = pivasFormulaMethod.getPivasFormulaMethodDrugList().get(0).getPivasFormulaDrug();
		
		String itemDesc = pivasFormulaDrug.getDisplayname();
		if (pivasFormulaDrug.getSaltProperty() != null) {
			itemDesc += " " + pivasFormulaDrug.getSaltProperty();
		}
		if (pivasFormulaDrug.getFormCode() != null) {
			DmForm dmForm = dmFormCacher.getFormByFormCode(pivasFormulaDrug.getFormCode());
			if (dmForm != null) {
				itemDesc += " " + dmForm.getLabelDesc();
			}
		}
		if (pivasFormulaDrug.getStrength() != null) {
			itemDesc += " " + pivasFormulaDrug.getStrength();
		}
		mdsOverrideReasonLabel.setItemDesc(itemDesc);
		
		mdsOverrideReasonLabel.setWard(pharmOrderItem.getPharmOrder().getWardCode());
		
		if (pharmOrderItem.getPharmOrder().getMedCase() != null) {
			mdsOverrideReasonLabel.setCaseNum(pharmOrderItem.getPharmOrder().getMedCase().getCaseNum());
			mdsOverrideReasonLabel.setBedNum(pharmOrderItem.getPharmOrder().getMedCase().getPasBedNum());
		}
		
		mdsOverrideReasonLabel.setSpecCode(pharmOrderItem.getPharmOrder().getSpecCode());
		
		if (dispOrderItem.isLegacy()) {
			mdsOverrideReasonLabel.setHospName(dispOrderItem.getDispOrder().getWorkstore().getHospCode());
			mdsOverrideReasonLabel.setHospNameChi(StringUtils.EMPTY);
		} else {
			mdsOverrideReasonLabel.setHospName( this.getHospitalNameEng(LABEL_DISPLABEL_HOSPITAL_NAMEENG.get()) );
			mdsOverrideReasonLabel.setHospNameChi(LABEL_DISPLABEL_HOSPITAL_NAMECHI.get());
		}
		
		mdsOverrideReasonLabel.setDispDate(dispOrderItem.getDispOrder().getDispDate());
		mdsOverrideReasonLabel.setBinNum(dispOrderItem.getBinNum()); 
		
		//No FloatingDrugName in PIVAS
		mdsOverrideReasonLabel.setFloatingDrugName(null);
		
		if (pharmOrderItem.getMedOrderItem() != null) {
			mdsOverrideReasonLabel.setActionStatus(pharmOrderItem.getActionStatus());
			mdsOverrideReasonLabel.setAlertFlag(pharmOrderItem.getMedOrderItem().getAlertFlag()==null?false:pharmOrderItem.getMedOrderItem().getAlertFlag());
		}
		

		if (dispOrderItem.getDispOrder().getTicket() != null) {
			int batchDay = new DateTime(dispOrderItem.getDispOrder().getTicket().getTicketDate()).getDayOfMonth();
			mdsOverrideReasonLabel.setBatchNum( (( batchDay < 10 )?"0":"") + batchDay + dispOrderItem.getDispOrder().getTicket().getTicketNum());
		}
		
		//No Urgent Flag in PIVAS MdsOverrideReason Label
		mdsOverrideReasonLabel.setUrgentFlag(false);
		mdsOverrideReasonLabel.setHkid(pharmOrderItem.getPharmOrder().getPatient().getHkid());
		mdsOverrideReasonLabel.setSingleOrderFlag(dispOrderItem.getSingleDispFlag()==null?false:dispOrderItem.getSingleDispFlag());
		mdsOverrideReasonLabel.setNewOrderFlag(dispOrderItem.getDispOrder().getRefillFlag()==null?false:!dispOrderItem.getDispOrder().getRefillFlag());
		mdsOverrideReasonLabel.setPrivateFlag(pharmOrderItem.getPharmOrder().getPrivateFlag());
		//No Replenish RR Indicator in PIVAS MdsOverrideReason Label
		mdsOverrideReasonLabel.setReplenishFlag(false);
		
		//The following fields are not shown PIVAS version MdsOverrideReason Label
		mdsOverrideReasonLabel.setItemNum(null);
		mdsOverrideReasonLabel.setItemCode(null);
		mdsOverrideReasonLabel.setIssueQty(null);
		mdsOverrideReasonLabel.setBaseUnit(null);
		
		if ( StringUtils.isNotBlank(dispOrderItem.getVerifyUser())){
			mdsOverrideReasonLabel.setUserCode(getMaskedUserCode(dispOrderItem.getVerifyUser()));
		} else {
			mdsOverrideReasonLabel.setUserCode(getMaskedUserCode(uamInfo.getUserId()));
		}

		//for label sorting
		mdsOverrideReasonLabel.setTrxId(convertToString(dispOrderItem.getDeliveryItemId()));

		return mdsOverrideReasonLabel;
	}
	
	// PMS-5562
	public String extractOrderSupplFreqDesc(MedProfileMoItem medProfileMoItem) {
		String supplFreqDesc = null;
		
		if (medProfileMoItem.getRxItemType() != RxItemType.Iv) {
			Dose firstDose = pivasWorklistManager.extractOrderFirstDose(medProfileMoItem);
			
			// For non-IPMOE order, frequency object exists even there is no supplFreq
			// need to check supplFreq existence by supplFreqCode or empty supplFreqDesc
			if (firstDose != null && firstDose.getSupplFreq() != null && 
				StringUtils.isNotBlank(firstDose.getSupplFreq().getCode()) && StringUtils.isNotBlank(firstDose.getSupplFreq().getDesc())) {
				
				Freq supplFreq = firstDose.getSupplFreq();
				// display of supplFreqCode 00021 in PIVAS reports are different from front-end screen
				// which Monday is the first one instead of Sunday in the raw supplFreqDesc
				if ( SUPPL_FREQ_CODE_WEEKDAY.equals(supplFreq.getCode()) && supplFreq.getParam() != null && supplFreq.getParam().length > 0) {
					supplFreqDesc = getSupplFreqDescForWeekday(supplFreq.getParam()[0]);
				} else {
					supplFreqDesc = firstDose.getSupplFreq().getDesc();
				}
			}
		}
		
		if (supplFreqDesc != null) {
			supplFreqDesc = supplFreqDesc.toLowerCase();
		}
		
		return supplFreqDesc;
	}
	
	private String getSupplFreqDescForWeekday(String param) {

		StringBuilder supplFreqDesc = new StringBuilder();
		supplFreqDesc.append("on every").append(SPACE);
		
		boolean firstDay = true;
		int checkIndex;
		for (int i = 0; i < param.length(); i++) {
			checkIndex = (i+1)%7;
			if (param.charAt(checkIndex) == '1') {
				if (!firstDay) {
					supplFreqDesc.append(COMMMA_SPACE);
				} else {
					firstDay = false;
				}
				supplFreqDesc.append(DAY_ARRAY[i]);
			}
		}
		
		return supplFreqDesc.toString();
	}
	
	private String getMaskedUserCode(String userCode){
		if ( userCode == null ) {
			return StringUtils.EMPTY;
		}
		int numOfMaskChars = LABEL_DISPLABEL_UPDATEUSER_CHARACTER_MASK.get(3);
		String maskedUserCode;
		if (userCode.length() < numOfMaskChars) {
			maskedUserCode = StringUtils.repeat("*", userCode.length());
		} else {
			maskedUserCode = userCode.substring(0, userCode.length() - numOfMaskChars).concat(StringUtils.repeat("*", numOfMaskChars));	
		}
		return maskedUserCode;
	}
	
	private String getHospitalNameEng(String fullHospitalNameEng){
		if( fullHospitalNameEng.length() >= 28 ){
			String[] hospitalNameEngArray = fullHospitalNameEng.split(" ");
			StringBuilder hospitalNameEngBuilder = new StringBuilder();
			for( String block : hospitalNameEngArray ){
				if( hospitalNameEngBuilder.length() != 0 ){
					hospitalNameEngBuilder.append(" ");
				}
				if( hospitalNameEngBuilder.length() + block.length() <= 24 ){
					hospitalNameEngBuilder.append(block);
				}else{
					break;
				}
			}
			hospitalNameEngBuilder.append("...");
			return hospitalNameEngBuilder.toString();
		}else{
			return fullHospitalNameEng;
		}
	}
	
	// PMSIPU-2601
	private String getTruncatedStrengthVolume(String strength, String volumeValue, String volumeUnit) {
		Integer strLenStartToTruncate = Integer.valueOf(20);
		String fullStr = "";
		
		if (StringUtils.isNotBlank(strength)) {
			fullStr += strength;
		}
		
		if (StringUtils.isNotBlank(volumeValue) && StringUtils.isNotBlank(volumeUnit)) {
			if (fullStr.length() > 0) {
				fullStr += SPACE;
			}
			fullStr += volumeValue + volumeUnit;
		}
		
		if (fullStr.length() > strLenStartToTruncate) {
			return fullStr.substring(0, strLenStartToTruncate) + "...";
		} else {
			return null;
		}
	}
	
	private String calOrderDosageSplit(BigDecimal orderDosage, Integer splitCount) {
		BigDecimal orderDosageSplit = orderDosage.divide(new BigDecimal (splitCount), 4, RoundingMode.HALF_UP);
		return new DecimalFormat("#.####").format(orderDosageSplit);
	}
	
	private String calFinalVolumeSplit(BigDecimal volume, Integer splitCount) {
		BigDecimal finalVolumeSplit = volume.divide(new BigDecimal (splitCount), 2, RoundingMode.HALF_UP);
		return new DecimalFormat("#.##").format(finalVolumeSplit);
	}
	
	private <N extends Number> String convertToString(N value) {
		if ( value == null ) {
			return StringUtils.EMPTY;
		}
		String str = null;
		
		if (value instanceof Integer) {
			str = value.toString();
		} else if (value instanceof Long) {
			str = value.toString();
		} else if (value instanceof Double) {
			Double decimalValue =  (Double) value - value.intValue();
			if ( decimalValue == 0 ) {
				str =  Integer.toString(value.intValue());
			} else {
				str = value.toString();
			}
		} else if (value instanceof BigDecimal) {
			Double decimalValue =  value.doubleValue() - value.intValue();
			if ( decimalValue == 0 ) {
				str = Integer.toString(value.intValue());
			} else {
				str = ((BigDecimal) value).stripTrailingZeros().toPlainString();
			}
		}
		
		return str;
	}
}
