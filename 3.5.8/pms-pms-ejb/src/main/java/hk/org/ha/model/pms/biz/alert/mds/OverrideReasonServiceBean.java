package hk.org.ha.model.pms.biz.alert.mds;

import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.sys.SysProfile;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.AlertEntity;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedOrderItemAlert;
import hk.org.ha.model.pms.uam.udt.AuthorizeStatus;
import hk.org.ha.model.pms.uam.vo.AuthenticateCriteria;
import hk.org.ha.model.pms.vo.alert.mds.AlertDdim;
import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;
import hk.org.ha.model.pms.vo.alert.mds.MedProfileAlertMsg;
import hk.org.ha.model.pms.vo.alert.mds.OverrideReasonResult;
import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;
import hk.org.ha.service.biz.pms.uam.interfaces.UamServiceJmsRemote;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.security.Identity;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("overrideReasonService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class OverrideReasonServiceBean implements OverrideReasonServiceLocal {

	private static final String ACK_RIGHT = "ackMds";
	
	private static final String OVERRIDE_RIGHT = "overrideMds";
	
	@In
	private UamInfo uamInfo;

	@In
	private SysProfile sysProfile;

	@In(required = false)
	private OrderViewInfo orderViewInfo;
	
	@In
	private Identity identity;
	
	@In
	private UamServiceJmsRemote uamServiceProxy;
	
	@In(required = false)
	private List<MedProfileAlertMsg> prescAlertMsgList;
	
	public OverrideReasonResult acknowledgeOverrideReasonForOp() {

		OverrideReasonResult overrideReasonResult = new OverrideReasonResult();
		
		if (checkAcknowledgePermissionForOp()) {
			setAckUserInfo(getMedOrderItem(), overrideReasonResult);
			overrideReasonResult.setSuccessFlag(true);
		}
		else {
			overrideReasonResult.setSuccessFlag(false);
		}
		
		return overrideReasonResult;
	}
	
	public OverrideReasonResult acknowledgeOverrideReasonForIp(String ackUser) {
		
		OverrideReasonResult overrideReasonResult = new OverrideReasonResult();
		String medProfileAckUser = ackUser;

		if (medProfileAckUser != null) {
			overrideReasonResult.setSuccessFlag(true);
		}
		else if (sysProfile.isDebugEnabled() || identity.hasPermission(ACK_RIGHT, "Y")) {
			medProfileAckUser = uamInfo.getUserId();
			overrideReasonResult.setSuccessFlag(true);
		}
		else {
			overrideReasonResult.setSuccessFlag(false);
		}

		if (overrideReasonResult.isSuccessFlag()) {
			overrideReasonResult.setAckUser(medProfileAckUser);
			overrideReasonResult.setAckDate(Calendar.getInstance().getTime());
		}
		
		return overrideReasonResult;
	}
	
	public OverrideReasonResult editItemReason(List<String> overrideReason, Integer ddiAlertIndex, Boolean orderEditFlag) {
		
		OverrideReasonResult overrideReasonResult = new OverrideReasonResult();
		
		if (checkOverridePermission()) {
			setItemOverrideReason(getMedOrderItem(), overrideReason, ddiAlertIndex, orderEditFlag, overrideReasonResult);
			overrideReasonResult.setSuccessFlag(true);
		}
		else {
			overrideReasonResult.setSuccessFlag(false);
		}
		
		return overrideReasonResult;
	}

	public OverrideReasonResult editPrescReason(List<String> overrideReason, Integer alertIndex) {
		
		OverrideReasonResult overrideReasonResult = new OverrideReasonResult();
		
		if (checkOverridePermission()) {
			setPrescOverrideReason(prescAlertMsgList.get(alertIndex), overrideReason);
			overrideReasonResult.setSuccessFlag(true);
		}
		else {
			overrideReasonResult.setSuccessFlag(false);
		}
		
		return overrideReasonResult;
	}
	
	private boolean checkAcknowledgePermissionForOp() {
		
		if (orderViewInfo.getAcknowledgeReasonUser() != null) {
			return true;
		}
		
		else if (sysProfile.isDebugEnabled() || identity.hasPermission(ACK_RIGHT, "Y")) {
			orderViewInfo.setAcknowledgeReasonUser(uamInfo.getUserId());
			return true;
		}

		return false;
	}
	
	private boolean checkOverridePermission() {
		
		if (orderViewInfo.getOverrideAlertUser() != null) {
			return true;
		}
		
		else if (sysProfile.isDebugEnabled() || identity.hasPermission(OVERRIDE_RIGHT, "Y")) {
			orderViewInfo.setOverrideAlertUser(uamInfo.getUserId());
			return true;
		}
	
		return false;
	}
	
	public OverrideReasonResult retrieveOverridePermission() {
		OverrideReasonResult overrideReasonResult = new OverrideReasonResult();
		overrideReasonResult.setSuccessFlag(checkOverridePermission());
		return overrideReasonResult;
	}

	private void setAckUserInfo(MedOrderItem medOrderItem, OverrideReasonResult overrideReasonResult) {
		
		Date currentTime = Calendar.getInstance().getTime();
		
		for (MedOrderItemAlert medOrderItemAlert : medOrderItem.getMedOrderItemAlertList()) {
			if (medOrderItemAlert.isMdsAlert()) {
				medOrderItemAlert.setAckUser(orderViewInfo.getAcknowledgeReasonUser());
				medOrderItemAlert.setAckDate(currentTime);
			}
		}
		
		medOrderItem.setChangeFlag(Boolean.TRUE);
		
		overrideReasonResult.setAckUser(orderViewInfo.getAcknowledgeReasonUser());
		overrideReasonResult.setAckDate(currentTime);
	}
	
	private void setItemOverrideReason(MedOrderItem medOrderItem, List<String> overrideReason, Integer ddiAlertIndex, Boolean orderEditFlag, OverrideReasonResult overrideReasonResult) {

		Date currentTime = Calendar.getInstance().getTime();
		
		overrideReasonResult.setAckUser(orderViewInfo.getOverrideAlertUser());
		overrideReasonResult.setAckDate(currentTime);

		if (orderEditFlag) {
			return;
		}
		
		for (MedOrderItemAlert medOrderItemAlert : getMedOrderItemAlert(medOrderItem, ddiAlertIndex)) {
			medOrderItemAlert.setOverrideReason(overrideReason);
			medOrderItemAlert.setAckUser(orderViewInfo.getOverrideAlertUser());
			medOrderItemAlert.setAckDate(currentTime);
			medOrderItemAlert.getMedOrderItem().setChangeFlag(Boolean.TRUE);
		}
	}
	
	private void setPrescOverrideReason(MedProfileAlertMsg medProfileAlertMsg, List<String> overrideReason) {
		
		Date currentTime = Calendar.getInstance().getTime();
		
		Set<AlertEntity> alertSet = new HashSet<AlertEntity>();
		for (AlertMsg alertMsg : medProfileAlertMsg.getAlertMsgList()) {
			alertSet.addAll(alertMsg.getPatAlertList());
			alertSet.addAll(alertMsg.getDdiAlertList());
		}
		
		for (AlertEntity alertEntity : alertSet) {
			alertEntity.setOverrideReason(overrideReason);
			alertEntity.setAckUser(orderViewInfo.getOverrideAlertUser());
			alertEntity.setAckDate(currentTime);
		}
	}
	
	public OverrideReasonResult authenticateAcknowledgePermissionForOp(String userName, String password) {
		
		OverrideReasonResult overrideReasonResult = new OverrideReasonResult();
		
		switch (authenticateUam(userName, password, ACK_RIGHT)) {
		
			case Succeed:
				overrideReasonResult.setSuccessFlag(true);
				orderViewInfo.setAcknowledgeReasonUser(userName);
				break;
				
			case Invalid:
				overrideReasonResult.setSuccessFlag(false);
				overrideReasonResult.setMsgCode("0091");
				break;
			
			case NoAccess:
				overrideReasonResult.setSuccessFlag(false);
				overrideReasonResult.setMsgCode("0234");
				break;
		}
		
		return overrideReasonResult;
	}
	
	public OverrideReasonResult authenticateAcknowledgePermissionForIp(String userName, String password) {
		
		OverrideReasonResult overrideReasonResult = new OverrideReasonResult();
		
		switch (authenticateUam(userName, password, ACK_RIGHT)) {
		
			case Succeed:
				overrideReasonResult.setSuccessFlag(true);
				overrideReasonResult.setAckUser(userName);
				break;
				
			case Invalid:
				overrideReasonResult.setSuccessFlag(false);
				overrideReasonResult.setMsgCode("0091");
				break;
			
			case NoAccess:
				overrideReasonResult.setSuccessFlag(false);
				overrideReasonResult.setMsgCode("0234");
				break;
		}
		
		return overrideReasonResult;
	}
	
	public OverrideReasonResult authenticateOverridePermission(String userName, String password) {
		
		OverrideReasonResult overrideReasonResult = new OverrideReasonResult();
		
		switch (authenticateUam(userName, password, OVERRIDE_RIGHT)) {

			case Succeed:
				overrideReasonResult.setSuccessFlag(true);
				orderViewInfo.setOverrideAlertUser(userName);
				break;
			
			case Invalid:
				overrideReasonResult.setSuccessFlag(false);
				overrideReasonResult.setMsgCode("0091");
				break;
			
			case NoAccess:
				overrideReasonResult.setSuccessFlag(false);
				overrideReasonResult.setMsgCode("0235");
				break;
		}
		
		return overrideReasonResult;
	}
	
	private AuthorizeStatus authenticateUam(String userName, String password, String right) {

		if ("".equals(userName) || "".equals(password)) {
			return AuthorizeStatus.Invalid;
		}
		
		AuthenticateCriteria authenticateCriteria = new AuthenticateCriteria();
		authenticateCriteria.setProfileCode(uamInfo.getProfileCode());
		authenticateCriteria.setUserName(userName);
		authenticateCriteria.setPassword(password);
		authenticateCriteria.setTarget(right);
		authenticateCriteria.setAction("Y");

		return uamServiceProxy.authenticate(authenticateCriteria);
	}
	
	private MedOrderItem getMedOrderItem() {
		return (MedOrderItem) Contexts.getSessionContext().get("mdsMedOrderItem");
	}

	private List<MedOrderItemAlert> getMedOrderItemAlert(MedOrderItem medOrderItem, Integer ddiAlertIndex) {

		if (ddiAlertIndex == null) { // patient-specific alert
			return medOrderItem.getPatAlertList();
		}
		
		MedOrderItemAlert moiAlert = medOrderItem.getDdiAlertList().get(ddiAlertIndex);
		if (((AlertDdim) moiAlert.getAlert()).isOnHand()) {
			return Arrays.asList(moiAlert);
		}
		else {
			return medOrderItem.getMedOrder().getMedOrderItemAlertListByHashCode(moiAlert.getAlert().hashCode());
		}
	}
	
	public String authenticateMpPermission(String userName, String password, String permission) {
		return authenticateUam(userName, password, permission).getDataValue();
	}
	
	@Remove
	public void destroy() {
	}
}
