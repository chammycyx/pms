package hk.org.ha.model.pms.vo.pivas;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PivasWardInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<String> patHospCodeList = new ArrayList<String>();
	
	private List<PivasWardItem> pivasWardItemList = new ArrayList<PivasWardItem>();
	
	public List<String> getPatHospCodeList() {
		return patHospCodeList;
	}
	public void setPatHospCodeList(List<String> patHospCodeList) {
		this.patHospCodeList = patHospCodeList;
	}
	public List<PivasWardItem> getPivasWardItemList() {
		return pivasWardItemList;
	}
	public void setPivasWardItemList(List<PivasWardItem> pivasWardItemList) {
		this.pivasWardItemList = pivasWardItemList;
	}
}
