package hk.org.ha.model.pms.biz.reftable;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.PmsPcuMapping;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import javax.ejb.Remove;
import javax.ejb.Stateless;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateless
@Name("qtyDurationConversionWorkstoreListManager")
@Restrict("#{identity.loggedIn}")
@MeasureCalls
public class QtyDurationConversionWorkstoreListManagerBean implements QtyDurationConversionWorkstoreListManagerLocal {

	@In
	private Workstore workstore;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@SuppressWarnings("unchecked")
	public List<PmsPcuMapping> retrievePmsPcuMapping() 
	{
		return dmsPmsServiceProxy.retrievePmsPcuMappingList(
				workstore.getHospCode(),workstore.getWorkstoreCode());
	}
}
