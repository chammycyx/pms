package hk.org.ha.model.pms.persistence.phs;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Institution;
import hk.org.ha.model.pms.udt.RecordStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "SPECIALTY")
@IdClass(SpecialtyPK.class)
public class Specialty extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "INST_CODE", length = 3)
	private String instCode;
	
	@Id
	@Column(name = "SPEC_CODE", length = 4)
	private String specCode;
	
	@Column(name = "DESCRIPTION", length = 100)
	private String description;

    @Converter(name = "Specialty.status", converterClass = RecordStatus.Converter.class)
    @Convert("Specialty.status")
	@Column(name = "STATUS", nullable = false, length = 1)
	private RecordStatus status;
        
    @ManyToOne
    @JoinColumn(name = "INST_CODE", nullable = false, insertable = false, updatable = false)
    private Institution institution;

    public Specialty() {
		status = RecordStatus.Active;
	}
	
	public Specialty(String instCode, String specCode) {
		this();
		this.instCode = instCode;
		this.specCode = specCode;
	}

	public Specialty(String instCode, String specCode, String description, String status) {
		this(instCode, specCode);
    	this.description = description;
    	if( "Y".equals(status) ){
    		this.status = RecordStatus.Suspend;
    	}else{
    		this.status = RecordStatus.Active;
    	}
	}

	public String getInstCode() {
		return instCode;
	}

	public void setInstCode(String instCode) {
		this.instCode = instCode;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public RecordStatus getStatus() {
		return status;
	}

	public void setStatus(RecordStatus status) {
		this.status = status;
	}

	public Institution getInstitution() {
		return institution;
	}

	// helper methods
	public SpecialtyPK getId() {
		return new SpecialtyPK(this.getInstCode(), this.getSpecCode());
	}
	
	@Override
	public int hashCode() {		
		return new HashCodeBuilder()
			.append(instCode)
			.append(specCode)
			.toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		Specialty other = (Specialty) obj;
		return new EqualsBuilder()
			.append(instCode, other.getInstCode())
			.append(specCode, other.getSpecCode())
			.isEquals();
	}
}
