package hk.org.ha.model.pms.vo.medprofile;

import java.io.Serializable;

import hk.org.ha.model.pms.persistence.corp.Workstore;


public class PasParams implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Boolean updateBodyInfo;
	private Boolean updateItemTransferInfo;
	private Workstore workstore;
	private String patHospCode;
	private String patKey;
	private String hkid;
	private String caseNum;
	private String pasSpecCode;
	private String pasSubSpecCode;
	private String pasBedNum;
	private String pasWardCode;
    private Boolean privateFlag;
    private String wardCode;
    private String specCode;
    
    public PasParams() {
    	updateBodyInfo = Boolean.FALSE;
    	updateItemTransferInfo = Boolean.FALSE;
    }

	public Boolean getUpdateBodyInfo() {
		return updateBodyInfo;
	}

	public void setUpdateBodyInfo(Boolean updateBodyInfo) {
		this.updateBodyInfo = updateBodyInfo;
	}
	
	public Boolean getUpdateItemTransferInfo() {
		return updateItemTransferInfo;
	}

	public void setUpdateItemTransferInfo(Boolean updateItemTransferInfo) {
		this.updateItemTransferInfo = updateItemTransferInfo;
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}
	
	public String getHospCode() {
		return workstore == null ? null : workstore.getHospCode();
	}

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}
	
	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getPatKey() {
		return patKey;
	}

	public void setPatKey(String patKey) {
		this.patKey = patKey;
	}
	
	public String getPasSpecCode() {
		return pasSpecCode;
	}
	
	public void setPasSpecCode(String pasSpecCode) {
		this.pasSpecCode = pasSpecCode;
	}
	
	public String getPasSubSpecCode() {
		return pasSubSpecCode;
	}
	
	public void setPasSubSpecCode(String pasSubSpecCode) {
		this.pasSubSpecCode = pasSubSpecCode;
	}

	public String getPasBedNum() {
		return pasBedNum;
	}

	public void setPasBedNum(String pasBedNum) {
		this.pasBedNum = pasBedNum;
	}

	public String getPasWardCode() {
		return pasWardCode;
	}

	public void setPasWardCode(String pasWardCode) {
		this.pasWardCode = pasWardCode;
	}
	
	public Boolean getPrivateFlag() {
		return privateFlag;
	}

	public void setPrivateFlag(Boolean privateFlag) {
		this.privateFlag = privateFlag;
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}
}
