package hk.org.ha.model.pms.biz.reftable;
import java.util.Date;

import javax.ejb.Local;

@Local
public interface HospitalClusterTaskManagerLocal {

	boolean lockHospitalClusterTask(String taskName, Date date);
	
	boolean lockHospitalClusterTask(String taskName, Date date, long graceTime);
}
