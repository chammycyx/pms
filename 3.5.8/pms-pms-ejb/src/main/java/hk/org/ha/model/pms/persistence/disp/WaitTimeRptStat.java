package hk.org.ha.model.pms.persistence.disp;

import hk.org.ha.fmk.pms.entity.CreateDate;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlTransient;

import org.joda.time.DateTime;

@Entity
@Table(name = "WAIT_TIME_RPT_STAT")
public class WaitTimeRptStat {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "waitTimeRptStatSeq")
	@SequenceGenerator(name = "waitTimeRptStatSeq", sequenceName = "SQ_WAIT_TIME_RPT_STAT", initialValue = 100000000)
	private Long id;
	
	@Column(name = "HOSP_CODE", length=3)
	private String hospCode;
	
	@Column(name = "WORKSTORE_CODE", length=4)
	private String workstoreCode;
	
	@Column(name = "YEAR")
	private Integer year;
	
	@Column(name = "MONTH")
	private Integer month;
	
	@Column(name = "DAY")
	private Integer day;
	
	@Column(name = "DAY_OF_WEEK")
	private Integer dayOfWeek;
	
	@Column(name = "HOUR")
	private Integer hour;
	
	@Column(name = "WAIT_TIME")
	private Integer waitTime;

	@Column(name = "BATCH_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date batchDate;
	
	@CreateDate
	@Column(name = "CREATE_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@XmlTransient
	// change to public to avoid exception from Reflections.setAndWrap(Reflections.java:131)	
	private Date createDate;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public void setDayOfWeek(Integer dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public Integer getDayOfWeek() {
		return dayOfWeek;
	}

	public Integer getHour() {
		return hour;
	}

	public void setHour(Integer hour) {
		this.hour = hour;
	}

	public Integer getWaitTime() {
		return waitTime;
	}

	public void setWaitTime(Integer waitTime) {
		this.waitTime = waitTime;
	}
	
	public Date getCreateDate() {
		return cloneDate(createDate);
	}

	public void setCreateDate(Date createDate) {
		this.createDate = cloneDate(createDate);
	}
	
	public Date getBatchDate() {		
		return cloneDate(batchDate);
	}
	
	private Date cloneDate(Date date) {
		return (date != null) ? new Date(date.getTime()) : null;
	}

	public void setBatchDate(Date batchDate) {
		this.batchDate = cloneDate(batchDate);
	}
}
