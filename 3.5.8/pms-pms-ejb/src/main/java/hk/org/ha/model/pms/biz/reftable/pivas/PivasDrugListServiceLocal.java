package hk.org.ha.model.pms.biz.reftable.pivas;

import java.util.List;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrug;
import hk.org.ha.model.pms.dms.vo.pivas.PivasDrugValidateCriteria;
import hk.org.ha.model.pms.dms.vo.pivas.PivasDrugValidateResponse;
import hk.org.ha.model.pms.vo.reftable.pivas.PivasDrugListInfo;

import javax.ejb.Local;

@Local
public interface PivasDrugListServiceLocal {

	DmDrug retrieveDrugByItemCode(String itemCode);
		
	PivasDrugListInfo retrievePivasDrugListInfo(DmDrug dmDrug);
	
	List<PivasDrug> retrievePivasDrugListBySolventCode(String solventCode);
	
	PivasDrugValidateResponse updatePivasDrugList(List<PivasDrug> pivasDrugList, PivasDrugValidateCriteria pivasDrugValidateCriteria);
	
	Boolean retrievePivasDrugRptList();

	void exportPivasDrugRpt();
	
	void destroy();
	
}
 