package hk.org.ha.model.pms.udt.alert;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum AllergenType implements StringValuedEnum {
	
	Previous          ("P", "Previous Allergy"),
	FreeText          ("O", "Free Text Drug Allergy"),
	DrugAllergy       ("D", "Drug Allergy"),
	AllergenGroup     ("G", "Allergen Group"),
	NonDrug           ("X", "Free Text Non-Drug Allergy"),
	NoKnownDrugAllergy("N", "No Known Drug Allergy"),
	XP                ("XP", "");
	
    private final String dataValue;
    private final String displayValue;
    
	AllergenType(final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}

	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}

	public static AllergenType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(AllergenType.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<AllergenType> {
		
		private static final long serialVersionUID = 1L;

		public Class<AllergenType> getEnumClass() {
    		return AllergenType.class;
    	}
    }
	
}
