package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmRegimenCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmRegimen;

import java.util.List;

import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("dmRegimenListManager")
@MeasureCalls
public class DmRegimenListManagerBean implements DmRegimenListManagerLocal {

	@Logger
	private Log logger;

	@In
	private DmRegimenCacherInf dmRegimenCacher;

	@SuppressWarnings("unused")
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private List<DmRegimen> dmRegimenList;	
	
	public void retrieveDmRegimenList(String prefixRegimenCode) 
	{
		logger.debug("retrieveDmRegimenList");
				
		if(StringUtils.isBlank(prefixRegimenCode)){
			dmRegimenList = dmRegimenCacher.getRegimenList();
		}else{
			dmRegimenList = dmRegimenCacher.getRegimenListByRegimenCode(prefixRegimenCode);
		}
	}
}
