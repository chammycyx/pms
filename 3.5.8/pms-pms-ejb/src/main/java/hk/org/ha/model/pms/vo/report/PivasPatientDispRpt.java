package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class PivasPatientDispRpt implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private PivasRptHdr pivasRptHdr;
	
	private List<PivasPatientDispRptDtl> pivasPatientDispRptDtlList;

	
	public PivasRptHdr getPivasRptHdr() {
		return pivasRptHdr;
	}

	public void setPivasRptHdr(PivasRptHdr pivasRptHdr) {
		this.pivasRptHdr = pivasRptHdr;
	}

	public List<PivasPatientDispRptDtl> getPivasPatientDispRptDtlList() {
		return pivasPatientDispRptDtlList;
	}

	public void setPivasPatientDispRptDtlList(
			List<PivasPatientDispRptDtl> pivasPatientDispRptDtlList) {
		this.pivasPatientDispRptDtlList = pivasPatientDispRptDtlList;
	}

}
