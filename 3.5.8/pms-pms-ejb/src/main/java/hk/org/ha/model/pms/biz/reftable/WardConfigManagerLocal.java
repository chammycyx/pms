package hk.org.ha.model.pms.biz.reftable;
import java.util.List;

import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
import hk.org.ha.model.pms.persistence.reftable.WardConfig;

import javax.ejb.Local;

@Local
public interface WardConfigManagerLocal {

	WardConfig retrieveWardConfig(WorkstoreGroup workstoreGroup, String wardCode);
	
	List<WardConfig> retrieveMpWardConfigList(WorkstoreGroup workstoreGroup);
	
	List<WardConfig> retrieveWardConfigList(WorkstoreGroup workstoreGroup, List<String> wardCodeList);
}
