package hk.org.ha.model.pms.vo.checkissue.mp;

import hk.org.ha.model.pms.persistence.medprofile.Delivery;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class AssignBatchResult implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Date batchDate;
	
	private String batchNum;
	
	private String wardCode;
	
	private Delivery delivery;
	
	List<DeliveryItem> deliveryItemWithCheckedStatusList;
	
	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public Delivery getDelivery() {
		return delivery;
	}

	public void setDelivery(Delivery delivery) {
		this.delivery = delivery;
	}

	public List<DeliveryItem> getDeliveryItemWithCheckedStatusList() {
		return deliveryItemWithCheckedStatusList;
	}

	public void setDeliveryItemWithCheckedStatusList(
			List<DeliveryItem> deliveryItemWithCheckedStatusList) {
		this.deliveryItemWithCheckedStatusList = deliveryItemWithCheckedStatusList;
	}

	public void setBatchDate(Date batchDate) {
		this.batchDate = batchDate;
	}

	public Date getBatchDate() {
		return batchDate;
	}
}