package hk.org.ha.model.pms.biz.refill;

import static hk.org.ha.model.pms.prop.Prop.REFILL_STANDARD_ENABLED;
import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.reftable.RefillHolidayManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmRegimenCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmRegimen;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.persistence.disp.RefillScheduleItem;
import hk.org.ha.model.pms.persistence.reftable.DrsConfig;
import hk.org.ha.model.pms.persistence.reftable.DrsPatient;
import hk.org.ha.model.pms.persistence.reftable.RefillConfig;
import hk.org.ha.model.pms.persistence.reftable.RefillSelection;
import hk.org.ha.model.pms.udt.disp.DiscontinueStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.patient.PasSpecialtyType;
import hk.org.ha.model.pms.udt.refill.DocType;
import hk.org.ha.model.pms.udt.refill.RefillScheduleRefillStatus;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.udt.vetting.RegimenDurationUnit;
import hk.org.ha.model.pms.udt.vetting.RegimenType;
import hk.org.ha.model.pms.vo.rx.CapdItem;
import hk.org.ha.model.pms.vo.rx.CapdRxDrug;
import hk.org.ha.model.pms.vo.rx.IvRxDrug;
import hk.org.ha.model.pms.vo.rx.Regimen;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.ObjectUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("refillScheduleManager")
@MeasureCalls
public class RefillScheduleManagerBean implements RefillScheduleManagerLocal {
		
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In
	private Workstore workstore;
	
	@In
	private RefillHolidayManagerLocal refillHolidayManager;
	
	@In
	private DmRegimenCacherInf dmRegimenCacher;
	
	public RefillScheduleRefillStatus retrieveInitRefillStatusByRefillNum( Integer refillNumIn ){
		if( refillNumIn == 1 ){
			return RefillScheduleRefillStatus.Current;				
		}else if( refillNumIn == 2 ){
			return RefillScheduleRefillStatus.Next;
		}else{
			return RefillScheduleRefillStatus.None;			
		}
	}

	//Order Base
	public boolean isAllowStdRefill(PharmOrder pharmOrder, RefillConfig refillConfig, boolean isAutoGen, boolean drsFlag){
		if( !REFILL_STANDARD_ENABLED.get(Boolean.FALSE) ){
			return false;
		}
			
		if(!isAutoGen){
			return true;
		}
		
		if( drsFlag ){
			return true;
		}
		
		String pasSpecCode = null;
		if( pharmOrder.getMedCase() != null ){
			pasSpecCode = pharmOrder.getMedCase().getPasSpecCode();
		}
		
		logger.debug("isAllowStdRefill MedOrderDocType #0, ManualRx PasSpecFlag #1, pasSpecCode #2 ", pharmOrder.getMedOrder().getDocType(),refillConfig.getManuRxNoPasSpecFlag(),pasSpecCode);
		
		if( MedOrderDocType.Manual.equals( pharmOrder.getMedOrder().getDocType() ) &&
			pasSpecCode == null && !refillConfig.getManuRxNoPasSpecFlag()	){
			return false;
		}
		
		return true;
	}
		
	//Basic Refill Item Validation
	private boolean isValidRefillPharmOrderItem(PharmOrderItem pharmOrderItem){
		
		if ( !ActionStatus.DispByPharm.equals(pharmOrderItem.getActionStatus()) ){
			return false;
		}
		
		if ( (pharmOrderItem.getMedOrderItem().getMedOrder().getPrescType() == MedOrderPrescType.MpDischarge ||
				pharmOrderItem.getMedOrderItem().getMedOrder().getPrescType() == MedOrderPrescType.MpHomeLeave )&& 
				pharmOrderItem.getMedOrderItem().isSfi() ) {
			return false;
		}
		
		BigDecimal issueQty = BigDecimal.ZERO;
		BigDecimal issueQtyNoScale = BigDecimal.ZERO;
		if( pharmOrderItem.getIssueQty() != null ){
			issueQty = pharmOrderItem.getIssueQty();
			issueQtyNoScale = issueQty.setScale(0, RoundingMode.DOWN);
		}

		if( issueQty.compareTo(issueQtyNoScale) != 0 || issueQty.compareTo(BigDecimal.ONE) <= 0) {
			return false;			
		}
		
		// CAPD item
		if( pharmOrderItem.getMedOrderItem().getCapdRxDrug() != null ){
			return false;		
		}
		
		//Free Text Entry
		if( FmStatus.FreeTextEntryItem.getDataValue().equals(pharmOrderItem.getMedOrderItem().getFmStatus()) ){
			return false;
		}
		
		// StepUpDown - No Refill
		if( RegimenType.StepUpDown == pharmOrderItem.getRegimen().getType() ){
			return false;
		}
		
		//SFI Safety Net in Special item table
		
		//DiscontinueItem
		if( pharmOrderItem.getMedOrderItem().getDiscontinueStatus() != null && DiscontinueStatus.None != pharmOrderItem.getMedOrderItem().getDiscontinueStatus() ){
			return false;
		}
		
		return true;
	}
	
	public boolean isValidPharmOrderItemForStandardRefill(PharmOrderItem pharmOrderItem, boolean autoGenFlag, Integer refillThreshold, Integer lastIntervalMaxDay){
		if( !isValidRefillPharmOrderItem(pharmOrderItem) ){
			return false;
		}
		
		if( !autoGenFlag ){
			return true;
		}
		
		if( pharmOrderItem.getRegimen().getDurationInDay() == null ){
			return false;
		}

		if( pharmOrderItem.getRegimen().getDurationInDay() <= refillThreshold || pharmOrderItem.getRegimen().getDurationInDay() <= lastIntervalMaxDay ){
			return false;
		}		
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public List<PharmOrderItem> filterPharmOrderItemListByRefillSelection(List<PharmOrderItem> pharmOrderItemListIn, RefillConfig refillConfig){
		List<PharmOrderItem> pharmOrderItemResultList = new ArrayList<PharmOrderItem>();
		
		String medCasePasSpecCode = null;
		MedOrderPrescType medOrderPrescType = null;
		String pasSpecType = PasSpecialtyType.OutPatient.getDataValue();
		
		if( pharmOrderItemListIn.isEmpty() ){
			return pharmOrderItemListIn;
		}
		
		if( pharmOrderItemListIn.get(0).getPharmOrder().getMedCase() != null ){
			medCasePasSpecCode = pharmOrderItemListIn.get(0).getPharmOrder().getMedCase().getPasSpecCode();
		}
		medOrderPrescType = pharmOrderItemListIn.get(0).getPharmOrder().getMedOrder().getPrescType();
		
		if( MedOrderPrescType.REFILL_IP_PAS_SPEC.contains(medOrderPrescType) ){
			pasSpecType = PasSpecialtyType.InPatient.getDataValue();
		}
		
		List<RefillSelection> refillSelectionList = em.createQuery(
				"select o from RefillSelection o" + // 20120303 index check : RefillSelection.workstore,itemCode,pasSpecCode,type : UI_REFILL_SELECTION_01
				" where o.workstore = :workstore" +
				" and o.itemCode = :allItemCode" +
				" and o.pasSpecCode = :pasSpecCode" +
				" and o.type = :type")
				.setParameter("workstore", workstore)
				.setParameter("allItemCode", "*")
				.setParameter("pasSpecCode", medCasePasSpecCode)
				.setParameter("type", pasSpecType)
				.getResultList();
		
		if( refillSelectionList.size() > 0 ){
			return pharmOrderItemListIn;
		}else{
			
			List<String> itemCodeList = new ArrayList<String>();
			
			for( PharmOrderItem pharmOrderItem : pharmOrderItemListIn ){
				itemCodeList.add(pharmOrderItem.getItemCode());
			}
			
			refillSelectionList = em.createQuery(
					"select o from RefillSelection o" + // 20120303 index check : RefillSelection.workstore,itemCode,pasSpecCode,type : UI_REFILL_SELECTION_01
					" where o.workstore = :workstore" +
					" and o.itemCode in :itemCodeList" +
					" and ( o.pasSpecCode = :allSpecCode or ( o.pasSpecCode = :pasSpecCode and o.type = :type ) )")
					.setParameter("workstore", workstore)
					.setParameter("itemCodeList", itemCodeList)
					.setParameter("allSpecCode", "*")
					.setParameter("pasSpecCode", medCasePasSpecCode)
					.setParameter("type", pasSpecType)
					.getResultList();
			
			if( refillSelectionList.size() > 0 ){
				itemCodeList = new ArrayList<String>();
				for( RefillSelection refillSelect : refillSelectionList ){
					itemCodeList.add(refillSelect.getItemCode());
				}
				
				for( PharmOrderItem pharmOrderItem : pharmOrderItemListIn ){
					if( itemCodeList.contains(pharmOrderItem.getItemCode()) ){
						pharmOrderItemResultList.add(pharmOrderItem);
					}
				}
			}
		}
		return pharmOrderItemResultList;
	}

	public void removeRefillScheduleListByPharmOrderItemNum(PharmOrder pharmOrder, Integer pharmOrderItemNum){		
		pharmOrder.reconnectRefillScheduleList();
		List<RefillSchedule> refillScheduleList = pharmOrder.getRefillScheduleList(DocType.Standard);
		logger.debug("removeRefillScheduleListByPharmOrderItemNum refillScheduleList size: #0", refillScheduleList.size());
		pharmOrder.clearRefillScheduleList();
		pharmOrder.disconnectRefillScheduleList();
		
		for(Iterator<RefillSchedule> refillScheduleIterator = refillScheduleList.iterator(); refillScheduleIterator.hasNext();){			
			RefillSchedule refillSchedule = (RefillSchedule) refillScheduleIterator.next();
			for(Iterator<RefillScheduleItem> refillScheduleItemIterator = refillSchedule.getRefillScheduleItemList().iterator(); refillScheduleItemIterator.hasNext();){
				RefillScheduleItem refillScheduleItem = (RefillScheduleItem) refillScheduleItemIterator.next();
				if (refillScheduleItem.getItemNum().equals(pharmOrderItemNum)) {	//PoiEdit
					refillScheduleItem.getRefillSchedule().setPrintFlag(false);
					refillScheduleItemIterator.remove();
				}else{				
					PharmOrderItem pharmOrderItem = pharmOrder.getPharmOrderItemByOrgItemNum(refillScheduleItem.getItemNum());				
					if( pharmOrderItem == null ){	//MoiEdit
						refillScheduleItem.getRefillSchedule().setPrintFlag(false);
						refillScheduleItemIterator.remove();
					}
				}
			}
			
			if( refillSchedule.getRefillScheduleItemList().isEmpty() ){
				refillScheduleIterator.remove();
			}
		}
		
		for( RefillSchedule refillSchedule : refillScheduleList ){
			refillSchedule.connect(pharmOrder);				
		}
		
		logger.debug("updateMedOrderFromOrderEdit refillScheduleList size After : #0", refillScheduleList.size());
		
		pharmOrder.disconnectRefillScheduleList();
	}
	
	@SuppressWarnings("unchecked")
	public String retrieveManualMedOrderNumForCoupon(PharmOrder pharmOrder){
			if( pharmOrder.getMedOrder().getOrderNum() == null){
				return null;
			}
			
			if( pharmOrder.getMedOrder().getPrevOrderNum() == null ){
				return pharmOrder.getMedOrder().getOrderNum();
			}
		
			List<RefillSchedule> prevRsList = (List<RefillSchedule>) em.createQuery(
																	"select o from RefillSchedule o" + // 20120307 index check : RefillSchedule.pharmOrder : FK_REFILL_SCHEDULE_01
																	" where o.pharmOrder.id = :pharmOrderId")
																	.setParameter("pharmOrderId", pharmOrder.getId())
																	.getResultList();
			
			if( !prevRsList.isEmpty() ){
				for( RefillSchedule prevRs : prevRsList ){
					if( DocType.Standard == prevRs.getDocType() ){
						return prevRs.getRefillCouponNum().substring(0, 12);
					}else if( DocType.Sfi == prevRs.getDocType() ){
						return prevRs.getRefillCouponNum().substring(3, 15);
					}
				
				}
			}else{
				MedOrder prevMedOrder = pharmOrder.getMedOrder();
				while ( prevMedOrder.getPrevOrderNum() != null ){
				List<MedOrder> prevMedOrderList = (List<MedOrder>) em.createQuery(
						"select o from MedOrder o" + // 20120215 index check : MedOrder.orderNum : I_MED_ORDER_01
						" where o.orderNum = :orderNum" +
						" and o.orderType = :medOrderType" +							
						" and o.docType = :docType" +
						" order by o.orderDate desc")
						.setParameter("orderNum", prevMedOrder.getPrevOrderNum())
						.setParameter("medOrderType", OrderType.OutPatient)
						.setParameter("docType", MedOrderDocType.Manual)
						.getResultList();
				
					if( !prevMedOrderList.isEmpty() ){
						prevMedOrder = prevMedOrderList.get(0);
					}else{
						break;
					}
				}				
				return prevMedOrder.getOrderNum();				
			}
			return null;		
	}
	
	@SuppressWarnings("unchecked")
	public Map<DocType, List<RefillSchedule>> retrieveNextRefillScheduleList(DispOrder dispOrder) {
		Map<DocType, List<RefillSchedule>> nextRefillScheduleMap = new HashMap<DocType, List<RefillSchedule>>();
		
		List<RefillSchedule> standardRefillList = new ArrayList<RefillSchedule>();
		List<RefillSchedule> sfiRefillList = new ArrayList<RefillSchedule>();
		
		List<RefillSchedule> refillScheduleList = em.createQuery(
				"select o from RefillSchedule o" + // 20121112 index check : RefillSchedule.dispOrder : FK_REFILL_SCHEDULE_02
				" where o.dispOrder = :dispOrder")
				.setParameter("dispOrder", dispOrder)
				.getResultList();
		
		if ( refillScheduleList.size() > 0 ) {
			List<Integer> standardRefillGroupNumList = new ArrayList<Integer>();
			List<Integer> sfiRefillGroupNumList = new ArrayList<Integer>();
			for ( RefillSchedule rs : refillScheduleList ) {
				if ( rs.getDocType() == DocType.Sfi ) {
					sfiRefillGroupNumList.add(rs.getGroupNum());
				} else {
					standardRefillGroupNumList.add(rs.getGroupNum());
				}
			}
			
			if ( standardRefillGroupNumList.size() > 0 ) {
				standardRefillList = em.createQuery(
						"select o from RefillSchedule o" + // 20121112 index check : RefillSchedule.pharmOrder : FK_REFILL_SCHEDULE_01
						" where o.pharmOrder = :pharmOrder" +
						" and o.groupNum in :standardRefillGroupNumList" +
						" and o.refillStatus = :refillStatus" +
						" and o.docType = :docType" +
						" order by o.refillCouponNum")
						.setParameter("pharmOrder", dispOrder.getPharmOrder())
						.setParameter("standardRefillGroupNumList", standardRefillGroupNumList)
						.setParameter("refillStatus", RefillScheduleRefillStatus.Next)
						.setParameter("docType", DocType.Standard)
						.getResultList();
			}
			
			if ( sfiRefillGroupNumList.size() > 0 ) {
				sfiRefillList = em.createQuery(
						"select o from RefillSchedule o" + // 20121112 index check : RefillSchedule.pharmOrder : FK_REFILL_SCHEDULE_01
						" where o.pharmOrder = :pharmOrder" +
						" and o.groupNum in :sfiRefillGroupNumList" +
						" and o.refillStatus = :refillStatus" +
						" and o.docType = :docType" +
						" order by o.refillCouponNum")
						.setParameter("pharmOrder", dispOrder.getPharmOrder())
						.setParameter("sfiRefillGroupNumList", sfiRefillGroupNumList)
						.setParameter("refillStatus", RefillScheduleRefillStatus.Next)
						.setParameter("docType", DocType.Sfi)
						.getResultList();
			}
		}
		
		nextRefillScheduleMap.put(DocType.Sfi, sfiRefillList);
		nextRefillScheduleMap.put(DocType.Standard, standardRefillList);
		
		return nextRefillScheduleMap;
	}

	private boolean checkDrsDurationExceedForRx(Regimen regimen, Integer refillThreshold)
	{
		if(regimen == null)
		{
			return false;
		}
		
		if(RegimenType.StepUpDown == regimen.getType())
		{
			return false;
		}
		
		if(regimen.getDoseGroupList() == null || regimen.getDoseGroupList().isEmpty())
		{
			return false;
		}
		
		int durationInDay = regimen.getDoseGroupList().get(0).getDurationInDay();

		if( durationInDay <= refillThreshold)
		{
			return false;
		}
		
		return true;
		
	}
	
	private boolean checkDrsDurationExceedForIv(IvRxDrug ivRxDrug, Integer refillThreshold)
	{
		Integer duration = ivRxDrug.getDuration();
		RegimenDurationUnit durationUnit = ivRxDrug.getDurationUnit();
		
		if(duration == null || durationUnit == null)
		{
			return false;
		}
		
		DmRegimen dmRegimen = dmRegimenCacher.getRegimenByRegimenCode(durationUnit.getDataValue());
		
		if(dmRegimen == null)
		{
			return false;
		}
		
		Integer durationInDay = duration.intValue() * dmRegimen.getRegimenMultiplier();

		if( durationInDay <= refillThreshold)
		{
			return false;
		}
		
		return true;
	}
	
	private boolean checkDrsDurationExceedForCapd(CapdRxDrug capdRxDrug, Integer refillThreshold)
	{
		List<CapdItem> capdItemList = capdRxDrug.getCapdItemList();
		
		if(capdItemList == null || capdItemList.isEmpty())
		{
			return false;
		}
		
		for(CapdItem capdItem: capdItemList)
		{
			int durationInDay = capdItem.getDurationInDay();

			if(durationInDay > refillThreshold)
			{
				return true;
			}
		}
		
		return false;
	}
	
	private boolean isDrsDurationExceed(MedOrderItem medOrderItem, Integer refillThreshold )
	{
		if(refillThreshold == null)
		{
			return false;
		}

		if(medOrderItem.getRxDrug()!= null)
		{
			return checkDrsDurationExceedForRx(medOrderItem.getRxDrug().getRegimen(), refillThreshold);
		}
		else if(medOrderItem.getIvRxDrug()!= null)
		{
			return checkDrsDurationExceedForIv(medOrderItem.getIvRxDrug(), refillThreshold);
		}
		else if(medOrderItem.getCapdRxDrug()!=null)
		{
			return checkDrsDurationExceedForCapd(medOrderItem.getCapdRxDrug(), refillThreshold);
		}
		
		return false;
		
	}
	
	@SuppressWarnings("unchecked")
	public Pair<DrsConfig, List<String>> retrieveDrsConfig(PharmOrder pharmOrder, MedOrder medOrder, boolean refillOrderFlag){
		DrsPatient drsPatient = null;
		List<String> drsMessageList = new ArrayList<String>();
		DrsConfig vettingDrsConfig = null;
		
		List<DrsPatient> drsPatientList = em.createQuery(
										"select o from DrsPatient o" + // 20171023 index check : DrsPatient.hospital,patient : UI_DRS_PATIENT_01
										" where o.hospital = :hospital" +
										" and o.patient.hkid = :hkid")
										.setParameter("hospital", workstore.getHospital())
										.setParameter("hkid", pharmOrder.getHkid())
										.getResultList();
		
		if( drsPatientList.isEmpty() ){
			return new Pair<DrsConfig, List<String>>(vettingDrsConfig, drsMessageList);
		}else{			
			drsPatient = drsPatientList.get(0);
		}

		if( drsPatient.getEnableFlag() ){
			String pasSpecialtyType = PasSpecialtyType.OutPatient.getDataValue();
			
			if( MedOrderPrescType.REFILL_IP_PAS_SPEC.contains(medOrder.getPrescType()) ){
				pasSpecialtyType = PasSpecialtyType.InPatient.getDataValue();
			}
			
			List<DrsConfig> drsConfigList = em.createQuery(
											"select o from DrsConfig o" + // 20171023 index check : DrsConfig.workstore,type,pasSpecCode : UI_DRS_CONFIG_01
											" where o.workstore = :workstore" +
											" and o.pasSpecCode = :pasSpecCode" +
											" and o.type = :type")
											.setParameter("workstore", workstore)
											.setParameter("pasSpecCode", pharmOrder.getMedCase().getPasSpecCode())
											.setParameter("type", pasSpecialtyType)
											.getResultList();
			
			if( !drsConfigList.isEmpty() ){

				DrsConfig drsConfig = drsConfigList.get(0);
			
				if( refillOrderFlag ){
					return new Pair<DrsConfig, List<String>>(drsConfig, drsMessageList);
				}
				
				if( drsConfig.getMrReminderFlag() ){
					drsMessageList.add("1055");
				}

				Integer medOrderItemCount = Integer.valueOf(0);
				if( medOrder.getDocType() == MedOrderDocType.Normal && Boolean.TRUE.equals(drsPatient.getItemCountCheckFlag()) ){
					for(MedOrderItem moi: medOrder.getMedOrderItemList()){
						if( moi.getStatus() == MedOrderItemStatus.Completed || moi.getStatus() == MedOrderItemStatus.Outstanding ||  moi.getStatus() == MedOrderItemStatus.Fulfilling){
							medOrderItemCount = medOrderItemCount + 1;
						}
					}
					
					if( ObjectUtils.compare(medOrderItemCount, drsConfig.getItemCount()) < 0 ){
						return new Pair<DrsConfig, List<String>>(vettingDrsConfig, drsMessageList);
					}
				}
				
				if( medOrder.getDocType() == MedOrderDocType.Normal && Boolean.TRUE.equals(drsPatient.getDurationCheckFlag()) ){
					boolean drsDurationExceed = false;
					for(MedOrderItem moi: medOrder.getMedOrderItemList()){
						drsDurationExceed = isDrsDurationExceed(moi, drsConfig.getThreshold());
						if( drsDurationExceed ){
							break;
						}
					}
					
					if( !drsDurationExceed ){
						return new Pair<DrsConfig, List<String>>(vettingDrsConfig, drsMessageList);
					}
				}
				
				vettingDrsConfig = drsConfig;
				
				if( medOrder.getDocType() == MedOrderDocType.Normal && 
					Boolean.TRUE.equals(drsPatient.getItemCountCheckFlag()) && 
					Boolean.TRUE.equals(drsPatient.getDurationCheckFlag()) ){
					drsMessageList.add("1056");
				}else{
					drsMessageList.add("1057");
				}
			}
		}
		return new Pair<DrsConfig, List<String>>(vettingDrsConfig, drsMessageList);
	}
	
	@Remove
	public void destroy() {
		
	}
}
