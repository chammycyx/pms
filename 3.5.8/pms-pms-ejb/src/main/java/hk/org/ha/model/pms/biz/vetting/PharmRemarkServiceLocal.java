package hk.org.ha.model.pms.biz.vetting;

import hk.org.ha.model.pms.persistence.disp.MedOrderItem;

import javax.ejb.Local;

@Local
public interface PharmRemarkServiceLocal {

	void addPharmRemarkOrder();
	
	void checkPharmRemarkItem(String sourceAction, Integer medOrderSelectedIndex, MedOrderItem medOrderItem);
	
	void retrievePharmRemarkItem(String sourceAction, MedOrderItem medOrderItem);
	
	void validatePharmRemarkLogon(String user, String password);
	
	void retrievePharmRemarkDateItemNum(Boolean itemNumRequired);
	
	void destroy();
}
