package hk.org.ha.model.pms.biz.alert.ehr;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.alert.ehr.EhrAllergyException;
import hk.org.ha.model.pms.vo.alert.ehr.AdrDetail;
import hk.org.ha.model.pms.vo.alert.ehr.AllergenDetail;
import hk.org.ha.model.pms.vo.alert.ehr.EhrAllergyDetail;
import hk.org.ha.service.pms.asa.interfaces.alert.EhrAlertServiceJmsRemote;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("ehrAlertService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class EhrAlertServiceBean implements EhrAlertServiceLocal {

	@In
	private EhrAlertServiceJmsRemote ehrAlertServiceProxy;

	public EhrAllergyDetail retrieveEhrAllergyDetail(String patHospCode, String ehrNum) throws EhrAllergyException {
		EhrAllergyDetail ehrAllergyDetail = ehrAlertServiceProxy.retrieveEhrAllergyDetail(patHospCode, ehrNum);
		ehrAllergyDetail.setAllergenDetailList(groupAndSortAllergenDetail(ehrAllergyDetail.getAllergenDetailList()));
		ehrAllergyDetail.setAdrDetailList(groupAndSortAdrDetail(ehrAllergyDetail.getAdrDetailList()));
		return ehrAllergyDetail;
	}

	private List<AllergenDetail> groupAndSortAllergenDetail(List<AllergenDetail> allergenDetailList) {

		Map<String, List<AllergenDetail>> groupMap = new HashMap<String, List<AllergenDetail>>();
		List<AllergenDetail> returnList = new ArrayList<AllergenDetail>();
		
		for (AllergenDetail allergenDetail : allergenDetailList) {
			String key = allergenDetail.getVtmTermId();
			if (key == null) {
				returnList.add(allergenDetail);
				continue;
			}
			if (!groupMap.containsKey(key)) {
				groupMap.put(key, new ArrayList<AllergenDetail>());
			}
			groupMap.get(key).add(allergenDetail);
		}
		
		for (List<AllergenDetail> list : groupMap.values()) {
			Collections.sort(list, new AllergenDetailComparator());
			returnList.add(constructAllergenDetailContainer(list));
		}
		
		Collections.sort(returnList, new AllergenDetailComparator());
		return returnList;
	}

	private List<AdrDetail> groupAndSortAdrDetail(List<AdrDetail> adrDetailList) {

		Map<String, List<AdrDetail>> groupMap = new HashMap<String, List<AdrDetail>>();
		List<AdrDetail> returnList = new ArrayList<AdrDetail>();
		
		for (AdrDetail adrDetail : adrDetailList) {
			String key = adrDetail.getVtmTermId();
			if (key == null) {
				returnList.add(adrDetail);
				continue;
			}
			if (!groupMap.containsKey(key)) {
				groupMap.put(key, new ArrayList<AdrDetail>());
			}
			groupMap.get(key).add(adrDetail);
		}
		
		for (List<AdrDetail> list : groupMap.values()) {
			Collections.sort(list, new AdrDetailComparator());
			returnList.add(constructAdrDetailContainer(list));
		}
		
		Collections.sort(returnList, new AdrDetailComparator());
		return returnList;
	}

	private AllergenDetail constructAllergenDetailContainer(List<AllergenDetail> allergenDetailList) {
		AllergenDetail container = new AllergenDetail();
		container.setChildList(allergenDetailList);
		container.setAllergenDesc(allergenDetailList.get(0).getVtmEhrDesc());
		container.setInstCode(allergenDetailList.get(0).getInstCode());
		Date latestLastUpdate = null;
		for (AllergenDetail allergenDetail : allergenDetailList) {
			if (latestLastUpdate == null || allergenDetail.getLastUpdate().compareTo(latestLastUpdate) > 0) {
				latestLastUpdate = allergenDetail.getLastUpdate();
			}
		}
		container.setLastUpdate(latestLastUpdate);
		return container;
	}
	
	private AdrDetail constructAdrDetailContainer(List<AdrDetail> adrDetailList) {
		AdrDetail container = new AdrDetail();
		container.setChildList(adrDetailList);
		container.setAdrDesc(adrDetailList.get(0).getVtmEhrDesc());
		container.setInstCode(adrDetailList.get(0).getInstCode());
		Date latestLastUpdate = null;
		for (AdrDetail adrDetail : adrDetailList) {
			if (latestLastUpdate == null || adrDetail.getLastUpdate().compareTo(latestLastUpdate) > 0) {
				latestLastUpdate = adrDetail.getLastUpdate();
			}
		}
		container.setLastUpdate(latestLastUpdate);
		return container;
	}
	
	@Remove
	public void destroy() {
	}
	
	private static class AllergenDetailComparator implements Comparator<AllergenDetail>, Serializable {

		private static final long serialVersionUID = 1L;
		
		public int compare(AllergenDetail o1, AllergenDetail o2) {
			if (o1.getLastUpdate().compareTo(o2.getLastUpdate()) != 0) {
				return o2.getLastUpdate().compareTo(o1.getLastUpdate());
			}
			if (!o1.getAllergenDesc().equals(o2.getAllergenDesc())) {
				return o1.getAllergenDesc().compareTo(o2.getAllergenDesc());
			}
			if (!o1.getInstCode().equals(o2.getInstCode())) {
				return o1.getInstCode().compareTo(o2.getInstCode());
			}
			return 0;
		}
	}
	
	private static class AdrDetailComparator implements Comparator<AdrDetail>, Serializable {

		private static final long serialVersionUID = 1L;
		
		public int compare(AdrDetail o1, AdrDetail o2) {
			if (o1.getLastUpdate().compareTo(o2.getLastUpdate()) != 0) {
				return o2.getLastUpdate().compareTo(o1.getLastUpdate());
			}
			if (!o1.getAdrDesc().equals(o2.getAdrDesc())) {
				return o1.getAdrDesc().compareTo(o2.getAdrDesc());
			}
			if (!o1.getInstCode().equals(o2.getInstCode())) {
				return o1.getInstCode().compareTo(o2.getInstCode());
			}
			return 0;
		}
	}
}
