package hk.org.ha.model.pms.udt.patient;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PasSpecialtyType implements StringValuedEnum {

	Blank(" ","(Blank)"),
	OutPatient("O", "O"),
	InPatient("I", "I");	
	
    private final String dataValue;
    private final String displayValue;
        
    PasSpecialtyType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static PasSpecialtyType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PasSpecialtyType.class, dataValue);
	}
        
	public static class Converter extends StringValuedEnumConverter<PasSpecialtyType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<PasSpecialtyType> getEnumClass() {
    		return PasSpecialtyType.class;
    	}
    }
}
