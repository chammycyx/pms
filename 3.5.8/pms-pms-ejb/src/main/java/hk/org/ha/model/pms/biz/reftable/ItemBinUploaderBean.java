package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.security.Uam;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.TimerProp;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.ItemLocation;
import hk.org.ha.model.pms.udt.reftable.MachineType;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpMaintServiceJmsRemote;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("itemBinUploader")
@MeasureCalls
public class ItemBinUploaderBean implements ItemBinUploaderLocal {

	private static final String UPLOAD_ITEM_BIN = "uploadItemBin";
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;

	@In
	private Uam uam;
	
	@In
	private HospitalClusterTaskManagerLocal hospitalClusterTaskManager;	
	
	@In
	private CorpMaintServiceJmsRemote corpMaintServiceProxy;
	
	@In
	private TimerProp timerProp;
	
	@SuppressWarnings("unchecked")
	private void uploadItemBin(){
		List<Hospital> hospitalList = em.createQuery(
				"select o from Hospital o" + // 20120823 index check : Hospital.hospitalcluster : PK_HOSPITAL_CLUSTER
				" where o.hospitalCluster.clusterCode = :clusterCode")
				.setHint( QueryHints.FETCH, "o.workstoreList" )
				.setParameter("clusterCode", uam.getClusterCode())
				.getResultList();
		
		for( Hospital hospital : hospitalList ){	// hospitals under same cluster
			for( Workstore workstore : hospital.getWorkstoreList() ){	// workstores under same hospital
				List<ItemLocation> itemLocationList = em.createQuery(
						"select o from ItemLocation o" + // 20121112 index check : ItemLocation.workstore : FK_ITEM_LOCATION_01
						" where o.machine.type = :type" +
						" and o.workstore = :workstore")
						.setParameter("type", MachineType.Manual)
						.setParameter("workstore", workstore)
						.getResultList();
				
				corpMaintServiceProxy.receiveItemBinList(itemLocationList, workstore);	// upload to CORP by workstore 
			}
		}
	}
	
	public void uploadItemBinByTimer(Long duration, Long intervalDuration) {

		try {
			if (timerProp.isActivate()) 
			{		
				if (hospitalClusterTaskManager.lockHospitalClusterTask(
						UPLOAD_ITEM_BIN, 
						new Date(), 
						intervalDuration.longValue())) {			
					uploadItemBin();
				} else {
					logger.info("uploadItemBinByTimer has been run previously within #0ms, bypass.", intervalDuration);
				}
			} else {
				logger.warn("uploadItemBinTimer temporary disabled by " + TimerProp.TIMER_PROPERTIES);
			}
		} catch (Exception e) {
			logger.error(e,e);
		}
	}
}

