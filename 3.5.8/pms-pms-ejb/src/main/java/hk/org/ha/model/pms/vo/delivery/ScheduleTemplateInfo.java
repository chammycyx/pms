package hk.org.ha.model.pms.vo.delivery;

import hk.org.ha.model.pms.persistence.medprofile.DeliverySchedule;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryScheduleItem;
import java.io.Serializable;
import java.util.List;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class ScheduleTemplateInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private DeliverySchedule defaultDeliverySchedule;
	
	private DeliverySchedule currentDeliverySchedule;
	
	private List<DeliveryScheduleItem> deliveryScheduleItemList;
	
	private List<DeliverySchedule> deliveryScheduleList;
	
	private Boolean emptyDefaultTemplateNameFlag;

	public ScheduleTemplateInfo() {
		super();
		emptyDefaultTemplateNameFlag = Boolean.FALSE;
	}

	public DeliverySchedule getDefaultDeliverySchedule() {
		return defaultDeliverySchedule;
	}

	public void setDefaultDeliverySchedule(DeliverySchedule defaultDeliverySchedule) {
		this.defaultDeliverySchedule = defaultDeliverySchedule;
	}

	public DeliverySchedule getCurrentDeliverySchedule() {
		return currentDeliverySchedule;
	}

	public void setCurrentDeliverySchedule(DeliverySchedule currentDeliverySchedule) {
		this.currentDeliverySchedule = currentDeliverySchedule;
	}

	public List<DeliveryScheduleItem> getDeliveryScheduleItemList() {
		return deliveryScheduleItemList;
	}

	public void setDeliveryScheduleItemList(
			List<DeliveryScheduleItem> deliveryScheduleItemList) {
		this.deliveryScheduleItemList = deliveryScheduleItemList;
	}

	public List<DeliverySchedule> getDeliveryScheduleList() {
		return deliveryScheduleList;
	}

	public void setDeliveryScheduleList(List<DeliverySchedule> deliveryScheduleList) {
		this.deliveryScheduleList = deliveryScheduleList;
	}

	public Boolean getEmptyDefaultTemplateNameFlag() {
		return emptyDefaultTemplateNameFlag;
	}

	public void setEmptyDefaultTemplateNameFlag(
			Boolean emptyDefaultTemplateNameFlag) {
		this.emptyDefaultTemplateNameFlag = emptyDefaultTemplateNameFlag;
	}

}
