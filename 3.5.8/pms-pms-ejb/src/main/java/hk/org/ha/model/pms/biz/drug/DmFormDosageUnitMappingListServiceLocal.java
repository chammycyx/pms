package hk.org.ha.model.pms.biz.drug;
import java.util.List;

import javax.ejb.Local;

@Local
public interface DmFormDosageUnitMappingListServiceLocal {
	List<String> retrieveDosageUnitListByFormCode( String formCode );	
	void destroy();
}
