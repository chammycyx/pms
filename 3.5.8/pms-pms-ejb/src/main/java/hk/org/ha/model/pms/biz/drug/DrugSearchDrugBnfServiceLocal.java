package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.model.pms.dms.vo.BnfSearchCriteria;
import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
import hk.org.ha.model.pms.vo.drug.DrugSearchReturn;

import javax.ejb.Local;

@Local
public interface DrugSearchDrugBnfServiceLocal {
	
	DrugSearchReturn retrieveDrugSearchBnf(DrugSearchSource drugSearchSource, BnfSearchCriteria bnfSearchCriteria);

	void destroy();
}
