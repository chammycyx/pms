package hk.org.ha.model.pms.persistence.pivas;

import hk.org.ha.model.pms.dms.persistence.DmWarning;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrugManuf;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaMethod;
import hk.org.ha.model.pms.persistence.VersionEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.eclipse.persistence.annotations.PrivateOwned;

@Entity
@Table(name = "PIVAS_PREP_METHOD")
public class PivasPrepMethod extends VersionEntity implements Comparable<PivasPrepMethod> {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pivasPrepMethodSeq")
	@SequenceGenerator(name = "pivasPrepMethodSeq", sequenceName = "SQ_PIVAS_PREP_METHOD", initialValue = 100000000)
	private Long id;

	@Column(name = "PIVAS_PREP_ID", nullable = false)
	private Long pivasPrepId;
	
	@Column(name = "PIVAS_FORMULA_METHOD_ID", nullable = false)
	private Long pivasFormulaMethodId;

	@Column(name = "DILUENT_CODE", length = 25)
	private String diluentCode;
	
	@Column(name = "DILUENT_DESC", length = 100)
	private String diluentDesc;
	
	@Column(name = "DILUENT_ITEM_CODE", length = 20)
	private String diluentItemCode;

	@Column(name = "DILUENT_MANUF_CODE", length = 20)
	private String diluentManufCode;

	@Column(name = "DILUENT_BATCH_NUM", length = 20)
	private String diluentBatchNum;
	
	@Column(name = "DILUENT_EXPIRY_DATE")
	@Temporal(TemporalType.DATE)
	private Date diluentExpiryDate;	
	
	@Column(name = "WARN_CODE1", length = 4)
	private String warnCode1;
	
	@Column(name = "WARN_CODE2", length = 4)
	private String warnCode2; 

	@PrivateOwned
    @OrderBy("drugItemCode,drugManufCode,solventItemCode,solventManufCode")
    @OneToMany(mappedBy = "pivasPrepMethod", cascade = CascadeType.ALL)
    private List<PivasPrepMethodItem> pivasPrepMethodItemList;

	@Transient
	private PivasFormulaMethod pivasFormulaMethod;

	@Transient
	private DmWarning dmWarning1;

	@Transient
	private DmWarning dmWarning2;
	
	@Transient
	private PivasDrugManuf diluentPivasDrugManuf;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPivasPrepId() {
		return pivasPrepId;
	}

	public void setPivasPrepId(Long pivasPrepId) {
		this.pivasPrepId = pivasPrepId;
	}

	public Long getPivasFormulaMethodId() {
		return pivasFormulaMethodId;
	}

	public void setPivasFormulaMethodId(Long pivasFormulaMethodId) {
		this.pivasFormulaMethodId = pivasFormulaMethodId;
	}

	public String getDiluentCode() {
		return diluentCode;
	}

	public void setDiluentCode(String diluentCode) {
		this.diluentCode = diluentCode;
	}

	public String getDiluentDesc() {
		return diluentDesc;
	}

	public void setDiluentDesc(String diluentDesc) {
		this.diluentDesc = diluentDesc;
	}

	public String getDiluentItemCode() {
		return diluentItemCode;
	}

	public void setDiluentItemCode(String diluentItemCode) {
		this.diluentItemCode = diluentItemCode;
	}

	public String getDiluentManufCode() {
		return diluentManufCode;
	}

	public void setDiluentManufCode(String diluentManufCode) {
		this.diluentManufCode = diluentManufCode;
	}

	public String getDiluentBatchNum() {
		return diluentBatchNum;
	}

	public void setDiluentBatchNum(String diluentBatchNum) {
		this.diluentBatchNum = diluentBatchNum;
	}

	public Date getDiluentExpiryDate() {
		return diluentExpiryDate == null ? null : new Date(diluentExpiryDate.getTime());
	}

	public void setDiluentExpiryDate(Date diluentExpiryDate) {
		this.diluentExpiryDate = diluentExpiryDate == null ? null : new Date(diluentExpiryDate.getTime());
	}

	public String getWarnCode1() {
		return warnCode1;
	}

	public void setWarnCode1(String warnCode1) {
		this.warnCode1 = warnCode1;
	}

	public String getWarnCode2() {
		return warnCode2;
	}

	public void setWarnCode2(String warnCode2) {
		this.warnCode2 = warnCode2;
	}

	public List<PivasPrepMethodItem> getPivasPrepMethodItemList() {
		if (pivasPrepMethodItemList == null) {
			pivasPrepMethodItemList = new ArrayList<PivasPrepMethodItem>();
		}
		return pivasPrepMethodItemList;
	}

	public void setPivasPrepMethodItemList(List<PivasPrepMethodItem> pivasPrepMethodItemList) {
		this.pivasPrepMethodItemList = pivasPrepMethodItemList;
	}

	public PivasFormulaMethod getPivasFormulaMethod() {
		return pivasFormulaMethod;
	}

	public void setPivasFormulaMethod(PivasFormulaMethod pivasFormulaMethod) {
		this.pivasFormulaMethod = pivasFormulaMethod;
	}

	public DmWarning getDmWarning1() {
		return dmWarning1;
	}

	public void setDmWarning1(DmWarning dmWarning1) {
		this.dmWarning1 = dmWarning1;
	}

	public DmWarning getDmWarning2() {
		return dmWarning2;
	}

	public void setDmWarning2(DmWarning dmWarning2) {
		this.dmWarning2 = dmWarning2;
	}
	
	public PivasDrugManuf getDiluentPivasDrugManuf() {
		return diluentPivasDrugManuf;
	}

	public void setDiluentPivasDrugManuf(PivasDrugManuf diluentPivasDrugManuf) {
		this.diluentPivasDrugManuf = diluentPivasDrugManuf;
	}

	public void addPivasPrepMethodItem(PivasPrepMethodItem pivasPrepMethodItem) {
		pivasPrepMethodItem.setPivasPrepMethod(this);
		getPivasPrepMethodItemList().add(pivasPrepMethodItem);
	}
	
	@Override
	public int compareTo(PivasPrepMethod pivasPrepMethod) {
		int pivasPrepMethodResult = new CompareToBuilder()
				.append( pivasFormulaMethodId, pivasPrepMethod.getPivasFormulaMethodId() )
				.append( diluentItemCode, pivasPrepMethod.getDiluentItemCode() )
				.append( diluentManufCode, pivasPrepMethod.getDiluentManufCode() )
				.append( diluentBatchNum, pivasPrepMethod.getDiluentBatchNum() )
				.append( diluentExpiryDate, pivasPrepMethod.getDiluentExpiryDate() )
				.append( warnCode1, pivasPrepMethod.getWarnCode1() )
				.append( warnCode2, pivasPrepMethod.getWarnCode2() )
				.append( pivasPrepMethodItemList.size(), pivasPrepMethod.getPivasPrepMethodItemList().size() )
				.toComparison();
		
		if (pivasPrepMethodResult != 0) {
			return pivasPrepMethodResult;
		}
		
		Iterator<PivasPrepMethodItem> itemItr1 = pivasPrepMethodItemList.iterator();
		Iterator<PivasPrepMethodItem> itemItr2 = pivasPrepMethod.getPivasPrepMethodItemList().iterator();
				
		while (itemItr1.hasNext() && itemItr2.hasNext()) {
			PivasPrepMethodItem item1 = itemItr1.next();
			PivasPrepMethodItem item2 = itemItr2.next();
			
			int pivasPrepMethodItemResult = new CompareToBuilder()
					.append( item1.getDrugItemCode(), item2.getDrugItemCode() )
					.append( item1.getDrugManufCode(), item2.getDrugManufCode() )
					.append( item1.getDrugBatchNum(), item2.getDrugBatchNum() )
					.append( item1.getDrugExpiryDate(), item2.getDrugExpiryDate() )
					.append( item1.getSolventItemCode(), item2.getSolventItemCode() )
					.append( item1.getSolventManufCode(), item2.getSolventManufCode() )
					.append( item1.getSolventBatchNum(), item2.getSolventBatchNum() )
					.append( item1.getSolventExpiryDate(), item2.getSolventExpiryDate() )
					.append( item1.getDrugCalQty(), item2.getDrugCalQty() )
					.append( item1.getDrugDispQty(), item2.getDrugDispQty() )
					.toComparison();
			
			if (pivasPrepMethodItemResult != 0) {
				return pivasPrepMethodItemResult;
			}
		}
		
		return 0;
	}
}
