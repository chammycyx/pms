package hk.org.ha.model.pms.vo.alert.mds;

import hk.org.ha.model.pms.persistence.PatientEntity;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.udt.disp.OnHandSourceType;
import hk.org.ha.model.pms.udt.vetting.CmsOrderSubType;
import hk.org.ha.model.pms.udt.vetting.CmsOrderType;
import hk.org.ha.model.pms.udt.vetting.OnHandProfileOrderType;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class MdsCriteria implements Serializable {

	private static final long serialVersionUID = 1L;

	private String orderNum;
	
	private PatientEntity patientEntity;
	
	private Date orderDate;
	
	private CmsOrderType orderType;
	
	private CmsOrderSubType orderSubType;
	
	private String caseNum;

	private String patHospCode;
	
	private String specCode;

	private String hospCode;
	
	private String workstoreCode;
	
	private String userId;
	
	private String workstationId;

	private boolean checkAlertProfile;
	
	private boolean checkDdiFlag;
	
	private boolean checkOnHandDrugFlag;
	
	private boolean checkPregnancyFlag;
	
	private boolean checkHlaFlag;
	
	private boolean showLogFlag;
	
	private boolean checkSteroidFlag;
	
	private boolean filterContinuousInfusionDdi;
	
	private int ddiMaxSeverityLevel;
	
	private String g6pdCode;
	
	private String g6pdConditionId;
	
	private int g6pdMaxSeverityLevel;
	
	private String pregnancyConditionId;
	
	private int pregnancyMaxSeverityLevel;
	
	private List<MedOrderItem> medOrderItemList;

	private boolean pasPatientEnabled;
	
	private boolean useDrugMdsPropertyHq;
	
	private boolean isDhOrder;
	
	private OnHandSourceType onHandSourceType;
	
	private Boolean corpOnHandEnabled;
	
	private OnHandProfileOrderType onHandProfileOrderType;
	
	public String getOrderNum() {
		return orderNum;
	}
	
	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public PatientEntity getPatientEntity() {
		return patientEntity;
	}

	public void setPatientEntity(PatientEntity patientEntity) {
		this.patientEntity = patientEntity;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public CmsOrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(CmsOrderType orderType) {
		this.orderType = orderType;
	}

	public CmsOrderSubType getOrderSubType() {
		return orderSubType;
	}

	public void setOrderSubType(CmsOrderSubType orderSubType) {
		this.orderSubType = orderSubType;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWorkstationId() {
		return workstationId;
	}

	public void setWorkstationId(String workstationId) {
		this.workstationId = workstationId;
	}

	public boolean getCheckAlertProfile() {
		return checkAlertProfile;
	}

	public void setCheckAlertProfile(boolean checkAlertProfile) {
		this.checkAlertProfile = checkAlertProfile;
	}

	public boolean getCheckDdiFlag() {
		return checkDdiFlag;
	}

	public void setCheckDdiFlag(boolean checkDdiFlag) {
		this.checkDdiFlag = checkDdiFlag;
	}

	public boolean getCheckOnHandDrugFlag() {
		return checkOnHandDrugFlag;
	}

	public void setCheckOnHandDrugFlag(boolean checkOnHandDrugFlag) {
		this.checkOnHandDrugFlag = checkOnHandDrugFlag;
	}

	public boolean getCheckPregnancyFlag() {
		return checkPregnancyFlag;
	}

	public void setCheckPregnancyFlag(boolean checkPregnancyFlag) {
		this.checkPregnancyFlag = checkPregnancyFlag;
	}
	
	public boolean getCheckHlaFlag() {
		return checkHlaFlag;
	}

	public void setCheckHlaFlag(boolean checkHlaFlag) {
		this.checkHlaFlag = checkHlaFlag;
	}

	public boolean getShowLogFlag() {
		return showLogFlag;
	}
	
	public void setShowLogFlag(boolean showLogFlag) {
		this.showLogFlag = showLogFlag; 
	}

	public boolean getCheckSteroidFlag() {
		return checkSteroidFlag;
	}

	public void setCheckSteroidFlag(boolean checkSteroidFlag) {
		this.checkSteroidFlag = checkSteroidFlag;
	}

	public boolean getFilterContinuousInfusionDdi() {
		return filterContinuousInfusionDdi;
	}

	public void setFilterContinuousInfusionDdi(boolean filterContinuousInfusionDdi) {
		this.filterContinuousInfusionDdi = filterContinuousInfusionDdi;
	}

	public int getDdiMaxSeverityLevel() {
		return ddiMaxSeverityLevel;
	}

	public void setDdiMaxSeverityLevel(int ddiMaxSeverityLevel) {
		this.ddiMaxSeverityLevel = ddiMaxSeverityLevel;
	}

	public String getG6pdCode() {
		return g6pdCode;
	}

	public void setG6pdCode(String g6pdCode) {
		this.g6pdCode = g6pdCode;
	}

	public String getG6pdConditionId() {
		return g6pdConditionId;
	}

	public void setG6pdConditionId(String g6pdConditionId) {
		this.g6pdConditionId = g6pdConditionId;
	}

	public int getG6pdMaxSeverityLevel() {
		return g6pdMaxSeverityLevel;
	}

	public void setG6pdMaxSeverityLevel(int g6pdMaxSeverityLevel) {
		this.g6pdMaxSeverityLevel = g6pdMaxSeverityLevel;
	}

	public String getPregnancyConditionId() {
		return pregnancyConditionId;
	}

	public void setPregnancyConditionId(String pregnancyConditionId) {
		this.pregnancyConditionId = pregnancyConditionId;
	}

	public int getPregnancyMaxSeverityLevel() {
		return pregnancyMaxSeverityLevel;
	}

	public void setPregnancyMaxSeverityLevel(int pregnancyMaxSeverityLevel) {
		this.pregnancyMaxSeverityLevel = pregnancyMaxSeverityLevel;
	}

	public List<MedOrderItem> getMedOrderItemList() {
		return medOrderItemList;
	}

	public void setMedOrderItemList(List<MedOrderItem> medOrderItemList) {
		this.medOrderItemList = medOrderItemList;
	}

	public boolean getPasPatientEnabled() {
		return pasPatientEnabled;
	}

	public void setPasPatientEnabled(boolean pasPatientEnabled) {
		this.pasPatientEnabled = pasPatientEnabled;
	}

	public boolean getUseDrugMdsPropertyHq() {
		return useDrugMdsPropertyHq;
	}

	public void setUseDrugMdsPropertyHq(boolean useDrugMdsPropertyHq) {
		this.useDrugMdsPropertyHq = useDrugMdsPropertyHq;
	}

	public boolean getIsDhOrder() {
		return isDhOrder;
	}

	public void setIsDhOrder(boolean isDhOrder) {
		this.isDhOrder = isDhOrder;
	}

	public OnHandSourceType getOnHandSourceType() {
		return onHandSourceType;
	}

	public void setOnHandSourceType(OnHandSourceType onHandSourceType) {
		this.onHandSourceType = onHandSourceType;
	}

	public Boolean getCorpOnHandEnabled() {
		return corpOnHandEnabled;
	}

	public void setCorpOnHandEnabled(Boolean corpOnHandEnabled) {
		this.corpOnHandEnabled = corpOnHandEnabled;
	}

	public OnHandProfileOrderType getOnHandProfileOrderType() {
		return onHandProfileOrderType;
	}

	public void setOnHandProfileOrderType(
			OnHandProfileOrderType onHandProfileOrderType) {
		this.onHandProfileOrderType = onHandProfileOrderType;
	}
}
