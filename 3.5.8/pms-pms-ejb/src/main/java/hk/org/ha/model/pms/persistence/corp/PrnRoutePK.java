package hk.org.ha.model.pms.persistence.corp;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class PrnRoutePK implements Serializable {

	private static final long serialVersionUID = 1L;

	private String hospCode;
	private String workstoreCode;
	private String routeCode;
	
	public PrnRoutePK() {
	}
	
	public PrnRoutePK(String hospCode, String workstoreCode, String routeCode) {
		super();
		this.hospCode = hospCode;
		this.workstoreCode = workstoreCode;
		this.routeCode = routeCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public void setRouteCode(String routeCode) {
		this.routeCode = routeCode;
	}

	public String getHospCode() {
		return hospCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public String getRouteCode() {
		return routeCode;
	}

	public boolean equals(Object other) {
		return EqualsBuilder.reflectionEquals(this, other);
	}
	
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
}
