package hk.org.ha.model.pms.vo.pivas.worklist;

import hk.org.ha.model.pms.persistence.pivas.PivasWorklist;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PivasItemEditInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	private Boolean pivasFormulaFlag;
	private PivasWorklist pivasWorklist;
	private Boolean processFlag;
	private Boolean pivasWardFlag;
	
	public PivasItemEditInfo() {
		pivasFormulaFlag = Boolean.FALSE;
		pivasWorklist = null;
		processFlag = Boolean.FALSE;
		setPivasWardFlag(Boolean.FALSE);
	}
	
	public Boolean getPivasFormulaFlag() {
		return pivasFormulaFlag;
	}
	public void setPivasFormulaFlag(Boolean pivasFormulaFlag) {
		this.pivasFormulaFlag = pivasFormulaFlag;
	}
	public PivasWorklist getPivasWorklist() {
		return pivasWorklist;
	}
	public void setPivasWorklist(PivasWorklist pivasWorklist) {
		this.pivasWorklist = pivasWorklist;
	}
	public Boolean getProcessFlag() {
		return processFlag;
	}
	public void setProcessFlag(Boolean processFlag) {
		this.processFlag = processFlag;
	}
	public Boolean getPivasWardFlag() {
		return pivasWardFlag;
	}
	public void setPivasWardFlag(Boolean pivasWardFlag) {
		this.pivasWardFlag = pivasWardFlag;
	}
}
