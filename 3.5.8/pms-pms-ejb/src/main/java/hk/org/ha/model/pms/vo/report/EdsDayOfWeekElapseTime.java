package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class EdsDayOfWeekElapseTime implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String dayOfWeekDesc;
	
	private String timeRange;
	
	private Integer avgTime;

	public EdsDayOfWeekElapseTime(String dayOfWeekDesc, String timeRange, Integer avgTime) {
		super();
		this.dayOfWeekDesc = dayOfWeekDesc;
		this.timeRange = timeRange;
		this.avgTime = avgTime;
	}
	
	public void setDayOfWeekDesc(String dayOfWeekDesc) {
		this.dayOfWeekDesc = dayOfWeekDesc;
	}

	public String getDayOfWeekDesc() {
		return dayOfWeekDesc;
	}

	public void setTimeRange(String timeRange) {
		this.timeRange = timeRange;
	}

	public String getTimeRange() {
		return timeRange;
	}

	public Integer getAvgTime() {
		return avgTime;
	}

	public void setAvgTime(Integer avgTime) {
		this.avgTime = avgTime;
	}
}
