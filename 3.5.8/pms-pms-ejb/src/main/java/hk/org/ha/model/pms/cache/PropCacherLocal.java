package hk.org.ha.model.pms.cache;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.persistence.PropEntity;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.prop.Scope;

import javax.ejb.Local;

@Local
public interface PropCacherLocal extends BaseCacherInf {
	
    PropEntity getPropEntity(Scope type, String name);

    PropEntity getPropEntity(Workstore workstore, String name);
    
    PropEntity getPropEntity(Workstation workstation, String name);
    
    PropEntity getPropEntity(Hospital hospital, String name);

    PropEntity getPropEntity(String name);

    void clearWorkstoreCache(Workstore workstore);
    
    void clearWorkstationCache(Workstation workstation);

}
