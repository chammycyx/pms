package hk.org.ha.model.pms.biz.reftable;

import java.util.List;

import hk.org.ha.model.pms.asa.exception.pas.PasException;
import hk.org.ha.model.pms.vo.patient.PasSpecialty;

import javax.ejb.Local;

@Local
public interface PmsRestrictItemSpecBySpecListServiceLocal {

	void retrievePmsRestrictItemListBySpecialty(String specType, String specCode, String subSpecCode);
	
	List<PasSpecialty> retrievePasSpecDisplayList() throws PasException;

	void destroy();
	
}
 