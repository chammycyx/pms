package hk.org.ha.model.pms.udt.refill;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum RefillScheduleActionType implements StringValuedEnum {

	NormalRefill("NR", "Normal Refill"),
	LateRefill("LR", "Late Refill"),
	TerminateRefill("TR", "Terminate Refill"),
	AbortRefill("AR","Abort Refill"),
	NoRefillAction("NA","No Refill Action"),
	ReadOnlyRefill("RR","ReadOnly Refill Action");
	
    private final String dataValue;
    private final String displayValue;

    RefillScheduleActionType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }  
    
    public static RefillScheduleActionType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(RefillScheduleActionType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<RefillScheduleActionType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<RefillScheduleActionType> getEnumClass() {
    		return RefillScheduleActionType.class;
    	}
    }
}
