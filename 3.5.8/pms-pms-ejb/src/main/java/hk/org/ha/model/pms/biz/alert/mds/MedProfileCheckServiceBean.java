package hk.org.ha.model.pms.biz.alert.mds;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.vo.alert.mds.MedProfileCheckResult;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("medProfileCheckService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MedProfileCheckServiceBean implements MedProfileCheckServiceLocal {

	@In
	private List<MedProfileItem> medProfileItemList;

	@In
	private MedProfileCheckManagerLocal medProfileCheckManager;
	
	public MedProfileCheckResult checkMedProfileAlertForSaveProfile(MedProfile medProfile) {
		
		return medProfileCheckManager.checkMedProfileAlertForSaveProfile(medProfile, medProfileItemList);
		
	}
	
	@Remove
	public void destory() {
	}
}
