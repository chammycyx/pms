package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.dms.persistence.PmsDispenseConfig;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
import hk.org.ha.model.pms.persistence.reftable.ItemDispConfig;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("itemDispConfigManager")
public class ItemDispConfigManagerBean implements ItemDispConfigManagerLocal {
	
	@PersistenceContext
	private EntityManager em;

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

	
	@Override
	public ItemDispConfig retrieveItemDispConfig(String itemCode, WorkstoreGroup workstoreGroup) 
	{
		return retrieveItemDispConfig(itemCode, workstoreGroup, false);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public ItemDispConfig retrieveItemDispConfig(String itemCode, WorkstoreGroup workstoreGroup, boolean retrieveDispDays)
	{
		List<ItemDispConfig> itemDispConfigList = em.createQuery(
				"select o from ItemDispConfig o" + // 20120303 index check : ItemDispConfig.workstoreGroup,itemCode : UI_ITEM_DISP_CONFIG_01
				" where o.workstoreGroup = :workstoreGroup" + 
				" and o.itemCode = :itemCode" +
				" order by o.createDate, o.id")
				.setParameter("workstoreGroup", workstoreGroup)
				.setParameter("itemCode", itemCode)
				.getResultList();
		
		if (itemDispConfigList.isEmpty() && !retrieveDispDays) {
			return null;
		}
		
		PmsDispenseConfig pmsDispenseConfig = null;
		if (retrieveDispDays) {
			pmsDispenseConfig = dmsPmsServiceProxy.retrievePmsDispenseConfig(
					itemCode, workstoreGroup.getWorkstoreGroupCode());
		}
		
		if (itemDispConfigList.isEmpty() && pmsDispenseConfig == null){
			return null;
		}
		
		ItemDispConfig itemDispConfig = new ItemDispConfig();
		if ( !itemDispConfigList.isEmpty() ){
			itemDispConfig = itemDispConfigList.get(0);
		}
		
		if (pmsDispenseConfig != null && pmsDispenseConfig.getDefaultDispenseDay() != null) {
			itemDispConfig.setDefaultDispDays(pmsDispenseConfig.getDefaultDispenseDay());
			itemDispConfig.setWardStockWithout2DCodeFlag(pmsDispenseConfig.getWardStockWithout2DCodeFlag());
			itemDispConfig.setDdWithout2DCodeFlag(pmsDispenseConfig.getDdWithout2DCodeFlag());
		}
		
		if (retrieveDispDays && itemDispConfig.getDailyRefillFlag()) {
			itemDispConfig.setDefaultDispDays( 1 );
		}
		
		return itemDispConfig;
	}

	@Override
	public List<ItemDispConfig> retrieveItemDispConfigList(WorkstoreGroup workstoreGroup)
	{
		return retrieveItemDispConfigList(workstoreGroup, true);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<ItemDispConfig> retrieveItemDispConfigList(WorkstoreGroup workstoreGroup, boolean retrieveDispDays)
	{
		Map<String, PmsDispenseConfig> pmsDispenseConfigMap = new HashMap<String, PmsDispenseConfig>();
		if (retrieveDispDays) {
			List<PmsDispenseConfig> pmsDispenseConfigList = dmsPmsServiceProxy.retrievePmsDispenseConfigList( workstoreGroup.getWorkstoreGroupCode() );
			for(PmsDispenseConfig pmsDispenseConfig : pmsDispenseConfigList){
				pmsDispenseConfigMap.put(pmsDispenseConfig.getItemCode(), pmsDispenseConfig);
			}
		}
		
		List<ItemDispConfig> itemDispConfigList = em.createQuery(
				"select o from ItemDispConfig o" + // 20120328 index check : ItemDispConfig.workstoreGroup : FK_ITEM_DISP_CONFIG_01
				" where o.workstoreGroup = :workstoreGroup" + 
				" order by o.id")
				.setParameter("workstoreGroup", workstoreGroup)
				.getResultList(); 
		
		if (retrieveDispDays) {
			for (ItemDispConfig itemDispConfig : itemDispConfigList) {
				PmsDispenseConfig pmsDispenseConfig = pmsDispenseConfigMap.get( itemDispConfig.getItemCode() );
				
				if (pmsDispenseConfig != null && pmsDispenseConfig.getDefaultDispenseDay() != null) {
					itemDispConfig.setDefaultDispDays(pmsDispenseConfig.getDefaultDispenseDay());
				}
			}
		}
		
		return itemDispConfigList;
	}
	
}
