package hk.org.ha.model.pms.biz.checkissue.mp;

import java.io.Serializable;
import java.util.Map;

public interface PharmSendOutClientNotifierInf {

    void notifyClient(Serializable object, Map<String, Object> headerParams);
	
}
