package hk.org.ha.model.pms.biz.order;

import java.util.List;

import javax.ejb.Local;

import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.medprofile.Delivery;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.persistence.medprofile.PurchaseRequest;
import hk.org.ha.model.pms.persistence.medprofile.TpnRequest;
import hk.org.ha.model.pms.persistence.reftable.Workstation;

@Local
public interface DispOrderConverterLocal {
	List<DispOrder> convertDeliveryToDispOrderList(Delivery delivery, boolean ipSfiInvoice, Workstore workstore);
	
	DispOrder convertMedProfileItemToDispOrder(MedProfileItem medProfileItem, MedProfileMoItem medProfileMoItem, Workstore workstore);
	
	Invoice convertMedProfileItemToInvoice(PurchaseRequest purchaseRequest, boolean isReprint, Workstore workstore);
	
	List<Invoice> convertPurchaseRequestToVoidInvoiceList(List<PurchaseRequest> voidPurchaseRequestList, Workstore workstore);
	
	PharmOrderItem convertToPharmOrderItemForInstructionList(MedProfilePoItem medProfilePoItem, Workstore workstore);
	
	DispOrder convertTpnRequestListToDispOrder(List<TpnRequest> tpnRequestList, Workstore workstore, Workstation workstation);
	
	Invoice createInvoiceFromPurchaseRequestSnapshot(PurchaseRequest purchaseRequest, Workstore workstore);
}
