package hk.org.ha.model.pms.cache;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.persistence.phs.Ward;
import hk.org.ha.model.pms.persistence.phs.WardStock;

import java.util.List;

public interface WardStockCacherInf extends BaseCacherInf {
	
	List<WardStock> getWardStockList(Ward ward);
	
	WardStock getWardStock(Ward ward, String itemCode);
}
