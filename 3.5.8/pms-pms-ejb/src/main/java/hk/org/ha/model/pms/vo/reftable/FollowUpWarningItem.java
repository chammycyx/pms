package hk.org.ha.model.pms.vo.reftable;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

import hk.org.ha.model.pms.persistence.reftable.LocalWarning;

@ExternalizedBean(type=DefaultExternalizer.class)
public class FollowUpWarningItem implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String preHqWarnCode;
	
	private String preHqWarnDesc;
	
	private String currentHqWarnCode;
	
	private String currentHqWarnDesc;
	
	private String currentHqWarnIpOnly;

	private String currentHqWarnCatCode;
	
	private LocalWarning localWarning;

	private String localWarnCode;
	
	private String localWarnDesc;

	private String localWarnIpOnly;
	
	private boolean followCurrentHqWarnCode;
	
	private boolean keepLocalWarnCode;
	
	private boolean visiable = false;
	
	private Integer sortSeq;
	
	private boolean suspend;

	public String getCurrentHqWarnCode() {
		return currentHqWarnCode;
	}

	public void setCurrentHqWarnCode(String currentHqWarnCode) {
		this.currentHqWarnCode = currentHqWarnCode;
	}

	public String getCurrentHqWarnDesc() {
		return currentHqWarnDesc;
	}

	public void setCurrentHqWarnDesc(String currentHqWarnDesc) {
		this.currentHqWarnDesc = currentHqWarnDesc;
	}


	public String getLocalWarnCode() {
		return localWarnCode;
	}

	public void setLocalWarnCode(String localWarnCode) {
		this.localWarnCode = localWarnCode;
	}

	public String getLocalWarnDesc() {
		return localWarnDesc;
	}

	public void setLocalWarnDesc(String localWarnDesc) {
		this.localWarnDesc = localWarnDesc;
	}

	public boolean isKeepLocalWarnCode() {
		return keepLocalWarnCode;
	}

	public void setKeepLocalWarnCode(boolean keepLocalWarnCode) {
		this.keepLocalWarnCode = keepLocalWarnCode;
	}

	public void setSortSeq(Integer sortSeq) {
		this.sortSeq = sortSeq;
	}

	public Integer getSortSeq() {
		return sortSeq;
	}

	public void setCurrentHqWarnIpOnly(String currentHqWarnIpOnly) {
		this.currentHqWarnIpOnly = currentHqWarnIpOnly;
	}

	public String getCurrentHqWarnIpOnly() {
		return currentHqWarnIpOnly;
	}

	public void setFollowCurrentHqWarnCode(boolean followCurrentHqWarnCode) {
		this.followCurrentHqWarnCode = followCurrentHqWarnCode;
	}

	public boolean isFollowCurrentHqWarnCode() {
		return followCurrentHqWarnCode;
	}

	public void setLocalWarnIpOnly(String localWarnIpOnly) {
		this.localWarnIpOnly = localWarnIpOnly;
	}

	public String getLocalWarnIpOnly() {
		return localWarnIpOnly;
	}

	public void setLocalWarning(LocalWarning localWarning) {
		this.localWarning = localWarning;
	}

	public LocalWarning getLocalWarning() {
		return localWarning;
	}

	public void setPreHqWarnCode(String preHqWarnCode) {
		this.preHqWarnCode = preHqWarnCode;
	}

	public String getPreHqWarnCode() {
		return preHqWarnCode;
	}

	public void setPreHqWarnDesc(String preHqWarnDesc) {
		this.preHqWarnDesc = preHqWarnDesc;
	}

	public String getPreHqWarnDesc() {
		return preHqWarnDesc;
	}

	public void setCurrentHqWarnCatCode(String currentHqWarnCatCode) {
		this.currentHqWarnCatCode = currentHqWarnCatCode;
	}

	public String getCurrentHqWarnCatCode() {
		return currentHqWarnCatCode;
	}

	public void setVisiable(boolean visiable) {
		this.visiable = visiable;
	}

	public boolean isVisiable() {
		return visiable;
	}

	public boolean getSuspend() {
		return suspend;
	}

	public void setSuspend(boolean suspend) {
		this.suspend = suspend;
	}
}