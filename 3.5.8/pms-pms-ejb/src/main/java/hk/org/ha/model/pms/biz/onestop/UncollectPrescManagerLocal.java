package hk.org.ha.model.pms.biz.onestop;

import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.udt.disp.PharmOrderAdminStatus;

import javax.ejb.Local;

@Local
public interface UncollectPrescManagerLocal {
	void uncollectPrescription(DispOrder dispOrder, String suspendCode, String caller);
	
	DispOrder markRevokeFlagForUncollectMOEOrder(DispOrder dispOrder, PharmOrderAdminStatus adminStatus);
	
	DispOrder forceUpdateDispOrder(DispOrder dispOrder);
	
	void writeAuditLog(String msgCode, Long dispOrderId, String suspendCode);
	
	void reverseUncollectPrescription(DispOrder dispOrder, String caller, boolean updateCorp, boolean updateLargeLayoutFlag);
}
