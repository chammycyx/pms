package hk.org.ha.model.pms.udt.medprofile;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum ReplenishmentStatus implements StringValuedEnum {
	
	Outstanding("O", "Outstanding"),
	Processed("P", "Processed"),
	SysDeleted("X", "SysDeleted");
	
	private final String dataValue;
	private final String displayValue;

	private ReplenishmentStatus(String dataValue,
			String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}
	
	public static ReplenishmentStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(ReplenishmentStatus.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<ReplenishmentStatus> {
		
		private static final long serialVersionUID = 1L;

		public Class<ReplenishmentStatus> getEnumClass() {
			return ReplenishmentStatus.class;
		}
	}
}
