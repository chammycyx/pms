package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class MpItemPropertyRpt implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String workstoreGroupCode;
	
	private String itemCode;
	
	private String fullDrugDesc;
	
	private String singleFlag;
	
	private String dailyRefillFlag;
	
	private String defaultDispDay;
	
	private String warningMessage;
	
	public MpItemPropertyRpt(){
		
	}
	
	public MpItemPropertyRpt(String workstoreGroupCode, String itemCode, String fullDrugDesc, String specCode,
			String singleFlag, String dailtyRefillFlag, String defaultDispDay, String warningMessage){
		super();
		this.workstoreGroupCode = workstoreGroupCode;
		this.itemCode = itemCode;
		this.fullDrugDesc = fullDrugDesc;
		this.singleFlag = singleFlag;
		this.dailyRefillFlag = dailtyRefillFlag;
		this.defaultDispDay = defaultDispDay;
		this.warningMessage = warningMessage;
	}

	public String getWorkstoreGroupCode() {
		return workstoreGroupCode;
	}

	public void setWorkstoreGroupCode(String workstoreGroupCode) {
		this.workstoreGroupCode = workstoreGroupCode;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getFullDrugDesc() {
		return fullDrugDesc;
	}

	public void setFullDrugDesc(String fullDrugDesc) {
		this.fullDrugDesc = fullDrugDesc;
	}

	public String getSingleFlag() {
		return singleFlag;
	}

	public void setSingleFlag(String singleFlag) {
		this.singleFlag = singleFlag;
	}

	public String getDailyRefillFlag() {
		return dailyRefillFlag;
	}

	public void setDailyRefillFlag(String dailyRefillFlag) {
		this.dailyRefillFlag = dailyRefillFlag;
	}

	public String getDefaultDispDay() {
		return defaultDispDay;
	}

	public void setDefaultDispDay(String defaultDispDay) {
		this.defaultDispDay = defaultDispDay;
	}

	public String getWarningMessage() {
		return warningMessage;
	}

	public void setWarningMessage(String warningMessage) {
		this.warningMessage = warningMessage;
	}
	
}
