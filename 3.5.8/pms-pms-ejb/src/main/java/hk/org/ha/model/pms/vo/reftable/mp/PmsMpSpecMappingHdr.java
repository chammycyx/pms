package hk.org.ha.model.pms.vo.reftable.mp;

import hk.org.ha.model.pms.dms.persistence.PmsIpSpecMapping;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PmsMpSpecMappingHdr implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String patHospCode;
	
	private String pasSpecialty;
	
	private String ward;
	
	private Integer mapped;
	
	private boolean excludeFlag = false;
	
	private List<PmsIpSpecMapping> pmsIpSpecMappingList;

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public String getPasSpecialty() {
		return pasSpecialty;
	}

	public void setPasSpecialty(String pasSpecialty) {
		this.pasSpecialty = pasSpecialty;
	}

	public String getWard() {
		return ward;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}

	public Integer getMapped() {
		return mapped;
	}

	public void setMapped(Integer mapped) {
		this.mapped = mapped;
	}

	public boolean getExcludeFlag() {
		return excludeFlag;
	}

	public void setExcludeFlag(boolean excludeFlag) {
		this.excludeFlag = excludeFlag;
	}

	public List<PmsIpSpecMapping> getPmsIpSpecMappingList() {
		return pmsIpSpecMappingList;
	}

	public void setPmsIpSpecMappingList(List<PmsIpSpecMapping> pmsIpSpecMappingList) {
		this.pmsIpSpecMappingList = pmsIpSpecMappingList;
	}
}