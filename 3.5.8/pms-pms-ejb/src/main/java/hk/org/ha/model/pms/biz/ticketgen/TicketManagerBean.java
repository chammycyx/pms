package hk.org.ha.model.pms.biz.ticketgen;

import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.order.NumberGeneratorLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
import hk.org.ha.model.pms.udt.refill.RefillScheduleStatus;
import hk.org.ha.model.pms.udt.ticketgen.TicketType;
import hk.org.ha.model.pms.udt.vetting.OrderType;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("ticketManager")
@MeasureCalls
public class TicketManagerBean implements TicketManagerLocal {
	
	@PersistenceContext
	private EntityManager em;
			
	@In
	private Workstore workstore;
	
	@In
	private NumberGeneratorLocal numberGenerator;
	
	private static final String ERR_MSG_BEEN_USED = "0249";
					
	@SuppressWarnings("unchecked")
	public Ticket retrieveTicket(String ticketNum, Date ticketDate){			
		Ticket ticket = null;
		
		List<Ticket> ticketList = em.createQuery(
				"select o from Ticket o" + // 20120307 index check : Ticket.workstore,orderType,ticketDate,ticketNum : UI_TICKET_01
				" where o.workstore = :workstore" +
				" and o.orderType = :orderType" +
				" and o.ticketDate = :ticketDate" +
				" and o.ticketNum = :ticketNum")
		.setParameter("workstore", workstore)
		.setParameter("orderType", OrderType.OutPatient)
		.setParameter("ticketNum", ticketNum)
		.setParameter("ticketDate", ticketDate, TemporalType.DATE)
		.getResultList();
		
		if (!ticketList.isEmpty()) {		
			ticket = ticketList.get(0);
		}
		return ticket;
	}

	public Ticket generateTicket(Date ticketDate, TicketType ticketType, String customTicketNumText, String moeOrderNum){
		Integer customTicketNum = null;	
		if( customTicketNumText != null ){
			customTicketNum = Integer.valueOf(customTicketNumText);
		}
		
		Ticket newTicket = numberGenerator.retrieveTicketByTickDateType(ticketDate, ticketType, customTicketNum);

		if( newTicket != null && StringUtils.isNotBlank(moeOrderNum) ){
			newTicket.setOrderNum(moeOrderNum);
			em.merge(newTicket);
			em.flush();
		}
		em.clear();
		
		return newTicket;
	}
	
	public boolean isFastQueueTicket(Date ticketDate, String ticketNum){
		return numberGenerator.isFastQueueTicketNum(ticketNum);
	}
	
	@SuppressWarnings("unchecked")
	public Pair<String, Ticket> validateTicket(Date ticketDate, String ticketNum, Ticket ticketIn, Long medOrderId, OneStopOrderType orderType, boolean oneStopCheck){
		String errMsg = null;
		Ticket ticket = ticketIn;
		
		if( oneStopCheck ){
		
			ticket = retrieveTicket(ticketNum, ticketDate);
			
			if( ticket == null ){
				errMsg = "0002";
				return new Pair<String, Ticket>(errMsg, ticket);
			}
		}
		
		List<Long> medOrderIdList = em.createQuery(
				"select o.id from MedOrder o" + // 20120307 index check : MedOrder.ticket : FK_MED_ORDER_03
				" where o.ticket = :ticket" +
				" and o.status in :statusList" +
				" order by o.orderDate desc")										
				.setParameter("ticket", ticket)
				.setParameter("statusList", MedOrderStatus.Outstanding_Withhold_PreVet)
				.getResultList();
		
		if( medOrderIdList.size() > 0 ){
			if ( medOrderId != null && (medOrderIdList.get(0)).compareTo(medOrderId) == 0 ){
				return new Pair<String, Ticket>(errMsg, ticket);
			}
			ticket = null;
			errMsg = ERR_MSG_BEEN_USED;
			return new Pair<String, Ticket>(errMsg, ticket);
		}
		
		List<Long> dispOrderList = em.createQuery(
				"select o.id from DispOrder o" + // 20120307 index check : DispOrder.ticket : FK_DISP_ORDER_03
				" where o.ticket = :ticket"+
				" and o.status <> :dispOrderStatus")
				.setParameter("ticket", ticket)
				.setParameter("dispOrderStatus", DispOrderStatus.SysDeleted)
				.getResultList();

		if( dispOrderList.size() > 0 ){
			ticket = null;
			errMsg = ERR_MSG_BEEN_USED;
			return new Pair<String, Ticket>(errMsg, ticket);
		}
		
		List<Long> refillScheduleList = em.createQuery(
				"select o.id from RefillSchedule o" + // 20120307 index check : RefillSchedule.ticket : FK_REFILL_SCHEDULE_03
				" where o.ticket = :ticket" +
				" and o.status in :statusList")
				.setParameter("ticket", ticket)
				.setParameter("statusList", Arrays.asList(RefillScheduleStatus.Abort))
				.getResultList();
		
		if( refillScheduleList.size() > 0 ){
			ticket = null;
			errMsg = ERR_MSG_BEEN_USED;
			return new Pair<String, Ticket>(ERR_MSG_BEEN_USED, ticket);
		}
		
		//Check Ticket Num is used by Cancelled MOE Order Num
		List<MedOrder> cancelMedOrderList = em.createQuery(
												"select o from MedOrder o" + // 20120307 index check : MedOrder.ticket : FK_MED_ORDER_03
												" where o.ticket = :ticket" +
												" and o.status = :status" +
												" and o.docType = :docType")
												.setParameter("ticket", ticket)
												.setParameter("status", MedOrderStatus.Deleted)
												.setParameter("docType", MedOrderDocType.Normal)
												.getResultList();
		
		if( cancelMedOrderList.size() > 0 ){
			if( OneStopOrderType.MedOrder == orderType && medOrderId != null ){
				MedOrder currMedOrder = em.find(MedOrder.class, medOrderId);
				if( currMedOrder.getPrevOrderNum() != null ){
					for( MedOrder prevCancelMo : cancelMedOrderList ){
						if ( prevCancelMo.getOrderNum().equals(currMedOrder.getPrevOrderNum()) ){
							return new Pair<String, Ticket>(errMsg, ticket);
						}
					}
					
					String prevMedOrderNum = currMedOrder.getPrevOrderNum();
					while( prevMedOrderNum != null ){
						cancelMedOrderList = retrievePrevMedOrderByPrevMedOrderNum(prevMedOrderNum);
						if( cancelMedOrderList.size() > 0 ){
							for( MedOrder prevCancelMo : cancelMedOrderList ){
								if ( ticket.equals(prevCancelMo.getTicket()) ){
									prevMedOrderNum = null;
									return new Pair<String, Ticket>(errMsg, ticket);
								}else if( prevCancelMo.getTicket() == null ){
									prevMedOrderNum = prevCancelMo.getPrevOrderNum();
								}else{
									prevMedOrderNum = null;									
								}
							}
						}else{
							break;
						}
					}
				}
			}
			ticket = null;
			errMsg = ERR_MSG_BEEN_USED;
		}
		return new Pair<String, Ticket>(errMsg, ticket);
	}
	
	@SuppressWarnings("unchecked")
	private List<MedOrder> retrievePrevMedOrderByPrevMedOrderNum(String prevMedOrderNum){
		List<MedOrder> cancelMedOrderList = em.createQuery(
				"select o from MedOrder o" + // 20140204 index check : MedOrder.orderNum : I_MED_ORDER_01
				" where o.orderNum = :prevMedOrderNum" +
				" and o.status = :status" +
				" and o.docType = :docType")
				.setParameter("prevMedOrderNum", prevMedOrderNum)
				.setParameter("status", MedOrderStatus.Deleted)
				.setParameter("docType", MedOrderDocType.Normal)
				.getResultList();
				
		return cancelMedOrderList;
	}
}