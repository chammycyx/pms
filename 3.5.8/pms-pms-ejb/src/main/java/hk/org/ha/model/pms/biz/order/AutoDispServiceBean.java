package hk.org.ha.model.pms.biz.order;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

/**
 * Session Bean implementation class AutoDispServiceBean
 */
@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("autoDispService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@TransactionManagement(TransactionManagementType.BEAN)
public class AutoDispServiceBean implements AutoDispServiceLocal {

	@In
	private AutoDispManagerLocal autoDispManager;

	public void genTicket() {
		autoDispManager.genTicket();
	}
	
	public void retrieveOrder(String orderNum) {
		autoDispManager.retrieveOrder(orderNum);
	}
	
	public void retrievePatientAndMedCase(String patientHkid, String patientName) {
		autoDispManager.retrievePatientAndMedCase(patientHkid, patientName);
	}
	
	public void checkTicket() {
		autoDispManager.checkTicket();
	}
	
	public void startVetting() throws Exception {
		autoDispManager.startVetting();
	}

	public void removeItemsIfNotConverted() throws Exception {
		autoDispManager.removeItemsIfNotConverted();
	}
	
	public void endVetting() throws Exception {
		autoDispManager.endVetting();
	}
	
	public void drugPick() {
		autoDispManager.drugPick();
	}
	
	public void assembling() {
		autoDispManager.assembling();
	}

	public void checkOrder() throws Exception {
		autoDispManager.checkOrder();
	}
	
	public void issueOrder() throws Exception {
		autoDispManager.issueOrder();
	}
	
	public void cddhSearch(String patientHkid, String patientName) {
		autoDispManager.cddhSearch(patientHkid, patientName);
	}

	public void drugSearch(String drugName) {
		autoDispManager.drugSearch(drugName);
	}

	@Remove
	public void destroy() {
	}
	
}
