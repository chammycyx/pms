package hk.org.ha.model.pms.biz.onestop;

import javax.ejb.Local;

@Local
public interface SpecialtyMoMappingServiceLocal {
	
	void retrieveDmSpecialtyMoMappingBySpecialtyCode(String specCode);
	
	void destroy();
}
