package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.persistence.reftable.RefillConfig;

import javax.ejb.Local;

@Local
public interface RefillConfigManagerLocal {
	
	RefillConfig retrieveRefillConfig();
}
