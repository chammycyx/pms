package hk.org.ha.model.pms.biz.vetting;

import static hk.org.ha.model.pms.prop.Prop.HOSPITAL_ADDRESSCHI;
import static hk.org.ha.model.pms.prop.Prop.HOSPITAL_ADDRESSENG;
import static hk.org.ha.model.pms.prop.Prop.HOSPITAL_NAMECHI;
import static hk.org.ha.model.pms.prop.Prop.HOSPITAL_NAMEENG;
import static hk.org.ha.model.pms.prop.Prop.ISSUE_TICKETMATCH_ENABLED;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.order.NumberGeneratorLocal;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.cache.DmCapdVoucherMapCacherInf;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmCapdVoucherMap;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.CapdVoucher;
import hk.org.ha.model.pms.persistence.disp.CapdVoucherItem;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.phs.Supplier;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.vetting.CapdVoucherStatus;
import hk.org.ha.model.pms.udt.vetting.DmCapdVoucherMapType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.vetting.CapdManualVoucher;
import hk.org.ha.model.pms.vo.vetting.CapdSplit;
import hk.org.ha.model.pms.vo.vetting.CapdSplitItem;
import hk.org.ha.model.pms.vo.vetting.CapdVoucherPrintOut;
import hk.org.ha.model.pms.vo.vetting.CapdVoucherPrintOutDtl;
import hk.org.ha.model.pms.vo.vetting.CapdVoucherPrintOutHdr;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("capdVoucherManager")
@MeasureCalls
public class CapdVoucherManagerBean implements CapdVoucherManagerLocal {

	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private PrintAgentInf printAgent;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private DmCapdVoucherMapCacherInf dmCapdVoucherMapCacher;

	@In
	private Workstore workstore;

	@In
	private DmDrugCacherInf dmDrugCacher;

	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
	
	@In
	private NumberGeneratorLocal numberGenerator;
	
	private static final int numOfMaskChars = 3;
	
	private DateFormat ticketLabelDateFormat = new SimpleDateFormat("ddMMyyyy");
	
	private static final String DUMMY_ITEMNUM = "00";
	
	public List<CapdVoucher> createCapdVoucherList(DispOrder dispOrder, List<CapdVoucher> prevCvList){

		List<CapdVoucher> voucherList = new ArrayList<CapdVoucher>();

		CapdSplit capdSplit = dispOrder.getPharmOrder().getCapdSplit();
		if (capdSplit != null) {
			Integer currentGroupNum = 0;
			int qtyNum = 1;
			int curQtyNum = 0;
			
			String newVoucherNum = StringUtils.EMPTY;
			CapdVoucher newCapdVoucher = null;
			String qtyField = "qty0";
					
			capdSplit = sortCapdSplitItemList(capdSplit);		
			capdSplit = clearVoucherNumList(capdSplit);
			
			// build the create list
			for (qtyNum = 1; qtyNum<=10; qtyNum++) {
				
				for ( CapdSplitItem capdSplitItem : capdSplit.getCapdSplitItemList() ) {
					if ( !capdSplitItem.getGenVoucherFlag() ) {
						continue;
					}
					
					if ( !capdSplitItem.getPrintFlag() ) {
						capdSplitItem.setGroupNum(0);
						continue;
					}
					
					if ( qtyNum==10 ) {
						qtyField = "qty";
					} else {
						qtyField = "qty0";
					}
					
					Integer dispQty = 0;
					
					try {
						dispQty = (Integer) PropertyUtils.getProperty(capdSplitItem, qtyField+qtyNum);
					} catch (IllegalAccessException e) {
					} catch (InvocationTargetException e) {
					} catch (NoSuchMethodException e) {
					}
					
					if ( dispQty <= 0 ) {
						continue;
					}
					
					//assign new Voucher no.
					if ( !currentGroupNum.equals(capdSplitItem.getGroupNum()) || qtyNum != curQtyNum) {
						newVoucherNum =  numberGenerator.retrieveCapdVoucherNum();
						currentGroupNum = capdSplitItem.getGroupNum();
	
						newCapdVoucher = new CapdVoucher();
						newCapdVoucher.setVoucherNum(newVoucherNum);
						newCapdVoucher.setStatus(CapdVoucherStatus.Suspended);
						newCapdVoucher.setSupplierCode(capdSplitItem.getSupplierCode());
						newCapdVoucher.clearCapdVoucherItemList();
						CapdManualVoucher capdManualVoucher = capdSplit.getCapdManualVoucherList().get(0);
						newCapdVoucher.setManualVoucherNum(this.retrieveManualVoucherItem(capdManualVoucher, qtyNum));
						voucherList.add(newCapdVoucher);
						capdSplitItem.getVoucherNumList().add(newVoucherNum);
						curQtyNum = qtyNum;
	
						//link
						dispOrder.addCapdVoucher(newCapdVoucher);
					} 
					
					CapdVoucherItem capdVoucherItem = new CapdVoucherItem();
					capdVoucherItem.setQty(dispQty);
					newCapdVoucher.addCapdVoucherItem(capdVoucherItem);
	
					//link
					DispOrderItem doi = dispOrder.getDispOrderItemByPoOrgItemNum(capdSplitItem.getItemNum());
					doi.addCapdVoucherItem(capdVoucherItem);				
				}
			}
	
			int newGroupNum = 0;
			currentGroupNum = 0;
			for (CapdSplitItem capdSplitItem : capdSplit.getCapdSplitItemList()){
				capdSplitItem.setGenVoucherFlag(Boolean.FALSE);
				if ( !currentGroupNum.equals(capdSplitItem.getGroupNum()) ){
					currentGroupNum = capdSplitItem.getGroupNum();
					newGroupNum++;
				}
				capdSplitItem.setGroupNum(newGroupNum);
			}

			// mark delete
			List<CapdVoucher> delCvList = new ArrayList<CapdVoucher>(prevCvList);				
			for (Iterator<CapdVoucher> itr = delCvList.iterator();itr.hasNext();) {
				CapdVoucher capdVoucher = itr.next();
				boolean removeFlag = false;
				for (CapdSplitItem capdSplitItem : capdSplit.getCapdSplitItemList()) {
					for (String voucherNum : capdSplitItem.getVoucherNumList()) {
						if (capdVoucher.getVoucherNum().equals(voucherNum)) {
							removeFlag = true;
							break;
						}
					}
				}
				if ( removeFlag ) {
					itr.remove();
				}
			}		
			for (CapdVoucher capdVoucher : delCvList) {
				capdVoucher.setStatus(CapdVoucherStatus.Deleted);
			}
		} else {
			// just mark everything deleted
			for (CapdVoucher capdVoucher : prevCvList) {
				capdVoucher.setStatus(CapdVoucherStatus.Deleted);
			}
		}
		
		// preserve all the prevoius CapdVoucher (include deleted)
		for (CapdVoucher capdVoucher : prevCvList) {
			//replace with the attached dispOrder
			capdVoucher.replace(dispOrder);
			voucherList.add(capdVoucher);
		}
		
		Collections.sort(voucherList, new ComparatorForCapdVoucherList());
		
		for ( CapdVoucher capdVoucher : voucherList ) {
			if ( capdVoucher.getId() == null ) {
				Collections.sort(capdVoucher.getCapdVoucherItemList(), new ComparatorForCapdVoucherItemList());
				int itemNum = 1;
				for ( CapdVoucherItem capdVoucherItem : capdVoucher.getCapdVoucherItemList() ) {
					capdVoucherItem.setItemNum(itemNum);
					itemNum++;
				}
			}
		}
		
		return voucherList;
	}
	
	private CapdSplit clearVoucherNumList(CapdSplit capdSplit){
		for ( CapdSplitItem capdSplitItem : capdSplit.getCapdSplitItemList() ) {
			if ( capdSplitItem.getGenVoucherFlag() || !capdSplitItem.getPrintFlag() ) {
				capdSplitItem.setVoucherNumList(new ArrayList<String>());
			}
		}
		return capdSplit;
	}
	
	@SuppressWarnings("unchecked")
	private CapdSplit sortCapdSplitItemList(CapdSplit capdSplit){
		Collections.sort(capdSplit.getCapdSplitItemList(), new Comparator(){
			public int compare(Object o1, Object o2) {
				CapdSplitItem item1 = (CapdSplitItem) o1;
				CapdSplitItem item2 = (CapdSplitItem) o2;
				return item1.getItemNum().compareTo(item2.getItemNum());
			}
		});	
		
		Collections.sort(capdSplit.getCapdSplitItemList(), new Comparator(){
			public int compare(Object o1, Object o2) {
				CapdSplitItem item1 = (CapdSplitItem) o1;
				CapdSplitItem item2 = (CapdSplitItem) o2;
				return item1.getGroupNum().compareTo(item2.getGroupNum());
			}
		});
		return capdSplit;
	}
	
	private String retrieveManualVoucherItem(CapdManualVoucher capdManualVoucher, int qtyNum){
		String manualVoucherItem = StringUtils.EMPTY;
		String numField = "num0";
		
		if ( qtyNum==10 ) {
			numField = "num";
		}

		try {
			manualVoucherItem = (String) PropertyUtils.getProperty(capdManualVoucher, numField+qtyNum);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		
		return manualVoucherItem;
	}
	
	public CapdVoucherPrintOut retrieveCapdVoucherPrintOut (CapdVoucher capdVoucher, boolean isReprint, PrintLang printLang) {
		CapdVoucherPrintOut capdVoucherPrintOut  = new CapdVoucherPrintOut();
		CapdVoucherPrintOutDtl capdVoucherPrintOutDtl = new CapdVoucherPrintOutDtl();
		CapdVoucherPrintOutHdr capdVoucherPrintOutHdr = new CapdVoucherPrintOutHdr();
		
		capdVoucherPrintOutHdr.setReprintFlag(isReprint);
		if ( StringUtils.isNotBlank(capdVoucher.getManualVoucherNum()) ){
			capdVoucherPrintOutHdr.setManualVoucherNum(capdVoucher.getManualVoucherNum());
		}
		
		capdVoucherPrintOutHdr.setTicketNum(capdVoucher.getDispOrder().getTicket().getTicketNum());
		capdVoucherPrintOutHdr.setWorkstoreCode(workstore.getWorkstoreCode());
		
		if( ISSUE_TICKETMATCH_ENABLED.get(false) ){
			StringBuilder ticketMatchBarcodeSb = new StringBuilder();
			ticketMatchBarcodeSb.append(ticketLabelDateFormat.format(capdVoucher.getDispOrder().getTicket().getTicketDate()))
						.append(capdVoucher.getDispOrder().getTicket().getTicketNum())
						.append(DUMMY_ITEMNUM);
			capdVoucherPrintOutHdr.setTicketMatchBarcode(ticketMatchBarcodeSb.toString());
		}
		
		if ( capdVoucher.getDispOrder().getPharmOrder().getMedCase() != null ) {
			capdVoucherPrintOutHdr.setCaseNum(capdVoucher.getDispOrder().getPharmOrder().getMedCase().getCaseNum());
		}
		if ( printLang.equals(PrintLang.Chi) ) {
			capdVoucherPrintOutHdr.setHospName(HOSPITAL_NAMECHI.get());
			capdVoucherPrintOutHdr.setHospAddress(HOSPITAL_ADDRESSCHI.get());
		} else {
			capdVoucherPrintOutHdr.setHospName(HOSPITAL_NAMEENG.get());
			capdVoucherPrintOutHdr.setHospAddress(HOSPITAL_ADDRESSENG.get());
		}
		capdVoucherPrintOutHdr.setVoucherNum(capdVoucher.getVoucherNum());
		if ( StringUtils.isNotBlank(capdVoucher.getSupplierCode()) ) {
			Supplier supplier = em.find(Supplier.class, capdVoucher.getSupplierCode());
			if ( supplier != null ){
				capdVoucherPrintOutHdr.setSupplierName(supplier.getName());
				capdVoucherPrintOutHdr.setSupplierNameChi(supplier.getNameChi());
				capdVoucherPrintOutHdr.setSupplierPhone(supplier.getCapdPhone());
			}
		}
		
		capdVoucherPrintOutHdr.setPatName(capdVoucher.getDispOrder().getPharmOrder().getPatient().getName());
		capdVoucherPrintOutHdr.setPatNameChi(capdVoucher.getDispOrder().getPharmOrder().getPatient().getNameChi());
		capdVoucherPrintOutHdr.setHkid(getMaskedHkid(capdVoucher.getDispOrder().getPharmOrder().getPatient().getHkid()));
		capdVoucherPrintOutHdr.setDispDate(capdVoucher.getDispOrder().getTicket().getTicketDate());
		
		int voucherItemNum = 1;
		for ( CapdVoucherItem capdVoucherItem : capdVoucher.getCapdVoucherItemList() ){
				try {
					DmDrug dmDrug = dmDrugCacher.getDmDrug(capdVoucherItem.getDispOrderItem().getPharmOrderItem().getItemCode());
					PropertyUtils.setProperty(capdVoucherPrintOutDtl, "itemCode"+voucherItemNum,  capdVoucherItem.getDispOrderItem().getPharmOrderItem().getItemCode());
					PropertyUtils.setProperty(capdVoucherPrintOutDtl, "fullDrugDesc"+voucherItemNum, capdVoucherItem.getDrugDesc());
					PropertyUtils.setProperty(capdVoucherPrintOutDtl, "dispQty"+voucherItemNum, capdVoucherItem.getQty().toString());
					
					if ( dmDrug.getDmDrugCapd() != null ){
						if ( printLang.equals(PrintLang.Eng)) {
	 						PropertyUtils.setProperty(capdVoucherPrintOutDtl, "moDosageUnit"+voucherItemNum, dmDrug.getBaseUnit());
	 						if ( dmDrug.getDmDrugCapd().getVolumeValue() != null ) {
								PropertyUtils.setProperty(capdVoucherPrintOutDtl, "volume"+voucherItemNum, dmDrug.getDmDrugCapd().getVolumeValue().toString());
								PropertyUtils.setProperty(capdVoucherPrintOutDtl, "volumeUnit"+voucherItemNum, dmDrug.getDmDrugCapd().getVolumeUnit());
	 						}
	 						if ( dmDrug.getDmDrugCapd().getStrengthValue() != null ) {
								PropertyUtils.setProperty(capdVoucherPrintOutDtl, "strengthValue"+voucherItemNum, dmDrug.getDmDrugCapd().getStrengthValue().toString());
	 						}
	 						if ( dmDrug.getDmDrugCapd().getStrengthUnit() != null ) {
								PropertyUtils.setProperty(capdVoucherPrintOutDtl, "strengthUnit"+voucherItemNum, dmDrug.getDmDrugCapd().getStrengthUnit());
	 						}
							PropertyUtils.setProperty(capdVoucherPrintOutDtl, "ingredient"+voucherItemNum, dmDrug.getDmDrugCapd().getIngredient());
							PropertyUtils.setProperty(capdVoucherPrintOutDtl, "calciumStrengthUnit"+voucherItemNum, dmDrug.getDmDrugCapd().getCalciumStrengthUnit());
							PropertyUtils.setProperty(capdVoucherPrintOutDtl, "connectSystem"+voucherItemNum, dmDrug.getDmDrugCapd().getConnectSystem());
						} 
						else 
						{
							DmCapdVoucherMap baseUnit = dmCapdVoucherMapCacher.getDmCapdVoucher(dmDrug.getBaseUnit(), DmCapdVoucherMapType.BaseUnit);
							if ( baseUnit != null ) 
							{
								PropertyUtils.setProperty(capdVoucherPrintOutDtl, "moDosageUnit"+voucherItemNum, baseUnit.getTxtChi());
							} 
							else 
							{
		 						PropertyUtils.setProperty(capdVoucherPrintOutDtl, "moDosageUnit"+voucherItemNum, dmDrug.getBaseUnit());
							}
							
							if ( dmDrug.getDmDrugCapd().getVolumeValue() != null ) 
							{
								PropertyUtils.setProperty(capdVoucherPrintOutDtl, "volume"+voucherItemNum, dmDrug.getDmDrugCapd().getVolumeValue().toString());
								DmCapdVoucherMap volumeUnit = dmCapdVoucherMapCacher.getDmCapdVoucher(dmDrug.getDmDrugCapd().getVolumeUnit(), DmCapdVoucherMapType.VolumeUnit);
								if ( volumeUnit != null ) 
								{
									PropertyUtils.setProperty(capdVoucherPrintOutDtl, "volumeUnit"+voucherItemNum, volumeUnit.getTxtChi() );
								}
								else 
								{
									PropertyUtils.setProperty(capdVoucherPrintOutDtl, "volumeUnit"+voucherItemNum, dmDrug.getDmDrugCapd().getVolumeUnit());
								}
							}
							if ( dmDrug.getDmDrugCapd().getStrengthValue() != null ) 
							{
								PropertyUtils.setProperty(capdVoucherPrintOutDtl, "strengthValue"+voucherItemNum, dmDrug.getDmDrugCapd().getStrengthValue().toString());
							}
							
							if ( dmDrug.getDmDrugCapd().getStrengthUnit() != null ) 
							{
								PropertyUtils.setProperty(capdVoucherPrintOutDtl, "strengthUnit"+voucherItemNum, dmDrug.getDmDrugCapd().getStrengthUnit());
							}
							
							DmCapdVoucherMap ingredient = dmCapdVoucherMapCacher.getDmCapdVoucher(dmDrug.getDmDrugCapd().getIngredient(), DmCapdVoucherMapType.Ingredient);
							if ( ingredient != null ) 
							{
								PropertyUtils.setProperty(capdVoucherPrintOutDtl, "ingredient"+voucherItemNum, ingredient.getTxtChi());
							}
							else 
							{
								PropertyUtils.setProperty(capdVoucherPrintOutDtl, "ingredient"+voucherItemNum, dmDrug.getDmDrugCapd().getIngredient());
							}
							
							DmCapdVoucherMap calciumStrengthUnit = dmCapdVoucherMapCacher.getDmCapdVoucher(dmDrug.getDmDrugCapd().getCalciumStrengthUnit(), DmCapdVoucherMapType.CalciumStrengthUnit);
							if ( calciumStrengthUnit != null ) {
								PropertyUtils.setProperty(capdVoucherPrintOutDtl, "calciumStrengthUnit"+voucherItemNum, calciumStrengthUnit.getTxtChi());
							} else {
								PropertyUtils.setProperty(capdVoucherPrintOutDtl, "calciumStrengthUnit"+voucherItemNum, dmDrug.getDmDrugCapd().getCalciumStrengthUnit());
							}
							
							DmCapdVoucherMap connectSystem = dmCapdVoucherMapCacher.getDmCapdVoucher(dmDrug.getDmDrugCapd().getConnectSystem(), DmCapdVoucherMapType.ConnectSystem);
							if ( connectSystem != null ) {
								PropertyUtils.setProperty(capdVoucherPrintOutDtl, "connectSystem"+voucherItemNum, connectSystem.getTxtChi());
							} else {
								PropertyUtils.setProperty(capdVoucherPrintOutDtl, "connectSystem"+voucherItemNum, dmDrug.getDmDrugCapd().getConnectSystem());
							}
						}
					}
					
				} catch (IllegalAccessException e) {
				} catch (InvocationTargetException e) {
				} catch (NoSuchMethodException e) {
				}
				voucherItemNum++;
		}
		
		capdVoucherPrintOut.setCapdVoucherPrintOutDtl(capdVoucherPrintOutDtl);
		capdVoucherPrintOut.setCapdVoucherPrintOutHdr(capdVoucherPrintOutHdr);
		return capdVoucherPrintOut;
	}

	public void printCapdVoucher(PrintLang printLang, List<CapdVoucher> capdVoucherList, boolean isReprint){

		List<RenderAndPrintJob> printJobList = new ArrayList<RenderAndPrintJob>();
		
		this.prepareCapdVoucherPrintJob(printJobList, printLang, capdVoucherList, isReprint);
		
		printAgent.renderAndPrint(printJobList);
	}
	
	public void prepareCapdVoucherPrintJob(List<RenderAndPrintJob> printJobList, PrintLang printLang, List<CapdVoucher> capdVoucherList, boolean isReprint)
	{
		String voucherType = "CapdVoucher";
		
		PrintOption printOption = new PrintOption();
		printOption.setPrintLang(printLang);
		
		for (CapdVoucher capdVoucher : capdVoucherList){
			
			Collections.sort(capdVoucher.getCapdVoucherItemList(), new ComparatorForCapdVoucherItemNumList());
			List<CapdVoucherPrintOut> capdVoucherPrintOutList = new ArrayList<CapdVoucherPrintOut>();
			capdVoucherPrintOutList.add(this.retrieveCapdVoucherPrintOut(capdVoucher, isReprint, printLang));
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("hospCode", workstore.getHospCode());
			parameters.put("workstoreCode", workstore.getWorkstoreCode());

		    printAgent.renderAndPrint(new RenderAndPrintJob(
		    		voucherType,
		    		printOption,
		    		printerSelector.retrievePrinterSelect(PrintDocType.CapdVoucher),
				    parameters,
				    capdVoucherPrintOutList,
				    "[CapdVoucher] "
					));
		}
		
	}
	
	public List<CapdVoucher> retrieveCapdVoucherList(DispOrder dispOrder){
		return retrieveCapdVoucherList(dispOrder, false);
	}
	
	@SuppressWarnings("unchecked")
	public List<CapdVoucher> retrieveCapdVoucherList(DispOrder dispOrder, boolean includeDeleted){

		if (dispOrder == null) {
			return new ArrayList<CapdVoucher>();
		}
		
		StringBuilder sb = new StringBuilder();
		sb.append("select o from CapdVoucher o" + // 20120307 index check : CapdVoucher.dispOrder : FK_CAPD_VOUCHER_01
					" where o.dispOrder = :dispOrder");
		if (!includeDeleted) {
			sb.append(" and o.status = :status");
		}
		sb.append(" order by o.voucherNum");
		
		Query query = em.createQuery(sb.toString());		
		query.setParameter("dispOrder", dispOrder);
		if (!includeDeleted) {
			query.setParameter("status", CapdVoucherStatus.Issued);
		}
			
		return query.getResultList();
	}	
	
	@SuppressWarnings("unchecked")
	public void prepareCapdVoucherPrintJob(List<RenderAndPrintJob> printJobList, DispOrder dispOrder) {
		List<CapdVoucher> capdVoucherList = em.createQuery(
				"select o from CapdVoucher o" + // 20130829 index check : CapdVoucher.dispOrder : FK_CAPD_VOUCHER_01
				" where o.dispOrder = :dispOrder" +
				" and o.status = :suspendedStatus")
				.setParameter("dispOrder", dispOrder)
				.setParameter("suspendedStatus", CapdVoucherStatus.Suspended)
				.getResultList();
		
		if ( !capdVoucherList.isEmpty() ) {
			prepareCapdVoucherPrintJob(new ArrayList<RenderAndPrintJob>(), dispOrder.getPrintLang(), capdVoucherList, false);
			updateCapdVoucherStatusToIssue(capdVoucherList);
		}
	}
	
	public void updateCapdVoucherStatusToIssue(List<CapdVoucher> capdVoucherList){
		corpPmsServiceProxy.updateCapdVoucherStatusToIssue(capdVoucherList);
		for ( CapdVoucher capdVoucher : capdVoucherList ) {
			capdVoucher.setStatus(CapdVoucherStatus.Issued);
			em.merge(capdVoucher);
		}
		em.flush();
	}	
	
	private String getMaskedHkid(String hkid){
		String maskedHkid = StringUtils.EMPTY;
		if ( StringUtils.isNotBlank(hkid) ) {
			maskedHkid = hkid.substring(0, hkid.length() - numOfMaskChars).concat(StringUtils.repeat("*", numOfMaskChars));	
		}
		return maskedHkid;
	}
	
	private static class ComparatorForCapdVoucherList implements Comparator<CapdVoucher>, Serializable {
		
		private static final long serialVersionUID = 1L;

		public int compare(CapdVoucher v1, CapdVoucher v2) {
						
			return v1.getVoucherNum().compareTo(v2.getVoucherNum());
		}
	}
	
	private static class ComparatorForCapdVoucherItemList implements Comparator<CapdVoucherItem>, Serializable {
		
		private static final long serialVersionUID = 1L;

		public int compare(CapdVoucherItem v1, CapdVoucherItem v2) {
						
			return v1.getDispOrderItem().getPharmOrderItem().getItemNum().compareTo(v2.getDispOrderItem().getPharmOrderItem().getItemNum());
		}
	}
	
	private static class ComparatorForCapdVoucherItemNumList implements Comparator<CapdVoucherItem>, Serializable {
		
		private static final long serialVersionUID = 1L;

		public int compare(CapdVoucherItem v1, CapdVoucherItem v2) {
						
			if ( v1.getItemNum() == null || v2.getItemNum() == null ) {
				return v1.getDispOrderItem().getPharmOrderItem().getItemNum().compareTo(v2.getDispOrderItem().getPharmOrderItem().getItemNum());
			}
			return v1.getItemNum().compareTo(v2.getItemNum());
		}
	}
}