package hk.org.ha.model.pms.biz.reftable.cmm;

import java.util.ArrayList;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.reftable.SpecialtyManagerLocal;
import hk.org.ha.model.pms.dms.persistence.cmm.CmmSpecialty;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.phs.Specialty;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("cmmSpecialtyService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CmmSpecialtyServiceBean implements CmmSpecialtyServiceLocal {

	@PersistenceContext
	private EntityManager em;

	@In
	private Workstore workstore;
	
	@In
	private SpecialtyManagerLocal specialtyManager;

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

	public List<String> retrieveSpecialtyCodeList() {
		List<String> specCodeList = new ArrayList<String>();
		for (Specialty specialty : specialtyManager.retrieveSpecialtyListWithoutDeleteStatus()) {
			specCodeList.add(specialty.getSpecCode());
		}
		return specCodeList;
	}

	public List<CmmSpecialty> retrieveCmmSpecialtyList() {
		return dmsPmsServiceProxy.retrieveCmmSpecialtyList(workstore.getHospCode());
	}

	public List<CmmSpecialty> updateCmmSpecialtyList(List<CmmSpecialty> cmmSpecialtyList) {
		dmsPmsServiceProxy.updateCmmSpecialtyList(cmmSpecialtyList, workstore.getHospCode());
		return retrieveCmmSpecialtyList();
	}

	@Remove
	public void destroy() {
	}
}
