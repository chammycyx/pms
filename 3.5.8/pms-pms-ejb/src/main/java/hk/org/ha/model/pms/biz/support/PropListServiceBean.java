package hk.org.ha.model.pms.biz.support;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.CorporateProp;
import hk.org.ha.model.pms.persistence.reftable.HospitalProp;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.persistence.reftable.OperationProp;
import hk.org.ha.model.pms.persistence.reftable.Prop;
import hk.org.ha.model.pms.persistence.reftable.WorkstoreProp;
import hk.org.ha.model.pms.udt.DataType;
import hk.org.ha.model.pms.udt.RecordStatus;
import hk.org.ha.model.pms.udt.YesNoFlag;
import hk.org.ha.model.pms.udt.reftable.MaintScreen;
import hk.org.ha.model.pms.udt.support.PropType;
import hk.org.ha.model.pms.vo.support.PropInfo;
import hk.org.ha.model.pms.vo.support.PropInfoItem;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("propListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PropListServiceBean implements PropListServiceLocal {
	
	@PersistenceContext
	private EntityManager em;	
	
	@In
	private PropManagerLocal propManager;
	
	@In 
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
	
	@Out(required = false)
	private List<PropInfo> propInfoList;
	
	@Out(required = false)
	private List<PropInfoItem> propInfoItemList;
	
	private PropType propType;
	private Hospital hospital;
	private Long operationProfileId;
	
	@SuppressWarnings("unchecked")
	public List<Hospital> retrieveHospitalList() {
		return  em.createQuery(
					"select o from Hospital o" + // 20120306 index check : none
					" where o.status = :activeStatus" + 
					" order by o.hospCode")
					.setParameter("activeStatus", RecordStatus.Active)
					.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Workstore> retrieveWorkstoreList() {
		return em.createQuery(
					"select o from Workstore o" + // 20130223 index check : Workstore.hospital : FK_WORKSTORE_01
					" where o.hospital.status = :hospitalActiveStatus" +
					" and o.status = :activeStatus" +
					" order by o.hospCode," +
					" o.workstoreCode")
					.setParameter("hospitalActiveStatus", RecordStatus.Active)
					.setParameter("activeStatus", RecordStatus.Active)
					.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<OperationProfile> retrieveOperationProfileList(String hospCode, String workstoreCode) {
		return em.createQuery(
					"select o from OperationProfile o" + // 20120831 index check : OperationProfile.workstore : FK_OPERATION_PROFILE_01
					" where o.workstore.hospCode = :hospCode" +
					" and o.workstore.workstoreCode = :workstoreCode" +
					" and o.id <> o.workstore.operationProfileId" +
					" order by o.name")
					.setParameter("hospCode", hospCode)
					.setParameter("workstoreCode", workstoreCode)
					.getResultList();
	}

	public void retrievePropList(PropType inPropType, Hospital inHospital, Long inOperationProfileId) {
		propType = inPropType;
		hospital = inHospital;
		operationProfileId = inOperationProfileId;
		
		switch (propType) {
			case CorporateProp:
				retrieveClusterCorpPropInfoItemList();
				break;
			case HospitalProp:
				retrieveHospitalPropInfoList();
				break;
			case WorkstoreProp:
				retrieveWorkstorePropInfoList(hospital);
				break;
			case OperationProp:
				retrieveOperationPropInfoItemList(operationProfileId);
				break;
		}
	}
	
	private void retrieveClusterCorpPropInfoItemList() {
		propInfoItemList = new ArrayList<PropInfoItem>();
		
		List<CorporateProp> clusterCorpPropList = propManager.retrieveCorporatePropList();
		if ( !clusterCorpPropList.isEmpty() ) {
			convertClusterCorpPropListToPropInfoItemList(clusterCorpPropList);
		}
	}
	
	private void retrieveOperationPropInfoItemList(Long inOperationProfileId) {
		propInfoItemList = new ArrayList<PropInfoItem>();
		
		List<OperationProp> operationPropList = propManager.retrieveOperationPropList(inOperationProfileId);
		if ( !operationPropList.isEmpty() ) {
			convertOperationPropListToPropInfoItemList(operationPropList);
		}
	}
	
	private void retrieveHospitalPropInfoList() {
		propInfoList = new ArrayList<PropInfo>();
		
		List<HospitalProp> hospitalPropList = propManager.retrieveHospitalPropList();
		if ( !hospitalPropList.isEmpty() ) {
			convertHospitalPropListToPropInfoList(hospitalPropList);
			sortPropInfoList(propInfoList);
			for ( PropInfo propInfo : propInfoList ) {
				sortPropInfoItemByHospCode(propInfo.getPropInfoItemList());
			}
		}
	}
	
	private void retrieveWorkstorePropInfoList(Hospital inHospital) {
		propInfoList = new ArrayList<PropInfo>();
		
		List<WorkstoreProp> workstorePropList = propManager.retrieveWorkstorePropList(inHospital);
		if ( !workstorePropList.isEmpty() ) {
			convertWorkstorePropListToPropInfoList(workstorePropList);
			sortPropInfoList(propInfoList);
			for ( PropInfo propInfo : propInfoList ) {
				sortPropInfoItemByWorkstoreCode(propInfo.getPropInfoItemList());
			}
		}
	}
	
	private void convertClusterCorpPropListToPropInfoItemList(List<CorporateProp> clusterCorpPropList) 
	{
		for ( CorporateProp corpProp : clusterCorpPropList ) {
			PropInfoItem propInfoItem = new PropInfoItem();
			propInfoItemList.add(propInfoItem);
			propInfoItem.setProp(corpProp.getProp());
			propInfoItem.setSortSeq(corpProp.getSortSeq());
			propInfoItem.setSupportMaintFlag(getSupportMaintFlag(corpProp.getProp().getMaintScreen()));
			
			propInfoItem.setDescription(corpProp.getProp().getDescription());
			propInfoItem.setPropItemId(corpProp.getId());
			propInfoItem.setValue(corpProp.getValue());
			if ( propInfoItem.getProp().getType() == DataType.BooleanType ) {
				propInfoItem.setDisplayValue(getBooleanDisplayValue(corpProp.getValue()));
			} else {
				propInfoItem.setDisplayValue(corpProp.getValue());
			}
		}
	}
	
	private void convertOperationPropListToPropInfoItemList(List<OperationProp> operationPropList) {
		for ( OperationProp operationProp : operationPropList ) {
			PropInfoItem propInfoItem = new PropInfoItem();
			propInfoItemList.add(propInfoItem);
			propInfoItem.setProp(operationProp.getProp());
			propInfoItem.setSortSeq(operationProp.getSortSeq());
			propInfoItem.setSupportMaintFlag(getSupportMaintFlag(operationProp.getProp().getMaintScreen()));
			
			propInfoItem.setDescription(operationProp.getProp().getDescription());
			propInfoItem.setPropItemId(operationProp.getId());
			propInfoItem.setValue(operationProp.getValue());
			if ( propInfoItem.getProp().getType() == DataType.BooleanType ) {
				propInfoItem.setDisplayValue(getBooleanDisplayValue(operationProp.getValue()));
			} else {
				propInfoItem.setDisplayValue(operationProp.getValue());
			}
		}
	}
	
	private void convertHospitalPropListToPropInfoList(List<HospitalProp> hospitalPropList) {
		
		Map<Prop, List<HospitalProp>> propMap = new HashMap<Prop, List<HospitalProp>>();
		for ( HospitalProp hospitalProp : hospitalPropList ) {
			if ( !propMap.containsKey(hospitalProp.getProp()) ) {
				propMap.put(hospitalProp.getProp(), new ArrayList<HospitalProp>());
			} 
			propMap.get(hospitalProp.getProp()).add(hospitalProp);
		}
		
		for ( Map.Entry<Prop, List<HospitalProp>> entry : propMap.entrySet() ) {
			Prop prop = entry.getKey();
			List<HospitalProp> propList = entry.getValue();
			
			PropInfo propInfo = new PropInfo();
			propInfoList.add(propInfo);
			propInfo.setDescription(prop.getDescription());
			propInfo.setType(prop.getType());
			propInfo.setValidation(prop.getValidation());
			propInfo.setSortSeq(propList.get(0).getSortSeq());
			propInfo.setSupportMaintFlag(getSupportMaintFlag(prop.getMaintScreen()));
			propInfo.setPropInfoItemList(new ArrayList<PropInfoItem>());
			
			for ( HospitalProp hp : propList ) {
				PropInfoItem propInfoItem = new PropInfoItem();
				propInfoItem.setProp(prop);
				propInfoItem.setDescription(prop.getDescription());
				propInfoItem.setPropItemId(hp.getId());
				propInfoItem.setHospCode(hp.getHospital().getHospCode());
				propInfoItem.setValue(hp.getValue());
				if ( propInfo.getType() == DataType.BooleanType ) {
					propInfoItem.setDisplayValue(getBooleanDisplayValue(hp.getValue()));
				} else {
					propInfoItem.setDisplayValue(hp.getValue());
				}
				propInfo.getPropInfoItemList().add(propInfoItem);
			}
		}
	}
	
	private void convertWorkstorePropListToPropInfoList(List<WorkstoreProp> workstorePropList) {
		
		Map<Prop, List<WorkstoreProp>> propMap = new HashMap<Prop, List<WorkstoreProp>>();
		for ( WorkstoreProp workstoreProp : workstorePropList ) {
			if ( !propMap.containsKey(workstoreProp.getProp()) ) {
				propMap.put(workstoreProp.getProp(), new ArrayList<WorkstoreProp>());
			} 
			propMap.get(workstoreProp.getProp()).add(workstoreProp);
		}
		
		for ( Map.Entry<Prop, List<WorkstoreProp>> entry : propMap.entrySet() ) {
			Prop prop = entry.getKey();
			List<WorkstoreProp> propList = entry.getValue();
			
			PropInfo propInfo = new PropInfo();
			propInfoList.add(propInfo);
			propInfo.setDescription(prop.getDescription());
			propInfo.setType(prop.getType());
			propInfo.setValidation(prop.getValidation());
			propInfo.setSortSeq(propList.get(0).getSortSeq());
			propInfo.setSupportMaintFlag(getSupportMaintFlag(prop.getMaintScreen()));
			propInfo.setPropInfoItemList(new ArrayList<PropInfoItem>());
			
			for ( WorkstoreProp wp : propList ) {
				PropInfoItem propInfoItem = new PropInfoItem();
				propInfoItem.setProp(prop);
				propInfoItem.setDescription(prop.getDescription());
				propInfoItem.setPropItemId(wp.getId());
				propInfoItem.setHospCode(wp.getWorkstore().getHospCode());
				propInfoItem.setWorkstoreCode(wp.getWorkstore().getWorkstoreCode());
				propInfoItem.setValue(wp.getValue());
				if ( propInfo.getType() == DataType.BooleanType ) {
					propInfoItem.setDisplayValue(getBooleanDisplayValue(wp.getValue()));
				} else {
					propInfoItem.setDisplayValue(wp.getValue());
				}
				propInfo.getPropInfoItemList().add(propInfoItem);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void sortPropInfoList(List<PropInfo> propInfoList){
		Collections.sort(propInfoList, new Comparator(){
			public int compare(Object o1, Object o2) {
				PropInfo item1 = (PropInfo) o1;
				PropInfo item2 = (PropInfo) o2;
				return item1.getDescription().compareTo(item2.getDescription());
			}
		});	
	}
	
	@SuppressWarnings("unchecked")
	private void sortPropInfoItemByHospCode(List<PropInfoItem> itemList) {
		Collections.sort(itemList, new Comparator(){
			public int compare(Object o1, Object o2) {
				PropInfoItem item1 = (PropInfoItem) o1;
				PropInfoItem item2 = (PropInfoItem) o2;
				return item1.getHospCode().compareTo(item2.getHospCode());
			}
		});	
	}
	
	@SuppressWarnings("unchecked")
	private void sortPropInfoItemByWorkstoreCode(List<PropInfoItem> itemList) {
		Collections.sort(itemList, new Comparator(){
			public int compare(Object o1, Object o2) {
				PropInfoItem item1 = (PropInfoItem) o1;
				PropInfoItem item2 = (PropInfoItem) o2;
				return item1.getWorkstoreCode().compareTo(item2.getWorkstoreCode());
			}
		});	
	}
	
	private String getBooleanDisplayValue(String dataValue) {
		return YesNoFlag.dataValueOf(dataValue).getDisplayValue();
	}
	
	private boolean getSupportMaintFlag(MaintScreen maintScreen) {
		return maintScreen == MaintScreen.Support;
	}
	
	public void updatePropList(List<PropInfoItem> propInfoItemList) {
		
		if ( !propInfoItemList.isEmpty() ) {
			switch (propType) {
				case CorporateProp:
					updateClusterCorpPropList(propInfoItemList);
					break;
				case HospitalProp:
					updateHospitalPropList(propInfoItemList);
					break;
				case WorkstoreProp:
					updateWorkstorePropList(propInfoItemList);
					break;
				case OperationProp:
					updateOperationPropList(propInfoItemList);
					break;
			}
		}
		retrievePropList(propType, hospital, operationProfileId);
	}
	
	private void updateClusterCorpPropList(List<PropInfoItem> inCorpPropInfoList) {
		List<CorporateProp> corporatePropList = new ArrayList<CorporateProp>();
		List<CorporateProp> syncCorporatePropList = new ArrayList<CorporateProp>();
		
		if ( !inCorpPropInfoList.isEmpty() ) {
			for ( PropInfoItem corpPropInfo : inCorpPropInfoList ) {
				CorporateProp corpProp = em.find(CorporateProp.class, corpPropInfo.getPropItemId());
				corpProp.setValue(corpPropInfo.getValue());
				corporatePropList.add(corpProp);
				if ( corpProp.getProp().getSyncFlag() ) {
					syncCorporatePropList.add(corpProp);
				}
			}
		}
		
		if ( !corporatePropList.isEmpty() ) {
			propManager.updateCorporatePropList(corporatePropList);
		}
		
		if ( !syncCorporatePropList.isEmpty() ) {
			corpPmsServiceProxy.updateCorporatePropByPropMaint(syncCorporatePropList);
		}
			
	}
	
	private void updateOperationPropList(List<PropInfoItem> inOperationPropInfoList) {
		List<OperationProp> operationPropList = new ArrayList<OperationProp>();
		
		if ( !inOperationPropInfoList.isEmpty() ) {
			for ( PropInfoItem operationPropInfo : inOperationPropInfoList ) {
				OperationProp operationProp = em.find(OperationProp.class, operationPropInfo.getPropItemId());
				operationProp.setValue(operationPropInfo.getValue());
				operationPropList.add(operationProp);
			}
		}
		
		if ( !operationPropList.isEmpty() ) {
			propManager.updateOperationPropList(operationPropList);
		}
	}
	
	private void updateHospitalPropList(List<PropInfoItem> propInfoItemList) {
		List<HospitalProp> hospitalPropList = new ArrayList<HospitalProp>();
		List<HospitalProp> syncHospitalPropList = new ArrayList<HospitalProp>();
		
		for ( PropInfoItem propInfoItem : propInfoItemList ) {
			HospitalProp hospitalProp = em.find(HospitalProp.class, propInfoItem.getPropItemId());
			hospitalProp.setValue(propInfoItem.getValue());
			hospitalPropList.add(hospitalProp);
			
			if ( hospitalProp.getProp().getSyncFlag() ) {
				syncHospitalPropList.add(hospitalProp);
			}
		}
		
		if ( !hospitalPropList.isEmpty() ) {
			propManager.updateHospitalPropList(hospitalPropList);
		}
		
		if ( !syncHospitalPropList.isEmpty() ) {
			corpPmsServiceProxy.updateHospitalProp(syncHospitalPropList);
		}
		
	}
	
	private void updateWorkstorePropList(List<PropInfoItem> propInfoItemList) {
		List<WorkstoreProp> workstorePropList = new ArrayList<WorkstoreProp>();
		List<WorkstoreProp> syncWorkstorePropList = new ArrayList<WorkstoreProp>();
		
		for ( PropInfoItem propInfoItem : propInfoItemList ) {
			WorkstoreProp workstoreProp = em.find(WorkstoreProp.class, propInfoItem.getPropItemId());
			workstoreProp.setValue(propInfoItem.getValue());
			workstorePropList.add(workstoreProp);
			if ( workstoreProp.getProp().getSyncFlag() ) {
				syncWorkstorePropList.add(workstoreProp);
			}
		}
		
		if ( !workstorePropList.isEmpty() ) {
			propManager.updateWorkstorePropList(workstorePropList);
		}
		
		if ( !syncWorkstorePropList.isEmpty() ) {
			corpPmsServiceProxy.updateWorkstoreProp(workstorePropList, false);
		}
	}
	
	@Remove
	public void destroy() 
	{
		if ( propInfoList != null ) {
			propInfoList = null;
		}
		if ( propInfoItemList != null ) {
			propInfoItemList = null;
		}
	}
}
