package hk.org.ha.model.pms.biz;

import hk.org.ha.fmk.pms.remote.JmsServiceProxy;
import hk.org.ha.fmk.pms.security.Uam;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.medprofile.MedProfileOrderManagerLocal;
import hk.org.ha.model.pms.biz.reftable.FollowUpWarningManagerLocal;
import hk.org.ha.model.pms.biz.reftable.HospitalClusterTaskManagerLocal;
import hk.org.ha.model.pms.biz.report.WaitTimeRptStatManagerLocal;
import hk.org.ha.model.pms.cache.PasSpecialtyCacherInf;
import hk.org.ha.model.pms.corp.cache.PasWardCacherInf;
import hk.org.ha.model.pms.cache.PropCacherLocal;
import hk.org.ha.model.pms.cache.WardStockCacherInf;
import hk.org.ha.model.pms.cache.DrugSiteMappingCacherInf;
import hk.org.ha.model.pms.corp.batch.worker.reftable.moe.ActiveWardHelper;
import hk.org.ha.model.pms.corp.batch.worker.reftable.moe.WardAdminFreqHelper;
import hk.org.ha.model.pms.corp.batch.worker.reftable.phs.PhsDataHelper;
import hk.org.ha.model.pms.corp.cache.CacherHelper;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.corp.vo.medprofile.SignatureCountResult;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.corp.ActiveWard;
import hk.org.ha.model.pms.persistence.corp.WardAdminFreq;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.phs.CapdSupplierItem;
import hk.org.ha.model.pms.persistence.phs.PatientCat;
import hk.org.ha.model.pms.persistence.phs.Specialty;
import hk.org.ha.model.pms.persistence.phs.Ward;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.reftable.OperationWorkstationType;
import hk.org.ha.model.pms.vo.reftable.PmsSessionInfo;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpSubscriberJmsRemote;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.contexts.Context;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.core.Events;
import org.jboss.seam.log.Log;
import org.jboss.seam.security.Identity;
import org.jboss.seam.web.Session;

@AutoCreate
@Stateless
@Name("pmsSubscriber")
@MeasureCalls
public class PmsSubscriberBean implements PmsSubscriberLocal {

	public static final String UPDATE_FOLLOW_UP_WARNING = "updateFollowUpWarning";	
	public static final String UPDATE_PATIENT_CAT = "updatePatientCat";
	public static final String UPDATE_WARD = "updateWard";	
	public static final String UPDATE_SPECIALTY = "updateSpecialty";	
	public static final String UPDATE_CAPD_SUPPLIER_ITEM = "updateCapdSupplierItem";
	public static final String UPDATE_WARD_ADMIN_FREQ = "updateWardAdminFreq";
	public static final String UPDATE_ACTIVE_WARD = "updateActiveWard";
	public static final String CHECK_SIGN_ORDER = "checkSignOrder";
	
	@PersistenceContext
	private EntityManager em;
	
	@Logger
	private Log logger;
	
	@In
	private HospitalClusterTaskManagerLocal hospitalClusterTaskManager;
	
	@In
	private FollowUpWarningManagerLocal followUpWarningManager;
		
	@In
	private MedProfileOrderManagerLocal medProfileOrderManager;
	
	@In
	private WaitTimeRptStatManagerLocal waitTimeRptStatManager;
	
	@In
	private PropCacherLocal propCacher;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private WardStockCacherInf wardStockCacher;
	
	@In
	private PasSpecialtyCacherInf pasSpecialtyCacher;
	
	@In
	private DrugSiteMappingCacherInf drugSiteMappingCacher;	

	@In
	private PasWardCacherInf pasWardCacher;
	
	@In
	private WardAdminFreqHelper wardAdminFreqHelper;
	
	@In
	private ActiveWardHelper activeWardHelper;
	
	@In
	private PhsDataHelper phsDataHelper;

	@In
	private CacherHelper cacherHelper;
	
	@In
	private CorpSubscriberJmsRemote corpSubscriberProxy;
	

	@In
	private Uam uam;
	
	private Random random = new Random();
	
	private int randomSleepTime = 3*60*1000;

	public int getRandomSleepTime() {
		return randomSleepTime;
	}

	public void setRandomSleepTime(int randomSleepTime) {
		this.randomSleepTime = randomSleepTime;
	}

	public void updateFollowUpWarningItemCode(List<String> itemCodeList, Date updateTime) {
		logger.debug("updateFollowUpWarningItemCode called");
		if (hospitalClusterTaskManager.lockHospitalClusterTask(UPDATE_FOLLOW_UP_WARNING, updateTime)) {
			followUpWarningManager.syncFollowUpWarning(itemCodeList);
		}
	}

	public void createWaitTimeRptStat(Date batchDate, String clusterCode, Date createTime) {
		logger.info("createWaitTimeRptStat batchDate:#0, clusterCode:#1, createTime:#2", batchDate, clusterCode, createTime);
		if (uam.getClusterCode().equals(clusterCode) || StringUtils.isBlank(clusterCode)) {
			if (hospitalClusterTaskManager.lockHospitalClusterTask(UPDATE_FOLLOW_UP_WARNING, createTime)) {
				logger.info("create waiting time report stat : accepted, clusterCode:#0", clusterCode);
				waitTimeRptStatManager.createWaitTimeRptStat(uam.getClusterCode(), batchDate);
				logger.info("createWaitTimeRptStat : time used #0", System.currentTimeMillis() - createTime.getTime());	
			}
		} else {
			logger.info("create waiting time report stat : ignored, clusterCode not matched (#0/#1)", uam.getClusterCode(), clusterCode);
		}		
	}
	
	public void initCache() {
		initCacheWithDmDrugList(null);
	}

	public void initCacheByCluster(String clusterCode) 
	{
		initCacheByClusterWithDmDrugList(clusterCode, null);
	}
	
	public void initCacheByClusterWithDmDrugList(String clusterCode, List<DmDrug> dmDrugList) 
	{
		if (uam.getClusterCode().equals(clusterCode)) {
			logger.info("initCache : accepted, clusterCode matched (#0)", uam.getClusterCode());
			this.initCacheWithDmDrugList(dmDrugList);
		} else {
			logger.info("initCache : ignored, clusterCode not matched (#0/#1)", uam.getClusterCode(), clusterCode);
		}
	}
		
	public void initCacheWithDmDrugList(List<DmDrug> dmDrugList) 
	{
		cacherHelper.clearDmInfo();
		wardStockCacher.clear();
		pasSpecialtyCacher.clear();
		pasWardCacher.clear();
		drugSiteMappingCacher.clear();
		
		if (dmDrugList == null) {
			
			dmDrugCacher.clear();
			
			int sleepTime = random.nextInt(randomSleepTime);		
			logger.info("initCache : start, random sleep for #0", sleepTime);		
	
			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
			}
			
			long startTime = System.currentTimeMillis();
	
			dmDrugCacher.getDmDrugList();
			logger.info("initCache : time used #0", System.currentTimeMillis() - startTime);		

		} else {
			
			logger.info("initCache : received dmDrugList.size:" + dmDrugList.size());
			dmDrugCacher.initDmDrugCache(dmDrugList);
			logger.info("initCache : done!");
		
		}
	}
	
	public void initJmsServiceProxyByCluster(String clusterCode) {
		if (uam.getClusterCode().equals(clusterCode)) {
			logger.info("initJmsServiceProxy : accepted, clusterCode matched (#0)", uam.getClusterCode());
			initJmsServiceProxy();
		} else {
			logger.info("initJmsServiceProxy : ignored, clusterCode not matched (#0/#1)", uam.getClusterCode(), clusterCode);
		}		
	}
	
	public void initJmsServiceProxy() {
		JmsServiceProxy.REINITING.get().set(true);
		try {
			Events.instance().raiseEvent(JmsServiceProxy.EVENT_REINIT_PROXY);
		} finally {
			JmsServiceProxy.REINITING.get().set(false);
		}
	}

	public void purgeMsWorkstoreDrug(Workstore workstore) {
	    	
		if (uam.getClusterCode().equals(workstore.getHospital().getHospitalCluster().getClusterCode()))
		{
			logger.info("purgeMsWorkstoreDrug : start hospital : #0, workstore : #1", workstore.getHospCode(), workstore.getWorkstoreCode());

			long startTime = System.currentTimeMillis();			
			dmDrugCacher.clearMsWorkstoreDrugCache(workstore);			
			dmDrugCacher.getMsWorkstoreDrugList(workstore);
			logger.info("purgeMsWorkstoreDrug : time used #0", System.currentTimeMillis() - startTime);		
		}
	}
	
	@SuppressWarnings("unchecked")
	public void invalidateSession(Workstore workstore) {
		
		propCacher.clearWorkstoreCache(workstore);
		
		Context applicationContext = Contexts.getApplicationContext();
		List<HttpSession> sessionList = (List<HttpSession>) 
			applicationContext.get("hk.org.ha.pms.web.sessionList");

		synchronized (sessionList) {
			for (HttpSession hs : sessionList) 
			{
				Workstation workstation = (Workstation) hs.getAttribute("workstation");
				if (workstation != null) {				
					if (workstore.equals(workstation.getWorkstore())) {
						
						propCacher.clearWorkstationCache(workstation);

						logger.info("invalidate http session for workstation #0(#1) under #2/#3", 
								workstation.getWorkstationCode(),
								workstation.getHostName(),
								workstation.getWorkstore().getHospCode(),
								workstation.getWorkstore().getWorkstoreCode());
						
						((Identity) hs.getAttribute("org.jboss.seam.security.identity")).unAuthenticate();		
						((Session) hs.getAttribute("org.jboss.seam.web.session")).invalidate();						
					}
				}
			}
		}
	}	
	
	public void updatePatientCatForPms(List<PatientCat> patientCatList, Date updateTime){
		logger.debug("updatePatientCatForPms called");
		if (hospitalClusterTaskManager.lockHospitalClusterTask(UPDATE_PATIENT_CAT, updateTime)) {
			phsDataHelper.updatePatientCatForPms(em, patientCatList);
		}
	}

	public void updateWardForPms(List<Ward> wardList, Date updateTime){
		logger.debug("updateWardForPms called");
		if (hospitalClusterTaskManager.lockHospitalClusterTask(UPDATE_WARD, updateTime)) {
			phsDataHelper.updateWardForPms(em, wardList);
		}
	}

	public void updateSpecialtyForPms(List<Specialty> specialtyList, Date updateTime){
		logger.debug("updateSpecialtyForPms called");
		if (hospitalClusterTaskManager.lockHospitalClusterTask(UPDATE_SPECIALTY, updateTime)) {
			phsDataHelper.updateSpecialtyForPms(em, specialtyList);
		}
	}
	
	public void updateCapdSupplierItemForPms(List<CapdSupplierItem> capdSupplierItemList, Date updateTime){
		logger.debug("updateCapdSupplierItemForPms called");
		if (hospitalClusterTaskManager.lockHospitalClusterTask(UPDATE_CAPD_SUPPLIER_ITEM, updateTime)) {
			phsDataHelper.updateCapdSupplierItemForPms(em, capdSupplierItemList);
		}
	}
	
	public void updateWardAdminFreqForPms(List<WardAdminFreq> wardAdminFreqList, List<String> errorPatHospCodeList, Date updateTime){
		logger.debug("updateWardAdminFreqForPms(List<WardAdminFreq> wardAdminFreqList, List<String> errorPatHospCodeList, Date updateTime) called");
		if (hospitalClusterTaskManager.lockHospitalClusterTask(UPDATE_WARD_ADMIN_FREQ, updateTime)) {
			wardAdminFreqHelper.updateWardAdminFreqForPms(em, wardAdminFreqList, errorPatHospCodeList);
		}
	}
	
	public void updateActiveWardForPms(List<ActiveWard> activeWardList, List<String> errorPatHospCodeList, Date updateTime){
		logger.debug("updateActiveWardForPms(List<ActiveWard> activeWardList, List<String> errorPatHospCodeList, Date updateTime) called");
		if (hospitalClusterTaskManager.lockHospitalClusterTask(UPDATE_ACTIVE_WARD, updateTime)) {
			activeWardHelper.updateActiveWardForPms(em, activeWardList, errorPatHospCodeList);
		}
	}
	
	public void updateWardAdminFreqForPms(List<WardAdminFreq> wardAdminFreqList, Date updateTime){
		logger.debug("updateWardAdminFreqForPms called");
		if (hospitalClusterTaskManager.lockHospitalClusterTask(UPDATE_WARD_ADMIN_FREQ, updateTime)) {
			wardAdminFreqHelper.updateWardAdminFreqForPms(em, wardAdminFreqList);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void requestPmsSessionInfo(String requestId, Workstore workstore) {
		List<PmsSessionInfo> pmsSessionInfoList = new ArrayList<PmsSessionInfo>();
		
		Context applicationContext = Contexts.getApplicationContext();
		List<HttpSession> sessionList = (List<HttpSession>) 
			applicationContext.get("hk.org.ha.pms.web.sessionList");

		synchronized (sessionList) {
			for (HttpSession hs : sessionList) 
			{
				UamInfo uamInfo = (UamInfo) hs.getAttribute("uamInfo");
				Workstation workstation = (Workstation) hs.getAttribute("workstation");
				OperationWorkstationType operationWorkstationType = (OperationWorkstationType) hs.getAttribute("operationWorkstationType");
				if (workstation != null && uamInfo != null && operationWorkstationType != null) {		
					if (workstore.equals(workstation.getWorkstore())) {
						PmsSessionInfo pmsSessionInfo = new PmsSessionInfo();
						pmsSessionInfo.setHostName(workstation.getHostName());
						pmsSessionInfo.setWorkstationCode(workstation.getWorkstationCode());
						pmsSessionInfo.setUserId(uamInfo.getUserId());
						pmsSessionInfo.setWorkstationType(operationWorkstationType);
						pmsSessionInfoList.add(pmsSessionInfo);
					}
				}
			}
		}
		
		corpSubscriberProxy.responsePmsSessionInfo(requestId, pmsSessionInfoList);
	}
	
	public void requestPmsSignatureCountResult(String requestId, Date batchDate, Date updateTime) {
		if (hospitalClusterTaskManager.lockHospitalClusterTask(CHECK_SIGN_ORDER, updateTime)) {
			List<SignatureCountResult> signatureCountResultList = medProfileOrderManager.retrieveSignatureCountResult(batchDate);		
			corpSubscriberProxy.responsePmsSignatureCountResult(requestId, signatureCountResultList);
		}
	}
	
}
