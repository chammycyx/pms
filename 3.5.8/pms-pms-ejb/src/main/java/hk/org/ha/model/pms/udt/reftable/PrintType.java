package hk.org.ha.model.pms.udt.reftable;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PrintType implements StringValuedEnum {

	None("-", "None"),
	Printer("P", "Printer"),
	Redirect("R", "Redirect");

    private final String dataValue;
    private final String displayValue;

    PrintType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }
    
    public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}
	
	public static PrintType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PrintType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<PrintType> {

		private static final long serialVersionUID = 1L;

		public Class<PrintType> getEnumClass() {
			return PrintType.class;
		}
		
	}
}
