package hk.org.ha.model.pms.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum YesNoCancelFlag implements StringValuedEnum {

	Yes("Y", "Yes"),
	No("N", "No"),
	Cancel("C", "Cancel");
	
    private final String dataValue;
    private final String displayValue;

    YesNoCancelFlag(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    } 
    
    public static YesNoCancelFlag dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(YesNoCancelFlag.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<YesNoCancelFlag> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<YesNoCancelFlag> getEnumClass() {
    		return YesNoCancelFlag.class;
    	}
    }
}



