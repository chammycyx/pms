package hk.org.ha.model.pms.udt.drug;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DrugSearchSource implements StringValuedEnum {
	// note: generation of granite for the enum is disabled
	//       after modifying the enum, need to modify DrugSearchSource.as manually

	Vetting("V", "Vetting", Boolean.FALSE, Boolean.FALSE, Boolean.TRUE),
	MpVetting("MV", "In-patient Vetting", Boolean.FALSE, Boolean.FALSE, Boolean.TRUE),
	OrderEdit("O", "Order Edit", Boolean.FALSE, Boolean.FALSE, Boolean.TRUE),
	OpDosageConversion("OC", "OP Dosage Conversion Maintenance", Boolean.TRUE, Boolean.TRUE, Boolean.TRUE),
	MpDosageConversion("MC", "MP Dosage Conversion Maintenance", Boolean.TRUE, Boolean.TRUE, Boolean.FALSE),
	MultiDoseConversion("MDC", "Multiple Dose Conversion Maintenance", Boolean.TRUE, Boolean.TRUE, Boolean.FALSE),
	CommonOrderStatus("COS", "Common Order Status Maintenance", Boolean.TRUE, Boolean.FALSE, Boolean.FALSE),	//Conversion?
	FixedConcnActivation("FC", "Fixed Concentration Preparations Activation Maintenance", Boolean.TRUE, Boolean.FALSE, Boolean.FALSE),	
	PivasDrug("PD", "Pivas Drug Maintenance", Boolean.TRUE, Boolean.FALSE, Boolean.FALSE);	
	
    private final String dataValue;
    private final String displayValue;
	private final Boolean maintenance;
	private final Boolean dosageConversion;
	private final Boolean enableCommonDosage;
        
    DrugSearchSource(final String dataValue, final String displayValue, final Boolean maintenance, final Boolean dosageConversion, final Boolean enableCommonDosage){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
        this.maintenance = maintenance;
        this.dosageConversion = dosageConversion;
        this.enableCommonDosage = enableCommonDosage;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   

    public Boolean getMaintenance() {
        return this.maintenance;
    }   
    
    public Boolean getDosageConversion() {
        return this.dosageConversion;
    }   

    public Boolean getEnableCommonDosage() {
        return this.enableCommonDosage;
    }   
    
    public static DrugSearchSource dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DrugSearchSource.class, dataValue);
	}
        
	public static class Converter extends StringValuedEnumConverter<DrugSearchSource> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<DrugSearchSource> getEnumClass() {
    		return DrugSearchSource.class;
    	}
    }
}
