package hk.org.ha.model.pms.persistence.medprofile;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.vo.vetting.mp.MpPharmInfo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "PURCHASE_REQUEST")
public class PurchaseRequest extends VersionEntity{

	private static final long serialVersionUID = 1L;
	
	private static final String JAXB_CONTEXT_DISP = "hk.org.ha.model.pms.persistence.disp";

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "purchaseRequestSeq")
	@SequenceGenerator(name = "purchaseRequestSeq", sequenceName = "SQ_PURCHASE_REQUEST", initialValue = 100000000)
	private Long id;
	
	@Column(name = "ISSUE_QTY", nullable = false, precision = 19, scale = 4)
	private BigDecimal issueQty;

	@Column(name = "DIRECT_PRINT_FLAG", nullable = false)
	private Boolean directPrintFlag;
	
	@Column(name = "INVOICE_DATE")
	@Temporal(TemporalType.DATE)
	private Date invoiceDate;
	
	@Column(name = "INVOICE_NUM", length = 15)
	private String invoiceNum;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DUE_DATE")
	private Date dueDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "PRINT_DATE")
	private Date printDate;
	
	@Column(name = "TOTAL_AMOUNT", precision = 19, scale = 0)
	private BigDecimal totalAmount;
	
	@Column(name = "RECEIVE_AMOUNT", precision = 19, scale = 0)
	private BigDecimal receiveAmount;
	
	@Converter(name = "PurchaseRequest.invoiceStatus", converterClass = InvoiceStatus.Converter.class)
    @Convert("PurchaseRequest.invoiceStatus")
	@Column(name="INVOICE_STATUS", length = 1)
	private InvoiceStatus invoiceStatus;
	
	@Column(name = "FORCE_PROCEED_FLAG", nullable = false)
	private Boolean forceProceedFlag;
	
	@Column(name = "FORCE_PROCEED_CODE", length = 12)
	private String forceProceedCode;
	
	@Column(name = "FORCE_PROCEED_USER", length = 12)
	private String forceProceedUser;
	
	@Column(name = "FORCE_PROCEED_DATE")
	@Temporal(TemporalType.DATE)
	private Date forceProceedDate;

	@Column(name = "RECEIPT_NUM", length = 20)
	private String receiptNum;
	
	@Column(name = "MANUAL_INVOICE_NUM", length = 15)
	private String manualInvoiceNum;
	
	@Column(name = "MANUAL_RECEIPT_NUM", length = 20)
	private String manualReceiptNum;

	@Column(name = "FCS_CHECK_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fcsCheckDate;
	
	@Column(name = "DURATION_IN_DAY", length = 4)
	private Integer durationInDay;
	
	@Column(name = "DOSE_QTY", precision = 19, scale = 4)
	private BigDecimal doseQty;

	@Column(name = "REQUEST_BY", length = 100)
	private String requestBy;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MED_PROFILE_PO_ITEM_ID", nullable = false)
	private MedProfilePoItem medProfilePoItem;
	
	@Column(name = "MED_PROFILE_PO_ITEM_ID", nullable = false, insertable = false, updatable = false)
	private Long medProfilePoItemId;	
    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MED_PROFILE_ID", nullable = false)
	private MedProfile medProfile;
	
	@Column(name = "MED_PROFILE_ID", nullable = false, insertable = false, updatable = false)
	private Long medProfileId;	
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE",      referencedColumnName = "HOSP_CODE",      nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;
	
	@Column(name = "HOSP_CODE", nullable = false, insertable = false, updatable = false)
	private String hospCode;
	
	@Column(name = "LIFESTYLE_FLAG", nullable = false)
	private Boolean lifestyleFlag;
	
	@Column(name = "ONCOLOGY_FLAG", nullable = false)
	private Boolean oncologyFlag;
	
	@Column(name = "PAS_PAY_CODE", length = 3)
	private String pasPayCode;
	
	@Column(name = "PAS_PAT_GROUP_CODE", length = 5)
	private String pasPatGroupCode;
	
    @Lob
    @Column(name = "INVOICE_XML")
    private String invoiceXml;
    
    @OneToMany(mappedBy = "purchaseRequest")
    private List<DeliveryItem> deliveryItemList;
    
	@Transient
	private Integer mpiOrgItemNum;
	
	@Transient
	private Date sentDate;
	
	@Transient
	private String itemStatus;
	
	@Transient
	private Boolean markDelete;
	
	@Transient
	private MpPharmInfo mpPharmInfo;
	
	@Transient
	private Long replenishmentOrgId;
	
	@Transient
	private String fmStatus;
	
	@Transient
	private Invoice invoice;
	
	private transient String ChargeAmount;
	private transient String receiptMessage;
	
	@PostLoad
	public void postLoad() {
		if(invoiceXml!=null)
		{
			JaxbWrapper<Invoice> jaxpWrapper = JaxbWrapper.instance(JAXB_CONTEXT_DISP);
			invoice = (Invoice)jaxpWrapper.unmarshall(invoiceXml);
		}
	}
	
	@PrePersist
    @PreUpdate
	public void preSave() {
		if(invoice!=null)
		{
			JaxbWrapper<Invoice> jaxpWrapper = JaxbWrapper.instance(JAXB_CONTEXT_DISP);
			invoiceXml = jaxpWrapper.marshall(invoice);
		}
	}
	
	public PurchaseRequest() {
		forceProceedFlag = Boolean.FALSE;
		directPrintFlag = Boolean.FALSE;
		setLifestyleFlag(Boolean.FALSE);
		setOncologyFlag(Boolean.FALSE);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getInvoiceNum() {
		return invoiceNum;
	}

	public void setInvoiceNum(String invoiceNum) {
		this.invoiceNum = invoiceNum;
	}

	public BigDecimal getIssueQty() {
		return issueQty;
	}

	public void setIssueQty(BigDecimal issueQty) {
		this.issueQty = issueQty;
	}

	public Boolean getDirectPrintFlag() {
		return directPrintFlag;
	}
	
	public void setDirectPrintFlag(Boolean directPrintFlag) {
		this.directPrintFlag = directPrintFlag;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Date getPrintDate() {
		return printDate;
	}
	
	public void setPrintDate(Date printDate) {
		this.printDate = printDate;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getReceiveAmount() {
		return receiveAmount;
	}

	public void setReceiveAmount(BigDecimal receiveAmount) {
		this.receiveAmount = receiveAmount;
	}

	public InvoiceStatus getInvoiceStatus() {
		return invoiceStatus;
	}

	public void setInvoiceStatus(InvoiceStatus invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public Boolean getForceProceedFlag() {
		return forceProceedFlag;
	}

	public void setForceProceedFlag(Boolean forceProceedFlag) {
		this.forceProceedFlag = forceProceedFlag;
	}

	public String getForceProceedCode() {
		return forceProceedCode;
	}

	public void setForceProceedCode(String forceProceedCode) {
		this.forceProceedCode = forceProceedCode;
	}

	public String getForceProceedUser() {
		return forceProceedUser;
	}

	public void setForceProceedUser(String forceProceedUser) {
		this.forceProceedUser = forceProceedUser;
	}

	public Date getForceProceedDate() {
		if (forceProceedDate == null) {
			return null;
		}
		else {
			return new Date(forceProceedDate.getTime());
		}
	}

	public void setForceProceedDate(Date forceProceedDate) {
		if (forceProceedDate == null) {
			this.forceProceedDate = null;
		}
		else {
			this.forceProceedDate = new Date(forceProceedDate.getTime());
		}
	}

	public String getReceiptNum() {
		return receiptNum;
	}

	public void setReceiptNum(String receiptNum) {
		this.receiptNum = receiptNum;
	}

	public String getManualInvoiceNum() {
		return manualInvoiceNum;
	}

	public void setManualInvoiceNum(String manualInvoiceNum) {
		this.manualInvoiceNum = manualInvoiceNum;
	}

	public String getManualReceiptNum() {
		return manualReceiptNum;
	}

	public void setManualReceiptNum(String manualReceiptNum) {
		this.manualReceiptNum = manualReceiptNum;
	}

	public Date getFcsCheckDate() {
		if (fcsCheckDate == null) {
			return null;
		}
		else {
			return new Date(fcsCheckDate.getTime());
		}
	}

	public void setFcsCheckDate(Date fcsCheckDate) {
		if (fcsCheckDate == null) {
			this.fcsCheckDate = null;
		}
		else {
			this.fcsCheckDate = new Date(fcsCheckDate.getTime());
		}
	}

	public Integer getDurationInDay() {
		return durationInDay;
	}

	public void setDurationInDay(Integer durationInDay) {
		this.durationInDay = durationInDay;
	}

	public BigDecimal getDoseQty() {
		return doseQty;
	}

	public void setDoseQty(BigDecimal doseQty) {
		this.doseQty = doseQty;
	}

	public String getRequestBy() {
		return requestBy;
	}

	public void setRequestBy(String requestBy) {
		this.requestBy = requestBy;
	}

	public MedProfilePoItem getMedProfilePoItem() {
		return medProfilePoItem;
	}

	public void setMedProfilePoItem(MedProfilePoItem medProfilePoItem) {
		this.medProfilePoItem = medProfilePoItem;
	}
	
	public Long getMedProfilePoItemId() {
		return medProfilePoItemId;
	}

	public void setMedProfilePoItemId(Long medProfilePoItemId) {
		this.medProfilePoItemId = medProfilePoItemId;
	}

	public MedProfile getMedProfile() {
		return medProfile;
	}

	public void setMedProfile(MedProfile medProfile) {
		this.medProfile = medProfile;
	}
	
	public Long getMedProfileId() {
		return medProfileId;
	}

	public void setMedProfileId(Long medProfileId) {
		this.medProfileId = medProfileId;
	}

	public void setMarkDelete(Boolean markDelete) {
		this.markDelete = markDelete;
	}

	public Boolean getMarkDelete() {
		return markDelete;
	}
	
	@Transient
	public boolean clearanceCheckRequired() {
		return getInvoiceStatus() == InvoiceStatus.Outstanding && !Boolean.TRUE.equals(getForceProceedFlag());
	}

	public void setDeliveryItemList(List<DeliveryItem> deliveryItemList) {
		this.deliveryItemList = deliveryItemList;
	}

	public List<DeliveryItem> getDeliveryItemList() {
		return deliveryItemList;
	}
		
	public void loadDeliveryInfo(){
		this.mpiOrgItemNum = this.getMedProfilePoItem().getMedProfileMoItem().getMedProfileItem().getOrgItemNum();
		
		if( !getDeliveryItemList().isEmpty() ){
			this.itemStatus = getDeliveryItemList().get(0).getStatus().getDisplayValue();
			this.sentDate =  getDeliveryItemList().get(0).getDelivery().getDeliveryDate();
		}else{
			this.itemStatus = this.getMedProfilePoItem().getMedProfileMoItem().getMedProfileItem().getStatus().getDisplayValue();
		}
	}

	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}
	
	public Date getSentDate(){
		if( sentDate == null ){
			return null;
		}
		return sentDate;
	}
	
	public void setItemStatus(String itemStatus) {
		this.itemStatus = itemStatus;
	}
	
	public String getItemStatus(){
		return itemStatus;
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}
	
	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}
	
	public void setLifestyleFlag(Boolean lifestyleFlag) {
		this.lifestyleFlag = lifestyleFlag;
	}

	public Boolean getLifestyleFlag() {
		return lifestyleFlag;
	}

	public void setOncologyFlag(Boolean oncologyFlag) {
		this.oncologyFlag = oncologyFlag;
	}

	public Boolean getOncologyFlag() {
		return oncologyFlag;
	}

	public void setPasPayCode(String pasPayCode) {
		this.pasPayCode = pasPayCode;
	}

	public String getPasPayCode() {
		return pasPayCode;
	}

	public void setPasPatGroupCode(String pasPatGroupCode) {
		this.pasPatGroupCode = pasPatGroupCode;
	}

	public String getPasPatGroupCode() {
		return pasPatGroupCode;
	}

	public String getInvoiceXml() {
		return invoiceXml;
	}

	public void setInvoiceXml(String invoiceXml) {
		this.invoiceXml = invoiceXml;
	}

	public void setChargeAmount(String chargeAmount) {
		ChargeAmount = chargeAmount;
	}

	public String getChargeAmount() {
		return ChargeAmount;
	}

	public void setReceiptMessage(String receiptMessage) {
		this.receiptMessage = receiptMessage;
	}

	public String getReceiptMessage() {
		return receiptMessage;
	}

	public void setMpPharmInfo(MpPharmInfo mpPharmInfo) {
		this.mpPharmInfo = mpPharmInfo;
	}

	public MpPharmInfo getMpPharmInfo() {
		return mpPharmInfo;
	}
	
	public void loadMpPharmInfo(){
		MpPharmInfo newMpPharmInfo = new MpPharmInfo();
		
		if( !getDeliveryItemList().isEmpty() ){
			this.itemStatus = getDeliveryItemList().get(0).getStatus().getDisplayValue();
			this.sentDate =  getDeliveryItemList().get(0).getDelivery().getDeliveryDate();
			newMpPharmInfo.setDeliveryItemStatus(getDeliveryItemList().get(0).getStatus());
		}
		
		if( this.getMedProfilePoItem() != null ){
			MedProfilePoItem medProfilePoItem = this.getMedProfilePoItem();			
			newMpPharmInfo.setItemCode(medProfilePoItem.getItemCode());
			newMpPharmInfo.setBaseUnit(medProfilePoItem.getBaseUnit());			
			newMpPharmInfo.setDrugName(medProfilePoItem.getDrugName());
			newMpPharmInfo.setFormLabelDesc(medProfilePoItem.getFormLabelDesc());
			newMpPharmInfo.setStrength(medProfilePoItem.getStrength());
			newMpPharmInfo.setVolumeText(medProfilePoItem.getVolumeText());
			newMpPharmInfo.setMedProfilePoItemId(medProfilePoItemId);
			newMpPharmInfo.setMedProfilePoItemUuid(medProfilePoItem.getUuid());
			newMpPharmInfo.setMedProfileMoItemId(medProfilePoItem.getMedProfileMoItem().getId());
			newMpPharmInfo.setMedProfileItemId(medProfilePoItem.getMedProfileMoItem().getMedProfileItem().getId());
			
			DmDrug dmDrug = DmDrugCacher.instance().getDmDrug(medProfilePoItem.getItemCode());
			if( dmDrug != null && dmDrug.getDrugChargeInfo() != null ){
				newMpPharmInfo.setLifestyleFlag(dmDrug.getDrugChargeInfo().getLifestyleDrugIndicator());
				newMpPharmInfo.setOncologyFlag(dmDrug.getDrugChargeInfo().getOncologyDrugIndicator());
				if( FmStatus.SafetyNetItem.getDataValue().equals(medProfilePoItem.getFmStatus()) ){
					newMpPharmInfo.setSafetyNetFlag(true);
				}else{
					newMpPharmInfo.setSafetyNetFlag(false);
				}
			}else{
				newMpPharmInfo.setLifestyleFlag(false);
				newMpPharmInfo.setOncologyFlag(false);
			}
			
			this.mpiOrgItemNum = this.getMedProfilePoItem().getMedProfileMoItem().getMedProfileItem().getOrgItemNum();
			if( newMpPharmInfo.getDeliveryItemStatus() == null ){
				if( getMedProfilePoItem().getMedProfileMoItem().isManualItem() || getMedProfilePoItem().getMedProfileMoItem().getItemNum() == getMedProfilePoItem().getMedProfileMoItem().getMedProfileItem().getMedProfileMoItem().getItemNum() ){
					newMpPharmInfo.setMedProfileItemStatus(this.getMedProfilePoItem().getMedProfileMoItem().getMedProfileItem().getStatus());
					this.itemStatus = this.getMedProfilePoItem().getMedProfileMoItem().getMedProfileItem().getStatus().getDisplayValue();
				}else{
					newMpPharmInfo.setMedProfileItemStatus(null);
					this.itemStatus = "Modified";
				}
			}
			this.fmStatus = this.getMedProfilePoItem().getFmStatus();
			this.setMedProfilePoItem(null);
			this.setMedProfile(null);
		}
		this.mpPharmInfo = newMpPharmInfo;
	}

	public Long getReplenishmentOrgId() {
		return replenishmentOrgId;
	}

	public void setReplenishmentOrgId(Long replenishmentOrgId) {
		this.replenishmentOrgId = replenishmentOrgId;
	}

	public void setMpiOrgItemNum(Integer mpiOrgItemNum) {
		this.mpiOrgItemNum = mpiOrgItemNum;
	}

	public Integer getMpiOrgItemNum() {
		return mpiOrgItemNum;
	}
	
	public boolean isActiveOutstandingPurchaseRequest(){
		if( this.printDate != null ){
			return false;
		}
		if( this.invoiceStatus == InvoiceStatus.Void || this.invoiceStatus == InvoiceStatus.SysDeleted ){
			return false;
		}
		return true;
	}

	public String getFmStatus() {
		return fmStatus;
	}

	public void setFmStatus(String fmStatus) {
		this.fmStatus = fmStatus;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}
}
