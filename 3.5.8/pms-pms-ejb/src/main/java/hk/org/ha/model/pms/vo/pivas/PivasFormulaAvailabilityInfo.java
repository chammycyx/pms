package hk.org.ha.model.pms.vo.pivas;

import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaExclWard;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasWard;

import java.io.Serializable;
import java.util.List;

public class PivasFormulaAvailabilityInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private List<PivasWard> pivasWardList;
	
	private List<PivasFormulaExclWard> pivasFormulaExclWardList;

	public List<PivasWard> getPivasWardList() {
		return pivasWardList;
	}

	public void setPivasWardList(List<PivasWard> pivasWardList) {
		this.pivasWardList = pivasWardList;
	}

	public List<PivasFormulaExclWard> getPivasFormulaExclWardList() {
		return pivasFormulaExclWardList;
	}

	public void setPivasFormulaExclWardList(
			List<PivasFormulaExclWard> pivasFormulaExclWardList) {
		this.pivasFormulaExclWardList = pivasFormulaExclWardList;
	}
}
