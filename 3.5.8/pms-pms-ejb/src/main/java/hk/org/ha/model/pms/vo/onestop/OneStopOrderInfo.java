package hk.org.ha.model.pms.vo.onestop;

import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.disp.PharmOrderPatType;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
import hk.org.ha.model.pms.vo.ehr.EhrPatient;
import hk.org.ha.model.pms.vo.prevet.PreVetInfo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class OneStopOrderInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private PharmOrderPatType patType;
	
	private String doctorCode;
	
	private String doctorName;
		
	private MedCase medCase;
	
	private String phsSpecCode;
	
	private String phsWardCode;
	
	private String phsPatCat;
	
	private MedOrderPrescType prescType;
	
	private OneStopOrderType oneStopOrderType;
	
	private String patHospCode;
	
	private Boolean standardForceProceed;
	
	private Boolean noAppointmentRequired;
	
	private String receiptNum;
	
	private Boolean privateFlag;
	
	private Boolean isOrderReadOnly;
	
	private Boolean preVetUrgentFlag;
	
	private List<PreVetInfo> preVetInfoList;
	
	private String preVetUser;
	
	private Date preVetDate;
	
	private String preVetUserName;
	
	private EhrPatient ehrPatient;
		
	public OneStopOrderInfo() {
		
	}
	
	public String getPhsSpecCode() {
		return phsSpecCode;
	}

	public void setPhsSpecCode(String phsSpecCode) {
		this.phsSpecCode = phsSpecCode;
	}

	public String getPhsWardCode() {
		return phsWardCode;
	}

	public void setPhsWardCode(String phsWardCode) {
		this.phsWardCode = phsWardCode;
	}

	public String getPhsPatCat() {
		return phsPatCat;
	}

	public void setPhsPatCat(String phsPatCat) {
		this.phsPatCat = phsPatCat;
	}

	public MedOrderPrescType getPrescType() {
		return prescType;
	}

	public void setPrescType(MedOrderPrescType prescType) {
		this.prescType = prescType;
	}

	public void setMedCase(MedCase medCase) {
		this.medCase = medCase;
	}

	public MedCase getMedCase() {
		return medCase;
	}

	public void setPatType(PharmOrderPatType patType) {
		this.patType = patType;
	}

	public PharmOrderPatType getPatType() {
		return patType;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public void setOneStopOrderType(OneStopOrderType oneStopOrderType) {
		this.oneStopOrderType = oneStopOrderType;
	}

	public OneStopOrderType getOneStopOrderType() {
		return oneStopOrderType;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setStandardForceProceed(Boolean standardForceProceed) {
		this.standardForceProceed = standardForceProceed;
	}

	public Boolean getStandardForceProceed() {
		return standardForceProceed;
	}

	public Boolean getNoAppointmentRequired() {
		return noAppointmentRequired;
	}

	public void setNoAppointmentRequired(Boolean noAppointmentRequired) {
		this.noAppointmentRequired = noAppointmentRequired;
	}

	public String getReceiptNum() {
		return receiptNum;
	}

	public void setReceiptNum(String receiptNum) {
		this.receiptNum = receiptNum;
	}

	public Boolean getPrivateFlag() {
		return privateFlag;
	}

	public void setPrivateFlag(Boolean privateFlag) {
		this.privateFlag = privateFlag;
	}

	public void setIsOrderReadOnly(Boolean isOrderReadOnly) {
		this.isOrderReadOnly = isOrderReadOnly;
	}

	public Boolean getIsOrderReadOnly() {
		return isOrderReadOnly;
	}
	
	public Boolean getPreVetUrgentFlag() {
		return preVetUrgentFlag;
	}

	public void setPreVetUrgentFlag(Boolean preVetUrgentFlag) {
		this.preVetUrgentFlag = preVetUrgentFlag;
	}

	public List<PreVetInfo> getPreVetInfoList() {
		return preVetInfoList;
	}

	public void setPreVetInfoList(List<PreVetInfo> preVetInfoList) {
		this.preVetInfoList = preVetInfoList;
	}
	
	public String getPreVetUser() {
		return preVetUser;
	}

	public void setPreVetUser(String preVetUser) {
		this.preVetUser = preVetUser;
	}

	public Date getPreVetDate() {
		return preVetDate;
	}

	public void setPreVetDate(Date preVetDate) {
		this.preVetDate = preVetDate;
	}

	public void setPreVetUserName(String preVetUserName) {
		this.preVetUserName = preVetUserName;
	}

	public String getPreVetUserName() {
		return preVetUserName;
	}

	public EhrPatient getEhrPatient() {
		return ehrPatient;
	}

	public void setEhrPatient(EhrPatient ehrPatient) {
		this.ehrPatient = ehrPatient;
	}
}