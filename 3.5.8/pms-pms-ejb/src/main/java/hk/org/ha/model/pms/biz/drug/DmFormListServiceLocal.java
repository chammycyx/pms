package hk.org.ha.model.pms.biz.drug;

import javax.ejb.Local;

@Local
public interface DmFormListServiceLocal {

	void retrieveDmFormList(String prefixFormCode);
	
	void destroy();
	
}
