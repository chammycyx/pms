package hk.org.ha.model.pms.persistence.reftable;

import java.util.List;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.disp.Patient;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Customizer;
import org.eclipse.persistence.annotations.PrivateOwned;

@Entity
@Table(name = "DRS_PATIENT")
@Customizer(AuditCustomizer.class)
public class DrsPatient extends VersionEntity {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "drsPatientSeq")
	@SequenceGenerator(name = "drsPatientSeq", sequenceName = "SQ_DRS_PATIENT", initialValue = 100000000)
	private Long id;
	
	@Column(name = "ENABLE_FLAG", nullable = false)
	private Boolean enableFlag;
	
	@Column(name = "DURATION_CHECK_FLAG", nullable = false)
	private Boolean durationCheckFlag;
	
	@Column(name = "ITEM_COUNT_CHECK_FLAG", nullable = false)
	private Boolean itemCountCheckFlag;
	
	@Column(name = "REMARK_TEXT", length = 1000)
	private String remarkText;
	
	@ManyToOne
	@JoinColumn(name = "HOSP_CODE", nullable = false)
	private Hospital hospital;

    @ManyToOne
    @JoinColumn(name = "PATIENT_ID")
    private Patient patient;
	
	@PrivateOwned
	@OrderBy("description,phone,phoneExt")
    @OneToMany(mappedBy="drsPatient", cascade=CascadeType.ALL)
    private List<DrsPatientContact> drsPatientContactList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getEnableFlag() {
		return enableFlag;
	}

	public void setEnableFlag(Boolean enableFlag) {
		this.enableFlag = enableFlag;
	}

	public Boolean getDurationCheckFlag() {
		return durationCheckFlag;
	}

	public void setDurationCheckFlag(Boolean durationCheckFlag) {
		this.durationCheckFlag = durationCheckFlag;
	}

	public Boolean getItemCountCheckFlag() {
		return itemCountCheckFlag;
	}

	public void setItemCountCheckFlag(Boolean itemCountCheckFlag) {
		this.itemCountCheckFlag = itemCountCheckFlag;
	}

	public String getRemarkText() {
		return remarkText;
	}

	public void setRemarkText(String remarkText) {
		this.remarkText = remarkText;
	}

	public Hospital getHospital() {
		return hospital;
	}

	public void setHospital(Hospital hospital) {
		this.hospital = hospital;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public List<DrsPatientContact> getDrsPatientContactList() {
		return drsPatientContactList;
	}

	public void setDrsPatientContactList(List<DrsPatientContact> drsPatientContactList) {
		this.drsPatientContactList = drsPatientContactList;
	}
}
