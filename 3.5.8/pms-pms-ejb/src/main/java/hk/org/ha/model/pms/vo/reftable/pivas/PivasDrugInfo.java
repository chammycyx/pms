package hk.org.ha.model.pms.vo.reftable.pivas;

import hk.org.ha.model.pms.dms.persistence.Company;
import hk.org.ha.model.pms.dms.persistence.DmDiluent;
import hk.org.ha.model.pms.dms.persistence.DmDrugReconstitution;
import hk.org.ha.model.pms.dms.persistence.DmSite;
import hk.org.ha.model.pms.dms.persistence.MsWorkstoreDrug;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrug;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PivasDrugInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private MsWorkstoreDrug msWorkstoreDrug;
	
	private PivasDrug pivasDrug;
	
	private List<Company> companyList;

	private List<DmSite> whiteDmSiteList;

	private List<DmSite> greyDmSiteList;

	private List<DmDrugReconstitution> dmDrugReconstitutionList;

	private List<DmDiluent> pivasSolventList;

	public MsWorkstoreDrug getMsWorkstoreDrug() {
		return msWorkstoreDrug;
	}

	public void setMsWorkstoreDrug(MsWorkstoreDrug msWorkstoreDrug) {
		this.msWorkstoreDrug = msWorkstoreDrug;
	}

	public PivasDrug getPivasDrug() {
		return pivasDrug;
	}

	public void setPivasDrug(PivasDrug pivasDrug) {
		this.pivasDrug = pivasDrug;
	}

	public List<Company> getCompanyList() {
		return companyList;
	}

	public void setCompanyList(List<Company> companyList) {
		this.companyList = companyList;
	}

	public List<DmSite> getWhiteDmSiteList() {
		return whiteDmSiteList;
	}

	public void setWhiteDmSiteList(List<DmSite> whiteDmSiteList) {
		this.whiteDmSiteList = whiteDmSiteList;
	}

	public List<DmSite> getGreyDmSiteList() {
		return greyDmSiteList;
	}

	public void setGreyDmSiteList(List<DmSite> greyDmSiteList) {
		this.greyDmSiteList = greyDmSiteList;
	}

	public List<DmDrugReconstitution> getDmDrugReconstitutionList() {
		return dmDrugReconstitutionList;
	}

	public void setDmDrugReconstitutionList(List<DmDrugReconstitution> dmDrugReconstitutionList) {
		this.dmDrugReconstitutionList = dmDrugReconstitutionList;
	}

	public List<DmDiluent> getPivasSolventList() {
		return pivasSolventList;
	}

	public void setPivasSolventList(List<DmDiluent> pivasSolventList) {
		this.pivasSolventList = pivasSolventList;
	}	
}