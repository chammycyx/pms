package hk.org.ha.model.pms.vo.alert.mds;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class DrugLog implements Serializable {

	private static final long serialVersionUID = 1L;

	private String rgenId;

	private String rdfgenId;

	private String gcnSeqNum;

	private String drugDdimDisplayName;

	private Boolean onHandFlag; 
	
	public DrugLog() {
		onHandFlag = Boolean.FALSE;
	}

	public DrugLog(Boolean onHandFlag) {
		this.onHandFlag = onHandFlag;
	}

	public String getRgenId() {
		return rgenId;
	}

	public void setRgenId(String rgenId) {
		this.rgenId = rgenId;
	}

	public String getRdfgenId() {
		return rdfgenId;
	}

	public void setRdfgenId(String rdfgenId) {
		this.rdfgenId = rdfgenId;
	}

	public String getGcnSeqNum() {
		return gcnSeqNum;
	}

	public void setGcnSeqNum(String gcnSeqNum) {
		this.gcnSeqNum = gcnSeqNum;
	}

	public String getDrugDdimDisplayName() {
		return drugDdimDisplayName;
	}

	public void setDrugDdimDisplayName(String drugDdimDisplayName) {
		this.drugDdimDisplayName = drugDdimDisplayName;
	}

	public Boolean getOnHandFlag() {
		return onHandFlag;
	}

	public void setOnHandFlag(Boolean onHandFlag) {
		this.onHandFlag = onHandFlag;
	}
}
