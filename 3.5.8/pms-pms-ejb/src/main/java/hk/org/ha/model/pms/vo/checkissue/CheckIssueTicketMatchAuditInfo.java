package hk.org.ha.model.pms.vo.checkissue;

import hk.org.ha.model.pms.udt.disp.TicketMatchDocType;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class CheckIssueTicketMatchAuditInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Date ticketDate;
	
	private String ticketNum;
	
	private Long dispOrderId;
	
	private TicketMatchDocType firstTicketMatchDocType;
	
	private TicketMatchDocType secondTicketMatchDocType;
	
	private String action;
	
	private String invalidMessageCode;
	
	private String firstTicketMatchBarcode;
	
	private String secondTicketMatchBarcode;
		
	public Date getTicketDate() {
		return ticketDate;
	}

	public void setTicketDate(Date ticketDate) {
		this.ticketDate = ticketDate;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public void setDispOrderId(Long dispOrderId) {
		this.dispOrderId = dispOrderId;
	}

	public Long getDispOrderId() {
		return dispOrderId;
	}

	public TicketMatchDocType getFirstTicketMatchDocType() {
		return firstTicketMatchDocType;
	}

	public void setFirstTicketMatchDocType(TicketMatchDocType firstTicketMatchDocType) {
		this.firstTicketMatchDocType = firstTicketMatchDocType;
	}

	public TicketMatchDocType getSecondTicketMatchDocType() {
		return secondTicketMatchDocType;
	}

	public void setSecondTicketMatchDocType(TicketMatchDocType secondTicketMatchDocType) {
		this.secondTicketMatchDocType = secondTicketMatchDocType;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getInvalidMessageCode() {
		return invalidMessageCode;
	}

	public void setInvalidMessageCode(String invalidMessage) {
		this.invalidMessageCode = invalidMessage;
	}

	public String getFirstTicketMatchBarcode() {
		return firstTicketMatchBarcode;
	}

	public void setFirstTicketMatchBarcode(String firstTicketMatchBarcode) {
		this.firstTicketMatchBarcode = firstTicketMatchBarcode;
	}

	public String getSecondTicketMatchBarcode() {
		return secondTicketMatchBarcode;
	}

	public void setSecondTicketMatchBarcode(String secondTicketMatchBarcode) {
		this.secondTicketMatchBarcode = secondTicketMatchBarcode;
	}	
}