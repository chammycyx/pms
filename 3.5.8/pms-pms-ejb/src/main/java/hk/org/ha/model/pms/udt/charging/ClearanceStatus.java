package hk.org.ha.model.pms.udt.charging;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum ClearanceStatus implements StringValuedEnum {
	PaymentClear("P","Payment Clear"),
	PaymentOutstanding("F","Payment Outstanding"),
	RecordNotFound("N","Record Not Found"),
	Error("E","Error");
	
    private final String dataValue;
    private final String displayValue;

    ClearanceStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    } 
    
    public static ClearanceStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(ClearanceStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<ClearanceStatus> {
		
		private static final long serialVersionUID = 1L;

		@Override
    	public Class<ClearanceStatus> getEnumClass() {
    		return ClearanceStatus.class;
    	}
    }
}



