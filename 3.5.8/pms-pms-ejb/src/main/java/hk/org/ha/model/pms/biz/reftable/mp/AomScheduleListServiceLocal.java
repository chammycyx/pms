package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.model.pms.persistence.reftable.AomSchedule;

import java.util.List;

import javax.ejb.Local;

@Local
public interface AomScheduleListServiceLocal {
	
	List<AomSchedule> retrieveAomScheduleList();
	
	List<AomSchedule> updateAomScheduleList(List<AomSchedule> updateAomScheduleList, List<AomSchedule> delAomScheduleList);
	
	void destroy();

}
