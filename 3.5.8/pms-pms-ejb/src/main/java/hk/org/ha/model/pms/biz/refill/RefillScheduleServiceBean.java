package hk.org.ha.model.pms.biz.refill;

import static hk.org.ha.model.pms.prop.Prop.SUSPEND_ORDER_SUSPENDCODE;
import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.alert.mds.AlertRetrieveManagerLocal;
import hk.org.ha.model.pms.biz.alert.mds.PrescCheckManagerLocal;
import hk.org.ha.model.pms.biz.charging.InvoiceManagerLocal;
import hk.org.ha.model.pms.biz.onestop.SuspendPrescManagerLocal;
import hk.org.ha.model.pms.biz.order.DispOrderManagerLocal;
import hk.org.ha.model.pms.biz.ticketgen.TicketManagerLocal;
import hk.org.ha.model.pms.biz.vetting.VettingManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.corp.vo.EndVettingResult;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.exception.refill.SaveRefillException;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.persistence.disp.RefillScheduleItem;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.udt.disp.DiscontinueStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
import hk.org.ha.model.pms.udt.refill.DocType;
import hk.org.ha.model.pms.udt.refill.RefillScheduleItemStatus;
import hk.org.ha.model.pms.udt.refill.RefillScheduleRefillStatus;
import hk.org.ha.model.pms.udt.refill.RefillScheduleStatus;
import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.log.Log;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.contexts.Contexts;

@Stateful
@Scope(ScopeType.SESSION)
@Name("refillScheduleService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class RefillScheduleServiceBean implements RefillScheduleServiceLocal {
	
	@PersistenceContext
	private EntityManager em;
	
	@Logger
	private Log logger;
	
	@In
	private DispOrderManagerLocal dispOrderManager;
	
	@In
	private VettingManagerLocal vettingManager;
	
	@In
	private RefillCouponRendererLocal refillCouponRenderer;
	
	@In
	private RefillScheduleListManagerLocal refillScheduleListManager;
	
	@In
	private SuspendPrescManagerLocal suspendPrescManager;
	
	@In
	private AlertRetrieveManagerLocal alertRetrieveManager;

	@In
	private PrescCheckManagerLocal prescCheckManager;
	
	@In
	private TicketManagerLocal ticketManager;
	
	@In
	private InvoiceManagerLocal invoiceManager;
	
	@In
	private RefillScheduleUtilsLocal refillScheduleUtils;
	
	@In(required=false)
	private OrderViewInfo orderViewInfo;
	
	private static final String MOI_VERSION_MISMATCH = "0307";
	private static final String ORDER_LOCK_ERR_MSG = "0187";

	private void updateOrderViewInfoForRefillSave(OrderViewInfo orderViewInfoIn){
		orderViewInfo.setPrintLang(orderViewInfoIn.getPrintLang());
		orderViewInfo.setUrgentFlag(orderViewInfoIn.getUrgentFlag());
		orderViewInfo.setNumOfCoupon(orderViewInfoIn.getNumOfCoupon());
		orderViewInfo.setPregnancyCheckFlag(orderViewInfoIn.getPregnancyCheckFlag());
		orderViewInfo.setRefillPrintReminderFlag(orderViewInfoIn.getRefillPrintReminderFlag());
	}
	
	public void forceCompleteStdRefillSchedule(RefillSchedule refillScheduleIn, OrderViewInfo orderViewInfoIn) throws SaveRefillException{
		updateOrderViewInfoForRefillSave(orderViewInfoIn);
		
		if(isMedOrderModified(orderViewInfoIn)){
			refillScheduleListManager.refreshRefillScheduleList(refillScheduleIn.getId(), ORDER_LOCK_ERR_MSG);
			return;
		}

		if( isMedOrderItemModified(refillScheduleIn)){
			refillScheduleListManager.refreshRefillScheduleList(refillScheduleIn.getId(), MOI_VERSION_MISMATCH);
			return;
		}
		
		//Lock MedOrderItem
		List<Long> lockedMoiIdList = lockMedOrderItemList(refillScheduleIn);
		
		PharmOrder pharmOrder = (PharmOrder) Contexts.getSessionContext().get("pharmOrder");
		Long ticketId = pharmOrder.getMedOrder().getTicket().getId();
		Ticket ticket = em.find(Ticket.class, ticketId);
		
		RefillSchedule currRefillSchedule = em.find(RefillSchedule.class, refillScheduleIn.getId());		
		currRefillSchedule.loadChild();
		currRefillSchedule.setStatus(RefillScheduleStatus.ForceComplete);
		currRefillSchedule.setTicket(ticket);
		currRefillSchedule.setPrintReminderFlag(orderViewInfoIn.getRefillPrintReminderFlag());

		for( RefillScheduleItem currRefillScheduleItem : currRefillSchedule.getRefillScheduleItemList() ){
			currRefillScheduleItem.setRefillQty(Integer.valueOf(0));
			currRefillScheduleItem.setUnitPrice(this.retrieveUnitPriceByPharmOrderItem(currRefillScheduleItem.getPharmOrderItem()));
			
			if( currRefillScheduleItem.getPharmOrderItem().getMedOrderItem().getDiscontinueStatus() != null &&
				currRefillScheduleItem.getPharmOrderItem().getMedOrderItem().getDiscontinueStatus() != DiscontinueStatus.None ){
				currRefillScheduleItem.setStatus(RefillScheduleItemStatus.Discontinue);
			}
		}
		em.merge(currRefillSchedule);
		em.flush();
			
		refillScheduleUtils.assignItemNumForRefillSave(Arrays.asList(currRefillSchedule), null);
		for ( MedOrderItem medOrderItem : currRefillSchedule.getPharmOrder().getMedOrder().getMedOrderItemList()) {
			medOrderItem.loadDmInfo(currRefillSchedule.getPharmOrder().getMedOrder().isMpDischarge());
		}
		
		DispOrder dispOrder = dispOrderManager.createDispOrderForRefill(currRefillSchedule, ticket);
		dispOrder.setWardCode(orderViewInfoIn.getWardCode());
		dispOrder.setPatCatCode(orderViewInfoIn.getPatCatCode());
		dispOrder.setSpecCode(orderViewInfoIn.getSpecCode());

		dispOrder.setPrintLang(orderViewInfoIn.getPrintLang());

		em.persist(dispOrder);
		em.flush();
		
		dispOrder.setOrgDispOrderId(dispOrder.getId());
		
		currRefillSchedule = em.find(RefillSchedule.class, currRefillSchedule.getId());
		currRefillSchedule.setDispOrder(dispOrder);
		
		//restore refillQty after create DispOrder 
		for( RefillScheduleItem rsi : currRefillSchedule.getRefillScheduleItemList() ){
			rsi.setRefillQty(rsi.getCalQty());
		}
		
		this.updateRefillScheduleRefillStatus(currRefillSchedule, false, orderViewInfoIn);
		em.flush();
		
		this.updatePharmOrderRefillCount(pharmOrder.getId(), 1);
		
		refillScheduleUtils.restorePharmOrderItemNum(dispOrder);
		
		//save Invoice
		invoiceManager.createInvoiceListFromDispOrder(orderViewInfoIn, dispOrder, new ArrayList<Invoice>());
		
		List<MedOrderItem> confirmDisconItemList = markDiscontinueForRefillSchedultItemList(currRefillSchedule, lockedMoiIdList);
		
		// send DispOrder to corp
		EndVettingResult result = dispOrderManager.refillDispensing(dispOrder, confirmDisconItemList);
		if( !result.getSuccessFlag() ){
			throw new SaveRefillException(result.getMessageCode(), result.getMessageParam());
		}
		dispOrder.getPharmOrder().loadDmInfo();
		
		dispOrder = em.find(DispOrder.class, dispOrder.getId());
		
		//unlock
		MedOrder medOrder = dispOrder.getPharmOrder().getMedOrder();
		medOrder.setProcessingFlag(false);
		medOrder.setWorkstationId(null);
		
		// sync delta change type from CORP disp order to PMS disp order 
		// (because delta change type is checked and updated in CORP)
		vettingManager.updatePharmDispOrderItemDeltaChangeType(result.getDeltaChangeTypeMap(), dispOrder);
		
		//print RefillCoupon
		refillCouponRenderer.printRefillCouponFromDispOrder(dispOrder, false);
	}
	
	public void refillStdRefillSchedule(RefillSchedule refillScheduleIn, OrderViewInfo orderViewInfoIn) throws SaveRefillException {				
		updateOrderViewInfoForRefillSave(orderViewInfoIn);		
		
		if(isMedOrderModified(orderViewInfoIn)){
			refillScheduleListManager.refreshRefillScheduleList(refillScheduleIn.getId(), ORDER_LOCK_ERR_MSG);
			return;
		}
		
		RefillSchedule currRefillSchedule = em.find(RefillSchedule.class, refillScheduleIn.getId());
		currRefillSchedule.loadChild();

		if (alertRetrieveManager.isMdsEnable() && !prescCheckManager.checkPrescAlertByMedOrderItemList(orderViewInfo, currRefillSchedule.getMedOrderItemList(), currRefillSchedule.getPharmOrder().getMedOrder().isMpDischarge())) {
			return;
		}
		
		if( isMedOrderItemModified(refillScheduleIn) ){
			refillScheduleListManager.refreshRefillScheduleList(refillScheduleIn.getId(), MOI_VERSION_MISMATCH);
			return;
		}
		
		//Lock MedOrderItem
		List<Long> lockedMoiIdList = lockMedOrderItemList(refillScheduleIn);
		
		PharmOrder pharmOrder = (PharmOrder) Contexts.getSessionContext().get("pharmOrder");
		Long ticketId = pharmOrder.getMedOrder().getTicket().getId();
		Ticket ticket = em.find(Ticket.class, ticketId);
		currRefillSchedule.setTicket(ticket);
		
		currRefillSchedule.setStatus(RefillScheduleStatus.BeingProcessed);
		currRefillSchedule.setPrintReminderFlag(orderViewInfoIn.getRefillPrintReminderFlag());
		Map<Integer,RefillScheduleItem> rsiInMap = new HashMap<Integer, RefillScheduleItem> ();		
		for( RefillScheduleItem rsiIn : refillScheduleIn.getRefillScheduleItemList() ){
			rsiInMap.put(rsiIn.getItemNum(), rsiIn);
		}
			
		for( RefillScheduleItem rsi : currRefillSchedule.getRefillScheduleItemList() ){			
			if( rsiInMap.containsKey(rsi.getItemNum()) ){
				RefillScheduleItem rsiIn = rsiInMap.get(rsi.getItemNum());
				rsi.setRefillQty(rsiIn.getRefillQty());
				rsi.setAdjQty(rsiIn.getAdjQty());
				rsi.setUnitPrice(this.retrieveUnitPriceByPharmOrderItem(rsi.getPharmOrderItem()));
				
				if( rsi.getPharmOrderItem().getMedOrderItem().getDiscontinueStatus() != null &&
					rsi.getPharmOrderItem().getMedOrderItem().getDiscontinueStatus() != DiscontinueStatus.None ){
					rsi.setStatus(RefillScheduleItemStatus.Discontinue);
				}
			}			
		}
		em.flush();
		em.clear();

		refillScheduleUtils.assignItemNumForRefillSave(Arrays.asList(currRefillSchedule), null);
		for ( MedOrderItem medOrderItem : currRefillSchedule.getPharmOrder().getMedOrder().getMedOrderItemList()) {
			medOrderItem.loadDmInfo(currRefillSchedule.getPharmOrder().getMedOrder().isMpDischarge());
		}
		
		DispOrder dispOrder = dispOrderManager.createDispOrderForRefill(currRefillSchedule, ticket);
		dispOrder.setWardCode(orderViewInfoIn.getWardCode());
		dispOrder.setPatCatCode(orderViewInfoIn.getPatCatCode());
		dispOrder.setSpecCode(orderViewInfoIn.getSpecCode());
		dispOrder.setPrintLang(orderViewInfoIn.getPrintLang());

		for( DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList() ){
			dispOrderItem.setChargeSpecCode(dispOrder.getSpecCode());
		}
		
		// set large layout flag for label reprint and CDDH label preview
		// (note: since the flag is needed to sync to CORP for CDDH label preview, it is set here instead of in print disp order function)
		dispOrderManager.updateDispOrderLargeLayoutFlag(dispOrder);
		
		em.persist(dispOrder);
		em.flush();
		
		dispOrder.setOrgDispOrderId(dispOrder.getId());

		currRefillSchedule = em.find(RefillSchedule.class, currRefillSchedule.getId());
		if( DispOrderStatus.Issued == dispOrder.getStatus() ){
			currRefillSchedule.setStatus(RefillScheduleStatus.Dispensed);
		}
		currRefillSchedule.setDispOrder(dispOrder);
		
		this.updateRefillScheduleRefillStatus(currRefillSchedule, false, orderViewInfoIn);		
		em.flush();
		
		this.updatePharmOrderRefillCount(pharmOrder.getId(), 1);

		refillScheduleUtils.restorePharmOrderItemNum(dispOrder);
		
		//save Invoice
		invoiceManager.createInvoiceListFromDispOrder(orderViewInfoIn, dispOrder, new ArrayList<Invoice>());
		
		List<MedOrderItem> confirmDisconItemList = markDiscontinueForRefillSchedultItemList(currRefillSchedule, lockedMoiIdList);
		
		// send DispOrder to corp
		EndVettingResult result = dispOrderManager.refillDispensing(dispOrder, confirmDisconItemList);
		if( !result.getSuccessFlag() ){
			throw new SaveRefillException(result.getMessageCode(), result.getMessageParam());
		}
		
		dispOrder = em.find(DispOrder.class, dispOrder.getId());
		
		//unlock
		MedOrder medOrder = dispOrder.getPharmOrder().getMedOrder();
		medOrder.setProcessingFlag(false);
		medOrder.setWorkstationId(null);
		
		// sync delta change type from CORP disp order to PMS disp order 
		// (because delta change type is checked and updated in CORP)
		vettingManager.updatePharmDispOrderItemDeltaChangeType(result.getDeltaChangeTypeMap(), dispOrder);
		
		//print Label
		dispOrderManager.printDispOrder(dispOrder, false);
	}
	
	public void cancelDispenseStdRefillSchedule(RefillSchedule refillScheduleIn, OrderViewInfo orderViewInfoIn){
		if(isMedOrderModified(orderViewInfoIn)){
			refillScheduleListManager.refreshRefillScheduleList(refillScheduleIn.getId(), ORDER_LOCK_ERR_MSG);
			return;
		}
		
		RefillSchedule cancelDispRs = em.find(RefillSchedule.class, refillScheduleIn.getId());		
		dispOrderManager.cancelDispensing(cancelDispRs.getDispOrder());									
		
		// Restore changes if Qty is Adjusted due to late refill
		for( RefillScheduleItem cancelDispRsi : cancelDispRs.getRefillScheduleItemList() ){
			cancelDispRsi.setAdjQty(0);
			cancelDispRsi.setRefillQty(cancelDispRsi.getCalQty());
			cancelDispRsi.setUnitPrice(null);
		}
		
		cancelDispRs.setTicket(null);
		cancelDispRs.setDispOrder(null);
		cancelDispRs.setStatus(RefillScheduleStatus.NotYetDispensed);
		cancelDispRs.setPrintReminderFlag(Boolean.FALSE);
		updateRefillScheduleRefillStatus(cancelDispRs, true, null);
		em.flush();
		
		//unlock
		MedOrder medOrder = cancelDispRs.getPharmOrder().getMedOrder();
		medOrder.setProcessingFlag(false);
		medOrder.setWorkstationId(null);
		
		this.updatePharmOrderRefillCount(cancelDispRs.getPharmOrder().getId(), -1);
	}
	
	public void abortStdRefillScheduleList(RefillSchedule refillScheduleIn, OrderViewInfo orderViewInfoIn){
		
		RefillSchedule currRs = em.find(RefillSchedule.class, refillScheduleIn.getId());
		
		if(isMedOrderModified(orderViewInfoIn)){
			refillScheduleListManager.refreshRefillScheduleList(refillScheduleIn.getId(), ORDER_LOCK_ERR_MSG);
			return;
		}
		
		Integer refillCount = this.markDeleteRefillScheduleList(currRs);

		this.updatePharmOrderRefillCount(currRs.getPharmOrder().getId(), refillCount);
		
		//unlock
		MedOrder medOrder = currRs.getPharmOrder().getMedOrder();
		
		vettingManager.releaseOrder(orderViewInfoIn, medOrder);
		
		refillScheduleListManager.refreshRefillScheduleList(currRs.getId(), "0180");
	}
	
	public void terminateStdRefillScheduleList(RefillSchedule refillScheduleIn, OrderViewInfo orderViewInfoIn) throws SaveRefillException{
		RefillSchedule currRs = em.find(RefillSchedule.class, refillScheduleIn.getId());
		
		if(isMedOrderModified(orderViewInfoIn)){
			refillScheduleListManager.refreshRefillScheduleList(refillScheduleIn.getId(), ORDER_LOCK_ERR_MSG);
			return;
		}
		
		boolean associateTicket = true;
		if( RefillScheduleRefillStatus.Current.equals(currRs.getRefillStatus()) ){
			associateTicket = false;
		}
		
		Integer refillCount = this.markDeleteRefillScheduleList(currRs);
		em.flush();
		
		this.updatePharmOrderRefillCount(currRs.getPharmOrder().getId(), refillCount);
		
		if( associateTicket ){
			PharmOrder pharmOrder = (PharmOrder) Contexts.getSessionContext().get("pharmOrder");
			Ticket ticketIn = em.find(Ticket.class, pharmOrder.getMedOrder().getTicket().getId());
			
			Pair<String, Ticket> result = ticketManager.validateTicket(ticketIn.getTicketDate(), ticketIn.getTicketNum(), ticketIn, null, OneStopOrderType.RefillOrder, false);
			if( result.getFirst() != null ){
				throw new SaveRefillException(result.getFirst(), null);
			}
			currRs.setTicket(result.getSecond());
		}
		//unlock
		MedOrder medOrder = currRs.getPharmOrder().getMedOrder();
		
		vettingManager.releaseOrder(orderViewInfoIn, medOrder);
	}
	
	public void suspendRefillSchedule(RefillSchedule refillScheduleIn, OrderViewInfo orderViewInfoIn){
		RefillSchedule currRs = em.find(RefillSchedule.class, refillScheduleIn.getId());
		DispOrder dispOrder = currRs.getDispOrder();
		String overrideRefillAction = null;
		
		if(isMedOrderModified(orderViewInfoIn)){
			refillScheduleListManager.refreshRefillScheduleList(refillScheduleIn.getId(), ORDER_LOCK_ERR_MSG);
			return;
		}
		
		suspendPrescManager.verifySuspendPrescription(dispOrder.getId(), dispOrder.getVersion(), SUSPEND_ORDER_SUSPENDCODE.get(), "refill");
		
		refillScheduleListManager.refreshRefillScheduleList(currRs.getId(), overrideRefillAction);
	}
	
	public void reverseSuspendRefillSchedule(RefillSchedule refillScheduleIn, OrderViewInfo orderViewInfoIn) {
		updateOrderViewInfoForRefillSave(orderViewInfoIn);
		
		if(isMedOrderModified(orderViewInfoIn)){
			refillScheduleListManager.refreshRefillScheduleList(refillScheduleIn.getId(), ORDER_LOCK_ERR_MSG);
			return;
		}
		
		RefillSchedule currRs = em.find(RefillSchedule.class, refillScheduleIn.getId());
		currRs.loadChild();
		
		if (alertRetrieveManager.isMdsEnable() && !prescCheckManager.checkPrescAlertByMedOrderItemList(orderViewInfo, currRs.getMedOrderItemList(), currRs.getPharmOrder().getMedOrder().isMpDischarge())) {
			return;
		}
		
		boolean requireCreateDispOrder = false;
		String overrideRefillAction = null;
		
		DispOrder dispOrder = currRs.getDispOrder();
		for( RefillScheduleItem refillScheduleItem : currRs.getRefillScheduleItemList() ){
			DispOrderItem dispOrderItem = dispOrder.getDispOrderItemByPoOrgItemNum(refillScheduleItem.getItemNum());
			if( dispOrderItem != null && 
					(dispOrderItem.getDispQty().compareTo(BigDecimal.valueOf(refillScheduleItem.getRefillQty())) != 0 || 
							dispOrderItem.getDurationInDay().compareTo(refillScheduleItem.getRefillDuration()) != 0 )){
				requireCreateDispOrder = true;
				break;
			}
		}
		
		if( requireCreateDispOrder ){
			if( !reverseSuspendAndCreateDispOrderForRefillSchedule(orderViewInfo, currRs) ){
				overrideRefillAction = MOI_VERSION_MISMATCH;
			}
		}else{
			suspendPrescManager.reversePrescriptionSuspend(dispOrder.getId(), dispOrder.getVersion(), "refill");			
		}
		refillScheduleListManager.refreshOrderAndRefillScheduleList(currRs.getId(), overrideRefillAction);
	}
	
	private boolean reverseSuspendAndCreateDispOrderForRefillSchedule(OrderViewInfo orderViewInfoIn, RefillSchedule refillScheduleIn){
		if( isMedOrderItemModified(refillScheduleIn) ){
			return false;
		}

		//Lock MedOrderItem
		List<Long> lockedMoiIdList = lockMedOrderItemList(refillScheduleIn);
		
		Long ticketId = refillScheduleIn.getTicket().getId();
		Ticket ticket = em.find(Ticket.class, ticketId);
		
		DispOrder prevDispOrder = em.find(DispOrder.class, refillScheduleIn.getDispOrder().getId());
		refillScheduleIn.setDispOrder(null);
		
		refillScheduleUtils.assignItemNumForRefillSave(Arrays.asList(refillScheduleIn), prevDispOrder);
		for ( MedOrderItem medOrderItem : refillScheduleIn.getPharmOrder().getMedOrder().getMedOrderItemList()) {
			medOrderItem.loadDmInfo(refillScheduleIn.getPharmOrder().getMedOrder().isMpDischarge());
		}
		
		DispOrder dispOrder = dispOrderManager.createDispOrderForRefill(refillScheduleIn, ticket);
		dispOrder.setSpecCode(prevDispOrder.getSpecCode());
		for( DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList() ){
			dispOrderItem.setChargeSpecCode(dispOrder.getSpecCode());
		}
		
		prevDispOrder = em.find(DispOrder.class, prevDispOrder.getId());
		if (prevDispOrder != null) {
			dispOrderManager.updateDispOrderByPrevDispOrder(dispOrder, prevDispOrder);
		}

		if (dispOrder.getStatus() == DispOrderStatus.Issued) {
			dispOrder.setIssueDate(new Date());
		}

		// set large layout flag for label reprint and CDDH label preview (for PMS mode)
		// (note: since the flag is needed to sync to CORP for CDDH label preview, it is set here instead of in print disp order function)
		Map<Long, Boolean> largeLayoutFlagMap = dispOrderManager.updateDispOrderLargeLayoutFlag(dispOrder);
		
		em.persist(dispOrder);
		em.flush();
		
		if (prevDispOrder == null) {
			dispOrder.setOrgDispOrderId(dispOrder.getId());
		}

		prevDispOrder = em.find(DispOrder.class, prevDispOrder.getId());
		prevDispOrder.setAdminStatus(DispOrderAdminStatus.Normal);
		prevDispOrder.markSysDeleted();
		em.flush();
		
		RefillSchedule currRs = em.find(RefillSchedule.class, refillScheduleIn.getId());
		currRs.setDispOrder(dispOrder);
		currRs.setPrintReminderFlag(orderViewInfoIn.getRefillPrintReminderFlag());
		currRs.setStatus(RefillScheduleStatus.BeingProcessed);
		if( DispOrderStatus.Issued == dispOrder.getStatus() ){
			currRs.setStatus(RefillScheduleStatus.Dispensed);
		}
		for( RefillScheduleItem currRsi : currRs.getRefillScheduleItemList() ){
			currRsi.setUnitPrice(this.retrieveUnitPriceByPharmOrderItem(currRsi.getPharmOrderItem()));
			
			if( currRsi.getPharmOrderItem().getMedOrderItem().getDiscontinueStatus() != null &&
				currRsi.getPharmOrderItem().getMedOrderItem().getDiscontinueStatus() != DiscontinueStatus.None ){
				currRsi.setStatus(RefillScheduleItemStatus.Discontinue);
			}
		}
		
		updateNextRefillScheduleInfo(currRs, orderViewInfoIn);
		
		refillScheduleUtils.restorePharmOrderItemNum(dispOrder);
		
		//save Invoice
		invoiceManager.createInvoiceListFromDispOrder(orderViewInfoIn, dispOrder, prevDispOrder.getInvoiceList());
		
		List<MedOrderItem> confirmDisconItemList = markDiscontinueForRefillSchedultItemList(currRs, lockedMoiIdList);
		
		// send DispOrder to corp
		EndVettingResult result = dispOrderManager.refillDispensing(dispOrder, confirmDisconItemList, false, largeLayoutFlagMap);
				
		dispOrder = em.find(DispOrder.class, dispOrder.getId());

		// sync delta change type from CORP disp order to PMS disp order 
		// (because delta change type is checked and updated in CORP)
		vettingManager.updatePharmDispOrderItemDeltaChangeType(result.getDeltaChangeTypeMap(), dispOrder);
		
		//print Label
		dispOrderManager.printDispOrder(dispOrder, false);
		
		return true;
	}
	
	@SuppressWarnings("unchecked")
	private void updateNextRefillScheduleInfo(RefillSchedule currRs, OrderViewInfo orderViewInfoIn){
		List<RefillSchedule> rsList = (List<RefillSchedule>) em.createQuery(
				"select o from RefillSchedule o" + // 20120303 index check : RefillSchedule.pharmOrder : FK_REFILL_SCHEDULE_01
				" where o.pharmOrder.id = :pharmOrderId" +
				" and o.groupNum = :groupNum" +
				" and o.docType = :docType" +
				" and o.refillStatus = :refillStatus")
				.setParameter("pharmOrderId", currRs.getPharmOrder().getId())
				.setParameter("groupNum", currRs.getGroupNum() )																					
				.setParameter("docType", DocType.Standard)
				.setParameter("refillStatus", RefillScheduleRefillStatus.Next)
				.getResultList();
		
		if( !rsList.isEmpty() ){
			RefillSchedule nextRs = rsList.get(0);
			nextRs.setPrintFlag(false);
			
			if( orderViewInfoIn != null ){
				nextRs.setNumOfCoupon(orderViewInfoIn.getNumOfCoupon());
				nextRs.setPrintLang(orderViewInfoIn.getPrintLang());
			}
			em.flush();
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateRefillScheduleRefillStatus(RefillSchedule currRs, boolean isReverse, OrderViewInfo orderViewInfoIn){
		// add Previous 
		Integer prevRefillNum = currRs.getRefillNum() - 1;
		Integer currRefillNum = currRs.getRefillNum();
		Integer nextRefillNum = currRs.getRefillNum() + 1;
		
		if( isReverse ){
			prevRefillNum = currRs.getRefillNum()-2;
			currRefillNum = currRs.getRefillNum()-1;
			nextRefillNum = currRs.getRefillNum();
		}
		
		List<RefillSchedule> rsList = (List<RefillSchedule>) em.createQuery(
				"select o from RefillSchedule o" + // 20120303 index check : RefillSchedule.pharmOrder : FK_REFILL_SCHEDULE_01
				" where o.pharmOrder.id = :pharmOrderId" +
				" and o.groupNum = :groupNum" +
				" and o.docType = :docType")
				.setParameter("pharmOrderId", currRs.getPharmOrder().getId())
				.setParameter("groupNum", currRs.getGroupNum() )																					
				.setParameter("docType", DocType.Standard)
				.getResultList();
		
		for( RefillSchedule updateStatusRs : rsList ){	
			if( updateStatusRs.getRefillNum().equals( currRefillNum ) ){
				updateStatusRs.setRefillStatus(RefillScheduleRefillStatus.Current);
			}else if( updateStatusRs.getRefillNum().equals( nextRefillNum ) ){
				updateStatusRs.setRefillStatus(RefillScheduleRefillStatus.Next);
				updateStatusRs.setPrintFlag(false);
				if( orderViewInfoIn != null ){
					updateStatusRs.setNumOfCoupon(orderViewInfoIn.getNumOfCoupon());
					updateStatusRs.setPrintLang(orderViewInfoIn.getPrintLang());
				}
			}else if(  updateStatusRs.getRefillNum().equals( prevRefillNum ) ){
				updateStatusRs.setRefillStatus(RefillScheduleRefillStatus.Previous);
			}else{
				updateStatusRs.setRefillStatus(RefillScheduleRefillStatus.None);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private Integer markDeleteRefillScheduleList(RefillSchedule refillScheduleIn){
		List<RefillSchedule> abortRsList = (List<RefillSchedule>) em.createQuery(
				"select o from RefillSchedule o" + // 20120303 index check : RefillSchedule.pharmOrder : FK_REFILL_SCHEDULE_01
				" where o.pharmOrder.id = :pharmOrderId" +
				" and o.ticket is null" +
				" and o.docType = :docType")
				.setParameter("pharmOrderId", refillScheduleIn.getPharmOrder().getId() )
				.setParameter("docType", DocType.Standard)
				.getResultList();

		Integer abortRefillCount = abortRsList.size();
		
		List<RefillSchedule> currAbortRsList = new ArrayList<RefillSchedule>();
		
		for( RefillSchedule abortRs : abortRsList ){
			abortRs.setStatus(RefillScheduleStatus.Abort);
			if( RefillScheduleRefillStatus.Next.equals( abortRs.getRefillStatus()) ){
				currAbortRsList.add(abortRs);
			}
		}
		
		for( RefillSchedule currAbortRs : currAbortRsList ){
			this.updateRefillScheduleRefillStatus(currAbortRs, false, null);
		}
		
		return abortRefillCount;
	}
	
	@SuppressWarnings("unchecked")
	private List<MedOrderItem> markDiscontinueForRefillSchedultItemList(RefillSchedule refillSchedule, List<Long>  medOrderItemIdList){
		List<MedOrderItem> confirmDisconIdList = new ArrayList<MedOrderItem>();
		List<MedOrderItem> moiList = em.createQuery(
				"select o from MedOrderItem o" + // 20150206 index check : MedOrderItem.id : PK_MED_ORDER_ITEM
				" where o.id in :idList")
				.setParameter("idList", medOrderItemIdList)
				.getResultList();
		//update MOI ConfirmDiscon
		for( MedOrderItem medOrderItem : moiList){
			if( medOrderItem.getDiscontinueStatus() == DiscontinueStatus.Pending ){
				medOrderItem.setDiscontinueStatus(DiscontinueStatus.Confirmed);
				confirmDisconIdList.add(medOrderItem);
			}
		}

		List<RefillScheduleItem> disconRsiList = (List<RefillScheduleItem>) em.createQuery(
				"select o from RefillScheduleItem o" + // 20150206 index check : RefillSchedule.pharmOrder : FK_REFILL_SCHEDULE_01
				" where o.refillSchedule.pharmOrder.id = :pharmOrderId" +
				" and o.refillSchedule.ticket is null" +
				" and o.refillSchedule.groupNum = :groupNum " +
				" and o.refillSchedule.docType = :docType" +
				" and o.pharmOrderItem.medOrderItem.discontinueStatus = :disConStatus")
				.setParameter("pharmOrderId", refillSchedule.getPharmOrder().getId() )
				.setParameter("groupNum", refillSchedule.getGroupNum() )
				.setParameter("docType", DocType.Standard)
				.setParameter("disConStatus", DiscontinueStatus.Confirmed)
				.getResultList();
		
		for( RefillScheduleItem disconRsi : disconRsiList ){
			disconRsi.setStatus(RefillScheduleItemStatus.Discontinue);
		}
		return confirmDisconIdList;
	}
	
	private BigDecimal retrieveUnitPriceByPharmOrderItem(PharmOrderItem pharmOrderItemIn){	
		DmDrug dmDrug = DmDrugCacher.instance().getDmDrug(pharmOrderItemIn.getItemCode()); 
		
		if( dmDrug != null && dmDrug.getDmProcureSummary() != null ){				
			return BigDecimal.valueOf(dmDrug.getDmProcureSummary().getAverageUnitPrice());			
		}
		
		return null;
	}
	
	private void updatePharmOrderRefillCount( Long pharmOrderId ,Integer numOfDispRefill ){
		PharmOrder managedPharmOrder = em.find(PharmOrder.class, pharmOrderId);		
		Integer refillCount = managedPharmOrder.getRefillCount() + numOfDispRefill;
		managedPharmOrder.setRefillCount(refillCount);
		em.flush();
	}
	
	@SuppressWarnings("unchecked")
	private List<Long> lockMedOrderItemList(RefillSchedule refillScheduleIn) {
		List<Long> medOrderItemIdList = new ArrayList<Long>();
		PharmOrder pharmOrder = (PharmOrder) Contexts.getSessionContext().get("pharmOrder");
		
		for(RefillScheduleItem rsi : refillScheduleIn.getRefillScheduleItemList()){
			PharmOrderItem currPoi = pharmOrder.getPharmOrderItemByOrgItemNum(rsi.getItemNum());
			
			if( currPoi != null ){
				medOrderItemIdList.add(currPoi.getMedOrderItem().getId());
			}
		}	
		
		if (medOrderItemIdList.size() == 0) {
			throw new IllegalArgumentException("lockMedOrderItemList missing medOrderItemId!");
		}
		
		Query query = em.createQuery(
				"select o.id from MedOrderItem o" +  // 20150209 index check : MedOrderItem.id : PK_MED_ORDER_ITEM
				" where o.id in :medOrderItemIdList")
				.setParameter("medOrderItemIdList", medOrderItemIdList)
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock);
		return query.getResultList();
	}
	
	private boolean isMedOrderItemModified(RefillSchedule refillScheduleIn){
		PharmOrder pharmOrder = (PharmOrder) Contexts.getSessionContext().get("pharmOrder");
		
		for(RefillScheduleItem rsi : refillScheduleIn.getRefillScheduleItemList()){
			PharmOrderItem currPoi = pharmOrder.getPharmOrderItemByOrgItemNum(rsi.getItemNum());
			
			if( currPoi != null ){
				MedOrderItem orgMoi = em.find(MedOrderItem.class, currPoi.getMedOrderItem().getId());
				if( !orgMoi.getVersion().equals(currPoi.getMedOrderItem().getVersion()) ){
					return true;
				}				
			}
		}
		return false;
	}
	
	private boolean isMedOrderModified(OrderViewInfo orderViewInfoIn,MedOrder orgMedOrder){
		logger.info("isMedOrderModified==========================isMedOrderModified, orgMedOrder:#0, orderViewInfoIn:#1", orgMedOrder.getVersion(), orderViewInfoIn.getMedOrderVersion());
		if (!orgMedOrder.getVersion().equals(orderViewInfoIn.getMedOrderVersion())) {
			return true;
		}
		return false;
	}
	
	private boolean isMedOrderModified(OrderViewInfo orderViewInfoIn) {
		MedOrder orgMedOrder = em.find(MedOrder.class, orderViewInfoIn.getMedOrderId());
		return this.isMedOrderModified(orderViewInfoIn, orgMedOrder);
	}
	
	public void cancelRefill(OrderViewInfo orderViewInfoIn){
		MedOrder medOrder = em.find(MedOrder.class, orderViewInfoIn.getMedOrderId());
		if( isMedOrderModified(orderViewInfoIn, medOrder) ){
			return;
		}
		vettingManager.releaseOrder(orderViewInfo, medOrder);
	}
	
	public String modifyRefillSuccess(OrderViewInfo orderViewInfoIn){
		String errMsg = "";		
		MedOrder medOrder = em.find(MedOrder.class, orderViewInfoIn.getMedOrderId());
		if( isMedOrderModified(orderViewInfoIn, medOrder) ){
			errMsg = "0187";
		}
		return errMsg;
	}
	
	@Remove
	public void destroy(){
	}
}
