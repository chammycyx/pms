package hk.org.ha.model.pms.vo.onestop;

import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.udt.validation.SearchTextType;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class OneStopSummary implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Patient patient;
	
	private SearchTextType searchTextType;

	private List<OneStopOrderItemSummary> oneStopOrderItemSummaryList;
	
	private List<OneStopRefillItemSummary> oneStopRefillItemSummaryList;
	
	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public void setSearchTextType(SearchTextType searchTextType) {
		this.searchTextType = searchTextType;
	}

	public SearchTextType getSearchTextType() {
		return searchTextType;
	}
	
	public List<OneStopOrderItemSummary> getOneStopOrderItemSummaryList() {
		return oneStopOrderItemSummaryList;
	}

	public void setOneStopOrderItemSummaryList(List<OneStopOrderItemSummary> oneStopOrderItemSummaryList) {
		this.oneStopOrderItemSummaryList = oneStopOrderItemSummaryList;
	}

	public List<OneStopRefillItemSummary> getOneStopRefillItemSummaryList() {
		return oneStopRefillItemSummaryList;
	}

	public void setOneStopRefillItemSummaryList(List<OneStopRefillItemSummary> oneStopRefillItemSummaryList) {
		this.oneStopRefillItemSummaryList = oneStopRefillItemSummaryList;
	}
}