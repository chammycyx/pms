package hk.org.ha.model.pms.biz.order;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;

import javax.ejb.Stateless;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;

@AutoCreate
@Stateless
@Name("medOrderManager")
@MeasureCalls
public class MedOrderManagerBean implements MedOrderManagerLocal {

	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private RefillSchedule refillSchedule;

	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private DispOrder dispOrder;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private PharmOrder pharmOrder;
	
	public void setRefillSchedule(RefillSchedule inRefillSchedule) {
		refillSchedule = inRefillSchedule;
	}

	public void setDispOrder(DispOrder inDispOrder) {
		dispOrder = inDispOrder;
	}

	public void setPharmOrder(PharmOrder inPharmOrder) {
		pharmOrder = inPharmOrder;
	}

	public void clear() {
		if (refillSchedule != null) {
			refillSchedule = null;
		}
		if (dispOrder != null) {
			dispOrder = null;
		}
		if (pharmOrder != null) {
			pharmOrder = null;
		}
	}	
}
