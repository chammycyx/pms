package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.math.BigDecimal;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class RefillExpiryRpt implements Serializable {

	private static final long serialVersionUID = 1L;

	private String workstoreCode;
	
	private String specCode;
	
	private Long totalRefillCount;
	
	private BigDecimal totalDrugCost;

	public RefillExpiryRpt(String workstoreCode, String specCode,
			Long totalRefillCount, BigDecimal totalDrugCost) {
		super();
		this.workstoreCode = workstoreCode;
		this.specCode = specCode;
		this.totalRefillCount = totalRefillCount;
		this.totalDrugCost = totalDrugCost;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public Long getTotalRefillCount() {
		return totalRefillCount;
	}

	public void setTotalRefillCount(Long totalRefillCount) {
		this.totalRefillCount = totalRefillCount;
	}

	public void setTotalDrugCost(BigDecimal totalDrugCost) {
		this.totalDrugCost = totalDrugCost;
	}

	public BigDecimal getTotalDrugCost() {
		return totalDrugCost;
	}

}
