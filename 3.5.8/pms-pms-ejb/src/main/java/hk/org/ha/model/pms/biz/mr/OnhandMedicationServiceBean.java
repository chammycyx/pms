package hk.org.ha.model.pms.biz.mr;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.vo.mr.MedProfileMrItem;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;
import org.jboss.seam.security.Identity;

@Stateful
@Scope(ScopeType.SESSION)
@Name("onhandMedicationService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class OnhandMedicationServiceBean implements OnhandMedicationServiceLocal {

	@Logger
	private Log logger;	
	
	@In
	private MrManagerLocal mrManager;

	@In
	private Identity identity;
	
	@In
	private MedProfile medProfile;

	@Out(required = false)
	private List<MedProfileMrItem> onhandMedicationList;
	
	@Out(required = false)
	private Boolean errorOnRetrieveOnhandMedicationList;
	
	@Out(required = false)
	private Boolean errorOnAddMrAnnotationToOnhandMedication;
	
	@Override
	public void retrieveOnhandMedicationList() {
		
		onhandMedicationList = null;
		errorOnRetrieveOnhandMedicationList = Boolean.FALSE;
		
		try {
			onhandMedicationList = mrManager.retrieveOnhandMedicationList(medProfile);
		} catch (Exception ex) {
			logger.info(ex, ex);
			errorOnRetrieveOnhandMedicationList = Boolean.TRUE;
		}		
	}
	
	@Override
	public boolean addMrAnnotationToOnhandMedication(String patHospCode, String orderNum,
			Integer itemNum, String mrAnnotation) {
		
		errorOnAddMrAnnotationToOnhandMedication = Boolean.FALSE;
		boolean success = false;
		
		try {
			success = mrManager.addMrAnnotation(identity, patHospCode, orderNum, itemNum, mrAnnotation);
			onhandMedicationList = mrManager.retrieveOnhandMedicationList(medProfile);
		} catch (Exception ex) {
			logger.info(ex, ex);
			errorOnAddMrAnnotationToOnhandMedication = Boolean.TRUE;
		}
		return success;
	}
	
	@Remove
	@Override
	public void destroy() {
	}
}
