package hk.org.ha.model.pms.vo.vetting;

import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.udt.charging.ClearanceStatus;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.vo.alert.AlertProfile;
import hk.org.ha.model.pms.vo.alert.mds.ErrorInfo;
import hk.org.ha.model.pms.vo.charging.PatientSfi;
import hk.org.ha.model.pms.vo.ehr.EhrPatient;
import hk.org.ha.model.pms.vo.onestop.OneStopOrderInfo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class OrderViewInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private boolean pharmViewFlag;
	
	private boolean allowEditFlag;
	
	private boolean autoSuspendFlag;
	
	private boolean skipDuplicateCheckFlag;
	
	private boolean alertOverrideFlag;

	private boolean alertCheckFlag;
	
	private boolean urgentFlag;

	private boolean awaitSfiFlag;
	
	private boolean awaitSfiEditableFlag;
	
	private boolean pregnancyCheckFlag;
	
	private boolean pregnancyCheckEditableFlag;
	
	private boolean ticketAssociateFlag;

	private boolean dispSfiRefillFlag;
	
	private ClearanceStatus sfiInvoiceClearanceStatus;
	
	private boolean sfiInvoicePaymentClearFlag;
	
	private boolean skipSfiClearanceFlag; //For Sfi Force Proceed
	
	private boolean dispOrderWoSfiItemEditFlag;
	
	private boolean refillPrintReminderFlag;
	
	private String specCode;
	
	private String wardCode;
	
	private String patCatCode;
	
	private String pharmRemarkLogonUser;
	
	private String acknowledgeReasonUser;
	
	private String overrideAlertUser;
	
	private String doctorCode;
	
	private String doctorName; 

	private String hkid;
	
	private String otherDoc;
	
	private String saveSuspendCode;
	
	private String prevSuspendCode;
	
	private int numOfCoupon;
	
	private OneStopOrderType oneStopOrderType;
	
	private PrintLang printLang;
	
	private Long refillScheduleId;
	
	private Long dispOrderId;
	
	private Integer orgMaxItemNum;
	
	//for frontEnd handling
	private String sfiRefillCouponNum;
	
	private String sfiRefillCouponNumErrorString;
	
	private String medOrderNumForCoupon;
	
	private List<MedOrderItemInfo> medOrderItemInfoList;
	
	private List<PharmOrderItemInfo> pharmOrderItemInfoList;

	private List<String> infoMsgCodeList;
	
	private List<String[]> infoMsgParamList;
	
	private List<String> errorMsgCodeList;

	private List<String[]> errorMsgParamList;
	
	private List<ErrorInfo> alertErrorInfoList;
	
	private List<String> drsMsgCodeList;

	private AlertProfile alertProfile;
	
	private Map<Integer, String> discontinueMessageTlf;
	
	private long medOrderId;
	
	private long medOrderVersion;
	
	private boolean readOnlyDeleteRx;
	
	private PatientSfi patientSfi;
	
	private boolean drsFlag;
	
	private EhrPatient ehrPatient;
		
	public OrderViewInfo() {

		super();

		pharmViewFlag              = true;
		allowEditFlag              = true;
		autoSuspendFlag            = false;
		skipDuplicateCheckFlag     = false;
		alertOverrideFlag          = false;
		alertCheckFlag           = false;
		urgentFlag                 = false;
		awaitSfiFlag               = false;
		awaitSfiEditableFlag       = true;
		pregnancyCheckFlag         = false;
		pregnancyCheckEditableFlag = true;
		ticketAssociateFlag        = false;
		dispSfiRefillFlag		   = false;
		dispOrderWoSfiItemEditFlag = true;
		skipSfiClearanceFlag	   = false;
		refillPrintReminderFlag	   = false;
		sfiInvoicePaymentClearFlag = false;
		readOnlyDeleteRx           = false;
		drsFlag					   = false;
		
		numOfCoupon            = 1;
		orgMaxItemNum          = Integer.valueOf(0);
		
		medOrderItemInfoList   = new ArrayList<MedOrderItemInfo>();
		pharmOrderItemInfoList = new ArrayList<PharmOrderItemInfo>();
		infoMsgCodeList        = new ArrayList<String>();
		infoMsgParamList       = new ArrayList<String[]>();
		errorMsgCodeList       = new ArrayList<String>();
		errorMsgParamList      = new ArrayList<String[]>();
		alertErrorInfoList     = new ArrayList<ErrorInfo>();
		drsMsgCodeList		   = new ArrayList<String>();
		
		sfiRefillCouponNum = StringUtils.EMPTY;
		sfiRefillCouponNumErrorString = StringUtils.EMPTY;
		medOrderNumForCoupon = null;
		sfiInvoiceClearanceStatus = null;
		patientSfi = null;
		
		discontinueMessageTlf = new HashMap<Integer, String>();
		
		medOrderId = -1;
		medOrderVersion = -1;
	}
		
	public boolean getPharmViewFlag() {
		return pharmViewFlag;
	}

	public void setPharmViewFlag(boolean pharmViewFlag) {
		this.pharmViewFlag = pharmViewFlag;
	}

	public boolean getAllowEditFlag() {
		return allowEditFlag;
	}

	public void setAllowEditFlag(boolean allowEditFlag) {
		this.allowEditFlag = allowEditFlag;
	}

	public boolean getAutoSuspendFlag() {
		return autoSuspendFlag;
	}

	public void setAutoSuspendFlag(boolean autoSuspendFlag) {
		this.autoSuspendFlag = autoSuspendFlag;
	}

	public boolean getSkipDuplicateCheckFlag() {
		return skipDuplicateCheckFlag;
	}

	public void setSkipDuplicateCheckFlag(boolean skipDuplicateCheckFlag) {
		this.skipDuplicateCheckFlag = skipDuplicateCheckFlag;
	}

	public boolean getAlertOverrideFlag() {
		return alertOverrideFlag;
	}

	public void setAlertOverrideFlag(boolean alertOverrideFlag) {
		this.alertOverrideFlag = alertOverrideFlag;
	}

	public boolean getAlertCheckFlag() {
		return alertCheckFlag;
	}

	public void setAlertCheckFlag(boolean alertCheckFlag) {
		this.alertCheckFlag = alertCheckFlag;
	}

	public boolean getUrgentFlag() {
		return urgentFlag;
	}

	public void setUrgentFlag(boolean urgentFlag) {
		this.urgentFlag = urgentFlag;
	}

	public boolean getAwaitSfiFlag() {
		return awaitSfiFlag;
	}

	public void setAwaitSfiFlag(boolean awaitSfiFlag) {
		this.awaitSfiFlag = awaitSfiFlag;
	}

	public boolean getAwaitSfiEditableFlag() {
		return awaitSfiEditableFlag;
	}

	public void setAwaitSfiEditableFlag(boolean awaitSfiEditableFlag) {
		this.awaitSfiEditableFlag = awaitSfiEditableFlag;
	}
	
	public boolean getPregnancyCheckFlag() {
		return pregnancyCheckFlag;
	}

	public void setPregnancyCheckFlag(boolean pregnancyCheckFlag) {
		this.pregnancyCheckFlag = pregnancyCheckFlag;
	}

	public boolean getPregnancyCheckEditableFlag() {
		return pregnancyCheckEditableFlag;
	}

	public void setPregnancyCheckEditableFlag(boolean pregnancyCheckEditableFlag) {
		this.pregnancyCheckEditableFlag = pregnancyCheckEditableFlag;
	}

	public boolean getTicketAssociateFlag() {
		return ticketAssociateFlag;
	}
	
	public void setTicketAssociateFlag(boolean ticketAssociateFlag) {
		this.ticketAssociateFlag = ticketAssociateFlag;
	}
	
	public boolean isDispSfiRefillFlag() {
		return dispSfiRefillFlag;
	}

	public void setDispSfiRefillFlag(boolean dispSfiRefillFlag) {
		this.dispSfiRefillFlag = dispSfiRefillFlag;
	}

	public boolean isSkipSfiClearanceFlag() {
		return skipSfiClearanceFlag;
	}

	public void setSkipSfiClearanceFlag(boolean skipSfiClearanceFlag) {
		this.skipSfiClearanceFlag = skipSfiClearanceFlag;
	}
	
	public boolean isSfiInvoicePaymentClearFlag() {
		return sfiInvoicePaymentClearFlag;
	}

	public void setSfiInvoicePaymentClearFlag(boolean sfiInvoicePaymentClearFlag) {
		this.sfiInvoicePaymentClearFlag = sfiInvoicePaymentClearFlag;
	}

	public boolean isDispOrderWoSfiItemEditFlag() {
		return dispOrderWoSfiItemEditFlag;
	}

	public void setDispOrderWoSfiItemEditFlag(boolean dispOrderWoSfiItemEditFlag) {
		this.dispOrderWoSfiItemEditFlag = dispOrderWoSfiItemEditFlag;
	}
	
	public boolean getRefillPrintReminderFlag() {
		return refillPrintReminderFlag;
	}
	
	public void setRefillPrintReminderFlag(boolean refillPrintReminderFlag) {
		this.refillPrintReminderFlag = refillPrintReminderFlag;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public String getPatCatCode() {
		return patCatCode;
	}

	public void setPatCatCode(String patCatCode) {
		this.patCatCode = patCatCode;
	}

	public String getPharmRemarkLogonUser() {
		return pharmRemarkLogonUser;
	}

	public void setPharmRemarkLogonUser(String pharmRemarkLogonUser) {
		this.pharmRemarkLogonUser = pharmRemarkLogonUser;
	}

	public String getAcknowledgeReasonUser() {
		return acknowledgeReasonUser;
	}

	public void setAcknowledgeReasonUser(String acknowledgeReasonUser) {
		this.acknowledgeReasonUser = acknowledgeReasonUser;
	}

	public String getOverrideAlertUser() {
		return overrideAlertUser;
	}
	
	public void setOverrideAlertUser(String overrideAlertUser) {
		this.overrideAlertUser = overrideAlertUser;
	}
		
	public String getDoctorCode() {
		return doctorCode;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}
	
	public String getOtherDoc() {
		return otherDoc;
	}

	public void setOtherDoc(String otherDoc) {
		this.otherDoc = otherDoc;
	}
	
	public String getSaveSuspendCode() {
		return saveSuspendCode;
	}

	public void setSaveSuspendCode(String saveSuspendCode) {
		this.saveSuspendCode = saveSuspendCode;
	}

	public String getPrevSuspendCode() {
		return prevSuspendCode;
	}

	public void setPrevSuspendCode(String prevSuspendCode) {
		this.prevSuspendCode = prevSuspendCode;
	}
	
	public int getNumOfCoupon() {
		return numOfCoupon;
	}

	public void setNumOfCoupon(int numOfCoupon) {
		this.numOfCoupon = numOfCoupon;
	}

	public OneStopOrderType getOneStopOrderType() {
		return oneStopOrderType;
	}

	public void setOneStopOrderType(OneStopOrderType oneStopOrderType) {
		this.oneStopOrderType = oneStopOrderType;
	}

	public PrintLang getPrintLang() {
		return printLang;
	}

	public void setPrintLang(PrintLang printLang) {
		this.printLang = printLang;
	}

	public Long getRefillScheduleId() {
		return refillScheduleId;
	}

	public void setRefillScheduleId(Long refillScheduleId) {
		this.refillScheduleId = refillScheduleId;
	}

	public Long getDispOrderId() {
		return dispOrderId;
	}

	public void setDispOrderId(Long dispOrderId) {
		this.dispOrderId = dispOrderId;
	}

	public Integer getOrgMaxItemNum() {
		return orgMaxItemNum;
	}

	public void setOrgMaxItemNum(Integer orgMaxItemNum) {
		this.orgMaxItemNum = orgMaxItemNum;
	}

	public List<MedOrderItemInfo> getMedOrderItemInfoList() {
		return medOrderItemInfoList;
	}

	public void setMedOrderItemInfoList(List<MedOrderItemInfo> medOrderItemInfoList) {
		this.medOrderItemInfoList = medOrderItemInfoList;
	}

	public List<PharmOrderItemInfo> getPharmOrderItemInfoList() {
		return pharmOrderItemInfoList;
	}

	public void setPharmOrderItemInfoList(
			List<PharmOrderItemInfo> pharmOrderItemInfoList) {
		this.pharmOrderItemInfoList = pharmOrderItemInfoList;
	}

	public List<String> getInfoMsgCodeList() {
		return infoMsgCodeList;
	}
	
	public List<String[]> getInfoMsgParamList() {
		return infoMsgParamList;
	}
	
	public List<String> getErrorMsgCodeList() {
		return errorMsgCodeList;
	}
	
	public List<String[]> getErrorMsgParamList() {
		return errorMsgParamList;
	}
	
	public MedOrderItemInfo getMedOrderItemInfoByItemNum(Integer itemNum) {
		for (MedOrderItemInfo mi : this.getMedOrderItemInfoList()) {
			if (mi.getItemNum().equals(itemNum)) {
				return mi;
			}
		}
		return null;
	}
	
	public PharmOrderItemInfo getPharmOrderItemInfoByItemNum(Integer itemNum) {
		for (PharmOrderItemInfo pi : this.getPharmOrderItemInfoList()) {
			if (pi.getItemNum().equals(itemNum)) {
				return pi;
			}
		}
		return null;
	}
	
	public void setPharmOrder(PharmOrder pharmOrder) {

		pharmOrder.setSpecCode(StringUtils.upperCase(specCode));
		pharmOrder.setWardCode(StringUtils.upperCase(wardCode));
		pharmOrder.setPatCatCode(StringUtils.upperCase(patCatCode));

		pharmOrder.setAwaitSfiFlag(awaitSfiFlag);
		pharmOrder.setPregnancyCheckFlag(pregnancyCheckFlag);
		
		pharmOrder.setHkid(hkid);
		pharmOrder.setOtherDoc(otherDoc);
	}
	
	public void setDispOrder(DispOrder dispOrder) {

		dispOrder.setSpecCode(StringUtils.upperCase(specCode));
		dispOrder.setWardCode(StringUtils.upperCase(wardCode));
		dispOrder.setPatCatCode(StringUtils.upperCase(patCatCode));

		dispOrder.setUrgentFlag(urgentFlag);
		dispOrder.setPrintLang(printLang);
	}
	
	public void bindPharmOrder(PharmOrder pharmOrder) {
		awaitSfiFlag = pharmOrder.getAwaitSfiFlag();
		pregnancyCheckFlag = pharmOrder.getPregnancyCheckFlag();
		hkid = pharmOrder.getHkid();
		otherDoc = pharmOrder.getOtherDoc();
		drsFlag = pharmOrder.getDrsFlag() != null ? pharmOrder.getDrsFlag() : false;
	}
	
	public void bindDispOrder(DispOrder dispOrder) {
		printLang = dispOrder.getPrintLang();
	}
	
	public void bindOneStopOrderInfo(OneStopOrderInfo oneStopOrderInfo, Workstore workstore) {
		specCode = oneStopOrderInfo.getPhsSpecCode();
		wardCode = oneStopOrderInfo.getPhsWardCode();
		patCatCode = oneStopOrderInfo.getPhsPatCat();
		doctorCode = StringUtils.trimToNull(oneStopOrderInfo.getDoctorCode());
		doctorName = StringUtils.trimToNull(oneStopOrderInfo.getDoctorName());
	}

	public List<ErrorInfo> getAlertErrorInfoList() {
		return alertErrorInfoList;
	}
	
	public void setAlertErrorInfoList(List<ErrorInfo> alertErrorInfoList) {
		this.alertErrorInfoList = alertErrorInfoList;
	}
	
	public void addInfoMsg(String code) {
		addInfoMsg(code, null);
	}
	
	public void addInfoMsg(String code, String[] param) {
		infoMsgCodeList.add(code);
		infoMsgParamList.add(param);
	}
	
	public void addErrorMsg(String code) {
		addErrorMsg(code, null);
	}

	public void addErrorMsg(String code, String[] param) {
		errorMsgCodeList.add(code);
		errorMsgParamList.add(param);
	}
	
	public boolean hasErrorMsg() {
		return !errorMsgCodeList.isEmpty();
	}
	
	public List<String> getDrsMsgCodeList() {
		return drsMsgCodeList;
	}

	public void setDrsMsgCodeList(List<String> drsMsgCodeList) {
		this.drsMsgCodeList = drsMsgCodeList;
	}

	public void addDrsMsg(String code) {
		drsMsgCodeList.add(code);
	}
	
	public AlertProfile getAlertProfile() {
		return alertProfile;
	}

	public void setAlertProfile(AlertProfile alertProfile) {
		this.alertProfile = alertProfile;
	}

	public void setSfiRefillCouponNum(String sfiRefillCouponNum) {
		this.sfiRefillCouponNum = sfiRefillCouponNum;
	}

	public String getSfiRefillCouponNum() {
		return sfiRefillCouponNum;
	}

	public void setSfiRefillCouponNumErrorString(
			String sfiRefillCouponNumErrorString) {
		this.sfiRefillCouponNumErrorString = sfiRefillCouponNumErrorString;
	}

	public String getSfiRefillCouponNumErrorString() {
		return sfiRefillCouponNumErrorString;
	}

	public void setMedOrderNumForCoupon(String medOrderNumForCoupon) {
		this.medOrderNumForCoupon = medOrderNumForCoupon;
	}

	public String getMedOrderNumForCoupon() {
		return medOrderNumForCoupon;
	}

	public void setSfiInvoiceClearanceStatus(ClearanceStatus sfiInvoiceClearanceStatus) {
		this.sfiInvoiceClearanceStatus = sfiInvoiceClearanceStatus;
	}

	public ClearanceStatus getSfiInvoiceClearanceStatus() {
		return sfiInvoiceClearanceStatus;
	}

	public Map<Integer, String> getDiscontinueMessageTlf() {
		return discontinueMessageTlf;
	}

	public void setDiscontinueMessageTlf(Map<Integer, String> discontinueMessageTlf) {
		this.discontinueMessageTlf = discontinueMessageTlf;
	}
	
	public long getMedOrderId() {
		return medOrderId;
	}

	public void setMedOrderId(long medOrderId) {
		this.medOrderId = medOrderId;
	}

	public long getMedOrderVersion() {
		return medOrderVersion;
	}

	public void setMedOrderVersion(long medOrderVersion) {
		this.medOrderVersion = medOrderVersion;
	}

	public boolean getReadOnlyDeleteRx() {
		return readOnlyDeleteRx;
	}

	public void setReadOnlyDeleteRx(boolean readOnlyDeleteRx) {
		this.readOnlyDeleteRx = readOnlyDeleteRx;
	}

	public PatientSfi getPatientSfi() {
		return patientSfi;
	}

	public void setPatientSfi(PatientSfi patientSfi) {
		this.patientSfi = patientSfi;
	}

	public boolean isDrsFlag() {
		return drsFlag;
	}

	public void setDrsFlag(boolean drsFlag) {
		this.drsFlag = drsFlag;
	}

	public EhrPatient getEhrPatient() {
		return ehrPatient;
	}

	public void setEhrPatient(EhrPatient ehrPatient) {
		this.ehrPatient = ehrPatient;
	}
}
