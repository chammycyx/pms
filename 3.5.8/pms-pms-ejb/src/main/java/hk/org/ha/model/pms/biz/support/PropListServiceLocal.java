package hk.org.ha.model.pms.biz.support;

import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.udt.support.PropType;
import hk.org.ha.model.pms.vo.support.PropInfoItem;

import java.util.List;

import javax.ejb.Local;

@Local
public interface PropListServiceLocal {
	
	List<Hospital> retrieveHospitalList();
	
	List<Workstore> retrieveWorkstoreList();
	
	List<OperationProfile> retrieveOperationProfileList(String hospCode, String workstoreCode);
	
	void retrievePropList(PropType propType, Hospital hospital, Long inOperationProfileId);
	
	void updatePropList(List<PropInfoItem> propList);
	
	void destroy();
	
}