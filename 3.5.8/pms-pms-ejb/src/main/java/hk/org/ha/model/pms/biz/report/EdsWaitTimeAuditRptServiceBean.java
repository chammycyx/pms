package hk.org.ha.model.pms.biz.report;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.util.JavaBeanResult;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.WaitTimeRptStat;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.prop.Prop;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderStatus;
import hk.org.ha.model.pms.udt.onestop.AllowCommentType;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.report.WaitTimeAuditRptResult;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.EdsDayOfWeekElapseTime;
import hk.org.ha.model.pms.vo.report.EdsElapseTime;
import hk.org.ha.model.pms.vo.report.EdsMonthlyElapseTime;
import hk.org.ha.model.pms.vo.report.EdsMonthlyWaitTimeRpt;
import hk.org.ha.model.pms.vo.report.EdsWaitTimeAuditRptHdr;
import hk.org.ha.model.pms.vo.report.EdsWaitTimeRpt;
import hk.org.ha.model.pms.vo.report.EdsWaitTimeRptHdr;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;

@Stateful
@Scope(ScopeType.SESSION)
@Name("edsWaitTimeAuditRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class EdsWaitTimeAuditRptServiceBean implements EdsWaitTimeAuditRptServiceLocal {

	@PersistenceContext
	private EntityManager em;
	
	@Logger
	private Log logger;
	
	private List<EdsWaitTimeRpt> edsWaitTimeRptList;
	
	private List<EdsMonthlyWaitTimeRpt> edsMonthlyWaitTimeRptList;
	
	@In
	private PrintAgentInf printAgent;
	
	@In
	private Workstore workstore;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	private DateFormat fileNameDf = new SimpleDateFormat("yyyyMMdd"); 
	
	private DateFormat yearMonthFormat = new SimpleDateFormat("MM/yyyy"); 
	
	private final String[] dayOfWeekArray = new String[] {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
	
	private static final String EDS_DAILY_WAIT_TIME_AUDIT_RPT = "EdsDailyWaitTimeAuditRpt";
	
	private static final String EDS_MONTHLY_WAIT_TIME_AUDIT_RPT = "EdsMonthlyWaitTimeAuditRpt";
	
	private String getRptFileName(String rptName, Date rptDate){
		return rptName+fileNameDf.format(rptDate);
	}
	
	@SuppressWarnings("unchecked")
	private List<WaitTimeRptStat> retrieveDailyWaitTimeRptStat(Date retrieveDate) {
		DateTime dateTime = new DateTime(retrieveDate);
		return em.createQuery(
				"select o from WaitTimeRptStat o " + // 20151019 index check : WaitTimeRptStat,hospCode,workstoreCode,year,month,day : UI_WAIT_TIME_RPT_STAT_01
				"where o.hospCode=:hospCode " +
				"and o.workstoreCode=:workstoreCode " +
				"and o.year=:year " +
				"and o.month=:month " +
				"and o.day=:day " +
				"order by o.hospCode, o.workstoreCode, o.year, o.month, o.day, o.hour")
				.setParameter("hospCode", workstore.getHospCode())
				.setParameter("workstoreCode", workstore.getWorkstoreCode())
				.setParameter("year", Integer.valueOf(dateTime.getYear()))
				.setParameter("month", Integer.valueOf(dateTime.getMonthOfYear()))
				.setParameter("day", Integer.valueOf(dateTime.getDayOfMonth()))
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	private List<EdsMonthlyElapseTime> retrieveMonthlyWaitTimeRptStat(Date retrieveDate) {
		DateTime dateTime = new DateTime(retrieveDate);
		
		Query query = em.createQuery(
				"select o.dayOfWeek, o.hour, avg(o.waitTime) as avgTime from WaitTimeRptStat o " + // 20151019 index check : WaitTimeRptStat,hospCode,workstoreCode,year,month,dayOfWeek,hour : I_WAIT_TIME_RPT_STAT_01
				"where o.hospCode = :hospCode " +
				" and o.workstoreCode = :workstoreCode " +
				" and o.year = :year " +
				" and o.month = :month " +
				" and o.dayOfWeek between :startDayOfWeek and :endDayOfWeek " +
				" and o.hour between :startHour and :endHour " +
				" group by o.dayOfWeek, o.hour " +
				" order by o.dayOfWeek, o.hour ");				

		JavaBeanResult.setQueryResultClass(query, EdsMonthlyElapseTime.class);
		
		return query.setParameter("hospCode", workstore.getHospCode())
		  .setParameter("workstoreCode", workstore.getWorkstoreCode())
		  .setParameter("year", Integer.valueOf(dateTime.getYear()))
		  .setParameter("month", Integer.valueOf(dateTime.getMonthOfYear()))
		  .setParameter("startDayOfWeek", Prop.REPORT_WAITINGTIME_AUDIT_STARTDAYOFWEEK.get())
		  .setParameter("endDayOfWeek", Prop.REPORT_WAITINGTIME_AUDIT_ENDDAYOFWEEK.get())
		  .setParameter("startHour", Prop.REPORT_WAITINGTIME_AUDIT_STARTHOUR.get())
		  .setParameter("endHour", Prop.REPORT_WAITINGTIME_AUDIT_ENDHOUR.get())
		  .getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<String> retrieveCurrentAvgWaitTime() {
		
		DateTime today = (new DateMidnight()).toDateTime();

		List<BigDecimal> waitTimeList = em.createNativeQuery(
				"SELECT time_diff('MI',t.create_date,d.check_date) " + // index check : DispOrder.workstore,checkDate : I_DISP_ORDER_06
				"FROM pharm_order p, disp_order d, ticket t, med_order m " +		
				"WHERE d.pharm_order_id = p.id " +
				"AND d.ticket_id = t.id " +
				"AND p.med_order_id = m.id " +
				"AND t.ticket_date = ?1 " +
				"AND d.check_date IS NOT NULL " +
				"AND d.org_disp_order_id = d.id " +
				"AND d.status not in (?2, ?3) " +
				"AND d.hosp_code = ?4 " +
				"AND d.workstore_code = ?5 " +
				"AND d.admin_status = ?6 " +
				"AND d.suspend_code is null " +
				"AND p.status not in (?7,?8) " +
				"AND p.allow_comment_type = ?9 " +
				"AND p.allow_comment_before_flag <> 1 " +
				"AND p.admin_status = ?10 " +
				"AND m.order_type = 'O' " +
				"AND time_diff('MI',t.create_date,d.check_date) <= 180 " +
				"AND rownum <= ?11 ")				
		.setParameter(1, today.toDate())
		.setParameter(2, DispOrderStatus.Deleted.getDataValue())
		.setParameter(3, DispOrderStatus.SysDeleted.getDataValue())
		.setParameter(4, workstore.getHospCode())
		.setParameter(5, workstore.getWorkstoreCode())
		.setParameter(6, DispOrderAdminStatus.Normal.getDataValue())
		.setParameter(7, PharmOrderStatus.Deleted.getDataValue())
		.setParameter(8, PharmOrderStatus.SysDeleted.getDataValue())
		.setParameter(9, AllowCommentType.None.getDataValue())
		.setParameter(10, PharmOrderAdminStatus.Normal.getDataValue())
		.setParameter(11, Prop.REPORT_WAITINGTIME_CURRENT_CRITERIA_ORDER_MAX.get())
		.getResultList();
		
		List<String> resultList = new ArrayList<String>();
		
		resultList.add(String.valueOf(Prop.REPORT_WAITINGTIME_CURRENT_CRITERIA_ORDER_MAX.get().intValue()));
		
		if (waitTimeList.size() > 0) {
			
			long totalTime = 0L;
			for (BigDecimal waitTime : waitTimeList) {
				totalTime += waitTime.longValue();
			}						
			
			long avgTime = totalTime/waitTimeList.size();
			
			resultList.add(avgTime/60 + " hr. " + avgTime%60 + " min. ");	
		} else {
			resultList.add("");
		}
		return resultList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Date> retrieveAvailableMonthForMonthlyWaitTimeAuditRpt() {
		DateTime endDate = (new DateTime()).minusMonths(1);
		DateTime startDate = endDate.minusMonths(Prop.REPORT_WAITINGTIME_AUDIT_CRITERIA_MONTH_DURATION.get().intValue());
		List<Date> dateList = em.createQuery(
				"select distinct s.batchDate " + // 20151019 index check : WaitTimeRptStat,hospCode,workstoreCode,batchDate : I_WAIT_TIME_RPT_STAT_02
				"from WaitTimeRptStat s " +
				"where s.hospCode = :hospCode " +
				"and s.workstoreCode = :workstoreCode " +
				"and s.dayOfWeek between :startDayOfWeek and :endDayOfWeek " +
				"and s.hour between :startHour and :endHour " +
				"and s.batchDate between :startBatchDate and :endBatchDate " +
				"order by s.batchDate desc")
				.setParameter("hospCode", workstore.getHospCode())
				.setParameter("workstoreCode", workstore.getWorkstoreCode())
				.setParameter("startDayOfWeek", Prop.REPORT_WAITINGTIME_AUDIT_STARTDAYOFWEEK.get())
				.setParameter("endDayOfWeek", Prop.REPORT_WAITINGTIME_AUDIT_ENDDAYOFWEEK.get())
				.setParameter("startHour", Prop.REPORT_WAITINGTIME_AUDIT_STARTHOUR.get())
				.setParameter("endHour", Prop.REPORT_WAITINGTIME_AUDIT_ENDHOUR.get())
				.setParameter("startBatchDate", startDate.toDate())
				.setParameter("endBatchDate", endDate.toDate())
				.getResultList();
		
		List<Date> yearMonthList = new ArrayList<Date>();
		Map<String,Boolean> yearMonthMap = new HashMap<String,Boolean>();
		for (Date date:dateList) {
			if (yearMonthMap.get(yearMonthFormat.format(date)) == null) {
				yearMonthList.add(date);
				yearMonthMap.put(yearMonthFormat.format(date), Boolean.TRUE);
			}
		}
		return yearMonthList;
	}
	
	public WaitTimeAuditRptResult retrieveDailyWaitTimeAuditRpt(Date retrieveDate) {
		List<EdsElapseTime> edsWaitTimeRptDtlList = new ArrayList<EdsElapseTime>();
		EdsWaitTimeRpt edsWaitTimeRpt = new EdsWaitTimeRpt();
		Map<Integer, WaitTimeRptStat> resultMap = new HashMap<Integer, WaitTimeRptStat>();
		
		List<WaitTimeRptStat> waitTimeRptStatList = retrieveDailyWaitTimeRptStat(retrieveDate);
		logger.info("Number of record(s):#0", waitTimeRptStatList.size());
		
		if (waitTimeRptStatList.size() == 0) {
			return WaitTimeAuditRptResult.NoRecordFound;
		}
		
		for (WaitTimeRptStat stat : waitTimeRptStatList) {
			resultMap.put(stat.getHour(), stat);
		}
		
		boolean notApplicable = true;
		
		for (int i = Prop.REPORT_WAITINGTIME_AUDIT_STARTHOUR.get().intValue(); i < Prop.REPORT_WAITINGTIME_AUDIT_ENDHOUR.get().intValue(); i++) {
			EdsElapseTime edsWaitTimeRptDtl = null;
			String startTime = StringUtils.leftPad(String.valueOf(i),2,'0');
			String endTime = StringUtils.leftPad(String.valueOf(i+1),2,'0');
			
			if (resultMap.get(Integer.valueOf(i)) != null) {
				notApplicable = false;
				edsWaitTimeRptDtl = new EdsElapseTime( startTime + "-" + endTime, resultMap.get(Integer.valueOf(i)).getWaitTime());
			}
			else {
				edsWaitTimeRptDtl = new EdsElapseTime( startTime + "-" + endTime, 0);
			}

			edsWaitTimeRptDtlList.add(edsWaitTimeRptDtl);
		}
		
		if (notApplicable) {
			return WaitTimeAuditRptResult.NotApplicable;
		}
		
		edsWaitTimeRpt.setEdsWaitTimeRptDtlList(edsWaitTimeRptDtlList);
		
		EdsWaitTimeRptHdr edsWaitTimeRptHdr = new EdsWaitTimeRptHdr();
	
		OperationProfile op = em.find(OperationProfile.class, workstore.getOperationProfileId());
		
		edsWaitTimeRptHdr.setActiveProfileName(op.getName());
		edsWaitTimeRptHdr.setHospCode(workstore.getHospCode());
		edsWaitTimeRptHdr.setWorkstoreCode(workstore.getWorkstoreCode());
		edsWaitTimeRptHdr.setDate(retrieveDate);
		edsWaitTimeRpt.setEdsWaitTimeRptHdr(edsWaitTimeRptHdr);
		
		edsWaitTimeRptList = Arrays.asList(edsWaitTimeRpt);
		
		return WaitTimeAuditRptResult.RecordFound;
	}
	
	public Boolean retrieveMonthlyWaitTimeAuditRpt(Date retrieveDate) {
		List<EdsDayOfWeekElapseTime> edsDayOfWeekElapseTimeRptDtlList = new ArrayList<EdsDayOfWeekElapseTime>();
		EdsMonthlyWaitTimeRpt edsMonthlyWaitTimeRpt = new EdsMonthlyWaitTimeRpt();
		Map<String, EdsMonthlyElapseTime> resultMap = new HashMap<String, EdsMonthlyElapseTime>();
		
		List<EdsMonthlyElapseTime> monthlyElapseTimeList = retrieveMonthlyWaitTimeRptStat(retrieveDate);
		logger.info("Number of record(s):#0", monthlyElapseTimeList.size());
		
		if (monthlyElapseTimeList.size() == 0) {
			return Boolean.FALSE;
		}
		
		for (EdsMonthlyElapseTime elapseTime : monthlyElapseTimeList) {
			logger.info("elapseTime.getDayOfWeek():#0, elapseTime.getHour():#1", elapseTime.getDayOfWeek(), elapseTime.getHour());
			resultMap.put(String.valueOf(elapseTime.getDayOfWeek().intValue()) + String.valueOf(elapseTime.getHour().intValue()), elapseTime);
		}
		
		EdsMonthlyElapseTime monthlyElapseTime;
		String dayOfWeekDesc;
		for (int i=Prop.REPORT_WAITINGTIME_AUDIT_STARTDAYOFWEEK.get().intValue(); i<=Prop.REPORT_WAITINGTIME_AUDIT_ENDDAYOFWEEK.get().intValue(); i++) {
			dayOfWeekDesc = dayOfWeekArray[i-1];
			for (int hr = Prop.REPORT_WAITINGTIME_AUDIT_STARTHOUR.get().intValue(); hr < Prop.REPORT_WAITINGTIME_AUDIT_ENDHOUR.get().intValue(); hr++) {
				EdsDayOfWeekElapseTime edsWaitTimeRptDtl = null;
				String startTime = StringUtils.leftPad(String.valueOf(hr),2,'0');
				String endTime = StringUtils.leftPad(String.valueOf(hr+1),2,'0');
				
				monthlyElapseTime = resultMap.get(String.valueOf(i)+String.valueOf(hr));
				
				if (monthlyElapseTime != null) {
					edsWaitTimeRptDtl = new EdsDayOfWeekElapseTime(dayOfWeekDesc, startTime + "-" + endTime, monthlyElapseTime.getAvgTime());
				}
				else {
					edsWaitTimeRptDtl = new EdsDayOfWeekElapseTime(dayOfWeekDesc, startTime + "-" + endTime, 0);
				}

				edsDayOfWeekElapseTimeRptDtlList.add(edsWaitTimeRptDtl);
			}
		}
		
		EdsWaitTimeAuditRptHdr edsWaitTimeAuditRptHdr = new EdsWaitTimeAuditRptHdr();
		edsMonthlyWaitTimeRpt.setEdsWaitTimeAuditRptHdr(edsWaitTimeAuditRptHdr);
		edsMonthlyWaitTimeRpt.setEdsMonthlyWaitTimeRptDtlList(edsDayOfWeekElapseTimeRptDtlList);
		
		OperationProfile op = em.find(OperationProfile.class, workstore.getOperationProfileId());
		
		edsWaitTimeAuditRptHdr.setActiveProfileName(op.getName());
		edsWaitTimeAuditRptHdr.setHospCode(workstore.getHospCode());
		edsWaitTimeAuditRptHdr.setWorkstoreCode(workstore.getWorkstoreCode());
		edsWaitTimeAuditRptHdr.setDate(retrieveDate);
		
		edsMonthlyWaitTimeRptList = Arrays.asList(edsMonthlyWaitTimeRpt);
		
		return Boolean.TRUE;
	}
	
	public void generateEdsDailyWaitTimeAuditRpt() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		printAgent.renderAndRedirect(new RenderJob(
				EDS_DAILY_WAIT_TIME_AUDIT_RPT, 
				new PrintOption(), 
				parameters, 
				edsWaitTimeRptList));
	}

	public void printEdsDailyWaitTimeAuditRpt(){
		printAgent.renderAndPrint(new RenderAndPrintJob(
				EDS_DAILY_WAIT_TIME_AUDIT_RPT,
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				new HashMap<String, Object>(), 
				edsWaitTimeRptList));
	}
	
	public void exportEdsDailyWaitTimeAuditRpt() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("hospCode", workstore.getHospCode());
		parameters.put("workstoreCode", workstore.getWorkstoreCode());
		
		String filename = getRptFileName(EDS_DAILY_WAIT_TIME_AUDIT_RPT, new Date());
		
		PrintOption po = new PrintOption();
		po.setDocType(ReportProvider.XLS);
		printAgent.renderAndRedirect(new RenderJob(
				"EdsDailyWaitTimeAuditXls", 
				po, 
				parameters, 
				edsWaitTimeRptList.get(0).getEdsWaitTimeRptDtlList()), filename);

	}
	
	public void generateEdsMonthlyWaitTimeAuditRpt() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		printAgent.renderAndRedirect(new RenderJob(
				EDS_MONTHLY_WAIT_TIME_AUDIT_RPT, 
				new PrintOption(), 
				parameters, 
				edsMonthlyWaitTimeRptList));
	}
	
	public void printEdsMonthlyWaitTimeAuditRpt(){
		printAgent.renderAndPrint(new RenderAndPrintJob(
				EDS_MONTHLY_WAIT_TIME_AUDIT_RPT,
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				new HashMap<String, Object>(), 
				edsMonthlyWaitTimeRptList));
	}
	
	public void exportEdsMonthlyWaitTimeAuditRpt() {	
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("hospCode", workstore.getHospCode());
		parameters.put("workstoreCode", workstore.getWorkstoreCode());
		
		String filename = getRptFileName(EDS_MONTHLY_WAIT_TIME_AUDIT_RPT, new Date());
		
		PrintOption po = new PrintOption();
		po.setDocType(ReportProvider.XLS);
		printAgent.renderAndRedirect(new RenderJob(
				"EdsMonthlyWaitTimeAuditXls", 
				po, 
				parameters, 
				edsMonthlyWaitTimeRptList.get(0).getEdsMonthlyWaitTimeRptDtlList()), filename);

	}
	
	@Remove
	public void destroy() {
		if (edsMonthlyWaitTimeRptList != null) {
			edsMonthlyWaitTimeRptList = null;
		}
		if (edsWaitTimeRptList != null) {
			edsWaitTimeRptList = null;
		}
	}
}
