package hk.org.ha.model.pms.vo.label;

import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.printing.PrintType;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.machine.AtdpsMessage;
import hk.org.ha.model.pms.vo.machine.AtdpsMessageContent;
import hk.org.ha.model.pms.vo.printing.PrintOption;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "MpDispLabel")
@ExternalizedBean(type=DefaultExternalizer.class)
public class MpDispLabel extends BaseLabel {
	
	private static final long serialVersionUID = 1L;

	@XmlElement(required = true)	
	private String userCode;
	
	@XmlElement(required = true)	
	private String maskedUserCode;
	
	@XmlElement(required = true)	
	private String floatingDrugName;
	
	@XmlElement(required = true)	
	private boolean alertFlag;
	
	@XmlElement(required = true)	
	private ActionStatus actionStatus;
	
	@XmlElement(required = true)	
	private boolean privateFlag;
	
	@XmlElement(required = true)	
	private boolean fcFlag;
	
	@XmlElement(required = true)	
	private boolean singleOrderFlag;

	@XmlElement(required = true)	
	private boolean newOrderFlag;

	@XmlElement(required = true)	
	private boolean replenishFlag;
	
	@XmlElement(required = true)	
	private boolean urgentFlag;
	
	@XmlElement(required = true)	
	private String batchNum;
	
	@XmlElement(required = true)	
	private String qrCodeXml;
	
	@XmlElement(required = true)
	private String hkid;
	
	@XmlElement(required = true)
	private boolean showInstructionFlag;
	
	@XmlElement(required = true)
	private PrintLang printLang;

	@XmlElement(required = true)
	private String engMealTimeDesc;
	
	@XmlElement(required = true)
	private String chiMealTimeDesc;
	
	@XmlElement(required = false)
	private boolean refrigerateFlag;
	
	@XmlElement(required = false)
	private Boolean reDispFlag;
	
	@XmlElement(required = false)
	private Boolean wardStockFlag;
	
	@XmlElement(required = false)
	private List<AtdpsMessageContent> atdpsInfoEngList;
	
	@XmlElement(required = false)
	private List<AtdpsMessageContent> atdpsInfoChiList;
	
	@XmlElement(required = false)
	private List<AtdpsMessage> atdpsXmlList;
	
	@XmlElement(required = true)
	private String formCode;
	
	@XmlElement(required = false)
	private String patOverrideReason;

	@XmlElement(required = false)
	private String ddiOverrideReason;

	@XmlTransient
	private Integer itemNum;
	
	@XmlTransient
	private String drugOrderTlf;
	
	@XmlTransient
	private boolean reprintFlag = false;
	
	@XmlTransient
	private boolean allowReprintFlag = false;
	
	@XmlTransient
	private MpDispLabelQrCode mpDispLabelQrCode;
	
	@XmlTransient
	private boolean homeLeaveFlag = false;
	
	@XmlTransient
	private boolean canPrintInstructionFlag = true;
	
	private transient Integer totalNumOfLabel;
	
	private transient String itemType;

	@XmlTransient	
	private Long orderEditRank;
	
	@XmlTransient	
	private Long orderLineRank;
	
	@XmlTransient
	private String exception;
	
	@XmlTransient
	private boolean allowAtdpsFlag = false;
	
	@XmlTransient
	private boolean pivasFlag = false;
	
	@XmlTransient
	private Long deliveryItemId;
	
	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public PrintLang getPrintLang() {
		return printLang;
	}

	public void setPrintLang(PrintLang printLang) {
		this.printLang = printLang;
	}

	public Integer getTotalNumOfLabel() {
		return totalNumOfLabel;
	}

	public void setTotalNumOfLabel(Integer totalNumOfLabel) {
		this.totalNumOfLabel = totalNumOfLabel;
	}

	public MpDispLabel() {
		urgentFlag = Boolean.FALSE;
	}	
	
	public String getFloatingDrugName() {
		return floatingDrugName;
	}

	public void setFloatingDrugName(String floatingDrugName) {
		this.floatingDrugName = floatingDrugName;
	}

	public boolean getAlertFlag() {
		return alertFlag;
	}

	public void setAlertFlag(boolean alertFlag) {
		this.alertFlag = alertFlag;
	}

	public boolean getSingleOrderFlag() {
		return singleOrderFlag;
	}

	public void setSingleOrderFlag(boolean singleOrderFlag) {
		this.singleOrderFlag = singleOrderFlag;
	}

	public boolean getNewOrderFlag() {
		return newOrderFlag;
	}

	public void setNewOrderFlag(boolean newOrderFlag) {
		this.newOrderFlag = newOrderFlag;
	}

	public boolean getUrgentFlag() {
		return urgentFlag;
	}

	public void setUrgentFlag(boolean urgentFlag) {
		this.urgentFlag = urgentFlag;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public void setQrCodeXml(String qrCodeXml) {
		this.qrCodeXml = qrCodeXml;
	}

	public String getQrCodeXml() {
		return qrCodeXml;
	}

	public void setActionStatus(ActionStatus actionStatus) {
		this.actionStatus = actionStatus;
	}

	public ActionStatus getActionStatus() {
		return actionStatus;
	}

	public boolean isPrivateFlag() {
		return privateFlag;
	}

	public void setPrivateFlag(boolean privateFlag) {
		this.privateFlag = privateFlag;
	}

	public boolean isFcFlag() {
		return fcFlag;
	}

	public void setFcFlag(boolean fcFlag) {
		this.fcFlag = fcFlag;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getHkid() {
		return hkid;
	}	

	public void setShowInstructionFlag(boolean showInstructionFlag) {
		this.showInstructionFlag = showInstructionFlag;
	}

	public boolean getShowInstructionFlag() {
		return showInstructionFlag;
	}
	
	public String getEngMealTimeDesc() {
		return engMealTimeDesc;
	}

	public void setEngMealTimeDesc(String engMealTimeDesc) {
		this.engMealTimeDesc = engMealTimeDesc;
	}

	public String getChiMealTimeDesc() {
		return chiMealTimeDesc;
	}

	public void setChiMealTimeDesc(String chiMealTimeDesc) {
		this.chiMealTimeDesc = chiMealTimeDesc;
	}

	public int getCharLimit() {
		int charLimit;
		PrintOption printOption = super.getPrintOption();
		if (printOption.getPrintType() == PrintType.Normal) {
			if (printOption.getPrintLang() == PrintLang.Eng) {
				charLimit = 42;
			} else {
				charLimit = 35;
			}
		} else {
			if (printOption.getPrintLang() == PrintLang.Eng) {
				charLimit = 30;
			} else {
				charLimit = 25;
			}
		}
		return charLimit;
	}
	
	public OrderType getOrderType() {
		return OrderType.InPatient;
	}

	public void setRefrigerateFlag(boolean refrigerateFlag) {
		this.refrigerateFlag = refrigerateFlag;
	}

	public boolean getRefrigerateFlag() {
		return refrigerateFlag;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public Integer getItemNum() {
		return itemNum;
	}
	
	public String getDrugOrderTlf() {
		return drugOrderTlf;
	}

	public void setDrugOrderTlf(String drugOrderTlf) {
		this.drugOrderTlf = drugOrderTlf;
	}

	public void setReprintFlag(boolean reprintFlag) {
		this.reprintFlag = reprintFlag;
	}

	public boolean isReprintFlag() {
		return reprintFlag;
	}

	public void setAllowReprintFlag(boolean allowReprintFlag) {
		this.allowReprintFlag = allowReprintFlag;
	}

	public boolean isAllowReprintFlag() {
		return allowReprintFlag;
	}

	public void setMpDispLabelQrCode(MpDispLabelQrCode mpDispLabelQrCode) {
		this.mpDispLabelQrCode = mpDispLabelQrCode;
	}

	public MpDispLabelQrCode getMpDispLabelQrCode() {
		return mpDispLabelQrCode;
	}

	public boolean isHomeLeaveFlag() {
		return homeLeaveFlag;
	}

	public void setHomeLeaveFlag(boolean homeLeaveFlag) {
		this.homeLeaveFlag = homeLeaveFlag;
	}

	public void setCanPrintInstructionFlag(boolean canPrintInstructionFlag) {
		this.canPrintInstructionFlag = canPrintInstructionFlag;
	}

	public boolean isCanPrintInstructionFlag() {
		return canPrintInstructionFlag;
	}
	
	public void setException(String exception) {
		this.exception = exception;
	}

	public String getException() {
		return exception;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getItemType() {
		return itemType;
	}

	public Boolean getReDispFlag() {
		return reDispFlag;
	}

	public void setReDispFlag(Boolean reDispFlag) {
		this.reDispFlag = reDispFlag;
	}

	public Boolean getWardStockFlag() {
		return wardStockFlag;
	}

	public void setWardStockFlag(Boolean wardStockFlag) {
		this.wardStockFlag = wardStockFlag;
	}

	public void setOrderEditRank(Long orderEditRank) {
		this.orderEditRank = orderEditRank;
	}

	public Long getOrderEditRank() {
		return orderEditRank;
	}

	public void setOrderLineRank(Long orderLineRank) {
		this.orderLineRank = orderLineRank;
	}

	public Long getOrderLineRank() {
		return orderLineRank;
	}

	public void setAllowAtdpsFlag(boolean allowAtdpsFlag) {
		this.allowAtdpsFlag = allowAtdpsFlag;
	}

	public boolean isAllowAtdpsFlag() {
		return allowAtdpsFlag;
	}

	public void setAtdpsInfoEngList(List<AtdpsMessageContent> atdpsInfoEngList) {
		this.atdpsInfoEngList = atdpsInfoEngList;
	}

	public List<AtdpsMessageContent> getAtdpsInfoEngList() {
		return atdpsInfoEngList;
	}

	public void setAtdpsInfoChiList(List<AtdpsMessageContent> atdpsInfoChiList) {
		this.atdpsInfoChiList = atdpsInfoChiList;
	}

	public List<AtdpsMessageContent> getAtdpsInfoChiList() {
		return atdpsInfoChiList;
	}

	public void setReplenishFlag(boolean replenishFlag) {
		this.replenishFlag = replenishFlag;
	}

	public boolean isReplenishFlag() {
		return replenishFlag;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	public String getFormCode() {
		return formCode;
	}

	public String getPatOverrideReason() {
		return patOverrideReason;
	}

	public void setPatOverrideReason(String patOverrideReason) {
		this.patOverrideReason = patOverrideReason;
	}

	public String getDdiOverrideReason() {
		return ddiOverrideReason;
	}

	public void setDdiOverrideReason(String ddiOverrideReason) {
		this.ddiOverrideReason = ddiOverrideReason;
	}
	
	public boolean isPivasFlag() {
		return pivasFlag;
	}

	public void setPivasFlag(boolean pivasFlag) {
		this.pivasFlag = pivasFlag;
	}

	public void setDeliveryItemId(Long deliveryItemId) {
		this.deliveryItemId = deliveryItemId;
	}

	public Long getDeliveryItemId() {
		return deliveryItemId;
	}

	public void setAtdpsXmlList(List<AtdpsMessage> atdpsXmlList) {
		this.atdpsXmlList = atdpsXmlList;
	}

	public List<AtdpsMessage> getAtdpsXmlList() {
		return atdpsXmlList;
	}

	public void setMaskedUserCode(String maskedUserCode) {
		this.maskedUserCode = maskedUserCode;
	}

	public String getMaskedUserCode() {
		return maskedUserCode;
	}
}