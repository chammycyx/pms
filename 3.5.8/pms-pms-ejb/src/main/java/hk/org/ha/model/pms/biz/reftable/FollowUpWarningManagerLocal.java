package hk.org.ha.model.pms.biz.reftable;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.FollowUpWarning;

import java.util.List;

import javax.ejb.Local;

@Local
public interface FollowUpWarningManagerLocal {
	
	void syncFollowUpWarning(List<String> itemCodeList);
	
	List<FollowUpWarning> retrieveFollowUpWarningList(Workstore workstore);
}
