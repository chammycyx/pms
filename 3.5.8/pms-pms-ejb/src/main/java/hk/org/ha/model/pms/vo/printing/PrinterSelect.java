package hk.org.ha.model.pms.vo.printing;

import java.io.Serializable;

import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;

public class PrinterSelect implements Serializable {
		
	private static final long serialVersionUID = 1L;

	private Workstation printerWorkstation;
	
	private Workstation defaultPrinterWorkstation;

	private PrintDocType printDocType;

	private int copies = 1;
	private boolean reverse = false;
	
	public PrinterSelect() {		
	}
	
	public PrinterSelect(Workstation printerWorkstation) {
		this(printerWorkstation, PrintDocType.Report);
	}

	public PrinterSelect(Workstation printerWorkstation, PrintDocType printDocType) {
		this.printerWorkstation = printerWorkstation;
		this.printDocType = printDocType;
	}

	public PrinterSelect(PrinterSelect o) {
		this.printerWorkstation = o.printerWorkstation;
		this.defaultPrinterWorkstation = o.defaultPrinterWorkstation;
		this.printDocType = o.printDocType;
		this.copies = o.copies;
		this.reverse = o.reverse;
	}
	
	public Workstation getPrinterWorkstation() {
		return printerWorkstation;
	}

	public void setPrinterWorkstation(Workstation printerWorkstation) {
		this.printerWorkstation = printerWorkstation;
	}

	public Workstation getDefaultPrinterWorkstation() {
		return defaultPrinterWorkstation;
	}

	public void setDefaultPrinterWorkstation(Workstation defaultPrinterWorkstation) {
		this.defaultPrinterWorkstation = defaultPrinterWorkstation;
	}
	
	public PrintDocType getPrintDocType() {
		return printDocType;
	}

	public void setPrintDocType(PrintDocType printDocType) {
		this.printDocType = printDocType;
	}

	public int getCopies() {
		return copies;
	}

	public void setCopies(int copies) {
		this.copies = copies;
	}

	public boolean isReverse() {
		return reverse;
	}

	public void setReverse(boolean reverse) {
		this.reverse = reverse;
	}
	
	public Workstation getResolvedPrinterWorkstation() {
		return (printerWorkstation != null) ? printerWorkstation : defaultPrinterWorkstation;
	}
	
}