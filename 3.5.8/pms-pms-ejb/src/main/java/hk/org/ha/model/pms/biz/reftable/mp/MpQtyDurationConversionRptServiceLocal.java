package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.model.pms.dms.persistence.PmsIpDurationConv;
import hk.org.ha.model.pms.dms.persistence.PmsIpQtyConv;
import hk.org.ha.model.pms.dms.persistence.PmsQtyConversionInsu;

import java.util.List;

import javax.ejb.Local;
 
@Local
public interface MpQtyDurationConversionRptServiceLocal {
	
	List<PmsIpQtyConv> retrieveMpQtyConversionListForReport();

	List<PmsIpDurationConv> retrieveMpDurationConversionListForReport();
	
	List<PmsQtyConversionInsu> retrievePmsQtyConversionInsuListForReport();
	
	void printMpQtyDurationConversionRpt();
	
	void retrieveMpQtyConversionRptList();
	
	void retrieveMpDurConversionRptList();
	
	void retrieveQtyConversionInsuRptList();
	
	void exportMpQtyConversionRpt();
	
	void exportMpDurationConversionRpt();
	
	void exportPmsQtyConversionInsuRpt();

	void destroy();
	
} 
 