package hk.org.ha.model.pms.vo.checkissue;

import java.io.Serializable;


public class CheckIssueDocType implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String sfiInvoice;
	
	private String sfiRefillCoupon;
	
	private String capdVoucher;
	
	private String refillCoupon;

	public String getSfiInvoice() {
		return sfiInvoice;
	}

	public void setSfiInvoice(String sfiInvoice) {
		this.sfiInvoice = sfiInvoice;
	}

	public String getSfiRefillCoupon() {
		return sfiRefillCoupon;
	}

	public void setSfiRefillCoupon(String sfiRefillCoupon) {
		this.sfiRefillCoupon = sfiRefillCoupon;
	}

	public String getCapdVoucher() {
		return capdVoucher;
	}

	public void setCapdVoucher(String capdVoucher) {
		this.capdVoucher = capdVoucher;
	}

	public String getRefillCoupon() {
		return refillCoupon;
	}

	public void setRefillCoupon(String refillCoupon) {
		this.refillCoupon = refillCoupon;
	}
	
	

}