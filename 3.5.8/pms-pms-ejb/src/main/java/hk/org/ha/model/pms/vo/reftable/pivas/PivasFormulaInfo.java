package hk.org.ha.model.pms.vo.reftable.pivas;

import hk.org.ha.model.pms.dms.persistence.DmDiluent;
import hk.org.ha.model.pms.dms.persistence.DmSite;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PivasFormulaInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<DmSite> whiteList;
	
	private List<DmSite> greyList;
	
	private List<DmDiluent> dmDiluentList;
	
	private List<String> pivasDosageUnitList;
	
	private List<String> strengthList;

	public List<DmSite> getWhiteList() {
		return whiteList;
	}

	public void setWhiteList(List<DmSite> whiteList) {
		this.whiteList = whiteList;
	}

	public List<DmSite> getGreyList() {
		return greyList;
	}

	public void setGreyList(List<DmSite> greyList) {
		this.greyList = greyList;
	}

	public List<DmDiluent> getDmDiluentList() {
		return dmDiluentList;
	}

	public void setDmDiluentList(List<DmDiluent> dmDiluentList) {
		this.dmDiluentList = dmDiluentList;
	}

	public List<String> getPivasDosageUnitList() {
		return pivasDosageUnitList;
	}

	public void setPivasDosageUnitList(List<String> pivasDosageUnitList) {
		this.pivasDosageUnitList = pivasDosageUnitList;
	}

	public List<String> getStrengthList() {
		return strengthList;
	}

	public void setStrengthList(List<String> strengthList) {
		this.strengthList = strengthList;
	}
}
