package hk.org.ha.model.pms.biz.alert;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.alert.AlertProfileException;
import hk.org.ha.model.pms.persistence.PatientEntity;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.vo.alert.AlertProfileHistory;
import hk.org.ha.service.pms.asa.interfaces.alert.AlertServiceJmsRemote;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.security.Identity;

@Stateful
@Scope(ScopeType.SESSION)
@Name("alertProfileService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class AlertProfileServiceBean implements AlertProfileServiceLocal {

	@In
	private AlertServiceJmsRemote alertServiceProxy;
	
	@In
	private Workstation workstation;

	@In
	private Identity identity;

	public AlertProfileHistory retrieveAlertProfileHistory(String caseNum, String patHospCode, PatientEntity patientEntity) throws AlertProfileException {
		
		return alertServiceProxy.retrieveAlertProfileHistory(
				patientEntity,
				caseNum,
				patHospCode,
				identity.getCredentials().getUsername(),
				workstation.getHostName()
			);
	}

	@Remove
	public void destroy() {
	}
}
