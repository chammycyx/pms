package hk.org.ha.model.pms.biz.checkissue;
import hk.org.ha.model.pms.udt.machine.IqdsAction;
import hk.org.ha.model.pms.vo.machine.IqdsMessageContent;

import java.util.List;

import javax.ejb.Local;

@Local
public interface IqdsSenderLocal {

	void sendTicketToIqds(String hospCode, String workstoreCode, String ticketNum, Integer issueWindowNum, IqdsAction action);
	
	void sendTicketToIqds(List<IqdsMessageContent> iqdsMessageContentList);

}
