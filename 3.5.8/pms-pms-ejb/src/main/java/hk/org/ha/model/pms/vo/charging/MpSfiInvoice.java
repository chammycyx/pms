package hk.org.ha.model.pms.vo.charging;

import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class MpSfiInvoice implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private MedProfile medProfile;
	
	private Patient patient; 
	
	private MedCase medCase;
	
	private Date invoiceDate; 
	
	private InvoiceStatus status;
	
	private String invoiceNum;
	
	private boolean allowPatTypeEdit;
	
	private boolean issueNewMpSfiInvoice;
	
	private DispOrderAdminStatus dispOrderAdminStatus;
 	
	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public InvoiceStatus getStatus() {
		return status;
	}

	public void setStatus(InvoiceStatus status) {
		this.status = status;
	}

	private List<MpSfiInvoiceItem> mpSfiInvoiceItemList;

	public MedProfile getMedProfile() {
		return medProfile;
	}

	public void setMedProfile(MedProfile medProfile) {
		this.medProfile = medProfile;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public MedCase getMedCase() {
		return medCase;
	}

	public void setMedCase(MedCase medCase) {
		this.medCase = medCase;
	}

	public List<MpSfiInvoiceItem> getMpSfiInvoiceItemList() {
		return mpSfiInvoiceItemList;
	}

	public void setMpSfiInvoiceItemList(List<MpSfiInvoiceItem> mpSfiInvoiceItemList) {
		this.mpSfiInvoiceItemList = mpSfiInvoiceItemList;
	}

	public void setInvoiceNum(String invoiceNum) {
		this.invoiceNum = invoiceNum;
	}

	public String getInvoiceNum() {
		return invoiceNum;
	}

	public void setAllowPatTypeEdit(boolean allowPatTypeEdit) {
		this.allowPatTypeEdit = allowPatTypeEdit;
	}

	public boolean isAllowPatTypeEdit() {
		return allowPatTypeEdit;
	}

	public void setDispOrderAdminStatus(DispOrderAdminStatus dispOrderAdminStatus) {
		this.dispOrderAdminStatus = dispOrderAdminStatus;
	}

	public DispOrderAdminStatus getDispOrderAdminStatus() {
		return dispOrderAdminStatus;
	}

	public void setIssueNewMpSfiInvoice(boolean issueNewMpSfiInvoice) {
		this.issueNewMpSfiInvoice = issueNewMpSfiInvoice;
	}

	public boolean isIssueNewMpSfiInvoice() {
		return issueNewMpSfiInvoice;
	}
}
