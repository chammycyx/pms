package hk.org.ha.model.pms.cache;
import hk.org.ha.model.pms.dms.vo.DrugRouteCriteria;
import hk.org.ha.model.pms.vo.rx.Site;

import java.util.List;

import javax.ejb.Local;

@Local
public interface MpSiteCacherInf {	
	List<Site> getIpSiteListByDrugRouteCriteria(DrugRouteCriteria criteria);
	List<Site> getDischargeSiteListByDrugRouteCriteria(DrugRouteCriteria criteria, Boolean excludeParenteralFlag);
	List<Site> getManualEntrySiteListByDrugRouteCriteria(DrugRouteCriteria criteria);
}
