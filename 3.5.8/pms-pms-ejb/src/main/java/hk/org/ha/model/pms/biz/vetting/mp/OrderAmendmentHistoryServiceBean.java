package hk.org.ha.model.pms.biz.vetting.mp;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.util.QueryUtils;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.vo.label.MpDispLabel;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Local
@Scope(ScopeType.SESSION)
@Name("orderAmendmentHistoryService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class OrderAmendmentHistoryServiceBean implements OrderAmendmentHistoryServiceLocal {

	private static final String DELIVERY_ITEM_QUERY_HINT_FETCH_1 = "o.delivery";
	
	private static JaxbWrapper<MpDispLabel> MP_DISP_LABEL_JAXB_WRAPPER = JaxbWrapper.instance("hk.org.ha.model.pms.vo.label");
	
	@PersistenceContext
	private EntityManager em;
	
    public OrderAmendmentHistoryServiceBean() {
    }
    
	@SuppressWarnings("unchecked")
	@Override
    public List<DeliveryItem> retriveDeliveryItemList(List<Long> deliveryItemIdList) {
    	
    	if (deliveryItemIdList == null || deliveryItemIdList.size() <= 0) {
    		return new ArrayList<DeliveryItem>();
    	}
    	
    	List<DeliveryItem> deliveryItemList = QueryUtils.splitExecute(em.createQuery(
    			"select o from DeliveryItem o" + // 20151012 index check : DeliveryItem.id : PK_DELIVERY_ITEM
    			" where o.id in :deliveryItemIdList")
    			.setHint(QueryHints.FETCH, DELIVERY_ITEM_QUERY_HINT_FETCH_1),
    			"deliveryItemIdList", deliveryItemIdList);
    	
    	for (DeliveryItem deliveryItem: deliveryItemList) {
    		if (deliveryItem.getLabelXml() != null) {
	    		MpDispLabel label = MP_DISP_LABEL_JAXB_WRAPPER.unmarshall(deliveryItem.getLabelXml());
	    		deliveryItem.setLabelWardCode(label.getWard());
	    		deliveryItem.setLabelBedNum(label.getBedNum());
    		}
    	}
    	return deliveryItemList;
    }
    
    @Remove
	@Override
	public void destroy() {
    }
}
