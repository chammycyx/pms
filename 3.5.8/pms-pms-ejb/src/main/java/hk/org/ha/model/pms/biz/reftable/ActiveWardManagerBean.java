package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.ActiveWard;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("activeWardManager")
@MeasureCalls
public class ActiveWardManagerBean implements ActiveWardManagerLocal {

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public boolean isActiveWard(String patHospCode, String pasWardCode) {
		return isActiveWard(patHospCode, pasWardCode, new Date());
	}

	@Override
	public boolean isActiveWard(String patHospCode, String pasWardCode, Date date) {

		return ((Long) em.createQuery(
				"select count(a) from ActiveWard a" + // 20150206 index check : ActiveWard.patHospCode,pasWardCode : PK_ACTIVE_WARD
				" where a.patHospCode = :patHospCode" +
				" and a.pasWardCode = :pasWardCode" +
				" and a.effectiveDate <= :date" +
				" and a.status = :activeRecordStatus")
				.setParameter("patHospCode", patHospCode)
				.setParameter("pasWardCode", pasWardCode)
				.setParameter("date", date)
				.setParameter("activeRecordStatus", RecordStatus.Active)
				.getSingleResult()) > 0;
	}
	
	@Override
	public void initInvalidPasWardFlag(MedProfile medProfile) {
		initInvalidPasWardFlag(medProfile, new Date());
	}
	
	@Override
	public void initInvalidPasWardFlag(MedProfile medProfile, Date date) {
		
		if (!medProfile.isInactivePasWardFlagInited()) {
			medProfile.initInactivePasWardFlag(!isActiveWard(medProfile.getPatHospCode(), medProfile.getMedCase().getPasWardCode(), date));
		}
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, List<String>> retrieveActiveWardMapByPatHospCodeList(List<String> patHospCodeList) {
		Map<String, List<String>> activeWardListByPatHospCodeMap = new HashMap<String, List<String>>();
		
		if (patHospCodeList.isEmpty()) {
			return activeWardListByPatHospCodeMap;
		}
		
		List<ActiveWard> activeWardList = em.createQuery(
				"select o from ActiveWard o" +  // 20161128 index check : ActiveWard.patHospCode : PK_ACTIVE_WARD
				" where o.patHospCode in :patHospCode" +
				" and o.effectiveDate <= :date" +
				" and o.status = :activeRecordStatus")
				.setParameter("patHospCode", patHospCodeList)
				.setParameter("date", new Date())
				.setParameter("activeRecordStatus", RecordStatus.Active)
				.getResultList();
		
		for (ActiveWard activeWard : activeWardList) {
			List<String> wardCodeList;
			if (activeWardListByPatHospCodeMap.containsKey(activeWard.getPatHospCode())) {
				wardCodeList = activeWardListByPatHospCodeMap.get(activeWard.getPatHospCode());
			} else {
				wardCodeList = new ArrayList<String>();
				activeWardListByPatHospCodeMap.put(activeWard.getPatHospCode(), wardCodeList);
			}
			
			wardCodeList.add(activeWard.getPasWardCode());
		}
		
		return activeWardListByPatHospCodeMap;
	}
}
