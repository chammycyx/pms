package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class EdsPerfRptDtl implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer pickCount;
	private Integer assembleCount;
	private Integer checkCount;
	private Integer issueCount;
	private Integer suspendCount;
	public Integer getPickCount() {
		return pickCount;
	}
	public void setPickCount(Integer pickCount) {
		this.pickCount = pickCount;
	}
	public Integer getAssembleCount() {
		return assembleCount;
	}
	public void setAssembleCount(Integer assembleCount) {
		this.assembleCount = assembleCount;
	}
	public Integer getCheckCount() {
		return checkCount;
	}
	public void setCheckCount(Integer checkCount) {
		this.checkCount = checkCount;
	}
	public Integer getIssueCount() {
		return issueCount;
	}
	public void setIssueCount(Integer issueCount) {
		this.issueCount = issueCount;
	}
	public Integer getSuspendCount() {
		return suspendCount;
	}
	public void setSuspendCount(Integer suspendCount) {
		this.suspendCount = suspendCount;
	}
}
