package hk.org.ha.model.pms.biz.vetting;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.CapdVoucher;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.udt.vetting.CapdVoucherStatus;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("capdManualVoucherService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CapdManualVoucherServiceBean implements CapdManualVoucherServiceLocal {
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private Workstore workstore;
	
	@Out(required = false)
	private CapdVoucher capdManualVoucher;
	
	@In(required = false)
	private DispOrder dispOrder;
	
	@SuppressWarnings("unchecked")
	public void retrieveCapdManualVoucherByManualVoucherNum(String capdManualVoucherNum){
		capdManualVoucher = null;
		
		StringBuilder sb = new StringBuilder(
				"select o from CapdVoucher o" + // 20120306 index check : CapdVoucher.manualVoucherNum : I_CAPD_VOUCHER_02
				" where o.dispOrder.workstore = :workstore" +
				" and o.manualVoucherNum = :manualVoucherNum" +
				" and o.status <> :deleteStatus");
		
		if ( dispOrder != null ) {
			sb.append(" and o.dispOrder.orgDispOrderId <> :orgDispOrderId");
		}
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("workstore", workstore);
		query.setParameter("manualVoucherNum", capdManualVoucherNum);
		query.setParameter("deleteStatus", CapdVoucherStatus.Deleted);
		
		if ( dispOrder != null ) {
			query.setParameter("orgDispOrderId", dispOrder.getOrgDispOrderId());
		}
		
		List<CapdVoucher> capdVoucherList = query.getResultList();
		
		if ( !capdVoucherList.isEmpty() ) {
			capdManualVoucher = capdVoucherList.get(0);
		}
	}
	
	@Remove
	public void destroy() 
	{
		if ( capdManualVoucher!=null ) {
			capdManualVoucher = null;
		}
	}
}
