package hk.org.ha.model.pms.biz.report;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.prop.Prop;
import hk.org.ha.model.pms.udt.disp.DispOrderDayEndStatus;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.DayEndDefDoctorRpt;
import hk.org.ha.model.pms.vo.report.DayEndDefDoctorRptDtl;
import hk.org.ha.model.pms.vo.report.DayEndDefDoctorRptFtr;
import hk.org.ha.model.pms.vo.report.DayEndDefDoctorRptHdr;
import hk.org.ha.model.pms.vo.report.DayEndFileExtractRpt;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;


@Stateful
@Scope(ScopeType.SESSION)
@Name("dayEndRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DayEndRptServiceBean implements DayEndRptServiceLocal {
	@PersistenceContext
	private EntityManager em;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent; 
	
	@In 
	private Workstore workstore;

	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
	
	private boolean retrieveSuccess;
	
	private List<DayEndDefDoctorRpt> dayEndDefDoctorRptList;
	
	private List<DayEndFileExtractRpt> dayEndFileExtractRptList;
	
	private String currentRpt = null;
	
	private List<?> reportObj = null;

	public void retrieveDayEndRpt(Integer selectedTab, String workstoreCode, Date batchDate) {
		if(selectedTab == 0){
			currentRpt = "DayEndDefDoctorRpt";
			reportObj = retrieveDayEndDefDoctorRptList(workstoreCode, batchDate);
		}
		else if(selectedTab == 1){
			currentRpt = "DayEndFileExtractRpt";
			reportObj = retrieveDayEndFileExtractRptList(workstoreCode, batchDate);
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<DayEndDefDoctorRpt> retrieveDayEndDefDoctorRptList(String workstoreCode, Date batchDate) {
		
		dayEndDefDoctorRptList = new ArrayList<DayEndDefDoctorRpt>();

		List<DispOrderDayEndStatus> doDayEndStatusList = new ArrayList<DispOrderDayEndStatus>();
		doDayEndStatusList.add(DispOrderDayEndStatus.Completed);
		doDayEndStatusList.add(DispOrderDayEndStatus.Uncollected);
		
		List<DispOrder> dispOrderList = em.createQuery(
							"SELECT o FROM DispOrder o" + // 20121112 index check : DispOrder.workstore,dayEndBatchDate : I_DISP_ORDER_08
							" WHERE o.workstore.hospCode = :hospCode" +
							" AND o.workstore.workstoreCode = :workstoreCode" +
							" AND o.dayEndStatus in :doDayEndStatusList" +
							" AND o.pharmOrder.defaultDoctorCode = o.pharmOrder.doctorCode" +
							" AND o.pharmOrder.medOrder.orderType = :orderType" +
							" AND o.dayEndBatchDate = :dayEndBatchDate" +
							" ORDER BY o.specCode, o.pharmOrder.doctorCode")
							.setParameter("hospCode", workstore.getHospCode())
							.setParameter("workstoreCode", workstoreCode)
							.setParameter("doDayEndStatusList", doDayEndStatusList)
							.setParameter("orderType", OrderType.OutPatient)
							.setParameter("dayEndBatchDate", batchDate, TemporalType.DATE)
							.setHint(QueryHints.BATCH, "o.pharmOrder.medOrder.patient")
							.setHint(QueryHints.BATCH, "o.ticket")
							.setHint(QueryHints.BATCH, "o.workstore")
							.getResultList();
		
		DayEndDefDoctorRpt dayEndDefDoctorRpt = new DayEndDefDoctorRpt();
		
		DayEndDefDoctorRptHdr dayEndDefDoctorRptHdr = new DayEndDefDoctorRptHdr();
		dayEndDefDoctorRptHdr.setHospCode(workstore.getHospCode());
		dayEndDefDoctorRptHdr.setWorkstoreCode(workstoreCode);
		dayEndDefDoctorRptHdr.setHospName(Prop.HOSPITAL_NAMEENG.get());
		dayEndDefDoctorRptHdr.setBatchDate(batchDate);
		
		dayEndDefDoctorRpt.setDayEndDefDoctorRptHdr(dayEndDefDoctorRptHdr);
		
		List<DayEndDefDoctorRptDtl> dayEndDefDoctorRptDtlList = new ArrayList<DayEndDefDoctorRptDtl>();

		for (DispOrder dispOrder:dispOrderList){
			
			DayEndDefDoctorRptDtl dayEndDefDoctorRptDtl = new DayEndDefDoctorRptDtl();
			dayEndDefDoctorRptDtl.setSpecCode(dispOrder.getSpecCode());
			dayEndDefDoctorRptDtl.setDoctorCode(dispOrder.getPharmOrder().getDefaultDoctorCode());
			dayEndDefDoctorRptDtl.setDispDate(dispOrder.getDispDate());
			dayEndDefDoctorRptDtl.setPatType("O");
			dayEndDefDoctorRptDtl.setTicketNum(dispOrder.getTicket().getTicketNum());
			dayEndDefDoctorRptDtl.setPatName(dispOrder.getPharmOrder().getMedOrder().getPatient().getName());
			dayEndDefDoctorRptDtl.setOrderNum(dispOrder.getPharmOrder().getMedOrder().getOrderNum());
		
			dayEndDefDoctorRptDtlList.add(dayEndDefDoctorRptDtl);
			
		}
		
		dayEndDefDoctorRpt.setDayEndDefDoctorRptDtlList(dayEndDefDoctorRptDtlList);
		
		int opPrescCount = dispOrderList.size();
		
		DayEndDefDoctorRptFtr dayEndDefDoctorRptFtr = new DayEndDefDoctorRptFtr();
		dayEndDefDoctorRptFtr.setOpPrescCount(opPrescCount);
		dayEndDefDoctorRptFtr.setTotalPrescCount(opPrescCount);
		
		dayEndDefDoctorRpt.setDayEndDefDoctorRptFtr(dayEndDefDoctorRptFtr);
		
		dayEndDefDoctorRptList.add(dayEndDefDoctorRpt);
		
		if ( !dayEndDefDoctorRptList.isEmpty() ) {
			retrieveSuccess = true;
		}
		else{
			retrieveSuccess = false;
		}
		return dayEndDefDoctorRptList;
	}
	
	private List<DayEndFileExtractRpt> retrieveDayEndFileExtractRptList(String workstoreCode, Date batchDate) {
		dayEndFileExtractRptList = corpPmsServiceProxy.retrieveDayEndFileExtractRptList(workstore.getHospCode(), workstoreCode, batchDate);
		
		if ( !dayEndFileExtractRptList.isEmpty() ) {
			retrieveSuccess = true;
		} else{
			retrieveSuccess = false;
		}
		
		return dayEndFileExtractRptList;
	}
	
	public void generateDayEndRpt() {
		
		printAgent.renderAndRedirect(new RenderJob(
				currentRpt, 
				new PrintOption(), 
				getReportObj()));

	}
	
	public void printDayEndRpt(){

		printAgent.renderAndPrint(new RenderAndPrintJob(
				currentRpt, 
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				getReportObj()));
		
	}

	@Remove
	public void destroy() {
		if(dayEndDefDoctorRptList!=null){
			dayEndDefDoctorRptList=null;
		}
		
		if(dayEndFileExtractRptList!=null){
			dayEndFileExtractRptList=null;
		}
		
	}

	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}

	public List<?> getReportObj() {
		return reportObj;
	}
	
}
