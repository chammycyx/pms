package hk.org.ha.model.pms.biz.enquiry;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmFmDrug;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("specialDrugEnqListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SpecialDrugEnqListServiceBean implements SpecialDrugEnqListServiceLocal {
	
	@Out(required = false)
	private List<DmFmDrug> dmFmDrugList;

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private Workstore workstore;
	
	public void retrieveDmFmDrugList(String itemCodePrefix) {
		dmFmDrugList = dmsPmsServiceProxy.retrieveDmFmDrugByItemCode(workstore.getHospCode(), workstore.getWorkstoreCode(), itemCodePrefix);
	}
	
	public void retrieveDmFmDrugListByFullDrugDesc(String fullDrugDescPrefix) {
		dmFmDrugList = dmsPmsServiceProxy.retrieveDmFmDrugByFullDrugDesc(workstore.getHospCode(), workstore.getWorkstoreCode(), fullDrugDescPrefix);
	}

	@Remove
	public void destroy() 
	{
		if (dmFmDrugList != null) {
			dmFmDrugList = null;
		}
	}
}
