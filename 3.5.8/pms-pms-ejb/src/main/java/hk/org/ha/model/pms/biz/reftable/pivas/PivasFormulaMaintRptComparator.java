package hk.org.ha.model.pms.biz.reftable.pivas;

import hk.org.ha.model.pms.dms.udt.pivas.PivasFormulaType;
import hk.org.ha.model.pms.vo.report.PivasFormulaMaintRpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Comparator;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;

public class PivasFormulaMaintRptComparator implements Comparator<PivasFormulaMaintRpt>, Serializable {

	private static final long serialVersionUID = 1L;
	
	public int compare(PivasFormulaMaintRpt item1, PivasFormulaMaintRpt item2) {
		
		boolean isItem1MergePivasFormulaType = (StringUtils.equals(item1.getPivasFormulaType(), PivasFormulaType.Merge.getDisplayValue()) ? true : false);
		boolean isItem2MergePivasFormulaType = (StringUtils.equals(item2.getPivasFormulaType(), PivasFormulaType.Merge.getDisplayValue()) ? true : false);
		
		if (!StringUtils.equals(item1.getWorkstoreGroupCode(), item2.getWorkstoreGroupCode())) {
			return StringUtils.trimToEmpty(item1.getWorkstoreGroupCode()).compareTo(StringUtils.trimToEmpty(item2.getWorkstoreGroupCode()));
		}
		
		// sort in DESC
		if (!StringUtils.equals(item2.getPivasFormulaType(), item1.getPivasFormulaType())) {
			return StringUtils.trimToEmpty(item2.getPivasFormulaType()).compareTo(StringUtils.trimToEmpty(item1.getPivasFormulaType()));
		}
		
		if (!StringUtils.equals(item1.getPrintName(), item2.getPrintName())) {
			return StringUtils.trimToEmpty(item1.getPrintName()).compareTo(StringUtils.trimToEmpty(item2.getPrintName()));
		}
		
		Integer drugKey1 = isItem1MergePivasFormulaType ? 0 : item1.getDrugKey();
		Integer drugKey2 = isItem2MergePivasFormulaType ? 0 : item2.getDrugKey();
		
		if (drugKey1.compareTo(drugKey2) != 0) {
			return drugKey1.compareTo(drugKey2);
		}
		
		if (!StringUtils.equals(item1.getStrength(), item2.getStrength())) {
			return StringUtils.trimToEmpty(item1.getStrength()).compareTo(StringUtils.trimToEmpty(item2.getStrength()));
		}
		
		if (!StringUtils.equals(item1.getSiteCode(), item2.getSiteCode())) {
			return StringUtils.trimToEmpty(item1.getSiteCode()).compareTo(StringUtils.trimToEmpty(item2.getSiteCode()));
		}
		
		if (!StringUtils.equals(item1.getDiluentCode(), item2.getDiluentCode())) {
			return StringUtils.trimToEmpty(item1.getDiluentCode()).compareTo(StringUtils.trimToEmpty(item2.getDiluentCode()));
		}

		BigDecimal concn1 = item1.getConcn() == null ? BigDecimal.ZERO : item1.getConcn();
		BigDecimal concn2 = item2.getConcn() == null ? BigDecimal.ZERO : item2.getConcn();
		
		if (concn1.compareTo(concn2) != 0) {
			return concn1.compareTo(concn2);
		}
		
		BigDecimal rate1 = item1.getRate() == null ? BigDecimal.ZERO : item1.getRate();
		BigDecimal rate2 = item2.getRate() == null ? BigDecimal.ZERO : item2.getRate();
		
		if (rate1.compareTo(rate2) != 0) {
			return rate1.compareTo(rate2);
		}
		
		if (item1.getFormulaId().compareTo(item2.getFormulaId()) != 0) {
			return item1.getFormulaId().compareTo(item2.getFormulaId());
		}
		
		// sort in DESC
		if (item2.getCoreFlag().compareTo(item1.getCoreFlag()) != 0) {
			return item2.getCoreFlag().compareTo(item1.getCoreFlag());
		}
		
		if (!StringUtils.equals(item1.getPivasFormulaMethodStatus(), item2.getPivasFormulaMethodStatus())) {
			return StringUtils.trimToEmpty(item1.getPivasFormulaMethodStatus()).compareTo(StringUtils.trimToEmpty(item2.getPivasFormulaMethodStatus()));
		}
		
		if (item1.getFormulaMethodId().compareTo(item2.getFormulaMethodId()) != 0) {
			return item1.getFormulaMethodId().compareTo(item2.getFormulaMethodId());
		}
		
		if (item1.getFormulaDrugId().compareTo(item2.getFormulaDrugId()) != 0) {
			return item1.getFormulaDrugId().compareTo(item2.getFormulaDrugId());
		}
		
		return CompareToBuilder.reflectionCompare(item1, item2);
	}
	
}