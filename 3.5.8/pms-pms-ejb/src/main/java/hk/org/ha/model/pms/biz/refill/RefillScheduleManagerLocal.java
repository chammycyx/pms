package hk.org.ha.model.pms.biz.refill;

import java.util.List;
import java.util.Map;

import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.persistence.reftable.DrsConfig;
import hk.org.ha.model.pms.persistence.reftable.RefillConfig;
import hk.org.ha.model.pms.udt.refill.DocType;
import hk.org.ha.model.pms.udt.refill.RefillScheduleRefillStatus;

import javax.ejb.Local;

@Local
public interface RefillScheduleManagerLocal {
		
	boolean isAllowStdRefill(PharmOrder pharmOrder, RefillConfig refillConfig, boolean isAutoGen, boolean drsFlag);
	boolean isValidPharmOrderItemForStandardRefill(PharmOrderItem pharmOrderItem, boolean autoGenFlag, Integer refillThreshold, Integer lastIntervalMaxDay);
	void removeRefillScheduleListByPharmOrderItemNum(PharmOrder pharmOrder, Integer pharmOrderItemNum);
	RefillScheduleRefillStatus retrieveInitRefillStatusByRefillNum( Integer refillNumIn );
	List<PharmOrderItem> filterPharmOrderItemListByRefillSelection(List<PharmOrderItem> pharmOrderItemListIn, RefillConfig refillConfig);
	String retrieveManualMedOrderNumForCoupon(PharmOrder pharmOrder);
	Map<DocType, List<RefillSchedule>> retrieveNextRefillScheduleList(DispOrder dispOrder);
	Pair<DrsConfig, List<String>> retrieveDrsConfig(PharmOrder pharmOrder, MedOrder medOrder, boolean refillOrderFlag);
	void destroy();	
}
