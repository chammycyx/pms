package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;


import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class PivasBatchDispRptRawMaterialDtl implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Integer sortSeq;
		
	private String solventDesc;
	
	private String diluentDesc;
	
	private Boolean vendSupplySolvFlag;
	
	private String itemCode;
	
	private String manufCode;
	
	private String batchNum;
	
	private Date expiryDate;

	public PivasBatchDispRptRawMaterialDtl() {
		vendSupplySolvFlag = Boolean.FALSE;
	}

	public Integer getSortSeq() {
		return sortSeq;
	}

	public void setSortSeq(Integer sortSeq) {
		this.sortSeq = sortSeq;
	}

	public String getSolventDesc() {
		return solventDesc;
	}

	public void setSolventDesc(String solventDesc) {
		this.solventDesc = solventDesc;
	}

	public String getDiluentDesc() {
		return diluentDesc;
	}

	public void setDiluentDesc(String diluentDesc) {
		this.diluentDesc = diluentDesc;
	}
	
	public Boolean getVendSupplySolvFlag() {
		return vendSupplySolvFlag;
	}

	public void setVendSupplySolvFlag(Boolean vendSupplySolvFlag) {
		this.vendSupplySolvFlag = vendSupplySolvFlag;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getManufCode() {
		return manufCode;
	}

	public void setManufCode(String manufCode) {
		this.manufCode = manufCode;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	
}	