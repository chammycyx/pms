package hk.org.ha.model.pms.biz.reftable;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.reftable.ChargeReason;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.persistence.reftable.OperationWorkstation;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.vetting.OrderType;

import javax.ejb.Local;

@Local
public interface RefTableManagerLocal  {

	OperationProfile retrieveActiveProfile(Workstore workstore);
	
	void clearActiveProfile();
	
	OperationWorkstation retrieveOperationWorkstation(Workstation workstation, OperationProfile activeProfile);

	String getFdnByItemCodeOrderTypeWorkstore(String itemCode, OrderType orderType, Workstore workstoreIn);
	
	Map<String, String> getFdnByItemCodeListOrderTypeWorkstore(Collection<String> itemCodeList, OrderType orderType, Workstore workstore);	
	
	Map<String,String> getFdnMappingMapByPharmOrderItemList(List<PharmOrderItem> pharmOrderItemList, OrderType orderType, Workstore workstore);
	
	Map<String,String> getFdnMappingMapByDispOrderItemList(List<DispOrderItem> dispOrderItemList, OrderType orderType, Workstore workstore);

	List<ChargeReason> retrieveExemptChargeReasonList();
	
	List<OperationWorkstation> getActiveAtdpsWorkstation(OperationProfile activeProfile);

}
