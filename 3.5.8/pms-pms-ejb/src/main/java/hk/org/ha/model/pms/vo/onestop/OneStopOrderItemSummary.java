package hk.org.ha.model.pms.vo.onestop;

import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.disp.PharmOrderPatType;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderStatus;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderStatusIndicator;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class OneStopOrderItemSummary implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Long id;
	
	private OneStopOrderType oneStopOrderType;

	private PharmOrderPatType patType;
	
	private String patHospCode;
	
	private String doctorCode;
	
	private String doctorName;
	
	private String phsSpecCode;
	
	private String orignalPhsSpecCode;
	
	private String phsWardCode;
	
	private String patCatCode;
	
	private MedOrderPrescType prescType;
	
	private String orderNum;
	
	private String refNum;
	
	private Date orderDate;
	
	private OneStopOrderStatus oneStopOrderStatus;
	
	private OneStopOrderStatusIndicator oneStopOrderStatusInd;
	
	private OneStopOrderStatusIndicator pharmcyRemark;
	
	private Date ticketDate;
	
	private String ticketNum;
	
	private String ticketWks;
	
	private String ticketHospCode;
	
	private String orignalWks;

	private String receiptNum;

	private Date dispDate;

	private MedOrderDocType docType;

	private String suspendCode;

	private String suspendDesc;
	
	private MedCase medCase;
	
	private DispOrderStatus dispOrderStatus;
	
	private Long dispOrderId;
	
	private String dispWorkStore;
	
	private String dispHospCode;
	
	private Long lockWorkstationId;
	
	private Long dispOrderVersion;
	
	private Long medOrderVersion;
	
	private String lockWorkstationName;
	
	private Boolean unlockOrder;
	
	private Integer refillCount;
	
	private Boolean allowReversePharmcyRemark;
	
	private String remarkText;
	
	private Boolean dayEnd;
	
	private Boolean privateFlag;
	
	private String invoiceNum;
	
	private Patient patient;
	
	private Boolean hasPostComment;
	
	private Boolean hasSfiRefill;
	
	private Boolean refillFlag;
	
	private Date remarkConfirmDate;
	
	private Boolean preVetUrgentFlag;
	
	private String medOrderPhsSpecCode;
	
	private String medOrderPhsWardCode;
	
	private Boolean isLatestDhOrder;
	
	private Boolean isCannotConvertDhPatient;

	public OneStopOrderItemSummary () {
		this.unlockOrder = false;
		this.dayEnd = false;
		this.hasPostComment = false;
		this.hasSfiRefill = false;
		this.isLatestDhOrder = false;
		this.isCannotConvertDhPatient = false;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public OneStopOrderType getOneStopOrderType() {
		return oneStopOrderType;
	}

	public void setOneStopOrderType(OneStopOrderType oneStopOrderType) {
		this.oneStopOrderType = oneStopOrderType;
	}

	public PharmOrderPatType getPatType() {
		return patType;
	}

	public void setPatType(PharmOrderPatType patType) {
		this.patType = patType;
	}

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}
	
	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getPhsSpecCode() {
		return phsSpecCode;
	}

	public void setPhsSpecCode(String phsSpecCode) {
		this.phsSpecCode = phsSpecCode;
	}

	public String getPhsWardCode() {
		return phsWardCode;
	}

	public void setPhsWardCode(String phsWardCode) {
		this.phsWardCode = phsWardCode;
	}

	public String getPatCatCode() {
		return patCatCode;
	}

	public void setPatCatCode(String patCatCode) {
		this.patCatCode = patCatCode;
	}

	public MedOrderPrescType getPrescType() {
		return prescType;
	}

	public void setPrescType(MedOrderPrescType prescType) {
		this.prescType = prescType;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public String getRefNum() {
		return refNum;
	}

	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public OneStopOrderStatus getOneStopOrderStatus() {
		return oneStopOrderStatus;
	}

	public void setOneStopOrderStatus(OneStopOrderStatus oneStopOrderStatus) {
		this.oneStopOrderStatus = oneStopOrderStatus;
	}

	public OneStopOrderStatusIndicator getOneStopOrderStatusInd() {
		return oneStopOrderStatusInd;
	}

	public void setOneStopOrderStatusInd(
			OneStopOrderStatusIndicator oneStopOrderStatusInd) {
		this.oneStopOrderStatusInd = oneStopOrderStatusInd;
	}

	public Date getTicketDate() {
		return ticketDate;
	}

	public void setTicketDate(Date ticketDate) {
		this.ticketDate = ticketDate;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}
	
	public String getTicketWks() {
		return ticketWks;
	}

	public void setTicketWks(String ticketWks) {
		this.ticketWks = ticketWks;
	}
	
	public String getTicketHospCode() {
		return ticketHospCode;
	}

	public void setTicketHospCode(String ticketHospCode) {
		this.ticketHospCode = ticketHospCode;
	}

	public String getOrignalWks() {
		return orignalWks;
	}

	public void setOrignalWks(String orignalWks) {
		this.orignalWks = orignalWks;
	}

	public String getReceiptNum() {
		return receiptNum;
	}

	public void setReceiptNum(String receiptNum) {
		this.receiptNum = receiptNum;
	}

	public Date getDispDate() {
		return dispDate;
	}

	public void setDispDate(Date dispDate) {
		this.dispDate = dispDate;
	}

	public MedOrderDocType getDocType() {
		return docType;
	}

	public void setDocType(MedOrderDocType docType) {
		this.docType = docType;
	}

	public String getSuspendCode() {
		return suspendCode;
	}

	public void setSuspendCode(String suspendCode) {
		this.suspendCode = suspendCode;
	}

	public String getSuspendDesc() {
		return suspendDesc;
	}

	public void setSuspendDesc(String suspendDesc) {
		this.suspendDesc = suspendDesc;
	}

	public MedCase getMedCase() {
		return medCase;
	}

	public void setMedCase(MedCase medCase) {
		this.medCase = medCase;
	}

	public DispOrderStatus getDispOrderStatus() {
		return dispOrderStatus;
	}

	public void setDispOrderStatus(DispOrderStatus dispOrderStatus) {
		this.dispOrderStatus = dispOrderStatus;
	}

	public Long getDispOrderId() {
		return dispOrderId;
	}

	public void setDispOrderId(Long dispOrderId) {
		this.dispOrderId = dispOrderId;
	}

	public String getDispWorkStore() {
		return dispWorkStore;
	}

	public void setDispWorkStore(String dispWorkStore) {
		this.dispWorkStore = dispWorkStore;
	}

	public Long getLockWorkstationId() {
		return lockWorkstationId;
	}

	public void setLockWorkstationId(Long lockWorkstationId) {
		this.lockWorkstationId = lockWorkstationId;
	}

	public Long getDispOrderVersion() {
		return dispOrderVersion;
	}

	public void setDispOrderVersion(Long dispOrderVersion) {
		this.dispOrderVersion = dispOrderVersion;
	}

	public Long getMedOrderVersion() {
		return medOrderVersion;
	}

	public void setMedOrderVersion(Long medOrderVersion) {
		this.medOrderVersion = medOrderVersion;
	}

	public String getLockWorkstationName() {
		return lockWorkstationName;
	}

	public void setLockWorkstationName(String lockWorkstationName) {
		this.lockWorkstationName = lockWorkstationName;
	}

	public Boolean getUnlockOrder() {
		return unlockOrder;
	}

	public void setUnlockOrder(Boolean unlockOrder) {
		this.unlockOrder = unlockOrder;
	}

	public Boolean getAllowReversePharmcyRemark() {
		return allowReversePharmcyRemark;
	}

	public void setAllowReversePharmcyRemark(Boolean allowReversePharmcyRemark) {
		this.allowReversePharmcyRemark = allowReversePharmcyRemark;
	}

	public String getRemarkText() {
		return remarkText;
	}

	public void setRemarkText(String remarkText) {
		this.remarkText = remarkText;
	}

	public Boolean getDayEnd() {
		return dayEnd;
	}

	public void setDayEnd(Boolean dayEnd) {
		this.dayEnd = dayEnd;
	}

	public OneStopOrderStatusIndicator getPharmcyRemark() {
		return pharmcyRemark;
	}

	public void setPharmcyRemark(OneStopOrderStatusIndicator pharmcyRemark) {
		this.pharmcyRemark = pharmcyRemark;
	}

	public Boolean getPrivateFlag() {
		return privateFlag;
	}

	public void setPrivateFlag(Boolean privateFlag) {
		this.privateFlag = privateFlag;
	}

	public String getInvoiceNum() {
		return invoiceNum;
	}

	public void setInvoiceNum(String invoiceNum) {
		this.invoiceNum = invoiceNum;
	}
	
	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public void setRefillCount(Integer refillCount) {
		this.refillCount = refillCount;
	}

	public Integer getRefillCount() {
		return refillCount;
	}

	public void setHasPostComment(Boolean hasPostComment) {
		this.hasPostComment = hasPostComment;
	}

	public Boolean getHasPostComment() {
		return hasPostComment;
	}

	public Boolean getHasSfiRefill() {
		return hasSfiRefill;
	}

	public void setHasSfiRefill(Boolean hasSfiRefill) {
		this.hasSfiRefill = hasSfiRefill;
	}

	public void setRefillFlag(Boolean refillFlag) {
		this.refillFlag = refillFlag;
	}

	public Boolean getRefillFlag() {
		return refillFlag;
	}

	public void setOrignalPhsSpecCode(String orignalPhsSpecCode) {
		this.orignalPhsSpecCode = orignalPhsSpecCode;
	}

	public String getOrignalPhsSpecCode() {
		return orignalPhsSpecCode;
	}

	public void setRemarkConfirmDate(Date remarkConfirmDate) {
		this.remarkConfirmDate = remarkConfirmDate;
	}

	public Date getRemarkConfirmDate() {
		return remarkConfirmDate;
	}
	
	public Boolean getPreVetUrgentFlag() {
		return preVetUrgentFlag;
	}

	public void setPreVetUrgentFlag(Boolean preVetUrgentFlag) {
		this.preVetUrgentFlag = preVetUrgentFlag;
	}

	public String getMedOrderPhsSpecCode() {
		return medOrderPhsSpecCode;
	}
	
	public void setMedOrderPhsSpecCode(String medOrderPhsSpecCode) {
		this.medOrderPhsSpecCode = medOrderPhsSpecCode;
	}

	public String getMedOrderPhsWardCode() {
		return medOrderPhsWardCode;
	}

	public void setMedOrderPhsWardCode(String medOrderPhsWardCode) {
		this.medOrderPhsWardCode = medOrderPhsWardCode;
	}
	
	public Boolean getIsLatestDhOrder() {
		return isLatestDhOrder;
	}

	public void setIsLatestDhOrder(Boolean isLatestDhOrder) {
		this.isLatestDhOrder = isLatestDhOrder;
	}
	
	public Boolean getIsCannotConvertDhPatient() {
		return isCannotConvertDhPatient;
	}

	public void setIsCannotConvertDhPatient(Boolean isCannotConvertDhPatient) {
		this.isCannotConvertDhPatient = isCannotConvertDhPatient;
	}

	public String getDispHospCode() {
		return dispHospCode;
	}

	public void setDispHospCode(String dispHospCode) {
		this.dispHospCode = dispHospCode;
	}
}