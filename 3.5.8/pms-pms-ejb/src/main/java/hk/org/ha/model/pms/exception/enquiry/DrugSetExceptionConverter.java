package hk.org.ha.model.pms.exception.enquiry;

import hk.org.ha.model.pms.asa.exception.moe.DrugSetException;

import java.util.Map;

import org.granite.messaging.service.ExceptionConverter;
import org.granite.messaging.service.ServiceException;

public class DrugSetExceptionConverter implements ExceptionConverter {

	public static final String DRUG_SET_EXCEPTION = "DrugSetException";
	
	@Override
	public boolean accepts(Throwable t, Throwable finalException) {
		return t.getClass().equals(DrugSetException.class);
	}

	@Override
	public ServiceException convert(Throwable t, String detail, Map<String, Object> extendedData) {
		ServiceException se; 		
		se = new ServiceException(DRUG_SET_EXCEPTION, t.getMessage(), detail, t);
		se.getExtendedData().putAll(extendedData);
		return se;
	}
}
