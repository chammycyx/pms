package hk.org.ha.model.pms.biz.drug;

import javax.ejb.Local;

@Local
public interface MsFmStatusListServiceLocal {

	void retrieveMsFmStatusList(String prefixFmStatus);
	
	void destroy();
	
}
