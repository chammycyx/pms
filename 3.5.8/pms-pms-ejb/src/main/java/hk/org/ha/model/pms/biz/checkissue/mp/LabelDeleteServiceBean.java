package hk.org.ha.model.pms.biz.checkissue.mp;

import static hk.org.ha.model.pms.prop.Prop.CDDH_LEGACY_PERSISTENCE_ENABLED;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.udt.medprofile.DeliveryItemStatus;
import hk.org.ha.model.pms.vo.checkissue.mp.LabelDelete;
import hk.org.ha.model.pms.vo.checkissue.mp.LabelDeleteItem;
import hk.org.ha.model.pms.vo.label.MpDispLabelQrCode;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("labelDeleteService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class LabelDeleteServiceBean implements LabelDeleteServiceLocal {
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;

	@In
	private Workstore workstore;
	
	@Out(required = false)
	private LabelDelete labelDelete;
	
	private Map<Long, DeliveryItem> deliveryItemMap;
	
	private String msgCode;

	private static final String JAXB_CONTEXT_LABEL = "hk.org.ha.model.pms.vo.label";
	
	private static final int QUERY_CHUNK_SIZE = 500; 
	
	
	@SuppressWarnings("unchecked")
	public void retrieveLabelDeleteItemList(Date trxDate, String caseNum, String batchNum, String mpDispLabelQrCodeXml) {
		msgCode = null;
		labelDelete = new LabelDelete();
		labelDelete.setLabelDeleteItemList(new ArrayList<LabelDeleteItem>());
		Date transactionDate = new Date();
		deliveryItemMap = new HashMap<Long, DeliveryItem>();
		
		if ( StringUtils.isNotBlank(mpDispLabelQrCodeXml) ) {
			labelDelete.setMpDispLabelQrCodeXml(mpDispLabelQrCodeXml);
			retrieveDeliveryItemListByTrxId(mpDispLabelQrCodeXml);
			return;
		}
		
		if ( trxDate == null ) {
			labelDelete.setTrxDate(transactionDate);
		}else{
			labelDelete.setTrxDate(null);
		}
		
		labelDelete.setBatchNum(batchNum);
		labelDelete.setCaseNum(caseNum);
		labelDelete.setMpDispLabelQrCodeXml(mpDispLabelQrCodeXml);
		
		StringBuilder sb = new StringBuilder(
				"select o from DeliveryItem o" + // 20120317 index check : Delivery.hospCode,batchDate : I_DELIVERY_01
				" where o.medProfilePoItem is not null" +
				" and o.delivery.hospital = :hospital" +
				" and o.delivery.batchDate = :batchDate");
				
		if ( StringUtils.isNotBlank(caseNum) ) {
			sb.append(" and o.medProfilePoItem.medProfileMoItem.medProfileItem.medProfile.medCase.caseNum = :caseNum");
		}
		
		if ( StringUtils.isNotBlank(batchNum) ) {
			sb.append(" and o.delivery.batchNum = :batchNum");
		}
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("hospital", workstore.getHospital());
		query.setParameter("batchDate", trxDate, TemporalType.DATE);
				
		if ( StringUtils.isNotBlank(caseNum) ) {
			query.setParameter("caseNum", caseNum);
		}
		if ( StringUtils.isNotBlank(batchNum) ) {
			query.setParameter("batchNum", batchNum);
		}
		
		List<DeliveryItem> deliveryItemList = query.getResultList();

		removeDayEndItem(deliveryItemList);
		
		for (DeliveryItem deliveryItem : deliveryItemList ) {
			LabelDeleteItem labelDeleteItem = new LabelDeleteItem();
			convertLabelDeleteItem(labelDeleteItem, deliveryItem);
			labelDelete.getLabelDeleteItemList().add(labelDeleteItem);
			deliveryItemMap.put(deliveryItem.getId(), deliveryItem);
		}
				
		if ( !labelDelete.getLabelDeleteItemList().isEmpty() ) {
			sortLabelDeleteItemList();
		}
		
	}
	
	private void retrieveDeliveryItemListByTrxId(String mpDispLabelQrCodeXmlIn){		
		String mpDispLabelQrCodeXml = null;
		Long trxId = null;
		if ( mpDispLabelQrCodeXmlIn.indexOf("DRUG>") >= 0 ) {
			mpDispLabelQrCodeXml = mpDispLabelQrCodeXmlIn.replaceAll("DRUG>", "MpDispLabelQrCode>");
			MpDispLabelQrCode mpDispLabelQrCode = null;
			try{
				JaxbWrapper<MpDispLabelQrCode> jaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
				mpDispLabelQrCode = jaxbWrapper.unmarshall(mpDispLabelQrCodeXml);
				trxId = mpDispLabelQrCode.getE();
			}catch (Exception ex ) {
				trxId = null;
			}
		} else {
			try {
				trxId = Long.parseLong(mpDispLabelQrCodeXmlIn.trim());
			} catch (java.lang.NumberFormatException e) {
				trxId = null;
			}
		}
		
		if ( trxId == null ) {
			msgCode = "0261";
			return;
		}
		
		labelDelete.setTrxId(trxId);
		DeliveryItem deliveryItem = em.find(DeliveryItem.class, trxId);
		
		if ( deliveryItem != null ) {
			labelDelete.setTrxDate(deliveryItem.getDelivery().getBatchDate());
			List<DeliveryItem> deliveryItemList = new ArrayList<DeliveryItem>();
			deliveryItemList.add(deliveryItem);
			removeDayEndItem(deliveryItemList);
			
			if ( !deliveryItemList.isEmpty() ) {
				deliveryItemMap.put(deliveryItem.getId(), deliveryItem);
				
				LabelDeleteItem labelDeleteItem = new LabelDeleteItem();
				convertLabelDeleteItem(labelDeleteItem, deliveryItem);
				labelDelete.getLabelDeleteItemList().add(labelDeleteItem);
				sortLabelDeleteItemList();
			}
		}
	}
	
	private void convertLabelDeleteItem(LabelDeleteItem labelDeleteItem, DeliveryItem deliveryItem){

		labelDeleteItem.setDeliveryItemId(deliveryItem.getId());
		
		labelDeleteItem.setWard(deliveryItem.getDelivery().getWardCode());
		
		MedProfilePoItem medProfilePoItem = deliveryItem.resolveMedProfilePoItem();
			
		labelDeleteItem.setBaseUnit(medProfilePoItem.getBaseUnit());
		labelDeleteItem.setDoseUnit(medProfilePoItem.getDoseUnit());
		labelDeleteItem.setIssueQty(deliveryItem.getIssueQty());
		labelDeleteItem.setItemCode(medProfilePoItem.getItemCode());
		
		labelDeleteItem.setDrugName(medProfilePoItem.getDrugName());
		labelDeleteItem.setFormLabelDesc(medProfilePoItem.getFormLabelDesc());
		labelDeleteItem.setStrength(medProfilePoItem.getStrength());
		labelDeleteItem.setVolumeText(medProfilePoItem.getVolumeText());
		
		labelDeleteItem.setCaseNum(medProfilePoItem.getMedProfileMoItem().getMedProfileItem().getMedProfile().getMedCase().getCaseNum());
		labelDeleteItem.setPasBedNum(medProfilePoItem.getMedProfileMoItem().getMedProfileItem().getMedProfile().getMedCase().getPasBedNum());
		labelDeleteItem.setStatus(deliveryItem.getStatus().getDisplayValue());
		
		if (deliveryItem.getStatus() == DeliveryItemStatus.Deleted) {
			labelDeleteItem.setStatus(deliveryItem.getStatus().getDisplayValue());
		} else 
		if ( medProfilePoItem.getMedProfileMoItem().getMedProfileItem().getMedProfile().getDischargeDate() != null ) {
			labelDeleteItem.setStatus("Discharged");
		}
	}
	
	private void removeDayEndItem(List<DeliveryItem> inDeliveryItemList){		
		if ( inDeliveryItemList.isEmpty() ) {
			return;
		}
		
		boolean lastSplitEntry = false;
		int counterSplit = 0;
		List<Long> deliveryItemIdList = new ArrayList<Long>();
		List<DispOrderItem> dispOrderItemList = new ArrayList<DispOrderItem>();
			
		for ( DeliveryItem deliveryItem : inDeliveryItemList) {
			if( counterSplit < QUERY_CHUNK_SIZE){
				deliveryItemIdList.add(deliveryItem.getId());
				counterSplit++;
				lastSplitEntry = true;
			}else{
				deliveryItemIdList.add(deliveryItem.getId());
				List<DispOrderItem> dispOrderItemListSplited = corpPmsServiceProxy.retrieveDispOrderItemListByDeliveryItemId(deliveryItemIdList);
				dispOrderItemList.addAll(dispOrderItemListSplited);
				deliveryItemIdList.clear();
				counterSplit = 0;
				lastSplitEntry = false;
			}
			
		}
		
		if(lastSplitEntry){
			List<DispOrderItem> dispOrderItemListSplited = corpPmsServiceProxy.retrieveDispOrderItemListByDeliveryItemId(deliveryItemIdList);
			dispOrderItemList.addAll(dispOrderItemListSplited);
		}
		
		
		if ( !dispOrderItemList.isEmpty() ) {
			for (Iterator<DeliveryItem> itr = inDeliveryItemList.iterator();itr.hasNext();) {
				DeliveryItem deliveryItem = itr.next();
				boolean removeFlag = true;
				for (DispOrderItem dispOrderItem : dispOrderItemList) {
					if ( deliveryItem.getId().equals(dispOrderItem.getDeliveryItemId()) ) {
						removeFlag = false;
						break;
					}
				}
				if ( removeFlag ) {
					itr.remove();
				}
			}
		} else {
			inDeliveryItemList.clear();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void updateDeliveryItemStatusToDeleted(LabelDelete inLabelDelete){
		List<Long> deliveryItemIdList = new ArrayList<Long>();
		for (LabelDeleteItem labelDeleteItem : inLabelDelete.getLabelDeleteItemList() ) {
			if ( !labelDeleteItem.isMarkDelete() ) {
				continue;
			}
			deliveryItemIdList.add(labelDeleteItem.getDeliveryItemId());
		}
		
		if ( !deliveryItemIdList.isEmpty() ) {
			
			List<DeliveryItem> deleteDeliveryItemList = em.createQuery(
					"select o from DeliveryItem o" + // 20120214 index check : DeliveryItem.id : PK_DELIVERY_ITEM
					" where o.id in :deliveryItemIdList")
					.setParameter("deliveryItemIdList", deliveryItemIdList)
					.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
					.getResultList();
			
			for ( DeliveryItem deleteDeliveryItem : deleteDeliveryItemList ) {					
				if (!deliveryItemMap.get(deleteDeliveryItem.getId()).getVersion().equals(deleteDeliveryItem.getVersion())) {
					throw new OptimisticLockException();
				}										
			}
			 
			boolean updateDispOrderItem = corpPmsServiceProxy.updateDispOrderItemStatusToDeleted(deliveryItemIdList, CDDH_LEGACY_PERSISTENCE_ENABLED.get(false));
			
			if ( updateDispOrderItem ) {
				for ( DeliveryItem deleteDeliveryItem : deleteDeliveryItemList ) {		
					deleteDeliveryItem.setStatus(DeliveryItemStatus.Deleted);
				}
			} else {
				throw new OptimisticLockException();
			}
		}
		
		retrieveLabelDeleteItemList(inLabelDelete.getTrxDate(), inLabelDelete.getCaseNum(), inLabelDelete.getBatchNum(), inLabelDelete.getMpDispLabelQrCodeXml());
	}
	
	@SuppressWarnings("unchecked")
	private void sortLabelDeleteItemList(){
		Collections.sort(labelDelete.getLabelDeleteItemList(), new Comparator(){
			public int compare(Object o1, Object o2) {
				LabelDeleteItem item1 = (LabelDeleteItem) o1;
				LabelDeleteItem item2 = (LabelDeleteItem) o2;
				return item1.getCaseNum().compareTo(item2.getCaseNum());
			}
		});	
	}

	public String getMsgCode() {
		return msgCode;
	}
	
	@Remove
	public void destroy() {
		if ( labelDelete != null ) {
			labelDelete = null;
		}
	}

}
