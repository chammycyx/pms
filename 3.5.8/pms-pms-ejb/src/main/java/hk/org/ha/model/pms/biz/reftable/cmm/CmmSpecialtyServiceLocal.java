package hk.org.ha.model.pms.biz.reftable.cmm;

import hk.org.ha.model.pms.dms.persistence.cmm.CmmSpecialty;

import java.util.List;

import javax.ejb.Local;

@Local
public interface CmmSpecialtyServiceLocal {

	List<String> retrieveSpecialtyCodeList();

	List<CmmSpecialty> retrieveCmmSpecialtyList();
	
	List<CmmSpecialty> updateCmmSpecialtyList(List<CmmSpecialty> cmmSpecialtyList);
	
	void destroy();

}
