package hk.org.ha.model.pms.biz.order;

import hk.org.ha.fmk.pms.remote.marshall.EclipseLinkXStreamMarshaller;
import hk.org.ha.fmk.pms.security.Uam;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.sys.SysProfile;
import hk.org.ha.model.pms.biz.assembling.AssemblingListServiceLocal;
import hk.org.ha.model.pms.biz.assembling.AssemblingServiceLocal;
import hk.org.ha.model.pms.biz.cddh.CddhServiceLocal;
import hk.org.ha.model.pms.biz.checkissue.CheckIssueServiceLocal;
import hk.org.ha.model.pms.biz.drug.DrugSearchServiceLocal;
import hk.org.ha.model.pms.biz.onestop.OneStopServiceLocal;
import hk.org.ha.model.pms.biz.picking.DrugPickListServiceLocal;
import hk.org.ha.model.pms.biz.picking.DrugPickServiceLocal;
import hk.org.ha.model.pms.biz.reftable.RefTableManagerLocal;
import hk.org.ha.model.pms.biz.reftable.WorkstationManagerLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.biz.ticketgen.TicketServiceLocal;
import hk.org.ha.model.pms.biz.vetting.OrderViewServiceLocal;
import hk.org.ha.model.pms.dms.vo.RelatedDrugSearchCriteria;
import hk.org.ha.model.pms.dms.vo.RouteFormSearchCriteria;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.Gender;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.drug.DrugScope;
import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
import hk.org.ha.model.pms.udt.ticketgen.TicketType;
import hk.org.ha.model.pms.vo.cddh.CddhCriteria;
import hk.org.ha.model.pms.vo.cddh.CddhDispOrder;
import hk.org.ha.model.pms.vo.checkissue.CheckIssueSummary;
import hk.org.ha.model.pms.vo.checkissue.CheckIssueViewInfo;
import hk.org.ha.model.pms.vo.checkissue.PrescDetail;
import hk.org.ha.model.pms.vo.onestop.OneStopOrderInfo;
import hk.org.ha.model.pms.vo.onestop.OneStopOrderItemSummary;
import hk.org.ha.model.pms.vo.onestop.OneStopSummary;
import hk.org.ha.model.pms.vo.ticketgen.TicketGenResult;
import hk.org.ha.model.pms.vo.vetting.MedOrderItemInfo;
import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;
import hk.org.ha.model.pms.vo.vetting.PharmOrderItemInfo;

import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;
import org.jboss.seam.security.Identity;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;

@AutoCreate
@Stateless
@Name("autoDispManager")
@TransactionManagement(TransactionManagementType.BEAN)
public class AutoDispManagerBean implements AutoDispManagerLocal {

	private static EclipseLinkXStreamMarshaller marshaller = new EclipseLinkXStreamMarshaller();

	@Logger
	private Log logger;
		
	@In
	private Identity identity;

	@In
	private Uam uam;

	@In
	private SysProfile sysProfile;

	@In
	private WorkstationManagerLocal workstationManager;

	@In
	private RefTableManagerLocal refTableManager;
	
	@In
	private PostLogonServiceLocal postLogonService;
	
	@In
	private TicketServiceLocal ticketService;	

	@In
	private OneStopServiceLocal oneStopService;
	
	@In
	private OrderViewServiceLocal orderViewService;


	@In
	private DrugPickListServiceLocal drugPickListService;

	@In
	private DrugPickServiceLocal drugPickService;
	
	@In
	private AssemblingListServiceLocal assemblingListService;

	@In
	private AssemblingServiceLocal assemblingService;

	@In
	private CheckIssueServiceLocal checkIssueService;
	
	@In
	private CddhServiceLocal cddhService;

	@In
	private DrugSearchServiceLocal drugSearchService;
	
	public void login(String username, Workstore workstore, String workstationCode) throws Exception 
	{
		if (!identity.isLoggedIn()) 
		{
			identity.getCredentials().setUsername(username);
			identity.login();

			if (uam.getUamInfo() == null)
			{
				Workstation workstation = workstationManager.retrieveWorkstation(workstore, workstationCode);
				
				UamInfo uamInfo = new UamInfo();
				uamInfo.setUserId(username);
				uamInfo.setWorkstationId(workstation.getHostName());
				uamInfo.setHospital(workstore.getHospCode());
				uamInfo.setWorkstore(workstore.getWorkstoreCode());
				uamInfo.setAppCode("PMS");
				uamInfo.setUserRole("__DEBUG__");
				uamInfo.setProfileCode("PMS_" + workstore.getHospCode());
				uamInfo.setPermissionMap(new HashMap<String,String>());
				uam.setUamInfo(uamInfo);
				
				sysProfile.setDebugEnabled(true);
			}
			
			postLogonService.postLogon(uam.getUamInfo().getHospital(), uam.getUamInfo().getWorkstore());		
		}

		if (logger.isDebugEnabled())  {
			logger.debug("uamInfo : \n#0\n", marshaller.toXML(uam.getUamInfo()));
		}
		
		Workstation workstation = this.get("workstation");
		if (workstation == null) {
			throw new Exception("Workstation not found!");
		}
	}
	
	public OperationProfile getActiveProfile(Workstore workstore) {
		return refTableManager.retrieveActiveProfile(workstore);
	}
		
	public void genTicket() 
	{
		ticketService.retrieveTicketByTicketType(TicketType.Queue1);
		
		TicketGenResult ticketGenResult = this.get("ticketGenResult");
		
		if (logger.isDebugEnabled()) {
			logger.debug("ticketGenResult : \n#0\n", marshaller.toXML(ticketGenResult));		
		}

		if (ticketGenResult == null) {
			throw new RuntimeException("Ticket not found!");
		}
	}
	
	public void retrieveOrder(String orderNum) {

		oneStopService.retrieveOrder("MOE" + orderNum, Boolean.FALSE);
		
		OneStopSummary oneStopSummary = this.get("oneStopSummary");				
		OneStopOrderItemSummary itemSummary = oneStopSummary.getOneStopOrderItemSummaryList().get(0);

		if (logger.isDebugEnabled()) {
			logger.debug("oneStopSummary : \n#0\n", marshaller.toXML(oneStopSummary));
		}
								
		OneStopOrderInfo oneStopOrderInfo = this.get("oneStopOrderInfo");
		List<String> oneStopSpecialtyList = this.get("oneStopSpecialtyList");
		List<String> oneStopPatientCatList = this.get("oneStopPatientCatList");
		
		oneStopOrderInfo.setDoctorCode(itemSummary.getDoctorCode());
		oneStopOrderInfo.setMedCase(itemSummary.getMedCase());
		oneStopOrderInfo.setPatType(itemSummary.getPatType());

		if (!StringUtils.isEmpty(itemSummary.getPhsSpecCode())) {
			oneStopOrderInfo.setPhsSpecCode(itemSummary.getPhsSpecCode());
		} else {
			if (oneStopSpecialtyList.isEmpty()) {
				throw new RuntimeException("Missing oneStopSpecialtyList!");
			}
			oneStopOrderInfo.setPhsSpecCode(oneStopSpecialtyList.get(0));
		}
		
		if (!StringUtils.isEmpty(itemSummary.getPatCatCode())) {
			oneStopOrderInfo.setPhsPatCat(itemSummary.getPatCatCode());
		} else {
			if (oneStopPatientCatList.isEmpty()) {
				throw new RuntimeException("Missing oneStopPatientCatList!");
			}
			if (!StringUtils.isEmpty(oneStopPatientCatList.get(0))) {
				oneStopOrderInfo.setPhsPatCat(oneStopPatientCatList.get(0));
			} else {
				oneStopOrderInfo.setPhsPatCat(oneStopPatientCatList.get(1));
			}
		}
		
		oneStopOrderInfo.setPrescType(itemSummary.getPrescType());
		oneStopOrderInfo.setPrivateFlag(itemSummary.getPrivateFlag());
		oneStopOrderInfo.setStandardForceProceed(Boolean.FALSE);

		if (logger.isDebugEnabled()) {
			logger.debug("oneStopOrderInfo : \n#0\n", marshaller.toXML(oneStopOrderInfo));
		}
	}
	
	public void retrievePatientAndMedCase(String patientHkid, String patientName) {
		oneStopService.retrieveHkPmiPatientByOrderNumOrRefNumOrTickNum();
		
		OneStopSummary oneStopSummary = this.get("oneStopSummary");
		
		Patient defaultPatient = (Patient) this.get("defaultPatient");
		if (defaultPatient.getPatKey() == null) {
			defaultPatient.setHkid(patientHkid);
			defaultPatient.setName(patientName);
			defaultPatient.setSex(Gender.Male);
		}
		
		oneStopSummary.setPatient(defaultPatient);
	}
	
	public void checkTicket() {
				
		OneStopSummary oneStopSummary = this.get("oneStopSummary");				
		OneStopOrderItemSummary itemSummary = oneStopSummary.getOneStopOrderItemSummaryList().get(0);

		TicketGenResult ticketGenResult = this.get("ticketGenResult");
		ticketService.retrieveTicketByTicketNum(
				ticketGenResult.getTicket().getTicketDate(), 
				ticketGenResult.getTicket().getTicketNum(), 
				itemSummary.getId(),
				itemSummary.getOneStopOrderType());										
	}
	
	public void startVetting() throws Exception {
		
		OneStopSummary oneStopSummary = this.get("oneStopSummary");				
		OneStopOrderItemSummary itemSummary = oneStopSummary.getOneStopOrderItemSummaryList().get(0);
		
		orderViewService.retrieveOrder(itemSummary.getId(), OneStopOrderType.MedOrder, true);
	}
	
	public void removeItemsIfNotConverted() throws Exception {
		
		OrderViewInfo orderViewInfo = this.get("orderViewInfo");
		PharmOrder pharmOrder = (PharmOrder) this.get("pharmOrder");

		boolean isAllRemoved = true;
		
		int i = 0;
		for (MedOrderItem moi : pharmOrder.getMedOrder().getMedOrderItemList()) {
			if (moi.getPharmOrderItemList().size() == 0) {
				orderViewService.removeMedOrderItem(orderViewInfo, i, 
						new Date(), 
						identity.getCredentials().getUsername(), 
						"remove unconverted medOrder item");
			} else {
				isAllRemoved = false;
			}
			i++;
		}
		
		if (isAllRemoved) {
			throw new Exception("All Item Removed, cannot continue!");
		}
	}
	
	public void endVetting() throws Exception {
		OrderViewInfo orderViewInfo = this.get("orderViewInfo");
		PharmOrder pharmOrder = (PharmOrder) this.get("pharmOrder");

		// update from front-end
		updateOrderViewInfo(orderViewInfo, pharmOrder.getMedOrder());
		
		// save order
		orderViewService.saveOrder(orderViewInfo);
		if (!orderViewInfo.getErrorMsgCodeList().isEmpty()) {
			throw new Exception("Error during save " + StringUtils.join(orderViewInfo.getErrorMsgCodeList().toArray(),","));
		}

		// confirm duplicate item
		List<MedOrderItem> localDuplicateItemList = this.get("localDuplicateItemList");
		if (localDuplicateItemList != null && !localDuplicateItemList.isEmpty()) {
			orderViewInfo.setSkipDuplicateCheckFlag(true);
			orderViewService.saveOrder(orderViewInfo);
			if (!orderViewInfo.getErrorMsgCodeList().isEmpty()) {
				throw new Exception("Error during save " + StringUtils.join(orderViewInfo.getErrorMsgCodeList().toArray(),","));
			}
		}
	}
	
	private void updateOrderViewInfo(OrderViewInfo orderViewInfo, MedOrder medOrder) {
		
		orderViewInfo.getMedOrderItemInfoList().clear();
		orderViewInfo.getPharmOrderItemInfoList().clear();

		for (MedOrderItem moi : medOrder.getMedOrderItemList()) {
			orderViewInfo.getMedOrderItemInfoList().add(constructMedOrderItemInfo(moi));
			for (PharmOrderItem poi : moi.getPharmOrderItemList()) {
				orderViewInfo.getPharmOrderItemInfoList().add(constructPharmOrderItemInfo(poi));
			}
		}
	}

	private MedOrderItemInfo constructMedOrderItemInfo(MedOrderItem moi) {
		MedOrderItemInfo info = new MedOrderItemInfo();
		info.setVetFlag(Boolean.TRUE);
		info.setStatus(moi.getStatus());
		info.setItemNum(moi.getItemNum());
		info.setNumOfLabel(Integer.valueOf(1));
		return info;
	}

	private PharmOrderItemInfo constructPharmOrderItemInfo(PharmOrderItem poi) {
		
		PharmOrderItemInfo info = new PharmOrderItemInfo();
		
		if (poi.getIssueQty() == null) {
			info.setIssueQty(poi.getCalQty().setScale(0, RoundingMode.UP));
		} else {
			info.setIssueQty(poi.getIssueQty());
		}
		info.setSfiQty(poi.getSfiQty());
		
		info.setWarnCode1(poi.getWarnCode1());
		info.setWarnCode2(poi.getWarnCode2());
		info.setWarnCode3(poi.getWarnCode3());
		info.setWarnCode4(poi.getWarnCode4());
		
		info.setItemNum(poi.getItemNum());
		
		info.setDirtyFlag(poi.isDirtyFlag());
		
		info.setHandleExemptCode(poi.getHandleExemptCode());
		info.setNextHandleExemptCode(poi.getNextHandleExemptCode());
		info.setHandleExemptUser(poi.getHandleExemptUser());
		
		if (!poi.getRefillScheduleItemList().isEmpty()) {
			info.setRefillScheduleItemId(poi.getRefillScheduleItemList().get(0).getId());
		}
		
		return info;
	}	
	
	public void drugPick() {
		TicketGenResult ticketGenResult = this.get("ticketGenResult");
		
		List<Integer> itemNumList = drugPickListService.retrieveAvaliableItemNumList(
				ticketGenResult.getTicket().getTicketDate(), 
				ticketGenResult.getTicket().getTicketNum());

		for (Integer itemNum : itemNumList) {
			Workstation workstation = this.get("workstation");
			drugPickService.updateDispOrderItemStatusToPicked(
					ticketGenResult.getTicket().getTicketDate(), 
					ticketGenResult.getTicket().getTicketNum(),
					itemNum,
					identity.getCredentials().getUsername(), 
					workstation.getId());
		}
	}
	
	public void assembling() {
		
		TicketGenResult ticketGenResult = this.get("ticketGenResult");
		
		List<Integer> itemNumList = assemblingListService.retrieveAvaliableItemNumList(
				ticketGenResult.getTicket().getTicketDate(), 
				ticketGenResult.getTicket().getTicketNum());

		for (Integer itemNum : itemNumList) {
			assemblingService.updateDispOrderItemStatusToAssembled(
					ticketGenResult.getTicket().getTicketDate(), 
					ticketGenResult.getTicket().getTicketNum(),
					itemNum);
		}
	}
	
	public void checkOrder() throws Exception {
		TicketGenResult ticketGenResult = this.get("ticketGenResult");
		CheckIssueViewInfo checkIssueViewInfo = new CheckIssueViewInfo();
		checkIssueViewInfo.setTicketDate(ticketGenResult.getTicket().getTicketDate());
		checkIssueViewInfo.setTicketNum(ticketGenResult.getTicket().getTicketNum());
		checkIssueViewInfo.setIssueWindowNum(0);
		checkIssueViewInfo.setAutoCheckIssue(true);
		checkIssueViewInfo.setAutoRefresh(false);
		checkIssueViewInfo.setRefreshIssuedList(false);
		checkIssueViewInfo.setCheckIssueListDate(new Date());
		checkIssueViewInfo.setOtherCheckListDate(new Date());
		checkIssueViewInfo.setIssuedListDate(new Date());
		
		CheckIssueSummary checkIssueSummary = checkIssueService.retrievePrescDetail(checkIssueViewInfo);
		
		PrescDetail prescDetail = checkIssueSummary.getPrescDetail();
		if (prescDetail != null && prescDetail.getDispOrderStatus() == DispOrderStatus.Checked) {
			checkIssueService.updateDispOrderStatusToIssued(checkIssueViewInfo, false);
		}
	}
	
	public void issueOrder() throws Exception {
		TicketGenResult ticketGenResult = this.get("ticketGenResult");
		CheckIssueViewInfo checkIssueViewInfo = new CheckIssueViewInfo();
		checkIssueViewInfo.setTicketDate(ticketGenResult.getTicket().getTicketDate());
		checkIssueViewInfo.setTicketNum(ticketGenResult.getTicket().getTicketNum());
		checkIssueViewInfo.setIssueWindowNum(0);
		checkIssueViewInfo.setAutoCheckIssue(true);
		checkIssueViewInfo.setAutoRefresh(false);
		checkIssueViewInfo.setRefreshIssuedList(false);
		checkIssueViewInfo.setCheckIssueListDate(new Date());
		checkIssueViewInfo.setOtherCheckListDate(new Date());
		checkIssueViewInfo.setIssuedListDate(new Date());
		
		CheckIssueSummary checkIssueSummary = checkIssueService.retrievePrescDetail(checkIssueViewInfo);
		
		PrescDetail prescDetail = checkIssueSummary.getPrescDetail();
		if (prescDetail != null && prescDetail.getDispOrderStatus() == DispOrderStatus.Checked) {
			checkIssueService.updateDispOrderStatusToIssued(checkIssueViewInfo, false);
		}
	}
	
	public void cddhSearch(String patientHkid, String patientName) {
				
		cddhService.createCddhCriteria();
		CddhCriteria cddhCriteria = this.get("cddhCriteria");
		
		if (StringUtils.isNotBlank(patientHkid)) {
			cddhCriteria.setHkid(patientHkid);
			
			cddhService.retrieveDispOrderItem(cddhCriteria);
			
			String errMsg = this.get("errMsg");
			if (errMsg == null) {
				return;
			} else {				
				logger.info("PAS is not avaliable! hkid:#0", patientHkid);
			}
		}

		if (StringUtils.isNotBlank(patientName)) {
	
			DateMidnight fromDate = new DateMidnight();
			DateTime toDate = fromDate.toDateTime().plusDays(1).minus(1);				 
	
			cddhCriteria.setHkid(null);
			cddhCriteria.setNameOnLabel(patientName);		
			cddhCriteria.setLabelDispDateStart(fromDate.toDate());
			cddhCriteria.setLabelDispDateEnd(toDate.toDate());
	
			cddhService.retrieveDispOrderLikePatName(cddhCriteria);			
			List<CddhDispOrder> cddhDispOrderList = this.get("cddhDispOrderList");		
			
			logger.info("Avalible Patient List! size:#0", cddhDispOrderList.size());
			
			for (int i = cddhDispOrderList.size(); i > 0; i--) {
				CddhDispOrder cddhDispOrder = cddhDispOrderList.get(i-1);
				if (cddhDispOrder.getDoCddhExceptionFlag()) {
					
					logger.info("Selected patient:#0 dispOrderId:#1", cddhDispOrder.getPoName(), cddhDispOrder.getDoId());

					cddhCriteria.setDispOrderId(cddhDispOrder.getDoId());
					cddhCriteria.setDispHospCode(cddhDispOrder.getDoHospCode());
					cddhCriteria.setDispOrderPatName(cddhDispOrder.getPoName());
					cddhCriteria.setPatListDispDate(cddhDispOrder.getDoIssueDate());
					cddhCriteria.setExcept(cddhDispOrder.getDoCddhExceptionFlag());
					cddhService.retrieveDispOrderItem(cddhCriteria);	
					break;
				}
			}
		}
	
	}

	public void drugSearch(String drugName) {
		
		RelatedDrugSearchCriteria relatedDrugSearchCriteria = new RelatedDrugSearchCriteria();
		relatedDrugSearchCriteria.setFirstDisplayname(drugName);
		relatedDrugSearchCriteria.setContainSearch(Boolean.FALSE);
		relatedDrugSearchCriteria.setDrugScope(DrugScope.IpDrugList.getDataValue());
		
		RouteFormSearchCriteria routeFormSearchCriteria = new RouteFormSearchCriteria();
		routeFormSearchCriteria.setSpecialtyType("O");
		routeFormSearchCriteria.setPasSpecialty("");
		routeFormSearchCriteria.setPasSubSpecialty("");
		routeFormSearchCriteria.setDrugScope(DrugScope.IpDrugList.getDataValue());
		
		drugSearchService.drugSearch(DrugSearchSource.OrderEdit, relatedDrugSearchCriteria, routeFormSearchCriteria);	
	}

	public void logout() 
	{
		if (identity.isLoggedIn()) 
		{
			identity.logout();
		}
	}
		
	@SuppressWarnings("unchecked")
	protected <T> T get(String name) {
		return (T) Contexts.getSessionContext().get(name);
	}

	protected <T> void set(String name, T o) {
		Contexts.getSessionContext().set(name, o);
	}
}
