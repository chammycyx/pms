package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.model.pms.dms.vo.IngredientSearchCriteria;
import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
import hk.org.ha.model.pms.vo.drug.DrugSearchReturn;

import javax.ejb.Local;

@Local
public interface DrugSearchIngredientServiceLocal {
	
	DrugSearchReturn retrieveDrugSearchIngredient(DrugSearchSource drugSearchSource, IngredientSearchCriteria ingredientSearchCriteria);

	void destroy();
}
