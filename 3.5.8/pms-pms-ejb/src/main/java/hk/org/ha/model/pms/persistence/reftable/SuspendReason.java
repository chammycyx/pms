package hk.org.ha.model.pms.persistence.reftable;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.udt.RecordStatus;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "SUSPEND_REASON")
@Customizer(AuditCustomizer.class)
public class SuspendReason extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "suspendReasonSeq")
	@SequenceGenerator(name = "suspendReasonSeq", sequenceName = "SQ_SUSPEND_REASON", initialValue = 100000000)
	private Long id;

	@Column(name = "SUSPEND_CODE", nullable = false, length = 2)
	private String suspendCode;
	
	@Column(name = "DESCRIPTION", length = 44)
	private String description;

	@ManyToOne
	@JoinColumn(name = "HOSP_CODE", nullable = false)
	private Hospital hospital;
    
    @Converter(name = "SuspendReason.status", converterClass = RecordStatus.Converter.class)
    @Convert("SuspendReason.status")
	@Column(name="STATUS", nullable = false, length = 1)
	private RecordStatus status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSuspendCode() {
		return suspendCode;
	}

	public void setSuspendCode(String suspendCode) {
		this.suspendCode = suspendCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Hospital getHospital() {
		return hospital;
	}

	public void setHospital(Hospital hospital) {
		this.hospital = hospital;
	}

	public RecordStatus getStatus() {
		return status;
	}

	public void setStatus(RecordStatus status) {
		this.status = status;
	}
}
