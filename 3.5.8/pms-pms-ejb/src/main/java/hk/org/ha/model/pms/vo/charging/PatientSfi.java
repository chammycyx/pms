package hk.org.ha.model.pms.vo.charging;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="PatientSfi")
@ExternalizedBean(type=DefaultExternalizer.class)
public class PatientSfi implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@XmlElement
	private String hkid;
	
	@XmlElement(name="patientSfiDtl")
	private List<PatientSfiDtl> patientSfiDtlList;

	public PatientSfi() {
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public List<PatientSfiDtl> getPatientSfiDtlList() {
		return patientSfiDtlList;
	}

	public void setPatientSfiDtlList(List<PatientSfiDtl> patientSfiDtlList) {
		this.patientSfiDtlList = patientSfiDtlList;
	}

}
