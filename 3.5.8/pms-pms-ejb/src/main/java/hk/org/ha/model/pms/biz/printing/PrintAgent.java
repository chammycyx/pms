package hk.org.ha.model.pms.biz.printing;

import hk.org.ha.fmk.pms.report.ByteArrayDocumentData;
import hk.org.ha.fmk.pms.report.ReportData;
import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.persistence.reftable.WorkstationPrint;
import hk.org.ha.model.pms.prop.Prop;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.printing.PrintType;
import hk.org.ha.model.pms.vo.printing.PrintJobInfo;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.PrinterSelect;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.contexts.Lifecycle;
import org.jboss.seam.document.DocumentData;
import org.jboss.seam.document.DocumentData.DocumentType;
import org.jboss.seam.log.Log;

import commonj.work.Work;
import commonj.work.WorkManager;

@AutoCreate
@Name("printAgent")
@Scope(ScopeType.SESSION)
@MeasureCalls
public class PrintAgent implements PrintAgentInf {

	private static final long serialVersionUID = 1L;

	private static final String SUBREPORT_DIR_KEY = "SUBREPORT_DIR";

	private static final String REPORT_DIR = "report/";
	
	@Logger
	private Log logger;
	
	@In
	private ReportProvider<JRDataSource> reportProvider;
		
	@In
	private SilentPrintSenderInf silentPrintSender;
	
	@In
	private WorkManager printWorkManager;
		
	private DocumentData keepData;
			
	public void renderAndPrint(RenderAndPrintJob job)
	{
		this.renderAndPrint(Arrays.asList(job));
	}
	
	public void renderAndPrint(List<RenderAndPrintJob> jobs)
	{
		renderAndPrint(jobs, true); 
	}
	
	public void renderAndPrint(List<RenderAndPrintJob> jobs, boolean includeLotInfo)
	{
		for (RenderAndPrintJob job : jobs) {
			this.updateDocTypeIfEmpty(job, ReportProvider.PDF);
		}
		
		printWorkManager.schedule(
				new RenderAndPrintWork(
						logger, reportProvider, silentPrintSender, jobs, includeLotInfo));
	}
	
	public void zipAndRedirect(RenderJob job, String password) throws IOException, ZipException {
		String contentId = this.render(job);
		
		String zipContentId = renderZipContentId(contentId, job.getDocName(), password);
		
		this.redirect(zipContentId);
	}
	
	public void zipAndRedirect(RenderJob job, String fileName, String password) throws IOException, ZipException {
		String contentId = this.render(job);
		
		String zipContentId = renderZipContentId(contentId, fileName, password);
		
		this.redirect(zipContentId, fileName);
	}
	
	private String renderZipContentId(String contentId, String filename, String password) throws IOException, ZipException {
        ByteArrayDocumentData dd = (ByteArrayDocumentData) reportProvider.getDocumentStore().getDocumentData(contentId);
        
        File tempDir = new File(System.getProperty("java.io.tmpdir") + System.currentTimeMillis());
        if (!tempDir.mkdirs()) {
        	throw new IOException("Cannot create directory : " + tempDir);
        }
        
        File f = new File(tempDir,filename + "." + dd.getDocumentType().getExtension());
        FileUtils.writeByteArrayToFile(f, dd.getData());

        ArrayList<File> fl = new ArrayList<File>();
        fl.add(f);
        File zipFile = generateZipFile(fl, password);
        
        String zipContentId = reportProvider.getDocumentStore().newId();
        
        ByteArrayDocumentData zipdd = new ByteArrayDocumentData(filename,ReportProvider.ZIP, FileUtils.readFileToByteArray(zipFile),1);        
        reportProvider.getDocumentStore().saveData(zipContentId, zipdd);
        
        if (!zipFile.delete()) {
           	throw new IOException("Cannot delete zip file : " + zipFile);
        }
        
        FileUtils.deleteDirectory(tempDir);
        
        return zipContentId;
	}

	public File generateZipFile(List<File> fileArrayList, String password) throws ZipException {
		
		File tempFile = createTempFile("", ".zip");
		
		try {

			ZipFile zipFile = new ZipFile(tempFile);

			ZipParameters parameters = new ZipParameters();
			parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
			parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);

			parameters.setEncryptFiles(true);
			parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_STANDARD);
			parameters.setPassword(password);

			zipFile.addFiles(new ArrayList<File>(fileArrayList), parameters);

			return tempFile;
			
		} catch (ZipException e) {
			
			tempFile.delete();
			throw e;
		}
	}
	
	private File createTempFile(String prefix, String suffix) {
		SecureRandom random = new SecureRandom();
		long n = random.nextLong();
		if (n == Long.MIN_VALUE) {
			n = 0; // corner case
		} else {
			n = Math.abs(n);
		}
		return new File(System.getProperty("java.io.tmpdir"), prefix + Long.toString(n) + suffix);
	}
	
	public void renderAndRedirect(RenderJob job) {
		String contentId = this.render(job);		
		this.redirect(contentId);
	}
		
	public void renderAndRedirect(RenderJob job, String fileName) {
		String contentId = this.render(job);		
		this.redirect(contentId, fileName);
	}

	public String render(RenderJob job) {
		
		String template = this.prepareTemplate(job);

		this.updateDocTypeIfEmpty(job, 
				"PDF".equals(Prop.REPORT_PREVIEW_FORMAT.get()) ? 
						ReportProvider.PDF : ReportProvider.PNG);

		return reportProvider.generateReport(
				template, 
				job.getParameters(), 
				job.getPrintOption().getDocType(),
				new JRBeanCollectionDataSource(job.getObjectList()));
	}
			
	private String prepareTemplate(RenderJob job) {
				
		this.reflectSetPrintOption(job.getObjectList(), job.getPrintOption());
		
		if (!job.getParameters().containsKey(SUBREPORT_DIR_KEY)) {
			job.getParameters().put(SUBREPORT_DIR_KEY, REPORT_DIR + job.getDocName() +"/");
		}

		StringBuilder sb = new StringBuilder();
		sb.append(REPORT_DIR);
		sb.append(job.getDocName());
		if (job.getPrintOption().getPrintType() != PrintType.Normal) {
			sb.append(job.getPrintOption().getPrintType().getDisplayValue());
		}
		if (job.getPrintOption().getPrintLang() != PrintLang.Eng) {
			sb.append(job.getPrintOption().getPrintLang().toString());
		}
		sb.append(".jasper");	
		
		return sb.toString();
	}
	
	private void updateDocTypeIfEmpty(RenderJob job, DocumentType docType) {
		if (job.getPrintOption().getDocType() == null) {
			job.getPrintOption().setDocType(docType);
		}
	}
		
	public void keep(String contentId) {
		keepData = reportProvider.getDocumentStore().getDocumentData(contentId);
	}
	
	public void print(PrinterSelect printerSelect, String printInfo) {
		if (keepData != null) {
			if (keepData.getDocumentType() == ReportProvider.PDF) {
				this.print(keepData, printerSelect, printInfo);
			} else {
				throw new UnsupportedOperationException("unable to print non-PDF document!");
			}
		} else {
			throw new UnsupportedOperationException("please keep your content first!");
		}
	}

	public void print(String contentId, PrinterSelect printerSelect, String printInfo) {
		DocumentData data = reportProvider.getDocumentStore().getDocumentData(contentId);
		if (data.getDocumentType() == ReportProvider.PDF) {
			this.print(data, printerSelect, printInfo);
		} else {
			throw new UnsupportedOperationException("unable to print non-PDF document!");
		}
	}

	private void print(DocumentData data, PrinterSelect printerSelect, String printInfo) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			data.writeDataToStream(baos);		
			
			Workstation workstation = printerSelect.getResolvedPrinterWorkstation();
			if (workstation != null) {
				silentPrintSender.sendToPrinter(new PrintJobInfo(
						baos.toByteArray(),
						printerSelect, 
						findPrinterName(workstation, printerSelect),
						printInfo));
			}
		} catch (IOException e) {
			logger.error("unexpected error", e);
		}
	}
	
	public void redirect(String contentId) {
		reportProvider.redirectReport(contentId);
	}

	public void redirect(String contentId, String fileName) {
		reportProvider.redirectReport(contentId, fileName);
	}
	
	private void reflectSetPrintOption(List<?> objectList, PrintOption printOption) {
		String methodName = "printOption";
		if (!objectList.isEmpty()) {
			if (PropertyUtils.isWriteable(objectList.get(0), methodName)) {			
				for (Object o : objectList) {
					try {
						PropertyUtils.setProperty(o, methodName, printOption);
					} catch (IllegalAccessException e) {
					} catch (InvocationTargetException e) {
					} catch (NoSuchMethodException e) {
					}
				}
			}
		}
	}
	
	private String findPrinterName(Workstation workstation, PrinterSelect printerSelect) {
		if (printerSelect.getPrintDocType() != null) {
			WorkstationPrint workstationPrint = workstation.findWorkstationPrint(printerSelect.getPrintDocType());
			if (workstationPrint.getType() == hk.org.ha.model.pms.udt.reftable.PrintType.Printer && 
					StringUtils.isNotBlank(workstationPrint.getValue())) {
				return workstationPrint.getValue();
			}
		}
		return null;
	}	
	
	private class RenderAndPrintWork implements Work {

		private Log logger;
		private ReportProvider<JRDataSource> reportProvider;
		private SilentPrintSenderInf silentPrintSender;
		private List<RenderAndPrintJob> jobs;
		private boolean includeLotInfo;
		
	    private boolean createContexts;

	    public RenderAndPrintWork(
				Log logger,
				ReportProvider<JRDataSource> reportProvider,
				SilentPrintSenderInf silentPrintSender,
				List<RenderAndPrintJob> jobs,
				boolean includeLotInfo
	    		) {
			this.logger = logger;
			this.reportProvider = reportProvider;
			this.silentPrintSender = silentPrintSender;			
			this.jobs = jobs;
			this.includeLotInfo = includeLotInfo;

			this.createContexts = !Contexts.isApplicationContextActive();
		}

		@Override
		public void run() {
			
	         setup();
	         try
	         {
	            process();
	         }
	         finally
	         {
	            cleanup();
	         }
			
		}

		private void setup() {
			if (createContexts) {
				Lifecycle.beginCall();
			}
		}

		private void cleanup() {
			if (createContexts) {
				Lifecycle.endCall();
			}
		}

		private void process() {
			
			long startTime = 0;
			
			if (logger.isDebugEnabled()) {
				startTime = System.currentTimeMillis();			
				logger.debug("[#0] start print work", 
						Thread.currentThread().getId());
			}
			
			int i = 0;
			
			Map<String,List<PrintJobInfo>> groupingMap = new LinkedHashMap<String,List<PrintJobInfo>>();
			
			for (RenderAndPrintJob job : jobs) {
				
				++i;
				
				if (logger.isDebugEnabled()) {
					logger.debug("[#0] print job #1 for #2", Thread.currentThread().getId(), i, job.getDocName());
				}

				String template = prepareTemplate(job);
				
				try {
					ReportData ra = reportProvider.generateReportData(
							template, 
							job.getParameters(), 
							job.getPrintOption().getDocType(), 
							new JRBeanCollectionDataSource(job.getObjectList()));
					
					Workstation workstation = job.getPrinterSelect().getResolvedPrinterWorkstation();
					
					if (workstation != null) 
					{
						PrintJobInfo printJobInfo = new PrintJobInfo(
								ra.getData(),
								job.getPrinterSelect(),
								findPrinterName(workstation, job.getPrinterSelect()),
								job.getPrintInfo(),
								job.getPrintDocType());
						
						if (includeLotInfo)
						{
							String hostName = workstation.getHostName();
							List<PrintJobInfo> list = groupingMap.get(hostName);
							if (list == null) {
								list = new ArrayList<PrintJobInfo>();
								groupingMap.put(hostName, list);
							}
							list.add(printJobInfo);
						}
						else
						{
							silentPrintSender.sendToPrinter(printJobInfo);
						}
					}
					
				} catch (RuntimeException e) {
					logger.error("[#0] error during render and print job #1 for #2", e, Thread.currentThread().getId(), i, job.getDocName());
					throw e;
				}
			}
			
			if (includeLotInfo) {
				for (Map.Entry<String, List<PrintJobInfo>> entry : groupingMap.entrySet()) {
					
					String lotNum = UUID.randomUUID().toString();
					int totalItem = entry.getValue().size();
					
					int currentItem = 1;
					for (PrintJobInfo printJobInfo : entry.getValue()) 
					{
						printJobInfo.setLotNum(lotNum);
						printJobInfo.setTotalItem(totalItem);
						printJobInfo.setCurrentItem(currentItem++);
						
						try {
							silentPrintSender.sendToPrinter(printJobInfo);
							
						} catch (RuntimeException e) {
							logger.error("[#0] error during send #1 #2/#3 to printer #4", e, Thread.currentThread().getId(), 
									printJobInfo.getLotNum(),
									printJobInfo.getCurrentItem(),
									printJobInfo.getTotalItem(),
									printJobInfo.getPrinterName());
							throw e;
						}
					}
				}
			}
			
			if (logger.isDebugEnabled()) {
				logger.debug("[#0] end print work, timeUsed #1",
						Thread.currentThread().getId(),
						System.currentTimeMillis() - startTime);
			}
		}
		

		@Override
		public boolean isDaemon() {
			return false;
		}

		@Override
		public void release() {
		}
		
	}
}
