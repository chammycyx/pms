package hk.org.ha.model.pms.biz.checkissue.mp;

import javax.ejb.Local;

@Local
public interface SendListServiceLocal {
	
	void retrieveSentList();
	
	void destroy();
	
}