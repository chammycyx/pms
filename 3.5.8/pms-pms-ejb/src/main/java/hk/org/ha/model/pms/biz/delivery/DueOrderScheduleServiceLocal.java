package hk.org.ha.model.pms.biz.delivery;

import hk.org.ha.model.pms.vo.delivery.ScheduleTemplateInfo;
import javax.ejb.Local;

@Local
public interface DueOrderScheduleServiceLocal {
	ScheduleTemplateInfo retrieveDeliveryScheduleList();
	ScheduleTemplateInfo changeDeliveryScheduleTemplate(String templateName);
	void destroy();
}
