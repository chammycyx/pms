package hk.org.ha.model.pms.biz.vetting;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.moe.MoeException;
import hk.org.ha.model.pms.dms.vo.PmsFmStatusSearchCriteria;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderFm;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.vo.vetting.FmIndication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.drools.util.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.joda.time.DateTime;

@Stateful
@Scope(ScopeType.SESSION)
@Name("fmIndicationService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class FmIndicationServiceBean implements FmIndicationServiceLocal {
	
	@In
	private FmIndicationManagerLocal fmIndicationManager; 
	
	@In(required = false)
	private PharmOrder pharmOrder;
	
	@In(required = false)
	private MedProfile medProfile;
	
	@Out(required = false)
	private Boolean isSpecialDrug;
	
	@PersistenceContext
	private EntityManager em;
	
	public List<FmIndication> retrieveFmIndicationListById(Long medOrderItemId) throws MoeException {
		MedOrderItem medOrderItem = em.find(MedOrderItem.class, medOrderItemId);
		medOrderItem.loadDmInfo();
		return retrieveFmIndicationList(medOrderItem.getMedOrder(), medOrderItem, medOrderItem.getMedOrder().getMedOrderFmList());
	}
	
	public List<FmIndication> retrieveFmIndicationListByMedOrderItem(MedOrderItem medOrderItem) throws MoeException {
		List<FmIndication> fmIndicationList = retrieveFmIndicationList(pharmOrder.getMedOrder(), medOrderItem, pharmOrder.getMedOrder().getMedOrderFmList());
		if (fmIndicationList != null) {
			for (FmIndication fmIndication : fmIndicationList) {
				((MedOrderFm)fmIndication.getFmEntity()).setMedOrder(null);
			}
		}
		return fmIndicationList;
	}
	
	private List<FmIndication> retrieveFmIndicationList(MedOrder medOrder, MedOrderItem medOrderItem, List<MedOrderFm> medOrderFmList) throws MoeException {
		List<FmIndication> fmIndicationList = fmIndicationManager.retrieveLocalFmIndicationListByMedOrderItem(medOrderItem, medOrderFmList);

		return validateFmIndicationList(medOrder.getPatHospCode(), fmIndicationList);
	}
	
	public List<FmIndication> retrieveFmIndicationListByMedProfileMoItem(MedProfileMoItem medProfileMoItem) throws MoeException {
		List<FmIndication> fmIndicationList = fmIndicationManager.retrieveLocalFmIndicationListByMedProfileMoItem(medProfileMoItem, medProfileMoItem.getMedProfileMoItemFmList());

		return validateFmIndicationList(medProfile.getPatHospCode(), fmIndicationList);
	}
	
	private List<FmIndication> validateFmIndicationList(String patHospCode, List<FmIndication> fmIndicationList) {
		List<FmIndication> validFmIndicationList = new ArrayList<FmIndication>();
		Map<String,String> prescibeDoctorNameMap = new HashMap<String, String>();
		
		for (FmIndication fmIndication : fmIndicationList) {
			// validate FM corp ind and set indValidPeriodForDisplay by DMS valid period 
			boolean validFmCorpIndByDms = fmIndicationManager.validateFmCorpIndByDms(fmIndication.getRxDrug(), fmIndication.getFmEntity());	

			if (validFmCorpIndByDms) {
				boolean compatible = false;
				
				List<ActionStatus> comPatActionStatusList = ActionStatus.getCompatActionStatusByPrescOption(fmIndication.getFmEntity().getPrescOption());
				for (ActionStatus actionStatus : comPatActionStatusList) {
					if (actionStatus == fmIndication.getRxDrug().getActionStatus()) {
						compatible = true;
						break;
					}
				}
					
				if (compatible) {
					DateTime authDateTime = new DateTime(fmIndication.getFmEntity().getFmCreateDate()).plusMonths(fmIndication.getFmEntity().getIndValidPeriodForDisplay());
					fmIndication.setAuthDate(authDateTime.toDate());
					StringBuilder presDoctorNameBuilder = new StringBuilder();
					
					if( prescibeDoctorNameMap.get(fmIndication.getFmEntity().getFmCreateUser()) != null ){
						presDoctorNameBuilder.append(prescibeDoctorNameMap.get(fmIndication.getFmEntity().getFmCreateUser()));
						presDoctorNameBuilder.append(" (");
						presDoctorNameBuilder.append(fmIndication.getFmEntity().getFmCreateUser());
						presDoctorNameBuilder.append(")");
						fmIndication.setPrescibingDoctorName(presDoctorNameBuilder.toString());
					}else{
						String presDoctorName = fmIndicationManager.retrievePrescibingDoctorNameFromMoeUserCode(patHospCode, fmIndication.getFmEntity().getFmCreateUser()); 
						if( !StringUtils.isEmpty(presDoctorName) ){
							presDoctorNameBuilder.append(presDoctorName);
						}
						presDoctorNameBuilder.append(" (");
						presDoctorNameBuilder.append(fmIndication.getFmEntity().getFmCreateUser());
						presDoctorNameBuilder.append(")");
						fmIndication.setPrescibingDoctorName(presDoctorNameBuilder.toString());
						prescibeDoctorNameMap.put(fmIndication.getFmEntity().getFmCreateUser(), fmIndication.getPrescibingDoctorName());
					}
					
					validFmIndicationList.add(fmIndication);
				}
			}
		}
		
		if (validFmIndicationList.isEmpty()) {
			return null;
		} else {
			return validFmIndicationList;
		}
	}
	
	public void checkFmIndicationForOrderEdit(PmsFmStatusSearchCriteria criteria) {
		isSpecialDrug = fmIndicationManager.checkSpecialDrug(criteria);
	}

	@Remove
	public void destroy() {
		if ( isSpecialDrug != null ) {
			isSpecialDrug = null;
		}
	}
}
