package hk.org.ha.model.pms.biz.checkissue.mp;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.reftable.WardManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.udt.medprofile.DeliveryItemStatus;
import hk.org.ha.model.pms.vo.checkissue.mp.AssignBatchSummaryItem;
import hk.org.ha.model.pms.vo.label.MpDispLabel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.joda.time.DateTime;

@Stateful
@Scope(ScopeType.SESSION)
@Name("assignBatchListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class AssignBatchListServiceBean implements AssignBatchListServiceLocal {
	
	private static final String JAXB_CONTEXT_LABEL = "hk.org.ha.model.pms.vo.label";

	
	@PersistenceContext
	private EntityManager em;

	@In
	private Workstore workstore;
	
	@In
	private CheckManagerLocal checkManager;
	
	@In
	private WardManagerLocal wardManager;
	
	@Out(required = false)
	private List<AssignBatchSummaryItem> unlinkedDeliveryItemList;
	
	@Out(required = false)
	private List<String> assignBatchWardList;
	
	public void retrieveWardList(){
		assignBatchWardList = wardManager.retrieveWardCodeList();
	}
	
	@SuppressWarnings("unchecked")
	public void retrieveUnlinkedDeliveryItemList(Date batchDate){
		unlinkedDeliveryItemList = new ArrayList<AssignBatchSummaryItem>();
		List<DeliveryItem> deliveryItemList = em.createQuery(
				"select o from DeliveryItem o" +// 20120312 index check : Delivery.hospital,batchDate : I_DELIVERY_01
				" where o.delivery.hospital = :hospital" +
				" and o.delivery.batchDate between :fromDate and :toDate" +
				" and o.status = :status")
				.setParameter("hospital", workstore.getHospital())
				.setParameter("fromDate", new DateTime(batchDate).minusDays(1).toDate())
				.setParameter("toDate", batchDate)
				.setParameter("status", DeliveryItemStatus.Unlink)
				.getResultList();
		
		if ( !deliveryItemList.isEmpty() ) {
			for ( DeliveryItem deliveryItem : deliveryItemList ) {
				AssignBatchSummaryItem assignBatchItem = new AssignBatchSummaryItem();
				unlinkedDeliveryItemList.add(assignBatchItem);
				convertToAssignBatchItem(deliveryItem, assignBatchItem);
			}
			Collections.sort(unlinkedDeliveryItemList, new UnlinkedDeliveryItemComparator());
		}
	}
	
	private void convertToAssignBatchItem(DeliveryItem deliveryItem, AssignBatchSummaryItem assignBatchSummaryItem) {
		
		JaxbWrapper<MpDispLabel> labelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
		
		MedProfilePoItem medProfilePoItem = deliveryItem.resolveMedProfilePoItem();
		
		assignBatchSummaryItem.setDeliveryItemId(deliveryItem.getId());
		assignBatchSummaryItem.setCaseNum(medProfilePoItem.getMedProfileMoItem().getMedProfileItem().getMedProfile().getMedCase().getCaseNum());
		
		
		MpDispLabel mpDispLabel = labelJaxbWrapper.unmarshall(deliveryItem.getLabelXml());
		assignBatchSummaryItem.setWardCode(mpDispLabel.getWard());
		assignBatchSummaryItem.setBedNum(mpDispLabel.getBedNum());
		Patient patient = medProfilePoItem.getMedProfileMoItem().getMedProfileItem().getMedProfile().getPatient();
		
		if(patient.getNameChi() == null || patient.getNameChi().isEmpty()){
			assignBatchSummaryItem.setPatientName(patient.getName());
			assignBatchSummaryItem.setHavePatientChiName(false);
		}else{
			assignBatchSummaryItem.setPatientName(patient.getNameChi());
			assignBatchSummaryItem.setHavePatientChiName(true);
		}
		
		assignBatchSummaryItem.setItemCode(medProfilePoItem.getItemCode());
		assignBatchSummaryItem.setDrugName(medProfilePoItem.getDrugName());
		assignBatchSummaryItem.setIssueQty(deliveryItem.getIssueQty());
		assignBatchSummaryItem.setBaseUnit(medProfilePoItem.getBaseUnit());
		assignBatchSummaryItem.setDoseUnit(medProfilePoItem.getDoseUnit());
		assignBatchSummaryItem.setFormLabelDesc(medProfilePoItem.getFormLabelDesc());
		assignBatchSummaryItem.setStrength(medProfilePoItem.getStrength());
		assignBatchSummaryItem.setVolumeText(medProfilePoItem.getVolumeText());
		assignBatchSummaryItem.setException(checkManager.getProblemItemException(deliveryItem));
	}
	
	public static class UnlinkedDeliveryItemComparator implements Comparator<AssignBatchSummaryItem>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(AssignBatchSummaryItem item1, AssignBatchSummaryItem item2) {
			return new CompareToBuilder().append( item1.getCaseNum(), item2.getCaseNum() )
										 .append( item1.getWardCode(), item2.getWardCode() )
										 .append( item1.getPatientName(), item2.getPatientName())
										 .append( item1.getBedNum(), item2.getBedNum() )
										 .append( item1.getItemCode(), item2.getItemCode() )
										 .append( item1.getIssueQty(), item2.getIssueQty() )
										 .toComparison();
		}		
	}
	
	@Remove
	public void destroy() {
		if ( assignBatchWardList != null ) {
			assignBatchWardList = null;
		}
		if ( unlinkedDeliveryItemList != null ) {
			unlinkedDeliveryItemList = null;
		}
	}

}
