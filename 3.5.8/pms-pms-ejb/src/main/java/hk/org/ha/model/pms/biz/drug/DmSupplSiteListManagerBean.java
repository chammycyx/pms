package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmSupplSiteCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmSupplSite;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateless;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("dmSupplSiteListManager")
@MeasureCalls
public class DmSupplSiteListManagerBean implements DmSupplSiteListManagerLocal {

	@Logger
	private Log logger;
	
	@In
	private DmSupplSiteCacherInf dmSupplSiteCacher; 
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private List<DmSupplSite> dmSupplSiteList;
	
	public void retrieveDmSupplSiteList() {
		logger.debug("retrieveDmSupplSiteList");
		dmSupplSiteList = dmSupplSiteCacher.getSupplSiteList();
		if ( dmSupplSiteList != null ) {
			logger.debug("dmSupplSiteList size #0", dmSupplSiteList.size());
		}
	}
	
	@Remove
	public void destroy() 
	{
		if (dmSupplSiteList != null) {
			dmSupplSiteList = null;
		}
	}
}
