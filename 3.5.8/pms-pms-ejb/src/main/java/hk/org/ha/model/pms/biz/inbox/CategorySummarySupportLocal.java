package hk.org.ha.model.pms.biz.inbox;
import hk.org.ha.model.pms.persistence.corp.Workstore;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface CategorySummarySupportLocal {

	List<CategoryCount> retrieveOrderTypeCategoryCountList(Workstore workstore, Boolean showChemoProfileOnly, List<String> wardCodeList, Date now);
	List<CategoryCount> retrieveAdminTypeCategoryCountList(Workstore workstore, Boolean showChemoProfileOnly, List<String> wardCodeList, Date now);
	Long countErrorStat(Workstore workstore, List<String> wardCodeList, Date now);
	Long countDischargeMedOrder(Workstore workstore, List<String> wardCodeList, Date now);
	Long countCancelDischargeTransferCount(Workstore workstore);
}
