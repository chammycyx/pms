package hk.org.ha.model.pms.biz.inbox;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.medprofile.MedProfileManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("manualProfileService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ManualProfileServiceBean implements ManualProfileServiceLocal {

	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;	
	
	@In
	private MedProfileManagerLocal medProfileManager;
	
	@In
	private ManualProfileManagerLocal manualProfileManager;
	
	@In
	private Workstore workstore;
	
	@Out(required = false)
	private List<String> manualProfileWardCodeList;
	
	@Out(required = false)
	private List<String> manualProfileSpecCodeList;
	
    public ManualProfileServiceBean() {
    }
    
    @Override
    public MedProfile constructManualProfile(String hkid, String caseNum) {

    	MedProfile medProfile = manualProfileManager.constructManualProfile(hkid, caseNum, workstore);
    	retrieveSpecCodeListAndWardCodeList();
    	
    	return medProfile;
    }
    
    @Override
    public void retrieveSpecCodeListAndWardCodeList() {
    	manualProfileSpecCodeList = retrieveSpecCodeList();
    	manualProfileWardCodeList = retrieveWardCodeList();
    }
    
    @Override
    public Patient retrievePasPatient(String hkid) {
    	return medProfileManager.retrievePasPatient(hkid);
    }
    
    @SuppressWarnings("unchecked")
	private List<String> retrieveWardCodeList() {
    	
    	return em.createQuery(
    			"select o.wardCode from Ward o" + // 20150206 index check : Ward.institution : FK_WARD_01
    			" where o.institution = :institution" +
    			" and o.status = :activeStatus" +
    			" order by o.wardCode")
    			.setParameter("institution", workstore.getWorkstoreGroup().getInstitution())
    			.setParameter("activeStatus", RecordStatus.Active)
    			.getResultList();
    	
    }
    
    @SuppressWarnings("unchecked")
	private List<String> retrieveSpecCodeList() {

    	return em.createQuery(
    			"select o.specCode from Specialty o" + // 20150206 index check : Specialty.institution : FK_SPECIALTY_01
    			" where o.institution = :institution" +
    			" and o.status = :activeStatus" +
    			" order by o.specCode")
    			.setParameter("institution", workstore.getWorkstoreGroup().getInstitution())
    			.setParameter("activeStatus", RecordStatus.Active)
    			.getResultList();
    }
    
	@Remove    
	@Override
	public void destroy() {
		manualProfileWardCodeList = null;
		manualProfileSpecCodeList = null;
	}
}
