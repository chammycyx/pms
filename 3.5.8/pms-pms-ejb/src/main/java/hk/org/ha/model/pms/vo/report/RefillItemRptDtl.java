package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class RefillItemRptDtl implements Serializable {

	private static final long serialVersionUID = 1L;

	private String refillCouponNum;
	
	private String caseNum;
	
	private String patName;
	
	private String hkid;
	
	private String otherDoc;
	
	private String orderNum;	
	
	private String refNum;
	
	private String fullDrugDesc;
	
	private Date refillDate;
	
	private String overDuePeriod;
	
	private String refillNum;
	
	private String numOfRefill;
	
	private String doWorkstoreCode;
	
	private String specCode;
	
	private String itemTotalDuration;
	
	private String totalQty;
	
	private String baseUnit;
	
	private String refillDuration;
	
	private String refillQty;

	private String adjQty;
	
	private String status;
	
	private Double avgUnitPrice;
	
	private String durationUnit;
	
	private Date dispDate;
	
	private String drsWorkstoreCode;

	public String getRefillCouponNum() {
		return refillCouponNum;
	}

	public void setRefillCouponNum(String refillCouponNum) {
		this.refillCouponNum = refillCouponNum;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getOtherDoc() {
		return otherDoc;
	}

	public void setOtherDoc(String otherDoc) {
		this.otherDoc = otherDoc;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public String getRefNum() {
		return refNum;
	}

	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}

	public String getFullDrugDesc() {
		return fullDrugDesc;
	}

	public void setFullDrugDesc(String fullDrugDesc) {
		this.fullDrugDesc = fullDrugDesc;
	}

	public Date getRefillDate() {
		return refillDate;
	}

	public void setRefillDate(Date refillDate) {
		this.refillDate = refillDate;
	}

	public String getOverDuePeriod() {
		return overDuePeriod;
	}

	public void setOverDuePeriod(String overDuePeriod) {
		this.overDuePeriod = overDuePeriod;
	}

	public String getRefillNum() {
		return refillNum;
	}

	public void setRefillNum(String refillNum) {
		this.refillNum = refillNum;
	}

	public String getNumOfRefill() {
		return numOfRefill;
	}

	public void setNumOfRefill(String numOfRefill) {
		this.numOfRefill = numOfRefill;
	}

	public String getDoWorkstoreCode() {
		return doWorkstoreCode;
	}

	public void setDoWorkstoreCode(String doWorkstoreCode) {
		this.doWorkstoreCode = doWorkstoreCode;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public String getItemTotalDuration() {
		return itemTotalDuration;
	}

	public void setItemTotalDuration(String itemTotalDuration) {
		this.itemTotalDuration = itemTotalDuration;
	}

	public String getTotalQty() {
		return totalQty;
	}

	public void setTotalQty(String totalQty) {
		this.totalQty = totalQty;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public String getRefillDuration() {
		return refillDuration;
	}

	public void setRefillDuration(String refillDuration) {
		this.refillDuration = refillDuration;
	}

	public String getRefillQty() {
		return refillQty;
	}

	public void setRefillQty(String refillQty) {
		this.refillQty = refillQty;
	}

	public String getAdjQty() {
		return adjQty;
	}

	public void setAdjQty(String adjQty) {
		this.adjQty = adjQty;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Double getAvgUnitPrice() {
		return avgUnitPrice;
	}

	public void setAvgUnitPrice(Double avgUnitPrice) {
		this.avgUnitPrice = avgUnitPrice;
	}

	public int hashCode() {
		return new HashCodeBuilder()
			.append(refillCouponNum)
			.append(status)
			.toHashCode();
	}

	public boolean equals(Object obj) {
		RefillItemRptDtl o = (RefillItemRptDtl) obj;
		return new EqualsBuilder()
					.append(refillCouponNum, o.refillCouponNum)
					.append(status, o.status)
					.isEquals();
	}
	
	public int compareTo(Object obj) {
		RefillItemRptDtl o = (RefillItemRptDtl) obj;
		return new CompareToBuilder()
					.append(refillCouponNum, o.refillCouponNum)
					.append(status, o.status)
				.toComparison();
	}

	public void setDispDate(Date dispDate) {
		this.dispDate = dispDate;
	}

	public Date getDispDate() {
		return dispDate;
	}

	public void setDurationUnit(String durationUnit) {
		this.durationUnit = durationUnit;
	}

	public String getDurationUnit() {
		return durationUnit;
	}
	
	public void setDrsWorkstoreCode(String drsWorkstoreCode) {
		this.drsWorkstoreCode = drsWorkstoreCode;
	}
	
	public String getDrsWorkstoreCode() {
		return drsWorkstoreCode;
	}
	
}
