package hk.org.ha.model.pms.biz.charging;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.sys.SysProfile;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.validation.SearchTextValidatorLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.persistence.reftable.ChargeReason;
import static hk.org.ha.model.pms.prop.Prop.*;
import hk.org.ha.model.pms.vo.charging.FcsDowntimePaymentInfo;
import hk.org.ha.model.pms.vo.charging.SfiForceProceedInfo;
import hk.org.ha.model.pms.uam.udt.AuthorizeStatus;
import hk.org.ha.model.pms.uam.vo.AuthenticateCriteria;
import hk.org.ha.model.pms.udt.charging.DowntimePaymentStatus;
import hk.org.ha.model.pms.udt.disp.InvoiceDocType;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.validation.SearchTextType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.service.biz.pms.uam.interfaces.UamServiceJmsRemote;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("sfiForceProceedService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SfiForceProceedServiceBean implements SfiForceProceedServiceLocal {
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@Out(required=false)
	private SfiForceProceedInfo sfiForceProceedInfo;
	
	@Out(required=false)
	private List<ChargeReason> forceProceedReasonList;
	
	@In
	private UamServiceJmsRemote uamServiceProxy;
	
	@In
	private SearchTextValidatorLocal searchTextValidator;
	
	@In
	private DrugChargeClearanceManagerLocal drugChargeClearanceManager; 
	
	@In
	private UamInfo uamInfo;
	
	@In
	private SysProfile sysProfile;
	
	@In
	private Workstore workstore;
	
	private String errMsg;
	
	private String errMsgDetail;
	
	private DateFormat sfiInvoiceYearFormat = new SimpleDateFormat("yy");
	
	@SuppressWarnings("unchecked")
	public void retrieveForceProceedChargeReasonList(){
		forceProceedReasonList = (List<ChargeReason>) em.createQuery(
				"select o from ChargeReason o " +  // 20120214 index check : none
				"where o.type = 'F' " +
				"order by o.id")
				.getResultList();
	}
	
	public void validateSfiForceProceedEntry(SfiForceProceedInfo sfiForceProceedEntry){
		setErrMsg(StringUtils.EMPTY);
		setErrMsgDetail(StringUtils.EMPTY);
		sfiForceProceedInfo = null;
		
		if( !StringUtils.isEmpty(sfiForceProceedEntry.getReceiptNum()) || !StringUtils.isEmpty(sfiForceProceedEntry.getInvoiceNum()) ){
			if( !isValidInputNumber( sfiForceProceedEntry )){
				return;
			}
		}
		
		//Check Authorize User From Uam
		AuthorizeStatus authStatus = authSfiForceProccedUser(sfiForceProceedEntry);
		logger.debug("validateSfiForceProceedEntry: User Role #0, OR Authorize Status #1", uamInfo.getUserRole(), authStatus);
				
		sfiForceProceedInfo = createSfiForceProceedInfo(authStatus, sfiForceProceedEntry);
	}
	
	@SuppressWarnings("unchecked")
	private boolean isValidSfiInvoice(Long dispOrderId, String sfiInvoiceNum){
		//check downtime no. digit
		String invoiceNumDigit = sfiInvoiceNum.substring(6, 7);
		if( StringUtils.equals(invoiceNumDigit, "3") || StringUtils.equals(invoiceNumDigit, "4") ){
			return true;
		}else{
			//Invoice to be paid
			DispOrder dispOrder = em.find(DispOrder.class, dispOrderId);
			dispOrder.loadChild();
			em.clear();
			
			Invoice sfiInvoice = null;
			
			for( Invoice invoice : dispOrder.getInvoiceList() ){
				if( invoice.getDocType() == InvoiceDocType.Sfi){
					sfiInvoice = invoice;
					break;
				}
			}
			
			//Invoice Num from searchText
			List<Invoice> searchInvList = (List<Invoice>) em.createQuery(
															"select o from Invoice o" + // 20120215 index check : Invoice.invoiceNum : I_INVOICE_01
															" where o.docType = :invoiceDocType" +
															" and o.dispOrder.pharmOrder.medOrder.orderType = :medOrderType" +
															" and o.dispOrder.workstore = :workstore" +
															" and o.status not in :invoiceStatusList" +
															" and o.invoiceNum = :invoiceNum")
																		
																			.setParameter("invoiceDocType", InvoiceDocType.Sfi)
																			.setParameter("medOrderType", OrderType.OutPatient)
																			.setParameter("workstore", workstore)
																			.setParameter("invoiceStatusList", Arrays.asList(InvoiceStatus.SysDeleted))
																			.setParameter("invoiceNum", sfiInvoiceNum)
																			.getResultList();
			
			if( searchInvList.size() > 0 ){
				Invoice searchInv = searchInvList.get(0);
				if( !StringUtils.equals(searchInv.getDispOrder().getPharmOrder().getHkid(), sfiInvoice.getDispOrder().getPharmOrder().getHkid()) ) {
					setErrMsg("0623");
					setErrMsgDetail(sfiInvoiceNum);
					return false;
				}else if( StringUtils.equals(searchInv.getInvoiceNum(), sfiInvoice.getInvoiceNum()) ){
					setErrMsg("0624");
					return false;
				}else{
					Invoice prevSfiInvoice = null;
					Long prevSfiInvoiceId = sfiInvoice.getPrevInvoiceId();
					while( prevSfiInvoiceId != null ){
						prevSfiInvoice = em.find(Invoice.class, prevSfiInvoiceId);
						if( StringUtils.equals(prevSfiInvoice.getInvoiceNum(), sfiInvoice.getInvoiceNum()) ){
							prevSfiInvoiceId = prevSfiInvoice.getPrevInvoiceId();
							prevSfiInvoice = null;
						}else{
							prevSfiInvoiceId = null;
						}
					}
					
					if( prevSfiInvoice != null && StringUtils.equals(searchInv.getInvoiceNum(), prevSfiInvoice.getInvoiceNum()) ){
						return true;
					}
				}
				
				Ticket searchInvTicket = searchInv.getDispOrder().getTicket();
				Ticket sfiInvTicket = sfiInvoice.getDispOrder().getTicket();
				
				if( StringUtils.equals(searchInvTicket.getWorkstore().getHospCode(), sfiInvTicket.getWorkstore().getHospCode()) &&
					StringUtils.equals(searchInvTicket.getWorkstore().getWorkstoreCode(), sfiInvTicket.getWorkstore().getWorkstoreCode()) && 
					(searchInvTicket.getTicketDate().compareTo(sfiInvTicket.getTicketDate()) == 0 ) &&
					StringUtils.equals(searchInvTicket.getTicketNum(), sfiInvTicket.getTicketNum()) ){
					return true;
				}else if( InvoiceStatus.Void == searchInv.getStatus() ){
					setErrMsg("0625");
					setErrMsgDetail(sfiInvoiceNum);
					return false;
				}else{
					setErrMsg("0623");
					setErrMsgDetail(sfiInvoiceNum);
					return false;
				}
			}
			setErrMsg("0088");
			return false;
		}
	}
	
	private boolean isValidInputNumber(SfiForceProceedInfo sfiForceProceedEntry){
		
		String searchType = "I";
		String searchText = StringUtils.EMPTY;
		
		if( !StringUtils.isEmpty(sfiForceProceedEntry.getReceiptNum()) ){			
			if(!isValidReceiptNum(sfiForceProceedEntry.getReceiptNum())){
				setErrMsg("0230");
				return false;
			}
			searchType = "R";
			searchText = sfiForceProceedEntry.getReceiptNum();
			
		}else if( !StringUtils.isEmpty(sfiForceProceedEntry.getInvoiceNum()) ){
			
			searchText = sfiForceProceedEntry.getInvoiceNum();
			
			SearchTextType searchTextType = searchTextValidator.checkSearchInputType(searchText);
			if( !SearchTextType.SfiInvoice.equals(searchTextType) ){
				setErrMsg("0231");
				return false;				
			}

			String invoiceHospCode = StringUtils.trim(searchText.substring(1, 4));
			if( !StringUtils.equals(workstore.getHospCode(), invoiceHospCode) ){
				setErrMsg("0232");
				return false;
			}
			searchType = "I";
		}
		
		FcsDowntimePaymentInfo dtPaymentStatus = null;
		
		if( sfiForceProceedEntry.getDispOrderId() != null ){
			dtPaymentStatus = this.retrieveDowntimePaymentStatusByDispOrder(sfiForceProceedEntry.getDispOrderId(), searchType, searchText);
		}else if( sfiForceProceedEntry.getHkid() != null ){
			dtPaymentStatus = this.retrieveDowntimePaymentStatusForIp(sfiForceProceedEntry.getPatHospCode(), sfiForceProceedEntry.getMpInvoiceNum(), sfiForceProceedEntry.getHkid(), sfiForceProceedEntry.getCaseNum(), searchType, searchText);
		}
		
		if( dtPaymentStatus != null ){
			logger.info("isValidInputNumber dtPaymentStatus: #0, #1", dtPaymentStatus.getFcsDowntimePaymentStatus().getDisplayValue(), dtPaymentStatus.getFcsReturnMessage());
			
			if( DowntimePaymentStatus.NumberIsValid == dtPaymentStatus.getFcsDowntimePaymentStatus() ){
				return true;
			}else{
				if( StringUtils.isNotBlank(dtPaymentStatus.getFcsReturnMessage()) ){
					setErrMsg("0247");
					setErrMsgDetail(dtPaymentStatus.getFcsReturnMessage());
				}else if( DowntimePaymentStatus.Error == dtPaymentStatus.getFcsDowntimePaymentStatus() ){
					if( StringUtils.equals(searchType, "R") ){
						return true;
					}else if( StringUtils.equals(searchType, "I") ){
						return isValidSfiInvoice(sfiForceProceedEntry.getDispOrderId(), searchText);
					}
				}
				return false;
			}
		}
		return false;
	}
	
	private FcsDowntimePaymentInfo retrieveDowntimePaymentStatusForIp(String patHospCode, String invoiceNum, String hkid, String caseNum, String searchType, String searchText){
		String mpInvoiceNum = invoiceNum;
		if( mpInvoiceNum == null ){
			StringBuilder dummyInvBuilder = new StringBuilder();
			dummyInvBuilder.append("I");
			dummyInvBuilder.append(workstore.getHospCode());
			if ( workstore.getHospCode().length() == 2 ){
				dummyInvBuilder.append(" ");
			}
			dummyInvBuilder.append(sfiInvoiceYearFormat.format(new Date()));
			dummyInvBuilder.append("1");
			dummyInvBuilder.append("0");
			
			mpInvoiceNum = dummyInvBuilder.toString();
		}
		FcsDowntimePaymentInfo dtPaymentStatus = drugChargeClearanceManager.retrieveDowntimePaymentStatus(patHospCode,mpInvoiceNum, hkid, caseNum, searchType, searchText);
		logger.info("retrieveDowntimePaymentStatusForIp dtPaymentStatus: #0, #1, #2", mpInvoiceNum, dtPaymentStatus.getFcsDowntimePaymentStatus().getDisplayValue(), dtPaymentStatus.getFcsReturnMessage());
		return dtPaymentStatus;
	}
	
	private FcsDowntimePaymentInfo retrieveDowntimePaymentStatusByDispOrder(Long dispOrderId, String searchType, String searchText){
		DispOrder dispOrder = em.find(DispOrder.class, dispOrderId);
		dispOrder.loadChild();
		em.clear();
		
		String invoiceNum = StringUtils.EMPTY;
		
		for( Invoice invoice : dispOrder.getInvoiceList() ){
			if( invoice.getDocType() == InvoiceDocType.Sfi){
				invoiceNum = invoice.getInvoiceNum();
				break;
			}
		}
		
		PharmOrder pharmOrder = dispOrder.getPharmOrder();

		String caseNum = StringUtils.EMPTY;
		if( pharmOrder.getMedCase() != null ){
			caseNum = pharmOrder.getMedCase().getCaseNum();
		}
		
		String hkid = StringUtils.EMPTY;
		if( pharmOrder.getPatient() != null ){
			hkid = pharmOrder.getPatient().getHkid();
		}
		
		FcsDowntimePaymentInfo dtPaymentStatus = drugChargeClearanceManager.retrieveDowntimePaymentStatus(pharmOrder.getMedOrder().getPatHospCode(), 
																					 invoiceNum, hkid, caseNum, searchType, searchText);		
																					 
		return dtPaymentStatus;
	}
	
	private boolean isValidReceiptNum(String receiptNum){
		if(receiptNum.length() <= 20){	
			if( isValidReceiptPrefix(receiptNum) ){
				return true;
			}else if( receiptNum.length() == 8 || receiptNum.length() ==9 ){
				try{
					Integer.parseInt(receiptNum);
				}catch( NumberFormatException nfe ){
					return false;
				}
				return true;				
			}
		}
		return false;
	}
	
	private boolean isValidReceiptPrefix(String receiptNum){
		List<String> prefixList = new ArrayList<String>();
		String prefixString = CHARGING_SFI_RECEIPT_RECEIPTNUM_PREFIX.get();
		
		if( prefixString != null && prefixString.length() > 0 ){
			for( int i=0; i< prefixString.length(); i++ ){
				if( i == prefixString.length()-1 ){
					prefixList.add(prefixString.substring(i));
				}else{
					prefixList.add(prefixString.substring(i, i+1));
				}
			}
		}
		
		for( String prefix: prefixList ){
			if( receiptNum.startsWith(prefix) ){
				return true;
			}
		}
		return false;
	}
	
	private AuthorizeStatus authSfiForceProccedUser(SfiForceProceedInfo sfiForceProceedEntry){
		AuthenticateCriteria authenticateCriteria = new AuthenticateCriteria();
		authenticateCriteria.setProfileCode(uamInfo.getProfileCode());
		authenticateCriteria.setUserName(sfiForceProceedEntry.getForceProceedBy());
		authenticateCriteria.setPassword(sfiForceProceedEntry.getForceProceedPw());
		authenticateCriteria.setTarget("sfiForceProceed");
		authenticateCriteria.setAction("Y");
		
		return uamServiceProxy.authenticate(authenticateCriteria);
	}
	
	private SfiForceProceedInfo createSfiForceProceedInfo(AuthorizeStatus authStatus, SfiForceProceedInfo sfiForceProceedEntry){
		SfiForceProceedInfo forceProceedInfo = new SfiForceProceedInfo();
		forceProceedInfo.setForceProceedFlag(true);
		forceProceedInfo.setActionBy(sfiForceProceedEntry.getActionBy());
		forceProceedInfo.setForceProceedBy(sfiForceProceedEntry.getForceProceedBy());
		
		if( sysProfile.isDebugEnabled() || AuthorizeStatus.Succeed.equals(authStatus) ){
			
			if( !StringUtils.isEmpty(sfiForceProceedEntry.getForceProceedReason())  ){			
				forceProceedInfo.setForceProceedReason(sfiForceProceedEntry.getForceProceedReason());			
			}else if( !StringUtils.isEmpty(sfiForceProceedEntry.getReceiptNum()) ){			
				forceProceedInfo.setReceiptNum(sfiForceProceedEntry.getReceiptNum());
			}else if( !StringUtils.isEmpty(sfiForceProceedEntry.getInvoiceNum()) ){
				forceProceedInfo.setInvoiceNum(sfiForceProceedEntry.getInvoiceNum());
			}else{
				return null;				
			}
		}else if ( AuthorizeStatus.Invalid.equals(authStatus) ){			
			setErrMsg("0091");
			return null;
		}else{
			setErrMsg("0170");
			return null;
		}
		return forceProceedInfo;
	}
	
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public String getErrMsg() {
		return errMsg;
	}
	
	public void setErrMsgDetail(String errMsgDetail) {
		this.errMsgDetail = errMsgDetail;
	}

	public String getErrMsgDetail() {
		return errMsgDetail;
	}
		
	@Remove
	public void destroy() 
	{
		if ( forceProceedReasonList != null){
			forceProceedReasonList = null;
		}
		
		if( sfiForceProceedInfo != null ){
			sfiForceProceedInfo = null;
		}
	}
}
