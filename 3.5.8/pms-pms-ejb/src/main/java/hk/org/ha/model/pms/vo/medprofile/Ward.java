package hk.org.ha.model.pms.vo.medprofile;

import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class Ward implements Serializable, Comparable<Ward> {

	private static final long serialVersionUID = 1L;

	private String wardCode;
	
	private RecordStatus status;
	
	private Boolean selected;

	private Boolean manageByCurrentWorkstore;
	
	private Boolean mpWardFlag;
	
	public Ward() {
		super();
	}
	
	public Ward(String wardCode) {
		this(wardCode, RecordStatus.Active);
	}
	
	public Ward(String wardCode, RecordStatus status) {
		super();
		this.wardCode = wardCode;
		this.status = status;
		this.selected = Boolean.FALSE;
		this.manageByCurrentWorkstore = Boolean.FALSE;
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}
	
	public RecordStatus getStatus() {
		return status;
	}

	public void setStatus(RecordStatus status) {
		this.status = status;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public Boolean getManageByCurrentWorkstore() {
		return manageByCurrentWorkstore;
	}

	public void setManageByCurrentWorkstore(Boolean manageByCurrentWorkstore) {
		this.manageByCurrentWorkstore = manageByCurrentWorkstore;
	}
	
	public Boolean getMpWardFlag() {
		return mpWardFlag;
	}

	public void setMpWardFlag(Boolean mpWardFlag) {
		this.mpWardFlag = mpWardFlag;
	}

	@Override
	public int compareTo(Ward o) {
		return MedProfileUtils.compare(this.wardCode, o == null ? null : o.wardCode);
	}
}
