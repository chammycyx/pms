package hk.org.ha.model.pms.biz.assembling;

import hk.org.ha.model.pms.vo.assembling.AssembleOrder;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface AssemblingListServiceLocal {
	
	List<AssembleOrder> retrieveAssembleOrderList();
	
	List<Integer> retrieveAvaliableItemNumList(Date ticketDate, String ticketNum);	
	
	void destroy();
	
}
