package hk.org.ha.model.pms.biz.order;

import hk.org.ha.service.biz.pms.interfaces.PmsServiceJmsRemote;

import javax.ejb.Local;

@Local
public interface PmsServiceLocal extends PmsServiceJmsRemote {
}
