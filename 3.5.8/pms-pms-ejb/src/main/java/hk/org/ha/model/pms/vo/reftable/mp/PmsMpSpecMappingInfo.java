package hk.org.ha.model.pms.vo.reftable.mp;

import hk.org.ha.model.pms.dms.persistence.PmsIpSpecMapping;
import hk.org.ha.model.pms.persistence.corp.HospitalMapping;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.phs.Specialty;
import hk.org.ha.model.pms.persistence.phs.Ward;
import hk.org.ha.model.pms.vo.patient.PasWard;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PmsMpSpecMappingInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<PmsMpSpecMappingHdr> pmsMpSpecMappingHdrList;
	
	private List<Workstore> pharmacyWorkstoreList;

	private List<Specialty> phsSpecialtyList;
	
	private List<Ward> wardList;
	
	private List<HospitalMapping> pmsSpecMappingHospitalMappingList;
	
	private List<String> patHospList;
	
	private List<PmsIpSpecMapping> pmsMpSpecMappingList;
		
	private List<PasWard> pasWardList;
	
	public List<PmsMpSpecMappingHdr> getPmsMpSpecMappingHdrList() {
		return pmsMpSpecMappingHdrList;
	}

	public void setPmsMpSpecMappingHdrList(List<PmsMpSpecMappingHdr> pmsMpSpecMappingHdrList) {
		this.pmsMpSpecMappingHdrList = pmsMpSpecMappingHdrList;
	}

	public List<Workstore> getPharmacyWorkstoreList() {
		return pharmacyWorkstoreList;
	}

	public void setPharmacyWorkstoreList(List<Workstore> pharmacyWorkstoreList) {
		this.pharmacyWorkstoreList = pharmacyWorkstoreList;
	}

	public List<Specialty> getPhsSpecialtyList() {
		return phsSpecialtyList;
	}

	public void setPhsSpecialtyList(List<Specialty> phsSpecialtyList) {
		this.phsSpecialtyList = phsSpecialtyList;
	}

	public List<Ward> getWardList() {
		return wardList;
	}

	public void setWardList(List<Ward> wardList) {
		this.wardList = wardList;
	}

	public List<HospitalMapping> getPmsSpecMappingHospitalMappingList() {
		return pmsSpecMappingHospitalMappingList;
	}

	public void setPmsSpecMappingHospitalMappingList(List<HospitalMapping> pmsSpecMappingHospitalMappingList) {
		this.pmsSpecMappingHospitalMappingList = pmsSpecMappingHospitalMappingList;
	}

	public void setPatHospList(List<String> patHospList) {
		this.patHospList = patHospList;
	}

	public List<String> getPatHospList() {
		return patHospList;
	}

	public void setPmsMpSpecMappingList(List<PmsIpSpecMapping> pmsMpSpecMappingList) {
		this.pmsMpSpecMappingList = pmsMpSpecMappingList;
	}

	public List<PmsIpSpecMapping> getPmsMpSpecMappingList() {
		return pmsMpSpecMappingList;
	}

	public void setPasWardList(List<PasWard> pasWardList) {
		this.pasWardList = pasWardList;
	}

	public List<PasWard> getPasWardList() {
		return pasWardList;
	}
}