package hk.org.ha.model.pms.persistence.medprofile;

import java.util.Arrays;
import java.util.List;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.medprofile.TpnRequestStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "TPN_REQUEST")
public class TpnRequest extends VersionEntity {
	
	private static final long serialVersionUID = 1L;
	
	public static final String CDDH_STATUS_NEW = "N";
	
	public static final String CDDH_STATUS_MODIFY = "M";
	
	public static final String CDDH_STATUS_DELETE = "D";
	
	public static final List<String> CDDH_STATUS_NEW_MODIFY = Arrays.asList(TpnRequest.CDDH_STATUS_NEW, TpnRequest.CDDH_STATUS_MODIFY);
	
	public static final List<String> CDDH_STATUS_DELETE_MODIFY = Arrays.asList(TpnRequest.CDDH_STATUS_DELETE, TpnRequest.CDDH_STATUS_MODIFY);
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tpnRequestSeq")
	@SequenceGenerator(name = "tpnRequestSeq", sequenceName = "SQ_TPN_REQUEST", initialValue = 100000000)
	private Long id;
	
	@Column(name = "DURATION")
	private Integer duration;
	
	@Converter(name = "TpnRequest.status", converterClass = TpnRequestStatus.Converter.class)
    @Convert("TpnRequest.status")
	@Column(name="STATUS", nullable = false, length = 1)
	private TpnRequestStatus status;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MED_PROFILE_ID", nullable = true)
	private MedProfile medProfile;

	@Transient
	private boolean modifyFlag = false;
	
	@Transient
	private String cddhStatus;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public TpnRequestStatus getStatus() {
		return status;
	}

	public void setStatus(TpnRequestStatus status) {
		this.status = status;
	}

	public MedProfile getMedProfile() {
		return medProfile;
	}

	public void setMedProfile(MedProfile medProfile) {
		this.medProfile = medProfile;
	}
	
	public boolean getModifyFlag() {
		return modifyFlag;
	}

	public void setModifyFlag(boolean modifyFlag) {
		this.modifyFlag = modifyFlag;
	}

	public String getCddhStatus() {
		return cddhStatus;
	}

	public void setCddhStatus(String cddhStatus) {
		this.cddhStatus = cddhStatus;
	}
}