package hk.org.ha.model.pms.udt.medprofile;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum MedProfileOrderStatus implements StringValuedEnum {
	
	Pending("P", "Pending"),
	Completed("C", "Completed"),
	Error("E", "Error");

    private final String dataValue;
    private final String displayValue;
    
	private MedProfileOrderStatus(String dataValue, String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}

	@Override
	public String getDataValue() {
		return dataValue;
	}

	@Override
	public String getDisplayValue() {
		return displayValue;
	}
	
    public static MedProfileOrderStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MedProfileOrderStatus.class, dataValue);
	}
    
	public static class Converter extends StringValuedEnumConverter<MedProfileOrderStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<MedProfileOrderStatus> getEnumClass() {
    		return MedProfileOrderStatus.class;
    	}
    }
}
