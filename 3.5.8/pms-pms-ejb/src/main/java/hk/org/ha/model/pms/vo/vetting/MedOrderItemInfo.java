package hk.org.ha.model.pms.vo.vetting;

import java.io.Serializable;

import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class MedOrderItemInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private Boolean showWarnCodeFlag;
	
	private Boolean showExemptCodeFlag;
	
	private Boolean vetFlag;
	
	private MedOrderItemStatus status;
	
	private Integer itemNum;
	
	private Integer numOfLabel;

	public void setMedOrderItem(MedOrderItem medOrderItem) {
		medOrderItem.setShowWarnCodeFlag(showWarnCodeFlag);
		medOrderItem.setShowExemptCodeFlag(showExemptCodeFlag);
		medOrderItem.setVetFlag(vetFlag);
		medOrderItem.setStatus(status);
		medOrderItem.setItemNum(itemNum);
		medOrderItem.setNumOfLabel(numOfLabel);
	}
	
	public Boolean getShowWarnCodeFlag() {
		return showWarnCodeFlag;
	}

	public void setShowWarnCodeFlag(Boolean showWarnCodeFlag) {
		this.showWarnCodeFlag = showWarnCodeFlag;
	}

	public Boolean getShowExemptCodeFlag() {
		return showExemptCodeFlag;
	}

	public void setShowExemptCodeFlag(Boolean showExemptCodeFlag) {
		this.showExemptCodeFlag = showExemptCodeFlag;
	}

	public Boolean getVetFlag() {
		return vetFlag;
	}

	public void setVetFlag(Boolean vetFlag) {
		this.vetFlag = vetFlag;
	}

	public MedOrderItemStatus getStatus() {
		return status;
	}

	public void setStatus(MedOrderItemStatus status) {
		this.status = status;
	}

	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public Integer getNumOfLabel() {
		return numOfLabel;
	}

	public void setNumOfLabel(Integer numOfLabel) {
		this.numOfLabel = numOfLabel;
	}
}
