package hk.org.ha.model.pms.biz.report;

import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.ApplicationProp;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.exception.sftp.SftpCommandException;
import hk.org.ha.model.pms.udt.report.FmReportType;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import net.lingala.zip4j.exception.ZipException;
import net.sf.jasperreports.engine.JRDataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.contexts.Context;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;
import org.joda.time.DateTime;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.ChannelSftp.LsEntry;

@Stateful
@Scope(ScopeType.SESSION)
@Name("haDrugFormularyMgmtRptDnldService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class HaDrugFormularyMgmtRptDnldServiceBean implements HaDrugFormularyMgmtRptDnldServiceLocal {
	
	@Logger
	private Log logger;

	@In
	private AuditLogger auditLogger;
	
	@In
	private ReportProvider<JRDataSource> reportProvider;
	
	@In
	private PrintAgentInf printAgent;
	
	@In
	private ApplicationProp applicationProp;
	
	private String filePath;
	private String fileName;
	private String password;
	
	private static final String DNLD_XLS_FILE_EXT = ".xls";
	private static final String SLASH = "/";
	private static final String SPLIT = "\\|";
	private static final String EXCEPTION_CLASS_NAME = "HaDrugFormularyMgmtRpt";
	
	private ChannelSftp fmRptChannelSftp;
	private List<File> fileArrayList;
	private File tempDir;
		
	@SuppressWarnings("unchecked")
	public void zipFmRpt(String fileName, String password) throws IOException, SftpCommandException {
		try {
			Context context = Contexts.getEventContext();
			context.set("sftp_pms_host", applicationProp.getSftpPmsHost());
			context.set("sftp_pms_username", applicationProp.getSftpPmsUsername());
			context.set("sftp_pms_password", applicationProp.getSftpPmsPassword());
			context.set("sftp_pms_rootDir", applicationProp.getSftpPmsRootDir());
			fmRptChannelSftp = (ChannelSftp) Component.getInstance("fmRptChannelSftp");
		} 
		catch (Exception e) 
		{
			logger.error("Login failed to file server: #0", applicationProp.getSftpPmsHost());
			throw new SftpCommandException(e, EXCEPTION_CLASS_NAME);
		}
		
		logger.info("Start download fm report: #0", new DateTime());
		String[] temp = fileName.split(SPLIT);
		filePath = getTrxFileDir(FmReportType.dataValueOf(temp[0]), temp[1], temp[2] + SLASH + temp[3] + DNLD_XLS_FILE_EXT);
		
		this.password = password;
		this.fileName = temp[3];
		
		if (!StringUtils.isEmpty(filePath)) {
	        tempDir = new File(System.getProperty("java.io.tmpdir") + System.currentTimeMillis());
	        if (!tempDir.mkdirs()) {
	        	throw new IOException("Cannot create directory : " + tempDir);
	        }
				
			fileArrayList = new ArrayList<File>();	
			String tempFileString;
			String fullFileName = null;
			if (!SLASH.equals(StringUtils.substring(filePath, -1))) {
				tempFileString = tempDir + SLASH + this.fileName + DNLD_XLS_FILE_EXT;
				fullFileName = this.fileName + DNLD_XLS_FILE_EXT;
				try {
					fmRptChannelSftp.get(filePath, tempFileString);
					fileArrayList.add(new File(tempFileString));		
				} catch (SftpException e) {
					logger.error("Sftp command error");
					throw new SftpCommandException(e, EXCEPTION_CLASS_NAME);
				}
			} else {
				try {
					fmRptChannelSftp.cd(filePath);
					List fileVector = fmRptChannelSftp.ls(".");

					if (fileVector.size() != 0) {
						for (int i = 0; i < fileVector.size(); i++) {
							LsEntry lsEntry = (LsEntry) fileVector.get(i);

							if (!lsEntry.getFilename().equals(".") && !lsEntry.getFilename().equals("..")) {
								tempFileString = tempDir + SLASH + lsEntry.getFilename();
								fullFileName = lsEntry.getFilename();
								fmRptChannelSftp.get(lsEntry.getFilename(), tempFileString);
								fileArrayList.add(new File(tempFileString));					
							}
						}
					}		
				} catch (SftpException e) {
					logger.error("Sftp command error");
					throw new SftpCommandException(e, EXCEPTION_CLASS_NAME);
				}
			}
			
			if (fullFileName != null) {
				auditLogger.log("#0711", temp[2], fullFileName);
			}
		}
	}
	
	private String getTrxFileDir(FmReportType fmReportType, String year, String folder) {
		switch(fmReportType) {
			case MonthlyData:
			case MonthlyClusterSpecStat:
				return applicationProp.getFmRptDataSubDir() + SLASH + year + SLASH + folder;
				
			case MonthlyCorpSpecStat:
				return applicationProp.getFmRptDataSubDir() + SLASH + year + StringUtils.trim(folder);
				
			default:
				return "";
		}
	}
	
	public void dnldFmRpt() throws ZipException, IOException {
		if (fileArrayList.size() > 0) {
			File tempFile = printAgent.generateZipFile(fileArrayList, password);
			String contentId = reportProvider.storeReport(tempFile, ReportProvider.ZIP);
			FileUtils.deleteQuietly(tempFile);	
			reportProvider.redirectReport(contentId, fileName);
		}
			
		if (tempDir.exists()) {
			FileUtils.deleteDirectory(tempDir);
		}
		
		logger.info("End download fm report: #0", new DateTime());
	}
	
	@Remove
	public void destroy() {
		
	}
}
