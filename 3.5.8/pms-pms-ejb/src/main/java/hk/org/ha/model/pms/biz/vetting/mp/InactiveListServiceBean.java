package hk.org.ha.model.pms.biz.vetting.mp;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileMoItemStatus;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.ObjectUtils;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("inactiveListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class InactiveListServiceBean implements InactiveListServiceLocal {

	private static final String MED_PROFILE_MO_ITEM_QUERY_HINT_BATCH_1 = "m.medProfilePoItemList";
	private static final String MED_PROFILE_MO_ITEM_QUERY_HINT_BATCH_2 = "m.medProfileMoItemFmList";
	private static final String MED_PROFILE_MO_ITEM_QUERY_HINT_BATCH_3 = "m.medProfileMoItemAlertList";
	private static final String MED_PROFILE_MO_ITEM_QUERY_HINT_BATCH_4 = "m.medProfileItem.medProfileMoItem";
	
	private static InactiveItemComparator INACTIVE_ITEM_COMPARATOR = new InactiveItemComparator();
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private Workstore workstore;
	
	@In
	private MedProfile medProfile;
	
	@In
	private Date startVettingDate;	
	
	@Out(required = false)
	private List<MedProfileMoItem> inactiveList;
	
    @Override
    @SuppressWarnings("unchecked")
	public void retrieveInactiveList() {

    	List<MedProfileMoItem> inactiveList = em.createQuery(
    			"select m from MedProfileMoItem m" + // 20120306 index check : MedProfileItem.medProfile : FK_MED_PROFILE_ITEM_01
    			" where ((m.endDate < :startVettingDate" +
    			" and (m.medProfileItem.status <> :unvetStatus" +
    			" or m.medProfileItem.adminDate is not null))" +
    			" or m.medProfileItem.status = :discontinuedStatus" +
    			" or m.medProfileItem.status = :endedStatus" +
    			" or (m.transferFlag = true and m.medProfileItem.medProfileMoItem = m)" +
    			" or m.medProfileItem.medProfileMoItem <> m)" +
    			" and (m.medProfileItem.status <> :deletedStatus" +
    			" or m.medProfileItem.medProfileMoItem <> m)" +
    			" and m.status <> :sysDeletedStatus"+
    			" and m.medProfileItem.medProfile = :medProfile" +
    			" order by m.medProfileItem.statusUpdateDate desc, m.orderDate desc")
    			.setParameter("startVettingDate", startVettingDate)
    			.setParameter("unvetStatus", MedProfileItemStatus.Unvet)
    			.setParameter("discontinuedStatus", MedProfileItemStatus.Discontinued)
    			.setParameter("endedStatus", MedProfileItemStatus.Ended)
    			.setParameter("deletedStatus", MedProfileItemStatus.Deleted)
    			.setParameter("sysDeletedStatus", MedProfileMoItemStatus.SysDeleted)
    			.setParameter("medProfile", medProfile)
    			.setHint(QueryHints.BATCH, MED_PROFILE_MO_ITEM_QUERY_HINT_BATCH_1)
    			.setHint(QueryHints.BATCH, MED_PROFILE_MO_ITEM_QUERY_HINT_BATCH_2)
    			.setHint(QueryHints.BATCH, MED_PROFILE_MO_ITEM_QUERY_HINT_BATCH_3)
    			.setHint(QueryHints.BATCH, MED_PROFILE_MO_ITEM_QUERY_HINT_BATCH_4)
    			.getResultList();
    	
    	MedProfileUtils.lookup(inactiveList, MED_PROFILE_MO_ITEM_QUERY_HINT_BATCH_1);
    	MedProfileUtils.lookup(inactiveList, MED_PROFILE_MO_ITEM_QUERY_HINT_BATCH_2);
    	MedProfileUtils.lookup(inactiveList, MED_PROFILE_MO_ITEM_QUERY_HINT_BATCH_3);
    	MedProfileUtils.lookup(inactiveList, MED_PROFILE_MO_ITEM_QUERY_HINT_BATCH_4);
    	Collections.sort(inactiveList, INACTIVE_ITEM_COMPARATOR);
    	
    	markMealFlag(workstore, inactiveList);
    	markFirstItemFlag(inactiveList);
    	markFirstManualItem(inactiveList);
    	this.inactiveList = inactiveList;
    	
    	for (MedProfileMoItem medProfileMoItem: inactiveList) {
    		medProfileMoItem.loadDmInfo();
    		for (MedProfilePoItem medProfilePoItem: medProfileMoItem.getMedProfilePoItemList()) {
    			medProfilePoItem.loadDmInfo();
    		}
    	}
    }
    
    private void markMealFlag(Workstore workstore, List<MedProfileMoItem> medProfileMoItemList) {
    	for (MedProfileMoItem medProfileMoItem: medProfileMoItemList) {
        	MedProfileUtils.markMealFlag(workstore, medProfileMoItem);
        }
    }
    
    private void markFirstItemFlag(List<MedProfileMoItem> medProfileMoItemList) {
    	
    	Map<Integer, Integer> firstItemNumMap = new HashMap<Integer, Integer>();
    	for (MedProfileMoItem medProfileMoItem: medProfileMoItemList) {
    		Integer firstItemNum = firstItemNumMap.get(medProfileMoItem.getOrgItemNum());
    		if (firstItemNum == null || MedProfileUtils.compare(firstItemNum, medProfileMoItem.getItemNum()) > 0 ) {
    			firstItemNumMap.put(medProfileMoItem.getOrgItemNum(), medProfileMoItem.getItemNum());
    		}
    	}
    	
    	for (MedProfileMoItem medProfileMoItem: medProfileMoItemList) {
    		medProfileMoItem.setFirstItemFlag(MedProfileUtils.compare(firstItemNumMap.get(medProfileMoItem.getOrgItemNum()), 
    				medProfileMoItem.getItemNum()) == 0);
    	}
    }

    private void markFirstManualItem(List<MedProfileMoItem> medProfileMoItemList) {
    	for (MedProfileMoItem medProfileMoItem: medProfileMoItemList) {
    		if (medProfileMoItem.isManualItem()) {
    			medProfileMoItem.setFirstManualItem(true);
        		break;
    		}
    	}
    }
    
    public static class InactiveItemComparator implements Comparator<MedProfileMoItem> {

		@Override
		public int compare(MedProfileMoItem medProfileMoItem1, MedProfileMoItem medProfileMoItem2) {
			
			int result = ObjectUtils.compare(medProfileMoItem1.isManualItem(), medProfileMoItem2.isManualItem());
			if (result != 0) {
				return result;
			}
			
			result = ObjectUtils.compare(extractDateForComparison(medProfileMoItem1), extractDateForComparison(medProfileMoItem2));
			if (result != 0) {
				return -1 * result;
			}
			
			return -1 * ObjectUtils.compare(medProfileMoItem1.getOrderDate(), medProfileMoItem2.getOrderDate());
		}
		
		private Date extractDateForComparison(MedProfileMoItem medProfileMoItem) {
			
			MedProfileItem medProfileItem = medProfileMoItem.getMedProfileItem();
			Date resultDate;
			
			if (!medProfileMoItem.isManualItem() && Boolean.TRUE.equals(medProfileMoItem.getTransferFlag()) && medProfileItem != null &&
					medProfileMoItem == medProfileItem.getMedProfileMoItem() && MedProfileItemStatus.Unvet_Vetted_Verified.contains(medProfileItem.getStatus())) {
				resultDate = medProfileItem.getStatusUpdateDate();
			} else {
				resultDate = medProfileMoItem.getEndDate();
			}
			
			return resultDate == null ? medProfileMoItem.getOrderDate() : resultDate;
		}
    }
    
    @Remove
	@Override
	public void destroy() {
    	if (inactiveList != null) {
    		inactiveList = null;
    	}
    }
}
