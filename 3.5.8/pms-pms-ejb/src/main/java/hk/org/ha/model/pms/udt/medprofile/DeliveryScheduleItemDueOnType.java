package hk.org.ha.model.pms.udt.medprofile;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DeliveryScheduleItemDueOnType implements StringValuedEnum {
	
	Today("N", "Today"),
	Tomorrow("T", "Tomorrow"),
	DayAfterTomorrow("A", "The Day after Tomorrow");
	
    private final String dataValue;
    private final String displayValue;
	
	private DeliveryScheduleItemDueOnType(String dataValue, String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}
	
	public static DeliveryScheduleItemDueOnType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DeliveryScheduleItemDueOnType.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<DeliveryScheduleItemDueOnType> {
		
		private static final long serialVersionUID = 1L;

		public Class<DeliveryScheduleItemDueOnType> getEnumClass() {
			return DeliveryScheduleItemDueOnType.class;
		}
	}
}
