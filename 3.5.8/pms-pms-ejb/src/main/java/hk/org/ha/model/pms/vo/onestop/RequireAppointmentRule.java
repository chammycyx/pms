package hk.org.ha.model.pms.vo.onestop;

import java.io.Serializable;

public class RequireAppointmentRule implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String patHospCode;
	
	private String pasSpecCode;
	
	private Boolean noAppointmentRequired;

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public String getPasSpecCode() {
		return pasSpecCode;
	}

	public void setPasSpecCode(String pasSpecCode) {
		this.pasSpecCode = pasSpecCode;
	}

	public Boolean getNoAppointmentRequired() {
		return noAppointmentRequired;
	}

	public void setNoAppointmentRequired(Boolean noAppointmentRequired) {
		this.noAppointmentRequired = noAppointmentRequired;
	}
}
