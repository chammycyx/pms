package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.vo.report.EdsStatusDetailRpt;
import hk.org.ha.model.pms.vo.report.SuspendPrescRpt;

import java.util.List;

import javax.ejb.Local;

@Local
public interface EdsStatusDetailRptServiceLocal {

	void retrieveEdsStatusDetailRptList(Integer dayRange);
	
	List<SuspendPrescRpt> retrieveSuspendPrescRptList(List<EdsStatusDetailRpt> printEdsStatusDetailRptList);
	
	void printSuspendPrescRpt(List<EdsStatusDetailRpt> printEdsStatusDetailRptList);
	
	void exportFilteredEdsStatusDetailRpt(List<EdsStatusDetailRpt> printEdsStatusDetailRptList);
	
	void exportEdsStatusDetailRpt();
	
	void destroy();
	
}
