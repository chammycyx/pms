package hk.org.ha.model.pms.udt.cddh;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum ItemDescFilterMode implements StringValuedEnum {
	BeginWith("B","Begins With"),
	Contain("C","Contains");
	
    private final String dataValue;
    private final String displayValue;

    ItemDescFilterMode(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static ItemDescFilterMode dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(ItemDescFilterMode.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<ItemDescFilterMode> {
		
		private static final long serialVersionUID = 1L;

		@Override
    	public Class<ItemDescFilterMode> getEnumClass() {
    		return ItemDescFilterMode.class;
    	}
    }
}



