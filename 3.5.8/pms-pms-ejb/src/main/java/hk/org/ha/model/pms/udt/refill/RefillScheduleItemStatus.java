package hk.org.ha.model.pms.udt.refill;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum RefillScheduleItemStatus implements StringValuedEnum {

	Normal("N", "Normal"),
	Discontinue("D", "Discontinue"); 
	
    private final String dataValue;
    private final String displayValue;

    RefillScheduleItemStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static RefillScheduleItemStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(RefillScheduleItemStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<RefillScheduleItemStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<RefillScheduleItemStatus> getEnumClass() {
    		return RefillScheduleItemStatus.class;
    	}
    }
}
