package hk.org.ha.model.pms.biz.mr;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.vo.mr.MedProfileMrItem;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;
import org.jboss.seam.security.Identity;

@Stateful
@Scope(ScopeType.SESSION)
@Name("currentMedicationService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CurrentMedicationServiceBean implements CurrentMedicationServiceLocal {

	@Logger
	private Log logger;	

	@In
	private MrManagerLocal mrManager;
	
	@In
	private Identity identity;
	
	@In
	private MedProfile medProfile;
	
	@Out(required = false)
	public List<MedProfileMrItem> currentMedicationList;
	
	@Out(required = false)
	public Boolean errorOnRetrieveCurrentMedicationList;
	
	@Out(required = false)
	public Boolean errorOnAddMrAnnotationToCurrentMedication;

	@Override
	public void retrieveCurrentMedicationList() {
		
		currentMedicationList = null;
		errorOnRetrieveCurrentMedicationList = Boolean.FALSE;
		
		try {
			currentMedicationList = mrManager.retrieveCurrentMedicationList(medProfile);
		} catch (Exception ex) {
			logger.info(ex, ex);
			errorOnRetrieveCurrentMedicationList = Boolean.TRUE;
		}
	}
	
	@Override
	public boolean addMrAnnotationToCurrentMedication(String patHospCode, String orderNum,
			Integer itemNum, String mrAnnotation) {
		
		errorOnAddMrAnnotationToCurrentMedication = Boolean.FALSE;
		boolean success = false;
		
		try {
			success = mrManager.addMrAnnotation(identity, patHospCode, orderNum, itemNum, mrAnnotation);
			currentMedicationList = mrManager.retrieveCurrentMedicationList(medProfile);
		} catch (Exception ex) {
			logger.info(ex, ex);
			errorOnAddMrAnnotationToCurrentMedication = Boolean.TRUE;
		}
		return success;
	}
	
	@Remove
	@Override
	public void destroy() {
		currentMedicationList = null;
	}
}
