@XmlJavaTypeAdapters({
	@XmlJavaTypeAdapter(value=CertaintyAdapter.class, type=Certainty.class),
	@XmlJavaTypeAdapter(value=SeverityAdapter.class, type=Severity.class),
	@XmlJavaTypeAdapter(value=DiseaseTypeAdapter.class, type=DiseaseType.class),
	@XmlJavaTypeAdapter(value=AlertProfileStatusAdapter.class, type=AlertProfileStatus.class),
	@XmlJavaTypeAdapter(value=AllergenTypeAdapter.class, type=AllergenType.class),
	@XmlJavaTypeAdapter(value=DrugTypeAdapter.class, type=DrugType.class)
})
package hk.org.ha.model.pms.vo.alert.mds;

import hk.org.ha.model.pms.udt.alert.AlertProfileStatus;
import hk.org.ha.model.pms.udt.alert.AllergenType;
import hk.org.ha.model.pms.udt.alert.Certainty;
import hk.org.ha.model.pms.udt.alert.DiseaseType;
import hk.org.ha.model.pms.udt.alert.DrugType;
import hk.org.ha.model.pms.udt.alert.Severity;
import hk.org.ha.model.pms.vo.alert.AlertProfileStatusAdapter;
import hk.org.ha.model.pms.vo.alert.AllergenTypeAdapter;
import hk.org.ha.model.pms.vo.alert.CertaintyAdapter;
import hk.org.ha.model.pms.vo.alert.DiseaseTypeAdapter;
import hk.org.ha.model.pms.vo.alert.DrugTypeAdapter;
import hk.org.ha.model.pms.vo.alert.SeverityAdapter;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;

