package hk.org.ha.model.pms.biz.reftable;

import java.util.List;

import javax.ejb.Local;

@Local
public interface PatientCatListManagerLocal {

	List<String> retrievePatientCatList();
	
	List<String> retrievePatientCatListWithSuspend();
}
