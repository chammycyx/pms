package hk.org.ha.model.pms.biz.refill;

import hk.org.ha.model.pms.exception.refill.SaveRefillException;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;

import javax.ejb.Local;

@Local
public interface RefillScheduleServiceLocal {
	
	void refillStdRefillSchedule(RefillSchedule refillScheduleIn, OrderViewInfo orderViewInfoIn) throws SaveRefillException;
	
	void forceCompleteStdRefillSchedule(RefillSchedule refillScheduleIn, OrderViewInfo orderViewInfoIn) throws SaveRefillException;
	
	void cancelDispenseStdRefillSchedule(RefillSchedule refillScheduleIn, OrderViewInfo orderViewInfoIn);
	
	void abortStdRefillScheduleList(RefillSchedule refillScheduleIn, OrderViewInfo orderViewInfoIn);
	
	void suspendRefillSchedule(RefillSchedule refillScheduleIn, OrderViewInfo orderViewInfoIn);
	
	void reverseSuspendRefillSchedule(RefillSchedule refillScheduleIn, OrderViewInfo orderViewInfoIn);
	
	void terminateStdRefillScheduleList(RefillSchedule refillScheduleIn, OrderViewInfo orderViewInfoIn) throws SaveRefillException;
	
	void cancelRefill(OrderViewInfo orderViewInfoIn);
	
	String modifyRefillSuccess(OrderViewInfo orderViewInfoIn);
	
	void destroy();
}
