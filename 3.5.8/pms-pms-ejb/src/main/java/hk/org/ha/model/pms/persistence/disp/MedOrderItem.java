package hk.org.ha.model.pms.persistence.disp;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.util.ObjectArray;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.MsFmStatus;
import hk.org.ha.model.pms.persistence.AlertEntity;
import hk.org.ha.model.pms.persistence.RemarkEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.YesNoBlankFlag;
import hk.org.ha.model.pms.udt.disp.DiscontinueStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.RemarkItemStatus;
import hk.org.ha.model.pms.udt.disp.RemarkStatus;
import hk.org.ha.model.pms.udt.rx.RxItemType;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.FixedConcnSource;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.udt.vetting.NameType;
import hk.org.ha.model.pms.util.StopWatcher;
import hk.org.ha.model.pms.vo.alert.mds.AlertDdim;
import hk.org.ha.model.pms.vo.drug.DmDrugLite;
import hk.org.ha.model.pms.vo.drug.DmFormLite;
import hk.org.ha.model.pms.vo.drug.DmRouteLite;
import hk.org.ha.model.pms.vo.rx.CapdRxDrug;
import hk.org.ha.model.pms.vo.rx.CapdRxDrugRemark;
import hk.org.ha.model.pms.vo.rx.DhRxDrug;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.IvAdditive;
import hk.org.ha.model.pms.vo.rx.IvFluid;
import hk.org.ha.model.pms.vo.rx.IvRxDrug;
import hk.org.ha.model.pms.vo.rx.Regimen;
import hk.org.ha.model.pms.vo.rx.RegimenRemark;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.eclipse.persistence.annotations.BatchFetch;
import org.eclipse.persistence.annotations.BatchFetchType;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.PrivateOwned;
import org.granite.messaging.amf.io.util.externalizer.annotation.IgnoredProperty;
import org.jboss.seam.contexts.Contexts;

@Entity
@Table(name = "MED_ORDER_ITEM")
@XmlAccessorType(XmlAccessType.FIELD)
public class MedOrderItem extends RemarkEntity implements MedItemInf {

	private static final long serialVersionUID = 1L;

	protected static final char SPACE = ' ';
	
	private static final String JAXB_CONTEXT_RX = "hk.org.ha.model.pms.vo.rx";

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "medOrderItemSeq")
	@SequenceGenerator(name = "medOrderItemSeq", sequenceName = "SQ_MED_ORDER_ITEM", initialValue = 100000000)
	@XmlElement
	private Long id;
					
	@Column(name = "ITEM_NUM", nullable = false)
	@XmlElement
	private Integer itemNum;
	
	@Column(name = "ORG_ITEM_NUM", nullable = false)
	@XmlElement
	private Integer orgItemNum;
		
	@Column(name = "PRINT_SEQ")
	@XmlTransient
	private Integer printSeq;
	
	@Lob
	@Column(name = "RX_ITEM_XML")
	@XmlElement
	private String rxItemXml;

    @Converter(name = "MedOrderItem.rxItemType", converterClass = RxItemType.Converter.class)
    @Convert("MedOrderItem.rxItemType")
	@Column(name = "RX_ITEM_TYPE", length = 1, nullable = false)
	@XmlTransient
	private RxItemType rxItemType;	
	
	@Converter(name = "MedOrderItem.status", converterClass = MedOrderItemStatus.Converter.class)
	@Convert("MedOrderItem.status")
	@Column(name="STATUS", length = 1, nullable = false)
	@XmlTransient
	private MedOrderItemStatus status;

	@Column(name = "PREV_MO_ITEM_ID")
	@XmlTransient
	private Long prevMoItemId;
	
	@Column(name = "NUM_OF_LABEL", length = 2)
	@XmlTransient
	private Integer numOfLabel;

	@Column(name = "COMMENT_TEXT", length=100)
	@XmlTransient
	private String commentText;

	@Column(name = "COMMENT_USER", length=12)
	@XmlTransient
	private String commentUser;

	@Column(name = "COMMENT_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	@XmlTransient
	private Date commentDate;
	
	@Column(name = "UNIT_OF_CHARGE", nullable = false)
	@XmlTransient
	private Integer unitOfCharge;

	@Column(name = "CMS_CONFIRM_UNIT_OF_CHARGE", nullable = false)
	@XmlTransient
	private Integer cmsConfirmUnitOfCharge;
	
	@Column(name = "PPMI_FLAG", nullable = false)
	@XmlTransient
	private Boolean ppmiFlag;

	@Converter(name = "MedOrderItem.chargeFlag", converterClass = YesNoBlankFlag.Converter.class)
	@Convert("MedOrderItem.chargeFlag")
	@Column(name = "CHARGE_FLAG", nullable = false, length = 1)
	@XmlElement
	private YesNoBlankFlag chargeFlag;
	
	@Converter(name = "MedOrderItem.cmsChargeFlag", converterClass = YesNoBlankFlag.Converter.class)
	@Convert("MedOrderItem.cmsChargeFlag")
	@Column(name = "CMS_CHARGE_FLAG", nullable = false, length = 1)
	@XmlTransient
	private YesNoBlankFlag cmsChargeFlag;

	@Converter(name = "MedOrderItem.cmsConfirmChargeFlag", converterClass = YesNoBlankFlag.Converter.class)
	@Convert("MedOrderItem.cmsConfirmChargeFlag")
	@Column(name = "CMS_CONFIRM_CHARGE_FLAG", nullable = false, length = 1)
	@XmlTransient
	private YesNoBlankFlag cmsConfirmChargeFlag;
	
	@Converter(name = "MedOrderItem.remarkItemStatus", converterClass = RemarkItemStatus.Converter.class)
	@Convert("MedOrderItem.remarkItemStatus")
	@Column(name = "REMARK_ITEM_STATUS", length = 2)
	@XmlElement
	private RemarkItemStatus remarkItemStatus;

	@Column(name = "CMS_FM_STATUS", length=1)
	@XmlTransient
	private String cmsFmStatus;

	@Column(name = "CONFIRM_PREV_FM_IND_FLAG")
	@XmlTransient
	private Boolean confirmPrevFmIndFlag;
	
	@Column(name = "MODIFY_FLAG")
	@XmlTransient
	private Boolean modifyFlag;

	@Column(name = "PRE_VET_NOTE_TEXT", length = 255)
	@XmlTransient
	private String preVetNoteText;
	
	@Column(name = "PRE_VET_NOTE_USER", length = 12)
	@XmlTransient
	private String preVetNoteUser;
	
	@Column(name = "PRE_VET_NOTE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	@XmlTransient
	private Date preVetNoteDate;
	
	@Lob
	@Column(name = "CMS_EXTRA_XML")
	@XmlTransient
	private String cmsExtraXml;
	
	@Converter(name = "MedOrderItem.discontinueStatus", converterClass = DiscontinueStatus.Converter.class)
	@Convert("MedOrderItem.discontinueStatus")
	@Column(name="DISCONTINUE_STATUS", length = 1)
	@XmlTransient
	private DiscontinueStatus discontinueStatus;
    
    @Column(name = "ALERT_DESC", length = 500)
    @XmlTransient
    private String alertDesc;
    
    @Column(name = "ALERT_REMARK", length = 500)
    @XmlTransient
    private String alertRemark;
    
	@Lob
	@Column(name = "DH_RX_ITEM_XML")
	@XmlElement
	private String dhRxItemXml;

	
	@ManyToOne
	@JoinColumn(name = "MED_ORDER_ID", nullable = false)
	@XmlTransient
	private MedOrder medOrder;

	@PrivateOwned
	@BatchFetch(BatchFetchType.JOIN)
	@OrderBy("sortSeq")
	@OneToMany(mappedBy="medOrderItem", cascade=CascadeType.ALL)
	@XmlTransient
	private List<MedOrderItemAlert> medOrderItemAlertList;

	@Transient
	@XmlTransient
	private List<PharmOrderItem> pharmOrderItemList; 

    @Transient
	@XmlTransient
    private String action;
    
	@Transient
	@XmlTransient
	private Boolean showWarnCodeFlag;
	
	@Transient
	@XmlTransient
	private Boolean showExemptCodeFlag;
	
	@Transient
	@XmlTransient
	private Boolean vetFlag;

	@Transient
	@XmlTransient
	private Boolean changeFlag;
	
	@Transient
	@XmlTransient
	private Boolean groupEndFlag;
	
	@Transient
	@XmlElement
	private Boolean fmIndFlag;	

	@Transient
	@XmlTransient
	private Boolean drugNameRemark;
	
	@Transient
	@XmlTransient
	private Boolean singleUseFlagRemark;
	
	@Transient
	@XmlTransient
	private Boolean fixPeriodFlagRemark;
	
	@Transient
	@XmlTransient
	private Boolean specialInstructionRemark;
	
	@Transient
	@XmlTransient
	private Boolean commentRemark;
	
	@Transient
	@XmlTransient
	private Boolean actionStatusRemark;
	
	@Transient
	@XmlTransient
	private Integer seqNum;
	
	@Transient
	@XmlTransient
	private List<DispOrderItem> duplicateItemList;
	
	@SuppressWarnings("unused") // in order to import RxItem for MedItemInf
	@Transient
	@XmlTransient
	private RxItem dummyRxItem;
	
	@Transient
	@XmlTransient
	private RxDrug rxDrug;
	
	@Transient
	@XmlTransient
	private CapdRxDrug capdRxDrug;

	@Transient
	@XmlTransient
	private IvRxDrug ivRxDrug;
	
	@Transient
	@XmlTransient
	private DhRxDrug dhRxDrug;
	
	@Transient
	@XmlTransient
	private Integer numOfSystemDeducedItem;
	
	@Transient
	@XmlTransient
	private Boolean uniqueDrugFlag;
	
	@Transient
	@XmlTransient
	private Boolean mpDischargeFlag;
	
	@Transient
	@XmlTransient
	private List<Long> relatedMedOrderItemIdList;
	
	@Transient
	@XmlTransient
	private String discontinueReason;
	
	@Transient
	@XmlTransient
	private String discontinueHospCode;
	
	@Transient
	@XmlTransient
	private Date discontinueDate;
	
	@Transient
	@XmlTransient
	private Boolean manualProfileFlag;
	
	@Transient
	@XmlTransient
	private String patHospCode;
	
	@Transient
	@XmlTransient
	@IgnoredProperty
	private Boolean isMultipleChemoItem;
	
	public MedOrderItem() {
    	status = MedOrderItemStatus.Outstanding;
		showWarnCodeFlag = Boolean.FALSE;
		vetFlag = Boolean.FALSE;
		changeFlag = Boolean.FALSE;
		groupEndFlag = Boolean.FALSE;
		fmIndFlag = Boolean.FALSE;
		
		drugNameRemark = Boolean.FALSE;
		singleUseFlagRemark = Boolean.FALSE;
		fixPeriodFlagRemark = Boolean.FALSE;
		specialInstructionRemark = Boolean.FALSE;
		commentRemark = Boolean.FALSE;
		actionStatusRemark = Boolean.FALSE;
		remarkItemStatus = RemarkItemStatus.NoRemark;
		
		chargeFlag = YesNoBlankFlag.Blank;
		cmsChargeFlag = YesNoBlankFlag.Blank;
		cmsConfirmChargeFlag = YesNoBlankFlag.Blank;
		
		ppmiFlag = Boolean.FALSE;
		
		discontinueStatus = DiscontinueStatus.None;
		
		uniqueDrugFlag = Boolean.TRUE;
		mpDischargeFlag = Boolean.FALSE;
		
		numOfLabel = Integer.valueOf(1);
		unitOfCharge = Integer.valueOf(0);
		cmsConfirmUnitOfCharge = Integer.valueOf(0);
		
		manualProfileFlag = Boolean.FALSE;
		isMultipleChemoItem = Boolean.FALSE;
	}
	
	public MedOrderItem(RxItem rxItem) {
		this();
		this.initRxItem(rxItem);
	}
	
    @PostLoad
    public void postLoad() {
    	if (StopWatcher.isDebugEnabled()) {
    		StopWatcher.resume();
    	}
    
		if (!this.hasInitRxItem() && rxItemXml != null) {
			JaxbWrapper<RxItem> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
    		this.initRxItem(rxJaxbWrapper.unmarshall(rxItemXml));
    	}
    	
		if (status != MedOrderItemStatus.Deleted) {
	    	if (status == MedOrderItemStatus.Completed || (FmStatus.FreeTextEntryItem.getDataValue().equals(this.getFmStatus()))) {
	    		vetFlag = Boolean.TRUE;
	    	}
		}

		if (dhRxDrug == null && dhRxItemXml != null) {
			JaxbWrapper<RxItem> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
			dhRxDrug = (DhRxDrug) rxJaxbWrapper.unmarshall(dhRxItemXml);
		}
		
    	if (StopWatcher.isDebugEnabled()) {
    		StopWatcher.suspend();
    	}
    }
    
	@PrePersist
    @PreUpdate
	public void preSave() {

		JaxbWrapper<RxItem> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
		RxItem rxItem = getRxItem();
		if (rxItem != null) {
			rxItemXml = rxJaxbWrapper.marshall(rxItem);
		}
		if (dhRxDrug != null) {
			dhRxItemXml = rxJaxbWrapper.marshall(dhRxDrug);
		}
	}
	
	public void loadDmInfo() {
		loadDmInfo(false);//default load OP site list
	}
	
    public void loadDmInfo(boolean isMedDischarge)  {    
    	loadDmInfo(isMedDischarge, true);
    }
    
    public void loadDmInfo(boolean isMedDischarge, boolean loadRegimen)  {    		    		
    	
		Workstore workstore = null;
		if (this.getMedOrder() != null) {
			workstore = this.getMedOrder().getWorkstore();
		}
		if (workstore == null) {
			workstore = (Workstore) Contexts.getSessionContext().get("workstore");
		}
		
    	if (rxDrug != null) {
    		rxDrug.loadDmInfo(workstore, isMedDischarge, loadRegimen, true);
    	} else if (ivRxDrug != null) {
    		if (ivRxDrug.getIvAdditiveList() != null) {
	    		for (IvAdditive ivAdditive:ivRxDrug.getIvAdditiveList()) {
	    			ivAdditive.loadDmInfo(workstore, isMedDischarge, loadRegimen, false);
	    		}
    		}
    	} else if (capdRxDrug != null) {
    		capdRxDrug.initDmCapd();
    	}
    	
    }

    public void clearDmInfo() {
    	if (rxDrug != null) {
    		rxDrug.clearDmInfo();
    	} else if (ivRxDrug != null) {
    		if (ivRxDrug.getIvAdditiveList() != null) {
	    		for (IvAdditive ivAdditive:ivRxDrug.getIvAdditiveList()) {
	    			ivAdditive.clearDmInfo();
	    		}
    		}
    	}
    }
    
    public void postLoadAll() {
		postLoad();
		loadDmInfo();
	}
    
	private boolean hasInitRxItem() {
		return getRxItem() != null;
	}
	
	private void initRxItem(RxItem rxItem) {
		
    	if (rxItem.isRxDrug()) {
    		rxDrug = (RxDrug) rxItem;
    		if (rxItem.isOralRxDrug()) {
    			rxItemType = RxItemType.Oral;
    		} else if (rxItem.isInjectionRxDrug()) {
        		rxItemType = RxItemType.Injection;
    		}
    	} else if (rxItem.isCapdRxDrug()) {
    		capdRxDrug = (CapdRxDrug) rxItem;
    		rxItemType = RxItemType.Capd;
    	} else if (rxItem.isIvRxDrug()){
    		ivRxDrug = (IvRxDrug) rxItem;
    		rxItemType = RxItemType.Iv;
    	}
    	rxItem.buildTlf();
	}    
    
	public RxItem getRxItem() {
		if (rxDrug != null) {
			return rxDrug;
		}
		else if (capdRxDrug != null) {
			return capdRxDrug;
		}
		else if (ivRxDrug != null) {
			return ivRxDrug;
		}
		else {
			return null;
		}
	}
	
	public void setRxItem(RxItem rxItem) {
		if (rxItem == null) {
			return;
		}
		else if (rxItem.isRxDrug()) {
			this.rxDrug = (RxDrug) rxItem;
		}
		else if (rxItem.isCapdRxDrug()) {
			this.capdRxDrug = (CapdRxDrug) rxItem;
		}
		else if (rxItem.isIvRxDrug()) {
			this.ivRxDrug = (IvRxDrug) rxItem;
		}
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRxItemXml() {
		return rxItemXml;
	}

	public void setRxItemXml(String rxItemXml) {
		this.rxItemXml = rxItemXml;
	}

	public RxItemType getRxItemType() {
		return rxItemType;
	}

	public void setRxItemType(RxItemType rxItemType) {
		this.rxItemType = rxItemType;
	}

	public String getItemCode() {
		if (rxDrug != null) {
			return rxDrug.getItemCode();
		} else {
			return null;
		}
	}

	public void setItemCode(String itemCode) {
		if (rxDrug != null) {
			rxDrug.setItemCode(itemCode);
		} 
	}

	public String getDisplayName() {
		if (rxDrug != null) {
			return rxDrug.getDisplayName();
		} else {
			return null;
		}
	}

	public void setDisplayName(String displayName) {
		if (rxDrug != null) {
			rxDrug.setDisplayName(displayName);
		} 
	}

	public String getFormCode() {
		if (rxDrug != null) {
			return rxDrug.getFormCode();
		} else {
			return null;
		}
	}

	public void setFormCode(String formCode) {
		if (rxDrug != null) {
			rxDrug.setFormCode(formCode);
		}
	}

	public String getSaltProperty() {
		if (rxDrug != null) {
			return rxDrug.getSaltProperty();
		} else {
			return null;
		}
	}

	public void setSaltProperty(String saltProperty) {
		if (rxDrug != null) {
			rxDrug.setSaltProperty(saltProperty);
		}
	}

	public String getFirstDisplayName() {
		if (rxDrug != null) {
			return rxDrug.getFirstDisplayName();
		} else if (capdRxDrug != null) {
			return "CAPD";
		} else {
			return null;
		}
	}

	public void setFirstDisplayName(String firstDisplayName) {
		if (rxDrug != null) {
			rxDrug.setFirstDisplayName(firstDisplayName);	
		}
	}

	public String getSecondDisplayName() {
		if (rxDrug != null) {
			return rxDrug.getSecondDisplayName();
		} else {
			return null;
		}
	}

	public void setSecondDisplayName(String secondDisplayName) {
		if (rxDrug != null) {
			rxDrug.setSecondDisplayName(secondDisplayName);	
		}
	}

	public String getBaseUnit() {
		if (rxDrug != null) {
			return rxDrug.getBaseUnit();
		} else {
			return null;
		}
	}

	public void setBaseUnit(String baseUnit) {
		if (rxDrug != null) {
			rxDrug.setBaseUnit(baseUnit);
		}
	}

	public Integer getPrintSeq() {
		return printSeq;
	}

	public void setPrintSeq(Integer printSeq) {
		this.printSeq = printSeq;
	}

	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public Integer getOrgItemNum() {
		return orgItemNum;
	}

	public void setOrgItemNum(Integer orgItemNum) {
		this.orgItemNum = orgItemNum;
	}

	public String getFmStatus() {
		if (rxDrug != null) {
			return rxDrug.getFmStatus();
		} else if (capdRxDrug != null) {
			return FmStatus.GeneralDrug.getDataValue();
		} else {
			return null;
		}
	}

	public void setFmStatus(String fmStatus) {
		if (rxDrug != null) {
			rxDrug.setFmStatus(fmStatus);
		}
	}

	public Boolean getDangerDrugFlag() {
		if (rxDrug != null) {
			return rxDrug.getDangerDrugFlag();
		} else {
			return null;
		}
	}

	public void setDangerDrugFlag(Boolean dangerDrugFlag) {
		if (rxDrug != null) {
			rxDrug.setDangerDrugFlag(dangerDrugFlag);
		}
	}

	public Boolean getSingleUseFlag() {
		if (getRxItem() != null) {
			return getRxItem().getSingleUseFlag();
		} else {
			return null;
		}
	}

	public void setSingleUseFlag(Boolean singleUseFlag) {
		if (getRxItem() != null) {
			getRxItem().setSingleUseFlag(singleUseFlag);	
		}
	}

	public Boolean getFixPeriodFlag() {
		if (getRxItem() != null) {
			return getRxItem().getFixPeriodFlag();
		} else {
			return null;
		}
	}

	public void setFixPeriodFlag(Boolean fixPeriodFlag) {
		if (getRxItem() != null) {
			getRxItem().setFixPeriodFlag(fixPeriodFlag);
		}
	}

	public Boolean getRestrictFlag() {
		if (rxDrug != null) {
			return rxDrug.getRestrictFlag();
		} else {
			return null;
		}
	}

	public void setRestrictFlag(Boolean restrictFlag) {
		if (rxDrug != null) {
			rxDrug.setRestrictFlag(restrictFlag);
		}
	}
	
	public Boolean getAlertFlag() {
		if (getRxItem() != null) {
			return getRxItem().getAlertFlag();
		}
		else {
			return null;
		}
	}

	public void setAlertFlag(Boolean alertFlag) {
		if (getRxItem() != null) {
			getRxItem().setAlertFlag(alertFlag);
		}
	}

	public Boolean getExternalUseFlag() {
		if (getRxItem() != null) {
			return getRxItem().getExternalUseFlag();
		} else {
			return null;
		}
	}

	public void setExternalUseFlag(Boolean externalUseFlag) {
		if (getRxItem() != null) {
			getRxItem().setExternalUseFlag(externalUseFlag);
		}
	}

	public void setActionStatus(ActionStatus actionStatus) {
		if (rxDrug != null) {
			rxDrug.setActionStatus(actionStatus);
		} else if (capdRxDrug != null) {
			capdRxDrug.setActionStatus(actionStatus);
		}
	}

	public ActionStatus getActionStatus() {
		if (rxDrug != null) {
			return rxDrug.getActionStatus();
		} else if (capdRxDrug != null) {
			return capdRxDrug.getActionStatus();
		} else {
			return null;
		}
	}

	public MedOrderItemStatus getStatus() {
		return status;
	}

	public void setStatus(MedOrderItemStatus status) {
		this.status = status;
	}

	public NameType getNameType() {
		if (rxDrug != null) {
			return rxDrug.getNameType();
		} else {
			return null;
		}
	}

	public void setNameType(NameType nameType) {
		if (rxDrug != null) {
			rxDrug.setNameType(nameType);
		}
	}

	public String getAliasName() {
		if (rxDrug != null) {
			return rxDrug.getAliasName();
		} else {
			return null;
		}
	}

	public void setAliasName(String aliasName) {
		if (rxDrug != null) {
			rxDrug.setAliasName(aliasName);
		}
	}

	public String getStrength() {
		if (rxDrug != null) {
			return rxDrug.getStrength();
		} else {
			return null;
		}
	}

	public void setStrength(String strength) {
		if (rxDrug != null) {
			rxDrug.setStrength(strength);
		}
	}

	public BigDecimal getVolume() {
		if (rxDrug != null) {
			return rxDrug.getVolume();
		} else {
			return null;
		}
	}

	public void setVolume(BigDecimal volume) {
		if (rxDrug != null) {
			rxDrug.setVolume(volume);
		}
	}

	public String getVolumeUnit() {
		if (rxDrug != null) {
			return rxDrug.getVolumeUnit();
		} else {
			return null;
		}
	}

	public void setVolumeUnit(String volumeUnit) {
		if (rxDrug != null) {
			rxDrug.setVolumeUnit(volumeUnit);
		}
	}

	public String getExtraInfo() {
		if (rxDrug != null) {
			return rxDrug.getExtraInfo();
		} else {
			return null;
		}
	}

	public void setExtraInfo(String extraInfo) {
		if (rxDrug != null) {
			rxDrug.setExtraInfo(extraInfo);
		}
	}

	public String getSpecialInstruction() {
		if (getRxItem() != null) {
			return getRxItem().getSpecialInstruction();
		} else {
			return null;
		}
	}

	public void setSpecialInstruction(String specialInstruction) {
		if (getRxItem() != null) {
			getRxItem().setSpecialInstruction(specialInstruction);
		}
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public String getCommentUser() {
		return commentUser;
	}

	public void setCommentUser(String commentUser) {
		this.commentUser = commentUser;
	}

	public Date getCommentDate() {
		return commentDate;
	}

	public void setCommentDate(Date commentDate) {
		this.commentDate = commentDate;
	}

	public String getRouteCode() {
		if (rxDrug != null) {
			return rxDrug.getRouteCode();
		} else {
			return null;
		}
	}

	public void setRouteCode(String routeCode) {
		if (rxDrug != null) {
			rxDrug.setRouteCode(routeCode);
		}
	}

	public String getRouteDesc() {
		if (rxDrug != null) {
			return rxDrug.getRouteDesc();
		} else {
			return null;
		}
	}

	public void setRouteDesc(String routeDesc) {
		if (rxDrug != null) {
			rxDrug.setRouteDesc(routeDesc);
		}
	}

	public String getFormDesc() {
		if (rxDrug != null) {
			return rxDrug.getFormDesc();
		} else {
			return null;
		}
	}

	public void setFormDesc(String formDesc) {
		if (rxDrug != null) {
			rxDrug.setFormDesc(formDesc);
		}
	}

	public Long getPrevMoItemId() {
		return prevMoItemId;
	}

	public void setPrevMoItemId(Long prevMoItemId) {
		this.prevMoItemId = prevMoItemId;
	}

	public Integer getNumOfLabel() {
		return numOfLabel;
	}

	public void setNumOfLabel(Integer numOfLabel) {
		this.numOfLabel = numOfLabel;
	}

	public String getTradeName() {
		if (rxDrug != null) {
			return rxDrug.getTradeName();
		} else {
			return null;
		}
	}

	public void setTradeName(String tradeName) {
		if (rxDrug != null) {
			rxDrug.setTradeName(tradeName);
		}
	}

	public MedOrder getMedOrder() {
		return medOrder;
	}
	
	public void setMedOrder(MedOrder medOrder) {
		this.medOrder = medOrder;
	}

	public List<PharmOrderItem> getPharmOrderItemList() {
		if (pharmOrderItemList == null) {
			pharmOrderItemList = new ArrayList<PharmOrderItem>();
		}
		return pharmOrderItemList;
	}

	public void setPharmOrderItemList(List<PharmOrderItem> pharmOrderItemList) {
		this.pharmOrderItemList = pharmOrderItemList;
	}
	
	public void addPharmOrderItem(PharmOrderItem pharmOrderItem) {
		pharmOrderItem.setMedOrderItem(this);
		getPharmOrderItemList().add(pharmOrderItem);
	}
	
	public void clearPharmOrderItemList() {
		pharmOrderItemList = new ArrayList<PharmOrderItem>();
	}
	
	public void removePharmOrderItemList() {
		for (PharmOrderItem pharmOrderItem : getPharmOrderItemList()) {
			pharmOrderItem.getPharmOrder().getPharmOrderItemList().remove(pharmOrderItem);
		}
		clearPharmOrderItemList();
	}
	
	public List<MedOrderItemAlert> getMedOrderItemAlertList() {
		if (medOrderItemAlertList == null) {
			medOrderItemAlertList = new ArrayList<MedOrderItemAlert>();
		}
		return medOrderItemAlertList;
	}

	public void setMedOrderItemAlertList(List<MedOrderItemAlert> medOrderItemAlertList) {
		this.medOrderItemAlertList = medOrderItemAlertList;
	}
	
	public void addMedOrderItemAlert(MedOrderItemAlert medOrderItemAlert) {
		medOrderItemAlert.setMedOrderItem(this);
		getMedOrderItemAlertList().add(medOrderItemAlert);
	}
	
	public void clearMedOrderItemAlertList() {
		medOrderItemAlertList = new ArrayList<MedOrderItemAlert>();
	}
	
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public void setDmDrugLite(DmDrugLite dmDrugLite) {
		if (rxDrug != null) {
			rxDrug.setDmDrugLite(dmDrugLite);
		}		
	}

	public DmDrugLite getDmDrugLite() {
		if (rxDrug != null) {
			return rxDrug.getDmDrugLite();
		} else {			
			return null;
		}
	}
	
	public ObjectArray getItemKey() {
		return new ObjectArray(
			this.getFirstDisplayName(),
			this.getSecondDisplayName(),
			this.getFormCode(),
			this.getSaltProperty()
		);
	}

	public void setRegimen(Regimen regimen) {
		if (rxDrug != null) {
			rxDrug.setRegimen(regimen);
		}
	}

	public Regimen getRegimen() {
		if (rxDrug != null) {
			return rxDrug.getRegimen();
		} else {
			return null;
		}
	}
	
	public RxDrug getRxDrug() {
		return rxDrug;
	}

	public void setRxDrug(RxDrug rxDrug) {
		this.rxDrug = rxDrug;
	}

	public CapdRxDrug getCapdRxDrug() {
		return capdRxDrug;
	}

	public void setCapdRxDrug(CapdRxDrug capdRxDrug) {
		this.capdRxDrug = capdRxDrug;
	}

	public IvRxDrug getIvRxDrug() {
		return ivRxDrug;
	}

	public void setIvRxDrug(IvRxDrug ivRxDrug) {
		this.ivRxDrug = ivRxDrug;
	}

	public DhRxDrug getDhRxDrug() {
		return dhRxDrug;
	}

	public void setDhRxDrug(DhRxDrug dhRxDrug) {
		this.dhRxDrug = dhRxDrug;
	}

	public MsFmStatus getMsFmStatus() {
		if (rxDrug != null) {
			return rxDrug.getMsFmStatus();
		}
		else {
			return null;
		}
	}

	public void setMsFmStatus(MsFmStatus msFmStatus) {
		if (rxDrug != null) {
			rxDrug.setMsFmStatus(msFmStatus);
		}
	}

	public DmFormLite getDmFormLite() {
		if (rxDrug != null) {
			return rxDrug.getDmFormLite();
		}
		else {
			return null;
		}
	}

	public void setDmFormLite(DmFormLite dmFormLite) {
		if (rxDrug != null) {
			rxDrug.setDmFormLite(dmFormLite);
		}
	}

	public Boolean getShowWarnCodeFlag() {
		return showWarnCodeFlag;
	}

	public void setShowWarnCodeFlag(Boolean showWarnCodeFlag) {
		this.showWarnCodeFlag = showWarnCodeFlag;
	}

	public Boolean getShowExemptCodeFlag() {
		return showExemptCodeFlag;
	}

	public void setShowExemptCodeFlag(Boolean showExemptCodeFlag) {
		this.showExemptCodeFlag = showExemptCodeFlag;
	}

	public Boolean getVetFlag() {
		return vetFlag;
	}

	public void setVetFlag(Boolean vetFlag) {
		this.vetFlag = vetFlag;
	}

	public Boolean getChangeFlag() {
		return changeFlag;
	}

	public void setChangeFlag(Boolean changeFlag) {
		this.changeFlag = changeFlag;
	}

	public Boolean getGroupEndFlag() {
		return groupEndFlag;
	}

	public void setGroupEndFlag(Boolean groupEndFlag) {
		this.groupEndFlag = groupEndFlag;
	}
	
	public Boolean getFmIndFlag() {
		return fmIndFlag;
	}

	public void setFmIndFlag(Boolean fmIndFlag) {
		this.fmIndFlag = fmIndFlag;
		if (getRxItem() != null) {
			getRxItem().setFmFlag(fmIndFlag);
		}
	}

	public Boolean getWorkStoreDrugFlag() {
		if (rxDrug != null) {
			return rxDrug.getWorkStoreDrugFlag();
		}
		else {			
			return null;
		}
	}

	public void setWorkStoreDrugFlag(Boolean workStoreDrugFlag) {
		if (rxDrug != null) {
			rxDrug.setWorkStoreDrugFlag(workStoreDrugFlag);
		}
	}

	public RemarkStatus getRemarkStatus() {
		if (getRemarkCreateDate() == null) {
			return RemarkStatus.None;
		}
		else if (getRemarkConfirmDate() == null) {
			return RemarkStatus.Unconfirm;
		}
		else {
			return RemarkStatus.Confirm;
		}
	}

	public void setDosageCompulsory(Boolean dosageCompulsory) {
		if (rxDrug != null) {
			rxDrug.setDosageCompulsory(dosageCompulsory);
		} 		
	}

	public Boolean getDosageCompulsory() {
		if (rxDrug != null) {
			return rxDrug.getDosageCompulsory();
		} 
		else {
			return null;
		}
	}

	public void setStrengthCompulsory(Boolean strengthCompulsory) {
		if (rxDrug != null) {
			rxDrug.setStrengthCompulsory(strengthCompulsory);
		}
	}

	public Boolean getStrengthCompulsory() {
		if (rxDrug != null) {
			return rxDrug.getStrengthCompulsory();
		}
		else {
			return null;
		}		
	}
	
	public String getSfiCat() {
		if (rxDrug != null) {
			return rxDrug.getSfiCat();
		}
		else {
			return null;
		}
	}

	public void setSfiCat(String sfiCat) {
		if (rxDrug != null) {
			rxDrug.setSfiCat(sfiCat);
		}		
	}

	public DmRouteLite getDmRouteLite() {
		if (rxDrug != null) {
			return rxDrug.getDmRouteLite();
		} 
		else {
			return null;
		}
	}

	public void setDmRouteLite(DmRouteLite dmRouteLite) {
		if (rxDrug != null) {
			rxDrug.setDmRouteLite(dmRouteLite);
		}		
	}

	public Boolean getDrugNameRemark() {
		return drugNameRemark;
	}

	public void setDrugNameRemark(Boolean drugNameRemark) {
		this.drugNameRemark = drugNameRemark;
	}

	public Boolean getSingleUseFlagRemark() {
		return singleUseFlagRemark;
	}

	public void setSingleUseFlagRemark(Boolean singleUseFlagRemark) {
		this.singleUseFlagRemark = singleUseFlagRemark;
	}

	public Boolean getFixPeriodFlagRemark() {
		return fixPeriodFlagRemark;
	}

	public void setFixPeriodFlagRemark(Boolean fixPeriodFlagRemark) {
		this.fixPeriodFlagRemark = fixPeriodFlagRemark;
	}

	public Boolean getSpecialInstructionRemark() {
		return specialInstructionRemark;
	}

	public void setSpecialInstructionRemark(Boolean specialInstructionRemark) {
		this.specialInstructionRemark = specialInstructionRemark;
	}

	public Boolean getCommentRemark() {
		return commentRemark;
	}

	public void setCommentRemark(Boolean commentRemark) {
		this.commentRemark = commentRemark;
	}

	public Boolean getActionStatusRemark() {
		return actionStatusRemark;
	}

	public void setActionStatusRemark(Boolean actionStatusRemark) {
		this.actionStatusRemark = actionStatusRemark;
	}
	
	public RegimenRemark getRegimenRemark() {
		return (RegimenRemark) this.getRegimen();
	}
	
	public void setRegimenRemark(RegimenRemark regimenRemark) {
		this.setRegimen(regimenRemark);
	}
	
	public CapdRxDrugRemark getCapdRxDrugRemark() {
		return (CapdRxDrugRemark)this.capdRxDrug;
	}
	
	public void setCapdRxDrugRemark(CapdRxDrugRemark capdRxDrugRemark) {
		this.capdRxDrug = capdRxDrugRemark;
	}

	public Integer getSeqNum() {
		return seqNum;
	}

	public void setSeqNum(Integer seqNum) {
		this.seqNum = seqNum;
	}

    public List<DispOrderItem> getDuplicateItemList() {
    	if (duplicateItemList == null) {
    		duplicateItemList = new ArrayList<DispOrderItem>();
    	}
   		return duplicateItemList;
	}

	public void setDuplicateItemList(List<DispOrderItem> duplicateItemList) {
		this.duplicateItemList = duplicateItemList;
	}
	
	public Integer getUnitOfCharge() {
		return unitOfCharge;
	}

	public void setUnitOfCharge(Integer unitOfCharge) {
		this.unitOfCharge = unitOfCharge;
	}

	public Integer getCmsConfirmUnitOfCharge() {
		return cmsConfirmUnitOfCharge;
	}

	public void setCmsConfirmUnitOfCharge(Integer cmsConfirmUnitOfCharge) {
		this.cmsConfirmUnitOfCharge = cmsConfirmUnitOfCharge;
	}

	public YesNoBlankFlag getChargeFlag() {
		return chargeFlag;
	}

	public void setChargeFlag(YesNoBlankFlag chargeFlag) {
		this.chargeFlag = chargeFlag;
	}

	public YesNoBlankFlag getCmsConfirmChargeFlag() {
		return cmsConfirmChargeFlag;
	}

	public void setCmsConfirmChargeFlag(YesNoBlankFlag cmsConfirmChargeFlag) {
		this.cmsConfirmChargeFlag = cmsConfirmChargeFlag;
	}

	public Boolean getPpmiFlag() {
		return ppmiFlag;
	}

	public void setPpmiFlag(Boolean ppmiFlag) {
		this.ppmiFlag = ppmiFlag;
	}
	
	public RemarkItemStatus getRemarkItemStatus() {
		return remarkItemStatus;
	}

	public void setRemarkItemStatus(RemarkItemStatus remarkItemStatus) {
		this.remarkItemStatus = remarkItemStatus;
	}

	public String getCmsFmStatus() {
		return cmsFmStatus;
	}

	public void setCmsFmStatus(String cmsFmStatus) {
		this.cmsFmStatus = cmsFmStatus;
	}

	public Boolean getConfirmPrevFmIndFlag() {
		return confirmPrevFmIndFlag;
	}

	public void setConfirmPrevFmIndFlag(Boolean confirmPrevFmIndFlag) {
		this.confirmPrevFmIndFlag = confirmPrevFmIndFlag;
	}
	
	public Boolean getModifyFlag() {
		return modifyFlag;
	}

	public void setModifyFlag(Boolean modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	
	public String getPreVetNoteText() {
		return preVetNoteText;
	}

	public void setPreVetNoteText(String preVetNoteText) {
		this.preVetNoteText = preVetNoteText;
	}

	public String getPreVetNoteUser() {
		return preVetNoteUser;
	}

	public void setPreVetNoteUser(String preVetNoteUser) {
		this.preVetNoteUser = preVetNoteUser;
	}

	public Date getPreVetNoteDate() {
		return preVetNoteDate;
	}

	public void setPreVetNoteDate(Date preVetNoteDate) {
		this.preVetNoteDate = preVetNoteDate;
	}

	public String getCmsExtraXml() {
		return cmsExtraXml;
	}

	public void setCmsExtraXml(String cmsExtraXml) {
		this.cmsExtraXml = cmsExtraXml;
	}

	public DiscontinueStatus getDiscontinueStatus() {
		return discontinueStatus;
	}

	public void setDiscontinueStatus(DiscontinueStatus discontinueStatus) {
		this.discontinueStatus = discontinueStatus;
	}

	public void setDrugKey(Integer drugKey) {
		if (rxDrug != null) {
			rxDrug.setDrugKey(drugKey);
		}
	}

	public Integer getDrugKey() {
		if (rxDrug != null) {
			return rxDrug.getDrugKey();
		} 
		else {
			return null;
		}
	}

	public void setNumOfSystemDeducedItem(Integer numOfSystemDeducedItem) {
		this.numOfSystemDeducedItem = numOfSystemDeducedItem;
	}

	public Integer getNumOfSystemDeducedItem() {
		return numOfSystemDeducedItem;
	}	

	public Boolean getUniqueDrugFlag() {
		return uniqueDrugFlag;
	}

	public void setUniqueDrugFlag(Boolean uniqueDrugFlag) {
		this.uniqueDrugFlag = uniqueDrugFlag;
	}
	
	public Boolean getMpDischargeFlag() {
		return mpDischargeFlag;
	}

	public void setMpDischargeFlag(Boolean mpDischargeFlag) {
		this.mpDischargeFlag = mpDischargeFlag;
	}

	public List<Long> getRelatedMedOrderItemIdList() {
		if (relatedMedOrderItemIdList == null) {
			relatedMedOrderItemIdList = new ArrayList<Long>();
		}
		return relatedMedOrderItemIdList;
	}
	
	public void addRelatedMedOrderItemId(Long relatedMedOrderItemId) {
		getRelatedMedOrderItemIdList().add(relatedMedOrderItemId);
	}

	public void setRelatedMedOrderItemIdList(List<Long> relatedMedOrderItemIdList) {
		this.relatedMedOrderItemIdList = relatedMedOrderItemIdList;
	}

	public String getDiscontinueReason() {
		return discontinueReason;
	}

	public void setDiscontinueReason(String discontinueReason) {
		this.discontinueReason = discontinueReason;
	}

	public String getDiscontinueHospCode() {
		return discontinueHospCode;
	}

	public void setDiscontinueHospCode(String discontinueHospCode) {
		this.discontinueHospCode = discontinueHospCode;
	}

	public Date getDiscontinueDate() {
		return discontinueDate;
	}

	public void setDiscontinueDate(Date discontinueDate) {
		this.discontinueDate = discontinueDate;
	}

	public void markAlertFlag() {
		setAlertFlag(!getMdsAlertList().isEmpty());
	}
	
	public void clearAlert() {
		List<MedOrderItemAlert> moiAlertList = getMedOrderItemAlertList();
		for (MedOrderItemAlert moiAlert : getMdsAlertList()) {
			moiAlertList.remove(moiAlert);
		}
		setMedOrderItemAlertList(moiAlertList);
	}
	
	public List<MedOrderItemAlert> getMdsAlertList() {
		List<MedOrderItemAlert> list = new ArrayList<MedOrderItemAlert>();
		for (MedOrderItemAlert moiAlert : getMedOrderItemAlertList()) {
			if (moiAlert.isMdsAlert()) {
				list.add(moiAlert);
			}
		}
		return list;
	}
	
	public List<MedOrderItemAlert> getPatAlertList() {
		List<MedOrderItemAlert> list = new ArrayList<MedOrderItemAlert>();
		for (MedOrderItemAlert moiAlert : getMedOrderItemAlertList()) {
			if (moiAlert.isPatAlert()) {
				list.add(moiAlert);
			}
		}
		return list;
	}
	
	public List<MedOrderItemAlert> getDdiAlertList() {
		List<MedOrderItemAlert> list = new ArrayList<MedOrderItemAlert>();
		for (MedOrderItemAlert moiAlert : getMedOrderItemAlertList()) {
			if (moiAlert.isDdiAlert()) {
				list.add(moiAlert);
			}
		}
		return list;
	}
	
	public List<MedOrderItemAlert> getDrcAlertList() {
		List<MedOrderItemAlert> list = new ArrayList<MedOrderItemAlert>();
		for (MedOrderItemAlert moiAlert : getMedOrderItemAlertList()) {
			if (moiAlert.isDrcAlert()) {
				list.add(moiAlert);
			}
		}
		return list;
	}
	
	public boolean isSfi() {
		
		boolean isActionStatusSfi = false;

		if (rxDrug != null) {
			if (rxDrug.getActionStatus() == ActionStatus.PurchaseByPatient || rxDrug.getActionStatus() == ActionStatus.SafetyNet) {
				isActionStatusSfi = true;
			}
			else {
				for (DoseGroup doseGroup : rxDrug.getRegimen().getDoseGroupList()) {
					for (Dose dose : doseGroup.getDoseList()) {
						if ((dose.getDoseFluid() != null) && (dose.getDoseFluid().getActionStatus() == ActionStatus.PurchaseByPatient || dose.getDoseFluid().getActionStatus() == ActionStatus.SafetyNet)) {
							isActionStatusSfi = true;
							break;
						}
					}
				}
			}
		}
		else if (ivRxDrug != null) {
			for (IvAdditive ivAdditive : ivRxDrug.getIvAdditiveList()) {
				if (ivAdditive.getActionStatus() == ActionStatus.PurchaseByPatient || ivAdditive.getActionStatus() == ActionStatus.SafetyNet) {
					isActionStatusSfi = true;
					break;
				}
			}
			for (IvFluid ivFluid : ivRxDrug.getIvFluidList()) {
				if (ivFluid.getActionStatus() == ActionStatus.PurchaseByPatient || ivFluid.getActionStatus() == ActionStatus.SafetyNet) {
					isActionStatusSfi = true;
					break;
				}
			}
		}

		return (
			(isActionStatusSfi) &&
			(capdRxDrug == null) &&
			(!FmStatus.FreeTextEntryItem.getDataValue().equals(this.getFmStatus()))
		);
	}
	
	public boolean isCapdSfi(){
		return ((this.getActionStatus() == ActionStatus.PurchaseByPatient || this.getActionStatus() == ActionStatus.SafetyNet) &&  capdRxDrug != null);
	}
	
	public void markPurchaseByPatient() {
		if (rxDrug != null) {
			if (rxDrug.getActionStatus() != ActionStatus.ContinueWithOwnStock) {
				rxDrug.setActionStatus(ActionStatus.PurchaseByPatient);
			}
			for (DoseGroup doseGroup : rxDrug.getRegimen().getDoseGroupList()) {
				for (Dose dose : doseGroup.getDoseList()) {
					if (dose.getDoseFluid() != null && dose.getDoseFluid().getActionStatus() != ActionStatus.ContinueWithOwnStock) {
						dose.getDoseFluid().setActionStatus(ActionStatus.PurchaseByPatient);
					}
				}
			}
		}
		else if (capdRxDrug != null) {
			if (capdRxDrug.getActionStatus() != ActionStatus.ContinueWithOwnStock) {
				capdRxDrug.setActionStatus(ActionStatus.PurchaseByPatient);
			}
		}
		else if (ivRxDrug != null) {
			for (IvAdditive ivAdditive : ivRxDrug.getIvAdditiveList()) {
				if (ivAdditive.getActionStatus() != ActionStatus.ContinueWithOwnStock) {
					ivAdditive.setActionStatus(ActionStatus.PurchaseByPatient);
				}
			}
			for (IvFluid ivFluid : ivRxDrug.getIvFluidList()) {
				if (ivFluid.getActionStatus() != ActionStatus.ContinueWithOwnStock) {
					ivFluid.setActionStatus(ActionStatus.PurchaseByPatient);
				}
			}
		}
	}
	
	public boolean isInfusion() {
		if (capdRxDrug != null || dhRxDrug != null) {
			return false;
		}
		else if (ivRxDrug != null) {
			return true;
		}
		else if (rxDrug.isOralRxDrug()) {
			return false;
		}
		else {
			String siteCategoryCode = rxDrug.getRegimen().getFirstDose().getDmSite().getSiteCategoryCode();
			return "CF".equals(siteCategoryCode) || "IF".equals(siteCategoryCode);
		}
	}
	
	public String getFullDrugDesc() {
		if (rxDrug != null) {
			StringBuilder sb = new StringBuilder(rxDrug.getFullDrugDesc());
			if (getFmStatus() != null) {
				FmStatus fmStatus = FmStatus.dataValueOf(getFmStatus());
				if (fmStatus != null && fmStatus != FmStatus.GeneralDrug && fmStatus != FmStatus.FreeTextEntryItem) {
					sb.append(SPACE).append('<').append(fmStatus.getDisplayValue()).append('>');
				}
			}
			return sb.toString();
		}
		else {
			return null;
		}
	}
	
	public String getDisplayNameSaltForm() {
		if (getRxItem() != null) {
			return getRxItem().getDisplayNameSaltForm();
		}
		else {
			return null;
		}
	}
	
	public String getMdsDisplayName() {
		if (getRxItem() != null) {
			return getRxItem().getMdsDisplayName();
		}
		else {
			return null;
		}
	}
	
	public String getMdsOrderDesc() {
		if (getRxItem() != null) {
			return getRxItem().getMdsOrderDesc();
		}
		else {
			return null;
		}
	}
	
	public void setCmsChargeFlag(YesNoBlankFlag cmsChargeFlag) {
		this.cmsChargeFlag = cmsChargeFlag;
	}

	public YesNoBlankFlag getCmsChargeFlag() {
		return cmsChargeFlag;
	}

	public void updateDdiRelatedAlert() {
		for (MedOrderItemAlert moiAlert : this.getMedOrderItemAlertList()) {
			if (!moiAlert.isDdiAlert()) {
				continue;
			}
			AlertDdim alertDdim = (AlertDdim) moiAlert.getAlert();
			if (alertDdim.isOnHand()) {
				continue;
			}
			
			MedOrderItemAlert otherMoiAlert = getMedOrder().getDdiRelatedAlert(this.itemNum, alertDdim);
			if (otherMoiAlert == null) {
				continue;
			}

			otherMoiAlert.setOverrideReason(moiAlert.getOverrideReason());
			otherMoiAlert.setAckUser(moiAlert.getAckUser());
			otherMoiAlert.setAckDate(moiAlert.getAckDate());
			
			AlertDdim otherAlertDdim = (AlertDdim) otherMoiAlert.getAlert();
			otherAlertDdim.setItemNum1(alertDdim.getItemNum1());
			otherAlertDdim.setItemNum2(alertDdim.getItemNum2());
		}
	}
	
	public void recalTotalDurationInDay() {
		if (!getRxItem().isIvRxDrug()) {
			if (!getRxItem().isCapdRxDrug()) {
				getRxDrug().getRegimen().calTotalDurationInDay();
			}	
		}
	}
	
	public FixedConcnSource getFixedConcnSource() {
		if (getRxItem() != null) {
			return getRxItem().getFixedConcnSource();
		}
		
		return FixedConcnSource.None;
	}

	public List<? extends AlertEntity> getAlertList() {
		return getMedOrderItemAlertList();
	}

	public List<? extends AlertEntity> getPatAlertList(Boolean includeHlaFlag) {
		return getPatAlertList();
	}

	public Boolean getManualProfileFlag() {
		return manualProfileFlag;
	}

	public void setManualProfileFlag(Boolean manualProfileFlag) {
		this.manualProfileFlag = manualProfileFlag;
	}

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public void setAlertDesc(String alertDesc) {
		this.alertDesc = alertDesc;
	}

	public String getAlertDesc() {
		return alertDesc;
	}

	public void setAlertRemark(String alertRemark) {
		this.alertRemark = alertRemark;
	}

	public String getAlertRemark() {
		return alertRemark;
	}
	
	public String getDhRxItemXml() {
		return dhRxItemXml;
	}

	public void setDhRxItemXml(String dhRxItemXml) {
		this.dhRxItemXml = dhRxItemXml;
	}

	public boolean isManualItem() {
		return orgItemNum != null && orgItemNum.intValue() >= 10000;
	}

	public Boolean getIsMultipleChemoItem() {
		return isMultipleChemoItem;
	}

	public void setIsMultipleChemoItem(Boolean isMultipleChemoItem) {
		this.isMultipleChemoItem = isMultipleChemoItem;
	}
}
