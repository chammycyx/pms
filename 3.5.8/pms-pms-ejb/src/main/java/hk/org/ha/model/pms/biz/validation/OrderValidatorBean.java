package hk.org.ha.model.pms.biz.validation;

import static hk.org.ha.model.pms.prop.Prop.ONESTOP_TICKET_TICKETNUM_PREFIX;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.udt.validation.SearchTextType;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("orderValidator")
@MeasureCalls
public class OrderValidatorBean implements OrderValidatorLocal {
	
	private static final Pattern ticketWithoutCheckDigitPattern = Pattern.compile("^[0-9]+$");
	private static final Pattern ticketWithCheckDigitPattern = Pattern.compile("^9[0-9]+$");
	private static final Pattern refNumPattern = Pattern.compile("^[0-9][0-9][0-9][0-9][A-Za-z]?$");
	private static final Pattern orderNumPattern1 = Pattern.compile("^MOE[A-Za-z][A-Za-z][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$");
	private static final Pattern orderNumPattern2 = Pattern.compile("^MOE[A-Za-z][A-Za-z][A-Za-z ][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$");
	private static final Pattern orderNumWithCheckDigitPattern1 = Pattern.compile("^MOE[A-Za-z][A-Za-z][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$");
	private static final Pattern orderNumWithCheckDigitPattern2 = Pattern.compile("^MOE[A-Za-z][A-Za-z][A-Za-z ][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$");		
	private static final Pattern refillNumPattern1 = Pattern.compile("^[A-Za-z][A-Za-z][0-9]+$");
	private static final Pattern refillNumPattern2 = Pattern.compile("^[A-Za-z][A-Za-z][A-Za-z ][0-9]+$");
	private static final Pattern refillNumWithAeHnPattern = Pattern.compile("^((AE)|(HN))[0-9]+$");
	private static final Pattern sfiRefillNumPattern = Pattern.compile("^[A-Za-z][A-Za-z][A-Za-z ][A-Za-z][A-Za-z][A-Za-z ][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$");
	private static final Pattern sfiInvoiceNumPattern = Pattern.compile("^I[A-Za-z][A-Za-z][A-Za-z ][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$");
		
	private Map<String, Pattern> ticketPatternMap = new HashMap<String, Pattern>();

	private Pattern getTicketPattern() {
		String ticketNumPrefix = ONESTOP_TICKET_TICKETNUM_PREFIX.get();
		Pattern ticketPattern = ticketPatternMap.get(ticketNumPrefix);		
		if (ticketPattern == null) {
			ticketPattern = Pattern.compile("^[" + ticketNumPrefix + "][0-9][0-9][0-9][0-9]$");
			ticketPatternMap.put(ticketNumPrefix, ticketPattern);
		}
		return ticketPattern;
	}
	
	public SearchTextType checkSearchType(String searchText) {
		if (isOrderNum(searchText)) {
			if ((searchText.length() == 15 && orderNumWithCheckDigitPattern1.matcher(searchText).find()) || searchText.length() == 16) {
				return checkOrderNumDigit(searchText);
			}
			return SearchTextType.OrderNum;
		} else if (isRefNum(searchText)) {
			return SearchTextType.OrderRefNum;
		} else if (isTicketNum(searchText)) {
			if (searchText.length() == 14 && ticketWithCheckDigitPattern.matcher(searchText).find()) {
				return checkSGLTicketNumDigit(searchText);
			}
			return SearchTextType.Ticket;
		} else if (isRefillNum(searchText)) {
			return SearchTextType.Refill;
		} else if (isSfiRefill(searchText)) {
			return SearchTextType.SfiRefill;
		} else if (isSfiInvoice(searchText)){
			return SearchTextType.SfiInvoice;
		}
		else {
			return SearchTextType.Unknown;
		}
	}	
	
	private Boolean isOrderNum(String orderNum) {
		if (orderNum.length() == 14 && orderNumPattern1.matcher(orderNum).find()) {
			return true;
		}
		
		if (orderNum.length() == 15 && (orderNumPattern2.matcher(orderNum).find() || orderNumWithCheckDigitPattern1.matcher(orderNum).find())) {
			return true;
		}
		
		if (orderNum.length() == 16 && orderNumWithCheckDigitPattern2.matcher(orderNum).find()) {
			return true;
		}
				
		return false;
	}
		
	private SearchTextType checkOrderNumDigit(String orderNum) {
		String trunOrderNum;
		int digit = 0;
		int totalSum = 0;
		int checkSum;
		String checkDigit;
		
		if 	(orderNum.length() == 15) {
			trunOrderNum = orderNum.substring(5, 14);
		} else {
			trunOrderNum = orderNum.substring(6, 15);
		}
		
		for (int i = trunOrderNum.length(); i > 0; i--) {
			int index = trunOrderNum.length() - i;
			if (trunOrderNum.charAt(index) >= 'A' && trunOrderNum.charAt(index) <= 'Z') {
				digit = 66 - (int) trunOrderNum.charAt(index);
			} else {
				digit = Integer.parseInt(trunOrderNum.substring(index, index + 1));
			}
			totalSum = totalSum + digit * i;
		}
		
		checkSum = trunOrderNum.length() - (totalSum % trunOrderNum.length());
		
		if (checkSum > 9) {
			checkDigit = Character.toString((char) (55 + checkSum));
		} else {
			checkDigit = Integer.toString(checkSum);
		}
				
		if (orderNum.substring(orderNum.length()-1).equals(checkDigit)) {
			return SearchTextType.OrderNum;
		}
		
		return SearchTextType.Unknown;
	}
	
	private Boolean isRefNum(String refNum) {
		if (refNum.length() <= 5 && refNumPattern.matcher(refNum).find()) {
			return true;
		} 
			
		return false;
	}
	
	private Boolean isTicketNum(String ticketNum) {
		if (ticketNum.length() == 5 && this.getTicketPattern().matcher(ticketNum).find()) {
			return true;
		}
		
		if (ticketNum.length() == 12 && ticketWithoutCheckDigitPattern.matcher(ticketNum).find()) {
			return true;
		}
		
		if (ticketNum.length() == 14 && (ticketWithCheckDigitPattern.matcher(ticketNum).find() || ticketWithoutCheckDigitPattern.matcher(ticketNum).find())) {
			return true;
		}
						
		return false;
	}
	
	private SearchTextType checkSGLTicketNumDigit(String ticketNum) {
		//remove the first two digit of the ticket
		String trunTicketNum = ticketNum.substring(2);
		int digit = 0;
		int totalSum = 0;
		String checkSum;
		
		for (int i = trunTicketNum.length(); i > 0; i--) {
			int index = trunTicketNum.length() - i;
			if (trunTicketNum.charAt(index) >= 'A' && trunTicketNum.charAt(index) <= 'Z') {
				digit = 66 - (int) trunTicketNum.charAt(index);
			} else {
				digit = Integer.parseInt(trunTicketNum.substring(index, index + 1));
			}
			totalSum = totalSum + digit * i;
		}
		
		checkSum = Integer.toString(trunTicketNum.length() - (totalSum % trunTicketNum.length()));
		
		if (ticketNum.substring(1,2).equals(checkSum.substring(checkSum.length() -1, checkSum.length()))) {
			return SearchTextType.Ticket;
		}
		
		return SearchTextType.Unknown;
	}
		
	private Boolean isRefillNum(String refillNum) {
		if ((refillNum.length() == 13 || refillNum.length() ==15) && !refillNumWithAeHnPattern.matcher(refillNum).find()) {
			if (refillNumPattern1.matcher(refillNum).find()) {
				return true;
			}
		}
		
		if ((refillNum.length() == 14 || refillNum.length() == 16) && refillNumPattern2.matcher(refillNum).find()) {
			return true;
		}
					
		return false;
	}
	
	private Boolean isSfiRefill(String sfiRefillNum) {
		if (sfiRefillNum.length() == 19 && sfiRefillNumPattern.matcher(sfiRefillNum).find()) {
			return true;
		}
		return false;
	}
	
	private Boolean isSfiInvoice(String sfiInvoiceNum) {
		if (sfiInvoiceNum.length() == 15 && sfiInvoiceNumPattern.matcher(sfiInvoiceNum).find()) {
			return true;
		}				
		return false;
	}
}