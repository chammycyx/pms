package hk.org.ha.model.pms.vo.uncollect;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class UncollectOrder implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Date ticketDate;
	
	private String ticketNum;
	
	private String caseNum;
	
	private String receiptNum;
	
	private Long dispOrderId;
	
	private Long pharmOrderVersion;
	
	private boolean markDelete;
	
	private boolean sfiFlag;
	
	private boolean bold;
	
	private boolean withinUncollectDuration;

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getReceiptNum() {
		return receiptNum;
	}

	public void setReceiptNum(String receiptNum) {
		this.receiptNum = receiptNum;
	}

	public void setMarkDelete(boolean markDelete) {
		this.markDelete = markDelete;
	}

	public boolean isMarkDelete() {
		return markDelete;
	}

	public void setSfiFlag(boolean sfiFlag) {
		this.sfiFlag = sfiFlag;
	}

	public boolean isSfiFlag() {
		return sfiFlag;
	}

	public void setWithinUncollectDuration(boolean withinUncollectDuration) {
		this.withinUncollectDuration = withinUncollectDuration;
	}

	public boolean isWithinUncollectDuration() {
		return withinUncollectDuration;
	}

	public void setBold(boolean bold) {
		this.bold = bold;
	}

	public boolean isBold() {
		return bold;
	}

	public Long getPharmOrderVersion() {
		return pharmOrderVersion;
	}

	public void setPharmOrderVersion(Long pharmOrderVersion) {
		this.pharmOrderVersion = pharmOrderVersion;
	}

	public void setDispOrderId(Long dispOrderId) {
		this.dispOrderId = dispOrderId;
	}

	public Long getDispOrderId() {
		return dispOrderId;
	}

	public void setTicketDate(Date ticketDate) {
		this.ticketDate = ticketDate;
	}

	public Date getTicketDate() {
		return ticketDate;
	}
	
}