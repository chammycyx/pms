package hk.org.ha.model.pms.udt.disp;

import java.util.Arrays;
import java.util.List;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PharmOrderStatus implements StringValuedEnum {

    Outstanding("O", "Outstanding"),
	Fulfilling("F", "Fulfilling"),
	Completed("C", "Completed"), 
	Deleted("D", "Deleted"),
	SysDeleted("X", "SysDeleted");
	
    public static final List<PharmOrderStatus> Deleted_SysDeleted = Arrays.asList(Deleted,SysDeleted);

    private final String dataValue;
    private final String displayValue;

    PharmOrderStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static PharmOrderStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PharmOrderStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<PharmOrderStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<PharmOrderStatus> getEnumClass() {
    		return PharmOrderStatus.class;
    	}
    }
}
