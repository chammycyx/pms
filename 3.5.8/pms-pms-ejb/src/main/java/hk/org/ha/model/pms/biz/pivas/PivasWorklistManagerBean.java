package hk.org.ha.model.pms.biz.pivas;

import static hk.org.ha.model.pms.prop.Prop.PIVAS_SUPPLYDATE_DURATION;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.corp.cache.DmDailyFrequencyCacherInf;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDailyFrequency;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrug;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrugManuf;
import hk.org.ha.model.pms.dms.udt.pivas.PivasDrugManufStatus;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.persistence.pivas.MaterialRequestItem;
import hk.org.ha.model.pms.persistence.pivas.PivasWorklist;
import hk.org.ha.model.pms.persistence.reftable.pivas.PivasServiceHour;
import hk.org.ha.model.pms.udt.RecordStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
import hk.org.ha.model.pms.udt.pivas.MaterialRequestItemStatus;
import hk.org.ha.model.pms.udt.pivas.PivasWorklistStatus;
import hk.org.ha.model.pms.udt.rx.RxItemType;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.model.pms.vo.pivas.PivasDiluentInfo;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.Freq;
import hk.org.ha.model.pms.vo.rx.IvAdditive;
import hk.org.ha.model.pms.vo.rx.IvRxDrug;
import hk.org.ha.model.pms.vo.rx.RxDrug;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.joda.time.DateTime;

@AutoCreate
@Stateless
@Name("pivasWorklistManager")
@MeasureCalls
@SuppressWarnings("unchecked")
public class PivasWorklistManagerBean implements PivasWorklistManagerLocal {
		
	public static int MAX_SEARCH_DAY = 365;
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;

	@In
	private DmDailyFrequencyCacherInf dmDailyFrequencyCacher;
	
	@In
	private PivasServiceHourManagerLocal pivasServiceHourManager;
	
	private static PivasDiluentInfoComparator pivasDiluentInfoComparator = new PivasDiluentInfoComparator();
	private static PivasDrugManufComparator pivasDrugManufComparator = new PivasDrugManufComparator();
	private static final List<PivasWorklistStatus> NEW_PREPARED_REFILL = Arrays.asList(PivasWorklistStatus.New, PivasWorklistStatus.Prepared, PivasWorklistStatus.Refill);
	private static final List<PivasWorklistStatus> DELELTED_SYSDELETED = Arrays.asList(PivasWorklistStatus.Deleted, PivasWorklistStatus.SysDeleted);
	
	private static final String PIVAS_WORKLIST_QUERY_HINT_FETCH1 = "o.medProfileMoItem.medProfileItem";	
	private static final String PIVAS_WORKLIST_QUERY_HINT_FETCH2 = "o.firstDoseMaterialRequest.medProfileMoItem";
	private static final String PIVAS_WORKLIST_QUERY_HINT_FETCH3 = "o.pivasPrep";
	private static final String PIVAS_WORKLIST_QUERY_HINT_FETCH4 = "o.medProfile";
	private static final String PIVAS_WORKLIST_QUERY_HINT_BATCh1 = "o.firstDoseMaterialRequest.medProfileMoItem.medProfilePoItemList";
	private static final String PIVAS_WORKLIST_QUERY_HINT_BATCH2 = "o.firstDoseMaterialRequest.materialRequestItemList.medProfilePoItem";
	private static final String PIVAS_WORKLIST_QUERY_HINT_BATCH3 = "o.medProfileMoItem.medProfilePoItemList";
	
	private void lookupPivasWorklistLazyLink(PivasWorklist pivasWorklist) {
		MedProfileUtils.lookup(pivasWorklist, PIVAS_WORKLIST_QUERY_HINT_FETCH1);
		MedProfileUtils.lookup(pivasWorklist, PIVAS_WORKLIST_QUERY_HINT_FETCH3);
		MedProfileUtils.lookup(pivasWorklist, PIVAS_WORKLIST_QUERY_HINT_FETCH4);
		MedProfileUtils.lookup(pivasWorklist, PIVAS_WORKLIST_QUERY_HINT_BATCh1);
		MedProfileUtils.lookup(pivasWorklist, PIVAS_WORKLIST_QUERY_HINT_BATCH2);
		MedProfileUtils.lookup(pivasWorklist, PIVAS_WORKLIST_QUERY_HINT_BATCH3);
	}
	
	public PivasWorklist retrievePivasWorklistByMedProfileItemId(Long medProfileItemId) {
		List<PivasWorklist> pivasWorklistList = em.createQuery(
				"select o from PivasWorklist o " + // 20160615 index check : MedProfileMoItem.medProfileItemId : FK_MED_PROFILE_MO_ITEM_01
				"where o.status not in :deleteStatusList " +
				"and o.medProfileMoItem.medProfileItem.id = :medProfileItemId " +
				"and o.refillFlag = :refillFlag ")
		.setParameter("deleteStatusList", DELELTED_SYSDELETED)
		.setParameter("medProfileItemId", medProfileItemId)
		.setParameter("refillFlag", Boolean.FALSE)		
		.setHint(QueryHints.FETCH, PIVAS_WORKLIST_QUERY_HINT_FETCH1)		
		.setHint(QueryHints.LEFT_FETCH, PIVAS_WORKLIST_QUERY_HINT_FETCH2)		
		.getResultList();
		
		if (pivasWorklistList.size() == 0) {
			logger.info("No pivas detail in DB!");
			return null;
		} 
		
		for (PivasWorklist pivasWorklist : pivasWorklistList) {			
			if ( pivasWorklist.getPivasPrep() != null) {
				if (Boolean.TRUE.equals(pivasWorklist.getAdHocFlag()) ) {
					continue;
				}
				if (pivasWorklist.getPivasPrep().getStatus() == RecordStatus.Delete) {
					continue;
				}
			}
				
			if (MedProfileItemStatus.Unvet_Vetted.contains(pivasWorklist.getMedProfileMoItem().getMedProfileItem().getStatus()) 
					&& !pivasWorklist.getMedProfileMoItem().getMedProfileItem().getMedProfileMoItem().getId().equals(pivasWorklist.getMedProfileMoItem().getId())) {
				logger.info("ignore completed PIVAS worklist which base order has been modified");
				return null;//ignore completed PIVAS worklist which base order has been modified
			} else {
				lookupPivasWorklistLazyLink(pivasWorklist);
				return pivasWorklist;
			}
		}

		return null;
	}
	
	public List<PivasWorklist> retrieveRedundantWorklist(List<Long> medProfileItemIdList) {
		List<PivasWorklist> pivasWorklistList = em.createQuery(
					"select o from PivasWorklist o " + // 20160615 index check : MedProfileMoItem.medProfileItemId : FK_MED_PROFILE_MO_ITEM_01
					"where o.status in :statusList " +
					"and o.medProfileMoItem.medProfileItem.status not in :vettedVerified  " +
					"and o.medProfileMoItem.medProfileItem.id in :medProfileItemIdList ") 
			.setParameter("vettedVerified", MedProfileItemStatus.Vetted_Verified)
			.setParameter("statusList", NEW_PREPARED_REFILL)
			.setParameter("medProfileItemIdList", medProfileItemIdList)
			.setHint(QueryHints.FETCH, PIVAS_WORKLIST_QUERY_HINT_FETCH1)		
			.setHint(QueryHints.LEFT_FETCH, PIVAS_WORKLIST_QUERY_HINT_FETCH2)	
			.getResultList();
		
		for (PivasWorklist pivasWorklist : pivasWorklistList) {		
			lookupPivasWorklistLazyLink(pivasWorklist);
		}
		return pivasWorklistList;
	}
	
	public List<PivasServiceHour> retrievePivasSupplyDateList(String workstoreGroupCode, Hospital hospital) {
		return retrievePivasSupplyDateList(workstoreGroupCode, new Date(), hospital);
	}
	
	public List<PivasServiceHour> retrievePivasSupplyDateList(String workstoreGroupCode, Date dueDate, Hospital hospital) {
		Map<String, List<PivasServiceHour>> pivasServiceHourListByDayOfWeekMap = pivasServiceHourManager.retrievePivasServiceHourListByDayOfWeekMap(workstoreGroupCode, hospital);
		
		Set<Date> pivasHolidayDateSet = pivasServiceHourManager.retrievePivasHolidayDateSet(workstoreGroupCode);
		
		Date searchDate = DateUtils.truncate(dueDate, Calendar.DAY_OF_MONTH);
		
		Integer supplyDateDuration = PIVAS_SUPPLYDATE_DURATION.get(hospital);
		Date supplyDateEnd = new DateTime(searchDate).plusDays(supplyDateDuration-1).toDate();

		// get all supply date from day 0 to day 6
		List<PivasServiceHour> outPivasServiceHourList = new ArrayList<PivasServiceHour>();
		
		int count = 0;
		
		while (searchDate.compareTo(supplyDateEnd) <= 0 && count < MAX_SEARCH_DAY) {
			count++;
			
			if ( ! pivasHolidayDateSet.contains(searchDate)) {
				int dayOfWeekValue = new DateTime(searchDate).getDayOfWeek();
				
				List<PivasServiceHour> pivasServiceHourList = pivasServiceHourListByDayOfWeekMap.get(String.valueOf(dayOfWeekValue));
				
				if (pivasServiceHourList != null) {
					for (PivasServiceHour pivasServiceHour : pivasServiceHourList) {
						Integer supplyTime = pivasServiceHour.getSupplyTime();
						int supplyHour = supplyTime / 100;
						int supplyMinutes = supplyTime % 100;
						Date supplyDate = new DateTime(searchDate).plusHours(supplyHour).plusMinutes(supplyMinutes).toDate();
						
						pivasServiceHour.setSupplyDate(supplyDate);
						outPivasServiceHourList.add(pivasServiceHour);
					}
				}
			}
			
			searchDate = new DateTime(searchDate).plusDays(1).toDate();
		}
		
		if (count >= MAX_SEARCH_DAY) {
			throw new UnsupportedOperationException("Count is over 365 days when calculating issue quantity, dueDate = " + dueDate);
		}
		
		return outPivasServiceHourList;
	}
	
	public Date calculateSupplyDateByDueDate(String workstoreGroupCode, Hospital hospital, Date dueDate, boolean throwException) {
		if (dueDate == null) {
			return null;
		}
		
		Map<String, List<PivasServiceHour>> pivasServiceHourListByDayOfWeekMap = pivasServiceHourManager.retrievePivasServiceHourListByDayOfWeekMap(workstoreGroupCode, hospital);

		// retrieve holiday map
		Set<Date> pivasHolidayDateSet = pivasServiceHourManager.retrievePivasHolidayDateSet(workstoreGroupCode);
		
		// search for next supply date
		Date dueDateWithoutTime = DateUtils.truncate(dueDate, Calendar.DAY_OF_MONTH);
		DateTime searchDateDt = new DateTime(dueDateWithoutTime);
		int hour = new DateTime(dueDate).getHourOfDay();
		int minutes = new DateTime(dueDate).getMinuteOfHour();
		Integer searchTime = hour * 100 + minutes;
		
		int count = 0;
		boolean prevSupplyDateFound = false;
		Date prevSupplyDate = null;
		
		while (count < MAX_SEARCH_DAY) {
			count++;
			
			if ( ! pivasHolidayDateSet.contains(searchDateDt.toDate())) {
				int dayOfWeekValue = searchDateDt.getDayOfWeek();
				
				List<PivasServiceHour> pivasServiceHourList = pivasServiceHourListByDayOfWeekMap.get(String.valueOf(dayOfWeekValue));
				
				if (pivasServiceHourList != null) {
					for (int i=pivasServiceHourList.size()-1; i>=0; i--) {
						PivasServiceHour pivasServiceHour = pivasServiceHourList.get(i);
						
						if (searchTime == null || (searchTime != null && pivasServiceHour.getSupplyTime() <= searchTime) ) {
							prevSupplyDateFound = true;
							int supplyHour = pivasServiceHour.getSupplyTime() / 100;
							int supplyMinutes = pivasServiceHour.getSupplyTime() % 100;
							prevSupplyDate = searchDateDt.plusHours(supplyHour).plusMinutes(supplyMinutes).toDate();
							break;
						}
					}
				}
				
				if (prevSupplyDateFound) {
					// exit looping
					break;
				}
			}
			
			searchDateDt = searchDateDt.minusDays(1);
			searchTime = null;	// since next search date is after supply date, all time can be searched from next search date 
		}
		
		if (count >= MAX_SEARCH_DAY) {
			if ( throwException ) {
				throw new UnsupportedOperationException("Count is over 365 days when calculating issue quantity, dueDate = " + dueDate);
			} else {
				return null;
			}
		}
		
		return prevSupplyDate;
	}
	
	public void resumeDispInPmsIp(Long medProfileItemId) {
		List<PivasWorklist> pivasWorklistList = em.createQuery(
				"select o from PivasWorklist o " + // 20160615 index check : MedProfileMoItem.medProfileItemId : FK_MED_PROFILE_MO_ITEM_01
				"where o.medProfileMoItem.medProfileItem.id = :medProfileItemId ")
		.setParameter("medProfileItemId", medProfileItemId)
		.setHint(QueryHints.FETCH, "o.medProfileMoItem.medProfileItem")
		.setHint(QueryHints.LEFT_FETCH, "o.pivasPrep")
		.setHint(QueryHints.LEFT_FETCH, "o.firstDoseMaterialRequest")
		.setHint(QueryHints.LEFT_FETCH, "o.materialRequest")
		.getResultList();
		
		if (pivasWorklistList.size() == 0) {
			logger.info("No pivas detail in DB for MedProfileItem[id=#0]!",medProfileItemId);
		} 
		
		List<MaterialRequestItem> requestItemList;
		for (PivasWorklist worklist : pivasWorklistList) {
			//Mark pivasFlag as false
			if (worklist.getMedProfileMoItemId().equals(worklist.getMedProfileMoItem().getMedProfileItem().getMedProfileMoItemId())) {
				worklist.getMedProfileMoItem().setPivasFlag(Boolean.FALSE);
			}
			//Mark delete to worklist for New, Prepared, Refill
			if (NEW_PREPARED_REFILL.contains(worklist.getStatus())) {
				worklist.setStatus(PivasWorklistStatus.Deleted);				
			}
			requestItemList = new ArrayList<MaterialRequestItem>();
			if (worklist.getFirstDoseMaterialRequest() != null) {
				requestItemList.addAll(worklist.getFirstDoseMaterialRequest().getMaterialRequestItemList());
				
			}
			if (worklist.getMaterialRequest() != null) {
				requestItemList.addAll(worklist.getMaterialRequest().getMaterialRequestItemList());
			}
			//Mark delete to all materialRequestItem
			for (MaterialRequestItem materialRequestItem : requestItemList) {
				materialRequestItem.setStatus(MaterialRequestItemStatus.Deleted);	
			}
			//Mark delete to all pivasPrep
			if (worklist.getPivasPrep() != null) {
				worklist.getPivasPrep().setStatus(RecordStatus.Delete);
			}
		}
	}
	
	public List<PivasDiluentInfo> convertPivasDiluentList(List<PivasDrug> pivasDrugDiluentList, Workstore workstore){
		List<PivasDiluentInfo> diluentList = new ArrayList<PivasDiluentInfo>();
		for ( PivasDrug pivasDrug : pivasDrugDiluentList ) {
			DmDrug dmDrug = retrieveValidDmDrug(pivasDrug.getItemCode(), workstore);
			if ( dmDrug != null ) {
				if ( pivasDrug.getDmDiluent() != null && 
						"N".equals(pivasDrug.getDmDiluent().getSuspend()) && 
						"Y".equals(pivasDrug.getDmDiluent().getDiluentFlag()) )
				{
					List<PivasDrugManuf> pivasDrugManufList = new ArrayList<PivasDrugManuf>();
					for ( PivasDrugManuf pivasDrugManuf : pivasDrug.getPivasDrugManufList() ) {
						if ( pivasDrugManuf.getStatus() == PivasDrugManufStatus.Active &&
								"N".equals(pivasDrugManuf.getCompany().getSuspend()) ) {
							pivasDrugManufList.add(pivasDrugManuf);
						}
					}
					if ( !pivasDrugManufList.isEmpty() ) {
						Collections.sort(pivasDrugManufList, pivasDrugManufComparator);
						PivasDiluentInfo pivasDiluentInfo = new PivasDiluentInfo();
						pivasDiluentInfo.setItemCode(pivasDrug.getItemCode());
						pivasDiluentInfo.setDmDrug(dmDrug);
						pivasDiluentInfo.setActionType(pivasDrug.getDiluentActionType());
						pivasDiluentInfo.setDmDiluent(pivasDrug.getDmDiluent());
						pivasDiluentInfo.setPivasDrugManufList(pivasDrugManufList);
						diluentList.add(pivasDiluentInfo);
					}
				}
			}
		}
		Collections.sort(diluentList, pivasDiluentInfoComparator);
		return diluentList;
	}
	
	public List<PivasDiluentInfo> convertPivasSolventList(List<PivasDrug> pivasDrugSolventList, Workstore workstore){
		List<PivasDiluentInfo> solventList = new ArrayList<PivasDiluentInfo>();
		for ( PivasDrug pivasDrug : pivasDrugSolventList ) {
			DmDrug dmDrug = retrieveValidDmDrug(pivasDrug.getItemCode(), workstore);
			if ( dmDrug != null ) {
				if ( pivasDrug.getDmDiluent() != null && 
						"N".equals(pivasDrug.getDmDiluent().getSuspend()) && 
						"Y".equals(pivasDrug.getDmDiluent().getSolventFlag()) )
				{
					List<PivasDrugManuf> pivasDrugManufList = new ArrayList<PivasDrugManuf>();
					for ( PivasDrugManuf pivasDrugManuf : pivasDrug.getPivasDrugManufList() ) {
						if ( pivasDrugManuf.getStatus() == PivasDrugManufStatus.Active && 
								"N".equals(pivasDrugManuf.getCompany().getSuspend())) {
							pivasDrugManufList.add(pivasDrugManuf);
						}
					}
					if ( !pivasDrugManufList.isEmpty() ) {
						Collections.sort(pivasDrugManufList, pivasDrugManufComparator);
						PivasDiluentInfo pivasDiluentInfo = new PivasDiluentInfo();
						pivasDiluentInfo.setItemCode(pivasDrug.getItemCode());
						pivasDiluentInfo.setDmDrug(dmDrug);
						pivasDiluentInfo.setActionType(pivasDrug.getSolventActionType());
						pivasDiluentInfo.setDmDiluent(pivasDrug.getDmDiluent());
						pivasDiluentInfo.setPivasDrugManufList(pivasDrugManufList);
						solventList.add(pivasDiluentInfo);
					}
				}
			}
		}
		Collections.sort(solventList, pivasDiluentInfoComparator);
		return solventList;
	}
	
	public DmDrug retrieveValidDmDrug(String itemCode, Workstore workstore){
		WorkstoreDrug workstoreDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, itemCode);
		if ( workstoreDrug != null && workstoreDrug.getMsWorkstoreDrug() != null) {
			if( "A".equals(workstoreDrug.getMsWorkstoreDrug().getItemStatus())){
				return workstoreDrug.getDmDrug();
			}
		}
		return null;
	}

	public Dose extractOrderFirstDose(MedProfileMoItem medProfileMoItem) {
		Dose firstDose = null; 
		if (medProfileMoItem.isManualItem()) {
			MedProfilePoItem medProfilePoItem = medProfileMoItem.getMedProfilePoItemList().get(0);
			firstDose = medProfilePoItem.getRegimen().getDoseGroupList().get(0).getDoseList().get(0);
		} else {
			RxDrug rxDrug = (RxDrug)medProfileMoItem.getRxItem();
			firstDose = rxDrug.getRegimen().getDoseGroupList().get(0).getDoseList().get(0);
		}
		
		return firstDose;
	}

	public IvAdditive extractOrderFirstIvAdditive(MedProfileMoItem medProfileMoItem) {
		IvRxDrug ivRxDrug = (IvRxDrug)medProfileMoItem.getRxItem();
		IvAdditive firstIvAdditive = ivRxDrug.getIvAdditiveList().get(0);
		return firstIvAdditive;
	}
	
	public BigDecimal extractOrderDosage(MedProfileMoItem medProfileMoItem) {
		if (medProfileMoItem.getRxItemType() == RxItemType.Iv) {
			IvAdditive firstIvAdditive = extractOrderFirstIvAdditive(medProfileMoItem);
			return firstIvAdditive.getDosage();
		} else {
			Dose firstDose = extractOrderFirstDose(medProfileMoItem);
			return firstDose.getDosage();
		}
	}
	
	public String extractOrderDosageUnit(MedProfileMoItem medProfileMoItem) {
		if (medProfileMoItem.getRxItemType() == RxItemType.Iv) {
			IvAdditive firstIvAdditive = extractOrderFirstIvAdditive(medProfileMoItem);
			return firstIvAdditive.getDosageUnit();
		} else {
			Dose firstDose = extractOrderFirstDose(medProfileMoItem);
			return firstDose.getDosageUnit();
		}
	}
	
	public Freq extractOrderDailyFreq(MedProfileMoItem medProfileMoItem) {
		Freq dailyFreq = null;
		
		if (medProfileMoItem.getRxItemType() == RxItemType.Iv) {
			IvRxDrug ivRxDrug = (IvRxDrug)medProfileMoItem.getRxItem();
			dailyFreq = ivRxDrug.getDailyFreq();
		} else {
			Dose firstDose = extractOrderFirstDose(medProfileMoItem);
			dailyFreq = firstDose.getDailyFreq();
		}
		
		// hardcode to STAT frequency if there is no frequency (follow item conversion logic)
		if (StringUtils.isEmpty(dailyFreq.getCode())) {
			dailyFreq.setCode("00037");
			DmDailyFrequency dmDailyFrequency = dmDailyFrequencyCacher.getDmDailyFrequencyByDailyFreqCode(dailyFreq.getCode());
			if (dmDailyFrequency != null) {
				dailyFreq.setDesc(dmDailyFrequency.getDailyFreqDesc());
			}
		}
		
		return dailyFreq;
	}

	public Freq extractOrderSupplFreq(MedProfileMoItem medProfileMoItem) {
		Freq supplFreq = null;
		
		if (medProfileMoItem.getRxItemType() != RxItemType.Iv) {
			Dose firstDose = extractOrderFirstDose(medProfileMoItem);
			supplFreq = firstDose.getSupplFreq();
		}
		
		return supplFreq;
	}
	
	public String extractOrderSiteCode(MedProfileMoItem medProfileMoItem) {
		if (medProfileMoItem.getRxItemType() == RxItemType.Iv) {
			IvRxDrug ivRxDrug = (IvRxDrug)medProfileMoItem.getRxItem();
			return ivRxDrug.getSiteCode();
		} else {
			Dose firstDose = extractOrderFirstDose(medProfileMoItem);
			return firstDose.getSiteCode();
		}
	}

	public Integer extractOrderDispQty(MedProfileMoItem medProfileMoItem) {
		if (medProfileMoItem.getRxItemType() == RxItemType.Iv) {
			IvRxDrug ivRxDrug = (IvRxDrug)medProfileMoItem.getRxItem();
			return ivRxDrug.getDispQty();
		} else {
			Dose firstDose = extractOrderFirstDose(medProfileMoItem);
			return firstDose.getDispQty();
		}
	}
	
	public static class PivasDiluentInfoComparator implements Comparator<PivasDiluentInfo>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(PivasDiluentInfo pivasDiluentInfo1, PivasDiluentInfo pivasDiluentInfo2) {
			return pivasDiluentInfo1.getItemCode().compareTo(pivasDiluentInfo2.getItemCode());
		}
	}
	 
	public static class PivasDrugManufComparator implements Comparator<PivasDrugManuf>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(PivasDrugManuf pivasDrugManuf1, PivasDrugManuf pivasDrugManuf2) {
			return pivasDrugManuf1.getManufacturerCode().compareTo(pivasDrugManuf2.getManufacturerCode());
		}
    }	
}
