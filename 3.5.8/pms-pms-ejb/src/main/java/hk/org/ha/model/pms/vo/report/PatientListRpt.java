package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class PatientListRpt implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String wardCode;
	
	private List<PatientListRptDtl> patientListDtlList;

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public List<PatientListRptDtl> getPatientListDtlList() {
		return patientListDtlList;
	}

	public void setPatientListDtlList(List<PatientListRptDtl> patientListDtlList) {
		this.patientListDtlList = patientListDtlList;
	}
}
