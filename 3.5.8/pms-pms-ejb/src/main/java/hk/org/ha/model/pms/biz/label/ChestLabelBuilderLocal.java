package hk.org.ha.model.pms.biz.label;

import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.vo.label.ChestLabelList;

import javax.ejb.Local;

@Local
public interface ChestLabelBuilderLocal {
	
	ChestLabelList convertToChestLabelList(DispOrder dispOrder);
	
}