package hk.org.ha.model.pms.vo.printing;


import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class RenderAndPrintJob extends RenderJob implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private PrinterSelect printerSelect;
	private String printInfo;	
	private String printDocType;	
		
	public RenderAndPrintJob(String docName, PrintOption printOption,
			PrinterSelect printerSelect, Map<String, Object> parameters,
			List<?> objectList, String printInfo, String printDocType) {
		super(docName, printOption, parameters, objectList);
		this.printerSelect = printerSelect;
		this.printInfo = printInfo;
		this.printDocType = printDocType;
	}

	public RenderAndPrintJob(String docName, PrintOption printOption,
			PrinterSelect printerSelect, Map<String, Object> parameters,
			List<?> objectList, String printInfo) {
		this(docName, printOption, printerSelect, parameters, objectList, printInfo, null);
	}

	public RenderAndPrintJob(String docName, PrintOption printOption,
			PrinterSelect printerSelect,
			List<?> objectList, String printInfo, String printDocType) {
		this(docName, printOption, printerSelect, null, objectList, printInfo, printDocType);
	}

	public RenderAndPrintJob(String docName, PrintOption printOption,
			PrinterSelect printerSelect,
			List<?> objectList, String printInfo) {
		this(docName, printOption, printerSelect, null, objectList, printInfo, null);
	}	

	public RenderAndPrintJob(String docName, PrintOption printOption,
			PrinterSelect printerSelect, Map<String, Object> parameters,
			List<?> objectList) {
		this(docName, printOption, printerSelect, parameters, objectList, null, null);
	}

	public RenderAndPrintJob(String docName, PrintOption printOption,
			PrinterSelect printerSelect,
			List<?> objectList) {
		this(docName, printOption, printerSelect, null, objectList, null, null);
	}

	public PrinterSelect getPrinterSelect() {
		return printerSelect;
	}

	public void setPrinterSelect(PrinterSelect printerSelect) {
		this.printerSelect = printerSelect;
	}

	public String getPrintInfo() {
		return printInfo;
	}

	public void setPrintInfo(String printInfo) {
		this.printInfo = printInfo;
	}

	public String getPrintDocType() {
		return printDocType;
	}

	public void setPrintDocType(String printDocType) {
		this.printDocType = printDocType;
	}
}
