package hk.org.ha.model.pms.udt.pivas;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PivasDueOrderListSortColumn implements StringValuedEnum {

	DueDate("1", "Due Date"),
	PreparationDtlDueDate("2", "Preparation Details & Due Date"),
	PreparationDtlPhsWardBedNumPatName("3", "Preparation Details, PHS Ward, Bed & Patient Name"),
	PatNamePreparationDtl("4", "Patient Name & Preparation Details"),
	PhsWardBedNumPatNamePreparationDtl("5", "PHS Ward, Bed, Patient Name & Preparation Details");
	
    private final String dataValue;
    private final String displayValue;

    PivasDueOrderListSortColumn(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    } 

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static PivasDueOrderListSortColumn dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PivasDueOrderListSortColumn.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<PivasDueOrderListSortColumn> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<PivasDueOrderListSortColumn> getEnumClass() {
    		return PivasDueOrderListSortColumn.class;
    	}
    }
}



