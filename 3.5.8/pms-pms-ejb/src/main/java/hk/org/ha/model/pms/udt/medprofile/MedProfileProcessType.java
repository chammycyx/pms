package hk.org.ha.model.pms.udt.medprofile;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum MedProfileProcessType implements StringValuedEnum {
	

	Vet("vet", "Vet"),
	Verify("verify", "Verify"),
	Check("check", "Check");
	
	
	private final String dataValue;
	private final String displayValue;
	
	private MedProfileProcessType(String dataValue,
			String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}

	
	 public static MedProfileProcessType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MedProfileProcessType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<MedProfileProcessType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<MedProfileProcessType> getEnumClass() {
    		return MedProfileProcessType.class;
    	}
    }

	
	
}
