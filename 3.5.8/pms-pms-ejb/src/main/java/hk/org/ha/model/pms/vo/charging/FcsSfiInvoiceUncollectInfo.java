package hk.org.ha.model.pms.vo.charging;

import java.io.Serializable;

import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.Invoice;

public class FcsSfiInvoiceUncollectInfo extends FcsSfiInvoiceInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	// confirm uncollect
	String uncollectUser;

	public FcsSfiInvoiceUncollectInfo() {
		// default constructor
	}
	
	public FcsSfiInvoiceUncollectInfo(Invoice invoice) {
		super(invoice);
		
		DispOrder dispOrder = invoice.getDispOrder();
		uncollectUser = dispOrder.getUncollectUser();
	}
	
	public String getUncollectUser() {
		return uncollectUser;
	}

	public void setUncollectUser(String uncollectUser) {
		this.uncollectUser = uncollectUser;
	}
}
