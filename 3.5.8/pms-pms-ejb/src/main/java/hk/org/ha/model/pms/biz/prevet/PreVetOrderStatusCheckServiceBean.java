package hk.org.ha.model.pms.biz.prevet;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.onestop.OneStopOrderStatusManagerLocal;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.udt.prevet.PreVetMode;

@Stateful
@Scope(ScopeType.SESSION)
@Name("preVetOrderStatusCheckService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PreVetOrderStatusCheckServiceBean implements PreVetOrderStatusCheckServiceLocal {
	
	@In
	private OneStopOrderStatusManagerLocal oneStopOrderStatusManager;
	    
	public String checkOrderStatus(MedOrder currentMedOrder, PreVetMode preVetMode)
    {    	
    	String orderStatusMsg = oneStopOrderStatusManager.verifyMedOrderStatusFromPreVetOperation(currentMedOrder);
		//special message handling for CMS cancel in pharmacy inbox
		if (orderStatusMsg == "0604" && preVetMode == PreVetMode.Inbox)
		{
			return "0690";
		}
    	
    	return orderStatusMsg;
    }
	
	@Remove
	@Override
	public void destroy() {
		
	}
}
