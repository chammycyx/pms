package hk.org.ha.model.pms.vo.cddh;

import hk.org.ha.model.pms.udt.vetting.RegimenType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class PatientCddhPharmItem implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@XmlElement
	private Long pharmOrderItemId;	

	@XmlElement
	private String itemCode;
	
	@XmlElement
	private String orderNum;

	@XmlElement
	private String caseNum;
	
	@XmlElement
	private Long medOrderId;	
	
	@XmlElement
	private Long pharmOrderId;	
	
	@XmlElement
	private String fmStatus;
	
	@XmlElement
	private RegimenType regimenType;

	@XmlElement(name="patientCddhDoseGroup")
	private List<PatientCddhDoseGroup> patientCddhDoseGroupList;

	@XmlElement(name="patientCddhDispItem")
	private List<PatientCddhDispItem> patientCddhDispItemList;
	
	public PatientCddhPharmItem() {
	}

	public Long getPharmOrderItemId() {
		return pharmOrderItemId;
	}

	public void setPharmOrderItemId(Long pharmOrderItemId) {
		this.pharmOrderItemId = pharmOrderItemId;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public Long getMedOrderId() {
		return medOrderId;
	}

	public void setMedOrderId(Long medOrderId) {
		this.medOrderId = medOrderId;
	}

	public Long getPharmOrderId() {
		return pharmOrderId;
	}

	public void setPharmOrderId(Long pharmOrderId) {
		this.pharmOrderId = pharmOrderId;
	}

	public String getFmStatus() {
		return fmStatus;
	}

	public void setFmStatus(String fmStatus) {
		this.fmStatus = fmStatus;
	}

	public RegimenType getRegimenType() {
		return regimenType;
	}

	public void setRegimenType(RegimenType regimenType) {
		this.regimenType = regimenType;
	}

	public List<PatientCddhDoseGroup> getPatientCddhDoseGroupList() {
		if (patientCddhDoseGroupList == null) {
			patientCddhDoseGroupList = new ArrayList<PatientCddhDoseGroup>();
		}
		return patientCddhDoseGroupList;
	}

	public void setPatientCddhDoseGroupList(List<PatientCddhDoseGroup> patientCddhDoseGroupList) {
		this.patientCddhDoseGroupList = patientCddhDoseGroupList;
	}

	public List<PatientCddhDispItem> getPatientCddhDispItemList() {
		if (patientCddhDispItemList == null) {
			patientCddhDispItemList = new ArrayList<PatientCddhDispItem>();
		}
		return patientCddhDispItemList;
	}

	public void setPatientCddhDispItemList(List<PatientCddhDispItem> patientCddhDispItemList) {
		this.patientCddhDispItemList = patientCddhDispItemList;
	}
	
	public void addPatientCddhDoseGroup(PatientCddhDoseGroup patientCddhDoseGroup) {
		getPatientCddhDoseGroupList().add(patientCddhDoseGroup);
	}

	public void addPatientCddhDispItem(PatientCddhDispItem patientCddhDispItem) {
		getPatientCddhDispItemList().add(patientCddhDispItem);
	}
}
