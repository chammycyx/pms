package hk.org.ha.model.pms.biz.assembling;

import static hk.org.ha.model.pms.prop.Prop.BATCH_DAYEND_ORDER_DURATION;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.assembling.AssembleOrder;

import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.joda.time.DateTime;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("assemblingListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class AssemblingListServiceBean implements AssemblingListServiceLocal {

	private static final String ASSEMBLE_ORDER_CLASS_NAME = AssembleOrder.class.getName();
	
	@PersistenceContext
	private EntityManager em;

	@In
	private Workstore workstore; 

	@SuppressWarnings("unchecked")
	public List<AssembleOrder> retrieveAssembleOrderList() {		
		return em.createQuery(
				"select new " + ASSEMBLE_ORDER_CLASS_NAME + // 20120214 index check : Ticket.workstore,orderType,ticketDate : UI_TICKET_01
				"  (o.ticketDate, o.ticketNum, o.urgentFlag, o.pharmOrder.medOrder.docType)" +
				" from DispOrder o" +
				" where o.workstore = :workstore" +
				" and o.orderType = :orderType" +
				" and o.ticketDate between :startDate and :endDate" +
				" and o.status = :status" +
				" and o.adminStatus = :dispOrderAdminStatus" +
				" order by" +
				" o.urgentFlag desc," +
				" o.ticketDate desc," +
				" o.ticketNum desc")
				.setParameter("workstore", workstore)
				.setParameter("orderType", OrderType.OutPatient)
				.setParameter("startDate", new DateTime().minusDays(BATCH_DAYEND_ORDER_DURATION.get()).toDate())
				.setParameter("endDate", new Date())
				.setParameter("status", DispOrderStatus.Assembled)
				.setParameter("dispOrderAdminStatus", DispOrderAdminStatus.Normal)
				.getResultList();
	}
		
	@SuppressWarnings("unchecked")
	public List<Integer> retrieveAvaliableItemNumList(Date ticketDate, String ticketNum) {		
		return em.createQuery(
				"select o.itemNum from DispOrderItem o" + // 20160615 index check : DispOrder.workstore,orderType,ticketDate,ticketNum : I_DISP_ORDER_15
				" where o.dispOrder.workstore = :workstore" +
				" and o.dispOrder.orderType = :orderType" +
				" and o.dispOrder.ticketDate = :ticketDate" +
				" and o.dispOrder.ticketNum = :ticketNum" +
				" and o.dispOrder.status not in :dispOrderStatus" +
				" and o.status = :status")
				.setParameter("workstore", workstore)
				.setParameter("orderType", OrderType.OutPatient)
				.setParameter("ticketDate", ticketDate, TemporalType.DATE)
				.setParameter("ticketNum", ticketNum)
				.setParameter("dispOrderStatus", DispOrderStatus.Deleted_SysDeleted)
				.setParameter("status", DispOrderItemStatus.Picked)
				.getResultList();
	}
	
	@Remove
	public void destroy() 
	{
	}
}
