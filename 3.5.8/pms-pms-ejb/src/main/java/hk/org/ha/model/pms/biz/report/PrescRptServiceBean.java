package hk.org.ha.model.pms.biz.report;

import static hk.org.ha.model.pms.prop.Prop.ORDER_ALLOWUPDATE_DURATION;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.biz.reftable.RefTableManagerLocal;
import hk.org.ha.model.pms.biz.validation.SearchTextValidatorLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.report.PrescRptStatus;
import hk.org.ha.model.pms.udt.validation.SearchTextType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.label.DispLabel;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.PrescRpt;
import hk.org.ha.model.pms.vo.report.PrescRptDtl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;
import org.joda.time.DateMidnight;

@Stateful
@Scope(ScopeType.SESSION)
@Name("prescRptService")
@Restrict("#{identity.loggedIn}") 
@RemoteDestination
@MeasureCalls
public class PrescRptServiceBean implements PrescRptServiceLocal {
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent; 
	
	@In
	private Workstore workstore;
	
	@In
	private RefTableManagerLocal refTableManager;

	private List<PrescRpt> prescRptList;
	
	private boolean retrieveSuccess;
	
	private boolean validHkid;
	
	private boolean isFilterByItemCode;
	
	private Integer totalDispQty;
	
	@In
	private SearchTextValidatorLocal searchTextValidator;
	
	public List<PrescRpt> retrievePrescRptList(Date rptDate, String ticketNum, String caseNum, String hkid, String itemCode, String nameTxt) {
		if ( StringUtils.isNotBlank(ticketNum) ) {
			logger.info("retrievePrescRptList rptDate=#0, ticketNum=#1", rptDate, ticketNum);
		} else if ( StringUtils.isNotBlank(caseNum) ) {
			logger.info("retrievePrescRptList rptDate=#0, caseNum=#1", rptDate, caseNum);
		} else if ( StringUtils.isNotBlank(hkid) ) {
			logger.info("retrievePrescRptList by HKID, rptDate=#0", rptDate);
		} else if ( StringUtils.isNotBlank(itemCode) ) {
			logger.info("retrievePrescRptList rptDate=#0, itemCode=#1", rptDate, itemCode);
		} else if ( StringUtils.isNotBlank(nameTxt) ) {
			logger.info("retrievePrescRptList rptDate=#0, nameTxt=#1", rptDate, nameTxt);
		}
		
		List<DispOrderItem> dispOrderItemList = retrieveDispOrderList(rptDate, ticketNum, caseNum, hkid, itemCode, nameTxt);
		
		List<PrescRpt> prescRptList = genPrescRptList(dispOrderItemList);

		if ( !prescRptList.isEmpty() ) {
			retrieveSuccess = true;
		}
		else{
			retrieveSuccess = false;
		}
		
		validHkid = false;
		if( StringUtils.isBlank(hkid) || searchTextValidator.checkSearchInputType(hkid).equals(SearchTextType.Hkid)){ 
			validHkid = true;
		}
		
		return prescRptList;

	}
	
	@SuppressWarnings("unchecked")
	private List<DispOrderItem> retrieveDispOrderList(Date rptDate, String ticketNum, String caseNum, String hkid, String itemCode, String nameTxt) {
		isFilterByItemCode = false;
		Date createDateStart = new DateMidnight(rptDate).toDateTime().toDate();
		Date createDateEnd = new DateMidnight(rptDate).plusDays(ORDER_ALLOWUPDATE_DURATION.get()).plusDays(1).toDateTime().minus(1).toDate();
		
		StringBuilder sb = new StringBuilder(
				"select o from DispOrderItem o" + // 20120306 index check : Ticket.workstore,orderType,ticketDate,ticketNum : UI_TICKET_01
				" where o.dispOrder.ticket.workstore = :workstore" +
				" and o.dispOrder.ticket.orderType = :orderType" + 
				" and o.dispOrder.ticket.ticketDate = :rptDate" +
				" and (o.dispOrder.status not in :status" +
				" or (o.dispOrder.status =:statusDelete" +
				" or (o.dispOrder.status =:statusSysDelete and o.status in :doiStatus ) ) )" +
				" and o.createDate between :createDateStart and :createDateEnd" +
				" and o.dispOrder.createDate between :createDateStart and :createDateEnd"); 
				
		
		if ( StringUtils.isNotBlank(ticketNum) ) {
			sb.append(" and o.dispOrder.ticket.ticketNum = :ticketNum");
		} else if ( StringUtils.isNotBlank(caseNum) ) {
			sb.append(" and o.dispOrder.pharmOrder.medCase.caseNum = :caseNum");
		} else if ( StringUtils.isNotBlank(hkid) ) {
			sb.append(" and o.dispOrder.pharmOrder.hkid = :hkid");
		} else if ( StringUtils.isNotBlank(itemCode) ) {
			isFilterByItemCode = true;
			sb.append(" and o.pharmOrderItem.itemCode = :itemCode");
		} else if ( StringUtils.isNotBlank(nameTxt) ) {
			sb.append(" and o.dispOrder.pharmOrder.name = :nameTxt");
		}
		
		sb.append(" order by o.dispOrder.ticket.ticketNum, o.pharmOrderItem.medOrderItem.itemNum ,o.pharmOrderItem.itemCode");
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("workstore", workstore);
		query.setParameter("orderType", OrderType.OutPatient);
		query.setParameter("rptDate", rptDate);
		query.setParameter("status", DispOrderStatus.Deleted_SysDeleted);
		query.setParameter("statusDelete", DispOrderStatus.Deleted);
		query.setParameter("statusSysDelete", DispOrderStatus.SysDeleted);
		query.setParameter("doiStatus", DispOrderItemStatus.KeepRecord_Deleted);
		query.setParameter("createDateStart", createDateStart);
		query.setParameter("createDateEnd", createDateEnd);
		
		if ( StringUtils.isNotBlank(ticketNum) ) {
			query.setParameter("ticketNum", ticketNum);
		} else if ( StringUtils.isNotBlank(caseNum) ) {
			query.setParameter("caseNum", caseNum);
		} else if ( StringUtils.isNotBlank(hkid) ) {
			query.setParameter("hkid", hkid);
		} else if ( StringUtils.isNotBlank(itemCode) ) {
			query.setParameter("itemCode", itemCode);
		} else if ( StringUtils.isNotBlank(nameTxt) ) {
			query.setParameter("nameTxt", nameTxt);
		}	
		
		List<DispOrderItem> doiList = query.getResultList();

		List<DispOrderItem> dispOrderList = removeDuplicateVoucher(doiList);
		
		return dispOrderList;
	}
	
	private List<PrescRpt> genPrescRptList(List<DispOrderItem> dispOrderItemList) {
		prescRptList = new ArrayList<PrescRpt>();
		String preTicketNum = StringUtils.EMPTY;
		PrescRpt prescRpt = new PrescRpt();
		totalDispQty = 0;
		
		for ( DispOrderItem dispOrderItem : dispOrderItemList ) {
			if ( !StringUtils.trimToEmpty(preTicketNum).equals(StringUtils.trimToEmpty(dispOrderItem.getDispOrder().getTicket().getTicketNum())) ) {
				prescRpt = new PrescRpt();
				
				if ( dispOrderItem.getDispOrder().getPharmOrder().getMedCase() != null ) {
					prescRpt.setCaseNum(dispOrderItem.getDispOrder().getPharmOrder().getMedCase().getCaseNum());
				}
				prescRpt.setHkid(dispOrderItem.getDispOrder().getPharmOrder().getPatient().getHkid());
				prescRpt.setPatName(dispOrderItem.getDispOrder().getPharmOrder().getPatient().getName());
				prescRpt.setPatNameChi(dispOrderItem.getDispOrder().getPharmOrder().getPatient().getNameChi());
				prescRpt.setOrderType(dispOrderItem.getDispOrder().getPharmOrder().getMedOrder().getOrderType().getDataValue());
				if ( dispOrderItem.getDispOrder().getPharmOrder().getMedOrder().getDocType() == MedOrderDocType.Normal ) {
					prescRpt.setRefNum(dispOrderItem.getDispOrder().getPharmOrder().getMedOrder().getRefNum());
				}
				prescRpt.setTicketNum(dispOrderItem.getDispOrder().getTicket().getTicketNum());
				prescRpt.setTicketDate(dispOrderItem.getDispOrder().getTicket().getTicketDate());
				
				prescRpt.setPrescRptDtlList(new ArrayList<PrescRptDtl>());
				prescRptList.add(prescRpt);
				
				preTicketNum = dispOrderItem.getDispOrder().getTicket().getTicketNum();
			}
			
			PrescRptDtl prescRptDtl = new PrescRptDtl();
			prescRptDtl.setDispQty(dispOrderItem.getDispQty().intValue());
			prescRptDtl.setDoctorCode(dispOrderItem.getDispOrder().getPharmOrder().getDoctorCode());
			prescRptDtl.setDurationInDay(dispOrderItem.getDurationInDay());
			prescRptDtl.setItemCode(dispOrderItem.getPharmOrderItem().getItemCode());
			prescRptDtl.setItemNum(dispOrderItem.getPharmOrderItem().getItemNum());
			
			if ( dispOrderItem.getBaseLabel() != null ) {
				DispLabel dispLabel = (DispLabel)dispOrderItem.getBaseLabel();
				dispLabel.setPrintOption(new PrintOption());
				prescRptDtl.setLabelDesc(dispOrderItem.getBaseLabel().getItemDesc());
				prescRptDtl.setLabelInstructionText(dispOrderItem.getBaseLabel().getInstructionText());
			} 					
			
			
			prescRptDtl.setStatus(this.getStatus(dispOrderItem));
			if ( prescRptDtl.getStatus().equals(PrescRptStatus.Issued.getDisplayValue()) ) {
				totalDispQty = totalDispQty + dispOrderItem.getDispQty().intValue();
			}

			prescRpt.getPrescRptDtlList().add(prescRptDtl);
		}

		return prescRptList;
	}
	
	private String getStatus(DispOrderItem dispOrderItem){
		String status = "";
		
		if (dispOrderItem.getDispOrder().getStatus() == DispOrderStatus.SysDeleted || dispOrderItem.getDispOrder().getStatus() == DispOrderStatus.Deleted) {
			if (dispOrderItem.getPharmOrderItem().getMedOrderItem().getCapdRxDrug() != null && dispOrderItem.getPharmOrderItem().getCapdVoucherFlag()) {
				status = PrescRptStatus.DelCapd.getDisplayValue();
			}else {
				status = PrescRptStatus.Deleted.getDisplayValue();
			}
		} else if ( !dispOrderItem.getDispOrder().getAdminStatus().equals(DispOrderAdminStatus.Normal) ) {//Suspended
			status = PrescRptStatus.Suspended.getDisplayValue();
		} else if ( dispOrderItem.getPharmOrderItem().getMedOrderItem().getCapdRxDrug() != null && dispOrderItem.getPharmOrderItem().getCapdVoucherFlag() ){
			status = PrescRptStatus.Capd.getDisplayValue();
		} else {
			status = dispOrderItem.getStatus().getDisplayValue();
			switch (dispOrderItem.getStatus()) {
				case Vetted:
					status = PrescRptStatus.Vetted.getDisplayValue();
					break;
				case Picked:
					status = PrescRptStatus.Picked.getDisplayValue();
					break;
				case Assembled:
					status = PrescRptStatus.Assembled.getDisplayValue();
					break;
				case Checked:
					status = PrescRptStatus.Checked.getDisplayValue();
					break;
				case Issued:
					status = PrescRptStatus.Issued.getDisplayValue();
					break;
				case KeepRecord:
					status = "No Label";
					break;
			}
		}
		
		return status;
	}
	
	//remove duplicate: (dispOrderItem status = K and dispOrder status = X) / (dispOrderItem status = K and dispOrder status = I)
	private List<DispOrderItem> removeDuplicateVoucher(List<DispOrderItem> dispOrderItemList) { 
		List<DispOrderItem> delDispOrderItemList = new ArrayList<DispOrderItem>();
		Map<String, DispOrderItem> dispOrderItemMap = new HashMap<String, DispOrderItem>();
		
		
		for ( DispOrderItem doi : dispOrderItemList ) {
				if (dispOrderItemMap.get(doi.getDispOrder().getTicket().getTicketDate() + doi.getDispOrder().getTicket().getTicketNum() + doi.getItemNum()) !=null) {
					DispOrderItem item = dispOrderItemMap.get(doi.getDispOrder().getTicket().getTicketDate() + doi.getDispOrder().getTicket().getTicketNum() + doi.getItemNum());
					
					if (  item.getDispOrder().getCreateDate().compareTo(doi.getDispOrder().getCreateDate()) < 0 ) { 
						dispOrderItemMap.put(doi.getDispOrder().getTicket().getTicketDate() + doi.getDispOrder().getTicket().getTicketNum() + doi.getItemNum(), doi);
						delDispOrderItemList.add(item);
					} else {
						delDispOrderItemList.add(doi);
					}
					
				}else {
					dispOrderItemMap.put(doi.getDispOrder().getTicket().getTicketDate() + doi.getDispOrder().getTicket().getTicketNum() + doi.getItemNum(), doi);
				}
				
			}
		
		for ( DispOrderItem delItem : delDispOrderItemList ) {
			dispOrderItemList.remove(delItem);
		}
		
		return dispOrderItemList;
	}
	
	private Map<String, Object> parameters = new HashMap<String, Object>();
	public void generatePrescRpt() {
				
		OperationProfile activeProfile = refTableManager.retrieveActiveProfile(workstore);

		parameters.put("hospCode", workstore.getHospCode());		
		parameters.put("workstoreCode", workstore.getWorkstoreCode());
		parameters.put("activeProfileName", activeProfile.getName());
		
		if (isFilterByItemCode) {
			parameters.put("totalDispQty", totalDispQty.toString());
		}else {
			parameters.put("totalDispQty", "None");
		}
		
		printAgent.renderAndRedirect(new RenderJob(
				"PrescRpt", 
				new PrintOption(), 
				parameters, 
				prescRptList));

	}
	
	public void printPrescRpt(){
		printAgent.renderAndPrint(new RenderAndPrintJob(
				"PrescRpt", 
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				parameters, 
				prescRptList));
		
	}
	
	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}
	
	public boolean isValidHkid() {
		return validHkid;
	}

	@Remove
	public void destroy() {
		if(prescRptList!=null){
			prescRptList=null;
		}
		
	}

}
