package hk.org.ha.model.pms.biz.drug;

import javax.ejb.Local;

@Local
public interface DmSiteServiceLocal {
	
	void retrieveDmSite(String siteCode);
	
	boolean isRetrieveSuccess();
	
	void destroy();
	
}
 