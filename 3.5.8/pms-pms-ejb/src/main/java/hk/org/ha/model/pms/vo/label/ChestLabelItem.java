package hk.org.ha.model.pms.vo.label;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ChestLabelItem")
@ExternalizedBean(type=DefaultExternalizer.class)
public class ChestLabelItem implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@XmlElement(required = true)
	private String itemDesc;

	@XmlElement(required = true)
	private String doseQty;
	
	@XmlElement(required = true)
	private String doseUnit;
	
	@XmlElement(required = true)
	private String itemCode;

	public String getItemDesc() {
		return itemDesc;
	}

	public void setItemDesc(String itemDesc) {
		if (itemDesc.length() > 44) {
			this.itemDesc = itemDesc.substring(0,43);
		} else {
			this.itemDesc = itemDesc;	
		}
	}

	public String getDoseQty() {
		return doseQty;
	}

	public void setDoseQty(String doseQty) {
		this.doseQty = doseQty;
	}

	public String getDoseUnit() {
		return doseUnit;
	}

	public void setDoseUnit(String doseUnit) {
		this.doseUnit = doseUnit;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;	
	}

}

