package hk.org.ha.model.pms.vo.reftable.mp;

import hk.org.ha.model.pms.dms.persistence.PmsIpQtyConv;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class MpPmsQtyConversionItem implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String workstoreCode;
	
	private String hospCode;
	
	private boolean sameWorkstoreGroup; 
	
	private List<PmsIpQtyConv> pmsIpQtyConvList;

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setPmsIpQtyConvList(List<PmsIpQtyConv> pmsIpQtyConvList) {
		this.pmsIpQtyConvList = pmsIpQtyConvList;
	}

	public List<PmsIpQtyConv> getPmsIpQtyConvList() {
		if ( pmsIpQtyConvList == null ) {
			pmsIpQtyConvList = new ArrayList<PmsIpQtyConv>();
		}
		return pmsIpQtyConvList;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setSameWorkstoreGroup(boolean sameWorkstoreGroup) {
		this.sameWorkstoreGroup = sameWorkstoreGroup;
	}

	public boolean isSameWorkstoreGroup() {
		return sameWorkstoreGroup;
	}
	
}