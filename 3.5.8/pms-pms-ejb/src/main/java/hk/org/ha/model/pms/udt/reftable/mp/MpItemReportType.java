package hk.org.ha.model.pms.udt.reftable.mp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum MpItemReportType implements StringValuedEnum{

	ItemPropList("I", "Item Properties List"),
	SpecialControlList("S", "Special Control Item List"),
	MpFdnMappingList("F", "Floating Drug Name List");

	private final String dataValue;
	private final String displayValue;
	
	MpItemReportType(final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}
	
	public static MpItemReportType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MpItemReportType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<MpItemReportType> {

		private static final long serialVersionUID = 1L;

		public Class<MpItemReportType> getEnumClass() {
			return MpItemReportType.class;
		}

	}
}
