package hk.org.ha.model.pms.biz.drug;

import java.math.BigDecimal;
import java.util.List;

import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.vo.rx.PharmDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;

import javax.ejb.Local;

@Local
public interface ItemConvertManagerLocal {

	void convertMedOrder(PharmOrder pharmOrder);
	
	List<PharmOrderItem> convertMedOrderItem(PharmOrder pharmOrder, MedOrderItem medOrderItem);
	
	List<MedOrderItem> convertChest(List<PharmOrderItem> poiList);
	
	PharmDrug recalPharmOrderItem(PharmOrderItem inPharmOrderItem);
	
	List<BigDecimal> calSubTotalDosage(RxItem rxItem, Integer doseGroupIndex, Integer doseIndex);

	List<String> formatDiscontinueMessage(List<MedOrderItem> medOrderItemList);
}
