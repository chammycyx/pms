package hk.org.ha.model.pms.persistence.reftable;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.reftable.FdnType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "FDN_MAPPING")
@Customizer(AuditCustomizer.class)
public class FdnMapping extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fdnMappingSeq")
	@SequenceGenerator(name = "fdnMappingSeq", sequenceName = "SQ_FDN_MAPPING", initialValue = 100000000)
	private Long id;

	@Converter(name = "FdnMapping.orderType", converterClass = OrderType.Converter.class)
	@Convert("FdnMapping.orderType")
	@Column(name = "ORDER_TYPE", nullable = false, length = 1)
	private OrderType orderType;

	@Column(name = "ITEM_CODE", nullable = false, length = 6)
	private String itemCode;

	@Converter(name = "FdnMapping.type", converterClass = FdnType.Converter.class)
	@Convert("FdnMapping.type")
	@Column(name = "TYPE", nullable = false, length = 3)
	private FdnType type;

	@Column(name = "FLOAT_DRUG_NAME", length = 46)
	private String floatDrugName;

	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE", referencedColumnName = "HOSP_CODE", nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public FdnType getType() {
		return type;
	}

	public void setType(FdnType type) {
		this.type = type;
	}

	public String getFloatDrugName() {
		return floatDrugName;
	}

	public void setFloatDrugName(String floatDrugName) {
		this.floatDrugName = floatDrugName;
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}
	
	public OrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}

	// helper methods
    public boolean isNew() {
    	return this.getId() == null;
    }
}
