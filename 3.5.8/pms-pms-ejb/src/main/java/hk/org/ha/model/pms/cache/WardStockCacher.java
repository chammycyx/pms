package hk.org.ha.model.pms.cache;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.phs.Ward;
import hk.org.ha.model.pms.persistence.phs.WardPK;
import hk.org.ha.model.pms.persistence.phs.WardStock;
import hk.org.ha.model.pms.persistence.phs.WardStockPK;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("wardStockCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class WardStockCacher extends BaseCacher implements WardStockCacherInf {

	@In
	public CorpPmsServiceJmsRemote corpPmsServiceProxy;

	private Map<WardPK, InnerCacher> cacherMap = new HashMap<WardPK, InnerCacher>();
		
	public WardStock getWardStock(Ward ward, String itemCode) {
		return this.getCacher(ward).get(
				new WardStockPK(
						ward.getInstCode(), 
						ward.getWardCode(),
						itemCode));
	}

	public List<WardStock> getWardStockList(Ward ward) {		
		return (List<WardStock>) this.getCacher(ward).getAll();
	}
	
	public void clear() { 
		synchronized (this) {
			cacherMap.clear();
		}
	}
	
	private InnerCacher getCacher(Ward ward) {
		synchronized (this) {
			WardPK key = ward.getId();			
			InnerCacher cacher = cacherMap.get(key);
			if (cacher == null) {
				cacher = new InnerCacher(ward, this.getExpireTime());
				cacherMap.put(key, cacher);
			}
			return cacher;
		}
	}

	private class InnerCacher extends AbstractCacher<WardStockPK, WardStock> {
		private Ward ward = null;

		public InnerCacher(Ward ward, int expireTime) {
			super(expireTime);
			this.ward = ward;
		}

		public WardStock create(WardStockPK key) {
			throw new UnsupportedOperationException();
		}
		
		public Collection<WardStock> createAll() {
			return corpPmsServiceProxy.retrieveWardStockList(ward);
		}

		public WardStockPK retrieveKey(WardStock wardStock) {
			return wardStock.getId();
		}
	}

	public static WardStockCacher instance()
	{
		  if (!Contexts.isApplicationContextActive())
	      {
	         throw new IllegalStateException("No active application scope");
	      }
	      return (WardStockCacher) Component.getInstance("wardStockCacher", ScopeType.APPLICATION);
    }


}
