package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.ItemLocation;
import hk.org.ha.model.pms.persistence.reftable.PickStation;
import hk.org.ha.model.pms.udt.reftable.MachineType;
import hk.org.ha.model.pms.vo.drug.DmDrugLite;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.model.pms.vo.reftable.ItemLocationInfo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("itemLocationListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ItemLocationListServiceBean implements ItemLocationListServiceLocal {

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<PickStation> itemLocationPickStationList;
	
	@Out(required = false)
	private DmDrugLite itemLocationDmDrugLite;

	@In
	private Workstore workstore;

	@In
	private ItemLocationListManagerLocal itemLocationListManager;
	
	@In
	private PickStationListManagerLocal pickStationListManager;
	
	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;
	
	private boolean saveSuccess;
	
	private String binNum;
	
	public void retrieveDmDrugLite(String itemCode) {
		itemLocationDmDrugLite = null;
		WorkstoreDrug workstoreDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, itemCode);
		if ( workstoreDrug != null && workstoreDrug.getMsWorkstoreDrug() != null && workstoreDrug.getDmDrug() != null) {
			itemLocationDmDrugLite = workstoreDrug.getDmDrugLite();
		}
	}
	
	public void retrievePickStationList() 
	{	
		itemLocationPickStationList = pickStationListManager.retrievePickStationList();
	}
	
	public List<ItemLocationInfo> retrieveItemLocationListByItemCode(String itemCode)
	{
		return itemLocationListManager.retrieveItemLocationListByItemCode(itemCode);
	}
	
	public List<ItemLocationInfo> retrieveItemLocationListByPickStation(int pickStationNum) 
	{
		return itemLocationListManager.retrieveItemLocationListByPickStation(pickStationNum);		
	}
	
	@SuppressWarnings("unchecked")
	public void updateItemLocationList(Collection<ItemLocationInfo> updateItemLocationInfoList)
	{
		saveSuccess = true;
		
		workstore = em.find(Workstore.class, workstore.getId());
		List<Long> delItemLocationIDList = new ArrayList<Long>();
		List<ItemLocation> updateItemLocationList = new ArrayList<ItemLocation>();
		
		for (ItemLocationInfo itemLocationInfo:updateItemLocationInfoList) {
			if(itemLocationInfo.getItemLocationId() != null && itemLocationInfo.isMarkDelete()){
				ItemLocation itemLocation = em.find(ItemLocation.class, itemLocationInfo.getItemLocationId());
				delItemLocationIDList.add(itemLocation.getId());
				itemLocation.setMarkDelete(true);
				updateItemLocationList.add(itemLocation);
			} else {
				ItemLocation itemLocation = new ItemLocation();
				itemLocation.setItemCode(itemLocationInfo.getItemCode());
				itemLocation.setBinNum(itemLocationInfo.getBinNum());
				itemLocation.setBakerCellNum(itemLocationInfo.getBakerCellNum());
				itemLocation.setMaxQty(itemLocationInfo.getMaxQty());
				itemLocation.setWorkstore(workstore);
				itemLocation.setMachine(itemLocationInfo.getMachine());
				updateItemLocationList.add(itemLocation);
			}
		}
		
		for (ItemLocation itemLocation:updateItemLocationList) {
			if(itemLocation.getId() == null) {
				List<ItemLocation> checkBinNumItemList = null;
				
				if ( itemLocation.getMachine().getType() == MachineType.ATDPS && "-".equals(itemLocation.getBinNum()) ) {
					continue;
				}
				if(delItemLocationIDList.isEmpty()){
					checkBinNumItemList = em.createQuery(
							"select o from ItemLocation o" + // 20120303 index check : ItemLocation.workstore,binNum : FK_ITEM_LOCATION_03
							" where o.workstore = :workstore" +
							" and o.binNum = :binNum" +
							" and (o.machine.type <> :atdpsType or (o.machine.type = :atdpsType and o.binNum <> '-'))")
							.setParameter("workstore", workstore)
							.setParameter("binNum", itemLocation.getBinNum())
							.setParameter("atdpsType", MachineType.ATDPS)
							.getResultList();
				}else {
					checkBinNumItemList = em.createQuery(
							"select o from ItemLocation o" + // 20120303 index check : ItemLocation.workstore,binNum : FK_ITEM_LOCATION_03
							" where o.workstore = :workstore" +
							" and o.binNum = :binNum" +
							" and o.id not in :delItemLocationIDList" +
							" and (o.machine.type <> :atdpsType or (o.machine.type = :atdpsType and o.binNum <> '-'))")
							.setParameter("workstore", workstore)
							.setParameter("binNum", itemLocation.getBinNum())
							.setParameter("delItemLocationIDList", delItemLocationIDList)
							.setParameter("atdpsType", MachineType.ATDPS)
							.getResultList();
				}

				if(!checkBinNumItemList.isEmpty()){
					saveSuccess = false;
					binNum = itemLocation.getBinNum();
					retrieveItemLocationListByPickStation(0);
					return;
				}
			}
		}
		
		for (ItemLocation itemLocation:updateItemLocationList) {
			if(itemLocation.isMarkDelete() && itemLocation.getId() != null){
				em.remove(em.merge(itemLocation));
			}
		}

		em.flush();
		
		for (ItemLocation itemLocation:updateItemLocationList) {
			if(!itemLocation.isMarkDelete() && itemLocation.getId() == null) {
				em.persist(itemLocation);
			}
		}
		
		em.flush(); 
		em.clear();
		retrieveItemLocationListByPickStation(0);
	}

	public boolean isSaveSuccess(){
		return saveSuccess;
	}
	
	public String getBinNum(){
		return binNum;
	}
	
	@Remove
	public void destroy() 
	{
		if ( itemLocationPickStationList != null ) {
			itemLocationPickStationList = null;
		}
		if ( itemLocationDmDrugLite != null ) {
			itemLocationDmDrugLite = null;
		}
	}
}
