package hk.org.ha.model.pms.vo.label;

import java.io.Serializable;

import hk.org.ha.model.pms.udt.printing.PrintLang;

import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "HkidBarcodeLabel")
@ExternalizedBean(type=DefaultExternalizer.class)
public class HkidBarcodeLabel implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@XmlElement(required = true)	
	private String patName;

	@XmlElement(required = true)	
	private String patNameChi;
	
	@XmlElement(required = true)	
	private String hkid;
	
	@XmlElement(required = true)	
	private PrintLang printLang;
	
	@Transient
	private Integer copies;

	public HkidBarcodeLabel() {
		printLang = PrintLang.Eng;
	}
	
	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getPatNameChi() {
		return patNameChi;
	}

	public void setPatNameChi(String patNameChi) {
		this.patNameChi = patNameChi;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public PrintLang getPrintLang() {
		return printLang;
	}

	public void setPrintLang(PrintLang printLang) {
		this.printLang = printLang;
	}

	public void setCopies(Integer copies) {
		this.copies = copies;
	}

	public Integer getCopies() {
		return copies;
	}
}