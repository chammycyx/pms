package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.biz.reftable.WardFilterManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.report.WardFilterRpt;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("wardFilterRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class WardFilterRptServiceBean implements WardFilterRptServiceLocal {
	
	@In
	private Workstore workstore;
	
	@In
	private WardFilterManagerLocal wardFilterManager;
	@In
	private PrintAgentInf printAgent;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	public void printWardFilterRptList() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("hospCode", workstore.getHospCode());
		List<WardFilterRpt> wardFilterRptList = wardFilterManager.genWardFilterRptList();
		
		printAgent.renderAndPrint(new RenderAndPrintJob(
			    "WardFilterRpt",
			    new PrintOption(),
			    printerSelector.retrievePrinterSelect(PrintDocType.Report),
			    parameters,
			    wardFilterRptList));	
	}

	@Remove
	public void destroy() {

	}
}
