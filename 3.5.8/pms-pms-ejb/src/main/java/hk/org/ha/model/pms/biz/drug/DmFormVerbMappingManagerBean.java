package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.util.StringArray;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmFormVerbMappingCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmFormVerbMapping;
import hk.org.ha.model.pms.vo.security.PropMap;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("dmFormVerbMappingManager")
@MeasureCalls
public class DmFormVerbMappingManagerBean implements DmFormVerbMappingManagerLocal {

	@In
	private DmFormVerbMappingCacherInf dmFormVerbMappingCacher;
	
	private DmFormVerbMappingComparator comparator = new DmFormVerbMappingComparator();
	
	private PropMap constructFormVerbProperties(boolean isMpFlag) {
		List<DmFormVerbMapping> dmFormVerbMappinglist = dmFormVerbMappingCacher.getDmFormVerbMappingList();
		PropMap formVerbMap = new PropMap();
		Integer count;
		StringBuffer key;
		
		for ( DmFormVerbMapping dmFormVerbMapping : dmFormVerbMappinglist ) {	
			if (!isMpFlag && Integer.valueOf(500).compareTo(dmFormVerbMapping.getRank()) <= 0 ) {
				continue;//PMSU-2290
			}
			key = new StringBuffer();
			key.append(dmFormVerbMapping.getDmForm().getFormCode());
			key.append(dmFormVerbMapping.getDmSite().getSiteCode());
			key.append(dmFormVerbMapping.getSupplementarySite()==null?"":dmFormVerbMapping.getSupplementarySite());			
			count = formVerbMap.getValueAsInteger(key.toString());
			if ( count == null ) {
				count = Integer.valueOf(1);				
			} else {
				count = Integer.valueOf(count.intValue()+1);
			}
			formVerbMap.add(key.toString(), count.toString());			
		}
		return formVerbMap;
	}
	
	public PropMap getFormVerbProperties() {				
		return constructFormVerbProperties(false);
	}
	
	public PropMap getMpFormVerbProperties() {				
		return constructFormVerbProperties(true);
	}
	
	public List<DmFormVerbMapping> getDmFormVerbMappingListBySiteCode(String siteCode) {
		Map<StringArray,DmFormVerbMapping> formVerbMappingHashMap = new HashMap<StringArray,DmFormVerbMapping>();
		List<DmFormVerbMapping> dmFormVerbMappingList = new ArrayList<DmFormVerbMapping>();
		List<DmFormVerbMapping> formVerbMappingList = dmFormVerbMappingCacher.getDmFormVerbMappingListBySiteCode(siteCode);		
		for ( DmFormVerbMapping formVerbMapping : formVerbMappingList ) {
			StringArray key = new StringArray(formVerbMapping.getDmForm().getLabelDesc(),											
					formVerbMapping.getDmForm().getDmRoute().getFullRouteDesc(),
					formVerbMapping.getVerbEng(),
					formVerbMapping.getSiteCode(),
					formVerbMapping.getSupplementarySite());
			if ( formVerbMappingHashMap.get(key) == null ) {
				formVerbMappingHashMap.put(key, formVerbMapping);
				dmFormVerbMappingList.add(formVerbMapping);
			}
		}
		Collections.sort(dmFormVerbMappingList,comparator);
		return dmFormVerbMappingList;
	}
	
	public static class DmFormVerbMappingComparator implements Comparator<DmFormVerbMapping>, Serializable {

		private static final long serialVersionUID = 1L;

		public int compare( DmFormVerbMapping m1, DmFormVerbMapping m2 ) {
			return new CompareToBuilder()
			.append( m1.getDmForm().getLabelDesc(), m2.getDmForm().getLabelDesc() )
			.append( m1.getDmForm().getDmRoute().getFullRouteDesc(), m2.getDmForm().getDmRoute().getFullRouteDesc() )
			.append( m1.getVerbEng(), m2.getVerbEng() )
			.append( m1.getSiteCode(), m2.getSiteCode() )
			.append( m1.getSupplementarySite(), m2.getSupplementarySite() )
			.toComparison();
		}
	}
}
