package hk.org.ha.model.pms.biz.enquiry;

import hk.org.ha.fmk.pms.remote.marshall.EclipseLinkXStreamMarshaller;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.moe.DrugSetException;
import hk.org.ha.model.pms.persistence.corp.HospitalMapping;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedOrderFm;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedOrderItemAlert;
import hk.org.ha.model.pms.udt.RecordStatus;
import hk.org.ha.model.pms.vo.enquiry.DrugSet;
import hk.org.ha.model.pms.vo.enquiry.DrugSetDtl;
import hk.org.ha.service.pms.asa.interfaces.moe.MoeServiceJmsRemote;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("drugSetListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DrugSetListServiceBean implements DrugSetListServiceLocal {
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private MoeServiceJmsRemote moeServiceProxy;
	
	@In
	private Workstore workstore;
	
	@Out(required = false)
	private List<HospitalMapping> drugSetHospitalMappingList;
	
	@Out(required = false)
	private List<DrugSet> drugSetList;
	
	@Out(required = false)
	private List<DrugSetDtl> drugSetDtlList;
	
	@Out(required = false)
	private List<String> subLevelList;
	
	@SuppressWarnings("unchecked")
	public void retrieveHospitalMappingList() throws DrugSetException {
		
		drugSetHospitalMappingList = em.createQuery(
				"select o from HospitalMapping o" + // 20140801 index check : HospitalMapping.hospCode : FK_HOSPITAL_MAPPING_01
				" where o.status = :status" +
				" and o.prescribeFlag = :prescribeFlag" +
				" and o.hospCode = :hospCode")
				.setParameter("status", RecordStatus.Active)
				.setParameter("prescribeFlag", Boolean.TRUE)
				.setParameter("hospCode", workstore.getHospCode())
				.getResultList();
	}
	
	public void retrieveDrugSetList(String patHospCode, Boolean getFirstDrugSet) throws DrugSetException {
		drugSetList = moeServiceProxy.retrieveDrugSet(patHospCode);
		
		if ( !drugSetList.isEmpty() && getFirstDrugSet ) {
			DrugSet firstDrugSet = drugSetList.get(0);
			retrieveDrugSetDtlList(patHospCode, firstDrugSet.getDrugSetNum(), firstDrugSet.getDrugSetCode(), firstDrugSet.getDrugHospCode(), firstDrugSet.getSubLevelFlag());
		}
	}
	
	public void retrieveDrugSetDtlList(String patHospCode, Integer drugSetNum, String drugSetCode, String drugHospCode, Boolean subLevelFlag) throws DrugSetException {
		drugSetDtlList = moeServiceProxy.retrieveDrugSetDtl(patHospCode, drugSetNum, drugSetCode, patHospCode);
		
		subLevelList = new ArrayList<String>();
		
		Map<String, Boolean> subLevelMap = new HashMap<String, Boolean>(); 
		
		for (DrugSetDtl drugSetDtl: drugSetDtlList){
			logger.debug(new EclipseLinkXStreamMarshaller().toXML(drugSetDtl));
			
			drugSetDtl.getMedOrderItem().preSave();
			drugSetDtl.getMedOrderItem().setCapdRxDrug(null);
			drugSetDtl.getMedOrderItem().setRxDrug(null);
			drugSetDtl.getMedOrderItem().postLoadAll();

			drugSetDtl.getMedOrderItem().clearPharmOrderItemList();
			drugSetDtl.getMedOrderItem().setMedOrderItemAlertList(new ArrayList<MedOrderItemAlert>());
			
			drugSetDtl.getMedOrderItem().getMedOrder().setMedOrderItemList(new ArrayList<MedOrderItem>());
			drugSetDtl.getMedOrderItem().getMedOrder().setPatient(null);
			drugSetDtl.getMedOrderItem().getMedOrder().setWorkstore(null);
			drugSetDtl.getMedOrderItem().getMedOrder().setMedCase(null);
			drugSetDtl.getMedOrderItem().getMedOrder().setTicket(null);
			drugSetDtl.getMedOrderItem().getMedOrder().setMedOrderFmList(new ArrayList<MedOrderFm>());
			
			if ( subLevelFlag ) {
				String subLevel = StringUtils.trimToEmpty(drugSetDtl.getSubLevel());
				if ( ! StringUtils.isEmpty(subLevel) && ! subLevelMap.containsKey(subLevel)) {
					subLevelList.add(subLevel);
					subLevelMap.put(subLevel, Boolean.TRUE);
				}
			}
		}
	}
	
	@Remove
	public void destroy() 
	{
		if ( drugSetHospitalMappingList != null ) {
			drugSetHospitalMappingList = null;
		}
		if ( drugSetList != null ) {
			drugSetList = null;
		}
		if ( drugSetDtlList != null ) {
			drugSetDtlList = null;
		}
		if ( subLevelList != null ) {
			subLevelList = null;
		}
	}
}
