package hk.org.ha.model.pms.udt.medprofile;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DeliveryScheduleItemWardType implements StringValuedEnum {
	
	AllWard("A", "All Ward"),
	WardGroup("G", "Ward Group"),
	Ward("W", "Multi-Ward");
	
    private final String dataValue;
    private final String displayValue;
	
	private DeliveryScheduleItemWardType(String dataValue, String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}
	
	public static DeliveryScheduleItemWardType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DeliveryScheduleItemWardType.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<DeliveryScheduleItemWardType> {
		
		private static final long serialVersionUID = 1L;

		public Class<DeliveryScheduleItemWardType> getEnumClass() {
			return DeliveryScheduleItemWardType.class;
		}
	}
}
