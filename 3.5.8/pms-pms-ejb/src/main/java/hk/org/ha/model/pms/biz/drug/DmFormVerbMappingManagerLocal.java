package hk.org.ha.model.pms.biz.drug;
import java.util.List;

import hk.org.ha.model.pms.dms.persistence.DmFormVerbMapping;
import hk.org.ha.model.pms.vo.security.PropMap;

import javax.ejb.Local;

@Local
public interface DmFormVerbMappingManagerLocal {

	PropMap getFormVerbProperties();
	PropMap getMpFormVerbProperties();
	List<DmFormVerbMapping> getDmFormVerbMappingListBySiteCode(String siteCode);	
}
