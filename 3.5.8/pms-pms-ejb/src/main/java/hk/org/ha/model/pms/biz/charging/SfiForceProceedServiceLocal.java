package hk.org.ha.model.pms.biz.charging;

import hk.org.ha.model.pms.vo.charging.SfiForceProceedInfo;

import javax.ejb.Local;

@Local
public interface SfiForceProceedServiceLocal {

	void retrieveForceProceedChargeReasonList();
	void validateSfiForceProceedEntry(SfiForceProceedInfo sfiForceProceedEntry);
	void destroy();

}
