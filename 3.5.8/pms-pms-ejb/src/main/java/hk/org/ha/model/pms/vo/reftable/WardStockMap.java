package hk.org.ha.model.pms.vo.reftable;

import hk.org.ha.model.pms.persistence.phs.WardStock;

import java.io.Serializable;
import java.util.Map;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class WardStockMap implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Map<String, WardStock> wardStock;
	
	public void setWardStock(Map<String, WardStock> wardStock) {
		this.wardStock = wardStock;
	}

	public Map<String, WardStock> getWardStock() {
		return wardStock;
	}
}