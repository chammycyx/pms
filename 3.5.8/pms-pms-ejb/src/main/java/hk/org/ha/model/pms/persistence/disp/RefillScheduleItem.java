package hk.org.ha.model.pms.persistence.disp;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.refill.RefillScheduleItemStatus;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlTransient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;


@Entity
@Table(name = "REFILL_SCHEDULE_ITEM")
public class RefillScheduleItem extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "refillScheduleItemSeq")
	@SequenceGenerator(name = "refillScheduleItemSeq", sequenceName = "SQ_REFILL_SCHEDULE_ITEM", initialValue = 100000000)
	private Long id;
	
	@Column(name = "REFILL_DURATION", nullable = false)
	private Integer refillDuration;
	
	@Column(name = "REFILL_END_DATE")
	@Temporal(TemporalType.DATE)
	private Date refillEndDate;
	
	@Column(name = "CAL_QTY", nullable = false)
	private Integer calQty;

	@Column(name = "ADJ_QTY", nullable = false)
	private Integer adjQty;

	@Column(name = "REFILL_QTY", nullable = false)
	private Integer refillQty;

	@Column(name = "HANDLE_EXEMPT_CODE", length = 2)
	private String handleExemptCode;

	@Column(name = "HANDLE_EXEMPT_USER", length = 12)
	private String handleExemptUser;
	
	@Column(name = "NEXT_HANDLE_EXEMPT_CODE", length = 2)
	private String nextHandleExemptCode;
	
	@Column(name = "ITEM_NUM", nullable = false)
	private Integer itemNum;

	@Column(name = "SUPPLY_FOR_DAY")
	private Integer supplyForDay;

	@Column(name = "ACTIVE_FLAG", nullable = false)
	private Boolean activeFlag;
	
	@Column(name = "UNIT_PRICE", precision=19, scale=4)
	private BigDecimal unitPrice;
	
	@Column(name = "NUM_OF_REFILL", nullable = false)
	private Integer numOfRefill;

	@Converter(name = "RefillScheduleItem.status", converterClass = RefillScheduleItemStatus.Converter.class)
	@Convert("RefillScheduleItem.status")
	@Column(name="STATUS", length = 1)
	@XmlTransient
	private RefillScheduleItemStatus status;
	
	@ManyToOne
	@JoinColumn(name = "REFILL_SCHEDULE_ID", nullable = false)
	private RefillSchedule refillSchedule;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "PHARM_ORDER_ITEM_ID", nullable = false)
	private PharmOrderItem pharmOrderItem;

	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE",      referencedColumnName = "HOSP_CODE",      nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;
	
	@Transient
    private boolean allowQtyAdjust;
	
	@Transient
	private Integer refillNum;

	@Transient
	private Integer lateRefillAdjQty;
    
	@Transient
	private Integer lateRefillRemainDur;
	
	@Transient
	private boolean dirtyFlag;
	
	public RefillScheduleItem() {
		setAllowQtyAdjust(false);
		activeFlag = Boolean.TRUE;
		dirtyFlag = Boolean.FALSE;
		setLateRefillAdjQty(0);
		setLateRefillRemainDur(refillDuration);
		numOfRefill = Integer.valueOf(0);
		status = RefillScheduleItemStatus.Normal;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getRefillDuration() {
		return refillDuration;
	}

	public void setRefillDuration(Integer refillDuration) {
		this.refillDuration = refillDuration;
	}

	public Date getRefillEndDate() {
		if (refillEndDate == null) {
			return null;
		}
		else {
			return new Date(refillEndDate.getTime());
		}
	}

	public void setRefillEndDate(Date refillEndDate) {
		if (refillEndDate == null) {
			this.refillEndDate = null;
		}
		else {
			this.refillEndDate = new Date(refillEndDate.getTime());
		}
	}

	public Integer getCalQty() {
		return calQty;
	}

	public void setCalQty(Integer calQty) {
		this.calQty = calQty;
	}

	public Integer getAdjQty() {
		return adjQty;
	}

	public void setAdjQty(Integer adjQty) {
		this.adjQty = adjQty;
	}

	public Integer getRefillQty() {
		return refillQty;
	}

	public void setRefillQty(Integer refillQty) {
		this.refillQty = refillQty;
	}

	public String getHandleExemptCode() {
		return handleExemptCode;
	}

	public void setHandleExemptCode(String handleExemptCode) {
		this.handleExemptCode = handleExemptCode;
	}

	public String getHandleExemptUser() {
		return handleExemptUser;
	}
	
	public void setHandleExemptUser(String handleExemptUser) {
		this.handleExemptUser = handleExemptUser;
	}

	public String getNextHandleExemptCode() {
		return nextHandleExemptCode;
	}

	public void setNextHandleExemptCode(String nextHandleExemptCode) {
		this.nextHandleExemptCode = nextHandleExemptCode;
	}

	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public Integer getSupplyForDay() {
		return supplyForDay;
	}

	public void setSupplyForDay(Integer supplyForDay) {
		this.supplyForDay = supplyForDay;
	}

	public Boolean getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public RefillSchedule getRefillSchedule() {
		return refillSchedule;
	}
	
	public void setRefillSchedule(RefillSchedule refillSchedule) {
		this.refillSchedule = refillSchedule;
	}

	public PharmOrderItem getPharmOrderItem() {
		return pharmOrderItem;
	}

	public void setPharmOrderItem(PharmOrderItem pharmOrderItem) {
		this.pharmOrderItem = pharmOrderItem;
	}

	@Transient
	public boolean isAllowQtyAdjust() {
		return allowQtyAdjust;
	}
	
	@Transient
	public void setAllowQtyAdjust(boolean allowQtyAdjust) {
		this.allowQtyAdjust = allowQtyAdjust;
	}

	@Transient
	public Integer getRefillNum() {
		return refillNum;
	}
	
	@Transient
	public void setRefillNum(Integer refillNum) {
		this.refillNum = refillNum;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}
	
	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}
	
	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}

	public Workstore getWorkstore() {
		return workstore;
	}
	
	public void setNumOfRefill(Integer numOfRefill) {
		this.numOfRefill = numOfRefill;
	}

	public Integer getNumOfRefill() {
		return numOfRefill;
	}

	public RefillScheduleItemStatus getStatus() {
		return status;
	}

	public void setStatus(RefillScheduleItemStatus status) {
		this.status = status;
	}
	
	@Transient
	public void setLateRefillAdjQty(Integer lateRefillAdjQty) {
		this.lateRefillAdjQty = lateRefillAdjQty;
	}
	
	@Transient
	public Integer getLateRefillAdjQty() {
		return lateRefillAdjQty;
	}
	
	@Transient
	public void setLateRefillRemainDur(Integer lateRefillRemainDur) {
		this.lateRefillRemainDur = lateRefillRemainDur;
	}
	
	@Transient
	public Integer getLateRefillRemainDur() {
		return lateRefillRemainDur;
	}

	@Transient
	public void setDirtyFlag(boolean dirtyFlag) {
		this.dirtyFlag = dirtyFlag;
	}

	@Transient
	public boolean isDirtyFlag() {
		return dirtyFlag;
	}
	
}
