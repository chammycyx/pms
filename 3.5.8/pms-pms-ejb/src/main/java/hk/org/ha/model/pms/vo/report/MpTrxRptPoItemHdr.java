package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class MpTrxRptPoItemHdr implements Serializable {

	private static final long serialVersionUID = 1L;

	private String overrideReasonHeader;

	private String overrideReasonDetail;

	public String getOverrideReasonHeader() {
		return overrideReasonHeader;
	}

	public void setOverrideReasonHeader(String overrideReasonHeader) {
		this.overrideReasonHeader = overrideReasonHeader;
	}

	public String getOverrideReasonDetail() {
		return overrideReasonDetail;
	}

	public void setOverrideReasonDetail(String overrideReasonDetail) {
		this.overrideReasonDetail = overrideReasonDetail;
	}
	
	
}
