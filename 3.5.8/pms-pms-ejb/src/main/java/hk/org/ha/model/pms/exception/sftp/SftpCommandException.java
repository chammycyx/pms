package hk.org.ha.model.pms.exception.sftp;

import hk.org.ha.fmk.pms.exception.SerializableException;

public class SftpCommandException extends SerializableException {

	private static final long serialVersionUID = 1L;
	
	private String exceptionClassName;

	public SftpCommandException(Throwable source, String exceptionClassName) {
        super(source);
        this.setExceptionClassName(exceptionClassName);
    }

	public void setExceptionClassName(String exceptionClassName) {
		this.exceptionClassName = exceptionClassName;
	}

	public String getExceptionClassName() {
		return exceptionClassName;
	}    
}
