
package hk.org.ha.model.pms.biz.report;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.InvoiceItem;
import hk.org.ha.model.pms.persistence.reftable.ChargeReason;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.InvoiceDocType;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.SfiHandleChargeExemptionRpt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.joda.time.DateTime;

@Stateful
@Scope(ScopeType.SESSION)
@Name("sfiHandleChargeExemptionRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SfiHandleChargeExemptionRptServiceBean implements SfiHandleChargeExemptionRptServiceLocal {
	@PersistenceContext
	private EntityManager em;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent; 
	
	@In 
	private Workstore workstore;
	
	@Out
	private List<SfiHandleChargeExemptionRpt> sfiHandleChargeExemptionRptList;
	
	private Date reportDate; 
	
	private boolean retrieveSuccess;
	
	@SuppressWarnings("unchecked")
	public List<SfiHandleChargeExemptionRpt> retrieveSfiHandleChargeExemptionRptList(Date rptDate) {
		reportDate = new Date(rptDate.getTime());
		
		sfiHandleChargeExemptionRptList = new ArrayList<SfiHandleChargeExemptionRpt>();

		DateTime dateTo = new DateTime(reportDate);
		dateTo = dateTo.plusMonths(1);
		dateTo = dateTo.minusDays(1);
		
		List<InvoiceItem> invoiceItemList = em.createQuery(
				"select o from InvoiceItem o" + // 20120329 index check : DispOrder.workstore,dispDate : I_DISP_ORDER_03
				" where o.invoice.dispOrder.workstore = :workstore" +
				" and o.invoice.dispOrder.dispDate between :dateFrom and :dateTo" +
				" and o.invoice.docType = :type" +
				" and o.invoice.dispOrder.ticket.orderType = :orderType" +
				" and o.invoice.dispOrder.adminStatus <> :adminStatus" +
				" and o.invoice.dispOrder.status =:status" +
				" and o.invoice.status not in :invoiceStatusList"+
				" order by o.invoice.dispOrder.dispDate")
				.setParameter("workstore", workstore)
				.setParameter("dateFrom", reportDate, TemporalType.DATE)
				.setParameter("dateTo", dateTo.toDate(), TemporalType.DATE)
				.setParameter("type", InvoiceDocType.Sfi)
				.setParameter("orderType", OrderType.OutPatient)
				.setParameter("adminStatus", DispOrderAdminStatus.Suspended)
				.setParameter("status", DispOrderStatus.Issued)
				.setParameter("invoiceStatusList", Arrays.asList(InvoiceStatus.SysDeleted, InvoiceStatus.Void))
				.getResultList();

		Map<String, String> chargeReasonMap = new HashMap<String, String>();
		
		for (InvoiceItem i : invoiceItemList){
			if(i.getHandleExemptCode()!=null || i.getNextHandleExemptCode()!=null){
				SfiHandleChargeExemptionRpt sfiHandleChargeExemptionRpt = new SfiHandleChargeExemptionRpt();
				sfiHandleChargeExemptionRpt.setDispDate(i.getInvoice().getDispOrder().getDispDate());
				sfiHandleChargeExemptionRpt.setTicketNum(i.getInvoice().getDispOrder().getTicket().getTicketNum());

				if (i.getInvoice().getDispOrder().getPharmOrder().getMedCase() != null) {
					sfiHandleChargeExemptionRpt.setCaseNum(i.getInvoice().getDispOrder().getPharmOrder().getMedCase().getCaseNum());
				}
				
				sfiHandleChargeExemptionRpt.setItemCode(i.getDispOrderItem().getPharmOrderItem().getItemCode());
				sfiHandleChargeExemptionRpt.setInvoiceNum(i.getInvoice().getInvoiceNum());
				sfiHandleChargeExemptionRpt.setHandleExemptBy(i.getHandleExemptUser());
				sfiHandleChargeExemptionRpt.setTotalQty(i.getDispOrderItem().getPharmOrderItem().getIssueQty().intValue());
				sfiHandleChargeExemptionRpt.setIssueQty(i.getDispOrderItem().getDispQty().intValue());
				
				if(i.getHandleExemptCode()!=null){
					sfiHandleChargeExemptionRpt.setCurrentExemptCode(i.getHandleExemptCode());
					if(chargeReasonMap.get(i.getHandleExemptCode())==null){
						String reasonDesc = retrieveChargeReasonByChargeCode(i.getHandleExemptCode());
						chargeReasonMap.put(i.getHandleExemptCode(), reasonDesc);
						sfiHandleChargeExemptionRpt.setCurrentExemptDesc(reasonDesc);
					}
					else{
						sfiHandleChargeExemptionRpt.setCurrentExemptDesc(chargeReasonMap.get(i.getHandleExemptCode()));
					}
				}
				
				if(i.getNextHandleExemptCode()!=null){
					sfiHandleChargeExemptionRpt.setNextExemptCode(i.getNextHandleExemptCode());
					
					if(chargeReasonMap.get(i.getNextHandleExemptCode())==null){
						String reasonDesc = retrieveChargeReasonByChargeCode(i.getNextHandleExemptCode());
						chargeReasonMap.put(i.getNextHandleExemptCode(), reasonDesc);
						sfiHandleChargeExemptionRpt.setNextExemptDesc(reasonDesc);
					}
					else{
						sfiHandleChargeExemptionRpt.setNextExemptDesc(chargeReasonMap.get(i.getNextHandleExemptCode()));
					}
				}
				sfiHandleChargeExemptionRptList.add(sfiHandleChargeExemptionRpt);
			}
			
		}		
		
		if(sfiHandleChargeExemptionRptList.isEmpty()){
			retrieveSuccess = false;
		}
		else {
			retrieveSuccess = true;
		}
		return sfiHandleChargeExemptionRptList;
	}
	
	@SuppressWarnings("unchecked")
	public String retrieveChargeReasonByChargeCode(String reasonCode){
		
		List<ChargeReason> chargeReasonList = (List<ChargeReason>) em.createQuery(
				"select o from ChargeReason o" + // 20120306 index check : ChargeReason.reasonCode,type : UI_CHARGE_REASON_01
				" where o.reasonCode = :reasonCode" +
				" and o.type = 'E'")
		.setParameter("reasonCode", reasonCode)
		.getResultList();

		if( chargeReasonList.size() > 0 ){
			return chargeReasonList.get(0).getDescription();
		}else{
			return reasonCode;
		}
	}
	
	private Map<String, Object> parameters = new HashMap<String, Object>();
	public void generateSfiHandleChargeExemptionRpt() {
	
		parameters.put("hospCode", workstore.getHospCode());
		parameters.put("workstoreCode", workstore.getWorkstoreCode());	
		parameters.put("reportDate", reportDate);

		printAgent.renderAndRedirect(new RenderJob(
				"SfiHandleChargeExemptionRpt", 
				new PrintOption(), 
				parameters, 
				sfiHandleChargeExemptionRptList));
	}
	
	public void exportSfiHandleChargeExemptionRpt() {	

		PrintOption po = new PrintOption();
		po.setDocType(ReportProvider.XLS);
		String contentId = printAgent.render(new RenderJob(
				"SfiHandleChargeExemptionXls", 
				po, 
				parameters,
				sfiHandleChargeExemptionRptList));
		
		printAgent.redirect(contentId);
	}
	
	public void printSfiHandleChargeExemptionRpt(){

		PrintOption po = new PrintOption();
		po.setDocType(ReportProvider.PDF);
		
		printAgent.renderAndPrint(new RenderAndPrintJob(
				"SfiHandleChargeExemptionRpt", 
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				parameters, 
				sfiHandleChargeExemptionRptList));
	}

	@Remove
	public void destroy() {
		if(sfiHandleChargeExemptionRptList!=null){
			sfiHandleChargeExemptionRptList=null;
		}
		
	}
	
	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}

}
