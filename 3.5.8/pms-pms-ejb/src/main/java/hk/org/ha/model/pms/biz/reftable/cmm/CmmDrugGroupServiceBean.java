package hk.org.ha.model.pms.biz.reftable.cmm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.dms.persistence.cmm.CmmDrugGroup;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.CmmDrugGroupRpt;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("cmmDrugGroupService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CmmDrugGroupServiceBean implements CmmDrugGroupServiceLocal {

	@In
	private PrintAgentInf printAgent; 
	
	@In
	private Workstore workstore;

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

	private List<CmmDrugGroupRpt> cmmDrugGroupRptList;
	
	public List<CmmDrugGroup> retrieveCmmDrugGroupList(String prefixDisplayName) {
		return dmsPmsServiceProxy.retrieveCmmDrugGroupListByDisplayName(workstore.getHospCode(), workstore.getWorkstoreCode(), prefixDisplayName);
	}

	public void updateCmmDrugGroupList(List<CmmDrugGroup> cmmDrugGroupList) {
		dmsPmsServiceProxy.updateCmmDrugGroupList(cmmDrugGroupList);
	}
	
	public void retrieveCmmDrugGroupRptList() {
		
		cmmDrugGroupRptList = new ArrayList<CmmDrugGroupRpt>();
		
		List<CmmDrugGroup> cmmDrugGroupList = dmsPmsServiceProxy.retrieveCmmDrugGroupListForExport(workstore.getHospCode(), workstore.getWorkstoreCode());
		
		int groupSeqNum = 0;
		Long prevGroupId = null;

		for (CmmDrugGroup cmmDrugGroup : cmmDrugGroupList) {

			CmmDrugGroupRpt cmmDrugGroupRpt = new CmmDrugGroupRpt(cmmDrugGroup);
			cmmDrugGroupRpt.setGroupSeqNum(cmmDrugGroupList.indexOf(cmmDrugGroup));
			cmmDrugGroupRptList.add(cmmDrugGroupRpt);
			
			if (!cmmDrugGroup.getGroupId().equals(prevGroupId)) {
				groupSeqNum++;
				prevGroupId = cmmDrugGroup.getGroupId();
			}
			cmmDrugGroupRpt.setGroupSeqNum(groupSeqNum);
		}
	}
	
	public void exportCmmDrugGroupRptList() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("hospCode", workstore.getHospCode());
		PrintOption po = new PrintOption();
		po.setDocType(ReportProvider.XLS);
		printAgent.renderAndRedirect(new RenderJob(
				"CmmDrugGroupXls", 
				po, 
				parameters, 
				cmmDrugGroupRptList));
	}
	
	@Remove
	public void destroy() {
	}
}
