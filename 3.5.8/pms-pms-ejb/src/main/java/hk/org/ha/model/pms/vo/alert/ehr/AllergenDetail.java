package hk.org.ha.model.pms.vo.alert.ehr;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class AllergenDetail implements Serializable {

	private static final long serialVersionUID = 1L;

	private String vtmTermId;
	
	private String vtmEhrDesc;
	
	private String allergenDesc;
	
	private String certainty;
	
	private String allergenType;
	
	private String remark;
	
	private String note;
	
	private String deleteReason;
	
	private Date lastUpdate;
	
	private String instCode;
	
	private String instDesc;
	
	private List<AllergenDetail> childList;

	public String getVtmTermId() {
		return vtmTermId;
	}

	public void setVtmTermId(String vtmTermId) {
		this.vtmTermId = vtmTermId;
	}

	public String getVtmEhrDesc() {
		return vtmEhrDesc;
	}

	public void setVtmEhrDesc(String vtmEhrDesc) {
		this.vtmEhrDesc = vtmEhrDesc;
	}

	public String getAllergenDesc() {
		return allergenDesc;
	}

	public void setAllergenDesc(String allergenDesc) {
		this.allergenDesc = allergenDesc;
	}

	public String getCertainty() {
		return certainty;
	}

	public void setCertainty(String certainty) {
		this.certainty = certainty;
	}

	public String getAllergenType() {
		return allergenType;
	}

	public void setAllergenType(String allergenType) {
		this.allergenType = allergenType;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getDeleteReason() {
		return deleteReason;
	}

	public void setDeleteReason(String deleteReason) {
		this.deleteReason = deleteReason;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getInstCode() {
		return instCode;
	}

	public void setInstCode(String instCode) {
		this.instCode = instCode;
	}

	public String getInstDesc() {
		return instDesc;
	}

	public void setInstDesc(String instDesc) {
		this.instDesc = instDesc;
	}

	public List<AllergenDetail> getChildList() {
		return childList;
	}

	public void setChildList(List<AllergenDetail> childList) {
		this.childList = childList;
	}
}
