package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

public class LocalWarningRpt implements Serializable {

	private static final long serialVersionUID = 1L;

	private String itemCode;
	
	private String fullDrugDesc;
	
	private String phsWarnCode1;
	
	private String phsWarnDesc1;
	
	private String phsWarnIpOnly1;
	
	private String phsWarnCode2;
	
	private String phsWarnDesc2;
	
	private String phsWarnIpOnly2;
	
	private String phsWarnCode3;
	
	private String phsWarnDesc3;
	
	private String phsWarnIpOnly3;
	
	private String phsWarnCode4;
	
	private String phsWarnDesc4;
	
	private String phsWarnIpOnly4;

	private String localWarnCode1;
	
	private String localWarnDesc1;

	private String localWarnIpOnly1;

	private String localWarnCode2;
	
	private String localWarnDesc2;

	private String localWarnIpOnly2;

	private String localWarnCode3;
	
	private String localWarnDesc3;

	private String localWarnIpOnly3;

	private String localWarnCode4;
	
	private String localWarnDesc4;

	private String localWarnIpOnly4;

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getFullDrugDesc() {
		return fullDrugDesc;
	}

	public void setFullDrugDesc(String fullDrugDesc) {
		this.fullDrugDesc = fullDrugDesc;
	}

	public String getPhsWarnCode1() {
		return phsWarnCode1;
	}

	public void setPhsWarnCode1(String phsWarnCode1) {
		this.phsWarnCode1 = phsWarnCode1;
	}

	public String getPhsWarnDesc1() {
		return phsWarnDesc1;
	}

	public void setPhsWarnDesc1(String phsWarnDesc1) {
		this.phsWarnDesc1 = phsWarnDesc1;
	}

	public String getPhsWarnIpOnly1() {
		return phsWarnIpOnly1;
	}

	public void setPhsWarnIpOnly1(String phsWarnIpOnly1) {
		this.phsWarnIpOnly1 = phsWarnIpOnly1;
	}

	public String getPhsWarnCode2() {
		return phsWarnCode2;
	}

	public void setPhsWarnCode2(String phsWarnCode2) {
		this.phsWarnCode2 = phsWarnCode2;
	}

	public String getPhsWarnDesc2() {
		return phsWarnDesc2;
	}

	public void setPhsWarnDesc2(String phsWarnDesc2) {
		this.phsWarnDesc2 = phsWarnDesc2;
	}

	public String getPhsWarnIpOnly2() {
		return phsWarnIpOnly2;
	}

	public void setPhsWarnIpOnly2(String phsWarnIpOnly2) {
		this.phsWarnIpOnly2 = phsWarnIpOnly2;
	}

	public String getPhsWarnCode3() {
		return phsWarnCode3;
	}

	public void setPhsWarnCode3(String phsWarnCode3) {
		this.phsWarnCode3 = phsWarnCode3;
	}

	public String getPhsWarnDesc3() {
		return phsWarnDesc3;
	}

	public void setPhsWarnDesc3(String phsWarnDesc3) {
		this.phsWarnDesc3 = phsWarnDesc3;
	}

	public String getPhsWarnIpOnly3() {
		return phsWarnIpOnly3;
	}

	public void setPhsWarnIpOnly3(String phsWarnIpOnly3) {
		this.phsWarnIpOnly3 = phsWarnIpOnly3;
	}

	public String getPhsWarnCode4() {
		return phsWarnCode4;
	}

	public void setPhsWarnCode4(String phsWarnCode4) {
		this.phsWarnCode4 = phsWarnCode4;
	}

	public String getPhsWarnDesc4() {
		return phsWarnDesc4;
	}

	public void setPhsWarnDesc4(String phsWarnDesc4) {
		this.phsWarnDesc4 = phsWarnDesc4;
	}

	public String getPhsWarnIpOnly4() {
		return phsWarnIpOnly4;
	}

	public void setPhsWarnIpOnly4(String phsWarnIpOnly4) {
		this.phsWarnIpOnly4 = phsWarnIpOnly4;
	}

	public String getLocalWarnCode1() {
		return localWarnCode1;
	}

	public void setLocalWarnCode1(String localWarnCode1) {
		this.localWarnCode1 = localWarnCode1;
	}

	public String getLocalWarnDesc1() {
		return localWarnDesc1;
	}

	public void setLocalWarnDesc1(String localWarnDesc1) {
		this.localWarnDesc1 = localWarnDesc1;
	}

	public String getLocalWarnIpOnly1() {
		return localWarnIpOnly1;
	}

	public void setLocalWarnIpOnly1(String localWarnIpOnly1) {
		this.localWarnIpOnly1 = localWarnIpOnly1;
	}

	public String getLocalWarnCode2() {
		return localWarnCode2;
	}

	public void setLocalWarnCode2(String localWarnCode2) {
		this.localWarnCode2 = localWarnCode2;
	}

	public String getLocalWarnDesc2() {
		return localWarnDesc2;
	}

	public void setLocalWarnDesc2(String localWarnDesc2) {
		this.localWarnDesc2 = localWarnDesc2;
	}

	public String getLocalWarnIpOnly2() {
		return localWarnIpOnly2;
	}

	public void setLocalWarnIpOnly2(String localWarnIpOnly2) {
		this.localWarnIpOnly2 = localWarnIpOnly2;
	}

	public String getLocalWarnCode3() {
		return localWarnCode3;
	}

	public void setLocalWarnCode3(String localWarnCode3) {
		this.localWarnCode3 = localWarnCode3;
	}

	public String getLocalWarnDesc3() {
		return localWarnDesc3;
	}

	public void setLocalWarnDesc3(String localWarnDesc3) {
		this.localWarnDesc3 = localWarnDesc3;
	}

	public String getLocalWarnIpOnly3() {
		return localWarnIpOnly3;
	}

	public void setLocalWarnIpOnly3(String localWarnIpOnly3) {
		this.localWarnIpOnly3 = localWarnIpOnly3;
	}

	public String getLocalWarnCode4() {
		return localWarnCode4;
	}

	public void setLocalWarnCode4(String localWarnCode4) {
		this.localWarnCode4 = localWarnCode4;
	}

	public String getLocalWarnDesc4() {
		return localWarnDesc4;
	}

	public void setLocalWarnDesc4(String localWarnDesc4) {
		this.localWarnDesc4 = localWarnDesc4;
	}

	public String getLocalWarnIpOnly4() {
		return localWarnIpOnly4;
	}

	public void setLocalWarnIpOnly4(String localWarnIpOnly4) {
		this.localWarnIpOnly4 = localWarnIpOnly4;
	}
	
}