package hk.org.ha.model.pms.persistence.medprofile;

import hk.org.ha.fmk.pms.entity.CreateDate;
import hk.org.ha.fmk.pms.entity.CreateUser;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "MP_RENAL_ECRCL_LOG")
public class MpRenalEcrclLog implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MpRenalEcrclLogSeq")
	@SequenceGenerator(name = "MpRenalEcrclLogSeq", sequenceName = "SQ_MP_RENAL_ECRCL_LOG", initialValue = 100000000)
	private Long id;

	@CreateDate
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_DATE", nullable = false)
	// change to public to avoid exception from Reflections.setAndWrap(Reflections.java:131)
	public Date createDate;
	
	@CreateUser
	@Column(name = "CREATE_USER", nullable = false, length = 12)
	// change to public to avoid exception from Reflections.setAndWrap(Reflections.java:131)	
	public String createUser;
	
	@Column(name = "BODY_WEIGHT", nullable = false, precision = 5, scale = 2)
	private BigDecimal bodyWeight;
	
	@Column(name = "SERUM_CREATININE", nullable = false, precision = 5, scale = 2)
	private BigDecimal serumCreatinine;
	
	@Column(name = "ECRCL", nullable = false, precision = 7, scale = 2)
	private BigDecimal ecrcl;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MED_PROFILE_ID", nullable = false)
	private MedProfile medProfile;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate == null ? null : new Date(createDate.getTime());
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate == null ? null : new Date(createDate.getTime());
	}
	
	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public BigDecimal getBodyWeight() {
		return bodyWeight;
	}

	public void setBodyWeight(BigDecimal bodyWeight) {
		this.bodyWeight = bodyWeight;
	}

	public BigDecimal getSerumCreatinine() {
		return serumCreatinine;
	}

	public void setSerumCreatinine(BigDecimal serumCreatinine) {
		this.serumCreatinine = serumCreatinine;
	}

	public BigDecimal getEcrcl() {
		return ecrcl;
	}

	public void setEcrcl(BigDecimal ecrcl) {
		this.ecrcl = ecrcl;
	}
	
	public MedProfile getMedProfile() {
		return medProfile;
	}

	public void setMedProfile(MedProfile medProfile) {
		this.medProfile = medProfile;
	}
}
