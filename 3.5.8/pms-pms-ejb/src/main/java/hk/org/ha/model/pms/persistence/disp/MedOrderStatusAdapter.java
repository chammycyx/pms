package hk.org.ha.model.pms.persistence.disp;

import hk.org.ha.model.pms.udt.disp.MedOrderStatus;

import java.io.Serializable;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class MedOrderStatusAdapter extends XmlAdapter<String, MedOrderStatus> implements Serializable {

	private static final long serialVersionUID = 1L;

	public String marshal( MedOrderStatus v ) {		
		return (v == null) ? null : v.getDataValue();
	}
	
	public MedOrderStatus unmarshal( String v ) {		
		return MedOrderStatus.dataValueOf(v);
	}

}