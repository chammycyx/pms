package hk.org.ha.model.pms.vo.report;

import hk.org.ha.model.pms.dms.udt.pivas.PivasFormulaType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class PivasWorkloadSummaryBatchRptDtl implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private PivasFormulaType pivasFormulaType;
	
	private String itemCode;
	
	private String pivasFormulaDescWithoutRatio;
	
	private String pivasFormulaDesc;
	
	private BigDecimal ratio;
	
	private Integer totalNumOfDose;
	
	// for export only
	private Date supplyDate;
	
	
	public PivasFormulaType getPivasFormulaType() {
		return pivasFormulaType;
	}

	public void setPivasFormulaType(PivasFormulaType pivasFormulaType) {
		this.pivasFormulaType = pivasFormulaType;
	}	
	
	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getPivasFormulaDescWithoutRatio() {
		return pivasFormulaDescWithoutRatio;
	}

	public void setPivasFormulaDescWithoutRatio(String pivasFormulaDescWithoutRatio) {
		this.pivasFormulaDescWithoutRatio = pivasFormulaDescWithoutRatio;
	}

	public String getPivasFormulaDesc() {
		return pivasFormulaDesc;
	}

	public void setPivasFormulaDesc(String pivasFormulaDesc) {
		this.pivasFormulaDesc = pivasFormulaDesc;
	}

	public BigDecimal getRatio() {
		return ratio;
	}

	public void setRatio(BigDecimal ratio) {
		this.ratio = ratio;
	}

	public Integer getTotalNumOfDose() {
		return totalNumOfDose;
	}

	public void setTotalNumOfDose(Integer totalNumOfDose) {
		this.totalNumOfDose = totalNumOfDose;
	}

	public Date getSupplyDate() {
		return supplyDate;
	}

	public void setSupplyDate(Date supplyDate) {
		this.supplyDate = supplyDate;
	}

}
