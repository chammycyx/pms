package hk.org.ha.model.pms.udt.report;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PivasExportType implements StringValuedEnum {

	PivasConsumptionExport("PivasConsumptionXls", "PivasConsumptionXls"),
	PivasPatientDispReport("PivasPatientDispXls", "PivasPatientDispXls"),
	PivasBatchDispReport("PivasBatchDispXls", "PivasBatchDispXls"),
	PivasWorkloadSummaryPatientRpt("PivasWorkloadSummaryPatientXls", "PivasWorkloadSummaryPatientXls"),
	PivasWorkloadSummaryBatchRpt("PivasWorkloadSummaryBatchXls", "PivasWorkloadSummaryBatchXls");
	
    private final String dataValue;
    private final String displayValue;

    PivasExportType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static PivasExportType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PivasExportType.class, dataValue);
	}
        
	public static class Converter extends StringValuedEnumConverter<PivasExportType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<PivasExportType> getEnumClass() {
    		return PivasExportType.class;
    	}
    }
}
