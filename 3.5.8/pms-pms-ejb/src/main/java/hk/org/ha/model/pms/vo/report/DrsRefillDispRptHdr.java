package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;

public class DrsRefillDispRptHdr implements Serializable {

	private static final long serialVersionUID = 1L;

	private String virtualTicketNum;
	
	private String patName;
	
	private String patNameChi;
	
	private String caseNum;
	
	private String hkid;
	
	private String sex;
	
	private String age;
	
	private String specCode;
	
	private String refillCouponNum;
	
	private Date refillDueDate;
	
	private Integer numOfRefill;
	
	private Integer refillNum;
	
	private Integer groupNum;
	
	private Integer numOfGroup;
	
	private String workstoreCode;
	
	private String actualTicketNum;
	
	private Date actualDispDate;
	
	private Date orgDispDate;
	
	private String orgWorkstoreCode;
	
	private String orgTicketNum;
	
	private String remark;
	
	private Boolean dangerDrugFlag;
	
	private Boolean exclusionItemFlag;
	
	private Boolean fridgeItemFlag;

	public String getVirtualTicketNum() {
		return virtualTicketNum;
	}

	public void setVirtualTicketNum(String virtualTicketNum) {
		this.virtualTicketNum = virtualTicketNum;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}
	
	public String getPatNameChi() {
		return patNameChi;
	}

	public void setPatNameChi(String patNameChi) {
		this.patNameChi = patNameChi;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public String getRefillCouponNum() {
		return refillCouponNum;
	}

	public void setRefillCouponNum(String refillCouponNum) {
		this.refillCouponNum = refillCouponNum;
	}

	public Date getRefillDueDate() {
		return refillDueDate;
	}

	public void setRefillDueDate(Date refillDueDate) {
		this.refillDueDate = refillDueDate;
	}

	public Integer getNumOfRefill() {
		return numOfRefill;
	}

	public void setNumOfRefill(Integer numOfRefill) {
		this.numOfRefill = numOfRefill;
	}

	public Integer getRefillNum() {
		return refillNum;
	}

	public void setRefillNum(Integer refillNum) {
		this.refillNum = refillNum;
	}

	public Integer getGroupNum() {
		return groupNum;
	}

	public void setGroupNum(Integer groupNum) {
		this.groupNum = groupNum;
	}

	public Integer getNumOfGroup() {
		return numOfGroup;
	}

	public void setNumOfGroup(Integer numOfGroup) {
		this.numOfGroup = numOfGroup;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public String getActualTicketNum() {
		return actualTicketNum;
	}

	public void setActualTicketNum(String actualTicketNum) {
		this.actualTicketNum = actualTicketNum;
	}

	public Date getActualDispDate() {
		return actualDispDate;
	}

	public void setActualDispDate(Date actualDispDate) {
		this.actualDispDate = actualDispDate;
	}

	public Date getOrgDispDate() {
		return orgDispDate;
	}

	public void setOrgDispDate(Date orgDispDate) {
		this.orgDispDate = orgDispDate;
	}

	public String getOrgWorkstoreCode() {
		return orgWorkstoreCode;
	}

	public void setOrgWorkstoreCode(String orgWorkstoreCode) {
		this.orgWorkstoreCode = orgWorkstoreCode;
	}

	public String getOrgTicketNum() {
		return orgTicketNum;
	}

	public void setOrgTicketNum(String orgTicketNum) {
		this.orgTicketNum = orgTicketNum;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Boolean getDangerDrugFlag() {
		return dangerDrugFlag;
	}

	public void setDangerDrugFlag(Boolean dangerDrugFlag) {
		this.dangerDrugFlag = dangerDrugFlag;
	}

	public Boolean getExclusionItemFlag() {
		return exclusionItemFlag;
	}

	public void setExclusionItemFlag(Boolean exclusionItemFlag) {
		this.exclusionItemFlag = exclusionItemFlag;
	}

	public Boolean getFridgeItemFlag() {
		return fridgeItemFlag;
	}

	public void setFridgeItemFlag(Boolean fridgeItemFlag) {
		this.fridgeItemFlag = fridgeItemFlag;
	}
	
}
