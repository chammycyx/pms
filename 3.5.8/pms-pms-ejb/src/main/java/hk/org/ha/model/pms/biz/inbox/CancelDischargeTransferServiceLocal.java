package hk.org.ha.model.pms.biz.inbox;
import javax.ejb.Local;

@Local
public interface CancelDischargeTransferServiceLocal {
	void retrieveCancelDischargeTransferList();
	void destroy();
}
