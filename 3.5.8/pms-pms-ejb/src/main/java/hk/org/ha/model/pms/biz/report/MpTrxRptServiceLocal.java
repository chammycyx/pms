package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.vo.medprofile.Ward;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface MpTrxRptServiceLocal {

	List<Ward> retrieveWardSelectionList();
	
	void retrieveDeliveryList(String wardCode, String batchCode, String caseNum, Date dispenseDate);
	
	void destroy();
	
	boolean isRetrieveSuccess();
	
}
