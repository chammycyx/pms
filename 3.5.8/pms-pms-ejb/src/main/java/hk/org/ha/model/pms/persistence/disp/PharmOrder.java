package hk.org.ha.model.pms.persistence.disp;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.PatientEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderPatType;
import hk.org.ha.model.pms.udt.disp.PharmOrderStatus;
import hk.org.ha.model.pms.udt.onestop.AllowCommentType;
import hk.org.ha.model.pms.udt.refill.DocType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.vetting.CapdSplit;
import hk.org.ha.model.pms.vo.vetting.CapdSplitItem;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.PrivateOwned;

@Entity
@Table(name = "PHARM_ORDER")
public class PharmOrder extends PatientEntity {

	private static final long serialVersionUID = 1L;

	private static final String JAXB_CONTEXT_VETTING = "hk.org.ha.model.pms.vo.vetting";	
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pharmOrderSeq")
	@SequenceGenerator(name = "pharmOrderSeq", sequenceName = "SQ_PHARM_ORDER", initialValue = 100000000)
	private Long id;

	@Version
	@Column(name="VERSION", nullable = false)
	private Long version;
	
	@Column(name = "WARD_CODE", length = 4)
	private String wardCode;
	
	@Column(name = "SPEC_CODE", length = 4)
	private String specCode;
	
	@Column(name = "PAT_CAT_CODE", length = 2)
	private String patCatCode;
	
	@Converter(name = "PharmOrder.patType", converterClass = PharmOrderPatType.Converter.class)
    @Convert("PharmOrder.patType")
	@Column(name = "PAT_TYPE", nullable = false, length = 1)
	private PharmOrderPatType patType;
	
	@Converter(name = "PharmOrder.adminStatus", converterClass = PharmOrderAdminStatus.Converter.class)
    @Convert("PharmOrder.adminStatus")
	@Column(name="ADMIN_STATUS", nullable = false, length = 1)
	private PharmOrderAdminStatus adminStatus;
	
	@Converter(name = "PharmOrder.allowCommentType", converterClass = AllowCommentType.Converter.class)
    @Convert("PharmOrder.allowCommentType")
	@Column(name="ALLOW_COMMENT_TYPE", nullable = false, length = 1)
	private AllowCommentType allowCommentType;

	@Converter(name = "PharmOrder.status", converterClass = PharmOrderStatus.Converter.class)
    @Convert("PharmOrder.status")
	@Column(name="STATUS", nullable = false, length = 1)
	private PharmOrderStatus status;
	
	@Column(name = "MAX_ITEM_NUM", nullable = false)
	private Integer maxItemNum;
	
	@Column(name = "CHEST_FLAG", nullable = false)
	private Boolean chestFlag;
	
	@Column(name = "AWAIT_SFI_FLAG", nullable = false)
	private Boolean awaitSfiFlag;
		
	@Column(name = "PRIVATE_FLAG", nullable = false)
	private Boolean privateFlag;
	
	@Column(name = "PREGNANCY_CHECK_FLAG", nullable = false)
	private Boolean pregnancyCheckFlag;

	@Column(name = "NUM_OF_REFILL", nullable = false)
	private Integer numOfRefill;

	@Column(name = "NUM_OF_GROUP", nullable = false)
	private Integer numOfGroup;
	
	@Lob
	@Column(name = "CAPD_SPLIT_XML")
	@Basic(fetch=FetchType.LAZY)
	private String capdSplitXml;
	
    @PrivateOwned
    @OrderBy("orgItemNum")
    @OneToMany(mappedBy="pharmOrder", cascade=CascadeType.ALL)
	private List<PharmOrderItem> pharmOrderItemList;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "MED_ORDER_ID")
    private MedOrder medOrder;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE",      referencedColumnName = "HOSP_CODE",      nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "MED_CASE_ID")
	private MedCase medCase;
	
	@Column(name = "WORKSTATION_CODE", length = 4)
	private String workstationCode;
	
	@Column(name = "DOCTOR_CODE", length = 12)
	private String doctorCode;
	
	@Column(name = "DOCTOR_NAME", length = 48)
	private String doctorName;

	@Column(name = "DEFAULT_DOCTOR_CODE", length = 12)
	private String defaultDoctorCode;

	@Column(name = "ORDER_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date orderDate;
	
	@Column(name = "TICKET_DATE", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date ticketDate;
	
	// for manual order only
	@Column(name = "FORCE_PROCEED_FLAG", nullable = false)
	private Boolean forceProceedFlag;
	
	// for manual order only
	@Column(name = "RECEIPT_NUM", length = 20)
	private String receiptNum;
	
	// for onestop - dispRefillCount
	@Column(name = "REFILL_COUNT", nullable = false)
	private Integer refillCount;
	
	@Column(name = "URGENT_FLAG")
	private Boolean urgentFlag;
	
	@Column(name = "ALLOW_COMMENT_BEFORE_FLAG")
	private Boolean allowCommentBeforeFlag;
	
	@Column(name = "DRS_FLAG")
	private Boolean drsFlag;
	
	@Transient
	private List<RefillSchedule> refillScheduleList; 

	@Transient
	private List<DispOrder> dispOrderList;

	@Transient
	private CapdSplit capdSplit;
	
	@Transient
	private Integer maxOrgItemNum;
	
	public PharmOrder() {
		super();
		numOfRefill = 0;
		numOfGroup = 0;
		maxItemNum = Integer.valueOf(0);
		maxOrgItemNum = null;
		chestFlag = Boolean.FALSE;
		awaitSfiFlag = Boolean.FALSE;
		patType = PharmOrderPatType.Public;
		adminStatus = PharmOrderAdminStatus.Normal;
		allowCommentType = AllowCommentType.None;
		allowCommentBeforeFlag = Boolean.FALSE;
		status = PharmOrderStatus.Outstanding;
		privateFlag = Boolean.FALSE;
		pregnancyCheckFlag = Boolean.FALSE; 
		forceProceedFlag = Boolean.FALSE;
		refillCount = 0;
		urgentFlag = Boolean.FALSE;
		drsFlag = Boolean.FALSE;
	}
	
	private void initMaxOrgItemNum() {
		maxOrgItemNum = Integer.valueOf(0);
		for (PharmOrderItem poi : getPharmOrderItemList()) {
			if (poi.getOrgItemNum() != null && poi.getOrgItemNum().compareTo(maxOrgItemNum) > 0) {
				maxOrgItemNum = poi.getOrgItemNum();
			}
		}
	}
	
    public void loadCapdSplit() {

    	if (capdSplitXml != null && capdSplit == null) {
    		JaxbWrapper<CapdSplit> vettingJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_VETTING);
			capdSplit = vettingJaxbWrapper.unmarshall(capdSplitXml);
			
			for ( CapdSplitItem capdSplitItem : capdSplit.getCapdSplitItemList() ){
				if ( capdSplitItem.getItemCode() != null && StringUtils.isBlank(capdSplitItem.getFullDrugDesc()) ) {
					DmDrug dmDrug = DmDrugCacher.instance().getDmDrug(capdSplitItem.getItemCode());
					String fullDrugDesc = dmDrug.getDrugName();
					if ( dmDrug.getDmDrugCapd() != null ) {
						if (StringUtils.isNotBlank(dmDrug.getDmDrugCapd().getConnectSystem())) {
							fullDrugDesc += " "+dmDrug.getDmDrugCapd().getConnectSystem();
						}
						
						if (dmDrug.getVolumeValue() != null && StringUtils.isNotBlank(dmDrug.getVolumeUnit())) {
							fullDrugDesc += " "+dmDrug.getVolumeValue()+" "+dmDrug.getVolumeUnit();
						}
					}
					capdSplitItem.setFullDrugDesc(fullDrugDesc);
					capdSplitItem.setBaseUnit(dmDrug.getBaseUnit());
				}
				if (capdSplitItem.getItemNum() != null) {
					PharmOrderItem pharmOrderItem = getPharmOrderItemByOrgItemNum(capdSplitItem.getItemNum());
					if (pharmOrderItem != null) {
						capdSplitItem.setOrderQty(pharmOrderItem.getCalQty());
					}
				}
			}
		}    	
	}
	
    @PrePersist
    @PreUpdate
    public void preSave() {
    	if (capdSplit != null) {
    		JaxbWrapper<CapdSplit> vettingJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_VETTING);
    		capdSplitXml = vettingJaxbWrapper.marshall(capdSplit);
    	}
    }

    public void loadDmInfo() {    	
    	for (PharmOrderItem item : getPharmOrderItemList()) {
    		item.loadDmInfo();
    	}    	
    	if (getMedOrder() != null) {
    		getMedOrder().loadDmInfo();
    	}
    }
    
    public void clearDmInfo() {    	
    	for (PharmOrderItem item : getPharmOrderItemList()) {
    		item.clearDmInfo();
    	}    	
    	if (getMedOrder() != null) {
    		getMedOrder().clearDmInfo();
    	}
    }
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}		
	
	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public String getPatCatCode() {
		return patCatCode;
	}

	public void setPatCatCode(String patCatCode) {
		this.patCatCode = patCatCode;
	}

	public void setPatType(PharmOrderPatType patType) {
		this.patType = patType;
	}

	public PharmOrderPatType getPatType() {
		return patType;
	}

	public Integer getNumOfRefill() {
		return numOfRefill;
	}

	public void setNumOfRefill(Integer numOfRefill) {
		this.numOfRefill = numOfRefill;
	}

	public Integer getNumOfGroup() {
		return numOfGroup;
	}

	public void setNumOfGroup(Integer numOfGroup) {
		this.numOfGroup = numOfGroup;
	}

	public PharmOrderAdminStatus getAdminStatus() {
		return adminStatus;
	}

	public void setAdminStatus(PharmOrderAdminStatus adminStatus) {
		this.adminStatus = adminStatus;
	}

	public AllowCommentType getAllowCommentType() {
		return allowCommentType;
	}

	public void setAllowCommentType(AllowCommentType allowCommentType) {
		this.allowCommentType = allowCommentType;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public PharmOrderStatus getStatus() {
		return status;
	}

	public void setStatus(PharmOrderStatus status) {
		this.status = status;
	}

	public Integer getMaxItemNum() {
		return maxItemNum;
	}

	public void setMaxItemNum(Integer maxItemNum) {
		this.maxItemNum = maxItemNum;
	}

	public Boolean getChestFlag() {
		return chestFlag;
	}

	public void setChestFlag(Boolean chestFlag) {
		this.chestFlag = chestFlag;
	}

	public Boolean getAwaitSfiFlag() {
		return awaitSfiFlag;
	}

	public void setAwaitSfiFlag(Boolean awaitSfiFlag) {
		this.awaitSfiFlag = awaitSfiFlag;
	}

	public Boolean getPrivateFlag() {
		return privateFlag;
	}

	public void setPrivateFlag(Boolean privateFlag) {
		this.privateFlag = privateFlag;
	}

	public Boolean getPregnancyCheckFlag() {
		return pregnancyCheckFlag;
	}

	public void setPregnancyCheckFlag(Boolean pregnancyCheckFlag) {
		this.pregnancyCheckFlag = pregnancyCheckFlag;
	}

	public String getWorkstationCode() {
		return workstationCode;
	}

	public void setWorkstationCode(String workstationCode) {
		this.workstationCode = workstationCode;
	}
	
	public String getDoctorCode() {
		return doctorCode;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}
	
	public String getDefaultDoctorCode() {
		return defaultDoctorCode;
	}

	public void setDefaultDoctorCode(String defaultDoctorCode) {
		this.defaultDoctorCode = defaultDoctorCode;
	}
	
	public Date getOrderDate() {
		if (orderDate == null) {
			return null;
		}
		else {
			return new Date(orderDate.getTime());
		}
	}

	public void setOrderDate(Date orderDate) {
		if (orderDate == null) {
			this.orderDate = null;
		}
		else {
			this.orderDate = new Date(orderDate.getTime());
		}
	}

	public Date getTicketDate() {
		if (ticketDate == null) {
			return null;
		}
		else {
			return new Date(ticketDate.getTime());
		}
	}

	public void setTicketDate(Date ticketDate) {
		if (ticketDate == null) {
			this.ticketDate = null;
		}
		else {
			this.ticketDate = new Date(ticketDate.getTime());
		}
	}

	public Boolean getForceProceedFlag() {
		return forceProceedFlag;
	}

	public void setForceProceedFlag(Boolean forceProceedFlag) {
		this.forceProceedFlag = forceProceedFlag;
	}

	public String getReceiptNum() {
		return receiptNum;
	}

	public void setReceiptNum(String receiptNum) {
		this.receiptNum = receiptNum;
	}

	public void setRefillCount(Integer refillCount) {
		this.refillCount = refillCount;
	}

	public Integer getRefillCount() {
		return refillCount;
	}
	
	public Boolean getUrgentFlag() {
		return urgentFlag;
	}

	public void setUrgentFlag(Boolean urgentFlag) {
		this.urgentFlag = urgentFlag;
	}

	public void setAllowCommentBeforeFlag(Boolean allowCommentBeforeFlag) {
		this.allowCommentBeforeFlag = allowCommentBeforeFlag;
	}

	public Boolean getAllowCommentBeforeFlag() {
		return allowCommentBeforeFlag;
	}

	public Boolean getDrsFlag() {
		return drsFlag;
	}

	public void setDrsFlag(Boolean drsFlag) {
		this.drsFlag = drsFlag;
	}

	public String getCapdSplitXml() {
		return capdSplitXml;
	}

	public void setCapdSplitXml(String capdSplitXml) {
		this.capdSplitXml = capdSplitXml;
	}

	public List<PharmOrderItem> getPharmOrderItemList() {
		if (pharmOrderItemList == null) {
			pharmOrderItemList = new ArrayList<PharmOrderItem>();
		}
		return pharmOrderItemList;
	}
	
	public void setPharmOrderItemList(List<PharmOrderItem> pharmOrderItemList) {
		this.pharmOrderItemList = pharmOrderItemList;
	}
	
	public void addPharmOrderItem(PharmOrderItem pharmOrderItem) {
		pharmOrderItem.setPharmOrder(this);
		getPharmOrderItemList().add(pharmOrderItem);
	}
	
	public void clearPharmOrderItemList() {
		pharmOrderItemList = new ArrayList<PharmOrderItem>();
	}
	
	public MedOrder getMedOrder() {
		// special handling
		if (_medOrder != null) {
			return _medOrder;
		}
		return medOrder;
	}

	public void setMedOrder(MedOrder medOrder) {
		this.medOrder = medOrder;
	}

	public List<RefillSchedule> getRefillScheduleList() {
		if (refillScheduleList == null) {
			refillScheduleList = new ArrayList<RefillSchedule>();
		}
		return refillScheduleList;
	}

	public void setRefillScheduleList(List<RefillSchedule> refillScheduleList) {
		this.refillScheduleList = refillScheduleList;
	}
	
	public void addRefillSchedule(RefillSchedule refillSchedule) {
		refillSchedule.setPharmOrder(this);
		getRefillScheduleList().add(refillSchedule);
	}
	
	public void clearRefillScheduleList() {
		refillScheduleList = new ArrayList<RefillSchedule>();
	}
	
	public List<DispOrder> getDispOrderList() {
		if (dispOrderList == null) {
			dispOrderList = new ArrayList<DispOrder>();
		}
		return dispOrderList;
	}

	public void setDispOrderList(List<DispOrder> dispOrderList) {
		this.dispOrderList = dispOrderList;
	}
	
	public void addDispOrder(DispOrder dispOrder) {
		this.getDispOrderList().add(dispOrder);
	}
	
	public void clearDispOrderList() {
		dispOrderList = new ArrayList<DispOrder>();
	}
	
	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}

	public MedCase getMedCase() {
		// special handling
		if (_medCase != null) {
			return _medCase;
		}
		return medCase;
	}

	public void setMedCase(MedCase medCase) {
		this.medCase = medCase;
	}
	
	public void setCapdSplit(CapdSplit capdSplit) {
		this.capdSplit = capdSplit;
	}

	public CapdSplit getCapdSplit() {
		return capdSplit;
	}	
	
	public Integer getMaxOrgItemNum() {
		if (maxOrgItemNum == null) {
			initMaxOrgItemNum();
		}
		return maxOrgItemNum;
	}

	public void setMaxOrgItemNum(Integer maxOrgItemNum) {
		this.maxOrgItemNum = maxOrgItemNum;
	}

	@Transient
	private transient boolean childLoaded = false;
	
	public void loadChild() {
		
		if (getId() == null) return;

		if (!childLoaded) {			
			childLoaded = true;

			this.getMedCase();
			this.getWorkstore();			
			this.getCapdSplitXml();
	
			if (this.getMedOrder() != null) {
				this.getMedOrder().loadChild();
				this.getMedOrder().addPharmOrder(this);
			}
	
			for (PharmOrderItem item : this.getPharmOrderItemList()) {
				item.getPharmOrder();
				if (item.getMedOrderItem() != null) {
					item.getMedOrderItem().addPharmOrderItem(item);
				}
			}
			
			// finally init the list
			this.clearDispOrderList();			
			this.clearRefillScheduleList();
			
			for (PharmOrderItem item : this.getPharmOrderItemList()) {
				item.clearDispOrderItemList();
			}
		}
	}
	
	public void replace(MedOrder mo) 
	{
		replace(mo, null);
	}		

	public void replace(MedOrder mo, Map<Integer, Integer> itemMap) 
	{
		for (MedOrderItem item : mo.getMedOrderItemList()) {
			item.clearPharmOrderItemList();
		}

		for (PharmOrderItem item : this.getPharmOrderItemList()) {
			Integer medOrderItemNum = null;
			if (itemMap != null) {
				medOrderItemNum = itemMap.get(item.getItemNum());
			} else {
				medOrderItemNum = item.getMedOrderItem().getItemNum();
			}
			MedOrderItem moi = mo.getMedOrderItemByItemNum(medOrderItemNum);
			moi.addPharmOrderItem(item);
		}
		
		mo.clearPharmOrderList();		
		mo.addPharmOrder(this);
	}
	
	public Map<Integer, Integer> createMedOrderItemMap() {
		Map<Integer, Integer> itemMap = new HashMap<Integer, Integer>();
		for (PharmOrderItem pharmOrderItem : this.getPharmOrderItemList()) {
			itemMap.put(pharmOrderItem.getItemNum(), pharmOrderItem.getMedOrderItem().getItemNum());
		}
		return itemMap;
	}
		
	public PharmOrderItem getPharmOrderItemByItemNum(Integer itemNum) {
		for (PharmOrderItem item : this.getPharmOrderItemList()) {
			if (item.getItemNum().equals(itemNum)) {
				return item;
			}
		}
		return null;
	}
	
	public PharmOrderItem getPharmOrderItemByOrgItemNum(Integer orgItemNum) {
		for (PharmOrderItem item : this.getPharmOrderItemList()) {
			if (item.getOrgItemNum().equals(orgItemNum)) {
				return item;
			}
		}
		return null;
	}

	public void clearId() {
		this.setId(null);
		for (PharmOrderItem item : this.getPharmOrderItemList()) {
			item.setId(null);
		}		
	}

	public void markCompleted() {
		this.setStatus(PharmOrderStatus.Completed);		
		for (PharmOrderItem item : this.getPharmOrderItemList()) {			
			item.setStatus(PharmOrderItemStatus.Completed);
		}		
	}

	public List<DispOrder> markDeleted() {
		if (this.getStatus() == PharmOrderStatus.SysDeleted) {
			return new ArrayList<DispOrder>();
		}

		return this.markDeleted(PharmOrderStatus.Deleted);
	}
	
	public List<DispOrder> markSysDeleted() {
		return this.markDeleted(PharmOrderStatus.SysDeleted);
	}
	
	public List<DispOrder> markDeleted(PharmOrderStatus status) {
		List<DispOrder> delDispOrderList = new ArrayList<DispOrder>();

		if (this.getStatus() != status) {
			this.setStatus(status);
			
			for (DispOrder dispOrder : this.getDispOrderList()) {
				if (status == PharmOrderStatus.SysDeleted) {
					dispOrder.markDeleted(DispOrderStatus.SysDeleted);
				} else if (status == PharmOrderStatus.Deleted)  {
					dispOrder.markDeleted(DispOrderStatus.Deleted);
				}
				delDispOrderList.add(dispOrder);
			}
		}
		
		return delDispOrderList;
	}
	
	public void markChestFlag() {
		boolean flag = false;
		for (PharmOrderItem poi : getPharmOrderItemList()) {
			if (poi.getMedOrderItem().getStatus() == MedOrderItemStatus.Deleted || poi.getMedOrderItem().getStatus() == MedOrderItemStatus.SysDeleted) {
				continue;
			}
			if (poi.getChestFlag()) {
				flag = true;
			}
		}
		setChestFlag(flag);
	}
	
	public void calulateAdjQty() {
		for (PharmOrderItem item : this.getPharmOrderItemList()) {			
			BigDecimal adjQty = item.getIssueQty();			
			if (item.getCalQty() != null) {
				adjQty = adjQty.subtract(item.getCalQty());
			}
			item.setAdjQty(adjQty);
		}
	}
	
	public List<RefillSchedule> getRefillScheduleList(DocType docType) {
		List<RefillSchedule> retList = new ArrayList<RefillSchedule>();
		for (RefillSchedule rs : this.getRefillScheduleList()) {
			if (rs.getDocType() == docType) {
				retList.add(rs);
			}
		}
		return retList;
	}
	
	public void relinkAllItem() {
		
		MedOrder medOrder = this.getMedOrder();
		
		for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
			medOrderItem.setMedOrder(medOrder);
		}
		for (PharmOrderItem pharmOrderItem : this.getPharmOrderItemList()) {
			pharmOrderItem.setPharmOrder(this);
		}
		for (MedOrderFm medOrderFm : medOrder.getMedOrderFmList()) {
			medOrderFm.setMedOrder(medOrder);
		}
	}	
	
	public void markCapdVoucherFlag() {

		loadCapdSplit();
		if (capdSplit == null) {
			return;
		}

		for (CapdSplitItem capdSplitItem : capdSplit.getCapdSplitItemList()) {
			getPharmOrderItemByOrgItemNum(capdSplitItem.getItemNum()).setCapdVoucherFlag(capdSplitItem.getPrintFlag());
		}
	}
	
	public void markDirtyFlag() {
		for (PharmOrderItem poi : pharmOrderItemList) {
			poi.setDirtyFlag(Boolean.TRUE);
		}
	}
	
	public void clearDirtyFlag() {
		for (PharmOrderItem poi : pharmOrderItemList) {
			poi.setDirtyFlag(Boolean.FALSE);
		}
	}
	
	public void markSfiStatusResetFlag(){
		for (PharmOrderItem poi : pharmOrderItemList) {
			if( poi.isSfi() ){
				poi.setSfiItemStatusResetFlag(Boolean.TRUE);
			}
		}
	}
	
	@Transient
	private transient MedOrder _medOrder;
	
	@Transient
	private transient MedCase _medCase;
	
	public void disconnectMedOrder() {

		_medOrder = this.getMedOrder();
		this.setMedOrder(null);

		_medCase = this.getMedCase();
		this.setMedCase(null);
		
		for (PharmOrderItem item : this.getPharmOrderItemList()) {			
			item.disconnectMedOrderItem();
		}
	}
	
	public void reconnectMedOrder() {

		this.setMedOrder(_medOrder);
		_medOrder = null;
		
		this.setMedCase(_medCase);
		_medCase = null;
		
		for (PharmOrderItem item : this.getPharmOrderItemList()) {			
			item.reconnectMedOrderItem();
		}
	}
	
	@Transient
	private transient List<DispOrder> _dispOrderList = null;

	public void disconnectDispOrderList() {
		
		_dispOrderList = this.getDispOrderList();
		this.clearDispOrderList();
		
		for (PharmOrderItem item : this.getPharmOrderItemList()) {
			item.clearDispOrderItemList();
		}
	}
	
	public void reconnectDispOrderList() {
		
		this.setDispOrderList(_dispOrderList);
		
		for (DispOrder dispOrder : _dispOrderList) {
			for (DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList()) {
				dispOrderItem.getPharmOrderItem().addDispOrderItem(dispOrderItem);
			}
		}
	}	
		
	@Transient
	private transient List<RefillSchedule> _refillScheduleList = null;
		
	public void disconnectRefillScheduleList() {

		_refillScheduleList = this.getRefillScheduleList();
		this.clearRefillScheduleList();
		
		for (PharmOrderItem item : this.getPharmOrderItemList()) {
			item.disconnectRefillScheduleItemList();
		}
	}
	
	public void reconnectRefillScheduleList() {
		
		if (_refillScheduleList != null) {		
			this.setRefillScheduleList(_refillScheduleList);
			
			for (PharmOrderItem item : this.getPharmOrderItemList()) {
				item.reconnectRefillScheduleItemList();
			}
		}
	}
	
	public void setPatient(PatientEntity patient) {
		this.setPatKey(patient.getPatKey());
		this.setHkid(patient.getHkid());
		this.setDob(patient.getDob());
		this.setSex(patient.getSex());
		this.setName(patient.getName());
		this.setNameChi(patient.getNameChi());
		this.setPhone(patient.getPhone());
		this.setAddress(patient.getAddress());
		this.setDeathFlag(patient.getDeathFlag());
		this.setOtherDoc(patient.getOtherDoc());
		this.setCcCode(patient.getCcCode());
	}
	
	public Patient getPatient() {
		Patient patient = new Patient();
		patient.setPatKey(getPatKey());
		patient.setHkid(getHkid());
		patient.setDob(getDob());
		patient.setSex(getSex());
		patient.setName(getName());
		patient.setNameChi(getNameChi());
		patient.setPhone(getPhone());
		patient.setAddress(getAddress());
		patient.setDeathFlag(getDeathFlag());
		patient.setOtherDoc(getOtherDoc());
		patient.setCcCode(getCcCode());
		return patient;
	}
	
	public Integer getAndIncreaseMaxItemNum() {

		int maxAllowedItemNum = this.getMedOrder().getOrderType() == OrderType.InPatient ? 999 : 99;
		if (maxItemNum.intValue() > maxAllowedItemNum) {
			throw new RuntimeException("ERROR : PharmOrder.itemNum excess " + maxAllowedItemNum);
		}
		
		maxItemNum = Integer.valueOf(maxItemNum.intValue() + 1);
		return maxItemNum;
	}
	
	public Integer getAndIncreaseMaxOrgItemNum() {
		setMaxOrgItemNum(Integer.valueOf(getMaxOrgItemNum().intValue() + 1));
		return getMaxOrgItemNum();
	}
	
	@SuppressWarnings("unchecked")
	public void assignItemNum(Integer orgMaxItemNum) {
		maxItemNum = orgMaxItemNum;
		for (MedOrderItem moi : medOrder.getMedOrderItemList()) {
			for (PharmOrderItem poi : moi.getPharmOrderItemList()) {
				if (poi.isDirtyFlag()) {
					poi.setItemNum(getAndIncreaseMaxItemNum());
				}
			}
		}
		Collections.sort(getPharmOrderItemList(), new BeanComparator("itemNum"));
	}
	
	public boolean hasDirtySfiItem() {
		for (PharmOrderItem poi : pharmOrderItemList) {
			if (poi.isDirtyFlag() && poi.isSfi()) {
				return true;
			}
		}
		return false;
	}
	
	public boolean patientHeaderEquals(PharmOrder other) {
		return new EqualsBuilder()
			.append(StringUtils.trimToNull(specCode),   StringUtils.trimToNull(other.getSpecCode()))
			.append(StringUtils.trimToNull(wardCode),   StringUtils.trimToNull(other.getWardCode()))
			.append(StringUtils.trimToNull(patCatCode), StringUtils.trimToNull(other.getPatCatCode()))
			.isEquals();
	}
}
