package hk.org.ha.model.pms.biz.vetting.mp;

import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;

import java.util.List;

public interface OrderAmendmentHistoryServiceLocal {
	List<DeliveryItem> retriveDeliveryItemList(List<Long> deliveryItemIdList);	
	void destroy();
}
