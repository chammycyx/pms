package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DispTrxType implements StringValuedEnum {

	Blank("-", " ", null),
	
	// From PMS to EPR
	DispCreate("CREATE", "Create Dispensing Order", DispMessageType.PmsDispOrder),
	DispUpdate("UPDATE", "Update Dispensing Order", DispMessageType.PmsDispOrder),
	DispDelete("DELETE", "Delete Dispensing Order", DispMessageType.PmsDispOrderStatus),
	DispSuspend("SUSPEND", "Suspend Dispensing Order", DispMessageType.PmsDispOrderStatus),
	DispUnsuspend("UNSUSPEND", "Unsuspend Dispensing Order", DispMessageType.PmsDispOrderStatus);
	
	private final String dataValue;
	private final String displayValue;
	private final DispMessageType messageType;
	
	DispTrxType (final String dataValue, final String displayValue, final DispMessageType messageType) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
		this.messageType = messageType;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}

	public DispMessageType getMessageType() {
		return this.messageType;
	}
	
	public static DispTrxType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DispTrxType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<DispTrxType> {

		private static final long serialVersionUID = 1L;

		public Class<DispTrxType> getEnumClass() {
    		return DispTrxType.class;
    	}
    }
}
