package hk.org.ha.model.pms.biz.inbox;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.biz.reftable.MpWardManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileStat;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatAdminType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatOrderType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileType;
import hk.org.ha.model.pms.vo.medprofile.Ward;
import hk.org.ha.model.pms.vo.medprofile.WardGroup;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;
import org.springframework.util.CollectionUtils;

@Stateful
@Scope(ScopeType.SESSION)
@Name("pharmInboxService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PharmInboxServiceBean implements PharmInboxServiceLocal {

	private static final String MED_PROFILE_QUERY_HINT_FETCH_1 = "m.medProfile.medCase";
	private static final String MED_PROFILE_QUERY_HINT_FETCH_2 = "m.medProfile.patient";
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private PharmInboxManagerLocal pharmInboxManager;
	
	@In
	private MpWardManagerLocal mpWardManager;
	
	@In
	private CategorySummaryLocal categorySummary;
	
	@In
	private Workstation workstation;
	
	@In
	private Workstore workstore;
	
	@Out(required = false)
	private List<MedProfileStat> medProfileStatList;

	private MedProfileStatAdminType adminType;
	
	private MedProfileStatOrderType orderType;
	
	private List<Ward> wardFilterList;
	
	private List<WardGroup> wardGroupFilterList;
	
	@In @Out
	private Boolean showAllWards;
	
	@In @Out
	private Boolean showChemoProfileOnly;
	
    public PharmInboxServiceBean() {
    }
    
    @Create
    @Override
    public void init()
    {
    	showAllWards = pharmInboxManager.getShowAllWards(workstation);
    	showChemoProfileOnly = pharmInboxManager.getShowChemoProfileOnly(workstation, showChemoProfileOnly);
    }
    
    @Override
    public void setMedProfileStatTypes(MedProfileStatAdminType adminType, MedProfileStatOrderType orderType)
    {
    	this.adminType = adminType;
    	this.orderType = orderType;
    	retrieveMedProfileStatList();
    }
    
    @Override
    public void setShowAllWards(Boolean showAllWards)
    {
    	pharmInboxManager.setShowAllWards(workstation, showAllWards);
    	this.showAllWards = showAllWards;
    	retrieveMedProfileStatList();
    }
    
    @Override
    public void setShowChemoProfileOnly(Boolean showChemoProfileOnly) {
    	this.showChemoProfileOnly = showChemoProfileOnly;
    	retrieveMedProfileStatList();
    }
    
    @Override
    public void setStatTypesAndFilters(MedProfileStatAdminType adminType, MedProfileStatOrderType orderType,
    		List<Ward> wardFilterList, List<WardGroup> wardGroupFilterList)
    {
    	this.adminType = adminType;
    	this.orderType = orderType;
    	this.wardFilterList = wardFilterList;
    	this.wardGroupFilterList = wardGroupFilterList;
    	retrieveMedProfileStatList();
    }
    
	@Override
	public void retrieveMedProfileStatList() {
		
		List<String> wardCodeList;
		List<String> wardCodeListForCounting;
		
		if (orderType == MedProfileStatOrderType.All) {
			wardCodeList = mpWardManager.retrieveWardFilterList(workstore, wardFilterList, wardGroupFilterList);
			wardCodeListForCounting = mpWardManager.retrieveWardFilterList(workstore, showAllWards);
		} else {
			wardCodeList = mpWardManager.retrieveWardFilterList(workstore, showAllWards);
			wardCodeListForCounting = wardCodeList; 
		}
		
		medProfileStatList = retrieveMedProfileStatList(adminType, orderType, workstore, wardCodeList);
		logger.info("Number of MedProfile records fetched for #0 #1 : #2", adminType, orderType, medProfileStatList.size());
		categorySummary.countCategories(adminType, workstore, showChemoProfileOnly, wardCodeListForCounting);
	}
	
	@SuppressWarnings("unchecked")
	private List<MedProfileStat> retrieveMedProfileStatList(MedProfileStatAdminType adminType,
			MedProfileStatOrderType orderType, Workstore workstore, List<String> wardCodeList)
	{
		boolean wardExists = !CollectionUtils.isEmpty(wardCodeList);
		boolean showChemoProfileOnly = Boolean.TRUE.equals(this.showChemoProfileOnly);
		
		Query query = em.createQuery(
				"select m from MedProfileStat m" + // 20140926 index check : none
				" where m.adminType = :adminType and m.orderType = :orderType" +
				" and m.medProfile.hospCode = :hospCode" +
				" and (" + (wardExists ? " m.medProfile.wardCode in :wardCodeList or" : "") + 
				"   m.medProfile.wardCode is null)" +
				" and m.medProfile.status <> :inactiveStatus" +
				(showChemoProfileOnly ? " and m.medProfile.type = :chemoProfileType" : "") +
				" and (m.medProfile.type is null or m.medProfile.type = :normalProfileType" +
				"   or (m.medProfile.medCase.caseNum is not null and m.dueDate < :chemoStartDate))")
				.setParameter("adminType", adminType)
				.setParameter("orderType", orderType)
				.setParameter("hospCode", workstore.getHospCode())
				.setParameter("inactiveStatus", MedProfileStatus.Inactive)
				.setParameter("normalProfileType", MedProfileType.Normal)
				.setParameter("chemoStartDate", MedProfileUtils.addDays(MedProfileUtils.getTodayBegin(), 2))
				.setHint(QueryHints.FETCH, MED_PROFILE_QUERY_HINT_FETCH_1)
				.setHint(QueryHints.FETCH, MED_PROFILE_QUERY_HINT_FETCH_2);
		
		if (wardExists) {
			query.setParameter("wardCodeList", wardCodeList);
		}
		
		if (showChemoProfileOnly) {
			query.setParameter("chemoProfileType", MedProfileType.Chemo);
		}
		
		return query.getResultList();
	}
	
	@Remove
	@Override
	public void destroy() {
		medProfileStatList = null;
	}	
}
