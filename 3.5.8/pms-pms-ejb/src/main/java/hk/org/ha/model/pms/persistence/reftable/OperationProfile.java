package hk.org.ha.model.pms.persistence.reftable;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.reftable.OperationProfileType;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "OPERATION_PROFILE")
public class OperationProfile extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "operationProfileSeq")
	/*lower the inital value to avoid the auto delete by using clean_db() */
	@SequenceGenerator(name = "operationProfileSeq", sequenceName = "SQ_OPERATION_PROFILE", initialValue = 1000000) 
	private Long id;
	
	@Converter(name = "OperationProfile.type", converterClass = OperationProfileType.Converter.class)
    @Convert("OperationProfile.type")
	@Column(name = "TYPE", nullable = false, length = 1)
	private OperationProfileType type;
	
	@Column(name = "NAME", nullable = false, length = 100)
	private String name;

	@OrderBy("sortSeq")
    @OneToMany(mappedBy="operationProfile", cascade=CascadeType.MERGE)
    private List<OperationProp> operationPropList;
	
    @OneToMany(mappedBy="operationProfile", cascade=CascadeType.MERGE)
    private List<OperationWorkstation> operationWorkstationList;

	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE", referencedColumnName = "HOSP_CODE", nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;

	@Transient
	private boolean active = false;	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public OperationProfileType getType() {
		return type;
	}

	public void setType(OperationProfileType type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<OperationProp> getOperationPropList() {
		if (operationPropList == null) {
			operationPropList = new ArrayList<OperationProp>();
		}
		return operationPropList;
	}

	public void setOperationPropList(List<OperationProp> operationPropList) {
		this.operationPropList = operationPropList;
	}
	
	public void addOperationProp(OperationProp operationProp) {
		operationProp.setOperationProfile(this);
		getOperationPropList().add(operationProp);
	}
	
	public void removeOperationProp(OperationProp operationProp) {
		getOperationPropList().remove(operationProp);
	}

	public void clearOperationPropList() {
		operationPropList = new ArrayList<OperationProp>();
	}
	
	public List<OperationWorkstation> getOperationWorkstationList() {
		if (operationWorkstationList == null) {
			operationWorkstationList = new ArrayList<OperationWorkstation>();
		}
		return operationWorkstationList;
	}

	public void setOperationWorkstationList(List<OperationWorkstation> operationWorkstationList) {
		this.operationWorkstationList = operationWorkstationList;
	}
	
	public void addOperationWorkstation(OperationWorkstation operationWorkstation) {
		operationWorkstation.setOperationProfile(this);
		getOperationWorkstationList().add(operationWorkstation);
	}
	
	public void removeOperationWorkstation(OperationWorkstation operationWorkstation) {
		getOperationWorkstationList().remove(operationWorkstation);
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isActive() {
		return active;
	}
}
