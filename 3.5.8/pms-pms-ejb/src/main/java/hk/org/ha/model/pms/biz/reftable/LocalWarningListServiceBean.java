package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.corp.cache.DmWarningCacherInf;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmWarning;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.FollowUpWarning;
import hk.org.ha.model.pms.persistence.reftable.LocalWarning;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.report.LocalWarningRpt;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.beanutils.PropertyUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("localWarningListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class LocalWarningListServiceBean implements LocalWarningListServiceLocal {
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private Workstore workstore;
	
	@In
	private DmWarningCacherInf dmWarningCacher;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;
	
	@In
	private PrintAgentInf printAgent;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@Out(required = false)
	private List<LocalWarning> localWarningList;

	@Out(required = false)
	private List<DmWarning> localWarningDmWarningList;
	
	@Out(required = false)
	private DmDrug localWarningDmDrug;
	
	private boolean followUp;
	
	private boolean validItemCode;
	
	private boolean validPrint;
	
	public List<DmWarning> retrieveDmWarningValidateList()
	{
		return  dmWarningCacher.getDmWarningList();
	}
	
	@SuppressWarnings("unchecked")
	public void retrieveLocalWarningList(String itemCode) 
	{
		validItemCode = true;

		localWarningDmDrug = dmDrugCacher.getDmDrug(itemCode);
		WorkstoreDrug workstoreDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, itemCode);

		if ( localWarningDmDrug != null && workstoreDrug != null && workstoreDrug.getMsWorkstoreDrug() != null ) {
			workstore = em.find(Workstore.class, workstore.getId());
			followUp = false;
			
			List<FollowUpWarning> followupWarningList = em.createQuery(
					"select o from FollowUpWarning o" + // 20120303 index check : FollowUpWarning.workstoreGroup,itemCode : UI_FOLLOW_UP_WARNING_01
					" where o.itemCode = :itemCode" +
					" and o.workstoreGroup = :workstoreGroup")
					.setParameter("itemCode", localWarningDmDrug.getItemCode())
					.setParameter("workstoreGroup", workstore.getWorkstoreGroup())
					.getResultList();
			
			if ( followupWarningList.isEmpty() ) {
				localWarningList = em.createQuery(
						"select o from LocalWarning o " + // 20120303 index check : LocalWarning.workstoreGroup,itemCode : I_LOCAL_WARNING_01
						" where o.itemCode = :itemCode " +
						" and o.workstoreGroup = :workstoreGroup ")
					.setParameter("itemCode", localWarningDmDrug.getItemCode())
					.setParameter("workstoreGroup", workstore.getWorkstoreGroup()).getResultList();
				
				retrieveDmWarningList(localWarningDmDrug);
			} else {
				followUp = true;
			}
		} else {
			validItemCode = false;
		}
		
	
		
	}
	
	private void retrieveDmWarningList(DmDrug dmDrug)
	{
		if(dmDrug.getDmMoeProperty()!=null){
			localWarningDmWarningList = new ArrayList<DmWarning>();
			
			for (int i = 1; i <=4; i++) {
				String warnCode = null;
				try {
					warnCode = (String) PropertyUtils.getProperty(dmDrug.getDmMoeProperty(), "warnCode"+i);
				} catch (IllegalAccessException e) {
				} catch (InvocationTargetException e) {
				} catch (NoSuchMethodException e) {
				}
				if (warnCode != null) {
					localWarningDmWarningList.add(dmWarningCacher.getDmWarningByWarnCode(warnCode));
				}
			}
		}
	}
	 
	public void updateLocalWarningList(List<LocalWarning> updateLocalWarningList)
	{
		workstore = em.find(Workstore.class, workstore.getId());
		for (LocalWarning localWarning:updateLocalWarningList) {
			localWarning.setWorkstoreGroup(workstore.getWorkstoreGroup());
			if ( !localWarning.isMarkDelete() ) {
				if ( localWarning.getId()==null ) {
					em.persist(localWarning);
				} else {
					em.merge(localWarning);
				}
			} else if ( localWarning.getId()!=null ) {
				em.remove(em.merge(localWarning));
			}
		}
		em.flush();
		em.clear();
	}
	
	@SuppressWarnings("unchecked")
	public List<LocalWarningRpt> retrieveLocalWarningRptList()
	{
		workstore = em.find(Workstore.class, workstore.getId());
		
		List<LocalWarning> localWarningFullList = em.createQuery(
				"select o from LocalWarning o" + // 20120303 index check : LocalWarning.workstoreGroup : FK_LOCAL_WARNING_01
				" where o.workstoreGroup = :workstoreGroup")
				.setParameter("workstoreGroup", workstore.getWorkstoreGroup())
				.getResultList();

		Map<String, LocalWarningRpt> localWarningMap = new HashMap<String, LocalWarningRpt>();
		DmWarning dmWarning;
		for(LocalWarning localWarning: localWarningFullList) {

			DmDrug drug = dmDrugCacher.getDmDrug(localWarning.getItemCode());
			
			if ( drug !=null ) {
				LocalWarningRpt localWarningRpt = localWarningMap.get(localWarning.getItemCode());
		
				if(localWarningRpt==null) {
					localWarningRpt = new LocalWarningRpt();
					
					localWarningRpt.setItemCode(localWarning.getItemCode());
					localWarningRpt.setFullDrugDesc(drug.getFullDrugDesc());

					localWarningRpt.setPhsWarnCode1(drug.getDmMoeProperty().getWarnCode1());
					dmWarning = dmWarningCacher.getDmWarningByWarnCode(drug.getDmMoeProperty().getWarnCode1());
					if (dmWarning != null) {
						localWarningRpt.setPhsWarnDesc1(dmWarning.getWarnMessageEng());
						if ( "O".equals(dmWarning.getUsageType()) ) {
							localWarningRpt.setPhsWarnIpOnly1("Y");
						}
					}
					
					localWarningRpt.setPhsWarnCode2(drug.getDmMoeProperty().getWarnCode2());
					dmWarning = dmWarningCacher.getDmWarningByWarnCode(drug.getDmMoeProperty().getWarnCode2());
					if (dmWarning != null) {
						localWarningRpt.setPhsWarnDesc2(dmWarning.getWarnMessageEng());
						if ( "O".equals(dmWarning.getUsageType()) ) {
							localWarningRpt.setPhsWarnIpOnly2("Y");
						}
					}
					
					localWarningRpt.setPhsWarnCode3(drug.getDmMoeProperty().getWarnCode3());
					dmWarning = dmWarningCacher.getDmWarningByWarnCode(drug.getDmMoeProperty().getWarnCode3());
					if (dmWarning != null) {
						localWarningRpt.setPhsWarnDesc3(dmWarning.getWarnMessageEng());
						if ( "O".equals(dmWarning.getUsageType()) ) {
							localWarningRpt.setPhsWarnIpOnly3("Y");
						}
					}
					
					localWarningRpt.setPhsWarnCode4(drug.getDmMoeProperty().getWarnCode4());
					dmWarning = dmWarningCacher.getDmWarningByWarnCode(drug.getDmMoeProperty().getWarnCode4());
					if (dmWarning != null) {
						localWarningRpt.setPhsWarnDesc4(dmWarning.getWarnMessageEng());
						if ( "O".equals(dmWarning.getUsageType()) ) {
							localWarningRpt.setPhsWarnIpOnly4("Y");
						}
					}
					
					localWarningMap.put(localWarning.getItemCode(), localWarningRpt);
				}

				dmWarning = dmWarningCacher.getDmWarningByWarnCode(localWarning.getWarnCode());
				if(localWarning.getSortSeq()==1) {
					localWarningRpt.setLocalWarnCode1(localWarning.getWarnCode());
					if (dmWarning != null) {
						localWarningRpt.setLocalWarnDesc1(dmWarning.getWarnMessageEng());
						if ( "O".equals(dmWarning.getUsageType()) ) {
							localWarningRpt.setLocalWarnIpOnly1("Y");
						}
					}
				} else if (localWarning.getSortSeq()==2) {
					localWarningRpt.setLocalWarnCode2(localWarning.getWarnCode());
						if (dmWarning != null) {
							localWarningRpt.setLocalWarnDesc2(dmWarning.getWarnMessageEng());
							if ( "O".equals(dmWarning.getUsageType()) ) {
								localWarningRpt.setLocalWarnIpOnly2("Y");
							}
						}
				} else if (localWarning.getSortSeq()==3) {
						localWarningRpt.setLocalWarnCode3(localWarning.getWarnCode());
						if (dmWarning != null) {
							localWarningRpt.setLocalWarnDesc3(dmWarning.getWarnMessageEng());
							if ( "O".equals(dmWarning.getUsageType()) ) {
								localWarningRpt.setLocalWarnIpOnly3("Y");
							}
						}
				} else if (localWarning.getSortSeq()==4) {
						localWarningRpt.setLocalWarnCode4(localWarning.getWarnCode());
						if (dmWarning != null) {
							localWarningRpt.setLocalWarnDesc4(dmWarning.getWarnMessageEng());
							if ( "O".equals(dmWarning.getUsageType()) ) {
								localWarningRpt.setLocalWarnIpOnly4("Y");
							}
						}
				}
			}
		}
		
		List<LocalWarningRpt> localWarningRptList = new ArrayList<LocalWarningRpt>(localWarningMap.values());
		Collections.sort(localWarningRptList, new Comparator(){
			@Override
			public int compare(Object o1, Object o2) {
				return ((LocalWarningRpt) o1).getItemCode().compareTo(((LocalWarningRpt) o2).getItemCode());
			}
		});
		
		return localWarningRptList;
	}
	
	public void printLocalWarningRpt()
	{
		List<LocalWarningRpt> localWarningRptList = retrieveLocalWarningRptList();
		
		if (!localWarningRptList.isEmpty()) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("hospCode", workstore.getHospCode());
			parameters.put("workstoreCode", workstore.getWorkstoreCode());
			
		    printAgent.renderAndPrint(new RenderAndPrintJob(
				    "LocalWarningRpt",
				    new PrintOption(),
				    printerSelector.retrievePrinterSelect(PrintDocType.Report),
				    parameters,
				    localWarningRptList));
		    validPrint = true;
		}else {
			validPrint = false;
		}
	}
	
	@Remove
	public void destroy() 
	{		
		if (localWarningList != null) {
			localWarningList = null;
		}	
		if (localWarningDmWarningList != null) {
			localWarningDmWarningList = null;
		}
		if ( localWarningDmDrug != null ) {
			localWarningDmDrug = null;
		}
	}

	public boolean isFollowUp() {
		return followUp;
	}

	public boolean isValidItemCode() {
		return validItemCode;
	}
	
	public boolean isValidPrint() {
		return validPrint;
	}
}
