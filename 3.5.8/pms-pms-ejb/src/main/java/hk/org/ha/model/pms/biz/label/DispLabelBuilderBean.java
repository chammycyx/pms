package hk.org.ha.model.pms.biz.label;

import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_DISPBARCODE_VISIBLE;
import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_HOSPITAL_NAMECHI;
import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_HOSPITAL_NAMEENG;
import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_INSTRUCTION_VISIBLE;
import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_ITEMCODEBARCODE_VISIBLE;
import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_UPDATEUSER_CHARACTER_MASK;
import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_UPDATEUSER_VISIBLE;
import static hk.org.ha.model.pms.prop.Prop.LABEL_MPDISPLABEL_HKID_VISIBLE;
import static hk.org.ha.model.pms.prop.Prop.LABEL_MPDISPLABEL_WARDRTNBARCODE_VISIBLE;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.model.pms.biz.ticketgen.TicketManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.vetting.FixedConcnSource;
import hk.org.ha.model.pms.vo.drug.DmWarningLite;
import hk.org.ha.model.pms.vo.label.ChestLabel;
import hk.org.ha.model.pms.vo.label.DispLabel;
import hk.org.ha.model.pms.vo.label.MpDispLabel;
import hk.org.ha.model.pms.vo.label.MpDispLabelQrCode;
import hk.org.ha.model.pms.vo.label.Warning;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.DoseGroup;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;

@AutoCreate
@Stateless
@Name("dispLabelBuilder")
@Scope(ScopeType.STATELESS)
public class DispLabelBuilderBean implements DispLabelBuilderLocal {
	
	private static final String JAXB_CONTEXT_LABEL = "hk.org.ha.model.pms.vo.label";
	
	private String refrigerateWarnCode;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private LabelManagerLocal labelManager;
	
	@In
	private TicketManagerLocal ticketManager;
	
	@In(required = false)
	private UamInfo uamInfo;
	
	@In
	private InstructionBuilder instructionBuilder;
	
	public Map<String, Object> getDispLabelParam(DispLabel dispLabel) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("SHOW_DISP_BARCODE", LABEL_DISPLABEL_DISPBARCODE_VISIBLE.get(true));
		params.put("SHOW_ITEMCODE_BARCODE", LABEL_DISPLABEL_ITEMCODEBARCODE_VISIBLE.get(true));
		params.put("SHOW_UPDATE_USER", LABEL_DISPLABEL_UPDATEUSER_VISIBLE.get(false));
		params.put("SHOW_INSTRUCTION", LABEL_DISPLABEL_INSTRUCTION_VISIBLE.get(true));
		String maskedUserCode = getMaskedUserCode(dispLabel.getUserCode());
		params.put("MASKED_USER_CODE", maskedUserCode);
		return params;
	}
	
	public Map<String, Object> getMpDispLabelParam(MpDispLabel mpDispLabel) {
		Map<String, Object> params = new HashMap<String, Object>();
		
		if (mpDispLabel.getCaseNum() == null || mpDispLabel.getCaseNum().equals("")) {
			params.put("SHOW_HKID", true);
		}else {
			params.put("SHOW_HKID", LABEL_MPDISPLABEL_HKID_VISIBLE.get(false));
		}
		
		params.put("SHOW_UPDATE_USER", LABEL_DISPLABEL_UPDATEUSER_VISIBLE.get(false));
		params.put("SHOW_WARDRETURN_BARCODE", LABEL_MPDISPLABEL_WARDRTNBARCODE_VISIBLE.get(false));
		return params;
	}
	
	public Map<String, Object> getChestLabelParam(ChestLabel chestLabel) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("SHOW_UPDATE_USER", LABEL_DISPLABEL_UPDATEUSER_VISIBLE.get(false));
		String maskedUserCode = getMaskedUserCode(chestLabel.getUserCode());
		params.put("MASKED_USER_CODE", maskedUserCode);
		return params;
	}
	
	public String getMaskedUserCode(String userCode){
		if ( userCode == null ) {
			return StringUtils.EMPTY;
		}
		int numOfMaskChars = LABEL_DISPLABEL_UPDATEUSER_CHARACTER_MASK.get(3);
		String maskedUserCode;
		if (userCode.length() < numOfMaskChars) {
			maskedUserCode = StringUtils.repeat("*", userCode.length());
		} else {
			maskedUserCode = userCode.substring(0, userCode.length() - numOfMaskChars).concat(StringUtils.repeat("*", numOfMaskChars));	
		}
		return maskedUserCode;
	}
	
	public DispLabel convertToDispLabel(DispOrderItem dispOrderItem, Map<String,String> fdnMappingMap) {
		PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
		if (!dispOrderItem.isLegacy()) {
			pharmOrderItem.postLoad();
		}
		DispLabel dispLabel = new DispLabel();
		String itemCode = pharmOrderItem.getItemCode();
		dispLabel.setBinNum(dispOrderItem.getBinNum()); 
		if (pharmOrderItem.getPharmOrder().getMedCase() != null) {
			dispLabel.setBedNum(pharmOrderItem.getPharmOrder().getMedCase().getPasBedNum());
			dispLabel.setCaseNum(pharmOrderItem.getPharmOrder().getMedCase().getCaseNum());
		}
		if (pharmOrderItem.getMedOrderItem() != null) {
			dispLabel.setItemNum(dispOrderItem.getItemNum());
		}
		
		if (pharmOrderItem.getPharmOrder().getMedOrder() != null) {
			MedOrder medOrder = pharmOrderItem.getPharmOrder().getMedOrder();
			dispLabel.setPatName(medOrder.getPatient().getName());
			dispLabel.setPatNameChi(StringUtils.trim(medOrder.getPatient().getNameChi()));
			
			if (!dispOrderItem.isLegacy() && (!dispOrderItem.getDispOrder().getRefillFlag()) ) {
				Date startDate = dispOrderItem.getStartDate();
				Date ticketDate = dispOrderItem.getDispOrder().getTicket().getTicketDate();
				Date orderDate = new DateMidnight(medOrder.getOrderDate()).toDate();
				if( medOrder.getDocType() == MedOrderDocType.Manual && !medOrder.isDhOrder() ){
					if (!startDate.equals(ticketDate) ){
						dispLabel.setStartDate(dispOrderItem.getStartDate());
					}
				}else{
					if( !startDate.equals(orderDate) ){
						dispLabel.setStartDate(dispOrderItem.getStartDate());
					}
				}
			}else if( dispOrderItem.getPrintRefillAuxLabelFlag() ){
				dispLabel.setStartDate(dispOrderItem.getStartDate());
			}
		}
		if (dispOrderItem.getDispOrder().getTicket() != null) {
			Ticket ticket = dispOrderItem.getDispOrder().getTicket();
			dispLabel.setTicketNum(ticket.getTicketNum());
			dispLabel.setTicketDate(ticket.getTicketDate());
			dispLabel.setFastQueueFlag(ticketManager.isFastQueueTicket(ticket.getTicketDate(), ticket.getTicketNum()));
		}
		dispLabel.setSpecCode(pharmOrderItem.getPharmOrder().getSpecCode());
		dispLabel.setDispDate(dispOrderItem.getDispOrder().getDispDate());
		if (dispOrderItem.isLegacy()) {
			dispLabel.setHospName(dispOrderItem.getDispOrder().getWorkstore().getHospCode());
			dispLabel.setHospNameChi(StringUtils.EMPTY);
		} else {
			dispLabel.setHospName( this.getHospitalNameEng(LABEL_DISPLABEL_HOSPITAL_NAMEENG.get()) );
			dispLabel.setHospNameChi(LABEL_DISPLABEL_HOSPITAL_NAMECHI.get());
		}
		if (dispOrderItem.getDispQty() != null){
			dispLabel.setIssueQty(dispOrderItem.getDispQty().intValue());
		}
		dispLabel.setItemCode(itemCode);
		dispLabel.setBaseUnit(pharmOrderItem.getBaseUnit());
		dispLabel.setPatCatCode(pharmOrderItem.getPharmOrder().getPatCatCode());
		
		if( StringUtils.isNotBlank( dispOrderItem.getEpisodeType() ) ){
			dispLabel.setUserCode(dispOrderItem.getCreateUser());
		}else{
			dispLabel.setUserCode(uamInfo.getUserId());	
		}
	
		
		
		dispLabel.setWard(pharmOrderItem.getPharmOrder().getWardCode());

		if (pharmOrderItem.getDrugName() != null) {			
			dispLabel.setItemDesc(DmDrug.buildFullDrugDesc(
					labelManager.retreiveDisplayDrugName(fdnMappingMap, pharmOrderItem),
					pharmOrderItem.getFormCode(), 
					pharmOrderItem.getFormLabelDesc(), 
					pharmOrderItem.getStrength(), 
					pharmOrderItem.getVolumeText()));
			
			dispOrderItem.getPharmOrderItem().loadDmInfo();
			
			dispLabel.setInstructionList(instructionBuilder.buildInstructionListForDispLabel(dispOrderItem));
			dispLabel.setWarningList(getWarningList(dispOrderItem));
		}
		
		return dispLabel;
	}
	
	public String getHospitalNameEng(String fullHospitalNameEng){
		if( fullHospitalNameEng.length() >= 28 ){
			String[] hospitalNameEngArray = fullHospitalNameEng.split(" ");
			StringBuilder hospitalNameEngBuilder = new StringBuilder();
			for( String block : hospitalNameEngArray ){
				if( hospitalNameEngBuilder.length() != 0 ){
					hospitalNameEngBuilder.append(" ");
				}
				if( hospitalNameEngBuilder.length() + block.length() <= 24 ){
					hospitalNameEngBuilder.append(block);
				}else{
					break;
				}
			}
			hospitalNameEngBuilder.append("...");
			return hospitalNameEngBuilder.toString();
		}else{
			return fullHospitalNameEng;
		}
	}
	
	private List<Warning> getWarningList(DispOrderItem dispOrderItem){
		
		List<String> warningLineListEng = new ArrayList<String>();
		List<String> warningLineListChi = new ArrayList<String>();
		
		if (!dispOrderItem.isLegacy()) {
			dispOrderItem.postLoad();
			
			DmWarningLite dmWarningLiteArray[] = { 
					dispOrderItem.getDmWarningLite1(), 
					dispOrderItem.getDmWarningLite2(), 
					dispOrderItem.getDmWarningLite3(), 
					dispOrderItem.getDmWarningLite4()};
			
			for (DmWarningLite dmWarningLite:dmWarningLiteArray) {
				if (dmWarningLite == null) {
					warningLineListEng.add("");
					warningLineListChi.add("");
				} else {
					warningLineListEng.add(dmWarningLite.getWarnMessageEng());
					warningLineListChi.add(dmWarningLite.getWarnMessageChi());
				}
			}
		} else {
			warningLineListEng.add(dispOrderItem.getWarnDesc1());
			warningLineListEng.add(dispOrderItem.getWarnDesc2());
			warningLineListEng.add(dispOrderItem.getWarnDesc3());
			warningLineListEng.add(dispOrderItem.getWarnDesc4());
		}
		
		Warning warningEng = new Warning();
		warningEng.setLineList(warningLineListEng);
		warningEng.setLang(PrintLang.Eng.getDataValue());
		
		Warning warningChi = new Warning();
		warningChi.setLineList(warningLineListChi);
		warningChi.setLang(PrintLang.Chi.getDataValue());
		
		List<Warning> warningList = new ArrayList<Warning>();
		warningList.add(warningEng);
		warningList.add(warningChi);
		
		return warningList;
	}

	public boolean checkRefrigerateWarning(DispOrderItem dispOrderItem) {
		String[] warnCodeArray = {dispOrderItem.getWarnCode1(), dispOrderItem.getWarnCode2(), dispOrderItem.getWarnCode3(), dispOrderItem.getWarnCode4()};
		
		String[] refrigerateWarnCodeArray = refrigerateWarnCode.split(",");
		for (String warnCode:warnCodeArray) {
			if (warnCode == null) {
				continue;
			}
			for (String s:refrigerateWarnCodeArray) {
				if ( StringUtils.isNotBlank(s) && s.length() == 1 ){
					s = " "+s;
				}
				if (warnCode.equals(s)) {
					return true;
				}
			}
		}
		return false;
	}
	
	public MpDispLabel convertToMpDispLabel(DispOrderItem dispOrderItem) {
		PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
		if (!dispOrderItem.isLegacy()) {
			pharmOrderItem.postLoad();
		}
		MpDispLabel mpDispLabel = new MpDispLabel();

		if ( dispOrderItem.getReplenishFlag() ) {
			mpDispLabel.setUserCode(dispOrderItem.getReplenishItemUpdateUser());
		} else if ( StringUtils.isNotBlank(dispOrderItem.getVerifyUser()) ){
			mpDispLabel.setUserCode(dispOrderItem.getVerifyUser());
		} else {
			mpDispLabel.setUserCode(uamInfo.getUserId());
		}
		mpDispLabel.setMaskedUserCode(getMaskedUserCode(mpDispLabel.getUserCode()));

		mpDispLabel.setTotalNumOfLabel( dispOrderItem.getTotalNumOfLabel() );
		mpDispLabel.setBinNum(dispOrderItem.getBinNum()); 
		if (pharmOrderItem.getPharmOrder().getMedCase() != null) {
			mpDispLabel.setBedNum(pharmOrderItem.getPharmOrder().getMedCase().getPasBedNum());
			mpDispLabel.setCaseNum(pharmOrderItem.getPharmOrder().getMedCase().getCaseNum());
		}
		
		mpDispLabel.setPrivateFlag(pharmOrderItem.getPharmOrder().getPrivateFlag());
		mpDispLabel.setPatName(pharmOrderItem.getPharmOrder().getPatient().getName());
		mpDispLabel.setPatNameChi(StringUtils.trim(pharmOrderItem.getPharmOrder().getPatient().getNameChi()));
		mpDispLabel.setSpecCode(dispOrderItem.getChargeSpecCode());
		//Source of dispDate might be change on phase 2
		mpDispLabel.setDispDate(new Date());
		if (dispOrderItem.isLegacy()) {
			mpDispLabel.setHospName(dispOrderItem.getDispOrder().getWorkstore().getHospCode());
			mpDispLabel.setHospNameChi(StringUtils.EMPTY);
		} else {
			mpDispLabel.setHospName( this.getHospitalNameEng(LABEL_DISPLABEL_HOSPITAL_NAMEENG.get()) );
			mpDispLabel.setHospNameChi(LABEL_DISPLABEL_HOSPITAL_NAMECHI.get());
		}
		if (dispOrderItem.getDispQty() != null) {
			mpDispLabel.setIssueQty(dispOrderItem.getDispQty().intValue());
		}
		if( pharmOrderItem.getBaseUnit() != null ){
			mpDispLabel.setBaseUnit(pharmOrderItem.getBaseUnit().toLowerCase());
		}
		if ( pharmOrderItem.getFormCode() != null ) {
			mpDispLabel.setFormCode(pharmOrderItem.getFormCode());
		}
		mpDispLabel.setItemCode(pharmOrderItem.getItemCode());
		mpDispLabel.setWard(pharmOrderItem.getPharmOrder().getWardCode());

		boolean statFlag = false;
		for (DoseGroup doseGroup:pharmOrderItem.getRegimen().getDoseGroupList()) {
			for (Dose dose:doseGroup.getDoseList()) {
				if (dose.getDailyFreq() != null && dose.getDailyFreq().getCode().equals("00036")) {
					statFlag = true;
					break;
				}
			}
		}
		
		mpDispLabel.setUrgentFlag(statFlag || dispOrderItem.getUrgentFlag());
		mpDispLabel.setRefrigerateFlag(checkRefrigerateWarning(dispOrderItem));
		mpDispLabel.setSingleOrderFlag(dispOrderItem.getSingleDispFlag()==null?false:dispOrderItem.getSingleDispFlag());
		mpDispLabel.setNewOrderFlag(dispOrderItem.getDispOrder().getRefillFlag()==null?false:!dispOrderItem.getDispOrder().getRefillFlag());
		mpDispLabel.setReplenishFlag(dispOrderItem.getReplenishFlag());
		
		if (pharmOrderItem.getMedOrderItem() != null) {
			mpDispLabel.setActionStatus(pharmOrderItem.getActionStatus());
			if( pharmOrderItem.getMedOrderItem().getFixedConcnSource() != null 
					&& pharmOrderItem.getMedOrderItem().getFixedConcnSource() != FixedConcnSource.None ){
				mpDispLabel.setFcFlag(true);	
			}
			mpDispLabel.setAlertFlag(pharmOrderItem.getMedOrderItem().getAlertFlag()==null?false:pharmOrderItem.getMedOrderItem().getAlertFlag());
		}

		String itemDesc = DmDrug.buildFullDrugDesc(
				pharmOrderItem.getDrugName(), 
				pharmOrderItem.getFormCode(), 
				pharmOrderItem.getFormLabelDesc(), 
				pharmOrderItem.getStrength(),
				pharmOrderItem.getVolumeText());
		mpDispLabel.setItemDesc(itemDesc);

		mpDispLabel.setFloatingDrugName(pharmOrderItem.getSynonym());
		mpDispLabel.setHkid(pharmOrderItem.getPharmOrder().getPatient().getHkid());
		
		if (dispOrderItem.getDispOrder().getTicket() != null) {
			int batchDay = new DateTime(dispOrderItem.getDispOrder().getTicket().getTicketDate()).getDayOfMonth();
			mpDispLabel.setBatchNum( (( batchDay < 10 )?"0":"") + batchDay + dispOrderItem.getDispOrder().getTicket().getTicketNum());
		}
		
		mpDispLabel.setPrintLang(dispOrderItem.getDispOrder().getPrintLang());
		mpDispLabel.setShowInstructionFlag(dispOrderItem.getPrintMpDispLabelInstructionFlag());
		
		mpDispLabel.setReDispFlag(dispOrderItem.getReDispFlag());
		mpDispLabel.setWardStockFlag(dispOrderItem.getPharmOrderItem().getWardStockFlag());

		dispOrderItem.getPharmOrderItem().loadDmInfo();
		
		mpDispLabel.setInstructionList(instructionBuilder.buildInstructionListForDispLabel(dispOrderItem));
		mpDispLabel.setWarningList(getWarningList(dispOrderItem));
		JaxbWrapper<MpDispLabelQrCode> dispLabelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
		String qrCode = dispLabelJaxbWrapper.marshall(buildQrCode(dispOrderItem, itemDesc));
		qrCode = qrCode.substring(qrCode.indexOf(">") + 1).replaceAll("MpDispLabelQrCode", "DRUG");
		mpDispLabel.setQrCodeXml(qrCode);
		mpDispLabel.setDeliveryItemId(dispOrderItem.getDeliveryItemId());
		
		return mpDispLabel;
	}
	
	private MpDispLabelQrCode buildQrCode(DispOrderItem dispOrderItem, String itemDesc) {
		MpDispLabelQrCode qrCode = new MpDispLabelQrCode();
		
		DmDrug dmDrug = dmDrugCacher.getDmDrug(dispOrderItem.getPharmOrderItem().getItemCode());
		//Tradename
		
		qrCode.setA((dmDrug == null || dmDrug.getDmDrugProperty().getDisplayname() == null)?"":dmDrug.getDmDrugProperty().getDisplayname());
		qrCode.setB((dmDrug == null || dmDrug.getDmDrugProperty().getSaltProperty() == null)?"":dmDrug.getDmDrugProperty().getSaltProperty());
		qrCode.setC(itemDesc);
		qrCode.setD(dispOrderItem.getPharmOrderItem().getFormCode());
		qrCode.setE(dispOrderItem.getDeliveryItemId());
		qrCode.setF(dispOrderItem.getPharmOrderItem().getItemCode());
		qrCode.setG(dispOrderItem.getPharmOrderItem().getPharmOrder().getWardCode());
		qrCode.setH("");
		qrCode.setI("");
		qrCode.setJ("");

		if (dispOrderItem.getPharmOrderItem().getPharmOrder().getMedOrder().getIsChemoOrder()) {
			qrCode.setType(MpDispLabelQrCode.CHEMO_ORDER);
			qrCode.setK(dispOrderItem.getPharmOrderItem().getPharmOrder().getPatKey());
			String orderNum = dispOrderItem.getPharmOrderItem().getPharmOrder().getMedOrder().getOrderNum();
			if (orderNum != null && orderNum.length() >= 3) {
				qrCode.setL(orderNum.substring(3));
			}
			if (dispOrderItem.getPharmOrderItem().getMedOrderItem().getIsMultipleChemoItem()) {
				qrCode.setM("0");
			}
			else {
				qrCode.setM(dispOrderItem.getPharmOrderItem().getMedOrderItem().getItemNum().toString());
			}
		}
		else {
			qrCode.setType(MpDispLabelQrCode.NORMAL_ORDER);
		}
		return qrCode;
	}
}
