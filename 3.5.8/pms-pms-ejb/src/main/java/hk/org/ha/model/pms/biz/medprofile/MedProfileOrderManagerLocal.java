package hk.org.ha.model.pms.biz.medprofile;
import java.util.Date;
import java.util.List;

import hk.org.ha.model.pms.corp.vo.medprofile.SignatureCountResult;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileOrder;

import javax.ejb.Local;

@Local
public interface MedProfileOrderManagerLocal {
	void saveMedProfileOrder(MedProfileOrder medProfileOrder);
	List<SignatureCountResult> retrieveSignatureCountResult(Date batchDate);
}
