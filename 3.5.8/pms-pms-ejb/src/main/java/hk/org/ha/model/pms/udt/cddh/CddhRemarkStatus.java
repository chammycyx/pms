package hk.org.ha.model.pms.udt.cddh;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum CddhRemarkStatus implements StringValuedEnum {
	Success("S","Success"),
	AccessDenied("D","Access Denied"),
	DayEndProcessing("P", "Day End Processing"),
	Error("E","Error");
	
    private final String dataValue;
    private final String displayValue;

    CddhRemarkStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static CddhRemarkStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(CddhRemarkStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<CddhRemarkStatus> {
		private static final long serialVersionUID = 1L;

		@Override
    	public Class<CddhRemarkStatus> getEnumClass() {
    		return CddhRemarkStatus.class;
    	}
    }
}



