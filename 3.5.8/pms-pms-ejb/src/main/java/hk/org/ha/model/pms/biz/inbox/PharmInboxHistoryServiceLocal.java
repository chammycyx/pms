package hk.org.ha.model.pms.biz.inbox;
import javax.ejb.Local;

@Local
public interface PharmInboxHistoryServiceLocal {
	void retrieveMedProfileTrxLogList();
	void destroy();
}
