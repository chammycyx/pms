package hk.org.ha.model.pms.udt.report;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PivasReportType implements StringValuedEnum {

	PivasConsumptionReport("PivasConsumptionRpt", "Consumption Report"),
	PivasPatientDispReport("PivasPatientDispRpt", "Patient Dispensing Report"),
	PivasBatchDispReport("PivasBatchDispRpt", "Batch Dispensing Report"),
	PivasWorkloadSummaryPatientRpt("PivasWorkloadSummaryPatientRpt", "Workload Summary (Patient)"),
	PivasWorkloadSummaryBatchRpt("PivasWorkloadSummaryBatchRpt", "Workload Summary (Batch)");
	
    private final String dataValue;
    private final String displayValue;

    PivasReportType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static PivasReportType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PivasReportType.class, dataValue);
	}
        
	public static class Converter extends StringValuedEnumConverter<PivasReportType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<PivasReportType> getEnumClass() {
    		return PivasReportType.class;
    	}
    }
}
