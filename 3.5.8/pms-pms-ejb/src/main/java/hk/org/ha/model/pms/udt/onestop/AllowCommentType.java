package hk.org.ha.model.pms.udt.onestop;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum AllowCommentType implements StringValuedEnum {
	
	None("N", "None"),
	PostComment("Y", "PostComment"),
	PrescType("P", "PrescType"),
	All("A", "All");
	
    private final String dataValue;
    private final String displayValue;
        
    AllowCommentType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static AllowCommentType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(AllowCommentType.class, dataValue);
	}
        
	public static class Converter extends StringValuedEnumConverter<AllowCommentType> {

		private static final long serialVersionUID = 1L;

		public Class<AllowCommentType> getEnumClass() {
    		return AllowCommentType.class;
    	}
    }
}
