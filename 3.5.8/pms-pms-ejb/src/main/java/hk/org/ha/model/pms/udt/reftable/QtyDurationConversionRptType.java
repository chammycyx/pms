package hk.org.ha.model.pms.udt.reftable;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum QtyDurationConversionRptType implements StringValuedEnum{

	QuantityConversionRpt("Q", "Quantity Conversion Report"),
	DurationConversionRpt("D", "Duration Conversion Report"),
	QuantityConversionInsuRpt("I", "Insulin Quantity Conversion Report");

	private final String dataValue;
	private final String displayValue;
	
	QtyDurationConversionRptType(final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}
	
	public static QtyDurationConversionRptType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(QtyDurationConversionRptType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<QtyDurationConversionRptType> {

		private static final long serialVersionUID = 1L;

		public Class<QtyDurationConversionRptType> getEnumClass() {
			return QtyDurationConversionRptType.class;
		}

	}
}
