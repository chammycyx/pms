package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.persistence.reftable.SuspendStat;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import org.joda.time.DateTime;

@Local
public interface SuspendStatRptServiceLocal {

	void retrieveSuspendStatList(Integer pastDay, Date dateFrom, Date dateTo);
	
	List<SuspendStat> constructSuspendStatList(Integer pastDay, Date today, Date dateFrom, Date dateTo);
	
	void generateSuspendStatRpt();
	
	void printSuspendStatRpt();
	
	void destroy();
	
}
