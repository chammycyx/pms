package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.vo.Ingredient;
import hk.org.ha.model.pms.dms.vo.IngredientSearchCriteria;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
import hk.org.ha.model.pms.vo.drug.DrugSearchReturn;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.WordUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("drugSearchIngredientService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DrugSearchIngredientServiceBean implements DrugSearchIngredientServiceLocal {
	
	private static final char[] DELIMITER = new char[] {' ' , '~' , '&' , '-' , '[' , '(' , '<', '+'}; 
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private Workstore workstore;

	
	public DrugSearchReturn retrieveDrugSearchIngredient(DrugSearchSource drugSearchSource, IngredientSearchCriteria ingredientSearchCriteria) {
		ingredientSearchCriteria.setDispHospCode(workstore.getHospCode());
		ingredientSearchCriteria.setDispWorkstore(workstore.getWorkstoreCode());

		List<Ingredient> drugSearchDrugIngredientList = dmsPmsServiceProxy.retrieveIngredient(ingredientSearchCriteria);
		for (Ingredient ingredient : drugSearchDrugIngredientList) {
			ingredient.setIngredientDesc(WordUtils.capitalizeFully(ingredient.getIngredientDesc(), DELIMITER));
		}
		
		DrugSearchReturn drugSearchReturn = new DrugSearchReturn();
		drugSearchReturn.setDrugSearchSource(drugSearchSource);
		drugSearchReturn.setDrugSearchDrugIngredientList(drugSearchDrugIngredientList);
		return drugSearchReturn;
	}
	
	@Remove
	public void destroy() 
	{
	}
}