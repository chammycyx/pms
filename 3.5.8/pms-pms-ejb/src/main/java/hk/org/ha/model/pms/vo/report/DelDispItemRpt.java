package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class DelDispItemRpt implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String ticketNum;

	private String patType;
	
	private String caseNum;
	
	private String patName;
	
	private String patNameChi;
	
	private String hkid;
	
	private String refNum;
	
	private String dispDate;
	
	public String getDispDate() {
		return dispDate;
	}

	public void setDispDate(String dispDate) {
		this.dispDate = dispDate;
	}

	private List<DelDispItemRptDtl> delDispItemRptDtlList;
	
	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public String getPatType() {
		return patType;
	}

	public void setPatType(String patType) {
		this.patType = patType;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getRefNum() {
		return refNum;
	}

	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}

	public List<DelDispItemRptDtl> getDelDispItemRptDtlList() {
		return delDispItemRptDtlList;
	}

	public void setDelDispItemRptDtlList(
			List<DelDispItemRptDtl> delDispItemRptDtlList) {
		this.delDispItemRptDtlList = delDispItemRptDtlList;
	}

	public void setPatNameChi(String patNameChi) {
		this.patNameChi = patNameChi;
	}

	public String getPatNameChi() {
		return patNameChi;
	}


}