package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.persistence.corp.Workstore;

import java.util.List;

import javax.ejb.Local;

@Local
public interface WorkstoreListManagerLocal {
	List<Workstore> retrieveWorkstoreList();
	
	void destroy();
	
}

