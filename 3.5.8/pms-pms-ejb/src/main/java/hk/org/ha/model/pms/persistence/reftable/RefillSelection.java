package hk.org.ha.model.pms.persistence.reftable;

import flexjson.JSON;
import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.MsWorkstoreDrug;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.Customizer;
import org.jboss.seam.security.Identity;

@Entity
@Table(name = "REFILL_SELECTION")
@Customizer(AuditCustomizer.class)
public class RefillSelection extends VersionEntity {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "refillSelectionSeq")
	@SequenceGenerator(name = "refillSelectionSeq", sequenceName = "SQ_REFILL_SELECTION", initialValue = 100000000)
	private Long id;
	
	@Column(name = "ITEM_CODE", nullable = false, length = 6)
	private String itemCode;
	
	@Column(name = "PAS_SPEC_CODE", nullable = false, length = 4)
	private String pasSpecCode;
	
	@Column(name = "TYPE", nullable = false, length = 1)
	private String type;

	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE", referencedColumnName = "HOSP_CODE", nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;

	@Transient
	private DmDrug dmDrug;	

	@Transient
	private MsWorkstoreDrug msWorkstoreDrug;	
	
	@PostLoad
	public void postLoad() {
		if (itemCode != null && Identity.instance().isLoggedIn()) {
			dmDrug = DmDrugCacher.instance().getDmDrug(itemCode);
			msWorkstoreDrug = DmDrugCacher.instance().getMsWorkstoreDrug(workstore, itemCode);
		}
	}	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getPasSpecCode() {
		return pasSpecCode;
	}

	public void setPasSpecCode(String pasSpecCode) {
		this.pasSpecCode = pasSpecCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}

	public void setDmDrug(DmDrug dmDrug) {
		this.dmDrug = dmDrug;
	}

	@JSON(include=false)
	public DmDrug getDmDrug() {
		return dmDrug;
	}
	
	@JSON(include=false)
	public MsWorkstoreDrug getMsWorkstoreDrug() {
		return msWorkstoreDrug;
	}

	public void setMsWorkstoreDrug(MsWorkstoreDrug msWorkstoreDrug) {
		this.msWorkstoreDrug = msWorkstoreDrug;
	}
}
