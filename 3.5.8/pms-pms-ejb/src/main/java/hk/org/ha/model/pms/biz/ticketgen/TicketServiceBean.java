package hk.org.ha.model.pms.biz.ticketgen;

import static hk.org.ha.model.pms.prop.Prop.TICKETGEN_TICKET_PHARMLOCATIONCHI;
import static hk.org.ha.model.pms.prop.Prop.TICKETGEN_TICKET_PHARMLOCATIONENG;
import static hk.org.ha.model.pms.prop.Prop.TICKETGEN_TICKET_REMARK;
import static hk.org.ha.model.pms.prop.Prop.TICKETGEN_TICKET_TICKETDATE_DAYOFWEEK_VISIBLE;
import static hk.org.ha.model.pms.prop.Prop.ONESTOP_TICKETGEN_TICKET_CHECK;
import static hk.org.ha.model.pms.prop.Prop.ONESTOP_TICKETGEN_AUTO_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.TICKETGEN_FASTQUEUE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.TICKETGEN_FASTQUEUE_MOITEM_MAX;
import static hk.org.ha.model.pms.prop.Prop.ISSUE_TICKETMATCH_ENABLED;
import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.UnreachableException;
import hk.org.ha.model.pms.asa.exception.pas.PasException;
import hk.org.ha.model.pms.biz.charging.DrugChargeClearanceManagerLocal;
import hk.org.ha.model.pms.biz.onestop.OneStopUtilsLocal;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.biz.validation.SearchTextValidatorLocal;
import hk.org.ha.model.pms.pas.PasServiceWrapper;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.udt.charging.ClearanceStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.ticketgen.TicketType;
import hk.org.ha.model.pms.udt.validation.SearchTextType;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.label.TicketLabel;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.PrinterSelect;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.security.PropMap;
import hk.org.ha.model.pms.vo.ticketgen.TicketGenResult;
import hk.org.ha.service.pms.asa.interfaces.pas.PasServiceJmsRemote;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("ticketService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class TicketServiceBean implements TicketServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;

	@In(required = false)
	@Out(required = false)
	private Ticket ticket;
	
	@In(required = false)
	@Out(required = false)
	private String errMsg;
	
	@In
	private PropMap propMap;
	
	@Out(required = false)
	private TicketGenResult ticketGenResult;
		
	@In
	private Workstore workstore;
	
	@In
	private DrugChargeClearanceManagerLocal drugChargeClearanceManager;
	
	@In
	private PasServiceJmsRemote pasServiceProxy;
	
	@In
	private PasServiceWrapper pasServiceWrapper;
	
	@In
	private PrintAgentInf printAgent;
	
	@In
	private PrinterSelectorLocal printerSelector;		

	@In
	private TicketManagerLocal ticketManager;
	
	@In
	private SearchTextValidatorLocal searchTextValidator;
	
	@In
	private OneStopUtilsLocal oneStopUtils;
	
	private DateFormat pasDateFormat = new SimpleDateFormat("yyyyMMdd HH:mm"); 	
	private DateFormat ticketLabelDateFormat = new SimpleDateFormat("ddMMyyyy");
	
	public static final Pattern TICKET_NUM_PATTERN = Pattern.compile("^[0-9][0-9][0-9][0-9]");	
	public static final Pattern HN_CASE_NUM_PATTERN = Pattern.compile("^HN[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9].$");
	public static final Pattern AE_CASE_NUM_PATTERN = Pattern.compile("^AE[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9].$");
	public static final Pattern DHP_CASE_NUM_PATTERN = Pattern.compile("^DHP|CIM[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$");	
	
	private static final String ERR_MSG_NOT_FOUND = "0005";
	private static final String GEN_TICKET_BARCODE_INPUT = "GEN";
	private static final String TICKETGEN_BATCH_NOOFTICKET = "ticketGen.batch.noOfTicket";
	
	private static final Integer DEF_MAX_TICKET_NUM = 4999;
						
	public void retrieveTicketByTicketNum( Date ticketDate, String ticketNum, Long medOrderId, OneStopOrderType orderType ){
		if( propMap.getValueAsBool(ONESTOP_TICKETGEN_TICKET_CHECK.getName(), false) ){ //TicketCheck in Onestop
			validateTicket(ticketDate, ticketNum, medOrderId, orderType, true);
		}else if( propMap.getValueAsBool(ONESTOP_TICKETGEN_AUTO_ENABLED.getName(), false) ){
			//No TicketCheck and AutoGen enable - generate ticket in sequence by system
			retrieveTicketByCustomTicketNumForOneStop(null ,null);
		}else{
			// No TicketCheck and AutoGen disable - validate and generateTicket by InputTicketNum
			ticket = ticketManager.retrieveTicket(ticketNum, ticketDate);

			boolean validCustomTicketNum = false;
			try{
				validCustomTicketNum = (Integer.parseInt(ticketNum) <= DEF_MAX_TICKET_NUM);
			}catch ( NumberFormatException nfe ){
			}

			if( ticket == null && validCustomTicketNum ){
				retrieveTicketByCustomTicketNumForOneStop(ticketDate, ticketNum);
			}else{
				validateTicket(ticketDate, ticketNum, medOrderId, orderType, false);
			}
		}
	}
	
	private void validateTicket(Date ticketDate, String ticketNum, Long medOrderId, OneStopOrderType orderType, boolean oneStopCheck){
		if( ticket == null ){
			ticket = ticketManager.retrieveTicket(ticketNum, ticketDate);
		}
		
		Pair<String, Ticket> result = ticketManager.validateTicket(ticketDate, ticketNum, ticket, medOrderId, orderType, oneStopCheck);
		errMsg = result.getFirst();
		ticket = result.getSecond();
	}
	
	private void retrieveTicketByCustomTicketNumForOneStop(Date ticketDate, String ticketNum){
		errMsg = null;

		Date today = getTicketDate();
		if( ticketNum != null ){
			today = new DateMidnight(ticketDate).toDate();
		}

		logger.debug("retrieveTicketByCustomTicketNumForOneStop==========================#0,#1", today, ticketNum);
		ticket = ticketManager.generateTicket(today, TicketType.Queue1, ticketNum, null);
	}
	
	public void retrieveTicketByOrderNumCaseNum(String tickSearchCriteria, boolean enableDrugCharge, boolean checkOpas, boolean checkFcs, boolean forcePrintTicket) {		
		//Check GEN_TICK string
		if( GEN_TICKET_BARCODE_INPUT.equals(StringUtils.trimToEmpty(tickSearchCriteria))  ){
			retrieveTicketByTicketType(TicketType.Queue1);
			return;
		}		
		
		if ( !enableDrugCharge ){
			ticketGenResult = new TicketGenResult();
			ticketGenResult.setInfoMessageCode("0027");
			return;
		}
		
		SearchTextType inputTextType = searchTextValidator.checkSearchInputType(StringUtils.trimToEmpty(tickSearchCriteria));		
		
		if ( SearchTextType.OrderNum.equals(inputTextType) ) {			
			retrieveTicketByOrderNum(tickSearchCriteria, checkFcs, forcePrintTicket);			
		}else if ( SearchTextType.Case.equals(inputTextType) && (tickSearchCriteria.length() == 12 || tickSearchCriteria.length() == 11) ){
			retrieveTicketByCaseNum(tickSearchCriteria, checkOpas, checkFcs);
		}else if ( SearchTextType.SfiRefill.equals(inputTextType) ){
			retrieveTicketByTicketType(TicketType.Queue1);
		}else{
			ticketGenResult = new TicketGenResult();
			ticketGenResult.setErrorMessageCode("0016");
			return;
		}
	}	

	private void retrieveTicketByOrder(Object order, boolean checkFcs, boolean forcePrintTicket, String inMoeOrderNum){
		DispOrder dispOrder = null;
		MedOrder medOrder = null;
		if (order instanceof DispOrder) {
			dispOrder = (DispOrder) order;
			medOrder = dispOrder.getPharmOrder().getMedOrder();
		} else {
			medOrder = (MedOrder) order;
		}

		ticketGenResult = new TicketGenResult();
		ticketGenResult.setOrderNumforDisplay(medOrder.getOrderNum());
		medOrder.getMedCase();
		if (!(medOrder.getDocType().equals(MedOrderDocType.Normal) && medOrder.getStatus().equals(MedOrderStatus.Deleted))) {
			ticketGenResult.setTotalUnitOfCharge(this.retrieveTotalUnitOfChargeByMedOrder(medOrder));
		}

		Pair<Boolean,Boolean> checkStdDrugClearanceResult = checkStdDrugClearance(medOrder, dispOrder, checkFcs);
		String moeOrderNum = null;
		boolean isDeleteOrder = (Boolean)checkStdDrugClearanceResult.getSecond();
		
		if ( isDeleteOrder ){
			return;
		}
		
		boolean printTicket = (Boolean)checkStdDrugClearanceResult.getFirst();
		if( !printTicket && forcePrintTicket ){
			moeOrderNum = inMoeOrderNum;
		}else if( !printTicket ){
			return;
		}

		TicketType orderTicketType = TicketType.Queue1;
		if( isFastQueueMedOrder(medOrder) ){
			orderTicketType = TicketType.FastQueue;
		}
		generateAndPrintTicket(orderTicketType, moeOrderNum);
	}

	private boolean isFastQueueMedOrder(MedOrder medOrder){
		if( !TICKETGEN_FASTQUEUE_ENABLED.get(false) ){
			return false;
		}
		
		Integer itemCount = 0;
		Integer keepRecordCount = 0;
		Integer maxFastQueueCount = TICKETGEN_FASTQUEUE_MOITEM_MAX.get(0);
		if( maxFastQueueCount > 0 ){
			for(MedOrderItem moi : medOrder.getMedOrderItemList() ){
				if( ActionStatus.PurchaseByPatient == moi.getActionStatus() || 
					ActionStatus.SafetyNet == moi.getActionStatus() ||
					ActionStatus.DispByPharm == moi.getActionStatus() ){
					itemCount++;
					if( itemCount > maxFastQueueCount ){
						return false;
					}
				}else if( ActionStatus.DispInClinic == moi.getActionStatus() || 
						ActionStatus.ContinueWithOwnStock == moi.getActionStatus() ){
					keepRecordCount++;
				}
			}
			
			if( itemCount > 0 && maxFastQueueCount >= itemCount  ){
				return true;
			}else if( itemCount == 0 && keepRecordCount > 0 ){
				return true;
			}
		}
		return false;
	}
	
	private Integer retrieveTotalUnitOfChargeByMedOrder(MedOrder medOrder){
		Integer totalUnitOfCharge = Integer.valueOf(0);
		if( medOrder.getMedCase() != null && !medOrder.getMedCase().getChargeFlag() ){
			// do nothing
		}else{		
			for( MedOrderItem moi : medOrder.getMedOrderItemList() ){
				totalUnitOfCharge += moi.getCmsConfirmUnitOfCharge();
			}
		}
		return totalUnitOfCharge;
	}
	
	@SuppressWarnings("unchecked")
	private void retrieveTicketByOrderNum(String moeOrderNum, boolean checkFcs, boolean forcePrintTicket){
		String trimOrderNum = oneStopUtils.convertOrderNum(moeOrderNum);
		
		List<MedOrder> medOrderList = em.createQuery(
				"select o from MedOrder o" + // 20120307 index check : MedOrder.orderNum : I_MED_ORDER_01
				" where o.orderNum = :orderNum" +
				" and o.orderType = :orderType" +
				" and o.status in :statusList" +
				" and o.docType = :docType" +
				" order by o.orderDate desc")
				.setParameter("orderNum", trimOrderNum)
				.setParameter("orderType", OrderType.OutPatient)
				.setParameter("docType", MedOrderDocType.Normal)
				.setParameter("statusList", MedOrderStatus.Outstanding_Deleted_Withhold_PreVet)
				.getResultList();
		
		if( medOrderList.size() > 0 ){
			retrieveTicketByOrder(medOrderList.get(0), checkFcs, forcePrintTicket, moeOrderNum.substring(3));
			return;
		}else{
			List<DispOrder> dispOrderList = (List<DispOrder>) em.createQuery(
					"select o from DispOrder o" + // 20120307 index check : MedOrder.orderNum : I_MED_ORDER_01
					" where o.pharmOrder.medOrder.orderNum = :orderNum" +
					" and o.pharmOrder.medOrder.orderType = :orderType" +
					" and o.pharmOrder.medOrder.docType = :docType" +
					" and o.pharmOrder.medOrder.status in :medOrderstatus")							
					.setParameter("orderNum", trimOrderNum)
					.setParameter("orderType", OrderType.OutPatient)
					.setParameter("docType", MedOrderDocType.Normal)
					.setParameter("medOrderstatus", MedOrderStatus.Fulfilling_Complete_Delete)
					.getResultList();
			
			if( dispOrderList.size() > 0 ){
				retrieveTicketByOrder( dispOrderList.get(0), checkFcs, forcePrintTicket, moeOrderNum.substring(3));
				return;
			}
		}
		
		ticketGenResult = new TicketGenResult();
		ticketGenResult.setInfoMessageCode(ERR_MSG_NOT_FOUND);
	}

	@SuppressWarnings("unchecked")
	private void retrieveTicketByCaseNum(String caseNum, boolean checkOpas, boolean checkFcs){	
		ticketGenResult = new TicketGenResult();
		String trimCaseNum = StringUtils.trimToEmpty(caseNum);
		ticketGenResult.setOrderNumforDisplay(trimCaseNum);
		TicketType caseNumTicketType = TicketType.Queue1;
		
		Patient patient = null;
		
		if( !checkOpas ){
			ticketGenResult.setInfoMessageCode("0023");//No OPAS Interface
			return;
		}
		
		try {
			patient = pasServiceWrapper.retrievePasPatientByCaseNum(workstore.getDefPatHospCode(), trimCaseNum, false);
		} catch (PasException e) {
			ticketGenResult.setInfoMessageCode("0020");
			return;
		} catch (UnreachableException e) {
			ticketGenResult.setInfoMessageCode("0281");
			return;
		}
		
		if ( patient == null ){
			ticketGenResult.setInfoMessageCode("0020");//NO PMI Information
			return;
		}
		
		if( HN_CASE_NUM_PATTERN.matcher(trimCaseNum).find() ||
			AE_CASE_NUM_PATTERN.matcher(trimCaseNum).find()||
			DHP_CASE_NUM_PATTERN.matcher(trimCaseNum).find()){
			
			ticketGenResult.setStdDrugClearance(ClearanceStatus.PaymentClear);
			
		}else{						
			List<MedCase> medCaseList;
			
			DateTime startDate = new DateTime().minusDays(7);
			
			try {
				medCaseList =  pasServiceProxy.retrievePasAppointment( workstore.getDefPatHospCode(), trimCaseNum, "", patient.getPatKey(), patient.getHkid(), "", "", pasDateFormat.format(startDate.toDate()), pasDateFormat.format(new Date()) );					
			} catch (PasException e) {
				ticketGenResult.setInfoMessageCode("0023");//NO OPAS Information
				return;
			} catch (UnreachableException e) {
				ticketGenResult.setInfoMessageCode("0023");//NO OPAS Information
				return;
			}
			
			if( medCaseList.size() == 0 ){
				logger.info("No OPAS Information");
				ticketGenResult.setInfoMessageCode("0021");//NO OPAS Information
				return;
			}else if ( medCaseList.size() > 1 ){
				logger.info("Multiple Appointment #0", medCaseList.size());						
				ticketGenResult.setInfoMessageCode("0022");//Multiple Appointment Exists
				return;
			}else {				
				MedCase medCase = medCaseList.get(0);
				
				List<MedOrder> medOrderList = (List<MedOrder>) em.createQuery(
						"select o from MedOrder o" + // 20120307 index check : MedCase.caseNum : I_MED_CASE_01
						" where o.medCase.caseNum = :caseNum" +
						" and o.orderType = :orderType" +
						" and o.status in :statusList" +
						" order by o.orderDate desc")
						.setParameter("caseNum", trimCaseNum)
						.setParameter("orderType", OrderType.OutPatient)
						.setParameter("statusList", MedOrderStatus.Outstanding_Withhold_PreVet)
						.setHint(QueryHints.FETCH, "o.medCase")
						.getResultList();
							
				if ( medOrderList.size() > 0 ) {
					MedOrder medOrder = medOrderList.get(0);
					medOrder.getMedCase();
					
					Pair<Boolean,Boolean> checkStdDrugClearanceResult = checkStdDrugClearance(medOrder, null, checkFcs);
					if ( !(Boolean)checkStdDrugClearanceResult.getFirst() ){
						return;
					}
					
					if( isFastQueueMedOrder(medOrder) ){
						caseNumTicketType = TicketType.FastQueue;
					}
					ticketGenResult.setOrderNumforDisplay(medOrder.getOrderNum());
					ticketGenResult.setTotalUnitOfCharge(this.retrieveTotalUnitOfChargeByMedOrder(medOrder));
				}else{	
					
					MedOrder medOrder = new MedOrder();
					medOrder.setPatHospCode(workstore.getDefPatHospCode());					
					medOrder.setMedCase(medCase);
					
					Pair<Boolean,Boolean> checkStdDrugClearanceResult = checkStdDrugClearance(medOrder, null, checkFcs);					
					if ( !(Boolean)checkStdDrugClearanceResult.getFirst() ){
						return;
					}
				}		
			}

		}

		generateAndPrintTicket(caseNumTicketType, null);
	}
	
	public void retrieveTicketByReprintTicketNum(String reprintTicketNum) {
		ticketGenResult = new TicketGenResult();
		if (reprintTicketNum.length() == 4 && TICKET_NUM_PATTERN.matcher(reprintTicketNum).find()) {
			Ticket ticketGenTicket = ticketManager.retrieveTicket(reprintTicketNum, getTicketDate());
			if( ticketGenTicket != null ){
				ticketGenResult.setTicket(ticketGenTicket);
				printTicketLabel(ticketGenTicket, true);
			}else{
				ticketGenResult.setInfoMessageCode(ERR_MSG_NOT_FOUND);
			}
		}
	}
	
	public void retrieveTicketByTicketType(TicketType ticketType) {	
		ticketGenResult = new TicketGenResult();
		generateAndPrintTicket(ticketType, null);
	}
	
	public void retrieveBatchTicket() {	

		int printCopies = 1;

		printCopies = propMap.getValueAsInt(TICKETGEN_BATCH_NOOFTICKET, 1);
		
		List<RenderAndPrintJob> printJobList = new ArrayList<RenderAndPrintJob> ();
		for (int x=1; x<=printCopies; x++) {
			
			Ticket ticketGenTicket = ticketManager.generateTicket( getTicketDate(), TicketType.Queue1, null, null);

			ticketGenResult = new TicketGenResult();
			if( ticketGenTicket != null ){
				convertTicketToPrintJob(printJobList, ticketGenTicket);
				ticketGenResult.setTicket(ticketGenTicket);
			}else{
				ticketGenResult.setErrorMessageCode("0361");
			}
		}
		printAgent.renderAndPrint(printJobList);
	}
	
	private void generateAndPrintTicket(TicketType ticketType, String inMoeOrderNum){
		Ticket ticketGenTicket = ticketManager.generateTicket( getTicketDate(), ticketType, null, inMoeOrderNum);
		if( ticketGenTicket != null ){
			printTicketLabel(ticketGenTicket, false);
			ticketGenResult.setTicket(ticketGenTicket);
		}else{
			ticketGenResult.setErrorMessageCode("0361");
		}
	}
		
	private Pair<Boolean, Boolean> checkStdDrugClearance( MedOrder medOrder, DispOrder dispOrder, boolean checkFcs){	
		boolean isDeleteOrder = false;

		if (medOrder.getDocType().equals(MedOrderDocType.Normal)&& medOrder.getStatus().equals(MedOrderStatus.Deleted)) {
			isDeleteOrder = true;
			ticketGenResult.setErrorMessageCode("0714");
			return new Pair<Boolean, Boolean>(false, isDeleteOrder);
		}
		
		if( !checkFcs ){
			ticketGenResult.setInfoMessageCode("0014");
			return new Pair<Boolean, Boolean>(false, isDeleteOrder);
		}
		
		ClearanceStatus stdDrugClearance = null;
		if( dispOrder != null ){
			stdDrugClearance = drugChargeClearanceManager.checkStdDrugClearanceByDispOrder(dispOrder);
		}else{
			stdDrugClearance = drugChargeClearanceManager.checkStdDrugClearanceByMedOrder(medOrder);
		}
		ticketGenResult.setStdDrugClearance(stdDrugClearance);
		
		if( !ClearanceStatus.PaymentClear.equals(stdDrugClearance) ){				
			if( ClearanceStatus.PaymentOutstanding.equals(stdDrugClearance) || ClearanceStatus.RecordNotFound.equals(stdDrugClearance)  ){					
				ticketGenResult.setInfoMessageCode("0015");
			}else{
				ticketGenResult.setInfoMessageCode("0014");
			}
			return new Pair<Boolean, Boolean>(false, isDeleteOrder);
		}
		return new Pair<Boolean, Boolean>(true, isDeleteOrder);
	}
	
	private Integer countChiLength(String chiChar) {
		Integer count = 0;
	    if (chiChar!=null){
			for (int i = 0; i < chiChar.length(); i++) {
		        int c = chiChar.charAt(i);
		        if (c > 0x7F) {
		        	count += 2;
		        } else {
		        	count += 1;
		        }
		    }
	    }
	    return count;
	}
	
	private void printTicketLabel(Ticket ticketIn, boolean fromReprint){
		TicketLabel ticketLabel = new TicketLabel();
		ticketLabel.setTicketNum(ticketIn.getTicketNum());
		ticketLabel.setHospCode(ticketIn.getWorkstore().getHospCode());
		ticketLabel.setWorkstoreCode(ticketIn.getWorkstore().getWorkstoreCode());
		ticketLabel.setCreateDate(ticketIn.getCreateDate());
		ticketLabel.setAddressEng(TICKETGEN_TICKET_PHARMLOCATIONENG.get());
		ticketLabel.setAddressChi(TICKETGEN_TICKET_PHARMLOCATIONCHI.get());
		ticketLabel.setAddressChiLength(countChiLength(ticketLabel.getAddressChi()));
		
		if( ISSUE_TICKETMATCH_ENABLED.get(false) ){
			ticketLabel.setDayOfWeek(new DateTime(ticketIn.getTicketDate()).getDayOfWeek());
			ticketLabel.setPrintDayOfWeekFlag(TICKETGEN_TICKET_TICKETDATE_DAYOFWEEK_VISIBLE.get(false));
			StringBuilder workstoreBarcodeSb = new StringBuilder(); 
			workstoreBarcodeSb.append(StringUtils.rightPad(ticketIn.getWorkstore().getHospCode(), 3))
							.append(StringUtils.rightPad(ticketIn.getWorkstore().getWorkstoreCode(), 4))
							.append(ticketLabelDateFormat.format(ticketIn.getTicketDate()))
							.append(ticketIn.getTicketNum());
			ticketLabel.setTicketMatchWorkstoreBarcode(workstoreBarcodeSb.toString());
		}
		
		String printLabelName = "TicketLabel";
		if( ISSUE_TICKETMATCH_ENABLED.get(false) ){
			printLabelName = "TicketMatchingTicketLabel";
		}
		if( ticketIn.getOrderNum() != null ){
			ticketLabel.setMoeOrderNum("MOE"+ticketIn.getOrderNum());
			ticketLabel.setRemark(TICKETGEN_TICKET_REMARK.get());
			ticketLabel.setRemarkChiLength(countChiLength(ticketLabel.getRemark()));
			printLabelName = "ExtendTicketLabel";
			
			if( ISSUE_TICKETMATCH_ENABLED.get(false) ){
				printLabelName = "TicketMatchingExtendTicketLabel";
			}
		}
		
		printAgent.renderAndPrint(new RenderAndPrintJob(
				printLabelName, 
				new PrintOption(),
				printerSelector.retrievePrinterSelect(PrintDocType.OtherLabel), 
				Arrays.asList(ticketLabel),
				"[TicketLabel] "+ 
				"TicketNum:" + ticketIn.getTicketNum()+
				", Reprint:"+fromReprint
		));
	}
	
	public void convertTicketToPrintJob(List<RenderAndPrintJob> printJobList, Ticket ticketIn){
		TicketLabel ticketLabel = new TicketLabel();
		ticketLabel.setTicketNum(ticketIn.getTicketNum());
		ticketLabel.setHospCode(ticketIn.getWorkstore().getHospCode());
		ticketLabel.setWorkstoreCode(ticketIn.getWorkstore().getWorkstoreCode());
		ticketLabel.setCreateDate(ticketIn.getCreateDate());
		ticketLabel.setAddressEng(TICKETGEN_TICKET_PHARMLOCATIONENG.get());
		ticketLabel.setAddressChi(TICKETGEN_TICKET_PHARMLOCATIONCHI.get());
		ticketLabel.setAddressChiLength(countChiLength(ticketLabel.getAddressChi()));
		
		if( ISSUE_TICKETMATCH_ENABLED.get(false) ){
			ticketLabel.setDayOfWeek(new DateTime(ticketIn.getTicketDate()).getDayOfWeek());
			ticketLabel.setPrintDayOfWeekFlag(TICKETGEN_TICKET_TICKETDATE_DAYOFWEEK_VISIBLE.get(false));
			StringBuilder workstoreBarcodeSb = new StringBuilder(); 
			workstoreBarcodeSb.append(StringUtils.rightPad(ticketIn.getWorkstore().getHospCode(), 3))
						.append(StringUtils.rightPad(ticketIn.getWorkstore().getWorkstoreCode(), 4))
						.append(ticketLabelDateFormat.format(ticketIn.getCreateDate()))
						.append(ticketIn.getTicketNum());
			ticketLabel.setTicketMatchWorkstoreBarcode(workstoreBarcodeSb.toString());
		}
		
		String printLabelName = "TicketLabel";
		if( ISSUE_TICKETMATCH_ENABLED.get(false) ){
			printLabelName = "TicketMatchingTicketLabel";
		}
		if( ticketIn.getOrderNum() != null ){
			ticketLabel.setMoeOrderNum("MOE"+ticketIn.getOrderNum());
			ticketLabel.setRemark(TICKETGEN_TICKET_REMARK.get());
			ticketLabel.setRemarkChiLength(countChiLength(ticketLabel.getRemark()));
			printLabelName = "ExtendTicketLabel";

			if( ISSUE_TICKETMATCH_ENABLED.get(false) ){
				printLabelName = "TicketMatchingExtendTicketLabel";
			}
		}
		
		printJobList.add(new RenderAndPrintJob(
				printLabelName, 
				new PrintOption(),
				printerSelector.retrievePrinterSelect(PrintDocType.OtherLabel), 
				Arrays.asList(ticketLabel),
				"[TicketLabel] "+ 
				"TicketNum:" + ticketIn.getTicketNum()
		));
	}

	private Date getTicketDate(){
		return new DateTime().toDate();
	}
	
	@Remove
	public void destroy() {
		if (ticket != null) {
			ticket = null;
		}
		
		if (ticketGenResult != null) {
			ticketGenResult = null;
		}
		
		if( errMsg != null ){
			errMsg = null;
		}
	}
}