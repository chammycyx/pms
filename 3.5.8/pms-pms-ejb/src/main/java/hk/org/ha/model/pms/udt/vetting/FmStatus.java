package hk.org.ha.model.pms.udt.vetting;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum FmStatus implements StringValuedEnum {

	SelfFinancedItemC("C","Self-financed item"),
	SelfFinancedItemD("D","Self-financed item"),
	SafetyNetItem("M","Self-financed item with Safety Net"),
	UnRegisteredDrug("N","Unregistered Drug:Named Pat Only"),
	SampleItem("S","Sample"),
	SpecialDrug("X","Special Drug"),
	GeneralDrug("F","General Drug"),
	FreeTextEntryItem("T", "Free Text Entry Item");

	private final String dataValue;
	private final String displayValue;
	
	FmStatus (final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}

	public static FmStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(FmStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<FmStatus> {

		private static final long serialVersionUID = 1L;

		public Class<FmStatus> getEnumClass() {
    		return FmStatus.class;
    	}
    }
}
