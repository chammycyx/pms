package hk.org.ha.model.pms.vo.alert.mds;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedProperty;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "AlertDrcm")
@ExternalizedBean(type=DefaultExternalizer.class)
public class AlertDrcm implements Alert, Serializable {

	private static final long serialVersionUID = 1L;
	
	private static final int ADDITIVE_NUM_MULTIPLIER = 10000;
	
	private static final int MANUAL_ITEM_MULTIPLIER = 10000;
	
	private static final int MANUAL_ITEM_RANGE = 100000000; //100010000

	@XmlElement(name="patHospCode", required = true)
	private String patHospCode;
	
	@XmlElement(name="ordNo", required = true)
	private Long ordNo;
	
	@XmlElement(name="ItemNum", required = true)
	private Long itemNum;

	@XmlElement(name="DrcStatus", required = true)
	private String drcStatus;

	@XmlElement(name="MinDoseMsg")
	private String minDoseMsg;
	
	@XmlElement(name="MaxDoseMsg")
	private String maxDoseMsg;
	
	@XmlElement(name="DailyMaxMsg")
	private String dailyMaxMsg;
	
	@XmlElement(name="DoseMaxMsg")
	private String doseMaxMsg;

	@XmlElement(name="MaxFrequencyMsg")
	private String maxFrequencyMsg;
	
	@XmlElement(name="MaxDurationMsg")
	private String maxDurationMsg;
	
	@XmlTransient
	private Long sortSeq;
	
	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public Long getOrdNo() {
		return ordNo;
	}

	public void setOrdNo(Long ordNo) {
		this.ordNo = ordNo;
	}

	public Long getItemNum() {
		return itemNum;
	}

	public void setItemNum(Long itemNum) {
		this.itemNum = itemNum;
	}

	public String getDrcStatus() {
		return drcStatus;
	}

	public void setDrcStatus(String drcStatus) {
		this.drcStatus = drcStatus;
	}

	public String getMinDoseMsg() {
		return minDoseMsg;
	}

	public void setMinDoseMsg(String minDoseMsg) {
		this.minDoseMsg = minDoseMsg;
	}

	public String getMaxDoseMsg() {
		return maxDoseMsg;
	}

	public void setMaxDoseMsg(String maxDoseMsg) {
		this.maxDoseMsg = maxDoseMsg;
	}

	public String getDailyMaxMsg() {
		return dailyMaxMsg;
	}

	public void setDailyMaxMsg(String dailyMaxMsg) {
		this.dailyMaxMsg = dailyMaxMsg;
	}

	public String getDoseMaxMsg() {
		return doseMaxMsg;
	}

	public void setDoseMaxMsg(String doseMaxMsg) {
		this.doseMaxMsg = doseMaxMsg;
	}

	public String getMaxFrequencyMsg() {
		return maxFrequencyMsg;
	}

	public void setMaxFrequencyMsg(String maxFrequencyMsg) {
		this.maxFrequencyMsg = maxFrequencyMsg;
	}

	public String getMaxDurationMsg() {
		return maxDurationMsg;
	}

	public void setMaxDurationMsg(String maxDurationMsg) {
		this.maxDurationMsg = maxDurationMsg;
	}

	public Long getSortSeq() {
		return sortSeq;
	}

	public void setSortSeq(Long sortSeq) {
		this.sortSeq = sortSeq;
	}

	@ExternalizedProperty
	public String getAlertTitle() {
		return "Dosage Range Checking";
	}
	
	@ExternalizedProperty
	public String getAlertDesc() {

		StringBuilder sb = new StringBuilder();

		if (StringUtils.isNotBlank(minDoseMsg)) {
			sb.append(minDoseMsg);
		}

		if (StringUtils.isNotBlank(maxDoseMsg)) {
			sb.append(maxDoseMsg);
		}

		if (StringUtils.isNotBlank(doseMaxMsg)) {
			sb.append(doseMaxMsg);
		}

		if (StringUtils.isNotBlank(dailyMaxMsg)) {
			sb.append(dailyMaxMsg);
		}

		if (StringUtils.isNotBlank(maxFrequencyMsg)) {
			sb.append(maxFrequencyMsg);
		}

		if (StringUtils.isNotBlank(maxDurationMsg)) {
			sb.append(maxDurationMsg);
		}

		return sb.toString();
	}
	
	public boolean equals(Object object) {
		
		if (!(object instanceof AlertDrcm)) {
			return false;
		}
		if (this == object) {
			return true;
		}

		AlertDrcm otherAlert = (AlertDrcm) object;
		
		return new EqualsBuilder()
			.append(StringUtils.trimToNull(StringUtils.lowerCase(patHospCode)),        StringUtils.trimToNull(StringUtils.lowerCase(otherAlert.patHospCode)))
			.append(ordNo,                                                             otherAlert.ordNo)
			.append(itemNum,                                                           otherAlert.itemNum)
			.append(StringUtils.trimToNull(StringUtils.lowerCase(drcStatus)),          StringUtils.trimToNull(StringUtils.lowerCase(otherAlert.drcStatus)))
			.append(StringUtils.trimToNull(StringUtils.lowerCase(minDoseMsg)),         StringUtils.trimToNull(StringUtils.lowerCase(otherAlert.minDoseMsg)))
			.append(StringUtils.trimToNull(StringUtils.lowerCase(maxDoseMsg)),         StringUtils.trimToNull(StringUtils.lowerCase(otherAlert.maxDoseMsg)))
			.append(StringUtils.trimToNull(StringUtils.lowerCase(dailyMaxMsg)),        StringUtils.trimToNull(StringUtils.lowerCase(otherAlert.dailyMaxMsg)))
			.append(StringUtils.trimToNull(StringUtils.lowerCase(doseMaxMsg)),         StringUtils.trimToNull(StringUtils.lowerCase(otherAlert.doseMaxMsg)))
			.append(StringUtils.trimToNull(StringUtils.lowerCase(maxFrequencyMsg)),    StringUtils.trimToNull(StringUtils.lowerCase(otherAlert.maxFrequencyMsg)))
			.append(StringUtils.trimToNull(StringUtils.lowerCase(maxDurationMsg)),     StringUtils.trimToNull(StringUtils.lowerCase(otherAlert.maxDurationMsg)))
			.isEquals();
	}
	
	public int hashCode() {
		return new HashCodeBuilder()
			.append(StringUtils.trimToNull(StringUtils.lowerCase(patHospCode)))
			.append(ordNo)
			.append(itemNum)
			.append(StringUtils.trimToNull(StringUtils.lowerCase(drcStatus)))
			.append(StringUtils.trimToNull(StringUtils.lowerCase(minDoseMsg)))
			.append(StringUtils.trimToNull(StringUtils.lowerCase(maxDoseMsg)))
			.append(StringUtils.trimToNull(StringUtils.lowerCase(dailyMaxMsg)))
			.append(StringUtils.trimToNull(StringUtils.lowerCase(doseMaxMsg)))
			.append(StringUtils.trimToNull(StringUtils.lowerCase(maxFrequencyMsg)))
			.append(StringUtils.trimToNull(StringUtils.lowerCase(maxDurationMsg)))
			.toHashCode();
	}

	public void loadDrugDesc(String drugDesc) {
	}

	public void replaceItemNum(Integer oldItemNum, Integer newItemNum, String orderNum) {
		itemNum = Long.valueOf(newItemNum.intValue());
	}

	public void replaceOrderNum(String oldOrderNum, String newOrderNum) {
		patHospCode = StringUtils.trimToNull(newOrderNum.substring(0, 3));
		ordNo = Long.valueOf(newOrderNum.substring(3));		
	}

	public void restoreItemNum() {
		if( itemNum.longValue() > MANUAL_ITEM_RANGE ){
			itemNum = Long.valueOf(itemNum.longValue() / MANUAL_ITEM_MULTIPLIER);
		}else if (itemNum.longValue() >= ADDITIVE_NUM_MULTIPLIER) {
			itemNum = Long.valueOf(itemNum.longValue() / ADDITIVE_NUM_MULTIPLIER);
		}
	}

	@ExternalizedProperty
	public String getKeys() {
		return null;
	}
}