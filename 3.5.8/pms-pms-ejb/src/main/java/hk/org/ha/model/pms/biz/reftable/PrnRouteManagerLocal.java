package hk.org.ha.model.pms.biz.reftable;

import java.util.List;

import hk.org.ha.model.pms.persistence.corp.PrnRoute;
import hk.org.ha.model.pms.persistence.corp.Workstore;

import javax.ejb.Local;

@Local
public interface PrnRouteManagerLocal {

	List<PrnRoute> getPrnRouteListByWorkstore(Workstore workstore);	
}
