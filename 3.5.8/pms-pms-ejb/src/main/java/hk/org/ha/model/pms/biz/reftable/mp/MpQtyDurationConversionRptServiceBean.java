package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.PmsIpDurationConv;
import hk.org.ha.model.pms.dms.persistence.PmsIpQtyConv;
import hk.org.ha.model.pms.dms.persistence.PmsQtyConversionInsu;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.reftable.DurationUnit;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsRptServiceJmsRemote;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("mpQtyDurationConversionRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MpQtyDurationConversionRptServiceBean implements MpQtyDurationConversionRptServiceLocal {
	
	@In
	private Workstore workstore;

	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
    private DmsRptServiceJmsRemote dmsRptServiceProxy;

	@In
	private PrintAgentInf printAgent;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	private List<PmsIpQtyConv> qtyConversionRptList;
	
	private List<PmsIpDurationConv> durationConversionRptList;
	
	private List<PmsQtyConversionInsu> qtyConversionInsuRptList;
	
	private boolean qtyRetrieveSuccess;
	
	private boolean durationRetrieveSuccess;
	
	private boolean qtyInsuRetrieveSuccess;

	private boolean qtyExportSuccess;

	private boolean durExportSuccess;
	
	private boolean qtyInsuExportSuccess;

	public List<PmsIpQtyConv> retrieveMpQtyConversionListForReport()
	{
		List<PmsIpQtyConv> qtyConversionList = dmsRptServiceProxy.retrievePmsIpQtyConvListForReport (
				workstore.getHospCode(),workstore.getWorkstoreCode());
		
		DmDrug printDmDrug;
		for (PmsIpQtyConv pmsIpQtyConv : qtyConversionList) {
			printDmDrug = dmDrugCacher.getDmDrug(pmsIpQtyConv.getItemCode());
			if ( printDmDrug != null ) {
				pmsIpQtyConv.setItemDesc(printDmDrug.getFullDrugDesc());	
			}
		}
		
		return qtyConversionList;
	}
	
	public List<PmsIpDurationConv> retrieveMpDurationConversionListForReport() 
	{
		List<PmsIpDurationConv> durationConversionList = dmsRptServiceProxy.retrievePmsIpDurationConvListForReport (
				workstore.getHospCode(),workstore.getWorkstoreCode());
		
		DmDrug printDmDrug;
		for (PmsIpDurationConv pmsIpDurationConv : durationConversionList) {
			printDmDrug = dmDrugCacher.getDmDrug(pmsIpDurationConv.getItemCode());
			
			if ( printDmDrug != null ) {
				pmsIpDurationConv.setItemDesc(printDmDrug.getFullDrugDesc());	
			}
			pmsIpDurationConv.setDurationUnitDesc( (DurationUnit.dataValueOf(pmsIpDurationConv.getDurationUnit())).getDisplayValue() );
		}
		
		return durationConversionList;
	}
	
	public List<PmsQtyConversionInsu> retrievePmsQtyConversionInsuListForReport()
	{
		List<PmsQtyConversionInsu> qtyConversionInsuList = dmsRptServiceProxy.retrievePmsQtyConversionInsuListForReport (
				workstore.getHospCode(),workstore.getWorkstoreCode());
		
		DmDrug printDmDrug;
		for (PmsQtyConversionInsu qtyConversionInsu : qtyConversionInsuList) {
			printDmDrug = dmDrugCacher.getDmDrug(qtyConversionInsu.getItemCode());
			if ( printDmDrug != null ) {
				qtyConversionInsu.setItemDesc(printDmDrug.getFullDrugDesc());	
			}
		}
		
		return qtyConversionInsuList;
	}
	
	public void printMpQtyDurationConversionRpt() {
		printMpQtyConversionRpt();
		printMpDurationConversionRpt();
		printQtyConversionInsuRpt();
	}
	
	private void printMpQtyConversionRpt() {
		qtyConversionRptList = retrieveMpQtyConversionListForReport();
		
		if (!qtyConversionRptList.isEmpty()) {
			qtyRetrieveSuccess = true; 
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("hospCode", workstore.getHospCode());
			
		    printAgent.renderAndPrint(new RenderAndPrintJob(
			    "QtyConversionRpt",
			    new PrintOption(),
			    printerSelector.retrievePrinterSelect(PrintDocType.Report),
			    parameters,
			    qtyConversionRptList));		
		}else {
			qtyRetrieveSuccess = false;
		}
	}
	
	private void printMpDurationConversionRpt() {
		durationConversionRptList = retrieveMpDurationConversionListForReport();
		
		if (!durationConversionRptList.isEmpty()) {
			durationRetrieveSuccess = true;
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("hospCode", workstore.getHospCode());
			
		    printAgent.renderAndPrint(new RenderAndPrintJob(
			    "DurationConversionRpt",
			    new PrintOption(),
			    printerSelector.retrievePrinterSelect(PrintDocType.Report),
			    parameters,
			    durationConversionRptList));
		}else {
			durationRetrieveSuccess = false;
		}
	}
	
	private void printQtyConversionInsuRpt() {
		qtyConversionInsuRptList = retrievePmsQtyConversionInsuListForReport();
		
		if (!qtyConversionInsuRptList.isEmpty()) {
			qtyInsuRetrieveSuccess = true; 
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("hospCode", workstore.getHospCode());
			
		    printAgent.renderAndPrint(new RenderAndPrintJob(
			    "QtyConversionInsuRpt",
			    new PrintOption(),
			    printerSelector.retrievePrinterSelect(PrintDocType.Report),
			    parameters,
			    qtyConversionInsuRptList));		
		}else {
			qtyInsuRetrieveSuccess = false;
		}
	}
	
	public void retrieveMpQtyConversionRptList() {
		qtyConversionRptList = retrieveMpQtyConversionListForReport();

		if (!qtyConversionRptList.isEmpty()) {
			qtyExportSuccess = true;
		} else {
			qtyExportSuccess = false;
		}
	}
	
	public void exportMpQtyConversionRpt() {
		if (!qtyConversionRptList.isEmpty()) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("hospCode", workstore.getHospCode());
			
			PrintOption po = new PrintOption();
			po.setDocType(ReportProvider.XLS);
			printAgent.renderAndRedirect(new RenderJob(
			    "QtyConversionXls",
			    po,
			    parameters, 
			    qtyConversionRptList));		
		}
	}
	
	public void retrieveMpDurConversionRptList() {
		durationConversionRptList = retrieveMpDurationConversionListForReport();
		
		if (!durationConversionRptList.isEmpty()) {
			durExportSuccess = true;
		} else {
			durExportSuccess = false;
		}
	}
	
	public void exportMpDurationConversionRpt() {
		if (!durationConversionRptList.isEmpty()) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("hospCode", workstore.getHospCode());
			
			PrintOption po = new PrintOption();
			po.setDocType(ReportProvider.XLS);
			printAgent.renderAndRedirect(new RenderJob(
			    "DurationConversionXls",
			    po,
			    parameters, 
			    durationConversionRptList));		
		} 
	}
	
	public void retrieveQtyConversionInsuRptList() {
		qtyConversionInsuRptList = retrievePmsQtyConversionInsuListForReport();
		
		if (!qtyConversionInsuRptList.isEmpty()) {
			qtyInsuExportSuccess = true;
		} else {
			qtyInsuExportSuccess = false;
		}
	}
	
	public void exportPmsQtyConversionInsuRpt() {
		if (!qtyConversionInsuRptList.isEmpty()) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("hospCode", workstore.getHospCode());
			
			PrintOption po = new PrintOption();
			po.setDocType(ReportProvider.XLS);
			printAgent.renderAndRedirect(new RenderJob(
			    "QtyConversionInsuXls",
			    po,
			    parameters, 
			    qtyConversionInsuRptList));		
		}
	}

	public boolean isQtyRetrieveSuccess() {
		return qtyRetrieveSuccess;
	}
	
	public boolean isDurationRetrieveSuccess() {
		return durationRetrieveSuccess;
	}
	
	public boolean isQtyInsuRetrieveSuccess() {
		return qtyInsuRetrieveSuccess;
	}
	
	public boolean isQtyExportSuccess() {
		return qtyExportSuccess;
	}
	
	public boolean isDurExportSuccess() {
		return durExportSuccess;
	}
	
	public boolean isQtyInsuExportSuccess() {
		return qtyInsuExportSuccess;
	}

	@Remove
	public void destroy() 
	{
	}

}
