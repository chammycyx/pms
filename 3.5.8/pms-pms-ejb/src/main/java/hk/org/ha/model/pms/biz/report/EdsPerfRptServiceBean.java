package hk.org.ha.model.pms.biz.report;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.EdsPerfRpt;
import hk.org.ha.model.pms.vo.report.EdsPerfRptDtl;
import hk.org.ha.model.pms.vo.report.EdsPerfRptHdr;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("edsPerfRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class EdsPerfRptServiceBean implements EdsPerfRptServiceLocal {
	@PersistenceContext
	private EntityManager em;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent; 
	
	@In 
	private Workstore workstore;

	private boolean retrieveSuccess;
	
	private List<EdsPerfRpt> edsPerfRptList;
	
	private String currentRpt = null;
	
	private List<?> reportObj = null;
	
	@In 
	private EdsStatusSummaryRptManagerLocal edsStatusSummaryRptManager;
	

	public void retrieveEdsPerfRpt(Integer selectedTab) {
		reportObj = retrieveEdsPerfRptList(new Date());
		
		if(selectedTab == 0){
			currentRpt = "EdsPerfChart";
		}
		else if(selectedTab == 1){
			currentRpt = "EdsPerfRpt";	
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<EdsPerfRpt> retrieveEdsPerfRptList(Date today) {
		edsPerfRptList = new ArrayList<EdsPerfRpt>();
	
		//retrieve order and item List
		Map<DispOrderItemStatus, Long> dispOrderItemStatusMap = edsStatusSummaryRptManager.retrieveDispOrderItemStatusCount(today);		
		Map<DispOrderStatus, Long> dispOrderStatusMap = edsStatusSummaryRptManager.retrieveDispOrderStatusCount(today);

		EdsPerfRpt edsPerfRpt = new EdsPerfRpt();
		
		EdsPerfRptHdr edsPerfRptHdr = new EdsPerfRptHdr();
		OperationProfile op = em.find(OperationProfile.class, workstore.getOperationProfileId());
		edsPerfRptHdr.setActiveProfileName(op.getName());
		edsPerfRptHdr.setHospCode(workstore.getHospCode());
		edsPerfRptHdr.setWorkstoreCode(workstore.getWorkstoreCode());
		edsPerfRpt.setEdsPerfRptHdr(edsPerfRptHdr);
		
		
		EdsPerfRptDtl edsPerfRptDtl = new EdsPerfRptDtl();
		edsPerfRptDtl.setPickCount(dispOrderItemStatusMap.get(DispOrderItemStatus.Vetted).intValue());
		edsPerfRptDtl.setAssembleCount(dispOrderItemStatusMap.get(DispOrderItemStatus.Picked).intValue());
		edsPerfRptDtl.setCheckCount(dispOrderStatusMap.get(DispOrderStatus.Assembled).intValue());
		edsPerfRptDtl.setIssueCount(dispOrderStatusMap.get(DispOrderStatus.Checked).intValue());
		
		//retrieve suspend order
		List<DispOrder> dispOrderList = em.createQuery(
				"select o from DispOrder o" + // 20120312 index check : DispOrder.workstore,dispDate : I_DISP_ORDER_03
				" where o.workstore = :workstore" +
				" and o.dispDate = :today" +
				" and o.adminStatus = :adminStatus" +
				" and o.status not in :status" +
				" and o.ticket.orderType = :orderType")
				.setParameter("workstore", workstore)
				.setParameter("today", today, TemporalType.DATE)
				.setParameter("adminStatus", DispOrderAdminStatus.Suspended)
				.setParameter("status", DispOrderStatus.Deleted_SysDeleted)
				.setParameter("orderType", OrderType.OutPatient)
				.getResultList();

		edsPerfRptDtl.setSuspendCount(dispOrderList.size());
		
		edsPerfRpt.setEdsPerfRptDtl(edsPerfRptDtl);
		edsPerfRptList.add(edsPerfRpt);

		if ( !edsPerfRptList.isEmpty() ) {
			retrieveSuccess = true;
		}
		else{
			retrieveSuccess = false;
		}
		return edsPerfRptList;
	}
	
	public void generateEdsPerfRpt() {

		Map<String, Object> parameters = new HashMap<String, Object>();
		if ("EdsPerfChart".equals(currentRpt)) {
			parameters.put("_PNG_ZOOM_RATIO", Float.valueOf(1f));
			printAgent.renderAndRedirect(new RenderJob(
					currentRpt + "Preview", 
					new PrintOption(), 
					parameters, 
					reportObj));
		} else {
			printAgent.renderAndRedirect(new RenderJob(
					currentRpt, 
					new PrintOption(), 
					parameters, 
					reportObj));
		}
	}
	
	public void printEdsPerfRpt(){

		printAgent.renderAndPrint(new RenderAndPrintJob(
				currentRpt, 
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				new HashMap<String, Object>(), 
				reportObj));
	}

	@Remove
	public void destroy() {
		if(edsPerfRptList!=null){
			edsPerfRptList=null;
		}
	}

	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}
	
}
