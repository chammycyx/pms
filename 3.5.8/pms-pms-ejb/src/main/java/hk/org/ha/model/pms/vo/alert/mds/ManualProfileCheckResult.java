package hk.org.ha.model.pms.vo.alert.mds;

import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedProperty;

@ExternalizedBean(type=DefaultExternalizer.class)
public class ManualProfileCheckResult implements Serializable {

	private static final long serialVersionUID = 1L;

	private Map<MedProfileMoItem, List<AlertMsg>> map;
	
	private List<ErrorInfo> errorInfoList;
	
	private Boolean medProfileSaveFlag = Boolean.FALSE;
	
	private Boolean includeCrossDdiFlag = Boolean.FALSE;
	
	public ManualProfileCheckResult() {
		map = new LinkedHashMap<MedProfileMoItem, List<AlertMsg>>();
	}

	public ManualProfileCheckResult(Map<MedProfileMoItem, List<AlertMsg>> map, boolean crossDdiFlag){
		this.map = map;
		this.includeCrossDdiFlag = crossDdiFlag;
		this.medProfileSaveFlag = true;
	}
	
	public Map<MedProfileMoItem, List<AlertMsg>> getMap() {
		return map;
	}

	public void setMap(Map<MedProfileMoItem, List<AlertMsg>> map) {
		this.map = map;
	}

	public void put(MedProfileMoItem medProfileMoItem, List<AlertMsg> alertMsgs) {
		map.put(medProfileMoItem, alertMsgs);
	}
	
	public void put(MedProfileMoItem medProfileMoItem, AlertMsg alertMsg) {
		map.put(medProfileMoItem, Arrays.asList(alertMsg));
	}
	
	@ExternalizedProperty
	public List<MedProfileMoItem> getMedProfileMoItemList() {
		return new ArrayList<MedProfileMoItem>(map.keySet());
	}

	public List<ErrorInfo> getErrorInfoList() {
		return errorInfoList;
	}

	public void setErrorInfoList(List<ErrorInfo> errorInfoList) {
		this.errorInfoList = errorInfoList;
	}

	public Boolean getMedProfileSaveFlag() {
		return medProfileSaveFlag;
	}

	public void setMedProfileSaveFlag(Boolean medProfileSaveFlag) {
		this.medProfileSaveFlag = medProfileSaveFlag;
	}

	public Boolean getIncludeCrossDdiFlag() {
		return includeCrossDdiFlag;
	}

	public void setIncludeCrossDdiFlag(Boolean includeCrossDdiFlag) {
		this.includeCrossDdiFlag = includeCrossDdiFlag;
	}
}

