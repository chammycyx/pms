package hk.org.ha.model.pms.biz.reftable.mp;

import javax.ejb.Local;

@Local
public interface MpItemRptServiceLocal {

	void genMpItemSpecListRpt();

	void exportMpItemSpecListRpt();
	
	void genMpItemPropertyListRpt();

	void exportMpItemPropertyListRpt();
	
	void genMpFdnMappingListRpt();
	
	void exportMpFdnMappingListRpt();
	
	void destroy();
	
}
