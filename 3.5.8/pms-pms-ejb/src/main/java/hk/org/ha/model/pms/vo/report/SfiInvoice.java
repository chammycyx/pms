package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class SfiInvoice implements Serializable {

	private static final long serialVersionUID = 1L;

	private SfiInvoiceHdr sfiInvoiceHdr;
	
	private List<SfiInvoiceDtl> sfiInvoiceDtlList;

	private SfiInvoiceFtr sfiInvoiceFtr;

	public SfiInvoiceHdr getSfiInvoiceHdr() {
		return sfiInvoiceHdr;
	}

	public void setSfiInvoiceHdr(SfiInvoiceHdr sfiInvoiceHdr) {
		this.sfiInvoiceHdr = sfiInvoiceHdr;
	}

	public List<SfiInvoiceDtl> getSfiInvoiceDtlList() {
		return sfiInvoiceDtlList;
	}

	public void setSfiInvoiceDtlList(List<SfiInvoiceDtl> sfiInvoiceDtlList) {
		this.sfiInvoiceDtlList = sfiInvoiceDtlList;
	}

	public SfiInvoiceFtr getSfiInvoiceFtr() {
		return sfiInvoiceFtr;
	}

	public void setSfiInvoiceFtr(SfiInvoiceFtr sfiInvoiceFtr) {
		this.sfiInvoiceFtr = sfiInvoiceFtr;
	}


}
