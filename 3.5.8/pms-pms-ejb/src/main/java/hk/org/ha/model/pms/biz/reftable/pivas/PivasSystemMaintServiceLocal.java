package hk.org.ha.model.pms.biz.reftable.pivas;

import hk.org.ha.model.pms.vo.reftable.pivas.PivasSystemMaintInfo;
import hk.org.ha.model.pms.vo.reftable.pivas.PivasSystemMaintUpdateSummary;

import javax.ejb.Local;

@Local
public interface PivasSystemMaintServiceLocal {
	
	void destroy();
	
	PivasSystemMaintInfo retrievePivasSystemMaintInfo(String year);
	
	PivasSystemMaintInfo updatePivasSystemMaint(PivasSystemMaintUpdateSummary pivasSystemMaintUpdateSummary);
	
}
 