package hk.org.ha.model.pms.vo.reftable.pivas;

import hk.org.ha.model.pms.dms.persistence.pivas.PivasContainer;
import hk.org.ha.model.pms.persistence.reftable.pivas.PivasHoliday;
import hk.org.ha.model.pms.persistence.reftable.pivas.PivasServiceHour;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PivasSystemMaintInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<PivasHoliday> pivasHolidayList;
	
	private List<PivasServiceHour> pivasServiceHourList;
	
	private List<PivasContainer> pivasContainerList;

	public List<PivasHoliday> getPivasHolidayList() {
		return pivasHolidayList;
	}

	public void setPivasHolidayList(List<PivasHoliday> pivasHolidayList) {
		this.pivasHolidayList = pivasHolidayList;
	}

	public List<PivasServiceHour> getPivasServiceHourList() {
		return pivasServiceHourList;
	}

	public void setPivasServiceHourList(List<PivasServiceHour> pivasServiceHourList) {
		this.pivasServiceHourList = pivasServiceHourList;
	}

	public void setPivasContainerList(List<PivasContainer> pivasContainerList) {
		this.pivasContainerList = pivasContainerList;
	}

	public List<PivasContainer> getPivasContainerList() {
		return pivasContainerList;
	}
	
}