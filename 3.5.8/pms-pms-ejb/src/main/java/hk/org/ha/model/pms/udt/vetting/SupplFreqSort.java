package hk.org.ha.model.pms.udt.vetting;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum SupplFreqSort implements StringValuedEnum{
	Rank("rank", "Sort by rank"),
	SupplFreqDesc("supplFreqDesc", "Sort by supplementary frequency description");
	
	private final String dataValue;
	private final String displayValue;
	
	SupplFreqSort (final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}

	public static SupplFreqSort dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(SupplFreqSort.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<SupplFreqSort> {

		private static final long serialVersionUID = 1L;

		public Class<SupplFreqSort> getEnumClass() {
    		return SupplFreqSort.class;
    	}
    }
}
