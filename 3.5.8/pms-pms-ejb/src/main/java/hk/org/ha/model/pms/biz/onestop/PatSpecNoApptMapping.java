package hk.org.ha.model.pms.biz.onestop;

import javax.ejb.Remove;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.vo.onestop.RequireAppointmentRule;

import org.drools.RuleBase;
import org.drools.StatelessSession;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Scope(ScopeType.APPLICATION)
@Name("patSpecNoApptMapping")
@Restrict("#{identity.loggedIn}")
@MeasureCalls
public class PatSpecNoApptMapping implements PatSpecNoApptMappingInf {
	
	@In(create=true)
	private RuleBase patSpecApptRuleBase;
	
	private transient StatelessSession patSpecApptMemory = null;
	
	@Create
	public void create() {
		if ( patSpecApptRuleBase != null) {
			patSpecApptMemory = patSpecApptRuleBase.newStatelessSession();
		}
	}
	
	public Boolean checkPatSpecApptRequire(RequireAppointmentRule requireAppointmentRule) {
		if ( patSpecApptMemory != null){
			patSpecApptMemory.execute(requireAppointmentRule);
		}
		
		return requireAppointmentRule.getNoAppointmentRequired();
	}
	
	@Remove
	public void destroy() 
	{
		if (patSpecApptMemory != null) {
			patSpecApptMemory = null;
		}
	}
}
