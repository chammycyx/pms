package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.vo.DrugRouteCriteria;
import hk.org.ha.model.pms.vo.rx.Site;

import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("drugRouteService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DrugRouteServiceBean implements DrugRouteServiceLocal {

	@In
	private DrugRouteManagerLocal drugRouteManager;
	
	@Out(required = false)
	private List<Map<String, Object>> ipSiteMapList;
	
	public List<Site> retrieveIpSiteListByItemCode(String itemCode) {
		return drugRouteManager.retrieveIpSiteListByItemCode(itemCode);
	}

	public List<Site> retrieveManualEntrySiteListByItemCode(String itemCode) {
		return drugRouteManager.retrieveManualEntrySiteListByItemCode(itemCode);
	}
	
	public void retrieveIpOthersSiteMapListByDrugRouteCriteria(DrugRouteCriteria criteria) {
		ipSiteMapList = drugRouteManager.retrieveIpOthersSiteMapListByDrugRouteCriteria(criteria);
	}
	
	public void retrieveManualEntryOthersSiteMapListByDrugRouteCriteria(DrugRouteCriteria criteria) {
		ipSiteMapList = drugRouteManager.retrieveManualEntryOthersSiteMapListByDrugRouteCriteria(criteria);
	}

	@Remove
	public void destroy() {
		if (ipSiteMapList != null) {
			ipSiteMapList = null;
		}
	}
}
