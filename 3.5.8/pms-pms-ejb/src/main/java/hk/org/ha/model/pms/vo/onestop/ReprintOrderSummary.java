package hk.org.ha.model.pms.vo.onestop;

import hk.org.ha.model.pms.persistence.disp.CapdVoucher;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.persistence.disp.Ticket;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class ReprintOrderSummary implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private List<DispOrderItem> reprintDispOrderItemList;
	
	private List<RefillSchedule> reprintRefillScheduleList;
	
	private List<RefillSchedule> reprintSfiRefillScheduleItemList;
	
	private List<CapdVoucher> reprintCapdVoucherList;
	
	private MedOrder reprintMedOrder;
	
	private MedCase reprintMedCase;
	
	private Ticket reprintTicket;
	
	private Patient reprintPatient;
	
	private RefillSchedule orignalRefillSchedule;
	
	private String doctorCode;
	
	private String doctorName;
	
	private Invoice sfiInvoice;
	
	private Boolean triggerByOtherScreen;
	
	private Boolean isNoLabelItem;
	
	private Boolean isDhOutdatedOrder;
	
	public ReprintOrderSummary()
	{
		isDhOutdatedOrder = false;
	}
	
	public List<DispOrderItem> getReprintDispOrderItemList() {
		return reprintDispOrderItemList;
	}

	public void setReprintDispOrderItemList(
			List<DispOrderItem> reprintDispOrderItemList) {
		this.reprintDispOrderItemList = reprintDispOrderItemList;
	}

	public List<RefillSchedule> getReprintRefillScheduleList() {
		return reprintRefillScheduleList;
	}

	public void setReprintRefillScheduleList(List<RefillSchedule> reprintRefillScheduleList) {
		this.reprintRefillScheduleList = reprintRefillScheduleList;
	}
	
	public List<RefillSchedule> getReprintSfiRefillScheduleItemList() {
		return reprintSfiRefillScheduleItemList;
	}

	public void setReprintSfiRefillScheduleItemList(List<RefillSchedule> reprintSfiRefillScheduleItemList) {
		this.reprintSfiRefillScheduleItemList = reprintSfiRefillScheduleItemList;
	}
	
	public void addReprintSfiRefillScheduleList(List<RefillSchedule> reprintSfiRefillScheduleItemList) {
		if (this.reprintSfiRefillScheduleItemList == null) {
			this.reprintSfiRefillScheduleItemList = new ArrayList<RefillSchedule>();
		}
		this.reprintSfiRefillScheduleItemList.addAll(reprintSfiRefillScheduleItemList);
	}

	public List<CapdVoucher> getReprintCapdVoucherList() {
		return reprintCapdVoucherList;
	}

	public void setReprintCapdVoucherList(List<CapdVoucher> reprintCapdVoucherList) {
		this.reprintCapdVoucherList = reprintCapdVoucherList;
	}

	public MedOrder getReprintMedOrder() {
		return reprintMedOrder;
	}

	public void setReprintMedOrder(MedOrder reprintMedOrder) {
		this.reprintMedOrder = reprintMedOrder;
	}

	public MedCase getReprintMedCase() {
		return reprintMedCase;
	}

	public void setReprintMedCase(MedCase reprintMedCase) {
		this.reprintMedCase = reprintMedCase;
	}

	public Ticket getReprintTicket() {
		return reprintTicket;
	}

	public void setReprintTicket(Ticket reprintTicket) {
		this.reprintTicket = reprintTicket;
	}

	public Patient getReprintPatient() {
		return reprintPatient;
	}

	public void setReprintPatient(Patient reprintPatient) {
		this.reprintPatient = reprintPatient;
	}

	public RefillSchedule getOrignalRefillSchedule() {
		return orignalRefillSchedule;
	}

	public void setOrignalRefillSchedule(RefillSchedule orignalRefillSchedule) {
		this.orignalRefillSchedule = orignalRefillSchedule;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public void setSfiInvoice(Invoice sfiInvoice) {
		this.sfiInvoice = sfiInvoice;
	}

	public Invoice getSfiInvoice() {
		return sfiInvoice;
	}

	public Boolean getTriggerByOtherScreen() {
		return triggerByOtherScreen;
	}

	public void setTriggerByOtherScreen(Boolean triggerByOtherScreen) {
		this.triggerByOtherScreen = triggerByOtherScreen;
	}

	public void setIsNoLabelItem(Boolean isNoLabelItem) {
		this.isNoLabelItem = isNoLabelItem;
	}

	public Boolean getIsNoLabelItem() {
		return isNoLabelItem;
	}
	
	public Boolean getIsDhOutdatedOrder() {
		return isDhOutdatedOrder;
	}

	public void setIsDhOutdatedOrder(Boolean isDhOutdatedOrder) {
		this.isDhOutdatedOrder = isDhOutdatedOrder;
	}
}