package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class MpItemSpecRpt implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String workstoreGroupCode;
	
	private String workstoreCode;
	
	private String itemCode;
	
	private String fullDrugDesc;
	
	private String specCode;
	
	public MpItemSpecRpt(){
		
	}
	
	public MpItemSpecRpt(String workstoreGroupCode, String workstoreCode, String itemCode, String fullDrugDesc, String specCode){
		super();
		this.workstoreGroupCode = workstoreGroupCode;
		this.workstoreCode = workstoreCode;
		this.itemCode = itemCode;
		this.fullDrugDesc = fullDrugDesc;
		this.specCode = specCode;
	}

	public String getWorkstoreGroupCode() {
		return workstoreGroupCode;
	}

	public void setWorkstoreGroupCode(String workstoreGroupCode) {
		this.workstoreGroupCode = workstoreGroupCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getFullDrugDesc() {
		return fullDrugDesc;
	}

	public void setFullDrugDesc(String fullDrugDesc) {
		this.fullDrugDesc = fullDrugDesc;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}
	
}
