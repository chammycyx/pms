package hk.org.ha.model.pms.udt.disp;

import java.io.Serializable;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class DispOrderAdminStatusAdapter extends XmlAdapter<String, DispOrderAdminStatus> implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public String marshal(DispOrderAdminStatus v) {
		return (v == null) ? null : v.getDataValue();
	}

	public DispOrderAdminStatus unmarshal(String v) {
		return DispOrderAdminStatus.dataValueOf(v);
	}

}
