package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class PharmClinicRefillCouponRpt implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private PharmClinicRefillCouponRptHdr pharmClinicRefillCouponRptHdr;
	
	private List<PharmClinicRefillCouponRptDtl> pharmClinicRefillCouponRptDtlList;

	public PharmClinicRefillCouponRptHdr getPharmClinicRefillCouponRptHdr() {
		return pharmClinicRefillCouponRptHdr;
	}

	public void setPharmClinicRefillCouponRptHdr(
			PharmClinicRefillCouponRptHdr pharmClinicRefillCouponRptHdr) {
		this.pharmClinicRefillCouponRptHdr = pharmClinicRefillCouponRptHdr;
	}

	public List<PharmClinicRefillCouponRptDtl> getPharmClinicRefillCouponRptDtlList() {
		return pharmClinicRefillCouponRptDtlList;
	}

	public void setPharmClinicRefillCouponRptDtlList(
			List<PharmClinicRefillCouponRptDtl> pharmClinicRefillCouponRptDtlList) {
		this.pharmClinicRefillCouponRptDtlList = pharmClinicRefillCouponRptDtlList;
	}
	
}
