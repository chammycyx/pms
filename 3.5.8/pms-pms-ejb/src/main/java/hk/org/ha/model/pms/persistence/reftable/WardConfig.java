package hk.org.ha.model.pms.persistence.reftable;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
import hk.org.ha.model.pms.udt.printing.PrintLang;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "WARD_CONFIG")
@Customizer(AuditCustomizer.class)
public class WardConfig extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wardConfigSeq")
	@SequenceGenerator(name = "wardConfigSeq", sequenceName = "SQ_WARD_CONFIG", initialValue = 100000000)
	private Long id;
	
	@Column(name = "WARD_CODE", nullable = false, length = 4)
	private String wardCode;
	
	@Column(name = "PRINT_DOSAGE_INSTRUCTION_FLAG", nullable = false)
	private Boolean printDosageInstructionFlag;
	
	@Converter(name = "WardConfig.dosageInstructionPrintLang", converterClass = PrintLang.Converter.class)
	@Convert("WardConfig.dosageInstructionPrintLang")
	@Column(name="DOSAGE_INSTRUCTION_PRINT_LANG", length = 10, nullable = false)
	private PrintLang dosageInstructionPrintLang;

	@Column(name = "MP_WARD_FLAG", nullable = false)
	private Boolean mpWardFlag;

	@Column(name = "ATDPS_WARD_FLAG", nullable = false)
	private Boolean atdpsWardFlag;
	
	@ManyToOne
	@JoinColumn(name = "WORKSTORE_GROUP_CODE", nullable = false)
	private WorkstoreGroup workstoreGroup;
	
	@Transient
	private boolean enableAtdpsWard;
	
	public WardConfig() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public Boolean getPrintDosageInstructionFlag() {
		return printDosageInstructionFlag;
	}

	public void setPrintDosageInstructionFlag(Boolean printDosageInstructionFlag) {
		this.printDosageInstructionFlag = printDosageInstructionFlag;
	}

	public PrintLang getDosageInstructionPrintLang() {
		return dosageInstructionPrintLang;
	}

	public void setDosageInstructionPrintLang(PrintLang dosageInstructionPrintLang) {
		this.dosageInstructionPrintLang = dosageInstructionPrintLang;
	}

	public Boolean getMpWardFlag() {
		return mpWardFlag;
	}

	public void setMpWardFlag(Boolean mpWardFlag) {
		this.mpWardFlag = mpWardFlag;
	}

	public WorkstoreGroup getWorkstoreGroup() {
		return workstoreGroup;
	}

	public void setWorkstoreGroup(WorkstoreGroup workstoreGroup) {
		this.workstoreGroup = workstoreGroup;
	}

	// helper methods
    public boolean isNew() {
    	return this.getId() == null;
    }

	public void setAtdpsWardFlag(Boolean atdpsWardFlag) {
		this.atdpsWardFlag = atdpsWardFlag;
	}

	public Boolean getAtdpsWardFlag() {
		return atdpsWardFlag;
	}

	public void setEnableAtdpsWard(boolean enableAtdpsWard) {
		this.enableAtdpsWard = enableAtdpsWard;
	}

	public boolean isEnableAtdpsWard() {
		return enableAtdpsWard;
	}
    
}
