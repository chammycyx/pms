@XmlJavaTypeAdapters({   
    @XmlJavaTypeAdapter(value=DateAdapter.class, type=Date.class),
    @XmlJavaTypeAdapter(value=MedProfileOrderStatusAdapter.class, type=MedProfileOrderStatus.class),
    @XmlJavaTypeAdapter(value=MedProfileMoItemStatusAdapter.class, type=MedProfileMoItemStatus.class)
})  
package hk.org.ha.model.pms.persistence.medprofile;

import hk.org.ha.fmk.pms.eai.mail.DateAdapter;
import hk.org.ha.model.pms.udt.medprofile.MedProfileMoItemStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileOrderStatus;

import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;
   