package hk.org.ha.model.pms.biz.alert.mds;

import hk.org.ha.model.pms.persistence.AlertEntity;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.vo.alert.mds.Alert;
import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;
import hk.org.ha.model.pms.vo.alert.mds.MedProfileAlertMsg;

import java.util.List;

import javax.ejb.Local;

@Local
public interface AlertMsgManagerLocal {

	AlertMsg retrieveAlertMsg(MedOrderItem medOrderItem);
	
	MedProfileAlertMsg retrieveMedProfileAlertMsg(MedOrderItem medOrderItem);
	
	void sortAlertList(List<Alert> alertList);
	
	void sortAlertEntityList(List<? extends AlertEntity> alertEntityList);
	
	AlertMsg retrieveMpAlertMsg(MedProfileMoItem medProfileMoItem);
	
	AlertMsg retrieveMpPatAlertMsg(MedProfileMoItem medProfileMoItem);
	
	List<AlertMsg> retrieveMpDdiAlertMsgList(MedProfileMoItem medProfileMoItem);
}
