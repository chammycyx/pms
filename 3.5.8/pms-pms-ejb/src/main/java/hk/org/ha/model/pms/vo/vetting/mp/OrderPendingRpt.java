package hk.org.ha.model.pms.vo.vetting.mp;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class OrderPendingRpt implements Serializable {

	private static final long serialVersionUID = 1L;

	private String caseNum;

	private String name;

	private String nameChi;

	private String sex;

	private String age;

	private String weight;

	private String specialty;

	private String ward;

	private String bedNum;

	private String drugOrderTlf;

	private String prescribeUser;

	private String prescribeDate;

	private String pendUser;

	private String pendReason;

	private String pendRemark;

	private String remark;

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameChi() {
		return nameChi;
	}

	public void setNameChi(String nameChi) {
		this.nameChi = nameChi;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getSpecialty() {
		return specialty;
	}

	public void setSpecialty(String specialty) {
		this.specialty = specialty;
	}

	public String getWard() {
		return ward;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}

	public String getBedNum() {
		return bedNum;
	}

	public void setBedNum(String bedNum) {
		this.bedNum = bedNum;
	}

	public String getDrugOrderTlf() {
		return drugOrderTlf;
	}

	public void setDrugOrderTlf(String drugOrderTlf) {
		this.drugOrderTlf = drugOrderTlf;
	}

	public String getPrescribeUser() {
		return prescribeUser;
	}

	public void setPrescribeUser(String prescribeUser) {
		this.prescribeUser = prescribeUser;
	}

	public String getPrescribeDate() {
		return prescribeDate;
	}

	public void setPrescribeDate(String prescribeDate) {
		this.prescribeDate = prescribeDate;
	}

	public String getPendUser() {
		return pendUser;
	}

	public void setPendUser(String pendUser) {
		this.pendUser = pendUser;
	}

	public String getPendReason() {
		return pendReason;
	}

	public void setPendReason(String pendReason) {
		this.pendReason = pendReason;
	}

	public String getPendRemark() {
		return pendRemark;
	}

	public void setPendRemark(String pendRemark) {
		this.pendRemark = pendRemark;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
