package hk.org.ha.model.pms.persistence.disp;

import hk.org.ha.model.pms.persistence.BaseEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.disp.LedOperationLogStatus;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "LED_OPERATION_LOG")
public class LedOperationLog extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ledOperationLogSeq")
	@SequenceGenerator(name = "ledOperationLogSeq", sequenceName = "SQ_LED_OPERATION_LOG", initialValue = 100000000)
	private Long id;
	
	@Column(name = "TICKET_NUM", nullable = false, length = 4)
	private String ticketNum;
	
	@Column(name = "TICKET_DATE", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date ticketDate;
	
	@Column(name = "WINDOW_NUM", nullable = false)
	private Integer windowNum;
	
	@Converter(name = "LedOperationLog.status", converterClass = LedOperationLogStatus.Converter.class)
    @Convert("LedOperationLog.status")
	@Column(name="STATUS", nullable = false, length = 1)
	private LedOperationLogStatus status;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE", referencedColumnName = "HOSP_CODE", nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getWindowNum() {
		return windowNum;
	}

	public void setWindowNum(Integer windowNum) {
		this.windowNum = windowNum;
	}

	public LedOperationLogStatus getStatus() {
		return status;
	}

	public void setStatus(LedOperationLogStatus status) {
		this.status = status;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public Date getTicketDate() {
		if (ticketDate == null) {
			return null;
		}
		else {
			return new Date(ticketDate.getTime());
		}
	}

	public void setTicketDate(Date ticketDate) {
		if (ticketDate == null) {
			this.ticketDate = null;
		}
		else {
			this.ticketDate = new Date(ticketDate.getTime());
		}
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}
}
