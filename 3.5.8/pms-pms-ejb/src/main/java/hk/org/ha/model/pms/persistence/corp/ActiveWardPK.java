package hk.org.ha.model.pms.persistence.corp;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;


public class ActiveWardPK implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String patHospCode;
	private String pasWardCode;
	
	public ActiveWardPK() {
	}
	
	public ActiveWardPK(String patHospCode, String pasWardCode) {
		this.patHospCode = patHospCode;
		this.pasWardCode = pasWardCode;
	}

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}
	
	public String getPasWardCode() {
		return pasWardCode;
	}

	public void setPasWardCode(String pasWardCode) {
		this.pasWardCode = pasWardCode;
	}

	public boolean equals(Object other) {
		return EqualsBuilder.reflectionEquals(this, other);
	}
	
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}	
}
