package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.PmsCommonOrderStatus;
import hk.org.ha.model.pms.vo.reftable.mp.CommonOrderStatusInfo;

import java.util.List;

import javax.ejb.Local;

@Local
public interface CommonOrderStatusListServiceLocal {

	CommonOrderStatusInfo retrieveCommonOrderStatusInfo( DmDrug dmDrug );
	
	CommonOrderStatusInfo retrievePmsCommonOrderStatusList( String patHospCode, String itemCode, Integer drugKey );
	
	void updateCommonOrderStatusList( List<PmsCommonOrderStatus> pmsCommonOrderStatusList );
	
	void destroy();
	
}
 