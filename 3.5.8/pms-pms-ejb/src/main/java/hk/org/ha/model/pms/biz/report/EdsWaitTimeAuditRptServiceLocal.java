package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.udt.report.WaitTimeAuditRptResult;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface EdsWaitTimeAuditRptServiceLocal {
	
	List<String> retrieveCurrentAvgWaitTime();
	WaitTimeAuditRptResult retrieveDailyWaitTimeAuditRpt(Date retrieveDate);
	Boolean retrieveMonthlyWaitTimeAuditRpt(Date retrieveDate);
	List<Date> retrieveAvailableMonthForMonthlyWaitTimeAuditRpt();
	void generateEdsDailyWaitTimeAuditRpt();
	void printEdsDailyWaitTimeAuditRpt();
	void exportEdsDailyWaitTimeAuditRpt();
	void generateEdsMonthlyWaitTimeAuditRpt();
	void printEdsMonthlyWaitTimeAuditRpt();
	void exportEdsMonthlyWaitTimeAuditRpt();
	void destroy();
}
