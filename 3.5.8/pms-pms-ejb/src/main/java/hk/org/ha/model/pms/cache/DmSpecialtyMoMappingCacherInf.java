package hk.org.ha.model.pms.cache;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmSpecialtyMoMapping;

import java.util.List;

public interface DmSpecialtyMoMappingCacherInf extends BaseCacherInf {

	DmSpecialtyMoMapping getDmSpecialtyMoMappingBySpecialtyCode(String specialtyCode);
	
	List<DmSpecialtyMoMapping> getDmSpecialtyMoMappingList();
	
	List<DmSpecialtyMoMapping> getDmSpecialtyMoMappingListBySpecialtyCode(String prefixSpecialtyCode);

}
