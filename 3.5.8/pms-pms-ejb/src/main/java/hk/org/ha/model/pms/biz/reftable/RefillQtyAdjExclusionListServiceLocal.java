package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.persistence.reftable.RefillQtyAdjExclusion;

import java.util.Collection;
import java.util.List;

import javax.ejb.Local;

@Local
public interface RefillQtyAdjExclusionListServiceLocal {

	List<RefillQtyAdjExclusion> retrieveRefillQtyAdjExclusionList();
	
	void updateRefillQtyAdjExclusionList(Collection<RefillQtyAdjExclusion> updateRefillQtyAdjExclusionList, 
											Collection<RefillQtyAdjExclusion> delRefillQtyAdjExclusionList);
	
	void destroy();
	
}
