package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.inbox.PharmInboxClientNotifierInf;
import hk.org.ha.model.pms.biz.medprofile.MedProfileManagerLocal;
import hk.org.ha.model.pms.biz.reftable.RefTableManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.phs.Ward;
import hk.org.ha.model.pms.persistence.phs.WardPK;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.persistence.reftable.OperationWorkstation;
import hk.org.ha.model.pms.persistence.reftable.WardConfig;
import hk.org.ha.model.pms.udt.RecordStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatus;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("wardConfigService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class WardConfigServiceBean implements WardConfigServiceLocal {
	
	private static final String MED_PROFILE_QUERY_HINT_FETCH_1 = "o.patient";
	private static final String MED_PROFILE_QUERY_HINT_FETCH_2 = "o.medCase";
	
	
	@PersistenceContext
	private EntityManager em;

	@Out(required = false)
	private WardConfig wardConfig;
	
	@Out(required = false)
	private Ward ward;
	
	@In
	private Workstore workstore;
	
	@In
	private RefTableManagerLocal refTableManager;

	@In
	private MedProfileManagerLocal medProfileManager;
	
	@In
	private PharmInboxClientNotifierInf pharmInboxClientNotifier;
	
	@In(required = false)
	private OperationProfile activeProfile;
	
	private Boolean enableAtdpsWard;
	
	@SuppressWarnings("unchecked")
	public void retrieveWardConfig(String wardCode) {
		
		ward = null;
		
		ward = em.find(Ward.class, new WardPK( workstore.getWorkstoreGroup().getInstitution().getInstCode(), wardCode ) );
		
		wardConfig = new WardConfig();
		wardConfig.setWardCode(wardCode);
		
		if( ward != null && ward.getStatus() != RecordStatus.Active ){
			ward = null;
			return;
		}

		List<WardConfig> wardConfigList = em.createQuery(
				"select o from WardConfig o" + // 20120303 index check : WardConfig.workstoreGroup,wardCode : UI_WARD_CONFIG_01
				" where o.workstoreGroup = :workstoreGroup" + 
				" and o.wardCode = :wardCode")
				.setParameter("workstoreGroup", workstore.getWorkstoreGroup())
				.setParameter("wardCode", wardCode)
				.getResultList(); 

		if( !wardConfigList.isEmpty() ){
			wardConfig = wardConfigList.get(0);
		}
		List<OperationWorkstation> operationWorkstationList = refTableManager.getActiveAtdpsWorkstation(activeProfile);
		if ( !operationWorkstationList.isEmpty() ) {
			enableAtdpsWard = true;
		} else {
			enableAtdpsWard = false;
		}
		
		wardConfig.setEnableAtdpsWard(enableAtdpsWard);

	}

	public void updateWardConfig(WardConfig wardConfig){
				
		WorkstoreGroup workstoreGroup = workstore.getWorkstoreGroup();
		wardConfig.setWorkstoreGroup(workstoreGroup);
		
		if( !wardConfig.isNew() )
		{
			wardConfig = em.merge(wardConfig);
		}
		else
		{
			em.persist(wardConfig);
		}
		em.flush();
		
		retrieveWardConfig(wardConfig.getWardCode());
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void refreshMedProfiles(WardConfig wardConfig, boolean refreshPasInfo, boolean refreshMrInfo) {
		
		List<MedProfile> medProfileList = em.createQuery(
				"select o from MedProfile o" + // 20160809 index check : MedProfile.hospCode,wardCode,status : I_MED_PROFILE_06
				" where o.processingFlag = false and o.deliveryGenFlag = false" +
				" and o.status in :notInactiveStatus" +
				" and o.workstore.workstoreGroup = :workstoreGroup" +
				" and o.hospCode = :hospCode" +
				" and o.wardCode = :wardCode")
				.setParameter("notInactiveStatus", MedProfileStatus.Not_Inactive)
				.setParameter("workstoreGroup", wardConfig.getWorkstoreGroup() == null ? workstore.getWorkstoreGroup() : wardConfig.getWorkstoreGroup())
				.setParameter("hospCode", workstore.getHospCode())
				.setParameter("wardCode", wardConfig.getWardCode())
				.setHint(QueryHints.FETCH, MED_PROFILE_QUERY_HINT_FETCH_1)
				.setHint(QueryHints.FETCH, MED_PROFILE_QUERY_HINT_FETCH_2)
				.getResultList();
		
		Set<String> hospCodeSet = new TreeSet<String>();
		for (MedProfile medProfile: medProfileList) {
			medProfileManager.refreshMedProfile(medProfile, refreshPasInfo, refreshMrInfo);
			hospCodeSet.add(medProfile.getHospCode());
		}
		pharmInboxClientNotifier.dispatchUpdateEvent(hospCodeSet);
	}

	@Remove
	public void destroy() {
		if ( wardConfig != null ) {
			wardConfig = null;
		}
		if ( ward != null ) {
			ward = null;
		}
		if ( enableAtdpsWard != null ) {
			enableAtdpsWard = null;
		}
	}
}
