package hk.org.ha.model.pms.biz.vetting;

import static hk.org.ha.model.pms.prop.Prop.CHARGING_STANDARD_CHARGEABLEUNIT;
import static hk.org.ha.model.pms.prop.Prop.CHARGING_STANDARD_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.VETTING_DUPLICATEDOITEM_CHECK;
import hk.org.ha.fmk.pms.remote.marshall.EclipseLinkXStreamMarshaller;
import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.drug.WorkstoreDrugListManagerLocal;
import hk.org.ha.model.pms.biz.onestop.OneStopUtilsLocal;
import hk.org.ha.model.pms.biz.refill.RefillScheduleManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.corp.cache.DmRouteCacher;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.dms.biz.formatengine.FormatHints;
import hk.org.ha.model.pms.dms.biz.formatengine.ParsedOrderLineFormatter;
import hk.org.ha.model.pms.dms.biz.formatengine.ParsedOrderLineFormatterBase;
import hk.org.ha.model.pms.dms.biz.formatengine.ParsedOrderLineFormatterFactory;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.udt.formatengine.FormatType;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderFm;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.udt.YesNoBlankFlag;
import hk.org.ha.model.pms.udt.disp.DupChkDrugType;
import hk.org.ha.model.pms.udt.disp.MedCasePasPatInd;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.rx.RxItemType;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.udt.vetting.RegimenDurationUnit;
import hk.org.ha.model.pms.vo.charging.MoiCapdChargeInfo;
import hk.org.ha.model.pms.vo.charging.MoiChargeInfo;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.model.pms.vo.rx.CapdItem;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.InjectionRxDrug;
import hk.org.ha.model.pms.vo.rx.OralRxDrug;
import hk.org.ha.model.pms.vo.rx.Regimen;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;
import hk.org.ha.model.pms.vo.security.PropMap;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;
import org.springframework.beans.BeanUtils;

@AutoCreate
@Stateless
@Name("orderEditManager")
@MeasureCalls
public class OrderEditManagerBean implements OrderEditManagerLocal {

	@Logger
	private Log logger;
		
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private MedOrderItem medOrderItem;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private List<MoiChargeInfo> moiChargeInfoList;
		
	@Out(scope=ScopeType.SESSION, required=false)
	private RxItem rxItemForOrderEdit;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private RefillScheduleManagerLocal refillScheduleManager;
	
	@In
	private DuplicateItemCheckManagerLocal duplicateItemCheckManager;
	
	@In
	private WorkstoreDrugListManagerLocal workstoreDrugListManager;
	
	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;
	
	@In
	private OneStopUtilsLocal oneStopUtils;
	
	@In
	private Workstore workstore;
	
	@In
	private PropMap propMap;
	
	private static final String KEY_SEPARATOR = "|";
	
	public String getFormattedDrugOrderTlf(RxItem rxItem) {	
		ParsedOrderLineFormatterBase formatter = ParsedOrderLineFormatterFactory.getParsedOrderLineFormatter(rxItem.getInterfaceVersion());
		formatter.setHint(FormatHints.FORMAT_TYPE, FormatType.TLF);
		formatter.setHint(FormatHints.LINE_WIDTH, 45);
		formatter.setHint(FormatHints.HIDE_STRIKE_THROUGH, false);
		formatter.setHint(FormatHints.EXCLUDE_TOKEN_IDS, new String[]{"order.specialInstruction"});
		return formatter.format(rxItem.getDrugOrderXml());
	}
	
	public void retrieveMedOrderItemForOrderEdit(int index) {
		
		if (logger.isDebugEnabled()) {
			logger.debug("retrieveMedOrderItemForOrderEdit #0", index);
		}

		MedOrder medOrder = ((PharmOrder) Contexts.getSessionContext().get("pharmOrder")).getMedOrder();

		if( !medOrder.getMedOrderItemList().isEmpty() && index != -1 && index < medOrder.getMedOrderItemList().size() ) {
			medOrderItem = medOrder.getMedOrderItemList().get(index);
			
			//Disconnect MedOrderItem		
			medOrderItem.setMedOrder(null);
			for ( PharmOrderItem pharmOrderItem : medOrderItem.getPharmOrderItemList() ) {
				pharmOrderItem.setPharmOrder(null);
				pharmOrderItem.clearDispOrderItemList();
				pharmOrderItem.setMedOrderItem(medOrderItem);
			}
		} else {
			medOrderItem = null;			
		}
	}
	
	private void dispatchLoggerErrorForManualOrder(MedOrder medOrder, MedOrderItem medOrderItemIn, String logMessage) {
		Workstore workstore = (Workstore)Contexts.getSessionContext().get("workstore");
		logger.error(logMessage + " [hospCode:#0, workstoreCode:#1, tickNum:#2, order item drug description:#3]", workstore.getHospCode(), workstore.getWorkstoreCode(), medOrder.getTicket().getTicketNum(), medOrderItemIn.getDisplayNameSaltForm());
	}
	
	private void dispatchLoggerErrorForMoeOrder(MedOrder medOrder, MedOrderItem medOrderItemIn, String logMessage) {
		Workstore workstore = (Workstore)Contexts.getSessionContext().get("workstore");
		logger.error(logMessage + " [hospCode:#0, workstoreCode:#1, medOrderId:#2, orderNum:#3, order item drug description:#4]", workstore.getHospCode(), workstore.getWorkstoreCode(), medOrder.getId(), medOrder.getOrderNum(), medOrderItemIn.getDisplayNameSaltForm());
	}
	
	private void dispatchLoggerError(MedOrder medOrder, MedOrderItem medOrderItemIn, String logMessage) {
		if (medOrder.getDocType() == MedOrderDocType.Manual) {
			dispatchLoggerErrorForManualOrder(medOrder, medOrderItemIn, logMessage);
		} else {
			dispatchLoggerErrorForMoeOrder(medOrder, medOrderItemIn, logMessage);
		}
	}
	
	private String getLoggerErrorMessage(MedOrderItem medOrderItemIn, boolean isDhOrder) {
		
		if (!medOrderItemIn.getRxItem().isRxDrug()) {
			return null;	
		}
		
		Regimen regimen = medOrderItemIn.getRegimen();
		
		if (regimen == null) {
			return "MedOrderItem has null regimen";
		}
		
		List<DoseGroup> doseGroupList = regimen.getDoseGroupList();
		if (doseGroupList == null) {
			return "MedOrderItem regimen has null doseGroupList";
		}
		
		for (DoseGroup doseGroup:doseGroupList) {
			if (doseGroup.getDoseList() == null) {
				return "MedOrderItem regimen has null doseList";
			}
			if (doseGroup.getDuration() == null && !isDhOrder) {
				return "MedOrderItem regimen has null duration";
			}
			for (Dose dose:doseGroup.getDoseList()) {				
				if (dose.getDailyFreq() == null) {
					return "MedOrderItem regimen has null daily frequency";
				}				
			}
		}
		return null;
	}
	
	private void dispatchloggerErrorForMissingRegimenMandatoryInfo(MedOrder medOrder, MedOrderItem medOrderItemIn) {
		if (medOrder.isMpDischarge()) {
			return;
		}
		
		String errorMsg = getLoggerErrorMessage(medOrderItemIn,medOrder.isDhOrder());
		
		if (errorMsg != null) {
			dispatchLoggerError(medOrder, medOrderItemIn, errorMsg);
		}
	}
	
	public List<Integer> updateMedOrderFromOrderEdit(int index, MedOrderItem medOrderItemIn, boolean skipDuplicateCheckFlag) { 
		
		if (logger.isDebugEnabled()) {
			logger.debug(new EclipseLinkXStreamMarshaller().toXML(medOrderItemIn));
		}
		
		List<Integer> itemNumList = new ArrayList<Integer>();
		medOrderItem = null;
		
		if ( medOrderItemIn == null ) {
			return itemNumList;
		}

		PharmOrder pharmOrder = (PharmOrder) Contexts.getSessionContext().get("pharmOrder");
		MedOrder medOrder = pharmOrder.getMedOrder();
		
		//will be eliminated after fixing PMSPRD-1448
		dispatchloggerErrorForMissingRegimenMandatoryInfo(medOrder, medOrderItemIn);
		
		for (PharmOrderItem poi : medOrderItemIn.getPharmOrderItemList()) {
			if (poi.getItemNum() == null) {
				poi.setOrgItemNum(pharmOrder.getAndIncreaseMaxOrgItemNum());
				poi.setItemNum(pharmOrder.getAndIncreaseMaxItemNum());
			}
			itemNumList.add(poi.getOrgItemNum());
			itemNumList.add(poi.getItemNum());
		}
		
		duplicateItemCheckManager.setCorpDuplicateItemList(null);

		// update dupChkDrugType
		for (PharmOrderItem poi : medOrderItemIn.getPharmOrderItemList()) {
			if (FmStatus.FreeTextEntryItem.getDataValue().equals(medOrderItemIn.getFmStatus())) {
				continue;
			}
			DmDrug dmDrug = DmDrugCacher.instance().getDmDrug(poi.getItemCode());
			if (dmDrug != null && dmDrug.getDmForm() != null && dmDrug.getDmForm().getRouteCode() != null && StringUtils.equals(DmRouteCacher.instance().getRouteByRouteCode(dmDrug.getDmForm().getRouteCode()).getRouteDesc(), "ORAL")) {
				poi.setDupChkDrugType(DupChkDrugType.Oral);
			}
			else {
				poi.setDupChkDrugType(DupChkDrugType.Other);
			}
		}

		// check duplicate item in corp
		if (!skipDuplicateCheckFlag && VETTING_DUPLICATEDOITEM_CHECK.get(Boolean.FALSE)) {
			if (!duplicateItemCheckManager.checkCorpDuplicateItemByMedOrderItem(pharmOrder, medOrderItemIn)) {
				duplicateItemCheckManager.setCorpDuplicateItemList(medOrderItemIn.getDuplicateItemList());
				medOrderItem = medOrderItemIn;
				return itemNumList;
			}
		}
		
		rxItemForOrderEdit = null;
		if (medOrder.isMpDischarge() 				
				&& !medOrderItemIn.isInfusion()
				&& !FmStatus.FreeTextEntryItem.getDataValue().equals(medOrderItemIn.getFmStatus())) {
			reconstructRxItemForDischarge(medOrderItemIn);
			rxItemForOrderEdit = medOrderItemIn.getRxItem();
		}
		
		medOrderItemIn.preSave();				
		
		if (index < medOrder.getMedOrderItemList().size()) { // replace item
			medOrder.getMedOrderItemList().set(index, medOrderItemIn);
			medOrderItemIn.setMedOrder(medOrder);
		}
		else { // add item
			medOrder.addMedOrderItem(medOrderItemIn);
		}	

		//Re-connect MedOrderItem and set max item num.	
		pharmOrder.getPharmOrderItemList().clear();
		Integer maxItemNum = pharmOrder.getMaxItemNum();
		for ( MedOrderItem moi:medOrder.getMedOrderItemList() ) {
			for ( PharmOrderItem poi:moi.getPharmOrderItemList() ) {
				poi.setMedOrderItem(moi);
				pharmOrder.addPharmOrderItem(poi);					
				if ( maxItemNum.compareTo(poi.getItemNum()) < 0 ) {
					pharmOrder.setMaxItemNum(poi.getItemNum());
				}
			}
		}		
		
		//clear RefillScheduleItemList By PharmOrderItemNum			
		for (PharmOrderItem pharmOrderItem : pharmOrder.getPharmOrderItemList()) {
			if (pharmOrderItem.getMedOrderItem().getItemNum().equals(medOrderItemIn.getItemNum()) ){
				refillScheduleManager.removeRefillScheduleListByPharmOrderItemNum(pharmOrder, pharmOrderItem.getOrgItemNum());
				//reset RefillScheduleFlag
				pharmOrderItem.setStdRefillCheckFlag(false);
			}
		}
		
		medOrderItemIn.updateDdiRelatedAlert();
		
		this.updateMedOrderItemChargeFlagByOrderEdit(medOrderItemIn, pharmOrder);
		
		this.updateMedOrderItemCmsFmStatusFlagByOrderEdit(medOrder, medOrderItemIn, pharmOrder);
		
		return itemNumList;
	}		
	
	public void reconstructRxItemForDischarge(MedOrderItem medOrderItemIn) {
		String siteCatCode;
		RxDrug targetRxDrug=null;
		if (!medOrderItemIn.isInfusion()) {
			if (medOrderItemIn.getRegimen()!= null) {
				if (medOrderItemIn.getRxItem().isInjectionRxDrug()) {
					siteCatCode = medOrderItemIn.getRegimen().getFirstDose().getDmSite().getSiteCategoryCode();					
					if (!"IJ".equals(siteCatCode)) {
						targetRxDrug = new OralRxDrug();
						BeanUtils.copyProperties(medOrderItemIn.getRxDrug(), targetRxDrug);
						medOrderItemIn.setRxItemType(RxItemType.Oral);
					}
				} else if (medOrderItemIn.getRxItem().isOralRxDrug()) {
					siteCatCode = medOrderItemIn.getRegimen().getFirstDose().getDmSite().getSiteCategoryCode();					
					if ("IJ".equals(siteCatCode)) {
						targetRxDrug = new InjectionRxDrug();
						((InjectionRxDrug)targetRxDrug).setNoDiluentRequireFlag(Boolean.FALSE);
						BeanUtils.copyProperties(medOrderItemIn.getRxDrug(), targetRxDrug);	
						medOrderItemIn.setRxItemType(RxItemType.Injection);
					}
				}
			}
		}
		
		if (targetRxDrug != null) {
			medOrderItemIn.setRxItem(targetRxDrug);
		}
	}
	
	private String retrieveDisplayNameFormSaltKey(MedOrderItem moiIn) {		
		StringBuilder dfsKey = new StringBuilder();
		dfsKey.append(moiIn.getDisplayName());
		dfsKey.append(KEY_SEPARATOR);
		dfsKey.append(moiIn.getFormCode());
		dfsKey.append(KEY_SEPARATOR);
		dfsKey.append(moiIn.getSaltProperty());		
		if( BooleanUtils.isTrue(moiIn.getStrengthCompulsory()) ){
			dfsKey.append(KEY_SEPARATOR);
			dfsKey.append(moiIn.getStrength());
		}
		return dfsKey.toString();
	}
	
	public void updateMedOrderItemChargeFlagByOrderEdit(MedOrderItem medOrderItemIn, PharmOrder pharmOrder){
		moiChargeInfoList = new ArrayList<MoiChargeInfo>();
		
		boolean updateMoeChargeFlagOnly = false;

		if( !propMap.getValueAsBoolean(CHARGING_STANDARD_ENABLED.getName())
			|| pharmOrder.getMedOrder().getPrescType() == MedOrderPrescType.MpDischarge
			|| (medOrderItemIn != null && !medOrderItemIn.getPharmOrderItemList().isEmpty() && medOrderItemIn.getPharmOrderItemList().get(0).getChestFlag())){
			return;
		}
		
		if( pharmOrder.getMedCase() != null ){
			if( pharmOrder.getMedCase().getCaseNum() != null && oneStopUtils.isInPatCase(pharmOrder.getMedCase().getCaseNum()) ){
				return;
			}else if( !pharmOrder.getMedCase().getChargeFlag() ){
				updateMoeChargeFlagOnly = true;
			}
		}
		
		if(medOrderItemIn == null || medOrderItemIn.getCapdRxDrug() == null ){
			updateMedOrderItemChargeFlag(pharmOrder.getMedOrder(), updateMoeChargeFlagOnly);
		}else{
			updateCapdMedOrderItemChargeFlag(pharmOrder.getMedOrder(), updateMoeChargeFlagOnly);
		}
	}
	
	private void updateMedOrderItemChargeFlag(MedOrder medOrder, boolean updateMoeChargeFlagOnly){
		List<String> moiKeyList = new ArrayList<String>();
		Map<Integer, MoiChargeInfo> moiChargeInfoMap = new HashMap<Integer, MoiChargeInfo>();
		Map<String,MoiChargeInfo> chargeableItemMap = new HashMap<String,MoiChargeInfo>();
		
		int i=-1;
		for ( MedOrderItem moi : medOrder.getMedOrderItemList() ) {
			++i;
			if ( moi.getActionStatus() != ActionStatus.DispByPharm ) {
				continue;
			}
			if( moi.getCapdRxDrug() == null ){
				String moiDrugKey = retrieveDisplayNameFormSaltKey(moi);
				
				if( !moiKeyList.contains(moiDrugKey) ){
					moiKeyList.add(moiDrugKey);	
				}

				MoiChargeInfo moiChargeInfo = new MoiChargeInfo();
				moiChargeInfo.setMedOrderItemIndex(Integer.valueOf(i));
				moiChargeInfo.setDurationInDay( moi.getRegimen().calTotalDurationInDay() );
				
				if (medOrder.isDhOrder()) {
					moiChargeInfo.setCmsChargeFlag(YesNoBlankFlag.Blank);
				}

				if( !MedOrderItemStatus.Deleted.equals(moi.getStatus()) ){
					if( !chargeableItemMap.containsKey(moiDrugKey) ){
						chargeableItemMap.put(moiDrugKey, moiChargeInfo);
					}else if ( moiChargeInfo.getDurationInDay() > chargeableItemMap.get(moiDrugKey).getDurationInDay() ) {
						chargeableItemMap.put(moiDrugKey, moiChargeInfo);
					}
				}
				moiChargeInfoMap.put(moiChargeInfo.getMedOrderItemIndex(), moiChargeInfo);				
			}
		}
		
		for( MoiChargeInfo moiChargeItem : chargeableItemMap.values() ){
			updateChargeableMedOrderItem(moiChargeInfoMap, moiChargeItem, updateMoeChargeFlagOnly, medOrder.isDhOrder());
		}

		for( MoiChargeInfo moiChargeInfo : moiChargeInfoMap.values() ){
			MedOrderItem selectedMoi =  medOrder.getMedOrderItemList().get(moiChargeInfo.getMedOrderItemIndex());

			if( selectedMoi.getCmsChargeFlag() != moiChargeInfo.getCmsChargeFlag() ||
					selectedMoi.getChargeFlag() != moiChargeInfo.getChargeFlag() ||
					!selectedMoi.getUnitOfCharge().equals(moiChargeInfo.getUnitOfCharge())
				){
					moiChargeInfo.setChangeFlag(true);
					selectedMoi.setChangeFlag(Boolean.TRUE);
					selectedMoi.setCmsChargeFlag( moiChargeInfo.getCmsChargeFlag() );
					selectedMoi.setChargeFlag( moiChargeInfo.getChargeFlag() );
					selectedMoi.setUnitOfCharge( moiChargeInfo.getUnitOfCharge() );
					
					moiChargeInfoList.add(moiChargeInfo);
			}
		}

		if (logger.isDebugEnabled()) {
			for( MoiChargeInfo outInfo: moiChargeInfoList ){
				logger.debug("updateMedOrderItemChargeFlag============cmsChargeFlag:#0, ChargeFlag:#1, DurationInDay:#2, UnitOfCharge:#3, ItemIndex:#4, ChangeFlag: #5", outInfo.getCmsChargeFlag(), outInfo.getChargeFlag(), outInfo.getDurationInDay(), outInfo.getUnitOfCharge(), outInfo.getMedOrderItemIndex(), outInfo.isChangeFlag());
			}
		}
	}
	
	private void updateChargeableMedOrderItem( Map<Integer,MoiChargeInfo> chargeInfoMap, MoiChargeInfo moiChargeItemIn, boolean updateMoeChargeFlagOnly, boolean isDhOrder){
		MoiChargeInfo moiChargeItemInfo = chargeInfoMap.get(moiChargeItemIn.getMedOrderItemIndex());
		
		moiChargeItemInfo.setCmsChargeFlag( isDhOrder ? YesNoBlankFlag.Blank : YesNoBlankFlag.Yes );
		if( !updateMoeChargeFlagOnly || isDhOrder ){
			moiChargeItemInfo.setChargeFlag( YesNoBlankFlag.Yes );
		}
		BigDecimal unitOfCharge = isDhOrder ? BigDecimal.ZERO : BigDecimal.valueOf(moiChargeItemInfo.getDurationInDay()).divide(BigDecimal.valueOf(CHARGING_STANDARD_CHARGEABLEUNIT.get(112)), RoundingMode.CEILING);
		moiChargeItemInfo.setUnitOfCharge(unitOfCharge.intValue());
		
		chargeInfoMap.put(moiChargeItemInfo.getMedOrderItemIndex(), moiChargeItemInfo);
	}

	private void updateCapdMedOrderItemChargeFlag(MedOrder medOrder, boolean updateMoeChargeFlagOnly){		
		List<String> capdItemKeyList = new ArrayList<String>();
		Map<String,MoiCapdChargeInfo> chargeableCapdItemMap = new HashMap<String,MoiCapdChargeInfo>();
		Map<Integer, MoiChargeInfo> moiChargeInfoMap = new HashMap<Integer,MoiChargeInfo>();
		
		int i=-1;
		for ( MedOrderItem moi : medOrder.getMedOrderItemList() ) {
			++i;
			
			if ( moi.getActionStatus() != ActionStatus.DispByPharm || moi.getCapdRxDrug() == null ) {
				continue;
			}
				
			int j=-1;
			for( CapdItem capdItem : moi.getCapdRxDrug().getCapdItemList() ){
				++j;
					
				String capdKey = moi.getCapdRxDrug().getSupplierSystem() + KEY_SEPARATOR + moi.getCapdRxDrug().getCalciumStrength()+KEY_SEPARATOR+capdItem.getConcentration();
				
				if( !capdItemKeyList.contains(capdKey) ){
					capdItemKeyList.add(capdKey);
				}
				
				if( capdItemKeyList.contains(capdKey) ){
					int durationInDay = capdItem.getDuration();
					if( RegimenDurationUnit.Week == capdItem.getDurationUnit() ){
						durationInDay = durationInDay * 7;
					}
						
					MoiCapdChargeInfo moiCapdChargeInfo = new MoiCapdChargeInfo();
					moiCapdChargeInfo.setMedOrderItemIndex(Integer.valueOf(i));
					moiCapdChargeInfo.setMedOrderCapdItemIndex(Integer.valueOf(j));
					moiCapdChargeInfo.setDurationInDay(durationInDay);
					moiCapdChargeInfo.setInitUnitOfCharge(capdItem.getUnitOfCharge());
					if( MedOrderItemStatus.Deleted == moi.getStatus() ){
						if( MedOrderDocType.Normal == medOrder.getDocType() ){
							moiCapdChargeInfo.setUnitOfCharge(0);
						}
					}else{
						if( !chargeableCapdItemMap.containsKey(capdKey) ){
							chargeableCapdItemMap.put(capdKey, moiCapdChargeInfo);
						}else if( moiCapdChargeInfo.getDurationInDay() > chargeableCapdItemMap.get(capdKey).getDurationInDay() ){
							chargeableCapdItemMap.put(capdKey, moiCapdChargeInfo);
						}
					}
						
					if( !moiChargeInfoMap.containsKey(Integer.valueOf(i)) ){
						MoiChargeInfo moiChargeInfo = new MoiChargeInfo();
						if( MedOrderItemStatus.Deleted == moi.getStatus() && MedOrderDocType.Normal == medOrder.getDocType()){
							moiChargeInfo.setCmsChargeFlag(YesNoBlankFlag.No);
							moiChargeInfo.setChargeFlag(YesNoBlankFlag.No);
							moiChargeInfo.setUnitOfCharge(0);
						}
						moiChargeInfo.setMedOrderItemIndex(moiCapdChargeInfo.getMedOrderItemIndex());
						moiChargeInfo.addMoiCapdChargeInfo(moiCapdChargeInfo);
						
						moiChargeInfoMap.put(moiChargeInfo.getMedOrderItemIndex(), moiChargeInfo);
					}else{
						MoiChargeInfo updateMoiChargeInfo = moiChargeInfoMap.get(moiCapdChargeInfo.getMedOrderItemIndex());
						updateMoiChargeInfo.getMoiCapdChargeInfoList().add(moiCapdChargeInfo);
						
						moiChargeInfoMap.put(updateMoiChargeInfo.getMedOrderItemIndex(), updateMoiChargeInfo);
					}
				}
			}
		}

		for( MoiCapdChargeInfo moiCapdChargeItem : chargeableCapdItemMap.values() ){
			updateChargeableCapdItem(moiChargeInfoMap, moiCapdChargeItem);
		}
		
		for( MoiChargeInfo updateMoiChargeInfo : moiChargeInfoMap.values() ){
			for( MoiCapdChargeInfo updateCapdChargeInfo : updateMoiChargeInfo.getMoiCapdChargeInfoList() ){
					if( !updateCapdChargeInfo.getUnitOfCharge().equals(updateCapdChargeInfo.getInitUnitOfCharge()) ){
						updateCapdChargeInfo.setChangeFlag(true);
						updateMoiChargeInfo.setChangeFlag(true);
						medOrder.getMedOrderItemList().get(updateCapdChargeInfo.getMedOrderItemIndex())
						.getCapdRxDrug().getCapdItemList().get(updateCapdChargeInfo.getMedOrderCapdItemIndex()).setUnitOfCharge(updateCapdChargeInfo.getUnitOfCharge());
					}
			}
			if( !updateMoiChargeInfo.getCmsChargeFlag().equals(medOrder.getMedOrderItemList().get(updateMoiChargeInfo.getMedOrderItemIndex()).getCmsChargeFlag()) ){
				updateMoiChargeInfo.setChangeFlag(true);
			}
		}
		
		for( MoiChargeInfo finalMoiChargeInfo : moiChargeInfoMap.values() ){
			Integer totalUnitOfCharge = 0;
			if( finalMoiChargeInfo.isChangeFlag() ){

				MedOrderItem moItem = medOrder.getMedOrderItemList().get(finalMoiChargeInfo.getMedOrderItemIndex());
				for( CapdItem capdItem : moItem.getCapdRxDrug().getCapdItemList() ){
					totalUnitOfCharge = totalUnitOfCharge + capdItem.getUnitOfCharge();
				}

				if( totalUnitOfCharge > 0 ){
					moItem.setCmsChargeFlag( YesNoBlankFlag.Yes );
					moItem.setUnitOfCharge(totalUnitOfCharge);
					if( !updateMoeChargeFlagOnly ){
						moItem.setChargeFlag( YesNoBlankFlag.Yes );
					}
				}else{
					moItem.setCmsChargeFlag( YesNoBlankFlag.No );
					moItem.setUnitOfCharge(totalUnitOfCharge);
					if( !updateMoeChargeFlagOnly ){
						moItem.setChargeFlag( YesNoBlankFlag.No );
					}
				}
				
				finalMoiChargeInfo.setCmsChargeFlag(moItem.getCmsChargeFlag());
				finalMoiChargeInfo.setChargeFlag(moItem.getChargeFlag());
				finalMoiChargeInfo.setUnitOfCharge(moItem.getUnitOfCharge());
				
				moiChargeInfoList.add(finalMoiChargeInfo);
			}
		}
		
		if (logger.isDebugEnabled()) {
			for( MoiChargeInfo outInfo: moiChargeInfoList ){
				logger.debug("updateCapdMedOrderItemChargeFlag============cmsChargeFlag:#0, ChargeFlag:#1, UnitOfCharge:#3, ItemIndex:#4, ChangeFlag: #5", outInfo.getCmsChargeFlag(), outInfo.getChargeFlag(), outInfo.getDurationInDay(), outInfo.getUnitOfCharge(), outInfo.getMedOrderItemIndex(), outInfo.isChangeFlag());
				for( MoiCapdChargeInfo outCapdInfo : outInfo.getMoiCapdChargeInfoList()  ){
					logger.debug("updateCapdMedOrderItemChargeFlag==================CapdItemIndex:#0, DurationInDay:#1, UnitOfCharge:#2, ChangeFlag: #3", outCapdInfo.getMedOrderCapdItemIndex(), outCapdInfo.getDurationInDay(), outCapdInfo.getUnitOfCharge(), outCapdInfo.isChangeFlag());
				}
			}
		}
	}
	
	private void updateChargeableCapdItem(Map<Integer,MoiChargeInfo> chargeInfoMap, MoiCapdChargeInfo chargeCapdItem){
		MoiChargeInfo moiChargeItemInfo = chargeInfoMap.get(chargeCapdItem.getMedOrderItemIndex());
		
		for( MoiCapdChargeInfo capdInfo : moiChargeItemInfo.getMoiCapdChargeInfoList() ){
			if( capdInfo.getMedOrderCapdItemIndex().equals(chargeCapdItem.getMedOrderCapdItemIndex()) ){
				BigDecimal unitOfCharge = BigDecimal.valueOf(capdInfo.getDurationInDay()).divide(BigDecimal.valueOf(CHARGING_STANDARD_CHARGEABLEUNIT.get(112)), RoundingMode.CEILING);
				capdInfo.setUnitOfCharge(unitOfCharge.intValue());

				if( !capdInfo.getUnitOfCharge().equals(capdInfo.getInitUnitOfCharge()) ){
					capdInfo.setChangeFlag(true);
					moiChargeItemInfo.setChangeFlag(true);
				}
				break;
			}
		}
		chargeInfoMap.put(moiChargeItemInfo.getMedOrderItemIndex(), moiChargeItemInfo);
	}
	
	private void updateMedOrderItemCmsFmStatusFlagByOrderEdit(MedOrder medOrder, MedOrderItem medOrderItemIn, PharmOrder pharmOrder){
		MedCase medCase = pharmOrder.getMedCase();
		List<MedOrderFm> medOrderFmList = medOrder.getMedOrderFmList();
		
		boolean privateFlag;
		if (medCase != null) {
			if ( medCase.getPasPatInd() == MedCasePasPatInd.Private ){
				privateFlag = true;
			} else {
				privateFlag = false;
			}
		} else {
			privateFlag = false;
		}
		
		if (medOrderItemIn.getRxItemType() == RxItemType.Iv) {
			// since IV item of discharge order cannot be modified in pms-pms (also privateFlag cannot be 
			// changed), original formulStatus2 in DO1/AR6 can be used, there is no need to set cmsFmStatus 
			// for VO7 in pms-pms 
			medOrderItemIn.setCmsFmStatus(null);
		} else if (privateFlag) {
			medOrderItemIn.setCmsFmStatus("P");
		} else if (medOrderItemIn.getRxItemType() == RxItemType.Capd) {
			medOrderItemIn.setCmsFmStatus("F");
		} else {
			FmStatus fmStatus = FmStatus.dataValueOf(medOrderItemIn.getRxDrug().getFmStatus());
			
			if (fmStatus == FmStatus.FreeTextEntryItem) {
				medOrderItemIn.setCmsFmStatus("T");
			} else if ( fmStatus == FmStatus.SelfFinancedItemC || fmStatus == FmStatus.SelfFinancedItemD ||
					fmStatus == FmStatus.SafetyNetItem || fmStatus == FmStatus.UnRegisteredDrug ||
					fmStatus == FmStatus.SampleItem || fmStatus == FmStatus.SpecialDrug) {
				MedOrderFm medOrderFm = retrieveLocalFm(medOrderItemIn, medOrderFmList);
				if (medOrderFm == null) {
					medOrderItemIn.setCmsFmStatus(null);
				} else if ( (fmStatus == FmStatus.SelfFinancedItemC || fmStatus == FmStatus.SelfFinancedItemD) &&
						medOrderFm.getAuthDoctorName() == null && medOrderFm.getCorpIndCode() == null) {
					medOrderItemIn.setCmsFmStatus("L");
				} else {
					medOrderItemIn.setCmsFmStatus(fmStatus.getDataValue());
				}
			} else {
				medOrderItemIn.setCmsFmStatus(fmStatus.getDataValue());
			}
		}		
	}
	
	private MedOrderFm retrieveLocalFm(MedOrderItem medOrderItemIn, List<MedOrderFm> medOrderFmList) {
		for (MedOrderFm medOrderFm : medOrderFmList) {
			if (medOrderItemIn.getStrengthCompulsory()) {
				if ( StringUtils.equals(medOrderFm.getDisplayName(), medOrderItemIn.getDisplayName()) &&
						StringUtils.equals(medOrderFm.getFormCode(), medOrderItemIn.getFormCode()) &&
						StringUtils.equals(medOrderFm.getSaltProperty(), medOrderItemIn.getSaltProperty()) && 
						medOrderFm.getFmStatus() == FmStatus.dataValueOf(medOrderItemIn.getFmStatus()) &&
						StringUtils.equals(medOrderFm.getItemCode(), medOrderItemIn.getItemCode()) ) {
					return medOrderFm;
				}
			} else {
				if ( StringUtils.equals(medOrderFm.getDisplayName(), medOrderItemIn.getDisplayName()) &&
						StringUtils.equals(medOrderFm.getFormCode(), medOrderItemIn.getFormCode()) &&
						StringUtils.equals(medOrderFm.getSaltProperty(), medOrderItemIn.getSaltProperty()) &&
						medOrderFm.getFmStatus() == FmStatus.dataValueOf(medOrderItemIn.getFmStatus()) ) {
					return medOrderFm;
				}
			}
		}
		
		return null;
	}
	
	public Boolean isDhMappingUpToDate(int index) {
		
		MedOrderItem medOrderItem = ((PharmOrder) Contexts.getSessionContext().get("pharmOrder")).getMedOrder().getMedOrderItemList().get(index);
		
		Pair<List<WorkstoreDrug>, DmDrug> mappingResult = workstoreDrugListManager.getWorkstoreDrugListDmDrugByDhRxDrug(medOrderItem.getDhRxDrug());
		List<WorkstoreDrug> workstoreDrugList = mappingResult.getFirst();
		workstoreDrugList = workstoreDrugList == null ? new ArrayList<WorkstoreDrug>() : workstoreDrugList;
		if (mappingResult.getSecond() != null && mappingResult.getSecond().getDmDrugProperty() != null) {
			workstoreDrugList.addAll(workstoreDrugCacher.getWorkstoreDrugListByDisplayName(workstore, mappingResult.getSecond().getDmDrugProperty().getDisplayname()));
		} else {
			if (!StringUtils.equals(medOrderItem.getDhRxDrug().getMapStatus(), "N")) {
				return false;
			}
		}
		
		for (PharmOrderItem pharmOrderItem : medOrderItem.getPharmOrderItemList()) {
			DmDrug dmDrug = DmDrugCacher.instance().getDmDrug(pharmOrderItem.getItemCode());
			if ("Y".equals(dmDrug.getDmMoeProperty().getSolventItem())) {
				continue;
			}
			boolean matchFlag = false;
			for (WorkstoreDrug workstoreDrug : workstoreDrugList) {
				if (dmDrug.getDmDrugProperty().getDisplayname().equals(workstoreDrug.getDmDrug().getDmDrugProperty().getDisplayname())) {
					matchFlag = true;
					break;
				}
			}
			if (matchFlag) {
				continue;
			}
			else {
				return false;
			}
		}
		
		return true;
	}	
	
	@Remove
	public void destroy() {		
		if (medOrderItem != null) {
			medOrderItem = null;
		}
		if(moiChargeInfoList != null){
			moiChargeInfoList = null;
		}
		if (rxItemForOrderEdit != null) {
			rxItemForOrderEdit = null;
		}
	}
	
}
