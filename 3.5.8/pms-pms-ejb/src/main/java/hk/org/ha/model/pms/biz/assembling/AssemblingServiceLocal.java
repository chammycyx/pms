package hk.org.ha.model.pms.biz.assembling;

import hk.org.ha.model.pms.vo.assembling.AssembleItem;

import java.util.Date;

import javax.ejb.Local;

@Local
public interface AssemblingServiceLocal {

	AssembleItem updateDispOrderItemStatusToAssembled(Date ticketDate, String ticketNum, Integer itemNum);
	
	boolean isUpdateSuccess();
	
	String getErrMsg();
	
	void destroy();
	
}