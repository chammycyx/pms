package hk.org.ha.model.pms.persistence.medprofile;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.model.pms.vo.medprofile.ErrorInfo;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "MED_PROFILE_ERROR_STAT")
public class MedProfileErrorStat {
	
	private static final String JAXB_CONTEXT_MEDPROFILE = "hk.org.ha.model.pms.vo.medprofile";	
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "medProfileErrorStatSeq")
	@SequenceGenerator(name = "medProfileErrorStatSeq", sequenceName = "SQ_MED_PROFILE_ERROR_STAT", initialValue = 100000000)
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DUE_DATE")
	private Date dueDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ERROR_DATE")
	private Date errorDate;

	@Lob
	@Column(name = "ERROR_XML")
	private String errorXml;
	
	@Column(name = "SUSPEND_FLAG", nullable = false)
	private Boolean suspendFlag;

	@Column(name = "MISS_SPEC_FLAG", nullable = false)
	private Boolean missSpecFlag;

	@Column(name = "MDS_ERROR_FLAG", nullable = false)
	private Boolean mdsErrorFlag;
	
	@Column(name = "FROZEN_FLAG")
	private Boolean frozenFlag;
	
	@Column(name = "CHECK_USER", length = 12)
	private String checkUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CHECK_DATE")
	private Date checkDate;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MED_PROFILE_ID", nullable = false)
	private MedProfile medProfile;
	
	@Column(name = "MED_PROFILE_ID", nullable = false, insertable = false, updatable = false)
	private Long medProfileId;		
	
	@Transient
	private ErrorInfo errorInfo;
	
	public MedProfileErrorStat() {
		reset();
	}
	
	@PostLoad
	public void postLoad() {
    	if (errorXml != null) {
    		JaxbWrapper<ErrorInfo> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_MEDPROFILE);
    		errorInfo = rxJaxbWrapper.unmarshall(errorXml);
    	}
	}
	
	@PrePersist
    @PreUpdate
    public void preSave() {
		if (errorInfo != null) {
			JaxbWrapper<ErrorInfo> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_MEDPROFILE);
			errorXml = rxJaxbWrapper.marshall(errorInfo);
		}
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDueDate() {
		return dueDate == null ? null: new Date(dueDate.getTime());
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate == null ? null: new Date(dueDate.getTime());
	}

	public Date getErrorDate() {
		return errorDate == null ? null: new Date(errorDate.getTime());
	}

	public void setErrorDate(Date errorDate) {
		this.errorDate = errorDate == null ? null: new Date(errorDate.getTime());
	}
	
	public String getErrorXml() {
		return errorXml;
	}

	public void setErrorXml(String errorXml) {
		this.errorXml = errorXml;
	}
	
	public ErrorInfo getErrorInfo() {
		return errorInfo;
	}

	public void setErrorInfo(ErrorInfo errorInfo) {
		this.errorInfo = errorInfo;
	}

	public Boolean getSuspendFlag() {
		return suspendFlag;
	}

	public void setSuspendFlag(Boolean suspendFlag) {
		this.suspendFlag = suspendFlag;
	}

	public Boolean getMissSpecFlag() {
		return missSpecFlag;
	}

	public void setMissSpecFlag(Boolean missSpecFlag) {
		this.missSpecFlag = missSpecFlag;
	}

	public Boolean getMdsErrorFlag() {
		return mdsErrorFlag;
	}

	public void setMdsErrorFlag(Boolean mdsErrorFlag) {
		this.mdsErrorFlag = mdsErrorFlag;
	}
	
	public Boolean getFrozenFlag() {
		return frozenFlag;
	}

	public void setFrozenFlag(Boolean frozenFlag) {
		this.frozenFlag = frozenFlag;
	}

	public String getCheckUser() {
		return checkUser;
	}

	public void setCheckUser(String checkUser) {
		this.checkUser = checkUser;
	}
	
	public Date getCheckDate() {
		return checkDate == null ? null : new Date(checkDate.getTime());
	}

	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate == null ? null : new Date(checkDate.getTime());
	}

	public MedProfile getMedProfile() {
		return medProfile;
	}

	public void setMedProfile(MedProfile medProfile) {
		this.medProfile = medProfile;
	}
	
	public Long getMedProfileId() {
		return medProfileId;
	}

	public void setMedProfileId(Long medProfileId) {
		this.medProfileId = medProfileId;
	}

	public void reset() {
		dueDate = null;
		errorDate = null;
		errorXml = null;
		errorInfo = null;
		suspendFlag = Boolean.FALSE;
		missSpecFlag = Boolean.FALSE;
		mdsErrorFlag = Boolean.FALSE;
		frozenFlag = Boolean.FALSE;
	}
	
	public void update(MedProfileItem medProfileItem) {
		
		MedProfileErrorLog errorLog = medProfileItem.getMedProfileErrorLog();
		
		if (errorLog == null)
			return;
		
		updateFlags(errorLog);
		updateErrorDate(errorLog);
		updateErrorInfo(errorLog);
		updateRefillDueDate(medProfileItem);
	}
	
	private void updateRefillDueDate(MedProfileItem medProfileItem) {
		
		List<MedProfilePoItem> medProfilePoItemList = medProfileItem.getMedProfileMoItem().getMedProfilePoItemList();
		if (medProfilePoItemList == null)
			return;
		
		for (MedProfilePoItem medProfilePoItem: medProfilePoItemList) {
			if (dueDate == null || (medProfilePoItem.getDueDate() != null &&
					dueDate.compareTo(medProfilePoItem.getDueDate()) > 0)) {
				setDueDate(medProfilePoItem.getDueDate());
			}
		}
	}
	
	private void updateErrorInfo(MedProfileErrorLog errorLog) {
		
		ErrorInfo itemErrorInfo = errorLog.getErrorInfo();
		if (itemErrorInfo != null && (!itemErrorInfo.getSuspendItemList().isEmpty() || !itemErrorInfo.getFrozenItemList().isEmpty())) {
			if (errorInfo == null) {
				errorInfo = new ErrorInfo();
			}
			errorInfo.getSuspendItemList().addAll(itemErrorInfo.getSuspendItemList());
			errorInfo.getFrozenItemList().addAll(itemErrorInfo.getFrozenItemList());
		}
	}
	
	private void updateFlags(MedProfileErrorLog errorLog) {
		
		switch (errorLog.getType()) {
			case MissingSpecialty:
				missSpecFlag = Boolean.TRUE;
				break;
			case Suspend:
				suspendFlag = Boolean.TRUE;
				break;
			case MdsAlert:
				mdsErrorFlag = Boolean.TRUE;
				break;
			case Frozen:
				frozenFlag = Boolean.TRUE;
				break;
			default:
		}
	}
	
	private void updateErrorDate(MedProfileErrorLog errorLog) {
		
		if (errorDate == null || (errorLog.getCreateDate() != null &&
				errorDate.compareTo(errorLog.getCreateDate()) > 0)) {
			setErrorDate(errorLog.getCreateDate());
		}
	}
}
