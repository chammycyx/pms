package hk.org.ha.model.pms.vo.rx;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Freq")
@ExternalizedBean(type = DefaultExternalizer.class)
public class Freq implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@XmlElement
	private String code;
	
	@XmlElement
	private String[] param;

	@XmlElement(required = true)
	private String desc;

	@XmlTransient
	private String labelDesc;
	
	@XmlElement(required = true)
	private String multiplierType;
	
	public Freq() {
		multiplierType = "-";
	}
	
	public void setCode(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setParam(String[] param) {
		this.param = param;
	}

	public String[] getParam() {
		return param;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getLabelDesc() {
		return labelDesc;
	}

	public void setLabelDesc(String labelDesc) {
		this.labelDesc = labelDesc;
	}

	public String getMultiplierType() {
		return multiplierType;
	}

	public void setMultiplierType(String multiplierType) {
		this.multiplierType = multiplierType;
	}
}
