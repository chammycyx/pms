package hk.org.ha.model.pms.biz.vetting;

import java.util.List;

import hk.org.ha.model.pms.asa.exception.moe.MoeException;
import hk.org.ha.model.pms.dms.vo.PmsFmStatusSearchCriteria;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.vo.vetting.FmIndication;

import javax.ejb.Local;

@Local
public interface FmIndicationServiceLocal {
	
	List<FmIndication> retrieveFmIndicationListById(Long medOrderItemId) throws MoeException;
	
	List<FmIndication> retrieveFmIndicationListByMedOrderItem(MedOrderItem medOrderItem) throws MoeException;
	
	List<FmIndication> retrieveFmIndicationListByMedProfileMoItem(MedProfileMoItem medProfileMoItem) throws MoeException;
	
	void checkFmIndicationForOrderEdit(PmsFmStatusSearchCriteria criteria);
	
	void destroy();
}
