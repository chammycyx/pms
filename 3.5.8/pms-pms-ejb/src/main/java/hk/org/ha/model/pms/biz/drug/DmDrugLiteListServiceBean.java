package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.drug.DmDrugLite;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("dmDrugLiteListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DmDrugLiteListServiceBean implements DmDrugLiteListServiceLocal {

	@Logger
	private Log logger;

	@In
	private Workstore workstore;

	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;
	
	@Out(required = false)
	private List<DmDrugLite> dmDrugLiteList;	
	
	public void retrieveDmDrugLiteList(String prefixItemCode, boolean showNonSuspendItemOnly, boolean fdn){
		
		logger.debug("retrieveDmDrugLiteList - prefixItemCode=#0, showNonSuspendItemOnly=#1, fdn=#2", prefixItemCode, showNonSuspendItemOnly, fdn);
		
		dmDrugLiteList = new ArrayList<DmDrugLite>();
		List<WorkstoreDrug> workstoreDrugList = null;
		
		if ( fdn ) {
			workstoreDrugList = workstoreDrugCacher.getWorkstoreDrugList(workstore, prefixItemCode, null, Boolean.TRUE);
		} else {
				if (showNonSuspendItemOnly) {
				workstoreDrugList = workstoreDrugCacher.getWorkstoreDrugList(workstore, prefixItemCode, Boolean.FALSE, null);
			} else {
				workstoreDrugList = workstoreDrugCacher.getWorkstoreDrugList(workstore, prefixItemCode);
			}
		}
		
		for ( WorkstoreDrug workstoreDrug : workstoreDrugList ) {
			dmDrugLiteList.add(workstoreDrug.getDmDrugLite());
		}
		
		if ( logger.isDebugEnabled() ) {
			logger.debug("dmDrugLiteList size:|#0", dmDrugLiteList.size());
		}
	}
	
	public void retrieveDmDrugLiteListByFullDrugDesc(String prefixFullDrugDesc)
	{	
		dmDrugLiteList = new ArrayList<DmDrugLite>();
	
		List<WorkstoreDrug> workstoreDrugList = workstoreDrugCacher.getWorkstoreDrugListByFullDrugDesc(workstore, prefixFullDrugDesc);		
		if ( workstoreDrugList == null ) {
			logger.debug("dmDrugLiteList is null");
			return;
		}
		
		for ( WorkstoreDrug workstoreDrug : workstoreDrugList ) {
			dmDrugLiteList.add(workstoreDrug.getDmDrugLite());
		}
		
		if ( logger.isDebugEnabled() ) {
			logger.debug("dmDrugLiteList size #0", workstoreDrugList.size());
		}
	}
	
	public void retrieveDmDrugLiteListForChestItem(String prefixItemCode){
		dmDrugLiteList = new ArrayList<DmDrugLite>();
		
		List<WorkstoreDrug> workstoreDrugList = workstoreDrugCacher.getWorkstoreDrugListForChestItem(workstore, prefixItemCode);	
		if ( workstoreDrugList == null ) {
			logger.debug("retrieveDmDrugLiteListForChestItem - dmDrugLiteList is null");
			return;
		}
		
		for ( WorkstoreDrug workstoreDrug : workstoreDrugList ) {
			dmDrugLiteList.add(workstoreDrug.getDmDrugLite());
		}
		
		if ( logger.isDebugEnabled() ) {
			logger.debug("retrieveDmDrugLiteListForChestItem - dmDrugLiteList size #0", workstoreDrugList.size());
		}
	}
	
	@Remove
	public void destroy() 
	{
		if (dmDrugLiteList != null) {
			dmDrugLiteList = null;
		}
	}
}
