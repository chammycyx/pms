package hk.org.ha.model.pms.biz.enquiry;

import java.util.Date;

import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.vo.charging.DrugCheckClearanceResult;

import javax.ejb.Local;

@Local
public interface DrugChargeClearanceEnqManagerLocal {

	DrugCheckClearanceResult checkClearanceByMoeOrderNo(String medOrderNum);
	DrugCheckClearanceResult checkClearanceByInvoiceNumber(String invoiceNum);
	DrugCheckClearanceResult checkClearanceByTicketNum(String ticketNum, Date ticketDate);
	
	DrugCheckClearanceResult checkClearanceForOnestop(Long medOrderId);
	DrugCheckClearanceResult checkClearanceForOnestopManual(String patHospCode, Long orderNum, Long pasApptSeq);
	DrugCheckClearanceResult checkClearanceForCheckIssue(DispOrder dispOrder);
}
