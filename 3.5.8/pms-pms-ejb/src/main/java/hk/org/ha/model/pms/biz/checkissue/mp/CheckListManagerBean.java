package hk.org.ha.model.pms.biz.checkissue.mp;

import static hk.org.ha.model.pms.prop.Prop.CHECK_MEDPROFILE_CHECKLIST_DURATION;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.medprofile.DeliveryItemStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryRequestStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryStatus;
import hk.org.ha.model.pms.vo.checkissue.mp.CheckDeliveryInfo;
import hk.org.ha.model.pms.vo.checkissue.mp.DrugCheckViewListSummary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;

@AutoCreate
@Stateless
@Name("checkListManager")
@MeasureCalls
public class CheckListManagerBean implements CheckListManagerLocal {
	
	@PersistenceContext
	private EntityManager em;

	@In
	private Workstore workstore;
	
	private static CheckDeliveryInfoComparator checkDeliveryInfoComparator = new CheckDeliveryInfoComparator();
	
	public DrugCheckViewListSummary retrieveDrugCheckViewListSummary() {
		DrugCheckViewListSummary drugCheckViewListSummary = new DrugCheckViewListSummary();
		drugCheckViewListSummary.setCheckedDeliveryList(retrieveCheckedDeliveryList());
		drugCheckViewListSummary.setReadyForCheckDeliveryList(retrieveReadyForCheckDeliveryList());
		return drugCheckViewListSummary;
	}
	
	@SuppressWarnings("unchecked")
	private List<CheckDeliveryInfo> retrieveCheckedDeliveryList() {
		
		DateTime toDate = new DateTime();
		DateMidnight fromDate = toDate.toDateMidnight();
		
		return em.createQuery(
				"select new " + CheckDeliveryInfo.class.getName() + // 20120317 index check : Delivery.hospital,checkDate : I_DELIVERY_02
				" (o.batchNum, o.batchDate, o.printMode, o.wardCode, o.checkDate, o.updateDate, o.id)" +
				" from Delivery o" + 
				" where o.hospital = :hospital" +
				" and o.checkDate between :fromDate and :toDate" +
				" and o.deliveryRequest.status = :deliveryRequestStatus" +
				" order by o.checkDate desc, o.batchNum desc ")
				.setParameter("hospital", workstore.getHospital())
				.setParameter("fromDate", fromDate.toDate())
				.setParameter("toDate", toDate.toDate())
				.setParameter("deliveryRequestStatus", DeliveryRequestStatus.Completed)
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	private List<CheckDeliveryInfo> retrieveReadyForCheckDeliveryList(){
		List<CheckDeliveryInfo> readyForCheckDeliveryList = new ArrayList<CheckDeliveryInfo>();
		List<CheckDeliveryInfo> checkDeliveryInfoList = em.createQuery(
				"select new " + CheckDeliveryInfo.class.getName() + // 20161128 index check : Delivery.hospCode,batchDate : I_DELIVERY_01
				" (o.delivery.batchNum, o.delivery.batchDate, o.delivery.printMode, o.delivery.wardCode, o.delivery.checkDate, o.delivery.updateDate, o.delivery.id)" +
				" from DeliveryItem o" + 
				" where o.delivery.hospital = :hospital" +
				" and o.delivery.deliveryRequest.status = :deliveryRequestStatus" +
				" and o.delivery.status = :deliveryStatus" +
				" and o.status = :deliveryItemStatus" +
				" and o.delivery.batchDate between :startDate and :endDate" +
				" order by o.delivery.batchDate, o.delivery.batchNum")
				.setParameter("hospital", workstore.getHospital())
				.setParameter("deliveryRequestStatus", DeliveryRequestStatus.Completed)
				.setParameter("deliveryStatus", DeliveryStatus.Printed)
				.setParameter("deliveryItemStatus", DeliveryItemStatus.Printed)
				.setParameter("startDate", new DateTime().minusDays(CHECK_MEDPROFILE_CHECKLIST_DURATION.get()-1).toDate())
				.setParameter("endDate", new Date())
				.getResultList();
		
		Map<Long, CheckDeliveryInfo> checkDeliveryInfoMap = new HashMap<Long, CheckDeliveryInfo>();
		for ( CheckDeliveryInfo checkDeliveryInfo : checkDeliveryInfoList ) {
			if ( checkDeliveryInfoMap.containsKey(checkDeliveryInfo.getDeliveryId() ) ){
				continue;
			}
			checkDeliveryInfoMap.put(checkDeliveryInfo.getDeliveryId(), checkDeliveryInfo);
		}
		
		readyForCheckDeliveryList.addAll(checkDeliveryInfoMap.values());
		Collections.sort(readyForCheckDeliveryList, checkDeliveryInfoComparator);
		
		return readyForCheckDeliveryList;
	}
	
    private static class CheckDeliveryInfoComparator implements Comparator<CheckDeliveryInfo>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(CheckDeliveryInfo checkDeliveryInfo1, CheckDeliveryInfo checkDeliveryInfo2) {
			int result = checkDeliveryInfo1.getBatchDate().compareTo(checkDeliveryInfo2.getBatchDate());
			
			if ( result != 0 ) {
				return result;
			}
			
			return checkDeliveryInfo1.getBatchNum().compareTo(checkDeliveryInfo2.getBatchNum());
		}
    }
}
