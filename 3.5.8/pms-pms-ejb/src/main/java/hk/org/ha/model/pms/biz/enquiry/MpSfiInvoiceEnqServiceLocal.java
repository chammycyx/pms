package hk.org.ha.model.pms.biz.enquiry;

import hk.org.ha.model.pms.udt.enquiry.SfiInvoiceEnqPatType;

import java.util.Date;
import javax.ejb.Local;

@Local
public interface MpSfiInvoiceEnqServiceLocal {
	void retrieveMpSfiInvoiceByInvoiceNumber(String invoiceNum, boolean includeVoidInvoice, SfiInvoiceEnqPatType patType);
	
	void retrieveMpSfiInvoiceByInvoiceDate(Date invoiceDate, boolean includeVoidInvoice, SfiInvoiceEnqPatType patType);
	
	void retrieveMpSfiInvoiceByCaseNum(String caseNum, boolean includeVoidInvoice, SfiInvoiceEnqPatType patType);
	
	void retrieveMpSfiInvoiceByHkid(String hkid, boolean includeVoidInvoice, SfiInvoiceEnqPatType patType);
	
	void retrieveMpSfiInvoiceByExtactionPeriod(Integer dayRange, boolean includeVoidInvoice, SfiInvoiceEnqPatType patType);
	
	void retrieveMpSfiInvoicePaymentStatus(Long invoiceId, Long purchaseReqId);
	
	void retrieveMpSfiInvoiceDetail(Long invoiceId, Long purchaseReqId, boolean isPreview);
	
	String getErrMsg();
	
	boolean isRetrieveSuccess();
	
	void destroy();	
}