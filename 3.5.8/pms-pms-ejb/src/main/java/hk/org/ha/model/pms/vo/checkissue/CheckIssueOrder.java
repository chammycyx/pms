package hk.org.ha.model.pms.vo.checkissue;

import hk.org.ha.model.pms.udt.disp.MedOrderDocType;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class CheckIssueOrder implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Date ticketDate;
	
	private String ticketNum;
	
	private Boolean urgentFlag;
	
	private MedOrderDocType docType;
	
	private Long dispOrderId;
	
	private Integer issueWindowNum;
	
	private String checkWorkstationCode;
	
	public CheckIssueOrder() {
		super();
	}
	
	public CheckIssueOrder(Long dispOrderId, Date ticketDate, String ticketNum, Integer issueWindowNum, Boolean urgentFlag, MedOrderDocType docType, String checkWorkstationCode) {
		super();
		this.dispOrderId = dispOrderId;
		this.ticketDate = ticketDate;
		this.ticketNum = ticketNum;
		this.issueWindowNum = issueWindowNum;
		this.urgentFlag = urgentFlag;
		this.docType = docType;
		this.checkWorkstationCode = checkWorkstationCode;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public void setUrgentFlag(Boolean urgentFlag) {
		this.urgentFlag = urgentFlag;
	}

	public Boolean getUrgentFlag() {
		return urgentFlag;
	}

	public void setDocType(MedOrderDocType docType) {
		this.docType = docType;
	}

	public MedOrderDocType getDocType() {
		return docType;
	}

	public void setDispOrderId(Long dispOrderId) {
		this.dispOrderId = dispOrderId;
	}

	public Long getDispOrderId() {
		return dispOrderId;
	}

	public void setIssueWindowNum(Integer issueWindowNum) {
		this.issueWindowNum = issueWindowNum;
	}

	public Integer getIssueWindowNum() {
		return issueWindowNum;
	}

	public void setCheckWorkstationCode(String checkWorkstationCode) {
		this.checkWorkstationCode = checkWorkstationCode;
	}

	public String getCheckWorkstationCode() {
		return checkWorkstationCode;
	}

	public void setTicketDate(Date ticketDate) {
		this.ticketDate = ticketDate;
	}

	public Date getTicketDate() {
		return ticketDate;
	}
}