package hk.org.ha.model.pms.vo.cddh;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class PatientCddhDoseGroup implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@XmlElement
	private Date startDate;
	
	@XmlElement
	private Date endDate;
	
	@XmlElement(name="patientCddhDose")
	private List<PatientCddhDose> patientCddhDoseList;
	
	public PatientCddhDoseGroup() {
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public List<PatientCddhDose> getPatientCddhDoseList() {
		if (patientCddhDoseList == null) {
			patientCddhDoseList = new ArrayList<PatientCddhDose>();
		}
		return patientCddhDoseList;
	}

	public void setPatientCddhDoseList(List<PatientCddhDose> patientCddhDoseList) {
		this.patientCddhDoseList = patientCddhDoseList;
	}
	
	public void addPatientCddhDose(PatientCddhDose patientCddhDose) {
		getPatientCddhDoseList().add(patientCddhDose);
	}
}
