package hk.org.ha.model.pms.biz.vetting;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.exception.vetting.VettingException;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;
import hk.org.ha.model.pms.vo.vetting.RetrievePatientResult;

import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("orderViewService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class OrderViewServiceBean implements OrderViewServiceLocal {

	@In
	private OrderViewManagerLocal orderViewManager;
	
	@Override
	public void cancelOrder(Boolean suspendFlag, String suspendCode) {
		orderViewManager.cancelOrder(suspendFlag, suspendCode);
	}

	@Override
	public void removeMedOrderItem(OrderViewInfo inOrderViewInfo,
			int itemIndex, Date remarkCreateDate, String remarkCreateUser,
			String remarkText) {
		orderViewManager.removeMedOrderItem(inOrderViewInfo, itemIndex, remarkCreateDate, remarkCreateUser, remarkText);
	}

	@Override
	public void removeOrder(String logonUser, String remarkText) {
		orderViewManager.removeOrder(logonUser, remarkText);
	}

	@Override
	public void removeSfiRefillOrder(Long refillScheduleId) {
		orderViewManager.removeSfiRefillOrder(refillScheduleId);
	}

	@Override
	public void retrieveOrder(Long id, OneStopOrderType orderType, Boolean requiredLockOrder) {
		orderViewManager.retrieveOrder(id, orderType, requiredLockOrder);
	}

	@Override
	public RetrievePatientResult retrievePatientByHkid(String hkid) {
		return orderViewManager.retrievePatientByHkid(hkid);
	}

	@Override
	public void saveOrder(OrderViewInfo orderViewInfo) throws VettingException {
		orderViewManager.saveOrder(orderViewInfo);
	}

	@Override
	public void updateOrderFromOrderView(OrderViewInfo inOrderViewInfo) {
		orderViewManager.updateOrderFromOrderView(inOrderViewInfo);
	}

	@Override
	public void updatePharmRemark(OrderViewInfo inOrderViewInfo, int itemIndex,
			Date remarkCreateDate, String remarkCreateUser, String remarkText) {
		orderViewManager.updatePharmRemark(inOrderViewInfo, itemIndex, remarkCreateDate, remarkCreateUser, remarkText);
	}

	@Override
	public List<String> usePatientFromHkpmi(Patient patient) {
		return orderViewManager.usePatientFromHkpmi(patient);
	}

	@Remove
	public void destroy() {
	}
}
