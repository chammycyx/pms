package hk.org.ha.model.pms.biz.enquiry;

import java.util.Date;

import javax.ejb.Local;

@Local
public interface OrderPendingSuspendLogEnqServiceLocal {
	void retrieveOrderPendingSuspendLogEnqList(Date fromDate, Date toDate, String caseNum, String filter);
	void destroy();	
}