package hk.org.ha.model.pms.biz.delivery;
import hk.org.ha.model.pms.persistence.medprofile.PurchaseRequest;
import hk.org.ha.model.pms.udt.medprofile.RetrievePurchaseRequestOption;

import java.util.List;

import javax.ejb.Local;

@Local
public interface PurchaseRequestListServiceLocal {
	List<PurchaseRequest> retrievePurchaseRequestList(
			RetrievePurchaseRequestOption option);
	void destroy();
}
