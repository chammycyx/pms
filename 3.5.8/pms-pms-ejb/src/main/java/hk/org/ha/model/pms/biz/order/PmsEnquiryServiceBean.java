package hk.org.ha.model.pms.biz.order;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.reftable.RefTableManagerLocal;
import hk.org.ha.model.pms.corp.vo.moe.DispenseStatusInfo;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.udt.medprofile.DeliveryItemStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileMoItemStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.label.MpDispLabel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@Stateless
@Name("pmsEnquiryService")
@MeasureCalls
public class PmsEnquiryServiceBean implements PmsEnquiryServiceLocal {

	private static final char[] DELIMITER = new char[] {' ' , '~' , '&' , '-' , '[' , '(' , '<'};
	
	private static final String JAXB_CONTEXT_LABEL = "hk.org.ha.model.pms.vo.label";
	
	private static JaxbWrapper<MpDispLabel> labelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private RefTableManagerLocal refTableManager;
	
	@SuppressWarnings("unchecked")
	public boolean allowDeleteMpItem(String clusterCode, String orderNum, Integer itemNum) {
		
		List<MedProfileItem> medProfileItemList = em.createQuery(
				"select o from MedProfileItem o" + // 20151012 index check : MedProfile.orderNum : UI_MED_PROFILE_01
				" where o.medProfile.orderNum = :orderNum" +
				" and o.medProfileMoItem.itemNum = :itemNum" +
				" order by o.createDate, o.id")
				.setParameter("orderNum", orderNum)
				.setParameter("itemNum", itemNum)
				.getResultList();
		
		return !medProfileItemList.isEmpty() && medProfileItemList.get(0).getStatus() == MedProfileItemStatus.Unvet;
	}

	@SuppressWarnings("unchecked")
	public List<DispenseStatusInfo> retrieveDispenseStatusInfoList(String clusterCode, String orderNum, Integer itemNum) {
		
		List<MedProfileItem> medProfileItemList = em.createQuery(
				"select o from MedProfileItem o " + // 20151012 index check : MedProfile.orderNum : UI_MED_PROFILE_01
				"where o.medProfile.orderNum = :orderNum " +
				"and o.medProfileMoItem.itemNum = :itemNum " +
				"and o.status <> :medProfileItemStatus " +
				"and o.medProfileMoItem.status <> :medProfileMoItemStatus " +
				"order by o.createDate, o.id ")
				.setParameter("orderNum", orderNum)
				.setParameter("itemNum", itemNum)
				.setParameter("medProfileItemStatus", MedProfileItemStatus.Deleted)
				.setParameter("medProfileMoItemStatus", MedProfileMoItemStatus.SysDeleted)
				.setHint(QueryHints.FETCH, "o.medProfile")
				.setHint(QueryHints.FETCH, "o.medProfileMoItem.medProfilePoItemList")
				.getResultList();
		
		Map<Long, Date> lastDeliveryDateMap = new HashMap<Long, Date>();
		Map<Long, String> processingRefillMap = new HashMap<Long, String>();
		Map<Long, DeliveryItem> lastDeliveryItemMap = new HashMap<Long, DeliveryItem>();

		for (MedProfileItem mpi : medProfileItemList) {
			for (MedProfilePoItem poItem : mpi.getMedProfileMoItem().getMedProfilePoItemList()) {
				if (poItem.getLastDeliveryItemId() != null) {
					lastDeliveryDateMap.put(poItem.getLastDeliveryItemId(), null);
					processingRefillMap.put(poItem.getLastDeliveryItemId(), null);
				}
			}
		}
		
		if (!lastDeliveryDateMap.isEmpty()) {
			List<DeliveryItem> deliveryItemList = em.createQuery(
					"select o from DeliveryItem o " + // 20151012 index check : DeliveryItem.id : PK_DELIVERY_ITEM
					"where o.id in :idSet " +
					"and o.status not in :deliveryItemStatus ")
					.setParameter("idSet", lastDeliveryDateMap.keySet())
					.setParameter("deliveryItemStatus", Arrays.asList(DeliveryItemStatus.Deleted))
					.setHint(QueryHints.FETCH, "o.delivery")
					.setHint(QueryHints.FETCH, "o.medProfilePoItem")
					.getResultList();
		
			for (DeliveryItem deliveryItem : deliveryItemList) {
				lastDeliveryItemMap.put(deliveryItem.getId(), deliveryItem);
				if (DeliveryItemStatus.KeepRecord_Unlink.contains(deliveryItem.getStatus())) {
					continue;
				}
				lastDeliveryDateMap.put(deliveryItem.getId(), deliveryItem.getDelivery().getDeliveryDate());
				if (deliveryItem.getDelivery().getDeliveryDate() == null) {
					processingRefillMap.put(deliveryItem.getId(), deliveryItem.getRefillFlag() ? "R" : "N");
				}
			}
		}
		
		List<DispenseStatusInfo> dispenseStatusInfoList = new ArrayList<DispenseStatusInfo>();
		
		for (MedProfileItem mpi : medProfileItemList) {
			for (MedProfilePoItem poItem : mpi.getMedProfileMoItem().getMedProfilePoItemList()) {
				DispenseStatusInfo dispenseStatusInfo = new DispenseStatusInfo();
				dispenseStatusInfo.setItemCode(poItem.getItemCode());
				String synonym;
				if (poItem.getLastDeliveryItemId() != null && lastDeliveryItemMap.containsKey(poItem.getLastDeliveryItemId())) {
					synonym = labelJaxbWrapper.unmarshall(lastDeliveryItemMap.get(poItem.getLastDeliveryItemId()).getLabelXml()).getFloatingDrugName();
				}
				else {
					synonym = refTableManager.getFdnByItemCodeOrderTypeWorkstore(poItem.getItemCode(), OrderType.InPatient, mpi.getMedProfile().getWorkstore());
				}
				dispenseStatusInfo.setItemDesc(buildDrugDesc(
						poItem.getDrugName(),
						poItem.getFormLabelDesc(),
						poItem.getStrength(),
						poItem.getVolumeText(),
						synonym)
					);
				if (poItem.getLastDeliveryItemId() != null) {
					dispenseStatusInfo.setLastDeliveryDate(lastDeliveryDateMap.get(poItem.getLastDeliveryItemId()));
					dispenseStatusInfo.setProcessingRefill(processingRefillMap.get(poItem.getLastDeliveryItemId()));
				}
				if (
					!poItem.getSingleDispFlag() &&
					!(poItem.getRemainIssueQty() != null && BigDecimal.ZERO.compareTo(poItem.getRemainIssueQty())==0) &&
					(!poItem.getWardStockFlag() || poItem.getReDispFlag()) &&
					(poItem.getEndDate() == null || poItem.getDueDate().before(poItem.getEndDate()))
				) {
					dispenseStatusInfo.setDueDate(poItem.getDueDate());
				}
				dispenseStatusInfoList.add(dispenseStatusInfo);
			}
		}
		
		return dispenseStatusInfoList;
	}
	
	private String buildDrugDesc(String drugName, String formLabelDesc, String strength, String volumeText, String synonym){
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(WordUtils.capitalizeFully(drugName, DELIMITER));
		if (StringUtils.isNotBlank(formLabelDesc)){
			sb.append(" ").append(WordUtils.capitalizeFully(formLabelDesc, DELIMITER));
		}
		if (StringUtils.isNotBlank(strength)){
			sb.append(" ").append(WordUtils.capitalizeFully(strength, DELIMITER));
		}
		if (StringUtils.isNotBlank(volumeText)){
			sb.append(" ").append(volumeText.toLowerCase());
		}
		if (StringUtils.isNotBlank(synonym)){
			sb.append(" (").append(synonym.toUpperCase()).append(")");
		}
		
		return sb.toString();
		
	}
}
