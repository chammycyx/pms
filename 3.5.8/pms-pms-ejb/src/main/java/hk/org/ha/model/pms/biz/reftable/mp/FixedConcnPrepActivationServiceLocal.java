package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.PmsFixedConcnStatus;
import hk.org.ha.model.pms.vo.reftable.mp.FixedConcnStatusInfo;

import java.util.List;

import javax.ejb.Local;

@Local
public interface FixedConcnPrepActivationServiceLocal {

	FixedConcnStatusInfo retrieveFixedConcnStatusInfo( DmDrug dmDrug );
	
	FixedConcnStatusInfo retrievePmsFixedConcnStatusList( String patHospCode, DmDrug dmDrug );
	
	public void updateFixedConcnStatusList( List<PmsFixedConcnStatus> pmsFixedConcnStatusList );
	
	void destroy();
	
}
 