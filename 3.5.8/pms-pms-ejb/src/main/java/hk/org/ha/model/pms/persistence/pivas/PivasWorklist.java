package hk.org.ha.model.pms.persistence.pivas;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.udt.pivas.PivasWorklistStatus;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "PIVAS_WORKLIST")
public class PivasWorklist extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pivasWorklistSeq")
	@SequenceGenerator(name = "pivasWorklistSeq", sequenceName = "SQ_PIVAS_WORKLIST", initialValue = 100000000)
	private Long id;
	
	@Column(name = "WORKSTORE_GROUP_CODE", nullable = false, length = 3)
	private String workstoreGroupCode;
	
	@Column(name = "SUPPLY_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date supplyDate;
	
	@Column(name = "DUE_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dueDate;
	
	@Column(name = "EXPIRY_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date expiryDate;
	
	@Column(name = "NEXT_DUE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date nextDueDate;
	
	@Column(name = "REFILL_FLAG", nullable = false)
	private Boolean refillFlag;
		
	@Column(name = "ISSUE_QTY", nullable = false, precision = 19, scale = 4)
	private BigDecimal issueQty;
	
	@Column(name = "ADJ_QTY", nullable = false, precision = 19, scale = 4)
	private BigDecimal adjQty;

	@Column(name = "MODIFY_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifyDate;
	
	@Column(name = "MR_ISSUE_QTY", nullable = false, precision = 19, scale = 4)
	private BigDecimal mrIssueQty;
	
	@Column(name = "SATELLITE_FLAG", nullable = false)
	private Boolean satelliteFlag;
	
	@Column(name = "SHELF_LIFE")
	private Integer shelfLife;

	@Column(name = "NUM_OF_LABEL")
	private Integer numOfLabel;

	@Column(name = "AD_HOC_FLAG", nullable = false)
	private Boolean adHocFlag;
	
	@Column(name = "PREV_PIVAS_WORKLIST_ID")
	private Long prevPivasWorklistId;
	
	@Converter(name = "PivasWorklist.status", converterClass = PivasWorklistStatus.Converter.class)
    @Convert("PivasWorklist.status")
	@Column(name="STATUS", nullable = false, length = 1)
	private PivasWorklistStatus status;	

	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE",      referencedColumnName = "HOSP_CODE",      nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;
	
	@ManyToOne
	@JoinColumn(name = "PIVAS_PREP_ID")
	private PivasPrep pivasPrep;
	
	@Column(name = "PIVAS_PREP_ID", insertable = false, updatable = false)
	private Long pivasPrepId;	

	@ManyToOne
	@JoinColumn(name = "PIVAS_PREP_METHOD_ID")
	private PivasPrepMethod pivasPrepMethod;

	@Column(name = "PIVAS_PREP_METHOD_ID", insertable = false, updatable = false)
	private Long pivasPrepMethodId;	
	
	@ManyToOne
	@JoinColumn(name = "MATERIAL_REQUEST_ID")
	private MaterialRequest materialRequest;
	
	@Column(name = "MATERIAL_REQUEST_ID", insertable = false, updatable = false)
	private Long materialRequestId;		

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "FIRST_DOSE_MATERIAL_REQUEST_ID")
	private MaterialRequest firstDoseMaterialRequest;

	@Column(name = "FIRST_DOSE_MATERIAL_REQUEST_ID", insertable = false, updatable = false)
	private Long firstDoseMaterialRequestId;		
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MED_PROFILE_ID", nullable = false)
	private MedProfile medProfile;
	
	@Column(name = "MED_PROFILE_ID", nullable = false, insertable = false, updatable = false)
	private Long medProfileId;	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MED_PROFILE_MO_ITEM_ID", nullable = false)
	private MedProfileMoItem medProfileMoItem;

	@Column(name = "MED_PROFILE_MO_ITEM_ID", nullable = false, insertable = false, updatable = false)
	private Long medProfileMoItemId;	
	
	@Transient
	private Date nextSupplyDate;

	@Transient
	private Boolean enableSatelliteFlag;
	
	@Transient
	private Boolean savedFlag;

	@Transient
	private Date checkNextSupplyDate;

	@Transient
	private MaterialRequest prevMaterialRequest;
	
	@Transient
	private Boolean filterFlag;

	@Transient
	private String infoCode;

	@Transient
	private List<String> infoParamList;
	
	@Transient
	private String errorCode;

	@Transient
	private List<String> errorParamList;

	@Transient
	private String errorRemark;

	@Transient
	private String exceedDoseLimitCode;
	
	@Transient
	private Boolean allowEditFlag;
	
	@Transient
	private Boolean resumeDispInPmsIpActionFlag;
	
	public PivasWorklist() {
		status = PivasWorklistStatus.New;
		refillFlag = Boolean.FALSE;
		issueQty = new BigDecimal(0);
		adjQty = new BigDecimal(0);
		mrIssueQty = new BigDecimal(0);
		satelliteFlag = Boolean.FALSE;
		adHocFlag = Boolean.FALSE;
		savedFlag = Boolean.FALSE;
		filterFlag = Boolean.FALSE;
		allowEditFlag = Boolean.TRUE;
		resumeDispInPmsIpActionFlag = Boolean.FALSE;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWorkstoreGroupCode() {
		return workstoreGroupCode;
	}

	public void setWorkstoreGroupCode(String workstoreGroupCode) {
		this.workstoreGroupCode = workstoreGroupCode;
	}

	public Date getSupplyDate() {
		return supplyDate == null ? null : new Date(supplyDate.getTime());
	}

	public void setSupplyDate(Date supplyDate) {
		this.supplyDate = supplyDate == null ? null : new Date(supplyDate.getTime());
	}

	public Date getDueDate() {
		return dueDate == null ? null : new Date(dueDate.getTime());
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate == null ? null : new Date(dueDate.getTime());
	}

	public Date getExpiryDate() {
		return expiryDate == null ? null : new Date(expiryDate.getTime());
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate == null ? null : new Date(expiryDate.getTime());
	}

	public Date getNextDueDate() {
		return nextDueDate == null ? null : new Date(nextDueDate.getTime());
	}

	public void setNextDueDate(Date nextDueDate) {
		this.nextDueDate = nextDueDate == null ? null : new Date(nextDueDate.getTime());
	}
	
	public Boolean getRefillFlag() {
		return refillFlag;
	}

	public void setRefillFlag(Boolean refillFlag) {
		this.refillFlag = refillFlag;
	}

	public BigDecimal getIssueQty() {
		return issueQty;
	}

	public void setIssueQty(BigDecimal issueQty) {
		this.issueQty = issueQty;
	}

	public BigDecimal getAdjQty() {
		return adjQty;
	}

	public void setAdjQty(BigDecimal adjQty) {
		this.adjQty = adjQty;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public BigDecimal getMrIssueQty() {
		return mrIssueQty;
	}

	public void setMrIssueQty(BigDecimal mrIssueQty) {
		this.mrIssueQty = mrIssueQty;
	}

	public Boolean getSatelliteFlag() {
		return satelliteFlag;
	}

	public void setSatelliteFlag(Boolean satelliteFlag) {
		this.satelliteFlag = satelliteFlag;
	}

	public Integer getShelfLife() {
		return shelfLife;
	}

	public void setShelfLife(Integer shelfLife) {
		this.shelfLife = shelfLife;
	}

	public Integer getNumOfLabel() {
		return numOfLabel;
	}

	public void setNumOfLabel(Integer numOfLabel) {
		this.numOfLabel = numOfLabel;
	}

	public Boolean getAdHocFlag() {
		return adHocFlag;
	}

	public void setAdHocFlag(Boolean adHocFlag) {
		this.adHocFlag = adHocFlag;
	}

	public Long getPrevPivasWorklistId() {
		return prevPivasWorklistId;
	}

	public void setPrevPivasWorklistId(Long prevPivasWorklistId) {
		this.prevPivasWorklistId = prevPivasWorklistId;
	}

	public PivasWorklistStatus getStatus() {
		return status;
	}

	public void setStatus(PivasWorklistStatus status) {
		this.status = status;
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}
	
	public PivasPrep getPivasPrep() {
		return pivasPrep;
	}

	public void setPivasPrep(PivasPrep pivasPrep) {
		this.pivasPrep = pivasPrep;
	}
	
	public Long getPivasPrepId() {
		return pivasPrepId;
	}

	public void setPivasPrepId(Long pivasPrepId) {
		this.pivasPrepId = pivasPrepId;
	}

	public PivasPrepMethod getPivasPrepMethod() {
		return pivasPrepMethod;
	}

	public void setPivasPrepMethod(PivasPrepMethod pivasPrepMethod) {
		this.pivasPrepMethod = pivasPrepMethod;
	}

	public Long getPivasPrepMethodId() {
		return pivasPrepMethodId;
	}

	public void setPivasPrepMethodId(Long pivasPrepMethodId) {
		this.pivasPrepMethodId = pivasPrepMethodId;
	}

	public MaterialRequest getMaterialRequest() {
		return materialRequest;
	}

	public void setMaterialRequest(MaterialRequest materialRequest) {
		this.materialRequest = materialRequest;
	}
	
	public Long getMaterialRequestId() {
		return materialRequestId;
	}

	public void setMaterialRequestId(Long materialRequestId) {
		this.materialRequestId = materialRequestId;
	}

	public MaterialRequest getFirstDoseMaterialRequest() {
		return firstDoseMaterialRequest;
	}

	public void setFirstDoseMaterialRequest(MaterialRequest firstDoseMaterialRequest) {
		this.firstDoseMaterialRequest = firstDoseMaterialRequest;
	}
	
	public Long getFirstDoseMaterialRequestId() {
		return firstDoseMaterialRequestId;
	}

	public void setFirstDoseMaterialRequestId(Long firstDoseMaterialRequestId) {
		this.firstDoseMaterialRequestId = firstDoseMaterialRequestId;
	}

	public MedProfile getMedProfile() {
		return medProfile;
	}

	public void setMedProfile(MedProfile medProfile) {
		this.medProfile = medProfile;
	}

	public Long getMedProfileId() {
		return medProfileId;
	}

	public void setMedProfileId(Long medProfileId) {
		this.medProfileId = medProfileId;
	}

	public MedProfileMoItem getMedProfileMoItem() {
		return medProfileMoItem;
	}

	public void setMedProfileMoItem(MedProfileMoItem medProfileMoItem) {
		this.medProfileMoItem = medProfileMoItem;
	}

	public Long getMedProfileMoItemId() {
		return medProfileMoItemId;
	}

	public void setMedProfileMoItemId(Long medProfileMoItemId) {
		this.medProfileMoItemId = medProfileMoItemId;
	}

	public Date getNextSupplyDate() {
		return nextSupplyDate == null ? null : new Date(nextSupplyDate.getTime());
	}

	public void setNextSupplyDate(Date nextSupplyDate) {
		this.nextSupplyDate = nextSupplyDate == null ? null : new Date(nextSupplyDate.getTime());
	}
	
	public Boolean getEnableSatelliteFlag() {
		return enableSatelliteFlag;
	}

	public void setEnableSatelliteFlag(Boolean enableSatelliteFlag) {
		this.enableSatelliteFlag = enableSatelliteFlag;
	}

	public Boolean getSavedFlag() {
		return savedFlag;
	}

	public void setSavedFlag(Boolean savedFlag) {
		this.savedFlag = savedFlag;
	}

	public Date getCheckNextSupplyDate() {
		return checkNextSupplyDate == null ? null : new Date(checkNextSupplyDate.getTime());
	}

	public void setCheckNextSupplyDate(Date checkNextSupplyDate) {
		this.checkNextSupplyDate = checkNextSupplyDate == null ? null : new Date(checkNextSupplyDate.getTime());
	}

	public MaterialRequest getPrevMaterialRequest() {
		return prevMaterialRequest;
	}

	public void setPrevMaterialRequest(MaterialRequest prevMaterialRequest) {
		this.prevMaterialRequest = prevMaterialRequest;
	}

	public Boolean getFilterFlag() {
		return filterFlag;
	}

	public void setFilterFlag(Boolean filterFlag) {
		this.filterFlag = filterFlag;
	}

	public String getInfoCode() {
		return infoCode;
	}

	public void setInfoCode(String infoCode) {
		this.infoCode = infoCode;
	}

	public List<String> getInfoParamList() {
		return infoParamList;
	}

	public void setInfoParamList(List<String> infoParamList) {
		this.infoParamList = infoParamList;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public List<String> getErrorParamList() {
		return errorParamList;
	}

	public void setErrorParamList(List<String> errorParamList) {
		this.errorParamList = errorParamList;
	}

	public String getErrorRemark() {
		return errorRemark;
	}

	public void setErrorRemark(String errorRemark) {
		this.errorRemark = errorRemark;
	}

	public String getExceedDoseLimitCode() {
		return exceedDoseLimitCode;
	}

	public void setExceedDoseLimitCode(String exceedDoseLimitCode) {
		this.exceedDoseLimitCode = exceedDoseLimitCode;
	}

	public Boolean getAllowEditFlag() {
		return allowEditFlag;
	}

	public void setAllowEditFlag(Boolean allowEditFlag) {
		this.allowEditFlag = allowEditFlag;
	}
	
	public Boolean getResumeDispInPmsIpActionFlag() {
		return resumeDispInPmsIpActionFlag;
	}

	public void setResumeDispInPmsIpActionFlag(Boolean resumeDispInPmsIpActionFlag) {
		this.resumeDispInPmsIpActionFlag = resumeDispInPmsIpActionFlag;
	}
}
