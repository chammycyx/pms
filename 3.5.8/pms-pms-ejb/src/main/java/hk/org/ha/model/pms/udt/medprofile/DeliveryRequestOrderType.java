package hk.org.ha.model.pms.udt.medprofile;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DeliveryRequestOrderType implements StringValuedEnum {
	
	None("-", "None"),
	NewOrder("N", "New"),
	RefillOrder("R", "Refill"),
	Both("B", "Both");
	
    private final String dataValue;
    private final String displayValue;
	
	private DeliveryRequestOrderType(String dataValue, String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}
	
	public static DeliveryRequestOrderType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DeliveryRequestOrderType.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<DeliveryRequestOrderType> {
		
		private static final long serialVersionUID = 1L;

		public Class<DeliveryRequestOrderType> getEnumClass() {
			return DeliveryRequestOrderType.class;
		}
	}
}
