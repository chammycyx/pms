package hk.org.ha.model.pms.udt.refill;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DocType implements StringValuedEnum {

	Standard("S", "Standard"),
	Sfi("F", "SFI");
	
    private final String dataValue;
    private final String displayValue;

    DocType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }  
    
    public static DocType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DocType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<DocType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<DocType> getEnumClass() {
    		return DocType.class;
    	}
    }
}
