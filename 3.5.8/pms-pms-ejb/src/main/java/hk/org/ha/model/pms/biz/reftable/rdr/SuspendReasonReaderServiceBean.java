package hk.org.ha.model.pms.biz.reftable.rdr;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.reftable.SuspendReasonManagerLocal;
import hk.org.ha.model.pms.persistence.reftable.SuspendReason;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("suspendReasonReaderService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SuspendReasonReaderServiceBean implements SuspendReasonReaderServiceLocal {
	
	@Out(required = false)
	private List<SuspendReason> suspendReasonRdrList;
	
	@In
	private SuspendReasonManagerLocal suspendReasonManager;
	
	public void retrieveSuspendReasonList() {
		suspendReasonRdrList = suspendReasonManager.retrieveSuspendReasonList();
	}

	@Remove
	public void destroy() {
		if (suspendReasonRdrList != null) {
			suspendReasonRdrList = null;
		}
	}
}
