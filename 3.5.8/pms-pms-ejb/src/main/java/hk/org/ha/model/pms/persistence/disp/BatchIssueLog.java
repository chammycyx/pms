package hk.org.ha.model.pms.persistence.disp;

import flexjson.JSON;
import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Customizer;
import org.eclipse.persistence.annotations.PrivateOwned;
import org.eclipse.persistence.indirection.IndirectList;

@Entity
@Table(name = "BATCH_ISSUE_LOG")
@Customizer(AuditCustomizer.class)
public class BatchIssueLog extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "batchIssueLogSeq")
	@SequenceGenerator(name = "batchIssueLogSeq", sequenceName = "SQ_BATCH_ISSUE_LOG", initialValue = 100000000)
	private Long id;

	@Column(name = "ISSUE_DATE", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date issueDate;

	@Column(name = "ISSUE_COUNT", nullable = false)
	private Integer issueCount;
	
	@Column(name = "ERROR_COUNT", nullable = false)
	private Integer errorCount;
	
	@PrivateOwned
	@OrderBy("sortSeq,id")
    @OneToMany(mappedBy="batchIssueLog", cascade=CascadeType.ALL)
    private List<BatchIssueLogDetail> batchIssueLogDetailList;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "UPDATE_WORKSTATION_ID", nullable = false)
	private Workstation updateWorkstation;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE", referencedColumnName = "HOSP_CODE", nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;
	
	public BatchIssueLog() {
		issueCount = Integer.valueOf(0);
		errorCount = Integer.valueOf(0);
	}
	
	@PrePersist
	@PreUpdate
	public void preSave() {	
		
		// only update sortSeq when batchIssueLogDetailList is not a lazy list
		if ( !(batchIssueLogDetailList instanceof IndirectList)
				|| ((IndirectList) batchIssueLogDetailList).isInstantiated()) {			
			int i = 1;
			for (BatchIssueLogDetail detail : this.getBatchIssueLogDetailList()) {
				detail.setSortSeq(Integer.valueOf(i++));
			}
		}
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getIssueDate() {
		if (issueDate == null) {
			return null;
		}
		else {
			return new Date(issueDate.getTime());
		}
	}

	public void setIssueDate(Date issueDate) {
		if (issueDate == null) {
			this.issueDate = null;
		}
		else {
			this.issueDate = new Date(issueDate.getTime());
		}
	}

	public Integer getIssueCount() {
		return issueCount;
	}

	public void setIssueCount(Integer issueCount) {
		this.issueCount = issueCount;
	}

	public Integer getErrorCount() {
		return errorCount;
	}

	public void setErrorCount(Integer errorCount) {
		this.errorCount = errorCount;
	}

	@JSON
	public List<BatchIssueLogDetail> getBatchIssueLogDetailList() {
		if (batchIssueLogDetailList == null) {
			batchIssueLogDetailList = new ArrayList<BatchIssueLogDetail>();
		}
		return batchIssueLogDetailList;
	}

	public void setBatchIssueLogDetailList(List<BatchIssueLogDetail> batchIssueLogDetailList) {
		this.batchIssueLogDetailList = batchIssueLogDetailList;
	}
	
	public BatchIssueLogDetail addBatchIssueLogDetail(BatchIssueLogDetail batchIssueLogDetail) {
		batchIssueLogDetail.setBatchIssueLog(this);
		getBatchIssueLogDetailList().add(batchIssueLogDetail);
		return batchIssueLogDetail;
	}
	
	@JSON(include=false)
	public Workstation getUpdateWorkstation() {
		return updateWorkstation;
	}

	public void setUpdateWorkstation(Workstation updateWorkstation) {
		this.updateWorkstation = updateWorkstation;
	}
	
	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}
}
