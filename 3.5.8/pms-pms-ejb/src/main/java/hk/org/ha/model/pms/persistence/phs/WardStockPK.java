package hk.org.ha.model.pms.persistence.phs;

import java.io.Serializable;

public class WardStockPK implements Serializable {

	private static final long serialVersionUID = 1L;

	private String instCode;
	private String wardCode;
	private String itemCode;
	
	public WardStockPK() {
	}

	public WardStockPK(String instCode, String wardCode, String itemCode) {
		this.instCode = instCode;
		this.wardCode = wardCode;
		this.itemCode = itemCode;
	}

	public String getInstCode() {
		return instCode;
	}

	public void setInstCode(String instCode) {
		this.instCode = instCode;
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((itemCode == null) ? 0 : itemCode.hashCode());
		result = prime * result
				+ ((wardCode == null) ? 0 : wardCode.hashCode());
		result = prime * result
				+ ((instCode == null) ? 0 : instCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WardStockPK other = (WardStockPK) obj;
		if (itemCode == null) {
			if (other.itemCode != null)
				return false;
		} else if (!itemCode.equals(other.itemCode))
			return false;
		if (wardCode == null) {
			if (other.wardCode != null)
				return false;
		} else if (!wardCode.equals(other.wardCode))
			return false;
		if (instCode == null) {
			if (other.instCode != null)
				return false;
		} else if (!instCode.equals(other.instCode))
			return false;
		return true;
	}
}
