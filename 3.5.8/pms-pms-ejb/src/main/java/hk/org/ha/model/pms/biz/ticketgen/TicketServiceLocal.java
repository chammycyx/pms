package hk.org.ha.model.pms.biz.ticketgen;

import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
import hk.org.ha.model.pms.udt.ticketgen.TicketType;

import java.util.Date;

import javax.ejb.Local;

@Local
public interface TicketServiceLocal {
		
	void retrieveTicketByTicketNum(Date ticketDate, String ticketNum, Long medOrderId, OneStopOrderType orderType);
	
	void retrieveTicketByTicketType(TicketType ticketType);
	
	void retrieveBatchTicket();
	
	void retrieveTicketByOrderNumCaseNum(String tickSearchCriteria, boolean enableDrugCharge, boolean checkOpas, boolean checkFcs, boolean forcePrintTicket);
	
	void retrieveTicketByReprintTicketNum(String reprintTicketNum);
	
	void destroy();

}
