package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmFormDosageUnitMappingCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmFormDosageUnitMapping;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("dmFormDosageUnitMappingListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DmFormDosageUnitMappingListServiceBean implements DmFormDosageUnitMappingListServiceLocal {

	@In
	private DmFormDosageUnitMappingCacherInf dmFormDosageUnitMappingCacher;	
	
	public List<String> retrieveDosageUnitListByFormCode( String formCode ) {
		List<String> dosageUnitList = new ArrayList<String>();
		List<DmFormDosageUnitMapping> dmFormDosageUnitMappingList = dmFormDosageUnitMappingCacher.getDmFormDosageUnitMappingListByFormCode(formCode);
		
		if ( dmFormDosageUnitMappingList != null ) {
			for ( DmFormDosageUnitMapping dmFormDosageUnitMapping : dmFormDosageUnitMappingList ) {
				dosageUnitList.add(dmFormDosageUnitMapping.getCompId().getDosageUnit());
			}
		}
		return dosageUnitList;
	}

	@Remove
	public void destroy() {		
	}
}
