package hk.org.ha.model.pms.vo.label;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "PivasMergeFormulaWorksheetRawMaterialDtl")
@ExternalizedBean(type=DefaultExternalizer.class)
public class PivasMergeFormulaWorksheetRawMaterialDtl implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@XmlElement(required = true)
	private Integer sortSeq;
	
	@XmlElement(required = true)
	private Integer drugKey;
	
	@XmlElement(required = false)
	private String solventDesc;
	
	@XmlElement(required = false)
	private String diluentDesc;
	
	@XmlElement(required = true)
	private Boolean vendSupplySolvFlag;
	
	@XmlElement(required = false)
	private String itemCode;
	
	@XmlElement(required = false)
	private String strength;
	
	@XmlElement(required = false)
	private String volumeValue;
	
	@XmlElement(required = false)
	private String volumeUnit;
	
	@XmlElement(required = true)
	private String manufCode;
	
	@XmlElement(required = false)
	private String batchNum;
	
	@XmlElement(required = true)
	private Date expiryDate;
	
	@XmlElement(required = true)
	private String displayName;
	
	@XmlElement(required = false)
	private String saltProperty;
	
	@XmlElement(required = true)
	private String formCode;
	
	// save for PIVAS report
	@XmlElement(required = false)
	private String itemDesc;

	public PivasMergeFormulaWorksheetRawMaterialDtl() {
		vendSupplySolvFlag = Boolean.FALSE;
	}

	public Integer getSortSeq() {
		return sortSeq;
	}

	public void setSortSeq(Integer sortSeq) {
		this.sortSeq = sortSeq;
	}

	public Integer getDrugKey() {
		return drugKey;
	}

	public void setDrugKey(Integer drugKey) {
		this.drugKey = drugKey;
	}

	public String getSolventDesc() {
		return solventDesc;
	}

	public void setSolventDesc(String solventDesc) {
		this.solventDesc = solventDesc;
	}

	public String getDiluentDesc() {
		return diluentDesc;
	}

	public void setDiluentDesc(String diluentDesc) {
		this.diluentDesc = diluentDesc;
	}

	public Boolean getVendSupplySolvFlag() {
		return vendSupplySolvFlag;
	}

	public void setVendSupplySolvFlag(Boolean vendSupplySolvFlag) {
		this.vendSupplySolvFlag = vendSupplySolvFlag;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getStrength() {
		return strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public String getVolumeValue() {
		return volumeValue;
	}

	public void setVolumeValue(String volumeValue) {
		this.volumeValue = volumeValue;
	}

	public String getVolumeUnit() {
		return volumeUnit;
	}

	public void setVolumeUnit(String volumeUnit) {
		this.volumeUnit = volumeUnit;
	}

	public String getManufCode() {
		return manufCode;
	}

	public void setManufCode(String manufCode) {
		this.manufCode = manufCode;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getSaltProperty() {
		return saltProperty;
	}

	public void setSaltProperty(String saltProperty) {
		this.saltProperty = saltProperty;
	}
	public String getFormCode() {
		return formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	public String getItemDesc() {
		return itemDesc;
	}

	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}
}
