package hk.org.ha.model.pms.vo.rx;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class IvFluid extends Fluid implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@XmlElement
	private Integer fluidNum;

	public Integer getFluidNum() {
		return fluidNum;
	}

	public void setFluidNum(Integer fluidNum) {
		this.fluidNum = fluidNum;
	}
}
