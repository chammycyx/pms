package hk.org.ha.model.pms.udt.medprofile;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum MedProfileStatChargeType implements StringValuedEnum {
	
	None("-", "None"),
	Private("P", "Private"),
	Sfi("S", "SFI");
	
    private final String dataValue;
    private final String displayValue;
    
	private MedProfileStatChargeType(String dataValue, String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}

	@Override
	public String getDataValue() {
		return dataValue;
	}

	@Override
	public String getDisplayValue() {
		return displayValue;
	}
	
    public static MedProfileStatChargeType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MedProfileStatChargeType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<MedProfileStatChargeType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<MedProfileStatChargeType> getEnumClass() {
    		return MedProfileStatChargeType.class;
    	}
    }
	
}
