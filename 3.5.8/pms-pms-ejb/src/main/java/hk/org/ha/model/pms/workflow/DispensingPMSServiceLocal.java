package hk.org.ha.model.pms.workflow;

import javax.ejb.Local;

@Local
public interface DispensingPMSServiceLocal {

	void checkClearance(String orderNo);
	
	void mdsOverrideReason();
	
	void createRefillSchedule();
		
	void printSFIInvoice();
	
	void suspendDispensingOrder();
	
	void checkSFIClearance();
	
	void resumeSuspendDispensingOrder();
}
