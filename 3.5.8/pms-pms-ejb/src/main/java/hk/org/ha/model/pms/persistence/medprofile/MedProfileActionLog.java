package hk.org.ha.model.pms.persistence.medprofile;

import hk.org.ha.fmk.pms.entity.CreateDate;
import hk.org.ha.model.pms.udt.medprofile.ActionLogType;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "MED_PROFILE_ACTION_LOG")
public class MedProfileActionLog {
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MedProfileActionLogSeq")
	@SequenceGenerator(name = "MedProfileActionLogSeq", sequenceName = "SQ_MED_PROFILE_ACTION_LOG", initialValue = 100000000)
	private Long id;
	
	@CreateDate
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_DATE", nullable = false)
	// change to public to avoid exception from Reflections.setAndWrap(Reflections.java:131)
	public Date createDate;
	
    @Converter(name = "MedProfileActionLog.type", converterClass = ActionLogType.Converter.class)
    @Convert("MedProfileActionLog.type")
	@Column(name = "TYPE", nullable = false, length = 2)
	private ActionLogType type;
	
    @Column(name = "ACTION_CODE", length = 2)
    private String actionCode;
    
	@Column(name = "MESSAGE", length = 255)
	private String message;
		
	@Column(name = "SUPPL_MESSAGE", length = 255)
	private String supplMessage;

	@Column(name = "ACTION_USER", nullable = false, length = 12)
	private String actionUser;

	@Column(name = "ACTION_USER_NAME", length = 150)
	private String actionUserName;
	
	@Column(name = "WARD_CODE", length = 4)
	private String wardCode;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MED_PROFILE_MO_ITEM_ID", nullable = false)
	private MedProfileMoItem medProfileMoItem;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate == null ? null : new Date(createDate.getTime());
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate == null ? null : new Date(createDate.getTime());
	}

	public ActionLogType getType() {
		return type;
	}

	public void setType(ActionLogType type) {
		this.type = type;
	}
	
	public String getActionCode() {
		return actionCode;
	}

	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSupplMessage() {
		return supplMessage;
	}

	public void setSupplMessage(String supplMessage) {
		this.supplMessage = supplMessage;
	}
	
	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public String getActionUser() {
		return actionUser;
	}

	public void setActionUser(String actionUser) {
		this.actionUser = actionUser;
	}

	public String getActionUserName() {
		return actionUserName;
	}

	public void setActionUserName(String actionUserName) {
		this.actionUserName = actionUserName;
	}

	public MedProfileMoItem getMedProfileMoItem() {
		return medProfileMoItem;
	}

	public void setMedProfileMoItem(MedProfileMoItem medProfileMoItem) {
		this.medProfileMoItem = medProfileMoItem;
	}
}
