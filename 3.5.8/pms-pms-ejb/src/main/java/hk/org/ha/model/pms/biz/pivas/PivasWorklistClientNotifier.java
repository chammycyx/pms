package hk.org.ha.model.pms.biz.pivas;

import hk.org.ha.fmk.pms.jms.MessageCreator;
import hk.org.ha.fmk.pms.jms.MessageProducerInf;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.naming.NamingException;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;


@AutoCreate
@Name("pivasWorklistClientNotifier")
@Scope(ScopeType.APPLICATION)
public class PivasWorklistClientNotifier implements PivasWorklistClientNotifierInf {

	@Logger
	private Log logger;

	@In
    private MessageProducerInf pivasWorklistGravityMessageProducer;   
    
    public void notifyClient(Serializable object, Map<String, Object> headerParams) {
    	String errorString = "Could not publish notification";
        try {
        	pivasWorklistGravityMessageProducer.send(new InnerMessageCreator(object, headerParams));
        } catch (JMSException e) {
        	logger.error(errorString, e);
        } catch (NamingException e) {
        	logger.error(errorString, e);
        }
    }
    
    public void dispatchUpdateEvent(String workstoreGroupCode) {
		Map<String, Object> headerParams = new HashMap<String, Object>();
		headerParams.put("WORKSTORE_GROUP_CODE", workstoreGroupCode);
		
		notifyClient("NOTIFICATION FROM PivasWorklistClientNotifier",
				headerParams);
    }

    public static class InnerMessageCreator implements MessageCreator {

    	private Serializable object;
    	private Map<String, Object> headerParams;
    	
    	public InnerMessageCreator(Serializable object, Map<String, Object> headerParams) {
    		this.object = object;
    		this.headerParams = headerParams;
    	}
    	
		public Message createMessage(Session session) throws JMSException {
			
			Message message = session.createObjectMessage(object);
			
			for (Entry<String, Object> entry: headerParams.entrySet()) {
				message.setObjectProperty(entry.getKey(), entry.getValue());
			}
			return message;
		}
    }
}
