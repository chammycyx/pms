package hk.org.ha.model.pms.persistence.corp;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "CDDH_WORKSTORE_GROUP")
public class CddhWorkstoreGroup extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "CDDH_WORKSTORE_GROUP_CODE", length = 3)
	private String cddhWorkstoreGroupCode;
	
    @Converter(name = "CddhWorkstoreGroup.status", converterClass = RecordStatus.Converter.class)
    @Convert("CddhWorkstoreGroup.status")
	@Column(name = "STATUS", nullable = false, length = 1)
	private RecordStatus status;
    
    @ManyToMany(targetEntity = Hospital.class)
    @JoinTable (
    	name = "CDDH_WORKSTORE_GROUP_HOS",
    	joinColumns = {@JoinColumn(name = "CDDH_WORKSTORE_GROUP_CODE")},
    	inverseJoinColumns = {@JoinColumn(name = "HOSP_CODE")}
    )
    List<Hospital> hospitalList;
    
	@OneToMany(mappedBy = "cddhWorkstoreGroup")
	private List<Workstore> workstoreList;

	public CddhWorkstoreGroup() {
		status = RecordStatus.Active;
	}

	public String getCddhWorkstoreGroupCode() {
		return cddhWorkstoreGroupCode;
	}

	public void setCddhWorkstoreGroupCode(String cddhWorkstoreGroupCode) {
		this.cddhWorkstoreGroupCode = cddhWorkstoreGroupCode;
	}
	
	public RecordStatus getStatus() {
		return status;
	}

	public void setStatus(RecordStatus status) {
		this.status = status;
	}

	public List<Hospital> getHospitalList() {
		return hospitalList;
	}

	public void setHospitalList(List<Hospital> hospitalList) {
		this.hospitalList = hospitalList;
	}

	public List<Workstore> getWorkstoreList() {
		return workstoreList;
	}

	public void setWorkstoreList(List<Workstore> workstoreList) {
		this.workstoreList = workstoreList;
	} 
	

}
