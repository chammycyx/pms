package hk.org.ha.model.pms.biz.sys;

import hk.org.ha.fmk.pms.sys.entity.SystemMessage;
import hk.org.ha.fmk.pms.util.Interpolator;
import hk.org.ha.fmk.pms.web.MeasureCalls;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;

/**
 * Session Bean implementation class SystemMessageManager
 */
@AutoCreate
@Stateless
@Name("systemMessageManager")
@MeasureCalls
public class SystemMessageManagerBean implements SystemMessageManagerLocal {

	@PersistenceContext
	private EntityManager em;

	public String resolve(String messageCode, Object... params) {
		SystemMessage tempSystemMessage = em.find(SystemMessage.class, messageCode);
		return Interpolator.instance().interpolate(tempSystemMessage.getMainMsg(),params);
	}

	public String retrieveMessageDesc(String messageCode) {
		SystemMessage tempSystemMessage = em.find(SystemMessage.class, messageCode);
		return tempSystemMessage.getMainMsg();
	}
}