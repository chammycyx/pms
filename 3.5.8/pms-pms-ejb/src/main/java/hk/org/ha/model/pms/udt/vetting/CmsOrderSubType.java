package hk.org.ha.model.pms.udt.vetting;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum CmsOrderSubType implements StringValuedEnum {

	None("-", "None"),
	AnE("A", "A&E"),
	InPatient("I", "In Patient"),
	OutPatient("O", "Out Patient");
	
	private final String dataValue;
	private final String displayValue;
	
	CmsOrderSubType (final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}
	
	public static CmsOrderSubType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(CmsOrderSubType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<CmsOrderSubType> {

		private static final long serialVersionUID = 1L;

		public Class<CmsOrderSubType> getEnumClass() {
    		return CmsOrderSubType.class;
    	}
    }
	
}
