package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

public class MdsExceptionRptHdr implements Serializable {

	private static final long serialVersionUID = 1L;

	private Boolean refillFlag;
	
	private String patientName;
	
	private String patientNameChi;
	
	private String sex;

	private String caseNum;

	private String ward;
	
	private String patAllergy;
	
	private String patAdr;
	
	private Boolean g6pdFlag;
	
	private Boolean pregnancyCheckFlag;

	private Boolean allergyServerAvailableFlag;
	
	private Boolean ehrAllergyServerAvailableFlag;
	
	private String ehrAllergy;
	
	private String ehrAdr;

	public Boolean getRefillFlag() {
		return refillFlag;
	}

	public void setRefillFlag(Boolean refillFlag) {
		this.refillFlag = refillFlag;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getPatientNameChi() {
		return patientNameChi;
	}

	public void setPatientNameChi(String patientNameChi) {
		this.patientNameChi = patientNameChi;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getWard() {
		return ward;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}

	public String getPatAllergy() {
		return patAllergy;
	}

	public void setPatAllergy(String patAllergy) {
		this.patAllergy = patAllergy;
	}
	
	public String getPatAdr() {
		return patAdr;
	}

	public void setPatAdr(String patAdr) {
		this.patAdr = patAdr;
	}

	public Boolean getG6pdFlag() {
		return g6pdFlag;
	}

	public void setG6pdFlag(Boolean g6pdFlag) {
		this.g6pdFlag = g6pdFlag;
	}

	public Boolean getPregnancyCheckFlag() {
		return pregnancyCheckFlag;
	}

	public void setPregnancyCheckFlag(Boolean pregnancyCheckFlag) {
		this.pregnancyCheckFlag = pregnancyCheckFlag;
	}

	public Boolean getAllergyServerAvailableFlag() {
		return allergyServerAvailableFlag;
	}

	public void setAllergyServerAvailableFlag(Boolean allergyServerAvailableFlag) {
		this.allergyServerAvailableFlag = allergyServerAvailableFlag;
	}

	public Boolean getEhrAllergyServerAvailableFlag() {
		return ehrAllergyServerAvailableFlag;
	}

	public void setEhrAllergyServerAvailableFlag(Boolean ehrAllergyServerAvailableFlag) {
		this.ehrAllergyServerAvailableFlag = ehrAllergyServerAvailableFlag;
	}

	public String getEhrAllergy() {
		return ehrAllergy;
	}

	public void setEhrAllergy(String ehrAllergy) {
		this.ehrAllergy = ehrAllergy;
	}

	public String getEhrAdr() {
		return ehrAdr;
	}

	public void setEhrAdr(String ehrAdr) {
		this.ehrAdr = ehrAdr;
	}
}
