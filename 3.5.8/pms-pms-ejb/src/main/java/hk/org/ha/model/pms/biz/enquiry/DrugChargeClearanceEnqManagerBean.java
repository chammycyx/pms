package hk.org.ha.model.pms.biz.enquiry;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.charging.DrugChargeClearanceManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.udt.charging.ClearanceStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.InvoiceDocType;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderPatType;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.charging.DrugCheckClearanceResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("drugChargeClearanceEnqManager")
@MeasureCalls
public class DrugChargeClearanceEnqManagerBean implements DrugChargeClearanceEnqManagerLocal {

	public static final String INVOICE_IS_VOID = "0296";
	public static final String FCS_INV_NOT_FOUND_MSG_CODE = "0070";
	public static final String FCS_ERROR_MSG_CODE = "0014";
	public static final String STANDARD_ITEM_NOT_FOUND = "0401";
	public static final String SFI_ITEM_NOT_FOUND = "0402";
	
	@PersistenceContext
	private EntityManager em;
		
	@In
	private Workstore workstore;
		
	@In
	private DrugChargeClearanceManagerLocal drugChargeClearanceManager;
		
	@SuppressWarnings({ "unchecked" })
	public DrugCheckClearanceResult checkClearanceByMoeOrderNo(String medOrderNum){
		List<DispOrder> dispOrderList = (List<DispOrder>) em.createQuery(
				"select o from DispOrder o " + // 20120215 index check : MedOrder.orderNum : I_MED_ORDER_01
				"where o.pharmOrder.medOrder.status <> :medOrderStatus " + 
				"and o.pharmOrder.medOrder.orderType = :medOrderType " +
				"and o.pharmOrder.medOrder.docType = :medOrderDocType " +
				"and o.pharmOrder.medOrder.orderNum = :orderNum " +
				"and o.status <> :dispOrderStatus " +
				"order by o.createDate desc, o.id desc")		
				.setParameter("medOrderStatus", MedOrderStatus.SysDeleted)
				.setParameter("medOrderType", OrderType.OutPatient)
				.setParameter("medOrderDocType", MedOrderDocType.Normal)
				.setParameter("orderNum", medOrderNum)
				.setParameter("dispOrderStatus", DispOrderStatus.SysDeleted)
				.getResultList();
		
		if( dispOrderList.size() > 0 ){
			DispOrder dispOrder = dispOrderList.get(0);
			return checkClearanceByDispOrder(dispOrder, false);
		}
		
		List<MedOrder> medOrderList = (List<MedOrder>) em.createQuery(
				"select o from MedOrder o" + // 20120215 index check : MedOrder.orderNum : I_MED_ORDER_01
				" where o.orderNum = :orderNum" +
				" and o.orderType = :medOrderType" +
				" and o.status <> :medOrderStatus" +
				" and o.docType = :docType" +
				" order by o.orderDate desc")
				.setParameter("orderNum", medOrderNum)
				.setParameter("medOrderType", OrderType.OutPatient)
				.setParameter("docType", MedOrderDocType.Normal)
				.setParameter("medOrderStatus", MedOrderStatus.SysDeleted)
				.getResultList();
			
		if( medOrderList.size() > 0 ){
			MedOrder medOrder = medOrderList.get(0);
			medOrder.getMedCase();
			return checkClearanceByMedOrder(medOrder);
		}
		
		DrugCheckClearanceResult drugClearCheckResult = new DrugCheckClearanceResult();		
		List<String> sysMsgCodeList = new ArrayList<String>();
		sysMsgCodeList.add(STANDARD_ITEM_NOT_FOUND);
		sysMsgCodeList.add(SFI_ITEM_NOT_FOUND);
		drugClearCheckResult.setSysMsgCodeList(sysMsgCodeList);
		return drugClearCheckResult;
	}
	
	@SuppressWarnings("unchecked")
	public DrugCheckClearanceResult checkClearanceByTicketNum(String ticketNum, Date ticketDate){
				
		List<DispOrder> dispOrderList = (List<DispOrder>) em.createQuery(
				"select o from DispOrder o " + // 20120215 index check : Ticket.workstore,orderType,ticketDate,ticketNum : UI_TICKET_01
				"where o.ticket.ticketNum = :ticketNum " +
				"and o.ticket.ticketDate = :ticketDate " +
				"and o.ticket.workstore = :workstore " +
				"and o.ticket.orderType = :ticketOrderType " +
				"and o.pharmOrder.medOrder.orderType = :medOrderType " +
				"and o.status <> :dispOrderStatus " +
				"and o.pharmOrder.medOrder.status <> :medOrderStatus " +
				"order by o.createDate desc, o.id desc")							
				.setParameter("ticketNum", ticketNum)
				.setParameter("ticketDate", ticketDate,TemporalType.DATE)												
				.setParameter("workstore", workstore)
				.setParameter("medOrderType", OrderType.OutPatient)
				.setParameter("ticketOrderType", OrderType.OutPatient)
				.setParameter("dispOrderStatus", DispOrderStatus.SysDeleted)
				.setParameter("medOrderStatus", MedOrderStatus.SysDeleted)
				.getResultList();
		
		if( dispOrderList.size() > 0 ){
			DispOrder dispOrder = dispOrderList.get(0);
			return checkClearanceByDispOrder(dispOrder, false);
		}else{
			DrugCheckClearanceResult drugClearCheckResult = new DrugCheckClearanceResult();		
			List<String> sysMsgCodeList = new ArrayList<String>();
			sysMsgCodeList.add(STANDARD_ITEM_NOT_FOUND);
			sysMsgCodeList.add(SFI_ITEM_NOT_FOUND);
			drugClearCheckResult.setSysMsgCodeList(sysMsgCodeList);
			return drugClearCheckResult;
		}		
	}
		
	@SuppressWarnings("unchecked")
	public DrugCheckClearanceResult checkClearanceByInvoiceNumber(String invoiceNum){
		Invoice sfiInvoice = null;
		
		List<Invoice> invoiceList = (List<Invoice>) em.createQuery(
				"select o from Invoice o" + // 20120215 index check : Invoice.invoiceNum : I_INVOICE_01
				" where o.invoiceNum = :invoiceNum" + 
				" and o.docType = :invoiceDocType" +
				" and o.status <> :invoiceStatus" +
				" and o.dispOrder.pharmOrder.medOrder.orderType = :medOrderType" +
				" order by o.createDate desc, o.id desc")						
				.setParameter("invoiceNum", invoiceNum)
				.setParameter("invoiceDocType", InvoiceDocType.Sfi)
				.setParameter("invoiceStatus", InvoiceStatus.SysDeleted)
				.setParameter("medOrderType", OrderType.OutPatient)
				.getResultList();
		
		if( invoiceList.size() == 0 ){
			DrugCheckClearanceResult drugClearCheckResult = new DrugCheckClearanceResult();		
			drugClearCheckResult.setSysMsgCodeList(Arrays.asList(SFI_ITEM_NOT_FOUND));
			return drugClearCheckResult;
		}else{
			sfiInvoice = invoiceList.get(0);
		}
		
		DrugCheckClearanceResult drugClearCheckResult = new DrugCheckClearanceResult();		
		drugClearCheckResult.setSfiInvNum(getDisplayInvoiceNum(sfiInvoice.getInvoiceNum()));
		
		if( InvoiceStatus.Void == sfiInvoice.getStatus() ){
			drugClearCheckResult.setSysMsgCodeList(Arrays.asList(INVOICE_IS_VOID));
			return drugClearCheckResult;
		}
		ClearanceStatus sfiDrugClearance = drugChargeClearanceManager.checkSfiDrugClearanceByInvoice(sfiInvoice).getClearanceStatus();						
		drugClearCheckResult.setSfiDrugClearance(sfiDrugClearance);		
		if ( ClearanceStatus.Error.equals(sfiDrugClearance)){
			drugClearCheckResult.setSysMsgCodeList(Arrays.asList(FCS_ERROR_MSG_CODE));
		}else if( ClearanceStatus.RecordNotFound.equals(sfiDrugClearance) ){
			drugClearCheckResult.setSysMsgCodeList(Arrays.asList(FCS_INV_NOT_FOUND_MSG_CODE));
		}
		return drugClearCheckResult;
	}
		
	private DrugCheckClearanceResult checkClearanceByMedOrder(MedOrder medOrder){
		boolean stdItemExist = false;
		// For non-vetted Order, check stdDrugCharge Only
		for(MedOrderItem medOrderItem : medOrder.getMedOrderItemList()){
			if( ActionStatus.DispByPharm == medOrderItem.getActionStatus() ||
				ActionStatus.DispInClinic == medOrderItem.getActionStatus() ){
				stdItemExist = true;
				break;
			}
		}

		DrugCheckClearanceResult drugClearCheckResult = new DrugCheckClearanceResult();
		List<String> sysMsgCodeList = new ArrayList<String>();

		if( stdItemExist ){
			drugClearCheckResult.setStdDrugClearance(drugChargeClearanceManager.checkStdDrugClearanceByMedOrder(medOrder));
			if( ClearanceStatus.Error.equals(drugClearCheckResult.getStdDrugClearance()) ){
				sysMsgCodeList.add(FCS_ERROR_MSG_CODE);
			}
		}else{
			sysMsgCodeList.add(STANDARD_ITEM_NOT_FOUND);
		}
		drugClearCheckResult.setSysMsgCodeList(sysMsgCodeList);
		return drugClearCheckResult;
	}
	
	private DrugCheckClearanceResult checkClearanceByDispOrder(DispOrder dispOrder, boolean fromCheckIssue){

		DrugCheckClearanceResult drugClearCheckResult = new DrugCheckClearanceResult();		

		boolean stdItemExist = false;
		boolean forceProceedFlag = true;
		String stdSysMsgCode = StringUtils.EMPTY;
		String sfiSysMsgCode = StringUtils.EMPTY;
		
		for ( Invoice invoice: dispOrder.getInvoiceList() ){
			if( InvoiceDocType.Standard == invoice.getDocType() ){
				drugClearCheckResult.setStdDrugClearance(drugChargeClearanceManager.checkStdDrugClearanceByDispOrder(dispOrder));

				if( ClearanceStatus.Error == drugClearCheckResult.getStdDrugClearance() ){
					stdSysMsgCode = FCS_ERROR_MSG_CODE;
				}

			}else if ( InvoiceDocType.Sfi == invoice.getDocType() ){
				drugClearCheckResult.setSfiInvNum(getDisplayInvoiceNum(invoice.getInvoiceNum()));
				
				if( InvoiceStatus.Void == invoice.getStatus() ){
					sfiSysMsgCode = INVOICE_IS_VOID;
				}else{
					ClearanceStatus sfiDrugClearance = drugChargeClearanceManager.checkSfiDrugClearanceByInvoice(invoice).getClearanceStatus();
					drugClearCheckResult.setSfiDrugClearance(sfiDrugClearance);
				}
				
				if( ClearanceStatus.Error == drugClearCheckResult.getSfiDrugClearance() ){
					sfiSysMsgCode = FCS_ERROR_MSG_CODE;
				}else if ( ClearanceStatus.RecordNotFound == drugClearCheckResult.getSfiDrugClearance() ){
					sfiSysMsgCode = FCS_INV_NOT_FOUND_MSG_CODE;
				}

				forceProceedFlag = false;
				if( PharmOrderPatType.Doh == dispOrder.getPharmOrder().getPatType() || invoice.getForceProceedFlag() ||
					ClearanceStatus.PaymentClear == drugClearCheckResult.getSfiDrugClearance() ){
					forceProceedFlag = true;
				}
			}
		}
		
		if( drugClearCheckResult.getStdDrugClearance() == null){
						
			for( DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList() ){
				if( ActionStatus.DispByPharm == dispOrderItem.getPharmOrderItem().getActionStatus() ||
					ActionStatus.DispInClinic == dispOrderItem.getPharmOrderItem().getActionStatus() ){ 
					stdItemExist = true;
					break;
				}
			}
			
			if( stdItemExist ){
				drugClearCheckResult.setStdDrugClearance(drugChargeClearanceManager.checkStdDrugClearanceByDispOrder(dispOrder));

				if( ClearanceStatus.Error == drugClearCheckResult.getStdDrugClearance() ){
					stdSysMsgCode = FCS_ERROR_MSG_CODE;
				}
			}else{
				stdSysMsgCode = STANDARD_ITEM_NOT_FOUND;
			}
		}
		
		if( drugClearCheckResult.getSfiDrugClearance() == null && drugClearCheckResult.getSfiInvNum() == null ){
			sfiSysMsgCode = SFI_ITEM_NOT_FOUND;
		}
		
		if(StringUtils.equals( stdSysMsgCode, sfiSysMsgCode)){
			sfiSysMsgCode = StringUtils.EMPTY;
		}
		
		if( fromCheckIssue ){
			drugClearCheckResult.setForceProceedFlag(forceProceedFlag);
			
			if( StringUtils.isNotBlank(stdSysMsgCode) && !StringUtils.equals( stdSysMsgCode, STANDARD_ITEM_NOT_FOUND ) ){
				drugClearCheckResult.setSystemMessageCode(stdSysMsgCode);
			}
			
			if( StringUtils.isNotBlank(sfiSysMsgCode) && !StringUtils.equals( sfiSysMsgCode, SFI_ITEM_NOT_FOUND ) ){
				drugClearCheckResult.setSystemMessageCode(sfiSysMsgCode);
			}
		}else{
			List<String> sysMsgCodeList = new ArrayList<String>();
			
			if( StringUtils.isNotBlank(stdSysMsgCode) ){
				if( StringUtils.isNotBlank(sfiSysMsgCode) && StringUtils.equals( sfiSysMsgCode, FCS_ERROR_MSG_CODE ) ){
					sysMsgCodeList.add(sfiSysMsgCode);
					sysMsgCodeList.add(stdSysMsgCode);
				}else if( StringUtils.isNotBlank(sfiSysMsgCode)){
					sysMsgCodeList.add(stdSysMsgCode);
					sysMsgCodeList.add(sfiSysMsgCode);
				}else{
					sysMsgCodeList.add(stdSysMsgCode);
				}
			}else if( StringUtils.isNotBlank(sfiSysMsgCode) ){
				sysMsgCodeList.add(sfiSysMsgCode);
			}
			drugClearCheckResult.setSysMsgCodeList(sysMsgCodeList);
		}
		
		return drugClearCheckResult;
	}
	
	public DrugCheckClearanceResult checkClearanceForCheckIssue(DispOrder dispOrder){
		return checkClearanceByDispOrder(dispOrder,true);
	}
	
	public DrugCheckClearanceResult checkClearanceForOnestop(Long medOrderId){
		MedOrder medOrder = em.find(MedOrder.class, medOrderId);
		
		ClearanceStatus clearanceStatus = drugChargeClearanceManager.checkStdDrugClearanceByMedOrder(medOrder); 

		DrugCheckClearanceResult drugClearCheckResult = new DrugCheckClearanceResult();		
		if( ClearanceStatus.PaymentOutstanding.equals(clearanceStatus)){
			drugClearCheckResult.setSystemMessageCode("0062");
		}else if( ClearanceStatus.Error.equals(clearanceStatus) ){
			drugClearCheckResult.setSystemMessageCode("0063");
		}else if( ClearanceStatus.RecordNotFound.equals(clearanceStatus) ) {
			drugClearCheckResult.setSystemMessageCode("0064");
		}else if( ClearanceStatus.PaymentClear.equals(clearanceStatus) ) {
			drugClearCheckResult.setSystemMessageCode("");
		}
		drugClearCheckResult.setStdDrugClearance(clearanceStatus);
		return drugClearCheckResult;
	}
	
	public DrugCheckClearanceResult checkClearanceForOnestopManual(String patHospCode, Long orderNum, Long pasApptSeq){
		ClearanceStatus clearanceStatus = drugChargeClearanceManager.checkStdDrugClearance(patHospCode, orderNum, pasApptSeq);

		DrugCheckClearanceResult drugClearCheckResult = new DrugCheckClearanceResult();		
		if( ClearanceStatus.PaymentOutstanding.equals(clearanceStatus)){
			drugClearCheckResult.setSystemMessageCode("0062");
		}else if( ClearanceStatus.Error.equals(clearanceStatus) ){
			drugClearCheckResult.setSystemMessageCode("0063");
		}else if( ClearanceStatus.RecordNotFound.equals(clearanceStatus) ) {
			drugClearCheckResult.setSystemMessageCode("0064");
		}else if( ClearanceStatus.PaymentClear.equals(clearanceStatus) ) {
			drugClearCheckResult.setSystemMessageCode("");
		}
		drugClearCheckResult.setStdDrugClearance(clearanceStatus);
		return drugClearCheckResult;
	}
	
	private String getDisplayInvoiceNum(String invNum){
		StringBuilder displayInvoiceNum = new StringBuilder();
		displayInvoiceNum.append("I");
		displayInvoiceNum.append("-");
		displayInvoiceNum.append(invNum.substring(1, 4));
		displayInvoiceNum.append("-");
		displayInvoiceNum.append(invNum.substring(4));		
		
		return displayInvoiceNum.toString();
	}
}
