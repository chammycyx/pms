package hk.org.ha.model.pms.biz.drug;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("solventListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SolventListServiceBean implements SolventListServiceLocal {

	@Logger
	private Log logger;

	@In
	private Workstore workstore;

	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;

	private WorkstoreDrugComparator workstoreDrugComparator = new WorkstoreDrugComparator();
	
	@Out(required = false)
	private List<WorkstoreDrug> solventList;
	
	private List<WorkstoreDrug> getWorkstoreDrugListWithExtraInfo(List<WorkstoreDrug> drugList) {
		List<WorkstoreDrug> resultList = new ArrayList<WorkstoreDrug>();
		for ( WorkstoreDrug drug : drugList ) {
			drug.getDmDrug().getFullDrugDesc();
			drug.getDmDrug().getVolumeText();
			if ( drug.getMsWorkstoreDrug() != null && Integer.valueOf(100).compareTo(drug.getMsWorkstoreDrug().getDrugScope()) <= 0 ) {
				resultList.add(drug);
			}
		}		
		Collections.sort(resultList,workstoreDrugComparator);
		return resultList;
	}
	
	public void retrieveSolventList() {
		logger.debug("retrieveSolventList");
		solventList = workstoreDrugCacher.getWorkstoreDrugListBySolvent(workstore);
		
		if ( solventList == null ) {		
			solventList = new ArrayList<WorkstoreDrug>();
		} else {
			solventList = getWorkstoreDrugListWithExtraInfo(solventList);
		}
		
		logger.debug("solventList size #0", solventList.size());
	}
	
	@Remove
	public void destroy() {
		if (solventList != null) {
			solventList = null;
		}
	}
}
