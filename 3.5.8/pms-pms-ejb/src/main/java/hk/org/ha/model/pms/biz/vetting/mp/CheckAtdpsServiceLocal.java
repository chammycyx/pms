package hk.org.ha.model.pms.biz.vetting.mp;
import java.util.List;

import javax.ejb.Local;

@Local
public interface CheckAtdpsServiceLocal {

	void checkAtdpsItemLocationByItemCode(String itemCode, List<Integer> pickStationNumList);
	void destroy();
}
