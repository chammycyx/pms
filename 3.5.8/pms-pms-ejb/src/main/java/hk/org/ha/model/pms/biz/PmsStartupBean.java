package hk.org.ha.model.pms.biz;

import hk.org.ha.model.pms.biz.medprofile.MedProfileOrderProcessorLocal;
import hk.org.ha.model.pms.biz.reftable.ItemBinUploaderLocal;
import hk.org.ha.model.pms.prop.Prop;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;


@AutoCreate
@Stateless
@Name("pmsStartup")
public class PmsStartupBean implements PmsStartupLocal {

	@Logger
	private Log logger;	
		
	@In
	private MedProfileOrderProcessorLocal medProfileOrderProcessor;
	
	@In
	private ItemBinUploaderLocal itemBinUploader;
	
	@Override
	public void startup(Long delayDuration) {		
		
		startupMedProfileTimer1();
		startupMedProfileTimer2();
		startupUpdateItemBin();
		
	}
	
	private void startupMedProfileTimer1() {
		int interval = Prop.MEDPROFILE_TIMER1_INTERVAL.get(5);
		if (interval > 0) {
			Long intervalInMilli = Long.valueOf(interval * 60000L);
			medProfileOrderProcessor.triggerMedProfileTimer1(5 * 60000L, intervalInMilli);
		} else {
			logger.warn("MedProfileTimer1 timer disabled.");
		}
	}
	
	private void startupMedProfileTimer2() {
		int interval = Prop.MEDPROFILE_TIMER2_INTERVAL.get(15);
		if (interval > 0) {
			Long intervalInMilli = Long.valueOf(interval * 60000L);
			medProfileOrderProcessor.triggerMedProfileTimer2(10 * 60000L, intervalInMilli);
		} else {
			logger.warn("MedProfileTimer2 timer disabled.");
		}
	}
	
	private void startupUpdateItemBin(){
		int interval = Prop.REFTABLE_ITEMLOCATION_SYNC_INTERVAL.get(1);
		if (interval > 0) {
			Long intervalInMilli = Long.valueOf(interval * 60 * 60000L);
			itemBinUploader.uploadItemBinByTimer(10 * 60000L, intervalInMilli);
		} else {
			logger.warn("uploadItemBin timer disabled.");
		}
	}
}
