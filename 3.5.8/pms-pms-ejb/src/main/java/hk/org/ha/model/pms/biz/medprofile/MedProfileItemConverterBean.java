package hk.org.ha.model.pms.biz.medprofile;

import static hk.org.ha.model.pms.prop.Prop.VETTING_MEDPROFILE_INACTIVEWARD_PRINTING_MODE;
import static hk.org.ha.model.pms.prop.Prop.VETTING_DRUG_FORM_ALLOWHALFTABLET;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.model.pms.biz.reftable.ChargeSpecialtyManagerLocal;
import hk.org.ha.model.pms.biz.reftable.ItemDispConfigManagerLocal;
import hk.org.ha.model.pms.biz.reftable.ItemLocationListManagerLocal;
import hk.org.ha.model.pms.biz.reftable.MpWardManagerLocal;
import hk.org.ha.model.pms.biz.reftable.RefTableManagerLocal;
import hk.org.ha.model.pms.biz.reftable.WardConfigManagerLocal;
import hk.org.ha.model.pms.biz.reftable.WarningManagerLocal;
import hk.org.ha.model.pms.biz.vetting.mp.CheckAtdpsManagerLocal;
import hk.org.ha.model.pms.dms.persistence.DmWarning;
import hk.org.ha.model.pms.dms.vo.conversion.ConvertParam;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItemInf;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.persistence.phs.WardStock;
import hk.org.ha.model.pms.persistence.reftable.ItemDispConfig;
import hk.org.ha.model.pms.persistence.reftable.ItemLocation;
import hk.org.ha.model.pms.persistence.reftable.WardConfig;
import hk.org.ha.model.pms.prop.Prop;
import hk.org.ha.model.pms.udt.medprofile.MedProfilePoItemRefillDurationUnit;
import hk.org.ha.model.pms.udt.reftable.ChargeSpecialtyType;
import hk.org.ha.model.pms.udt.reftable.MachineType;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.udt.vetting.RegimenType;
import hk.org.ha.model.pms.vo.rx.CapdItem;
import hk.org.ha.model.pms.vo.rx.CapdRxDrug;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.IvAdditive;
import hk.org.ha.model.pms.vo.rx.IvFluid;
import hk.org.ha.model.pms.vo.rx.IvRxDrug;
import hk.org.ha.model.pms.vo.rx.OralRxDrug;
import hk.org.ha.model.pms.vo.rx.PharmDrug;
import hk.org.ha.model.pms.vo.rx.Regimen;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.ejb.Stateless;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;

@AutoCreate
@Stateless
@Name("medProfileItemConverter")
public class MedProfileItemConverterBean implements MedProfileItemConverterLocal {

	private static final String DIRECT = "direct";	
	
	private static final String JAXB_CONTEXT_RX = "hk.org.ha.model.pms.vo.rx";
	
	@Logger
	private Log logger;

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

	@In
	private RefTableManagerLocal refTableManager;

	@In
	private MpWardManagerLocal mpWardManager;
	
	@In
	private WardConfigManagerLocal wardConfigManager;

	@In
	private WarningManagerLocal warningManager;
		
	@In
	private ItemDispConfigManagerLocal itemDispConfigManager;
	
	@In
	private ChargeSpecialtyManagerLocal chargeSpecialtyManager;
	
	@In
	private ItemLocationListManagerLocal itemLocationListManager;
	
	@In
	private CheckAtdpsManagerLocal checkAtdpsManager;
	
	@Override
	public void convertItemList(Workstore workstore, List<MedProfileMoItem> medProfileMoItemList, List<Integer> atdpsPickStationsNumList) {
		
		if (CollectionUtils.isEmpty(medProfileMoItemList)) {
			return;
		}
		
		convertItemList(workstore, medProfileMoItemList.get(0).getMedProfileItem().getMedProfile(), medProfileMoItemList, atdpsPickStationsNumList);
	}
	
	@Override
	public void convertItemList(Workstore workstore, MedProfile medProfile, List<MedProfileMoItem> medProfileMoItemList, List<Integer> atdpsPickStationsNumList) {
		
		if (CollectionUtils.isEmpty(medProfileMoItemList)) {
			return;
		}
		
		JaxbWrapper<RxItem> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
		ConvertParam convertParam = this.createConvertParam(medProfile, workstore);
		
		try {
			List<String> rxItemXmlList = convertRxItemXmlList(rxJaxbWrapper, medProfileMoItemList);
			
			rxItemXmlList = dmsPmsServiceProxy.convertDrug(rxItemXmlList, convertParam);
			
			for (int i = 0; i < rxItemXmlList.size(); i++) {
				processConvertedItem(rxJaxbWrapper, workstore, medProfile, medProfileMoItemList.get(i), rxItemXmlList.get(i), atdpsPickStationsNumList);
			}
			
		} catch (Exception e) {
			logger.error("Cannot convert Item", e);
		}
	}
	
	private List<String> convertRxItemXmlList(JaxbWrapper<RxItem> rxJaxbWrapper, List<?> itemList) {
		
		List<String> rxItemXmlList = new ArrayList<String>();

		for (Object item: itemList) {
			
			RxItem rxItem;
			if (item instanceof MedProfileMoItemInf) {
				rxItem = ((MedProfileMoItemInf) item).getRxItem();
			} else if (item instanceof MedOrderItem) {
				rxItem = ((MedOrderItem) item).getRxItem();
			} else if (item == null) {
				throw new IllegalArgumentException("Null value not allowed for conversion");
			} else {
				throw new IllegalArgumentException("Class " + item.getClass().getName() + " not supported for conversion");
			}
			
			String rxItemXml = rxJaxbWrapper.marshall(rxItem);
			rxItemXmlList.add(rxItemXml);
		}
		
		return rxItemXmlList;
	}
	
	
	
	private void processConvertedItem(JaxbWrapper<RxItem> rxJaxbWrapper, Workstore workstore, MedProfile medProfile, MedProfileMoItem medProfileMoItem,
			String rxItemXml, List<Integer> atdpsPickStationsNumList) {
		
		RxItem rxItem = rxJaxbWrapper.unmarshall(rxItemXml);
		if (rxItem == null) {
			return;
		}
		
		this.convertToMedProfilePoItemList(workstore, medProfile, medProfileMoItem, rxItem, atdpsPickStationsNumList);
		this.markMedProfilePoItemFlag(workstore, medProfile, medProfileMoItem, atdpsPickStationsNumList);
		this.markPrintInstructionFlag(workstore, medProfile, medProfileMoItem);

		rxItem.clearPharmDrugList();		
		rxItem.buildTlf();
		medProfileMoItem.setRxItem(rxItem);
		logger.debug("medProfileMoItem.getRxItem().getManualItemFlag():#0, Prop.VETTING_MEDPROFILE_PIVAS_IPMOEPIVASITEM_DISPENSE.get().booleanValue():#1", 
				medProfileMoItem.getRxItem().getManualItemFlag(), Prop.VETTING_MEDPROFILE_PIVAS_IPMOEPIVASITEM_DISPENSE.get().booleanValue());
		if (!BooleanUtils.isTrue(medProfileMoItem.getRxItem().getManualItemFlag()) 
				&& Prop.VETTING_MEDPROFILE_PIVAS_IPMOEPIVASITEM_DISPENSE.get().booleanValue()) {
			medProfileMoItem.setPivasFlag(rxItem.getPivasFormulaFlag());
		}
		medProfileMoItem.preSave();
	}

	@Override
	public void formatMedProfileMoItemList(List<? extends MedProfileMoItemInf> medProfileMoItemList) {
		
		formatItemList(medProfileMoItemList, false);
	}
	
	@Override
	public void formatDischargeMoItemList(List<?> itemList) {

		formatItemList(itemList, true);
	}
	
	private void formatItemList(List<?> itemList, boolean dischargeOrder) {

		JaxbWrapper<RxItem> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
		List<String> rxItemXmlList = convertRxItemXmlList(rxJaxbWrapper, itemList);			
		
		try {
			ConvertParam convertParam = new ConvertParam();
			convertParam.setOrderType(dischargeOrder ? "D" : "I");
			
			rxItemXmlList = dmsPmsServiceProxy.formatDrugOrderXml(rxItemXmlList, convertParam);
			
			for (int i = 0; i < rxItemXmlList.size(); i++) {

				Object item = itemList.get(i);
				RxItem rxItem = rxJaxbWrapper.unmarshall(rxItemXmlList.get(i));
				
				if (rxItem == null) {
					continue;
				}
				rxItem.clearPharmDrugList();
				rxItem.buildTlf();
				
				if (item instanceof MedProfileMoItemInf) {
					((MedProfileMoItemInf) item).setRxItem(rxItem);
					if (item instanceof MedProfileMoItem) {
						((MedProfileMoItem) item).preSave();
					}
				} else if (item instanceof MedOrderItem) {
					((MedOrderItem) item).setRxItem(rxItem);
				}
			}
		} catch (Exception e) {
			logger.error("Cannot format Item", e);
		}
	}

	private void convertToMedProfilePoItemList(Workstore workstore, MedProfile medProfile, MedProfileMoItem medProfileMoItem, RxItem rxItem, List<Integer> atdpsPickStationsNumList) {

		if (rxItem.isRxDrug()) 
		{
			convertToMedProfilePoItemList(workstore, medProfile, medProfileMoItem, (RxDrug) rxItem, atdpsPickStationsNumList);
		}
		else if (rxItem.isIvRxDrug())
		{
			convertToMedProfilePoItemList(workstore, medProfile, medProfileMoItem, (IvRxDrug) rxItem, atdpsPickStationsNumList);
		}
		else if (rxItem.isCapdRxDrug())
		{
			convertToMedProfilePoItemList(workstore, medProfile, medProfileMoItem, (CapdRxDrug) rxItem, atdpsPickStationsNumList);
		}
	}

	private void convertToMedProfilePoItemList(Workstore workstore, MedProfile medProfile, MedProfileMoItem medProfileMoItem, RxDrug rxDrug, List<Integer> atdpsPickStationsNumList) {

		for (PharmDrug pharmDrug : rxDrug.getPharmDrugList()) {
			this.addMedProfilePoItem(workstore, medProfile, medProfileMoItem, pharmDrug, 
					rxDrug.getActionStatus(), rxDrug.getFmStatus(), atdpsPickStationsNumList);
		}
	}

	private void convertToMedProfilePoItemList(Workstore workstore, MedProfile medProfile, MedProfileMoItem medProfileMoItem, IvRxDrug ivRxDrug, List<Integer> atdpsPickStationsNumList) {

		for (IvAdditive ivAdditive : ivRxDrug.getIvAdditiveList()) 
		{
			for (PharmDrug pharmDrug : ivAdditive.getPharmDrugList()) 
			{
				this.addMedProfilePoItem(workstore, medProfile, medProfileMoItem, pharmDrug, 
						ivAdditive.getActionStatus(), ivAdditive.getFmStatus(), atdpsPickStationsNumList);
			}								
		}

		for (IvFluid ivFluid : ivRxDrug.getIvFluidList()) 
		{
			for (PharmDrug pharmDrug : ivFluid.getPharmDrugList()) 
			{
				this.addMedProfilePoItem(workstore, medProfile, medProfileMoItem, pharmDrug, 
						ivFluid.getActionStatus(), null, atdpsPickStationsNumList);
			}
		}
	}
	
	private void convertToMedProfilePoItemList(Workstore workstore, MedProfile medProfile, MedProfileMoItem medProfileMoItem, CapdRxDrug capdRxDrug, List<Integer> atdpsPickStationsNumList) {
		
		for (CapdItem capdItem : capdRxDrug.getCapdItemList())
		{
			for (PharmDrug pharmDrug : capdItem.getPharmDrugList()) {
				this.addMedProfilePoItem(workstore, medProfile, medProfileMoItem, pharmDrug, 
						capdRxDrug.getActionStatus(), null, atdpsPickStationsNumList);
			}
		}
	}

	private void addMedProfilePoItem(Workstore workstore, MedProfile medProfile, MedProfileMoItem medProfileMoItem, PharmDrug pharmDrug, 
			ActionStatus actionStatus, String fmStatus, List<Integer> atdpsPickStationsNumList) {

		MedProfilePoItem medProfilePoItem = new MedProfilePoItem();
		BeanUtils.copyProperties(pharmDrug, medProfilePoItem);
		medProfilePoItem.setWorkstore(workstore);

		medProfilePoItem.setDueDate(resolveDueDate(pharmDrug, medProfileMoItem.getStartDate()));
		if (actionStatus != null) {
			medProfilePoItem.setActionStatus(actionStatus);
		}
		medProfilePoItem.setFmStatus(fmStatus);

		medProfilePoItem.setRefillDurationUnit(
				MedProfilePoItemRefillDurationUnit.dataValueOf(pharmDrug.getRegimen().getType().getDataValue()));
		
		if (pharmDrug.getCalDuration() != null) {
			medProfilePoItem.setRefillDuration(pharmDrug.getCalDuration());
		}

		if (pharmDrug.getRegimen() != null) {
			medProfilePoItem.setRegimen(pharmDrug.getRegimen());
			medProfilePoItem.setSingleDispFlag(isSingleDispenseItem(medProfile, medProfileMoItem, medProfilePoItem));
			medProfilePoItem.setStatFlag(MedProfileUtils.isStatDose(pharmDrug.getRegimen()));
			medProfilePoItem.getRegimen().loadDmInfo();
		}
		
		medProfilePoItem.setCalQty(medProfilePoItem.getCalQty() == null ? BigDecimal.ZERO : medProfilePoItem.getCalQty());
		medProfilePoItem.setIssueQty(medProfilePoItem.getCalQty() == null ? BigDecimal.ZERO : medProfilePoItem.getCalQty());				

		//update warning codes
		this.updateMedProfilePoItemWarnCode(medProfilePoItem, workstore.getWorkstoreGroup());
		
		medProfilePoItem.setDrugSynonym(refTableManager
				.getFdnByItemCodeOrderTypeWorkstore(medProfilePoItem.getItemCode(),
						OrderType.InPatient, workstore));

		Map<ChargeSpecialtyType, String> chargeSpecCodeMap = chargeSpecialtyManager.retrieveChargeSpecialtyCodeMap(
				workstore, medProfile.getSpecCode());
		
		medProfilePoItem.updateChargeSpecCode(chargeSpecCodeMap, medProfile.getSpecCode());	
		
		if ( medProfile.getInactivePasWardFlag() ) {
			medProfilePoItem.setDirectPrintFlag(DIRECT.equals(VETTING_MEDPROFILE_INACTIVEWARD_PRINTING_MODE.get(workstore)));
		}
		
		medProfilePoItem.setUuid(UUID.randomUUID().toString());
		medProfileMoItem.addMedProfilePoItem(medProfilePoItem);
	}

	private void updateMedProfilePoItemWarnCode(MedProfilePoItem medProfilePoItemIn, WorkstoreGroup workstoreGroup) {

		List<DmWarning> localWarningList = warningManager.retrieveLocalWarningList(medProfilePoItemIn.getItemCode(), workstoreGroup);
		List<DmWarning> resultList;
		if ( localWarningList != null ) {
			resultList = localWarningList;
		} else {
			resultList = warningManager.retrieveMoeWarmingList(medProfilePoItemIn.getItemCode());			
		}

		DmWarning dmWarning;
		String warnCode;				
		for (int i = 1; i<=4; i++) {
			try {											
				dmWarning = resultList.get(i-1);					
				if ( dmWarning != null ) {
					warnCode = dmWarning.getWarnCode();
				} else {
					warnCode = null;
				} 
				PropertyUtils.setProperty(medProfilePoItemIn, "warnCode"+String.valueOf(i), warnCode);					
			} catch (IllegalAccessException e) {
				throw new RuntimeException("IllegalAccessException");
			} catch (InvocationTargetException e) {
				throw new RuntimeException("InvocationTargetException");
			} catch (NoSuchMethodException e) {
				throw new RuntimeException("NoSuchMethodException");
			}				
		}		
	}

	public boolean isSingleDispenseItem(MedProfile medProfile, MedProfileMoItem medProfileMoItem, MedProfilePoItem medProfilePoItem) {
		
		if (isPublicPatientOwnedSfiItem(medProfile, medProfilePoItem)) {
			return true;//PMSIPU-2013
		}
		
		if (medProfile.isManualProfile() && StringUtils.isBlank(medProfile.getMedCase().getPasWardCode())) {
			return true;//PMSIPU-1988
		}
		
		Regimen regimen = medProfilePoItem.getRegimen();
		
		if (medProfileMoItem.getRxItem() instanceof CapdRxDrug && MedProfileUtils.isOnceDose(regimen)) {
			return true;
		}

		if (medProfileMoItem.getDoseSpecifiedFlag()) {
			return true;
		}
		
		if (medProfilePoItem.getActionStatus() == ActionStatus.ContinueWithOwnStock ||
				regimen.getType() == RegimenType.StepUpDown || 
				MedProfileUtils.isStatDose(regimen) || MedProfileUtils.isAtOnceDose(regimen)) {
			return true;
		}
		
		for (Dose dose: regimen.getDoseGroupList().get(0).getDoseList()) {
			if (Boolean.TRUE.equals(dose.getPrn()) || MedProfileUtils.isValidNonZeroValue(dose.getDispQty())) {
				return true;
			}
		}
		
		ItemDispConfig itemDispConfig = itemDispConfigManager.retrieveItemDispConfig(
				medProfilePoItem.getItemCode(), medProfilePoItem.getWorkstore().getWorkstoreGroup(), false);

		return itemDispConfig != null && Boolean.TRUE.equals(itemDispConfig.getSingleDispFlag());
	}
	
	private boolean isPublicPatientOwnedSfiItem(MedProfile medProfile, MedProfilePoItem medProfilePoItem) {
		return !Boolean.TRUE.equals(medProfile.getPrivateFlag()) && medProfilePoItem.isSfiItem();
	}
	
	@Override
	public MedProfilePoItem recalculateQty(Workstore workstore, MedProfile medProfile, MedProfilePoItem medProfilePoItem) {
		return recalculateQty(workstore, medProfile, Arrays.asList(medProfilePoItem)).get(0);
	}
	
	@Override
	public List<MedProfilePoItem> recalculateQty(Workstore workstore, MedProfile medProfile, List<MedProfilePoItem> medProfilePoItemList) {
		
		ConvertParam convertParam = this.createConvertParam(medProfile, workstore);
		convertParam.setIsReCalQty(Boolean.TRUE);
		JaxbWrapper<RxItem> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);

		List<String> rxItemXmlList = new ArrayList<String>();
		
		for (MedProfilePoItem medProfilePoItem: medProfilePoItemList) {
			if (medProfilePoItem.getMedProfileMoItem() == null) {
			 	throw new RuntimeException("Missing relationship linkage from medProfilePoItem[id:"+medProfilePoItem.getId()+", itemCode:"+medProfilePoItem.getItemCode()+", medProfileMoItemId:"+medProfilePoItem.getMedProfileMoItemId()+"] to medProfileMoItem of medProfile[id:"+medProfile.getId()+", orderNum:"+medProfile.getOrderNum()+"]"); 
			}
			
			if (medProfilePoItem.getMedProfileMoItem().getDoseSpecifiedFlag() 
					&& !Boolean.TRUE.equals(medProfilePoItem.getSingleDispFlag())) {
				medProfilePoItem.setTotalIssueQty(null);
				medProfilePoItem.setCalBothIssueQtyAndTotalQtyForDoseSpecified(Boolean.TRUE);
			}
			
			RxItem rxItem = constructRecalculationRxItem(rxJaxbWrapper, medProfile, medProfilePoItem);
			rxItemXmlList.add(rxJaxbWrapper.marshall(rxItem));
		}
		
		rxItemXmlList = dmsPmsServiceProxy.convertDrug(rxItemXmlList, convertParam);
		
		for (int i = 0; i < rxItemXmlList.size(); i++) {
			
			RxItem rxItem = (RxItem) rxJaxbWrapper.unmarshall(rxItemXmlList.get(i));
			assignCalculatedValues(rxItem, medProfilePoItemList.get(i));
		}
		
		return medProfilePoItemList;
	}

	private RxItem constructRecalculationRxItem(JaxbWrapper<RxItem> rxJaxbWrapper, MedProfile medProfile, MedProfilePoItem medProfilePoItem) {

		if ( medProfilePoItem.getMedProfileMoItem().isManualItem() ) {
			medProfilePoItem.getMedProfileMoItem().preSave();
		}

		PharmDrug pharmDrug = constructRecalculationPharmDrug(medProfilePoItem);
		RxItem rxItem = rxJaxbWrapper.unmarshall(medProfilePoItem.getMedProfileMoItem().getRxItemXml());
		
		if (rxItem instanceof RxDrug) {
			
			((RxDrug) rxItem).setPharmDrugList(Arrays.asList(pharmDrug));
			return rxItem;
			
		} else if (rxItem instanceof IvRxDrug && (pharmDrug.getAdditiveNum() != null || pharmDrug.getFluidNum() != null)) {
			IvRxDrug ivRxDrug = (IvRxDrug) rxItem;
			
			for (IvAdditive ivAdditive: ivRxDrug.getIvAdditiveList()) {
				if (MedProfileUtils.equalsButNotNull(ivAdditive.getAdditiveNum(), pharmDrug.getAdditiveNum())) {
					ivAdditive.setPharmDrugList(Arrays.asList(pharmDrug));
					return rxItem;
				}
			}
			for (IvFluid ivFluid: ivRxDrug.getIvFluidList()) {
				if (MedProfileUtils.equalsButNotNull(ivFluid.getFluidNum(), pharmDrug.getFluidNum())) {
					ivFluid.setPharmDrugList(Arrays.asList(pharmDrug));
					return rxItem;
				}
			}
		} else if (rxItem instanceof CapdRxDrug) {
			
			CapdRxDrug capdRxDrug = new CapdRxDrug();
			CapdItem capdItem = new CapdItem();
			capdItem.setPharmDrugList(Arrays.asList(pharmDrug));
			capdRxDrug.addCapdItem(capdItem);
			return capdRxDrug;
		}
		
		OralRxDrug rxDrug = new OralRxDrug();
		rxDrug.setActionStatus(medProfilePoItem.getActionStatus());
		rxDrug.setPharmDrugList(Arrays.asList(pharmDrug));
		return rxDrug;
	}
	
	private PharmDrug constructRecalculationPharmDrug(MedProfilePoItem medProfilePoItem) {
		
		PharmDrug pharmDrug = new PharmDrug();		
		BeanUtils.copyProperties(medProfilePoItem, pharmDrug, new String[]{"dmDrug"});		
		if (pharmDrug.getRegimen().getType() != RegimenType.StepUpDown) {
			pharmDrug.setCalDuration(medProfilePoItem.getRefillDuration());
		}
		return pharmDrug;
	}
	
	private void assignCalculatedValues(RxItem rxItem, MedProfilePoItem medProfilePoItem) {
		
		if (rxItem instanceof RxDrug) {
			
			RxDrug rxDrug = (RxDrug) rxItem;
			assignCalculatedValues(rxDrug.getPharmDrugList(), medProfilePoItem);
			
		} else if (rxItem instanceof IvRxDrug) {
			
			IvRxDrug ivRxDrug = (IvRxDrug) rxItem;
			
			for (IvAdditive ivAdditive: ivRxDrug.getIvAdditiveList()) {
				if (MedProfileUtils.equalsButNotNull(ivAdditive.getAdditiveNum(), medProfilePoItem.getAdditiveNum())) {
					assignCalculatedValues(ivAdditive.getPharmDrugList(), medProfilePoItem);
					return;
				}
			}
			for (IvFluid ivFluid: ivRxDrug.getIvFluidList()) {
				if (MedProfileUtils.equalsButNotNull(ivFluid.getFluidNum(), medProfilePoItem.getFluidNum())) {
					assignCalculatedValues(ivFluid.getPharmDrugList(), medProfilePoItem);
					return;
				}
			}
		} else if (rxItem instanceof CapdRxDrug) {
			
			CapdRxDrug capdRxDrug = (CapdRxDrug) rxItem;
			List<CapdItem> capdItemList = capdRxDrug.getCapdItemList();
			if (!capdItemList.isEmpty()) {
				assignCalculatedValues(capdItemList.get(0).getPharmDrugList(), medProfilePoItem);
			}
		}
	}
	
	private void assignCalculatedValues(List<PharmDrug> pharmDrugList, MedProfilePoItem medProfilePoItem) {
		
		if (!pharmDrugList.isEmpty()) {
			PharmDrug pharmDrug = pharmDrugList.get(0);
			medProfilePoItem.setDoseQty(pharmDrug.getDoseQty());
			medProfilePoItem.setCalQty(pharmDrug.getCalQty());
			medProfilePoItem.setDispDurationCalcFlag(pharmDrug.getDispDurationCalcFlag());
			medProfilePoItem.setTotalIssueQty(pharmDrug.getTotalIssueQty());
			if (BooleanUtils.isTrue(medProfilePoItem.getDispDurationCalcFlag())) {
				medProfilePoItem.setRefillDuration(pharmDrug.getCalDuration());//PMSIPU-1687
			}
		}
	}
	
	private ConvertParam createConvertParam(MedProfile medProfile, Workstore workstore) {

		if (workstore == null) {
			throw new IllegalArgumentException("Parameter workstore cannot be null");
		}
		
		ConvertParam param = new ConvertParam();
		param.setDispHospCode(workstore.getHospCode());
		param.setDispWorkstore(workstore.getWorkstoreCode());
		param.setIsHalfTab(VETTING_DRUG_FORM_ALLOWHALFTABLET.get(workstore, Boolean.TRUE));
		param.setIsChest(Boolean.FALSE);
		param.setIsReCalQty(Boolean.FALSE);
		param.setOrderType("I");
		param.setIsPrivatePatient(
				(BooleanUtils.isTrue(medProfile.getPrivateFlag()) || StringUtils.equals(medProfile.getMedCase().getPasPayCode(),"PIP"))?
						Boolean.TRUE
						:Boolean.FALSE);
		param.setPasWardCode(medProfile.getMedCase().getPasWardCode());
		param.setAllowPivasFlag(Prop.PIVAS_ENABLED.get());
		param.setDefPatHospCode(workstore.getDefPatHospCode());
		param.setWorkstoreGroupCode(workstore.getWorkstoreGroup().getWorkstoreGroupCode());		
		param.setIsManualProfile(medProfile == null ? Boolean.FALSE : Boolean.valueOf(medProfile.isManualProfile()));
		
		return param;
	}
	
	private Date resolveDueDate(PharmDrug pharmDrug, Date defDueDate) {
		
		Regimen regimen = pharmDrug.getRegimen();
		
		if (regimen != null && regimen.getDoseGroupList().size() > 0) {
			Date dueDate = regimen.getDoseGroupList().get(0).getStartDate();
			if (dueDate != null) {
				return dueDate;
			}
		}
		return defDueDate;
	}

	private void markMedProfilePoItemFlag(Workstore workstore, MedProfile medProfile, MedProfileMoItem medProfileMoItem, List<Integer> atdpsPickStationsNumList) {

		Map<String, WardStock> wardStockMap = mpWardManager.retrieveWardStockMap(
				workstore.getWorkstoreGroup().getInstitution().getInstCode(),
				medProfile.getWardCode());

		for (MedProfilePoItem medProfilePoItem : medProfileMoItem.getMedProfilePoItemList()) {
			medProfilePoItem.setWardStockFlag(wardStockMap.containsKey(medProfilePoItem.getItemCode()));
			medProfilePoItem.setUnitDoseFlag(isUnitDose(workstore, medProfile, medProfileMoItem, medProfilePoItem, atdpsPickStationsNumList));
		}
	}
	
	private void markPrintInstructionFlag(Workstore workstore, MedProfile medProfile, MedProfileMoItem medProfileMoItem) {
		
		WardConfig wardConfig = wardConfigManager.retrieveWardConfig(workstore.getWorkstoreGroup(), medProfile.getWardCode());
		medProfileMoItem.markDefaultPrintInstructionFlag(wardConfig);
	}
	
	public boolean isUnitDose(Workstore workstore, MedProfile medProfile, MedProfileMoItem medProfileMoItem, MedProfilePoItem medProfilePoItem, List<Integer> atdpsPickStationsNumList){		
		List<ItemLocation> itemLocationList = itemLocationListManager.retrieveItemLocationList(medProfilePoItem.getItemCode(),
																								atdpsPickStationsNumList, 
																								Arrays.asList(MachineType.ATDPS));
		
		medProfilePoItem.setUnitDoseItem(!itemLocationList.isEmpty());
		
		if ( medProfile.getAtdpsWardFlag() == null ) {
			WardConfig wardConfig = wardConfigManager.retrieveWardConfig(workstore.getWorkstoreGroup(), medProfile.getWardCode());
			if ( wardConfig != null ) {
				medProfile.setAtdpsWardFlag(wardConfig.getAtdpsWardFlag());
			} else {
				medProfile.setAtdpsWardFlag(false);
			}
		}

		if ( !medProfile.getAtdpsWardFlag() ) {
			return false;
		}
		
		boolean checkDuration = !medProfile.isManualProfile();
		if ( !checkAtdpsManager.isValidAtdpsItem(medProfileMoItem, medProfilePoItem, atdpsPickStationsNumList, checkDuration, true) ){
			return false;
		}
		return !itemLocationList.isEmpty();
	}

	protected static class ReplaceRegEx
	{
		private Pattern regex;
		private String replacement;

		public ReplaceRegEx(String regexStr, String replacement) {
			this.regex = Pattern.compile(regexStr, Pattern.CASE_INSENSITIVE);
			this.replacement = replacement;
		}

		public String replace(String input) {
			return regex.matcher(input).replaceAll(replacement);
		}
	}
}
