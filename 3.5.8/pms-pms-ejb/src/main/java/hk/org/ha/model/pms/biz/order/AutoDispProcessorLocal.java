package hk.org.ha.model.pms.biz.order;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import org.jboss.seam.annotations.async.Duration;

import javax.ejb.Local;

import org.jboss.seam.annotations.async.Asynchronous;

@Local
public interface AutoDispProcessorLocal {

	@Asynchronous
	void processMedOrder(@Duration Long startupDelay, Workstore workstore, String workstationKey);
}
