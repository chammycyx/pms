package hk.org.ha.model.pms.biz.enquiry;

import hk.org.ha.model.pms.vo.enquiry.DispItemStatusEnq;

import javax.ejb.Local;

@Local
public interface DispItemStatusEnqServiceLocal {
	void retrieveDispItemStatusEnqList(DispItemStatusEnq dispItemStatusEnqCriteria);
	void destroy();	
}