package hk.org.ha.model.pms.vo.charging;

import java.io.Serializable;

import hk.org.ha.model.pms.persistence.corp.Workstore;

public class OverrideChargeAmountRuleCriteria implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String dispHospCode;
	
	private String pharmOrderPatType;
		
	private String chargeType;
	
	private boolean includeMarkup;
	
	private boolean markUpOverride;
	
	private Double markUpOverrideValue;

	public void updateByDrugChargeRuleCriteria(DrugChargeRuleCriteria drugChargeRuleCriteria, Workstore workstore){
		this.dispHospCode = workstore.getHospCode();
		this.pharmOrderPatType = drugChargeRuleCriteria.getPharmOrderPatType();
		this.chargeType = drugChargeRuleCriteria.getChargeType();
		this.includeMarkup = drugChargeRuleCriteria.isIncludeMarkup();
	}
	
	public String getDispHospCode() {
		return dispHospCode;
	}

	public void setDispHospCode(String dispHospCode) {
		this.dispHospCode = dispHospCode;
	}

	public String getPharmOrderPatType() {
		return pharmOrderPatType;
	}

	public void setPharmOrderPatType(String pharmOrderPatType) {
		this.pharmOrderPatType = pharmOrderPatType;
	}

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public boolean isIncludeMarkup() {
		return includeMarkup;
	}
	
	public void setIncludeMarkup(boolean includeMarkup) {
		this.includeMarkup = includeMarkup;
	}
	
	public boolean isMarkUpOverride() {
		return markUpOverride;
	}

	public void setMarkUpOverride(boolean markUpOverride) {
		this.markUpOverride = markUpOverride;
	}

	public Double getMarkUpOverrideValue() {
		return markUpOverrideValue;
	}

	public void setMarkUpOverrideValue(Double markUpOverrideValue) {
		this.markUpOverrideValue = markUpOverrideValue;
	}
}
