package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class DayEndDefDoctorRptHdr implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String hospName;
	private String hospCode;
	private String workstoreCode;
	private Date batchDate;
	
	public String getHospName() {
		return hospName;
	}
	public void setHospName(String hospName) {
		this.hospName = hospName;
	}
	public String getHospCode() {
		return hospCode;
	}
	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}
	public String getWorkstoreCode() {
		return workstoreCode;
	}
	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}
	public Date getBatchDate() {
		return batchDate;
	}
	public void setBatchDate(Date batchDate) {
		this.batchDate = batchDate;
	}
}
