package hk.org.ha.model.pms.biz.alert;

import flexjson.JSONSerializer;
import flexjson.transformer.DateTransformer;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;
import hk.org.ha.model.pms.vo.alert.mds.ErrorInfo;
import hk.org.ha.model.pms.vo.alert.mds.MedProfileAlertMsg;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

@AutoCreate
@Stateless
@Name("alertAuditManager")
@MeasureCalls
public class AlertAuditManagerBean implements AlertAuditManagerLocal {

	private static final String MDS_DATE_PATTERN = "yyyy-MM-dd";
	
	private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern(MDS_DATE_PATTERN);
	
	private static final JSONSerializer serializer = new JSONSerializer().transform(new DateTransformer("yyyy-MM-dd HH:mm:ss.SSS"), Date.class);
	
	@In
	private AuditLogger auditLogger;

	@In(required=false)
	private PharmOrder pharmOrder;

	public void alertServiceUnavailable(PharmOrder pharmOrder, Ticket ticket) {
		alertServiceUnavailable(pharmOrder, ticket == null ? null : ticket.getTicketDate(), ticket == null ? null : ticket.getTicketNum());
	}

	public void alertServiceUnavailable(PharmOrder pharmOrder, Date ticketDate, String ticketNum) {
		writeAuditLog("#0640", pharmOrder.getMedOrder().getOrderNum(), pharmOrder.getHkid(), ticketDate, ticketNum,
				pharmOrder.getMedCase() == null ? null : pharmOrder.getMedCase().getCaseNum());
	}

	public void mdsServiceUnavailable(String hkid, Ticket ticket, String orderNum, String caseNum) {
		writeAuditLog("#0641", orderNum, hkid, ticket == null ? null : ticket.getTicketDate(), ticket == null ? null : ticket.getTicketNum(), caseNum);
	}

	public void drugOnHandServiceUnavailable(String orderNum, String hkid, Ticket ticket, String caseNum) {
		writeAuditLog("#0642", orderNum, hkid, ticket == null ? null : ticket.getTicketDate(), ticket == null ? null : ticket.getTicketNum(), caseNum);
	}

	private void writeAuditLog(String messageCode, String orderNum, String hkid, Date ticketDate, String ticketNum, String caseNum) {
		auditLogger.log(messageCode,
				orderNum,
				hkid,
				ticketNum,
				ticketDate == null ? null : DATE_FORMATTER.print(new DateTime(ticketDate)),
				caseNum);
	}

	public void newAlertDetect(PharmOrder pharmOrder, Ticket ticket, List<MedProfileAlertMsg> prescAlertMsgList) {
		auditLogger.log("#0639",
				pharmOrder.getMedOrder().getId(),
				pharmOrder.getHkid(),
				ticket == null ? null : DATE_FORMATTER.print(new DateTime(ticket.getTicketDate())),
				ticket == null ? null : ticket.getTicketNum(),
				pharmOrder.getMedCase() == null ? null : pharmOrder.getMedCase().getCaseNum(),
				serializer.exclude("class").exclude("alertMsgList.alertList").exclude("alertMsgList.alertXmlList").exclude("alertMsgList.class").deepSerialize(prescAlertMsgList));
	}

	public void alertMsg(String messageCode, String desc) {
		
		auditLogger.log(messageCode,
				pharmOrder.getMedOrder().getOrderNum(),
				pharmOrder.getHkid(),
				pharmOrder.getMedOrder().getTicket().getTicketNum(),
				DATE_FORMATTER.print(new DateTime(pharmOrder.getMedOrder().getTicket().getTicketDate())),
				pharmOrder.getMedCase() == null ? null : pharmOrder.getMedCase().getCaseNum(),
				desc);
	}
	
	public void newAlertDetectForManualProfile(String action, Long medProfileId, String hkid, String caseNum, List<AlertMsg> alertMsgList) {
		auditLogger.log("#0722",
				action,
				medProfileId,
				hkid,
				caseNum,
				serializer.exclude("class").exclude("alertList").exclude("alertXmlList").exclude("*.medItem").exclude("*.medProfileMoItem").deepSerialize(alertMsgList));
	}
	
	public void cancelPrescribeForManualProfile(String action, Long medProfileId, String hkid, String caseNum, String orderDesc) {
		auditLogger.log("#0723",
				action,
				medProfileId,
				hkid,
				caseNum,
				orderDesc);
	}

	public void reminderMsgForManualProfile(String action, Long medProfileId, String hkid, String caseNum, List<ErrorInfo> errorInfoList) {
		
		StringBuilder errorInfoDesc = new StringBuilder();
		for (ErrorInfo errorInfo : errorInfoList) {
			errorInfoDesc.append(errorInfo.getDesc());
			errorInfoDesc.append('\n');
			if (errorInfo.getCause() != null) {
				errorInfoDesc.append(errorInfo.getCause());
				errorInfoDesc.append('\n');
				errorInfoDesc.append(errorInfo.getAction());
				errorInfoDesc.append('\n');
			}
		}
		auditLogger.log("#0733",
				action,
				medProfileId,
				hkid,
				caseNum,
				errorInfoDesc);
	}
}
