package hk.org.ha.model.pms.biz.report;

import static hk.org.ha.model.pms.prop.Prop.*;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.alert.mds.AlertMsgManagerLocal;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.persistence.AlertEntity;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryRequest;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryRequestAlert;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.udt.alert.AlertProfileStatus;
import hk.org.ha.model.pms.udt.alert.ehr.EhrAlertStatus;
import hk.org.ha.model.pms.udt.medprofile.ErrorLogType;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.alert.AlertProfile;
import hk.org.ha.model.pms.vo.alert.PatAlert;
import hk.org.ha.model.pms.vo.alert.mds.Alert;
import hk.org.ha.model.pms.vo.alert.mds.AlertDdim;
import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;
import hk.org.ha.model.pms.vo.alert.mds.MedProfileAlertMsg;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.MdsExceptionOverrideRpt;
import hk.org.ha.model.pms.vo.report.MdsExceptionOverrideRptDtl;
import hk.org.ha.model.pms.vo.report.MdsExceptionRpt;
import hk.org.ha.model.pms.vo.report.MdsExceptionRptContainer;
import hk.org.ha.model.pms.vo.report.MdsExceptionRptDtl;
import hk.org.ha.model.pms.vo.report.MdsExceptionRptFtr;
import hk.org.ha.model.pms.vo.report.MdsExceptionRptHdr;
import hk.org.ha.model.pms.vo.report.MdsExceptionRptItemDtl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("mdsExceptionRptManager")
@MeasureCalls
public class MdsExceptionRptManagerBean implements MdsExceptionRptManagerLocal {

	private static final String RPT_NAME = "MdsExceptionRpt";

	private static final String MP_PAT_RPT_NAME = "MdsExceptionMpOverridePatRpt";
	
	private static final String MP_DDI_RPT_NAME = "MdsExceptionMpOverrideDdiRpt";

	@In
	private AlertMsgManagerLocal alertMsgManager;

	@In
	private PrinterSelectorLocal printerSelector;

	@PersistenceContext
	private EntityManager em;

	public List<MdsExceptionRptContainer> retrieveMdsExceptionRptListByMedProfileAlertMsg(MedProfile medProfile, List<MedProfileAlertMsg> medProfileAlertMsgList, AlertProfile alertProfile) {
		MdsExceptionRptContainer mdsExceptionRptContainer = new MdsExceptionRptContainer(
																	Arrays.asList(constructMdsExceptionRpt(
																	medProfile,
																	medProfileAlertMsgList,
																	alertProfile,
																	Boolean.TRUE,
																	Boolean.FALSE,
																	medProfile.getPregnancyCheckFlag()
																)));
		
		return Arrays.asList(mdsExceptionRptContainer);
	}
	
	public List<MdsExceptionRptContainer> retrieveMdsExceptionRptListByDeliveryReqId(Long deliveryReqId) {
		
		DeliveryRequest deliveryRequest = em.find(DeliveryRequest.class, deliveryReqId);
		
		Map<String, DeliveryRequestAlert> alertMap = new HashMap<String, DeliveryRequestAlert>();
		for (DeliveryRequestAlert deliveryRequestAlert : deliveryRequest.getDeliveryRequestAlertList()) {
			alertMap.put(deliveryRequestAlert.getMedProfileId() == null ? deliveryRequestAlert.getCaseNum() : deliveryRequestAlert.getMedProfileId().toString(), deliveryRequestAlert);
		}
		
		@SuppressWarnings("unchecked")
		List<MedProfile> medProfileList = em.createQuery(
				"select distinct o.medProfileItem.medProfile" + // 20120305 index check : MedProfileErrorLog.deliveryRequestId : FK_MED_PROFILE_ERROR_LOG_01
				" from MedProfileErrorLog o" +
				" where o.deliveryRequestId = :deliveryRequestId" +
				" and o.type = :type" +
				" order by o.medProfileItem.medProfile.wardCode," +
				" o.medProfileItem.medProfile.medCase.caseNum," +
				" o.medProfileItem.medProfileMoItem.itemNum")
				.setParameter("deliveryRequestId", deliveryReqId)
				.setParameter("type", ErrorLogType.MdsAlert)
				.getResultList();

		List<MdsExceptionRptContainer> containerList = new ArrayList<MdsExceptionRptContainer>();
		List<MdsExceptionRpt> mdsExceptionRptList = new ArrayList<MdsExceptionRpt>();
		
		for (MedProfile medProfile : medProfileList) {
			
			DeliveryRequestAlert deliveryRequestAlert = alertMap.containsKey(medProfile.getId().toString()) ? alertMap.get(medProfile.getId().toString()) : alertMap.get(medProfile.getMedCase().getCaseNum());
			
			if (deliveryRequestAlert.getMdsCheckResult().isIncludeManualProfileFlag()) { // data after integration
				if (!deliveryRequestAlert.getMdsCheckResult().getMedProfileAlertMsgList().isEmpty()) {
					mdsExceptionRptList.add(constructMdsExceptionRpt( // for ipmoe
							medProfile,
							deliveryRequestAlert.getMdsCheckResult().getMedProfileAlertMsgList(),
							deliveryRequestAlert.getAlertProfile(),
							deliveryRequestAlert.getEhrAlertProfileFlag(),
							Boolean.TRUE,
							deliveryRequestAlert.getPregnancyCheckFlag()
						));
				}
				for (AlertMsg alertMsg : deliveryRequestAlert.getMdsCheckResult().getManualProfileAlertMsgList()) {
					alertMsg.loadAlertEntityList();
					alertMsg.loadDrugDesc();
					containerList.addAll(contructMdsExceptionMpOverrideRpt( // for non-ipmoe
							medProfile,
							alertMsg,
							deliveryRequestAlert.getAlertProfile(),
							deliveryRequestAlert.getEhrAlertProfileFlag(),
							deliveryRequestAlert.getPregnancyCheckFlag()
						));
				}
			}
			else { // data before integration
				if (medProfile.getMedProfileItemList().get(0).getMedProfileMoItem().isManualItem()) { 
					containerList.addAll(contructMdsExceptionMpOverrideRpt( // for non-ipmoe
							medProfile,
							deliveryRequestAlert.getMdsCheckResult().getMedProfileAlertMsgList(),
							deliveryRequestAlert.getAlertProfile(),
							deliveryRequestAlert.getEhrAlertProfileFlag(),
							deliveryRequestAlert.getPregnancyCheckFlag()
						));
				}
				else {
					mdsExceptionRptList.add(constructMdsExceptionRpt( // for ipmoe
							medProfile,
							deliveryRequestAlert.getMdsCheckResult().getMedProfileAlertMsgList(),
							deliveryRequestAlert.getAlertProfile(),
							deliveryRequestAlert.getEhrAlertProfileFlag(),
							Boolean.TRUE,
							deliveryRequestAlert.getPregnancyCheckFlag()
						));
				}
			}
			
		}
		
		if (!mdsExceptionRptList.isEmpty()) {
			containerList.add(0, new MdsExceptionRptContainer(mdsExceptionRptList));
		}
		
		return containerList;
	}

	private List<MdsExceptionRptContainer> contructMdsExceptionMpOverrideRpt(MedProfile medProfile, List<MedProfileAlertMsg> medProfileAlertMsgList, AlertProfile alertProfile, Boolean ehrAlertProfileFlag, Boolean pregnancyCheckFlag) {
		List<MdsExceptionRptContainer> reportList = new ArrayList<MdsExceptionRptContainer>();
		for (MedProfileAlertMsg medProfileAlertMsg : medProfileAlertMsgList) {
			medProfileAlertMsg.loadDrugDesc();
			reportList.addAll(contructMdsExceptionMpOverrideRpt(medProfile, medProfileAlertMsg.getAlertMsgList().get(0), alertProfile, ehrAlertProfileFlag, pregnancyCheckFlag));
		}
		return reportList;
	}

	private List<MdsExceptionRptContainer> contructMdsExceptionMpOverrideRpt(MedProfile medProfile, AlertMsg alertMsg, AlertProfile alertProfile, Boolean ehrAlertProfileFlag, Boolean pregnancyCheckFlag) {
		
		List<MdsExceptionRptContainer> reportList = new ArrayList<MdsExceptionRptContainer>();

		alertMsg.loadAlertEntityList();

		if (!alertMsg.getPatAlertList().isEmpty()) {

			MdsExceptionOverrideRpt patRpt = constructMdsExceptionOverrideRptByMedProfile(medProfile, alertMsg, alertProfile, ehrAlertProfileFlag, pregnancyCheckFlag);
			patRpt.setMdsExceptionOverrideRptDtlList(constructMdsExceptionOverrideRptDtlList(alertMsg.getPatAlertList()));
			reportList.add(new MdsExceptionRptContainer(patRpt, false));
		}
		
		Map<Long, List<AlertEntity>> otherItemDdiAlertMsgMap = new HashMap<Long, List<AlertEntity>>();
		
		for (AlertEntity alertEntity : alertMsg.getDdiAlertList()) {
			AlertDdim alertDdim = (AlertDdim) alertEntity.getAlert();
			if( alertDdim.isCrossDdi() ){
				Long mapItemNum = alertDdim.getItemNum1() > alertDdim.getItemNum2() ?  alertDdim.getItemNum2() : alertDdim.getItemNum1();
				List<AlertEntity> ddiAlertList = otherItemDdiAlertMsgMap.get(mapItemNum);
				if( ddiAlertList == null ){
					ddiAlertList = new ArrayList<AlertEntity>();
				}
				ddiAlertList.add(alertEntity);
				otherItemDdiAlertMsgMap.put(mapItemNum, ddiAlertList);
			}else{
				if (alertDdim.getCopyFlag()) {
					continue;
				}
				MdsExceptionOverrideRpt ddiRpt = constructMdsExceptionOverrideRptByMedProfile(medProfile, alertMsg, alertProfile, ehrAlertProfileFlag, pregnancyCheckFlag);
				if( alertDdim.getItemNum2() > alertDdim.getItemNum1() ){
					ddiRpt.setDrugDisplayName1(alertDdim.getDrugName1());
					ddiRpt.setDrugDisplayName2(alertDdim.getDrugName2());
				}else{
					ddiRpt.setDrugDisplayName1(alertDdim.getDrugName2());
					ddiRpt.setDrugDisplayName2(alertDdim.getDrugName1());
				}
				ddiRpt.addMdsExceptionOverrideRptDtl(constructMdsExceptionOverrideRptDtl(alertEntity, true));
				
				reportList.add(new MdsExceptionRptContainer(ddiRpt, true));
			}
		}

		if( !otherItemDdiAlertMsgMap.isEmpty() ){
			for( Long otherItemNum : otherItemDdiAlertMsgMap.keySet() ){
				MdsExceptionOverrideRpt ddiRpt = constructMdsExceptionOverrideRptByMedProfile(medProfile, alertMsg, alertProfile, ehrAlertProfileFlag, pregnancyCheckFlag);
				
				ddiRpt.setCrossDdiFlag(true);				
				
				AlertDdim alertDdim = (AlertDdim) otherItemDdiAlertMsgMap.get(otherItemNum).get(0).getAlert();
				
				ddiRpt.setDrugDisplayName1(alertDdim.getCrossDdiIpmoeOrderDesc());
				ddiRpt.setDrugDisplayName2(alertDdim.getItemNum2() > alertDdim.getItemNum1() ? alertDdim.getDrugName2() : alertDdim.getDrugName1());
				
				for (AlertEntity crossAlertEntity : otherItemDdiAlertMsgMap.get(otherItemNum)) {
					
					boolean showTitleFlag = true;
					int itemIndex = otherItemDdiAlertMsgMap.get(otherItemNum).indexOf(crossAlertEntity);
					if (itemIndex > 0) {
						AlertEntity prevAlertEntity = otherItemDdiAlertMsgMap.get(otherItemNum).get(itemIndex - 1);
						showTitleFlag = !StringUtils.equals(crossAlertEntity.getAlert().getAlertTitle(), prevAlertEntity.getAlert().getAlertTitle());
					}

					ddiRpt.addMdsExceptionOverrideRptDtl(constructMdsExceptionOverrideRptDtl(crossAlertEntity, showTitleFlag));
				}
				reportList.add(new MdsExceptionRptContainer(ddiRpt, true));
			}
		}
		
		return reportList;
	}

	private MdsExceptionOverrideRptDtl constructMdsExceptionOverrideRptDtl(AlertEntity alertEntity, boolean showTitleFlag) {
		
		MdsExceptionOverrideRptDtl mdsRptDtl = new MdsExceptionOverrideRptDtl();
		
		if (showTitleFlag) {
			mdsRptDtl.setAlertTitle(alertEntity.getAlert().getAlertTitle());
		}
		mdsRptDtl.setAlertDesc(alertEntity.getAlert().getAlertDesc());
		
		return mdsRptDtl;
	}
	
	private List<MdsExceptionOverrideRptDtl> constructMdsExceptionOverrideRptDtlList(List<AlertEntity> alertEntityList) {
		
		String prevTitle = null;
		MdsExceptionOverrideRptDtl mdsRptDtl = null;
		StringBuilder alertDescString = new StringBuilder();
		
		List<MdsExceptionOverrideRptDtl> mdsRptDtlList = new ArrayList<MdsExceptionOverrideRptDtl>();
		
		for (AlertEntity alertEntity : alertEntityList) {
			boolean showTitleFlag = !alertEntity.getAlert().getAlertTitle().equals(prevTitle);

			if( showTitleFlag ){
				if( mdsRptDtl != null ){//Add Prev
					mdsRptDtl.setAlertDesc(alertDescString.toString());
					mdsRptDtlList.add(mdsRptDtl);
				}
				
				mdsRptDtl = new MdsExceptionOverrideRptDtl();
				mdsRptDtl.setAlertTitle(alertEntity.getAlert().getAlertTitle());
				
				alertDescString = new StringBuilder();
				alertDescString.append(alertEntity.getAlert().getAlertDesc());
			}else{
				alertDescString.append("\n");
				alertDescString.append("\n");
				alertDescString.append(alertEntity.getAlert().getAlertDesc());
			}
			prevTitle = alertEntity.getAlert().getAlertTitle();
		}
		//For Last One
		if( mdsRptDtl != null ){
			mdsRptDtl.setAlertDesc(alertDescString.toString());
			mdsRptDtlList.add(mdsRptDtl);
		}
		
		return mdsRptDtlList;
	}

	private MdsExceptionOverrideRpt constructMdsExceptionOverrideRptByMedProfile(MedProfile medProfile, AlertMsg alertMsg, AlertProfile alertProfile, Boolean ehrAlertProfileFlag, Boolean pregnancyCheckFlag) {
		
		String caseNum;
		if (medProfile.getMedCase() == null) {
			caseNum = null;
		}
		else {
			caseNum = medProfile.getMedCase().getCaseNum();
		}
		
		MdsExceptionOverrideRpt mdsRpt = new MdsExceptionOverrideRpt();
		
		StringBuilder sb = new StringBuilder();
		if (alertProfile != null) {
			for (PatAlert patAlert : alertProfile.getPatAlertList()) {
				if (sb.length() > 0) {
					sb.append(", ");
				}
				sb.append(patAlert.getAlertDesc());
			}
		}
		
		mdsRpt.setSex(medProfile.getPatient().getSex().getDataValue());
		mdsRpt.setPatName(medProfile.getPatient().getName());
		mdsRpt.setPatNameChi(medProfile.getPatient().getNameChi());
		mdsRpt.setHkid(medProfile.getPatient().getHkid());
		mdsRpt.setPatCatCode(medProfile.getPatCatCode());
		mdsRpt.setPatAllergy(alertProfile == null || alertProfile.getLastVerifyDate() != null ? null : alertProfile.getAllergyDesc());
		mdsRpt.setPatAdr(alertProfile == null ? null : alertProfile.getAdrDesc());
		mdsRpt.setG6pdFlag(alertProfile != null && alertProfile.getG6pdAlert() != null);
		mdsRpt.setPregnancyCheckFlag(pregnancyCheckFlag);
		mdsRpt.setAllergyServerAvailableFlag(alertProfile != null && alertProfile.getStatus() != AlertProfileStatus.Error && !alertProfile.isEhrAllergyOnly());
		mdsRpt.setDrugDisplayName(alertMsg.getMdsOrderDesc());
		mdsRpt.setCaseNum(caseNum);
		mdsRpt.setMdsExceptionOverrideRptDtlList(new ArrayList<MdsExceptionOverrideRptDtl>());
		mdsRpt.setWard(medProfile.getWardCode());
		mdsRpt.setRemark(REPORT_MDSEXCEPTION_REMARK.get());
		mdsRpt.setRefillFlag(true);
		mdsRpt.setEhrAllergyServerAvailableFlag(ehrAlertProfileFlag == null || (alertProfile != null && alertProfile.getEhrAllergySummary() != null && alertProfile.getEhrAllergySummary().getStatus() != EhrAlertStatus.Error));
		mdsRpt.setEhrAllergy(alertProfile != null && alertProfile.getEhrAllergySummary() != null ? alertProfile.getEhrAllergySummary().getAllergy() : null);
		mdsRpt.setEhrAdr(alertProfile != null && alertProfile.getEhrAllergySummary() != null ? alertProfile.getEhrAllergySummary().getAdr() : null);
		
		return mdsRpt;
	}

	private MdsExceptionRpt constructMdsExceptionRpt(MedProfile medProfile, List<MedProfileAlertMsg> medProfileAlertMsgList, AlertProfile alertProfile, Boolean ehrAlertProfileFlag, Boolean refillFlag, Boolean pregnancyCheckFlag) {
		
		MdsExceptionRpt mdsExceptionRpt = new MdsExceptionRpt();
		mdsExceptionRpt.setMdsExceptionRptHdr(constructMdsExceptionRptHdr(medProfile, alertProfile, ehrAlertProfileFlag, refillFlag, pregnancyCheckFlag));
		mdsExceptionRpt.setMdsExceptionRptFtr(constructMdsExceptionRptFtr());
		
		List<MdsExceptionRptDtl> mdsExceptionRptDtlList = new ArrayList<MdsExceptionRptDtl>();
		for (MedProfileAlertMsg mpAlertMsg : medProfileAlertMsgList) {
			mpAlertMsg.loadDrugDesc();
			mdsExceptionRptDtlList.add(constructMdsExceptionRptDtl(mpAlertMsg));
		}
		mdsExceptionRpt.setMdsExceptionRptDtlList(mdsExceptionRptDtlList);

		return mdsExceptionRpt;
	}

	private MdsExceptionRptHdr constructMdsExceptionRptHdr(MedProfile medProfile, AlertProfile alertProfile, Boolean ehrAlertProfileFlag, Boolean refillFlag, Boolean pregnancyCheckFlag) {
		MdsExceptionRptHdr mdsExceptionRptHdr = new MdsExceptionRptHdr();
		mdsExceptionRptHdr.setRefillFlag(refillFlag);
		mdsExceptionRptHdr.setPatientName(medProfile.getPatient().getName());
		mdsExceptionRptHdr.setPatientNameChi(StringUtils.trimToNull(medProfile.getPatient().getNameChi()));
		mdsExceptionRptHdr.setSex(medProfile.getPatient().getSex() == null ? null : medProfile.getPatient().getSex().getDisplayValue());
		mdsExceptionRptHdr.setCaseNum(medProfile.getMedCase().getCaseNum());
		mdsExceptionRptHdr.setWard(medProfile.getWardCode());
		mdsExceptionRptHdr.setPatAllergy(alertProfile == null || alertProfile.getLastVerifyDate() != null ? null : alertProfile.getAllergyDesc());
		mdsExceptionRptHdr.setPatAdr(alertProfile == null ? null : alertProfile.getAdrDesc());
		mdsExceptionRptHdr.setG6pdFlag(alertProfile != null && alertProfile.getG6pdAlert() != null);
		mdsExceptionRptHdr.setPregnancyCheckFlag(pregnancyCheckFlag);
		mdsExceptionRptHdr.setAllergyServerAvailableFlag(alertProfile != null && alertProfile.getStatus() != AlertProfileStatus.Error && !alertProfile.isEhrAllergyOnly());
		mdsExceptionRptHdr.setEhrAllergyServerAvailableFlag(ehrAlertProfileFlag == null || (alertProfile != null && alertProfile.getEhrAllergySummary() != null && alertProfile.getEhrAllergySummary().getStatus() != EhrAlertStatus.Error));
		mdsExceptionRptHdr.setEhrAllergy(alertProfile != null && alertProfile.getEhrAllergySummary() != null ? alertProfile.getEhrAllergySummary().getAllergy() : null);
		mdsExceptionRptHdr.setEhrAdr(alertProfile != null && alertProfile.getEhrAllergySummary() != null ? alertProfile.getEhrAllergySummary().getAdr() : null);
		return mdsExceptionRptHdr;
	}

	private MdsExceptionRptFtr constructMdsExceptionRptFtr() {
		MdsExceptionRptFtr mdsExceptionRptFtr = new MdsExceptionRptFtr();
		mdsExceptionRptFtr.setRemark(REPORT_MDSEXCEPTION_REMARK.get());
		return mdsExceptionRptFtr;
	}
	
	private MdsExceptionRptDtl constructMdsExceptionRptDtl(MedProfileAlertMsg mpAlertMsg) {
		MdsExceptionRptDtl mdsExceptionRptDtl = new MdsExceptionRptDtl();
		mdsExceptionRptDtl.setOrderDesc(mpAlertMsg.getOrderDesc());
		
		List<MdsExceptionRptItemDtl> mdsExceptionRptItemDtlList = new ArrayList<MdsExceptionRptItemDtl>();
		boolean showDrugDisplayName = mpAlertMsg.getAlertMsgList().size() > 1 || mpAlertMsg.isIvRxDrug();
		for (AlertMsg alertMsg : mpAlertMsg.getAlertMsgList()) {
			mdsExceptionRptItemDtlList.addAll(constructMdsExceptionRptItemDtlList(alertMsg, showDrugDisplayName));
		}
		mdsExceptionRptDtl.setMdsExceptionRptItemDtlList(mdsExceptionRptItemDtlList);
		return mdsExceptionRptDtl;
	}

	private List<MdsExceptionRptItemDtl> constructMdsExceptionRptItemDtlList(AlertMsg alertMsg, boolean showDrugDisplayName) {
		List<MdsExceptionRptItemDtl> mdsExceptionRptItemDtlList = new ArrayList<MdsExceptionRptItemDtl>();
		
		List<Alert> alertList = new ArrayList<Alert>(alertMsg.getAlertList());
		alertMsgManager.sortAlertList(alertList);
		
		String prevTitle = null;
		MdsExceptionRptItemDtl mdsExceptionRptItemDtl = null;
		StringBuilder alertDescString = new StringBuilder();
		for (Alert alert : alertList) {
			boolean showTitleFlag = !alert.getAlertTitle().equals(prevTitle);
			String drugDisplayName = null;
			if (showDrugDisplayName && alertList.indexOf(alert) == 0) {
				drugDisplayName = alertMsg.getMdsOrderDesc();
			}
			
			if( showTitleFlag ){
				if( mdsExceptionRptItemDtl != null ){//Add Prev
					mdsExceptionRptItemDtl.setAlertDesc(alertDescString.toString());
					mdsExceptionRptItemDtlList.add(mdsExceptionRptItemDtl);
				}
				
				mdsExceptionRptItemDtl = new MdsExceptionRptItemDtl();
				mdsExceptionRptItemDtl.setAlertTitle(alert.getAlertTitle());
				mdsExceptionRptItemDtl.setDrugDisplayName(drugDisplayName);
				
				alertDescString = new StringBuilder();
				alertDescString.append(alert.getAlertDesc());
			}else{
				alertDescString.append("\n");
				alertDescString.append("\n");
				alertDescString.append(alert.getAlertDesc());
			}
			prevTitle = alert.getAlertTitle();
		}
		//For Last One
		if( mdsExceptionRptItemDtl != null ){
			mdsExceptionRptItemDtl.setAlertDesc(alertDescString.toString());
			mdsExceptionRptItemDtlList.add(mdsExceptionRptItemDtl);
		}
		return mdsExceptionRptItemDtlList;
	}

	public List<RenderAndPrintJob> constructRenderAndPrintJob(List<MdsExceptionRptContainer> mdsExceptionRptList) {

		List<RenderAndPrintJob> jobList = new ArrayList<RenderAndPrintJob>();

		List<MdsExceptionRpt> ipmoeRptList = new ArrayList<MdsExceptionRpt>();

		for (MdsExceptionRptContainer container : mdsExceptionRptList) {
			if (container.getMdsExceptionOverrideRpt() != null) {
				jobList.add(new RenderAndPrintJob(
						container.isDdiRpt() ? MP_DDI_RPT_NAME : MP_PAT_RPT_NAME,
						new PrintOption(),
						printerSelector.retrievePrinterSelect(PrintDocType.Report),
						Arrays.asList(container.getMdsExceptionOverrideRpt()))
				);
			}
			else {
				ipmoeRptList.addAll(container.getMdsExceptionRptList());
			}
		}

		if (!ipmoeRptList.isEmpty()) {
			jobList.add(0, new RenderAndPrintJob(
					RPT_NAME,
					new PrintOption(),
					printerSelector.retrievePrinterSelect(PrintDocType.Report),
					ipmoeRptList
			));
		}

		return jobList;
	}

	public RenderJob constructRenderJob(List<MdsExceptionRptContainer> mdsExceptionRptList, int index) {
		
		if (index >= mdsExceptionRptList.size()) {
			return null;
		}
		
		MdsExceptionRptContainer container = mdsExceptionRptList.get(index);

		if (container.getMdsExceptionOverrideRpt() != null) {
			return new RenderJob(
					container.isDdiRpt() ? MP_DDI_RPT_NAME : MP_PAT_RPT_NAME,
					new PrintOption(),
					Arrays.asList(container.getMdsExceptionOverrideRpt()));
		}
		else {
			return new RenderJob(
					RPT_NAME,
					new PrintOption(),
					container.getMdsExceptionRptList());
		}
	}
}
