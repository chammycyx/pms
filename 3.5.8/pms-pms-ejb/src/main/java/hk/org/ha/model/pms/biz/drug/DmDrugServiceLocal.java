package hk.org.ha.model.pms.biz.drug;

import javax.ejb.Local;

@Local
public interface DmDrugServiceLocal {
	
	void retrieveDmDrug(String itemCode);
	
	void retrieveDmDrugForOrderEdit(String displayName, String fmStatus, String itemCode);
	
	void destroy();
	
}
 