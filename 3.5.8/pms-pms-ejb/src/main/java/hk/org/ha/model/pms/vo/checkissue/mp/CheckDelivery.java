package hk.org.ha.model.pms.vo.checkissue.mp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class CheckDelivery implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Long deliveryId;
	
	private Long deliveryVersion;
	
	private Date batchDate;
	
	private String batchNum;
	
	private List<DeliveryProblemItem> deliveryProblemItemList;

	public Long getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(Long deliveryId) {
		this.deliveryId = deliveryId;
	}
	
	public Long getDeliveryVersion() {
		return deliveryVersion;
	}

	public void setDeliveryVersion(Long deliveryVersion) {
		this.deliveryVersion = deliveryVersion;
	}

	public Date getBatchDate() {
		return batchDate;
	}

	public void setBatchDate(Date batchDate) {
		this.batchDate = batchDate;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public List<DeliveryProblemItem> getDeliveryProblemItemList() {
		if ( deliveryProblemItemList == null ) {
			deliveryProblemItemList = new ArrayList<DeliveryProblemItem>();
		}
		return deliveryProblemItemList;
	}

	public void setDeliveryProblemItemList(List<DeliveryProblemItem> deliveryProblemItemList) {
		this.deliveryProblemItemList = deliveryProblemItemList;
	}
}