package hk.org.ha.model.pms.biz.onestop;

import static hk.org.ha.model.pms.prop.Prop.ONESTOP_SUSPENDORDERCOUNT_DURATION_LIMIT;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.onestop.SuspendPrescManagerBean.ReversePrescriptionSuspendResult;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.vo.charging.FcsPaymentStatus;
import hk.org.ha.model.pms.vo.charging.SfiForceProceedInfo;
import hk.org.ha.model.pms.vo.onestop.MoStatusCount;
import hk.org.ha.model.pms.vo.security.PropMap;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("suspendPrescService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SuspendPrescServiceBean implements SuspendPrescServiceLocal {
			
	@In(required = false)
	@Out(required = false)
	private SfiForceProceedInfo sfiForceProceedInfo;
	
	@In
	private OneStopOrderStatusManagerLocal oneStopOrderStatusManager;
		
	@In
	private OneStopCountManagerLocal oneStopCountManager;
	
	@In
	private SuspendPrescManagerLocal suspendPrescManager;

	@In
	private Workstore workstore;
	
	@In
	private AuditLogger auditLogger;
	
	@In
	private UamInfo uamInfo;
	
	@In
	private Workstation workstation;
	
	@In
	public PropMap propMap;
		
	@Out(required = false)
	private MoStatusCount moStatusCount;
	
	private FcsPaymentStatus fcsPaymentStatus;
	
	private boolean sfiForceProceed;
	
	private String statusErrorMsg;
	
	public String checkSuspendOrderStatus(Long dispOrderId, Long dispOrderVersion)
	{
		retrieveUnvetAndSuspendCount();
		statusErrorMsg = oneStopOrderStatusManager.verifySuspendAllowModifyUncollectStatus(dispOrderId, dispOrderVersion);
		return statusErrorMsg;
	}
	
	@Override
	public void verifySuspendPrescription(Long dispOrderId, Long dispOrderVersion, String suspendCode, String caller) {
		
		if ("oneStopEntry".equals(caller))
		{
			if (checkSuspendOrderStatus(dispOrderId, dispOrderVersion) == null)
			{
				suspendPrescManager.verifySuspendPrescription(dispOrderId, dispOrderVersion, suspendCode, caller);
				auditLogger.log("#0516", uamInfo.getUserId(), workstation.getHostName(), dispOrderId, suspendCode);
			}
		}
		else
		{
			suspendPrescManager.verifySuspendPrescription(dispOrderId, dispOrderVersion, suspendCode, caller);
		}
		
		retrieveUnvetAndSuspendCount();
	}
	
	public void reversePrescriptionSuspend(Long dispOrderId, Long dispOrderVersion, String caller) {
		
		if ("oneStopEntry".equals(caller))
		{
			if (checkSuspendOrderStatus(dispOrderId, dispOrderVersion) == null)
			{
				reverseSuspendPrescription(dispOrderId, dispOrderVersion, caller);
				auditLogger.log("#0517", uamInfo.getUserId(), workstation.getHostName(), dispOrderId);
			}
		}
		else
		{
			reverseSuspendPrescription(dispOrderId, dispOrderVersion, caller);
		}
		
		retrieveUnvetAndSuspendCount();
	}
	
	private void reverseSuspendPrescription(Long dispOrderId, Long dispOrderVersion, String caller)
	{
		ReversePrescriptionSuspendResult result = suspendPrescManager.reversePrescriptionSuspend(dispOrderId, dispOrderVersion, caller);
		fcsPaymentStatus = result.getFcsPaymentStatus();
		sfiForceProceed = result.isSfiForceProceed();
	}
	
	@Override
	public void sfiPrescriptionSuspendDeferProceed(Long dispOrderId, Long dispOrderVersion, String caller) {
		
		if (checkSuspendOrderStatus(dispOrderId, dispOrderVersion) == null)
		{
			suspendPrescManager.sfiPrescriptionSuspendDeferProceed(dispOrderId, caller);
			auditLogger.log("#0517", uamInfo.getUserId(), workstation.getHostName(), dispOrderId);
		}
		
		retrieveUnvetAndSuspendCount();
	}

	@Override
	public void sfiPrescriptionSuspendForceProceed(Long dispOrderId, Long dispOrderVersion, String caller) {
		
		if (checkSuspendOrderStatus(dispOrderId, dispOrderVersion) == null)
		{
			suspendPrescManager.sfiPrescriptionSuspendForceProceed(dispOrderId, caller, sfiForceProceedInfo);
			sfiForceProceedInfo = null;
			auditLogger.log("#0517", uamInfo.getUserId(), workstation.getHostName(), dispOrderId);
		}
		
		retrieveUnvetAndSuspendCount();
	}
	
	private void retrieveUnvetAndSuspendCount() {
		if (oneStopCountManager.isCounterTimeOut(moStatusCount))
		{
			oneStopCountManager.retrieveUnvetAndSuspendCount(workstore, propMap.getValueAsInteger(ONESTOP_SUSPENDORDERCOUNT_DURATION_LIMIT.getName()));	
		}
		
		moStatusCount = OneStopCountManagerBean.getMoStatusCountByWorkstore(workstore);
	}
	
	public FcsPaymentStatus getFcsPaymentStatus() {
		return fcsPaymentStatus; 
	}
	
	public Boolean getSfiForceProceed() {
		return sfiForceProceed;
	}
	
	public String getStatusErrorMsg() {
		return statusErrorMsg;
	}

	@Remove
	public void destroy(){
		if (moStatusCount != null)
		{
			moStatusCount = null;
		}
	}
}
