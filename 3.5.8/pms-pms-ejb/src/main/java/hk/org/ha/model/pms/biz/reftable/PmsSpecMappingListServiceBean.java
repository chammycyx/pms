package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.cache.PasSpecialtyCacherInf;
import hk.org.ha.model.pms.corp.cache.PasWardCacherInf;
import hk.org.ha.model.pms.dms.persistence.PmsSpecMapping;
import hk.org.ha.model.pms.persistence.corp.HospitalMapping;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.phs.Specialty;
import hk.org.ha.model.pms.persistence.phs.Ward;
import hk.org.ha.model.pms.vo.patient.PasSpecialty;
import hk.org.ha.model.pms.vo.patient.PasSubSpecialty;
import hk.org.ha.model.pms.vo.patient.PasWard;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("pmsSpecMappingListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PmsSpecMappingListServiceBean implements PmsSpecMappingListServiceLocal {

	@PersistenceContext
	private EntityManager em;
	
	@In
	private Workstore workstore;

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private PasSpecialtyCacherInf pasSpecialtyCacher;
	
	@In
	private PasWardCacherInf pasWardCacher;
	
	@In
	private SpecialtyManagerLocal specialtyManager;
	
	@In
	private WardManagerLocal wardManager;

	@Out(required = false)
	private List<PasSpecialty> pmsSpecMappingPasSpecialtyList;

	@Out(required = false)
	private List<Workstore> pharmacyWorkstoreList;
	
	@Out(required = false)
	private List<Specialty> phsSpecialtyList;

	@Out(required = false)
	private List<PasWard> pasWardList;
	
	@Out(required = false)
	private List<Ward> wardList;
	
	@Out(required = false)
	private List<HospitalMapping> pmsSpecMappingHospitalMappingList;
	
	@Out(required = false)
	private List<PmsSpecMapping> pmsSpecMappingList;
	
	private List<String> patHospList;
	
	private boolean retrieveSuccess;
	
	public void retrievePmsSpecMappingList() {
		if(initRefList()) {
			retrieveSuccess = true;
			pmsSpecMappingList = dmsPmsServiceProxy.retrievePmsSpecMappingList( patHospList );
		}else {
			retrieveSuccess = false;
		}
	}
	
	private boolean initRefList() {
		workstore = em.find(Workstore.class, workstore.getId());
		if ( pmsSpecMappingHospitalMappingList == null ) {
			pmsSpecMappingHospitalMappingList = retrievePmsSpecMappingHospitalMappingList();
		}
		
		if ( pmsSpecMappingPasSpecialtyList == null ) {
			pmsSpecMappingPasSpecialtyList = new ArrayList<PasSpecialty>();
			for ( HospitalMapping hospitalMapping : pmsSpecMappingHospitalMappingList ) {
				pmsSpecMappingPasSpecialtyList.addAll(pasSpecialtyCacher.getPasSpecialtyWithSubSpecialtyListByPatHospCode(hospitalMapping.getPatHospCode()));
			}

			Collections.sort(pmsSpecMappingPasSpecialtyList, new Comparator<PasSpecialty>() {
				@Override
				public int compare(PasSpecialty o1, PasSpecialty o2) {
					return new CompareToBuilder().append( o1.getPasHospCode(), o2.getPasHospCode() )
												 .append(o1.getType(), o2.getType())
												 .append(o1.getPasSpecCode(), o2.getPasSpecCode())
												 .toComparison();
				}
			});
			
			for ( PasSpecialty pasSpecialty : pmsSpecMappingPasSpecialtyList) {
				if ( pasSpecialty != null && pasSpecialty.getPasSubSpecialtyList() != null) {
					Collections.sort(pasSpecialty.getPasSubSpecialtyList(), new Comparator<PasSubSpecialty>() {
						@Override
						public int compare(PasSubSpecialty o1, PasSubSpecialty o2) {
							return new CompareToBuilder().append( o1.getPasSubSpecCode(), o2.getPasSubSpecCode() ).toComparison();
						}
					});
				}
			}
		}
		
		if ( pasWardList == null ) {
			pasWardList = new ArrayList<PasWard>();
			for (HospitalMapping hospitalMapping : pmsSpecMappingHospitalMappingList) {
				pasWardList.addAll(pasWardCacher.getPasWardList(hospitalMapping.getPatHospCode()));
			}
		}
		
		if ( patHospList == null ) {
			patHospList = createPatHospList( pmsSpecMappingHospitalMappingList );
		}
		
		if ( pharmacyWorkstoreList == null ) {
			pharmacyWorkstoreList = retrievePharmacyWorkstoreList( patHospList );
		}	
		
		Set<String> instCodeSet = new HashSet<String>();
		for (Workstore workstore : pharmacyWorkstoreList) {
			instCodeSet.add(workstore.getWorkstoreGroup().getInstitution().getInstCode());
		}
		
		if ( wardList == null ) {
			if ( instCodeSet.isEmpty() ) {
				wardList = new ArrayList<Ward>();
			} else {
				wardList = wardManager.retrieveWardListByInstCode(new ArrayList<String>(instCodeSet));
			}
		}
		
		if ( phsSpecialtyList == null ) {
			if ( instCodeSet.isEmpty() ) {
				phsSpecialtyList = new ArrayList<Specialty>();
			} else {
				phsSpecialtyList = specialtyManager.retrieveSpecialtyListByInstCode(new ArrayList<String>(instCodeSet));
			}
		}
		
		return true;
	}
	
	@SuppressWarnings("unchecked")
	private List<Workstore> retrievePharmacyWorkstoreList(List<String> patHospCodeList) {
		
		if ( patHospCodeList.isEmpty() ) {
			return new ArrayList<Workstore>();
		}
		
		List<String> hospCodeList = em.createQuery(
				"select o.hospCode from HospitalMapping o" + // 20120831 index check : none
				" where o.patHospCode in :patHospCodeList" +
				" and o.prescribeFlag = :prescribeFlag")
				.setParameter("patHospCodeList", patHospCodeList)
				.setParameter("prescribeFlag", Boolean.TRUE)
				.getResultList();

		
		List<Workstore> workstoreList = em.createQuery(
											"select o from Workstore o" + // 20120831 index check : Workstore.hospital : FK_WORKSTORE_01
											" where o.hospCode in :hospCodeList" +
											" order by o.hospCode, o.workstoreCode")
											.setParameter("hospCodeList", hospCodeList)
											.getResultList();
		
		for ( Workstore workstore : workstoreList ) {
			workstore.getWorkstoreGroup().getInstitution();
		}
		return workstoreList;
		
	}
	
	private List<HospitalMapping> retrievePmsSpecMappingHospitalMappingList() 
	{
		List<HospitalMapping> hospMappingList = workstore.getHospital().getHospitalMappingList();
		
		if ( !hospMappingList.isEmpty() ) {
			for (Iterator<HospitalMapping> itr = hospMappingList.iterator();itr.hasNext();) {
				HospitalMapping hospitalMapping = itr.next();
				if ( !hospitalMapping.getPrescribeFlag() ) {
					itr.remove();
				}
			}	
		}
		return hospMappingList;
	}
	
	private List<String> createPatHospList(List<HospitalMapping> hospitalMappingList)  
	{
		List<String> tmpPatHospList = new ArrayList<String>();
		if ( !pmsSpecMappingHospitalMappingList.isEmpty() ) {
			for ( HospitalMapping hospitalMapping : hospitalMappingList ) {
				tmpPatHospList.add(hospitalMapping.getPatHospCode());
			}
		}
		return tmpPatHospList;
	}
	
	@Remove
	public void destroy() 
	{
		if (pmsSpecMappingPasSpecialtyList != null) {
			pmsSpecMappingPasSpecialtyList = null;
		}
		if (pmsSpecMappingList != null) {
			pmsSpecMappingList = null;
		}
		if (pasWardList != null) {
			pasWardList = null;
		}
		if (pharmacyWorkstoreList != null) {
			pharmacyWorkstoreList = null;
		}
		if (phsSpecialtyList != null) {
			phsSpecialtyList = null;
		}
		if (wardList != null) {
			wardList = null;
		}
		if (pmsSpecMappingHospitalMappingList != null) {
			pmsSpecMappingHospitalMappingList = null;
		}
	}

	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}
}
