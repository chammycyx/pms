package hk.org.ha.model.pms.udt.pivas;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PivasProductLabelType implements StringValuedEnum {
	New("N","New"),
	NewSingle("SN","New Single"),
	Replenish("RR","Replenish");

    private final String dataValue;
    private final String displayValue;
    
    PivasProductLabelType(final String dataValue,final String displayValue)
    {
    	this.dataValue = dataValue;
    	this.displayValue = displayValue;
    }
	
	@Override
	public String getDataValue() {
		return this.dataValue;
	}

	@Override
	public String getDisplayValue() {
		return this.displayValue;
	}
	
    public static PivasProductLabelType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PivasProductLabelType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<PivasProductLabelType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<PivasProductLabelType> getEnumClass() {
    		return PivasProductLabelType.class;
    	}
    }

}
