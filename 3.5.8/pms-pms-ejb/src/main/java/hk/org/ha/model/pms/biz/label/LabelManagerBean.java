package hk.org.ha.model.pms.biz.label;

import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_HOSPITAL_NAMECHI;
import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_HOSPITAL_NAMEENG;
import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_LARGEFONT_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_LARGELAYOUT_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.LABEL_MPDISPLABEL_LARGEFONT_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.LABEL_MPDISPLABEL_WARDRTNBARCODE_VISIBLE;
import static hk.org.ha.model.pms.prop.Prop.LABEL_PIVASMDSOVERRIDINGLABEL_PRINTING_SEQ;
import static hk.org.ha.model.pms.prop.Prop.LABEL_PIVASOUTERBAGLABEL_PRINTING_SEQ;
import static hk.org.ha.model.pms.prop.Prop.LABEL_PIVASPRODUCTLABEL_PRINTING_SEQ;
import static hk.org.ha.model.pms.prop.Prop.LABEL_PIVASWORKSHEET_LARGEFONT_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.LABEL_PIVASWORKSHEET_PRINTING_SEQ;
import static hk.org.ha.model.pms.prop.Prop.MACHINE_ATDPS_MESSAGE_ENCODING;
import static hk.org.ha.model.pms.prop.Prop.MACHINE_ATDPS_NEWITEM_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.MACHINE_ATDPS_PORT;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.fmk.pms.util.StringArray;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.delivery.DeliveryManagerLocal;
import hk.org.ha.model.pms.biz.label.MpDispLabelComparator.SortField;
import hk.org.ha.model.pms.biz.order.DispOrderConverterLocal;
import hk.org.ha.model.pms.biz.order.DispOrderManagerLocal;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.biz.reftable.ItemLocationListManagerLocal;
import hk.org.ha.model.pms.biz.reftable.RefTableManagerLocal;
import hk.org.ha.model.pms.biz.reftable.WardConfigManagerLocal;
import hk.org.ha.model.pms.biz.vetting.mp.CheckAtdpsManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.dms.exception.MsException;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasContainer;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaMethod;
import hk.org.ha.model.pms.exception.delivery.DueOrderPrintException;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedOrderItemAlert;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.medprofile.Delivery;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItemAlert;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.persistence.reftable.FdnMapping;
import hk.org.ha.model.pms.persistence.reftable.ItemLocation;
import hk.org.ha.model.pms.persistence.reftable.Machine;
import hk.org.ha.model.pms.persistence.reftable.WardConfig;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.machine.AtdpsPrescNature;
import hk.org.ha.model.pms.udt.machine.AtdpsPrintType;
import hk.org.ha.model.pms.udt.machine.MachinePrintLang;
import hk.org.ha.model.pms.udt.medprofile.DeliveryItemStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfilePoItemRefillDurationUnit;
import hk.org.ha.model.pms.udt.medprofile.PrintMode;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.printing.PrintStatus;
import hk.org.ha.model.pms.udt.printing.PrintType;
import hk.org.ha.model.pms.udt.reftable.FdnType;
import hk.org.ha.model.pms.udt.reftable.MachineType;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.label.BaseLabel;
import hk.org.ha.model.pms.vo.label.DeliveryLabel;
import hk.org.ha.model.pms.vo.label.DispLabel;
import hk.org.ha.model.pms.vo.label.DividerLabel;
import hk.org.ha.model.pms.vo.label.Instruction;
import hk.org.ha.model.pms.vo.label.LabelContainer;
import hk.org.ha.model.pms.vo.label.MdsOverrideReasonLabel;
import hk.org.ha.model.pms.vo.label.MpDispLabel;
import hk.org.ha.model.pms.vo.label.PivasOuterBagLabel;
import hk.org.ha.model.pms.vo.label.PivasProductLabel;
import hk.org.ha.model.pms.vo.label.PivasWorksheet;
import hk.org.ha.model.pms.vo.label.Warning;
import hk.org.ha.model.pms.vo.machine.AtdpsMessage;
import hk.org.ha.model.pms.vo.machine.AtdpsMessageContent;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.PrinterSelect;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.Regimen;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Transient;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang3.tuple.MutableTriple;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;
import org.joda.time.DateTime;

@AutoCreate
@Stateless
@Name("labelManager")
@MeasureCalls
public class LabelManagerBean implements LabelManagerLocal {

	private static final String JAXB_CONTEXT_LABEL = "hk.org.ha.model.pms.vo.label";
	
	private static final String MP_DISP_LABEL = "MpDispLabel";
	private static final String MP_DISP_LABEL_NO_INST = "MpDispLabelNoInst";
	private static final String MDS_OVERRIDE_REASON_LABEL = "MdsOverrideReasonLabel";
	private static final String PIVAS_PRODUCT_LABEL = "PivasProductLabel";
	private static final String PIVAS_WORKSHEET = "PivasWorksheet";
	private static final String PIVAS_OUTER_BAG_LABEL = "PivasOuterBagLabel";
	
	@PersistenceContext
	private EntityManager em;
	
	@Logger
	private Log logger;
	
	@In
	private DispLabelBuilderLocal dispLabelBuilder;
	
	@In
	private PivasLabelBuilderLocal pivasLabelBuilder;

	@In
	private DispOrderConverterLocal dispOrderConverter;
	
	@In
	private DeliveryManagerLocal deliveryManager;
	
	@In
	private DispOrderManagerLocal dispOrderManager;

	@In
	private RefTableManagerLocal refTableManager;

	@In
	private PrintAgentInf printAgent; 

	@In
	private Workstore workstore;

	@In
	private Workstation workstation;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private WardConfigManagerLocal wardConfigManager;
	
	@In
	private ItemLocationListManagerLocal itemLocationListManager;
	
	@In
	private InstructionBuilder instructionBuilder;
	
	@In
	private CheckAtdpsManagerLocal checkAtdpsManager;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	
	private DateFormat df = new SimpleDateFormat("yyyyMMdd"); 
	private DateFormat timeDf = new SimpleDateFormat("HH:mm:ss"); 
	private DateFormat datetimeDf = new SimpleDateFormat("yyyyMMdd HH:mm:ss"); 
	private static final char ASTERISK = '*';
	private static final String UNIT_DOSE = "unitDose";
	
	public List<DispLabel> getDispLabelPreview( PharmOrder pharmOrder, PrintLang printLang, String printType ) {
		PrintOption printOption = new PrintOption();
		printOption.setPrintLang(printLang);
		printOption.setPrintStatus(PrintStatus.Preview);
		if (LABEL_DISPLABEL_LARGELAYOUT_ENABLED.get(Boolean.FALSE)) {
			printOption.setPrintType(PrintType.LargeLayout);
		} else if (LABEL_DISPLABEL_LARGEFONT_ENABLED.get(Boolean.FALSE)) {
			printOption.setPrintType(PrintType.LargeFont);
		} else {
			printOption.setPrintType(PrintType.Normal);
		}

		for ( PharmOrderItem pharmOrderItem : pharmOrder.getPharmOrderItemList() ) {
			pharmOrderItem.setPharmOrder(pharmOrder);
		}
		
		List<DispLabel> dispLabelList = new ArrayList<DispLabel>();
		DispOrder dispOrder = dispOrderManager.createDispOrderFromPharmOrderForLabel(pharmOrder);

		// retrieve Floating Drug Name Map
		Map<String,String> fdnMappingMap = refTableManager.getFdnMappingMapByDispOrderItemList(dispOrder.getDispOrderItemList(), OrderType.OutPatient, workstore);

		for( DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList() ) {	
			DispLabel dl = dispLabelBuilder.convertToDispLabel(dispOrderItem, fdnMappingMap);
			dl.setItemNum(-1);
			dl.setPrintOption(printOption);
			dispLabelList.add( dl );
		}
		
		return dispLabelList;
	}	

	public List<MpDispLabel> getMpDispLabelPreview( MedProfileItem medProfileItem, MedProfileMoItem medProfileMoItem) {
		PrintOption printOption = new PrintOption();
		
		MedProfile medProfile = (MedProfile) Contexts.getSessionContext().get("medProfile");
		printOption.setPrintLang(medProfile.getPrintLang());
		printOption.setPrintStatus(PrintStatus.Preview);
		if (LABEL_MPDISPLABEL_LARGEFONT_ENABLED.get(Boolean.FALSE)) {
			printOption.setPrintType(PrintType.LargeFont);
		} else {
			printOption.setPrintType(PrintType.Normal);
		}
		
		DispOrder dispOrder = dispOrderConverter.convertMedProfileItemToDispOrder(medProfileItem, medProfileMoItem, workstore);
		
		int itemNum = 0;
		List<MpDispLabel> mpDispLabelList = new ArrayList<MpDispLabel>();
		for( DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList() ) {
			MpDispLabel mpDispLabel = dispLabelBuilder.convertToMpDispLabel(dispOrderItem);
			mpDispLabel.setItemNum(++itemNum);
			mpDispLabel.setPrintOption(printOption);
			if (!dispOrderItem.getPrintMpDispLabelInstructionFlag()) {
				printOption.setPrintType(PrintType.Normal);
				printOption.setPrintLang(PrintLang.Eng);
			}
			mpDispLabelList.add(mpDispLabel);			
		}
		return mpDispLabelList;
	}
	
	@SuppressWarnings("unchecked")
	public BaseLabel getCddhDispLabel(List<String> dispOrderItemKey){
		DispOrderItem dispOrderItem = new DispOrderItem();
		
		if(dispOrderItemKey!=null) {
			Map<StringArray, DispOrderItem> dispOrderItemHashMap = (Map<StringArray, DispOrderItem>) Contexts.getSessionContext().get("dispOrderItemHashMap");
			
			if (dispOrderItemKey.size() == 1) {
				dispOrderItem = dispOrderItemHashMap.get(new StringArray(dispOrderItemKey.get(0)));
			} else if (dispOrderItemKey.size() == 4) {
				dispOrderItem = dispOrderItemHashMap.get(new StringArray(dispOrderItemKey.get(0), 
																			  dispOrderItemKey.get(1), 
																			  dispOrderItemKey.get(2), 
																			  dispOrderItemKey.get(3)));
				logger.debug("insertDispOrderItemToHashMap: hospCode=#0, dispOrderNum=#1, itemNum=#2, issueDate=#3, dispOrderItem=#4", 
						dispOrderItemKey.get(0),
						dispOrderItemKey.get(1),
						dispOrderItemKey.get(2),
						dispOrderItemKey.get(3),
						dispOrderItem);
			}
		}
		
		BaseLabel baseLabel = null;
		if (dispOrderItem.isLegacy()) { // null when it is legacy single dosage
			DispLabel dispLabelConverted = dispLabelBuilder.convertToDispLabel(dispOrderItem, null);
			if (dispLabelConverted != null) {
				PrintOption printOption = new PrintOption();
				printOption.setPrintLang(PrintLang.Eng);
				printOption.setPrintStatus(PrintStatus.Preview);
				dispLabelConverted.setPrintOption(printOption);
				baseLabel = dispLabelConverted;
			}
		} else {
			// capd should not have preview of label
			baseLabel = dispOrderItem.getBaseLabel();
			PrintOption printOption = new PrintOption();
			printOption.setPrintLang(PrintLang.Eng);
			printOption.setPrintStatus(PrintStatus.Preview);
			if (dispOrderItem.getBaseLabel().getLargeLayoutFlag() != null && dispOrderItem.getBaseLabel().getLargeLayoutFlag()) {
				printOption.setPrintType(PrintType.LargeLayout);
			} else {
				// note: CDDH label preview does not have large font 
				printOption.setPrintType(PrintType.Normal);
			}
			baseLabel.setPrintOption(printOption);
		}
		
		if ( baseLabel != null ) {
			logger.debug("Running getCddhDispLabel: patName=#0, dispDate=#1, mpDispLabel=#2, opDispLabel=#3", 
					baseLabel.getPatName(), 
					baseLabel.getDispDate(),
					baseLabel.isMpDispLabel(),
					baseLabel.isOpDispLabel());
		} else {
			logger.debug("Running getCddhDispLabel: baseLabel is null");
		}
		
		return baseLabel;
	}

	@SuppressWarnings("unchecked")
	public MutableTriple<List<RenderAndPrintJob>, List<DispOrder>, List<Object>> genMpDispLabelPrintJobList(List<Delivery> deliveryList, List<Integer> atdpsPickStationsNumList) {
		
		PrinterSelect printerSelect = printerSelector.retrievePrinterSelect(PrintDocType.OtherLabel); 
		List<RenderAndPrintJob> renderAndPrintJobList = new ArrayList<RenderAndPrintJob>();
		List<DispOrder> renderDispOrderList = new ArrayList<DispOrder>();
		
		char sortFormat = 'I';
		boolean printDividerLabel = true;
		
		if( !deliveryList.isEmpty() ){
			sortFormat = deliveryList.get(0).getDeliveryRequest().getSortName().charAt(1);
			printDividerLabel = deliveryList.get(0).getDeliveryRequest().getDividerLabelFlag();
		}

		Set<String> itemCodeSet = new HashSet<String>();
		Set<String> wardCodeSet = new HashSet<String>();
		for (Delivery delivery : deliveryList ) {
			for( DeliveryItem deliveryItem : delivery.getDeliveryItemList() ) {
				MedProfilePoItem medProfilePoItem = deliveryItem.resolveMedProfilePoItem();
				MedProfile medProfile = medProfilePoItem.getMedProfileMoItem().getMedProfileItem().getMedProfile();
				itemCodeSet.add(medProfilePoItem.getItemCode());
				wardCodeSet.add(medProfile.getWardCode());
			}
		}
		
		List<WardConfig> wardConfigList = wardConfigManager.retrieveWardConfigList(workstore.getWorkstoreGroup(), new ArrayList<String>(wardCodeSet));
		Map<String, WardConfig> wardConfigMap = new HashMap<String, WardConfig>();
		for ( WardConfig wardConfig : wardConfigList) {
			wardConfigMap.put(wardConfig.getWardCode(), wardConfig);
		}

		Map<String, List<ItemLocation>> itemLocationListMap = itemLocationListManager.retrieveItemLocationListByItemCodeList(new ArrayList<String>(itemCodeSet), 
																																MachineType.ATDPS, 
																																atdpsPickStationsNumList);
		
		SortField[] sortFields = MpDispLabelComparator.SORT_BY_I_ARRAY;
		List<Object> atdpsMessageObjList = new ArrayList<Object>();
		if( sortFormat != 'L' ){
			Collections.sort(deliveryList, new DeliveryComparator());
			
			for( Delivery delivery : deliveryList ){
				
				List<DispOrder> dispOrderList = dispOrderConverter.convertDeliveryToDispOrderList(delivery, false, workstore);
				renderDispOrderList.addAll(dispOrderList);
				
				Pair labelContent = convertMpDispLabel(dispOrderList, wardConfigMap, itemLocationListMap, atdpsPickStationsNumList);
				List<MpDispLabel> mpDispLabelList = (List<MpDispLabel>)labelContent.getFirst();
				atdpsMessageObjList.addAll((List<Object>)labelContent.getSecond());
				
				if( !mpDispLabelList.isEmpty() ){
					switch (sortFormat) {
						case 'P':
							sortFields = MpDispLabelComparator.SORT_BY_P_ARRAY;
							break;
						case 'B':
							sortFields = MpDispLabelComparator.SORT_BY_B_ARRAY;
							break;
						case 'I':
							sortFields = MpDispLabelComparator.SORT_BY_I_ARRAY;
							break;
						case 'C':
							sortFields = MpDispLabelComparator.SORT_BY_C_ARRAY;
							break;
					}
					sortMpDispLabel(mpDispLabelList, sortFields);
					
					Map<String, Object> parameters = dispLabelBuilder.getMpDispLabelParam(mpDispLabelList.get(0));
					parameters.put("_PNG_ZOOM_RATIO", Float.valueOf(1.3f));
					
					for (MpDispLabel mpDispLabel:mpDispLabelList) {
						buildMpDispLabelPrintJobList(mpDispLabel, printerSelect, parameters, renderAndPrintJobList);
					}
					
					if ( delivery.getPrintMode() == PrintMode.Batch && printDividerLabel ) {					
						int batchDay = new DateTime(delivery.getBatchDate()).getDayOfMonth();
						String batchNum = (( batchDay < 10 )?"0":"") + batchDay + delivery.getBatchNum();
						renderAndPrintJobList.add(genDividerLabel(batchNum, mpDispLabelList, printerSelect));
					}
				}
			}
		}else{
			List<MpDispLabel> mpDispLabelList = new ArrayList<MpDispLabel>();
			
			for( Delivery delivery : deliveryList ){
				
				List<DispOrder> dispOrderList = dispOrderConverter.convertDeliveryToDispOrderList(delivery, false, workstore);
				renderDispOrderList.addAll(dispOrderList);
				
				Pair labelContent = convertMpDispLabel(dispOrderList, wardConfigMap, itemLocationListMap, atdpsPickStationsNumList);
				
				mpDispLabelList.addAll( (List<MpDispLabel>)labelContent.getFirst() );
				atdpsMessageObjList.addAll((List<Object>)labelContent.getSecond());
			}
				
			if( !mpDispLabelList.isEmpty() ){
				
				sortMpDispLabel(mpDispLabelList, MpDispLabelComparator.SORT_BY_L_ARRAY);
				
				Map<String, Object> parameters = dispLabelBuilder.getMpDispLabelParam(mpDispLabelList.get(0));
				parameters.put("_PNG_ZOOM_RATIO", Float.valueOf(1.3f));
				
				for (MpDispLabel mpDispLabel:mpDispLabelList) {
					buildMpDispLabelPrintJobList(mpDispLabel, printerSelect, parameters, renderAndPrintJobList);
				}
			}
		}
				
		return new MutableTriple(renderAndPrintJobList, renderDispOrderList, atdpsMessageObjList);
	}
	
	public MutableTriple<List<RenderAndPrintJob>, List<DispOrder>, List<Object>> genPivasLabelPrintJobList(List<Delivery> deliveryList) {
		
		List<RenderAndPrintJob> renderAndPrintJobList = new ArrayList<RenderAndPrintJob>();
		List<DispOrder> renderDispOrderList = new ArrayList<DispOrder>();
		
		for( Delivery delivery : deliveryList ){
			List<DispOrder> dispOrderList = dispOrderConverter.convertDeliveryToDispOrderList(delivery, false, workstore);
			renderDispOrderList.addAll(dispOrderList);

			List<LabelContainer> labelContainerList = convertPivasLabel(dispOrderList);

			if (labelContainerList.size() > 0) {
				PrintOption worksheetPrintOption = null;
				PrintOption printOption = new PrintOption(PrintType.Normal, PrintLang.Eng, PrintStatus.Print);
				
				if (LABEL_PIVASWORKSHEET_LARGEFONT_ENABLED.get(Boolean.FALSE)) {
					worksheetPrintOption = new PrintOption(PrintType.LargeFont, PrintLang.Eng, PrintStatus.Print);
				} else {
					worksheetPrintOption = new PrintOption(PrintType.Normal, PrintLang.Eng, PrintStatus.Print);
				}
				
				buildPivasLabelPrintJobList(labelContainerList, renderAndPrintJobList, worksheetPrintOption, printOption, null, false);
			}
		}		
		return new MutableTriple<List<RenderAndPrintJob>, List<DispOrder>, List<Object>>(renderAndPrintJobList, renderDispOrderList, null);
	}
	
	public void buildPivasLabelPrintJobList(List<LabelContainer> labelContainerList, List<RenderAndPrintJob> renderAndPrintJobList, 
											PrintOption worksheetPrintOption, PrintOption printOption, List<Long> transactionIdList, boolean isReprint) {
		
        for (LabelContainer labelContainer: labelContainerList) {
        	// PMS-5734 renderAndPrintJobList is used in mapping to avoid wrong setting in PIVAS label sequence rule values and lead to missing label printing
        	Map<Integer, List<RenderAndPrintJob>> printSeqMap = new HashMap<Integer, List<RenderAndPrintJob>>();
        	
	        printSeqMap = buildPivasLabelPrintSeqMap(printSeqMap, LABEL_PIVASWORKSHEET_PRINTING_SEQ.get(), 
	        										buildPivasWorksheetPrintJobList(labelContainer.getPivasWorksheet(), worksheetPrintOption, printerSelector.retrievePrinterSelect(PrintDocType.OtherLabel)));
	        
	        printSeqMap = buildPivasLabelPrintSeqMap(printSeqMap, LABEL_PIVASPRODUCTLABEL_PRINTING_SEQ.get(), 
	        										buildPivasProductLabelPrintJobList(labelContainer.getPivasProductLabel(), printOption, printerSelector.retrievePrinterSelect(PrintDocType.OtherLabel)));
	        
            if (labelContainer.getPivasOuterBagLabel() != null) {
	            printSeqMap = buildPivasLabelPrintSeqMap(printSeqMap, LABEL_PIVASOUTERBAGLABEL_PRINTING_SEQ.get(), 
	            										buildPivasOuterBagLabelPrintJobList(labelContainer.getPivasOuterBagLabel(), printOption, printerSelector.retrievePrinterSelect(PrintDocType.OtherLabel)));
            }
            
            if (labelContainer.getMdsOverrideReasonLabel() != null) {
	            printSeqMap = buildPivasLabelPrintSeqMap(printSeqMap, LABEL_PIVASMDSOVERRIDINGLABEL_PRINTING_SEQ.get(), 
	            										buildMdsOverrideReasonLabelPrintJobList(labelContainer.getMdsOverrideReasonLabel(), printOption, printerSelector.retrievePrinterSelect(PrintDocType.OtherLabel)));
            }
            
            List<Integer> keyList = new ArrayList<Integer>(printSeqMap.keySet());
            Collections.sort(keyList);
            
            for (Integer sortedKey: keyList) {
            	for (RenderAndPrintJob renderAndPrintJob: printSeqMap.get(sortedKey)) {
            		renderAndPrintJobList.add(renderAndPrintJob);
            	}
            }
            
            if (isReprint) {
                transactionIdList.add(Long.parseLong(labelContainer.getPivasProductLabel().getTrxId()));
            }
        }
	}
	
	private Map<Integer, List<RenderAndPrintJob>> buildPivasLabelPrintSeqMap(Map<Integer, List<RenderAndPrintJob>> printSeqMap, Integer seq, RenderAndPrintJob addRenderAndPrintJob) {
	    List<RenderAndPrintJob> mapRenderAndPrintJobList = null;
	    
	    if (printSeqMap.get(seq) != null && !printSeqMap.get(seq).isEmpty()) {
	        mapRenderAndPrintJobList = printSeqMap.get(seq);
	    } else {
	        mapRenderAndPrintJobList = new ArrayList<RenderAndPrintJob>();
	    }
	    mapRenderAndPrintJobList.add(addRenderAndPrintJob);
	    printSeqMap.put(seq, mapRenderAndPrintJobList);
	
		return printSeqMap;
	}
	
	private void buildMpDispLabelPrintJobList(MpDispLabel mpDispLabel, PrinterSelect printerSelect, Map<String, Object> parameters, List<RenderAndPrintJob> renderAndPrintJobList){
		try{
			PrintOption printOption = mpDispLabel.getPrintOption();
			if( printOption.getPrintType() == PrintType.LargeFont ){
				printerSelect.setReverse(true);
			}
			
			String labelType = MP_DISP_LABEL;
			if( !mpDispLabel.getShowInstructionFlag() ){
				labelType = MP_DISP_LABEL_NO_INST;
			}
			
			renderAndPrintJobList.add(new RenderAndPrintJob(labelType,
															mpDispLabel.getPrintOption(),
															printerSelect,
															parameters,
															Arrays.asList(mpDispLabel), 
															"[MpDispLabel] TransactionID:" + mpDispLabel.getDeliveryItemId() +
															" BatchNum:" + mpDispLabel.getBatchNum() +
															" ItemCode:" + mpDispLabel.getItemCode()));
			
			if( mpDispLabel.getTotalNumOfLabel() > 1 ){
				PrintOption reprintPrintOption = new PrintOption();
				reprintPrintOption.setPrintLang(printOption.getPrintLang());
				reprintPrintOption.setPrintType(printOption.getPrintType());
				reprintPrintOption.setPrintStatus(PrintStatus.Reprint);
				
				PrinterSelect reprintPrinterSelect = printerSelector.retrievePrinterSelect(PrintDocType.OtherLabel); 
				if( printOption.getPrintType() == PrintType.LargeFont ){
					reprintPrinterSelect.setReverse(true);
				}
				reprintPrinterSelect.setCopies( mpDispLabel.getTotalNumOfLabel()-1 );
				
				renderAndPrintJobList.add(new RenderAndPrintJob(labelType,
						reprintPrintOption,
						reprintPrinterSelect,
						parameters,
						Arrays.asList(mpDispLabel), 
						"[MpDispLabel] TransactionID:" + mpDispLabel.getDeliveryItemId() +
						" BatchNum:" + mpDispLabel.getBatchNum() +
						" ItemCode:" + mpDispLabel.getItemCode()));
			}
			
			if (StringUtils.isNotBlank(mpDispLabel.getPatOverrideReason()) || StringUtils.isNotBlank(mpDispLabel.getDdiOverrideReason())) {
				renderAndPrintJobList.add(new RenderAndPrintJob(MDS_OVERRIDE_REASON_LABEL,
						new PrintOption(mpDispLabel.getPrintOption().getPrintType(), PrintLang.Eng, mpDispLabel.getPrintOption().getPrintStatus()),
						printerSelect,
						parameters,
						Arrays.asList(mpDispLabel), 
						"[MdsOverrideReasonLabel] TransactionID:" + mpDispLabel.getDeliveryItemId() +
						" BatchNum:" + mpDispLabel.getBatchNum() +
						" ItemCode:" + mpDispLabel.getItemCode()));
			}
			
		}catch(Exception e){
			if( mpDispLabel != null ){
				throw new DueOrderPrintException(e, mpDispLabel.getWard(), mpDispLabel.getCaseNum());
			}else{
				throw new DueOrderPrintException(e, "", "");
			}
		}
	}
	
	private RenderAndPrintJob buildPivasWorksheetPrintJobList(PivasWorksheet pivasWorksheet, PrintOption printOption, PrinterSelect printerSelect) {
		try {
				Map<String, Object> param = pivasLabelBuilder.getPivasWorksheetParam(pivasWorksheet, workstore.getHospCode());
				
				if (printOption.getPrintType() == PrintType.LargeFont) {
					printerSelect.setReverse(true);
				}
				
				return new RenderAndPrintJob(PIVAS_WORKSHEET,
											printOption,
											printerSelect,
											param,
											Arrays.asList(pivasWorksheet), 
											"[PivasWorksheet] TransactionID:" + pivasWorksheet.getTrxId() +
											" BatchNum:" + pivasWorksheet.getBatchNum());

			} catch (Exception e) {
				if (pivasWorksheet != null) {
					throw new DueOrderPrintException(e, pivasWorksheet.getWard(), pivasWorksheet.getCaseNum());
				} else {
					throw new DueOrderPrintException(e, "", "");
				}
			}
	}
	
	private RenderAndPrintJob buildPivasProductLabelPrintJobList(PivasProductLabel pivasProductLabel, PrintOption printOption, PrinterSelect printerSelect) {
		try {
				Map<String, Object> param = pivasLabelBuilder.getPivasProductLabelParam(pivasProductLabel, workstore.getHospCode());
			
				if (printOption.getPrintStatus() == PrintStatus.Reprint) {
					pivasProductLabel.setReprintFlag(true);
				} else {
					pivasProductLabel.setReprintFlag(false);
				}
				
				printerSelect.setCopies(pivasProductLabel.getNumOfLabelSet().intValue());
				
				List<PivasProductLabel> pivasProductLabelList = new ArrayList<PivasProductLabel>();
				
				if (pivasProductLabel.getSplitCount() != null && Integer.parseInt(pivasProductLabel.getSplitCount()) > 1) {
					for( Integer currentPageNum = 1; currentPageNum <= Integer.parseInt(pivasProductLabel.getSplitCount()) ; currentPageNum++) {
						PivasProductLabel clonePivasProductLabel = (PivasProductLabel) BeanUtils.cloneBean(pivasProductLabel);					
						clonePivasProductLabel.setCurrentPageNum(currentPageNum.toString());
						pivasProductLabelList.add(clonePivasProductLabel);
					}
				} else {
					pivasProductLabelList.add(pivasProductLabel);
				}
				
				return new RenderAndPrintJob(PIVAS_PRODUCT_LABEL,
											printOption,
											printerSelect,
											param,
											pivasProductLabelList, 
											"[PivasProductLabel] TransactionID:" + pivasProductLabel.getTrxId() + 
											" BatchNum:" + pivasProductLabel.getBatchNum());
		} catch(Exception e){
			if (pivasProductLabel != null){
				throw new DueOrderPrintException(e, pivasProductLabel.getWard(), pivasProductLabel.getCaseNum());
			} else {
				throw new DueOrderPrintException(e, "", "");
			}
		}
	}

	private RenderAndPrintJob buildPivasOuterBagLabelPrintJobList(PivasOuterBagLabel pivasOuterBagLabel, PrintOption printOption, PrinterSelect printerSelect) {
		try {
			
			Map<String, Object> param = pivasLabelBuilder.getPivasOuterBagLabelParam(pivasOuterBagLabel, workstore.getHospCode());
			printerSelect.setCopies(pivasOuterBagLabel.getNumOfLabel().intValue());
			
			return new RenderAndPrintJob(PIVAS_OUTER_BAG_LABEL,
										printOption,
										printerSelect,
										param,
										Arrays.asList(pivasOuterBagLabel), 
										"[PivasOuterBagLabel] TransactionID:" + pivasOuterBagLabel.getTrxId() +
										" BatchNum:" + pivasOuterBagLabel.getBatchNum());
		} catch (Exception e) {
			if (pivasOuterBagLabel != null) {
				throw new DueOrderPrintException(e, pivasOuterBagLabel.getWard(), pivasOuterBagLabel.getCaseNum());
			} else {
				throw new DueOrderPrintException(e, "", "");
			}
		}
	}

	private RenderAndPrintJob buildMdsOverrideReasonLabelPrintJobList(MdsOverrideReasonLabel mdsOverrideReasonLabel, PrintOption printOption, PrinterSelect printerSelect){
		try {
				Map<String, Object> param = pivasLabelBuilder.getMdsOverrideReasonLabelParam(mdsOverrideReasonLabel, workstore.getHospCode());
				printOption.setPrintLang(PrintLang.Eng);
				
				return new RenderAndPrintJob(MDS_OVERRIDE_REASON_LABEL,
											printOption, 
											printerSelect,
											param,
											Arrays.asList(mdsOverrideReasonLabel), 
											"[MdsOverrideReasonLabel] BatchNum:" + mdsOverrideReasonLabel.getBatchNum());

			}catch(Exception e){
			if( mdsOverrideReasonLabel != null ){
				throw new DueOrderPrintException(e, mdsOverrideReasonLabel.getWard(), mdsOverrideReasonLabel.getCaseNum());
			}else{
				throw new DueOrderPrintException(e, "", "");
			}
		}
	}
	
	public MutableTriple<List<RenderAndPrintJob>, List<DispOrder>, List<Object>> printMpDispLabelList(Delivery delivery, List<Integer> atdpsPickStationsNumList) {					// Direct label print
		List<Delivery> deliveryList = new ArrayList<Delivery>();
		deliveryList.add(delivery);
		return genMpDispLabelPrintJobList(deliveryList, atdpsPickStationsNumList);
	}
	
	public MutableTriple<List<RenderAndPrintJob>, List<DispOrder>, List<Object>> printMpDispLabelList(List<Delivery> deliveryList, List<Integer> atdpsPickStationsNumList) {		// Due order label gen
		return genMpDispLabelPrintJobList(deliveryList, atdpsPickStationsNumList);
	}
	
	public MutableTriple<List<RenderAndPrintJob>, List<DispOrder>, List<Object>> printPivasLabelList(List<Delivery> deliveryList) {		// Due order label gen
		return genPivasLabelPrintJobList(deliveryList);
	}
	
	public void printMpDispLabel(List<RenderAndPrintJob> renderAndPrintJobList) {	// Label reprint 
		printAgent.renderAndPrint(renderAndPrintJobList);
	}
	
	public RenderAndPrintJob printDeliveryLabel(Delivery delivery, boolean isReprint) {
		JaxbWrapper<Object> dispLabelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);

		DeliveryLabel deliveryLabel = new DeliveryLabel();
		int batchDay = new DateTime(delivery.getBatchDate()).getDayOfMonth();
		deliveryLabel.setBatchNum( (( batchDay < 10 )?"0":"") + batchDay + delivery.getBatchNum() );
		deliveryLabel.setHospName( dispLabelBuilder.getHospitalNameEng(LABEL_DISPLABEL_HOSPITAL_NAMEENG.get()) );
		deliveryLabel.setWorkstoreCode(workstore.getWorkstoreCode());
		deliveryLabel.setPrintDate(new Date());
		
		
		Date deliveryDate = deliveryManager.calculateTargetDeliveryDate(delivery.getDeliveryItemList()); 
		deliveryLabel.setTargetedDeliveryTime(deliveryDate);
		
		Integer newCount = 0;
		Integer refillCount = 0;
		for (DeliveryItem deliveryItem:delivery.getDeliveryItemList()) {
			if( !DeliveryItemStatus.Printed_Assembled_Checked_Delivered.contains(deliveryItem.getStatus()) ){
				continue;
			}
			boolean labelUrgentFlag = false;
			boolean labelRefrigerateFlag = false;
			
			Object label = dispLabelJaxbWrapper.unmarshall(deliveryItem.getLabelXml());
			if (label instanceof MpDispLabel) {
				labelUrgentFlag = ((MpDispLabel) label).getUrgentFlag();
				labelRefrigerateFlag = ((MpDispLabel) label).getRefrigerateFlag();
			}
			
			deliveryLabel.setUrgentFlag(deliveryLabel.getUrgentFlag() || labelUrgentFlag);
			
			if((new Date()).after(deliveryDate)){
				deliveryLabel.setOverdueFlag(true);
			}else{
				deliveryLabel.setOverdueFlag(false);
			}
			
			deliveryLabel.setRefrigerateFlag(deliveryLabel.getRefrigerateFlag() || labelRefrigerateFlag);
			
			if (deliveryItem.getRefillFlag()) {
				refillCount++;
			} else {
				newCount++;
			}
		}	
		
		if (newCount>0) {
			deliveryLabel.setNewOrdersFlag(true);
			if (refillCount>0) {
				deliveryLabel.setRefillOrdersFlag(true);
			} else {
				deliveryLabel.setRefillOrdersFlag(false);
			}
		} else {
			deliveryLabel.setNewOrdersFlag(false);
			if (refillCount >0) {
				deliveryLabel.setRefillOrdersFlag(true);
			} else {
				deliveryLabel.setRefillOrdersFlag(false);
			}
		}
		
		deliveryLabel.setWardCode(delivery.getWardCode());
		String hospCode = workstore.getHospCode();

		deliveryLabel.setDeliveryId(deliveryLabel.getBatchNum() +
									hospCode +
									df.format(delivery.getBatchDate()) +
									delivery.getWardCode());
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("_PNG_ZOOM_RATIO", Float.valueOf(1.3f));
		
		PrintOption printOption = new PrintOption();
		if( isReprint ){
			printOption.setPrintStatus(PrintStatus.Reprint);
			deliveryLabel.setBatchDate(delivery.getBatchDate());
		}
		deliveryLabel.setPrintOption(printOption);
		
		return (new RenderAndPrintJob(
				"DeliveryLabel", 
				printOption, 
				printerSelector.retrievePrinterSelect(PrintDocType.OtherLabel), 
				parameters, 
				Arrays.asList(deliveryLabel), 
				"[DeliveryLabel] BatchNum :"+deliveryLabel.getBatchNum()));

	}
	
	public void printDividerLabel(Delivery delivery, boolean isReprint) {
		JaxbWrapper<MpDispLabel> dispLabelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);

		DividerLabel dividerLabel = new DividerLabel();
		int batchDay = new DateTime(delivery.getBatchDate()).getDayOfMonth();
		dividerLabel.setBatchNum( (( batchDay < 10 )?"0":"") + batchDay + delivery.getBatchNum() );
		dividerLabel.setHospName( dispLabelBuilder.getHospitalNameEng(LABEL_DISPLABEL_HOSPITAL_NAMEENG.get()) );
		dividerLabel.setPrintDate(new Date());
		dividerLabel.setWorkstoreCode(workstore.getWorkstoreCode());
		
		Integer numOfItems = 0;
		Integer numOfLabels = 0;
		for (DeliveryItem deliveryItem:delivery.getDeliveryItemList()) {
			MpDispLabel mpDispLabel = dispLabelJaxbWrapper.unmarshall(deliveryItem.getLabelXml());
			
			PrintOption printOption = new PrintOption();
			if( mpDispLabel.getShowInstructionFlag() ){
				printOption.setPrintLang( mpDispLabel.getPrintLang() );
				if( LABEL_MPDISPLABEL_LARGEFONT_ENABLED.get(Boolean.FALSE) ){
					printOption.setPrintType( PrintType.LargeFont );
				}else{
					printOption.setPrintType( PrintType.Normal );
				}
			}else{
				printOption.setPrintLang( PrintLang.Eng );
				printOption.setPrintType( PrintType.Normal );
			}
			
			mpDispLabel.setPrintOption(printOption);
			numOfItems++;
			numOfLabels += countNumOfLabels(mpDispLabel);
		}
		
		dividerLabel.setNumOfItems(numOfItems);
		dividerLabel.setNumOfLabels(numOfLabels);
				
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("_PNG_ZOOM_RATIO", Float.valueOf(1.3f));
		
		PrintOption printOption = new PrintOption();
		if( isReprint ){
			printOption.setPrintStatus(PrintStatus.Reprint);
			dividerLabel.setBatchDate(delivery.getBatchDate());
		}
		dividerLabel.setPrintOption(printOption);
		
		printAgent.renderAndPrint(new RenderAndPrintJob(
				"DividerLabel", 
				printOption, 
				printerSelector.retrievePrinterSelect(PrintDocType.OtherLabel), 
				parameters, 
				Arrays.asList(dividerLabel)));
		
	}
		
	private RenderAndPrintJob genDividerLabel(String batchNum, List<MpDispLabel> mpDispLabelList, PrinterSelect printerSelect) {
		DividerLabel dividerLabel = new DividerLabel();
		Integer numOfItems = 0;
		Integer numOfLabels = 0;
		for (MpDispLabel mpDispLabel:mpDispLabelList) {
			numOfItems++;
			numOfLabels += countNumOfLabels(mpDispLabel);
		}		
		dividerLabel.setBatchNum(batchNum);
		dividerLabel.setNumOfItems(numOfItems);
		dividerLabel.setNumOfLabels(numOfLabels);
		dividerLabel.setHospName( dispLabelBuilder.getHospitalNameEng(LABEL_DISPLABEL_HOSPITAL_NAMEENG.get()) );
		dividerLabel.setPrintDate(new Date());
		dividerLabel.setWorkstoreCode(workstore.getWorkstoreCode());
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("_PNG_ZOOM_RATIO", Float.valueOf(1.3f));
		
		return new RenderAndPrintJob("DividerLabel",
									new PrintOption(),
									printerSelect,
									parameters,
									Arrays.asList(dividerLabel));
	}
	
	private Integer countNumOfLabels(BaseLabel baseLabel) {
		Integer totalLine = 0;
		
		if (!((MpDispLabel) baseLabel).getShowInstructionFlag()){
			return 1;
		}
		
		totalLine = baseLabel.getWarningText().split("\n").length + baseLabel.getInstructionText().split("\n").length;
		
		if (LABEL_MPDISPLABEL_LARGEFONT_ENABLED.get(Boolean.TRUE)) {
			
			if (totalLine%4 >0){
				return (totalLine/4)+1;
			}else {
				return (totalLine/4);
			}
		}else {
			
			if (totalLine%6 >0){
				return (totalLine/6)+1;
			}else {
				return (totalLine/6);
			}
		}
	}
	
	public String retreiveDisplayDrugName(Map<String,String> fdnMappingMap, PharmOrderItem pharmOrderItem)
	{
		String fdn = null;
		if (fdnMappingMap != null) {
			fdn = fdnMappingMap.get(pharmOrderItem.getItemCode());
		}

		String displayDrugName = "";
		String dmDrugDrugName = null;
		if( dmDrugCacher.getDmDrug(pharmOrderItem.getItemCode()) != null ){
			dmDrugDrugName = dmDrugCacher.getDmDrug(pharmOrderItem.getItemCode()).getDrugName();
		}
		
		if( dmDrugDrugName != null && !dmDrugDrugName.equals( pharmOrderItem.getDrugName() ) ){
			displayDrugName = StringUtils.replace(StringUtils.replace(pharmOrderItem.getDrugName(),"(OR EQUIV)","")," "," ");
		}else{
			displayDrugName = fdn==null?StringUtils.replace(StringUtils.replace(pharmOrderItem.getDrugName(),"(OR EQUIV)","")," "," "):fdn;
		}
		
		return displayDrugName;
	}
	
	public void getMpDispLabel(Delivery delivery){
		for( DeliveryItem deliveryItem : delivery.getDeliveryItemList()){
			MpDispLabel mpDispLabel = new MpDispLabel();
			
			boolean isReplenishment = deliveryItem.getReplenishmentItem() != null;
			
			MedProfilePoItem medProfilePoItem = deliveryItem.resolveMedProfilePoItem();
			
			mpDispLabel.setWardStockFlag(medProfilePoItem.getWardStockFlag());
			mpDispLabel.setReDispFlag(medProfilePoItem.getReDispFlag());

			PharmOrderItem poiForInstruction = dispOrderConverter.convertToPharmOrderItemForInstructionList(medProfilePoItem, workstore);
			poiForInstruction.loadDmInfo();
    		DispOrderItem doiForInstruction = new DispOrderItem();
    		doiForInstruction.setLegacy(false);
    		doiForInstruction.setPharmOrderItem(poiForInstruction);
    		DispOrder dispOrder = new DispOrder();
    		dispOrder.setOrderType(OrderType.InPatient);
    		doiForInstruction.setDispOrder(dispOrder);
    		List<Instruction> instructionList = instructionBuilder.buildInstructionListForDispLabel(doiForInstruction);
    		
    		PrintOption printOption = new PrintOption();
			printOption.setPrintLang(PrintLang.Eng);
    		mpDispLabel.setInstructionList(instructionList);
    		mpDispLabel.setPrintOption(printOption);
			
			JaxbWrapper<MpDispLabel> dispLabelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
			deliveryItem.setLabelXml(dispLabelJaxbWrapper.marshall(mpDispLabel));
			
			if (!(medProfilePoItem.getActionStatus() == ActionStatus.ContinueWithOwnStock) &&
			                deliveryItem.getIssueQty().compareTo(BigDecimal.ZERO) > 0) {
			                if ( (!medProfilePoItem.getWardStockFlag() || isReplenishment ) &&
			                                                !medProfilePoItem.getDangerDrugFlag()){
			                	deliveryItem.setStatus(DeliveryItemStatus.Printed);
			                } else if (medProfilePoItem.getReDispFlag()) {
			                	deliveryItem.setStatus(DeliveryItemStatus.Printed);
			                }
			}
		}
	}
	
	private void sortMpDispLabel(List<MpDispLabel> mpDispLabelList, SortField[] sortBy) {
		Collections.sort(mpDispLabelList, new MpDispLabelComparator(sortBy));			
	}
	
	@SuppressWarnings("unchecked")
	private Pair<List<MpDispLabel>, List<Object>> convertMpDispLabel(List<DispOrder> dispOrderList, 
																					Map<String, WardConfig> wardConfigMap, 
																					Map<String, List<ItemLocation>> itemLocationMap, 
																					List<Integer> atdpsPickStationsNumList) throws DueOrderPrintException {
		JaxbWrapper<MpDispLabel> dispLabelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);

		List<MpDispLabel> tmpMpDispLabelList = new ArrayList<MpDispLabel>();
		List<Object> atdpsMessageObjList = new ArrayList<Object>();
		
		for (DispOrder dispOrder:dispOrderList) {
			dispOrder.getPharmOrder().loadDmInfo();
			for (DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList() ) {
				try{
					MpDispLabel mpDispLabel = dispLabelBuilder.convertToMpDispLabel(dispOrderItem);
					PrintOption printOption = new PrintOption();
					printOption.setPrintStatus(PrintStatus.Print);
					if (dispOrderItem.getPrintMpDispLabelInstructionFlag()) {
						if (LABEL_MPDISPLABEL_LARGEFONT_ENABLED.get(Boolean.FALSE)) {
							printOption.setPrintType(PrintType.LargeFont);
						} else {
							printOption.setPrintType(PrintType.Normal);
						}
						printOption.setPrintLang(dispOrder.getPrintLang());
					}else{
						printOption.setPrintLang(PrintLang.Eng);
						printOption.setPrintType(PrintType.Normal);	
					}
					mpDispLabel.setPrintOption(printOption);
					
					DeliveryItem deliveryItem = em.find(DeliveryItem.class, dispOrderItem.getDeliveryItemId());
					dispOrderItem.setBaseLabel(mpDispLabel);
					
					List<String> patOverrideReason = new ArrayList<String>();
					List<String> ddiOverrideReason = new ArrayList<String>();
					if (dispOrderItem.getPharmOrderItem().getMedOrderItem().isManualItem()) {
						if (!dispOrderItem.getPharmOrderItem().getMedOrderItem().getPatAlertList().isEmpty()) {
							patOverrideReason.add(dispOrderItem.getPharmOrderItem().getMedOrderItem().getPatAlertList().get(0).getOverrideReasonDesc());
						}
						for (MedOrderItemAlert moiAlert : dispOrderItem.getPharmOrderItem().getMedOrderItem().getDdiAlertList()) {
							ddiOverrideReason.add(moiAlert.getOverrideReasonDesc());
						}
					}
					mpDispLabel.setPatOverrideReason(StringUtils.replaceChars(StringUtils.join(patOverrideReason.toArray(), ",\n"), '\r', '\n'));
					mpDispLabel.setDdiOverrideReason(StringUtils.replaceChars(StringUtils.join(ddiOverrideReason.toArray(), ",\n"), '\r', '\n'));
					
					if ( dispOrderItem.getStatus() == DispOrderItemStatus.Issued ) {
						ItemLocation itemLocation = null;
						if ( dispOrderItem.getDispQty().compareTo(BigDecimal.ZERO) > 0 ) {
							itemLocation = this.getAtdpsItemLocation(deliveryItem, wardConfigMap, itemLocationMap, atdpsPickStationsNumList);
						}
						if ( itemLocation != null ) {
							deliveryItem.setBinNum(itemLocation.getBinNum());
							setMpDispLabelAtdpsInfo(mpDispLabel, 
															dispOrderItem, 
															deliveryItem, 
															itemLocation.getMachine(), 
															false);
							
							if ( "UTF-8".equals(MACHINE_ATDPS_MESSAGE_ENCODING.get()) ) {
								atdpsMessageObjList.addAll(mpDispLabel.getAtdpsXmlList());
							} else {
								atdpsMessageObjList.addAll(mpDispLabel.getAtdpsInfoEngList());
							}
						} else {
							deliveryItem.setBinNum(mpDispLabel.getBinNum());
							tmpMpDispLabelList.add(mpDispLabel);
						}
						deliveryItem.setStatus(DeliveryItemStatus.Printed);
					} else {
						deliveryItem.setStatus(DeliveryItemStatus.KeepRecord);
					}
					
					mpDispLabel.setItemNum(dispOrderItem.getPharmOrderItem().getMedOrderItem().getItemNum());
					String labelXml = dispLabelJaxbWrapper.marshall(mpDispLabel);
					deliveryItem.setLabelXml(labelXml);
					dispOrderItem.setLabelXml(labelXml);
				}catch(Exception e){
					if( dispOrderItem != null && 
								dispOrderItem.getPharmOrderItem() != null && 
								dispOrderItem.getPharmOrderItem().getPharmOrder() != null ){
						if( dispOrderItem.getPharmOrderItem().getPharmOrder().getMedCase() != null ){
							throw new DueOrderPrintException(e, dispOrderItem.getPharmOrderItem().getPharmOrder().getWardCode(), dispOrderItem.getPharmOrderItem().getPharmOrder().getMedCase().getCaseNum());
						}else{
							throw new DueOrderPrintException(e, dispOrderItem.getPharmOrderItem().getPharmOrder().getWardCode(), "");
						}
					}else{
						throw new DueOrderPrintException(e, "", "");
					}
				}
			}

		}
		
		return new Pair(tmpMpDispLabelList, atdpsMessageObjList);
	}
	
	private List<LabelContainer> convertPivasLabel(List<DispOrder> dispOrderList) throws DueOrderPrintException {
		JaxbWrapper<LabelContainer> labelContainerJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
		List<LabelContainer> tmpPivasLabelList = new ArrayList<LabelContainer>();
		
		for (DispOrder dispOrder:dispOrderList) {
			dispOrder.getPharmOrder().loadDmInfo();
			
			// minimise the number of times of DMS calling
			Map<Long, DeliveryItem> deliveryItemByIdMap = new HashMap<Long, DeliveryItem>();
			Set <Long> pivasFormulaMethodIdSet = new HashSet<Long>();
			Set <Long> pivasContainerIdSet = new HashSet<Long>();
			
			for (DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList() ) {
				DeliveryItem deliveryItem = em.find(DeliveryItem.class, dispOrderItem.getDeliveryItemId());
				deliveryItemByIdMap.put(dispOrderItem.getDeliveryItemId(), deliveryItem);
				
				pivasFormulaMethodIdSet.add(deliveryItem.getPivasWorklist().getPivasPrepMethod().getPivasFormulaMethodId());
				pivasContainerIdSet.add(deliveryItem.getPivasWorklist().getPivasPrep().getPivasContainerId());
			}
			
			List <Long> pivasFormulaMethodIdList = new ArrayList<Long>();
			List<Long> pivasContainerIdList = new ArrayList<Long>();
			
			pivasFormulaMethodIdList.addAll(pivasFormulaMethodIdSet);
			pivasContainerIdList.addAll(pivasContainerIdSet);
			
			List <PivasFormulaMethod> pivasFormulaMethodList = dmsPmsServiceProxy.retrievePivasFormulaMethodListByPivasFormulaMethodId(workstore.getHospCode(), pivasFormulaMethodIdList, true);
			List <PivasContainer> pivasContainerList = dmsPmsServiceProxy.retrievePivasContainerListByPivasContainerId(workstore.getWorkstoreGroup().getWorkstoreGroupCode(), pivasContainerIdList);
			
			Map<Long, PivasFormulaMethod> pivasFormulaMethodByIdMap = new HashMap<Long, PivasFormulaMethod>();
			Map<Long, PivasContainer> pivasContainerByIdMap = new HashMap<Long, PivasContainer>();
			
			for (PivasFormulaMethod pivasFormulaMethod : pivasFormulaMethodList ) {
				pivasFormulaMethodByIdMap.put(pivasFormulaMethod.getId(), pivasFormulaMethod);
			}
			
			for (PivasContainer pivasContainer : pivasContainerList) {
				pivasContainerByIdMap.put(pivasContainer.getId(), pivasContainer);
			}
			
			for (DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList() ) {
				try{
					DeliveryItem deliveryItem = deliveryItemByIdMap.get(dispOrderItem.getDeliveryItemId());
					PivasFormulaMethod pivasFormulaMethod = pivasFormulaMethodByIdMap.get(deliveryItem.getPivasWorklist().getPivasPrepMethod().getPivasFormulaMethodId());
					PivasContainer pivasContainer = pivasContainerByIdMap.get(deliveryItem.getPivasWorklist().getPivasPrep().getPivasContainerId());
					
					PivasWorksheet pivasWorksheet = pivasLabelBuilder.convertToPivasWorksheet(dispOrderItem, deliveryItem, pivasFormulaMethod, pivasContainer);
					PivasProductLabel pivasProductLabel = pivasLabelBuilder.convertToPivasProductLabel(dispOrderItem, deliveryItem, pivasFormulaMethod, pivasContainer);
					PivasOuterBagLabel pivasOuterBagLabel = null;
										
					if (deliveryItem.getPivasWorklist().getPivasPrep().getSplitCount() != null && deliveryItem.getPivasWorklist().getPivasPrep().getSplitCount() > 1) {
						pivasOuterBagLabel = pivasLabelBuilder.convertToPivasOuterBagLabel(dispOrderItem, deliveryItem, pivasFormulaMethod, pivasContainer);
					}
					
					MdsOverrideReasonLabel mdsOverrideReasonLabel = null;
					if (dispOrderItem.getPharmOrderItem().getMedOrderItem().isManualItem()) {
						mdsOverrideReasonLabel = pivasLabelBuilder.convertToMdsOverrideReasonLabel(dispOrderItem, deliveryItem, pivasFormulaMethod);
					}

					LabelContainer pivasLabel = new LabelContainer();
					pivasLabel.setPivasProductLabel(pivasProductLabel);
					pivasLabel.setPivasWorksheet(pivasWorksheet);
					
					if (pivasOuterBagLabel != null) {
						pivasLabel.setPivasOuterBagLabel(pivasOuterBagLabel);
					}
					
					if (mdsOverrideReasonLabel != null) {
						pivasLabel.setMdsOverrideReasonLabel(mdsOverrideReasonLabel);
					}

					tmpPivasLabelList.add(pivasLabel);
					deliveryItem.setStatus(DeliveryItemStatus.Printed);
					
					String labelXml = labelContainerJaxbWrapper.marshall(pivasLabel);
					deliveryItem.setLabelXml(labelXml);
				}catch(Exception e){
					if( dispOrderItem != null && 
								dispOrderItem.getPharmOrderItem() != null && 
								dispOrderItem.getPharmOrderItem().getPharmOrder() != null ){
						if( dispOrderItem.getPharmOrderItem().getPharmOrder().getMedCase() != null ){
							throw new DueOrderPrintException(e, dispOrderItem.getPharmOrderItem().getPharmOrder().getWardCode(), dispOrderItem.getPharmOrderItem().getPharmOrder().getMedCase().getCaseNum());
						}else{
							throw new DueOrderPrintException(e, dispOrderItem.getPharmOrderItem().getPharmOrder().getWardCode(), "");
						}
					}else{
						throw new DueOrderPrintException(e, "", "");
					}
				}
			}
		}
		
		return tmpPivasLabelList;
	}
		
	private void setMpDispLabelAtdpsInfo(MpDispLabel mpDispLabel, DispOrderItem dispOrderItem, 
																		DeliveryItem deliveryItem, 
																		Machine machine,  
																		boolean isReprint){
		
		if ( mpDispLabel.getAtdpsInfoChiList() == null ) {
			mpDispLabel.setAtdpsInfoChiList(new ArrayList<AtdpsMessageContent>());
			mpDispLabel.setAtdpsInfoEngList(new ArrayList<AtdpsMessageContent>());
			mpDispLabel.setAtdpsXmlList(new ArrayList<AtdpsMessage>());
		}
		
		mpDispLabel.getAtdpsInfoEngList().addAll(convertAtdpsMessageContent(dispOrderItem, 
																	deliveryItem, 
																	machine, 
																	PrintLang.Eng, 
																	false));
		mpDispLabel.getAtdpsInfoChiList().addAll(convertAtdpsMessageContent(dispOrderItem, 
																	deliveryItem, 
																	machine, 
																	PrintLang.Chi, 
																	false));
		
		mpDispLabel.getAtdpsXmlList().addAll(convertAtdpsMessage(dispOrderItem, deliveryItem, machine, PrintLang.Eng, isReprint));
	}
	
	private ItemLocation getAtdpsItemLocation(DeliveryItem deliveryItem, 
										Map<String, WardConfig> wardConfigMap, 
										Map<String, List<ItemLocation>> itemLocationMap, 
										List<Integer> atdpsPickStationsNumList){
		
		boolean isReplenishment = deliveryItem.getReplenishmentItem() != null;
		if ( isReplenishment ) {
			return null;
		}
		
		MedProfilePoItem medProfilePoItem = deliveryItem.resolveMedProfilePoItem();
		MedProfileMoItem medProfileMoItem = medProfilePoItem.getMedProfileMoItem();
		MedProfile medProfile = medProfileMoItem.getMedProfileItem().getMedProfile();
		List<ItemLocation> itemLocationList = itemLocationMap.get(medProfilePoItem.getItemCode());
		
		if ( medProfile.getAtdpsWardFlag() == null ) {
			WardConfig wardConfig = wardConfigMap.get(medProfile.getWardCode());
			if ( wardConfig != null ) {
				boolean isAtdpsWard = (wardConfig.getMpWardFlag() && wardConfig.getAtdpsWardFlag());
				medProfile.setAtdpsWardFlag(isAtdpsWard);
			} else {
				medProfile.setAtdpsWardFlag(false);
			}
		}
		boolean isValidAtdpsItem = checkAtdpsManager.isValidAtdpsItem(medProfileMoItem, medProfilePoItem, atdpsPickStationsNumList, true, false);
		
		if ( !medProfilePoItem.getUnitDoseFlag() || !isValidAtdpsItem || !medProfile.getAtdpsWardFlag() || (itemLocationList == null || itemLocationList.isEmpty())) {
			return null;
		}
		
		if ( !MACHINE_ATDPS_NEWITEM_ENABLED.get(Boolean.FALSE) && !deliveryItem.getRefillFlag()) {
			return null;
		}
		
		if ( !isReplenishment && (BigDecimal.ZERO.compareTo(medProfilePoItem.getCalQty()) == 0 || BigDecimal.ZERO.compareTo(deliveryItem.getPreIssueQty().remainder(medProfilePoItem.getCalQty())) != 0)) {
			return null;
		}

		if ( BigDecimal.ONE.compareTo(deliveryItem.getIssueDuration()) < 0  && BigDecimal.ZERO.compareTo(deliveryItem.getAdjQty()) > 0 ) {
			return null;
		}
		
		return itemLocationList.get(0);
	}
	
	private List<AtdpsMessageContent> convertAtdpsMessageContent(DispOrderItem dispOrderItem, 
																	DeliveryItem deliveryItem, 
																	Machine machine, 
																	PrintLang printLang, 
																	boolean isReprint)
	{
		List<AtdpsMessageContent> atdpsMessageContentList = new ArrayList<AtdpsMessageContent>();
		MedProfilePoItem medProfilePoItem = deliveryItem.resolveMedProfilePoItem();
		Delivery delivery = deliveryItem.getDelivery();
		PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
		PharmOrder pharmOrder = pharmOrderItem.getPharmOrder();
		
		AtdpsMessageContent atdpsMessageContent = new AtdpsMessageContent();
		atdpsMessageContentList.add(atdpsMessageContent);
		atdpsMessageContent.setTrxId(deliveryItem.getId());
		atdpsMessageContent.setHostname(machine.getHostName());
		atdpsMessageContent.setPickStationNum(machine.getPickStation().getPickStationNum());
		atdpsMessageContent.setPortNum(MACHINE_ATDPS_PORT.get());
		atdpsMessageContent.setHospCode(workstore.getHospCode());
		if ( printLang == PrintLang.Eng ) {
			atdpsMessageContent.setHospName(StringUtils.trimToEmpty(LABEL_DISPLABEL_HOSPITAL_NAMEENG.get()));
		} else {
			atdpsMessageContent.setHospName(StringUtils.trimToEmpty(LABEL_DISPLABEL_HOSPITAL_NAMECHI.get()));
		}
		
		//Patient Chinese name should be if this information can be found. Otherwise, patient English name would be used.
		if ( StringUtils.isNotBlank(pharmOrder.getNameChi())) {
			atdpsMessageContent.setPatName(StringUtils.trimToEmpty(pharmOrder.getNameChi()));
			atdpsMessageContent.setCcCode(pharmOrder.getCcCode());
		} else {
			atdpsMessageContent.setPatName(StringUtils.trimToEmpty(pharmOrder.getName()));
		}
		
		atdpsMessageContent.setWorkstoreCode(workstore.getWorkstoreCode());
		atdpsMessageContent.setWorkstationCode(workstation.getWorkstationCode());
		atdpsMessageContent.setLangType(getLangType(printLang, pharmOrder.getCcCode())); 
		atdpsMessageContent.setItemNum(dispOrderItem.getItemNum().toString());
		atdpsMessageContent.setBatchNum(StringUtils.trimToEmpty(delivery.getBatchNum()));
		
		atdpsMessageContent.setItemCode(pharmOrderItem.getItemCode());
		atdpsMessageContent.setTradeName(StringUtils.trimToEmpty(pharmOrderItem.getSynonym()));

		atdpsMessageContent.setFormCode(pharmOrderItem.getFormCode());
		atdpsMessageContent.setDispDate(df.format(dispOrderItem.getDispOrder().getDispDate()));
		atdpsMessageContent.setHkid(pharmOrder.getHkid());
		atdpsMessageContent.setWard(pharmOrder.getWardCode());
		atdpsMessageContent.setSpecCode(pharmOrder.getSpecCode());
		
		if ( pharmOrder.getMedCase() != null ) {
			atdpsMessageContent.setCaseNum(StringUtils.trimToEmpty(pharmOrder.getMedCase().getCaseNum()));
			atdpsMessageContent.setPasBedNum(StringUtils.trimToEmpty(pharmOrder.getMedCase().getPasBedNum()));
		} else {
			atdpsMessageContent.setCaseNum(StringUtils.EMPTY);
			atdpsMessageContent.setPasBedNum(StringUtils.EMPTY);
		}
		atdpsMessageContent.setRefillFlag(dispOrderItem.getDispOrder().getRefillFlag()?"Y":"N");
		atdpsMessageContent.setPatCatCode(StringUtils.trimToEmpty(pharmOrder.getPatCatCode()));
		atdpsMessageContent.setDoctorCode(StringUtils.trimToEmpty(pharmOrder.getDoctorCode()));
		
		atdpsMessageContent.setDispQty(dispOrderItem.getDispQty().toString());
		atdpsMessageContent.setModBaseunit(pharmOrderItem.getBaseUnit());
		atdpsMessageContent.setFmInficator(medProfilePoItem.getMedProfileMoItem().getFmFlag()?"Y":"N");
	    
		atdpsMessageContent.setFdnFlag(getFdnFlag(OrderType.InPatient, pharmOrderItem.getItemCode())?"Y":"N");
		atdpsMessageContent.setItemLocationBinNum(StringUtils.trimToEmpty(dispOrderItem.getBinNum()));
		atdpsMessageContent.setPrintInstruct("N");
		atdpsMessageContent.setShowInstructionFlag(false);
		atdpsMessageContent.setPrintBarcode("N");
		atdpsMessageContent.setPrintWardBarcode(LABEL_MPDISPLABEL_WARDRTNBARCODE_VISIBLE.get()?"Y":"N");
		atdpsMessageContent.setUpdateDate(df.format(deliveryItem.getUpdateDate()));
		atdpsMessageContent.setUpdateTime(timeDf.format(deliveryItem.getUpdateDate()));
		atdpsMessageContent.setAdjQty((deliveryItem.getAdjQty()!= null)?String.valueOf(deliveryItem.getAdjQty().intValue()):"0");
		
		//new item / single item
		atdpsMessageContent.setMsgCode(StringUtils.EMPTY);
		atdpsMessageContent.setLabelIndicator(StringUtils.EMPTY);
		if ( dispOrderItem.getSingleDispFlag() && !dispOrderItem.getDispOrder().getRefillFlag()) {
			atdpsMessageContent.setMsgCode("@@&&");
			atdpsMessageContent.setLabelIndicator("SN");
		} else if ( dispOrderItem.getSingleDispFlag() ) {
			atdpsMessageContent.setMsgCode("&&");
			atdpsMessageContent.setLabelIndicator("S");
		} else if ( !dispOrderItem.getDispOrder().getRefillFlag() ) {
			atdpsMessageContent.setMsgCode("@@");
			atdpsMessageContent.setLabelIndicator("N");
		}
	
		//override remark and reason
		List<MedProfileMoItemAlert> patAlertList = medProfilePoItem.getMedProfileMoItem().getPatAlertList(false);
		StringBuilder overrideRemarkSb = new StringBuilder();
		StringBuilder overrideReasonSb = new StringBuilder();
		if ( !patAlertList.isEmpty() ) {
			List<String> overrideReasonList = patAlertList.get(0).getOverrideReason();
			if ( !overrideReasonList.isEmpty() ) {
				for ( String str : overrideReasonList ) {
					if (str.charAt(str.length()-1) == ASTERISK) {
						if ( overrideRemarkSb.length() > 0 ) {
							overrideRemarkSb.append("|");
						}
						overrideRemarkSb.append(str.substring(0, str.length()-1));
					} else {
						if ( overrideReasonSb.length() > 0 ) {
							overrideReasonSb.append("|");
						}
						overrideReasonSb.append(str);
					}
				}
			}
		}
		atdpsMessageContent.setOverrideRemark(overrideRemarkSb.toString());
		atdpsMessageContent.setOverrideReason(overrideReasonSb.toString());
		
		MpDispLabel mpDispLabel = (MpDispLabel)dispOrderItem.getBaseLabel();
		atdpsMessageContent.setDrugName(mpDispLabel.getItemDesc());
		atdpsMessageContent.setMp2dBarCodeData(mpDispLabel.getQrCodeXml());
		atdpsMessageContent.setUrgentFlag(dispOrderItem.getUrgentFlag()?"Y":"N");
		atdpsMessageContent.setUserCode(dispLabelBuilder.getMaskedUserCode(mpDispLabel.getUserCode()));
		
		setAtdpsContentWarnDesc(mpDispLabel.getWarningList(), printLang, atdpsMessageContent);
		
		DateFormat batchCodeDf = new SimpleDateFormat("dd"); 
		atdpsMessageContent.setBatchCode(batchCodeDf.format(delivery.getBatchDate())+delivery.getBatchNum());
		atdpsMessageContent.setVerifierCode(medProfilePoItem.getMedProfileMoItem().getMedProfileItem().getVerifyUser());
		
		if ( !isReprint ) {
			atdpsMessageContent.setPrintType(AtdpsPrintType.New.getDataValue());
			atdpsMessageContent.setReprintFlag("N");
		} else {
			atdpsMessageContent.setPrintType(AtdpsPrintType.Reprint.getDataValue());
			atdpsMessageContent.setReprintFlag("Y");
		}
		
		Regimen regimen = pharmOrderItem.getRegimen();
		if ( regimen.getDoseGroupList().size() > 1 ){
			atdpsMessageContent.setPrescNature(AtdpsPrescNature.StepUpDown.getDataValue());
		} else if ( regimen.getDoseGroupList().get(0).getDoseList().size() > 1 ) {
			atdpsMessageContent.setPrescNature(AtdpsPrescNature.MultipleDose.getDataValue());
		} else {
			atdpsMessageContent.setPrescNature(AtdpsPrescNature.Normal.getDataValue());
		}
		
		atdpsMessageContent.setDuration(StringUtils.EMPTY);
		atdpsMessageContent.setDurationUnit(StringUtils.EMPTY);
		
		if ( deliveryItem.getIssueDuration() != null && deliveryItem.getIssueDuration().compareTo(BigDecimal.ZERO) != 0 ) {
			Integer refillDurationInDay = getDurationUnitInDays(medProfilePoItem.getRefillDurationUnit());
			BigDecimal duration = deliveryItem.getIssueDuration().divide(BigDecimal.valueOf(refillDurationInDay), RoundingMode.UP);
			atdpsMessageContent.setDuration(duration.toString());
			atdpsMessageContent.setDurationUnit(medProfilePoItem.getRefillDurationUnit().getDataValue());
		}
		
		List<String> instructionList = instructionBuilder.buildInstructionForAtdps(dispOrderItem, printLang);
		instructionBuilder.buildEngMealTimeDescForAtdps(dispOrderItem.getDispOrder());
		
		atdpsMessageContent.setInstruction(StringUtils.EMPTY);
		atdpsMessageContent.setDosage(StringUtils.EMPTY);
		atdpsMessageContent.setFractionalDosageFlag("N");
		atdpsMessageContent.setPrnFlag(StringUtils.EMPTY);
		atdpsMessageContent.setFreqCode(StringUtils.EMPTY);
		atdpsMessageContent.setSupplFreqCode(StringUtils.EMPTY);
		atdpsMessageContent.setMealTimeInfo(StringUtils.EMPTY);
		int doseCount = 1;
		DoseGroup doseGroup = regimen.getDoseGroupList().get(0);
		for ( Dose dose : doseGroup.getDoseList() ) {
			if ( doseCount > 1 ) {
				AtdpsMessageContent newAtdpsMessageContent = copyAtdpsMessageContent(atdpsMessageContent);
				atdpsMessageContentList.add(newAtdpsMessageContent);
				atdpsMessageContent = newAtdpsMessageContent;
			}
			if ( doseCount <= instructionList.size() ) {
				atdpsMessageContent.setInstruction(instructionList.get(doseCount-1));
			}
			atdpsMessageContent.setMealTimeInfo(StringUtils.trimToEmpty(dose.getMealTimeInfo()));
			
			if (dose.getDosageEnd() != null && dose.getDosageEnd().doubleValue() > 0.0) {
				atdpsMessageContent.setDosage(dose.getDosageEnd().toString());
				atdpsMessageContent.setFractionalDosageFlag(isNotInteger(dose.getDosageEnd())?"Y":"N");
			} else if ( dose.getDosage() != null ) {
				atdpsMessageContent.setDosage(dose.getDosage().toString());
				atdpsMessageContent.setFractionalDosageFlag(isNotInteger(dose.getDosage())?"Y":"N");
			}
			
			atdpsMessageContent.setPrnFlag(dose.getPrn()?"Y":"N");
			if ( dose.getDailyFreq() != null ) {
				atdpsMessageContent.setFreqCode(StringUtils.trimToEmpty(dose.getDailyFreq().getCode()));
			}
			if ( dose.getSupplFreq() != null ) {
				atdpsMessageContent.setSupplFreqCode(StringUtils.trimToEmpty(dose.getSupplFreq().getCode()));
			}
			doseCount++;
		}
		return atdpsMessageContentList;
	}
	
	private List<AtdpsMessage> convertAtdpsMessage(DispOrderItem dispOrderItem, 
																	DeliveryItem deliveryItem, 
																	Machine machine, 
																	PrintLang printLang, 
																	boolean isReprint) {
		
		List<AtdpsMessage> atdpsMessageList = new ArrayList<AtdpsMessage>();
		MedProfilePoItem medProfilePoItem = deliveryItem.resolveMedProfilePoItem();
		Delivery delivery = deliveryItem.getDelivery();
		PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
		PharmOrder pharmOrder = pharmOrderItem.getPharmOrder();
		
		AtdpsMessage atdpsMessage = new AtdpsMessage();
		atdpsMessageList.add(atdpsMessage);
		atdpsMessage.setTrxId(deliveryItem.getId());
		atdpsMessage.setHostname(machine.getHostName());
		atdpsMessage.setPickStationNum(machine.getPickStation().getPickStationNum());
		atdpsMessage.setPortNum(MACHINE_ATDPS_PORT.get());
		atdpsMessage.setHospCode(workstore.getHospCode());
		if ( printLang == PrintLang.Eng ) {
			atdpsMessage.setHospName(StringUtils.trimToEmpty(LABEL_DISPLABEL_HOSPITAL_NAMEENG.get()));
		} else {
			atdpsMessage.setHospName(StringUtils.trimToEmpty(LABEL_DISPLABEL_HOSPITAL_NAMECHI.get()));
		}
		
		//Patient Chinese name should be if this information can be found. Otherwise, patient English name would be used.
		if ( StringUtils.isNotBlank(pharmOrder.getNameChi())) {
			atdpsMessage.setPatName(StringUtils.trimToEmpty(pharmOrder.getNameChi()));
		} else {
			atdpsMessage.setPatName(StringUtils.trimToEmpty(pharmOrder.getName()));
		}
		
		atdpsMessage.setWorkstoreCode(workstore.getWorkstoreCode());
		atdpsMessage.setWorkstationCode(workstation.getWorkstationCode());
		atdpsMessage.setPrintLang(printLang.getShortFormValue());
		atdpsMessage.setItemNum(dispOrderItem.getItemNum().toString());
		
		atdpsMessage.setItemCode(pharmOrderItem.getItemCode());
		atdpsMessage.setTradeName(StringUtils.trimToEmpty(pharmOrderItem.getSynonym()));

		atdpsMessage.setFormCode(pharmOrderItem.getFormCode());
		atdpsMessage.setDispDate(df.format(dispOrderItem.getDispOrder().getDispDate()));
		atdpsMessage.setHkid(pharmOrder.getHkid());
		atdpsMessage.setWardCode(pharmOrder.getWardCode());
		atdpsMessage.setSpecCode(pharmOrder.getSpecCode());
		
		if ( pharmOrder.getMedCase() != null ) {
			atdpsMessage.setCaseNum(StringUtils.trimToEmpty(pharmOrder.getMedCase().getCaseNum()));
			atdpsMessage.setBedNum(StringUtils.trimToEmpty(pharmOrder.getMedCase().getPasBedNum()));
			atdpsMessage.setPasPayCode(StringUtils.trimToEmpty(pharmOrder.getMedCase().getPasPayCode()));
		} else {
			atdpsMessage.setCaseNum(StringUtils.EMPTY);
			atdpsMessage.setBedNum(StringUtils.EMPTY);
		}
		atdpsMessage.setRefillFlag(dispOrderItem.getDispOrder().getRefillFlag());
		
		atdpsMessage.setIssueQty(dispOrderItem.getDispQty().toString());
		atdpsMessage.setBaseunit(pharmOrderItem.getBaseUnit());
		atdpsMessage.setFmFlag(medProfilePoItem.getMedProfileMoItem().getFmFlag());
	    
		atdpsMessage.setFdnFlag(getFdnFlag(OrderType.InPatient, pharmOrderItem.getItemCode()));
		atdpsMessage.setBinNum(StringUtils.trimToEmpty(dispOrderItem.getBinNum()));
		atdpsMessage.setPrintInstructFlag(false);
		atdpsMessage.setPrintWardReturnFlag(LABEL_MPDISPLABEL_WARDRTNBARCODE_VISIBLE.get());
		atdpsMessage.setUpdateDate(datetimeDf.format(deliveryItem.getUpdateDate()));
		atdpsMessage.setAdjQty((deliveryItem.getAdjQty()!= null)?String.valueOf(deliveryItem.getAdjQty().intValue()):"0");
		
		//new item / single item
		atdpsMessage.setLabelIndicator(StringUtils.EMPTY);
		if ( dispOrderItem.getSingleDispFlag() && !dispOrderItem.getDispOrder().getRefillFlag()) {
			atdpsMessage.setLabelIndicator("SN");
		} else if ( dispOrderItem.getSingleDispFlag() ) {
			atdpsMessage.setLabelIndicator("S");
		} else if ( !dispOrderItem.getDispOrder().getRefillFlag() ) {
			atdpsMessage.setLabelIndicator("N");
		}
	
		//override remark and reason
		atdpsMessage.setPrintOverrideIndicatorFlag(medProfilePoItem.getMedProfileMoItem().getAlertFlag());
		
		MpDispLabel mpDispLabel = (MpDispLabel)dispOrderItem.getBaseLabel();
		atdpsMessage.setItemDesc(mpDispLabel.getItemDesc());
		atdpsMessage.setMpDispLabelBarcodeInfo(mpDispLabel.getQrCodeXml());
		atdpsMessage.setUrgentFlag(dispOrderItem.getUrgentFlag());
		atdpsMessage.setUserCode(dispLabelBuilder.getMaskedUserCode(mpDispLabel.getUserCode()));
		
		setAtdpsContentWarnDesc(mpDispLabel.getWarningList(), printLang, atdpsMessage);
		
		DateFormat batchCodeDf = new SimpleDateFormat("dd"); 
		atdpsMessage.setBatchCode(batchCodeDf.format(delivery.getBatchDate())+delivery.getBatchNum());
		atdpsMessage.setVerifyUser(medProfilePoItem.getMedProfileMoItem().getMedProfileItem().getVerifyUser());
		
		atdpsMessage.setReprintFlag(isReprint);
		
		Regimen regimen = pharmOrderItem.getRegimen();
		if ( regimen.getDoseGroupList().get(0).getDoseList().size() > 1 ) {
			atdpsMessage.setMultipleDoseFlag(true);
		} else {
			atdpsMessage.setMultipleDoseFlag(false);
		}
		
		atdpsMessage.setIssueDuration(StringUtils.EMPTY);
		if ( deliveryItem.getIssueDuration() != null && deliveryItem.getIssueDuration().compareTo(BigDecimal.ZERO) != 0 ) {
			Integer refillDurationInDay = getDurationUnitInDays(medProfilePoItem.getRefillDurationUnit());
			BigDecimal duration = deliveryItem.getIssueDuration().divide(BigDecimal.valueOf(refillDurationInDay), RoundingMode.UP);
			atdpsMessage.setIssueDuration(duration.toString());
		}
		atdpsMessage.setDurationUnit(medProfilePoItem.getRefillDurationUnit().getDataValue());
		
		atdpsMessage.setActionStatus(pharmOrderItem.getActionStatus().getDataValue());
			
		atdpsMessage.setInstruction(StringUtils.EMPTY);
		atdpsMessage.setDosage(StringUtils.EMPTY);
		atdpsMessage.setFractionDoseFlag(false);
		atdpsMessage.setPrnFlag(false);
		atdpsMessage.setFreqCode(StringUtils.EMPTY);
		atdpsMessage.setSupplFreqCode(StringUtils.EMPTY);
		atdpsMessage.setMealTimeInfo(StringUtils.EMPTY);
		int doseCount = 1;
		DoseGroup doseGroup = regimen.getDoseGroupList().get(0);
		for ( Dose dose : doseGroup.getDoseList() ) {
			if ( doseCount > 1 ) {
				AtdpsMessage newAtdpsMessage = copyAtdpsMessageContent(atdpsMessage);
				atdpsMessageList.add(newAtdpsMessage);
				atdpsMessage = newAtdpsMessage;
			}

			atdpsMessage.setMealTimeInfo(StringUtils.trimToEmpty(dose.getMealTimeInfo()));
			
			if (dose.getDosageEnd() != null && dose.getDosageEnd().doubleValue() > 0.0) {
				atdpsMessage.setDosage(dose.getDosageEnd().toString());
				atdpsMessage.setFractionDoseFlag(isNotInteger(dose.getDosageEnd()));
			} else if ( dose.getDosage() != null ) {
				atdpsMessage.setDosage(dose.getDosage().toString());
				atdpsMessage.setFractionDoseFlag(isNotInteger(dose.getDosage()));
			}
			
			atdpsMessage.setPrnFlag(dose.getPrn());
			if ( dose.getDailyFreq() != null ) {
				atdpsMessage.setFreqCode(StringUtils.trimToEmpty(dose.getDailyFreq().getCode()));
			}
			if ( dose.getSupplFreq() != null ) {
				atdpsMessage.setSupplFreqCode(StringUtils.trimToEmpty(dose.getSupplFreq().getCode()));
			}
			doseCount++;
		}
		
		
		return atdpsMessageList;
	}
	
	private boolean isNotInteger(BigDecimal dosage){
		Double decimalValue =  dosage.doubleValue() - dosage.intValue();
		if ( decimalValue == 0 ) {
			return false;
		}
		return true;
	}
	
	private AtdpsMessageContent copyAtdpsMessageContent(AtdpsMessageContent atdpsMessageContent) {
		AtdpsMessageContent newContent = new AtdpsMessageContent();
		try {
			PropertyUtils.copyProperties(newContent, atdpsMessageContent);
			} catch (NoSuchMethodException e) {
			throw new MsException( e );
			} catch (IllegalAccessException e) {
			throw new MsException( e );
			} catch (InvocationTargetException e) {
			throw new MsException( e );
			}
			newContent.setInstruction(StringUtils.EMPTY);
			newContent.setDosage(StringUtils.EMPTY);
			newContent.setFractionalDosageFlag("N");
			newContent.setPrnFlag(StringUtils.EMPTY);
			newContent.setFreqCode(StringUtils.EMPTY);
			newContent.setSupplFreqCode(StringUtils.EMPTY);
			newContent.setMealTimeInfo(StringUtils.EMPTY);
			newContent.setAdjQty("0");
		return newContent;
	}
	
	private AtdpsMessage copyAtdpsMessageContent(AtdpsMessage atdpsMessage) {
		AtdpsMessage newContent = new AtdpsMessage();
		try {
			PropertyUtils.copyProperties(newContent, atdpsMessage);
			} catch (NoSuchMethodException e) {
			throw new MsException( e );
			} catch (IllegalAccessException e) {
			throw new MsException( e );
			} catch (InvocationTargetException e) {
			throw new MsException( e );
			}
			newContent.setInstruction(StringUtils.EMPTY);
			newContent.setDosage(StringUtils.EMPTY);
			newContent.setFractionDoseFlag(false);
			newContent.setPrnFlag(false);
			newContent.setFreqCode(StringUtils.EMPTY);
			newContent.setSupplFreqCode(StringUtils.EMPTY);
			newContent.setMealTimeInfo(StringUtils.EMPTY);
			newContent.setAdjQty("0");
		return newContent;
	}
	
	@SuppressWarnings("unchecked")
	private boolean getFdnFlag(OrderType orderType, String itemCode){
		List<FdnMapping> fdnMappingList = em.createQuery(
												"select o from FdnMapping o" +  // 20140826 index check : FdnMapping.workstore, orderType, itemCode  : I_ITEM_LOCATION_02
												" where o.workstore = :workstore" +
												" and o.orderType = :orderType" +
												" and o.itemCode = :itemCode" +
												" and o.type = :fdnType")
												.setParameter("workstore", workstore)
												.setParameter("orderType", orderType)
												.setParameter("itemCode", itemCode)
												.setParameter("fdnType", FdnType.FloatDrugName)
												.getResultList();
		return !(fdnMappingList.isEmpty());
	}
	
	private void setAtdpsContentWarnDesc(List<Warning> warningList, PrintLang printLang, AtdpsMessageContent atdpsMessageContent){
		int i=1;
	    for (Warning warning : warningList) {
	    	if (warning.getLang().equals(printLang.getDataValue()) && warning.getLineList() != null) {
	    		for (String warnText : warning.getLineList() ) {
		    		try {
						PropertyUtils.setProperty(atdpsMessageContent, "warnDesc"+i, warnText);
						i++;
					} catch (IllegalAccessException e) {
					} catch (InvocationTargetException e) {
					} catch (NoSuchMethodException e) {
					}
	    		}
	    		break;
	    	}
	    }
	    
	    for ( int j=i;j<=4;j++){
	    	try {
				PropertyUtils.setProperty(atdpsMessageContent, "warnDesc"+j, "");
				j++;
			} catch (IllegalAccessException e) {
			} catch (InvocationTargetException e) {
			} catch (NoSuchMethodException e) {
			}
	    }
	}
	
	private void setAtdpsContentWarnDesc(List<Warning> warningList, PrintLang printLang, AtdpsMessage atdpsMessage){
		int i=1;
	    for (Warning warning : warningList) {
	    	if (warning.getLang().equals(printLang.getDataValue()) && warning.getLineList() != null) {
	    		for (String warnText : warning.getLineList() ) {
		    		try {
						PropertyUtils.setProperty(atdpsMessage, "warnDesc"+i, warnText);
						i++;
					} catch (IllegalAccessException e) {
					} catch (InvocationTargetException e) {
					} catch (NoSuchMethodException e) {
					}
	    		}
	    		break;
	    	}
	    }
	    
	    for ( int j=i;j<=4;j++){
	    	try {
				PropertyUtils.setProperty(atdpsMessage, "warnDesc"+j, "");
				j++;
			} catch (IllegalAccessException e) {
			} catch (InvocationTargetException e) {
			} catch (NoSuchMethodException e) {
			}
	    }
	}
	
	private String getLangType(PrintLang printLang, List<String> ccCode){
		if ( printLang == PrintLang.Chi ) {
			for ( String str : ccCode ) {
				if ( str.contains("S") ) {
					return MachinePrintLang.SimplifiedChi.getShortFormValue();
				}
			}
		}
		return printLang.getShortFormValue();
	}
	
	public static class DeliveryComparator implements Comparator<Delivery>, Serializable {

		private static final long serialVersionUID = 1L;
		
		public int compare(Delivery o1, Delivery o2) {    		
			return new CompareToBuilder().append( o1.getBatchNum(), o2.getBatchNum() ).toComparison();    		
		}
	}
	
	@Transient
	public Integer getDurationUnitInDays(MedProfilePoItemRefillDurationUnit refillDurationUnit) {
		if (refillDurationUnit == null){
			return null;
		}
	
		switch (refillDurationUnit) {
			case Day:
				return 1;
			case Week:
				return 7;
			case Month:
			case Cycle:
				return 28;
		}
		return null;
	}
}
