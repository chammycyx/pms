package hk.org.ha.model.pms.udt.disp;

import java.util.Arrays;
import java.util.List;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DispOrderItemStatus implements StringValuedEnum {

	Vetted("V", "Vetted", 1),
	Picked("P", "Picked", 2),
	Assembled("A", "Assembled", 3),
	Checked("C", "Checked", 4),
	Issued("I", "Issued", 5),
	KeepRecord("K", "KeepRecord"), 
	Deleted("D", "Deleted");
		
    private final String dataValue;
    private final String displayValue;
    private final Integer seqNum;

    public static final List<DispOrderItemStatus> Vetted_Picked = Arrays.asList(Vetted,Picked);
    public static final List<DispOrderItemStatus> KeepRecord_Deleted = Arrays.asList(KeepRecord,Deleted);
    
    DispOrderItemStatus(final String dataValue, final String displayValue, final Integer seqNum){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
        this.seqNum = seqNum;
    }
    
    DispOrderItemStatus(final String dataValue, final String displayValue){
    	this(dataValue, displayValue, null);
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }
    
    public Integer getSeqNum() {
    	return this.seqNum;
    }
    
    public boolean isLaterThan(DispOrderItemStatus status) {
    	return seqNum != null && status.getSeqNum() != null && seqNum.compareTo(status.getSeqNum()) > 0;
    }
    
    public boolean isLaterThanEqualTo(DispOrderItemStatus status) {
    	return isLaterThan(status) || (status == this);
    }
    
    public static DispOrderItemStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DispOrderItemStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<DispOrderItemStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<DispOrderItemStatus> getEnumClass() {
    		return DispOrderItemStatus.class;
    	}
    }
}
