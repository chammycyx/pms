package hk.org.ha.model.pms.biz.alert.mds;

import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;

import java.util.List;

import javax.ejb.Local;

@Local
public interface PrescCheckManagerLocal {

	boolean checkPrescAlert(OrderViewInfo orderViewInfo);
	
	boolean checkPrescAlertByMedOrderItemList(OrderViewInfo orderViewInfo, List<MedOrderItem> medOrderItemList, boolean isMpDischarge);
	
	void overridePrescAlert(List<Integer> itemNumList);

	void replacePrescAlert(MedOrder medOrder);
	
	void clearPrescAlert(OrderViewInfo orderViewInfo);

}
