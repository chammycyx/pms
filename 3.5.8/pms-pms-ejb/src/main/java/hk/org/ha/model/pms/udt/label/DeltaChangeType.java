package hk.org.ha.model.pms.udt.label;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DeltaChangeType implements StringValuedEnum {

	NoChange("U", "No Change"),
	NewItem("N", "New Item"),
	DosageChanged("D", "Dosage Changed"),
	FrequencyChanged("F", "Frequency Changed"),
	MultipleChange("M", "Multiple Fields Changed");
	
    private final String dataValue;
    private final String displayValue;
        
    DeltaChangeType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    public String getDataValue() {
        return this.dataValue;
    }

    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static DeltaChangeType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DeltaChangeType.class, dataValue);
	}
        
	public static class Converter extends StringValuedEnumConverter<DeltaChangeType> {

		private static final long serialVersionUID = 1L;

		public Class<DeltaChangeType> getEnumClass() {
    		return DeltaChangeType.class;
    	}
    }
}
