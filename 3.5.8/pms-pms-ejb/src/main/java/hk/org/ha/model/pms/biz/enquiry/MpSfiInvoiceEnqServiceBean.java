package hk.org.ha.model.pms.biz.enquiry;

import hk.org.ha.fmk.pms.util.QueryUtils;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.charging.DrugChargeClearanceManagerLocal;
import hk.org.ha.model.pms.biz.order.DispOrderConverterLocal;
import hk.org.ha.model.pms.biz.printing.InvoiceRendererLocal;
import hk.org.ha.model.pms.biz.validation.SearchTextValidatorLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.InvoiceItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.PurchaseRequest;
import hk.org.ha.model.pms.udt.charging.ClearanceStatus;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.enquiry.SfiInvoiceEnqPatType;
import hk.org.ha.model.pms.udt.validation.SearchTextType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.charging.FcsPaymentStatus;
import hk.org.ha.model.pms.vo.enquiry.SfiInvoiceInfo;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;
import org.joda.time.DateTime;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("mpSfiInvoiceEnqService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MpSfiInvoiceEnqServiceBean implements MpSfiInvoiceEnqServiceLocal {

	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;

	@Out(required=false)
	private List<SfiInvoiceInfo> mpSfiInvoiceSummaryList;
	
	@Out(required=false)
	private SfiInvoiceInfo mpSfiInvoiceEnqSummary;
		
	@In
	private DrugChargeClearanceManagerLocal drugChargeClearanceManager;
		
	@In
	private SfiInvoiceEnqManagerLocal sfiInvoiceEnqManager;
	
	@In
	private SearchTextValidatorLocal searchTextValidator;
	
	@In
	private Workstore workstore;
	
	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
	
	@In
	private DispOrderConverterLocal dispOrderConverter;
	
	@In
	private InvoiceRendererLocal invoiceRenderer;
	
	private boolean retrieveSuccess = false;
	
	private String errMsg;

	private static final String PARAM_INV_STATUS = "invoiceStatusList";	
	private static final String INVOICE_STATUS_ACTIVE = "Active";
	
	private static SfiInvoiceInfoComparator sfiInvoiceInfoComparator = new SfiInvoiceInfoComparator();
	
	public void retrieveMpSfiInvoiceByInvoiceNumber(String invoiceNum, boolean includeVoidInvoice, SfiInvoiceEnqPatType patType){				
		errMsg = StringUtils.EMPTY;
		retrieveSuccess = false;
		
		SearchTextType searchTextType = searchTextValidator.checkSearchInputType(invoiceNum);
		
		if( SearchTextType.SfiInvoice.equals(searchTextType) ){
			List<InvoiceItem> invoiceItemList = corpPmsServiceProxy.retrieveSfiInvoiceItemListByInvoiceNum(invoiceNum, includeVoidInvoice, OrderType.InPatient, workstore, patType);
			List<Invoice> invoiceList = convertToInvoiceList(invoiceItemList);
			List<SfiInvoiceInfo> tempSfiInvoiceSummary = new ArrayList<SfiInvoiceInfo>();
			
			if( !invoiceList.isEmpty() ){
				Invoice invoice = invoiceList.get(0);
				Map<String, FcsPaymentStatus> fcsPaymentStatusMap = new HashMap<String, FcsPaymentStatus>();
				if ( invoice.getStatus() == InvoiceStatus.Outstanding ) {
					fcsPaymentStatusMap = drugChargeClearanceManager.checkSfiClearance(invoiceList, null);
					FcsPaymentStatus invoicePaymentStatus  = fcsPaymentStatusMap.get(invoice.getInvoiceNum());
					if ( invoicePaymentStatus != null && invoicePaymentStatus.getClearanceStatus() == ClearanceStatus.PaymentClear ) {
						invoice.setReceiptNum(invoicePaymentStatus.getReceiptNum());					
						invoice.setReceiveAmount(new BigDecimal(invoicePaymentStatus.getReceiptAmount()));
						invoice.setStatus(InvoiceStatus.Settled);	
						invoice.setFcsCheckDate(new DateTime().toDate());

						//update corp invoice & PurchaseRequest payment status
						List<PurchaseRequest> purchaseRequestList = new ArrayList<PurchaseRequest>();
						if ( invoice.getDispOrder().getTicket() != null ) {
							purchaseRequestList = retrievePurchaseRequestListByInvoiceNum(invoice.getInvoiceNum(), false);
						}		
						if ( purchaseRequestList.isEmpty() ) {
							List<Long> invoiceIdList = new ArrayList<Long>();
							invoiceIdList.add(invoice.getId());
							corpPmsServiceProxy.updateInvoicePaymentStatus(invoiceIdList, fcsPaymentStatusMap);
						} else {
							updatePurchaseRequestPaymentStatus(purchaseRequestList, invoiceList, fcsPaymentStatusMap);
						}
					}
				}
				Map<String, SfiInvoiceInfo> sfiInvoiceSummaryMap = sfiInvoiceEnqManager.convertInvoiceListToInvoiceSummary(invoiceList);	
				tempSfiInvoiceSummary.addAll(sfiInvoiceSummaryMap.values());
			} else if ( patType != SfiInvoiceEnqPatType.Private ) {
				List<PurchaseRequest> purchaseRequestList = retrievePurchaseRequestListByInvoiceNum(invoiceNum, includeVoidInvoice);
				logger.debug("retrieveMpSfiInvoiceByInvoiceNumber size = #0", purchaseRequestList.size());
				
				if ( !purchaseRequestList.isEmpty() ) {
					PurchaseRequest purchaseRequest = purchaseRequestList.get(0);
					Map<String, SfiInvoiceInfo> sfiInvoiceSummaryMap = sfiInvoiceEnqManager.convertPurchaseRequestListToInvoiceSummary(purchaseRequestList);
					Map<String, FcsPaymentStatus> fcsPaymentStatusMap = new HashMap<String, FcsPaymentStatus>();
					if ( purchaseRequest.getInvoiceStatus() == InvoiceStatus.Outstanding ) {
						fcsPaymentStatusMap = drugChargeClearanceManager.checkSfiClearance(null, purchaseRequestList);
						FcsPaymentStatus invoicePaymentStatus  = fcsPaymentStatusMap.get(purchaseRequest.getInvoiceNum());
						if ( invoicePaymentStatus != null && invoicePaymentStatus.getClearanceStatus() == ClearanceStatus.PaymentClear ) {
							Map<String, PurchaseRequest> purchaseRequestMap = updatePurchaseRequestPaymentStatus(purchaseRequestList, invoiceList, fcsPaymentStatusMap);
							
							tempSfiInvoiceSummary.addAll(sfiInvoiceEnqManager.updateSfiInvoiceInfoFcsStatusPaymentInfo(sfiInvoiceSummaryMap, purchaseRequestMap).values());
						}
					}
					if ( tempSfiInvoiceSummary.isEmpty() ) {
						tempSfiInvoiceSummary = new ArrayList<SfiInvoiceInfo>(sfiInvoiceSummaryMap.values());
					}
				}
			}
			if ( !tempSfiInvoiceSummary.isEmpty() ) {
				retrieveSuccess = true;
			}
			
			mpSfiInvoiceSummaryList = tempSfiInvoiceSummary;
		}else{
			errMsg = "0088";
		}
	}
	
	public void retrieveMpSfiInvoiceByInvoiceDate(Date invoiceDate, boolean includeVoidInvoice, SfiInvoiceEnqPatType patType){				
		errMsg = StringUtils.EMPTY;
		retrieveSuccess = false;
		
		if (invoiceDate.compareTo(new DateTime().toDate()) <= 0 ){					
			List<InvoiceItem> invoiceItemList = corpPmsServiceProxy.retrieveSfiInvoiceItemListByInvoiceDate(invoiceDate, includeVoidInvoice, OrderType.InPatient, workstore, patType);	
			List<Invoice> invoiceList = convertToInvoiceList(invoiceItemList);
			logger.debug("retrieveMpSfiInvoiceByInvoiceDate invoiceList size = #0", invoiceList.size());
			
			List<PurchaseRequest> purchaseRequestList = new ArrayList<PurchaseRequest>();
			if ( patType != SfiInvoiceEnqPatType.Private ) {
				purchaseRequestList = retrievePurchaseRequestListByInvoiceDate(invoiceDate, includeVoidInvoice);
				logger.debug("retrieveMpSfiInvoiceByInvoiceDate purchaseRequestList size = #0", purchaseRequestList.size());
			}
			List<SfiInvoiceInfo> tempSfiInvoiceSummary = convertSfiInvoiceSummary(invoiceList, purchaseRequestList);
			retrieveSuccess = (!tempSfiInvoiceSummary.isEmpty());
			
			mpSfiInvoiceSummaryList = tempSfiInvoiceSummary;
		}else{
			errMsg = "0086";
		}
	}
	
	public void retrieveMpSfiInvoiceByCaseNum(String caseNum, boolean includeVoidInvoice, SfiInvoiceEnqPatType patType){
		errMsg = StringUtils.EMPTY;
		retrieveSuccess = false;		
		
		SearchTextType searchTextType = searchTextValidator.checkSearchInputType(caseNum);		
		
		if( SearchTextType.Case.equals(searchTextType) ){
			List<InvoiceItem> invoiceItemList = corpPmsServiceProxy.retrieveSfiInvoiceItemListByCaseNum(caseNum, includeVoidInvoice, OrderType.InPatient, workstore, patType);
			List<Invoice> invoiceList = convertToInvoiceList(invoiceItemList);
			logger.debug("retrieveMpSfiInvoiceByCaseNum invoiceList size = #0", invoiceList.size());
			
			List<PurchaseRequest> purchaseRequestList = new ArrayList<PurchaseRequest>();
			if ( patType != SfiInvoiceEnqPatType.Private ) {
				purchaseRequestList = retrievePurchaseRequestListByCaseNum(caseNum, includeVoidInvoice);
				logger.debug("retrieveMpSfiInvoiceByCaseNum purchaseRequestList size = #0", purchaseRequestList.size());
			}
			List<SfiInvoiceInfo> tempSfiInvoiceSummary = convertSfiInvoiceSummary(invoiceList, purchaseRequestList);
			retrieveSuccess = (!tempSfiInvoiceSummary.isEmpty());
			
			mpSfiInvoiceSummaryList = tempSfiInvoiceSummary;
		}else{
			errMsg = "0068";
		}
	}
	
	public void retrieveMpSfiInvoiceByHkid(String hkid, boolean includeVoidInvoice, SfiInvoiceEnqPatType patType){
		errMsg = StringUtils.EMPTY;
		retrieveSuccess = false;
		
		SearchTextType searchTextType = searchTextValidator.checkSearchInputType(hkid);		
		
		if( SearchTextType.Hkid.equals(searchTextType) ){
			List<InvoiceItem> invoiceItemList = corpPmsServiceProxy.retrieveSfiInvoiceItemListByHkid(hkid, includeVoidInvoice, OrderType.InPatient, workstore, patType);
			List<Invoice> invoiceList = convertToInvoiceList(invoiceItemList);
			logger.debug("retrieveMpSfiInvoiceByHkid invoiceList size = #0", invoiceList.size());
			
			List<PurchaseRequest> purchaseRequestList = new ArrayList<PurchaseRequest>();
			if ( patType != SfiInvoiceEnqPatType.Private ) {
				purchaseRequestList = retrievePurchaseRequestListByHkid(hkid, includeVoidInvoice);
				logger.debug("retrieveMpSfiInvoiceByHkid purchaseRequestList size = #0", purchaseRequestList.size());
			}
			List<SfiInvoiceInfo> tempSfiInvoiceSummary = convertSfiInvoiceSummary(invoiceList, purchaseRequestList);
			retrieveSuccess = (!tempSfiInvoiceSummary.isEmpty());
			
			mpSfiInvoiceSummaryList = tempSfiInvoiceSummary;
		}else{
			errMsg = "0033";
		}
	}
	
	public void retrieveMpSfiInvoiceByExtactionPeriod(Integer dayRange, boolean includeVoidInvoice, SfiInvoiceEnqPatType patType){
		if ( dayRange != null && dayRange >= 0 ) {
			List<InvoiceItem> invoiceItemList = corpPmsServiceProxy.retrieveSfiInvoiceItemListByExtactionPeriod(dayRange, includeVoidInvoice, OrderType.InPatient, workstore, patType);
			List<Invoice> invoiceList = convertToInvoiceList(invoiceItemList);
			logger.debug("retrieveMpSfiInvoiceByExtactionPeriod invoiceList size = #0", invoiceList.size());
			
			List<PurchaseRequest> purchaseRequestList = new ArrayList<PurchaseRequest>();
			if ( patType != SfiInvoiceEnqPatType.Private ) {
				purchaseRequestList = this.retrievePurchaseRequestListByExtactionPeriod(dayRange, includeVoidInvoice);
				logger.debug("retrieveMpSfiInvoiceByExtactionPeriod purchaseRequestList size = #0", purchaseRequestList.size());
			}
			List<SfiInvoiceInfo> tempSfiInvoiceSummary = convertSfiInvoiceSummary(invoiceList, purchaseRequestList);
			retrieveSuccess = (!tempSfiInvoiceSummary.isEmpty());
			
			mpSfiInvoiceSummaryList = tempSfiInvoiceSummary;
		} else {
			errMsg = "0034";
		}
	}
	
	private List<Invoice> convertToInvoiceList(List<InvoiceItem> invoiceItemList){
		Set<Invoice> invoiceSet = new HashSet<Invoice>();
		for ( InvoiceItem invoiceItem : invoiceItemList ) {
			invoiceItem.getInvoice().getDispOrder().getDispOrderItemList().add(invoiceItem.getDispOrderItem());
			invoiceSet.add(invoiceItem.getInvoice());
		}
		return new ArrayList<Invoice>(invoiceSet);
	}
			
	public void retrieveMpSfiInvoicePaymentStatus(Long invoiceId, Long purchaseReqId){
		mpSfiInvoiceEnqSummary = new SfiInvoiceInfo();
		
		Invoice sfiInvoice = retrieveInvoiceForEnq(invoiceId, purchaseReqId);
		
		if ( sfiInvoice != null ) {
			FcsPaymentStatus fcsPaymentStatus = drugChargeClearanceManager.checkSfiDrugClearanceByInvoice(sfiInvoice);
			mpSfiInvoiceEnqSummary.setChargeAmount(fcsPaymentStatus.getChargeAmount());
			mpSfiInvoiceEnqSummary.setReceiptAmount(fcsPaymentStatus.getReceiptAmount());
			mpSfiInvoiceEnqSummary.setReceiptMessage(fcsPaymentStatus.getReceiptMessage());
			mpSfiInvoiceEnqSummary.setReceiptNum(fcsPaymentStatus.getReceiptNum());
		}
		
	}
		
	public void retrieveMpSfiInvoiceDetail(Long invoiceId, Long purchaseReqId, boolean isPreview){
		errMsg = StringUtils.EMPTY;

		Invoice sfiInvoice = retrieveInvoiceForEnq(invoiceId, purchaseReqId);
		
		if( sfiInvoice != null ){
			if( isPreview ){
				invoiceRenderer.createSfiInvoiceListForPreview(sfiInvoice, true);
			}else{
				errMsg = invoiceRenderer.printSfiInvoice(sfiInvoice, true);
			}			
		}
	}
	
	private List<SfiInvoiceInfo> convertSfiInvoiceSummary(List<Invoice> invoiceList, List<PurchaseRequest> purchaseRequestList){
		List<SfiInvoiceInfo> tempSfiInvoiceSummary = new ArrayList<SfiInvoiceInfo>();
		Map<String, SfiInvoiceInfo> sfiInvoiceSummaryMap = new HashMap<String, SfiInvoiceInfo>();
		
		//for checkSfiClearance
		Map<String, Invoice> drugChargeClearanceInvoieMap = new HashMap<String, Invoice>();
		Map<String, PurchaseRequest> drugChargeClearancePurchaseRequestMap = new HashMap<String, PurchaseRequest>();
		
		//for update drug charge info
		Map<String, Invoice> invoiceMap = new HashMap<String, Invoice>();
		Map<String, PurchaseRequest> purchaseRequestHvInvoiceMap = new HashMap<String, PurchaseRequest>();
		
		//for convertPurchaseRequestListToInvoiceSummary
		Set<String> invoiceNumSet = new HashSet<String>();
		Map<String, PurchaseRequest> purchaseRequestWoInvoiceMap = new HashMap<String, PurchaseRequest>(); 
		
		for ( Invoice invoice : invoiceList ) {
			invoiceNumSet.add(invoice.getInvoiceNum());
			invoiceMap.put(invoice.getInvoiceNum(), invoice);
			if ( invoice.getStatus() != InvoiceStatus.Outstanding ){
				continue;
			}
			//use invoiceNum to check drug charge Clearance if invoice status = outstanding
			if ( !drugChargeClearanceInvoieMap.containsKey(invoice.getInvoiceNum()) ) {
				drugChargeClearanceInvoieMap.put(invoice.getInvoiceNum(), invoice);
			}
		}	
		
		for ( PurchaseRequest purchaseRequest : purchaseRequestList ) {
			if ( invoiceNumSet.contains(purchaseRequest.getInvoiceNum()) ) {
				purchaseRequestHvInvoiceMap.put(purchaseRequest.getInvoiceNum(), purchaseRequest);
			} else {
				purchaseRequestWoInvoiceMap.put(purchaseRequest.getInvoiceNum(), purchaseRequest);
			}
			if ( purchaseRequest.getInvoiceStatus() != InvoiceStatus.Outstanding ){
				continue;
			}
			logger.debug("convertSfiInvoiceSummary:purchaseRequestId:#0|MedProfileId:#1|ProcessingFlag:#2|DeliveryGenFlag:#3|", 
																								purchaseRequest.getId(), 
																								purchaseRequest.getMedProfile().getId(),
																								purchaseRequest.getMedProfile().getProcessingFlag(),
																								purchaseRequest.getMedProfile().getDeliveryGenFlag());
			if ( Boolean.TRUE.equals(purchaseRequest.getMedProfile().getProcessingFlag()) || Boolean.TRUE.equals(purchaseRequest.getMedProfile().getDeliveryGenFlag()) ) {
				//do not update invoice & purchaseRequest if medProfile is locked by others
				if ( drugChargeClearanceInvoieMap.containsKey(purchaseRequest.getInvoiceNum()) ) { 
					drugChargeClearanceInvoieMap.remove(purchaseRequest.getInvoiceNum());
				}
				if ( drugChargeClearancePurchaseRequestMap.containsKey(purchaseRequest.getInvoiceNum()) ) {
					drugChargeClearancePurchaseRequestMap.remove(purchaseRequest.getInvoiceNum());
				}
			} else if ( !drugChargeClearancePurchaseRequestMap.containsKey(purchaseRequest.getInvoiceNum()) && 
							!drugChargeClearanceInvoieMap.containsKey(purchaseRequest.getInvoiceNum())) {
				drugChargeClearancePurchaseRequestMap.put(purchaseRequest.getInvoiceNum(), purchaseRequest);
			}
		}

		Map<String, FcsPaymentStatus> fcsPaymentStatusMap = new HashMap<String, FcsPaymentStatus>();
		if ( !drugChargeClearanceInvoieMap.isEmpty() || !drugChargeClearancePurchaseRequestMap.isEmpty() ) {
			fcsPaymentStatusMap = drugChargeClearanceManager.checkSfiClearance(new ArrayList<Invoice>(drugChargeClearanceInvoieMap.values()), 
																				new ArrayList<PurchaseRequest>(drugChargeClearancePurchaseRequestMap.values()));
		}
		
		sfiInvoiceSummaryMap = sfiInvoiceEnqManager.convertInvoiceListToInvoiceSummary(invoiceList);
		sfiInvoiceSummaryMap.putAll(sfiInvoiceEnqManager.convertPurchaseRequestListToInvoiceSummary(new ArrayList<PurchaseRequest>(purchaseRequestWoInvoiceMap.values())));
		
		Map<Long, Invoice> invoiceIdWoPurchaseReqForPaymentMap = new HashMap<Long, Invoice>();
		List<Invoice> invoiceHvPurchaseReqForPaymentList = new ArrayList<Invoice>();
		List<PurchaseRequest> purchaseRequestForPaymentList = new ArrayList<PurchaseRequest>();
		Map<String, FcsPaymentStatus> fcsPaymentClearMap = new HashMap<String, FcsPaymentStatus>();
		for (Map.Entry<String, FcsPaymentStatus> entry : fcsPaymentStatusMap.entrySet() ) {
			String invoiceNum = entry.getKey();
			FcsPaymentStatus invoicePaymentStatus = entry.getValue();
			if ( invoicePaymentStatus != null && invoicePaymentStatus.getClearanceStatus() == ClearanceStatus.PaymentClear ) {
				fcsPaymentClearMap.put(invoiceNum, invoicePaymentStatus);
				if ( invoiceMap.containsKey(invoiceNum) ) {
					if ( purchaseRequestHvInvoiceMap.containsKey(invoiceNum) ) { //invoice is created by purchaseRequest
						invoiceHvPurchaseReqForPaymentList.add(invoiceMap.get(invoiceNum));
						purchaseRequestForPaymentList.add(purchaseRequestHvInvoiceMap.get(invoiceNum));
					} else { // invoice is created by charging module
						Invoice invoice = invoiceMap.get(invoiceNum);
						invoiceIdWoPurchaseReqForPaymentMap.put(invoice.getId(), invoice);
					}
				} else if ( purchaseRequestWoInvoiceMap.containsKey(invoiceNum) ) {
					purchaseRequestForPaymentList.add(purchaseRequestWoInvoiceMap.get(invoiceNum));
				}
			}
		}
		
		if ( !invoiceIdWoPurchaseReqForPaymentMap.isEmpty() ) {
			updateInvoicePaymentStatus(sfiInvoiceSummaryMap, invoiceIdWoPurchaseReqForPaymentMap, fcsPaymentClearMap);
		}
		if ( !purchaseRequestForPaymentList.isEmpty() ) {
			Map<String, PurchaseRequest> purchaseRequestMap = updatePurchaseRequestPaymentStatus(purchaseRequestForPaymentList, invoiceHvPurchaseReqForPaymentList, fcsPaymentClearMap);
			sfiInvoiceSummaryMap = sfiInvoiceEnqManager.updateSfiInvoiceInfoFcsStatusPaymentInfo(sfiInvoiceSummaryMap, purchaseRequestMap);
		} 
		
		tempSfiInvoiceSummary.addAll(sfiInvoiceSummaryMap.values());
		Collections.sort(tempSfiInvoiceSummary, sfiInvoiceInfoComparator);
		return tempSfiInvoiceSummary;
	}
	
	@SuppressWarnings("unchecked")
	private List<PurchaseRequest> retrievePurchaseRequestListByInvoiceNum(String invoiceNum, boolean includeVoidInvoice){
		return em.createQuery(
				"select o from PurchaseRequest o" + // 20150420 index check : PurchaseRequest.invoiceNum : I_PURCHASE_REQUEST_01
				" where o.hospCode = :hospCode" +
				" and o.invoiceNum = :invoiceNum" +
				" and o.invoiceStatus in :invoiceStatusList")
				.setParameter("hospCode", workstore.getHospCode())
				.setParameter("invoiceNum", invoiceNum)
				.setParameter(PARAM_INV_STATUS, includeVoidInvoice ? InvoiceStatus.Outstanding_Settle_Void : InvoiceStatus.Outstanding_Settle )
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	private List<PurchaseRequest> retrievePurchaseRequestListByInvoiceDate(Date invoiceDate, boolean includeVoidInvoice){
		DateTime invoiceEndDate = new DateTime(invoiceDate).plusDays(1);	
		return em.createQuery(
				"select o from PurchaseRequest o" + // 20150420 index check : PurchaseRequest.hospCode,invoiceDate,invoiceStatus : I_PURCHASE_REQUEST_03
				" where o.hospCode = :hospCode" +
				" and o.invoiceDate >= :invoiceDate" +
				" and o.invoiceDate < :invoiceEndDate" +
				" and o.invoiceStatus in :invoiceStatusList")
				.setParameter("hospCode", workstore.getHospCode())
				.setParameter("invoiceDate", invoiceDate, TemporalType.DATE)
				.setParameter("invoiceEndDate", invoiceEndDate.toDate(), TemporalType.DATE)
				.setParameter(PARAM_INV_STATUS, includeVoidInvoice ? InvoiceStatus.Outstanding_Settle_Void : InvoiceStatus.Outstanding_Settle )
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	private List<PurchaseRequest> retrievePurchaseRequestListByCaseNum(String caseNum, boolean includeVoidInvoice){
		return em.createQuery(
				"select o from PurchaseRequest o" + // 20150420 index check : MedCase.caseNum : I_MED_CASE_01
				" where o.medProfile.hospCode = :hospCode" +
				" and o.medProfile.medCase.caseNum = :caseNum" +
				" and o.invoiceStatus in :invoiceStatusList")
				.setParameter("hospCode", workstore.getHospCode())
				.setParameter("caseNum", caseNum)
				.setParameter(PARAM_INV_STATUS, includeVoidInvoice ? InvoiceStatus.Outstanding_Settle_Void : InvoiceStatus.Outstanding_Settle )
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	private List<PurchaseRequest> retrievePurchaseRequestListByHkid(String hkid, boolean includeVoidInvoice){
		return em.createQuery(
				"select o from PurchaseRequest o" + // 20150420 index check : Patient.hkid : I_PATIENT_02
				" where o.medProfile.hospCode = :hospCode" +
				" and o.medProfile.patient.hkid = :hkid" +
				" and o.invoiceStatus in :invoiceStatusList")
				.setParameter("hospCode", workstore.getHospCode())
				.setParameter("hkid", hkid)
				.setParameter(PARAM_INV_STATUS, includeVoidInvoice ? InvoiceStatus.Outstanding_Settle_Void : InvoiceStatus.Outstanding_Settle )
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	private List<PurchaseRequest> retrievePurchaseRequestListByExtactionPeriod(Integer dayRange, boolean includeVoidInvoice){
		DateTime today = new DateTime();
		return em.createQuery(
				"select o from PurchaseRequest o" + // 20150420 index check : PurchaseRequest.hospCode,invoiceDate,invoiceStatus : I_PURCHASE_REQUEST_03
				" where o.hospCode = :hospCode" +
				" and o.invoiceDate > :beforeDate" +
				" and o.invoiceDate < :nextday" +
				" and o.invoiceStatus in :invoiceStatusList")
				.setParameter("hospCode", workstore.getHospCode())
				.setParameter("nextday", today.plusDays(1).toDate() ,TemporalType.DATE)
				.setParameter("beforeDate", today.minusDays(dayRange+1).toDate() ,TemporalType.DATE)
				.setParameter(PARAM_INV_STATUS, includeVoidInvoice ? InvoiceStatus.Outstanding_Settle_Void : InvoiceStatus.Outstanding_Settle )
				.getResultList();
	}
	
	private void updateInvoicePaymentStatus(Map<String, SfiInvoiceInfo> sfiInvoiceSummaryMap, 
																Map<Long, Invoice> invoiceIdWoPurchaseReqForPaymentMap, 
																Map<String, FcsPaymentStatus> fcsPaymentClearMap){
		
		for ( Map.Entry<Long, Invoice> entry : invoiceIdWoPurchaseReqForPaymentMap.entrySet() ) {
			Invoice invoice = entry.getValue();
			logger.debug("updateInvoicePaymentStatus invoiceId:#0|invoiceNum:#1", invoice.getId(), invoice.getInvoiceNum());
			SfiInvoiceInfo sfiInvoiceInfo = sfiInvoiceSummaryMap.get(invoice.getInvoiceNum());
			FcsPaymentStatus fcsPaymentStatus = fcsPaymentClearMap.get(invoice.getInvoiceNum());
			if ( sfiInvoiceInfo != null && fcsPaymentStatus != null ) {
				sfiInvoiceInfo.setChargeAmount(fcsPaymentStatus.getChargeAmount());
				sfiInvoiceInfo.setInvoiceStatus(INVOICE_STATUS_ACTIVE);
				sfiInvoiceInfo.setReceiptNum(fcsPaymentStatus.getReceiptNum());
				sfiInvoiceInfo.setReceiptAmount(fcsPaymentStatus.getReceiptAmount());
				sfiInvoiceInfo.setReceiptMessage(fcsPaymentStatus.getReceiptMessage());
				sfiInvoiceInfo.setReceiptNum(fcsPaymentStatus.getReceiptNum());
				sfiInvoiceInfo.setInvoiceStatusForClearance(InvoiceStatus.Settled);
			}
		}
		corpPmsServiceProxy.updateInvoicePaymentStatus(new ArrayList<Long>(invoiceIdWoPurchaseReqForPaymentMap.keySet()), fcsPaymentClearMap);
	}
	
	@SuppressWarnings("unchecked")
	private Map<String, PurchaseRequest> updatePurchaseRequestPaymentStatus(List<PurchaseRequest> inPurchaseRequestList, List<Invoice> inInvoiceList, Map<String, FcsPaymentStatus> fcsPaymentStatusMap){
		Set<Long> medProfileIdSet = new TreeSet<Long>();
		Map<Long, List<PurchaseRequest>> purchaseRequestMedProfileIdMap = new HashMap<Long, List<PurchaseRequest>>();
    	for (PurchaseRequest purchaseRequest: inPurchaseRequestList) {
    		MedProfile medProfile = purchaseRequest.getMedProfile();
    		medProfileIdSet.add(medProfile.getId());
    		if ( !purchaseRequestMedProfileIdMap.containsKey(medProfile.getId()) ) {
    			purchaseRequestMedProfileIdMap.put(medProfile.getId(), new ArrayList<PurchaseRequest>());
    		}
    		purchaseRequestMedProfileIdMap.get(medProfile.getId()).add(purchaseRequest);
    	}
    	
    	Map<String, Invoice> invoiceNumMap = new HashMap<String, Invoice>();
    	for ( Invoice invoice : inInvoiceList ) {
    		invoiceNumMap.put(invoice.getInvoiceNum(), invoice);
    	}
    	
    	if ( medProfileIdSet.isEmpty() ) {
    		return new HashMap<String, PurchaseRequest>();
    	}
    	
    	medProfileIdSet = new TreeSet<Long>(QueryUtils.splitExecute(em.createQuery(
			    			"select o.id from MedProfile o" + // 20150420 index check : MedProfile.id : PK_MED_PROFILE
			    			" where o.id in :idSet" +
			    			" and o.processingFlag = false and o.deliveryGenFlag = false")
			    			.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock),
			    			"idSet", medProfileIdSet));

    	Map<String, FcsPaymentStatus> updateFcsPaymentStatusMap = new HashMap<String, FcsPaymentStatus>();
    	Map<String, PurchaseRequest> purchaseRequestMap = new HashMap<String, PurchaseRequest>();
    	List<Long> invoiceIdList = new ArrayList<Long>();
    	for (Long medProfileId : medProfileIdSet) {
    		List<PurchaseRequest> purchaseRequestList = purchaseRequestMedProfileIdMap.get(medProfileId);
    		for ( PurchaseRequest purchaseRequest : purchaseRequestList) {
        		logger.debug("updatePurchaseRequestPaymentStatus medProfileId#0|InvoiceNum:#1|", medProfileId, purchaseRequest.getInvoiceNum());
    			FcsPaymentStatus invoicePaymentStatus = fcsPaymentStatusMap.get(purchaseRequest.getInvoiceNum());
    			updateFcsPaymentStatusMap.put(invoicePaymentStatus.getInvoiceNum(), invoicePaymentStatus);
    			Invoice invoice = invoiceNumMap.get(purchaseRequest.getInvoiceNum());
    			if ( invoice != null ) {
    				invoiceIdList.add(invoice.getId());
    			}
        		try {
        			BigDecimal receiveAmount = new BigDecimal(invoicePaymentStatus.getReceiptAmount());
        			purchaseRequest.setInvoiceStatus(InvoiceStatus.Settled);
        			purchaseRequest.setReceiptNum(invoicePaymentStatus.getReceiptNum());
        			purchaseRequest.setReceiveAmount(receiveAmount);
        			purchaseRequest.setFcsCheckDate(new DateTime().toDate());
        			if ( purchaseRequest.getDueDate() == null ) {
        				purchaseRequest.setDueDate(purchaseRequest.getFcsCheckDate());
        			}
        			em.merge(purchaseRequest);
        			
        			//for mpSfiInvoiceEnq screen display
        			purchaseRequest.setChargeAmount(invoicePaymentStatus.getChargeAmount());
        			purchaseRequest.setReceiptMessage(invoicePaymentStatus.getReceiptMessage());
        			purchaseRequestMap.put(invoicePaymentStatus.getInvoiceNum(), purchaseRequest);
        		} catch (NumberFormatException ex) {
        			logger.error("Error parsing receiptAmount(#0) returned from FCS for PurchaseRequest(invoiceNum=#1)",
        								invoicePaymentStatus.getReceiptAmount(), purchaseRequest.getInvoiceNum());
        		}
    		}
    	}
    	em.flush();

    	if ( !invoiceIdList.isEmpty() ) {
        	corpPmsServiceProxy.updateInvoicePaymentStatus(new ArrayList<Long>(invoiceIdList), updateFcsPaymentStatusMap);
    	}
    	return purchaseRequestMap;
	}
	
	private Invoice retrieveInvoiceForEnq(Long invoiceId, Long purchaseReqId) {
		Invoice sfiInvoice = null;
		if( purchaseReqId != null) {
			PurchaseRequest purchaseRequest = em.find(PurchaseRequest.class, purchaseReqId);

			if ( purchaseRequest != null ) {
				if(invoiceId != null && Boolean.TRUE.equals(purchaseRequest.getDirectPrintFlag()))
				{
					sfiInvoice = corpPmsServiceProxy.retrieveInvoiceByInvoiceId(invoiceId);
				}
				else
				{
					sfiInvoice = dispOrderConverter.createInvoiceFromPurchaseRequestSnapshot(purchaseRequest, workstore);
				}
			}
		}
		else if(invoiceId != null) {
			sfiInvoice = corpPmsServiceProxy.retrieveInvoiceByInvoiceId(invoiceId);
		}
		
		return sfiInvoice;
	}
	
	public String getErrMsg() {
		return errMsg;
	}
	
	public boolean isRetrieveSuccess(){
		return retrieveSuccess;
	}
	
	private static class SfiInvoiceInfoComparator implements Comparator<SfiInvoiceInfo>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(SfiInvoiceInfo sfiInvoiceInfo1, SfiInvoiceInfo sfiInvoiceInfo2) {	
			return sfiInvoiceInfo1.getInvoiceNum().compareTo(sfiInvoiceInfo2.getInvoiceNum());
		}
    }
	
	@Remove
	public void destroy(){
		if ( mpSfiInvoiceSummaryList != null ){
			mpSfiInvoiceSummaryList = null;
		}
		
		if ( mpSfiInvoiceEnqSummary != null ){
			mpSfiInvoiceEnqSummary = null;
		}
	}
}
