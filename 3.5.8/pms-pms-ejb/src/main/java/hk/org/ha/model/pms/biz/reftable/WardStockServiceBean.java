package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.vo.reftable.WardStockMap;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("wardStockService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class WardStockServiceBean implements WardStockServiceLocal {

	@Logger
	private Log logger;
	
	@Out(required=false)
	private WardStockMap wardStockMap;
	
	@In
	private MpWardManagerLocal mpWardManager;
	
	public WardStockMap retrieveWardStockMap() {
		MedProfile medProfileInMemory = (MedProfile) Contexts.getSessionContext().get("medProfile");
		Workstore workstore = (Workstore) Contexts.getSessionContext().get("workstore");
		
		logger.debug("retrieveWardStockMap - workstore.getWorkstoreGroup().getWorkstoreGroupCode() | #0", workstore.getWorkstoreGroup().getWorkstoreGroupCode());
		logger.debug("retrieveWardStockMap - medProfile.getWardCode() | #0", medProfileInMemory.getWardCode());
		
		wardStockMap = new WardStockMap();		
		wardStockMap.setWardStock(
				mpWardManager.retrieveWardStockMap(
						workstore.getWorkstoreGroup().getInstitution().getInstCode(), 
						medProfileInMemory.getWardCode()
				)
		);
		logger.debug("wardStockMap size | #0", wardStockMap.getWardStock().size());
		return wardStockMap;
	}
	
	@Remove
	public void destroy() {
		if ( wardStockMap != null ) {			
			wardStockMap = null;
		}
	}

}
