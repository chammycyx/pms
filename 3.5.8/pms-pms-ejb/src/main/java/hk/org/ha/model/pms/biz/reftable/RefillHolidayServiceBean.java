package hk.org.ha.model.pms.biz.reftable;

import static hk.org.ha.model.pms.prop.Prop.REFILL_ADJHOLIDAY_ADVANCEDAYS;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.reftable.Prop;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.persistence.reftable.WorkstationProp;
import hk.org.ha.model.pms.vo.security.PropMap;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("refillHolidayService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class RefillHolidayServiceBean implements RefillHolidayServiceLocal {
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em; 
	
	@In
	private Workstation workstation;
	
	@In @Out(required=false)
	private PropMap propMap;
	
	@Out(required = false)
	private Prop refillHolidayAdvDueDateProp;
	
	@SuppressWarnings("unchecked")
	public void updateRefillHolidayAdvanceDueDate(Integer advanceDueDate)
	{
		List<WorkstationProp> workstationPropList = em.createQuery(
				"select o from WorkstationProp o" + // 20120303 index check : WorkstationProp.workstation : FK_WORKSTATION_PROP_01
				" where o.workstation = :workstation" +
				" and o.prop.name = :name")
				.setParameter("workstation", workstation)
				.setParameter("name", REFILL_ADJHOLIDAY_ADVANCEDAYS.getName())
				.getResultList();
		
		if ( !workstationPropList.isEmpty() ){
			workstationPropList.get(0).setValue(advanceDueDate.toString());
		} else {
			if ( refillHolidayAdvDueDateProp == null ) {
				retrieveRefillHolidayAdvanceDueDateProp();
			}
			if ( refillHolidayAdvDueDateProp != null ) {
				WorkstationProp workstationProp = new WorkstationProp();
				workstationProp.setProp(refillHolidayAdvDueDateProp);
				workstationProp.setValue(advanceDueDate.toString());
				workstationProp.setWorkstation(workstation);
				em.persist(workstationProp);
				em.flush();
			}
		}
		
		propMap.add(REFILL_ADJHOLIDAY_ADVANCEDAYS.getName(), advanceDueDate.toString());
	}
	
	@SuppressWarnings("unchecked")
	public void retrieveRefillHolidayAdvanceDueDateProp(){
		List<Prop>  propList = em.createQuery(
									"select o from Prop o" + // 20120303 index check : Prop.name : UI_PROP_01
									" where o.name = :name")
									.setParameter("name", REFILL_ADJHOLIDAY_ADVANCEDAYS.getName())
									.getResultList();
		
		if ( !propList.isEmpty() ) {
			refillHolidayAdvDueDateProp = propList.get(0);
		} else {
			refillHolidayAdvDueDateProp = null; 
			logger.info("Cannot find #0 Prop", REFILL_ADJHOLIDAY_ADVANCEDAYS.getName());
		}
	}

	@Remove
	public void destroy() 
	{
		if ( refillHolidayAdvDueDateProp != null ) {
			refillHolidayAdvDueDateProp = null;
		}
	}
	
}
