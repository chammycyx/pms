package hk.org.ha.model.pms.vo.reftable;

import hk.org.ha.model.pms.vo.patient.PasSpecialty;
import hk.org.ha.model.pms.persistence.reftable.DrsConfig;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class DrsConfigInfo implements Serializable 
{
	private static final long serialVersionUID = 1L;
	
	private List<DrsConfig> drsConfigList;

	private List<PasSpecialty> refillModulePasSpecialtyList;

	public void setDrsConfigList(List<DrsConfig> drsConfigList) {
		this.drsConfigList = drsConfigList;
	}

	public List<DrsConfig> getDrsConfigList() {
		return drsConfigList;
	}

	public void setRefillModulePasSpecialtyList(List<PasSpecialty> refillModulePasSpecialtyList) {
		this.refillModulePasSpecialtyList = refillModulePasSpecialtyList;
	}

	public List<PasSpecialty> getRefillModulePasSpecialtyList() {
		return refillModulePasSpecialtyList;
	}
}