package hk.org.ha.model.pms.udt.vetting;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

import java.util.Arrays;
import java.util.List;

public enum CapdVoucherStatus implements StringValuedEnum {

	Issued("I", "Issued"),
	Deleted("D", "Deleted"),
	Suspended("S", "Suspended");
	
    private final String dataValue;
    private final String displayValue;
    
    public static final List<CapdVoucherStatus> Issued_Suspended = Arrays.asList(Issued,Suspended);

    CapdVoucherStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    

	public static CapdVoucherStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(CapdVoucherStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<CapdVoucherStatus> {

		private static final long serialVersionUID = 1L;

		@Override
		public Class<CapdVoucherStatus> getEnumClass() {
    		return CapdVoucherStatus.class;
    	}
    }
}
