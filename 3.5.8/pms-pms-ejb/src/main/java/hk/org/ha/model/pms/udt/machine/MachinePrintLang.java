package hk.org.ha.model.pms.udt.machine;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum MachinePrintLang implements StringValuedEnum {

	Eng("en", "English", "E"),
	Chi("zh_TW", "Chinese", "C"), 
	SimplifiedChi("zh_CN", "Simplified Chinese", "S");
	
	private final String dataValue;
	private final String displayValue;
    private final String shortFormValue;
	
	MachinePrintLang (final String dataValue, final String displayValue, final String shortFormValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
        this.shortFormValue = shortFormValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }
    
    public String getShortFormValue() {
        return this.shortFormValue;
    }  
	
	public static MachinePrintLang dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MachinePrintLang.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<MachinePrintLang> {

		private static final long serialVersionUID = 1L;

		public Class<MachinePrintLang> getEnumClass() {
    		return MachinePrintLang.class;
    	}
    }
}
