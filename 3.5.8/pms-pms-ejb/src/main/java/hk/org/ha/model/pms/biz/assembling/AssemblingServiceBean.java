package hk.org.ha.model.pms.biz.assembling;

import static hk.org.ha.model.pms.prop.Prop.BATCH_DAYEND_ORDER_DURATION;
import hk.org.ha.fmk.pms.sys.SysProfile;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.refill.RefillScheduleManagerLocal;
import hk.org.ha.model.pms.biz.sys.SystemMessageManagerLocal;
import hk.org.ha.model.pms.biz.ticketgen.TicketManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.refill.DocType;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.assembling.AssembleItem;
import hk.org.ha.model.pms.vo.security.PropMap;
import hk.org.ha.model.pms.workflow.DispensingPmsModeLocal;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;
import org.jboss.seam.security.Identity;
import org.joda.time.DateTime;
import org.joda.time.Period;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("assemblingService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class AssemblingServiceBean implements AssemblingServiceLocal {
	
	@Logger
	private Log logger; 
	
	@PersistenceContext
	private EntityManager em;
	
	private boolean updateSuccess;
	
	private String errMsg; 
	
	@In
	private Workstore workstore;
		
	@In
	private SystemMessageManagerLocal systemMessageManager;

	@In
	private DispensingPmsModeLocal dispensingPmsMode;
	
	@In
	private RefillScheduleManagerLocal refillScheduleManager;
	
	@In
	private TicketManagerLocal ticketManager;
	
	@In
	private Identity identity;

	@In
	private SysProfile sysProfile;
	
	@In
	private PropMap propMap;
	
	@Out(required = false)
	private Long dispOrderId;
	
	private final static String ACTIVE_OPERATION_MODE = "active.operation.mode";
	private final static String WORKFLOW_EDS_BPM_ENABLED = "workflow.eds.bpm.enabled";
	private final static String FASTQUEUE_MESSAGE_SUFFIX = " - Fast Queue";
	private final static List<String> ignoreFastQueueMessageCodeList = Arrays.asList("0061","0075","0175");
	
	@SuppressWarnings("unchecked")
	public AssembleItem updateDispOrderItemStatusToAssembled(Date ticketDate, String ticketNum, Integer itemNum){
		
		AssembleItem assembleItem = new AssembleItem();
		assembleItem.setTicketDate(ticketDate);
		assembleItem.setTicketNum(ticketNum);
		assembleItem.setItemNum(itemNum);
		assembleItem.setFastQueueFlag(ticketManager.isFastQueueTicket(ticketDate, ticketNum));
		
		try {
			
			updateSuccess = false;
			errMsg = null;
			
			if ( !sysProfile.isTesting() ) {
				if (checkTicketDate(ticketDate) > 0 ){
					errMsg = "0058";
					setSystemMessageContent("0075", assembleItem);
					return assembleItem;
				}
			}
			
			if (itemNum == 0 ){
				updateSuccess = false;
				assembleItem.setReprintFlag(false);
				setSystemMessageContent("0061", assembleItem);
				return assembleItem;
				
			}

			em.createQuery(
					"select o.id from DispOrder o" + // 20160615 index check : DispOrder.workstore,orderType,ticketDate,ticketNum : I_DISP_ORDER_15
					" where o.workstore = :workstore" +
					" and o.orderType = :orderType" +
					" and o.ticketDate = :ticketDate" +
					" and o.ticketNum = :ticketNum" +
					" and o.status not in :dispOrderStatus")
					.setParameter("workstore", workstore)
					.setParameter("orderType", OrderType.OutPatient)
					.setParameter("ticketDate", ticketDate, TemporalType.DATE)
					.setParameter("ticketNum", ticketNum)
					.setParameter("dispOrderStatus", DispOrderStatus.Deleted_SysDeleted)
					.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
					.getResultList();

			List<DispOrderItem> dispOrderItemList = em.createQuery(
					"select o from DispOrderItem o" + // 20160615 index check : DispOrder.workstore,orderType,ticketDate,ticketNum : I_DISP_ORDER_15
					" where o.dispOrder.workstore = :workstore" +
					" and o.dispOrder.orderType = :orderType" +
					" and o.dispOrder.ticketDate = :ticketDate" +
					" and o.dispOrder.ticketNum = :ticketNum" +
					" and o.dispOrder.status <> :dispOrderStatus" +
					" and o.itemNum = :itemNum")
					.setParameter("workstore", workstore)
					.setParameter("orderType", OrderType.OutPatient)
					.setParameter("ticketDate", ticketDate, TemporalType.DATE)
					.setParameter("ticketNum", ticketNum)
					.setParameter("dispOrderStatus", DispOrderStatus.SysDeleted)
					.setParameter("itemNum", itemNum)
					.setHint(QueryHints.FETCH, "o.dispOrder")
					.setHint(QueryHints.FETCH, "o.pharmOrderItem.medOrderItem")
					.getResultList();
			
			if ( !dispOrderItemList.isEmpty() ) { 
				
				DispOrderItem dispOrderItem = dispOrderItemList.get(0);
				
				assembleItem.setDispOrderId(dispOrderItem.getDispOrder().getId());
				
				if ( dispOrderItem.getDispOrder().getStatus() == DispOrderStatus.Deleted ) { 

					updateSuccess = false;
					assembleItem.setReprintFlag(true);
					setSystemMessageContent("0074", assembleItem);
					return assembleItem;
				}
				
				if (FmStatus.FreeTextEntryItem.getDataValue().equals( StringUtils.trimToNull( dispOrderItem.getPharmOrderItem().getMedOrderItem().getFmStatus()) )) {
					updateSuccess = false;
					assembleItem.setReprintFlag(true);
					setSystemMessageContent("0175", assembleItem);
					return assembleItem;
				}
				
				if ( dispOrderItem.getDispOrder().getAdminStatus() == DispOrderAdminStatus.Suspended ) {		
					updateSuccess = false;
					assembleItem.setReprintFlag(true);
					setSystemMessageContent("0059", assembleItem);
					return assembleItem;
				}
				
				if ( dispOrderItem.getPharmOrderItem().getActionStatus() == ActionStatus.DispInClinic || 
						(dispOrderItem.getPharmOrderItem().getActionStatus() == ActionStatus.PurchaseByPatient && dispOrderItem.getDispQty().compareTo(BigDecimal.ZERO) == 0) ) {
					updateSuccess = false;
					assembleItem.setReprintFlag(true);
					setSystemMessageContent("0175", assembleItem);
					return assembleItem;
				}
				
				if ( dispOrderItem.getStatus() == DispOrderItemStatus.Picked ) {	
					dispOrderItem.setStatus(DispOrderItemStatus.Assembled);
					dispOrderItem.setAssembleUser(identity.getCredentials().getUsername());
					dispOrderItem.setAssembleDate(new Date());
					em.merge(dispOrderItem);
					em.flush();

					updateSuccess = true;
					assembleItem.setReprintFlag(true);
					setSystemMessageContent("0073", assembleItem);

					if ( dispOrderItem.getDispOrder().getStatus() == DispOrderStatus.Picked ||  
							dispOrderItem.getDispOrder().getStatus() == DispOrderStatus.Assembling ) {

						List<DispOrderItemStatus> dispOrderItemStatusList = new ArrayList<DispOrderItemStatus>();
						dispOrderItemStatusList.add(DispOrderItemStatus.Vetted);
						dispOrderItemStatusList.add(DispOrderItemStatus.Picked);
						
						int count = ((Long) em.createQuery(
								"select count(o) from DispOrderItem o" + // 20120214 index check : DispOrderItem.dispOrder : FK_DISP_ORDER_ITEM_01
								" where o.dispOrder = :dispOrder" +
								" and o.status in :dispOrderItemStatusList" +
								" and o.pharmOrderItem.medOrderItem.status not in :medOrderItemStatus" +
								" and o.pharmOrderItem.medOrderItem.medOrder.status <>  :medOrderStatus" +
								" and o.pharmOrderItem.medOrderItem.medOrder.orderType = :orderType")
								.setParameter("dispOrder", dispOrderItem.getDispOrder())
								.setParameter("dispOrderItemStatusList", dispOrderItemStatusList)
								.setParameter("medOrderItemStatus", Arrays.asList(MedOrderItemStatus.SysDeleted, MedOrderItemStatus.Deleted))
								.setParameter("medOrderStatus", MedOrderStatus.SysDeleted)
								.setParameter("orderType", OrderType.OutPatient)
								.getSingleResult()).intValue();

						if ( count == 0 ){
						
							DispOrder dispOrder = dispOrderItem.getDispOrder();
							if ("EDS".equals(propMap.getValue(ACTIVE_OPERATION_MODE)) && propMap.getValueAsBoolean(WORKFLOW_EDS_BPM_ENABLED)) 
							{
								dispOrderId = dispOrder.getId();
								Contexts.getSessionContext().set("dispOrderId", dispOrderId);
								dispensingPmsMode.updateToAssembleStatus(dispOrder);
							}  
							else 
							{
								dispOrder.setStatus(DispOrderStatus.Assembled);
							}
							dispOrder.setAssembleDate(new Date());
							
							em.merge(dispOrder);
							em.flush();
							assembleItem.setCompleteAssembling(true);

						} else {
							if ( dispOrderItem.getDispOrder().getStatus() != DispOrderStatus.Assembling) {

								DispOrder dispOrder = dispOrderItem.getDispOrder();
								dispOrder.setStatus(DispOrderStatus.Assembling);
								em.merge(dispOrder);
								em.flush();

							}
						}
					}
				} else {
					updateSuccess = false;
					assembleItem.setReprintFlag(true);
					
					if (dispOrderItem.getStatus() == DispOrderItemStatus.KeepRecord) {
						setSystemMessageContent("0175", assembleItem);
					} else if (dispOrderItem.getStatus() == DispOrderItemStatus.Vetted) {
						setSystemMessageContent("0072", assembleItem);
					} else if ((dispOrderItem.getDispOrder().getStatus() == DispOrderStatus.Assembled) || 
							(dispOrderItem.getDispOrder().getStatus() == DispOrderStatus.Checked) || (dispOrderItem.getDispOrder().getStatus() == DispOrderStatus.Issued)) {
						updateSuccess = true;
						setSystemMessageContent("0073", assembleItem);
						assembleItem.setCompleteAssembling(true);
					} else {
						setSystemMessageContent("0071", assembleItem);
					}
				}
				
				assembleItem.setUrgentFlag(dispOrderItem.getDispOrder().getUrgentFlag());
				
				Map<DocType, List<RefillSchedule>> nextRefillScheduleMap = refillScheduleManager.retrieveNextRefillScheduleList(dispOrderItem.getDispOrder());

				if ( nextRefillScheduleMap.get(DocType.Standard).size() > 0 ) {
					assembleItem.setPrescWithRefill(true);
					
				} else if (nextRefillScheduleMap.get(DocType.Sfi).size() > 0) {
					assembleItem.setPrescWithRefill(false);
					for (RefillSchedule rs: nextRefillScheduleMap.get(DocType.Sfi)) {
						if (rs.getPrintFlag()){
							assembleItem.setPrescWithRefill(true);
							break;
						}
					}
				} else {
					assembleItem.setPrescWithRefill(false);
					
				}
				
			} else {
				
				List<DispOrder> dispOrderList = em.createQuery(
						"select o from DispOrder o" + // 20160615 index check : DispOrder.workstore,orderType,ticketDate,ticketNum : I_DISP_ORDER_15
						" where o.workstore = :workstore" +
						" and o.status <> :dispOrderDeletedStatus"+
						" and o.ticketDate = :ticketDate" +
						" and o.ticketNum = :ticketNum" +
						" and o.orderType = :orderType")
						.setParameter("workstore", workstore)
						.setParameter("dispOrderDeletedStatus", DispOrderStatus.SysDeleted)
						.setParameter("ticketDate", ticketDate, TemporalType.DATE)
						.setParameter("ticketNum", ticketNum)
						.setParameter("orderType", OrderType.OutPatient)
						.getResultList();
				
				if (!dispOrderList.isEmpty()) {
					if (itemNum <= dispOrderList.get(0).getMaxItemNum()){
							updateSuccess = false;
							assembleItem.setDispOrderId(dispOrderList.get(0).getId());
							assembleItem.setReprintFlag(true);
							setSystemMessageContent("0074", assembleItem);
					}else {
						updateSuccess = false;
						assembleItem.setReprintFlag(false);
						setSystemMessageContent("0061", assembleItem);
					}
				}else{
					updateSuccess = false;
					assembleItem.setReprintFlag(false);
					setSystemMessageContent("0061", assembleItem);
				}
			}

		} catch (Exception e) {
			setSystemMessageContent("0076", assembleItem);
			logger.error("Fail to assemble order #0", e);
		}
		
		return assembleItem;
	}

	public boolean isUpdateSuccess() {
		return updateSuccess;
	}

	public String getErrMsg() {
		return errMsg;
	}

	private void setSystemMessageContent(String messageCode, AssembleItem assembleItem){
		StringBuilder messageBuilder = new StringBuilder();
		messageBuilder.append(systemMessageManager.retrieveMessageDesc(messageCode));
		if( !ignoreFastQueueMessageCodeList.contains(messageCode) ){
			if( assembleItem.getFastQueueFlag() ){
				messageBuilder.append(FASTQUEUE_MESSAGE_SUFFIX);
			}
		}
		assembleItem.setMessageCode(messageCode);		
		assembleItem.setMessage(messageBuilder.toString());
	}
	
	private int checkTicketDate(Date ticketDate){
				
		Period period = Period.days(BATCH_DAYEND_ORDER_DURATION.get());
		DateTime endDate = new DateTime();
		DateTime dispDateTime = new DateTime(ticketDate);
		
		DateTime startDate = endDate.minus(period);
		 
		return startDate.toDate().compareTo(dispDateTime.toDate());

	}

	@Remove
	public void destroy() {
		if (dispOrderId != null) {
			dispOrderId = null;
		}
	}

}
