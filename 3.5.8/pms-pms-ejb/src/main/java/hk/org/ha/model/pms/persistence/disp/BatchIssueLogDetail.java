package hk.org.ha.model.pms.persistence.disp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

import flexjson.JSON;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;

@Entity
@Table(name = "BATCH_ISSUE_LOG_DETAIL")
public class BatchIssueLogDetail extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "batchIssueLogDetailSeq")
	@SequenceGenerator(name = "batchIssueLogDetailSeq", sequenceName = "SQ_BATCH_ISSUE_LOG_DETAIL", initialValue = 100000000)
	private Long id;

	@Converter(name = "BatchIssueLogDetail.status", converterClass = DispOrderStatus.Converter.class)
    @Convert("BatchIssueLogDetail.status")
	@Column(name="STATUS", length = 2, nullable = false)
	private DispOrderStatus status;
	
	@Column(name = "ISSUE_FLAG", nullable = false, length = 1)
	private Boolean issueFlag;	
	
	@Column(name = "RESULT_MSG", length = 1000)
	private String resultMsg;

	@Column(name = "SORT_SEQ")
	private Integer sortSeq;
	
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "DISP_ORDER_ID", nullable = false)
    private DispOrder dispOrder;

	@ManyToOne
	@JoinColumn(name = "BATCH_ISSUE_LOG_ID", nullable = false)
	private BatchIssueLog batchIssueLog;

	public BatchIssueLogDetail() {
		status = DispOrderStatus.Vetted;
		issueFlag = Boolean.FALSE;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DispOrderStatus getStatus() {
		return status;
	}

	public void setStatus(DispOrderStatus status) {
		this.status = status;
	}

	public Boolean getIssueFlag() {
		return issueFlag;
	}

	public void setIssueFlag(Boolean issueFlag) {
		this.issueFlag = issueFlag;
	}

	public String getResultMsg() {
		return resultMsg;
	}

	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}

	public Integer getSortSeq() {
		return sortSeq;
	}

	public void setSortSeq(Integer sortSeq) {
		this.sortSeq = sortSeq;
	}

	@JSON(include=false)
	public DispOrder getDispOrder() {
		return dispOrder;
	}

	public void setDispOrder(DispOrder dispOrder) {
		this.dispOrder = dispOrder;
	}

	@JSON(include=false)
	public BatchIssueLog getBatchIssueLog() {
		return batchIssueLog;
	}

	public void setBatchIssueLog(BatchIssueLog batchIssueLog) {
		this.batchIssueLog = batchIssueLog;
	}

}
