package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.persistence.reftable.RefillHoliday;

import java.util.Collection;

import javax.ejb.Local;

@Local
public interface RefillHolidayListServiceLocal {

	void retrieveRefillHolidayList();
	
	void updateRefillHolidayList(Collection<RefillHoliday> updateRefillHolidayList, Collection<RefillHoliday> deleteRefillHolidayList);
	
	void destroy();
	
}
