package hk.org.ha.model.pms.persistence.phs;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Institution;
import hk.org.ha.model.pms.udt.RecordStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "PATIENT_CAT")
@IdClass(PatientCatPK.class)
public class PatientCat extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "INST_CODE", nullable = false)
	private String instCode;
	
	@Id
	@Column(name = "PAT_CAT_CODE", nullable = false, length = 2)
	private String patCatCode;
	
	@Column(name = "DESCRIPTION", length = 50)
	private String description;

    @Converter(name = "PatientCat.status", converterClass = RecordStatus.Converter.class)
    @Convert("PatientCat.status")
	@Column(name="STATUS", nullable = false, length = 1)
	private RecordStatus status;
    
    @ManyToOne
    @JoinColumn(name = "INST_CODE", nullable = false, insertable = false, updatable = false)
    private Institution institution;

    public PatientCat() {
    	status = RecordStatus.Active;
    }
    
    public PatientCat(String instCode, String patCatCode) {
		this();
    	this.instCode = instCode;
    	this.patCatCode = patCatCode;
    }

    public PatientCat(String instCode, String patCatCode, String description, String status) {
		this(instCode, patCatCode);
    	this.description = description;
    	if( "Y".equals(status) ){
    		this.status = RecordStatus.Suspend;
    	}else{
    		this.status = RecordStatus.Active;
    	}
    }    
    
    public String getInstCode() {
		return instCode;
	}
	
	public void setInstCode(String instCode) {
		this.instCode = instCode;
	}

	public String getPatCatCode() {
		return patCatCode;
	}

	public void setPatCatCode(String patCatCode) {
		this.patCatCode = patCatCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public RecordStatus getStatus() {
		return status;
	}

	public void setStatus(RecordStatus status) {
		this.status = status;
	}

	public Institution getInstitution() {
		return institution;
	}
	
	public PatientCatPK getId(){
		return new PatientCatPK(instCode, patCatCode);
	}

	@Override
	public int hashCode() {		
		return new HashCodeBuilder()
			.append(instCode)
			.append(patCatCode)
			.toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		PatientCat other = (PatientCat) obj;
		return new EqualsBuilder()
			.append(instCode, other.getInstCode())
			.append(patCatCode, other.getPatCatCode())
			.isEquals();
	}
}
