package hk.org.ha.model.pms.biz.drug;

import static hk.org.ha.model.pms.prop.Prop.VETTING_REGIMEN_DAILYFREQ_SORT;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmDailyFrequencyCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDailyFrequency;
import hk.org.ha.model.pms.udt.vetting.DailyFreqSort;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("dmDailyFrequencyListManager")
@MeasureCalls
public class DmDailyFrequencyListManagerBean implements DmDailyFrequencyListManagerLocal {

	@Logger
	private Log logger;
	
	@In
	private DmDailyFrequencyCacherInf dmDailyFrequencyCacher;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private List<DmDailyFrequency> dmDailyFrequencyList;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private List<DmDailyFrequency> dmDailyFrequencyWithoutStopList;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private List<DmDailyFrequency> dmDailyFrequencySuspendList;
	
	public void retrieveDmDailyFrequencyList() {
				
		if ( !logger.isDebugEnabled() ) {
			logger.debug("retrieveDmDailyFrequencyList");
		}		
		// set default sort sequence by rule value			
		DailyFreqSort sortSeq = DailyFreqSort.dataValueOf( VETTING_REGIMEN_DAILYFREQ_SORT.get() );				
		
		dmDailyFrequencyList = dmDailyFrequencyCacher.getDmDailyFrequencyList( sortSeq );		
		
		if ( dmDailyFrequencyList != null ) {
			
			dmDailyFrequencyWithoutStopList = new ArrayList<DmDailyFrequency>();
			for ( DmDailyFrequency dmDailyFrequency : dmDailyFrequencyList ) {
				if ( !"00039".equals(dmDailyFrequency.getDailyFreqCode()) ) {
					dmDailyFrequencyWithoutStopList.add(dmDailyFrequency);		
				}
			}
			
			if ( !logger.isDebugEnabled() ) {
				logger.debug("dmDailyFrequencyList size #0", dmDailyFrequencyList.size());
				logger.debug("dmDailyFrequencyWithoutStopList size #0", dmDailyFrequencyWithoutStopList.size());
			}
		}
		
		dmDailyFrequencySuspendList = dmDailyFrequencyCacher.getDmDailyFrequencySuspendList(sortSeq);
	}
}
