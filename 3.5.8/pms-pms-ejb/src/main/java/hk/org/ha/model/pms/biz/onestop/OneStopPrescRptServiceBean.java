package hk.org.ha.model.pms.biz.onestop;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.onestop.MoeFilterOption;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.onestop.OneStopSummary;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("oneStopPrescRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class OneStopPrescRptServiceBean implements OneStopPrescRptServiceLocal {
	
	@In
	private OneStopPrescManagerLocal oneStopPrescManager;
	
	@In
	private Workstore workstore;
	
	@In
	private PrintAgentInf printAgent;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	public void printOSEPrescRptData(Date orderDate, MoeFilterOption moeFilterOption)
	{
		List<OneStopSummary> unvetPrescList = oneStopPrescManager.retrieveUnvetPrescList(orderDate, moeFilterOption, workstore, false);
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("orderDate", orderDate);
		parameters.put("hospCode", workstore.getHospCode());
		parameters.put("workstoreCode", workstore.getWorkstoreCode());
		parameters.put("prescType", moeFilterOption.getDisplayValue());

	    printAgent.renderAndPrint(new RenderAndPrintJob(
	    		"OneStopPrescriptionRpt",
			    new PrintOption(),
			    printerSelector.retrievePrinterSelect(PrintDocType.Report),
			    parameters,
			    unvetPrescList));
	}
	
	@Remove
	public void destroy()
	{

	}
}
