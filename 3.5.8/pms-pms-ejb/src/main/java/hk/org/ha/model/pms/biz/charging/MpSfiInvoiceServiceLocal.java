package hk.org.ha.model.pms.biz.charging;

import hk.org.ha.model.pms.vo.charging.MpSfiInvoice;
import hk.org.ha.model.pms.vo.charging.MpSfiInvoiceItem;
import hk.org.ha.model.pms.vo.charging.MpSfiInvoiceInvoiceListInfo;

import java.util.List;

import javax.ejb.Local;

@Local
public interface MpSfiInvoiceServiceLocal {
	
	MpSfiInvoiceItem createMpSfiInvoiceItem(String itemCode);
	
	MpSfiInvoice retrieveDeliveryItemList(String hkid, String caseNum, Long medProfileId);
	
	void retrievePrivatePatList();
	
	MpSfiInvoice retrieveMpSfiInvoice(String invoiceNum);
	
	List<MpSfiInvoiceInvoiceListInfo> retrieveInvoiceList(String hkid, String caseNum);
	
	void updateMpSfiInvoiceToIssued(MpSfiInvoice mpSfiInvoice);
	
	void saveAndPrintInvoice(MpSfiInvoice mpSfiInvoice);
	
	void reprintMpSfiInvoice();
	
	void clearScreen();
	
	void voidMpSfiInvoice(String invoiceNum, List<Long> deliveryItemIdList);
	
	String getMessageCode();
	
	boolean isSuccess();
	
	void destroy();

}
