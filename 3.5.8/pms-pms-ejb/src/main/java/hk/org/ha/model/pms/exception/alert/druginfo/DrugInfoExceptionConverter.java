package hk.org.ha.model.pms.exception.alert.druginfo;

import hk.org.ha.model.pms.asa.exception.alert.druginfo.DrugInfoException;

import java.util.Map;

import org.granite.messaging.service.ExceptionConverter;
import org.granite.messaging.service.ServiceException;

public class DrugInfoExceptionConverter implements ExceptionConverter {

	public static final String DRUG_INFO_EXCEPTION = "DrugInfoException";
	
	@Override
	public boolean accepts(Throwable t, Throwable finalException) {
		return t.getClass().equals(DrugInfoException.class);
	}

	@Override
	public ServiceException convert(Throwable t, String detail, Map<String, Object> extendedData) {
		ServiceException se = new ServiceException(DRUG_INFO_EXCEPTION, t.getMessage(), detail, t);
		DrugInfoException drugInfoException = (DrugInfoException) t;
		extendedData.put("msgCode", drugInfoException.getMsgCode());
		extendedData.put("errorCode", drugInfoException.getErrorCode());
		extendedData.put("logType", drugInfoException.getLogType());
		se.getExtendedData().putAll(extendedData);
		return se;
	}
}
