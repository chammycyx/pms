package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.List;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class EdsActivityRpt implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private EdsActivityRptHdr edsActivityRptHdr;
	
	private List<EdsActivityRptDtl> edsActivityRptDtlList;

	public EdsActivityRptHdr getEdsActivityRptHdr() {
		return edsActivityRptHdr;
	}

	public void setEdsActivityRptHdr(EdsActivityRptHdr edsActivityRptHdr) {
		this.edsActivityRptHdr = edsActivityRptHdr;
	}

	public List<EdsActivityRptDtl> getEdsActivityRptDtlList() {
		return edsActivityRptDtlList;
	}

	public void setEdsActivityRptDtlList(
			List<EdsActivityRptDtl> edsActivityRptDtlList) {
		this.edsActivityRptDtlList = edsActivityRptDtlList;
	}
}