package hk.org.ha.model.pms.udt.drug;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum SearchType implements StringValuedEnum {

	Basic("B", "Basic Search"),
	AnyWord("A", "Any Word in Drug Name"),
	MatchIngredient("I", "Match Ingredients"),
	TherapeuticClass("T", "Therapeutic Class");
	
    private final String dataValue;
    private final String displayValue;
        
    SearchType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static SearchType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(SearchType.class, dataValue);
	}
        
	public static class Converter extends StringValuedEnumConverter<SearchType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<SearchType> getEnumClass() {
    		return SearchType.class;
    	}
    }
}
