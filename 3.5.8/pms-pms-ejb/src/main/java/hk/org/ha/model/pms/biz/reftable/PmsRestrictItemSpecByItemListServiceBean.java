package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.cache.PasSpecialtyCacherInf;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.PmsRestrictItem;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.model.pms.vo.patient.PasSpecialty;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("pmsRestrictItemSpecByItemListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PmsRestrictItemSpecByItemListServiceBean implements PmsRestrictItemSpecByItemListServiceLocal {
	
	@In
	private AuditLogger auditLogger;
	
	@In
	private PasSpecialtyCacherInf pasSpecialtyCacher;
	
	@Out(required = false)
	private List<PasSpecialty> restrictItemPasSpecialtyList;
	
	@Out(required = false)
	private List<PmsRestrictItem> pmsRestrictItemList;

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;
	
	@In
	private Workstore workstore;

	@Out(required = false)
	private WorkstoreDrug drug;
	
	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;

	public void retrievePasSpecList() {
		restrictItemPasSpecialtyList = pasSpecialtyCacher.getPasSpecialtyWithSubSpecialtyList();
		
		Collections.sort(restrictItemPasSpecialtyList, new Comparator<PasSpecialty>() {
			@Override
			public int compare(PasSpecialty o1, PasSpecialty o2) {
				String type = "OI";
				return new CompareToBuilder()
										.append( type.indexOf(o1.getType().getDataValue()), type.indexOf(o2.getType().getDataValue()) )
										.append( o1.getPasSpecCode(), o2.getPasSpecCode() )
										.toComparison();
			}
		});
	}
	
	public void retrievePmsRestrictItemListByItem(String itemCode, String pmsFmStatus)
	{	
		pmsRestrictItemList = null;
		restrictItemPasSpecialtyList = null;
		
		drug = workstoreDrugCacher.getWorkstoreDrug(workstore, itemCode);
		
		if (drug != null){
			if (drug.getMsWorkstoreDrug() != null){
				if( drug.getDmDrug() != null && "PDF".equals(drug.getDmDrug().getTheraGroup()) ){
					return;
				}
				retrievePasSpecList();
				pmsRestrictItemList = dmsPmsServiceProxy.retrievePmsRestrictItemList(workstore.getWorkstoreGroup().getWorkstoreGroupCode(), itemCode);
				for(PmsRestrictItem pmsRestrictItem:pmsRestrictItemList){
					pmsRestrictItem.setDmDrug( dmDrugCacher.getDmDrug(pmsRestrictItem.getItemCode()) );
				}
			}
		}
	}
	
	public void updatePmsRestrictItemList(List<PmsRestrictItem> newPmsRestrictItemList, String itemCode, String fmStatus)
	{	
		
		for (PmsRestrictItem pmsRestrictItem: newPmsRestrictItemList) {
			pmsRestrictItem.setWorkstoreGroupCode(workstore.getWorkstoreGroup().getWorkstoreGroupCode());
			pmsRestrictItem.setItemCode(itemCode);
			pmsRestrictItem.setPmsFmStatus(fmStatus);
		}
		
		dmsPmsServiceProxy.updatePmsRestrictItemList(newPmsRestrictItemList, workstore.getWorkstoreGroup().getWorkstoreGroupCode(), itemCode);
		createAuditLog(newPmsRestrictItemList, itemCode);
		corpPmsServiceProxy.purgeMsWorkstoreDrug(workstore);
	}
	
	@SuppressWarnings("unchecked")
	private void createAuditLog(List<PmsRestrictItem> newPmsRestrictItemList, String itemCode) 
	{
		List<String> oldList = new ArrayList<String>();
		List<String> newList = new ArrayList<String>();
		
		List<String> deletedItemList = new ArrayList<String>();
		List<String> insertedItemList = new ArrayList<String>();
		
		for (PmsRestrictItem pmsRestrictItem: pmsRestrictItemList) {
			oldList.add(pmsRestrictItem.getSpecialtyType() + "-" + pmsRestrictItem.getPasSpecialty() +"-" +  pmsRestrictItem.getPasSubSpecialty());
		}
		
		for (PmsRestrictItem pmsRestrictItem: newPmsRestrictItemList) {
			newList.add(pmsRestrictItem.getSpecialtyType() + "-" + pmsRestrictItem.getPasSpecialty() +"-" +  pmsRestrictItem.getPasSubSpecialty());
		}

		insertedItemList = (List<String>)CollectionUtils.subtract(newList, oldList);
		deletedItemList = (List<String>)CollectionUtils.subtract(oldList, newList);
		
		StringBuffer insertedString = new StringBuffer();
		StringBuffer deletedString = new StringBuffer();
		
		boolean firstInsertedObj = true;
		for (String pmsRestrictItem: insertedItemList) {
			if (firstInsertedObj) {
				firstInsertedObj = false;
			}else {
				insertedString.append(", ");
			}
			insertedString.append(pmsRestrictItem);
		}
		
		boolean firstDeletedObj = true;
		for (String pmsRestrictItem: deletedItemList) {
			if (firstDeletedObj) {
				firstDeletedObj = false;
			}else {
				deletedString.append(", ");
			}
			deletedString.append(pmsRestrictItem);
		}
		if (!insertedString.toString().isEmpty()){
			auditLogger.log("#0745", itemCode, insertedString.toString());
		}
		
		if (!deletedString.toString().isEmpty()){
			auditLogger.log("#0746", itemCode, deletedString.toString());
		}
	}
	
	@Remove
	public void destroy() 
	{
		if (restrictItemPasSpecialtyList != null) {
			restrictItemPasSpecialtyList = null;
		}
		
		if (pmsRestrictItemList != null) {
			pmsRestrictItemList = null;
		}

	}
}
