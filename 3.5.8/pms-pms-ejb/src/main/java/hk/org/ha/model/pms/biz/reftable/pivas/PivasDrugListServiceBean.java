package hk.org.ha.model.pms.biz.reftable.pivas;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.cache.DrugSiteMappingCacherInf;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.corp.cache.DmSiteCacherInf;
import hk.org.ha.model.pms.corp.cache.DmWarningCacherInf;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.Company;
import hk.org.ha.model.pms.dms.persistence.DmDiluent;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmDrugReconstitution;
import hk.org.ha.model.pms.dms.persistence.DmSite;
import hk.org.ha.model.pms.dms.persistence.DmWarning;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrug;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrugManuf;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrugManufConfig;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrugManufMethod;
import hk.org.ha.model.pms.dms.vo.DrugRouteCriteria;
import hk.org.ha.model.pms.dms.vo.DrugSiteMapping;
import hk.org.ha.model.pms.dms.vo.pivas.PivasDrugValidateCriteria;
import hk.org.ha.model.pms.dms.vo.pivas.PivasDrugValidateResponse;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.reftable.pivas.PivasDrugInfo;
import hk.org.ha.model.pms.vo.reftable.pivas.PivasDrugListInfo;
import hk.org.ha.model.pms.vo.report.PivasDrugRpt;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("pivasDrugListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PivasDrugListServiceBean implements PivasDrugListServiceLocal {
	
	@In
	private Workstore workstore;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

	@In
	private PrintAgentInf printAgent; 

	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private DmWarningCacherInf dmWarningCacher;	

	@In
	private DrugSiteMappingCacherInf drugSiteMappingCacher;
	
	@In
	private DmSiteCacherInf dmSiteCacher;

	private List<PivasDrugRpt> pivasDrugRptList;

	@Override
	public DmDrug retrieveDrugByItemCode(String itemCode) {
		WorkstoreDrug workstoreDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, itemCode);
		
		if (workstoreDrug == null || workstoreDrug.getMsWorkstoreDrug() == null) {
			return null;
		}
		
		return workstoreDrug.getDmDrug();
	}
	
	@Override
	public PivasDrugListInfo retrievePivasDrugListInfo(DmDrug dmDrug) {
		List<WorkstoreDrug> workstoreDrugList = new ArrayList<WorkstoreDrug>();
		
		List<WorkstoreDrug> tempWorkstoreDrugList = workstoreDrugCacher.getWorkstoreDrugListByDrugKey(workstore, dmDrug.getDrugKey());
		for (WorkstoreDrug tempWorkstoreDrug : tempWorkstoreDrugList) {
			if (tempWorkstoreDrug.getDmDrug().getDmMoeProperty().getMoDosageUnit() == null) {
				// note: when strength compul = Y, items of same drug key can have different MODU, so it is needed to
				//       filter out items without MODU
				continue;
			}
			workstoreDrugList.add(tempWorkstoreDrug);
		}
		
		List<PivasDrug> pivasDrugList = dmsPmsServiceProxy.retrievePivasDrugList(workstore.getWorkstoreGroup().getWorkstoreGroupCode(), dmDrug.getDrugKey(), workstore.getHospCode());
		
		List<DmDiluent> dmDiluentList = dmsPmsServiceProxy.retrieveDmDiluentList(false, false, false);
		
		List<DmWarning> dmWarningList = new ArrayList<DmWarning>();
		List<DmWarning> allDmWarningList = dmWarningCacher.getDmWarningList();
		for (DmWarning dmWarning : allDmWarningList) {
			if ( "ST".equals(dmWarning.getWarnCatCode()) ) {
				dmWarningList.add(dmWarning);
			}
		}
		
		List<String> itemCodeList = new ArrayList<String>();
		for (WorkstoreDrug workstoreDrug : workstoreDrugList) {
			itemCodeList.add(workstoreDrug.getDmDrug().getItemCode());
		}
		
		Map<String, List<Company>> companyListMap = dmsPmsServiceProxy.retrieveCompanyListMapByItemCode(itemCodeList);

		Map<String, List<DmDrugReconstitution>> dmDrugReconstitutionListMap = dmsPmsServiceProxy.retrieveDmDrugReconstitutionListMapByItemCode(itemCodeList);
		
		
		// init PIVAS drug
		List<PivasDrugInfo> pivasDrugInfoList = new ArrayList<PivasDrugInfo>();
		
		Map<String, PivasDrug> pivasDrugByItemCodeMap = new HashMap<String, PivasDrug>();
		for (PivasDrug pivasDrug : pivasDrugList) {
			pivasDrugByItemCodeMap.put(pivasDrug.getItemCode(), pivasDrug);
		}

		Map<String, DmDiluent> dmDiluentMap = new HashMap<String, DmDiluent>();
		for (DmDiluent dmDiluent : dmDiluentList) {
			dmDiluentMap.put(dmDiluent.getDiluentCode(), dmDiluent);
		}
		
		for (WorkstoreDrug workstoreDrug : workstoreDrugList) {
			PivasDrugInfo pivasDrugInfo = new PivasDrugInfo();
			
			pivasDrugInfo.setMsWorkstoreDrug(workstoreDrug.getMsWorkstoreDrug());
			
			if ( pivasDrugByItemCodeMap.containsKey(workstoreDrug.getDmDrug().getItemCode()) ) {
				PivasDrug pivasDrug = pivasDrugByItemCodeMap.get(workstoreDrug.getDmDrug().getItemCode());
				
				for (PivasDrugManuf pivasDrugManuf : pivasDrug.getPivasDrugManufList()) {
					// backup manufacturer status
					pivasDrugManuf.setPrevStatus(pivasDrugManuf.getStatus());
					
					if (pivasDrugManuf.getRequireSolventFlag()) {
						for (PivasDrugManufMethod pivasDrugManufMethod : pivasDrugManuf.getPivasDrugManufMethodList()) {
							// convert site code list to DmSite list for front-end use
							List<DmSite> dmSiteList = new ArrayList<DmSite>();
							for (String siteCode : pivasDrugManufMethod.getSiteCodes()) {
								dmSiteList.add(dmSiteCacher.getSiteBySiteCode(siteCode));
							}
							pivasDrugManufMethod.setDmSiteList(dmSiteList);
							pivasDrugManufMethod.setSiteCodes(null);
						}
					}
				}
					
				pivasDrugInfo.setPivasDrug(pivasDrug);
				
			} else {
				PivasDrug pivasDrug = new PivasDrug();
				pivasDrug.setWorkstoreGroupCode(workstore.getWorkstoreGroup().getWorkstoreGroupCode());
				pivasDrug.setItemCode(workstoreDrug.getDmDrug().getItemCode());
				pivasDrug.setDrugKey(workstoreDrug.getDmDrug().getDrugKey());
				pivasDrug.setMoDosageUnit(workstoreDrug.getDmDrug().getDmMoeProperty().getMoDosageUnit());
				pivasDrug.setDiluentCode(workstoreDrug.getDmDrug().getDmMoeProperty().getDiluentCode());
				
				pivasDrug.setPivasDosageUnit(workstoreDrug.getDmDrug().getDmMoeProperty().getMoDosageUnit());	// default value
				
				pivasDrug.setPivasDrugManufList(new ArrayList<PivasDrugManuf>());

				if (pivasDrug.getDiluentCode() != null) {
					DmDiluent dmDiluent = dmDiluentMap.get(pivasDrug.getDiluentCode());
					pivasDrug.setDmDiluent(dmDiluent);
				}
				
				pivasDrugInfo.setPivasDrug(pivasDrug);
			}
			
			pivasDrugInfo.getPivasDrug().setDmDrug(workstoreDrug.getDmDrug());
					
			if (pivasDrugInfo.getPivasDrug().getDmDiluent() != null) {
				if ( "Y".equals(pivasDrugInfo.getPivasDrug().getDmDiluent().getDiluentFlag()) ) {
					pivasDrugInfo.getPivasDrug().setEnableDiluentActionTypeFlag(Boolean.TRUE);
				}
				if ( "Y".equals(pivasDrugInfo.getPivasDrug().getDmDiluent().getSolventFlag()) ) {
					pivasDrugInfo.getPivasDrug().setEnableSolventActionTypeFlag(Boolean.TRUE);
				}
			}
			
			pivasDrugInfo.setCompanyList(companyListMap.get(workstoreDrug.getDmDrug().getItemCode()));
			
			pivasDrugInfo.setWhiteDmSiteList(retrieveWhiteDmSiteList(workstore.getHospCode(), workstore.getWorkstoreCode(), workstoreDrug.getDmDrug().getItemCode()));

			pivasDrugInfo.setGreyDmSiteList(retrieveGreyDmSiteList(workstore.getHospCode(), workstore.getWorkstoreCode(), workstoreDrug.getDmDrug().getItemCode()));
			
			pivasDrugInfo.setDmDrugReconstitutionList(dmDrugReconstitutionListMap.get(workstoreDrug.getDmDrug().getItemCode()));
			
			List<DmDiluent> pivasSolventList = new ArrayList<DmDiluent>();
			for (DmDiluent dmDiluent : dmDiluentList) {
				if ( "Y".equals(dmDiluent.getSolventFlag()) && dmDiluent.getPivasDiluentDesc() != null ) {
					pivasSolventList.add(dmDiluent);
				}
			}
			pivasDrugInfo.setPivasSolventList(pivasSolventList);
			
			pivasDrugInfoList.add(pivasDrugInfo);
		}

		sortPivasDrugInfoList(pivasDrugInfoList);
		
		PivasDrugListInfo pivasDrugListInfo = new PivasDrugListInfo();
		pivasDrugListInfo.setSelectItemCode(dmDrug.getItemCode());
		pivasDrugListInfo.setPivasDrugInfoList(pivasDrugInfoList);
		pivasDrugListInfo.setDmWarningList(dmWarningList);
		
		return pivasDrugListInfo;
	}
	
	private List<DmSite> retrieveWhiteDmSiteList(String hospCode, String workstoreCode, String itemCode) {
		DrugRouteCriteria drugRouteCriteria = new DrugRouteCriteria();
		drugRouteCriteria.setItemCode(itemCode);
		drugRouteCriteria.setHqFlag(Boolean.TRUE);
		List<DrugSiteMapping> drugSiteMappingList = drugSiteMappingCacher.getDrugSiteMappingListByDrugRouteCriteria(drugRouteCriteria);
		
		List<DmSite> whiteDmSiteList = new ArrayList<DmSite>();
		
		for (DrugSiteMapping drugSiteMapping : drugSiteMappingList) {
			if ( drugSiteMapping.getGreyListLevel() == null && "Y".equals(drugSiteMapping.getParenteralSite()) ) {
				DmSite dmSite = dmSiteCacher.getSiteBySiteCode(drugSiteMapping.getSiteCode());
				whiteDmSiteList.add(dmSite);
			}
		}
		
		return whiteDmSiteList;
	}

	private List<DmSite> retrieveGreyDmSiteList(String hospCode, String workstoreCode, String itemCode) {
		DrugRouteCriteria drugRouteCriteria = new DrugRouteCriteria();
		drugRouteCriteria.setItemCode(itemCode);
		drugRouteCriteria.setHqFlag(Boolean.TRUE);
		List<DrugSiteMapping> drugSiteMappingList = drugSiteMappingCacher.getDrugSiteMappingListByDrugRouteCriteria(drugRouteCriteria);
		
		List<DmSite> greyDmSiteList = new ArrayList<DmSite>();
		
		for (DrugSiteMapping drugSiteMapping : drugSiteMappingList) {
			if ( drugSiteMapping.getGreyListLevel() != null && "Y".equals(drugSiteMapping.getParenteralSite()) ) {
				DmSite dmSite = dmSiteCacher.getSiteBySiteCode(drugSiteMapping.getSiteCode());
				greyDmSiteList.add(dmSite);
			}
		}
		
		return greyDmSiteList;
	}
	
	private void sortPivasDrugInfoList(List<PivasDrugInfo> pivasDrugInfoList) {
		for (PivasDrugInfo pivasDrugInfo : pivasDrugInfoList) {
			for (PivasDrugManuf pivasDrugManuf : pivasDrugInfo.getPivasDrug().getPivasDrugManufList()) {
				if (pivasDrugManuf.getPivasDrugManufMethodList() != null) {
					Collections.sort(pivasDrugManuf.getPivasDrugManufMethodList(), new PivasDrugManufMethodComparator());
				}
			}
			
			Collections.sort(pivasDrugInfo.getPivasDrug().getPivasDrugManufList(), new PivasDrugManufComparator());
		}
	}

	public List<PivasDrug> retrievePivasDrugListBySolventCode(String solventCode) {
		List<PivasDrug> pivasDrugList = new ArrayList<PivasDrug>();
		
		List<PivasDrug> dmsPivasDrugList = dmsPmsServiceProxy.retrievePivasDrugListBySolventCode(workstore.getWorkstoreGroup().getWorkstoreGroupCode(), solventCode);
		
		for (PivasDrug pivasDrug : dmsPivasDrugList) {
			WorkstoreDrug workstoreDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, pivasDrug.getItemCode());
			
			if (workstoreDrug.getMsWorkstoreDrug() != null && "A".equals(workstoreDrug.getMsWorkstoreDrug().getItemStatus()) ) {
				pivasDrug.setDmDrug( workstoreDrug.getDmDrug() );
				pivasDrugList.add(pivasDrug);
			}
		}
		
		return pivasDrugList;
	}
	
	public PivasDrugValidateResponse updatePivasDrugList(List<PivasDrug> pivasDrugList, PivasDrugValidateCriteria pivasDrugValidateCriteria) {
		for (PivasDrug pivasDrug : pivasDrugList) {
			for (PivasDrugManuf pivasDrugManuf : pivasDrug.getPivasDrugManufList()) {
				for (PivasDrugManufMethod pivasDrugManufMethod : pivasDrugManuf.getPivasDrugManufMethodList()) {
					if (pivasDrugManufMethod.getDmSiteList() != null) {
						// convert siteCodes from DmSite list for entity save
						List<String> siteCodeList = new ArrayList<String>();
						for (DmSite dmSite : pivasDrugManufMethod.getDmSiteList()) {
							siteCodeList.add(dmSite.getSiteCode());
						}
						pivasDrugManufMethod.setSiteCodes(siteCodeList);
					}
				}
			}
		}
		
		return dmsPmsServiceProxy.updatePivasDrugList(pivasDrugList, pivasDrugValidateCriteria, workstore.getHospCode());
	}
	
	public Boolean retrievePivasDrugRptList() {
		pivasDrugRptList = new ArrayList<PivasDrugRpt>();
		
		List<PivasDrug> pivasDrugList = dmsPmsServiceProxy.retrievePivasDrugListForExport(workstore.getWorkstoreGroup().getWorkstoreGroupCode());
		
		for (PivasDrug pivasDrug : pivasDrugList) {
			for (PivasDrugManuf pivasDrugManuf : pivasDrug.getPivasDrugManufList()) {
				for (PivasDrugManufConfig pivasDrugManufConfig : pivasDrugManuf.getPivasDrugManufConfigList()) {
					for (PivasDrugManufMethod pivasDrugManufMethod : pivasDrugManuf.getPivasDrugManufMethodList()) {
						PivasDrugRpt pivasDrugRpt = new PivasDrugRpt();

						DmDrug dmDrug = DmDrugCacher.instance().getDmDrug(pivasDrug.getItemCode());
						
						pivasDrugRpt.setWorkstoreGroupCode(pivasDrug.getWorkstoreGroupCode());
						pivasDrugRpt.setPivasDrugStatus(pivasDrug.getStatus().getDisplayValue());
						pivasDrugRpt.setPivasDrugManufStatus(pivasDrugManuf.getStatus().getDisplayValue());
						pivasDrugRpt.setDrugKey(pivasDrug.getDrugKey());
						pivasDrugRpt.setItemCode(pivasDrug.getItemCode());
						pivasDrugRpt.setDmDrugFullDrugDesc(dmDrug.getFullDrugDesc());
						pivasDrugRpt.setPivasDosageUnit(pivasDrug.getPivasDosageUnit());
						if (pivasDrug.getDmDiluent() != null) {
							pivasDrugRpt.setDiluentDesc(pivasDrug.getDmDiluent().getPivasDiluentDesc());
						}
						pivasDrugRpt.setDiluentActionType(pivasDrug.getDiluentActionType().getDisplayValue());
						pivasDrugRpt.setSolventActionType(pivasDrug.getSolventActionType().getDisplayValue());
						pivasDrugRpt.setManufacturerCode(pivasDrugManuf.getManufacturerCode());
						pivasDrugRpt.setRequireSolventFlag(pivasDrugManuf.getRequireSolventFlag());
						pivasDrugRpt.setWarnCode1(pivasDrugManuf.getWarnCode1());
						pivasDrugRpt.setWarnCode2(pivasDrugManuf.getWarnCode2());
						pivasDrugRpt.setDispHospCode(pivasDrugManufConfig.getDispHospCode());
						pivasDrugRpt.setBatchNum(pivasDrugManufConfig.getBatchNum());
						pivasDrugRpt.setExpireDate(pivasDrugManufConfig.getExpireDate());
						pivasDrugRpt.setSatelliteBatchNum(pivasDrugManufConfig.getSatelliteBatchNum());
						pivasDrugRpt.setSatelliteExpireDate(pivasDrugManufConfig.getSatelliteExpireDate());
						if (pivasDrugManufMethod.getDmDiluent() != null) {
							pivasDrugRpt.setSolventDesc(pivasDrugManufMethod.getDmDiluent().getPivasDiluentDesc());
						}
						pivasDrugRpt.setSolventItemCode(pivasDrugManufMethod.getItemCode());
						pivasDrugRpt.setDosageQty(pivasDrugManufMethod.getDosageQty());
						pivasDrugRpt.setVolume(pivasDrugManufMethod.getVolume());
						pivasDrugRpt.setAfterVolume(pivasDrugManufMethod.getAfterVolume());
						pivasDrugRpt.setAfterConcn(pivasDrugManufMethod.getAfterConcn());
						if ( pivasDrugManufMethod.getSiteCodes() != null && ! pivasDrugManufMethod.getSiteCodes().isEmpty() ) {
							pivasDrugRpt.setSiteCodes( StringUtils.join(pivasDrugManufMethod.getSiteCodes().toArray(), ",") );
						}
						pivasDrugRpt.setPivasDrugId(pivasDrug.getId());
						pivasDrugRpt.setPivasDrugUpdateUser(pivasDrug.getUpdateUser());
						pivasDrugRpt.setPivasDrugUpdateDate(pivasDrug.getUpdateDate());
						pivasDrugRpt.setPivasDrugManufId(pivasDrugManuf.getId());
						pivasDrugRpt.setPivasDrugManufUpdateUser(pivasDrugManuf.getUpdateUser());
						pivasDrugRpt.setPivasDrugManufUpdateDate(pivasDrugManuf.getUpdateDate());
						pivasDrugRpt.setPivasDrugManufMethodId(pivasDrugManufMethod.getId());
						pivasDrugRpt.setPivasDrugManufMethodStatus(pivasDrugManufMethod.getStatus().getDisplayValue());
						pivasDrugRpt.setPivasDrugManufMethodUpdateUser(pivasDrugManufMethod.getUpdateUser());
						pivasDrugRpt.setPivasDrugManufMethodUpdateDate(pivasDrugManufMethod.getUpdateDate());
						
						pivasDrugRptList.add(pivasDrugRpt);
					}
				}
			}
		}
		
		Collections.sort(pivasDrugRptList, new PivasDrugRptComparator());
		
		if ( pivasDrugRptList.isEmpty() ) {
			return Boolean.FALSE;
		} else{
			return Boolean.TRUE;
		}
	}

	public void exportPivasDrugRpt() {
		PrintOption printOption = new PrintOption();
		printOption.setDocType(ReportProvider.XLS);
		printAgent.renderAndRedirect(new RenderJob(
				"PivasDrugXls", 
				printOption, 
				new HashMap<String, Object>(), 
				pivasDrugRptList));
	}
	
	@Remove
	public void destroy() {
	}
	
	private static class PivasDrugManufComparator implements Comparator<PivasDrugManuf>, Serializable
    {
		private static final long serialVersionUID = 1L;

		@Override
		public int compare(PivasDrugManuf o1, PivasDrugManuf o2) {
			return new CompareToBuilder()
						.append( o1.getManufacturerCode(), o2.getManufacturerCode() )
						.toComparison();
		}
    }
	
	private static class PivasDrugManufMethodComparator implements Comparator<PivasDrugManufMethod>, Serializable
    {
		private static final long serialVersionUID = 1L;

		@Override
		public int compare(PivasDrugManufMethod o1, PivasDrugManufMethod o2) {
			StringBuilder allIpmoeDescSb1 = new StringBuilder("");
			for (DmSite dmSite : o1.getDmSiteList()) {
				allIpmoeDescSb1.append(dmSite.getIpmoeDesc());
			}

			StringBuilder allIpmoeDescSb2 = new StringBuilder("");
			for (DmSite dmSite : o2.getDmSiteList()) {
				allIpmoeDescSb2.append(dmSite.getIpmoeDesc());
			}
			
			return new CompareToBuilder()
						.append( allIpmoeDescSb1.toString(), allIpmoeDescSb2.toString() )
						.toComparison();
		}
    }
	
	private static class PivasDrugRptComparator implements Comparator<PivasDrugRpt>, Serializable
    {
		private static final long serialVersionUID = 1L;

		@Override
		public int compare(PivasDrugRpt o1, PivasDrugRpt o2) {
			return new CompareToBuilder()
						.append( o1.getWorkstoreGroupCode(), o2.getWorkstoreGroupCode() )
						.append( o1.getDrugKey(), o2.getDrugKey() )
						.append( o1.getItemCode(), o2.getItemCode() )
						.append( o1.getManufacturerCode(), o2.getManufacturerCode() )
						.append( o1.getPivasDrugManufMethodStatus(), o2.getPivasDrugManufMethodStatus() )
						.append( o1.getAfterConcn(), o2.getAfterConcn() )
						.toComparison();
		}
    }
}



