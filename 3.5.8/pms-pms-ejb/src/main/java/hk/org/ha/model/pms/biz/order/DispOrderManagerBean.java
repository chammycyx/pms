package hk.org.ha.model.pms.biz.order;

import static hk.org.ha.model.pms.prop.Prop.CDDH_LEGACY_PERSISTENCE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.CHECK_HOSTNAME;
import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_LARGEFONT_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_LARGELAYOUT_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_PRINTING_MODE;
import static hk.org.ha.model.pms.prop.Prop.PICKING_AUTO_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.PICKING_ONCE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.VETTING_URGENTORDER_STATUS;
import hk.org.ha.fmk.pms.cache.CacheResult;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.label.ChestLabelBuilderLocal;
import hk.org.ha.model.pms.biz.label.DispLabelBuilderLocal;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrintLabelManagerLocal;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.biz.refill.RefillCouponRendererLocal;
import hk.org.ha.model.pms.biz.refill.SfiRefillScheduleListManagerLocal;
import hk.org.ha.model.pms.biz.reftable.RefTableManagerLocal;
import hk.org.ha.model.pms.biz.reftable.WorkstationManagerLocal;
import hk.org.ha.model.pms.biz.report.InfusionInstructionSummaryManagerLocal;
import hk.org.ha.model.pms.biz.ticketgen.TicketManagerLocal;
import hk.org.ha.model.pms.biz.vetting.CapdVoucherManagerLocal;
import hk.org.ha.model.pms.biz.vetting.MedOrderItemComparator;
import hk.org.ha.model.pms.cache.DmSpecialtyMoMappingCacherInf;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.corp.vo.EndVettingResult;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmSpecialtyMoMapping;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.corp.WorkstorePK;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.persistence.disp.RefillScheduleItem;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.persistence.reftable.ChargeSpecialty;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.printing.PrintType;
import hk.org.ha.model.pms.udt.refill.DocType;
import hk.org.ha.model.pms.udt.refill.RefillScheduleRefillStatus;
import hk.org.ha.model.pms.udt.refill.RefillScheduleStatus;
import hk.org.ha.model.pms.udt.reftable.ChargeSpecialtyType;
import hk.org.ha.model.pms.udt.reftable.OperationProfileType;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.udt.vetting.RegimenDurationUnit;
import hk.org.ha.model.pms.udt.vetting.RegimenType;
import hk.org.ha.model.pms.vo.alert.mds.ErrorInfo;
import hk.org.ha.model.pms.vo.checkissue.DispInfo;
import hk.org.ha.model.pms.vo.checkissue.DispItemInfo;
import hk.org.ha.model.pms.vo.label.ChestLabel;
import hk.org.ha.model.pms.vo.label.DispLabel;
import hk.org.ha.model.pms.vo.printing.PrintLabelDetail;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.IvAdditive;
import hk.org.ha.model.pms.vo.security.PropMap;
import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;
import hk.org.ha.model.pms.workflow.DispensingPmsModeLocal;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;
import org.jboss.seam.security.Identity;
import org.joda.time.DateTime;
import org.joda.time.Days;

@AutoCreate
@Stateless
@Name("dispOrderManager")
@MeasureCalls
public class DispOrderManagerBean implements DispOrderManagerLocal {

	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private Identity identity;

	@In
	private PrintLabelManagerLocal printLabelManager;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private DispLabelBuilderLocal dispLabelBuilder;
	
	@In
	private ChestLabelBuilderLocal chestLabelBuilder;
	
	@In
	private RefTableManagerLocal refTableManager;

	@In
	private WorkstationManagerLocal workstationManager;
	
	@In
	private TicketManagerLocal ticketManager;
	
	@In
	private PrintAgentInf printAgent;
	
	@In
	private Workstore workstore;
		
	@In
	private OperationProfile activeProfile;

	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
	
	@In
	private DispensingPmsModeLocal dispensingPmsMode;
	
	@In
	private PropMap propMap;
	
	@In
	private DmSpecialtyMoMappingCacherInf dmSpecialtyMoMappingCacher;
	
	@In
	private RefillCouponRendererLocal refillCouponRenderer;
	
	@In
	private CapdVoucherManagerLocal capdVoucherManager;
	
	@In
	private InfusionInstructionSummaryManagerLocal infusionInstructionSummaryManager;

	@In
	private SfiRefillScheduleListManagerLocal sfiRefillScheduleListManager;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private Long dispOrderId;

	private final static String ACTIVE_OPERATION_MODE = "active.operation.mode";
	private final static String WORKFLOW_EDS_BPM_ENABLED = "workflow.eds.bpm.enabled";

	private DateFormat df = new SimpleDateFormat("ddMMyyyy"); 
		
	@SuppressWarnings("unchecked")
	public DispOrder retrieveDispOrderByTicket(Date ticketDate, String ticketNum, Workstore workstore) {
		List<DispOrder> dispOrderList = em.createQuery(
				"select o from DispOrder o " + // 20120214 index check : Ticket.workstore,orderType,ticketDate,ticketNum : UI_TICKET_01
				"where o.workstore = :workstore " +
				"and o.ticket.ticketDate = :ticketDate " +
				"and o.ticket.ticketNum = :ticketNum " +
				"and o.status <> :dispOrderStatus " +
				"and o.pharmOrder.medOrder.orderType = :orderType " +
				"and o.ticket.orderType = :ticketOrderType ")
				.setParameter("workstore", workstore)
				.setParameter("ticketDate", ticketDate, TemporalType.DATE)
				.setParameter("ticketNum", ticketNum)
				.setParameter("dispOrderStatus", DispOrderStatus.SysDeleted)
				.setParameter("orderType", OrderType.OutPatient)
				.setParameter("ticketOrderType", OrderType.OutPatient)
				.getResultList();

		if ( !dispOrderList.isEmpty() ) {
			dispOrderList.get(0).loadChild();
			return dispOrderList.get(0);
		}
		
		return null;
	}
	
	public List<MedOrderItem> retrieveMedOrderItemList(DispOrder dispOrder) {
		List<MedOrderItem> medOrderItemList = new ArrayList<MedOrderItem>();
		if (dispOrder.getRefillFlag())
		{
			for ( DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList() ) {
				PharmOrderItem poi = dispOrderItem.getPharmOrderItem();
				MedOrderItem moi = dispOrderItem.getPharmOrderItem().getMedOrderItem();
				
				poi.loadDmInfo();
				moi.loadDmInfo();
				//replace dose unit if refill
				updatePharmOrderIssueQty(dispOrderItem);
				
				if ( !medOrderItemList.contains(moi)) {
					medOrderItemList.add(moi);
				}
			}
		}
		else
		{
			medOrderItemList = dispOrder.getPharmOrder().getMedOrder().getMedOrderItemList();		
			for ( DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList() ) {
				PharmOrderItem poi = dispOrderItem.getPharmOrderItem();
				MedOrderItem moi = dispOrderItem.getPharmOrderItem().getMedOrderItem();
				
				poi.loadDmInfo();
				moi.loadDmInfo();
				//replace dose unit if refill
				updatePharmOrderIssueQty(dispOrderItem);
			}
		}
				
		Collections.sort(medOrderItemList, new MedOrderItemComparator(dispOrder.getPharmOrder().getMedOrder().isDhOrder())); // sort MedOrderItem				
		return medOrderItemList;
	}
	
	private void updatePharmOrderIssueQty(DispOrderItem dispOrderItem)
	{
		if (!dispOrderItem.getPharmOrderItem().getRefillScheduleItemList().isEmpty()) {
			dispOrderItem.getPharmOrderItem().getRegimen().getDoseGroupList().get(0).setDuration(dispOrderItem.getDurationInDay());
			dispOrderItem.getPharmOrderItem().getRegimen().getDoseGroupList().get(0).setDurationUnit(RegimenDurationUnit.Day);
		}
				
		dispOrderItem.getPharmOrderItem().setIssueQty(dispOrderItem.getDispQty());
	}
	
	@SuppressWarnings("unchecked")
	public void retrieveRefillScheduleList(DispOrder dispOrder) {
		List<RefillSchedule> refillScheduleList = em.createQuery(
				"select o from RefillSchedule o" + // 20120831 index check : RefillSchedule.dispOrder : FK_REFILL_SCHEDULE_02
				" where o.dispOrder = :dispOrder" +
				" and o.docType = :docType")
				.setParameter("dispOrder", dispOrder)
				.setParameter("docType", DocType.Standard)
				.getResultList();
		
		for (RefillSchedule refillSchedule : refillScheduleList) {
			refillSchedule.loadChild();
		}
	}
	
	public List<MedOrderItem> retrievePrescItemListForOneStop(Date ticketDate, String ticketNum, String workstoreCode) {
		DispOrder dispOrder = retrieveDispOrderByTicket(ticketDate, ticketNum, 
				em.find(Workstore.class, new WorkstorePK(workstore.getHospCode(), workstoreCode)));
		
		if (dispOrder != null) {
			retrieveRefillScheduleList(dispOrder);
			em.clear();
			return retrieveMedOrderItemList(dispOrder);
		}
		
		return null;
	}
	
	public DispOrder retrieveDispOrderById(Long id) {
		return em.find(DispOrder.class, id);
	}

	public DispOrder createDispOrderFromPharmOrderForLabel(PharmOrder pharmOrder) {		
		return createDispOrderForInitialRefill(pharmOrder, pharmOrder.getRefillScheduleList(DocType.Standard), pharmOrder.getMedOrder().getTicket());
	}
	
	public DispOrder createDispOrderFromPharmOrder(PharmOrder pharmOrder, Ticket ticket) { // no refill
		List<PharmOrderItem> poiList = new ArrayList<PharmOrderItem>();
		for (MedOrderItem moi : pharmOrder.getMedOrder().getMedOrderItemList()) {
			poiList.addAll(moi.getPharmOrderItemList());
		}
		for (PharmOrderItem poi : poiList) {
			if (poi.getPharmOrder() == null) {
				poi.setPharmOrder(pharmOrder);
			}
		}
		return createDispOrder(pharmOrder, poiList, ticket, false, false);
	}

	public DispOrder createDispOrderForSfiRefill(List<RefillSchedule> inRefillScheduleList, Ticket ticket){		
		return createDispOrderFromRefillScheduleList(inRefillScheduleList.get(0).getPharmOrder(), inRefillScheduleList, ticket, false, false);
	}
	
	public DispOrder createDispOrderForRefill(RefillSchedule refillSchedule, Ticket ticket) {
		boolean isForceCompleteRefill = ( refillSchedule.getStatus() == RefillScheduleStatus.ForceComplete );
		return createDispOrderFromRefillScheduleList(refillSchedule.getPharmOrder(), Arrays.asList(refillSchedule), ticket, false, isForceCompleteRefill);
	}

	public DispOrder createDispOrderForInitialRefill(PharmOrder pharmOrder, List<RefillSchedule> inRefillScheduleList, Ticket ticket) {
	
		// current RS list
		List<RefillSchedule> currentRefillScheduleList = new ArrayList<RefillSchedule>();
	
		for (RefillSchedule refillSchedule : inRefillScheduleList) {
			if (refillSchedule.getRefillStatus() == RefillScheduleRefillStatus.Current) {
				currentRefillScheduleList.add(refillSchedule);
			}
		}
	
		DispOrder dispOrder = createDispOrderFromRefillScheduleList(pharmOrder, currentRefillScheduleList, ticket, true, false);
	
		// set refill flag
		dispOrder.setRefillFlag(Boolean.FALSE);

		return dispOrder;
	}

	private DispOrder createDispOrderFromRefillScheduleList(PharmOrder pharmOrder, List<RefillSchedule> refillScheduleList, Ticket ticket, boolean isInitRefill, boolean isForceCompleteRefill) { // refill
		
		// POI list for create DO
		List<PharmOrderItem> pharmOrderItemList = new ArrayList<PharmOrderItem>();

		// POI from RS list
		Set<PharmOrderItem> poiSetFromRefill = new HashSet<PharmOrderItem>();
		Map<PharmOrderItem, RefillScheduleItem> refillItemMap = new HashMap<PharmOrderItem, RefillScheduleItem>();
		for (RefillSchedule refillSchedule : refillScheduleList) {
			for (RefillScheduleItem refillScheduleItem : refillSchedule.getRefillScheduleItemList()) {
				PharmOrderItem poi = pharmOrder.getPharmOrderItemByOrgItemNum(refillScheduleItem.getItemNum());
				if (!poiSetFromRefill.contains(poi)) {
					poiSetFromRefill.add(poi);
					refillItemMap.put(poi, refillScheduleItem);
				}
			}
		}

		if( isInitRefill ){
			for (MedOrderItem moi : pharmOrder.getMedOrder().getMedOrderItemList()) {
				pharmOrderItemList.addAll(moi.getPharmOrderItemList());
			}
		}
		else {
			if( DocType.Standard == refillScheduleList.get(0).getDocType() ){
				pharmOrderItemList.addAll(pharmOrder.getPharmOrderItemList());
			}else{
				for (MedOrderItem moi : pharmOrder.getMedOrder().getMedOrderItemList()) {
					for (PharmOrderItem poi : moi.getPharmOrderItemList()) {
						if (poiSetFromRefill.contains(poi)) {
							pharmOrderItemList.add(poi);
						}
					}
				}
			}
		}

		DispOrder dispOrder = createDispOrder(pharmOrder, pharmOrderItemList, ticket, !isInitRefill, isForceCompleteRefill);

		if( !isInitRefill ){
			// set refill flag
			dispOrder.setRefillFlag(Boolean.TRUE);
		}
		
		// retrieve Floating Drug Name Map
		Map<String,String> fdnMappingMap = refTableManager.getFdnMappingMapByDispOrderItemList(dispOrder.getDispOrderItemList(), OrderType.OutPatient, workstore);
		
		Map<Integer, Boolean> printRefillAuxLabelFlagMap = new HashMap<Integer, Boolean>();
		//SFI Refill Order
		if( !isInitRefill && dispOrder.getSfiFlag() ){
			List<Long> currRefillPharmOrderItemIdList = new ArrayList<Long>();
			List<Long> nextRefillPharmOrderItemIdList = new ArrayList<Long>();
			
			for( DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList()){
				if( dispOrderItem.getDispQty().compareTo(BigDecimal.ZERO) > 0 ){
					
					RefillScheduleItem rsi = refillItemMap.get(dispOrderItem.getPharmOrderItem());
					
					if( rsi != null ){
						if( rsi.getRefillSchedule().getRefillStatus() == RefillScheduleRefillStatus.Current ){
							currRefillPharmOrderItemIdList.add(dispOrderItem.getPharmOrderItem().getId());
						}else if( rsi.getRefillSchedule().getRefillStatus() == RefillScheduleRefillStatus.Next ){
							nextRefillPharmOrderItemIdList.add(dispOrderItem.getPharmOrderItem().getId());
						}
					}
				}
			}
			printRefillAuxLabelFlagMap = sfiRefillScheduleListManager.getPrintRefillAuxLabelFlagMap(currRefillPharmOrderItemIdList, nextRefillPharmOrderItemIdList, refillScheduleList.get(0));
		}

		// set refill qty
		for (DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList()) {
			if (refillItemMap.containsKey(dispOrderItem.getPharmOrderItem())) {
				RefillScheduleItem refillScheduleItem = refillItemMap.get(dispOrderItem.getPharmOrderItem());
				dispOrderItem.setDispQty(BigDecimal.valueOf(refillScheduleItem.getRefillQty()));

				if( !isInitRefill ){
					if( DocType.Sfi == refillScheduleItem.getRefillSchedule().getDocType() ){
						Boolean printAuxLabelFlag = printRefillAuxLabelFlagMap.get(dispOrderItem.getPharmOrderItem().getOrgItemNum());
						if( printAuxLabelFlag == null ){
							printAuxLabelFlag = Boolean.FALSE;
						}
						dispOrderItem.setPrintRefillAuxLabelFlag(printAuxLabelFlag);
					}else{
						dispOrderItem.setStartDate(refillScheduleItem.getRefillSchedule().getRefillDate());
					}
				}
				
				if( DocType.Standard == refillScheduleItem.getRefillSchedule().getDocType() ){
					dispOrderItem.setDurationInDay(refillScheduleItem.getRefillDuration());					
				}
				DateTime endDate = new DateTime(dispOrderItem.getStartDate());
				if( dispOrderItem.getDurationInDay() != null ){
					endDate = endDate.plusDays(dispOrderItem.getDurationInDay());
				}
				dispOrderItem.setEndDate(endDate.toDateMidnight().toDate());				
				dispOrderItem.setBaseLabel(dispLabelBuilder.convertToDispLabel(dispOrderItem, fdnMappingMap));
				dispOrderItem.preSave();
			}
		}
		
		return dispOrder;
	}

	private DispOrder createDispOrder(PharmOrder pharmOrder, List<PharmOrderItem> pharmOrderItemList, Ticket ticket, boolean resetItemNum, boolean isForceCompleteRefill) {
		
		OrderViewInfo orderViewInfo = (OrderViewInfo) Contexts.getSessionContext().get("orderViewInfo");
		
		// create DispOrder
		DispOrder dispOrder = new DispOrder();
		dispOrder.setPharmOrder(pharmOrder);
		pharmOrder.addDispOrder(dispOrder);

		dispOrder.setDispDate(Calendar.getInstance().getTime());
		dispOrder.setVetDate(new Date());
		
		Workstation workstation = workstationManager.retrieveWorkstationByHostNameWorkstore(CHECK_HOSTNAME.get(), workstore);
		if (workstation != null) {
			dispOrder.setCheckWorkstationCode(workstation.getWorkstationCode());
		}
		
		dispOrder.setWorkstore(workstore);
		dispOrder.clearRefillScheduleList();
		dispOrder.clearInvoiceList();
		
		dispOrder.setSpecCode(pharmOrder.getSpecCode());
		dispOrder.setWardCode(pharmOrder.getWardCode());
		dispOrder.setPatCatCode(pharmOrder.getPatCatCode());

		// set ticket
		dispOrder.setTicket(ticket);
		dispOrder.setTicketCreateDate(ticket.getCreateDate());
		dispOrder.setTicketDate(ticket.getTicketDate());
		dispOrder.setTicketNum(ticket.getTicketNum());
		dispOrder.setOrderType(ticket.getOrderType());
		
		if (orderViewInfo!=null) {
			dispOrder.setUrgentFlag(orderViewInfo.getUrgentFlag());
			dispOrder.setPrintLang(orderViewInfo.getPrintLang());
			
			// set doctor
			pharmOrder.setDoctorCode(orderViewInfo.getDoctorCode());
			pharmOrder.setDoctorName(orderViewInfo.getDoctorName());
			
			dispOrder.setAlertCheckFlag(orderViewInfo.getAlertCheckFlag());
			
			StringBuilder alertCheckRemark = new StringBuilder();
			for (ErrorInfo errorInfo : orderViewInfo.getAlertErrorInfoList()) {
				alertCheckRemark.append(errorInfo.getDesc());
				alertCheckRemark.append('\n');
				if (errorInfo.getCause() != null) {
					alertCheckRemark.append(errorInfo.getCause());
					alertCheckRemark.append('\n');
					alertCheckRemark.append(errorInfo.getAction());
					alertCheckRemark.append('\n');
				}
			}
			
			dispOrder.setAlertCheckRemark(alertCheckRemark.toString());
		} else {
			dispOrder.setUrgentFlag(false);
		}
		
		DmSpecialtyMoMapping dmSpecialtyMoMapping = dmSpecialtyMoMappingCacher.getDmSpecialtyMoMappingBySpecialtyCode(pharmOrder.getSpecCode());
		if (dmSpecialtyMoMapping != null) {
			pharmOrder.setDefaultDoctorCode(dmSpecialtyMoMapping.getMedicalOfficerCode());
		}
		
		// retrieve Floating Drug Name Map
		Map<String,String> fdnMappingMap = refTableManager.getFdnMappingMapByPharmOrderItemList(pharmOrderItemList, OrderType.OutPatient, workstore);
		
		// create DispOrderItem
		boolean refrigItemFlag = false;
		for (PharmOrderItem pharmOrderItem : pharmOrderItemList) 
		{
			if (pharmOrderItem.getMedOrderItem().getStatus() == MedOrderItemStatus.SysDeleted ||
				pharmOrderItem.getMedOrderItem().getStatus() == MedOrderItemStatus.Deleted) {
				continue;
			}

			Integer itemNum = pharmOrderItem.getItemNum();

			DispOrderItem dispOrderItem = createDispOrderItem(pharmOrderItem, itemNum);
			dispOrder.addDispOrderItem(dispOrderItem);			

			// set sfi flag
			if (pharmOrderItem.isSfi() && dispOrderItem.getDispQty().intValue() > 0) {
				dispOrder.setSfiFlag(Boolean.TRUE);
			}

			// update status
			logger.debug("pharmOrderItem.getItemNum() : #0", pharmOrderItem.getItemNum());
			updateDispOrderItemStatus(dispOrderItem, isForceCompleteRefill, dispOrder.getUrgentFlag());

			// set label object
			dispOrderItem.setBaseLabel(dispLabelBuilder.convertToDispLabel(dispOrderItem, fdnMappingMap));
			dispOrderItem.preSave();
			
			if ( !refrigItemFlag ) {
				if ( dispOrderItem.getDispQty().intValue() > 0 && dispLabelBuilder.checkRefrigerateWarning(dispOrderItem) ){
					refrigItemFlag = true;
				}
			}
		}
		dispOrder.setRefrigItemFlag(refrigItemFlag);
		
		markFluidWithoutActiveDrug(dispOrder.getDispOrderItemList());
		
		// set DispOrder status
		updateDispOrderStatus(dispOrder);
		if( isForceCompleteRefill ){
			dispOrder.setIssueWindowNum(Integer.valueOf(0));			
			dispOrder.setIssueUser(null);
		}
		
		if (pharmOrder.getChestFlag()) {
			dispOrder.setChestLabelList(chestLabelBuilder.convertToChestLabelList(dispOrder));
		}

		dispOrder.preSave();
		
		return dispOrder;
	}
	
	private void markFluidWithoutActiveDrug(List<DispOrderItem> allDispOrderItemList) {
		
		Map<MedOrderItem, List<DispOrderItem>> orderItemMap = new HashMap<MedOrderItem, List<DispOrderItem>>();
		for (DispOrderItem doi : allDispOrderItemList) {
			MedOrderItem moi = doi.getPharmOrderItem().getMedOrderItem();
			if (!orderItemMap.containsKey(moi)) {
				orderItemMap.put(moi, new ArrayList<DispOrderItem>());
			}
			orderItemMap.get(moi).add(doi);
		}

		for (Entry<MedOrderItem, List<DispOrderItem>> entry : orderItemMap.entrySet()) {

			MedOrderItem medOrderItem = entry.getKey();
			List<DispOrderItem> groupedDoiList = entry.getValue();

			if (!medOrderItem.isInfusion()) {
				continue;
			}
			
			Set<String> drugNameSet = new HashSet<String>();
	
			if (medOrderItem.getIvRxDrug() == null) {
				drugNameSet.add(medOrderItem.getDisplayName());
			}
			else {
				List<IvAdditive> ivAdditiveList = medOrderItem.getIvRxDrug().getIvAdditiveList();
				if (ivAdditiveList == null || ivAdditiveList.isEmpty()) {
					continue;
				}
				
				for (IvAdditive ivAdditive : ivAdditiveList) {
					if (StringUtils.isNotBlank(ivAdditive.getDisplayName())) {
						drugNameSet.add(ivAdditive.getDisplayName());
					}
				}
			}
	
			List<DispOrderItem> fluidList = new ArrayList<DispOrderItem>();
			boolean activeDrugExist = false;
			for (DispOrderItem doi : groupedDoiList) {
				if (drugNameSet.contains(dmDrugCacher.getDmDrug(doi.getPharmOrderItem().getItemCode()).getDmDrugProperty().getDisplayname())) {
					if (!DispOrderItemStatus.KeepRecord_Deleted.contains(doi.getStatus())) {
						activeDrugExist = true;
					}
				}
				else {
					fluidList.add(doi);
				}
			}
			
			if (!activeDrugExist) {
				for (DispOrderItem doi : fluidList) {
					doi.setStatus(DispOrderItemStatus.KeepRecord);
					doi.setPickUser(null);
					doi.setPickDate(null);
					doi.setAssembleUser(null);
					doi.setAssembleDate(null);
				}
			}
		}
	}

	private DispOrderItem createDispOrderItem(PharmOrderItem pharmOrderItem, Integer itemNum) {

		pharmOrderItem.loadDmInfo();
		
		DispOrderItem dispOrderItem = new DispOrderItem();
		dispOrderItem.setItemNum(itemNum);
		
		dispOrderItem.setCreateUser(identity.getCredentials().getUsername()); // for label preview

		if (pharmOrderItem.getMedOrderItem().isSfi() && (pharmOrderItem.getMedOrderItem().getRegimen() == null || pharmOrderItem.getMedOrderItem().getRegimen().getType() != RegimenType.StepUpDown)) {
			dispOrderItem.setDispQty(BigDecimal.valueOf(pharmOrderItem.getSfiQty()));
		}
		else {
			dispOrderItem.setDispQty(pharmOrderItem.getIssueQty());
		}
		
		// set durationInDay, startDate, endDate
		Date startDate = null, endDate = null;
		for (DoseGroup doseGroup : pharmOrderItem.getRegimen().getDoseGroupList()) {
			if (startDate == null || startDate.after(doseGroup.getStartDate())) {
				startDate = doseGroup.getStartDate();
			}
			if (endDate == null || endDate.before(doseGroup.getEndDate())) {
				endDate = doseGroup.getEndDate();
			}
		}
		dispOrderItem.setStartDate(startDate);
		dispOrderItem.setEndDate(endDate);
		if (startDate != null && endDate != null) {
			dispOrderItem.setDurationInDay(1 + Days.daysBetween(new DateTime(startDate.getTime()), new DateTime(endDate.getTime())).getDays());
		}

		dispOrderItem.clearInvoiceItemList();

		// link PharmOrderItem
		dispOrderItem.setPharmOrderItem(pharmOrderItem);
		pharmOrderItem.addDispOrderItem(dispOrderItem);
		
		// set warning
		if (pharmOrderItem.getDmWarningLite1() != null) {
			dispOrderItem.setWarnCode1(pharmOrderItem.getDmWarningLite1().getWarnCode());
			dispOrderItem.setWarnDesc1(pharmOrderItem.getDmWarningLite1().getWarnMessageEng());
		}
		if (pharmOrderItem.getDmWarningLite2() != null) {
			dispOrderItem.setWarnCode2(pharmOrderItem.getDmWarningLite2().getWarnCode());
			dispOrderItem.setWarnDesc2(pharmOrderItem.getDmWarningLite2().getWarnMessageEng());
		}
		if (pharmOrderItem.getDmWarningLite3() != null) {
			dispOrderItem.setWarnCode3(pharmOrderItem.getDmWarningLite3().getWarnCode());
			dispOrderItem.setWarnDesc3(pharmOrderItem.getDmWarningLite3().getWarnMessageEng());
		}
		if (pharmOrderItem.getdmWarningLite4() != null) {
			dispOrderItem.setWarnCode4(pharmOrderItem.getdmWarningLite4().getWarnCode());
			dispOrderItem.setWarnDesc4(pharmOrderItem.getdmWarningLite4().getWarnMessageEng());
		}

		logger.debug("pharmOrderItem:#0, isDirtyFlag:#1", pharmOrderItem.getItemNum(), pharmOrderItem.isDirtyFlag());
		dispOrderItem.setPrintFlag(!pharmOrderItem.isDirtyFlag());
		
		DmDrug dmDrug = DmDrugCacher.instance().getDmDrug(pharmOrderItem.getItemCode());
		if (dmDrug != null && dmDrug.getDmProcureSummary() != null) {
			dispOrderItem.setUnitPrice(BigDecimal.valueOf(dmDrug.getDmProcureSummary().getAverageUnitPrice()));
		}
		
		//Flags for SFI Item
		logger.debug("createDispOrderItem pharmOrderItem - dmDrug() #0", dmDrug);
		if( dmDrug != null ){
			logger.debug("createDispOrderItem pharmOrderItem - dmDrug.getDrugChargeInfo() #0", dmDrug.getDrugChargeInfo());
		}
		
		if (pharmOrderItem.getMedOrderItem().isSfi() && dmDrug != null && dmDrug.getDrugChargeInfo() != null ){
			dispOrderItem.setLifestyleFlag(dmDrug.getDrugChargeInfo().isLifestyleDrug());
			dispOrderItem.setOncologyFlag(dmDrug.getDrugChargeInfo().isOncologyDrug());
		}
		
		// default chargeSpecCode
		dispOrderItem.setChargeSpecCode(pharmOrderItem.getPharmOrder().getSpecCode());
		
		if (pharmOrderItem.getMedOrderItem().isSfi() && dispOrderItem.getDispQty().compareTo(BigDecimal.ZERO) > 0) {
			String specCode = pharmOrderItem.getPharmOrder().getSpecCode();
			ActionStatus actionStatus = pharmOrderItem.getActionStatus();
			ChargeSpecialty chargeSpecialty = retrieveChargeSpecialty(workstore, specCode, actionStatus);
			
			if (chargeSpecialty != null) {
				dispOrderItem.setChargeSpecCode(chargeSpecialty.getChargeSpecCode());
			}
		}
		
		dispOrderItem.setSfiOverrideWarnFlag(pharmOrderItem.getSfiOverrideWarnFlag());
		
		return dispOrderItem;
	}
	
	@SuppressWarnings("unchecked")
	@CacheResult
	private ChargeSpecialty retrieveChargeSpecialty(Workstore workstore, String poSpecCode, ActionStatus actionStatus) {
		ChargeSpecialtyType chargeSpecialtyType = ChargeSpecialtyType.dataValueOf(actionStatus.getDataValue());
		
		List<ChargeSpecialty> chargeSpecialtyList = em.createQuery(
				"select o from ChargeSpecialty o" + // 20120227 index check : ChargeSpecialty.hospital,type,specCode : UI_CHARGE_SPECIALTY_01
				" where o.hospital = :hospital" +
				" and o.specCode = :specCode" +
				" and o.type = :type")
				.setParameter("hospital", workstore.getHospital())
				.setParameter("specCode", poSpecCode)
				.setParameter("type", chargeSpecialtyType)
				.getResultList();
		
		if (chargeSpecialtyList.isEmpty()) {
			return null;
		} else {
			return chargeSpecialtyList.get(0);
		}
	}
	
	public void printDispOrder(DispOrder dispOrder, boolean printCapdVoucher) {
		List<RenderAndPrintJob> printList = new ArrayList<RenderAndPrintJob>();
		this.prepareDispOrderPrintJob(printList, dispOrder);
		refillCouponRenderer.prepareRefillCouponPrintJob(printList, dispOrder, false);	
		printAgent.renderAndPrint(printList);
		
		if ( printCapdVoucher ) {
			capdVoucherManager.prepareCapdVoucherPrintJob(printList, dispOrder);
		}
	}
	
	public void prepareDispOrderPrintJob(List<RenderAndPrintJob> printJobList, DispOrder dispOrder) {

		if (dispOrder.getDispOrderItemList().isEmpty()) {
			return;
		}
		
		PrintLabelDetail orderPrintLabelDetail;
		if ("random".equals(LABEL_DISPLABEL_PRINTING_MODE.get())) 
		{
			orderPrintLabelDetail = printLabelManager.retrievePrintLabelDetailOnNonPeakMode(dispOrder.getDispOrderItemList(), dispOrder.getPrintLang());
		}
		else if ("local".equals(LABEL_DISPLABEL_PRINTING_MODE.get())) 
		{
			orderPrintLabelDetail = null;
			printLabelManager.retrievePrintLabelDetailOnPmsMode(dispOrder.getDispOrderItemList(), dispOrder.getPrintLang());
		} 
		else if ("itemLocation".equals(LABEL_DISPLABEL_PRINTING_MODE.get()))
		{
			orderPrintLabelDetail = null;
			printLabelManager.retrievePrintLabelDetailOnEdsMode(dispOrder.getDispOrderItemList(), dispOrder.getPrintLang());
		}	
		else 
		{
			orderPrintLabelDetail = null;
		}

		if (dispOrder.getPharmOrder().getChestFlag()) {		
			boolean printChestLabel = false;
			for ( DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList() ) {
				if (!dispOrderItem.getPharmOrderItem().getChestFlag()) {
					continue;
				}
				logger.debug("ChestOrder PrintFlag=#0, itemNum=#1", dispOrderItem.getPrintFlag(), dispOrderItem.getPharmOrderItem().getItemNum());
				if ( !dispOrderItem.getPrintFlag() ) {
					printChestLabel = true;
					dispOrderItem.setPrintFlag(Boolean.TRUE);
				}
			}
			
			if ( printChestLabel ) {
				dispOrder.loadChestLabel();

				PrintOption printOption = new PrintOption();
				printOption.setPrintLang(dispOrder.getPrintLang());
				if (LABEL_DISPLABEL_LARGELAYOUT_ENABLED.get(Boolean.FALSE)) {
					printOption.setPrintType(PrintType.LargeLayout);
				} else {
					printOption.setPrintType(PrintType.Normal);
				}
				
				for (ChestLabel chestLabel:dispOrder.getChestLabelList().getChestLabels()) {
					printJobList.add(new RenderAndPrintJob("ChestLabel",
												printOption,
												printerSelector.retrievePrinterSelect(PrintDocType.OtherLabel),
												dispLabelBuilder.getChestLabelParam(chestLabel),
												Arrays.asList(chestLabel),
												"[ChestLabel] TicketNum:" + chestLabel.getTicketNum()
												));
				}
			}
		}
			 
		boolean printInfusionInstructionSummary = false;
		for (DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList()) {
			logger.debug("PrintFlag=#0, itemNum=#1", dispOrderItem.getPrintFlag(), dispOrderItem.getPharmOrderItem().getItemNum());
			if (dispOrderItem.getPrintFlag() || dispOrderItem.getStatus() == DispOrderItemStatus.KeepRecord || dispOrderItem.getPharmOrderItem().getChestFlag()) {
				continue;
			}
			if ( !printInfusionInstructionSummary ){
				printInfusionInstructionSummary = infusionInstructionSummaryManager.checkIfPrintInfusionInstructionSummary(dispOrderItem);
			}
			this.prepareDispOrderItemPrintJob(printJobList, dispOrderItem, orderPrintLabelDetail);
			dispOrderItem.setPrintFlag(Boolean.TRUE);
		}
			
		if ( printInfusionInstructionSummary ) {
			infusionInstructionSummaryManager.prepareInfusionInstructionSummaryPrintJobWithReminder(printJobList, dispOrder.getPrintLang(), dispOrder);
		}
	}

	
	private void prepareDispOrderItemPrintJob(List<RenderAndPrintJob> renderAndPrintJobList, DispOrderItem dispOrderItem, PrintLabelDetail orderPrintLabelDetail) {

		// set print label detail
		PrintLabelDetail itemPrintLabelDetail;
		if (orderPrintLabelDetail == null) {
			itemPrintLabelDetail = dispOrderItem.getPrintLabelDetail();
		} else {
			itemPrintLabelDetail = orderPrintLabelDetail;
		}
		
		if (itemPrintLabelDetail.getWorkstation() != null) {
			dispOrderItem.setWorkstationCode(itemPrintLabelDetail.getWorkstation().getWorkstationCode());
			dispOrderItem.setWorkstationId(itemPrintLabelDetail.getWorkstation().getId());
		}
		if (itemPrintLabelDetail.getItemLocation() != null) {
			dispOrderItem.replaceBinNum(itemPrintLabelDetail.getItemLocation().getBinNum());
		}
		
		if ( itemPrintLabelDetail.getDispOrderItemIdMap().containsKey(dispOrderItem.getId())) {
			// skip normal label printing for scriptpro item 
			return;
		}

		// print label
		PrintOption printOption = new PrintOption();
		printOption.setPrintLang(dispOrderItem.getDispOrder().getPrintLang());
		if (LABEL_DISPLABEL_LARGELAYOUT_ENABLED.get(Boolean.FALSE)) {
			printOption.setPrintType(PrintType.LargeLayout);
		} else if (LABEL_DISPLABEL_LARGEFONT_ENABLED.get(Boolean.FALSE)) {
			printOption.setPrintType(PrintType.LargeFont);
		} else {
			printOption.setPrintType(PrintType.Normal);
		}
		
		itemPrintLabelDetail.setReverse(true);
		itemPrintLabelDetail.setCopies(dispOrderItem.getPharmOrderItem().getMedOrderItem().getNumOfLabel());
		
		String fastQueueDocType = null;
		if( Boolean.TRUE == itemPrintLabelDetail.getPrintFastQueueLabelPrinterFlag() ){
			fastQueueDocType = "FastQueue";
		}
		
		renderAndPrintJobList.add(new RenderAndPrintJob("DispLabel",
									printOption,
									itemPrintLabelDetail,
									dispLabelBuilder.getDispLabelParam((DispLabel)dispOrderItem.getBaseLabel()),
									Arrays.asList((DispLabel)dispOrderItem.getBaseLabel()),
									"[DispLabel] TicketDate:" + df.format(dispOrderItem.getDispOrder().getTicket().getTicketDate()) +
									", TicketNum:" + dispOrderItem.getDispOrder().getTicket().getTicketNum() +
									", ItemCode:" + dispOrderItem.getPharmOrderItem().getItemCode() +
									", HospCode:" + dispOrderItem.getDispOrder().getWorkstore().getHospCode() +
									", WorkStoreCode:" + dispOrderItem.getDispOrder().getWorkstore().getWorkstoreCode()
									,fastQueueDocType));
	}
	
	private void updateDispOrderStatus(DispOrder dispOrder) {

		DispOrderStatus status = retrieveDispOrderStatus(dispOrder);
		dispOrder.setStatus(status);
		
		Date currentTime = new Date();
		String currentUser = identity.getCredentials().getUsername();
		
		if (status.isLaterThanEqualTo(DispOrderStatus.Checked)) {
			dispOrder.setIssueWindowNum(Integer.valueOf(0));
		}
		
		if (status == DispOrderStatus.Checked) {
			dispOrder.setCheckUser(currentUser);
			dispOrder.setCheckDate(currentTime);
		}
		
		if (status == DispOrderStatus.Issued) {
			dispOrder.setIssueDate(currentTime);
			dispOrder.setIssueUser(currentUser);
		}
		
		if (status == DispOrderStatus.Picked) {
			dispOrder.setPickDate(currentTime);
		}
		
		if (status == DispOrderStatus.Assembled) {
			dispOrder.setAssembleDate(currentTime);
		}
	}

	public void updateDispOrderByPrevDispOrder(DispOrder dispOrder, DispOrder prevDispOrder) {

		Set<DispOrderItem> delDispOrderItemSet = new HashSet<DispOrderItem>(prevDispOrder.getDispOrderItemList());

		for (DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList()) {
			DispOrderItem prevDispOrderItem = prevDispOrder.getDispOrderItemByPoOrgItemNum(dispOrderItem.getPharmOrderItem().getOrgItemNum());
			if (prevDispOrderItem != null) {
				dispOrderItem.updateFromPrevDispOrderItem(prevDispOrderItem);
			}

			if( dispOrder.getRefillFlag() ){
				if( prevDispOrderItem != null && prevDispOrderItem.getItemNum().equals(dispOrderItem.getItemNum()) ){
					delDispOrderItemSet.remove(prevDispOrderItem);
				}
			}else{
				prevDispOrderItem = prevDispOrder.getDispOrderItemByPoItemNum(dispOrderItem.getPharmOrderItem().getItemNum());
				if (prevDispOrderItem != null) {
					delDispOrderItemSet.remove(prevDispOrderItem);
				}
			}
		}

		updateDispOrderStatus(dispOrder);
		dispOrder.updateFromPrevDispOrder(prevDispOrder);
		
		for (DispOrderItem delDispOrderItem : delDispOrderItemSet) {
			delDispOrderItem.setStatus(DispOrderItemStatus.Deleted);
		}
	}
	
	public Map<Long, Boolean> updateDispOrderLargeLayoutFlag(DispOrder dispOrder) {
		if (dispOrder.getAdminStatus() != DispOrderAdminStatus.Normal) {
			return null;
		}
		
		if (dispOrder.getDispOrderItemList().isEmpty()) {
			return null;
		}
			
		if (dispOrder.getPharmOrder().getChestFlag()) {
			if (dispOrder.getPharmOrder().getChestFlag()) {		
				boolean printChestLabel = false;
				
				for ( DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList() ) {
					if ( ! dispOrderItem.getPharmOrderItem().getChestFlag()) {
						continue;
					}
					if ( ! dispOrderItem.getPrintFlag() ) {
						printChestLabel = true;
					}
				}
				
				if ( printChestLabel ) {
					dispOrder.setLargeLayoutFlag( LABEL_DISPLABEL_LARGELAYOUT_ENABLED.get(Boolean.FALSE) );
				}
			}
		}
		
		Map<Long, Boolean> largeLayoutFlagByDoiIdMap = new HashMap<Long, Boolean>();
		
		for (DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList()) {
			if (dispOrderItem.getPrintFlag() || dispOrderItem.getStatus() == DispOrderItemStatus.KeepRecord) {
				// note: in label print, apart from printed and keep record only item, DOI is also skipped to print 
				//       if it is chest item
				//       but since CDDH needs to preview large OP label for chest item printed with large chest label,
				//       DOI large layout flag is set for chest item here
				continue;
			}
			
			dispOrderItem.setLargeLayoutFlag( LABEL_DISPLABEL_LARGELAYOUT_ENABLED.get(Boolean.FALSE) );
			
			largeLayoutFlagByDoiIdMap.put(dispOrderItem.getId(), LABEL_DISPLABEL_LARGELAYOUT_ENABLED.get(Boolean.FALSE));
		}
		
		return largeLayoutFlagByDoiIdMap;
	}
	
	private DispOrderStatus retrieveDispOrderStatus(DispOrder dispOrder) {

		if (activeProfile.getType() == OperationProfileType.Pms) {
			return DispOrderStatus.Issued;
		}

		DispOrderStatus orderStatus = DispOrderStatus.Issued;
		DispOrderStatus lastStatus  = DispOrderStatus.Vetted;
		for (DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList()) {
			if (dispOrderItem.getStatus() == DispOrderItemStatus.KeepRecord) {
				continue;
			}
			DispOrderStatus itemStatus = DispOrderStatus.dataValueOf(dispOrderItem.getStatus().getDataValue());
			if (orderStatus.isLaterThan(itemStatus)) {
				orderStatus = itemStatus;
			}
			if (itemStatus.isLaterThan(lastStatus)) {
				lastStatus = itemStatus;
			}
		}

		if (PICKING_AUTO_ENABLED.get(false) && DispOrderStatus.Picked.isLaterThan(orderStatus)) {
			orderStatus = DispOrderStatus.Picked;
		}
		
		if (orderStatus == DispOrderStatus.Vetted && lastStatus.isLaterThan(DispOrderStatus.Vetted)) {
			orderStatus = DispOrderStatus.Picking;
		}
		else if (orderStatus == DispOrderStatus.Picked && lastStatus.isLaterThan(DispOrderStatus.Picked)) {
			orderStatus = DispOrderStatus.Assembling;
		}
		
		return orderStatus;
	}
	
	private void updateDispOrderItemStatus(DispOrderItem dispOrderItem, boolean isForceCompleteRefill, boolean urgentFlag) {

		DispOrderItemStatus status; 
		if (isForceCompleteRefill) {
			status = DispOrderItemStatus.KeepRecord;
		}
		else {
			status = retrieveDispOrderItemStatus(dispOrderItem, urgentFlag);
		}
		dispOrderItem.setStatus(status);
		
		if (status == DispOrderItemStatus.Deleted || status == DispOrderItemStatus.KeepRecord) {
			return;
		}
		
		Date currentTime = new Date();
		String currentUser = identity.getCredentials().getUsername();
		
		if (status == DispOrderItemStatus.Picked) {
			dispOrderItem.setPickUser(currentUser);
			dispOrderItem.setPickDate(currentTime);
		}
		
		if (status == DispOrderItemStatus.Assembled) {
			dispOrderItem.setAssembleUser(currentUser);
			dispOrderItem.setAssembleDate(currentTime);
		}
	}
	
	private DispOrderItemStatus retrieveDispOrderItemStatus(DispOrderItem dispOrderItem, boolean urgentFlag) {

		MedOrderItem medOrderItem = dispOrderItem.getPharmOrderItem().getMedOrderItem();
		ActionStatus actionStatus = dispOrderItem.getPharmOrderItem().getActionStatus();
		String fmStatus = medOrderItem.getFmStatus();
		
		if (dispOrderItem.getPharmOrderItem().getCapdVoucherFlag()
			||  actionStatus == ActionStatus.DispInClinic
			||  actionStatus == ActionStatus.ContinueWithOwnStock
			|| (actionStatus == ActionStatus.PurchaseByPatient && dispOrderItem.getDispQty().compareTo(BigDecimal.ZERO) == 0)
			|| FmStatus.FreeTextEntryItem.getDataValue().equals(fmStatus)) 
		{
			return DispOrderItemStatus.KeepRecord;
		}

		if (activeProfile.getType() == OperationProfileType.Pms) {
			return DispOrderItemStatus.Issued;
		}

		DispOrderItemStatus itemStatus = DispOrderItemStatus.Vetted;

		logger.debug("dispOrderItem.getPrintFlag()=#0", dispOrderItem.getPrintFlag());
		if (urgentFlag && !dispOrderItem.getPrintFlag()) {
			DispOrderItemStatus urgentStatus = DispOrderItemStatus.dataValueOf(VETTING_URGENTORDER_STATUS.get());
			if (urgentStatus.isLaterThan(itemStatus)) {
				itemStatus = urgentStatus;
			}
		}

		if (PICKING_AUTO_ENABLED.get()) {
			DispOrderItemStatus pickStatus;
			if (activeProfile.getType() == OperationProfileType.NonPeak && PICKING_ONCE_ENABLED.get()) {
				pickStatus = DispOrderItemStatus.Assembled;
			}
			else {
				pickStatus = DispOrderItemStatus.Picked;
			}
			if (pickStatus.isLaterThan(itemStatus)) {
				itemStatus = pickStatus;
			}
		}

		return itemStatus;
	}

	public void endDispensing(DispOrder dispOrder, Invoice sfiInvoice, boolean reverseUncollect) {
		em.flush();
		if ( reverseUncollect ) {
			corpPmsServiceProxy.reverseUncollectAndEndDispensing(dispOrder, 
																	initDispInfo(dispOrder), 
																	CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED.get(false), 
																	CDDH_LEGACY_PERSISTENCE_ENABLED.get(false), 
																	sfiInvoice);
		} else {
			corpPmsServiceProxy.endDispensing(dispOrder.getId(), 
												initDispInfo(dispOrder),
												CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED.get(false),
												CDDH_LEGACY_PERSISTENCE_ENABLED.get(false), 
												sfiInvoice);
		}
	}
	
	public void endDispensingForBatchIsuse(List<DispOrder> dispOrderList){
		List<DispInfo> dispInfoList = new ArrayList<DispInfo>();
		for ( DispOrder dispOrder : dispOrderList ) {
			updateStatusForEndDispensing(dispOrder);
			if ( dispOrder.getIssueWindowNum() == null ) {
				dispOrder.setIssueWindowNum(Integer.valueOf(0));
			}
			dispInfoList.add(initDispInfo(dispOrder));
		}
		em.flush();
		corpPmsServiceProxy.endDispensingForBatchIssue(dispInfoList,
														CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED.get(false),
														CDDH_LEGACY_PERSISTENCE_ENABLED.get(false));
		
	}
	
	@SuppressWarnings("unchecked")
	public void updateStatusForEndDispensing(DispOrder dispOrder) {
		// update status
		if ("EDS".equals(propMap.getValue(ACTIVE_OPERATION_MODE)) && propMap.getValueAsBoolean(WORKFLOW_EDS_BPM_ENABLED)) 
		{
			dispOrderId = dispOrder.getId();
			Contexts.getSessionContext().set("dispOrderId", dispOrderId);
			dispensingPmsMode.updateToIssuedStatus(dispOrder);
		} 
		else 
		{
			dispOrder.setIssueDate(new Date());
			dispOrder.setIssueUser(identity.getCredentials().getUsername());
			dispOrder.setStatus(DispOrderStatus.Issued);
		}
		for (DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList()) {
			if ( dispOrderItem.getStatus() == DispOrderItemStatus.KeepRecord || 
					dispOrderItem.getStatus() == DispOrderItemStatus.Deleted) {
				continue;
			}
			dispOrderItem.setStatus(DispOrderItemStatus.Issued);
		}
		
		
		List<RefillSchedule> refillScehduleList = em.createQuery(
				"select o from RefillSchedule o" + // 20120227 index check : RefillSchedule.dispOrder : FK_REFILL_SCHEDULE_02
				" where o.dispOrder.id = :dispOrderId")
				.setParameter("dispOrderId", dispOrder.getId())
				.getResultList();
				
		for(RefillSchedule refillSchedule : refillScehduleList){
			refillSchedule.setStatus(RefillScheduleStatus.Dispensed);			
		}
	}
	
	private DispInfo initDispInfo(DispOrder dispOrder){
		DispInfo dispInfo = new DispInfo();
		
		dispInfo.setDispOrderId(dispOrder.getId());
		dispInfo.setCheckDate(dispOrder.getCheckDate());
		dispInfo.setCheckUser(dispOrder.getCheckUser());
		dispInfo.setAssembleDate(dispOrder.getAssembleDate());
		dispInfo.setPickDate(dispOrder.getPickDate());
		dispInfo.setIssueDate(dispOrder.getIssueDate());
		dispInfo.setIssueUser(dispOrder.getIssueUser());
		
		dispInfo.setDispItemInfoMap(new HashMap<Long, DispItemInfo>());
		
		for ( DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList() ) {
			DispItemInfo dispItemInfo = new DispItemInfo();

			dispItemInfo.setPickDate(dispOrderItem.getPickDate());
			dispItemInfo.setPickUser(dispOrderItem.getPickUser());
			dispItemInfo.setAssembleDate(dispOrderItem.getAssembleDate());
			dispItemInfo.setAssembleUser(dispOrderItem.getAssembleUser());
			
			// sync large layout flag of all item once to CORP (for EDS mode)
			// (because reverse suspend does not call CORP when order is not issued, large layout flag 
			//  of reverse suspend item in EDS mode need to be sync to CORP here)  
			if (dispOrderItem.getBaseLabel() != null) {
				dispItemInfo.setLargeLayoutFlag(dispOrderItem.getBaseLabel().getLargeLayoutFlag());
			}
			
			dispInfo.getDispItemInfoMap().put(dispOrderItem.getId(), dispItemInfo);
		}
		
		return dispInfo;
	}
	
	public void cancelDispensing(DispOrder dispOrder){		
		dispOrder.markDeleted();
		
		corpPmsServiceProxy.cancelDispensing(
				dispOrder.getId(),
				CDDH_LEGACY_PERSISTENCE_ENABLED.get(false),
				CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED.get(false),
				workstore.getHospCode());
	}
	
	public EndVettingResult refillDispensing(DispOrder dispOrder, List<MedOrderItem> confirmDisconIdList){
		return refillDispensing(dispOrder, confirmDisconIdList, true, null);
	}
	
	public EndVettingResult refillDispensing(DispOrder dispOrder, List<MedOrderItem> confirmDisconIdList, boolean releaseOrder, Map<Long, Boolean> largeLayoutFlagMap){
		dispOrder.getPharmOrder().clearDmInfo();
		String hospCodeForReleaseOrder = null;
		if( releaseOrder ){
			hospCodeForReleaseOrder = workstore.getHospCode();
		}
		return corpPmsServiceProxy.refillDispensing(dispOrder, 
				CDDH_LEGACY_PERSISTENCE_ENABLED.get(false),
				CHARGING_FCS_LEGACY_PERSISTENCE_ENABLED.get(false),
				confirmDisconIdList,
				hospCodeForReleaseOrder,
				largeLayoutFlagMap);
	}
}
