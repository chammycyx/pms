package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.corp.cache.DmWarningCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmMoeProperty;
import hk.org.ha.model.pms.dms.persistence.DmWarning;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
import hk.org.ha.model.pms.persistence.reftable.LocalWarning;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("warningManager")
public class WarningManagerBean implements WarningManagerLocal {

	@PersistenceContext
	private EntityManager em;	

	@Logger
	private Log logger;

	@In
	private DmWarningCacherInf dmWarningCacher;

	@In
	private DmDrugCacherInf dmDrugCacher; 
	
	private final int NUM_OF_WARNING = 4;

	public WarningManagerBean() {
	}

	@SuppressWarnings("unchecked")
	public List<DmWarning> retrieveLocalWarningList(String itemCode, WorkstoreGroup workstoreGroup) {				    	

		List<LocalWarning> localWarningList = em.createQuery(
				"select o from LocalWarning o" + // 20120303 index check : LocalWarning.workstoreGroup,itemCode : I_LOCAL_WARNING_01
				" where o.workstoreGroup = :workstoreGroup " +
				" and o.itemCode = :itemCode")
				.setParameter("workstoreGroup", workstoreGroup)
				.setParameter("itemCode", itemCode)
				.getResultList();

		if ( localWarningList == null || localWarningList.size() == 0 ) {
			return null;
		}

		List<DmWarning> resultList = new ArrayList<DmWarning>();			
		for ( int i=0; i<NUM_OF_WARNING; i++ ) {
			resultList.add(null);	
		}

		for (LocalWarning localWarning : localWarningList) {			
			if ( localWarning.getSortSeq().intValue() <= NUM_OF_WARNING ) {
				resultList.set(localWarning.getSortSeq().intValue()-1, dmWarningCacher.getDmWarningByWarnCode(localWarning.getWarnCode()));
			}
		}			

		return removeAndPaddingForOldSystemWarning(resultList);
	}

	public List<DmWarning> retrieveMoeWarmingList(String itemCode) {		

		List<DmWarning> moeWarningList;
		List<DmWarning> dmWarningList = new ArrayList<DmWarning>();		
		DmMoeProperty dmMoeProperty = dmDrugCacher.getDmDrug(itemCode).getDmMoeProperty();
		//get Warning 1
		if ( !StringUtils.isEmpty(dmMoeProperty.getWarnCode1()) ) {
			dmWarningList.add(dmWarningCacher.getDmWarningByWarnCode(dmMoeProperty.getWarnCode1()));
		} else {
			dmWarningList.add(null);
		}

		//get Warning 2
		if ( !StringUtils.isEmpty(dmMoeProperty.getWarnCode2()) ) {
			dmWarningList.add(dmWarningCacher.getDmWarningByWarnCode(dmMoeProperty.getWarnCode2()));
		} else {
			dmWarningList.add(null);
		}

		//get Warning 3
		if ( !StringUtils.isEmpty(dmMoeProperty.getWarnCode3()) ) {
			dmWarningList.add(dmWarningCacher.getDmWarningByWarnCode(dmMoeProperty.getWarnCode3()));
		} else {
			dmWarningList.add(null);
		}

		//get Warning 4
		if ( !StringUtils.isEmpty(dmMoeProperty.getWarnCode4()) ) {
			dmWarningList.add(dmWarningCacher.getDmWarningByWarnCode(dmMoeProperty.getWarnCode4()));
		} else {
			dmWarningList.add(null);
		}

		moeWarningList = removeAndPaddingForOldSystemWarning(dmWarningList);		
		logger.debug("moeWarningList size #0", moeWarningList.size());		
		
		return moeWarningList;
	}	

	private List<DmWarning> removeAndPaddingForOldSystemWarning( List<DmWarning> dmWarningListIn ) {
		List<DmWarning> moeWarningList = new ArrayList<DmWarning>();
		for ( int startReplaceIndex=0; startReplaceIndex<dmWarningListIn.size(); startReplaceIndex++ ) {
			DmWarning dmWarning = dmWarningListIn.get(startReplaceIndex);
			if ( dmWarningListIn.get(startReplaceIndex)!= null ) {	
				if ( !"O".equals(dmWarning.getUsageType()) ) {
					moeWarningList.add(dmWarning);				
				}
			} else {
				moeWarningList.add(null);
			}			
		}
		//padding null
		for ( int i=moeWarningList.size(); i<4; i++ ) {
			moeWarningList.add(null);
		}
		return moeWarningList;
	}
}
