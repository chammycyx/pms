package hk.org.ha.model.pms.biz.refill;

import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.udt.printing.PrintLang;

import javax.ejb.Local;

@Local
public interface PrintRefillCouponServiceLocal {
	
	void printStandardRefillCoupon(RefillSchedule refillScheduleIn, boolean isReprint, PrintLang printLang, Integer numOfCopies);

	void destroy();
}
