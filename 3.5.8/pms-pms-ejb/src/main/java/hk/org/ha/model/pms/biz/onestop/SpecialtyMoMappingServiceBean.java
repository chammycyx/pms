package hk.org.ha.model.pms.biz.onestop;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.cache.DmSpecialtyMoMappingCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmSpecialtyMoMapping;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

@RemoteDestination
@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("specialtyMoMappingService")
@MeasureCalls
public class SpecialtyMoMappingServiceBean implements SpecialtyMoMappingServiceLocal {
	
	@In
	private DmSpecialtyMoMappingCacherInf dmSpecialtyMoMappingCacher;
	
	@Out(required = false)
	private DmSpecialtyMoMapping dmSpecialtyMoMapping;
	
	@Logger
	private Log logger;
		
	public void retrieveDmSpecialtyMoMappingBySpecialtyCode(String specCode) 
	{
		dmSpecialtyMoMapping = dmSpecialtyMoMappingCacher.getDmSpecialtyMoMappingBySpecialtyCode(specCode);
		
		logger.debug("retrieveDmSpecialtyMoMappingBySpecialtyCode: #0", (dmSpecialtyMoMapping==null?"No mo specialty mapping found":dmSpecialtyMoMapping.getMedicalOfficerCode()));
	}
		
	@Remove
	public void destroy() 
	{
		if (dmSpecialtyMoMapping != null) {
			dmSpecialtyMoMapping = null;
		}
	}
}