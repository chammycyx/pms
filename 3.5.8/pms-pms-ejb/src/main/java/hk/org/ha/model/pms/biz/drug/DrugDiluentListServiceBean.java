package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("drugDiluentListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DrugDiluentListServiceBean implements DrugDiluentListServiceLocal {

	@In
	private DrugDiluentListManagerLocal drugDiluentListManager;

	@SuppressWarnings("unused")
	@Out
	private List<WorkstoreDrug> drugDiluentList;

	public void retrieveDrugDiluentListByRxItem(RxItem rxItem) {
		drugDiluentList = drugDiluentListManager.getDrugDiluentList(rxItem);		
	}
	
	@Remove
	public void destroy() {		
	}
}
