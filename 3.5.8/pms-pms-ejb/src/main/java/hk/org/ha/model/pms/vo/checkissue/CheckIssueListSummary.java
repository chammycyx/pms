package hk.org.ha.model.pms.vo.checkissue;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class CheckIssueListSummary implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private List<CheckIssueOrder> checkOrderList;

	private List<CheckIssueOrder> otherCheckOrderList;
	
	private List<CheckIssueOrder> issueOrderList;
	
	private List<CheckIssueOrder> issuedOrderList;
	
	private Date checkIssueListDate; 
	
	private Date otherCheckListDate; 
	
	private Date issuedListDate;

	public Date getCheckIssueListDate() {
		return checkIssueListDate;
	}

	public void setCheckIssueListDate(Date checkIssueListDate) {
		this.checkIssueListDate = checkIssueListDate;
	}

	public Date getOtherCheckListDate() {
		return otherCheckListDate;
	}

	public void setOtherCheckListDate(Date otherCheckListDate) {
		this.otherCheckListDate = otherCheckListDate;
	}

	public Date getIssuedListDate() {
		return issuedListDate;
	}

	public void setIssuedListDate(Date issuedListDate) {
		this.issuedListDate = issuedListDate;
	}

	public List<CheckIssueOrder> getCheckOrderList() {
		return checkOrderList;
	}

	public void setCheckOrderList(List<CheckIssueOrder> checkOrderList) {
		this.checkOrderList = checkOrderList;
	}

	public List<CheckIssueOrder> getOtherCheckOrderList() {
		return otherCheckOrderList;
	}

	public void setOtherCheckOrderList(List<CheckIssueOrder> otherCheckOrderList) {
		this.otherCheckOrderList = otherCheckOrderList;
	}

	public List<CheckIssueOrder> getIssueOrderList() {
		return issueOrderList;
	}

	public void setIssueOrderList(List<CheckIssueOrder> issueOrderList) {
		this.issueOrderList = issueOrderList;
	}

	public List<CheckIssueOrder> getIssuedOrderList() {
		return issuedOrderList;
	}

	public void setIssuedOrderList(List<CheckIssueOrder> issuedOrderList) {
		this.issuedOrderList = issuedOrderList;
	}
	
}