package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Institution;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.phs.Specialty;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("specialtyManager")
@MeasureCalls
public class SpecialtyManagerBean implements SpecialtyManagerLocal {

	@PersistenceContext
	private EntityManager em;

	@In
	private Workstore workstore;
	
	@SuppressWarnings("unchecked")
	public List<String> retrieveSpecialtyCodeList() 
	{
		return em.createQuery(
				"select o.specCode from Specialty o" + // 20120227 index check : Specialty.institution : FK_SPECIALTY_01
				" where o.institution = :institution" +
				" and o.status = :status" +
				" order by o.specCode")
				.setParameter("institution", workstore.getWorkstoreGroup().getInstitution())
				.setParameter("status", RecordStatus.Active)
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Specialty> retrieveSpecialtyList() 
	{
		return em.createQuery(
				"select o from Specialty o" + // 20120227 index check : Specialty.institution : FK_SPECIALTY_01
				" where o.institution = :institution" +
				" order by o.specCode")
				.setParameter("institution", workstore.getWorkstoreGroup().getInstitution())
				.getResultList();		
	}
	
	@SuppressWarnings("unchecked")
	public List<Specialty> retrieveSpecialtyListWithoutDeleteStatus() 
	{
		return em.createQuery(
				"select o from Specialty o" + // 20121112 index check : Specialty.institution : FK_SPECIALTY_01
				" where o.institution = :institution" +
				" and o.status <> :status" +
				" order by o.specCode")
				.setParameter("institution", workstore.getWorkstoreGroup().getInstitution())
				.setParameter("status", RecordStatus.Delete)
				.getResultList();		
	}
	
	@SuppressWarnings("unchecked")
	public List<Specialty> retrieveSpecialtyListWithActiveStatus() 
	{
		return em.createQuery(
				"select o from Specialty o" + // 20121112 index check : Specialty.institution : FK_SPECIALTY_01
				" where o.institution = :institution" +
				" and o.status = :status" +
				" order by o.specCode")
				.setParameter("institution", workstore.getWorkstoreGroup().getInstitution())
				.setParameter("status", RecordStatus.Active)
				.getResultList();		
	}
	
	@SuppressWarnings("unchecked")
	public List<Specialty> retrieveSpecialtyListByInstCode(List<String> instCodeList) 
	{
		return em.createQuery(
				"select o from Specialty o" + // 20121112 index check : Specialty.institution : FK_SPECIALTY_01
				" where o.institution.instCode in :instCodeList" +
				" and o.status <> :status" +
				" order by o.institution.instCode, o.specCode")
				.setParameter("instCodeList", instCodeList)
				.setParameter("status", RecordStatus.Delete)
				.getResultList();		
	}
}
