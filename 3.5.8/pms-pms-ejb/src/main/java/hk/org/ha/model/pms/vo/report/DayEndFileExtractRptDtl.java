package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class DayEndFileExtractRptDtl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer opItemCount;
	
	private Integer opPrescCount;
	
	private Integer opDispSampleCount;
	
	private Integer opDispSfiCount;
	
	private Integer opDispSafetyNetItemCount;
	
	private Integer capdVoucherCount;
	
	private Integer capdItemCount;
	
	private Integer mpItemCount;

	private Integer mpDispSampleCount;
	
	private Integer mpDispSfiCount;
	
	private Integer mpDispSafetyNetItemCount;

	private Integer mpDispWardStockCount;

	private Integer mpDispDangerDrugCount;

	private Integer itemCount;
	
	public Integer getOpItemCount() {
		return opItemCount;
	}
	
	public void setOpItemCount(Integer opItemCount) {
		this.opItemCount = opItemCount;
	}
	
	public Integer getOpPrescCount() {
		return opPrescCount;
	}
	
	public void setOpPrescCount(Integer opPrescCount) {
		this.opPrescCount = opPrescCount;
	}
	
	public Integer getOpDispSampleCount() {
		return opDispSampleCount;
	}
	
	public void setOpDispSampleCount(Integer opDispSampleCount) {
		this.opDispSampleCount = opDispSampleCount;
	}
	
	public Integer getOpDispSfiCount() {
		return opDispSfiCount;
	}
	
	public void setOpDispSfiCount(Integer opDispSfiCount) {
		this.opDispSfiCount = opDispSfiCount;
	}
	
	public Integer getOpDispSafetyNetItemCount() {
		return opDispSafetyNetItemCount;
	}
	
	public void setOpDispSafetyNetItemCount(Integer opDispSafetyNetItemCount) {
		this.opDispSafetyNetItemCount = opDispSafetyNetItemCount;
	}
	
	public Integer getCapdVoucherCount() {
		return capdVoucherCount;
	}
	
	public void setCapdVoucherCount(Integer capdVoucherCount) {
		this.capdVoucherCount = capdVoucherCount;
	}
	
	public Integer getCapdItemCount() {
		return capdItemCount;
	}
	
	public void setCapdItemCount(Integer capdItemCount) {
		this.capdItemCount = capdItemCount;
	}

	public Integer getMpItemCount() {
		return mpItemCount;
	}

	public void setMpItemCount(Integer mpItemCount) {
		this.mpItemCount = mpItemCount;
	}

	public Integer getMpDispSampleCount() {
		return mpDispSampleCount;
	}

	public void setMpDispSampleCount(Integer mpDispSampleCount) {
		this.mpDispSampleCount = mpDispSampleCount;
	}

	public Integer getMpDispSfiCount() {
		return mpDispSfiCount;
	}

	public void setMpDispSfiCount(Integer mpDispSfiCount) {
		this.mpDispSfiCount = mpDispSfiCount;
	}

	public Integer getMpDispSafetyNetItemCount() {
		return mpDispSafetyNetItemCount;
	}

	public void setMpDispSafetyNetItemCount(Integer mpDispSafetyNetItemCount) {
		this.mpDispSafetyNetItemCount = mpDispSafetyNetItemCount;
	}

	public Integer getMpDispWardStockCount() {
		return mpDispWardStockCount;
	}

	public void setMpDispWardStockCount(Integer mpDispWardStockCount) {
		this.mpDispWardStockCount = mpDispWardStockCount;
	}

	public Integer getMpDispDangerDrugCount() {
		return mpDispDangerDrugCount;
	}

	public void setMpDispDangerDrugCount(Integer mpDispDangerDrugCount) {
		this.mpDispDangerDrugCount = mpDispDangerDrugCount;
	}

	public Integer getItemCount() {
		return itemCount;
	}

	public void setItemCount(Integer itemCount) {
		this.itemCount = itemCount;
	}	
}
