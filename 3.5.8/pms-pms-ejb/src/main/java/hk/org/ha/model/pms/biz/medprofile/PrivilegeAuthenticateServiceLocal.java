package hk.org.ha.model.pms.biz.medprofile;
import hk.org.ha.model.pms.vo.medprofile.AuthenticateResult;

import javax.ejb.Local;

@Local
public interface PrivilegeAuthenticateServiceLocal {
	AuthenticateResult authenticate(String userName, String password, String right);
	void destroy();
}
