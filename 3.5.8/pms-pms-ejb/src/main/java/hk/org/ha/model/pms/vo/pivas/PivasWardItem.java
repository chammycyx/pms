package hk.org.ha.model.pms.vo.pivas;

import hk.org.ha.model.pms.dms.persistence.pivas.PivasWard;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PivasWardItem implements Serializable {

	private static final long serialVersionUID = 1L;

	private boolean pivasWardEnableFlag;
	
	private String pivasWardCode;
	
	private String pivasWardDesc;
	
	private PivasWard pivasWard;

	public PivasWard getPivasWard() {
		return pivasWard;
	}

	public void setPivasWard(PivasWard pivasWard) {
		this.pivasWard = pivasWard;
	}

	public boolean isPivasWardEnableFlag() {
		return pivasWardEnableFlag;
	}

	public void setPivasWardEnableFlag(boolean pivasWardEnableFlag) {
		this.pivasWardEnableFlag = pivasWardEnableFlag;
	}

	public void setPivasWardCode(String pivasWardCode) {
		this.pivasWardCode = pivasWardCode;
	}

	public String getPivasWardCode() {
		return pivasWardCode;
	}

	public void setPivasWardDesc(String pivasWardDesc) {
		this.pivasWardDesc = pivasWardDesc;
	}

	public String getPivasWardDesc() {
		return pivasWardDesc;
	}
}
