package hk.org.ha.model.pms.udt.enquiry;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum SfiInvoiceEnqPatType implements StringValuedEnum {

	Both("B", "BOTH"),
	Public("U", "PUBLIC"),
	Private("R", "PRIVATE");

	private final String dataValue;
	private final String displayValue;
	
	SfiInvoiceEnqPatType(final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}
	
	public static SfiInvoiceEnqPatType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(SfiInvoiceEnqPatType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<SfiInvoiceEnqPatType> {

		private static final long serialVersionUID = 1L;

		@Override
		public Class<SfiInvoiceEnqPatType> getEnumClass() {
			return SfiInvoiceEnqPatType.class;
		}

	}
	
}
