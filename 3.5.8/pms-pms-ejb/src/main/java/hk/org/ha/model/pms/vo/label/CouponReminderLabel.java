package hk.org.ha.model.pms.vo.label;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;


@ExternalizedBean(type=DefaultExternalizer.class)
public class CouponReminderLabel implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Date dispDate;
	
	private String ticketNum;

	private String patName;
	
	private String caseNum;
	
	private String orderNum;
	
	private Date updateDate;
	
	private String sfiRefillNum;
	
	private String hospName;

	public Date getDispDate() {
		return dispDate;
	}

	public void setDispDate(Date dispDate) {
		this.dispDate = dispDate;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getSfiRefillNum() {
		return sfiRefillNum;
	}

	public void setSfiRefillNum(String sfiRefillNum) {
		this.sfiRefillNum = sfiRefillNum;
	}

	public String getHospName() {
		return hospName;
	}

	public void setHospName(String hospName) {
		this.hospName = hospName;
	}


}