package hk.org.ha.model.pms.biz.delivery;
import java.util.List;

import hk.org.ha.model.pms.persistence.medprofile.DeliveryRequest;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryScheduleItem;
import hk.org.ha.model.pms.udt.medprofile.DeliveryRequestGroupStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryRequestStatus;

import javax.ejb.Local;

@Local
public interface DueOrderPrintServiceLocal {
	DeliveryRequestStatus assignBatchAndPrint(DeliveryRequest request);
	DeliveryRequestStatus retrieveAssignBatchAndPrintStatus(long timeout);	
	
	DeliveryRequestGroupStatus assignBatchAndPrintByDeliveryScheduleItem(DeliveryScheduleItem deliveryScheduleItem, List<DeliveryRequest> requestList);
	DeliveryRequestGroupStatus retrieveDeliveryRequestGroupStatus(long timeout);
	void destroy();
}
