package hk.org.ha.model.pms.udt.medprofile;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum MedProfileType implements StringValuedEnum {
	
	Normal("N", "Normal"),
	Chemo("C", "Chemo");
	
    private final String dataValue;
    private final String displayValue;	
	
	private MedProfileType(String dataValue, String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	@Override
	public String getDataValue() {
		return dataValue;
	}

	@Override
	public String getDisplayValue() {
		return displayValue;
	}
	
	public static MedProfileType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MedProfileType.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<MedProfileType> {
		
		private static final long serialVersionUID = 1L;

		@Override
		public Class<MedProfileType> getEnumClass() {
			return MedProfileType.class;
		}
	}
}
