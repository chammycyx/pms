package hk.org.ha.model.pms.biz.checkissue.mp;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.medprofile.MedProfileManagerLocal;
import hk.org.ha.model.pms.biz.reftable.ActiveWardManagerLocal;
import hk.org.ha.model.pms.biz.sys.SystemMessageManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.MsWorkstoreDrug;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileTrxLog;
import hk.org.ha.model.pms.prop.Prop;
import hk.org.ha.model.pms.udt.disp.MpTrxType;
import hk.org.ha.model.pms.udt.medprofile.DeliveryItemStatus;
import hk.org.ha.model.pms.udt.medprofile.EndType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileProcessType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatus;
import hk.org.ha.model.pms.vo.label.LabelContainer;
import hk.org.ha.model.pms.vo.label.MpDispLabel;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("checkManager")
@MeasureCalls
public class CheckManagerBean implements CheckManagerLocal {
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private SystemMessageManagerLocal systemMessageManager;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private MedProfileManagerLocal medProfileManager;
	
	@In
	private ActiveWardManagerLocal activeWardManager;
		
	@In
	private Workstore workstore;
	
	private static final String JAXB_CONTEXT_LABEL = "hk.org.ha.model.pms.vo.label";
	
	public String getProblemItemException(DeliveryItem deliveryItem) {
		
		JaxbWrapper<Object> labelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
		
		MedProfilePoItem medProfilePoItem = deliveryItem.resolveMedProfilePoItem();
		MedProfileMoItem medProfileMoItem = deliveryItem.getMedProfileMoItem();
		MedProfileItem medProfileItem = medProfileMoItem.getMedProfileItem();
		
		MedProfile medProfile = deliveryItem.getMedProfile();
		
		//Inactive patient status (Home Leave / Discharge)
		if (medProfile.getStatus() != MedProfileStatus.Active) {
			return medProfile.resolveStatusString();
		}
		
		//ward transfer is detected
		Object label = labelJaxbWrapper.unmarshall(deliveryItem.getLabelXml());
		String labelWardCode = label instanceof MpDispLabel ? ((MpDispLabel) label).getWard() : (label instanceof LabelContainer ? ((LabelContainer) label).getPivasProductLabel().getWard() : null);
		
		String deliveryWardCode = deliveryItem.getDelivery().getWardCode();
		String wardCode = medProfile.getWardCode();
		if ( StringUtils.isBlank(wardCode) ) {
			wardCode = medProfile.getMedCase().getPasWardCode()+"*";
		}
		String medProfileWardCodeStatus = medProfilePoItem != null && Boolean.TRUE.equals(medProfilePoItem.getWardStockFlag()) ? "ward stock" : "normal item";
		if ( !StringUtils.equals(labelWardCode, wardCode) ) {
			if(!StringUtils.equals(deliveryWardCode, wardCode)){
				if( Boolean.TRUE.equals(medProfileMoItem.getFrozenFlag()) ){
					return systemMessageManager.resolve("0931",  labelWardCode, wardCode, medProfileWardCodeStatus);
				}else{
					//IPMOE - Ward Transfer
					if( !medProfileMoItem.isManualItem() && !activeWardManager.isActiveWard(medProfile.getPatHospCode(), medProfile.getMedCase().getPasWardCode()) ){
						//Check inactiveStatus
						Date now = new Date();
						
						if( Boolean.TRUE.equals(medProfileMoItem.getTransferFlag()) ||
							medProfileItem.getStatus() == MedProfileItemStatus.Discontinued ||
							medProfileItem.getStatus() == MedProfileItemStatus.Deleted ||
							medProfileItem.getStatus() == MedProfileItemStatus.Ended ||
							( medProfileMoItem.getEndDate() != null && now.compareTo(medProfileMoItem.getEndDate()) > 0) ){
							return systemMessageManager.resolve("0217",  labelWardCode, wardCode, medProfileWardCodeStatus);
						}else{
							return systemMessageManager.resolve("0931",  labelWardCode, wardCode, medProfileWardCodeStatus);	
						}
					}
					return systemMessageManager.resolve("0217",  labelWardCode, wardCode, medProfileWardCodeStatus);
				}
			}
		}
		
		//Inactive Item - item code is suspend
		MsWorkstoreDrug msWorkstoreDrug = dmDrugCacher.getMsWorkstoreDrug(workstore, deliveryItem.getItemCode());
		if (msWorkstoreDrug != null && "Y".equalsIgnoreCase(msWorkstoreDrug.getSuspend())) {
			return systemMessageManager.retrieveMessageDesc("0059");
		}

		//Inactive Item - order item suspend/pending
		//Drug Master level -- drug level (independent on the profile)
		//local suspend -- drug level (independent on the profile)
		
		if (medProfileItem.getStatus() == MedProfileItemStatus.Discontinued) {
			if( medProfileMoItem.isManualItem() ){
				return systemMessageManager.resolve("0932", "ended");
			}else{
				return systemMessageManager.resolve("0222", MedProfileItemStatus.Discontinued);
			}
		}
		
		//ManualEntry Item - Deleted
		if( medProfileMoItem.isManualItem() && medProfileItem.getStatus() == MedProfileItemStatus.Deleted){
			return systemMessageManager.resolve("0932", "deleted");
		}
		
		if (medProfileItem.getStatus() == MedProfileItemStatus.Ended){
			if (medProfileItem.getEndType() == EndType.EndNow){
				//End now
				return systemMessageManager.resolve("0222", "ended now");
			} else if (medProfileItem.getEndType() == EndType.CompleteAdmin){
				//Complete Admin
				return systemMessageManager.resolve("0222", "complete admin");
			} else if (medProfileItem.getEndType() == EndType.EndTimePassed || 
					medProfileItem.getEndType() == null){
				//Late refill
				return systemMessageManager.resolve("0222", "end time is passed");
			}else if( medProfileItem.getEndType() == EndType.WardTransfered ){
				return systemMessageManager.resolve("0222", EndType.WardTransfered.getDisplayValue());
			}
		}

		MedProfileMoItem latestMedProfileMoItem = medProfileItem.getMedProfileMoItem();		
		
		//Modified Order PMSIPU-593
		//Check current MOI != Latest MOI and End Date Exist
		
		if (!ObjectUtils.equals(medProfileMoItem.getItemNum(), latestMedProfileMoItem.getItemNum())) {
			return systemMessageManager.resolve("0222", "modified");
		}
		
		if (Boolean.TRUE.equals(medProfileMoItem.getTransferFlag())) {
			return systemMessageManager.resolve("0222", "transfer");
		}
		
		if (StringUtils.isNotBlank(latestMedProfileMoItem.getPendingCode())) {
			return systemMessageManager.resolve("0222", "pending");
		}
		
		if (StringUtils.isNotBlank(latestMedProfileMoItem.getSuspendCode())) {
			return systemMessageManager.resolve("0222", "suspended");
		}
		
		// IPMOE - Deleted
		if (!medProfileMoItem.isManualItem() && medProfilePoItem != null && medProfilePoItem.getOrgMedProfilePoItemId() != null) {
			boolean isDeleted = true;
			for (MedProfilePoItem poi : medProfileItem.getMedProfileMoItem().getMedProfilePoItemList()) {
				if (medProfilePoItem.getOrgMedProfilePoItemId().equals(poi.getOrgMedProfilePoItemId())) {
					isDeleted = false;
					break;
				}
			}
			if (isDeleted) {
				return systemMessageManager.resolve("0932", MedProfileItemStatus.Deleted);
			}
		}
		
		return StringUtils.EMPTY;
	}

	public void insertTrxLogAndSendMessageToCorp(MpTrxType mpTrxType, List<MedProfileMoItem> moiList) {
		// lock all corresponding medProfile at once
		Set<Long> medProfileIdList = new HashSet<Long>();	
		List<String> refNumList = new ArrayList<String>();
		for (MedProfileMoItem moi : moiList) {
			medProfileIdList.add(moi.getMedProfileItem().getMedProfile().getId());		

			if (!refNumList.contains(moi.getItemNum().toString())) {
				refNumList.add(moi.getItemNum().toString());
			}
		}
		
		Map<Long, Set<String>> mpProfileIdDulpicauteItemSet = medProfileManager.retrieveSentRefNumMap(mpTrxType, medProfileIdList, refNumList);
		
		em.createQuery(
				"select o.id from MedProfile o" + // 20130309 index check : MedProfile.id : PK_MED_PROFILE
				" where o.id in :medProfileIdList")
				.setParameter("medProfileIdList", medProfileIdList)
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
				.getResultList();
		
		// insert all the trx log
		
		List<MedProfileMoItem> moiListToSent = new ArrayList<MedProfileMoItem>();
		moiListToSent.addAll(moiList);
		for (MedProfileMoItem moi : moiList) {
			
			if(mpProfileIdDulpicauteItemSet.containsKey(moi.getMedProfileItem().getMedProfile().getId())){
				if(!mpProfileIdDulpicauteItemSet.get(moi.getMedProfileItem().getMedProfile().getId()).contains(moi.getItemNum().toString())){
					MedProfileTrxLog mptLog = new MedProfileTrxLog();
					mptLog.setRefNum(moi.getItemNum().toString());
					mptLog.setMedProfile(moi.getMedProfileItem().getMedProfile());
					mptLog.setType(mpTrxType);
					em.persist(mptLog);
					
				}else{
					//else duplicate not save
					moiListToSent.remove(moi);
				}
			}else{
				MedProfileTrxLog mptLog = new MedProfileTrxLog();
				mptLog.setRefNum(moi.getItemNum().toString());
				mptLog.setMedProfile(moi.getMedProfileItem().getMedProfile());
				mptLog.setType(mpTrxType);
				em.persist(mptLog);
			}
			
		}			
		em.flush();
		
		//CorpMessage 
		medProfileManager.directSendMessages(workstore.getHospital(), mpTrxType, moiListToSent);
	}

	public void sendMsgToCmsFromCheckedDeliveryItemList(List<DeliveryItem> deliveryItemList) {
		List<MedProfileMoItem> medProfileMoItemListForMpVetOrderList = new ArrayList<MedProfileMoItem>();
		List<MedProfileMoItem> medProfileMoItemListForMpUrgentDispenseList = new ArrayList<MedProfileMoItem>();
		Map<Long, MedProfileMoItem> medProfileMoItemMap = new HashMap<Long, MedProfileMoItem>();
		
		for (DeliveryItem deliveryItem : deliveryItemList) {
			
			MedProfileMoItem medProfileMoItem = deliveryItem.getMedProfileMoItem();
			
			if (medProfileMoItem != null && checkMedProfileProcessingRuleStatus(deliveryItem)) {
				
				if (!medProfileMoItemMap.containsKey(medProfileMoItem.getId())) {
				
					MedProfile mp =	medProfileMoItem.getMedProfileItem().getMedProfile();
					mp.getPatient();
					mp.getMedCase();
					
					medProfileMoItemListForMpVetOrderList.add(medProfileMoItem);
					
					if (medProfileMoItem.getUrgentNum() != null) {
						medProfileMoItemListForMpUrgentDispenseList.add(medProfileMoItem);
					}
					medProfileMoItemMap.put(medProfileMoItem.getId(), medProfileMoItem);
				}
			}
		}
		
		if(!medProfileMoItemListForMpVetOrderList.isEmpty()){
			//SU3 - has Vetting
			insertTrxLogAndSendMessageToCorp(MpTrxType.MpVetOrder, medProfileMoItemListForMpVetOrderList);
		}
		
		if(!medProfileMoItemListForMpUrgentDispenseList.isEmpty()){
			//UD2 - has ungentNum
			insertTrxLogAndSendMessageToCorp(MpTrxType.MpUrgentDispenseUpdate, medProfileMoItemListForMpUrgentDispenseList);
		}
	}
	
	private boolean checkMedProfileProcessingRuleStatus(DeliveryItem deliveryItem) {
		try{
	    	MedProfileProcessType processType = deliveryItem.getStatus() == DeliveryItemStatus.KeepRecord ?
	    			MedProfileProcessType.dataValueOf(Prop.MEDPROFILE_NOLABELITEM_SENDSTATUSMSG.get(workstore.getHospital())) : 
	    			MedProfileProcessType.dataValueOf(Prop.MEDPROFILE_NORMALITEM_SENDSTATUSMSG.get(workstore.getHospital()));

			return processType.equals(MedProfileProcessType.Check);
		
		}catch (Exception e){
			logger.error(e);
			return false;
		}
	}
	
}
