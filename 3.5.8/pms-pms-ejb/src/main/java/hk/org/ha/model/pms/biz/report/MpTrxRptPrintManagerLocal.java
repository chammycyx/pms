package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.persistence.medprofile.Delivery;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.report.MpTrxRpt;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface MpTrxRptPrintManagerLocal {
	
	List<MpTrxRpt> retrieveMpTrxRptListByBatchNum(String batchNum, Date dispenseDate);
	
	List<RenderAndPrintJob> printMpTrxRpt(List<Delivery> deliveryList);		// Due order label gen
	
	List<RenderAndPrintJob> printPivasTrxRpt(List<Delivery> deliveryList);		// Due order label gen
		
	List<RenderAndPrintJob> printMpTrxRpt(Delivery delivery);				// Direct Label print
	
	void reprintMpTrxRpt(Delivery delivery);								// Check
	
	void printMpTrxRptForAssignBatch(Delivery delivery);					// Assign Batch
	
	List<MpTrxRpt> retrieveMpTrxRptListByDeliveryList(List<Delivery> deliveryList); //Drug Refill List
}