package hk.org.ha.model.pms.vo.charging;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class HandleExemptAuthInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String exemptAuthBy;
	
	private String exemptAuthPw;
	
	private String errMsg;

	public String getExemptAuthBy() {
		return exemptAuthBy;
	}

	public void setExemptAuthBy(String exemptAuthBy) {
		this.exemptAuthBy = exemptAuthBy;
	}

	public String getExemptAuthPw() {
		return exemptAuthPw;
	}

	public void setExemptAuthPw(String exemptAuthPw) {
		this.exemptAuthPw = exemptAuthPw;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}	
}
