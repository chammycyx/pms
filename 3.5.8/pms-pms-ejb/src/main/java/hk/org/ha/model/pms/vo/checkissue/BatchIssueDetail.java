package hk.org.ha.model.pms.vo.checkissue;

import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.BatchIssueLogDetail;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class BatchIssueDetail implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Date issueDate;
	
	private Workstore workstore;
	
	private boolean enableIssue;
	
	private List<BatchIssueOrder> batchIssueList;
	
	private boolean overrideBatchIssueLogFlag;
	
	private List<BatchIssueLogDetail> batchIssueLogDetailList;
	
	private List<BatchIssueOrder> batchIssueSfiPrescList;

	public boolean isEnableIssue() {
		return enableIssue;
	}

	public void setEnableIssue(boolean enableIssue) {
		this.enableIssue = enableIssue;
	}

	public List<BatchIssueOrder> getBatchIssueList() {
		if ( batchIssueList == null ) {
			batchIssueList = new ArrayList<BatchIssueOrder>();
		}
		return batchIssueList;
	}

	public void setBatchIssueList(List<BatchIssueOrder> batchIssueList) {
		this.batchIssueList = batchIssueList;
	}

	public boolean isOverrideBatchIssueLogFlag() {
		return overrideBatchIssueLogFlag;
	}

	public void setOverrideBatchIssueLogFlag(boolean overrideBatchIssueLogFlag) {
		this.overrideBatchIssueLogFlag = overrideBatchIssueLogFlag;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}

	public List<BatchIssueLogDetail> getBatchIssueLogDetailList() {
		if ( batchIssueLogDetailList == null ) {
			batchIssueLogDetailList = new ArrayList<BatchIssueLogDetail>();
		}
		return batchIssueLogDetailList;
	}

	public void setBatchIssueLogDetailList(
			List<BatchIssueLogDetail> batchIssueLogDetailList) {
		this.batchIssueLogDetailList = batchIssueLogDetailList;
	}

	public void setBatchIssueSfiPrescList(List<BatchIssueOrder> batchIssueSfiPrescList) {
		this.batchIssueSfiPrescList = batchIssueSfiPrescList;
	}

	public List<BatchIssueOrder> getBatchIssueSfiPrescList() {
		if ( batchIssueSfiPrescList==null ){
			batchIssueSfiPrescList = new ArrayList<BatchIssueOrder>();
		}
		return batchIssueSfiPrescList;
	}

}