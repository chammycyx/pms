package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

public class RefillCouponDtl implements Serializable {
		
	private static final long serialVersionUID = 1L;

	private Integer itemNum;
	
	private String fullDrugDesc;
	
	private String displayInstruction;
	
	private Integer issueQty;
	
	private String baseUnit;

	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public String getFullDrugDesc() {
		return fullDrugDesc;
	}

	public void setFullDrugDesc(String fullDrugDesc) {
		this.fullDrugDesc = fullDrugDesc;
	}

	public String getDisplayInstruction() {
		return displayInstruction;
	}

	public void setDisplayInstruction(String displayInstruction) {
		this.displayInstruction = displayInstruction;
	}

	public Integer getIssueQty() {
		return issueQty;
	}

	public void setIssueQty(Integer issueQty) {
		this.issueQty = issueQty;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}
	
}
