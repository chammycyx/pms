package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class ManualPrescSummary implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String hospCode;
	
	private String specCode;
	
	private Long count;
	
	public ManualPrescSummary(String hospCode, String specCode, Long count){
		super();
		this.hospCode = hospCode;
		this.specCode = specCode;
		this.count = count;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	
}
