package hk.org.ha.model.pms.biz.enquiry;

import javax.ejb.Local;

@Local
public interface SfiDrugPriceGenDateListServiceLocal {

	void retrieveDrugPriceGenDateList();
	void destroy();	
}
