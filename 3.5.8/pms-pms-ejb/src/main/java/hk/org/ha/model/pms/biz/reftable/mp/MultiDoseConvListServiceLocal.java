package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.PmsMultiDoseConvSet;
import hk.org.ha.model.pms.udt.vetting.OrderType;

import java.util.List;

import javax.ejb.Local;

@Local
public interface MultiDoseConvListServiceLocal {

	List<PmsMultiDoseConvSet> retrievePmsMultiDoseConvSetList(DmDrug dmDrug);
	
	void updatePmsMultiDoseConvSetList(List<PmsMultiDoseConvSet> pmsMultiDoseConvSetList, DmDrug dmDrug);
	
	boolean isRetrieveSuccess();
	
	boolean isInconsistenceDdu();

	boolean isInconsistenceModu();

	boolean isSaveSuccess();

	String getErrorCode();

	String[] getErrorParam();
	
	void destroy();
}
