package hk.org.ha.model.pms.biz.reftable.pivas;

import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaExclWard;
import hk.org.ha.model.pms.vo.pivas.PivasWardInfo;
import hk.org.ha.model.pms.vo.pivas.PivasWardItem;

import java.util.List;

import javax.ejb.Local;

@Local
public interface PivasWardMaintServiceLocal {
	PivasWardInfo retrievePivasWardInfo();
	
	List<PivasWardItem> retrievePivasWardList(String patHospCode);
	
	List<PivasWardItem> retrievePivasFormulaAvailabilityInfo(Long pivasFormulaId);
	
	void updatePivasWard(List<PivasWardItem> pivasWardItemList, String patHospCode);
	
	void updatePivasFormulaExclWardList(Long pivasFormulaId, List<PivasFormulaExclWard> pivasFormulaExclWardList);
	
	void destroy();
}
