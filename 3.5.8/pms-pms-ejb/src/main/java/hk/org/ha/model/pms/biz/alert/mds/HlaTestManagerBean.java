package hk.org.ha.model.pms.biz.alert.mds;

import static hk.org.ha.model.pms.prop.Prop.ALERT_HLA_OVERRIDE_REASON;
import static hk.org.ha.model.pms.prop.Prop.PATIENT_PAS_PATIENT_ENABLED;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.UnreachableException;
import hk.org.ha.model.pms.asa.exception.pas.PasException;
import hk.org.ha.model.pms.pas.PasServiceWrapper;
import hk.org.ha.model.pms.persistence.AlertEntity;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.udt.alert.mds.AlertType;
import hk.org.ha.model.pms.vo.alert.mds.AlertHlaTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("hlaTestManager")
@MeasureCalls
public class HlaTestManagerBean implements HlaTestManagerLocal {

	@In
	private UamInfo uamInfo;
	
	@In
	private PharmOrder pharmOrder;
	
	@In
	private PasServiceWrapper pasServiceWrapper;
	
	@PersistenceContext
	private EntityManager em;
	
	@Logger
	private Log logger;
	
	public AlertEntity constructHlaTestAlert(MedOrderItem medOrderItem) {
		
		AlertEntity alertEntity = new AlertEntity();
		alertEntity.setOverrideReason(Arrays.asList(ALERT_HLA_OVERRIDE_REASON.get()));
		alertEntity.setAckUser(uamInfo.getUserId());
		alertEntity.setAckDate(Calendar.getInstance().getTime());
		alertEntity.setAlertType(AlertType.OpHla);
		
		AlertHlaTest alertHlaTest = new AlertHlaTest();
		alertHlaTest.setDisplayName(medOrderItem.getMdsDisplayName());
		alertHlaTest.setHospCode(pharmOrder.getMedOrder().getPatHospCode());
		alertHlaTest.setItemNum(Long.valueOf(medOrderItem.getItemNum().longValue()));
		alertHlaTest.setSortSeq(Long.valueOf(0));
		
		if (pharmOrder.getMedOrder().getOrderNum() == null) {
			alertHlaTest.setOrdNo(Long.valueOf(0));
		}
		else {
			alertHlaTest.setOrdNo(Long.valueOf(pharmOrder.getMedOrder().getOrderNum().substring(3)));
		}
	
		if (pharmOrder.getMedCase() != null) {
			alertHlaTest.setCaseNum(pharmOrder.getMedCase().getCaseNum());
		}
		
		alertEntity.setAlert(alertHlaTest);
		
		return alertEntity;
	}

	public boolean hasHlaTestAlertBefore(String hkid, String patHospCode, String caseNum) {
		List<String> caseNumList = retrieveCaseNumListByHkid(hkid, patHospCode, caseNum);
		if (caseNumList.isEmpty()) {
			return false;
		}
		else {
			return !retrieveHlaItemListByCaseNum(caseNumList).isEmpty();
		}
	}

	private List<String> retrieveCaseNumListByHkid(String hkid, String patHospCode, String caseNum) {
		if (PATIENT_PAS_PATIENT_ENABLED.get() && hkid != null) {
			try {
				List<String> caseNumList = new ArrayList<String>();
				Patient pasPatient = pasServiceWrapper.retrievePasPatientFullCaseListByHkid(patHospCode, hkid, false);
				if (pasPatient != null && pasPatient.getMedCaseList() != null) {
					for (MedCase medCase : pasPatient.getMedCaseList()) {
						caseNumList.add(medCase.getCaseNum());
					}
				}
				return caseNumList;
			}
			catch (PasException e) {
				logger.debug("No patient is found in HKPMI");
			}
			catch (UnreachableException e) {
				logger.debug("Hkpmi service is down");
			}
		}
		if (caseNum == null) {
			return new ArrayList<String>();
		}
		else {
			return Arrays.asList(caseNum);
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<MedOrderItem> retrieveHlaItemListByCaseNum(List<String> caseNumList) {
		List<MedOrderItem> moiList = (List<MedOrderItem>) em.createQuery(
				"select o from MedOrder o" + // 20120831 index check : MedOrder.medCase : FK_MED_ORDER_02
				" where o.medCase.caseNum in :caseNum" +
				" and o.hlaFlag = :hlaFlag")
				.setParameter("caseNum", caseNumList)
				.setParameter("hlaFlag", true)
				.getResultList();
		
		if (!moiList.isEmpty()) {
			return moiList;
		}
		
		moiList = (List<MedOrderItem>) em.createQuery(
				"select o from PharmOrder o" + // 20120831 index check : PharmOrder.medCase : FK_PHARM_ORDER_03
				" where o.medCase.caseNum in :caseNum" +
				" and o.medOrder.hlaFlag = :hlaFlag")
				.setParameter("caseNum", caseNumList)
				.setParameter("hlaFlag", true)
				.getResultList();
		
		return moiList;
	}

}
