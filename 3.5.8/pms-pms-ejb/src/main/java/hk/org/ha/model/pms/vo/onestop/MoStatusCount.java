package hk.org.ha.model.pms.vo.onestop;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class MoStatusCount implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Integer unvetCount;
	
	private Integer suspendCount;
	
	private Date lastCallingTime;

	public Integer getUnvetCount() {
		return unvetCount;
	}

	public void setUnvetCount(Integer unvetCount) {
		this.unvetCount = unvetCount;
	}

	public Integer getSuspendCount() {
		return suspendCount;
	}

	public void setSuspendCount(Integer suspendCount) {
		this.suspendCount = suspendCount;
	}

	public void setLastCallingTime(Date lastCallingTime) {
		this.lastCallingTime = lastCallingTime;
	}

	public Date getLastCallingTime() {
		return lastCallingTime;
	}
	
}