package hk.org.ha.model.pms.biz.enquiry;

import javax.ejb.Local;

@Local
public interface DrugChargeClearanceEnqServiceLocal {

	void retrieveDrugClearance(String searchCriteria);
	void checkClearanceForOnestop(Long medOrderId);
	void checkClearanceForOnestopManual(String patHospCode, Long orderNum, Long pasApptSeq);
	
	void destroy();

}
