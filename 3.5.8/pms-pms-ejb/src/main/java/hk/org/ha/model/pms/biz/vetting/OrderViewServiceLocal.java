package hk.org.ha.model.pms.biz.vetting;

import hk.org.ha.model.pms.exception.vetting.VettingException;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;
import hk.org.ha.model.pms.vo.vetting.RetrievePatientResult;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface OrderViewServiceLocal {

	void retrieveOrder(Long id, OneStopOrderType orderType, Boolean requiredLockOrder);
	
	void saveOrder(OrderViewInfo orderViewInfo) throws VettingException;
	
	void cancelOrder(Boolean suspendFlag, String suspendCode);
	
	void removeOrder(String logonUser, String remarkText);
	
	void removeSfiRefillOrder(Long refillScheduleId);
	
	void removeMedOrderItem(OrderViewInfo inOrderViewInfo, int itemIndex, Date remarkCreateDate, String remarkCreateUser, String remarkText);
	
	void updateOrderFromOrderView(OrderViewInfo inOrderViewInfo);
	
	void updatePharmRemark(OrderViewInfo inOrderViewInfo, int itemIndex, Date remarkCreateDate, String remarkCreateUser, String remarkText);
			
	RetrievePatientResult retrievePatientByHkid(String hkid);
	
	List<String> usePatientFromHkpmi(Patient patient);
	
	void destroy();
}
