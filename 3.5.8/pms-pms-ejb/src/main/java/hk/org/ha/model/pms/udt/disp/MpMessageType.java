package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum MpMessageType implements StringValuedEnum {

	// From MOE Ordering to PMS
	NewOrder("N", "newOrder", "create"),
	UpdateOrder("U", "updateOrder", "update"),
	DeleteOrder("D", "deleteOrder", "delete"),
	DiscontinueOrder("T", "discontinueOrder", "discontinue"),
	UpdatePregnancy("P", "updatePregnancy", "pregnancy"),
	AmendSchedule("S", "amendSchedule", "amendschedule"),
	CustomSchedule("C", "customSchedule", "customschedule"),
	MarkAnnotation("O", "markAnnotation", "annotation"),
	ModifyMds("MM", "modifyMds", "modifymds"),
	ModifyReviewDate("MR", "modifyReviewDate", "modifyreviewdate"),
	
	// From MOE Admin to PMS
	AdminRecord("A", "adminRecord", "admin"),
	Urgent("G", "urgent", "urgent"),
	Replenishment("R", "replenishment", "replenish"),
	CompleteAdmin("CA", "completeAdmin", "completeadmin"),
	Withhold("W", "withhold", "withhold"),
	
	// From PMS to MOE Ordering
	VetOrder("V", "vetOrder", "vet"),
	UpdateDispensing("UD", "updateDispensing", "vet"),
	VetDischarge("VD", "vetDischarge", "vetdischarge"),
	
	// From PMS to MOE Admin
	PendOrder("E", "pendOrder", "pend");
	
	
	private final String dataValue;
	private final String displayValue;
	private final String packageName;
	
	MpMessageType (final String dataValue, final String displayValue, final String packageName) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
		this.packageName = packageName;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}

	public String getPackageName() {
		return this.packageName;
	}
	
	public static MpMessageType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MpMessageType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<MpMessageType> {

		private static final long serialVersionUID = 1L;

		public Class<MpMessageType> getEnumClass() {
    		return MpMessageType.class;
    	}
    }
}