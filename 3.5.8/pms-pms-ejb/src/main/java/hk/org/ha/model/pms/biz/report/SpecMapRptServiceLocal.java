package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.dms.persistence.PmsSpecMapping;
import hk.org.ha.model.pms.dms.vo.pms.PmsSpecMapReportCriteria;

import java.util.List;

import javax.ejb.Local;

@Local
public interface SpecMapRptServiceLocal {
	
	void exportSpecMapRpt();
	
	void printSpecMapRpt();
	
	void retrieveSpecMapRpt(PmsSpecMapReportCriteria pmsSpecMapReportCriteria); 
	
	List<PmsSpecMapping> retrievePmsSpecMappingList(PmsSpecMapReportCriteria pmsSpecMapReportCriteria);
	
	void retrieveSpecMapRptList();
	
	void clearSpecMapRpt();
	
	void destroy();
	
}
