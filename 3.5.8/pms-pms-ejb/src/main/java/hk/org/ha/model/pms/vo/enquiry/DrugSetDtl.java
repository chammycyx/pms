package hk.org.ha.model.pms.vo.enquiry;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

import hk.org.ha.model.pms.persistence.disp.MedOrderItem;

@ExternalizedBean(type=DefaultExternalizer.class)
public class DrugSetDtl implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private MedOrderItem medOrderItem;
	
	private String subLevel;

	public MedOrderItem getMedOrderItem() {
		return medOrderItem;
	}

	public void setMedOrderItem(MedOrderItem medOrderItem) {
		this.medOrderItem = medOrderItem;
	}

	public void setSubLevel(String subLevel) {
		this.subLevel = subLevel;
	}

	public String getSubLevel() {
		return subLevel;
	}	
}
