package hk.org.ha.model.pms.vo.alert.mds;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class ErrorInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String desc;
	
	private String cause;
	
	private String action;
	
	private Boolean suppressFlag;

	public ErrorInfo() {
		suppressFlag = Boolean.FALSE;
	}
	
	public ErrorInfo(String desc) {
		this(desc, null, null);
	}
	
	public ErrorInfo(String desc, String cause) {
		this(desc, cause, null);
	}
	
	public ErrorInfo(String desc, String cause, String action) {
		this();
		this.desc   = desc;
		this.cause  = cause;
		this.action = action;
	}
	
	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getCause() {
		return cause;
	}

	public void setCause(String cause) {
		this.cause = cause;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Boolean getSuppressFlag() {
		return suppressFlag;
	}
	
	public void setSuppressFlag(Boolean suppressFlag) {
		this.suppressFlag = suppressFlag;
	}
}
