package hk.org.ha.model.pms.biz.order;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.biz.order.OrderHelper;
import hk.org.ha.model.pms.persistence.disp.Patient;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("patientManager")
@MeasureCalls
public class PatientManagerBean implements PatientManagerLocal {

	@PersistenceContext
	private EntityManager em;
	
	@In
	private OrderHelper orderHelper;
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Patient savePatient(Patient remotePatient) {		
		return orderHelper.savePatient(em, remotePatient);
	}
}
