package hk.org.ha.model.pms.biz.drug;
import java.util.List;

import hk.org.ha.model.pms.dms.vo.RouteForm;
import hk.org.ha.model.pms.dms.vo.RouteFormSearchCriteria;

import javax.ejb.Local;

@Local
public interface RouteFormListServiceLocal {

	List<RouteForm> retrieveRouteFormList(RouteFormSearchCriteria criteria);
	
	void destroy();	
}
