package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.asa.exception.pas.PasException;
import hk.org.ha.model.pms.persistence.reftable.DrsConfig;
import hk.org.ha.model.pms.udt.patient.PasSpecialtyType;
import hk.org.ha.model.pms.vo.patient.PasSpecialty;
import hk.org.ha.model.pms.vo.reftable.DrsConfigInfo;

import java.util.Collection;
import java.util.List;

import javax.ejb.Local;

@Local
public interface DrsConfigListServiceLocal 
{	
	DrsConfigInfo retrieveDrsConfigList() throws PasException;
	
	List<PasSpecialty> retrievePasSpecList(String prefixSpecCode, PasSpecialtyType specType) throws PasException;
	
	void updateDrsConfigList(Collection<DrsConfig> updateDrsConfigList, Collection<DrsConfig> delDrsConfigList) throws PasException;
	
	void destroy();
}
