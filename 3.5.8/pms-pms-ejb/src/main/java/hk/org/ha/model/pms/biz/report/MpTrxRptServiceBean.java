package hk.org.ha.model.pms.biz.report;

import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.delivery.DeliveryManagerLocal;
import hk.org.ha.model.pms.biz.reftable.MpWardManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.Delivery;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.udt.report.ReportFilterOption;
import hk.org.ha.model.pms.vo.medprofile.Ward;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("mpTrxRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MpTrxRptServiceBean implements MpTrxRptServiceLocal {

	public static final int MAX_DRUG_ORDER_TLF_LENGTH = 90;
	public static final String TLF_BREAK_CHAR = "<span breakOpportunity=\"any\"> </span>";
	public static final String NEW_LINE = "\n";
	
	@In
	private MpWardManagerLocal mpWardManager;
	
	@In
	private DeliveryManagerLocal deliveryManager;
	
	@In 
	private Workstore workstore;
	
	@In
	private AuditLogger auditLogger; 


	private boolean retrieveSuccess;
	
	private MpTrxRptComparator mpTrxRptComparator = new MpTrxRptComparator();
	
	private List<Ward> mpTrxRptWardSelectionList;
	
	@Out(required = false)
	private List<Delivery> mpTrxRptDeliveryList;
	
	public List<Ward> retrieveWardSelectionList() {
		mpTrxRptWardSelectionList = mpWardManager.retrieveActiveWardList(workstore.getWorkstoreGroup());
		mpTrxRptWardSelectionList.add(0, new Ward(ReportFilterOption.All.getDataValue()));
		return mpTrxRptWardSelectionList;
	}
	
	public void retrieveDeliveryList(String wardCode, String batchCode, String caseNum, Date dispenseDate) {
		
		TreeMap<Long, Delivery> deliveryMap = new TreeMap<Long, Delivery>();

		List<DeliveryItem> deliveryItemListWithoutReplenishment = deliveryManager.retrieveDeliveryItemList(wardCode, batchCode, caseNum, dispenseDate, null);
		for ( DeliveryItem deliveryItem : deliveryItemListWithoutReplenishment ){
			deliveryMap.put(deliveryItem.getDelivery().getId(),deliveryItem.getDelivery());
		}
				
		deliveryMap = deliveryManager.retrieveDeliveryNumOfLabel(deliveryMap);
		
		mpTrxRptDeliveryList = new ArrayList<Delivery>();
		mpTrxRptDeliveryList.addAll(deliveryMap.values());
		
		Collections.sort(mpTrxRptDeliveryList, mpTrxRptComparator);
		
		auditLogger.log("#0186", wardCode, caseNum, batchCode);

	}
    
	@Remove
	public void destroy() {
		
		if (mpTrxRptDeliveryList != null) {
			mpTrxRptDeliveryList = null;
		}
		
		if(mpTrxRptWardSelectionList!=null){
			mpTrxRptWardSelectionList=null;
		}
		
	}

	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}
	
	private static class MpTrxRptComparator implements Comparator<Delivery>, Serializable {
		private static final long serialVersionUID = 1L;

		@Override
          public int compare(Delivery o1, Delivery o2) {
                return new CompareToBuilder()
                	  .append( o1.getBatchNum(), o2.getBatchNum())
                      .toComparison();
          }
    }
	
}
