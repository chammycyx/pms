package hk.org.ha.model.pms.biz.checkissue.mp;

import static hk.org.ha.model.pms.prop.Prop.CHARGING_SFI_CHECKISSUE_MP_ISSUEINVOICE;
import static hk.org.ha.model.pms.prop.Prop.CHECK_MEDPROFILE_SENDOUTLIST_DURATION;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.sys.SystemMessageManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.Delivery;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.persistence.medprofile.PurchaseRequest;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryItemStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryRequestStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryStatus;
import hk.org.ha.model.pms.vo.checkissue.mp.SentDelivery;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;
import org.joda.time.DateTime;


@Stateful
@AutoCreate
@Scope(ScopeType.SESSION)
@Name("sendService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SendServiceBean implements SendServiceLocal {
	
	@PersistenceContext
	private EntityManager em;
	
	@Logger
	private Log logger;
	
	@In
	private PharmSendOutClientNotifier pharmSendOutClientNotifier;
	
	@In
	private SystemMessageManagerLocal systemMessageManager;

	@In
	private Workstore workstore;
	
	@In
	private AuditLogger auditLogger;
	
	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
	
	@Out(required = false)
	private SentDelivery sentDelivery;
	
	@Out(required = false)
	private List<SentDelivery> readyToSendDeliveryList; 
	
	@Out(required = false)
	private List<SentDelivery> sentDeliveryFromBatchSendList;
	
	private ReadyToSendDeliveryListComparator readyToSendDeliveryListComparator = new ReadyToSendDeliveryListComparator();
	
	private boolean autoUpdate = true;
	
	private DateFormat df = new SimpleDateFormat("yyyyMMdd"); 
	
	private static final String SEND_OUT = "sendOut";
	
	@SuppressWarnings("unchecked")
	public void sendDelivery(Date batchDate, String batchNum) {
		
		HashMap<String,String> aduitDeliveryMap = new HashMap<String,String> ();
		
		sentDelivery = new SentDelivery();
		sentDelivery.setBatchNum(batchNum);
		sentDelivery.setDeliveryDate(new Date());
		
		List<Delivery> deliveryList = em.createQuery(
				"select o from Delivery o" + // 20120317 index check : Delivery.hospital,batchDate,batchNum : I_DELIVERY_01
				" where o.hospital = :hospital" +
				" and o.batchDate = :batchDate" + 
				" and o.batchNum = :batchNum" +
				" and o.deliveryRequest.status = :deliveryRequestStatus")
				.setParameter("hospital", workstore.getHospital())
				.setParameter("batchDate", batchDate)
				.setParameter("batchNum", batchNum)
				.setParameter("deliveryRequestStatus", DeliveryRequestStatus.Completed)
				.getResultList();
		
		if ( deliveryList.isEmpty() ) {
			sentDelivery.setMsgCode("0005");
			sentDelivery.setStatus(systemMessageManager.retrieveMessageDesc("0005"));
			return;
		}
		
		Delivery delivery = deliveryList.get(0);
		
		sentDelivery.setDeliveryId(delivery.getId());
		sentDelivery.setBatchNum(delivery.getBatchNum());
		sentDelivery.setWardCode(delivery.getWardCode());
		sentDelivery.setPrintMode(delivery.getPrintMode());
		sentDelivery.setBatchDate(delivery.getBatchDate());
		
		String previousStatus = delivery.getStatus().getDataValue();
		
		if ( delivery.getStatus() == DeliveryStatus.Checked ) {
			delivery.setStatus(DeliveryStatus.Delivered);
			delivery.setDeliveryDate(new Date());
			updateDeliveryStatusToDelivered(delivery);
			
			aduitDeliveryMap.put(df.format(delivery.getBatchDate())+delivery.getBatchNum(), previousStatus + "=>" + delivery.getStatus().getDataValue());
			auditLogger.log("#0677",  aduitDeliveryMap);
			
			sentDelivery.setDeliveryDate(delivery.getDeliveryDate());
			sentDelivery.setStatus(DeliveryStatus.Delivered.getDisplayValue());
			
			updateFcsSfiInvoiceStatus(Arrays.asList(delivery));
		} else if ( delivery.getStatus() == DeliveryStatus.Delivered ) {
			sentDelivery.setMsgCode("0286");
			sentDelivery.setStatus(systemMessageManager.retrieveMessageDesc("0286"));
		} else if ( delivery.getStatus() == DeliveryStatus.KeepRecord || delivery.getStatus() == DeliveryStatus.Deleted ) {
			sentDelivery.setMsgCode("0659");
			sentDelivery.setStatus(systemMessageManager.retrieveMessageDesc("0659"));
		} else {
			sentDelivery.setMsgCode("0285");
			sentDelivery.setStatus(systemMessageManager.retrieveMessageDesc("0285"));
		}
		
		if(autoUpdate){
			dispatchUpdateEvent(deliveryList.get(0).getDeliveryRequest().getWorkstore());
		}
		
		
	}
	
	
	public void readyToSendDelivery() {
		
		readyToSendDeliveryList = new ArrayList<SentDelivery>();
		List<Delivery> deliveryList = getCheckedDeliveryList();
		
		
		if ( deliveryList.isEmpty() ) {
			return;
		}
		
		for(Delivery delivery : deliveryList){
		
			SentDelivery sentDeliveryForUpdate = new SentDelivery();
			sentDeliveryForUpdate.setDeliveryId(delivery.getId());
			sentDeliveryForUpdate.setCheckDate(delivery.getCheckDate());
			sentDeliveryForUpdate.setBatchNum(delivery.getBatchNum());
			sentDeliveryForUpdate.setWardCode(delivery.getWardCode());
			sentDeliveryForUpdate.setPrintMode(delivery.getPrintMode());
			sentDeliveryForUpdate.setBatchDate(delivery.getBatchDate());
			sentDeliveryForUpdate.setDeliveryDate(delivery.getDeliveryDate());
			sentDeliveryForUpdate.setStatus(delivery.getStatus().getDisplayValue());
			if(!StringUtils.equals(delivery.getStatus().toString(),DeliveryStatus.Delivered.getDisplayValue())){
				readyToSendDeliveryList.add(sentDeliveryForUpdate);
			}
			
		}
		
		Collections.sort(readyToSendDeliveryList, readyToSendDeliveryListComparator);
		
	}
	
	@SuppressWarnings("unchecked")
	private List<Delivery> getCheckedDeliveryList(){
		return em.createQuery(
				"select o from Delivery o " + // 20130125 index check : Delivery.hospital,status : I_DELIVERY_05
				" where o.hospital = :hospital " +
				" and o.status = :status" +
				" and o.deliveryRequest.status = :deliveryRequestStatus" +
				" and o.batchDate >= :startDate")
				.setParameter("hospital", workstore.getHospital())
				.setParameter("status", DeliveryStatus.Checked)
				.setParameter("deliveryRequestStatus", DeliveryRequestStatus.Completed)
				.setParameter("startDate", new DateTime().minusDays(CHECK_MEDPROFILE_SENDOUTLIST_DURATION.get() - 1).toDate())
				.getResultList();
	}
	
	
	public List<SentDelivery> batchSendDelivery(List<SentDelivery> readyToSendDeliveryList) {
		HashMap<String,String> aduitDeliveryMap = new HashMap<String,String> ();
		
		Map<Long, SentDelivery> readyToSendDeliveryMarkSentMap = new HashMap<Long, SentDelivery>();
		sentDeliveryFromBatchSendList= new ArrayList<SentDelivery>();
		for ( SentDelivery sentDeliveryToUpdate : readyToSendDeliveryList ) {
			if ( sentDeliveryToUpdate.isCheckedToSend() ) {
				readyToSendDeliveryMarkSentMap.put(sentDeliveryToUpdate.getDeliveryId(), sentDeliveryToUpdate);
			}
		}
		
		List<Delivery> deliveryList = getCheckedDeliveryList();
		if ( deliveryList.isEmpty() ) {
			return null;
		}
		List<Delivery> deliveryListForInvoice = new ArrayList<Delivery>();
		
		for(Delivery delivery : deliveryList){
			if ( readyToSendDeliveryMarkSentMap.containsKey(delivery.getId())){
				aduitDeliveryMap.put(df.format(delivery.getBatchDate())+delivery.getBatchNum(), delivery.getStatus().getDataValue() + "=>" + DeliveryStatus.Delivered.getDataValue());
				
				delivery.setStatus(DeliveryStatus.Delivered);
				delivery.setDeliveryDate(new Date());
				updateDeliveryStatusToDelivered(delivery);
				
				SentDelivery sentDeliveryToUpdate = readyToSendDeliveryMarkSentMap.get(delivery.getId());
				sentDeliveryToUpdate.setStatus(DeliveryStatus.Delivered.getDisplayValue());
				sentDeliveryToUpdate.setDeliveryDate(new Date());
				sentDeliveryFromBatchSendList.add(sentDeliveryToUpdate);
				
				deliveryListForInvoice.add(delivery);
			}
		}
		
		if(autoUpdate){
			dispatchUpdateEvent(deliveryList.get(0).getDeliveryRequest().getWorkstore());
		}
		
		auditLogger.log("#0677",  aduitDeliveryMap);
		
		if ( !deliveryListForInvoice.isEmpty() ) {
			updateFcsSfiInvoiceStatus(deliveryListForInvoice);
		}
		
		return sentDeliveryFromBatchSendList;
	}
	
	public void updateDeliveryDate(SentDelivery inSentDelivery){
		sentDelivery = new SentDelivery();
		sentDelivery.setDeliveryDate(new Date());
		sentDelivery.setBatchNum(inSentDelivery.getBatchNum());
		
		Delivery delivery = em.find(Delivery.class, inSentDelivery.getDeliveryId());
		
		if ( delivery == null ) {
			sentDelivery.setMsgCode("0005");
			sentDelivery.setStatus(systemMessageManager.retrieveMessageDesc("0005"));
		} else {
			delivery.setDeliveryDate(new Date());
			sentDelivery.setDeliveryId(delivery.getId());
			sentDelivery.setBatchDate(delivery.getBatchDate());
			sentDelivery.setBatchNum(delivery.getBatchNum());
			sentDelivery.setWardCode(delivery.getWardCode());
			sentDelivery.setPrintMode(delivery.getPrintMode());
			sentDelivery.setStatus(delivery.getStatus().getDisplayValue());
			
			auditLogger.log("#0681",  df.format(delivery.getBatchDate())+ delivery.getBatchNum()+"="+delivery.getStatus().getDataValue()+"=>"+delivery.getStatus().getDataValue());
			
		}
		
	
		
	}
	
	private void updateDeliveryStatusToDelivered(Delivery delivery) {
	
		for ( DeliveryItem deliveryItem : delivery.getDeliveryItemList() ) {
			if ( deliveryItem.getStatus() != DeliveryItemStatus.Deleted && 
					deliveryItem.getStatus() != DeliveryItemStatus.KeepRecord && 
					deliveryItem.getStatus() != DeliveryItemStatus.Unlink ) {
				deliveryItem.setStatus(DeliveryItemStatus.Delivered);
			}
		}
		
	}
	
	private void updateFcsSfiInvoiceStatus(List<Delivery> deliveryList) {
		if( !StringUtils.equals(SEND_OUT, CHARGING_SFI_CHECKISSUE_MP_ISSUEINVOICE.get()) ){
			return;
		}
		
		List<String> invoiceNumList = new ArrayList<String>();
		for ( Delivery delivery : deliveryList ) {
			for ( DeliveryItem deliveryItem : delivery.getDeliveryItemList() ) {
				PurchaseRequest purchaseRequest = deliveryItem.getPurchaseRequest();
				if ( isActiveInvoice(purchaseRequest) ) {
					invoiceNumList.add(purchaseRequest.getInvoiceNum());
				}
			}
		}
		if ( !invoiceNumList.isEmpty() ) {
			corpPmsServiceProxy.updateFcsSfiInvoiceStatus(invoiceNumList);
		}
	}
	
	private boolean isActiveInvoice(PurchaseRequest purchaseRequest){
		if ( purchaseRequest == null ) {
			return false;
		}
		return (purchaseRequest.getInvoiceStatus() != InvoiceStatus.Void && purchaseRequest.getInvoiceStatus() != InvoiceStatus.SysDeleted);
	}

	@Remove
	public void destroy() {
		if ( sentDelivery != null ) {
			sentDelivery = null;
		}
	}
	
	private static class ReadyToSendDeliveryListComparator implements Comparator<SentDelivery>, Serializable
    {
		private static final long serialVersionUID = 1L;

		@Override
          public int compare(SentDelivery o1, SentDelivery o2) {
                return new CompareToBuilder()
                	  .append( o1.getBatchDate(), o2.getBatchDate() )
                	  .append( o1.getBatchNum(), o2.getBatchNum() )
                      .toComparison();
          }
    }
	
	
	public void dispatchUpdateEvent(Workstore workstore) {
		
		try {
			Map<String, Object> headerParams = new HashMap<String, Object>();
			
			if (workstore != null) {
				headerParams.put("HOSP_CODE", workstore.getHospCode());
				headerParams.put("WORKSTORE_CODE", workstore.getWorkstoreCode());
			}
			
			pharmSendOutClientNotifier.notifyClient("NOTIFICATION FROM SendOutServiceBean",
					headerParams);

			logger.info("Notification has been sent");
		} catch (Exception ex) {
			logger.error(ex, ex);
		}
	}

	
}
