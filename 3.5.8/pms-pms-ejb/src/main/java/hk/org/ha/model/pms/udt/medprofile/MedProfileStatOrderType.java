package hk.org.ha.model.pms.udt.medprofile;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum MedProfileStatOrderType implements StringValuedEnum {
	
	Normal("N", "Normal"),
	Urgent("U", "Urgent"),
	PendingSuspend("P", "PendingSuspend"),
	All("A", "All");

    private final String dataValue;
    private final String displayValue;
    
	private MedProfileStatOrderType(String dataValue, String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
    
	@Override
	public String getDataValue() {
		return dataValue;
	}

	@Override
	public String getDisplayValue() {
		return displayValue;
	}
	
    public static MedProfileStatOrderType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MedProfileStatOrderType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<MedProfileStatOrderType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<MedProfileStatOrderType> getEnumClass() {
    		return MedProfileStatOrderType.class;
    	}
    }
}
