package hk.org.ha.model.pms.vo.rx;

import hk.org.ha.model.pms.udt.vetting.FixedConcnSource;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="InjectionRxDrug")
@ExternalizedBean(type=DefaultExternalizer.class)
public class InjectionRxDrug extends RxDrug implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement
	private Boolean noDiluentRequireFlag;

	@XmlElement
	private FixedConcnSource fixedConcnSource;

	@XmlElement
	private Integer fixedConcnCode;

	@XmlElement
	private String fixedConcnDesc;

	@XmlElement
	private BigDecimal fixedConcnRatio;
	
	public InjectionRxDrug() {
		super();
		noDiluentRequireFlag = Boolean.FALSE;
		fixedConcnSource = FixedConcnSource.None;
	}
	
	public Boolean getNoDiluentRequireFlag() {
		return noDiluentRequireFlag;
	}

	public void setNoDiluentRequireFlag(Boolean noDiluentRequireFlag) {
		this.noDiluentRequireFlag = noDiluentRequireFlag;
	}

	public FixedConcnSource getFixedConcnSource() {
		return fixedConcnSource;
	}

	public void setFixedConcnSource(FixedConcnSource fixedConcnSource) {
		this.fixedConcnSource = fixedConcnSource;
	}

	public Integer getFixedConcnCode() {
		return fixedConcnCode;
	}

	public void setFixedConcnCode(Integer fixedConcnCode) {
		this.fixedConcnCode = fixedConcnCode;
	}

	public String getFixedConcnDesc() {
		return fixedConcnDesc;
	}

	public void setFixedConcnDesc(String fixedConcnDesc) {
		this.fixedConcnDesc = fixedConcnDesc;
	}

	public BigDecimal getFixedConcnRatio() {
		return fixedConcnRatio;
	}

	public void setFixedConcnRatio(BigDecimal fixedConcnRatio) {
		this.fixedConcnRatio = fixedConcnRatio;
	}
}
