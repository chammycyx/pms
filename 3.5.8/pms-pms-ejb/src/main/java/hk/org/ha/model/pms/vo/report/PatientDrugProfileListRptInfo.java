package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class PatientDrugProfileListRptInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String hkidCaseNum;
	
	private List<String> wardCodeList;
	
	private Boolean printEndedItemFlag;
	
	private Date startItemEndedDate;

	public String getHkidCaseNum() {
		return hkidCaseNum;
	}

	public void setHkidCaseNum(String hkidCaseNum) {
		this.hkidCaseNum = hkidCaseNum;
	}

	public List<String> getWardCodeList() {
		return wardCodeList;
	}

	public void setWardCodeList(List<String> wardCodeList) {
		this.wardCodeList = wardCodeList;
	}

	public Boolean getPrintEndedItemFlag() {
		return printEndedItemFlag;
	}

	public void setPrintEndedItemFlag(Boolean printEndedItemFlag) {
		this.printEndedItemFlag = printEndedItemFlag;
	}

	public Date getStartItemEndedDate() {
		return startItemEndedDate;
	}

	public void setStartItemEndedDate(Date startItemEndedDate) {
		this.startItemEndedDate = startItemEndedDate;
	}
}
