package hk.org.ha.model.pms.udt.alert;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DiseaseType implements StringValuedEnum {
	
	G6pd("G", "G6PD"),
	Pregnancy("P", "Pregnancy");

    private final String dataValue;
    private final String displayValue;
    
	DiseaseType(final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}

	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}

	public static DiseaseType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DiseaseType.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<DiseaseType> {
		
		private static final long serialVersionUID = 1L;

		public Class<DiseaseType> getEnumClass() {
    		return DiseaseType.class;
    	}

	}
	
}
