package hk.org.ha.model.pms.biz.security;
import javax.ejb.Local;

@Local
public interface SecurityServiceLocal {

	void logoff();
	
	void destroy();
}
