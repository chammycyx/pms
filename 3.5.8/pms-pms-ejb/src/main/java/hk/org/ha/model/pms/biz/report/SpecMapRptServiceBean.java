package hk.org.ha.model.pms.biz.report;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.dms.persistence.PmsSpecMapping;
import hk.org.ha.model.pms.dms.vo.pms.PmsSpecMapReportCriteria;
import hk.org.ha.model.pms.persistence.corp.HospitalMapping;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.report.ReportFilterOption;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsRptServiceJmsRemote;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("specMapRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SpecMapRptServiceBean implements SpecMapRptServiceLocal {
		
	@In
	private Workstore workstore;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent; 
	
    @In
    private DmsRptServiceJmsRemote dmsRptServiceProxy;	
	
	@Out(required = false)
	private List<String> patHospCodeStrList;

	@Out(required = false)
	private List<String> pasSpecialtyStrList;
	
	@Out(required = false)
	private List<String> pasSubSpecialtyStrList;
	
	@Out(required = false)
	private List<String> wardStrList;
	
	@Out(required = false)
	private List<String> dispHospCodeStrList;
	
	@Out(required = false)
	private List<String> phsSpecialtyStrList;

	@Out(required = false)
	private List<String> phsWardStrList;

	@Out(required = false)
	private List<String> dispWorkstoreStrList;

	@Out(required = false)
	private List<PmsSpecMapping> specMapRptPmsSpecMappingList;
	
	private List<PmsSpecMapping> pmsSpecMappingList;
	private List<String> patHospList;

	public void exportSpecMapRpt() {
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		PrintOption po = new PrintOption();
		po.setDocType(ReportProvider.XLS);
		printAgent.renderAndRedirect(new RenderJob(
				"SpecMapXls", 
				po, 
				parameters, 
				pmsSpecMappingList));

	}

	public void printSpecMapRpt(){

		Map<String, Object> parameters = new HashMap<String, Object>();
		printAgent.renderAndPrint(new RenderAndPrintJob(
				"SpecMapRpt", 
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				parameters, 
				specMapRptPmsSpecMappingList));
		
	}

	public void retrieveSpecMapRpt(PmsSpecMapReportCriteria pmsSpecMapReportCriteria){
		
		if (pmsSpecMappingList == null) {

			pmsSpecMappingList = new ArrayList<PmsSpecMapping>();
			
			pmsSpecMappingList = retrievePmsSpecMappingList(pmsSpecMapReportCriteria);

			if (pmsSpecMappingList.size() > 0) {
				
		    	Collections.sort(pmsSpecMappingList, new SpecMapRptComparator());
		    	
				retrieveSpecMapRptList();
			}
			
		}
		
		specMapRptPmsSpecMappingList = new ArrayList<PmsSpecMapping>();
		
		for (PmsSpecMapping pmsSpecMapping : pmsSpecMappingList) {
			
			if (pmsSpecMapReportCriteria.getDispWorkstore() == null || 
					pmsSpecMapReportCriteria.getDispWorkstore().equals(pmsSpecMapping.getDispWorkstore())) {

				if ((pmsSpecMapReportCriteria.getSpecialtyType() == null || 
						pmsSpecMapReportCriteria.getSpecialtyType().equals(pmsSpecMapping.getSpecialtyType())) &&
						(pmsSpecMapReportCriteria.getPatHospCode() == null || 
						pmsSpecMapReportCriteria.getPatHospCode().equals(pmsSpecMapping.getPatHospCode())) &&
						(pmsSpecMapReportCriteria.getPasSpecialty() == null || 
						pmsSpecMapReportCriteria.getPasSpecialty().equals(pmsSpecMapping.getPasSpecialty())) &&
						(pmsSpecMapReportCriteria.getPrivateFlag() == null || 
						pmsSpecMapReportCriteria.getPrivateFlag().equals(pmsSpecMapping.getPrivateFlag())) &&
						(pmsSpecMapReportCriteria.getDispHospCode() == null || 
						pmsSpecMapReportCriteria.getDispHospCode().equals(pmsSpecMapping.getDispHospCode())) &&
						(pmsSpecMapReportCriteria.getPhsSpecialty() == null || 
						pmsSpecMapReportCriteria.getPhsSpecialty().equals(pmsSpecMapping.getPhsSpecialty())) &&
						(pmsSpecMapReportCriteria.getDefaultWorkstore() == null || 
						pmsSpecMapReportCriteria.getDefaultWorkstore().equals(pmsSpecMapping.getDefaultWorkstore())) &&
						("*".equals(pmsSpecMapReportCriteria.getPasSubSpecialty()) ||
						(pmsSpecMapReportCriteria.getPasSubSpecialty() == null && StringUtils.isBlank(pmsSpecMapping.getPasSubSpecialty())) || 
						(pmsSpecMapReportCriteria.getPasSubSpecialty() != null && pmsSpecMapReportCriteria.getPasSubSpecialty().equals(pmsSpecMapping.getPasSubSpecialty()))) &&
						("*".equals(pmsSpecMapReportCriteria.getWard()) ||
						(pmsSpecMapReportCriteria.getWard() == null && StringUtils.isBlank(pmsSpecMapping.getWard())) || 
						(pmsSpecMapReportCriteria.getWard() != null && pmsSpecMapReportCriteria.getWard().equals(pmsSpecMapping.getWard()))) &&
						("*".equals(pmsSpecMapReportCriteria.getPhsWard()) ||
						(pmsSpecMapReportCriteria.getPhsWard() == null && StringUtils.isBlank(pmsSpecMapping.getPhsWard())) || 
						(pmsSpecMapReportCriteria.getPhsWard() != null && pmsSpecMapReportCriteria.getPhsWard().equals(pmsSpecMapping.getPhsWard()))))  {

					if (pmsSpecMapping.getPasSpecialty() == null && pmsSpecMapping.getPasSubSpecialty()==null && pmsSpecMapping.getWard() == null
							&& pmsSpecMapping.getPhsSpecialty() == null && pmsSpecMapping.getPhsWard() == null ) {
						continue;
					}
					
					specMapRptPmsSpecMappingList.add(pmsSpecMapping);
				}
			}
		}
	}
	
    private static class SpecMapRptComparator implements Comparator<PmsSpecMapping>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(PmsSpecMapping pmsSpecMapping1, PmsSpecMapping pmsSpecMapping2) {
			int compareResult = 0;
			String specialtyType1 = StringUtils.trimToEmpty(pmsSpecMapping1.getSpecialtyType());
			String specialtyType2 = StringUtils.trimToEmpty(pmsSpecMapping2.getSpecialtyType());
			compareResult = specialtyType1.compareTo(specialtyType2);

			if (compareResult == 0) {
				String patHospCode1 = StringUtils.trimToEmpty(pmsSpecMapping1.getPatHospCode());
				String patHospCode2 = StringUtils.trimToEmpty(pmsSpecMapping2.getPatHospCode());
				compareResult = patHospCode1.compareTo(patHospCode2);
			}

			if (compareResult == 0) {
				String pasSpecialty1 = StringUtils.trimToEmpty(pmsSpecMapping1.getPasSpecialty());
				String pasSpecialty2 = StringUtils.trimToEmpty(pmsSpecMapping2.getPasSpecialty());
				compareResult = pasSpecialty1.compareTo(pasSpecialty2);
			}

			if (compareResult == 0) {
				String pasSubSpecialty1 = StringUtils.trimToEmpty(pmsSpecMapping1.getPasSubSpecialty());
				String pasSubSpecialty2 = StringUtils.trimToEmpty(pmsSpecMapping2.getPasSubSpecialty());
				compareResult = pasSubSpecialty1.compareTo(pasSubSpecialty2);
			}

			if (compareResult == 0) {
				String ward1 = StringUtils.trimToEmpty(pmsSpecMapping1.getWard());
				String ward2 = StringUtils.trimToEmpty(pmsSpecMapping2.getWard());
				compareResult = ward1.compareTo(ward2);
			}

			if (compareResult == 0) {
				String privateFlag1 = StringUtils.trimToEmpty(pmsSpecMapping1.getPrivateFlag());
				String privateFlag2 = StringUtils.trimToEmpty(pmsSpecMapping2.getPrivateFlag());
				compareResult = privateFlag1.compareTo(privateFlag2);
			}

			if (compareResult == 0) {
				String dispWorkstore1 = StringUtils.trimToEmpty(pmsSpecMapping1.getDispWorkstore());
				String dispWorkstore2 = StringUtils.trimToEmpty(pmsSpecMapping2.getDispWorkstore());
				compareResult = dispWorkstore1.compareTo(dispWorkstore2);
			}

			return compareResult;
		}
    }
	
	public List<PmsSpecMapping> retrievePmsSpecMappingList(PmsSpecMapReportCriteria pmsSpecMapReportCriteria){
		if ( patHospList == null ) {
			patHospList = createPatHospList();
		}
		return dmsRptServiceProxy.retrievePmsSpecMappingForReport(patHospList, pmsSpecMapReportCriteria);
	}	
	
	private List<String> createPatHospList() 
	{
		List<HospitalMapping> hospMappingList = workstore.getHospital().getHospitalMappingList();
		List<String> tmpPatHospList = new ArrayList<String>();
		
		if ( !hospMappingList.isEmpty() ) {
			for ( HospitalMapping hospitalMapping : hospMappingList ) {
				if ( hospitalMapping.getPrescribeFlag() ) {
					tmpPatHospList.add(hospitalMapping.getPatHospCode());
				}
			}	
		}
		
		return tmpPatHospList;
	}

	public void retrieveSpecMapRptList(){

		TreeSet<String> patHospCodeTreeSet = new TreeSet<String>();
		TreeSet<String> pasSpecialtyTreeSet = new TreeSet<String>();
		TreeSet<String> pasSubSpecialtyTreeSet = new TreeSet<String>();
		TreeSet<String> wardTreeSet = new TreeSet<String>();
		TreeSet<String> dispHospCodeTreeSet = new TreeSet<String>();
		TreeSet<String> phsSpecialtyTreeSet = new TreeSet<String>();
		TreeSet<String> phsWardTreeSet = new TreeSet<String>();
		TreeSet<String> dispWorkstoreTreeSet = new TreeSet<String>();
		
		for ( Iterator<PmsSpecMapping> pmsSpecMappingIterator = pmsSpecMappingList.iterator(); pmsSpecMappingIterator.hasNext(); ) {
			PmsSpecMapping pmsSpecMapping = pmsSpecMappingIterator.next();
			
			if (StringUtils.isNotBlank(pmsSpecMapping.getPatHospCode())){
				patHospCodeTreeSet.add(pmsSpecMapping.getPatHospCode());
			}
			if (StringUtils.isNotBlank(pmsSpecMapping.getPasSpecialty())){
				pasSpecialtyTreeSet.add(pmsSpecMapping.getPasSpecialty());
			}
			if (StringUtils.isNotBlank(pmsSpecMapping.getPasSubSpecialty())){
				pasSubSpecialtyTreeSet.add(pmsSpecMapping.getPasSubSpecialty());
			}
			if (StringUtils.isNotBlank(pmsSpecMapping.getWard())){
				wardTreeSet.add(pmsSpecMapping.getWard());
			}
			if (StringUtils.isNotBlank(pmsSpecMapping.getDispHospCode())){
				dispHospCodeTreeSet.add(pmsSpecMapping.getDispHospCode());
			}
			if (StringUtils.isNotBlank(pmsSpecMapping.getPhsSpecialty())){
				phsSpecialtyTreeSet.add(pmsSpecMapping.getPhsSpecialty());
			}
			if (StringUtils.isNotBlank(pmsSpecMapping.getPhsWard())){
				phsWardTreeSet.add(pmsSpecMapping.getPhsWard());
			}
			if (StringUtils.isNotBlank(pmsSpecMapping.getDispWorkstore())){
				dispWorkstoreTreeSet.add(pmsSpecMapping.getDispWorkstore());
			}
		}

		patHospCodeStrList = new ArrayList<String>(patHospCodeTreeSet);
		patHospCodeStrList.add(0, ReportFilterOption.All.getDataValue());
		
		pasSpecialtyStrList = new ArrayList<String>(pasSpecialtyTreeSet);
		pasSpecialtyStrList.add(0, ReportFilterOption.All.getDataValue());
		
		pasSubSpecialtyStrList = new ArrayList<String>(pasSubSpecialtyTreeSet);
		pasSubSpecialtyStrList.add(0, ReportFilterOption.All.getDataValue());
		pasSubSpecialtyStrList.add(1, ReportFilterOption.Blank.getDataValue());
		
		wardStrList = new ArrayList<String>(wardTreeSet);
		wardStrList.add(0, ReportFilterOption.All.getDataValue());
		wardStrList.add(1, ReportFilterOption.Blank.getDataValue());
		
		dispHospCodeStrList = new ArrayList<String>(dispHospCodeTreeSet);
		dispHospCodeStrList.add(0, ReportFilterOption.All.getDataValue());
		
		phsSpecialtyStrList = new ArrayList<String>(phsSpecialtyTreeSet);
		phsSpecialtyStrList.add(0, ReportFilterOption.All.getDataValue());
		
		phsWardStrList = new ArrayList<String>(phsWardTreeSet);
		phsWardStrList.add(0, ReportFilterOption.All.getDataValue());
		phsWardStrList.add(1, ReportFilterOption.Blank.getDataValue());
		
		dispWorkstoreStrList = new ArrayList<String>(dispWorkstoreTreeSet);
		dispWorkstoreStrList.add(0, ReportFilterOption.All.getDataValue());
		
	}
	
	public void clearSpecMapRpt() {
		destroy();
	}
	
	@Remove
	public void destroy() {

		if (specMapRptPmsSpecMappingList != null) {
			specMapRptPmsSpecMappingList = null;
		}
		if (pmsSpecMappingList != null) {
			pmsSpecMappingList = null;
		}
		if (patHospCodeStrList != null) {
			patHospCodeStrList = null;
		}
		if (pasSpecialtyStrList != null) {
			pasSpecialtyStrList = null;
		}
		if (pasSubSpecialtyStrList != null) {
			pasSubSpecialtyStrList = null;
		}
		if (wardStrList != null) {
			wardStrList = null;
		}
		if (dispHospCodeStrList != null) {
			dispHospCodeStrList = null;
		}
		if (phsSpecialtyStrList != null) {
			phsSpecialtyStrList = null;
		}
		if (phsWardStrList != null) {
			phsWardStrList = null;
		}
		if (dispWorkstoreStrList != null) {
			dispWorkstoreStrList = null;
		}
		
	}
}
