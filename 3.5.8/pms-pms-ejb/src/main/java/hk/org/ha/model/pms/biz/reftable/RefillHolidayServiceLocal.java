package hk.org.ha.model.pms.biz.reftable;

import javax.ejb.Local;

@Local
public interface RefillHolidayServiceLocal {
	
	void updateRefillHolidayAdvanceDueDate(Integer advanceDueDate);
	
	void retrieveRefillHolidayAdvanceDueDateProp();
	
	void destroy();
	
}
