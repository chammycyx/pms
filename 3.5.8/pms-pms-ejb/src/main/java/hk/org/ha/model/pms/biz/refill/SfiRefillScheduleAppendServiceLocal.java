package hk.org.ha.model.pms.biz.refill;

import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;

import javax.ejb.Local;

@Local
public interface SfiRefillScheduleAppendServiceLocal {
	
	Integer appendSfiRefillSchedule(OrderViewInfo inOrderViewInfo);
	
	String getErrMsg();
	
	void destroy();	
}
