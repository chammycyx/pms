package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class TicketNumRptDtl implements Serializable {

	private static final long serialVersionUID = 1L;

	private String itemNum;
	
	private String itemCode;
	
	private String vetUser;
	
	private String pickUser;
	
	private String assembleUser;
	
	private String checkUser;
	
	private String issueUser;
	
	private String suspendCode;	
	
	private String dispOrderItemStatus;

	public String getItemNum() {
		return itemNum;
	}

	public void setItemNum(String itemNum) {
		this.itemNum = itemNum;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getVetUser() {
		return vetUser;
	}

	public void setVetUser(String vetUser) {
		this.vetUser = vetUser;
	}

	public String getPickUser() {
		return pickUser;
	}

	public void setPickUser(String pickUser) {
		this.pickUser = pickUser;
	}

	public String getAssembleUser() {
		return assembleUser;
	}

	public void setAssembleUser(String assembleUser) {
		this.assembleUser = assembleUser;
	}

	public String getCheckUser() {
		return checkUser;
	}

	public void setCheckUser(String checkUser) {
		this.checkUser = checkUser;
	}

	public String getIssueUser() {
		return issueUser;
	}

	public void setIssueUser(String issueUser) {
		this.issueUser = issueUser;
	}

	public String getSuspendCode() {
		return suspendCode;
	}

	public void setSuspendCode(String suspendCode) {
		this.suspendCode = suspendCode;
	}

	public void setDispOrderItemStatus(String dispOrderItemStatus) {
		this.dispOrderItemStatus = dispOrderItemStatus;
	}

	public String getDispOrderItemStatus() {
		return dispOrderItemStatus;
	}



	
		
}
