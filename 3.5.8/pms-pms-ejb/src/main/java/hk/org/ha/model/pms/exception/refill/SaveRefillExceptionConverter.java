package hk.org.ha.model.pms.exception.refill;

import java.util.Map;

import org.granite.messaging.service.ExceptionConverter;
import org.granite.messaging.service.ServiceException;

public class SaveRefillExceptionConverter implements ExceptionConverter {

	public static final String SAVEREFILL_EXCEPTION = "SaveRefillException";
	
	@Override
	public boolean accepts(Throwable t, Throwable finalException) {
		return t.getClass().equals(SaveRefillException.class);
	}

	@Override
	public ServiceException convert(Throwable t, String detail, Map<String, Object> extendedData) {
		ServiceException se = new ServiceException(SAVEREFILL_EXCEPTION, t.getMessage(), detail, t);
		SaveRefillException saveRefillException = (SaveRefillException) t;
		extendedData.put("messageCode", saveRefillException.getMessageCode());
		extendedData.put("messageParam", saveRefillException.getMessageParam());
		se.getExtendedData().putAll(extendedData);
		return se;
	}
}
