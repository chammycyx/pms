package hk.org.ha.model.pms.biz.label;

import hk.org.ha.model.pms.vo.label.SpecialLabel;

import javax.ejb.Local;

@Local
public interface SpecialLabelListServiceLocal {  

	void retrieveSpecialLabelList(String labelNumPrefix);
	
	void printSpecialLabel(SpecialLabel specialLabel);

	void destroy(); 
}