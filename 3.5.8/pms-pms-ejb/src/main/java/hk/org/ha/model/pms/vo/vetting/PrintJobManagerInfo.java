package hk.org.ha.model.pms.vo.vetting;

import hk.org.ha.model.pms.persistence.disp.CapdVoucher;
import hk.org.ha.model.pms.persistence.disp.DispOrder;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PrintJobManagerInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private DispOrder dispOrder;
	
	private List<CapdVoucher> voucherList;

	public DispOrder getDispOrder() {
		return dispOrder;
	}

	public void setDispOrder(DispOrder dispOrder) {
		this.dispOrder = dispOrder;
	}

	public List<CapdVoucher> getVoucherList() {
		return voucherList;
	}

	public void setVoucherList(List<CapdVoucher> voucherList) {
		this.voucherList = voucherList;
	}
}
