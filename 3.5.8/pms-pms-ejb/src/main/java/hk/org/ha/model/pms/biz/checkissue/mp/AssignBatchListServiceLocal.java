package hk.org.ha.model.pms.biz.checkissue.mp;

import java.util.Date;

import javax.ejb.Local;

@Local
public interface AssignBatchListServiceLocal {
	
	void retrieveWardList();
	
	void retrieveUnlinkedDeliveryItemList(Date batchDate);
	
	void destroy();
	
}