package hk.org.ha.model.pms.biz.cddh;

import static hk.org.ha.model.pms.prop.Prop.CDDH_LEGACY_PERSISTENCE_ENABLED;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.util.StringArray;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.label.DispLabelBuilderLocal;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.udt.cddh.CddhRemarkStatus;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.cddh.CddhDispOrderItem;
import hk.org.ha.model.pms.vo.drug.DmDrugLite;
import hk.org.ha.model.pms.vo.label.DispLabel;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.report.CddhPatDrugProfileRpt;
import hk.org.ha.model.pms.vo.report.CddhPatDrugProfileRptDtl;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpCddhServiceJmsRemote;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.security.Identity;

/**
 * Session Bean implementation class CddhServiceBean
 */
@Stateful
@Scope(ScopeType.SESSION)
@Name("cddhAidService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CddhAidServiceBean implements CddhAidServiceLocal {
		
	@In
	private DispLabelBuilderLocal dispLabelBuilder;
	
	@Out(required = false)
	private DispLabel cddhDispLabel;
	
	@In
	private CorpCddhServiceJmsRemote corpCddhServiceProxy;
	
	@In
	private Identity identity;
	
	@In
	private Workstore workstore;
		
	@In
	private PrintAgentInf printAgent;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	AuditLogger auditLogger;
	
	@Out(required = false)
	private String errMsg;
	
	private List<CddhPatDrugProfileRpt> cddhPatDrugProfileRptList;
	
	private DateFormat dateFormatter = new SimpleDateFormat("ddMMyyyy");
	
	@SuppressWarnings("unchecked")
	public void retrieveCddhDispLabel(List<String> dispOrderItemKey) {
		DispOrderItem dispOrderItemFromHashMap = null;
		
		Map<StringArray, DispOrderItem> dispOrderItemHashMap = (Map<StringArray, DispOrderItem>) Contexts.getSessionContext().get("dispOrderItemHashMap");
		
		if (dispOrderItemKey.size() == 4) {
			dispOrderItemFromHashMap = dispOrderItemHashMap.get(new StringArray(dispOrderItemKey.get(0), 
																				dispOrderItemKey.get(1), 
																				dispOrderItemKey.get(2), 
																				dispOrderItemKey.get(3)));
		} else if (dispOrderItemKey.size() == 1) {
			dispOrderItemFromHashMap = dispOrderItemHashMap.get(new StringArray(dispOrderItemKey.get(0)));
		}

		cddhDispLabel = dispLabelBuilder.convertToDispLabel(dispOrderItemFromHashMap, null);
		if (cddhDispLabel != null) {
			PrintOption printOption = new PrintOption();
			printOption.setPrintLang(dispOrderItemFromHashMap.getDispOrder().getPrintLang());
			cddhDispLabel.setPrintOption(printOption);
		}
	}
	
	public void checkCddhRemarkStatus(Long dispOrderId, String displayHospCode){
		CddhRemarkStatus remarkStatus = corpCddhServiceProxy.checkDispOrderItemRemarkAccess(dispOrderId, workstore.getHospCode(), displayHospCode);
		errMsg = null;
		if (remarkStatus.equals(CddhRemarkStatus.AccessDenied)) {
			errMsg = "0082";
		} else if (remarkStatus.equals(CddhRemarkStatus.DayEndProcessing)) {
			errMsg = "0297";
		}
	}
	
	@SuppressWarnings("unchecked")
	public void updateDispOrderItemRemark(CddhDispOrderItem cddhDispOrderItem) {
		Boolean legacyPersistenceEnabled = CDDH_LEGACY_PERSISTENCE_ENABLED.get(false);
		
		Map<StringArray, DispOrderItem> dispOrderItemHashMap = (Map<StringArray, DispOrderItem>) Contexts.getSessionContext().get("dispOrderItemHashMap");

		DispOrderItem dispOrderItem;
		if (cddhDispOrderItem.getDoiLegacy()) {
			dispOrderItem = dispOrderItemHashMap.get(new StringArray( cddhDispOrderItem.getDoHospCode(), 
																		cddhDispOrderItem.getDoId().toString(), 
																		cddhDispOrderItem.getPoiItemNum().toString(), 
																		dateFormatter.format(cddhDispOrderItem.getDoIssueDate())) );
		} else {
			dispOrderItem = dispOrderItemHashMap.get(new StringArray(cddhDispOrderItem.getDoiId().toString()));
		}
		PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
		MedOrderItem medOrderItem = pharmOrderItem.getMedOrderItem();
		
		// update remark fields to original dispOrderItem
		dispOrderItem.setRemarkType(cddhDispOrderItem.getDoiRemarkType());
		dispOrderItem.setRemarkText(cddhDispOrderItem.getDoiRemarkText());				
		dispOrderItem.setRemarkUpdateDate(new Date());
		dispOrderItem.setRemarkUpdateUser(identity.getCredentials().getUsername());
		
		// set DmDrug to null because some object linked by it is not serializable, and 
		// corpCddhServiceProxy may use java serialization
		DmDrugLite tempPoiDmDrug = pharmOrderItem.getDmDrugLite();
		pharmOrderItem.setDmDrugLite(null);
		
		corpCddhServiceProxy.updateDispOrderItemRemark(dispOrderItem, legacyPersistenceEnabled);

		// resume DmDrug
		pharmOrderItem.setDmDrugLite(tempPoiDmDrug);
		
		DispOrder dispOrder = dispOrderItem.getDispOrder();
		PharmOrder pharmOrder = dispOrder.getPharmOrder();
		MedOrder medOrder = pharmOrder.getMedOrder();
		Patient patient = medOrder.getPatient();
		
		String caseNum = null;
		if (pharmOrder.getMedCase() != null) {
			caseNum = pharmOrder.getMedCase().getCaseNum();
		}
		
		auditLogger.log("#0244", patient.getHkid(), caseNum, dispOrder.getDispOrderNum(), dispOrderItem.getPharmOrderItem().getItemCode());
	}

	@SuppressWarnings("unchecked")
	public void deleteDispOrderItemRemark(CddhDispOrderItem cddhDispOrderItem) {
		Boolean legacyPersistenceEnabled = CDDH_LEGACY_PERSISTENCE_ENABLED.get(false);
		
		Map<StringArray, DispOrderItem> dispOrderItemHashMap = (Map<StringArray, DispOrderItem>) Contexts.getSessionContext().get("dispOrderItemHashMap");

		DispOrderItem dispOrderItem; 
		if (cddhDispOrderItem.getDoiLegacy()) {
			dispOrderItem = dispOrderItemHashMap.get(new StringArray( cddhDispOrderItem.getDoHospCode(), 
																		cddhDispOrderItem.getDoId().toString(), 
																		cddhDispOrderItem.getPoiItemNum().toString(), 
																		dateFormatter.format(cddhDispOrderItem.getDoIssueDate())) );
		} else {
			dispOrderItem = dispOrderItemHashMap.get(new StringArray(cddhDispOrderItem.getDoiId().toString()));
		}
		PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
		MedOrderItem medOrderItem = pharmOrderItem.getMedOrderItem();

		
		// set DmDrug to null because some object linked by it is not serializable, and 
		// corpCddhServiceProxy may use java serialization
		DmDrugLite tempPoiDmDrug = pharmOrderItem.getDmDrugLite();
		pharmOrderItem.setDmDrugLite(null);
		
		corpCddhServiceProxy.deleteDispOrderItemRemark(dispOrderItem, legacyPersistenceEnabled);
		
		// resume DmDrug
		pharmOrderItem.setDmDrugLite(tempPoiDmDrug);
		
		DispOrder dispOrder = dispOrderItem.getDispOrder();
		PharmOrder pharmOrder = dispOrder.getPharmOrder();
		MedOrder medOrder = pharmOrder.getMedOrder();
		Patient patient = medOrder.getPatient();
		
		String caseNum = null;
		if (pharmOrder.getMedCase() != null) {
			caseNum = pharmOrder.getMedCase().getCaseNum();
		}
		
		auditLogger.log("#0245", patient.getHkid(), caseNum, dispOrder.getDispOrderNum(), dispOrderItem.getPharmOrderItem().getItemCode());
	}
	
	@SuppressWarnings("unchecked")
	public void printCddhPatDrugProfileRpt(CddhPatDrugProfileRpt cddhPatDrugProfileRpt) {
		// handle legacy Cddh instruction
		PrintOption printOption = new PrintOption();
		List<CddhPatDrugProfileRptDtl> cddhPatDrugProfileRptDtlList = cddhPatDrugProfileRpt.getCddhPatDrugProfileRptDtlList();
		for (CddhPatDrugProfileRptDtl cddhPatDrugProfileRptDtl: cddhPatDrugProfileRptDtlList) {
			DispOrderItem dispOrderItemFromHashMap = null;
			if (cddhPatDrugProfileRptDtl.isLegacyMultDose()) {
				Map<StringArray, DispOrderItem> dispOrderItemHashMap = (Map<StringArray, DispOrderItem>) Contexts.getSessionContext().get("dispOrderItemHashMap");
				dispOrderItemFromHashMap = dispOrderItemHashMap.get(new StringArray(cddhPatDrugProfileRptDtl.getDispHospCode(), 
																					cddhPatDrugProfileRptDtl.getDispOrderId(),
																					cddhPatDrugProfileRptDtl.getItemNum(), 
																					cddhPatDrugProfileRptDtl.getIssueDate()));
				DispLabel dispLabel = dispLabelBuilder.convertToDispLabel(dispOrderItemFromHashMap, null);
				dispLabel.setPrintOption(printOption);
				cddhPatDrugProfileRptDtl.setInstructionText(dispLabel.getInstructionText());
			}
		}
		
		cddhPatDrugProfileRptList = new ArrayList<CddhPatDrugProfileRpt>();
		cddhPatDrugProfileRptList.add(cddhPatDrugProfileRpt);
		
	    printAgent.renderAndPrint(new RenderAndPrintJob(
			    "CddhPatDrugProfileRpt",
			    printOption,
			    printerSelector.retrievePrinterSelect(PrintDocType.Report),
			    cddhPatDrugProfileRptList));
	    
	    auditLogger.log("#0243", cddhPatDrugProfileRpt.getCddhPatDrugProfileRptHdr().getHkid(), cddhPatDrugProfileRpt.getCddhPatDrugProfileRptHdr().getName());
	}
	
	public String getErrMsg() {
		return errMsg;
	}

	@Remove
	public void destroy() {
		
	}
}
