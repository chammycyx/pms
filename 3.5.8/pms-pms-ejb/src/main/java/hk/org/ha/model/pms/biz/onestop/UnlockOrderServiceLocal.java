package hk.org.ha.model.pms.biz.onestop;

import javax.ejb.Local;

@Local
public interface UnlockOrderServiceLocal {
		
	void destroy();
	
	void validateMedOrder(Long id, Long medOrderVersion, Long refillId);
	
	void checkOrderIsLocked(Long id, Long medOrderVersion, Long refillId, Boolean requiredUnlock);
	
	void unlockOrderByRequest(Long id, Long medOrderVersion, Long refillId);
	
}
