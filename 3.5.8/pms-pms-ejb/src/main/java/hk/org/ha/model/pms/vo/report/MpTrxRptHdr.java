package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class MpTrxRptHdr implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String wardCode;
	
	private Date dueDate;
	
	private Integer numOfItem;
	
	private String batchCode;
	
	private String deliveryId;
	
	private String deliveryDate;
	
	private String pivasSupplyDate;
	
	private Boolean alertProfileFlag;
	
	private Boolean ehrAlertProfileFlag;
	
	private Boolean mdsCheckFlag;
	
	private Boolean ddiCheckFlag;
	
	private Boolean drugRefillListFlag;

	private Boolean patientDrugProfileListFlag;
	
	private Boolean pivasTrxRptFlag;	
	
	public MpTrxRptHdr(){
		drugRefillListFlag = Boolean.FALSE;
		patientDrugProfileListFlag = Boolean.FALSE;
		pivasTrxRptFlag = Boolean.FALSE;
	}
	
	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Integer getNumOfItem() {
		return numOfItem;
	}

	public void setNumOfItem(Integer numOfItem) {
		this.numOfItem = numOfItem;
	}

	public String getBatchCode() {
		return batchCode;
	}

	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}

	public String getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(String deliveryId) {
		this.deliveryId = deliveryId;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Boolean getMdsCheckFlag() {
		return mdsCheckFlag;
	}

	public void setMdsCheckFlag(Boolean mdsCheckFlag) {
		this.mdsCheckFlag = mdsCheckFlag;
	}

	public Boolean getAlertProfileFlag() {
		return alertProfileFlag;
	}

	public void setAlertProfileFlag(Boolean alertProfileFlag) {
		this.alertProfileFlag = alertProfileFlag;
	}

	public Boolean getEhrAlertProfileFlag() {
		return ehrAlertProfileFlag;
	}

	public void setEhrAlertProfileFlag(Boolean ehrAlertProfileFlag) {
		this.ehrAlertProfileFlag = ehrAlertProfileFlag;
	}

	public void setDrugRefillListFlag(Boolean drugRefillListFlag) {
		this.drugRefillListFlag = drugRefillListFlag;
	}

	public Boolean getDrugRefillListFlag() {
		return drugRefillListFlag;
	}

	public Boolean getPatientDrugProfileListFlag() {
		return patientDrugProfileListFlag;
	}

	public void setPatientDrugProfileListFlag(Boolean patientDrugProfileListFlag) {
		this.patientDrugProfileListFlag = patientDrugProfileListFlag;
	}

	public Boolean getPivasTrxRptFlag() {
		return pivasTrxRptFlag;
	}

	public void setPivasTrxRptFlag(Boolean pivasTrxRptFlag) {
		this.pivasTrxRptFlag = pivasTrxRptFlag;
	}

	public String getPivasSupplyDate() {
		return pivasSupplyDate;
	}

	public void setPivasSupplyDate(String pivasSupplyDate) {
		this.pivasSupplyDate = pivasSupplyDate;
	}

	public Boolean getDdiCheckFlag() {
		return ddiCheckFlag;
	}

	public void setDdiCheckFlag(Boolean ddiCheckFlag) {
		this.ddiCheckFlag = ddiCheckFlag;
	}
}
