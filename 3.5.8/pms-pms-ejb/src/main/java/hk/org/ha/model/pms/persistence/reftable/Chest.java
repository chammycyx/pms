package hk.org.ha.model.pms.persistence.reftable;

import flexjson.JSON;
import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacher;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Customizer;
import org.eclipse.persistence.annotations.PrivateOwned;
import org.jboss.seam.contexts.Contexts;

@Entity
@Table(name = "CHEST")
@Customizer(AuditCustomizer.class)
public class Chest extends VersionEntity {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "chestSeq")
	@SequenceGenerator(name = "chestSeq", sequenceName = "SQ_CHEST", initialValue = 100000000)
	private Long id;
	
	@Column(name = "UNIT_DOSE_NAME", nullable = false, length = 100)
	private String unitDoseName;
	
	@Column(name = "DEFAULT_ADV_START", nullable = false, length=1)
	private Integer defaultAdvStart;
	
	@Column(name = "DEFAULT_DURATION", nullable = false, length=2)
	private Integer defaultDuration;

	@PrivateOwned
	@OrderBy("sortSeq,itemCode")
    @OneToMany(mappedBy="chest", cascade=CascadeType.ALL)
    private List<ChestItem> chestItemList;

	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE", referencedColumnName = "HOSP_CODE", nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;
	
	public void loadChild(Workstore inWorkstore) {
		if ( inWorkstore == null ) {
			inWorkstore = (Workstore) Contexts.getSessionContext().get("workstore");
		}
		
		if (inWorkstore != null) {
			for ( ChestItem chestItem : chestItemList ) {
				WorkstoreDrug workstoreDrug = WorkstoreDrugCacher.instance().getWorkstoreDrug(inWorkstore, chestItem.getItemCode());
				if ( workstoreDrug != null ) {
					chestItem.setDmDrugLite(workstoreDrug.getDmDrugLite());
				}
			}
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUnitDoseName() {
		return unitDoseName;
	}

	public void setUnitDoseName(String unitDoseName) {
		this.unitDoseName = unitDoseName;
	}

	public Integer getDefaultAdvStart() {
		return defaultAdvStart;
	}

	public void setDefaultAdvStart(Integer defaultAdvStart) {
		this.defaultAdvStart = defaultAdvStart;
	}

	public Integer getDefaultDuration() {
		return defaultDuration;
	}

	public void setDefaultDuration(Integer defaultDuration) {
		this.defaultDuration = defaultDuration;
	}

	@JSON
	public List<ChestItem> getChestItemList() {
		if (chestItemList == null) {
			chestItemList = new ArrayList<ChestItem>();
		}
		return chestItemList;
	}

	public void setChestItemList(List<ChestItem> chestItemList) {
		this.chestItemList = chestItemList;
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}
	
	// helper methods
    public boolean isNew() {
    	return this.getId() == null;
    }
	
}
