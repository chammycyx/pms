package hk.org.ha.model.pms.biz.charging;

import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.medprofile.PurchaseRequest;
import hk.org.ha.model.pms.udt.charging.ClearanceStatus;
import hk.org.ha.model.pms.vo.charging.FcsDowntimePaymentInfo;
import hk.org.ha.model.pms.vo.charging.FcsPaymentStatus;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

@Local
public interface DrugChargeClearanceManagerLocal {

	ClearanceStatus checkStdDrugClearanceByMedOrder(MedOrder medOrder);

	ClearanceStatus checkStdDrugClearanceByDispOrder(DispOrder dispOrder);
	
	ClearanceStatus checkStdDrugClearance(String patHospCode, Long orderNum, Long pasApptSeq );
	
	FcsPaymentStatus checkSfiDrugClearanceByInvoice(Invoice invoice);
	
	FcsPaymentStatus checkSfiDrugClearanceByInvoice(Invoice invoice, boolean updateResult);
	
	Map<String,FcsPaymentStatus> checkSfiClearance(List<Invoice> invoiceList, List<PurchaseRequest> purchaseRequestList);
	
	FcsDowntimePaymentInfo retrieveDowntimePaymentStatus(String patHospCode, String invoiceNum, 
			String hkid, String caseNum,
			String searchType, String searchText);

	void updateDrugChargeClearance(List<PurchaseRequest> purchaseRequestList);

	List<PurchaseRequest> buildClearanceCheckList(List<PurchaseRequest> purchaseRequestList, boolean checkProfileCurrentProcessing);

	void updatePaymentStatus(Map<String, FcsPaymentStatus> paymentStatusMap, List<PurchaseRequest> purchaseRequestList, Boolean updateCorpInvoice);
}
