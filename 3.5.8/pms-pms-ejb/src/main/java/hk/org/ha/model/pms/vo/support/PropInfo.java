package hk.org.ha.model.pms.vo.support;

import hk.org.ha.model.pms.udt.DataType;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PropInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long propId;
	
	private String description;
	
	private String validation;
	
	private DataType type;
	
	private Integer sortSeq;
	
	private boolean supportMaintFlag;
	
	private List<PropInfoItem> propInfoItemList;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getValidation() {
		return validation;
	}

	public void setValidation(String validation) {
		this.validation = validation;
	}

	public DataType getType() {
		return type;
	}

	public void setType(DataType type) {
		this.type = type;
	}

	public List<PropInfoItem> getPropInfoItemList() {
		return propInfoItemList;
	}

	public void setPropInfoItemList(List<PropInfoItem> propInfoItemList) {
		this.propInfoItemList = propInfoItemList;
	}

	public void setSortSeq(Integer sortSeq) {
		this.sortSeq = sortSeq;
	}

	public Integer getSortSeq() {
		return sortSeq;
	}

	public void setPropId(Long propId) {
		this.propId = propId;
	}

	public Long getPropId() {
		return propId;
	}

	public void setSupportMaintFlag(boolean supportMaintFlag) {
		this.supportMaintFlag = supportMaintFlag;
	}

	public boolean isSupportMaintFlag() {
		return supportMaintFlag;
	}
	
}