package hk.org.ha.model.pms.biz.mar;
import hk.org.ha.model.pms.asa.exception.mar.MarException;

import javax.ejb.Local;

@Local
public interface MarServiceLocal {
	void retrieveMarRpt() throws MarException;
	void showMarRpt();
	void destroy();
}
