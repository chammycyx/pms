package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DispOrderAdminStatus implements StringValuedEnum {

	Normal("N", "Normal"),
	Suspended("S", "Suspended");

    private final String dataValue;
    private final String displayValue;

    DispOrderAdminStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static DispOrderAdminStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DispOrderAdminStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<DispOrderAdminStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<DispOrderAdminStatus> getEnumClass() {
    		return DispOrderAdminStatus.class;
    	}
    }
}
