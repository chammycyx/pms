package hk.org.ha.model.pms.udt.charging;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DowntimePaymentStatus implements StringValuedEnum {
	NumberIsValid("P","Number is valid"),
	NumberNotValid("F","Number not valid"),
	RecordNotFound("N","Record not found"),
	Error("E","Error");
	
    private final String dataValue;
    private final String displayValue;

    DowntimePaymentStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    } 
    
    public static DowntimePaymentStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DowntimePaymentStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<DowntimePaymentStatus> {
		
		private static final long serialVersionUID = 1L;

		@Override
    	public Class<DowntimePaymentStatus> getEnumClass() {
    		return DowntimePaymentStatus.class;
    	}
    }
}



