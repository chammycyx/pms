package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.PmsDispenseConfig;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
import hk.org.ha.model.pms.persistence.reftable.FdnMapping;
import hk.org.ha.model.pms.persistence.reftable.ItemDispConfig;
import hk.org.ha.model.pms.persistence.reftable.ItemSpecialty;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.MpFdnMappingRpt;
import hk.org.ha.model.pms.vo.report.MpItemPropertyRpt;
import hk.org.ha.model.pms.vo.report.MpItemSpecRpt;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsRptServiceJmsRemote;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("mpItemRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MpItemRptServiceBean implements MpItemRptServiceLocal {

	@PersistenceContext
	private EntityManager em;
	
	@In
	private Workstore workstore;
	
	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;
	
	@In
	private PrintAgentInf printAgent; 
	
	@In
	private DmsRptServiceJmsRemote dmsRptServiceProxy;
	
	private Map<String, Object> parameters = new HashMap<String, Object>();
	
	private List<MpItemSpecRpt> mpItemSpecRptList;
	
	private List<MpItemPropertyRpt> mpItemPropertyRptList;
	
	private List<MpFdnMappingRpt> mpFdnMappingRptList;
	
	@SuppressWarnings("unchecked")
	public void genMpItemSpecListRpt() {
		WorkstoreGroup workstoreGroup = workstore.getWorkstoreGroup();
		mpItemSpecRptList = new ArrayList<MpItemSpecRpt>();
		List<ItemSpecialty> itemSpecList = em.createQuery(
				"select o from ItemSpecialty o" + // 20120303 index check : ItemSpecialty.workstoreGroup : FK_ITEM_SPECIALTY_01
				" where o.workstoreGroup = :workstoreGroup")
				.setHint( QueryHints.FETCH, "o.workstoreGroup.workstoreList" )
				.setParameter("workstoreGroup", workstoreGroup)
				.getResultList();

		for(ItemSpecialty itemSpecialty : itemSpecList){
			WorkstoreDrug drug = workstoreDrugCacher.getWorkstoreDrug(workstore, itemSpecialty.getItemCode());
			DmDrug dmDrug = null;
			if( drug != null ){
				if (drug.getMsWorkstoreDrug() != null && drug.getDmDrug()!=null) {
					dmDrug = drug.getDmDrug();
				}else{
					continue;
				}
			}else{
				continue;
			}	

			MpItemSpecRpt mpItemSpecRpt = new MpItemSpecRpt();
			mpItemSpecRpt.setWorkstoreGroupCode(workstoreGroup.getWorkstoreGroupCode());
			mpItemSpecRpt.setWorkstoreCode(workstore.getWorkstoreCode());
			mpItemSpecRpt.setItemCode(itemSpecialty.getItemCode());
			if( dmDrug!=null ){
				mpItemSpecRpt.setFullDrugDesc(dmDrug.getFullDrugDesc());
			}
			mpItemSpecRpt.setSpecCode(itemSpecialty.getSpecCode());
			mpItemSpecRptList.add(mpItemSpecRpt);
		}
	}

	public void exportMpItemSpecListRpt() {	

		PrintOption po = new PrintOption();
		po.setDocType(ReportProvider.XLS);
		printAgent.renderAndRedirect(new RenderJob(
				"MpItemSpecXls", 
				po, 
				parameters, 
				mpItemSpecRptList));

	}

	@SuppressWarnings("unchecked")
	public void genMpItemPropertyListRpt() {
		WorkstoreGroup workstoreGroup = workstore.getWorkstoreGroup();
		mpItemPropertyRptList = new ArrayList<MpItemPropertyRpt>();
		
		List<PmsDispenseConfig> pmsDispenseConfigList = dmsRptServiceProxy.retrievePmsDispenseConfigList( workstoreGroup.getWorkstoreGroupCode() );
		
		Map<String, PmsDispenseConfig> pmsDispenseConfigMap = new HashMap<String, PmsDispenseConfig>();
		for( PmsDispenseConfig pmsDispenseConfig : pmsDispenseConfigList){
			pmsDispenseConfigMap.put(pmsDispenseConfig.getItemCode(), pmsDispenseConfig);
		}
		
		List<ItemDispConfig> itemDispConfigList = em.createQuery(
				"select o from ItemDispConfig o" + // 20120303 index check : ItemSpecialty.workstoreGroup : FK_ITEM_SPECIALTY_01
				" where o.workstoreGroup = :workstoreGroup")
				.setParameter("workstoreGroup", workstoreGroup)
				.getResultList();

		for(ItemDispConfig itemDispConfig : itemDispConfigList){

			WorkstoreDrug drug = workstoreDrugCacher.getWorkstoreDrug(workstore, itemDispConfig.getItemCode());
			DmDrug dmDrug = null;
			if( drug != null ){
				if (drug.getMsWorkstoreDrug() != null && drug.getDmDrug()!=null) {
					dmDrug = drug.getDmDrug();
				}else{
					continue;
				}
			}else{
				continue;
			}

			MpItemPropertyRpt mpItemPropertyRpt = new MpItemPropertyRpt();
			
			mpItemPropertyRpt.setWorkstoreGroupCode(workstoreGroup.getWorkstoreGroupCode());
			mpItemPropertyRpt.setItemCode(itemDispConfig.getItemCode());
			
			if( dmDrug!=null ){
				mpItemPropertyRpt.setFullDrugDesc(dmDrug.getFullDrugDesc());
			}else{
				mpItemPropertyRpt.setFullDrugDesc("");
			}
			
			mpItemPropertyRpt.setDailyRefillFlag(itemDispConfig.getDailyRefillFlag()?"Y":"N");
			mpItemPropertyRpt.setSingleFlag(itemDispConfig.getSingleDispFlag()?"Y":"N");
			
			if( pmsDispenseConfigMap.containsKey( itemDispConfig.getItemCode() ) ){
				mpItemPropertyRpt.setDefaultDispDay( pmsDispenseConfigMap.get(itemDispConfig.getItemCode()).getDefaultDispenseDay().toString() );
				pmsDispenseConfigMap.remove( itemDispConfig.getItemCode() );
			}else{
				mpItemPropertyRpt.setDefaultDispDay("");
			}
			
			if( StringUtils.isNotBlank( itemDispConfig.getWarningMessage() )){
				mpItemPropertyRpt.setWarningMessage(itemDispConfig.getWarningMessage());
			}else{
				mpItemPropertyRpt.setWarningMessage("");
			}
			
			mpItemPropertyRptList.add(mpItemPropertyRpt);
		}
		
		for(PmsDispenseConfig pmsDispenseConfig : pmsDispenseConfigMap.values()){
			WorkstoreDrug drug = workstoreDrugCacher.getWorkstoreDrug(workstore, pmsDispenseConfig.getItemCode());
			DmDrug dmDrug = null;
			if( drug != null ){
				if (drug.getMsWorkstoreDrug() != null && drug.getDmDrug()!=null) {
					dmDrug = drug.getDmDrug();
				}else{
					continue;
				}
			}else{
				continue;
			}
			
			MpItemPropertyRpt mpItemPropertyRpt = new MpItemPropertyRpt();
			mpItemPropertyRpt.setItemCode( pmsDispenseConfig.getItemCode() );
			mpItemPropertyRpt.setWorkstoreGroupCode( workstoreGroup.getWorkstoreGroupCode() );
			mpItemPropertyRpt.setDailyRefillFlag("N");
			mpItemPropertyRpt.setSingleFlag("N");
			mpItemPropertyRpt.setWarningMessage("");
			if( dmDrug!=null ){
				mpItemPropertyRpt.setFullDrugDesc(dmDrug.getFullDrugDesc());
			}else{
				mpItemPropertyRpt.setFullDrugDesc("");
			}
			mpItemPropertyRpt.setDefaultDispDay( pmsDispenseConfig.getDefaultDispenseDay().toString() );
			
			mpItemPropertyRptList.add(mpItemPropertyRpt);
		}

	}

	public void exportMpItemPropertyListRpt() {	

		PrintOption po = new PrintOption();
		po.setDocType(ReportProvider.XLS);
		printAgent.renderAndRedirect(new RenderJob(
				"MpItemPropertyXls", 
				po, 
				parameters, 
				mpItemPropertyRptList));

	}

	@SuppressWarnings("unchecked")
	public void genMpFdnMappingListRpt() {
		
		mpFdnMappingRptList = new ArrayList<MpFdnMappingRpt>();

		List<FdnMapping> fdnMappingList = em.createQuery(
				"select o from FdnMapping o" + // 20120303 index check : FdnMapping.workstore,orderType : UI_FDN_MAPPING_01 
				" where o.workstore.hospCode = :hospCode" + 
				" and o.orderType = :orderType")
		.setParameter("hospCode", workstore.getHospCode())
		.setParameter("orderType", OrderType.InPatient)
		.getResultList();

		for(FdnMapping fdnMapping : fdnMappingList){

			WorkstoreDrug drug = workstoreDrugCacher.getWorkstoreDrug(workstore, fdnMapping.getItemCode());
			DmDrug dmDrug = null;
			if( drug != null ){
				if (drug.getMsWorkstoreDrug() != null && drug.getDmDrug()!=null) {
					dmDrug = drug.getDmDrug();
				}else{
					continue;
				}
			}else{
				continue;
			}			

			MpFdnMappingRpt mpFdnMappingRpt = new MpFdnMappingRpt();
			mpFdnMappingRpt.setHospCode(fdnMapping.getWorkstore().getHospCode());
			mpFdnMappingRpt.setWorkstoreCode(fdnMapping.getWorkstore().getWorkstoreCode());
			mpFdnMappingRpt.setItemCode(fdnMapping.getItemCode());
			if( dmDrug!=null ){
				mpFdnMappingRpt.setFullDrugDesc(dmDrug.getFullDrugDesc());
			}
			mpFdnMappingRpt.setFloatDrugName(fdnMapping.getFloatDrugName());
			mpFdnMappingRptList.add(mpFdnMappingRpt);
		}
	}

	public void exportMpFdnMappingListRpt() {	

		PrintOption po = new PrintOption();
		po.setDocType(ReportProvider.XLS);
		printAgent.renderAndRedirect(new RenderJob(
				"MpFdnMappingXls", 
				po, 
				parameters, 
				mpFdnMappingRptList));

	}

	@Remove
	public void destroy() {
		if ( mpItemSpecRptList!=null ) {
			mpItemSpecRptList = null;
		}
		if ( mpItemPropertyRptList!=null ) {
			mpItemPropertyRptList = null;
		}
		if ( mpFdnMappingRptList!=null) {
			mpFdnMappingRptList = null;
		}
	}
}
