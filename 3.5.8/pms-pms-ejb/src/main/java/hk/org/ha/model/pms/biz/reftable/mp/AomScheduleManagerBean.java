package hk.org.ha.model.pms.biz.reftable.mp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.reftable.AomSchedule;
import hk.org.ha.model.pms.prop.Prop;
import hk.org.ha.model.pms.udt.reftable.mp.AomScheduleType;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.time.DateUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Stateless
@Name("aomScheduleManager")
@MeasureCalls
public class AomScheduleManagerBean implements AomScheduleManagerLocal {

	@PersistenceContext
	private EntityManager em;

	@In
	private Workstore workstore;
	
	private DateComparator dateComparator = new DateComparator();
	
	@SuppressWarnings("unchecked")
	public AomSchedule retrieveAomScheduleByDailyCode(String dailyFreqCode, AomScheduleType type) {
		
		List<AomSchedule> aomScheduleList = (List<AomSchedule>) em.createQuery(
				"select o from AomSchedule o" + // 20171023 index check : AomSchedule.hospital,dailyFreqCode,type : I_AOM_SCHEDULE_01
				" where o.hospital = :hospital" +
				" and o.dailyFreqCode = :dailyFreqCode" +
				" and o.type = :type")
				.setParameter("hospital", workstore.getHospital())
				.setParameter("dailyFreqCode", dailyFreqCode)
				.setParameter("type", type)
				.getResultList();
		
		if (aomScheduleList.size() > 0) {
			return aomScheduleList.get(0);
		} else {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<AomSchedule> retrieveAomScheduleListByMultiDailyCode(List<String> dailyFreqCodeList, AomScheduleType type) {
		if (dailyFreqCodeList.size() == 0) {
			return new ArrayList<AomSchedule>();
		} else {
			return (List<AomSchedule>)em.createQuery(
					"select o from AomSchedule o" + // 20171023 index check : AomSchedule.hospital,dailyFreqCode,type : I_AOM_SCHEDULE_01
					" where o.hospital = :hospital" +
					" and o.dailyFreqCode in :dailyFreqCodeList" +
					" and o.type = :type")
					.setParameter("hospital", workstore.getHospital())
					.setParameter("dailyFreqCodeList", dailyFreqCodeList)
					.setParameter("type", type)
					.getResultList();
		}
	}
	
	public Date getNextAdminDueDate(List<String> dailyFreqCodeList, AomScheduleType type) {
		
		Date dueDate = new Date();
		List<AomSchedule> aomScheduleList = retrieveAomScheduleListByMultiDailyCode(dailyFreqCodeList, type);
		if (aomScheduleList.size() == 0) {
			return DateUtils.truncate(new Date(), Calendar.HOUR);
		} else {
			MedProfile medProfileInMemory = (MedProfile)Contexts.getSessionContext().get("medProfile");
			List<Date> adminTimeList = new ArrayList<Date>();
			
			Calendar adminTimeCal;
			for (AomSchedule aomSchedule:aomScheduleList) {
				for (String adminTime:aomSchedule.getAdminTime()) {
					adminTimeCal = Calendar.getInstance();				
					adminTimeCal.setTime(new Date(dueDate.getTime()));
					adminTimeCal.set(Calendar.HOUR_OF_DAY, Integer.valueOf(adminTime.substring(0, 2)).intValue());
					adminTimeCal.set(Calendar.MINUTE, Integer.valueOf(adminTime.substring(2, 4)).intValue());	
					adminTimeCal.add(Calendar.MINUTE, 
							Prop.MEDPROFILE_ITEM_ADMINTIME_BUFFER.get(medProfileInMemory.getWorkstore(), 0));
					adminTimeList.add(adminTimeCal.getTime());
				}
			}
			
			Collections.sort(adminTimeList, dateComparator);
			
			for (Date adminTime:adminTimeList) {
				if (adminTime.after(dueDate)) {
					adminTimeCal = Calendar.getInstance();
					adminTimeCal.setTime(new Date(adminTime.getTime()));
					adminTimeCal.add(Calendar.MINUTE, 
							Prop.MEDPROFILE_ITEM_ADMINTIME_BUFFER.get(medProfileInMemory.getWorkstore(), 0)*-1);
					return adminTimeCal.getTime();
				}	
			}
						
			adminTimeCal = Calendar.getInstance();
			adminTimeCal.setTime(new Date(adminTimeList.get(0).getTime()));
			adminTimeCal.add(Calendar.MINUTE, 
					Prop.MEDPROFILE_ITEM_ADMINTIME_BUFFER.get(medProfileInMemory.getWorkstore(), 0)*-1);
			adminTimeCal.add(Calendar.DATE, 1);
			return adminTimeCal.getTime();
		}
	}
	
	private static class DateComparator implements Comparator<Date> {
		@Override
		public int compare(Date o1, Date o2) {
			return o1.compareTo(o2);
		}
	}
}
