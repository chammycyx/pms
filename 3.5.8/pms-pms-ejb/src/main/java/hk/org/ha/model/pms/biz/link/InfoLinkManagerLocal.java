package hk.org.ha.model.pms.biz.link;

import hk.org.ha.model.pms.persistence.InfoLink;

import java.util.List;

import javax.ejb.Local;

@Local
public interface InfoLinkManagerLocal {
	
	List<InfoLink> retrieveInfoLink();
	
}
