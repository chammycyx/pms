package hk.org.ha.model.pms.biz.checkissue.mp;

import static hk.org.ha.model.pms.prop.Prop.CDDH_LEGACY_PERSISTENCE_ENABLED;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.util.QueryUtils;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.checkissue.mp.CheckServiceBean.DeliveryProblemItemComparator;
import hk.org.ha.model.pms.biz.medprofile.MedProfileManagerLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileStatManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.medprofile.Delivery;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.persistence.pivas.MaterialRequest;
import hk.org.ha.model.pms.persistence.pivas.MaterialRequestItem;
import hk.org.ha.model.pms.persistence.pivas.PivasPrep;
import hk.org.ha.model.pms.persistence.pivas.PivasWorklist;
import hk.org.ha.model.pms.udt.RecordStatus;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryItemStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryRequestStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryStatus;
import hk.org.ha.model.pms.udt.pivas.MaterialRequestItemStatus;
import hk.org.ha.model.pms.udt.pivas.PivasWorklistStatus;
import hk.org.ha.model.pms.vo.checkissue.mp.CheckDelivery;
import hk.org.ha.model.pms.vo.checkissue.mp.DeliveryProblemItem;
import hk.org.ha.model.pms.vo.label.LabelContainer;
import hk.org.ha.model.pms.vo.label.MpDispLabel;
import hk.org.ha.model.pms.vo.medprofile.PasContext;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;
import org.springframework.beans.BeanUtils;

@Stateful
@Scope(ScopeType.SESSION)
@Name("unlinkService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class UnlinkServiceBean implements UnlinkServiceLocal {
	
	private static final String JAXB_CONTEXT_LABEL = "hk.org.ha.model.pms.vo.label";
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private Workstore workstore;
	
	@In
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
	
	@In
	private MedProfileManagerLocal medProfileManager;
	
	@In
	private MedProfileStatManagerLocal medProfileStatManager;

	@In
	private CheckManagerLocal checkManager;
	
	@In
	private SendServiceLocal sendService;
	
	@In
	private AuditLogger auditLogger; 
	
	@Out(required = false)
	private String resultMsg;
	
	private String batchNum;
		
	private DateFormat df = new SimpleDateFormat("yyyyMMdd"); 
	private Map<Long,DeliveryItem> deliveryItemAllMap;
	
	private static final int QUERY_CHUNK_SIZE = 500; 
	
	private boolean autoUpdate = true;
	
	private DateFormat dfForBatchPrefix = new SimpleDateFormat("dd");
	
	private static final List<DeliveryItemStatus> NON_ACTIVE_DELIVERY_ITEM_STATUS_LIST = Arrays.asList(DeliveryItemStatus.KeepRecord, DeliveryItemStatus.Unlink, DeliveryItemStatus.Deleted);
	private static final List<DeliveryItemStatus> NON_ACTIVE_DELIVERY_ITEM_STATUS_LIST_FOR_ITD = Arrays.asList(DeliveryItemStatus.Unlink,DeliveryItemStatus.Deleted);
	
	public CheckDelivery retrieveDeliveryItemByForUnlinkDrug(
			Date batchDate,
		String batchNumIn,
		String caseNum,
		String trxIdString
		) {
		
		String batchNumLocal = null;
		resultMsg = StringUtils.EMPTY;
		Long trxId = null;
		CheckDelivery checkDelivery = new CheckDelivery();
		if(trxIdString != null ){
			try {
				trxId = Long.parseLong(trxIdString.trim());
			} catch (java.lang.NumberFormatException e) {
				trxId = null;
			}
		}else{
			trxId = null;
		}
		
		//FRS Chapter 05 Workflow 145 , prevent Delivery Label barcode value too long 
		if(batchNumIn != null && batchNumIn.length() > 4){
			batchNumLocal = batchNumIn.substring(0, 4).trim();
		}else if(batchNumIn != null){
			batchNumLocal =  StringUtils.isEmpty(batchNumIn.trim()) ? null: batchNumIn.trim();
		}
		
		if(batchDate == null && batchNumLocal == null && caseNum == null ) {
			if(trxId == null || StringUtils.isEmpty(trxId.toString()) ){
				resultMsg = "0005"; //Record not founnd.
				return checkDelivery;
			}
		}
		deliveryItemAllMap = retrieveDeliveryItemList(batchDate, batchNumLocal, caseNum, trxId);
		
		Map<Long, MedProfile> medProfileMap = new TreeMap<Long, MedProfile>();
		
		if ( !deliveryItemAllMap.isEmpty() ) {
			
			for ( Long key : deliveryItemAllMap.keySet() ) {
				
				DeliveryItem deliveryItem = deliveryItemAllMap.get(key);
				deliveryItem.getDelivery();
				
				deliveryItem.getDelivery().getDeliveryRequest();
				
				if ( deliveryItem.getDelivery().getDeliveryRequest() != null ) {
					deliveryItem.getDelivery().getDeliveryRequest().getDeliveryRequestAlertList();
					deliveryItem.getDelivery().getDeliveryRequest().getWorkstore();
				}
				MedProfile medProfile = deliveryItem.getMedProfile();
				
				medProfile.getMedCase();
				medProfile.getPatient().getName();
				medProfileMap.put(medProfile.getId(), medProfile);
			}
			
		}else {
			resultMsg = "0005"; //Record not founnd.
			return checkDelivery;
		}
		
		for ( MedProfile medProfile : medProfileMap.values() ) {
			updatePasInfo(medProfile);
		}
		
		if(trxId != null){
			for ( Long key : deliveryItemAllMap.keySet() ) {
				DeliveryItem deliveryItem = deliveryItemAllMap.get(key);
				checkDelivery.setBatchDate(deliveryItem.getDelivery().getBatchDate());
			}
		}else{
			checkDelivery.setBatchDate(batchDate);
		}
		
		checkDelivery.setBatchNum(batchNumIn);
		checkDelivery.setDeliveryProblemItemList(new ArrayList<DeliveryProblemItem>());
		List<Long> mpSfiInvoiceDeliveryItemIdList = new ArrayList<Long>();
		
		if ( !deliveryItemAllMap.isEmpty() ) {
			for ( Long key : deliveryItemAllMap.keySet() ) {
				
				DeliveryItem deliveryItem = deliveryItemAllMap.get(key);
				
				String exception = checkManager.getProblemItemException(deliveryItem);
				
				DeliveryProblemItem deliveryProblemItem = new DeliveryProblemItem();
				convertDeliveryProblemItem(deliveryProblemItem, deliveryItem, exception);
				
				if ( haveMpSfiInvoice(deliveryItem) ) {
					mpSfiInvoiceDeliveryItemIdList.add(deliveryItem.getId());
				}
				
				checkDelivery.getDeliveryProblemItemList().add(deliveryProblemItem);
			}
			checkDelivery.setDeliveryProblemItemList(markDayEndDeliveryProblemItem(checkDelivery.getDeliveryProblemItemList()));
		}
		
		if ( !mpSfiInvoiceDeliveryItemIdList.isEmpty() ) {//retrieve invoiceNum for mpSfiInvoice warning message
			setDeliveryProfileItemInvoiceNum(checkDelivery.getDeliveryProblemItemList(), mpSfiInvoiceDeliveryItemIdList);
		}
		
		if ( !checkDelivery.getDeliveryProblemItemList().isEmpty() ) {
			Collections.sort(checkDelivery.getDeliveryProblemItemList(), new DeliveryProblemItemComparator());
		}  else {
			resultMsg = "0005"; //Record not founnd.
		}
		
		return checkDelivery;
	}
	
	private void setDeliveryProfileItemInvoiceNum(List<DeliveryProblemItem> deliveryProblemItemList, List<Long> mpSfiInvoiceDeliveryItemIdList){
		Map<Long, String> invoiceNumMap = corpPmsServiceProxy.retrieveInvoiceNumByDeliveryItemId(mpSfiInvoiceDeliveryItemIdList);
		for ( DeliveryProblemItem deliveryProblemItem : deliveryProblemItemList ) {
			if ( invoiceNumMap.containsKey(deliveryProblemItem.getDeliveryItemId()) ) {
				deliveryProblemItem.setMpSfiInvoiceNum(invoiceNumMap.get(deliveryProblemItem.getDeliveryItemId()));
			}
		}
	}
	
	private void convertDeliveryProblemItem(DeliveryProblemItem deliveryProblemItem, DeliveryItem deliveryItem, String exception) {
		
		JaxbWrapper<Object> labelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
		
		MedProfilePoItem medProfilePoItem = deliveryItem.resolveMedProfilePoItem();
		MedProfile medProfile = deliveryItem.getMedProfile();
		
		deliveryProblemItem.setDeliveryId(deliveryItem.getDelivery().getId());
		deliveryProblemItem.setDeliveryItemId(deliveryItem.getId());
		deliveryProblemItem.setCaseNum(medProfile.getMedCase().getCaseNum());
		deliveryProblemItem.setException(StringUtils.isEmpty(exception) ? "" :exception);
		
		if (deliveryItem.getPivasWorklist() != null) {
			deliveryProblemItem.setBaseUnit("DOSE");
		} else if (medProfilePoItem != null) {
			deliveryProblemItem.setDrugName(medProfilePoItem.getDrugName());
			deliveryProblemItem.setFormLabelDesc(medProfilePoItem.getFormLabelDesc());
			deliveryProblemItem.setStrength(medProfilePoItem.getStrength());
			deliveryProblemItem.setVolumeText(medProfilePoItem.getVolumeText());
			
			deliveryProblemItem.setBaseUnit(medProfilePoItem.getBaseUnit());
			deliveryProblemItem.setDoseUnit(medProfilePoItem.getDoseUnit());
		}

		deliveryProblemItem.setItemCode(deliveryItem.getItemCode());
		deliveryProblemItem.setIssueQty(deliveryItem.getIssueQty());
		deliveryProblemItem.setDeliveryItemStatus(deliveryItem.getStatus());
		
		if(!(deliveryItem.getStatus() == DeliveryItemStatus.Unlink || deliveryItem.getStatus() == DeliveryItemStatus.Deleted)){
			deliveryProblemItem.setBatchCode(dfForBatchPrefix.format(deliveryItem.getDelivery().getBatchDate())+deliveryItem.getDelivery().getBatchNum());
		}else{
			deliveryProblemItem.setBatchCode("");
		}
		
		Object label = labelJaxbWrapper.unmarshall(deliveryItem.getLabelXml());
		
		if (label instanceof MpDispLabel) {
			deliveryProblemItem.setWardCode(((MpDispLabel) label).getWard());
			deliveryProblemItem.setBedNum(((MpDispLabel) label).getBedNum());
		} else if (label instanceof LabelContainer) {
			deliveryProblemItem.setItemDescTlf(((LabelContainer) label).getPivasProductLabel().getDeliveryItemDescTlf());
			deliveryProblemItem.setWardCode(((LabelContainer) label).getPivasProductLabel().getWard());
			deliveryProblemItem.setBedNum(((LabelContainer) label).getPivasProductLabel().getBedNum());
		}
		
		Patient patient = medProfile.getPatient();
		
		if(patient.getNameChi() == null || patient.getNameChi().isEmpty()){
			deliveryProblemItem.setPatientName(patient.getName());
			deliveryProblemItem.setHavePatientChiName(false);
		}else{
			deliveryProblemItem.setPatientName(patient.getNameChi());
			deliveryProblemItem.setHavePatientChiName(true);
		}
		
		if(deliveryItem.getStatus() == DeliveryItemStatus.KeepRecord){
			deliveryProblemItem.setKeepRecorded(true);
		}
		if(deliveryItem.getStatus() == DeliveryItemStatus.Unlink){
			deliveryProblemItem.setMarkUnLink(true);
			deliveryProblemItem.setException(StringUtils.isEmpty(exception) ? "Delinked" :exception);
		}
		if(deliveryItem.getStatus() == DeliveryItemStatus.Deleted){
			deliveryProblemItem.setMarkDelete(true);
			deliveryProblemItem.setMarkUnLink(true);
			deliveryProblemItem.setException(StringUtils.isEmpty(exception) ? "Deleted" :exception);
		}
		
		if(deliveryItem.getStatus() == DeliveryItemStatus.Checked ){
			deliveryProblemItem.setKeepRecorded(true);
			deliveryProblemItem.setException(StringUtils.isEmpty(exception) ? "Checked" :exception);
		}else if(deliveryItem.getStatus() == DeliveryItemStatus.Delivered){
			deliveryProblemItem.setKeepRecorded(true);
			deliveryProblemItem.setException(StringUtils.isEmpty(exception) ? "Delivered" :exception);
		}
	}
	
	@SuppressWarnings("unchecked")
	private HashMap<Long,DeliveryItem> retrieveDeliveryItemList(Date batchDate, String batchNum ,String caseNum, Long txId) {
		Query query = null;
		HashMap<Long,DeliveryItem> retrieveDeliveryItemMap = new HashMap<Long,DeliveryItem>();
		
		StringBuilder sb = new StringBuilder(
				"select o from DeliveryItem o" + // 20140128 index check : Delivery.hospital,batchDate,batchNum DeliveryItem.id MedCase.caseNum : I_DELIVERY_01 PK_DELIVERY_ITEM I_MED_CASE_01
				" where o.delivery.hospital = :hospital" +
				" and o.delivery.deliveryRequest.status = :deliveryRequestStatus");
		
		if (batchDate != null) {
			sb.append(" and o.delivery.batchDate = :batchDate");
		}
		if (!StringUtils.isEmpty(batchNum)) {
			sb.append(" and o.delivery.batchNum = :batchNum");
			sb.append(" and o.status not in :status");
		}
		if (!StringUtils.isEmpty(caseNum)) {
			sb.append(" and o.medProfile.medCase.caseNum = :caseNum");
		}
		if (txId != null) {
			sb.append(" and o.id = :txId");
		}		
		sb.append(" and o.status != :statusDelete");
		
		sb.append(" order by o.medProfile.medCase.caseNum,");
		sb.append(" o.delivery.wardCode,");
		sb.append(" o.medProfile.medCase.pasBedNum,");
		sb.append(" o.delivery.batchNum,");
		sb.append(" o.itemCode");
		
		query = em.createQuery(sb.toString())
		.setParameter("hospital", workstore.getHospital())
		.setParameter("deliveryRequestStatus", DeliveryRequestStatus.Completed);
		
		if (batchDate != null) {
			query.setParameter("batchDate", batchDate, TemporalType.DATE);
		}
		if (!StringUtils.isEmpty(batchNum)) {
			query.setParameter("batchNum", batchNum);
			query.setParameter("status", NON_ACTIVE_DELIVERY_ITEM_STATUS_LIST_FOR_ITD);
		}
		if (!StringUtils.isEmpty(caseNum)) {
			query.setParameter("caseNum", caseNum);
		}
		if (txId != null) {
			query.setParameter("txId", txId);
		}		
		query.setParameter("statusDelete", DeliveryItemStatus.Deleted);
				
		List<DeliveryItem> resultList = query.getResultList();
		
		for (DeliveryItem di :resultList){
			retrieveDeliveryItemMap.put(di.getId(), di);
		}
		return retrieveDeliveryItemMap;
	}
	
	private List<DeliveryProblemItem> markDayEndDeliveryProblemItem(List<DeliveryProblemItem> inDeliveryProblmmItemList){		
		if ( inDeliveryProblmmItemList.isEmpty() ) {
			return null;
		}
		
		List<Long> deliveryItemIdList = new ArrayList<Long>();
		boolean lastSplitEntry = false;
		int counterSplit = 0;
		
		List<DispOrderItem> dispOrderItemList = new ArrayList<DispOrderItem>();
			
		for ( DeliveryProblemItem deliveryProblemItem : inDeliveryProblmmItemList) {
			if( counterSplit < QUERY_CHUNK_SIZE){
				deliveryItemIdList.add(deliveryProblemItem.getDeliveryItemId());
				counterSplit++;
				lastSplitEntry = true;
			}else{
				deliveryItemIdList.add(deliveryProblemItem.getDeliveryItemId());
				List<DispOrderItem> dispOrderItemListSplited = corpPmsServiceProxy.retrieveDispOrderItemListByDeliveryItemId(deliveryItemIdList);
				dispOrderItemList.addAll(dispOrderItemListSplited);
				deliveryItemIdList.clear();
				counterSplit = 0;
				lastSplitEntry = false;
			}
			
		}
		
		if(lastSplitEntry){
			List<DispOrderItem> dispOrderItemListSplited = corpPmsServiceProxy.retrieveDispOrderItemListByDeliveryItemId(deliveryItemIdList);
			dispOrderItemList.addAll(dispOrderItemListSplited);
		}
		
		
		if ( !dispOrderItemList.isEmpty() ) {
			
			for (Iterator<DeliveryProblemItem> itr = inDeliveryProblmmItemList.iterator();itr.hasNext();) {
				DeliveryProblemItem deliveryProblemItem = itr.next();
				
				boolean dayEndFlag = true;
				for (DispOrderItem dispOrderItem : dispOrderItemList) {
					
					if ( deliveryProblemItem.getDeliveryItemId().equals(dispOrderItem.getDeliveryItemId()) ) {
						deliveryProblemItem.setDayendProcess(false);
						dayEndFlag = false;
					}
				}
				if ( dayEndFlag ) {
					deliveryProblemItem.setDayendProcess(true);
				}
				
				
			}
		} else {
			for (Iterator<DeliveryProblemItem> itr = inDeliveryProblmmItemList.iterator();itr.hasNext();) {
				DeliveryProblemItem deliveryProblemItem = itr.next();
				deliveryProblemItem.setDayendProcess(true);
				
			}
		}
		
		
		
		return inDeliveryProblmmItemList;
	}
	
	@SuppressWarnings("unchecked")
	private void updatePasInfo(MedProfile medProfile){
		PasContext pasContext = medProfileManager.retrieveContext(medProfile);
		
		List<MedProfile> medProfileList = em.createQuery(
				"select o from MedProfile o" + // 20120214 index check : MedProfile.id : PK_MED_PROFILE
				" where o.id = :medProfileId" +
				" and o.id is not null") // don't delete : magic code fix bug for https://bugs.eclipse.org/bugs/show_bug.cgi?id=347592
				.setParameter("medProfileId", medProfile.getId())
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
				.getResultList();
		
		medProfileManager.applyChanges(pasContext, medProfileList.get(0));
		medProfileStatManager.recalculateMedProfilePoItemStat(medProfileList.get(0));
	}
	
	@SuppressWarnings("unchecked")
	public CheckDelivery updateDeliveryItemStatuToUnlink(CheckDelivery inCheckOrder) {
		
		Set<Long> deliveryIdSet = new HashSet<Long>();
		boolean resultMsgFlag = false;
		resultMsg = StringUtils.EMPTY;
		Map<Long, DeliveryProblemItem> deliveryProblemItemMarkUnlinkMap = new HashMap<Long, DeliveryProblemItem>();
		Map<Long, DeliveryProblemItem> deliveryProblemItemMarkDeleteMap = new HashMap<Long, DeliveryProblemItem>();
		
		HashMap<String,String> aduitDeliveryMap = new HashMap<String,String> ();
		HashMap<Long,String> aduitDeliveryItemMap = new HashMap<Long,String> ();
		
		for ( DeliveryProblemItem deliveryProblemItem : inCheckOrder.getDeliveryProblemItemList() ) {
			if ( deliveryProblemItem.isMarkUnLink() ) {
				deliveryProblemItemMarkUnlinkMap.put(deliveryProblemItem.getDeliveryItemId(), deliveryProblemItem);
				deliveryProblemItem.setMarkUnLink(true);
				if(!deliveryIdSet.contains(deliveryProblemItem.getDeliveryId())){
					deliveryIdSet.add(deliveryProblemItem.getDeliveryId());
				}
			} 
			if(deliveryProblemItem.isMarkDelete()){
				deliveryProblemItem.setMarkUnLink(true);
				deliveryProblemItem.setMarkDelete(true);
				deliveryProblemItemMarkDeleteMap.put(deliveryProblemItem.getDeliveryItemId(), deliveryProblemItem);
				if(!deliveryIdSet.contains(deliveryProblemItem.getDeliveryId())){
					deliveryIdSet.add(deliveryProblemItem.getDeliveryId());
				}
			} 
		}
		
		boolean deleteItemExist = false;
		List<Long> deleteDeliveryIdList = new ArrayList<Long>();
		Map<Long, DeliveryItem> deleteDeliveryItemMap = new HashMap<Long, DeliveryItem>();
		
		
		if(!deliveryIdSet.isEmpty()){
			List<Delivery> deliveryList = em.createQuery(
					"select o from Delivery o" + // 20120214 index check : MedProfile.id : PK_MED_PROFILE
					" where o.id in :deliveryIdList") 
					.setParameter("deliveryIdList", deliveryIdSet)
					.getResultList();
			
			if (!lockMedProfilesForPivasItemsDeletion(deliveryList, deliveryProblemItemMarkDeleteMap)) {
				resultMsg = "0909";
				return inCheckOrder;
			}
			
			for(Delivery delivery : deliveryList){
				
				String previousDeliveryStatus = delivery.getStatus().getDataValue();
				boolean keepRecordStatus = false;
				boolean unlinkDeleteStatus = false;
				boolean activeStatus = false;
				
				for ( DeliveryItem deliveryItem : delivery.getDeliveryItemList() ) {
					
					String previousDeliveryItemStatus = deliveryItem.getStatus().getDataValue();
					String newDeliveryItemStatus = deliveryItem.getStatus().getDataValue();
					
					if(deliveryItem.getStatus() == DeliveryItemStatus.KeepRecord){
						newDeliveryItemStatus = DeliveryItemStatus.KeepRecord.getDataValue(); //for auditLog
					}
					
					if ( deliveryProblemItemMarkUnlinkMap.containsKey(deliveryItem.getId()) && 
							!deliveryProblemItemMarkDeleteMap.containsKey(deliveryItem.getId()) && 
							deliveryItem.getStatus() != DeliveryItemStatus.Checked && 
							deliveryItem.getStatus() != DeliveryItemStatus.Delivered ) { // marked unlink

						deliveryItem.setStatus(DeliveryItemStatus.Unlink);
						newDeliveryItemStatus = DeliveryItemStatus.Unlink.getDataValue();
						
					} else if ( deliveryProblemItemMarkDeleteMap.containsKey(deliveryItem.getId()) ) { //marked delete
						
						deleteDeliveryItemMap.put(deliveryItem.getId(), deliveryItem);
						deleteDeliveryIdList.add(deliveryItem.getId());
						deleteItemExist = true;
						newDeliveryItemStatus = DeliveryItemStatus.Deleted.getDataValue();
					}
					
					aduitDeliveryItemMap.put(deliveryItem.getId(), previousDeliveryItemStatus+"=>"+newDeliveryItemStatus);
					
					if ( DeliveryItemStatus.dataValueOf(newDeliveryItemStatus) == DeliveryItemStatus.KeepRecord ) {
						keepRecordStatus = true;
					} else if ( DeliveryItemStatus.dataValueOf(newDeliveryItemStatus) == DeliveryItemStatus.Deleted || 
									DeliveryItemStatus.dataValueOf(newDeliveryItemStatus) == DeliveryItemStatus.Unlink ) {
						unlinkDeleteStatus = true;
					} else {
						activeStatus = true;
					}
				}				
				
				batchNum = delivery.getBatchNum();
				
				if ( !activeStatus ) {
					if ( keepRecordStatus ) {
						//delivery only have keep record item
						delivery.setStatus(DeliveryStatus.KeepRecord);
						delivery.setModifyDate(new Date());
						delivery.setUpdateDate(new Date());
						resultMsgFlag= true;
					} else if ( unlinkDeleteStatus ){
						delivery.setStatus(DeliveryStatus.Deleted);
						delivery.setModifyDate(new Date());
						delivery.setUpdateDate(new Date());
						if(autoUpdate){
							sendService.dispatchUpdateEvent(delivery.getDeliveryRequest().getWorkstore());
						}
						resultMsgFlag= true;
					}
				} else if ( unlinkDeleteStatus ) {
					//for transaction report to check update
					delivery.setModifyDate(new Date());
					resultMsgFlag= true;
				}
				aduitDeliveryMap.put(df.format(delivery.getBatchDate())+delivery.getBatchNum(), previousDeliveryStatus+"=>"+delivery.getStatus().getDataValue());
			}
			
			if(!deliveryList.isEmpty()){
				auditLogger.log("#0680", aduitDeliveryMap, aduitDeliveryItemMap, deleteItemExist);
			}
		
		}
		
		if(resultMsgFlag){
			resultMsg = "0665";
		}else{
			resultMsg = "0664";
		}
		
		if(deleteItemExist){
			deleteDeliveryItemToCorp(deleteDeliveryIdList, deleteDeliveryItemMap);
		}
		
		return inCheckOrder;
	}
	
	@SuppressWarnings("unchecked")
	private boolean lockMedProfilesForPivasItemsDeletion(List<Delivery> deliveryList, Map<Long, DeliveryProblemItem> deliveryProblemItemMarkDeleteMap) {

		Set<Long> medProfileIdSet = new TreeSet<Long>();
		
		for (Delivery delivery : deliveryList) {
			for (DeliveryItem deliveryItem: delivery.getDeliveryItemList()) {
				if (deliveryProblemItemMarkDeleteMap.containsKey(deliveryItem.getId()) && deliveryItem.getPivasWorklist() != null) {
					medProfileIdSet.add(deliveryItem.getPivasWorklist().getMedProfileId());
				}
			}
		}
		
		List<Long> lockedMedProfileIdList = QueryUtils.splitExecute(em.createQuery(
				"select o.id from MedProfile o" + // 20160615 index check : MedProfile.id : PK_MED_PROFILE
				" where o.processingFlag = false" +
				" and o.deliveryGenFlag = false" +
				" and o.id in :medProfileIdSet")
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock), "medProfileIdSet", medProfileIdSet);
		
		return ObjectUtils.compare(lockedMedProfileIdList.size(), medProfileIdSet.size()) == 0;
	}
	
	@SuppressWarnings("unchecked")
	private boolean deletePivasItemsAndRevertPoItemDueDates(Collection<DeliveryItem> deleteDeliveryItemList) {

		Map<Long, PivasWorklist> revertDueDateMap = new TreeMap<Long, PivasWorklist>();
		
		for (DeliveryItem deliveryItem: deleteDeliveryItemList) {
			PivasWorklist pivasWorklist = deliveryItem.getPivasWorklist();
			if (pivasWorklist != null) {
				deletePivasItem(revertDueDateMap, pivasWorklist);
			}
		}
		
		List<MedProfilePoItem> medProfilePoItemList = QueryUtils.splitExecute(em.createQuery(
				"select o from MedProfilePoItem o" + // 20160615 index check : MedProfilePoItem.medProfileMoItemId : FK_MED_PROFILE_PO_ITEM_01
				" where o.medProfileMoItemId in :medProfileMoItemIdSet"), "medProfileMoItemIdSet", revertDueDateMap.keySet());
		
		for (MedProfilePoItem medProfilePoItem: medProfilePoItemList) {
			PivasWorklist pivasWorklist = revertDueDateMap.get(medProfilePoItem.getMedProfileMoItemId());
			if (ObjectUtils.compare(pivasWorklist.getDueDate(), medProfilePoItem.getDueDate(), true) < 0) {
				medProfilePoItem.setDueDate(pivasWorklist.getDueDate());
			}
		}
		return true;
	}
	
	private void deletePivasItem(Map<Long, PivasWorklist> revertDueDateMap, PivasWorklist pivasWorklist) {

		deleteMaterialRequest(pivasWorklist);
		pivasWorklist.setStatus(PivasWorklistStatus.SysDeleted);
		em.flush();
		
		if (Boolean.TRUE.equals(pivasWorklist.getAdHocFlag())) {
			return;
		}
		
		PivasPrep pivasPrep = pivasWorklist.getPivasPrep();
		PivasWorklist prevPivasWorklist = lookupPrevPivasWorklist(pivasPrep.getPivasWorklist());
		
		if (ObjectUtils.compare(pivasWorklist.getId(), pivasPrep.getPivasWorklist().getId()) == 0 &&
			ObjectUtils.compare(pivasWorklist.getId(), prevPivasWorklist.getId()) != 0) {
			pivasPrep.setPivasWorklist(prevPivasWorklist);
			pivasPrep.setDueDate(prevPivasWorklist.getNextDueDate());
			revertDueDateMap.put(pivasWorklist.getMedProfileMoItemId(), pivasWorklist);
		}
		if (prevPivasWorklist.getStatus() == PivasWorklistStatus.SysDeleted) {
			pivasPrep.setStatus(RecordStatus.Delete);
			em.persist(constructPivasWorklist(prevPivasWorklist));
		}
	}
	
	private PivasWorklist lookupPrevPivasWorklist(PivasWorklist pivasWorklist) {
		
		while (pivasWorklist.getPrevPivasWorklistId() != null) {
			pivasWorklist = em.find(PivasWorklist.class, pivasWorklist.getPrevPivasWorklistId());
			if (pivasWorklist.getStatus() != PivasWorklistStatus.SysDeleted) {
				break;
			}
		}
		return pivasWorklist;
	}
	
	private void deleteMaterialRequest(PivasWorklist pivasWorklist) {
		
		MaterialRequest materialRequest = pivasWorklist.getMaterialRequest();
		if (materialRequest == null) {
			return;
		}
		
		for (MaterialRequestItem materialRequestItem: pivasWorklist.getMaterialRequest().getMaterialRequestItemList()) {
			if (MaterialRequestItemStatus.None_Outstanding.contains(materialRequestItem.getStatus())) {
				materialRequestItem.setStatus(MaterialRequestItemStatus.SysDeleted);
			}
		}
	}
	
	private PivasWorklist constructPivasWorklist(PivasWorklist pivasWorklist) {
		
		PivasWorklist newPivasWorklist = new PivasWorklist();
		newPivasWorklist.setWorkstoreGroupCode(pivasWorklist.getWorkstoreGroupCode());
		newPivasWorklist.setSupplyDate(pivasWorklist.getSupplyDate());
		newPivasWorklist.setDueDate(pivasWorklist.getDueDate());
		newPivasWorklist.setModifyDate(pivasWorklist.getModifyDate());
		newPivasWorklist.setStatus(PivasWorklistStatus.New);
		newPivasWorklist.setWorkstore(pivasWorklist.getWorkstore());
		newPivasWorklist.setFirstDoseMaterialRequest(pivasWorklist.getFirstDoseMaterialRequest());
		newPivasWorklist.setMedProfile(pivasWorklist.getMedProfile());
		newPivasWorklist.setMedProfileMoItem(pivasWorklist.getMedProfileMoItem());
		
		return newPivasWorklist;
	}
	
	@SuppressWarnings("unchecked")
	private void deleteDeliveryItemToCorp(List<Long> deleteDeliveryItemIdList, Map<Long,DeliveryItem> deliveryItemMap){
		
		if (deleteDeliveryItemIdList.isEmpty()) {
			return;
		}
		
		List<DeliveryItem> deleteDeliveryItemList = em.createQuery(
				"select o from DeliveryItem o" + // 20120214 index check : DeliveryItem.id : PK_DELIVERY_ITEM
				" where o.id in :deliveryItemIdList")
				.setParameter("deliveryItemIdList", deleteDeliveryItemIdList)
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
				.getResultList();
		
		for (DeliveryItem deleteDeliveryItem : deleteDeliveryItemList) {					
			if (!deliveryItemMap.get(deleteDeliveryItem.getId()).getVersion().equals(deleteDeliveryItem.getVersion())) {
				throw new OptimisticLockException();
			}		
			if (deleteDeliveryItem.getChargeQty() != null) { //mpSfiInvoice exist
				logger.debug("deliveryItem [#0] chargeQty is not null", deleteDeliveryItem.getId());
				throw new OptimisticLockException();
			}
		}

		for (DeliveryItem deleteDeliveryItem : deleteDeliveryItemList) {	
			deleteDeliveryItem.setStatus(DeliveryItemStatus.Deleted);
			if (deleteDeliveryItem.getPurchaseRequest() != null) {
				deleteDeliveryItem.getPurchaseRequest().setInvoiceStatus(InvoiceStatus.Void);
				deleteDeliveryItem.setChargeQty(null);
			}
		}
		deletePivasItemsAndRevertPoItemDueDates(deleteDeliveryItemList);
		em.flush();
		
		boolean updateDispOrderItem = corpPmsServiceProxy.updateDispOrderItemStatusToDeleted(deleteDeliveryItemIdList, CDDH_LEGACY_PERSISTENCE_ENABLED.get(false));
		if (!updateDispOrderItem) {
			throw new OptimisticLockException();
		}
	}
	
	private boolean haveMpSfiInvoice(DeliveryItem deliveryItem){
		return ( deliveryItem.getChargeQty() != null && deliveryItem.getPurchaseRequest() == null );
	}
	
	@Override
	public String getResultMsg() {
		return resultMsg;
	} 
	
	@Remove
	public void destroy() {
		
	}

	@Override
	public String getBatchNum() {
		// TODO Auto-generated method stub
		return batchNum;
	}
}
