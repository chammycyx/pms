package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.biz.reftable.DosageConversionListServiceBean;
import hk.org.ha.model.pms.corp.cache.DmDailyFrequencyCacherInf;
import hk.org.ha.model.pms.corp.cache.DmSupplFrequencyCacherInf;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDailyFrequency;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmSupplFrequency;
import hk.org.ha.model.pms.dms.persistence.PmsMultiDoseConvItem;
import hk.org.ha.model.pms.dms.persistence.PmsMultiDoseConvOrder;
import hk.org.ha.model.pms.dms.persistence.PmsMultiDoseConvSet;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsRptServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.NoResultException;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("multiDoseConvListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MultiDoseConvListServiceBean implements MultiDoseConvListServiceLocal {

	private List<PmsMultiDoseConvSet> pmsMultiDoseConvSetList;
	
	@Out(required = false)
	private List<WorkstoreDrug> pmsMultiDoseConvDmDrugList;
	
	@Out(required = false)
	private List<WorkstoreDrug> pmsMultiDoseConvExtendDrugList;
	
	@In
	private Workstore workstore;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private DmsRptServiceJmsRemote dmsRptServiceProxy;
	
	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;
	
	@In
	private DmDailyFrequencyCacherInf dmDailyFrequencyCacher;
	
	@In
	private DmSupplFrequencyCacherInf dmSupplFrequencyCacher;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent; 

	private boolean saveSuccess = true;
	
	private String errorCode = "";
	
	private String[] errorParam = {""};
	
	private boolean inconsistenceDdu;
	
	private boolean inconsistenceModu;
	
	private Map<String, Object> parameters = new HashMap<String, Object>();
	
	private boolean retrieveSuccess;

	public List<PmsMultiDoseConvSet> retrievePmsMultiDoseConvSetList(DmDrug dmDrug) {
		retrieveSuccess = true;
		inconsistenceDdu = false;
		inconsistenceModu = false;
		
		try{
			retrieveDmDrugListByDrugKey(dmDrug);
			retrieveDmDrugListByDisplayName(dmDrug);
			
			pmsMultiDoseConvSetList = dmsPmsServiceProxy.retrievePmsMultiDoseConvSetList(
					dmDrug.getDrugKey(), 
					workstore.getWorkstoreGroup().getWorkstoreGroupCode(),
					dmDrug.getPmsFmStatus().getFmStatus());
			
			for( PmsMultiDoseConvSet pmsMultiDoseConvSet : pmsMultiDoseConvSetList ){
				pmsMultiDoseConvSet.setDmDrug(dmDrug);
				for( PmsMultiDoseConvItem pmsMultiDoseConvItem : pmsMultiDoseConvSet.getPmsMultiDoseConvItemList() ){	
					pmsMultiDoseConvItem.setPmsMultiDoseConvSet( pmsMultiDoseConvSet );
					
					WorkstoreDrug pdcDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, pmsMultiDoseConvItem.getItemCode());
					
					if( pdcDrug != null && pdcDrug.getMsWorkstoreDrug() != null ){
						pmsMultiDoseConvItem.setDmDrug( pdcDrug.getDmDrug() );
						pmsMultiDoseConvItem.setMsWorkstoreDrug( pdcDrug.getMsWorkstoreDrug() );
					}else{
						pmsMultiDoseConvItem.setDmDrug( null );
					}
					
					DmDailyFrequency dmDailyFrequency = dmDailyFrequencyCacher.getDmDailyFrequencyByDailyFreqCode( pmsMultiDoseConvItem.getDailyFreqCode() );
					if( dmDailyFrequency != null ){
						pmsMultiDoseConvItem.setDailyFreqDesc( dmDailyFrequency.getDailyFreqDesc() );
						pmsMultiDoseConvItem.setNumOfDailyFreqInputValue( dmDailyFrequency.getNumOfInputValue() );
						if ( dmDailyFrequency.getNumOfInputValue() == 0 ){
							pmsMultiDoseConvItem.setDailyFreqVal( null );
						}
					}else{
						pmsMultiDoseConvItem.setDailyFreqDesc( "" );
						pmsMultiDoseConvItem.setNumOfDailyFreqInputValue( 0 );
						pmsMultiDoseConvItem.setDailyFreqVal( null );
					}
					
					if( StringUtils.isNotEmpty( pmsMultiDoseConvItem.getSupplFreqCode() ) ){
						DmSupplFrequency dmSupplFrequency = dmSupplFrequencyCacher.getDmSupplFrequencyBySupplFreqCode( pmsMultiDoseConvItem.getSupplFreqCode() );
						if( dmSupplFrequency != null ){
							pmsMultiDoseConvItem.setSupplFreqDesc( dmSupplFrequency.getSupplFreqDesc() );
							pmsMultiDoseConvItem.setNumOfSupplFreqInputValue( dmSupplFrequency.getNumOfInputValue() );
							if( dmSupplFrequency.getNumOfInputValue() < 2 ){
								pmsMultiDoseConvItem.setSupplFreqVal2( null );
								if( dmSupplFrequency.getNumOfInputValue() < 1 ){
									pmsMultiDoseConvItem.setSupplFreqVal1( null );
								}
							}
						}else{
							pmsMultiDoseConvItem.setSupplFreqDesc( "" );
							pmsMultiDoseConvItem.setNumOfSupplFreqInputValue( 0 );
							pmsMultiDoseConvItem.setSupplFreqVal2( null );
							pmsMultiDoseConvItem.setSupplFreqVal1( null );
						}
					}else{
						pmsMultiDoseConvItem.setSupplFreqDesc("");
						pmsMultiDoseConvItem.setNumOfSupplFreqInputValue( 0 );
						pmsMultiDoseConvItem.setSupplFreqVal1( null );
						pmsMultiDoseConvItem.setSupplFreqVal2( null );
					}
				}
				for( PmsMultiDoseConvOrder pmsMultiDoseConvOrder : pmsMultiDoseConvSet.getPmsMultiDoseConvOrderList() ){	
					pmsMultiDoseConvOrder.setPmsMultiDoseConvSet( pmsMultiDoseConvSet );
					DmDailyFrequency dmDailyFrequency = dmDailyFrequencyCacher.getDmDailyFrequencyByDailyFreqCode( pmsMultiDoseConvOrder.getDailyFreqCode() );
					if( dmDailyFrequency != null ){
						pmsMultiDoseConvOrder.setDailyFreqDesc( dmDailyFrequency.getDailyFreqDesc() );
						pmsMultiDoseConvOrder.setNumOfDailyFreqInputValue( dmDailyFrequency.getNumOfInputValue() );
						if ( dmDailyFrequency.getNumOfInputValue() == 0 ){
							pmsMultiDoseConvOrder.setDailyFreqVal( null );
						}
					}else{
						pmsMultiDoseConvOrder.setDailyFreqDesc( "" );
						pmsMultiDoseConvOrder.setNumOfDailyFreqInputValue( 0 );
						pmsMultiDoseConvOrder.setDailyFreqVal( null );
					}
					if( StringUtils.isNotEmpty( pmsMultiDoseConvOrder.getSupplFreqCode() ) ){
						DmSupplFrequency dmSupplFrequency = dmSupplFrequencyCacher.getDmSupplFrequencyBySupplFreqCode( pmsMultiDoseConvOrder.getSupplFreqCode() );
						if( dmSupplFrequency != null ){
							pmsMultiDoseConvOrder.setSupplFreqDesc( dmSupplFrequency.getSupplFreqDesc() );
							pmsMultiDoseConvOrder.setNumOfSupplFreqInputValue( dmSupplFrequency.getNumOfInputValue() );
							if( dmSupplFrequency.getNumOfInputValue() < 2 ){
								pmsMultiDoseConvOrder.setSupplFreqVal2( null );
								if( dmSupplFrequency.getNumOfInputValue() < 1 ){
									pmsMultiDoseConvOrder.setSupplFreqVal1( null );
								}
							}
						}else{
							pmsMultiDoseConvOrder.setSupplFreqDesc( "" );
							pmsMultiDoseConvOrder.setNumOfSupplFreqInputValue( 0 );
							pmsMultiDoseConvOrder.setSupplFreqVal2( null );
							pmsMultiDoseConvOrder.setSupplFreqVal1( null );
						}
					}else{
						pmsMultiDoseConvOrder.setSupplFreqDesc("");
						pmsMultiDoseConvOrder.setNumOfSupplFreqInputValue( 0 );
						pmsMultiDoseConvOrder.setSupplFreqVal1( null );
						pmsMultiDoseConvOrder.setSupplFreqVal2( null );
					}
				}
			}
			
			for( PmsMultiDoseConvSet pmsMultiDoseConvSet : pmsMultiDoseConvSetList ){
				inconsistenceDdu = checkConsistenceDdu(pmsMultiDoseConvSet);
				if( inconsistenceDdu ){
					break;
				}
			}
			for( PmsMultiDoseConvSet pmsMultiDoseConvSet : pmsMultiDoseConvSetList ){
				inconsistenceModu = checkConsistenceModu(pmsMultiDoseConvSet, dmDrug);
				if( inconsistenceModu ){
					break;
				}
			}
			
			return pmsMultiDoseConvSetList;
			
		} catch (NoResultException e){
			retrieveSuccess = false;
			return new ArrayList<PmsMultiDoseConvSet>();
		}
	}
	
	private Boolean checkConsistenceDdu(PmsMultiDoseConvSet pmsMultiDoseConvSet){
		for(PmsMultiDoseConvItem pmdci :pmsMultiDoseConvSet.getPmsMultiDoseConvItemList()){
			if( pmdci.getDmDrug() != null ){
				if  (!StringUtils.equals(pmdci.getDmDrug().getDmMoeProperty().getDispenseDosageUnit(), pmdci.getDispenseDosageUnit())) {
					return true;
				}
			}
		}
		return false;
	}
	
	private Boolean checkConsistenceModu(PmsMultiDoseConvSet pmsMultiDoseConvSet, DmDrug dmDrug){
		for(PmsMultiDoseConvOrder pmdco :pmsMultiDoseConvSet.getPmsMultiDoseConvOrderList()){
			if  (!StringUtils.equals(dmDrug.getDmMoeProperty().getMoDosageUnit(), pmdco.getMoDosageUnit())) {
				return true;
			}
		}
		return false;
	}
	
	public void retrieveDmDrugListByDrugKey(DmDrug dmDrug) {
		pmsMultiDoseConvDmDrugList = workstoreDrugCacher.getWorkstoreDrugListByDrugKey(
				workstore,
				dmDrug.getDrugKey());
		
		Collections.sort(pmsMultiDoseConvDmDrugList, new DosageConversionListServiceBean.WorkstoreDrugComparator());
		
	}
	
	public void retrieveDmDrugListByDisplayName(DmDrug dmDrug) {
		pmsMultiDoseConvExtendDrugList = workstoreDrugCacher.getWorkstoreDrugListByDisplayName(
				workstore,
				dmDrug.getDmDrugProperty().getDisplayname());
		
		Collections.sort(pmsMultiDoseConvExtendDrugList, new DosageConversionListServiceBean.WorkstoreDrugComparator());
		
	}
	
	public void updatePmsMultiDoseConvSetList(List<PmsMultiDoseConvSet> pmsMultiDoseConvSetList, DmDrug dmDrug) {
		if ( isValidMultiDoseConvSetList(pmsMultiDoseConvSetList) ) {
			
			for(PmsMultiDoseConvSet pmsMultiDoseConvSet : pmsMultiDoseConvSetList){
				pmsMultiDoseConvSet.setWorkstoreGroupCode( workstore.getWorkstoreGroup().getWorkstoreGroupCode() );
			}
		
			dmsPmsServiceProxy.updatePmsMultiDoseConvSetList(pmsMultiDoseConvSetList, dmDrug.getDrugKey(), workstore.getWorkstoreGroup().getWorkstoreGroupCode());
		}else{
			this.pmsMultiDoseConvSetList = pmsMultiDoseConvSetList;
		}
	}
	
	private Boolean isValidMultiDoseConvSetList( List<PmsMultiDoseConvSet> pmsMultiDoseConvSetList ){
		for( PmsMultiDoseConvSet pmsMultiDoseConvSet : pmsMultiDoseConvSetList ){
			if( hasDuplicateOrderList(pmsMultiDoseConvSet, pmsMultiDoseConvSetList) ){
				saveSuccess = false;
				errorCode = "0328";
				return false;
			}
		}
		saveSuccess = true;
		errorCode = "";
		return true;
		
	}
	
	private boolean hasDuplicateOrderList( PmsMultiDoseConvSet pmsMultiDoseConvSet, List<PmsMultiDoseConvSet> pmsMultiDoseConvSetList ){
		boolean hasDuplicateOrderList = false;
		Map<String, PmsMultiDoseConvOrder> multiDoseConvOrderMap = new HashMap<String, PmsMultiDoseConvOrder>();
		List<PmsMultiDoseConvOrder> pmsMultiDoseConvOrderList = pmsMultiDoseConvSet.getPmsMultiDoseConvOrderList();
		for( PmsMultiDoseConvOrder pmsMultiDoseConvOrder : pmsMultiDoseConvOrderList ){
			String key = getMultiDoseConvOrderKey( pmsMultiDoseConvOrder );
			multiDoseConvOrderMap.put(key, pmsMultiDoseConvOrder);
		}
		
		for( PmsMultiDoseConvSet pmdcs : pmsMultiDoseConvSetList ){
			if( pmsMultiDoseConvSet.getSetNum().equals(pmdcs.getSetNum()) ){
				continue;
			}
			List<PmsMultiDoseConvOrder> pmdcoList = pmdcs.getPmsMultiDoseConvOrderList();
			if( pmsMultiDoseConvOrderList.size() != pmdcoList.size() ){
				continue;
			}
			hasDuplicateOrderList = true;
			for( PmsMultiDoseConvOrder pmdco : pmdcoList ){
				String key = getMultiDoseConvOrderKey( pmdco );
				if( !multiDoseConvOrderMap.containsKey(key) ){
					hasDuplicateOrderList = false;
					break;
				}
			}
			if(hasDuplicateOrderList){
				errorParam = new String[]{getSetNumString(pmsMultiDoseConvSet.getSetNum()), getSetNumString(pmdcs.getSetNum())};
				return true;
			}
		}
		return false;
		
	}
	
	private String getMultiDoseConvOrderKey(PmsMultiDoseConvOrder pmsMultiDoseConvOrder){
		StringBuilder key = new StringBuilder();
		key.append(pmsMultiDoseConvOrder.getMoDosage().toString());
		key.append(pmsMultiDoseConvOrder.getDailyFreqCode());
		if( pmsMultiDoseConvOrder.getDailyFreqVal() != null ){
			key.append(pmsMultiDoseConvOrder.getDailyFreqVal());
		}
		if( StringUtils.isNotBlank(pmsMultiDoseConvOrder.getSupplFreqCode()) ){
			key.append(pmsMultiDoseConvOrder.getSupplFreqCode());
		}
		if( pmsMultiDoseConvOrder.getSupplFreqVal1() != null ){
			key.append(pmsMultiDoseConvOrder.getSupplFreqVal1());
		}
		if( pmsMultiDoseConvOrder.getSupplFreqVal2() != null ){
			key.append(pmsMultiDoseConvOrder.getSupplFreqVal2());
		}
		
		return key.toString();
	}
	
	private String getSetNumString(Integer setNum){
		if( setNum < 1000 ){
			return StringUtils.leftPad(setNum.toString(), 3, "0");
		}else{
			return setNum.toString();
		}
	}
	
	public boolean isRetrieveSuccess(){
		return retrieveSuccess;
	}
	
	public boolean isInconsistenceDdu(){
		return inconsistenceDdu;
	}

	public boolean isInconsistenceModu() {
		return inconsistenceModu;
	}

	public boolean isSaveSuccess() {
		return saveSuccess;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String[] getErrorParam() {
		return errorParam;
	}

	@Remove
	public void destroy() {
		if (pmsMultiDoseConvSetList != null) {
			pmsMultiDoseConvSetList = null;
		}
		if (pmsMultiDoseConvDmDrugList != null) {
			pmsMultiDoseConvDmDrugList = null;
		}
		if (pmsMultiDoseConvExtendDrugList != null) {
			pmsMultiDoseConvExtendDrugList = null;
		}
		if ( StringUtils.isNotEmpty(errorCode) ){
			errorCode = "";
		}
		if (errorParam.length > 0){
			errorParam = new String[]{};
		}
		inconsistenceDdu = false;
		inconsistenceModu = false;
	}

}
