package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.asa.exception.pas.PasException;
import hk.org.ha.model.pms.dms.persistence.PmsRestrictItem;

import java.util.List;

import javax.ejb.Local;

@Local
public interface PmsRestrictItemSpecByItemListServiceLocal {

	void retrievePasSpecList()throws PasException ;
	
	void retrievePmsRestrictItemListByItem(String itemCode, String pmsFmStatus);
	
	void updatePmsRestrictItemList(List<PmsRestrictItem> newPmsRestrictItemList, String itemCode, String fmStatus);
	
	void destroy();
	
}
 