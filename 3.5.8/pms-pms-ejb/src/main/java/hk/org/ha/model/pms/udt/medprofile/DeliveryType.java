package hk.org.ha.model.pms.udt.medprofile;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DeliveryType implements StringValuedEnum {

	Normal("N", "Normal");
	
    private final String dataValue;
    private final String displayValue;
    
	private DeliveryType(String dataValue, String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}

	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}
	
    public static DeliveryType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DeliveryType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<DeliveryType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<DeliveryType> getEnumClass() {
    		return DeliveryType.class;
    	}
    }
}
