package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.vo.report.EdsWorkloadRpt;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface EdsWorkloadRptServiceLocal {

	void retrieveEdsWorkloadRpt(Integer selectedTab, Date date);
	
	List<EdsWorkloadRpt> retrieveEdsWorkloadRptList(Date date);
	
	void generateEdsWorkloadRpt();
	
	void printEdsWorkloadRpt();
	
	void destroy();
	
}
