package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import hk.org.ha.model.pms.udt.reftable.MachineType;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class ItemLocationRpt implements Serializable {

	private static final long serialVersionUID = 1L;

	private String itemCode;

	private String fullDrugDesc;

	private String baseUnit;

	private Integer pickStationNum;

	private String binNum;

	private String suspend;

	private MachineType type;

	private String machineType;

	private String bakerCellNum;

	private String orderBy;

	private String workstoreCode;
	
	private String drugName;
	
	private String tradeName;
	
	private String strength;
	
	private String formDesc;
	
	private Integer maxQty;
	
	private String hostName;
	
	public String getDrugName() {
		return drugName;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public String getTradeName() {
		return tradeName;
	}

	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}

	public String getStrength() {
		return strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public String getFormDesc() {
		return formDesc;
	}

	public void setFormDesc(String formDesc) {
		this.formDesc = formDesc;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}	
	
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBinNum(String binNum) {
		this.binNum = binNum;
	}

	public String getBinNum() {
		return binNum;
	}

	public void setSuspend(String suspend) {
		this.suspend = suspend;
	}

	public String getSuspend() {
		return suspend;
	}

	public void setType(MachineType type) {
		this.type = type;
	}

	public MachineType getType() {
		return type;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public String getFullDrugDesc() {
		return fullDrugDesc;
	}

	public void setFullDrugDesc(String fullDrugDesc) {
		this.fullDrugDesc = fullDrugDesc;
	}

	public Integer getPickStationNum() {
		return pickStationNum;
	}

	public void setPickStationNum(Integer pickStationNum) {
		this.pickStationNum = pickStationNum;
	}

	public String getMachineType() {
		return machineType;
	}

	public void setMachineType(String machineType) {
		this.machineType = machineType;
	}

	public String getBakerCellNum() {
		return bakerCellNum;
	}

	public void setBakerCellNum(String bakerCellNum) {
		this.bakerCellNum = bakerCellNum;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public void setMaxQty(Integer maxQty) {
		this.maxQty = maxQty;
	}

	public Integer getMaxQty() {
		return maxQty;
	}

}
