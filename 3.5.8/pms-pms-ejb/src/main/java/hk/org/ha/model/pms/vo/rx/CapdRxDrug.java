package hk.org.ha.model.pms.vo.rx;

import hk.org.ha.model.pms.dms.vo.Capd;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="CapdRxDrug")
@ExternalizedBean(type=DefaultExternalizer.class)
public class CapdRxDrug extends RxItem implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement(required = true)
	private String supplierSystem;
	
	@XmlElement(required = true)
	private String calciumStrength;

	@XmlElement(required = true)
	private ActionStatus actionStatus;
	
	@XmlElement(name="CapdItem", required = true)
	private List<CapdItem> capdItemList;
	
	@XmlTransient
	private Boolean updateGroupNumFlag;
	
	@XmlTransient
	private Boolean updateCapdItemQty;	
		
	public CapdRxDrug(){
		setUpdateGroupNumFlag(Boolean.FALSE);
	}
	
	public void initDmCapd() {
		for (CapdItem capdItem: this.getCapdItemList()) {
			Capd capd = new Capd();
			capd.setItemCode(capdItem.getItemCode());
			capdItem.setCapd(capd);
		}
	}
	
	public String getSupplierSystem() {
		return supplierSystem;
	}

	public void setSupplierSystem(String supplierSystem) {
		this.supplierSystem = supplierSystem;
	}

	public String getCalciumStrength() {
		return calciumStrength;
	}

	public void setCalciumStrength(String calciumStrength) {
		this.calciumStrength = calciumStrength;
	}
	
	public ActionStatus getActionStatus() {
		return actionStatus;
	}

	public void setActionStatus(ActionStatus actionStatus) {
		this.actionStatus = actionStatus;
	}

	public List<CapdItem> getCapdItemList() {
		if ( capdItemList == null ) {
			capdItemList = new ArrayList<CapdItem>();
		}
		return capdItemList;
	}

	public void setCapdItemList(List<CapdItem> capdItemList) {
		this.capdItemList = capdItemList;
	}

	public void addCapdItem(CapdItem capdItem) {
		this.getCapdItemList().add(capdItem);
	}

	public void setUpdateGroupNumFlag(Boolean updateGroupNumFlag) {
		this.updateGroupNumFlag = updateGroupNumFlag;
	}

	public Boolean getUpdateGroupNumFlag() {
		if ( updateGroupNumFlag == null ) {
			updateGroupNumFlag = Boolean.FALSE;
		}
		return updateGroupNumFlag;
	}

	public void setUpdateCapdItemQty(Boolean updateCapdItemQty) {
		this.updateCapdItemQty = updateCapdItemQty;
	}

	public Boolean getUpdateCapdItemQty() {
		return updateCapdItemQty;
	}	
	
	public int getMaxDurationInDay() {
		int maxDuration = 0;
		
		for (CapdItem capdItem: getCapdItemList()) {
			int durationInDay = capdItem.getDurationInDay();
			if (durationInDay > maxDuration) {
				maxDuration = durationInDay;
			}
		}
		
		return maxDuration;
	}
}
