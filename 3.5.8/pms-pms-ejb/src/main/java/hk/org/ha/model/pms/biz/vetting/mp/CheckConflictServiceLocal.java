package hk.org.ha.model.pms.biz.vetting.mp;
import hk.org.ha.model.pms.vo.medprofile.CheckConflictItem;

import java.util.List;

import javax.ejb.Local;

@Local
public interface CheckConflictServiceLocal {

	void checkConflictExists(String orderNum, List<CheckConflictItem> medProfileCheckConflictItemList,
    		List<CheckConflictItem> replenishmentCheckConflictItemList);
	void destroy();
}
