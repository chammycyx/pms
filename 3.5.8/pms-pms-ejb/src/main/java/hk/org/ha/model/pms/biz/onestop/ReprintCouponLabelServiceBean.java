package hk.org.ha.model.pms.biz.onestop;

import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_LARGEFONT_ENABLED;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.label.DispLabelBuilderLocal;
import hk.org.ha.model.pms.biz.printing.InvoiceRendererLocal;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrintLabelManagerLocal;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.biz.refill.RefillCouponRendererLocal;
import hk.org.ha.model.pms.biz.report.InfusionInstructionSummaryManagerLocal;
import hk.org.ha.model.pms.biz.report.MedSummaryRptManagerLocal;
import hk.org.ha.model.pms.biz.vetting.CapdVoucherManagerLocal;
import hk.org.ha.model.pms.persistence.disp.CapdVoucher;
import hk.org.ha.model.pms.persistence.disp.CapdVoucherItem;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.printing.PrintType;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.label.ChestLabel;
import hk.org.ha.model.pms.vo.label.DispLabel;
import hk.org.ha.model.pms.vo.onestop.ReprintDispOrderItemInfo;
import hk.org.ha.model.pms.vo.onestop.ReprintOrderSummary;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.PrinterSelect;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("reprintCouponLabelService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ReprintCouponLabelServiceBean implements ReprintCouponLabelServiceBeanLocal {
		
	@PersistenceContext
	private EntityManager em;
	
	@Logger
	private Log logger;
	
	@In
	private CapdVoucherManagerLocal capdVoucherManager;
	
	@In
	private InvoiceRendererLocal invoiceRenderer;
	
	@In
	private PrintAgentInf printAgent;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintLabelManagerLocal printLabelManager;
	
	@In
	private DispLabelBuilderLocal dispLabelBuilder;
	
	@In
	private RefillCouponRendererLocal refillCouponRenderer;	
	
	@In
	private MedSummaryRptManagerLocal medSummaryRptManager;
	
	@In
	private ReprintOrderSummary reprintOrderSummary;
	
	@In
	private InfusionInstructionSummaryManagerLocal infusionInstructionSummaryManager;
	
	private DateFormat df = new SimpleDateFormat("ddMMyyyy"); 
		
	public String reprintLabelAndRefillCoupon(List<ReprintDispOrderItemInfo> reprintDispOrderItemList, List<Long> stdRefillScheduleItemList, 
											List<Long> reprintCapdVoucherList, Boolean printSfiInvoice, Boolean printSfiCoupon, PrintLang printLang, 
											Boolean printMedSummaryReport, Date ticketDate, String ticketNum)
	{
		
		List<DispOrderItem> checkDoiList = new ArrayList<DispOrderItem>();		
		boolean firstItem = true;
		for (ReprintDispOrderItemInfo reprintItemInfo : reprintDispOrderItemList){
			DispOrderItem checkDoi = em.find(DispOrderItem.class, reprintItemInfo.getDispOrderItemId());
			if( firstItem ){
				firstItem = false;
				if( checkDoi != null ){
					DispOrder checkDo = checkDoi.getDispOrder();
					if( checkDo != null && ( checkDo.getStatus() == DispOrderStatus.Deleted || checkDo.getStatus() == DispOrderStatus.SysDeleted ) ){
						return "0307";
					}
				}	
			}
			if( checkDoi != null ){
				checkDoiList.add(checkDoi);
			}
		}
		
		if (!checkDoiList.isEmpty())
		{
			printLabelManager.updateBaseLabelBinNumForReprint(checkDoiList);
		}		
		
		if (printMedSummaryReport)
		{
			medSummaryRptManager.printMedSummaryRpt(ticketDate, ticketNum, printLang);
		}
		
		if (!reprintDispOrderItemList.isEmpty())
		{
			List<DispOrderItem> dispOrderItemList = new ArrayList<DispOrderItem>();			
			for (ReprintDispOrderItemInfo reprintItemInfo : reprintDispOrderItemList)
			{
				for (DispOrderItem dispOrderItem : reprintOrderSummary.getReprintDispOrderItemList())
				{
					if (reprintItemInfo.getDispOrderItemId().equals(dispOrderItem.getId()))
					{
						dispOrderItem.setNumOfLabel(reprintItemInfo.getNumOfLabel());
						dispOrderItemList.add(dispOrderItem);
						break;
					}
				}
			}
			
			reprintDispItemLabel(dispOrderItemList, printLang);
		}
					
		if (!stdRefillScheduleItemList.isEmpty()) 
		{
			for (Long id : stdRefillScheduleItemList)
			{
				for (RefillSchedule stdRefillScheduleItem : reprintOrderSummary.getReprintRefillScheduleList()) 
				{
					if (id.equals(stdRefillScheduleItem.getId()))
					{
						refillCouponRenderer.printStandardRefillCoupon(stdRefillScheduleItem, true, printLang, 1);
						break;
					}
				}
			}
		}
		
		if (!reprintCapdVoucherList.isEmpty())
		{
			List<CapdVoucher> capdVoucherList = new ArrayList<CapdVoucher>();			
			for (Long id : reprintCapdVoucherList)
			{
				for (CapdVoucher capdVoucher : reprintOrderSummary.getReprintCapdVoucherList())
				{
					if (id.equals(capdVoucher.getId()))
					{
						Collections.sort(capdVoucher.getCapdVoucherItemList(), new ComparatorForCapdVoucherItemList());
						capdVoucherList.add(capdVoucher);
						break;
					}
				}
			}
						
			capdVoucherManager.printCapdVoucher(printLang, capdVoucherList, true);
		}
		
		if (printSfiInvoice) 
		{
			invoiceRenderer.printSfiInvoice(reprintOrderSummary.getSfiInvoice(), true);
		}
			
		if (printSfiCoupon)
		{
			for (RefillSchedule sfiRefillScheduleItem : reprintOrderSummary.getReprintSfiRefillScheduleItemList()) {
				refillCouponRenderer.printSfiRefillCoupon(sfiRefillScheduleItem, true, printLang);
			}
		}
		
		return null;
	}
	
	private void reprintDispItemLabel(List<DispOrderItem> dispOrderItemList, PrintLang printLang) 
	{
		DispOrder dispOrder = dispOrderItemList.get(0).getDispOrder();
		
		List<RenderAndPrintJob> renderAndPrintJobList = new ArrayList<RenderAndPrintJob>();

		boolean printChest = false;
		for (DispOrderItem doi : dispOrderItemList) {
			if (doi.getPharmOrderItem().getChestFlag()) {
				printChest = true;
				break;
			}
		}
		
		if (printChest)
		{			
			dispOrder.loadChestLabel();
			for (ChestLabel chestLabel:dispOrder.getChestLabelList().getChestLabels()) {
				PrintOption printOption = new PrintOption();
				printOption.setPrintLang(printLang);
				if (chestLabel.getLargeLayoutFlag() != null && chestLabel.getLargeLayoutFlag()) {
					printOption.setPrintType(PrintType.LargeLayout);
				} else {
					printOption.setPrintType(PrintType.Normal);
				}
				
				renderAndPrintJobList.add(new RenderAndPrintJob("ChestLabel",
												printOption,
												printerSelector.retrievePrinterSelect(PrintDocType.OtherLabel),
												dispLabelBuilder.getChestLabelParam(chestLabel),
												Arrays.asList(chestLabel),
												"[ChestLabel] TicketNum:" + chestLabel.getTicketNum()
												));
			}
		}

		PrinterSelect printerSelect = printLabelManager.retrievePrinterSelectOnLocalWorkstation();
		boolean printInfusionInstructionSummary = false;
			
		printerSelect.setReverse(true);
		printLabelManager.updateBaseLabelBinNumForReprint(dispOrderItemList);
			
		for (DispOrderItem dispOrderItem : dispOrderItemList) {
			
			if (dispOrderItem.getPharmOrderItem().getChestFlag()) {
				continue;
			}
			
			logger.debug("reprint dispItemLabel itemCode #0 itemNum #1 baseLabelBinNum #2", dispOrderItem.getPharmOrderItem().getItemCode(), 
																							dispOrderItem.getPharmOrderItem().getItemNum(), 
																							dispOrderItem.getBaseLabel().getBinNum());
			printDispOrderItem(renderAndPrintJobList, dispOrderItem, printLang, printerSelect);
				
			if ( !printInfusionInstructionSummary ){
				printInfusionInstructionSummary = infusionInstructionSummaryManager.checkIfPrintInfusionInstructionSummary(dispOrderItem);
			}
		}

		if ( printInfusionInstructionSummary ) {
			infusionInstructionSummaryManager.prepareInfusionInstructionSummaryPrintJobWithReminder(renderAndPrintJobList, printLang, dispOrder);
		}
		
		if (!renderAndPrintJobList.isEmpty()) {
			printAgent.renderAndPrint(renderAndPrintJobList);
		}
		
	}
	
	private void printDispOrderItem(List<RenderAndPrintJob> renderAndPrintJobList, DispOrderItem dispOrderItem, PrintLang printLang, PrinterSelect printerSelect) {

			// print label
			PrintOption printOption = new PrintOption();
			printOption.setPrintLang(printLang);
			if (dispOrderItem.getBaseLabel().getLargeLayoutFlag() != null && dispOrderItem.getBaseLabel().getLargeLayoutFlag()) {
				printOption.setPrintType(PrintType.LargeLayout);
			} else if (LABEL_DISPLABEL_LARGEFONT_ENABLED.get(false)) {
				printOption.setPrintType(PrintType.LargeFont);
			} else {
				printOption.setPrintType(PrintType.Normal);
			}

			PrinterSelect tmpPrintSelect = new PrinterSelect(printerSelect);
			tmpPrintSelect.setCopies(dispOrderItem.getNumOfLabel());
			logger.debug("Reprint #0 copy for dispOrderItem, size=#1", tmpPrintSelect.getCopies(), printOption.getPrintType().getDisplayValue());

			renderAndPrintJobList.add(new RenderAndPrintJob("DispLabel",
										printOption,
										tmpPrintSelect,
										dispLabelBuilder.getDispLabelParam((DispLabel)dispOrderItem.getBaseLabel()),
										Arrays.asList((DispLabel)dispOrderItem.getBaseLabel()),
										"[DispLabel] TicketDate:" + df.format(dispOrderItem.getDispOrder().getTicket().getTicketDate()) +
										", TicketNum:" + dispOrderItem.getDispOrder().getTicket().getTicketNum() +
										", ItemCode:" + dispOrderItem.getPharmOrderItem().getItemCode() +
										", HospCode:" + dispOrderItem.getDispOrder().getWorkstore().getHospCode() +
										", WorkStoreCode:" + dispOrderItem.getDispOrder().getWorkstore().getWorkstoreCode()
										));		
	}
	
	private static class ComparatorForCapdVoucherItemList implements Comparator<CapdVoucherItem>, Serializable {
		
		private static final long serialVersionUID = 1L;

		public int compare(CapdVoucherItem c1, CapdVoucherItem c2) {
						
			return c1.getDispOrderItem().getPharmOrderItem().getItemNum().compareTo(c2.getDispOrderItem().getPharmOrderItem().getItemNum());
		}
	}
	
	private <E extends Comparable<E>> int compareObj(E s1, E s2) {
		
		if (s1 == null && s2 == null) {
			return 0;
		} else if (s1 == null && s2 != null) {
			return 1;
		} else if (s1 != null && s2 == null) { 
			return -1;
		} else {
			return s1.compareTo(s2);
		}
	}
		
	@Remove
	public void destroy()
	{
	}
}
