package hk.org.ha.model.pms.vo.label;

import java.io.Serializable;
import java.util.List;

public class LabelReprintMedProfileMoItem implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private List<MpDispLabel> mpDispLabelList;
	
	private String bedNum;
	
	private String caseNum;
	
	private Long medProfileMoIdForReprint;
	
	private Long medProfilePoIdForReprint;

	public void setMpDispLabelList(
			List<MpDispLabel> mpDispLabelList) {
		this.mpDispLabelList = mpDispLabelList;
	}

	public List<MpDispLabel> getMpDispLabelList() {
		return mpDispLabelList;
	}

	public void setMedProfileMoIdForReprint(Long medProfileMoIdForReprint) {
		this.medProfileMoIdForReprint = medProfileMoIdForReprint;
	}

	public Long getMedProfileMoIdForReprint() {
		return medProfileMoIdForReprint;
	}

	public void setBedNum(String bedNum) {
		this.bedNum = bedNum;
	}

	public String getBedNum() {
		return bedNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setMedProfilePoIdForReprint(Long medProfilePoIdForReprint) {
		this.medProfilePoIdForReprint = medProfilePoIdForReprint;
	}

	public Long getMedProfilePoIdForReprint() {
		return medProfilePoIdForReprint;
	}

}