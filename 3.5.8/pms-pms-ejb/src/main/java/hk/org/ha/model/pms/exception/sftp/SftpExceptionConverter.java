package hk.org.ha.model.pms.exception.sftp;

import java.lang.reflect.Method;
import java.util.Map;

import org.granite.messaging.service.ExceptionConverter;
import org.granite.messaging.service.ServiceException;

public class SftpExceptionConverter implements ExceptionConverter {

	public static final String SFTP_COMMAND_EXCEPTION = "SftpCommandException";
	
	@Override
	public boolean accepts(Throwable t, Throwable finalException) {
		return t.getClass().equals(SftpCommandException.class);
	}

	@Override
	public ServiceException convert(Throwable t, String detail, Map<String, Object> extendedData) {
		ServiceException se; 		
		se = new ServiceException(SFTP_COMMAND_EXCEPTION, t.getMessage(), detail, t);
		se.getExtendedData().putAll(extendedData);
		try {
			Method method = t.getClass().getMethod("getExceptionClassName", new Class[0]);
			Object child = method.invoke(t, new Object[0]);
			se.getExtendedData().put("Exception Class", child);
		} catch (Exception ex) {
		}
		return se;
	}
}
