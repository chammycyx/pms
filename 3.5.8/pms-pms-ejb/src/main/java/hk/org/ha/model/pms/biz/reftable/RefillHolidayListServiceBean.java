package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.RefillHoliday;
import java.util.Collection;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("refillHolidayListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class RefillHolidayListServiceBean implements RefillHolidayListServiceLocal {
	
	@PersistenceContext
	private EntityManager em; 
	
	@Out(required = false)
	private List<RefillHoliday> refillHolidayList;

	@In
	private Workstore workstore;
	
	@In
	private RefillHolidayManagerLocal refillHolidayManager;
	
	public void retrieveRefillHolidayList()
	{
		refillHolidayList = refillHolidayManager.retrieveRefillHolidayList();
	}
	
	public void updateRefillHolidayList(Collection<RefillHoliday> updateRefillHolidayList, Collection<RefillHoliday> deleteRefillHolidayList)
	{
		workstore = em.find(Workstore.class, workstore.getId());		
		if(!deleteRefillHolidayList.isEmpty())
		{
			for(RefillHoliday delRefillHoliday:deleteRefillHolidayList)
			{
				if ( delRefillHoliday.getId() != null ) {
					em.remove(em.merge(delRefillHoliday));
				}
			}
		}
		em.flush();	
		
		for(RefillHoliday refillHoliday:updateRefillHolidayList)
		{
			if( StringUtils.isNotBlank( refillHoliday.getHoliday().toString() ) )
			{
				if(refillHoliday.getId()==null){
					refillHoliday.setWorkstore(workstore);
					em.persist(refillHoliday);
				}
				else
				{
					em.merge(refillHoliday);
				}
			}
		}
		em.flush();		
		
		retrieveRefillHolidayList();
	}
	

	@Remove
	public void destroy() 
	{
		if (refillHolidayList != null) {
			refillHolidayList = null;
		}
	}
	
}
