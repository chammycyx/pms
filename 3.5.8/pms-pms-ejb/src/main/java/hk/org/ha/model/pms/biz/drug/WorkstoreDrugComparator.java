package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;

import java.io.Serializable;
import java.util.Comparator;

import org.apache.commons.lang.builder.CompareToBuilder;

public class WorkstoreDrugComparator implements Comparator<WorkstoreDrug>, Serializable {
	
	private static final long serialVersionUID = 1L;

	@Override
	public int compare(WorkstoreDrug wsDrug1, WorkstoreDrug wsDrug2) {
		
		return new CompareToBuilder().append( wsDrug1.getDmDrug().getDrugName(), wsDrug2.getDmDrug().getDrugName())
					.append(wsDrug1.getDmDrug().getDmForm().getLabelDesc(), wsDrug2.getDmDrug().getDmForm().getLabelDesc())
					.append(wsDrug1.getDmDrug().getDmMoeProperty().getStrengthUnit(),wsDrug2.getDmDrug().getDmMoeProperty().getStrengthUnit())
					.append(wsDrug1.getDmDrug().getDmMoeProperty().getStrengthValue(),wsDrug2.getDmDrug().getDmMoeProperty().getStrengthValue())
					.append(wsDrug1.getDmDrug().getVolumeUnit(),wsDrug2.getDmDrug().getVolumeUnit())
					.append(wsDrug1.getDmDrug().getVolumeValue(),wsDrug2.getDmDrug().getVolumeValue())
					.append(wsDrug1.getDmDrug().getItemCode(),wsDrug2.getDmDrug().getItemCode())
					.toComparison();			
	}
}
