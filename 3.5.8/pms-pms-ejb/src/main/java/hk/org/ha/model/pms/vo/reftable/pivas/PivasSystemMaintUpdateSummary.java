package hk.org.ha.model.pms.vo.reftable.pivas;

import hk.org.ha.model.pms.dms.persistence.pivas.PivasContainer;
import hk.org.ha.model.pms.persistence.reftable.pivas.PivasHoliday;
import hk.org.ha.model.pms.persistence.reftable.pivas.PivasServiceHour;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PivasSystemMaintUpdateSummary implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<PivasHoliday> updatePivasHolidayList;
	
	private List<PivasHoliday> deletePivasHolidayList;
	
	private List<PivasServiceHour> updatePivasServiceHourList;
	
	private List<PivasServiceHour> deletePivasServiceHourList;
	
	private List<PivasContainer> updatePivasContainerList;
	
	private String pivasHolidaySelectedYear;

	public List<PivasHoliday> getUpdatePivasHolidayList() {
		return updatePivasHolidayList;
	}

	public void setUpdatePivasHolidayList(List<PivasHoliday> updatePivasHolidayList) {
		this.updatePivasHolidayList = updatePivasHolidayList;
	}

	public List<PivasHoliday> getDeletePivasHolidayList() {
		return deletePivasHolidayList;
	}

	public void setDeletePivasHolidayList(List<PivasHoliday> deletePivasHolidayList) {
		this.deletePivasHolidayList = deletePivasHolidayList;
	}

	public List<PivasServiceHour> getUpdatePivasServiceHourList() {
		return updatePivasServiceHourList;
	}

	public void setUpdatePivasServiceHourList(
			List<PivasServiceHour> updatePivasServiceHourList) {
		this.updatePivasServiceHourList = updatePivasServiceHourList;
	}

	public List<PivasServiceHour> getDeletePivasServiceHourList() {
		return deletePivasServiceHourList;
	}

	public void setDeletePivasServiceHourList(
			List<PivasServiceHour> deletePivasServiceHourList) {
		this.deletePivasServiceHourList = deletePivasServiceHourList;
	}

	public void setPivasHolidaySelectedYear(String pivasHolidaySelectedYear) {
		this.pivasHolidaySelectedYear = pivasHolidaySelectedYear;
	}

	public String getPivasHolidaySelectedYear() {
		return pivasHolidaySelectedYear;
	}

	public void setUpdatePivasContainerList(List<PivasContainer> updatePivasContainerList) {
		this.updatePivasContainerList = updatePivasContainerList;
	}

	public List<PivasContainer> getUpdatePivasContainerList() {
		return updatePivasContainerList;
	}
	
}