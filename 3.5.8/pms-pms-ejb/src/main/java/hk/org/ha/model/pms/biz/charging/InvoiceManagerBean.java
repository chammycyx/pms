package hk.org.ha.model.pms.biz.charging;

import static hk.org.ha.model.pms.prop.Prop.CHARGING_SFI_INVOICE_INVOICENUM_PREFIX;
import static hk.org.ha.model.pms.prop.Prop.CHARGING_SFI_SUSPEND_ORDER_SUSPENDCODE;
import static hk.org.ha.model.pms.prop.Prop.CHARGING_SFI_WAIVE_ITEMTYPE;
import static hk.org.ha.model.pms.prop.Prop.CHARGING_SFI_WAIVE_MEDCASE_PASPATGROUPCODE;
import static hk.org.ha.model.pms.prop.Prop.CHARGING_STANDARD_INVOICE_INVOICENUM_PREFIX;
import static hk.org.ha.model.pms.prop.Prop.CHARGING_STANDARD_CHARGEABLEUNITPRICE;
import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.onestop.OneStopUtilsLocal;
import hk.org.ha.model.pms.biz.order.NumberGeneratorLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.vo.pms.DrugChargeInfo;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.InvoiceItem;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.medprofile.PurchaseRequest;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.udt.YesNoBlankFlag;
import hk.org.ha.model.pms.udt.disp.InvoiceDocType;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.disp.PharmOrderItemType;
import hk.org.ha.model.pms.udt.disp.PharmOrderPatType;
import hk.org.ha.model.pms.udt.reftable.OperationProfileType;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.vo.charging.DrugChargeRuleCriteria;
import hk.org.ha.model.pms.vo.charging.OverrideChargeAmountRuleCriteria;
import hk.org.ha.model.pms.vo.charging.SaveInvoiceListInfo;
import hk.org.ha.model.pms.vo.charging.SfiForceProceedInfo;
import hk.org.ha.model.pms.vo.rx.CapdItem;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.DoseFluid;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.InjectionRxDrug;
import hk.org.ha.model.pms.vo.rx.IvAdditive;
import hk.org.ha.model.pms.vo.rx.IvFluid;
import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.log.Log;
import org.joda.time.DateMidnight;

@AutoCreate
@Stateless
@Name("invoiceManager")
@MeasureCalls
public class InvoiceManagerBean implements InvoiceManagerLocal {
	private final static String SEPARATOR = "|";

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unused")
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private Invoice invoice;
		
	@In
	private ChargeCalculationInf chargeCalculation;
	
	@In
	private OneStopUtilsLocal oneStopUtils;

	@In
	private NumberGeneratorLocal numberGenerator;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In(scope=ScopeType.SESSION, required=false)
	private OperationProfile activeProfile;

	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private SfiForceProceedInfo sfiForceProceedInfo;
		
	private DecimalFormat sfiInvoiceNumFormat = new DecimalFormat( "00000000" );
	
	private DateFormat sfiInvoiceYearFormat = new SimpleDateFormat("yy");
	
	private static final String STANDARD = "Standard";
	private static final String SFI = "sfi";
	
	public void retrieveInvoice(Long invoiceId) {
		logger.info("retrieveInvoice #0", invoiceId);
		invoice = em.find(Invoice.class, invoiceId);
	}

	public SaveInvoiceListInfo createInvoiceListFromDispOrder(OrderViewInfo orderViewInfo, DispOrder dispOrder, List<Invoice> voidInvoiceList) {

		List<DispOrderItem> sfiDispOrderItemList = new ArrayList<DispOrderItem>();
        List<DispOrderItem> stdDispOrderItemList = new ArrayList<DispOrderItem>();
        List<Invoice> invoiceList = new ArrayList<Invoice>();
        SaveInvoiceListInfo saveInvoiceListInfo = new SaveInvoiceListInfo();
        
        if ( dispOrder.getPharmOrder().getMedOrder().getPrescType() != MedOrderPrescType.MpDischarge 
        		&& dispOrder.getPharmOrder().getMedOrder().getPrescType() != MedOrderPrescType.MpHomeLeave ) {
    		for (DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList()) {
    			ActionStatus actionStatus = dispOrderItem.getPharmOrderItem().getActionStatus();
    			
    			if ( dispOrderItem.getPharmOrderItem().getMedOrderItem().isSfi() ){
    				// Count As SFI
    				if( dispOrderItem.getDispQty().compareTo(BigDecimal.ZERO) > 0  ){
    					sfiDispOrderItemList.add(dispOrderItem);
    				}
    				
    			}else if ( ActionStatus.DispByPharm.equals(actionStatus) || dispOrderItem.getPharmOrderItem().getMedOrderItem().isCapdSfi() ){
    				//Count As Std DrugCharge
    				if( dispOrderItem.getDispQty().compareTo(BigDecimal.ZERO) > 0  ){
    					stdDispOrderItemList.add(dispOrderItem);
    				}
    			}
    		}
        } else { 
        	Map<String, List<DispOrderItem>> resultMap = updateSfiChangeFlagForMpDischarge(dispOrder);
    		sfiDispOrderItemList = resultMap.get(SFI);
    		stdDispOrderItemList = resultMap.get(STANDARD);
        }        
	
		

		Invoice prevStdInvoice = null;
		Invoice prevSfiInvoice = null;
		
		for (Invoice voidInvoice : voidInvoiceList) {
			switch (voidInvoice.getDocType())
			{
				case Standard :
					prevStdInvoice = voidInvoice;
					break;
				case Sfi :
					prevSfiInvoice = voidInvoice;
					break;
			}
		}
		
		if( stdDispOrderItemList.size() > 0 ){
			invoiceList.add( createInvoiceForStdItem(stdDispOrderItemList, prevStdInvoice) );
		}
		
		if( sfiDispOrderItemList.size() > 0 ){
			Invoice sfiInvoice = createInvoiceForSfiItem(sfiDispOrderItemList, prevSfiInvoice, orderViewInfo);
			saveInvoiceListInfo = updateSaveInvoiceListInfo(saveInvoiceListInfo, sfiInvoice, prevSfiInvoice);
			invoiceList.add( sfiInvoice );
		}
		
		dispOrder.setInvoiceList(invoiceList);
		saveInvoiceListInfo.setInvoiceList(invoiceList);
		return saveInvoiceListInfo;
	}
	
	private List<InvoiceItem> createStdInvoiceItemListForCapdItem(List<DispOrderItem> stdDispOrderItemList){
		//for capdItem Charging
		Map<Integer, Map<Integer,CapdItem>> capdItemListMap = new HashMap<Integer, Map<Integer,CapdItem>>();
		Map<Integer, Map<Integer,DispOrderItem>> dispOrderItemListMap = new HashMap<Integer, Map<Integer,DispOrderItem>>();
		List<InvoiceItem> capdInvoiceItemList = new ArrayList<InvoiceItem>();
		BigDecimal chargeableUnitPrice = BigDecimal.valueOf(CHARGING_STANDARD_CHARGEABLEUNITPRICE.get(Double.valueOf(10)));
		
		for( DispOrderItem dispOrderItem : stdDispOrderItemList ){
			MedOrderItem medOrderItem = dispOrderItem.getPharmOrderItem().getMedOrderItem();			
			
			if( !capdItemListMap.containsKey(medOrderItem.getOrgItemNum()) ){
				Map<Integer,CapdItem> capdItemMap = new HashMap<Integer, CapdItem>();
				int count = 0;
				for( CapdItem capdItem : medOrderItem.getCapdRxDrug().getCapdItemList() ){
					capdItemMap.put(Integer.valueOf(count), capdItem);
					count++;
				}
				capdItemListMap.put(medOrderItem.getOrgItemNum(), capdItemMap);
			}
			
			Map<Integer,DispOrderItem> capdDoiMap = dispOrderItemListMap.get(medOrderItem.getOrgItemNum());
			if( capdDoiMap == null ){
				capdDoiMap = new HashMap<Integer,DispOrderItem>();
			}
			capdDoiMap.put(dispOrderItem.getPharmOrderItem().getOrgItemNum(), dispOrderItem);
			dispOrderItemListMap.put(medOrderItem.getOrgItemNum(), capdDoiMap);
		}

		for( Map.Entry<Integer, Map<Integer,DispOrderItem>> entry : dispOrderItemListMap.entrySet()) {
			Integer medOrderOrgItemNum = entry.getKey();
			Map<Integer,DispOrderItem> dispOrderItemMap = entry.getValue();
							
			List<Integer> pOIOrgItemNumList = new ArrayList<Integer>();
			for( Integer pharmOrderItemNum : dispOrderItemMap.keySet() ){
				pOIOrgItemNumList.add(pharmOrderItemNum);
			}
			Collections.sort(pOIOrgItemNumList);
			
			int capdListCount = 0;
			for( Integer pharmOrderItemNum : pOIOrgItemNumList){
				DispOrderItem dispOrderItem = dispOrderItemMap.get(pharmOrderItemNum);				
				CapdItem capdItem = null;
				if( capdItemListMap.get(medOrderOrgItemNum) != null ){
					capdItem = capdItemListMap.get(medOrderOrgItemNum).get(Integer.valueOf(capdListCount));				
				}
				
				InvoiceItem invoiceItem = new InvoiceItem();
				BigDecimal itemTotalAmount = BigDecimal.ZERO;
				if( capdItem != null ){
					logger.debug("capdItem : #0, #1, #2==", capdItem.getItemCode(), capdItem.getDispQty(), capdItem.getUnitOfCharge() );
					logger.debug("pharmOrderItem : #0, #1==", dispOrderItem.getPharmOrderItem().getItemCode(), dispOrderItem.getDispQty() );
					itemTotalAmount = chargeableUnitPrice.multiply(BigDecimal.valueOf(capdItem.getUnitOfCharge()));
				}
				invoiceItem.setHandleAmount( itemTotalAmount );
				invoiceItem.setAmount(invoiceItem.getHandleAmount());
				
				invoiceItem.setChargeQty(dispOrderItem.getDispQty());
	            invoiceItem.setDispOrderItem(dispOrderItem);
	            capdInvoiceItemList.add(invoiceItem);
				
				capdListCount++;
			}
		}		
		return capdInvoiceItemList;
	}
	
	private List<InvoiceItem> createStdInvoiceItemListForChargeableItem(List<DispOrderItem> stdDispOrderItemList){
		Map<Integer, Integer> medOrderItemNumMap = new HashMap<Integer,Integer>();
		List<InvoiceItem> chargeableInvoiceItemList = new ArrayList<InvoiceItem>();
		BigDecimal chargeableUnitPrice = BigDecimal.valueOf(CHARGING_STANDARD_CHARGEABLEUNITPRICE.get(Double.valueOf(10)));
		
		for( DispOrderItem dispOrderItem : stdDispOrderItemList ){
			MedOrderItem medOrderItem = dispOrderItem.getPharmOrderItem().getMedOrderItem();
			
			InvoiceItem invoiceItem = new InvoiceItem();
			if( dispOrderItem.getDispOrder().getRefillFlag() ){
				invoiceItem.setHandleAmount(BigDecimal.ZERO);
			}else{
				if( !medOrderItemNumMap.containsKey(medOrderItem.getOrgItemNum()) ){
					BigDecimal itemTotalAmount = chargeableUnitPrice.multiply(BigDecimal.valueOf(medOrderItem.getUnitOfCharge()));
					invoiceItem.setHandleAmount( itemTotalAmount );
				}else{
					invoiceItem.setHandleAmount(BigDecimal.ZERO);
				}
			}
			invoiceItem.setAmount(invoiceItem.getHandleAmount());
			      
			invoiceItem.setChargeQty(dispOrderItem.getDispQty());
	        invoiceItem.setDispOrderItem(dispOrderItem);				
	        chargeableInvoiceItemList.add(invoiceItem);
			medOrderItemNumMap.put(medOrderItem.getOrgItemNum(), medOrderItem.getOrgItemNum());
		}
		return chargeableInvoiceItemList;
	}
	
	private Invoice createInvoiceForStdItem(List<DispOrderItem> stdDispOrderItemList, Invoice prevStdInvoice){
		Invoice stdInvoice = new Invoice();
		BigDecimal totalAmount = BigDecimal.ZERO;	
		DispOrder dispOrder = stdDispOrderItemList.get(0).getDispOrder();
		PharmOrder pharmOrder = dispOrder.getPharmOrder();

		boolean requireStandardCharge = true;
		if( pharmOrder.getMedCase() != null && !pharmOrder.getMedCase().getChargeFlag() ){
			requireStandardCharge = false;
		}else if( !PharmOrderPatType.Public.equals(pharmOrder.getPatType()) ) {
			requireStandardCharge = false;
		}
		
		List<DispOrderItem> capdDispOrderItemList = new ArrayList<DispOrderItem>();
		List<DispOrderItem> chargeableDispOrderItemList = new ArrayList<DispOrderItem>();

		for( DispOrderItem dispOrderItem : stdDispOrderItemList ){
			MedOrderItem medOrderItem = dispOrderItem.getPharmOrderItem().getMedOrderItem();			

			if( requireStandardCharge && YesNoBlankFlag.Yes.equals(medOrderItem.getChargeFlag()) ){
				if( medOrderItem.getCapdRxDrug() != null ){
					capdDispOrderItemList.add(dispOrderItem);
				}else{
					chargeableDispOrderItemList.add(dispOrderItem);
				}
			}else{
				InvoiceItem invoiceItem = new InvoiceItem();
				invoiceItem.setHandleAmount(BigDecimal.ZERO);
				invoiceItem.setAmount(invoiceItem.getHandleAmount());
	            invoiceItem.setDispOrderItem(dispOrderItem);
				stdInvoice.addInvoiceItem(invoiceItem);

				PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
				if(!StringUtils.isBlank(pharmOrderItem.getHandleExemptCode()) || !StringUtils.isBlank(pharmOrderItem.getNextHandleExemptCode()) ){
					invoiceItem.setHandleExemptCode(pharmOrderItem.getHandleExemptCode());
					invoiceItem.setNextHandleExemptCode(pharmOrderItem.getNextHandleExemptCode());
					invoiceItem.setHandleExemptDate(new Date());
					invoiceItem.setHandleExemptUser(pharmOrderItem.getHandleExemptUser());
				}
			}
		}
		
		List<InvoiceItem> capdInvoiceItemList = this.createStdInvoiceItemListForCapdItem(capdDispOrderItemList);
		for( InvoiceItem capdInvoiceItem : capdInvoiceItemList ){
			stdInvoice.addInvoiceItem(capdInvoiceItem);
			totalAmount = totalAmount.add(capdInvoiceItem.getAmount());	
		}
		
		List<InvoiceItem> chargeableInvoiceItemList = this.createStdInvoiceItemListForChargeableItem(chargeableDispOrderItemList);
		for( InvoiceItem chargeableInvoiceItem : chargeableInvoiceItemList ){			
			stdInvoice.addInvoiceItem(chargeableInvoiceItem);
			totalAmount = totalAmount.add(chargeableInvoiceItem.getAmount());
		}
				
		stdInvoice.setDocType(InvoiceDocType.Standard);
		stdInvoice.setTotalAmount(totalAmount.setScale(0, RoundingMode.HALF_UP));
		
		stdInvoice.setForceProceedFlag(pharmOrder.getForceProceedFlag());
		stdInvoice.setReceiptNum(pharmOrder.getReceiptNum());

		if( stdInvoice.getForceProceedFlag() ){
			stdInvoice.setForceProceedDate(dispOrder.getDispDate());
			stdInvoice.setStatus(InvoiceStatus.Outstanding);
		}else{
			stdInvoice.setStatus(InvoiceStatus.Settled);
		}
		stdInvoice.setWorkstore(dispOrder.getWorkstore());
		stdInvoice.setInvoiceNum(this.generateInvoiceNum(stdInvoice, dispOrder.getDispDate()));
		stdInvoice.setPrintFlag(true);
		stdInvoice.setHkid(pharmOrder.getPatient().getHkid());
		stdInvoice.setOrderType(dispOrder.getOrderType());
		stdInvoice.setInvoiceDate(dispOrder.getDispDate());
		
		dispOrder.addInvoice(stdInvoice);
	
		em.persist(stdInvoice);
		em.flush();		
		
		if( stdInvoice.getForceProceedFlag() ){
			stdInvoice.setForceProceedUser(stdInvoice.getUpdateUser());
		}
		
		if (prevStdInvoice != null) {
			stdInvoice.setPrevInvoiceId(prevStdInvoice.getId());
			stdInvoice.setOrgInvoiceId(prevStdInvoice.getOrgInvoiceId());
		} else {
			stdInvoice.setOrgInvoiceId(stdInvoice.getId());
		}	
		
			
		return stdInvoice;
	}
	
	public Invoice createInvoiceForMpSfiInvoice(List<DispOrderItem> sfiDispOrderItemList, String chargeOrderNum) {
		DispOrder dispOrder = sfiDispOrderItemList.get(0).getDispOrder();
		Invoice sfiInvoice = createSfiInvoice(sfiDispOrderItemList, null);
		
		//Invoice Total Amount
		BigDecimal invoiceTotalAmt = BigDecimal.ZERO;		
		for( InvoiceItem invoiceItem : sfiInvoice.getInvoiceItemList() ){
			invoiceTotalAmt = invoiceTotalAmt.add(invoiceItem.getAmount());
		}
		
		sfiInvoice.setInvoiceDate(new Date());
		sfiInvoice.setTotalAmount(invoiceTotalAmt);
		sfiInvoice.setInvoiceNum(this.generateInvoiceNum(sfiInvoice, dispOrder.getDispDate()));
		sfiInvoice.setChargeOrderNum(chargeOrderNum);
		
		if( BigDecimal.ZERO.compareTo(sfiInvoice.getTotalAmount()) == 0 ){
			sfiInvoice.setStatus(InvoiceStatus.Settled);
			sfiInvoice.setFcsCheckDate(new Date());
		}
		return sfiInvoice;
	}
	
	public Invoice createInvoiceForPurchaseRequest(List<DispOrderItem> sfiDispOrderItemList, PurchaseRequest purchaseRequest){

		Invoice sfiInvoice = createSfiInvoice(sfiDispOrderItemList, purchaseRequest);
		
		BigDecimal invoiceTotalAmt = BigDecimal.ZERO;		
		for( InvoiceItem invoiceItem : sfiInvoice.getInvoiceItemList() ){
			invoiceTotalAmt = invoiceTotalAmt.add(invoiceItem.getAmount());
		}
		sfiInvoice.setTotalAmount(invoiceTotalAmt);

		updateSfiInvoiceWithPurchaseRequest(purchaseRequest, sfiInvoice);

		return sfiInvoice;
	}	

	private void updateSfiInvoiceWithPurchaseRequest(PurchaseRequest purchaseRequest, Invoice sfiInvoice)
	{
		if( BigDecimal.ZERO.compareTo(sfiInvoice.getTotalAmount()) == 0 ){
			sfiInvoice.setStatus(InvoiceStatus.Settled);
			sfiInvoice.setFcsCheckDate(new Date());
		}
		
		if( purchaseRequest.getInvoiceNum() != null ){
			sfiInvoice.setInvoiceDate(purchaseRequest.getInvoiceDate());
			sfiInvoice.setInvoiceNum(purchaseRequest.getInvoiceNum());
		}else{
			Date now = new Date();
			sfiInvoice.setInvoiceDate(now);
			sfiInvoice.setInvoiceNum(this.generateInvoiceNum(sfiInvoice, now));
		}
				
		if( purchaseRequest.getForceProceedDate() != null ){
			sfiInvoice.setForceProceedCode(purchaseRequest.getForceProceedCode());
			sfiInvoice.setForceProceedDate(purchaseRequest.getForceProceedDate());
			sfiInvoice.setForceProceedFlag(purchaseRequest.getForceProceedFlag());
			sfiInvoice.setForceProceedUser(purchaseRequest.getForceProceedUser());
			sfiInvoice.setManualInvoiceNum(purchaseRequest.getManualInvoiceNum());
			sfiInvoice.setManualReceiptNum(purchaseRequest.getManualReceiptNum());
		
		}
		if( purchaseRequest.getFcsCheckDate() != null ){
			sfiInvoice.setFcsCheckDate(purchaseRequest.getFcsCheckDate());
			sfiInvoice.setReceiveAmount(purchaseRequest.getReceiveAmount());
			sfiInvoice.setStatus(purchaseRequest.getInvoiceStatus());
		}
		sfiInvoice.setChargeOrderNum(purchaseRequest.getMedProfile().getChargeOrderNum());
		
	}

	public Invoice createInvoiceFromPurchaseRequestForPrint(DispOrderItem sfiDispOrderItem, PurchaseRequest purchaseRequest) {
		Invoice sfiInvoice;
		if(purchaseRequest.getInvoice()!=null)
		{
			DispOrder dispOrder = sfiDispOrderItem.getDispOrder();
			sfiInvoice = purchaseRequest.getInvoice();
			sfiInvoice.setDispOrder(dispOrder);
			sfiInvoice.setWorkstore(dispOrder.getWorkstore());
			if(sfiInvoice.getInvoiceItemList()!=null)
			{
				InvoiceItem invoiceItem = sfiInvoice.getInvoiceItemList().get(0);
				invoiceItem.setDispOrderItem(sfiDispOrderItem);
				invoiceItem.setInvoice(sfiInvoice);
			}
		}
		else
		{
			sfiInvoice = createMockInvoiceFromPurchaseRequest(sfiDispOrderItem, purchaseRequest);
		}
		
		updateSfiInvoiceWithPurchaseRequest(purchaseRequest, sfiInvoice);
		
		return sfiInvoice;
	}

	public Invoice createInvoiceFromPurchaseRequestForEnq(DispOrderItem sfiDispOrderItem, PurchaseRequest purchaseRequest ) {
		Invoice sfiInvoice;

		sfiInvoice = createMockInvoiceFromPurchaseRequest(sfiDispOrderItem, purchaseRequest);

		updateSfiInvoiceWithPurchaseRequest(purchaseRequest, sfiInvoice);
		
		return sfiInvoice;
	}

	private Invoice createMockInvoiceFromPurchaseRequest(DispOrderItem sfiDispOrderItem, PurchaseRequest purchaseRequest)
	{
		DispOrder dispOrder = sfiDispOrderItem.getDispOrder();
		Invoice sfiInvoice = new Invoice();
		sfiInvoice.setDocType(InvoiceDocType.Sfi);
		sfiInvoice.setDispOrder(dispOrder);
		sfiInvoice.setWorkstore(dispOrder.getWorkstore());
		sfiInvoice.setHkid(dispOrder.getPharmOrder().getPatient().getHkid());
		sfiInvoice.setOrderType(dispOrder.getOrderType());
		sfiInvoice.setPrintFlag(updateSfiInvoicePrintFlag(sfiInvoice,null));
		sfiInvoice.setHospCode(purchaseRequest.getHospCode());
		sfiInvoice.setStatus(purchaseRequest.getInvoiceStatus());
		sfiInvoice.setFcsCheckDate(purchaseRequest.getFcsCheckDate());
		sfiInvoice.setTotalAmount(purchaseRequest.getTotalAmount());
		
		InvoiceItem invoiceItem = new InvoiceItem();
		invoiceItem.setCost(BigDecimal.ZERO);
		invoiceItem.setMarkupAmount(BigDecimal.ZERO);
		invoiceItem.setHandleAmount(BigDecimal.ZERO);
		invoiceItem.setHandleExemptAmount(BigDecimal.ZERO);
		invoiceItem.setActualMarkupAmount(BigDecimal.ZERO);
		invoiceItem.setCapMarkupAmount(BigDecimal.ZERO);
		invoiceItem.setAmount(purchaseRequest.getTotalAmount());
		invoiceItem.setDispOrderItem(sfiDispOrderItem);
		
		BigDecimal chargeQty = null;
		if ( sfiDispOrderItem.getChargeQty() != null ) { 
			chargeQty = sfiDispOrderItem.getChargeQty();
		}
		else {
			chargeQty = sfiDispOrderItem.getDispQty();
		}
		
		invoiceItem.setChargeQty(chargeQty);
		sfiInvoice.addInvoiceItem(invoiceItem);
		
		return sfiInvoice;
	}
	
	private Invoice createSfiInvoice(List<DispOrderItem> sfiDispOrderItemList, PurchaseRequest purchaseRequest) {		
		DispOrder dispOrder = sfiDispOrderItemList.get(0).getDispOrder();
		PharmOrder pharmOrder = dispOrder.getPharmOrder();
		Invoice sfiInvoice = new Invoice();
		
		//Charge Rule for Order
		DrugChargeRuleCriteria chargingRuleCriteria = this.createDrugChargeRuleCriteriaByPharmOrder(pharmOrder);
		chargingRuleCriteria = chargeCalculation.checkPrivateHandleRule(chargingRuleCriteria);		
		
		sfiInvoice.setDocType(InvoiceDocType.Sfi);
		sfiInvoice.setStatus(InvoiceStatus.Outstanding);
				
		Map<String, List<DispOrderItem>> sameKeyDispOrderItemMap = this.createDispOrderItemListMapBySameDrugKey(sfiDispOrderItemList);
		
		for( List<DispOrderItem> dispOrderItemList : sameKeyDispOrderItemMap.values() ){
			List<InvoiceItem> sfiInvoiceItemList = this.createSfiInvoiceItemListByDispOrderItemList(dispOrderItemList, null, chargingRuleCriteria, purchaseRequest);			
			for( InvoiceItem invoiceItem :sfiInvoiceItemList ){
				sfiInvoice.addInvoiceItem(invoiceItem);
			}
		}
		
		sfiInvoice.setDispOrder(dispOrder);
		sfiInvoice.setWorkstore(dispOrder.getWorkstore());
		sfiInvoice.setPrintFlag(updateSfiInvoicePrintFlag(sfiInvoice,null));
		sfiInvoice.setHkid(dispOrder.getPharmOrder().getPatient().getHkid());
		sfiInvoice.setOrderType(dispOrder.getOrderType());
		
		if( !isExceptionDrugExistChecking(sfiInvoice, true) ){
			sfiInvoice.setStatus(InvoiceStatus.Settled);
			sfiInvoice.setFcsCheckDate(new Date());
		}
		return sfiInvoice;
	}
	
	private Invoice createInvoiceForSfiItem(List<DispOrderItem> sfiDispOrderItemList, Invoice prevSfiInvoice, OrderViewInfo orderViewInfo){
		DispOrder dispOrder = sfiDispOrderItemList.get(0).getDispOrder();
		PharmOrder pharmOrder = dispOrder.getPharmOrder();
		
		//Charge Rule for Order
		DrugChargeRuleCriteria chargingRuleCriteria = this.createDrugChargeRuleCriteriaByPharmOrder(pharmOrder);
		chargingRuleCriteria = chargeCalculation.checkPrivateHandleRule(chargingRuleCriteria);		
		
		Invoice sfiInvoice = new Invoice();
		sfiInvoice.setDocType(InvoiceDocType.Sfi);
		sfiInvoice.setStatus(InvoiceStatus.Outstanding);
				
		Map<String, List<DispOrderItem>> sameKeyDispOrderItemMap = this.createDispOrderItemListMapBySameDrugKey(sfiDispOrderItemList);
		List<Integer> sfiItemWithHandlingChargeList = this.retrievePoiOrgItemNumListForHandlingAmount(sfiDispOrderItemList);	
		
		for( List<DispOrderItem> dispOrderItemList : sameKeyDispOrderItemMap.values() ){
			List<InvoiceItem> sfiInvoiceItemList = this.createSfiInvoiceItemListByDispOrderItemList(dispOrderItemList, sfiItemWithHandlingChargeList, chargingRuleCriteria, null);			
			for( InvoiceItem invoiceItem :sfiInvoiceItemList ){
				sfiInvoice.addInvoiceItem(invoiceItem);
			}
		}
		
		//Invoice Total Amount
		BigDecimal invoiceTotalAmt = BigDecimal.ZERO;		
		for( InvoiceItem invoiceItem : sfiInvoice.getInvoiceItemList() ){
			invoiceTotalAmt = invoiceTotalAmt.add(invoiceItem.getAmount());
		}
		sfiInvoice.setTotalAmount(invoiceTotalAmt);
		sfiInvoice.setHkid(pharmOrder.getPatient().getHkid());
		sfiInvoice.setOrderType(dispOrder.getOrderType());
		sfiInvoice.setWorkstore(dispOrder.getWorkstore());
		
		if( prevSfiInvoice != null && orderViewInfo.isDispOrderWoSfiItemEditFlag() ){
			sfiInvoice.setInvoiceNum(prevSfiInvoice.getInvoiceNum());
			sfiInvoice.setForceProceedFlag(prevSfiInvoice.getForceProceedFlag());
			sfiInvoice.setForceProceedCode(prevSfiInvoice.getForceProceedCode());
			sfiInvoice.setForceProceedDate(prevSfiInvoice.getForceProceedDate());
			sfiInvoice.setForceProceedUser(prevSfiInvoice.getForceProceedUser());
			if( prevSfiInvoice.getInvoiceDate() != null ){
				sfiInvoice.setInvoiceDate(prevSfiInvoice.getInvoiceDate());
			}else{
				sfiInvoice.setInvoiceDate(dispOrder.getDispDate());
			}
			//updatePrevInvoiceStatus to SysDeleted
			Invoice sysDelInvoice = em.find(Invoice.class, prevSfiInvoice.getId());
			sysDelInvoice.setStatus(InvoiceStatus.SysDeleted);
		}else{		
			sfiInvoice.setInvoiceNum(this.generateInvoiceNum(sfiInvoice, dispOrder.getDispDate()));
			sfiInvoice.setInvoiceDate(dispOrder.getDispDate());
		}
		dispOrder.addInvoice(sfiInvoice);
		
		em.persist(sfiInvoice);
		em.flush();		
		
		if (prevSfiInvoice != null) {
			sfiInvoice.setPrevInvoiceId(prevSfiInvoice.getId());
			sfiInvoice.setOrgInvoiceId(prevSfiInvoice.getOrgInvoiceId());
		} else {
			sfiInvoice.setOrgInvoiceId(sfiInvoice.getId());
		}
		
		if( orderViewInfo.isSkipSfiClearanceFlag() && sfiForceProceedInfo != null ){
			sfiForceProceedInfo.setInvoice(sfiInvoice);
			sfiForceProceedInfo = null;
		}
		
		if( orderViewInfo.isSfiInvoicePaymentClearFlag() ){
			sfiInvoice.setStatus(InvoiceStatus.Settled);
		}

		sfiInvoice.setPrintFlag(updateSfiInvoicePrintFlag(sfiInvoice,prevSfiInvoice));		
		return sfiInvoice;
	}
	
	private Map<String, List<DispOrderItem>> createDispOrderItemListMapBySameDrugKey(List<DispOrderItem> sfiDispOrderItemList){
		Map<String, List<DispOrderItem>> sameKeyDispOrderItemMap = new HashMap<String, List<DispOrderItem>>();
		
		for ( DispOrderItem dispOrderItem : sfiDispOrderItemList ){			
			StringBuilder displaynameFormSaltKey = new StringBuilder();			
			
			PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();			
			DmDrug dmDrug = dmDrugCacher.getDmDrug(pharmOrderItem.getItemCode());
			
			//DrugKey
			displaynameFormSaltKey.append(dmDrug.getDmDrugProperty().getDisplayname()).append(SEPARATOR)
			.append(pharmOrderItem.getFormCode()).append(SEPARATOR);
			if( StringUtils.isNotBlank(dmDrug.getDmDrugProperty().getSaltProperty()) ){
				displaynameFormSaltKey.append(dmDrug.getDmDrugProperty().getSaltProperty()).append(SEPARATOR);
			}
			if( YesNoBlankFlag.Yes.getDataValue().equals(dmDrug.getDmDrugProperty().getStrengthCompul()) ){
				displaynameFormSaltKey.append(dmDrug.getStrength()).append(SEPARATOR);
			}
			
			if( sameKeyDispOrderItemMap.containsKey(displaynameFormSaltKey.toString()) ){
				List<DispOrderItem> dispOrderItemList = sameKeyDispOrderItemMap.get(displaynameFormSaltKey.toString());
				dispOrderItemList.add(dispOrderItem);
				sameKeyDispOrderItemMap.put(displaynameFormSaltKey.toString(), dispOrderItemList);
			}else{
				List<DispOrderItem> dispOrderItemList = new ArrayList<DispOrderItem>();
				dispOrderItemList.add(dispOrderItem);
				sameKeyDispOrderItemMap.put(displaynameFormSaltKey.toString(), dispOrderItemList);
			}
		}
		return sameKeyDispOrderItemMap;
	}
	
	private List<Integer> retrievePoiOrgItemNumListForHandlingAmount(List<DispOrderItem> sfiDispOrderItemList){
		List<Integer> poiOrgItemNumChargeHandlingList = new ArrayList<Integer>();
		
		Map<String, Integer> moiOrgItemNumMap = new HashMap<String, Integer>();
		
		for( DispOrderItem dispOrderItem : sfiDispOrderItemList ){
			
			PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();			
			DmDrug dmDrug = dmDrugCacher.getDmDrug(pharmOrderItem.getItemCode());
			
			StringBuilder displaynameFormSaltKey = new StringBuilder();
			displaynameFormSaltKey.append(dmDrug.getDmDrugProperty().getDisplayname()).append(SEPARATOR);
			displaynameFormSaltKey.append(pharmOrderItem.getFormCode()).append(SEPARATOR);
			if ( StringUtils.isNotBlank(dmDrug.getDmDrugProperty().getSaltProperty()) ) {
				displaynameFormSaltKey.append(dmDrug.getDmDrugProperty().getSaltProperty());
			}
			
			if( YesNoBlankFlag.Yes.getDataValue().equals(dmDrug.getDmDrugProperty().getStrengthCompul()) ){
				displaynameFormSaltKey.append(dmDrug.getStrength()).append(SEPARATOR);
			}
			
			if( !moiOrgItemNumMap.containsKey(displaynameFormSaltKey.toString()) ){
				moiOrgItemNumMap.put(displaynameFormSaltKey.toString(), dispOrderItem.getPharmOrderItem().getOrgItemNum());
			}else if( !StringUtils.isBlank(dispOrderItem.getPharmOrderItem().getHandleExemptCode()) ){
				moiOrgItemNumMap.put(displaynameFormSaltKey.toString(), dispOrderItem.getPharmOrderItem().getOrgItemNum());
			}
		}
		poiOrgItemNumChargeHandlingList.addAll(moiOrgItemNumMap.values());
		return poiOrgItemNumChargeHandlingList;
	}
		
	private DrugChargeRuleCriteria createDrugChargeRuleCriteriaByPharmOrder(PharmOrder pharmOrder){
		MedOrder medOrder = pharmOrder.getMedOrder();
		
		DrugChargeRuleCriteria chargingRuleCriteria = new DrugChargeRuleCriteria();
		chargingRuleCriteria.setPharmOrderPatType(pharmOrder.getPatType().getDataValue());
		chargingRuleCriteria.setSpecCode(pharmOrder.getSpecCode());
		chargingRuleCriteria.setMedOrderPrescType(medOrder.getPrescType().getDataValue());		
		chargingRuleCriteria.setDispHospCode(medOrder.getPatHospCode());
		chargingRuleCriteria.setPatHospCode(medOrder.getPatHospCode());
		
		if( pharmOrder.getMedCase() != null ){
			MedCase pharmMedCase = pharmOrder.getMedCase();

			chargingRuleCriteria.setPasSpecCode(pharmMedCase.getPasSpecCode());
			chargingRuleCriteria.setPasSubSpecCode(pharmMedCase.getPasSubSpecCode());			
			chargingRuleCriteria.setPasPayCode(pharmMedCase.getPasPayCode());
			chargingRuleCriteria.setPasPatGroupCode(pharmMedCase.getPasPatGroupCode());
			
			if( pharmMedCase.getCaseNum() != null && oneStopUtils.isInPatCase(pharmMedCase.getCaseNum()) ){
				chargingRuleCriteria.setInPatientCase(true);
			}			
		}
		return chargingRuleCriteria;
	}
	
	private InvoiceItem updateInvoiceItemHandlingProperties(List<Integer> handlingPoiOrgItemNumList, InvoiceItem invoiceItemIn, PharmOrderItem pharmOrderItemIn, DrugChargeRuleCriteria chargingRuleCriteria ){
		//Handle Amount Calculation
		BigDecimal handlingAmount = BigDecimal.ZERO;
		if( handlingPoiOrgItemNumList.contains(pharmOrderItemIn.getOrgItemNum()) ){
			DmDrug dmDrug = DmDrugCacher.instance().getDmDrug(pharmOrderItemIn.getItemCode());
			handlingAmount = BigDecimal.valueOf(chargeCalculation.retrieveHandlingAmtByChargeRule(chargingRuleCriteria, dmDrug.getDrugChargeInfo()));
		}
		
		if(!StringUtils.isBlank(pharmOrderItemIn.getHandleExemptCode()) || !StringUtils.isBlank(pharmOrderItemIn.getNextHandleExemptCode()) ){
			invoiceItemIn.setHandleExemptCode(pharmOrderItemIn.getHandleExemptCode());
			invoiceItemIn.setNextHandleExemptCode(pharmOrderItemIn.getNextHandleExemptCode());
			invoiceItemIn.setHandleExemptDate(new Date());
			invoiceItemIn.setHandleExemptUser(pharmOrderItemIn.getHandleExemptUser());
		}
		
		if( !StringUtils.isBlank(pharmOrderItemIn.getHandleExemptCode()) ){			
			invoiceItemIn.setHandleExemptAmount(handlingAmount);
		}
		invoiceItemIn.setHandleAmount(handlingAmount);

		return invoiceItemIn;
	}
	
	private List<InvoiceItem> createSfiInvoiceItemListByDispOrderItemList(List<DispOrderItem> dispOrderItemList, 
																			List<Integer> handlingPoiOrgItemNumList,
																			DrugChargeRuleCriteria chargingRuleCriteria, 
																			PurchaseRequest purchaseRequest){
		boolean sameKeyIncludeHandling = true; // as Init Flag in the DispOrderList
		boolean maxProfitLimitEnable = false;
		BigDecimal remainMaxProfitLimit = BigDecimal.ZERO;
		List<InvoiceItem> invoiceItemList = new ArrayList<InvoiceItem>();
		
		MedOrderPrescType prescType = dispOrderItemList.get(0).getDispOrder().getPharmOrder().getMedOrder().getPrescType();
		
		for( DispOrderItem dispOrderItem : dispOrderItemList ){
			
			PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
			if( ( (prescType == MedOrderPrescType.MpDischarge || prescType == MedOrderPrescType.MpHomeLeave) && !pharmOrderItem.getSfiChargeFlag()) || 
					( isNotMpOrder(prescType) && !StringUtils.equals(pharmOrderItem.getDupChkDisplayName(), pharmOrderItem.getMedOrderItem().getDisplayName()))){
				//solvent item
				InvoiceItem invoiceItem = new InvoiceItem();
				//Handling Exempt Properties
				if(!StringUtils.isBlank(pharmOrderItem.getHandleExemptCode()) || !StringUtils.isBlank(pharmOrderItem.getNextHandleExemptCode()) ){
					invoiceItem.setHandleExemptCode(pharmOrderItem.getHandleExemptCode());
					invoiceItem.setNextHandleExemptCode(pharmOrderItem.getNextHandleExemptCode());
					invoiceItem.setHandleExemptDate(new Date());
					invoiceItem.setHandleExemptUser(pharmOrderItem.getHandleExemptUser());
				}
				
				invoiceItem.setDispOrderItem(dispOrderItem);
				invoiceItemList.add(invoiceItem);
				continue;
			}
			
			chargingRuleCriteria.setFmStatus(pharmOrderItem.getFmStatus());
			// Default Setting
			chargingRuleCriteria.setIncludeHandling(true);
			chargingRuleCriteria.setIncludeMarkup(true);
			chargingRuleCriteria = chargeCalculation.checkSfiItemChargeRule(chargingRuleCriteria);
			//Override Including Handling for HA Patient
			if( (StringUtils.equals(chargingRuleCriteria.getPasPatGroupCode(), "HA") && !StringUtils.equals(chargingRuleCriteria.getPasPayCode(), "HAS"))  ){
				chargingRuleCriteria.setIncludeHandling(false);
			}
			OverrideChargeAmountRuleCriteria overrideChargeAmountRuleCriteria = new OverrideChargeAmountRuleCriteria();
			overrideChargeAmountRuleCriteria.updateByDrugChargeRuleCriteria(chargingRuleCriteria, dispOrderItem.getDispOrder().getWorkstore());												
			
			InvoiceItem invoiceItem = new InvoiceItem();

			//Handling Amount
			if( handlingPoiOrgItemNumList != null ){
				invoiceItem = this.updateInvoiceItemHandlingProperties(handlingPoiOrgItemNumList, invoiceItem, pharmOrderItem, chargingRuleCriteria);		
			}
			
			DrugChargeInfo drugChargeInfo = null;
			if( pharmOrderItem.getDmDrugLite() != null ){
				drugChargeInfo = pharmOrderItem.getDmDrugLite().getDrugChargeInfo();
			}else{
				DmDrug dmDrug = dmDrugCacher.getDmDrug(pharmOrderItem.getItemCode());
				drugChargeInfo = dmDrug.getDrugChargeInfo();
			}
			
			DateMidnight now = new DateMidnight();

			Double corpDrugPrice = drugChargeInfo.getCorpDrugPrice() == null ? 0.0 : drugChargeInfo.getCorpDrugPrice();
			Double markupFactor = chargeCalculation.retrieveMarkupFactorByChargeRule(overrideChargeAmountRuleCriteria, drugChargeInfo);
			
			//chargeQty
			BigDecimal chargeQty = null;
			if ( dispOrderItem.getChargeQty() != null ) { 
				chargeQty = dispOrderItem.getChargeQty();
			} 
			
			if ( chargeQty == null ) { //for mpSfiInvoice(private patient) new item
				chargeQty = dispOrderItem.getDispQty();
			}
			
			//Cost
			BigDecimal cost = null;
			cost = BigDecimal.valueOf(corpDrugPrice).multiply(chargeQty);
			invoiceItem.setCost(cost);
			invoiceItem.setChargeQty(chargeQty);
			
			//actualMarkupAmount
			BigDecimal actualMarkupAmount = BigDecimal.valueOf(markupFactor).subtract(BigDecimal.ONE);
			actualMarkupAmount = actualMarkupAmount.multiply(cost);
			invoiceItem.setActualMarkupAmount(actualMarkupAmount);
			
			//capMarkupAmount
			BigDecimal capMarkupAmount = actualMarkupAmount;
			if( sameKeyIncludeHandling ){
				maxProfitLimitEnable = chargeCalculation.isMarkupProfitLimitEnable(chargingRuleCriteria);
				remainMaxProfitLimit = BigDecimal.valueOf(chargeCalculation.retrieveMaxProfitLimit(chargingRuleCriteria));
			}
			
			if( maxProfitLimitEnable && actualMarkupAmount.compareTo(remainMaxProfitLimit) > 0 ){
				capMarkupAmount = remainMaxProfitLimit;
			}
			invoiceItem.setCapMarkupAmount(capMarkupAmount);
			
			//ActualAmount After Cap Before Rounding
			BigDecimal actualAmount = cost.add(capMarkupAmount);
			
			//Final Amount
			BigDecimal amount =  actualAmount.setScale(0, RoundingMode.HALF_UP);
			amount = amount.add(invoiceItem.getHandleAmount()).subtract(invoiceItem.getHandleExemptAmount());
			invoiceItem.setAmount(amount);
			
			//MarkupAmount
			BigDecimal roundAmount = actualAmount.setScale(0, RoundingMode.HALF_UP).subtract(actualAmount);
			
			BigDecimal markupAmount = capMarkupAmount;
			if( remainMaxProfitLimit.compareTo(BigDecimal.ZERO) == 0 ){
				markupAmount = BigDecimal.ZERO;
			}else{
				if( maxProfitLimitEnable && roundAmount.add(capMarkupAmount).compareTo(remainMaxProfitLimit) > 0 ){
					markupAmount = markupAmount.subtract(roundAmount);
				}else{
					markupAmount = markupAmount.add(roundAmount);
				}
			}
			invoiceItem.setMarkupAmount(markupAmount.setScale(0, RoundingMode.HALF_UP));
				
			sameKeyIncludeHandling = false;
			if( maxProfitLimitEnable ){
				remainMaxProfitLimit = remainMaxProfitLimit.subtract(invoiceItem.getMarkupAmount());
				if( remainMaxProfitLimit.compareTo(BigDecimal.ZERO) < 0 ){
					remainMaxProfitLimit = BigDecimal.ZERO;
				}
				logger.debug("createSfiInvoiceItemListByDispOrderItemList==============capMarkupAmount, markupAmount, remainMaxProfitLimit, roundAmount :#0, #1, #2, #3", capMarkupAmount, invoiceItem.getMarkupAmount(),remainMaxProfitLimit, roundAmount);				
			}
			
			invoiceItem.setDispOrderItem(dispOrderItem);
			invoiceItemList.add(invoiceItem);
		}
		return invoiceItemList;		
	}

	private Boolean updateSfiInvoicePrintFlag(Invoice sfiInvoice, Invoice prevSfiInvoice){
		PharmOrder pharmOrder = sfiInvoice.getDispOrder().getPharmOrder();
		
		if( PharmOrderPatType.Doh.equals(pharmOrder.getPatType()) ){
			return true;
		}else if( prevSfiInvoice != null && StringUtils.equals( prevSfiInvoice.getInvoiceNum() , sfiInvoice.getInvoiceNum()) ){
			return true;
		}else if( !this.isExceptionDrugExist(sfiInvoice) ){
			return true;
		}
		return false;
	}
	
	private SaveInvoiceListInfo updateSaveInvoiceListInfo(SaveInvoiceListInfo saveInvoiceListInfoIn, Invoice sfiInvoice, Invoice prevSfiInvoice){
		PharmOrder pharmOrder = sfiInvoice.getDispOrder().getPharmOrder();

		if( BigDecimal.ZERO.compareTo(sfiInvoice.getTotalAmount()) == 0 ){			//Zero Amount Invoice
			saveInvoiceListInfoIn.setSuspendOrder(false);
		}else if( PharmOrderPatType.Doh.equals(pharmOrder.getPatType()) ){	// Doh Patient
			saveInvoiceListInfoIn.setSuspendOrder(false);
		}else if( !this.isExceptionDrugExist(sfiInvoice) ){					// Ha Staff Benefit
			saveInvoiceListInfoIn.setSuspendOrder(false);
			saveInvoiceListInfoIn.setAlertMsg("0097");
		}else if( !this.isExceptionDrugExistExcludeZeroAmount(sfiInvoice) ){
			saveInvoiceListInfoIn.setSuspendOrder(false);
		}else if( !pharmOrder.getAwaitSfiFlag() ){							// uncheck AwaitSfi in Vetting
			saveInvoiceListInfoIn.setSuspendOrder(false);
		}else if( prevSfiInvoice != null && 
				  StringUtils.equals( prevSfiInvoice.getInvoiceNum() , sfiInvoice.getInvoiceNum() ) ){
			if( InvoiceStatus.Settled == sfiInvoice.getStatus() ){
				saveInvoiceListInfoIn.setSuspendOrder(false);					// Settled by Check Clearance
			}else if( activeProfile != null && !OperationProfileType.Pms.equals(activeProfile.getType()) ){
				saveInvoiceListInfoIn.setSuspendOrder(false);					// SaveOrder without Sfi Item changes in EDS Mode
			}else if( sfiInvoice.getForceProceedFlag() ){
				saveInvoiceListInfoIn.setSuspendOrder(false);					// Force Proceed in PMS Mode
			}else{
				saveInvoiceListInfoIn.setSuspendOrder(true);
				saveInvoiceListInfoIn.setSuspendCode(CHARGING_SFI_SUSPEND_ORDER_SUSPENDCODE.get());
			}
		}else{
			saveInvoiceListInfoIn.setSuspendOrder(true);
			saveInvoiceListInfoIn.setSuspendCode(CHARGING_SFI_SUSPEND_ORDER_SUSPENDCODE.get());
		}
		return saveInvoiceListInfoIn;
	}
		
	private String generateInvoiceNum( Invoice inInvoice, Date dispDate){
		StringBuilder sb = new StringBuilder();
		
		String invoicePrefix = null; 
		Workstore workstore = inInvoice.getWorkstore();
		
		if( InvoiceDocType.Sfi.equals(inInvoice.getDocType()) ){
			invoicePrefix = CHARGING_SFI_INVOICE_INVOICENUM_PREFIX.get(workstore.getHospital());
		}else{
			invoicePrefix = CHARGING_STANDARD_INVOICE_INVOICENUM_PREFIX.get(workstore.getHospital());
		}
		sb.append( invoicePrefix );
		sb.append(workstore.getHospCode());		
		if ( workstore.getHospCode().length() == 2 ){
			sb.append(" ");
		}		
		sb.append(sfiInvoiceYearFormat.format(dispDate));		
		sb.append("1");
		
		Integer invoiceNum = null;
		if( InvoiceDocType.Sfi.equals(inInvoice.getDocType()) ){
			invoiceNum = numberGenerator.retrieveSfiInvoiceNumber();
		}else{
			invoiceNum = numberGenerator.retrieveStandardInvoiceNumber();
		}
		sb.append(sfiInvoiceNumFormat.format(invoiceNum));		
		
		return sb.toString();
	}
	
	public boolean isExceptionDrugExistExcludeZeroAmount(Object clearanceObject){
		if( clearanceObject instanceof Invoice ){
			Invoice invoice = (Invoice)clearanceObject;
			return isExceptionDrugExistChecking(invoice, true);
		}else if( clearanceObject instanceof PurchaseRequest ){
			PurchaseRequest purchaseRequest = (PurchaseRequest)clearanceObject;
			return isExceptionDrugExistChecking(purchaseRequest, true);
		}else{
			return true;
		}
	}
	
	public boolean isExceptionDrugExist(Object clearanceObject){
		if( clearanceObject instanceof Invoice ){
			Invoice invoice = (Invoice)clearanceObject;
			return isExceptionDrugExistChecking(invoice, false);
		}else if( clearanceObject instanceof PurchaseRequest ){
			PurchaseRequest purchaseRequest = (PurchaseRequest)clearanceObject;
			return isExceptionDrugExistChecking(purchaseRequest, false);
		}else{
			return true;
		}
	}
	
	private boolean isExceptionDrugExistChecking(PurchaseRequest purchaseRequest, boolean excludeZeroAmount){
		String pasPatGroupCode = null;
		if( purchaseRequest.getMedProfile().getMedCase() != null ){
			pasPatGroupCode = purchaseRequest.getMedProfile().getMedCase().getPasPatGroupCode();
		}
		
		if( !StringUtils.equals(CHARGING_SFI_WAIVE_MEDCASE_PASPATGROUPCODE.get(purchaseRequest.getMedProfile().getWorkstore().getHospital()), pasPatGroupCode) ){			
			return true;
		}
		
		boolean exceptionDrugExist = false;
		BigDecimal exceptionTotalAmount = BigDecimal.ZERO;
		
		Pair<Boolean, BigDecimal> itemExceptionInfo = itemExceptionCheck(purchaseRequest.getMedProfile().getWorkstore().getHospital(),
				purchaseRequest.getLifestyleFlag(), purchaseRequest.getOncologyFlag(), purchaseRequest.getMedProfilePoItem().getFmStatus(), purchaseRequest.getTotalAmount());
		
		if( itemExceptionInfo.getFirst() ){
			exceptionDrugExist = true;
			exceptionTotalAmount = exceptionTotalAmount.add(itemExceptionInfo.getSecond());
		}
		
		if( exceptionTotalAmount.compareTo(BigDecimal.ZERO) > 0 ){
			return true;
		}else if( !excludeZeroAmount && exceptionDrugExist ){
			return true;
		}else if( excludeZeroAmount && exceptionTotalAmount.compareTo(BigDecimal.ZERO) == 0 ){
			return false;
		}
		return exceptionDrugExist;
 	}
	
	private boolean isExceptionDrugExistChecking(Invoice invoice, boolean excludeZeroAmount){
		PharmOrder pharmOrder = invoice.getDispOrder().getPharmOrder();
		
		String pasPatGroupCode = null;
		if( pharmOrder.getMedCase() != null ){
			pasPatGroupCode = pharmOrder.getMedCase().getPasPatGroupCode();
		}
		
		if( !StringUtils.equals(CHARGING_SFI_WAIVE_MEDCASE_PASPATGROUPCODE.get(invoice.getWorkstore().getHospital()), pasPatGroupCode) ){			
			return true;
		}
		
		boolean exceptionDrugExist = false;
		
		logger.debug("InvoiceManagerBean isExceptionDrugExistChecking CHARGING_SFI_WAIVE_ITEMTYPE #0", CHARGING_SFI_WAIVE_ITEMTYPE.get());	
		
		BigDecimal exceptionTotalAmount = BigDecimal.ZERO;
		
		for( InvoiceItem invoiceItem : invoice.getInvoiceItemList() ){
		
			DispOrderItem dispOrderItem = invoiceItem.getDispOrderItem();
			
			Pair<Boolean, BigDecimal> itemExceptionInfo = itemExceptionCheck(invoice.getWorkstore().getHospital(),
					dispOrderItem.getLifestyleFlag(), dispOrderItem.getOncologyFlag(), dispOrderItem.getPharmOrderItem().getFmStatus(), invoiceItem.getAmount());
			
			if( itemExceptionInfo.getFirst() ){
				exceptionDrugExist = true;
				exceptionTotalAmount = exceptionTotalAmount.add(itemExceptionInfo.getSecond());
			}
			
			logger.debug("InvoiceManagerBean isExceptionDrugExistChecking #0", exceptionTotalAmount);
			if( exceptionTotalAmount.compareTo(BigDecimal.ZERO) > 0 ){
				return true;
			}else if( !excludeZeroAmount && exceptionDrugExist ){
				return true;
			}
		}

		if( excludeZeroAmount && exceptionTotalAmount.compareTo(BigDecimal.ZERO) == 0 ){
			return false;
		}
		return exceptionDrugExist;
	}
	
	@SuppressWarnings("unchecked")
	private Pair<Boolean, BigDecimal> itemExceptionCheck(Hospital hospital, Boolean lifeStyleFlag, Boolean oncologyFlag, String fmStatus, BigDecimal itemAmount){
		boolean exceptionDrugExist = false;
		BigDecimal exceptionTotalAmount = BigDecimal.ZERO;
		
		if( lifeStyleFlag && !CHARGING_SFI_WAIVE_ITEMTYPE.get(hospital).contains(PharmOrderItemType.Lifestyle.getDataValue()) ){
			exceptionTotalAmount = exceptionTotalAmount.add(itemAmount);
			exceptionDrugExist = true;
		}
		
		if( oncologyFlag && !CHARGING_SFI_WAIVE_ITEMTYPE.get(hospital).contains(PharmOrderItemType.Oncology.getDataValue()) ){
			exceptionTotalAmount = exceptionTotalAmount.add(itemAmount);
			exceptionDrugExist = true;
		}

		if( StringUtils.equals(fmStatus, "M") && !CHARGING_SFI_WAIVE_ITEMTYPE.get(hospital).contains(PharmOrderItemType.SafetyNet.getDataValue()) ){
			exceptionTotalAmount = exceptionTotalAmount.add(itemAmount);
			exceptionDrugExist = true;
		}
		
		if( !CHARGING_SFI_WAIVE_ITEMTYPE.get(hospital).contains(PharmOrderItemType.General.getDataValue()) ){
			exceptionTotalAmount = exceptionTotalAmount.add(itemAmount);
			exceptionDrugExist = true;
		}
		
		return new Pair(exceptionDrugExist, exceptionTotalAmount);
	}
	
	private Map<String, List<DispOrderItem>> updateSfiChangeFlagForMpDischarge(DispOrder dispOrder){
		Map<DispOrderItem, DispOrderItem> resultMap = new HashMap<DispOrderItem, DispOrderItem>();
        List<DispOrderItem> stdDispOrderItemList = new ArrayList<DispOrderItem>();
		
		//Create dispOrderItemList by same medOrderItem
		Map<MedOrderItem, List<DispOrderItem>> dispOrderItemMap = new HashMap<MedOrderItem, List<DispOrderItem>>();

		for ( DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList() ) {
			
			//no standard drug is applied to mpDischarge
			if ( dispOrderItem.getPharmOrderItem().isSfi() ) { 
				
				// Count As SFI
				if( dispOrderItem.getDispQty().compareTo(BigDecimal.ZERO) > 0  ){
					resultMap.put(dispOrderItem, dispOrderItem);
				}

				if ( !dispOrderItemMap.containsKey(dispOrderItem.getPharmOrderItem().getMedOrderItem()) ) {
					dispOrderItemMap.put(dispOrderItem.getPharmOrderItem().getMedOrderItem(), new ArrayList<DispOrderItem>());
				}
				dispOrderItemMap.get(dispOrderItem.getPharmOrderItem().getMedOrderItem()).add(dispOrderItem);
				
			} else if ( ActionStatus.DispByPharm.equals(dispOrderItem.getPharmOrderItem().getActionStatus()) || 
					dispOrderItem.getPharmOrderItem().getMedOrderItem().isCapdSfi() ){
				//Count As Std DrugCharge
				if( dispOrderItem.getDispQty().compareTo(BigDecimal.ZERO) > 0  ){
					stdDispOrderItemList.add(dispOrderItem);
				}
			}
		}
		
		List<DispOrderItem> sfiDispOrderItemList = new ArrayList<DispOrderItem>();
		for ( Map.Entry<MedOrderItem, List<DispOrderItem>> entry : dispOrderItemMap.entrySet() ) {
			MedOrderItem medOrderItem = entry.getKey();
			List<DispOrderItem> dispOrderItemList = entry.getValue();
			
			if ( medOrderItem.getRxItem().isIvRxDrug() ) {
				sfiDispOrderItemList.addAll(this.getSfiDispOrderItemForIvRxDrug(dispOrderItemList, medOrderItem));
			} else if ( medOrderItem.getRxItem().isInjectionRxDrug() ) {
				sfiDispOrderItemList.addAll(this.getSfiDispOrderItemForInjectionRxDrug(dispOrderItemList, medOrderItem));
			} else {
				for (DispOrderItem dispOrderItem : dispOrderItemList ) {
					if ( StringUtils.equals(dispOrderItem.getPharmOrderItem().getDupChkDisplayName(), dispOrderItem.getPharmOrderItem().getMedOrderItem().getDisplayName()) ) {
						sfiDispOrderItemList.add(dispOrderItem);
					}
				}
			}
		}
		
		for ( DispOrderItem dispOrderItem : sfiDispOrderItemList ) {
			if ( resultMap.containsKey(dispOrderItem) ) {
				resultMap.get(dispOrderItem).getPharmOrderItem().setSfiChargeFlag(true);
			}
		}
		
		Map<String, List<DispOrderItem>> result = new HashMap<String, List<DispOrderItem>>();
		result.put(STANDARD, stdDispOrderItemList);
		result.put(SFI, new ArrayList<DispOrderItem>(resultMap.values()));
		
		return result;
	}
	
	private List<DispOrderItem> getSfiDispOrderItemForIvRxDrug(List<DispOrderItem> dispOrderItemList, MedOrderItem medOrderItem){
		List<DispOrderItem> resultList = new ArrayList<DispOrderItem>();
 		
		List<DispOrderItem> diluentItemList = new ArrayList<DispOrderItem>();
		List<DispOrderItem> nonDiluentItemList = new ArrayList<DispOrderItem>();
		
		for ( DispOrderItem dispOrderItem : dispOrderItemList ) {
			DmDrug dmDrug = dmDrugCacher.getDmDrug(dispOrderItem.getPharmOrderItem().getItemCode());
			
			String targetDisplayName = dmDrug.getDmDrugProperty().getDisplayname();
			String targetDiluentCode = dmDrug.getDmMoeProperty().getDiluentCode();
			boolean isAdditiveItem = isAdditiveItem(targetDisplayName, medOrderItem.getIvRxDrug().getIvAdditiveList());
			boolean isFluidItem = this.isFluidItem(targetDiluentCode, medOrderItem.getIvRxDrug().getIvFluidList());

			if ( isAdditiveItem ) {
				nonDiluentItemList.add(dispOrderItem);
			} else if ( isFluidItem ) {
				diluentItemList.add(dispOrderItem);
			}
			// solvent item will not be charge
		}
		
		if ( !nonDiluentItemList.isEmpty() ) {
			resultList.addAll(nonDiluentItemList);
		} else if ( !diluentItemList.isEmpty() ) {
			resultList.addAll(diluentItemList);
		}
		
		return resultList;
	}
	
	private List<DispOrderItem> getSfiDispOrderItemForInjectionRxDrug(List<DispOrderItem> dispOrderItemList, MedOrderItem medOrderItem){
		List<DispOrderItem> resultList = new ArrayList<DispOrderItem>();
 		
		List<DispOrderItem> diluentItemList = new ArrayList<DispOrderItem>();
		List<DispOrderItem> nonDiluentItemList = new ArrayList<DispOrderItem>();
		
		InjectionRxDrug injectionRxDrug = (InjectionRxDrug) medOrderItem.getRxItem();
		for ( DispOrderItem dispOrderItem : dispOrderItemList ) {
			
			DmDrug dmDrug = dmDrugCacher.getDmDrug(dispOrderItem.getPharmOrderItem().getItemCode());
			
			String targetDisplayName = dmDrug.getDmDrugProperty().getDisplayname();
			String targetDiluentCode = dmDrug.getDmMoeProperty().getDiluentCode();
			
			if ( targetDisplayName.equals(injectionRxDrug.getDisplayName()) ) {
				
				nonDiluentItemList.add(dispOrderItem);
			} else if ( StringUtils.isNotBlank(targetDiluentCode) ) {
				for (DoseGroup doseGroup : injectionRxDrug.getRegimen().getDoseGroupList() ) {
					for ( Dose dose : doseGroup.getDoseList() ) {
						DoseFluid doseFluid = dose.getDoseFluid();
						if ( doseFluid != null && 
								StringUtils.isNotBlank(doseFluid.getDiluentCode()) && 
								StringUtils.equals(targetDiluentCode, doseFluid.getDiluentCode()) ) 
						{
							diluentItemList.add(dispOrderItem);
						} 
						// solvent item will not be charge
					}
				}
			}
		}
		
		if ( !nonDiluentItemList.isEmpty() ) {
			resultList.addAll(nonDiluentItemList);
		} else if ( !diluentItemList.isEmpty() ) {
			resultList.addAll(diluentItemList);
		}
		
		return resultList;
	}
	
	private boolean isFluidItem(String targetDiluentCode, List<IvFluid> ivFluidList) {
		if ( ivFluidList == null ) {
			return false;
		}
		if ( StringUtils.isBlank(targetDiluentCode) ) {
			return false;
		}
		
		for ( IvFluid ivFluid : ivFluidList ) {
			if ( StringUtils.isBlank(ivFluid.getDiluentCode()) ) {
				continue;
			}
			if ( StringUtils.equals(targetDiluentCode, ivFluid.getDiluentCode()) ) {
				return true;
			}
		}
		return false;
	}
	
	private boolean isAdditiveItem(String targetDisplayName, List<IvAdditive> ivAdditiveList) {
		if ( ivAdditiveList == null ) {
			return false;
		}
		for ( IvAdditive ivAdditive : ivAdditiveList ) {
			if ( StringUtils.equals(targetDisplayName, ivAdditive.getDisplayName()) ) {
				return true;
			}
		}
		return false;
	}
	
	private boolean isNotMpOrder(MedOrderPrescType prescType) {
		return (prescType != MedOrderPrescType.MpDischarge && prescType != MedOrderPrescType.MpHomeLeave && prescType != MedOrderPrescType.In);
	}
	
	public void updateInvoicePrintFlag(Invoice invoice){
		//IP tempInvoice
		if( invoice.getId() == null ){
			return;
		}
		
		Invoice printedInvoice = em.find(Invoice.class, invoice.getId());		
		if( printedInvoice != null ){
			printedInvoice.setPrintFlag(true);
		}
	}
}
