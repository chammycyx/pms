package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.reftable.PmsSessionInfo;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import java.util.List;
import java.util.UUID;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("pmsSessionInfoListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PmsSessionInfoListServiceBean implements PmsSessionInfoListServiceLocal {
	
	@In
	private Workstore workstore;
	
	@In 
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;
	
	@Out(required = false)
	private List<PmsSessionInfo> pmsSessionInfoList;
	
	public void retrievePmsSessionInfoList()
	{
		pmsSessionInfoList = corpPmsServiceProxy.retrievePmsSessionInfoList(UUID.randomUUID().toString(), workstore);
	}

	@Remove
	public void destroy() 
	{

		if (pmsSessionInfoList != null) {
			pmsSessionInfoList = null;
		}
	}
}
