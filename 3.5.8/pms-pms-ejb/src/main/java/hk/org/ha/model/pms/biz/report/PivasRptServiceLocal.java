package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.udt.YesNoBlankFlag;
import hk.org.ha.model.pms.udt.report.PivasReportType;
import hk.org.ha.model.pms.udt.report.PivasRptDispMethod;
import hk.org.ha.model.pms.udt.report.PivasRptItemCatFilterOption;

import java.io.IOException;
import java.util.Date;

import javax.ejb.Local;

import net.lingala.zip4j.exception.ZipException;

@Local
public interface PivasRptServiceLocal {
	
	void retrievePivasRpt(PivasReportType reportType, Date supplyFromDate,  Date supplyToDate, YesNoBlankFlag satelliteFlag, 
			String hkidCaseNum, PivasRptDispMethod pivasRptDispMethod, PivasRptItemCatFilterOption pivasRptItemCatFilterOption, 
			String itemCode, Boolean drugKeyFlag, String manufCode, String batchNum);

	void generatePivasRpt();
	
	void printPivasRpt();
	
	void exportPivasRpt() throws IOException, ZipException;
	
	void setPivasRptPassword(String password);

	void destroy();
	
}
