package hk.org.ha.model.pms.biz.vetting;

import hk.org.ha.model.pms.persistence.disp.CapdVoucher;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;

import java.util.List;

import javax.ejb.Local;

@Local
public interface PrintJobManagerLocal {

	void renderAndPrint(DispOrder dispOrder, List<CapdVoucher> voucherList, OrderViewInfo orderViewInfo);
	
	void destroy();
}
