package hk.org.ha.model.pms.persistence.corp;

import hk.org.ha.model.pms.persistence.BaseEntity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "HOSPITAL_CLUSTER_TASK")
@IdClass(HospitalClusterTaskPK.class)
public class HospitalClusterTask extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "CLUSTER_CODE", length = 3)
	private String clusterCode;
	
	@Id
	@Column(name = "TASK_NAME", length = 100)
	private String taskName;
	
	@Column(name = "TASK_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date taskDate;
	
	@ManyToOne
	@JoinColumn(name = "CLUSTER_CODE", nullable = false, insertable = false, updatable = false)
	private HospitalCluster hospitalCluster;
	
	public String getClusterCode() {
		return clusterCode;
	}

	public void setClusterCode(String clusterCode) {
		this.clusterCode = clusterCode;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public Date getTaskDate() {
		if (taskDate == null) {
			return null;
		}
		else {
			return new Date(taskDate.getTime());
		}
	}
	
	public void setTaskDate(Date taskDate) {
		if (taskDate == null) {
			this.taskDate = null;
		}
		else {
			this.taskDate = new Date(taskDate.getTime());
		}
	}
	
	public HospitalCluster getHospitalCluster() {
		return hospitalCluster;
	}

	public void setHospitalCluster(HospitalCluster hospitalCluster) {
		this.hospitalCluster = hospitalCluster;
	}
	
	public HospitalClusterTaskPK getId() {
		return new HospitalClusterTaskPK(clusterCode, taskName);
	}
}
