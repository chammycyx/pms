package hk.org.ha.model.pms.biz.onestop;

import hk.org.ha.model.pms.udt.onestop.MoeFilterOption;
import hk.org.ha.model.pms.udt.onestop.MoeSortOption;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
import hk.org.ha.model.pms.vo.onestop.OneStopOrderInfo;
import hk.org.ha.model.pms.vo.onestop.OneStopOrderItemSummary;
import hk.org.ha.model.pms.vo.onestop.OneStopSummary;
import hk.org.ha.model.pms.vo.onestop.PrescriptionSortCriteria;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface OneStopServiceLocal {
		
	void clearOrder();
	
	void retrieveUnvetAndSuspendCount();
	
	void retrieveOrder(String searchText, Boolean displayAllMoe);
	
	List<OneStopSummary> getRefNumOrderList();
	
	List<OneStopOrderItemSummary> getCaseNumUnvetList();
	
	List<OneStopOrderItemSummary> getCaseNumVettedList();
	
	List<OneStopOrderItemSummary> getPrescriptionOrderList();
	
	String getVettingMsg();
	
	void retrievePrescriptionListFromLastThreeDays(OneStopOrderType oneStopOrderType, Long medOrderId, Long medOrderVersion, 
												   Long dispOrderId, Long refillId, String hkid);
	
	void refreshPharmacyOrderStatus(Long id);
	
	void refreshOrderStatus(OneStopOrderType oneStopOrderType, Long id, Long refillOrderId);
	
	void retrieveOrderByHkid(String hkid, Boolean displayAllMoe, Boolean callPasAppointment);
	
	void retrieveOrderByCaseNum(String searchText, Boolean displayAllMoe);
	
	void retrieveHkPmiPatientByOrderNumOrRefNumOrTickNum();
		
	void retrieveHkPmiPatientAndAppointmentByCaseNum(String caseNum, List<OneStopOrderItemSummary> caseNumUnvetList, 
		   	 										 List<OneStopOrderItemSummary> caseNumVettedList);
	
	void setCaseNumUnvetList(List<OneStopOrderItemSummary> caseNumUnvetList);
	
	void setCaseNumVettedList(List<OneStopOrderItemSummary> caseNumVettedList);
	
	void retrieveSuspendOrder(Date suspendStartDate);
	
	void retrieveMoeOrder(Date orderDate, MoeFilterOption moeFilterOption, List<PrescriptionSortCriteria> prescriptionSortCriteria);
	
	void retrieveHkPmiPatientForVettedOrder();
	
	String updatePreVetValue(Long medOrderId, OneStopSummary preVetOneStopSummary, OneStopOrderInfo preVetOneStopOrderInfo);
	
	void retrievePagedPatient(List<Long> oneStopOrderItemSummaryIdList, String caller);
	
	void filterMoeList(MoeFilterOption moeFilterOption, List<PrescriptionSortCriteria> prescriptionSortCriteriaList);
	
	void sortMoeList(PrescriptionSortCriteria prescriptionSortCriteria);
	
	void writeHkidUnknownDigitLog(String hkid);
	
	void destroy();
	
}
