package hk.org.ha.model.pms.udt.medprofile;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;
import hk.org.ha.model.pms.udt.disp.MpTrxType;

import java.util.Arrays;
import java.util.List;

public enum MedProfileOrderType implements StringValuedEnum {
	
	Create("C", "Create", MpTrxType.MpCreateOrderFirstItem),
	Add("A", "Add", MpTrxType.MpCreateOrderOtherItem),
	Modify("M", "Modify", MpTrxType.MpModifyOrder),
	Delete("D", "Delete", MpTrxType.MpDeleteOrder),
	Discontinue("T", "Discontinue", MpTrxType.MpDiscontinueOrder),
	CheckPregnancy("P", "CheckPregnancy", MpTrxType.MpUpdatePregnancy),
	Urgent("U", "Urgent", MpTrxType.MpUrgentDispensing),
	Replenishment("R", "Replenishment", MpTrxType.MpDrugReplenishment),
	OverridePending("O","OverridePending", MpTrxType.MpOverrideOnHoldOrder),
	AddAdmin("AA", "AddAdmin", MpTrxType.MpAddAdminRecord),
	ModifyAdmin("MA", "ModifyAdmin", MpTrxType.MpModifyAdminRecord),
	End("E", "End", MpTrxType.MpModifyOrder),
	CompleteAdmin("PA","CompleteAdmin", MpTrxType.MpCompleteAdmin),
	AmendSchedule("AS","AmendSchedule", MpTrxType.MpAmendSchedule),
	CustomSchedule("CS","CustomSchedule", MpTrxType.MpCustomSchedule),
	MarkAnnotation("AN","MarkAnnotation", MpTrxType.MpMarkAnnotation),
	ModifyMds("MM", "ModifyMds", MpTrxType.MpModifyMds),
	ModifyReviewDate("MR", "ModifyReviewDate", MpTrxType.MpModifyReviewDate),
	AddWithhold("W1", "AddWithhold", MpTrxType.MpAddWithholdRecord),
	ModifyWithhold("W2", "ModifyWithhold", MpTrxType.MpModifyWithholdRecord),
	EndWithhold("W3", "EndWithhold", MpTrxType.MpEndWithholdRecord),
	Discharge("PD", "Discharge", null),
	CancelDischarge("CD", "CancelDischarge", null),
	WardTransfer("WT", "WardTransfer", null),
	CancelTransfer("CT", "CancelTransfer", null),
	CancelAdmission("CA", "Cancel Admission", null),
	PatientRegistrationMp("PR", "Patient Registration (IP)", null);
	
    private final String dataValue;
    private final String displayValue;
    private final MpTrxType mpTrxType;
    
    public static final List<MedProfileOrderType> Create_Add = Arrays.asList(
    		Create, Add);
    
    public static final List<MedProfileOrderType> Create_Add_Modify = Arrays.asList(
    		Create, Add, Modify);
    
    public static final List<MedProfileOrderType> Create_Add_Modify_Replenishment = Arrays.asList(
    		Create, Add, Modify, Replenishment);
    
    public static final List<MedProfileOrderType> Modify_End_Delete_Discontinue_CompleteAdmin = Arrays.asList(
    		Modify, End, Delete, Discontinue, CompleteAdmin);
    
    public static final List<MedProfileOrderType> Create_Add_CheckPregnancy = Arrays.asList(
    		Create, Add, CheckPregnancy);
    
    public static final List<MedProfileOrderType> Urgent_ModifyMds_ModifyReviewDate = Arrays.asList(
    		Urgent, ModifyMds, ModifyReviewDate);
    
    public static final List<MedProfileOrderType> WardTransfer_CancelTransfer = Arrays.asList(
    		WardTransfer, CancelTransfer);
    
    public static final List<MedProfileOrderType> AddAdmin_ModifyAdmin = Arrays.asList(
    		AddAdmin, ModifyAdmin);
    
	private MedProfileOrderType(String dataValue, String displayValue, MpTrxType mpTrxType) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
		this.mpTrxType = mpTrxType;
	}

	@Override
	public String getDataValue() {
		return dataValue;
	}
	
	@Override
	public String getDisplayValue() {
		return displayValue;
	}

	public MpTrxType getMpTrxType() {
		return mpTrxType;
	}
	
    public static MedProfileOrderType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MedProfileOrderType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<MedProfileOrderType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<MedProfileOrderType> getEnumClass() {
    		return MedProfileOrderType.class;
    	}
    }
}
