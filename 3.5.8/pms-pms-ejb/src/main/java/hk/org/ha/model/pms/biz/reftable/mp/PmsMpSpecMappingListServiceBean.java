package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.util.QueryUtils;
import hk.org.ha.fmk.pms.util.StringArray;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.inbox.PharmInboxClientNotifierInf;
import hk.org.ha.model.pms.biz.medprofile.MedProfileManagerLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileStatManagerLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.biz.reftable.SpecialtyManagerLocal;
import hk.org.ha.model.pms.biz.reftable.WardManagerLocal;
import hk.org.ha.model.pms.cache.PasSpecialtyCacherInf;
import hk.org.ha.model.pms.corp.cache.PasWardCacherInf;
import hk.org.ha.model.pms.dms.persistence.PmsIpSpecMapExcl;
import hk.org.ha.model.pms.dms.persistence.PmsIpSpecMapping;
import hk.org.ha.model.pms.dms.vo.PmsSpecialtyMap;
import hk.org.ha.model.pms.persistence.corp.HospitalMapping;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.phs.Specialty;
import hk.org.ha.model.pms.persistence.phs.Ward;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatus;
import hk.org.ha.model.pms.udt.patient.PasSpecialtyType;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.patient.PasSpecialty;
import hk.org.ha.model.pms.vo.patient.PasWard;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.reftable.mp.PmsMpSpecMappingHdr;
import hk.org.ha.model.pms.vo.reftable.mp.PmsMpSpecMappingInfo;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Map.Entry;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.springframework.beans.BeanUtils;

@Stateful
@Scope(ScopeType.SESSION)
@Name("pmsMpSpecMappingListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PmsMpSpecMappingListServiceBean implements PmsMpSpecMappingListServiceLocal {

	private static final String MED_PROFILE_QUERY_HINT_BATCH_1 = "o.medCase";
	private static final String MED_PROFILE_QUERY_HINT_BATCH_2 = "o.medProfileItemList.medProfileMoItem.medProfilePoItemList";
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private Workstore workstore;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent;
	
	@In
	private PharmInboxClientNotifierInf pharmInboxClientNotifier;	

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private PasSpecialtyCacherInf pasSpecialtyCacher;
	
	@In
	private PasWardCacherInf pasWardCacher;
	
	@In
	private SpecialtyManagerLocal specialtyManager;
	
	@In
	private WardManagerLocal wardManager;
	
	@In
	private MedProfileStatManagerLocal medProfileStatManager;
	
	@In
	private MedProfileManagerLocal medProfileManager;

	private List<Workstore> pharmacyWorkstoreList;
	
	private List<Specialty> phsSpecialtyList;
	
	private List<Ward> wardList;
	
	private List<HospitalMapping> pmsSpecMappingHospitalMappingList;
	
	private List<PmsMpSpecMappingHdr> pmsMpSpecMappingHdrList;
	
	private List<String> patHospList;
	private Map<String, List<PasSpecialty>> pmsSpecMappingPasSpecialtyMap;
	private List<PasWard> pasWardList;
	
	private List<PmsIpSpecMapping> mpSpecMapRptList;
	
	public PmsMpSpecMappingInfo retrievePmsIpSpecMappingList() {
		PmsMpSpecMappingInfo pmsMpSpecMappingInfo = new PmsMpSpecMappingInfo();
		
		initRefList();
		pmsMpSpecMappingInfo.setPharmacyWorkstoreList(pharmacyWorkstoreList);
		pmsMpSpecMappingInfo.setPhsSpecialtyList(phsSpecialtyList);
		pmsMpSpecMappingInfo.setWardList(wardList);
		pmsMpSpecMappingInfo.setPmsSpecMappingHospitalMappingList(pmsSpecMappingHospitalMappingList);
		pmsMpSpecMappingInfo.setPatHospList(patHospList);
		pmsMpSpecMappingInfo.setPasWardList(pasWardList);
		
		List<PmsIpSpecMapping> list = initPmsMpSpacMappingInfoList(dmsPmsServiceProxy.retrievePmsIpSpecMappingList( patHospList ), dmsPmsServiceProxy.retrievePmsIpSpecMapExclList( patHospList ));
		pmsMpSpecMappingInfo.setPmsMpSpecMappingList(list);
		pmsMpSpecMappingInfo.setPmsMpSpecMappingHdrList(pmsMpSpecMappingHdrList);
		return pmsMpSpecMappingInfo;
	}
	
	private List<PmsIpSpecMapping> initPmsMpSpacMappingInfoList(List<PmsIpSpecMapping> pmsIpSpecMappingList, List<PmsIpSpecMapExcl> pmsIpSpecMapExclList) {
		List<PmsIpSpecMapping> resultList = new ArrayList<PmsIpSpecMapping>();
		
		pmsMpSpecMappingHdrList = new ArrayList<PmsMpSpecMappingHdr>();
		Map<String, List<PmsIpSpecMapping>> pmsMpSpecMappingMap = new HashMap<String, List<PmsIpSpecMapping>>();
		
		for ( PmsIpSpecMapping pmsIpSpecMapping : pmsIpSpecMappingList ) {
			String mapKey = pmsIpSpecMapping.getPatHospCode()+pmsIpSpecMapping.getPasSpecialty()+pmsIpSpecMapping.getWard();
			if ( pmsMpSpecMappingMap.get(mapKey) == null ) {
				pmsMpSpecMappingMap.put(mapKey, new ArrayList<PmsIpSpecMapping>());
			}
			pmsMpSpecMappingMap.get(mapKey).add(pmsIpSpecMapping);
		}
		
		Map<StringArray, PmsIpSpecMapExcl> pmsIpSpecMapExclMap = new HashMap<StringArray, PmsIpSpecMapExcl>();
		for (PmsIpSpecMapExcl pmsIpSpecMapExcl : pmsIpSpecMapExclList) {
			pmsIpSpecMapExclMap.put(new StringArray(pmsIpSpecMapExcl.getPatHospCode(), pmsIpSpecMapExcl.getPasSpecialty(), pmsIpSpecMapExcl.getWard()), pmsIpSpecMapExcl);
		}
		
		for ( String patHospCode : patHospList ) {
			List<PasSpecialty> pmsSpecMappingPasSpecialtyList = pmsSpecMappingPasSpecialtyMap.get(patHospCode);
			if ( pmsSpecMappingPasSpecialtyList == null ) {
				continue;
			}
			for ( PasSpecialty pasSpecialty : pmsSpecMappingPasSpecialtyList ) {
				if ( pasSpecialty.getType() == PasSpecialtyType.InPatient ) {
					for ( PasWard pasWard : pasWardCacher.getPasWardList(patHospCode) ) {
						
						if (!pasWardList.contains(pasWard)) {
							pasWardList.add(pasWard);
						}
						
						PmsMpSpecMappingHdr pmsMpSpecMappingHdr = new PmsMpSpecMappingHdr();
						pmsMpSpecMappingHdr.setPatHospCode(patHospCode);
						pmsMpSpecMappingHdr.setPasSpecialty(pasSpecialty.getPasSpecCode());
						pmsMpSpecMappingHdr.setWard(pasWard.getPasWardCode());
	
						String mapKey = patHospCode+pasSpecialty.getPasSpecCode()+pasWard.getPasWardCode();
						if ( pmsMpSpecMappingMap.get(mapKey) != null ) {
							pmsMpSpecMappingHdr.setPmsIpSpecMappingList(pmsMpSpecMappingMap.get(mapKey));
							resultList.addAll(pmsMpSpecMappingMap.get(mapKey));
						} else {
							pmsMpSpecMappingHdr.setPmsIpSpecMappingList(new ArrayList<PmsIpSpecMapping>());
						}
						
						pmsMpSpecMappingHdr.setMapped(pmsMpSpecMappingHdr.getPmsIpSpecMappingList().size());
						
						pmsMpSpecMappingHdr.setExcludeFlag(pmsIpSpecMapExclMap.containsKey(new StringArray(pmsMpSpecMappingHdr.getPatHospCode(), pmsMpSpecMappingHdr.getPasSpecialty(), pmsMpSpecMappingHdr.getWard())));
						
						pmsMpSpecMappingHdrList.add(pmsMpSpecMappingHdr);
					}
				}
			}
		}
		return resultList;
	}
	
	private void initRefList() {
		workstore = em.find(Workstore.class, workstore.getId());
		
		
		if ( pmsSpecMappingHospitalMappingList == null ) {
			pmsSpecMappingHospitalMappingList = retrievePmsSpecMappingHospitalMappingList();
		}
		
		if ( pmsSpecMappingPasSpecialtyMap == null ) {
			pmsSpecMappingPasSpecialtyMap = new HashMap<String, List<PasSpecialty>>();
			for ( HospitalMapping hospitalMapping : pmsSpecMappingHospitalMappingList ) {
				pmsSpecMappingPasSpecialtyMap.put(hospitalMapping.getPatHospCode(), pasSpecialtyCacher.getPasSpecialtyListByPatHospCode(hospitalMapping.getPatHospCode()));
			}
		}
		
		if ( pasWardList == null ) {
			pasWardList = new ArrayList<PasWard>();
		}
		
		if ( patHospList == null ) {
			patHospList = createPatHospList( pmsSpecMappingHospitalMappingList );
		}
		
		if ( pharmacyWorkstoreList == null ) {
			pharmacyWorkstoreList = retrievePharmacyWorkstoreList( patHospList );
		}

		Set<String> instCodeSet = new HashSet<String>();
		for (Workstore workstore : pharmacyWorkstoreList) {
			instCodeSet.add(workstore.getWorkstoreGroup().getInstitution().getInstCode());
		}
		
		if ( wardList == null  ) {
			if ( instCodeSet.isEmpty() ) {
				wardList = new ArrayList<Ward>();
			} else {
				wardList = wardManager.retrieveWardListByInstCode(new ArrayList<String>(instCodeSet));
			}
		}
		
		if ( phsSpecialtyList == null ) {
			if ( instCodeSet.isEmpty() ) {
				phsSpecialtyList = new ArrayList<Specialty>();
			} else {
				phsSpecialtyList = specialtyManager.retrieveSpecialtyListByInstCode(new ArrayList<String>(instCodeSet));
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<Workstore> retrievePharmacyWorkstoreList(List<String> patHospCodeList) {
		
		if ( patHospCodeList.isEmpty() ) {
			return new ArrayList<Workstore>();
		}
		
		List<String> hospCodeList = em.createQuery(
				"select o.hospCode from HospitalMapping o" + // 20120831 index check : none
				" where o.patHospCode in :patHospCodeList" +
				" and o.prescribeFlag = :prescribeFlag")
				.setParameter("patHospCodeList", patHospCodeList)
				.setParameter("prescribeFlag", Boolean.TRUE)
				.getResultList();
		
		List<Workstore> workstoreList = em.createQuery(
											"select o from Workstore o" + // 20120831 index check : Workstore.hospital : FK_WORKSTORE_01
											" where o.hospCode in :hospCodeList" +
											" order by o.hospCode, o.workstoreCode")
											.setParameter("hospCodeList", hospCodeList)
											.getResultList();
		
		for ( Workstore workstore : workstoreList ) {
			workstore.getWorkstoreGroup().getInstitution();
		}
		return workstoreList;
	}
	
	private List<HospitalMapping> retrievePmsSpecMappingHospitalMappingList() 
	{
		List<HospitalMapping> hospMappingList = workstore.getHospital().getHospitalMappingList();
		
		if ( !hospMappingList.isEmpty() ) {
			for (Iterator<HospitalMapping> itr = hospMappingList.iterator();itr.hasNext();) {
				HospitalMapping hospitalMapping = itr.next();
				if ( !hospitalMapping.getPrescribeFlag() ) {
					itr.remove();
				}
			}	
		}
		return hospMappingList;
	}
	
	private List<String> createPatHospList(List<HospitalMapping> hospitalMappingList)  
	{
		List<String> tmpPatHospList = new ArrayList<String>();
		if ( !pmsSpecMappingHospitalMappingList.isEmpty() ) {
			for ( HospitalMapping hospitalMapping : hospitalMappingList ) {
				tmpPatHospList.add(hospitalMapping.getPatHospCode());
			}
		}
		return tmpPatHospList;
	}
	
	public PmsMpSpecMappingInfo updatePmsIpSpecMappingList(List<PmsIpSpecMapping> inPmsIpSpecMappingList) {
		dmsPmsServiceProxy.updatePmsIpSpecMappingList(inPmsIpSpecMappingList);
		remapSpecialty(inPmsIpSpecMappingList);
		medProfileStatManager.clearMissingSpecialtyMappingException(inPmsIpSpecMappingList);
		return retrievePmsIpSpecMappingList();
	}
	
	private void remapSpecialty(List<PmsIpSpecMapping> pmsIpSpecMappingList) {
		
		Map<String, Pair<PmsIpSpecMapping, PmsSpecialtyMap>> pmsSpecMappingMap = buildPmsSpecMappingMap(pmsIpSpecMappingList);

		Set<String> updatedHospCodeSet = new HashSet<String>();
		List<MedProfile> medProfileList = findLockedMedProfileList(pmsIpSpecMappingList);
		
		for (MedProfile medProfile: medProfileList) {
			Pair<PmsIpSpecMapping, PmsSpecialtyMap> pmsSpecMapping = pmsSpecMappingMap.get(generatePasKey(medProfile));
			if (pmsSpecMapping == null) {
				continue;
			}
			medProfileManager.mapSpecialty(medProfile, pmsSpecMapping.getLeft().isMarkDelete() ?
					null : pmsSpecMapping.getRight());
			updatedHospCodeSet.add(medProfile.getWorkstore().getHospCode());
		}
		pharmInboxClientNotifier.dispatchUpdateEvent(updatedHospCodeSet);
	}
	
	private Map<String, Pair<PmsIpSpecMapping, PmsSpecialtyMap>> buildPmsSpecMappingMap(List<PmsIpSpecMapping> pmsIpSpecMappingList) {

		Map<String, Pair<PmsIpSpecMapping, PmsSpecialtyMap>> resultMap = new TreeMap<String, Pair<PmsIpSpecMapping, PmsSpecialtyMap>>();
		
		for (PmsIpSpecMapping pmsIpSpecMapping: pmsIpSpecMappingList) {
			
			String key = generatePasKey(pmsIpSpecMapping);
			if (!pmsIpSpecMapping.isMarkDelete() || !resultMap.containsKey(key)) {
				resultMap.put(key, Pair.of(pmsIpSpecMapping, convertToPmsSpecialtyMap(pmsIpSpecMapping)));
			}
		}
		return resultMap;
	}
	
	@SuppressWarnings("unchecked")
	private List<MedProfile> findLockedMedProfileList(List<PmsIpSpecMapping> pmsIpSpecMappingList) {
		
		Map<Pair<String, String>, Set<String>> pasSpecialtyMap = MedProfileUtils.buildPasSpecialtyMap(pmsIpSpecMappingList, true); 

		Set<Long> medProfileIdSet = new TreeSet<Long>(); 
		
		for (Entry<Pair<String, String>, Set<String>> entry: pasSpecialtyMap.entrySet()) {
			medProfileIdSet.addAll(QueryUtils.splitExecute(em.createQuery(
					"select o.id from MedProfile o" + // 20160809 index check : MedProfile.patHospCode,status : I_MED_PROFILE_07
					" where o.patHospCode = :patHospCode" +
					" and o.medCase.pasWardCode = :pasWardCode" +
					" and o.medCase.pasSpecCode in :pasSpecCodeSet" +
					" and o.status in :notInactiveStatus" +
					" and o.processingFlag = false" + 
					" and o.deliveryGenFlag = false")
					.setParameter("patHospCode", entry.getKey().getLeft())
					.setParameter("pasWardCode", entry.getKey().getRight())
					.setParameter("notInactiveStatus", MedProfileStatus.Not_Inactive),
					"pasSpecCodeSet", entry.getValue()));
		}
		
		return QueryUtils.splitExecute(em.createQuery(
				"select o from MedProfile o" + // 20150206 index check : MedProfile.id : PK_MED_PROFILE
				" where o.id in :medProfileIdSet" +
				" and o.processingFlag = false" +
				" and o.deliveryGenFlag = false")
				.setParameter("medProfileIdSet", medProfileIdSet)
				.setHint(QueryHints.BATCH, MED_PROFILE_QUERY_HINT_BATCH_1)
				.setHint(QueryHints.BATCH, MED_PROFILE_QUERY_HINT_BATCH_2)
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock),
				"medProfileIdSet", medProfileIdSet);
	}
	
	private String generatePasKey(PmsIpSpecMapping pmsIpSpecMapping) {
		return generatePasKey(pmsIpSpecMapping.getPatHospCode(), pmsIpSpecMapping.getWard(),
				pmsIpSpecMapping.getPasSpecialty(), pmsIpSpecMapping.getDispHospCode(), pmsIpSpecMapping.getDispWorkstore());
	}
	
	private String generatePasKey(MedProfile medProfile) {
		return generatePasKey(medProfile.getPatHospCode(), medProfile.getMedCase().getPasWardCode(),
				medProfile.getMedCase().getPasSpecCode(), medProfile.getWorkstore().getHospCode(),
				medProfile.getWorkstore().getWorkstoreCode());
	}
	
	private String generatePasKey(String patHospCode, String pasWardCode, String pasSpecCode, String hospCode, String wardCode) {
		return StringUtils.join(new String[]{patHospCode, pasWardCode, pasSpecCode, hospCode, wardCode} , "|");
	}
	
	private PmsSpecialtyMap convertToPmsSpecialtyMap(PmsIpSpecMapping pmsIpSpecMapping) {
		PmsSpecialtyMap pmsSpecialtyMap = new PmsSpecialtyMap();
		BeanUtils.copyProperties(pmsIpSpecMapping, pmsSpecialtyMap);
		return pmsSpecialtyMap;
	}
	
	public void clearList() {
		pmsSpecMappingHospitalMappingList = null;
		pmsSpecMappingPasSpecialtyMap = null;
		pasWardList = null;
		phsSpecialtyList = null;
		wardList = null;
		patHospList = null;
		pharmacyWorkstoreList = null;
		pmsMpSpecMappingHdrList = null;
	}
	
	public void printMpSpecMapRpt(List<PmsIpSpecMapping> inPmsIpSpecMappingList)
	{
		sendMpSpecMapRptList(inPmsIpSpecMappingList);
		Map<String, Object> parameters = new HashMap<String, Object>();
		printAgent.renderAndPrint(new RenderAndPrintJob(
				"MpSpecMapRpt", 
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				parameters, 
				mpSpecMapRptList));
		
	}
	
	public void exportMpSpecMapRpt(){
		Map<String, Object> parameters = new HashMap<String, Object>();
		PrintOption po = new PrintOption();
		po.setDocType(ReportProvider.XLS);
		printAgent.renderAndRedirect(new RenderJob(
				"MpSpecMapXls", 
				po, 
				parameters, 
				mpSpecMapRptList), 
				"IpmoeSpecMapXls");
	}
	
	public void sendMpSpecMapRptList(List<PmsIpSpecMapping> inPmsIpSpecMappingList) {
		mpSpecMapRptList = new ArrayList<PmsIpSpecMapping>();
		mpSpecMapRptList.addAll(inPmsIpSpecMappingList);
		Collections.sort(mpSpecMapRptList, new MpSpecMapRptComparator());
	}
	
	private static class MpSpecMapRptComparator implements Comparator<PmsIpSpecMapping>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(PmsIpSpecMapping pmsIpSpecMapping1, PmsIpSpecMapping pmsIpSpecMapping2) {
			int compareResult = 0;

			String patHospCode1 = StringUtils.trimToEmpty(pmsIpSpecMapping1.getPatHospCode());
			String patHospCode2 = StringUtils.trimToEmpty(pmsIpSpecMapping2.getPatHospCode());
			compareResult = patHospCode1.compareTo(patHospCode2);

			if (compareResult == 0) {
				String pasSpecialty1 = StringUtils.trimToEmpty(pmsIpSpecMapping1.getPasSpecialty());
				String pasSpecialty2 = StringUtils.trimToEmpty(pmsIpSpecMapping2.getPasSpecialty());
				compareResult = pasSpecialty1.compareTo(pasSpecialty2);
			}

			if (compareResult == 0) {
				String ward1 = StringUtils.trimToEmpty(pmsIpSpecMapping1.getWard());
				String ward2 = StringUtils.trimToEmpty(pmsIpSpecMapping2.getWard());
				compareResult = ward1.compareTo(ward2);
			}

			if (compareResult == 0) {
				String dispWorkstore1 = StringUtils.trimToEmpty(pmsIpSpecMapping1.getDispHospCode());
				String dispWorkstore2 = StringUtils.trimToEmpty(pmsIpSpecMapping2.getDispHospCode());
				compareResult = dispWorkstore1.compareTo(dispWorkstore2);
			}

			if (compareResult == 0) {
				String dispWorkstore1 = StringUtils.trimToEmpty(pmsIpSpecMapping1.getDispWorkstore());
				String dispWorkstore2 = StringUtils.trimToEmpty(pmsIpSpecMapping2.getDispWorkstore());
				compareResult = dispWorkstore1.compareTo(dispWorkstore2);
			}

			if (compareResult == 0) {
				String dispWorkstore1 = StringUtils.trimToEmpty(pmsIpSpecMapping1.getPasSpecialty());
				String dispWorkstore2 = StringUtils.trimToEmpty(pmsIpSpecMapping2.getPasSpecialty());
				compareResult = dispWorkstore1.compareTo(dispWorkstore2);
			}

			if (compareResult == 0) {
				String dispWorkstore1 = StringUtils.trimToEmpty(pmsIpSpecMapping1.getPhsWard());
				String dispWorkstore2 = StringUtils.trimToEmpty(pmsIpSpecMapping2.getPhsWard());
				compareResult = dispWorkstore1.compareTo(dispWorkstore2);
			}

			return compareResult;
		}
    }
	
	public void createPmsIpSpecMapExcl(String patHospCode, String pasSpecialty, String ward) {
		dmsPmsServiceProxy.createPmsIpSpecMapExcl(patHospCode, pasSpecialty, ward);
	}
	
	@Remove
	public void destroy() 
	{
		if ( pmsMpSpecMappingHdrList != null ) {
			pmsMpSpecMappingHdrList = null;
		}
		if (pmsSpecMappingPasSpecialtyMap != null) {
			pmsSpecMappingPasSpecialtyMap = null;
		}
		if (pasWardList != null) {
			pasWardList = null;
		}
		if (pharmacyWorkstoreList != null) {
			pharmacyWorkstoreList = null;
		}
		if (phsSpecialtyList != null) {
			phsSpecialtyList = null;
		}
		if (wardList != null) {
			wardList = null;
		}
		if (pmsSpecMappingHospitalMappingList != null) {
			pmsSpecMappingHospitalMappingList = null;
		}
	}
}
