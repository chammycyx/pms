package hk.org.ha.model.pms.vo.checkissue;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class DispItemInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long dispOrderItemId;
	
	private String pickUser;
	
	private Date pickDate;
	
	private String assembleUser;
	
	private Date assembleDate;

	private Boolean largeLayoutFlag;
	
	public Long getDispOrderItemId() {
		return dispOrderItemId;
	}

	public void setDispOrderItemId(Long dispOrderItemId) {
		this.dispOrderItemId = dispOrderItemId;
	}

	public String getPickUser() {
		return pickUser;
	}

	public void setPickUser(String pickUser) {
		this.pickUser = pickUser;
	}

	public Date getPickDate() {
		return pickDate;
	}

	public void setPickDate(Date pickDate) {
		this.pickDate = pickDate;
	}

	public String getAssembleUser() {
		return assembleUser;
	}

	public void setAssembleUser(String assembleUser) {
		this.assembleUser = assembleUser;
	}

	public Date getAssembleDate() {
		return assembleDate;
	}

	public void setAssembleDate(Date assembleDate) {
		this.assembleDate = assembleDate;
	}

	public Boolean getLargeLayoutFlag() {
		return largeLayoutFlag;
	}

	public void setLargeLayoutFlag(Boolean largeLayoutFlag) {
		this.largeLayoutFlag = largeLayoutFlag;
	}	
}