package hk.org.ha.model.pms.biz.label;

import hk.org.ha.model.pms.dms.persistence.pivas.PivasContainer;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaMethod;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.vo.label.MdsOverrideReasonLabel;
import hk.org.ha.model.pms.vo.label.PivasProductLabel;
import hk.org.ha.model.pms.vo.label.PivasWorksheet;
import hk.org.ha.model.pms.vo.label.PivasOuterBagLabel;
import java.util.Map;
import javax.ejb.Local;

@Local
public interface PivasLabelBuilderLocal {

	Map<String, Object> getPivasProductLabelParam(PivasProductLabel pivasProductLabel, String hospCode);
	
	Map<String, Object> getPivasWorksheetParam(PivasWorksheet pivasWorksheet, String hospCode);
	
	Map<String, Object> getPivasOuterBagLabelParam(PivasOuterBagLabel pivasOuterBagLabel, String hospCode);
	
	Map<String, Object> getMdsOverrideReasonLabelParam(MdsOverrideReasonLabel mdsOverrideReasonLabel, String hospCode);
	
	PivasProductLabel convertToPivasProductLabel(DispOrderItem dispOrderItem, DeliveryItem deliveryItem, PivasFormulaMethod pivasFormulaMethod, PivasContainer pivasContainer);
	
	PivasWorksheet convertToPivasWorksheet(DispOrderItem dispOrderItem, DeliveryItem deliveryItem, PivasFormulaMethod pivasFormulaMethod, PivasContainer pivasContainer);
	
	PivasOuterBagLabel convertToPivasOuterBagLabel(DispOrderItem dispOrderItem, DeliveryItem deliveryItem, PivasFormulaMethod pivasFormulaMethod, PivasContainer pivasContainer);
	
	MdsOverrideReasonLabel convertToMdsOverrideReasonLabel(DispOrderItem dispOrderItem, DeliveryItem deliveryItem, PivasFormulaMethod pivasFormulaMethod);
	
	String extractOrderSupplFreqDesc(MedProfileMoItem medProfileMoItem);
}