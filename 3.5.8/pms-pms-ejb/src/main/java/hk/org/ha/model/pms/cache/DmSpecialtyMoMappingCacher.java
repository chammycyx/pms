package hk.org.ha.model.pms.cache;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmSpecialtyMoMapping;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.corp.WorkstorePK;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("dmSpecialtyMoMappingCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class DmSpecialtyMoMappingCacher extends BaseCacher implements DmSpecialtyMoMappingCacherInf{
	
	@In
	private Workstore workstore;

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

    private Map<WorkstorePK, InnerCacher> cacherMap = new HashMap<WorkstorePK, InnerCacher>();	
	    	 
	public DmSpecialtyMoMapping getDmSpecialtyMoMappingBySpecialtyCode(String specialtyCode) {
		return (DmSpecialtyMoMapping) this.getCacher(workstore.getHospCode(), workstore.getWorkstoreCode()).get(specialtyCode);
	}
	
	public List<DmSpecialtyMoMapping> getDmSpecialtyMoMappingList() {
		return (List<DmSpecialtyMoMapping>) this.getCacher(workstore.getHospCode(), workstore.getWorkstoreCode()).getAll();
	}

	public List<DmSpecialtyMoMapping> getDmSpecialtyMoMappingListBySpecialtyCode(String prefixSpecialtyCode) {
		List<DmSpecialtyMoMapping> ret = new ArrayList<DmSpecialtyMoMapping>();
		boolean found = false;
		for (DmSpecialtyMoMapping dmSpecialtyMoMapping : getDmSpecialtyMoMappingList()) {
			if (dmSpecialtyMoMapping.getCompId().getSpecialtyCode().startsWith(prefixSpecialtyCode)){
				ret.add(dmSpecialtyMoMapping);
				found = true;
			} else {
				if (found){
					break;
				}
			}
		}
		return ret;
	}
		
    private InnerCacher getCacher(String hospCode, String workstoreCode) {
        synchronized (this) {
        	  WorkstorePK key = workstore.getId();
              InnerCacher cacher = cacherMap.get(key);
              if (cacher == null) {
                    cacher = new InnerCacher(key, this.getExpireTime());
                    cacherMap.put(key, cacher);
              }
              return cacher;
        }
  }

	private class InnerCacher extends AbstractCacher<String, DmSpecialtyMoMapping>
	{		
        private WorkstorePK workstorePK = null;

        public InnerCacher(WorkstorePK workstorePK, int expireTime) {
              super(expireTime);
              this.workstorePK = workstorePK;
        }
		
		@Override
		public DmSpecialtyMoMapping create(String specialtyCode) {
			this.getAll();
			return this.internalGet(specialtyCode);
		}

		@Override
		public Collection<DmSpecialtyMoMapping> createAll() {
			return dmsPmsServiceProxy.retrieveDmSpecialtyMoMappingList(workstorePK.getHospCode(), workstorePK.getWorkstoreCode());
		}

		@Override
		public String retrieveKey(DmSpecialtyMoMapping dmSpecialtyMoMapping) {
			return dmSpecialtyMoMapping.getCompId().getSpecialtyCode();
		}		
	}
	
	public static DmSpecialtyMoMappingCacherInf instance()
	{
		  if (!Contexts.isApplicationContextActive())
	      {
	         throw new IllegalStateException("No active application scope");
	      }
	      return (DmSpecialtyMoMappingCacherInf) Component.getInstance("dmSpecialtyMoMappingCacher", ScopeType.APPLICATION);
    }

		
}
