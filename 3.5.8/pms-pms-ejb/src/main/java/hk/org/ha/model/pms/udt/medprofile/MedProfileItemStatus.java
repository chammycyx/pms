package hk.org.ha.model.pms.udt.medprofile;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

import java.util.Arrays;
import java.util.List;

public enum MedProfileItemStatus implements StringValuedEnum {
	
	Unvet("U", "Unvet"),
	Vetted("V", "Vetted"),
	Verified("W", "Verified"),
	Discontinued("T", "Discontinued"),
	Deleted("D", "Deleted"),
	Ended("E", "Ended");
	
    private final String dataValue;
    private final String displayValue;
    
    public static final List<MedProfileItemStatus> Unvet_Vetted_Verified = Arrays.asList(Unvet, Vetted, Verified);
    public static final List<MedProfileItemStatus> Unvet_Vetted = Arrays.asList(Unvet, Vetted);
    public static final List<MedProfileItemStatus> Vetted_Verified = Arrays.asList(Vetted, Verified);
    
    
	private MedProfileItemStatus(String dataValue, String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}

	@Override
	public String getDataValue() {
		return dataValue;
	}

	@Override
	public String getDisplayValue() {
		return displayValue;
	}
	
    public static MedProfileItemStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MedProfileItemStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<MedProfileItemStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<MedProfileItemStatus> getEnumClass() {
    		return MedProfileItemStatus.class;
    	}
    }
}
