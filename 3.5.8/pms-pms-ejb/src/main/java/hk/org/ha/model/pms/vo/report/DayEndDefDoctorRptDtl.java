package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class DayEndDefDoctorRptDtl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String specCode;
	private String doctorCode;
	private Date   dispDate;
	private String patType;
	private String ticketNum;
	private String patName;
	private String orderNum;
	public String getSpecCode() {
		return specCode;
	}
	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}
	public String getDoctorCode() {
		return doctorCode;
	}
	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}
	public Date getDispDate() {
		return dispDate;
	}
	public void setDispDate(Date dispDate) {
		this.dispDate = dispDate;
	}
	public String getPatType() {
		return patType;
	}
	public void setPatType(String patType) {
		this.patType = patType;
	}
	public String getTicketNum() {
		return ticketNum;
	}
	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}
	public String getPatName() {
		return patName;
	}
	public void setPatName(String patName) {
		this.patName = patName;
	}
	public String getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}	
}
