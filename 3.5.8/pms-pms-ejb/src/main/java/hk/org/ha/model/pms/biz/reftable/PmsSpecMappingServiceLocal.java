package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.dms.persistence.PmsSpecMapping;

import javax.ejb.Local;

@Local
public interface PmsSpecMappingServiceLocal {
	
	void updatePmsSpecMapping(PmsSpecMapping updatePmsSpecMapping);
	
	void deletePmsSpecMapping(PmsSpecMapping delPmsSpecMapping);
	
	void destroy();
	
}
