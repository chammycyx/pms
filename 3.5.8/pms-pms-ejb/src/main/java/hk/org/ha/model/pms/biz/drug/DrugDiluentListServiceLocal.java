package hk.org.ha.model.pms.biz.drug;
import hk.org.ha.model.pms.vo.rx.RxItem;

import javax.ejb.Local;

@Local
public interface DrugDiluentListServiceLocal {
	void retrieveDrugDiluentListByRxItem(RxItem rxItem);
	void destroy();
}
