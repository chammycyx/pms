package hk.org.ha.model.pms.vo.charging;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class DrugPriceInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private boolean includeItem;
	
	private boolean suspendItem;
	
	private String suspendIndicator;
	
	private String itemCode;
	
	private Integer quantity;
	
	private String baseUnit;
	
	private boolean handleCharge;
	
	private Integer drugAmount;
	
	private String fullDrugDesc;
	
	private Double privateHandleCharge;
	
	private Double publicHandleCharge;
	
	private Double nepHandleCharge;
	
	private Double corpDrugPrice;
	
	private Double publicMarkupFactor;
	
	private Double privateMarkupFactor;
	
	private Double nepMarkupFactor;
	
	private String historyFullDrugDesc;
	
	private Double historyCorpDrugPrice;
	
	private boolean validPriceRetrieve;
	
	private boolean validHistoryPriceRetrieve;

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public boolean isIncludeItem() {
		return includeItem;
	}

	public void setIncludeItem(boolean includeItem) {
		this.includeItem = includeItem;
	}

	public boolean isHandleCharge() {
		return handleCharge;
	}

	public void setHandleCharge(boolean handleCharge) {
		this.handleCharge = handleCharge;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public Integer getDrugAmount() {
		return drugAmount;
	}

	public void setDrugAmount(Integer drugAmount) {
		this.drugAmount = drugAmount;
	}

	public boolean isSuspendItem() {
		return suspendItem;
	}

	public void setSuspendItem(boolean suspendItem) {
		this.suspendItem = suspendItem;
	}

	public String getFullDrugDesc() {
		return fullDrugDesc;
	}

	public void setFullDrugDesc(String fullDrugDesc) {
		this.fullDrugDesc = fullDrugDesc;
	}
	
	public Double getPrivateHandleCharge() {
		return privateHandleCharge;
	}

	public void setPrivateHandleCharge(Double privateHandleCharge) {
		this.privateHandleCharge = privateHandleCharge;
	}

	public Double getPublicHandleCharge() {
		return publicHandleCharge;
	}

	public void setPublicHandleCharge(Double publicHandleCharge) {
		this.publicHandleCharge = publicHandleCharge;
	}

	public Double getNepHandleCharge() {
		return nepHandleCharge;
	}

	public void setNepHandleCharge(Double nepHandleCharge) {
		this.nepHandleCharge = nepHandleCharge;
	}

	public String getHistoryFullDrugDesc() {
		return historyFullDrugDesc;
	}

	public void setHistoryFullDrugDesc(String historyFullDrugDesc) {
		this.historyFullDrugDesc = historyFullDrugDesc;
	}

	public Double getCorpDrugPrice() {
		return corpDrugPrice;
	}

	public void setCorpDrugPrice(Double corpDrugPrice) {
		this.corpDrugPrice = corpDrugPrice;
	}

	public Double getPublicMarkupFactor() {
		return publicMarkupFactor;
	}

	public void setPublicMarkupFactor(Double publicMarkupFactor) {
		this.publicMarkupFactor = publicMarkupFactor;
	}

	public Double getPrivateMarkupFactor() {
		return privateMarkupFactor;
	}

	public void setPrivateMarkupFactor(Double privateMarkupFactor) {
		this.privateMarkupFactor = privateMarkupFactor;
	}

	public Double getNepMarkupFactor() {
		return nepMarkupFactor;
	}

	public void setNepMarkupFactor(Double nepMarkupFactor) {
		this.nepMarkupFactor = nepMarkupFactor;
	}

	public void setHistoryCorpDrugPrice(Double historyCorpDrugPrice) {
		this.historyCorpDrugPrice = historyCorpDrugPrice;
	}

	public Double getHistoryCorpDrugPrice() {
		return historyCorpDrugPrice;
	}

	public void setValidPriceRetrieve(boolean validPriceRetrieve) {
		this.validPriceRetrieve = validPriceRetrieve;
	}

	public boolean isValidPriceRetrieve() {
		return validPriceRetrieve;
	}

	public void setValidHistoryPriceRetrieve(boolean validHistoryPriceRetrieve) {
		this.validHistoryPriceRetrieve = validHistoryPriceRetrieve;
	}

	public boolean isValidHistoryPriceRetrieve() {
		return validHistoryPriceRetrieve;
	}

	public void setSuspendIndicator(String suspendIndicator) {
		this.suspendIndicator = suspendIndicator;
	}

	public String getSuspendIndicator() {
		return suspendIndicator;
	}
}
