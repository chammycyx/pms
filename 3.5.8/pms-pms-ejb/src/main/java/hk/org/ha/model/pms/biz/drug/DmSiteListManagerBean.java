package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmSiteCacherInf;
import hk.org.ha.model.pms.corp.cache.DmSupplSiteCacherInf;
import hk.org.ha.model.pms.corp.cache.SiteCacher;
import hk.org.ha.model.pms.dms.persistence.DmSite;
import hk.org.ha.model.pms.dms.persistence.DmSupplSite;
import hk.org.ha.model.pms.vo.rx.Site;
import hk.org.ha.model.pms.vo.security.PropMap;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("dmSiteListManager")
@MeasureCalls
public class DmSiteListManagerBean implements DmSiteListManagerLocal {

	@Logger
	private Log logger;

	@In
	private DmSiteCacherInf dmSiteCacher;

	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private List<DmSite> dmSiteList;	

	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private List<DmSite> opTypeDmSiteList;	
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private List<Map<String, Object>> dmSiteMapList;

	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private PropMap dmSiteParenteralMap;

	@In
	private DmSupplSiteCacherInf dmSupplSiteCacher; 
	
	private OpTypeDmSiteComparator opTypeDmSiteComparator = new OpTypeDmSiteComparator();
	
	private void constuctDmSiteParenteralMap(List<DmSite> dmSiteList) {
		for ( DmSite principalSite : dmSiteList ) {
			if ( principalSite.getDmSiteCategory() != null ) {
				dmSiteParenteralMap.add(principalSite.getSiteCode(), principalSite.getDmSiteCategory().getParenteralSite());
			} else {
				dmSiteParenteralMap.add(principalSite.getSiteCode(), "N");
			}			
		}
	}		
	
	private void constructDmSiteMapList() {
		List<hk.org.ha.model.pms.dms.vo.Site> siteList = SiteCacher.instance().getSiteList();
		List<hk.org.ha.model.pms.dms.vo.SupplSite> supplSiteList;
		Map<String, Object> dmSiteMap;
		Map<String, Object> dmSupplSiteMap;
		DmSite dmSite;
		DmSupplSite dmSupplSite;
		List<Map<String,Object>> dmSupplSiteMapList;
		
		for ( hk.org.ha.model.pms.dms.vo.Site site : siteList ) {

			dmSiteMap = new HashMap<String, Object>();
			dmSite = dmSiteCacher.getSiteBySiteCode(site.getSiteCode());
			
			dmSiteMap.put("site", new Site(dmSite,null,true));			
			dmSiteMap.put("treeNode", site.getSiteCode());	
			
			if ( site.getSupplSites() != null && site.getSupplSites().length > 0 ) {

				supplSiteList = Arrays.asList(site.getSupplSites());								

				dmSupplSiteMapList = new ArrayList<Map<String,Object>>();

				for ( hk.org.ha.model.pms.dms.vo.SupplSite supplSite : supplSiteList ) {
					
					dmSupplSiteMap = new HashMap<String, Object>();
					dmSupplSite = dmSupplSiteCacher.getSupplSiteBySiteCodeSupplSiteEng(supplSite.getSiteCode(), supplSite.getSupplSiteEng());
					dmSupplSite.setDmSite(dmSite);
					
					dmSupplSiteMap.put("site", new Site(dmSite,dmSupplSite,true));
					dmSupplSiteMap.put("siteCode", supplSite.getSiteCode());					
					dmSupplSiteMap.put("treeNode", supplSite.getSupplSiteEng());
					dmSupplSiteMapList.add(dmSupplSiteMap);
				}
				dmSiteMap.put("children", dmSupplSiteMapList);				
			}
			dmSiteMapList.add(dmSiteMap);
		}
	}
	
	public void retrieveDmSiteList() 
	{
		logger.debug("retrieveDmSiteList");
		
		dmSiteMapList = new ArrayList<Map<String, Object>>();
		dmSiteParenteralMap	= new PropMap();

		dmSiteList = dmSiteCacher.getSiteList();		
		constuctDmSiteParenteralMap(dmSiteList);
		constructDmSiteMapList();
	}
	
	public void retrieveOpTypeDmSiteList() {
		opTypeDmSiteList = new ArrayList<DmSite>();		
		for (DmSite dmSite:dmSiteList) {
			if (!"O".equals(dmSite.getUsageType())) {
				continue;
			}
			opTypeDmSiteList.add(dmSite);
		}
		Collections.sort(opTypeDmSiteList,opTypeDmSiteComparator);
	}
	
	private static class OpTypeDmSiteComparator implements Comparator<DmSite>, Serializable {
		private static final long serialVersionUID = 1L;

		@Override
		public int compare(DmSite dmSite1, DmSite dmSite2) {			
			return (
					new CompareToBuilder()
					.append(dmSite1.getIpmoeDesc(), dmSite2.getIpmoeDesc())
					.append(dmSite1.getSiteDescEng(), dmSite2.getSiteDescEng())
			).toComparison();
		}
	}
}
