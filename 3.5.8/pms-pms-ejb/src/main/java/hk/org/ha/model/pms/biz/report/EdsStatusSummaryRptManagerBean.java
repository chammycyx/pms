package hk.org.ha.model.pms.biz.report;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.report.EdsStatusCount;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless 
@Name("edsStatusSummaryRptManager")
@MeasureCalls
public class EdsStatusSummaryRptManagerBean implements EdsStatusSummaryRptManagerLocal {

	private static final String EDS_STATUS_COUNT_CLASS_NAME = EdsStatusCount.class.getName();
	
	@PersistenceContext
	private EntityManager em;
	
	@In 
	private Workstore workstore;

	@SuppressWarnings("unchecked")
	public Map<DispOrderStatus, Long> retrieveDispOrderStatusCount(Date inputDate) {
		List<EdsStatusCount> resultList = em.createQuery(
				"select new " + EDS_STATUS_COUNT_CLASS_NAME + // 20120307 index check : DispOrder.workstore,dispDate : I_DISP_ORDER_03
				"  ('', o.status, count(o))" +
				" from DispOrder o" + 
				" where o.workstore = :workstore" + 
				" and o.dispDate = :inputDate" +
				" and o.status not in :dispOrderStatus" +
				" and o.adminStatus <> :adminStatus" +
				" and o.ticket.orderType = :orderType" +
				" group by o.status")
				.setParameter("workstore", workstore)
				.setParameter("inputDate", inputDate, TemporalType.DATE)
				.setParameter("dispOrderStatus", DispOrderStatus.Deleted_SysDeleted)
				.setParameter("adminStatus", DispOrderAdminStatus.Suspended)
				.setParameter("orderType", OrderType.OutPatient)
				.getResultList();

		Map<DispOrderStatus, Long> resultMap = new HashMap<DispOrderStatus, Long>();
		for (EdsStatusCount e : resultList) {
			resultMap.put((DispOrderStatus) e.getStatus(), e.getCount());
		}
		
		for(DispOrderStatus dos: DispOrderStatus.values()) {
			if(!resultMap.containsKey(dos)) {
				resultMap.put(dos, 0L);
			}
		}

		return resultMap;
	}
	
	@SuppressWarnings("unchecked")
	public Map<DispOrderItemStatus, Long> retrieveDispOrderItemStatusCount(Date inputDate) {
		List<EdsStatusCount> resultList = em.createQuery(
				"select new " + EDS_STATUS_COUNT_CLASS_NAME + // 20120307 index check : DispOrder.workstore,dispDate : I_DISP_ORDER_03
				"  ('', o.status, count(o))" +
				" from DispOrderItem o " +
				" where o.dispOrder.workstore = :workstore" + 
				" and o.dispOrder.dispDate = :inputDate" +
				" and o.dispOrder.status not in :dispOrderStatus" +
				" and o.dispOrder.adminStatus <> :adminStatus" +
				" and o.dispOrder.ticket.orderType = :orderType" +
				" group by o.status")
				.setParameter("workstore", workstore)
				.setParameter("inputDate", inputDate, TemporalType.DATE)
				.setParameter("dispOrderStatus", DispOrderStatus.Deleted_SysDeleted)
				.setParameter("adminStatus", DispOrderAdminStatus.Suspended)
				.setParameter("orderType", OrderType.OutPatient)
				.getResultList();

		Map<DispOrderItemStatus, Long> resultMap = new HashMap<DispOrderItemStatus, Long>();
		for (EdsStatusCount e : resultList) {
			resultMap.put((DispOrderItemStatus) e.getStatus(), e.getCount());
		}		

		for(DispOrderItemStatus dos: DispOrderItemStatus.values()) {
			if(!resultMap.containsKey(dos)) {
				resultMap.put(dos, 0L);
			}
		}

		return resultMap;
	}

	public int retrieveVettedOrderCount(Date inputDate) {
		return ((Long) em.createQuery(
					"select count(o) from DispOrder o" + // 20121112 index check : DispOrder.workstore,dispDate : I_DISP_ORDER_03
					" where o.workstore = :workstore" + 
					" and o.dispDate = :inputDate" +
					" and o.status not in :dispOrderStatus" +
					" and o.adminStatus <> :adminStatus" +
					" and o.ticket.orderType = :orderType")
					.setParameter("workstore", workstore)
					.setParameter("inputDate", inputDate, TemporalType.DATE)
					.setParameter("dispOrderStatus", DispOrderStatus.Deleted_SysDeleted)
					.setParameter("adminStatus", DispOrderAdminStatus.Suspended)
					.setParameter("orderType", OrderType.OutPatient)
					.getSingleResult()).intValue();
	}
	
	public int retrieveOutstandOrderCount(Date inputDate) {
		return ((Long) em.createQuery(
				"select count(o) from MedOrder o" + // 20140925 index check : MedOrder.workstore,status,orderType,orderDate : I_MED_ORDER_06
				" where o.workstore = :workstore" + 
				" and o.orderType = :orderType" +
				" and o.orderDate = :inputDate" +
				" and o.status = :status")
				.setParameter("workstore", workstore)
				.setParameter("orderType", OrderType.OutPatient)
				.setParameter("inputDate", inputDate, TemporalType.DATE)
				.setParameter("status", MedOrderStatus.Outstanding)
				.getSingleResult()).intValue();
	}
}
