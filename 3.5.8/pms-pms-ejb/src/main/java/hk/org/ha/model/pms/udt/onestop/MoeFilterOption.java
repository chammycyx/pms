package hk.org.ha.model.pms.udt.onestop;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum MoeFilterOption implements StringValuedEnum {
	
	AE("AE", "A&E Patient"),
	Out("O", "Out Patient"),
	In("I", "In Patient"),
	All("A", "All");
	
    private final String dataValue;
    private final String displayValue;
        
    MoeFilterOption(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static MoeFilterOption dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MoeFilterOption.class, dataValue);
	}
        
	public static class Converter extends StringValuedEnumConverter<MoeFilterOption> {

		private static final long serialVersionUID = 1L;

		public Class<MoeFilterOption> getEnumClass() {
    		return MoeFilterOption.class;
    	}
    }
}
