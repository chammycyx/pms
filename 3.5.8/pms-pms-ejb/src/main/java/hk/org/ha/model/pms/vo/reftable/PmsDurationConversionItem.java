package hk.org.ha.model.pms.vo.reftable;

import hk.org.ha.model.pms.dms.persistence.PmsDurationConversion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class PmsDurationConversionItem implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String workstoreCode;

	private String hospCode;
	
	private boolean sameWorkstoreGroup;
	
	private List<PmsDurationConversion> pmsDurationConversionList;

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setPmsDurationConversionList(
			List<PmsDurationConversion> pmsDurationConversionList) {
		this.pmsDurationConversionList = pmsDurationConversionList;
	}

	public List<PmsDurationConversion> getPmsDurationConversionList() {
		if ( pmsDurationConversionList == null ) {
			pmsDurationConversionList = new ArrayList<PmsDurationConversion>();
		}
		return pmsDurationConversionList;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setSameWorkstoreGroup(boolean sameWorkstoreGroup) {
		this.sameWorkstoreGroup = sameWorkstoreGroup;
	}

	public boolean isSameWorkstoreGroup() {
		return sameWorkstoreGroup;
	}
	
}