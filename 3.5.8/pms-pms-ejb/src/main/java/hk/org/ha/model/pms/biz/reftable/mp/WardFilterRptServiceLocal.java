package hk.org.ha.model.pms.biz.reftable.mp;

import javax.ejb.Local;

@Local
public interface WardFilterRptServiceLocal {
	
	void printWardFilterRptList();
	
	void destroy();
	
}
