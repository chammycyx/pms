package hk.org.ha.model.pms.test;

import java.io.IOException;

import javax.ejb.Local;

@Local
public interface InvokerLocal {
	
	void invoke() throws IOException;

}
