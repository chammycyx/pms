package hk.org.ha.model.pms.biz.report;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.vo.alert.AlertProfile;
import hk.org.ha.model.pms.vo.alert.mds.MedProfileAlertMsg;
import hk.org.ha.model.pms.vo.report.MdsExceptionRptContainer;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("mdsExceptionRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MdsExceptionRptServiceBean implements MdsExceptionRptServiceLocal {

	@In
	private PrintAgentInf printAgent;
	
	@In
	private MdsExceptionRptManagerLocal mdsExceptionRptManager;
	
	private List<MdsExceptionRptContainer> mdsExceptionRptList;
	
	private int reportIndex;
	
	public void retrieveMdsExceptionRptByMedProfileAlertMsg(MedProfile medProfile, List<MedProfileAlertMsg> medProfileAlertMsgList, AlertProfile alertProfile) {
		mdsExceptionRptList = mdsExceptionRptManager.retrieveMdsExceptionRptListByMedProfileAlertMsg(medProfile, medProfileAlertMsgList, alertProfile);
		printMdsExceptionRpt();
	}
	
	public int retrieveMdsExceptionRptByDeliveryReqId(Long deliveryReqId) {
		
		reportIndex = 0;
		
		mdsExceptionRptList = mdsExceptionRptManager.retrieveMdsExceptionRptListByDeliveryReqId(deliveryReqId);
		
		return mdsExceptionRptList.size();
	}

	public void generateMdsExceptionRpt() {
		printAgent.renderAndRedirect(mdsExceptionRptManager.constructRenderJob(mdsExceptionRptList, reportIndex));
	}

	public void printMdsExceptionRpt() {
		printAgent.renderAndPrint(mdsExceptionRptManager.constructRenderAndPrintJob(mdsExceptionRptList));
	}
	
	public void changeReportIndex(int index) {
		reportIndex = index;
	}

	@Remove
	public void destroy() {
		if (mdsExceptionRptList != null) {
			mdsExceptionRptList = null;
		}
	}
}
