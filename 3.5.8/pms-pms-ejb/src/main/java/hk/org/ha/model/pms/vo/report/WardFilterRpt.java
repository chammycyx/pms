package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class WardFilterRpt implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String workstore;
	
	private String monFlag;
	
	private String tueFlag;
	
	private String wedFlag;
	
	private String thuFlag;
	
	private String friFlag;
	
	private String satFlag;
	
	private String sunFlag;
	
	private String startTime;
	
	private String endTime;
	
	private String wardCode;
	
	private Date updateDate;

	private Date createDate;

	private String updateUser;

	private String createUser;

	public String getWorkstore() {
		return workstore;
	}

	public void setWorkstore(String workstore) {
		this.workstore = workstore;
	}

	public String getMonFlag() {
		return monFlag;
	}

	public void setMonFlag(String monFlag) {
		this.monFlag = monFlag;
	}

	public String getTueFlag() {
		return tueFlag;
	}

	public void setTueFlag(String tueFlag) {
		this.tueFlag = tueFlag;
	}

	public String getWedFlag() {
		return wedFlag;
	}

	public void setWedFlag(String wedFlag) {
		this.wedFlag = wedFlag;
	}

	public String getThuFlag() {
		return thuFlag;
	}

	public void setThuFlag(String thuFlag) {
		this.thuFlag = thuFlag;
	}

	public String getFriFlag() {
		return friFlag;
	}

	public void setFriFlag(String friFlag) {
		this.friFlag = friFlag;
	}

	public String getSatFlag() {
		return satFlag;
	}

	public void setSatFlag(String satFlag) {
		this.satFlag = satFlag;
	}

	public String getSunFlag() {
		return sunFlag;
	}

	public void setSunFlag(String sunFlag) {
		this.sunFlag = sunFlag;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	
}