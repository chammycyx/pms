package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.vo.alert.AlertProfile;
import hk.org.ha.model.pms.vo.alert.mds.MedProfileAlertMsg;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.MdsExceptionRptContainer;

import java.util.List;

import javax.ejb.Local;

@Local
public interface MdsExceptionRptManagerLocal {

	List<MdsExceptionRptContainer> retrieveMdsExceptionRptListByDeliveryReqId(Long deliveryReqId);
	
	List<MdsExceptionRptContainer> retrieveMdsExceptionRptListByMedProfileAlertMsg(MedProfile medProfile, List<MedProfileAlertMsg> medProfileAlertMsgList, AlertProfile alertProfile);
	
	List<RenderAndPrintJob> constructRenderAndPrintJob(List<MdsExceptionRptContainer> mdsExceptionRptList);
	
	RenderJob constructRenderJob(List<MdsExceptionRptContainer> mdsExceptionRptList, int index);
}
