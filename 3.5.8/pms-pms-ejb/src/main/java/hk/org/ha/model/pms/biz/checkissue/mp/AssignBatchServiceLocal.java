package hk.org.ha.model.pms.biz.checkissue.mp;

import hk.org.ha.model.pms.vo.checkissue.mp.AssignBatchResult;
import hk.org.ha.model.pms.vo.checkissue.mp.AssignBatchSummaryItem;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface AssignBatchServiceLocal {
	
	void retrieveAssignBatchSummary(Date batchDate, String batchNum);
	
	void retrieveUnlinkedItemByTrxId(String mpDispLabelQrCodeXml);
	
	String assignBatch(String wardCode, List<AssignBatchSummaryItem> assignBatchItemList, boolean printTrxRptFlag, boolean checkFlag, boolean assignBatch, String oriBatchNum, Date batchDate);
	
	List<AssignBatchResult> assignBatchByUnlinkItemList(List<AssignBatchSummaryItem> assignBatchSummaryItemList, boolean printTrxRptFlag, boolean checkFlag, boolean assignBatch, String oriBatchNum, Date batchDate);
	
	String getMsgCode();
	
	void destroy();
	
}