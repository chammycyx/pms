package hk.org.ha.model.pms.vo.pivas.worklist;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PivasWorklistResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String errorCode;

	private List<String> errorParamList;
	
	private String errorRemark;
	
	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	
	public List<String> getErrorParamList() {
		return errorParamList;
	}

	public void setErrorParamList(List<String> errorParamList) {
		this.errorParamList = errorParamList;
	}

	public String getErrorRemark() {
		return errorRemark;
	}

	public void setErrorRemark(String errorRemark) {
		this.errorRemark = errorRemark;
	}	
}