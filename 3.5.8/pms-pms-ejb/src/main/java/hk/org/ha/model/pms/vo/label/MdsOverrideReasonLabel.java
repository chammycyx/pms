package hk.org.ha.model.pms.vo.label;

import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.vo.printing.PrintOption;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "MdsOverrideReasonLabel")
@ExternalizedBean(type=DefaultExternalizer.class)
public class MdsOverrideReasonLabel implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@XmlElement(required = false)
	private String patName;
	
	@XmlElement(required = true)
	private String itemDesc;
	
	@XmlElement(required = false)
	private String ward;
	
	@XmlElement(required = false)
	private String bedNum;
	
	@XmlElement(required = true)	
	private String specCode;
	
	@XmlElement(required = false)
	private String caseNum;
	
	@XmlElement(required = false)
	private String itemCode;

	@XmlElement(required = true)	
	private String hospName;	
	
	@XmlElement(required = true)	
	private String hospNameChi;
	
	@XmlElement(required = true)	
	private Integer issueQty;
	
	@XmlElement(required = true)	
	private Date dispDate;
	
	@XmlElement(required = true)	
	private String binNum;
	
	@XmlElement(required = true)	
	private String floatingDrugName;
	
	@XmlElement(required = true)	
	private boolean alertFlag;
	
	@XmlElement(required = false)
	private String baseUnit;
	
	@XmlElement(required = false)
	private String batchNum;

	@XmlElement(required = true)	
	private ActionStatus actionStatus;
	
	@XmlElement(required = false)
	private boolean urgentFlag;
	
	@XmlElement(required = true)
	private String hkid;
		
	@XmlElement(required = false)
	private String patNameChi;
	
	@XmlElement(required = true)	
	private boolean singleOrderFlag;

	@XmlElement(required = true)	
	private boolean newOrderFlag;
	
	@XmlElement(required = false)
	private Integer itemNum;
	
	@XmlElement(required = true)	
	private boolean privateFlag;
	
	@XmlElement(required = true)	
	private boolean replenishFlag;
	
	@XmlElement(required = true)
	private String formCode;
	
	@XmlElement(required = false)
	private String patOverrideReason;

	@XmlElement(required = false)
	private String ddiOverrideReason;
	
	@XmlElement(required = true)
	private String userCode;
	
	@XmlElement(required = true)	
	private String maskedUserCode;
	
	@XmlElement(required = true)
	private String trxId;
	
	private transient PrintOption printOption;
	
	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getItemDesc() {
		return itemDesc;
	}

	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	public String getWard() {
		return ward;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}

	public String getBedNum() {
		return bedNum;
	}

	public void setBedNum(String bedNum) {
		this.bedNum = bedNum;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getHospName() {
		return hospName;
	}

	public void setHospName(String hospName) {
		this.hospName = hospName;
	}

	public String getHospNameChi() {
		return hospNameChi;
	}

	public void setHospNameChi(String hospNameChi) {
		this.hospNameChi = hospNameChi;
	}

	public Integer getIssueQty() {
		return issueQty;
	}

	public void setIssueQty(Integer issueQty) {
		this.issueQty = issueQty;
	}

	public Date getDispDate() {
		return dispDate;
	}

	public void setDispDate(Date dispDate) {
		this.dispDate = dispDate;
	}

	public String getBinNum() {
		return binNum;
	}

	public void setBinNum(String binNum) {
		this.binNum = binNum;
	}

	public String getFloatingDrugName() {
		return floatingDrugName;
	}

	public void setFloatingDrugName(String floatingDrugName) {
		this.floatingDrugName = floatingDrugName;
	}

	public boolean isAlertFlag() {
		return alertFlag;
	}

	public void setAlertFlag(boolean alertFlag) {
		this.alertFlag = alertFlag;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public ActionStatus getActionStatus() {
		return actionStatus;
	}

	public void setActionStatus(ActionStatus actionStatus) {
		this.actionStatus = actionStatus;
	}

	public boolean isUrgentFlag() {
		return urgentFlag;
	}

	public void setUrgentFlag(boolean urgentFlag) {
		this.urgentFlag = urgentFlag;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getPatNameChi() {
		return patNameChi;
	}

	public void setPatNameChi(String patNameChi) {
		this.patNameChi = patNameChi;
	}

	public boolean isSingleOrderFlag() {
		return singleOrderFlag;
	}

	public void setSingleOrderFlag(boolean singleOrderFlag) {
		this.singleOrderFlag = singleOrderFlag;
	}

	public boolean isNewOrderFlag() {
		return newOrderFlag;
	}

	public void setNewOrderFlag(boolean newOrderFlag) {
		this.newOrderFlag = newOrderFlag;
	}

	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public boolean isPrivateFlag() {
		return privateFlag;
	}

	public void setPrivateFlag(boolean privateFlag) {
		this.privateFlag = privateFlag;
	}

	public boolean isReplenishFlag() {
		return replenishFlag;
	}

	public void setReplenishFlag(boolean replenishFlag) {
		this.replenishFlag = replenishFlag;
	}

	public String getFormCode() {
		return formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	public String getPatOverrideReason() {
		return patOverrideReason;
	}

	public void setPatOverrideReason(String patOverrideReason) {
		this.patOverrideReason = patOverrideReason;
	}

	public String getDdiOverrideReason() {
		return ddiOverrideReason;
	}

	public void setDdiOverrideReason(String ddiOverrideReason) {
		this.ddiOverrideReason = ddiOverrideReason;
	}

	public String getMaskedUserCode() {
		return maskedUserCode;
	}

	public void setMaskedUserCode(String maskedUserCode) {
		this.maskedUserCode = maskedUserCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getTrxId() {
		return trxId;
	}

	public void setTrxId(String trxId) {
		this.trxId = trxId;
	}
	
	public PrintOption getPrintOption() {
		return printOption;
	}

	public void setPrintOption(PrintOption printOption) {
		this.printOption = printOption;
	}
}
