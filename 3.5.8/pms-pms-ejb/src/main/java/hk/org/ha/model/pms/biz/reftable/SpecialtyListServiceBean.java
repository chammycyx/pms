package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.phs.Specialty;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("specialtyListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SpecialtyListServiceBean implements SpecialtyListServiceLocal {
	
	@Out(required = false)
	private List<Specialty> specialtyList;
	
	@In
	private SpecialtyManagerLocal specialtyManager;
	
	public void retrieveSpecialtyList() 
	{
		specialtyList = specialtyManager.retrieveSpecialtyList();
	}

	@Remove
	public void destroy() 
	{
		if (specialtyList != null) {
			specialtyList = null;
		}
	}
}
