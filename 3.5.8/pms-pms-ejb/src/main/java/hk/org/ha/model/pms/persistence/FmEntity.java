package hk.org.ha.model.pms.persistence;

import hk.org.ha.model.pms.dms.persistence.DmCorpIndication;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.udt.vetting.FmStatusOption;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@MappedSuperclass
@ExternalizedBean(type=DefaultExternalizer.class)
public abstract class FmEntity extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "ITEM_CODE", length = 6)
	private String itemCode;
	
	@Column(name = "DISPLAY_NAME", nullable = false, length = 70)
	private String displayName;
	
	@Column(name = "FORM_CODE", nullable = false, length = 3)
	private String formCode;
	
	@Column(name = "SALT_PROPERTY", length = 35)
	private String saltProperty;
	
	@Column(name = "CORP_IND_CODE", length = 10)
	private String corpIndCode;
	
	@Column(name = "SUPPL_IND_CODE", length = 10)
	private String supplIndCode;
	
	@Column(name = "OTHER_MESSAGE", length = 100)
	private String otherMessage;
	
	@Column(name = "AUTH_DOCTOR_NAME", length = 70)
	private String authDoctorName;
	
	@Column(name = "FM_CREATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fmCreateDate;

	@Column(name = "FM_CREATE_USER", length = 12)
	private String fmCreateUser;
	
	@Column(name = "IND_VALID_PERIOD")
	private Integer indValidPeriod;
	
	@Converter(name = "FmEntity.prescOption", converterClass = FmStatusOption.Converter.class)
	@Convert("FmEntity.prescOption")
	@Column(name = "PRESC_OPTION", nullable = false, length = 2)
	private FmStatusOption prescOption;
	
	@Column(name = "REF_ORDER_NUM", nullable = false, length = 12)
	private String refOrderNum;
	
	@Column(name = "REF_ITEM_NUM", nullable = false)
	private Integer refItemNum;

	@Converter(name = "FmEntity.fmStatus", converterClass = FmStatus.Converter.class)
	@Convert("FmEntity.fmStatus")
	@Column(name = "FM_STATUS", nullable = false, length = 1)
	private FmStatus fmStatus;

	@Transient
	private DmCorpIndication dmCorpIndication;
	
	@Transient
	private Integer indValidPeriodForDisplay;
	
	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getFormCode() {
		return formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	public String getSaltProperty() {
		return saltProperty;
	}

	public void setSaltProperty(String saltProperty) {
		this.saltProperty = saltProperty;
	}

	public String getCorpIndCode() {
		return corpIndCode;
	}

	public void setCorpIndCode(String corpIndCode) {
		this.corpIndCode = corpIndCode;
	}

	public String getSupplIndCode() {
		return supplIndCode;
	}

	public void setSupplIndCode(String supplIndCode) {
		this.supplIndCode = supplIndCode;
	}

	public String getOtherMessage() {
		return otherMessage;
	}

	public void setOtherMessage(String otherMessage) {
		this.otherMessage = otherMessage;
	}

	public String getAuthDoctorName() {
		return authDoctorName;
	}

	public void setAuthDoctorName(String authDoctorName) {
		this.authDoctorName = authDoctorName;
	}

	public Date getFmCreateDate() {
		return (fmCreateDate == null) ? null : new Date(fmCreateDate.getTime());
	}

	public void setFmCreateDate(Date fmCreateDate) {
		this.fmCreateDate = (fmCreateDate == null) ? null : new Date(fmCreateDate.getTime());
	}	

	public String getFmCreateUser() {
		return fmCreateUser;
	}

	public void setFmCreateUser(String fmCreateUser) {
		this.fmCreateUser = fmCreateUser;
	}

	public Integer getIndValidPeriod() {
		return indValidPeriod;
	}

	public void setIndValidPeriod(Integer indValidPeriod) {
		this.indValidPeriod = indValidPeriod;
	}

	public FmStatusOption getPrescOption() {
		return prescOption;
	}

	public void setPrescOption(FmStatusOption prescOption) {
		this.prescOption = prescOption;
	}

	public String getRefOrderNum() {
		return refOrderNum;
	}

	public void setRefOrderNum(String refOrderNum) {
		this.refOrderNum = refOrderNum;
	}

	public Integer getRefItemNum() {
		return refItemNum;
	}

	public void setRefItemNum(Integer refItemNum) {
		this.refItemNum = refItemNum;
	}

	public FmStatus getFmStatus() {
		return fmStatus;
	}

	public void setFmStatus(FmStatus fmStatus) {
		this.fmStatus = fmStatus;
	}

	public DmCorpIndication getDmCorpIndication() {
		return dmCorpIndication;
	}

	public void setDmCorpIndication(DmCorpIndication dmCorpIndication) {
		this.dmCorpIndication = dmCorpIndication;
	}

	public Integer getIndValidPeriodForDisplay() {
		return indValidPeriodForDisplay;
	}

	public void setIndValidPeriodForDisplay(Integer indValidPeriodForDisplay) {
		this.indValidPeriodForDisplay = indValidPeriodForDisplay;
	}
}
