package hk.org.ha.model.pms.biz.validation;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.udt.validation.SearchTextType;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("searchTextValidator")
@MeasureCalls
public class SearchTextValidatorBean implements SearchTextValidatorLocal {
	
	@In
	private OrderValidatorLocal orderValidator;
	
	@In
	private PatientValidatorLocal patientValidator;
	
	public SearchTextType checkSearchInputType(String searchText) {
		
		if (orderValidator.checkSearchType(searchText) != SearchTextType.Unknown) {
			return orderValidator.checkSearchType(searchText);
		} else if (patientValidator.checkSearchType(searchText) != SearchTextType.Unknown) {
			return patientValidator.checkSearchType(searchText);
		} else {
			return SearchTextType.Unknown;
		}
	}
}