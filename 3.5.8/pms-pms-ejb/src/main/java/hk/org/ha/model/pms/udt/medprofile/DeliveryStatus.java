package hk.org.ha.model.pms.udt.medprofile;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DeliveryStatus implements StringValuedEnum {
	
	Assigning("A", "Assigning"),
	Printed("P", "Printed"),
	Checked("C", "Checked"),
	Delivered("E", "Sent"),
	KeepRecord("K", "KeepRecord"),
	Deleted("D", "Deleted");
	
    private final String dataValue;
    private final String displayValue;
    
	private DeliveryStatus(String dataValue, String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}

	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}
	
    public static DeliveryStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DeliveryStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<DeliveryStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<DeliveryStatus> getEnumClass() {
    		return DeliveryStatus.class;
    	}
    }
}
