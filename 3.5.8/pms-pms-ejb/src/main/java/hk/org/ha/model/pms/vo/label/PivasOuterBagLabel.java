package hk.org.ha.model.pms.vo.label;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "PivasOuterBagLabel")
@ExternalizedBean(type=DefaultExternalizer.class)
public class PivasOuterBagLabel implements Serializable {
	private static final long serialVersionUID = 1L;
		
	@XmlElement(required = true)
	private String formulaPrintName;
	
	@XmlElement(required = true)
	private String siteDesc;
	
	@XmlElement(required = true)
	private Date prepExpiryDate;
	
	@XmlElement(required = true)
	private String concn;
	
	@XmlElement(required = true)
	private String pivasDosageUnit;
	
	@XmlElement(required = true)
	private String trxId;
	
	@XmlElement(required = true)
	private String batchNum;
	
	@XmlElement(required = false)
	private String caseNum;
	
	@XmlElement(required = true)
	private String ward;
	
	@XmlElement(required = false)
	private String patNameChi;
	
	@XmlElement(required = false)
	private String patName;
	
	@XmlElement(required = true)
	private String containerName;
	
	@XmlElement(required = true)
	private Date manufactureDate;

	@XmlElement(required = false)
	private String hkid;
	
	@XmlElement(required = true)
	private String bedNum;

	@XmlElement(required = true)
	private String finalVolume;
	
	@XmlElement(required = true)
	private String diluentDesc;
	
	@XmlElement(required = false)
	private Date firstDoseDateTime;
	
	@XmlElement(required = false)
	private String freq;
	
	@XmlElement(required = false)
	private String supplFreq;
	
	@XmlElement(required = true)
	private String orderDosage;
	
	@XmlElement(required = true)
	private String orderDosageSplit;
	
	@XmlElement(required = true)
	private String finalVolumeSplit;
	
	@XmlElement(required = true)
	private String splitCount;
	
	@XmlElement(required = true)
	private Integer numOfLabel;
	
	@XmlElement(required = true)
	private boolean shelfLifeZeroFlag;

	public PivasOuterBagLabel() {
		shelfLifeZeroFlag = Boolean.FALSE;
	}

	public String getFormulaPrintName() {
		return formulaPrintName;
	}

	public void setFormulaPrintName(String formulaPrintName) {
		this.formulaPrintName = formulaPrintName;
	}

	public String getSiteDesc() {
		return siteDesc;
	}

	public void setSiteDesc(String siteDesc) {
		this.siteDesc = siteDesc;
	}

	public Date getPrepExpiryDate() {
		return prepExpiryDate;
	}

	public void setPrepExpiryDate(Date prepExpiryDate) {
		this.prepExpiryDate = prepExpiryDate;
	}

	public String getPivasDosageUnit() {
		return pivasDosageUnit;
	}

	public void setPivasDosageUnit(String pivasDosageUnit) {
		this.pivasDosageUnit = pivasDosageUnit;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getWard() {
		return ward;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}

	public String getContainerName() {
		return containerName;
	}

	public void setContainerName(String containerName) {
		this.containerName = containerName;
	}

	public Date getManufactureDate() {
		return manufactureDate;
	}

	public void setManufactureDate(Date manufactureDate) {
		this.manufactureDate = manufactureDate;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getPatNameChi() {
		return patNameChi;
	}

	public void setPatNameChi(String patNameChi) {
		this.patNameChi = patNameChi;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getBedNum() {
		return bedNum;
	}

	public void setBedNum(String bedNum) {
		this.bedNum = bedNum;
	}

	public String getDiluentDesc() {
		return diluentDesc;
	}

	public void setDiluentDesc(String diluentDesc) {
		this.diluentDesc = diluentDesc;
	}

	public Date getFirstDoseDateTime() {
		return firstDoseDateTime;
	}

	public void setFirstDoseDateTime(Date firstDoseDateTime) {
		this.firstDoseDateTime = firstDoseDateTime;
	}

	public String getFreq() {
		return freq;
	}

	public void setFreq(String freq) {
		this.freq = freq;
	}

	public String getSupplFreq() {
		return supplFreq;
	}

	public void setSupplFreq(String supplFreq) {
		this.supplFreq = supplFreq;
	}

	public boolean isShelfLifeZeroFlag() {
		return shelfLifeZeroFlag;
	}

	public void setShelfLifeZeroFlag(boolean shelfLifeZeroFlag) {
		this.shelfLifeZeroFlag = shelfLifeZeroFlag;
	}

	public String getSplitCount() {
		return splitCount;
	}

	public void setSplitCount(String splitCount) {
		this.splitCount = splitCount;
	}

	public String getFinalVolume() {
		return finalVolume;
	}

	public void setFinalVolume(String finalVolume) {
		this.finalVolume = finalVolume;
	}

	public String getFinalVolumeSplit() {
		return finalVolumeSplit;
	}

	public void setFinalVolumeSplit(String finalVolumeSplit) {
		this.finalVolumeSplit = finalVolumeSplit;
	}

	public String getConcn() {
		return concn;
	}

	public void setConcn(String concn) {
		this.concn = concn;
	}

	public String getTrxId() {
		return trxId;
	}

	public void setTrxId(String trxId) {
		this.trxId = trxId;
	}

	public Integer getNumOfLabel() {
		return numOfLabel;
	}

	public void setNumOfLabel(Integer numOfLabel) {
		this.numOfLabel = numOfLabel;
	}

	public String getOrderDosage() {
		return orderDosage;
	}

	public void setOrderDosage(String orderDosage) {
		this.orderDosage = orderDosage;
	}

	public String getOrderDosageSplit() {
		return orderDosageSplit;
	}

	public void setOrderDosageSplit(String orderDosageSplit) {
		this.orderDosageSplit = orderDosageSplit;
	}


}
