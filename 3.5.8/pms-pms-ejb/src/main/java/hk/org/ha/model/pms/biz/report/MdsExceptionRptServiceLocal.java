package hk.org.ha.model.pms.biz.report;

import java.util.List;

import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.vo.alert.AlertProfile;
import hk.org.ha.model.pms.vo.alert.mds.MedProfileAlertMsg;

import javax.ejb.Local;

@Local
public interface MdsExceptionRptServiceLocal {

	void retrieveMdsExceptionRptByMedProfileAlertMsg(MedProfile medProfile, List<MedProfileAlertMsg> medProfileAlertMsgList, AlertProfile alertProfile);

	int retrieveMdsExceptionRptByDeliveryReqId(Long deliveryReqId);
	
	void generateMdsExceptionRpt();
	
	void printMdsExceptionRpt();
	
	void changeReportIndex(int index);
	
	void destroy();
}
