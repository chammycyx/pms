package hk.org.ha.model.pms.biz.label;

import hk.org.ha.model.pms.vo.label.MpDispLabel;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;


public class MpDispLabelComparator implements Comparator<MpDispLabel>, Serializable
{
	public static enum SortField {
		CASE_NUM, BED_NUM, ITEM_CODE, BIN_NUM, WARD_CODE 
	}
	
	public static final SortField SORT_BY_P_ARRAY[] = {SortField.CASE_NUM};
	public static final SortField SORT_BY_B_ARRAY[] = {SortField.BED_NUM, SortField.CASE_NUM, SortField.ITEM_CODE};
	public static final SortField SORT_BY_I_ARRAY[] = {SortField.BIN_NUM, SortField.ITEM_CODE, SortField.CASE_NUM};
	public static final SortField SORT_BY_C_ARRAY[] = {SortField.ITEM_CODE, SortField.BIN_NUM, SortField.CASE_NUM};
	public static final SortField SORT_BY_L_ARRAY[] = {SortField.BIN_NUM, SortField.WARD_CODE, SortField.CASE_NUM, SortField.ITEM_CODE};
	
	private static final long serialVersionUID = 1L;

	private SortField[] sortBy;

	public MpDispLabelComparator(SortField[] sortBy) {
		this.sortBy = Arrays.copyOf(sortBy, sortBy.length);
	}
	
	public int compare(MpDispLabel o1, MpDispLabel o2) {
		
		int ret = 0;
		
		for (SortField sort : sortBy) {
			switch (sort) {
			case CASE_NUM:
				ret = compare(o1.getCaseNum(), o2.getCaseNum());
				break;
			case BED_NUM:
				ret = compare(o1.getBedNum(), o2.getBedNum());
				break;
			case ITEM_CODE:
				ret = compare(o1.getItemCode(), o2.getItemCode());
				break;
			case BIN_NUM:
				ret = compare(o1.getBinNum(), o2.getBinNum());
				break;
			case WARD_CODE:
				ret = compare(o1.getWard(), o2.getWard());
				break;
			}
				
			if (ret != 0) {
				return ret;
			}
		}			
		return ret;
	}
	
	private int compare(String s1, String s2) {
		
		if (s1 == s2) {
			return 0;
		} else if (s1 == null) {
			return -1;
		} else if (s2 == null) { 
			return 1;
		} else {
			return s1.compareTo(s2);
		}
	}
}	
