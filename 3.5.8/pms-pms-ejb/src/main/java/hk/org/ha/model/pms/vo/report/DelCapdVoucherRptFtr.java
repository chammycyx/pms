package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class DelCapdVoucherRptFtr implements Serializable {

	private static final long serialVersionUID = 1L;

	private int capdVoucherDelCount;
	
	private int capdItemDelCount;

	public int getCapdVoucherDelCount() {
		return capdVoucherDelCount;
	}

	public void setCapdVoucherDelCount(int capdVoucherDelCount) {
		this.capdVoucherDelCount = capdVoucherDelCount;
	}

	public int getCapdItemDelCount() {
		return capdItemDelCount;
	}

	public void setCapdItemDelCount(int capdItemDelCount) {
		this.capdItemDelCount = capdItemDelCount;
	}
	
}
