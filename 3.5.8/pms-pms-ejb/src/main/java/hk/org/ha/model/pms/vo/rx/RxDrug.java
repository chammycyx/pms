package hk.org.ha.model.pms.vo.rx;

import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.corp.cache.MsFmStatusCacher;
import hk.org.ha.model.pms.dms.biz.formatengine.FormatHints;
import hk.org.ha.model.pms.dms.biz.formatengine.ParsedOrderLineFormatter;
import hk.org.ha.model.pms.dms.biz.formatengine.ParsedOrderLineFormatterBase;
import hk.org.ha.model.pms.dms.biz.formatengine.ParsedOrderLineFormatterFactory;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.MsFmStatus;
import hk.org.ha.model.pms.dms.udt.formatengine.FormatType;
import hk.org.ha.model.pms.dms.vo.DrugRouteCriteria;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.udt.vetting.NameType;
import hk.org.ha.model.pms.vo.drug.DmDrugLite;
import hk.org.ha.model.pms.vo.drug.DmRouteLite;
import hk.org.ha.model.pms.vo.drug.DmFormLite;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
import org.granite.messaging.amf.io.util.externalizer.annotation.IgnoredProperty;
import org.jboss.seam.contexts.Contexts;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class RxDrug extends RxItem implements Serializable {

	private static final long serialVersionUID = 1L;

	protected static final char SPACE = ' ';
	
	protected static final char[] DELIMITER = new char[] {' ' , '~' , '&' , '-' , '[' , '(' , '<'};
	
	private static final String VOLUME_PATTERN = "#######.####";
	
	@XmlElement
	private String itemCode;

	@XmlElement(required = true)
	private String displayName;
	
	@XmlElement
	private String firstDisplayName;
	
	@XmlElement
	private String secondDisplayName;
	
	@XmlElement(required = true)
	private String formCode;
	
	@XmlElement(required = true)
	private String formDesc;
	
	@XmlElement(required = true)
	private String saltProperty;
	
	@XmlElement
	private String baseUnit;
		
	@XmlElement
	private String fmStatus;
	
	@XmlElement
	private Boolean dangerDrugFlag;
	
	@XmlElement
	private Boolean restrictFlag;

	@XmlElement
	private String aliasName;

	@XmlElement
	private String routeCode;

	@XmlElement
	private String routeDesc;

	@XmlElement
	private Boolean displayRouteDescFlag;
	
	@XmlElement
	private String strength;

	@XmlElement
	private BigDecimal strengthValue;

	@XmlElement
	private String strengthUnit;

	@XmlElement
	private BigDecimal volume;

	@XmlElement
	private String volumeUnit;

	@XmlElement(required = true)
	private ActionStatus actionStatus;

	@XmlElement(required = true)
	private NameType nameType;
	
	@XmlElement
	private String extraInfo;
	
	@XmlElement
	private String tradeName;

	@XmlElement
	private String drugNameHtml;
	
	@XmlElement
	private String drugNameTlf;
	
	@XmlElement
	private String drugNameXml;
	
	@XmlElement(name="Regimen")
	private Regimen regimen;
	
	@XmlElement(name="PharmDrug")
	private List<PharmDrug> pharmDrugList;
	
	@XmlTransient
	private String formattedDrugNameTlf;
	
	@XmlTransient
	private DmDrugLite dmDrugLite;
	
	@XmlTransient
	@Deprecated
	@IgnoredProperty
	private DmDrug dmDrug = null;// dummy for version 3.2.4 object reference compatibility in xml unmarshalling
	
	@XmlTransient
	private DmRouteLite dmRouteLite;
	
	@XmlTransient
	private DmFormLite dmFormLite;
	
	@XmlTransient
	private MsFmStatus msFmStatus;
	
	@XmlTransient
	private Boolean strengthCompulsory;
	
	@XmlTransient
	private String sfiCat;
	
	@XmlTransient
	private Boolean dosageCompulsory;
	
	@XmlTransient
	private Boolean workStoreDrugFlag;
	
	@XmlTransient
	private Integer drugKey;
	
	public RxDrug() {
		super();
    	dangerDrugFlag = Boolean.FALSE;
    	restrictFlag = Boolean.FALSE;
    	nameType = NameType.Blank;
    	actionStatus = ActionStatus.DispByPharm;
    	workStoreDrugFlag = Boolean.FALSE;
	}
	
	public void clearDmInfo() {
		if (dmDrugLite != null) {
			dmDrugLite = null;
		}
		if (dmRouteLite != null) {
			dmRouteLite = null;
		}
		if (dmFormLite != null) {
			dmFormLite = null;
		}
		if (msFmStatus != null) {
			msFmStatus = null;
		}
		if (dosageCompulsory != null) {
			dosageCompulsory = null;
		}
		if (strengthCompulsory != null) {
			strengthCompulsory = null;
		}
		if (sfiCat != null) {
			sfiCat = null;
		}
		if (workStoreDrugFlag != null) {
			workStoreDrugFlag = null;
		}
		if (drugKey != null) {
			drugKey = null;
		}
		
		if (regimen != null) {
			regimen.clearDmInfo();
		}		
	}
	
	public void loadDmInfo(Workstore workstore, boolean isMedDischarge, boolean loadRegimen, boolean loadSiteList) {
		
		if (dmDrugLite == null && this.getItemCode() != null) {
    		dmDrugLite = DmDrugLite.objValueOf(this.getItemCode());
    	}
		
		if (dmRouteLite == null && this.getRouteCode() != null) {
			dmRouteLite = DmRouteLite.objValueOf(this.getRouteCode());
		}
		
		if (dmFormLite == null && this.getFormCode() != null) {
			dmFormLite = DmFormLite.objValueOf(formCode);
		}
		
		if (msFmStatus == null && this.getFmStatus() != null) {
			msFmStatus = MsFmStatusCacher.instance().getMsFmStatusByFmStatus(this.getFmStatus());
		}
		
		if ((dosageCompulsory == null || strengthCompulsory == null || sfiCat == null) &&
    			this.getDisplayName() != null && this.getFormCode() != null && this.getFmStatus() != null) 
    	{
    		if (workstore == null) {
    			workstore = (Workstore) Contexts.getSessionContext().get("workstore");
    		}
    		
    		List<DmDrug> dmDrugList = null;
    		
    		if (workstore != null) {
	    		dmDrugList = DmDrugCacher.instance().getDmDrugListByDisplayNameFormSaltFmStatus(
	    				workstore, this.getDisplayName(), this.getFormCode(), this.getSaltProperty(), this.getFmStatus());
    		}
    		
    		if (dmDrugList != null && !dmDrugList.isEmpty()) {
    			DmDrug tmpDmDrug = dmDrugList.get(0);	    		    			
    			
    			if ( "Y".equals(tmpDmDrug.getDmDrugProperty().getDosageCompul()) ) {	    				
    				dosageCompulsory = Boolean.TRUE;
    			} else {	    				
    				dosageCompulsory = Boolean.FALSE;
    			}
    			
    			if ( "Y".equals(tmpDmDrug.getDmDrugProperty().getStrengthCompul()) ) {
	    			strengthCompulsory = Boolean.TRUE;
    			} else {
    				strengthCompulsory = Boolean.FALSE;
    			}
    			
    			setWorkStoreDrugFlag(Boolean.TRUE);
    			setSfiCat(tmpDmDrug.getDmMoeProperty().getSfiCategory());
    			setDrugKey(tmpDmDrug.getDmMoeProperty().getDrugKey());
    			
    		} else {
    			dosageCompulsory = Boolean.FALSE;
				strengthCompulsory = Boolean.FALSE;
    			setWorkStoreDrugFlag(Boolean.FALSE);
    			sfiCat = "";
    			setDrugKey(null);
    		}
    	}
		
		if (regimen != null && loadRegimen) {
			regimen.loadDmInfo();
			DrugRouteCriteria criteria = new DrugRouteCriteria();
			if (isMedDischarge) {
				criteria.setDispHospCode(workstore.getHospCode());
				criteria.setDispWorkstore(workstore.getWorkstoreCode());
				criteria.setFormCode(getFormCode());
				criteria.setSaltProperty(getSaltProperty());
				criteria.setTrueDisplayname(getDisplayName());
				regimen.loadFormVerbForDischarge(criteria);
			}
			else {
				regimen.loadFormVerb(formCode);
			}
			
			if (loadSiteList) {
				if ( FmStatus.FreeTextEntryItem.getDataValue().equals(fmStatus) ) {
					regimen.loadSiteList(null, isMedDischarge);
				} else {
					if (isMedDischarge) {
						regimen.loadDischargeOrderLineSiteList(criteria);
					} else {					
						regimen.loadOpSiteList(formCode);
					}
				}
			}
		}
	}		
	
	public void loadDmInfo(Workstore workstore) {
		loadDmInfo(workstore, false, true, false);		
	}
	
	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getFirstDisplayName() {
		return firstDisplayName;
	}

	public void setFirstDisplayName(String firstDisplayName) {
		this.firstDisplayName = firstDisplayName;
	}

	public String getSecondDisplayName() {
		return secondDisplayName;
	}

	public void setSecondDisplayName(String secondDisplayName) {
		this.secondDisplayName = secondDisplayName;
	}

	public String getFormCode() {
		return formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	public String getFormDesc() {
		return formDesc;
	}

	public void setFormDesc(String formDesc) {
		this.formDesc = formDesc;
	}

	public String getSaltProperty() {
		return saltProperty;
	}

	public void setSaltProperty(String saltProperty) {
		this.saltProperty = saltProperty;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public String getFmStatus() {
		return fmStatus;
	}

	public void setFmStatus(String fmStatus) {
		this.fmStatus = fmStatus;
	}
	
	public Boolean getDangerDrugFlag() {
		return dangerDrugFlag;
	}

	public void setDangerDrugFlag(Boolean dangerDrugFlag) {
		this.dangerDrugFlag = dangerDrugFlag;
	}

	public Boolean getRestrictFlag() {
		return restrictFlag;
	}

	public void setRestrictFlag(Boolean restrictFlag) {
		this.restrictFlag = restrictFlag;
	}

	public String getAliasName() {
		return aliasName;
	}

	public void setAliasName(String aliasName) {
		this.aliasName = aliasName;
	}

	public String getRouteCode() {
		return routeCode;
	}

	public void setRouteCode(String routeCode) {
		this.routeCode = routeCode;
	}

	public String getRouteDesc() {
		return routeDesc;
	}

	public void setRouteDesc(String routeDesc) {
		this.routeDesc = routeDesc;
	}

	public Boolean getDisplayRouteDescFlag() {
		return displayRouteDescFlag;
	}

	public void setDisplayRouteDescFlag(Boolean displayRouteDescFlag) {
		this.displayRouteDescFlag = displayRouteDescFlag;
	}

	public String getStrength() {
		return strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public BigDecimal getStrengthValue() {
		return strengthValue;
	}

	public void setStrengthValue(BigDecimal strengthValue) {
		this.strengthValue = strengthValue;
	}

	public String getStrengthUnit() {
		return strengthUnit;
	}

	public void setStrengthUnit(String strengthUnit) {
		this.strengthUnit = strengthUnit;
	}

	public BigDecimal getVolume() {
		return volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	public String getVolumeUnit() {
		return volumeUnit;
	}

	public void setVolumeUnit(String volumeUnit) {
		this.volumeUnit = volumeUnit;
	}

	public ActionStatus getActionStatus() {
		return actionStatus;
	}

	public void setActionStatus(ActionStatus actionStatus) {
		this.actionStatus = actionStatus;
	}

	public NameType getNameType() {
		return nameType;
	}

	public void setNameType(NameType nameType) {
		this.nameType = nameType;
	}
	
	public String getExtraInfo() {
		return extraInfo;
	}

	public void setExtraInfo(String extraInfo) {
		this.extraInfo = extraInfo;
	}
	
	public String getTradeName() {
		return tradeName;
	}

	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}

	public String getDrugNameHtml() {
		return drugNameHtml;
	}

	public void setDrugNameHtml(String drugNameHtml) {
		this.drugNameHtml = drugNameHtml;
	}
	
	public String getDrugNameTlf() {
		return drugNameTlf;
	}

	public void setDrugNameTlf(String drugNameTlf) {
		this.drugNameTlf = drugNameTlf;
	}

	public String getDrugNameXml() {
		return drugNameXml;
	}

	public void setDrugNameXml(String drugNameXml) {
		this.drugNameXml = drugNameXml;
	}

	public String getFormattedDrugNameTlf() {
		return formattedDrugNameTlf;
	}

	public void setFormattedDrugNameTlf(String formattedDrugNameTlf) {
		this.formattedDrugNameTlf = formattedDrugNameTlf;
	}

	public Regimen getRegimen() {
		return regimen;
	}

	public void setRegimen(Regimen regimen) {
		this.regimen = regimen;
	}
	
	public List<PharmDrug> getPharmDrugList() {
		if (pharmDrugList == null) {
			pharmDrugList = new ArrayList<PharmDrug>();
		}
		return pharmDrugList;
	}

	public void setPharmDrugList(List<PharmDrug> pharmDrugList) {
		this.pharmDrugList = pharmDrugList;
	}
	
	public void setDmDrugLite(DmDrugLite dmDrugLite) {
		this.dmDrugLite = dmDrugLite;
	}

	public DmDrugLite getDmDrugLite() {
		return dmDrugLite;
	}

	public void setDmRouteLite(DmRouteLite dmRouteLite) {
		this.dmRouteLite = dmRouteLite;
	}

	public DmRouteLite getDmRouteLite() {
		return dmRouteLite;
	}

	public void setDmFormLite(DmFormLite dmFormLite) {
		this.dmFormLite = dmFormLite;
	}

	public DmFormLite getDmFormLite() {
		return dmFormLite;
	}

	public void setMsFmStatus(MsFmStatus msFmStatus) {
		this.msFmStatus = msFmStatus;
	}

	public MsFmStatus getMsFmStatus() {
		return msFmStatus;
	}

	public void addPharmDrug(PharmDrug pharmDrug) {
		this.getPharmDrugList().add(pharmDrug);
	}

	public Boolean getStrengthCompulsory() {
		return strengthCompulsory;
	}

	public void setStrengthCompulsory(Boolean strengthCompulsory) {
		this.strengthCompulsory = strengthCompulsory;
	}
	
	public void setSfiCat(String sfiCat) {
		this.sfiCat = sfiCat;
	}

	public String getSfiCat() {
		return sfiCat;
	}

	public void setDosageCompulsory(Boolean dosageCompulsory) {
		this.dosageCompulsory = dosageCompulsory;
	}

	public Boolean getDosageCompulsory() {
		return dosageCompulsory;
	}

	public void setWorkStoreDrugFlag(Boolean workStoreDrugFlag) {
		this.workStoreDrugFlag = workStoreDrugFlag;
	}

	public Boolean getWorkStoreDrugFlag() {
		return workStoreDrugFlag;
	}

	public void setDrugKey(Integer drugKey) {
		this.drugKey = drugKey;
	}

	public Integer getDrugKey() {
		return drugKey;
	}

	public String getFullDrugDesc() {

		StringBuilder sb = new StringBuilder();

		sb.append(getDisplayNameSaltForm());
		
		if (StringUtils.isNotBlank(strength)) {
			sb.append(SPACE);
			sb.append(strength.toLowerCase());
		}
		
		if (volume != null && volume.compareTo(BigDecimal.ZERO) > 0 &&
			StringUtils.isNotBlank(volumeUnit)) {
			sb.append(SPACE);
			sb.append((new DecimalFormat(VOLUME_PATTERN)).format(volume));
			sb.append(volumeUnit.toLowerCase());
		}
		
		return sb.toString();
	}
	
	public String getDisplayNameSaltForm() {

		StringBuilder sb = new StringBuilder();

		sb.append(constructDisplayNameSalt());
		
		if (StringUtils.isNotBlank(formDesc)) {
			sb.append(SPACE);
			sb.append(formDesc.toLowerCase());
		}
		
		return sb.toString();
	}
	
	public String getMdsDisplayName() {
		return constructDisplayNameSalt();
	}
	
	private String constructDisplayNameSalt() {

		if (nameType == NameType.TradeName && StringUtils.isBlank(aliasName)) {
			return StringUtils.EMPTY;
		}
	
		StringBuilder sb = new StringBuilder();

		if (nameType == NameType.TradeName) {
			
			sb.append(WordUtils.capitalizeFully(aliasName, DELIMITER));
			
			sb.append(SPACE);
			sb.append('(');
			sb.append(WordUtils.capitalizeFully(displayName, DELIMITER));
			
			if (StringUtils.isNotBlank(saltProperty)) {
				sb.append(SPACE);
				sb.append(WordUtils.capitalizeFully(saltProperty, DELIMITER));
			}
			sb.append(')');

		}
		else {
			sb.append(WordUtils.capitalizeFully(displayName, DELIMITER));
			
			if (StringUtils.isNotBlank(saltProperty)) {
				sb.append(SPACE);
				sb.append(WordUtils.capitalizeFully(saltProperty, DELIMITER));
			}
		}
		
		return sb.toString();
	}
	
	public String getMdsOrderDesc() {
		return getDisplayNameSaltForm().toUpperCase();
	}
	
	public void buildTlf(){
		super.buildTlf();
		if( StringUtils.isNotEmpty( drugNameXml ) ){
			ParsedOrderLineFormatterBase formatter = ParsedOrderLineFormatterFactory.getParsedOrderLineFormatter(getInterfaceVersion());
			formatter.setHint(FormatHints.FORMAT_TYPE, FormatType.TLF);
			formatter.setHint(FormatHints.LINE_WIDTH, 45);
			formatter.setHint(FormatHints.HIDE_STRIKE_THROUGH, false);
			String result = formatter.format(drugNameXml);
			formattedDrugNameTlf = result;
		}else if( StringUtils.isNotEmpty( drugNameTlf ) ){
			formattedDrugNameTlf = drugNameTlf;
		}
	}
	
	@Deprecated
	public DmDrug getDmDrug() {
		return dmDrug;
	}

	@Deprecated
	public void setDmDrug(DmDrug dmDrug) {
		this.dmDrug = dmDrug;
	}
}
