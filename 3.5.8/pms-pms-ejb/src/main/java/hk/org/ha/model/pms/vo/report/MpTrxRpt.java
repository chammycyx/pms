package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class MpTrxRpt implements Serializable {

	private static final long serialVersionUID = 1L;

	private MpTrxRptHdr mpTrxRptHdr;
	
	private List<MpTrxRptDtl> mpTrxRptDtlList;

	public MpTrxRptHdr getMpTrxRptHdr() {
		return mpTrxRptHdr;
	}

	public void setMpTrxRptHdr(MpTrxRptHdr mpTrxRptHdr) {
		this.mpTrxRptHdr = mpTrxRptHdr;
	}

	public List<MpTrxRptDtl> getMpTrxRptDtlList() {
		return mpTrxRptDtlList;
	}

	public void setMpTrxRptDtlList(
			List<MpTrxRptDtl> mpTrxRptDtlList) {
		this.mpTrxRptDtlList = mpTrxRptDtlList;
	}

}
