package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PharmOrderItemStatus implements StringValuedEnum {

	Outstanding("O", "Outstanding"),
	Fulfilling("F", "Fulfilling"),
	Completed("C", "Completed");
	
    private final String dataValue;
    private final String displayValue;

    PharmOrderItemStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static PharmOrderItemStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PharmOrderItemStatus.class, dataValue);
	}
    
	public static class Converter extends StringValuedEnumConverter<PharmOrderItemStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<PharmOrderItemStatus> getEnumClass() {
    		return PharmOrderItemStatus.class;
    	}
    }
}
