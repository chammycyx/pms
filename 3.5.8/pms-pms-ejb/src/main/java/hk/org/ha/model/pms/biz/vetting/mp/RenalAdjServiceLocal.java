package hk.org.ha.model.pms.biz.vetting.mp;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.vo.RenalAdj;
import hk.org.ha.model.pms.persistence.medprofile.MpRenalEcrclLog;

import java.io.IOException;
import java.util.List;

import javax.ejb.Local;

@Local
public interface RenalAdjServiceLocal {

	void retrieveRenalChartRpt(List<MpRenalEcrclLog> mpRenalEcrclLogList, boolean highlightLastRecord);

	void generateRenalChartRpt();

	RenalAdj retrieveRenalPdfByItemCode(String itemCode) throws IOException;
	
	RenalAdj retrieveRenalPdfByDrugKey(String displayName, String formCode, String saltProperty) throws IOException;
	
	void generateRenalPdf();
	
	void printRenalPdf();
	
	DmDrug retrieveDmDrug(String itemCode);
	
	void destroy();

}
