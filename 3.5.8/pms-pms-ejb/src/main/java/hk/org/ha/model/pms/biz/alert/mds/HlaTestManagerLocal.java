package hk.org.ha.model.pms.biz.alert.mds;

import hk.org.ha.model.pms.persistence.AlertEntity;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;

import javax.ejb.Local;

@Local
public interface HlaTestManagerLocal {

	boolean hasHlaTestAlertBefore(String hkid, String patHospCode, String caseNum);

	AlertEntity constructHlaTestAlert(MedOrderItem medOrderItem);

}
