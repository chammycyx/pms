package hk.org.ha.model.pms.vo.onestop;

import hk.org.ha.model.pms.udt.onestop.MoeSortOption;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PrescriptionSortCriteria implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String grid;
	
	private MoeSortOption moeSortOption;
	
	private boolean descending;

	public String getGrid() {
		return grid;
	}

	public void setGrid(String grid) {
		this.grid = grid;
	}

	public MoeSortOption getMoeSortOption() {
		return moeSortOption;
	}

	public void setMoeSortOption(MoeSortOption moeSortOption) {
		this.moeSortOption = moeSortOption;
	}

	public boolean isDescending() {
		return descending;
	}

	public void setDescending(boolean descending) {
		this.descending = descending;
	}
}