package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.asa.exception.pas.PasException;
import hk.org.ha.model.pms.persistence.reftable.RefillSelection;
import hk.org.ha.model.pms.vo.reftable.RefillSelectionInfo;
import hk.org.ha.model.pms.vo.report.RefillModuleMaintRpt;

import java.util.Collection;
import java.util.List;

import javax.ejb.Local;

@Local
public interface RefillSelectionListServiceLocal {
	
	RefillSelectionInfo retrieveRefillSelectionList() throws PasException;
	
	void updateRefillSelectionList(Collection<RefillSelection> updateRefillSelectionList, Collection<RefillSelection> delRefillSelectionList)throws PasException;
	
	List <RefillModuleMaintRpt> retrieveRefillSelectionRptList();
	
	void exportRefillModuleMaintRpt();
	
	void destroy();
	
}
