package hk.org.ha.model.pms.biz.enquiry;

import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.medprofile.PurchaseRequest;
import hk.org.ha.model.pms.vo.enquiry.SfiInvoiceInfo;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

@Local
public interface SfiInvoiceEnqManagerLocal {
	List<SfiInvoiceInfo> convertInvoiceListToInvoiceSummaryForSfiInvoiceEnq(List<Invoice> invoiceList);
	
	Map<String, SfiInvoiceInfo> convertInvoiceListToInvoiceSummary(List<Invoice> invoiceList);
	
	Map<String, SfiInvoiceInfo> convertPurchaseRequestListToInvoiceSummary(List<PurchaseRequest> purchaseRequestList);
	
	Map<String, SfiInvoiceInfo> updateSfiInvoiceInfoFcsStatusPaymentInfo(Map<String, SfiInvoiceInfo> sfiInvoiceSummaryMap, Map<String, PurchaseRequest> purchaseRequestMap);
}
