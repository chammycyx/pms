package hk.org.ha.model.pms.biz.enquiry;

import javax.ejb.Local;

@Local
public interface SpecialDrugEnqServiceLocal {
	void retrieveDmFmDrug(String itemCode);
	
	void retrieveDmFmDrugByFullDrugDesc(String fullDrugDesc);
	
	void destroy();
	
}