package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
import hk.org.ha.model.pms.persistence.reftable.WardGroup;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("wardGroupManager")
@MeasureCalls
public class WardGroupManagerBean implements WardGroupManagerLocal {

	@PersistenceContext
	private EntityManager em;
	
    public WardGroupManagerBean() {
    }
    
    @SuppressWarnings("unchecked")
	public List<WardGroup> retrieveWardGroupList(WorkstoreGroup workstoreGroup) 
	{
		return em.createQuery(
						"select o from WardGroup o" + // 20120303 index check : WardGroup.workstoreGroup : FK_WARD_GROUP_01
						" where o.workstoreGroup = :workstoreGroup" +
						" order by o.wardGroupCode")
						.setParameter("workstoreGroup", workstoreGroup)
						.setHint(QueryHints.BATCH, "o.wardGroupItemList")
						.getResultList();
	}
    
}
