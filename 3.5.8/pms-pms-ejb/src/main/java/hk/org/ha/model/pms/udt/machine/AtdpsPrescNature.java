package hk.org.ha.model.pms.udt.machine;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum AtdpsPrescNature implements StringValuedEnum {

	Normal("N", "Normal"),
	MultipleDose("M","Multiple Dose"), 
	StepUpDown("S", "Step Up Down");
	
	private final String dataValue;
	private final String displayValue;
	
	AtdpsPrescNature (final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}
	
	public static AtdpsPrescNature dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(AtdpsPrescNature.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<AtdpsPrescNature> {

		private static final long serialVersionUID = 1L;

		public Class<AtdpsPrescNature> getEnumClass() {
    		return AtdpsPrescNature.class;
    	}
    }
}
