package hk.org.ha.model.pms.biz.checkissue;

import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.vo.checkissue.CheckIssueOrder;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface CheckIssueManagerLocal {
	Integer retrieveIssueWindowNum(Workstore workstore, Workstation workstation);
	
	List<CheckIssueOrder> retrieveCheckIssueOrderList(Workstore workstore, 
														Workstation workstation, 
														Date ticketDate, 
														DispOrderStatus status, 
														boolean retrieveCheckList, 
														boolean retrieveOtherCheckList, 
														String sortSeq);
	
	List<CheckIssueOrder> retrieveIssuedList(Workstore workstore, Date ticketDate, String sortSeq);
	
	Pair<List<DispOrder>, String> retrieveDispOrderByTicket(Date ticketDate, String ticketNum, Workstore workstore, boolean lockNoWait);
}
