package hk.org.ha.model.pms.persistence.reftable;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.eclipse.persistence.annotations.Customizer;
import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;

@Entity
@Table(name = "SUSPEND_STAT")
@Customizer(AuditCustomizer.class)
public class SuspendStat extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "suspendStatSeq")
	@SequenceGenerator(name = "suspendStatSeq", sequenceName = "SQ_SUSPEND_STAT", initialValue = 100000000)
	private Long id;
	
	@Column(name = "DAY_END_BATCH_DATE", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date dayEndBatchDate;
	
	@Column(name = "SUSPEND_CODE", nullable = false)
	private String suspendCode;
	
	@Column(name = "RECORD_COUNT", nullable = false)
	private Integer recordCount;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE", referencedColumnName = "HOSP_CODE", nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDayEndBatchDate() {
		if (dayEndBatchDate == null) {
			return null;
		}
		else {
			return new Date(dayEndBatchDate.getTime());
		}
	}

	public void setDayEndBatchDate(Date dayEndBatchDate) {
		if (dayEndBatchDate == null) {
			this.dayEndBatchDate = null;
		}
		else {
			this.dayEndBatchDate = new Date(dayEndBatchDate.getTime());
		}
	}

	public String getSuspendCode() {
		return suspendCode;
	}

	public void setSuspendCode(String suspendCode) {
		this.suspendCode = suspendCode;
	}

	public Integer getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}

}
