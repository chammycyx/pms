package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.PmsDurationConversion;
import hk.org.ha.model.pms.dms.persistence.PmsQtyConversion;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.reftable.DurationUnit;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsRptServiceJmsRemote;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("qtyDurationConversionRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class QtyDurationConversionRptServiceBean implements QtyDurationConversionRptServiceLocal {
	
	@In
	private Workstore workstore;

	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
    private DmsRptServiceJmsRemote dmsRptServiceProxy;

	@In
	private PrintAgentInf printAgent;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	private List<PmsQtyConversion> qtyConversionRptList;
	
	private List<PmsDurationConversion> durationConversionRptList;
	
	private boolean qtyRetrieveSuccess;
	
	private boolean durationRetrieveSuccess;

	private boolean qtyExportSuccess;

	private boolean durExportSuccess;
	
	public List<PmsQtyConversion> retrievePmsQtyConversionListForReport()
	{
		List<PmsQtyConversion> qtyConversionList = dmsRptServiceProxy.retrievePmsQtyConversionListForReport (
				workstore.getHospCode(),workstore.getWorkstoreCode());
		
		DmDrug printDmDrug;
		for (PmsQtyConversion pmsQtyConversion : qtyConversionList) {
			printDmDrug = dmDrugCacher.getDmDrug(pmsQtyConversion.getItemCode());
			if ( printDmDrug != null ) {
				pmsQtyConversion.setItemDesc(printDmDrug.getFullDrugDesc());	
			}
		}
		
		return qtyConversionList;
	}
	
	public List<PmsDurationConversion> retrievePmsDurationConversionListForReport() 
	{
		List<PmsDurationConversion> durationConversionList = dmsRptServiceProxy.retrievePmsDurationConversionListForReport (
				workstore.getHospCode(),workstore.getWorkstoreCode());
		
		DmDrug printDmDrug;
		for (PmsDurationConversion pmsDurationConversion : durationConversionList) {
			printDmDrug = dmDrugCacher.getDmDrug(pmsDurationConversion.getItemCode());
			
			if ( printDmDrug != null ) {
				pmsDurationConversion.setItemDesc(printDmDrug.getFullDrugDesc());	
			}
			pmsDurationConversion.setDurationUnitDesc( (DurationUnit.dataValueOf(pmsDurationConversion.getDurationUnit())).getDisplayValue() );
		}
		
		return durationConversionList;
	}
	
	public void printQtyDurationConversionRpt() {
		printQtyConversionRpt();
		printDurationConversionRpt();
	}
	
	private void printQtyConversionRpt() {
		qtyConversionRptList = retrievePmsQtyConversionListForReport();
		
		if (!qtyConversionRptList.isEmpty()) {
			qtyRetrieveSuccess = true; 
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("hospCode", workstore.getHospCode());
			
		    printAgent.renderAndPrint(new RenderAndPrintJob(
			    "QtyConversionRpt",
			    new PrintOption(),
			    printerSelector.retrievePrinterSelect(PrintDocType.Report),
			    parameters,
			    qtyConversionRptList));		
		}else {
			qtyRetrieveSuccess = false;
		}
	}
	
	private void printDurationConversionRpt() {
		durationConversionRptList = retrievePmsDurationConversionListForReport();
		
		if (!durationConversionRptList.isEmpty()) {
			durationRetrieveSuccess = true;
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("hospCode", workstore.getHospCode());
			
		    printAgent.renderAndPrint(new RenderAndPrintJob(
			    "DurationConversionRpt",
			    new PrintOption(),
			    printerSelector.retrievePrinterSelect(PrintDocType.Report),
			    parameters,
			    durationConversionRptList));
		}else {
			durationRetrieveSuccess = false;
		}
	}
	
	public void retrieveQtyConversionRptList() {
		qtyConversionRptList = retrievePmsQtyConversionListForReport();

		if (!qtyConversionRptList.isEmpty()) {
			qtyExportSuccess = true;
		} else {
			qtyExportSuccess = false;
		}
	}
	
	public void exportQtyConversionRpt() {
		
		if (!qtyConversionRptList.isEmpty()) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("hospCode", workstore.getHospCode());
			
			PrintOption po = new PrintOption();
			po.setDocType(ReportProvider.XLS);
			printAgent.renderAndRedirect(new RenderJob(
			    "QtyConversionXls",
			    po,
			    parameters, 
			    qtyConversionRptList));		
		}
		
	}
	
	public void retrieveDurConversionRptList() {
		durationConversionRptList = retrievePmsDurationConversionListForReport();
		
		if (!durationConversionRptList.isEmpty()) {
			durExportSuccess = true;
		} else {
			durExportSuccess = false;
		}
	}
	
	public void exportDurationConversionRpt() {
		
		if (!durationConversionRptList.isEmpty()) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("hospCode", workstore.getHospCode());
			
			PrintOption po = new PrintOption();
			po.setDocType(ReportProvider.XLS);
			printAgent.renderAndRedirect(new RenderJob(
			    "DurationConversionXls",
			    po,
			    parameters, 
			    durationConversionRptList));		
		} 
	}

	public boolean isQtyRetrieveSuccess() {
		return qtyRetrieveSuccess;
	}
	
	public boolean isDurationRetrieveSuccess() {
		return durationRetrieveSuccess;
	}
	
	public boolean isQtyExportSuccess() {
		return qtyExportSuccess;
	}
	
	public boolean isDurExportSuccess() {
		return durExportSuccess;
	}
	
	

	@Remove
	public void destroy() 
	{
	}

}
