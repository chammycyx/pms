package hk.org.ha.model.pms.vo.label;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
@XmlAccessorType(XmlAccessType.FIELD)

public class SpecialLabelInstruction implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@XmlElement(required = true)
	private String line;

	public void setLine(String line) {
		this.line = line;
	}

	public String getLine() {
		return line;
	}
}

