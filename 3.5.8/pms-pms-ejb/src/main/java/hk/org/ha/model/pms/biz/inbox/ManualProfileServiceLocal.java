package hk.org.ha.model.pms.biz.inbox;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;

import javax.ejb.Local;

@Local
public interface ManualProfileServiceLocal {
	MedProfile constructManualProfile(String hkid, String caseNum);
	void retrieveSpecCodeListAndWardCodeList();
	Patient retrievePasPatient(String hkid);
	void destroy();
}
