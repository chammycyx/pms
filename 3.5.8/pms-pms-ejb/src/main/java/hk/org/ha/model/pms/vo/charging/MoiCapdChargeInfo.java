package hk.org.ha.model.pms.vo.charging;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class MoiCapdChargeInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Integer medOrderItemIndex;
	
	private Integer medOrderCapdItemIndex;
		
	private Integer unitOfCharge;
	
	private Integer durationInDay;
	
	private boolean changeFlag;
	
	private Integer initUnitOfCharge;
	
	public MoiCapdChargeInfo(){
		unitOfCharge = 0;
		changeFlag = false;
	}
	
	public Integer getMedOrderItemIndex() {
		return medOrderItemIndex;
	}

	public void setMedOrderItemIndex(Integer medOrderItemIndex) {
		this.medOrderItemIndex = medOrderItemIndex;
	}

	public Integer getMedOrderCapdItemIndex() {
		return medOrderCapdItemIndex;
	}

	public void setMedOrderCapdItemIndex(Integer medOrderCapdItemIndex) {
		this.medOrderCapdItemIndex = medOrderCapdItemIndex;
	}

	public Integer getUnitOfCharge() {
		return unitOfCharge;
	}

	public void setUnitOfCharge(Integer unitOfCharge) {
		this.unitOfCharge = unitOfCharge;
	}

	public Integer getDurationInDay() {
		return durationInDay;
	}
	
	public void setDurationInDay(Integer durationInDay) {
		this.durationInDay = durationInDay;
	}

	public void setChangeFlag(boolean changeFlag) {
		this.changeFlag = changeFlag;
	}

	public boolean isChangeFlag() {
		return changeFlag;
	}

	public void setInitUnitOfCharge(Integer initUnitOfCharge) {
		this.initUnitOfCharge = initUnitOfCharge;
	}

	public Integer getInitUnitOfCharge() {
		return initUnitOfCharge;
	}
}
