package hk.org.ha.model.pms.biz.alert.mds;

import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.vo.alert.mds.ManualProfileCheckResult;

import java.util.List;
import java.util.Set;

import javax.ejb.Local;

@Local
public interface ManualProfileCheckServiceLocal {

	ManualProfileCheckResult retrieveAlertForSaveItem(List<MedProfileItem> medProfileItemList, MedProfile medProfile);
	
	ManualProfileCheckResult retrieveAlertForSaveProfile(MedProfile medProfile, Set<Integer> itemNumSet);
	
	ManualProfileCheckResult retrieveAlertForVerifyItem(MedProfileItem medProfileItem, MedProfile medProfile);
	
	void destory();

}
