package hk.org.ha.model.pms.biz.cddh;

import static hk.org.ha.model.pms.prop.Prop.CDDH_CRITERIA_DISPDATE_DURATION;
import static hk.org.ha.model.pms.prop.Prop.CDDH_LEGACY_ENQUIRY_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.PATIENT_PAS_PATIENT_ENABLED;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.util.StringArray;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.UnreachableException;
import hk.org.ha.model.pms.asa.exception.pas.PasException;
import hk.org.ha.model.pms.biz.label.DispLabelBuilderLocal;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.biz.validation.SearchTextValidatorLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.dms.biz.formatengine.FormatHints;
import hk.org.ha.model.pms.dms.biz.formatengine.ParsedOrderLineFormatterBase;
import hk.org.ha.model.pms.dms.biz.formatengine.ParsedOrderLineFormatterFactory;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.udt.formatengine.FormatType;
import hk.org.ha.model.pms.pas.PasServiceWrapper;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.corp.HospitalMapping;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.Gender;
import hk.org.ha.model.pms.udt.YesNoBlankFlag;
import hk.org.ha.model.pms.udt.cddh.CddhRemarkStatus;
import hk.org.ha.model.pms.udt.cddh.ItemDescFilterMode;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.validation.SearchTextType;
import hk.org.ha.model.pms.vo.cddh.CddhCriteria;
import hk.org.ha.model.pms.vo.cddh.CddhDispOrder;
import hk.org.ha.model.pms.vo.cddh.CddhDispOrderItem;
import hk.org.ha.model.pms.vo.drug.DmDrugLite;
import hk.org.ha.model.pms.vo.label.BaseLabel;
import hk.org.ha.model.pms.vo.label.DispLabel;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.report.CddhPatDrugProfileRpt;
import hk.org.ha.model.pms.vo.rx.Regimen;
import hk.org.ha.model.pms.vo.security.PropMap;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpCddhServiceJmsRemote;
import hk.org.ha.service.pms.asa.interfaces.pas.PasServiceJmsRemote;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.joda.time.Months;
import org.joda.time.Period;

/**
 * Session Bean implementation class CddhServiceBean
 */
@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("cddhService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CddhServiceBean implements CddhServiceLocal {

	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private Patient cddhPatient;

	@Out(required = false)
	private List<CddhDispOrderItem> cddhDispOrderItemList;

	@Out(required = false)
	private List<CddhDispOrder> cddhDispOrderList;

	@Out(required = false)
	private CddhCriteria cddhCriteria;
	
	@In
	private CorpCddhServiceJmsRemote corpCddhServiceProxy;
	
	@In
	private CorpCddhServiceJmsRemote corpCddhService2Proxy;

	@In
	private PasServiceWrapper pasServiceWrapper;

	@In
	private PasServiceJmsRemote pasServiceProxy;

	@In
	private SearchTextValidatorLocal searchTextValidator;

	@In
	private Workstore workstore;

	@In
	private PrintAgentInf printAgent;

	@In
	private DispLabelBuilderLocal dispLabelBuilder;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private PropMap propMap;
	
	@In
	private UamInfo uamInfo;
	
	@In
	private Workstation workstation;
	
	@Out(required = false)
	private String errMsg;

	private List<CddhPatDrugProfileRpt> cddhPatDrugProfileRptList;
	
	private SearchTextType searchTextType;
	
	private DateFormat df = new SimpleDateFormat("ddMMyyyy");

	public final static Pattern PATNAME_PATTERN = Pattern.compile("^[A-Za-z\\-\\s]+,[A-Za-z\\-\\s]*");
	// for performance issue, use the following reg exp to prevent user from input surname only
	public final static Pattern SEARCH_BY_NAME_ON_LABEL_PATTERN = Pattern.compile("^[A-Za-z\\-\\s]+,[A-Za-z\\-\\s]+");
	
	private final static String PERMISSION_EDIT_CDDH_REMARK = "permission.editCddhRemark";
	
	public static final String NEW_LINE = "\n";
	
	private static Boolean isPatName(String patName) {
		if (PATNAME_PATTERN.matcher(patName).find()) {
			return true;
		}
		return false;
	}
	
	private static Boolean isPatternForNameOnLabel(String patName) {
		if (SEARCH_BY_NAME_ON_LABEL_PATTERN.matcher(StringUtils.trim(patName)).find()) {
			return true;
		}
		return false;
	}
	
	private void validateCddhCriteria(CddhCriteria cddhCriteria) {
		errMsg = null;
		
		logger.debug("validateCddhCriteria: start validating ...");
		
		if (StringUtils.isNotBlank(cddhCriteria.getHkid())) {
			searchTextType = searchTextValidator.checkSearchInputType(cddhCriteria.getHkid());
			
			logger.debug("validateCddhCriteria: HKID - #0, seachTextType - #1", cddhCriteria.getHkid(), searchTextType.getDisplayValue());
			if (!searchTextType.equals(SearchTextType.Hkid)) {
				errMsg = "0033";
				return;
			}
		} else if (StringUtils.isNotBlank(cddhCriteria.getCaseNum())) {
			searchTextType = searchTextValidator.checkSearchInputType(cddhCriteria.getCaseNum());
			
			logger.debug("validateCddhCriteria: caseNum - #0, seachTextType - #1", cddhCriteria.getCaseNum(), searchTextType.getDisplayValue());
			if (!searchTextType.equals(SearchTextType.Case)) {
				errMsg = "0068";
				return;
			}
		} else if (StringUtils.isNotBlank(cddhCriteria.getPatientName())) {
			logger.debug("validateCddhCriteria: patName - #0", cddhCriteria.getPatientName());
			if (!isPatName(cddhCriteria.getPatientName())) {
				errMsg = "0069";
				return;
			}
		} else if (StringUtils.isNotBlank(cddhCriteria.getNameOnLabel())) {
			logger.debug("validateCddhCriteria: nameOnLabel - #0", cddhCriteria.getNameOnLabel());
			if (!isPatternForNameOnLabel(cddhCriteria.getNameOnLabel())) {
				errMsg = "0069";
				return;
			}
		}
	}
	
	private Patient retrievePasPatientByFriendClubList(String hospCode, String caseNum) throws UnreachableException {
		List<String> friendClubList = new ArrayList<String>();
		
		Hospital hospital = em.find(Hospital.class, hospCode);
		if (hospital != null) {
			for (HospitalMapping hospitalMapping : hospital.getHospitalMappingList()) { 
				friendClubList.add(hospitalMapping.getPatHospCode());
			}
		}
		
		Patient patient = null;
		for (String patHospCode: friendClubList) {
			try {
				patient = pasServiceWrapper.retrievePasPatientByCaseNum(patHospCode, caseNum, false);
				
				logger.info("retrievePasPatientByFriendClubList: Query CDDH by caseNum, successfully retrieve patient from opas by friend club hospital: patKey=#0, hkid=#1, patHospCode=#2", 
						patient.getPatKey(), 
						patient.getHkid(), 
						patHospCode);
				
				return patient;
			} catch (PasException e) {
				// search by another patHospCode when not found.
			}
		}
		
		return patient;
	}
	
	private Patient retrievePasPatient(CddhCriteria cddhCriteria) {
		Patient patient = null;
		errMsg = null;
		if (PATIENT_PAS_PATIENT_ENABLED.get(false)) {
			if (!StringUtils.isEmpty(cddhCriteria.getHkid())) {
				try {
					patient = pasServiceWrapper.retrievePasPatientByHkid(cddhCriteria.getHkid(), "", "", false);
				} catch (PasException e) {
					errMsg = "0020";
					logger.debug("No patient infomation is found by hkid: #0", cddhCriteria.getHkid());
				} catch (UnreachableException e) {
					errMsg = "0281";
					logger.debug("Hkpmi service is down");
				}
			} else if (!StringUtils.isEmpty(cddhCriteria.getCaseNum())) {
				try {
					try {
						patient = pasServiceWrapper.retrievePasPatientByCaseNum(cddhCriteria.getPatientHospcode(), cddhCriteria.getCaseNum(), false);
					} catch (PasException e) {
						patient = retrievePasPatientByFriendClubList(cddhCriteria.getPatientHospcode(), cddhCriteria.getCaseNum());
						if (patient == null) {
							throw e;
						}
					}
				} catch (PasException e) {
					errMsg = "0020";
					logger.debug("No patient infomation is found by case: #0" , cddhCriteria.getCaseNum());
				} catch (UnreachableException e) {
					errMsg = "0281";
					logger.debug("Hkpmi service is down");
				}
			}
		} else {
			logger.debug("PATIENT_PAS_PATIENT_ENABLED rule is N");
			errMsg = "0023";
		}
		return patient;
	}
	
	private boolean proxy2ServiceRequired(Date startDate, Date endDate) {
		if (endDate == null) {
			endDate = new Date();
		}
		
		if (startDate == null) {
			return true;
		} else {
			DateTime start = new DateTime(startDate.getTime());
			DateTime end = new DateTime(endDate.getTime());
			
			Months months = Months.monthsBetween(start, end);

			return months.getMonths() > 12;
		}
	}
	
	private void postLoad(
			DispOrderItem dispOrderItem,
			PharmOrderItem pharmOrderItem,
			MedOrderItem medOrderItem) {
		
		if (pharmOrderItem.getDmDrugLite() == null && pharmOrderItem.getItemCode() != null) {
    		pharmOrderItem.setDmDrugLite(DmDrugLite.objValueOf(pharmOrderItem.getItemCode()));
    	}
	}
	
	@SuppressWarnings("unchecked")
	private void insertDispOrderItemToHashMap(DispOrderItem dispOrderItem) {
		DispOrder dispOrder = dispOrderItem.getDispOrder();
		Workstore doWorkstore = dispOrder.getWorkstore();
		PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
		
		// insert dispOrderItem into hash map
		Map<StringArray, DispOrderItem> dispOrderItemHashMap = (Map<StringArray, DispOrderItem>) Contexts.getSessionContext().get("dispOrderItemHashMap");
		if (!dispOrderItem.isLegacy()) {
			dispOrderItemHashMap.put(new StringArray(dispOrderItem.getId().toString()), dispOrderItem);
		} else {
			dispOrderItemHashMap.put(new StringArray(
					doWorkstore.getHospCode(), 
					dispOrder.getId().toString(), 
					pharmOrderItem.getItemNum().toString(), 
					df.format(dispOrder.getIssueDate())), 
					dispOrderItem);
			
			logger.debug("insertDispOrderItemToHashMap: hospCode=#0, dispOrderNum=#1, itemNum=#2, issueDate=#3, dispOrderItem=#4", 
					doWorkstore.getHospCode(),
					dispOrder.getId().toString(),
					pharmOrderItem.getItemNum().toString(),
					df.format(dispOrder.getIssueDate()),
					dispOrderItem);
		}
	}
	
	public void retrieveDispOrderItem(CddhCriteria cddhCriteria) {
		Patient patient = null;
		List<DispOrderItem> dispOrderItemList = null;
		
		Map<StringArray, DispOrderItem> dispOrderItemHashMap = new HashMap<StringArray, DispOrderItem>();
		Contexts.getSessionContext().set("dispOrderItemHashMap", dispOrderItemHashMap);
		
		validateCddhCriteria(cddhCriteria);
		if (errMsg != null) {
			return;
		}
		
		// retrieve patient from pas
		if (StringUtils.isNotBlank(cddhCriteria.getHkid()) && cddhCriteria.getHkid().contains("P:")) {// temporarily 
			cddhCriteria.setPatKey(StringUtils.replace(cddhCriteria.getHkid(), "P:", ""));
		} else { 
			if (!cddhCriteria.isExcept()) {
				patient = this.retrievePasPatient(cddhCriteria);
				if (patient != null){
					cddhCriteria.setPatKey(patient.getPatKey());
				}
			}
		}
		
		if (errMsg != null) {
			return;
		}
		
		Boolean includeLegacyCddh = CDDH_LEGACY_ENQUIRY_ENABLED.get(false);
		Boolean hkpmiEnabled = PATIENT_PAS_PATIENT_ENABLED.get(false);
		
		cddhCriteria.setLegacyCddhIncluded(includeLegacyCddh);
		cddhCriteria.setHkpmiEnabled(hkpmiEnabled);
		cddhCriteria.setLoginHospCode(workstore.getHospCode());
		cddhCriteria.setLoginWorkstoreCode(workstore.getWorkstoreCode());
		cddhCriteria.setUserCode(uamInfo.getUserId());
		cddhCriteria.setWorkstationCode(workstation.getHostName());
		cddhCriteria.setRetrieveFromPmsFlag(true);
		
		if (proxy2ServiceRequired(cddhCriteria.getDispDateStart(), cddhCriteria.getDispDateEnd())) {
			dispOrderItemList = corpCddhService2Proxy.retrieveDispOrderItem(cddhCriteria);
			logger.debug("retrieveDispOrderItem: corpCddhService2Proxy activated for Cddh");
		} else {
			dispOrderItemList = corpCddhServiceProxy.retrieveDispOrderItem(cddhCriteria);
			logger.debug("retrieveDispOrderItem: corpCddhServiceProxy activated for Cddh");
		}
		
		if (dispOrderItemList == null || dispOrderItemList.isEmpty()) {
			errMsg = "0005";
			return;
		} else {
			// for exceptional case
			if (cddhCriteria.isExcept()) {
				Patient tempPatient = dispOrderItemList.get(0).getDispOrder().getPharmOrder().getMedOrder().getPatient();
				if (StringUtils.isBlank(tempPatient.getPatKey())) {
					patient = tempPatient;
				}
			}
			// for except case end
		}
		
		for (DispOrderItem dispOrderItem : dispOrderItemList) {
			
			DispOrder dispOrder = dispOrderItem.getDispOrder();
			PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
			MedOrderItem medOrderItem = pharmOrderItem.getMedOrderItem();
			
			postLoad(dispOrderItem, pharmOrderItem, medOrderItem);
			insertDispOrderItemToHashMap(dispOrderItem);
			
			BaseLabel baseLabel = dispOrderItem.getBaseLabel();
			
			if (baseLabel != null) {
				PrintOption printOption = new PrintOption();
				printOption.setPrintLang(PrintLang.Eng);
				baseLabel.setPrintOption(printOption);
			}
			
			// access right check part 2
			if (!propMap.getValueAsBoolean(PERMISSION_EDIT_CDDH_REMARK)) {
				dispOrder.setCddhRemarkStatus(CddhRemarkStatus.AccessDenied);
			}
		}
		
    	// sorting DispOrderItemComparator
    	Collections.sort(dispOrderItemList, new DispOrderItemComparator());

		this.cddhCriteria = cddhCriteria;
    	this.cddhPatient = patient;
    	
    	
    	this.cddhDispOrderItemList = convertCddhDispOrderItemList(dispOrderItemList);
	}
	
	@SuppressWarnings("unchecked")
	public void retrieveLegacyDispLabel(List<String> dispOrderItemKey) {
		DispOrderItem dispOrderItemFromHashMap = null;

		Map<StringArray, DispOrderItem> dispOrderItemHashMap = (Map<StringArray, DispOrderItem>) Contexts.getSessionContext().get("dispOrderItemHashMap");
		dispOrderItemFromHashMap = dispOrderItemHashMap.get(new StringArray(dispOrderItemKey.get(0), dispOrderItemKey.get(1), dispOrderItemKey.get(2), dispOrderItemKey.get(3)));

		DispLabel dispLabelConverted = dispLabelBuilder.convertToDispLabel(dispOrderItemFromHashMap, null);
		if (dispLabelConverted != null) {
			PrintOption printOption = new PrintOption();
			printOption.setPrintLang(PrintLang.Eng);
			dispLabelConverted.setPrintOption(printOption);
		}
		//dispLabelConverted.setItemDesc(dispOrderItemFromHashMap.getDrugName());
		dispOrderItemFromHashMap.setBaseLabel(dispLabelConverted);
	}

	public void createCddhCriteria() {

		cddhCriteria = new CddhCriteria();
		
		// cddg disp date period from today
		Period period = Period.months(CDDH_CRITERIA_DISPDATE_DURATION.get());

		// calculate the start day from today
		DateTime endDate = (new DateMidnight()).toDateTime();
		DateTime startDate = endDate.minus(period);

		cddhCriteria.setDispDateStart(startDate.toDate());
		cddhCriteria.setDispDateEnd(endDate.toDate());
		cddhCriteria.setDefaultDispDateStart(startDate.toDate());
		cddhCriteria.setDefaultDispDateEnd(endDate.toDate());
		cddhCriteria.setPatientHospcode(workstore.getHospCode());
		cddhCriteria.setDangerDrugFlag(YesNoBlankFlag.Blank);
		cddhCriteria.setGender(Gender.Male);
		cddhCriteria.setItemDescFilterMode(ItemDescFilterMode.BeginWith);
		
		errMsg = null;
		cddhPatient = null;
		cddhDispOrderItemList = new ArrayList<CddhDispOrderItem>();
		cddhDispOrderList = new ArrayList<CddhDispOrder>();
		
		Map<StringArray, DispOrderItem> dispOrderItemHashMap = new HashMap<StringArray, DispOrderItem>();
		Contexts.getSessionContext().set("dispOrderItemHashMap", dispOrderItemHashMap);
	}

	private List<Patient> retrievePasPatientByNameSexAge(CddhCriteria cddhCriteria) {
		
		List<Patient> patList = new ArrayList<Patient>();
		
		if (PATIENT_PAS_PATIENT_ENABLED.get(false)) {
			try {
				logger.debug("retrievePasPatientByNameSexAge: PatientName=#0, Gender=#1, FromAge=#2, ToAge=#3", 
						 cddhCriteria.getPatientName(),
						 cddhCriteria.getGender(),
						 cddhCriteria.getFromAge(),
						 cddhCriteria.getToAge());
				
				List<Patient> pasPatientList = pasServiceProxy.retrievePasPatientByNameSexAge(
						cddhCriteria.getPatientName(),
						cddhCriteria.getGender(),
						cddhCriteria.getFromAge(),
						cddhCriteria.getToAge());
				
				logger.debug("retrievePasPatientByNameSexAge: ** Retrieve from OPAS completed !!" );
				
				return pasPatientList;
				
			} catch (PasException e) {
				errMsg = "0020";
				logger.debug("No patient infomation is found by Name, Sex, and Age");
			} catch (UnreachableException e) {
				errMsg = "0281";
				logger.debug("Hkpmi service is down");
			}
		} else {
			 
			logger.debug("PATIENT_PAS_PATIENT_ENABLED rule is false");
			errMsg = "0023";
				
		}
		
		return patList;
	}
	
	public void retrieveDispOrderLikePatName(CddhCriteria cddhCriteria) {

		this.cddhCriteria = cddhCriteria;
		validateCddhCriteria(cddhCriteria);

		if (errMsg != null) {
			return;
		}
		
		CddhCriteria cddhCriteriaSearchLikePatName = new CddhCriteria();

		List<Patient> patList = null;
		Boolean includeCddh = CDDH_LEGACY_ENQUIRY_ENABLED.get(false);
		Boolean hkpmiEnabled = PATIENT_PAS_PATIENT_ENABLED.get(false);
		cddhCriteriaSearchLikePatName.setLegacyCddhIncluded(includeCddh);
		cddhCriteriaSearchLikePatName.setHkpmiEnabled(hkpmiEnabled);
		
		if (!StringUtils.isBlank(cddhCriteria.getPatientName())) {
			
			cddhCriteriaSearchLikePatName.setPatientName(cddhCriteria.getPatientName());
			cddhCriteriaSearchLikePatName.setGender(cddhCriteria.getGender());
			cddhCriteriaSearchLikePatName.setFromAge(cddhCriteria.getFromAge());
			cddhCriteriaSearchLikePatName.setToAge(cddhCriteria.getToAge());
			
			patList = retrievePasPatientByNameSexAge(cddhCriteria);
			cddhCriteriaSearchLikePatName.setPatList(patList);

		} else if (!StringUtils.isBlank(cddhCriteria.getNameOnLabel())) {
			cddhCriteriaSearchLikePatName.setLabelDispDateStart(cddhCriteria.getLabelDispDateStart());
			cddhCriteriaSearchLikePatName.setLabelDispDateEnd(cddhCriteria.getLabelDispDateEnd());
			cddhCriteriaSearchLikePatName.setLabelDob(cddhCriteria.getLabelDob());
			cddhCriteriaSearchLikePatName.setLabelHospcode(cddhCriteria.getLabelHospcode());
			cddhCriteriaSearchLikePatName.setNameOnLabel(cddhCriteria.getNameOnLabel());
		}

		List<DispOrder> tempDispOrderList = corpCddhServiceProxy.retrieveDispOrderLikePatName(cddhCriteriaSearchLikePatName);
		cddhDispOrderList = convertCddhDispOrderList(tempDispOrderList);

		// sorting by date
    	Collections.sort(cddhDispOrderList, new PatListComparator());
	}
	
	public void printCddhPatDrugProfileRpt(CddhPatDrugProfileRpt cddhPatDrugProfileRpt) {
		cddhPatDrugProfileRptList = new ArrayList<CddhPatDrugProfileRpt>();
		cddhPatDrugProfileRptList.add(cddhPatDrugProfileRpt);
		
	    printAgent.renderAndPrint(new RenderAndPrintJob(
			    "CddhPatDrugProfileRpt",
			    new PrintOption(),
			    printerSelector.retrievePrinterSelect(PrintDocType.Report),
			    cddhPatDrugProfileRptList));
	}

	private List<CddhDispOrder> convertCddhDispOrderList(List<DispOrder> dispOrderList) {
		List<CddhDispOrder> tempCddhDispOrderList = new ArrayList<CddhDispOrder>();
		
		for (DispOrder dispOrder : dispOrderList) {
			CddhDispOrder cddhDispOrder = new CddhDispOrder();
			
			MedOrder medOrder = dispOrder.getPharmOrder().getMedOrder();
			PharmOrder pharmOrder = dispOrder.getPharmOrder();

			if (medOrder.getPatient() != null) {
				cddhDispOrder.setMoPatientName(medOrder.getPatient().getName());
				cddhDispOrder.setMoPatientHkid(medOrder.getPatient().getHkid());
				cddhDispOrder.setMoPatientPatKey(medOrder.getPatient().getPatKey());
				cddhDispOrder.setMoPatientDob(medOrder.getPatient().getDob());
				cddhDispOrder.setMoPatientPhone(medOrder.getPatient().getPhone());				
			}
			
			cddhDispOrder.setPoName(pharmOrder.getName());
			
			cddhDispOrder.setDoId(dispOrder.getId());
			cddhDispOrder.setDoHospCode(dispOrder.getWorkstore().getHospCode());
			cddhDispOrder.setDoIssueDate(dispOrder.getIssueDate());
			cddhDispOrder.setDoCddhExceptionFlag(dispOrder.getCddhExceptionFlag());
			
			tempCddhDispOrderList.add(cddhDispOrder);			
		}
		
		return tempCddhDispOrderList;
	}
	
	private List<CddhDispOrderItem> convertCddhDispOrderItemList(List<DispOrderItem> dispOrderItemList) {
		List<CddhDispOrderItem> tempCddhDispOrderItemList = new ArrayList<CddhDispOrderItem>();
		
		for (DispOrderItem dispOrderItem : dispOrderItemList) {
			CddhDispOrderItem cddhDispOrderItem = new CddhDispOrderItem();
			
			MedOrder medOrder = dispOrderItem.getDispOrder().getPharmOrder().getMedOrder();
			PharmOrder pharmOrder = dispOrderItem.getDispOrder().getPharmOrder();
			DispOrder dispOrder = dispOrderItem.getDispOrder();
			MedOrderItem medOrderItem = dispOrderItem.getPharmOrderItem().getMedOrderItem();
			PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
			
			if (dispOrder.getTicket() == null) {
				// ticket may be null because of batch join, so just skip it
				logger.info("dispOrderItem skipped due to batch join, id = " + dispOrderItem.getId());
				continue;
			}			
			
			cddhDispOrderItem.setMoRefNum(medOrder.getRefNum());
			cddhDispOrderItem.setMoOrderType(medOrder.getOrderType());
			cddhDispOrderItem.setMoDocType(medOrder.getDocType());
			
			cddhDispOrderItem.setMoiFmStatus(medOrderItem.getFmStatus());
			cddhDispOrderItem.setMoiAlertDesc(medOrderItem.getAlertDesc());
			cddhDispOrderItem.setMoiAlertRemark(medOrderItem.getAlertRemark());
			
			if (medOrderItem.getItemNum() != null && medOrderItem.getItemNum() >= 10000) {
				// do not show order line tooltip for CARS2 non-IPMOE order
				cddhDispOrderItem.setMoiDrugOrderText(null);
			} else if (medOrderItem.getRxItem().getDrugOrderXml() != null) {
				ParsedOrderLineFormatterBase formatter = ParsedOrderLineFormatterFactory.getParsedOrderLineFormatter(medOrderItem.getRxItem().getInterfaceVersion());
				formatter.setHint(FormatHints.FORMAT_TYPE, FormatType.TXT);
				formatter.setHint(FormatHints.LINE_WIDTH, 999);
				formatter.setHint(FormatHints.HIDE_STRIKE_THROUGH, false);
				cddhDispOrderItem.setMoiDrugOrderText(formatter.format(medOrderItem.getRxItem().getDrugOrderXml()));
			} else if (medOrderItem.getRxItem().getFormattedDrugOrderTlf() != null) {
				String drugOrderText = removeDrugOrderTlfTag(medOrderItem.getRxItem().getFormattedDrugOrderTlf());
				cddhDispOrderItem.setMoiDrugOrderText(drugOrderText);
			} else {
				cddhDispOrderItem.setMoiDrugOrderText(null);
			}
			
			cddhDispOrderItem.setPoWardCode(pharmOrder.getWardCode());
			cddhDispOrderItem.setPoDoctorCode(pharmOrder.getDoctorCode());
			cddhDispOrderItem.setPoDoctorName(pharmOrder.getDoctorName());
			cddhDispOrderItem.setPoCaseNum(pharmOrder.getMedCase().getCaseNum());
			cddhDispOrderItem.setPoPasPatGroupCode(pharmOrder.getMedCase().getPasPatGroupCode());
			cddhDispOrderItem.setPoPasPayCode(pharmOrder.getMedCase().getPasPayCode());
			
			cddhDispOrderItem.setPoiItemCode(pharmOrderItem.getItemCode());
			cddhDispOrderItem.setPoiItemNum(pharmOrderItem.getItemNum());
			cddhDispOrderItem.setPoiFmStatus(pharmOrderItem.getFmStatus());
			cddhDispOrderItem.setPoiActionStatus(pharmOrderItem.getActionStatus());
			cddhDispOrderItem.setPoiTradeName(pharmOrderItem.getTradeName());
			cddhDispOrderItem.setPoiCapdVoucherFlag(pharmOrderItem.getCapdVoucherFlag());
			cddhDispOrderItem.setPoiBaseUnit(pharmOrderItem.getBaseUnit());
			cddhDispOrderItem.setPoiRemarkText(pharmOrderItem.getRemarkText());
			cddhDispOrderItem.setPoiMultiDoseFlag(convertMultiDoseFlag(pharmOrderItem));
			cddhDispOrderItem.setPoiRegimenDuration(pharmOrderItem.getRegimen().getDuration());
			cddhDispOrderItem.setPoiRegimenDurationUnit(pharmOrderItem.getRegimen().getDurationUnit());
			if (pharmOrderItem.getDmDrugLite() != null) {
				DmDrug dmDrug = DmDrugCacher.instance().getDmDrug(pharmOrderItem.getItemCode());
				cddhDispOrderItem.setPoiDmDrugCommodityGroup(dmDrug.getCommodityGroup());
				cddhDispOrderItem.setPoiDmDrugCommodityType(dmDrug.getCommodityType());
			}
			
			cddhDispOrderItem.setDoId(dispOrder.getId());
			cddhDispOrderItem.setDoIssueDate(dispOrder.getIssueDate());
			cddhDispOrderItem.setDoRefillFlag(dispOrder.getRefillFlag());
			cddhDispOrderItem.setDoSpecCode(dispOrder.getSpecCode());
			cddhDispOrderItem.setDoPatCatCode(dispOrder.getPatCatCode());
			cddhDispOrderItem.setDoCddhRemarkStatus(dispOrder.getCddhRemarkStatus());
			cddhDispOrderItem.setDoHospCode(dispOrder.getWorkstore().getHospCode());
			cddhDispOrderItem.setDoWorkstoreCode(dispOrder.getWorkstore().getWorkstoreCode());
			if (dispOrder.getWorkstore().getCddhWorkstoreGroup() != null) {
				cddhDispOrderItem.setDoCddhWorkstoreGroupCode(dispOrder.getWorkstore().getCddhWorkstoreGroup().getCddhWorkstoreGroupCode());
			} else {
				cddhDispOrderItem.setDoCddhWorkstoreGroupCode(null);
			}
			if (dispOrder.getCddhSpecCode() != null) {
				cddhDispOrderItem.setDoCddhSpecCode(dispOrder.getCddhSpecCode());
			} else {
				cddhDispOrderItem.setDoCddhSpecCode(null);
			}
			
			cddhDispOrderItem.setDoiId(dispOrderItem.getId());
			cddhDispOrderItem.setDoiDispQty(dispOrderItem.getDispQty());
			cddhDispOrderItem.setDoiDurationInDay(dispOrderItem.getDurationInDay());
			cddhDispOrderItem.setDoiStartDate(dispOrderItem.getStartDate());
			cddhDispOrderItem.setDoiEndDate(dispOrderItem.getEndDate());
			cddhDispOrderItem.setDoiWarnCode1(dispOrderItem.getWarnCode1());
			cddhDispOrderItem.setDoiWarnCode2(dispOrderItem.getWarnCode2());
			cddhDispOrderItem.setDoiWarnCode3(dispOrderItem.getWarnCode3());
			cddhDispOrderItem.setDoiWarnCode4(dispOrderItem.getWarnCode4());
			cddhDispOrderItem.setDoiWarnDesc1(dispOrderItem.getWarnDesc1());
			cddhDispOrderItem.setDoiWarnDesc2(dispOrderItem.getWarnDesc2());
			cddhDispOrderItem.setDoiWarnDesc3(dispOrderItem.getWarnDesc3());
			cddhDispOrderItem.setDoiWarnDesc4(dispOrderItem.getWarnDesc4());
			cddhDispOrderItem.setDoiRemarkText(dispOrderItem.getRemarkText());
			cddhDispOrderItem.setDoiRemarkType(dispOrderItem.getRemarkType());
			cddhDispOrderItem.setDoiRemarkUpdateUser(dispOrderItem.getRemarkUpdateUser());
			cddhDispOrderItem.setDoiRemarkUpdateDate(dispOrderItem.getRemarkUpdateDate());
			cddhDispOrderItem.setDoiUnitPrice(dispOrderItem.getUnitPrice());
			cddhDispOrderItem.setDoiChargeSpecCode(dispOrderItem.getChargeSpecCode());
			cddhDispOrderItem.setDoiLegacy(dispOrderItem.isLegacy());
			cddhDispOrderItem.setDoiLegacyMultiDose(dispOrderItem.isLegacyMultiDose());
			cddhDispOrderItem.setDoiSingleDosageValue(dispOrderItem.getSingleDosageValue());
			cddhDispOrderItem.setDoiEpisodeType(dispOrderItem.getEpisodeType());
			cddhDispOrderItem.setDoiOverrideSeqNum(dispOrderItem.getOverrideSeqNum());
			cddhDispOrderItem.setDoiItemCatCode(dispOrderItem.getItemCatCode());
			cddhDispOrderItem.setDoiCddhDrugDesc(dispOrderItem.getCddhDrugDesc());
			cddhDispOrderItem.setDoiPivasFlag(dispOrderItem.getPivasFlag());
			cddhDispOrderItem.setDoiBaseLabelExist(dispOrderItem.getBaseLabel() != null);
			if (dispOrderItem.getBaseLabel() != null) {
				cddhDispOrderItem.setDoiBaseLabelInstructionListExist(dispOrderItem.getBaseLabel().getInstructionList() != null);
				cddhDispOrderItem.setDoiBaseLabelItemDesc(dispOrderItem.getBaseLabel().getItemDesc());
				cddhDispOrderItem.setDoiBaseLabelInstructionText(dispOrderItem.getBaseLabel().getInstructionText());
			} else {
				cddhDispOrderItem.setDoiBaseLabelInstructionListExist(Boolean.FALSE);
				cddhDispOrderItem.setDoiBaseLabelItemDesc(null);
				cddhDispOrderItem.setDoiBaseLabelInstructionText(null);
			}
			
			cddhDispOrderItem.setTicketNum(dispOrder.getTicket().getTicketNum());
			cddhDispOrderItem.setTicketDate(dispOrder.getTicket().getTicketDate());
			
			tempCddhDispOrderItemList.add(cddhDispOrderItem);			
		}
		
		return tempCddhDispOrderItemList;
	}
	
	private Boolean convertMultiDoseFlag(PharmOrderItem pharmOrderItem) {
		Regimen regimen = pharmOrderItem.getRegimen();
		
		if (regimen != null){
			if (regimen.getDoseGroupList().size() > 1) { // step up down
				return Boolean.TRUE;
			} else if (regimen.getDoseGroupList().get(0).getDoseList().size() > 1) {
				return Boolean.TRUE;
			}
		}
		
		return Boolean.FALSE;
	}
	
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public String getErrMsg() {
		return errMsg;
	}
	
	private String removeDrugOrderTlfTag(String drugOrderTlf) {
		
		return drugOrderTlf.replace("<br/>",NEW_LINE)
							.replace("</div>",NEW_LINE)
							.replace("&nbsp;"," ")
							.replace("&ndash;","-")
							.replaceAll("<[^>]*>","")
							.replace("&lt;","<")
							.replace("&gt;",">")
							.replace("&apos;","'")
							.replace("&quot","\"")
							.replace("&amp;","&");
	}
	
	@Remove
	public void destroy() {
		if (cddhDispOrderItemList != null) {
			cddhDispOrderItemList = null;
		}
		if (cddhDispOrderList != null) {
			cddhDispOrderList = null;
		}
		if (cddhPatient != null) {
			cddhPatient = null;
		}
		if (cddhCriteria != null) {
			cddhCriteria = null;
		}
		if (corpCddhServiceProxy != null) {
			corpCddhServiceProxy = null;
		}
		if (pasServiceProxy != null) {
			pasServiceProxy = null;
		}
	}
	
    // Comparator for Cddh DispOrderItem sort by date
    private static class DispOrderItemComparator implements Comparator<DispOrderItem>, Serializable {

    	private static final long serialVersionUID = 1L;

    	@Override
		public int compare(DispOrderItem dispOrderItem1, DispOrderItem dispOrderItem2) {
			int compareResult = 0;
			Date issueDate1 = DateUtils.truncate(dispOrderItem1.getDispOrder().getIssueDate(), Calendar.DAY_OF_MONTH);
			Date issueDate2 = DateUtils.truncate(dispOrderItem2.getDispOrder().getIssueDate(), Calendar.DAY_OF_MONTH);
			compareResult = issueDate2.compareTo(issueDate1);

			if (compareResult == 0) {
				Ticket ticket1 = dispOrderItem1.getDispOrder().getTicket();
				Ticket ticket2 = dispOrderItem2.getDispOrder().getTicket();
				if (ticket1 != null && ticket2 != null) {
					compareResult = StringUtils.trimToEmpty(ticket2.getTicketNum()).compareTo(StringUtils.trimToEmpty(ticket1.getTicketNum()));
				}
				
				if (compareResult == 0) {
					compareResult = StringUtils.trimToEmpty(dispOrderItem1.getCddhDrugDesc()).compareTo(StringUtils.trimToEmpty(dispOrderItem2.getCddhDrugDesc()));
						
					if (compareResult == 0) {
						compareResult = dispOrderItem1.getDispQty().compareTo(dispOrderItem2.getDispQty());
					}
				}
			}
			return compareResult;
		}
    }
    
    private static class PatListComparator implements Comparator<CddhDispOrder>, Serializable {

    	private static final long serialVersionUID = 1L;

    	@Override
		public int compare(CddhDispOrder dispOrder1, CddhDispOrder dispOrder2) {
			int compareResult = 0;
			String hospCode1 = dispOrder1.getDoHospCode();
			String hospCode2 = dispOrder2.getDoHospCode();
			compareResult = hospCode1.compareTo(hospCode2);
			
			if (compareResult == 0) {
				Date issueDate1 = DateUtils.truncate(dispOrder1.getDoIssueDate(), Calendar.DAY_OF_MONTH);
				Date issueDate2 = DateUtils.truncate(dispOrder2.getDoIssueDate(), Calendar.DAY_OF_MONTH);
				compareResult = issueDate2.compareTo(issueDate1);
				
				if (compareResult == 0) {
					compareResult = dispOrder1.getDoIssueDate().compareTo(dispOrder2.getDoIssueDate());
				}
			}
			return compareResult;
		}
    }
}
