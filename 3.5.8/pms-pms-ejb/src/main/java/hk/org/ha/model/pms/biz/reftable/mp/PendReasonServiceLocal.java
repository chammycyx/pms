package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.model.pms.persistence.reftable.PendingReason;

import java.util.Collection;

import javax.ejb.Local;

@Local
public interface PendReasonServiceLocal {

	void retrievePendingReasonList();
	
	void updatePendingReasonList(Collection<PendingReason> newPendingList);
	
	void destroy();
	
}
