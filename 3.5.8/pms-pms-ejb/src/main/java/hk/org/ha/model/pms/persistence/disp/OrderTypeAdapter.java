package hk.org.ha.model.pms.persistence.disp;

import hk.org.ha.model.pms.udt.vetting.OrderType;

import java.io.Serializable;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class OrderTypeAdapter extends XmlAdapter<String, OrderType> implements Serializable {

	private static final long serialVersionUID = 1L;

	public String marshal( OrderType v ) {		
		return (v == null) ? null : v.getDataValue();
	}
	
	public OrderType unmarshal( String v ) {		
		return OrderType.dataValueOf(v);
	}

}