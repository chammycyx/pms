package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum RemarkItemStatus implements StringValuedEnum {

	NoRemark("A", "No Remark"),
	UnconfirmedAdd("N", "Unconfirmed Remark - Item Add"),
	UnconfirmedOrginial("I", "Unconfirmed Remark - Item Modify Original"),
	UnconfirmedModify("*", "Unconfirmed Remark - Item Modify"),
	UnconfirmedDelete("R", "Unconfirmed Remark - Item Delete"),
	Confirmed("C", "Confirmed Remark"),
	ConfirmedOriginal("L", "Confirmed Remark - Original Item in Item Modify or Item Delete"),
	ConfirmedOldOriginal("H", "Confirmed Remark - Old Original Item in Item Modify"),
	Deleted("D", "Deleted"),
	OldVersion("OV", "Deleted Old Version Item (not sent in OG message)");

    private final String dataValue;
    private final String displayValue;

    RemarkItemStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static RemarkItemStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(RemarkItemStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<RemarkItemStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<RemarkItemStatus> getEnumClass() {
    		return RemarkItemStatus.class;
    	}
    }
}
