package hk.org.ha.model.pms.biz.vetting.mp;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;

import java.util.List;

import javax.ejb.Local;

@Local
public interface CheckAtdpsManagerLocal {

	boolean isValidAtdpsItem(MedProfileMoItem medProfileMoItem, MedProfilePoItem medProfilePoItem, List<Integer> atdpsPickStationsNumList, boolean checkDuration, boolean callByVetting);
	boolean isValidAtdpsItem(Workstore workstore, MedProfileMoItem medProfileMoItem, MedProfilePoItem medProfilePoItem, boolean checkDuration, boolean callByVetting);
}
