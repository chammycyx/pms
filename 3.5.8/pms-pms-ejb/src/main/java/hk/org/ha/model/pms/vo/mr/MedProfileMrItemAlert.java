package hk.org.ha.model.pms.vo.mr;

import hk.org.ha.model.pms.persistence.AlertEntity;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class MedProfileMrItemAlert extends AlertEntity {
	
	private static final long serialVersionUID = 1L;

	private MedProfileMrItem medProfileMrItem;

	public MedProfileMrItemAlert() {
	}
	
	public MedProfileMrItemAlert(AlertEntity alertEntity) {
		this();
		setAlertType(alertEntity.getAlertType());
		setAlertXml(alertEntity.getAlertXml());
		setOverrideReason(alertEntity.getOverrideReason());
		setAckUser(alertEntity.getAckUser());
		setAckDate(alertEntity.getAckDate());
		setAlert(alertEntity.getAlert());
	}
	
	public MedProfileMrItem getMedProfileMrItem() {
		return medProfileMrItem;
	}

	public void setMedProfileMrItem(MedProfileMrItem medProfileMrItem) {
		this.medProfileMrItem = medProfileMrItem;
	}
}
