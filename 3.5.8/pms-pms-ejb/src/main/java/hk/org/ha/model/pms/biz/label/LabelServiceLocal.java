package hk.org.ha.model.pms.biz.label;

import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.udt.printing.PrintLang;

import java.util.List;

import javax.ejb.Local;

@Local
public interface LabelServiceLocal {

	void getCddhDispLabel(List<String> dispOrderItemKey);
	
	void getDispLabelPreview( PharmOrder pharmOrder, PrintLang printLang, String printType );
	
	void getMpDispLabelPreview( MedProfileItem medProfileItem, MedProfileMoItem medProfileMoItem );
	
	void previewDispLabel();
	
	void previewMpDispLabel();
	
	void printDispLabelForOrderEdit();
	
	void printMpDispLabelForOrderEdit();
	
    void destroy();    

}