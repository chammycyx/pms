package hk.org.ha.model.pms.vo.reftable.mp;

import hk.org.ha.model.pms.dms.persistence.PmsFixedConcnSpec;
import hk.org.ha.model.pms.dms.persistence.PmsFixedConcnStatus;
import hk.org.ha.model.pms.persistence.corp.HospitalMapping;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class FixedConcnStatusInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<Map<String, Object>> pmsFixedConcnStatusTreeNode;
	
	private List<PmsFixedConcnSpec> pmsFixedConcnSpecTempList;
	
	private List<WorkstoreDrug> workstoreDrugList;
	
	private List<HospitalMapping> hospitalMappingList;

	public List<Map<String, Object>> getPmsFixedConcnStatusTreeNode() {
		return pmsFixedConcnStatusTreeNode;
	}

	public void setPmsFixedConcnStatusTreeNode(
			List<Map<String, Object>> pmsFixedConcnStatusTreeNode) {
		this.pmsFixedConcnStatusTreeNode = pmsFixedConcnStatusTreeNode;
	}

	public List<PmsFixedConcnSpec> getPmsFixedConcnSpecTempList() {
		return pmsFixedConcnSpecTempList;
	}

	public void setPmsFixedConcnSpecTempList(
			List<PmsFixedConcnSpec> pmsFixedConcnSpecTempList) {
		this.pmsFixedConcnSpecTempList = pmsFixedConcnSpecTempList;
	}

	public List<WorkstoreDrug> getWorkstoreDrugList() {
		return workstoreDrugList;
	}

	public void setWorkstoreDrugList(List<WorkstoreDrug> workstoreDrugList) {
		this.workstoreDrugList = workstoreDrugList;
	}

	public List<HospitalMapping> getHospitalMappingList() {
		return hospitalMappingList;
	}

	public void setHospitalMappingList(List<HospitalMapping> hospitalMappingList) {
		this.hospitalMappingList = hospitalMappingList;
	}

}