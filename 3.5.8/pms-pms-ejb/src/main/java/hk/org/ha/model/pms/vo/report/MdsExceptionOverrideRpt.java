package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class MdsExceptionOverrideRpt implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String caseNum;
	
	private String patName;
	
	private String patNameChi;
	
	private String hkid;
	
	private String sex;
	
	private String patCatCode;
	
	private String patAllergy;
	
	private String patAdr;
	
	private Boolean g6pdFlag;
	
	private Boolean pregnancyCheckFlag;
	
	private Boolean allergyServerAvailableFlag;
	
	private String drugDisplayName;
	
	private String drugDisplayName1;
	
	private String drugDisplayName2;
	
	private String ward;
	
	private Boolean refillFlag = Boolean.FALSE;
	
	private String remark;
	
	private Boolean crossDdiFlag = Boolean.FALSE;
	
	private List<MdsExceptionOverrideRptDtl> mdsExceptionOverrideRptDtlList;	
	
	private Boolean ehrAllergyServerAvailableFlag;
	
	private String ehrAllergy;
	
	private String ehrAdr;

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getPatNameChi() {
		return patNameChi;
	}

	public void setPatNameChi(String patNameChi) {
		this.patNameChi = patNameChi;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getPatCatCode() {
		return patCatCode;
	}

	public void setPatCatCode(String patCatCode) {
		this.patCatCode = patCatCode;
	}

	public String getPatAllergy() {
		return patAllergy;
	}

	public void setPatAllergy(String patAllergy) {
		this.patAllergy = patAllergy;
	}

	public String getPatAdr() {
		return patAdr;
	}

	public void setPatAdr(String patAdr) {
		this.patAdr = patAdr;
	}

	public Boolean getG6pdFlag() {
		return g6pdFlag;
	}

	public void setG6pdFlag(Boolean g6pdFlag) {
		this.g6pdFlag = g6pdFlag;
	}

	public Boolean getPregnancyCheckFlag() {
		return pregnancyCheckFlag;
	}

	public void setPregnancyCheckFlag(Boolean pregnancyCheckFlag) {
		this.pregnancyCheckFlag = pregnancyCheckFlag;
	}

	public Boolean getAllergyServerAvailableFlag() {
		return allergyServerAvailableFlag;
	}

	public void setAllergyServerAvailableFlag(Boolean allergyServerAvailableFlag) {
		this.allergyServerAvailableFlag = allergyServerAvailableFlag;
	}

	public String getDrugDisplayName() {
		return drugDisplayName;
	}

	public void setDrugDisplayName(String drugDisplayName) {
		this.drugDisplayName = drugDisplayName;
	}
	
	public String getDrugDisplayName1() {
		return drugDisplayName1;
	}

	public void setDrugDisplayName1(String drugDisplayName1) {
		this.drugDisplayName1 = drugDisplayName1;
	}

	public String getDrugDisplayName2() {
		return drugDisplayName2;
	}

	public void setDrugDisplayName2(String drugDisplayName2) {
		this.drugDisplayName2 = drugDisplayName2;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getSex() {
		return sex;
	}

	public String getWard() {
		return ward;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}

	public Boolean getRefillFlag() {
		return refillFlag;
	}

	public void setRefillFlag(Boolean refillFlag) {
		this.refillFlag = refillFlag;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public List<MdsExceptionOverrideRptDtl> getMdsExceptionOverrideRptDtlList() {
		if (mdsExceptionOverrideRptDtlList == null) {
			mdsExceptionOverrideRptDtlList = new ArrayList<MdsExceptionOverrideRptDtl>();
		}
		return mdsExceptionOverrideRptDtlList;
	}

	public void setMdsExceptionOverrideRptDtlList(List<MdsExceptionOverrideRptDtl> mdsExceptionOverrideRptDtlList) {
		this.mdsExceptionOverrideRptDtlList = mdsExceptionOverrideRptDtlList;
	}
	
	public void addMdsExceptionOverrideRptDtl(MdsExceptionOverrideRptDtl mdsExceptionOverrideRptDtl) {
		getMdsExceptionOverrideRptDtlList().add(mdsExceptionOverrideRptDtl);
	}

	public Boolean getCrossDdiFlag() {
		return crossDdiFlag;
	}

	public void setCrossDdiFlag(Boolean crossDdiFlag) {
		this.crossDdiFlag = crossDdiFlag;
	}

	public Boolean getEhrAllergyServerAvailableFlag() {
		return ehrAllergyServerAvailableFlag;
	}

	public void setEhrAllergyServerAvailableFlag(Boolean ehrAllergyServerAvailableFlag) {
		this.ehrAllergyServerAvailableFlag = ehrAllergyServerAvailableFlag;
	}

	public String getEhrAllergy() {
		return ehrAllergy;
	}

	public void setEhrAllergy(String ehrAllergy) {
		this.ehrAllergy = ehrAllergy;
	}

	public String getEhrAdr() {
		return ehrAdr;
	}

	public void setEhrAdr(String ehrAdr) {
		this.ehrAdr = ehrAdr;
	}
}