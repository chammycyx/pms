package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class CapdVoucherDtl implements Serializable {
		
	private static final long serialVersionUID = 1L;

	private String voucherNum;
	
	private String itemCode;
	
	private String drugName;
	
	private String connectSystem;
	
	private Integer dispQty;
	
	private String baseUnit;
	
	private String manualVoucherNum;
	
	private String status;

	public String getVoucherNum() {
		return voucherNum;
	}

	public void setVoucherNum(String voucherNum) {
		this.voucherNum = voucherNum;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getDrugName() {
		return drugName;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public String getConnectSystem() {
		return connectSystem;
	}

	public void setConnectSystem(String connectSystem) {
		this.connectSystem = connectSystem;
	}

	public Integer getDispQty() {
		return dispQty;
	}

	public void setDispQty(Integer dispQty) {
		this.dispQty = dispQty;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public String getManualVoucherNum() {
		return manualVoucherNum;
	}

	public void setManualVoucherNum(String manualVoucherNum) {
		this.manualVoucherNum = manualVoucherNum;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	


}
