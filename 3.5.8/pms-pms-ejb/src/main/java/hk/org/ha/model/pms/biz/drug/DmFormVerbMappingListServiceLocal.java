package hk.org.ha.model.pms.biz.drug;
import hk.org.ha.model.pms.vo.security.PropMap;

import javax.ejb.Local;

@Local
public interface DmFormVerbMappingListServiceLocal {

	void retrieveDmFormVerbMappingListBySiteCode(String siteCode);
		
	PropMap getFormVerbProperties();
	
	void destroy();
	
}
