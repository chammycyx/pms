package hk.org.ha.model.pms.vo.charging;

import hk.org.ha.model.pms.udt.medprofile.DeliveryItemStatus;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class MpSfiInvoiceItem implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String itemCode;
	
	private String fullDrugDesc;

	private String baseUnit;
	
	private Date dispDate;
	
	private DeliveryItemStatus deliveryItemStatus;
	
	private BigDecimal dispQty;
	
	private BigDecimal chargeQty;
	
	private Integer drugAmount;
	
	private Double corpDrugPrice;
	
	private Double publicMarkupFactor;
	
	private Double privateMarkupFactor;
	
	private Double nepMarkupFactor;
	
	private boolean includeItem;
	
	private Long deliveryItemId;
	
	private Long deliveryItemVersion;
	
	private ActionStatus actionStatus;
	
	private String drugName;
	
	private boolean wardStockFlag;
	
	private boolean itemSuspend;
	
	private String issueDuration;
	
	private Date endDate;

	public boolean isWardStockFlag() {
		return wardStockFlag;
	}

	public void setWardStockFlag(boolean wardStockFlag) {
		this.wardStockFlag = wardStockFlag;
	}

	public boolean isItemSuspend() {
		return itemSuspend;
	}

	public void setItemSuspend(boolean itemSuspend) {
		this.itemSuspend = itemSuspend;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getFullDrugDesc() {
		return fullDrugDesc;
	}

	public void setFullDrugDesc(String fullDrugDesc) {
		this.fullDrugDesc = fullDrugDesc;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public Date getDispDate() {
		return dispDate;
	}

	public void setDispDate(Date dispDate) {
		this.dispDate = dispDate;
	}

	public DeliveryItemStatus getDeliveryItemStatus() {
		return deliveryItemStatus;
	}

	public void setDeliveryItemStatus(DeliveryItemStatus deliveryItemStatus) {
		this.deliveryItemStatus = deliveryItemStatus;
	}

	public BigDecimal getDispQty() {
		return dispQty;
	}

	public void setDispQty(BigDecimal dispQty) {
		this.dispQty = dispQty;
	}

	public BigDecimal getChargeQty() {
		return chargeQty;
	}

	public void setChargeQty(BigDecimal chargeQty) {
		this.chargeQty = chargeQty;
	}

	public Integer getDrugAmount() {
		return drugAmount;
	}

	public void setDrugAmount(Integer drugAmount) {
		this.drugAmount = drugAmount;
	}

	public Double getCorpDrugPrice() {
		return corpDrugPrice;
	}

	public void setCorpDrugPrice(Double corpDrugPrice) {
		this.corpDrugPrice = corpDrugPrice;
	}

	public Double getPublicMarkupFactor() {
		return publicMarkupFactor;
	}

	public void setPublicMarkupFactor(Double publicMarkupFactor) {
		this.publicMarkupFactor = publicMarkupFactor;
	}

	public Double getPrivateMarkupFactor() {
		return privateMarkupFactor;
	}

	public void setPrivateMarkupFactor(Double privateMarkupFactor) {
		this.privateMarkupFactor = privateMarkupFactor;
	}

	public Double getNepMarkupFactor() {
		return nepMarkupFactor;
	}

	public void setNepMarkupFactor(Double nepMarkupFactor) {
		this.nepMarkupFactor = nepMarkupFactor;
	}

	public void setIncludeItem(boolean includeItem) {
		this.includeItem = includeItem;
	}

	public boolean isIncludeItem() {
		return includeItem;
	}

	public void setDeliveryItemId(Long deliveryItemId) {
		this.deliveryItemId = deliveryItemId;
	}

	public Long getDeliveryItemId() {
		return deliveryItemId;
	}

	public void setDeliveryItemVersion(Long deliveryItemVersion) {
		this.deliveryItemVersion = deliveryItemVersion;
	}

	public Long getDeliveryItemVersion() {
		return deliveryItemVersion;
	}

	public void setActionStatus(ActionStatus actionStatus) {
		this.actionStatus = actionStatus;
	}

	public ActionStatus getActionStatus() {
		return actionStatus;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public String getDrugName() {
		return drugName;
	}

	public void setIssueDuration(String issueDuration) {
		this.issueDuration = issueDuration;
	}

	public String getIssueDuration() {
		return issueDuration;
	}
	
}
