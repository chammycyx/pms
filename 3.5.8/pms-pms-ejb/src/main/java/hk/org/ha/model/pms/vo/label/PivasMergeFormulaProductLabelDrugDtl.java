package hk.org.ha.model.pms.vo.label;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "PivasMergeFormulaProductLabelDrugDtl")
@ExternalizedBean(type=DefaultExternalizer.class)
public class PivasMergeFormulaProductLabelDrugDtl implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@XmlElement(required = true)
	private Integer sortSeq;
	
	@XmlElement(required = true)
	private String drugVolume;
	
	@XmlElement(required = true)
	private String drugDisplayName;
	
	@XmlElement(required = false)
	private String drugSaltProperty;
	
	@XmlElement(required = false)
	private String drugStrength;
	
	@XmlElement(required = true)
	private String pivasDosageUnit;

	@XmlElement(required = false)
	private String afterConcn;
	
	@XmlElement(required = false)
	private String diluentDesc;
	
	@XmlElement(required = false)
	private String diluentVolume;
	
	public Integer getSortSeq() {
		return sortSeq;
	}

	public void setSortSeq(Integer sortSeq) {
		this.sortSeq = sortSeq;
	}
	
	public String getDrugVolume() {
		return drugVolume;
	}

	public void setDrugVolume(String drugVolume) {
		this.drugVolume = drugVolume;
	}

	public String getDrugDisplayName() {
		return drugDisplayName;
	}

	public void setDrugDisplayName(String drugDisplayName) {
		this.drugDisplayName = drugDisplayName;
	}

	public String getDrugSaltProperty() {
		return drugSaltProperty;
	}

	public void setDrugSaltProperty(String drugSaltProperty) {
		this.drugSaltProperty = drugSaltProperty;
	}

	public String getDrugStrength() {
		return drugStrength;
	}

	public void setDrugStrength(String drugStrength) {
		this.drugStrength = drugStrength;
	}

	public String getPivasDosageUnit() {
		return pivasDosageUnit;
	}

	public void setPivasDosageUnit(String pivasDosageUnit) {
		this.pivasDosageUnit = pivasDosageUnit;
	}

	public String getAfterConcn() {
		return afterConcn;
	}

	public void setAfterConcn(String afterConcn) {
		this.afterConcn = afterConcn;
	}

	public String getDiluentDesc() {
		return diluentDesc;
	}

	public void setDiluentDesc(String diluentDesc) {
		this.diluentDesc = diluentDesc;
	}

	public String getDiluentVolume() {
		return diluentVolume;
	}

	public void setDiluentVolume(String diluentVolume) {
		this.diluentVolume = diluentVolume;
	}

}
