package hk.org.ha.model.pms.biz.report;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.ManualPrescRpt;
import hk.org.ha.model.pms.vo.report.ManualPrescSummary;
import hk.org.ha.model.pms.vo.rx.CapdItem;
import hk.org.ha.model.pms.vo.rx.CapdRxDrug;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.DoseGroup;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import net.lingala.zip4j.exception.ZipException;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;


@Stateful
@Scope(ScopeType.SESSION)
@Name("manualPrescRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ManualPrescRptServiceBean implements ManualPrescRptServiceLocal {

	private static final String MANUAL_PRESC_SUMMARY_CLASS_NAME = ManualPrescSummary.class.getName();
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private Workstore workstore;
	
	@In
	private PrintAgentInf printAgent;

	private List<ManualPrescRpt> manualPrescRptList;

	private List<ManualPrescSummary> manualPrescRptSummaryList;
	
	private boolean retrieveSuccess;
	
	private Date rptDateFrom;
	
	private Date rptDateTo;
	
	private DateFormat df = new SimpleDateFormat("dd-MMM-yyyy"); 
	private DateFormat fileNameDf = new SimpleDateFormat("yyyyMMdd"); 
	
	private String exportPassword;
	
	private final static String MANUAL_PRESC_RPT_FILENAME = "ManualRx_details";
	private final static String MANUAL_PRESC_SUMMARY_RPT_FILENAME = "ManualRx_summary_";
	
	@SuppressWarnings("unchecked")
	public void retrieveManualPrescRptList(Date dateFrom, String password, Boolean exportCurrentMonth){
		exportPassword = password;
		rptDateFrom = dateFrom;
		rptDateTo = getDateTo(dateFrom);

		retrieveSuccess = false;
		
		if (exportCurrentMonth) {
			rptDateTo = new Date();
		}
		
		manualPrescRptList = new ArrayList<ManualPrescRpt>();

		List<DispOrder> dispOrderList = em.createQuery(
				"select o from DispOrder o" + // 20120305 index check : MedOrder.orderType,orderDate : FK_MED_ORDER_06
				" where o.pharmOrder.medOrder.workstore.hospCode = :hospCode" +
				" and o.pharmOrder.medOrder.orderType = :orderType" +
				" and o.pharmOrder.medOrder.orderDate between :orderDateFrom and :orderDateTo" +
				" and o.pharmOrder.medOrder.docType = :docType" +
				" and o.pharmOrder.medOrder.prescType not in :prescType" +
				" and o.status = :dispOrderStatus" +
				" and o.adminStatus = :dispOrderAdminStatus" +
				" order by" +
				" o.pharmOrder.medOrder.patHospCode," +
				" o.pharmOrder.medOrder.orderDate," +
				" o.pharmOrder.medOrder.orderNum" )
				.setParameter("hospCode", workstore.getHospCode())
				.setParameter("orderType", OrderType.OutPatient)
				.setParameter("orderDateFrom", rptDateFrom)
				.setParameter("orderDateTo", rptDateTo)
				.setParameter("docType", MedOrderDocType.Manual)
				.setParameter("prescType", Arrays.asList(MedOrderPrescType.Dh))
				.setParameter("dispOrderStatus", DispOrderStatus.Issued)
				.setParameter("dispOrderAdminStatus", DispOrderAdminStatus.Normal)
				.getResultList();
		
		if ( !dispOrderList.isEmpty() ) {
			retrieveSuccess = true;
			for (DispOrder dispOrder : dispOrderList) {
				Patient patient = dispOrder.getPharmOrder().getPatient();
				MedOrder medOrder = sortMedOrderItemListByItemNum(dispOrder.getPharmOrder().getMedOrder());
				MedCase medCase = dispOrder.getPharmOrder().getMedCase();
				
				for ( MedOrderItem medOrderItem : medOrder.getMedOrderItemList() ) {
					if ( medOrderItem.getCapdRxDrug() != null ) {
						CapdRxDrug capdRxDrug = medOrderItem.getCapdRxDrug();
						for ( CapdItem capdItem : capdRxDrug.getCapdItemList() ) {
							ManualPrescRpt manualPrescRpt = new ManualPrescRpt();
							convertManualPrescRptMedOrderItem(manualPrescRpt, medOrderItem, dispOrder.getTicket(), dispOrder.getPharmOrder().getDoctorCode(), patient);
							convertManualPrescRptCapd(manualPrescRpt, capdItem, capdRxDrug);
							manualPrescRpt.setWorkstationCode(dispOrder.getPharmOrder().getWorkstationCode());
					
							if (medCase != null) {
								manualPrescRpt.setCaseNum(medCase.getCaseNum());
								manualPrescRpt.setPasSpecCode(medCase.getPasSpecCode());
								manualPrescRpt.setPasSubSpecCode(medCase.getPasSubSpecCode());
								
								if ( medCase.getPasApptSeq() != null && medCase.getPasApptSeq()!=-1 ) {
									manualPrescRpt.setPasApptSeq(medCase.getPasApptSeq());
								}
							}
							manualPrescRpt.setWardCode(dispOrder.getWardCode());
							
							manualPrescRptList.add(manualPrescRpt);
						}
					} else {
						if ( medOrderItem.getRegimen() != null ) {
							for ( DoseGroup doseGroup : medOrderItem.getRegimen().getDoseGroupList() ) {
								for ( Dose dose : doseGroup.getDoseList() ) {
									ManualPrescRpt manualPrescRpt = new ManualPrescRpt();
									convertManualPrescRptMedOrderItem(manualPrescRpt, medOrderItem, dispOrder.getTicket(), dispOrder.getPharmOrder().getDoctorCode(), patient);
									convertManualPrescRptNonCapd(manualPrescRpt, dose, doseGroup);
									manualPrescRpt.setWorkstationCode(dispOrder.getPharmOrder().getWorkstationCode());
									
									if (medCase != null) {
										manualPrescRpt.setCaseNum(medCase.getCaseNum());
										manualPrescRpt.setPasSpecCode(medCase.getPasSpecCode());
										manualPrescRpt.setPasSubSpecCode(medCase.getPasSubSpecCode());
										
										if ( medCase.getPasApptSeq() != null && medCase.getPasApptSeq()!=-1 ) {
											manualPrescRpt.setPasApptSeq(medCase.getPasApptSeq());
										}
									}
									manualPrescRpt.setWardCode(dispOrder.getWardCode());
									
									manualPrescRptList.add(manualPrescRpt);
								}
							}
						}
					}
				}				
			}
		}
	}
	
	private void convertManualPrescRptNonCapd(ManualPrescRpt manualPrescRpt, Dose dose, DoseGroup doseGroup){
		manualPrescRpt.setDuration(doseGroup.getDuration());
		manualPrescRpt.setDurationUnit(doseGroup.getDurationUnit());
		
		manualPrescRpt.setDosage(dose.getDosage());
		manualPrescRpt.setDosageUnit(dose.getDosageUnit());
		manualPrescRpt.setPrn(dose.getPrn());
		manualPrescRpt.setPrnPercent(dose.getPrnPercent());
		manualPrescRpt.setSiteCode(dose.getSiteCode());
		manualPrescRpt.setSupplSiteDesc(dose.getSupplSiteDesc());
		manualPrescRpt.setAdminTimeDesc(dose.getAdminTimeDesc());
		manualPrescRpt.setDispQty(dose.getDispQty());
		manualPrescRpt.setDoseBaseUnit(dose.getBaseUnit());
		
		if ( dose.getDailyFreq() != null ){
			manualPrescRpt.setDailyFreqDesc(dose.getDailyFreq().getDesc());
		}
		
		if ( dose.getSupplFreq() != null ) {
			manualPrescRpt.setSupplFreqDesc(dose.getSupplFreq().getDesc());
		}
		
		if ( doseGroup.getDoseList().size()>1 ) {
			manualPrescRpt.setMultDoseFlag(true);
		}
	}
	
	private void convertManualPrescRptCapd(ManualPrescRpt manualPrescRpt, CapdItem capdItem, CapdRxDrug capdRxDrug){
		manualPrescRpt.setSupplierSystem(capdRxDrug.getSupplierSystem());
		manualPrescRpt.setCalciumStrength(capdRxDrug.getCalciumStrength());
		
		manualPrescRpt.setFullConcentration(capdItem.getFullConcentration());
		manualPrescRpt.setCapdBaseUnit(capdItem.getBaseUnit());
		
		if ( capdRxDrug.getCapdItemList().size()>1 ) {
			manualPrescRpt.setMultDoseFlag(true);
		}
	}
	
	private void convertManualPrescRptMedOrderItem(ManualPrescRpt manualPrescRpt, MedOrderItem medOrderItem, Ticket ticket, String doctorCode, Patient patient){
		MedOrder medOrder = medOrderItem.getMedOrder();
		
		manualPrescRpt.setHospCode(medOrder.getWorkstore().getHospCode());
		manualPrescRpt.setWorkstoreCode(medOrder.getWorkstore().getWorkstoreCode());
		
		manualPrescRpt.setTicketDate(ticket.getTicketDate());
		manualPrescRpt.setTicketNum(ticket.getTicketNum());
		
		if ( medOrder.getMedCase()!= null ) {		
			manualPrescRpt.setPasWardCode(medOrder.getMedCase().getPasWardCode());
		}
		
		manualPrescRpt.setHkid(patient.getHkid());
		manualPrescRpt.setName(patient.getName());
		
		manualPrescRpt.setOrderDate(medOrder.getOrderDate());
		manualPrescRpt.setOrderNum(medOrder.getOrderNum());
		manualPrescRpt.setPatHospCode(medOrder.getPatHospCode());
		manualPrescRpt.setDoctorCode(doctorCode);
		manualPrescRpt.setMedOrderStatus(medOrder.getStatus());
		manualPrescRpt.setSpecCode(medOrder.getSpecCode());
		
		manualPrescRpt.setItemCode(medOrderItem.getItemCode());
		manualPrescRpt.setDisplayName(medOrderItem.getDisplayName());
		manualPrescRpt.setSaltProperty(medOrderItem.getSaltProperty());
		manualPrescRpt.setFormCode(medOrderItem.getFormCode());
		manualPrescRpt.setFormDesc(medOrderItem.getFormDesc());
		manualPrescRpt.setItemNum(medOrderItem.getItemNum());
		manualPrescRpt.setFmStatus(medOrderItem.getFmStatus());
		manualPrescRpt.setDangerDrugFlag(medOrderItem.getDangerDrugFlag());
		manualPrescRpt.setSingleUseFlag(medOrderItem.getSingleUseFlag());
		manualPrescRpt.setActionStatus(medOrderItem.getActionStatus());
		manualPrescRpt.setStrength(medOrderItem.getStrength());
		manualPrescRpt.setVolume(medOrderItem.getVolume());
		manualPrescRpt.setVolumeUnit(medOrderItem.getVolumeUnit());
		manualPrescRpt.setExtraInfo(medOrderItem.getExtraInfo());
		manualPrescRpt.setSpecialInstruction(medOrderItem.getSpecialInstruction());
		manualPrescRpt.setRouteDesc(medOrderItem.getRouteDesc());
		manualPrescRpt.setTradeName(medOrderItem.getTradeName());
		manualPrescRpt.setExternalUseFlag(medOrderItem.getExternalUseFlag());
		
		manualPrescRpt.setAlertFlag(medOrderItem.getAlertFlag());
		
		manualPrescRpt.setUpdateBy(medOrderItem.getUpdateUser());
		manualPrescRpt.setUpdateDate(medOrderItem.getUpdateDate());
		manualPrescRpt.setBaseUnit(medOrderItem.getBaseUnit());
	}
	
	@SuppressWarnings("unchecked")
	private MedOrder sortMedOrderItemListByItemNum(MedOrder medOrder){
		Collections.sort(medOrder.getMedOrderItemList(), new Comparator(){
			public int compare(Object o1, Object o2) {
				MedOrderItem moi1 = (MedOrderItem) o1;
				MedOrderItem moi2 = (MedOrderItem) o2;
				return moi1.getItemNum().compareTo(moi2.getItemNum());
			}
		});	
		return medOrder;
	}
    
    @SuppressWarnings("unchecked")
	public void retrieveManualPrescSummaryList(Date dateFrom, String password, Boolean exportCurrentMonth) {
    	retrieveSuccess = false;
    	
    	exportPassword = password;
		rptDateFrom = dateFrom;
		rptDateTo = getDateTo(dateFrom);
		
		if (exportCurrentMonth) {
			rptDateTo = new Date();
		}
		
		List<MedOrderStatus> medOrderStatusList = new ArrayList<MedOrderStatus>();
		medOrderStatusList.add(MedOrderStatus.Outstanding);
		medOrderStatusList.add(MedOrderStatus.Fulfilling);
		medOrderStatusList.add(MedOrderStatus.Complete);

		manualPrescRptSummaryList = em.createQuery(
				"select new " + MANUAL_PRESC_SUMMARY_CLASS_NAME + // 20120305 index check : MedOrder.orderType,orderDate : FK_MED_ORDER_06
				"  (o.pharmOrder.medOrder.workstore.hospCode, o.pharmOrder.medOrder.specCode, count(o)) " +
				" from DispOrder o" +
				" where o.pharmOrder.medOrder.workstore.hospCode = :hospCode" +
				" and o.pharmOrder.medOrder.orderType = :orderType" +
				" and o.pharmOrder.medOrder.prescType not in :prescType" +
				" and o.pharmOrder.medOrder.orderDate between :orderDateFrom and :orderDateTo" +
				" and o.pharmOrder.medOrder.docType = :docType" +
				" and o.status = :dispOrderStatus" +
				" and o.adminStatus = :dispOrderAdminStatus" +
				" group by o.pharmOrder.medOrder.workstore.hospCode, o.pharmOrder.medOrder.specCode" +
				" order by o.pharmOrder.medOrder.workstore.hospCode, o.pharmOrder.medOrder.specCode")
				.setParameter("hospCode", workstore.getHospCode())
				.setParameter("orderType", OrderType.OutPatient)
				.setParameter("prescType", Arrays.asList(MedOrderPrescType.Dh))
				.setParameter("orderDateFrom", rptDateFrom)
				.setParameter("orderDateTo", rptDateTo)
				.setParameter("docType", MedOrderDocType.Manual)
				.setParameter("dispOrderStatus", DispOrderStatus.Issued)
				.setParameter("dispOrderAdminStatus", DispOrderAdminStatus.Normal)
				.getResultList();

    	if ( !manualPrescRptSummaryList.isEmpty() ) {
    		retrieveSuccess = true;
    	}
    }

	private Date getDateTo(Date dateFrom){
    	Calendar dateFromCal = Calendar.getInstance();
    	dateFromCal.setTime(dateFrom);

    	int maxDay = dateFromCal.getActualMaximum(Calendar.DAY_OF_MONTH);

    	Calendar dateToCal = Calendar.getInstance();
    	dateToCal.set(dateFromCal.get(Calendar.YEAR), dateFromCal.get(Calendar.MONTH), maxDay);
    	
    	return dateToCal.getTime();
    }
	
	private String getRptFileName(String rptName, Date rptDate){
		return rptName+"("+fileNameDf.format(rptDate)+")_"+workstore.getHospCode();
	}

	public void exportManualPrescSummaryRpt() throws IOException, ZipException {
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("dateFrom", df.format(rptDateFrom));
		parameters.put("dateTo", df.format(rptDateTo));
		
		String filename = getRptFileName(MANUAL_PRESC_SUMMARY_RPT_FILENAME, rptDateFrom);
		
		PrintOption po = new PrintOption();
		po.setDocType(ReportProvider.XLS);
		printAgent.zipAndRedirect(new RenderJob(
				"ManualPrescSummaryXls", 
				po, 
				parameters, 
				manualPrescRptSummaryList), 
				filename, exportPassword);

	}
	
	public void exportManualPrescRpt() throws IOException, ZipException {
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("dateFrom", df.format(rptDateFrom));
		parameters.put("dateTo", df.format(rptDateTo));

		String filename = getRptFileName(MANUAL_PRESC_RPT_FILENAME, rptDateFrom);
		
		PrintOption po = new PrintOption();
		po.setDocType(ReportProvider.XLS);
		printAgent.zipAndRedirect(new RenderJob(
				"ManualPrescXls", 
				po, 
				parameters, 
				manualPrescRptList), 
				filename, exportPassword);

	}
	
	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}
	
	@Remove
	public void destroy() {
		if(manualPrescRptList!=null){
			manualPrescRptList=null;
		}
		
		if ( manualPrescRptSummaryList != null ) {
			manualPrescRptSummaryList = null;
		}
	}

}
