package hk.org.ha.model.pms.biz.checkissue.mp;

import hk.org.ha.model.pms.vo.checkissue.mp.CheckDelivery;
import hk.org.ha.model.pms.vo.checkissue.mp.DrugCheckViewListSummary;

import java.util.Date;

import javax.ejb.Local;

@Local
public interface CheckServiceLocal {
	
	DrugCheckViewListSummary retrieveDelivery(Date batchDate, String batchNum);
	
	DrugCheckViewListSummary convertToCheckDelivery();
	
	DrugCheckViewListSummary updateDeliveryStatusToChecked(CheckDelivery inCheckOrder, boolean printRptFlag);
		
	String getResultMsg();
	
	String getBatchCode();
	
	String getBatchDate();
	
	void destroy();
	
}