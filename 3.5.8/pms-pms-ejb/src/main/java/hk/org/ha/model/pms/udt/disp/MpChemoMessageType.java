package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum MpChemoMessageType implements StringValuedEnum {

	// From MOE Ordering to PMS
	ChangeOrder("C", "changeOrder", "change");
	
	private final String dataValue;
	private final String displayValue;
	private final String packageName;
	
	MpChemoMessageType (final String dataValue, final String displayValue, final String packageName) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
		this.packageName = packageName;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}

	public String getPackageName() {
		return this.packageName;
	}
	
	public static MpChemoMessageType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MpChemoMessageType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<MpChemoMessageType> {

		private static final long serialVersionUID = 1L;

		public Class<MpChemoMessageType> getEnumClass() {
    		return MpChemoMessageType.class;
    	}
    }
}