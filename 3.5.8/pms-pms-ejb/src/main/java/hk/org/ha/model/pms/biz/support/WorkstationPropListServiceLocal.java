package hk.org.ha.model.pms.biz.support;

import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.vo.support.PropInfo;

import java.util.List;

import javax.ejb.Local;

@Local
public interface WorkstationPropListServiceLocal {
	
	void clearWorkstationPropInfoList();
	
	List<Hospital> retrieveHospitalList();
	
	List<String> retrieveHostNameList(String hospCode, String prefixHostName);
	
	void retrieveWorkstationPropList(Hospital hospital, String hostName);
	
	void updateWorkstationPropList(List<PropInfo> propInfoList);
	
	boolean isValidHostName();
	
	void destroy();
	
}