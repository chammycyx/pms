package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.vo.report.EdsStatusSummaryRpt;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

@Local
public interface EdsStatusSummaryRptServiceLocal {

	void retrieveEdsStatusSummaryRpt(Integer selectedTab, Date date);
	
	Map<DispOrderStatus, Long> getDispOrderStatusCount(Date inputDate);
	
	Map<DispOrderItemStatus, Long> getDispOrderItemStatusCount(Date inputDate);
	
	List<EdsStatusSummaryRpt> retrievEdsStatusSummaryRptList(Date date);
	
	void generateEdsStatusSummaryRpt();
	
	void printEdsStatusSummaryRpt();
	
	void destroy();
	
}
