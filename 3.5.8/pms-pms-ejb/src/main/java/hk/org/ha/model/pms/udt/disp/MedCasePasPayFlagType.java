package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum MedCasePasPayFlagType implements StringValuedEnum {

	Blank("-", " "),
	Charge("Y", "charge"),
	NoCharge("N", "No charge"),
	HaStaff("X", "HA");

    private final String dataValue;
    private final String displayValue;

    MedCasePasPayFlagType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static MedCasePasPayFlagType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MedCasePasPayFlagType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<MedCasePasPayFlagType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<MedCasePasPayFlagType> getEnumClass() {
    		return MedCasePasPayFlagType.class;
    	}
    }
}
