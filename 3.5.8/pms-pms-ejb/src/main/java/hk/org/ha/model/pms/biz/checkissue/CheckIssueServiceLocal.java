package hk.org.ha.model.pms.biz.checkissue;

import hk.org.ha.model.pms.vo.checkissue.CheckIssueListSummary;
import hk.org.ha.model.pms.vo.checkissue.CheckIssueOrder;
import hk.org.ha.model.pms.vo.checkissue.CheckIssueSummary;
import hk.org.ha.model.pms.vo.checkissue.CheckIssueTicketMatchAuditInfo;
import hk.org.ha.model.pms.vo.checkissue.CheckIssueViewInfo;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface CheckIssueServiceLocal {
		
	CheckIssueSummary retrievePrescDetail(CheckIssueViewInfo checkIssueViewInfo);

	CheckIssueSummary updateDispOrderStatusToChecked(CheckIssueViewInfo checkIssueViewInfo, boolean reverseUncollect);

	CheckIssueSummary updateDispOrderStatusToIssued(CheckIssueViewInfo checkIssueViewInfo, boolean reverseUncollect);
	
	CheckIssueListSummary refreshCheckIssueList(boolean refreshIssuedList, Date checkIssueListDate, Date otherCheckListDate, Date issuedListDate);
	
	CheckIssueListSummary retrieveCheckIssueList(Date checkIssueListDate);
	
	List<CheckIssueOrder> retrieveOtherCheckList(Date ticketDate);
	
	List<CheckIssueOrder> retrieveIssuedList(Date ticketDate);
	
	CheckIssueSummary reverseUncollectPrescriptionByPropup(CheckIssueViewInfo checkIssueViewInfo);
	
	void writeCancelTicketMatchAuditMessage(CheckIssueTicketMatchAuditInfo ticketMatchAuditInfo);
	
	boolean isUpdateSuccess();
	
	String getErrMsg();
	
	String getAction();
	
	void destroy();
	
}