package hk.org.ha.model.pms.vo.machine;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "AtdpsMessage")
@ExternalizedBean(type=DefaultExternalizer.class)
public class AtdpsMessage implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@XmlElement(required = true)
	private String hospCode;
	
	@XmlElement(required = false)
	private String hospName;
	
	@XmlElement(required = true)
	private String workstoreCode;
	
	@XmlElement(required = true)
	private String workstationCode;
	
	@XmlElement(required = true)
	private String printLang;
	
	@XmlElement(required = true)
	private String itemNum;
	
	@XmlElement(required = true)
	private String itemCode;
	
	@XmlElement(required = false)
	private String itemDesc;
	
	@XmlElement(required = false)
	private String tradeName;
	
	@XmlElement(required = false)
	private String instruction;
	
	@XmlElement(required = true)
	private String formCode;
	
	@XmlElement(required = true)
	private String dispDate;
	
	@XmlElement(required = false)
	private String patName;
	
	@XmlElement(required = false)
	private String hkid;
	
	@XmlElement(required = false)
	private String caseNum;
	
	@XmlElement(required = false)
	private String wardCode;
	
	@XmlElement(required = true)
	private boolean refillFlag;
	
	@XmlElement(required = false)
	private String specCode;
	
	@XmlElement(required = false)
	private String bedNum;
	
	@XmlElement(required = true)
	private String userCode;
	
	@XmlElement(required = false)
	private String dosage;

	@XmlElement(required = false)
	private String issueDuration;
	
	@XmlElement(required = false)
	private String durationUnit;
	
	@XmlElement(required = false)
	private String issueQty;
	
	@XmlElement(required = false)
	private String baseunit;
	
	@XmlElement(required = false)
	private String warnDesc1;
	
	@XmlElement(required = false)
	private String warnDesc2;
	
	@XmlElement(required = false)
	private String warnDesc3;
	
	@XmlElement(required = false)
	private String warnDesc4;
	
	@XmlElement(required = true)
	private boolean fdnFlag;
	
	@XmlElement(required = false)
	private String binNum;
	
	@XmlElement(required = true)
	private boolean printInstructFlag;
	
	@XmlElement(required = true)
	private boolean printWardReturnFlag;
	
	@XmlElement(required = true)
	private String updateDate;
	
	@XmlElement(required = true)
	private String mpDispLabelBarcodeInfo;
	
	@XmlElement(required = false)
	private String freqCode;
	
	@XmlElement(required = false)
	private String supplFreqCode;
	
	@XmlElement(required = false)
	private String mealTimeInfo;
	
	@XmlElement(required = true)
	private boolean urgentFlag;
	
	@XmlElement(required = true)
	private String batchCode;
	
	@XmlElement(required = true)
	private boolean fmFlag;
	
	@XmlElement(required = false)
	private String labelIndicator;
	
	@XmlElement(required = true)
	private boolean reprintFlag;
	
	@XmlElement(required = true)
	private boolean fractionDoseFlag;
	
	@XmlElement(required = true)
	private boolean prnFlag;
	
	@XmlElement(required = true)
	private String verifyUser;
	
	@XmlElement(required = true)
	private String adjQty;
	
	@XmlElement(required = false)
	private boolean printOverrideIndicatorFlag;
	
	@XmlElement(required = true)
	private boolean multipleDoseFlag;
	
	@XmlElement(required = false)
	private String pasPayCode;
	
	@XmlElement(required = true)
	private String actionStatus;
	
	@XmlElement(required = false)
	private Integer pickStationNum;
	
	@XmlTransient
	private String hostname;
	
	@XmlTransient
	private int portNum;
	
	@XmlTransient
	private Long trxId;

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public int getPortNum() {
		return portNum;
	}

	public void setPortNum(int portNum) {
		this.portNum = portNum;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getHospName() {
		return hospName;
	}

	public void setHospName(String hospName) {
		this.hospName = hospName;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public String getWorkstationCode() {
		return workstationCode;
	}

	public void setWorkstationCode(String workstationCode) {
		this.workstationCode = workstationCode;
	}

	public String getPrintLang() {
		return printLang;
	}

	public void setPrintLang(String printLang) {
		this.printLang = printLang;
	}

	public String getItemNum() {
		return itemNum;
	}

	public void setItemNum(String itemNum) {
		this.itemNum = itemNum;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemDesc() {
		return itemDesc;
	}

	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	public String getTradeName() {
		return tradeName;
	}

	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}

	public String getInstruction() {
		return instruction;
	}

	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}

	public String getFormCode() {
		return formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	public String getDispDate() {
		return dispDate;
	}

	public void setDispDate(String dispDate) {
		this.dispDate = dispDate;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public boolean isRefillFlag() {
		return refillFlag;
	}

	public void setRefillFlag(boolean refillFlag) {
		this.refillFlag = refillFlag;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public String getBedNum() {
		return bedNum;
	}

	public void setBedNum(String bedNum) {
		this.bedNum = bedNum;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getDosage() {
		return dosage;
	}

	public void setDosage(String dosage) {
		this.dosage = dosage;
	}

	public String getIssueDuration() {
		return issueDuration;
	}

	public void setIssueDuration(String issueDuration) {
		this.issueDuration = issueDuration;
	}

	public String getDurationUnit() {
		return durationUnit;
	}

	public void setDurationUnit(String durationUnit) {
		this.durationUnit = durationUnit;
	}

	public String getIssueQty() {
		return issueQty;
	}

	public void setIssueQty(String issueQty) {
		this.issueQty = issueQty;
	}

	public String getBaseunit() {
		return baseunit;
	}

	public void setBaseunit(String baseunit) {
		this.baseunit = baseunit;
	}

	public String getWarnDesc1() {
		return warnDesc1;
	}

	public void setWarnDesc1(String warnDesc1) {
		this.warnDesc1 = warnDesc1;
	}

	public String getWarnDesc2() {
		return warnDesc2;
	}

	public void setWarnDesc2(String warnDesc2) {
		this.warnDesc2 = warnDesc2;
	}

	public String getWarnDesc3() {
		return warnDesc3;
	}

	public void setWarnDesc3(String warnDesc3) {
		this.warnDesc3 = warnDesc3;
	}

	public String getWarnDesc4() {
		return warnDesc4;
	}

	public void setWarnDesc4(String warnDesc4) {
		this.warnDesc4 = warnDesc4;
	}

	public boolean isFdnFlag() {
		return fdnFlag;
	}

	public void setFdnFlag(boolean fdnFlag) {
		this.fdnFlag = fdnFlag;
	}

	public String getBinNum() {
		return binNum;
	}

	public void setBinNum(String binNum) {
		this.binNum = binNum;
	}

	public boolean isPrintInstructFlag() {
		return printInstructFlag;
	}

	public void setPrintInstructFlag(boolean printInstructFlag) {
		this.printInstructFlag = printInstructFlag;
	}

	public boolean isPrintWardReturnFlag() {
		return printWardReturnFlag;
	}

	public void setPrintWardReturnFlag(boolean printWardReturnFlag) {
		this.printWardReturnFlag = printWardReturnFlag;
	}

	public String getFreqCode() {
		return freqCode;
	}

	public void setFreqCode(String freqCode) {
		this.freqCode = freqCode;
	}

	public String getSupplFreqCode() {
		return supplFreqCode;
	}

	public void setSupplFreqCode(String supplFreqCode) {
		this.supplFreqCode = supplFreqCode;
	}

	public String getMealTimeInfo() {
		return mealTimeInfo;
	}

	public void setMealTimeInfo(String mealTimeInfo) {
		this.mealTimeInfo = mealTimeInfo;
	}

	public boolean isUrgentFlag() {
		return urgentFlag;
	}

	public void setUrgentFlag(boolean urgentFlag) {
		this.urgentFlag = urgentFlag;
	}

	public String getBatchCode() {
		return batchCode;
	}

	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}

	public boolean isFmFlag() {
		return fmFlag;
	}

	public void setFmFlag(boolean fmFlag) {
		this.fmFlag = fmFlag;
	}

	public String getLabelIndicator() {
		return labelIndicator;
	}

	public void setLabelIndicator(String labelIndicator) {
		this.labelIndicator = labelIndicator;
	}

	public boolean isReprintFlag() {
		return reprintFlag;
	}

	public void setReprintFlag(boolean reprintFlag) {
		this.reprintFlag = reprintFlag;
	}

	public boolean isFractionDoseFlag() {
		return fractionDoseFlag;
	}

	public void setFractionDoseFlag(boolean fractionDoseFlag) {
		this.fractionDoseFlag = fractionDoseFlag;
	}

	public boolean isPrnFlag() {
		return prnFlag;
	}

	public void setPrnFlag(boolean prnFlag) {
		this.prnFlag = prnFlag;
	}

	public String getVerifyUser() {
		return verifyUser;
	}

	public void setVerifyUser(String verifyUser) {
		this.verifyUser = verifyUser;
	}

	public String getAdjQty() {
		return adjQty;
	}

	public void setAdjQty(String adjQty) {
		this.adjQty = adjQty;
	}

	public boolean isPrintOverrideIndicatorFlag() {
		return printOverrideIndicatorFlag;
	}

	public void setPrintOverrideIndicatorFlag(boolean printOverrideIndicatorFlag) {
		this.printOverrideIndicatorFlag = printOverrideIndicatorFlag;
	}

	public boolean isMultipleDoseFlag() {
		return multipleDoseFlag;
	}

	public void setMultipleDoseFlag(boolean multipleDoseFlag) {
		this.multipleDoseFlag = multipleDoseFlag;
	}

	public String getPasPayCode() {
		return pasPayCode;
	}

	public void setPasPayCode(String pasPayCode) {
		this.pasPayCode = pasPayCode;
	}

	public String getActionStatus() {
		return actionStatus;
	}

	public void setActionStatus(String actionStatus) {
		this.actionStatus = actionStatus;
	}

	public Integer getPickStationNum() {
		return pickStationNum;
	}

	public void setPickStationNum(Integer pickStationNum) {
		this.pickStationNum = pickStationNum;
	}

	public Long getTrxId() {
		return trxId;
	}

	public void setTrxId(Long trxId) {
		this.trxId = trxId;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setMpDispLabelBarcodeInfo(String mpDispLabelBarcodeInfo) {
		this.mpDispLabelBarcodeInfo = mpDispLabelBarcodeInfo;
	}

	public String getMpDispLabelBarcodeInfo() {
		return mpDispLabelBarcodeInfo;
	}

}
