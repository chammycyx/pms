package hk.org.ha.model.pms.persistence.pivas;

import hk.org.ha.model.pms.dms.persistence.Company;
import hk.org.ha.model.pms.dms.persistence.DmDiluent;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaMethodDrug;
import hk.org.ha.model.pms.persistence.VersionEntity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "PIVAS_BATCH_PREP_ITEM")
public class PivasBatchPrepItem extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pivasBatchPrepItemSeq")
	@SequenceGenerator(name = "pivasBatchPrepItemSeq", sequenceName = "SQ_PIVAS_BATCH_PREP_ITEM", initialValue = 100000000)
	private Long id;
	
	@Column(name = "DRUG_KEY", nullable = false)
	private Integer drugKey;
	
	@Column(name = "DOSAGE_QTY", precision = 19, scale = 4)
	private BigDecimal dosageQty;
	
	@Column(name = "PIVAS_DOSAGE_UNIT", nullable = false, length = 15)
	private String pivasDosageUnit;
	
	@Column(name = "DRUG_ITEM_CODE", length = 20)
	private String drugItemCode;

	@Column(name = "DRUG_MANUF_CODE", length = 20)
	private String drugManufCode;
	
	@Column(name = "DRUG_BATCH_NUM", length = 20)
	private String drugBatchNum;
	
	@Column(name = "DRUG_EXPIRY_DATE")
	@Temporal(TemporalType.DATE)
	private Date drugExpiryDate;

	@Column(name = "SOLVENT_CODE", length = 25)
	private String solventCode;
	
	@Column(name = "SOLVENT_DESC", length = 100)
	private String solventDesc;
	
	@Column(name = "SOLVENT_ITEM_CODE", length = 20)
	private String solventItemCode;

	@Column(name = "SOLVENT_MANUF_CODE", length = 20)
	private String solventManufCode;
	
	@Column(name = "SOLVENT_BATCH_NUM", length = 20)
	private String solventBatchNum;
	
	@Column(name = "SOLVENT_EXPIRY_DATE")
	@Temporal(TemporalType.DATE)
	private Date solventExpiryDate;
	
	@Column(name = "PIVAS_FORMULA_METHOD_DRUG_ID", nullable = false)
	private Long pivasFormulaMethodDrugId;
	
	@Column(name = "AFTER_CONCN", precision = 19, scale = 4)
	private BigDecimal afterConcn;
	
	@Column(name = "AFTER_VOLUME", precision = 19, scale = 4)
	private BigDecimal afterVolume;
	
	@Column(name = "PIVAS_BATCH_PREP_LOT_NUM", nullable = false, insertable = false, updatable = false)
	private String pivasBatchPrepLotNum;
	
	@ManyToOne
	@JoinColumn(name = "PIVAS_BATCH_PREP_LOT_NUM", nullable = false)
	private PivasBatchPrep pivasBatchPrep;

	@Transient
	private Company solventCompany;

	@Transient
	private DmDrug solventDmDrug;

	@Transient
	private DmDiluent solventDmDiluent;

	@Transient
	private PivasFormulaMethodDrug pivasFormulaMethodDrug;
	
	public PivasBatchPrepItem() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getDrugKey() {
		return drugKey;
	}

	public void setDrugKey(Integer drugKey) {
		this.drugKey = drugKey;
	}

	public BigDecimal getDosageQty() {
		return dosageQty;
	}

	public void setDosageQty(BigDecimal dosageQty) {
		this.dosageQty = dosageQty;
	}

	public String getPivasDosageUnit() {
		return pivasDosageUnit;
	}

	public void setPivasDosageUnit(String pivasDosageUnit) {
		this.pivasDosageUnit = pivasDosageUnit;
	}

	public String getDrugItemCode() {
		return drugItemCode;
	}

	public void setDrugItemCode(String drugItemCode) {
		this.drugItemCode = drugItemCode;
	}

	public String getDrugManufCode() {
		return drugManufCode;
	}

	public void setDrugManufCode(String drugManufCode) {
		this.drugManufCode = drugManufCode;
	}

	public String getDrugBatchNum() {
		return drugBatchNum;
	}

	public void setDrugBatchNum(String drugBatchNum) {
		this.drugBatchNum = drugBatchNum;
	}

	public Date getDrugExpiryDate() {
		return drugExpiryDate;
	}

	public void setDrugExpiryDate(Date drugExpiryDate) {
		this.drugExpiryDate = drugExpiryDate;
	}

	public String getSolventCode() {
		return solventCode;
	}

	public void setSolventCode(String solventCode) {
		this.solventCode = solventCode;
	}

	public String getSolventDesc() {
		return solventDesc;
	}

	public void setSolventDesc(String solventDesc) {
		this.solventDesc = solventDesc;
	}

	public String getSolventItemCode() {
		return solventItemCode;
	}

	public void setSolventItemCode(String solventItemCode) {
		this.solventItemCode = solventItemCode;
	}

	public String getSolventManufCode() {
		return solventManufCode;
	}

	public void setSolventManufCode(String solventManufCode) {
		this.solventManufCode = solventManufCode;
	}

	public String getSolventBatchNum() {
		return solventBatchNum;
	}

	public void setSolventBatchNum(String solventBatchNum) {
		this.solventBatchNum = solventBatchNum;
	}

	public Date getSolventExpiryDate() {
		return solventExpiryDate;
	}

	public void setSolventExpiryDate(Date solventExpiryDate) {
		this.solventExpiryDate = solventExpiryDate;
	}

	public Long getPivasFormulaMethodDrugId() {
		return pivasFormulaMethodDrugId;
	}

	public void setPivasFormulaMethodDrugId(Long pivasFormulaMethodDrugId) {
		this.pivasFormulaMethodDrugId = pivasFormulaMethodDrugId;
	}

	public BigDecimal getAfterConcn() {
		return afterConcn;
	}

	public void setAfterConcn(BigDecimal afterConcn) {
		this.afterConcn = afterConcn;
	}

	public BigDecimal getAfterVolume() {
		return afterVolume;
	}

	public void setAfterVolume(BigDecimal afterVolume) {
		this.afterVolume = afterVolume;
	}
	
	public String getPivasBatchPrepLotNum() {
		return pivasBatchPrepLotNum;
	}

	public void setPivasBatchPrepLotNum(String pivasBatchPrepLotNum) {
		this.pivasBatchPrepLotNum = pivasBatchPrepLotNum;
	}

	public PivasBatchPrep getPivasBatchPrep() {
		return pivasBatchPrep;
	}

	public void setPivasBatchPrep(PivasBatchPrep pivasBatchPrep) {
		this.pivasBatchPrep = pivasBatchPrep;
	}

	public Company getSolventCompany() {
		return solventCompany;
	}

	public void setSolventCompany(Company solventCompany) {
		this.solventCompany = solventCompany;
	}

	public DmDrug getSolventDmDrug() {
		return solventDmDrug;
	}

	public void setSolventDmDrug(DmDrug solventDmDrug) {
		this.solventDmDrug = solventDmDrug;
	}

	public DmDiluent getSolventDmDiluent() {
		return solventDmDiluent;
	}

	public void setSolventDmDiluent(DmDiluent solventDmDiluent) {
		this.solventDmDiluent = solventDmDiluent;
	}

	public PivasFormulaMethodDrug getPivasFormulaMethodDrug() {
		return pivasFormulaMethodDrug;
	}

	public void setPivasFormulaMethodDrug(
			PivasFormulaMethodDrug pivasFormulaMethodDrug) {
		this.pivasFormulaMethodDrug = pivasFormulaMethodDrug;
	}
}
