package hk.org.ha.model.pms.biz.support;

import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.reftable.CorporateProp;
import hk.org.ha.model.pms.persistence.reftable.HospitalProp;
import hk.org.ha.model.pms.persistence.reftable.OperationProp;
import hk.org.ha.model.pms.persistence.reftable.Prop;
import hk.org.ha.model.pms.persistence.reftable.WorkstationProp;
import hk.org.ha.model.pms.persistence.reftable.WorkstoreProp;

import java.util.List;

import javax.ejb.Local;

@Local
public interface PropManagerLocal  {
	
	List<CorporateProp> retrieveCorporatePropList();

	List<HospitalProp> retrieveHospitalPropList();
	
	List<WorkstoreProp> retrieveWorkstorePropList(Hospital hospital);
	
	List<Prop> retrievePropListByWorkstationPropName();
	
	List<WorkstationProp> retrieveWorkstationPropListByHostName(String hospCode, String hostName);
	
	List<OperationProp> retrieveOperationPropList(Long operationProfileId);
	
	void updateCorporatePropList(List<CorporateProp> corporatePropList);
	
	void updateHospitalPropList(List<HospitalProp> hospitalPropList);
	
	void updateWorkstorePropList(List<WorkstoreProp> workstorePropList);
	
	void updateWorkstationPropList(List<WorkstationProp> workstationPropList);
	
	void updateOperationPropList(List<OperationProp> operationPropList);
}
