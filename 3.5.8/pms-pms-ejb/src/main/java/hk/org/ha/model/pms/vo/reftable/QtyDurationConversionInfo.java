package hk.org.ha.model.pms.vo.reftable;

import hk.org.ha.model.pms.dms.persistence.PmsDurationConversion;
import hk.org.ha.model.pms.dms.persistence.PmsQtyConversion;
import hk.org.ha.model.pms.dms.persistence.PmsQtyConversionAdj;
import hk.org.ha.model.pms.dms.persistence.PmsQtyConversionInsu;
import hk.org.ha.model.pms.persistence.reftable.RefillSelection;
import hk.org.ha.model.pms.vo.drug.DmDrugLite;
import hk.org.ha.model.pms.vo.patient.PasSpecialty;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
import org.jboss.seam.annotations.Out;

@ExternalizedBean(type=DefaultExternalizer.class)
public class QtyDurationConversionInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<PmsQtyConversion> qtyConversionList;

	private List<PmsQtyConversionItem> qtyConversionOtherWorkstoreList;
	
	private List<PmsDurationConversion> durationConversionList;
	
	private List<PmsDurationConversionItem> durationConversionOtherWorkstoreList;

	private DmDrugLite qtyDurationConversionDmDrugLite;

	public List<PmsQtyConversion> getQtyConversionList() {
		return qtyConversionList;
	}

	public void setQtyConversionList(List<PmsQtyConversion> qtyConversionList) {
		this.qtyConversionList = qtyConversionList;
	}

	public List<PmsQtyConversionItem> getQtyConversionOtherWorkstoreList() {
		return qtyConversionOtherWorkstoreList;
	}

	public void setQtyConversionOtherWorkstoreList(
			List<PmsQtyConversionItem> qtyConversionOtherWorkstoreList) {
		this.qtyConversionOtherWorkstoreList = qtyConversionOtherWorkstoreList;
	}

	public List<PmsDurationConversion> getDurationConversionList() {
		return durationConversionList;
	}

	public void setDurationConversionList(
			List<PmsDurationConversion> durationConversionList) {
		this.durationConversionList = durationConversionList;
	}

	public List<PmsDurationConversionItem> getDurationConversionOtherWorkstoreList() {
		return durationConversionOtherWorkstoreList;
	}

	public void setDurationConversionOtherWorkstoreList(
			List<PmsDurationConversionItem> durationConversionOtherWorkstoreList) {
		this.durationConversionOtherWorkstoreList = durationConversionOtherWorkstoreList;
	}

	public DmDrugLite getQtyDurationConversionDmDrugLite() {
		return qtyDurationConversionDmDrugLite;
	}

	public void setQtyDurationConversionDmDrugLite(
			DmDrugLite qtyDurationConversionDmDrugLite) {
		this.qtyDurationConversionDmDrugLite = qtyDurationConversionDmDrugLite;
	}

}