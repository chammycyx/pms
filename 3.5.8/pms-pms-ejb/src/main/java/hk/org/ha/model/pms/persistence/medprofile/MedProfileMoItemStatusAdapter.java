package hk.org.ha.model.pms.persistence.medprofile;

import hk.org.ha.model.pms.udt.medprofile.MedProfileMoItemStatus;

import java.io.Serializable;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class MedProfileMoItemStatusAdapter extends XmlAdapter<String, MedProfileMoItemStatus> implements Serializable {

	private static final long serialVersionUID = 1L;

	public String marshal( MedProfileMoItemStatus v ) {		
		return (v == null) ? null : v.getDataValue();
	}
	
	public MedProfileMoItemStatus unmarshal( String v ) {		
		return MedProfileMoItemStatus.dataValueOf(v);
	}
}