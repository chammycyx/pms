package hk.org.ha.model.pms.udt.pivas;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PivasBatchPrepStatus implements StringValuedEnum {

	Printed("P", "Printed"),
	Deleted("D", "Deleted");
	
    private final String dataValue;
    private final String displayValue;

    PivasBatchPrepStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    } 

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static PivasBatchPrepStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PivasBatchPrepStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<PivasBatchPrepStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<PivasBatchPrepStatus> getEnumClass() {
    		return PivasBatchPrepStatus.class;
    	}
    }
}



