package hk.org.ha.model.pms.persistence.reftable;

import flexjson.JSON;
import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.corp.cache.DmWarningCacher;
import hk.org.ha.model.pms.dms.persistence.DmWarning;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.Customizer;
import org.jboss.seam.security.Identity;

@Entity
@Table(name = "LOCAL_WARNING")
@Customizer(AuditCustomizer.class)
public class LocalWarning extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "localWarningSeq")
	@SequenceGenerator(name = "localWarningSeq", sequenceName = "SQ_LOCAL_WARNING", initialValue = 100000000)
	private Long id;

	@Column(name = "ITEM_CODE", nullable = false, length = 6)
	private String itemCode;

	@Column(name = "WARN_CODE", length = 2)
	private String warnCode;
	
	@Column(name = "SORT_SEQ", length = 1)
	private Integer sortSeq;
	
	@Column(name = "ORG_WARN_CODE", length = 2)
	private String orgWarnCode;
	
	@Column(name = "ORG_WARN_CAT", length = 2)
	private String orgWarnCat;

	@ManyToOne
	@JoinColumn(name = "WORKSTORE_GROUP_CODE", nullable = false)
	private WorkstoreGroup workstoreGroup;

	@Transient
	private DmWarning dmWarning;	
	
	@Transient
	private boolean markDelete = false;	

	@PostLoad
	public void postLoad() {
		if (warnCode != null && Identity.instance().isLoggedIn()) {
			dmWarning = DmWarningCacher.instance().getDmWarningByWarnCode(warnCode);
		}
	}		
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getWarnCode() {
		return warnCode;
	}

	public void setWarnCode(String warnCode) {
		this.warnCode = warnCode;
	}

	public Integer getSortSeq() {
		return sortSeq;
	}

	public void setSortSeq(Integer sortSeq) {
		this.sortSeq = sortSeq;
	}

	public String getOrgWarnCode() {
		return orgWarnCode;
	}

	public void setOrgWarnCode(String orgWarnCode) {
		this.orgWarnCode = orgWarnCode;
	}

	public WorkstoreGroup getWorkstoreGroup() {
		return workstoreGroup;
	}

	public void setWorkstoreGroup(WorkstoreGroup workstoreGroup) {
		this.workstoreGroup = workstoreGroup;
	}

	public void setDmWarning(DmWarning dmWarning) {
		this.dmWarning = dmWarning;
	}

	@JSON(include=false)
	public DmWarning getDmWarning() {
		return dmWarning;
	}

	public void setOrgWarnCat(String orgWarnCat) {
		this.orgWarnCat = orgWarnCat;
	}

	public String getOrgWarnCat() {
		return orgWarnCat;
	}

	public void setMarkDelete(boolean markDelete) {
		this.markDelete = markDelete;
	}

	public boolean isMarkDelete() {
		return markDelete;
	}
}
