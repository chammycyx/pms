package hk.org.ha.model.pms.biz.printing;

import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.printing.PrinterSelect;

import javax.ejb.Local;

@Local
public interface PrinterSelectorLocal {
		
	PrinterSelect retrievePrinterSelect(PrintDocType printDocType);
	
}
