package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;

import java.util.List;

import javax.ejb.Local;

@Local
public interface InfusionInstructionSummaryManagerLocal {
	
	boolean checkIfPrintInfusionInstructionSummary(DispOrderItem dispOrderItem);
	
	void prepareInfusionInstructionSummaryPrintJobWithReminder(List<RenderAndPrintJob> printJobList, PrintLang printLang, DispOrder dispOrder);
	
}
