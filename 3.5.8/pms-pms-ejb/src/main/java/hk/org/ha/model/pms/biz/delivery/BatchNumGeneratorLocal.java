package hk.org.ha.model.pms.biz.delivery;
import hk.org.ha.model.pms.persistence.corp.Hospital;

import java.util.Date;

import javax.ejb.Local;

@Local
public interface BatchNumGeneratorLocal {

	public enum BatchType {Normal, Sfi, Pivas};
	
	public String getNextFormattedBatchNum(Hospital hospital, Date batchDate, BatchType batchType);
	public int getNextBatchNum(Hospital hospital, Date batchDate, BatchType batchType);
	
}
