package hk.org.ha.model.pms.vo.label;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Instruction")
@ExternalizedBean(type=DefaultExternalizer.class)
public class Instruction implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@XmlAttribute(required = true)
	private String lang;
	
	@XmlElement(name="line", required = true)
	private List<String> lineList;

	@XmlElement(required = true)
	private String mealTimeDesc;
	
	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public List<String> getLineList() {
		return lineList;
	}

	public void setLineList(List<String> lineList) {
		if( lineList == null ){
			lineList = new ArrayList<String>();			
		}
		this.lineList = lineList;
	}

	public void setMealTimeDesc(String mealTimeDesc) {
		this.mealTimeDesc = mealTimeDesc;
	}

	public String getMealTimeDesc() {
		return mealTimeDesc;
	}
}

