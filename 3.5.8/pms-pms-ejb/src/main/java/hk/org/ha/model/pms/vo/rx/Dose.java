package hk.org.ha.model.pms.vo.rx;

import hk.org.ha.model.pms.corp.cache.DmDailyFrequencyCacher;
import hk.org.ha.model.pms.corp.cache.DmSiteCacher;
import hk.org.ha.model.pms.corp.cache.DmSupplFrequencyCacher;
import hk.org.ha.model.pms.corp.cache.DmSupplSiteCacher;
import hk.org.ha.model.pms.dms.persistence.DmDailyFrequency;
import hk.org.ha.model.pms.dms.persistence.DmSite;
import hk.org.ha.model.pms.dms.persistence.DmSupplFrequency;
import hk.org.ha.model.pms.dms.persistence.DmSupplSite;
import hk.org.ha.model.pms.dms.vo.FormVerb;
import hk.org.ha.model.pms.udt.vetting.PrnPercentage;
import hk.org.ha.model.pms.vo.drug.DmAdminTimeLite;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type = DefaultExternalizer.class)
public class Dose implements Serializable {

	private static final long serialVersionUID = 1L;

	private final transient Log logger = LogFactory.getLog(getClass());
	
	@XmlElement(required = true)
	private BigDecimal dosage;

	@XmlElement
	private BigDecimal dosageEnd;
	
	@XmlElement(required = true)
	private String dosageUnit;
		
	@XmlElement(required = true)
	private Boolean prn;
	
	@XmlElement
	private PrnPercentage prnPercent;		
	
	@XmlElement
	private String siteCode;
	
	@XmlElement(required = true)
	private String siteDesc;

	@XmlElement
	private String displaySiteDesc;
	
	@XmlElement
	private String ipmoeDesc;

	@XmlElement
	private String supplSiteDesc;	

	@XmlElement
	private String adminTimeCode;
	
	@XmlElement
	private String adminTimeDesc;

	@XmlElement
	private Integer dispQty;
	
	@XmlElement
	private String baseUnit;

	@XmlElement
	private String doseRuleCode;
	
	@XmlElement(name="DailyFreq")
	private Freq dailyFreq;
	
	@XmlElement(name="SupplFreq")
	private Freq supplFreq;	
	
	@XmlElement(name="DoseFluid")
	private DoseFluid doseFluid;	
	
	@XmlElement(name="StartAdminRate")
	private AdminRate startAdminRate;	

	@XmlElement(name="EndAdminRate")
	private AdminRate endAdminRate;
	
	@XmlTransient
	private String formVerb;
	
	@XmlTransient
	private DmDailyFrequency dmDailyFrequency;
	
	@XmlTransient
	private DmSupplFrequency dmSupplFrequency;
	
	@XmlTransient
	private DmSite dmSite;
	
	@XmlTransient
	private DmSupplSite dmSupplSite;
	
	@XmlTransient
	private DmAdminTimeLite dmAdminTimeLite;
	
	@XmlTransient
	private List<Site> siteList;
	
	@XmlTransient
	private List<String> dosageUnitList;
	
	// OP
	@XmlTransient
	private FormVerb defaultFormVerb;	
	
	@XmlTransient
	private String legacyDosage;
	
	@XmlTransient
	private String siteLabelDesc;
	
	@XmlTransient
	private String adminTimeLabelDesc;
	
	@XmlTransient
	private String mealTimeInfo;
	
	@XmlTransient
	private String siteCategoryCode;
	
	public Dose() {
		prn = Boolean.FALSE;
	}
	
	public void clearDmInfo() {
		if (dmDailyFrequency != null) {
			dmDailyFrequency = null;
		}
		if (dmSupplFrequency != null) {
			dmSupplFrequency = null;
		}
		if (dmSite != null) {
			dmSite = null;
		}
		if (dmSupplSite != null) {
			dmSupplSite = null;
		}
		if (dmAdminTimeLite != null) {
			dmAdminTimeLite = null;
		}
		if (siteList != null) {
			siteList = null;
		}
		if (dosageUnitList != null) {
			dosageUnitList = null;
		}
	}
	
	public void loadDmInfo() 
	{		
		if (dmDailyFrequency == null && this.getDailyFreq() != null) {
			dmDailyFrequency = DmDailyFrequencyCacher.instance().getDmDailyFrequencyByDailyFreqCode(this.getDailyFreq().getCode());
		}

		if (dmSupplFrequency == null && this.getSupplFreq() != null) {
			dmSupplFrequency = DmSupplFrequencyCacher.instance().getDmSupplFrequencyBySupplFreqCode(this.getSupplFreq().getCode());
		}

		if (dmSite == null && this.getSiteCode() != null) {
			dmSite = DmSiteCacher.instance().getSiteBySiteCode(this.getSiteCode());
			if (dmSite != null) {
				siteCategoryCode = dmSite.getSiteCategoryCode();
				if (StringUtils.isBlank(ipmoeDesc)) {
					ipmoeDesc = dmSite.getIpmoeDesc();
				}
			} else {				
				if (logger != null) {
					logger.info("siteCode["+this.getSiteCode()+"] does not exist");					
				}
			}			
		}

		if (dmSupplSite == null && this.getSiteCode() != null && this.getSupplSiteDesc() != null) {
			dmSupplSite = DmSupplSiteCacher.instance().getSupplSiteBySiteCodeSupplSiteEng(this.getSiteCode(), this.getSupplSiteDesc());
		}

		if (dmAdminTimeLite == null && this.getAdminTimeCode() != null) {
			dmAdminTimeLite = DmAdminTimeLite.objValueOf(this.getAdminTimeCode());
		}
	}
	
	public void setDosage(BigDecimal dosage) {
		this.dosage = dosage;
	}

	public BigDecimal getDosage() {
		return dosage;
	}

	public BigDecimal getDosageEnd() {
		return dosageEnd;
	}

	public void setDosageEnd(BigDecimal dosageEnd) {
		this.dosageEnd = dosageEnd;
	}

	public void setDosageUnit(String dosageUnit) {
		this.dosageUnit = dosageUnit;
	}

	public String getDosageUnit() {
		return dosageUnit;
	}

	public void setPrn(Boolean prn) {
		this.prn = prn;
	}

	public Boolean getPrn() {
		return prn;
	}

	public void setPrnPercent(PrnPercentage prnPercent) {
		this.prnPercent = prnPercent;
	}

	public PrnPercentage getPrnPercent() {
		return prnPercent;
	}

	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

	public String getSiteCode() {
		return siteCode;
	}

	public String getSiteDesc() {
		return siteDesc;
	}

	public void setSiteDesc(String siteDesc) {
		this.siteDesc = siteDesc;
	}

	public String getDisplaySiteDesc() {
		return displaySiteDesc;
	}

	public void setDisplaySiteDesc(String displaySiteDesc) {
		this.displaySiteDesc = displaySiteDesc;
	}

	public String getIpmoeDesc() {
		return ipmoeDesc;
	}

	public void setIpmoeDesc(String ipmoeDesc) {
		this.ipmoeDesc = ipmoeDesc;
	}

	public void setSupplSiteDesc(String supplSiteDesc) {
		this.supplSiteDesc = supplSiteDesc;
	}

	public String getSupplSiteDesc() {
		return supplSiteDesc;
	}
	
	public String getFormVerb() {
		return formVerb;
	}

	public void setFormVerb(String formVerb) {
		this.formVerb = formVerb;
	}

	public String getAdminTimeCode() {
		return adminTimeCode;
	}

	public void setAdminTimeCode(String adminTimeCode) {
		this.adminTimeCode = adminTimeCode;
	}

	public String getAdminTimeDesc() {
		return adminTimeDesc;
	}

	public void setAdminTimeDesc(String adminTimeDesc) {
		this.adminTimeDesc = adminTimeDesc;
	}

	public void setDispQty(Integer dispQty) {
		this.dispQty = dispQty;
	}

	public Integer getDispQty() {
		return dispQty;
	}
	
	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public String getDoseRuleCode() {
		return doseRuleCode;
	}

	public void setDoseRuleCode(String doseRuleCode) {
		this.doseRuleCode = doseRuleCode;
	}

	public Freq getDailyFreq() {
		return dailyFreq;
	}

	public void setDailyFreq(Freq dailyFreq) {
		this.dailyFreq = dailyFreq;
	}

	public Freq getSupplFreq() {
		return supplFreq;
	}

	public void setSupplFreq(Freq supplFreq) {
		this.supplFreq = supplFreq;
	}

	public DoseFluid getDoseFluid() {
		return doseFluid;
	}

	public void setDoseFluid(DoseFluid doseFluid) {
		this.doseFluid = doseFluid;
	}

	public AdminRate getStartAdminRate() {
		return startAdminRate;
	}

	public void setStartAdminRate(AdminRate startAdminRate) {
		this.startAdminRate = startAdminRate;
	}

	public AdminRate getEndAdminRate() {
		return endAdminRate;
	}

	public void setEndAdminRate(AdminRate endAdminRate) {
		this.endAdminRate = endAdminRate;
	}
	
	public DmDailyFrequency getDmDailyFrequency() {
		return dmDailyFrequency;
	}

	public void setDmDailyFrequency(DmDailyFrequency dmDailyFrequency) {
		this.dmDailyFrequency = dmDailyFrequency;
	}

	public DmSupplFrequency getDmSupplFrequency() {
		return dmSupplFrequency;
	}

	public void setDmSupplFrequency(DmSupplFrequency dmSupplFrequency) {
		this.dmSupplFrequency = dmSupplFrequency;
	}

	public DmSite getDmSite() {
		return dmSite;
	}

	public void setDmSite(DmSite dmSite) {
		this.dmSite = dmSite;
	}

	public DmSupplSite getDmSupplSite() {
		return dmSupplSite;
	}

	public void setDmSupplSite(DmSupplSite dmSupplSite) {
		this.dmSupplSite = dmSupplSite;
	}

	public DmAdminTimeLite getDmAdminTimeLite() {
		return dmAdminTimeLite;
	}

	public void setDmAdminTimeLite(DmAdminTimeLite dmAdminTimeLite) {
		this.dmAdminTimeLite = dmAdminTimeLite;
	}

	public void setSiteList(List<Site> siteList) {
		this.siteList = siteList;
	}

	public List<Site> getSiteList() {
		return siteList;
	}

	public void setDosageUnitList(List<String> dosageUnitList) {
		this.dosageUnitList = dosageUnitList;
	}

	public List<String> getDosageUnitList() {
		return dosageUnitList;
	}
	
	public void setDefaultFormVerb(FormVerb defaultFormVerb) {
		this.defaultFormVerb = defaultFormVerb;
	}

	public FormVerb getDefaultFormVerb() {
		return defaultFormVerb;
	}

	public String getLegacyDosage() {
		return legacyDosage;
	}

	public void setLegacyDosage(String legacyDosage) {
		this.legacyDosage = legacyDosage;
	}

	public String getSiteLabelDesc() {
		return siteLabelDesc;
	}

	public void setSiteLabelDesc(String siteLabelDesc) {
		this.siteLabelDesc = siteLabelDesc;
	}

	public String getAdminTimeLabelDesc() {
		return adminTimeLabelDesc;
	}

	public void setAdminTimeLabelDesc(String adminTimeLabelDesc) {
		this.adminTimeLabelDesc = adminTimeLabelDesc;
	}

	public String getMealTimeInfo() {
		return mealTimeInfo;
	}

	public void setMealTimeInfo(String mealTimeInfo) {
		this.mealTimeInfo = mealTimeInfo;
	}

	public void setSiteCategoryCode(String siteCategoryCode) {
		this.siteCategoryCode = siteCategoryCode;
	}

	public String getSiteCategoryCode() {
		return siteCategoryCode;
	}	
	
}
