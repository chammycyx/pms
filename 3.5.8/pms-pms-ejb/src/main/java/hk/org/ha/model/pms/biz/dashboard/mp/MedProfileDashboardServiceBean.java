package hk.org.ha.model.pms.biz.dashboard.mp;

import static hk.org.ha.model.pms.prop.Prop.CHECK_MEDPROFILE_CHECKLIST_DURATION;
import static hk.org.ha.model.pms.prop.Prop.CHECK_MEDPROFILE_SENDOUTLIST_DURATION;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.delivery.DueOrderPrintManagerLocal;
import hk.org.ha.model.pms.biz.delivery.OverdueCount;
import hk.org.ha.model.pms.biz.inbox.CategoryCount;
import hk.org.ha.model.pms.biz.inbox.CategorySummarySupportLocal;
import hk.org.ha.model.pms.biz.inbox.PharmInboxManagerLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.biz.reftable.MpWardManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.PurchaseRequest;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryItemStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryRequestStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatAdminType;
import hk.org.ha.model.pms.udt.medprofile.RetrievePurchaseRequestOption;

import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("medProfileDashboardService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MedProfileDashboardServiceBean implements MedProfileDashboardServiceLocal {

	@PersistenceContext
	private EntityManager em;	
	
	@In
	private Workstore workstore;

	@In
	private PharmInboxManagerLocal pharmInboxManager;
	
	@In
	private MpWardManagerLocal mpWardManager;
	
	@In
	private DueOrderPrintManagerLocal dueOrderPrintManager;
	
	@In
	private CategorySummarySupportLocal categorySummarySupport;

	@In
	private Workstation workstation;
	
	@In @Out
	private Boolean showAllWards;
	
	@In @Out
	private Boolean showChemoProfileOnly;
	
	@Out(required = false)
	protected Long newCount;
	
	@Out(required = false)
	protected Long verifyCount;
	
	@Out(required = false)
	protected Long newNormalCount;
	
	@Out(required = false)
	protected Long newUrgentCount;
	
	@Out(required = false)
	protected Long newPendingCount;

	@Out(required = false)
	protected Long newOverridePendingCount;
	
	@Out(required = false)
	protected Long newSuspendCount;
	
	@Out(required = false)
	protected Long verifyNormalCount;
	
	@Out(required = false)
	protected Long verifyUrgentCount;
	
	@Out(required = false)
	protected Long verifyPendingCount;

	@Out(required = false)
	protected Long verifyOverridePendingCount;
	
	@Out(required = false)
	protected Long verifySuspendCount;

	@Out(required = false)
	protected Long exceptionCount;
	
	@Out(required = false)
	protected Long followUpCount;
	
	@Out(required = false)
	protected Date dashboardUpdateDate;
	
	@Out(required = false)
	protected Long dashboardOverdueNewCount;
	
	@Out(required = false)
	protected Long dashboardOverdueRefillCount;
	
	@Out(required = false)
	protected Long dashboardForecastNewCount;
	
	@Out(required = false)
	protected Long dashboardForecastRefillCount;
	
	@Out(required = false)
	private Long paidCount;
	
	@Out(required = false)
	private Long awaitPaymentCount;
	
	@Out(required = false)
	private Long uncheckedCount;
	
	@Out(required = false)
	private Long unsentCount;
	
    public MedProfileDashboardServiceBean() {
    }
    
    @Create
    @Override
    public void init() {
    	showAllWards = pharmInboxManager.getShowAllWards(workstation);
    	showChemoProfileOnly = pharmInboxManager.getShowChemoProfileOnly(workstation, showChemoProfileOnly);
    }    
    
    @Override
    public void countAllDashboardItems() {
    	
    	Date now = new Date();
    	countInboxRelatedItems(now);
    	countDueOrderPrintRelatedItems(now);
    	countWorkflowRelatedItems(now);
    	dashboardUpdateDate = now;
    }
    
    private void countInboxRelatedItems(Date now) {

    	List<String> wardCodeList = mpWardManager.retrieveWardFilterList(workstore, showAllWards);

		List<CategoryCount> orderTypeCounts = categorySummarySupport.retrieveOrderTypeCategoryCountList(workstore, showChemoProfileOnly, wardCodeList, now);
		List<CategoryCount> adminTypeCounts = categorySummarySupport.retrieveAdminTypeCategoryCountList(workstore, showChemoProfileOnly, wardCodeList, now);
    	
		newCount = 0L;
		verifyCount = 0L;
		newNormalCount = 0L;
		newUrgentCount = 0L;
		newPendingCount = 0L;
		newOverridePendingCount = 0L;
		newSuspendCount = 0L;
		verifyNormalCount = 0L;
		verifyUrgentCount = 0L;
		verifyPendingCount = 0L;
		verifyOverridePendingCount = 0L;
		verifySuspendCount = 0L;
		
    	exceptionCount = categorySummarySupport.countErrorStat(workstore, wardCodeList, now);
    	
		for (CategoryCount count: adminTypeCounts) {
			assignAdminTypeCategoryCount(count);
		}
		for (CategoryCount count: orderTypeCounts) {
			assignOrderTypeCategoryCount(count);
		}
    }
    
    private void countDueOrderPrintRelatedItems(Date now) {
    	
    	Date startDate = MedProfileUtils.getOverdueStartDate(now);
    	Date forecaseDate = MedProfileUtils.addHours(now, 2);

    	followUpCount = dueOrderPrintManager.retrieveFollowUpCount(workstore);
    	
    	OverdueCount overdueCount = dueOrderPrintManager.retrieveOverdueCountByStat(workstore, startDate, now, now, null, false);
    	OverdueCount forecastCount = dueOrderPrintManager.retrieveOverdueCountByStat(workstore, now, forecaseDate, now, null, false);
    	
    	dashboardOverdueNewCount = overdueCount.getNewCount();
    	dashboardOverdueRefillCount = overdueCount.getRefillCount();
    	dashboardForecastNewCount = forecastCount.getNewCount();
    	dashboardForecastRefillCount = forecastCount.getRefillCount();
    	
    	countSfiOverdueItems(dueOrderPrintManager.retrievePurchaseRequestList(workstore, now, RetrievePurchaseRequestOption.All));
    }
    
    private void countWorkflowRelatedItems(Date now) {
    	
    	uncheckedCount = 0L;
    	unsentCount = 0L;
    	
    	uncheckedCount = countDelivery(MedProfileUtils.addDays(now, (CHECK_MEDPROFILE_CHECKLIST_DURATION.get(7) - 1) * -1), DeliveryStatus.Printed, DeliveryItemStatus.Printed);
    	unsentCount = countDelivery(MedProfileUtils.addDays(now, (CHECK_MEDPROFILE_SENDOUTLIST_DURATION.get(7) - 1) * -1), DeliveryStatus.Checked);
    }
    
    private Long countDelivery(Date startDate, DeliveryStatus deliveryStatus, DeliveryItemStatus deliveryItemStatus) {
    	
    	return (Long) em.createQuery(
    			"select count(distinct o.delivery.id) from DeliveryItem o" + // 20171023 index check : Delivery.hospCode,batchDate : I_DELIVERY_01
    			" where o.delivery.hospital = :hospital" +
    			" and o.delivery.deliveryRequest.status = :deliveryRequestStatus" +
    			" and o.delivery.status = :deliveryStatus" +
    			" and o.status = :deliveryItemStatus" +
    			" and o.delivery.batchDate >= :startDate")
    			.setParameter("hospital", workstore.getHospital())
    			.setParameter("deliveryRequestStatus", DeliveryRequestStatus.Completed)
    			.setParameter("deliveryStatus", deliveryStatus)
    			.setParameter("deliveryItemStatus", deliveryItemStatus)
    			.setParameter("startDate", startDate)
    			.getSingleResult();    	
    }
    
    private Long countDelivery(Date startDate, DeliveryStatus deliveryStatus) {
    	
    	return (Long) em.createQuery(
    			"select count(o) from Delivery o" + // 20171023 index check : Delivery.hospCode,batchDate : I_DELIVERY_01
    			" where o.hospital = :hospital" +
    			" and o.deliveryRequest.status = :deliveryRequestStatus" +
    			" and o.status = :deliveryStatus" +
    			" and o.batchDate >= :startDate")
    			.setParameter("hospital", workstore.getHospital())
    			.setParameter("deliveryRequestStatus", DeliveryRequestStatus.Completed)
    			.setParameter("deliveryStatus", deliveryStatus)
    			.setParameter("startDate", startDate)
    			.getSingleResult();    	
    }
    
	private void assignAdminTypeCategoryCount(CategoryCount count) {
		
		if (count.getAdminType() == MedProfileStatAdminType.NewArrival) {
			newCount = count.getCount();
		} else if (count.getAdminType() == MedProfileStatAdminType.Verify) {
			verifyCount = count.getCount();
		}
	}
	
	private void assignOrderTypeCategoryCount(CategoryCount count) {

		switch (count.getAdminType()) {
			case NewArrival: 
				switch (count.getOrderType()) {
					case Normal:
						newNormalCount = count.getCount();
						break;
					case Urgent:
						newUrgentCount = count.getCount();
						break;
					case PendingSuspend:
						newPendingCount = count.getPendingCount();
						newSuspendCount = count.getSuspendCount();
						newOverridePendingCount = count.getOverridePendingCount();
						break;
				}
				break;
				
			case Verify: 
				switch (count.getOrderType()) {
					case Normal:
						verifyNormalCount = count.getCount();
						break;
					case Urgent:
						verifyUrgentCount = count.getCount();
						break;
					case PendingSuspend:
						verifyPendingCount = count.getPendingCount();
						verifySuspendCount = count.getSuspendCount();
						verifyOverridePendingCount = count.getOverridePendingCount();
						break;
				}
				break;
		}
	}
    
    private void countSfiOverdueItems(List<PurchaseRequest> purchaseRequestList) {
    	
    	paidCount = 0L;
    	awaitPaymentCount = 0L;
    	
    	for (PurchaseRequest purchaseRequest: purchaseRequestList) {
    		
    		if (!InvoiceStatus.Outstanding_Settle.contains(purchaseRequest.getInvoiceStatus())) {
    			continue;
    		}
    		
			if (purchaseRequest.clearanceCheckRequired()) {
				awaitPaymentCount++;
			} else {
		    	paidCount++;
			}
		}
    }
    
	@Remove
	@Override
	public void destroy() {
	}
}
