package hk.org.ha.model.pms.vo.charging;

import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.InvoiceItem;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.disp.MedCasePasPatInd;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.PharmOrderPatType;
import hk.org.ha.model.pms.udt.vetting.OrderType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FcsSfiInvoice implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private static final String DUMMY_TICKET_NUM = "0000";
	
	private String patHospCode;
	private String hospCode;
	private String workstoreCode;
	private Date dispDate;
	private String ticketNum;
	private String prevOrderNum;
	private String orderNum;
	private Integer dispOrderNum;
	private String invoiceNum;
	private Date createDate;
	private String prevInvoiceNum;
	private Date prevInvoiceCreateDate;
	private String orgInvoiceNum;
	private Date orgInvoiceCreateDate;	
	private String manualInvoiceNum;
	private Date forceProceedDate;
	private String manualReceiptNum;
	private String caseNum;
	private String hkid;
	private Long pasApptSeq;
	private MedCasePasPatInd pasPatInd;
	private String pasPayCode;
	private BigDecimal totalAmount;
	private String pasSpecCode;
	private String pasSubSpecCode;
	private MedOrderDocType docType;
	private PharmOrderPatType patType;
	private OrderType orderType;
	
	// for disp_status
	private DispOrderStatus dispOrderStatus;
	private DispOrderAdminStatus dispOrderAdminStatus;
	
	// for inv_status
	private InvoiceStatus invoiceStatus;
	
	private String uncollectUser;
	private Boolean forceProceedFlag;
	private String forceProceedUser;
	private String forceProceedCode;
	private Boolean refillFlag;
	private Date updateDate;
	private String updateUser;
	private Date fcsCheckDate;
	private BigDecimal receiveAmount;
	
	private List<FcsSfiInvoiceItem> fcsSfiInvoiceItemList;

	public FcsSfiInvoice() {
		// default constructor
	}
	
	public FcsSfiInvoice(Invoice invoice, Invoice prevInvoice) {

		DispOrder dispOrder = invoice.getDispOrder();
		PharmOrder pharmOrder = dispOrder.getPharmOrder();
		MedOrder medOrder = pharmOrder.getMedOrder();
		Workstore workstore = dispOrder.getWorkstore();
		Ticket ticket = dispOrder.getTicket();
		MedCase medCase = pharmOrder.getMedCase();
		Patient patient = medOrder.getPatient();

		patHospCode = medOrder.getPatHospCode();
		hospCode = workstore.getHospCode();
		workstoreCode = workstore.getWorkstoreCode();
		dispDate = dispOrder.getDispDate();
		ticketNum = ticket.getTicketNum();
		prevOrderNum = medOrder.getPrevOrderNum();
		orderNum = medOrder.getOrderNum();
		dispOrderNum = dispOrder.getDispOrderNum();
		invoiceNum = invoice.getInvoiceNum();
		createDate = invoice.getCreateDate();
		if (prevInvoice != null) {
			prevInvoiceNum = prevInvoice.getInvoiceNum();
			prevInvoiceCreateDate = prevInvoice.getCreateDate();
		}
		manualInvoiceNum = invoice.getManualInvoiceNum();
		forceProceedDate = invoice.getForceProceedDate();
		manualReceiptNum = invoice.getManualReceiptNum();
		hkid = patient.getHkid();
		totalAmount = invoice.getTotalAmount();
		docType = medOrder.getDocType();
		patType = pharmOrder.getPatType();
		orderType = dispOrder.getOrderType();
		if (medCase != null) {
			caseNum = medCase.getCaseNum();
			pasApptSeq = medCase.getPasApptSeq();
			pasPayCode = medCase.getPasPayCode();
			pasSpecCode = medCase.getPasSpecCode();
			pasSubSpecCode = medCase.getPasSubSpecCode();
			pasPatInd = medCase.getPasPatInd();
		}
		
		dispOrderStatus = dispOrder.getStatus();
		dispOrderAdminStatus = dispOrder.getAdminStatus();
		
		invoiceStatus = invoice.getStatus();
		
		uncollectUser = dispOrder.getUncollectUser();
		forceProceedFlag = invoice.getForceProceedFlag();
		forceProceedUser = invoice.getForceProceedUser();
		forceProceedCode = invoice.getForceProceedCode();
		refillFlag = dispOrder.getRefillFlag();
		updateDate = invoice.getUpdateDate();
		updateUser = invoice.getUpdateUser();
		fcsCheckDate = invoice.getFcsCheckDate();
		receiveAmount = invoice.getReceiveAmount();
		
		fcsSfiInvoiceItemList = new ArrayList<FcsSfiInvoiceItem>();
		for (InvoiceItem invoiceItem : invoice.getInvoiceItemList()) {
			fcsSfiInvoiceItemList.add(new FcsSfiInvoiceItem(invoiceItem));
		}
	}
	
	public FcsSfiInvoice(Invoice invoice, Invoice prevInvoice, Invoice orgInvoice) {

		DispOrder dispOrder = invoice.getDispOrder();
		PharmOrder pharmOrder = dispOrder.getPharmOrder();
		MedOrder medOrder = pharmOrder.getMedOrder();
		Workstore workstore = dispOrder.getWorkstore();
		Ticket ticket = dispOrder.getTicket();
		MedCase medCase = pharmOrder.getMedCase();
		Patient patient = medOrder.getPatient();

		patHospCode = medOrder.getPatHospCode();
		hospCode = workstore.getHospCode();
		workstoreCode = workstore.getWorkstoreCode();
		dispDate = dispOrder.getDispDate();
		orderType = dispOrder.getOrderType();
		
		if( OrderType.InPatient.equals(orderType) ){
			ticketNum = DUMMY_TICKET_NUM;
			orderNum = invoice.getChargeOrderNum();
			if( ticket != null && DUMMY_TICKET_NUM.equals(ticket.getTicketNum()) ){
				dispOrderStatus = DispOrderStatus.Issued;//IPSFI Save - mark Issue directly
			}else{
				dispOrderStatus = DispOrderStatus.Vetted;//SFI Purchase Save - override to Vetted
			}
		}else{// Out-Patient
			ticketNum = ticket.getTicketNum();
			orderNum = medOrder.getOrderNum();
			dispOrderStatus = dispOrder.getStatus();
		}
		dispOrderAdminStatus = dispOrder.getAdminStatus();
		prevOrderNum = medOrder.getPrevOrderNum();
		
		if( dispOrder.getDispOrderNum() != null ){
			dispOrderNum = dispOrder.getDispOrderNum();
		}else{
			dispOrderNum = Integer.valueOf(0);
		}
		invoiceNum = invoice.getInvoiceNum();
		createDate = invoice.getCreateDate();
		
		if (prevInvoice != null) {
			prevInvoiceNum = prevInvoice.getInvoiceNum();
			prevInvoiceCreateDate = prevInvoice.getCreateDate();
		}
		
		if (orgInvoice != null) {
			orgInvoiceNum = orgInvoice.getInvoiceNum();
			orgInvoiceCreateDate = orgInvoice.getCreateDate();
		}
		
		manualInvoiceNum = invoice.getManualInvoiceNum();
		forceProceedDate = invoice.getForceProceedDate();
		manualReceiptNum = invoice.getManualReceiptNum();
		hkid = patient.getHkid();
		totalAmount = invoice.getTotalAmount();
		docType = medOrder.getDocType();
		patType = pharmOrder.getPatType();
		if (medCase != null) {
			caseNum = medCase.getCaseNum();
			pasApptSeq = medCase.getPasApptSeq();
			pasPayCode = medCase.getPasPayCode();
			pasSpecCode = medCase.getPasSpecCode();
			pasSubSpecCode = medCase.getPasSubSpecCode();
			pasPatInd = medCase.getPasPatInd();
		}
		
		invoiceStatus = invoice.getStatus();
		
		uncollectUser = dispOrder.getUncollectUser();
		forceProceedFlag = invoice.getForceProceedFlag();
		forceProceedUser = invoice.getForceProceedUser();
		forceProceedCode = invoice.getForceProceedCode();
		refillFlag = dispOrder.getRefillFlag();
		updateDate = invoice.getUpdateDate();
		updateUser = invoice.getUpdateUser();
		fcsCheckDate = invoice.getFcsCheckDate();
		receiveAmount = invoice.getReceiveAmount();
		
		fcsSfiInvoiceItemList = new ArrayList<FcsSfiInvoiceItem>();
		for (InvoiceItem invoiceItem : invoice.getInvoiceItemList()) {
			fcsSfiInvoiceItemList.add(new FcsSfiInvoiceItem(invoiceItem));
		}
	}

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public Date getDispDate() {
		return dispDate;
	}

	public void setDispDate(Date dispDate) {
		this.dispDate = dispDate;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public String getPrevOrderNum() {
		return prevOrderNum;
	}

	public void setPrevOrderNum(String prevOrderNum) {
		this.prevOrderNum = prevOrderNum;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public Integer getDispOrderNum() {
		return dispOrderNum;
	}

	public void setDispOrderNum(Integer dispOrderNum) {
		this.dispOrderNum = dispOrderNum;
	}

	public String getInvoiceNum() {
		return invoiceNum;
	}

	public void setInvoiceNum(String invoiceNum) {
		this.invoiceNum = invoiceNum;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getPrevInvoiceNum() {
		return prevInvoiceNum;
	}

	public void setPrevInvoiceNum(String prevInvoiceNum) {
		this.prevInvoiceNum = prevInvoiceNum;
	}

	public Date getPrevInvoiceCreateDate() {
		return prevInvoiceCreateDate;
	}

	public void setPrevInvoiceCreateDate(Date prevInvoiceCreateDate) {
		this.prevInvoiceCreateDate = prevInvoiceCreateDate;
	}

	public String getManualInvoiceNum() {
		return manualInvoiceNum;
	}

	public void setManualInvoiceNum(String manualInvoiceNum) {
		this.manualInvoiceNum = manualInvoiceNum;
	}

	public Date getForceProceedDate() {
		return forceProceedDate;
	}

	public void setForceProceedDate(Date forceProceedDate) {
		this.forceProceedDate = forceProceedDate;
	}

	public String getManualReceiptNum() {
		return manualReceiptNum;
	}

	public void setManualReceiptNum(String manualReceiptNum) {
		this.manualReceiptNum = manualReceiptNum;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public Long getPasApptSeq() {
		return pasApptSeq;
	}

	public void setPasApptSeq(Long pasApptSeq) {
		this.pasApptSeq = pasApptSeq;
	}

	public String getPasPayCode() {
		return pasPayCode;
	}

	public void setPasPayCode(String pasPayCode) {
		this.pasPayCode = pasPayCode;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getPasSpecCode() {
		return pasSpecCode;
	}

	public void setPasSpecCode(String pasSpecCode) {
		this.pasSpecCode = pasSpecCode;
	}

	public String getPasSubSpecCode() {
		return pasSubSpecCode;
	}

	public void setPasSubSpecCode(String pasSubSpecCode) {
		this.pasSubSpecCode = pasSubSpecCode;
	}

	public MedOrderDocType getDocType() {
		return docType;
	}

	public void setDocType(MedOrderDocType docType) {
		this.docType = docType;
	}

	public DispOrderStatus getDispOrderStatus() {
		return dispOrderStatus;
	}

	public void setDispOrderStatus(DispOrderStatus dispOrderStatus) {
		this.dispOrderStatus = dispOrderStatus;
	}

	public InvoiceStatus getInvoiceStatus() {
		return invoiceStatus;
	}

	public void setInvoiceStatus(InvoiceStatus invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public String getUncollectUser() {
		return uncollectUser;
	}

	public void setUncollectUser(String uncollectUser) {
		this.uncollectUser = uncollectUser;
	}

	public Boolean getForceProceedFlag() {
		return forceProceedFlag;
	}

	public void setForceProceedFlag(Boolean forceProceedFlag) {
		this.forceProceedFlag = forceProceedFlag;
	}

	public String getForceProceedUser() {
		return forceProceedUser;
	}

	public void setForceProceedUser(String forceProceedUser) {
		this.forceProceedUser = forceProceedUser;
	}

	public String getForceProceedCode() {
		return forceProceedCode;
	}

	public void setForceProceedCode(String forceProceedCode) {
		this.forceProceedCode = forceProceedCode;
	}

	public Boolean getRefillFlag() {
		return refillFlag;
	}

	public void setRefillFlag(Boolean refillFlag) {
		this.refillFlag = refillFlag;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getFcsCheckDate() {
		return fcsCheckDate;
	}

	public void setFcsCheckDate(Date fcsCheckDate) {
		this.fcsCheckDate = fcsCheckDate;
	}

	public BigDecimal getReceiveAmount() {
		return receiveAmount;
	}

	public void setReceiveAmount(BigDecimal receiveAmount) {
		this.receiveAmount = receiveAmount;
	}

	public List<FcsSfiInvoiceItem> getFcsSfiInvoiceItemList() {
		return fcsSfiInvoiceItemList;
	}

	public void setFcsSfiInvoiceItemList(
			List<FcsSfiInvoiceItem> fcsSfiInvoiceItemList) {
		this.fcsSfiInvoiceItemList = fcsSfiInvoiceItemList;
	}

	public DispOrderAdminStatus getDispOrderAdminStatus() {
		return dispOrderAdminStatus;
	}

	public void setDispOrderAdminStatus(DispOrderAdminStatus dispOrderAdminStatus) {
		this.dispOrderAdminStatus = dispOrderAdminStatus;
	}

	public String getOrgInvoiceNum() {
		return orgInvoiceNum;
	}

	public void setOrgInvoiceNum(String orgInvoiceNum) {
		this.orgInvoiceNum = orgInvoiceNum;
	}

	public Date getOrgInvoiceCreateDate() {
		return orgInvoiceCreateDate;
	}

	public void setOrgInvoiceCreateDate(Date orgInvoiceCreateDate) {
		this.orgInvoiceCreateDate = orgInvoiceCreateDate;
	}

	public MedCasePasPatInd getPasPatInd() {
		return pasPatInd;
	}

	public void setPasPatInd(MedCasePasPatInd pasPatInd) {
		this.pasPatInd = pasPatInd;
	}

	public PharmOrderPatType getPatType() {
		return patType;
	}

	public void setPatType(PharmOrderPatType patType) {
		this.patType = patType;
	}
	
	
}
