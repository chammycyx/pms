package hk.org.ha.model.pms.udt.reftable;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DurationUnit implements StringValuedEnum {

	Day("D", "Days"),
	Month("M", "Months"),
	Cycle("C", "Cycles");	
	
    private final String dataValue;
    private final String displayValue;
        
    DurationUnit(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static DurationUnit dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DurationUnit.class, dataValue);
	}
    
	public static class Converter extends StringValuedEnumConverter<DurationUnit> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<DurationUnit> getEnumClass() {
    		return DurationUnit.class;
    	}
    }
}
