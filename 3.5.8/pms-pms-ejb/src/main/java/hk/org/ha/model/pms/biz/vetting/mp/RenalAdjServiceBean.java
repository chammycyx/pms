package hk.org.ha.model.pms.biz.vetting.mp;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmDrugProperty;
import hk.org.ha.model.pms.dms.vo.RenalAdj;
import hk.org.ha.model.pms.persistence.medprofile.MpRenalEcrclLog;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.vetting.mp.RenalChartRpt;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import net.sf.jasperreports.engine.JRDataSource;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("renalAdjService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class RenalAdjServiceBean implements RenalAdjServiceLocal, Serializable {

	private static final long serialVersionUID = 1L;

	@In
	private PrintAgentInf printAgent;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

	@In
	private ReportProvider<JRDataSource> reportProvider;

	@In
	private PrinterSelectorLocal printerSelector;

	private List<RenalChartRpt> renalChartRptList;
	private boolean highlightLastRecord;
	
	private byte[] file;
	
	public void generateRenalChartRpt() {
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("_PNG_ZOOM_RATIO", Float.valueOf(1f));
		parameters.put("HIGHLIGHT_LAST_RECORD", highlightLastRecord);

		printAgent.renderAndRedirect(new RenderJob(
				"RenalChart",
				new PrintOption(), 
				parameters, 
				renalChartRptList));
	}

	public void retrieveRenalChartRpt(List<MpRenalEcrclLog> mpRenalEcrclLogList, boolean highlightLastRecord) {
		
		this.highlightLastRecord = highlightLastRecord;
		renalChartRptList = new ArrayList<RenalChartRpt>();
		
		for (MpRenalEcrclLog mpRenalEcrclLog : mpRenalEcrclLogList) {
			RenalChartRpt renalChartRpt = new RenalChartRpt();
			renalChartRpt.setEcrcl(mpRenalEcrclLog.getEcrcl());
			renalChartRpt.setCreateDate(mpRenalEcrclLog.getCreateDate());
			renalChartRptList.add(renalChartRpt);
		}
	}

	public RenalAdj retrieveRenalPdfByItemCode(String itemCode) throws IOException {
		DmDrugProperty dmDrugProperty = DmDrugCacher.instance().getDmDrug(itemCode).getDmDrugProperty();
		return retrieveRenalPdfByDrugKey(dmDrugProperty.getDisplayname(), dmDrugProperty.getFormCode(), dmDrugProperty.getSaltProperty());
	}

	public RenalAdj retrieveRenalPdfByDrugKey(String displayName, String formCode, String saltProperty) throws IOException {
		RenalAdj renalAdj = dmsPmsServiceProxy.retrieveRenalAdj(displayName, formCode, saltProperty);
		renalAdj = renalAdj == null ? new RenalAdj() : renalAdj;
		file = null;
		if (Arrays.asList("01", "03").contains(renalAdj.getRenalAdjCode())) {
			file = dmsPmsServiceProxy.retrieveRenalMonograph(displayName, formCode, saltProperty);
		}
		return renalAdj;
	}

	public void generateRenalPdf() {
		reportProvider.redirectReport(reportProvider.storeReport("renalPdf", file, ReportProvider.PDF));
	}
	
	public void printRenalPdf() {
		printAgent.print(
				reportProvider.storeReport("renalPdf", file, ReportProvider.PDF),
				printerSelector.retrievePrinterSelect(PrintDocType.Report),
				null
			);
	}
	
	public DmDrug retrieveDmDrug(String itemCode) {
		DmDrug dmDrug = DmDrugCacher.instance().getDmDrug(itemCode);
		if (dmDrug != null) {
			dmDrug.getFullDrugDesc();
		}
		return dmDrug;
	}

	@Remove
	public void destroy() {
	}
}
