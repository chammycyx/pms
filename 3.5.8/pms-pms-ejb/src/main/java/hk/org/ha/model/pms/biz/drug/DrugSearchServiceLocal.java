package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.model.pms.dms.persistence.DmRegimen;
import hk.org.ha.model.pms.dms.vo.BnfCriteria;
import hk.org.ha.model.pms.dms.vo.Capd;
import hk.org.ha.model.pms.dms.vo.CommonOrder;
import hk.org.ha.model.pms.dms.vo.DrugCommonDosage;
import hk.org.ha.model.pms.dms.vo.DrugName;
import hk.org.ha.model.pms.dms.vo.PivasFormula;
import hk.org.ha.model.pms.dms.vo.PreparationProperty;
import hk.org.ha.model.pms.dms.vo.PreparationSearchCriteria;
import hk.org.ha.model.pms.dms.vo.RelatedDrugSearchCriteria;
import hk.org.ha.model.pms.dms.vo.RouteForm;
import hk.org.ha.model.pms.dms.vo.RouteFormSearchCriteria;
import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
import hk.org.ha.model.pms.udt.vetting.RegimenDurationUnit;
import hk.org.ha.model.pms.vo.drug.DrugSearchReturn;
import hk.org.ha.model.pms.vo.rx.RxDrug;

import javax.ejb.Local;

@Local
public interface DrugSearchServiceLocal {
	
	DrugSearchReturn drugSearch(DrugSearchSource drugSearchSource, RelatedDrugSearchCriteria relatedDrugSearchCriteria, RouteFormSearchCriteria routeFormSearchCriteria);
	
	DrugSearchReturn drugSearchByBnf(DrugSearchSource drugSearchSource, BnfCriteria bnfCriteria);

	DrugSearchReturn retrieveBnfTreeDetail(DrugSearchSource drugSearchSource, BnfCriteria bnfCriteria);
	
	DrugSearchReturn retrieveDrugDetail(DrugSearchSource drugSearchSource, RouteFormSearchCriteria routeFormSearchCriteria, String theraGroup);

	DrugSearchReturn retrieveDrugPrice(DrugSearchSource drugSearchSource, DrugName drugName, PreparationSearchCriteria preparationSearchCriteria, RelatedDrugSearchCriteria relatedDrugSearchCriteria);
	
	DrugSearchReturn retrieveFreeTextEntry(DrugSearchSource drugSearchSource);
	
	DrugSearchReturn retrieveMpFreeTextEntry(DrugSearchSource drugSearchSource);
	
  	DrugSearchReturn retrieveMpCapdDrugSupplier(DrugSearchSource drugSearchSource);
  	
	DrugSearchReturn retrieveMpCapdDrugCalcium(DrugSearchSource drugSearchSource, String supplierSystem);
	
	DrugSearchReturn retrieveMpCapdDrugConcentration(DrugSearchSource drugSearchSource, String supplierSystem, String calciumStrength);
	
	DrugSearchReturn createFreeTextEntryDrug(DrugSearchSource drugSearchSource, RxDrug rxDrug, Integer carryDuration, RegimenDurationUnit carryDurationUnit);
	
	DrugSearchReturn createCapdDrug(DrugSearchSource drugSearchSource);
	
	DrugSearchReturn createSelectedDrugByDosageConversion(DrugSearchSource drugSearchSource, DrugName drugName, RouteForm routeForm, PreparationProperty preparation, DrugCommonDosage commonDosage, DmRegimen dmRegimen);
	
	DrugSearchReturn createSelectedDrugByVetting(DrugSearchSource drugSearchSource, DrugName drugName, RouteForm routeForm, PreparationProperty preparation, DrugCommonDosage commonDosage, DmRegimen dmRegimen, Integer carryDuration, RegimenDurationUnit carryDurationUnit, String specialInstruction);

	DrugSearchReturn createMpFreeTextEntryDrug(DrugSearchSource drugSearchSource, RxDrug rxDrug);
	
	DrugSearchReturn createMpCapdDrug(DrugSearchSource drugSearchSource, String supplierSystem, String calciumStrength, Capd capd);
	
	DrugSearchReturn createSelectedMpDrugByVetting(DrugSearchSource drugSearchSource, DrugName drugName, RouteForm routeForm, PreparationProperty preparation, CommonOrder commonOrder, PivasFormula pivasFormula, DmRegimen dmRegimen, String pasSpecialty, String pasSubSpecialty, String itemCode);
	
	void destroy();
	
}
