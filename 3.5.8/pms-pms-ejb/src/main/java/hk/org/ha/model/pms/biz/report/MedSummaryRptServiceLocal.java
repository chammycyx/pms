package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.vo.report.MedSummaryRpt;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface MedSummaryRptServiceLocal {

	List<MedSummaryRpt> retrieveMedSummaryRptList(Date dispDate, String ticketNum, PrintLang printLang);
	
	void printMedSummaryRpt(Date dispDate, String ticketNum, PrintLang printLang);
	
	boolean isRetrieveSuccess();
	
	void destroy();
	
}
