package hk.org.ha.model.pms.persistence.disp;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.util.StringArray;
import hk.org.ha.model.pms.corp.cache.DmMedicalOfficerCacher;
import hk.org.ha.model.pms.dms.persistence.DmMedicalOfficer;
import hk.org.ha.model.pms.persistence.RemarkEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.alert.mds.AlertType;
import hk.org.ha.model.pms.udt.disp.DiscontinueStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.disp.OnHandSourceType;
import hk.org.ha.model.pms.udt.disp.PharmOrderStatus;
import hk.org.ha.model.pms.udt.disp.RemarkStatus;
import hk.org.ha.model.pms.udt.vetting.CmsOrderSubType;
import hk.org.ha.model.pms.udt.vetting.CmsOrderType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.util.StopWatcher;
import hk.org.ha.model.pms.vo.alert.mds.AlertDdim;
import hk.org.ha.model.pms.vo.patient.DhPatient;
import hk.org.ha.model.pms.vo.rx.IvAdditive;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.PrivateOwned;
import org.granite.messaging.amf.io.util.externalizer.annotation.IgnoredProperty;
import org.jboss.seam.security.Identity;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;


@Entity
@Table(name = "MED_ORDER")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="MedOrder")
public class MedOrder extends RemarkEntity {

	private static final long serialVersionUID = 1L;
	
	private static final String YYYY = "yyyy";
	
	public static final List<MedOrderStatus> UNUSABLE_STATUS = Arrays.asList(new MedOrderStatus[]{MedOrderStatus.SysDeleted, MedOrderStatus.MoeSuspended});
	
	private static final String JAXB_CONTEXT_PATIENT = "hk.org.ha.model.pms.vo.patient";
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "medOrderSeq")
	@SequenceGenerator(name = "medOrderSeq", sequenceName = "SQ_MED_ORDER", initialValue = 100000000)
	@XmlElement
	private Long id;	
	
	@Column(name = "ORDER_NUM", nullable = false, length = 36)
	@XmlElement
	private String orderNum;
	
	@Column(name = "ORDER_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@XmlElement
	private Date orderDate;
	
	@Column(name = "REF_NUM", length = 5)
	@XmlElement
	private String refNum;
	
    @Column(name = "PAT_HOSP_CODE", nullable = false, length = 3)
	@XmlElement
    private String patHospCode;
    
	@Converter(name = "MedOrder.prescType", converterClass = MedOrderPrescType.Converter.class)
    @Convert("MedOrder.prescType")
    @Column(name = "PRESC_TYPE", nullable = false, length = 1)
	@XmlElement
	private MedOrderPrescType prescType;
	
	@Converter(name = "MedOrder.orderType", converterClass = OrderType.Converter.class)
    @Convert("MedOrder.orderType")
	@Column(name="ORDER_TYPE", nullable = false, length = 1)
	@XmlElement
	private OrderType orderType;
	
	@Converter(name = "MedOrder.docType", converterClass = MedOrderDocType.Converter.class)
    @Convert("MedOrder.docType")
	@Column(name = "DOC_TYPE", nullable = false, length = 1)
	@XmlElement
	private MedOrderDocType docType;
	
	@Column(name = "PREV_ORDER_NUM", length = 12)
	@XmlElement
	private String prevOrderNum;
	
	@Column(name = "PREGNANCY_CHECK_FLAG", nullable = false)
	@XmlTransient
	private Boolean pregnancyCheckFlag;

	@Column(name = "MAX_ITEM_NUM", nullable = false)
	@XmlTransient
	private Integer maxItemNum;
	
	@Column(name = "PROCESSING_FLAG", nullable = false)
	@XmlElement
	private Boolean processingFlag;
	
	@Converter(name = "MedOrder.cmsOrderType", converterClass = CmsOrderType.Converter.class)
    @Convert("MedOrder.cmsOrderType")
	@Column(name="CMS_ORDER_TYPE", nullable = false, length = 1)
	@XmlTransient
	private CmsOrderType cmsOrderType;

	@Converter(name = "MedOrder.cmsOrderSubType", converterClass = CmsOrderSubType.Converter.class)
    @Convert("MedOrder.cmsOrderSubType")
	@Column(name="CMS_ORDER_SUB_TYPE", nullable = false, length = 1)
	@XmlTransient
	private CmsOrderSubType cmsOrderSubType;

	@Column(name = "DOCTOR_CODE", length = 12)
	@XmlTransient
	private String doctorCode;

	@Column(name = "DOCTOR_RANK_CODE", length = 10)
	@XmlTransient
	private String doctorRankCode;
	
	@Column(name = "DOCTOR_NAME", length = 48)
	@XmlTransient
	private String doctorName;
	
	@Column(name = "SPEC_CODE", length = 4)
	@XmlTransient
	private String specCode;
	
	@Column(name = "WARD_CODE", length = 4)
	@XmlTransient
	private String wardCode;

	@Converter(name = "MedOrder.status", converterClass = MedOrderStatus.Converter.class)
    @Convert("MedOrder.status")
	@Column(name="STATUS", nullable = false, length = 1)
	@XmlElement
	private MedOrderStatus status;
	
	@Column(name = "WORKSTATION_ID")
	@XmlTransient
	private Long workstationId;

	@Converter(name = "MedOrder.remarkStatus", converterClass = RemarkStatus.Converter.class)
    @Convert("MedOrder.remarkStatus")
	@Column(name = "REMARK_STATUS", nullable = false, length = 1)
	@XmlElement
	private RemarkStatus remarkStatus;
	
	@Column(name = "EIS_SPEC_CODE", length = 4)
	@XmlTransient
	private String eisSpecCode;
	
	@Column(name = "CMS_UPDATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	@XmlTransient
	private Date cmsUpdateDate;
	
    @PrivateOwned
    @OrderBy("itemNum")
    @OneToMany(mappedBy="medOrder", cascade=CascadeType.ALL)
	@XmlElement(name="MedOrderItem")
	private List<MedOrderItem> medOrderItemList;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "PATIENT_ID")
    @XmlTransient
    private Patient patient;
    
    @Column(name = "ORG_PATIENT_ID")
    @XmlTransient
    private Long orgPatientId;

    @Column(name = "PREV_PATIENT_ID")
    @XmlTransient
    private Long prevPatientId;
    
    @Column(name = "VET_BEFORE_FLAG", nullable = false)
    @XmlTransient
    private Boolean vetBeforeFlag;

    @Column(name = "VET_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	@XmlTransient
    private Date vetDate;
    
    @Column(name = "UNVET_FLAG", nullable = false)
    @XmlTransient
    private Boolean unvetFlag;
    
    @Column(name = "COMMENT_FLAG", nullable = false)
    @XmlTransient
    private Boolean commentFlag;
    
	@Column(name = "HLA_FLAG", nullable = false)
	@XmlTransient
	private Boolean hlaFlag;

	@Column(name = "ITEM_REMARK_CONFIRMED_FLAG", nullable = false)
	@XmlTransient
	private Boolean itemRemarkConfirmedFlag;
	
	@Column(name = "REF_PHARM_ORDER_ID")
	@XmlTransient
	private Long refPharmOrderId;

    @Column(name = "PRESC_TYPE_UPDATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	@XmlTransient
    private Date prescTypeUpdateDate;
	
	@Column(name = "URGENT_FLAG")
	@XmlTransient
	private Boolean urgentFlag;
	
	@Column(name = "PRE_VET_USER", length = 12)
	@XmlTransient
	private String preVetUser;
	
	@Column(name = "PRE_VET_USER_NAME", length = 150)
	@XmlTransient
	private String preVetUserName;	
	
	@Column(name = "PRE_VET_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	@XmlTransient
	private Date preVetDate;
	
	@Lob
	@Column(name = "CMS_EXTRA_XML")
	@Basic(fetch=FetchType.LAZY)
	@XmlTransient
	private String cmsExtraXml;
	
	@Column(name = "WITHHOLD_USER", length = 12)
	@XmlTransient
	private String withholdUser;
	
	@Column(name = "WITHHOLD_USER_NAME", length = 150)
	@XmlTransient
	private String withholdUserName;	
	
	@Column(name = "WITHHOLD_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	@XmlTransient	
	private Date withholdDate;

	@Column(name = "ORG_MED_ORDER_ID")
	@XmlElement
	private Long orgMedOrderId;
	
	@Column(name = "PREV_MED_ORDER_ID")
	@XmlElement
	private Long prevMedOrderId;

	@Lob
	@Column(name = "DH_PATIENT_XML")
	@Basic(fetch=FetchType.LAZY)
    @XmlTransient
	private String dhPatientXml;

    @Column(name = "DH_LATEST_FLAG")
    @XmlTransient
    private Boolean dhLatestFlag;

	@Converter(name = "MedOrder.onHandSourceType", converterClass = OnHandSourceType.Converter.class)
    @Convert("MedOrder.onHandSourceType")
	@Column(name="ON_HAND_SOURCE_TYPE", nullable = false, length = 1)
	@XmlElement
	private OnHandSourceType onHandSourceType;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE",      referencedColumnName = "HOSP_CODE",      nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	@XmlTransient
	private Workstore workstore;
	
	@Column(name = "HOSP_CODE", nullable = false, insertable = false, updatable = false)
	private String hospCode;	
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "MED_CASE_ID")
	@XmlElement
	private MedCase medCase;

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "TICKET_ID")
	@XmlTransient
	private Ticket ticket;
	
    @PrivateOwned
    @OneToMany(mappedBy="medOrder", cascade=CascadeType.ALL)
    @XmlTransient
	private List<MedOrderFm> medOrderFmList;
	
	@Transient
	@XmlTransient
	private DmMedicalOfficer dmMedicalOfficer;	
	
	@Transient
	@XmlTransient
	private List<PharmOrder> pharmOrderList;
	
	@Transient
	@XmlTransient
	private Boolean hasReversedItemAddRemark;

	@Transient
	@XmlTransient
	private Boolean isRevampPrevOrder;

	@Transient
	@XmlTransient
	private Boolean isUnvetPharmRemarkDeleteItem;
	
	@Transient
	@XmlTransient
	@IgnoredProperty
	private Boolean isChemoOrder;
		
	@Transient
	@XmlTransient
	private Boolean includeItemFlag;
	
	@Transient
	@XmlTransient
	@IgnoredProperty
	private DhPatient dhPatient;
	
	public MedOrder() {
    	maxItemNum = Integer.valueOf(0);
    	orderType = OrderType.OutPatient;
    	cmsOrderType = CmsOrderType.Pharmacy;
    	cmsOrderSubType = CmsOrderSubType.OutPatient;
    	prescType = MedOrderPrescType.Out;
    	docType = MedOrderDocType.Normal;
    	status = MedOrderStatus.Outstanding;
    	processingFlag = Boolean.FALSE;
    	pregnancyCheckFlag = Boolean.FALSE;
    	remarkStatus = RemarkStatus.None;
    	hasReversedItemAddRemark = Boolean.FALSE;
    	vetBeforeFlag = Boolean.FALSE;
    	unvetFlag = Boolean.FALSE;
    	commentFlag = Boolean.FALSE;
		hlaFlag = Boolean.FALSE;
		itemRemarkConfirmedFlag = Boolean.FALSE;
		urgentFlag = Boolean.FALSE;
		isChemoOrder = Boolean.FALSE;
	}
    
    @PostLoad
    public void postLoad() {
    	if (StopWatcher.isDebugEnabled()) {
    		StopWatcher.resume();
    	}

    	if (Identity.instance().isLoggedIn()) {
    		if (doctorCode != null) {			
    			dmMedicalOfficer = DmMedicalOfficerCacher.instance().getDmMedicalOfficerByMedicalOfficerCode(workstore, doctorCode);
    		}
    		
    	}
    	
    	if (dhPatientXml != null && dhPatient == null) {
    		JaxbWrapper<DhPatient> dhPatientJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_PATIENT);
    		dhPatient = dhPatientJaxbWrapper.unmarshall(dhPatientXml);
    	}
		
    	if (StopWatcher.isDebugEnabled()) {
    		StopWatcher.suspend();
    	}
    }

	@PrePersist
	@PreUpdate
	public void preSave() {
		if (dhPatient != null) {
			JaxbWrapper<DhPatient> dhPatientJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_PATIENT);
			dhPatientXml = dhPatientJaxbWrapper.marshall(dhPatient);
		}
	}
	
    public void loadDmInfo() {
    	for (MedOrderItem item : getMedOrderItemList()) {
    		item.loadDmInfo();
    	}    	
    }
    
    public void clearDmInfo() {
    	for (MedOrderItem item : getMedOrderItemList()) {
    		item.clearDmInfo();
    	}    	
    }
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public Date getOrderDate() {
		if (orderDate == null) {
			return null;
		}
		else {
			return new Date(orderDate.getTime());
		}
	}

	public void setOrderDate(Date orderDate) {
		if (orderDate == null) {
			this.orderDate = null;
		}
		else {
			this.orderDate = new Date(orderDate.getTime());
		}
	}

	public String getRefNum() {
		return refNum;
	}

	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}
	
	public MedOrderPrescType getPrescType() {
		return prescType;
	}

	public void setPrescType(MedOrderPrescType prescType) {
		this.prescType = prescType;
	}
	
	public OrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}

	public MedOrderDocType getDocType() {
		return docType;
	}

	public void setDocType(MedOrderDocType docType) {
		this.docType = docType;
	}

	public String getPrevOrderNum() {
		return prevOrderNum;
	}

	public void setPrevOrderNum(String prevOrderNum) {
		this.prevOrderNum = prevOrderNum;
	}

	public Boolean getPregnancyCheckFlag() {
		return pregnancyCheckFlag;
	}

	public void setPregnancyCheckFlag(Boolean pregnancyCheckFlag) {
		this.pregnancyCheckFlag = pregnancyCheckFlag;
	}

	public Integer getMaxItemNum() {
		return maxItemNum;
	}

	public void setMaxItemNum(Integer maxItemNum) {
		this.maxItemNum = maxItemNum;
	}

	public Boolean getProcessingFlag() {
		return processingFlag;
	}

	public void setProcessingFlag(Boolean processingFlag) {
		this.processingFlag = processingFlag;
	}

	public CmsOrderType getCmsOrderType() {
		return cmsOrderType;
	}

	public void setCmsOrderType(CmsOrderType cmsOrderType) {
		this.cmsOrderType = cmsOrderType;
	}

	public CmsOrderSubType getCmsOrderSubType() {
		return cmsOrderSubType;
	}

	public void setCmsOrderSubType(CmsOrderSubType cmsOrderSubType) {
		this.cmsOrderSubType = cmsOrderSubType;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public String getDoctorRankCode() {
		return doctorRankCode;
	}

	public void setDoctorRankCode(String doctorRankCode) {
		this.doctorRankCode = doctorRankCode;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public MedOrderStatus getStatus() {
		return status;
	}

	public void setStatus(MedOrderStatus status) {
		this.status = status;
	}

	public Long getWorkstationId() {
		return workstationId;
	}

	public void setWorkstationId(Long workstationId) {
		this.workstationId = workstationId;
	}

	public RemarkStatus getRemarkStatus() {
		return remarkStatus;
	}

	public void setRemarkStatus(RemarkStatus remarkStatus) {
		this.remarkStatus = remarkStatus;
	}

	public String getEisSpecCode() {
		return eisSpecCode;
	}

	public void setEisSpecCode(String eisSpecCode) {
		this.eisSpecCode = eisSpecCode;
	}

	public Date getCmsUpdateDate() {
		return cmsUpdateDate;
	}

	public void setCmsUpdateDate(Date cmsUpdateDate) {
		this.cmsUpdateDate = cmsUpdateDate;
	}

	public List<MedOrderItem> getMedOrderItemList() {
		if (medOrderItemList == null) {
			medOrderItemList = new ArrayList<MedOrderItem>();
		}
		return medOrderItemList;
	}
	
	public void setMedOrderItemList(List<MedOrderItem> medOrderItemList) {
		this.medOrderItemList = medOrderItemList;
	}
	
	public void addMedOrderItem(MedOrderItem medOrderItem) {
		medOrderItem.setMedOrder(this);
		getMedOrderItemList().add(medOrderItem);
	}
	
	public void clearMedOrderItemList() {
		medOrderItemList = new ArrayList<MedOrderItem>();
	}
	
	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public List<PharmOrder> getPharmOrderList() {
		if (pharmOrderList == null) {
			pharmOrderList = new ArrayList<PharmOrder>();
		}
		return pharmOrderList;
	}

	public void setPharmOrderList(List<PharmOrder> pharmOrderList) {
		this.pharmOrderList = pharmOrderList;
	}
	
	public void addPharmOrder(PharmOrder pharmOrder) {
		pharmOrder.setMedOrder(this);
		this.getPharmOrderList().add(pharmOrder);
	}
	
	public void clearPharmOrderList() {
		pharmOrderList = new ArrayList<PharmOrder>();
	}	
	
	public DmMedicalOfficer getDmMedicalOfficer() {
		return dmMedicalOfficer;
	}

	public void setDmMedicalOfficer(DmMedicalOfficer dmMedicalOfficer) {
		this.dmMedicalOfficer = dmMedicalOfficer;
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}
	
	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public MedCase getMedCase() {
		return medCase;
	}

	public void setMedCase(MedCase medCase) {
		this.medCase = medCase;
	}

	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}
	
	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public List<MedOrderFm> getMedOrderFmList() {
		if (medOrderFmList == null) {
			medOrderFmList = new ArrayList<MedOrderFm>();
		}
		return medOrderFmList;
	}

	public void setMedOrderFmList(List<MedOrderFm> medOrderFmList) {
		this.medOrderFmList = medOrderFmList;
	}
	
	public void addMedOrderFm(MedOrderFm medOrderFm) {
		medOrderFm.setMedOrder(this);
		getMedOrderFmList().add(medOrderFm);
	}
	
	public void clearMedOrderFmList() {
		medOrderFmList = new ArrayList<MedOrderFm>();
	}

	public Boolean getHasReversedItemAddRemark() {
		return hasReversedItemAddRemark;
	}

	public void setHasReversedItemAddRemark(Boolean hasReversedItemAddRemark) {
		this.hasReversedItemAddRemark = hasReversedItemAddRemark;
	}
	
	public Boolean getIsRevampPrevOrder() {
		return isRevampPrevOrder;
	}

	public void setIsRevampPrevOrder(Boolean isRevampPrevOrder) {
		this.isRevampPrevOrder = isRevampPrevOrder;
	}

	public Boolean getIsUnvetPharmRemarkDeleteItem() {
		return isUnvetPharmRemarkDeleteItem;
	}

	public void setIsUnvetPharmRemarkDeleteItem(Boolean isUnvetPharmRemarkDeleteItem) {
		this.isUnvetPharmRemarkDeleteItem = isUnvetPharmRemarkDeleteItem;
	}

	public Boolean getIsChemoOrder() {
		return isChemoOrder;
	}

	public void setIsChemoOrder(Boolean isChemoOrder) {
		this.isChemoOrder = isChemoOrder;
	}

	public Boolean getIncludeItemFlag() {
		return includeItemFlag;
	}

	public void setIncludeItemFlag(Boolean includeItemFlag) {
		this.includeItemFlag = includeItemFlag;
	}

	public String getYear() {
		return DateTimeFormat.forPattern(YYYY).print(new DateTime(this.getOrderDate()));
	}

	@Transient
	private transient boolean childLoaded = false;
	
	public boolean isManualProfile() {
		return orderNum == null || orderNum.length() > 12;
	}

	public DhPatient getDhPatient() {
		return dhPatient;
	}

	public void setDhPatient(DhPatient dhPatient) {
		this.dhPatient = dhPatient;
	}

	public void loadChild() {
		
		if (getId() == null) return;

		if (!childLoaded) {			
			childLoaded = true;

			this.getTicket();
			this.getMedCase();
			this.getPatient();
			this.getWorkstore();
			
			this.getMedOrderFmList().size();
			
			for (MedOrderItem item : this.getMedOrderItemList()) {
				item.getMedOrder();
				
				for (MedOrderItemAlert medOrderItemAlert : item.getMedOrderItemAlertList()) {
					medOrderItemAlert.getMedOrderItem();
				}
			}
			
			// finally init the list
			this.clearPharmOrderList();
			
			for (MedOrderItem item : this.getMedOrderItemList()) {
				item.clearPharmOrderItemList();
			}
			
	    	this.markFmIndFlag();
	    	this.markItemHlaFlag();
	    	this.markItemOrderDate();
	    	
		}
	}
	
	public void removeDeletedMedOrderItem(MedOrderItem medOrderItem) {
		this.getMedOrderItemList().remove(medOrderItem);
		for (PharmOrderItem pharmOrderItem : medOrderItem.getPharmOrderItemList()) {
			for ( RefillScheduleItem refillScheduleItem : pharmOrderItem.getRefillScheduleItemList() ){
				pharmOrderItem.getPharmOrder().getRefillScheduleList().remove(refillScheduleItem.getRefillSchedule());				
			}
			pharmOrderItem.getPharmOrder().getPharmOrderItemList().remove(pharmOrderItem);
		}
		
		for (MedOrderItem moi : getMedOrderItemList()) {
			Set<MedOrderItemAlert> deleteMoiAlertSet = new HashSet<MedOrderItemAlert>();
			for (MedOrderItemAlert moiAlert : moi.getDdiAlertList()) {
				AlertDdim alertDdim = (AlertDdim) moiAlert.getAlert();
				if ((StringUtils.equals(alertDdim.getOrderNum1(), orderNum) && medOrderItem.getItemNum().intValue() == alertDdim.getItemNum1().intValue())
				 || (StringUtils.equals(alertDdim.getOrderNum2(), orderNum) && medOrderItem.getItemNum().intValue() == alertDdim.getItemNum2().intValue())) {
					deleteMoiAlertSet.add(moiAlert);
				}
			}
			moi.getMedOrderItemAlertList().removeAll(deleteMoiAlertSet);
			moi.markAlertFlag();
		}
	}
	
	public void removeSysDetetedItem() {
		Set<MedOrderItem> deleteSet = new HashSet<MedOrderItem>();
		for (MedOrderItem moi : getMedOrderItemList()) {
			if (moi.getStatus() == MedOrderItemStatus.SysDeleted) {
				deleteSet.add(moi);
			}
		}
		getMedOrderItemList().removeAll(deleteSet);
	}
	
	public MedOrderItem getMedOrderItemByItemNum(Integer itemNum) {
		for (MedOrderItem item : this.getMedOrderItemList()) {
			if (item.getItemNum().equals(itemNum)) {
				return item;
			}
		}
		return null;
	}
	
	public MedOrderItem getMedOrderItemByOrgItemNum(Integer orgItemNum) {
		for (MedOrderItem item : this.getMedOrderItemList()) {
			if (item.getOrgItemNum().equals(orgItemNum)) {
				return item;
			}
		}
		return null;
	}
	
	public void clearId() {
		
		this.setId(null);
		
		for (MedOrderFm medOrderFm : this.getMedOrderFmList()) {
			medOrderFm.setId(null);
		}
		
		for (MedOrderItem medOrderItem : this.getMedOrderItemList()) {
			medOrderItem.setId(null);
			
			for (MedOrderItemAlert medOrderItemAlert : medOrderItem.getMedOrderItemAlertList()) {
				medOrderItemAlert.setId(null);
			}
		}
	}
	
	public void markPharmRemarkStatus() {
		int unconfirmRemarkNum = 0;
		int confirmRemarkNum = 0;
		for (MedOrderItem medOrderItem : this.getMedOrderItemList()) {
			if (medOrderItem.getRemarkStatus() == RemarkStatus.Unconfirm) {
				unconfirmRemarkNum++;
			} else if (medOrderItem.getRemarkStatus() == RemarkStatus.Confirm) {
				confirmRemarkNum++;
			}
		}
		if (unconfirmRemarkNum > 0) {
			this.setRemarkStatus(RemarkStatus.Unconfirm);
		} else if (confirmRemarkNum > 0 || this.getItemRemarkConfirmedFlag()) {
			this.setRemarkStatus(RemarkStatus.Confirm);
		} else {
			this.setRemarkStatus(RemarkStatus.None);
		}
	}

	public List<DispOrder> markDeleted() {
		if (this.getStatus() == MedOrderStatus.SysDeleted) {
			return new ArrayList<DispOrder>();
		}
		
		return markDeleted(MedOrderStatus.Deleted, true);
	}
	
	public List<DispOrder> markSysDeleted() {
		return markDeleted(MedOrderStatus.SysDeleted, true);
	}
	
	public List<DispOrder> markDeleted(MedOrderStatus status, boolean cascade) {

		this.setVetBeforeFlag(Boolean.FALSE);
		
		List<DispOrder> delDispOrderList = new ArrayList<DispOrder>();
		 
		if (this.getStatus() != status) {
			this.setStatus(status);
			
			if (cascade) {
				for (PharmOrder pharmOrder : this.getPharmOrderList()) {
					if (status == MedOrderStatus.SysDeleted) {
						delDispOrderList.addAll(pharmOrder.markDeleted(PharmOrderStatus.SysDeleted));
					} else if (status == MedOrderStatus.Deleted) {
						delDispOrderList.addAll(pharmOrder.markDeleted(PharmOrderStatus.Deleted));
					}
				}
			}
		}
		
		return delDispOrderList;
	}
	
	public boolean hasChanged() {
		return ((docType == MedOrderDocType.Manual && !isDhOrder())
			|| hasReversedItemAddRemark
			|| hasItemChanged());
	}
	
	private boolean hasItemChanged() {
		for (MedOrderItem medOrderItem : getMedOrderItemList()) {
			if (medOrderItem.getChangeFlag()) {
				return true;
			}
		}		
		return false;
	}	
	
	public Long getOrgPatientId() {
		return orgPatientId;
	}

	public void setOrgPatientId(Long orgPatientId) {
		this.orgPatientId = orgPatientId;
	}

	public Long getPrevPatientId() {
		return prevPatientId;
	}

	public void setPrevPatientId(Long prevPatientId) {
		this.prevPatientId = prevPatientId;
	}
	
	public Boolean getVetBeforeFlag() {
		return vetBeforeFlag;
	}

    public void setVetBeforeFlag(Boolean vetBeforeFlag) {
		this.vetBeforeFlag = vetBeforeFlag;
	}

	public Date getVetDate() {
		if (vetDate == null) {
			return null;
		}
		else {
			return new Date(vetDate.getTime());
		}
	}

	public void setVetDate(Date vetDate) {
		if (vetDate == null) {
			this.vetDate = null;
		}
		else {
			this.vetDate = new Date(vetDate.getTime());
		}
	}

	public Boolean getUnvetFlag() {
		return unvetFlag;
	}

	public void setUnvetFlag(Boolean unvetFlag) {
		this.unvetFlag = unvetFlag;
	}

	@Transient
	private transient List<PharmOrder> _pharmOrderList = null;

	public void disconnectPharmOrderList() {
		
		_pharmOrderList = this.getPharmOrderList();
		this.clearPharmOrderList();
		
		for (MedOrderItem item : this.getMedOrderItemList()) {
			item.clearPharmOrderItemList();
		}
	}
	
	public void reconnectPharmOrderList() {
		
		this.setPharmOrderList(_pharmOrderList);
		
		for (PharmOrder pharmOrder : _pharmOrderList) {
			for (PharmOrderItem pharmOrderItem : pharmOrder.getPharmOrderItemList()) {
				pharmOrderItem.getMedOrderItem().addPharmOrderItem(pharmOrderItem);
			}
		}
	}
	
	public void markOutstanding() {
		this.setStatus(MedOrderStatus.Outstanding);		
		for (MedOrderItem item : this.getMedOrderItemList()) {			
			item.setStatus(MedOrderItemStatus.Outstanding);
		}		
	}

	public void markPrevet() {
		this.setStatus(MedOrderStatus.PreVet);		
		for (MedOrderItem item : this.getMedOrderItemList()) {			
			item.setStatus(MedOrderItemStatus.Outstanding);
		}		
	}
	
	public void markFmIndFlag() {
		if (medOrderItemList == null || medOrderFmList == null) {
			return;
		}
		
		Set<StringArray> fmKeySet = new HashSet<StringArray>();
		for (MedOrderFm fm : medOrderFmList) {
			fmKeySet.add(new StringArray(
					fm.getDisplayName(),
					fm.getFormCode(),
					fm.getSaltProperty(),
					fm.getFmStatus().getDataValue()
				));
		}
		
		for (MedOrderItem moi : medOrderItemList) {
			switch (moi.getRxItemType()) {
				case Oral:
				case Injection:
					moi.setFmIndFlag(fmKeySet.contains(new StringArray(
							moi.getRxDrug().getDisplayName(),
							moi.getRxDrug().getFormCode(),
							moi.getRxDrug().getSaltProperty(),
							moi.getRxDrug().getFmStatus()
						)));
					break;
					
				case Iv:
					Boolean tempFmIndFlag = Boolean.FALSE;
					for (IvAdditive ivAdditive : moi.getIvRxDrug().getIvAdditiveList()) {
						if ( fmKeySet.contains(new StringArray(
								ivAdditive.getDisplayName(),
								ivAdditive.getFormCode(),
								ivAdditive.getSaltProperty(),
								ivAdditive.getFmStatus()
							)) ) {
							tempFmIndFlag = Boolean.TRUE;
							break;
						}
					}
					moi.setFmIndFlag(tempFmIndFlag);
					
					break;
					
				default:
					moi.setFmIndFlag(Boolean.FALSE);	// capd case
			}
		}
	}
	
	public void markHlaFlag(String hlaDisplayName) {
		hlaFlag = Boolean.FALSE;
		for (MedOrderItem medOrderItem : getMedOrderItemList()) {
			if (medOrderItem.getDisplayName() != null && medOrderItem.getDisplayName().equals(hlaDisplayName)) {
				hlaFlag = Boolean.TRUE;
				return;
			}
		}
	}
	
	public void markItemHlaFlag() {
		for (MedOrderItem moi : getMedOrderItemList()) {
			Boolean flag = Boolean.FALSE;
			for (MedOrderItemAlert moia : moi.getMedOrderItemAlertList()) {
				if (moia.getAlertType() == AlertType.OpHla || moia.getAlertType() == AlertType.IpHla) {
					flag = Boolean.TRUE;
					break;
				}
			}
			moi.getRxItem().setHlaFlag(flag);
		}
	}
	
	public void markItemOrderDate() {
		for (MedOrderItem moi : getMedOrderItemList()) {
			moi.getRxItem().setOrderDate(orderDate);
		}
	}
	
	public void replaceOrderNum(String newOrderNum) {

		String oldOrderNum = orderNum;
		orderNum = newOrderNum;

		for (MedOrderItem moi : getMedOrderItemList()) {
			for (MedOrderItemAlert moia : moi.getMedOrderItemAlertList()) {
				moia.getAlert().replaceOrderNum(oldOrderNum, newOrderNum);
				moia.setAlertXml(null);
				moia.preSave();
			}
		}
	}
	
	public List<MedOrderItemAlert> getMedOrderItemAlertListByHashCode(int hashCode) {
		List<MedOrderItemAlert> list = new ArrayList<MedOrderItemAlert>();
		for (MedOrderItem medOrderItem : getMedOrderItemList()) {
			for (MedOrderItemAlert medOrderItemAlert : medOrderItem.getMedOrderItemAlertList()) {
				if (medOrderItemAlert.getAlert().hashCode() == hashCode) {
					list.add(medOrderItemAlert);
				}
			}
		}
		return list;
	}
	
	public MedOrderItemAlert getDdiRelatedAlert(Integer itemNum, AlertDdim alertDdim) {
		Set<Integer> itemNumSet = new HashSet<Integer>();
		for (MedOrderItem moi : getMedOrderItemList()) {
			itemNumSet.add(moi.getItemNum());
		}

		Integer otherItemNum;
		if (alertDdim.getItemNum1().intValue() == itemNum.intValue()) {
			otherItemNum = Integer.valueOf(alertDdim.getItemNum2().intValue());
		}
		else {
			otherItemNum = Integer.valueOf(alertDdim.getItemNum1().intValue());
		}
		
		MedOrderItem otherMoi = getMedOrderItemByItemNum(otherItemNum);
		if (otherMoi == null) {
			return null;
		}
		for (MedOrderItemAlert moiAlert : otherMoi.getMedOrderItemAlertList()) {
			if (!moiAlert.isDdiAlert()) {
				continue;
			}
			AlertDdim otherAlertDdim = (AlertDdim) moiAlert.getAlert();
			if (otherAlertDdim.isOnHand()) {
				continue;
			}
			
			if (otherAlertDdim.hashCode() == alertDdim.hashCode()) {
				return moiAlert;
			}

			Integer otherItemNum1 = Integer.valueOf(otherAlertDdim.getItemNum1().intValue());
			Integer otherItemNum2 = Integer.valueOf(otherAlertDdim.getItemNum2().intValue());
			if (!itemNumSet.contains(otherItemNum1) || !itemNumSet.contains(otherItemNum2)) {
				return moiAlert;
			}
		}
		return null;
	}
	
	public String getClusterCode() {
		return this.getWorkstore().getHospital().getHospitalCluster().getClusterCode();
	}
	
	public Integer getAndIncreaseMaxItemNum() {
		synchronized(this){
			maxItemNum = Integer.valueOf(maxItemNum.intValue() + 1);
			return maxItemNum;
		}
	}

	public void setCommentFlag(Boolean commentFlag) {
		this.commentFlag = commentFlag;
	}

	public Boolean getCommentFlag() {
		return commentFlag;
	}
	
	public Boolean getHlaFlag() {
		return hlaFlag;
	}

	public void setHlaFlag(Boolean hlaFlag) {
		this.hlaFlag = hlaFlag;
	}

	public Boolean getItemRemarkConfirmedFlag() {
		return itemRemarkConfirmedFlag;
	}

	public void setItemRemarkConfirmedFlag(Boolean itemRemarkConfirmedFlag) {
		this.itemRemarkConfirmedFlag = itemRemarkConfirmedFlag;
	}
	
	public void resetNumOfLabel() {
		for (MedOrderItem moi : getMedOrderItemList()) {
			moi.setNumOfLabel(Integer.valueOf(1));
		}
	}
	
	public void updateMaxItemNum(){
		int maxItemNum = 0;
		for( MedOrderItem medOrderItem : this.getMedOrderItemList() ){
			if( medOrderItem.getItemNum().intValue() > maxItemNum ){
				maxItemNum = medOrderItem.getItemNum().intValue();
			}
		}
		this.setMaxItemNum(Integer.valueOf(maxItemNum));
	}

	public void setRefPharmOrderId(Long refPharmOrderId) {
		this.refPharmOrderId = refPharmOrderId;
	}

	public Long getRefPharmOrderId() {
		return refPharmOrderId;
	}
	
	public Date getPrescTypeUpdateDate() {
		if (prescTypeUpdateDate == null) {
			return null;
		}
		else {
			return new Date(prescTypeUpdateDate.getTime());
		}
	}

	public void setPrescTypeUpdateDate(Date prescTypeUpdateDate) {
		if (prescTypeUpdateDate == null) {
			this.prescTypeUpdateDate = null;
		}
		else {
			this.prescTypeUpdateDate = new Date(prescTypeUpdateDate.getTime());
		}
	}	
	
	public Boolean getUrgentFlag() {
		return urgentFlag;
	}

	public void setUrgentFlag(Boolean urgentFlag) {
		this.urgentFlag = urgentFlag;
	}
	
	public String getPreVetUser() {
		return preVetUser;
	}

	public void setPreVetUser(String preVetUser) {
		this.preVetUser = preVetUser;
	}
	
	public Date getPreVetDate() {
		return preVetDate;
	}
	
	public void setPreVetUserName(String preVetUserName) {
		this.preVetUserName = preVetUserName;
	}

	public String getCmsExtraXml() {
		return cmsExtraXml;
	}

	public void setCmsExtraXml(String cmsExtraXml) {
		this.cmsExtraXml = cmsExtraXml;
	}

	public String getPreVetUserName() {
		return preVetUserName;
	}

	public void setPreVetDate(Date preVetDate) {
		this.preVetDate = preVetDate;
	}

	public boolean isMpDischarge() {
		return getPrescType() == MedOrderPrescType.MpDischarge || getPrescType() == MedOrderPrescType.MpHomeLeave;
	}

	public void setWithholdUser(String withHoldUser) {
		this.withholdUser = withHoldUser;
	}

	public String getWithholdUser() {
		return withholdUser;
	}

	public void setWithholdUserName(String withHoldUserName) {
		this.withholdUserName = withHoldUserName;
	}

	public String getWithholdUserName() {
		return withholdUserName;
	}

	public void setWithholdDate(Date withholdDate) {
		this.withholdDate = withholdDate;
	}

	public Date getWithholdDate() {
		return withholdDate;
	}
	
	public Long getOrgMedOrderId() {
		return orgMedOrderId;
	}

	public void setOrgMedOrderId(Long orgMedOrderId) {
		this.orgMedOrderId = orgMedOrderId;
	}

	public Long getPrevMedOrderId() {
		return prevMedOrderId;
	}

	public void setPrevMedOrderId(Long prevMedOrderId) {
		this.prevMedOrderId = prevMedOrderId;
	}

	public String getDhPatientXml() {
		return dhPatientXml;
	}

	public void setDhPatientXml(String dhPatientXml) {
		this.dhPatientXml = dhPatientXml;
	}
	
	public Boolean getDhLatestFlag() {
		return dhLatestFlag == null ? false : dhLatestFlag;
	}

	public void setDhLatestFlag(Boolean dhLatestFlag) {
		this.dhLatestFlag = dhLatestFlag;
	}

	public OnHandSourceType getOnHandSourceType() {
		return onHandSourceType;
	}

	public void setOnHandSourceType(OnHandSourceType onHandSourceType) {
		this.onHandSourceType = onHandSourceType;
	}

	public boolean isDiscontinue() {
		return !getDiscontinueItem().isEmpty();
	}
	
	public List<MedOrderItem> getDiscontinueItem() {
		List<MedOrderItem> moiList = new ArrayList<MedOrderItem>();
		for (MedOrderItem moi : getMedOrderItemList()) {
			if (moi.getDiscontinueStatus() == DiscontinueStatus.Confirmed || moi.getDiscontinueStatus() == DiscontinueStatus.Pending) {
				moiList.add(moi);
			}
		}
		return moiList;
	}
	
	public boolean isDhOrder() {
		return prescType == MedOrderPrescType.Dh;
	}
	
	public Integer getDhOrderVersion() {
		if (!isDhOrder()) {
			return null;
		}
		else if (getRefNum() == null) {
			return Integer.valueOf(0);
		}
		else {
			return Integer.valueOf(getRefNum());
		}
	}
}