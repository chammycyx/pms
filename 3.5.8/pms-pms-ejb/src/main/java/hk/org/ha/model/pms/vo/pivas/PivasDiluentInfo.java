package hk.org.ha.model.pms.vo.pivas;

import hk.org.ha.model.pms.dms.persistence.DmDiluent;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrugManuf;
import hk.org.ha.model.pms.dms.udt.pivas.ActionType;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PivasDiluentInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String itemCode;
	
	private DmDrug dmDrug;
	
	private ActionType actionType;

	private List<PivasDrugManuf> pivasDrugManufList;
	
	private DmDiluent dmDiluent;
 
	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public ActionType getActionType() {
		return actionType;
	}

	public void setActionType(ActionType actionType) {
		this.actionType = actionType;
	}

	public void setPivasDrugManufList(List<PivasDrugManuf> pivasDrugManufList) {
		this.pivasDrugManufList = pivasDrugManufList;
	}

	public List<PivasDrugManuf> getPivasDrugManufList() {
		return pivasDrugManufList;
	}

	public DmDrug getDmDrug() {
		return dmDrug;
	}

	public void setDmDrug(DmDrug dmDrug) {
		this.dmDrug = dmDrug;
	}

	public DmDiluent getDmDiluent() {
		return dmDiluent;
	}

	public void setDmDiluent(DmDiluent dmDiluent) {
		this.dmDiluent = dmDiluent;
	}

}