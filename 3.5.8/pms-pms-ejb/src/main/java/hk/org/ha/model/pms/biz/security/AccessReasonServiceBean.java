package hk.org.ha.model.pms.biz.security;

import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.web.MeasureCalls;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Stateful
@Name("accessReasonService")
@Scope(ScopeType.SESSION)
@RemoteDestination
@MeasureCalls
public class AccessReasonServiceBean implements AccessReasonServiceLocal {
		
	@In
	private AuditLogger auditLogger;
	
	public void saveLogonReason(String reason)
	{
		auditLogger.log("#0003", reason);
	}
	
	@Remove
	public void destroy() {
	}
}