package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.pas.PasException;
import hk.org.ha.model.pms.cache.PasSpecialtyCacherInf;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.DrsConfig;
import hk.org.ha.model.pms.udt.patient.PasSpecialtyType;
import hk.org.ha.model.pms.vo.patient.PasSpecialty;
import hk.org.ha.model.pms.vo.reftable.DrsConfigInfo;

import java.util.Collection;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("drsConfigListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DrsConfigListServiceBean implements DrsConfigListServiceLocal 
{
	@PersistenceContext
	private EntityManager em;

	@In
	private Workstore workstore;
	
	@In
	private PasSpecialtyCacherInf pasSpecialtyCacher;

	@Out(required = false)
	private PasSpecialty pasSpecialty;

	@SuppressWarnings("unchecked")
	public DrsConfigInfo retrieveDrsConfigList() throws PasException {	
		List<DrsConfig> refillDrsConfigList = em.createQuery(
									"select o from DrsConfig o" + // 20171023 index check : DrsConfig.workstore : FK_DRS_CONFIG_01
									" where o.workstore = :workstore" +
									" order by o.type, o.pasSpecCode")
									.setParameter("workstore", workstore)
									.getResultList();
		
		List<PasSpecialty> refillModulePasSpecialtyList = pasSpecialtyCacher.getPasSpecialtyList();
		
		DrsConfigInfo drsConfigInfo = new DrsConfigInfo();
		drsConfigInfo.setRefillModulePasSpecialtyList(refillModulePasSpecialtyList);
		drsConfigInfo.setDrsConfigList(refillDrsConfigList);
		return drsConfigInfo;
	}
	
	public List<PasSpecialty> retrievePasSpecList(String prefixSpecCode, PasSpecialtyType specType) throws PasException {				 
		List<PasSpecialty> drsConfigPasSpecialtyList = pasSpecialtyCacher.getPasSpecialtyListBySpecCode(prefixSpecCode, specType);		
		return drsConfigPasSpecialtyList;
	}
	
	public void updateDrsConfigList(Collection<DrsConfig> updateDrsConfigList, Collection<DrsConfig> delDrsConfigList) throws PasException {
		workstore = em.find(Workstore.class, workstore.getId());

		if (!delDrsConfigList.isEmpty()) {
			for (DrsConfig delDrsConfig:delDrsConfigList){
				if (delDrsConfig.getId()!=null){
					em.remove(em.merge(delDrsConfig));
				}
			}
		}
		em.flush();
		
		for(DrsConfig drsConfig:updateDrsConfigList)
		{
			if (drsConfig.getId() == null) {
				drsConfig.setWorkstore(workstore);
				em.persist(drsConfig);
			} else {
				drsConfig.setWorkstore(workstore);
				em.merge(drsConfig);
			}
			
		}		
		em.flush();
		em.clear();
	}
	
	@Remove
	public void destroy() 
	{
	}	
}
