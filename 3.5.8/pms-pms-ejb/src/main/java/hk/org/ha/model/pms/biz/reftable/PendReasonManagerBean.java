package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.PendingReason;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.util.Collection;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateless
@Name("pendReasonManager")
@Restrict("#{identity.loggedIn}")
@MeasureCalls
public class PendReasonManagerBean implements PendReasonManagerLocal {

	@PersistenceContext
	private EntityManager em;
	
	@In
	private Workstore workstore;
	
	@SuppressWarnings("unchecked")
	public List<PendingReason> retrievePendingReasonList() {
		return em.createQuery(
				"select o from PendingReason o" + // 20120303 index check : PendingReason.hospital : FK_PENDING_REASON_01
				" where o.hospital = :hospital" +
				" and o.status = :status" +
				" order by o.pendingCode")
				.setParameter("hospital", workstore.getHospital())
				.setParameter("status", RecordStatus.Active)
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public void updatePendingReasonList(Collection<PendingReason> newPendingReasonList) {
		Hospital hospital = workstore.getHospital();

		for (PendingReason pendingReason : newPendingReasonList) {
			if (pendingReason.getStatus()== RecordStatus.Delete) {
				em.merge(pendingReason);
			} else {
				List pendingReasonList = em.createQuery(
						"select o from PendingReason o" + // 20120303 index check : PendingReason.hospital,pendingCode : UI_PENDING_REASON_01
						" where o.hospital= :hospital" +
						" and o.pendingCode = :pendingCode")
						.setParameter("hospital", hospital)
						.setParameter("pendingCode", pendingReason.getPendingCode()).getResultList();

				if (!pendingReasonList.isEmpty()) {
					PendingReason pendingReasonObj = ((PendingReason) pendingReasonList.get(0));					
					pendingReasonObj.setStatus(RecordStatus.Active);
					pendingReasonObj.setDescription(pendingReason.getDescription());
				} else {
					pendingReason.setHospital(hospital);
					em.persist(pendingReason);
				}				
			}			
		}
		em.flush();		
	}
}
