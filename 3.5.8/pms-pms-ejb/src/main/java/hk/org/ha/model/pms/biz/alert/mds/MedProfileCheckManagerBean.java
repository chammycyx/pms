package hk.org.ha.model.pms.biz.alert.mds;

import static hk.org.ha.model.pms.prop.Prop.*;
import hk.org.ha.fmk.pms.remote.marshall.EclipseLinkXStreamMarshaller;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.AlertEntity;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItemAlert;
import hk.org.ha.model.pms.udt.medprofile.EndType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
import hk.org.ha.model.pms.vo.alert.mds.AlertDdim;
import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;
import hk.org.ha.model.pms.vo.alert.mds.ManualProfileCheckResult;
import hk.org.ha.model.pms.vo.alert.mds.MdsResult;
import hk.org.ha.model.pms.vo.alert.mds.MedProfileAlertMsg;
import hk.org.ha.model.pms.vo.alert.mds.MedProfileCheckResult;
import hk.org.ha.model.pms.vo.rx.IvAdditive;
import hk.org.ha.model.pms.vo.rx.IvRxDrug;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Remove;
import javax.ejb.Stateless;

import org.apache.commons.beanutils.PropertyUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("medProfileCheckManager")
@MeasureCalls
public class MedProfileCheckManagerBean implements MedProfileCheckManagerLocal {

	private static final MedProfileAlertMsgComparator medProfileAlertMsgComparator = new MedProfileAlertMsgComparator();
	
	private static final String JAXB_CONTEXT_RX = "hk.org.ha.model.pms.vo.rx";

	@Logger
	private Log logger;

	@In
	private AlertRetrieveManagerLocal alertRetrieveManager;
	
	@In
	private AlertMsgManagerLocal alertMsgManager;
	
	@In
	private AlertComparatorInf alertComparator;

	public MedProfileCheckResult checkMedProfileAlertForSaveProfile(MedProfile medProfile, List<MedProfileItem> medProfileItemList) {
		if (!VETTING_MEDPROFILE_ALERT_MDS_CHECK.get(Boolean.FALSE) || !ALERT_MDS_ENABLED.get(Boolean.FALSE) || medProfileItemList.size() == 0) {
			return null;
		}

		JaxbWrapper<RxItem> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);

		List<MedProfileItem> newList = new ArrayList<MedProfileItem>();

		for (MedProfileItem medProfileItem : medProfileItemList) {
			if( MedProfileItemStatus.Ended == medProfileItem.getStatus() && EndType.WardTransfered == medProfileItem.getEndType() ){
				continue;
			}
			
			if( medProfileItem.getMedProfileMoItem() == null ){
				continue;
			}
			
			if( medProfileItem.getMedProfileMoItem().isManualItem() ){
				if( MedProfileItemStatus.Deleted == medProfileItem.getStatus() || MedProfileItemStatus.Discontinued == medProfileItem.getStatus()){
					continue;
				}
			}
			
			MedProfileItem mpi = new MedProfileItem();
			MedProfileMoItem mpMoi = new MedProfileMoItem();
			try {
				PropertyUtils.copyProperties(mpi, medProfileItem);
				PropertyUtils.copyProperties(mpMoi, medProfileItem.getMedProfileMoItem());
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
			mpi.setMedProfileMoItem(mpMoi);
			mpMoi.setMedProfileItem(mpi);
			if (mpMoi.getRxItem() != null) {
				mpMoi.setRxItem(rxJaxbWrapper.unmarshall(rxJaxbWrapper.marshall(mpMoi.getRxItem())));
			}
			newList.add(mpi);
		}

		return checkMedProfileAlert(medProfile, newList, false, true);
	}
	
	public MedProfileCheckResult checkMedProfileAlertForDueOrderPrint(MedProfile medProfile, List<MedProfileItem> medProfileItemList, boolean checkDdiFlag) {
    	
		JaxbWrapper<RxItem> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);

		List<MedProfileItem> newList = new ArrayList<MedProfileItem>();

		for (MedProfileItem medProfileItem : medProfileItemList) {
			if( MedProfileItemStatus.Ended == medProfileItem.getStatus() && EndType.WardTransfered == medProfileItem.getEndType() ){
				continue;
			}
			
			if( medProfileItem.getMedProfileMoItem() == null ){
				continue;
			}
			
			if( medProfileItem.getMedProfileMoItem().isManualItem() ){
				if( MedProfileItemStatus.Deleted == medProfileItem.getStatus() || MedProfileItemStatus.Discontinued == medProfileItem.getStatus()){
					continue;
				}
			}
			
			MedProfileItem mpi = new MedProfileItem();
			MedProfileMoItem mpMoi = new MedProfileMoItem();
			try {
				PropertyUtils.copyProperties(mpi, medProfileItem);
				PropertyUtils.copyProperties(mpMoi, medProfileItem.getMedProfileMoItem());
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
			mpi.setMedProfileMoItem(mpMoi);
			mpMoi.setMedProfileItem(mpi);
			if (mpMoi.getRxItem() != null) {
				mpMoi.setRxItem(rxJaxbWrapper.unmarshall(rxJaxbWrapper.marshall(mpMoi.getRxItem())));
			}
			newList.add(mpi);
		}
		
    	return checkMedProfileAlert(medProfile, newList, true, checkDdiFlag);
	}
	
	private MedProfileCheckResult checkMedProfileAlert(MedProfile medProfile, List<MedProfileItem> medProfileItemList, boolean fromDueOrderLabelGenFlag, boolean checkDdiFlag) {
		
		Set<Integer> newOrderItemNumSet = constructNewOrderItemNumSet(medProfileItemList);

		MdsResult mdsResult = alertRetrieveManager.retrieveAlertByMedProfileItemList(medProfile, medProfileItemList, checkDdiFlag);
		
		List<MedProfileAlertMsg> medProfileAlertMsgList = new ArrayList<MedProfileAlertMsg>();
		List<Long> withHoldList = new ArrayList<Long>();
		Map<MedProfileMoItem, List<AlertMsg>> manualMap = new LinkedHashMap<MedProfileMoItem, List<AlertMsg>>();
		boolean includeCrossDdiAlert = false;
		Map<Integer, Long> dueOrderCrossDdiWithHoldMap = new HashMap<Integer, Long>();
		
		Map<Integer, String> mdsOrderDescMap = new HashMap<Integer, String>();
		for (MedProfileItem medProfileItem : medProfileItemList) {
			MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
			mdsOrderDescMap.put(medProfileMoItem.getItemNum(), medProfileMoItem.getMdsOrderDesc());
			dueOrderCrossDdiWithHoldMap.put(medProfileMoItem.getItemNum(), medProfileItem.getId());
			
			if( medProfileMoItem.isManualItem() ){
				manualMap.put(medProfileMoItem, new ArrayList<AlertMsg>());
			}
		}

		for (MedProfileItem medProfileItem : medProfileItemList) {
			
			MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
			
			List<AlertEntity> filteredCrossDdiAlertList = mdsResult.getAlertEntityMap().get(medProfileMoItem.getItemNum());
			filterCrossDdiAlertForIpmoeItem(filteredCrossDdiAlertList, medProfileMoItem.isManualItem(), mdsOrderDescMap);
			
			List<AlertEntity> alertEntityList = filteredCrossDdiAlertList;
			if(fromDueOrderLabelGenFlag || !medProfileMoItem.isManualItem()){
				alertEntityList = alertComparator.constructOutStandingAlertList(
							filteredCrossDdiAlertList,
							medProfileMoItem.getMedProfileMoItemAlertList(),
							fromDueOrderLabelGenFlag
						);
			}
			
			if( medProfileMoItem.isManualItem() ){
				medProfileMoItem.clearMedProfileMoItemAlertList();
				medProfileMoItem.setMdsSuspendReason(null);
			}else{
				filterDdiAlert(alertEntityList, newOrderItemNumSet, medProfileMoItem.getItemNum());
			}

			if (alertEntityList.isEmpty()) {
				continue;
			}

			withHoldList.add(medProfileItem.getId());
			
			if( medProfileMoItem.isManualItem() ){
				List<AlertMsg> manualItemAlertMsgList = new ArrayList<AlertMsg>();
				List<AlertEntity> normalAlertEntityList = new ArrayList<AlertEntity>();
				List<AlertEntity> crossDdiAlertEntityList = new ArrayList<AlertEntity>();
				
				for(AlertEntity manualAlertEntity: alertEntityList){
					if( manualAlertEntity.isDdiAlert() ){
						AlertDdim alertDdim = (AlertDdim) manualAlertEntity.getAlert();
						if( alertDdim.isCrossDdi() ){
							crossDdiAlertEntityList.add(manualAlertEntity);
						}else{
							normalAlertEntityList.add(manualAlertEntity);
						}
					}else{
						normalAlertEntityList.add(manualAlertEntity);
					}
				}
				manualItemAlertMsgList.addAll(constructManualItemAlertMsg(medProfileItem, normalAlertEntityList));
				
				if( !crossDdiAlertEntityList.isEmpty() ){
					if(!includeCrossDdiAlert){
						includeCrossDdiAlert = true;
					}	
						
					if( fromDueOrderLabelGenFlag ){
						for( AlertEntity crossDdiEntity:  crossDdiAlertEntityList ){
							AlertDdim alertDdim = (AlertDdim) crossDdiEntity.getAlert();
							Long otherItemNum = alertDdim.getItemNum1() > alertDdim.getItemNum2() ?  alertDdim.getItemNum2() : alertDdim.getItemNum1();
							Long withHoldId = dueOrderCrossDdiWithHoldMap.get(Integer.valueOf(otherItemNum.intValue()));
							if( !withHoldList.contains(withHoldId)){
								withHoldList.add(withHoldId);
							}
						}
					}
					manualItemAlertMsgList.addAll(constructCrossDdiManualItemAlertMsg(medProfileItem, crossDdiAlertEntityList));
				}
				manualMap.put(medProfileMoItem, manualItemAlertMsgList);

			}else{
				medProfileAlertMsgList.add(constructMedProfileAlertMsg(medProfileItem, alertEntityList));
			}
		}

		List<AlertMsg> manualProfileAlertMsgList = new ArrayList<AlertMsg>();
		List<AlertMsg> ddiAlertMsgList = new ArrayList<AlertMsg>();
		List<AlertMsg> crossDdiAlertMsgList = new ArrayList<AlertMsg>();
		for (List<AlertMsg> alertMsgList : manualMap.values()) {
			for (AlertMsg alertMsg : alertMsgList) {
				alertMsg.saveAlertList();
				if( alertMsg.getCrossDdiFlag() ){
					crossDdiAlertMsgList.add(alertMsg);
				}else if( !alertMsg.getPatAlertList().isEmpty() ){
					manualProfileAlertMsgList.add(alertMsg);
				}else{
					ddiAlertMsgList.add(alertMsg);					
				}
			}
		}
		manualProfileAlertMsgList.addAll(ddiAlertMsgList);
		manualProfileAlertMsgList.addAll(crossDdiAlertMsgList);
		
		Collections.sort(medProfileAlertMsgList, medProfileAlertMsgComparator);
		
		logger.debug("medProfileAlertMsgList:\n#0", (new EclipseLinkXStreamMarshaller()).toXML(medProfileAlertMsgList));
		logger.debug("errorInfoList:\n#0", (new EclipseLinkXStreamMarshaller()).toXML(mdsResult.getErrorInfoList()));
		
		MedProfileCheckResult result = new MedProfileCheckResult();
		result.setIncludeManualProfileFlag(true);
		result.setMedProfileAlertMsgList(medProfileAlertMsgList);
		result.setErrorInfoList(mdsResult.getErrorInfoList());
		result.setWithHoldList(withHoldList);
		result.setManualProfileCheckResult(constructManualProfileCheckResult(medProfileItemList, manualMap, includeCrossDdiAlert));
		result.setManualProfileAlertMsgList(manualProfileAlertMsgList);
		
		return result;
	}
	
	private Set<Integer> constructNewOrderItemNumSet(List<MedProfileItem> medProfileItemList) {
		Set<Integer> itemNumSet = new HashSet<Integer>();
		for (MedProfileItem medProfileItem : medProfileItemList) {
			if (medProfileItem.getMedProfileMoItem().getFirstPrintDate() == null) {
				itemNumSet.add(medProfileItem.getMedProfileMoItem().getItemNum());
			}
		}
		return itemNumSet;
	}
	
	private void filterCrossDdiAlertForIpmoeItem(List<AlertEntity> alertEntityList, boolean manualItem, Map<Integer, String> mdsOrderDescMap){
		List<AlertEntity> removeList = new ArrayList<AlertEntity>();
		
		for (AlertEntity alertEntity : alertEntityList) {
			if (!alertEntity.isDdiAlert()) {
				continue;
			}
			
			AlertDdim alertDdim = (AlertDdim) alertEntity.getAlert();
			if( alertDdim.isCrossDdi() ){
				alertDdim.setCopyFlag(false);
				
				Long ipmoeItemNum = alertDdim.getItemNum1()> alertDdim.getItemNum2()?alertDdim.getItemNum2():alertDdim.getItemNum1();
				if( mdsOrderDescMap.get(Integer.valueOf(ipmoeItemNum.intValue())) != null ){
					alertDdim.setCrossDdiIpmoeOrderDesc(mdsOrderDescMap.get(Integer.valueOf(ipmoeItemNum.intValue())).toUpperCase());
				}
				if( !manualItem ){
					removeList.add(alertEntity);
				}
			}
		}
		alertEntityList.removeAll(removeList);
	}
	
	private void filterDdiAlert(List<AlertEntity> alertEntityList, Set<Integer> newOrderItemNumSet, Integer currentItemNum) {

		if (newOrderItemNumSet.contains(currentItemNum)) {
			return;
		}
		
		List<AlertEntity> removeList = new ArrayList<AlertEntity>();
		
		for (AlertEntity alertEntity : alertEntityList) {
			if (!alertEntity.isDdiAlert()) {
				continue;
			}
			AlertDdim alertDdim = (AlertDdim) alertEntity.getAlert();

			Integer otherItemNum;
			if (Integer.valueOf(alertDdim.getItemNum1().intValue()).equals(currentItemNum)) {
				otherItemNum = Integer.valueOf(alertDdim.getItemNum2().intValue());
			}
			else {
				otherItemNum = Integer.valueOf(alertDdim.getItemNum1().intValue());
			}
			
			if (newOrderItemNumSet.contains(otherItemNum)) {
				removeList.add(alertEntity);
			}
		}
		alertEntityList.removeAll(removeList);
	}
	
	public ManualProfileCheckResult constructManualProfileCheckResult(List<MedProfileItem> medProfileItemList, Map<MedProfileMoItem, List<AlertMsg>> manualMap, boolean includeCrossDdiAlert){
		if( manualMap.isEmpty() ){
			return null;
		}

		for(MedProfileItem medProfileItem : medProfileItemList){
			if( !manualMap.containsKey(medProfileItem.getMedProfileMoItem())){
				manualMap.put(medProfileItem.getMedProfileMoItem(), new ArrayList<AlertMsg>());
			}
		}
		return new ManualProfileCheckResult(manualMap, includeCrossDdiAlert);
	}
		
	private List<AlertMsg> constructCrossDdiManualItemAlertMsg(MedProfileItem medProfileItem, List<AlertEntity> alertList){
		List<AlertMsg> alertMsgList = new ArrayList<AlertMsg>();

		MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
		
		List<MedProfileMoItemAlert> medProfileMoItemAlertList = new ArrayList<MedProfileMoItemAlert>();		
		for (AlertEntity alertEntity : alertList) {
			MedProfileMoItemAlert mpAlert = new MedProfileMoItemAlert(alertEntity);
			mpAlert.setCreateDate(new Date());
			medProfileMoItem.addMedProfileMoItemAlert(mpAlert);
			medProfileMoItemAlertList.add(mpAlert);
		}
		
		Map<Long, List<AlertEntity>> otherItemDdiAlertMsgMap = new LinkedHashMap<Long, List<AlertEntity>>();
		
		for (MedProfileMoItemAlert medProfileMoItemAlert : medProfileMoItemAlertList) {
			AlertDdim alertDdim = (AlertDdim) medProfileMoItemAlert.getAlert();
			Long mapItemNum = alertDdim.getItemNum1() > alertDdim.getItemNum2() ?  alertDdim.getItemNum2() : alertDdim.getItemNum1();
			List<AlertEntity> ddiAlertList = otherItemDdiAlertMsgMap.get(mapItemNum);
			if( ddiAlertList == null ){
				ddiAlertList = new ArrayList<AlertEntity>();
			}
			ddiAlertList.add(medProfileMoItemAlert);
			otherItemDdiAlertMsgMap.put(mapItemNum, ddiAlertList);
		}
		
		if( !otherItemDdiAlertMsgMap.isEmpty() ){
			for( Long otherItemNum : otherItemDdiAlertMsgMap.keySet() ){
				AlertMsg crossDdiAlertMsg = new AlertMsg(
						medProfileMoItem.getItemNum(),
						medProfileMoItem.getMdsOrderDesc(),
						medProfileMoItem.getDisplayNameSaltForm(),
						null,
						otherItemDdiAlertMsgMap.get(otherItemNum)
					);
					crossDdiAlertMsg.setCrossDdiFlag(true);
					alertMsgList.add(crossDdiAlertMsg);			
			}
		}
		return alertMsgList;
	}
	
	private List<AlertMsg> constructManualItemAlertMsg(MedProfileItem medProfileItem, List<AlertEntity> alertList){
		List<AlertMsg> alertMsgList = new ArrayList<AlertMsg>();
		MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
		
		for (AlertEntity alertEntity : alertList) {
			MedProfileMoItemAlert mpAlert = new MedProfileMoItemAlert(alertEntity);
			mpAlert.setCreateDate(new Date());
			medProfileMoItem.addMedProfileMoItemAlert(mpAlert);
		}
		
		if (!medProfileMoItem.getPatAlertList(false).isEmpty()) {
			alertMsgList.add(alertMsgManager.retrieveMpPatAlertMsg(medProfileMoItem));
		}
		alertMsgList.addAll(alertMsgManager.retrieveMpDdiAlertMsgList(medProfileMoItem));
		
		return alertMsgList;
	}
	
	private MedProfileAlertMsg constructMedProfileAlertMsg(MedProfileItem medProfileItem, List<AlertEntity> alertList) {

		MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
		
		List<MedProfileMoItemAlert> medProfileMoItemAlertList = new ArrayList<MedProfileMoItemAlert>();
		for (AlertEntity alertEntity : alertList) {
			MedProfileMoItemAlert medProfileMoItemAlert = new MedProfileMoItemAlert(alertEntity);
			medProfileMoItemAlert.setMedProfileMoItem(null);
			medProfileMoItemAlertList.add(medProfileMoItemAlert);
		}
		
		List<AlertMsg> alertMsgList;
		if (medProfileMoItem.getRxItem().isIvRxDrug()) {
			alertMsgList = constructAlertMsgForIvRxDrug(medProfileMoItem, medProfileMoItemAlertList);
		}
		else {
			alertMsgList = constructAlertMsgForNonIvRxDrug(medProfileMoItem, medProfileMoItemAlertList);
		}
		
		for (AlertMsg alertMsg : alertMsgList) {
			alertMsg.saveAlertList();
		}
		
		MedProfileAlertMsg medProfileAlertMsg = new MedProfileAlertMsg();
		medProfileAlertMsg.setOrderDesc(medProfileMoItem.getMdsOrderDesc());
		medProfileAlertMsg.setIvRxDrug(medProfileMoItem.getRxItem().isIvRxDrug());
		medProfileAlertMsg.setAlertMsgList(alertMsgList);
		
		return medProfileAlertMsg;
	}

	private List<AlertMsg> constructAlertMsgForNonIvRxDrug(MedProfileMoItem medProfileMoItem, List<MedProfileMoItemAlert> medProfileMoItemAlertList) {
		
		List<AlertEntity> patAlertList = new ArrayList<AlertEntity>();
		List<AlertEntity> ddiAlertList = new ArrayList<AlertEntity>();
		List<AlertEntity> drcAlertList = new ArrayList<AlertEntity>();
		
		for (MedProfileMoItemAlert medProfileMoItemAlert : medProfileMoItemAlertList) {
			if (medProfileMoItemAlert.isPatAlert(MEDPROFILE_ALERT_HLA_ENABLED.get())) {
				patAlertList.add(medProfileMoItemAlert);
			}
			else if (medProfileMoItemAlert.isDdiAlert()) {
				ddiAlertList.add(medProfileMoItemAlert);
			}
			else if (medProfileMoItemAlert.isDrcAlert()) {
				drcAlertList.add(medProfileMoItemAlert);
			}
		}
		
		alertMsgManager.sortAlertEntityList(patAlertList);
		
		return Arrays.asList(new AlertMsg(
				medProfileMoItem.getItemNum(),
				medProfileMoItem.getMdsOrderDesc(),
				medProfileMoItem.getDisplayNameSaltForm(),
				patAlertList,
				ddiAlertList,
				drcAlertList
		));
	}

	private List<AlertMsg> constructAlertMsgForIvRxDrug(MedProfileMoItem medProfileMoItem, List<MedProfileMoItemAlert> medProfileMoItemAlertList) {

		List<AlertMsg> alertMsgList = new ArrayList<AlertMsg>();

		for (IvAdditive ivAdditive : ((IvRxDrug) medProfileMoItem.getRxItem()).getIvAdditiveList()) {
			
			List<AlertEntity> patAlertList = new ArrayList<AlertEntity>();
			List<AlertEntity> ddiAlertList = new ArrayList<AlertEntity>();
			List<AlertEntity> drcAlertList = new ArrayList<AlertEntity>();
			
			for (MedProfileMoItemAlert medProfileMoItemAlert : medProfileMoItemAlertList) {
				if (!ivAdditive.getAdditiveNum().equals(medProfileMoItemAlert.getAdditiveNum())) {
					continue;
				}
				if (medProfileMoItemAlert.isPatAlert(MEDPROFILE_ALERT_HLA_ENABLED.get())) {
					patAlertList.add(medProfileMoItemAlert);
				}
				else if (medProfileMoItemAlert.isDdiAlert()) {
					ddiAlertList.add(medProfileMoItemAlert);
				}
				else if (medProfileMoItemAlert.isDrcAlert()) {
					drcAlertList.add(medProfileMoItemAlert);
				}
			}
			
			if (patAlertList.isEmpty() && ddiAlertList.isEmpty() && drcAlertList.isEmpty()) {
				continue;
			}
			
			alertMsgManager.sortAlertEntityList(patAlertList);
			
			alertMsgList.add(new AlertMsg(
					medProfileMoItem.getItemNum(),
					ivAdditive.getMdsOrderDesc(),
					ivAdditive.getDisplayNameSaltForm(),
					new ArrayList<AlertEntity>(patAlertList),
					new ArrayList<AlertEntity>(ddiAlertList),
					new ArrayList<AlertEntity>(drcAlertList)
			));
		}
		
		return alertMsgList;
	}

	@Remove
	public void destory() {
	}

	private static class MedProfileAlertMsgComparator implements Comparator<MedProfileAlertMsg>, Serializable {

		private static final long serialVersionUID = 1L;

		public int compare(MedProfileAlertMsg mpAlertMsg1, MedProfileAlertMsg mpAlertMsg2) {
			return mpAlertMsg2.getAlertMsgList().get(0).getItemNum().compareTo(mpAlertMsg1.getAlertMsgList().get(0).getItemNum());
		}
		
	}
}
