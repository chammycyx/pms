package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;

public class DrsRefillDispRptOtherItem implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String fullDrugDesc;
	
	private String displayInstruction;
	
	private String actionStatus;
	
	private String nextRefillCouponNum;
	
	private Date nextRefillDate;

	public String getFullDrugDesc() {
		return fullDrugDesc;
	}

	public void setFullDrugDesc(String fullDrugDesc) {
		this.fullDrugDesc = fullDrugDesc;
	}

	public String getDisplayInstruction() {
		return displayInstruction;
	}

	public void setDisplayInstruction(String displayInstruction) {
		this.displayInstruction = displayInstruction;
	}

	public String getActionStatus() {
		return actionStatus;
	}

	public void setActionStatus(String actionStatus) {
		this.actionStatus = actionStatus;
	}

	public String getNextRefillCouponNum() {
		return nextRefillCouponNum;
	}

	public void setNextRefillCouponNum(String nextRefillCouponNum) {
		this.nextRefillCouponNum = nextRefillCouponNum;
	}

	public Date getNextRefillDate() {
		return nextRefillDate;
	}

	public void setNextRefillDate(Date nextRefillDate) {
		this.nextRefillDate = nextRefillDate;
	}
	
}
