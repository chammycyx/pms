package hk.org.ha.model.pms.biz.inbox;

import static hk.org.ha.model.pms.prop.Prop.INBOX_DISCHARGEORDER_DURATION_LIMIT;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.RecordStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatOrderType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("categorySummarySupport")
@MeasureCalls
public class CategorySummarySupportBean implements CategorySummarySupportLocal {

	private static final String CATEGORY_COUNT_CLASS_NAME = CategoryCount.class.getName();
	
	@PersistenceContext
	private EntityManager em;
	
    public CategorySummarySupportBean() {
    }
    
	@SuppressWarnings("unchecked")
	@Override
	public List<CategoryCount> retrieveOrderTypeCategoryCountList(Workstore workstore, Boolean showChemoProfileOnly, List<String> wardCodeList, Date now) {
		
		if (wardCodeList == null || wardCodeList.size() <= 0) {
			return new ArrayList<CategoryCount>();
		}
		
		boolean countChemoProfileOnly = Boolean.TRUE.equals(showChemoProfileOnly);
		
		Query query = em.createQuery(
				"select new " + CATEGORY_COUNT_CLASS_NAME + // 20140925 index check : none
				"  (m.adminType, m.orderType, count(m), count(nullif(m.statFlag, false)), count(nullif(m.pendingFlag, false))," +
				"   count(coalesce(nullif(m.suspendFlag, false), nullif(m.mdsErrorFlag, false), nullif(m.unitDoseExceptFlag, false)))," +
				"   count(nullif(m.overridePendingFlag, false))," +
				"   min(m.dueDate))" +
				" from MedProfileStat m" +
				" where m.medProfile.hospCode = :hospCode" +
				" and m.medProfile.status <> :inactiveStatus" +
				(countChemoProfileOnly ? " and m.medProfile.type = :chemoProfileType" : "") +
				" and (m.medProfile.type is null or m.medProfile.type = :normalProfileType" +
				"   or (m.medProfile.medCase.caseNum is not null and m.dueDate < :chemoStartDate))" +
				" and (m.medProfile.wardCode in :wardCodeList or m.medProfile.wardCode is null)" +
				" and m.orderType <> :allOrderType" +
				" group by m.adminType, m.orderType")
				.setParameter("hospCode", workstore.getHospCode())
				.setParameter("inactiveStatus", MedProfileStatus.Inactive)
				.setParameter("normalProfileType", MedProfileType.Normal)
				.setParameter("chemoStartDate", MedProfileUtils.addDays(MedProfileUtils.getDayBegin(now), 2))
				.setParameter("wardCodeList", wardCodeList)
				.setParameter("allOrderType", MedProfileStatOrderType.All);
		
		if (countChemoProfileOnly) {
			query.setParameter("chemoProfileType", MedProfileType.Chemo);
		}
		
		return query.getResultList();
	}
	
    @SuppressWarnings("unchecked")
    @Override
	public List<CategoryCount> retrieveAdminTypeCategoryCountList(Workstore workstore, Boolean showChemoProfileOnly, List<String> wardCodeList, Date now) {
    	
		if (wardCodeList == null || wardCodeList.size() <= 0) {
			return new ArrayList<CategoryCount>();
		}
		
		boolean countChemoProfileOnly = Boolean.TRUE.equals(showChemoProfileOnly);
		
		Query query = em.createQuery(
				"select new " + CATEGORY_COUNT_CLASS_NAME + // 20140925 index check : none
				"  (m.adminType, count(distinct m.medProfile))" +
				" from MedProfileStat m" +
				" where m.medProfile.hospCode = :hospCode" +
				" and m.medProfile.status <> :inactiveStatus" +
				(countChemoProfileOnly ? " and m.medProfile.type = :chemoProfileType" : "") +
				" and (m.medProfile.type is null or m.medProfile.type = :normalProfileType" +
				"   or (m.medProfile.medCase.caseNum is not null and m.dueDate < :chemoStartDate))" +				
				" and (m.medProfile.wardCode in :wardCodeList or m.medProfile.wardCode is null)" +				
				" group by m.adminType")
				.setParameter("hospCode", workstore.getHospCode())
				.setParameter("inactiveStatus", MedProfileStatus.Inactive)
				.setParameter("normalProfileType", MedProfileType.Normal)
				.setParameter("chemoStartDate", MedProfileUtils.addDays(MedProfileUtils.getDayBegin(now), 2))
				.setParameter("wardCodeList", wardCodeList);
		
		if (countChemoProfileOnly) {
			query.setParameter("chemoProfileType", MedProfileType.Chemo);
		}
		
		return query.getResultList();
    }
	
    @Override
    public Long countErrorStat(Workstore workstore, List<String> wardCodeList, Date now) {
		
		if (wardCodeList == null || wardCodeList.size() <= 0) {
			return 0L;
		}
		
		Query query = em.createQuery(
				"select count(e) from MedProfileErrorStat e" + // 20140926 index check : none
				" where e.medProfile.hospCode = :hospCode" +
				" and e.medProfile.status <> :inactiveStatus" +
				" and (e.medProfile.wardCode in :wardCodeList or e.medProfile.wardCode is null)")
				.setParameter("hospCode", workstore.getHospCode())
				.setParameter("inactiveStatus", MedProfileStatus.Inactive)
				.setParameter("wardCodeList", wardCodeList);
		
		return (Long) query.getSingleResult();
	}
	
    @Override
    public Long countDischargeMedOrder(Workstore workstore, List<String> wardCodeList, Date now) {

		if (wardCodeList == null || wardCodeList.size() <= 0) {
			return 0L;
		}
		
		return (Long) em.createQuery(
				"select count(o) from MedOrder o" + // 20140925 index check : MedOrder.hospCode,prescType,status,orderDate : I_MED_ORDER_07
				" where o.prescType in :prescTypeList" +
				" and o.status in :statusList" +
				" and o.orderDate > :startDate" +
				" and o.remarkCreateDate is null" +
				" and o.hospCode = :hospCode" +
				" and (o.wardCode in :wardCodeList or o.wardCode is null)")
				.setParameter("prescTypeList", MedOrderPrescType.MpDischarge_MpHomeLeave)
				.setParameter("statusList", MedOrderStatus.Outstanding_Withhold)
				.setParameter("startDate", MedProfileUtils.addDays(now, INBOX_DISCHARGEORDER_DURATION_LIMIT.get(7) * -1))
				.setParameter("hospCode", workstore.getHospCode())
				.setParameter("wardCodeList", wardCodeList)
				.getSingleResult();
	}
	
    @Override
    public Long countCancelDischargeTransferCount(Workstore workstore) {
		
		return (Long) em.createQuery(
				"select count(o) from MedProfile o" + // 20140128 index check : MedProfile.hospCode,cancelDischargeDate MedProfile.hospCode,confirmCancelDischargeDate : I_MED_PROFILE_04 I_MED_PROFILE_05
				" where o.hospCode = :hospCode" +
    			" and o.cancelDischargeDate is not null" +
    			" and (o.confirmCancelDischargeDate > :startDate" +
    			"   or o.confirmCancelDischargeDate is null)" +
    			" and (o.wardCode is null or o.wardCode in " +
		    	"  (select w.wardCode from Ward w, WardConfig wc" +
		    	"   where w.institution = :institution and w.status = :activeRecordStatus and wc.workstoreGroup = :workstoreGroup" +
		    	"   and w.wardCode = wc.wardCode and wc.mpWardFlag = true))")
    			.setParameter("hospCode", workstore.getHospCode())
    			.setParameter("institution", workstore.getWorkstoreGroup().getInstitution())
    			.setParameter("workstoreGroup", workstore.getWorkstoreGroup())
    			.setParameter("activeRecordStatus", RecordStatus.Active)
    			.setParameter("startDate", MedProfileUtils.getTodayBegin())
				.getSingleResult();
	}
}
