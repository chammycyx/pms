package hk.org.ha.model.pms.vo.picking;

import java.io.Serializable;

public class DrugPickWorkstation implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long workstationId;
	
	private String workstationCode;
	
	private String hostName;

	public Long getWorkstationId() {
		return workstationId;
	}

	public void setWorkstationId(Long workstationId) {
		this.workstationId = workstationId;
	}

	public String getWorkstationCode() {
		return workstationCode;
	}

	public void setWorkstationCode(String workstationCode) {
		this.workstationCode = workstationCode;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	
}