package hk.org.ha.model.pms.biz.medprofile;

import java.util.ArrayList;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;
import org.jboss.seam.log.Log;


@Startup
@Name("medProfileOrderScheduler")
@Scope(ScopeType.APPLICATION)
public class MedProfileOrderScheduler implements MedProfileOrderSchedulerInf {
	
	@Logger
	private Log logger;
	
	private ArrayList<String> orderQueueList = new ArrayList<String>();
	
	private int maxRunningCount = 2;
	
	private int currRunningCount = 0;
			
	public int getMaxRunningCount() {
		return maxRunningCount;
	}

	public void setMaxRunningCount(int maxRunningCount) {
		this.maxRunningCount = maxRunningCount;
	}

	public boolean scheduleMedProfileOrder(String orderNum) {
		synchronized (this) {
			if (!orderQueueList.contains(orderNum)) {
				orderQueueList.add(orderNum);
				return true;
			} else {
				return false;
			}
		}
	}
	
	public void stopRunning() {
		synchronized (this) {
			if (currRunningCount > 0) {
				currRunningCount--;
				logger.debug("#0 stopRunning currRunningCount back to #1", this.hashCode(), currRunningCount);
			} else {
				logger.error("#0 stopRunning, unexpected error ocurr, currRunningCount is #1", this.hashCode(), currRunningCount);
			}
		}
	}
	
	public boolean shouldStartRunning() {			
		synchronized (this) {						
			if (orderQueueList.isEmpty()) {
				return false;
			}
			if (currRunningCount < maxRunningCount) {
				currRunningCount++;
				logger.debug("#0 startRunning currRunningCount raise to #1 instance:#1", this.hashCode(), currRunningCount);
				return true;
			} else {
				return false;
			}
		}
	}
	
	public String getNextOrderNumToRun() {	
		synchronized (this) {
			if (!orderQueueList.isEmpty()) {
				return orderQueueList.remove(0);
			} else {
				return null;
			}
		}
	}
	
}
