package hk.org.ha.model.pms.biz.enquiry;

import hk.org.ha.model.pms.vo.enquiry.DeliveryRequestInfo;

import java.util.List;

import javax.ejb.Local;

@Local
public interface MdsExceptionEnqServiceLocal {

	List<DeliveryRequestInfo> retrieveDeliveryRequestInfoList();
	
	void destroy();
}
