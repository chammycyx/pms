package hk.org.ha.model.pms.biz.drug;

import static hk.org.ha.model.pms.prop.Prop.VETTING_REGIMEN_SUPPLFREQ_SORT;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmSupplFrequencyCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmSupplFrequency;
import hk.org.ha.model.pms.udt.vetting.RegimenType;
import hk.org.ha.model.pms.udt.vetting.SupplFreqSort;

import java.util.List;

import javax.ejb.Stateless;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("dmSupplFrequencyListManager")
@MeasureCalls
public class DmSupplFrequencyListManagerBean implements DmSupplFrequencyListManagerLocal {

	@Logger
	private Log logger;
	
	@In
	private DmSupplFrequencyCacherInf dmSupplFrequencyCacher;

	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private List<DmSupplFrequency> dailySupplFrequencyList;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private List<DmSupplFrequency> weeklySupplFrequencyList;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private List<DmSupplFrequency> monthlySupplFrequencyList;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private List<DmSupplFrequency> cyclicSupplFrequencyList;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private List<DmSupplFrequency> dailySupplFrequencySuspendList;
	
	public void retrieveDmSupplFrequencyList( ) {
		if ( logger.isDebugEnabled() ) {
			logger.debug("retrieveDmSupplFrequencyList");
		}		
	
		//set default sort sequence by rule value
		SupplFreqSort sortSeq = SupplFreqSort.dataValueOf(VETTING_REGIMEN_SUPPLFREQ_SORT.get());									
		
		dailySupplFrequencyList = dmSupplFrequencyCacher.getDmSupplFrequencyList( RegimenType.Daily.getDataValue(), sortSeq );
		weeklySupplFrequencyList = dmSupplFrequencyCacher.getDmSupplFrequencyList( RegimenType.Weekly.getDataValue(), sortSeq );
		monthlySupplFrequencyList = dmSupplFrequencyCacher.getDmSupplFrequencyList( RegimenType.Monthly.getDataValue(), sortSeq );
		cyclicSupplFrequencyList = dmSupplFrequencyCacher.getDmSupplFrequencyList( RegimenType.Cyclic.getDataValue(), sortSeq );
		dailySupplFrequencySuspendList = dmSupplFrequencyCacher.getDmSupplFrequencySuspendList( RegimenType.Daily.getDataValue(), sortSeq );
		
		if ( !logger.isDebugEnabled() ) {
			return;
		}
		
		if ( dailySupplFrequencyList != null ) {
			logger.debug("dailySupplFrequencyList size #0", dailySupplFrequencyList.size());
		}
		if ( weeklySupplFrequencyList != null ) {
			logger.debug("weeklySupplFrequencyList size #0", weeklySupplFrequencyList.size());
		}
		if ( monthlySupplFrequencyList != null ) {
			logger.debug("monthlySupplFrequencyList size #0", monthlySupplFrequencyList.size());
		}
		if ( cyclicSupplFrequencyList != null ) {
			logger.debug("cyclicSupplFrequencyList size #0", cyclicSupplFrequencyList.size());
		}
		if ( dailySupplFrequencySuspendList != null ) {
			logger.debug("dailySupplFrequencySuspendList size #0", dailySupplFrequencySuspendList.size());
		}
	}
}
