package hk.org.ha.model.pms.vo.medprofile;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class BodyInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private BigDecimal bodyWeight;
	
	private BigDecimal bodyHeight;
	
	private Date bodyInfoUpdateDate;

	public BigDecimal getBodyWeight() {
		return bodyWeight;
	}

	public void setBodyWeight(BigDecimal bodyWeight) {
		this.bodyWeight = bodyWeight;
	}

	public BigDecimal getBodyHeight() {
		return bodyHeight;
	}

	public void setBodyHeight(BigDecimal bodyHeight) {
		this.bodyHeight = bodyHeight;
	}

	public Date getBodyInfoUpdateDate() {
		return bodyInfoUpdateDate;
	}

	public void setBodyInfoUpdateDate(Date bodyInfoUpdateDate) {
		this.bodyInfoUpdateDate = bodyInfoUpdateDate;
	}
}
