package hk.org.ha.model.pms.vo.rx;

import hk.org.ha.model.pms.udt.vetting.FixedConcnSource;
import hk.org.ha.model.pms.udt.vetting.NameType;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="IvAdditive")
@ExternalizedBean(type=DefaultExternalizer.class)
public class IvAdditive extends RxDrug implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement
	private Integer additiveNum;	

	@XmlElement(required = true)
	private BigDecimal dosage;

	@XmlElement
	private BigDecimal dosageEnd;
	
	@XmlElement(required = true)
	private String dosageUnit;
	
	@XmlElement
	private Boolean noDiluentRequireFlag;

	@XmlElement
	private FixedConcnSource fixedConcnSource;

	@XmlElement
	private Integer fixedConcnCode;

	@XmlElement
	private String fixedConcnDesc;

	@XmlElement
	private BigDecimal fixedConcnRatio;
	
	@XmlElement
	private String cmsFmStatus;

	public IvAdditive() {
		super();
		noDiluentRequireFlag = Boolean.FALSE;
		fixedConcnSource = FixedConcnSource.None;
	}
	
	public Integer getAdditiveNum() {
		return additiveNum;
	}

	public void setAdditiveNum(Integer additiveNum) {
		this.additiveNum = additiveNum;
	}

	public BigDecimal getDosage() {
		return dosage;
	}

	public void setDosage(BigDecimal dosage) {
		this.dosage = dosage;
	}

	public BigDecimal getDosageEnd() {
		return dosageEnd;
	}

	public void setDosageEnd(BigDecimal dosageEnd) {
		this.dosageEnd = dosageEnd;
	}

	public String getDosageUnit() {
		return dosageUnit;
	}

	public void setDosageUnit(String dosageUnit) {
		this.dosageUnit = dosageUnit;
	}		
	
	public Boolean getNoDiluentRequireFlag() {
		return noDiluentRequireFlag;
	}

	public void setNoDiluentRequireFlag(Boolean noDiluentRequireFlag) {
		this.noDiluentRequireFlag = noDiluentRequireFlag;
	}

	public FixedConcnSource getFixedConcnSource() {
		return fixedConcnSource;
	}

	public void setFixedConcnSource(FixedConcnSource fixedConcnSource) {
		this.fixedConcnSource = fixedConcnSource;
	}

	public Integer getFixedConcnCode() {
		return fixedConcnCode;
	}

	public void setFixedConcnCode(Integer fixedConcnCode) {
		this.fixedConcnCode = fixedConcnCode;
	}

	public String getFixedConcnDesc() {
		return fixedConcnDesc;
	}

	public void setFixedConcnDesc(String fixedConcnDesc) {
		this.fixedConcnDesc = fixedConcnDesc;
	}

	public BigDecimal getFixedConcnRatio() {
		return fixedConcnRatio;
	}

	public void setFixedConcnRatio(BigDecimal fixedConcnRatio) {
		this.fixedConcnRatio = fixedConcnRatio;
	}

	public String getCmsFmStatus() {
		return cmsFmStatus;
	}

	public void setCmsFmStatus(String cmsFmStatus) {
		this.cmsFmStatus = cmsFmStatus;
	}

	public String getMdsDisplayName() {
		
		StringBuilder sb = new StringBuilder();
		
		if (this.getNameType() == NameType.TradeName) {
			sb.append(WordUtils.capitalizeFully(this.getAliasName(), DELIMITER));
			
			sb.append(SPACE);
			sb.append("(");
			sb.append(WordUtils.capitalizeFully(this.getDisplayName(), DELIMITER));
			
			if (StringUtils.isNotBlank(this.getSaltProperty())) {
				sb.append(SPACE);
				sb.append(WordUtils.capitalizeFully(this.getSaltProperty(), DELIMITER));
			}

			sb.append(")");
		}
		else {
			sb.append(WordUtils.capitalizeFully(this.getDisplayName(), DELIMITER));

			if (StringUtils.isNotBlank(this.getSaltProperty())) {
				sb.append(SPACE);
				sb.append(WordUtils.capitalizeFully(this.getSaltProperty(), DELIMITER));
			}
		}

		if (StringUtils.isNotBlank(this.getFormDesc())) {
			sb.append(SPACE);
			sb.append(this.getFormDesc().toLowerCase());
		}

		return sb.toString();
	}

	public String getMdsOrderDesc() {
		return getMdsDisplayName().toUpperCase();
	}
	
	@Override
	@Deprecated
	public Boolean getAlertFlag() {		
		// not apply for IvAdditive
		throw new UnsupportedOperationException();
	}

	@Override
	@Deprecated
	public void setAlertFlag(Boolean alertFlag) {
		// not apply for IvAdditive
		throw new UnsupportedOperationException();
	}
}
