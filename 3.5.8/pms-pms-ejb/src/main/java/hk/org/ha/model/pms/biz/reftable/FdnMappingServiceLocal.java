package hk.org.ha.model.pms.biz.reftable;
import hk.org.ha.model.pms.udt.vetting.OrderType;

import javax.ejb.Local;

@Local
public interface FdnMappingServiceLocal {
	
	String retrieveFloatingDrugName(String itemCode, OrderType orderType);
	
	void destroy();
}
