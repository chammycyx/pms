package hk.org.ha.model.pms.biz.enquiry;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.charging.ChargeCalculationInf;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmCdpReport;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.MsWorkstoreDrug;
import hk.org.ha.model.pms.dms.vo.pms.DrugChargeInfo;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.YesNoBlankFlag;
import hk.org.ha.model.pms.udt.disp.PharmOrderPatType;
import hk.org.ha.model.pms.vo.charging.DrugChargeRuleCriteria;
import hk.org.ha.model.pms.vo.charging.DrugPriceInfo;
import hk.org.ha.model.pms.vo.charging.OverrideChargeAmountRuleCriteria;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("sfiDrugPriceEnqService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SfiDrugPriceEnqServiceBean implements SfiDrugPriceEnqServiceLocal {
				
	@Logger
	private Log logger;
	
	@Out(required=false)
	private DrugPriceInfo drugPriceEnquiryInfo;
	
	@Out(required=false)
	private List<DrugPriceInfo> drugPriceEnquiryInfoList;
	
	@In
	private Workstore workstore;

	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;	
	
	@In
	private ChargeCalculationInf chargeCalculation;
	
	private Map<String, Map<String, DmCdpReport>> sfiDrugPriceHistoryMap;
		
	private DateFormat df = new SimpleDateFormat("MM-yyyy"); 
	
	private void refreshDrugPriceHistoryMap(List<String> itemCodeList){
		
		if( itemCodeList.isEmpty() ){
			return;
		}
				
		List<DmCdpReport> dmCdpReportList = dmsPmsServiceProxy.retrieveDmCpdReportList(itemCodeList);
		
		if( sfiDrugPriceHistoryMap == null ){
			sfiDrugPriceHistoryMap = new HashMap<String, Map<String, DmCdpReport>>();
		}
		
		for( DmCdpReport dmCdpReport:dmCdpReportList){
			Map<String,DmCdpReport> dmCdpReportMap;
			
			if( sfiDrugPriceHistoryMap.containsKey(dmCdpReport.getCompId().getItemCode()) ){
				dmCdpReportMap = sfiDrugPriceHistoryMap.get(dmCdpReport.getCompId().getItemCode());
				
				if( !dmCdpReportMap.containsKey( df.format(dmCdpReport.getCompId().getGenDate())) ) {
				
					logger.debug("sfiDrugPriceHistoryMap Add GenDate dmCdpReportMap of #0 genDate #1", dmCdpReport.getCompId().getItemCode(), df.format(dmCdpReport.getCompId().getGenDate()));
					
					dmCdpReportMap.put(df.format(dmCdpReport.getCompId().getGenDate()), dmCdpReport);
					sfiDrugPriceHistoryMap.put(dmCdpReport.getCompId().getItemCode(), dmCdpReportMap);
				}
				
			}else{
				
				logger.debug("sfiDrugPriceHistoryMap New dmCdpReportMap of #0 genDate #1", dmCdpReport.getCompId().getItemCode(), df.format(dmCdpReport.getCompId().getGenDate()));
				
				dmCdpReportMap = new HashMap<String,DmCdpReport>();
				dmCdpReportMap.put(df.format(dmCdpReport.getCompId().getGenDate()), dmCdpReport);
				sfiDrugPriceHistoryMap.put(dmCdpReport.getCompId().getItemCode(), dmCdpReportMap);
			}
		}
	}
		
	private List<String> extractNotExistingItemCodeList(List<DrugPriceInfo> drugPriceEnquiryInfoList){	

		List<String> itemCodeEnqList = new ArrayList<String>();
						
		for( DrugPriceInfo drugPriceEnqInfo : drugPriceEnquiryInfoList ){
			logger.info("extractNotExistingItemCodeList itemCodeList #0", drugPriceEnqInfo.getItemCode());
			if( !StringUtils.isNotBlank(drugPriceEnqInfo.getFullDrugDesc()) ){
				continue;
			}
			
			if ( sfiDrugPriceHistoryMap == null || !sfiDrugPriceHistoryMap.containsKey(drugPriceEnqInfo.getItemCode()) ){
				itemCodeEnqList.add(drugPriceEnqInfo.getItemCode());
			}
		}
		
		return itemCodeEnqList;
	}
	
	public void retrieveSfiDrugPriceHistory(List<DrugPriceInfo> drugPriceEnqInfoList, Date retrieveMonth){
		logger.info("RetrieveMonth:#0",  retrieveMonth);
		logger.info("Formatted RetrieveMonth:#0",  df.format(retrieveMonth));
		
		Date currentMonth = new Date();
		if ( df.format(retrieveMonth).equals(df.format(currentMonth)) ){
			return;
		}
		
		List<String> enqItemCodeList = extractNotExistingItemCodeList(drugPriceEnqInfoList);
		
		for( DrugPriceInfo drugPriceEnqInfo : drugPriceEnqInfoList){			
			
			if( enqItemCodeList.contains(drugPriceEnqInfo.getItemCode()) && StringUtils.isNotBlank(drugPriceEnqInfo.getFullDrugDesc()) ){
				DrugPriceInfo drugPriceInfo = retrieveBaseDrugPriceInfo(drugPriceEnqInfo.getItemCode());
				if( drugPriceInfo != null ){
					drugPriceEnqInfo.setFullDrugDesc(drugPriceInfo.getFullDrugDesc());
				}
			}
		}
		
		enqItemCodeList = extractNotExistingItemCodeList(drugPriceEnqInfoList);
		refreshDrugPriceHistoryMap(enqItemCodeList);
						
		for( DrugPriceInfo drugPriceEnqInfo : drugPriceEnqInfoList){			
			if( sfiDrugPriceHistoryMap == null ){
				break;
			}
			
			if( sfiDrugPriceHistoryMap.get(drugPriceEnqInfo.getItemCode())!= null ){
				DmCdpReport dmCdpReport = sfiDrugPriceHistoryMap.get(drugPriceEnqInfo.getItemCode()).get(df.format(retrieveMonth));				
				updateHistoryDrugPriceInfo(dmCdpReport, drugPriceEnqInfo);
			}else{
				updateHistoryDrugPriceInfo(null, drugPriceEnqInfo);
			}
		}
		
		drugPriceEnquiryInfoList = drugPriceEnqInfoList;
	}
		
	private DrugPriceInfo retrieveBaseDrugPriceInfo(String itemCode){
		DmDrug dmDrug = dmDrugCacher.getDmDrug(itemCode);
		
		if( dmDrug == null ){
			return null;
		}
		
		MsWorkstoreDrug msWorkStoreDrug = dmDrugCacher.getMsWorkstoreDrug(workstore, dmDrug.getItemCode());
		
		if( msWorkStoreDrug == null ){
			return null;
		}
		
		DrugPriceInfo drugPriceInfo = new DrugPriceInfo();
		drugPriceInfo = updateDrugPriceInfoByDmDrug(dmDrug, drugPriceInfo);

		if( StringUtils.equals(msWorkStoreDrug.getItemStatus(),"A") ){
			drugPriceInfo.setSuspendIndicator(YesNoBlankFlag.No.getDataValue());
		}else{
			drugPriceInfo.setSuspendIndicator(YesNoBlankFlag.Yes.getDataValue());
		}
		return drugPriceInfo;
	}
	
	private DrugPriceInfo updateDrugPriceInfoByDmDrug(DmDrug dmDrug, DrugPriceInfo drugPriceInfo){
		drugPriceInfo.setItemCode(dmDrug.getItemCode());
		drugPriceInfo.setBaseUnit(dmDrug.getBaseUnit());
		drugPriceInfo.setFullDrugDesc(dmDrug.getFullDrugDesc());
		drugPriceInfo.setValidPriceRetrieve(true);
		
		DrugChargeInfo drugChargeInfo = dmDrug.getDrugChargeInfo();
		if( dmDrug.getDrugChargeInfo() != null ){				
			String itemFmStatus = dmDrug.getPmsFmStatus().getFmStatus();
			
			Double corpDrugPrice = drugChargeInfo.getCorpDrugPrice() == null ? 0.0 : drugChargeInfo.getCorpDrugPrice();
			drugPriceInfo.setCorpDrugPrice(corpDrugPrice);
			
			DrugChargeRuleCriteria chargingRuleCriteria  = chargeCalculation.retrieveChargeRuleByPatTypeFmStatus(PharmOrderPatType.Public, itemFmStatus);
			OverrideChargeAmountRuleCriteria overrideChargeAmountRuleCriteria = new OverrideChargeAmountRuleCriteria();
			overrideChargeAmountRuleCriteria.updateByDrugChargeRuleCriteria(chargingRuleCriteria, workstore);
			drugPriceInfo.setPublicMarkupFactor(chargeCalculation.retrieveMarkupFactorByChargeRule(overrideChargeAmountRuleCriteria, drugChargeInfo));
			drugPriceInfo.setPublicHandleCharge(chargeCalculation.retrieveHandlingAmtByChargeRule(chargingRuleCriteria, drugChargeInfo));
			
			chargingRuleCriteria  = chargeCalculation.retrieveChargeRuleByPatTypeFmStatus(PharmOrderPatType.Private, itemFmStatus);
			overrideChargeAmountRuleCriteria.updateByDrugChargeRuleCriteria(chargingRuleCriteria, workstore);
			drugPriceInfo.setPrivateMarkupFactor(chargeCalculation.retrieveMarkupFactorByChargeRule(overrideChargeAmountRuleCriteria, drugChargeInfo));
			drugPriceInfo.setPrivateHandleCharge(chargeCalculation.retrieveHandlingAmtByChargeRule(chargingRuleCriteria, drugChargeInfo));

			chargingRuleCriteria  = chargeCalculation.retrieveChargeRuleByPatTypeFmStatus(PharmOrderPatType.Nep, itemFmStatus);
			overrideChargeAmountRuleCriteria.updateByDrugChargeRuleCriteria(chargingRuleCriteria, workstore);
			drugPriceInfo.setNepMarkupFactor(chargeCalculation.retrieveMarkupFactorByChargeRule(overrideChargeAmountRuleCriteria, drugChargeInfo));
			drugPriceInfo.setNepHandleCharge(chargeCalculation.retrieveHandlingAmtByChargeRule(chargingRuleCriteria, drugChargeInfo));
		}
		return drugPriceInfo;
	}
	
	public void retrieveSfiDrugPrice(String itemCode, Date retrieveMonth){
				
			drugPriceEnquiryInfo = retrieveBaseDrugPriceInfo(itemCode);
			
			if( drugPriceEnquiryInfo != null ){
				List<String> itemCodeList = new ArrayList<String>();
				itemCodeList.add(itemCode);
				refreshDrugPriceHistoryMap(itemCodeList);
				
				if ( sfiDrugPriceHistoryMap.get(itemCode) != null && sfiDrugPriceHistoryMap.get(itemCode).get(df.format(retrieveMonth)) != null ){
					DmCdpReport dmCdpReport = sfiDrugPriceHistoryMap.get(itemCode).get(df.format(retrieveMonth));
					updateHistoryDrugPriceInfo(dmCdpReport, drugPriceEnquiryInfo);
				}else{
					updateHistoryDrugPriceInfo(null, drugPriceEnquiryInfo);
				}
			}
	}
	
	private DrugPriceInfo updateHistoryDrugPriceInfo(DmCdpReport dmCdpReport, DrugPriceInfo drugPriceEnqInfo){
		if( dmCdpReport != null ){
			drugPriceEnqInfo.setHistoryFullDrugDesc( dmCdpReport.getFullDrugDesc() );
			drugPriceEnqInfo.setHistoryCorpDrugPrice(dmCdpReport.getCorpDrugPrice() == null ? 0.0: dmCdpReport.getCorpDrugPrice());
			drugPriceEnqInfo.setValidHistoryPriceRetrieve(true);
		}else{
			if( StringUtils.isNotBlank(drugPriceEnqInfo.getFullDrugDesc()) ){
				drugPriceEnqInfo.setHistoryFullDrugDesc(drugPriceEnqInfo.getFullDrugDesc());
			}else{
				drugPriceEnqInfo.setHistoryFullDrugDesc(null);
			}
			drugPriceEnqInfo.setHistoryCorpDrugPrice(Double.valueOf(0));
			drugPriceEnqInfo.setValidHistoryPriceRetrieve(false);
		}
		return drugPriceEnqInfo;		
	}
	
	@Remove
	public void destroy() 
	{
		if ( drugPriceEnquiryInfo != null ){
			drugPriceEnquiryInfo = null;
		}
			
		if ( drugPriceEnquiryInfoList != null ){
			drugPriceEnquiryInfoList = null;
		}
		
		if( sfiDrugPriceHistoryMap != null ){
			sfiDrugPriceHistoryMap = null;
		}
	}
}
