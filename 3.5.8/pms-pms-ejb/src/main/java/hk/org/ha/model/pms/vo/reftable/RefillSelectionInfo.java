package hk.org.ha.model.pms.vo.reftable;

import hk.org.ha.model.pms.persistence.reftable.RefillSelection;
import hk.org.ha.model.pms.vo.patient.PasSpecialty;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class RefillSelectionInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<RefillSelection> refillSelectionList;

	private List<PasSpecialty> refillModulePasSpecialtyList;

	public void setRefillSelectionList(List<RefillSelection> refillSelectionList) {
		this.refillSelectionList = refillSelectionList;
	}

	public List<RefillSelection> getRefillSelectionList() {
		return refillSelectionList;
	}

	public void setRefillModulePasSpecialtyList(
			List<PasSpecialty> refillModulePasSpecialtyList) {
		this.refillModulePasSpecialtyList = refillModulePasSpecialtyList;
	}

	public List<PasSpecialty> getRefillModulePasSpecialtyList() {
		return refillModulePasSpecialtyList;
	}
}