package hk.org.ha.model.pms.vo.medprofile;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type = DefaultExternalizer.class)
public class ErrorInfoItem implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@XmlElement
	private String itemCode;
	
	@XmlElement
	private String drugName;
	
	@XmlElement
	private String formLabelDesc;
	
	@XmlElement
	private String strength;
	
	@XmlElement
	private String volumeText;
	
	@XmlElement
	private String drugSynonym;
	
	@XmlElement
	private String drugOrderTlf;

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	
	public String getDrugName() {
		return drugName;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public String getFormLabelDesc() {
		return formLabelDesc;
	}

	public void setFormLabelDesc(String formLabelDesc) {
		this.formLabelDesc = formLabelDesc;
	}

	public String getStrength() {
		return strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public String getVolumeText() {
		return volumeText;
	}

	public void setVolumeText(String volumeText) {
		this.volumeText = volumeText;
	}

	public String getDrugSynonym() {
		return drugSynonym;
	}

	public void setDrugSynonym(String drugSynonym) {
		this.drugSynonym = drugSynonym;
	}

	public String getDrugOrderTlf() {
		return drugOrderTlf;
	}

	public void setDrugOrderTlf(String drugOrderTlf) {
		this.drugOrderTlf = drugOrderTlf;
	}
}
