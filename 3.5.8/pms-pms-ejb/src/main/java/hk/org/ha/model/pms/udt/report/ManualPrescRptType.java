package hk.org.ha.model.pms.udt.report;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;
import hk.org.ha.model.pms.udt.charging.ClearanceStatus;

public enum ManualPrescRptType implements StringValuedEnum {

	SummaryManualPresc("SummaryManualPresc", "Summary on number of manual prescription entered in PMS-OP"),
	DetailManualPresc("DetailManualPresc", "Details of manual prescriptions entered in PMS-OP");
	
    private final String dataValue;
    private final String displayValue;

    ManualPrescRptType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static ManualPrescRptType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(ManualPrescRptType.class, dataValue);
	}
    
	public static class Converter extends StringValuedEnumConverter<ClearanceStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<ClearanceStatus> getEnumClass() {
    		return ClearanceStatus.class;
    	}
    }
}
