package hk.org.ha.model.pms.cache;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmCapdVoucherMap;
import hk.org.ha.model.pms.dms.persistence.DmCapdVoucherMapPK;
import hk.org.ha.model.pms.udt.vetting.DmCapdVoucherMapType;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.Collection;
import java.util.List;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("dmCapdVoucherMapCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class DmCapdVoucherMapCacher extends BaseCacher implements DmCapdVoucherMapCacherInf {

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

    private InnerCacher cacher;
	
	public List<DmCapdVoucherMap> getDmCapdVoucherMapList() {
		return (List<DmCapdVoucherMap>) this.getCacher().getAll();
	}

	public DmCapdVoucherMap getDmCapdVoucher(String txtEng, DmCapdVoucherMapType mapType) {
		DmCapdVoucherMapPK pk = new DmCapdVoucherMapPK();
		pk.setTxtEng(txtEng);
		pk.setMapType(mapType.getDataValue());
		return this.getCacher().get(pk);
	}
	
    private InnerCacher getCacher() {
		synchronized (this) {
			if (cacher == null) {
				cacher = new InnerCacher(this.getExpireTime());
			}
			return cacher;
		}
  }

	private class InnerCacher extends AbstractCacher<DmCapdVoucherMapPK, DmCapdVoucherMap>
	{		
        public InnerCacher(int expireTime) {
              super(expireTime);
        }

		@Override
		public DmCapdVoucherMap create(DmCapdVoucherMapPK key) {
			this.getAll();
			return this.internalGet(key);
		}

		@Override
		public Collection<DmCapdVoucherMap> createAll() {
			return dmsPmsServiceProxy.retrieveDmCapdVoucherMap();
		}

		@Override
		public DmCapdVoucherMapPK retrieveKey(DmCapdVoucherMap dmCapdVoucherMap) {
			return dmCapdVoucherMap.getCompId();
		}	
		
	}
	
	public static DmCapdVoucherMapCacherInf instance()
	{
		  if (!Contexts.isApplicationContextActive())
	      {
	         throw new IllegalStateException("No active application scope");
	      }
	      return (DmCapdVoucherMapCacherInf) Component.getInstance("dmCapdVoucherMapCacher", ScopeType.APPLICATION);
    }	
}
