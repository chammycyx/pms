package hk.org.ha.model.pms.biz.inbox;

import hk.org.ha.model.pms.persistence.disp.MedOrder;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

public interface PharmInboxClientNotifierInf {

    void notifyClient(Serializable object, Map<String, Object> headerParams);
    void dispatchUpdateEvent(String hospCode);
    void dispatchUpdateEvent(Set<String> hospCodeSet);
	void dispatchUpdateEvent(MedOrder medOrder);    
}
