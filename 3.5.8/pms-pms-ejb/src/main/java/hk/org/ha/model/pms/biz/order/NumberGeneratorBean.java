package hk.org.ha.model.pms.biz.order;

import static hk.org.ha.model.pms.prop.Prop.CHARGING_SFI_INVOICE_INVOICENUM;
import static hk.org.ha.model.pms.prop.Prop.CHARGING_STANDARD_INVOICE_INVOICENUM;
import static hk.org.ha.model.pms.prop.Prop.PIVAS_BATCHPREP_LOTNUM;
import static hk.org.ha.model.pms.prop.Prop.PIVAS_BATCHPREP_LOTNUM_MAX;
import static hk.org.ha.model.pms.prop.Prop.PIVAS_BATCHPREP_LOTNUM_MIN;
import static hk.org.ha.model.pms.prop.Prop.TICKETGEN_FASTQUEUE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.TICKETGEN_FASTQUEUE_TICKET_TICKETNUM;
import static hk.org.ha.model.pms.prop.Prop.TICKETGEN_FASTQUEUE_TICKET_TICKETNUM_MAX;
import static hk.org.ha.model.pms.prop.Prop.TICKETGEN_FASTQUEUE_TICKET_TICKETNUM_MIN;
import static hk.org.ha.model.pms.prop.Prop.TICKETGEN_QUEUE1_TICKET_TICKETNUM;
import static hk.org.ha.model.pms.prop.Prop.TICKETGEN_QUEUE1_TICKET_TICKETNUM_MAX;
import static hk.org.ha.model.pms.prop.Prop.TICKETGEN_QUEUE1_TICKET_TICKETNUM_MIN;
import static hk.org.ha.model.pms.prop.Prop.TICKETGEN_QUEUE2_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.TICKETGEN_QUEUE2_TICKET_TICKETNUM;
import static hk.org.ha.model.pms.prop.Prop.TICKETGEN_QUEUE2_TICKET_TICKETNUM_MAX;
import static hk.org.ha.model.pms.prop.Prop.TICKETGEN_QUEUE2_TICKET_TICKETNUM_MIN;
import static hk.org.ha.model.pms.prop.Prop.VETTING_CAPD_VOUCHER_VOUCHERNUM;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.persistence.reftable.CorporateProp;
import hk.org.ha.model.pms.persistence.reftable.HospitalProp;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.persistence.reftable.WorkstoreProp;
import hk.org.ha.model.pms.udt.YesNoFlag;
import hk.org.ha.model.pms.udt.ticketgen.TicketType;
import hk.org.ha.model.pms.udt.vetting.OrderType;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;

@AutoCreate
@Stateless
@Name("numberGenerator")
@MeasureCalls
public class NumberGeneratorBean implements NumberGeneratorLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;

	@In
	private Workstore workstore;
	
	@In
	private Workstation workstation;
		
	private DecimalFormat voucherDecimalFormat = new DecimalFormat( "000000" );

	private DateFormat yearDateFormat = new SimpleDateFormat("yy");
	
	private DateFormat fullYearDateFormat = new SimpleDateFormat("yyyy");
	
	private DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

	private static final Integer DEF_MAX_TICKET_NUM = 9999;
	
	private DecimalFormat ticketNumFormat = new DecimalFormat("0000");
	
	private static final String[] TICKET_TICKETNUM_PROP_NAME_LIST = new String[]{TICKETGEN_QUEUE1_TICKET_TICKETNUM.getName(),
																				 TICKETGEN_QUEUE2_TICKET_TICKETNUM.getName(),
																				 TICKETGEN_FASTQUEUE_TICKET_TICKETNUM.getName()};
	
	private static final List<String> INIT_TICKETNUM_LIST = Arrays.asList(TICKETGEN_QUEUE1_TICKET_TICKETNUM_MAX.getName(),
																		  TICKETGEN_QUEUE2_TICKET_TICKETNUM_MAX.getName(),
																		  TICKETGEN_FASTQUEUE_TICKET_TICKETNUM_MAX.getName(),
																		  TICKETGEN_QUEUE1_TICKET_TICKETNUM_MIN.getName(),
																		  TICKETGEN_QUEUE2_TICKET_TICKETNUM_MIN.getName(),
																		  TICKETGEN_FASTQUEUE_TICKET_TICKETNUM_MIN.getName(),
																		  TICKETGEN_QUEUE2_ENABLED.getName(),
																		  TICKETGEN_FASTQUEUE_ENABLED.getName());
		
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public String retrieveCapdVoucherNum(){
		
		List<Long> hospitalPropIdList = this.lockHospitalProp(workstore.getHospital(), VETTING_CAPD_VOUCHER_VOUCHERNUM.getName());
		
		List<HospitalProp> hospitalPropList = em.createQuery(
				"select o from HospitalProp o" + // 20120312 index check : HospitalProp.id : PK_HOSPITAL_PROP
				" where o.id in :idList")
				.setParameter("idList", hospitalPropIdList)
				.getResultList();
		
		if ( hospitalPropList.isEmpty() ) {
			logger.info("retrieveCapdVoucherNum cannot find prop: #0", VETTING_CAPD_VOUCHER_VOUCHERNUM.getName());
			return StringUtils.EMPTY;
		}
		
		HospitalProp capdVoucherNumProp = hospitalPropList.get(0);
		Integer capdVoucherNum = Integer.valueOf(capdVoucherNumProp.getValue());	
		String capdVoucherNumPropDate = fullYearDateFormat.format(capdVoucherNumProp.getUpdateDate());
		
		Integer newVoucherNum = capdVoucherNum;
		if ( fullYearDateFormat.format(new Date()).equals(capdVoucherNumPropDate)) 
		{
			newVoucherNum = newVoucherNum+1;
		} 
		else 
		{
			String validationString = capdVoucherNumProp.getProp().getValidation();
			String[] validationSplit = StringUtils.split(validationString, "-");
			newVoucherNum = Integer.parseInt(validationSplit[0]) + 1;
			logger.debug("retrieveCapdVoucherNum newYear reset base on validation: #0", newVoucherNum);
		}
		
		capdVoucherNumProp.setValue(newVoucherNum.toString());
		
		if ( capdVoucherNum.equals(newVoucherNum) ) 
		{
			capdVoucherNumProp.setUpdateDate(new DateTime().toDate());
		}
		
		return StringUtils.rightPad(workstore.getHospCode(), 3) + yearDateFormat.format(new Date()) + voucherDecimalFormat.format(newVoucherNum);

	}	
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public String retrievePivasBatchPrepNum(){
		
		List<Long> hospitalPropIdList = this.lockHospitalProp(workstore.getHospital(), PIVAS_BATCHPREP_LOTNUM.getName());
		
		List<HospitalProp> hospitalPropList = em.createQuery(
				"select o from HospitalProp o" + // 20120312 index check : HospitalProp.id : PK_HOSPITAL_PROP
				" where o.id in :idList")
				.setParameter("idList", hospitalPropIdList)
				.getResultList();
		
		if ( hospitalPropList.isEmpty() ) {
			logger.info("retrievePivasBatchPrepNum cannot find prop: #0", PIVAS_BATCHPREP_LOTNUM.getName());
			return StringUtils.EMPTY;
		}
		
		HospitalProp batchPrepLotNumProp = hospitalPropList.get(0);
		Integer batchPrepLotNum = Integer.valueOf(batchPrepLotNumProp.getValue());	
		String batchPrepLotNumPropDate = dateFormat.format(batchPrepLotNumProp.getUpdateDate());
		
		Integer newPivasBatchPrepNum = batchPrepLotNum;
		if ( dateFormat.format(new Date()).equals(batchPrepLotNumPropDate) )  
		{
			newPivasBatchPrepNum = newPivasBatchPrepNum+1;
		} 
		else 
		{
			newPivasBatchPrepNum = PIVAS_BATCHPREP_LOTNUM_MIN.get();
			logger.debug("retrievePivasBatchPrepNum reset base on validation: #0", newPivasBatchPrepNum);
		}
		
		if ( newPivasBatchPrepNum > PIVAS_BATCHPREP_LOTNUM_MAX.get() ) {
			return null;
		}
		
		batchPrepLotNumProp.setValue(newPivasBatchPrepNum.toString());
		
		if ( batchPrepLotNum.equals(newPivasBatchPrepNum) ) 
		{
			batchPrepLotNumProp.setUpdateDate(new DateTime().toDate());
		}
		
		return StringUtils.rightPad(workstore.getHospCode(), 3) + dateFormat.format(new Date()) + newPivasBatchPrepNum;

	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Integer retrieveSfiInvoiceNumber(){
		List<Long> corpPropIdList = this.lockCorporateProp(CHARGING_SFI_INVOICE_INVOICENUM.getName());
		
		List<CorporateProp> corpPropList = em.createQuery(
				"select o from CorporateProp o" + // 20140317 index check : CorporateProp.id : PK_CORPORATE_PROP
				" where o.id in :idList")
				.setParameter("idList", corpPropIdList)
				.getResultList();		
		
		Integer invoiceNum = null;
		if( corpPropList.isEmpty() ){
			logger.info("retrieveSfiInvoiceNumber cannot find prop: #0", CHARGING_SFI_INVOICE_INVOICENUM.getName());
			return invoiceNum;
		}
		
		CorporateProp invoiceCorporateProp = corpPropList.get(0);
		invoiceNum = Integer.parseInt(invoiceCorporateProp.getValue());
		if( StringUtils.equals(yearDateFormat.format(invoiceCorporateProp.getUpdateDate()), yearDateFormat.format(new Date()))){
			invoiceNum = invoiceNum + 1;
		}else{
			String validationString = invoiceCorporateProp.getProp().getValidation();
			String[] validationSplit = StringUtils.split(validationString, "-");
			if(  StringUtils.isNumeric(validationSplit[0]) ){
				invoiceNum = Integer.parseInt(validationSplit[0]) + 1;
				logger.debug("retrieveSfiInvoiceNumber newYear reset base on validation: #0", invoiceNum);
			}
			invoiceCorporateProp.setUpdateDate(new DateTime().toDate());
		}
		invoiceCorporateProp.setValue(invoiceNum.toString());
		
		return invoiceNum;
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Integer retrieveStandardInvoiceNumber(){
		List<Long> hospitalPropIdList = this.lockHospitalProp(workstore.getHospital(), CHARGING_STANDARD_INVOICE_INVOICENUM.getName());
		
		List<HospitalProp> hospitalPropList = em.createQuery(
				"select o from HospitalProp o" + // 20120312 index check : HospitalProp.id : PK_HOSPITAL_PROP
				" where o.id in :idList")
				.setParameter("idList", hospitalPropIdList)
				.getResultList();
		
		Integer invoiceNum = null;
		if( hospitalPropList.isEmpty() ){
			logger.info("retrieveStandardInvoiceNumber cannot find prop: #0", CHARGING_STANDARD_INVOICE_INVOICENUM.getName());
			return invoiceNum;
		}
		
		HospitalProp invoiceHospProp = hospitalPropList.get(0);
		invoiceNum = Integer.parseInt(invoiceHospProp.getValue());
		if( StringUtils.equals(yearDateFormat.format(invoiceHospProp.getUpdateDate()), yearDateFormat.format(new Date()))){
			invoiceNum = invoiceNum + 1;
		}else{
			String validationString = invoiceHospProp.getProp().getValidation();
			String[] validationSplit = StringUtils.split(validationString, "-");
			if(  StringUtils.isNumeric(validationSplit[0]) ){
				invoiceNum = Integer.parseInt(validationSplit[0]) + 1;
				logger.debug("retrieveStandardInvoiceNumber newYear reset base on validation: #0", invoiceNum);
			}
			invoiceHospProp.setUpdateDate(new DateTime().toDate());
		}
		invoiceHospProp.setValue(invoiceNum.toString());
		
		return invoiceNum;		
	}
	
	private TicketType retrieveExtendedTicketType(boolean queue1Avail, boolean queue2Avail, boolean queue3Avail){
		Integer maxDefaultTicketNum = null;
		TicketType extendedTicketType = null;
		
		if( queue1Avail ){
			maxDefaultTicketNum = retrieveMaxTicketNumByTicketType(TicketType.Queue1);
			extendedTicketType = TicketType.Queue1;
		}

		if( queue2Avail ){
			if( maxDefaultTicketNum == null || retrieveMaxTicketNumByTicketType(TicketType.Queue2) > maxDefaultTicketNum ){
				maxDefaultTicketNum = retrieveMaxTicketNumByTicketType(TicketType.Queue2);
				extendedTicketType = TicketType.Queue2;
			}
		}
		
		if( queue3Avail ){
			if( maxDefaultTicketNum == null || retrieveMaxTicketNumByTicketType(TicketType.FastQueue) > maxDefaultTicketNum ){
				maxDefaultTicketNum = retrieveMaxTicketNumByTicketType(TicketType.FastQueue);
				extendedTicketType = TicketType.FastQueue;
			}
		}
		return extendedTicketType;
	}
	
	private TicketType retrieveMaxTicketType(TicketType ticketType1, TicketType ticketType2){
		if( retrieveMaxTicketNumByTicketType(ticketType1) > retrieveInitTicketNumByTicketType(ticketType2) ){
			return ticketType1;
		}
		return ticketType2;
	}
	
	private boolean isTicketTypeAvailByCurrTicketNum(boolean ticketTypeAvail, WorkstoreProp ticketNumWorkstoreProp, TicketType extendedTicketType, TicketType inTicketType){
		if( !ticketTypeAvail ){
			return ticketTypeAvail;
		}
		
		Integer currTicketNum = null;
		
		DateMidnight workstoreUpdateDate = new DateMidnight(ticketNumWorkstoreProp.getUpdateDate());
		if( !new DateMidnight().equals(workstoreUpdateDate) ){
			currTicketNum = retrieveInitTicketNumByTicketType(inTicketType);
		}else{
			currTicketNum = Integer.valueOf(ticketNumWorkstoreProp.getValue());
		}
		
		logger.debug("isTicketTypeAvailByCurrTicketNum====================ticketType:#0, currTicketNum:#1, maxTicketNum:#2, extendedTicketType:#3", inTicketType, currTicketNum, retrieveMaxTicketNumByTicketType(inTicketType), extendedTicketType);
		
		if( !extendedTicketType.equals(inTicketType) && currTicketNum >= retrieveMaxTicketNumByTicketType(inTicketType) ){
			ticketTypeAvail = false;
		}
		return ticketTypeAvail;
	}
	
	private List<TicketType> retrieveSortedActiveTicketTypeList(Map<TicketType, WorkstoreProp> ticketWorkstorePropMap){
		boolean queue1Avail = false;
		boolean queue2Avail = false;
		boolean queue3Avail = false;
		
		queue1Avail = isActiveTicketType(TicketType.Queue1);
		queue2Avail = isActiveTicketType(TicketType.Queue2);
		queue3Avail = isActiveTicketType(TicketType.FastQueue);

		if( !queue2Avail && !queue3Avail ){
			return Arrays.asList(TicketType.Queue1);
		}else if( !queue1Avail && !queue3Avail ){
			return Arrays.asList(TicketType.Queue2);
		}else if( !queue1Avail && !queue2Avail ){
			return Arrays.asList(TicketType.FastQueue);
		}
		
		TicketType extendedTicketType = retrieveExtendedTicketType(queue1Avail, queue2Avail, queue3Avail);
		
		logger.debug("retrieveSortedActiveTicketTypeList === TicketAvail: Queue1:#0, Queue2:#1, Queue3:#2, ExtendedTicketType:#3", queue1Avail, queue2Avail, queue3Avail, extendedTicketType);

		if( ticketWorkstorePropMap.containsKey(TicketType.Queue1) ){
			queue1Avail = isTicketTypeAvailByCurrTicketNum(queue1Avail, ticketWorkstorePropMap.get(TicketType.Queue1), extendedTicketType, TicketType.Queue1);
		}
		
		if( ticketWorkstorePropMap.containsKey(TicketType.Queue2) ){
			queue2Avail = isTicketTypeAvailByCurrTicketNum(queue2Avail, ticketWorkstorePropMap.get(TicketType.Queue2), extendedTicketType, TicketType.Queue2);
		}
		
		if( ticketWorkstorePropMap.containsKey(TicketType.FastQueue) ){
			queue3Avail = isTicketTypeAvailByCurrTicketNum(queue3Avail, ticketWorkstorePropMap.get(TicketType.FastQueue), extendedTicketType, TicketType.FastQueue);
		}

		logger.debug("retrieveSortedActiveTicketTypeList2 === TicketAvail: Queue1:#0, Queue2:#1, Queue3:#2, ExtendedTicketType:#3", queue1Avail, queue2Avail, queue3Avail, extendedTicketType);

		if( !queue2Avail && !queue3Avail ){
			return Arrays.asList(TicketType.Queue1);
		}else if( !queue1Avail && !queue3Avail ){
			return Arrays.asList(TicketType.Queue2);
		}else if( !queue1Avail && !queue2Avail ){
			return Arrays.asList(TicketType.FastQueue);
		}
		
		TicketType minTicketType = null;
		TicketType midTicketType = null;
		TicketType maxTicketType = null;

		if( queue3Avail ){
			minTicketType = TicketType.FastQueue;
		}
		
		if( queue2Avail ){
			if( minTicketType == null ){
				minTicketType = TicketType.Queue2;
			}else{
				midTicketType = retrieveMaxTicketType( TicketType.Queue2, minTicketType );
				if( midTicketType.equals(minTicketType) ){
					minTicketType = TicketType.Queue2;
				}
			}
		}
		
		if( queue1Avail ){
			if( minTicketType == null ){
				minTicketType = TicketType.Queue1;
			}else if( midTicketType == null ){
				midTicketType = retrieveMaxTicketType( TicketType.Queue1, minTicketType );
				if( midTicketType.equals(minTicketType) ){
					minTicketType = TicketType.Queue1;
				}
			}else{
				maxTicketType = retrieveMaxTicketType( TicketType.Queue1, midTicketType );
				if( maxTicketType.equals(midTicketType) ){
					midTicketType = retrieveMaxTicketType( TicketType.Queue1, minTicketType );
					if( midTicketType.equals(minTicketType) ){
						minTicketType = TicketType.Queue1;
					}
				}
			}
		}
		
		List<TicketType> validTicketTypeList = new ArrayList<TicketType>();
		if( minTicketType != null ){
			validTicketTypeList.add(minTicketType);
		}
		if( midTicketType != null ){
			validTicketTypeList.add(midTicketType);
		}
		if( maxTicketType != null ){
			validTicketTypeList.add(maxTicketType);
		}
		return validTicketTypeList;
	}
	
	private List<TicketType> retrieveActiveTicketTypeList(Map<TicketType, WorkstoreProp> ticketWorkstorePropMap, TicketType inTicketType, Integer inCurrentTicketNum){
		List<TicketType> sortedTicketTypeList = retrieveSortedActiveTicketTypeList(ticketWorkstorePropMap);
		
		if( sortedTicketTypeList.size() == 1 ){
			return sortedTicketTypeList;
		}
		
		Integer currentTicketNum = inCurrentTicketNum;
		if( currentTicketNum == null ){
			currentTicketNum = retrieveInitTicketNumByTicketType(inTicketType);
		}

		List<TicketType> activeTicketTypeList = new ArrayList<TicketType>();

		for( TicketType sortTicketType : sortedTicketTypeList ){
			if( currentTicketNum >= retrieveInitTicketNumByTicketType(sortTicketType) && currentTicketNum < retrieveMaxTicketNumByTicketType(sortTicketType) ){
				activeTicketTypeList.add(sortTicketType);
			}else if( currentTicketNum >= retrieveInitTicketNumByTicketType(sortTicketType) && inCurrentTicketNum != null && inCurrentTicketNum <= retrieveMaxTicketNumByTicketType(sortTicketType) ){
				activeTicketTypeList.add(sortTicketType);
			}else if (currentTicketNum < retrieveInitTicketNumByTicketType(sortTicketType) ){
				activeTicketTypeList.add(sortTicketType);
			}
		}
		return activeTicketTypeList;
	}
		
	public boolean isFastQueueTicketNum(String ticketNum){
		if( !isActiveTicketType(TicketType.FastQueue) ){
			return false;
		}
		
		Integer initNum = retrieveInitTicketNumByTicketType(TicketType.FastQueue);
		Integer maxNum = retrieveMaxTicketNumByTicketType(TicketType.FastQueue);
		Integer currTicketNum = Integer.valueOf(ticketNum);
		if( currTicketNum >= initNum && currTicketNum <= maxNum ){
			return true;
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Ticket retrieveTicketByTickDateType(Date ticketDate, TicketType ticketType, Integer customTicketNum){
		
		List<Long> workstorePropIdList = this.lockWorkstoreProp(
				workstore,
				TICKET_TICKETNUM_PROP_NAME_LIST);
				
		List<WorkstoreProp> ticketWorkstorePropList = em.createQuery(
				"select o from WorkstoreProp o" + // 20120312 index check : WorkstoreProp.id : PK_WORKSTORE_PROP
				" where o.id in :idList")
				.setParameter("idList", workstorePropIdList)
				.getResultList();
		
		//generate Ticket for Previous Date
		Date today = new DateMidnight().toDate();
		if( customTicketNum != null && today.compareTo(ticketDate) > 0 ){
			logger.debug("retrieveTicketByTickDateType====PrevsDateTicket:#0, customTicketNum:#1",ticketDate, customTicketNum);
			return createTicket(ticketDate, customTicketNum);
		}
		
		//generate Ticket for Current Date
		if( ticketWorkstorePropList.size() > 0 ){
			Map<TicketType,WorkstoreProp> ticketTypePropMap = new HashMap<TicketType, WorkstoreProp>();
			
			for( WorkstoreProp workstoreProp : ticketWorkstorePropList ){
				if( workstoreProp.getProp().getName().equals(TICKETGEN_QUEUE1_TICKET_TICKETNUM.getName()) ){
					ticketTypePropMap.put(TicketType.Queue1, workstoreProp);
				}else if( workstoreProp.getProp().getName().equals(TICKETGEN_QUEUE2_TICKET_TICKETNUM.getName()) ){
					ticketTypePropMap.put(TicketType.Queue2, workstoreProp);
				}else if( workstoreProp.getProp().getName().equals(TICKETGEN_FASTQUEUE_TICKET_TICKETNUM.getName()) ){
					ticketTypePropMap.put(TicketType.FastQueue, workstoreProp);
				}
			}
			List<TicketType> sortedWorkstorePropList = retrieveActiveTicketTypeList(ticketTypePropMap, ticketType, customTicketNum);
		
				for( TicketType sortTicketType : sortedWorkstorePropList ){
						WorkstoreProp ticketNumProp = ticketTypePropMap.get(sortTicketType);
						Integer newTicketNum = Integer.valueOf(ticketNumProp.getValue());
						
						if( customTicketNum == null ){
							if( !new DateMidnight(ticketDate).equals(new DateMidnight(ticketNumProp.getUpdateDate())) ){
								newTicketNum = retrieveTicketNumMinWorkstorePropFromDb(sortTicketType);
								if( newTicketNum.equals(Integer.valueOf(ticketNumProp.getValue())) ){
									ticketNumProp.setUpdateDate(new DateTime().toDate());
								}
							}else if( newTicketNum.equals(retrieveInitTicketNumByTicketType(sortTicketType)) && (new DateMidnight(ticketDate).equals(new DateMidnight(ticketNumProp.getUpdateDate()))) ){
								List<Ticket> initTicketList = this.retrieveTicketListByTicketNumDate(ticketDate, newTicketNum);
								
								if( initTicketList.isEmpty() ){
									ticketNumProp.setUpdateDate(ticketDate);
								}else{
									newTicketNum = newTicketNum+1;
								}
							}else{
								newTicketNum = newTicketNum+1;
							}
							if( newTicketNum <= DEF_MAX_TICKET_NUM ){
								ticketNumProp.setValue(newTicketNum.toString());
								ticketNumProp.setUpdateDate(ticketDate);
							}
						}else{
							if( !new DateMidnight(ticketDate).equals(new DateMidnight(ticketNumProp.getUpdateDate())) ){
								newTicketNum = retrieveTicketNumMinWorkstorePropFromDb(sortTicketType);
								if( newTicketNum.equals(customTicketNum) ){
									ticketNumProp.setUpdateDate(ticketDate);
									ticketNumProp.setValue(newTicketNum.toString());
								}else if( customTicketNum > newTicketNum ){
									newTicketNum = customTicketNum;
								}
							}else if( newTicketNum.equals(retrieveInitTicketNumByTicketType(sortTicketType)) && customTicketNum.equals(newTicketNum) && (new DateMidnight(ticketDate).equals(new DateMidnight(ticketNumProp.getUpdateDate()))) ){
								List<Ticket> initTicketList = retrieveTicketListByTicketNumDate(ticketDate, newTicketNum);

								if( initTicketList.isEmpty() ){
									newTicketNum = customTicketNum;
								}else{
									newTicketNum = newTicketNum+1;
								}
							}else if ( newTicketNum > customTicketNum ){
								Ticket emptyTicket = null;
								
								List<Ticket> ticketList = retrieveTicketListByTicketNumDate(ticketDate, customTicketNum);

								if (!ticketList.isEmpty()) {
									emptyTicket = ticketList.get(0);
								}
								
								if( emptyTicket == null ){
									logger.debug("retrieveTicketByTickDateType - Workstore[#0, #1], Workstation[#2], newTicketNum:#3, ticketNumProp:[#4,#5,#6]",
											workstore.getHospCode(), workstore.getWorkstoreCode(),
											(workstation == null ? null : workstation.getHostName()),
											customTicketNum, ticketNumProp.getId(), ticketNumProp.getValue(), ticketNumProp.getUpdateDate());
									return createTicket(ticketDate, customTicketNum);
								}else{
									newTicketNum = newTicketNum + 1;
									ticketNumProp.setValue(newTicketNum.toString());
									ticketNumProp.setUpdateDate(ticketDate);
								}
							}else{
								if( customTicketNum == newTicketNum + 1 ){
									ticketNumProp.setValue(customTicketNum.toString());
									ticketNumProp.setUpdateDate(ticketDate);
								}
								newTicketNum = customTicketNum;
							}
						}
							
						Integer maxTicketNum = this.retrieveMaxTicketNumByTicketType(sortTicketType);
						if( newTicketNum > maxTicketNum ){
							maxTicketNum = DEF_MAX_TICKET_NUM;
						}
					
						if( newTicketNum > DEF_MAX_TICKET_NUM ){
							logger.debug("retrieveTicketByTickDateType - Workstore[#0, #1], Workstation[#2], newTicketNum:#3",
									workstore.getHospCode(), workstore.getWorkstoreCode(),
									(workstation == null ? null : workstation.getHostName()),
									newTicketNum);
							return null;
						}
						
						List<Ticket> ticketList = em.createQuery(
													"select o from Ticket o" + // 20120307 index check : Ticket.workstore,orderType,ticketDate,ticketNum : UI_TICKET_01
													" where o.workstore = :workstore" +
													" and o.orderType = :orderType" +
													" and o.ticketDate = :ticketDate" +
													" and o.ticketNum between :startTicketNum and :endTicketNum " +
													" order by o.ticketNum")
													.setParameter("workstore", workstore)
													.setParameter("orderType", OrderType.OutPatient)
													.setParameter("startTicketNum", ticketNumFormat.format(newTicketNum))
													.setParameter("endTicketNum", ticketNumFormat.format(maxTicketNum))
													.setParameter("ticketDate", ticketDate, TemporalType.DATE)
													.getResultList();
				
						if( ticketList.isEmpty() ){
							logger.debug("retrieveTicketByTickDateType - Workstore[#0, #1], Workstation[#2], newTicketNum:#3, ticketNumProp:[#4,#5,#6]",
									workstore.getHospCode(), workstore.getWorkstoreCode(),
									(workstation == null ? null : workstation.getHostName()),
									newTicketNum, ticketNumProp.getId(), ticketNumProp.getValue(), ticketNumProp.getUpdateDate());
							return createTicket(ticketDate, newTicketNum);
						}else{
							if( ticketList.size() == (maxTicketNum-newTicketNum+1) ){
								continue;
							}else{
								Integer refTicketNum = null;
								Boolean isInitNewTicketNum = true;
								for( Ticket ticket : ticketList ){
									refTicketNum = Integer.valueOf(ticket.getTicketNum());
									if( refTicketNum.equals(newTicketNum) ){
										newTicketNum = newTicketNum + 1;
										isInitNewTicketNum = false;
									}else if( refTicketNum > newTicketNum ){
										Integer propTicketNum = Integer.valueOf(ticketNumProp.getValue());
										if( refTicketNum == propTicketNum +1 ){
											ticketNumProp.setValue(refTicketNum.toString());
											ticketNumProp.setUpdateDate(ticketDate);
										}else if( !isInitNewTicketNum ){
											ticketNumProp.setValue(newTicketNum.toString());
											ticketNumProp.setUpdateDate(ticketDate);
										}
										logger.debug("retrieveTicketByTickDateType - Workstore[#0, #1], Workstation[#2], newTicketNum:#3, ticketNumProp:[#4,#5,#6]",
												workstore.getHospCode(), workstore.getWorkstoreCode(),
												(workstation == null ? null : workstation.getHostName()),
												newTicketNum, ticketNumProp.getId(), ticketNumProp.getValue(), ticketNumProp.getUpdateDate());
										return createTicket(ticketDate, newTicketNum);
									}
								}
								
								if( newTicketNum <= maxTicketNum ){
									ticketNumProp.setValue(newTicketNum.toString());
									ticketNumProp.setUpdateDate(ticketDate);
									logger.debug("retrieveTicketByTickDateType - Workstore[#0, #1], Workstation[#2], newTicketNum:#3, ticketNumProp:[#4,#5,#6]",
																		workstore.getHospCode(), workstore.getWorkstoreCode(),
																		(workstation == null ? null : workstation.getHostName()),
																		newTicketNum, ticketNumProp.getId(), ticketNumProp.getValue(), ticketNumProp.getUpdateDate());
									return createTicket(ticketDate, newTicketNum);
								}
							}
						}
			}
		}
		return null;		
	}

	@SuppressWarnings("unchecked")
	private List<Ticket> retrieveTicketListByTicketNumDate(Date ticketDate, Integer ticketNum){
		List<Ticket> initTicketList = em.createQuery(
				"select o from Ticket o" + // 20120307 index check : Ticket.workstore,orderType,ticketDate,ticketNum : UI_TICKET_01
				" where o.workstore = :workstore" +
				" and o.orderType = :orderType" +
				" and o.ticketDate = :ticketDate" +
				" and o.ticketNum = :ticketNum")
					.setParameter("workstore", workstore)
					.setParameter("orderType", OrderType.OutPatient)
					.setParameter("ticketNum", ticketNumFormat.format(ticketNum))
					.setParameter("ticketDate", ticketDate, TemporalType.DATE)
					.getResultList();
		
		return initTicketList;
	}
	
	private Ticket createTicket(Date ticketDate, Integer ticketNum){
		Ticket newTicket = new Ticket();
		newTicket.setTicketDate(ticketDate);
		newTicket.setTicketNum(ticketNumFormat.format(ticketNum));
		newTicket.setWorkstore(workstore);
		em.persist(newTicket);
		em.flush();
		
		return newTicket;
	}
	
	private Integer retrieveTicketNumMinWorkstorePropFromDb(TicketType ticketType){
		List<WorkstoreProp> ticketPropList = this.retrieveWorkstoreProp(workstore, INIT_TICKETNUM_LIST);
		Map<String, WorkstoreProp> ticketWorkstorePropMap = new HashMap<String, WorkstoreProp>();
		
		for( WorkstoreProp ticketWkProp : ticketPropList ){
			ticketWorkstorePropMap.put(ticketWkProp.getProp().getName(), ticketWkProp);
		}
		
		Integer pmsMinValue = null;
		Integer pmsMaxValue = null;
		
		Integer dbMinValue = null;
		Integer dbMaxValue = null;
		
		Boolean pmsQueueEnable = null;
		Boolean dbQueueEnable = null;		
		
		if (TicketType.Queue1 == ticketType){
			pmsMinValue = TICKETGEN_QUEUE1_TICKET_TICKETNUM_MIN.get(1);
			pmsMaxValue = TICKETGEN_QUEUE1_TICKET_TICKETNUM_MAX.get(1);
			dbMinValue = ticketWorkstorePropMap.get(TICKETGEN_QUEUE1_TICKET_TICKETNUM_MIN.getName()) == null ? Integer.valueOf(1) : Integer.valueOf(ticketWorkstorePropMap.get(TICKETGEN_QUEUE1_TICKET_TICKETNUM_MIN.getName()).getValue());
			dbMaxValue = ticketWorkstorePropMap.get(TICKETGEN_QUEUE1_TICKET_TICKETNUM_MAX.getName()) == null ? Integer.valueOf(1) : Integer.valueOf(ticketWorkstorePropMap.get(TICKETGEN_QUEUE1_TICKET_TICKETNUM_MAX.getName()).getValue());
			
			if( dbMaxValue <= dbMinValue ){
				return Integer.valueOf(1);
			}else{
				if( ObjectUtils.compare(pmsMinValue, dbMinValue) != 0 || ObjectUtils.compare(pmsMaxValue, dbMaxValue) != 0 ){
					logger.info("WorkstoreProp is different in Ticket Generation|QueueName[#0]|MinValue:PMS[#1],DB[#2]|MaxValue:PMS[#3],DB[#4]|Return DB Value[#5]", ticketType,pmsMinValue,dbMinValue,pmsMaxValue,dbMaxValue,dbMinValue);
				}
				return dbMinValue;
			}
		}else if(TicketType.Queue2 == ticketType){
			pmsQueueEnable = TICKETGEN_QUEUE2_ENABLED.get(false);
			dbQueueEnable = ticketWorkstorePropMap.get(TICKETGEN_QUEUE2_ENABLED.getName()) == null ? Boolean.FALSE : YesNoFlag.Yes.getDataValue().equals(ticketWorkstorePropMap.get(TICKETGEN_QUEUE2_ENABLED.getName()).getValue());
			pmsMinValue = TICKETGEN_QUEUE2_TICKET_TICKETNUM_MIN.get(1);
			pmsMaxValue = TICKETGEN_QUEUE2_TICKET_TICKETNUM_MAX.get(1);
			dbMinValue = ticketWorkstorePropMap.get(TICKETGEN_QUEUE2_TICKET_TICKETNUM_MIN.getName()) == null ? Integer.valueOf(1) : Integer.valueOf(ticketWorkstorePropMap.get(TICKETGEN_QUEUE2_TICKET_TICKETNUM_MIN.getName()).getValue());
			dbMaxValue = ticketWorkstorePropMap.get(TICKETGEN_QUEUE2_TICKET_TICKETNUM_MAX.getName()) == null ? Integer.valueOf(1) : Integer.valueOf(ticketWorkstorePropMap.get(TICKETGEN_QUEUE2_TICKET_TICKETNUM_MAX.getName()).getValue());
			
			if( dbQueueEnable && dbMinValue < dbMaxValue ){
				if( ObjectUtils.compare(pmsMinValue, dbMinValue) != 0 || ObjectUtils.compare(pmsMaxValue, dbMaxValue) != 0 || ObjectUtils.compare(pmsQueueEnable, dbQueueEnable) != 0 ){
					logger.info("WorkstoreProp is different in Ticket Generation|QueueName[#0]|QueueEnabled:PMS[#1],DB[#2]|MinValue:PMS[#3],DB[#4]|MaxValue:PMS[#5],DB[#6]|Return DB Value[#7]", ticketType,pmsQueueEnable,dbQueueEnable,pmsMinValue,dbMinValue,pmsMaxValue,dbMaxValue,dbMinValue);
				}
				return dbMinValue;
			}else{
				if( ObjectUtils.compare(pmsMinValue, dbMinValue) != 0 || ObjectUtils.compare(pmsMaxValue, dbMaxValue) != 0 || ObjectUtils.compare(pmsQueueEnable, dbQueueEnable) != 0 ){
					logger.info("WorkstoreProp is different in Ticket Generation|QueueName[#0]|QueueEnabled:PMS[#1],DB[#2]|MinValue:PMS[#3],DB[#4]|MaxValue:PMS[#5],DB[#6]|Return PMS Value[#7]", ticketType,pmsQueueEnable,dbQueueEnable,pmsMinValue,dbMinValue,pmsMaxValue,dbMaxValue,pmsMinValue);
				}
				return pmsMinValue;
			}
			
		}else{
			pmsQueueEnable = TICKETGEN_FASTQUEUE_ENABLED.get(false);
			dbQueueEnable = ticketWorkstorePropMap.get(TICKETGEN_FASTQUEUE_ENABLED.getName()) == null ? Boolean.FALSE : YesNoFlag.Yes.getDataValue().equals(ticketWorkstorePropMap.get(TICKETGEN_FASTQUEUE_ENABLED.getName()).getValue());
			pmsMinValue = TICKETGEN_FASTQUEUE_TICKET_TICKETNUM_MIN.get(1);
			pmsMaxValue = TICKETGEN_FASTQUEUE_TICKET_TICKETNUM_MAX.get(1);
			dbMinValue = ticketWorkstorePropMap.get(TICKETGEN_FASTQUEUE_TICKET_TICKETNUM_MIN.getName()) == null ? Integer.valueOf(1) : Integer.valueOf(ticketWorkstorePropMap.get(TICKETGEN_FASTQUEUE_TICKET_TICKETNUM_MIN.getName()).getValue());
			dbMaxValue = ticketWorkstorePropMap.get(TICKETGEN_FASTQUEUE_TICKET_TICKETNUM_MAX.getName()) == null ? Integer.valueOf(1) : Integer.valueOf(ticketWorkstorePropMap.get(TICKETGEN_FASTQUEUE_TICKET_TICKETNUM_MAX.getName()).getValue());
			
			if( dbQueueEnable && dbMinValue < dbMaxValue ){
				if( ObjectUtils.compare(pmsMinValue, dbMinValue) != 0 || ObjectUtils.compare(pmsMaxValue, dbMaxValue) != 0 || ObjectUtils.compare(pmsQueueEnable, dbQueueEnable) != 0 ){
					logger.info("WorkstoreProp is different in Ticket Generation|QueueName[#0]|QueueEnabled:PMS[#1],DB[#2]|MinValue:PMS[#3],DB[#4]|MaxValue:PMS[#5],DB[#6]|Return DB Value[#7]", ticketType,pmsQueueEnable,dbQueueEnable,pmsMinValue,dbMinValue,pmsMaxValue,dbMaxValue,dbMinValue);
				}
				return dbMinValue;
			}else{
				if( ObjectUtils.compare(pmsMinValue, dbMinValue) != 0 || ObjectUtils.compare(pmsMaxValue, dbMaxValue) != 0 || ObjectUtils.compare(pmsQueueEnable, dbQueueEnable) != 0 ){
					logger.info("WorkstoreProp is different in Ticket Generation|QueueName[#0]|QueueEnabled:PMS[#1],DB[#2]|MinValue:PMS[#3],DB[#4]|MaxValue:PMS[#5],DB[#6]|Return PMS Value[#7]", ticketType,pmsQueueEnable,dbQueueEnable,pmsMinValue,dbMinValue,pmsMaxValue,dbMaxValue,pmsMinValue);
				}
				return pmsMinValue;
			}
		}
	}
	
	private Integer retrieveMaxTicketNumByTicketType(TicketType ticketType){		
		if (TicketType.Queue1 == ticketType){		
			return TICKETGEN_QUEUE1_TICKET_TICKETNUM_MAX.get(1);				
		}else if(TicketType.Queue2 == ticketType){		
			return TICKETGEN_QUEUE2_TICKET_TICKETNUM_MAX.get(1);
		}else{
			return TICKETGEN_FASTQUEUE_TICKET_TICKETNUM_MAX.get(1);
		}
	}
	
	private Integer retrieveInitTicketNumByTicketType(TicketType ticketType){		
		if (TicketType.Queue1 == ticketType){		
			return TICKETGEN_QUEUE1_TICKET_TICKETNUM_MIN.get(1);		
		}else if(TicketType.Queue2 == ticketType){		
			return TICKETGEN_QUEUE2_TICKET_TICKETNUM_MIN.get(1);
		}else{
			return TICKETGEN_FASTQUEUE_TICKET_TICKETNUM_MIN.get(1);
		}
	}
	
	private boolean isActiveTicketType(TicketType ticketType){
		if (TicketType.Queue1 == ticketType){		
			if( TICKETGEN_QUEUE1_TICKET_TICKETNUM_MAX.get(1) <= TICKETGEN_QUEUE1_TICKET_TICKETNUM_MIN.get(1) ){
				return false;
			}
			
			if( TICKETGEN_QUEUE1_TICKET_TICKETNUM_MAX.get(1) > DEF_MAX_TICKET_NUM || TICKETGEN_QUEUE1_TICKET_TICKETNUM_MIN.get(1) > DEF_MAX_TICKET_NUM ){
				return false;
			}
			return true;
		}else if(TicketType.Queue2 == ticketType){
			if( !TICKETGEN_QUEUE2_ENABLED.get(false) ){
				return false;
			}
			
			if( TICKETGEN_QUEUE2_TICKET_TICKETNUM_MAX.get(1) <= TICKETGEN_QUEUE2_TICKET_TICKETNUM_MIN.get(1) ){
				return false;
			}
			
			if( TICKETGEN_QUEUE2_TICKET_TICKETNUM_MAX.get(1) > DEF_MAX_TICKET_NUM || TICKETGEN_QUEUE2_TICKET_TICKETNUM_MIN.get(1) > DEF_MAX_TICKET_NUM ){
				return false;
			}
			return true;
		}else{
			if( !TICKETGEN_FASTQUEUE_ENABLED.get(false) ){
				return false;
			}
			
			if( TICKETGEN_FASTQUEUE_TICKET_TICKETNUM_MAX.get(1) <= TICKETGEN_FASTQUEUE_TICKET_TICKETNUM_MIN.get(1) ){
				return false;
			}
			
			if( TICKETGEN_FASTQUEUE_TICKET_TICKETNUM_MAX.get(1) > DEF_MAX_TICKET_NUM || TICKETGEN_FASTQUEUE_TICKET_TICKETNUM_MIN.get(1) > DEF_MAX_TICKET_NUM ){
				return false;
			}
			return true;
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<WorkstoreProp> retrieveWorkstoreProp(Workstore workstoreProp, List<String> workstorePropNameList){
		List<WorkstoreProp> workstorePropList = em.createQuery(
												"select o from WorkstoreProp o" + // 20171023 index check : WorkstoreProp.workstore,prop : UI_WORKSTORE_PROP_01
												" where o.workstore = :workstore" +
												" and o.prop.name in :propNameList")
													.setParameter("workstore", workstore)
													.setParameter("propNameList", workstorePropNameList)
													.getResultList();
					
		return workstorePropList;
	}
	
	@SuppressWarnings("unchecked")
	private List<Long> lockWorkstoreProp(Workstore workstore, String... propNames) {
		
		if (propNames.length == 0) {
			throw new IllegalArgumentException("lockWorkstoreProp missing propName(s)!");
		}
		
		// need to use native query in order to only lock the workstore_prop table
		Query query = em.createNativeQuery(
			"select o.id from workstore_prop o" + // 20130129 index check : WorkstoreProp.workstore,prop : UI_WORKSTORE_PROP_01
			" where o.hosp_code = ?1" +
			" and o.workstore_code = ?2" +
			" and o.prop_id in (select p.id from prop p where p.name in ('" + StringUtils.join(propNames, "','")  + "'))" +
			" for update");
			query.setParameter(1, workstore.getHospCode());
			query.setParameter(2, workstore.getWorkstoreCode());
		return query.getResultList();		
	}
	
	@SuppressWarnings("unchecked")
	private List<Long> lockHospitalProp(Hospital hospital, String... propNames) {
		
		if (propNames.length == 0) {
			throw new IllegalArgumentException("lockHospitalProp missing propName(s)!");
		}
		
		// need to use native query in order to only lock the hospital_prop table
		Query query = em.createNativeQuery(
			"select o.id from hospital_prop o" + // 20130129 index check : HospitalProp.hospital,prop : UI_HOSPITAL_PROP_01
			" where o.hosp_code = ?1" +
			" and o.prop_id in (select p.id from prop p where p.name in ('" + StringUtils.join(propNames, "','")  + "'))" +
			" for update");
			query.setParameter(1, hospital.getHospCode());
		return query.getResultList();
	}
	
	
	@SuppressWarnings("unchecked")
	private List<Long> lockCorporateProp(String... propNames) {
		
		if (propNames.length == 0) {
			throw new IllegalArgumentException("lockCorporateProp missing propName(s)!");
		}
		
		// need to use native query in order to only lock the corporate_prop table
		Query query = em.createNativeQuery(
			"select o.id from corporate_prop o" + // 20140317 index check : CorporateProp.prop : UI_CORPORATE_PROP_01
			" where o.prop_id in (select p.id from prop p where p.name in ('" + StringUtils.join(propNames, "','")  + "'))" +
			" for update");
		return query.getResultList();
	}
}