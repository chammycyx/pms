package hk.org.ha.model.pms.vo.report;

import hk.org.ha.model.pms.udt.vetting.ActionStatus;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class MpTrxRptPivasItemDtl implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String deliveryItemId;

	private String itemType;
	
	private ActionStatus actionStatus;
	
	private String newRefillInfo;
	
	private String pivasDesc;
	
	private String prepExpiryDate;
	
	private String finalVolume;
	
	private String prepQty;
	
	private String containerName;
	
	private String firstDoseDateTime;
	
	private String issueQty;
	
	private String adjQty;
	
	private Boolean deletedFlag;

	public MpTrxRptPivasItemDtl(){;
		deletedFlag = Boolean.FALSE;
	}
	
	public String getDeliveryItemId() {
		return deliveryItemId;
	}

	public void setDeliveryItemId(String deliveryItemId) {
		this.deliveryItemId = deliveryItemId;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getNewRefillInfo() {
		return newRefillInfo;
	}

	public void setNewRefillInfo(String newRefillInfo) {
		this.newRefillInfo = newRefillInfo;
	}

	public ActionStatus getActionStatus() {
		return actionStatus;
	}

	public void setActionStatus(ActionStatus actionStatus) {
		this.actionStatus = actionStatus;
	}

	public String getPivasDesc() {
		return pivasDesc;
	}

	public void setPivasDesc(String pivasDesc) {
		this.pivasDesc = pivasDesc;
	}

	public String getPrepExpiryDate() {
		return prepExpiryDate;
	}

	public void setPrepExpiryDate(String prepExpiryDate) {
		this.prepExpiryDate = prepExpiryDate;
	}

	public String getFinalVolume() {
		return finalVolume;
	}

	public void setFinalVolume(String finalVolume) {
		this.finalVolume = finalVolume;
	}

	public String getPrepQty() {
		return prepQty;
	}

	public void setPrepQty(String prepQty) {
		this.prepQty = prepQty;
	}

	public String getContainerName() {
		return containerName;
	}

	public void setContainerName(String containerName) {
		this.containerName = containerName;
	}
	
	public String getFirstDoseDateTime() {
		return firstDoseDateTime;
	}

	public void setFirstDoseDateTime(String firstDoseDateTime) {
		this.firstDoseDateTime = firstDoseDateTime;
	}

	public String getIssueQty() {
		return issueQty;
	}

	public void setIssueQty(String issueQty) {
		this.issueQty = issueQty;
	}

	public String getAdjQty() {
		return adjQty;
	}

	public void setAdjQty(String adjQty) {
		this.adjQty = adjQty;
	}

	public Boolean getDeletedFlag() {
		return deletedFlag;
	}

	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

}