package hk.org.ha.model.pms.vo.checkissue.mp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class LabelDelete implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Date trxDate;
	
	private String caseNum;
	
	private String batchNum;
	
	private Long trxId;
	
	private String mpDispLabelQrCodeXml;
	
	private List<LabelDeleteItem> labelDeleteItemList;

	public Date getTrxDate() {
		return trxDate;
	}

	public void setTrxDate(Date trxDate) {
		this.trxDate = trxDate;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public Long getTrxId() {
		return trxId;
	}

	public void setTrxId(Long trxId) {
		this.trxId = trxId;
	}

	public String getMpDispLabelQrCodeXml() {
		return mpDispLabelQrCodeXml;
	}

	public void setMpDispLabelQrCodeXml(String mpDispLabelQrCodeXml) {
		this.mpDispLabelQrCodeXml = mpDispLabelQrCodeXml;
	}

	public List<LabelDeleteItem> getLabelDeleteItemList() {
		if ( labelDeleteItemList == null ) {
			labelDeleteItemList = new ArrayList<LabelDeleteItem>();
		}
		return labelDeleteItemList;
	}

	public void setLabelDeleteItemList(List<LabelDeleteItem> labelDeleteItemList) {
		this.labelDeleteItemList = labelDeleteItemList;
	}
	
}