package hk.org.ha.model.pms.biz.onestop;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.order.DispOrderManagerLocal;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;

import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("prescOrderListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PrescOrderListServiceBean implements PrescOrderListServiceLocal {
	
	@Logger
	private Log logger;
	
	private List<MedOrderItem> oneStopMedOrderItemList;
	
	@In
	private DispOrderManagerLocal dispOrderManager;
	
	public void retrievePrescItemList(Date ticketDate, String ticketNum, String workstoreCode) {
		oneStopMedOrderItemList = dispOrderManager.retrievePrescItemListForOneStop(ticketDate, ticketNum, workstoreCode);
		if (oneStopMedOrderItemList == null) {
			logger.debug("Error! Unable to find order");
		}
	}
	
	public List<MedOrderItem> getOneStopMedOrderItemList()
	{
		return oneStopMedOrderItemList;
	}

	@Remove
	public void destroy(){
		if (oneStopMedOrderItemList != null) {
			oneStopMedOrderItemList = null;
		}
	}
}