package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class CddhPatDrugProfileRptDtl implements Serializable {

	private static final long serialVersionUID = 1L;

	private boolean legacyMultDose;

	private String dispOrderId;
	
	private String dispHospCode;
	
	private String itemNum;
	
	private String issueDate;

	private Date dispDate;
	
	private String itemDesc;
	
	private String duration;
	
	private String durationUnit;
	
	private String issueQty;
	
	private String hospCode;
	
	private String cddhSpecGroupCode;
	
	private String instructionText;
	
	private String actionStatus;
	
	private String remark;

	public Date getDispDate() {
		return dispDate;
	}

	public void setDispDate(Date dispDate) {
		this.dispDate = dispDate;
	}

	public String getItemDesc() {
		return itemDesc;
	}

	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getDurationUnit() {
		return durationUnit;
	}

	public void setDurationUnit(String durationUnit) {
		this.durationUnit = durationUnit;
	}

	public String getIssueQty() {
		return issueQty;
	}

	public void setIssueQty(String issueQty) {
		this.issueQty = issueQty;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getCddhSpecGroupCode() {
		return cddhSpecGroupCode;
	}

	public void setCddhSpecGroupCode(String cddhSpecGroupCode) {
		this.cddhSpecGroupCode = cddhSpecGroupCode;
	}

	public String getInstructionText() {
		return instructionText;
	}

	public void setInstructionText(String instructionText) {
		this.instructionText = instructionText;
	}

	public String getActionStatus() {
		return actionStatus;
	}

	public void setActionStatus(String actionStatus) {
		this.actionStatus = actionStatus;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public String getDispOrderId() {
		return dispOrderId;
	}

	public void setDispOrderId(String dispOrderId) {
		this.dispOrderId = dispOrderId;
	}

	public String getDispHospCode() {
		return dispHospCode;
	}

	public void setDispHospCode(String dispHospCode) {
		this.dispHospCode = dispHospCode;
	}

	public String getItemNum() {
		return itemNum;
	}

	public void setItemNum(String itemNum) {
		this.itemNum = itemNum;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}
	
	public boolean isLegacyMultDose() {
		return legacyMultDose;
	}

	public void setLegacyMultDose(boolean legacyMultDose) {
		this.legacyMultDose = legacyMultDose;
	}
}
