package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.reftable.PendReasonManagerLocal;
import hk.org.ha.model.pms.persistence.reftable.PendingReason;
import java.util.Collection;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("pendReasonService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PendReasonServiceBean implements PendReasonServiceLocal {
	
	@Out	
	private List<PendingReason> pendReasonList;
	
	@In
	private PendReasonManagerLocal pendReasonManager;
	
	public void retrievePendingReasonList() {
		pendReasonList = pendReasonManager.retrievePendingReasonList();
	}
	
	public void updatePendingReasonList(Collection<PendingReason> newPendingReasonList) {
		pendReasonManager.updatePendingReasonList(newPendingReasonList);
	}

	@Remove
	public void destroy() {
		if (pendReasonList != null) {
			pendReasonList = null;
		}
	}
}
