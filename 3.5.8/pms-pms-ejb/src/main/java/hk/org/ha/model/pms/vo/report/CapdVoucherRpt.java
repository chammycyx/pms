package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class CapdVoucherRpt implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer issueVoucherCount;
	
	private Integer issueItemCount;
	
	private Integer outstandVoucherCount;
	
	private Integer outstandItemCount;
	
	private Integer suspendVoucherCount;
	
	private Integer suspendItemCount;
	
	private Integer deleteVoucherCount;
	
	private Integer deleteItemCount;
	
	private List<CapdVoucherRptDtl> capdVoucherRptDtlList;

	public List<CapdVoucherRptDtl> getCapdVoucherRptDtlList() {
		return capdVoucherRptDtlList;
	}

	public void setCapdVoucherRptDtlList(
			List<CapdVoucherRptDtl> capdVoucherRptDtlList) {
		this.capdVoucherRptDtlList = capdVoucherRptDtlList;
	}

	public Integer getIssueVoucherCount() {
		return issueVoucherCount;
	}

	public void setIssueVoucherCount(Integer issueVoucherCount) {
		this.issueVoucherCount = issueVoucherCount;
	}

	public Integer getIssueItemCount() {
		return issueItemCount;
	}

	public void setIssueItemCount(Integer issueItemCount) {
		this.issueItemCount = issueItemCount;
	}

	public Integer getOutstandVoucherCount() {
		return outstandVoucherCount;
	}

	public void setOutstandVoucherCount(Integer outstandVoucherCount) {
		this.outstandVoucherCount = outstandVoucherCount;
	}


	public Integer getSuspendVoucherCount() {
		return suspendVoucherCount;
	}

	public void setSuspendVoucherCount(Integer suspendVoucherCount) {
		this.suspendVoucherCount = suspendVoucherCount;
	}

	public Integer getSuspendItemCount() {
		return suspendItemCount;
	}

	public void setSuspendItemCount(Integer suspendItemCount) {
		this.suspendItemCount = suspendItemCount;
	}

	public Integer getDeleteVoucherCount() {
		return deleteVoucherCount;
	}

	public void setDeleteVoucherCount(Integer deleteVoucherCount) {
		this.deleteVoucherCount = deleteVoucherCount;
	}

	public Integer getDeleteItemCount() {
		return deleteItemCount;
	}

	public void setDeleteItemCount(Integer deleteItemCount) {
		this.deleteItemCount = deleteItemCount;
	}

	public void setOutstandItemCount(Integer outstandItemCount) {
		this.outstandItemCount = outstandItemCount;
	}

	public Integer getOutstandItemCount() {
		return outstandItemCount;
	}
	

}
