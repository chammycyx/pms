package hk.org.ha.model.pms.biz.onestop;

import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.onestop.MoStatusCount;

import javax.ejb.Local;

import org.jboss.seam.annotations.async.Asynchronous;
import org.joda.time.DateTime;

@Local
public interface OneStopCountManagerLocal {
	
	@Asynchronous
	void retrieveUnvetAndSuspendCount(Workstore workstore, int suspendCountDurationLimit);
	
	DateTime getDefaultSuspendOrderBeginDate(int suspendCountDurationLimit);
	
	Boolean isCounterTimeOut(MoStatusCount moStatusCount);
}
