package hk.org.ha.model.pms.persistence.corp;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class HospitalClusterTaskPK implements Serializable {

	private static final long serialVersionUID = 1L;

	private String clusterCode;
	private String taskName;
	
	public HospitalClusterTaskPK() {
	}
	
	public HospitalClusterTaskPK(String clusterCode, String taskName) {
		this.clusterCode = clusterCode;
		this.taskName = taskName;
	}
	
	public String getClusterCode() {
		return clusterCode;
	}

	public void setClusterCode(String clusterCode) {
		this.clusterCode = clusterCode;
	}
	
	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	
	public boolean equals(Object other) {
		return EqualsBuilder.reflectionEquals(this, other);
	}
	
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
}
