package hk.org.ha.model.pms.persistence.disp;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.disp.DiscontinueStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderDayEndStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.refill.DocType;
import hk.org.ha.model.pms.udt.refill.RefillScheduleRefillStatus;
import hk.org.ha.model.pms.udt.refill.RefillScheduleStatus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.PrivateOwned;

@Entity
@Table(name = "REFILL_SCHEDULE")
public class RefillSchedule extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "refillScheduleSeq")
	@SequenceGenerator(name = "refillScheduleSeq", sequenceName = "SQ_REFILL_SCHEDULE_ID", initialValue = 100000000)
	private Long id;
	
	@Column(name = "REFILL_COUPON_NUM", nullable = false, length = 19)
	private String refillCouponNum;
	
	@Column(name = "TRIM_REFILL_COUPON_NUM", nullable = false, length = 17)
	private String trimRefillCouponNum;
	
	@Column(name = "GROUP_NUM", nullable = false)
	private Integer groupNum;
	
	@Column(name = "REFILL_NUM", nullable = false)
	private Integer refillNum;
	
	@Column(name = "NUM_OF_REFILL", nullable = false)
	private Integer numOfRefill;
	
	@Column(name = "REFILL_DATE", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date refillDate;
	
	@Converter(name = "RefillSchedule.docType", converterClass = DocType.Converter.class)
    @Convert("RefillSchedule.docType")
	@Column(name="DOC_TYPE", nullable = false, length = 1)
	private DocType docType;
	
	@Converter(name = "RefillSchedule.refillStatus", converterClass = RefillScheduleRefillStatus.Converter.class)
	@Convert("RefillSchedule.refillStatus")
	@Column(name="REFILL_STATUS", nullable = false, length = 1)
	private RefillScheduleRefillStatus refillStatus;

	@Converter(name = "RefillSchedule.status", converterClass = RefillScheduleStatus.Converter.class)
    @Convert("RefillSchedule.status")
	@Column(name="STATUS", nullable = false, length = 1)
	private RefillScheduleStatus status;

	@Column(name = "NEXT_PHARM_APPT_DATE")
	@Temporal(TemporalType.DATE)
	private Date nextPharmApptDate;
		
	@Column(name = "BEFORE_ADJ_REFILL_DATE")
	@Temporal(TemporalType.DATE)
	private Date beforeAdjRefillDate;

	@Column(name = "NUM_OF_COUPON")
	private Integer numOfCoupon;
	
	@Column(name= "PRINT_FLAG", nullable = false)
	private Boolean printFlag;
	
	@Converter(name = "RefillSchedule.printLang", converterClass = PrintLang.Converter.class)
	@Convert("RefillSchedule.printLang")
	@Column(name="PRINT_LANG", nullable = false, length = 10)
	private PrintLang printLang;
		
	@Column(name= "PRINT_REMINDER_FLAG", nullable = false)
	private Boolean printReminderFlag;
	
	@Column(name = "REFILL_INTERVAL")
	private Integer refillInterval;
	
	@Column(name = "REFILL_INTERVAL_LAST")
	private Integer refillIntervalLast;
	
	@Column(name = "RE_DISP_FLAG", nullable = false)
	private Boolean reDispFlag;
	
	@Column(name = "MO_ORG_ITEM_NUM")
	@XmlElement
	private Integer moOrgItemNum;
	
    @PrivateOwned
    @OrderBy("itemNum")
    @OneToMany(mappedBy="refillSchedule", cascade=CascadeType.ALL)
	private List<RefillScheduleItem> refillScheduleItemList;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "PHARM_ORDER_ID", nullable = false)
    private PharmOrder pharmOrder;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "TICKET_ID")
	private Ticket ticket;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "DISP_ORDER_ID")
	private DispOrder dispOrder;
	
	@Column(name = "USE_MO_ITEM_NUM_FLAG", nullable = false)
	private Boolean useMoItemNumFlag;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE",      referencedColumnName = "HOSP_CODE",      nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;
	
	@Transient
	private DispOrderStatus dispOrderStatus;
	
	@Transient
	private DispOrderAdminStatus dispOrderAdminStatus;
	
	@Transient
	private DispOrderDayEndStatus dispOrderDayEndStatus;
	
	@Transient
	private Boolean dispOrderBatchProcessingFlag;
	
	@Transient
	private Boolean allItemDiscontinueFlag;
		
	public RefillSchedule() {
		printLang = PrintLang.Eng;
		numOfRefill = Integer.valueOf(0);
		status = RefillScheduleStatus.NotYetDispensed;
		refillStatus = RefillScheduleRefillStatus.None;
		numOfCoupon = Integer.valueOf(1);
		printFlag = Boolean.FALSE;
		printReminderFlag = Boolean.FALSE;
		reDispFlag = Boolean.FALSE;
		useMoItemNumFlag = Boolean.TRUE;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRefillCouponNum() {
		return refillCouponNum;
	}

	public void setRefillCouponNum(String refillCouponNum) {
		this.refillCouponNum = refillCouponNum;
	}

	public Integer getGroupNum() {
		return groupNum;
	}

	public void setGroupNum(Integer groupNum) {
		this.groupNum = groupNum;
	}

	public Integer getRefillNum() {
		return refillNum;
	}

	public void setRefillNum(Integer refillNum) {
		this.refillNum = refillNum;
	}

	public Integer getNumOfRefill() {
		return numOfRefill;
	}

	public void setNumOfRefill(Integer numOfRefill) {
		this.numOfRefill = numOfRefill;
	}

	public Date getRefillDate() {
		if (refillDate == null) {
			return null;
		}
		else {
			return new Date(refillDate.getTime());
		}
	}

	public void setRefillDate(Date refillDate) {
		if (refillDate == null) {
			this.refillDate = null;
		}
		else {
			this.refillDate = new Date(refillDate.getTime());
		}
	}

	public DocType getDocType() {
		return docType;
	}

	public void setDocType(DocType docType) {
		this.docType = docType;
	}

	public RefillScheduleRefillStatus getRefillStatus() {
		return refillStatus;
	}

	public void setRefillStatus(RefillScheduleRefillStatus refillStatus) {
		this.refillStatus = refillStatus;
	}

	public RefillScheduleStatus getStatus() {
		return status;
	}

	public void setStatus(RefillScheduleStatus status) {
		this.status = status;
	}

	public Date getNextPharmApptDate() {
		if (nextPharmApptDate == null) {
			return null;
		}
		else {
			return new Date(nextPharmApptDate.getTime());
		}
	}

	public void setNextPharmApptDate(Date nextPharmApptDate) {
		if (nextPharmApptDate == null) {
			this.nextPharmApptDate = null;
		}
		else {
			this.nextPharmApptDate = new Date(nextPharmApptDate.getTime());
		}
	}

	public Date getBeforeAdjRefillDate() {
		if (beforeAdjRefillDate == null) {
			return null;
		}
		else {
			return new Date(beforeAdjRefillDate.getTime());
		}
	}

	public void setBeforeAdjRefillDate(Date beforeAdjRefillDate) {
		if (beforeAdjRefillDate == null) {
			this.beforeAdjRefillDate = null;
		}
		else {
			this.beforeAdjRefillDate = new Date(beforeAdjRefillDate.getTime());
		}
	}

	public List<RefillScheduleItem> getRefillScheduleItemList() {
		if (refillScheduleItemList == null) {
			refillScheduleItemList = new ArrayList<RefillScheduleItem>();
		}
		return refillScheduleItemList;
	}
	
	public void setRefillScheduleItemList(List<RefillScheduleItem> refillScheduleItemList) {
		this.refillScheduleItemList = refillScheduleItemList;
	}
	
	public void addRefillScheduleItem(RefillScheduleItem refillScheduleItem) {
		refillScheduleItem.setRefillSchedule(this);
		getRefillScheduleItemList().add(refillScheduleItem);
	}
	
	public void clearRefillScheduleItemList() {
		refillScheduleItemList = new ArrayList<RefillScheduleItem>();
	}
	
	public PharmOrder getPharmOrder() {
		return pharmOrder;
	}

	public void setPharmOrder(PharmOrder pharmOrder) {
		this.pharmOrder = pharmOrder;
	}

	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public DispOrder getDispOrder() {
		return dispOrder;
	}

	public void setDispOrder(DispOrder dispOrder) {
		this.dispOrder = dispOrder;
	}
	
	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}
		
	public Integer getRefillInterval() {
		return refillInterval;
	}

	public void setRefillInterval(Integer refillInterval) {
		this.refillInterval = refillInterval;
	}

	public Integer getRefillIntervalLast() {
		return refillIntervalLast;
	}

	public void setRefillIntervalLast(Integer refillIntervalLast) {
		this.refillIntervalLast = refillIntervalLast;
	}

	public DispOrderStatus getDispOrderStatus() {
		return dispOrderStatus;
	}
	
	public void setDispOrderStatus(DispOrderStatus dispOrderStatus) {
		this.dispOrderStatus = dispOrderStatus;
	}
		
	public void setDispOrderAdminStatus(DispOrderAdminStatus dispOrderAdminStatus) {
		this.dispOrderAdminStatus = dispOrderAdminStatus;
	}

	public DispOrderAdminStatus getDispOrderAdminStatus() {
		return dispOrderAdminStatus;
	}
		
	public String getTrimRefillCouponNum() {
		return trimRefillCouponNum;
	}

	public void setTrimRefillCouponNum(String trimRefillCouponNum) {
		this.trimRefillCouponNum = trimRefillCouponNum;
	}

	@Transient
	private transient boolean childLoaded = false;
	
	public void loadChild() {
		
		if (getId() == null) return;

		if (!childLoaded) {			
			childLoaded = true;

			this.getTicket();
			this.getWorkstore();
	
			if (this.getPharmOrder() != null) {
				this.getPharmOrder().loadChild();
				this.getPharmOrder().addRefillSchedule(this);
			}
	
			if (this.getDispOrder() != null) {
				this.getDispOrder().loadChild();
				this.getDispOrder().addRefillSchedule(this);
			}
	
			for (RefillScheduleItem item : this.getRefillScheduleItemList()) {
				item.getRefillSchedule();
				item.getWorkstore();
				if (item.getPharmOrderItem() != null) {
					item.getPharmOrderItem().addRefillScheduleItem(item);
				}
			}
		}
	}

	public void clearId() {
		this.setId(null);
		for (RefillScheduleItem item : this.getRefillScheduleItemList()) {
			item.setId(null);
		}		
	}
	
	public void disconnect() {
		
		if (this.getDispOrder() != null) 
		{
			this.setDispOrderStatus(this.getDispOrder().getStatus());
			this.setDispOrderAdminStatus(this.getDispOrder().getAdminStatus());
			this.setDispOrderDayEndStatus(this.getDispOrder().getDayEndStatus());
			this.setDispOrderBatchProcessingFlag(this.getDispOrder().getBatchProcessingFlag());
			this.getDispOrder().getRefillScheduleList().remove(this);
			this.setDispOrder(null);
		}
		
		if (this.getTicket() != null) 
		{
			Workstore ws = this.getTicket().getWorkstore();
			ws.setCddhWorkstoreGroup(null);
			ws.setWorkstoreGroup(null);
		}

		if (this.getPharmOrder() != null) 
		{
			this.getPharmOrder().getRefillScheduleList().remove(this);
			this.setPharmOrder(null);
		}

		this.setAllItemDiscontinueFlag(Boolean.TRUE);
		for (RefillScheduleItem rsi : this.getRefillScheduleItemList()) 
		{
			if (rsi.getPharmOrderItem() != null) {
				if( rsi.getPharmOrderItem().getMedOrderItem().getDiscontinueStatus() == null || rsi.getPharmOrderItem().getMedOrderItem().getDiscontinueStatus() == DiscontinueStatus.None){
					this.setAllItemDiscontinueFlag(Boolean.FALSE);
				}				
				rsi.getPharmOrderItem().getRefillScheduleItemList().remove(rsi);
				rsi.setPharmOrderItem(null);
			}
		}
	}
	
	public void connect(DispOrder dispOrder) {
		
		connect(dispOrder.getPharmOrder());
		
		dispOrder.addRefillSchedule(this);
	}
	
	public void connect(PharmOrder pharmOrder) {
		
		for (RefillScheduleItem refillScheduleItem : this.getRefillScheduleItemList()) 
		{
			PharmOrderItem pharmOrderItem = pharmOrder.getPharmOrderItemByOrgItemNum(
					refillScheduleItem.getItemNum());
			
			if (pharmOrderItem != null) {
				pharmOrderItem.addRefillScheduleItem(refillScheduleItem);
			}
		}		
		
		pharmOrder.addRefillSchedule(this);
	}

	public void setNumOfCoupon(Integer numOfCoupon) {
		this.numOfCoupon = numOfCoupon;
	}

	public Integer getNumOfCoupon() {
		return numOfCoupon;
	}

	public void setPrintFlag(Boolean printFlag) {
		this.printFlag = printFlag;
	}

	public Boolean getPrintFlag() {
		return printFlag;
	}
	
	public void setDispOrderDayEndStatus(DispOrderDayEndStatus dispOrderDayEndStatus) {
		this.dispOrderDayEndStatus = dispOrderDayEndStatus;
	}

	public DispOrderDayEndStatus getDispOrderDayEndStatus() {
		return dispOrderDayEndStatus;
	}

	public void setPrintLang(PrintLang printLang) {
		this.printLang = printLang;
	}

	public PrintLang getPrintLang() {
		return printLang;
	}

	public void setPrintReminderFlag(Boolean printReminderFlag) {
		this.printReminderFlag = printReminderFlag;
	}

	public Boolean getPrintReminderFlag() {
		return printReminderFlag;
	}
	
	public List<MedOrderItem> getMedOrderItemList() {
		List<MedOrderItem> moiList = new ArrayList<MedOrderItem>();
		for (RefillScheduleItem rsi : getRefillScheduleItemList()) {
			MedOrderItem moi = rsi.getPharmOrderItem().getMedOrderItem();
			if (!moiList.contains(moi)) {
				moiList.add(moi);
			}
		}
		return moiList;
	}

	public void setDispOrderBatchProcessingFlag(
			Boolean dispOrderBatchProcessingFlag) {
		this.dispOrderBatchProcessingFlag = dispOrderBatchProcessingFlag;
	}

	public Boolean getDispOrderBatchProcessingFlag() {
		return dispOrderBatchProcessingFlag;
	}

	public void setReDispFlag(Boolean reDispFlag) {
		this.reDispFlag = reDispFlag;
	}

	public Boolean getReDispFlag() {
		return reDispFlag;
	}

	public Integer getMoOrgItemNum() {
		return moOrgItemNum;
	}

	public void setMoOrgItemNum(Integer moOrgItemNum) {
		this.moOrgItemNum = moOrgItemNum;
	}

	public boolean isDiscontinue() {
		return !getDiscontinueItem().isEmpty();
	}
	
	public List<MedOrderItem> getDiscontinueItem() {
		List<MedOrderItem> moiList = new ArrayList<MedOrderItem>();
		for (RefillScheduleItem rsi : getRefillScheduleItemList()) {
			MedOrderItem moi = rsi.getPharmOrderItem().getMedOrderItem();
			if (moi.getDiscontinueStatus() == DiscontinueStatus.Confirmed || moi.getDiscontinueStatus() == DiscontinueStatus.Pending) {
				moiList.add(moi);
			}
		}
		return moiList;
	}

	public void setAllItemDiscontinueFlag(Boolean allItemDiscontinueFlag) {
		this.allItemDiscontinueFlag = allItemDiscontinueFlag;
	}

	public Boolean getAllItemDiscontinueFlag() {
		return allItemDiscontinueFlag;
	}

	public void setUseMoItemNumFlag(Boolean useMoItemNumFlag) {
		this.useMoItemNumFlag = useMoItemNumFlag;
	}
	
	public Boolean getUseMoItemNumFlag() {
		return useMoItemNumFlag;
	}
}
