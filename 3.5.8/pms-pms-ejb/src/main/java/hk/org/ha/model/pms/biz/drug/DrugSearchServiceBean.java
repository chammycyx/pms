package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.vetting.CapdListManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.corp.cache.DmFormCacher;
import hk.org.ha.model.pms.corp.cache.DmRegimenCacher;
import hk.org.ha.model.pms.corp.cache.DmRouteCacher;
import hk.org.ha.model.pms.corp.cache.DmSiteCacher;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmRegimen;
import hk.org.ha.model.pms.dms.persistence.DmSite;
import hk.org.ha.model.pms.dms.vo.Bnf;
import hk.org.ha.model.pms.dms.vo.BnfCriteria;
import hk.org.ha.model.pms.dms.vo.BnfDrugInfo;
import hk.org.ha.model.pms.dms.vo.Capd;
import hk.org.ha.model.pms.dms.vo.CommonOrder;
import hk.org.ha.model.pms.dms.vo.DrugCommonDosage;
import hk.org.ha.model.pms.dms.vo.DrugName;
import hk.org.ha.model.pms.dms.vo.PivasFormula;
import hk.org.ha.model.pms.dms.vo.PreparationProperty;
import hk.org.ha.model.pms.dms.vo.PreparationSearchCriteria;
import hk.org.ha.model.pms.dms.vo.RelatedDrugSearchCriteria;
import hk.org.ha.model.pms.dms.vo.RouteForm;
import hk.org.ha.model.pms.dms.vo.RouteFormSearchCriteria;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.udt.drug.AgeRange;
import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.udt.vetting.RegimenDurationUnit;
import hk.org.ha.model.pms.vo.drug.DrugSearchReturn;
import hk.org.ha.model.pms.vo.rx.Regimen;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.rx.Site;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("drugSearchService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DrugSearchServiceBean implements DrugSearchServiceLocal {
	
	private static final String SPACE = " ";
	
	private static final char[] DELIMITER = new char[] {' ' , '~' , '&' , '-' , '[' , '(' , '<', '+', '.'}; 
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

	@In
	private DrugSearchOpManagerLocal drugSearchOpManager;
	
	@In
	private DrugSearchMpManagerLocal drugSearchMpManager;

	@In
	private CapdListManagerLocal capdListManager;
	
	@In
	private Workstore workstore;
	
	@In(required = false)
	private PharmOrder pharmOrder;
	
	@In(required = false)
	private MedProfile medProfile;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private List<Integer> atdpsPickStationsNumList;
	
	@Out(required = false)
	private List<Map<String, Object>> drugSearchDrugPriceList;

	@Out(required = false)
	private List<String> drugSearchCapdSupplierSystemList;

	@Out(required = false)
	private List<String> drugSearchCapdCalciumStrengthList;

	@Out(required = false)
	private List<Capd> drugSearchCapdConcentrationList;
	
	@Out(required = false)
	private MedOrderItem drugSearchMedOrderItem;
	

	public DrugSearchReturn drugSearch(DrugSearchSource drugSearchSource, RelatedDrugSearchCriteria relatedDrugSearchCriteria, RouteFormSearchCriteria routeFormSearchCriteria) {
		String itemCodeByItemCodeSearch = null; 
		
		relatedDrugSearchCriteria.setDispHospCode(workstore.getHospCode());
		relatedDrugSearchCriteria.setDispWorkstore(workstore.getWorkstoreCode());
		
		// change to item code search if drug name search input is item code
		if (relatedDrugSearchCriteria.getFirstDisplayname() != null) {
			// if search type is basic or any word 
			String searchKey = relatedDrugSearchCriteria.getFirstDisplayname();
			if (searchKey.length() >= 6 && searchKey.length() <= 10) {
				DmDrug dmDrug = dmDrugCacher.getDmDrug(searchKey);
				if (dmDrug != null) {
					relatedDrugSearchCriteria.setFirstDisplayname(null);
					relatedDrugSearchCriteria.setContainSearch(Boolean.FALSE);
					relatedDrugSearchCriteria.setItemCode(searchKey);
					
					routeFormSearchCriteria.setItemCode(searchKey);

					itemCodeByItemCodeSearch = searchKey;	// sync item code search state to front-end
				}
			}
		}
		
		List<DrugName> drugNameList = dmsPmsServiceProxy.retrieveRelatedDrug(relatedDrugSearchCriteria);

		List<Map<String, Object>> drugSearchNameMapList = new ArrayList<Map<String, Object>>();
		
		if (drugNameList.size() == 1) {
			DrugName drugName = drugNameList.get(0);
			
			routeFormSearchCriteria.setFirstDisplayname(drugName.getFirstDisplayname());
			routeFormSearchCriteria.setSecondDisplayname(drugName.getSecondDisplayname());
			routeFormSearchCriteria.setTrueDisplayname(drugName.getTrueDisplayname());
			routeFormSearchCriteria.setNameType(drugName.getNameType());
			routeFormSearchCriteria.setPmsFmStatus(drugName.getPmsFmStatus());
			routeFormSearchCriteria.setConcatIngredients(drugName.getConcatIngredients());
			
			Map<String, Object> drugNameMap = new HashMap<String, Object>();
			drugNameMap.put("treeNode", getDrugDisplayname(drugName.getFirstDisplayname(), drugName.getSecondDisplayname(), drugName.getConcatIngredients()));
			drugNameMap.put("drugName", drugName);
			if ( drugSearchSource == DrugSearchSource.MpVetting || ! "PDF".equals(drugName.getTheraGroup()) ) {
				// do not retrieve drug detail for OP CAPD drug
				drugNameMap.put("children", getDrugDetail(drugSearchSource, routeFormSearchCriteria, drugName.getTheraGroup()));
			}
			drugNameMap.put("childrenLoaded", true);
			drugSearchNameMapList.add(drugNameMap);
			
		} else {
			for (DrugName drugName : drugNameList) {
				Map<String, Object> displaynameMap = new HashMap<String, Object>();
				displaynameMap.put("treeNode", getDrugDisplayname(drugName.getFirstDisplayname(), drugName.getSecondDisplayname(), drugName.getConcatIngredients()));
				displaynameMap.put("drugName", drugName);
				if ( drugSearchSource == DrugSearchSource.MpVetting || ! "PDF".equals(drugName.getTheraGroup()) ) { 
					displaynameMap.put("children", new ArrayList<Map<String, Object>>());
					displaynameMap.put("childrenLoaded", false);
				} else {
					// do not allow user to expand drugName for OP CAPD drug
					displaynameMap.put("childrenLoaded", true);
				}
				drugSearchNameMapList.add(displaynameMap);
			}
		}
		
		DrugSearchReturn drugSearchReturn = new DrugSearchReturn();
		drugSearchReturn.setDrugSearchSource(drugSearchSource);
		drugSearchReturn.setDrugSearchNameMapList(drugSearchNameMapList);
		drugSearchReturn.setItemCodeByItemCodeSearch(itemCodeByItemCodeSearch);
		return drugSearchReturn;
	}

	public DrugSearchReturn drugSearchByBnf(DrugSearchSource drugSearchSource, BnfCriteria bnfCriteria) {
		bnfCriteria.setDispHospCode(workstore.getHospCode());
		bnfCriteria.setDispWorkstore(workstore.getWorkstoreCode());
		
		DrugSearchReturn drugSearchReturn = new DrugSearchReturn();
		drugSearchReturn.setDrugSearchSource(drugSearchSource);
		drugSearchReturn.setDrugSearchBnfMapList(getBnfTreeDetail(drugSearchSource, bnfCriteria));
		return drugSearchReturn;
	}

	public DrugSearchReturn retrieveBnfTreeDetail(DrugSearchSource drugSearchSource, BnfCriteria bnfCriteria) {
		bnfCriteria.setDispHospCode(workstore.getHospCode());
		bnfCriteria.setDispWorkstore(workstore.getWorkstoreCode());

		DrugSearchReturn drugSearchReturn = new DrugSearchReturn();
		drugSearchReturn.setDrugSearchSource(drugSearchSource);
		drugSearchReturn.setDrugSearchBnfNodeMapList(getBnfTreeDetail(drugSearchSource, bnfCriteria));
		return drugSearchReturn; 
	}	
	
	private List<Map<String, Object>> getBnfTreeDetail(DrugSearchSource drugSearchSource, BnfCriteria bnfCriteria) {
		List<Map<String, Object>> bnfDrugInfoMapList = null;
		List<Map<String, Object>> bnfMapList = null; 
		List<Map<String, Object>> bnfDrugDetailMapList = new ArrayList<Map<String, Object>>(); 

		if (bnfCriteria.getBnfNo() != null) {
			List<BnfDrugInfo> bnfDrugInfoList = dmsPmsServiceProxy.retrieveBnfDrugInfo(bnfCriteria);
			bnfDrugInfoMapList = getBnfDrugInfoMapList(drugSearchSource, bnfDrugInfoList);
			if (bnfDrugInfoMapList != null && bnfDrugInfoMapList.size() > 0) {
				bnfDrugDetailMapList.addAll(bnfDrugInfoMapList);
			}
		}
		
		List<Bnf> bnfList = dmsPmsServiceProxy.retrieveBnf(bnfCriteria);
		bnfMapList = this.getBnfMapList(bnfList);
		if (bnfMapList.size() > 0) {
			bnfDrugDetailMapList.addAll(bnfMapList);
		}
		
		if (bnfDrugInfoMapList != null && bnfDrugInfoMapList.size() == 1 && bnfDrugDetailMapList.size() == 1) {
			bnfDrugInfoMapList.get(0).put("autoExpand", true);
		}

		if (bnfMapList.size() == 1 && bnfDrugDetailMapList.size() == 1) {
			Bnf bnf = (Bnf)bnfMapList.get(0).get("bnf");
			if (bnf.getHaveRecord()) {
				BnfCriteria innerBnfCriteria = new BnfCriteria();
				innerBnfCriteria.setDispHospCode(workstore.getHospCode());
				innerBnfCriteria.setDispWorkstore(workstore.getWorkstoreCode());
				innerBnfCriteria.setBnfNo(bnf.getBnfNo());
				innerBnfCriteria.setDrugScope(bnfCriteria.getDrugScope());
				bnfMapList.get(0).put("children", getBnfTreeDetail(drugSearchSource, innerBnfCriteria));
				bnfMapList.get(0).put("childrenLoaded", true);
				bnfMapList.get(0).put("autoExpand", true);
			}
		}
		
		return bnfDrugDetailMapList;
	}
	
	private List<Map<String, Object>> getBnfMapList(List<Bnf> bnfList) {
		List<Map<String, Object>> bnfMapList = new ArrayList<Map<String, Object>>();

		for (Bnf bnf : bnfList) {
			StringBuilder bnfDescSb = new StringBuilder();	
			bnfDescSb.append(SPACE)
					 .append(bnf.getBnfDesc());
			
			Map<String, Object> bnfMap = new HashMap<String, Object>();
			bnfMap.put("treeNode", bnf.getBnfNo() + WordUtils.capitalizeFully(bnfDescSb.toString(), DELIMITER));
			bnfMap.put("bnf", bnf);
			bnfMap.put("childrenLoaded", false);
			bnfMap.put("autoExpand", false);
			
			if (bnf.getHaveRecord()) {
				bnfMap.put("children", new ArrayList<Map<String, Object>>());
			}
			
			bnfMapList.add(bnfMap);
		}
		
		return bnfMapList;
	}
	
	private List<Map<String, Object>> getBnfDrugInfoMapList(DrugSearchSource drugSearchSource, List<BnfDrugInfo> bnfDrugInfoList) {
		List<Map<String, Object>> bnfDrugInfoMapList = new ArrayList<Map<String, Object>>();

		for (BnfDrugInfo bnfDrugInfo : bnfDrugInfoList) {
			Map<String, Object> bnfDrugInfoMap = new HashMap<String, Object>();
			bnfDrugInfoMap.put("treeNode", getDrugDisplayname(bnfDrugInfo.getFirstDisplayname(), bnfDrugInfo.getSecondDisplayname(), bnfDrugInfo.getConcatIngredients()));
			bnfDrugInfoMap.put("bnfDrugInfo", bnfDrugInfo);
			if ( drugSearchSource == DrugSearchSource.MpVetting || ! "PDF".equals(bnfDrugInfo.getTheraGroup()) ) { 
				bnfDrugInfoMap.put("children", new ArrayList<Map<String, Object>>());
				bnfDrugInfoMap.put("childrenLoaded", false); 
			} else {
				// do not allow user to expand BNF name for OP CAPD drug
				bnfDrugInfoMap.put("childrenLoaded", true); 
			}
			bnfDrugInfoMap.put("autoExpand", false);
			
			bnfDrugInfoMapList.add(bnfDrugInfoMap);
		}
		
		return bnfDrugInfoMapList;
	}
	
	public DrugSearchReturn retrieveDrugDetail(DrugSearchSource drugSearchSource, RouteFormSearchCriteria routeFormSearchCriteria, String theraGroup) {
		DrugSearchReturn drugSearchReturn = new DrugSearchReturn();
		drugSearchReturn.setDrugSearchSource(drugSearchSource);
		drugSearchReturn.setDrugSearchDetailMapList(getDrugDetail(drugSearchSource, routeFormSearchCriteria, theraGroup));
		return drugSearchReturn;
	}

	private List<Map<String, Object>> getDrugDetail(DrugSearchSource drugSearchSource, RouteFormSearchCriteria routeFormSearchCriteria, String theraGroup) {
		routeFormSearchCriteria.setDispHospCode(workstore.getHospCode());
		routeFormSearchCriteria.setDispWorkstore(workstore.getWorkstoreCode());

		if (drugSearchSource == DrugSearchSource.MpVetting) {
			routeFormSearchCriteria.setOpIncludeCommonOrders(true);
		} else {
			routeFormSearchCriteria.setOpIncludeCommonOrders(false);
		}
		if (drugSearchSource == DrugSearchSource.MpVetting) {
			if (medProfile.getMedCase().getCaseNum() != null) {
				routeFormSearchCriteria.setPatHospCode(medProfile.getPatHospCode());
				routeFormSearchCriteria.setWard(medProfile.getMedCase().getPasWardCode());
			} else {
				routeFormSearchCriteria.setPatHospCode(workstore.getDefPatHospCode());
			}
		}
		
		List<RouteForm> srcRouteFormList = dmsPmsServiceProxy.retrieveDrugSearchDetail(routeFormSearchCriteria);

		return getRouteFormMapList(drugSearchSource, srcRouteFormList, routeFormSearchCriteria, theraGroup);
	}
	
	private List<Map<String, Object>> getRouteFormMapList(DrugSearchSource drugSearchSource, List<RouteForm> srcRouteFormList, RouteFormSearchCriteria routeFormSearchCriteria, String theraGroup) {
		// create route form level tree node
		List<Map<String, Object>> routeFormMapList = new ArrayList<Map<String, Object>>();
		
		for (RouteForm routeForm : srcRouteFormList) {
			Map<String, Object> routeFormMap = new HashMap<String, Object>();			
			routeFormMap.put("treeNode", getRouteFormDisplay(routeForm));
			routeFormMap.put("routeForm", routeForm);
			if ( "Y".equals(routeForm.getSpecRestrict()) ) {
				routeFormMap.put("restrictedDrugDesc", getRestrictedDrugDesc(routeFormSearchCriteria, routeForm, null));
			}

			if (routeForm.getPreparationProperties() != null && routeForm.getPreparationProperties().length > 0) {
				routeFormMap.put("children", getPreparationMapList(drugSearchSource, routeForm.getPreparationProperties(), routeFormSearchCriteria, routeForm, theraGroup));
				routeFormMap.put("childrenLoaded", true);
				routeFormMap.put("hasPreparation", true);
				routeFormMap.put("hasCommonDosage", false);
				routeFormMap.put("hasCommonOrder", false);
			} else if ( routeForm.getDrugCommonDosages() != null && routeForm.getDrugCommonDosages().length > 0 && 
							isCommonDosageAvailableByAge(drugSearchSource, Arrays.asList(routeForm.getDrugCommonDosages())) ) {
				routeFormMap.put("commonDosageList", getCommonDosageMapList(routeForm.getDrugCommonDosages()));
				routeFormMap.put("hasPreparation", false);
				routeFormMap.put("hasCommonDosage", true);
				routeFormMap.put("hasCommonOrder", false);
			} else if ( ( routeForm.getCommonOrders() != null && routeForm.getCommonOrders().length > 0 && isCommonOrderAvailable(Arrays.asList(routeForm.getCommonOrders())) ) ||
						( routeForm.getPivasFormulas() != null && routeForm.getPivasFormulas().length > 0 ) ) {
				routeFormMap.put("commonOrderPivasFormulaList", getCommonOrderPivasFormulaMapList(routeForm.getCommonOrders(), routeForm.getPivasFormulas()));
				routeFormMap.put("hasPreparation", false);
				routeFormMap.put("hasCommonDosage", false);
				routeFormMap.put("hasCommonOrder", true);
			} else {
				routeFormMap.put("regimenList", getDmRegimenList(drugSearchSource, theraGroup));
				routeFormMap.put("hasPreparation", false);
				routeFormMap.put("hasCommonDosage", false);
				routeFormMap.put("hasCommonOrder", false);
			}

			routeFormMapList.add(routeFormMap);
		}
		
		return routeFormMapList;
	}
	
	private List<Map<String, Object>> getPreparationMapList(DrugSearchSource drugSearchSource, PreparationProperty[] srcPreparationList, RouteFormSearchCriteria routeFormSearchCriteria, RouteForm routeForm, String theraGroup) {
		// create preparation (item code) level tree node
		List<Map<String, Object>> preparationMapList = new ArrayList<Map<String, Object>>();
		for (PreparationProperty preparation : srcPreparationList) {
			Map<String, Object> preparationMap = new HashMap<String, Object>();
			preparationMap.put("treeNode", preparation.getPreparationDesc()); 
			preparationMap.put("preparation", preparation);
			if ( "Y".equals(preparation.getSpecRestrict()) ) {
				preparationMap.put("restrictedDrugDesc", getRestrictedDrugDesc(routeFormSearchCriteria, routeForm, preparation));
			}
			
			if ( preparation.getDrugCommonDosages() != null && preparation.getDrugCommonDosages().length > 0 &&
					isCommonDosageAvailableByAge(drugSearchSource, Arrays.asList(preparation.getDrugCommonDosages())) ) {
				preparationMap.put("commonDosageList", getCommonDosageMapList(preparation.getDrugCommonDosages()));
				preparationMap.put("hasCommonDosage", true);
				preparationMap.put("hasCommonOrder", false);
			} else if ( ( preparation.getCommonOrders() != null && preparation.getCommonOrders().length > 0 && isCommonOrderAvailable(Arrays.asList(preparation.getCommonOrders())) ) ||
						( preparation.getPivasFormulas() != null && preparation.getPivasFormulas().length > 0) ) {
				preparationMap.put("commonOrderPivasFormulaList", getCommonOrderPivasFormulaMapList(preparation.getCommonOrders(), preparation.getPivasFormulas()));
				preparationMap.put("hasCommonDosage", false);
				preparationMap.put("hasCommonOrder", true);
			} else {
				preparationMap.put("regimenList", getDmRegimenList(drugSearchSource, theraGroup));
				preparationMap.put("hasCommonDosage", false);
				preparationMap.put("hasCommonOrder", false);
			}
			
			preparationMapList.add(preparationMap);
		}
		
		return preparationMapList;
	}	
	
	private List<Map<String, Object>> getCommonDosageMapList(DrugCommonDosage[] srcCommonDosage) {
		// create common dosage level tree node
		List<Map<String, Object>> commonDosageMapList = new ArrayList<Map<String, Object>>();
		for (DrugCommonDosage commonDosage : srcCommonDosage) {
			Map<String, Object> commonDosageMap = new HashMap<String, Object>();
			commonDosageMap.put("dosageTypeDisplay", commonDosage.getDosageType());
			commonDosageMap.put("commonDosageDesc", getCommonDosageDisplay(commonDosage));
			commonDosageMap.put("drugCommonDosage", commonDosage);
			commonDosageMapList.add(commonDosageMap);
		}
		
		return commonDosageMapList;
	}
	
	private List<Map<String, Object>> getCommonOrderPivasFormulaMapList(CommonOrder[] srcCommonOrder, PivasFormula[] srcPivasFormula) {
		// create common order level tree node
		List<Map<String, Object>> commonOrderPivasFormulaMapList = new ArrayList<Map<String, Object>>();
		if (srcCommonOrder != null && srcCommonOrder.length > 0) {
			for (CommonOrder commonOrder : srcCommonOrder) {
				if (commonOrder.getDoseRuleCode() == null) {
					Map<String, Object> commonOrderMap = new HashMap<String, Object>();
					commonOrderMap.put("commonOrderDesc", getCommonOrderDisplay(commonOrder));
					commonOrderMap.put("commonOrder", commonOrder);
					commonOrderPivasFormulaMapList.add(commonOrderMap);
				}
			}
		}
		if (srcPivasFormula != null && srcPivasFormula.length > 0) {
			for (PivasFormula pivasFormula : srcPivasFormula) {
				Map<String, Object> commonOrderMap = new HashMap<String, Object>();
				commonOrderMap.put("pivasFormulaDesc", getPivasFormulaDisplay(pivasFormula));
				commonOrderMap.put("pivasFormula", pivasFormula);
				commonOrderPivasFormulaMapList.add(commonOrderMap);
			}
		}
		
		return commonOrderPivasFormulaMapList;
	}

	private List<Map<String, Object>> getDmRegimenList(DrugSearchSource drugSearchSource, String theraGroup) {
		List<DmRegimen> dmRegimenList = DmRegimenCacher.instance().getRegimenList();
		List<Map<String, Object>> dmRegimenMapList = new ArrayList<Map<String, Object>>();

		for (DmRegimen dmRegimen : dmRegimenList) {
			if ( drugSearchSource == DrugSearchSource.MpVetting && "S".equals(dmRegimen.getRegimenCode()) ) {
				// skip step up/down regimen for CARS2 non-IPMOE
				continue;
			}
			
			if ( "PDF".equals(theraGroup) && ! "D".equals(dmRegimen.getRegimenCode()) ) {
				continue;
			}
			
			Map<String, Object> dmRegimenMap = new HashMap<String, Object>();
			dmRegimenMap.put("regimenDesc", dmRegimen.getRegimenDesc()); 
			dmRegimenMap.put("dmRegimen", dmRegimen);
			dmRegimenMapList.add(dmRegimenMap);
		}
		
		return dmRegimenMapList;
	}
	
	private boolean isCommonDosageAvailableByAge(DrugSearchSource drugSearchSource, List<DrugCommonDosage> drugCommonDosageList) {
		AgeRange ageRange = null;
		if (drugSearchSource.getMaintenance()) {
			ageRange = AgeRange.All;	// set to All for Dosage Conversion Maint					
		} else {
			if (pharmOrder != null) {
				if (pharmOrder.getDob() == null) { 
					ageRange = AgeRange.All; // set to All if date of birth not provided
				} else if ( ! "year(s)".equals(pharmOrder.getDateUnit())) {
					ageRange = AgeRange.Pediatric;	// set to Pediatric if dob is in days or months
				} else if (Integer.valueOf(pharmOrder.getAge()) < 13) {
					ageRange = AgeRange.Pediatric;
				} else if (Integer.valueOf(pharmOrder.getAge()) >= 19) {
					ageRange = AgeRange.Adult;
				} else {	// between age 13 and 19
					ageRange = AgeRange.All;
				}
			} else {
				ageRange = AgeRange.All;	// set to All for Dosage Conversion Maint
			}
		}
		
		if (ageRange == AgeRange.Adult) {
			boolean isAdultExist = false;
			for (DrugCommonDosage drugCommonDosage : drugCommonDosageList) {
				if ("C".equals(drugCommonDosage.getDosageType()) || "A".equals(drugCommonDosage.getDosageType())) {
					isAdultExist = true;
				}
			}
			return isAdultExist;
		} else if (ageRange == AgeRange.Pediatric) {
			boolean isPaedExist = false;
			for (DrugCommonDosage drugCommonDosage : drugCommonDosageList) {
				if ("C".equals(drugCommonDosage.getDosageType()) || "P".equals(drugCommonDosage.getDosageType())) {
					isPaedExist = true;
				}
			}			
			return isPaedExist;
		}
		return true;
	}
	
	private boolean isCommonOrderAvailable(List<CommonOrder> commonOrderList) {
		boolean isNoDoseRuleExist = false;
		for (CommonOrder commonOrder : commonOrderList) {
			if (commonOrder.getDoseRuleCode() == null) {
				isNoDoseRuleExist = true;
				break;
			}
		}
		return isNoDoseRuleExist;
	}
	
	private String getDrugDisplayname(String inFirstDisplayname, String inSecondDisplayname, String inIngredient) {
		String tempFirstDisplayname = null;
		String fmStatusDesc = null;
		String[] nameArr = inFirstDisplayname.split("<");
		tempFirstDisplayname = nameArr[0];
		if (nameArr.length > 1) {
			fmStatusDesc = nameArr[1].substring(0, nameArr[1].length()-1); // remove the last '>'
		}

		StringBuilder displayStr = new StringBuilder();
		displayStr.append(WordUtils.capitalizeFully(StringUtils.trim(tempFirstDisplayname), DELIMITER));

		if (inIngredient != null && ( ! tempFirstDisplayname.contains("+"))) {
			displayStr.append(SPACE).append("[")
					  .append(WordUtils.capitalizeFully(inIngredient, DELIMITER))
					  .append("]");
		} else if (inSecondDisplayname != null) {
			displayStr.append(SPACE).append("[")
					  .append(WordUtils.capitalizeFully(inSecondDisplayname, DELIMITER))
					  .append("]");
		}
		
		if (StringUtils.isNotBlank(fmStatusDesc)) {
			displayStr.append(SPACE).append("<")
					  .append(StringUtils.trim(fmStatusDesc))
					  .append(">");
		}		
		return displayStr.toString();
	}

	private String getRouteFormDisplay(RouteForm routeForm) {
		StringBuilder displayStr = new StringBuilder();

		displayStr.append(routeForm.getMoeRouteFormDesc());
		if (routeForm.getSaltProperty() != null) {
			displayStr.append(SPACE).append("(")
					  .append(routeForm.getSaltProperty())
					  .append(")");
		}
		
		return WordUtils.capitalizeFully(displayStr.toString(), DELIMITER);
	}
	
	private String getCommonDosageDisplay(DrugCommonDosage commonDosage) {
		StringBuilder displayStr = new StringBuilder();

		if (commonDosage.getDosageValue() != 0) {
			DecimalFormat df = new DecimalFormat("#######.####");
			displayStr.append( df.format(commonDosage.getDosageValue()).toString() );
			if (commonDosage.getMoDosageUnit() != null && commonDosage.getMoDosageUnit().length() > 0 && 
					Character.isDigit(commonDosage.getMoDosageUnit().charAt(0))) {
				displayStr.append(SPACE)
				  		  .append("x");
			}
			displayStr.append(SPACE)
					  .append(commonDosage.getMoDosageUnit());
		}
		
    	if (commonDosage.getDailyFreq() != null) {
        	StringBuilder nFreqDescSb = new StringBuilder();
        	if (commonDosage.getDailyFreq().getDailyFreqBlk1() != null) {
        		nFreqDescSb.append(commonDosage.getDailyFreq().getDailyFreqBlk1());
        	}
    		if (commonDosage.getFreqValue() != null && commonDosage.getFreqValue() != 0){
    			nFreqDescSb.append(" ");
    			nFreqDescSb.append(commonDosage.getFreqValue());
    			nFreqDescSb.append(" ");
    		}
        	if (commonDosage.getDailyFreq().getDailyFreqBlk2() != null) {
        		nFreqDescSb.append(commonDosage.getDailyFreq().getDailyFreqBlk2());
        	}
        	if (nFreqDescSb.toString().length() > 0) {
    			if (displayStr.toString().length() > 0) {
    				displayStr.append(SPACE);
    			}
    			displayStr.append(nFreqDescSb.toString());
        	}
    	}
		
    	if (commonDosage.getSupplFreq() != null) {
        	StringBuilder nSupplFreqDescSb = new StringBuilder();
    		
        	if (commonDosage.getSupplFreq().getSupplFreqBlk1() != null) {
        		nSupplFreqDescSb.append(commonDosage.getSupplFreq().getSupplFreqBlk1());
        	}
    		if (commonDosage.getSupplFreqVal1() != null && commonDosage.getSupplFreqVal1() != 0){
    			nSupplFreqDescSb.append(" ");
    			nSupplFreqDescSb.append(commonDosage.getSupplFreqVal1());
    			nSupplFreqDescSb.append(" ");
    		}
        	if (commonDosage.getSupplFreq().getSupplFreqBlk2() != null) {
        		nSupplFreqDescSb.append(commonDosage.getSupplFreq().getSupplFreqBlk2());
        	}
    		if (commonDosage.getSupplFreqVal2() != null && commonDosage.getSupplFreqVal2() != 0){
    			nSupplFreqDescSb.append(" ");
    			nSupplFreqDescSb.append(commonDosage.getSupplFreqVal2());
    			nSupplFreqDescSb.append(" ");
    		}
        	if (commonDosage.getSupplFreq().getSupplFreqBlk3() != null) {
        		nSupplFreqDescSb.append(commonDosage.getSupplFreq().getSupplFreqBlk3());
        	}
        	if (nSupplFreqDescSb.toString().length() > 0) {
    			if (displayStr.toString().length() > 0) {
    				displayStr.append(SPACE);
    			}
    			displayStr.append("(");
    			displayStr.append(nSupplFreqDescSb.toString());
    			displayStr.append(")");
        	}
    	}
		
		if ( "Y".equals(commonDosage.getPrn()) ) {
			if (displayStr.toString().length() > 0) {
				displayStr.append(SPACE);
			}			
			displayStr.append("PRN");
		}
		
		return displayStr.toString();
	}

	private String getCommonOrderDisplay(CommonOrder commonOrder) {
		StringBuilder displayStr = new StringBuilder();
		
		boolean siteExist = false;
		if (commonOrder.getSupplSiteDesc() != null) {
			siteExist = true;
			displayStr.append(commonOrder.getSupplSiteDesc());
		} else if (commonOrder.getIpmoeDesc() != null) {
			siteExist = true;
			displayStr.append(commonOrder.getIpmoeDesc());
		}		

		boolean doseTypeExist = false;
		if ( ! "C".equals(commonOrder.getDoseTypeCode()) ) {
			doseTypeExist = true;
			
			if (displayStr.toString().length() > 0) {
				displayStr.append(SPACE);
			}			
			displayStr.append("(");
			if ( "L".equals(commonOrder.getDoseTypeCode()) ) {
				displayStr.append("Loading");
			} else if ( "M".equals(commonOrder.getDoseTypeCode()) ) {
				displayStr.append("Maintenance");
			} else if ( "P".equals(commonOrder.getDoseTypeCode()) ) {
				displayStr.append("Surgical Prophylaxis");
			} else if ( "R".equals(commonOrder.getDoseTypeCode()) ) {
				displayStr.append("Renal");
			}
			displayStr.append(")");
		}
		
		if (siteExist || doseTypeExist) { 
			displayStr.append(": ");
		}
		
		if (commonOrder.getDoseValueFrom() != null && commonOrder.getDoseValueFrom() != 0) {
			DecimalFormat df = new DecimalFormat("#######.####");
			displayStr.append( df.format(commonOrder.getDoseValueFrom()).toString() );
			if (commonOrder.getDoseValueTo() != null && commonOrder.getDoseValueTo() != 0) {
				displayStr.append(" - ");
				displayStr.append( df.format(commonOrder.getDoseValueTo()).toString() );
			}
			
			if (commonOrder.getMoDosageUnit() != null && commonOrder.getMoDosageUnit().length() > 0 && 
					Character.isDigit(commonOrder.getMoDosageUnit().charAt(0))) {
				displayStr.append(SPACE)
				  		  .append("x");
			}
			displayStr.append(SPACE)
					  .append(commonOrder.getMoDosageUnit());
		}
		
    	if (commonOrder.getDailyFreq() != null) {
        	StringBuilder nFreqDescSb = new StringBuilder();
        	if (commonOrder.getDailyFreq().getDailyFreqBlk1() != null) {
        		nFreqDescSb.append(commonOrder.getDailyFreq().getDailyFreqBlk1());
        	}
    		if (commonOrder.getFreqValue1() != null && commonOrder.getFreqValue1() != 0){
    			nFreqDescSb.append(" ");
    			nFreqDescSb.append(commonOrder.getFreqValue1().intValue());
    			nFreqDescSb.append(" ");
    		}
        	if (commonOrder.getDailyFreq().getDailyFreqBlk2() != null) {
        		nFreqDescSb.append(commonOrder.getDailyFreq().getDailyFreqBlk2());
        	}
        	if (nFreqDescSb.toString().length() > 0) {
    			if (displayStr.toString().length() > 0) {
    				displayStr.append(SPACE);
    			}
    			displayStr.append(nFreqDescSb.toString());
        	}
    	}
		
    	if (commonOrder.getSupplFreq() != null) {
        	StringBuilder nSupplFreqDescSb = new StringBuilder();
    		
        	if (commonOrder.getSupplFreq().getSupplFreqBlk1() != null) {
        		nSupplFreqDescSb.append(commonOrder.getSupplFreq().getSupplFreqBlk1());
        	}
    		if (commonOrder.getSupplFreqValue1() != null && commonOrder.getSupplFreqValue1() != 0){
    			nSupplFreqDescSb.append(" ");
    			nSupplFreqDescSb.append(commonOrder.getSupplFreqValue1().intValue());
    			nSupplFreqDescSb.append(" ");
    		}
        	if (commonOrder.getSupplFreq().getSupplFreqBlk2() != null) {
        		nSupplFreqDescSb.append(commonOrder.getSupplFreq().getSupplFreqBlk2());
        	}
    		if (commonOrder.getSupplFreqValue2() != null && commonOrder.getSupplFreqValue2() != 0){
    			nSupplFreqDescSb.append(" ");
    			nSupplFreqDescSb.append(commonOrder.getSupplFreqValue2().intValue());
    			nSupplFreqDescSb.append(" ");
    		}
        	if (commonOrder.getSupplFreq().getSupplFreqBlk3() != null) {
        		nSupplFreqDescSb.append(commonOrder.getSupplFreq().getSupplFreqBlk3());
        	}
        	if (nSupplFreqDescSb.toString().length() > 0) {
    			if (displayStr.toString().length() > 0) {
    				displayStr.append(SPACE);
    			}
    			displayStr.append("(");
    			displayStr.append(nSupplFreqDescSb.toString());
    			displayStr.append(")");
        	}
    	}
		
		if ( "Y".equals(commonOrder.getPrn()) ) {
			if (displayStr.toString().length() > 0) {
				displayStr.append(SPACE);
			}			
			displayStr.append("PRN");
		}
    	
		return displayStr.toString();
	}

	private String getPivasFormulaDisplay(PivasFormula pivasFormula) {
		StringBuilder displayStr = new StringBuilder();

		if (pivasFormula.getIpmoeDesc() != null) {
			displayStr.append(pivasFormula.getIpmoeDesc());
		} 
		
		if ("ML".equals(pivasFormula.getPivasDosageUnit())) {
			if (pivasFormula.getDiluentCode() != null) {
				DecimalFormat df = new DecimalFormat("######.##");
				displayStr.append(": ");
				displayStr.append( df.format(pivasFormula.getVolume()) );
				displayStr.append("ML in ");
				displayStr.append( df.format(pivasFormula.getVolume().add(pivasFormula.getDiluentVolume())) );
				displayStr.append("ML ");
				displayStr.append(pivasFormula.getDiluentCode());
			}
			
		} else {
			DecimalFormat df = new DecimalFormat("######.###");
			displayStr.append(": ");
			displayStr.append( df.format(pivasFormula.getConcn()) );
			displayStr.append(pivasFormula.getPivasDosageUnit());
			displayStr.append(" per ML");
			
			if (pivasFormula.getDiluentCode() != null) {
				displayStr.append(" ");
				displayStr.append(pivasFormula.getDiluentCode());
			}
		}
		
		return displayStr.toString();
	}
	
	private String getRestrictedDrugDesc(RouteFormSearchCriteria routeFormSearchCriteria, RouteForm routeForm, PreparationProperty preparationProperty) {
		StringBuilder displayStr = new StringBuilder();
		boolean isIncludeAlias = false;
		
		if (routeForm.getTrueAliasname() != null && "T".equals(routeFormSearchCriteria.getNameType())) {
			isIncludeAlias = true;
		}

		if (isIncludeAlias) {
			displayStr.append(WordUtils.capitalizeFully(routeForm.getTrueAliasname())).append(" (");
		}
		
		displayStr.append(WordUtils.capitalizeFully(routeFormSearchCriteria.getTrueDisplayname(), DELIMITER));
		
		if (routeForm.getSaltProperty() != null) {
			displayStr.append(SPACE)
					  .append(WordUtils.capitalizeFully(routeForm.getSaltProperty()));
		}
		
		if (isIncludeAlias) {
			displayStr.append(")");
		}		
		
		if (routeForm.getMoeDesc() != null) {
			displayStr.append(SPACE)
					  .append(routeForm.getMoeDesc().toLowerCase());
		}
		
		if (preparationProperty != null) {
			if (preparationProperty.getStrength() != null) {
				displayStr.append(SPACE)
						  .append(preparationProperty.getStrength().toLowerCase());
			}
			
			if (preparationProperty.getVolumeValue() != null && preparationProperty.getVolumeValue() > 0) {
				displayStr.append(SPACE);
				if ( new Double(preparationProperty.getVolumeValue().intValue()).equals(preparationProperty.getVolumeValue()) ) {
					displayStr.append(preparationProperty.getVolumeValue().intValue());
				} else {
					displayStr.append(preparationProperty.getVolumeValue());
				}
				if (preparationProperty.getVolumeUnit() != null) {
					displayStr.append(preparationProperty.getVolumeUnit().toLowerCase());
				}
			}
			
			if ( preparationProperty.getExtraInfo() != null && ! preparationProperty.getExtraInfo().equals(routeForm.getTrueAliasname()) ) {
				displayStr.append(SPACE).append("(")
						  .append(preparationProperty.getExtraInfo().toLowerCase())
						  .append(")");
			}
		}

		String fmStatusStr;
		if (preparationProperty == null) {
			fmStatusStr = routeForm.getPmsFmStatus();
		} else {
			fmStatusStr = preparationProperty.getPmsFmStatus();
		}
		if (fmStatusStr != null) {
			FmStatus fmStatus = FmStatus.dataValueOf(fmStatusStr);
			if (fmStatus != null && fmStatus != FmStatus.GeneralDrug && fmStatus != FmStatus.FreeTextEntryItem) {
				displayStr.append(SPACE).append("<");
				displayStr.append(fmStatus.getDisplayValue());
				displayStr.append(">");
			}
		}
		
		return displayStr.toString();
	}
	
	public DrugSearchReturn retrieveDrugPrice(DrugSearchSource drugSearchSource, DrugName drugName, PreparationSearchCriteria preparationSearchCriteria, RelatedDrugSearchCriteria relatedDrugSearchCriteria) {
		drugSearchDrugPriceList = new ArrayList<Map<String, Object>>();
		
		if (drugName == null) {
			// retrieve price of related drugs from Drug Price Enquiry popup
			// (reuse preparationSearchCriteria which used to retrive drug price at first time)
			
			relatedDrugSearchCriteria.setDispHospCode(workstore.getHospCode());
			relatedDrugSearchCriteria.setDispWorkstore(workstore.getWorkstoreCode());
			
			List<DrugName> drugNameList = dmsPmsServiceProxy.retrieveRelatedDrug(relatedDrugSearchCriteria);
			
			for (DrugName findDrugName : drugNameList) {
				preparationSearchCriteria.setDispHospCode(workstore.getHospCode());
				preparationSearchCriteria.setDispWorkstore(workstore.getWorkstoreCode());
				preparationSearchCriteria.setTrueDisplayname(findDrugName.getTrueDisplayname());
				preparationSearchCriteria.setPmsFmStatus(findDrugName.getPmsFmStatus());
				
				List<PreparationProperty> preparationList = dmsPmsServiceProxy.retrievePreparation(preparationSearchCriteria);
				drugSearchDrugPriceList.addAll(groupDrugPriceList(findDrugName, preparationList));
			}
		} else {
			// retrieve drug price from selected drug search tree node
			preparationSearchCriteria.setDispHospCode(workstore.getHospCode());
			preparationSearchCriteria.setDispWorkstore(workstore.getWorkstoreCode());

			List<PreparationProperty> preparationList = dmsPmsServiceProxy.retrievePreparation(preparationSearchCriteria);
			drugSearchDrugPriceList = groupDrugPriceList(drugName, preparationList);
		}
		
		// note: sorting of drugSearchDrugPriceList is done in front-end, because groupingCollection2 in flex will override 
		//       the sorting in back-end
		
		DrugSearchReturn drugSearchReturn = new DrugSearchReturn();
		drugSearchReturn.setDrugSearchSource(drugSearchSource);
		return drugSearchReturn;
	}
	
	private List<Map<String, Object>> groupDrugPriceList(DrugName drugName, List<PreparationProperty> preparationList) {
		List<Map<String, Object>> drugPriceMapList = new ArrayList<Map<String, Object>>();
		
		for (PreparationProperty preparation : preparationList) {
			if (FmStatus.dataValueOf(preparation.getPmsFmStatus()) == FmStatus.SelfFinancedItemC || 
					FmStatus.dataValueOf(preparation.getPmsFmStatus()) == FmStatus.SampleItem) {
				// skip formulary item in drug price list
				continue;
			}
			
			Map<String, Object> drugPriceMap = new HashMap<String, Object>();
			
			// set drug name 
			boolean secondDisplaynameExist = false;
			String[] drugNameArr = drugName.getFirstDisplayname().split("<");
			StringBuilder drugNameOut = new StringBuilder();
			drugNameOut.append(WordUtils.capitalizeFully(StringUtils.trim(drugNameArr[0]), DELIMITER)); 
			if (drugName.getSecondDisplayname() != null) {
				secondDisplaynameExist = true;
				drugNameOut.append(" (");
				drugNameOut.append(WordUtils.capitalizeFully(drugName.getSecondDisplayname(), DELIMITER));
			}	
			if (preparation.getSaltProperty() != null) {
				drugNameOut.append(SPACE);
				drugNameOut.append(WordUtils.capitalizeFully(preparation.getSaltProperty(), DELIMITER));
			}
			if (secondDisplaynameExist) {
				drugNameOut.append(")");
			}
			drugPriceMap.put("drugName", drugNameOut.toString());
			
			// set route desc and preparation desc
			drugPriceMap.put("moeRouteFormDesc", WordUtils.capitalizeFully(preparation.getMoeRouteFormDesc()));
			drugPriceMap.put("preparationDesc", preparation.getPreparationDesc());
			
			// set drug price
			if (preparation.getPublicCdp() != null) {
				drugPriceMap.put("publicCdp", preparation.getPublicCdp());
			}
			
			// set quantity and base unit
			drugPriceMap.put("commonPackSize", 1);	// preparation.commonPackSize is obsolete and can be hardcode to 1
			drugPriceMap.put("baseUnit", preparation.getBaseUnit());

			// set formulary status
			FmStatus fmStatus = FmStatus.dataValueOf(preparation.getPmsFmStatus());
			if (fmStatus != null && fmStatus != FmStatus.GeneralDrug) {
				drugPriceMap.put("fmStatusDesc", fmStatus.getDisplayValue());
			}
			
			// set preparation (for sorting)
			drugPriceMap.put("preparation", preparation);
			
			drugPriceMapList.add(drugPriceMap);
		}		

		// note: sorting of drugPriceMapList is done in front-end, because groupingCollection2 in flex will override 
		//       the sorting in back-end
		
		return drugPriceMapList;
	}

	public DrugSearchReturn retrieveFreeTextEntry(DrugSearchSource drugSearchSource) {
		List<String> drugSearchRouteDescList = DmRouteCacher.instance().getDistinctRouteDesc();
		List<String> drugSearchFormDescList = DmFormCacher.instance().getDistinctFormDesc();
		Collections.sort(drugSearchRouteDescList);
		Collections.sort(drugSearchFormDescList);
		
		DrugSearchReturn drugSearchReturn = new DrugSearchReturn();
		drugSearchReturn.setDrugSearchSource(drugSearchSource);
		drugSearchReturn.setDrugSearchRouteDescList(drugSearchRouteDescList);
		drugSearchReturn.setDrugSearchFormDescList(drugSearchFormDescList);
		return drugSearchReturn;
	}

	public DrugSearchReturn retrieveMpFreeTextEntry(DrugSearchSource drugSearchSource) {
		List<Site> drugSearchSiteList = new ArrayList<Site>();
		
		List<DmSite> dmSiteList = DmSiteCacher.instance().getSiteList();
		for (DmSite dmSite:dmSiteList) {
			if ("B".equals(dmSite.getUsageType()) || "I".equals(dmSite.getUsageType())) {
				drugSearchSiteList.add(new Site(dmSite,null,false));
			}
		}
		
		Collections.sort(drugSearchSiteList, new Regimen.PmsSiteComparator());
		
		// add OTHERS option
		Site site = new Site();
		site.setSiteCode( "OTHERS" );
		site.setSupplSiteDesc( "OTHERS" );
		site.setSiteLabelDesc( "OTHERS" );
		drugSearchSiteList.add( site );		
		
		DrugSearchReturn drugSearchReturn = new DrugSearchReturn();
		drugSearchReturn.setDrugSearchSource(drugSearchSource);
		drugSearchReturn.setDrugSearchSiteList(drugSearchSiteList);
		return drugSearchReturn;
	}
	
	public DrugSearchReturn retrieveMpCapdDrugSupplier(DrugSearchSource drugSearchSource) {
		drugSearchCapdSupplierSystemList = capdListManager.retrieveCapdSupplierSystemList();
		drugSearchCapdCalciumStrengthList = new ArrayList<String>();
		drugSearchCapdConcentrationList = new ArrayList<Capd>();

		DrugSearchReturn drugSearchReturn = new DrugSearchReturn();
		drugSearchReturn.setDrugSearchSource(drugSearchSource);
		return drugSearchReturn;
	}
	
	public DrugSearchReturn retrieveMpCapdDrugCalcium(DrugSearchSource drugSearchSource, String supplierSystem) {
		drugSearchCapdCalciumStrengthList = capdListManager.retrieveCapdCalciumStrengthList(supplierSystem);
		drugSearchCapdConcentrationList = new ArrayList<Capd>();
		
		DrugSearchReturn drugSearchReturn = new DrugSearchReturn();
		drugSearchReturn.setDrugSearchSource(drugSearchSource);
		return drugSearchReturn;
	}
	
	public DrugSearchReturn retrieveMpCapdDrugConcentration(DrugSearchSource drugSearchSource, String supplierSystem, String calciumStrength) {
		drugSearchCapdConcentrationList = capdListManager.retrieveCapdConcentrationList(supplierSystem, calciumStrength);
		
		DrugSearchReturn drugSearchReturn = new DrugSearchReturn();
		drugSearchReturn.setDrugSearchSource(drugSearchSource);
		return drugSearchReturn;
	}	
	
	public DrugSearchReturn createFreeTextEntryDrug(DrugSearchSource drugSearchSource, RxDrug rxDrug, Integer carryDuration, RegimenDurationUnit carryDurationUnit) {
		DrugSearchReturn drugSearchReturn = drugSearchOpManager.createFreeTextEntryDrug(drugSearchSource, rxDrug, carryDuration, carryDurationUnit, pharmOrder);
		
		drugSearchMedOrderItem = drugSearchReturn.getDrugSearchMedOrderItem();
		
		return drugSearchReturn; 
	}
	
	public DrugSearchReturn createCapdDrug(DrugSearchSource drugSearchSource) {
		DrugSearchReturn drugSearchReturn = drugSearchOpManager.createCapdDrug(drugSearchSource, pharmOrder);
		
		drugSearchMedOrderItem = drugSearchReturn.getDrugSearchMedOrderItem();
		
		return drugSearchReturn; 
	}

	public DrugSearchReturn createSelectedDrugByDosageConversion(DrugSearchSource drugSearchSource, DrugName drugName, RouteForm routeForm, PreparationProperty preparation, DrugCommonDosage commonDosage, DmRegimen dmRegimen) {
		DrugSearchReturn drugSearchReturn = drugSearchOpManager.createSelectedDrugByDosageConversion(drugSearchSource, drugName, routeForm, preparation, commonDosage, dmRegimen);
		return drugSearchReturn; 
	}
	
	public DrugSearchReturn createSelectedDrugByVetting(DrugSearchSource drugSearchSource, DrugName drugName, RouteForm routeForm, PreparationProperty preparation, DrugCommonDosage commonDosage, DmRegimen dmRegimen, Integer carryDuration, RegimenDurationUnit carryDurationUnit, String specialInstruction) {
		DrugSearchReturn drugSearchReturn = drugSearchOpManager.createSelectedDrugByVetting(drugSearchSource, drugName, routeForm, preparation, commonDosage, dmRegimen, carryDuration, carryDurationUnit, specialInstruction, pharmOrder);		

		drugSearchMedOrderItem = drugSearchReturn.getDrugSearchMedOrderItem();
		
		return drugSearchReturn; 
	}	
	
	public DrugSearchReturn createMpFreeTextEntryDrug(DrugSearchSource drugSearchSource, RxDrug rxDrug) {
		DrugSearchReturn drugSearchReturn = drugSearchMpManager.createMpFreeTextEntryDrug(drugSearchSource, rxDrug, medProfile, atdpsPickStationsNumList);
		return drugSearchReturn; 
	}
	
	public DrugSearchReturn createMpCapdDrug(DrugSearchSource drugSearchSource, String supplierSystem, String calciumStrength, Capd capd) {
		DrugSearchReturn drugSearchReturn = drugSearchMpManager.createMpCapdDrug(drugSearchSource, supplierSystem, calciumStrength, capd, medProfile, atdpsPickStationsNumList);
		return drugSearchReturn; 
	}
	
	public DrugSearchReturn createSelectedMpDrugByVetting(DrugSearchSource drugSearchSource, DrugName drugName, RouteForm routeForm, PreparationProperty preparation, CommonOrder commonOrder, PivasFormula pivasFormula, DmRegimen dmRegimen, String pasSpecialty, String pasSubSpecialty, String itemCode) {
		DrugSearchReturn drugSearchReturn = drugSearchMpManager.createSelectedMpDrugByVetting(drugSearchSource, drugName, routeForm, preparation, commonOrder, pivasFormula, dmRegimen, pasSpecialty, pasSubSpecialty, itemCode, medProfile, atdpsPickStationsNumList);
		return drugSearchReturn; 
	}	
	
	public static class DrugPriceNameComparator implements Comparator<Map<String, Object>>, Serializable {

		private static final long serialVersionUID = 1L;
		
		@Override
		public int compare(Map<String, Object> drugPriceName1, Map<String, Object> drugPriceName2) {
			return new CompareToBuilder().append( drugPriceName1.get("drugName"), drugPriceName2.get("drugName") )
										 .toComparison();
		}		
	}
	
	public static class DrugPricePreparationComparator implements Comparator<Map<String, Object>>, Serializable {
		
		private static final long serialVersionUID = 1L;

		@Override
		public int compare(Map<String, Object> drugPriceMap1, Map<String, Object> drugPriceMap2) {
			PreparationProperty preparation1 = (PreparationProperty)drugPriceMap1.get("preparation");
			PreparationProperty preparation2 = (PreparationProperty)drugPriceMap2.get("preparation");
			
			return new CompareToBuilder().append( drugPriceMap1.get("drugName"), drugPriceMap2.get("drugName") )
										 .append( preparation1.getRouteSortSeq(), preparation2.getRouteSortSeq() )
										 .append( preparation1.getRouteFormSortSeq(), preparation2.getRouteFormSortSeq() )
										 .append( preparation1.getFormRank(), preparation2.getFormRank() )
										 .append( preparation1.getPreparationDesc(), preparation2.getPreparationDesc() )
										 .toComparison();
		}		
	}
	
	@Remove
	public void destroy() 
	{
	}
}