package hk.org.ha.model.pms.biz.reftable;
import hk.org.ha.model.pms.vo.reftable.WardStockMap;

import javax.ejb.Local;

@Local
public interface WardStockServiceLocal {
	WardStockMap retrieveWardStockMap();
	void destroy();
}
