package hk.org.ha.model.pms.udt.medprofile;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;


public enum DischargeMedOrderListType implements StringValuedEnum {

	Normal("N", "Normal"),
	All("A", "All"),
	History("H", "History");
	
    private final String dataValue;
    private final String displayValue;
	
	private DischargeMedOrderListType(String dataValue, String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}

    public static DischargeMedOrderListType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DischargeMedOrderListType.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<DischargeMedOrderListType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<DischargeMedOrderListType> getEnumClass() {
    		return DischargeMedOrderListType.class;
    	}
    }    
}
