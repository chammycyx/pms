package hk.org.ha.model.pms.vo.report;

import hk.org.ha.model.pms.udt.report.PivasItemCat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class PivasWorklistSummaryRptRawMaterialDtl implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private PivasItemCat pivasItemCat;

	private String itemCode;
	
	private String itemDesc;
	
	private String manufCode;
	
	private String batchNum;
	
	private Date expiryDate;
	
	private BigDecimal dosage;
	
	private String totalDosage;
	
	private String modu;
	
	private String estimatedQty;
	
	private String baseUnit;
	
	private String roundQty;

	public PivasItemCat getPivasItemCat() {
		return pivasItemCat;
	}

	public void setPivasItemCat(PivasItemCat pivasItemCat) {
		this.pivasItemCat = pivasItemCat;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemDesc() {
		return itemDesc;
	}

	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	public String getManufCode() {
		return manufCode;
	}

	public void setManufCode(String manufCode) {
		this.manufCode = manufCode;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public BigDecimal getDosage() {
		return dosage;
	}

	public void setDosage(BigDecimal dosage) {
		this.dosage = dosage;
	}

	public String getTotalDosage() {
		return totalDosage;
	}

	public void setTotalDosage(String totalDosage) {
		this.totalDosage = totalDosage;
	}

	public String getModu() {
		return modu;
	}

	public void setModu(String modu) {
		this.modu = modu;
	}

	public String getEstimatedQty() {
		return estimatedQty;
	}

	public void setEstimatedQty(String estimatedQty) {
		this.estimatedQty = estimatedQty;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public String getRoundQty() {
		return roundQty;
	}

	public void setRoundQty(String roundQty) {
		this.roundQty = roundQty;
	}
}
