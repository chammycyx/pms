package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum MedOrderDocType implements StringValuedEnum {

	// Both OP & IP
	Normal("N", "Normal"),
	// for OP
	Manual("M", "Manual");

    private final String dataValue;
    private final String displayValue;
    
    MedOrderDocType(final String dataValue, final String displayValue) {
    	this.dataValue = dataValue;
    	this.displayValue = displayValue;
    }
	
    public String getDataValue() {
        return this.dataValue;
    }

    public String getDisplayValue() {
        return this.displayValue;
    }
    
    public static MedOrderDocType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MedOrderDocType.class, dataValue);
	}
    
    public static class Converter extends StringValuedEnumConverter<MedOrderDocType> {
    	
		private static final long serialVersionUID = 1L;

		public Class<MedOrderDocType> getEnumClass() {
    		return MedOrderDocType.class;
    	}
    }
}
