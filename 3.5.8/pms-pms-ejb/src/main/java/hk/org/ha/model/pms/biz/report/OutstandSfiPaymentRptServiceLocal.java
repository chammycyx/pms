package hk.org.ha.model.pms.biz.report;

import hk.org.ha.model.pms.persistence.disp.Invoice;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface OutstandSfiPaymentRptServiceLocal {

	List<Invoice> retrieveOutstandSfiPaymentRpt(Date dispDate);
	
	void generateOutstandSfiPaymentRpt();

	void printOutstandSfiPaymentRpt();

	void destroy();
}
