package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class MpTrxRptItemDtl implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private List<MpTrxRptPoItemDtl> mpTrxRptPoItemDtlList;

	private List<MpTrxRptPoItemHdr> mpTrxRptPoItemHdrList;
	
	private List<MpTrxRptPivasItemDtl> mpTrxRptPivasItemDtlList;

	private Long medProfileMoItemId;
	
	private String drugOrder;

	private String overrideReason;
	
	private String siteCode;
	
	private boolean separateLineIndicator;
	
	private Date startDate;
	
	private Date reviewDate;
	
	private Date endDate;
	
	private Integer medProfileMoItemOrgItemNum;
	
	private Boolean pivasFlag;

	public List<MpTrxRptPoItemDtl> getMpTrxRptPoItemDtlList() {
		return mpTrxRptPoItemDtlList;
	}

	public void setMpTrxRptPoItemDtlList(
			List<MpTrxRptPoItemDtl> mpTrxRptPoItemDtlList) {
		this.mpTrxRptPoItemDtlList = mpTrxRptPoItemDtlList;
	}

	public String getDrugOrder() {
		return drugOrder;
	}

	public void setDrugOrder(String drugOrder) {
		this.drugOrder = drugOrder;
	}

	public String getOverrideReason() {
		return overrideReason;
	}

	public void setOverrideReason(String overrideReason) {
		this.overrideReason = overrideReason;
	}

	public String getSiteCode() {
		return siteCode;
	}

	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

	public boolean isSeparateLineIndicator() {
		return separateLineIndicator;
	}

	public void setSeparateLineIndicator(boolean separateLineIndicator) {
		this.separateLineIndicator = separateLineIndicator;
	}

	public List<MpTrxRptPoItemHdr> getMpTrxRptPoItemHdrList() {
		return mpTrxRptPoItemHdrList;
	}

	public void setMpTrxRptPoItemHdrList(
			List<MpTrxRptPoItemHdr> mpTrxRptPoItemHdrList) {
		this.mpTrxRptPoItemHdrList = mpTrxRptPoItemHdrList;
	}

	public Long getMedProfileMoItemId() {
		return medProfileMoItemId;
	}

	public void setMedProfileMoItemId(Long medProfileMoItemId) {
		this.medProfileMoItemId = medProfileMoItemId;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setReviewDate(Date reviewDate) {
		this.reviewDate = reviewDate;
	}

	public Date getReviewDate() {
		return reviewDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setMedProfileMoItemOrgItemNum(Integer medProfileMoItemOrgItemNum) {
		this.medProfileMoItemOrgItemNum = medProfileMoItemOrgItemNum;
	}

	public Integer getMedProfileMoItemOrgItemNum() {
		return medProfileMoItemOrgItemNum;
	}

	public List<MpTrxRptPivasItemDtl> getMpTrxRptPivasItemDtlList() {
		return mpTrxRptPivasItemDtlList;
	}

	public void setMpTrxRptPivasItemDtlList(
			List<MpTrxRptPivasItemDtl> mpTrxRptPivasItemDtlList) {
		this.mpTrxRptPivasItemDtlList = mpTrxRptPivasItemDtlList;
	}

	public Boolean getPivasFlag() {
		return pivasFlag;
	}

	public void setPivasFlag(Boolean pivasFlag) {
		this.pivasFlag = pivasFlag;
	}

}