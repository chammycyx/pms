package hk.org.ha.model.pms.vo.medprofile;

import java.io.Serializable;

public class AuthenticateResult implements Serializable {

	private static final long serialVersionUID = 1L;

	private String status;
	
	private String userName;
	
	private String displayName;

	public AuthenticateResult() {
		super();
	}
	
	public AuthenticateResult(hk.org.ha.model.pms.uam.vo.AuthenticateResult result) {
		
		super();
		status = "invalid";
		if (result != null) {
			switch (result.getStatus()) {
				case Succeed:
					status = "success";
					break;
				case NoAccess:
					status = "no_access";
					break;
			}
			userName = result.getUserName();
			displayName = result.getDisplayName();
		}
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
}
