package hk.org.ha.model.pms.vo.alert;

import java.io.Serializable;

import hk.org.ha.model.pms.udt.alert.DiseaseType;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class DiseaseTypeAdapter extends XmlAdapter<String, DiseaseType> implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public String marshal(DiseaseType v) { 
		return v.getDataValue();
	}

	public DiseaseType unmarshal(String v) {
		return DiseaseType.dataValueOf(v);
	}

}
