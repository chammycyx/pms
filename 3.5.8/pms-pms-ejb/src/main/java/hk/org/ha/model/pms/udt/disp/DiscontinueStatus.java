package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DiscontinueStatus implements StringValuedEnum {

	None("-", "None"),
	Pending("P", "Pending"),
	Confirmed("C", "Confirmed"); 
	
    private final String dataValue;
    private final String displayValue;

    DiscontinueStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static DiscontinueStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DiscontinueStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<DiscontinueStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<DiscontinueStatus> getEnumClass() {
    		return DiscontinueStatus.class;
    	}
    }
}
