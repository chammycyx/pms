package hk.org.ha.model.pms.biz.sys;
import java.util.Date;

import javax.ejb.Local;

@Local
public interface TimeServiceLocal {
	Date getCurrentServerTime();
	void destroy();
}
