package hk.org.ha.model.pms.udt.vetting;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum FixedConcnSource implements StringValuedEnum {
	
	None("-", "None"),
	PMS("PMS", "PMS"),
	PIVAS("PIVAS", "PIVAS");

	private final String dataValue;
	private final String displayValue;
	
	FixedConcnSource (final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}
	
	public static FixedConcnSource dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(FixedConcnSource.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<FixedConcnSource> {
		private static final long serialVersionUID = 1L;

		public Class<FixedConcnSource> getEnumClass() {
    		return FixedConcnSource.class;
    	}
    }
}
