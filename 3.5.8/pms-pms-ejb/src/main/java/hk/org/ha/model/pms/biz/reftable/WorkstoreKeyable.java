package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.cache.Keyable;
import hk.org.ha.model.pms.persistence.corp.Workstore;

public class WorkstoreKeyable implements Keyable {
	
	@Override
	public Object toKey(Object[] params) {
		
		if (params.length == 0 || params[0] == null) {
			return null;
		}
		Workstore workstore = (Workstore) params[0];
		return workstore.getHospCode() + "." + workstore.getWorkstoreCode();
	}		
}