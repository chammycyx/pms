package hk.org.ha.model.pms.biz.reftable.pivas;

import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
import hk.org.ha.model.pms.persistence.reftable.pivas.PivasHoliday;

import java.util.List;

import javax.ejb.Local;

@Local
public interface PivasHolidayListManagerLocal {
	List<PivasHoliday> retrievePivasHolidayList(WorkstoreGroup workstoreGroup, String year); 
	
	void updatePivasHolidayList(List<PivasHoliday> updatePivasHolidayList, List<PivasHoliday> deletePivasHolidayList, WorkstoreGroup workstoreGroup);
}
