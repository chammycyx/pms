package hk.org.ha.model.pms.vo.alert;

import java.io.Serializable;

import hk.org.ha.model.pms.udt.alert.AlertProfileStatus;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class AlertProfileStatusAdapter extends XmlAdapter<String, AlertProfileStatus> implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public String marshal(AlertProfileStatus v) { 
		return v.getDataValue();
	}

	public AlertProfileStatus unmarshal(String v) {
		return AlertProfileStatus.dataValueOf(v);
	}

}
