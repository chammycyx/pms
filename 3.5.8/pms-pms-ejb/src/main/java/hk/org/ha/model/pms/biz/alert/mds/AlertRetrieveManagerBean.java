package hk.org.ha.model.pms.biz.alert.mds;

import static hk.org.ha.model.pms.prop.Prop.ALERT_MDS_DDI_SEVERITY_LEVEL_MAX;
import static hk.org.ha.model.pms.prop.Prop.ALERT_MDS_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.ALERT_MDS_G6PD_CODE;
import static hk.org.ha.model.pms.prop.Prop.ALERT_MDS_G6PD_CONDITIONID;
import static hk.org.ha.model.pms.prop.Prop.ALERT_MDS_G6PD_SEVERITY_LEVEL_MAX;
import static hk.org.ha.model.pms.prop.Prop.ALERT_MDS_LOG_VISIBLE;
import static hk.org.ha.model.pms.prop.Prop.ALERT_MDS_PREGNANCY_CONDITIONID;
import static hk.org.ha.model.pms.prop.Prop.ALERT_MDS_PREGNANCY_SEVERITY_LEVEL_MAX;
import static hk.org.ha.model.pms.prop.Prop.ALERT_PROFILE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.MEDPROFILE_ALERT_HLA_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.PATIENT_MOE_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.PATIENT_PAS_PATIENT_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.REFILL_SFI_ALERT_MDS_CHECK;
import static hk.org.ha.model.pms.prop.Prop.REFILL_STANDARD_ALERT_MDS_CHECK;
import static hk.org.ha.model.pms.prop.Prop.VETTING_ALERT_MDS_CHECK;
import static hk.org.ha.model.pms.prop.Prop.VETTING_DRUGONHAND_ONHANDSOURCETYPE;
import static hk.org.ha.model.pms.prop.Prop.VETTING_ORDER_MANUAL_ALERT_MDS_CHECK;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.alert.mds.MdsException;
import hk.org.ha.model.pms.biz.alert.AlertAuditManagerLocal;
import hk.org.ha.model.pms.biz.sys.SystemMessageManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.corp.cache.DmSiteCacher;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.AlertEntity;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.OnHandSourceType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
import hk.org.ha.model.pms.udt.vetting.CmsOrderSubType;
import hk.org.ha.model.pms.udt.vetting.CmsOrderType;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.udt.vetting.OnHandProfileOrderType;
import hk.org.ha.model.pms.udt.vetting.RegimenType;
import hk.org.ha.model.pms.vo.alert.mds.ErrorInfo;
import hk.org.ha.model.pms.vo.alert.mds.MdsCriteria;
import hk.org.ha.model.pms.vo.alert.mds.MdsLogInfo;
import hk.org.ha.model.pms.vo.alert.mds.MdsResult;
import hk.org.ha.model.pms.vo.rx.DhRxDrug;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.IvAdditive;
import hk.org.ha.model.pms.vo.rx.IvRxDrug;
import hk.org.ha.model.pms.vo.rx.Regimen;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.security.PropMap;
import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;
import hk.org.ha.service.pms.asa.interfaces.alert.AlertServiceJmsRemote;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.security.Identity;

@AutoCreate
@Stateless
@Name("alertRetrieveManager")
@MeasureCalls
public class AlertRetrieveManagerBean implements AlertRetrieveManagerLocal {

	private static final String ON_HAND_DRUG_NAME_SUFFIX = " (on hand drug)";
	
	private static final String DH_DRUG_NAME_SUFFIX = " (DH)";
	
	private static final int ADDITIVE_NUM_MULTIPLIER = 10000;
	
	private static final int MANUAL_ITEM_MULTIPLIER = 10000;
	
	private static final int MANUAL_ITEM_RANGE = 100000000; //100010000
	
	private static final String JAXB_CONTEXT_RX = "hk.org.ha.model.pms.vo.rx";
	
	@In
	private Workstation workstation;
	
	@In
	private Identity identity;

	@In
	private SystemMessageManagerLocal systemMessageManager;
	
	@In
	private AlertAuditManagerLocal alertAuditManager;
	
	@In
	private AlertServiceJmsRemote alertServiceProxy;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

	@In
	private PropMap propMap;
	
	@SuppressWarnings("unused")
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private MdsLogInfo mdsLogInfo;
	
	@SuppressWarnings("unused")
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private ErrorInfo mdsExceptionInfo;
	
	public List<AlertEntity> retrieveAlertByMedOrderItem(MedOrderItem medOrderItem, boolean checkDdiFlag) {
		
		Map<Integer, List<AlertEntity>> map = retrieveAlertByMedOrderItemList(Arrays.asList(medOrderItem), checkDdiFlag, false).getAlertEntityMap();
		if (map.containsKey(medOrderItem.getItemNum())) {
			return map.get(medOrderItem.getItemNum());
		}
		else {
			return new ArrayList<AlertEntity>();
		}
	}
	
	public boolean isMdsEnable() {

		mdsLogInfo = null;
		mdsExceptionInfo = null;
		
		if (!ALERT_MDS_ENABLED.get(Boolean.FALSE)) {
			return false;
		}
		
		switch (getOrderViewInfo().getOneStopOrderType()) {
			case MedOrder:
				return VETTING_ALERT_MDS_CHECK.get(Boolean.FALSE);

			case ManualOrder:
			case DhOrder:
				return VETTING_ORDER_MANUAL_ALERT_MDS_CHECK.get(Boolean.FALSE);

			case DispOrder:
				if (getPharmOrder().getMedOrder().getDocType() == MedOrderDocType.Normal) {
					return VETTING_ALERT_MDS_CHECK.get(Boolean.FALSE);
				}
				else {
					return VETTING_ORDER_MANUAL_ALERT_MDS_CHECK.get(Boolean.FALSE);
				}

			case RefillOrder:
				return REFILL_STANDARD_ALERT_MDS_CHECK.get(Boolean.FALSE);
				
			case SfiOrder:
				return REFILL_SFI_ALERT_MDS_CHECK.get(Boolean.FALSE);

			default:
				return false;
		}
	}
	
	public MdsResult retrieveAlertByMedOrderItemList(Collection<MedOrderItem> inMedOrderItemList, boolean checkDdiFlag, boolean checkSteroid) {
		
		OrderViewInfo orderViewInfo = getOrderViewInfo();
		
		DispOrder  dispOrder  = getDispOrder();
		PharmOrder pharmOrder = getPharmOrder();

		String caseNum;
		if (pharmOrder.getMedCase() == null) {
			caseNum = null;
		}
		else {
			caseNum = pharmOrder.getMedCase().getCaseNum();
		}
		
		Ticket ticket;
		String specCode;
		
		if (dispOrder == null) {
			specCode = pharmOrder.getSpecCode();
			ticket = pharmOrder.getMedOrder().getTicket();
		}
		else {
			specCode = dispOrder.getSpecCode();
			ticket = dispOrder.getTicket();
		}

		List<MedOrderItem> checkMedOrderItemList = new ArrayList<MedOrderItem>();
		List<MedOrderItem> noMappingDhItemList   = new ArrayList<MedOrderItem>();
		List<Integer> continuousInfusionList = new ArrayList<Integer>();
		Map<Integer, MedOrderItem> dhItemMap = new HashMap<Integer, MedOrderItem>();
		
		if (pharmOrder.getMedOrder().isDhOrder()) {
			dhItemMap = retrieveUpToDateDhMapping(pharmOrder.getMedOrder().getMedOrderItemList());
		}
		
		for (MedOrderItem inMedOrderItem : inMedOrderItemList) {
			if (inMedOrderItem.getStatus() == MedOrderItemStatus.SysDeleted || (inMedOrderItem.getStatus() == MedOrderItemStatus.Deleted && !pharmOrder.getMedOrder().isDhOrder())) {
				continue;
			}
			MedOrderItem checkMedOrderItem;
			if (pharmOrder.getMedOrder().isDhOrder()) {
				if (!dhItemMap.containsKey(inMedOrderItem.getItemNum())) {
					noMappingDhItemList.add(inMedOrderItem);
					continue;
				}
				checkMedOrderItem = dhItemMap.get(inMedOrderItem.getItemNum());
			}
			else {
				checkMedOrderItem = inMedOrderItem;
			}
			
			if (checkMedOrderItem.getRxDrug() != null) {
				if (FmStatus.FreeTextEntryItem.getDataValue().equals(checkMedOrderItem.getFmStatus())) {
					continue;
				}
				checkMedOrderItemList.add(constructMedOrderItemForNonIvRxDrug(checkMedOrderItem));
				if (checkMedOrderItem.getRxDrug().isInjectionRxDrug() && "CF".equals(DmSiteCacher.instance().getSiteBySiteCode(checkMedOrderItem.getRxDrug().getRegimen().getFirstDose().getSiteCode()).getSiteCategoryCode())) {
					continuousInfusionList.add(checkMedOrderItem.getItemNum());
				}
			}
			else if (checkMedOrderItem.getIvRxDrug() != null) {
				checkMedOrderItemList.addAll(constructMedOrderItemListForIvRxDrug(checkMedOrderItem));
				if ("CF".equals(DmSiteCacher.instance().getSiteBySiteCode(checkMedOrderItem.getIvRxDrug().getSiteCode()).getSiteCategoryCode())) {
					continuousInfusionList.add(checkMedOrderItem.getItemNum());
				}
			}
		}
		
		MedOrder medOrder = pharmOrder.getMedOrder();
		
		MdsCriteria mdsCriteria = constructMdsCriteria();

		mdsCriteria.setOrderNum(medOrder.getOrderNum());
		mdsCriteria.setPatientEntity(pharmOrder.getPatient());
		mdsCriteria.setOrderDate(medOrder.getOrderDate());
		mdsCriteria.setOrderType(medOrder.isDhOrder() ? CmsOrderType.Pharmacy : medOrder.getCmsOrderType());
		mdsCriteria.setOrderSubType(medOrder.getCmsOrderSubType());
		mdsCriteria.setCaseNum(caseNum);

		mdsCriteria.setPatHospCode(medOrder.getPatHospCode());
		mdsCriteria.setSpecCode(specCode);

		mdsCriteria.setCheckDdiFlag(checkDdiFlag);
		mdsCriteria.setCheckOnHandDrugFlag(PATIENT_MOE_ENABLED.get() && !StringUtils.isEmpty(pharmOrder.getPatient().getPatKey()));
		mdsCriteria.setCheckPregnancyFlag(orderViewInfo.getPregnancyCheckFlag());
		mdsCriteria.setCheckHlaFlag(false);
		mdsCriteria.setCheckSteroidFlag(checkSteroid);
		mdsCriteria.setMedOrderItemList(checkMedOrderItemList);
		
		mdsCriteria.setUseDrugMdsPropertyHq(pharmOrder.getMedOrder().isMpDischarge() || pharmOrder.getMedOrder().isDhOrder());
		mdsCriteria.setIsDhOrder(medOrder.isDhOrder());
		
		if ( mdsCriteria.getCheckOnHandDrugFlag() ) {
			if ( OnHandSourceType.Corporate.getDataValue().equals(propMap.getValue(VETTING_DRUGONHAND_ONHANDSOURCETYPE.getName())) ) {
				mdsCriteria.setCorpOnHandEnabled(Boolean.TRUE);
			} else {
				mdsCriteria.setCorpOnHandEnabled(Boolean.FALSE);
			}
			mdsCriteria.setOnHandSourceType(medOrder.getOnHandSourceType());
			mdsCriteria.setPasPatientEnabled(propMap.getValueAsBoolean(PATIENT_PAS_PATIENT_ENABLED.getName()));
			
			if ( dispOrder != null && dispOrder.getRefillFlag() ) {
				if ( dispOrder.getSfiFlag() ) {
					mdsCriteria.setOnHandProfileOrderType(OnHandProfileOrderType.SfiOrder);
				} else {
					mdsCriteria.setOnHandProfileOrderType(OnHandProfileOrderType.RefillOrder);
				}
			} else {
				if ( medOrder.isDhOrder() ) {
					mdsCriteria.setOnHandProfileOrderType(OnHandProfileOrderType.DhOrder);
				} else if ( medOrder.getDocType() == MedOrderDocType.Normal ) {
					mdsCriteria.setOnHandProfileOrderType(OnHandProfileOrderType.MedOrder);
				} else {
					mdsCriteria.setOnHandProfileOrderType(OnHandProfileOrderType.ManualOrder);
				}
			}
		}
		
		MdsResult mdsResult = retrieveMdsResultByMdsCriteria(mdsCriteria, ticket, noMappingDhItemList);

		restoreItemNumInAlertMap(mdsResult.getAlertEntityMap());
		
		for (MedOrderItem moi : inMedOrderItemList) {
			Integer itemNum = moi.getItemNum();
			if (!mdsResult.getAlertEntityMap().containsKey(itemNum)) {
				mdsResult.getAlertEntityMap().put(itemNum, new ArrayList<AlertEntity>());
			}
		}
		
		return mdsResult;
	}

	private Map<Integer, MedOrderItem> retrieveUpToDateDhMapping(List<MedOrderItem> medOrderItemList) {
		
		Map<Integer, MedOrderItem> inMap = new HashMap<Integer, MedOrderItem>();
		Map<Integer, MedOrderItem> outMap = new HashMap<Integer, MedOrderItem>();
		
		JaxbWrapper<DhRxDrug> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
		
		List<String> inXmlList = new ArrayList<String>();
		for (MedOrderItem medOrderItem : medOrderItemList) {
			inXmlList.add(rxJaxbWrapper.marshall(medOrderItem.getDhRxDrug()));
			inMap.put(medOrderItem.getDhRxDrug().getItemNum(), medOrderItem);
		}
		
		List<String> outXmlList = dmsPmsServiceProxy.retrieveHaDrugMappingInfoForDhOrder(inXmlList);

		for (String outXml : outXmlList) {
			
			DhRxDrug dhRxDrug = rxJaxbWrapper.unmarshall(outXml);
			
			MedOrderItem inMoi = inMap.get(dhRxDrug.getItemNum());
			
			MedOrderItem outMoi = new MedOrderItem();
			outMoi.setItemNum(inMoi.getItemNum());
			outMoi.setOrgItemNum(inMoi.getOrgItemNum());
			outMoi.setRxItemXml(inMoi.getRxItemXml());
			outMoi.setRxItemType(inMoi.getRxItemType());
			outMoi.setDhRxDrug(dhRxDrug);
			outMoi.setRxDrug(outMoi.getDhRxDrug());
			
			if (!StringUtils.equals(inMoi.getRxDrug().getDisplayName(), dhRxDrug.getDisplayName())) {
				inMoi.getRxDrug().setDisplayName(dhRxDrug.getDisplayName());
				inMoi.setChangeFlag(Boolean.TRUE);
			}
			if (!StringUtils.equals(inMoi.getRxDrug().getFormCode(), dhRxDrug.getFormCode())) {
				inMoi.getRxDrug().setFormCode(dhRxDrug.getFormCode());
				inMoi.setChangeFlag(Boolean.TRUE);
			}
			if (!StringUtils.equals(inMoi.getRxDrug().getFormDesc(), dhRxDrug.getFormDesc())) {
				inMoi.getRxDrug().setFormDesc(dhRxDrug.getFormDesc());
				inMoi.setChangeFlag(Boolean.TRUE);
			}
			if (!StringUtils.equals(inMoi.getRxDrug().getSaltProperty(), dhRxDrug.getSaltProperty())) {
				inMoi.getRxDrug().setSaltProperty(dhRxDrug.getSaltProperty());
				inMoi.setChangeFlag(Boolean.TRUE);
			}
			
			outMap.put(outMoi.getItemNum(), outMoi);
		}
		
		return outMap;
	}

	public MdsResult retrieveAlertByMedProfileItemList(MedProfile medProfile, Collection<MedProfileItem> medProfileItemList, boolean checkDdiFlag) {

		List<MedOrderItem> moiList = new ArrayList<MedOrderItem>();
		for (MedProfileItem medProfileItem : medProfileItemList) {
			
			MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
			
			if (medProfileMoItem.getRxItem().isRxDrug()) {
				RxDrug rxDrug = (RxDrug) medProfileMoItem.getRxItem();
				if (FmStatus.FreeTextEntryItem.getDataValue().equals(rxDrug.getFmStatus())) {
					continue;
				}
				MedOrderItem moi = constructMedOrderItemForNonIvRxDrug(medProfileMoItem);
				if (medProfileMoItem.isManualItem()) {
					if (checkDdiFlag) {
						moi.getRxDrug().setRegimen(createOrderLineRegimen(medProfileMoItem));
					}
					moi.getRxDrug().setItemCode(medProfileMoItem.getMedProfilePoItemList().get(0).getItemCode());
				}
				moiList.add(moi);
			}
			else if (medProfileMoItem.getRxItem().isIvRxDrug()) {
				moiList.addAll(constructMedOrderItemListForIvRxDrug(medProfileMoItem));
			}
		}
		
		MdsCriteria mdsCriteria = constructMdsCriteria();

		if (medProfile.getOrderNum() != null && medProfile.getOrderNum().length() > 3 && StringUtils.isNumeric(medProfile.getOrderNum().substring(3))) {
			mdsCriteria.setOrderNum(medProfile.getOrderNum());
		}
		
		mdsCriteria.setPatientEntity(medProfile.getPatient());
		mdsCriteria.setOrderType(CmsOrderType.Pharmacy);
		mdsCriteria.setOrderSubType(CmsOrderSubType.InPatient);
		
		mdsCriteria.setPatHospCode(medProfile.getPatHospCode());
		mdsCriteria.setSpecCode(medProfile.getSpecCode());
		
		mdsCriteria.setCheckDdiFlag(checkDdiFlag);
		mdsCriteria.setCheckOnHandDrugFlag(false);
		mdsCriteria.setCheckPregnancyFlag(medProfile.getPregnancyCheckFlag());
		mdsCriteria.setCheckHlaFlag(MEDPROFILE_ALERT_HLA_ENABLED.get());
		mdsCriteria.setMedOrderItemList(moiList);

		mdsCriteria.setUseDrugMdsPropertyHq(true);
		
		MdsResult mdsResult = retrieveMdsResultByMdsCriteria(mdsCriteria);
		
		restoreItemNumInAlertMap(mdsResult.getAlertEntityMap());
		
		for (MedProfileItem medProfileItem : medProfileItemList) {
			Integer itemNum = medProfileItem.getMedProfileMoItem().getItemNum();
			if (!mdsResult.getAlertEntityMap().containsKey(itemNum)) {
				mdsResult.getAlertEntityMap().put(itemNum, new ArrayList<AlertEntity>());
			}
		}
		
		return mdsResult;
	}
	
	private void restoreItemNumInAlertMap(Map<Integer, List<AlertEntity>> inAlertMap) {
		
		Map<Integer, List<AlertEntity>> outAlertMap = new HashMap<Integer, List<AlertEntity>>();

		for (Entry<Integer, List<AlertEntity>> entry : inAlertMap.entrySet()) {
			
			Integer itemNum, additiveNum;
			if( entry.getKey().intValue() > MANUAL_ITEM_RANGE ){ // ManualItem
				itemNum     = Integer.valueOf(entry.getKey().intValue() / MANUAL_ITEM_MULTIPLIER);
				additiveNum = null;
			}else if (entry.getKey().intValue() > ADDITIVE_NUM_MULTIPLIER) {
				itemNum     = Integer.valueOf(entry.getKey().intValue() / ADDITIVE_NUM_MULTIPLIER);
				additiveNum = Integer.valueOf(entry.getKey().intValue() % ADDITIVE_NUM_MULTIPLIER);
			}else {
				itemNum = entry.getKey();
				additiveNum = null;
			}
			
			if (!outAlertMap.containsKey(itemNum)) {
				outAlertMap.put(itemNum, new ArrayList<AlertEntity>());
			}
			
			for (AlertEntity alertEntity : entry.getValue()) {
				alertEntity.setAdditiveNum(additiveNum);
				alertEntity.restoreItemNum();
				outAlertMap.get(itemNum).add(alertEntity);
			}
		}
		
		inAlertMap.clear();
		inAlertMap.putAll(outAlertMap);
	}
	
	private MdsResult retrieveMdsResultByMdsCriteria(MdsCriteria mdsCriteria) {
		return retrieveMdsResultByMdsCriteria(mdsCriteria, null, new ArrayList<MedOrderItem>());
	}
	
	private MdsResult retrieveMdsResultByMdsCriteria(MdsCriteria mdsCriteria, Ticket ticket, List<MedOrderItem> noMappingDhItemList) {
		
		MdsResult mdsResult;
		try {
			mdsResult = alertServiceProxy.mdsCheck(mdsCriteria);
			List<ErrorInfo> errorInfoList = new ArrayList<ErrorInfo>();

			if (mdsResult.getOnHandExceptionFlag()) {
				errorInfoList.add(new ErrorInfo(systemMessageManager.retrieveMessageDesc("0514")));
				if (ticket != null) {
					alertAuditManager.drugOnHandServiceUnavailable(mdsCriteria.getOrderNum(), mdsCriteria.getPatientEntity().getHkid(), ticket, mdsCriteria.getCaseNum());
				}
			}

			ErrorInfo skipCheckErrorInfo = constructErrorInfo(
					mdsResult.getSkipAllergyList(),
					mdsResult.getSkipAdrList(),
					mdsResult.getSkipPrescItemList(),
					mdsResult.getSkipOnHandItemList(),
					mdsResult.getChangePrescItemList(),
					mdsResult.getChangeOnHandItemList(),
					noMappingDhItemList
				);
			if (skipCheckErrorInfo != null) {
				errorInfoList.add(skipCheckErrorInfo);
			}
			
			errorInfoList.addAll(mdsResult.getErrorInfoList());
			mdsResult.setErrorInfoList(errorInfoList);
			mdsExceptionInfo = null;
		}
		catch (MdsException e) {
			mdsResult = new MdsResult();
			mdsExceptionInfo = new ErrorInfo();
			alertAuditManager.mdsServiceUnavailable(mdsCriteria.getPatientEntity().getHkid(), ticket, mdsCriteria.getOrderNum(), mdsCriteria.getCaseNum());
		}
		
		mdsLogInfo = mdsResult.getMdsLogInfo();

		return mdsResult;
	}

	private ErrorInfo constructErrorInfo(
			List<String> allergyList,
			List<String> adrList,
			List<MedOrderItem> skipPrescItemList,
			List<MedOrderItem> skipOnHandItemList,
			List<MedOrderItem> changePrescItemList,
			List<MedOrderItem> changeOnHandItemList,
			List<MedOrderItem> noMappingDhItemList) {

		List<String> msgLineList = new ArrayList<String>();

		if (!allergyList.isEmpty()) {
			if (!msgLineList.isEmpty()) {
				msgLineList.add("\n");
			}
			msgLineList.add(systemMessageManager.retrieveMessageDesc("0436"));

			StringBuilder sb = new StringBuilder();
			for (String allergy : allergyList) {
				if (sb.length() > 0) {
					sb.append(", ");
				}
				sb.append(allergy);
			}
			msgLineList.add(sb.toString());
		}

		if (!adrList.isEmpty()) {
			if (!msgLineList.isEmpty()) {
				msgLineList.add("\n");
			}
			msgLineList.add(systemMessageManager.retrieveMessageDesc("0437"));
			
			StringBuilder sb = new StringBuilder();
			for (String adr : adrList) {
				if (sb.length() > 0) {
					sb.append(", ");
				}
				sb.append(adr);
			}
			msgLineList.add(sb.toString());
		}
		
		int drugIndex = 0;
		boolean useNumbering = (skipPrescItemList.size() + skipOnHandItemList.size() + noMappingDhItemList.size()) > 1;
		if (!skipPrescItemList.isEmpty() || !skipOnHandItemList.isEmpty() || !noMappingDhItemList.isEmpty()) {
			if (!msgLineList.isEmpty()) {
				msgLineList.add("\n");
			}
			msgLineList.add(systemMessageManager.retrieveMessageDesc("0438"));

			for (MedOrderItem moi : skipPrescItemList) {
				moi.preSave();
				moi.setCapdRxDrug(null);
				moi.setRxDrug(null);
				moi.postLoad();
				
				String prefix = "";
				if (useNumbering) {
					prefix = String.valueOf(++drugIndex) + ". ";
				}
				msgLineList.add(prefix + moi.getFullDrugDesc());
			}
			for (MedOrderItem moi : skipOnHandItemList) {
				moi.preSave();
				moi.setCapdRxDrug(null);
				moi.setRxDrug(null);
				moi.postLoad();
				
				String prefix = "";
				if (useNumbering) {
					prefix = String.valueOf(++drugIndex) + ". ";
				}
				msgLineList.add(prefix + moi.getFullDrugDesc() + ON_HAND_DRUG_NAME_SUFFIX);
			}
			for (MedOrderItem moi : noMappingDhItemList) {
				String prefix = "";
				if (useNumbering) {
					prefix = String.valueOf(++drugIndex) + ". ";
				}
				msgLineList.add(prefix + moi.getDhRxDrug().getDhDrugDesc() + DH_DRUG_NAME_SUFFIX);
			}
		}
		
		drugIndex = 0;
		useNumbering = (changePrescItemList.size() + changeOnHandItemList.size()) > 1;
		if (!changePrescItemList.isEmpty() || !changeOnHandItemList.isEmpty()) {
			if (!msgLineList.isEmpty()) {
				msgLineList.add("\n");
			}
			msgLineList.add(systemMessageManager.retrieveMessageDesc("0439"));
			
			for (MedOrderItem moi : changePrescItemList) {
				moi.preSave();
				moi.setCapdRxDrug(null);
				moi.setRxDrug(null);
				moi.postLoad();
				
				String prefix = "";
				if (useNumbering) {
					prefix = String.valueOf(++drugIndex) + ". ";
				}
				msgLineList.add(prefix + moi.getFullDrugDesc());
			}
			for (MedOrderItem moi : changeOnHandItemList) {
				moi.preSave();
				moi.setCapdRxDrug(null);
				moi.setRxDrug(null);
				moi.postLoad();
				
				String prefix = "";
				if (useNumbering) {
					prefix = String.valueOf(++drugIndex) + ". ";
				}
				msgLineList.add(prefix + moi.getFullDrugDesc() + ON_HAND_DRUG_NAME_SUFFIX);
			}
		}
		
		if (msgLineList.isEmpty()) {
			return null;
		}

		StringBuilder sb = new StringBuilder();
		for (String msgLine : msgLineList) {
			if (sb.length() > 0) {
				sb.append('\n');
			}
			sb.append(msgLine);
		}
		
		return new ErrorInfo(
				sb.toString(),
				systemMessageManager.retrieveMessageDesc("0440")
			);
	}
	
	private MedOrderItem constructMedOrderItemForNonIvRxDrug(MedProfileMoItem medProfileMoItem) {
		
		MedOrderItem medOrderItem = new MedOrderItem();

		Integer itemNum;
		if( medProfileMoItem.isManualItem() ){
			itemNum = Integer.valueOf(medProfileMoItem.getItemNum().intValue() * MANUAL_ITEM_MULTIPLIER);
		}else{
			itemNum = medProfileMoItem.getItemNum();
		}
		medOrderItem.setItemNum(itemNum);
		medOrderItem.setOrgItemNum(itemNum);
		medOrderItem.setRxItemType(medProfileMoItem.getRxItemType());
		medOrderItem.setRxDrug((RxDrug)medProfileMoItem.getRxItem());
		medOrderItem.preSave();
		medOrderItem.setRxDrug(null);
		medOrderItem.postLoad();
		
		return medOrderItem;
	}
	
	private MedOrderItem constructMedOrderItemForNonIvRxDrug(MedOrderItem inMoi) {
		
		MedOrderItem outMoi = new MedOrderItem();
		
		outMoi.setItemNum(inMoi.getItemNum());
		outMoi.setOrgItemNum(inMoi.getOrgItemNum());
		outMoi.setRxItemXml(inMoi.getRxItemXml());
		outMoi.setRxItemType(inMoi.getRxItemType());
		outMoi.setRxDrug(inMoi.getRxDrug());
		
		return outMoi;
	}
	
	private List<MedOrderItem> constructMedOrderItemListForIvRxDrug(MedProfileMoItem medProfileMoItem) {
		
		List<MedOrderItem> moiList = new ArrayList<MedOrderItem>();
		
		IvRxDrug ivRxDrug = (IvRxDrug) medProfileMoItem.getRxItem();
		
		for (IvAdditive ivAdditive : ivRxDrug.getIvAdditiveList()) {
			if (ivAdditive.getRegimen() == null) {
				ivAdditive.setRegimen(constructRegimen(ivRxDrug, ivAdditive));
			}
			MedOrderItem medOrderItem = new MedOrderItem();
			
			Integer itemNum = Integer.valueOf(medProfileMoItem.getItemNum().intValue() * ADDITIVE_NUM_MULTIPLIER + ivAdditive.getAdditiveNum().intValue());
			
			medOrderItem.setItemNum(itemNum);
			medOrderItem.setOrgItemNum(itemNum);
			medOrderItem.setRxDrug(ivAdditive);
			moiList.add(medOrderItem);
		}

		return moiList;
	}
	
	private List<MedOrderItem> constructMedOrderItemListForIvRxDrug(MedOrderItem inMoi) {
		
		List<MedOrderItem> outMoiList = new ArrayList<MedOrderItem>();
		
		IvRxDrug ivRxDrug = inMoi.getIvRxDrug();
		
		for (IvAdditive ivAdditive : ivRxDrug.getIvAdditiveList()) {
			if (ivAdditive.getRegimen() == null) {
				ivAdditive.setRegimen(constructRegimen(ivRxDrug, ivAdditive));
			}
			MedOrderItem outMoi = new MedOrderItem();
			
			Integer itemNum = Integer.valueOf(inMoi.getItemNum().intValue() * ADDITIVE_NUM_MULTIPLIER + ivAdditive.getAdditiveNum().intValue());
			
			outMoi.setItemNum(itemNum);
			outMoi.setOrgItemNum(itemNum);
			outMoi.setRxDrug(ivAdditive);
			outMoiList.add(outMoi);
		}
		
		return outMoiList;
	}
	
	private Regimen constructRegimen(IvRxDrug ivRxDrug, IvAdditive ivAdditive) {
		
		Regimen regimen = new Regimen();
		regimen.setType(RegimenType.Daily);
		
		DoseGroup doseGroup = new DoseGroup();
		Dose dose = new Dose();
		doseGroup.setDoseList(Arrays.asList(dose));
		regimen.setDoseGroupList(Arrays.asList(doseGroup));
		
		dose.setDailyFreq(ivRxDrug.getDailyFreq());
		dose.setDosage(ivAdditive.getDosage());
		dose.setDosageUnit(ivAdditive.getDosageUnit());
		doseGroup.setDuration(ivRxDrug.getDuration());
		doseGroup.setDurationUnit(ivRxDrug.getDurationUnit());
		
		return regimen;
	}
	
	private MdsCriteria constructMdsCriteria() {
		
		MdsCriteria mdsCriteria = new MdsCriteria();
		
		mdsCriteria.setHospCode(workstation.getWorkstore().getHospCode());
		mdsCriteria.setWorkstoreCode(workstation.getWorkstore().getWorkstoreCode());
		
		mdsCriteria.setUserId(identity.getCredentials().getUsername());
		mdsCriteria.setWorkstationId(workstation.getHostName());
		
		mdsCriteria.setCheckAlertProfile(ALERT_PROFILE_ENABLED.get(false));
		mdsCriteria.setDdiMaxSeverityLevel(ALERT_MDS_DDI_SEVERITY_LEVEL_MAX.get());
		mdsCriteria.setG6pdCode(ALERT_MDS_G6PD_CODE.get());
		mdsCriteria.setG6pdConditionId(ALERT_MDS_G6PD_CONDITIONID.get());
		mdsCriteria.setG6pdMaxSeverityLevel(ALERT_MDS_G6PD_SEVERITY_LEVEL_MAX.get());
		mdsCriteria.setPregnancyConditionId(ALERT_MDS_PREGNANCY_CONDITIONID.get());
		mdsCriteria.setPregnancyMaxSeverityLevel(ALERT_MDS_PREGNANCY_SEVERITY_LEVEL_MAX.get());
		
		mdsCriteria.setShowLogFlag(ALERT_MDS_LOG_VISIBLE.get(Boolean.FALSE));
		mdsCriteria.setPasPatientEnabled(PATIENT_PAS_PATIENT_ENABLED.get(Boolean.FALSE));
		
		return mdsCriteria;
	}
	
	private DispOrder getDispOrder() {
		return (DispOrder) Contexts.getSessionContext().get("dispOrder");
	}
	
	private PharmOrder getPharmOrder() {
		return (PharmOrder) Contexts.getSessionContext().get("pharmOrder");
	}
	
	private OrderViewInfo getOrderViewInfo() {
		return (OrderViewInfo) Contexts.getSessionContext().get("orderViewInfo");
	}

	public MdsResult retrieveMpAlert(Collection<MedProfileItem> medProfileItems, MedProfile medProfile, boolean checkDdi) {
		
		mdsLogInfo = null;
		
		List<MedOrderItem> moiList = new ArrayList<MedOrderItem>();
		
		for (MedProfileItem medProfileItem : medProfileItems) {
			if (medProfileItem.getStatus() == MedProfileItemStatus.Deleted || medProfileItem.getStatus() == MedProfileItemStatus.Discontinued) {
				continue;
			}
			MedProfileMoItem medProfileMoItem = medProfileItem.getMedProfileMoItem();
			if (medProfileMoItem.getRxItem().isCapdRxDrug()) {
				continue;
			}
			if (FmStatus.FreeTextEntryItem.getDataValue().equals(((RxDrug) medProfileMoItem.getRxItem()).getFmStatus())) {
				continue;
			}
			MedOrderItem moi = constructMedOrderItemForNonIvRxDrug(medProfileMoItem);
			if (checkDdi) {
				moi.getRxDrug().setRegimen(createOrderLineRegimen(medProfileMoItem));
			}
			moi.setManualProfileFlag(medProfileMoItem.getItemNum() >= 10000);
			if (moi.getManualProfileFlag()) {
				moi.setFmStatus(FmStatus.GeneralDrug.getDataValue());
			}
			moiList.add(moi);
		}
		
		MdsCriteria mdsCriteria = constructMdsCriteria();
		
		if (medProfile.getOrderNum() != null && medProfile.getOrderNum().length() > 3 && StringUtils.isNumeric(medProfile.getOrderNum().substring(3))) {
			mdsCriteria.setOrderNum(medProfile.getOrderNum());
		}
		
		mdsCriteria.setPatientEntity(medProfile.getPatient());
		mdsCriteria.setOrderType(CmsOrderType.Pharmacy);
		mdsCriteria.setOrderSubType(CmsOrderSubType.InPatient);
		
		mdsCriteria.setPatHospCode(medProfile.getPatHospCode());
		mdsCriteria.setSpecCode(medProfile.getSpecCode());

		mdsCriteria.setCheckDdiFlag(checkDdi);
		mdsCriteria.setCheckOnHandDrugFlag(false);
		mdsCriteria.setCheckPregnancyFlag(medProfile.getPregnancyCheckFlag());
		mdsCriteria.setCheckHlaFlag(false);
		mdsCriteria.setMedOrderItemList(moiList);
		mdsCriteria.setFilterContinuousInfusionDdi(false);
		
		mdsCriteria.setUseDrugMdsPropertyHq(true);
		
		MdsResult mdsResult = retrieveMdsResultByMdsCriteria(mdsCriteria);
		
		restoreItemNumInAlertMap(mdsResult.getAlertEntityMap());
		
		if (checkDdi) {
			for (MedProfileItem medProfileItem : medProfileItems) {
				Integer itemNum = medProfileItem.getMedProfileMoItem().getItemNum();
				if (!mdsResult.getAlertEntityMap().containsKey(itemNum)) {
					mdsResult.getAlertEntityMap().put(itemNum, new ArrayList<AlertEntity>());
				}
			}
		}
		return mdsResult;
	}

	private Regimen createOrderLineRegimen(MedProfileMoItem medProfileMoItem) {

		MedProfilePoItem medProfilePoItem = medProfileMoItem.getMedProfilePoItemList().get(0);
		Regimen regimen = medProfilePoItem.getRegimen();
		
		if (regimen == null || regimen.getType() != RegimenType.Daily || regimen.getDoseGroupList().size() != 1) {
			return null;
		}
		
		JaxbWrapper<Regimen> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
		regimen = rxJaxbWrapper.unmarshall(rxJaxbWrapper.marshall(regimen));
		
		DoseGroup doseGroup = regimen.getDoseGroupList().get(0);
		if (doseGroup.getDoseList().size() != 1) {
			return null;
		}
		Dose dose = doseGroup.getDoseList().get(0);

		DmDrug dmDrug = DmDrugCacher.instance().getDmDrug(medProfilePoItem.getItemCode());
		Double dduToMduRatio = dmDrug.getDmMoeProperty().getDduToMduRatio();

		BigDecimal ratio = dduToMduRatio == null ? null : BigDecimal.valueOf(dduToMduRatio);
		if (ratio == null || ratio.equals(BigDecimal.ZERO) || dose == null || dose.getDosage() == null) {
			return null;
		}

		dose.setDosage(dose.getDosage().multiply(ratio));
		dose.setDosageUnit(dmDrug.getDmMoeProperty().getMoDosageUnit());
		
		return regimen;
	}
}
