package hk.org.ha.model.pms.persistence.phs;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.RecordStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "SUPPLIER")
public class Supplier extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "SUPPLIER_CODE", length = 4)
	private String supplierCode;

	@Column(name = "NAME", length = 100)
	private String name;

	@Column(name = "NAME_CHI", length = 100)
	private String nameChi;

	@Column(name = "CAPD_PHONE", length = 13)
	private String capdPhone;

	@Converter(name = "Supplier.status", converterClass = RecordStatus.Converter.class)
    @Convert("Supplier.status")
	@Column(name="STATUS", nullable = false, length = 1)
	private RecordStatus status;
	
	public Supplier() {
		status = RecordStatus.Active;
	}
	
	public String getSupplierCode() {
		return supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameChi() {
		return nameChi;
	}

	public void setNameChi(String nameChi) {
		this.nameChi = nameChi;
	}

	public String getCapdPhone() {
		return capdPhone;
	}

	public void setCapdPhone(String capdPhone) {
		this.capdPhone = capdPhone;
	}
	
	public RecordStatus getStatus() {
		return status;
	}

	public void setStatus(RecordStatus status) {
		this.status = status;
	}
}
