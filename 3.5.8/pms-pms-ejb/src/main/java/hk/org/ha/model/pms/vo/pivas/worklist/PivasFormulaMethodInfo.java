package hk.org.ha.model.pms.vo.pivas.worklist;

import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaMethod;
import hk.org.ha.model.pms.vo.pivas.PivasDiluentInfo;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PivasFormulaMethodInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private PivasFormulaMethod pivasFormulaMethod;
	
	private List<PivasDiluentInfo> solventPivasDiluentInfoList;

	private List<PivasDiluentInfo> diluentPivasDiluentInfoList;
	
	public PivasFormulaMethod getPivasFormulaMethod() {
		return pivasFormulaMethod;
	}

	public void setPivasFormulaMethod(PivasFormulaMethod pivasFormulaMethod) {
		this.pivasFormulaMethod = pivasFormulaMethod;
	}

	public List<PivasDiluentInfo> getSolventPivasDiluentInfoList() {
		return solventPivasDiluentInfoList;
	}

	public void setSolventPivasDiluentInfoList(List<PivasDiluentInfo> solventPivasDiluentInfoList) {
		this.solventPivasDiluentInfoList = solventPivasDiluentInfoList;
	}

	public List<PivasDiluentInfo> getDiluentPivasDiluentInfoList() {
		return diluentPivasDiluentInfoList;
	}

	public void setDiluentPivasDiluentInfoList(List<PivasDiluentInfo> diluentPivasDiluentInfoList) {
		this.diluentPivasDiluentInfoList = diluentPivasDiluentInfoList;
	}
}