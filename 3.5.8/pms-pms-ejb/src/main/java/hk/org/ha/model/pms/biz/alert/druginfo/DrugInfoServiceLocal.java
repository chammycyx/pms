package hk.org.ha.model.pms.biz.alert.druginfo;

import hk.org.ha.model.pms.asa.exception.alert.druginfo.DrugInfoException;
import hk.org.ha.model.pms.dms.vo.DrugMds;
import hk.org.ha.model.pms.dms.vo.DrugName;
import hk.org.ha.model.pms.dms.vo.PreparationProperty;
import hk.org.ha.model.pms.dms.vo.RouteForm;
import hk.org.ha.model.pms.vo.alert.druginfo.ClassificationNode;
import hk.org.ha.model.pms.vo.alert.druginfo.DrugInteraction;
import hk.org.ha.model.pms.vo.alert.druginfo.FoodInteraction;
import hk.org.ha.model.pms.vo.alert.druginfo.PatientEducation;
import hk.org.ha.model.pms.vo.alert.mds.RetrievePreparationPropertyListResult;
import hk.org.ha.model.pms.vo.rx.DhRxDrug;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface DrugInfoServiceLocal {

	List<ClassificationNode> retrieveDrugClassificationList(Integer gcnSeqNum, Integer rdfgenId, Integer rgenId, String patHospCode, List<PreparationProperty> preparationPropertyList) throws DrugInfoException;
	
	String retrieveSideEffect(Integer gcnSeqNum, Integer rdfgenId, Integer rgenId, String patHospCode, List<PreparationProperty> preparationPropertyList) throws DrugInfoException;
	
	String retrieveContraindication(Integer gcnSeqNum, Integer rdfgenId, Integer rgenId, String patHospCode, List<PreparationProperty> preparationPropertyList) throws DrugInfoException;
	
	String retrievePrecaution(Integer gcnSeqNum, Integer rdfgenId, Integer rgenId, String patHospCode, List<PreparationProperty> preparationPropertyList) throws DrugInfoException;
	
	List<DrugInteraction> retrieveDrugInteraction(Integer gcnSeqNum, Integer rdfgenId, Integer rgenId, String patHospCode, List<PreparationProperty> preparationPropertyList) throws DrugInfoException;
	
	List<FoodInteraction> retrieveFoodInteraction(Integer gcnSeqNum, Integer rdfgenId, Integer rgenId, String patHospCode, List<PreparationProperty> preparationPropertyList) throws DrugInfoException;
	
	List<PatientEducation> retrievePatientEducation(Integer gcnSeqNum, Integer rdfgenId, Integer rgenId, String patHospCode, List<PreparationProperty> preparationPropertyList) throws DrugInfoException;
	
	String retrieveCommonOrder(Integer gcnSeqNum, Integer rdfgenId, Integer rgenId, String patHospCode, List<PreparationProperty> preparationPropertyList) throws DrugInfoException;
	
	String retrieveDosageRange(Integer gcnSeqNum, Integer rdfgenId, Integer rgenId, String patHospCode, Integer age, String ageUnit, List<PreparationProperty> preparationPropertyList, Integer selectedStrengthIndex) throws DrugInfoException;
	
	String getRenalAdjCode();
	
	Date getRenalReviewDate();
	
	DrugMds retrieveDrugMdsByItemCode(String itemCode, boolean checkHq, boolean checkRenal);
	
	DrugMds retrieveDrugMdsByDisplayNameFormSaltFmStatus(String displayName, String formCode, String saltProperty, String fmStatus, boolean checkHq, boolean checkRenal);
	
	DrugMds retrieveDrugMdsByDhRxDrug(DhRxDrug dhRxDrug, boolean checkHq, boolean checkRenal);
	
	List<PreparationProperty> retrievePreparationPropertyListForOrderEntry(String displayName, String formCode, String saltProperty, String fmStatus, String aliasName, String firstDisplayName, Boolean checkHq);
	
	List<PreparationProperty> retrievePreparationPropertyListForDrugSearch(DrugName drugName, RouteForm routeForm, PreparationProperty preparation);
	
	RetrievePreparationPropertyListResult retrievePreparationForDhOrder(DhRxDrug dhRxDrug);

	String retrieveMonograph(Integer monoId, Integer monoType, String monoVersion, String patHospCode) throws DrugInfoException;
	
	void destroy();
}
