package hk.org.ha.model.pms.udt.report;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum ReportMonth implements StringValuedEnum {

	Jan("1","JAN"),
	Feb("2","FEB"),
	Mar("3","MAR"),
	Apr("4","APR"),
	May("5","MAY"),
	Jun("6","JUN"),
	Jul("7","JUL"),
	Aug("8","AUG"),
	Sep("9","SEP"),
	Oct("10","OCT"),
	Nov("11","NOV"),
	Dec("12","DEC");
	
    private final String dataValue;
    private final String displayValue;

    ReportMonth(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static ReportMonth dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(ReportMonth.class, dataValue);
	}
        
	public static class Converter extends StringValuedEnumConverter<ReportMonth> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<ReportMonth> getEnumClass() {
    		return ReportMonth.class;
    	}
    }
}
