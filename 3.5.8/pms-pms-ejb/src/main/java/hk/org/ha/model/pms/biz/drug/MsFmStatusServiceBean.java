package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.MsFmStatusCacher;
import hk.org.ha.model.pms.dms.persistence.MsFmStatus;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("msFmStatusService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MsFmStatusServiceBean implements MsFmStatusServiceLocal {

	@Logger
	private Log logger;

	@In
	private MsFmStatusCacher msFmStatusCacher;

	@Out(required = false)
	private MsFmStatus msFmStatus;
	
	private boolean retrieveSuccess;
	
	public void retrieveMsFmStatus(String fmStatus)
	{
		logger.debug("retrieveMsFmStatus #0", fmStatus);

		retrieveSuccess = false;		
		msFmStatus = msFmStatusCacher.getMsFmStatusByFmStatus(fmStatus);
		if (msFmStatus != null) {
			retrieveSuccess = true;
		}
		
		logger.debug("retrieveMsFmStatus result #0", retrieveSuccess);
	}

	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}
	
	@Remove
	public void destroy() 
	{
		if (msFmStatus != null) {
			msFmStatus = null;
		}
	}

}
