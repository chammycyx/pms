package hk.org.ha.model.pms.biz.reftable.mp;

import javax.ejb.Local;

@Local
public interface MpDosageConversionRptListServiceLocal {

	void genMpDosageConversionRptList();
	
	void exportMpDosageConversionRpt();
	
	void printMpDosageConversionRpt();
	
	void destroy();
}
