package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;

public class RefillCouponHdr implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String hospitalNameEng;
	
	private String hospitalNameChi;
	
	private Boolean reprintFlag;
	
	private String patName;
	
	private String patNameChi;
	
	private String caseNum;
	
	private String hkid;
	
	private String sex;
	
	private String age;
	
	private String specCode;
	
	private Date dispDate;
	
	private String ticketNum;
	
	private String workstoreCode;
	
	private Date orgDispDate;
	
	private String orgWorkstoreCode;
	
	private String orgTicketNum;
	
	private Integer refillNum;
	
	private Integer numOfRefill;
	
	private Integer groupNum;
	
	private Integer numOfGroup;
	
	private String doctorCode;
	
	private Date refillDate;
	
	private Date nextApptDate;
	
	private boolean showChargeInfo;
	
	private Boolean drsFlag;
	
	public String getHospitalNameEng() {
		return hospitalNameEng;
	}

	public void setHospitalNameEng(String hospitalNameEng) {
		this.hospitalNameEng = hospitalNameEng;
	}

	public String getHospitalNameChi() {
		return hospitalNameChi;
	}

	public void setHospitalNameChi(String hospitalNameChi) {
		this.hospitalNameChi = hospitalNameChi;
	}

	public Boolean getReprintFlag() {
		return reprintFlag;
	}

	public void setReprintFlag(Boolean reprintFlag) {
		this.reprintFlag = reprintFlag;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getPatNameChi() {
		return patNameChi;
	}

	public void setPatNameChi(String patNameChi) {
		this.patNameChi = patNameChi;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public Date getDispDate() {
		return dispDate;
	}

	public void setDispDate(Date dispDate) {
		this.dispDate = dispDate;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public Date getOrgDispDate() {
		return orgDispDate;
	}

	public void setOrgDispDate(Date orgDispDate) {
		this.orgDispDate = orgDispDate;
	}

	public String getOrgWorkstoreCode() {
		return orgWorkstoreCode;
	}

	public void setOrgWorkstoreCode(String orgWorkstoreCode) {
		this.orgWorkstoreCode = orgWorkstoreCode;
	}

	public String getOrgTicketNum() {
		return orgTicketNum;
	}

	public void setOrgTicketNum(String orgTicketNum) {
		this.orgTicketNum = orgTicketNum;
	}

	public Integer getRefillNum() {
		return refillNum;
	}

	public void setRefillNum(Integer refillNum) {
		this.refillNum = refillNum;
	}

	public Integer getNumOfRefill() {
		return numOfRefill;
	}

	public void setNumOfRefill(Integer numOfRefill) {
		this.numOfRefill = numOfRefill;
	}

	public Integer getGroupNum() {
		return groupNum;
	}

	public void setGroupNum(Integer groupNum) {
		this.groupNum = groupNum;
	}

	public Integer getNumOfGroup() {
		return numOfGroup;
	}

	public void setNumOfGroup(Integer numOfGroup) {
		this.numOfGroup = numOfGroup;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public Date getRefillDate() {
		return refillDate;
	}

	public void setRefillDate(Date refillDate) {
		this.refillDate = refillDate;
	}

	public Date getNextApptDate() {
		return nextApptDate;
	}

	public void setNextApptDate(Date nextApptDate) {
		this.nextApptDate = nextApptDate;
	}

	public void setShowChargeInfo(boolean showChargeInfo) {
		this.showChargeInfo = showChargeInfo;
	}

	public boolean isShowChargeInfo() {
		return showChargeInfo;
	}

	public Boolean getDrsFlag() {
		return drsFlag;
	}

	public void setDrsFlag(Boolean drsFlag) {
		this.drsFlag = drsFlag;
	}
}
