
package hk.org.ha.model.pms.biz.onestop;

import static hk.org.ha.model.pms.prop.Prop.CHARGING_SFI_SUSPEND_ORDER_SUSPENDCODE;
import static hk.org.ha.model.pms.prop.Prop.ONESTOP_MEDCASE_PASSPECCODE_UNKNOWN_CHECK;
import static hk.org.ha.model.pms.prop.Prop.ONESTOP_MEDCASE_PASWARDCODE_UNKNOWN_CHECK;
import static hk.org.ha.model.pms.prop.Prop.ONESTOP_PATIENT_PAS_APPT_DURATION;
import static hk.org.ha.model.pms.prop.Prop.ONESTOP_TICKET_TICKETNUM_PREFIX;
import static hk.org.ha.model.pms.prop.Prop.ONESTOP_SPECMAPPING_PATHOSPCODE_ENABLED;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.biz.onestop.OneStopHelper;
import hk.org.ha.model.pms.dms.persistence.PmsSpecMapping;
import hk.org.ha.model.pms.persistence.corp.HospitalMapping;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.persistence.phs.Specialty;
import hk.org.ha.model.pms.persistence.phs.SpecialtyPK;
import hk.org.ha.model.pms.persistence.phs.Ward;
import hk.org.ha.model.pms.persistence.phs.WardPK;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.RecordStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderDayEndStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.InvoiceDocType;
import hk.org.ha.model.pms.udt.disp.MedCasePasPatInd;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderPatType;
import hk.org.ha.model.pms.udt.disp.RemarkStatus;
import hk.org.ha.model.pms.udt.onestop.AllowCommentType;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderStatus;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderStatusIndicator;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
import hk.org.ha.model.pms.udt.refill.DocType;
import hk.org.ha.model.pms.udt.refill.RefillScheduleRefillStatus;
import hk.org.ha.model.pms.udt.refill.RefillScheduleStatus;
import hk.org.ha.model.pms.vo.onestop.OneStopOrderItemSummary;
import hk.org.ha.model.pms.vo.onestop.OneStopRefillItemSummary;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.ejb.Remove;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.beanutils.BeanUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;

@AutoCreate
@Stateless
@Name("oneStopUtils")
@MeasureCalls
public class OneStopUtilsBean implements OneStopUtilsLocal {
	
	private final static String TICKET_NUM = "ticketNum";
	private final static String START_DATE = "startDate";
	private final static String END_DATE = "endDate";
	private final static String ERROR_MESSAGE = "errMsg";
	private final static String PHS_SPECIALTY = "phsSpecailty";
	private final static String PHS_WARD = "phsWard";
	
	private static final Pattern ticketWithoutCheckDigitPattern = Pattern.compile("^[0-9]+$");                                                                           
	private static final Pattern ticketWithCheckDigitPattern = Pattern.compile("^9[0-9]+$");                                                                             
	private static final Pattern orderNumPattern1 = Pattern.compile("^MOE[A-Za-z][A-Za-z][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$");                               
	private static final Pattern orderNumPattern2 = Pattern.compile("^MOE[A-Za-z][A-Za-z][A-Za-z ][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$");                      
	private static final Pattern orderNumWithCheckDigitPattern1 = Pattern.compile("^MOE[A-Za-z][A-Za-z][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$");            
	private static final Pattern orderNumWithCheckDigitPattern2 = Pattern.compile("^MOE[A-Za-z][A-Za-z][A-Za-z ][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$");		
	private static final Pattern refillNumPattern1 = Pattern.compile("^[A-Za-z][A-Za-z][0-9]+$");                                                                        
	private static final Pattern refillNumPattern2 = Pattern.compile("^[A-Za-z][A-Za-z][A-Za-z ][0-9]+$");                                                               
	private static final Pattern ipCaseNumPattern1 = Pattern.compile("^AE[0-9]+[A-Za-z0-9]$");                                                                           
	private static final Pattern ipCaseNumPattern2 = Pattern.compile("^HN[0-9]+[A-Za-z0-9]$");                                                                           
		
	@PersistenceContext
	private EntityManager em;
	
	@Logger
	private Log logger;
	
	@In
	private Workstore workstore;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private OneStopHelper oneStopHelper;
	
	private DateFormat pasDateFormat = new SimpleDateFormat("yyyyMMdd 00:00"); 

	private Map<String, Pattern> ticketPatternMap = new HashMap<String, Pattern>();

	private Pattern getTicketPattern() {
		String ticketNumPrefix = ONESTOP_TICKET_TICKETNUM_PREFIX.get();
		Pattern ticketPattern = ticketPatternMap.get(ticketNumPrefix);		
		if (ticketPattern == null) {
			ticketPattern = Pattern.compile("^[" + ticketNumPrefix + "][0-9][0-9][0-9][0-9]$");
			ticketPatternMap.put(ticketNumPrefix, ticketPattern);
		}
		return ticketPattern;
	}
	
	public String convertOrderNum(String orderNum) 
	{
		if (orderNum.length() == 14 && orderNumPattern1.matcher(orderNum).find()) 
		{
			return orderNum.substring(3,5) + " " + orderNum.substring(5);
		}

		if (orderNum.length() == 15 && (orderNumPattern2.matcher(orderNum).find() || orderNumWithCheckDigitPattern1.matcher(orderNum).find())) 
		{
			if (orderNumWithCheckDigitPattern1.matcher(orderNum).find()) 
			{
				return orderNum.substring(3,5) + " " + orderNum.substring(5, 14);
			}

			return orderNum.substring(3);
		}
		
		if (orderNum.length() == 16 && orderNumWithCheckDigitPattern2.matcher(orderNum).find())
		{
			return orderNum.substring(3,15);
		}
		
		return orderNum;
	}
		
	public List<String> retrieveFriendClubListAndDefaultPatHospCode() {
		List<String> friendClubList = new ArrayList<String>();
		friendClubList.add(workstore.getDefPatHospCode());
		for (HospitalMapping hospitalMapping : workstore.getHospital().getHospitalMappingList()) 
		{
			friendClubList.add(hospitalMapping.getPatHospCode());
		}
		
		return friendClubList;
	}
	
	public Boolean isInPatCase(String caseNum) 
	{
		if (ipCaseNumPattern1.matcher(caseNum).find() || ipCaseNumPattern2.matcher(caseNum).find()) 
		{
			return true;
		}
		
		return false;
	}
	
	public Map<String, Object> retrieveTicketParam(String ticketNum) 
	{		
		Map<String, Object> ticketParm = new HashMap<String, Object>();
		
		try 
		{
			if (ticketNum.length() == 5 && this.getTicketPattern().matcher(ticketNum).find()) 
			{//one date
				ticketParm.put(TICKET_NUM, ticketNum.substring(1));
				ticketParm.put(START_DATE, new DateTime().toDateMidnight().toDate());
				ticketParm.put(END_DATE, new DateTime().plusDays(1).toDateMidnight().toDate());
			}
			else if (ticketNum.length() == 12 && ticketWithoutCheckDigitPattern.matcher(ticketNum).find()) 
			{//current year
				ticketParm.put(TICKET_NUM, ticketNum.substring(8));
				ticketParm.put(START_DATE, new DateTime(new DateTime().getYear() + "-" + ticketNum.substring(0,2) + "-" + ticketNum.substring(2,4)).toDateMidnight().toDate());
				ticketParm.put(END_DATE, new DateTime(new DateTime().getYear() + "-" + ticketNum.substring(0,2) + "-" + ticketNum.substring(2,4)).plusDays(1).toDateMidnight().toDate());
			}
			else if (ticketNum.length() == 14 && ticketWithCheckDigitPattern.matcher(ticketNum).find()) 
			{//current year
				ticketParm.put(TICKET_NUM, ticketNum.substring(10));
				ticketParm.put(START_DATE, new DateTime(new DateTime().getYear() + "-" + ticketNum.substring(2,4) + "-" + ticketNum.substring(4,6)).toDateMidnight().toDate());
				ticketParm.put(END_DATE, new DateTime(new DateTime().getYear() + "-" + ticketNum.substring(2,4) + "-" + ticketNum.substring(4,6)).plusDays(1).toDateMidnight().toDate());
			}
			else if (ticketNum.length() == 14 && ticketWithoutCheckDigitPattern.matcher(ticketNum).find()) 
			{//one year
				ticketParm.put(TICKET_NUM, ticketNum.substring(8,12));
				ticketParm.put(START_DATE, new DateTime(ticketNum.substring(4,8) + "-" + ticketNum.substring(2,4) + "-" + ticketNum.substring(0,2)).toDateMidnight().toDate());
				ticketParm.put(END_DATE, new DateTime(ticketNum.substring(4,8) + "-" + ticketNum.substring(2,4) + "-" + ticketNum.substring(0,2)).plusDays(1).toDateMidnight().toDate());
			}
		} 
		catch (RuntimeException e) 
		{
			ticketParm.put(ERROR_MESSAGE, "0034");	
		}

		return ticketParm;
	}
	
	public String convertRefillNum(String refillNum) 
	{
		if (refillNum.length() == 13 && !isInPatCase(refillNum)) 
		{
			if (refillNumPattern1.matcher(refillNum).find()) 
			{
				return refillNum + "01";
			}
		}
		
		if (refillNum.length() == 14 && refillNumPattern2.matcher(refillNum).find()) 
		{
			return refillNum + "01";
		}
		
		return refillNum;
	}
	
	public String startDate(Boolean displayAllMoe) {
		if (!displayAllMoe) {
			return pasDateFormat.format(new DateTime().minusDays(ONESTOP_PATIENT_PAS_APPT_DURATION.get()).toDate());
		}
		return "";
	}
	
	public String endDate(Boolean displayAllMoe) {
		if (!displayAllMoe) {
			return pasDateFormat.format(new DateTime().plusDays(1).toDate());
		}
		return "";
	}
	
	private Workstation retrieveLockUserDetails(MedOrder medOrder) 
	{
		return em.find(Workstation.class, medOrder.getWorkstationId());
	}
	
	public OneStopOrderItemSummary convertSummaryFromUnvetOrder(MedOrder medOrder) 
	{	
		OneStopOrderItemSummary oneStopOrderItemSummary = oneStopHelper.convertSummaryFromUnvetOrder(medOrder);
		
		if (medOrder.getRemarkStatus() == RemarkStatus.Unconfirm) 
		{
			oneStopOrderItemSummary.setPharmcyRemark(OneStopOrderStatusIndicator.PharmacyRemark);
			if (medOrder.getRemarkCreateDate() != null)
			{
				oneStopOrderItemSummary.setRemarkText(medOrder.getRemarkText());
				oneStopOrderItemSummary.setAllowReversePharmcyRemark(true);
			}
		} 
		else if (medOrder.getRemarkStatus() == RemarkStatus.Confirm) 
		{
			oneStopOrderItemSummary.setRemarkText(medOrder.getRemarkText());
			oneStopOrderItemSummary.setPharmcyRemark(OneStopOrderStatusIndicator.PharmacyremarkConfirm);
		}
		
		if (medOrder.getProcessingFlag()) 
		{
			oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.ProcessingByPharmacy);
			Workstation locker = retrieveLockUserDetails(medOrder);
			oneStopOrderItemSummary.setLockWorkstationName(locker.getHostName());
			
			if (locker.getWorkstore().getWorkstoreCode().equals(workstore.getWorkstoreCode()) && locker.getWorkstore().getHospCode().equals(workstore.getHospCode())) 
			{
				oneStopOrderItemSummary.setUnlockOrder(true);
			} 
			else 
			{
				oneStopOrderItemSummary.setUnlockOrder(false);
			}
			
		} 
		else if (medOrder.getStatus() == MedOrderStatus.Deleted) 
		{
			oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.Cancelled);
			oneStopOrderItemSummary.setRemarkConfirmDate(medOrder.getRemarkConfirmDate());
			if (medOrder.getRefPharmOrderId() != null)
			{
				retrieveVettedDrAndPhsInfo(medOrder.getRefPharmOrderId(), oneStopOrderItemSummary);
			}
		} 
		else if (medOrder.getRemarkStatus() != RemarkStatus.None)
		{
			oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.Vetted);
			if (medOrder.getRefPharmOrderId() != null)
			{
				retrieveVettedDrAndPhsInfo(medOrder.getRefPharmOrderId(), oneStopOrderItemSummary);
			}
		}
		else if (medOrder.getStatus() == MedOrderStatus.Withhold)
		{
			oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.Withhold);
		}
		else if (medOrder.getStatus() == MedOrderStatus.PreVet)
		{
			oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.PreVet);
		}
		else 
		{
			oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.Unvet);
		}
		
		if (medOrder.getTicket() != null) 
		{
			if (oneStopOrderItemSummary.getOneStopOrderStatus() == OneStopOrderStatus.Unvet && 
				medOrder.getTicket().getWorkstore().getWorkstoreCode().equals(workstore.getWorkstoreCode()) && 
				medOrder.getTicket().getWorkstore().getHospCode().equals(workstore.getHospCode()) &&
			    new DateMidnight(medOrder.getTicket().getTicketDate()).compareTo(new DateMidnight()) == 0)
			{
				associateTicket(oneStopOrderItemSummary, medOrder.getTicket());
			}
			else if (oneStopOrderItemSummary.getOneStopOrderStatus() == OneStopOrderStatus.Cancelled)
			{
				associateTicket(oneStopOrderItemSummary, medOrder.getTicket());
			}
			else if (medOrder.getTicket().getWorkstore().getWorkstoreCode().equals(workstore.getWorkstoreCode()) && 
					 medOrder.getTicket().getWorkstore().getHospCode().equals(workstore.getHospCode()) &&
					 oneStopOrderItemSummary.getOneStopOrderStatus() != OneStopOrderStatus.Unvet)
			{
				associateTicket(oneStopOrderItemSummary, medOrder.getTicket());
			}
			else
			{
				oneStopOrderItemSummary.setTicketWks(medOrder.getWorkstore().getWorkstoreCode());
			}
		}
		else
		{
			oneStopOrderItemSummary.setTicketWks(medOrder.getWorkstore().getWorkstoreCode());
		}
		
		return oneStopOrderItemSummary;
	}
	
	private void associateTicket(OneStopOrderItemSummary oneStopOrderItemSummary, Ticket ticket) 
	{
		oneStopOrderItemSummary.setTicketDate(ticket.getTicketDate());
		oneStopOrderItemSummary.setTicketNum(ticket.getTicketNum());
		oneStopOrderItemSummary.setTicketWks(ticket.getWorkstore().getWorkstoreCode());
		oneStopOrderItemSummary.setTicketHospCode(ticket.getWorkstore().getHospCode());
	}
	
	private void retrieveVettedDrAndPhsInfo(Long prevPharmOrderId, OneStopOrderItemSummary oneStopOrderItemSummary)
	{
		PharmOrder pharmOrder = em.find(PharmOrder.class, prevPharmOrderId);
		oneStopOrderItemSummary.setPhsSpecCode(pharmOrder.getSpecCode());
		oneStopOrderItemSummary.setOrignalPhsSpecCode(pharmOrder.getSpecCode());
		oneStopOrderItemSummary.setPhsWardCode(pharmOrder.getWardCode());
		oneStopOrderItemSummary.setPatCatCode(pharmOrder.getPatCatCode());
		oneStopOrderItemSummary.setDoctorCode(pharmOrder.getDoctorCode());
		oneStopOrderItemSummary.setDoctorName(pharmOrder.getDoctorName());
	}
	
	public Map<String, String> phsSpecialtyWardMapping(String dispHospCode, String dispWorkstoreCode,
													   String patHospCode, String mappingType, String pasSpecCode,
													   String pasSubSpecCode, String pasWardCode,
													   String patType,
													   boolean useDefaultAssociation) {
		
		Map<String, String> specWardMapping = new HashMap<String, String>();

		logger.debug("phsSpecialtyWardMapping parameter dispHospCode(#0), dispWorkstoreCode(#1), patHospCode(#2), mappingType(#3)" +
				" pasSpecCode(#4), pasSubSpecCode(#5), pasWardCode(#6), patType(#7), includePatHospCodeLevel(#8)", 
				dispHospCode, dispWorkstoreCode, patHospCode, mappingType, pasSpecCode, pasSubSpecCode, pasWardCode, 
				patType, ONESTOP_SPECMAPPING_PATHOSPCODE_ENABLED.get());
		
		List <PmsSpecMapping> pmsSpecMappingList = dmsPmsServiceProxy.retrievePmsSpecMappingForOneStopEntry(dispHospCode, dispWorkstoreCode,
																											patHospCode, mappingType, pasSpecCode,
																											pasSubSpecCode, pasWardCode, patType, 
																											ONESTOP_SPECMAPPING_PATHOSPCODE_ENABLED.get());
		logger.debug("dms retrun result #0", pmsSpecMappingList.size());
		for (PmsSpecMapping pmsSpecMapping : pmsSpecMappingList) 
		{
			SpecialtyPK specialtyPk = new SpecialtyPK(workstore.getWorkstoreGroup().getInstitution().getInstCode(), pmsSpecMapping.getPhsSpecialty());
			Specialty phsSpecaialty = em.find(Specialty.class, specialtyPk);
			logger.debug("phsSpecaialty (#1 #2)", phsSpecaialty==null?"no specaialty":phsSpecaialty.getSpecCode(), phsSpecaialty==null?"no status":phsSpecaialty.getStatus());
			if (phsSpecaialty == null || phsSpecaialty.getStatus() == RecordStatus.Delete || phsSpecaialty.getStatus() == RecordStatus.Suspend) 
			{
				continue;
			}

			WardPK wardPk = new WardPK(workstore.getWorkstoreGroup().getInstitution().getInstCode(), pmsSpecMapping.getPhsWard());
			Ward phsWard = em.find(Ward.class, wardPk);
			logger.debug("phsWard (#1 #2)", phsWard==null?"no ward":phsWard.getWardCode(), phsWard==null?"no status":phsWard.getStatus());
			if (phsWard == null) 
			{
				specWardMapping.put(PHS_SPECIALTY, pmsSpecMapping.getPhsSpecialty());
				break;
			} 
			else if (phsWard.getStatus() == RecordStatus.Delete || phsWard.getStatus() == RecordStatus.Suspend) 
			{
				continue;
			} 
			else 
			{
				specWardMapping.put(PHS_SPECIALTY, pmsSpecMapping.getPhsSpecialty());
				specWardMapping.put(PHS_WARD, pmsSpecMapping.getPhsWard());
				break;
			}					
		}
		
		logger.debug("specWardMapping.containsKey(PHS_SPECIALTY) #0", specWardMapping.containsKey(PHS_SPECIALTY));
		
		if (!specWardMapping.containsKey(PHS_SPECIALTY) && ONESTOP_SPECMAPPING_PATHOSPCODE_ENABLED.get() &&
			(ONESTOP_MEDCASE_PASSPECCODE_UNKNOWN_CHECK.get() || ONESTOP_MEDCASE_PASWARDCODE_UNKNOWN_CHECK.get())
			&& useDefaultAssociation) 
		{
			SpecialtyPK specialtyPk = new SpecialtyPK(workstore.getWorkstoreGroup().getInstitution().getInstCode(), pasSpecCode);
			Specialty phsSpecaialty = em.find(Specialty.class, specialtyPk);
			logger.debug("default associate phsSpecaialty (#0)", phsSpecaialty==null?"no phsSpecialty":phsSpecaialty.getStatus());	
			if (phsSpecaialty == null || phsSpecaialty.getStatus() == RecordStatus.Delete || phsSpecaialty.getStatus() == RecordStatus.Suspend) 
			{
				return specWardMapping;
			}

			WardPK wardPk = new WardPK(workstore.getWorkstoreGroup().getInstitution().getInstCode(), pasWardCode);
			Ward phsWard = em.find(Ward.class, wardPk);
			logger.debug("default associate phsWard (#0)", phsWard==null?"no phsWard":phsWard.getStatus());	
			if (phsWard == null) 
			{
				if (ONESTOP_MEDCASE_PASSPECCODE_UNKNOWN_CHECK.get()) 
				{
					specWardMapping.put(PHS_SPECIALTY, phsSpecaialty.getSpecCode());
				}
			} 
			else if (phsWard.getStatus() == RecordStatus.Delete || phsWard.getStatus() == RecordStatus.Suspend) 
			{
				logger.debug("branch one");	
				return specWardMapping;
			} 
			else 
			{
				logger.debug("branch two");	
				if (ONESTOP_MEDCASE_PASSPECCODE_UNKNOWN_CHECK.get()) 
				{	
					specWardMapping.put(PHS_SPECIALTY, phsSpecaialty.getSpecCode());
				}
				
				if (ONESTOP_MEDCASE_PASWARDCODE_UNKNOWN_CHECK.get())
				{
					specWardMapping.put(PHS_WARD, phsWard.getWardCode());
				}
			}				
		}
				
		logger.debug("pms retrun result #0 #1", specWardMapping.get(PHS_SPECIALTY), specWardMapping.get(PHS_WARD));
		return specWardMapping;
	}

	private OneStopOrderItemSummary constructOneStopOrderItemSummary (MedOrder medOrder, PharmOrder pharmOrder, DispOrder dispOrder) {
		OneStopOrderItemSummary oneStopOrderItemSummary = new OneStopOrderItemSummary();
		oneStopOrderItemSummary.setId(medOrder.getId());
		oneStopOrderItemSummary.setPatType(pharmOrder.getPatType());
		oneStopOrderItemSummary.setPatHospCode(medOrder.getPatHospCode());
		oneStopOrderItemSummary.setPhsSpecCode(pharmOrder.getSpecCode());
		oneStopOrderItemSummary.setOrignalPhsSpecCode(pharmOrder.getSpecCode());
		oneStopOrderItemSummary.setPhsWardCode(pharmOrder.getWardCode());
		oneStopOrderItemSummary.setPatCatCode(pharmOrder.getPatCatCode());
		oneStopOrderItemSummary.setPrescType(medOrder.getPrescType());
		oneStopOrderItemSummary.setOrderDate(medOrder.getOrderDate());
		oneStopOrderItemSummary.setMedOrderVersion(medOrder.getVersion());
		oneStopOrderItemSummary.setOrignalWks(medOrder.getWorkstore().getWorkstoreCode());
		oneStopOrderItemSummary.setDocType(medOrder.getDocType());
		oneStopOrderItemSummary.setHasPostComment(medOrder.getCommentFlag());
		oneStopOrderItemSummary.setMedCase(pharmOrder.getMedCase());
		oneStopOrderItemSummary.setOneStopOrderType(OneStopOrderType.DispOrder);
		oneStopOrderItemSummary.setPrivateFlag(pharmOrder.getPrivateFlag());
		oneStopOrderItemSummary.setMedOrderPhsSpecCode(medOrder.getSpecCode());
		oneStopOrderItemSummary.setMedOrderPhsWardCode(medOrder.getWardCode());
				
		oneStopOrderItemSummary.setOrderNum(medOrder.getOrderNum());
		oneStopOrderItemSummary.setRefNum(medOrder.getRefNum());
		
		if (medOrder.getProcessingFlag() && dispOrder.getDayEndStatus() == DispOrderDayEndStatus.None) {
			Workstation locker = retrieveLockUserDetails(medOrder);
			if (locker.getWorkstore().getWorkstoreCode().equals(workstore.getWorkstoreCode()) && locker.getWorkstore().getHospCode().equals(workstore.getHospCode())) {
				oneStopOrderItemSummary.setUnlockOrder(true);
			}  else {
				oneStopOrderItemSummary.setUnlockOrder(false);
			}
			
			oneStopOrderItemSummary.setLockWorkstationName(locker.getHostName());
			oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.ProcessingByPharmacy);
			
		}  else if (medOrder.getStatus() == MedOrderStatus.Deleted) {	
			if (medOrder.getDocType() == MedOrderDocType.Normal) {	
				oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.Cancelled);
				oneStopOrderItemSummary.setRemarkConfirmDate(medOrder.getRemarkConfirmDate());
			} else {
				oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.Deleted);
			}
		}
		
		return oneStopOrderItemSummary;
	}
	
	private String retrieveInvoiceNum(List<Invoice> invoiceList) {
		for (Invoice invoice : invoiceList) 
		{
			if (invoice.getDocType() == InvoiceDocType.Sfi)
			{
				return invoice.getInvoiceNum();
			}
		}
		
		return null;
	}
	
	public OneStopOrderItemSummary convertSummaryFromVettedOrder(DispOrder dispOrder) 
	{
		MedOrder medOrder = dispOrder.getPharmOrder().getMedOrder();
		PharmOrder pharmOrder = dispOrder.getPharmOrder();		
		OneStopOrderItemSummary oneStopOrderItemSummary = constructDispendedOrderStatus(
				oneStopHelper.convertSummaryFromVettedOrder(dispOrder, pharmOrder, medOrder), dispOrder, pharmOrder, medOrder);				
		return oneStopOrderItemSummary;
	}
	
	public OneStopOrderItemSummary constructDispendedOrderStatus(OneStopOrderItemSummary oneStopOrderItemSummary, DispOrder dispOrder, PharmOrder pharmOrder, MedOrder medOrder) 
	{
		oneStopOrderItemSummary.setMedOrderVersion(medOrder.getVersion());
		oneStopOrderItemSummary.setDispOrderVersion(dispOrder.getVersion());
		oneStopOrderItemSummary.setSuspendCode(dispOrder.getSuspendCode());
		oneStopOrderItemSummary.setRefillFlag(dispOrder.getRefillFlag());
		oneStopOrderItemSummary.setRefillCount(pharmOrder.getRefillCount());
		
		if (medOrder.getProcessingFlag() && dispOrder.getDayEndStatus() == DispOrderDayEndStatus.None) {
			Workstation locker = retrieveLockUserDetails(medOrder);
			if (locker.getWorkstore().getWorkstoreCode().equals(workstore.getWorkstoreCode()) && locker.getWorkstore().getHospCode().equals(workstore.getHospCode())) {
				oneStopOrderItemSummary.setUnlockOrder(true);
			}  else {
				oneStopOrderItemSummary.setUnlockOrder(false);
			}
			
			oneStopOrderItemSummary.setLockWorkstationName(locker.getHostName());
			oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.ProcessingByPharmacy);
			
		}  else if (medOrder.getStatus() == MedOrderStatus.Deleted) {	
			if (medOrder.getDocType() == MedOrderDocType.Normal) {	
				oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.Cancelled);
				oneStopOrderItemSummary.setRemarkConfirmDate(medOrder.getRemarkConfirmDate());
			} else if (!medOrder.isDhOrder()) {
				oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.Deleted);
			}
		}
		
		if (pharmOrder.getRefillCount() > 0)
		{
			oneStopOrderItemSummary.setHasSfiRefill(vettedOrderHasSfiRefill(pharmOrder));
		}
		
		if (dispOrder.getAdminStatus() == DispOrderAdminStatus.Normal) {
			if (dispOrder.getDayEndStatus() != DispOrderDayEndStatus.None || dispOrder.getBatchProcessingFlag()) {
				oneStopOrderItemSummary.setDayEnd(true);								
				if (dispOrder.getBatchProcessingFlag()) {
					oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.LockingByDayEnd);
				}
				
				if (medOrder.getStatus() == MedOrderStatus.Deleted && oneStopOrderItemSummary.getOneStopOrderStatus() == null) {
					oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.Cancelled);
				}
			}			
				
			if (oneStopOrderItemSummary.getOneStopOrderStatus() == null || 
					(medOrder.isDhOrder() && oneStopOrderItemSummary.getOneStopOrderStatus() == OneStopOrderStatus.Vetted)) {
				switch (dispOrder.getStatus())  {
					case Vetted:
					case Picking:
						oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.Vetted);
						break;
					case Picked:
					case Assembling: 
						oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.Picking);			
						break;
					case Assembled:
						oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.Assembling);
						break;
					case Checked:
						oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.Checked);
						break;
					case Issued:
						oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.Issued);
						break;
					default:
						break;
				}
			}
			
			if (pharmOrder.getAllowCommentType() != AllowCommentType.None) {
				oneStopOrderItemSummary.setOneStopOrderStatusInd(retrieveAllowModifyPostCommentOption(pharmOrder.getAllowCommentType()));
			}
						
			if (pharmOrder.getAdminStatus() == PharmOrderAdminStatus.AllowModify) {
				oneStopOrderItemSummary.setOneStopOrderStatusInd(OneStopOrderStatusIndicator.AllowModify);
			}
			
			if (pharmOrder.getAdminStatus() == PharmOrderAdminStatus.Revoke) {
				oneStopOrderItemSummary.setOneStopOrderStatusInd(OneStopOrderStatusIndicator.Uncollect);
			}
			
			if (medOrder.getRemarkStatus() != RemarkStatus.None) {
				oneStopOrderItemSummary.setPharmcyRemark(retrievePharmcyRemark(medOrder));
			}
						
		} else if (dispOrder.getAdminStatus() == DispOrderAdminStatus.Suspended) {
			
			if (dispOrder.getDayEndStatus() != DispOrderDayEndStatus.None || dispOrder.getBatchProcessingFlag()) {
				oneStopOrderItemSummary.setDayEnd(true);								
				if (dispOrder.getBatchProcessingFlag()) {
					oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.LockingByDayEnd);
				}
			}
			else if (pharmOrder.getAdminStatus() == PharmOrderAdminStatus.AllowModify) {
				
				if (oneStopOrderItemSummary.getOneStopOrderStatus() == null) {
					oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.Suspended);
				}
							
				oneStopOrderItemSummary.setOneStopOrderStatusInd(OneStopOrderStatusIndicator.AllowModify);
				
			} else if (pharmOrder.getAdminStatus() == PharmOrderAdminStatus.Revoke) {
				if (oneStopOrderItemSummary.getOneStopOrderStatus() == null) {
					oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.Suspended);
				}
				
				oneStopOrderItemSummary.setOneStopOrderStatusInd(OneStopOrderStatusIndicator.Uncollect);
				
			} else if (CHARGING_SFI_SUSPEND_ORDER_SUSPENDCODE.get().equals(dispOrder.getSuspendCode())) {
				if (oneStopOrderItemSummary.getOneStopOrderStatus() == null)
				{
					oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.AwaitSfiPayment);
				}
				
				if (pharmOrder.getAllowCommentType() != AllowCommentType.None) 
				{
					oneStopOrderItemSummary.setOneStopOrderStatusInd(retrieveAllowModifyPostCommentOption(pharmOrder.getAllowCommentType()));
				} 
				else  
				{
					oneStopOrderItemSummary.setOneStopOrderStatusInd(OneStopOrderStatusIndicator.Suspend);
				}
				
			} else {
				if (oneStopOrderItemSummary.getOneStopOrderStatus() == null || (oneStopOrderItemSummary.getOneStopOrderStatus() == OneStopOrderStatus.Vetted && medOrder.isDhOrder()))
				{
					oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.Suspended);
				}
				
				if (pharmOrder.getAllowCommentType() != AllowCommentType.None) 
				{
					oneStopOrderItemSummary.setOneStopOrderStatusInd(retrieveAllowModifyPostCommentOption(pharmOrder.getAllowCommentType()));
				} 
				else  
				{
					oneStopOrderItemSummary.setOneStopOrderStatusInd(OneStopOrderStatusIndicator.Suspend);
				}
			}
			
			if (medOrder.getRemarkStatus() != RemarkStatus.None && oneStopOrderItemSummary.getOneStopOrderStatus() != OneStopOrderStatus.LockingByDayEnd) {
				oneStopOrderItemSummary.setPharmcyRemark(retrievePharmcyRemark(medOrder));
			}
		}
		
		return oneStopOrderItemSummary;
	}
	
	@SuppressWarnings("unchecked")
	private Boolean vettedOrderHasSfiRefill(PharmOrder pharmOrder)
	{
		List<RefillSchedule> refillScheduleList = em.createQuery(
				"select o from RefillSchedule o" + // 20121112 index check : RefillSchedule.pharmOrder : FK_REFILL_SCHEDULE_01
				" where o.pharmOrder = :pharmOrder" +
				" and o.docType = :docType" +
				" and o.dispOrder is not null" +
				" and o.dispOrder.status not in :dispOrderStatus" +
				" and o.refillNum > 1" +
				" order by o.groupNum, o.refillNum")
				.setParameter("pharmOrder", pharmOrder)
				.setParameter("docType", DocType.Sfi)
				.setParameter("dispOrderStatus", DispOrderStatus.Deleted_SysDeleted)
				.getResultList();
		
		if (!refillScheduleList.isEmpty())
		{
			return true;
		}
		
		return false;
	}
	
	private OneStopOrderStatusIndicator retrieveAllowModifyPostCommentOption(AllowCommentType allowCommentType)
	{
		switch (allowCommentType)
		{
			case PostComment:
				return OneStopOrderStatusIndicator.AllowModifyPostComment;		
				
			case PrescType:
				return OneStopOrderStatusIndicator.AllowModifyChangePrescType;
				
			default:
				return OneStopOrderStatusIndicator.AllowModifyAll;
		}
	}
	
	private OneStopOrderStatusIndicator retrievePharmcyRemark(MedOrder medOrder) {
		
			if (medOrder.getRemarkStatus() == RemarkStatus.Unconfirm) 
			{
				return OneStopOrderStatusIndicator.PharmacyRemark;
			}
			
			return OneStopOrderStatusIndicator.PharmacyremarkConfirm;		
	}
	
	public List<OneStopRefillItemSummary> convertRefillSfiRefillOrder(List<RefillSchedule> refillScheduleList, Boolean isSfiRefillSearch) {
		List<OneStopRefillItemSummary> oneStopRefillItemSummaryList = new ArrayList<OneStopRefillItemSummary>();
		HashMap<Long, RefillSchedule> refillScheduleMap = new HashMap<Long, RefillSchedule> ();
		HashMap<Long, DispOrder> orgDispOrderMap = new HashMap<Long, DispOrder>();
		HashMap<Long, Ticket> ticketMap = new HashMap<Long, Ticket> ();
		
		//get sfi previous dispOrder status for front end checking
		for (RefillSchedule refillSchedule : refillScheduleList)
		{
			if (refillSchedule.getDocType() == DocType.Sfi && refillSchedule.getRefillStatus() == RefillScheduleRefillStatus.Current) 
			{
				refillScheduleMap.put(refillSchedule.getPharmOrder().getId(), refillSchedule);
			}
		}
		
		for (RefillSchedule refillSchedule : refillScheduleList) 
		{
			if (refillSchedule.getRefillNum() == 1)
			{
				orgDispOrderMap.put(refillSchedule.getPharmOrder().getId(), refillSchedule.getDispOrder());
				
				continue;
			}
			
			if (refillSchedule.getDocType() == DocType.Sfi && refillSchedule.getTicket() != null)
			{
				if (ticketMap.get(refillSchedule.getTicket().getId()) != null)
				{
					continue;
				}
				
				ticketMap.put(refillSchedule.getTicket().getId(), refillSchedule.getTicket());
			}
			
			DispOrder orgDispOrder = orgDispOrderMap.get(refillSchedule.getPharmOrder().getId());
			
			OneStopRefillItemSummary oneStopRefillItemSummary = new OneStopRefillItemSummary();
			PharmOrder pharmOrder = refillSchedule.getPharmOrder();
			MedOrder medOrder = refillSchedule.getPharmOrder().getMedOrder();
			MedCase medCase = refillSchedule.getPharmOrder().getMedCase();
			DispOrder dispOrder = refillSchedule.getDispOrder();
			oneStopRefillItemSummary.setId(medOrder.getId());
			oneStopRefillItemSummary.setMedOrderVersion(medOrder.getVersion());
			oneStopRefillItemSummary.setPatHospCode(medOrder.getPatHospCode());
			oneStopRefillItemSummary.setMedCase(medCase);
			oneStopRefillItemSummary.setRefillCouponNum(refillSchedule.getRefillCouponNum());
			oneStopRefillItemSummary.setRefillScheduleId(refillSchedule.getId());
			convertRefillItemStatus(oneStopRefillItemSummary, medOrder, dispOrder, refillSchedule);
			oneStopRefillItemSummary.setRefillCount(0);			
			oneStopRefillItemSummary.setOrderDate(medOrder.getOrderDate());
			oneStopRefillItemSummary.setPatType(pharmOrder.getPatType());
			oneStopRefillItemSummary.setPrivateFlag(pharmOrder.getPrivateFlag());
			oneStopRefillItemSummary.setPatient(pharmOrder.getPatient());
			oneStopRefillItemSummary.setDoctorCode(pharmOrder.getDoctorCode());
			oneStopRefillItemSummary.setDoctorName(pharmOrder.getDoctorName());
			oneStopRefillItemSummary.setPrescType(medOrder.getPrescType());
			oneStopRefillItemSummary.setPrevDispOrderId(orgDispOrder.getId());
			oneStopRefillItemSummary.setPrevDispOrderVersion(orgDispOrder.getVersion());
			
			if (refillSchedule.getRefillStatus() == RefillScheduleRefillStatus.Current && 
					(dispOrder.getDayEndStatus() != DispOrderDayEndStatus.None || dispOrder.getBatchProcessingFlag()))
			{
				oneStopRefillItemSummary.setRefillCount(1);
			}
						
			if (dispOrder != null) 
			{
				oneStopRefillItemSummary.setDispOrderId(dispOrder.getId());
				oneStopRefillItemSummary.setDispOrderVersion(dispOrder.getVersion());
				oneStopRefillItemSummary.setRefillDate(dispOrder.getDispDate());
				oneStopRefillItemSummary.setDispWorkstore(dispOrder.getWorkstore().getWorkstoreCode());
				oneStopRefillItemSummary.setDispHospCode(dispOrder.getWorkstore().getHospCode());
				oneStopRefillItemSummary.setDispDate(dispOrder.getDispDate());
				oneStopRefillItemSummary.setTicketDate(dispOrder.getTicket().getTicketDate());
				oneStopRefillItemSummary.setTicketNum(dispOrder.getTicket().getTicketNum());
				oneStopRefillItemSummary.setTicketWks(dispOrder.getTicket().getWorkstore().getWorkstoreCode());
				oneStopRefillItemSummary.setTicketHospCode(dispOrder.getTicket().getWorkstore().getHospCode());
				oneStopRefillItemSummary.setPhsSpecCode(dispOrder.getSpecCode());
				oneStopRefillItemSummary.setOrignalPhsSpecCode(dispOrder.getSpecCode());
				oneStopRefillItemSummary.setPhsWardCode(dispOrder.getWardCode());
				oneStopRefillItemSummary.setPatCatCode(dispOrder.getPatCatCode());
				
				if (dispOrder.getDayEndStatus() != DispOrderDayEndStatus.None || dispOrder.getBatchProcessingFlag())
				{
					oneStopRefillItemSummary.setDayEnd(true);
					
					if (dispOrder.getBatchProcessingFlag()) {
						oneStopRefillItemSummary.setOneStopOrderStatus(OneStopOrderStatus.LockingByDayEnd);
					}
				}
				
				if (dispOrder.getSfiFlag()) {
					oneStopRefillItemSummary.setInvoiceNum(retrieveInvoiceNum(dispOrder.getInvoiceList()));
				}
			}
			else
			{
				oneStopRefillItemSummary.setPhsSpecCode(pharmOrder.getSpecCode());
				oneStopRefillItemSummary.setOrignalPhsSpecCode(pharmOrder.getSpecCode());
				oneStopRefillItemSummary.setPhsWardCode(pharmOrder.getWardCode());
				oneStopRefillItemSummary.setPatCatCode(pharmOrder.getPatCatCode());
			}
			
			if (refillSchedule.getDocType() == DocType.Standard && 
					refillSchedule.getRefillStatus() == RefillScheduleRefillStatus.Next)
			{
				if (orgDispOrder.getAdminStatus() == DispOrderAdminStatus.Suspended)
				{
					oneStopRefillItemSummary.setIsOrignalOrderSuspended(true);
				}
				else if (medOrder.getStatus() == MedOrderStatus.Deleted)
				{
					oneStopRefillItemSummary.setIsOrignalOrderDeleted(true);
				}
			}
			
			if (refillSchedule.getDocType() == DocType.Sfi) 
			{
				oneStopRefillItemSummary.setOneStopOrderType(OneStopOrderType.SfiOrder);
				oneStopRefillItemSummary.setOrderNum(refillSchedule.getRefillCouponNum().substring(3, 15));
				
				//decision make refill order is read only
				if (refillSchedule.getRefillStatus() == RefillScheduleRefillStatus.None)
				{
					oneStopRefillItemSummary.setRefillCount(1);
				}
							
				if (refillSchedule.getRefillStatus() == RefillScheduleRefillStatus.Next)
				{
					switch (retirvePreviousSfiOrderStatus(refillScheduleMap.get(refillSchedule.getPharmOrder().getId())))
					{
						case ProcessingByPharmacy:
							oneStopRefillItemSummary.setIsOrignalOrderLocked(true);
							break;
						case Suspended:
							oneStopRefillItemSummary.setIsOrignalOrderSuspended(true);
							break;
						case Cancelled:
							oneStopRefillItemSummary.setIsOrignalOrderDeleted(true);
							break;
						case Issued:
							//handle first dispOrder and mark corresponding status
							if (orgDispOrder.getAdminStatus() == DispOrderAdminStatus.Suspended)
							{
								oneStopRefillItemSummary.setIsOrignalOrderSuspended(true);
							}
							break;
						default:
							oneStopRefillItemSummary.setIsPreviousOrderIssued(false);
							break;
					}
				}
				else if (refillSchedule.getRefillStatus() == RefillScheduleRefillStatus.Current)
				{
					if (orgDispOrder.getAdminStatus() == DispOrderAdminStatus.Suspended)
					{
						oneStopRefillItemSummary.setIsOrignalOrderSuspended(true);
					}
				}
			}
			else
			{
				oneStopRefillItemSummary.setOneStopOrderType(OneStopOrderType.RefillOrder);
				oneStopRefillItemSummary.setOrderNum(refillSchedule.getRefillCouponNum().substring(0, 12));
				oneStopRefillItemSummary.setNumOfRefill(refillSchedule.getRefillNum() + "/" + refillSchedule.getNumOfRefill());
				oneStopRefillItemSummary.setGroupNum(refillSchedule.getGroupNum() + "/" + pharmOrder.getNumOfGroup());
			}

			if (refillSchedule.getDocType() != DocType.Sfi || 
				refillSchedule.getStatus() != RefillScheduleStatus.NotYetDispensed || isSfiRefillSearch)
			{
				oneStopRefillItemSummaryList.add(oneStopRefillItemSummary);
			}
			
		}
	
		return oneStopRefillItemSummaryList;
	}
	
	private void convertRefillItemStatus(OneStopRefillItemSummary oneStopRefillItemSummary, MedOrder medOrder,
			DispOrder dispOrder, RefillSchedule refillSchedule)
	{
		if ((medOrder.getProcessingFlag() && dispOrder == null) || (medOrder.getProcessingFlag() && 
				dispOrder != null && dispOrder.getDayEndStatus() == DispOrderDayEndStatus.None))
		{
			Workstation locker = retrieveLockUserDetails(medOrder);
			if (locker.getWorkstore().getWorkstoreCode().equals(workstore.getWorkstoreCode()) && locker.getWorkstore().getHospCode().equals(workstore.getHospCode())) {
				oneStopRefillItemSummary.setUnlockOrder(true);
			}  else {
				oneStopRefillItemSummary.setUnlockOrder(false);
			}
			
			oneStopRefillItemSummary.setLockWorkstationName(locker.getHostName());
			oneStopRefillItemSummary.setOneStopOrderStatus(OneStopOrderStatus.ProcessingByPharmacy);
		}
		
		else if (dispOrder != null && dispOrder.getAdminStatus() == DispOrderAdminStatus.Suspended)
		{
			oneStopRefillItemSummary.setIsOrderSuspended(true);
			
			if (CHARGING_SFI_SUSPEND_ORDER_SUSPENDCODE.get().equals(dispOrder.getSuspendCode())) {
				oneStopRefillItemSummary.setOneStopOrderStatus(OneStopOrderStatus.AwaitSfiPayment);
			} else {
				oneStopRefillItemSummary.setOneStopOrderStatus(OneStopOrderStatus.Suspended);
			}
		}
		else {
			switch (refillSchedule.getStatus())
			{
				case NotYetDispensed:
					oneStopRefillItemSummary.setOneStopOrderStatus(OneStopOrderStatus.NotYetDispensed);
					break;
					
				case BeingProcessed:
					oneStopRefillItemSummary.setOneStopOrderStatus(OneStopOrderStatus.BeingProcessed);
					break;
					
				case Dispensed:
					oneStopRefillItemSummary.setOneStopOrderStatus(OneStopOrderStatus.Dispensed);
					break;
					
				case ForceComplete:
					oneStopRefillItemSummary.setOneStopOrderStatus(OneStopOrderStatus.ForceComplete);
					break;
					
				case Abort:
					oneStopRefillItemSummary.setOneStopOrderStatus(OneStopOrderStatus.Abort);
					break;
			}
		}
	}
	
	private OneStopOrderStatus retirvePreviousSfiOrderStatus(RefillSchedule refillSchedule) 
	{
		MedOrder medOrder = refillSchedule.getPharmOrder().getMedOrder();
		DispOrder dispOrder = refillSchedule.getDispOrder();
		
		if (medOrder.getProcessingFlag())
		{
			return OneStopOrderStatus.ProcessingByPharmacy;
		}
		else if (medOrder.getStatus() == MedOrderStatus.Deleted)
		{
			return OneStopOrderStatus.Cancelled;
		}
		else if (dispOrder.getAdminStatus() == DispOrderAdminStatus.Suspended)
		{
			return OneStopOrderStatus.Suspended;
		}
		else
		{
			if (dispOrder.getAdminStatus() == DispOrderAdminStatus.Normal && dispOrder.getStatus() == DispOrderStatus.Issued)
			{
				return OneStopOrderStatus.Issued;
			}
			
			return OneStopOrderStatus.Vetted;
		}
	}
			
	@Remove
	public void destroy() {
		
	}
}