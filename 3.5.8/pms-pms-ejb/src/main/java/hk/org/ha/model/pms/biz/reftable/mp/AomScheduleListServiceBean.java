package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmDailyFrequency;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.AomSchedule;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("aomScheduleListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class AomScheduleListServiceBean implements AomScheduleListServiceLocal {

	@PersistenceContext
	private EntityManager em;

	@In
	private Workstore workstore;
	
	private static AomScheduleComparator aomScheduleComparator = new AomScheduleComparator();

	@SuppressWarnings("unchecked")
	@Override
	public List<AomSchedule> retrieveAomScheduleList() {
		List<AomSchedule> aomScheduleList = em.createQuery(
				"select o from AomSchedule o" + // 20150209 index check : AomSchedule.hospital : FK_AOM_SCHEDULE_01
				" where o.hospital = :hospital")
				.setParameter("hospital", workstore.getHospital())
				.getResultList();
		
		Collections.sort(aomScheduleList,  aomScheduleComparator);
		return aomScheduleList;
	}

	@Override
	public List<AomSchedule> updateAomScheduleList(List<AomSchedule> updateAomScheduleList, List<AomSchedule> delAomScheduleList) {
		for ( AomSchedule delAomSchedule : delAomScheduleList ) {
			if ( delAomSchedule.getId() != null ) {
				em.remove(em.merge(delAomSchedule));
			}
		}
		em.flush();
		
		for ( AomSchedule updateAomSchedule : updateAomScheduleList ) {
			if ( updateAomSchedule.getId() != null ) {
				em.merge(updateAomSchedule);
			} else {
				updateAomSchedule.setHospital(workstore.getHospital());
				em.persist(updateAomSchedule);
			}
		}
		em.flush();
		
		return retrieveAomScheduleList();
	}
	
	 private static class AomScheduleComparator implements Comparator<AomSchedule>, Serializable {

			private static final long serialVersionUID = 1L;

			@Override
			public int compare(AomSchedule aomSchedule1, AomSchedule aomSchedule2) {
				DmDailyFrequency dmDailyFrequency1 = aomSchedule1.getDmDailyFrequency();
				DmDailyFrequency dmDailyFrequency2 = aomSchedule2.getDmDailyFrequency();

				Integer rank1 = 0;
				Integer rank2 = 0;
				 if ( dmDailyFrequency1 != null ) {
					 rank1 = dmDailyFrequency1.getRank();
				 }
				 if ( dmDailyFrequency2 != null ) {
					 rank2 = dmDailyFrequency2.getRank();
				 }

				return rank1.compareTo(rank2);
			}
	   }

	@Remove
	public void destroy() {
	}
}
