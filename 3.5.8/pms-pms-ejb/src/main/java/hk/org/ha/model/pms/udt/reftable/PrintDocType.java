package hk.org.ha.model.pms.udt.reftable;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PrintDocType implements StringValuedEnum {

	OtherLabel("OL", "Other Label"),
	Report("R", "Report"),
	SfiInvoice("SI", "SFI Invoice"),
	SfiRefillCoupon("SRC", "SFI Refill Coupon"),
	RefillCoupon("RC", "Refill Coupon"),
	CapdVoucher("CV", "CAPD Voucher"),
	PivasLargeLabel("PLL", "PIVAS Large Label"),
	PivasColorLabel("PCL", "PIVAS Colour Label");

    private final String dataValue;
    private final String displayValue;

    PrintDocType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }
    
    public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}
	
	public static PrintDocType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PrintDocType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<PrintDocType> {

		private static final long serialVersionUID = 1L;

		public Class<PrintDocType> getEnumClass() {
			return PrintDocType.class;
		}
		
	}
}
