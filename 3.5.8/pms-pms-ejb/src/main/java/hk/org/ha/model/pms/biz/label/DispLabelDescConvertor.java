package hk.org.ha.model.pms.biz.label;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.vo.label.ConvertDispLabelDescCriteria;

import javax.ejb.Remove;

import org.drools.RuleBase;
import org.drools.StatelessSession;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@AutoCreate
@Scope(ScopeType.APPLICATION)
@Name("dispLabelDescConvertor")
@MeasureCalls
public class DispLabelDescConvertor implements DispLabelDescConvertorInf {
	
	@In(create=true)
	private RuleBase convertDispLabelDescRuleBase;

	private transient StatelessSession convertDispLabelDescWorkingMemory = null;
	
	@Create
	public void create() {
		if ( convertDispLabelDescRuleBase != null) {
			convertDispLabelDescWorkingMemory = convertDispLabelDescRuleBase.newStatelessSession();
		}
	}
	
	public void checkConvertDispLabelDescRule(ConvertDispLabelDescCriteria convertDispLabelDescCriteria){
		if( convertDispLabelDescWorkingMemory != null ){		
			convertDispLabelDescWorkingMemory.execute(convertDispLabelDescCriteria);
		}	
	}
		
	@Remove
	public void destroy() 
	{
		
	}
}
