package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum InvoiceDocType implements StringValuedEnum {

	Standard("S", "Standard"),
	Sfi("F", "SFI");
	
    private final String dataValue;
    private final String displayValue;

    InvoiceDocType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }  
    
    public static InvoiceDocType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(InvoiceDocType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<InvoiceDocType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<InvoiceDocType> getEnumClass() {
    		return InvoiceDocType.class;
    	}
    }
}
