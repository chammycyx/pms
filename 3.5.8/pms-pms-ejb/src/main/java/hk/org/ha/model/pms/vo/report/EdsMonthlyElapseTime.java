package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class EdsMonthlyElapseTime implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Integer dayOfWeek;
	
	private Integer hour;
	
	private Integer avgTime;

	public EdsMonthlyElapseTime(Integer dayOfWeek, Integer hour, Integer avgTime) {
		super();
		this.dayOfWeek = dayOfWeek;
		this.hour = hour;
		this.avgTime = avgTime;
	}

	public void setDayOfWeek(Integer dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}
	
	public Integer getDayOfWeek() {
		return dayOfWeek;
	}

	public void setHour(Integer hour) {
		this.hour = hour;
	}

	public Integer getHour() {
		return hour;
	}

	public Integer getAvgTime() {
		return avgTime;
	}

	public void setAvgTime(Integer avgTime) {
		this.avgTime = avgTime;
	}
}
