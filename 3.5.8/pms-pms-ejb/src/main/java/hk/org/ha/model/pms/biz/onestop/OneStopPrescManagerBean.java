package hk.org.ha.model.pms.biz.onestop;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderStatus;
import hk.org.ha.model.pms.udt.onestop.MoeFilterOption;
import hk.org.ha.model.pms.udt.refill.DocType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.onestop.OneStopOrderItemSummary;
import hk.org.ha.model.pms.vo.onestop.OneStopSummary;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.joda.time.DateTime;

@AutoCreate
@Stateless
@Name("oneStopPrescManager")
@MeasureCalls
public class OneStopPrescManagerBean implements OneStopPrescManagerLocal {
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private OneStopUtilsLocal oneStopUtils;
	
	private final String WORKSTORE = "workstore";
	private final String STATUS = "status";
	private final String ORDER_DATE = "orderDate";
	private final String ORDER_END_DATE = "orderEndDate";
	private final String DOC_TYPE = "docType";
	private final String ORDER_TYPE = "orderType";
	private final String PHARM_ORDER_STATUS = "pharmOrderStatus";
	private final String DISP_ORDER_STATUS = "dispOrderStatus";
	private final String REFILL_TYPE = "refillType";
	
	private final Pattern aeCaseNumPattern = Pattern.compile("^AE[0-9]+[A-Za-z0-9]$$");
	private final Pattern hnCaseNumPattern1 = Pattern.compile("^HN[0-9]+[A-Za-z0-9]$");
		
	@SuppressWarnings("unchecked")
	public Map<String, Map<Long, OneStopSummary>> retrieveOrdersPrescMap(Date orderDate, Workstore workstore, Boolean retrieveAll)
	{		
		Map<String, Map<Long, OneStopSummary>> orderMap = new HashMap<String, Map<Long, OneStopSummary>>();
		Map<Long, OneStopSummary> originalRefillOneStopSummaryMap = new LinkedHashMap<Long, OneStopSummary>();
		Map<Long, OneStopSummary> originalUnvetOneStopSummaryMap = new LinkedHashMap<Long, OneStopSummary>();
		Map<Long, OneStopSummary> originalCancelledOneStopSummaryMap = new LinkedHashMap<Long, OneStopSummary>();
		DateTime orderEndDate = new DateTime(orderDate).plusDays(1);
		
		if (retrieveAll)
		{
			// get refill order (get standard refill only)
			List<RefillSchedule> allRefillOrderList = (List<RefillSchedule>) em.createQuery(
					"select o from RefillSchedule o" + // 20160809 index check : MedOrder.workstore,status,orderType,orderDate : I_MED_ORDER_06
					" where o.pharmOrder.medOrder.workstore = :workstore" +
					" and o.pharmOrder.medOrder.orderType = :orderType" +
					" and o.pharmOrder.medOrder.orderDate >= :orderDate" +
					" and o.pharmOrder.medOrder.orderDate < :orderEndDate" +				
					" and o.pharmOrder.medOrder.status in :status" +				
					" and o.pharmOrder.medOrder.docType = :docType" +
					" and o.pharmOrder.status <> :pharmOrderStatus" +		
					" and o.docType = :refillType" +
					" and o.refillNum = 1" +
					" order by o.pharmOrder.medOrder.orderDate")
					.setParameter(WORKSTORE, workstore)
					.setParameter(ORDER_TYPE, OrderType.OutPatient)
					.setParameter(ORDER_DATE, orderDate, TemporalType.DATE)
					.setParameter(ORDER_END_DATE, orderEndDate.toDate(), TemporalType.DATE)
					.setParameter(STATUS, MedOrderStatus.Not_Outstanding_Deleted_SysDeleted)
					.setParameter(DOC_TYPE, MedOrderDocType.Normal)
					.setParameter(PHARM_ORDER_STATUS, PharmOrderStatus.SysDeleted)
					.setParameter(REFILL_TYPE, DocType.Standard)
					.getResultList();
			
			for (RefillSchedule refillSchedule : allRefillOrderList) {
				OneStopSummary oneStopSummaryForRefill = new OneStopSummary();
				List<OneStopOrderItemSummary> oneStopOrderItemSummaryList = new ArrayList<OneStopOrderItemSummary>();
				oneStopOrderItemSummaryList.add(oneStopUtils.convertSummaryFromVettedOrder(refillSchedule.getDispOrder()));
				oneStopSummaryForRefill.setOneStopOrderItemSummaryList(oneStopOrderItemSummaryList);
				oneStopSummaryForRefill.setPatient(refillSchedule.getPharmOrder().getPatient());
				originalRefillOneStopSummaryMap.put(oneStopOrderItemSummaryList.get(0).getId(), oneStopSummaryForRefill);
			}
		}
		
		// get unvet order and unvet cancel order		
		List<MedOrder> unvetOrderList = (List<MedOrder>) em.createQuery(
				"select o from MedOrder o" + // 20140925 index check : MedOrder.workstore,status,orderType,orderDate : I_MED_ORDER_06
				" where o.workstore = :workstore" + 
				" and o.orderType = :orderType" +
				" and o.orderDate >= :orderDate" +
				" and o.orderDate < :orderEndDate" +
				" and o.status in :status" +
				" and o.docType = :docType" + 
				" and o.vetDate is null" + 
				" order by o.orderDate")
				.setParameter(WORKSTORE, workstore)
				.setParameter(ORDER_TYPE, OrderType.OutPatient)
				.setParameter(ORDER_DATE, orderDate, TemporalType.DATE)
				.setParameter(ORDER_END_DATE, orderEndDate.toDate(), TemporalType.DATE)
				.setParameter(STATUS, MedOrderStatus.Outstanding_Deleted_Withhold_PreVet)
				.setParameter(DOC_TYPE, MedOrderDocType.Normal)				
				.getResultList();
		
		for (MedOrder unvetOrder : unvetOrderList) {
			OneStopSummary oneStopSummaryForUnvet = new OneStopSummary();
			List<OneStopOrderItemSummary> oneStopOrderItemSummaryList = new ArrayList<OneStopOrderItemSummary>();
			OneStopOrderItemSummary oneStopOrderItemSummary = oneStopUtils.convertSummaryFromUnvetOrder(unvetOrder);
			oneStopOrderItemSummaryList.add(oneStopOrderItemSummary);
			oneStopSummaryForUnvet.setOneStopOrderItemSummaryList(oneStopOrderItemSummaryList);
							
			if (unvetOrder.getStatus() == MedOrderStatus.Deleted || 
				(unvetOrder.getStatus() == MedOrderStatus.Outstanding && unvetOrder.getRemarkCreateUser() != null) ) 
			{
				originalCancelledOneStopSummaryMap.put(oneStopOrderItemSummary.getId(), oneStopSummaryForUnvet);
			} 
			else 
			{
				originalUnvetOneStopSummaryMap.put(oneStopOrderItemSummary.getId(), oneStopSummaryForUnvet);
			}
		}
		
		if (retrieveAll)
		{
			// get vetted cancel order
			List<DispOrder> vetCancelOrderList = (List<DispOrder>) em.createQuery(
					"select distinct o from DispOrder o" + // 20120403 index check : MedOrder.workstore,orderType,orderDate : FK_MED_ORDER_06
					" where o.pharmOrder.medOrder.workstore = :workstore" +
					" and o.pharmOrder.medOrder.orderType = :orderType" +
					" and o.pharmOrder.medOrder.orderDate >= :orderDate" +
					" and o.pharmOrder.medOrder.orderDate < :orderEndDate" +
					" and o.pharmOrder.medOrder.status = :status" +
					" and o.status <> :dispOrderStatus" +
					" and o.pharmOrder.medOrder.docType = :docType" + 
					" order by o.pharmOrder.medOrder.orderDate")
					.setParameter(WORKSTORE, workstore)
					.setParameter(ORDER_TYPE, OrderType.OutPatient)
					.setParameter(ORDER_DATE, orderDate, TemporalType.DATE)
					.setParameter(ORDER_END_DATE, orderEndDate.toDate(), TemporalType.DATE)
					.setParameter(STATUS, MedOrderStatus.Deleted)
					.setParameter(DISP_ORDER_STATUS, DispOrderStatus.SysDeleted)
					.setParameter(DOC_TYPE, MedOrderDocType.Normal)
					.getResultList();
			
			for (DispOrder cancelDispOrder : vetCancelOrderList) {
				OneStopSummary oneStopSummaryForVet = new OneStopSummary();
				List<OneStopOrderItemSummary> oneStopOrderItemSummaryList = new ArrayList<OneStopOrderItemSummary>();
				OneStopOrderItemSummary oneStopOrderItemSummary = oneStopUtils.convertSummaryFromVettedOrder(cancelDispOrder);
				oneStopOrderItemSummaryList.add(oneStopOrderItemSummary);
				oneStopSummaryForVet.setOneStopOrderItemSummaryList(oneStopOrderItemSummaryList);
				oneStopSummaryForVet.setPatient(cancelDispOrder.getPharmOrder().getPatient());
				originalCancelledOneStopSummaryMap.put(oneStopOrderItemSummary.getId(), oneStopSummaryForVet);
			}
		}
		
		orderMap.put("refill", originalRefillOneStopSummaryMap);
		orderMap.put("unvet", originalUnvetOneStopSummaryMap);
		orderMap.put("cancel", originalCancelledOneStopSummaryMap);
		return orderMap;
	}
	

	public List<OneStopSummary> retrieveUnvetPrescList(Date orderDate, MoeFilterOption moeFilterOption, Workstore workstore, Boolean retrieveAll)
	{
		Map<String, Map<Long, OneStopSummary>> retrieveOrdersPrescMap = retrieveOrdersPrescMap(orderDate, workstore, retrieveAll);		
		return filterUnvetRefillCancelledList(retrieveOrdersPrescMap.get("unvet"), moeFilterOption);
	}
	
	public List<OneStopSummary> filterUnvetRefillCancelledList(Map<Long, OneStopSummary> sourceMap, MoeFilterOption moeFilterOption)
	{
		List<OneStopSummary> dataList = new ArrayList<OneStopSummary>();
		
		for (Map.Entry<Long, OneStopSummary> entry : sourceMap.entrySet())
		{
			OneStopSummary oneStopSummary = entry.getValue();
			MedCase medCase = oneStopSummary.getOneStopOrderItemSummaryList().get(0).getMedCase();
			
		    if (moeFilterOption == MoeFilterOption.All)
		    {
		    	dataList.add(oneStopSummary);
		    }
		    else if (moeFilterOption == MoeFilterOption.In)
		    {
		    	if (aeCaseNumPattern.matcher(medCase.getCaseNum()).find() || hnCaseNumPattern1.matcher(medCase.getCaseNum()).find())
		    	{
		    		dataList.add(oneStopSummary);
		    	}
		    }
		    else if (moeFilterOption == MoeFilterOption.Out)
		    {
		    	if (!aeCaseNumPattern.matcher(medCase.getCaseNum()).find() && !hnCaseNumPattern1.matcher(medCase.getCaseNum()).find())
		    	{
		    		dataList.add(oneStopSummary);
		    	}
		    }
		    else if (moeFilterOption == MoeFilterOption.AE)
		    {
		    	if (aeCaseNumPattern.matcher(medCase.getCaseNum()).find())
		    	{
		    		dataList.add(oneStopSummary);
		    	}
		    }
		}
		
		return dataList;
	}
}