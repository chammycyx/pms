package hk.org.ha.model.pms.biz.reftable;

import javax.ejb.Local;

@Local
public interface PmsSessionInfoListServiceLocal {
	
	void retrievePmsSessionInfoList();
	
	void destroy();
	
}
