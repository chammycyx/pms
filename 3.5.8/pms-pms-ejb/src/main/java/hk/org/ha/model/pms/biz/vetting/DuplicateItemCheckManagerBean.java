package hk.org.ha.model.pms.biz.vetting;

import static hk.org.ha.model.pms.prop.Prop.CDDH_LEGACY_ENQUIRY_ENABLED;
import hk.org.ha.fmk.pms.remote.marshall.EclipseLinkXStreamMarshaller;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.label.InstructionBuilder;
import hk.org.ha.model.pms.biz.reftable.RefTableManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.corp.cache.DmRouteCacher;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.udt.disp.DupChkDrugType;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.label.DispLabel;
import hk.org.ha.model.pms.vo.label.Warning;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpCddhCheckServiceJmsRemote;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.ejb.Remove;
import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("duplicateItemCheckManager")
@MeasureCalls
public class DuplicateItemCheckManagerBean implements DuplicateItemCheckManagerLocal {

	private static final DupItemComparator dupItemComparator = new DupItemComparator();
	
	private static final DupItemListComparator dupItemListComparator = new DupItemListComparator();
	
	@Logger
	private Log logger;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private List<MedOrderItem> localDuplicateItemList;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private List<DispOrderItem> corpDuplicateItemList;
	
	@In
	private CorpCddhCheckServiceJmsRemote corpCddhCheckServiceProxy;
	
	@In
	private InstructionBuilder instructionBuilder;
	
	@In
	private RefTableManagerLocal refTableManager;
	
	@In
	private Workstore workstore;

	public boolean checkLocalDuplicateItem(PharmOrder pharmOrder) {
		
		// assign seqNum
		for (MedOrderItem moi : pharmOrder.getMedOrder().getMedOrderItemList()) {
			moi.setSeqNum(pharmOrder.getMedOrder().getMedOrderItemList().indexOf(moi));
		}
		
		// group item by displayName / itemCode
		Map<String, Set<MedOrderItem>> oralItemSetMap    = new HashMap<String, Set<MedOrderItem>>();
		Map<String, Set<MedOrderItem>> nonOralItemSetMap = new HashMap<String, Set<MedOrderItem>>();
		Set<Set<MedOrderItem>> allItemSet = new HashSet<Set<MedOrderItem>>();
		
		for (PharmOrderItem pharmOrderItem : pharmOrder.getPharmOrderItemList()) {
			
			MedOrderItem medOrderItem = pharmOrderItem.getMedOrderItem();
			pharmOrderItem.loadDmInfo();
			if (medOrderItem.getStatus() == MedOrderItemStatus.SysDeleted ||
					medOrderItem.getStatus() == MedOrderItemStatus.Deleted ||
				FmStatus.FreeTextEntryItem.getDataValue().equals(medOrderItem.getFmStatus())) {
				continue;
			}
			
			String key;
			Map<String, Set<MedOrderItem>> itemSetMap;
			
			if (pharmOrderItem.getDupChkDrugType() == DupChkDrugType.Oral) { // compare by displayname for oral drug
				key = pharmOrderItem.getDupChkDisplayName();
				itemSetMap = oralItemSetMap;
			}
			else { // compare by itemCode for non-oral drug
				key = pharmOrderItem.getItemCode();
				itemSetMap = nonOralItemSetMap;
			}

			if (itemSetMap.containsKey(key)) {
				itemSetMap.get(key).add(medOrderItem);
			}
			else {
				Set<MedOrderItem> itemSet = new HashSet<MedOrderItem>();
				itemSet.add(medOrderItem);
				itemSetMap.put(key, itemSet);
			}
		}
		allItemSet.addAll(oralItemSetMap.values());
		allItemSet.addAll(nonOralItemSetMap.values());

		// sort item within group, then sort the group
		List<List<MedOrderItem>> allItemList = new ArrayList<List<MedOrderItem>>();
		for (Set<MedOrderItem> itemSet : allItemSet) {
			if (itemSet.size() <= 1) {
				continue;
			}
			List<MedOrderItem> itemList = new ArrayList<MedOrderItem>(itemSet);
			Collections.sort(itemList, dupItemComparator);
			allItemList.add(itemList);
		}
		Collections.sort(allItemList, dupItemListComparator);

		// remove duplicate item, the sort the group again
		Set<List<MedOrderItem>> delItemListSet = new HashSet<List<MedOrderItem>>();
		Set<MedOrderItem> showItemSet = new HashSet<MedOrderItem>();

		for (List<MedOrderItem> itemList : allItemList) {
			Set<MedOrderItem> delItemSet = new HashSet<MedOrderItem>();
			for (MedOrderItem item : itemList) {
				if (showItemSet.contains(item)) {
					delItemSet.add(item);
				}
				else {
					showItemSet.add(item);
				}
			}
			itemList.removeAll(delItemSet);
			if (itemList.isEmpty()) {
				delItemListSet.add(itemList);
			}
		}
		
		allItemList.removeAll(delItemListSet);
		Collections.sort(allItemList, dupItemListComparator);
		
		// mark end-of-group
		localDuplicateItemList = new ArrayList<MedOrderItem>();
		for (List<MedOrderItem> itemList : allItemList) {
			localDuplicateItemList.addAll(itemList);
			if (allItemList.indexOf(itemList) == 0) {
				continue;
			}
			itemList.get(0).setGroupEndFlag(Boolean.TRUE);
		}

		// disconnect
		for (MedOrderItem medOrderItem : localDuplicateItemList) {
			medOrderItem.setMedOrder(null);
			for (PharmOrderItem pharmOrderItem : medOrderItem.getPharmOrderItemList() ) {
				pharmOrderItem.setPharmOrder(null);
				pharmOrderItem.clearDispOrderItemList();
				pharmOrderItem.setMedOrderItem(medOrderItem);
			}
		}
		
		return localDuplicateItemList.isEmpty();
	}

	public void clearLocalDuplicateItem() {
		localDuplicateItemList = new ArrayList<MedOrderItem>();
	}
	
	private Map<Integer, List<DispOrderItem>> retrieveCorpDuplicateItem(PharmOrder pharmOrder, List<PharmOrderItem> pharmOrderItemList) {

		// group pharmOrderItem by medOrderItem.orgItemNum
		Map<Integer, List<PharmOrderItem>> pharmOrderItemMap = new HashMap<Integer, List<PharmOrderItem>>();

		int pharmOrderItemCount = 0;
		
		for (PharmOrderItem pharmOrderItem : pharmOrderItemList) {

			MedOrderItem medOrderItem = pharmOrderItem.getMedOrderItem();
			
			if (medOrderItem.getStatus() == MedOrderItemStatus.SysDeleted || medOrderItem.getStatus() == MedOrderItemStatus.Deleted) {
				continue;
			}
			if (FmStatus.FreeTextEntryItem.getDataValue().equals(medOrderItem.getFmStatus()) && pharmOrderItem.getDupChkDrugType() == DupChkDrugType.Other) {
				continue;
			}
			
			pharmOrderItemCount++;
			
			// get list by orgItemNum
			Integer orgItemNum = medOrderItem.getOrgItemNum();
			if (!pharmOrderItemMap.containsKey(orgItemNum)) {
				pharmOrderItemMap.put(orgItemNum, new ArrayList<PharmOrderItem>());
			}

			// add pharmOrderItem
			PharmOrderItem outItem = new PharmOrderItem();
			outItem.setDupChkDrugType(pharmOrderItem.getDupChkDrugType());
			outItem.setItemCode(pharmOrderItem.getItemCode());
			outItem.setDupChkDisplayName(pharmOrderItem.getDupChkDisplayName());
			pharmOrderItemMap.get(orgItemNum).add(outItem);
		}
		
		if (pharmOrderItemCount == 0) {
			return new HashMap<Integer, List<DispOrderItem>>();
		}
		
		// check duplicate item in corp
		MedOrder medOrder = pharmOrder.getMedOrder();
		
		String caseNum = null;
		if (pharmOrder.getMedCase() != null) {
			caseNum = pharmOrder.getMedCase().getCaseNum();
		}
		
		Map<Integer, List<DispOrderItem>> corpDuplicateItemMap = corpCddhCheckServiceProxy.retrieveDuplicateDispensedItem(
				medOrder.getPatient().getHkid(),
				caseNum,
				medOrder.getPatHospCode(),
				medOrder.getOrderNum(),
				pharmOrderItemMap,
				CDDH_LEGACY_ENQUIRY_ENABLED.get(Boolean.FALSE)
		);

		if (logger.isDebugEnabled()) {
			logger.debug("corpDuplicateItemMap=#0", new EclipseLinkXStreamMarshaller().toXML(corpDuplicateItemMap));
		}
		
		for (List<DispOrderItem> dispOrderItemList : corpDuplicateItemMap.values()) {

			for (DispOrderItem dispOrderItem : dispOrderItemList) {
				
				// construct dispLabel
				if (dispOrderItem.isLegacy() && dispOrderItem.getBaseLabel() == null) {
					DispLabel dispLabel = new DispLabel();
					PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
					pharmOrderItem.setMedOrderItem(null);
					String fdn = refTableManager.getFdnByItemCodeOrderTypeWorkstore(
							pharmOrderItem.getItemCode(),
							OrderType.OutPatient,
							workstore
					);
					dispLabel.setItemDesc(
							DmDrug.buildFullDrugDesc(
									(fdn==null?pharmOrderItem.getDrugName():fdn),
									pharmOrderItem.getFormCode(),
									pharmOrderItem.getFormLabelDesc(),
									pharmOrderItem.getStrength(),
									pharmOrderItem.getVolumeText()
							)
					);
					
					dispOrderItem.getPharmOrderItem().loadDmInfo();
					
					dispLabel.setInstructionList(instructionBuilder.buildInstructionListForDispLabel(dispOrderItem));
					dispLabel.setWarningList(new ArrayList<Warning>());
					dispOrderItem.setBaseLabel(dispLabel);
				}
				
				// set printLang
				if (dispOrderItem.getBaseLabel() != null) {
					PrintOption printOption = new PrintOption();
					printOption.setPrintLang(PrintLang.Eng);
					dispOrderItem.getBaseLabel().setPrintOption(printOption);
				}
				
				// prepare to outject
				dispOrderItem.getDispOrder().loadChild();
				dispOrderItem.getDispOrder().getWorkstore().getWorkstoreGroup();
				dispOrderItem.getDispOrder().getWorkstore().getCddhWorkstoreGroup();
			}
		}
		
		return corpDuplicateItemMap;
	}

	public void checkCorpDuplicateItemByPharmOrder(PharmOrder pharmOrder) {
		for (Entry<Integer, List<DispOrderItem>> entry : retrieveCorpDuplicateItem(pharmOrder, pharmOrder.getPharmOrderItemList()).entrySet()) {
			pharmOrder.getMedOrder().getMedOrderItemByOrgItemNum(entry.getKey()).setDuplicateItemList(entry.getValue());
		}
	}
	
	public boolean checkCorpDuplicateItemByMedOrderItem(PharmOrder pharmOrder, MedOrderItem medOrderItem) {
		int duplicateItemCount = 0;
		for (Entry<Integer, List<DispOrderItem>> entry : retrieveCorpDuplicateItem(pharmOrder, medOrderItem.getPharmOrderItemList()).entrySet()) {
			if (medOrderItem.getOrgItemNum().equals(entry.getKey())) {
				medOrderItem.setDuplicateItemList(entry.getValue());
			}
			duplicateItemCount += entry.getValue().size();
		}
		return duplicateItemCount == 0;
		
	}

	public void setCorpDuplicateItemList(List<DispOrderItem> dispOrderItemList) {
		corpDuplicateItemList = dispOrderItemList;
	}

	@Remove
	public void destroy() {
		if (localDuplicateItemList != null) {
			localDuplicateItemList = null;
		}
		if (corpDuplicateItemList != null) {
			corpDuplicateItemList = null;
		}
	}
	
	private static class DupItemListComparator implements Comparator<List<MedOrderItem>>, Serializable {

		private static final long serialVersionUID = 1L;

		public int compare(List<MedOrderItem> moiList1, List<MedOrderItem> moiList2) {

			DupItemComparator comparator = new DupItemComparator();
			
			int listSize = moiList1.size() > moiList2.size() ? moiList2.size() : moiList1.size();

			for (int i=0; i<listSize; i++) {
				int compareResult = comparator.compare(moiList1.get(i), moiList2.get(i));
				if (compareResult != 0) {
					return compareResult;
				}
			}

			if (moiList1.size() > moiList2.size()) {
				return -1;
			}
			else {
				return 1;
			}
		}
		
	}
	
	private static class DupItemComparator extends MedOrderItemComparator {

		private static final long serialVersionUID = 1L;

		private String getDisplayName(MedOrderItem moi) {
			String displayName = null;
			for (PharmOrderItem poi : moi.getPharmOrderItemList()) {
				DmDrug dmDrug = DmDrugCacher.instance().getDmDrug(poi.getItemCode());
				if (displayName == null || (displayName.compareTo(dmDrug.getDmDrugProperty().getDisplayname()) > 0)) {
					displayName = dmDrug.getDmDrugProperty().getDisplayname();
				}
			}
			return displayName;
		}
		
		private String getRouteDesc(MedOrderItem moi) {
			String routeDesc = null;
			DmDrug dmDrug;
			for (PharmOrderItem poi : moi.getPharmOrderItemList()) {
				dmDrug = DmDrugCacher.instance().getDmDrug(poi.getItemCode());
				if (routeDesc == null || (routeDesc.compareTo(DmRouteCacher.instance().getRouteByRouteCode(dmDrug.getDmForm().getRouteCode()).getRouteDesc()) > 0)) {
					routeDesc = DmRouteCacher.instance().getRouteByRouteCode(dmDrug.getDmForm().getRouteCode()).getRouteDesc();
				}
			}
			return routeDesc;
		}
		
		public int compare(MedOrderItem moi1, MedOrderItem moi2) {
			
			// compare displayName
			String displayName1 = getDisplayName(moi1);
			String displayName2 = getDisplayName(moi2);
			if (!displayName1.equals(displayName2)) {
				return displayName1.compareTo(displayName2);
			}

			// compare routeDesc
			String routeDesc1 = StringUtils.trimToEmpty(getRouteDesc(moi1));
			String routeDesc2 = StringUtils.trimToEmpty(getRouteDesc(moi2));
			if (!routeDesc1.equals(routeDesc2)) {
				return routeDesc1.compareTo(routeDesc2);
			}
			
			// compare seqNum
			Integer seqNum1 = moi1.getSeqNum();
			Integer seqNum2 = moi2.getSeqNum();
			if (!seqNum1.equals(seqNum2)) {
				return seqNum1.compareTo(seqNum2);
			}
			
			return super.compare(moi1, moi2);
		}
	}
}
