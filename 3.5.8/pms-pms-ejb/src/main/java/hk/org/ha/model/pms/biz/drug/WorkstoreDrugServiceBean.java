package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.model.pms.vo.rx.DhRxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("workstoreDrugService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class WorkstoreDrugServiceBean implements WorkstoreDrugServiceLocal {
	
	@In
	private Workstore workstore;

	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;

	@In
	private WorkstoreDrugListManagerLocal workstoreDrugListManager;	
	
	@In
	private DrugDiluentListManagerLocal drugDiluentListManager;

	public WorkstoreDrug retrieveWorkstoreDrug(String itemCode)
	{	
		WorkstoreDrug workstoreDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, itemCode);
		if ( workstoreDrug != null ) {
			workstoreDrug.getDmDrug().getFullDrugDesc();
		}
		return workstoreDrug;
	}

	public WorkstoreDrug retrieveWorkstoreDrugForOrderEdit(String displayName, String itemCode)
	{		
		List<WorkstoreDrug> fullDrugList = new ArrayList<WorkstoreDrug>();
		List<WorkstoreDrug> drugList = workstoreDrugCacher.getWorkstoreDrugListByDisplayName(workstore, displayName);		
		List<WorkstoreDrug> solventList = workstoreDrugCacher.getWorkstoreDrugListBySolvent(workstore);
		if ( drugList != null ) {
			fullDrugList.addAll(drugList);			
		}		
		if ( solventList != null ) {		
			fullDrugList.addAll(solventList);			
		}
		return findWorkstoreDrugFromDrugList(fullDrugList, itemCode);									
	}

	public WorkstoreDrug retrieveWorkstoreDrugForMpOrderEdit(RxItem rxItem, String itemCode)
	{
		List<WorkstoreDrug> resultList = new ArrayList<WorkstoreDrug>();
		List<WorkstoreDrug> drugList = workstoreDrugListManager.getWorkstoreDrugListByRxItem(rxItem, true);	
		List<WorkstoreDrug> solventList = workstoreDrugCacher.getWorkstoreDrugListBySolvent(workstore);
		List<WorkstoreDrug> diluentList = drugDiluentListManager.getDrugDiluentList(rxItem);		
		if ( drugList != null ) {
			resultList.addAll(drugList);
		}
		if ( diluentList != null ) {
			resultList.addAll(diluentList);
		}
		if ( solventList != null ) {
			resultList.addAll(solventList);			
		}
		return findWorkstoreDrugFromDrugList( resultList, itemCode );					
	}
	
	private WorkstoreDrug findWorkstoreDrugFromDrugList(List<WorkstoreDrug> drugList, String itemCode)
	{
		for ( WorkstoreDrug drug : drugList ) {
			if ( drug.getDmDrug().getItemCode().equals(itemCode) ) {	
				drug.getDmDrug().getFullDrugDesc();
				drug.getDmDrug().getVolumeText();
				return drug;				
			}
		}
		return null;
	}
	
	public WorkstoreDrug retrieveWorkstoreDrugForDhOrderEdit(DhRxDrug dhRxDrug, String itemCode) {
		
		Pair<List<WorkstoreDrug>, DmDrug> mappingResult = workstoreDrugListManager.getWorkstoreDrugListDmDrugByDhRxDrug(dhRxDrug);
		List<WorkstoreDrug> fullDrugList = mappingResult.getFirst();
		
		if (fullDrugList == null) {
			WorkstoreDrug workstoreDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, itemCode);
			if (workstoreDrug == null) {
				fullDrugList = new ArrayList<WorkstoreDrug>();
			}
			else {
				fullDrugList = new ArrayList<WorkstoreDrug>(Arrays.asList(workstoreDrug));
			}
		}
		else {
			fullDrugList.addAll(workstoreDrugCacher.getWorkstoreDrugListByDisplayName(workstore, mappingResult.getSecond().getDmDrugProperty().getDisplayname()));
			fullDrugList.addAll(workstoreDrugCacher.getWorkstoreDrugListBySolvent(workstore));
		}
		
		return findWorkstoreDrugFromDrugList(fullDrugList, itemCode);
	}

	@Remove
	public void destroy() 
	{
	}
}
