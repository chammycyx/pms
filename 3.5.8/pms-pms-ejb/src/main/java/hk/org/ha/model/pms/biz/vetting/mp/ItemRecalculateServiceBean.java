package hk.org.ha.model.pms.biz.vetting.mp;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.medprofile.MedProfileItemConverterLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("itemRecalculateService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ItemRecalculateServiceBean implements ItemRecalculateServiceLocal {

	@In
	private Workstore workstore;
	
	@In(required = false)
	private MedProfile medProfile;
	
	@In
	private MedProfileItemConverterLocal medProfileItemConverter; 
	
	@Override
	public MedProfilePoItem recalculateQty(MedProfilePoItem medProfilePoItem) {
	
		if (medProfile != null) {
			medProfilePoItem = medProfileItemConverter.recalculateQty(workstore, medProfile, medProfilePoItem);
		}
		medProfilePoItem.setMedProfileMoItem(null);
		
		return medProfilePoItem;
	}
	
    @Remove
	@Override
	public void destroy() {
	}	
}
