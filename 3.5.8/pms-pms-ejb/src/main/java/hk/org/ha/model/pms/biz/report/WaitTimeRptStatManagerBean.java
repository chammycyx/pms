package hk.org.ha.model.pms.biz.report;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hk.org.ha.fmk.pms.util.JavaBeanResult;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.WaitTimeRptStat;
import hk.org.ha.model.pms.prop.Prop;
import hk.org.ha.model.pms.udt.RecordStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderStatus;
import hk.org.ha.model.pms.udt.onestop.AllowCommentType;
import hk.org.ha.model.pms.vo.report.EdsElapseTime;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.joda.time.DateTime;

@AutoCreate
@Stateless
@Name("waitTimeRptStatManager")
@MeasureCalls
public class WaitTimeRptStatManagerBean implements WaitTimeRptStatManagerLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;

	private WaitTimeRptStat getNewWaitTimeRptStat(String hospCode, String workstoreCode, DateTime statDate) {
		WaitTimeRptStat waitTimeRptStat = new WaitTimeRptStat();
		waitTimeRptStat.setHospCode(hospCode);
		waitTimeRptStat.setWorkstoreCode(workstoreCode);
		waitTimeRptStat.setYear(Integer.valueOf(statDate.getYear()));
		waitTimeRptStat.setMonth(Integer.valueOf(statDate.getMonthOfYear()));
		waitTimeRptStat.setDay(Integer.valueOf(statDate.getDayOfMonth()));
		waitTimeRptStat.setDayOfWeek(Integer.valueOf(statDate.getDayOfWeek()));
		waitTimeRptStat.setBatchDate(new DateTime(statDate.getYear(),statDate.getMonthOfYear(),statDate.getDayOfMonth(),0,0,0,0).toDate());
		return waitTimeRptStat;
	}

	@SuppressWarnings("unchecked")
	private List<Workstore> getNotYetGeneratedWorkstoreList(String clusterCode, Date batchDate) {
		try {
			DateTime batchDateTime = new DateTime(batchDate);
			return em.createQuery("select o from Workstore o " + // 20160616 index check : WaitTimeRptStat.hospCode,workstoreCode,year,month,day,hour : UI_WAIT_TIME_RPT_STAT_01
					"where o.hospital.hospitalCluster.clusterCode = :clusterCode " +
					"and o.status = :status " +
					"and o.hospital.status = :status " +
					"and not exists ( " +
					"				select w from WaitTimeRptStat w " +
					"			    where w.hospCode = o.hospCode " +
					"				and w.workstoreCode = o.workstoreCode " +
					"				and w.year = :year " +
					"				and w.month = :month " +
					"				and w.day = :day " +
					"				and w.hour = :hour " +
			")")
			.setParameter("clusterCode", clusterCode)
			.setParameter("status", RecordStatus.Active)
			.setParameter("year", Integer.valueOf(batchDateTime.getYear()))
			.setParameter("month", Integer.valueOf(batchDateTime.getMonthOfYear()))
			.setParameter("day", Integer.valueOf(batchDateTime.getDayOfMonth()))
			.setParameter("hour", Integer.valueOf(0))
			.getResultList();
		} catch (Exception e) {
			logger.fatal("System can't retrieve workstore by [clusterCode:#0, batchDate:#1]", e, clusterCode, batchDate);				
		}
		return new ArrayList<Workstore>();
	}

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void createWaitTimeRptStat(String hospCode, String workstoreCode, Date batchDate) {
		try {
			
			DateTime fromDate = new DateTime(batchDate);
			DateTime toDate = fromDate.plusDays(1);

			Query query = em.createNativeQuery(
					"SELECT to_char(d.check_date, 'HH24') AS timeRange, " + // index check : DispOrder.workstore,checkDate : I_DISP_ORDER_06
					"round(avg(time_diff('MI',t.create_date,d.check_date))) as avgTime " + 
					"FROM pharm_order p, disp_order d, ticket t, med_order m " +		
					"WHERE d.pharm_order_id = p.id " +
					"AND d.ticket_id = t.id " +
					"AND p.med_order_id = m.id " +
					"AND d.check_date IS NOT NULL " +
					"AND d.org_disp_order_id = d.id " +
					"AND d.status not in (?1, ?2) " +
					"AND d.check_date BETWEEN ?3 AND ?4 " +
					"AND d.hosp_code = ?5 " +
					"AND d.workstore_code = ?6 " +				
					"AND p.status not in (?7,?8) " +
					"AND t.ticket_date = ?9 " +
					"AND m.order_type = 'O' " +
					"AND d.admin_status = ?10 " +
					"AND d.suspend_code is null " +
					"AND p.allow_comment_type  = ?11 " +
					"AND p.allow_comment_before_flag <> 1 " +
					"AND p.admin_status = ?12 " +
					"AND time_diff('MI',t.create_date,d.check_date) <= ?13 " +
					"GROUP BY to_char(d.check_date, 'HH24') " +
			"ORDER BY to_char(d.check_date, 'HH24')" );

			JavaBeanResult.setQueryResultClass(query, EdsElapseTime.class);

			List<EdsElapseTime> resultList = query.setParameter(1, DispOrderStatus.Deleted.getDataValue())
			.setParameter(2, DispOrderStatus.SysDeleted.getDataValue())
			.setParameter(3, fromDate.toDate())
			.setParameter(4, toDate.toDate())
			.setParameter(5, hospCode)
			.setParameter(6, workstoreCode)
			.setParameter(7, PharmOrderStatus.Deleted.getDataValue())
			.setParameter(8, PharmOrderStatus.SysDeleted.getDataValue())
			.setParameter(9, batchDate)
			.setParameter(10, DispOrderAdminStatus.Normal.getDataValue())
			.setParameter(11, AllowCommentType.None.getDataValue())
			.setParameter(12, PharmOrderAdminStatus.Normal.getDataValue())
			.setParameter(13, Prop.REPORT_WAITINGTIME_DURATION_EXCLUSION.get())
			.getResultList();

			logger.info("fromDate:#0, toDate:#1, " +
					"hospCode:#2, workstoreCode:#3, " +
					"pharmOrderStatus1:#4, pharmOrderStatus2:#5, " +
					"batchDate:#6, REPORT_WAITINGTIME_DURATION_EXCLUSION:#7", 				
					fromDate.toDate(), 
					toDate.toDate(), 
					hospCode, 
					workstoreCode, 
					PharmOrderStatus.Deleted.getDataValue(), 
					PharmOrderStatus.SysDeleted.getDataValue(),
					batchDate,
					Prop.REPORT_WAITINGTIME_DURATION_EXCLUSION.get());

			WaitTimeRptStat waitTimeRptStat;		
			DateTime statDate = new DateTime(batchDate); 

			//default record for 00:00 to 01:00
			waitTimeRptStat = getNewWaitTimeRptStat(hospCode, workstoreCode, statDate);
			waitTimeRptStat.setHour(0);
			waitTimeRptStat.setWaitTime(0);
			em.persist(waitTimeRptStat);
			em.flush();

			if (resultList.size() == 0) {
				return;
			}

			for (EdsElapseTime elapseTime : resultList) {
				if (Integer.valueOf(elapseTime.getTimeRange()).intValue() == 0 && waitTimeRptStat.getHour().intValue() == 0){
					waitTimeRptStat.setWaitTime(elapseTime.getAvgTime());
					em.merge(waitTimeRptStat);
				} else {
					waitTimeRptStat = getNewWaitTimeRptStat(hospCode, workstoreCode, statDate);
					waitTimeRptStat.setHour(Integer.valueOf(elapseTime.getTimeRange()));
					waitTimeRptStat.setWaitTime(elapseTime.getAvgTime());
					em.persist(waitTimeRptStat);
				}
			}
			em.flush();
		} catch (Exception e) {
			logger.fatal("System can't create wait time report statistic on [hospCode:#0, workstore:#1, batchDate:#2]", e,
					hospCode, workstoreCode, batchDate);
		}
	}

	public void createWaitTimeRptStat(String clusterCode, Date batchDate) {
		for (Workstore workstore : getNotYetGeneratedWorkstoreList(clusterCode, batchDate)) {
			createWaitTimeRptStat(workstore.getHospCode(), workstore.getWorkstoreCode(), batchDate);
		}
	}
}
