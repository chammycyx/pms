package hk.org.ha.model.pms.vo.rx;

import hk.org.ha.model.pms.cache.DrugSiteMappingCacher;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.corp.cache.DmFormCacher;
import hk.org.ha.model.pms.corp.cache.DmFormDosageUnitMappingCacher;
import hk.org.ha.model.pms.corp.cache.DmFormVerbMappingCacher;
import hk.org.ha.model.pms.corp.cache.DmRegimenCacher;
import hk.org.ha.model.pms.corp.cache.DmSiteCacher;
import hk.org.ha.model.pms.corp.cache.FormVerbCacher;
import hk.org.ha.model.pms.corp.cache.SiteFormVerbCacher;
import hk.org.ha.model.pms.dms.persistence.DmFormDosageUnitMapping;
import hk.org.ha.model.pms.dms.persistence.DmFormVerbMapping;
import hk.org.ha.model.pms.dms.persistence.DmRegimen;
import hk.org.ha.model.pms.dms.persistence.DmSite;
import hk.org.ha.model.pms.dms.vo.DrugRouteCriteria;
import hk.org.ha.model.pms.dms.vo.DrugSiteMapping;
import hk.org.ha.model.pms.dms.vo.FormVerb;
import hk.org.ha.model.pms.dms.vo.SupplSite;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.vetting.RegimenDurationUnit;
import hk.org.ha.model.pms.udt.vetting.RegimenType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
import org.jboss.seam.contexts.Contexts;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Regimen")
@ExternalizedBean(type=DefaultExternalizer.class)
public class Regimen implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@XmlAttribute(name="type", required = true)
	private RegimenType type;

	@XmlElement
	private Integer durationInDay;
	
	@XmlElement(name="DoseGroup", required = true)
	private List<DoseGroup> doseGroupList;
	
	@XmlTransient
	private Integer duration;

	@XmlTransient
	private RegimenDurationUnit durationUnit;

	@XmlTransient
	private DmRegimen dmRegimen;

	private static PmsSiteComparator siteComparator = new PmsSiteComparator();
	
	public void clearDmInfo() {
		if (dmRegimen != null) {
			dmRegimen = null;
		}
		for (DoseGroup doseGroup : this.getDoseGroupList()) {				
			doseGroup.clearDmInfo();
		}
	}
	
	public void loadDmInfo() 
	{
		if (dmRegimen == null && type != null) {
			dmRegimen = DmRegimenCacher.instance().getRegimenByRegimenCode(type.getDataValue());
		}
		for (DoseGroup doseGroup : this.getDoseGroupList()) {				
			doseGroup.loadDmInfo();
		}
	}		
	
	private void loadDosageUnitListByFormCode( String formCode ) {		
		List<DmFormDosageUnitMapping> dmFormDosageUnitMappingList = DmFormDosageUnitMappingCacher.instance().getDmFormDosageUnitMappingListByFormCode(formCode);		
		Dose firstDose = null;
		if ( dmFormDosageUnitMappingList == null || doseGroupList == null || doseGroupList.size() == 0 
				|| doseGroupList.get(0).getDoseList() == null || doseGroupList.get(0).getDoseList().size() == 0 ) {			
			return;
		}		
		firstDose = this.doseGroupList.get(0).getDoseList().get(0);	
		firstDose.setDosageUnitList(new ArrayList<String>());
		for ( DmFormDosageUnitMapping dmFormDosageUnitMapping : dmFormDosageUnitMappingList ) {
			firstDose.getDosageUnitList().add(dmFormDosageUnitMapping.getCompId().getDosageUnit());
		}		
	}		

	public void carryDurationInfo(Integer duration, RegimenDurationUnit durationUnit) {
		if ( duration == null || durationUnit == null ) {
			return;			
		}

		if (!(type == RegimenType.Daily || type == RegimenType.Weekly)) {
			return;
		}

		for (DoseGroup doseGroup : this.getDoseGroupList()){
			if ( type == RegimenType.Weekly ) {
				if ( doseGroup.getDurationUnit() == durationUnit ) {
					doseGroup.setDuration(duration);
				}
			} else {
				doseGroup.setDuration(duration);
				doseGroup.setDurationUnit(durationUnit);
			}
			doseGroup.setDmRegimen(DmRegimenCacher.instance().getRegimenByRegimenCode(durationUnit.getDataValue()));
		}
	}		
	
	public int calTotalDurationInDay() 
	{
		int tmpTotalDuration = 0;
		int tmpTotalDurationInDay = 0;
	
		for (DoseGroup doseGroup : getDoseGroupList()) {				
								
			tmpTotalDurationInDay += doseGroup.getDurationInDay();	

			if(doseGroup.getDuration() != null && type != null) {
				if( type == RegimenType.StepUpDown || type == RegimenType.Daily ) {
					tmpTotalDuration = tmpTotalDurationInDay;	
				} else {
					tmpTotalDuration += doseGroup.getDuration().intValue();
				}					
			}
		}

		if ( tmpTotalDurationInDay == 0 ) {			
			duration = Integer.valueOf(0);
			durationInDay = Integer.valueOf(0);
		} else {
			duration = Integer.valueOf(tmpTotalDuration);
			durationInDay = Integer.valueOf(tmpTotalDurationInDay);				
		}		
		
		return durationInDay.intValue();
	}	
	
	public void loadFormVerb(String formCode) {
		loadFormVerb(formCode, formCode);
	}
	
	public void loadFormVerb( String orgFormCode, String formCode ) {
		for ( DoseGroup doseGroup : getDoseGroupList() ) {
			for ( Dose dose : doseGroup.getDoseList() ) {
				//current form verb desc
				DmFormVerbMapping dmFormVerbMapping = DmFormVerbMappingCacher.instance().getDmFormVerbMappingByFormCodeSiteCode(formCode, dose.getSiteCode());				
				if ( dmFormVerbMapping != null ) {
					dose.setFormVerb( dmFormVerbMapping.getVerbEng() );
				}
				//orginal form verb
				FormVerb orgFormVerb = SiteFormVerbCacher.instance().getDefaultFormVerbByFormCode(orgFormCode);
				if ( orgFormVerb != null ) {
					dose.setDefaultFormVerb(orgFormVerb);					
				}
			}
		}
	}
	
	public void loadFormVerbForDischarge(DrugRouteCriteria criteria) {
		loadFormVerb(criteria.getFormCode(), criteria.getFormCode());
		if (getFirstDose().getDefaultFormVerb() == null) {
			return;
		}
		if (isValidSiteCode(getFirstDose().getDefaultFormVerb().getSiteCode(), criteria)) {
			return;
		}
		for ( DoseGroup doseGroup : getDoseGroupList() ) {
			for ( Dose dose : doseGroup.getDoseList() ) {
				dose.setDefaultFormVerb(null);
			}
		}
	}
	
	public boolean isValidSiteCode(String siteCode, DrugRouteCriteria criteria) {

		for (Site site : DrugSiteMappingCacher.instance().getDischargeOrderSiteList(criteria)) {
			if (StringUtils.equalsIgnoreCase(site.getSiteCode(), siteCode)) {
				return true;
			}
		}

		for (String otherSiteCode : buildMpOthersSiteList(DrugSiteMappingCacher.instance().getDrugSiteMappingListByDrugRouteCriteria(criteria), criteria)) {
			if (StringUtils.equalsIgnoreCase(otherSiteCode, siteCode)) {
				return true;
			}
		}
		
		return false;
	}
	
	private List<String> buildMpOthersSiteList(List<DrugSiteMapping> drugSiteMappingList, DrugRouteCriteria criteria) {
		List<String> otherSiteList = new ArrayList<String>();
		if ( drugSiteMappingList == null ) {
			return otherSiteList;
		}
		
		boolean parenteralFlag = false;
		
		if (StringUtils.isEmpty(criteria.getItemCode())) {
			parenteralFlag = DmFormCacher.instance().getFormByFormCode(criteria.getFormCode()).getRank().startsWith("9");
		} else {
			parenteralFlag = DmDrugCacher.instance().getDmDrug(criteria.getItemCode()).getDmForm().getRank().startsWith("9");
		}
		for (DrugSiteMapping drugSiteMapping : drugSiteMappingList ) {
			if ("CF".equals(drugSiteMapping.getSiteCategoryCode()) || "IF".equals(drugSiteMapping.getSiteCategoryCode())) {
				continue;
			}
			if ( !parenteralFlag ) {
				if ( drugSiteMapping.getGreyListLevel() != null && drugSiteMapping.getGreyListLevel().intValue() == 2 ) {
					otherSiteList.addAll(constructSiteListByDrugSiteMapping( drugSiteMapping ));	
				}								
			} else {
				if (drugSiteMapping.getGreyListLevel() == null) {
					continue;
				}
				otherSiteList.addAll(constructSiteListByDrugSiteMapping( drugSiteMapping ));
			}
		}			
	
		return otherSiteList;
	}
	
	private List<String> constructSiteListByDrugSiteMapping(DrugSiteMapping drugSiteMapping) {				

		List<String> siteList = new ArrayList<String>();
		
		if ( drugSiteMapping.getSite() == null ) {
			return siteList;
		}

		siteList.add(drugSiteMapping.getSite().getSiteCode());
		
		SupplSite[] supplSiteArray = drugSiteMapping.getSite().getSupplSites();
		if ( supplSiteArray == null || supplSiteArray.length == 0 ) {
			return siteList;
		}

		List<String> supplSiteList = new ArrayList<String>();	
		for (int k=0; k<supplSiteArray.length; k++) {
			supplSiteList.add(supplSiteArray[k].getSupplSiteEng());
		}	
		siteList.addAll(supplSiteList);
		
		return siteList;
	}
	
	public void loadOpPharmLineList(String orgFormCode, String formCode) {
		loadOpSiteList(orgFormCode);
		loadDosageUnitListByFormCode(formCode);
		if (orgFormCode != null && formCode != null) {
			loadFormVerb(orgFormCode, formCode);
		}
	}
	
	public void loadDischargePharmLineList(String itemCode, String orgFormCode, String formCode) {
		Workstore workstore = (Workstore) Contexts.getSessionContext().get("workstore");
		loadMpOrDischargeSiteList(workstore, itemCode);		
		loadDosageUnitListByFormCode(formCode);
		if (orgFormCode != null && formCode != null) {
			loadFormVerb(orgFormCode, formCode);
		}
	}
	
	public void loadManualEntryPharmLineList(String itemCode, String orgFormCode, String formCode) {
		loadDosageUnitListByFormCode(formCode);
		if (orgFormCode != null && formCode != null) {
			loadFormVerb(orgFormCode, formCode);
		}
	}
	
	public void loadSiteList(String formCode, boolean isMpDischarge) {
		//retrieve siteList		
		ArrayList<Site> siteList = new ArrayList<Site>();
		List<DmSite> dmSiteList = DmSiteCacher.instance().getSiteList();
		
		List<FormVerb> formVerbList;
		if ( StringUtils.isEmpty(formCode) ) {
			//load free text site list both OP and MP
			if (isMpDischarge) {
				for (DmSite dmSite:dmSiteList) {
					if ("B".equals(dmSite.getUsageType()) || "I".equals(dmSite.getUsageType())) {
						siteList.add(new Site(dmSite, null, !isMpDischarge));
					}
				}
				Collections.sort(siteList, siteComparator);
			} else {
				formVerbList = FormVerbCacher.instance().getFormVerbList();
				for ( FormVerb fv : formVerbList ) {
					siteList.add(new Site(fv.getSiteCode(), null, true));
				}				
			}
		} else {
			//load OP site list by specified form code
			formVerbList = FormVerbCacher.instance().getFormVerbListByFormCode(formCode);
			for ( FormVerb fv : formVerbList ) {				
				siteList.add(new Site(fv.getSiteCode(), fv.getSupplementarySite(), true));
			}
		}
		
		siteList.add( buildOtherSite() );

		//assign siteList to each Dose
		for ( DoseGroup doseGroup : this.getDoseGroupList() ) {
			for ( Dose dose : doseGroup.getDoseList() ) {				
				dose.setSiteList(siteList);
			}		
		}		
	}
	
	private Site buildOtherSite() {
		Site site = new Site();
		site.setSiteCode( "OTHERS" );
		site.setSupplSiteDesc( "OTHERS" );
		site.setSiteLabelDesc( "OTHERS" );
		return site;
	}
	
	public void loadOpSiteList(String formCode) {
		loadSiteList(formCode, false);
	}

	
	public void loadDischargeOrderLineSiteList(DrugRouteCriteria criteria) {
		List<Site> siteList = DrugSiteMappingCacher.instance().getDischargeOrderSiteList(criteria);
		for ( DoseGroup doseGroup : doseGroupList ) {
			for ( Dose dose : doseGroup.getDoseList() ) {
				dose.setSiteList(siteList);
			} 
		}
	}
		
	private void loadMpOrDischargeSiteList(Workstore workstore, String itemCode) {
		Dose firstDose = getFirstDose();
		boolean isParenteral = false; 
		if (firstDose.getDmSite() != null && firstDose.getDmSite().getDmSiteCategory() != null) {
			isParenteral = "Y".equals(firstDose.getDmSite().getDmSiteCategory().getParenteralSite());
		}
		
		if (!isParenteral) {
			if (workstore != null && !StringUtils.isEmpty(itemCode)) {
				DrugRouteCriteria criteria = new DrugRouteCriteria();
				criteria.setDispHospCode(workstore.getHospCode());
				criteria.setDispWorkstore(workstore.getWorkstoreCode());
				criteria.setItemCode(itemCode);
				List<Site> siteList = DrugSiteMappingCacher.instance().getIpSiteList(criteria);
				if (siteList.isEmpty()) {
					siteList.add(buildOtherSite());
				}
				for ( DoseGroup doseGroup : doseGroupList ) {
					for ( Dose dose : doseGroup.getDoseList() ) {
						dose.setSiteList( siteList );
					} 
				}				
			}	
		} else {
			for ( DoseGroup doseGroup : doseGroupList ) {
				for ( Dose dose : doseGroup.getDoseList() ) {
					Site site = new Site();
					site.setSiteCode( firstDose.getDmSite().getSiteCode() );
					
					DmSite dmSite = firstDose.getDmSite();
					
					site.setSiteCategoryCode(dmSite.getSiteCategoryCode());
					site.setSiteDescEng(dmSite.getSiteDescEng());
					site.setIpmoeDesc(dmSite.getIpmoeDesc());
										
					if ( firstDose.getDmSupplSite() != null ) {
						site.setIsSupplSite(Boolean.TRUE);
						site.setSupplSiteDesc( firstDose.getDmSupplSite().getCompId().getSupplSiteEng() );
						site.setSiteLabelDesc( firstDose.getDmSupplSite().getCompId().getSupplSiteEng() );
					} else {
						site.setIsSupplSite(Boolean.FALSE);
						site.setSupplSiteDesc( null );
						site.setSiteLabelDesc( firstDose.getDmSite().getIpmoeDesc() );
					}																
					dose.setSiteList( Arrays.asList(site) );
				} 
			}
		}
	}
	
	public void loadListForMp(String itemCode, String formCode ) {	
		if (!StringUtils.isEmpty(formCode)) {
			loadDosageUnitListByFormCode(formCode);	
			loadFormVerb(formCode, formCode);
		}
	}
	
	public Dose getFirstDose() {
		return doseGroupList.get(0).getDoseList().get(0);
	}
		
	public List<String> getDailyFreqCodeList() {
		List<String> dailyFreqCodeList = new ArrayList<String>();
		for (DoseGroup doseGroup:getDoseGroupList()) {
			for (Dose dose:doseGroup.getDoseList()) {				
				if (!StringUtils.isBlank(dose.getDailyFreq().getCode())) {
					dailyFreqCodeList.add(dose.getDailyFreq().getCode());
				}
			}
		}	
		return dailyFreqCodeList;
	}
	
	public void setDoseGroupList(List<DoseGroup> doseGroupList) {
		this.doseGroupList = doseGroupList;
	}

	public List<DoseGroup> getDoseGroupList() {
		if (doseGroupList == null) {
			doseGroupList = new ArrayList<DoseGroup>();
		}
		return doseGroupList;
	}
	
	public Integer getDuration() {
		return duration;
	}
	
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	
	public void setDurationUnit(RegimenDurationUnit durationUnit) {
		this.durationUnit = durationUnit;
	}

	public RegimenDurationUnit getDurationUnit() {
		return durationUnit;
	}	
	
	public RegimenType getType() {
		return type;
	}

	public void setType(RegimenType type) {
		this.type = type;
	}
	
	public DmRegimen getDmRegimen() {
		return dmRegimen;
	}
	
	public void setDmRegimen(DmRegimen dmRegimen) {
		this.dmRegimen = dmRegimen;
	}
	
	public Integer getDurationInDay() {
		return durationInDay;
	}
	
	public void setDurationInDay(Integer durationInDay) {
		this.durationInDay = durationInDay;
	}
	
	public static class PmsSiteComparator implements Comparator<Site>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(Site site1, Site site2) {
			return site1.getSiteLabelDesc().compareTo(site2.getSiteLabelDesc());
		}
		
	}
}
