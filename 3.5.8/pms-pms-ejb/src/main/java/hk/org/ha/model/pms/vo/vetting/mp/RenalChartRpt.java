package hk.org.ha.model.pms.vo.vetting.mp;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class RenalChartRpt implements Serializable {

	private static final long serialVersionUID = 1L;

	private Date createDate;
	
	private BigDecimal ecrcl;

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public BigDecimal getEcrcl() {
		return ecrcl;
	}

	public void setEcrcl(BigDecimal ecrcl) {
		this.ecrcl = ecrcl;
	}
}
