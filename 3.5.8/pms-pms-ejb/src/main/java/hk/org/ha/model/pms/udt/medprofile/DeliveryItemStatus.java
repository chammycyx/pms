package hk.org.ha.model.pms.udt.medprofile;

import java.util.Arrays;
import java.util.List;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DeliveryItemStatus implements StringValuedEnum {

	Printed("P", "Printed"),
	Assembled("A", "Assembled"),
	Checked("C", "Checked"),
	Delivered("E", "Sent"),
	KeepRecord("K", "Keep Record"),
	Unlink("U", "Delinked"), 
	Deleted("D", "Deleted");
	
    private final String dataValue;
    private final String displayValue;
    
    public static final List<DeliveryItemStatus> Printed_Assembled_Checked_Delivered = Arrays.asList(Printed, Assembled, Checked, Delivered);
    public static final List<DeliveryItemStatus> KeepRecord_Unlink = Arrays.asList(KeepRecord, Unlink);
    public static final List<DeliveryItemStatus> Not_Deleted = Arrays.asList(StringValuedEnumReflect.getValuesExclude(DeliveryItemStatus.class, Deleted));
    
	private DeliveryItemStatus(String dataValue, String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}

	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}
	
    public static DeliveryItemStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DeliveryItemStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<DeliveryItemStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<DeliveryItemStatus> getEnumClass() {
    		return DeliveryItemStatus.class;
    	}
    }
}
