package hk.org.ha.model.pms.vo.reftable.pivas;

import hk.org.ha.model.pms.dms.persistence.DmDiluent;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PivasSolventInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private Boolean whiteSolvent;
	
	private DmDiluent dmDiluent;

	public Boolean getWhiteSolvent() {
		return whiteSolvent;
	}

	public void setWhiteSolvent(Boolean whiteSolvent) {
		this.whiteSolvent = whiteSolvent;
	}

	public DmDiluent getDmDiluent() {
		return dmDiluent;
	}

	public void setDmDiluent(DmDiluent dmDiluent) {
		this.dmDiluent = dmDiluent;
	}
}