package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.cache.CompanyCacherInf;
import hk.org.ha.model.pms.dms.persistence.Company;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("companyListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CompanyListServiceBean implements CompanyListServiceLocal {
	
	@In
	private CompanyCacherInf companyCacher;
	
	@In(required = false)
	@Out(required = false)
	private List<Company> companyList;
	
	@In(required = false)
	@Out(required = false)
	private List<Company> manufList;
	
	public void retrieveCompanyList(String prefixCompanyCode) {
		if (StringUtils.isEmpty(prefixCompanyCode)) {
			companyList = companyCacher.getCompanyList();
		} else {
			companyList = companyCacher.getCompanyListByCompanyCode(prefixCompanyCode);
		}
	}
	
	public void retrieveManufacturerList(String prefixCompanyCode) {
		if (StringUtils.isEmpty(prefixCompanyCode)) {
			manufList = companyCacher.getManufacturerList();
		} else {
			manufList = companyCacher.getManufacturerListByCompanyCode(prefixCompanyCode);
		}
	}
	
	@Remove
	public void destroy() {
		if (companyList != null) {
			companyList = null;
		}
		
		if (manufList != null) {
			manufList = null;
		}
	}

}
