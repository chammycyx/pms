package hk.org.ha.model.pms.cache;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.util.StringArray;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.corp.cache.DmFormCacher;
import hk.org.ha.model.pms.corp.cache.SiteFormVerbCacher;
import hk.org.ha.model.pms.dms.vo.Drug;
import hk.org.ha.model.pms.dms.vo.DrugRouteCriteria;
import hk.org.ha.model.pms.dms.vo.DrugSiteMapping;
import hk.org.ha.model.pms.dms.vo.FormVerb;
import hk.org.ha.model.pms.dms.vo.SupplSite;
import hk.org.ha.model.pms.vo.rx.Site;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("drugSiteMappingCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class DrugSiteMappingCacher extends BaseCacher implements DrugSiteMappingCacherInf {

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;			
	
	private Map<StringArray, InnerCacher> cacherMap = new HashMap<StringArray, InnerCacher>();
	
	@Override
	public void clear() {
		synchronized (this) {
			cacherMap.clear();
		}
	}
	
	private Site constructSiteListByFormVerb(FormVerb fv) {
		if ( StringUtils.isEmpty(fv.getSupplementarySite()) ) {
			return new Site(fv.getSiteCode(), null, false);
		} else {
			return new Site(fv.getSiteCode(), fv.getSupplementarySite(), false);
		}
	}
	
	private void addPrincipleAndSupplSite(List<Site> siteList, DrugSiteMapping drugSiteMapping) {
		siteList.add(new Site(drugSiteMapping.getSiteCode(), null, false));
		if (drugSiteMapping.getSite().getSupplSites() != null) {
			for (SupplSite supplSite: drugSiteMapping.getSite().getSupplSites()) {
				siteList.add(new Site(drugSiteMapping.getSiteCode(), supplSite.getSupplSiteEng(), false));
			}
		}
	}
	
	private void addOpDefaultSite(List<Site> siteList, String itemCode) {
		FormVerb formVerb = SiteFormVerbCacher.instance().getDefaultFormVerbByFormCode(DmDrugCacher.instance().getDmDrug(itemCode).getDmForm().getFormCode());
		if (formVerb == null) {
			return;
		}
		for (Site site : siteList) {
			if (StringUtils.equals(site.getSiteCode(), formVerb.getSiteCode()) && StringUtils.equals(formVerb.getSupplementarySite(), site.getSupplSiteDesc())) {
				return;
			}
		}
		if (StringUtils.isEmpty(formVerb.getSupplementarySite())) {
			siteList.add(0, new Site(formVerb.getSiteCode(),null,false));
		} else {
			siteList.add(0, new Site(formVerb.getSiteCode(),formVerb.getSupplementarySite(),false));
		}
	}		
	
	private Site buildOtherSite() {
		Site site = new Site();
		site.setSiteCode( "OTHERS" );
		site.setSupplSiteDesc( "OTHERS" );
		site.setSiteLabelDesc( "OTHERS" );
		site.setIsSupplSite(Boolean.FALSE);		
		return site;		
	}
	
	public List<Site> getIpSiteList(DrugRouteCriteria criteria) {	
		List<Site> resultList = new ArrayList<Site>();
		boolean parenteralFlag = false;
		if ( StringUtils.isNotBlank(criteria.getFormCode()) ) {
			parenteralFlag = DmFormCacher.instance().getFormByFormCode(criteria.getFormCode()).getRank().startsWith("9");
		} else if( StringUtils.isNotBlank(criteria.getItemCode()) ) {
			parenteralFlag = DmDrugCacher.instance().getDmDrug(criteria.getItemCode()).getDmForm().getRank().startsWith("9");
		} else {
			return resultList;
		}
		if (!parenteralFlag) {
			for ( DrugSiteMapping drugSiteMapping : getDrugSiteMappingListByDrugRouteCriteria(criteria) ) {
				if ( drugSiteMapping.getFormVerb() != null 
						&& drugSiteMapping.getGreyListLevel() != null
						&& drugSiteMapping.getGreyListLevel().intValue() == 1 
						&& StringUtils.equals(drugSiteMapping.getParenteralSite(), "N") ){
					resultList.add( constructSiteListByFormVerb(drugSiteMapping.getFormVerb()) );
				}				
			}
			resultList.add(buildOtherSite());
		}		
		return resultList;
	}

	public List<Site> getDischargeOrderSiteList(DrugRouteCriteria criteria) {

		List<Site> resultList = new ArrayList<Site>();

		boolean parenteralFlag = DmFormCacher.instance().getFormByFormCode(criteria.getFormCode()).getRank().startsWith("9");
		
		for ( DrugSiteMapping drugSiteMapping : getDrugSiteMappingListByDrugRouteCriteria(criteria) ) {
			if ("CF".equals(drugSiteMapping.getSiteCategoryCode()) || "IF".equals(drugSiteMapping.getSiteCategoryCode())) {
				continue;
			}
			if (parenteralFlag) {
				if (!"W".equals(drugSiteMapping.getCompatibleType())) {
					continue;
				}
			}
			else {
				if (drugSiteMapping.getGreyListLevel() != 1) {
					continue;
				}
			}
			resultList.add(new Site(drugSiteMapping.getSiteCode(), drugSiteMapping.getFormVerb() == null ? null : drugSiteMapping.getFormVerb().getSupplementarySite(), false));
		}

		resultList.add(buildOtherSite());

		return resultList;
	}
	
	public List<Site> getManualEntrySiteList(DrugRouteCriteria criteria) {		
		boolean parenteralFlag = DmDrugCacher.instance().getDmDrug(criteria.getItemCode()).getDmForm().getRank().startsWith("9");
		ArrayList<Site> resultList = new ArrayList<Site>();
		for ( DrugSiteMapping drugSiteMapping : getDrugSiteMappingListByDrugRouteCriteria(criteria) ) {
			
			if (parenteralFlag) {
				if ( !StringUtils.equals(drugSiteMapping.getCompatibleType(),"W") ){
					continue;
				}
				addPrincipleAndSupplSite(resultList, drugSiteMapping);
			}
			else {
				if ( drugSiteMapping.getGreyListLevel() != null && drugSiteMapping.getGreyListLevel().intValue() != 1 ){
					continue;
				}
				if ( drugSiteMapping.getFormVerb() != null ) {
					resultList.add( constructSiteListByFormVerb(drugSiteMapping.getFormVerb()) );
				}				
			}						
		}
		addOpDefaultSite(resultList, criteria.getItemCode());
		resultList.add( buildOtherSite() );
		return resultList;
	}
	
	public List<DrugSiteMapping> getDrugSiteMappingListByDrugRouteCriteria(DrugRouteCriteria criteria) {
		return (List<DrugSiteMapping>) this.getCacher(criteria).getAll();
	}
	
	private InnerCacher getCacher( DrugRouteCriteria criteria ) {
		synchronized (this) {			
			StringArray key = new StringArray(criteria.getDispHospCode(),criteria.getDispWorkstore(),criteria.getItemCode(),criteria.getTrueDisplayname(),criteria.getFormCode(),criteria.getSaltProperty());
			InnerCacher cacher = cacherMap.get(key);
			if (cacher == null) {
				cacher = new InnerCacher(criteria, this.getExpireTime());
				cacherMap.put(key, cacher);
			}		
			return cacher;
		}
	}

	private class InnerCacher extends AbstractCacher<String, DrugSiteMapping> {
		private DrugRouteCriteria criteria;
				
		public InnerCacher(DrugRouteCriteria criteriaIn, int expireTime) {
			super(expireTime);
			this.criteria = criteriaIn;
		}

		@Override
		public DrugSiteMapping create(String key) {
			this.getAll();
			return null;
		}

		@Override
		public Collection<DrugSiteMapping> createAll() {						
			List<Drug> drugList = dmsPmsServiceProxy.getDrugRouteForIp(criteria);	
			if ( drugList.size() > 0 ) {
				return Arrays.asList(drugList.get(0).getDrugSiteMappings()); 
			} 
			else {
				return new ArrayList<DrugSiteMapping>();
			}
		}

		@Override
		public String retrieveKey(DrugSiteMapping drugSiteMapping) {
			return null;
		}
	}

	public static DrugSiteMappingCacherInf instance() {
		if (!Contexts.isApplicationContextActive()) {
			throw new IllegalStateException("No active application scope");
		}
		return (DrugSiteMappingCacherInf) Component.getInstance("drugSiteMappingCacher",
				ScopeType.APPLICATION);
	}
}
