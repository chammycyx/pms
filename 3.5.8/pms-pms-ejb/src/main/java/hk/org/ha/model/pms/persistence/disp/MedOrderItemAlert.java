package hk.org.ha.model.pms.persistence.disp;

import hk.org.ha.model.pms.persistence.AlertEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "MED_ORDER_ITEM_ALERT")
public class MedOrderItemAlert extends AlertEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "medOrderItemAlertSeq")
	@SequenceGenerator(name = "medOrderItemAlertSeq", sequenceName = "SQ_MED_ORDER_ITEM_ALERT", initialValue = 100000000)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "MED_ORDER_ITEM_ID", nullable = false)
	private MedOrderItem medOrderItem;
	
	public MedOrderItemAlert() {
	}
	
	public MedOrderItemAlert(AlertEntity alertEntity) {
		this();
		setAlertType(alertEntity.getAlertType());
		setAlertXml(alertEntity.getAlertXml());
		setOverrideReason(alertEntity.getOverrideReason());
		setAckUser(alertEntity.getAckUser());
		setAckDate(alertEntity.getAckDate());
		setAlert(alertEntity.getAlert());
		setAdditiveNum(alertEntity.getAdditiveNum());
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MedOrderItem getMedOrderItem() {
		return medOrderItem;
	}

	public void setMedOrderItem(MedOrderItem medOrderItem) {
		this.medOrderItem = medOrderItem;
	}
	
	public void replaceItemNum(Integer oldItemNum, Integer newItemNum, String orderNum) {
		getAlert().replaceItemNum(oldItemNum, newItemNum, orderNum);
	}
	
	public MedOrderItem getMedItem() {
		return medOrderItem;
	}
}
