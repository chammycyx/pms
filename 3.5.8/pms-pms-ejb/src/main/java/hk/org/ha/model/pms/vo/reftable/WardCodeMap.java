package hk.org.ha.model.pms.vo.reftable;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class WardCodeMap implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Map<String, List<String>> wardCodeMap;

	public Map<String, List<String>> getWardCodeMap() {
		return wardCodeMap;
	}

	public void setWardCodeMap(Map<String, List<String>> wardCodeMap) {
		this.wardCodeMap = wardCodeMap;
	}

}