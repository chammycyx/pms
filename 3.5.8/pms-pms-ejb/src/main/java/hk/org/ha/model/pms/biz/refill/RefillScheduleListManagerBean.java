package hk.org.ha.model.pms.biz.refill;

import static hk.org.ha.model.pms.prop.Prop.ORDER_ALLOWUPDATE_DURATION;
import static hk.org.ha.model.pms.prop.Prop.SUSPEND_ORDER_SUSPENDCODE;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.drug.ItemConvertManagerLocal;
import hk.org.ha.model.pms.biz.onestop.SuspendPrescManagerLocal;
import hk.org.ha.model.pms.biz.reftable.RefillHolidayManagerLocal;
import hk.org.ha.model.pms.biz.vetting.OrderViewManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.persistence.disp.RefillScheduleItem;
import hk.org.ha.model.pms.persistence.reftable.DrsConfig;
import hk.org.ha.model.pms.persistence.reftable.RefillConfig;
import hk.org.ha.model.pms.persistence.reftable.RefillHoliday;
import hk.org.ha.model.pms.persistence.reftable.RefillQtyAdj;
import hk.org.ha.model.pms.persistence.reftable.RefillQtyAdjExclusion;
import hk.org.ha.model.pms.udt.disp.DiscontinueStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderDayEndStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.refill.DocType;
import hk.org.ha.model.pms.udt.refill.RefillScheduleActionType;
import hk.org.ha.model.pms.udt.refill.RefillScheduleItemStatus;
import hk.org.ha.model.pms.udt.refill.RefillScheduleRefillStatus;
import hk.org.ha.model.pms.udt.refill.RefillScheduleStatus;
import hk.org.ha.model.pms.vo.alert.mds.MedProfileAlertMsg;
import hk.org.ha.model.pms.vo.onestop.OneStopOrderInfo;
import hk.org.ha.model.pms.vo.refill.RefillPrescInfo;
import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.joda.time.Days;

@AutoCreate
@Stateless
@Name("refillScheduleListManager")
@MeasureCalls
@SuppressWarnings("unchecked")
public class RefillScheduleListManagerBean implements RefillScheduleListManagerLocal {

	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
			
	@In
	private Workstore workstore;
	
	@In
	private ItemConvertManagerLocal itemConvertManager;	
	
	@In
	private RefillScheduleManagerLocal refillScheduleManager;
	
	@In
	private RefillHolidayManagerLocal refillHolidayManager;
	
	@In
	private OrderViewManagerLocal orderViewManager;

	@In
	private SuspendPrescManagerLocal suspendPrescManager;
	
	@In
	private RefillConfig vettingRefillConfig;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private OrderViewInfo orderViewInfo;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private List<RefillSchedule> refillScheduleList;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private List<Integer> refillPoiItemNumList;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private List<Integer> refillPoiItemNumInitList;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private List<RefillHoliday> refillHolidayListForRefillEdit;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private RefillPrescInfo refillPrescInfo;

	@In(scope=ScopeType.SESSION, required=false)
	private DrsConfig vettingDrsConfig;
	
	public void retrieveRefillScheduleByOrderNum(String orderNum) {
		logger.info("retrieveRefillScheduleByOrderNum #0", orderNum);

		refillScheduleList = em.createQuery(
				"select o from RefillSchedule o" + // 20120307 index check : MedOrder.orderNum : I_MED_ORDER_01
				" where o.pharmOrder.medOrder.orderNum = :orderNum")
			.setParameter("orderNum", orderNum)
			.getResultList();
	}
	
	private boolean isAllowQtyAdjust(PharmOrderItem pharmOrderItem){
		if( pharmOrderItem.getMedOrderItem().getDiscontinueStatus() != null &&
			pharmOrderItem.getMedOrderItem().getDiscontinueStatus() != DiscontinueStatus.None ){
			return false;
		}
		
		if( vettingRefillConfig.getEnableQtyAdjFlag() ){
			
			List<RefillQtyAdjExclusion> refillQtyAdjExclusionList = em.createQuery(
					"select o from RefillQtyAdjExclusion o" + // 20120303 index check : RefillQtyAdjExclusion.workstore,itemCode : UI_REFILL_QTY_ADJ_EXCLUSION_01
					" where o.workstore = :workstore" +
					" and o.itemCode = :itemCode")
					.setParameter("workstore", workstore)
					.setParameter("itemCode", pharmOrderItem.getItemCode())
					.getResultList();
			
			if( refillQtyAdjExclusionList.size() == 0 ){
				
				List<RefillQtyAdj> refillQtyAdjList = em.createQuery(
						"select o from RefillQtyAdj o" + // 20120303 index check : RefillQtyAdj.workstore,formCode : UI_REFILL_QTY_ADJ_01
						" where o.workstore = :workstore" +
						" and o.formCode = :formCode")
						.setParameter("workstore", workstore)
						.setParameter("formCode", pharmOrderItem.getFormCode())
						.getResultList();
				
				
				if( refillQtyAdjList.size() > 0 ){
					return true;
				}
			}
		}
		return false;
	}
		
	private boolean isLateRefill(RefillSchedule nextRs){
		
		if( !vettingRefillConfig.getEnableQtyAdjFlag() ){
			return false;
		}
			
		DateMidnight refillDueDate = new DateMidnight(nextRs.getBeforeAdjRefillDate());
		refillDueDate = refillDueDate.plusDays(vettingRefillConfig.getLateRefillAlertDay());		
		
		if( refillDueDate.compareTo(new DateMidnight()) < 0 ){			
			for( RefillScheduleItem refillScheduleItem : nextRs.getRefillScheduleItemList() ){
				if( isAllowQtyAdjust(refillScheduleItem.getPharmOrderItem()) ){
					return true;
				}
			}
		}
		
		return false;		
	}
	
	private boolean isAbortRefill(RefillSchedule lastRefillSchedule, RefillSchedule initRefillSchedule){
		DateMidnight latestRefillEndDate = new DateMidnight(initRefillSchedule.getBeforeAdjRefillDate());
		for( RefillScheduleItem refillScheduleItem : lastRefillSchedule.getRefillScheduleItemList() ){
			if( latestRefillEndDate.compareTo(new DateMidnight(refillScheduleItem.getRefillEndDate())) < 0 ){
				latestRefillEndDate = new DateMidnight(refillScheduleItem.getRefillEndDate());
			}
		}
		latestRefillEndDate = latestRefillEndDate.plusDays(vettingRefillConfig.getAbortRefillDay());
		if( latestRefillEndDate.compareTo(new DateMidnight()) < 0 ){
			return true;
		}
		return false;
	}
				
	public void updateRefillScheduleListOnly(List<RefillSchedule> refillScheduleListIn, Long retrieveRsId){
		RefillSchedule refillScheduleIn = em.find(RefillSchedule.class, retrieveRsId);
		
		if( !refillScheduleIn.getPharmOrder().getMedOrder().getVersion().equals(orderViewInfo.getMedOrderVersion()) ){
			refreshRefillScheduleList(retrieveRsId, "OrderLock");
			return;
		}
		
		RefillScheduleRefillStatus retrieveRsStatus = refillScheduleIn.getRefillStatus();
		
		List<RefillSchedule> rsList = retrieveRefillScheduleByPharmOrderId(refillScheduleIn.getPharmOrder().getId());
		
		Integer numOfRefill = 0;
		
		Map<Integer, Integer> rsiNumOfRefillMap = new HashMap<Integer,Integer>();
		
		for( RefillSchedule rsIn: refillScheduleListIn ){
			if( rsIn.getGroupNum().equals(refillScheduleIn.getGroupNum()) ){
				numOfRefill = numOfRefill + 1;
			}
			
			for( RefillScheduleItem rsiItem : rsIn.getRefillScheduleItemList() ){				
				if( !rsiNumOfRefillMap.containsKey(rsiItem.getItemNum()) ){
					rsiNumOfRefillMap.put(rsiItem.getItemNum(), rsiItem.getNumOfRefill());
				}
			}
		}
		
		Long currentRsId = null;
		
		// Remove Not Yet Dispensed Refill with Same GroupNum
		for( RefillSchedule refillSchedule : rsList ){
			if( refillScheduleIn.getGroupNum().equals(refillSchedule.getGroupNum()) ){
				if( refillSchedule.getTicket() == null ){
					em.remove(refillSchedule);
				}else{
					refillSchedule.setNumOfRefill(numOfRefill);
				
					for( RefillScheduleItem refillScheduleItem : refillSchedule.getRefillScheduleItemList() ){
						if( rsiNumOfRefillMap.containsKey(refillScheduleItem.getItemNum()) ){
							refillScheduleItem.setNumOfRefill(rsiNumOfRefillMap.get(refillScheduleItem.getItemNum()));
						}
					}
					
					if( RefillScheduleRefillStatus.Current == refillSchedule.getRefillStatus() &&
						refillSchedule.getRefillNum() > 1 &&
						(RefillScheduleStatus.BeingProcessed == refillSchedule.getStatus() || RefillScheduleStatus.Dispensed == refillSchedule.getStatus()) &&
						( DispOrderDayEndStatus.None == refillSchedule.getDispOrder().getDayEndStatus() && !refillSchedule.getDispOrder().getBatchProcessingFlag() ) ){
						currentRsId = refillSchedule.getId();
					}
				}
			}
		}
		
		em.flush();
		em.clear();		
		
		PharmOrder pharmOrder = (PharmOrder) Contexts.getSessionContext().get("pharmOrder");
		Long refreshRsId = retrieveRsId;
		Long nextRsId = null;
		RefillSchedule updateCurrRs = null;
		
		List<Integer> confirmDisconItemList = new ArrayList<Integer>();
		
		for( PharmOrderItem poi : pharmOrder.getPharmOrderItemList() ){
			if( DiscontinueStatus.Confirmed == poi.getMedOrderItem().getDiscontinueStatus() ){
				if( !confirmDisconItemList.contains(poi.getOrgItemNum()) ){
					confirmDisconItemList.add(poi.getOrgItemNum());
				}
			}
		}
				
		for( RefillSchedule updateRs: refillScheduleListIn ){
			if( refillScheduleIn.getGroupNum().equals(updateRs.getGroupNum()) ){
				if( updateRs.getTicket() == null ){
				
					for( RefillScheduleItem updateRsi : updateRs.getRefillScheduleItemList() ){
						DateMidnight refillEndDate = new DateMidnight(updateRs.getBeforeAdjRefillDate());
						refillEndDate = refillEndDate.plusDays(updateRsi.getRefillDuration());
						updateRsi.setRefillEndDate(refillEndDate.toDate());
						updateRsi.setWorkstore(pharmOrder.getWorkstore());
						
						if( confirmDisconItemList.contains(updateRsi.getItemNum())){
							updateRsi.setStatus(RefillScheduleItemStatus.Discontinue);
						}else{
							updateRsi.setStatus(RefillScheduleItemStatus.Normal);
						}
					}
					
					String refillCouponNum = generateRefillCouponNum(workstore.getHospCode(), orderViewInfo.getMedOrderNumForCoupon(),updateRs.getRefillNum(), updateRs.getGroupNum());
					updateRs.setRefillCouponNum(refillCouponNum);
					updateRs.setTrimRefillCouponNum(refillCouponNum.substring(0,14));
					updateRs.setNumOfRefill(numOfRefill);
					updateRs.setWorkstore( pharmOrder.getWorkstore() ); //Original Workstore
					updateRs.clearId();
					updateRs.connect(pharmOrder);
					em.persist(updateRs);
					
					if( RefillScheduleRefillStatus.Next == updateRs.getRefillStatus() ){
						nextRsId = updateRs.getId();
					}
				}else if( currentRsId != null &&  RefillScheduleRefillStatus.Current == updateRs.getRefillStatus() && workstore.equals(updateRs.getTicket().getWorkstore()) ){
					updateCurrRs = updateRs;
				}
				
				if( retrieveRsStatus.equals(updateRs.getRefillStatus()) ){
					refreshRsId = updateRs.getId();
				}else if( updateRs.getRefillNum().equals(updateRs.getRefillNum()) && RefillScheduleRefillStatus.Current == updateRs.getRefillStatus() ){
					refreshRsId = updateRs.getId();
				}
			}
		}
		
		logger.debug("updateRefillScheduleListOnly refreshRefillScheduleList refreshRsId #0", refreshRsId);
		
		if( updateCurrRs != null ){
			boolean requireDispSuspend = false;
			
			Map<Integer,RefillScheduleItem> updateCurrRsiMap = new HashMap<Integer, RefillScheduleItem>();
			
			RefillSchedule currentRs = em.find(RefillSchedule.class, currentRsId);
			DispOrder dispOrder = currentRs.getDispOrder();
			
			for( RefillScheduleItem refillScheduleItem : updateCurrRs.getRefillScheduleItemList() ){
				DispOrderItem dispOrderItem = dispOrder.getDispOrderItemByPoOrgItemNum(refillScheduleItem.getItemNum());

				if( dispOrderItem != null ){
					if( !refillScheduleItem.getRefillQty().equals(Integer.valueOf(dispOrderItem.getDispQty().intValue())) ||
						!refillScheduleItem.getRefillDuration().equals(dispOrderItem.getDurationInDay())  ){
						requireDispSuspend = true;
						updateCurrRsiMap.put(refillScheduleItem.getItemNum(), refillScheduleItem);
					}
				}
			}
			
			if( requireDispSuspend ){

				if( nextRsId != null ){
					RefillSchedule nextRs = em.find(RefillSchedule.class, nextRsId);
					nextRs.setPrintFlag(false);
				}
				
				if( currentRs.getRefillDate().compareTo(updateCurrRs.getRefillDate()) != 0){
					currentRs.setRefillDate(updateCurrRs.getRefillDate());
				}
				
				if( currentRs.getBeforeAdjRefillDate().compareTo(updateCurrRs.getBeforeAdjRefillDate()) != 0){
					currentRs.setBeforeAdjRefillDate(updateCurrRs.getBeforeAdjRefillDate());
				}
				
				for( RefillScheduleItem currRsi : currentRs.getRefillScheduleItemList() ){
					RefillScheduleItem updateRsi = updateCurrRsiMap.get(currRsi.getItemNum());
					if( updateRsi != null ){
						currRsi.setRefillDuration(updateRsi.getRefillDuration());
						currRsi.setSupplyForDay(updateRsi.getRefillDuration());
						currRsi.setCalQty(updateRsi.getCalQty());
						currRsi.setAdjQty(updateRsi.getAdjQty());
						currRsi.setRefillQty(updateRsi.getRefillQty());
						currRsi.setNumOfRefill(updateRsi.getNumOfRefill());
						
						DateMidnight refillEndDate = new DateMidnight(currentRs.getBeforeAdjRefillDate());
						refillEndDate = refillEndDate.plusDays(updateRsi.getRefillDuration());
						currRsi.setRefillEndDate(refillEndDate.toDate());
					}
				}
				
				suspendPrescManager.verifySuspendPrescription(dispOrder.getId(), dispOrder.getVersion(), SUSPEND_ORDER_SUSPENDCODE.get(), "refill");				
			}
		}
		em.flush();
		em.clear();
		
		OneStopOrderInfo tempOneStopInfo = new OneStopOrderInfo();
		tempOneStopInfo.setPhsSpecCode( orderViewInfo.getSpecCode() );
		tempOneStopInfo.setPhsWardCode( orderViewInfo.getWardCode() );
		tempOneStopInfo.setPhsPatCat( orderViewInfo.getPatCatCode() );
		tempOneStopInfo.setDoctorCode( orderViewInfo.getDoctorCode() );
		tempOneStopInfo.setDoctorName( orderViewInfo.getDoctorName() );
		orderViewManager.retrieveOrderByRefillScheduleId(refreshRsId);
		orderViewInfo.bindOneStopOrderInfo(tempOneStopInfo, workstore);
		refreshRefillScheduleList(refreshRsId, null);
	}
	
	private List<Integer> updateRefillSchedulePrintFlagByRsiDirtyFlag(List<RefillSchedule> refillScheduleListIn){
		List<Integer> printFlagList = new ArrayList<Integer>();
		
		for( RefillSchedule updateRs2: refillScheduleListIn ){
			if( updateRs2.getRefillNum() == 1 ){
				for( RefillScheduleItem updateRsi : updateRs2.getRefillScheduleItemList() ){
					if( updateRsi.isDirtyFlag() ){
						if( !printFlagList.contains(updateRs2.getGroupNum()) ){
							printFlagList.add(updateRs2.getGroupNum());
							logger.debug("updateRefillSchedulePrintFlagByRsiDirtyFlag============updateRs2.getGroupNum()======================#0", updateRs2.getGroupNum());
						}
					}
				}
			}
		}		
		return printFlagList;
	}
	
	private List<Integer> updateRefillSchedulePrintFlagByPharmOrderItem(Long pharmOrderId, List<Integer> printFlagListIn, List<RefillSchedule> refillScheduleListIn){
		if( pharmOrderId != null ){
			
			List<RefillSchedule> origRsList = retrieveRefillScheduleByPharmOrderId(pharmOrderId);
			Map<Integer, RefillSchedule> prevRsMap = new HashMap<Integer, RefillSchedule>();
			Map<Integer, Integer> rsiCountMap =  new HashMap<Integer, Integer>();
			
			for( RefillSchedule prevRs : origRsList ){
				if( !printFlagListIn.contains(prevRs.getGroupNum()) && !prevRsMap.containsKey(prevRs.getGroupNum()) && prevRs.getRefillNum() == 1 ){
					prevRsMap.put(prevRs.getGroupNum(), prevRs);
					rsiCountMap.put(prevRs.getGroupNum(), prevRs.getRefillScheduleItemList().size());
				}
			}
			
			for( RefillSchedule currRs : refillScheduleListIn ){
				if( !printFlagListIn.contains(currRs.getGroupNum()) && currRs.getRefillNum() == 1 ){
					if( !prevRsMap.containsKey(currRs.getGroupNum()) ){
						printFlagListIn.add(currRs.getGroupNum());
						logger.debug("updateRefillSchedulePrintFlagByPharmOrderItem1===============================#0", currRs.getGroupNum());
					}else{
						RefillSchedule prevRs = prevRsMap.get(currRs.getGroupNum());
						
						if( !currRs.getNumOfRefill().equals(prevRs.getNumOfRefill()) ||
							!currRs.getRefillInterval().equals(prevRs.getRefillInterval()) ){
							printFlagListIn.add(currRs.getGroupNum());
							logger.debug("updateRefillSchedulePrintFlagByPharmOrderItem2===============================#0", currRs.getGroupNum());
							continue;
						}
						
						Integer rsiCount = 0;
						
						for( RefillScheduleItem currRsi : currRs.getRefillScheduleItemList() ){
							if( currRsi.getActiveFlag() ){
								rsiCount += 1;
							}
						}
						
						if( !rsiCount.equals(rsiCountMap.get(currRs.getGroupNum())) ){
							printFlagListIn.add(currRs.getGroupNum());
							logger.debug("updateRefillSchedulePrintFlagByPharmOrderItem3===============================#0", currRs.getGroupNum());
						}
					}
				}
			}
		}
		return printFlagListIn;
	}
	
	public void updateStdRefillScheduleList(List<RefillSchedule> refillScheduleListIn){
		PharmOrder pharmOrder = (PharmOrder) Contexts.getSessionContext().get("pharmOrder");
		pharmOrder.clearRefillScheduleList();
		logger.debug("updateStdRefillScheduleList pharmOrder.getRefillScheduleList(DocType.Standard) 1 size: #0", pharmOrder.getRefillScheduleList(DocType.Standard).size());
				
		if( refillScheduleListIn != null){
			
			if( orderViewInfo.getMedOrderNumForCoupon() == null ){
				orderViewInfo.setMedOrderNumForCoupon(refillScheduleManager.retrieveManualMedOrderNumForCoupon(pharmOrder));
			}
			
			Map<Integer, Integer> numOfRefillMap = new HashMap<Integer, Integer>();
			Map<Integer, Integer> refillIntervalMap = new HashMap<Integer, Integer>();
			Map<Integer, Integer> refillIntervalLastMap = new HashMap<Integer, Integer>();
			
			for ( RefillSchedule refillSchedule : refillScheduleListIn ){
				if( numOfRefillMap.get(refillSchedule.getGroupNum()) == null ){
					numOfRefillMap.put(refillSchedule.getGroupNum(), 1);
				}else{
					Integer numOfRefill = numOfRefillMap.get(refillSchedule.getGroupNum()) + 1;
					numOfRefillMap.put(refillSchedule.getGroupNum(), numOfRefill);
				}
				
				if( !refillIntervalMap.containsKey(refillSchedule.getGroupNum()) ){
					refillIntervalMap.put(refillSchedule.getGroupNum(), refillSchedule.getRefillInterval());
				}
				
				if( !refillIntervalLastMap.containsKey(refillSchedule.getGroupNum()) ){
					refillIntervalLastMap.put(refillSchedule.getGroupNum(), refillSchedule.getRefillIntervalLast());
				}
				
				for(RefillScheduleItem refillScheduleItem : refillSchedule.getRefillScheduleItemList()){
					refillScheduleItem.setStatus(RefillScheduleItemStatus.Normal);
				}
			}
			
			for( RefillSchedule updateRs: refillScheduleListIn ){
				updateRs.setRefillStatus(refillScheduleManager.retrieveInitRefillStatusByRefillNum(updateRs.getRefillNum()));
				updateRs.setWorkstore(workstore);
				updateRs.setNumOfRefill(numOfRefillMap.get(updateRs.getGroupNum()));
				updateRs.setRefillInterval(refillIntervalMap.get(updateRs.getGroupNum()));
				updateRs.setRefillIntervalLast(refillIntervalLastMap.get(updateRs.getGroupNum()));
				
				String refillCouponNum = generateRefillCouponNum(workstore.getHospCode(), orderViewInfo.getMedOrderNumForCoupon(),updateRs.getRefillNum(), updateRs.getGroupNum());
				updateRs.setRefillCouponNum(refillCouponNum);
				if( orderViewInfo.getMedOrderNumForCoupon() != null ){
					updateRs.setTrimRefillCouponNum(refillCouponNum.substring(0,14));
				}else{
					updateRs.setTrimRefillCouponNum(refillCouponNum);
				}
				
				if( updateRs.getRefillNum() == 1 ){					
					updateRs.setPrintReminderFlag(orderViewInfo.getRefillPrintReminderFlag());
				}
				
				for( RefillScheduleItem refillScheduleItem : updateRs.getRefillScheduleItemList() ){
					refillScheduleItem.setWorkstore(workstore);
				}
				updateRs.connect(pharmOrder);
			}
			
			for( PharmOrderItem pharmOrderItem : pharmOrder.getPharmOrderItemList() ){
				pharmOrderItem.setStdRefillCheckFlag(true);
			}
			
			List<Integer> printFlagList = updateRefillSchedulePrintFlagByRsiDirtyFlag(refillScheduleListIn);
			
			printFlagList = updateRefillSchedulePrintFlagByPharmOrderItem(pharmOrder.getId(), printFlagList, refillScheduleListIn);
			
			for( RefillSchedule updatePrintRs: refillScheduleListIn ){
				if( RefillScheduleRefillStatus.Next == updatePrintRs.getRefillStatus() && printFlagList.contains(updatePrintRs.getGroupNum()) ){
					logger.debug("updateStdRefillScheduleList==================================GroupNum PrintFlag:#0", updatePrintRs.getGroupNum());
					updatePrintRs.setPrintFlag(false);
				}
			}
			pharmOrder.setNumOfGroup(refillIntervalMap.size());
		}else{
			
			List<RefillSchedule> savedRefillScheduleList = (List<RefillSchedule>) Contexts.getSessionContext().get("refillScheduleList");
			for ( RefillSchedule refillSchedule : savedRefillScheduleList ){
				refillSchedule.connect(pharmOrder);
			}
		}
		
		logger.debug("updateStdRefillScheduleList After pharmOrder.getRefillScheduleList(DocType.Standard) 2 size: #0", pharmOrder.getRefillScheduleList(DocType.Standard).size());		
		pharmOrder.disconnectRefillScheduleList();
		
		//Clear context list		
		refillScheduleList = new ArrayList<RefillSchedule>();
		refillPoiItemNumList = new ArrayList<Integer>();
		refillPoiItemNumInitList = new ArrayList<Integer>();
	}
		
	public static String generateRefillCouponNum(String dispHospCode, String medOrderNum, Integer refillNum, Integer groupNum ){
		StringBuilder refillCouponNum = new StringBuilder();
		
		refillCouponNum.append(dispHospCode);
		if ( dispHospCode.length() == 2 ){
			refillCouponNum.append(" ");
		}
		
		if( medOrderNum != null && medOrderNum.length() > 3 ){
			refillCouponNum.append(medOrderNum.substring(3));
		}
		
		if( refillNum < 10 ){
			refillCouponNum.append("0");
		}
		refillCouponNum.append(refillNum);
		
		if( groupNum < 10 ){
			refillCouponNum.append("0");
		}
		refillCouponNum.append(groupNum);
		
		return refillCouponNum.toString();
	}
	
	private RefillPrescInfo retrieveRefillPrescInfo(Long refillScheduleId){
		RefillPrescInfo newRefillPrescInfo = new RefillPrescInfo();
		
		if( vettingDrsConfig != null ){
			newRefillPrescInfo.setDrsThreshold(vettingDrsConfig.getThreshold());
			newRefillPrescInfo.setDrsRefillInterval(vettingDrsConfig.getInterval());
			newRefillPrescInfo.setDrsLastIntervalMaxDay(vettingDrsConfig.getLastIntervalMaxDay());
		}
		
		newRefillPrescInfo.setRefillScheduleId(refillScheduleId);
		newRefillPrescInfo.setThreshold(vettingRefillConfig.getThreshold());
		newRefillPrescInfo.setRefillInterval(vettingRefillConfig.getInterval());
		newRefillPrescInfo.setLastIntervalMaxDay(vettingRefillConfig.getLastIntervalMaxDay());
		newRefillPrescInfo.setOrgHospCode(workstore.getHospCode());
		newRefillPrescInfo.setOrgWorkstoreCode(workstore.getWorkstoreCode());
		
		if( refillScheduleId != null ){
			RefillSchedule refillSchedule = em.find(RefillSchedule.class, refillScheduleId);
			if( refillSchedule != null ){
				newRefillPrescInfo.setCurrentGroupNum(refillSchedule.getGroupNum());
				newRefillPrescInfo.setOrgHospCode(refillSchedule.getPharmOrder().getWorkstore().getHospCode());
				newRefillPrescInfo.setOrgWorkstoreCode(refillSchedule.getPharmOrder().getWorkstore().getWorkstoreCode());
				newRefillPrescInfo.setMedOrderNum(refillSchedule.getRefillCouponNum().substring(0,12));
				if( refillSchedule.getRefillInterval() != null ){
					newRefillPrescInfo.setThreshold(refillSchedule.getRefillInterval());
					newRefillPrescInfo.setRefillInterval(refillSchedule.getRefillInterval());
					newRefillPrescInfo.setLastIntervalMaxDay(refillSchedule.getRefillIntervalLast());
					newRefillPrescInfo.setDrsThreshold(refillSchedule.getRefillInterval());
					newRefillPrescInfo.setDrsRefillInterval(refillSchedule.getRefillInterval());
					newRefillPrescInfo.setDrsLastIntervalMaxDay(refillSchedule.getRefillIntervalLast());
				}
			}
		}
		return newRefillPrescInfo;
	}
	
	public void retrieveRefillScheduleFromVetting(boolean manualRefillSave){
		PharmOrder pharmOrder = (PharmOrder) Contexts.getSessionContext().get("pharmOrder");					
		Long rsId = null;
		
		pharmOrder.reconnectRefillScheduleList();
		
		boolean showRefill = false;
		if( !pharmOrder.getChestFlag() ){
			for( PharmOrderItem pharmOrderItem : pharmOrder.getPharmOrderItemList() ){			
				if( pharmOrderItem.isDirtyFlag() && !pharmOrderItem.isStdRefillCheckFlag() ){
					refillScheduleManager.removeRefillScheduleListByPharmOrderItemNum(pharmOrder, pharmOrderItem.getOrgItemNum());
					showRefill = true;
				}
			}
		}
		
		if( manualRefillSave && !showRefill ){
			refillScheduleList = new ArrayList<RefillSchedule>();
			refillPoiItemNumList = new ArrayList<Integer>();
			refillPoiItemNumInitList = new ArrayList<Integer>();
			pharmOrder.disconnectRefillScheduleList();
			return;
		}
		
		pharmOrder.reconnectRefillScheduleList();
		
		List<Integer> existRefillSchedulePoiList = new ArrayList<Integer>();
		
		List<RefillSchedule> existRefillScheduleList = pharmOrder.getRefillScheduleList(DocType.Standard);
		for( RefillSchedule existRs : existRefillScheduleList ){
			for( RefillScheduleItem existRsi : existRs.getRefillScheduleItemList() ){
				if( existRsi.getPharmOrderItem() != null && !existRefillSchedulePoiList.contains(existRsi.getPharmOrderItem().getOrgItemNum()) ){
					existRefillSchedulePoiList.add(existRsi.getPharmOrderItem().getOrgItemNum());
					logger.debug("existRefillSchedulePoiList============================Add:#0", existRsi.getPharmOrderItem().getOrgItemNum());
				}
			}
		}
		refillScheduleList = new ArrayList<RefillSchedule>();
		refillPoiItemNumList = new ArrayList<Integer>();
		refillPoiItemNumInitList = new ArrayList<Integer>();		
		
		refillPoiItemNumList = this.retrieveNoRefillPharmOrderItemList(pharmOrder, existRefillSchedulePoiList, true, orderViewInfo);
		existRefillSchedulePoiList.addAll(refillPoiItemNumList);
		logger.debug("retrieveRefillScheduleFromVetting refillPoiItemNumList: #0", refillPoiItemNumList.size());
		
		if( manualRefillSave && refillPoiItemNumList.size() == 0 ){
			refillScheduleList = new ArrayList<RefillSchedule>();
			refillPoiItemNumList = new ArrayList<Integer>();
			refillPoiItemNumInitList = new ArrayList<Integer>();
			pharmOrder.disconnectRefillScheduleList();
			return;
		}
		
		refillPoiItemNumInitList = retrieveNoRefillPharmOrderItemList(pharmOrder, existRefillSchedulePoiList, false, orderViewInfo);	
		logger.debug("retrieveRefillScheduleFromVetting refillPoiItemNumInitList: #0", refillPoiItemNumInitList.size());		
		
		refillScheduleList = pharmOrder.getRefillScheduleList(DocType.Standard);		
		for (RefillSchedule rs : refillScheduleList) {
			rs.setWorkstore(workstore);
			rs.disconnect();
		}
		
		if( pharmOrder.getId() != null ){
			for( RefillSchedule rs : refillScheduleList ){
				if( rs.getGroupNum().equals(1)){
					rsId = rs.getId();	
					break;
				}
			}
		}
		
		refillPrescInfo = retrieveRefillPrescInfo(rsId);
		refillPrescInfo.setSourceView("vetting");
		refillPrescInfo.setDrsFlag(orderViewInfo.isDrsFlag());
		refillPrescInfo.setUrgentFlag(orderViewInfo.getUrgentFlag());

		refillHolidayListForRefillEdit = refillHolidayManager.retrieveRefillHolidayList();
	}
	
	private List<Integer> retrieveNoRefillPharmOrderItemList(PharmOrder pharmOrder, List<Integer> existRsPoiItemList, boolean autoCalculate, OrderViewInfo orderViewInfoIn){
		List<Integer> itemNumList = new ArrayList<Integer>();
		if( autoCalculate ){
			boolean allowStdRefill = refillScheduleManager.isAllowStdRefill(pharmOrder,vettingRefillConfig, true, orderViewInfoIn.isDrsFlag());
			
			if( !allowStdRefill ){
				return itemNumList;
			}
		}
		
		List<PharmOrderItem> poiRefillList = this.retrieveNoRefillPharmOrderItemList(pharmOrder, autoCalculate, orderViewInfoIn);
		
		for( PharmOrderItem pharmOrderItem : poiRefillList ){
			if( !existRsPoiItemList.contains(pharmOrderItem.getOrgItemNum()) ){
				itemNumList.add(pharmOrderItem.getOrgItemNum());
				logger.debug("retrieveNoRefillPharmOrderItemList itemNumList, autoCalculate: #0, #1", pharmOrderItem.getOrgItemNum(), autoCalculate);
			}
		}
		
		return itemNumList;
	}
	
	private List<PharmOrderItem> retrieveNoRefillPharmOrderItemList(PharmOrder pharmOrder, boolean isAutoGen, OrderViewInfo orderViewInfoIn){
		List<PharmOrderItem> poiRefillList = new ArrayList<PharmOrderItem>();

		for( PharmOrderItem pharmOrderItem : pharmOrder.getPharmOrderItemList() ){
			
			if( isAutoGen && MedOrderDocType.Normal == pharmOrder.getMedOrder().getDocType() && pharmOrder.getId() != null && !pharmOrderItem.isDirtyFlag() ){
				continue;
			}
			
			if( pharmOrderItem.getId() != null && !pharmOrderItem.getRefillScheduleItemList().isEmpty() ){
				continue;
			}
			
			if( isAutoGen && pharmOrderItem.getId() != null && !pharmOrderItem.isDirtyFlag() ){
				continue;
			}
			
			if( pharmOrderItem.getIssueQty().compareTo(BigDecimal.ZERO) == 0 ){
				continue;
			}
			
			if( isAutoGen && pharmOrderItem.isStdRefillCheckFlag() ){
				continue;
			}
			
			Integer threshold = vettingRefillConfig.getThreshold();
			Integer lastIntervalMaxDay = vettingRefillConfig.getLastIntervalMaxDay();			
			if( orderViewInfoIn.isDrsFlag() && vettingDrsConfig != null ){
				threshold = vettingDrsConfig.getThreshold();
				lastIntervalMaxDay = vettingDrsConfig.getLastIntervalMaxDay();
			}			
			if( refillScheduleManager.isValidPharmOrderItemForStandardRefill(pharmOrderItem, isAutoGen, threshold, lastIntervalMaxDay)){
				poiRefillList.add(pharmOrderItem);
			}
		}
			
		if( orderViewInfoIn.isDrsFlag() ){ 
			//do nothing
		}else{
			if( isAutoGen && MedOrderDocType.Normal == pharmOrder.getMedOrder().getDocType() && !vettingRefillConfig.getEnableSelectionFlag() ){		
				poiRefillList = refillScheduleManager.filterPharmOrderItemListByRefillSelection(poiRefillList, vettingRefillConfig);
			}
		}
		return poiRefillList;
	}
	
	public List<RefillSchedule> retrieveRefillScheduleByPharmOrderId(Long pharmOrderId){
		
		List<RefillSchedule> rsList = (List<RefillSchedule>) em.createQuery(
				"select o from RefillSchedule o" + // 20120303 index check : RefillSchedule.pharmOrder : FK_REFILL_SCHEDULE_01
				" where o.pharmOrder.id = :pharmOrderId" +
				" and o.docType = :docType" +
				" order by o.groupNum, o.refillNum ")
				.setParameter("pharmOrderId", pharmOrderId)
				.setParameter("docType", DocType.Standard)
				.getResultList();
		
		for( RefillSchedule refillSchedule : rsList ){
			refillSchedule.loadChild();
		}
		
		return rsList;
	}

	public void retrieveRefillScheduleFromCheckIssue(Long refillScheduleId){
		PharmOrder pharmOrder = (PharmOrder) Contexts.getSessionContext().get("pharmOrder");
		
		refillScheduleList = retrieveRefillScheduleByPharmOrderId(pharmOrder.getId());
		em.clear();
		
		List<MedOrderItem> refillMoiList = new ArrayList<MedOrderItem>();
		List<Integer> moiItemNumList = new ArrayList<Integer>();
		for( RefillSchedule disConRefillSchedule : refillScheduleList ){
			if( disConRefillSchedule.getRefillNum().equals(Integer.valueOf(1)) ){
				for( RefillScheduleItem refillScheduleItem : disConRefillSchedule.getRefillScheduleItemList() ){
					MedOrderItem disconMoi = refillScheduleItem.getPharmOrderItem().getMedOrderItem();
					if( disconMoi.getDiscontinueStatus() != null && disconMoi.getDiscontinueStatus() != DiscontinueStatus.None && !moiItemNumList.contains(disconMoi.getItemNum()) ){
						refillMoiList.add(disconMoi);
					}
				}
			}
		}
		
		for( RefillSchedule refillSchedule : refillScheduleList ){
			refillSchedule.disconnect();
		}

		refillPrescInfo = retrieveRefillPrescInfo(refillScheduleId);
		refillPrescInfo.setRefillActionType(RefillScheduleActionType.ReadOnlyRefill);
		refillPrescInfo.setSourceView("checkIssue");
		if ( pharmOrder.getDrsFlag() != null ){
			refillPrescInfo.setDrsFlag(pharmOrder.getDrsFlag());
		}else{
			refillPrescInfo.setDrsFlag(Boolean.FALSE);
		}
		this.retrieveRefillDiscontinueMessageMap(refillMoiList);
	}
	
	public void retrieveRefillScheduleFromOneStop(Long refillScheduleId){

		RefillSchedule refillScheduleIn = em.find(RefillSchedule.class, refillScheduleId);
		
		List<RefillSchedule> rsList = retrieveRefillScheduleByPharmOrderId(refillScheduleIn.getPharmOrder().getId());
		em.clear();
		
		List<MedOrderItem> refillMoiList = new ArrayList<MedOrderItem>();
		List<Integer> moiItemNumList = new ArrayList<Integer>();
		Boolean isAllDiscontinueItem = true;		
		
		for( RefillSchedule disConRefillSchedule : rsList ){
			if( disConRefillSchedule.getRefillNum().equals(Integer.valueOf(1)) ){
				for( RefillScheduleItem refillScheduleItem : disConRefillSchedule.getRefillScheduleItemList() ){
					MedOrderItem disconMoi = refillScheduleItem.getPharmOrderItem().getMedOrderItem();
					if( disconMoi.getDiscontinueStatus() != null && disconMoi.getDiscontinueStatus() != DiscontinueStatus.None && !moiItemNumList.contains(disconMoi.getItemNum()) ){
						refillMoiList.add(disconMoi);
					}
				}
			}
			
			if( disConRefillSchedule.getStatus() == RefillScheduleStatus.NotYetDispensed ){
				for( RefillScheduleItem refillScheduleItem : disConRefillSchedule.getRefillScheduleItemList() ){
					MedOrderItem refillMoi = refillScheduleItem.getPharmOrderItem().getMedOrderItem();
					
					if( refillMoi.getDiscontinueStatus() != null && refillMoi.getDiscontinueStatus() != DiscontinueStatus.None ){
						refillScheduleItem.setStatus(RefillScheduleItemStatus.Discontinue);
					}else if( refillScheduleIn.getGroupNum().equals(disConRefillSchedule.getGroupNum()) && disConRefillSchedule.getRefillStatus() == RefillScheduleRefillStatus.Next ){
						isAllDiscontinueItem = false;
					}
				}
			}else if( refillScheduleIn.getGroupNum().equals(disConRefillSchedule.getGroupNum()) && disConRefillSchedule.getRefillStatus() == RefillScheduleRefillStatus.Current ){
				for( RefillScheduleItem currRefillScheduleItem : disConRefillSchedule.getRefillScheduleItemList() ){
					MedOrderItem currRefillMoi = currRefillScheduleItem.getPharmOrderItem().getMedOrderItem();
					
					if( currRefillMoi.getDiscontinueStatus() == null || currRefillMoi.getDiscontinueStatus() == DiscontinueStatus.None ){
						isAllDiscontinueItem = false;
						break;
					}
				}
			}
		}
		
		refillPrescInfo = retrieveRefillPrescInfo(refillScheduleId);		
		refillPrescInfo.setRefillActionType(retrieveRefillActionFromOnestop(rsList,refillScheduleIn));
		refillPrescInfo.setSourceView("onestop");
		PharmOrder pharmOrder = refillScheduleIn.getPharmOrder();
		if ( pharmOrder.getDrsFlag() != null ){
			refillPrescInfo.setDrsFlag(pharmOrder.getDrsFlag());
		}else{
			refillPrescInfo.setDrsFlag(Boolean.FALSE);
		}
		this.retrieveRefillDiscontinueMessageMap(refillMoiList);

		if( isAllDiscontinueItem ){
			if( RefillScheduleActionType.NormalRefill == refillPrescInfo.getRefillActionType() || RefillScheduleActionType.LateRefill == refillPrescInfo.getRefillActionType() ){
				refillPrescInfo.setRefillActionType(RefillScheduleActionType.ReadOnlyRefill);
			}
		}
		
		//Check if suspendOrder require ReadOnly
		if( isAllDiscontinueItem ){
			for( RefillSchedule disConRefillSchedule : rsList ){
				if( refillScheduleIn.getGroupNum().equals(disConRefillSchedule.getGroupNum()) && 
					disConRefillSchedule.getRefillStatus() == RefillScheduleRefillStatus.Current){
					DispOrder dispOrder = disConRefillSchedule.getDispOrder();
					
					if(  dispOrder.getAdminStatus() == DispOrderAdminStatus.Normal){
						break;
					}
					
					for( RefillScheduleItem refillScheduleItem : disConRefillSchedule.getRefillScheduleItemList() ){
						DispOrderItem dispOrderItem = dispOrder.getDispOrderItemByPoOrgItemNum(refillScheduleItem.getItemNum());
						if( dispOrderItem != null && 
								(dispOrderItem.getDispQty().compareTo(BigDecimal.valueOf(refillScheduleItem.getRefillQty())) != 0 || 
										dispOrderItem.getDurationInDay().compareTo(refillScheduleItem.getRefillDuration()) != 0 )){
							refillPrescInfo.setRefillActionType(RefillScheduleActionType.ReadOnlyRefill);
							break;
						}
					}
					break;
				}
			}
		}
		
		// set Allow Qty Adjust
		if( RefillScheduleActionType.LateRefill == refillPrescInfo.getRefillActionType() ){
			for( RefillSchedule refillSchedule : rsList ){
				if( refillSchedule.getGroupNum().equals(refillScheduleIn.getGroupNum()) && RefillScheduleRefillStatus.Next == refillSchedule.getRefillStatus() ){
					for( RefillScheduleItem refillScheduleItem : refillSchedule.getRefillScheduleItemList() ){
						refillScheduleItem.setAllowQtyAdjust(isAllowQtyAdjust(refillScheduleItem.getPharmOrderItem()));						
						DateMidnight today = new DateMidnight();						
						int days = Days.daysBetween(new DateMidnight(refillSchedule.getBeforeAdjRefillDate()), today).getDays(); 
						refillPrescInfo.setLateRefillDays(days);
						
						logger.debug("retrieveRefillScheduleFromOneStop LateRefillDays #0", days);
						
						if( refillScheduleItem.isAllowQtyAdjust() ){
							calculateLateRefillAdjustQty(refillScheduleItem);
						}
					}
					break;
				}
			}
		}
		
		for( RefillSchedule refillSchedule :rsList ){
			if( refillSchedule.getGroupNum().equals(refillScheduleIn.getGroupNum()) && refillSchedule.getRefillNum() > 1 ){
				if( RefillScheduleStatus.BeingProcessed == refillSchedule.getStatus() || 
					( refillSchedule.getDispOrder() != null && DispOrderAdminStatus.Suspended == refillSchedule.getDispOrder().getAdminStatus()) ){
					refillPrescInfo.setUrgentFlag(true);
				}
			}
			refillSchedule.disconnect();
		}
		refillScheduleList = rsList;
		refillPoiItemNumList = new ArrayList<Integer>();
		refillPoiItemNumInitList = new ArrayList<Integer>();
		refillHolidayListForRefillEdit = refillHolidayManager.retrieveRefillHolidayList();
	}
	
	private void retrieveRefillDiscontinueMessageMap(List<MedOrderItem> moiList){
		List<String> messageTlfList = itemConvertManager.formatDiscontinueMessage(moiList);
		Map<Integer,String> discontinueMessageMap = new HashMap<Integer,String>();
		for (String messageTlf : messageTlfList) {
			discontinueMessageMap.put(moiList.get(messageTlfList.indexOf(messageTlf)).getItemNum(), messageTlf);
		}
		refillPrescInfo.setRefillDiscontinueMessageTlf(discontinueMessageMap);
	}
	
	private void calculateLateRefillAdjustQty(RefillScheduleItem refillScheduleItemIn){
		DateMidnight today = new DateMidnight();
		int refillDaysDiff = Days.daysBetween(today, new DateMidnight(refillScheduleItemIn.getRefillEndDate())).getDays();
		refillDaysDiff = refillDaysDiff + 1;
		logger.debug("calculateLateRefillAdjustQty refillDaysDiff #0", refillDaysDiff);
		
		BigDecimal remainRefillDuration = BigDecimal.valueOf(Math.max(0, refillDaysDiff));
		refillScheduleItemIn.setLateRefillRemainDur(remainRefillDuration.intValue());
		logger.debug("calculateLateRefillAdjustQty remainRefillDuration #0", remainRefillDuration);
		
		remainRefillDuration = remainRefillDuration.divide(BigDecimal.valueOf(7), RoundingMode.CEILING);
		remainRefillDuration = remainRefillDuration.multiply(BigDecimal.valueOf(7));
		
		logger.debug("calculateLateRefillAdjustQty remainRefillDuration #0", remainRefillDuration);
		//PMS-914 Rounding Problem
		BigDecimal lateAdjFactor = remainRefillDuration.divide(BigDecimal.valueOf(refillScheduleItemIn.getRefillDuration()), 4, RoundingMode.HALF_UP);
		
		if( lateAdjFactor.compareTo(BigDecimal.ONE) > 0 ){
			lateAdjFactor = BigDecimal.ONE;
		}
		logger.debug("calculateLateRefillAdjustQty lateAdjFactor #0", lateAdjFactor);
		
		BigDecimal lateRefillAdjQty = lateAdjFactor.subtract(BigDecimal.ONE);
		logger.debug("calculateLateRefillAdjustQty lateRefillAdjQty  after subtract one #0", lateRefillAdjQty);
		lateRefillAdjQty = lateRefillAdjQty.multiply( BigDecimal.valueOf(refillScheduleItemIn.getRefillQty() ));
		lateRefillAdjQty = lateRefillAdjQty.setScale(0, RoundingMode.CEILING);
		logger.debug("calculateLateRefillAdjustQty lateRefillAdjQty #0", lateRefillAdjQty);
		
		refillScheduleItemIn.setLateRefillAdjQty(lateRefillAdjQty.intValue());
	}
	
	private RefillScheduleActionType retrieveRefillActionFromOnestop(List<RefillSchedule> refillScheduleListIn, RefillSchedule refillScheduleIn){
		
		if (refillScheduleIn.getDispOrder() != null && !isWithinBatchDuration(refillScheduleIn.getDispOrder().getTicketDate())) {
			orderViewInfo.addInfoMsg("0260", new String[]{String.valueOf(ORDER_ALLOWUPDATE_DURATION.get())});
			return RefillScheduleActionType.ReadOnlyRefill;
		}
		
		RefillSchedule initRs = new RefillSchedule();
		RefillSchedule currRs = new RefillSchedule();
		RefillSchedule nextRs = new RefillSchedule();
		RefillSchedule lastRs = new RefillSchedule();
		
		for( RefillSchedule refillSchedule : refillScheduleListIn ) {
			
			if( refillSchedule.getGroupNum().equals(refillScheduleIn.getGroupNum()) ){
						
				if( refillSchedule.getRefillNum().equals(1) ){
					initRs = refillSchedule;				
				}
				
				if( RefillScheduleRefillStatus.Current == refillSchedule.getRefillStatus() ){
					currRs = refillSchedule;
				}
				
				if( RefillScheduleRefillStatus.Next == refillSchedule.getRefillStatus() ){					
					nextRs = refillSchedule;					
				}
				
				if( refillSchedule.getRefillNum().equals(refillSchedule.getNumOfRefill()) ){
					lastRs = refillSchedule;					
				}		
			}			
		}
		
		List<MedProfileAlertMsg> alertMsgList = (List<MedProfileAlertMsg>) Contexts.getSessionContext().get("prescAlertMsgList");
		if ( alertMsgList != null && !alertMsgList.isEmpty() ) {
			return RefillScheduleActionType.TerminateRefill;
		}
		
		if( RefillScheduleRefillStatus.Previous == refillScheduleIn.getRefillStatus() ){
			return RefillScheduleActionType.ReadOnlyRefill;
		}else if( RefillScheduleStatus.Abort == currRs.getStatus() ){
			return RefillScheduleActionType.ReadOnlyRefill;
		}else if( RefillScheduleRefillStatus.Next == refillScheduleIn.getRefillStatus() ){
			if( isAbortRefill(lastRs, initRs) ){
				return RefillScheduleActionType.AbortRefill;
			}else if( RefillScheduleStatus.BeingProcessed == currRs.getStatus() ){
				if( Integer.valueOf(1).equals(currRs.getRefillNum()) ){
					return RefillScheduleActionType.ReadOnlyRefill;
				}else{
					return RefillScheduleActionType.NoRefillAction;
				}
			}else if( isLateRefill(nextRs) ){
				return RefillScheduleActionType.LateRefill;				
			}else if( RefillScheduleStatus.Dispensed == currRs.getStatus() && DispOrderAdminStatus.Normal == currRs.getDispOrder().getAdminStatus() ) {
				return RefillScheduleActionType.NormalRefill;
			}else if( RefillScheduleStatus.ForceComplete == currRs.getStatus() ){
				return RefillScheduleActionType.NormalRefill;
			}else{
				return RefillScheduleActionType.NoRefillAction;
			}
		}else{
			if( refillScheduleIn.getRefillNum().equals(refillScheduleIn.getNumOfRefill()) &&
				RefillScheduleStatus.Dispensed == refillScheduleIn.getStatus() &&
				DispOrderAdminStatus.Normal == refillScheduleIn.getDispOrder().getAdminStatus() && 
				(DispOrderDayEndStatus.None != refillScheduleIn.getDispOrder().getDayEndStatus() || refillScheduleIn.getDispOrder().getBatchProcessingFlag() ) ){
				return RefillScheduleActionType.ReadOnlyRefill;
			}else{
				return RefillScheduleActionType.NoRefillAction;
			}
		}
	}
	
	public void refreshRefillScheduleList(Long refillScheduleId, String errorMsgCode){
		retrieveRefillScheduleFromOneStop(refillScheduleId);
		
		if( StringUtils.isNotBlank(errorMsgCode) ){
			refillPrescInfo.addInfoMsg(errorMsgCode);
		}
	}
	
	public void refreshOrderAndRefillScheduleList(Long refillScheduleId, String refillActionIn){
		orderViewManager.retrieveOrderByRefillScheduleId(refillScheduleId);
		this.refreshRefillScheduleList(refillScheduleId, refillActionIn);
	}
	
	private boolean isWithinBatchDuration(Date ticketDate){
		return Days.daysBetween(new DateTime(ticketDate), new DateTime()).getDays() <= ORDER_ALLOWUPDATE_DURATION.get();
	}
}

