package hk.org.ha.model.pms.persistence;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.util.StringCollectionConverter;
import hk.org.ha.model.pms.persistence.disp.MedItemInf;
import hk.org.ha.model.pms.udt.alert.mds.AlertType;
import hk.org.ha.model.pms.vo.alert.mds.Alert;
import hk.org.ha.model.pms.vo.alert.mds.AlertAdr;
import hk.org.ha.model.pms.vo.alert.mds.AlertAllergen;
import hk.org.ha.model.pms.vo.alert.mds.AlertDdcm;
import hk.org.ha.model.pms.vo.alert.mds.AlertDdim;
import hk.org.ha.model.pms.vo.alert.mds.AlertHlaTest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedProperty;

@MappedSuperclass
@ExternalizedBean(type=DefaultExternalizer.class)
public class AlertEntity extends VersionEntity {

	private static final long serialVersionUID = 1L;

	private static final String JAXB_CONTEXT_MDS_ALERT = "hk.org.ha.model.pms.vo.alert.mds";
	
	private static final char NEWLINE = '\n';
	
	private static final char ASTERISK = '*';
	
	@Converter(name = "MedOrderItemAlert.alertType", converterClass = AlertType.Converter.class)
	@Convert("MedOrderItemAlert.alertType")
	@Column(name = "ALERT_TYPE", nullable = false, length = 8)
	private AlertType alertType;
	
	@Lob
	@Column(name = "ALERT_XML")
	private String alertXml;
		
	@Converter(name = "MedOrderItemAlert.overrideReason", converterClass = StringCollectionConverter.class)
	@Convert("MedOrderItemAlert.overrideReason")
	@Column(name = "OVERRIDE_REASON", length = 500)
	private List<String> overrideReason;
	
	@Column(name = "ACK_USER", length = 12)
	private String ackUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ACK_DATE")
	private Date ackDate;
	
	@Column(name = "SORT_SEQ", nullable = false)
	private Long sortSeq;
	
	@Column(name = "ADDITIVE_NUM")
	private Integer additiveNum;
	
	@Transient
	private Alert alert;
	
	public AlertEntity() {
	}
	
	public AlertEntity(Alert alert) {
		if (alert instanceof AlertAllergen) {
			alertType = AlertType.Allergen;
		}
		else if (alert instanceof AlertAdr) {
			alertType = AlertType.Adr;
		}
		else if (alert instanceof AlertDdcm) {
			alertType = AlertType.Ddcm;
		}
		else if (alert instanceof AlertDdim) {
			alertType = AlertType.Ddim;
		}
		else if (alert instanceof AlertHlaTest) {
			alertType = AlertType.IpHla;
		}
		this.alert = alert;
		preSave();
	}
	
	@PostLoad
    public void postLoad() {
    	if (alertXml != null && alert == null) {
    		JaxbWrapper<Alert> mdsAlertJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_MDS_ALERT);
    		alert = mdsAlertJaxbWrapper.unmarshall(alertXml);
    		alert.setSortSeq(sortSeq);
    	}
    }

    @PrePersist
    @PreUpdate
    public void preSave() {    	
    	 if (alert != null) {
    		 JaxbWrapper<Alert> mdsAlertJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_MDS_ALERT);
    		 alertXml = mdsAlertJaxbWrapper.marshall(alert);
    		 sortSeq = alert.getSortSeq();
    	 }
    }
    
	public AlertType getAlertType() {
		return alertType;
	}

	public void setAlertType(AlertType alertType) {
		this.alertType = alertType;
	}

	public String getAlertXml() {
		return alertXml;
	}

	public void setAlertXml(String alertXml) {
		this.alertXml = alertXml;
	}

	public List<String> getOverrideReason() {
		if (overrideReason == null) {
			return new ArrayList<String>();
		}
		else {
			return new ArrayList<String>(overrideReason);
		}
	}

	public void setOverrideReason(List<String> overrideReason) {
		if (overrideReason == null) {
			this.overrideReason = new ArrayList<String>();
		}
		else {
			this.overrideReason = new ArrayList<String>(overrideReason);
		}
	}

	public String getAckUser() {
		return ackUser;
	}

	public void setAckUser(String ackUser) {
		this.ackUser = ackUser;
	}

	public Date getAckDate() {
		if (ackDate == null) {
			return null;
		}
		else {
			return new Date(ackDate.getTime());
		}
	}

	public void setAckDate(Date ackDate) {
		if (ackDate == null) {
			this.ackDate = null;
		}
		else {
			this.ackDate = new Date(ackDate.getTime());
		}
	}

	public Long getSortSeq() {
		return sortSeq;
	}

	public void setSortSeq(Long sortSeq) {
		this.sortSeq = sortSeq;
	}
	
	public Integer getAdditiveNum() {
		return additiveNum;
	}

	public void setAdditiveNum(Integer additiveNum) {
		this.additiveNum = additiveNum;
	}

	public Alert getAlert() {
		return alert;
	}

	public void setAlert(Alert alert) {
		this.alert = alert;
	}
	
	@ExternalizedProperty
	public String getOverrideReasonDesc() {
		StringBuilder sb = new StringBuilder();
		
		for (String overrideReason : getOverrideReason()) {
			if (overrideReason.isEmpty()) {
				continue;
			}
			if (sb.length() > 0) {
				sb.append(NEWLINE);
			}
			if (overrideReason.charAt(overrideReason.length()-1) == ASTERISK) {
				sb.append(overrideReason.substring(0, overrideReason.length()-1));
			}
			else {
				sb.append(overrideReason);
			}
		}
		
		if (sb.length() == 0) {
			return null;
		}
		else {
			return sb.toString();
		}
	}
	
	public boolean isMdsAlert() {
		return isMdsAlert(Boolean.FALSE);
	}
	
	public boolean isMdsAlert(Boolean includeHlaFlag) {
		return isPatAlert(includeHlaFlag)
			|| isDdiAlert()
			|| isDrcAlert();
	}
	
	public boolean isPatAlert() {
		return isPatAlert(Boolean.FALSE);
	}
		
	public boolean isPatAlert(Boolean includeHlaFlag) {
		return (alertType == AlertType.Allergen)
			|| (alertType == AlertType.Adr)
			|| (alertType == AlertType.Ddcm)
			|| (alertType == AlertType.IpHla && includeHlaFlag);
	}
	
	public boolean isDdiAlert() {
		return (alertType == AlertType.Ddim);
	}
	
	public boolean isDrcAlert() {
		return (alertType == AlertType.Drcm);
	}
	
	public MedItemInf getMedItem() {
		return null;
	}

	public void restoreItemNum() {
		getAlert().restoreItemNum();
	}
}
