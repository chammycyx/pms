package hk.org.ha.model.pms.vo.reftable.mp;

import hk.org.ha.model.pms.dms.persistence.PmsCommonOrderStatus;
import hk.org.ha.model.pms.persistence.corp.HospitalMapping;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class CommonOrderStatusInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<PmsCommonOrderStatus> pmsCommonOrderStatusList;
	
	private List<WorkstoreDrug> workstoreDrugList;
	
	private List<HospitalMapping> hospitalMappingList;

	public List<PmsCommonOrderStatus> getPmsCommonOrderStatusList() {
		return pmsCommonOrderStatusList;
	}

	public void setPmsCommonOrderStatusList(
			List<PmsCommonOrderStatus> pmsCommonOrderStatusList) {
		this.pmsCommonOrderStatusList = pmsCommonOrderStatusList;
	}

	public List<WorkstoreDrug> getWorkstoreDrugList() {
		return workstoreDrugList;
	}

	public void setWorkstoreDrugList(List<WorkstoreDrug> workstoreDrugList) {
		this.workstoreDrugList = workstoreDrugList;
	}

	public List<HospitalMapping> getHospitalMappingList() {
		return hospitalMappingList;
	}

	public void setHospitalMappingList(List<HospitalMapping> hospitalMappingList) {
		this.hospitalMappingList = hospitalMappingList;
	}
	
}