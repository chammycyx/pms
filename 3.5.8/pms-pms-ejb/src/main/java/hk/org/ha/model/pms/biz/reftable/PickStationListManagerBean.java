package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.Machine;
import hk.org.ha.model.pms.persistence.reftable.PickStation;

import java.util.Collection;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("pickStationListManager")
@MeasureCalls
public class PickStationListManagerBean implements PickStationListManagerLocal {

	@PersistenceContext
	private EntityManager em;
	
	@In
	private Workstore workstore;
	
	public void updatePickStationList(Collection<PickStation> updatePickStationList)
	{	
		workstore = em.find(Workstore.class, workstore.getId());	
		for(PickStation pickStation:updatePickStationList) {
			if(pickStation.getId()!=null){
				em.merge(pickStation);
				for(Machine machine:pickStation.getMachineList()){
					machine.setPickStation(pickStation);
					if(machine.getId()==null){
						em.persist(machine);
					}else{
						em.merge(machine);
					}
				}
			}else{
				pickStation.setWorkstore(workstore);
				em.persist(pickStation);
				
				for(Machine machine:pickStation.getMachineList()){
					machine.setPickStation(pickStation);
					em.persist(machine);
				}
			}
		}
		
		em.flush();
	}
	
	@SuppressWarnings("unchecked")
	public List<PickStation> retrievePickStationList() 
	{	
		List<PickStation> pickStationList = em.createQuery(
												"select o from PickStation o" + // 20120303 index check : PickStation.workstore,pickStationNum : UI_PICK_STATION_01
												" where o.workstore = :workstore" +
												" order by o.pickStationNum")
												.setParameter("workstore", workstore)
												.getResultList();
		
		for (PickStation pickStation :pickStationList) {
			pickStation.getMachineList().size();
		}
		
		return pickStationList;
	}
}
