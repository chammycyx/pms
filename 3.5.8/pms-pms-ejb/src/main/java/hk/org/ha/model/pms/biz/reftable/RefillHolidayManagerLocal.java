package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.persistence.reftable.RefillHoliday;

import java.util.List;

import javax.ejb.Local;

@Local
public interface RefillHolidayManagerLocal {
	
	List<RefillHoliday> retrieveRefillHolidayList();
}
