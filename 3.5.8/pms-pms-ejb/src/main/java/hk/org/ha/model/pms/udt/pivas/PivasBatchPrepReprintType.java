package hk.org.ha.model.pms.udt.pivas;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PivasBatchPrepReprintType implements StringValuedEnum {

	Both("B", "Worksheet & Product Label"),
	Worksheet("W", "Worksheet"), 
	ProductLabel("P", "Product Label");
	
    private final String dataValue;
    private final String displayValue;

    PivasBatchPrepReprintType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    } 

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static PivasBatchPrepReprintType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PivasBatchPrepReprintType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<PivasBatchPrepReprintType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<PivasBatchPrepReprintType> getEnumClass() {
    		return PivasBatchPrepReprintType.class;
    	}
    }
}



