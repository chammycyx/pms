package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.FdnMapping;
import hk.org.ha.model.pms.udt.reftable.FdnType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.drug.DmDrugLite;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("fdnMappingListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class FdnMappingListServiceBean implements FdnMappingListServiceLocal {

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<FdnMapping> fdnMappingList;
	
	@Out(required = false)
	private DmDrugLite fdnMappingDmDrugLite;

	@In
	private Workstore workstore;
	
	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;
	
	@SuppressWarnings("unchecked")
	public void retrieveFdnMappingList(String itemCode) 
	{
		fdnMappingDmDrugLite = null;
		fdnMappingList = null;

		WorkstoreDrug workstoreDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, itemCode);

		if ( workstoreDrug !=null ){
			if (workstoreDrug.getMsWorkstoreDrug() != null && workstoreDrug.getDmDrug()!=null) {				
				fdnMappingDmDrugLite = workstoreDrug.getDmDrugLite();
				
				List<FdnMapping> tempFdnMappingList = em.createQuery(
						"select o from FdnMapping o" + // 20120303 index check : FdnMapping.workstore,orderType,itemCode : UI_FDN_MAPPING_01
						" where o.workstore.hospCode = :hospCode" + 
						" and o.orderType = :orderType" +
						" and o.itemCode = :itemCode")
						.setParameter("hospCode", workstore.getHospCode())
						.setParameter("orderType", OrderType.OutPatient)
						.setParameter("itemCode", itemCode)
						.getResultList();
				
				fdnMappingList = new ArrayList<FdnMapping>();
				
				for (Workstore wks : workstore.getHospital().getWorkstoreList())
				{
					boolean found = false;
					for (FdnMapping fdnMapping : tempFdnMappingList) 
					{
						if(fdnMapping.getWorkstore().getId().equals(wks.getId())) {
							fdnMappingList.add(fdnMapping);
							found = true;
							break;
						}
					}
					
					if (!found) 
					{
						FdnMapping fdnMapping = new FdnMapping();
						fdnMapping.setWorkstore(wks);
						fdnMapping.setItemCode(itemCode);
						fdnMapping.setType(FdnType.DefaultName);
						fdnMapping.setOrderType(OrderType.OutPatient);
						fdnMappingList.add(fdnMapping);
					}
				}
			}
		}
	}
	
	public void updateFdnMappingList(Collection<FdnMapping> fdnMappingList, String itemCode) 
	{
		for (FdnMapping fdnMapping : fdnMappingList) 
		{
			if (fdnMapping.isNew()) 
			{
				if (!FdnType.DefaultName.equals(fdnMapping.getType())) {
					fdnMapping.setWorkstore(em.find(Workstore.class, fdnMapping.getWorkstore().getId()));
					fdnMapping.setOrderType(OrderType.OutPatient);
					em.persist(fdnMapping);
					em.flush();		
				}				
			} 
			else 
			{
				if (FdnType.DefaultName.equals(fdnMapping.getType())) { 
					em.remove(em.merge(fdnMapping));
				} else {
					em.merge(fdnMapping);
				}
				em.flush();		
			}
		}		
		
		retrieveFdnMappingList(itemCode);
	}

	public DmDrugLite getFdnMappingDmDrugLite() {
		return fdnMappingDmDrugLite;
	}

	@Remove
	public void destroy() 
	{
		if (fdnMappingList != null) {
			fdnMappingList = null;
		}
	}
}
