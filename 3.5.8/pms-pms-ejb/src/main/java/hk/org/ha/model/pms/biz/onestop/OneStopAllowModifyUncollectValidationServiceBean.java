package hk.org.ha.model.pms.biz.onestop;

import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.sys.SysProfile;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.uam.udt.AuthorizeStatus;
import hk.org.ha.model.pms.uam.vo.AuthenticateCriteria;
import hk.org.ha.service.biz.pms.uam.interfaces.UamServiceJmsRemote;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("oneStopAllowModifyUncollectValidationService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class OneStopAllowModifyUncollectValidationServiceBean implements OneStopAllowModifyUncollectValidationServiceLocal {
	
	@Logger
	private Log logger;
	
	@In
	private UamInfo uamInfo;
	
	@In
	private SysProfile sysProfile;
	
	@In
	private UamServiceJmsRemote uamServiceProxy;
	
	@Out(required=false)
	private String reAuthUserId;
	
	private String allowModifyUncollectFailMsg;

	public void validateAllowModifyUncollectUser(String userId, String password, String target) {
		
		AuthenticateCriteria authenticateCriteria = new AuthenticateCriteria();
		authenticateCriteria.setProfileCode(uamInfo.getProfileCode());
		authenticateCriteria.setUserName(userId);
		authenticateCriteria.setPassword(password);
		authenticateCriteria.setTarget(target);
		authenticateCriteria.setAction("Y");
		AuthorizeStatus authStatus = uamServiceProxy.authenticate(authenticateCriteria);
		reAuthUserId = userId;
		if (sysProfile.isDebugEnabled() || AuthorizeStatus.Succeed.equals(authStatus) ){
			allowModifyUncollectFailMsg = null;
		} else {
			if ( AuthorizeStatus.Invalid.equals(authStatus) ){			
				allowModifyUncollectFailMsg = "0091";
			} else {
				allowModifyUncollectFailMsg = "0513";
			}
		}
		
		logger.debug("allowModifyUncollectFailMsg #0", allowModifyUncollectFailMsg);
	}
	
	public String getAllowModifyUncollectFailMsg()
	{
		return allowModifyUncollectFailMsg;
	}
		
	@Remove
	public void destroy(){
		if (reAuthUserId != null)
		{
			reAuthUserId = null;
		}
		
	}
}
