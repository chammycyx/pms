package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.model.pms.vo.support.PropInfoItem;

import java.util.List;

import javax.ejb.Local;
 
@Local
public interface MpWorkingStoreListServiceLocal {
	
	void retrieveMpWorkStorePropList();
	
	void updateMpWorkingStorePropList(List<PropInfoItem> workingStorePropList);
	
	void destroy();
	
} 
 