package hk.org.ha.model.pms.biz.report;

import static hk.org.ha.model.pms.prop.Prop.REPORT_MDSEXCEPTION_REMARK;
import hk.org.ha.fmk.pms.remote.marshall.EclipseLinkXStreamMarshaller;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.persistence.AlertEntity;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.udt.alert.AlertProfileStatus;
import hk.org.ha.model.pms.udt.alert.ehr.EhrAlertStatus;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.alert.AlertProfile;
import hk.org.ha.model.pms.vo.alert.PatAlert;
import hk.org.ha.model.pms.vo.alert.mds.AlertDdim;
import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.report.MdsExceptionOverrideRpt;
import hk.org.ha.model.pms.vo.report.MdsExceptionOverrideRptDtl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("mdsExceptionOverrideRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MdsExceptionOverrideRptServiceBean implements MdsExceptionOverrideRptServiceLocal {

	private static final String OP_PAT_RPT_NAME = "MdsExceptionOpOverridePatRpt";
	
	private static final String OP_DDI_RPT_NAME = "MdsExceptionOpOverrideDdiRpt";
	
	private static final String MP_PAT_RPT_NAME = "MdsExceptionMpOverridePatRpt";
	
	private static final String MP_DDI_RPT_NAME = "MdsExceptionMpOverrideDdiRpt";
	
	@Logger
	private Log logger;
	
	@In
	private PrinterSelectorLocal printerSelector;

	@In
	private PrintAgentInf printAgent;
	
	@In(required = false)
	private PharmOrder pharmOrder;
	
	@In(required = false)
	private MedProfile medProfile;

	public void printMdsExceptionOpOverrideRpt(AlertMsg alertMsg, AlertProfile alertProfile) {

		List<RenderAndPrintJob> printJobList = new ArrayList<RenderAndPrintJob>();
		
		if (logger.isDebugEnabled()) {
			logger.debug("alertMsg:\n#0", (new EclipseLinkXStreamMarshaller()).toXML(alertMsg));
		}
		
		MdsExceptionOverrideRpt patRpt = constructMdsExceptionOverrideRptByPharmOrder(alertMsg, alertProfile);
		patRpt.setMdsExceptionOverrideRptDtlList(constructMdsExceptionOverrideRptDtlList(alertMsg.getPatAlertList()));
		
		if (!patRpt.getMdsExceptionOverrideRptDtlList().isEmpty()) {
			
			printJobList.add(new RenderAndPrintJob(
					OP_PAT_RPT_NAME,
					new PrintOption(),
					printerSelector.retrievePrinterSelect(PrintDocType.Report),
					Arrays.asList(patRpt)
			));
		}
		
		for (AlertEntity alertEntity : alertMsg.getDdiAlertList()) {

			MdsExceptionOverrideRpt ddiRpt = constructMdsExceptionOverrideRptByPharmOrder(alertMsg, alertProfile);
			ddiRpt.addMdsExceptionOverrideRptDtl(constructMdsExceptionOverrideRptDtl(alertEntity, true));
			
			printJobList.add(new RenderAndPrintJob(
					OP_DDI_RPT_NAME,
					new PrintOption(),
					printerSelector.retrievePrinterSelect(PrintDocType.Report),
					Arrays.asList(ddiRpt)
			));
		}
		
		if (!printJobList.isEmpty()) {
			printAgent.renderAndPrint(printJobList);
		}
	}

	private MdsExceptionOverrideRpt constructMdsExceptionOverrideRptByPharmOrder(AlertMsg alertMsg, AlertProfile alertProfile) {
		
		String caseNum;
		if (pharmOrder.getMedCase() == null) {
			caseNum = null;
		}
		else {
			caseNum = pharmOrder.getMedCase().getCaseNum();
		}
		
		MdsExceptionOverrideRpt mdsRpt = new MdsExceptionOverrideRpt();
		
		StringBuilder sb = new StringBuilder();
		if (alertProfile != null) {
			for (PatAlert patAlert : alertProfile.getPatAlertList()) {
				if (sb.length() > 0) {
					sb.append(", ");
				}
				sb.append(patAlert.getAlertDesc());
			}
		}
		
		mdsRpt.setSex(pharmOrder.getSex().getDataValue());
		mdsRpt.setPatName(pharmOrder.getName());
		mdsRpt.setPatNameChi(pharmOrder.getNameChi());
		mdsRpt.setHkid(pharmOrder.getHkid());
		mdsRpt.setPatCatCode(pharmOrder.getPatCatCode());
		mdsRpt.setPatAllergy(alertProfile == null || alertProfile.getLastVerifyDate() != null ? null : alertProfile.getAllergyDesc());
		mdsRpt.setPatAdr(alertProfile == null ? null : alertProfile.getAdrDesc());
		mdsRpt.setG6pdFlag(alertProfile != null && alertProfile.getG6pdAlert() != null);
		mdsRpt.setPregnancyCheckFlag(pharmOrder.getPregnancyCheckFlag());
		mdsRpt.setAllergyServerAvailableFlag(alertProfile != null && alertProfile.getStatus() != AlertProfileStatus.Error && !alertProfile.isEhrAllergyOnly());
		mdsRpt.setDrugDisplayName(alertMsg.getMdsOrderDesc());
		mdsRpt.setCaseNum(caseNum);
		mdsRpt.setMdsExceptionOverrideRptDtlList(new ArrayList<MdsExceptionOverrideRptDtl>());
		mdsRpt.setEhrAllergyServerAvailableFlag(alertProfile != null && alertProfile.getEhrAllergySummary() != null && alertProfile.getEhrAllergySummary().getStatus() != EhrAlertStatus.Error);
		mdsRpt.setEhrAllergy(alertProfile != null && alertProfile.getEhrAllergySummary() != null ? alertProfile.getEhrAllergySummary().getAllergy() : null);
		mdsRpt.setEhrAdr(alertProfile != null && alertProfile.getEhrAllergySummary() != null ? alertProfile.getEhrAllergySummary().getAdr() : null);
		
		return mdsRpt;
	}
	
	private MdsExceptionOverrideRptDtl constructMdsExceptionOverrideRptDtl(AlertEntity alertEntity, boolean showTitleFlag) {
		
		MdsExceptionOverrideRptDtl mdsRptDtl = new MdsExceptionOverrideRptDtl();
		
		if (showTitleFlag) {
			mdsRptDtl.setAlertTitle(alertEntity.getAlert().getAlertTitle());
		}
		mdsRptDtl.setAlertDesc(alertEntity.getAlert().getAlertDesc());
		
		return mdsRptDtl;
	}
	
	private List<MdsExceptionOverrideRptDtl> constructMdsExceptionOverrideRptDtlList(List<AlertEntity> alertEntityList) {
		
		String prevTitle = null;
		MdsExceptionOverrideRptDtl mdsRptDtl = null;
		StringBuilder alertDescString = new StringBuilder();
		
		List<MdsExceptionOverrideRptDtl> mdsRptDtlList = new ArrayList<MdsExceptionOverrideRptDtl>();
		
		for (AlertEntity alertEntity : alertEntityList) {
			boolean showTitleFlag = !alertEntity.getAlert().getAlertTitle().equals(prevTitle);

			if( showTitleFlag ){
				if( mdsRptDtl != null ){//Add Prev
					mdsRptDtl.setAlertDesc(alertDescString.toString());
					mdsRptDtlList.add(mdsRptDtl);
				}
				
				mdsRptDtl = new MdsExceptionOverrideRptDtl();
				mdsRptDtl.setAlertTitle(alertEntity.getAlert().getAlertTitle());
				
				alertDescString = new StringBuilder();
				alertDescString.append(alertEntity.getAlert().getAlertDesc());
			}else{
				alertDescString.append("\n");
				alertDescString.append("\n");
				alertDescString.append(alertEntity.getAlert().getAlertDesc());
			}
			prevTitle = alertEntity.getAlert().getAlertTitle();
		}
		//For Last One
		if( mdsRptDtl != null ){
			mdsRptDtl.setAlertDesc(alertDescString.toString());
			mdsRptDtlList.add(mdsRptDtl);
		}
		
		return mdsRptDtlList;
	}
	
	public void printMdsExceptionMpOverrideRpt(AlertMsg alertMsg, AlertProfile alertProfile) {

		List<RenderAndPrintJob> printJobList = new ArrayList<RenderAndPrintJob>();
		
		if (logger.isDebugEnabled()) {
			logger.debug("alertMsg:\n#0", (new EclipseLinkXStreamMarshaller()).toXML(alertMsg));
		}
		
		MdsExceptionOverrideRpt patRpt = constructMdsExceptionOverrideRptByMedProfile(alertMsg, alertProfile);
		patRpt.setMdsExceptionOverrideRptDtlList(constructMdsExceptionOverrideRptDtlList(alertMsg.getPatAlertList()));
		
		if (!patRpt.getMdsExceptionOverrideRptDtlList().isEmpty()) {
			
			printJobList.add(new RenderAndPrintJob(
					MP_PAT_RPT_NAME,
					new PrintOption(),
					printerSelector.retrievePrinterSelect(PrintDocType.Report),
					Arrays.asList(patRpt)
			));
		}

		Map<Long, List<AlertEntity>> otherItemDdiAlertMsgMap = new LinkedHashMap<Long, List<AlertEntity>>();
		
		for (AlertEntity alertEntity : alertMsg.getDdiAlertList()) {
			AlertDdim alertDdim = (AlertDdim) alertEntity.getAlert();
			if( alertDdim.isCrossDdi() ){
				Long mapItemNum = alertDdim.getItemNum1() > alertDdim.getItemNum2() ?  alertDdim.getItemNum2() : alertDdim.getItemNum1();
				List<AlertEntity> ddiAlertList = otherItemDdiAlertMsgMap.get(mapItemNum);
				if( ddiAlertList == null ){
					ddiAlertList = new ArrayList<AlertEntity>();
				}
				ddiAlertList.add(alertEntity);
				otherItemDdiAlertMsgMap.put(mapItemNum, ddiAlertList);
			}else{
				MdsExceptionOverrideRpt ddiRpt = constructMdsExceptionOverrideRptByMedProfile(alertMsg, alertProfile);
				if( alertDdim.getItemNum2() > alertDdim.getItemNum1() ){
					ddiRpt.setDrugDisplayName1(alertDdim.getDrugName1());
					ddiRpt.setDrugDisplayName2(alertDdim.getDrugName2());
				}else{
					ddiRpt.setDrugDisplayName1(alertDdim.getDrugName2());
					ddiRpt.setDrugDisplayName2(alertDdim.getDrugName1());
				}
				ddiRpt.addMdsExceptionOverrideRptDtl(constructMdsExceptionOverrideRptDtl(alertEntity, true));
				
				printJobList.add(new RenderAndPrintJob(
						MP_DDI_RPT_NAME,
						new PrintOption(),
						printerSelector.retrievePrinterSelect(PrintDocType.Report),
						Arrays.asList(ddiRpt)
				));
			}
		}

		if( !otherItemDdiAlertMsgMap.isEmpty() ){
			for( Long otherItemNum : otherItemDdiAlertMsgMap.keySet() ){
				MdsExceptionOverrideRpt ddiRpt = constructMdsExceptionOverrideRptByMedProfile(alertMsg, alertProfile);
				
				ddiRpt.setCrossDdiFlag(true);
				
				AlertDdim alertDdim = (AlertDdim) otherItemDdiAlertMsgMap.get(otherItemNum).get(0).getAlert();
				
				ddiRpt.setDrugDisplayName1(alertDdim.getCrossDdiIpmoeOrderDesc());
				ddiRpt.setDrugDisplayName2(alertDdim.getItemNum2() > alertDdim.getItemNum1() ? alertDdim.getDrugName2() : alertDdim.getDrugName1());

				for (AlertEntity crossAlertEntity : otherItemDdiAlertMsgMap.get(otherItemNum)) {
					
					boolean showTitleFlag = true;
					int itemIndex = otherItemDdiAlertMsgMap.get(otherItemNum).indexOf(crossAlertEntity);
					if (itemIndex > 0) {
						AlertEntity prevAlertEntity = otherItemDdiAlertMsgMap.get(otherItemNum).get(itemIndex - 1);
						showTitleFlag = !StringUtils.equals(crossAlertEntity.getAlert().getAlertTitle(), prevAlertEntity.getAlert().getAlertTitle());
					}

					ddiRpt.addMdsExceptionOverrideRptDtl(constructMdsExceptionOverrideRptDtl(crossAlertEntity, showTitleFlag));
				}
				
				printJobList.add(new RenderAndPrintJob(
						MP_DDI_RPT_NAME,
						new PrintOption(),
						printerSelector.retrievePrinterSelect(PrintDocType.Report),
						Arrays.asList(ddiRpt)
				));
			}
		}

			
		if (!printJobList.isEmpty()) {
			printAgent.renderAndPrint(printJobList);
		}
	}

	private MdsExceptionOverrideRpt constructMdsExceptionOverrideRptByMedProfile(AlertMsg alertMsg, AlertProfile alertProfile) {
		
		String caseNum;
		if (medProfile.getMedCase() == null) {
			caseNum = null;
		}
		else {
			caseNum = medProfile.getMedCase().getCaseNum();
		}
		
		MdsExceptionOverrideRpt mdsRpt = new MdsExceptionOverrideRpt();
		
		StringBuilder sb = new StringBuilder();
		if (alertProfile != null) {
			for (PatAlert patAlert : alertProfile.getPatAlertList()) {
				if (sb.length() > 0) {
					sb.append(", ");
				}
				sb.append(patAlert.getAlertDesc());
			}
		}
		
		mdsRpt.setSex(medProfile.getPatient().getSex().getDataValue());
		mdsRpt.setPatName(medProfile.getPatient().getName());
		mdsRpt.setPatNameChi(medProfile.getPatient().getNameChi());
		mdsRpt.setHkid(medProfile.getPatient().getHkid());
		mdsRpt.setPatCatCode(medProfile.getPatCatCode());
		mdsRpt.setPatAllergy(alertProfile == null || alertProfile.getLastVerifyDate() != null ? null : alertProfile.getAllergyDesc());
		mdsRpt.setPatAdr(alertProfile == null ? null : alertProfile.getAdrDesc());
		mdsRpt.setG6pdFlag(alertProfile != null && alertProfile.getG6pdAlert() != null);
		mdsRpt.setPregnancyCheckFlag(medProfile.getPregnancyCheckFlag());
		mdsRpt.setAllergyServerAvailableFlag(alertProfile != null && alertProfile.getStatus() != AlertProfileStatus.Error && !alertProfile.isEhrAllergyOnly());
		mdsRpt.setDrugDisplayName(alertMsg.getMdsOrderDesc());
		mdsRpt.setCaseNum(caseNum);
		mdsRpt.setMdsExceptionOverrideRptDtlList(new ArrayList<MdsExceptionOverrideRptDtl>());
		mdsRpt.setWard(medProfile.getWardCode());
		mdsRpt.setRemark(REPORT_MDSEXCEPTION_REMARK.get());
		mdsRpt.setEhrAllergyServerAvailableFlag(alertProfile != null && alertProfile.getEhrAllergySummary() != null && alertProfile.getEhrAllergySummary().getStatus() != EhrAlertStatus.Error);
		mdsRpt.setEhrAllergy(alertProfile != null && alertProfile.getEhrAllergySummary() != null ? alertProfile.getEhrAllergySummary().getAllergy() : null);
		mdsRpt.setEhrAdr(alertProfile != null && alertProfile.getEhrAllergySummary() != null ? alertProfile.getEhrAllergySummary().getAdr() : null);
		
		return mdsRpt;
	}
	
	@Remove
	public void destroy() {
	}

}
