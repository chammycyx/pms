package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.model.pms.dms.persistence.DmWarning;

import java.util.List;

import javax.ejb.Local;

@Local
public interface DmWarningListServiceLocal {
	
	void retrieveDmWarningList(String prefixWarnCode);
	
	List<DmWarning> retrieveDmWarningListForOrderEdit(String itemCode);		
	
	void destroy();
	
}
 