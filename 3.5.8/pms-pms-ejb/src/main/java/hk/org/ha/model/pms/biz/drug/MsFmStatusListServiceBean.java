package hk.org.ha.model.pms.biz.drug;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.MsFmStatusCacher;
import hk.org.ha.model.pms.dms.persistence.MsFmStatus;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("msFmStatusListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MsFmStatusListServiceBean implements MsFmStatusListServiceLocal {

	@Logger
	private Log logger;

	@In
	private MsFmStatusCacher msFmStatusCacher;

	@Out(required = false)
	private List<MsFmStatus> msFmStatusList;	
	
	public void retrieveMsFmStatusList(String prefixFmStatus) 
	{
		logger.debug("retrieveMsFmStatusList");
				
		if(StringUtils.isBlank(prefixFmStatus)){
			msFmStatusList = msFmStatusCacher.getMsFmStatusList();
		}else{
			msFmStatusList = msFmStatusCacher.getMsFmStatusListByFmStatus(prefixFmStatus);
		}
	}

	@Remove
	public void destroy() 
	{
		if (msFmStatusList != null) {
			msFmStatusList = null;
		}
	}
	
}
