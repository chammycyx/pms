package hk.org.ha.model.pms.vo.report;

import hk.org.ha.model.pms.udt.alert.AlertProfileStatus;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class InfusionInstructionSummaryHdr implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String ticketNum;
	
	private Date ticketDate;

	private String pasSpecCode;
	
	private String pasSubSpecCode;
	
	private String caseNum;
	
	private String patName;
	
	private String patNameChi;
	
	private String gender;
	
	private String age;
	
	private String drugAllergenDesc;
	
	private String adrAlertDesc;
	
	private String otherAllergyDesc;
	
	private String ehrAllergy;
	
	private String ehrAdr;

	private Boolean allergyServerAvailableFlag;
	
	private Boolean ehrAllergyException;
	
	private AlertProfileStatus alertProfileStatus;

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public Date getTicketDate() {
		return ticketDate;
	}

	public void setTicketDate(Date ticketDate) {
		this.ticketDate = ticketDate;
	}

	public String getPasSpecCode() {
		return pasSpecCode;
	}

	public void setPasSpecCode(String pasSpecCode) {
		this.pasSpecCode = pasSpecCode;
	}

	public String getPasSubSpecCode() {
		return pasSubSpecCode;
	}

	public void setPasSubSpecCode(String pasSubSpecCode) {
		this.pasSubSpecCode = pasSubSpecCode;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getPatNameChi() {
		return patNameChi;
	}

	public void setPatNameChi(String patNameChi) {
		this.patNameChi = patNameChi;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getDrugAllergenDesc() {
		return drugAllergenDesc;
	}

	public void setDrugAllergenDesc(String drugAllergenDesc) {
		this.drugAllergenDesc = drugAllergenDesc;
	}

	public String getAdrAlertDesc() {
		return adrAlertDesc;
	}

	public void setAdrAlertDesc(String adrAlertDesc) {
		this.adrAlertDesc = adrAlertDesc;
	}

	public String getOtherAllergyDesc() {
		return otherAllergyDesc;
	}

	public void setOtherAllergyDesc(String otherAllergyDesc) {
		this.otherAllergyDesc = otherAllergyDesc;
	}

	public String getEhrAllergy() {
		return ehrAllergy;
	}

	public void setEhrAllergy(String ehrAllergy) {
		this.ehrAllergy = ehrAllergy;
	}

	public String getEhrAdr() {
		return ehrAdr;
	}

	public void setEhrAdr(String ehrAdr) {
		this.ehrAdr = ehrAdr;
	}

	public void setAllergyServerAvailableFlag(Boolean allergyServerAvailableFlag) {
		this.allergyServerAvailableFlag = allergyServerAvailableFlag;
	}

	public Boolean getAllergyServerAvailableFlag() {
		return allergyServerAvailableFlag;
	}

	public void setEhrAllergyException(Boolean ehrAllergyException) {
		this.ehrAllergyException = ehrAllergyException;
	}

	public Boolean getEhrAllergyException() {
		return ehrAllergyException;
	}

	public void setAlertProfileStatus(AlertProfileStatus alertProfileStatus) {
		this.alertProfileStatus = alertProfileStatus;
	}

	public AlertProfileStatus getAlertProfileStatus() {
		return alertProfileStatus;
	}
	
}
