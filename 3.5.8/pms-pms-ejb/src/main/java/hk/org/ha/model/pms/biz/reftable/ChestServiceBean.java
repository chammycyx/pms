package hk.org.ha.model.pms.biz.reftable;


import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmMoeProperty;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.Chest;
import hk.org.ha.model.pms.persistence.reftable.ChestItem;
import hk.org.ha.model.pms.vo.drug.DmDrugLite;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("chestService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ChestServiceBean implements ChestServiceLocal {
	
	@PersistenceContext
	private EntityManager em;

	private Chest chest;

	@In
	private Workstore workstore;
	
	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;
	
	@In
	private ChestManagerLocal chestManager;
	
	private boolean retrieveSuccess;
	
	private boolean saveSuccess;
	
	private String formularyStatus = "1,2,3,4,5,6";
	
	public DmDrugLite retrieveChestDmDrugLite(String itemCode){
		DmDrugLite chestDmDrugLite = null;
		WorkstoreDrug workstoreDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, itemCode);
		if ( workstoreDrug != null && 
				workstoreDrug.getMsWorkstoreDrug() != null &&
				workstoreDrug.getDmDrug() != null && 
				isValidChestItemDmDrug(workstoreDrug)) {
			chestDmDrugLite = workstoreDrug.getDmDrugLite();
		}
		
		return chestDmDrugLite;
	}
	
	private boolean isValidChestItemDmDrug(WorkstoreDrug workstoreDrug) {
		DmMoeProperty dmMoeProperty = workstoreDrug.getDmDrug().getDmMoeProperty();
		return ( dmMoeProperty != null && 
					dmMoeProperty.getDrugScope() > 100 && 
					isFormularyItem(dmMoeProperty.getFormularyStatus()) && 
					StringUtils.isNotBlank(dmMoeProperty.getDispenseDosageUnit()));
	}
	
	private boolean isFormularyItem(String status){
		String[] formularyStatusArray = formularyStatus.split(",");
		for ( String s : formularyStatusArray ) {
			if ( StringUtils.equals(status, s) ) {
				return true;
			}
		}
		return false;
	}
	
	public Chest createChest()
	{
		chest = new Chest();
		chest.setWorkstore(workstore);
		chest.setChestItemList(new ArrayList<ChestItem>());
		
		return chest;
	}
	
	public Chest retrieveChest(String unitDoseName) 
	{
		retrieveSuccess = true;		
		
		Chest inChest = chestManager.retrieveChest(unitDoseName);
		
		if ( inChest != null ) {
			chest = inChest;
			chest.loadChild(workstore);
		} else {
			retrieveSuccess = false;
		}
		return chest;
	}
	
	public void updateChest(Chest chest) 
	{
		chest.setWorkstore(em.find(Workstore.class, chest.getWorkstore().getId()));
		if(chest.isNew())
		{
			Chest inChest = chestManager.retrieveChest(chest.getUnitDoseName());
			
			if( inChest != null )
			{
				saveSuccess = false;
				return;
			}
			else
			{
				checkChestItemMarkDelete(chest);
				if( !chest.getChestItemList().isEmpty() )
				{
					em.persist(chest);
					em.flush();
				}
			}
		}
		else
		{
			checkChestItemMarkDelete(chest);
			em.merge(chest);
			em.flush();
		}
		
		saveSuccess = true;

	}
	 
	public void deleteChest(Chest chest)
	{
		em.remove(em.merge(chest));
		em.flush();
	}
	
	private void checkChestItemMarkDelete(Chest chest)
	{
		int sortSeq = 1;
		for(Iterator<ChestItem> it = chest.getChestItemList().iterator();it.hasNext();)
		{
			ChestItem chestItem = it.next();
			if(chestItem.isMarkDelete())
			{
				it.remove();
			}
			else if( StringUtils.isBlank(chestItem.getItemCode()) ) 
			{
				it.remove();
			}
			else
			{
				chestItem.setSortSeq(sortSeq++);
			}
		}
	}
	
	public boolean isRetrieveSuccess(){
		return retrieveSuccess;
	}

	public boolean isSaveSuccess(){
		return saveSuccess;
	}
	
	@Remove
	public void destroy() 
	{
		if (chest != null) {
			chest = null;
		}
	}
}
