package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class InfusionInstructionSummary implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private InfusionInstructionSummaryHdr infusionInstructionSummaryHdr;
	
	private List<InfusionInstructionSummaryDtl> infusionInstructionSummaryDtlList;
	
	private String infusionInstructionSummaryFtr;

	public void setInfusionInstructionSummaryHdr(
			InfusionInstructionSummaryHdr infusionInstructionSummaryHdr) {
		this.infusionInstructionSummaryHdr = infusionInstructionSummaryHdr;
	}

	public InfusionInstructionSummaryHdr getInfusionInstructionSummaryHdr() {
		return infusionInstructionSummaryHdr;
	}

	public void setInfusionInstructionSummaryDtlList(
			List<InfusionInstructionSummaryDtl> infusionInstructionSummaryDtlList) {
		this.infusionInstructionSummaryDtlList = infusionInstructionSummaryDtlList;
	}

	public List<InfusionInstructionSummaryDtl> getInfusionInstructionSummaryDtlList() {
		return infusionInstructionSummaryDtlList;
	}

	public void setInfusionInstructionSummaryFtr(
			String infusionInstructionSummaryFtr) {
		this.infusionInstructionSummaryFtr = infusionInstructionSummaryFtr;
	}

	public String getInfusionInstructionSummaryFtr() {
		return infusionInstructionSummaryFtr;
	}
	
}
