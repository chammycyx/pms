package hk.org.ha.model.pms.biz.dashboard.mp;
import javax.ejb.Local;

@Local
public interface MedProfileDashboardServiceLocal {
	void init();
	void countAllDashboardItems();
	void destroy();
}
