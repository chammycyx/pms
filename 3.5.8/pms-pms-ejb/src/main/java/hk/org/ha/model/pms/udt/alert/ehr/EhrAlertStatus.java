package hk.org.ha.model.pms.udt.alert.ehr;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum EhrAlertStatus implements StringValuedEnum {

	Exist("X", "Exist"),
	NotFound("N", "Not Found"),
	Error("E", "Error");
	
    private final String dataValue;
    private final String displayValue;

    EhrAlertStatus(final String dataValue, final String displayValue) {
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }
    
    public String getDataValue() {
        return this.dataValue;
    }
    
    public String getDisplayValue() {
        return this.displayValue;
    }
    
    public static EhrAlertStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(EhrAlertStatus.class, dataValue);
    }
    
    public static class Converter extends StringValuedEnumConverter<EhrAlertStatus> {
    	
		private static final long serialVersionUID = 1L;

		public Class<EhrAlertStatus> getEnumClass() {
			return EhrAlertStatus.class;
		}
    }
}
