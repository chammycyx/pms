package hk.org.ha.model.pms.persistence.medprofile;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.RecordStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryScheduleItemDueOnType;
import hk.org.ha.model.pms.udt.medprofile.DeliveryScheduleItemOrderType;
import hk.org.ha.model.pms.udt.medprofile.DeliveryScheduleItemWardType;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "DELIVERY_SCHEDULE_ITEM")
@Customizer(AuditCustomizer.class)
public class DeliveryScheduleItem extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "deliveryScheduleItemSeq")
	@SequenceGenerator(name = "deliveryScheduleItemSeq", sequenceName = "SQ_DELIVERY_SCHEDULE_ITEM", initialValue = 100000000)
	private Long id;
	
	@Converter(name = "DeliveryScheduleItem.orderType", converterClass = DeliveryScheduleItemOrderType.Converter.class)
	@Convert("DeliveryScheduleItem.orderType")
	@Column(name = "ORDER_TYPE", length = 2)
	private DeliveryScheduleItemOrderType orderType;
	
	@Converter(name = "DeliveryScheduleItem.wardType", converterClass = DeliveryScheduleItemWardType.Converter.class)
	@Convert("DeliveryScheduleItem.wardType")
	@Column(name = "WARD_TYPE", length = 1)
	private DeliveryScheduleItemWardType wardType;
	
	@Lob
	@Column(name = "WARD_CODE_CSV")
	private String wardCodeCsv;
	
	@Lob
	@Column(name = "WARD_GROUP_CSV")
	private String wardGroupCsv;
	
	@Column(name = "SCHEDULE_TIME", length = 4)
	private Integer scheduleTime;
	
	@Column(name = "DUE_HOUR", length = 2)
	private Integer dueHour;
	
	@Converter(name = "DeliveryScheduleItem.dueOnType", converterClass = DeliveryScheduleItemDueOnType.Converter.class)
	@Convert("DeliveryScheduleItem.dueOnType")
	@Column(name = "DUE_ON_TYPE", length = 1)
	private DeliveryScheduleItemDueOnType dueOnType;
	
	@Column(name = "GENERATE_DAY", length = 1)
	private Integer generateDay;
	
	@Column(name = "URGENT_FLAG", nullable = false)
	private Boolean urgentFlag;
	
	@Column(name = "STAT_FLAG", nullable = false)
	private Boolean statFlag;
	
	@Column(name = "OVERDUE_FLAG", nullable = false)
	private Boolean overdueFlag;
	
	@Column(name = "HIGHLIGHT_FLAG", nullable = false)
	private Boolean highlightFlag;
	
	@Converter(name = "DeliveryScheduleItem.status", converterClass = RecordStatus.Converter.class)
	@Convert("DeliveryScheduleItem.status")
	@Column(name = "STATUS", length = 1)
	private RecordStatus status;
		
	@ManyToOne
	@JoinColumn(name = "DELIVERY_SCHEDULE_ID", nullable = false)
	private DeliverySchedule deliverySchedule;
	
    @OneToMany(mappedBy = "deliveryScheduleItem")
    private List<DeliveryRequestGroup> deliveryRequestGroupList;
	
	@Transient
	private String scheduleTimeStr;
	
	@Transient
	private String dueHourStr;
	
	@Transient
	private String deliveryRequestCreateDateStr;

	@Transient
	private String deliveryRequestWorkstation;
	
	@Transient
	private String deliveryRequestCreateUser;
	
	@Transient
	private Boolean multiDeliveryFlag;
	
	public DeliveryScheduleItem() {
		urgentFlag = Boolean.FALSE;
		statFlag = Boolean.FALSE;
		overdueFlag = Boolean.FALSE;
		highlightFlag = Boolean.FALSE;
		multiDeliveryFlag = Boolean.FALSE;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DeliveryScheduleItemOrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(DeliveryScheduleItemOrderType orderType) {
		this.orderType = orderType;
	}

	public DeliveryScheduleItemWardType getWardType() {
		return wardType;
	}

	public void setWardType(DeliveryScheduleItemWardType wardType) {
		this.wardType = wardType;
	}

	public String getWardGroupCsv() {
		return wardGroupCsv;
	}

	public void setWardGroupCsv(String wardGroupCsv) {
		this.wardGroupCsv = wardGroupCsv;
	}

	public Integer getScheduleTime() {
		return scheduleTime;
	}

	public void setScheduleTime(Integer scheduleTime) {
		this.scheduleTime = scheduleTime;
	}

	public Integer getDueHour() {
		return dueHour;
	}

	public void setDueHour(Integer dueHour) {
		this.dueHour = dueHour;
	}

	public DeliveryScheduleItemDueOnType getDueOnType() {
		return dueOnType;
	}

	public void setDueOnType(DeliveryScheduleItemDueOnType dueOnType) {
		this.dueOnType = dueOnType;
	}

	public Integer getGenerateDay() {
		return generateDay;
	}

	public void setGenerateDay(Integer generateDay) {
		this.generateDay = generateDay;
	}

	public Boolean getUrgentFlag() {
		return urgentFlag;
	}

	public void setUrgentFlag(Boolean urgentFlag) {
		this.urgentFlag = urgentFlag;
	}

	public Boolean getStatFlag() {
		return statFlag;
	}

	public void setStatFlag(Boolean statFlag) {
		this.statFlag = statFlag;
	}

	public Boolean getOverdueFlag() {
		return overdueFlag;
	}

	public void setOverdueFlag(Boolean overdueFlag) {
		this.overdueFlag = overdueFlag;
	}

	public Boolean getHighlightFlag() {
		return highlightFlag;
	}

	public void setHighlightFlag(Boolean highlightFlag) {
		this.highlightFlag = highlightFlag;
	}

	public RecordStatus getStatus() {
		return status;
	}

	public void setStatus(RecordStatus status) {
		this.status = status;
	}

	public DeliverySchedule getDeliverySchedule() {
		return deliverySchedule;
	}

	public void setDeliverySchedule(DeliverySchedule deliverySchedule) {
		this.deliverySchedule = deliverySchedule;
	}

	public List<DeliveryRequestGroup> getDeliveryRequestGroupList() {
		return deliveryRequestGroupList;
	}

	public void setDeliveryRequestGroupList(List<DeliveryRequestGroup> deliveryRequestGroupList) {
		this.deliveryRequestGroupList = deliveryRequestGroupList;
	}
	
	public String getWardCodeCsv() {
		return wardCodeCsv;
	}

	public void setWardCodeCsv(String wardCodeCsv) {
		this.wardCodeCsv = wardCodeCsv;
	}
	
	public String getScheduleTimeStr() {
		return scheduleTimeStr;
	}

	public void setScheduleTimeStr(String scheduleTimeStr) {
		this.scheduleTimeStr = scheduleTimeStr;
	}

	public String getDueHourStr() {
		return dueHourStr;
	}

	public void setDueHourStr(String dueHourStr) {
		this.dueHourStr = dueHourStr;
	}

	public String getDeliveryRequestCreateDateStr() {
		return deliveryRequestCreateDateStr;
	}

	public void setDeliveryRequestCreateDateStr(String deliveryRequestCreateDateStr) {
		this.deliveryRequestCreateDateStr = deliveryRequestCreateDateStr;
	}

	public String getDeliveryRequestWorkstation() {
		return deliveryRequestWorkstation;
	}

	public void setDeliveryRequestWorkstation(String deliveryRequestWorkstation) {
		this.deliveryRequestWorkstation = deliveryRequestWorkstation;
	}

	public String getDeliveryRequestCreateUser() {
		return deliveryRequestCreateUser;
	}

	public void setDeliveryRequestCreateUser(String deliveryRequestCreateUser) {
		this.deliveryRequestCreateUser = deliveryRequestCreateUser;
	}

	public Boolean getMultiDeliveryFlag() {
		return multiDeliveryFlag;
	}

	public void setMultiDeliveryFlag(Boolean multiDeliveryFlag) {
		this.multiDeliveryFlag = multiDeliveryFlag;
	}

	// helper methods
	public boolean isNew() {
		return this.getId() == null;
	}
}
