package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class EdsActivityRptDtl implements Serializable {

	private static final long serialVersionUID = 1L;

	private String timeRange;
	
	private int dataEntryAvgTime;
	
	private int pickAvgTime;
	
	private int assembleAvgTime;
	
	private int checkAvgTime;
	
	private int issueAvgTime;

	public String getTimeRange() {
		return timeRange;
	}

	public void setTimeRange(String timeRange) {
		this.timeRange = timeRange;
	}

	public int getDataEntryAvgTime() {
		return dataEntryAvgTime;
	}

	public void setDataEntryAvgTime(int dataEntryAvgTime) {
		this.dataEntryAvgTime = dataEntryAvgTime;
	}

	public int getPickAvgTime() {
		return pickAvgTime;
	}

	public void setPickAvgTime(int pickAvgTime) {
		this.pickAvgTime = pickAvgTime;
	}

	public int getAssembleAvgTime() {
		return assembleAvgTime;
	}

	public void setAssembleAvgTime(int assembleAvgTime) {
		this.assembleAvgTime = assembleAvgTime;
	}

	public int getCheckAvgTime() {
		return checkAvgTime;
	}

	public void setCheckAvgTime(int checkAvgTime) {
		this.checkAvgTime = checkAvgTime;
	}

	public int getIssueAvgTime() {
		return issueAvgTime;
	}

	public void setIssueAvgTime(int issueAvgTime) {
		this.issueAvgTime = issueAvgTime;
	}
	
}
