package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.ChargeSpecialty;
import hk.org.ha.model.pms.udt.reftable.ChargeSpecialtyType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("chargeSpecialtyManager")
public class ChargeSpecialtyManagerBean implements ChargeSpecialtyManagerLocal {
	
	@PersistenceContext
	private EntityManager em;
	
    public ChargeSpecialtyManagerBean() {
    }

    @Override
    @SuppressWarnings("unchecked")
	public Map<ChargeSpecialtyType, String> retrieveChargeSpecialtyCodeMap(Workstore workstore, String specCode) {

    	Map<ChargeSpecialtyType, String> chargeSpecialtyCodeMap = new HashMap<ChargeSpecialtyType, String>();
    	
    	if (workstore == null) {
    		return chargeSpecialtyCodeMap;
    	}
    	
    	List<ChargeSpecialty> chargeSpecialtyList = em.createQuery(
    			"select o from ChargeSpecialty o" + // 20120303 index check : ChargeSpecialty.hospital : I_CHARGE_SPECIALTY_01
    			" where o.hospital = :hospital" +
    			" and o.specCode = :specCode")
    			.setParameter("hospital", workstore.getHospital())
    			.setParameter("specCode", specCode)
    			.getResultList();
    	
    	for (ChargeSpecialty chargeSpeicalty: chargeSpecialtyList) {
    		chargeSpecialtyCodeMap.put(chargeSpeicalty.getType(), chargeSpeicalty.getChargeSpecCode());
    	}
    	
    	return chargeSpecialtyCodeMap;
    }
}
