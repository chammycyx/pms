package hk.org.ha.model.pms.biz.reftable;


import hk.org.ha.model.pms.persistence.reftable.PharmRemarkTemplate;

import java.util.Collection;

import javax.ejb.Local;

@Local
public interface PharmRemarkTemplateListServiceLocal {

	void retrievePharmRemarkTemplateList();
	
	void updatePharmRemarkTemplateList(Collection<PharmRemarkTemplate> newPharmRemarkTemplateList);
	
	void destroy();
}
