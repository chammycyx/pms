package hk.org.ha.model.pms.persistence.disp;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.disp.DispOrderItemRemarkType;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.label.DeltaChangeType;
import hk.org.ha.model.pms.util.StopWatcher;
import hk.org.ha.model.pms.vo.drug.DmWarningLite;
import hk.org.ha.model.pms.vo.label.BaseLabel;
import hk.org.ha.model.pms.vo.label.DispLabel;
import hk.org.ha.model.pms.vo.printing.PrintLabelDetail;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.jboss.seam.security.Identity;

@Entity
@Table(name = "DISP_ORDER_ITEM")
public class DispOrderItem extends VersionEntity {

	private static final long serialVersionUID = 1L;

	private static final String JAXB_CONTEXT_LABEL = "hk.org.ha.model.pms.vo.label";
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dispOrderItemSeq")
	@SequenceGenerator(name = "dispOrderItemSeq", sequenceName = "SQ_DISP_ORDER_ITEM", initialValue = 100000000)
	private Long id;
	
	@Column(name = "DISP_QTY", precision=19, scale=4, nullable = false)
	private BigDecimal dispQty;
	
	@Column(name = "DURATION_IN_DAY", length = 4)
	private Integer durationInDay;
	
	@Column(name = "START_DATE")
	@Temporal(TemporalType.DATE)
	private Date startDate;
	
	@Column(name = "END_DATE")
	@Temporal(TemporalType.DATE)
	private Date endDate;
	
	@Column(name = "WARN_CODE_1", length = 2)
	private String warnCode1;
	
	@Column(name = "WARN_CODE_2", length = 2)
	private String warnCode2;
	
	@Column(name = "WARN_CODE_3", length = 2)
	private String warnCode3;
	
	@Column(name = "WARN_CODE_4", length = 2)
	private String warnCode4;

	@Column(name = "WARN_DESC_1", length = 40)
	private String warnDesc1;

	@Column(name = "WARN_DESC_2", length = 40)
	private String warnDesc2;

	@Column(name = "WARN_DESC_3", length = 40)
	private String warnDesc3;

	@Column(name = "WARN_DESC_4", length = 40)
	private String warnDesc4;

	@Column(name = "LIFESTYLE_FLAG", nullable = false)
	private Boolean lifestyleFlag;
	
	@Column(name = "ONCOLOGY_FLAG", nullable = false)
	private Boolean oncologyFlag;

	@Lob
	@Column(name = "LABEL_XML")
	private String labelXml;
	
	@Column(name = "REMARK_TEXT", length = 50)
	private String remarkText;
	
	@Converter(name = "DispOrderItem.remarkType", converterClass = DispOrderItemRemarkType.Converter.class)
    @Convert("DispOrderItem.remarkType")
	@Column(name="REMARK_TYPE", nullable = false, length = 1)
	private DispOrderItemRemarkType remarkType;

	@Column(name = "REMARK_UPDATE_USER", length = 12)
	private String remarkUpdateUser;
	
	@Column(name = "REMARK_UPDATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date remarkUpdateDate;
	
	@Column(name = "BIN_NUM", length = 4)
	private String binNum;

	@Converter(name = "DispOrderItem.status", converterClass = DispOrderItemStatus.Converter.class)
    @Convert("DispOrderItem.status")
	@Column(name="STATUS", nullable = false, length = 1)
	private DispOrderItemStatus status;
	
	@Column(name = "WORKSTATION_ID")
	private Long workstationId;

	@Column(name = "WORKSTATION_CODE", length = 4)
	private String workstationCode;

	@Column(name = "PICK_USER", length = 12)
	private String pickUser;
	
	@Column(name = "PICK_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date pickDate;
	
	@Column(name = "ASSEMBLE_USER", length = 12)
	private String assembleUser;
	
	@Column(name = "ASSEMBLE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date assembleDate;
	
	@Column(name = "UNIT_PRICE", precision=19, scale=4)
	private BigDecimal unitPrice;

	@Column(name = "PRINT_FLAG", nullable = false)
	private Boolean printFlag;

	@Column(name = "CHARGE_SPEC_CODE", length = 4)
	private String chargeSpecCode;
	
	@Column(name = "DELIVERY_ITEM_ID")
	private Long deliveryItemId;
	
	@Column(name = "RE_DISP_FLAG", nullable = false)
	private Boolean reDispFlag;
	
	@ManyToOne
	@JoinColumn(name = "DISP_ORDER_ID", nullable = false)
	private DispOrder dispOrder;
	
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "PHARM_ORDER_ITEM_ID", nullable = false)
    private PharmOrderItem pharmOrderItem;

	@Column(name = "ITEM_NUM", nullable = false)
	private Integer itemNum;
	
	@Column(name = "NUM_OF_LABEL", nullable = false, length = 1)
	private Integer numOfLabel;
	
	@Column(name = "SORT_SEQ")
	private Integer sortSeq;
	
	@Column(name = "PIVAS_FLAG")
	private Boolean pivasFlag;
	
	@Column(name = "TPN_REQUEST_ID")
	private Long tpnRequestId;
	
	@Column(name = "SFI_OVERRIDE_WARN_FLAG")
	private Boolean sfiOverrideWarnFlag;

	@Transient
	private Integer totalNumOfLabel;	//for calculate the total mpDispLabel amount
	
	@Transient
	private List<InvoiceItem> invoiceItemList;
	
	@Transient
	private List<CapdVoucherItem> capdVoucherItemList;

	@Transient
	private BaseLabel baseLabel;
	
	@Transient
	private DmWarningLite dmWarningLite1 = null;
	
	@Transient
	private DmWarningLite dmWarningLite2 = null;
	
	@Transient
	private DmWarningLite dmWarningLite3 = null;
	
	@Transient
	private DmWarningLite dmWarningLite4 = null;
	
	// for Cddh retrieving from Legacy
	@Transient
	private boolean legacy;

	@Transient
	private boolean legacyMultiDose;

	@Transient
	private String singleDosageValue;
	
	@Transient
	private String overrideDesc;
	
	@Transient
	private String overrideRemark;
	
	@Transient
	private String carsRemark;
	
	@Transient
	private String episodeType;
	
	@Transient
	private String overrideSeqNum;
	
	@Transient
	private String itemCatCode;
	
	@Transient
	private String cddhDrugDesc;
	
	@Transient
	private String splitFlag;
	
	@Transient
	private Boolean reprint;

	@Transient
	private Boolean printMpDispLabelInstructionFlag;
	
	private transient PrintLabelDetail printLabelDetail;
	
	@Transient
	private Boolean singleDispFlag;

	private transient Boolean replenishFlag;
	
	private transient Boolean urgentFlag;
	
	private transient Boolean printRefillAuxLabelFlag;
	
	private transient BigDecimal chargeQty;
	
	private transient String verifyUser;
	
	private transient String replenishItemUpdateUser;
	
	@Transient
	private String aspItemType;
	
	public DispOrderItem() {
		remarkType = DispOrderItemRemarkType.Empty;
		printFlag = Boolean.FALSE;
		setNumOfLabel(Integer.valueOf(1));
		reprint = Boolean.FALSE;
		lifestyleFlag = Boolean.FALSE;
		oncologyFlag = Boolean.FALSE;
		reDispFlag = Boolean.FALSE;
		urgentFlag = Boolean.FALSE;
		printRefillAuxLabelFlag = Boolean.FALSE;
		replenishFlag = Boolean.FALSE;
		pivasFlag = Boolean.FALSE;
		sfiOverrideWarnFlag = Boolean.FALSE;
	}
	
	@PostLoad
	public void postLoad() {
    	if (StopWatcher.isDebugEnabled()) {
    		StopWatcher.resume();
    	}
	
		if (Identity.instance().isLoggedIn()) {
	    	if (warnCode1 != null) {
	    		dmWarningLite1 = DmWarningLite.objValueOf(warnCode1);
	    	}
	    	if (warnCode2 != null) {
	    		dmWarningLite2 = DmWarningLite.objValueOf(warnCode2);
	    	}
	    	if (warnCode3 != null) {
	    		dmWarningLite3 = DmWarningLite.objValueOf(warnCode3);
	    	}
	    	if (warnCode4 != null) {
	    		dmWarningLite4 = DmWarningLite.objValueOf(warnCode4);
	    	}
		}
		if (labelXml != null) {
			JaxbWrapper<BaseLabel> labelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
			baseLabel = labelJaxbWrapper.unmarshall(labelXml);
		}
		
    	if (StopWatcher.isDebugEnabled()) {
    		StopWatcher.suspend();
    	}
	}

	@PrePersist
	@PreUpdate
	public void preSave() {
		if (baseLabel != null) {
			JaxbWrapper<BaseLabel> labelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
			labelXml = labelJaxbWrapper.marshall(baseLabel);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getDispQty() {
		return dispQty;
	}

	public void setDispQty(BigDecimal dispQty) {
		this.dispQty = dispQty;
	}

	public Integer getDurationInDay() {
		return durationInDay;
	}

	public void setDurationInDay(Integer durationInDay) {
		this.durationInDay = durationInDay;
	}

	public Date getStartDate() {
		if (startDate == null) {
			return null;
		}
		else {
			return new Date(startDate.getTime());
		}
	}

	public void setStartDate(Date startDate) {
		if (startDate == null) {
			this.startDate = null;
		}
		else {
			this.startDate = new Date(startDate.getTime());
		}
	}

	public Date getEndDate() {
		if (endDate == null) {
			return null;
		}
		else {
			return new Date(endDate.getTime());
		}
	}

	public void setEndDate(Date endDate) {
		if (endDate == null) {
			this.endDate = null;
		}
		else {
			this.endDate = new Date(endDate.getTime());
		}
	}

	public String getWarnCode1() {
		return warnCode1;
	}

	public void setWarnCode1(String warnCode1) {
		this.warnCode1 = warnCode1;
	}

	public String getWarnCode2() {
		return warnCode2;
	}

	public void setWarnCode2(String warnCode2) {
		this.warnCode2 = warnCode2;
	}

	public String getWarnCode3() {
		return warnCode3;
	}

	public void setWarnCode3(String warnCode3) {
		this.warnCode3 = warnCode3;
	}

	public String getWarnCode4() {
		return warnCode4;
	}

	public void setWarnCode4(String warnCode4) {
		this.warnCode4 = warnCode4;
	}

	public String getWarnDesc1() {
		return warnDesc1;
	}

	public void setWarnDesc1(String warnDesc1) {
		this.warnDesc1 = warnDesc1;
	}

	public String getWarnDesc2() {
		return warnDesc2;
	}

	public void setWarnDesc2(String warnDesc2) {
		this.warnDesc2 = warnDesc2;
	}

	public String getWarnDesc3() {
		return warnDesc3;
	}

	public void setWarnDesc3(String warnDesc3) {
		this.warnDesc3 = warnDesc3;
	}

	public String getWarnDesc4() {
		return warnDesc4;
	}

	public void setWarnDesc4(String warnDesc4) {
		this.warnDesc4 = warnDesc4;
	}

	public Boolean getLifestyleFlag() {
		return lifestyleFlag;
	}

	public void setLifestyleFlag(Boolean lifestyleFlag) {
		this.lifestyleFlag = lifestyleFlag;
	}

	public Boolean getOncologyFlag() {
		return oncologyFlag;
	}

	public void setOncologyFlag(Boolean oncologyFlag) {
		this.oncologyFlag = oncologyFlag;
	}

	public String getLabelXml() {
		return labelXml;
	}

	public void setLabelXml(String labelXml) {
		this.labelXml = labelXml;
	}

	public String getRemarkText() {
		return remarkText;
	}

	public void setRemarkText(String remarkText) {
		this.remarkText = remarkText;
	}

	public DispOrderItemRemarkType getRemarkType() {
		return remarkType;
	}

	public void setRemarkType(DispOrderItemRemarkType remarkType) {
		this.remarkType = remarkType;
	}

	public String getRemarkUpdateUser() {
		return remarkUpdateUser;
	}

	public void setRemarkUpdateUser(String remarkUpdateUser) {
		this.remarkUpdateUser = remarkUpdateUser;
	}

	public Date getRemarkUpdateDate() {
		if (remarkUpdateDate == null) {
			return null;
		}
		else {
			return new Date(remarkUpdateDate.getTime());
		}
	}

	public void setRemarkUpdateDate(Date remarkUpdateDate) {
		if (remarkUpdateDate == null) {
			this.remarkUpdateDate = null;
		}
		else {
			this.remarkUpdateDate = new Date(remarkUpdateDate.getTime());
		}
	}

	public String getBinNum() {
		return binNum;
	}

	public void setBinNum(String binNum) {
		this.binNum = binNum;
	}

	public DispOrderItemStatus getStatus() {
		return status;
	}

	public void setStatus(DispOrderItemStatus status) {
		this.status = status;
	}

	public Long getWorkstationId() {
		return workstationId;
	}

	public void setWorkstationId(Long workstationId) {
		this.workstationId = workstationId;
	}

	public String getWorkstationCode() {
		return workstationCode;
	}

	public void setWorkstationCode(String workstationCode) {
		this.workstationCode = workstationCode;
	}

	public String getPickUser() {
		return pickUser;
	}

	public void setPickUser(String pickUser) {
		this.pickUser = pickUser;
	}

	public Date getPickDate() {
		if (pickDate == null) {
			return null;
		}
		else {
			return new Date(pickDate.getTime());
		}
	}

	public void setPickDate(Date pickDate) {
		if (pickDate == null) {
			this.pickDate = null;
		}
		else {
			this.pickDate = new Date(pickDate.getTime());
		}
	}

	public String getAssembleUser() {
		return assembleUser;
	}

	public void setAssembleUser(String assembleUser) {
		this.assembleUser = assembleUser;
	}

	public Date getAssembleDate() {
		if (assembleDate == null) {
			return null;
		}
		else {
			return new Date(assembleDate.getTime());
		}
	}

	public void setAssembleDate(Date assembleDate) {
		if (assembleDate == null) {
			this.assembleDate = null;
		}
		else {
			this.assembleDate = new Date(assembleDate.getTime());
		}
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Boolean getPrintFlag() {
		return printFlag;
	}

	public void setPrintFlag(Boolean printFlag) {
		this.printFlag = printFlag;
	}
	
	public String getChargeSpecCode() {
		return chargeSpecCode;
	}

	public void setChargeSpecCode(String chargeSpecCode) {
		this.chargeSpecCode = chargeSpecCode;
	}
	
	public Long getDeliveryItemId() {
		return deliveryItemId;
	}

	public void setDeliveryItemId(Long deliveryItemId) {
		this.deliveryItemId = deliveryItemId;
	}

	public Boolean getReDispFlag() {
		return reDispFlag;
	}

	public void setReDispFlag(Boolean reDispFlag) {
		this.reDispFlag = reDispFlag;
	}

	public DispOrder getDispOrder() {
		return dispOrder;
	}
	
	public void setDispOrder(DispOrder dispOrder) {
		this.dispOrder = dispOrder;
	}

	public PharmOrderItem getPharmOrderItem() {
		return pharmOrderItem;
	}

	public void setPharmOrderItem(PharmOrderItem pharmOrderItem) {
		this.pharmOrderItem = pharmOrderItem;
	}

	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public Integer getTotalNumOfLabel() {
		return totalNumOfLabel;
	}

	public void setTotalNumOfLabel(Integer totalNumOfLabel) {
		this.totalNumOfLabel = totalNumOfLabel;
	}

	public void setNumOfLabel(Integer numOfLabel) {
		this.numOfLabel = numOfLabel;
	}

	public Integer getNumOfLabel() {
		return numOfLabel;
	}

	public Integer getSortSeq() {
		return sortSeq;
	}

	public void setSortSeq(Integer sortSeq) {
		this.sortSeq = sortSeq;
	}
	
	public Boolean getPivasFlag() {
		return pivasFlag;
	}

	public void setPivasFlag(Boolean pivasFlag) {
		this.pivasFlag = pivasFlag;
	}

	public Long getTpnRequestId() {
		return tpnRequestId;
	}

	public void setTpnRequestId(Long tpnRequestId) {
		this.tpnRequestId = tpnRequestId;
	}

	public List<InvoiceItem> getInvoiceItemList() {
		if (invoiceItemList == null) {
			invoiceItemList = new ArrayList<InvoiceItem>();
		}
		return invoiceItemList;
	}

	public void setInvoiceItemList(List<InvoiceItem> invoiceItemList) {
		this.invoiceItemList = invoiceItemList;
	}
	
	public void addInvoiceItem(InvoiceItem invoiceItem) {
		invoiceItem.setDispOrderItem(this);
		getInvoiceItemList().add(invoiceItem);
	}
	
	public void clearInvoiceItemList() {
		invoiceItemList = new ArrayList<InvoiceItem>();
	}
	
	public List<CapdVoucherItem> getCapdVoucherItemList() {
		if (capdVoucherItemList == null) {
			capdVoucherItemList = new ArrayList<CapdVoucherItem>();
		}
		return capdVoucherItemList;
	}

	public void setCapdVoucherItemList(List<CapdVoucherItem> capdVoucherItemList) {
		this.capdVoucherItemList = capdVoucherItemList;
	}
	
	public void addCapdVoucherItem(CapdVoucherItem capdVoucherItem) {
		capdVoucherItem.setDispOrderItem(this);
		getCapdVoucherItemList().add(capdVoucherItem);
	}
	
	public void clearCapdVoucherItemList() {
		capdVoucherItemList = new ArrayList<CapdVoucherItem>();
	}
	
	public BaseLabel getBaseLabel() {
		return baseLabel;
	}

	public void setBaseLabel(BaseLabel baseLabel) {
		this.baseLabel = baseLabel;
	}

	public DmWarningLite getDmWarningLite1() {
		return dmWarningLite1;
	}

	public void setDmWarningLite1(DmWarningLite dmWarningLite1) {
		this.dmWarningLite1 = dmWarningLite1;
	}

	public DmWarningLite getDmWarningLite2() {
		return dmWarningLite2;
	}

	public void setDmWarningLite2(DmWarningLite dmWarningLite2) {
		this.dmWarningLite2 = dmWarningLite2;
	}

	public DmWarningLite getDmWarningLite3() {
		return dmWarningLite3;
	}

	public void setDmWarningLite3(DmWarningLite dmWarningLite3) {
		this.dmWarningLite3 = dmWarningLite3;
	}

	public DmWarningLite getDmWarningLite4() {
		return dmWarningLite4;
	}

	public void setDmWarningLite4(DmWarningLite dmWarningLite4) {
		this.dmWarningLite4 = dmWarningLite4;
	}

	public boolean isLegacy() {
		return legacy;
	}

	public void setLegacy(boolean legacy) {
		this.legacy = legacy;
	}

	public boolean isLegacyMultiDose() {
		return legacyMultiDose;
	}

	public void setLegacyMultiDose(boolean legacyMultiDose) {
		this.legacyMultiDose = legacyMultiDose;
	}

	public String getSingleDosageValue() {
		return singleDosageValue;
	}

	public void setSingleDosageValue(String singleDosageValue) {
		this.singleDosageValue = singleDosageValue;
	}

	public String getOverrideDesc() {
		return overrideDesc;
	}

	public void setOverrideDesc(String overrideDesc) {
		this.overrideDesc = overrideDesc;
	}

	public String getOverrideRemark() {
		return overrideRemark;
	}

	public void setOverrideRemark(String overrideRemark) {
		this.overrideRemark = overrideRemark;
	}

	public String getCarsRemark() {
		return carsRemark;
	}

	public void setCarsRemark(String carsRemark) {
		this.carsRemark = carsRemark;
	}

	public String getEpisodeType() {
		return episodeType;
	}

	public void setEpisodeType(String episodeType) {
		this.episodeType = episodeType;
	}

	public String getOverrideSeqNum() {
		return overrideSeqNum;
	}

	public void setOverrideSeqNum(String overrideSeqNum) {
		this.overrideSeqNum = overrideSeqNum;
	}

	public String getItemCatCode() {
		return itemCatCode;
	}

	public void setItemCatCode(String itemCatCode) {
		this.itemCatCode = itemCatCode;
	}

	public Boolean getReprint() {
		return reprint;
	}

	public void setReprint(Boolean reprint) {
		this.reprint = reprint;
	}

	public String getCddhDrugDesc() {
		return cddhDrugDesc;
	}

	public void setCddhDrugDesc(String cddhDrugDesc) {
		this.cddhDrugDesc = cddhDrugDesc;
	}
	
	public String getSplitFlag() {
		return splitFlag;
	}

	public void setSplitFlag(String splitFlag) {
		this.splitFlag = splitFlag;
	}



	@Transient
	private transient List<CapdVoucherItem> _capdVoucherItemList;
	
	public void disconnectCapdVoucherItemList() {
		_capdVoucherItemList = this.getCapdVoucherItemList();
		this.clearCapdVoucherItemList();
	}

	public void reconnectCapdVoucherItemList() {
		this.setCapdVoucherItemList(_capdVoucherItemList);
		_capdVoucherItemList = null;
	}

	public void setPrintMpDispLabelInstructionFlag(
			Boolean printMpDispLabelInstructionFlag) {
		this.printMpDispLabelInstructionFlag = printMpDispLabelInstructionFlag;
	}

	public Boolean getPrintMpDispLabelInstructionFlag() {
		return printMpDispLabelInstructionFlag;
	}	

	public void updateFromPrevDispOrderItem(DispOrderItem prevDispOrderItem) {
		
		if (!pharmOrderItem.isDirtyFlag()) {

			printFlag = prevDispOrderItem.getPrintFlag();
			if (printFlag) {
				replaceBinNum( prevDispOrderItem.getBinNum() );
				workstationCode = prevDispOrderItem.getWorkstationCode();
			}

			if (prevDispOrderItem.getBaseLabel() != null) {
				setLargeLayoutFlag(prevDispOrderItem.getBaseLabel().getLargeLayoutFlag());
				if (baseLabel instanceof DispLabel) {
					setDeltaChangeType( ((DispLabel)prevDispOrderItem.getBaseLabel()).getDeltaChangeType() );
				}
			}
			
			DispOrderItemStatus prevStatus = prevDispOrderItem.getStatus();
			if (prevStatus.isLaterThan(status)) {
				status = prevStatus;
			}

			if( pharmOrderItem.getSfiItemStatusResetFlag() && status.isLaterThan(DispOrderItemStatus.Assembled) ){
				status = DispOrderItemStatus.Assembled;
			}

			if (status.isLaterThanEqualTo(DispOrderItemStatus.Picked) && prevStatus.isLaterThanEqualTo(DispOrderItemStatus.Picked)) {
				pickUser = prevDispOrderItem.getPickUser();
				pickDate = prevDispOrderItem.getPickDate();
			}

			if (status.isLaterThanEqualTo(DispOrderItemStatus.Assembled) && prevStatus.isLaterThanEqualTo(DispOrderItemStatus.Assembled)) {
				assembleUser = prevDispOrderItem.getAssembleUser();
				assembleDate = prevDispOrderItem.getAssembleDate();
			}
			
			createUser = prevDispOrderItem.getCreateUser();
			createDate = prevDispOrderItem.getCreateDate();
			
			setUserCode(createUser);
		}

		remarkText       = prevDispOrderItem.getRemarkText();
		remarkType       = prevDispOrderItem.getRemarkType();
		remarkUpdateUser = prevDispOrderItem.getRemarkUpdateUser();
		remarkUpdateDate = prevDispOrderItem.getRemarkUpdateDate();
	}
	
	public void replaceBinNum(String binNum) {
		this.binNum = binNum;
		
		if (baseLabel == null) {
			return;
		}
		baseLabel.setBinNum(binNum);
		
		if (labelXml == null) {
			return;
		}
		JaxbWrapper<BaseLabel> labelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
		labelXml = labelJaxbWrapper.marshall(baseLabel);
	}
	
	private void setUserCode(String userCode) {
		if (baseLabel == null || !(baseLabel instanceof DispLabel)) {
			return;
		}
		((DispLabel) baseLabel).setUserCode(userCode);
		
		if (labelXml == null) {
			return;
		}
		JaxbWrapper<BaseLabel> labelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
		labelXml = labelJaxbWrapper.marshall(baseLabel);
	}
	
	public void setLargeLayoutFlag(Boolean largeLayoutFlag) {
		if (baseLabel == null) {
			return;
		}
		baseLabel.setLargeLayoutFlag(largeLayoutFlag);
		
		if (labelXml == null) {
			return;
		}
		JaxbWrapper<BaseLabel> labelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
		labelXml = labelJaxbWrapper.marshall(baseLabel);
	}

	public void setDeltaChangeType(String deltaChangeType) {
		if (baseLabel == null || !(baseLabel instanceof DispLabel)) {
			return;
		}
		
		((DispLabel) baseLabel).setDeltaChangeType(deltaChangeType);
		
		if (labelXml == null) {
			return;
		}
		JaxbWrapper<BaseLabel> labelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
		labelXml = labelJaxbWrapper.marshall(baseLabel);
	}
	
	public void setPrintLabelDetail(PrintLabelDetail printLabelDetail) {
		this.printLabelDetail = printLabelDetail;
	}

	public PrintLabelDetail getPrintLabelDetail() {
		return printLabelDetail;
	}

	public Boolean getSingleDispFlag() {
		return singleDispFlag;
	}

	public void setSingleDispFlag(Boolean singleDispFlag) {
		this.singleDispFlag = singleDispFlag;
	}

	public Boolean getUrgentFlag() {
		return urgentFlag == null ? Boolean.FALSE : urgentFlag;
	}

	public void setUrgentFlag(Boolean urgentFlag) {
		this.urgentFlag = urgentFlag;
	}

	public void setPrintRefillAuxLabelFlag(Boolean printRefillAuxLabelFlag) {
		this.printRefillAuxLabelFlag = printRefillAuxLabelFlag;
	}

	public Boolean getPrintRefillAuxLabelFlag() {
		return printRefillAuxLabelFlag == null ? Boolean.FALSE : printRefillAuxLabelFlag;
	}

	public void setChargeQty(BigDecimal chargeQty) {
		this.chargeQty = chargeQty;
	}

	public BigDecimal getChargeQty() {
		return chargeQty;
	}

	public void setReplenishFlag(Boolean replenishFlag) {
		this.replenishFlag = replenishFlag;
	}

	public Boolean getReplenishFlag() {
		return replenishFlag;
	}

	public void setVerifyUser(String verifyUser) {
		this.verifyUser = verifyUser;
	}

	public String getVerifyUser() {
		return verifyUser;
	}

	public void setReplenishItemUpdateUser(String replenishItemUpdateUser) {
		this.replenishItemUpdateUser = replenishItemUpdateUser;
	}

	public String getReplenishItemUpdateUser() {
		return replenishItemUpdateUser;
	}

	public String getAspItemType() {
		return aspItemType;
	}

	public void setAspItemType(String aspItemType) {
		this.aspItemType = aspItemType;
	}

	public Boolean getSfiOverrideWarnFlag() {
		return sfiOverrideWarnFlag;
	}

	public void setSfiOverrideWarnFlag(Boolean sfiOverrideWarnFlag) {
		this.sfiOverrideWarnFlag = sfiOverrideWarnFlag;
	}
}
