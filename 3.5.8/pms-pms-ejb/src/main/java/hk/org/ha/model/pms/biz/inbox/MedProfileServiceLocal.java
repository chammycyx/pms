package hk.org.ha.model.pms.biz.inbox;
import hk.org.ha.model.pms.udt.medprofile.FindMedProfileListResult;

import javax.ejb.Local;

@Local
public interface MedProfileServiceLocal {

	FindMedProfileListResult findMedProfileList(String hkidCaseNum);
	void destroy();
}
