package hk.org.ha.model.pms.vo.checkissue.mp;

import hk.org.ha.model.pms.udt.medprofile.PrintMode;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class CheckDeliveryInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String batchNum;
	
	private Date batchDate;
	
	private PrintMode printMode;
	
	private String wardCode;
	
	private Date updateDate;
	
	private Date checkDate;
	
	private Long deliveryId;
	
	public CheckDeliveryInfo (String batchNum, Date batchDate, PrintMode printMode, String wardCode, Date checkDate, Date updateDate, Long deliveryId) {
		this.batchNum = batchNum;
		this.batchDate = batchDate;
		this.printMode = printMode;
		this.wardCode = wardCode;
		this.checkDate = checkDate;
		this.updateDate = updateDate;
		this.deliveryId = deliveryId;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public Date getBatchDate() {
		return batchDate;
	}

	public void setBatchDate(Date batchDate) {
		this.batchDate = batchDate;
	}

	public PrintMode getPrintMode() {
		return printMode;
	}

	public void setPrintMode(PrintMode printMode) {
		this.printMode = printMode;
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(Long deliveryId) {
		this.deliveryId = deliveryId;
	}

	public Date getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}
	
}