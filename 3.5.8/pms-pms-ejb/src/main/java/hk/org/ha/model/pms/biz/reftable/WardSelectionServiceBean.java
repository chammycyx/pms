package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.vo.medprofile.Ward;
import hk.org.ha.model.pms.vo.medprofile.WardGroup;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("wardSelectionService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
@SuppressWarnings("unused")
public class WardSelectionServiceBean implements WardSelectionServiceLocal {

	@Logger
	private Log logger;
	
	@In
	private MpWardManagerLocal mpWardManager;
	
	@In
	private Workstore workstore;
	
	@In
	private Workstation workstation;
	
	@Out(required = false)
	private List<Ward> wardSelectionList;
	
	@Out(required = false)
	private List<WardGroup> wardGroupSelectionList;
	
	@Out(required = false)
	private List<String> wardCodeSelectionList;
	
	@Out(required = false)
	private List<String> wardGroupCodeSelectionList;
	
	
    public WardSelectionServiceBean() {
    }

	@Override
	public void retrieveWardAndWardGroupList() {
		
		wardGroupSelectionList = mpWardManager.retrieveWardGroupList(workstore.getWorkstoreGroup());
		wardSelectionList = mpWardManager.retrieveWardList(workstore.getWorkstoreGroup());
		List<String> wardCodeDisplayList = mpWardManager.retrieveActiveWardCodeList(workstore);

		Set<String> wardCodeDisplaySet = new HashSet<String>();
		for (String wardCode: wardCodeDisplayList) {
			wardCodeDisplaySet.add(wardCode);
		}
		
		for (Ward ward: wardSelectionList) {
			ward.setManageByCurrentWorkstore(wardCodeDisplaySet.contains(ward.getWardCode()));
		}
	}
	
	@Remove
	@Override
	public void destroy() {
		wardSelectionList = null;
		wardGroupSelectionList = null;
	}
}
