package hk.org.ha.model.pms.persistence.disp;

import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;

import java.io.Serializable;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class MedOrderPrescTypeAdapter extends XmlAdapter<String, MedOrderPrescType> implements Serializable {

	private static final long serialVersionUID = 1L;

	public String marshal( MedOrderPrescType v ) {		
		return (v == null) ? null : v.getDataValue();
	}
	
	public MedOrderPrescType unmarshal( String v ) {		
		return MedOrderPrescType.dataValueOf(v);
	}
}
