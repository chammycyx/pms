package hk.org.ha.model.pms.persistence.reftable;

import hk.org.ha.model.pms.persistence.PropEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "OPERATION_PROP")
public class OperationProp extends PropEntity {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "operationPropSeq")
	/*lower the inital value to avoid the auto delete by using clean_db() */
	@SequenceGenerator(name = "operationPropSeq", sequenceName = "SQ_OPERATION_PROP", initialValue = 1000000)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "OPERATION_PROFILE_ID", nullable = false)
	private OperationProfile operationProfile;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public OperationProfile getOperationProfile() {
		return operationProfile;
	}

	public void setOperationProfile(OperationProfile operationProfile) {
		this.operationProfile = operationProfile;
	}
}
