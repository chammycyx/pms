package hk.org.ha.model.pms.biz.reftable;

import static hk.org.ha.model.pms.prop.Prop.WORKSTORE_ACTIVATEOPERATIONPROFILE_LOGOUTSESSION;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.support.PropManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.phs.PatientCat;
import hk.org.ha.model.pms.persistence.reftable.Machine;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.persistence.reftable.OperationProp;
import hk.org.ha.model.pms.persistence.reftable.OperationWorkstation;
import hk.org.ha.model.pms.persistence.reftable.PickStation;
import hk.org.ha.model.pms.persistence.reftable.Prop;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.persistence.reftable.WorkstationPrint;
import hk.org.ha.model.pms.persistence.reftable.WorkstationProp;
import hk.org.ha.model.pms.persistence.reftable.WorkstoreProp;
import hk.org.ha.model.pms.udt.RecordStatus;
import hk.org.ha.model.pms.udt.reftable.MaintScreen;
import hk.org.ha.model.pms.udt.reftable.OperationWorkstationType;
import hk.org.ha.model.pms.udt.reftable.PrinterConnectionType;
import hk.org.ha.model.pms.udt.reftable.PrinterType;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("operationModeListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class OperationModeListServiceBean implements OperationModeListServiceLocal {
	
	@In
	private AuditLogger auditLogger;
	
	@PersistenceContext
	private EntityManager em;

	@Out(required = false)
	private List<OperationProfile> operationProfileList;
	
	@Out(required = false)
	private List<Workstation> operationModeWorkstationList;
	
	@Out(required = false)
	private List<PickStation> operationModePickStationList;
	
	@Out(required = false)
	private List<WorkstoreProp> commonRuleProfileList;

	@Out(required = false)
	private List<PatientCat> patientCatList;
	
	@In
	private Workstore workstore;
	
	@In
	private PickStationListManagerLocal pickStationListManager;
	
	@In 
	private CorpPmsServiceJmsRemote corpPmsServiceProxy;

	@In
	private PropManagerLocal propManager;
	
	@In
	private RefTableManagerLocal refTableManager;
	
	private boolean invalidateSession;
	private static OperationPropComparator operationPropComparator = new OperationPropComparator();
	
	public void retrievePickStationList() 
	{	
		operationModePickStationList = pickStationListManager.retrievePickStationList();
	}
	
	@SuppressWarnings("unchecked")
	public void retrieveOperationModeList() 
	{		
		retrievePickStationList();
		retrievePatientCatList();
		commonRuleProfileList = retrieveCommonRuleProfileList();
		
		workstore = em.find(Workstore.class, workstore.getId());

		operationModeWorkstationList = em.createQuery(
				"select o from Workstation o" + // 20120303 index check : Workstation.workstore : FK_WORKSTATION_01
				" where o.workstore = :workstore" + 
				" order by o.workstationCode")
				.setParameter("workstore", workstore)
				.getResultList();
		
		if ( !operationModeWorkstationList.isEmpty() ) {
			for (Workstation workstation : operationModeWorkstationList ) {
				workstation.loadPrintInfo();
			}
		}
		
		operationProfileList = retrieveOperationProfileList();
		
		OperationProfile activeProfile = em.find(OperationProfile.class, workstore.getOperationProfileId());
		
		for (OperationProfile operationProfile:operationProfileList) {
			if(activeProfile.getId().equals(operationProfile.getId())) {
				operationProfile.setActive(true);
			}
			
			Collections.sort(operationProfile.getOperationPropList(), operationPropComparator);
			
			List<OperationWorkstation> operationWorkstationList = em.createQuery(
					"select o from OperationWorkstation o" + // 20120303 index check : OperationWorkstation.operationProfile : FK_OPERATION_WORKSTATION_01
					" where o.operationProfile = :operationProfile" +
					" order by o.workstation.workstationCode")
					.setParameter("operationProfile", operationProfile)
					.getResultList();
			
			operationProfile.setOperationWorkstationList(operationWorkstationList);
			
			for (OperationWorkstation operationWorkstation :operationProfile.getOperationWorkstationList()) {
				
				operationWorkstation.getWorkstation();
				if(operationWorkstation.getMachine() !=null){
					operationWorkstation.getMachine().getPickStation();
				}
			}
		}
	}
	
	public void updateOperationModeList(Collection<OperationProp> operationPropList, 
										Collection<OperationWorkstation> operationWorkstationList, 
										Collection<Workstation> newWorkstationList, 
										Collection<PickStation> pickStationList, 
										Long activeOperationProfileId, 
										Collection<WorkstoreProp> workstorePropList) 
	{
		invalidateSession = false;
		workstore = em.find(Workstore.class, workstore.getId());
		
		//update workstoreProp
		List<WorkstoreProp> syncWorkstoreProp = new ArrayList<WorkstoreProp>();
		for ( WorkstoreProp workstoreProp : workstorePropList ) {
			em.merge(workstoreProp);
			auditLogger.log("#0567", 
								workstoreProp.getWorkstore().getHospCode(), 
								workstoreProp.getWorkstore().getWorkstoreCode(), 
								workstoreProp.getProp().getName(), 
								workstoreProp.getOrgValue(), 
								workstoreProp.getValue());
			
			if ( workstoreProp.getProp().getSyncFlag() ) {
				syncWorkstoreProp.add(workstoreProp);
			}
		}
		
		if ( !syncWorkstoreProp.isEmpty() ) {
			corpPmsServiceProxy.updateWorkstoreProp(syncWorkstoreProp, true);
		}
		
		//update OperationProp
		for (OperationProp operationProp:operationPropList){
			if ( StringUtils.equals(operationProp.getOrgValue(), operationProp.getValue()) ) {
				continue;
			}
			
			auditLogger.log("#1070", operationProp.getOperationProfile().getType(), operationProp.getProp().getName(), operationProp.getOrgValue(), operationProp.getValue());
			em.merge(operationProp);
		}
		
		if ( !pickStationList.isEmpty() ) {
			pickStationListManager.updatePickStationList(pickStationList);
		}
		
		//update OperationWorkstation
		for (OperationWorkstation operationWorkstation: operationWorkstationList){
			if ( operationWorkstation.getMachine() !=null ) {
				if ( operationWorkstation.getMachine().getId() == null ) {
					operationWorkstation.setMachine(retrieveMachine(operationWorkstation.getMachine()));
				} else {
					em.merge(operationWorkstation.getMachine());
				}
			}
			em.merge(operationWorkstation);
		}		
		em.flush();
		em.clear();
		
		
		//new workstation
		for (Workstation workstation: newWorkstationList) {
			
			if (workstation.getId() == null) {
				workstation.updatePrintInfo();
				workstation.setWorkstore(workstore);
				workstation.setHostName(workstation.getHostName());
				em.persist(workstation);
				createWorkstationProp(workstation);
				
				List<OperationProfile> oplist = retrieveOperationProfileList();
				
				OperationProfile ap = em.find(OperationProfile.class, workstore.getOperationProfileId());
				
				for (OperationProfile operationProfile:oplist) {
					if( !operationProfile.getId().equals(ap.getId())){
						OperationWorkstation newOperationWorkstation = new OperationWorkstation();
						newOperationWorkstation.setOperationProfile(operationProfile);
						newOperationWorkstation.setPrinterConnectionType(PrinterConnectionType.No);
						newOperationWorkstation.setWorkstation(workstation);
						newOperationWorkstation.setType(OperationWorkstationType.MultiFunction);
						newOperationWorkstation.setPrinterType(PrinterType.Normal);
						newOperationWorkstation.setIssueWindowNum(0);
						em.persist(newOperationWorkstation);
					}
				}
			}else {
				List<WorkstationPrint> workstationPrintList = workstation.buildPrintInfo();
				workstation.clearWorkstationPrintList();
				workstation = em.merge(workstation);
				em.flush();
				for (WorkstationPrint workstationPrint : workstationPrintList) {
					workstation.addWorkstationPrint(workstationPrint);
				}
			}
		}
		
		em.flush();
		em.clear();
		
		//active profile
		boolean activeOperation = false;
		if ( activeOperationProfileId != null ) {
			updateActiveProfile(activeOperationProfileId, false);
			activeOperation = true;
		}
		
		em.flush();
		em.clear();

		retrieveOperationModeList();
		
		if ( activeOperation && WORKSTORE_ACTIVATEOPERATIONPROFILE_LOGOUTSESSION.get(false) ) {
			corpPmsServiceProxy.invalidateSession(workstore);	
			invalidateSession = true;
		}	
	}
	
	private void createWorkstationProp(Workstation workstation){
		List<Prop> propList= propManager.retrievePropListByWorkstationPropName();
		for (Prop prop : propList ) {
			WorkstationProp workstationProp = new WorkstationProp();
			workstationProp.setProp(prop);
			workstationProp.setValue(prop.getDefValue());
			workstationProp.setWorkstation(workstation);
			workstationProp.setSortSeq(prop.getId().intValue());
			em.persist(workstationProp);
		}
	}
	
	public void updateActiveProfile(Long activeOperationProfileId, boolean setInvalidateSession){
		OperationProfile activeProfile = em.find(OperationProfile.class, workstore.getOperationProfileId());
		String orgActivePropfileName = activeProfile.getName();
		OperationProfile op = em.find(OperationProfile.class, activeOperationProfileId);

		List<WorkstoreProp> syncWorkstoreProp = new ArrayList<WorkstoreProp>();
		if ( op != null ) {
			activeProfile.setName(op.getName());
			activeProfile.setType(op.getType());
			
			Map<String, OperationProp> operationPropMap = new HashMap<String, OperationProp>();
			for (OperationProp operationProp : op.getOperationPropList()) {
				operationPropMap.put(operationProp.getProp().getName(), operationProp);
				
				if ( operationProp.getProp().getSyncFlag() ) {
					syncWorkstoreProp.add(convertToWorkstoreProp(operationProp));
				}
			}
			for (OperationProp activeProp : activeProfile.getOperationPropList()) {
				if ( !operationPropMap.containsKey(activeProp.getProp().getName() ) ) {
					continue;
				}
				OperationProp operationProp = operationPropMap.get(activeProp.getProp().getName());
				if ( !StringUtils.equals(operationProp.getValue(), activeProp.getValue()) ) {
					auditLogger.log("#0390", operationProp.getProp().getName(), activeProp.getValue(), operationProp.getValue());
					activeProp.setValue(operationProp.getValue());
				}
			}
			
			Map<String, OperationWorkstation> activeOwMap = new HashMap<String, OperationWorkstation>();
			for ( OperationWorkstation activeOw : activeProfile.getOperationWorkstationList() ) {
				activeOwMap.put(activeOw.getWorkstation().getWorkstationCode(), activeOw);
			}

			for (OperationWorkstation operationWorkstation : op.getOperationWorkstationList()){
				OperationWorkstation activeOw = activeOwMap.get(operationWorkstation.getWorkstation().getWorkstationCode());
				if ( activeOw == null ){
					activeOw = new OperationWorkstation();
					activeOw.setWorkstation(operationWorkstation.getWorkstation());
					activeOw.setOperationProfile(activeProfile);
					activeProfile.getOperationWorkstationList().add(activeOw);
					em.persist(activeOw);
				}
				if ( operationWorkstation.getType() != activeOw.getType() ) {
					activeOw.setType(operationWorkstation.getType());
				}
				if ( !operationWorkstation.getIssueWindowNum().equals(activeOw.getIssueWindowNum()) ) {
					activeOw.setIssueWindowNum(operationWorkstation.getIssueWindowNum());
				}
				if ( operationWorkstation.getPrinterType() != activeOw.getPrinterType() ) {
					activeOw.setPrinterType(operationWorkstation.getPrinterType());
				}
				if ( !StringUtils.equals(operationWorkstation.getRedirectPrintStation(), activeOw.getRedirectPrintStation()) ) {
					activeOw.setRedirectPrintStation(operationWorkstation.getRedirectPrintStation());
				}
				if ( operationWorkstation.getEnableFlag() != activeOw.getEnableFlag() ) {
					activeOw.setEnableFlag(operationWorkstation.getEnableFlag());
				}
				if ( operationWorkstation.getPrinterConnectionType() != activeOw.getPrinterConnectionType() ) {
					activeOw.setPrinterConnectionType(operationWorkstation.getPrinterConnectionType());
				}
				if ( operationWorkstation.getMachine() != activeOw.getMachine() ) {
					activeOw.setMachine(operationWorkstation.getMachine());
				}
				if ( operationWorkstation.getFastQueuePrintFlag() != activeOw.getFastQueuePrintFlag() ) {
					activeOw.setFastQueuePrintFlag(operationWorkstation.getFastQueuePrintFlag());
				}
				if ( !StringUtils.equals(operationWorkstation.getRedirectFastQueueStation(), activeOw.getRedirectFastQueueStation()) ) {
					activeOw.setRedirectFastQueueStation(operationWorkstation.getRedirectFastQueueStation());
				}
			}

			em.flush();
			em.clear();
			
			refTableManager.clearActiveProfile();
			
			auditLogger.log("#0391", orgActivePropfileName, activeProfile.getName());
		}
		
		if ( !syncWorkstoreProp.isEmpty() ) {
			corpPmsServiceProxy.updateWorkstoreProp(syncWorkstoreProp, true);
		}
		
		retrieveOperationModeList();
		
		if ( setInvalidateSession ) {
			corpPmsServiceProxy.invalidateSession(workstore);	
		}
	}
	
	private WorkstoreProp convertToWorkstoreProp(OperationProp operationProp){
		WorkstoreProp workstoreProp = new WorkstoreProp();
		workstoreProp.setProp(operationProp.getProp());
		workstoreProp.setWorkstore(operationProp.getOperationProfile().getWorkstore());
		workstoreProp.setValue(operationProp.getValue());
		return workstoreProp;
	}
	
	@SuppressWarnings("unchecked")
	private Machine retrieveMachine(Machine newMachine){
		Machine machine = null;
		List<Machine> machineList = em.createQuery(
				"select o from Machine o" + // 20120303 index check : PickStation.workstore,pickStationNum : UI_PICK_STATION_01
				" where o.pickStation.workstore = :workstore" +
				" and o.pickStation.pickStationNum = :pickStationNum" +
				" and o.type = :type ")
				.setParameter("workstore", workstore)
				.setParameter("pickStationNum", newMachine.getPickStation().getPickStationNum())
				.setParameter("type", newMachine.getType())
				.getResultList();
		
		if ( !machineList.isEmpty() ) {
			machine = machineList.get(0);
		}
		return machine;
	}
	
	@SuppressWarnings("unchecked")
	private List<OperationProfile> retrieveOperationProfileList(){
		return em.createQuery(
				"select o from OperationProfile o" + // 20120303 index check : OperationProfile.workstore : FK_OPERATION_PROFILE_01
				" where o.workstore = :workstore")
				.setParameter("workstore", workstore)
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	private List<WorkstoreProp> retrieveCommonRuleProfileList() {
		List<MaintScreen> maintScreenList = new ArrayList<MaintScreen>();
		maintScreenList.add(MaintScreen.OperationMode);
		
		return em.createQuery(
					"select o from WorkstoreProp o" + // 20121112 index check : WorkstoreProp.workstore : FK_WORKSTORE_PROP_01
					" where o.workstore = :workstore" +
					" and o.prop.maintScreen in :maintScreenList" +
					" order by o.prop.description")
					.setParameter("workstore", workstore)
					.setParameter("maintScreenList", maintScreenList)
					.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	private void retrievePatientCatList() {
		
		if (patientCatList==null) {
			patientCatList = em.createQuery(
					"select o from PatientCat o" + // 20120303 index check : PatientCat.institution : FK_PATIENT_CAT_01
					" where o.institution = :institution" +
					" and o.status = :status")
					.setParameter("institution", workstore.getWorkstoreGroup().getInstitution())
					.setParameter("status", RecordStatus.Active)
					.getResultList();
		}
	}

	public boolean isInvalidateSession() {
		return invalidateSession;
	}
	
    private static class OperationPropComparator implements Comparator<OperationProp>, Serializable {
		private static final long serialVersionUID = 1L;

		@Override
		public int compare(OperationProp operationProp1, OperationProp operationProp2) {
			int result = operationProp1.getSortSeq().compareTo(operationProp2.getSortSeq());
			if ( result == 0 ) {
				result = operationProp1.getProp().getDescription().compareTo(operationProp2.getProp().getDescription());
			}
			return result;
		}	
   }

	@Remove
	public void destroy() 
	{
		if (operationProfileList != null) {
			operationProfileList = null;
		}
		if (operationModeWorkstationList != null) {
			operationModeWorkstationList = null;
		}
		if ( operationModePickStationList != null ) {
			operationModePickStationList = null;
		}
		if ( commonRuleProfileList != null ) {
			commonRuleProfileList = null;
		}
		if (patientCatList != null) {
			patientCatList = null;
		}
	}
}
