package hk.org.ha.model.pms.vo.label;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "LabelContainer")
@ExternalizedBean(type=DefaultExternalizer.class)
public class LabelContainer implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@XmlElement(required = false)
	private PivasWorksheet pivasWorksheet;
	
	@XmlElement(required = false)
	private PivasProductLabel pivasProductLabel;
	
	@XmlElement(required = false)
	private PivasOuterBagLabel pivasOuterBagLabel;
	
	@XmlElement(required = false)
	private MdsOverrideReasonLabel mdsOverrideReasonLabel;
	
	@XmlElement(required = false)
	private PivasMergeFormulaWorksheet pivasMergeFormulaWorksheet;
	
	@XmlElement(required = false)
	private PivasMergeFormulaProductLabel pivasMergeFormulaProductLabel;

	public PivasWorksheet getPivasWorksheet() {
		return pivasWorksheet;
	}

	public void setPivasWorksheet(PivasWorksheet pivasWorksheet) {
		this.pivasWorksheet = pivasWorksheet;
	}

	public PivasProductLabel getPivasProductLabel() {
		return pivasProductLabel;
	}

	public void setPivasProductLabel(PivasProductLabel pivasProductLabel) {
		this.pivasProductLabel = pivasProductLabel;
	}
	
	public PivasOuterBagLabel getPivasOuterBagLabel() {
		return pivasOuterBagLabel;
	}

	public void setPivasOuterBagLabel(PivasOuterBagLabel pivasOuterBagLabel) {
		this.pivasOuterBagLabel = pivasOuterBagLabel;
	}

	public MdsOverrideReasonLabel getMdsOverrideReasonLabel() {
		return mdsOverrideReasonLabel;
	}

	public void setMdsOverrideReasonLabel(
			MdsOverrideReasonLabel mdsOverrideReasonLabel) {
		this.mdsOverrideReasonLabel = mdsOverrideReasonLabel;
	}

	public PivasMergeFormulaWorksheet getPivasMergeFormulaWorksheet() {
		return pivasMergeFormulaWorksheet;
	}

	public void setPivasMergeFormulaWorksheet(
			PivasMergeFormulaWorksheet pivasMergeFormulaWorksheet) {
		this.pivasMergeFormulaWorksheet = pivasMergeFormulaWorksheet;
	}

	public PivasMergeFormulaProductLabel getPivasMergeFormulaProductLabel() {
		return pivasMergeFormulaProductLabel;
	}

	public void setPivasMergeFormulaProductLabel(
			PivasMergeFormulaProductLabel pivasMergeFormulaProductLabel) {
		this.pivasMergeFormulaProductLabel = pivasMergeFormulaProductLabel;
	}
}
