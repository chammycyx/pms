package hk.org.ha.model.pms.biz.alert;

import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;

import java.util.List;

import javax.ejb.Local;

@Local
public interface AlertAuditServiceLocal {

	void alertMsg(String messageCode, String desc);

	void newAlertDetectForManualProfile(String action, Long medProfileId, String hkid, String caseNum, List<AlertMsg> alertMsgList);
	
	void cancelPrescribeForManualProfile(String action, Long medProfileId, String hkid, String caseNum, String orderDesc);
	
	void destroy();

}
