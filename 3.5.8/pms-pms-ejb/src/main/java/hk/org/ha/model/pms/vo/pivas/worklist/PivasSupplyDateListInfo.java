package hk.org.ha.model.pms.vo.pivas.worklist;

import hk.org.ha.model.pms.persistence.reftable.pivas.PivasServiceHour;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PivasSupplyDateListInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Date defaultSupplyDate;
	
	private List<PivasServiceHour> pivasServiceHourList;

	public Date getDefaultSupplyDate() {
		return defaultSupplyDate;
	}

	public void setDefaultSupplyDate(Date defaultSupplyDate) {
		this.defaultSupplyDate = defaultSupplyDate;
	}

	public List<PivasServiceHour> getPivasServiceHourList() {
		return pivasServiceHourList;
	}

	public void setPivasServiceHourList(List<PivasServiceHour> pivasServiceHourList) {
		this.pivasServiceHourList = pivasServiceHourList;
	}
	
}