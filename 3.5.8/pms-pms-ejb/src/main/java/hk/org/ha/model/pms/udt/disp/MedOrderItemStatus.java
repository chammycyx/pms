package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum MedOrderItemStatus implements StringValuedEnum {

	Outstanding("O", "Outstanding"),
	Fulfilling("F", "Fulfilling"),
	Completed("C", "Completed"), 
	Deleted("D", "Deleted"),
	SysDeleted("X", "SysDeleted");
	
    private final String dataValue;
    private final String displayValue;

    MedOrderItemStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static MedOrderItemStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MedOrderItemStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<MedOrderItemStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<MedOrderItemStatus> getEnumClass() {
    		return MedOrderItemStatus.class;
    	}
    }
}
