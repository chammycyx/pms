package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.reftable.ItemDispConfigManagerLocal;
import hk.org.ha.model.pms.biz.reftable.SpecialtyManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.PmsDispenseConfig;
import hk.org.ha.model.pms.dms.persistence.PmsDrugProperty;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
import hk.org.ha.model.pms.persistence.phs.Specialty;
import hk.org.ha.model.pms.persistence.reftable.FdnMapping;
import hk.org.ha.model.pms.persistence.reftable.ItemDispConfig;
import hk.org.ha.model.pms.persistence.reftable.ItemSpecialty;
import hk.org.ha.model.pms.udt.reftable.FdnType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("mpItemService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MpItemServiceBean implements MpItemServiceLocal {

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<FdnMapping> itemMaintFdnMappingList;
	
	@Out(required = false)
	private List<ItemSpecialty> itemMaintItemSpecList;

	@Out(required = false)
	private ItemDispConfig itemMaintItemDispConfig;
	
	@Out(required = false)
	private List<Specialty> itemMaintSpecialtyList;
	
	@In
	private Workstore workstore;
	
	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private ItemDispConfigManagerLocal itemDispConfigManager;
	
	@In
	private SpecialtyManagerLocal specialtyManager;
	
	@Out(required = false)
	private DmDrug itemMaintDmDrug;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	private PmsDispenseConfig pmsDispenseConfig;
	
	private PmsDrugProperty pmsDrugProperty ;
	
	@SuppressWarnings("unchecked")
	public void retrieveMpItem(String itemCode) {
		
		clearItemMaint();

		initSpecialtyList();
		
		WorkstoreDrug drug = workstoreDrugCacher.getWorkstoreDrug(workstore, itemCode);
		if( drug != null ){
			if(drug.getMsWorkstoreDrug() != null && drug.getDmDrug()!=null){
				itemMaintDmDrug = drug.getDmDrug();
			}else{
				return;
			}
		}else{
			return;
		}
			
		itemMaintFdnMappingList = retrieveFdnMappingList(itemCode);
		
		List<Specialty> specialtyList = em.createQuery(
				"select o from Specialty o" + // 20120831 index check : Specialty.institution FK_PECIALTY_01
				" where o.institution = :institution")
				.setParameter("institution", workstore.getWorkstoreGroup().getInstitution())
				.getResultList();
		
		Map<String, String> specialtyMap = new HashMap<String, String>();
		for( Specialty specialty : specialtyList ){
			specialtyMap.put(specialty.getSpecCode(), specialty.getDescription());
		}
		
		itemMaintItemSpecList = em.createQuery(
				"select o from ItemSpecialty o" + // 20120303 index check : ItemSpecialty.workstoreGroup,itemCode UI_ITEM_SPECIALTY_01
				" where o.workstoreGroup = :workstoreGroup" + 
				" and o.itemCode = :itemCode" +
				" order by o.createDate, o.id")
				.setParameter("workstoreGroup", workstore.getWorkstoreGroup())
				.setParameter("itemCode", itemCode)
				.getResultList();
		
		//retrieve spec by init, create map, and set desc to trans
		
		for( ItemSpecialty itemSpecialty : itemMaintItemSpecList ){
			if( specialtyMap.get(itemSpecialty.getSpecCode()) != null ){
				itemSpecialty.setDescription( specialtyMap.get(itemSpecialty.getSpecCode()) );
			}
		}
		
		ItemDispConfig itemDispConfig = itemDispConfigManager.retrieveItemDispConfig(itemCode, workstore.getWorkstoreGroup());

		if( itemDispConfig != null ){
			itemMaintItemDispConfig = itemDispConfig;
		}
		else{
			itemMaintItemDispConfig = new ItemDispConfig();
		}
		
		pmsDispenseConfig = dmsPmsServiceProxy.retrievePmsDispenseConfig(itemCode,  workstore.getWorkstoreGroup().getWorkstoreGroupCode());
		
		if (pmsDispenseConfig != null) {
			if( pmsDispenseConfig.getDefaultDispenseDay() != null ){
				itemMaintItemDispConfig.setDefaultDispDays(pmsDispenseConfig.getDefaultDispenseDay());
			}else{
				itemMaintItemDispConfig.setDefaultDispDays(0);
			}
			itemMaintItemDispConfig.setDdWithout2DCodeFlag(pmsDispenseConfig.getDdWithout2DCodeFlag());
			itemMaintItemDispConfig.setWardStockWithout2DCodeFlag(pmsDispenseConfig.getWardStockWithout2DCodeFlag());
		}		
		
		DmDrug dmDrug = dmDrugCacher.getDmDrug(itemCode);
		if(dmDrug != null){
			pmsDrugProperty = dmsPmsServiceProxy.retrievePmsDrugProperty(dmDrug.getDrugKey(),workstore.getWorkstoreGroup().getWorkstoreGroupCode());
		}
		
		if(pmsDrugProperty != null){
			
			if( pmsDrugProperty.getReviewDay() != null ){
				itemMaintItemDispConfig.setReviewDay(pmsDrugProperty.getReviewDay());
			}else{
				itemMaintItemDispConfig.setReviewDay(0);
			}
			itemMaintItemDispConfig.setReviewRepeatFlag(pmsDrugProperty.getReviewRepeatFlag());
			itemMaintItemDispConfig.setReviewMessage(pmsDrugProperty.getReviewMessage());
		}
		
		itemMaintItemDispConfig.setEnableReviewInfoFlag(Boolean.TRUE);
		
		List<DmDrug> sameDrugKeyDmDrugList = dmDrugCacher.getDmDrugListByDrugKey(itemMaintDmDrug.getDrugKey());
		
		for (DmDrug tmpDmDrug : sameDrugKeyDmDrugList) {
			if ("PDF".equals(tmpDmDrug.getTheraGroup()) || tmpDmDrug.getDmMoeProperty().getDiluentCode() != null) {
				itemMaintItemDispConfig.setEnableReviewInfoFlag(Boolean.FALSE);
				break;
			}
		}
		
	}
	
	private void initSpecialtyList(){
		if( itemMaintSpecialtyList == null ){
			itemMaintSpecialtyList = specialtyManager.retrieveSpecialtyListWithActiveStatus();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void updateMpItem(Collection<FdnMapping> fdnMappingList, Collection<ItemSpecialty> itemSpecialtyList, 
			ItemDispConfig itemDispConfig, String itemCode) {
		WorkstoreGroup workstoreGroup = workstore.getWorkstoreGroup();
		
		itemDispConfig.setWorkstoreGroup(workstoreGroup);
		
		if( itemDispConfig.getDefaultDispDays() != null ){
			pmsDispenseConfig.setDefaultDispenseDay(itemDispConfig.getDefaultDispDays());
		}else{
			pmsDispenseConfig.setDefaultDispenseDay( 0 );
		}
		
		if(itemDispConfig.getDdWithout2DCodeFlag() != null){
			pmsDispenseConfig.setDdWithout2DCodeFlag(itemDispConfig.getDdWithout2DCodeFlag());
		}
		if(itemDispConfig.getWardStockWithout2DCodeFlag() != null){
			pmsDispenseConfig.setWardStockWithout2DCodeFlag(itemDispConfig.getWardStockWithout2DCodeFlag());
		}

		dmsPmsServiceProxy.updatePmsDispenseConfig( pmsDispenseConfig );
		
		
		//pms-4044
		if(itemDispConfig.getReviewDay() != null){
			pmsDrugProperty.setReviewDay(itemDispConfig.getReviewDay());
		}else{
			pmsDrugProperty.setReviewDay(0);
		}
		
		if(itemDispConfig.getReviewRepeatFlag() != null){
			pmsDrugProperty.setReviewRepeatFlag(itemDispConfig.getReviewRepeatFlag());
		}
		if(itemDispConfig.getReviewMessage() != null){
			pmsDrugProperty.setReviewMessage(itemDispConfig.getReviewMessage());
		}
		
		dmsPmsServiceProxy.updatePmsDrugProperty(pmsDrugProperty);
		
		if( itemDispConfig.getId()!=null ){
			if( !isValidConfigInfo(itemDispConfig) ){
				em.remove( em.merge(itemDispConfig) );
			}else{
				em.merge(itemDispConfig);
			}
		}else{
			if( isValidConfigInfo(itemDispConfig) ){
				em.persist(itemDispConfig);
			}
		}
		em.flush();
		
		for(ItemSpecialty itemSpecialty : itemSpecialtyList){
			if( itemSpecialty.getMarkDelete() ){
				if( !itemSpecialty.isNew() ){
					//for prevent violate null check constraint before remove record
					if( StringUtils.isBlank(itemSpecialty.getSpecCode()) ){
						itemSpecialty.setSpecCode("*");
					}
					em.remove( em.merge(itemSpecialty) );
				}
			}
		}
		em.flush();
		
		for(ItemSpecialty itemSpecialty : itemSpecialtyList){
			if( !itemSpecialty.getMarkDelete() ){
				if( !itemSpecialty.isNew() ){
					em.merge(itemSpecialty);
				}else{
					itemSpecialty.setWorkstoreGroup(workstoreGroup);
					em.persist(itemSpecialty);
				}
			}	
		}
		em.flush();
		
		for(FdnMapping fdnMapping : fdnMappingList){
			if( fdnMapping.isNew() ){
				if (!FdnType.DefaultName.equals(fdnMapping.getType())) {
					fdnMapping.setWorkstore(em.find(Workstore.class, fdnMapping.getWorkstore().getId()));
					fdnMapping.setOrderType(OrderType.InPatient);
					em.persist(fdnMapping);
					em.flush();		
				}				
			}else{
				if (FdnType.DefaultName.equals(fdnMapping.getType())) { 
					em.remove(em.merge(fdnMapping));
				} else {
					em.merge(fdnMapping);
				}
				em.flush();		
			}
		}
		em.flush();
		
		itemMaintItemDispConfig = itemDispConfig;
	}
	
	private boolean isValidConfigInfo(ItemDispConfig itemDispConfig){
		return ( itemDispConfig.getDailyRefillFlag() || itemDispConfig.getSingleDispFlag() || StringUtils.isNotBlank(itemDispConfig.getWarningMessage()) );
	}
	
	@SuppressWarnings("unchecked")
	private List<FdnMapping> retrieveFdnMappingList(String itemCode) 
	{
		List<FdnMapping> fdnMappingList = new ArrayList<FdnMapping>();

		List<FdnMapping> tempFdnMappingList = em.createQuery( 
				"select o from FdnMapping o" +		// 20120303 index check : FdnMapping.workstore,orderType,itemCode : UI_FDN_MAPPING_01
				" where o.workstore.hospCode = :hospCode" + 
				" and o.orderType = :orderType" +
				" and o.itemCode = :itemCode")
				.setParameter("hospCode", workstore.getHospCode())
				.setParameter("orderType", OrderType.InPatient)
				.setParameter("itemCode", itemCode)
				.getResultList();

		for(Workstore wks : workstore.getHospital().getWorkstoreList()){
			boolean found = false;
			for(FdnMapping fdnMapping : tempFdnMappingList){
				if(fdnMapping.getWorkstore().getId().equals(wks.getId())){
					fdnMappingList.add(fdnMapping);
					found = true;
					break;
				}
			}
			if(!found){
				FdnMapping fdnMapping = new FdnMapping();
				fdnMapping.setWorkstore(wks);
				fdnMapping.setItemCode(itemCode);
				fdnMapping.setType(FdnType.DefaultName);
				fdnMapping.setOrderType(OrderType.InPatient);
				fdnMappingList.add(fdnMapping);
			}
		}
		
		return fdnMappingList;
	}
	
	public void clearItemMaint() {
		if (itemMaintItemDispConfig != null) {
			itemMaintItemDispConfig = null;
		}
		if (itemMaintItemSpecList != null) {
			itemMaintItemSpecList = null;
		}
		if (itemMaintFdnMappingList != null) {
			itemMaintFdnMappingList = null;
		}
		if (itemMaintDmDrug != null) {
			itemMaintDmDrug = null;
		}
		if (pmsDispenseConfig != null) {
			pmsDispenseConfig = null;
		}
	}

	@Remove
	public void destroy() {
		clearItemMaint();
	}
}
