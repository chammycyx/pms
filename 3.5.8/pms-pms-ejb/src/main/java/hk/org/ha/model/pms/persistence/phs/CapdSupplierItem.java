package hk.org.ha.model.pms.persistence.phs;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.RecordStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;


@Entity
@Table(name = "CAPD_SUPPLIER_ITEM")
public class CapdSupplierItem extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ITEM_CODE", length = 6)
	private String itemCode;

	@Column(name = "SUPPLIER_CODE", length = 4)
	private String supplierCode;
	
    @ManyToOne
    @JoinColumn(name = "SUPPLIER_CODE", nullable = false, insertable = false, updatable = false)
    private Supplier supplier;

    @Converter(name = "CapdSupplierItem.status", converterClass = RecordStatus.Converter.class)
    @Convert("CapdSupplierItem.status")
	@Column(name="STATUS", nullable = false, length = 1)
	private RecordStatus status;

    public CapdSupplierItem() {
    	status = RecordStatus.Active; 
    }
    
    public CapdSupplierItem(String itemCode) {
    	this();
    	this.itemCode = itemCode;
    }
    
    public CapdSupplierItem(String itemCode, String supplierCode) {
    	this(itemCode);
    	this.supplierCode = supplierCode;
    }
    
	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getSupplierCode() {
		return supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public RecordStatus getStatus() {
		return status;
	}

	public void setStatus(RecordStatus status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {		
		return new HashCodeBuilder()
			.append(itemCode)
			.append(supplierCode)
			.toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		CapdSupplierItem other = (CapdSupplierItem) obj;
		return new EqualsBuilder()
			.append(itemCode, other.getItemCode())
			.append(supplierCode, other.getSupplierCode())
			.isEquals();
	}
}
