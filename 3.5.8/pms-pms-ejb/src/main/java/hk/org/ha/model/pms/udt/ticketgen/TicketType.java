package hk.org.ha.model.pms.udt.ticketgen;

public enum TicketType {
	Queue1,
	Queue2,
	FastQueue;
}
