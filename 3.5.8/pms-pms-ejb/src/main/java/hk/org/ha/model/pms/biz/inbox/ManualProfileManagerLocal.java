package hk.org.ha.model.pms.biz.inbox;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;

import javax.ejb.Local;

@Local
public interface ManualProfileManagerLocal {

	MedProfile constructManualProfile(String hkid, String caseNum, Workstore workstore);
	
}
