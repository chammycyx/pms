package hk.org.ha.model.pms.udt.disp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum TicketMatchDocType implements StringValuedEnum {

	Rx("RX", "Rx"),
	ScanTicket("TT", "Ticket"),
	ManualTicket("MT", "Ticket"),
	DrugLabel("DL", "Label"); 
	
    private final String dataValue;
    private final String displayValue;

    TicketMatchDocType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static TicketMatchDocType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(TicketMatchDocType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<TicketMatchDocType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<TicketMatchDocType> getEnumClass() {
    		return TicketMatchDocType.class;
    	}
    }
}