package hk.org.ha.model.pms.biz.delivery;

import static hk.org.ha.model.pms.prop.Prop.DELIVERY_BATCHNUM;
import static hk.org.ha.model.pms.prop.Prop.DELIVERY_BATCHNUM_MAX;
import static hk.org.ha.model.pms.prop.Prop.DELIVERY_BATCHNUM_MIN;
import static hk.org.ha.model.pms.prop.Prop.DELIVERY_PIVAS_BATCHNUM;
import static hk.org.ha.model.pms.prop.Prop.DELIVERY_PIVAS_BATCHNUM_MAX;
import static hk.org.ha.model.pms.prop.Prop.DELIVERY_PIVAS_BATCHNUM_MIN;
import static hk.org.ha.model.pms.prop.Prop.DELIVERY_PURCHASEREQUEST_BATCHNUM;
import static hk.org.ha.model.pms.prop.Prop.DELIVERY_PURCHASEREQUEST_BATCHNUM_MAX;
import static hk.org.ha.model.pms.prop.Prop.DELIVERY_PURCHASEREQUEST_BATCHNUM_MIN;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.reftable.HospitalProp;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.joda.time.DateMidnight;

@AutoCreate
@Stateless
@Name("batchNumGenerator")
@MeasureCalls
public class BatchNumGeneratorBean implements BatchNumGeneratorLocal {

	private static final String BATCH_NUM_FORMAT = "0000";	
	
	@PersistenceContext
	private EntityManager em;
	
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public String getNextFormattedBatchNum(Hospital hospital, Date batchDate, BatchType batchType) {
		
		return new DecimalFormat(BATCH_NUM_FORMAT).format(getNextBatchNum(hospital, batchDate, batchType));
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public int getNextBatchNum(Hospital hospital, Date batchDate, BatchType batchType) {
		
		List<Long> hospitalPropIdList = this.lockHospitalProp(hospital, resolveBatchNumPropName(batchType));
		
		List<HospitalProp> hospitalPropList = em.createQuery(
				"select o from HospitalProp o" + // 20120312 index check : none
				" where o.id in :idList")
				.setParameter("idList", hospitalPropIdList)
				.getResultList();
		
		HospitalProp prop = hospitalPropList.get(0);
		
		int currentNum = ((Integer) prop.getConvertedValue()).intValue();
		
		if (new DateMidnight(batchDate).compareTo(new DateMidnight(prop.getUpdateDate())) != 0) {
			currentNum = resolveBatchNumMinValue(batchType, hospital);
		} else {
			currentNum += 1;
		}
		
		if (currentNum > resolveBatchNumMaxValue(batchType ,hospital)) {
			throw new RuntimeException("no batch num avaliable for " + hospital.getHospCode() + " on " + batchDate);
		}
		
		prop.setValue(String.valueOf(currentNum));
		
		// need to set the updateDate manually 
		// in case the currentNum not change on different day
		prop.setUpdateDate(batchDate);
		
		return currentNum;
	}
	
	@SuppressWarnings("unchecked")
	private List<Long> lockHospitalProp(Hospital hospital, String... propNames) {
		
		if (propNames.length == 0) {
			throw new IllegalArgumentException("missing propName(s)!");
		}
		
		// need to use native query in order to only lock the hospital_prop table
		Query query = em.createNativeQuery(
			"select o.id from hospital_prop o" + // 20130129 index check : HospitalProp.hospital,prop : UI_HOSPITAL_PROP_01
			" where o.hosp_code = ?1" +
			" and o.prop_id in (select p.id from prop p where p.name in ('" + StringUtils.join(propNames, "','")  + "'))" +
			" for update");
			query.setParameter(1, hospital.getHospCode());
		return query.getResultList();
	}
	
	private String resolveBatchNumPropName(BatchType batchType) {
		
		switch (batchType) {
			case Normal:
				return DELIVERY_BATCHNUM.getName();
			case Sfi:
				return DELIVERY_PURCHASEREQUEST_BATCHNUM.getName();
			case Pivas:
				return DELIVERY_PIVAS_BATCHNUM.getName();
			default:
				throw new IllegalArgumentException("Unsupported BatchType - " + batchType);
		}
	}
	
	private Integer resolveBatchNumMinValue(BatchType batchType, Hospital hospital) {
		
		switch (batchType) {
			case Normal:
				return DELIVERY_BATCHNUM_MIN.get(hospital, 1);
			case Sfi:
				return DELIVERY_PURCHASEREQUEST_BATCHNUM_MIN.get(hospital, 8000);
			case Pivas:
				return DELIVERY_PIVAS_BATCHNUM_MIN.get(hospital, 9000);
			default:
				throw new IllegalArgumentException("Unsupported BatchType - " + batchType);
		}
	}
	
	private Integer resolveBatchNumMaxValue(BatchType batchType, Hospital hospital) {
		
		switch (batchType) {
			case Normal:
				return DELIVERY_BATCHNUM_MAX.get(hospital, 7999);
			case Sfi:
				return DELIVERY_PURCHASEREQUEST_BATCHNUM_MAX.get(hospital, 8999);
			case Pivas:
				return DELIVERY_PIVAS_BATCHNUM_MAX.get(hospital, 9999);
			default:
				throw new IllegalArgumentException("Unsupported BatchType - " + batchType);
		}
	}
}
